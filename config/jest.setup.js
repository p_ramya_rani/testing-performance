// 9fbef606107a605d69c0edbcd8029e5d
const { configure } = require('enzyme');
const Adapter = require('@wojtekmaj/enzyme-adapter-react-17');

configure({ adapter: new Adapter() });

window.matchMedia =
  window.matchMedia ||
  function matchMedia() {
    return {
      matches: false,
      addListener() {},
      removeListener() {},
    };
  };

jest.mock('../packages/core/src/components/common/hoc/withErrorBoundary/errorBoundary', () => {
  return (WrappedComponent) => WrappedComponent;
});

jest.mock('express-http-context', () => {
  return {
    get: () => 'sample-id',
    set: () => {},
  };
});
