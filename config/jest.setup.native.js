// 9fbef606107a605d69c0edbcd8029e5d
// eslint-disable-next-line import/no-unresolved
import { NativeModules } from 'react-native';
import MockStorage from './__mocks__/MockStorage';

const { configure } = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

configure({ adapter: new Adapter() });

Date.now = jest.fn(() => new Date('2019-08-01'));

const storageCache = {};
const AsyncStorage = new MockStorage(storageCache);

// Mock Asyncstorage
jest.setMock('@react-native-community/async-storage', AsyncStorage);

// Mock Timers for Animation
jest.useFakeTimers();

jest.mock('react-native-cookies', () => ({
  addEventListener: jest.fn(),
  removeEventListener: jest.fn(),
  openURL: jest.fn(),
  canOpenURL: jest.fn(),
  getInitialURL: jest.fn(),
  get: () => Promise.resolve(null),
}));

jest.mock('react-native-reanimated', () => {
  return {
    Value: jest.fn(),
    event: jest.fn(),
    add: jest.fn(),
    eq: jest.fn(),
    set: jest.fn(),
    cond: jest.fn(),
    interpolate: jest.fn(),
    Extrapolate: { CLAMP: jest.fn() },
    createAnimatedComponent: jest.fn(),
    Easing: {
      in: jest.fn(),
      out: jest.fn(),
      inOut: jest.fn(),
    },
    addWhitelistedUIProps: jest.fn(),
    addWhitelistedNativeProps: jest.fn(),
  };
});

// eslint-disable-next-line no-underscore-dangle
global.__reanimatedWorkletInit = jest.fn();

jest.mock('@stevenmasini/react-native-fast-image', () => {
  return {};
});

// jest.mock('react-native-check-notification-permission', () => ({
//   changeNotificationSetting: jest.fn(),
//   checkNotificationPermission: jest.fn(() => Promise.resolve(false)),
// }));

jest.mock('react-native-awesome-card-io', () => {
  return {
    DETECTION_MODE: 'IMAGE_AND_NUMBER',
    CardIOUtilities: {
      preload: jest.fn(() => Promise.resolve('the response')),
    },
  };
});

jest.mock('react-native-keychain', () => {
  return {
    setGenericPassword: jest.fn(),
    getGenericPassword: jest.fn(() => Promise.resolve({ username: '', password: '' })),
    resetGenericPassword: jest.fn(),
  };
});

jest.mock('react-native-permissions', () => {
  return {
    checkNotifications: jest.fn(() => Promise.resolve('granted')),
  };
});

jest.mock('react-native-gesture-handler', () => {
  return {
    gestureHandlerRootHOC: jest.fn(),
  };
});

jest.mock('react-native-device-info', () => {
  return {
    getVersion: jest.fn(),
  };
});

// Mock firebase analytics methods
jest.mock('@react-native-firebase/analytics', () => {
  return () => ({
    logAppOpen: jest.fn(),
    logEvent: jest.fn(),
  });
});

jest.mock('react-native/Libraries/Utilities/Platform', () => {
  const platform = jest.requireActual('react-native/Libraries/Utilities/Platform');
  return {
    ...platform,
    constants: {
      ...platform.constants,
      reactNativeVersion: {
        major: 0,
        minor: 62,
        patch: 2,
      },
    },
  };
});

// Mock NetInfo for react-native modules
NativeModules.RNCNetInfo = {
  getCurrentState: jest.fn(() => Promise.resolve()),
  addListener: jest.fn(),
  removeListeners: jest.fn(),
};

// Mock permission for react-native modules
NativeModules.RNPermissions = {};
