// 9fbef606107a605d69c0edbcd8029e5d
module.exports = {
  rootDir: '../',
  preset: './packages/mobileapp/node_modules/react-native/jest-preset.js',
  verbose: true,
  roots: ['<rootDir>/packages/core', '<rootDir>/packages/mobileapp'],
  moduleDirectories: ['node_modules', './packages/mobileapp/node_modules'],
  moduleFileExtensions: ['js', 'jsx'],
  moduleNameMapper: {
    'styled-components':
      '<rootDir>/packages/mobileapp/node_modules/styled-components/native/dist/styled-components.native.cjs.js',
    barcode: '<rootDir>/packages/mobileapp/node_modules/react-native-barcode-builder/index.js',
  },
  testMatch: [
    '**/core/**/__tests__/*-test.native.+(js|jsx)',
    '**/mobileapp/**/__tests__/*-test.+(js|jsx)',
  ],

  transform: {
    '^.+\\.jsx?$': '<rootDir>/packages/mobileapp/node_modules/react-native/jest/preprocessor.js',
  },
  setupFiles: ['<rootDir>/config/jest.setup.native.js'],
  collectCoverage: true,
  coverageDirectory: 'reports/mobile/coverage',
  collectCoverageFrom: [
    '**/core/**/*.native.js',
    '**/core/**/*.native.jsx',
    '**/core/**/*.app.js',
    '**/core/**/*.app.jsx',
    '**/mobileapp/**/*.js',
    '**/mobileapp/**/*.jsx',
    '!**/*.style.js',
    '!**/*.style.native.js',
    '!**/*.constants.js',
    '!**/*.config.js',
    '!**/core/styles/**',
    '!**/*.actions.js',
    '!**/*.container.js',
    '!**/*.query.js',
    '!**/__mocks__/**',
  ],
  testPathIgnorePatterns: ['/deprecated/'],
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '/deprecated/',
    'index.native.js',
    'index.js',
    'mock.js',
    '/flow-typed/',
    '/pages/',
    '/__mocks__/',
    '/features/account/LoginPage/container/loginUtils/keychain.utils.native.js',
    '/common/atoms/Toast/views/Toast.native.jsx',
    'vendorLib/atoms/GooglePlaceInput/views/GooglePlacesAutocomplete.native.jsx',
    '/common/atoms/Swipeable',
    '/PickupStoreModal/',
    '/features/account/Payment/molecules/GiftCards/views/GiftCards.native.jsx',
    'packages/mobileapp/src/components/features/analytics/container/Analytics.container.native.jsx',
    '/src/context/constants',
    'initializeStore.js',
    'sagas.js',
    'packages/mobileapp/src/components/features/content/Navigation/molecules/NavMenuLevel2/views/NavMenuLevel2.view.jsx',
    'packages/core/src/components/common/atoms/DamImage/views/DamImage.native.jsx',
  ],
  coverageReporters: ['json', 'lcov', 'text', 'text-summary'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  transformIgnorePatterns: [
    'node_modules/(?!(jest-)?react-native|@react-native|@react-native-community|react-navigation)',
  ],
};
