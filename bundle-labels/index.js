// 9fbef606107a605d69c0edbcd8029e5d
const axios = require('axios');
const path = require('path');
const fs = require('fs');
const { argv } = require('optimist');

const updatedTimeStamp = new Date().getTime();
const assetsDir = path.resolve(__dirname, '../packages/mobileapp/src/assets/json');

const getLabelsDataJSON = (brandId, endpoint) => {
  axios
    .post(
      `${endpoint}/graphql`,
      {
        operationName: 'fetchCMSData',
        variables: {},
        query: `query fetchCMSData {labels: labels(brand: "${brandId}", country: "USA", channel: "Mobile") {  name  errorMessage  subcategories {    name    labels {      key      value      __typename    }    referred {      name      contentId      __typename    }    __typename  }  __typename}}`,
      },
      {
        timeout: 20000,
        headers: { 'Content-Type': 'application/json' },
      }
    )
    .then((response) => {
      if (response && response.data && response.data.data) {
        const { labels } = response.data.data;
        fs.writeFileSync(
          `${assetsDir}/${brandId.toLowerCase()}-labels.json`,
          JSON.stringify({
            lastUpdated: updatedTimeStamp,
            labels,
          })
        );
      }
    })
    .catch((error) => {
      console.error(error);
    });
};
console.log('tcpUrl---->', argv.tcpUrl);
console.log('gymUrl---->', argv.gymUrl);

getLabelsDataJSON(
  'TCP',
  argv.tcpUrl ? argv.tcpUrl.replace('www', 'rwd-stage') : 'https://rwd-stage.childrensplace.com'
);
getLabelsDataJSON(
  'GYM',
  argv.gymUrl ? argv.gymUrl.replace('www', 'rwd-stage') : 'https://rwd-stage.gymboree.com'
);
