module.exports = () => `<!DOCTYPE html>
<html lang="en">

<head>
  <title>Login</title>
  <script type="text/javascript">
    function login(e) {
      console.log(this);

      var urlParams = new URLSearchParams(window.location.search);
      var queryRedirectURI = urlParams.get('redirect_uri');
      var queryState = urlParams.get('state');
      var queryScope = urlParams.get('scope');

      var data = JSON.stringify({
        "logonId1": document.getElementById('username').value,
        "logonPassword1": document.getElementById('password').value
      });

      var xhr = new XMLHttpRequest();
      xhr.withCredentials = true;

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
          console.log(this);
          var redirectURI = this.getResponseHeader('redirect_uri')
          console.log('redirectURI', redirectURI);
          var scope = this.getResponseHeader('scope');
          console.log('scope', scope);
          var responseJSON = JSON.parse(this.responseText);
          console.log('responseJSON.accessCode', responseJSON.accessCode);
          var urlSearchParams = new URLSearchParams();
          urlSearchParams.append('code', responseJSON.accessCode);
          urlSearchParams.append('scope', queryScope);
          urlSearchParams.append('state', queryState);
          var redirectURL = redirectURI + '?' + urlSearchParams.toString();
          console.log('Redirect to: ' + redirectURL)
          window.location = redirectURL;
        }
      });

      var postQueryParams = new URLSearchParams();
      postQueryParams.append("redirect_uri", queryRedirectURI);
      postQueryParams.append("scope", queryScope);
      postQueryParams.append("state", queryState);
      var postURL = "https://test-uat4.childrensplace.com/api/v2/account/loginIdentityjson" + "?" + postQueryParams.toString();
      xhr.open("POST", postURL);

      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("accept", "application/json");
      xhr.setRequestHeader("cache-control", "no-store, must-revalidate");
      xhr.setRequestHeader("storeid", "10151");

      xhr.send(data);
      return false;
    }
  </script>
</head>

<body>
  <h1>Login Form</h1>
  <form onsubmit="login();return false;" method="post">
    <label for="username">User Name:</label>
    <input type="text" id="username" name="logonId"><br><br>
    <label for="password">Password:</label>
    <input type="password" id="password" name="logonPassword" style="margin-left: 13px;"><br><br>
    <button type="submit">
      Login
    </button>
  </form>
</body>

</html>
`;
