// 9fbef606107a605d69c0edbcd8029e5d
const superagent = require('superagent');
const logger = require('@tcp/core/src/utils/loggerInstance');
const https = require('https');
const fs = require('fs');
const API_CONFIG = require('./config/apiConfig');
const os = require('os');
const httpContext = require('express-http-context');

const certOptions = {
  key: fs.readFileSync('./server/ssl/server.key'),
  cert: fs.readFileSync('./server/ssl/server.cer'),
};

if (process.env.RWD_APPD_ENABLED === 'true') {
  try {
    require('appdynamics').profile({
      controllerHostName: process.env.RWD_APPD_CONTROLLER_HOST_NAME,
      controllerPort: 443,
      controllerSslEnabled: true,
      accountName: process.env.RWD_APPD_ACCOUNT_NAME,
      accountAccessKey: process.env.RWD_APPD_ACCOUNT_ACCESS_KEY,
      applicationName: process.env.RWD_APPD_APPLICATION_NAME,
      tierName: process.env.RWD_APPD_TIER_NAME,
      nodeName: os.hostname(),
      noNodeNameSuffix: true,
      maxProcessSnapshotsPerPeriod: 0,
      logging: {
        logfiles: [
          { filename: 'nodejs_agent_%N.log', level: 'TRACE' },
          { filename: 'nodejs_agent_%N.protolog', level: 'TRACE', channel: 'protobuf' },
        ],
      },
    });
  } catch (error) {
    logger.error('Unable to initialize AppDynamics', error);
  }
}

const express = require('express');
const { parse } = require('url');
const next = require('next');
const helmet = require('helmet');
const morgan = require('morgan');
const rfs = require('rotating-file-stream');
const path = require('path');

const {
  sitesInfo: {
    port: WEB_PORT,
    assetHost,
    BV_API_KEY,
    BV_URL,
    GQL_WARN_ENABLED,
    FACEBOOK_SHARE_URL,
    GQL_WARN_THRESHOLD,
    langId,
    MELISSA_KEY,
    unbxd,
  },
} = API_CONFIG;

const getPort = () => process.env.PORT || WEB_PORT || 3000;

const shouldAppendToken = process.env.ADD_TOKEN === true || process.env.ADD_TOKEN === 'true';

const {
  ROUTES_LIST,
  ROUTING_MAP,
  ROUTE_PATH,
  preRouteSlugs,
} = require('@tcp/core/src/config/route.config');
const { join } = require('path');

const {
  initErrorReporter,
  getExpressMiddleware,
} = require('@tcp/core/src/utils/errorReporter.util');
const { ENV_DEVELOPMENT, ENV_PRODUCTION } = require('@tcp/core/src/constants/env.config');
const {
  settingHelmetConfig,
  sites,
  siteIds,
  setEnvConfig,
  HEALTH_CHECK_PATH,
  ERROR_REDIRECT_STATUS,
  CACHE_CLEAR_PATH,
} = require('./config/server.config');

const dev = process.env.NODE_ENV === 'development';
const isLocalBuild = process.env.NODE_LOCAL_ENV_BUILD === 'true';
const isProd = process.env.NODE_ENV === ENV_PRODUCTION;
setEnvConfig(dev || isLocalBuild);
const isLocalEnv = process.env.ENV_ID === 'LOCAL';
const port = getPort();
const apiEndpoint = process.env.API_DOMAIN || ''; // TO ensure relative URLs for MS APIs
const assetEndpoint = process.env.STATIC_ASSET_PATH;
const webDomainName = process.env.DOMAIN_NAME || '';
const graphQlEndpoint = process.env.GRAPHQL_API_ENDPOINT;
const graphQlEndpointServer = process.env.GRAPHQL_API_ENDPOINT_SERVER;

const baseConfig = {
  ACQUISITION_ID: process.env.ACQUISITION_ID,
  analyticsUrl: process.env.ANALYTICS_SCRIPT_URL,
  apiEndpoint: `${apiEndpoint}/`,
  assetHost: `${apiEndpoint}/`,
  assetHostTCP: process.env.WEB_DAM_HOST || assetHost,
  assetHostGYM: process.env.WEB_DAM_HOST || assetHost,
  assetHostSNJ: process.env.WEB_DAM_HOST || assetHost,
  BAZAARVOICE_SPOTLIGHT: process.env.BV_SPOTLIGHT_API_KEY,
  BAZAARVOICE_REVIEWS: process.env.BV_PRODUCT_REVIEWS_API_KEY,
  borderFree: process.env.BORDERS_FREE,
  borderFreeComm: process.env.BORDERS_FREE_COMM,
  BV_API_KEY: process.env.BV_API_KEY || BV_API_KEY,
  BV_API_URL: process.env.BV_API_URL || BV_URL,
  BV_SHARED_KEY: process.env.BV_SHARED_KEY,
  CANDID_API_KEY: process.env.CANDID_API_KEY,
  CANDID_API_URL: process.env.CANDID_URL,
  channelId: API_CONFIG.channelIds.Desktop, // TODO - Make it dynamic for all 3 platforms
  crossDomain: process.env.CROSS_DOMAIN,
  brandSpecificCrossDomain: {
    TCP: process.env.CROSS_DOMAIN_TCP || '',
    GYM: process.env.CROSS_DOMAIN_GYM || '',
    SNJ: process.env.CROSS_DOMAIN_SNJ || '',
  },
  damCloudName: process.env.RWD_CLOUDINARY_CLOUD_NAME,
  domain: `${apiEndpoint}/${process.env.API_IDENTIFIER}/`,
  enableGqlWarn: process.env.ENABLE_GQL_WARN || GQL_WARN_ENABLED || false,
  envId: process.env.ENV_ID,
  facebookShareURL: FACEBOOK_SHARE_URL,
  fbkey: process.env.FACEBOOKKEY,
  fontsAssetPath: process.env.FONTS_HOST,
  googleApiKey: process.env.GOOGLE_MAPS_API_KEY,
  mapBoxApiKey: process.env.MAPBOX_API_KEY,
  mapBoxApiURL: process.env.MAPBOX_API_URL,
  mapboxStoreLocatorApiURL: process.env.MAPBOX_STORE_LOCATOR_API_URL,
  gqlWarnThreshold: process.env.GQL_WARN_THRESHOLD || GQL_WARN_THRESHOLD || 10000,
  GRAPHQL_CLIENT_CACHE_CLEAR_INTERVAL: +process.env.GQL_PURGE_TIME_MINS,
  GRAPHQL_API_TIMEOUT: process.env.GRAPHQL_API_TIMEOUT,
  GRAPHQL_API_TIMEOUT_CLIENT: process.env.GRAPHQL_API_TIMEOUT_CLIENT,
  graphql_region: process.env.GRAPHQL_API_REGION,
  graphql_endpoint_url: `${graphQlEndpoint}/${process.env.GRAPHQL_API_IDENTIFIER}`,
  graphql_endpoint_url_server: `${graphQlEndpointServer}/${process.env.GRAPHQL_API_IDENTIFIER}`,
  graphql_persisted_query: process.env.GQL_PQ,
  graphql_auth_type: process.env.GRAPHQL_API_AUTH_TYPE,
  graphql_api_key: process.env.GRAPHQL_API_KEY || '',
  instakey: process.env.INSTAGRAM,
  instagramStatus: process.env.RWD_INSTAGRAM_STATUS,
  isErrorReportingNodeActive: process.env.IS_ERROR_REPORTING_NODE_ACTIVE,
  isErrorReportingBrowserActive: process.env.IS_ERROR_REPORTING_BROWSER_ACTIVE,
  langId: process.env.LANGID || langId,
  MELISSA_KEY: process.env.MELISSA_KEY || MELISSA_KEY,
  paypalEnv: process.env.PAYPAL_ENV,
  paypalStaticUrl: process.env.RWD_APP_PAYPAL_STATIC_DOMAIN,
  previewEnvId: process.env.PREVIEW_ENV,
  productAssetPathTCP: `${process.env.DAM_PRODUCT_IMAGE_PATH}/tcp`,
  productAssetPathGYM: `${process.env.DAM_PRODUCT_IMAGE_PATH}/gym`,
  productAssetPathSNJ: `${process.env.DAM_PRODUCT_IMAGE_PATH}/snj`,
  queryOverridesUrl: process.env.QUERY_OVERRIDES_JSON || '',
  raygunApiKey: process.env.RAYGUN_API_KEY,
  RWD_WEB_SNARE_SCRIPT_URL: process.env.SNARE_SCRIPT_URL,
  staticAssetHost: `${assetEndpoint}`,
  styliticsRegionGYM: process.env.STYLITICS_REGION_GYM,
  styliticsUserNameTCP: process.env.STYLITICS_USERNAME_TCP,
  styliticsUserNameGYM: process.env.STYLITICS_USERNAME_GYM,
  traceIdCount: 0,
  unbxdTCP: process.env.UNBXD_DOMAIN_TCP || unbxd,
  unbxdGYM: process.env.UNBXD_DOMAIN_GYM || unbxd,
  unbxdSNJ: process.env.UNBXD_DOMAIN_SNJ || unbxd,
  webDomainName,
  firstLoadPLPProductCount: process.env.PLP_PRODUCT_COUNT || 8,
  enableApplePay: process.env.APPLE_PAY_PAYMENT_ENABLED || false,
  enableApplePayOnBilling: process.env.APPLE_PAY_BILLPG_ENABLED || false,
  BUILD_NUMBER: process.env.BUILD_VERSION || '',
  ssrHomeSlotRenderCount: process.env.SSR_HOME_SLOT_COUNT,
  shouldAppendToken,
  isNonInvStagEnabled: process.env.IS_NON_INV_STAG_ENABLED,
  snjMinOrderNo: process.env.SNJ_MIN_ORDER_NO,
};

function getAPIInfoFromEnv(env, countryCode = 'us', languageCode = 'en') {
  const country = countryCode.toUpperCase();
  const language = languageCode.toUpperCase();
  const unbxdApiKeyTCP = env[`UNBXD_API_KEY_${country}_${language}_TCP`];
  const unbxdApiKeyGYM = env[`UNBXD_API_KEY_${country}_${language}_GYM`];
  const unbxdSiteKeyTCP = env[`UNBXD_SITE_KEY_${country}_${language}_TCP`];
  const unbxdSiteKeyGYM = env[`UNBXD_SITE_KEY_${country}_${language}_GYM`];
  const unbxdApiKeySNJ = env[`UNBXD_API_KEY_${country}_${language}_SNJ`];
  const unbxdSiteKeySNJ = env[`UNBXD_SITE_KEY_${country}_${language}_SNJ`];
  return {
    locale: `${language.toLowerCase()}_${country}`,
    unboxKeyTCP: `${unbxdApiKeyTCP}/${env[`UNBXD_SITE_KEY_${country}_${language}_TCP`]}`,
    unboxKeyGYM: `${unbxdApiKeyGYM}/${env[`UNBXD_SITE_KEY_${country}_${language}_GYM`]}`,
    unboxKeySNJ: `${unbxdApiKeySNJ}/${env[`UNBXD_SITE_KEY_${country}_${language}_SNJ`]}`,
    styliticsRegionTCP: env.STYLITICS_REGION_TCP && country,
    unbxdApiKeyTCP,
    unbxdApiKeyGYM,
    unbxdSiteKeyTCP,
    unbxdSiteKeyGYM,
    unbxdApiKeySNJ,
    unbxdSiteKeySNJ,
    monetateUrl: env.MONETATE_URL,
    monetateChannel: env.MONETATE_CHANNEL,
    monetateEnabled: env.MONETATE_ENABLED,
    isCustomerServiceHelpEnable: env.IS_CSH_ENABLED,
    loadHomePageDataEnabled:
      env.LOAD_HOME_PAGE_DATA_ENABLED === true || env.LOAD_HOME_PAGE_DATA_ENABLED === 'true',
    timeOutGap: env.LOAD_HOME_PAGE_DATA_TIMEOUT,
    afterpayButtonKey: env.AFTERPAY_BUTTON_MERCHANT_KEY,
    afterpayButtonURL: env.AFTERPAY_BUTTON_URL,
    isNonInvStagEnabled: env.IS_NON_INV_STAG_ENABLED,
    isCandidEnabled: env.CANDID_ENABLED,
  };
}

const configs = {
  us: {
    en: {
      ...baseConfig,
      ...API_CONFIG.US_CONFIG_OPTIONS,
      ...getAPIInfoFromEnv(process.env, 'us', 'en'),
    },
    es: {
      ...baseConfig,
      ...API_CONFIG.US_CONFIG_OPTIONS,
      ...getAPIInfoFromEnv(process.env, 'us', 'es'),
    },
  },
  ca: {
    en: {
      ...baseConfig,
      ...API_CONFIG.CA_CONFIG_OPTIONS,
      ...getAPIInfoFromEnv(process.env, 'ca', 'en'),
    },
    fr: {
      ...baseConfig,
      ...API_CONFIG.CA_CONFIG_OPTIONS,
      ...getAPIInfoFromEnv(process.env, 'ca', 'fr'),
    },
  },
};

const app = next({ dev, dir: './src' });
const handle = app.getRequestHandler();

const xrayEnabled = process.env.XRAY_ENABLED === 'true';

const setErrorReporter = (server) => {
  const config = {
    isServer: true,
    envId: process.env.ENV_ID,
    raygunApiKey: process.env.RAYGUN_API_KEY,
    isDevelopment: process.env.NODE_ENV === ENV_DEVELOPMENT,
  };
  if (process.env.IS_ERROR_REPORTING_NODE_ACTIVE) {
    initErrorReporter(config);
  }
  const expressMiddleWare = getExpressMiddleware();
  if (expressMiddleWare) {
    server.use(expressMiddleWare);
  }
};

const getLanguage = (req) => {
  const domain = req.hostname;
  const host = req.headers.host;
  const domainLang = domain.substr(0, 2).toLowerCase();
  const hostLang = host.substr(0, 2).toLowerCase();

  if (domainLang === 'es' || domainLang === 'fr') {
    return domainLang;
  } else {
    return hostLang === 'es' || hostLang === 'fr' ? hostLang : 'en';
  }
};

const getSiteDetails = (req, res) => {
  const { url } = req;
  let siteId = siteIds.us;
  const reqUrl = url.split('/');
  for (let i = 0; i < reqUrl.length - 1; i++) {
    if (reqUrl[i].toLowerCase() === siteIds.ca) {
      siteId = siteIds.ca;
      break;
    }
  }
  return {
    siteId,
    country: siteId === siteIds.ca ? 'CA' : 'US',
    currency: siteId === siteIds.ca ? 'CAD' : 'USD',
    language: getLanguage(req),
  };
};

const getBrandId = (req, res) => {
  if (isLocalEnv) {
    const { hostname } = req;
    let brandId = 'tcp';
    const reqUrl = hostname.split('.');
    for (let i = 0; i < reqUrl.length - 1; i++) {
      if (reqUrl[i].toLowerCase() === 'gymboree') {
        brandId = 'gym';
        break;
      }
      if (reqUrl[i].toLowerCase() === 'sugarandjade') {
        brandId = 'snj';
        break;
      }
    }
    return brandId;
  }
  return process.env.BRANDID;
};

const getHostname = (req, res) => {
  return req.hostname;
};

function createAPIConfig(req, res) {
  const {
    host: reqHostname,
    path: originalPath,
    headers: {
      'x-gql-ak': overRiddenGraphqlEndpoint,
      'x-gql-server': overRiddenGraphqlEndpointServer,
      'x-tcp-channel': isAppChannelHeader,
      preview: isPreviewEnvHeader,
    },
    query: {
      preview: isPreviewEnvQuery,
      preview_date: previewDate,
      is_csr_home_enabled: isCSRHomeEnabledQuery = false,
    },
  } = req;
  const isPreviewEnv = isPreviewEnvHeader || isPreviewEnvQuery || false;
  const isAppChannel = isAppChannelHeader === 'app';

  const { brandId, country = 'us', language = 'en', hostname, currency } = res;
  const {
    unbxdGYM,
    unbxdTCP,
    unbxdSNJ,
    unbxdSiteKeyTCP,
    unbxdSiteKeyGYM,
    unbxdSiteKeySNJ,
    unbxdApiKeyTCP,
    unbxdApiKeyGYM,
    unbxdApiKeySNJ,
  } = configs[country.toLowerCase()][language.toLowerCase()];

  let unbxdParams = {
    unbxdUrl: unbxdTCP,
    unbxdSiteKey: unbxdSiteKeyTCP,
    unbxdApiKey: unbxdApiKeyTCP,
  };
  if (brandId === 'gym') {
    unbxdParams = {
      unbxdUrl: unbxdGYM,
      unbxdSiteKey: unbxdSiteKeyGYM,
      unbxdApiKey: unbxdApiKeyGYM,
    };
  }
  if (brandId === 'snj') {
    unbxdParams = {
      unbxdUrl: unbxdSNJ,
      unbxdSiteKey: unbxdSiteKeySNJ,
      unbxdApiKey: unbxdApiKeySNJ,
    };
  }

  const catalogId = API_CONFIG.CATALOGID_CONFIG[brandId.toUpperCase()][country.toUpperCase()];
  const brandConfig =
    brandId === API_CONFIG.brandIds.gym
      ? API_CONFIG.GYM_CONFIG_OPTIONS
      : API_CONFIG.TCP_CONFIG_OPTIONS;

  let reqBasedParams = {
    brandId,
    catalogId,
    country,
    currency,
    hostname,
    language,
    originalPath,
    reqHostname,
    isPreviewEnv,
    isAppChannel,
    previewDate,
    ...unbxdParams,
  };

  if (overRiddenGraphqlEndpoint) {
    reqBasedParams.graphql_endpoint_url = `${overRiddenGraphqlEndpoint}/${process.env.GRAPHQL_API_IDENTIFIER}`;
  }

  if (overRiddenGraphqlEndpointServer) {
    reqBasedParams.graphql_endpoint_url_server = `${overRiddenGraphqlEndpointServer}/${process.env.GRAPHQL_API_IDENTIFIER}`;
  }

  const isBotRequest = req && req.headers && req.headers['x-bot-user'];

  return {
    ...configs[country.toLowerCase()][language.toLowerCase()],
    ...brandConfig,
    ...reqBasedParams,
    isMobile: false,
    cookie: null,
    isCSREnabledForHome:
      !isBotRequest && (isCSRHomeEnabledQuery || process.env.CSR_HOME_ENABLED === 'true' || false),
  };
}

const redirectToErrorPage = (req, res) => {
  // TODO - To handle all this in Akamai redirect ?
  // This should redirect based on country like us/ca - Note hardcoded US
  // This method should handle all other cases like /wrongCountry/wrongRoute & /us/wrongRoute
  const errorPageRoute = `/${siteIds.us}${ROUTING_MAP.error}`;
  res.redirect(ERROR_REDIRECT_STATUS, errorPageRoute);
};

const redirectToHomePage = (req, res) => {
  const errorPageRoute = `/${siteIds.us}${ROUTE_PATH.home}`;
  res.redirect(ERROR_REDIRECT_STATUS, errorPageRoute);
};

/**
 * Function to get the cache key for the requested path
 * @param {object} req The request object
 */
const getCacheKey = (req) => {
  return `${req.path}`;
};

// create a rotating write stream
const accessLogStream = rfs.createStream('access.log', {
  interval: '1d', // rotate daily
  path: join(__dirname, 'log'),
});

app.prepare().then(() => {
  const server = express();

  if (xrayEnabled) {
    var AWSXRay = require('aws-xray-sdk');
    server.use(AWSXRay.express.openSegment(process.env.XRAY_ENVIRONMENT));
  }
  server.use(httpContext.middleware);
  server.use(function (req, res, next) {
    httpContext.set('RequestId', req.header('x-request-id'));
    httpContext.set('TraceId', req.header('x-amzn-trace-id'));
    httpContext.set('RequestPath', req.path);
    next();
  });

  setErrorReporter(server);
  settingHelmetConfig(server, helmet);
  // static files path - ignore version and serve file from the directory
  // this is being done to avoid serving stale files from Akamai - add version numbers to static files
  server.get(
    '/static/:buildId?/*.(css|js|jpeg|jpg|svg|png|gif|ttf|woff|woff2|eot|otf)',
    (req, res) => {
      const filePath = join(__dirname, '..', 'src/static', `${req.params[0]}.${req.params[1]}`);
      res.sendFile(filePath);
    }
  );

  // Looping through the routes and providing the corresponding resolver route
  ROUTES_LIST.forEach((route) => {
    const routeWithSlug = preRouteSlugs.join('') + route.path;
    // creating routes for country code eg: /:siteId?/xyz
    server.get(routeWithSlug, (req, res) => {
      const reqSiteId = req.params.siteId;
      const isValidSiteId = reqSiteId && sites.includes(reqSiteId.toLowerCase());
      if (!isValidSiteId) {
        redirectToErrorPage(req, res);
      }
      const siteDetails = getSiteDetails(req);
      const brandId = getBrandId(req);
      const hostname = getHostname(req);

      const apiConfig = createAPIConfig(req, { ...siteDetails, brandId, hostname });
      res.locals.apiConfig = { ...apiConfig };

      // Handling routes without params
      if (!route.params) {
        app.render(req, res, route.resolver, req.query);
        return;
      }

      // Handling routes with params
      const params = route.params.reduce((componentParam, paramKey) => {
        // eslint-disable-next-line no-param-reassign
        componentParam[paramKey] = req.params[paramKey];
        return componentParam;
      }, {});
      app.render(req, res, route.resolver, params);
    });
  });

  server.get(HEALTH_CHECK_PATH, (req, res) => {
    res.send({
      success: true,
    });
  });

  // START: IDP POC
  server.get('/idp/authentication', (req, res) => {
    const originalUrl = req.originalUrl;
    const indexOfQuery = originalUrl.indexOf('?');
    const queryString = indexOfQuery >= 0 ? originalUrl.substring(originalUrl.indexOf('?')) : '';
    const processIDPLoginHtml = require('./idpLogin');
    res.send(processIDPLoginHtml(queryString));
  });

  server.get('/callback', (req, res) => {
    const { usid, state, scope, code } = req.query;
    superagent
      .post(
        'https://prd.us.shopper.commercecloud.salesforce.com/api/v1/organization/zzrh_001/oauth2/token'
      )
      .send({ grant_type: 'client_credentials', usid })
      .set({
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization:
          'Basic ZGYwMDI4NTAtYjEyNy00YzgyLWI0MDUtNGUyYzI5MzM4OTJiOmFseHhxb2o2M2FiUThoNTVZRVJncEZ4UGhx',
      })
      .end((error, response) => {
        res.cookie('access_token_sfcc', response.body.access_token);
        res.cookie('refresh_token_sfcc', response.body.refresh_token);
        redirectToHomePage(req, res);
      });
  });

  // END: IDP POC

  // setup the logger
  if (isProd) {
    server.use(morgan('combined', { stream: accessLogStream }));
  }

  server.get('/', redirectToHomePage);

  // handling of other routes
  server.get('*', (req, res) => {
    // TODO - To handle static files and all other routes here..
    // server.get('*', redirectToErrorPage);
    return handle(req, res);
  });

  if (xrayEnabled) {
    server.use(AWSXRay.express.closeSegment());
  }
  if (process.env.mode === 'https') {
    https.createServer(certOptions, server).listen(port, (err) => {
      if (err) throw err;
      logger.info(`> Ready on https://local.childrensplace.com:${port}`);
    });
  } else {
    server.listen(port, (err) => {
      if (err) throw err;
      logger.info(`> Ready on http://local.childrensplace.com:${port}`);
    });
  }
});
