// 9fbef606107a605d69c0edbcd8029e5d 
const API_CONFIG = {
  sites: ['us', 'ca'],
  brands: ['tcp', 'gym'],
  channels: ['Desktop', 'MobileWeb', 'MobileApp'],
  channelIds: {
    Desktop: 'Desktop',
    MobileWeb: 'MobileWeb',
    MobileApp: 'MobileApp',
  },
  siteIds: {
    // the values here are the strings that make up the siteId protion of the sites' urls (i.e., it is the 'us' in the path ( /us/favorites)
    us: 'us',
    ca: 'ca',
  },
  companyIds: {
    us: '1',
    ca: '2',
  },
  brandIds: {
    tcp: 'tcp',
    gym: 'gym',
  },
  TCP_CONFIG_OPTIONS: {
    brandId: 'tcp',
    brandIdCMS: 'TCP',
  },
  GYM_CONFIG_OPTIONS: {
    brandId: 'gym',
    brandIdCMS: 'Gymboree',
  },
  CATALOGID_CONFIG: {
    TCP: {
      US: '10551',
      CA: '10552',
    },
    GYM: {
      US: '10555',
      CA: '10556',
    },
  },
  sitesInfo: {
    port: 8081,
    proto: 'https',
    protoSeparator: '://',
    langId: '-1',
    MELISSA_KEY: '63987687',
    BV_API_KEY: 'e50ab0a9-ac0b-436b-9932-2a74b9486436',
    traceIdCount: 0,
    assetHost: '/',
    domain: '/',
    unbxd: 'https://search.unbxd.io',
    BV_WEB_VIEW_URL: 'https://test5.childrensplace.com/static/bazaarVoice/index.html',
    BV_ENV: 'production',
    BV_SUBMISSION_URL:
      'https://display.ugc.bazaarvoice.com/static/ChildrensPlace/#INSTANCE#/en_US/container.htm?bvaction=rr_submit_review&bvproductId=#PRODUCTID#&bvuserToken=#TOKEN#&env=#ENV#&instance=#DEPLOYMENT_ZONE#',
    BV_TCP_INSTANCE: 'tcp_hosted',
    BV_GYM_INSTANCE: 'gym_hosted',
    BV_SPOTLIGHT_INSTANCE: 'spotlights',
    BV_SHARED_KEY: 'Fca3yih00AVeVDFvmaDwnwlWM',
    BV_SPOTLIGHT_ENV: 'production',
    FACEBOOK_SHARE_URL: 'https://www.facebook.com/sharer.php?u=',
  },
  US_CONFIG_OPTIONS: {
    storeId: '10151',
    isUSStore: true,
    countryKey: '_US',
    siteId: 'us',
    siteIdCMS: 'USA',
  },
  CA_CONFIG_OPTIONS: {
    storeId: '10152',
    isUSStore: false,
    countryKey: '_CA',
    siteId: 'ca',
    siteIdCMS: 'Canada',
  },
  /* --------- UNBXD ------- */
  version: 'V2',
  pagetype: 'boolean',
  variantcount: '100',
  apiRequestTimeout: {
    response: 30000,
    deadline: 120000,
  },
  overlayTimeout: 5000,
  sessionCookieKey: 'QuantumMetricSessionID',
  pageCountCookieKey: 'pv',
  apiContentType: 'application/json',
  landingSite: 'LandingBrand',
};

module.exports = API_CONFIG;
