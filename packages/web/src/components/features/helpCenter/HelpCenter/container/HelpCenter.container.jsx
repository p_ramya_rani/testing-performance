// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import hoistNonReactStatic from 'hoist-non-react-statics';
import { fetchPageLayout, loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import { MODULES_CONSTANT } from '@tcp/core/src/reduxStore/constants';
import { createLayoutPath, getAPIConfig } from '@tcp/core/src/utils';
import HelpCenterView from '../views/HelpCenter.view';
import constants from '../HelpCenter.constants';

const pathPrefix = constants.HELP_CENTER_PAGE_PREFIX;

HelpCenterView.getInitialProps = async ({ store, isServer, query, res = {} }, pageProps) => {
  const state = store.getState();
  const { pageName = constants.HELP_CENTER_HOME_PATH } = query;
  let apiConfig;
  if (isServer) {
    const {
      locals: { apiConfig: config },
    } = res;
    apiConfig = config;
  } else {
    apiConfig = getAPIConfig();
  }
  store.dispatch(loadPageSEOData({ page: `/${pathPrefix}${pageName}`, apiConfig }));
  const formattedPageName = createLayoutPath(pageName);
  if (!isServer && !state.Layouts[formattedPageName]) {
    store.dispatch(fetchPageLayout({ pageName: `${pathPrefix}${pageName}`, apiConfig }));
  }
  return pageProps;
};

HelpCenterView.pageInfo = {
  pageId: 'help-center',
  name: constants.HELP_CENTER_HOME_PATH,
  staticPage: true,
  paramName: 'pageName',
  pathPrefix,
};

// eslint-disable-next-line sonarjs/cognitive-complexity
const mapStateToProps = (state, props) => {
  const {
    router: {
      query: { pageName = constants.HELP_CENTER_HOME_PATH },
    },
  } = props;
  const { Layouts, Modules, SEOData } = state;
  const formattedPageName = createLayoutPath(`${pathPrefix}${pageName}`);
  const helpCenterPageSlots = Layouts[formattedPageName] ? Layouts[formattedPageName].slots : [];

  return {
    slots: helpCenterPageSlots.map((slot) => {
      const { contentId: slotContent = '' } = slot;
      const contentIds = slotContent && slotContent.split(',');
      if (contentIds && contentIds.length > 1) {
        const response = {
          ...slot,
          data: {
            slot: [],
          },
        };

        contentIds.forEach((contentId) => {
          const placeHolderName =
            Modules[contentId] && Modules[contentId].val ? Modules[contentId].val : '';
          response.data.slot.push(
            Modules[contentId] && Modules[contentId].moduleName !== MODULES_CONSTANT.placeholder
              ? Modules[contentId]
              : {
                  ...Modules[contentId],
                  [placeHolderName]: Modules[contentId]
                    ? state[Modules[contentId].moduleClassName][placeHolderName]
                    : '',
                }
          );
        });

        return response;
      }
      return {
        ...slot,
        data: Modules[slot.contentId],
      };
    }),
    labels: state.Labels.global && state.Labels.global.helpCenter,
    selectedPage: pageName,
    seoData: SEOData && SEOData['help-center'],
  };
};

const helpCenterConnectComponent = connect(mapStateToProps)(HelpCenterView);

const helpCenterWithRouter = withRouter(helpCenterConnectComponent);

hoistNonReactStatic(helpCenterWithRouter, helpCenterConnectComponent, {
  getInitialProps: true,
});

export default helpCenterWithRouter;
