// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import module2columns from '@tcp/core/src/components/common/molecules/HelpCenterModuleTwoCol';
import accordion from '@tcp/core/src/components/common/molecules/AccordionModule';
import moduleX from '@tcp/core/src/components/common/molecules/ModuleX';
import { Anchor } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import PageSlots from '@tcp/core/src/components/common/molecules/PageSlots';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import SeoCopy from '@tcp/core/src/components/features/browse/ProductListing/molecules/SeoCopy/views';
import styles from '../styles/HelpCenter.style';

const modules = { module2columns, accordion, moduleX };

const HelpCenterDynamicRender = (slotData) => {
  const { slots, setResStatusNotFound } = slotData;
  if ((!slots || !slots.length) && setResStatusNotFound) {
    setResStatusNotFound();
  }
  return <PageSlots slots={slots} modules={modules} {...slotData} />;
};

const BackButton = ({ labels }) => {
  return (
    <Anchor
      fontSizeVariation="xlarge"
      anchorVariation="secondary"
      title={getLabelValue(labels, 'lbl_helpCenter_continueShopping')}
      to="/home"
      asPath="/home"
    >
      <span className="left-arrow" />
      {getLabelValue(labels, 'lbl_helpCenter_continueShopping')}
    </Anchor>
  );
};

const HelpCenterView = (props) => {
  const fromMobileAppCookie = readCookie('fromMobileApp') || false;
  const { className, labels, seoData } = props;
  return (
    <div className={`${className} helpcenter__view`}>
      {!fromMobileAppCookie && <BackButton labels={labels} />}
      <HelpCenterDynamicRender {...props} />
      <SeoCopy {...seoData} />
    </div>
  );
};

BackButton.propTypes = {
  labels: PropTypes.shape({}),
};

BackButton.defaultProps = {
  labels: {},
};

HelpCenterView.propTypes = {
  slotData: PropTypes.shape({}),
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
  seoData: PropTypes.shape({}),
};

HelpCenterView.defaultProps = {
  slotData: {},
  labels: {},
  seoData: {},
};

export default withStyles(errorBoundary(HelpCenterView), styles);
export { HelpCenterView as HelpCenterViewVanilla };
