// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { SEO_DATA } from '@tcp/core/src/reduxStore/constants';
import StoreDetailContainer from '@tcp/core/src/components/features/storeLocator/StoreDetail';
import { loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import PAGES from '@tcp/core/src/constants/pages.constants';
import { getCurrentStoreInfo } from '@tcp/core/src/components/features/storeLocator/StoreDetail/container/StoreDetail.actions';
import { fetchStoreIdFromUrlPath, isClient, getAPIConfig } from '@tcp/core/src/utils';

const StoreDetail = ({ setResStatusNotFound }) => (
  <StoreDetailContainer setResStatusNotFound={setResStatusNotFound} />
);

StoreDetail.pageInfo = {
  pageId: PAGES.STORE,
};

StoreDetail.getInitialProps = ({ store, query, res, isServer }) => {
  const { storeStr } = query;
  let apiConfig;
  if (isServer) {
    const {
      locals: { apiConfig: config },
    } = res;
    apiConfig = config;
  } else {
    apiConfig = getAPIConfig();
  }
  store.dispatch(loadPageSEOData({ page: `${SEO_DATA.store}/${storeStr}`, apiConfig }));
  if (isClient()) {
    store.dispatch(getCurrentStoreInfo({ storeID: fetchStoreIdFromUrlPath(storeStr) }));
  }
  return {
    pageData: {
      loadAnalyticsOnload: false,
    },
  };
};

StoreDetail.propTypes = {
  router: PropTypes.shape({}).isRequired,
  setResStatusNotFound: PropTypes.func.isRequired,
};
export default StoreDetail;
