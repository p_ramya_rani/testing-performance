// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { InternationalCheckoutVanilla } from '../organisms/InternationalCheckout';

describe('InternationalCheckoutVanilla', () => {
  const props = {
    iframeUrl: '',
    apiUrl: 'https://global.sbx.borderfree.com/cdn/checkout/v5/dist/merchant.js',
    communicationUrl: 'https://global.sbx.borderfree.com/checkoutapi/utils/empty.jsp',
  };

  it('should render InternationalCheckoutVanilla section', () => {
    const component = shallow(<InternationalCheckoutVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
