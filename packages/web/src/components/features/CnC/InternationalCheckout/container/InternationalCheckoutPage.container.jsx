// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CheckoutActions from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.action';
import selectors from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import InternationalCheckoutPage from '../views/InternationalCheckoutPage.view';

export class InternationalCheckoutPageContainer extends React.PureComponent {
  /**
   * @function componentDidMount
   * @description call international checkout settings on componentdidmount
   * @memberof InternationalCheckoutPageContainer
   */
  componentDidMount() {
    const { initIntlCheckout } = this.props;
    initIntlCheckout();
  }

  /**
   * @function render
   * @description render of international checkout view
   * @returns
   * @memberof InternationalCheckoutPageContainer
   */
  render() {
    const { apiUrl, communicationUrl, iframeUrl, isLoading } = this.props;
    return (
      <InternationalCheckoutPage
        apiUrl={apiUrl}
        communicationUrl={communicationUrl}
        iframeUrl={iframeUrl}
        isLoading={isLoading}
      />
    );
  }
}
/**
 *
 * @param {*} state
 * @function mapStateToProps
 * @description initial props to container
 */
export const mapStateToProps = state => {
  const {
    getInternationalCheckoutCommUrl,
    getInternationalCheckoutApiUrl,
    getInternationalCheckoutUrl,
  } = selectors;
  return {
    apiUrl: getInternationalCheckoutApiUrl(),
    communicationUrl: getInternationalCheckoutCommUrl(),
    iframeUrl: getInternationalCheckoutUrl(state),
    isLoading: false,
  };
};
/**
 *
 * @param {*} state
 * @function mapDispatchToProps
 * @description initial actions to container
 */
export const mapDispatchToProps = dispatch => {
  return {
    initIntlCheckout: () => {
      dispatch(CheckoutActions.initIntlCheckoutAction());
    },
  };
};

InternationalCheckoutPageContainer.propTypes = {
  initIntlCheckout: PropTypes.func,
  apiUrl: PropTypes.string,
  communicationUrl: PropTypes.string,
  iframeUrl: PropTypes.string,
  isLoading: PropTypes.bool,
};
InternationalCheckoutPageContainer.defaultProps = {
  initIntlCheckout: () => {},
  apiUrl: '',
  communicationUrl: '',
  iframeUrl: '',
  isLoading: false,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InternationalCheckoutPageContainer);
