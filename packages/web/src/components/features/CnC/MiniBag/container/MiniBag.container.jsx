// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes, { bool, shape, func } from 'prop-types';
import { connect } from 'react-redux';
import {
  getGrandTotal,
  getGiftCardsTotal,
} from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import {
  openMiniBag,
  closeMiniBag,
} from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import { openOverlayModal } from '@tcp/core/src/components/features/account/OverlayModal/container/OverlayModal.actions';
import BAG_PAGE_ACTIONS, {
  updateMiniBagFlag,
} from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import {
  getEnableFullPageAuth,
  getMinibagOnHoverEnabled,
} from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import { getCartItemCount } from '@tcp/core/src/utils/cookie.util';
import { isPlccUser } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getAddedToBagError } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import { getSaveForLaterSwitch } from '@tcp/core/src/components/features/CnC/SaveForLater/container/SaveForLater.selectors';
import PlaceCashSelector from '@tcp/core/src/components/features/CnC/PlaceCashBanner/container/PlaceCashBanner.selectors';
import {
  getIsChangeRecalForEcom,
  getIsNewMiniBag,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import BAGPAGE_SELECTORS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {
  getUpdateMiniBagFlag,
  getCartOrderList,
  getCartLoadingState,
} from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { isCanada, isMobileApp } from '@tcp/core/src/utils';
import MiniBagView from '../views/MiniBag.view';
import MiniBagViewNew from '../views/MiniBagNew.view';
import {
  getLabelsMiniBag,
  getTotalItemCount,
  getIsCartItemsUpdating,
  getIsCartItemsSFL,
  getCartItemsSflError,
  getMiniBagLoaderState,
  getAccessibilityLabels,
} from './MiniBag.selectors';
import {
  getCurrentPointsState,
  getTotalRewardsState,
  isRememberedUser,
  getUserLoggedInState,
} from '../../../../../../../core/src/components/features/account/User/container/User.selectors';
import BAG_ACTIONS from '../../../../../../../core/src/components/features/CnC/BagPage/container/BagPage.actions';

export class MiniBagContainer extends React.PureComponent {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    totalItems: PropTypes.number.isRequired,
    labels: PropTypes.shape({}).isRequired,
    userName: PropTypes.string.isRequired,
    subTotal: PropTypes.number.isRequired,
    currentPoints: PropTypes.number.isRequired,
    totalRewards: PropTypes.number.isRequired,
    isCartItemsUpdating: PropTypes.bool.isRequired,
    isCartItemSFL: PropTypes.bool.isRequired,
    cartItemSflError: PropTypes.string.isRequired,
    updateCartItemCount: PropTypes.func.isRequired,
    closeMiniBagDispatch: PropTypes.func.isRequired,
    openOverlay: PropTypes.func.isRequired,
    resetSuccessMessage: PropTypes.func.isRequired,
    isPlcc: PropTypes.bool.isRequired,
    addedToBagError: PropTypes.string.isRequired,
    isShowSaveForLaterSwitch: PropTypes.bool.isRequired,
    rememberedUserFlag: PropTypes.bool.isRequired,
    isUserLoggedIn: PropTypes.bool.isRequired,
    updateMiniBag: bool.isRequired,
    orderItems: shape([]).isRequired,
    isRecalculateTaxes: bool.isRequired,
    isECOM: bool.isRequired,
    miniBagLoaderState: PropTypes.bool.isRequired,
    getOrderDetailsDispatch: func.isRequired,
    setUpdateMiniBagFlag: func.isRequired,
    airmilesBannerContentId: PropTypes.shape({}).isRequired,
    fetchAirMilesModuleX: PropTypes.func.isRequired,
    showFullPageAuth: PropTypes.bool,
    minibagOnHoverEnabled: PropTypes.bool,
    isCartLoading: PropTypes.bool,
    isNewMiniBag: PropTypes.bool,
  };

  static defaultProps = {
    showFullPageAuth: false,
    minibagOnHoverEnabled: false,
    isCartLoading: false,
    isNewMiniBag: false,
  };

  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);
  }

  componentDidMount() {
    const {
      airmilesBannerContentId,
      fetchAirMilesModuleX,
      orderItems,
      updateMiniBag,
      isOpen,
      isRecalculateTaxes,
      isECOM,
      getOrderDetailsDispatch,
      setUpdateMiniBagFlag,
    } = this.props;
    if (isCanada()) {
      fetchAirMilesModuleX([airmilesBannerContentId]);
    }
    if (!isMobileApp() && isOpen && (!(orderItems && orderItems.size) || updateMiniBag)) {
      const isRecalc = isECOM && isRecalculateTaxes;
      getOrderDetailsDispatch(isRecalc);
      setUpdateMiniBagFlag(false);
    }
  }

  closeModal(e) {
    if (e) e.preventDefault();
    const { updateCartItemCount, closeMiniBagDispatch } = this.props;
    closeMiniBagDispatch();
    updateCartItemCount();
  }

  render() {
    const {
      labels,
      totalItems,
      isOpen,
      userName,
      subTotal,
      currentPoints,
      totalRewards,
      isCartItemsUpdating,
      isCartItemSFL,
      cartItemSflError,
      closeMiniBagDispatch,
      openOverlay,
      resetSuccessMessage,
      isPlcc,
      addedToBagError,
      isShowSaveForLaterSwitch,
      rememberedUserFlag,
      updateMiniBag,
      orderItems,
      isUserLoggedIn,
      miniBagLoaderState,
      showFullPageAuth,
      minibagOnHoverEnabled,
      isCartLoading,
      isNewMiniBag,
    } = this.props;

    return isNewMiniBag ? (
      <MiniBagViewNew
        openState={isOpen}
        onRequestClose={this.closeModal}
        labels={labels}
        totalItems={totalItems}
        userName={userName}
        subTotal={subTotal}
        currentPoints={currentPoints}
        totalRewards={totalRewards}
        isCartItemsUpdating={isCartItemsUpdating}
        isCartItemSFL={isCartItemSFL}
        cartItemSflError={cartItemSflError}
        closeMiniBagDispatch={closeMiniBagDispatch}
        openOverlay={openOverlay}
        resetSuccessMessage={resetSuccessMessage}
        isPlcc={isPlcc}
        addedToBagError={addedToBagError}
        isShowSaveForLaterSwitch={isShowSaveForLaterSwitch}
        isRememberedUser={rememberedUserFlag}
        updateMiniBag={updateMiniBag}
        orderItems={orderItems}
        isUserLoggedIn={isUserLoggedIn}
        miniBagLoaderState={miniBagLoaderState}
        showFullPageAuth={showFullPageAuth}
        minibagOnHoverEnabled
        isCartLoading={isCartLoading}
      />
    ) : (
      <MiniBagView
        openState={isOpen}
        onRequestClose={this.closeModal}
        labels={labels}
        totalItems={totalItems}
        userName={userName}
        subTotal={subTotal}
        currentPoints={currentPoints}
        totalRewards={totalRewards}
        isCartItemsUpdating={isCartItemsUpdating}
        isCartItemSFL={isCartItemSFL}
        cartItemSflError={cartItemSflError}
        closeMiniBagDispatch={closeMiniBagDispatch}
        openOverlay={openOverlay}
        resetSuccessMessage={resetSuccessMessage}
        isPlcc={isPlcc}
        addedToBagError={addedToBagError}
        isShowSaveForLaterSwitch={isShowSaveForLaterSwitch}
        isRememberedUser={rememberedUserFlag}
        updateMiniBag={updateMiniBag}
        orderItems={orderItems}
        isUserLoggedIn={isUserLoggedIn}
        miniBagLoaderState={miniBagLoaderState}
        showFullPageAuth={showFullPageAuth}
        minibagOnHoverEnabled={minibagOnHoverEnabled}
        isCartLoading={isCartLoading}
      />
    );
  }
}
const mapStateToProps = (state) => {
  return {
    labels: {
      ...getLabelsMiniBag(state),
      accessibility: getAccessibilityLabels(state),
    },
    totalItems: getTotalItemCount(state),
    subTotal: getGrandTotal(state) - getGiftCardsTotal(state),
    currentPoints: getCurrentPointsState(state),
    totalRewards: getTotalRewardsState(state),
    isCartItemsUpdating: getIsCartItemsUpdating(state),
    isCartItemSFL: getIsCartItemsSFL(state),
    cartItemSflError: getCartItemsSflError(state),
    isPlcc: isPlccUser(state),
    addedToBagError: getAddedToBagError(state),
    isShowSaveForLaterSwitch: getSaveForLaterSwitch(state),
    rememberedUserFlag: isRememberedUser(state),
    isUserLoggedIn: getUserLoggedInState(state),
    miniBagLoaderState: getMiniBagLoaderState(state),
    isRecalculateTaxes: getIsChangeRecalForEcom(state),
    isECOM: BAGPAGE_SELECTORS.getECOMStatus(state),
    updateMiniBag: getUpdateMiniBagFlag(state),
    orderItems: getCartOrderList(state),
    airmilesBannerContentId: PlaceCashSelector.getAirmilesBannerContentInfo(state),
    showFullPageAuth: getEnableFullPageAuth(state),
    minibagOnHoverEnabled: getMinibagOnHoverEnabled(state),
    isCartLoading: getCartLoadingState(state),
    isNewMiniBag: getIsNewMiniBag(state),
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    openMiniBagDispatch: () => {
      if (getCartItemCount() > 0) {
        dispatch(BAG_PAGE_ACTIONS.getOrderDetails());
      }
      dispatch(openMiniBag());
    },
    closeMiniBagDispatch: () => {
      dispatch(closeMiniBag());
    },
    getOrderDetailsDispatch: (isRecalc) => {
      if (getCartItemCount() > 0) {
        dispatch(BAG_PAGE_ACTIONS.getOrderDetails({ isCartPage: true, isRecalc }));
      }
    },
    setUpdateMiniBagFlag: (payload) => dispatch(updateMiniBagFlag(payload)),
    openOverlay: (component) => dispatch(openOverlayModal(component)),
    resetSuccessMessage: (payload) => {
      dispatch(BAG_ACTIONS.setCartItemsSFL(payload));
    },
    fetchAirMilesModuleX: (contentIds) => {
      dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MiniBagContainer);
