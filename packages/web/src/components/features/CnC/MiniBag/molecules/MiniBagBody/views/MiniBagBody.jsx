// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Row from '@tcp/core/src/components/common/atoms/Row';
import { getCartItemCount } from '@tcp/core/src/utils/cookie.util';
import Col from '@tcp/core/src/components/common/atoms/Col';
import { Image } from '@tcp/core/src/components/common/atoms';
import { getIconPath, routerPush, scrollToElement } from '@tcp/core/src/utils';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ProductTileWrapper from '@tcp/core/src/components/features/CnC/CartItemTile/organisms/ProductTileWrapper/container/ProductTileWrapper.container';
import AirmilesBanner from '@tcp/core/src/components/features/CnC/common/organism/AirmilesBanner';
import AddedToBagActions from '@tcp/core/src/components/features/CnC/AddedToBagActions';
import { CHECKOUT_ROUTES } from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import LoyaltyBanner from '@tcp/core/src/components/features/CnC/LoyaltyBanner';
import InformationHeader from '@tcp/core/src/components/features/CnC/common/molecules/InformationHeader';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import ErrorMessage from '../../../../../../../../../core/src/components/features/CnC/common/molecules/ErrorMessage';
import styles from '../styles/MiniBagBody.style';
import EmptyMiniBag from '../../EmptyMiniBag/views/EmptyMiniBag';

class MiniBagBody extends React.PureComponent {
  isEditing = false;

  constructor(props) {
    super(props);
    this.state = {
      headerError: false,
      isShowServerError: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { addedToBagError } = this.props;
    const { addedToBagError: prevAddedToBagError } = prevProps;
    const { isShowServerError } = this.state;
    if (!isShowServerError && addedToBagError !== prevAddedToBagError) this.setisShowServerError();
  }

  componentWillUnmount() {
    const { resetSuccessMessage } = this.props;
    resetSuccessMessage(false);
  }

  setisShowServerError = () => {
    this.setState({ isShowServerError: true });
  };

  setHeaderErrorState = (state, ...params) => {
    this.setState({ headerError: true, params });
  };

  handleItemEdit = (value) => {
    this.isEditing = value;
  };

  ViewSaveForLaterLink = (savedforLaterQty, isShowSaveForLaterSwitch) => {
    const { labels, closeMiniBag, router } = this.props;
    if (!isShowSaveForLaterSwitch || savedforLaterQty <= 0) {
      return null;
    }
    return (
      <Anchor
        fontSizeVariation="medium"
        noLink
        underline
        anchorVariation="primary"
        data-locator="cartitem-saveforlater"
        className="elem-ml-MED"
        onClick={(e) => {
          const pathName = router.pathname;
          const timestamp = Date.now();
          e.preventDefault();
          if (pathName && pathName.toLowerCase() !== CHECKOUT_ROUTES.bagPage.asPath) {
            routerPush(
              `${CHECKOUT_ROUTES.bagPage.to}?isSfl=${timestamp}`,
              `${CHECKOUT_ROUTES.bagPage.to}`
            );
          } else {
            routerPush(`${CHECKOUT_ROUTES.bagPage.to}`, `${CHECKOUT_ROUTES.bagPage.to}`);
            const headingElem = document.querySelector('.save-for-later-section-heading');
            const timer = setTimeout(() => {
              scrollToElement(headingElem);
              clearTimeout(timer);
            }, 250);
          }
          closeMiniBag();
        }}
      >
        {`${labels.viewSfl} (${savedforLaterQty})`}
      </Anchor>
    );
  };

  renderCartItemSflSuceessMessage = () => {
    const { isCartItemSFL, labels } = this.props;
    if (isCartItemSFL) {
      return (
        <Row className="mainWrapper">
          <Col className="deleteMsg" colSize={{ small: 6, medium: 8, large: 12 }}>
            <Image
              alt={labels.tickIcon}
              className="tick-icon-image"
              src={getIconPath('active_icon')}
              height={12}
              width={12}
            />
            <BodyCopy
              component="span"
              fontSize="fs12"
              textAlign="center"
              fontFamily="secondary"
              fontWeight="extrabold"
            >
              {labels.sflSuccess}
            </BodyCopy>
          </Col>
        </Row>
      );
    }
    return null;
  };

  renderGiftCardError = () => {
    const { cartItemSflError } = this.props;
    if (cartItemSflError) {
      return (
        <Row className="mainWrapper">
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <ErrorMessage
              className="error_box"
              error={cartItemSflError}
              fontSize="fs12"
              fontWeight="extrabold"
            />
          </Col>
        </Row>
      );
    }
    return null;
  };

  renderLoyaltyBanner = (newMiniBag) => {
    if (getCartItemCount() > 0) {
      return <LoyaltyBanner pageCategory="miniBag" newMiniBag={newMiniBag} isCartEmpty={false} />;
    }
    return <LoyaltyBanner pageCategory="miniBag" newMiniBag={newMiniBag} isCartEmpty />;
  };

  getHeaderError = ({
    labels,
    orderItems,
    pageView,
    isUnavailable,
    isSoldOut,
    getUnavailableOOSItems,
    confirmRemoveCartItem,
    isBagPageSflSection,
    isCartItemSFL,
    isCartItemsUpdating,
    isSflItemRemoved,
  }) => {
    return (
      <div className="information-header elem-pl-MED">
        <InformationHeader
          labels={labels}
          orderItems={orderItems}
          pageView={pageView}
          isUnavailable={isUnavailable}
          isSoldOut={isSoldOut}
          getUnavailableOOSItems={getUnavailableOOSItems}
          confirmRemoveCartItem={confirmRemoveCartItem}
          isBagPageSflSection={isBagPageSflSection}
          isCartItemSFL={isCartItemSFL}
          isCartItemsUpdating={isCartItemsUpdating}
          isSflItemRemoved={isSflItemRemoved}
        />
      </div>
    );
  };

  renderServerError = () => {
    const { addedToBagError } = this.props;
    if (!addedToBagError) {
      return null;
    }
    return (
      <Row className="mainWrapper">
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <ErrorMessage
            error={addedToBagError}
            className="error_box minibag-error"
            fontSize="fs12"
            fontWeight="extrabold"
          />
        </Col>
      </Row>
    );
  };

  handleViewBag = (e) => {
    e.preventDefault();
    const { closeMiniBag } = this.props;
    const fromMiniBag = true;
    closeMiniBag();
    routerPush(
      `${CHECKOUT_ROUTES.bagPage.to}?fromMiniBag=${fromMiniBag}`,
      `${CHECKOUT_ROUTES.bagPage.to}`
    );
  };

  closeMiniBag = (e) => {
    if (e) e.preventDefault();
    const { closeMiniBagDispatch } = this.props;
    closeMiniBagDispatch();
  };

  // To make subTotal quickly zero when last item deleted
  getSubTotal = (cartItemCount, isCartLoading, subTotal) => {
    return cartItemCount === 1 && isCartLoading ? 0 : subTotal;
  };

  render() {
    const {
      labels,
      className,
      userName,
      cartItemCount,
      savedforLaterQty,
      subTotal,
      closeMiniBag,
      onLinkClick,
      isShowSaveForLaterSwitch,
      isRememberedUser,
      isUserLoggedIn,
      isMiniBag,
      newMiniBag,
      isCartLoading,
    } = this.props;
    const { headerError, params, isShowServerError } = this.state;
    return (
      <div className={`${className} ${newMiniBag ? 'newMiniBagWrapper' : ''}`}>
        <div className="minibag-viewbag">
          <Row className="mainWrapper">
            {!newMiniBag && (
              <Col className="subHeaderText" colSize={{ small: 6, medium: 8, large: 12 }}>
                {userName ? (
                  <BodyCopy component="span" fontSize="fs12" textAlign="left">
                    <Anchor
                      fontSizeVariation="medium"
                      underline
                      anchorVariation="primary"
                      noLink
                      dataLocator="addressbook-makedefault"
                      onClick={(e) => this.handleViewBag(e)}
                    >
                      {`${labels.viewBag} (${cartItemCount})`}
                    </Anchor>
                    {this.ViewSaveForLaterLink(savedforLaterQty, isShowSaveForLaterSwitch)}
                  </BodyCopy>
                ) : (
                  <BodyCopy component="span" fontSize="fs12" textAlign="left">
                    <Anchor
                      fontSizeVariation="medium"
                      underline
                      anchorVariation="primary"
                      noLink
                      data-locator="addressbook-makedefault"
                      onClick={(e) => this.handleViewBag(e)}
                    >
                      {`${labels.viewBag} (${cartItemCount})`}
                    </Anchor>
                    {this.ViewSaveForLaterLink(savedforLaterQty, isShowSaveForLaterSwitch)}
                  </BodyCopy>
                )}
              </Col>
            )}
            {headerError && this.getHeaderError(cartItemCount ? params[0] : this.props)}
            {!isShowServerError ? this.renderGiftCardError() : null}
          </Row>
        </div>
        {isShowServerError ? this.renderServerError() : null}
        <BodyCopy
          component="div"
          className={`viewBagAndProduct ${params?.[0]?.isSoldOut ? 'minibag-Ooos' : ''}`}
        >
          {cartItemCount ? (
            <ProductTileWrapper
              sflItemsCount={savedforLaterQty}
              onItemEdit={this.handleItemEdit}
              setHeaderErrorState={this.setHeaderErrorState}
              isMiniBag={isMiniBag}
              closeMiniBag={closeMiniBag}
              newMiniBag={newMiniBag}
            />
          ) : (
            <EmptyMiniBag
              labels={labels}
              userName={userName}
              closeMiniBag={closeMiniBag}
              onLinkClick={onLinkClick}
              isRememberedUser={isRememberedUser}
              isUserLoggedIn={isUserLoggedIn}
              newMiniBag={newMiniBag}
            />
          )}
        </BodyCopy>
        {cartItemCount ? (
          <React.Fragment>
            <div className="miniBagFooter">
              {newMiniBag && (
                <div className="miniBagFooter-content">
                  <BodyCopy
                    component="div"
                    fontSize="fs12"
                    textAlign="left"
                    className="elem-pt-MED"
                    fontWeight="bold"
                  >
                    <Anchor
                      fontSizeVariation="medium"
                      underline
                      anchorVariation="primary"
                      noLink
                      data-locator="addressbook-makedefault"
                      onClick={(e) => this.handleViewBag(e)}
                    >
                      {labels.viewShoppingBag}
                    </Anchor>
                  </BodyCopy>
                  <BodyCopy
                    component="div"
                    fontSize="fs14"
                    fontWeight="bold"
                    textAlign="right"
                    className="subTotal"
                    fontFamily="secondary"
                  >
                    {`${labels.subTotal}:  `}
                    <BodyCopy
                      component="span"
                      fontSize="fs14"
                      fontWeight="extrabold"
                      fontFamily="secondary"
                    >
                      <PriceCurrency
                        price={this.getSubTotal(cartItemCount, isCartLoading, subTotal)}
                      />
                    </BodyCopy>
                  </BodyCopy>
                </div>
              )}
              {!newMiniBag && (
                <BodyCopy
                  tag="span"
                  fontSize="fs14"
                  fontWeight="semibold"
                  textAlign="center"
                  className="subTotal"
                >
                  {`${labels.subTotal}: `}
                  <PriceCurrency price={this.getSubTotal(cartItemCount, isCartLoading, subTotal)} />
                </BodyCopy>
              )}
              <AfterPayMessaging
                isOrderThresholdEnabled
                offerPrice={subTotal}
                disableForGC
                centered
              />
              <AddedToBagActions
                containerId="paypal-button-container-minibag"
                showAddTobag={false}
                isEditingItem={this.isEditing}
                closeMiniBag={closeMiniBag}
                showVenmo={false} // No Venmo CTA on Minibag, as per venmo requirement
                isMiniBag={isMiniBag}
                newMiniBag={newMiniBag}
              />
              <AirmilesBanner isMiniBag={isMiniBag} />
            </div>
          </React.Fragment>
        ) : (
          <div className="miniBagFooter">
            <BodyCopy
              tag="span"
              fontSize="fs14"
              fontWeight="semibold"
              className="subTotalEmpty"
              fontFamily="secondary"
            >
              {`${labels.subTotal}:`}
              <PriceCurrency price={0.0} />
            </BodyCopy>
          </div>
        )}
        {this.renderLoyaltyBanner(newMiniBag)}
      </div>
    );
  }
}

MiniBagBody.defaultProps = {
  isMiniBag: true,
  newMiniBag: false,
};

MiniBagBody.propTypes = {
  className: PropTypes.string.isRequired,
  userName: PropTypes.string.isRequired,
  isCartItemsUpdating: PropTypes.shape({}).isRequired,
  labels: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  subTotal: PropTypes.number.isRequired,
  cartItemCount: PropTypes.number.isRequired,
  savedforLaterQty: PropTypes.number.isRequired,
  isCartItemSFL: PropTypes.bool.isRequired,
  cartItemSflError: PropTypes.string.isRequired,
  closeMiniBag: PropTypes.func.isRequired,
  onLinkClick: PropTypes.func.isRequired,
  resetSuccessMessage: PropTypes.func.isRequired,
  addedToBagError: PropTypes.string.isRequired,
  isShowSaveForLaterSwitch: PropTypes.bool.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  isRememberedUser: PropTypes.bool.isRequired,
  router: PropTypes.shape({ pathname: PropTypes.string }).isRequired,
  isMiniBag: PropTypes.bool,
  closeMiniBagDispatch: PropTypes.func.isRequired,
  newMiniBag: PropTypes.bool,
  isCartLoading: PropTypes.bool.isRequired,
};

export default withStyles(MiniBagBody, styles);
export { MiniBagBody as MiniBagBodyVanilla };
