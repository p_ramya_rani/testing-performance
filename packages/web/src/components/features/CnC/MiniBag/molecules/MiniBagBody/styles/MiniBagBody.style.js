// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  &.newMiniBagWrapper {
    min-height: 120px;
    max-height: calc(100vh - 30px);
  }

  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  ${(props) => (props.newMiniBag ? '' : 'height: calc(100vh - 69px);')}

  .viewBagAndProduct {
    height: calc(100% - 120px);
    overflow-y: auto;
    flex-grow: 1;
    min-height: 150px;
    .tile-header {
      padding-right: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.SM};
      ${(props) =>
        props.newMiniBag
          ? `background-color: ${props.theme.colors.WHITE};
          border-radius: 6px;
          margin: ${props.theme.spacing.ELEM_SPACING.XXS} ${props.theme.spacing.ELEM_SPACING.SM}
          ${props.theme.spacing.ELEM_SPACING.XXS}
          ${props.theme.spacing.ELEM_SPACING.SM};`
          : ''}
    }
    .align-product-img {
      margin-right: ${(props) => props.newMiniBag && '0px'};
    }
  }
  .viewBagAndProduct.minibag-Ooos {
    min-height: 200px;
  }

  .tile-header:last-child {
    border-bottom: 0;
  }
  .mainWrapper {
    text-align: center;
    background-color: ${(props) => props.theme.colorPalette.gray['300']};
    height: auto;
    padding-right: 0;
    padding-left: 0;
    margin: 0;
    width: 100%;

    .delete-msg {
      border: solid 2px ${(props) => props.theme.colors.NOTIFICATION.SUCCESS};
      text-align: left;
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      background: ${(props) => props.theme.colors.WHITE};
      display: flex;
      align-items: center;
      margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      margin-top: 0;
      .tick-icon {
        height: 23px;
        width: 23px;
        margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      }
    }

    .information-header {
      width: 100%;
    }
  }

  .minibag-viewbag {
    flex-grow: 0;
  }

  .deleteMsg {
    border: solid 2px ${(props) => props.theme.colors.NOTIFICATION.SUCCESS};
    text-align: left;
    padding: 10px;
    background: ${(props) => props.theme.colors.WHITE};
    display: flex;
    align-items: center;
    margin: 12px;
    margin-top: 0px;
    .tick-icon-image {
      position: relative;
      top: 1px;
      right: 9px;
      height: 23px;
      width: 23px;
      padding-left: 10px;
    }
  }

  .subHeaderText {
    text-decoration: underline;
    vertical-align: middle;
    line-height: 42px;
  }
  .miniBagFooter {
    text-align: center;
    background-color: ${(props) =>
      props.newMiniBag ? props.theme.colors.WHITE : props.theme.colorPalette.gray['300']};
    padding-right: 0;
    padding-left: 0;
    border-bottom: ${(props) => props.theme.colorPalette.gray['600']};
    bottom: 0px;
    margin-bottom: 0px;
    height: auto;
    flex-grow: 0;

    ${(props) =>
      props.newMiniBag
        ? `box-shadow: 0 -3px 6px 0 rgba(163, 162, 162, 0.3); z-index: 99;
        margin-bottom: 0px;
        div:nth-child(2) {
        margin-bottom: -2px;
        }
        `
        : ''}
  }
  .subTotal {
    text-align: center;
    padding: ${(props) => (props.newMiniBag ? `13px 15px 13px 0` : '13px 0 13px 0')};
  }
  .checkout-button {
    padding-top: 10px;
  }

  .checkout {
    height: 48px;
    flex: 1;

    background-color: ${(props) => props.theme.colorPalette.blue.C900};
    &:hover {
      background: ${(props) => props.theme.colorPalette.blue[800]};
    }
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-left: 0;
      width: 220px;
    }
  }
  .payPal-button {
    width: 220px;
  }
  .subTotalEmpty {
    text-align: center;
    padding: 13px 0 13px 0;
    bottom: 0px;
    margin-bottom: 0px;
  }
  .miniBagFooter-content {
    display: flex;
    justify-content: space-between;
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0
      ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }
`;
