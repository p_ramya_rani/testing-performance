// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import MiniBagSelectBox from '../MiniBagSelectBox';

describe('MiniBagSelectBox component', () => {
  it('MiniBagSelectBox component renders correctly', () => {
    const props = {
      input: {
        name: '',
      },
    };
    const component = shallow(<MiniBagSelectBox {...props} />);
    expect(component).toMatchSnapshot();
  });
});
