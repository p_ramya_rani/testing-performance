// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { isCanada } from '@tcp/core/src/utils';
import ProductTileWrapper from '@tcp/core/src/components/features/CnC/CartItemTile/organisms/ProductTileWrapper/container/ProductTileWrapper.container';
import LoyaltyBanner from '@tcp/core/src/components/features/CnC/LoyaltyBanner';
import InformationHeader from '@tcp/core/src/components/features/CnC/common/molecules/InformationHeader';
import { MiniBagBodyVanilla } from '../views/MiniBagBody';
import EmptyMiniBag from '../../EmptyMiniBag/views/EmptyMiniBag';

jest.mock('@tcp/core/src/utils', () => ({
  ...jest.requireActual('@tcp/core/src/utils'),
  isCanada: jest.fn(),
  routerPush: jest.fn(),
}));

describe('MiniBagBody component', () => {
  const props = {
    labels: {
      viewBag: 'View bag',
      subTotal: 'Subtotal',
      currencySymbol: '$',
    },
    userName: 'Christine',
    cartItemCount: 1,
    subTotal: 112,
    isCartItemsUpdating: { isDeleting: true, isUpdating: false },
  };
  it('isCanada true for Loyalty Banner', () => {
    isCanada.mockImplementation(() => true);
    const component = shallow(<MiniBagBodyVanilla {...props} />);
    expect(component).toMatchSnapshot();
    expect(component.find(LoyaltyBanner).length).toEqual(1);
  });
  it('isCanada false for Loyalty Banner', () => {
    isCanada.mockImplementation(() => false);
    const component = shallow(<MiniBagBodyVanilla {...props} />);
    expect(component).toMatchSnapshot();
    expect(component.find(LoyaltyBanner).length).toEqual(1);
  });
  it('renders main component correctly', () => {
    const component = shallow(<MiniBagBodyVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('renders EmptyMiniBag correctly', () => {
    const props1 = {
      ...props,
      labels: {
        viewBag: 'View bag empty',
        viewSaveForLater: 'later',
        subTotal: 'Sub',
        currencySymbol: '$',
      },
    };
    const tree = shallow(<MiniBagBodyVanilla {...props1} />);
    expect(tree.find(ProductTileWrapper)).toBeTruthy();
  });
  it('renders correctly', () => {
    const props1 = {
      labels: {
        viewBag: 'View bag empty',
        viewSaveForLater: 'later',
        subTotal: 'Sub',
        currencySymbol: 'USD',
      },
      userName: 'User1',
      cartItemCount: null,
      subTotal: 23,
      isCartItemsUpdating: {},
      addedToBagError: 'something went wrong',
    };
    const tree = shallow(<MiniBagBodyVanilla {...props1} />);
    expect(tree.find(EmptyMiniBag)).toBeTruthy();
  });
  it('handleViewBag called', () => {
    const props1 = {
      ...props,
      isCartItemsUpdating: {},
      addedToBagError: 'something',
      closeMiniBag: jest.fn(),
    };
    const e = {
      preventDefault: jest.fn(),
    };
    const component = shallow(<MiniBagBodyVanilla {...props1} />);
    component.instance().handleViewBag(e);
  });
  it('InformationHeader renders correctly', () => {
    const props1 = {
      ...props,
      newMiniBag: '',
      isCartItemsUpdating: {},
      addedToBagError: 'something',
    };

    const component = shallow(<MiniBagBodyVanilla {...props1} />);
    expect(component.find(InformationHeader)).toBeTruthy();
  });
  it('closeMiniBag called', () => {
    const props1 = {
      ...props,
      isCartItemsUpdating: {},
      addedToBagError: 'something',
      isCartItemSFL: false,
      closeMiniBag: jest.fn(),
      closeMiniBagDispatch: jest.fn(),
    };
    const component = shallow(<MiniBagBodyVanilla {...props1} />);
    component.instance().closeMiniBag();
  });
  it('isCartItemSFL false for renderCartItemSflSuceessMessage ', () => {
    const props1 = {
      ...props,
      userName: null,
      addedToBagError: 'something',
      isCartItemSFL: false,
      newMiniBag: null,
    };

    const component = shallow(<MiniBagBodyVanilla {...props1} />);
    component.instance().renderCartItemSflSuceessMessage();
  });
  it('isCartItemSFL true for renderCartItemSflSuceessMessage', () => {
    const props1 = {
      ...props,
      addedToBagError: 'something',
      isCartItemSFL: true,
      newMiniBag: null,
      cartItemSflError: '',
      headerError: true,
    };

    const component = shallow(<MiniBagBodyVanilla {...props1} />);
    component.instance().renderCartItemSflSuceessMessage();
  });
  it('componentDidUpdate to be called', () => {
    const props1 = {
      ...props,
      userName: null,
      isCartItemsUpdating: {},
      addedToBagError: 'something',
      isCartItemSFL: false,
      newMiniBag: null,
    };
    const props2 = {
      addedToBagError: '',
    };
    const component = shallow(<MiniBagBodyVanilla {...props1} />);
    component.instance().componentDidUpdate(props2);
    expect(component).toBeDefined();
  });

  it('componentWillUnmount called', () => {
    const props1 = {
      ...props,
      isCartItemSFL: false,
      newMiniBag: null,
      resetSuccessMessage: jest.fn(),
    };
    const component = shallow(<MiniBagBodyVanilla {...props1} />);
    const instance = component.instance();
    instance.componentWillUnmount();
  });
  it('getheader error called', () => {
    const args = {
      labels: {},
      orderItems: '',
      pageView: '',
      isUnavailable: false,
      isSoldOut: false,
      getUnavailableOOSItems: jest.fn(),
      confirmRemoveCartItem: '',
      isBagPageSflSection: '',
      isCartItemSFL: '',
      isCartItemsUpdating: '',
      isSflItemRemoved: '',
    };
    const component = shallow(<MiniBagBodyVanilla {...props} />);
    component.instance().getHeaderError(args);
  });
});
