// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { bool, number, shape, string } from 'prop-types';
import { BodyCopy, SelectBox } from '@tcp/core/src/components/common/atoms';
import SizeChart from '@tcp/core/src/components/common/molecules/ProductAddToBag/molecules/SizeChart/container';
import SIZE_CHART_LINK_POSITIONS from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import QuantitySelection from '@tcp/core/src/components/common/molecules/QuantitySelection/QuantitySelection';
import selectBoxStyle from './MiniBagSelectBox.style';

const MiniBagSelect = (props) => {
  const {
    input: { name, ariaLabel },
    className,
    id,
    placeholder,
    input,
    options,
    keepAlive,
    dataLocator,
    disabled,
    title,
    newQVDesign,
    sizeChartLinkVisibility,
    sizeChartDetails,
    selectedFit,
    categoryPath2Map,
    quickViewLabels,
    labels,
    isNewPDPEnabled,
    isQuantitySection,
    fromBagPage,
  } = props;

  return (
    <React.Fragment>
      {!isNewPDPEnabled && (
        <BodyCopy
          fontSize="fs10"
          fontFamily="secondary"
          fontWeight="extrabold"
          className={`pdp-qty ${newQVDesign ? 'quick-view-drawer-redesign' : ''}`}
        >
          <span>{`${name}${newQVDesign ? '' : ':'}`}</span>
        </BodyCopy>
      )}
      {newQVDesign ? (
        <div className="quantity-size-chart">
          <BodyCopy fontFamily="secondary">
            <QuantitySelection
              id={id}
              placeholder={placeholder}
              input={input}
              options={options}
              dataLocator={dataLocator}
              disabled={disabled}
              title={title}
              className={className}
              inheritedStyles={selectBoxStyle}
              isNewPDPEnabled={isNewPDPEnabled}
              fromBagPage={fromBagPage}
              {...props}
            />
          </BodyCopy>
          {sizeChartLinkVisibility === SIZE_CHART_LINK_POSITIONS.AFTER_SIZE && (
            <SizeChart
              sizeChartDetails={sizeChartDetails}
              labels={labels}
              selectedFit={selectedFit}
              categoryPath2Map={categoryPath2Map}
              newQVDesign={newQVDesign}
              quickViewLabels={quickViewLabels}
              isNewPDPEnabled={isNewPDPEnabled}
              isQuantitySection={isQuantitySection}
            />
          )}
        </div>
      ) : (
        <BodyCopy fontSize="fs12" fontFamily="secondary">
          <SelectBox
            {...props}
            ariaLabel={ariaLabel || name}
            inheritedStyles={selectBoxStyle}
            disabled={keepAlive}
          />
        </BodyCopy>
      )}
    </React.Fragment>
  );
};

MiniBagSelect.propTypes = {
  keepAlive: bool,
  id: string,
  className: string,
  ariaLabel: string,
  name: string,
  type: string,
  placeholder: string,
  meta: shape({}).isRequired,
  input: shape({
    name: string,
    ariaLabel: string,
  }).isRequired,
  options: shape([]).isRequired,
  dataLocator: string,
  disabled: bool,
  title: string,
  preText: string,
  forceSelect: bool,
  newQVDesign: bool,
  sizeChartLinkVisibility: number.isRequired,
  sizeChartDetails: string.isRequired,
  selectedFit: shape({}).isRequired,
  categoryPath2Map: shape([]).isRequired,
  quickViewLabels: shape({}),
  labels: shape({}),
  isQuantitySection: bool,
  isNewPDPEnabled: bool,
};

MiniBagSelect.defaultProps = {
  keepAlive: false,
  id: '',
  ariaLabel: '',
  name: '',
  type: 'text',
  placeholder: '',
  dataLocator: '',
  disabled: false,
  title: '',
  className: '',
  preText: '',
  forceSelect: '',
  newQVDesign: false,
  quickViewLabels: {},
  labels: {},
  isQuantitySection: false,
  isNewPDPEnabled: false,
};

export default MiniBagSelect;
