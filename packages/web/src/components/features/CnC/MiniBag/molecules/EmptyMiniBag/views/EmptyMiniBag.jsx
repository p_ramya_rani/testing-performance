// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLoginPayload, getCreateAccountPayload } from '@tcp/core/src/constants/analytics';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import Button from '@tcp/core/src/components/common/atoms/Button';
import { routerPush } from '../../../../../../../../../core/src/utils';
import styles from '../styles/EmptyMiniBag.style';

const MiniBagHeader = ({
  labels,
  className,
  userName,
  onLinkClick,
  closeMiniBag,
  isRememberedUser,
  isUserLoggedIn,
}) => {
  const createAccount = 'createAccount';
  const login = 'login';
  return (
    <div className={className}>
      <div className="continue-shopping">
        <BodyCopy
          component="span"
          fontSize="fs18"
          fontWeight="extrabold"
          textAlign="left"
          fontFamily="secondary"
          color="gray.800"
        >
          {labels.yourShoppingBag}
        </BodyCopy>
      </div>
      <div className="continue-shopping">
        <Anchor
          className="continueShoppingText"
          underline
          anchorVariation="primary"
          onClick={(e) => {
            e.preventDefault();
            closeMiniBag();
            routerPush('/home', '/home');
          }}
          dataLocator="emptyMiniBag-continueShopping"
        >
          <BodyCopy component="span" fontSize="fs15" fontFamily="secondary">
            {labels.continueShopping}
          </BodyCopy>
        </Anchor>
      </div>
      {!userName || !isUserLoggedIn ? (
        <>
          <div className="continue-shopping">
            <ClickTracker clickData={getLoginPayload('minibag')}>
              <Button className="logIn" onClick={(e) => onLinkClick({ e, componentId: login })}>
                <BodyCopy
                  component="span"
                  color="white"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  fontSize="fs14"
                >
                  {labels.logIn}
                </BodyCopy>
              </Button>
            </ClickTracker>
          </div>
          {!isRememberedUser && (
            <>
              <div className="createAccountWrapper">
                <BodyCopy className="accountText" component="p" fontSize="fs14" textAlign="left">
                  {labels.dontHaveAccount}
                </BodyCopy>
                <BodyCopy className="accountText" component="p" fontSize="fs14" textAlign="left">
                  {labels.createOne}
                </BodyCopy>
              </div>
              <div className="continue-shopping">
                <ClickTracker clickData={getCreateAccountPayload('minibag')}>
                  <Button
                    className="createAccount"
                    onClick={(e) => onLinkClick({ e, componentId: createAccount })}
                  >
                    <BodyCopy
                      component="span"
                      color="white"
                      fontWeight="extrabold"
                      fontFamily="secondary"
                      fontSize="fs14"
                    >
                      {labels.createAccount}
                    </BodyCopy>
                  </Button>
                </ClickTracker>
              </div>
            </>
          )}
        </>
      ) : (
        ``
      )}
    </div>
  );
};

MiniBagHeader.propTypes = {
  labels: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  className: PropTypes.string,
  userName: PropTypes.string,
  onLinkClick: PropTypes.func,
  closeMiniBag: PropTypes.func,
  isRememberedUser: PropTypes.bool,
  isUserLoggedIn: PropTypes.bool,
};

MiniBagHeader.defaultProps = {
  className: '',
  userName: '',
  onLinkClick: () => {},
  closeMiniBag: () => {},
  isRememberedUser: false,
  isUserLoggedIn: false,
};

export default withStyles(MiniBagHeader, styles);
export { MiniBagHeader as MiniBagHeaderVanilla };
