// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { MiniBagVanilla } from '../views/MiniBag.view';

describe('MiniBag template', () => {
  const closeMiniBagDispatchMocked = jest.fn();
  it('should render correctly', () => {
    const props = {
      className: '',
      closeMiniBagDispatch: jest.fn(),
      openState: true,
      labels: {
        accessibility: {},
      },
      router: {},
      userName: '',
      subTotal: '',
      currencySymbol: '',
      currentPoints: '',
      totalRewards: '',
      isCartItemsUpdating: true,
    };
    const tree = shallow(<MiniBagVanilla {...props} />);
    tree.setProps({
      router: {
        asPath: 'Pickup',
      },
    });
    expect(tree).toMatchSnapshot();
    expect(tree.find('Styled(Modal)')).toHaveLength(1);
  });

  it('should call closeMiniBag when minibagOnHoverEnabled is true', () => {
    const props = {
      className: '',
      openState: true,
      labels: {
        accessibility: {},
      },
      router: {},
      userName: '',
      subTotal: '',
      currencySymbol: '',
      currentPoints: '',
      totalRewards: '',
      isCartItemsUpdating: true,
      minibagOnHoverEnabled: true,
      closeMiniBagDispatch: closeMiniBagDispatchMocked,
    };
    const tree = shallow(<MiniBagVanilla {...props} />);
    const componentInstance = tree.instance();
    componentInstance.onMouseLeave({ preventDefault: () => {} });
    setTimeout(() => {
      expect(closeMiniBagDispatchMocked).toBeCalled(true);
    }, 10);
  });

  it('should not call closeMiniBag when minibagOnHoverEnabled is false', () => {
    const props = {
      className: '',
      openState: true,
      labels: {
        accessibility: {},
      },
      router: {},
      userName: '',
      subTotal: '',
      currencySymbol: '',
      currentPoints: '',
      totalRewards: '',
      isCartItemsUpdating: true,
      minibagOnHoverEnabled: false,
      closeMiniBagDispatch: closeMiniBagDispatchMocked,
    };
    const tree = shallow(<MiniBagVanilla {...props} />);
    const componentInstance = tree.instance();
    componentInstance.onMouseLeave({ preventDefault: () => {} });
    setTimeout(() => {
      expect(closeMiniBagDispatchMocked).not.toBeCalled(true);
    }, 10);
  });
});
