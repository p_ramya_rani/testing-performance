// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { MiniBagContainer } from '../container/MiniBag.container';

describe('MiniBagContainer', () => {
  it('should render correctly', () => {
    const props = {
      updateCartItemCount: jest.fn(),
      closeMiniBagDispatch: jest.fn(),
      fetchAirMilesModuleX: jest.fn(),
    };
    const e = {
      preventDefault: jest.fn(),
    };
    const tree = shallow(<MiniBagContainer {...props} />);
    expect(tree).toMatchSnapshot();
    tree.instance().closeModal(e);
    expect(props.updateCartItemCount).toBeCalled();
  });
  it('new minibag should render correctly', () => {
    const props = {
      updateCartItemCount: jest.fn(),
      closeMiniBagDispatch: jest.fn(),
      fetchAirMilesModuleX: jest.fn(),
      isNewMiniBag: true,
    };
    const e = {
      preventDefault: jest.fn(),
    };
    const tree = shallow(<MiniBagContainer {...props} />);
    expect(tree).toMatchSnapshot();
    tree.instance().closeModal(e);
    expect(props.updateCartItemCount).toBeCalled();
  });
});
