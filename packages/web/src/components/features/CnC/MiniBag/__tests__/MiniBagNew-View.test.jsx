// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { MiniBagVanilla } from '../views/MiniBagNew.view';

describe('MiniBagNew template', () => {
  it('should render correctly', () => {
    const props = {
      className: '',
      closeMiniBagDispatch: jest.fn(),
      openState: true,
      labels: {
        accessibility: {},
      },
      router: {},
      userName: '',
      subTotal: '',
      currencySymbol: '',
      currentPoints: '',
      totalRewards: '',
      isCartItemsUpdating: true,
    };
    const tree = shallow(<MiniBagVanilla {...props} />);
    tree.setProps({
      router: {
        asPath: 'Pickup',
      },
    });
    expect(tree).toMatchSnapshot();
  });
});
