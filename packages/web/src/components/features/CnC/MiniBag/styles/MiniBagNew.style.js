// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  max-width: 1440px;
  margin: 0 auto;
  position: relative;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @keyframes fadeOut {
    0% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }

  .fade-in {
    animation: fadeIn ease 0.6s;
  }

  .fade-out {
    animation: fadeOut ease 0.6s;
  }

  .minibag-overlay {
    width: 450px;
    position: absolute;
    background: ${(props) => props.theme.colorPalette.gray[300]};
    border-radius: 10px;
    z-index: ${(props) => props.theme.zindex.zIndexMiniBag};
    right: 1px;
    border: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
    margin-top: -70px;
    box-shadow: 4px 6px 6px 0 rgb(163 162 162 / 20%);
    top: inherit;

    &::after {
      content: '';
      height: 20px;
      width: 20px;
      position: absolute;
      background-color: ${(props) => props.theme.colorPalette.gray[300]};
      top: -12px;
      right: 17px;
      border-top: ${(props) => props.theme.colorPalette.gray[500]} solid 1px;
      border-left: ${(props) => props.theme.colorPalette.gray[500]} solid 1px;
      transform: rotate(45deg);
      z-index: -1;
    }
    &::before {
      content: '';
      height: 50px;
      width: 60px;
      position: absolute;
      opacity: 0;
      top: -50px;
      right: -2px;
      z-index: 1;
    }
  }

  .fake-icon {
    height: 42px;
    width: 44px;
    position: absolute;
    opacity: 0;
    top: -48px;
    right: 1px;
    z-index: 1;
    cursor: pointer;
    border: none;
  }
`;
export const customStyles = css`
  .spinner-overlay {
    position: absolute;
  }
`;

export default styles;
