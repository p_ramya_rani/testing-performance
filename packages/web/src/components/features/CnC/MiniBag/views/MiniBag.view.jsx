// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'next/router'; //eslint-disable-line
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import { getLabelValue, routerPush } from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getSflItemCount, getCartItemCount } from '@tcp/core/src/utils/cookie.util';
import SpinnerOverlay from '@tcp/core/src/components/common/atoms/SpinnerOverlay';
import styles, { modalStyles, customStyles } from '../styles/MiniBag.style';
import MiniBagHeader from '../molecules/MiniBagHeader/views/MiniBagHeader';
import MiniBagBody from '../molecules/MiniBagBody/views/MiniBagBody';
import { getSiteId } from '../../../../../../../core/src/utils/utils.web';

class MiniBag extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      country: getSiteId() && getSiteId().toUpperCase(),
    };
  }

  /* eslint-disable-next-line */
  UNSAFE_componentWillReceiveProps({ router: nextRouter }) {
    const { router, closeMiniBagDispatch } = this.props;
    /* istanbul ignore else */
    if (router && nextRouter && router.asPath !== nextRouter.asPath) {
      closeMiniBagDispatch();
    }
  }

  onLinkClick = ({ e, componentId }) => {
    const { onRequestClose, openOverlay, showFullPageAuth } = this.props;
    const { pathname = 'referrer' } = (window && window.location) || {};
    e.preventDefault();

    if (showFullPageAuth && componentId === 'createAccount') {
      routerPush(`/home?target=register&successtarget=${pathname}`, '/home/register');
    } else if (showFullPageAuth && componentId === 'login') {
      routerPush(`/home?target=login&successtarget=${pathname}`, '/home/login');
    } else {
      openOverlay({
        component: componentId,
        variation: 'primary',
      });
      onRequestClose();
    }
  };

  closeMiniBag = (e) => {
    if (e) e.preventDefault();
    const { closeMiniBagDispatch } = this.props;
    setTimeout(() => {
      closeMiniBagDispatch();
    }, 10);
  };

  onMouseLeave = (e) => {
    const { minibagOnHoverEnabled } = this.props;
    if (minibagOnHoverEnabled) {
      this.closeMiniBag(e);
    }
  };

  renderMiniBagHeader = (cartItemCount) => {
    const {
      labels,
      userName,
      currentPoints,
      totalRewards,
      onRequestClose,
      openOverlay,
      isPlcc,
      isUserLoggedIn,
      isRememberedUser,
      showFullPageAuth,
    } = this.props;

    return (
      <MiniBagHeader
        labels={labels}
        cartItemCount={cartItemCount}
        userName={userName}
        currentPoints={currentPoints}
        totalRewards={totalRewards}
        onRequestClose={onRequestClose}
        openOverlay={openOverlay}
        isPlcc={isPlcc}
        isUserLoggedIn={isUserLoggedIn}
        isRememberedUser={isRememberedUser}
        showFullPageAuth={showFullPageAuth}
      />
    );
  };

  render() {
    const {
      onRequestClose,
      className,
      openState,
      labels,
      userName,
      subTotal,
      isCartItemsUpdating,
      isCartItemSFL,
      cartItemSflError,
      resetSuccessMessage,
      addedToBagError,
      isShowSaveForLaterSwitch,
      isUserLoggedIn,
      isRememberedUser,
      miniBagLoaderState,
      isMiniBag,
      router,
      closeMiniBagDispatch,
      totalItems,
      isCartLoading,
    } = this.props;
    const { country } = this.state;
    const cartItemCount = totalItems || getCartItemCount();
    const sflItemsCount = getSflItemCount(country);
    return (
      openState && (
        <Modal
          isOpen={openState}
          onRequestClose={onRequestClose}
          overlayClassName="TCPModal__Overlay"
          className={`TCPModal__Content, ${className}`}
          closeIconDataLocator="mini-bag-close"
          aria={{
            labelledby: 'Mini Bag',
            describedby: 'Mini Bag Modal',
          }}
          contentLabel={getLabelValue(labels.accessibility, 'cartIconButton')}
          data-locator="mini-bag-modal"
          inheritedStyles={modalStyles}
          closeIconLeftAligned
        >
          <div onMouseLeave={(e) => this.onMouseLeave(e)}>
            {miniBagLoaderState && <SpinnerOverlay inheritedStyles={customStyles} />}
            {this.renderMiniBagHeader(cartItemCount)}
            <MiniBagBody
              closeMiniBag={onRequestClose}
              labels={labels}
              cartItemCount={cartItemCount}
              userName={userName}
              subTotal={subTotal}
              isCartItemsUpdating={isCartItemsUpdating}
              savedforLaterQty={sflItemsCount}
              isCartItemSFL={isCartItemSFL}
              cartItemSflError={cartItemSflError}
              onLinkClick={this.onLinkClick}
              resetSuccessMessage={resetSuccessMessage}
              addedToBagError={addedToBagError}
              isShowSaveForLaterSwitch={isShowSaveForLaterSwitch}
              isUserLoggedIn={isUserLoggedIn}
              router={router}
              closeMiniBagDispatch={closeMiniBagDispatch}
              isRememberedUser={isRememberedUser}
              isMiniBag={isMiniBag}
              isCartLoading={isCartLoading}
            />
          </div>
        </Modal>
      )
    );
  }
}

MiniBag.propTypes = {
  className: PropTypes.string.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  openState: PropTypes.bool.isRequired,
  labels: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  router: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  userName: PropTypes.string.isRequired,
  subTotal: PropTypes.string.isRequired,
  currentPoints: PropTypes.string.isRequired,
  totalRewards: PropTypes.string.isRequired,
  isCartItemsUpdating: PropTypes.bool.isRequired,
  isCartItemSFL: PropTypes.bool.isRequired,
  cartItemSflError: PropTypes.string.isRequired,
  closeMiniBagDispatch: PropTypes.func.isRequired,
  openOverlay: PropTypes.func.isRequired,
  resetSuccessMessage: PropTypes.func.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  addedToBagError: PropTypes.string.isRequired,
  isShowSaveForLaterSwitch: PropTypes.bool.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  isRememberedUser: PropTypes.bool.isRequired,
  miniBagLoaderState: PropTypes.bool.isRequired,
  isMiniBag: PropTypes.bool.isRequired,
  totalItems: PropTypes.number,
  showFullPageAuth: PropTypes.bool,
  minibagOnHoverEnabled: PropTypes.bool,
  isCartLoading: PropTypes.bool.isRequired,
};

MiniBag.defaultProps = {
  totalItems: 0,
  showFullPageAuth: false,
  minibagOnHoverEnabled: false,
};

export default withRouter(withStyles(MiniBag, styles));
export { MiniBag as MiniBagVanilla };
