// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'next/router';
import Account from '@tcp/core/src/components/features/account/Account';

const TrackOrder = ({ router, ...props }) => {
  const routerProp = {
    ...router,
    ...{
      query: {
        id: 'orders',
        subSection: 'order-details',
      },
    },
  };

  return <Account router={routerProp} {...props} />;
};

TrackOrder.propTypes = {
  router: PropTypes.objectOf(PropTypes.shape({})).isRequired,
};

export default withRouter(TrackOrder);
