// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

export const Icon = styled.span`
  width: 70px;
  height: 70px;
  background-size: cover;
  border-radius: 10px;
  display: inline-block;
  vertical-align: middle;
`;

export const Container = styled.div`
  margin-top: 300px;
  text-align: center;
`;
