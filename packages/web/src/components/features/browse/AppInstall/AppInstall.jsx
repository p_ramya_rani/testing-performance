// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import {
  getIOSAppStoreDetail,
  androidPlayStoreLink,
} from '@tcp/core/src/components/common/atoms/AppBanner/AppBanner.utils';
import { getLabelValue, isiOSWeb, getBrand, getIconPath } from '@tcp/core/src/utils';
import { Container, Icon } from './AppInstall.style';

const tcpIcon = getIconPath('tcp-smart-banner-icon');
const GymIcon = getIconPath('gym-smart-banner-icon');

export const AppInstall = props => {
  useEffect(() => {
    const timer = setTimeout(() => {
      if (window.history.length > 2) window.history.back();
      clearTimeout(timer);
    }, 5000);
    window.location = isiOSWeb() ? getIOSAppStoreDetail() : androidPlayStoreLink();
  });
  const { redirectLabel } = props;
  const icon = getBrand() === 'tcp' ? tcpIcon : GymIcon;
  const iconStyle = {
    backgroundImage: `url(${icon})`,
  };
  return (
    <Container>
      <h4>{redirectLabel}</h4>
      <Icon style={iconStyle} />
    </Container>
  );
};

AppInstall.propTypes = {
  redirectLabel: PropTypes.string.isRequired,
};

export const mapStateToProps = state => {
  return {
    redirectLabel: getLabelValue(
      state.Labels,
      `lbl_redirect_${isiOSWeb() ? 'app' : 'play'}_store`,
      'modules',
      'global'
    ),
  };
};

export default connect(mapStateToProps)(AppInstall);
