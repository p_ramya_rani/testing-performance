// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { AppInstall, mapStateToProps } from '../AppInstall';

jest.mock('../../../../../../../core/src/utils', () => ({
  getBrand: jest
    .fn()
    .mockReturnValueOnce('tcp')
    .mockReturnValueOnce('gym'),
  getIconPath: jest
    .fn()
    .mockReturnValueOnce('tcp-smart-banner-icon')
    .mockReturnValueOnce('gym-smart-banner-icon'),
  isiOSWeb: jest
    .fn()
    .mockReturnValueOnce(true)
    .mockReturnValueOnce(false),
  getLabelValue: jest
    .fn()
    .mockReturnValueOnce('lbl_redirect_app_store')
    .mockReturnValueOnce('lbl_redirect_play_store'),
}));

describe('Appinstall page component', () => {
  let AppInstallComp;

  beforeEach(() => {
    AppInstallComp = shallow(<AppInstall />);
  });
  it('should render correctly', () => {
    expect(AppInstallComp).toMatchSnapshot();
  });
});

describe('Appinstall page mapStateToProps', () => {
  const initialState = {
    Labels: {
      global: {
        modules: {
          lbl_redirect_app_store: 'lbl_redirect_app_store',
          lbl_redirect_play_store: 'lbl_redirect_play_store',
        },
      },
    },
  };

  it('should retuen ios label', () => {
    expect(mapStateToProps(initialState).redirectLabel).toEqual('lbl_redirect_app_store');
  });

  it('should return android label', () => {
    expect(mapStateToProps(initialState).redirectLabel).toEqual('lbl_redirect_play_store');
  });
});
