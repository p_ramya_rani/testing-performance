// 9fbef606107a605d69c0edbcd8029e5d 
import { withRouter } from 'next/router';
import hoistNonReactStatic from 'hoist-non-react-statics';
import OutfitDetails from '@tcp/core/src/components/features/browse/OutfitDetails';

const outfitDetailsWithRouter = withRouter(OutfitDetails);

hoistNonReactStatic(outfitDetailsWithRouter, OutfitDetails, {
  getInitialProps: true,
});

export default outfitDetailsWithRouter;
