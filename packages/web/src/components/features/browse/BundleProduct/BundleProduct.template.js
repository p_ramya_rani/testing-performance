// 9fbef606107a605d69c0edbcd8029e5d 
import { withRouter } from 'next/router';
import hoistNonReactStatic from 'hoist-non-react-statics';
import BundleProduct from '@tcp/core/src/components/features/browse/BundleProduct';

const BundleWithRouter = withRouter(BundleProduct);

hoistNonReactStatic(BundleWithRouter, BundleProduct, {
  getInitialProps: true,
});

export default BundleWithRouter;
