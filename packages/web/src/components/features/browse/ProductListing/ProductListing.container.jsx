/* eslint-disable extra-rules/no-commented-out-code, max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import Head from 'next/head';
import { NextSeo } from 'next-seo';
import isEqual from 'lodash/isEqual';
import logger from '@tcp/core/src/utils/loggerInstance';
import withIsomorphicRenderer from '@tcp/core/src/components/common/hoc/withIsomorphicRenderer';
import withHotfix from '@tcp/core/src/components/common/hoc/withHotfix';
import withRefWrapper from '@tcp/core/src/components/common/hoc/withRefWrapper';
import SEOTags from '@tcp/web/src/components/common/atoms';
import ProductListingAbstractor from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListingApiHandler';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { change, getFormValues } from 'redux-form';
import dynamic from 'next/dynamic';
import { loadLayoutData, fetchPageLayout } from '@tcp/core/src/reduxStore/actions';
import { arrayOf, bool, func, objectOf, oneOfType, number, shape, string } from 'prop-types';
import {
  getAPIConfig,
  getStaticFilePath,
  isClient,
  isIEBrowser,
  isMobileApp,
} from '@tcp/core/src/utils/utils';
import utils, {
  cleanUpNonStdQueryString,
  getSessionStorage,
  setSessionStorage,
} from '@tcp/core/src/utils';
import { deriveSEOTags } from '@tcp/core/src/config/SEOTags.config';
import {
  getIsKeepAliveProduct,
  getIsInternationalShipping,
  getIsShowPriceRange,
  getABTestIsShowPriceRange,
  getIsPlpCategoryFiltersEnabled,
  getCategoryFilterEnabledList,
  getIsBrierleyPromoEnabled,
  getIsPlpPdpAnchoringEnabled,
  getIsSeoHeaderEnabled,
  getIsDynamicBadgeEnabled,
  getABTestForStickyFilter,
  getIsPLPCarouselEnabled,
  getIsNonInvSessionFlag,
  getIsShowBrandNameEnabled,
  getIsBopisFilterEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import navigationAbstractor from '@tcp/core/src/services/abstractors/bootstrap/navigation/navigation';
import {
  getDefaultStore,
  isPlccUser,
  getUserLoggedInState,
  isRememberedUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getFavoriteStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { trackPageView, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import {
  getPlpProducts,
  getMorePlpProducts,
  setEssentialShopStep,
  setListingFirstProductsPage,
  setPlpScrollState,
  setPlpLoadingState,
  loadCategoryPageSeoData,
  setBopisFilterState,
} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.actions';
import {
  removeAddToFavoriteErrorState,
  addItemsToWishlist,
  getUserFavt,
  setLastDeletedItemIdAction,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import {
  getPageName,
  getPageSection,
  getPageSubSection,
} from '@tcp/core/src/components/common/organisms/PickupStoreModal/molecules/PickupStoreSelectionForm/container/PickupStoreSelectionForm.selectors';
import {
  openQuickViewWithValues,
  closeQuickViewModal,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import {
  processBreadCrumbs,
  getProductsAndTitleBlocks,
  findCategoryIdandName,
  getCategoryFilters,
  getIsCLPPage,
  checkPromoCondition,
} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import {
  getProductsSelect,
  getNavigationTree,
  getLoadedProductsCount,
  getUnbxdId,
  getProductsFilters,
  getCategoryId,
  getLabelsProductListing,
  getLongDescription,
  getSeoHeaderCopytext,
  getIsLoadingMore,
  getShouldPlpScroll,
  getLastLoadedPageNumber,
  getLoadedProductsPages,
  getTotalProductsCount,
  getAppliedFilters,
  getIsBopisFilterOn,
  getLabels,
  getAccessibilityLabels,
  get404Labels,
  getIsFilterBy,
  getPLPTopPromos,
  getPLPGridPromos,
  getPlpHorizontalPromo,
  getLabelsOutOfStock,
  getIsSearchPage,
  getOnModelImageAbTestPlp,
  getIsHidePLPAddToBag,
  getEntityCategory,
  getPageUrl,
  getIsHidePLPRatings,
  getIs404AbTest,
  get404RedirectExperience,
  getAbPlpPersonalized,
  getAbPlpPersonalizedCategories,
  getNewPLPExperienceAbTest,
  getCategoryNavigationInfo,
  getIsShowProductsOnL1,
  getCategoryFilterArray,
  getFilterAppliedState,
  getPlpInGridRecommendation,
  getPlpInMultiGridRecommendation,
  checkInGridRecommendationEnabled,
  getGenericCarousel,
  getShowCarouselFlag,
  getInitialValues,
  getSeoTitle,
  getSeoMetaDesc,
  getCanonicalUrl,
} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.selectors';
import submitProductListingFiltersForm from '@tcp/core/src/components/features/browse/ProductListing/container/productListingOnSubmitHandler';
import getSortLabels from '@tcp/core/src/components/features/browse/ProductListing/molecules/SortSelector/views/Sort.selectors';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import {
  fetchAddToFavoriteErrorMsg,
  fetchErrorMessages,
  wishListFromState,
  getDefaultFavItemInProgressFlag,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.selectors';
import {
  resetStoresByCoordinates,
  getFavoriteStoreActn,
} from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import { styliticsProductTabListDataReqforOutfit } from '@tcp/core/src/components/common/organisms/StyliticsProductTabList/container/StyliticsProductTabList.actions';

import { PRODUCTS_PER_LOAD } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.constants';
import { singlePageLoadAction } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.actions';
import { SEO_DATA } from '@tcp/core/src/reduxStore/constants';
import { getCategoryName } from '@tcp/core/src/components/features/browse/CategoryListing/utils/utility';

const CategoryListing = dynamic(() =>
  import('@tcp/core/src/components/features/browse/CategoryListing')
);
const ProductListing = dynamic(() =>
  import('@tcp/core/src/components/features/browse/ProductListing/views')
);
const OutfitListingContainer = dynamic(() =>
  import('@tcp/core/src/components/features/browse/OutfitListing/container')
);
const categoryPromo = dynamic(() =>
  import(
    /* webpackChunkName: "categoryPromo" */ '@tcp/core/src/components/features/browse/CategoryListing/molecules/CategoryPromoImages'
  )
);
const outfitCarousel = dynamic(() =>
  import(
    /* webpackChunkName: "outfitCarousel" */ '@tcp/core/src/components/common/molecules/OutfitCarouselModule'
  )
);
const moduleX = dynamic(() =>
  import(/* webpackChunkName: "moduleX" */ '@tcp/core/src/components/common/molecules/ModuleX')
);
const Espot = dynamic(() =>
  import(/* webpackChunkName: "espot" */ '@tcp/core/src/components/common/molecules/Espot')
);
const divisionTabs = dynamic(() =>
  import(
    /* webpackChunkName: "divisionTabs" */ '@tcp/core/src/components/common/molecules/DivisionTabModule'
  )
);
const jeans = dynamic(() =>
  import(/* webpackChunkName: "jeans" */ '@tcp/core/src/components/common/molecules/JeansModule')
);
const essentials = dynamic(() =>
  import(
    /* webpackChunkName: "essentials" */ '@tcp/core/src/components/features/browse/ProductListing/organisms/EssentialShop'
  )
);

const dynamicClpModules = {
  categoryPromo,
  moduleX,
  outfitCarousel,
};

const dynamicPromoModules = {
  Espot,
  divisionTabs,
  outfitCarousel,
  jeans,
  moduleX,
  essentials,
};

const checkIfBackPlpPage = ({ loadedProductCount = 0, pageUrl }) => {
  const savedUrl = decodeURIComponent(pageUrl).replace(/\?/g, '');
  const newUrl =
    (isClient() &&
      decodeURIComponent(`${window.location.pathname}?${window.location.search}`).replace(
        /\?/g,
        ''
      )) ||
    '';

  return savedUrl === newUrl && loadedProductCount > 0;
};

const getPageInfo = (navigation, asPath, props) => {
  return {
    isBackPLPPage: checkIfBackPlpPage(props),
    url: (navigation && navigation.getParam('url')) || asPath,
  };
};

const checkIfL2L3DataAvailable = (navigationData) => {
  const nonEmptySubCat =
    navigationData &&
    navigationData.find((navItem) => Object.keys(navItem.subCategories).length !== 0);
  if (!nonEmptySubCat) {
    return false;
  }
  return true;
};

const shouldUnbxdRequestTrigger = (
  isBackPLPPage,
  navigationData,
  productsBlockLength,
  pageUrl,
  asPath,
  isCLP
) => {
  let productsBlockArrayLength = productsBlockLength;
  if ((pageUrl && pageUrl.split('?')[0] !== asPath) || isCLP) {
    productsBlockArrayLength = 0;
  }
  return (
    !isBackPLPPage &&
    !window.isUnbxdRequestTriggered &&
    checkIfL2L3DataAvailable(navigationData) &&
    !productsBlockArrayLength
  );
};

const shouldRetryNavigationInfo = (navigationData, prevNavigationData) => {
  return (
    typeof window !== 'undefined' &&
    (!window.PLP_CATEGORY_NAVIGATION_INFO || isIEBrowser()) &&
    !isEqual(navigationData, prevNavigationData) &&
    !checkIfL2L3DataAvailable(prevNavigationData) &&
    checkIfL2L3DataAvailable(navigationData)
  );
};

const getPLPServerloadProductCount = (state) => {
  return state && state.APIConfig && state.APIConfig.firstLoadPLPProductCount;
};

class ProductListingContainer extends React.Component {
  static getCategoryIdFromUrl = (url) => {
    const catIdWithQuery = url && url.split('/c/')[1];
    return catIdWithQuery && catIdWithQuery.split('?')[0];
  };

  static loadNavigationData = (originalUrl, deviceBot, state, productCount, apiConfig) => {
    const catId = ProductListingContainer.getCategoryIdFromUrl(originalUrl);

    logger.info(
      `PLPDEBUGLOGS ::: loadNavigationData Requested catId: ${catId} OriginalURL:  ${originalUrl}`
    );

    return navigationAbstractor
      .getData(
        {
          module: 'navigation',
          data: {
            depth: 2,
            ignoreCache: true,
          },
        },
        apiConfig
      )
      .then((data) => {
        const catNavFromTaxonomy = findCategoryIdandName(data, catId, true).reverse();
        const plpSeoInfo = findCategoryIdandName(data, catId);
        return ProductListingContainer.getParsedResponse(
          originalUrl,
          data,
          deviceBot,
          state,
          productCount,
          apiConfig
        ).then((response) => {
          return {
            apiRes: response.apiRes,
            catNavFromTaxonomy,
            plpSeoInfo,
            catId,
            deviceBot,
            layout: response.layout,
            modules: response.modules,
          };
        });
      });
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity, complexity
  static getInitialProps = async ({ isServer, props, req, deviceBot, state, store }) => {
    const {
      getProducts,
      navigation,
      navigationData,
      routerParam,
      router = {},
      getStyliticsProductTabListData,
      apiConfig,
      productsBlock,
      pageUrl,
      loadSeoData,
    } = props;
    const productsBlockLength = productsBlock ? productsBlock.length : 0;
    let { asPath = '' } = routerParam || router;
    let pathForSeo;
    if (isServer) {
      const { params: { cid = '' } = {} } = req || {};
      pathForSeo = cid;
    } else {
      const { query: { cid = '' } = {} } = router;
      pathForSeo = cid;
    }
    const payload = {
      apiConfig,
      page: cleanUpNonStdQueryString(pathForSeo),
    };
    loadSeoData({ module: SEO_DATA.categorySeoData, ...payload });

    asPath = asPath || req.originalUrl;
    const path = asPath.substring(asPath.lastIndexOf('/') + 1);
    if (!isServer) {
      const { url, isBackPLPPage } = getPageInfo(navigation, asPath, props);
      const { siteId } = getAPIConfig();
      const isCLP = getIsCLPPage(navigationData, router, siteId);
      /* Send the unbxd request only when the navigation data for L2 and L3 is available
      and pageUrl is not stale */
      if (
        shouldUnbxdRequestTrigger(
          isBackPLPPage,
          navigationData,
          productsBlockLength,
          pageUrl,
          asPath,
          isCLP
        )
      ) {
        await getProducts({ URI: 'category', url, ignoreCache: true });
      }
    } else if (path.indexOf('-outfits') > -1) {
      const categoryId = (navigation && navigation.getParam('outfitPath')) || path;
      await getStyliticsProductTabListData({ categoryId, count: 20 });
    } else {
      // loads L1-2-3 data on server
      const productCount = getPLPServerloadProductCount(state);
      const navDataObj = await ProductListingContainer.loadNavigationData(
        req.originalUrl,
        deviceBot,
        state,
        productCount,
        apiConfig
      );
      logger.info(
        'PLPDEBUGLOGS ::: navDataObj ProductListingContainer.loadNavigationData(req.originalUrl, deviceBot, state) ##### ',
        navDataObj
      );
      const { apiRes: { processedPlpProducts: { isDepartment: isClp = false } = {} } = {} } =
        navDataObj;
      if (isClp) {
        const pageName = getCategoryName(asPath);
        store.dispatch(fetchPageLayout({ pageName, layoutName: null, isClpPage: true, apiConfig }));
      }
      store.dispatch(
        getPlpProducts({
          URI: 'category',
          url: req.originalUrl,
          prefetchedResponse: navDataObj.apiRes,
          isProcessedResponse: true,
          productCount,
          apiConfig,
          navDataObj,
        })
      );
      return { ...navDataObj, productCount };
    }

    return {};
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { router: { asPath } = {}, navigationData } = nextProps;
    const { siteId } = getAPIConfig();
    const asPathWithoutQueryString = asPath && asPath.split('?')[0];

    // This check identifies if this page is DLP/CLP
    const isCLP =
      navigationData &&
      navigationData.find((item) => {
        return (
          item.categoryContent &&
          `/${siteId}${item.categoryContent.asPath}` === asPathWithoutQueryString
        );
      });
    const path = asPath.substring(asPath.lastIndexOf('/') + 1);
    return {
      ...prevState,
      isOutfit: path.indexOf('-outfits') > -1,
      asPath: path,
      isCLP,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      isOutfit: false,
      asPath: '',
      staticBreadCrumbs: [], // Stores breadcrumb until new breadcrumb fetched
    };
    this.plpResponseCallback = this.plpResponseCallback.bind(this);
    if (isClient() && window.isUnbxdRequestTriggered) {
      if (window.elem && !window.PLP_RESPONSE_INITIAL) {
        window.elem.addEventListener('unbxdResRecieved', this.plpResponseCallback);
      } else if (window.PLP_RESPONSE_INITIAL) {
        this.plpResponseCallback(
          {
            detail: {
              unbxdResponse: window.PLP_RESPONSE_INITIAL.unbxdResponse,
            },
          },
          this
        );
      }
    }
  }

  componentDidMount() {
    const { getHomePageLayout, homeLayouts, navigationData, router, breadCrumbs } = this.props;
    const apiConfigObj = getAPIConfig();
    const { loadHomePageDataEnabled, timeOutGap, siteId } = apiConfigObj;
    const isCLP = getIsCLPPage(navigationData, router, siteId);
    if (
      !isMobileApp() &&
      !isCLP &&
      loadHomePageDataEnabled &&
      Object.entries(homeLayouts).length === 0
    ) {
      setTimeout(() => {
        getHomePageLayout(apiConfigObj);
      }, timeOutGap);
    }
    if (breadCrumbs && breadCrumbs.length > 0) {
      this.setState({ staticBreadCrumbs: breadCrumbs });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  componentDidUpdate(prevProps) {
    const {
      router: { asPath, query: { cid } } = {},
      appliedFilterIds: prevAppliedFilterIds,
      loadSeoData,
      breadCrumbs,
    } = prevProps;
    const {
      router,
      router: { asPath: currentAsPath } = {},
      onFavtLoad,
      isLoggedIn,
      setEssentialStep,
      navigationData,
      isFilterApplied,
      resetPlpScrollState,
      updatePlpLoadingState,
    } = this.props;
    if (prevProps.isLoggedIn !== isLoggedIn) {
      onFavtLoad(true);
    }
    if (breadCrumbs && breadCrumbs.length > 0) {
      this.setState({ staticBreadCrumbs: breadCrumbs });
    } else {
      this.setBreadCrumbFromLocal();
    }
    let modifiedAsPath = asPath.split('?');
    let modifiedCurrentAsPath = currentAsPath.split('?');

    if (
      shouldRetryNavigationInfo(navigationData, prevProps.navigationData) ||
      this.getFilterChanges(asPath, currentAsPath, prevAppliedFilterIds)
    ) {
      this.makeApiCall();
    } else {
      modifiedAsPath = asPath.split('?');
      modifiedCurrentAsPath = currentAsPath.split('?');
      const payload = {
        apiConfig: getAPIConfig(),
        page: cleanUpNonStdQueryString(cid),
      };
      loadSeoData({ module: SEO_DATA.categorySeoData, ...payload });
      if (modifiedAsPath[0] !== modifiedCurrentAsPath[0]) {
        resetPlpScrollState(false);
        this.makeApiCall();
      } else if (modifiedAsPath[1] !== modifiedCurrentAsPath[1]) {
        if (isFilterApplied) {
          updatePlpLoadingState({ filterApplied: false });
        } else {
          this.makeApiCall();
        }
        resetPlpScrollState(false);
      }
    }
    this.setDefaultStore(prevAppliedFilterIds);

    if (modifiedAsPath[0] !== modifiedCurrentAsPath[0]) {
      setEssentialStep(0);
    }
    const { isBackPLPPage } = getPageInfo(null, null, this.props);
    const { siteId } = getAPIConfig();
    const isCLP = getIsCLPPage(navigationData, router, siteId);
    const scrollPointInStorage = getSessionStorage('SCROLL_POINT');
    const scrollPointNum = parseInt(scrollPointInStorage, 10);
    const unbxdparamSKU = getSessionStorage('UNBXDPARAM_SKU');
    const item = document.querySelectorAll(`[unbxdparam_sku = '${unbxdparamSKU}']`);
    if (isBackPLPPage && isCLP && scrollPointNum && item.length) {
      this.handleScroll(resetPlpScrollState);
      sessionStorage.removeItem('UNBXDPARAM_SKU');
    }
  }

  componentWillUnmount() {
    if (window.elem && !window.PLP_RESPONSE_INITIAL) {
      window.elem.removeEventListener('unbxdResRecieved', this.plpResponseCallback);
    }
  }

  setBreadCrumbFromLocal = () => {
    try {
      const staticBreadCrumbs = processBreadCrumbs(
        JSON.parse(getLocalStorage('staticPlpBreadCrumbs'))
      );
      if (staticBreadCrumbs && staticBreadCrumbs.length > 0) {
        this.setState({ staticBreadCrumbs });
      }
    } catch (e) {
      console.log(e);
    }
  };

  getDefaultStore = () => {
    const { favStore } = this.props;
    try {
      if (favStore?.basicInfo?.id) return favStore;
      return JSON.parse(getLocalStorage('defaultStore')) || {};
    } catch (e) {
      return {};
    }
  };

  setDefaultStore = () => {
    const { getFavoriteStoreById, appliedFilterIds } = this.props;
    const [bopisStoreId] = appliedFilterIds?.bopisStoreId || [];

    const defaultStore = this.getDefaultStore();
    const localStoreId = defaultStore?.basicInfo?.id;
    const [storeId] = bopisStoreId?.match(/\d+/) || [];
    const fullStoreId = `11${storeId}`;
    if (
      !this.isFavStoreUpdated &&
      storeId &&
      bopisStoreId &&
      localStoreId &&
      fullStoreId !== localStoreId
    ) {
      getFavoriteStoreById({ storeId: fullStoreId });
      this.isFavStoreUpdated = true;
    }
  };

  static getParsedResponse = (originalUrl, data, deviceBot, state, productCount, apiConfig) => {
    const locationFromUrl = (originalUrl && originalUrl.split('?')) || [];
    return ProductListingAbstractor({
      navigationData: data,
      location: {
        pathname: locationFromUrl[0],
        search: locationFromUrl[1] ? `?${locationFromUrl[1]}` : '',
      },
      state,
      productCount: deviceBot ? PRODUCTS_PER_LOAD : productCount,
      apiConfig,
    });
  };

  getAppliedFilterValues = (appliedFilterIds) => {
    const filterOptions = Object.keys(appliedFilterIds).map((key) => {
      return appliedFilterIds[key];
    });

    const newArray = filterOptions.filter((element) => {
      if (element.length > 0) return element;
      return null;
    });

    return newArray && newArray.length === 0;
  };

  getFilterChanges(asPath, currentAsPath, prevAppliedFilterIds) {
    const { appliedFilterIds, isLoadingMore } = this.props;
    const compareFilter = !isLoadingMore && !isEqual(prevAppliedFilterIds, appliedFilterIds);

    return (
      asPath !== currentAsPath &&
      currentAsPath.includes('?') &&
      appliedFilterIds &&
      compareFilter &&
      (Object.keys(appliedFilterIds).length === 0 ||
        (Object.keys(appliedFilterIds).length && this.getAppliedFilterValues(appliedFilterIds)))
    );
  }

  plpResponseCallback = (e) => {
    const { getProducts } = this.props;
    const { unbxdResponse } = e.detail;
    getProducts({
      URI: 'category',
      prefetchedResponse: unbxdResponse,
      responseFromPrefetchScript: true,
      productCount: window.firstPageProductsPerLoad,
    });
    window.isUnbxdRequestTriggered = false;
  };

  makeApiCall = () => {
    const {
      getProducts,
      navigation,
      navigationData,
      router: { asPath } = {},
      isShowProductsOnL1,
    } = this.props;
    const path = asPath.substring(asPath.lastIndexOf('/') + 1);
    if (path.indexOf('-outfits') > -1) {
      this.setState({
        isOutfit: true,
        asPath: path,
      });
    } else {
      this.setState({
        isOutfit: false,
      });
    }
    const url = navigation && navigation.getParam('url');

    const isCLP = navigationData.find(
      (item) => item.categoryContent && item.categoryContent.asPath === asPath
    );
    if (!isCLP || isShowProductsOnL1) {
      getProducts({ URI: 'category', url, ignoreCache: true });
    }
  };

  getSearchTypeParam = () => {
    const { router } = this.props;
    return utils.getObjectValue(router, '', 'query', 'searchType');
  };

  getImageForSEO = () => {
    const { products } = this.props;
    const currentProduct = products && products[0];
    const colorArr =
      currentProduct && Array.isArray(currentProduct.colorsMap) && currentProduct.colorsMap[0];
    const color = colorArr && colorArr.color && colorArr.color.name;
    return (
      color && currentProduct.imagesByColor && currentProduct.imagesByColor[color].basicImageUrl
    );
  };

  getSEOTags = (pageId) => {
    const {
      productsBlock,
      plpSeoInfo,
      seoTitle = '',
      seoMetaDesc = '',
      canonicalUrl = '',
      router,
    } = this.props;

    if (pageId && productsBlock) {
      let imageURL = null;
      try {
        imageURL = this.getImageForSEO();
      } catch (ex) {
        logger.error({
          error: `Error while extracting PLP image for og tag - ${ex}`,
          extraData: {
            componentName: `ProductListing Container`,
            method: `getSEOTags`,
            plpSeoInfo,
            productsBlock,
          },
        });
      }
      const seoConfig = deriveSEOTags(
        pageId,
        { seoTitle, seoMetaDesc, canonicalUrl },
        router,
        imageURL
      );
      return seoConfig && seoConfig.title && <SEOTags seoConfig={seoConfig} pageId={pageId} />;
    }
    return null;
  };

  linkSEO = (lang) => <link rel="alternate" hrefLang={lang.id} href={lang.canonicalUrl} />;

  handleScroll = (resetPlpScrollState) => {
    resetPlpScrollState(false);
    const scrollPointInStorage = getSessionStorage('SCROLL_POINT');
    const scrollPoint = isClient() ? scrollPointInStorage && parseInt(scrollPointInStorage, 10) : 0;
    if (isClient() && scrollPoint) {
      window.scrollTo({ top: scrollPoint, behavior: 'smooth' });
      setSessionStorage({ key: 'SCROLL_POINT', value: 0 });
    }
  };

  // eslint-disable-next-line complexity, sonarjs/cognitive-complexity
  render() {
    const { botProps, catNavFromTaxonomy, catId, deviceBot, productCount } = this.props;
    const { staticBreadCrumbs } = this.state;
    logger.info(
      'PLPDEBUGLOGS ::: render catNavFromTaxonomy :: ',
      catNavFromTaxonomy,
      'PLPDEBUGLOGS ::: render catId :: ',
      catId
    );

    const compProps = (deviceBot && botProps) || this.props;
    const {
      filterCount,
      products,
      filters,
      currentNavIds,
      categoryId,
      breadCrumbs,
      totalProductsCount,
      filtersLength,
      initialValues,
      longDescription,
      lastLoadedPageNumber,
      productsBlock,
    } = compProps;
    const {
      isLoadingMore,
      labelsFilter,
      getProducts,
      onSubmit,
      onQuickViewOpenClick,
      formValues,
      sortLabels,
      slpLabels,
      isLoggedIn,
      isPlcc,
      currencyAttributes,
      currency,
      plpTopPromos,
      plpGridPromos,
      plpHorizontalPromos,
      isSearchListing,
      navigation,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      pageNameProp,
      pageSectionProp,
      pageSubSectionProp,
      trackPageLoad,
      isSearchPage,
      entityCategoryName,
      defaultWishListFromState,
      apiConfig,
      defaultStoreId,
      storeId,
      router,
      labels,
      accessibilityLabels,
      isShowProductsOnL1,
      categoryFilterArray,
      isPlpCategoryFiltersEnabled,
      categoryFilterEnabledList,
      isBrierleyPromoEnabled,
      isPlpPdpAnchoringEnabled,
      isSeoHeaderEnabled,
      isAbPlpPersonalized,
      abPlpPersonalizedCategories,
      navigationData,
      seoHeaderCopyText,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isPlpBannerRecoEnabled,
      isDynamicBadgeEnabled,
      isABTestForStickyFilter,
      genericCarousel,
      isPromoApplied,
      isNonInvSessionFlag,
      isShowBrandNameEnabled,
      isBopisFilterOn,
      ...otherProps
    } = this.props;
    const apiConfigObj = getAPIConfig();
    const { isNonInvStagEnabled } = apiConfigObj || {};
    const isSecureImageFlagEnabled = isNonInvSessionFlag && isNonInvStagEnabled;
    const categoryFilters =
      catNavFromTaxonomy && catNavFromTaxonomy.length
        ? getCategoryFilters(catNavFromTaxonomy)
        : categoryFilterArray;

    const { router: { asPath: asPathVal } = {} } = this.props;
    let path = asPathVal;
    const { isOutfit, asPath, isCLP } = this.state;
    const searchType = this.getSearchTypeParam();
    if (isClient()) {
      path = window.location.pathname;
    }
    const navTree = getNavigationTree(navigationData, path);

    const seoTags = ProductListingContainer.pageInfo.pageId
      ? this.getSEOTags(ProductListingContainer.pageInfo.pageId)
      : null;

    const seoProps = seoTags && seoTags.props && seoTags.props.seoConfig;

    if (isCLP && !isShowProductsOnL1) {
      return (
        <>
          <NextSeo {...seoProps} />
          <Head>{seoProps && seoProps.hrefLangs && seoProps.hrefLangs.map(this.linkSEO)}</Head>
          <CategoryListing
            breadCrumbs={breadCrumbs}
            currentNavIds={currentNavIds}
            navTree={navTree}
            categoryId={categoryId}
            isSearchPage={isSearchPage}
            searchType={searchType}
            filterCount={filterCount}
            accessibilityLabels={accessibilityLabels}
            isPlcc={isPlcc}
            isLoggedIn={isLoggedIn}
            plpTopPromos={plpTopPromos}
            isLoadingMore={isLoadingMore}
            seoHeaderCopyText={seoHeaderCopyText}
            isSeoHeaderEnabled={isSeoHeaderEnabled}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            isABTestForStickyFilter={isABTestForStickyFilter}
            isSecureImageFlagEnabled={isSecureImageFlagEnabled}
            dynamicModules={dynamicClpModules}
            dynamicPromoModules={dynamicPromoModules}
            seoText={longDescription}
            isBopisFilterOn={isBopisFilterOn}
          />
        </>
      );
    }
    const brand = apiConfig.brandId.toUpperCase();
    const brandUnboxKey = apiConfig[`unboxKey${brand}`];
    const unbxdKey = apiConfig[`unbxd${brand}`];

    const requestUrl = `${unbxdKey}/${brandUnboxKey}/category`;
    const defaultStore = this.getDefaultStore();

    const catInfoString =
      (typeof window !== 'undefined' && window.PLP_CATEGORY_NAVIGATION_INFO) ||
      (catNavFromTaxonomy &&
        catNavFromTaxonomy.length &&
        JSON.stringify(catNavFromTaxonomy).replace(/'/g, "\\'")) ||
      '';

    if (!isClient() && catInfoString === '') {
      logger.error({
        error: `Navigation error: Could not find category in navigation data`,
        extraData: {
          componentName: 'ProductListing.container',
          methodName: 'render',
          propsReceived: this.props,
          category: categoryId,
          catNavFromTaxonomy,
        },
      });
    }

    if (!isClient() && catInfoString !== '' && catInfoString.indexOf(catId) === -1) {
      logger.error({
        error: `PLP error: Category information does not match the requested category`,
        extraData: {
          componentName: 'ProductListing.container',
          methodName: 'render',
          propsReceived: this.props,
          catInfoString,
          catId,
        },
      });
    }

    return !isOutfit ? (
      <>
        <NextSeo {...seoProps} />
        <Head>{seoProps && seoProps.hrefLangs && seoProps.hrefLangs.map(this.linkSEO)}</Head>
        {catInfoString && (
          <>
            {!isClient() && (
              <Head>
                <link
                  rel="preload"
                  href={getStaticFilePath('scripts/prefetch-unbxd.min.js', 'noCookieLess')}
                  as="script"
                />
                <script
                  id="unbxdPrefetchMeta"
                  key="rwd_unbxd_prefetch_meta"
                  // eslint-disable-next-line react/no-danger
                  dangerouslySetInnerHTML={{
                    __html: `
                    var PLP_CATEGORY_NAVIGATION_INFO = '${catInfoString}';
                    var unbxdUrl = '${requestUrl}';
                    var firstPageProductsPerLoad = ${productCount};
                  `,
                  }}
                />
                <script
                  key="rwd_unbxd_perfetch_script"
                  type="text/javascript"
                  src={getStaticFilePath('scripts/prefetch-unbxd.min.js', 'noCookieLess')}
                />
              </Head>
            )}
            <div id="elem" />
          </>
        )}
        <ProductListing
          {...otherProps}
          productsBlock={productsBlock}
          products={products}
          filters={filters}
          currentNavIds={currentNavIds}
          categoryId={categoryId}
          navTree={navTree}
          breadCrumbs={staticBreadCrumbs}
          totalProductsCount={totalProductsCount}
          initialValues={initialValues}
          filtersLength={filtersLength}
          longDescription={longDescription}
          seoHeaderCopyText={seoHeaderCopyText}
          labelsFilter={labelsFilter}
          labels={labels}
          accessibilityLabels={accessibilityLabels}
          isLoadingMore={isLoadingMore}
          lastLoadedPageNumber={lastLoadedPageNumber}
          getProducts={getProducts}
          onSubmit={onSubmit}
          onQuickViewOpenClick={onQuickViewOpenClick}
          formValues={formValues}
          sortLabels={sortLabels}
          slpLabels={slpLabels}
          isLoggedIn={isLoggedIn}
          isPlcc={isPlcc}
          currency={currency}
          currencyAttributes={currencyAttributes}
          plpTopPromos={plpTopPromos}
          plpGridPromos={plpGridPromos}
          plpHorizontalPromos={plpHorizontalPromos}
          asPathVal={asPathVal}
          isSearchListing={isSearchListing}
          navigation={navigation}
          AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
          removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
          pageNameProp={pageNameProp}
          pageSectionProp={pageSectionProp}
          pageSubSectionProp={pageSubSectionProp}
          trackPageLoad={trackPageLoad}
          isSearchPage={isSearchPage}
          entityCategoryName={entityCategoryName}
          searchType={searchType}
          filterCount={filterCount}
          wishList={defaultWishListFromState}
          storeId={defaultStoreId || storeId}
          router={router}
          categoryFilters={categoryFilters}
          isPlpCategoryFiltersEnabled={isPlpCategoryFiltersEnabled}
          categoryFilterEnabledList={categoryFilterEnabledList}
          asPath={asPath}
          isBrierleyPromoEnabled={isBrierleyPromoEnabled}
          isPlpPdpAnchoringEnabled={isPlpPdpAnchoringEnabled}
          isAbPlpPersonalized={isAbPlpPersonalized}
          abPlpPersonalizedCategories={abPlpPersonalizedCategories}
          isSeoHeaderEnabled={isSeoHeaderEnabled}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          isPlpBannerRecoEnabled={isPlpBannerRecoEnabled}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          isABTestForStickyFilter={isABTestForStickyFilter}
          genericCarousel={genericCarousel}
          isPromoApplied={isPromoApplied}
          dynamicPromoModules={dynamicPromoModules}
          isSecureImageFlagEnabled={isSecureImageFlagEnabled}
          isShowBrandNameEnabled={isShowBrandNameEnabled}
          isBopisFilterOn={isBopisFilterOn}
          defaultStore={defaultStore}
        />
      </>
    ) : (
      <>
        <NextSeo {...seoProps} />
        <Head>{seoProps && seoProps.hrefLangs && seoProps.hrefLangs.map(this.linkSEO)}</Head>
        <OutfitListingContainer
          {...otherProps}
          asPath={asPath}
          asPathVal={asPathVal}
          breadCrumbs={breadCrumbs}
          navTree={navTree}
          currentNavIds={currentNavIds}
          longDescription={longDescription}
          seoHeaderCopyText={seoHeaderCopyText}
          categoryId={categoryId}
          plpTopPromos={plpTopPromos}
          setClickAnalyticsData={setClickAnalyticsData}
          pageNameProp={pageNameProp}
          pageSectionProp={pageSectionProp}
          pageSubSectionProp={pageSubSectionProp}
          trackPageLoad={trackPageLoad}
          searchType={searchType}
          filterCount={filterCount}
          router={router}
          isSeoHeaderEnabled={isSeoHeaderEnabled}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          isABTestForStickyFilter={isABTestForStickyFilter}
        />
      </>
    );
  }
}

ProductListingContainer.pageInfo = {
  pageId: 'c',
  pageData: {
    pageName: 'browse',
    pageSection: 'browse',
    pageSubSection: 'browse',
    loadAnalyticsOnload: false,
  },
};

// eslint-disable-next-line complexity
function mapStateToProps(state) {
  const productBlocks = getLoadedProductsPages(state);
  const appliedFilters = getAppliedFilters(state);

  const loadedProductCount = getLoadedProductsCount(state);
  const totalProductsCount = getTotalProductsCount(state);

  const filtersLength = {};
  let filterCount = 0;

  // eslint-disable-next-line
  for (let key in appliedFilters) {
    if (appliedFilters[key]) {
      filtersLength[`${key}Filters`] = appliedFilters[key].length;
      filterCount += appliedFilters[key].length;
    }
  }
  const plpHorizontalPromos = getPlpHorizontalPromo(state);
  const plpGridPromos = getPLPGridPromos(state);
  const inGridPlp = getPlpInGridRecommendation(state);
  const inMultiGridPlp = getPlpInMultiGridRecommendation(state);
  const { showInGridPromos } = utils.getABtestFromState(state.AbTest);
  const isPromoApplied = checkPromoCondition(
    filterCount,
    plpHorizontalPromos,
    plpGridPromos,
    inGridPlp,
    inMultiGridPlp,
    showInGridPromos
  );
  return {
    apiConfig: state.APIConfig,
    productsBlock: isPromoApplied
      ? getProductsAndTitleBlocks(
          state,
          productBlocks,
          plpGridPromos,
          plpHorizontalPromos,
          4,
          filterCount,
          inGridPlp,
          inMultiGridPlp,
          loadedProductCount,
          totalProductsCount
        )
      : productBlocks,
    filterCount,
    isBopisFilterEnabled: getIsBopisFilterEnabled(state),
    products: getProductsSelect(state),
    filters: getProductsFilters(state),
    currentNavIds: state.ProductListing && state.ProductListing.currentNavigationIds,
    categoryId: getCategoryId(state),
    // navTree: getNavigationTree(state, originalPath),
    breadCrumbs: processBreadCrumbs(state.ProductListing && state.ProductListing.breadCrumbTrail),
    loadedProductCount,
    unbxdId: getUnbxdId(state),
    totalProductsCount,
    filtersLength,
    initialValues: getInitialValues(state),
    labelsFilter: state.Labels && state.Labels.PLP && state.Labels.PLP.PLP_sort_filter,
    longDescription: getLongDescription(state),
    seoHeaderCopyText: getSeoHeaderCopytext(state),
    labels: getLabelsProductListing(state),
    accessibilityLabels: getAccessibilityLabels(state),
    banner404Labels: get404Labels(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    isLoadingMore: getIsLoadingMore(state),
    shouldPlpScrollState: getShouldPlpScroll(state),
    lastLoadedPageNumber: getLastLoadedPageNumber(state),
    onSubmit: submitProductListingFiltersForm,
    // Need to pass form values in as prop so we can compare current values to previous values
    formValues: getFormValues('filter-form')(state),
    isPlcc: isPlccUser(state),
    sortLabels: getSortLabels(state),
    slpLabels: getLabels(state),
    isGuest: getUserLoggedInState(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    isFilterBy: getIsFilterBy(state),
    pageUrl: getPageUrl(state),
    currencyAttributes: getCurrencyAttributes(state),
    currency: getCurrentCurrency(state),
    routerParam: state.routerParam,
    plpTopPromos: getPLPTopPromos(state),
    plpGridPromos: getPLPGridPromos(state),
    plpHorizontalPromos: getPlpHorizontalPromo(state),
    AddToFavoriteErrorMsg: fetchAddToFavoriteErrorMsg(state),
    navigationData: state.Navigation && state.Navigation.navigationData,
    isKeepAliveEnabled: getIsKeepAliveProduct(state),
    pageNameProp: getPageName(state),
    pageSectionProp: getPageSection(state),
    pageSubSectionProp: getPageSubSection(state),
    isSearchPage: getIsSearchPage(state),
    errorMessages: fetchErrorMessages(state),
    defaultWishListFromState: wishListFromState(state),
    isOnModelAbTestPlp: getOnModelImageAbTestPlp(state),
    isInternationalShipping: getIsInternationalShipping(state),
    entityCategoryName: getEntityCategory(state),
    isShowPriceRangeKillSwitch: getIsShowPriceRange(state),
    showPriceRangeForABTest: getABTestIsShowPriceRange(state),
    storeId: getFavoriteStore(state),
    favStore: state.User && state.User.get('defaultStore'),
    defaultStoreId: getDefaultStore(state),
    appliedFilterIds: getAppliedFilters(state),
    isBopisFilterOn: getIsBopisFilterOn(state),
    isHidePLPAddToBag: getIsHidePLPAddToBag(state),
    isHidePLPRatings: getIsHidePLPRatings(state),
    is404AbTest: getIs404AbTest(state),
    redirect404Patterns: get404RedirectExperience(state),
    isAbPlpPersonalized: getAbPlpPersonalized(state),
    abPlpPersonalizedCategories: getAbPlpPersonalizedCategories(state),
    categoryNavigationInfo: getCategoryNavigationInfo(state),
    isShowProductsOnL1: getIsShowProductsOnL1(state),
    categoryFilterArray: getCategoryFilterArray(state),
    isPlpCategoryFiltersEnabled: getIsPlpCategoryFiltersEnabled(state),
    categoryFilterEnabledList: getCategoryFilterEnabledList(state),
    isBrierleyPromoEnabled: getIsBrierleyPromoEnabled(state),
    isFilterApplied: getFilterAppliedState(state),
    isPlpPdpAnchoringEnabled: getIsPlpPdpAnchoringEnabled(state),
    isSeoHeaderEnabled: getIsSeoHeaderEnabled(state),
    deleteFavItemInProgressFlag: getDefaultFavItemInProgressFlag(state),
    isPlpBannerRecoEnabled: getNewPLPExperienceAbTest(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    isABTestForStickyFilter: getABTestForStickyFilter(state),
    isPlpGridEnabled: checkInGridRecommendationEnabled(state),
    homeLayouts: state.Layouts.home,
    isPLPCarouselEnabled: getIsPLPCarouselEnabled(state),
    genericCarousel: getGenericCarousel(state),
    showCarouselInspiteOfPromo: getShowCarouselFlag(state),
    isPromoApplied,
    seoTitle: getSeoTitle(state),
    seoMetaDesc: getSeoMetaDesc(state),
    canonicalUrl: getCanonicalUrl(state),
    isNonInvSessionFlag: getIsNonInvSessionFlag(state),
    isShowBrandNameEnabled: getIsShowBrandNameEnabled(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeQuickViewModalAction: () => {
      dispatch(closeQuickViewModal({ isModalOpen: false }));
    },
    onFavtLoad: (payload) => {
      dispatch(getUserFavt(payload));
    },
    getProducts: (payload) => {
      dispatch(getPlpProducts(payload));
    },
    setEssentialStep: (payload) => {
      dispatch(setEssentialShopStep(payload));
    },
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    getMoreProducts: (payload) => {
      dispatch(getMorePlpProducts(payload));
    },
    onAddItemToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
    getStyliticsProductTabListData: (payload) => {
      dispatch(styliticsProductTabListDataReqforOutfit(payload));
    },
    removeAddToFavoritesErrorMsg: (payload) => {
      dispatch(removeAddToFavoriteErrorState(payload));
    },
    addToCartEcom: () => {},
    addItemToCartBopis: () => {},
    trackPageLoad: (payload) => {
      const {
        products,
        customEvents,
        departmentList,
        categoryList,
        listingSubCategory,
        pageSearchText,
        listingCount,
        pageSearchType,
        internalCampaignId,
        supressProps,
        storeId,
        promoFilterCategoryTrail,
        ...others
      } = payload;
      dispatch(
        setClickAnalyticsData({
          products,
          customEvents,
          departmentList,
          categoryList,
          listingSubCategory,
          pageSearchText,
          listingCount,
          pageSearchType,
          internalCampaignId,
          supressProps,
          storeId,
          promoFilterCategoryTrail,
        })
      );
      const timer = setTimeout(() => {
        dispatch(
          trackPageView({
            props: {
              initialProps: {
                pageProps: {
                  pageData: {
                    ...others,
                  },
                },
              },
            },
          })
        );
        const analyticsTimer = setTimeout(() => {
          dispatch(setClickAnalyticsData({}));
          clearTimeout(analyticsTimer);
        }, 200);
        clearTimeout(timer);
      }, 100);
    },
    setListingFirstProductsPageActn: (payload) => {
      dispatch(setListingFirstProductsPage(payload));
    },
    resetPlpScrollState: () => {
      dispatch(setPlpScrollState(false));
    },
    setBopisFilterStateActn: (payload) => {
      dispatch(setBopisFilterState(payload));
    },
    updatePlpLoadingState: (payload) => {
      dispatch(setPlpLoadingState(payload));
    },
    onSetLastDeletedItemIdAction: (payload) => {
      dispatch(setLastDeletedItemIdAction(payload));
    },
    onsinglePageLoadAction: (payload) => {
      dispatch(singlePageLoadAction(payload));
    },
    getHomePageLayout: (apiConfig) => {
      dispatch(fetchPageLayout({ pageName: 'home', apiConfig }));
    },
    loadSeoData: (payload) => {
      dispatch(loadCategoryPageSeoData(payload));
    },
    clearPromoDataOnRouteChange: () => {
      dispatch(loadLayoutData({}, 'productListingPage'));
    },
    updateFormValue: (field, value) => {
      dispatch(change('filter-form', field, value));
    },
    resetSuggestedStores: (payload) => dispatch(resetStoresByCoordinates(payload)),
    getFavoriteStoreById: (payload) => dispatch(getFavoriteStoreActn(payload)),
  };
}

ProductListingContainer.propTypes = {
  botProps: shape({}),
  catNavFromTaxonomy: arrayOf(shape({})),
  plpSeoInfo: shape({}),
  getProducts: func.isRequired,
  onQuickViewOpenClick: func.isRequired,
  getMoreProducts: func.isRequired,
  productsBlock: arrayOf(shape({})),
  categoryId: string.isRequired,
  products: arrayOf(shape({})),
  currentNavIds: arrayOf(shape({})),
  // navTree: shape({}),
  breadCrumbs: arrayOf(shape({})),
  filters: shape({}),
  totalProductsCount: string,
  filtersLength: shape({}),
  initialValues: shape({}),
  longDescription: string,
  seoHeaderCopyText: string,
  navigation: shape({
    getParam: func,
  }).isRequired,
  navigationData: shape({
    find: func,
  }),
  labels: objectOf(oneOfType([string])),
  accessibilityLabels: objectOf(oneOfType([string])),
  labelsFilter: objectOf(oneOfType([string])),
  isLoadingMore: bool,
  lastLoadedPageNumber: number,
  router: shape({
    asPath: string,
  }).isRequired,
  onSubmit: func.isRequired,
  formValues: shape({
    sort: string.isRequired,
  }).isRequired,
  sortLabels: arrayOf(shape({})),
  slpLabels: objectOf(oneOfType([string])),
  isLoggedIn: bool,
  isPlcc: bool,
  currencyAttributes: shape({}),
  currency: string,
  plpTopPromos: shape({}),
  closeQuickViewModalAction: func,
  isSearchListing: bool,
  plpGridPromos: shape({}),
  plpHorizontalPromos: shape({}),
  AddToFavoriteErrorMsg: string,
  removeAddToFavoritesErrorMsg: func,
  pageNameProp: string,
  pageSectionProp: string,
  pageSubSectionProp: string,
  trackPageLoad: func,
  isSearchPage: bool,
  isInternationalShipping: bool.isRequired,
  isGuest: bool.isRequired,
  entityCategoryName: string,
  filterCount: number,
  onFavtLoad: func.isRequired,
  defaultWishListFromState: func.isRequired,
  setFirstProducts: func.isRequired,
  apiConfig: shape({
    brandId: string,
  }).isRequired,
  defaultStoreId: string,
  storeId: string,
  appliedFilterIds: shape({}),
  setEssentialStep: func.isRequired,
  categoryNavigationInfo: shape({}),
  isShowProductsOnL1: bool,
  catId: string,
  categoryFilterArray: arrayOf(shape({})),
  isPlpCategoryFiltersEnabled: bool,
  deviceBot: bool,
  categoryFilterEnabledList: arrayOf(shape({})),
  productCount: number,
  isBrierleyPromoEnabled: bool,
  isFilterApplied: bool,
  resetPlpScrollState: func.isRequired,
  updatePlpLoadingState: func.isRequired,
  isAbPlpPersonalized: bool.isRequired,
  abPlpPersonalizedCategories: shape([]).isRequired,
  isPlpPdpAnchoringEnabled: bool,
  isSeoHeaderEnabled: bool,
  onSetLastDeletedItemIdAction: bool,
  deleteFavItemInProgressFlag: bool,
  isPlpBannerRecoEnabled: arrayOf({}),
  onsinglePageLoadAction: shape([]).isRequired,
  isDynamicBadgeEnabled: shape({}),
  isABTestForStickyFilter: bool,
  getHomePageLayout: func.isRequired,
  clearPromoDataOnRouteChange: func.isRequired,
  homeLayouts: shape({}),
  genericCarousel: shape([]),
  showCarouselInspiteOfPromo: bool,
  isPromoApplied: bool,
  isNonInvSessionFlag: bool,
  isShowBrandNameEnabled: bool,
  seoTitle: string,
  seoMetaDesc: string,
  canonicalUrl: string,
  loadSeoData: func,
  isBopisFilterEnabled: bool,
};

ProductListingContainer.defaultProps = {
  botProps: null,
  isBopisFilterEnabled: false,
  catNavFromTaxonomy: [],
  plpSeoInfo: null,
  products: [],
  productsBlock: [],
  currentNavIds: [],
  // navTree: {},
  breadCrumbs: [],
  filters: {},
  totalProductsCount: '0',
  filtersLength: {},
  initialValues: {},
  longDescription: '',
  seoHeaderCopyText: '',
  labels: {},
  accessibilityLabels: {},
  labelsFilter: {},
  isLoadingMore: true,
  lastLoadedPageNumber: 0,
  sortLabels: [],
  slpLabels: {},
  isLoggedIn: false,
  currencyAttributes: {
    exchangevalue: 1,
  },
  currency: 'USD',
  plpTopPromos: [],
  closeQuickViewModalAction: () => {},
  navigationData: null,
  isSearchListing: false,
  plpGridPromos: {},
  plpHorizontalPromos: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  isPlcc: false,
  pageNameProp: '',
  pageSectionProp: '',
  pageSubSectionProp: '',
  trackPageLoad: () => {},
  isSearchPage: false,
  entityCategoryName: '',
  filterCount: 0,
  defaultStoreId: null,
  storeId: null,
  appliedFilterIds: {},
  categoryNavigationInfo: {},
  isShowProductsOnL1: false,
  catId: '',
  categoryFilterArray: [],
  isPlpCategoryFiltersEnabled: false,
  deviceBot: false,
  categoryFilterEnabledList: [],
  productCount: null,
  isBrierleyPromoEnabled: false,
  isFilterApplied: false,
  isPlpPdpAnchoringEnabled: false,
  isSeoHeaderEnabled: false,
  isPlpBannerRecoEnabled: [],
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isDynamicBadgeEnabled: false,
  isABTestForStickyFilter: false,
  homeLayouts: {},
  genericCarousel: [],
  showCarouselInspiteOfPromo: false,
  isPromoApplied: false,
  isNonInvSessionFlag: false,
  isShowBrandNameEnabled: false,
  seoTitle: '',
  seoMetaDesc: '',
  canonicalUrl: '',
  loadSeoData: () => {},
};

const IsomorphicProductListingContainer = withIsomorphicRenderer({
  WrappedComponent: ProductListingContainer,
  mapStateToProps,
  mapDispatchToProps,
});

/**
 * Hotfix-Aware Component. The use of `withRefWrapper` and `withHotfix`
 * below are just for making the page hotfix-aware.
 */
const RefWrappedProductListingContainer = withRefWrapper(IsomorphicProductListingContainer);
RefWrappedProductListingContainer.displayName = 'ProductListingPage';
// eslint-disable-next-line no-unused-vars
const HotfixAwareProductListingContainer = withHotfix(RefWrappedProductListingContainer);

export default IsomorphicProductListingContainer;
