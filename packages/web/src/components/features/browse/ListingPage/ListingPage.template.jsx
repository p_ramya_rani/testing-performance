// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { withRouter } from 'next/router'; //eslint-disable-line
import { PropTypes } from 'prop-types';
import isEqual from 'lodash/isEqual';

const CategoryListing = dynamic(
  () => import('@tcp/core/src/components/features/browse/CategoryListing'),
  {
    loading: () => null,
  }
);

const ProductListing = dynamic(
  () => import('@tcp/core/src/components/features/browse/ProductListing'),
  {
    loading: () => null,
  }
);

class ListingPage extends React.Component {
  shouldComponentUpdate(nextProps) {
    if (isEqual(nextProps, this.props)) {
      return false;
    }
    return true;
  }

  render() {
    const { navigationData, router: { asPath } = {} } = this.props;
    return (
      <>
        {navigationData &&
        navigationData.find(
          item => item.categoryContent && item.categoryContent.asPath === asPath
        ) ? (
          <CategoryListing />
        ) : (
          <ProductListing />
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  navigationData: state.Navigation.navigationData,
});

ListingPage.propTypes = {
  navigationData: PropTypes.arrayOf(PropTypes.shape({})),
  router: PropTypes.shape({}),
};

ListingPage.defaultProps = {
  navigationData: [{}],
  router: {},
};

export default withRouter(connect(mapStateToProps)(ListingPage));
