/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router'; // eslint-disable-line
import hoistNonReactStatic from 'hoist-non-react-statics';
import withRefWrapper from '@tcp/core/src/components/common/hoc/withRefWrapper';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import withHotfix from '@tcp/core/src/components/common/hoc/withHotfix';
import moduleGConfig from '@tcp/core/src/components/common/molecules/ModuleG/moduleG.config';
import moduleJConfig from '@tcp/core/src/components/common/molecules/ModuleJ/moduleJ.config';
import { imageData } from '@tcp/core/src/components/features/browse/OutfitDetails/config';
import SEOTags from '@tcp/web/src/components/common/atoms';
import { NextSeo } from 'next-seo';
import Head from 'next/head';
import {
  getIfBotDevice,
  getObjectValue,
  isClient,
  parseBoolean,
  getRecommendation,
  scrollToBreadCrumb,
  isMobileWeb,
  isServer,
  isMobileApp,
  getAPIConfig,
} from '@tcp/core/src/utils';
import { getBrandNameFromHref } from '@tcp/core/src/utils/utils';
import { deriveSEOTags } from '@tcp/core/src/config/SEOTags.config';
import { arrayOf, bool, func, shape, string } from 'prop-types';
import {
  getDefaultStore,
  getUserLoggedInState,
  isRememberedUser,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getProducts } from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import { getFavoriteStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import ProductDetailView from '@tcp/core/src/components/features/browse/ProductDetail/views';
import {
  getProductDetails,
  setProductDetailsDynamicData,
  setProductDetails,
  resetCountUpdated,
  setDisableSelectedTab,
  loadSocialProofData,
  clearSocialProofData,
} from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.actions';

import { trackPageView, setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import {
  removeAddToFavoriteErrorState,
  addItemsToWishlist,
  getUserFavt,
  setLastDeletedItemIdAction,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import {
  getIsShowPriceRange,
  getIsKeepAliveProduct,
  getIsInternationalShipping,
  getABTestIsShowPriceRange,
  getIsCompleteTheLookTestEnabled,
  getABTestAddToBagDrawer,
  getMultiPackThreshold,
  getIsPerUnitPriceEnabled,
  getIsPlpPdpAnchoringEnabled,
  getIsBrierleyPromoEnabled,
  getABTestForPDPColorOrImageSwatchHover,
  getMultiColorNameEnabled,
  getIsDynamicBadgeEnabled,
  getIsPDPSmoothScrollEnabled,
  getIsFamilyOutfitABTestEnabled,
  getIsNewPDPEnabled,
  getIsABLoaded,
  getIsShowBrandNameEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getSearchTerm } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import {
  getAddedToBagError,
  addedToBagMsgSelector,
  getPDPlargeImageColorSelector,
} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import { getAddedToBagErrorCatId } from '@tcp/core/src/components/features/browse/OutfitDetails/container/OutfitDetails.selectors';

import {
  getNavTree,
  prodDetails,
  getBreadCrumbs,
  getDescription,
  getRatingsProductId,
  getDefaultImage,
  getPlpLabels,
  getCurrentProduct,
  getPDPLabels,
  getProductDetailFormValues,
  getShortDescription,
  getGeneralProductId,
  getAlternateSizes,
  getPDPTopPromos,
  getPDPMiddlePromos,
  getPDPBottomPromos,
  getPDPBelowRecsPromos,
  getSizeChartDetails,
  getPDPLoadingState,
  getAccessibilityLabels,
  getOnModelImageAbTestPdp,
  getIsAbPdpPlaEnabled,
  getIsOosPdpEnabled,
  isValidProduct,
  getProductInfoFromProductListingData,
  getStyliticsProductTabListSelector,
  getPageUrl,
  getProductInfoFromRecommendationData,
  getProductInfoFromModule,
  getProductInfoFromOutfit,
  getProductInfoFromBundle,
  getUpdatedCount,
  getPriceDisplayState,
  getMultiPackProductInfo,
  getMultiPackCount,
  getAllMultiPackData,
  partIdInformation,
  getAllNewMultiPackData,
  getSelectedMultipack,
  getPdpNewRecommendation,
  getIsPdp1andPdp2Enabled,
  getInitialMultipackMapping,
  getInitialAvailableTCPmapNewStyleId,
  getSinglePageLoad,
  getInitialTCPStyleQTY,
  getDisableMultiPackTab,
  getSocialProofMessage,
} from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.selectors';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import {
  getLabelsOutOfStock,
  getExternalCampaignFired,
} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.selectors';
import {
  addToCartEcom,
  clearAddToBagErrorState,
} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';
import { getCartItemInfo } from '@tcp/core/src/components/features/CnC/AddedToBag/util/utility';
import {
  fetchAddToFavoriteErrorMsg,
  wishListFromState,
  getDefaultFavItemInProgressFlag,
  fetchErrorMessages,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.selectors';
import PRODUCTDETAIL_CONSTANTS from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.constants';
import ProductDetailSkeleton from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductDetailSkeleton/index';
import { setExternalCampaignState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { fetchPageLayout } from '@tcp/core/src/reduxStore/actions';
import { getQuickViewLabels } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import logger from '@tcp/core/src/utils/loggerInstance';

const imgConfigValArray = {
  moduleG: moduleGConfig,
  moduleJ: moduleJConfig,
};

/**
 * Hotfix-Aware Component. The use of `withRefWrapper` and `withHotfix`
 * below are just for making the page hotfix-aware.
 */
const ProductDetailWithRef = withRefWrapper(ProductDetailView);
ProductDetailWithRef.displayName = 'ProductDetailPage';
const ProductDetail = withHotfix(ProductDetailWithRef);

const setIsBackPage = (pageUrl) => {
  return pageUrl && pageUrl === (isClient() && window.location.pathname);
};
const getPageInfo = (props) => {
  const { pageUrl } = props;
  return {
    isBackPage: setIsBackPage(pageUrl),
    url: isClient() ? window.location.pathname : null,
  };
};

const resetProductDetailPage = (
  clearAddToBagError,
  resetProductDetailsDynamicData,
  resetProductDetails
) => {
  clearAddToBagError();
  resetProductDetailsDynamicData();
  resetProductDetails();
};

const getBundleProductState = (state) =>
  state &&
  state.BundleProduct &&
  state.BundleProduct.currentBundle &&
  state.BundleProduct.currentBundle;

const getOutfitProductState = (state) =>
  state &&
  state.OutfitDetails &&
  state.OutfitDetails.currentOutfit &&
  state.OutfitDetails.currentOutfit.products;

class ProductDetailContainer extends React.PureComponent {
  static extractPID = (props) => {
    const {
      router: {
        query: { pid },
      },
    } = props;
    // TODO - fix this to extract the product ID from the page.
    const id = pid && pid.split('-');
    let productId = id && id.length > 1 ? `${id[id.length - 2]}_${id[id.length - 1]}` : pid;
    if (
      (id.indexOf('Gift') > -1 || id.indexOf('gift') > -1) &&
      (id.indexOf('Card') > -1 || id.indexOf('card') > -1)
    ) {
      productId = 'gift';
    }
    return productId;
  };

  static getDerivedStateFromProps = (props, state) => {
    const {
      productListingDetail,
      searchListingDetail,
      recommendationDetail,
      ProductTabList,
      BundleDetails,
      OutfitDetails,
      router: {
        query: { isSearchPage, dataSource = '' },
      },
    } = props;
    const dataToCarryForward = parseBoolean(isSearchPage)
      ? searchListingDetail
      : productListingDetail;

    const pid = ProductDetailContainer.extractPID(props);

    const { renderedPid } = state;
    if (pid !== renderedPid && isClient()) {
      if (dataSource === 'recommendation') {
        return {
          renderedPid: pid,
          retrievedProductInfo: getProductInfoFromRecommendationData({
            pid,
            details: recommendationDetail,
          }),
          imgConfigVal: { imgConfig: null },
        };
      }
      if (dataSource.indexOf('module_') !== -1) {
        const dataSourceParts = dataSource.split('_');
        const currentCatId = dataSourceParts[2];
        const moduleName = dataSourceParts[1];
        return {
          renderedPid: pid,
          retrievedProductInfo: getProductInfoFromModule({
            pid,
            details: ProductTabList[currentCatId],
          }),
          imgConfigVal: imgConfigValArray[`module${moduleName}`].IMG_DATA.productImgConfig,
        };
      }
      if (dataSource.indexOf('outfit') !== -1) {
        return {
          renderedPid: pid,
          retrievedProductInfo: getProductInfoFromOutfit({
            pid,
            details: OutfitDetails,
          }),
          imgConfigVal: imageData.imgConfig,
        };
      }
      if (dataSource.indexOf('bundle') !== -1) {
        return {
          renderedPid: pid,
          retrievedProductInfo: getProductInfoFromBundle({
            pid,
            details: BundleDetails,
          }),
          imgConfigVal: imageData.imgConfig,
        };
      }
      return {
        renderedPid: pid,
        retrievedProductInfo: getProductInfoFromProductListingData({
          pid,
          details: dataToCarryForward,
        }),
      };
    }
    return null;
  };

  state = {
    renderedPid: '',
    retrievedProductInfo: null,
  };

  componentDidMount() {
    const { props } = this;
    const { isPlpPdpAnchoringEnabled, getHomePageLayout, layouts, isNewPDPEnabled } = props;
    const { isSSRData } = props.productInfo;
    const apiConfigObj = getAPIConfig();
    const { loadHomePageDataEnabled, timeOutGap } = apiConfigObj;
    if (isMobileWeb()) {
      if (isPlpPdpAnchoringEnabled && isSSRData !== true) {
        scrollToBreadCrumb(false, isNewPDPEnabled);
      } else {
        window.scrollTo(0, 0);
      }
    } else {
      window.scrollTo(0, 0);
    }
    ProductDetailContainer.getInitialProps({ props });
    if (!isMobileApp() && loadHomePageDataEnabled && !layouts.home) {
      setTimeout(() => {
        getHomePageLayout(apiConfigObj);
      }, timeOutGap);
    }
  }
  /*
  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  } */

  componentDidUpdate(prevProps) {
    const {
      onFavtLoad,
      isLoggedIn,
      router: {
        query: { pid },
      },
      itemPartNumber,
      isPlpPdpAnchoringEnabled,
      isNewPDPEnabled,
    } = this.props;
    const productId = ProductDetailContainer.extractPID(this.props);
    const apiConfig = getAPIConfig();
    this.prevSearchPage = prevProps.router.query.isSearchPage;
    if (prevProps.router.query.pid !== pid) {
      this.makeApiCall({ productColorId: productId, apiConfig });
      if (isPlpPdpAnchoringEnabled && isMobileWeb()) {
        scrollToBreadCrumb(false, isNewPDPEnabled);
      } else {
        window.scrollTo(0, 100);
      }
    } else if (itemPartNumber && itemPartNumber !== productId) {
      this.makeApiCall({ productColorId: productId, apiConfig });
    }
    if (prevProps.isLoggedIn !== isLoggedIn) {
      onFavtLoad(true);
    }
  }

  makeApiCall = (payload) => {
    const {
      getDetails,
      router,
      pageUrl,
      clearAddToBagError,
      resetProductDetailsDynamicData,
      resetProductDetails,
    } = this.props;
    const asPath = (router && router.asPath) || '';
    const brandName = getBrandNameFromHref(asPath);
    const [url] = asPath.split('?');
    if (pageUrl && pageUrl !== url) {
      clearAddToBagError();
      resetProductDetailsDynamicData();
      resetProductDetails();
    }

    if ((pageUrl && pageUrl !== url) || !pageUrl) {
      getDetails({
        ...payload,
        ...(isClient() ? { url } : {}),
        otherBrand: brandName,
      });
    }
  };

  getSEOTags = (pageId, pdpLabels) => {
    const { productInfo, router } = this.props;
    const storeDetails = { labels: pdpLabels, productInfo };
    if (pageId) {
      const seoConfig = deriveSEOTags(pageId, storeDetails, router);
      return seoConfig ? <SEOTags seoConfig={seoConfig} pageId={pageId} /> : null;
    }
    return null;
  };

  handleAddToBag = () => {
    const {
      addToBagEcom,
      formValues,
      productInfo,
      multipackProduct,
      multiPackCount,
      getAllMultiPack,
      showAddtoBagDrawer,
      partIdInfo,
      getAllNewMultiPack,
      selectedMultipack,
      router,
    } = this.props;
    const asPath = router && router.asPath;
    const brandName = getBrandNameFromHref(asPath);
    const primaryBrand = productInfo && productInfo.primaryBrand;
    let cartItemInfo = getCartItemInfo(productInfo, formValues);
    cartItemInfo = {
      ...cartItemInfo,
      isPdpPage: true,
      originPage: this.prevSearchPage ? 'spdp' : 'pdp',
      multipackProduct,
      multiPackCount,
      getAllMultiPack,
      skipAddToBagModal: showAddtoBagDrawer,
      partIdInfo,
      getAllNewMultiPack,
      selectedMultipack,
      primaryBrand,
      alternateBrand: brandName,
    };
    addToBagEcom(cartItemInfo);
  };

  getNavigateTypeParam = () => {
    const { router } = this.props;
    return getObjectValue(router, '', 'query', 'navigateType', 'components');
  };

  checkIfFromPLPorSLP = () => {
    const { router } = this.props;
    if (!isServer() && router.components) {
      const isProductListing = Object.prototype.hasOwnProperty.call(
        router.components,
        '/ProductListing'
      );
      const isSearchPage = Object.prototype.hasOwnProperty.call(router.components, '/SearchDetail');
      return isProductListing || isSearchPage;
    }
    return false;
  };

  isProductAvailable = () => {
    const { productInfo, itemPartNumber } = this.props;
    const productId = ProductDetailContainer.extractPID(this.props);
    return Object.keys(productInfo).length > 0 && productId === itemPartNumber;
  };

  getOOSFlag = (productInfo) => {
    const { colorFitsSizesMap, tcpMultiPackReferenceUSStore, multiPackUSStore } = productInfo;
    const filteredColorFitsList =
      colorFitsSizesMap && colorFitsSizesMap.filter((item) => item.maxAvailable === 0);

    if (
      (colorFitsSizesMap && colorFitsSizesMap.length) ===
        (filteredColorFitsList && filteredColorFitsList.length) &&
      !(tcpMultiPackReferenceUSStore && multiPackUSStore)
    ) {
      return true;
    }
    return false;
  };

  openModal = (e) => {
    e.preventDefault();
    const { toggleModal } = this.props;
    toggleModal({ isModalOpen: true });
  };

  render() {
    const {
      productDetails,
      breadCrumbs,
      longDescription,
      itemPartNumber,
      shortDescription,
      ratingsProductId,
      defaultImage,
      productInfo,
      currency,
      currencyAttributes,
      plpLabels,
      pdpLabels,
      addToBagError,
      onAddItemToFavorites,
      isLoggedIn,
      isPlcc,
      alternateSizes,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      outOfStockLabels,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      topPromos,
      middlePromos,
      bottomPromos,
      belowrecsPromos,
      isLoading,
      trackPageLoad,
      sizeChartDetails,
      accessibilityLabels,
      isKeepAliveEnabled,
      pageProps,
      defaultWishListFromState,
      storeId,
      resetCountUpdate,
      countUpdated,
      isCompleteTheLookTestEnabled,
      hidePriceDisplay,
      addBtnMsg,
      multiPackCount,
      multiPackThreshold,
      multipackProduct,
      partIdInfo,
      getAllNewMultiPack,
      getDetails,
      getRecommendationProduct,
      isBrierleyPromoEnabled,
      isOosPdpEnabled,
      isPdpNewRecommendationEnabled,
      isPdp1andPdp2Enabled,
      largeImageNameOnHover,
      hasABTestForPDPColorOrImageSwatchHover,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      resetProductDetailsDynamicData,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      disableMultiPackTab,
      getDisableSelectedTab,
      istMultiColorNameEnabled,
      isDynamicBadgeEnabled,
      isPDPSmoothScrollEnabled,
      socialProofMessage,
      getSocialProofData,
      isFamilyOutfitEnabled,
      isNewPDPEnabled,
      errorMessages,
      isABTestLoaded,
      quickViewLabels,
      isShowBrandNameEnabled,
      ...otherProps
    } = this.props;

    const checkForOOSForAllVariantsFlag = this.getOOSFlag(productInfo);
    const { retrievedProductInfo, renderedPid, imgConfigVal } = this.state;
    const { router: { asPath: asPathVal } = {} } = this.props;
    const brandName = getBrandNameFromHref(asPathVal);
    const isProductDataAvailable = this.isProductAvailable();
    const navigateType = this.getNavigateTypeParam();
    const getPriceRange = isShowPriceRangeKillSwitch
      ? parseBoolean(isShowPriceRangeKillSwitch)
      : false;
    const getABTestPriceRange = showPriceRangeForABTest;
    const seoTags =
      ProductDetailContainer.pageInfo.pageId && productInfo
        ? this.getSEOTags(ProductDetailContainer.pageInfo.pageId, pdpLabels)
        : null;

    const seoProps = seoTags && seoTags.props && seoTags.props.seoConfig;
    const fromPLPPage = this.checkIfFromPLPorSLP();
    return (
      <>
        <NextSeo {...seoProps} />
        <Head>
          {seoProps &&
            seoProps.hrefLangs.map((lang) => (
              <link rel="alternate" hrefLang={lang.id} href={lang.canonicalUrl} />
            ))}
        </Head>
        <React.Fragment>
          {isProductDataAvailable ? (
            <ProductDetail
              {...otherProps}
              productDetails={productDetails}
              breadCrumbs={breadCrumbs}
              itemPartNumber={itemPartNumber}
              longDescription={longDescription}
              shortDescription={shortDescription}
              openModal={this.openModal}
              ratingsProductId={ratingsProductId}
              defaultImage={defaultImage}
              plpLabels={plpLabels}
              pdpLabels={pdpLabels}
              currency={currency}
              currencyAttributes={currencyAttributes}
              productInfo={productInfo}
              handleAddToBag={this.handleAddToBag}
              addToBagError={addToBagError}
              onAddItemToFavorites={onAddItemToFavorites}
              isLoggedIn={isLoggedIn}
              isPlcc={isPlcc}
              alternateSizes={alternateSizes}
              isShowPriceRangeKillSwitch={getPriceRange}
              showPriceRangeForABTest={getABTestPriceRange}
              outOfStockLabels={outOfStockLabels}
              AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
              removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
              asPathVal={asPathVal}
              topPromos={topPromos}
              middlePromos={middlePromos}
              bottomPromos={bottomPromos}
              belowrecsPromos={belowrecsPromos}
              trackPageLoad={trackPageLoad}
              sizeChartDetails={sizeChartDetails}
              accessibilityLabels={accessibilityLabels}
              isLoading={isLoading}
              hidePriceDisplay={hidePriceDisplay}
              isMatchingFamily // TODO: Need to add kill switch for this
              renderedPid={renderedPid}
              isKeepAliveEnabled={isKeepAliveEnabled}
              navigateType={navigateType}
              wishList={defaultWishListFromState}
              storeId={storeId}
              imgConfigVal={imgConfigVal}
              resetCountUpdate={resetCountUpdate}
              getSocialProofData={getSocialProofData}
              countUpdated={countUpdated}
              isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
              addBtnMsg={addBtnMsg}
              multiPackThreshold={multiPackThreshold}
              multiPackCount={multiPackCount}
              multipackProduct={multipackProduct}
              partIdInfo={partIdInfo}
              getAllNewMultiPack={getAllNewMultiPack}
              getDetails={getDetails}
              isRecommendationAvailable={getRecommendation(getRecommendationProduct)}
              checkForOOSForVariant={checkForOOSForAllVariantsFlag}
              isBrierleyPromoEnabled={isBrierleyPromoEnabled}
              isOosPdpEnabled={isOosPdpEnabled}
              isPdpNewRecommendationEnabled={isPdpNewRecommendationEnabled}
              isPdp1andPdp2Enabled={isPdp1andPdp2Enabled}
              largeImageNameOnHover={largeImageNameOnHover}
              hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
              initialMultipackMapping={initialMultipackMapping}
              setInitialTCPStyleQty={setInitialTCPStyleQty}
              resetProductDetailsDynamicData={resetProductDetailsDynamicData}
              onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
              deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
              disableMultiPackTab={disableMultiPackTab}
              getDisableSelectedTab={getDisableSelectedTab}
              istMultiColorNameEnabled={istMultiColorNameEnabled}
              isDynamicBadgeEnabled={isDynamicBadgeEnabled}
              fromPLPPage={fromPLPPage}
              isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
              socialProofMessage={socialProofMessage}
              isFamilyOutfitEnabled={isFamilyOutfitEnabled}
              errorMessages={errorMessages}
              isABTestLoaded={isABTestLoaded}
              isNewPDPEnabled={isNewPDPEnabled}
              quickViewLabels={quickViewLabels}
              isShowBrandNameEnabled={isShowBrandNameEnabled}
              primaryBrand={productInfo && productInfo.primaryBrand}
              alternateBrand={brandName}
            />
          ) : null}
          {!isProductDataAvailable && isLoading ? (
            <ProductDetailSkeleton
              retrievedProductInfo={retrievedProductInfo}
              breadCrumbs={breadCrumbs}
              ratingsProductId={ratingsProductId}
              pdpLabels={pdpLabels}
              outOfStockLabels={outOfStockLabels}
              accessibilityLabels={accessibilityLabels}
              isKeepAliveEnabled={isKeepAliveEnabled}
              productInfo={productInfo}
              currencyAttributes={currencyAttributes}
              currency={currency}
              imgConfigVal={imgConfigVal}
              isShowPriceRangeKillSwitch={getPriceRange}
              showPriceRangeForABTest={getABTestPriceRange}
              largeImageNameOnHover={largeImageNameOnHover}
              hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
              isDynamicBadgeEnabled={isDynamicBadgeEnabled}
              fromPLPPage={fromPLPPage}
              wishList={defaultWishListFromState}
              isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
              isFamilyOutfitEnabled={isFamilyOutfitEnabled}
              primaryBrand={retrievedProductInfo && retrievedProductInfo.primaryBrand}
              alternateBrand={brandName}
              isNewPDPEnabled={isNewPDPEnabled}
            />
          ) : null}
        </React.Fragment>
      </>
    );
  }
}

ProductDetailContainer.pageInfo = {
  pageId: 'p',
  pageData: {
    pageName: 'product',
    pageSection: 'product',
    pageSubSection: 'product',
    loadAnalyticsOnload: false,
  },
};

const isHomeBreadCrumb = (breadCrumbs) =>
  breadCrumbs &&
  breadCrumbs[0] &&
  breadCrumbs[0].displayName === PRODUCTDETAIL_CONSTANTS.FIRST_BREAD_CRUMB;

function mapStateToProps(state) {
  const breadCrumbs = getBreadCrumbs(state);
  const isDefaultBreadCrumb = isHomeBreadCrumb(breadCrumbs);
  return {
    navTree: getNavTree(state),
    isLoading: getPDPLoadingState(state),
    productDetails: prodDetails(state),
    breadCrumbs: getBreadCrumbs(state),
    longDescription: getDescription(state),
    itemPartNumber: getGeneralProductId(state),
    shortDescription: getShortDescription(state),
    ratingsProductId: getRatingsProductId(state),
    // This is just to check if the product is correct
    defaultImage: getDefaultImage(state),
    productInfo: getCurrentProduct(state),
    isValidProduct: isValidProduct(state),
    currency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    plpLabels: getPlpLabels(state),
    pdpLabels: getPDPLabels(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    addToBagError: getAddedToBagError(state),
    addToBagErrorId: getAddedToBagErrorCatId(state),
    formValues: getProductDetailFormValues(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    isPlcc: isPlccUser(state),
    alternateSizes: getAlternateSizes(state),
    isShowPriceRangeKillSwitch: getIsShowPriceRange(state),
    showPriceRangeForABTest: getABTestIsShowPriceRange(state),
    isKeepAliveEnabled: getIsKeepAliveProduct(state),
    isKeepAliveProduct: getIsKeepAliveProduct(state),
    AddToFavoriteErrorMsg: fetchAddToFavoriteErrorMsg(state),
    topPromos: getPDPTopPromos(state),
    middlePromos: getPDPMiddlePromos(state),
    bottomPromos: getPDPBottomPromos(state),
    belowrecsPromos: getPDPBelowRecsPromos(state),
    sizeChartDetails: getSizeChartDetails(state),
    accessibilityLabels: getAccessibilityLabels(state),
    isOnModelAbTestPdp: getOnModelImageAbTestPdp(state),
    isAbPdpPlaEnabled: getIsAbPdpPlaEnabled(state),
    isOosPdpEnabled: getIsOosPdpEnabled(state),
    completeLookSlotTile: getStyliticsProductTabListSelector(state),
    productListingDetail:
      state &&
      state.ProductListing.cacheUntil &&
      state.ProductListing &&
      state.ProductListing.loadedProductsPages,
    searchListingDetail:
      state && state.SearchListingPage && state.SearchListingPage.loadedProductsPages,
    recommendationDetail: state && state.Recommendations,
    ProductTabList: state && state.ProductTabList,
    BundleDetails: getBundleProductState(state),
    OutfitDetails: getOutfitProductState(state),
    isInternationalShipping: getIsInternationalShipping(state),
    defaultWishListFromState: wishListFromState(state),
    showAddtoBagDrawer: getABTestAddToBagDrawer(state), // show added to bag flash CTA for PDP
    pageUrl: getPageUrl(state),
    searchedText: getSearchTerm(state),
    storeId: getDefaultStore(state) || getFavoriteStore(state),
    countUpdated: getUpdatedCount(state),
    isExternalCampaignFired: getExternalCampaignFired(state),
    isCompleteTheLookTestEnabled: getIsCompleteTheLookTestEnabled(state),
    hidePriceDisplay: getPriceDisplayState(state),
    addBtnMsg: addedToBagMsgSelector(state),
    isDefaultBreadCrumb,
    multipackProduct: getMultiPackProductInfo(state),
    multiPackCount: getMultiPackCount(state),
    multiPackThreshold: getMultiPackThreshold(state),
    getAllMultiPack: getAllMultiPackData(state),
    partIdInfo: partIdInformation(state),
    getAllNewMultiPack: getAllNewMultiPackData(state),
    selectedMultipack: getSelectedMultipack(state),
    isPerUnitPriceEnabled: getIsPerUnitPriceEnabled(state),
    getRecommendationProduct: getProducts(state, 'pdp'),
    isPlpPdpAnchoringEnabled: getIsPlpPdpAnchoringEnabled(state),
    isBrierleyPromoEnabled: getIsBrierleyPromoEnabled(state),
    isPdpNewRecommendationEnabled: getPdpNewRecommendation(state),
    isPdp1andPdp2Enabled: getIsPdp1andPdp2Enabled(state),
    largeImageNameOnHover: getPDPlargeImageColorSelector(state),
    hasABTestForPDPColorOrImageSwatchHover: getABTestForPDPColorOrImageSwatchHover(state),
    initialMultipackMapping: getInitialMultipackMapping(state),
    setInitialTCPStyleQty: getInitialTCPStyleQTY(state),
    singlePageLoad: getSinglePageLoad(state),
    deleteFavItemInProgressFlag: getDefaultFavItemInProgressFlag(state),
    disableMultiPackTab: getDisableMultiPackTab(state),
    istMultiColorNameEnabled: getMultiColorNameEnabled(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    layouts: state.Layouts,
    availableTCPmapNewStyleId: getInitialAvailableTCPmapNewStyleId(state),
    isPDPSmoothScrollEnabled: getIsPDPSmoothScrollEnabled(state),
    socialProofMessage: getSocialProofMessage(state),
    isFamilyOutfitEnabled: getIsFamilyOutfitABTestEnabled(state),
    isNewPDPEnabled: getIsNewPDPEnabled(state),
    errorMessages: fetchErrorMessages(state),
    isABTestLoaded: getIsABLoaded(state),
    quickViewLabels: getQuickViewLabels(state),
    isShowBrandNameEnabled: getIsShowBrandNameEnabled(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toggleModal: (payload) => {
      dispatch(toggleApplyNowModal(payload));
    },
    resetProductDetailsDynamicData: () => dispatch(setProductDetailsDynamicData({ product: {} })),
    resetProductDetails: () => dispatch(setProductDetails({ product: {} })),
    getDetails: (payload) => {
      dispatch(getProductDetails(payload));
    },
    onFavtLoad: (payload) => {
      dispatch(getUserFavt(payload));
    },
    addToBagEcom: (payload) => {
      dispatch(addToCartEcom(payload));
    },
    clearAddToBagError: () => {
      dispatch(clearAddToBagErrorState());
    },
    onAddItemToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
    removeAddToFavoritesErrorMsg: (payload) => {
      dispatch(removeAddToFavoriteErrorState(payload));
    },
    setExternalCampaignFired: (payload) => {
      dispatch(setExternalCampaignState(payload));
    },
    trackPageLoad: (payload) => {
      const { products, customEvents, productId, pageSearchType, storeId, ...restPayload } =
        payload;
      dispatch(setClickAnalyticsData(payload));
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  products,
                  ...restPayload,
                },
              },
            },
          },
        })
      );
    },
    trackClick: (payload) => {
      dispatch(setClickAnalyticsData(payload));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 500);
    },
    resetCountUpdate: (payload) => {
      dispatch(resetCountUpdated(payload));
    },
    onSetLastDeletedItemIdAction: (payload) => {
      dispatch(setLastDeletedItemIdAction(payload));
    },
    getDisableSelectedTab: (payload) => {
      dispatch(setDisableSelectedTab(payload));
    },
    getHomePageLayout: (apiConfig) => {
      dispatch(fetchPageLayout({ pageName: 'home', apiConfig }));
    },
    getSocialProofData: (action) => {
      dispatch(clearSocialProofData(action));
      dispatch(loadSocialProofData(action));
    },
  };
}

function resetPDPCondition(productInfo) {
  return isClient() && productInfo && !productInfo.isSSRData;
}

ProductDetailContainer.getInitialProps = async (
  { props: passedProps, store, isServer: isServerReq, query, req, res, apiConfig },
  { setResStatusNotFound } = {}
  // eslint-disable-next-line sonarjs/cognitive-complexity
) => {
  const props = passedProps || {
    ...mapStateToProps(store.getState()),
    ...mapDispatchToProps(store.dispatch),
  };

  let config;
  if (isServerReq) {
    config = res.locals.apiConfig;
  } else {
    config = getAPIConfig();
  }

  const { unbxdUrl, unbxdSiteKey, unbxdApiKey } = config;
  const originalURL = `${unbxdUrl}/${unbxdApiKey}/${unbxdSiteKey}`;

  const {
    getDetails,
    clearAddToBagError,
    resetProductDetailsDynamicData,
    resetProductDetails,
    productInfo,
    router,
  } = props;

  let pid;
  if (isServerReq) {
    ({ pid } = query);
  } else {
    ({
      router: {
        query: { pid },
      },
    } = props);
  }
  // TODO - fix this to extract the product ID from the page.
  const productId = ProductDetailContainer.extractPID({
    ...props,
    router: { query: { pid } },
  });
  const isBotDevice = getIfBotDevice(req);

  const { url, isBackPage } = getPageInfo(props);
  const asPath = router && router.asPath;
  const brandName = getBrandNameFromHref(asPath);
  if (!isBackPage && !isServer()) {
    if (resetPDPCondition(productInfo)) {
      resetProductDetailPage(
        clearAddToBagError,
        resetProductDetailsDynamicData,
        resetProductDetails
      );
    }
    logger.info(`Making call to unbxd for product id: ${productId}`);
    await new Promise((resolve) =>
      getDetails({
        productColorId: productId,
        escapeEmptyProduct: true,
        resolve,
        isBotDevice,
        url,
        originalURL,
        apiConfig,
        otherBrand: brandName,
      })
    );
  }
  // Build a page name for tracking
  if (store && !isValidProduct(store.getState()) && setResStatusNotFound) {
    setResStatusNotFound();
  }
  let pageName = '';
  if (productId) {
    const productIdParts = productId.split('_');
    pageName = `product:${productIdParts[0]}:${(pid || '')
      .replace(productIdParts[0], '')
      .replace(productIdParts[1], '')
      .split('-')
      .join(' ')
      .trim()
      .toLowerCase()}`;
  }
  return {
    pageProps: {
      pageName,
      isBotDevice,
      brandName,
    },
  };
};

ProductDetailContainer.propTypes = {
  productDetails: arrayOf(shape({})),
  getDetails: func.isRequired,
  addToBagError: string,
  clearAddToBagError: func.isRequired,
  formValues: shape({}).isRequired,
  addToBagEcom: func.isRequired,
  productInfo: arrayOf(shape({})),
  breadCrumbs: shape([]),
  pdpLabels: shape({}),
  longDescription: string,
  shortDescription: string,
  itemPartNumber: string,
  ratingsProductId: string,
  isShowPriceRangeKillSwitch: bool.isRequired,
  showPriceRangeForABTest: bool.isRequired,
  router: shape({
    query: shape({
      pid: string,
      isSearchPage: bool,
    }),
  }).isRequired,
  defaultImage: string,
  currency: string,
  currencyAttributes: shape({}),
  plpLabels: shape({ lbl_sort: string }),
  onAddItemToFavorites: func.isRequired,
  isLoggedIn: bool,
  isPlcc: bool,
  alternateSizes: shape({ key: string }),
  outOfStockLabels: shape({}).isRequired,
  AddToFavoriteErrorMsg: string,
  removeAddToFavoritesErrorMsg: func,
  sizeChartDetails: shape([]),
  topPromos: string,
  middlePromos: string,
  bottomPromos: string,
  belowrecsPromos: string,
  isLoading: bool,
  trackPageLoad: func,
  accessibilityLabels: shape({}),
  pageProps: shape({}),
  resetProductDetailsDynamicData: func.isRequired,
  isKeepAliveEnabled: bool,
  completeLookSlotTile: shape({}),
  resetProductDetails: func.isRequired,
  isInternationalShipping: bool.isRequired,
  onFavtLoad: func.isRequired,
  defaultWishListFromState: func.isRequired,
  pageUrl: string.isRequired,
  storeId: string,
  countUpdated: bool,
  resetCountUpdate: func.isRequired,
  getSocialProofData: func,
  isCompleteTheLookTestEnabled: bool,
  showAddtoBagDrawer: bool.isRequired,
  hidePriceDisplay: bool,
  addBtnMsg: string.isRequired,
  multipackProduct: string.isRequired,
  multiPackCount: string.isRequired,
  multiPackThreshold: string.isRequired,
  getAllMultiPack: shape({}).isRequired,
  partIdInfo: string.isRequired,
  getAllNewMultiPack: shape({}).isRequired,
  getRecommendationProduct: bool.isRequired,
  isPlpPdpAnchoringEnabled: bool,
  isBrierleyPromoEnabled: bool,
  isOosPdpEnabled: bool,
  isPdpNewRecommendationEnabled: bool,
  isPdp1andPdp2Enabled: bool,
  selectedMultipack: string,
  largeImageNameOnHover: string,
  hasABTestForPDPColorOrImageSwatchHover: bool,
  initialMultipackMapping: shape([]).isRequired,
  setInitialTCPStyleQty: bool.isRequired,
  onSetLastDeletedItemIdAction: func,
  deleteFavItemInProgressFlag: bool,
  disableMultiPackTab: bool.isRequired,
  getDisableSelectedTab: func.isRequired,
  istMultiColorNameEnabled: bool.isRequired,
  isDynamicBadgeEnabled: shape({}),
  apiConfig: shape({}),
  getHomePageLayout: func.isRequired,
  layouts: shape({}),
  isPDPSmoothScrollEnabled: bool,
  socialProofMessage: string,
  isFamilyOutfitEnabled: bool,
  isNewPDPEnabled: bool,
  errorMessages: shape({}),
  isABTestLoaded: bool,
  quickViewLabels: shape({
    addToBag: string,
    viewProductDetails: string,
  }).isRequired,
  isShowBrandNameEnabled: bool,
  toggleModal: func,
};

ProductDetailContainer.defaultProps = {
  productDetails: [],
  productInfo: {},
  pageProps: {},
  addToBagError: '',
  breadCrumbs: null,
  longDescription: '',
  shortDescription: '',
  ratingsProductId: '',
  defaultImage: '',
  currency: 'USD',
  currencyAttributes: { exchangevalue: 1 },
  plpLabels: { lbl_sort: '' },
  itemPartNumber: '',
  pdpLabels: {},
  isLoggedIn: false,
  isPlcc: false,
  alternateSizes: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  sizeChartDetails: [],
  topPromos: '',
  middlePromos: '',
  bottomPromos: '',
  isLoading: false,
  trackPageLoad: () => {},
  getSocialProofData: () => {},
  accessibilityLabels: {},
  isKeepAliveEnabled: false,
  completeLookSlotTile: {},
  storeId: null,
  countUpdated: false,
  isCompleteTheLookTestEnabled: false,
  hidePriceDisplay: false,
  isPlpPdpAnchoringEnabled: false,
  isBrierleyPromoEnabled: false,
  isOosPdpEnabled: false,
  isPdpNewRecommendationEnabled: false,
  isPdp1andPdp2Enabled: true,
  selectedMultipack: '1',
  largeImageNameOnHover: '',
  hasABTestForPDPColorOrImageSwatchHover: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isDynamicBadgeEnabled: false,
  apiConfig: {},
  layouts: {},
  isPDPSmoothScrollEnabled: false,
  socialProofMessage: '',
  isFamilyOutfitEnabled: false,
  isNewPDPEnabled: false,
  errorMessages: {},
  isABTestLoaded: false,
  isShowBrandNameEnabled: false,
  toggleModal: () => {},
};

const PDPConnectComponent = connect(mapStateToProps, mapDispatchToProps)(ProductDetailContainer);

const PDPRouterWrapper = withRouter(PDPConnectComponent);

hoistNonReactStatic(PDPRouterWrapper, PDPConnectComponent, {
  getInitialProps: true,
});

export default PDPRouterWrapper;
