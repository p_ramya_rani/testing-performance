// 9fbef606107a605d69c0edbcd8029e5d
const FOOTER_CONSTANTS = {
  TCP_SUB_DOMAIN: '.childrensplace.com',
  GYMBOREE_SUB_DOMAIN: '.gymboree.com',
  PERMISSION_DENIED: 'footer_location_permission_denied',
  COOKIE_PLCC_OFFER_MODAL_PERSISTENT: 'tcp_plcc_offer_modal_persistent',
};
export default FOOTER_CONSTANTS;
