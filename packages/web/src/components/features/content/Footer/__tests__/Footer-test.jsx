// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { isClient, getAPIConfig } from '@tcp/core/src/utils/utils';
import { FooterVanilla } from '../views/Footer.view';

jest.mock('@tcp/core/src/utils/utils', () => ({
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  isMobileApp: jest.fn(),
  getAPIConfig: jest.fn(),
}));

jest.mock('@tcp/core/src/utils/utils.web', () => ({
  getPageNameUsingURL: jest.fn(),
  getPageGenerationDate: jest.fn(),
}));

getAPIConfig.mockImplementation(() => ({ BUILD_NUMBER: '9711' }));

describe('Footer component', () => {
  it('Footer component renders correctly without props', () => {
    isClient.mockImplementation(() => false);
    const component = shallow(<FooterVanilla />);
    expect(component).toMatchSnapshot();
  });

  it('Footer component renders correctly with props', () => {
    isClient.mockImplementation(() => true);
    const props = {
      className: 'footer-class',
    };
    const component = shallow(<FooterVanilla {...props} />);
    expect(component).toMatchSnapshot();
    expect(component.find('.footer-class')).toHaveLength(4);
  });

  it('should call subscriptionPopUpOnPageLoad for home and isRegisteredUserCallDone', () => {
    const props = {
      className: 'footer-class2',
    };
    const component = shallow(<FooterVanilla {...props} />);
    component.setProps({ pageName: 'home', isRegisteredUserCallDone: true });
  });

  it('should not call subscriptionPopUpOnPageLoad for other page', () => {
    const props = {
      className: 'footer-class1',
    };
    const component = shallow(<FooterVanilla {...props} />);

    component.setProps({ pageName: 'test', isRegisteredUserCallDone: true });
  });
});
