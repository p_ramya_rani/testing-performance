// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import { Button, Col, Row } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getPageGenerationDate } from '@tcp/core/src/utils/utils.web';
import { getDeviceTier } from '@tcp/web/src/utils/device-tiering';
import { getAPIConfig, isClient, isCanada } from '@tcp/core/src/utils/utils';
import CountrySelector from '../../Header/molecules/CountrySelector';
import {
  FooterMiddleMobile,
  FooterMiddleDesktop,
  LegalLinks,
  Copyright,
  FooterTopCandidateA,
  FooterTopCandidateB,
} from '../molecules';
import style from '../Footer.style';

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showFooterTopCandidateB: false,
    };
  }

  componentDidMount() {
    // TODO: Need to change this when proper solution for A/B test come
    if (window.location.search.match('cand-b')) {
      this.setState({ showFooterTopCandidateB: true });
    } else {
      this.setState({ showFooterTopCandidateB: false });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  shouldPlccTakeOver = () => {
    const { plccOfferTakeOverABTestEnabled } = this.props;
    return plccOfferTakeOverABTestEnabled && !isCanada();
  };

  render() {
    const { props } = this;
    const {
      className,
      copyrightText,
      legalLinks,
      navLinks,
      getUserInfoAction,
      getOrderDetailAction,
      isLoggedIn,
      linkConfig,
      footerActionCreator,
      isNavigationFooter,
      defaultStore = {},
      isPlcc,
      isGuest,
      fullPageAuthEnabled,
    } = props;
    const { showFooterTopCandidateB } = this.state;
    const buildVersion = getAPIConfig().BUILD_NUMBER;
    return (
      <footer className={`${className} footer-global`}>
        <Row className="footer-candidate-wrapper" fullBleed={!isNavigationFooter}>
          {isNavigationFooter && (
            <Col
              colSize={{
                large: 12,
                medium: 8,
                small: 6,
              }}
            >
              <div className="divider hide-in-large-up" />
            </Col>
          )}
          {showFooterTopCandidateB ? (
            <FooterTopCandidateB isNavigationFooter={isNavigationFooter} {...props} />
          ) : (
            <FooterTopCandidateA isNavigationFooter={isNavigationFooter} {...props} />
          )}
        </Row>
        <Row className="footer-middle mobile" fullBleed>
          <FooterMiddleMobile
            className={className}
            navLinkItems={navLinks}
            isLoggedIn={isLoggedIn}
            linkConfig={linkConfig}
            footerActionCreator={footerActionCreator}
            defaultStore={defaultStore}
            isPlcc={isPlcc}
            isGuest={isGuest}
            fullPageAuthEnabled={fullPageAuthEnabled}
          />
        </Row>
        <Row className="content-wrapper" fullBleed>
          <Row className="footer-middle desktop">
            <FooterMiddleDesktop
              className={className}
              navLinks={navLinks}
              isLoggedIn={isLoggedIn}
              linkConfig={linkConfig}
              footerActionCreator={footerActionCreator}
              defaultStore={defaultStore}
              isPlcc={isPlcc}
              isGuest={isGuest}
              fullPageAuthEnabled={fullPageAuthEnabled}
            />
          </Row>
        </Row>
        <div className="footer-bottom">
          <Row className="content-wrapper">
            <Row className="fullbleed-mobile">
              <Col
                className="footer-bottom__slot--1 default-offset"
                colSize={{
                  large: 2,
                  medium: 8,
                  small: 6,
                }}
              >
                <Copyright>{copyrightText}</Copyright>
                <div className="poc-hide">
                  <Button onClick={getUserInfoAction}>Get Registered User Info</Button>
                  <Button onClick={getOrderDetailAction}>Get Order detail</Button>
                </div>
              </Col>
              <Col
                className="footer-bottom__slot--2 default-offset"
                colSize={{
                  large: 8,
                  medium: 8,
                  small: 6,
                }}
              >
                <LegalLinks links={legalLinks} className="footer-legel-links" />
              </Col>
              <Col
                className="footer-bottom__slot--3 default-offset"
                colSize={{
                  large: 2,
                  medium: 8,
                  small: 6,
                }}
              >
                <CountrySelector showInFooter />
              </Col>
            </Row>
            <Row fullBleed>
              <Col
                colSize={{
                  large: 12,
                  medium: 8,
                  small: 6,
                }}
              >
                <BodyCopy className="reference-id" fontSize="fs32">
                  {isClient() ? ` | ${getPageGenerationDate()}` : ''}
                  {buildVersion ? ` | ${buildVersion}` : ''}
                  {` | ${getDeviceTier()}`}
                </BodyCopy>
              </Col>
            </Row>
          </Row>
        </div>
      </footer>
    );
  }
}

Footer.propTypes = {
  className: PropTypes.string.isRequired,
  copyrightText: PropTypes.string,
  navLinks: PropTypes.shape({}),
  legalLinks: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string,
      title: PropTypes.text,
    })
  ),
  referenceID: PropTypes.string,
  getUserInfoAction: PropTypes.func.isRequired,
  openEmailSignUpModal: PropTypes.func,
  openSmsSignUpModal: PropTypes.func,
  getOrderDetailAction: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  linkConfig: PropTypes.shape({
    'track-order': PropTypes.func,
    favorites: PropTypes.func,
    'log-out': PropTypes.func,
    'login-account': PropTypes.func,
    'create-account': PropTypes.func,
  }).isRequired,
  footerActionCreator: PropTypes.func.isRequired,
  pageName: PropTypes.string,
  isNavigationFooter: PropTypes.bool,
  isLocationEnabledForGuest: PropTypes.bool,
  isLocationEnabledForLoggedInUser: PropTypes.bool,
  smsSignupLabels: PropTypes.shape({}).isRequired,
  emailSignupLabels: PropTypes.shape({}).isRequired,
  defaultStore: PropTypes.shape({}),
  emailSignup: PropTypes.shape({}),
  smsSignup: PropTypes.shape({}),
  isPlcc: PropTypes.bool,
  isGuest: PropTypes.bool,
  isEmailModalOpen: PropTypes.bool,
  isSMSModalOpen: PropTypes.bool,
  openPlccOfferModal: PropTypes.func,
  isRegisteredUserCallDone: PropTypes.bool,
  fullPageAuthEnabled: PropTypes.bool,
  plccOfferTakeOverABTestEnabled: PropTypes.bool.isRequired,
};

Footer.defaultProps = {
  copyrightText: '',
  legalLinks: [],
  navLinks: [],
  referenceID: '',
  isLoggedIn: false,
  pageName: '',
  openEmailSignUpModal: () => {},
  openSmsSignUpModal: () => {},
  isNavigationFooter: false,
  isLocationEnabledForGuest: true,
  isLocationEnabledForLoggedInUser: true,
  defaultStore: {},
  emailSignup: {},
  smsSignup: {},
  isPlcc: false,
  isGuest: false,
  isEmailModalOpen: false,
  isSMSModalOpen: false,
  openPlccOfferModal: () => {},
  isRegisteredUserCallDone: false,
  fullPageAuthEnabled: false,
};

export default errorBoundary(withStyles(Footer, style));
export { Footer as FooterVanilla };
