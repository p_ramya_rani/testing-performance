// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { arrayOf, bool, func, shape, string } from 'prop-types';
import {
  Button,
  RichText,
  Col,
  Row,
  BodyCopy,
  TextItems,
} from '@tcp/core/src/components/common/atoms';
import SMSTapToJoin from '@tcp/core/src/components/common/molecules/SMSTapToJoin';
import { reduxForm } from 'redux-form';
import { Grid } from '@tcp/core/src/components/common/molecules';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { formatPhoneNumber } from '@tcp/core/src/utils/formValidation/phoneNumber';
import { getLocator, isGymboree, isCanada, getViewportInfo, isServer } from '@tcp/core/src/utils';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import DownloadAppPromo from '@tcp/web/src/components/common/molecules/DownloadAppPromo';
import SocialMediaLinks from '../SocialMediaLinks';
/* TODO move to component itself */
import style from '../../Footer.style';

import FooterTopSignUpForm from '../FooterTopSignUpForm';

const emailSignupFieldName = 'signup';
const emailSignupSecondBrand = 'isEmailOptInSecondBrand';
const FooterTopEmailSignUpForm = reduxForm({
  form: 'FooterTopEmailSignUpForm', // a unique identifier for this form
  initialValues: {
    [emailSignupFieldName]: '',
    [emailSignupSecondBrand]: false,
  },
})(FooterTopSignUpForm);

const smsSignupFieldName = 'footerTopSmsSignup';
const textSignupSecondBrand = 'isTextOptInSecondBrand';
const FooterTopSmsSignUpForm = reduxForm({
  form: 'FooterTopSmsSignUpForm', // a unique identifier for this form
  initialValues: {
    [smsSignupFieldName]: '',
    [textSignupSecondBrand]: false,
  },
})(FooterTopSignUpForm);

class FooterTopCandidateA extends React.PureComponent {
  getColSize = (device, isGym) => {
    if (device === 'medium') return isGym ? 8 : 4;
    return null;
  };

  renderEmailSignupForm = () => {
    const {
      emailSignup,
      emailSignupLabels,
      validateSignupEmail,
      submitEmailSubscription,
      emailSubscription,
      openEmailSignUpModal,
      isNavigationFooter,
    } = this.props;
    return (
      <Col
        className="col-md-half-width"
        colSize={{
          large: 4,
          medium: 4,
          small: 6,
        }}
        ignoreGutter={{
          small: true,
        }}
      >
        <BodyCopy
          component="h2"
          fontWeight="black"
          fontSize="fs15"
          className="heading_text email-sign-up"
          dataLocator="email_promo_text"
        >
          <TextItems textItems={emailSignup.textItems} />
        </BodyCopy>
        <FooterTopEmailSignUpForm
          labels={emailSignupLabels}
          validateForm={validateSignupEmail}
          onFormSubmit={submitEmailSubscription}
          subscription={emailSubscription}
          openSuccessModal={openEmailSignUpModal}
          dataLocators={{
            submitButton: 'email_submit_btn',
            inputField: 'enter_email_text_field',
            errorDataLocator: 'email_error_message',
            checkBox_gym: 'check_box_gym_opt_in',
            checkBox_tcp: 'check_box_tcp_opt_in',
          }}
          fieldName={emailSignupFieldName}
          secondFieldName={emailSignupSecondBrand}
          isNavigationFooter={isNavigationFooter}
        />

        {emailSignup && emailSignup.richText && emailSignup.richText.text ? (
          <RichText richTextHtml={emailSignup.richText.text} />
        ) : null}

        <div>
          <div className="divider hide-in-medium-up" />
        </div>
      </Col>
    );
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      smsSignup,
      smsSignupLabels,
      connectWithUsLabel,
      links,
      referAFriendButtonLabels,
      referAFriend,
      validateSignupSmsPhoneNumber,
      submitSmsSubscription,
      smsSubscription,
      openSmsSignUpModal,
      isNavigationFooter,
    } = this.props;

    const isGym = isGymboree();
    const showSmsSignup = !(isGym && isCanada());
    const { isMobile } = isServer() ? { isMobile: false } : getViewportInfo();

    return (
      <Grid className="footer_top_candidate_a content-wrapper">
        <Row>
          {/* ------------ Email Sign Up starts here ----------------- */}
          {!isNavigationFooter && this.renderEmailSignupForm()}

          {/* ---------- Email Signup ends here ---------- */}

          {/* ---------- SMS Sign Up starts here ---------- */}
          {!isNavigationFooter && showSmsSignup && (
            <Col
              className="col-md-half-width"
              colSize={{
                large: 4,
                medium: 4,
                small: 6,
              }}
              ignoreGutter={{
                small: true,
              }}
            >
              {isMobile ? (
                <SMSTapToJoin smsFromPage="footer" isGym={isGym} />
              ) : (
                <>
                  <BodyCopy
                    component="h2"
                    fontWeight="black"
                    fontSize="fs15"
                    className="heading_text sms_sign_up"
                    dataLocator="sms_promo_text"
                  >
                    <TextItems textItems={smsSignup.textItems} />
                  </BodyCopy>
                  <FooterTopSmsSignUpForm
                    labels={smsSignupLabels}
                    fieldName={smsSignupFieldName}
                    fieldProps={{
                      normalize: formatPhoneNumber,
                    }}
                    validateForm={validateSignupSmsPhoneNumber}
                    onFormSubmit={submitSmsSubscription}
                    subscription={smsSubscription}
                    openSuccessModal={openSmsSignUpModal}
                    dataLocators={{
                      submitButton: 'sms_submit_btn',
                      inputField: 'sms_field',
                      errorDataLocator: 'sms_error_message',
                      checkBox_gym: 'sms_gym_opt_in',
                      checkBox_tcp: 'sms_tcp_opt_in',
                    }}
                    secondFieldName={textSignupSecondBrand}
                    isNavigationFooter={isNavigationFooter}
                  />
                </>
              )}

              {smsSignup && smsSignup.richText && smsSignup.richText.text ? (
                <RichText richTextHtml={smsSignup.richText.text} />
              ) : null}
            </Col>
          )}
          {!isNavigationFooter && <div className="divider hide-in-large-up" />}
          {/* ---------- SMS Signup ends here ------------ */}

          {/* ---------- Refer a friend start here-------- */}

          {!isNavigationFooter && (
            <Col
              className="refer_a_friend_desktop col-md-half-width"
              colSize={{
                large: 4,
                medium: isGym ? 0 : 4,
                small: 6,
              }}
              ignoreGutter={{
                small: true,
              }}
            >
              <Grid>
                <Row fullBleed className="flex-align-center">
                  <Col
                    className="col-md-half-width"
                    colSize={{
                      large: 7.1,
                      medium: 4,
                      small: 7,
                    }}
                    ignoreGutter={{
                      small: true,
                    }}
                  >
                    <BodyCopy
                      component="h2"
                      fontWeight="black"
                      fontSize="fs15"
                      className="heading_text refer-a-friend"
                    >
                      <TextItems textItems={referAFriend.textItems} />
                    </BodyCopy>
                  </Col>
                  <Col
                    colSize={{
                      large: 4.9,
                      medium: 4,
                      small: 6,
                    }}
                    ignoreGutter={{
                      small: false,
                    }}
                    className="candidate_a_inline_container_button col-md-half-width"
                  >
                    <ClickTracker
                      as={Button}
                      id={
                        isNavigationFooter
                          ? 'extole_zone_global_navigation_footer'
                          : 'extole_zone_global_footer'
                      }
                      buttonVariation="variable-width"
                      data-locator={getLocator('refer_friend')}
                      className="refer_a_friend_button"
                      clickData={{
                        customEvents: ['event80'],
                        pageShortName: 'content:referafriend  confirmation',
                        pageName: 'content:email confirmation',
                      }}
                    >
                      {referAFriendButtonLabels.text}
                    </ClickTracker>
                  </Col>
                </Row>
                <div className="divider hide-in-medium-down" />
                <Row fullBleed className="hide-in-medium-down">
                  <Col
                    className=""
                    colSize={{
                      large: 12,
                      medium: 12,
                      small: 12,
                    }}
                    ignoreGutter={{
                      small: true,
                    }}
                  >
                    {!isGym && <div className="divider hide-in-medium-up" />}
                    <div className="social_media_wrapper">
                      <SocialMediaLinks
                        connectWithUsLabel={connectWithUsLabel}
                        links={links}
                        className="footer_top_candidate_a_social_links"
                      />
                    </div>
                  </Col>
                </Row>
              </Grid>
            </Col>
          )}
          {/* ---------- Refer a friend ends here-------- */}

          {!isNavigationFooter && !isGym && <div className="divider hide-in-medium-up" />}

          {/* ---------- DownloadAppPromo starts here-------- */}

          <Col
            className=""
            colSize={{
              large: 0,
              medium: 0,
              small: 12,
            }}
            ignoreGutter={{
              small: true,
            }}
          >
            <DownloadAppPromo isFooter={!isNavigationFooter} />
          </Col>

          <Col
            className="hide-in-large-up refer_a_frient_last_colm col-md-half-width"
            colSize={{
              large: 0,
              medium: this.getColSize('medium', isGym),
              small: 12,
            }}
            ignoreGutter={{
              small: true,
            }}
          >
            <SocialMediaLinks
              connectWithUsLabel={connectWithUsLabel}
              links={links}
              className="social-media-links"
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

FooterTopCandidateA.propTypes = {
  emailSignup: shape({
    richText: { text: string },
    textItems: arrayOf({}),
  }),
  smsSignup: shape({
    richText: { text: string },
    textItems: arrayOf({}),
  }),
  emailSignupLabels: shape({
    lbl_SignUp_placeholderText: string,
    lbl_SignUp_validationErrorLabel: string,
    lbl_SignUp_termsTextLabel: string,
    lbl_SignUp_submitButtonLabel: string,
    lbl_SignUp_gymSignUpLabel: string,
    lbl_SignUp_tcpSignUpLabel: string,
  }),
  smsSignupLabels: shape({
    lbl_SignUp_placeholderText: string,
    lbl_SignUp_validationErrorLabel: string,
    lbl_SignUp_termsTextLabel: string,
    lbl_SignUp_submitButtonLabel: string,
  }),
  connectWithUsLabel: string.isRequired,
  links: shape([]).isRequired,
  referAFriend: shape({}),
  referAFriendButtonLabels: shape({
    title: string,
    text: string,
  }),
  validateSignupEmail: func,
  validateSignupSmsPhoneNumber: func,
  submitEmailSubscription: func,
  submitSmsSubscription: func,
  openEmailSignUpModal: func,
  openSmsSignUpModal: func,
  emailSubscription: shape({}),
  smsSubscription: shape({}),
  isNavigationFooter: bool,
};

FooterTopCandidateA.defaultProps = {
  emailSignup: {
    text: '',
    title: '',
  },
  smsSignup: {
    text: '',
    title: '',
  },
  referAFriendButtonLabels: {
    title: '',
    text: '',
  },
  emailSignupLabels: {
    lbl_SignUp_placeholderText: '',
    lbl_SignUp_validationErrorLabel: '',
    lbl_SignUp_termsTextLabel: '',
    lbl_SignUp_submitButtonLabel: '',
    lbl_SignUp_gymSignUpLabel: '',
    lbl_SignUp_tcpSignUpLabel: '',
  },
  smsSignupLabels: {
    lbl_SignUp_placeholderText: '',
    lbl_SignUp_validationErrorLabel: '',
    lbl_SignUp_termsTextLabel: '',
    lbl_SignUp_submitButtonLabel: '',
  },
  referAFriend: {
    title: '',
    text: '',
  },
  validateSignupEmail: () => Promise.resolve(),
  validateSignupSmsPhoneNumber: () => Promise.resolve(),
  submitEmailSubscription: () => {},
  submitSmsSubscription: () => {},
  openEmailSignUpModal: () => {},
  openSmsSignUpModal: () => {},
  emailSubscription: {},
  smsSubscription: {},
  isNavigationFooter: false,
};

export default withStyles(FooterTopCandidateA, style);

export { FooterTopCandidateA as FooterTopCandidateAVanilla };
