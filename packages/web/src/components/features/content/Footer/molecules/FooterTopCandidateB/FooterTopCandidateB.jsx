// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';

import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Button, Col, Row, TextItems, BodyCopy } from '@tcp/core/src/components/common/atoms';
import { getLocator, isGymboree } from '@tcp/core/src/utils';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import DownloadAppPromo from '@tcp/web/src/components/common/molecules/DownloadAppPromo';
import SocialMediaLinks from '../SocialMediaLinks';

/* TODO move to component itself */
import style from '../../Footer.style';

// eslint-disable-next-line sonarjs/cognitive-complexity
const FooterTopCandidateB = (props) => {
  const {
    referAFriend,
    connectWithUsLabel,
    links,
    openEmailSignUpModal,
    openSmsSignUpModal,
    emailSignup,
    smsSignup,
    isNavigationFooter,
  } = props;

  const getColSizeForSocialMediaLinks = (device) => {
    if (device === 'large') {
      return isGymboree() ? 4 : 3;
    }
    if (device === 'medium') {
      return isGymboree() ? 8 : 4;
    }
    return 6;
  };

  return (
    <div className="footer-top content-wrapper">
      <Row>
        {!isNavigationFooter && (
          <Col
            className="footer-top__slots col-md-half-width"
            colSize={{
              large: isGymboree() ? 4 : 3,
              medium: 4,
              small: 6,
            }}
            ignoreGutter={{
              small: true,
            }}
          >
            <Button
              customStyle="shadow-button"
              title={emailSignup.title}
              onClick={openEmailSignUpModal}
              dataLocator="footer_email_signup_btn"
              className="candidate-b_buttons"
            >
              <BodyCopy component="div" fontWeight="black" fontSize="fs15" className="heading_text">
                <TextItems textItems={emailSignup.textItems} />
              </BodyCopy>
            </Button>
          </Col>
        )}
        {!isNavigationFooter && (
          <Col
            className="footer-top__slots col-md-half-width"
            colSize={{
              large: isGymboree() ? 4 : 3,
              medium: 4,
              small: 6,
            }}
            ignoreGutter={{
              small: true,
              medium: true,
            }}
          >
            <Button
              dataLocator="footer_sms_signup_btn"
              customStyle="shadow-button"
              title={smsSignup.title}
              onClick={openSmsSignUpModal}
              className="candidate-b_buttons"
            >
              <BodyCopy component="div" fontWeight="black" fontSize="fs15" className="heading_text">
                <TextItems textItems={smsSignup.textItems} />
              </BodyCopy>
            </Button>
          </Col>
        )}
        {!isNavigationFooter && (
          <Col
            className="footer-top__slots col-md-half-width"
            colSize={{
              large: 3,
              medium: 4,
              small: 6,
            }}
            ignoreGutter={{
              small: true,
            }}
          >
            <span
              id={
                isNavigationFooter
                  ? 'extole_zone_global_navigation_footer'
                  : 'extole_zone_global_footer'
              }
              title={referAFriend.title}
            >
              <ClickTracker
                as={Button}
                customStyle="shadow-button"
                data-locator={getLocator('refer_friend')}
                className="candidate-b_buttons"
                clickData={{
                  customEvents: ['event80'],
                  pageShortName: 'content:referafriend  confirmation',
                  pageName: 'content:email confirmation',
                }}
              >
                <BodyCopy
                  component="div"
                  fontWeight="black"
                  fontSize="fs15"
                  className="heading_text"
                >
                  <TextItems textItems={referAFriend.textItems} />
                </BodyCopy>
              </ClickTracker>
            </span>
          </Col>
        )}
        {/* ---------- DownloadAppPromo starts here-------- */}
        <Col
          className=""
          colSize={{
            large: 0,
            medium: 0,
            small: 12,
          }}
          ignoreGutter={{
            small: true,
          }}
        >
          <DownloadAppPromo isFooter={!isNavigationFooter} />
        </Col>
        <Col
          className="footer-top__slot--2 col-md-half-width"
          colSize={{
            large: getColSizeForSocialMediaLinks('large'),
            medium: getColSizeForSocialMediaLinks('medium'),
            small: getColSizeForSocialMediaLinks('small'),
          }}
        >
          <SocialMediaLinks
            connectWithUsLabel={connectWithUsLabel}
            links={links}
            className={isGymboree() ? 'footer_top_candidate_a_social_links' : 'social-media-links'}
          />
        </Col>
      </Row>
    </div>
  );
};

FooterTopCandidateB.propTypes = {
  navLinks: PropTypes.shape({}),
  connectWithUsLabel: PropTypes.string.isRequired,
  links: PropTypes.shape([]).isRequired,
  emailSignup: PropTypes.shape({
    title: PropTypes.string,
    text: PropTypes.string,
    textItems: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  smsSignup: PropTypes.shape({
    title: PropTypes.string,
    text: PropTypes.string,
    textItems: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  referAFriend: PropTypes.shape({
    title: PropTypes.string,
    text: PropTypes.string,
    textItems: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  openEmailSignUpModal: PropTypes.func,
  openSmsSignUpModal: PropTypes.func,
  isNavigationFooter: PropTypes.bool,
};

FooterTopCandidateB.defaultProps = {
  navLinks: [],
  emailSignup: {},
  smsSignup: {},
  referAFriend: {},
  openEmailSignUpModal: () => {},
  openSmsSignUpModal: () => {},
  isNavigationFooter: false,
};

export default withStyles(FooterTopCandidateB, style);
export { FooterTopCandidateB as FooterTopCandidateBVanilla };
