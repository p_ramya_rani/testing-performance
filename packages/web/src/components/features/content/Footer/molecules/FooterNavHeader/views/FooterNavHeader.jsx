// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  getMPRImageClickPayload,
  getPLCCImageClickPayload,
  setLoyaltyLocation,
} from '@tcp/core/src/constants/analytics';
import { Anchor, BodyCopy, DamImage } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { configureInternalNavigationFromCMSUrl, isCanada } from '@tcp/core/src/utils';
import { getUserType } from '@tcp/core/src/components/features/browse/ApplyCardPage/utils/utility';
import { setProp } from '@tcp/core/src/analytics/utils';
import userConstants from '@tcp/core/src/components/features/CnC/LoyaltyBanner/molecules/LoyaltyFooterSection/loyaltyFooterSection.constants';
import styles from '../FooterNavHeader.style';
import ClickTracker from '../../../../../../common/atoms/ClickTracker';

const FooterNavHeader = ({
  className,
  titleText,
  titleObj,
  ariaLabel,
  headerAsImage,
  isSubHeader,
  colNum,
  isPlcc,
  isGuest,
}) => {
  const formattedSiteId = isCanada() ? '-CA' : '';
  const globalFooterText = 'global-footer';

  const getLoyaltyPayload = () => {
    if (titleText && titleText.toLowerCase() === 'my place rewards')
      return getMPRImageClickPayload(globalFooterText);
    if (titleText && titleText.toLowerCase() === 'my place rewards credit card')
      return getPLCCImageClickPayload(globalFooterText);

    return null;
  };

  const setAnalyticsProps = () => {
    setLoyaltyLocation(globalFooterText);

    let eventType = '';
    const userType = getUserType(isGuest, isPlcc);
    if (userType === userConstants.PLCC_USER) {
      eventType = 'eVar103';
    } else if (userType === userConstants.MPR_USER) {
      eventType = 'eVar102';
    } else {
      eventType = 'eVar105';
    }
    setProp(eventType, `${userType.toUpperCase()}-Image`);
  };
  if (!headerAsImage) {
    return (
      <BodyCopy
        className={!isSubHeader ? className : `${className} subHeader`}
        aria-label={ariaLabel}
        data-index={colNum}
        data-locator={`col_heading_${colNum}`}
        component="p"
        fontFamily="secondary"
        fontWeight="semibold"
        fontSize="fs16"
        color="text.primary"
      >
        {titleText}
      </BodyCopy>
    );
  }
  return (
    <ClickTracker
      as={Anchor}
      to={configureInternalNavigationFromCMSUrl(titleObj.url)}
      asPath={titleObj.url}
      aria-label={titleText}
      className={`${className} img-link`}
      dataLocator={`col_heading_${colNum}`}
      onClick={() => setAnalyticsProps()}
      clickData={{
        ...getLoyaltyPayload(),
        eventName: 'Naviation',
        pageNavigationText: `${globalFooterText}-${titleText}`,
      }}
    >
      <DamImage
        imgData={{
          url: `${titleObj.class}${formattedSiteId}.png`,
          alt: titleObj.image_alt || titleObj.text,
        }}
        imgConfigs={['w_auto', 'w_auto', 'w_auto']}
        isHomePage
        lowQualityEnabled
      />
    </ClickTracker>
  );
};

FooterNavHeader.propTypes = {
  className: PropTypes.string.isRequired,
  ariaLabel: PropTypes.string.isRequired,
  headerAsImage: PropTypes.bool.isRequired,
  colNum: PropTypes.number.isRequired,
  titleText: PropTypes.string.isRequired,
  titleObj: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  isSubHeader: PropTypes.bool.isRequired,
  isPlcc: PropTypes.bool,
  isGuest: PropTypes.bool,
};

FooterNavHeader.defaultProps = {
  isPlcc: false,
  isGuest: false,
};

export default withStyles(FooterNavHeader, styles);

export { FooterNavHeader as FooterNavHeaderVanilla };
