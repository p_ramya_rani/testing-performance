// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import React, { Fragment } from 'react';
import Col from '@tcp/core/src/components/common/atoms/Col';
import PropTypes from 'prop-types';
import { isCanada } from '@tcp/core/src/utils';
import FooterNavLinks from '../../FooterNavLinks';

const renderFooterNavLinks = (
  navLink,
  className,
  colNum,
  linkConfig,
  footerActionCreator,
  {
    isSubHeader,
    isLoggedIn,
    headerAsImage,
    defaultStore,
    isPlcc,
    isGuest,
    fullPageAuthEnabled,
  } = {}
) => {
  if (!navLink) {
    return null;
  }

  return (
    <FooterNavLinks
      className={className}
      isSubHeader={isSubHeader}
      isLoggedIn={isLoggedIn}
      headerAsImage={headerAsImage}
      navLinkItems={{
        header: navLink.header,
        links: navLink.links,
      }}
      colNum={colNum}
      linkConfig={linkConfig}
      footerActionCreator={footerActionCreator}
      defaultStore={defaultStore}
      isPlcc={isPlcc}
      isGuest={isGuest}
      fullPageAuthEnabled={fullPageAuthEnabled}
    />
  );
};

const FooterMiddleDesktop = ({
  navLinks,
  className,
  isLoggedIn,
  linkConfig,
  footerActionCreator,
  defaultStore = {},
  isPlcc,
  isGuest,
  fullPageAuthEnabled,
}) => {
  let numberOfNavLinkCols = navLinks.length;

  const navLinkColumns = [];

  for (let i = 2; i < navLinks.length; i += 1) {
    if (navLinks[i + 1] && navLinks[i + 1].isSubHeader) {
      // For each subheader, it is going to be one col less
      // as it will adjust in the previous column bottom itself.
      // Hence, reducing the number of nav link columns.
      numberOfNavLinkCols -= 1;
      navLinkColumns.push(
        <Col
          colSize={{
            large: 2,
            medium: 8,
            small: 6,
          }}
        >
          {renderFooterNavLinks(navLinks[i], className, i, linkConfig, footerActionCreator, {
            defaultStore,
            isPlcc,
            isGuest,
            fullPageAuthEnabled,
          })}
          {renderFooterNavLinks(
            navLinks[i + 1],
            className,
            i + 1,
            linkConfig,
            footerActionCreator,
            {
              isSubHeader: true,
              isPlcc,
              isGuest,
              fullPageAuthEnabled,
            }
          )}
        </Col>
      );
      i += 1;
    } else {
      navLinkColumns.push(
        <Col
          colSize={{
            large: 2,
            medium: 8,
            small: 6,
          }}
        >
          {renderFooterNavLinks(navLinks[i], className, i, linkConfig, footerActionCreator, {
            isSubHeader: false,
            isLoggedIn,
            defaultStore,
            isPlcc,
            isGuest,
            fullPageAuthEnabled,
          })}
        </Col>
      );
    }
  }

  return (
    <Fragment>
      <Col
        colSize={{
          large: 2,
          medium: 8,
          small: 6,
        }}
      >
        {renderFooterNavLinks(navLinks[0], className, 0, linkConfig, footerActionCreator, {
          isSubHeader: false,
          isLoggedIn,
          headerAsImage: true,
          defaultStore,
          isPlcc,
          isGuest,
          fullPageAuthEnabled,
        })}
      </Col>
      {!isCanada() ? (
        <Col
          colSize={{
            large: 2,
            medium: 8,
            small: 6,
          }}
        >
          {renderFooterNavLinks(navLinks[1], className, 1, linkConfig, footerActionCreator, {
            isSubHeader: false,
            isLoggedIn,
            headerAsImage: true,
            defaultStore,
            isPlcc,
            isGuest,
            fullPageAuthEnabled,
          })}
        </Col>
      ) : null}
      {numberOfNavLinkCols <= 5 ? (
        <Col
          className="divider"
          colSize={{
            large: 1,
            medium: 8,
            small: 6,
          }}
        />
      ) : (
        ''
      )}
      {navLinkColumns}
    </Fragment>
  );
};

FooterMiddleDesktop.propTypes = {
  navLinks: PropTypes.shape([]).isRequired,
  className: PropTypes.string.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  linkConfig: PropTypes.shape({
    'track-order': PropTypes.func,
    favorites: PropTypes.func,
    'log-out': PropTypes.func,
    'login-account': PropTypes.func,
    'create-account': PropTypes.func,
  }).isRequired,
  footerActionCreator: PropTypes.func.isRequired,
  defaultStore: PropTypes.shape({}),
  isPlcc: PropTypes.bool,
  isGuest: PropTypes.bool,
  fullPageAuthEnabled: PropTypes.bool,
};

FooterMiddleDesktop.defaultProps = {
  defaultStore: {},
  isPlcc: false,
  isGuest: false,
  fullPageAuthEnabled: false,
};

export default FooterMiddleDesktop;
