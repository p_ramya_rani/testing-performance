// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { arrayOf, bool, func, number, shape, string } from 'prop-types';

import FooterNavLinksList from '../../FooterNavLinksList';
import FooterNavHeader from '../../FooterNavHeader';

const FooterNavLinks = ({
  className,
  navLinkItems,
  updateAccordionState,
  headerAsImage,
  isSubHeader,
  colNum,
  isLoggedIn,
  linkConfig,
  footerActionCreator,
  defaultStore = {},
  isPlcc,
  isGuest,
  fullPageAuthEnabled,
}) => {
  return (
    <div className={`${className} container-nav-link`} key={navLinkItems.id} data-index={colNum}>
      <FooterNavHeader
        headerAsImage={headerAsImage}
        titleText={navLinkItems.header.text}
        titleObj={navLinkItems.header}
        updateAccordionState={updateAccordionState}
        isSubHeader={isSubHeader}
        colNum={colNum}
        isPlcc={isPlcc}
        isGuest={isGuest}
      />
      <FooterNavLinksList
        listArray={navLinkItems.links}
        colNum={colNum}
        isLoggedIn={isLoggedIn}
        linkConfig={linkConfig}
        footerActionCreator={footerActionCreator}
        defaultStore={defaultStore}
        fullPageAuthEnabled={fullPageAuthEnabled}
      />
    </div>
  );
};

FooterNavLinks.propTypes = {
  className: string.isRequired,
  navLinkItems: shape({
    header: shape({ text: string }),
    id: string,
    links: arrayOf({}),
  }).isRequired,
  updateAccordionState: func.isRequired,
  headerAsImage: bool.isRequired,
  isSubHeader: bool.isRequired,
  colNum: number.isRequired,
  isLoggedIn: bool,
  linkConfig: shape({
    'track-order': func,
    favorites: func,
    'log-out': func,
    'login-account': func,
    'create-account': func,
  }).isRequired,
  footerActionCreator: func.isRequired,
  defaultStore: shape({}),
  isPlcc: bool,
  isGuest: bool,
  fullPageAuthEnabled: bool,
};

FooterNavLinks.defaultProps = {
  isLoggedIn: false,
  defaultStore: {},
  isPlcc: false,
  isGuest: false,
  fullPageAuthEnabled: false,
};

export default FooterNavLinks;
