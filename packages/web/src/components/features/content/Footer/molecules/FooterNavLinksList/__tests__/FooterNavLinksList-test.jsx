// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { FooterNavLinksListVanilla } from '../views/FooterNavLinksList';

describe('FooterNavLinksList component', () => {
  const props = {
    id: {},
    className: 'footer-nav-header1',
    titleText: 'Title',
    ariaLabel: 'ABCD',
    isSubHeader: false,
    headerAsImage: false,
    updateAccordionState: () => {},
    listArray: [
      {
        title: 'gift card',
        url: '',
        text: '',
        action: '',
      },
      {
        title: 'gift card',
        url: '',
        text: '',
        action: '',
      },
      {
        title: 'gift card',
        url: '',
        text: '',
        action: '',
      },
    ],
    linkConfig: {},
    colNum: 1,
    isLoggedIn: true,
  };
  it('renders default footer correctly', () => {
    const component = shallow(<FooterNavLinksListVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should call setLoyaltyLocation', () => {
    const setLoyaltyLocation = jest.fn();
    const component = shallow(<FooterNavLinksListVanilla {...props} />);
    const listArrayNew = [
      {
        title: 'favorites',
        url: '/favorites',
        text: '',
        action: 'favorites',
      },
      {
        title: 'redeem-rewards',
        url: '/redeem-rewards',
        text: '',
        action: 'redeem-rewards',
      },
      {
        title: 'track-order',
        url: '/track-order',
        text: '',
        action: 'track-order',
      },
    ];
    component.setProps({ listArray: listArrayNew });
    component.find(ClickTracker).at(0).simulate('click');
    setLoyaltyLocation.mockImplementation();
    expect(window.loyaltyLocation).toBe('global-footer');
  });
  it('should call setLoyaltyLocation', () => {
    const setLoyaltyLocation = jest.fn();
    const component = shallow(<FooterNavLinksListVanilla {...props} />);
    component.find(ClickTracker).at(0).simulate('click');
    setLoyaltyLocation.mockImplementation();
    expect(window.loyaltyLocation).toBe('global-footer');
  });
});
