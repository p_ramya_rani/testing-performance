// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { FooterTopCandidateAVanilla } from '../FooterTopCandidateA';

describe('FooterTopCandidateAVanilla component', () => {
  it('renders correctly', () => {
    const component = shallow(<FooterTopCandidateAVanilla />);
    expect(component).toMatchSnapshot();
  });
});
