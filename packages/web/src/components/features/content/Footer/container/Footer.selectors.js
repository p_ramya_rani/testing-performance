// 9fbef606107a605d69c0edbcd8029e5d 
import isEmpty from 'lodash/isEmpty';
import { getABtestFromState, parseBoolean } from '@tcp/core/src/utils';

export const isLocationEnabledForGuest = state => {
  return state.session.siteDetails.IS_LOCATION_ENABLED_FOR_GUEST;
};

export const isLocationEnabledForLoggedInUser = state => {
  return state.session.siteDetails.IS_LOCATION_ENABLED_FOR_LOGGED_IN_USER;
};

export const getPlccOfferTakeOverABTest = state => {
  const getAbtests = getABtestFromState(state.AbTest);
  return !isEmpty(getAbtests) ? parseBoolean(getAbtests.plccOfferTakeOverABTest) : null;
};

export const getPlccModalPersistentCookieExpiry = state => {
  return state &&
    state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.PLCC_SIGNUP_MODAL_PERSISTENT_COOKIE_EXPIRY !== undefined
    ? parseInt(state.session.siteDetails.PLCC_SIGNUP_MODAL_PERSISTENT_COOKIE_EXPIRY, 10)
    : 120;
};
