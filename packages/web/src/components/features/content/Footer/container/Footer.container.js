// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import {
  getUserInfoPOC,
  getOrderDetail,
} from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.actions';
import { loginModalOpenState } from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.selectors';
import { submitEmailSignup } from '@tcp/core/src/components/common/organisms/EmailSignupForm/container/EmailSignupForm.actions';
import { toggleEmailSignupModal } from '@tcp/web/src/components/common/molecules/EmailSignupModal/container/EmailSignupModal.actions';
import { togglePlccOfferModal } from '@tcp/web/src/components/common/molecules/PlccOfferModal/container/PlccOfferModal.actions';
import {
  submitSmsSignup,
  clearSmsSignupForm,
} from '@tcp/core/src/components/common/organisms/SmsSignupForm/container/SmsSignupForm.actions';
import { toggleSmsSignupModal } from '@tcp/web/src/components/common/molecules/SmsSignupModal/container/SmsSignupModal.actions';
import {
  getUserLoggedInState,
  isPlccUser,
  getIsRegisteredUserCallDone,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import { isGuest } from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import { validatePhoneNumber } from '@tcp/core/src/utils/formValidation/phoneNumber';
import { setTrackOrderModalMountedState } from '@tcp/core/src/components/features/account/TrackOrder/container/TrackOrder.actions';
import { closeNavigationDrawer } from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import { getDefaultStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import LinkConfig from '../../../../../config/footerLinkActionMapping.config';
import FooterView from '../views';
import {
  isLocationEnabledForGuest,
  isLocationEnabledForLoggedInUser,
  getPlccModalPersistentCookieExpiry,
  getPlccOfferTakeOverABTest,
} from './Footer.selectors';

const mapStateToProps = (state) => {
  const { Footer } = state;
  const {
    global: {
      footerDefault: {
        lbl_footerDefault_connectwithus: connectWithUsLabel,
        lbl_footerDefault_referenceId: referenceID,
      } = {},
      emailSignup: emailSignupLabels,
      smsSignup: smsSignupLabels,
      referAFriend: referAFriendButtonLabels,
    } = {},
  } = state.Labels;
  const {
    EmailSignUp: {
      EmailSignupModalReducer: { isModalOpen: isEmailModalOpen } = {},
      EmailSignupFormReducer: { subscription: emailSubscription } = {},
    } = {},
    SmsSignUp: {
      SmsSignupModalReducer: { isModalOpen: isSMSModalOpen },
      SmsSignupFormReducer: { subscription: smsSubscription } = {},
    } = {},
    PlccOffer: { isModalOpen: isPlccOfferModalOpen } = {},
  } = state;

  return {
    legalLinks: Footer.legalLinks,
    navLinks: Footer.navLinks,
    connectWithUsLabel,
    links: Footer.socialLinks,
    smsSubscription,
    emailSubscription,
    emailSignup: Footer.emailSignupBtn,
    smsSignup: Footer.smsSignupBtn,
    referAFriend: Footer.referFriendBtn,
    referAFriendButtonLabels,
    copyrightText: Footer.copyrightText,
    referenceID,
    emailSignupLabels,
    smsSignupLabels,
    loginModalMountedState: loginModalOpenState(state),
    isLoggedIn: getUserLoggedInState(state),
    linkConfig: LinkConfig,
    isLocationEnabledForLoggedInUser: isLocationEnabledForLoggedInUser(state),
    isLocationEnabledForGuest: isLocationEnabledForGuest(state),
    defaultStore: getDefaultStore(state),
    isGuest: isGuest(state),
    isPlcc: isPlccUser(state),
    plccOfferTakeOverABTestEnabled: getPlccOfferTakeOverABTest(state),
    plccModalPersistentCookieExpiry: getPlccModalPersistentCookieExpiry(state),
    isRegisteredUserCallDone: getIsRegisteredUserCallDone(state),
    fullPageAuthEnabled: getEnableFullPageAuth(state),
    isEmailModalOpen,
    isSMSModalOpen,
    isPlccOfferModalOpen,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    footerActionCreator: (dispatchFn, payload) => {
      dispatch(dispatchFn(payload));
      dispatch(closeNavigationDrawer());
    },
    getUserInfoAction: () => {
      dispatch(getUserInfoPOC());
    },
    getOrderDetailAction: () => {
      dispatch(getOrderDetail());
    },
    openEmailSignUpModal: (value) => {
      dispatch(toggleEmailSignupModal({ isModalOpen: value }));
    },
    openPlccOfferModal: () => {
      dispatch(togglePlccOfferModal({ isModalOpen: true }));
    },
    openSmsSignUpModal: () => {
      dispatch(toggleSmsSignupModal({ isModalOpen: true }));
    },
    submitEmailSubscription: (payload, status) => {
      dispatch(submitEmailSignup(payload, status));
    },
    submitSmsSubscription: (payload) => {
      dispatch(clearSmsSignupForm());
      dispatch(submitSmsSignup(payload));
    },
    validateSignupEmail: (values) => {
      return emailSignupAbstractor.verifyEmail(values.signup);
    },
    validateSignupSmsPhoneNumber: (values) => {
      return validatePhoneNumber(values.footerTopSmsSignup)
        ? Promise.resolve({})
        : Promise.reject();
    },
    openTrackOrder: (payload) => dispatch(setTrackOrderModalMountedState(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FooterView);
