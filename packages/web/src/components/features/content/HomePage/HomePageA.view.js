import PropTypes from 'prop-types';

export const HomePageWrapperDefaultProps = {
  storeId: '',
  currency: 'USD',
};

export const HomePageWrapperPropTypes = {
  children: PropTypes.element.isRequired,
  openCountrySelectorModal: PropTypes.func.isRequired,
  router: PropTypes.element.isRequired,
  setCampaignId: PropTypes.func.isRequired,
  trackHomepageView: PropTypes.func.isRequired,
  isRegisteredUserCallDone: PropTypes.bool.isRequired,
  setClickAnalyticsData: PropTypes.bool.isRequired,
  storeId: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  currency: PropTypes.string,
  getPageLayout: PropTypes.func.isRequired,
  pageName: PropTypes.string.isRequired,
  plccOfferTakeOverABTestEnabled: PropTypes.bool.isRequired,
  openEmailSignUpModal: PropTypes.bool.isRequired,
  openSmsSignUpModal: PropTypes.bool.isRequired,
  openPlccOfferModal: PropTypes.bool.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  plccModalPersistentCookieExpiry: PropTypes.bool.isRequired,
};

export const HomePageViewPropTypes = {
  name: PropTypes.string,
  slots: PropTypes.arrayOf(PropTypes.object).isRequired,
  openCountrySelectorModal: PropTypes.func.isRequired,
  setCampaignId: PropTypes.func.isRequired,
  trackHomepageView: PropTypes.func.isRequired,
  isRegisteredUserCallDone: PropTypes.bool.isRequired,
  getPageLayout: PropTypes.func.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

export const HomePageViewDefaultProps = {
  name: null,
};
