// 9fbef606107a605d69c0edbcd8029e5d
import { isMobileApp, parseBoolean } from '@tcp/core/src/utils';
import {
  hpModulesData,
  getEnableFullPageAuth,
  getMinibagOnHoverEnabled,
} from '../HomePage.selectors';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  getAPIConfig: jest.fn(),
  parseBoolean: jest.fn().mockImplementation(() => true),
  getABtestFromState: jest.fn().mockImplementation(() => {
    return {
      lazyloadModules: true,
      removeModules: [],
    };
  }),
}));

const stateMock = {
  Layouts: {
    home: {
      slots: [
        {
          name: 'slot_1',
          moduleName: 'moduleA',
          contentId: 'a123',
        },
        {
          name: 'slot_2',
          moduleName: 'moduleB',
          contentId: 'b123',
        },
      ],
    },
  },
  Modules: {
    a123: { a: 'ModuleAContent' },
    b123: { b: 'ModuleBContent' },
  },
  Labels: { global: { accessibility: { label: 'Click Me' } } },
};

describe('Homepage selectors', () => {
  it('should extract slots modules data', () => {
    expect(hpModulesData(stateMock)).toMatchObject([
      {
        accessibility: { label: 'Click Me' },
        contentId: 'a123',
        data: { a: 'ModuleAContent' },
        moduleName: 'moduleA',
        name: 'slot_1',
      },
      {
        accessibility: { label: 'Click Me' },
        contentId: 'b123',
        data: { b: 'ModuleBContent' },
        moduleName: 'moduleB',
        name: 'slot_2',
      },
    ]);
  });

  it('should return false for mobile from getEnableFullPageAuth', () => {
    isMobileApp.mockImplementation(() => true);
    expect(getEnableFullPageAuth({})).toBeFalsy();
  });

  it('should return true for web from getEnableFullPageAuth', () => {
    isMobileApp.mockImplementation(() => false);
    expect(
      getEnableFullPageAuth({
        session: { siteDetails: { ENABLE_FULL_PAGE_LOGIN_REGISTRATION: 1 } },
      })
    ).toBeTruthy();
  });

  it('#getMinibagOnHoverEnabled should return featureSwitch state', () => {
    const state = {
      session: {
        siteDetails: {
          MINIBAG_HOVER_ENABLED: 1,
        },
      },
    };
    expect(getMinibagOnHoverEnabled(state)).toEqual(
      parseBoolean(state.session.siteDetails.MINIBAG_HOVER_ENABLED)
    );
  });
});
