// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { getAPIConfig } from '@tcp/core/src/utils';
import { HomePageViewVanilla, HomePageWrapper } from '../views/HomePage.view';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  getImageFilePath: jest.fn(),
  isGymboree: jest.fn(),
  isCanada: jest.fn(),
}));

describe('HomePageViewVanilla', () => {
  let component;
  const getBootstrapData = jest.fn();

  beforeEach(() => {
    const props = {
      slot_1: { className: 'moduleD' },
      slot_2: { className: 'moduleH' },
      labels: { recentlyViewed: '' },
      getBootstrapData,
      appType: 'tcp',
      navigation: {
        getParam: () => false,
      },
      loadNavigationData: () => {},
      router: {
        components: {
          '/index': {
            props: {
              isServer: true,
            },
          },
        },
      },
    };
    component = shallow(<HomePageViewVanilla {...props} />);
  });

  it('should be defined', () => {
    getAPIConfig.mockImplementation(() => false);
    expect(HomePageViewVanilla).toBeDefined();
  });

  it('should render correctly', () => {
    getAPIConfig.mockImplementation(() => false);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly with CSR Enabled', () => {
    getAPIConfig.mockImplementation(() => true);
    expect(component).toMatchSnapshot();
  });
});

describe('HomePageWrapper', () => {
  it('should set showRegister to false for non register url', () => {
    getAPIConfig.mockImplementation(() => false);
    const props = {
      router: {
        query: {},
        components: {
          '/index': {
            props: {
              isServer: true,
            },
          },
        },
      },
    };
    const component = shallow(<HomePageWrapper {...props} />);
    expect(component.state('showRegister')).toBeFalsy();
  });

  it('should set showRegister to true for register url', () => {
    getAPIConfig.mockImplementation(() => false);
    const props1 = {
      router: {
        query: { target: 'register' },
        components: {
          '/index': {
            props: {
              isServer: true,
            },
          },
        },
      },
    };
    const component = shallow(<HomePageWrapper {...props1} />);
    expect(component.state('showRegister')).toBeTruthy();
  });

  it('should set showLogin to true for login url', () => {
    getAPIConfig.mockImplementation(() => false);
    const props1 = {
      router: {
        query: { target: 'login' },
        components: {
          '/index': {
            props: {
              isServer: true,
            },
          },
        },
      },
    };
    const component = shallow(<HomePageWrapper {...props1} />);
    expect(component.state('showLogin')).toBeTruthy();
  });
});
