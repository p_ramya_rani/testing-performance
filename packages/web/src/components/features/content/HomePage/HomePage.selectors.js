// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import isEmpty from 'lodash/isEmpty';
import { getLabelValue, parseBoolean, isMobileApp, getABtestFromState } from '@tcp/core/src/utils';

const allLayouts = (state) => state.Layouts;
const allModules = (state) => state.Modules;
const allLabels = (state) => state.Labels;
const allSeo = (state) => state.SEOData;
const allApiConfig = (state) => state.APIConfig;

export const getEnableFullPageAuth = (state) => {
  if (isMobileApp()) {
    return false;
  }
  return state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.ENABLE_FULL_PAGE_LOGIN_REGISTRATION
    ? parseBoolean(state.session.siteDetails.ENABLE_FULL_PAGE_LOGIN_REGISTRATION)
    : false;
};

export const getPlccOfferTakeOverABTest = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return !isEmpty(getAbtests) ? parseBoolean(getAbtests.plccOfferTakeOverABTest) : null;
};

export const getLazyloadModules = (state) => {
  const { lazyloadModules = true } = getABtestFromState(state.AbTest);
  return lazyloadModules;
};

export const getModulesToRemove = (state) => {
  const { removeModules = [] } = getABtestFromState(state.AbTest);
  return removeModules;
};

export const getPlccModalPersistentCookieExpiry = (state) => {
  return state &&
    state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.PLCC_SIGNUP_MODAL_PERSISTENT_COOKIE_EXPIRY !== undefined
    ? parseInt(state.session.siteDetails.PLCC_SIGNUP_MODAL_PERSISTENT_COOKIE_EXPIRY, 10)
    : 120;
};

export const getMinibagOnHoverEnabled = (state) => {
  if (isMobileApp()) {
    return false;
  }
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.MINIBAG_HOVER_ENABLED)
  );
};

export const hpLabels = createSelector(allLabels, (labels) => {
  return {
    recentlyViewed: getLabelValue(labels, 'lbl_recently_viewed', 'PDP_Sub', 'ProductDetailPage'),
  };
});

export const hpSeoData = createSelector(allSeo, (seoData) => seoData.home);

const globalLabels = createSelector(allLabels, (labels) => labels.global || {});

const accessibility = createSelector(globalLabels, (global) => global.accessibility);

const hpLayout = createSelector(allLayouts, allApiConfig, (Layouts = {}, apiConfig = {}) => {
  const keyIfAltHome =
    apiConfig.originalPath &&
    apiConfig.originalPath
      .split('/')
      .pop()
      .replace(/-([a-z])/g, (g) => {
        return g[1].toUpperCase();
      });
  return Layouts.home || Layouts[keyIfAltHome];
});
const hpSlots = createSelector(
  hpLayout,
  getModulesToRemove,
  (home, modulesToRemove = []) =>
    home &&
    home.slots &&
    home.slots.filter((slot) => modulesToRemove.indexOf(slot.contentId) === -1)
);

export const hpModulesData = createSelector(
  hpSlots,
  allModules,
  accessibility,
  (homepageSlots = [], Modules = {}, a11y = {}) => {
    return homepageSlots.map((slot) => {
      // Logic for accommodating two modules in one slot (Half width modules view)
      const { contentId: slotContent = '' } = slot;
      const contentIds = slotContent && slotContent.split(',');
      if (contentIds && contentIds.length > 1) {
        const response = {
          ...slot,
          accessibility: a11y,
          data: {
            slot: [],
          },
        };

        contentIds.forEach((contentId) => {
          response.data.slot.push(Modules[contentId]);
        });

        return response;
      }
      // Logic ends
      return {
        ...slot,
        accessibility: a11y,
        data: Modules[slot.contentId],
      };
    });
  }
);

export default {
  hpSeoData,
  hpLabels,
  hpModulesData,
  getEnableFullPageAuth,
};
