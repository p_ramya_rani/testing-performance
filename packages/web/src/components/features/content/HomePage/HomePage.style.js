import styled from 'styled-components';
import HP_NEW_LAYOUT from '@tcp/core/src/constants/hpNewLayout.constants';

const StyledPageSlotsWrapper = styled.div`
  padding: ${(props) =>
    props.isHpNewModuleDesignEnabled ? `0 ${HP_NEW_LAYOUT.BODY_PADDING}px` : 0};
`;

export default StyledPageSlotsWrapper;
