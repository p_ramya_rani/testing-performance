/* eslint-disable max-lines, extra-rules/no-commented-out-code, */
/* 9fbef606107a605d69c0edbcd8029e5d */
import { withRouter } from 'next/router';
import React from 'react';
import dynamic from 'next/dynamic';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import withRefWrapper from '@tcp/core/src/components/common/hoc/withRefWrapper';
import withHotfix from '@tcp/core/src/components/common/hoc/withHotfix';
import PageSlots from '@tcp/core/src/components/common/molecules/PageSlots';
import SeoCopy from '@tcp/core/src/components/features/browse/ProductListing/molecules/SeoCopy/views';
import { DynamicModule } from '@tcp/core/src/components/common/atoms';

import {
  getQueryParamsFromUrl,
  internalCampaignProductAnalyticsList,
  fireEnhancedEcomm,
  getAPIConfig,
  isTCP,
  isCanada,
} from '@tcp/core/src/utils';
import { setProp, productArr } from '@tcp/core/src/analytics/utils';
import CreateAccount from '@tcp/core/src/components/features/account/CreateAccount';
import LoginContainer from '@tcp/core/src/components/features/account/LoginPage';

import HOMEPAGE_CONSTANTS from '../HomePage.constants';
import { isTier1Device } from '../../../../../utils/device-tiering';
import {
  HomePageWrapperDefaultProps,
  HomePageWrapperPropTypes,
  HomePageViewPropTypes,
  HomePageViewDefaultProps,
} from '../HomePageA.view';
import StyledPageSlotsWrapper from '../HomePage.style';
// removed module k and L - TICKET - DRP-12474
const modules = {
  moduleA: dynamic(() =>
    import(
      /* webpackChunkName: "moduleA" */ '@tcp/core/src/components/common/molecules/ModuleA/view/ModuleA'
    )
  ),
  moduleB: dynamic(() =>
    import(
      /* webpackChunkName: "moduleB" */ '@tcp/core/src/components/common/molecules/ModuleB/view/ModuleB'
    )
  ),
  moduleD: dynamic(() =>
    import(
      /* webpackChunkName: "moduleD" */ '@tcp/core/src/components/common/molecules/ModuleD/view/ModuleD'
    )
  ),
  moduleJ: dynamic(() =>
    import(/* webpackChunkName: "moduleJ" */ '@tcp/core/src/components/common/molecules/ModuleJ')
  ),
  moduleM: dynamic(() =>
    import(
      /* webpackChunkName: "moduleM" */ '@tcp/core/src/components/common/molecules/ModuleM/views/ModuleM'
    )
  ),
  moduleN: dynamic(() =>
    import(
      /* webpackChunkName: "moduleN" */ '@tcp/core/src/components/common/molecules/ModuleN/views/ModuleN'
    )
  ),
  moduleQ: dynamic(() =>
    import(/* webpackChunkName: "moduleQ" */ '@tcp/core/src/components/common/molecules/ModuleQ')
  ),
  moduleX: dynamic(() =>
    import(
      /* webpackChunkName: "moduleX" */ '@tcp/core/src/components/common/molecules/ModuleX/ModuleX'
    )
  ),
  moduleT: dynamic(() =>
    import(
      /* webpackChunkName: "moduleT" */ '@tcp/core/src/components/common/molecules/ModuleT/views/ModuleT'
    )
  ),
  moduleL: dynamic(() =>
    import(
      /* webpackChunkName: "moduleL" */ '@tcp/core/src/components/common/molecules/ModuleL/views/ModuleL'
    )
  ),
  moduleDynamic: dynamic(() =>
    import(
      /* webpackChunkName: "moduleDynamic" */ '@tcp/core/src/components/common/molecules/ModuleDynamic'
    )
  ),
  module2columns: dynamic(() =>
    import(
      /* webpackChunkName: "module2columns" */ '@tcp/core/src/components/common/molecules/ModuleTwoCol/views/ModuleTwoCol'
    )
  ),
  moduleG: dynamic(() =>
    import(/* webpackChunkName: "moduleG" */ '@tcp/core/src/components/common/molecules/ModuleG')
  ),
  moduleE: dynamic(() =>
    import(
      /* webpackChunkName: "moduleE" */ '@tcp/core/src/components/common/molecules/ModuleE/views/ModuleE'
    )
  ),
  outfitCarousel: dynamic(() =>
    import(
      /* webpackChunkName: "outfitCarousel" */ '@tcp/core/src/components/common/molecules/OutfitCarouselModule/views/OutfitCarouselModule.view'
    )
  ),
  module_container: dynamic(() =>
    import(
      /* webpackChunkName: "moduleContainer" */ '@tcp/core/src/components/common/molecules/ModuleContainer/view/ModuleContainer'
    )
  ),
  moduleFont: dynamic(() =>
    import(
      /* webpackChunkName: "moduleFont" */ '@tcp/core/src/components/common/molecules/ModuleFont/ModuleFont'
    )
  ),
};
const timer = 200;
export class HomePageWrapper extends React.Component {
  constructor(props) {
    super(props);
    const { router } = props;
    const { target } = (router && router.query) || {};
    this.state = {
      showRegister: target === 'register',
      showLogin: target === 'login',
    };
  }

  componentDidMount() {
    const apiConfig = getAPIConfig();
    const { isCSREnabledForHome } = apiConfig;
    const {
      openCountrySelectorModal,
      router,
      isRegisteredUserCallDone,
      getPageLayout,
      lazyloadModules,
    } = this.props;
    const { asPath } = router;
    const pagelayout =
      asPath &&
      (asPath.indexOf('?') < 0
        ? asPath.split('/').pop()
        : asPath.substring(asPath.lastIndexOf('/') + 1, asPath.indexOf('?')));
    if (isCSREnabledForHome && lazyloadModules) getPageLayout(pagelayout, apiConfig);
    if (router.query.target === 'ship-to') openCountrySelectorModal();
    if (isRegisteredUserCallDone) this.sHomePageAnalyticsVal(router);
    if (router.events) {
      router.events.on('routeChangeComplete', this.handleRouteChangeComplete);
    }
  }

  componentDidUpdate(prevProps) {
    const { router, isRegisteredUserCallDone, pageName, plccOfferTakeOverABTestEnabled } =
      this.props;
    if (
      isRegisteredUserCallDone &&
      isRegisteredUserCallDone !== prevProps.isRegisteredUserCallDone
    ) {
      // adding a timeout so that the landing brand cookie is dropped and avoid race conditions
      const analyticsTimer = setTimeout(() => {
        this.sHomePageAnalyticsVal(router);
        clearTimeout(analyticsTimer);
      }, timer);
    }

    const { pathname = '' } = (window && window.location) || {};
    if (
      pageName === 'home' &&
      isRegisteredUserCallDone &&
      plccOfferTakeOverABTestEnabled !== null &&
      pathname.indexOf('register') === -1 &&
      pathname.indexOf('login') === -1
    ) {
      this.subscriptionPopUpOnPageLoad();
    }
  }

  componentWillUnmount() {
    const { router } = this.props;
    if (router.events) {
      router.events.off('routeChangeComplete', this.handleRouteChangeComplete);
    }
  }

  subscriptionPopUpOnPageLoad = () => {
    const {
      openPlccOfferModal,
      isPlcc,
      plccModalPersistentCookieExpiry,
      plccOfferTakeOverABTestEnabled,
    } = this.props;
    const { TCP_SUB_DOMAIN, GYMBOREE_SUB_DOMAIN, COOKIE_PLCC_OFFER_MODAL_PERSISTENT } =
      HOMEPAGE_CONSTANTS;
    /**
     * @function domain check for the domain setting in cookie
     * using @function isTcp from utils
     */
    const domain = isTCP() ? TCP_SUB_DOMAIN : GYMBOREE_SUB_DOMAIN;

    /**
     * @function checkCookieExist function to check the existence
     * of cookie in the browser by returning @BOOLEAN
     */
    const checkCookieExist = (name) => document.cookie.indexOf(name) > -1;

    if (
      plccOfferTakeOverABTestEnabled &&
      !isCanada() &&
      !isPlcc &&
      !checkCookieExist(COOKIE_PLCC_OFFER_MODAL_PERSISTENT)
    ) {
      document.cookie = `${COOKIE_PLCC_OFFER_MODAL_PERSISTENT}=true; domain=${domain}; max-age=${(
        86400 * plccModalPersistentCookieExpiry
      ).toString()}`;
      openPlccOfferModal();
    }
  };

  // For Setting the Home Page Analytics Data.
  sHomePageAnalyticsVal = (router) => {
    const { setCampaignId, trackHomepageView, setClickAnalyticsData, storeId, currency } =
      this.props;
    const queryParams = getQueryParamsFromUrl(router && router.asPath);
    const campaingnId = 'cid';
    const products = internalCampaignProductAnalyticsList();
    const nonSerachStr = 'non-internal search';
    const formatProductDataForEE = productArr(products);
    setProp('eVar15', 'D-Vo');

    let analyticsData = {};

    if (queryParams[campaingnId]) {
      setProp('eVar22', `external channel:${queryParams[campaingnId]}` || '');
      setCampaignId(queryParams[campaingnId]);
      analyticsData = {
        customEvents: ['event18'],
        internalCampaignId: '',
        pageSearchText: '',
        pageSearchType: nonSerachStr,
        listingCount: nonSerachStr,
        pageNavigationText: '',
        storeId,
      };
    } else {
      analyticsData = { storeId };
    }
    setClickAnalyticsData(analyticsData);
    // Ticket https://childrensplace.atlassian.net/browse/GMP-318 to remove call
    // Keeping all changes because we may need this call in future
    const enableImpressionCall = false;
    if (enableImpressionCall) {
      fireEnhancedEcomm({
        eventName: 'internalcampaignimpression',
        productsObj: formatProductDataForEE,
        eventType: 'impressions',
        pageName: 'homepage',
        currCode: currency,
      });
    }
    trackHomepageView({
      internalCampaignIdList: products,
      pageName: 'home page',
      pageSection: 'homepage',
      pageSubSection: 'home page',
      pageType: 'home page',
      storeId,
    });
  };

  handleRouteChangeComplete = (url) => {
    if (url && url.indexOf('/register') !== -1) {
      this.setState({ showRegister: true, showLogin: false });
    } else if (url && url.indexOf('/login') !== -1) {
      this.setState({ showLogin: true, showRegister: false });
    } else {
      this.setState({ showLogin: false, showRegister: false });
    }
  };

  render() {
    const { children, router, isRegisteredUserCallDone } = this.props;
    const { showRegister } = this.state;
    const { showLogin } = this.state;
    if (showRegister) {
      return (
        <CreateAccount
          router={router}
          fullPageView
          isRegisteredUserCallDone={isRegisteredUserCallDone}
        />
      );
    }
    if (showLogin) {
      return <LoginContainer router={router} fullPage />;
    }
    return [children];
  }
}

const HomePageWithRouter = withRouter(HomePageWrapper);

const HomePageView = (compProps) => {
  const {
    emailSignup,
    getPageLayout,
    isPlccOfferModalOpen,
    isRegisteredUserCallDone,
    openCountrySelectorModal,
    openPlccOfferModal,
    pageName,
    plccModalPersistentCookieExpiry,
    plccOfferTakeOverABTestEnabled,
    seoData,
    setCampaignId,
    setClickAnalyticsData,
    slots,
    storeId,
    trackHomepageView,
    lazyloadModules,
    isHpNewDesignCTAEnabled,
    isHpNewModuleDesignEnabled,
  } = compProps;
  const isCandidEnabled = getAPIConfig() && getAPIConfig().isCandidEnabled;
  const willCandidFeatureDisplay = isCandidEnabled && isTier1Device();

  return (
    <>
      {isPlccOfferModalOpen && (
        <DynamicModule
          key="sign-up-modals"
          importCallback={() =>
            import(
              /* webpackChunkName: "plcc-offer-modal" */ '@tcp/web/src/components/common/molecules/PlccOfferModal/container'
            )
          }
          buttonConfig={emailSignup}
          isModalOpen={isPlccOfferModalOpen}
        />
      )}
      <HomePageWithRouter
        openCountrySelectorModal={openCountrySelectorModal}
        setCampaignId={setCampaignId}
        trackHomepageView={trackHomepageView}
        setClickAnalyticsData={setClickAnalyticsData}
        isRegisteredUserCallDone={isRegisteredUserCallDone}
        storeId={storeId}
        getPageLayout={getPageLayout}
        plccOfferTakeOverABTestEnabled={plccOfferTakeOverABTestEnabled}
        pageName={pageName}
        plccModalPersistentCookieExpiry={plccModalPersistentCookieExpiry}
        compProps={compProps}
        openPlccOfferModal={openPlccOfferModal}
        isPlccOfferModalOpen={isPlccOfferModalOpen}
        lazyloadModules={lazyloadModules}
      >
        <StyledPageSlotsWrapper isHpNewModuleDesignEnabled={isHpNewModuleDesignEnabled}>
          <PageSlots
            slots={slots}
            modules={modules}
            page={HOMEPAGE_CONSTANTS.PAGE_NAME}
            isHomePage
            isHpNewModuleDesignEnabled={isHpNewModuleDesignEnabled}
            isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
          />
        </StyledPageSlotsWrapper>
        {willCandidFeatureDisplay && (
          <DynamicModule
            key="get-candid"
            importCallback={() =>
              import(
                /* webpackChunkName: "get-candid" */ '@tcp/core/src/components/common/molecules/GetCandid'
              )
            }
          />
        )}
        <SeoCopy {...seoData} />
      </HomePageWithRouter>
    </>
  );
};

HomePageView.defaultProps = HomePageViewDefaultProps;
HomePageWrapper.defaultProps = HomePageWrapperDefaultProps;
HomePageWrapper.propTypes = HomePageWrapperPropTypes;
HomePageView.propTypes = HomePageViewPropTypes;
const HomePageViewWithErrorBoundary = errorBoundary(HomePageView);

// Wrap the home page with a ref-forwarding element
const RefWrappedHomePageView = withRefWrapper(HomePageViewWithErrorBoundary);

/**
 * Hotfix-Aware Component. The use of `withHotfix` is just for making
 * page hotfix-aware.
 */
RefWrappedHomePageView.displayName = 'HomePage';
const HotfixAwareHomePage = withHotfix(RefWrappedHomePageView);

export default HotfixAwareHomePage;

export { HomePageView as HomePageViewVanilla };
