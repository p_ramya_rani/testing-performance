// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { fetchPageLayout } from '@tcp/core/src/reduxStore/actions';
import {
  getIsRegisteredUserCallDone,
  getDefaultStore,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getFavoriteStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { getCurrentCurrency } from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import { togglePlccOfferModal } from '@tcp/web/src/components/common/molecules/PlccOfferModal/container/PlccOfferModal.actions';
import { getAPIConfig } from '@tcp/core/src/utils';
import {
  getIsHpNewModuleDesignEnabled,
  getIsHpNewDesignCTAEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import HomePageView from '../views/HomePage.view';
import { initActions } from './HomePage.actions';
import {
  setCampaignId,
  setClickAnalyticsData,
  trackPageView,
} from '../../../../../../../core/src/analytics/actions';
import { toggleCountrySelectorModal } from '../../Header/molecules/CountrySelector/container/CountrySelector.actions';
import {
  hpSeoData,
  hpLabels,
  hpModulesData,
  getPlccOfferTakeOverABTest,
  getPlccModalPersistentCookieExpiry,
  getLazyloadModules,
} from '../HomePage.selectors';

HomePageView.getInitialProps = async ({ store, isServer }, pageProps) => {
  const state = store.getState();
  if (!isServer && !state.Layouts.home) {
    const apiConfig = getAPIConfig();
    store.dispatch(fetchPageLayout({ pageName: 'home', apiConfig }));
  }
  return {
    ...pageProps,
    ...{
      pageData: {
        pageName: 'home page',
        pageSection: 'homepage',
        pageSubSection: 'home page',
        pageType: 'home page',
        loadAnalyticsOnload: false,
      },
    },
  };
};

HomePageView.getInitActions = () => initActions;

HomePageView.pageInfo = {
  pageId: 'Home',
  name: 'home',
  modules: ['labels', 'header', 'footer', 'navigation'],
};

const mapStateToProps = (state) => {
  const { Footer } = state;
  const { PlccOffer: { isModalOpen: isPlccOfferModalOpen } = {} } = state;
  return {
    legalLinks: Footer.legalLinks,
    navLinks: Footer.navLinks,
    links: Footer.socialLinks,
    emailSignup: Footer.emailSignupBtn,
    referAFriend: Footer.referFriendBtn,
    copyrightText: Footer.copyrightText,
    seoData: hpSeoData(state),
    slots: hpModulesData(state),
    isRegisteredUserCallDone: getIsRegisteredUserCallDone(state),
    storeId: getDefaultStore(state) || getFavoriteStore(state),
    currency: getCurrentCurrency(state),
    labels: hpLabels(state),
    plccOfferTakeOverABTestEnabled: getPlccOfferTakeOverABTest(state),
    plccModalPersistentCookieExpiry: getPlccModalPersistentCookieExpiry(state),
    isPlcc: isPlccUser(state),
    isPlccOfferModalOpen,
    lazyloadModules: getLazyloadModules(state),
    isHpNewDesignCTAEnabled: getIsHpNewDesignCTAEnabled(state),
    isHpNewModuleDesignEnabled: getIsHpNewModuleDesignEnabled(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    openPlccOfferModal: () => {
      dispatch(togglePlccOfferModal({ isModalOpen: true }));
    },
    openCountrySelectorModal: () =>
      dispatch(
        toggleCountrySelectorModal({
          isModalOpen: true,
        })
      ),
    setCampaignId: (campaignId) => dispatch(setCampaignId(campaignId)),
    setClickAnalyticsData: (payload) => dispatch(setClickAnalyticsData(payload)),
    getPageLayout: (pageName, apiConfig) => {
      dispatch(
        fetchPageLayout({
          pageName,
          layoutName: null,
          icClpPage: false,
          isCSREnabled: true,
          apiConfig,
        })
      );
    },
    trackHomepageView: (payload) => {
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...payload,
                },
              },
            },
          },
        })
      );
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePageView);
