// 9fbef606107a605d69c0edbcd8029e5d 
import { loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import { SEO_DATA } from '@tcp/core/src/reduxStore/constants';

/* eslint-disable import/prefer-default-export */
export const initActions = [
  args => {
    return loadPageSEOData({ page: SEO_DATA.home, ...args });
  },
];
