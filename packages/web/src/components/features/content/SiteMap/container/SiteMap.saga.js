// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import sitemapAbstractor from '@tcp/core/src/services/abstractors/common/sitemap';
import { validateReduxCache } from '@tcp/core/src/utils/cache.util';
import { isMobileApp } from '@tcp/core/src/utils';
import {
  defaultBrand,
  defaultChannel,
  defaultCountry,
  MobileChannel,
} from '@tcp/core/src/services/api.constants';

import SITEMAP_CONSTANTS from './SiteMap.constants';
import { setSiteMapData } from './SiteMap.actions';

export function* fetchSitemapData({ payload: { apiConfig } }) {
  try {
    const { language } = apiConfig;
    const isMobileChannel = isMobileApp() || apiConfig.isAppChannel;
    const channel = isMobileChannel ? MobileChannel : defaultChannel;

    const payload = {
      brand: (apiConfig && apiConfig.brandIdCMS) || defaultBrand,
      channel,
      country: (apiConfig && apiConfig.siteIdCMS) || defaultCountry,
      lang: language !== 'en' ? language : '',
    };
    const result = yield call(sitemapAbstractor.getData, payload, apiConfig);
    const sitemapData = result.data.siteMap;
    yield put(setSiteMapData(sitemapData));
  } catch (err) {
    logger.error(err);
    yield null;
  }
}

function* SiteMapSaga() {
  const cachedSiteMapData = validateReduxCache(fetchSitemapData);
  yield takeLatest(SITEMAP_CONSTANTS.FETCH_SITEMAP_DATA, cachedSiteMapData);
}

export default SiteMapSaga;
