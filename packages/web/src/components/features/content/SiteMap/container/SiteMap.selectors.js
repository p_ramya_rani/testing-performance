// 9fbef606107a605d69c0edbcd8029e5d 
import { SITEMAP_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';

const getSiteMapData = state => {
  return state[SITEMAP_REDUCER_KEY].siteMapData;
};

export default getSiteMapData;
