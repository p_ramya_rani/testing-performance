// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mock from '../mock';
import { SiteMapVanilla as SiteMap } from '../views/SiteMap.view';

describe('NavBar component', () => {
  it('renders correctly', () => {
    const SiteMapComp = shallow(<SiteMap navTreeData={mock.data} siteMapData={mock.data2} />);

    expect(SiteMapComp).toMatchSnapshot();
  });
});
