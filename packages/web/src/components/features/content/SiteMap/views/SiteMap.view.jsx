// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import { arrayOf, shape, string } from 'prop-types';
import { Anchor, BodyCopy, Col, Row } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { configureInternalNavigationFromCMSUrl } from '@tcp/core/src/utils';
import style from '../SiteMap.style';

class SiteMap extends React.PureComponent {
  render() {
    const { className, siteMapData, navTreeData } = this.props;
    const { categories, content } = siteMapData;
    const filteredSiteCategoriesWithoutHref =
      categories &&
      categories.filter((item) => {
        return item.href === '';
      });
    return (
      <Row className={`${className} siteMap`}>
        <div className="siteMap_heading-container">
          <BodyCopy
            component="h1"
            className="siteMap_heading"
            fontWeight="semibold"
            fontSize={['fs16', 'fs24']}
            textAlign="center"
            letterSpacing="normal"
          >
            {content}
          </BodyCopy>
        </div>
        <Col
          colSize={{ small: 4, medium: 6, large: 10 }}
          offsetLeft={{ small: 1, medium: 1, large: 1 }}
          offsetRight={{ small: 1, medium: 1, large: 1 }}
        >
          <div className="categories_container">
            {navTreeData &&
              navTreeData.map(({ categoryContent, subCategories }) => {
                const subCategoriesArray = Object.values(subCategories);
                return (
                  categoryContent &&
                  categoryContent.displayToCustomer &&
                  categoryContent.name && (
                    <ol className="level-one-container">
                      <BodyCopy
                        component="h2"
                        className="level-one-title"
                        color="gray.900"
                        fontWeight="semibold"
                        fontSize="fs16"
                        textAlign="left"
                        letterSpacing="normal"
                      >
                        {categoryContent.url ? (
                          <Anchor to={categoryContent.url} asPath={categoryContent.asPath}>
                            {categoryContent.name}
                          </Anchor>
                        ) : (
                          categoryContent.name
                        )}
                      </BodyCopy>
                      {subCategoriesArray &&
                        subCategoriesArray.map((element) => {
                          const subCategoryL2List = element.items;
                          return (
                            subCategoryL2List &&
                            subCategoryL2List.map(
                              ({
                                categoryContent: categoryContentL2,
                                asPath,
                                url: hrefL2,
                                subCategories: categoriesL3,
                              }) => {
                                return (
                                  categoryContentL2 &&
                                  categoryContentL2.displayToCustomer &&
                                  categoryContentL2.name && (
                                    <li>
                                      <ul className="level-two-container">
                                        <BodyCopy
                                          component="h5"
                                          className="level-two-title"
                                          color="gray.900"
                                          fontWeight="semibold"
                                          fontSize="fs16"
                                          textAlign="left"
                                          letterSpacing="normal"
                                        >
                                          {hrefL2 ? (
                                            <Anchor to={hrefL2} asPath={asPath}>
                                              {categoryContentL2.name}
                                            </Anchor>
                                          ) : (
                                            categoryContentL2.name
                                          )}
                                        </BodyCopy>
                                        {categoriesL3 &&
                                          categoriesL3.map(
                                            ({
                                              categoryContent: categoryContentL3,
                                              asPath: asPath3,
                                              url: hrefL3,
                                            }) => {
                                              return (
                                                categoryContentL3 &&
                                                categoryContentL3.displayToCustomer &&
                                                categoryContentL3.name && (
                                                  <li className="level-three-container">
                                                    {asPath3 ? (
                                                      <Anchor
                                                        to={configureInternalNavigationFromCMSUrl(
                                                          hrefL3
                                                        )}
                                                        asPath={asPath3}
                                                      >
                                                        {categoryContentL3.name}
                                                      </Anchor>
                                                    ) : (
                                                      categoryContentL3.name
                                                    )}
                                                  </li>
                                                )
                                              );
                                            }
                                          )}
                                      </ul>
                                    </li>
                                  )
                                );
                              }
                            )
                          );
                        })}
                    </ol>
                  )
                );
              })}
          </div>
        </Col>
        <Col
          colSize={{ small: 4, medium: 6, large: 10 }}
          offsetLeft={{ small: 1, medium: 1, large: 1 }}
          offsetRight={{ small: 1, medium: 1, large: 1 }}
        >
          <div className="categories_container">
            {filteredSiteCategoriesWithoutHref &&
              filteredSiteCategoriesWithoutHref.map(({ name, href, category }) => {
                return (
                  name && (
                    <ol className="level-one-container">
                      <BodyCopy
                        component="h2"
                        className="level-one-title"
                        color="gray.900"
                        fontWeight="semibold"
                        fontSize="fs16"
                        textAlign="left"
                        letterSpacing="normal"
                      >
                        {href ? (
                          <Anchor to={configureInternalNavigationFromCMSUrl(href)} asPath={href}>
                            {name}
                          </Anchor>
                        ) : (
                          name
                        )}
                      </BodyCopy>
                      {category &&
                        category.map(({ name: nameL2, href: hrefL2, category: categoriesL3 }) => {
                          return (
                            nameL2 && (
                              <li>
                                <ul className="level-two-container">
                                  <BodyCopy
                                    component="h5"
                                    className="level-two-title"
                                    color="gray.900"
                                    fontWeight="semibold"
                                    fontSize="fs16"
                                    textAlign="left"
                                    letterSpacing="normal"
                                  >
                                    {hrefL2 ? (
                                      <Anchor
                                        to={configureInternalNavigationFromCMSUrl(hrefL2)}
                                        asPath={hrefL2}
                                      >
                                        {nameL2}
                                      </Anchor>
                                    ) : (
                                      nameL2
                                    )}
                                  </BodyCopy>
                                  {categoriesL3 &&
                                    categoriesL3.map(({ name: nameL3, href: hrefL3 }) => {
                                      return (
                                        nameL3 && (
                                          <li className="level-three-container">
                                            {hrefL3 ? (
                                              <Anchor
                                                to={configureInternalNavigationFromCMSUrl(hrefL3)}
                                                asPath={hrefL3}
                                              >
                                                {nameL3}
                                              </Anchor>
                                            ) : (
                                              nameL3
                                            )}
                                          </li>
                                        )
                                      );
                                    })}
                                </ul>
                              </li>
                            )
                          );
                        })}
                    </ol>
                  )
                );
              })}
          </div>
        </Col>
      </Row>
    );
  }
}

SiteMap.propTypes = {
  className: string,
  siteMapData: shape({
    categories: shape({ map: arrayOf({}) }),
    content: string,
  }),
  navTreeData: shape({}),
};

SiteMap.defaultProps = {
  className: '',
  siteMapData: {},
  navTreeData: {},
};

export default withStyles(errorBoundary(SiteMap), style);
export { SiteMap as SiteMapVanilla };
