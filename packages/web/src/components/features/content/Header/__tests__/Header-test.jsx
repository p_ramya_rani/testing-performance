// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { HeaderVanilla } from '../views/Header.view';

describe('Header component', () => {
  const props = {
    router: {
      route: '/ProductDetail',
      pathname: '/ProductDetail',
      asPath: '/us/p/Girls-Roll-Cuff-Distressed-Denim-Skimmer-Shorts-3016910-32RS',
      basePath: '',
      events: {
        on: () => {},
      },
    },
    loyaltyPromoBanner: [],
    promoMessageWrapper: null,
    headerPromoTextArea: null,
    navigationDrawer: {
      close: true,
      open: false,
    },
    isUserGuest: true,
    isUserPlcc: false,
    isRememberedUser: false,
    userPoints: 0,
    isLoggedIn: false,
    cartItemCount: 0,
    totalItems: 0,
    globalLabels: {},
    storeLabels: {},
    favStore: null,
    isPickupModalOpen: false,
    isQVModalOpen: false,
    isOpenOverlay: false,
    isOpenAddedToBag: false,
    isCartItemSFL: false,
    showMiniBag: false,
    trackOrderMountedState: false,
    isDrawerOpen: false,
    showFullPageAuth: true,
    isECOM: false,
    isRecalculateTaxes: true,
    loginModalMountedState: false,
    isCSHEnabled: true,
    minibagOnHoverEnabled: true,
    openNavigationDrawer: jest.fn(),
    closeNavigationDrawer: jest.fn(),
    openMiniBagDispatch: jest.fn(),
    closeMiniBagDispatch: jest.fn(),
    openOverlay: jest.fn(),
    closedOverlay: jest.fn(),
    openTrackOrderOverlay: jest.fn(),
    setClickAnalyticsData: jest.fn(),
    onRouteChangeGlobalActions: jest.fn(),
    selectOrderAction: jest.fn(),
    setLoginModalMountState: jest.fn(),
    trackAnalyticsClick: jest.fn(),
    className: 'sc-eishCr fDoKwy',
  };

  it('renders correctly', () => {
    const component = shallow(
      <HeaderVanilla
        headerTopNav={{ composites: {} }}
        headerPromoArea={{ composites: { promoTextBanner: {} } }}
        router={{ events: { on: () => {} } }}
      />
    );
    expect(component).toMatchSnapshot();
  });

  it('renders correctly with props', () => {
    const component = shallow(<HeaderVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly with login modal', () => {
    const component = shallow(<HeaderVanilla {...props} loginModalMountedState />);
    expect(component).toMatchSnapshot();
  });
});
