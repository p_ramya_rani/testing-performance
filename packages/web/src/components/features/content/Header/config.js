// 9fbef606107a605d69c0edbcd8029e5d

export default {
  tcp: {
    alt: "The Children's Place",
    dataLocator: 'global_TCPlink',
    imgSrc: `tcp-logo.svg`,
    title: "The Children's Place",
  },
  gym: {
    alt: 'Gymboree',
    dataLocator: 'global_Gymboreelink',
    imgSrc: `gymboree-logo.svg`,
    title: 'Gymboree',
  },
  snj: {
    alt: 'S&J',
    dataLocator: 'global_SNJlink',
    imgSrc: `snj-logo.svg`,
    title: 'Sugar & Jade',
  },
};
