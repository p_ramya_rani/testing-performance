/* eslint-disable extra-rules/no-commented-out-code,max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes, { func, shape, bool } from 'prop-types';
import throttle from 'lodash/throttle';
import isEqual from 'lodash/isEqual';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import { getViewportInfo, getLabelValue, isClient } from '@tcp/core/src/utils';
import { NAVIGATION_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import DynamicModule from '@tcp/core/src/components/common/atoms/DynamicModule';
import LoyaltyPromoBanner from '@tcp/core/src/components/common/molecules/LoyaltyPromoBanner';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import HeaderTopNav from '../molecules/HeaderTopNav';
import HeaderPromo from '../molecules/HeaderPromo';
import HeaderMiddleNav from '../molecules/HeaderMiddleNav';
import CondensedHeader from '../molecules/CondensedHeader';
import style from '../Header.style';

const doesCondensedClassExists = (condensedHeader) =>
  condensedHeader &&
  condensedHeader.classList &&
  condensedHeader.classList.contains('show-condensed-header');

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showCondensedHeader: false,
      isSearchOpen: false,
      isFullSizeSearchModalOpen: false,
    };
    this.onCloseClick = this.onCloseClick.bind(this);
    this.setSearchState = this.setSearchState.bind(this);
    this.handleScrollThrottle = throttle(this.handleScroll, 100);
    this.prevScrollpos = 0;
  }

  componentDidMount() {
    this.addScrollListener();
    const { router, isNewPDPEnabled } = this.props;
    router.events.on('routeChangeComplete', (url) => this.handleRouteChange(url, isNewPDPEnabled));
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { favStore } = this.props;
    const userStore = JSON.parse(getLocalStorage('defaultStore')) || '';
    if (
      isEqual(nextProps, this.props) &&
      isEqual(nextState, this.state) &&
      favStore?.basicInfo?.storeName === userStore?.basicInfo?.storeName
    ) {
      return false;
    }
    return true;
  }

  componentDidUpdate() {
    this.minicartCondensed();
  }

  componentWillUnmount() {
    const { router, isNewPDPEnabled } = this.props;
    this.removeScrollListener();
    router.events.off('routeChangeComplete', (url) => this.handleRouteChange(url, isNewPDPEnabled));
  }

  onCloseClick() {
    const { isFullSizeSearchModalOpen, isSearchOpen } = this.state;

    this.setState({
      isFullSizeSearchModalOpen: !isFullSizeSearchModalOpen,
      isSearchOpen: !isSearchOpen,
    });

    this.closeNavDrawer();
  }

  setSearchState(currentStatus, cb = null) {
    this.setState({ isSearchOpen: currentStatus }, cb ? cb() : () => {});
  }

  closeNavDrawer = () => {
    const { navigationDrawer, closeNavigationDrawer } = this.props;
    if (navigationDrawer && navigationDrawer.open) {
      closeNavigationDrawer('l1_drawer');
    }
  };

  getStickyPosition = () => {
    const header = document.getElementById('header-middle-nav');
    return header && header.offsetTop;
  };

  addScrollListener = () => {
    window.addEventListener('scroll', this.handleScrollThrottle);
  };

  removeScrollListener = () => {
    window.removeEventListener('scroll', this.handleScrollThrottle);
  };

  setCondensedHeaderState = (newVal, callback = () => {}) => {
    const { showCondensedHeader } = this.state;
    if (showCondensedHeader !== newVal) {
      this.setState({ showCondensedHeader: newVal });
      callback();
    } else if (showCondensedHeader === true && newVal === true) {
      callback();
    }
  };

  getIsScrollEnabled = (url) => {
    return url.match(/\/p\//g);
  };

  getIsStickyHeader = () => {
    const { isNewPDPEnabled } = this.props;
    const sticky = this.getStickyPosition();
    const url = window.location.pathname;
    const isScrollEnabled = this.getIsScrollEnabled(url);
    const isSticky = window.pageYOffset > sticky;

    if (isNewPDPEnabled && getViewportInfo().isMobile && isScrollEnabled) {
      return window.pageYOffset && isSticky && window.pageYOffset < this.prevScrollpos;
    }

    return (
      (getViewportInfo().isDesktop && window.pageYOffset > sticky + 64) ||
      (!getViewportInfo().isDesktop && isSticky)
    );
  };

  minicartCondensed = () => {
    const condensedHeaderMini = document.getElementsByClassName('header-global')[0];
    const condensedHeader = document.getElementsByClassName('condensed-header')[0];
    const BAGCONDENSED = 'show-condensed-minibag';
    if (condensedHeaderMini) {
      if (condensedHeader) {
        if (condensedHeaderMini && !condensedHeaderMini.classList.contains(BAGCONDENSED)) {
          condensedHeaderMini.classList.add(BAGCONDENSED);
        }
      } else {
        condensedHeaderMini.classList.remove(BAGCONDENSED);
      }
    }
  };

  handleScroll = () => {
    /**
     * Note:
     * 1. Condensed header is 'stuck' if scrolled past Navigation bar
     * and scrolling direction is down
     * 2. Condensed header is 'unstuck' when scrolling up till Navigation bar get visible
     */
    let showGiftCard = true;
    if (this.getIsStickyHeader()) {
      this.setCondensedHeaderState(true, () => {
        const condensedHeader = document.getElementsByClassName('condensed-header')[0];
        if (condensedHeader && !doesCondensedClassExists(condensedHeader)) {
          condensedHeader.classList.add('show-condensed-header');
          this.minicartCondensed();
        }
        /*  This is to show Gift Cards Navigation Tab - code written in Launch
            https://launch.adobe.com/companies/CO5481a39341114faeaef51e2d4476320d/properties/PR0951db90dcc84cb683be4f6ff29a7843/rules/RL755519950c81466486b98df079adf431
          */
        if (showGiftCard && typeof window.addGiftNav === 'function') {
          window.addGiftNav();
          showGiftCard = false;
        }
      });
    } else {
      this.setCondensedHeaderState(false);
    }
    this.prevScrollpos = window.pageYOffset;
  };

  handleRouteChange(asPath, isNewPDPEnabled) {
    const isScrollEnabled = this.getIsScrollEnabled(asPath);
    if (isScrollEnabled && isNewPDPEnabled) {
      this.prevScrollpos = 0;
      this.setCondensedHeaderState(false);
    }
  }

  renderTopPromo = () => {
    const {
      topPromoBanner,
      isUserGuest,
      isUserPlcc,
      navigationDrawer: { close = true } = {},
    } = this.props;
    return (
      <>
        {topPromoBanner && close && (
          <LoyaltyPromoBanner
            key="loyalty-top-promo-banner"
            richTextList={topPromoBanner}
            className="header-promo__container top-promo-banner"
            cookieID="mprTopHead"
            isUserGuest={isUserGuest}
            isUserPlcc={isUserPlcc}
          />
        )}
      </>
    );
  };

  renderHeaderPromo = () => {
    /**
     * Note:
     * Mobile header to be rendered  for mobile viewport and desktop for larger devices
     */
    let isMobileView = false;
    const { headerPromoTextArea, headerPromoHtmlArea, globalLabels } = this.props;
    const placeCashMobileUrl = getLabelValue(
      globalLabels,
      'lbl_header_bgImage_mobile_url',
      'header'
    );
    const placeCashDesktopUrl = getLabelValue(
      globalLabels,
      'lbl_header_bgImage_desktop_url',
      'header'
    );
    if (isClient()) {
      if (getViewportInfo().isMobile) {
        isMobileView = true;
      }
      return (
        <>
          {isMobileView ? (
            <HeaderPromo
              mobileMarkup
              className="header__promo-area--mobile"
              dataTextPromo={headerPromoTextArea}
              dataHtmlPromo={headerPromoHtmlArea}
              placeCashMobileUrl={placeCashMobileUrl}
              placeCashDesktopUrl={placeCashDesktopUrl}
            />
          ) : (
            <HeaderPromo
              className="header__promo-area--desktop"
              dataTextPromo={headerPromoTextArea}
              dataHtmlPromo={headerPromoHtmlArea}
              placeCashMobileUrl={placeCashMobileUrl}
              placeCashDesktopUrl={placeCashDesktopUrl}
            />
          )}
        </>
      );
    }
    return (
      <HeaderPromo
        mobileMarkup
        dataTextPromo={headerPromoTextArea}
        dataHtmlPromo={headerPromoHtmlArea}
        placeCashMobileUrl={placeCashMobileUrl}
        placeCashDesktopUrl={placeCashDesktopUrl}
      />
    );
  };

  renderLoginModalDynamically = () => {
    const { isLoggedIn, loginModalMountedState, setLoginModalMountState } = this.props;
    if (!isLoggedIn && loginModalMountedState) {
      return (
        <DynamicModule
          key="login-fav-modal"
          importCallback={() =>
            import(
              /* webpackChunkName: "login-fav-modal" */ '@tcp/core/src/components/features/account/LoginPage/views/LoginModal'
            )
          }
          variation="favorites"
          setLoginModalMountState={setLoginModalMountState}
          openState={loginModalMountedState}
        />
      );
    }
    return null;
  };

  render() {
    const {
      className,
      brandTabs,
      promoMessageWrapper,
      navigationDrawer,
      openNavigationDrawer,
      closeNavigationDrawer,
      isUserGuest,
      isUserPlcc,
      isRememberedUser,
      userName,
      userPoints,
      userRewards,
      openOverlay,
      closedOverlay,
      isOpenOverlay,
      openTrackOrderOverlay,
      isLoggedIn,
      cartItemCount,
      globalLabels,
      storeLabels,
      openMiniBagDispatch,
      closeMiniBagDispatch,
      totalItems,
      favStore,
      isPickupModalOpen,
      loyaltyPromoBanner,
      setClickAnalyticsData,
      isQVModalOpen,
      onRouteChangeGlobalActions,
      isOpenAddedToBag,
      isCartItemSFL,
      router,
      showMiniBag,
      trackOrderMountedState,
      getOrderDetailsDispatch,
      setUpdateMiniBagFlag,
      orderItems,
      updateMiniBag,
      isECOM,
      isRecalculateTaxes,
      showFullPageAuth,
      isCSHEnabled,
      selectOrderAction,
      isSNJEnabled,
      minibagOnHoverEnabled,
      trackAnalyticsClick,
      isShowNavAnimation,
      isNewMiniBag,
      mergeAccountReset,
      resetEmailConfirmationRequest,
      isKeepMinibagCloseState,
      updateMiniBagCloseStatusActn,
      isKeepMinibagCloseStateQV,
      updateMiniBagCloseStatusActnQV,
    } = this.props;
    const { showCondensedHeader, isSearchOpen, isFullSizeSearchModalOpen } = this.state;
    const labels = { ...globalLabels, ...storeLabels };
    const { accessibility: { skipNavigation } = {} } = labels;
    const loyaltyPromoBannerImport = () =>
      import(
        /* webpackChunkName: "loyalty-promo-banner" */ '@tcp/core/src/components/common/molecules/LoyaltyPromoBanner'
      );

    return (
      <header className={`${className} header-global ${isCartItemSFL ? 'removeX-hidden' : ''}`}>
        {this.renderTopPromo()}

        <HeaderTopNav
          className="header-topnav"
          brandTabs={brandTabs}
          promoMessageWrapper={promoMessageWrapper}
          openOverlay={openTrackOrderOverlay}
          isUserLoggedIn={isLoggedIn}
          labels={labels}
          store={favStore}
          closeNavDrawer={this.closeNavDrawer}
          selectOrderAction={selectOrderAction}
          isSNJEnabled={isSNJEnabled}
          isCSHEnabled={isCSHEnabled}
          trackAnalyticsClick={trackAnalyticsClick}
          mergeAccountReset={mergeAccountReset}
          resetEmailConfirmationRequest={resetEmailConfirmationRequest}
        />

        <Anchor href="#overlayWrapper" noLink className="skip-main">
          {skipNavigation}
        </Anchor>

        <HeaderMiddleNav
          showCondensedHeader={showCondensedHeader}
          openNavigationDrawer={openNavigationDrawer}
          closeNavigationDrawer={closeNavigationDrawer}
          navigationDrawer={navigationDrawer}
          isUserPlcc={isUserPlcc}
          isRememberedUser={isRememberedUser}
          userName={userName}
          userPoints={userPoints}
          userRewards={userRewards}
          openOverlay={openOverlay}
          closedOverlay={closedOverlay}
          isOpenOverlay={isOpenOverlay}
          isLoggedIn={isLoggedIn}
          cartItemCount={cartItemCount}
          totalItems={totalItems}
          openMiniBagDispatch={openMiniBagDispatch}
          closeMiniBagDispatch={closeMiniBagDispatch}
          store={favStore}
          labels={labels}
          setClickAnalyticsData={setClickAnalyticsData}
          onRouteChangeGlobalActions={onRouteChangeGlobalActions}
          isSearchOpen={isSearchOpen}
          isFullSizeSearchModalOpen={isFullSizeSearchModalOpen}
          onCloseClick={this.onCloseClick}
          setSearchState={this.setSearchState}
          isOpenAddedToBag={isOpenAddedToBag}
          showMiniBag={showMiniBag}
          getOrderDetailsDispatch={getOrderDetailsDispatch}
          orderItems={orderItems}
          updateMiniBag={updateMiniBag}
          setUpdateMiniBagFlag={setUpdateMiniBagFlag}
          isECOM={isECOM}
          isRecalculateTaxes={isRecalculateTaxes}
          showFullPageAuth={showFullPageAuth}
          minibagOnHoverEnabled={minibagOnHoverEnabled || isNewMiniBag}
          isSNJEnabled={isSNJEnabled}
          isNewMiniBag={isNewMiniBag}
          isKeepMinibagCloseState={isKeepMinibagCloseState}
          updateMiniBagCloseStatusActn={updateMiniBagCloseStatusActn}
        />
        {isOpenOverlay && (
          <DynamicModule
            key="overlay-modal"
            importCallback={() =>
              import(
                /* webpackChunkName: "overlay-modal" */ '@tcp/core/src/components/features/account/OverlayModal'
              )
            }
            showCondensedHeader={showCondensedHeader}
            isLoggedIn={isLoggedIn}
            openState={isOpenOverlay}
          />
        )}

        {this.renderHeaderPromo()}

        {loyaltyPromoBanner && (
          <DynamicModule
            key="loyalty-promo-banner"
            importCallback={loyaltyPromoBannerImport}
            richTextList={loyaltyPromoBanner}
            className="header-promo__container"
            cookieID="mprAboveHead"
            isUserGuest={isUserGuest}
            isUserPlcc={isUserPlcc}
          />
        )}
        {showCondensedHeader && (
          <CondensedHeader
            openNavigationDrawer={openNavigationDrawer}
            openMiniBagDispatch={openMiniBagDispatch}
            closeMiniBagDispatch={closeMiniBagDispatch}
            closeNavigationDrawer={closeNavigationDrawer}
            navigationDrawer={navigationDrawer}
            userName={userName}
            userPoints={userPoints}
            userRewards={userRewards}
            openOverlay={openOverlay}
            isLoggedIn={isLoggedIn}
            cartItemCount={cartItemCount}
            totalItems={totalItems}
            showCondensedHeader={showCondensedHeader}
            labels={labels}
            onCloseClick={this.onCloseClick}
            setSearchState={this.setSearchState}
            isSearchOpen={isSearchOpen}
            isOpenAddedToBag={isOpenAddedToBag}
            getOrderDetailsDispatch={getOrderDetailsDispatch}
            orderItems={orderItems}
            updateMiniBag={updateMiniBag}
            setUpdateMiniBagFlag={setUpdateMiniBagFlag}
            isECOM={isECOM}
            isRecalculateTaxes={isRecalculateTaxes}
            showFullPageAuth={showFullPageAuth}
            minibagOnHoverEnabled={minibagOnHoverEnabled || isNewMiniBag}
            isUserPlcc={isUserPlcc}
            isShowNavAnimation={isShowNavAnimation}
            isKeepMinibagCloseState={isKeepMinibagCloseState}
            updateMiniBagCloseStatusActn={updateMiniBagCloseStatusActn}
            isKeepMinibagCloseStateQV={isKeepMinibagCloseStateQV}
            updateMiniBagCloseStatusActnQV={updateMiniBagCloseStatusActnQV}
          />
        )}
        {trackOrderMountedState && (
          <DynamicModule
            key="track-order"
            importCallback={() =>
              import(
                /* webpackChunkName: "track-order-modal" */ '@tcp/core/src/components/features/account/TrackOrder'
              )
            }
            trackOrderMountedState={trackOrderMountedState}
          />
        )}
        {isPickupModalOpen && (
          <DynamicModule
            key="pickup-modal"
            importCallback={() =>
              import(
                /* webpackChunkName: "pick-store-modal" */ '@tcp/core/src/components/common/organisms/PickupStoreModal'
              )
            }
            router={router}
          />
        )}
        {this.renderLoginModalDynamically()}
        {isQVModalOpen && (
          <DynamicModule
            key="quickview-modal"
            importCallback={() =>
              import(
                /* webpackChunkName: "quick-view-modal" */ '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.container'
              )
            }
          />
        )}
        <RenderPerf.Measure name={NAVIGATION_VISIBLE} />
      </header>
    );
  }
}

Header.propTypes = {
  className: PropTypes.string.isRequired,
  loyaltyPromoBanner: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  brandTabs: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  topPromoBanner: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  promoMessageWrapper: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  headerPromoTextArea: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  headerPromoHtmlArea: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  navigationDrawer: PropTypes.arrayOf(PropTypes.object).isRequired,
  openNavigationDrawer: PropTypes.func.isRequired,
  closeNavigationDrawer: PropTypes.func.isRequired,
  getOrderDetailsDispatch: func.isRequired,
  isUserGuest: PropTypes.bool.isRequired,
  isUserPlcc: PropTypes.bool.isRequired,
  userName: PropTypes.string.isRequired,
  userPoints: PropTypes.string.isRequired,
  userRewards: PropTypes.string.isRequired,
  openOverlay: PropTypes.func.isRequired,
  closedOverlay: PropTypes.func.isRequired,
  isOpenOverlay: PropTypes.bool.isRequired,
  openTrackOrderOverlay: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  cartItemCount: PropTypes.func.isRequired,
  openMiniBagDispatch: PropTypes.func.isRequired,
  closeMiniBagDispatch: PropTypes.func.isRequired,
  setUpdateMiniBagFlag: func.isRequired,
  updateMiniBag: bool.isRequired,
  globalLabels: PropTypes.arrayOf(PropTypes.object).isRequired,
  storeLabels: PropTypes.arrayOf(PropTypes.object).isRequired,
  totalItems: PropTypes.number,
  favStore: PropTypes.shape({
    basicInfo: PropTypes.shape({}),
    hours: PropTypes.shape({
      regularHours: PropTypes.shape([]),
      regularAndHolidayHours: PropTypes.shape([]),
      holidayHours: PropTypes.shape([]),
    }),
    features: PropTypes.shape({}),
  }),
  isPickupModalOpen: PropTypes.bool,
  isRememberedUser: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func.isRequired,
  isQVModalOpen: PropTypes.bool,
  onRouteChangeGlobalActions: PropTypes.func,
  isOpenAddedToBag: PropTypes.bool,
  isCartItemSFL: PropTypes.bool,
  router: PropTypes.shape({}),
  orderItems: shape([]).isRequired,
  showMiniBag: PropTypes.bool,
  trackOrderMountedState: PropTypes.bool,
  isRecalculateTaxes: PropTypes.bool,
  isECOM: PropTypes.bool,
  showFullPageAuth: PropTypes.bool,
  isCSHEnabled: PropTypes.bool,
  selectOrderAction: PropTypes.func.isRequired,
  loginModalMountedState: PropTypes.bool.isRequired,
  setLoginModalMountState: PropTypes.func.isRequired,
  isSNJEnabled: PropTypes.bool.isRequired,
  minibagOnHoverEnabled: PropTypes.bool,
  trackAnalyticsClick: PropTypes.func,
  resetEmailConfirmationRequest: PropTypes.func,
  mergeAccountReset: PropTypes.func,
  isShowNavAnimation: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  isNewMiniBag: PropTypes.bool,
  isKeepMinibagCloseState: PropTypes.bool,
  isKeepMinibagCloseStateQV: PropTypes.bool,
  updateMiniBagCloseStatusActn: PropTypes.func,
  updateMiniBagCloseStatusActnQV: PropTypes.func,
};

Header.defaultProps = {
  totalItems: 0,
  favStore: {
    basicInfo: {
      id: '',
      storeName: '',
      phone: '',
      coordinates: {},
      address: {},
    },
    hours: {
      regularHours: [],
      regularAndHolidayHours: [],
      holidayHours: [],
    },
    features: {},
  },
  isPickupModalOpen: false,
  isRememberedUser: false,
  isQVModalOpen: false,
  onRouteChangeGlobalActions: () => {},
  isOpenAddedToBag: false,
  isCartItemSFL: false,
  router: {},
  showMiniBag: false,
  trackOrderMountedState: false,
  isRecalculateTaxes: false,
  isECOM: false,
  showFullPageAuth: false,
  isCSHEnabled: false,
  minibagOnHoverEnabled: false,
  trackAnalyticsClick: () => {},
  isShowNavAnimation: false,
  isNewPDPEnabled: false,
  isNewMiniBag: false,
  resetEmailConfirmationRequest: () => {},
  mergeAccountReset: () => {},
  isKeepMinibagCloseState: false,
  isKeepMinibagCloseStateQV: false,
  updateMiniBagCloseStatusActn: () => {},
  updateMiniBagCloseStatusActnQV: () => {},
};

export default errorBoundary(withStyles(Header, style));
export { Header as HeaderVanilla };
