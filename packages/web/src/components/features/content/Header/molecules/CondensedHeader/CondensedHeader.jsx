// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Anchor, BodyCopy } from '@tcp/core/src/components/common/atoms';
import { getCartItemCount } from '@tcp/core/src/utils/cookie.util';
import { getBrand, isGymboree, routerPush, getViewportInfo, isCanada } from '@tcp/core/src/utils';
import { breakpoints } from '@tcp/core/styles/themes/TCP/mediaQuery';
import Navigation from '../../../Navigation';
import BrandLogo from '../../../../../common/atoms/BrandLogo';
import style from './CondensedHeader.style';
import config from '../../config';
import { keyboard } from '../../../../../../constants/constants';
import {
  SearchImageComp,
  UserImageComp,
  CartImageComp,
  MenuImageComp,
  LogoComp,
} from './Inline-Svg-Icons';

const handleNavigationDrawer = (openNavigationDrawer, closeNavigationDrawer, isOpen) => () => {
  return isOpen ? closeNavigationDrawer('l1_drawer') : openNavigationDrawer('l1_drawer');
};

class CondensedHeader extends React.PureComponent {
  constructor(props) {
    super(props);
    const { isLoggedIn, cartItemCount } = props;
    this.state = {
      userNameClick: true,
      triggerLoginCreateAccount: true,
      isLoggedIn: isLoggedIn || false,
      cartItemCount,
    };
    this.openSearchBar = this.openSearchBar.bind(this);
    this.isBagCtaHovered = false;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isLoggedIn: prevLoggedInState, cartItemCount, isOpenAddedToBag } = prevState;
    const {
      isLoggedIn: nextLoggedInState,
      totalItems,
      isOpenAddedToBag: nextIsOpenAddedToBag,
    } = nextProps;
    if (
      prevLoggedInState !== nextLoggedInState ||
      totalItems !== cartItemCount ||
      (isOpenAddedToBag !== nextIsOpenAddedToBag && nextIsOpenAddedToBag === true)
    ) {
      return { cartItemCount: totalItems || getCartItemCount() };
    }
    return null;
  }

  toggleMiniBagModal = ({ e, isOpen, totalItems }) => {
    if (e) e.preventDefault();
    const { minibagOnHoverEnabled, isKeepMinibagCloseState, isKeepMinibagCloseStateQV } =
      this.props;
    if (
      minibagOnHoverEnabled &&
      window.innerWidth > breakpoints.values.lg &&
      !isKeepMinibagCloseState &&
      !isKeepMinibagCloseStateQV
    ) {
      const { openMiniBagDispatch } = this.props;
      setTimeout(() => {
        if (!this.isBagCtaHovered) {
          openMiniBagDispatch();
          if (!isOpen) {
            this.setState({
              cartItemCount: totalItems || getCartItemCount(),
            });
            this.isBagCtaHovered = true;
          }
        }
      }, 300);
    }
  };

  onDefocus = ({ e, isOpen, totalItems }) => {
    const { updateMiniBagCloseStatusActn, updateMiniBagCloseStatusActnQV } = this.props;
    if (e) e.preventDefault();
    this.toggleMiniBagModal({ e, isOpen, totalItems });

    setTimeout(() => {
      updateMiniBagCloseStatusActn(false);
      updateMiniBagCloseStatusActnQV(false);
      this.isBagCtaHovered = false;
    }, 1000);
  };

  onSetModalFlag = () => {
    const { updateMiniBagCloseStatusActn, updateMiniBagCloseStatusActnQV } = this.props;
    updateMiniBagCloseStatusActn(false);
    updateMiniBagCloseStatusActnQV(false);
  };

  miniBagClick = ({ e, isOpen, isRouting, totalItems }) => {
    if (e) e.preventDefault();
    const { minibagOnHoverEnabled } = this.props;
    if (!minibagOnHoverEnabled) {
      if (window.innerWidth <= breakpoints.values.lg && !isRouting) {
        routerPush('/bag', '/bag');
      } else {
        const { openMiniBagDispatch } = this.props;
        openMiniBagDispatch();
        if (!isOpen) {
          this.setState({
            cartItemCount: totalItems || getCartItemCount(),
          });
        }
      }
    } else if (window.innerWidth <= breakpoints.values.lg && !isRouting) {
      routerPush('/bag', '/bag');
    }
  };

  onLinkClick = ({ e, openOverlay, userNameClick, triggerLoginCreateAccount, componentToOpen }) => {
    e.preventDefault();
    const { showFullPageAuth } = this.props;
    const { pathname = 'referrer' } = (window && window.location) || {};

    if (userNameClick || triggerLoginCreateAccount) {
      if (showFullPageAuth && componentToOpen === 'login') {
        routerPush(`/home?target=login&successtarget=${pathname}`, '/home/login');
      } else {
        openOverlay({
          component: componentToOpen,
          variation: 'primary',
        });
      }
    }
    this.setState({
      userNameClick: triggerLoginCreateAccount && userNameClick ? userNameClick : !userNameClick,
    });
  };

  handleKeyDown = (event, openNavigationDrawer, closeNavigationDrawer, isNavigationDrawerOpen) => {
    const { KEY_ENTER, KEY_SPACE } = keyboard;
    const { which } = event;
    if (which === KEY_ENTER || which === KEY_SPACE) {
      handleNavigationDrawer(openNavigationDrawer, closeNavigationDrawer, isNavigationDrawerOpen)();
    }
  };

  getNavigation = () => {
    const { userName, userPoints, userRewards, closeNavigationDrawer, navigationDrawer } =
      this.props;
    return getViewportInfo().isDesktop ? (
      <div className="condensed-navigation">
        <Navigation
          openNavigationDrawer={navigationDrawer.open}
          closeNavigationDrawer={!navigationDrawer.open}
          closeNav={closeNavigationDrawer}
          userName={userName}
          userPoints={userPoints}
          userRewards={userRewards}
          isCondensedHeader
        />
      </div>
    ) : null;
  };

  getImgSrc = (navigationDrawer) => {
    return navigationDrawer.open ? <MenuImageComp isCloseIcon /> : <MenuImageComp />;
  };

  openSearchBar = (e) => {
    e.preventDefault();
    const { setSearchState, onCloseClick } = this.props;
    if (window.innerWidth <= breakpoints.values.lg) {
      onCloseClick();
    } else {
      setSearchState(true);
    }
  };

  getSearchIcon = (isGym) => {
    const { showCondensedHeader } = this.props;
    const classNamePrefix = showCondensedHeader && !isGym ? 'search-blue' : 'search-image';
    return <SearchImageComp classNamePrefix={classNamePrefix} />;
  };

  handleUserRewards = (userRewards = 0) => {
    return userRewards % 1 ? userRewards : Math.floor(userRewards);
  };

  render() {
    const {
      className,
      openNavigationDrawer,
      closeNavigationDrawer,
      navigationDrawer,
      openOverlay,
      userName,
      labels,
      isSearchOpen,
      totalItems,
      isLoggedIn,
      userRewards,
    } = this.props;
    const brand = getBrand();
    const { userNameClick, triggerLoginCreateAccount, cartItemCount } = this.state;
    const { accessibility: { cartIconButton, closeIconButton, hamburgerMenu } = {} } = labels;
    const accountDrawer = 'accountDrawer';
    const login = 'login';
    const isGym = isGymboree();

    const getSearchIcon = this.getSearchIcon(isGym);

    const rewards = this.handleUserRewards(userRewards);
    return (
      <React.Fragment>
        <Row id="condensedHeader" className={`${className} condensed-header`} noLastMargin>
          <div className="content-wrapper">
            <div className="condensed-hamburger-menu">
              <span
                tabIndex="0"
                role="button"
                aria-label={navigationDrawer.open ? closeIconButton : hamburgerMenu}
                className="hamburger-menu"
                onClick={handleNavigationDrawer(
                  openNavigationDrawer,
                  closeNavigationDrawer,
                  navigationDrawer.open
                )}
                onKeyDown={(e) =>
                  this.handleKeyDown(
                    e,
                    openNavigationDrawer,
                    closeNavigationDrawer,
                    navigationDrawer.open
                  )
                }
                data-locator={navigationDrawer.open ? 'L1_menu_close_Btn' : 'menu_bar_icon'}
              >
                {this.getImgSrc(navigationDrawer)}
              </span>
            </div>
            <BrandLogo
              alt={config[brand]?.alt}
              className="condensed-brand-logo"
              dataLocator={config[brand]?.dataLocator}
            >
              <LogoComp brand={brand} />
            </BrandLogo>
            {this.getNavigation()}
            <div className="condensed-header-icons">
              {!isSearchOpen && (
                <Anchor
                  noLink
                  className="search-image icon"
                  data-locator="search-icon"
                  onClick={this.openSearchBar}
                  fontSizeVariation="large"
                  anchorVariation="primary"
                  id="condense-global-searchIcon"
                >
                  {getSearchIcon}
                </Anchor>
              )}

              {userName && isLoggedIn ? (
                <React.Fragment>
                  <BodyCopy
                    id="accountDrawer"
                    textAlign="right"
                    className="username user-icon-container"
                    onClick={(e) =>
                      this.onLinkClick({
                        e,
                        openOverlay,
                        userNameClick,
                        componentToOpen: accountDrawer,
                      })
                    }
                    data-overlayTarget="accountDrawer"
                  >
                    <span className="rightLink userIcon hide-on-mobile" data-locator="user-icon">
                      <UserImageComp isGrayIcon={isGym} className="hide-on-mobile" />
                    </span>
                    <span
                      className="rightLink userIcon hide-on-desktop hide-on-tablet"
                      data-locator="user-icon"
                    >
                      <UserImageComp
                        reducedSize={rewards !== 0 && !isCanada()}
                        isGrayIcon={isGym}
                      />
                    </span>
                    {rewards !== 0 && !isCanada() && (
                      <BodyCopy
                        fontFamily="secondary"
                        component="span"
                        fontSize="fs10"
                        fontWeight="extrabold"
                        className="user-rewards hide-on-desktop hide-on-tablet"
                        color="white"
                      >
                        {`$${rewards}`}
                      </BodyCopy>
                    )}
                  </BodyCopy>
                </React.Fragment>
              ) : (
                <Anchor
                  href="#"
                  noLink
                  className="user-icon-link"
                  onClick={(e) =>
                    this.onLinkClick({
                      e,
                      openOverlay,
                      triggerLoginCreateAccount,
                      componentToOpen: login,
                    })
                  }
                  fontSizeVariation="large"
                  anchorVariation="primary"
                  data-overlayTarget="login"
                >
                  <div className="user-icon-container">
                    <span
                      className="rightLink userIcon"
                      id="condensedLogin"
                      data-locator="user-icon"
                    >
                      <UserImageComp isGrayIcon={isGym} />
                    </span>
                  </div>
                </Anchor>
              )}
              <Anchor
                to="#"
                id="cartIcon"
                aria-label={`${cartIconButton} ${cartItemCount} item`}
                className="rightLink"
                onClick={(e) => this.miniBagClick({ e, isOpen: true, totalItems })}
                onMouseOver={(e) => this.toggleMiniBagModal({ e, isOpen: true, totalItems })}
                onMouseOut={(e) => this.onDefocus({ e, isOpen: false, totalItems })}
                onBlur={(e) => this.onDefocus({ e, isOpen: false, totalItems })}
                onMouseEnter={() => this.onSetModalFlag()}
                fontSizeVariation="small"
                anchorVariation="primary"
                noLink
              >
                <span
                  alt=""
                  aria-hidden="true"
                  className="product-image"
                  data-locator="addedtobag-bag-icon"
                >
                  <CartImageComp isGrayIcon={isGym} />
                </span>
                <BodyCopy
                  className="cartCount"
                  component="span"
                  fontWeight="semibold"
                  fontSize="fs10"
                  tabIndex="-1"
                  aria-hidden="true"
                >
                  {cartItemCount || 0}
                </BodyCopy>
              </Anchor>
            </div>
          </div>
        </Row>
        <Row className={`${className} condensed-border`} />
      </React.Fragment>
    );
  }
}

CondensedHeader.propTypes = {
  className: PropTypes.string.isRequired,
  navigationDrawer: PropTypes.shape({
    open: PropTypes.bool.isRequired,
  }),
  openNavigationDrawer: PropTypes.func.isRequired,
  openMiniBagDispatch: PropTypes.func.isRequired,
  closeNavigationDrawer: PropTypes.func.isRequired,
  userName: PropTypes.string.isRequired,
  userPoints: PropTypes.string.isRequired,
  userRewards: PropTypes.string.isRequired,
  openOverlay: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  cartItemCount: PropTypes.func.isRequired,
  labels: PropTypes.shape({
    userNameClick: PropTypes.string.isRequired,
    triggerLoginCreateAccount: PropTypes.string.isRequired,
    cartItemCount: PropTypes.string.isRequired,
    accessibility: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  }).isRequired,
  showCondensedHeader: PropTypes.bool,
  isSearchOpen: PropTypes.bool,
  onCloseClick: PropTypes.func,
  setSearchState: PropTypes.func,
  totalItems: PropTypes.number,
  showFullPageAuth: PropTypes.bool,
  minibagOnHoverEnabled: PropTypes.bool,
  isKeepMinibagCloseState: PropTypes.bool,
  isKeepMinibagCloseStateQV: PropTypes.bool,
  updateMiniBagCloseStatusActn: PropTypes.func,
  updateMiniBagCloseStatusActnQV: PropTypes.func,
};

CondensedHeader.defaultProps = {
  navigationDrawer: {
    open: false,
  },
  showCondensedHeader: false,
  isSearchOpen: false,
  onCloseClick: () => {},
  setSearchState: () => {},
  totalItems: 0,
  showFullPageAuth: false,
  minibagOnHoverEnabled: false,
  isKeepMinibagCloseState: false,
  isKeepMinibagCloseStateQV: false,
  updateMiniBagCloseStatusActn: () => {},
  updateMiniBagCloseStatusActnQV: () => {},
};

export { CondensedHeader as CondensedHeaderVanilla };
export default withStyles(CondensedHeader, style);
