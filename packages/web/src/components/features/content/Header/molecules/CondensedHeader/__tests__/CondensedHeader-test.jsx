// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { routerPush } from '@tcp/core/src/utils';
import { CondensedHeaderVanilla } from '../CondensedHeader';

const openMiniBagDispatchMocked = jest.fn();
jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  readCookie: jest.fn(),
  getCartItemCount: jest.fn(),
  getBrand: jest.fn().mockImplementation(() => 'tcp'),
  isGymboree: jest.fn().mockImplementation(() => false),
  isCanada: jest.fn().mockImplementation(() => false),
  getSiteId: jest.fn().mockImplementation(() => '102230'),
  getViewportInfo: jest.fn().mockImplementation(() => {
    return { isDesktop: true };
  }),
  getImageFilePath: jest.fn(),
}));

const props = {
  openNavigationDrawer: jest.fn(),
  openMiniBagDispatch: openMiniBagDispatchMocked,
  closeNavigationDrawer: jest.fn(),
  navigationDrawer: {
    open: false,
  },
  userPoints: 0,
  openOverlay: jest.fn(),
  isLoggedIn: false,
  cartItemCount: 1,
  totalItems: 1,
  showCondensedHeader: true,
  labels: {
    accessibility: {
      accountIconButton: 'Account',
      cartIconButton: 'cart',
      hamburgerMenu: 'menu',
      searchIconButton: 'search',
    },
  },
  onCloseClick: jest.fn(),
  setSearchState: jest.fn(),
  isSearchOpen: false,
  isOpenAddedToBag: false,
  getOrderDetailsDispatch: jest.fn(),
  orderItems: [
    {
      productInfo: {
        imagePath: '3005138/3005138_10.jpg',
        size: 'XL (14)',
        generalProductId: '1396545',
        color: {
          name: 'WHITE',
        },
        variantNo: '3005138005',
        upc: '00193511317306',
        orderType: 'ECOM',
        isGiftCard: false,
        name: 'Boys Print Poplin Button Down Shirt',
        productPartNumber: '3005138_10',
        multiPackItems: null,
        itemPartNumber: '00193511317306',
        multiPack: false,
        pdpUrl: '/us/p/Boys-Christmas-Short-Sleeve-Print-Poplin-Button-Down-Shirt-3005138-10',
        itemBrand: 'TCP',
        colorFitSizeDisplayNames: {},
        skuId: '1397510',
      },
      itemInfo: {
        wasPrice: 18.95,
        salePrice: 18.94,
        quantity: 1,
        listPrice: 18.94,
        offerPrice: 18.94,
        itemId: '1245456827',
        itemUnitPrice: 18.94,
        itemPoints: 19,
        itemUnitDstPrice: 18.94,
      },
      miscInfo: {
        store: null,
        vendorColorDisplayId: '3005138_10',
        bossStartDate: null,
        badge: {
          defaultBadge: 'CLEARANCE',
        },
        isOnlineOnly: false,
        isInventoryAvailBOSS: false,
        storeTomorrowOpenRange: null,
        isBossEligible: true,
        clearanceItem: true,
        storeItemsCount: 0,
        storeTodayOpenRange: null,
        storeAddress: null,
        storePhoneNumber: null,
        isBopisEligible: true,
        availability: 'OK',
        orderItemType: 'ECOM',
        isStoreBOSSEligible: false,
        storeId: null,
        bossEndDate: null,
      },
    },
  ],
  updateMiniBag: false,
  setUpdateMiniBagFlag: jest.fn(),
  isECOM: true,
  isRecalculateTaxes: true,
  className: 'sc-GMQeP ghiqBH',
};

describe('<CondensedHeaderVanilla />', () => {
  let Wrapper = '';
  const mockedRouterPush = jest.fn();
  beforeEach(() => {
    Wrapper = shallow(<CondensedHeaderVanilla {...props} />);
  });

  test('Should match snapshot correctly', () => {
    expect(Wrapper).toMatchSnapshot();
  });

  test('Get updated Mini bag items from get Order Details API', () => {
    Wrapper = shallow(<CondensedHeaderVanilla {...props} updateMiniBag />);
    expect(Wrapper).toMatchSnapshot();
  });

  it('onLinkClick should not redirect to login page', () => {
    routerPush.mockImplementation(mockedRouterPush);
    Wrapper.setProps({
      showFullPageAuth: true,
    });
    const e = { preventDefault: () => {} };
    const componentInstance = Wrapper.instance();
    componentInstance.onLinkClick({ e, componentToOpen: 'login', userNameClick: false });
    expect(mockedRouterPush).not.toBeCalledWith(
      '/home?target=login&successtarget=/',
      '/home/login'
    );
  });

  it('onLinkClick should redirect to login page', () => {
    routerPush.mockImplementation(mockedRouterPush);
    Wrapper.setProps({
      showFullPageAuth: true,
    });
    const e = { preventDefault: () => {} };
    const componentInstance = Wrapper.instance();
    componentInstance.onLinkClick({ e, componentToOpen: 'login', userNameClick: true });
    expect(mockedRouterPush).toBeCalledWith('/home?target=login&successtarget=/', '/home/login');
  });

  it('miniBagOnClick  should redirect to bag page', () => {
    routerPush.mockImplementation(mockedRouterPush);
    Wrapper.setProps({
      minibagOnHoverEnabled: true,
    });
    window.innerWidth = 900;
    const componentInstance = Wrapper.instance();
    componentInstance.miniBagClick({});
    expect(mockedRouterPush).toBeCalledWith('/bag', '/bag');
  });

  it('miniBagOnClick should call openMiniBagDispatch ', () => {
    routerPush.mockImplementation(mockedRouterPush);
    Wrapper.setProps({
      minibagOnHoverEnabled: false,
    });
    window.innerWidth = 1300;
    const componentInstance = Wrapper.instance();
    componentInstance.miniBagClick({});
    expect(openMiniBagDispatchMocked).toBeCalled();
  });
});
