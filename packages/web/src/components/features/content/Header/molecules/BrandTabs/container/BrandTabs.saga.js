import { call, takeLatest, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { urlContainsQuery } from '@tcp/core/src/utils';
import { NavigateXHR } from '@tcp/core/src/services/abstractors/account';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { BRAND_TABS_CONSTANTS } from './BrandTabs.constants';

const getUpdatedUrl = ({ url, cookieToken }) => {
  if (urlContainsQuery(url)) {
    return `${url}&ct=${cookieToken}`;
  }
  return `${url}?ct=${cookieToken}`;
};

export function* handleBrandNavigation({ payload }) {
  const { brandName = '', active, url, target = '_self' } = payload || {};
  try {
    if (!active) {
      const isSNJEnabled = yield select(getIsSNJEnabled);
      const response = yield call(NavigateXHR, '', {}, brandName, isSNJEnabled);
      const { encodedCookie = '' } = response && response.data;
      if (encodedCookie) {
        const Url = getUpdatedUrl({ url, cookieToken: encodedCookie });
        window.open(Url, target);
      }
    }
  } catch (err) {
    logger.error(err);
  }
}

function* BrandTabsSaga() {
  yield takeLatest(BRAND_TABS_CONSTANTS.BRAND_TABS_NAVIGATION_XHR, handleBrandNavigation);
}

export default BrandTabsSaga;
