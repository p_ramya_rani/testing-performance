// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { BrandTabsVanilla } from '../views/BrandTabs';
import mockData from './mock';

jest.mock('@tcp/core/src/utils/utils', () => {
  const originalModule = jest.requireActual('@tcp/core/src/utils/utils');
  return {
    __esModule: true,
    ...originalModule,
    getBrandInCaps: jest.fn(() => 'TCP'),
    getBrand: jest.fn(() => 'GYM'),
  };
});

describe('BrandTabs component', () => {
  it('renders correctly with brand-tabs className', () => {
    const props = {
      className: 'brand-tabs',
      data: mockData.brand_tabs,
      brandNames: mockData.brandNames,
    };
    const component = shallow(<BrandTabsVanilla {...props}>Brand Tabs with TCP</BrandTabsVanilla>);
    expect(component).toMatchSnapshot();
    expect(component.find('.brand-tabs')).toHaveLength(1);
  });
});
