// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { routerPush } from '@tcp/core/src/utils';
import { HeaderTopNavVanilla } from '../view/HeaderTopNav';
import mockData from '../mock';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  getBrand: () => 'tcp',
}));

const props = {
  className: 'header-top-nav',
  brandTabs: mockData.dataTopNav.composites.brand_tabs,
  promoMessageWrapper: mockData.dataTopNav.composites.promo_message_wrapper,
  labels: {
    helpCenter: {
      lbl_customer_support: 'Customer Support',
    },
    trackOrder: {
      lbl_trackOrder_trackOrderHeaderLink: 'Track Order Header',
    },
  },
  store: {
    basicInfo: {
      address: {},
      coordinates: {},
      id: '',
      phone: '',
      storeName: '',
    },
    features: {},
    hours: {
      holidayHours: {},
      regularAndHolidayHours: {},
      regularHours: {},
    },
  },
  isCSHEnabled: true,
  isUserLoggedIn: true,
  openOverlay: jest.fn(),
  isSNJEnabled: true,
};

describe('HeaderTopNav component', () => {
  it('HeaderTopNav component renders correctly to match snapshot', () => {
    const component = shallow(<HeaderTopNavVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('HeaderTopNav component renders correctly when isCSHEnabled is false', () => {
    const component = shallow(<HeaderTopNavVanilla {...props} />);
    component.setProps({ isCSHEnabled: false, isSNJEnabled: false });
    expect(component).toMatchSnapshot();
  });

  it('shoud call navigateToCSH function', () => {
    const mockedRouterPush = jest.fn();
    routerPush.mockImplementation(mockedRouterPush);
    const navigateToCSH = jest.fn();
    const component = shallow(<HeaderTopNavVanilla {...props} />);
    component
      .find("[className='order-assistance no-border-button elem-pl-SM']")
      .simulate('click', { preventDefault: jest.fn() });
    navigateToCSH.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith('/customer-help', '/customer-help');
  });

  it('shoud call onLinkClick function', () => {
    const mockedRouterPush = jest.fn();
    routerPush.mockImplementation(mockedRouterPush);
    const onLinkClick = jest.fn();
    const component = shallow(<HeaderTopNavVanilla {...props} />);
    component
      .find("[dataLocator='track_order_header']")
      .simulate('click', { preventDefault: jest.fn() });
    onLinkClick.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith('/account?id=orders', '/account?id=orders');
    component.setProps({ isUserLoggedIn: false });
    component
      .find("[dataLocator='track_order_header']")
      .simulate('click', { preventDefault: jest.fn() });
    onLinkClick.mockImplementation();
    expect(props.openOverlay).toBeCalled();
  });
});
