/* eslint-disable extra-rules/no-commented-out-code,max-lines */
/* istanbul ignore file */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import dynamic from 'next/dynamic';
import PropTypes from 'prop-types';
import { Col, Row, Image, Anchor, BodyCopy } from '@tcp/core/src/components/common/atoms';
import DynamicModule from '@tcp/core/src/components/common/atoms/DynamicModule';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getCartItemCount, readCookie } from '@tcp/core/src/utils/cookie.util';
import { breakpoints } from '@tcp/core/styles/themes/TCP/mediaQuery';
import { getBrand, getIconPath, routerPush, getImageFilePath } from '@tcp/core/src/utils';
import SearchBar from '@tcp/core/src/components/common/molecules/SearchBar/index';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import Navigation from '../../../Navigation';
import BrandLogo from '../../../../../common/atoms/BrandLogo';
import config from '../../config';
import { keyboard } from '../../../../../../constants/constants';
import style, { customHeaderStyle } from './HeaderMiddleNav.style';
import LoggedInUserInfo from '../LoggedInUserInfo/view/LoggedInUserInfo';
import GuestUserInfo from '../GuestUserInfo/view/GuestUserInfo';

const StoreLocatorLink = dynamic(() => import('../StoreLocatorLink'));

/**
 * This function handles opening and closing for Navigation drawer on mobile and tablet viewport
 * @param {Function} openNavigationDrawer Function to dispatch open drawer action to store
 * @param {Function} closeNavigationDrawer  Function to dispatch close drawer action to store
 * @param {Boolean} isOpen Flag to determine if drawer is open
 */
const handleNavigationDrawer = (openNavigationDrawer, closeNavigationDrawer, isOpen) => () => {
  return isOpen ? closeNavigationDrawer('l1_drawer') : openNavigationDrawer('l1_drawer');
};

class HeaderMiddleNav extends React.PureComponent {
  constructor(props) {
    super(props);
    const { isLoggedIn, cartItemCount } = props;
    this.state = {
      userNameClick: true,
      triggerLoginCreateAccount: true,
      isLoggedIn: isLoggedIn || false,
      cartItemCount,
    };
    this.isBagCtaHovered = false;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isLoggedIn: prevLoggedInState, cartItemCount, isOpenAddedToBag } = prevState;
    const {
      isLoggedIn: nextLoggedInState,
      totalItems,
      isOpenAddedToBag: nextIsOpenAddedToBag,
    } = nextProps;
    if (
      prevLoggedInState !== nextLoggedInState ||
      totalItems !== cartItemCount ||
      (isOpenAddedToBag !== nextIsOpenAddedToBag && nextIsOpenAddedToBag === true)
    ) {
      return { cartItemCount: totalItems || getCartItemCount() };
    }
    return null;
  }

  onLinkClick = ({ openOverlay, navname }, componentToOpen) => {
    const { setClickAnalyticsData, isOpenOverlay, closeOverlay, showFullPageAuth } = this.props;
    const { pathname = 'referrer' } = (window && window.location) || {};

    setClickAnalyticsData({
      eventName: 'Click_Navigation_v97',
      pageNavigationText: navname,
    });

    if (showFullPageAuth && componentToOpen === 'createAccount') {
      routerPush(`/home?target=register&successtarget=${pathname}`, '/home/register');
    } else if (showFullPageAuth && componentToOpen === 'login') {
      routerPush(`/home?target=login&successtarget=${pathname}`, '/home/login');
    } else {
      if (!isOpenOverlay) {
        openOverlay({
          component: componentToOpen,
          variation: 'primary',
        });
      }
      closeOverlay();
    }

    this.closeNavDrawer();
  };

  renderAccountInfoSection = (
    userName,
    openOverlay,
    isUserPlcc,
    userPoints,
    userRewards,
    isRememberedUser,
    labels
  ) => {
    const { userNameClick, triggerLoginCreateAccount } = this.state;
    const { isOpenOverlay, isLoggedIn, isSearchOpen } = this.props;
    const displayName = userName || readCookie('tcp_firstname');

    return userName && isLoggedIn && !isSearchOpen ? (
      <LoggedInUserInfo
        mainId="accountDrawer"
        userName={userName}
        userPoints={userPoints}
        userRewards={userRewards}
        userNameClick={userNameClick}
        openOverlay={openOverlay}
        isOpenOverlay={isOpenOverlay}
        onLinkClick={this.onLinkClick}
        isDrawer={false}
        labels={labels}
        isUserPlcc={isUserPlcc}
      />
    ) : (
      !isSearchOpen && (
        <GuestUserInfo
          createAccount="createAccount"
          login="login"
          triggerLoginCreateAccount={triggerLoginCreateAccount}
          onLinkClick={this.onLinkClick}
          openOverlay={openOverlay}
          isRememberedUser={isRememberedUser}
          userName={displayName}
          userPoints={userPoints}
          userRewards={userRewards}
          isDrawer={false}
          labels={labels}
          loyaltyLocation="global-header"
        />
      )
    );
  };

  handleKeyDown = (event, openNavigationDrawer, closeNavigationDrawer, isNavigationDrawerOpen) => {
    const { KEY_ENTER, KEY_SPACE } = keyboard;
    const { which } = event;
    if (which === KEY_ENTER || which === KEY_SPACE) {
      handleNavigationDrawer(openNavigationDrawer, closeNavigationDrawer, isNavigationDrawerOpen)();
    }
  };

  updateCartItemCount = () => {
    const { totalItems } = this.props;
    this.setState({
      cartItemCount: totalItems || getCartItemCount(),
    });
  };

  miniBagOnHover = (e) => {
    if (e) e.preventDefault();
    const { minibagOnHoverEnabled } = this.props;
    this.isBagCtaHovered = true;
    if (minibagOnHoverEnabled && window && window.innerWidth > breakpoints.values.lg) {
      const { openMiniBagDispatch, closedOverlay } = this.props;
      setTimeout(() => {
        if (this.isBagCtaHovered) {
          openMiniBagDispatch();
          closedOverlay();
        }
      }, 300);
    }
    this.closeNavDrawer();
  };

  onDefocus = (e) => {
    if (e) e.preventDefault();
    this.isBagCtaHovered = false;
  };

  miniBagOnClick = ({ e }) => {
    if (e) e.preventDefault();
    const { openMiniBagDispatch, minibagOnHoverEnabled, isNewMiniBag } = this.props;
    if (isNewMiniBag) {
      routerPush('/bag', '/bag');
    }
    if (!minibagOnHoverEnabled) {
      if (window && window.innerWidth <= breakpoints.values.lg) {
        routerPush('/bag', '/bag');
      } else {
        openMiniBagDispatch();
      }
      this.closeNavDrawer();
    } else if (window && window.innerWidth <= breakpoints.values.lg) {
      routerPush('/bag', '/bag');
    }
  };

  closeNavDrawer = () => {
    const { navigationDrawer, closeNavigationDrawer } = this.props;
    if (navigationDrawer && navigationDrawer.open) {
      closeNavigationDrawer('l1_drawer');
    }
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      className,
      openNavigationDrawer,
      closeNavigationDrawer,
      navigationDrawer,
      openOverlay,
      isUserPlcc,
      userName,
      userPoints,
      userRewards,
      store,
      labels,
      isRememberedUser,
      onRouteChangeGlobalActions,
      isSearchOpen,
      isFullSizeSearchModalOpen,
      setSearchState,
      onCloseClick,
      showCondensedHeader,
      showMiniBag,
      isSNJEnabled,
    } = this.props;
    const { userNameClick, triggerLoginCreateAccount } = this.state;
    const brand = getBrand();
    const { cartItemCount } = this.state;
    const {
      accessibility: { cartIconButton, closeIconButton, hamburgerMenu } = {},
      store: storeLabel = {},
    } = labels;

    return (
      <React.Fragment>
        <Row className="content-wrapper" fullBleed>
          <Row className={`${className} header-middle-nav`}>
            <Col
              colSize={{
                large: 5,
                medium: 8,
                small: 6,
              }}
              className="header-middle-nav-storelocator"
            >
              <StoreLocatorLink
                store={store}
                labels={labels && { ...storeLabel, ...labels.StoreDetail }}
                onLinkClick={this.closeNavDrawer}
              />
            </Col>
            <Col
              className="header-middle-nav-search"
              colSize={{
                large: 2,
                medium: 8,
                small: 6,
              }}
            >
              <Image
                src={navigationDrawer.open ? getIconPath('mobile-close-dark') : getIconPath('menu')}
                alt="hamburger menu"
                role="button"
                tabIndex="0"
                aria-label={navigationDrawer.open ? closeIconButton : hamburgerMenu}
                className="hamburger-menu"
                onClick={handleNavigationDrawer(
                  openNavigationDrawer,
                  closeNavigationDrawer,
                  navigationDrawer.open
                )}
                onKeyDown={(e) =>
                  this.handleKeyDown(
                    e,
                    openNavigationDrawer,
                    closeNavigationDrawer,
                    navigationDrawer.open
                  )
                }
                data-locator={navigationDrawer.open ? 'L1_menu_close_Btn' : 'menu_bar_icon'}
              />

              {config[brand] && (
                <>
                  <BrandLogo
                    alt={config[brand].alt}
                    className="header-brand__home-logo--brand"
                    dataLocator={config[brand].dataLocator}
                    imgSrc={`${getImageFilePath()}/${config[brand].imgSrc}`}
                  />
                </>
              )}
            </Col>
            <Col
              colSize={{
                large: 5,
                medium: 8,
                small: 6,
              }}
              className={`textRight header-middle-login-section ${isSearchOpen && 'flexbox'}`}
            >
              {isFullSizeSearchModalOpen ? (
                <Modal
                  isOpen={isSearchOpen && isFullSizeSearchModalOpen}
                  onRequestClose={this.handleShowHideFullSizeModalClick}
                  overlayClassName="TCPModal__Overlay"
                  className="TCPModal__Content"
                  widthConfig={{ small: '375px', medium: '765px', large: '1023px' }}
                  heightConfig={{ height: '99%' }}
                  fixedWidth
                  inheritedStyles={customHeaderStyle}
                  headingAlign="center"
                  horizontalBar={false}
                  stickyCloseIcon
                  fullWidth
                  stickyHeader
                >
                  <SearchBar
                    className={!isSearchOpen}
                    setSearchState={setSearchState}
                    isSearchOpen={isSearchOpen}
                    onCloseClick={onCloseClick}
                    isFullSizeSearchModalOpen={isFullSizeSearchModalOpen}
                    fromCondensedHeader={showCondensedHeader}
                  />
                </Modal>
              ) : (
                <div className="headerSearchBox-wrapper">
                  <SearchBar
                    className="headerSearchBox"
                    setSearchState={setSearchState}
                    isSearchOpen={isSearchOpen}
                    onCloseClick={onCloseClick}
                    isFullSizeSearchModalOpen={isFullSizeSearchModalOpen}
                    fromCondensedHeader={showCondensedHeader}
                  />
                </div>
              )}
              {isSNJEnabled && (
                <div className="mobile-locator-icon">
                  <StoreLocatorLink
                    store={store}
                    labels={labels && { ...storeLabel, ...labels.StoreDetail }}
                    onLinkClick={this.closeNavDrawer}
                    isIconOnly
                  />
                </div>
              )}
              {this.renderAccountInfoSection(
                userName,
                openOverlay,
                isUserPlcc,
                userPoints,
                userRewards,
                isRememberedUser,
                labels
              )}
              <Anchor
                to=""
                id="cartIcon"
                aria-label={`${cartIconButton} ${cartItemCount} item`}
                className=""
                onClick={(e) => this.miniBagOnClick({ e })}
                onMouseOver={(e) => this.miniBagOnHover(e)}
                onFocus={(e) => this.miniBagOnHover(e)}
                onMouseOut={(e) => this.onDefocus(e)}
                onBlur={(e) => this.onDefocus(e)}
                fontSizeVariation="large"
                anchorVariation="primary"
                noLink
              >
                <Image
                  alt=""
                  aria-hidden="true"
                  className="product-image mini-bag-icon"
                  src={getIconPath('cart-icon-1')}
                  data-locator="addedtobag-bag-icon"
                />
                <BodyCopy
                  className="cartCount"
                  component="span"
                  fontWeight="semibold"
                  fontSize="fs10"
                  aria-hidden="true"
                  tabIndex="-1"
                >
                  {cartItemCount || 0}
                </BodyCopy>
              </Anchor>
            </Col>
          </Row>
        </Row>
        <Row
          fullBleed={{
            small: true,
            medium: true,
            large: true,
          }}
        >
          <Col
            className="header-middle-nav-bar"
            id="header-middle-nav"
            colSize={{
              large: 12,
              medium: 8,
              small: 6,
            }}
          >
            <Navigation
              openNavigationDrawer={navigationDrawer.open}
              closeNavigationDrawer={!navigationDrawer.open}
              closeNav={closeNavigationDrawer}
              userName={userName}
              userPoints={userPoints}
              userRewards={userRewards}
              userNameClick={userNameClick}
              onLinkClick={this.onLinkClick}
              triggerLoginCreateAccount={triggerLoginCreateAccount}
              onRouteChangeGlobalActions={onRouteChangeGlobalActions}
            />
          </Col>
        </Row>
        {showMiniBag && (
          <DynamicModule
            importCallback={() =>
              import(
                /* webpackChunkName: "mini-bag-container" */ '@tcp/web/src/components/features/CnC/MiniBag/container/MiniBag.container'
              )
            }
            isOpen={showMiniBag}
            userName={userName}
            updateCartItemCount={this.updateCartItemCount}
          />
        )}
      </React.Fragment>
    );
  }
}

HeaderMiddleNav.propTypes = {
  className: PropTypes.string.isRequired,
  navigationDrawer: PropTypes.objectOf(PropTypes.shape({})),
  openNavigationDrawer: PropTypes.func.isRequired,
  closeNavigationDrawer: PropTypes.func.isRequired,
  isUserPlcc: PropTypes.bool.isRequired,
  userName: PropTypes.string.isRequired,
  userPoints: PropTypes.string.isRequired,
  userRewards: PropTypes.string.isRequired,
  openOverlay: PropTypes.func.isRequired,
  closedOverlay: PropTypes.func.isRequired,
  isOpenOverlay: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  cartItemCount: PropTypes.func.isRequired,
  openMiniBagDispatch: PropTypes.func.isRequired,
  store: PropTypes.shape({
    basicInfo: PropTypes.shape({}),
    hours: PropTypes.shape({
      regularHours: PropTypes.shape([]),
      regularAndHolidayHours: PropTypes.shape([]),
      holidayHours: PropTypes.shape([]),
    }),
    features: PropTypes.shape({}),
  }),
  labels: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  isRememberedUser: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func.isRequired,
  closeOverlay: PropTypes.func,
  onRouteChangeGlobalActions: PropTypes.func,
  isSearchOpen: PropTypes.bool,
  isFullSizeSearchModalOpen: PropTypes.bool,
  onCloseClick: PropTypes.func,
  setSearchState: PropTypes.func,
  showCondensedHeader: PropTypes.bool,
  totalItems: PropTypes.number,
  showMiniBag: PropTypes.bool,
  showFullPageAuth: PropTypes.bool,
  minibagOnHoverEnabled: PropTypes.bool,
  isSNJEnabled: PropTypes.bool,
  isNewMiniBag: PropTypes.bool,
};

HeaderMiddleNav.defaultProps = {
  navigationDrawer: {
    open: false,
  },
  store: {
    basicInfo: {
      id: '',
      storeName: '',
      phone: '',
      coordinates: {},
      address: {},
    },
    hours: {
      regularHours: [],
      regularAndHolidayHours: [],
      holidayHours: [],
    },
    features: {},
  },
  isRememberedUser: false,
  closeOverlay: () => {},
  onRouteChangeGlobalActions: () => {},
  onCloseClick: () => {},
  setSearchState: () => {},
  isSearchOpen: false,
  isFullSizeSearchModalOpen: false,
  showCondensedHeader: false,
  totalItems: 0,
  showMiniBag: false,
  showFullPageAuth: false,
  minibagOnHoverEnabled: false,
  isSNJEnabled: false,
  isNewMiniBag: false,
};

export { HeaderMiddleNav as HeaderMiddleNavVanilla };

export default withStyles(HeaderMiddleNav, style);
