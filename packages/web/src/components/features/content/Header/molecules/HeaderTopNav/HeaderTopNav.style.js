// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};

  .header-topnav__row {
    overflow-x: hidden;
    padding: 0 11px;
    position: relative;
    box-sizing: border-box;
    display: flex;
    align-items: center;

    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 0 15px 0 9px;
    }
  }

  .header-topnav__brand-tabs,
  .header-topnav__brand-tabs_SNJ,
  .header-topnav__track-order,
  .header-topnav__promo-area {
    float: left;
  }

  .header-topnav__brand-tabs {
    width: 56%;

    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 30%;
    }
  }

  .header-topnav__brand-tabs_SNJ {
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 29%;
      overflow-x: hidden;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
    }
  }

  .header-topnav__promo-area {
    text-align: center;
    width: 45%;

    @media ${(props) => props.theme.mediaQuery.smallMax} {
      display: none;
    }
  }

  .header-option {
    display: flex;
    width: max-content;
    position: absolute;
    right: 100px;
    bottom: 15px;
  }

  .no-border-button {
    border: 0px;
    background: none;
  }
  .track-order {
    display: flex;
    border-right: 1px solid ${(props) => props.theme.colorPalette.gray['500']};
    line-height: 26px;
    padding-right: 10px;
    cursor: pointer;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  }

  .order-assistance {
    display: flex;
    line-height: 26px;
    padding-right: 10px;
    cursor: pointer;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  }

  .header-topnav__track-order {
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy1}px;
    text-align: right;
    width: 40%;
    display: none;

    @media ${(props) => props.theme.mediaQuery.medium} {
      display: block;
      width: 26%;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
  }

  .header-topnav__storelocator {
    display: none;

    @media ${(props) => props.theme.mediaQuery.smallMax} {
      display: block;
      margin-right: 0;
      margin-left: auto;
    }
  }

  .header-topnav__storelocator_SNJ {
    display: none;
  }
`;
