// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { routerPush } from '@tcp/core/src/utils';

import { HeaderMiddleNavVanilla as HeaderMiddleNav } from '../HeaderMiddleNav';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getImageFilePath: jest.fn(),
  readCookie: jest.fn(),
  getCartItemCount: jest.fn(),
  isCanada: jest.fn(),
  getSiteId: jest.fn(),
  getBrand: jest.fn().mockImplementation(() => 'tcp'),
}));

const openMiniBagDispatchMocked = jest.fn();

const props = {
  showCondensedHeader: false,
  openNavigationDrawer: jest.fn(),
  closeNavigationDrawer: jest.fn(),
  isUserPlcc: false,
  isRememberedUser: false,
  userPoints: 100,
  openOverlay: jest.fn(),
  isOpenOverlay: false,
  isLoggedIn: false,
  cartItemCount: 1,
  totalItems: 0,
  openMiniBagDispatch: openMiniBagDispatchMocked,
  store: null,
  labels: {
    accessibility: {
      closeIconButton: 'close',
    },
  },
  setClickAnalyticsData: jest.fn(),
  onRouteChangeGlobalActions: jest.fn(),
  isSearchOpen: false,
  isFullSizeSearchModalOpen: false,
  onCloseClick: jest.fn(),
  setSearchState: jest.fn(),
  isOpenAddedToBag: false,
  showMiniBag: false,
  getOrderDetailsDispatch: jest.fn(),
  updateMiniBag: false,
  setUpdateMiniBagFlag: jest.fn(),
  isECOM: false,
  isRecalculateTaxes: true,
};

describe('HeaderMiddleNav component', () => {
  let HeaderMiddleNavComp = '';

  beforeEach(() => {
    HeaderMiddleNavComp = shallow(<HeaderMiddleNav {...props} />);
  });

  const mockedRouterPush = jest.fn();

  it('renders correctly', () => {
    expect(HeaderMiddleNavComp).toMatchSnapshot();
  });

  it('Header Middle loaded perfectly', () => {
    expect(HeaderMiddleNavComp.find('.header-middle-nav')).toHaveLength(1);
  });

  it('Header Middle Search Bar loaded perfectly', () => {
    expect(HeaderMiddleNavComp.find('.header-middle-nav-search')).toHaveLength(1);
  });

  it('Header Middle Nav Bar loaded perfectly', () => {
    expect(HeaderMiddleNavComp.find('.header-middle-nav-bar')).toHaveLength(1);
  });

  it('renders correctly when props dont change', () => {
    expect(HeaderMiddleNavComp.state('isLoggedIn')).toBe(false);
    expect(HeaderMiddleNavComp).toMatchSnapshot();
  });

  test('get updated Mini bag items from get Order Details API', () => {
    HeaderMiddleNavComp = shallow(<HeaderMiddleNav {...props} updateMiniBag />);
    expect(HeaderMiddleNavComp).toMatchSnapshot();
  });

  it('onLinkClick should redirect to register page', () => {
    routerPush.mockImplementation(mockedRouterPush);
    HeaderMiddleNavComp.setProps({
      showFullPageAuth: true,
    });
    const componentInstance = HeaderMiddleNavComp.instance();
    componentInstance.onLinkClick({}, 'createAccount');
    expect(mockedRouterPush).toBeCalledWith(
      '/home?target=register&successtarget=/',
      '/home/register'
    );
  });

  it('miniBagOnClick  should redirect to bag page', () => {
    routerPush.mockImplementation(mockedRouterPush);
    HeaderMiddleNavComp.setProps({
      minibagOnHoverEnabled: true,
    });
    window.innerWidth = 900;
    const componentInstance = HeaderMiddleNavComp.instance();
    componentInstance.miniBagOnClick({});
    expect(mockedRouterPush).toBeCalledWith('/bag', '/bag');
  });

  it('miniBagOnClick  should call openMiniBagDispatch', () => {
    routerPush.mockImplementation(mockedRouterPush);
    HeaderMiddleNavComp.setProps({
      minibagOnHoverEnabled: false,
    });
    window.innerWidth = 1300;
    const componentInstance = HeaderMiddleNavComp.instance();
    componentInstance.miniBagOnClick({});
    expect(openMiniBagDispatchMocked).toBeCalled();
  });
});
