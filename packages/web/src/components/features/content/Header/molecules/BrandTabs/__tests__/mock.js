// 9fbef606107a605d69c0edbcd8029e5d
export default {
  brand_tabs: [
    {
      id: '1',
      url: '/',
      alt: "The Children's Place",
      title: "The Children's Place",
      target: '',
      class: 'header__brand-tab--tcp',
      active: true,
    },
    {
      id: '2',
      url: 'https://www.gymboree.com/',
      alt: 'Gymboree',
      title: 'Gymboree',
      target: '_blank',
      class: 'header__brand-tab-gymboree',
      active: false,
    },
  ],
  brandNames: ['The children place', 'Gymboree'],
};
