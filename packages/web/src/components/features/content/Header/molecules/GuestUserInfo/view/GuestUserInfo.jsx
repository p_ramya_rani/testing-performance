// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  getCreateAccountPayload,
  getLoginPayload,
  setLoyaltyLocation,
} from '@tcp/core/src/constants/analytics';
import { Image, Button, BodyCopy } from '@tcp/core/src/components/common/atoms';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { getIconPath, isCanada, getLabelValue } from '@tcp/core/src/utils';
import ACCOUNT_CONSTANTS from '@tcp/core/src/components/features/account/Account/Account.constants';

const AnalyticsNavigationText = ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationText;

const handleUserRewards = (userRewards) => {
  return userRewards % 1 ? userRewards : Math.floor(userRewards);
};

const GuestUserInfo = ({
  createAccount,
  login,
  openOverlay,
  triggerLoginCreateAccount,
  onLinkClick,
  userNameClick,
  isRememberedUser,
  userName,
  isDrawer,
  userPoints,
  userRewards,
  labels,
  loyaltyLocation,
}) => {
  const createAccountLabel = getLabelValue(labels, 'lbl_miniBag_createAccount', 'minibag');
  const loginLabel = getLabelValue(labels, 'lbl_header_login', 'header');
  const LoginLinkClick = (e) =>
    onLinkClick(
      {
        e,
        openOverlay,
        userNameClick,
        triggerLoginCreateAccount,
        navname: AnalyticsNavigationText.logIn,
      },
      login
    );

  return (
    <React.Fragment>
      {!isRememberedUser && (
        <>
          <Button
            nohover
            type="button"
            link
            id={createAccount}
            className="create-account-header-label"
            onClick={(e) =>
              onLinkClick(
                {
                  e,
                  openOverlay,
                  userNameClick,
                  triggerLoginCreateAccount,
                  navname: AnalyticsNavigationText.createAccount,
                },
                createAccount
              )
            }
            fontSizeVariation="large"
            anchorVariation="primary"
            data-overlayTarget={createAccount}
          >
            <ClickTracker
              name="create_account"
              onClick={() => {
                setLoyaltyLocation(loyaltyLocation);
              }}
              clickData={getCreateAccountPayload(loyaltyLocation)}
            >
              {createAccountLabel}
            </ClickTracker>
          </Button>
          <Button
            nohover
            type="button"
            link
            id={login}
            className="rightLink login-header-label"
            onClick={LoginLinkClick}
            fontSizeVariation="large"
            anchorVariation="primary"
            data-overlayTarget={login}
          >
            <ClickTracker
              name="log_in"
              onClick={() => {
                setLoyaltyLocation(loyaltyLocation);
              }}
              clickData={getLoginPayload(loyaltyLocation)}
            >
              {loginLabel}
            </ClickTracker>
          </Button>
        </>
      )}

      {isRememberedUser && (
        <BodyCopy component="div" className="account-info-section" tabIndex="0" id={login}>
          <ClickTracker name="log_in">
            <BodyCopy
              className="account-info user-name"
              component="div"
              role="button"
              onClick={LoginLinkClick}
              data-overlayTarget={login}
            >
              {`Hi, ${userName}`}
            </BodyCopy>

            {!isDrawer && (
              <Image
                alt="user"
                className="account-info"
                src={getIconPath('down_arrow_icon')}
                height="6px"
                onClick={LoginLinkClick}
              />
            )}

            {!isCanada() ? (
              <BodyCopy component="div">
                <div className="account-info user-points" id="account-info-user-points">
                  {`${userPoints} Points`}
                </div>
                <span className="account-info user-rewards rightLink">
                  {`$${handleUserRewards(userRewards)} Rewards`}
                </span>
              </BodyCopy>
            ) : null}
          </ClickTracker>
        </BodyCopy>
      )}

      {!isDrawer ? (
        <Image
          alt="user"
          className="usericon"
          src={getIconPath('user-icon-gray')}
          onClick={(e) =>
            onLinkClick({ e, openOverlay, userNameClick, triggerLoginCreateAccount }, login)
          }
        />
      ) : null}
    </React.Fragment>
  );
};

GuestUserInfo.propTypes = {
  createAccount: PropTypes.string.isRequired,
  login: PropTypes.string.isRequired,
  openOverlay: PropTypes.func.isRequired,
  userNameClick: PropTypes.bool.isRequired,
  onLinkClick: PropTypes.func.isRequired,
  triggerLoginCreateAccount: PropTypes.bool.isRequired,
  isDrawer: PropTypes.bool.isRequired,
  isRememberedUser: PropTypes.bool,
  userName: PropTypes.string,
  userPoints: PropTypes.string.isRequired,
  userRewards: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  loyaltyLocation: PropTypes.string,
};

GuestUserInfo.defaultProps = {
  isRememberedUser: false,
  userName: '',
  loyaltyLocation: 'global-header',
};
export default GuestUserInfo;
