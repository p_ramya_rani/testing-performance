import { BRAND_TABS_CONSTANTS } from './BrandTabs.constants';

export const getNavigationXHR = payload => {
  return {
    payload,
    type: BRAND_TABS_CONSTANTS.BRAND_TABS_NAVIGATION_XHR,
  };
};

export default {
  getNavigationXHR,
};
