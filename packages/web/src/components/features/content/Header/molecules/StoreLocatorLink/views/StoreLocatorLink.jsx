// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Anchor, Image, BodyCopy } from '@tcp/core/src/components/common/atoms';
import { setLocalStorage, getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { getIconPath, getLabelValue, getLocator, getStoreHours } from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import style from '../styles/StoreLocatorLink.style';
import ClickTracker from '../../../../../../common/atoms/ClickTracker';

const StoreLocatorLink = ({ className, labels, store, onLinkClick, isIconOnly }) => {
  let storeFromLocalStorage = store;
  if (store) {
    setLocalStorage({ key: 'defaultStore', value: JSON.stringify(store) });
  } else {
    storeFromLocalStorage = JSON.parse(getLocalStorage('defaultStore')) || '';
  }
  const basicInfo = (storeFromLocalStorage && storeFromLocalStorage?.basicInfo) || store?.basicInfo;
  const currentDate = new Date();
  const hours = storeFromLocalStorage && storeFromLocalStorage.hours;
  const storeTime = hours && getStoreHours(hours, labels, currentDate);
  const isInfoPresent = !!basicInfo && !!basicInfo.storeName;
  return (
    <React.Fragment>
      <ClickTracker
        onClick={onLinkClick}
        as={Anchor}
        dataLocator={getLocator('store_drawerlink')}
        fontSizeVariation="small"
        anchorVariation="primary"
        to="/store-locator"
        className={`${className} store-locator-link`}
        clickData={{
          customEvents: ['event80'],
          eVar65: 'storelocator',
        }}
      >
        <div
          className={`storelocatorlink__container${!isIconOnly && !isInfoPresent ? '--fav' : ''}`}
        >
          <div className="storelocatorlink__img">
            <Image
              src={getIconPath('marker-icon')}
              alt="Store Locator"
              className="storelocator--image"
              data-locator={getLocator('store_markericon')}
            />
          </div>
          {!isIconOnly && (
            <div className="storelocatorlink__detail">
              <BodyCopy
                component="div"
                data-locator={getLocator('store_namelabel')}
                fontFamily="secondary"
                fontSize="fs13"
                className="storelocatorlink__detail__storename"
              >
                {basicInfo?.storeName}
              </BodyCopy>
              <BodyCopy
                component="div"
                data-locator={getLocator('store_storetime')}
                fontFamily="secondary"
                fontSize="fs10"
                className="storelocatorlink__detail__storetime"
              >
                {storeTime}
              </BodyCopy>
            </div>
          )}
          {!isIconOnly && !basicInfo && (
            <BodyCopy
              component="div"
              data-locator={getLocator('store_findastore')}
              fontFamily="secondary"
              fontSize="fs13"
              className="storelocatorlink__detail"
            >
              <div className="store-welcome-txt ">
                {getLabelValue(labels, 'lbl_storelocator_welcomeTxt')}
              </div>
              {getLabelValue(labels, 'lbl_storelocator_findAStoreLink')}
            </BodyCopy>
          )}
        </div>
      </ClickTracker>
    </React.Fragment>
  );
};

StoreLocatorLink.propTypes = {
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  store: PropTypes.shape({
    basicInfo: PropTypes.shape({}),
    hours: PropTypes.shape({
      regularHours: PropTypes.shape([]),
      regularAndHolidayHours: PropTypes.shape([]),
      holidayHours: PropTypes.shape([]),
    }),
    features: PropTypes.shape({}),
  }),
  onLinkClick: PropTypes.func,
  isIconOnly: PropTypes.bool,
};

StoreLocatorLink.defaultProps = {
  store: {
    basicInfo: {
      id: '',
      storeName: '',
      phone: '',
      coordinates: {},
      address: {},
    },
    hours: {
      regularHours: [],
      regularAndHolidayHours: [],
      holidayHours: [],
    },
  },
  onLinkClick: () => {},
  isIconOnly: false,
};

export default withStyles(StoreLocatorLink, style);

export { StoreLocatorLink as StoreLocatorLinkVanilla };
