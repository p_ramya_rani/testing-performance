// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Anchor, Image } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import { SNJ } from '@tcp/core/src/constants/brands.constants';
import { getIconPath, getLocator, urlContainsQuery, getBrand } from '@tcp/core/src/utils';
import { BRAND_TO_UPDATE_COOKIE } from '../container/BrandTabs.constants';
import style from '../BrandTabs.style';

const applyBrandClass = (logoClass) => {
  const [brandId] = logoClass.split('-').reverse();
  return `brand-tab-${brandId}`;
};

const handleBrandClick = (
  e,
  logoClassName,
  cookieToken,
  active,
  defaultUrl,
  target,
  getNavigationXHR
) => {
  e.preventDefault();
  const url = e.currentTarget.href || defaultUrl;
  const [brandId] = logoClassName.split('-').reverse();
  const brandName = BRAND_TO_UPDATE_COOKIE[brandId];
  getNavigationXHR({ brandName, cookieToken, active, url, target });
};

const getUpdatedUrl = ({ url, cookieToken }) => {
  if (urlContainsQuery(url)) {
    return `${url}&ct=${cookieToken}`;
  }
  return `${url}?ct=${cookieToken}`;
};

const getNavigateToValue = ({ active = '', cookieToken = '', url = '' }) => {
  return !active && cookieToken ? getUpdatedUrl({ url, cookieToken }) : url;
};

const isActiveCheckout = (active, isCheckout) => active || isCheckout;

const BrandImage = ({ brandId, logoClass, active, isCheckout, logoClassAlt }) => {
  return (
    <Image
      alt={brandId}
      className={logoClass}
      src={
        isActiveCheckout(active, isCheckout) ? getIconPath(logoClass) : getIconPath(logoClassAlt)
      }
      data-locator={getLocator(logoClass)}
    />
  );
};

const getClassName = ({ active, logoClass }) => {
  return active
    ? `header-topnav__brand-tabs--activeTab ${applyBrandClass(logoClass)}`
    : applyBrandClass(logoClass);
};

const BrandTabs = ({
  className,
  data,
  cookieToken,
  isCheckout,
  getNavigationXHR,
  isSNJEnabled,
}) => {
  let brandId = getBrand();
  if (brandId && brandId.toUpperCase() === 'GYM') {
    brandId = 'gymboree';
  }
  return (
    <Fragment>
      <div className={className}>
        {data &&
          data.map((tabData) => {
            const { class: logoClass, target, url } = tabData;
            // Don't render SNJ Tab if SNJ kill switch is disabled
            if (!isSNJEnabled && logoClass.indexOf(SNJ.toLowerCase()) > -1) {
              return null;
            }
            const logoClassAlt = `${logoClass}-alt`;
            let active = false;
            if (
              logoClass === `header__brand-tab--${brandId}` ||
              logoClass === `header__brand-tab-${brandId}`
            ) {
              active = true;
            }
            const styleName = getClassName({ active, logoClass });
            const brandImage = BrandImage({
              brandId,
              logoClass,
              active,
              isCheckout,
              logoClassAlt,
            });
            return isSNJEnabled && !active ? (
              <Anchor
                className={styleName}
                handleLinkClick={(e) =>
                  handleBrandClick(e, logoClass, cookieToken, active, url, target, getNavigationXHR)
                }
                key={logoClass}
                noLink
                url={url}
              >
                {brandImage}
              </Anchor>
            ) : (
              <Anchor
                className={styleName}
                to={getNavigateToValue({ active, cookieToken, url })}
                target={target}
                key={logoClass}
              >
                {brandImage}
              </Anchor>
            );
          })}
      </div>
    </Fragment>
  );
};

BrandImage.propTypes = {
  brandId: PropTypes.string.isRequired,
  logoClass: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  isCheckout: PropTypes.bool.isRequired,
  logoClassAlt: PropTypes.string.isRequired,
};

BrandTabs.propTypes = {
  className: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]))
  ).isRequired,
  cookieToken: PropTypes.string,
  isCheckout: PropTypes.bool,
  getNavigationXHR: PropTypes.func.isRequired,
  isSNJEnabled: PropTypes.bool,
};

BrandTabs.defaultProps = {
  cookieToken: null,
  isCheckout: false,
  isSNJEnabled: false,
};

export default errorBoundary(withStyles(BrandTabs, style));
export { BrandTabs as BrandTabsVanilla };
