// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel';
import { Anchor, RichText } from '@tcp/core/src/components/common/atoms';
import CarouselConfig from '@tcp/web/src/config/carousel';
import { getLocator } from '@tcp/core/src/utils';
import style from '../PromotionalArea.style';

const PromotionalArea = ({ className, data, mobile, maxLoopCount }) => {
  const carouselConfig = mobile
    ? CarouselConfig.CAROUSEL_OPTIONS
    : CarouselConfig.CAROUSEL_FADE_OPTIONS;
  const wrapperClass = mobile
    ? 'header-topnav__promo-area--mobile'
    : 'header-topnav__promo-area--tablet';

  if (maxLoopCount) {
    carouselConfig.maxLoopCount = maxLoopCount;
  }
  return data ? (
    <div className={className}>
      <div className={wrapperClass}>
        <Carousel
          options={carouselConfig}
          carouselConfig={{ type: 'dark', arrow: 'small' }}
          className={mobile ? 'promotinal-carousel-wrapper' : ''}
        >
          {data.map((promotion) => {
            const {
              richText: { text, __typename },
              link: { url, target },
            } = promotion;
            const bgClass =
              mobile && promotion && promotion.class && promotion.class.class
                ? `${promotion.class.class}`
                : '';
            const htmlTemplate = `<span>${text}</span>`;
            return (
              <div className={`${bgClass} ${mobile ? 'top-nav-promo-area' : ''}`}>
                <Anchor to={url} target={target}>
                  <RichText
                    className="header-topnav__promo-area-content"
                    key={__typename}
                    richTextHtml={htmlTemplate}
                    dataLocator={getLocator('global_promoareaimg')}
                  />
                </Anchor>
              </div>
            );
          })}
        </Carousel>
      </div>
    </div>
  ) : null;
};

PromotionalArea.propTypes = {
  className: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  mobile: PropTypes.bool,
  maxLoopCount: PropTypes.number,
};

PromotionalArea.defaultProps = {
  mobile: PropTypes.bool,
  maxLoopCount: 0,
};

export default withStyles(PromotionalArea, style);
export { PromotionalArea as PromotionalAreaVanilla };
