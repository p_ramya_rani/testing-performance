// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const ICON_SIZE = '60px';

const HeaderPromoStyles = css`
  background: ${props => props.theme.colors.PRIMARY.PALEGRAY};
  .slick-prev,
  .slick-next {
    z-index: 1;
  }
  @media ${props => props.theme.mediaQuery.mediumMax} {
    .slick-track {
      display: flex !important;
    }

    .slick-slide {
      height: inherit !important;
    }
    .slick-slide > div:first-child {
      height: 100%;
    }
  }
  @media ${props => props.theme.mediaQuery.large} {
    background: ${props => props.theme.colors.WHITE};
  }

  .styled-text,
  .styled-text-line {
    display: block;
    /* TODO - Remove the style1, style2, style3 when the styles start coming up from CMS */
    &.style1,
    &[class*='orange-'] {
      color: ${props => props.theme.colors.BRAND.BOYS};
    }
    &.style2,
    &[class*='green-'] {
      color: ${props => props.theme.colors.PRIMARY.GREEN};
    }
    &.style3,
    &[class*='blue-'] {
      color: ${props => props.theme.colors.BRAND.PRIMARY};
    }
  }

  .header-promo-item__content {
    width: 100%;
    text-align: center;
    @media ${props => props.theme.mediaQuery.medium} {
      width: auto;
    }
    @media ${props => props.theme.mediaQuery.large} {
      width: calc(100% - ${ICON_SIZE});
    }
  }

  &.header__promo-area--mobile {
    @media ${props => props.theme.mediaQuery.large} {
      display: none;
    }
    .slick-slider {
      padding: 0;
      .header-promo-item__content {
        padding: 0 38px;
      }
    }
  }
  &.header__promo-area--desktop {
    display: none;
    @media ${props => props.theme.mediaQuery.large} {
      display: block;
    }
  }

  .header-promo__item {
    background-color: ${props => props.theme.colors.PRIMARY.PALEGRAY};
    display: flex !important;
    align-items: center;
    min-height: 42px;
    height: 100%;
    @media ${props => props.theme.mediaQuery.medium} {
      min-height: ${ICON_SIZE};
      justify-content: center;
    }
    &_green {
      background-color: ${props => props.theme.colors.NAVIGATION.GREEN};
    }
    &_yellow {
      background-color: ${props => props.theme.colors.NAVIGATION.YELLOW};
    }
    &_black {
      background-color: ${props => props.theme.colors.BLACK};
      .styled-text,
      .styled-text-line {
        color: ${props => props.theme.colors.WHITE};
      }
    }
    &_bgImage {
      background-image: url(${props => props.placeCashMobileUrl});
      @media ${props => props.theme.mediaQuery.large} {
        background-image: url(${props => props.placeCashDesktopUrl});
      }
    }
  }

  .header-promo-item__icon {
    height: ${ICON_SIZE};
    width: ${ICON_SIZE};
    align-items: center;
    justify-content: center;
    display: none;
    @media ${props => props.theme.mediaQuery.medium} {
      display: flex;
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
    }
    &[class*='header__promo-text-banner'] {
      background-color: ${props => props.theme.colors.BRAND.BOYS};
    }
    &[class*='orange-'] {
      background-color: ${props => props.theme.colors.BRAND.BOYS};
    }
    &[class*='green-'] {
      background-color: ${props => props.theme.colors.PRIMARY.GREEN};
    }
    &[class*='blue-'] {
      background-color: ${props => props.theme.colors.BRAND.PRIMARY};
    }
  }
`;

export default HeaderPromoStyles;
