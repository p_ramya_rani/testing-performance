import { handleBrandNavigation } from '../container/BrandTabs.saga';

describe('BrandTab Saga', () => {
  it('should call handleBrandNavigation generator function of BrandTab saga', () => {
    const handleBrandNav = handleBrandNavigation({});
    handleBrandNav.next();
    handleBrandNav.next();
    expect(handleBrandNav.next().done).toBe(true);
  });
});
