// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  margin-top: 5px;
  @media ${(props) => props.theme.mediaQuery.medium} {
    margin-top: 12px;
  }
  display: flex;

  @media ${(props) => props.theme.mediaQuery.large} {
    justify-content: left;
  }

  ${(props) => !props.isSNJEnabled && `justify-content: space-between;`}
  a.brand-tab-tcp,
  a.brand-tab-gymboree,
  a.brand-tab-snj {
    height: 32px;
    width: 73px;
    display: flex;
    align-items: center;
    ${(props) => props.isSNJEnabled && `margin-right: 5px;`}
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-right: 10px;
      height: 34px;
      width: 80px;
    }
  }
  .header__brand-tab-gymboree {
    height: 20px;
    width: 100%;
  }
  .header-topnav__brand-tabs--activeTab {
    .header__brand-tab-gymboree {
      height: 28px;
    }
  }

  .header__brand-tab--tcp {
    height: 30px;
    width: 100%;
  }

  .header__brand-tab-snj {
    height: 40px;
    width: 100%;
  }

  .brand-tab-gymboree:not(.header-topnav__brand-tabs--activeTab) {
    background-color: ${(props) =>
      props.isCheckout ? props.theme.colorPalette.white : props.theme.colorPalette.orange[800]};
  }
  .brand-tab-tcp:not(.header-topnav__brand-tabs--activeTab) {
    background-color: ${(props) =>
      props.isCheckout ? props.theme.colorPalette.white : props.theme.colors.BRAND.PRIMARY};
  }
  .brand-tab-snj:not(.header-topnav__brand-tabs--activeTab) {
    background-image: linear-gradient(
      138deg,
      ${(props) => props.theme.colorPalette.white},
      ${(props) => props.theme.colorPalette.white} 60%,
      ${(props) => props.theme.colorPalette.purpleCollection[500]} 152%
    );
  }

  a {
    border-radius: 5px 5px 0px 0px;
    display: inline-block;
    padding: 9px 15px;
    position: relative;

    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 8px 15px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 8px 20px;
    }

    &.header-topnav__brand-tabs--activeTab {
      background-color: ${(props) => props.theme.colors.WHITE};
    }

    &.header-topnav__brand-tabs--activeTab:before,
    &.header-topnav__brand-tabs--activeTab:after {
      content: '';
      position: absolute;
      height: 10px;
      width: 20px;
      bottom: 0;
      z-index: 1;
    }

    &.header-topnav__brand-tabs--activeTab:after {
      right: -20px;
      border-radius: 0 0 0 10px;
      box-shadow: -10px 0 0 0 ${(props) => props.theme.colors.WHITE};
    }

    &.header-topnav__brand-tabs--activeTab:before {
      left: -20px;
      border-radius: 0 0 10px 0;
      box-shadow: 10px 0 0 0 ${(props) => props.theme.colors.WHITE};
    }
  }
`;
