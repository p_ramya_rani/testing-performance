// 9fbef606107a605d69c0edbcd8029e5d
import { getLabelValue } from '@tcp/core/src/utils';

const getTcpBrandName = state =>
  getLabelValue(state.Labels, 'lbl_brandlogo_tcpAltText', 'accessibility', 'global');

const getGymBrandName = state =>
  getLabelValue(state.Labels, 'lbl_brandlogo_gymAltText', 'accessibility', 'global');

const getCookieToken = state => state.NavigateXHR.getIn(['cookie_token', 'encodedcookie']);

export default {
  getCookieToken,
  getTcpBrandName,
  getGymBrandName,
};
