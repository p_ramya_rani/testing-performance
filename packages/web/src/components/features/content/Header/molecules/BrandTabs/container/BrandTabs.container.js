// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import NavigateXHRSelector from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.selectors';
import BRANDTABS_SELECTORS from './BrandTabs.selectors';
import { getNavigationXHR } from './BrandTabs.actions';
import BrandTabsView from '../views';

export const mapDispatchToProps = dispatch => {
  return {
    getNavigationXHR: payload => {
      dispatch(getNavigationXHR(payload));
    },
  };
};

export const mapStateToProps = state => {
  return {
    tcpBrandName: BRANDTABS_SELECTORS.getTcpBrandName(state),
    gymBrandName: BRANDTABS_SELECTORS.getGymBrandName(state),
    cookieToken: NavigateXHRSelector.getCookieToken(state),
    isSNJEnabled: getIsSNJEnabled(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BrandTabsView);
