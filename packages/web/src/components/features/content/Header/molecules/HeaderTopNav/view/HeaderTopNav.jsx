// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import withLazyLoad from '@tcp/core/src/components/common/hoc/withLazyLoad';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { routerPush, getIconPath } from '@tcp/core/src/utils';
import { routeToPage } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import {
  CUSTOMER_HELP_GA,
  CUSTOMER_HELP_ROUTES,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import BrandTabs from '../../BrandTabs';
import PromotionalArea from '../../PromotionalArea';
import StoreLocatorLink from '../../StoreLocatorLink';
import HeaderTopNavStyle from '../HeaderTopNav.style';

const CountrySelectorView = dynamic(() => import('../../CountrySelector'));

const CountrySelector = withLazyLoad(CountrySelectorView);

const HeaderTopNav = ({
  className,
  brandTabs,
  promoMessageWrapper,
  openOverlay,
  isUserLoggedIn,
  labels,
  store,
  closeNavDrawer,
  isSNJEnabled,
  isCSHEnabled,
  trackAnalyticsClick,
  mergeAccountReset,
  resetEmailConfirmationRequest,
}) => {
  const onLinkClick = (e) => {
    e.preventDefault();
    if (!isUserLoggedIn) openOverlay({ state: true });
    else routerPush('/account?id=orders', '/account?id=orders');
  };
  const navigateToCSH = (e) => {
    e.preventDefault();
    trackAnalyticsClick({ orderHelpLocation: CUSTOMER_HELP_GA.HEADER_LINK_TRACK.CD126 });
    mergeAccountReset();
    resetEmailConfirmationRequest();
    routeToPage(CUSTOMER_HELP_ROUTES.LANDING_PAGE);
  };
  const brandTabsClassName = `header-topnav__brand-tabs${isSNJEnabled ? '_SNJ' : ''}`;
  return (
    // eslint-disable-next-line
    <div className={className} onClick={closeNavDrawer}>
      <PromotionalArea mobile data={promoMessageWrapper} maxLoopCount={1} />
      <div className="header-topnav__row content-wrapper">
        <div className={brandTabsClassName}>
          <BrandTabs data={brandTabs} />
        </div>
        <div className="header-topnav__promo-area">
          <PromotionalArea mobile={false} data={promoMessageWrapper} maxLoopCount={10} />
        </div>
        <div className="header-topnav__track-order">
          <div className="header-option">
            {isCSHEnabled && (
              <BodyCopy
                component="button"
                anchorVariation="primary"
                font-size="fs12"
                fontFamily="secondary"
                fontWeight="bold"
                fontSizeVariation="medium"
                id="orderAssistance"
                color="text.dark"
                onClick={(e) => navigateToCSH(e)}
                className="order-assistance no-border-button elem-pl-SM"
                title={labels.helpCenter && labels.helpCenter.lbl_customer_support}
              >
                <Image
                  className="header-icon elem-pr-XS elem-mt-XXXS"
                  width="20px"
                  height="20px"
                  src={getIconPath('question-circle')}
                  alt=""
                />
                <span className="header-option-text">
                  {labels.helpCenter && labels.helpCenter.lbl_customer_support}
                </span>
              </BodyCopy>
            )}
            <BodyCopy
              component="button"
              dataLocator="track_order_header"
              anchorVariation="primary"
              font-size="fs12"
              fontFamily="secondary"
              fontWeight="bold"
              fontSizeVariation="medium"
              id="trackOrder"
              color="text.dark"
              onClick={(e) => onLinkClick(e)}
              className="track-order no-border-button elem-pl-SM"
              title={labels.trackOrder && labels.trackOrder.lbl_trackOrder_trackOrderHeaderLink}
            >
              <Image
                className="header-icon elem-pr-XS elem-mt-XXXS"
                width="20px"
                height="21.2px"
                src={getIconPath('package')}
                alt=""
              />
              <span className="header-option-text">
                {labels.trackOrder && labels.trackOrder.lbl_trackOrder_trackOrderHeaderLink}
              </span>
            </BodyCopy>
          </div>
          <CountrySelector />
        </div>
        <div className={`header-topnav__storelocator${isSNJEnabled ? '_SNJ' : ''}`}>
          <StoreLocatorLink
            store={store}
            labels={labels && { ...labels.store, ...labels.StoreDetail }}
          />
        </div>
      </div>
    </div>
  );
};

HeaderTopNav.propTypes = {
  className: PropTypes.string.isRequired,
  brandTabs: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  promoMessageWrapper: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  openOverlay: PropTypes.func.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  labels: PropTypes.objectOf(
    PropTypes.objectOf({
      lbl_trackOrder_trackOrderHeaderLink: PropTypes.string,
    })
  ),
  store: PropTypes.shape({
    basicInfo: PropTypes.shape({}),
    hours: PropTypes.shape({
      regularHours: PropTypes.shape([]),
      regularAndHolidayHours: PropTypes.shape([]),
      holidayHours: PropTypes.shape([]),
    }),
    features: PropTypes.shape({}),
  }),
  closeNavDrawer: PropTypes.func,
  isCSHEnabled: PropTypes.bool,
  isSNJEnabled: PropTypes.bool.isRequired,
  trackAnalyticsClick: PropTypes.func,
  resetEmailConfirmationRequest: PropTypes.func,
  mergeAccountReset: PropTypes.func,
};

HeaderTopNav.defaultProps = {
  labels: {
    trackOrder: {
      lbl_trackOrder_trackOrderHeaderLink: '',
    },
    helpCenter: {
      lbl_order_help: '',
    },
  },
  store: {
    basicInfo: {
      id: '',
      storeName: '',
      phone: '',
      coordinates: {},
      address: {},
    },
    hours: {
      regularHours: [],
      regularAndHolidayHours: [],
      holidayHours: [],
    },
    features: {},
  },
  closeNavDrawer: () => {},
  isCSHEnabled: false,
  trackAnalyticsClick: () => {},
  resetEmailConfirmationRequest: () => {},
  mergeAccountReset: () => {},
};

export default withStyles(HeaderTopNav, HeaderTopNavStyle);
export { HeaderTopNav as HeaderTopNavVanilla };
