// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const style = css`
  box-sizing: border-box;
  padding: 14px 0;
  position: relative;
  text-align: center;

  .flexbox {
    display: flex;
  }

  .hamburger-menu {
    outline: none;
    cursor: pointer;
    width: 22px;
    height: 22px;
  }
  .header-middle-nav {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin: 14px 0;
    }
  }
  .header-middle-nav-search {
    margin-right: 3%;
    width: 8%;
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      display: flex;
      align-items: center;
      justify-content: space-between;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 24%;
      margin-right: 0;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 15%;
      margin-right: 1.3%;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      .store-locator-link {
        display: none;
      }
    }

    .storelocatorlink__container,
    .storelocatorlink__container--fav {
      display: none;

      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        display: flex;
      }
    }
  }
  .header-middle-login-section {
    justify-content: flex-end;
    align-items: center;
    display: inline-flex;
    width: 89%;
    ${(props) =>
      props.isSNJEnabled &&
      `@media ${props.theme.mediaQuery.smallOnly} {
      justify-content: space-between;
    }`}
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      margin-top: auto;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 75%;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 41%;
    }
  }

  .headerSearchBox-wrapper {
    ${(props) =>
      props.showCondensedHeader
        ? ` width:1440px;
            position:fixed;
            z-index: 1200;
            top: 14px;
            margin: 0 auto;
            text-align: center;`
        : ``};
  }

  .header-middle-login-section .headerSearchBox {
    ${(props) =>
      props.showCondensedHeader && props.isSearchOpen
        ? `position: absolute;
          right: 85px;
          width: 437px;
         `
        : `position: relative;
        `};
    flex-grow: unset;
    height: 100%;
    @media ${(props) => props.theme.mediaQuery.large} {
      height: 40px;
    }
  }
  .search-input-wrapper {
    margin-right: 3px;
    ${(props) =>
      !props.isSNJEnabled &&
      `
    @media ${props.theme.mediaQuery.small} {
      margin-right: 30px;
    }`}
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-right: 54px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: 37px;
    }

    ${(props) =>
      props.isSNJEnabled &&
      `
      @media ${props.theme.mediaQuery.smallOnly} {
        width: 180px;
      }

      @media (max-width: ${props.theme.breakpoints.small}) {
        width: 175px;
      }
    `}
  }

  .account-info-section {
    display: inline-block;
    line-height: normal;
  }
  .carrot-down-icon {
    display: inline-block;
    cursor: pointer;
    margin-left: 9px;
    transform: rotate(0deg);
    transition: transform 0.1s linear;
  }
  .carrot-up-icon {
    display: inline-block;
    cursor: pointer;
    margin-left: 9px;
    transform: rotate(180deg);
    transition: transform 0.15s linear;
  }

  .account-info {
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    display: none;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: inline-flex;
      align-items: center;
    }
  }

  .user-name {
    cursor: pointer;
    font-size: ${(props) => props.theme.typography.fontSizes.fs13};
    text-align: left;
    &:hover {
      color: ${(props) => props.theme.colorPalette.blue[500]};
    }
  }
  .user-points,
  .user-rewards {
    cursor: pointer;
    font-size: ${(props) => props.theme.typography.fontSizes.fs10};
    color: ${(props) =>
      props.isUserPlcc ? props.theme.colorPalette.blue[500] : props.theme.colorPalette.orange[800]};
  }

  .user-icon-container {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .user-rewards-pill {
    background-color: ${(props) =>
      props.isUserPlcc
        ? props.theme.colorPalette.userTheme.plcc
        : props.theme.colorPalette.userTheme.mpr};
    margin-top: -1px;
    padding: 2px 8px 0 8px;
    line-height: 1.1;
    border-radius: 6px;
    font-size: 10px;
    position: absolute;
    bottom: 12px;
  }

  .usericon {
    cursor: pointer;
    display: inline-flex;
    vertical-align: baseline;
    width: 23px;
    margin-top: 3px;
    max-width: initial;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
  }

  .usericon-reduced {
    width: 18px;
    margin-top: 0;
  }

  .product-image {
    height: 25px;
    width: 20px;
    margin-left: ${(props) => (props.isSNJEnabled ? '9px' : '7px')};
    ${(props) =>
      !props.isSNJEnabled &&
      `
      @media ${props.theme.mediaQuery.small} {
        margin-left: 25px;
      }
    `}
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 23px;
    }
    vertical-align: middle;
  }
  #login {
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }
  #createaccount {
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .rightLink {
    border-left: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
    box-sizing: border-box;
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    min-width: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    border-radius: 0px;
  }
  .header-middle-login-section a {
    position: relative;
  }
  .cartCount {
    background: ${(props) =>
      props.theme.isGymboree
        ? props.theme.colorPalette.primary.dark
        : props.theme.colorPalette.blue['800']};
    color: ${(props) => props.theme.colors.WHITE};
    border-radius: 8px;
    padding: 2px 5px;
    vertical-align: bottom;
    position: absolute;
    bottom: -5px;
    right: -10px;
  }
  @media ${(props) => props.theme.mediaQuery.mediumMax} {
    padding: 16px 0;
    text-align: left;
    .header-brand__home-logo--brand {
      display: none;
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    padding: 31px 0 0;
    .hamburger-menu {
      display: none;
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallMax} {
    .storelocatorlink__container,
    .storelocatorlink__container--fav {
      display: block;
    }
  }

  .header-middle-nav-storelocator {
    display: flex;
    align-items: center;

    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      display: none;
    }
  }

  .create-account-header-label,
  .login-header-label {
    display: none;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: inline-flex;
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .mini-bag-icon {
      width: 25px;
    }
  }

  .mobile-locator-icon {
    position: relative;
    top: 2px;
    display: none;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: block;
      right: 2px;
    }
    @media (max-width: ${(props) => props.theme.breakpoints.small}) {
      display: block;
      right: 2px;
    }
  }
`;

export const customHeaderStyle = css`
  .Modal_Heading {
    border-bottom: none;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    font-size: ${(props) => props.theme.typography.fontSizes.fs22};
    display: flex;
    justify-content: center;
    height: auto;
    padding-bottom: 0;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: flex;
      justify-content: center;
      height: auto;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .close-modal {
    display: none;
  }
`;

export default style;
