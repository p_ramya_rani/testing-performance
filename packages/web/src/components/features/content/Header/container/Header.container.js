// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import {
  openNavigationDrawer,
  closeNavigationDrawer,
  openMiniBag,
  closeMiniBag,
} from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import { showNavigationFooter } from '@tcp/core/src/components/features/content/Navigation/container/Navigation.actions';
import { setTrackOrderModalMountedState } from '@tcp/core/src/components/features/account/TrackOrder/container/TrackOrder.actions';
import {
  openOverlayModal,
  closeOverlayModal,
} from '@tcp/core/src/components/features/account/OverlayModal/container/OverlayModal.actions';
import { loginModalOpenState } from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.selectors';
import { getOpenState } from '@tcp/core/src/components/features/account/OverlayModal/container/OverlayModal.selectors';
import {
  getIsChangeRecalForEcom,
  getIsCSHEnabled,
  getIsShowNavAnimation,
  getIsSNJEnabled,
  getIsNewPDPEnabled,
  getIsNewMiniBag,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getIsGuest,
  isPlccUser,
  isRememberedUser,
  getUserName,
  getUserLoggedInState,
  getCurrentPointsState,
  getTotalRewardsState,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import BAGPAGE_SELECTORS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {
  getEnableFullPageAuth,
  getMinibagOnHoverEnabled,
} from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import { getIsPickupModalOpen } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import {
  getModalState,
  getIsKeepMinibagCloseQV,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import { getCartItemCount } from '@tcp/core/src/utils/cookie.util';
import {
  getIsCartItemsSFL,
  getIsMiniBagOpen,
} from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import {
  closeQuickViewModal,
  updateMiniBagCloseStatusQV,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import { closePickupModal } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import { setLoginModalMountedState } from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.actions';
import {
  isOpenAddedToBag,
  getIsKeepMinibagClose,
} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import { updateMiniBagCloseStatus } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';
import { getTrackOrderMountedState } from '@tcp/core/src/components/features/account/TrackOrder/container/TrackOrder.selectors';
import { selectOrder } from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.actions';
import { mergeAccountReset } from '@tcp/core/src/components/features/account/CustomerHelp/organisms/MergeAccounts/container/MergeAccounts.actions';
import { resetEmailConfirmationRequest } from '@tcp/core/src/components/features/account/CustomerHelp/organisms/EmailConfirmation/container/EmailConfirmation.action';
import HeaderView from '../views';

const mapStateToProps = (state) => {
  const { Header } = state;
  return {
    loyaltyPromoBanner: Header.loyaltyPromoBanner,
    brandTabs: Header.brandTabs,
    topPromoBanner: Header.topPromoBanner,
    promoMessageWrapper: Header.promoMessageWrapper,
    headerPromoTextArea: Header.promoTextBannerCarousel,
    headerPromoHtmlArea: Header.promoHtmlBannerCarousel,
    navigationDrawer: Header.navigationDrawer,
    isUserGuest: getIsGuest(state),
    isUserPlcc: isPlccUser(state),
    userName: getUserName(state),
    isRememberedUser: isRememberedUser(state),
    userPoints: getCurrentPointsState(state),
    userRewards: getTotalRewardsState(state),
    isLoggedIn: getUserLoggedInState(state),
    cartItemCount: getCartItemCount(),
    totalItems: BAGPAGE_SELECTORS.getTotalItems(state),
    globalLabels: state.Labels.global,
    storeLabels: state.Labels.StoreLocator,
    favStore: state.User && state.User.get('defaultStore'),
    isPickupModalOpen: getIsPickupModalOpen(state),
    isQVModalOpen: getModalState(state),
    isOpenOverlay: getOpenState(state),
    isOpenAddedToBag: isOpenAddedToBag(state),
    isCartItemSFL: getIsCartItemsSFL(state),
    showMiniBag: getIsMiniBagOpen(state),
    trackOrderMountedState: getTrackOrderMountedState(state),
    isDrawerOpen: state.Header.navigationDrawer && state.Header.navigationDrawer.open,
    showFullPageAuth: getEnableFullPageAuth(state),
    isECOM: BAGPAGE_SELECTORS.getECOMStatus(state),
    isRecalculateTaxes: getIsChangeRecalForEcom(state),
    loginModalMountedState: loginModalOpenState(state),
    isSNJEnabled: getIsSNJEnabled(state),
    isCSHEnabled: getIsCSHEnabled(state),
    minibagOnHoverEnabled: getMinibagOnHoverEnabled(state),
    isShowNavAnimation: getIsShowNavAnimation(state),
    isNewPDPEnabled: getIsNewPDPEnabled(state),
    isNewMiniBag: getIsNewMiniBag(state),
    isKeepMinibagCloseState: getIsKeepMinibagClose(state),
    isKeepMinibagCloseStateQV: getIsKeepMinibagCloseQV(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    openNavigationDrawer: (id) => {
      dispatch(openNavigationDrawer(id));
    },
    closeNavigationDrawer: () => {
      dispatch(showNavigationFooter());
      dispatch(closeNavigationDrawer());
    },
    openMiniBagDispatch: () => {
      dispatch(openMiniBag());
    },
    closeMiniBagDispatch: () => {
      dispatch(closeMiniBag());
    },
    openOverlay: (component) => dispatch(openOverlayModal(component)),
    closedOverlay: (payload) => {
      dispatch(closeOverlayModal(payload));
    },
    openTrackOrderOverlay: (payload) => dispatch(setTrackOrderModalMountedState(payload)),
    setClickAnalyticsData: (payload) => {
      dispatch(setClickAnalyticsData(payload));
    },
    onRouteChangeGlobalActions: () => {
      dispatch(closeQuickViewModal({ isModalOpen: false }));
      dispatch(closePickupModal({ isModalOpen: false }));
      dispatch(setLoginModalMountedState({ state: false, isAccountCardVisible: true }));
    },
    selectOrderAction: () => {
      dispatch(selectOrder({}));
    },
    setLoginModalMountState: (payload) => {
      dispatch(setLoginModalMountedState(payload));
    },
    trackAnalyticsClick: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 100);
    },
    mergeAccountReset: () => {
      dispatch(mergeAccountReset());
    },
    resetEmailConfirmationRequest: () => {
      dispatch(resetEmailConfirmationRequest());
    },
    updateMiniBagCloseStatusActn: (payload) => {
      dispatch(updateMiniBagCloseStatus(payload));
    },
    updateMiniBagCloseStatusActnQV: (payload) => {
      dispatch(updateMiniBagCloseStatusQV(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderView);
