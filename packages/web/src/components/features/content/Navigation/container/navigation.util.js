/**
 * @function isItemInRightPlace
 * @param {*} orderNos
 * @param {*} indexOfCurrent
 * @param {*} startingPoint
 * @param {*} i
 * @returns boolean value indicating the ith item is in its correct location or not
 */
export const isItemInRightPlace = (orderNos, indexOfCurrent, startingPoint, i) => {
  return orderNos[i] && indexOfCurrent !== -1 && orderNos[i] - startingPoint - 1 === indexOfCurrent;
};

/**
 * @function reOrderderingArray
 * @param {*} startingPoint
 * @param {*} navigationData
 * @param {*} variation
 * @param {*} orderNosArray
 * @returns Reordered Array based on variation
 */
export const reOrderderingArray = (startingPoint, navigationData, variation, orderNosArray) => {
  const orderNos = orderNosArray;
  let isReordered = false;
  while (!isReordered) {
    for (let i = startingPoint; i < navigationData.length; i += 1) {
      const indexOfCurrent = variation.indexOf(navigationData?.[i]?.categoryContent?.name);
      if (indexOfCurrent !== -1 && orderNos[i] !== startingPoint + indexOfCurrent + 1) {
        orderNos[i] = startingPoint + indexOfCurrent + 1;
        break;
      }
    }
    let noOfItemsOfInterest = variation.length;
    for (let i = startingPoint; i < navigationData.length; i += 1) {
      const indexOfCurrent = variation.indexOf(navigationData?.[i]?.categoryContent?.name);
      if (isItemInRightPlace(orderNos, indexOfCurrent, startingPoint, i)) {
        noOfItemsOfInterest -= 1;
      }
      if (noOfItemsOfInterest === 0) {
        isReordered = true;
        break;
      }
    }
  }
  return orderNos;
};
/**
 * @function generateOrderNos
 * @param {*} arr
 * @param {*} variation
 * @returns String with Order of all items for css based on variation required
 */
export const generateOrderNos = (arr, variation) => {
  const navigationData = arr;
  let startingPoint = 0;
  let orderNos = [];
  for (let i = 0; i < navigationData.length; i += 1) {
    const indexOfCurrent = variation.indexOf(navigationData?.[i]?.categoryContent?.name);
    if (indexOfCurrent !== -1) {
      startingPoint = i;
      break;
    }
  }
  for (let i = 0; i < startingPoint; i += 1) {
    orderNos[i] = i + 1;
  }

  orderNos = reOrderderingArray(startingPoint, navigationData, variation, orderNos);

  let maxOrderNoTillNow = 0;
  for (let i = 0; i < orderNos.length; i += 1) {
    if (orderNos[i] > maxOrderNoTillNow) {
      maxOrderNoTillNow = orderNos[i];
    }
  }
  for (let i = startingPoint; i < navigationData.length; i += 1) {
    if (!orderNos[i]) {
      orderNos[i] = maxOrderNoTillNow + 1;
      maxOrderNoTillNow += 1;
    }
  }
  let cssOrderNumbersString = '';
  for (let i = 0; i < orderNos.length; i += 1) {
    cssOrderNumbersString += `#l1menu_link_${i}{order:${orderNos[i]}} `;
  }
  return cssOrderNumbersString;
};

export default generateOrderNos;
