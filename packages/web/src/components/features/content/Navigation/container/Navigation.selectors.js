// 9fbef606107a605d69c0edbcd8029e5d
import { getABtestFromState } from '@tcp/core/src/utils';
import { createSelector } from 'reselect';
import { generateOrderNos } from './navigation.util';

const getNavigationData = state => {
  return state.Navigation;
};

export const getShopBySizeAbTest = state => {
  return getABtestFromState(state.AbTest).disableShopBySize;
};

export const getl1OrderAbTest = state => {
  return getABtestFromState(state.AbTest).l1Order;
};

export const getIsL1OrderingEnabled = state => {
  return getABtestFromState(state.AbTest).isL1OrderingEnabled;
};

export const getCssOrderNos = createSelector(
  getl1OrderAbTest,
  getNavigationData,
  getIsL1OrderingEnabled,
  (l1OrderABTest, navigationState, isL1OrderingEnabled) => {
    let cssOrderNos = '';
    const { navigationData = [] } = navigationState;

    let variationPresent = false;
    let noOfMatches = 0;
    for (let i = 0; i < navigationData.length; i += 1) {
      const indexOfCurrent = l1OrderABTest?.indexOf(navigationData?.[i]?.categoryContent?.name);
      if (indexOfCurrent !== -1) {
        noOfMatches += 1;
      }
    }
    if (noOfMatches === l1OrderABTest?.length) {
      variationPresent = true;
    }
    if (l1OrderABTest && isL1OrderingEnabled && variationPresent) {
      cssOrderNos = generateOrderNos(navigationData, l1OrderABTest);
    }
    return cssOrderNos;
  }
);

export default {
  getNavigationData,
};
