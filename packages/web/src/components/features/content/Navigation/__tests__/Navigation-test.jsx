// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mock from '@tcp/core/src/services/abstractors/bootstrap/navigation/mock';
import { NavigationVanilla as Navigation } from '../views/Navigation';
import { generateOrderNos } from '../container/navigation.util';

describe('NavBar component', () => {
  it('renders correctly', () => {
    const NavBarComp = shallow(
      <Navigation nav={mock.data.navigation} openNavigationDrawer closeNavigationDrawer={false} />
    );

    expect(NavBarComp).toMatchSnapshot();
  });

  it('DOM loaded perfectly', () => {
    const NavBarComp = shallow(
      <Navigation nav={mock.data.navigation} openNavigationDrawer closeNavigationDrawer={false} />
    );

    expect(NavBarComp.find('.nav-bar')).toHaveLength(1);
  });

  it('reorder Navigation selector test 1', () => {
    const variation = ['Toddler Boy', 'Baby', 'Girl', 'Boy', 'Toddler Girl'];
    const data = mock.data.navigation;
    expect(generateOrderNos(data, variation)).toMatchSnapshot();
  });

  it('reorder Navigation selector test 2', () => {
    const variation = ['Girl', 'Boy', 'Toddler Girl', 'Toddler Boy'];
    const data = mock.data.navigation;
    expect(generateOrderNos(data, variation)).toMatchSnapshot();
  });
});
