// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useCallback } from 'react';
import Router from 'next/router';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { setLocalStorage, getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { scrollToBreadCrumb, isMobileWeb } from '@tcp/core/src/utils';
import { getSessionStorage } from '@tcp/core/src/utils/utils.web';
import Drawer from '../molecules/Drawer';
import NavBar from '../organisms/NavBar';
import style from '../Navigation.style';

const getCustomerHelp = (url) => {
  return url.match(/\/customer-help\//g);
};

const getIsListingPage = (url) => {
  return url.match(/\/c\//g) || url.match(/\/search\//g);
};

const getIsScrollTop = (isListingPage, isUniqueUrl, isCustomerHelp, backFromPDP) => {
  return (!isListingPage || (isUniqueUrl && !backFromPDP)) && !isCustomerHelp;
};

/* eslint-disable */
const Footer = dynamic(() =>
  import(/* webpackChunkName: "footer-chunk" */ '../../Footer/container/Footer.container')
);

/**
 * This function scrolls page to top on route change complete
 */
const routeCompleteHandler = (
  url,
  isPlpPdpAnchoringEnabled,
  isDisableBagAnchoring,
  isNewPDPEnabled
) => {
  const isListingPage = getIsListingPage(url);
  const isCustomerHelp = getCustomerHelp(url);

  const isPDPPLPSearch = isListingPage || url.match(/\/p\//g);
  const isBagPage = url.match(/\/bag$/g);
  const previousUrl = getLocalStorage('previousUrl') || '';
  const isUniqueUrl = previousUrl.split('?')[0] !== url.split('?')[0];
  const isScrollEnabled = isNewPDPEnabled && url.match(/\/p\//g);
  const fromPDP = previousUrl && previousUrl.includes('/p/');
  const backFromPDP =
    isListingPage && getSessionStorage('LAST_PAGE_PATH') === window.location.pathname && fromPDP;
  if (
    isMobileWeb() &&
    ((isPlpPdpAnchoringEnabled && isPDPPLPSearch && isUniqueUrl && !backFromPDP) ||
      (!isDisableBagAnchoring && isBagPage))
  ) {
    scrollToBreadCrumb(isBagPage, isScrollEnabled);
  } else if (getIsScrollTop(isListingPage, isUniqueUrl, isCustomerHelp, backFromPDP)) {
    // If not listing page or if listing page with different URL and not backfrompdp to same page, scroll to top
    window.scrollTo(0, 0);
  }
  setLocalStorage({ key: 'previousUrl', value: url });
};

const Navigation = (props) => {
  const {
    openNavigationDrawer,
    className,
    userName,
    userPoints,
    userRewards,
    userNameClick,
    onLinkClick,
    triggerLoginCreateAccount,
    closeNavigationDrawer,
    hideNavigationFooter,
    showCondensedHeader,
    openOverlay,
    isCondensedHeader,
    accessibilityLabels,
    onRouteChangeGlobalActions,
    isPlpPdpAnchoringEnabled,
    isDisableBagAnchoring,
    labels,
    isShowNavAnimation,
    isNewPDPEnabled,
  } = props;

  const handleRouteChange = useCallback(() => {
    onRouteChangeGlobalActions();
    closeNavigationDrawer();
  }, []);

  const handleRouteComplete = useCallback((url) => {
    routeCompleteHandler(url, isPlpPdpAnchoringEnabled, isDisableBagAnchoring, isNewPDPEnabled);
  });

  useEffect(() => {
    Router.events.on('routeChangeStart', handleRouteChange);
    Router.events.on('routeChangeComplete', handleRouteComplete);

    return () => {
      Router.events.off('routeChangeStart', handleRouteChange);
      Router.events.off('routeChangeComplete', handleRouteComplete);
    };
  }, []);

  return (
    <Drawer
      id="l1_drawer"
      small
      medium
      userName={userName}
      userPoints={userPoints}
      userRewards={userRewards}
      userNameClick={userNameClick}
      onLinkClick={onLinkClick}
      triggerLoginCreateAccount={triggerLoginCreateAccount}
      open={openNavigationDrawer}
      close={closeNavigationDrawer}
      openOverlay={openOverlay}
      width={{
        small: '314px',
        medium: '314px',
        large: '100%',
      }}
      position={{
        top: !showCondensedHeader ? '155px' : '316px',
        left: 0,
        topMedium: '111px',
      }}
      renderOverlay
      drawerFooter={Footer}
      hideNavigationFooter={hideNavigationFooter}
      showCondensedHeader={showCondensedHeader}
      navigationLevel="l1"
      labels={labels}
      isShowNavAnimation={isShowNavAnimation}
    >
      <nav
        className={`${className} navigation nav-bar`}
        aria-label={isCondensedHeader && accessibilityLabels.condensed_navigation_aria_label}
      >
        <NavBar {...props} />
      </nav>
    </Drawer>
  );
};

Navigation.propTypes = {
  openNavigationDrawer: PropTypes.bool.isRequired,
  closeNavigationDrawer: PropTypes.bool,
  className: PropTypes.string.isRequired,
  hideNavigationFooter: PropTypes.bool.isRequired,
  showCondensedHeader: PropTypes.bool.isRequired,
  userName: PropTypes.string.isRequired,
  userPoints: PropTypes.string.isRequired,
  userRewards: PropTypes.string.isRequired,
  userNameClick: PropTypes.bool.isRequired,
  onLinkClick: PropTypes.func.isRequired,
  triggerLoginCreateAccount: PropTypes.bool.isRequired,
  openOverlay: PropTypes.func.isRequired,
  accessibilityLabels: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  isCondensedHeader: PropTypes.bool,
  isShopBySizeAbTestOff: PropTypes.bool.isRequired,
  onRouteChangeGlobalActions: PropTypes.func,
  isPlpPdpAnchoringEnabled: PropTypes.bool,
  isDisableBagAnchoring: PropTypes.bool,
  disableBabyL2Nesting: PropTypes.bool,
  labels: PropTypes.shape({}).isRequired,
  isShowNavAnimation: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
};

Navigation.defaultProps = {
  isCondensedHeader: false,
  onRouteChangeGlobalActions: () => {},
  closeNavigationDrawer: () => {},
  isPlpPdpAnchoringEnabled: false,
  disableBabyL2Nesting: false,
  isDisableBagAnchoring: false,
  isShowNavAnimation: false,
  isNewPDPEnabled: false,
};

export { Navigation as NavigationVanilla };
export default withStyles(Navigation, style);
