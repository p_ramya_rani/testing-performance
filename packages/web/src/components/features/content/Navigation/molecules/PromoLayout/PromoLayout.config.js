// 9fbef606107a605d69c0edbcd8029e5d 
export const imageConfig = {
  'one-col-img': { crops: ['', '', 't_nav_one_col_img_d'] },
  'two-col-img': { crops: ['', '', 't_nav_two_col_img_d'] },
  'three-col-img': { crops: ['', '', 't_nav_three_col_img_d'] },
  'half-img': { crops: ['', '', 't_nav_half_img_d'] },
};

export const colStructure = {
  shopBySizeTwoColumns: { col: [2, 2], imgClass: '' },
  oneColumn: { col: [6], imgClass: 'three-col-img' },
  threeColumns: { col: [2, 2, 2], imgClass: '' },
  twoColumns67by33: { col: [4, 2], imgClass: 'two-col-img' },
  oneColumn67: { col: [4, 2], imgClass: 'two-col-img' },
};
