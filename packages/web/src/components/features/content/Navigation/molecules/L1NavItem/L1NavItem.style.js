// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const darkArrow = getIconPath('icon-carrot-black-small');

export default css`
  padding: 0 14px;
  ${props => (props.isShowNavAnimation ? 'flex-grow: 1;' : '')}

  @keyframes slide-in-nav {
    0% {
      display: none;
      opacity: 0;
      transform: scaleY(1);
    }
    1% {
      display: block;
    }
    99% {
      display: block;
    }
    100% {
      display: block;
      opacity: 1;
      transform: scaleY(1);
    }
  }
  @media ${props => props.theme.mediaQuery.large} {
    border-bottom: 3px solid transparent;
  }

  &.is-open {
    background: ${props => props.theme.colorPalette.gray[300]};
  }

  span {
    display: inline-block;
  }
  .nav-bar-l1-content {
    display: flex;
    padding: 18px 0 17px 0;
    outline: 0;
    @media ${props => props.theme.mediaQuery.large} {
      text-align: center;
    }
  }

  .icon-arrow {
    background: url(${darkArrow}) no-repeat;
    width: 10px;
    height: 10px;
  }
  .nav-bar-item-label {
    width: 45%;
    cursor: pointer;
    @media ${props => props.theme.mediaQuery.large} {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    &.highlighted {
      color: ${props => props.theme.colorPalette.secondary.main};
    }
    &.full-width {
      width: 96%;
    }
  }
  .nav-bar-item-content {
    width: 51%;
    color: ${props => props.theme.colorPalette.primary.dark};
  }

  @media ${props => props.theme.mediaQuery.large} {
    padding: 0;
    color: ${props => props.theme.colorPalette.text.primary};
    span {
      display: inline;
    }
    .nav-bar-item-content,
    &.show-on-mobile {
      display: none;
    }
    .nav-bar-l1-content {
      display: block;
      cursor: pointer;
      position: relative;
      padding: 38px 20px 5px 20px;
      @media ${props => props.theme.mediaQuery.large} {
        text-align: center;
      }
    }
    .nav-bar-item-label {
      width: 100%;
      display: inline-block;
    }
    &.l1-overlay.is-open {
      background: ${props => props.theme.colorPalette.gray[900]};
      position: absolute;
      top: 75px;
      z-index: 1;
      opacity: 0.6;
      width: 100%;
      left: 0%;
      height: 200vh;
    }
    &.is-open {
      background: linear-gradient(
        to bottom,
        rgba(255, 255, 255, 0.99),
        ${props => props.theme.colorPalette.gray[300]}
      );
      color: ${props => props.theme.colorPalette.text.primary};
      border-bottom: 3px solid ${props => props.theme.colorPalette.primary.main};
      .nav-bar-l2 {
        ${props =>
          props.isShowNavAnimation ? 'animation: slide-in-nav 250ms forwards;' : 'display: block;'}
      }
      .nav-bar-item-sizes-range {
        cursor: default;
        position: absolute;
        display: block;
        top: calc(100% + 15px);
        left: 0;
        width: 100%;
        color: ${props => props.theme.colorPalette.gray[900]};
        text-align: center;
        font-weight: 600;
        z-index: ${props => props.theme.zindex.zDrawer + 1};
        white-space: nowrap;
      }
    }
  }
`;
