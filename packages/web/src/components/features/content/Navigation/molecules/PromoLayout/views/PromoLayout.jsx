// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { configureInternalNavigationFromCMSUrl, getViewportInfo } from '@tcp/core/src/utils';
import { Heading, Col, Anchor, BodyCopy, DamImage } from '@tcp/core/src/components/common/atoms';
import { StyledPromoBanner } from '../PromoLayout.style';
import { CLEARANCE_ID } from '../../L1NavItem/L1NavItem.config';
import { imageConfig, colStructure } from '../PromoLayout.config';

/**
 * This function will be used to create the shop by size link.
 * @param {*} links
 * @param {*} column
 * @param {*} hideL2Nav
 */
const createShopByLinks = (links, column, hideL2Nav) => {
  return (
    <ul>
      {links.map((link, index) => {
        const { url, text, title, target } = link;
        const to = configureInternalNavigationFromCMSUrl(url);
        const currentIndex = column > 1 ? index + 5 : index;
        return (
          <li>
            <Anchor
              to={to}
              asPath={url}
              title={title}
              target={target}
              dataLocator={`l2_size_btn_${currentIndex}`}
              onClick={() => hideL2Nav()}
            >
              <BodyCopy className="l2-circle-link">{text}</BodyCopy>
            </Anchor>
          </li>
        );
      })}
    </ul>
  );
};

/**
 * This function will be used to create the image banner.
 * @param {*} imageBanner
 * @param {*} l1Index
 * @param {*} categoryLayoutColName
 * @param {*} colSize
 * @param {*} colIndex
 * @param {*} hideL2Nav
 */
const createImgBanner = (
  imageBanner,
  l1Index,
  categoryLayoutColName,
  colSize,
  colIndex,
  hideL2Nav
) => {
  const imgBannerLength = imageBanner ? imageBanner.length : 0;
  const colProps = colStructure[categoryLayoutColName];
  let imgConfig = imageConfig['one-col-img'];
  if (imageConfig[colProps.imgClass] && colIndex === 0) {
    imgConfig = imageConfig[colProps.imgClass];
  } else if (imgBannerLength > 1) {
    imgConfig = imageConfig['half-img'];
  }
  return (
    imageBanner && (
      <Col
        className="l2-image-banner"
        colSize={{
          small: 6,
          medium: 8,
          large: colSize || 2,
        }}
        ignoreNthRule
      >
        {imageBanner.map(({ image, link }, index) => (
          <React.Fragment>
            <Anchor
              className="l2-image-banner-link"
              to={configureInternalNavigationFromCMSUrl(link.url)}
              asPath={link.url}
              title={link.title}
              dataLocator={`overlay_img_link_${l1Index}`}
              target={link.target}
              onClick={() => hideL2Nav()}
            >
              <DamImage
                imgData={image}
                data-locator={`overlay_img_${l1Index}`}
                imgConfigs={imgConfig.crops}
                className="l2-image-banner-image"
              />

              <BodyCopy
                className={`l2-nav-link ${
                  imgBannerLength > 1 && index === 0 ? 'l2-nav-split-nav-link' : ''
                }`}
                fontFamily="secondary"
                fontSize={['fs13', 'fs13', 'fs14']}
                lineHeight="lh107"
                color="text.primary"
                textAlign="center"
              >
                <span className="nav-bar-l1-item-label">{link.text}</span>
                <span className="icon-arrow" />
              </BodyCopy>
            </Anchor>
          </React.Fragment>
        ))}
      </Col>
    )
  );
};

/**
 * To create the array of all l2 panel categories item.
 * @param {*} panelData
 */
const getPanelDataLinks = (panelData) => {
  return Object.keys(panelData).reduce((acc, cur) => {
    const { items } = panelData[cur];
    acc.push(...items);
    return acc;
  }, []);
};

/**
 * This function will be used to map the text banner with the unbxd clerance categories.
 * @param {*} l2PanelLinks
 * @param {*} textBanner
 */

const updateTextBanner = (l2PanelLinks, textBanner) => {
  if (textBanner) {
    return textBanner.map((textBannerItem) => {
      const { category } = textBannerItem;
      const [catId] = category || [];
      const catIdArr = catId.split('|');
      const currentCategoryId = catIdArr[catIdArr.length - 1];
      let tempTextBanner = null;
      for (let itemIndex = 0; itemIndex < l2PanelLinks.length; itemIndex += 1) {
        const currentL2Item = l2PanelLinks[itemIndex];
        if (
          currentL2Item &&
          currentL2Item.categoryContent &&
          currentCategoryId === currentL2Item.categoryContent.id
        ) {
          tempTextBanner = {
            ...textBannerItem,
            textItems: textBannerItem.textItems,
            link: {
              url: currentL2Item.asPath,
              text: currentL2Item.categoryContent.name,
              title: currentL2Item.categoryContent.name,
            },
            renderBanner: true,
          };
          break;
        }
      }
      return tempTextBanner;
    });
  }
  return textBanner;
};

/**
 * This function will return the promo banner bottom link
 * @param {*} index
 * @param {*} arrowContainerClassName
 * @param {*} link
 */

const getPromoBannerBottomLink = (index, arrowContainerClassName, link) => {
  return (
    <BodyCopy
      className={`l2-nav-link ${index === 0 ? arrowContainerClassName : ''}`}
      fontFamily="secondary"
      fontSize={['fs13', 'fs13', 'fs14']}
      color="text.primary"
      textAlign="right"
    >
      <span className="nav-bar-l1-item-label">{link.text}</span>
      <span className="icon-arrow" />
    </BodyCopy>
  );
};
/**
 * This function will be used to create the promo banner.
 * @param {*} textBanner
 * @param {*} l1Index
 * @param {*} hideL2Nav
 */
const createPromoBanner = (textBanner, l1Index, hideL2Nav, l2PanelLinks) => {
  const isSplitView = textBanner && textBanner.length > 1;
  const textContainerClassName = isSplitView ? 'l2-half-promo-box' : '';
  const promoHalfClass = isSplitView ? 'promo-banner-half' : '';
  const arrowContainerClassName = isSplitView ? 'l2-nav-split-nav-link promo-box-link' : '';
  const updatedTextBanner = updateTextBanner(l2PanelLinks, textBanner);
  return (
    updatedTextBanner && (
      <Col
        className="l2-promo-box-wrapper"
        colSize={{
          small: 6,
          medium: 8,
          large: 2,
        }}
        ignoreNthRule
      >
        {updatedTextBanner.map((item, index) => {
          if (!item) {
            return null;
          }
          const { link, set, renderBanner } = item;
          const borderColorClass = set && set[0] ? set[0].val : 'border-pink';
          const promoTextColorClass = set && set[1] ? set[1].val : 'text-pink';
          return renderBanner ? (
            <React.Fragment>
              <Anchor
                className="l2-image-banner-link"
                to={configureInternalNavigationFromCMSUrl(link.url)}
                asPath={link.url}
                title={link.title}
                dataLocator={`overlay_img_link_${l1Index}`}
                target={link.target}
                onClick={() => hideL2Nav()}
              >
                <div className={`l2-promo-box ${textContainerClassName} ${borderColorClass}`}>
                  <StyledPromoBanner
                    promoBanner={[item]}
                    className={`promoBanner ${promoTextColorClass} ${promoHalfClass}`}
                  />
                </div>
                {getPromoBannerBottomLink(index, arrowContainerClassName, link)}
              </Anchor>
            </React.Fragment>
          ) : null;
        })}
      </Col>
    )
  );
};
/**
 * This function will be used to create the shop by size column.
 * @param {*} shopBySize
 * @param {*} hideL2Nav
 * @param {*} categoryLayoutColName
 * @param {*} colSize
 */
const createShopBySize = (
  shopBySize,
  hideL2Nav,
  categoryLayoutColName,
  colSize,
  isShopBySizeAbTestOff
) => {
  const sizes = shopBySize ? shopBySize[0] : {};
  const shopBySizeCol1 = shopBySize ? sizes.linkList.slice(0, 5) : [];
  const shopBySizeCol2 = shopBySize ? sizes.linkList.slice(5) : [];
  return (
    shopBySize && (
      <Col
        className={`l2-nav-category shop-by-size-category ${
          (isShopBySizeAbTestOff && 'hide-on-desktop') || ''
        }`}
        colSize={{
          small: 6,
          medium: 8,
          large: colSize || 2,
        }}
        ignoreNthRule
      >
        <div className="l2-nav-category-header">
          <Heading variant="h6" className="l2-nav-category-heading" dataLocator="l2_col_heading_3">
            {shopBySize ? sizes.text.text : ''}
          </Heading>
          <span className="l2-nav-category-divider" />
        </div>
        <div className="shop-by-size-links">
          {createShopByLinks(shopBySizeCol1, 1, hideL2Nav)}
          {createShopByLinks(shopBySizeCol2, 2, hideL2Nav)}
        </div>
      </Col>
    )
  );
};
/**
 * This function will be used to render the category layout
 * @param {*} props
 */

const GetCategoryLayout = (props) => {
  const {
    shopBySize,
    hideL2Nav,
    categoryLayoutColName,
    colSize,
    isDesktopView,
    imageBanner,
    l1Index,
    colIndex,
    l1Id,
    textBanner,
    l2PanelLinks,
    isShopBySizeAbTestOff,
  } = props;
  return (
    <React.Fragment>
      {shopBySize
        ? createShopBySize(
            shopBySize,
            hideL2Nav,
            categoryLayoutColName,
            colSize,
            isShopBySizeAbTestOff
          )
        : null}
      {isDesktopView && imageBanner
        ? createImgBanner(imageBanner, l1Index, categoryLayoutColName, colSize, colIndex, hideL2Nav)
        : null}
      {isDesktopView && l1Id === CLEARANCE_ID && textBanner
        ? createPromoBanner(textBanner, l1Index, hideL2Nav, l2PanelLinks)
        : null}
    </React.Fragment>
  );
};

/**
 * This function will be used to render imagebanner, promo banner and shop by size columns.
 * @param {*} columns
 * @param {*} l1Index
 * @param {*} hideL2Nav
 * @param {*} categoryLayoutColName
 * @param {*} panelColCount
 */

const createCategoryCol = (
  columns,
  l1Index,
  hideL2Nav,
  categoryLayoutColName,
  panelColCount,
  panelData,
  l1Id,
  isShopBySizeAbTestOff
  // eslint-disable-next-line max-params
) => {
  let totalCount = panelColCount;
  const isDesktopView = getViewportInfo().isDesktop;
  const colProps = colStructure[categoryLayoutColName];
  let l2PanelLinks = null;
  if (l1Id === CLEARANCE_ID) {
    l2PanelLinks = getPanelDataLinks(panelData);
  }
  return columns.map(({ imageBanner, shopBySize, textBanner }, colIndex) => {
    const colSize = colProps && colProps.col[colIndex] ? colProps.col[colIndex] : 2;
    totalCount += colSize;
    let createElem = isDesktopView ? totalCount <= 12 : true;
    if (l1Id === CLEARANCE_ID) {
      createElem = true;
    }
    if (!createElem) {
      return null;
    }
    return GetCategoryLayout({
      shopBySize,
      hideL2Nav,
      categoryLayoutColName,
      colSize,
      isDesktopView,
      imageBanner,
      l1Index,
      colIndex,
      l1Id,
      textBanner,
      l2PanelLinks,
      isShopBySizeAbTestOff,
    });
  });
};

/**
 * This component will render the promo layout section
 * @param {*} props
 */
const PromoLayout = (props) => {
  const {
    categoryLayout,
    l1Index,
    hideL2Nav,
    panelColCount,
    panelData,
    l1Id,
    isShopBySizeAbTestOff,
  } = props;
  return (
    <React.Fragment>
      {categoryLayout &&
        categoryLayout.map(({ columns, name }) =>
          createCategoryCol(
            columns,
            l1Index,
            hideL2Nav,
            name,
            panelColCount,
            panelData,
            l1Id,
            isShopBySizeAbTestOff
          )
        )}
    </React.Fragment>
  );
};

GetCategoryLayout.propTypes = {
  shopBySize: PropTypes.shape([]).isRequired,
  hideL2Nav: PropTypes.func.isRequired,
  categoryLayoutColName: PropTypes.string.isRequired,
  colSize: PropTypes.number.isRequired,
  isDesktopView: PropTypes.bool.isRequired,
  imageBanner: PropTypes.shape([]).isRequired,
  l1Index: PropTypes.number.isRequired,
  colIndex: PropTypes.number.isRequired,
  l1Id: PropTypes.string.isRequired,
  isShopBySizeAbTestOff: PropTypes.bool.isRequired,
  textBanner: PropTypes.shape([]).isRequired,
  l2PanelLinks: PropTypes.shape([]).isRequired,
};

PromoLayout.propTypes = {
  categoryLayout: PropTypes.shape([]),
  panelData: PropTypes.shape({}).isRequired,
  l1Index: PropTypes.number,
  hideL2Nav: PropTypes.func.isRequired,
  panelColCount: PropTypes.number.isRequired,
  l1Id: PropTypes.number.isRequired,
  isShopBySizeAbTestOff: PropTypes.bool,
};

PromoLayout.defaultProps = {
  categoryLayout: [],
  l1Index: 0,
  isShopBySizeAbTestOff: false,
};

export { PromoLayout as PromoLayoutVanilla };
export default PromoLayout;
