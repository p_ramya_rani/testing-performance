/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useCallback, useState } from 'react';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getViewportInfo } from '@tcp/core/src/utils';
import { Heading, Row, Col, Anchor, BodyCopy } from '@tcp/core/src/components/common/atoms';
import { visibleCategoryCount } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import { crossBrandRedirect, crossBrandUrl } from '@tcp/core/src/utils/utils.web';
import PromoLayout from '../PromoLayout';
import { keyboard } from '../../../../../../constants/constants';
import { HideDrawerConsumer } from '../L1NavItem/L1NavItem';
import PromoBadge from '../PromoBadge';
import L3Panel from '../L3Panel';
import style from './L2Panel.style';
import { CLEARANCE_ID } from '../L1NavItem/L1NavItem.config';
import L2PanelUtils from './L2Panel.utils';
import { getSetSessionStorageObj } from '../../../../../../analytics/dataLayers/browse.dataLayer';

const { KEY_ENTER, KEY_SPACE } = keyboard;

const {
  MAX_ITEMS_IN_COL,
  CONDITIONAL_MAX_ITEMS_IN_COL,
  renderArrowIcon,
  renderLabel,
  isNextColShopBySize,
  getColumnToDisplay,
  getIsNestedL2,
  getLargeColCount,
  getColClass,
  keydownHideL2Drawer,
  getHideOnMobileClass,
  getNoBorderClass,
  getTempPanelDataCount,
  getHeadingText,
  L2PropTypes,
  L2DefaultPropTypes,
} = L2PanelUtils;

const renderPromoBadge = (promoBadge, currentIndex) => {
  return (
    promoBadge && (
      <span className="nav-bar-item-content" data-locator={`promo_badge_${currentIndex}`}>
        <PromoBadge data={promoBadge} />
      </span>
    )
  );
};

const renderL3Panel = (
  index,
  l3Drawer,
  hideL3Drawer,
  name,
  { url, asPath, subCategories },
  { accessibilityLabels, hideL2Drawer, hideL2Nav, closeNav, analyticsData },
  { id, isShowNavAnimation }
) => {
  return (
    <L3Panel
      id={`l2-${id}-l3-drawer-${index.toString()}`}
      open={l3Drawer && l3Drawer.openDrawer}
      close={l3Drawer && l3Drawer.closeDrawer}
      hideL3Drawer={hideL3Drawer(`l3-drawer-${index.toString()}`)}
      hideL2Drawer={hideL2Drawer}
      hideL2Nav={hideL2Nav}
      name={name}
      links={subCategories}
      shopalllink={url}
      shopallaspath={asPath}
      accessibilityLabels={accessibilityLabels}
      closeNav={closeNav}
      analyticsData={analyticsData}
      isShowNavAnimation={isShowNavAnimation}
    />
  );
};

const openL3Nav = (
  id,
  index,
  hasL3,
  hideL2Nav,
  openL3Drawer,
  closeNav,
  { e, analyticsData, name, trackNavigation, asPath }
) => {
  const isCrossBrandUrl = crossBrandUrl(asPath);
  if (isCrossBrandUrl && !getSetSessionStorageObj('e97').e97FirstTouch) {
    const e97Obj = {
      e97FirstTouch: `${analyticsData}-${name.toLowerCase()}`,
      flag: true,
    };
    getSetSessionStorageObj('e97', true, e97Obj);
  }

  if (!getViewportInfo().isDesktop) {
    openL3Drawer(`l2-${id}-l3-drawer-${index.toString()}`, hasL3)(e);
    const drawerElement = document.getElementById('tcp-nav-drawer');
    if (drawerElement) {
      drawerElement.scrollTop = 0;
    }
    if (!hasL3) {
      closeNav();
    }
    const trackingData = {
      clickData: {
        pageNavigationText: `${analyticsData}-${name.toLowerCase()}`,
      },
    };

    if (hasL3) {
      trackNavigation(trackingData);
    }
  } else {
    hideL2Nav();
  }
};

const createLinks = (
  links,
  column,
  categoryIndex,
  {
    openL3Drawer,
    hideL3Drawer,
    l3Drawer,
    className,
    accessibilityLabels,
    hideL2Drawer,
    context,
    closeNav,
    analyticsData,
    trackNavigation,
    isShowNavAnimation,
  }
) => {
  const navHandler = {
    accessibilityLabels,
    hideL2Drawer,
    context,
    closeNav,
    analyticsData,
  };
  if (links.length) {
    return (
      <ul className={className}>
        {links.map((l2Links, index) => {
          const {
            url,
            asPath = '',
            categoryContent: { id, name, mainCategory, displayToCustomer },
            subCategories,
            hasL3,
          } = l2Links;
          if (!displayToCustomer) return null;
          const isRedirectURL = url && url.indexOf('#redirect') !== -1;
          const shopallparams = { url, asPath, subCategories };
          const promoBadge = mainCategory && mainCategory.promoBadge;
          const classForRedContent = id === '505519' ? `highlighted` : ``;
          const currentIndex = column > 1 ? index + MAX_ITEMS_IN_COL : index;
          const hasSubCategories = !isRedirectURL && subCategories && subCategories.length > 0;
          return (
            <li data-locator={`l2_col_${categoryIndex}_link_${currentIndex}`}>
              <div className="L2-panel-container">
                <Anchor
                  className="l2-link"
                  asPath={crossBrandRedirect(asPath, name, analyticsData).replace('#redirect', '')}
                  to={crossBrandRedirect(url, name, analyticsData)}
                  onClick={(e) =>
                    openL3Nav(
                      id,
                      currentIndex,
                      !isRedirectURL && hasL3,
                      context.hideL2Nav,
                      openL3Drawer,
                      closeNav,
                      {
                        e,
                        analyticsData,
                        name,
                        trackNavigation,
                        asPath,
                      }
                    )
                  }
                >
                  <BodyCopy
                    className="l2-nav-link"
                    fontFamily="secondary"
                    fontSize={['fs13', 'fs13', 'fs14']}
                    lineHeight="lh107"
                    color="text.primary"
                  >
                    {renderLabel(classForRedContent, promoBadge, name)}
                    {renderPromoBadge(promoBadge, currentIndex)}
                    {renderArrowIcon(hasSubCategories)}
                  </BodyCopy>
                </Anchor>
              </div>
              {hasSubCategories &&
                renderL3Panel(
                  currentIndex,
                  l3Drawer,
                  hideL3Drawer,
                  name,
                  shopallparams,
                  navHandler,
                  { id, isShowNavAnimation }
                )}
            </li>
          );
        })}
      </ul>
    );
  }
  return ``;
};

/**
 * This function will return the total column used by unbxd data.
 * @param {*} panelData
 */
const getPanelColCount = (panelData) => {
  let count = 0;
  Object.values(panelData).map((category) => {
    const { items } = category;
    count += getColumnToDisplay(count, visibleCategoryCount(items));
    return category;
  });
  return count;
};

/**
 * This function will be used create the unbxd columns header.
 * @param {*} label
 * @param {*} hideOnMobileClass
 * @param {*} categoryIndex
 */
const getHeader = (label, hideOnMobileClass, categoryIndex, isNestedL2) => {
  if (isNestedL2) {
    return null;
  }
  return label ? (
    <div className="l2-nav-category-header">
      <Heading
        variant="h6"
        className={`l2-nav-category-heading ${hideOnMobileClass}`}
        dataLocator={`l2_col_heading_${categoryIndex}`}
      >
        {label}
      </Heading>
      <span className="l2-nav-category-divider" />
    </div>
  ) : (
    <div className="l2-nav-category-empty-header" />
  );
};

const getFirstAndSecondColumnsData = (totalItemsCount, visibleItems) => {
  const L2_COLUMN_BREAK_LENGTH =
    totalItemsCount === 9 ? CONDITIONAL_MAX_ITEMS_IN_COL : MAX_ITEMS_IN_COL;
  const firstCol = visibleItems.slice(0, L2_COLUMN_BREAK_LENGTH);
  const secondCol = visibleItems.slice(L2_COLUMN_BREAK_LENGTH);
  return {
    firstCol,
    secondCol,
  };
};

const L2Panel = (props) => {
  const {
    className,
    panelData,
    categoryLayout,
    name,
    hideL2Drawer,
    l1Index,
    openL3Drawer,
    hideL3Drawer,
    l3Drawer,
    accessibilityLabels,
    closeNav,
    analyticsData,
    l1Id,
    trackNavigation,
    isShopBySizeAbTestOff,
    disableBabyL2Nesting,
    isShowNavAnimation,
  } = props;

  const [nestedL2Group, setNestedL2Group] = useState('');

  const handleL2GroupClick = useCallback((e) => {
    e.preventDefault();
    const groupId = e?.target?.dataset?.groupId;
    setNestedL2Group(groupId);
  });

  const { previousButton } = accessibilityLabels;
  const isShopBySizeCol = isNextColShopBySize(categoryLayout);
  const panelDataCount = getPanelColCount(panelData);
  let tempPanelDataCount = 0;
  const hideL2Links = getViewportInfo().isDesktop && l1Id === CLEARANCE_ID;
  return (
    <HideDrawerConsumer>
      {(context) => (
        <React.Fragment>
          <div className="content-wrapper">
            <div data-locator="overrlay_img" className={`${className} nav-bar-l2-panel`}>
              <div className="sizes-range-background">
                <span
                  role="button"
                  aria-label={previousButton}
                  tabIndex={0}
                  className="icon-back"
                  onClick={nestedL2Group ? () => setNestedL2Group('') : hideL2Drawer}
                  onKeyDown={(e) =>
                    nestedL2Group
                      ? (e.which === KEY_ENTER || e.which === KEY_SPACE) && setNestedL2Group('')
                      : keydownHideL2Drawer(e, hideL2Drawer)
                  }
                />
                <span className="l1-label">{getHeadingText(nestedL2Group, name)}</span>
              </div>
              <Row className="content-wrapper">
                <Row
                  className="nav-bar-l2-details"
                  tabIndex={-1}
                  fullBleed={{
                    small: true,
                    medium: true,
                  }}
                >
                  {!hideL2Links &&
                    Object.keys(panelData)
                      .sort((prevGroup, curGroup) => {
                        return (
                          parseInt(panelData[prevGroup].order, 10) -
                          parseInt(panelData[curGroup].order, 10)
                        );
                      })
                      .map((category, categoryIndex) => {
                        const { items, label } = panelData[category];
                        const visibleItems = items.filter(
                          (item) => item.categoryContent.displayToCustomer
                        );
                        const totalItemsCount = visibleItems.length;

                        if (!totalItemsCount) return null;

                        const colSize = {
                          small: 6,
                          medium: 8,
                          large: getLargeColCount(totalItemsCount),
                        };
                        /**
                         * As per RWD-4483
                         * If there are 8 or less, only one column displays
                         * If there are 9, then 7 display in first column and 2 display in second column
                         * If there are 10 or more, 8 display in first column and rest in second column (max 16)
                         */

                        const { firstCol, secondCol } = getFirstAndSecondColumnsData(
                          totalItemsCount,
                          visibleItems
                        );
                        const columnClass = getColClass(firstCol, secondCol);
                        // tempPanelDataCount will be used to identify the last unbxd column
                        tempPanelDataCount += getColumnToDisplay(
                          tempPanelDataCount,
                          totalItemsCount
                        );
                        const isLastPanelCol = tempPanelDataCount === panelDataCount;
                        // setting tempPanelDataCount to 0 because it will be equal to totalpanel data count and is last column
                        tempPanelDataCount = getTempPanelDataCount(
                          isLastPanelCol,
                          tempPanelDataCount
                        );
                        const hideOnMobileClass = getHideOnMobileClass(category);
                        const noBorderClass = getNoBorderClass(isLastPanelCol, isShopBySizeCol);

                        const isNestedL2 = getIsNestedL2(l1Id, disableBabyL2Nesting);

                        return (
                          <React.Fragment>
                            <Col
                              colSize={colSize}
                              ignoreNthRule
                              className={`l2-nav-category ${noBorderClass}`}
                            >
                              {getHeader(label, hideOnMobileClass, categoryIndex, isNestedL2)}
                              {isNestedL2 && nestedL2Group === '' && label && (
                                <div className="L2-panel-container">
                                  <Anchor
                                    className="group-link"
                                    noLink
                                    onClick={(e) => handleL2GroupClick(e)}
                                  >
                                    <BodyCopy
                                      className="l2-nav-link"
                                      fontFamily="secondary"
                                      fontSize={['fs13', 'fs13', 'fs14']}
                                      lineHeight="lh107"
                                      color="text.primary"
                                      data-group-id={label}
                                    >
                                      <span
                                        data-group-id={label}
                                        className="nav-bar-item-label full-width"
                                      >
                                        {label}
                                      </span>
                                      {renderArrowIcon(true, label)}
                                    </BodyCopy>
                                  </Anchor>
                                </div>
                              )}

                              {(nestedL2Group === label || !isNestedL2) && (
                                <div className="l2-nav-category-links">
                                  {createLinks(firstCol, 1, categoryIndex, {
                                    openL3Drawer,
                                    hideL3Drawer,
                                    l3Drawer,
                                    className: { columnClass },
                                    accessibilityLabels,
                                    hideL2Drawer,
                                    context,
                                    closeNav,
                                    analyticsData,
                                    trackNavigation,
                                    isShowNavAnimation,
                                  })}
                                  {createLinks(secondCol, 2, categoryIndex, {
                                    openL3Drawer,
                                    hideL3Drawer,
                                    l3Drawer,
                                    className: { columnClass },
                                    accessibilityLabels,
                                    hideL2Drawer,
                                    context,
                                    closeNav,
                                    analyticsData,
                                    trackNavigation,
                                    isShowNavAnimation,
                                  })}
                                </div>
                              )}
                            </Col>
                          </React.Fragment>
                        );
                      })}
                  <PromoLayout
                    l1Id={l1Id}
                    panelData={panelData}
                    categoryLayout={categoryLayout}
                    l1Index={l1Index}
                    hideL2Nav={context.hideL2Nav}
                    panelColCount={getPanelColCount(panelData)}
                    isShopBySizeAbTestOff={isShopBySizeAbTestOff}
                  />
                </Row>
              </Row>
            </div>
          </div>
        </React.Fragment>
      )}
    </HideDrawerConsumer>
  );
};

L2Panel.propTypes = L2PropTypes;

L2Panel.defaultProps = L2DefaultPropTypes;

export { L2Panel as L2PanelVanilla };
export default withStyles(L2Panel, style);
