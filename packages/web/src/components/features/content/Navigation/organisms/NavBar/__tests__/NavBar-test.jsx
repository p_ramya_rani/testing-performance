// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mock from '@tcp/core/src/services/abstractors/bootstrap/navigation/mock';
import { NavBarVanilla as NavBar } from '../NavBar';

describe('NavBar component', () => {
  it('renders correctly', () => {
    const NavBarComp = shallow(
      <NavBar nav={mock.data.navigation} openL2Drawer={() => {}} hideL2Drawer={() => {}} />
    );

    expect(NavBarComp).toMatchSnapshot();
  });

  it('renders correctly with L1OrderingEnabled', () => {
    const NavBarComp = shallow(
      <NavBar
        nav={mock.data.navigation}
        isL1OrderingEnabled
        isABTestLoaded
        cssOrderNos=""
        openL2Drawer={() => {}}
        hideL2Drawer={() => {}}
      />
    );

    expect(NavBarComp).toMatchSnapshot();
  });

  it('DOM loaded perfectly', () => {
    const NavBarComp = shallow(
      <NavBar nav={mock.data.navigation} openL2Drawer={() => {}} hideL2Drawer={() => {}} />
    );

    expect(NavBarComp.find('.nav-bar-l1')).toHaveLength(1);
  });
});
