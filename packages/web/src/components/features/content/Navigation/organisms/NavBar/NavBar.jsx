// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { getViewportInfo, isClient } from '@tcp/core/src/utils';
import L1NavItem from '../../molecules/L1NavItem';
import style from './NavBar.style';
import L2Panel from '../../molecules/L2Panel';
import Drawer from '../../molecules/Drawer';

/**
 * @function cssOrderStyle
 * @param {*} isABTestLoaded
 * @param {*} isL1OrderingEnabled
 * @param {*} cssOrderNos
 * @returns css styles for the order property of each of the elements
 */
const cssOrderStyle = (isABTestLoaded, isL1OrderingEnabled, cssOrderNos) => {
  return (
    isABTestLoaded &&
    isL1OrderingEnabled && (
      <span
        className="cssOrderStyle"
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: `<style>${cssOrderNos}</style>`,
        }}
      />
    )
  );
};

const NavBar = props => {
  const {
    nav: navigationData,
    cssOrderNos,
    isABTestLoaded,
    isL1OrderingEnabled,
    className,
    openL2Drawer,
    openDrawer,
    closeDrawer,
    hideL2Drawer,
    openL3Drawer,
    hideL3Drawer,
    l3Drawer,
    removeL1Focus,
    accessibilityLabels,
    closeNav,
    trackNavigation,
    isShopBySizeAbTestOff,
    disableBabyL2Nesting,
    isShowNavAnimation,
  } = props;

  return (
    <React.Fragment>
      {cssOrderStyle(isABTestLoaded, isL1OrderingEnabled, cssOrderNos)}
      <ul className={`${className} nav-bar-l1 content-wrapper`} role="menubar">
        {navigationData &&
          navigationData.map((navL1Item, index) => {
            const { displayToCustomer } = navL1Item.categoryContent;
            if (!displayToCustomer) return null;

            let categoryLayout = [];
            let size = '';
            const settings = {};
            const stringId = index.toString();
            if (navL1Item.categoryContent.mainCategory) {
              const { mainCategory } = navL1Item.categoryContent;
              const { categoryLayout: catLayout, sizesRange, set } = mainCategory;
              categoryLayout = catLayout;
              const range = sizesRange && sizesRange[0];
              size = range && range.text;
              if (set) {
                set.forEach(({ key, val }) => {
                  settings[key] = val;
                });
              }
            }
            let topNavigationAnalyticsData;
            if (isClient()) {
              topNavigationAnalyticsData = getViewportInfo().isDesktop
                ? `topmenu- ${navL1Item.categoryContent.name.toLowerCase()}`
                : `hamburger- ${navL1Item.categoryContent.name.toLowerCase()}`;
            }
            return (
              <L1NavItem
                dataLocator={`l1menu_link_${index}`}
                clickData={topNavigationAnalyticsData}
                index={index}
                key={`l1menu_link_${stringId}`}
                sizesRange={size}
                linkOverrideUrl={settings.sourceURL}
                onClick={() => {
                  openL2Drawer(`l2-drawer-${stringId}`)();
                  const drawerElement = document.getElementById('tcp-nav-drawer');
                  if (drawerElement) {
                    drawerElement.scrollTop = 0;
                  }
                }}
                // showOnlyOnApp={typeof settings.showOnlyOnApp !== 'undefined'}
                removeL1Focus={removeL1Focus}
                trackNavigation={trackNavigation}
                {...navL1Item}
                isShowNavAnimation={isShowNavAnimation}
              >
                <Drawer
                  id={`l2-drawer-${stringId}`}
                  drawerElemId={`drawer-wrapper-${index}`}
                  small
                  medium
                  open={openDrawer}
                  close={closeDrawer}
                  width={{
                    small: '314px',
                    medium: '314px',
                    large: '100%',
                  }}
                  position={{
                    top: 0,
                    left: 0,
                  }}
                  height="100%"
                  navigationLevel="l2"
                  isShowNavAnimation={isShowNavAnimation}
                >
                  <L2Panel
                    categoryLayout={categoryLayout}
                    panelData={navL1Item.subCategories}
                    name={navL1Item.categoryContent.name}
                    l1Id={navL1Item.categoryContent.id}
                    hideL2Drawer={hideL2Drawer(`l2-drawer-${stringId}`)}
                    className="nav-bar-l2"
                    l1Index={index}
                    openL3Drawer={openL3Drawer}
                    hideL3Drawer={hideL3Drawer}
                    l3Drawer={l3Drawer}
                    accessibilityLabels={accessibilityLabels}
                    closeNav={closeNav}
                    analyticsData={topNavigationAnalyticsData}
                    trackNavigation={trackNavigation}
                    isShopBySizeAbTestOff={isShopBySizeAbTestOff}
                    disableBabyL2Nesting={disableBabyL2Nesting}
                    isShowNavAnimation={isShowNavAnimation}
                    navL1ItemLength={Object.keys(navL1Item?.subCategories)?.length}
                  />
                </Drawer>
              </L1NavItem>
            );
          })}
      </ul>
    </React.Fragment>
  );
};

NavBar.propTypes = {
  nav: PropTypes.shape([]).isRequired,
  cssOrderNos: PropTypes.string,
  className: PropTypes.string.isRequired,
  mainCategory: PropTypes.shape({}),
  accessibilityLabels: PropTypes.shape({}).isRequired,
  openL2Drawer: PropTypes.func.isRequired,
  hideL2Drawer: PropTypes.func.isRequired,
  closeNav: PropTypes.func.isRequired,
  openDrawer: PropTypes.string.isRequired,
  closeDrawer: PropTypes.bool.isRequired,
  openL3Drawer: PropTypes.func.isRequired,
  hideL3Drawer: PropTypes.func.isRequired,
  l3Drawer: PropTypes.shape({}).isRequired,
  removeL1Focus: PropTypes.bool.isRequired,
  trackNavigation: PropTypes.func.isRequired,
  isShopBySizeAbTestOff: PropTypes.bool.isRequired,
  disableBabyL2Nesting: PropTypes.bool,
  isABTestLoaded: PropTypes.bool,
  isL1OrderingEnabled: PropTypes.bool,
  isShowNavAnimation: PropTypes.bool,
};

NavBar.defaultProps = {
  mainCategory: {},
  disableBabyL2Nesting: false,
  cssOrderNos: '',
  isABTestLoaded: false,
  isL1OrderingEnabled: false,
  isShowNavAnimation: false,
};

export { NavBar as NavBarVanilla };
export default errorBoundary(withStyles(NavBar, style));
