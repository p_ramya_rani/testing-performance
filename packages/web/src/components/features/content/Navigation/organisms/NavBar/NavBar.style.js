// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  list-style-type: none;
  box-sizing: border-box;

  @media ${props => props.theme.mediaQuery.mediumMax} {
    &.nav-bar-l1.content-wrapper {
      display: flex;
      flex-direction: column;
    }
  }

  @media ${props => props.theme.mediaQuery.large} {
    padding: 0 15px;
  }

  @media ${props => props.theme.mediaQuery.large} {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;
