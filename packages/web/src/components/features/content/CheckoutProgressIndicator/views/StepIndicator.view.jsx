// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import style from '../StepIndicator.style';

const textClass = (isOrderHelpLabel) => {
  return isOrderHelpLabel ? 'stageName orderHelpLabel' : 'stageName';
};

const StepIndicator = ({
  className,
  key,
  name,
  isActive,
  isComplete,
  onClick,
  clickable,
  isOrderHelpLabel,
}) => {
  let stepClass = '';
  let indicatorClass = '';

  if (isActive) {
    stepClass = 'active';
    indicatorClass = 'white-dot ';
  } else if (isComplete) {
    stepClass = 'completed';
    indicatorClass = 'white-background ';
    if (clickable) {
      stepClass = `${stepClass} pointer`;
    }
  } else {
    stepClass = 'pending';
    indicatorClass = 'pending-background ';
  }

  const clickHandler = (e) => {
    if (clickable) {
      onClick();
    } else {
      e.preventDefault();
    }
  };

  return (
    <BodyCopy
      component="li"
      className={`${className}${' stepIndicatorList '}${stepClass}`}
      key={key}
      onClick={clickHandler}
      onKeyUp={clickHandler}
    >
      <span className={indicatorClass} />
      {isComplete || clickable ? (
        <button type="button">
          <BodyCopy
            className={textClass(isOrderHelpLabel)}
            component="span"
            color="text.primary"
            fontFamily="secondary"
            fontWeight={stepClass !== 'pending' ? 'extrabold' : ''}
          >
            {name}
          </BodyCopy>
        </button>
      ) : (
        <BodyCopy
          className={textClass(isOrderHelpLabel)}
          component="span"
          color="text.primary"
          fontFamily="secondary"
          fontWeight={stepClass !== 'pending' ? 'extrabold' : ''}
        >
          {name}
        </BodyCopy>
      )}
    </BodyCopy>
  );
};

StepIndicator.propTypes = {
  className: PropTypes.string.isRequired,
  key: PropTypes.string,
  name: PropTypes.string.isRequired,
  isActive: PropTypes.bool,
  isComplete: PropTypes.bool,
  onClick: PropTypes.func,
  clickable: PropTypes.bool,
  isOrderHelpLabel: PropTypes.bool,
};

StepIndicator.defaultProps = {
  key: '',
  isActive: false,
  isComplete: false,
  clickable: false,
  onClick: () => {},
  isOrderHelpLabel: false,
};

export default withStyles(StepIndicator, style);
export { StepIndicator as StepIndicatorVanilla };
