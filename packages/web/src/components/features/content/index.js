// 9fbef606107a605d69c0edbcd8029e5d
import Header from './Header';
import Navigation from './Navigation';
import HomePage from './HomePage';

export { Header, Navigation, HomePage };

export default {
  Header,
  Navigation,
  HomePage,
};
