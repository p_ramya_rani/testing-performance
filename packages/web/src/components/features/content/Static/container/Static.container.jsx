// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { trackPageView, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import { createLayoutPath } from '@tcp/core/src/utils';
import STATIC_PAGE_CONSTANTS from '../Static.constants';
import Static from '../views/Static.view';

Static.propTypes = {
  slots: PropTypes.shape([]).isRequired,
  labels: PropTypes.shape({}),
};

Static.defaultProps = {
  labels: {},
};

const pathPrefix = STATIC_PAGE_CONSTANTS.STATIC_PAGE_PREFIX;
const mapStateToProps = (state, props) => {
  // TO DO - Replace the mock with the state.
  const { urlPath } = props;
  const { Layouts, Modules } = state;
  const formattedUrlPath = urlPath && createLayoutPath(`${pathPrefix}${urlPath}`);
  const contentSlots =
    Layouts && Layouts[formattedUrlPath] ? Layouts[formattedUrlPath].slots || [] : [];
  return {
    seoData: state.SEOData && state.SEOData.content,
    slots: contentSlots.map(slot => {
      const { contentId: slotContent = '' } = slot;
      const contentIds = slotContent && slotContent.split(',');
      if (contentIds && contentIds.length > 1) {
        const response = {
          ...slot,
          data: {
            slot: [],
          },
        };

        contentIds.forEach(contentId => {
          response.data.slot.push(Modules[contentId]);
        });

        return response;
      }
      return {
        ...slot,
        data: Modules[slot.contentId],
      };
    }),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setClickAnalyticsData: payload => dispatch(setClickAnalyticsData(payload)),
    trackStaticPageAnalytics: payload => {
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...payload,
                },
              },
            },
          },
        })
      );
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Static);
