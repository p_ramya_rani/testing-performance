// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import { PropTypes } from 'prop-types';
import { withRouter } from 'next/router';
import dynamic from 'next/dynamic';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import PageSlots from '@tcp/core/src/components/common/molecules/PageSlots';
import SeoCopy from '@tcp/core/src/components/features/browse/ProductListing/molecules/SeoCopy/views';
import { getIcidValue, internalCampaignProductAnalyticsList } from '@tcp/core/src/utils';

const modules = {
  moduleA: dynamic(() =>
    import(/* webpackChunkName: "moduleA" */ '@tcp/core/src/components/common/molecules/ModuleA')
  ),
  moduleB: dynamic(() =>
    import(/* webpackChunkName: "moduleB" */ '@tcp/core/src/components/common/molecules/ModuleB')
  ),
  moduleD: dynamic(() =>
    import(/* webpackChunkName: "moduleD" */ '@tcp/core/src/components/common/molecules/ModuleD')
  ),
  moduleE: dynamic(() =>
    import(/* webpackChunkName: "moduleE" */ '@tcp/core/src/components/common/molecules/ModuleE')
  ),
  moduleG: dynamic(() =>
    import(/* webpackChunkName: "moduleG" */ '@tcp/core/src/components/common/molecules/ModuleG')
  ),
  moduleJ: dynamic(() =>
    import(/* webpackChunkName: "moduleJ" */ '@tcp/core/src/components/common/molecules/ModuleJ')
  ),
  moduleM: dynamic(() =>
    import(/* webpackChunkName: "moduleM" */ '@tcp/core/src/components/common/molecules/ModuleM')
  ),
  moduleN: dynamic(() =>
    import(/* webpackChunkName: "moduleN" */ '@tcp/core/src/components/common/molecules/ModuleN')
  ),
  moduleQ: dynamic(() =>
    import(/* webpackChunkName: "moduleQ" */ '@tcp/core/src/components/common/molecules/ModuleQ')
  ),
  moduleT: dynamic(() =>
    import(/* webpackChunkName: "moduleT" */ '@tcp/core/src/components/common/molecules/ModuleT')
  ),
  moduleX: dynamic(() =>
    import(/* webpackChunkName: "moduleX" */ '@tcp/core/src/components/common/molecules/ModuleX')
  ),
  moduleDynamic: dynamic(() =>
    import(
      /* webpackChunkName: "moduleDynamic" */ '@tcp/core/src/components/common/molecules/ModuleDynamic'
    )
  ),
  module2columns: dynamic(() =>
    import(
      /* webpackChunkName: "module2columns" */ '@tcp/core/src/components/common/molecules/ModuleTwoCol'
    )
  ),
  formWrapper: dynamic(() =>
    import(
      /* webpackChunkName: "formWrapper" */ '@tcp/core/src/components/common/molecules/ModuleFormWrapper'
    )
  ),
  outfitCarousel: dynamic(() =>
    import(
      /* webpackChunkName: "outfitCarousel" */ '@tcp/core/src/components/common/molecules/OutfitCarouselModule'
    )
  ),
  module_container: dynamic(() =>
    import(
      /* webpackChunkName: "moduleContainer" */ '@tcp/core/src/components/common/molecules/ModuleContainer'
    )
  ),
  moduleFont: dynamic(() =>
    import(
      /* webpackChunkName: "moduleFont" */ '@tcp/core/src/components/common/molecules/ModuleFont'
    )
  ),
};

class StaticPageWrapper extends React.Component {
  componentDidMount() {
    const { router, trackStaticPageAnalytics, setClickAnalyticsData } = this.props;
    const icidValue = router && router.asPath && getIcidValue(router.asPath);
    const products = internalCampaignProductAnalyticsList();
    const contentType = router?.query?.contentType;
    const isGiftCard = contentType && (contentType === 'giftcardlp' || contentType === 'giftcard');
    const CONTENT_PAGE = 'content page';
    const giftCard = 'gift card';

    setClickAnalyticsData({
      internalCampaignId: icidValue,
      internalCampaignIdList: products,
    });

    trackStaticPageAnalytics({
      internalCampaignIdList: products,
      pageName: isGiftCard ? giftCard : CONTENT_PAGE,
      pageSection: isGiftCard ? giftCard : 'contentPage',
      pageSubSection: isGiftCard ? giftCard : CONTENT_PAGE,
      pageType: isGiftCard ? giftCard : CONTENT_PAGE,
    });

    const timer = setTimeout(() => {
      setClickAnalyticsData({});
      clearTimeout(timer);
    }, 200);
  }

  render() {
    const { children } = this.props;
    return [children];
  }
}

const StaticPageWrapperWithRouter = withRouter(StaticPageWrapper);

const Static = (slotData) => {
  const { slots, setResStatusNotFound, seoData, trackStaticPageAnalytics, setClickAnalyticsData } =
    slotData;
  if ((!slots || !slots.length) && setResStatusNotFound) {
    setResStatusNotFound();
  }
  return (
    <StaticPageWrapperWithRouter
      trackStaticPageAnalytics={trackStaticPageAnalytics}
      setClickAnalyticsData={setClickAnalyticsData}
    >
      <PageSlots slots={slots} modules={modules} />
      <SeoCopy {...seoData} />
    </StaticPageWrapperWithRouter>
  );
};

StaticPageWrapper.propTypes = {
  children: PropTypes.element.isRequired,
  router: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  trackStaticPageAnalytics: PropTypes.func,
  setClickAnalyticsData: PropTypes.func,
};

StaticPageWrapper.defaultProps = {
  trackStaticPageAnalytics: () => {},
  setClickAnalyticsData: () => {},
};

Static.propTypes = {
  slotData: PropTypes.shape({}),
  seoData: PropTypes.shape({}),
};

Static.defaultProps = {
  slotData: {},
  seoData: {},
};

export default errorBoundary(Static);
export { Static as StaticVanilla };
