// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  background-color: ${props => props.theme.colors.WHITE};

  .header-topnav__row {
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: auto;
    position: relative;
    width: calc(100% - 30px);

    @media ${props => props.theme.mediaQuery.large} {
      padding: ${props => props.theme.spacing.ELEM_SPACING.LRG} 0 15px;
      margin: 0;
    }
  }

  .header-stepindicator {
    position: relative;
    text-align: center;
    margin-left: 0;
    margin-right: 0;
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    width: 100%;
  }

  .header-topnav__brand-tabs,
  .header-topnav__track-order,
  .header-topnav__promo-area {
    float: left;
  }

  .header-topnav__brand-tabs {
    @media ${props => props.theme.mediaQuery.mediumMax} {
      padding-top: 0;
      text-align: center;
      margin-right: 0;
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      padding-left: ${props => props.theme.spacing.ELEM_SPACING.XXL};
    }

    img {
      @media ${props => props.theme.mediaQuery.medium} {
        width: 85px;
        height: auto;
      }
      @media ${props => props.theme.mediaQuery.smallMax} {
        height: auto;
      }
    }
    .brand-tab-gymboree {
      padding-top: 11px;
      @media ${props => props.theme.mediaQuery.smallOnly} {
        padding-top: 13px;
      }
    }
  }

  .header-topnav__promo-area {
    text-align: center;

    @media ${props => props.theme.mediaQuery.large} {
      padding-top: 12px;
    }

    @media ${props => props.theme.mediaQuery.mediumMax} {
      display: none;
    }
  }

  .header-topnav__track-order {
    font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy1}px;
    text-align: right;
    padding-top: 15px;
    ${props => (props.hideCartIconTest ? '' : 'float: right;')};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }

    @media ${props => props.theme.mediaQuery.mediumMax} {
      ${props => (props.hideCartIconTest ? 'display: none;' : 'display: inline;')};
    }

    .return-bag-link {
      font-size: 16px;
    }
  }

  .exitFromCheckout {
    position: absolute;
    top: 16px;
    left: -7px;
    border: none;
    background: transparent;

    @media ${props => props.theme.mediaQuery.large} {
      display: none;
    }
  }

  .checkout-mobile-header {
    text-align: center;
    padding: 10px 0 4px;
    margin-left: 0;
    width: 100%;
    border-top: 1px solid ${props => props.theme.colors.PRIMARY.LIGHTGRAY};

    @media ${props => props.theme.mediaQuery.large} {
      display: none;
    }

    @media ${props => props.theme.mediaQuery.mediumOnly} {
      padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
      padding-bottom: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    }
  }

  .checkout-header-title {
    margin: 0;
    text-align: center;
  }

  .non-checkout-pages & {
    display: none;
  }
  .checkout-mobile-header-font {
    @media ${props => props.theme.mediaQuery.medium} {
      font-size: ${props => props.theme.typography.fontSizes.fs24};
    }
  }
  .bag-container {
    display: flex;
    float: right;
    margin-bottom: 10px;
  }
  .cartCount {
    position: relative;
    height: ${props => props.theme.spacing.ELEM_SPACING.MED};
    width: ${props => props.theme.spacing.ELEM_SPACING.MED};
    background: ${props => props.theme.colorPalette.blue.E100};
    color: ${props => props.theme.colors.WHITE};
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 1px;
    font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    bottom: -15px;
    right: 5px;
  }
  .largeQty {
    width: 28px;
    right: 9px;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      padding: 3px;
    }
  }
`;
