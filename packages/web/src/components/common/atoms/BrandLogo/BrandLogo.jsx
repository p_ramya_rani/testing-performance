// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Anchor, Image } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { BRAND_IDENTITY_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import style from './BrandLogo.style';
import ClickTracker from '../ClickTracker';

/**
 * This just holds the logic for rendering a UX timer
 */
function SetPerformanceTimer({ imgURL }) {
  const [state, setState] = useState(false);
  useEffect(() => {
    if (imgURL) setState(true);
  }, [imgURL]);
  return state ? <RenderPerf.Measure name={BRAND_IDENTITY_VISIBLE} /> : null;
}
const BrandLogo = ({ className, alt, dataLocator, imgSrc, children }) => {
  return (
    <>
      <ClickTracker className={className} name="brand_logo">
        <Anchor to="/home" dataLocator={dataLocator}>
          {children || <Image src={imgSrc} alt={alt} />}
        </Anchor>
      </ClickTracker>
      <SetPerformanceTimer imgURL={imgSrc} />
    </>
  );
};

BrandLogo.propTypes = {
  alt: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  dataLocator: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  imgSrc: PropTypes.string.isRequired,
};
SetPerformanceTimer.propTypes = {
  imgURL: PropTypes.string.isRequired,
};

export default withStyles(BrandLogo, style);
export { BrandLogo as BrandLogoVanilla };
