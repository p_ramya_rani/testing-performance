// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useRef } from 'react';
import { PropTypes } from 'prop-types';
import Router from 'next/router';
import { getPageNameUsingURL } from '@tcp/core/src/utils';
import { getMonetateEnabledFlag } from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';

// TODO: Remove connect once react-redux@7 is in use
import { connect } from 'react-redux';
import { fetchAbTestData } from '@tcp/core/src/components/common/molecules/abTest/abTest.actions';

/**
 * A component that sets up event handlers for when the
 * route changes, and dispatches Redux actions with
 * payloads for A/B Test
 */
function AbTestPathTracker({ dispatch, MONETATE_ENABLED }) {
  const divRef = useRef(null);
  const setAbtestScript = (html) => {
    if (html) {
      const slotHtml = document.createRange().createContextualFragment(html); // Create a 'tiny' document and parse the html string
      divRef.current.innerHTML = ''; // Clear the container on path change
      divRef.current.appendChild(slotHtml);
    }
  };
  const handlePathChange = (pathname) => {
    const action = {
      payload: {
        abTest: {},
        pathname,
        setAbtestScript,
        MONETATE_ENABLED,
        page: getPageNameUsingURL(pathname),
      },
    };
    dispatch(fetchAbTestData(action));
  };
  useEffect(() => {
    // Track future route changes
    Router.events.on('routeChangeStart', handlePathChange);
    // Stop tracking route changes on unmount
    return () => Router.events.off('routeChangeStart', handlePathChange);
  }, []);

  return <div id="inject-abtest-script" ref={divRef} />;
}

AbTestPathTracker.propTypes = {
  dispatch: PropTypes.func.isRequired,
  MONETATE_ENABLED: PropTypes.bool,
};

AbTestPathTracker.defaultProps = {
  MONETATE_ENABLED: true,
};

function mapStateToProps(state) {
  const MONETATE_ENABLED = getMonetateEnabledFlag(state);
  return {
    MONETATE_ENABLED,
  };
}

export default connect(mapStateToProps)(AbTestPathTracker);
