// 9fbef606107a605d69c0edbcd8029e5d 
export default function ServerOnly({ children }) {
  return typeof window === 'undefined' ? children : null;
}
