// 9fbef606107a605d69c0edbcd8029e5d 
import PAGES from '@tcp/core/src/constants/pages.constants';
import { getAPIConfig } from '@tcp/core/src/utils';

export const getSeoConfig = (state, { router = {}, pageId: path = 'home' } = {}) => {
  let seoData = null;

  if (
    path !== PAGES.PRODUCT_LISTING_PAGE &&
    path !== PAGES.PRODUCT_DESCRIPTION_PAGE &&
    path !== PAGES.BUNDLED_PRODUCT_DESCRIPTION_PAGE
  ) {
    const { reqHostname, originalPath } = getAPIConfig();
    const { query: { contentType = '' } = {} } = router;
    const { pageTitle = '', description = '', canonicalUrl, keywords } =
      state.SEOData[path.toLowerCase()] || state.SEOData[contentType.toLowerCase()] || {};

    seoData = {
      title: pageTitle,
      description,
      canonical: canonicalUrl || `https://${reqHostname}${originalPath}`,
      keywords,
    };
  }

  return seoData;
};

export default getSeoConfig;
