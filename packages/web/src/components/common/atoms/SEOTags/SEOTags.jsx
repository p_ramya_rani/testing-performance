// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { NextSeo } from 'next-seo';
import Head from 'next/head';
import { connect } from 'react-redux';
import { getBrand, isCanada } from '@tcp/core/src/utils';
import {
  PAGE_TITLES,
  BRAND_NAMES,
  DEFAULT_TITLE,
} from '@tcp/core/src/constants/pageTitle.constants';
import { getSeoConfig } from './SEOTags.selectors';

const SEOTags = ({ seoConfig, pageId }) => {
  const seoProps = Object.assign({}, seoConfig);
  const brand = getBrand();
  const tcpTitle = `${BRAND_NAMES[brand]} ${isCanada() ? 'CA' : ''}`;
  seoProps.title =
    seoProps.title || `${PAGE_TITLES[pageId.toLowerCase()] || DEFAULT_TITLE} | ${tcpTitle}`;

  return (
    <React.Fragment>
      <NextSeo {...seoProps} />
      <Head>
        {seoConfig.hrefLangs.map(lang => (
          <link rel="alternate" hrefLang={lang.id} href={lang.canonicalUrl} />
        ))}
      </Head>
    </React.Fragment>
  );
};

SEOTags.propTypes = {
  pageId: PropTypes.string,
  seoConfig: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    canonical: PropTypes.string,
    twitter: PropTypes.objectOf(
      PropTypes.shape({
        cardType: PropTypes.string,
        site: PropTypes.string,
      })
    ),
    openGraph: PropTypes.objectOf(
      PropTypes.shape({
        url: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
      })
    ),
    hrefLangs: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        canonicalUrl: PropTypes.string,
      })
    ),
    additionalMetaTags: PropTypes.arrayOf(
      PropTypes.shape({
        property: PropTypes.string,
        content: PropTypes.string,
      })
    ),
  }),
};

SEOTags.defaultProps = {
  pageId: '',
  seoConfig: {
    title: '',
    description: '',
    canonical: '',
    twitter: {
      cardType: '',
      site: '',
    },
    openGraph: {
      url: '',
      title: '',
      description: '',
    },
    hrefLangs: [],
    additionalMetaTags: [],
  },
};

const mapStateToProps = (state, ownProps) => {
  const genericSEOData = getSeoConfig(state, ownProps);
  const { openGraph = {} } = ownProps.seoConfig;
  return {
    seoConfig: {
      ...ownProps.seoConfig,
      ...(genericSEOData
        ? {
            ...genericSEOData,
            openGraph: {
              ...openGraph,
              title: openGraph.title || genericSEOData.title,
              description: openGraph.description || genericSEOData.description,
            },
          }
        : {}),
    },
  };
};

export default connect(mapStateToProps)(SEOTags);
