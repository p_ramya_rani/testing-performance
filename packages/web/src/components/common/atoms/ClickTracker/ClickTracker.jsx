// 9fbef606107a605d69c0edbcd8029e5d
import React, { forwardRef, useEffect, useCallback } from 'react';
import { string, node, func, shape } from 'prop-types';
import { getLoyaltyLocation } from '@tcp/core/src/constants/analytics';
import { connect } from 'react-redux';
import { useClickTracking, useSetAnalyticsDataOnClick } from '@tcp/core/src/analytics';

/**
 * This component can be used for dispatching click
 * tracking actions when it or its associated ref
 * element receives click events. This can be used with
 * or without children. If used without children, a ref
 * should be passed to it.
 *
 * @example
 * <ClickTracker name="brand_logo">
 *   <BrandLogo />
 * </ClickTracker>
 *
 * @example
 * const logo = useRef()
 * <BrandLogo ref={logo} />
 * <ClickTracker name="brand_logo" ref={logo} />
 */
const ClickTracker = forwardRef((props, ref) => {
  const {
    as: Component,
    name,
    clickData,
    pageData,
    children,
    onClick: componentOnClick,
    dispatch,
    isLoyaltyClick,
    ...others
  } = props;
  const track = useClickTracking(dispatch);
  const setAnalyticsDataOnClick = useSetAnalyticsDataOnClick(dispatch);

  const handleClick = useCallback(e => {
    const payload = clickData;
    if (payload) {
      if (isLoyaltyClick) payload.loyaltyLocation = getLoyaltyLocation();

      setAnalyticsDataOnClick(payload);
    }
    track(name || (payload && payload.eventName));
    if (payload) {
      const timer = setTimeout(() => {
        setAnalyticsDataOnClick({});
        clearTimeout(timer);
      }, 100);
    }

    if (typeof componentOnClick === 'function') {
      componentOnClick(e);
    }
  });

  // eslint-disable-next-line consistent-return
  useEffect(() => {
    // If a ref us supplied, then track its click events.
    if (ref) {
      const target = ref.current;
      target.addEventListener('click', handleClick);
      return () => target.removeEventListener('click', handleClick);
    }
  }, [ref]);

  // If children are supplied, assume we want to track bubbling click events.
  return children ? (
    <Component {...others} onClick={handleClick}>
      {children}
    </Component>
  ) : null;
});

ClickTracker.propTypes = {
  as: string,
  name: string,
  children: node,
  clickData: shape({}),
  pageData: shape({}),
  dispatch: func.isRequired,
  onClick: func,
  isLoyaltyClick: string,
};

ClickTracker.defaultProps = {
  as: 'div',
  name: '',
  children: null,
  clickData: null,
  pageData: null,
  onClick: () => {},
  isLoyaltyClick: '',
};

export default connect()(ClickTracker);
