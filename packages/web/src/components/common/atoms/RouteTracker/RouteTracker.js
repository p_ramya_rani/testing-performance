// 9fbef606107a605d69c0edbcd8029e5d 
import { useEffect, useCallback } from 'react';
/**
 * NOTE: Using `withRouter` or `useRouter` did not work
 * for this component because the `components` property
 * was not getting set as expected.
 */
import Router from 'next/router';

// TODO: Remove connect once react-redux@7 is in use
import { connect } from 'react-redux';
import { readCookie, setCookie } from '@tcp/core/src/utils/cookie.util';
import { API_CONFIG } from '@tcp/core/src/services/config';
import { isClient } from '@tcp/core/src/utils';
import { setSessionStorage, getSessionStorage } from '@tcp/core/src/utils/utils.web';

function setPageCountCookie() {
  const { pageCountCookieKey } = API_CONFIG;
  const pageCount = parseInt(readCookie(pageCountCookieKey) || '0', 10) + 1;
  setCookie({
    key: pageCountCookieKey,
    value: pageCount,
  });
}

/**
 * A component that sets up cookie for number of
 * page visits and also sets the brand cookie
 * if already not present. It is for the initial
 * landing site only
 */
function RouteTracker(props) {
  const { brandId, router } = props;

  const handleChange = useCallback(() => {
    if (isClient()) {
      if (
        getSessionStorage('cidParam') === 'true' &&
        !(router && router.asPath && router.asPath.includes('cid'))
      ) {
        setSessionStorage({ key: 'cidParam', value: 'false' });
        setSessionStorage({ key: 'metric19', value: 'true' });
      } else {
        setSessionStorage({ key: 'metric19', value: 'false' });
      }
    }
    setPageCountCookie();
  }, []);

  useEffect(() => {
    // Track current route on mount
    handleChange();

    // Track future route changes
    Router.events.on('routeChangeComplete', handleChange);

    // Stop tracking route changes on unmount
    return () => Router.events.off('routeChangeComplete', handleChange);
  }, [brandId]);

  return null;
}

export default connect()(RouteTracker);
