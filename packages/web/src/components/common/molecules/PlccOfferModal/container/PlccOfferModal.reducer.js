// 9fbef606107a605d69c0edbcd8029e5d 
import PLCC_OFFER_CONSTANTS from './PlccOfferModal.constants';

const PlccOfferReducer = (state = {}, action) => {
  switch (action.type) {
    case PLCC_OFFER_CONSTANTS.PLCC_OFFER_MODAL_TOGGLE:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default PlccOfferReducer;
