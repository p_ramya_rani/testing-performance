// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import { togglePlccOfferModal } from './PlccOfferModal.actions';
import PlccOfferModal from '../views/PlccOfferModal.view';

export const mapDispatchToProps = dispatch => {
  return {
    closeModal: () => {
      dispatch(togglePlccOfferModal({ isModalOpen: false }));
    },
    togglePlccModal: payload => {
      dispatch(toggleApplyNowModal(payload));
    },
  };
};

const mapStateToProps = (state, props) => {
  return {
    formViewConfig: state.Labels.global.plccForm,
    ...props,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlccOfferModal);
