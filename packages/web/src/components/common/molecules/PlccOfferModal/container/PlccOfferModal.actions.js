// 9fbef606107a605d69c0edbcd8029e5d 
import PLCC_OFFER_CONSTANTS from './PlccOfferModal.constants';

export const togglePlccOfferModal = payload => {
  return {
    payload,
    type: PLCC_OFFER_CONSTANTS.PLCC_OFFER_MODAL_TOGGLE,
  };
};

export default {
  togglePlccOfferModal,
};
