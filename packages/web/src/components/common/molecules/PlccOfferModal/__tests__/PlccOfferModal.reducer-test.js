// 9fbef606107a605d69c0edbcd8029e5d 
import PlccOfferReducer from '../container/PlccOfferModal.reducer';
import { togglePlccOfferModal } from '../container/PlccOfferModal.actions';

describe('PlccOffer reducer', () => {
  it('should return empty object as default state', () => {
    expect(PlccOfferReducer(undefined, {})).toEqual({});
  });

  it('should return the modal status object state', () => {
    expect(PlccOfferReducer({}, togglePlccOfferModal({ isModalOpen: true }))).toEqual({
      isModalOpen: true,
    });
  });
});
