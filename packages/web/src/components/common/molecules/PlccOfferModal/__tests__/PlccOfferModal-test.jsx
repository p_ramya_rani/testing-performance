// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { routerPush } from '@tcp/core/src/utils';
import { PlccOfferModalVanilla } from '../views/PlccOfferModal.view';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  isCanada: jest.fn(),
  getSiteId: jest.fn(),
  getStaticFilePath: jest.fn,
}));

describe('PlccOfferModalVanilla component', () => {
  const mockedRouterPush = jest.fn();
  const mockedTogglePlccModal = jest.fn();
  it('renders correctly', () => {
    const props = {
      className: '',
      formViewConfig: {},
      isModalOpen: true,
    };
    const component = shallow(<PlccOfferModalVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call routerPush from onCtaClick with url', () => {
    const props = {
      className: '',
      formViewConfig: {},
      isModalOpen: true,
    };
    routerPush.mockImplementation(mockedRouterPush);
    const component = shallow(<PlccOfferModalVanilla {...props} />);
    component.instance().onCtaClick();
    expect(mockedRouterPush).toBeCalledWith('/place-card/application', '/place-card/application');
  });

  it('should call togglePlccModal from onCtaClick for plcc-modal', () => {
    const props = {
      className: '',
      formViewConfig: { lbl_PLCCOffer_cta_action: 'plcc-modal' },
      isModalOpen: true,
      togglePlccModal: mockedTogglePlccModal,
    };
    routerPush.mockImplementation(mockedRouterPush);
    const component = shallow(<PlccOfferModalVanilla {...props} />);
    component.instance().onCtaClick();
    expect(mockedTogglePlccModal).toBeCalledWith({ isModalOpen: true });
  });

  it('should call togglePlccModal from onCtaClick for application-modal', () => {
    const props = {
      className: '',
      formViewConfig: { lbl_PLCCOffer_cta_action: 'application-modal' },
      isModalOpen: true,
      togglePlccModal: mockedTogglePlccModal,
    };
    routerPush.mockImplementation(mockedRouterPush);
    const component = shallow(<PlccOfferModalVanilla {...props} />);
    component.instance().onCtaClick();
    expect(mockedTogglePlccModal).toBeCalledWith({ isModalOpen: false, isPLCCModalOpen: true });
  });
});
