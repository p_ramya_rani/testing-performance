// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .field-container {
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.XXXL};

    .email-signup-second-brand {
      padding-top: 25px;
      p {
        padding-top: 2px;
        display: block;
      }
    }

    .CheckBox__text {
      margin-top: 6px;
    }
  }
  div.TCPModal__InnerContent {
    height: 500px;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      padding-bottom: 0 !important;
    }
  }
  .form_container {
    position: relative;
  }
  .no-thanks-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
  .no-thanks-email {
    font-size: ${props => props.theme.fonts.fontSize.anchor.medium}px;
    text-decoration: underline;
    font-family: ${props => props.theme.typography.fonts.secondary};
  }
  .field-label {
    height: 40px;
  }
  .button-wrapper,
  .button-wrapper-form {
    background: ${props => props.theme.colors.BUTTON.WHITE.ALT_FOCUS};
    margin: 24px -15px 0 0;
    display: flex;
    justify-content: center;
    @media ${props => props.theme.mediaQuery.mediumMax} {
      position: sticky;
      left: 0;
      right: 0;
      bottom: 0;
      padding: 21px 0 20px 0;
    }
    @media ${props => props.theme.mediaQuery.smallOnly} {
      background: none;
    }
  }
  .button-wrapper-form {
    margin: '24px -14px 0 -14px';
    width: 100vw;
    @media ${props => props.theme.mediaQuery.medium} {
      margin: '24px -36px 0 -36px';
    }
    @media ${props => props.theme.mediaQuery.large} {
      margin: '24px -36px 0 -36px';
    }
  }

  .logo-wrapper {
    text-align: center;
    padding: 29px 0 28px 0;
    @media ${props => props.theme.mediaQuery.large} {
      padding: 31px 0 28px 0;
    }
  }

  .logo-wrapper img {
    height: 40px;
    @media ${props => props.theme.mediaQuery.medium} {
      height: 35px;
    }
  }

  .termscontainer {
    line-height: 1.4;
    font-size: ${props => props.theme.typography.fontSizes.fs10};
    margin-top: 20px;
  }

  .wrapper {
    width: 100%;
    margin-left: 0px;
    margin-right: 0px;
  }
  .button-container {
    width: 225px;
    box-sizing: border-box;
  }
  .terms-label {
    margin-top: 32px;
    padding-left: 47px;
    padding-right: 47px;
    @media ${props => props.theme.mediaQuery.medium} {
      padding-left: 0px;
      padding-right: 0px;
    }
  }
  .Modal-Header {
    .alignRight {
      right: 14px;

      @media ${props => props.theme.mediaQuery.medium} {
        right: 16px;
      }
    }
  }
  .alignTop {
    top: 14px;

    @media ${props => props.theme.mediaQuery.medium} {
      top: 16px;
    }
  }

  @media ${props => props.theme.mediaQuery.smallOnly} {
    .button-wrapper {
      position: sticky;
      width: 100%;
      bottom: 0;
    }
  }
  @media ${props => props.theme.mediaQuery.medium} {
    .button-wrapper,
    .button-wrapper-form {
      width: 457px;
      margin-left: -36px;
      margin-top: 50px;
    }
    .button-container {
      bottom: 24px;
      width: 222px;
      box-sizing: border-box;
    }
    .emailform__content {
      max-width: 381px;
      margin: 0 auto;
      padding-top: 30px;
    }
  }

  @media ${props => props.theme.mediaQuery.large} {
    .field-container {
      padding-top: 0;
    }
    .button-wrapper,
    .button-wrapper-form {
      margin: 24px auto 0;
      padding: 0;
      padding-bottom: 24px;
      width: 100%;
      background: none;
    }
    .img-wrapper {
      display: block;
      width: 300px;
      margin-right: 0;
      height: 500px;
      overflow: hidden;
      img {
        height: 100%;
      }
    }
    .img-wrapper + div {
      width: calc(100% - 300px);
      position: absolute;
      top: 0;
      bottom: 0;
      overflow-y: auto;
      right: 0;
    }
    .shop-button {
      bottom: 0;
      width: 100%;
    }
  }
  .email-modal-signup-image {
    max-width: 300px;
  }
`;

export default styles;
