// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import { string, shape, func, bool } from 'prop-types';
import { Button, Col, Row, DamImage } from '@tcp/core/src/components/common/atoms';
import { Grid, Modal } from '@tcp/core/src/components/common/molecules';
import PlccOfferIntro from '@tcp/core/src/components/common/molecules/PlccOfferIntro';
import { routerPush } from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
// eslint-disable-next-line import/no-unresolved
import styles from '../styles/PlccOfferModal.style';

const config = {
  IMG_DATA_MODAL: {
    imgConfig: ['w_300'],
  },
  modalColProps: {
    left: { small: 4, medium: 4, large: 4 },
    right: { small: 6, medium: 8, large: 8 },
  },
};

class PlccOfferModal extends React.PureComponent {
  closeThisModal = () => {
    const { closeModal } = this.props;
    closeModal();
  };

  onCtaClick = () => {
    const { togglePlccModal, formViewConfig = {} } = this.props;
    const actionType = formViewConfig.lbl_PLCCOffer_cta_action || '/place-card/application';
    this.closeThisModal();

    if (actionType && actionType.toLowerCase() === 'plcc-modal') {
      togglePlccModal({ isModalOpen: true });
    } else if (actionType && actionType.toLowerCase() === 'application-modal') {
      togglePlccModal({ isModalOpen: false, isPLCCModalOpen: true });
    } else {
      routerPush(actionType, actionType);
    }
  };

  render() {
    const { isModalOpen, className, formViewConfig } = this.props;
    const { left, right } = config.modalColProps;
    const { IMG_DATA_MODAL } = config;
    return (
      <Fragment>
        {isModalOpen ? (
          <Modal
            contentRef={this.setModalContentRef}
            isOpen={isModalOpen}
            colSet={{ small: 6, medium: 6, large: 8 }}
            className={`${className} email-signup-modal plcc-takeover`}
            overlayClassName="TCPModal__Overlay email-signup-modal"
            onRequestClose={this.closeThisModal}
            noPadding
            widthConfig={{ small: '100%', medium: '458px', large: '851px' }}
            heightConfig={{
              minHeight: '500px',
              height: '500px',
              maxHeight: '500px',
            }}
            closeIconDataLocator="plcc_offer_modal_close_btn"
            contentLabel={`${formViewConfig.lbl_SignUp_signUpForLabel} ${formViewConfig.lbl_SignUp_offerTypeLabel}`}
            aria={{
              describedby: 'plcc-offer-modal-form-intro-view',
            }}
          >
            <Grid>
              <Row fullBleed={{ large: true }} className={`${className} wrapper form_container`}>
                <Col
                  isNotInlineBlock
                  colSize={left}
                  hideCol={{ small: true, medium: true }}
                  className="img-wrapper"
                >
                  <DamImage
                    imgConfigs={IMG_DATA_MODAL}
                    imgData={{
                      url: formViewConfig.lbl_PLCCOffer_imageSrc,
                      alt: formViewConfig.lbl_PLCCOffer_imageAlt,
                    }}
                  />
                </Col>
                <Col colSize={right} className="emailform__wrapper form-wrapper">
                  <div className="emailform__content">
                    <PlccOfferIntro formViewConfig={formViewConfig} noModal={false} />
                    <Row className="button-wrapper-form" fullBleed>
                      <Col
                        colSize={{ small: 4, medium: 4, large: 6 }}
                        className="button-wrapper-form__joinnowbtn"
                      >
                        <Button
                          fullWidth
                          buttonVariation="fixed-width"
                          fill="BLUE"
                          className="join-button"
                          dataLocator="join_now_btn"
                          onClick={this.onCtaClick}
                        >
                          {formViewConfig.lbl_PLCCModal_applyNowLink}
                        </Button>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </Grid>
          </Modal>
        ) : null}
      </Fragment>
    );
  }
}

PlccOfferModal.propTypes = {
  className: string,
  formViewConfig: shape({
    lbl_SignUp_signUpForLabel: string,
    lbl_SignUp_offerTypeLabel: string,
    lbl_PLCCOffer_imageSrc: string,
    lbl_PLCCOffer_imageAlt: string,
    lbl_PLCCModal_applyNowLink: string,
  }).isRequired,
  closeModal: func,
  togglePlccModal: func,
  isModalOpen: bool,
};

PlccOfferModal.defaultProps = {
  closeModal: () => {},
  className: '',
  togglePlccModal: () => {},
  isModalOpen: false,
};

export default withStyles(PlccOfferModal, styles);
export { PlccOfferModal as PlccOfferModalVanilla };
