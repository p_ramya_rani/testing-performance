/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils/index';
import { Col } from '@tcp/core/src/components/common/atoms';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';

const downArrowIcon = getIconPath('down_arrow_toggle');
const upArrowIcon = getIconPath('up_arrow_toggle');

export const RecommendationCol = styled(Col)`
  ${(props) =>
    props.showPeek.small
      ? `@media ${props.theme.mediaQuery.smallOnly} {
    width: 125%;
  }`
      : ''}
  ${(props) =>
    props.showPeek.medium
      ? `@media ${props.theme.mediaQuery.mediumOnly} {
        width: 115%;
      }`
      : ''}

  ${(props) =>
    props.newRecommendationEnabled
      ? `@media ${props.theme.mediaQuery.large} {
      width: 100%;
      margin-left: 0;
    }`
      : ''}
`;

export default css`
  .smooth-scroll-list {
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
    width: 100%;
    -ms-overflow-style: none;
    height: 100%;
  }
  .smooth-scroll-list::-webkit-scrollbar {
    display: none;
  }
  .smooth-scroll-list-item {
    display: inline-block;
    vertical-align: top;
    white-space: normal;
    height: 100%;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => props.isNewPDPEnabled && `margin-right: 0px`}
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) => props.isNewPDPEnabled && `padding-left: 4px;`}
    }
    ${(props) =>
      props.page === Constants.RECOMMENDATIONS_PAGES_MAPPING.OUTFIT ? 'width: 33%;' : ''}
  }
  .smooth-scroll-list-item:first-child {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) => props.isNewPDPEnabled && `padding-left: 30px;`}
    }
  }
  .slick-track {
    display: flex;
  }

  .recommendations-header {
    font-family: TofinoWide;
    font-weight: ${(props) => (props.newRecommendationEnabled ? 900 : 500)};
    line-height: normal;
    letter-spacing: normal;
    text-align: left;
    ${(props) => (props.isNewPDPEnabled ? 'text-transform:uppercase;' : '')}
    margin: ${(props) =>
      props.newRecommendationEnabled
        ? `12px 0 12px 14px;`
        : `0 0 ${props.theme.spacing.MODULE_SPACING.MED} 0`};
    ${(props) => props.newRecommendationEnabled && 'font-size: 14px;'}
    ${(props) =>
      props.newRecommendationEnabled &&
      `border-top: 7px solid ${props.theme.colorPalette.gray[300]};`}
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => props.isNewPDPEnabled && `border-top: 0px`}
    }
    ${(props) =>
      props.newRecommendationEnabled && `margin: ${props.theme.spacing.ELEM_SPACING.SM} 0;`}
    ${(props) =>
      props.newRecommendationEnabled &&
      `padding: ${props.theme.spacing.ELEM_SPACING.SM} 0 0 ${props.theme.spacing.ELEM_SPACING.MED};`}
  }
  .product-image-container {
    @media ${(props) => props.theme.mediaQuery.large} {
      height: ${(props) =>
        props.isNewPDPEnabled && !props.page === Constants.RECOMMENDATIONS_PAGES_MAPPING.OUTFIT
          ? `300px`
          : `352px`};
    }
  }
  .recommendation-cta-container {
    text-align: center;
    margin-top: 16px;
  }

  .price-only {
    .slick-arrow {
      top: 40%;
    }
  }

  .no-carousel-container {
    display: flex;
    justify-content: center;
    ul {
      width: 149px;
    }
  }

  .recommendations-container-new {
    ${(props) =>
      `border-right: 7px solid ${props.theme.colorPalette.gray[300]};
     border-top: ${props.theme.spacing.ELEM_SPACING.XS_6} solid
     ${props.theme.colorPalette.gray[300]};
     border-bottom: ${props.theme.spacing.ELEM_SPACING.XS_6} solid
     ${props.theme.colorPalette.gray[300]};
     `};
    ${(props) =>
      `border-left: 7px solid ${props.theme.colorPalette.gray[300]};
     padding-bottom: 20px;
     border-radius: 6px;
     `};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          border-radius: 16px;background-color: ${props.theme.colors.WHITE};
     `
          : ``}
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      border: unset;
      ${(props) =>
        `border-bottom: ${props.theme.spacing.ELEM_SPACING.XXXS} solid
          ${props.theme.colorPalette.gray[300]}
        padding: 0 10px;
        `}
    }

    .slick-slide > div,
    .slick-slide > div > ul {
      height: 100%;
    }
    .added-to-bag {
      border-radius: 6px;
      background-color: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
      color: ${(props) => props.theme.colors.WHITE};
      ${(props) => props.isNewPDPEnabled && `margin-bottom: 14px;`}
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        ${(props) => props.isNewPDPEnabled && `padding: 11px 15px;`}
      }
    }
    .slick-list {
      display: flex;
    }
    .slick-list .slick-track {
      display: flex;
    }
  }

  .recommendations-container {
    .slick-slide > div,
    .slick-slide > div > ul {
      height: 100%;
    }
    .added-to-bag {
      ${(props) => props.isNewPDPEnabled && `margin-bottom: 14px;`}
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        padding-left: 20px;
        padding-right: 20px;
      }
    }
    .slick-list {
      display: flex;
    }
    .slick-list .slick-track {
      display: flex;
    }
  }

  &.recommendations-tile {
    background: ${(props) => props.theme.colorPalette.white};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          background: ${props.theme.colors.PRIMARY.PALEGRAY};
     `
          : ``}
    }
    .slick-arrow {
      z-index: ${(props) => props.theme.zindex.zPLPFilterDropDown};
      top: 30%;
      transform: inherit;
    }
  }

  .moduleP-variation {
    .slick-arrow {
      top: 25%;
    }
  }

  .recommendation-cta {
    width: 225px;
  }

  .slick-slide li {
    margin: 0;
  }

  .item-container-inner {
    box-sizing: border-box;
    ${(props) =>
      props.inGridPlpRecommendation !== true
        ? `padding: 0 ${props.theme.spacing.APP_LAYOUT_SPACING.XXS};`
        : ''}
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.inGridPlpRecommendation !== true && props.isNewPDPEnabled
          ? `
        width: 146px;
        height: 278px;
        margin: auto 3px;
        border-radius: 6px;
        border: solid 1px  ${props.theme.colors.PRIMARY.LIGHTGRAY};
        background-color: ${props.theme.colors.WHITE};
    `
          : ''}
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => (props.inGridPlpRecommendation !== true ? 'padding: 0 12px;' : '')}
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        props.inGridPlpRecommendation !== true && props.isNewPDPEnabled
          ? `
          width: 188px;
          height: 333px;
          margin: auto;
          border-radius: 6px;
          border: solid 1px  ${props.theme.colors.PRIMARY.LIGHTGRAY};
          `
          : ''}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) =>
        props.inGridPlpRecommendation !== true
          ? `padding: 0 ${props.theme.spacing.ELEM_SPACING.XXS};`
          : ``}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) =>
        props.inGridPlpRecommendation !== true && props.isNewPDPEnabled
          ? `
          width: 100%;
          height: 100%;
          margin: 0 ${props.theme.spacing.ELEM_SPACING.SM};
          padding: 0  ${props.theme.spacing.ELEM_SPACING.MED_1} ;
          border-radius: 6px;
          border: solid 1px  ${props.theme.colors.PRIMARY.LIGHTGRAY};
      `
          : ''}
    }
  }

  .slick-slide {
    ul li {
      display: flex;
      justify-content: center;
      width: 100%;
    }
  }
  .slick-slide .item-container-inner {
    width: 100%;
  }

  .accordion-recommendation {
    ${(props) =>
      !props.isNewPDPEnabled && ` border-top: 7px solid ${props.theme.colorPalette.gray[300]};`}
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0 0
      ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    ${(props) =>
      !props.isNewPDPEnabled && `background: url(${upArrowIcon}) no-repeat right 19px bottom 7px;`}
    text-transform: uppercase;
    display: block;
    width: 100%;
    justify-content: left;
    align-items: center;
    text-align: left;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) =>
        props.isNewPDPEnabled &&
        ` margin: ${props.theme.spacing.ELEM_SPACING.SM} ${props.theme.spacing.ELEM_SPACING.SM};`}
    }
  }

  .recommendation-height-toggle {
    background: url(${upArrowIcon}) no-repeat center;
    height: 30px;
  }

  .small-recos-height {
    background: url(${downArrowIcon}) no-repeat center;
  }

  .recommendations-section-row {
    overflow: hidden;
  }

  .small-recos-height-val + .recommendations-section-row {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: 70px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 70px;
    }
    height: auto;
  }

  .moduleO-variation {
    margin-top: 5px;
  }

  .show-accordion-toggle {
    ${(props) =>
      !props.isNewPDPEnabled &&
      `background: url(${downArrowIcon}) no-repeat right 19px bottom 7px;`}
  }

  .show-accordion-toggle ~ .recommendations-section-row {
    display: none;
  }

  .loadImage {
    object-fit: contain;
  }

  .product-list {
    max-width: 300px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => (props.newRecommendationEnabled || props.isNewPDPEnabled ? `margin: 0 5px` : '')}
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin: 0;
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .recommendation-cta {
      width: 162px;
    }
    .no-carousel-container {
      ul {
        width: 214px;
      }
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .recommendations-header {
      font-size: 14px;
      ${(props) =>
        props.newRecommendationEnabled &&
        `border-top: 1px solid ${props.theme.colorPalette.gray[300]};`}
      ${(props) => props.newRecommendationEnabled && `background: none;`}
    }
    .recommendation-cta {
      width: 210px;
    }
    .no-carousel-container {
      ul {
        width: 291px;
      }
    }
    .accordion-recommendation {
      border-top: 1px solid ${(props) => props.theme.colorPalette.gray[300]};
      background: none;
    }

    .show-accordion-toggle ~ .recommendations-section-row {
      display: flex;
    }
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};

  .suggested-item {
    .item-container-inner {
      height: inherit !important;
    }
    .product-image-container {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
      @media ${(props) => props.theme.mediaQuery.large} {
        height: 369px;
      }
    }
  }

  .recommended_products_ul {
    height: 100%;
  }
`;

export const RecommendationUl = styled.ul`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 8px;
  @media ${(props) => props.theme.mediaQuery.large} {
    margin-bottom: 48px;
  }
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    margin-bottom: 30px;
  }
  > * {
    width: 25%;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 29%;
      margin-right: 2%;
      margin-left: 2%;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 45%;
    }
    box-sizing: border-box;
  }
`;

export const RecommendationLi = styled.li`
  margin-top: 2em;
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    margin: 0 9px 24px 9px;
  }
`;

export const RecommendationMultiUl = styled.ul`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

export const RecommendationMultiLi = styled.li``;

export const RecommendationMultiGridHeader = styled.div`
  border-radius: 10px;
  text-align: center;
  height: 29px;
  box-shadow: ${(props) => props.theme.colors.PRIMARY.COLOR3} 0px 18px inset;
  color: ${(props) => props.theme.colors.WHITE};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
  text-transform: uppercase;
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    box-shadow: ${(props) => props.theme.colors.PRIMARY.COLOR3} 0px 16px inset;
    height: 25px;
    border-radius: 8px;
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy1}px;
    height: 24px;
    box-shadow: ${(props) => props.theme.colors.PRIMARY.COLOR3} 0px 14px inset;
    border-radius: 8px;
  }
`;

export const ButtonDiv = styled.div`
  width: 100%;
  text-align: center;
  margin-bottom: 48px;
`;

export const Button = styled.button`
  border: solid 1px ${(props) => props.theme.colors.TEXT.DARKERGRAY};
  background-color: ${(props) => props.theme.colors.WHITE};
  font-family: Nunito;
  font-size: 14px;
  font-weight: 800;
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
  justify-content: center;
  cursor: pointer;
  min-height: 59px;
  width: 21%;
  &:focus {
    border: solid 1px ${(props) => props.theme.colors.TEXT.DARKERGRAY};
  }
  &:active {
    border: solid 1px ${(props) => props.theme.colors.TEXT.DARKERGRAY};
  }
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    min-height: 42px;
    width: 26%;
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    min-height: 42px;
    width: 42%;
  }
`;
