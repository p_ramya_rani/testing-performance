// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { RecommendationsVanilla as Recommendations } from '../Recommendations';

const props = {
  products: [
    {
      pdpUrl:
        '/us/p/Girls-Long-Sleeve-Sequin-Unicorn-Faux-Fur-Lined-Hooded-Parka-Jacket-3003346-32DE',
      name: 'Girls Sequin Unicorn Parka Jacket',
      imagePath:
        'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/3003346_32DE.jpg',
      listPrice: '74.95',
      offerPrice: '74.95',
    },
  ],
  headerLabel: 'You May Also Like',
  className: 'test class',
  loadRecommendations: jest.fn(),
  loadRecommendationsMonetate: jest.fn(),
  MONETATE_ENABLED: false,
  isATBModalBackAbTestNewDesign: true,
};

describe('Recommendations Module O variation', () => {
  it('renders correctly', () => {
    const RecommendationsComp = shallow(<Recommendations variations="moduleO" {...props} />);
    expect(RecommendationsComp.find('.recommendation_type')).toHaveLength(0);
    expect(RecommendationsComp).toMatchSnapshot();
  });

  it('renders correctly for isATBModalBackAbTestNewDesign false', () => {
    const RecommendationsComp = shallow(
      <Recommendations
        variations="moduleO"
        {...{ ...props, isATBModalBackAbTestNewDesign: false }}
      />
    );
    expect(RecommendationsComp).toMatchSnapshot();
  });
});
