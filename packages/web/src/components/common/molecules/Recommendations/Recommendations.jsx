/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { Component } from 'react';
import dynamic from 'next/dynamic';
import { arrayOf, bool, func, oneOfType, number, string, shape } from 'prop-types';
import isEqual from 'lodash/isEqual';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/container/index.async';
import { Row, Heading, BodyCopy, Col } from '@tcp/core/src/components/common/atoms';
import ButtonCTA from '@tcp/core/src/components/common/molecules/ButtonCTA';
import {
  getIconPath,
  getViewportInfo,
  isClient,
  getSessionStorage,
  setSessionStorage,
} from '@tcp/core/src/utils';
import { capitalizeWords } from '@tcp/core/src/utils/utils';
import { setLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import RecomStyleWith from '@tcp/core/src/components/common/molecules/RecomStyleWith';
import RecomFBT from '@tcp/core/src/components/common/molecules/RecomFBT';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import config from './config';
import constant from './Recommendations.constant';
import style, {
  RecommendationCol,
  ButtonDiv,
  Button,
  RecommendationUl,
  RecommendationLi,
  RecommendationMultiUl,
  RecommendationMultiLi,
  RecommendationMultiGridHeader,
} from './Recommendations.style';

const productsLength = isClient() && getViewportInfo().isMobile ? 3 : 4;
let viewProducts = 8;
if (isClient() && (getViewportInfo().isMobile || getViewportInfo().isTablet)) viewProducts = 6;
let ModuleO;
let ModuleP;
/**
 * This method loads different variations for Recommendation Module dynamically
 */
const RecommendationComponentVariation = (dynamicComponentProps) => {
  if (!ModuleO)
    ModuleO = dynamic(() => import('@tcp/core/src/components/common/molecules/ModuleO'));
  if (!ModuleP)
    ModuleP = dynamic(() => import('@tcp/core/src/components/common/molecules/ModuleP'));
  switch (dynamicComponentProps.variation) {
    case config.variations.moduleO:
      return <ModuleO {...dynamicComponentProps} />;
    case config.variations.moduleP:
      return <ModuleP {...dynamicComponentProps} />;
    default:
      return <ModuleO {...dynamicComponentProps} />;
  }
};

const isShowHeader = (isFavoriteRecommendation, isSuggestedItem, accordionHeader, portalValue) => {
  return (
    !isFavoriteRecommendation &&
    !isSuggestedItem &&
    !accordionHeader &&
    portalValue !== Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS
  );
};

const { RECOMMENDATION } = constant;

class Recommendations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAccordionOpen: false,
      isRecoHeightSmall: false,
      showProducts: [],
      viewMoreProducts: [],
      showPdpProducts: 0,
    };
    this.handleAccordionToggle = this.handleAccordionToggle.bind(this);
    this.handleRecosHeightToggle = this.handleRecosHeightToggle.bind(this);
  }

  // eslint-disable-next-line complexity
  componentDidMount() {
    const {
      loadRecommendationsAdobe,
      loadRecommendationsMonetate,
      portalValue,
      partNumber,
      categoryName,
      reduxKey,
      excludedIds,
      provider = 'Monetate',
      MONETATE_ENABLED,
      cartItems,
      isBagLoading,
      isOutfitLoading,
      outfitItems,
      userEmail,
      addedToBagData,
      pdpDataInformation,
      styleWithEligibility,
      isPlcc,
      outOfStock,
      isRecProduct,
      actionId,
      isFBTEnabled,
    } = this.props;

    let { page } = this.props;
    const skuData = this.skuDetail(addedToBagData, pdpDataInformation);
    if (page === 'home page') page = 'homepage';

    let action = {
      page: page || 'homepageTest',
      ...(partNumber && { itemPartNumber: partNumber }),
      categoryName: categoryName || '',
      excludedIds,
      provider,
      MONETATE_ENABLED,
      cartItems,
      outfitItems,
      userEmail,
      skuData,
      styleWithEligibility,
      isPlcc,
      outOfStock,
      isRecProduct,
      actionId,
      isFBTEnabled,
    };

    if (reduxKey === 'favorites_global_products') {
      action.reduxKey = reduxKey;
    } else if (!MONETATE_ENABLED) {
      action.reduxKey = reduxKey;
      action = { ...action, ...(portalValue && { mboxName: portalValue }) };
    } else {
      action.reduxKey = action.page;
    }

    if (window.adobe && window.adobe.target) {
      return loadRecommendationsAdobe(action);
    }
    if (!(page === 'cart' && isBagLoading) && !(page === 'outfit' && isOutfitLoading))
      return window.addEventListener('load', loadRecommendationsMonetate(action));

    return null;
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate(prevProps) {
    const {
      loadRecommendationsMonetate,
      partNumber,
      categoryName,
      excludedIds,
      provider = 'Monetate',
      MONETATE_ENABLED,
      cartItems,
      isBagLoading,
      isOutfitLoading,
      outfitItems,
      userEmail,
      addedToBagData,
      pdpDataInformation,
      styleWithEligibility,
      outOfStock,
    } = this.props;

    const { page } = this.props;
    const skuData = this.skuDetail(addedToBagData, pdpDataInformation);
    const action = {
      page: page || 'homepageTest',
      ...(partNumber && { itemPartNumber: partNumber }),
      categoryName: categoryName || '',
      excludedIds,
      provider,
      MONETATE_ENABLED,
      cartItems,
      outfitItems,
      userEmail,
      skuData,
      styleWithEligibility,
      outOfStock,
    };

    action.reduxKey = action.page;

    if (
      !(page === 'cart' && isBagLoading) &&
      !(page === 'outfit' && isOutfitLoading) &&
      prevProps.styleWithEligibility !== styleWithEligibility
    )
      return window.addEventListener('load', loadRecommendationsMonetate(action));

    return null;
  }

  componentWillUnmount() {
    const { loadRecommendationsMonetate } = this.props;
    window.removeEventListener('load', loadRecommendationsMonetate);
  }

  skuDetail = (addedToBagData, pdpDataInformation) =>
    addedToBagData && addedToBagData.skuInfo ? addedToBagData.skuInfo : pdpDataInformation;

  isPromoAvailable = (products) =>
    products.map((product) => product.productInfo && product.productInfo.promotionalMessage);

  getRecoHeightStatus = (isRecoHeightSmall) => {
    return isRecoHeightSmall ? '' : 'small-recos-height';
  };

  getAccordionClass = (isAccordionOpen) => {
    return isAccordionOpen ? 'show-accordion-toggle' : '';
  };

  haveStyleWithProducts = (styleWithView, products) =>
    styleWithView && products.length && products[0].styleWithEligibility;

  haveFBTProducts = (isFBT, products) => isFBT && products.length && products[0].isFbt;

  showMoreProducts = () => {
    const { products, portalValue } = this.props;
    const { showProducts } = this.state;
    let count = showProducts.length + viewProducts;
    if (products.length <= viewProducts) {
      count = products.length;
    }

    const value =
      portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED
        ? getSessionStorage('PDP2')
        : getSessionStorage('PDP1');

    if (
      value > 0 &&
      isClient() &&
      getSessionStorage('LAST_PAGE_PATH') === window.location.pathname
    ) {
      count = value;
      this.setState({ showPdpProducts: count });
      if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED)
        setSessionStorage({ key: 'PDP2', value: 0 });
      else setSessionStorage({ key: 'PDP1', value: 0 });
    }

    this.setState({
      showProducts: products.slice(0, count),
    });
  };

  showMoreButton = () => {
    const buttonTxt = `VIEW MORE`;
    return (
      <ButtonDiv>
        <Button onClick={() => this.showMoreProducts()}>{buttonTxt}</Button>
      </ButtonDiv>
    );
  };

  showRecosBannerExperience = (recommendationBannerHTML) => {
    return <div dangerouslySetInnerHTML={{ __html: recommendationBannerHTML }} />;
  };

  bannerRecommendationForScrolling = (page, recommendationBannerHTML, LoadVariation) => {
    return (
      <div className="smooth-scroll-list">
        <ul className="smooth-scroll-list-item">
          <li className="product-list">
            <div className="bannerStyles">
              {this.showRecosBannerExperience(recommendationBannerHTML)}
            </div>
          </li>
        </ul>
        {LoadVariation}
      </div>
    );
  };

  loadInMultiGridPlpRecommendation = (variation) => {
    const { products, index, slots, slotsList, labels, portalValue } = this.props;
    const label =
      portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS
        ? labels.inGridHeader
        : labels.inMultiGridHeader;

    let currentRecosProduct = [];
    let pos = 0;
    let productId = '';

    if (slots[0].length > 0) {
      let len = 0;
      pos = slotsList.indexOf(index);
      slots.forEach((slot) => {
        if (index === slot[0]) {
          len = slot[1] - slot[0] + 1;
        }
      });
      currentRecosProduct =
        products.slice(pos, pos + len).length === len
          ? products.slice(pos, pos + len)
          : products.slice(0, len);
    } else {
      pos = slots.indexOf(index);
      if (pos === -1 || typeof products[pos] === 'undefined') pos = 0;
      currentRecosProduct = products[pos];
      const { generalProductId } = currentRecosProduct;
      productId = generalProductId;
    }

    return (
      <React.Fragment>
        <RecommendationMultiGridHeader>{label}</RecommendationMultiGridHeader>
        <RecommendationMultiUl>
          {slots[0].length > 0 ? (
            currentRecosProduct.map((product, i) => {
              const { generalProductId } = product;
              return (
                <RecommendationMultiLi
                  className="multi-grid-product-tile"
                  key={`recommended_products_li_${variation}_${generalProductId}`}
                >
                  {this.loadRecommendationVariation(
                    variation,
                    product,
                    pos + i,
                    'in-multi-grid-plp'
                  )}
                </RecommendationMultiLi>
              );
            })
          ) : (
            <RecommendationMultiLi
              className="multi-grid-product-tile"
              key={`recommended_products_li_${variation}_${productId}`}
            >
              {this.loadRecommendationVariation(
                variation,
                currentRecosProduct,
                pos,
                'in-multi-grid-plp'
              )}
            </RecommendationMultiLi>
          )}
        </RecommendationMultiUl>
      </React.Fragment>
    );
  };

  loadBannerRecommendation = (variation, carouselProps, customProductsLength) => {
    const isMobile = isClient() ? getViewportInfo().isMobile : null;
    const isTablet = isClient() ? getViewportInfo().isTablet : null;
    const { page, recommendationBannerHTML, products } = this.props;
    const LoadVariation = this.loadVariation(variation, 'smooth-scroll-list-item');
    const CarouselVariation = 'big-arrows';
    const CarouselcustomArrowLeft = getIconPath('carousel-big-carrot-left');
    const CArouselcustomArrowRight = getIconPath('carousel-big-carrot');
    if (isMobile || isTablet) {
      return this.bannerRecommendationForScrolling(page, recommendationBannerHTML, LoadVariation);
    }

    const carProps =
      products.length <= customProductsLength
        ? { ...carouselProps, infinite: false }
        : carouselProps;

    return (
      <React.Fragment>
        <Row>
          <Col className="recommendation-banner" colSize={{ small: 2, medium: 2, large: 2.5 }}>
            <div className="bannerStyles">
              {this.showRecosBannerExperience(recommendationBannerHTML)}
            </div>
          </Col>
          <Col
            className="recommendation-listview"
            colSize={{ small: 6, medium: 8, large: 9 }}
            offsetLeft={{
              small: 0,
              medium: 0,
              large: 0,
            }}
            offsetRight={{
              small: 0,
              medium: 0,
              large: 0,
            }}
          >
            <Carousel
              className={`${variation}-variation`}
              options={carProps}
              inheritedStyles={Carousel}
              carouselConfig={{
                variation: CarouselVariation,
                customArrowLeft: CarouselcustomArrowLeft,
                customArrowRight: CArouselcustomArrowRight,
                dataLocatorCarousel: `${variation}-variation`,
              }}
            >
              {this.loadVariation(variation)}
            </Carousel>
          </Col>
        </Row>
      </React.Fragment>
    );
  };

  loadRecommendationVariation = (variation, product, index, itemClassName) => {
    const {
      products,
      className,
      loadedProductCount,
      onPickUpOpenClick,
      labels,
      currency,
      currencyAttributes,
      onQuickViewOpenClick,
      page,
      portalValue,
      priceOnly,
      isDynamicBadgeEnabled,
      starRatingSize,
      moduleOHeaderLabel,
      isNewPDPEnabled,
      isBagPage,
      bagItemCount,
      ...otherProps
    } = this.props;
    const { generalProductId, productInfo: { tcpStyleQty, tcpStyleType } = {} } = product;
    const priceOnlyClass = priceOnly ? 'price-only' : '';
    const isPromoAvailable = this.isPromoAvailable(products);
    return (
      <RecommendationComponentVariation
        key={`recommended_products_${variation}_${generalProductId}`}
        loadedProductCount={loadedProductCount}
        generalProductId={generalProductId}
        item={product}
        isPerfectBlock
        productsBlock={product}
        onPickUpOpenClick={onPickUpOpenClick}
        onQuickViewOpenClick={onQuickViewOpenClick}
        className={`${className} product-list ${priceOnlyClass}`}
        labels={labels}
        starRatingSize={starRatingSize}
        sequenceNumber={index + 1}
        variation={variation}
        currencySymbol={currency}
        currencyExchange={currencyAttributes.exchangevalue}
        viaModule={RECOMMENDATION}
        page={page}
        isPromoAvailable={isPromoAvailable}
        itemClassName={itemClassName}
        portalValue={portalValue}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        tcpStyleQty={tcpStyleQty}
        tcpStyleType={tcpStyleType}
        moduleOHeaderLabel={moduleOHeaderLabel}
        isNewPDPEnabled={isNewPDPEnabled}
        isBagPage={isBagPage}
        bagItemCount={bagItemCount}
        {...otherProps}
      />
    );
  };

  checkingProductCount = (showProducts, products) => {
    if (
      showProducts.length === 0 ||
      (showProducts.length !== products.length && products.length < viewProducts) ||
      (showProducts.length < viewProducts && showProducts.length < products.length)
    )
      return true;
    return false;
  };

  // eslint-disable-next-line complexity
  loadVariationNew = (variation) => {
    const { products, labels, portalValue } = this.props;
    const { showProducts, viewMoreProducts, showPdpProducts } = this.state;

    if (JSON.stringify(products) !== JSON.stringify(viewMoreProducts)) {
      if (products.length > 0 && showPdpProducts > 0) {
        if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED)
          setSessionStorage({ key: 'PDP2', value: showPdpProducts });
        else setSessionStorage({ key: 'PDP1', value: showPdpProducts });
      }
      this.setState({ showProducts: [], viewMoreProducts: products });
    }

    if (
      labels &&
      labels.addToBag &&
      typeof labels.addToBag === 'object' &&
      labels.addToBag !== null
    )
      return null;

    if (this.checkingProductCount(showProducts, products)) this.showMoreProducts();
    const recos =
      portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED ? 'pdp2' : 'pdp1';
    setLocalStorage({
      key: recos,
      value: showProducts.length > viewProducts ? showProducts.length : 0,
    });

    return (
      <React.Fragment>
        <RecommendationUl>
          {showProducts.map((product, index) => {
            const { generalProductId } = product;
            return (
              <RecommendationLi key={`recommended_products_li_${variation}_${generalProductId}`}>
                {this.loadRecommendationVariation(
                  variation,
                  product,
                  index,
                  'recommended_products_ul'
                )}
              </RecommendationLi>
            );
          })}
        </RecommendationUl>
        {products.length !== showProducts.length && this.showMoreButton()}
      </React.Fragment>
    );
  };

  checkIfMultiGridRecs = (portalValue, products) => {
    if (
      products &&
      products.length > 0 &&
      (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS ||
        portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.IN_MULTI_GRID_PLP_PRODUCTS)
    ) {
      return true;
    }
    return false;
  };

  loadVariation(variation, itemClassName = '') {
    const {
      products,
      labels,
      currency,
      currencyAttributes,
      styleWithView,
      plpLabels,
      portalValue,
      getRecommendationProducts,
      addToBagError,
      addToBagErrorId,
      styleHeaderLabel,
      isFBTEnabled,
      headerLabel,
      addedToBagData,
      updateFbtStatus,
      addToBagFbtError,
      isFBTDisplayed,
      ...otherProps
    } = this.props;

    getRecommendationProducts(products);
    if (
      labels &&
      labels.addToBag &&
      typeof labels.addToBag === 'object' &&
      labels.addToBag !== null
    )
      return null;
    if (this.haveFBTProducts(isFBTEnabled, products)) {
      const product = products[0];
      const { generalProductId } = product;
      const styleWithData = product.styleWithData && product.styleWithData.product;
      return styleWithData ? (
        <RecomFBT
          generalProductId={generalProductId}
          productItem={styleWithData}
          labels={labels}
          variation={variation}
          currencySymbol={currency}
          currencyExchange={currencyAttributes.exchangevalue}
          plpLabels={plpLabels}
          addToBagError={addToBagFbtError}
          addToBagErrorId={addToBagErrorId}
          portalValue={portalValue}
          headerLabel={headerLabel}
          addedToBagData={addedToBagData}
          updateFbtStatus={updateFbtStatus}
          isFBTDisplayed={isFBTDisplayed}
          {...otherProps}
        />
      ) : null;
    }

    if (this.haveStyleWithProducts(styleWithView, products)) {
      const product = products[0];
      const { generalProductId } = product;
      const styleWithData = product.styleWithData && product.styleWithData.product;
      return styleWithData ? (
        <RecomStyleWith
          generalProductId={generalProductId}
          productItem={styleWithData}
          labels={labels}
          variation={variation}
          currencySymbol={currency}
          currencyExchange={currencyAttributes.exchangevalue}
          plpLabels={plpLabels}
          addToBagError={addToBagError}
          addToBagErrorId={addToBagErrorId}
          portalValue={portalValue}
          styleHeaderLabel={styleHeaderLabel}
          {...otherProps}
        />
      ) : null;
    }

    return products.map((product, index) => {
      return this.loadRecommendationVariation(variation, product, index, itemClassName);
    });
  }

  handleAccordionToggle() {
    const { isAccordionOpen } = this.state;
    this.setState({ isAccordionOpen: !isAccordionOpen });
  }

  handleRecosHeightToggle() {
    const { isRecoHeightSmall } = this.state;
    this.setState({ isRecoHeightSmall: !isRecoHeightSmall });
  }

  renderItemList = (products, variation, carouselProps, showPeek, customProductsLength) => {
    const { newRecommendationEnabled, isBannerRecommendation } = this.props;
    const isMobileOrTablet = isClient()
      ? getViewportInfo().isMobile || getViewportInfo().isTablet
      : null;
    const minProductsLength = customProductsLength || productsLength;
    if (isBannerRecommendation) {
      return products.length > 0
        ? this.loadBannerRecommendation(variation, carouselProps, customProductsLength)
        : null;
    }
    if (newRecommendationEnabled) return this.loadVariationNew(variation);
    if (products.length >= minProductsLength) {
      return isMobileOrTablet ? (
        <div className="smooth-scroll-list">
          {this.loadVariation(variation, 'smooth-scroll-list-item')}
        </div>
      ) : (
        <Carousel
          className={`${variation}-variation`}
          options={carouselProps}
          inheritedStyles={Carousel}
          carouselConfig={{
            variation: 'big-arrows',
            customArrowLeft: getIconPath('carousel-big-carrot-left'),
            customArrowRight: getIconPath('carousel-big-carrot'),
            dataLocatorCarousel: `${variation}-variation`,
          }}
        >
          {this.loadVariation(variation)}
        </Carousel>
      );
    }
    return (
      <div className={`no-carousel-container ${variation}-variation`}>
        {this.loadVariation(variation)}
      </div>
    );
  };

  getHeaderLabelabel = () => {
    const {
      variation,
      isATBModalBackAbTestNewDesign,
      page,
      moduleOHeaderLabel,
      modulePHeaderLabel,
    } = this.props;
    let headerLabel =
      variation === config.variations.moduleO ? moduleOHeaderLabel : modulePHeaderLabel;
    if (
      !isATBModalBackAbTestNewDesign &&
      page === Constants.RECOMMENDATIONS_PAGES_MAPPING.ADDED_TO_BAG
    ) {
      headerLabel = capitalizeWords(headerLabel);
    }
    return headerLabel;
  };

  // eslint-disable-next-line complexity
  renderRecommendationView(variation) {
    const {
      products,
      priceOnly,
      showButton,
      ctaText,
      ctaTitle,
      ctaUrl,
      carouselConfigProps,
      headerAlignment,
      isFavoriteRecommendation,
      isSuggestedItem,
      accordionHeader,
      showHeightToggle,
      showPeek,
      getRecommendationProducts,
      customProductsLength,
      newRecommendationEnabled,
      portalValue,
      newBag,
      isNewPDPEnabled,
      outOfStock,
    } = this.props;

    getRecommendationProducts(products);

    const { isAccordionOpen, isRecoHeightSmall } = this.state;

    const priceOnlyClass = priceOnly ? 'price-only' : '';
    const params = config.params[variation];
    const headerLabel = this.getHeaderLabelabel();

    const carouselProps = {
      ...config.CAROUSEL_OPTIONS,
      ...carouselConfigProps,
      adaptiveHeight: false,
    };
    const showHeader = isShowHeader(
      isFavoriteRecommendation,
      isSuggestedItem,
      accordionHeader,
      portalValue
    );
    const accordionToggleClass = this.getAccordionClass(isAccordionOpen);
    const heightToggleClass = this.getRecoHeightStatus(isRecoHeightSmall);

    if (this.checkIfMultiGridRecs(portalValue, products))
      return this.loadInMultiGridPlpRecommendation(variation);

    return (
      products &&
      products.length > 0 && (
        <div
          className={`${
            isNewPDPEnabled && !outOfStock
              ? 'recommendations-container-new'
              : 'recommendations-container'
          }`}
        >
          {accordionHeader && (
            <BodyCopy
              className={`accordion-recommendation ${priceOnlyClass} ${accordionToggleClass}`}
              fontSize="fs14"
              component="div"
              fontFamily="secondary"
              fontWeight="black"
              onClick={this.handleAccordionToggle}
              id="recommendation"
            >
              {headerLabel}
            </BodyCopy>
          )}
          {showHeader && (
            <Heading
              variant="h4"
              className={`recommendations-header ${priceOnlyClass}`}
              textAlign={headerAlignment || 'center'}
              dataLocator={params.dataLocator}
              id={
                newRecommendationEnabled &&
                `recommendation${headerLabel ? headerLabel.split(' ').join('') : ''}`
              }
            >
              {headerLabel}
            </Heading>
          )}
          {showHeightToggle && <div className={`${heightToggleClass}-val`} />}
          <Row fullBleed className="recommendations-section-row">
            <RecommendationCol
              showPeek={showPeek}
              newRecommendationEnabled={newRecommendationEnabled}
              className="recommendations-section-col"
              colSize={{
                small: 6,
                medium: 8,
                large: 10,
              }}
              offsetLeft={{
                small: 0,
                medium: 0,
                large: 1,
              }}
            >
              {this.renderItemList(
                products,
                variation,
                carouselProps,
                showPeek,
                customProductsLength
              )}
            </RecommendationCol>
          </Row>
          {showButton && (
            <div className="recommendation-cta-container">
              <ButtonCTA
                className="recommendation-cta"
                uniqueKey="recommendation-button"
                dataLocator={{
                  cta: params.dataLocatorCTA,
                }}
                ctaInfo={{
                  ctaVariation: 'fixed-width',
                  link: {
                    url: ctaUrl,
                    title: ctaTitle,
                    text: ctaText,
                  },
                }}
                newBag={newBag}
              />
            </div>
          )}
          {showHeightToggle && (
            <BodyCopy
              className={`recommendation-height-toggle ${heightToggleClass}`}
              fontSize="fs14"
              component="div"
              fontFamily="secondary"
              fontWeight="black"
              onClick={this.handleRecosHeightToggle}
              id="recommendation-height"
            />
          )}
        </div>
      )
    );
  }

  render() {
    const {
      className,
      variations,
      ariaPrevious,
      ariaNext,
      reduxKey,
      styleWithView,
      products,
      isLoading,
      newBag,
      carouselConfigProps,
      isFBTEnabled,
    } = this.props;
    config.CAROUSEL_OPTIONS.prevArrow = (
      <button
        type="button"
        aria-label={ariaPrevious}
        data-locator="moduleO_left_arrow"
        className="slick-prev"
        tabIndex="0"
      />
    );
    config.CAROUSEL_OPTIONS.nextArrow = (
      <button
        type="button"
        aria-label={ariaNext}
        data-locator="moduleO_right_arrow"
        className="slick-prev"
        tabIndex="0"
      />
    );

    const variation = variations.split(',');

    if (reduxKey === 'favorites_global_products') {
      return variation.map((value) => {
        const recVal = this.renderRecommendationView(value);
        const recClass = recVal ? 'recommendation-container full' : 'recommendation-container';
        return (
          <div className={recClass}>
            <section className={`${className} recommendations-tile`}>{recVal}</section>
          </div>
        );
      });
    }
    if (this.haveFBTProducts(isFBTEnabled, products)) {
      return <>{this.loadVariation('FBT')}</>;
    }

    if (this.haveStyleWithProducts(styleWithView, products)) {
      return <>{this.loadVariation('styleWith')}</>;
    }

    if (isLoading) return <React.Fragment />;

    return (
      <div
        className={`recommendation-container ${
          products.length > 4 && carouselConfigProps?.slidesToShow > 5
            ? 'many-item-recommendation-non-empty-bag'
            : ''
        }`}
      >
        {variation.map((value) => {
          return (
            <section
              className={`${className} recommendations-tile ${newBag ? 'rounded-container' : ''} `}
            >
              {this.renderRecommendationView(value)}
            </section>
          );
        })}
      </div>
    );
  }
}

Recommendations.propTypes = {
  loadRecommendationsAdobe: func.isRequired,
  loadRecommendationsMonetate: func.isRequired,
  moduleOHeaderLabel: string.isRequired,
  modulePHeaderLabel: string.isRequired,
  products: arrayOf(oneOfType(shape({}))).isRequired,
  className: string.isRequired,
  isPlcc: string,
  loadedProductCount: number.isRequired,
  onPickUpOpenClick: func.isRequired,
  priceOnly: bool,
  showButton: bool,
  ctaText: string,
  ctaTitle: string,
  ctaUrl: string,
  variations: string,
  currency: string,
  currencyAttributes: shape({ exchangevalue: number }),
  onQuickViewOpenClick: func.isRequired,
  page: string,
  portalValue: string,
  carouselConfigProps: shape({}),
  partNumber: string,
  categoryName: string,
  headerAlignment: string,
  reduxKey: string.isRequired,
  ariaPrevious: string,
  ariaNext: string,
  isFavoriteRecommendation: bool,
  isSuggestedItem: bool,
  router: shape({}).isRequired,
  excludedIds: string,
  accordionHeader: bool,
  showHeightToggle: bool,
  showPeek: shape({}),
  starRatingSize: shape({}),
  customProductsLength: number,
  hasSuggestedProduct: bool,
  provider: string.isRequired,
  MONETATE_ENABLED: bool,
  cartItems: arrayOf({}),
  isBagLoading: bool,
  isOutfitLoading: bool,
  outfitItems: arrayOf({}),
  styleWithView: bool,
  headerLabel: string,
  plpLabels: shape({}),
  userEmail: string,
  newRecommendationEnabled: bool,
  addedToBagData: shape({}),
  pdpDataInformation: shape({}).isRequired,
  getRecommendationProducts: func,
  addToBagError: string,
  addToBagErrorId: string,
  styleHeaderLabel: string,
  styleWithEligibility: bool,
  isBannerRecommendation: bool,
  recommendationBannerHTML: string,
  outOfStock: bool,
  isLoading: bool.isRequired,
  isDynamicBadgeEnabled: shape({}),
  index: number,
  slots: shape([]),
  slotsList: shape([]),
  inGridPlpRecommendation: bool,
  isRecProduct: string,
  actionId: string,
  labels: shape({
    addToBag: string,
    inGridHeader: string,
    inMultiGridHeader: string,
  }).isRequired,
  isNewPDPEnabled: bool,
  newBag: bool,
  isBagPage: bool,
  isATBModalBackAbTestNewDesign: bool,
  bagItemCount: bool,
};

Recommendations.defaultProps = {
  priceOnly: false,
  showButton: false,
  ctaText: '',
  ctaTitle: '',
  ctaUrl: '',
  variations: '',
  currency: 'USD',
  currencyAttributes: {
    exchangevalue: 1,
  },
  page: '',
  portalValue: '',
  carouselConfigProps: null,
  partNumber: '',
  categoryName: '',
  headerAlignment: '',
  ariaPrevious: '',
  ariaNext: '',
  isFavoriteRecommendation: false,
  isSuggestedItem: false,
  excludedIds: '',
  accordionHeader: false,
  showHeightToggle: false,
  showPeek: { small: false, medium: false },
  starRatingSize: { small: 72, large: 100 },
  customProductsLength: 0,
  hasSuggestedProduct: false,
  MONETATE_ENABLED: true,
  cartItems: [],
  isBagLoading: false,
  isOutfitLoading: false,
  outfitItems: [],
  styleWithView: false,
  headerLabel: null,
  plpLabels: {},
  isPlcc: false,
  userEmail: '',
  newRecommendationEnabled: false,
  addedToBagData: {},
  getRecommendationProducts: () => {},
  addToBagError: '',
  addToBagErrorId: '',
  styleHeaderLabel: '',
  styleWithEligibility: false,
  isBannerRecommendation: false,
  recommendationBannerHTML: '</>',
  outOfStock: false,
  isDynamicBadgeEnabled: false,
  index: 0,
  slots: [],
  slotsList: [],
  inGridPlpRecommendation: false,
  isRecProduct: '',
  actionId: '',
  isNewPDPEnabled: false,
  newBag: false,
  isBagPage: false,
  isATBModalBackAbTestNewDesign: false,
  bagItemCount: false,
};
export { Recommendations as RecommendationsVanilla };

export default errorBoundary(withStyles(Recommendations, style));
