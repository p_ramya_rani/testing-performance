// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { getLabelValue } from '@tcp/core/src/utils';
import PropTypes from 'prop-types';
import { openPickupModalWithValues } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import {
  fetchRecommendationsDataMonetate,
  fetchRecommendationsDataAdobe,
  sendEventsToMonetate,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.actions';
import {
  addToCartEcom,
  updateFbtViewStatus,
} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';
import {
  getProducts,
  getLoadedProductsCount,
  getLabelsProductListing,
  getFirstSuggestedProduct,
  getRecommendationsAbTest,
  getRecommendationModuleOLabel,
  getRecommendationModulePLabel,
  getABTestConfig,
  getLabelSuffix,
  getMboxName,
  getMonetateEnabledFlag,
  getUpdateFlag,
  getRecommendationLoadingState,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import {
  getProductDetailFormValues,
  getPlpLabels,
} from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.selectors';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import {
  getLoadingState,
  getOutfitProducts,
  getAddedToBagErrorCatId,
} from '@tcp/core/src/components/features/browse/OutfitDetails/container/OutfitDetails.selectors';
import {
  getAddedToBagError,
  getAddedToBagFbtError,
} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import ConfirmationSelector from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.selectors';
import { openQuickViewWithValues } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  getUserEmail,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {
  getIsDynamicBadgeEnabled,
  getIsPDPSmoothScrollEnabled,
  getIsNewPDPEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import RecommendationsView from '../Recommendations';

// eslint-disable-next-line sonarjs/cognitive-complexity, complexity
function getRecommendationProducts(state, ownProps, partNumber, reduxKey) {
  let { page } = ownProps;
  if (page === 'home page') page = 'homepage';

  const MONETATE_ENABLED = getMonetateEnabledFlag();
  let products = [];
  if (MONETATE_ENABLED === true)
    products = ownProps.isSuggestedItem
      ? getFirstSuggestedProduct(state, partNumber)
      : getProducts(state, page || 'homepageTest');
  else {
    products = ownProps.isSuggestedItem
      ? getFirstSuggestedProduct(state, partNumber)
      : getProducts(state, reduxKey);
    return products;
  }
  const productsList = [];
  if (!Array.isArray(products)) return [];
  for (let i = 0; i < products.length; i += 1) {
    switch (ownProps.portalValue) {
      case Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED:
        if (products[i].isRecentViewed) productsList.push(products[i]);
        break;
      case Constants.RECOMMENDATIONS_MBOXNAMES.CHECKOUT_SIMILAR_STYLES:
        if (products[i].isPdpPlaProducts) productsList.push(products[i]);
        break;
      case Constants.RECOMMENDATIONS_MBOXNAMES.OUT_OF_STOCK_PDP:
        if (products[i].isAbPdpOos) productsList.push(products[i]);
        break;
      case Constants.RECOMMENDATIONS_MBOXNAMES.PLP_PRODUCTS:
        if (products[i].isPlpProducts) productsList.push(products[i]);
        break;
      case Constants.RECOMMENDATIONS_MBOXNAMES.ADD_PRODUCT_TO_GET_PLACECASH:
        if (products[i].isAbCartCash) productsList.push(products[i]);
        break;
      case Constants.RECOMMENDATIONS_MBOXNAMES.BAG_CART_REWARD_PLCC:
        if (products[i].isAbCartReward) productsList.push(products[i]);
        break;
      case Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS:
        if (products[i].isInGridPlp) productsList.push(products[i]);
        break;
      case Constants.RECOMMENDATIONS_MBOXNAMES.IN_MULTI_GRID_PLP_PRODUCTS:
        if (products[i].isInMultiGridPlp) productsList.push(products[i]);
        break;
      case 'styleWith':
        if (products[i].styleWithEligibility || products[i].isRecentViewed === false) {
          productsList.push(products[i]);
        }
        break;
      case 'fbt':
        if (products[i].isFbt) {
          productsList.push(products[i]);
        }
        break;
      default:
        if (products[i].isRecentViewed === false) productsList.push(products[i]);
        break;
    }
  }
  return productsList;
}

const getReduxKey = (page, newPortalValue) => `${page}_${newPortalValue || 'global'}productsData`;
const getCtaText = (abTestConfig, labelSuffix) =>
  abTestConfig ? `CTA_TEXT_${labelSuffix}` : 'CTA_TEXT';
const getCtaTitle = (abTestConfig, labelSuffix) =>
  abTestConfig ? `CTA_TITLE_${labelSuffix}` : 'CTA_TITLE';
const getCtaUrl = (abTestConfig, labelSuffix) =>
  abTestConfig ? `CTA_URL_${labelSuffix}` : 'CTA_URL';

// eslint-disable-next-line
const mapStateToProps = (state, ownProps) => {
  const {
    page,
    portalValue,
    excludedIds,
    partNumber,
    isOptimisticModalWithError,
    topRecommendationOOS,
  } = ownProps;
  const { moduleClassName = '', icidParam = '' } = ownProps;
  const MONETATE_ENABLED = getMonetateEnabledFlag();
  let newPortalValue = portalValue;
  let abTestConfig = null;
  let labelSuffix = `${page}_${ownProps.sequence}`;

  const abTestRecommendations = getRecommendationsAbTest(state, ownProps.isAddedToBag);

  if (abTestRecommendations) {
    abTestConfig = getABTestConfig(abTestRecommendations, ownProps);
    newPortalValue = getMboxName(abTestConfig);
    labelSuffix = getLabelSuffix(page, ownProps, abTestRecommendations);
  }

  const moduleOHeaderLabel = getRecommendationModuleOLabel(
    abTestConfig,
    state,
    labelSuffix,
    ownProps,
    getLabelValue,
    isOptimisticModalWithError
  );

  const modulePHeaderLabel = getRecommendationModulePLabel(
    abTestConfig,
    state,
    labelSuffix,
    ownProps,
    getLabelValue,
    isOptimisticModalWithError
  );

  const reduxKey = getReduxKey(page, newPortalValue);
  const ctaText = getCtaText(abTestConfig, labelSuffix);
  const ctaTitle = getCtaTitle(abTestConfig, labelSuffix);
  const ctaUrl = getCtaUrl(abTestConfig, labelSuffix);

  const cartItems = [];
  let isBagLoading = false;
  let isOutfitLoading = false;
  let outfitItems = [];

  try {
    if (page === 'cart') {
      isBagLoading = getUpdateFlag(state);
      if (!isBagLoading) {
        const items = BagPageSelector.getOrderItems(state);
        items.forEach((item) => {
          cartItems.push({
            value: BagPageSelector.getItemPrice(item),
            sku: BagPageSelector.getItemUPC(item),
            pid: BagPageSelector.getItemPartNumber(item),
            quantity: BagPageSelector.getItemQuantity(item),
            currency: getCurrentCurrency(state),
          });
        });
      }
    }

    if (page === 'checkout') {
      const items = ConfirmationSelector.getOrderItems(state);
      if (Array.isArray(items)) {
        items.forEach((item) => {
          if (item) {
            cartItems.push({
              value: item.productInfo.offerPrice,
              sku: item.itemAtributes.UPC,
              pid: item.productInfo.productPartNumber,
              quantity: item.qty,
              currency: getCurrentCurrency(state),
            });
          }
        });
      }
    }

    if (page === 'outfit') {
      isOutfitLoading = getLoadingState(state);
      if (!isOutfitLoading) {
        const items = getOutfitProducts(state);
        outfitItems = items.map((item) => ({ productId: item.generalProductId }));
      }
    }
  } catch (e) {
    logger.error({
      error: e,
      extraData: {
        page,
        methodName: Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
        propsReceived: ownProps,
      },
    });
  }
  return {
    categoryName: ownProps.categoryName,
    products: getRecommendationProducts(state, ownProps, partNumber, reduxKey),
    moduleOHeaderLabel,
    modulePHeaderLabel,
    moduleClassName,
    icidParam,
    ctaText: getLabelValue(
      state.Labels,
      ctaText,
      Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
      'global'
    ),
    ctaTitle: getLabelValue(
      state.Labels,
      ctaTitle,
      Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
      'global'
    ),
    ctaUrl: getLabelValue(
      state.Labels,
      ctaUrl,
      Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
      'global'
    ),
    ariaPrevious: getLabelValue(state.Labels, 'previousButton', 'accessibility', 'global'),
    ariaNext: getLabelValue(state.Labels, 'nextIconButton', 'accessibility', 'global'),
    loadedProductCount: getLoadedProductsCount(state),
    labels: getLabelsProductListing(state),
    currency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    isPlcc: isPlccUser(state),
    reduxKey,
    excludedIds,
    portalValue: newPortalValue,
    showPriceRange: ownProps.showPriceRange,
    cartItems,
    isBagLoading,
    isOutfitLoading,
    outfitItems,
    MONETATE_ENABLED,
    formValues: getProductDetailFormValues(state),
    plpLabels: getPlpLabels(state),
    userEmail: getUserEmail(state),
    addToBagError: getAddedToBagError(state),
    addToBagErrorId: getAddedToBagErrorCatId(state),
    isLoading: getRecommendationLoadingState(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    isPDPSmoothScrollEnabled: getIsPDPSmoothScrollEnabled(state),
    isNewPDPEnabled: getIsNewPDPEnabled(state) && page === 'pdp' && !topRecommendationOOS,
    addToBagFbtError: getAddedToBagFbtError(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadRecommendationsMonetate: (action) => {
      dispatch(fetchRecommendationsDataMonetate(action));
    },
    loadSendEventsToMonetate: (action) => {
      dispatch(sendEventsToMonetate(action));
    },
    loadRecommendationsAdobe: (action) => {
      dispatch(fetchRecommendationsDataAdobe(action));
    },
    onPickUpOpenClick: (payload) => {
      dispatch(openPickupModalWithValues(payload));
    },
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    addToBagEcomAction: (payload) => {
      dispatch(addToCartEcom(payload));
    },
    updateFbtStatus: (payload) => {
      dispatch(updateFbtViewStatus(payload));
    },
  };
};
RecommendationsView.propTypes = {
  isNewPDPEnabled: PropTypes.bool,
  topRecommendationOOS: PropTypes.bool,
  bagItemCount: PropTypes.bool,
};

RecommendationsView.defaultProps = {
  isNewPDPEnabled: false,
  topRecommendationOOS: false,
  bagItemCount: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(RecommendationsView);
