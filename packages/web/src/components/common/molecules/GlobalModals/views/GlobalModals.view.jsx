// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import PropTypes from 'prop-types';
import DynamicModule from '@tcp/core/src/components/common/atoms/DynamicModule';

const GlobalModals = ({
  isApplyNowOpen,
  isOpenDialog,
  isLoginModalOpen,
  // isQMModalOpen,
  bagDeleteItemInfo,
  bagConfirmationModalInfo,
}) => {
  return (
    <>
      {(isLoginModalOpen || bagDeleteItemInfo.showModal || bagConfirmationModalInfo.showModal) && (
        <DynamicModule
          key="checkout-modals"
          importCallback={() =>
            import(
              /* webpackChunkName: "checkout-modals" */ '@tcp/core/src/components/features/CnC/common/organism/CheckoutModals'
            )
          }
        />
      )}
      {isOpenDialog && (
        <DynamicModule
          key="added-to-bag"
          importCallback={() =>
            import(
              /* webpackChunkName: "added-to-bag" */ '@tcp/core/src/components/features/CnC/AddedToBag'
            )
          }
          isOpenDialog={isOpenDialog}
        />
      )}
      {isApplyNowOpen && (
        <DynamicModule
          key="apply-now"
          importCallback={() =>
            import(
              /* webpackChunkName: "apply-now-modal" */ '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal'
            )
          }
        />
      )}
      {/* {isQMModalOpen && ( */}
      <DynamicModule
        key="qm-recommendation"
        importCallback={() =>
          import(
            /* webpackChunkName: "qmrecommendation-modal" */ '@tcp/core/src/components/common/molecules/QMRecommendationsModal'
          )
        }
      />
      {/* )} */}
    </>
  );
};

GlobalModals.propTypes = {
  isApplyNowOpen: PropTypes.bool,
  isOpenDialog: PropTypes.bool,
  isLoginModalOpen: PropTypes.bool,
  // isQMModalOpen: PropTypes.bool,
  bagDeleteItemInfo: PropTypes.shape({
    showModal: PropTypes.bool,
  }),
  bagConfirmationModalInfo: PropTypes.shape({
    showModal: PropTypes.bool,
  }),
};

GlobalModals.defaultProps = {
  isApplyNowOpen: false,
  isOpenDialog: false,
  isLoginModalOpen: false,
  // isQMModalOpen: false,
  bagDeleteItemInfo: {
    showModal: false,
  },
  bagConfirmationModalInfo: {
    showModal: false,
  },
};

export default GlobalModals;
