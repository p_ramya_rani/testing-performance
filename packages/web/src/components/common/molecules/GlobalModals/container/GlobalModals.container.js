// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import {
  getIsModalOpen,
  getIsPLCCModalOpen,
} from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.selectors';
import { getIsQMModalOpen } from '@tcp/core/src/components/common/molecules/QMRecommendationsModal/container/QMRecommendationModal.selectors';
import { isOpenAddedToBag } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import { checkoutModalOpenState } from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.selectors';
import {
  getCurrentDeleteSelectedItemInfo,
  getConfirmationModalFlag,
} from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';

import GlobalModals from '../views/GlobalModals.view';

const mapStateToProps = state => {
  return {
    isApplyNowOpen: getIsModalOpen(state) || getIsPLCCModalOpen(state),
    isQMModalOpen: getIsQMModalOpen(state),
    isOpenDialog: isOpenAddedToBag(state),
    isLoginModalOpen: checkoutModalOpenState(state),
    bagDeleteItemInfo: getCurrentDeleteSelectedItemInfo(state),
    bagConfirmationModalInfo: getConfirmationModalFlag(state),
  };
};

export default connect(mapStateToProps)(GlobalModals);
