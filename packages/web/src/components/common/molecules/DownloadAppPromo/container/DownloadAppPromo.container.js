// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { getLabelValue, getViewportInfo, isCanada, isClient } from '@tcp/core/src/utils';
import { downloadAppPromoEnabledFooter } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import DownloadAppPromo from '../views/DownloadAppPromo.view';

const mapStateToProps = (state, ownProps) => {
  const downloadAppPromoLabels = {
    title: getLabelValue(state.Labels, 'lbl_title', 'DownloadAppPromo', 'global'),
    subTitle: getLabelValue(state.Labels, 'lbl_subTitle', 'DownloadAppPromo', 'global'),
    cta: getLabelValue(state.Labels, 'lbl_download_cta', 'DownloadAppPromo', 'global'),
  };
  const downloadAppPromoNavVariation = state.AbTest && state.AbTest.downloadAppPromoAbtest;
  const { isMobile } = isClient() && getViewportInfo();
  return {
    downloadAppPromoLabels,
    downloadAppPromoEnabled: downloadAppPromoEnabledFooter(state),
    isFooter: ownProps.isFooter,
    downloadAppPromoNavVariation,
    renderAppBanner: !isCanada() && isMobile,
  };
};

export default connect(mapStateToProps)(DownloadAppPromo);
