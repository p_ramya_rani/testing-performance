// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { isiOSWeb, getBrand } from '@tcp/core/src/utils';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { BodyCopy, DamImage, Anchor } from '@tcp/core/src/components/common/atoms';
import { getAppInstallPageUrl } from '@tcp/core/src/components/common/atoms/AppBanner/AppBanner.utils';
import style from '../styles/DownloadAppPromo.style';
import dwnldPromoConfig from '../DownloadAppPromo.config';

class DownloadAppPromo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.variation = {
      storeButtonPhoneIcon: 'storeButtonPhoneIcon',
      storeButtonGenericIcon: 'storeButtonGenericIcon',
      genericButtonGenericIcon: 'genericButtonGenericIcon',
    };
  }

  canComponentRender = () => {
    const { downloadAppPromoEnabled, isFooter, downloadAppPromoNavVariation, renderAppBanner } =
      this.props;
    return (
      renderAppBanner &&
      ((isFooter && downloadAppPromoEnabled) || (!isFooter && downloadAppPromoNavVariation))
    );
  };

  redirectToAppStore = () => {
    window.location = getAppInstallPageUrl();
  };

  getStoreButtonImage = (isIos, storeVariation = false) => {
    const { isFooter } = this.props;
    let buttonImage = '';

    if (isIos) {
      buttonImage =
        isFooter || storeVariation ? 'ios-app-store-btn.png' : 'ios-app-store-btn-long.png';
    } else {
      buttonImage =
        isFooter || storeVariation ? 'android-app-store-btn.png' : 'android-app-store-btn-long.png';
    }
    return buttonImage;
  };

  getStoreButtonGenericIcon = (isIos) => {
    return {
      bannerImgData: {
        alt: 'download-img-icon',
        url: 'generic_icon_dwnld.png',
      },
      storeImgData: {
        alt: 'download-button',
        url: this.getStoreButtonImage(isIos),
      },
      iconImgConfig: dwnldPromoConfig.IMG_DATA.genericIcon,
      storeIconConfig: dwnldPromoConfig.IMG_DATA.storeBtn,
    };
  };

  getGenericButtonGenericIcon = () => {
    return {
      bannerImgData: {
        alt: 'download-img',
        url: 'generic_icon_dwnld.png',
      },
      iconImgConfig: dwnldPromoConfig.IMG_DATA.genericIcon,
      storeIconConfig: dwnldPromoConfig.IMG_DATA.storeBtn,
    };
  };

  getStoreButtonPhoneIcon = (isIos, isTCP) => {
    return {
      bannerImgData: {
        alt: 'download-img-phone',
        url: isTCP ? 'tcp-phone-dwnld.png' : 'gym-phone-dwnld.png',
      },
      storeImgData: {
        alt: 'download-button',
        url: this.getStoreButtonImage(isIos, true),
      },
      iconImgConfig: dwnldPromoConfig.IMG_DATA.phoneIcon,
      storeIconConfig: dwnldPromoConfig.IMG_DATA.storeBtn,
    };
  };

  getConfigForVariation = (variation) => {
    let configObj = {};
    const isIos = isiOSWeb();
    const isTCP = getBrand() === 'tcp';

    if (variation === this.variation.genericButtonGenericIcon) {
      configObj = this.getGenericButtonGenericIcon();
    } else if (variation === this.variation.storeButtonGenericIcon) {
      configObj = this.getStoreButtonGenericIcon(isIos, isTCP);
    } else {
      configObj = this.getStoreButtonPhoneIcon(isIos, isTCP);
    }
    configObj.variation = variation;
    configObj.isTCP = isTCP;
    return configObj;
  };

  getComponentConfig = () => {
    const { isFooter, downloadAppPromoNavVariation } = this.props;
    const variation = isFooter ? this.variation.storeButtonPhoneIcon : downloadAppPromoNavVariation;
    return this.getConfigForVariation(variation);
  };

  getEvars = (isFooter) => (isFooter ? 'footer' : 'navigation');

  getEventData = (isTCP) => (isTCP ? ['event170'] : ['event171']);

  getEventName = (isFooter) => (isFooter ? 'download promo: footer' : 'download promo: navigation');

  renderStoreIconButtonVariation = (config, downloadAppPromoLabels, isFooter) => {
    const classes = `downloadAppPromo ${
      isFooter ? 'downloadAppPromo-storeIcnBtnFooter' : 'downloadAppPromo-storeIcnBtnNav'
    }`;

    return (
      <div className={classes}>
        <div className="downloadAppPromo-left">
          <DamImage
            data-locator="downloadAppPromo-icon"
            imgConfigs={config.iconImgConfig}
            imgData={config.bannerImgData}
            itemProp="image"
            height={isFooter ? '102px' : '100px'}
          />
        </div>
        <div className="downloadAppPromo-right">
          <BodyCopy
            fontSize={isFooter ? 'fs15' : 'fs10'}
            fontFamily="primary"
            className="downloadAppPromo-title"
            data-locator="downloadAppPromo-title"
            fontWeight="black"
          >
            {downloadAppPromoLabels.title}
          </BodyCopy>
          <BodyCopy
            fontSize={isFooter ? 'fs13' : 'fs12'}
            fontFamily="secondary"
            className="downloadAppPromo-subTitle"
            data-locator="downloadAppPromo-subTitle"
          >
            {downloadAppPromoLabels.subTitle}
          </BodyCopy>
          <ClickTracker
            as={Anchor}
            noLink
            clickData={{
              customEvents: this.getEventData(config.isTCP),
              eventName: this.getEventName(isFooter),
              appDownloadLocation: this.getEvars(isFooter),
            }}
            onClick={(e) => {
              e.preventDefault();
              this.redirectToAppStore();
            }}
            className="downloadAppPromo-cta"
          >
            <DamImage
              data-locator="downloadAppPromo-store-icon"
              imgConfigs={config.storeIconConfig}
              imgData={config.storeImgData}
              itemProp="image"
              title=""
              height="32px"
              className="downloadAppPromo-cta-icon"
            />
          </ClickTracker>
        </div>
      </div>
    );
  };

  renderGenericIconGenericButton = (config, downloadAppPromoLabels, isFooter) => {
    return (
      <div className="downloadAppPromo downloadAppPromo-genericIconBtn">
        <div className="downloadAppPromo-top">
          <BodyCopy
            fontSize="fs10"
            fontFamily="primary"
            className="downloadAppPromo-title"
            data-locator="downloadAppPromo-title"
            fontWeight="black"
            textAlign="center"
          >
            {downloadAppPromoLabels.title}
          </BodyCopy>
        </div>
        <div className="donloadAppPromo-middle">
          <div className="downloadAppPromo-left">
            <DamImage
              data-locator="downloadAppPromo-icon"
              imgConfigs={config.iconImgConfig}
              imgData={config.bannerImgData}
              itemProp="image"
              title=""
              height="36px"
            />
          </div>
          <div className="downloadAppPromo-right">
            <BodyCopy
              fontSize="fs12"
              fontFamily="secondary"
              className="downloadAppPromo-subTitle"
              data-locator="downloadAppPromo-subTitle"
            >
              {downloadAppPromoLabels.subTitle}
            </BodyCopy>
          </div>
        </div>
        <div className="donloadAppPromo-bottom">
          <ClickTracker
            as={Anchor}
            noLink
            clickData={{
              customEvents: this.getEventData(config.isTCP),
              eventName: this.getEventName(isFooter),
              appDownloadLocation: this.getEvars(isFooter),
            }}
            onClick={(e) => {
              e.preventDefault();
              this.redirectToAppStore();
            }}
            className="downloadAppPromo-cta"
          >
            {config.variation === this.variation.genericButtonGenericIcon ? (
              <button
                className="downloadAppPromo-cta-btn"
                data-locator={downloadAppPromoLabels.subTitle}
                aria-labelledby={downloadAppPromoLabels.cta}
              >
                <BodyCopy
                  fontSize="fs10"
                  fontFamily="secondary"
                  fontWeight="extrabold"
                  className="downloadAppPromo-cta-text"
                  data-locator="downloadAppPromo-store-icon"
                  textAlign="center"
                  color="white"
                  lineHeight="lh107"
                  letterSpacing="ls1"
                >
                  {downloadAppPromoLabels.cta}
                </BodyCopy>
              </button>
            ) : (
              <DamImage
                data-locator="downloadAppPromo-store-icon"
                imgConfigs={config.storeIconConfig}
                imgData={config.storeImgData}
                itemProp="image"
                height="32px"
                className="downloadAppPromo-cta-icon"
              />
            )}
          </ClickTracker>
        </div>
      </div>
    );
  };

  renderNavigationBanner = (config, downloadAppPromoLabels, isFooter) => {
    return config.variation === this.variation.storeButtonPhoneIcon
      ? this.renderStoreIconButtonVariation(config, downloadAppPromoLabels, isFooter)
      : this.renderGenericIconGenericButton(config, downloadAppPromoLabels, isFooter);
  };

  render() {
    const { className, downloadAppPromoLabels, isFooter } = this.props;
    const renderComponent = this.canComponentRender();
    const config = renderComponent ? this.getComponentConfig() : {};

    return renderComponent ? (
      <div className={`${className}`}>
        {isFooter
          ? this.renderStoreIconButtonVariation(config, downloadAppPromoLabels, isFooter)
          : this.renderNavigationBanner(config, downloadAppPromoLabels, isFooter)}
      </div>
    ) : null;
  }
}

DownloadAppPromo.defaultProps = {
  className: '',
  downloadAppPromoLabels: {},
  downloadAppPromoEnabled: false,
  downloadAppPromoNavVariation: '',
  isFooter: false,
};

DownloadAppPromo.propTypes = {
  className: PropTypes.string,
  downloadAppPromoLabels: PropTypes.shape({}),
  downloadAppPromoEnabled: PropTypes.bool,
  downloadAppPromoNavVariation: PropTypes.string,
  isFooter: PropTypes.bool,
  renderAppBanner: PropTypes.bool.isRequired,
};

export default withStyles(DownloadAppPromo, style);

export { DownloadAppPromo as DownloadAppPromoVanilla };
