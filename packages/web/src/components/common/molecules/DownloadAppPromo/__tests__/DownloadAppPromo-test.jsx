// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { DownloadAppPromoVanilla } from '../views/DownloadAppPromo.view';

jest.mock('@tcp/core/src/utils', () => ({
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  getViewportInfo: () => ({
    isMobile: true,
  }),
  isCanada: jest.fn(),
  isMobile: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getBrand: () => 'tcp',
  isiOSWeb: jest.fn(),
}));

describe('DownloadAppPromo component Footer', () => {
  it('shoudld not render when downloadAppPromoEnabled kill switch is false', () => {
    const props = {
      className: '',
      downloadAppPromoLabels: {
        lbl_title: '',
        lbl_subTitle: '',
      },
      isFooter: true,
      downloadAppPromoEnabled: false,
      downloadAppPromoNavVariation: '',
      renderAppBanner: true,
    };
    const component = shallow(<DownloadAppPromoVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('component should render if kill switch is true', () => {
    const props = {
      className: '',
      downloadAppPromoLabels: {
        lbl_title: '',
        lbl_subTitle: '',
      },
      isFooter: true,
      downloadAppPromoEnabled: true,
      downloadAppPromoNavVariation: '',
      renderAppBanner: true,
    };
    const component = shallow(<DownloadAppPromoVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});

describe('DownloadAppPromo component Navigation', () => {
  it('shoudld not render when downloadAppPromoNavVariation is blank', () => {
    const props = {
      className: '',
      downloadAppPromoLabels: {
        lbl_title: '',
        lbl_subTitle: '',
      },
      isFooter: false,
      downloadAppPromoEnabled: true,
      downloadAppPromoNavVariation: '',
      renderAppBanner: true,
    };
    const component = shallow(<DownloadAppPromoVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('shoudld render when downloadAppPromoNavVariation is set', () => {
    const props = {
      className: '',
      downloadAppPromoLabels: {
        lbl_title: '',
        lbl_subTitle: '',
      },
      isFooter: false,
      downloadAppPromoEnabled: true,
      downloadAppPromoNavVariation: 'storeButtonPhoneIcon',
      renderAppBanner: true,
    };
    const component = shallow(<DownloadAppPromoVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('shoudld render when downloadAppPromoNavVariation is set storeButtonGenericIcon', () => {
    const props = {
      className: '',
      downloadAppPromoLabels: {
        lbl_title: '',
        lbl_subTitle: '',
      },
      isFooter: false,
      downloadAppPromoEnabled: true,
      downloadAppPromoNavVariation: 'storeButtonGenericIcon',
      renderAppBanner: true,
    };
    const component = shallow(<DownloadAppPromoVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('shoudld render when downloadAppPromoNavVariation is set genericButtonGenericIcon', () => {
    const props = {
      className: '',
      downloadAppPromoLabels: {
        lbl_title: '',
        lbl_subTitle: '',
      },
      isFooter: false,
      downloadAppPromoEnabled: true,
      downloadAppPromoNavVariation: 'genericButtonGenericIcon',
      renderAppBanner: true,
    };
    const component = shallow(<DownloadAppPromoVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
