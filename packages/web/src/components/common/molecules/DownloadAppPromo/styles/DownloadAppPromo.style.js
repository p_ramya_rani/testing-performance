// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const DownloadAppPromo = css`
  .downloadAppPromo {
    background-color: ${props => props.theme.colors.PRIMARY.PALEGRAY};

    &-left {
      margin-right: 24px;
    }

    &-right {
      width: 70%;
      position: relative;
    }

    &-cta {
      display: block;
    }

    &-cta-icon {
      position: absolute;
    }

    &-title {
      color: ${props => props.theme.colors.TEXT.DARKGRAY};
      margin-bottom: 2px;
    }

    &-storeIcnBtnFooter {
      padding: 19px 22px 22px 22px;
      margin: 0 -12px;
      outline: 2px solid ${props => props.theme.colors.PRIMARY.PALEGRAY};
      display: flex;
      height: 98px;
      .downloadAppPromo-right {
        width: 72%;
      }
      .downloadAppPromo-cta-icon {
        bottom: -2px;
      }
    }

    &-storeIcnBtnNav {
      margin: -50px -28px 16px -28px;
      padding: 28px 18px 26px 14px;
      display: flex;

      .downloadAppPromo-left {
        margin-right: 16px;
      }
      .downloadAppPromo-right {
        width: 77%;
      }
      .downloadAppPromo-cta-icon {
        bottom: 4px;
      }
    }

    &-genericIconBtn {
      padding: 22px 15px 26px 15px;
      margin: -50px -28px 16px -28px;

      .donloadAppPromo-middle {
        display: flex;
      }

      .downloadAppPromo-top {
        padding: 0 0 11px 0;
      }

      .downloadAppPromo-left {
        margin-right: 12px;
      }

      .downloadAppPromo-right {
        width: 76%;
      }

      .downloadAppPromo-cta-icon {
        position: static;
        margin-top: 14px;
        width: 100%;
      }

      .downloadAppPromo-cta-btn {
        height: 32px;
        background-color: ${props => props.theme.colorPalette.blue.C900};
        margin-top: 14px;
        width: 100%;
        border: none;
      }
    }
  }
`;

export default DownloadAppPromo;
