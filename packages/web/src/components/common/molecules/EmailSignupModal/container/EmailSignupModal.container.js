// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import get from 'lodash/get';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import { trackPageView, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import {
  submitEmailSignup,
  validateEmail,
  clearEmailSignupForm,
} from '@tcp/core/src/components/common/organisms/EmailSignupForm/container/EmailSignupForm.actions';
import { toggleEmailSignupModal } from './EmailSignupModal.actions';
import SignupModalView from '../views/EmailSignupModal.view';
import { getDefaultStore } from '../../../../../../../core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';

export const mapDispatchToProps = dispatch => {
  return {
    verifyEmailAddress: payload => {
      dispatch(validateEmail(payload));
    },
    submitEmailSubscription: (payload, status) => {
      dispatch(submitEmailSignup(payload, status));
    },
    clearEmailSignupForm: () => {
      dispatch(clearEmailSignupForm());
    },
    closeModal: () => {
      dispatch(toggleEmailSignupModal({ isModalOpen: false }));
    },
    validateSignupEmail: email => {
      return emailSignupAbstractor.verifyEmail(email);
    },
    trackSubscriptionSuccess: payload => {
      let storeId = '';
      if (payload) {
        const {
          basicInfo: { id = '' },
        } = payload;
        storeId = id;
      }
      dispatch(
        setClickAnalyticsData({
          customEvents: ['event15', 'event80'],
          pageName: 'content:email confirmation',
          pageShortName: 'content:email confirmation',
          pageSection: 'content',
          pageSubSection: 'content',
          pageType: 'content',
          pageTertiarySection: 'content',
          virtualPageView: true,
          storeId,
        })
      );

      dispatch(trackPageView({}));
      const timer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(timer);
      }, 200);
    },
  };
};

const mapStateToProps = (state, props) => {
  let formViewConfig = {};
  if (
    props.buttonConfig &&
    props.buttonConfig.link &&
    props.buttonConfig.link.action === 'open_signup_modal' &&
    state.Labels &&
    state.Labels.global
  ) {
    formViewConfig = {
      ...state.Labels.global.emailSignup,
    };
  }
  const { EmailSignupFormReducer: { subscription, isEmailValid } = {} } = state.EmailSignUp;
  const richText = get(props, 'buttonConfig.richText.text', '');
  const emailDisclaimer = { richText };
  return {
    formViewConfig,
    emailDisclaimer,
    subscription,
    isEmailValid,
    defaultStore: getDefaultStore(state),
    ...props,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupModalView);
