// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import EmailSmsSignUpForm from '@tcp/core/src/components/common/organisms/EmailSmsSignUpForm/views/EmailSmsSignUpForm.view';
import { Modal } from '@tcp/core/src/components/common/molecules';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles from '../styles/EmailSmsSignUp.style';

class SingleEmailSmsSignUp extends React.PureComponent {
  componentDidUpdate({ subscription: oldSubscription }) {
    const { subscription } = this.props;

    if (
      subscription.success !== oldSubscription.success &&
      subscription.success &&
      this.modalContentRef
    ) {
      this.modalContentRef.focus();
    }
  }

  setModalContentRef = (node) => {
    this.modalContentRef = node;
  };

  closeModal = () => {
    const { closeModal, clearEmailSmsSignupForm, reset } = this.props;
    closeModal();
    reset();
    clearEmailSmsSignupForm();
  };

  render() {
    const { isModalOpen, className, subscription } = this.props;
    return (
      <Fragment>
        {isModalOpen && (
          <Modal
            contentRef={this.setModalContentRef}
            isOpen={isModalOpen}
            colSet={{ small: 6, medium: 6, large: 8 }}
            className={`${className} email_sms-signup-modal`}
            overlayClassName="TCPModal__Overlay email_sms-signup-modal"
            onRequestClose={this.closeModal}
            noPadding
            widthConfig={{ small: '100%', medium: '458px', large: '851px' }}
            heightConfig={{ minHeight: '500px', height: '645px', maxHeight: '645px' }}
            closeIconDataLocator={
              subscription.success
                ? 'thank_you_modal_close_btn'
                : 'email_sms_signup_modal_close_btn'
            }
            aria={{
              describedby: subscription.success
                ? 'sign-up-modal-confirm-view'
                : 'sign-up-modal-form-intro-view',
            }}
          >
            <EmailSmsSignUpForm formType="email_sms_signup" {...this.props} />
          </Modal>
        )}
      </Fragment>
    );
  }
}

SingleEmailSmsSignUp.propTypes = {
  className: PropTypes.string,
  clearEmailSmsSignupForm: PropTypes.func,
  subscription: PropTypes.objectOf(PropTypes.shape({})),
  isModalOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  reset: PropTypes.func,
};

SingleEmailSmsSignUp.defaultProps = {
  className: '',
  subscription: {},
  isModalOpen: false,
  clearEmailSmsSignupForm: () => {},
  closeModal: () => {},
  reset: () => {},
};

export default withStyles(SingleEmailSmsSignUp, styles);
export { SingleEmailSmsSignUp as SingleEmailSmsSignUpVanilla };
