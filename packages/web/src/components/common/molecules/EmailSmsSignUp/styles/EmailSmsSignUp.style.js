import { css } from 'styled-components';

export default css`
  .email_sms-signup-modal {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      .TCPModal__InnerContent {
        transform: none;
        top: initial;
        position: absolute;
        left: 0px;
        bottom: 0px;
        display: flex;
        align-items: flex-end;
        height: auto;
      }
    }
  }
`;
