import React from 'react';
import { shallow } from 'enzyme';
import { SingleEmailSmsSignUpVanilla } from '../views/EmailSmsSignUp.view';

describe('SingleSignup Modal', () => {
  it('should renders correctly when subscription is success and modal is open', () => {
    const props = {
      subscription: { success: true },
      isModalOpen: true,
      className: 'classname',
    };
    const component = shallow(<SingleEmailSmsSignUpVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when modal is closed', () => {
    const props = {
      subscription: { success: true },
      isModalOpen: false,
      className: 'classname',
    };
    const component = shallow(<SingleEmailSmsSignUpVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when error in subscription', () => {
    const props = {
      subscription: { error: 'invalid' },
      isModalOpen: true,
      className: 'classname',
    };
    const component = shallow(<SingleEmailSmsSignUpVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when modal close icon is clicked', () => {
    const props = {
      subscription: { success: true },
      isModalOpen: true,
      className: 'classname',
      closeModal: jest.fn(),
      clearEmailSmsSignupForm: jest.fn(),
      onModalClose: jest.fn(),
    };
    const component = shallow(<SingleEmailSmsSignUpVanilla {...props} />);
    component.instance().setModalContentRef({ focus: () => {} });
    component.instance().componentDidUpdate({ subscription: { success: false } });
    component.instance().closeModal();
    expect(props.closeModal).toHaveBeenCalled();
    expect(props.clearEmailSmsSignupForm).toHaveBeenCalled();
    expect(component).toMatchSnapshot();
  });
});
