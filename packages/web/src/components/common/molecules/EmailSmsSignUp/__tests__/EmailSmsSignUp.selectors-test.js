import { getSingleSignUpLabels } from '../container/EmailSmsSignUp.selectors';

describe('EmailSmsSignUp selector', () => {
  it('getSingleSignUpLabels should return labels correctly', () => {
    const initialState = {
      global: {
        smsSignup: {
          lbl_combined_signup: 'lbl_combined_signup',
          lbl_combined_noThanks: 'lbl_combined_noThanks',
          lbl_combined_signup_image_url: 'lbl_combined_signup_image_url',
          lbl_combined_email_placeholder: 'lbl_combined_email_placeholder',
          lbl_combined_sms_placeholder: 'lbl_combined_sms_placeholder',
          lbl_combined_legal_text: 'lbl_combined_legal_text',
          lbl_combined_signup_heading: 'lbl_combined_signup_heading',
          lbl_combined_signup_get_text: 'lbl_combined_signup_get_text',
          lbl_combined_signup_dollar_text: 'lbl_combined_signup_dollar_text',
          lbl_combined_signup_ten_text: 'lbl_combined_signup_ten_text',
          lbl_combined_signup_off_text: 'lbl_combined_signup_off_text',
          lbl_combined_success_heading1: 'lbl_combined_success_heading1',
          lbl_combined_success_heading2: 'lbl_combined_success_heading2',
          lbl_combined_success_message: 'lbl_combined_success_message',
          lbl_combined_success_image_url: 'lbl_combined_success_image_url',
          lbl_combined_shop_now: 'lbl_combined_shop_now',
          lbl_combined_cross_brand_check: 'lbl_combined_cross_brand_check',
          lbl_combined_validation_error: 'lbl_combined_validation_error',
          lbl_combined_shop_now_url: 'lbl_combined_shop_now_url',
        },
      },
    };
    expect(getSingleSignUpLabels(initialState)).toMatchObject({
      signUp: 'lbl_combined_signup',
      noThanks: 'lbl_combined_noThanks',
      signUpImageUrl: 'lbl_combined_signup_image_url',
      emailPlaceHolder: 'lbl_combined_email_placeholder',
      smsPlaceHolder: 'lbl_combined_sms_placeholder',
      legalText: 'lbl_combined_legal_text',
      signUpHeading: 'lbl_combined_signup_heading',
      signUpGetText: 'lbl_combined_signup_get_text',
      signUpDollarText: 'lbl_combined_signup_dollar_text',
      signUpTenText: 'lbl_combined_signup_ten_text',
      signUpOffText: 'lbl_combined_signup_off_text',
      successHeading1: 'lbl_combined_success_heading1',
      successHeading2: 'lbl_combined_success_heading2',
      successMessage: 'lbl_combined_success_message',
      successImageUrl: 'lbl_combined_success_image_url',
      shopNow: 'lbl_combined_shop_now',
      crossBrandEmail: 'lbl_combined_cross_brand_check',
      emailVaidationError: 'lbl_combined_validation_error',
      shopNowButtonUrl: 'lbl_combined_shop_now_url',
    });
  });
});
