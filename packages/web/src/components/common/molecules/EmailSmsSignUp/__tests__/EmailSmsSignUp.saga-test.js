// 9fbef606107a605d69c0edbcd8029e5d
import { put } from 'redux-saga/effects';
import { subscribeEmailSms } from '../container/EmailSmsSignUp.saga';
import { setEmailSmsStatus } from '../container/EmailSmsSignUp.actions';

describe('Email Sms signup', () => {
  it('should dispatch subscribeEmailSms action for email', () => {
    const payload = {
      signupEmailAddress: 'abc@gmail.com',
    };
    const subscribeSmsGen = subscribeEmailSms({ payload });
    subscribeSmsGen.next();
    const putDescriptor = subscribeSmsGen.next(true).value;
    expect(putDescriptor).toEqual(put(setEmailSmsStatus({ subscription: true })));
  });
  it('should dispatch subscribeEmailSms action for phone', () => {
    const payload = {
      signupPhoneNumber: '8976546786',
    };
    const subscribeSmsGen = subscribeEmailSms({ payload });
    subscribeSmsGen.next();
    const putDescriptor = subscribeSmsGen.next(true).value;
    expect(putDescriptor).toEqual(put(setEmailSmsStatus({ subscription: true })));
  });
  it('should dispatch subscribeEmailSms action for email and phone', () => {
    const payload = {
      signupEmailAddress: 'abc@gmail.com',
      signupPhoneNumber: '8976546786',
      emailSignupSecondBrand: true,
    };
    const subscribeSmsGen = subscribeEmailSms({ payload });
    subscribeSmsGen.next();
    const putDescriptor = subscribeSmsGen.next(true).value;
    expect(putDescriptor).toEqual(put(setEmailSmsStatus({ subscription: true })));
  });
});
