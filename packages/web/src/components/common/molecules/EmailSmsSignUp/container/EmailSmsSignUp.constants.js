export default {
  EMAIL_REGISTRATION_SMS_CONSTANT: '25',
  EMAIL_REGISTRATION_EMAIL_CONSTANT: '26',
  EMAIL_SMS_SUBSCRIPTION_SUBMIT: 'EMAIL_SMS_SUBSCRIPTION_SUBMIT',
  EMAIL_SMS_MODAL_TOGGLE: 'EMAIL_SMS_MODAL_TOGGLE',
  CLEAR_SUBSCRIPTION_FORM: 'CLEAR_SUBSCRIPTION_FORM',
  EMAIL_SMS_FORM_SUBMIT: 'EMAIL_SMS_FORM_SUBMIT',
  EMAIL_SUBSCRIPTION_STATUS: 'EMAIL_SUBSCRIPTION_STATUS',
};
