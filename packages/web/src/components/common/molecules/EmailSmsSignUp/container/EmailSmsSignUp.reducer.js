import EMAIL_SMS_SIGNUP_CONSTANTS from './EmailSmsSignUp.constants';

const initialState = {};

const EmailSmsSignupReducer = (state = initialState, action) => {
  switch (action.type) {
    case EMAIL_SMS_SIGNUP_CONSTANTS.EMAIL_SUBSCRIPTION_STATUS:
      return { ...state, ...action.payload };
    case EMAIL_SMS_SIGNUP_CONSTANTS.EMAIL_SMS_MODAL_TOGGLE:
      return { ...state, ...action.payload };
    case EMAIL_SMS_SIGNUP_CONSTANTS.CLEAR_SUBSCRIPTION_FORM:
      return initialState;
    default:
      return state;
  }
};

export default EmailSmsSignupReducer;
