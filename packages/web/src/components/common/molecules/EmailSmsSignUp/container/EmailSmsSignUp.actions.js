import EMAIL_SMS_SIGNUP_CONSTANTS from './EmailSmsSignUp.constants';

export const submitEmailSmsSignUp = (payload) => {
  return {
    payload,
    type: EMAIL_SMS_SIGNUP_CONSTANTS.EMAIL_SMS_SUBSCRIPTION_SUBMIT,
  };
};

export const setEmailSmsStatus = (payload) => {
  return {
    payload,
    type: EMAIL_SMS_SIGNUP_CONSTANTS.EMAIL_SUBSCRIPTION_STATUS,
  };
};

export const toggleEmailSmsSignupModal = (payload) => {
  return {
    payload,
    type: EMAIL_SMS_SIGNUP_CONSTANTS.EMAIL_SMS_MODAL_TOGGLE,
  };
};

export const clearEmailSmsSignupForm = (payload) => {
  return {
    payload,
    type: EMAIL_SMS_SIGNUP_CONSTANTS.CLEAR_SUBSCRIPTION_FORM,
  };
};
