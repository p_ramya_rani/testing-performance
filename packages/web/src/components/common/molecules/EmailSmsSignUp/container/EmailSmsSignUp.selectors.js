import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';

const getLabels = (state) => state.Labels;

export const getSingleSignUpLabels = createSelector(getLabels, (labels) => {
  return {
    signUp: getLabelValue(labels, 'lbl_combined_signup', 'smsSignup', 'global'),
    noThanks: getLabelValue(labels, 'lbl_combined_noThanks', 'smsSignup', 'global'),
    signUpImageUrl: getLabelValue(labels, 'lbl_combined_signup_image_url', 'smsSignup', 'global'),
    emailPlaceHolder: getLabelValue(
      labels,
      'lbl_combined_email_placeholder',
      'smsSignup',
      'global'
    ),
    smsPlaceHolder: getLabelValue(labels, 'lbl_combined_sms_placeholder', 'smsSignup', 'global'),
    legalText: getLabelValue(labels, 'lbl_combined_legal_text', 'smsSignup', 'global'),
    signUpHeading: getLabelValue(labels, 'lbl_combined_signup_heading', 'smsSignup', 'global'),
    signUpGetText: getLabelValue(labels, 'lbl_combined_signup_get_text', 'smsSignup', 'global'),
    signUpDollarText: getLabelValue(
      labels,
      'lbl_combined_signup_dollar_text',
      'smsSignup',
      'global'
    ),
    signUpTenText: getLabelValue(labels, 'lbl_combined_signup_ten_text', 'smsSignup', 'global'),
    signUpOffText: getLabelValue(labels, 'lbl_combined_signup_off_text', 'smsSignup', 'global'),
    successHeading1: getLabelValue(labels, 'lbl_combined_success_heading1', 'smsSignup', 'global'),
    successHeading2: getLabelValue(labels, 'lbl_combined_success_heading2', 'smsSignup', 'global'),
    successMessage: getLabelValue(labels, 'lbl_combined_success_message', 'smsSignup', 'global'),
    successImageUrl: getLabelValue(labels, 'lbl_combined_success_image_url', 'smsSignup', 'global'),
    shopNow: getLabelValue(labels, 'lbl_combined_shop_now', 'smsSignup', 'global'),
    crossBrandEmail: getLabelValue(labels, 'lbl_combined_cross_brand_check', 'smsSignup', 'global'),
    emailVaidationError: getLabelValue(
      labels,
      'lbl_combined_validation_error',
      'smsSignup',
      'global'
    ),
    shopNowButtonUrl: getLabelValue(labels, 'lbl_combined_shop_now_url', 'smsSignup', 'global'),
  };
});

export const getSubscription = (state) => {
  const { subscription } = state.EmailSmsSignUpReducer;
  return subscription;
};
