// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import {
  clearEmailSmsSignupForm,
  toggleEmailSmsSignupModal,
  submitEmailSmsSignUp,
} from './EmailSmsSignUp.actions';
import EmailSmsSignUp from '../views/EmailSmsSignUp.view';
import { getSingleSignUpLabels, getSubscription } from './EmailSmsSignUp.selectors';

export const mapDispatchToProps = (dispatch) => {
  return {
    submitEmailSmsSubscription: (payload) => {
      dispatch(submitEmailSmsSignUp(payload));
    },
    clearEmailSignupForm: () => {
      dispatch(clearEmailSmsSignupForm());
    },
    closeModal: () => {
      dispatch(toggleEmailSmsSignupModal({ isEmailSmsModalOpen: false }));
    },
    validateSignupEmail: (email) => {
      return emailSignupAbstractor.verifyEmail(email);
    },
  };
};

const mapStateToProps = (state, props) => {
  return {
    singleSignUpLabels: getSingleSignUpLabels(state),
    subscription: getSubscription(state),
    ...props,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmailSmsSignUp);
