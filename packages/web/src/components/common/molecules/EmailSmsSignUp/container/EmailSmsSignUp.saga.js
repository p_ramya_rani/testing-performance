import { call, put, takeLatest } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { isGymboree } from '@tcp/core/src/utils/utils';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import EMAIL_SMS_SIGNUP_CONSTANTS from './EmailSmsSignUp.constants';
import { setEmailSmsStatus } from './EmailSmsSignUp.actions';

export function* subscribeEmailSms({ payload }) {
  try {
    const { signupEmailAddress, signupPhoneNumber, emailSignupSecondBrand, status } = payload;
    const isGym = isGymboree();
    const brandGYM = !!(isGym || emailSignupSecondBrand);
    const brandTCP = !!(!isGym || emailSignupSecondBrand);
    const reqPayload = {};
    let subscriptionBrands = brandTCP ? 'TCP' : '';
    if (brandGYM) {
      subscriptionBrands = subscriptionBrands ? `${subscriptionBrands},GYM` : 'GYM';
    }
    reqPayload.subscriptionBrands = subscriptionBrands;

    if (signupEmailAddress) {
      reqPayload.emailReqObj = {
        emailaddr: signupEmailAddress,
        URL: 'email-confirmation',
        response: `${status}:::false:false`,
        brandTCP,
        brandGYM,
        registrationType: signupPhoneNumber
          ? EMAIL_SMS_SIGNUP_CONSTANTS.EMAIL_REGISTRATION_SMS_CONSTANT
          : EMAIL_SMS_SIGNUP_CONSTANTS.EMAIL_REGISTRATION_EMAIL_CONSTANT,
      };
    }

    if (signupPhoneNumber) {
      reqPayload.smsReqObj = { footerTopSmsSignup: signupPhoneNumber };
    }

    const res = yield call(emailSignupAbstractor.emailSmsSignUp, reqPayload);
    yield put(setEmailSmsStatus({ subscription: res }));
  } catch (err) {
    logger.error(err);
  }
}

function* EmailSmsSignupSaga() {
  yield takeLatest(EMAIL_SMS_SIGNUP_CONSTANTS.EMAIL_SMS_SUBSCRIPTION_SUBMIT, subscribeEmailSms);
}

export default EmailSmsSignupSaga;
