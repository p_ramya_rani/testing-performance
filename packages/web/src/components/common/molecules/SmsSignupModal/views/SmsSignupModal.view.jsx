// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import SmsSignupForm from '@tcp/core/src/components/common/organisms/SmsSignupForm/views';
import { Modal } from '@tcp/core/src/components/common/molecules';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles from '../SmsSignupModal.style';

export const DynamicForm = (properties) => {
  return <SmsSignupForm {...properties} />;
};

class SmsSignupModal extends React.PureComponent {
  componentDidUpdate({ subscription: oldSubscription }) {
    const { subscription } = this.props;

    if (
      subscription.success !== oldSubscription.success &&
      subscription.success &&
      this.modalContentRef
    ) {
      this.modalContentRef.focus();
    }
  }

  setModalContentRef = (node) => {
    this.modalContentRef = node;
  };

  closeModal = () => {
    const { closeModal, clearSmsSignupForm, reset } = this.props;
    closeModal();
    reset();
    clearSmsSignupForm();
  };

  render() {
    const { isModalOpen, className, formViewConfig, subscription } = this.props;
    return (
      <Fragment>
        {isModalOpen && (
          <Modal
            contentRef={this.setModalContentRef}
            isOpen={isModalOpen}
            colSet={{ small: 6, medium: 6, large: 8 }}
            className={`${className} sms-signup-modal`}
            overlayClassName="TCPModal__Overlay sms-signup-modal"
            onRequestClose={this.closeModal}
            noPadding
            widthConfig={{ small: '100%', medium: '458px', large: '851px' }}
            heightConfig={{ minHeight: '500px', height: '645px', maxHeight: '645px' }}
            closeIconDataLocator={
              subscription.success ? 'thank_you_modal_close_btn' : 'sms_signup_modal_close_btn'
            }
            contentLabel={`${formViewConfig.lbl_SignUp_signUpForLabel} ${formViewConfig.lbl_SignUp_offerTypeLabel}`}
            aria={{
              describedby: subscription.success
                ? 'sign-up-modal-confirm-view'
                : 'sign-up-modal-form-intro-view',
            }}
          >
            <DynamicForm formType="sms_signup" smsFromPage="overlay" {...this.props} />
          </Modal>
        )}
      </Fragment>
    );
  }
}

SmsSignupModal.propTypes = {
  className: PropTypes.string,
  formViewConfig: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  confirmationViewConfig: PropTypes.shape({}).isRequired,
  clearSmsSignupForm: PropTypes.func,
  subscription: PropTypes.objectOf(PropTypes.shape({})),
  isModalOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  reset: PropTypes.func,
};

SmsSignupModal.defaultProps = {
  className: '',
  subscription: {},
  isModalOpen: false,
  clearSmsSignupForm: () => {},
  closeModal: () => {},
  reset: () => {},
};

export default withStyles(SmsSignupModal, styles);
export { SmsSignupModal as SmsSignupModalVanilla };
