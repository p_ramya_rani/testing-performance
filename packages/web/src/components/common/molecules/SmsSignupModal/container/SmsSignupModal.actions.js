// 9fbef606107a605d69c0edbcd8029e5d 
import SMS_SIGNUP_CONSTANTS from './SmsSignupModal.constants';

export const toggleSmsSignupModal = payload => {
  return {
    payload,
    type: SMS_SIGNUP_CONSTANTS.SMS_SUBSCRIPTION_MODAL_TOGGLE,
  };
};

export default {
  toggleSmsSignupModal,
};
