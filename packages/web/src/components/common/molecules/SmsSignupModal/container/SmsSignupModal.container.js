// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { validatePhoneNumber } from '@tcp/core/src/utils/formValidation/phoneNumber';
import { getLabelValue } from '@tcp/core/src/utils';
import { trackPageView, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import {
  submitSmsSignup,
  clearSmsSignupForm,
} from '@tcp/core/src/components/common/organisms/SmsSignupForm/container/SmsSignupForm.actions';
import get from 'lodash/get';
import { toggleSmsSignupModal } from './SmsSignupModal.actions';
import SignupModalView from '../views/SmsSignupModal.view';
import { getDefaultStore } from '../../../../../../../core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';

export const mapDispatchToProps = (dispatch) => {
  return {
    submitSmsSubscription: (payload) => {
      dispatch(submitSmsSignup(payload));
    },
    clearSmsSignupForm: () => {
      dispatch(clearSmsSignupForm());
    },
    closeModal: () => {
      dispatch(toggleSmsSignupModal({ isModalOpen: false }));
    },
    validateSignupSmsPhoneNumber: (phoneNumber) => {
      return validatePhoneNumber(phoneNumber) ? Promise.resolve({}) : Promise.reject();
    },

    trackSubscriptionSuccess: (payload) => {
      let storeId = '';
      if (payload) {
        const {
          basicInfo: { id = '' },
        } = payload;
        storeId = id;
      }
      dispatch(
        setClickAnalyticsData({
          customEvents: ['event107', 'event80'],
          pageName: 'content:sms confirmation',
          pageShortName: 'content:sms confirmation',
          pageSection: 'content',
          pageSubSection: 'content',
          pageType: 'content',
          pageTertiarySection: 'content',
          storeId,
        })
      );

      dispatch(trackPageView({}));
      const timer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(timer);
      }, 200);
    },
  };
};

const mapStateToProps = (state, props) => {
  let formViewConfig = {};
  if (
    props.buttonConfig &&
    props.buttonConfig.link &&
    props.buttonConfig.link.action === 'open_sms_modal' &&
    state.Labels &&
    state.Labels.global
  ) {
    formViewConfig = {
      ...state.Labels.global.smsSignup,
      noThanksLabel: getLabelValue(state.Labels, 'lbl_SignUp_noThanks', 'emailSignup', 'global'),
    };
  }

  const { SmsSignupFormReducer: { subscription } = {} } = state.SmsSignUp;
  const richText = get(props, 'buttonConfig.richText.text', '');
  const smsDisclaimer = {
    richText,
  };
  return {
    formViewConfig,
    smsDisclaimer,
    subscription,
    defaultStore: getDefaultStore(state),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignupModalView);
