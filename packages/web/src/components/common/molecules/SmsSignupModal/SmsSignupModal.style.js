// 9fbef606107a605d69c0edbcd8029e5d 
// stylelint-disable selector-max-specificity

// we are donig stylelint-disable selector-max-specificity because
// email-signup-modal and .sms-signup-modal have 70% identical CSS on line
import { css } from 'styled-components';

const SignupModalStyle = css`
  .email-signup-modal,
  .sms-signup-modal {
    &.TCPModal__InnerContent {
      overflow-x: hidden;
    }
    .alignRight {
      @media ${props => props.theme.mediaQuery.medium} {
        right: ${props => props.theme.spacing.ELEM_SPACING.MED};
      }
    }
    .alignTop {
      top: ${props => props.theme.spacing.ELEM_SPACING.MED};
      z-index: 1;
    }

    @media ${props => props.theme.mediaQuery.smallOnly} {
      .logo-wrapper,
      .sign-up__label {
        display: none;
      }

      .offer-type__label {
        margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXL};
        font-size: ${props => props.theme.typography.fontSizes.fs18};
      }

      .offer-type__label:after {
        margin: ${props => props.theme.spacing.ELEM_SPACING.SM} auto
          ${props => props.theme.spacing.ELEM_SPACING.MED};
      }

      .first-text,
      .know-text {
        font-size: ${props => props.theme.typography.fontSizes.fs36};
        line-height: normal;
      }
      .off-text {
        line-height: 1.7;
      }

      .desc-text,
      .desc-text {
        line-height: normal;
      }

      .field-container {
        padding-top: 0;
      }

      .button-wrapper-form {
        position: static;
        background: none;
        margin: 0;
      }
      .close-modal.alignTop.alignRight {
        position: absolute;
      }
      .TCPModal__InnerContent {
        transform: none;
        top: initial;
        position: absolute;
        left: 0px;
        bottom: 0px;
        display: flex;
        align-items: flex-end;
        height: auto;
      }
    }
    @media ${props => props.theme.mediaQuery.smallMax} {
      div#sign-up-modal-confirm-view {
        margin-top: ${props => props.theme.spacing.MODULE_SPACING.MED};
      }
      .alignRight {
        right: 14px;
      }
      .alignTop {
        top: 14px;
      }

      p.first-label {
        font-size: ${props => props.theme.typography.fontSizes.fs16};
        width: 80%;
        margin: 10px auto;
      }
      .disclaimer1 {
        font-size: 9px;
      }
      p.confirmation-label::after {
        margin-bottom: 10px;
      }
      br,
      .redeem-label {
        display: none !important;
      }
      p.thank-you__label {
        font-size: ${props => props.theme.typography.fontSizes.fs18};
      }
      .confirmation-label {
        font-size: ${props => props.theme.typography.fontSizes.fs14};
      }
      span.get-text,
      span.ten-text,
      p.off-text {
        font-size: ${props => props.theme.typography.fontSizes.fs42};
      }
      p.desc-text {
        font-size: ${props => props.theme.typography.fontSizes.fs12};
      }
      button.join-button {
        min-height: 33px;
      }

      .flash-text,
      .get-text,
      .dollar-text,
      .ten-text,
      .off-text {
        display: inline;
      }
      section#sign-up-modal-form-intro-view {
        text-align: center;
      }

      .off-text {
        margin-left: 5px;
      }
      p.offer-type__label::after {
        margin: 3px auto ${props => props.theme.spacing.ELEM_SPACING.XS_6};
      }

      p.offer-type__label {
        font-size: 15px;
        margin-top: 20px;
      }
    }
  }

  @media ${props => props.theme.mediaQuery.mediumMax} {
    .form_container .form-wrapper {
      min-height: 100%;
    }
    .form_container .button-wrapper-form {
      width: auto;
    }
  }

  @media ${props => props.theme.mediaQuery.smallMax} {
    .header-promo__container.top-promo-banner.content-wrapper {
      transform: scale(0.9);
      margin: 0px;
    }
    .plcctakeover {
      margin: 20px auto 0px auto;
    }
    .form_container {
      height: 430px;
    }
    .plcc-takeover .flash-text {
      display: block;
      margin-top: ${props => props.theme.spacing.MODULE_SPACING.MED};
    }
    .email-signup-modal {
      .email-signup-second-brand {
        padding-top: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
      }
      &.plcc-takeover p.plcc-offer-type__label {
        font-size: ${props => props.theme.typography.fontSizes.fs18};
        margin-top: 38px;
      }

      &.plcc-takeover p.desc-text {
        font-size: ${props => props.theme.typography.fontSizes.fs16};
        margin: ${props => props.theme.spacing.ELEM_SPACING.MED} auto
          ${props => props.theme.spacing.ELEM_SPACING.LRG};
      }
      &.plcc-takeover p.plcc-offer-type__label::after {
        margin: ${props => props.theme.spacing.ELEM_SPACING.SM} auto
          ${props => props.theme.spacing.ELEM_SPACING.XS_6};
      }
      .TCPModal__InnerContent {
        padding-bottom: 50px;
      }
      img.confirmation-image {
        width: 45px;
        height: 45px;
      }
      .button-wrapper {
        margin-bottom: 22%;
        margin-top: 0px;
      }
      .logo-wrapper {
        display: none !important;
      }
    }

    .sms-signup-modal {
      .sms-signup-modal div#sign-up-modal-confirm-view {
        margin-top: 30px;
      }
      .off-text {
        margin-top: -30px;
        line-height: 1.4;
      }

      .button-wrapper {
        margin-bottom: 2%;
        margin-top: 0px;
      }
      span.get-text,
      span.ten-text,
      p.off-text {
        font-size: ${props => props.theme.typography.fontSizes.fs42};
      }
      .flash-text {
        margin-top: -14px;
      }
      .termscontainer {
        height: 29px;
        overflow: auto;
      }

      .field-container {
        padding-top: 0px;
      }
    }

    #special-offer {
      text-align: center;
      width: 120px;
      height: 100%;
      margin: 0 auto;
    }
    #save-offer {
      font-size: 40px;
      margin: 10px auto;
    }
    #ecomUl {
      margin: 19px auto;
    }

    #plcc-message {
      font-size: 16px;
    }
    #ecomApply {
      display: block;
    }
  }

  @media ${props => props.theme.mediaQuery.small} {
    .sms-signup-modal {
      .button-wrapper {
        margin-bottom: 2%;
        margin-top: 0px;
      }
    }
  }
`;

export default SignupModalStyle;
