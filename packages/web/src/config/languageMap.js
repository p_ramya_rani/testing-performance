// 9fbef606107a605d69c0edbcd8029e5d 
/**
 * Created this map as in config we get only en, es, fr
 * but for seo lang attribute should contain en, es-US, fr-CA.
 */

export default {
  en: 'en',
  es: 'es-US',
  fr: 'fr-CA',
};
