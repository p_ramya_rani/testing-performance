// 9fbef606107a605d69c0edbcd8029e5d 
export default [
  'https://assets.theplace.com',
  'https://refer.childrensplace.com',
  'https://assets.adobedtm.com',
  'https://search.unbxd.io',
  'https://cdn.quantummetric.com',
  'https://tcp-sync.quantummetric.com',
  'https://origin.xtlo.net',
  'https://s.go-mpulse.net',
  'https://universal.iperceptions.com',
  'https://tagtracking.vibescm.com',
  'https://widget-api.stylitics.com',
  'https://dpm.demdex.net',
];
