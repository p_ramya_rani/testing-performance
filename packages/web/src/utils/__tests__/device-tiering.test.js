import setDeviceTier, { getDeviceTier } from '../device-tiering';

describe('getDeviceTier', () => {
  it('should return tier2 device', () => {
    global.window.deviceTier = 'tier2';
    expect(getDeviceTier()).toBe('tier2');
  });
  it('should return tier1 device', () => {
    global.window.deviceTier = 'tier1';
    expect(getDeviceTier()).toBe('tier1');
  });
  it('should return tier1 when deviceTier is not set', () => {
    expect(getDeviceTier()).toBe('tier1');
  });
});
describe.skip('setDeviceTier', () => {
  let navigatorGetter;
  beforeEach(() => {
    navigatorGetter = jest.spyOn(window, 'navigator', 'get');
  });

  it('should return tier3 device when network is 2g', () => {
    const mockValues = {
      connection: { effectiveType: '2g' },
      deviceMemory: '6',
    };
    navigatorGetter.mockReturnValue(mockValues);
    expect(setDeviceTier()).toBe('tier3');
  });
  it('should return tier3 device when network is 3g', () => {
    const mockValues = {
      connection: { effectiveType: '3g' },
      deviceMemory: '2',
    };
    navigatorGetter.mockReturnValue(mockValues);
    expect(setDeviceTier()).toBe('tier3');
  });
  it('should return tier2 device when network is 3g', () => {
    const mockValues = {
      connection: { effectiveType: '3g' },
      deviceMemory: '6',
    };
    navigatorGetter.mockReturnValue(mockValues);
    expect(setDeviceTier()).toBe('tier2');
  });
  it('should return tier3 device when network is 4g', () => {
    const mockValues = {
      connection: { effectiveType: '4g' },
      deviceMemory: '2',
    };
    navigatorGetter.mockReturnValue(mockValues);
    expect(setDeviceTier()).toBe('tier3');
  });
  it('should return tier2 device when network is 4g', () => {
    const mockValues = {
      connection: { effectiveType: '4g' },
      deviceMemory: '5',
    };
    navigatorGetter.mockReturnValue(mockValues);
    expect(setDeviceTier()).toBe('tier2');
  });
  it('should return tier1 device when network is 4g', () => {
    const mockValues = {
      connection: { effectiveType: '4g' },
      deviceMemory: '8',
    };
    navigatorGetter.mockReturnValue(mockValues);
    expect(setDeviceTier()).toBe('tier1');
  });
  afterEach(() => {
    navigatorGetter.mockRestore();
  });
});
