import { isAppleDevice, isSafariBrowser } from '@tcp/core/src/utils';
import {
  TIER1,
  TIER2,
  TIER3,
  TIER2NETWORKSPEED,
  TIER3NETWORKSPEED,
} from '@tcp/core/src/constants/tiering.constants';

const { effectiveType = '4g' } =
  typeof navigator !== 'undefined' && 'connection' in navigator ? navigator.connection : {};
const deviceMemory = typeof navigator !== 'undefined' ? navigator.deviceMemory : 8;

export function getiPhoneModelTier() {
  const deviceWidth = window.screen.width;
  const deviceHeight = window.screen.height;
  const { devicePixelRatio } = window;
  const screenResolution = `${deviceWidth}x${deviceHeight}`;

  let deviceType;

  if (screenResolution === '320x568' && devicePixelRatio <= 2) {
    // iPhone 4/4s 5/SE
    deviceType = TIER3;
  } else if (screenResolution === '414x736' || screenResolution === '375x667') {
    // iPhone 6+/6s+/7+ and 8+
    deviceType = TIER2;
  } else {
    // iPhone X, latest model
    deviceType = TIER1;
  }

  return deviceType;
}

function forNonIOS() {
  let deviceType;
  if (TIER3NETWORKSPEED.includes(effectiveType)) {
    deviceType = TIER3;
  } else if (TIER2NETWORKSPEED.includes(effectiveType)) {
    if (deviceMemory < 4) {
      deviceType = TIER3;
    } else {
      deviceType = TIER2;
    }
  } else if (deviceMemory < 4) {
    deviceType = TIER3;
  } else if (deviceMemory < 6) {
    deviceType = TIER2;
  } else {
    deviceType = TIER1;
  }
  return deviceType;
}
function forIOS() {
  let deviceType;
  if (TIER3NETWORKSPEED.includes(effectiveType)) {
    deviceType = TIER3;
  } else if (TIER2NETWORKSPEED.includes(effectiveType)) {
    if (deviceMemory < 4) {
      deviceType = TIER3;
    } else {
      deviceType = TIER2;
    }
  } else if (deviceMemory < 4) {
    deviceType = TIER2;
  } else {
    deviceType = TIER1;
  }
  return deviceType;
}

export default function setDeviceTier() {
  if (isSafariBrowser()) {
    return getiPhoneModelTier();
  }
  if (isAppleDevice()) {
    return forIOS();
  }
  return forNonIOS();
}

export function getDeviceTier() {
  return (typeof window !== 'undefined' && window.deviceTier) || TIER1;
}

export function isTier1Device() {
  return getDeviceTier() === TIER1;
}
export function isTier2Device() {
  return getDeviceTier() === TIER2;
}
export function isTier3Device() {
  return getDeviceTier() === TIER3;
}
