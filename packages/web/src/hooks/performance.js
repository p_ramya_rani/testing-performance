// 9fbef606107a605d69c0edbcd8029e5d 
import { useEffect, useDebugValue } from 'react';
import logger from '@tcp/core/src/utils/loggerInstance';
import { TIMER_NAMES } from '@tcp/core/src/constants/rum.constants';

const isEnabled = Boolean(process.env.PERF_TIMING);
const isSupported = typeof performance !== 'undefined';
const shouldRun = isEnabled && isSupported;

export function usePerfMeasure(name, start, end) {
  useDebugValue(name);
  useEffect(() => {
    const measureExists = performance.getEntriesByName(name).length;
    // Stop early if needed. This check must be within useEffect.
    if (!shouldRun || measureExists) return undefined;
    try {
      const allMarkers = performance
        .getEntriesByType('mark')
        .filter(entry => TIMER_NAMES.indexOf(entry.name) > -1);
      if (allMarkers && allMarkers.length) {
        performance.measure(name, start, end);
      }
    } catch (e) {
      logger.error(e);
      // This throws if "start" or "end" don't match existing entries.
    }
    // Clear measures when component un-mounts.
    return () => performance.clearMeasures(name);
  }, [name, start, end]);
}

export function usePerfMark(name) {
  useDebugValue(name);
  useEffect(() => {
    // Stop early if needed. This check must be within useEffect.
    if (!shouldRun) return undefined;
    // First clear existing entries that may be from SSR.
    performance.clearMarks(name);
    performance.mark(name);
    // Clear marks when component un-mounts.
    return () => performance.clearMarks(name);
  }, [name]);
}
