/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { readCookie, setCookie } from '@tcp/core/src/utils/cookie.util';
import { API_CONFIG } from '@tcp/core/src/services/config';
import { getSessionStorage } from '@tcp/core/src/utils';
import { dataLayer as defaultDataLayer } from '@tcp/core/src/analytics';
import { getPageNameValue } from '@tcp/core/src/analytics/utils';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {
  generateBrowseDataLayer,
  generateHomePageDataLayer,
  generateClickHandlerDataLayer,
} from './dataLayers';

import { getSetSessionStorageObj } from './dataLayers/browse.dataLayer';

const getCartCheckoutPageType = (pageType) => {
  return pageType === 'checkout' || pageType === 'shopping bag';
};

/**
 * Analytics data layer object for property lookups.
 *
 * This function will create an object for the analytics
 * script to reference when mapping data elements. This
 * object should be assigned to the global context
 * (e.g., window) for easy access.
 *
 * @example
 * // In the app source
 * global._dataLater = create(store);
 * // In the analytics script
 * _dataLater.pageName; // "gl:home"
 *
 * @param {Object} store reference to the Redux store
 * @returns {Object} data layer
 */
export default function create(store) {
  const browseDataLayer = generateBrowseDataLayer(store);
  const homepageDataLayer = generateHomePageDataLayer(store);
  const clickHandlerDataLayer = generateClickHandlerDataLayer(store);
  const siteType = 'global site';
  const { pageCountCookieKey, previousPageValue } = API_CONFIG;
  const getStatePropValue = (propName) => {
    const { pageData, AnalyticsDataKey } = store.getState();
    const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
    const propValue = clickActionAnalyticsData[propName]
      ? clickActionAnalyticsData[propName]
      : pageData[propName];

    return propValue || '';
  };

  const getEvar97 = () => {
    const { AnalyticsDataKey, pageData } = store.getState();
    const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
    const { pageType } = pageData;
    let navText = '';
    if (pageType === 'browse') {
      navText =
        clickActionAnalyticsData.pageNavigationText ||
        clickActionAnalyticsData.pageNavigationText === ''
          ? clickActionAnalyticsData.pageNavigationText
          : pageData.pageNavigationText;
    }
    return navText;
  };

  return Object.create(defaultDataLayer, {
    ...browseDataLayer,
    ...homepageDataLayer,
    ...clickHandlerDataLayer,
    pageCount: {
      get() {
        return readCookie(pageCountCookieKey);
      },
    },
    pageName: {
      get() {
        return getPageNameValue(store.getState());
      },
    },
    orderId: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.orderId
          ? clickActionAnalyticsData.orderId
          : pageData.orderId;
      },
    },
    paymentMethod: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.paymentMethod
          ? clickActionAnalyticsData.paymentMethod
          : pageData.paymentMethod;
      },
    },
    billingCountry: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.billingCountry
          ? clickActionAnalyticsData.billingCountry
          : pageData.billingCountry;
      },
    },
    billingZip: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.billingZip
          ? clickActionAnalyticsData.billingZip
          : pageData.billingZip;
      },
    },
    orderSubtotal: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.orderSubtotal
          ? clickActionAnalyticsData.orderSubtotal
          : pageData.orderSubtotal;
      },
    },
    isCurrentRoute: () => false,

    pageShortName: {
      get() {
        /* If clickActionAnalyticsData has pageShortName then this will be used else
           pageShortName will used from pageData. Also if pageShortName is not available then pageName will
           be used. This is usually require when on some event you need
           to override the pageName value. For instance, onClick event.
         */
        const { pageData, AnalyticsDataKey } = store.getState();

        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        const pageShortName = clickActionAnalyticsData.pageShortName
          ? clickActionAnalyticsData.pageShortName
          : pageData.pageShortName;
        const pageName = clickActionAnalyticsData.pageName
          ? clickActionAnalyticsData.pageName
          : pageData.pageName;
        return `${pageShortName || pageName}`;
      },
    },

    pageType: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        const pageType = clickActionAnalyticsData.pageType
          ? clickActionAnalyticsData.pageType
          : pageData.pageType;
        const pageName = clickActionAnalyticsData.pageName
          ? clickActionAnalyticsData.pageName
          : pageData.pageName;
        return `${pageType || pageName}`;
      },
    },
    pageUrl: {
      get() {
        return `https://${document.location.hostname}${document.location.pathname}`;
      },
    },

    countryId: {
      get() {
        return store.getState().APIConfig.storeId;
      },
    },

    pageLocale: {
      get() {
        const { session = {} } = store.getState();
        const { siteDetails } = session;
        return `${siteDetails.country.toLowerCase()}:${siteDetails.language.toLowerCase()}`;
      },
    },

    pageSection: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.pageSection
          ? clickActionAnalyticsData.pageSection
          : pageData.pageSection;
      },
    },

    pageSubSubSection: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.pageSubSection
          ? clickActionAnalyticsData.pageSubSection
          : pageData.pageSubSection;
      },
    },

    pageTertiarySection: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.pageTertiarySection
          ? clickActionAnalyticsData.pageTertiarySection
          : pageData.pageTertiarySection;
      },
    },

    siteType: {
      get() {
        return siteType;
      },
    },

    customerType: {
      get() {
        return getUserLoggedInState(store.getState()) ? 'no rewards:logged in' : 'no rewards:guest';
      },
    },

    checkoutType: {
      get() {
        const state = store.getState();
        const { pageType = '' } = state.pageData;
        const cartCheckoutPageType = getCartCheckoutPageType(pageType);
        let userType = '';
        if (cartCheckoutPageType) {
          const personalData = state.User.get('personalData');
          userType = personalData && personalData.get('isGuest') === false ? 'registered' : 'guest';
        }
        return userType;
      },
    },

    userEmailAddress: {
      get() {
        return store.getState().User.getIn(['personalData', 'contactInfo', 'emailAddress'], '');
      },
    },

    giftType: {
      get() {
        const clickAnalyticsData = store
          .getState()
          .AnalyticsDataKey.get('clickActionAnalyticsData', {});
        const giftType = getSessionStorage('PREFERRED_GIFT_SERVICE');
        return clickAnalyticsData.confirmationPage && giftType;
      },
    },

    currencyCode: {
      get() {
        return store.getState().session.siteDetails.currency.toUpperCase();
      },
    },

    pageDate: {
      get() {
        return new Date().toISOString().split('T')[0];
      },
    },

    customerId: {
      get() {
        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.customerId
          ? clickActionAnalyticsData.customerId
          : store.getState().User.getIn(['personalData', 'userId'], '');
      },
    },

    customerFirstName: {
      get() {
        return store.getState().User.getIn(['personalData', 'contactInfo', 'firstName'], '');
      },
    },

    customerLastName: {
      get() {
        return store.getState().User.getIn(['personalData', 'contactInfo', 'lastName'], '');
      },
    },

    productId: {
      get() {
        return getStatePropValue('productId');
      },
    },

    pageNavigationText: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const { pageType } = pageData;
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return (
          (pageType !== 'product' &&
            (clickActionAnalyticsData.pageNavigationText ||
            clickActionAnalyticsData.pageNavigationText === ''
              ? clickActionAnalyticsData.pageNavigationText
              : pageData.pageNavigationText)) ||
          ''
        );
      },
    },

    cartType: {
      get() {
        const { pageName = '' } = store.getState().pageData;
        let typeCart = '';
        if (pageName === 'checkout:confirmation') {
          const orderDetails = store.getState().CartPageReducer.get('orderDetails');
          typeCart = 'standard';
          const isBopisOrder = orderDetails.get('isBopisOrder');
          const isBossOrder = orderDetails.get('isBossOrder');
          const isPickupOrder = orderDetails.get('isPickupOrder');
          const isShippingOrder = orderDetails.get('isShippingOrder');
          if (isShippingOrder && (isBopisOrder || isBossOrder || isPickupOrder)) {
            typeCart = 'mix';
          } else if (isBopisOrder && !isBossOrder) {
            typeCart = 'bopis';
          } else if (isBossOrder && !isBopisOrder) {
            typeCart = 'boss';
          }
        }
        return typeCart;
      },
    },
    products: {
      get() {
        const { AnalyticsDataKey, pageData } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};

        const pageProducts = clickActionAnalyticsData.products
          ? clickActionAnalyticsData.products
          : pageData.products;

        return pageProducts || [];
      },
    },

    currentState: {
      get() {
        return store.getState();
      },
    },

    storeId: {
      get() {
        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.storeId;
      },
    },
    brandId: {
      get() {
        const { brandId = '' } = store.getState().APIConfig;
        return brandId.toUpperCase();
      },
    },
    landingSiteBrandId: {
      get() {
        const { landingSite } = API_CONFIG;
        return readCookie(landingSite) || '';
      },
    },
    pageErrorType: {
      get() {
        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.pageErrorType;
      },
    },
    pageErrorValues: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'pageErrorValues'], '');
      },
    },
    appDownloadLocation: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'appDownloadLocation'], '');
      },
    },
    essentialsFormValue: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'essentialsFormValue']);
      },
    },
    Click_Navigation_v97: {
      get() {
        return getEvar97() || '';
      },
    },
    eVar97FirstTouch: {
      get() {
        const analyticsObj = getSetSessionStorageObj('e97', false);
        const ftVal = (analyticsObj && analyticsObj.e97FirstTouch) || '';
        const flag = (analyticsObj && analyticsObj.flag) || false;
        if (ftVal !== '' && flag) return '';

        const ev97 = getEvar97();
        if (ev97 !== '') {
          const obj = {
            e97FirstTouch: ev97,
            flag: true,
          };
          getSetSessionStorageObj('e97', true, obj);
        }
        return ev97;
      },
    },
    eVar28: {
      get() {
        let prevValue = '';
        const pageName = getPageNameValue(store.getState());
        const cookieVal = readCookie(previousPageValue);
        if (cookieVal) {
          prevValue = cookieVal;
          if (prevValue !== pageName) {
            setCookie({
              key: previousPageValue,
              value: pageName,
            });
          }
        } else if (pageName) {
          setCookie({
            key: previousPageValue,
            value: pageName,
          });
        }
        return prevValue;
      },
    },
    mprItem: {
      get() {
        return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'mprItem'], '');
      },
    },
    plccItem: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'plccItem'], '');
      },
    },
    guestItem: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'guestItem'], '');
      },
    },
    metric86: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'metric86'], '');
      },
    },
    getAppleTrackValues: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'applePayButtonLocation'], '');
      },
    },
    orderHelpLocation: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'orderHelpLocation'], '');
      },
    },
    orderId_cd127: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'orderId_cd127'], '');
      },
    },
    orderHelp_path_cd128: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'orderHelp_path_cd128'], '');
      },
    },
    metric112: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'metric112'], '');
      },
    },
    loyaltyLocation: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'loyaltyLocation']);
      },
    },
    afterpay_metrics141: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'afterpay_metrics141'], '');
      },
    },
    afterpay_cd131: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'afterpay_cd131'], '');
      },
    },
    metric139: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event139'], '');
      },
    },
    metric142: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event142'], '');
      },
    },
    metric150: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event150'], '');
      },
    },
    metric151: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event151'], '');
      },
    },
    metric152: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event152'], '');
      },
    },
    metric153: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event153'], '');
      },
    },
    metric145: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'initiateLiveChat_cm145'], '');
      },
    },
    metric159: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'metric159'], '');
      },
    },
    metric160: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'metric160'], '');
      },
    },
  });
}
