// 9fbef606107a605d69c0edbcd8029e5d 
const getSubCategory = store => {
  return store
    .getState()
    .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'listingSubCategory'], '');
};

export const generateHomePageDataLayer = store => {
  return {
    listingSubCategory: {
      get() {
        return getSubCategory(store) || '';
      },
    },
    pageLocale: {
      get() {
        const { session = {} } = store.getState();
        const { siteDetails } = session;
        return `${siteDetails.country}:${siteDetails.language}`;
      },
    },
  };
};

export default generateHomePageDataLayer;
