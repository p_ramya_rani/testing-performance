// 9fbef606107a605d69c0edbcd8029e5d
import { getIcidCampaignList } from '@tcp/core/src/analytics/utils';
import { setSessionStorage, getSessionStorage } from '@tcp/core/src/utils/utils.web';

const sortfilterTypeSeparator = '^';
const multiSortFilterSeparator = ':';

export const getSetSessionStorageObj = (param, setObj, obj) => {
  const analyticsObj = JSON.parse(getSessionStorage('AnalyticsObj')) || {};
  if (setObj) {
    analyticsObj[param] = obj;
    setSessionStorage({ key: 'AnalyticsObj', value: JSON.stringify(analyticsObj) });
    return null;
  }
  return analyticsObj[param] || {};
};

const generateSortFilterKeys = (ParamObj) => {
  let sortFilterKey = '';
  Object.keys(ParamObj).forEach((key) => {
    if (ParamObj[key] && ParamObj[key].length) {
      if (sortFilterKey) sortFilterKey += sortfilterTypeSeparator;
      sortFilterKey += `${key}=${ParamObj[key].join(multiSortFilterSeparator)}`;
    }
  });
  return sortFilterKey;
};

const navConditions = (navText, pageType, products) => {
  if (pageType === 'myplace' || pageType === 'product') return false;
  if (
    navText !== '' &&
    !(
      navText.includes('header') ||
      navText.includes('footer') ||
      (products && products[0].dimension92)
    )
  )
    return true;
  return false;
};

const var22Val = (args) => {
  const { var22, icid, search, navText, pageType, products } = args;
  let returnVal = '';
  if (var22) {
    returnVal = var22;
  } else if (icid !== '') {
    returnVal = 'internal campaign';
  } else if (search !== '') {
    returnVal = 'internal search';
  } else if (navConditions(navText, pageType, products)) {
    returnVal = 'browse';
  } else if (
    (pageType === 'product' || pageType === 'outfit') &&
    products &&
    products[0].dimension92
  ) {
    returnVal = `cross sell - ${products[0].dimension92}`;
  }
  return returnVal;
};

export const getParamValues = (store) => {
  const internalCampaignId = store
    .getState()
    .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'internalCampaignId'], '');
  const searchText = store
    .getState()
    .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'pageSearchText'], '');
  const { pageData, AnalyticsDataKey } = store.getState();
  const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
  const navigationText =
    clickActionAnalyticsData.pageNavigationText ||
    clickActionAnalyticsData.pageNavigationText === ''
      ? clickActionAnalyticsData.pageNavigationText
      : pageData.pageNavigationText || '';
  return {
    icid: internalCampaignId,
    search: searchText,
    navText: navigationText,
  };
};

const getFilterParams = (store) => {
  const state = store.getState();
  const { form: { 'filter-form': { values = {} } = {} } = {} } = state;
  const {
    categoryPath2_uFilter: category = [],
    TCPColor_uFilter: color = [],
    v_tcpsize_uFilter: size = [],
    v_tcpfit_unbxd_uFilter: fit = [],
    age_group_uFilter: age = [],
    gender_uFilter: gender = [],
    unbxd_price_range_uFilter: price = [],
  } = values;
  return generateSortFilterKeys({ category, color, size, fit, age, gender, price });
};

const sortMapper = {
  'min_offer_price desc': 'price: high to low',
  'min_offer_price asc': 'price: low to high',
  'newest_score desc': 'new arrivals',
  'favoritedcount desc': 'most favorited',
  'TCPBazaarVoiceRating desc': 'top rated',
};

const getSortParams = (store) => {
  const state = store.getState();
  const { form: { 'filter-form': { values = {} } = {} } = {} } = state;
  const { sort } = values;
  return sort ? `sort=${sortMapper[sort]}` : 'sort=recommended';
};

const getDepartmentList = (store) => {
  return store
    .getState()
    .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'departmentList'], '');
};

const getCategoryList = (store) => {
  return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'categoryList'], '');
};

const getPageType = (store) => {
  const state = store.getState();
  return state.pageData && state.pageData.pageName;
};

const getListingCount = (store) => {
  const state = store.getState();
  const pageType = state.pageData && state.pageData.pageType;
  return pageType === 'browse' && state.ProductListing && state.ProductListing.totalProductsCount;
};

const getPageFullCategoryName = (store) => {
  const state = store.getState();
  const { AnalyticsDataKey } = state;
  const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
  const pageType = state.pageData && state.pageData.pageType;
  if (
    pageType === 'browse' &&
    (clickActionAnalyticsData.checkoutClick === undefined ||
      !clickActionAnalyticsData.checkoutClick)
  )
    return state.ProductListing && state.ProductListing.entityCategory;
  return '';
};

export const generateBrowseDataLayer = (store) => {
  return {
    listingFilterList: {
      get() {
        const filterLiteral = getFilterParams(store);
        return filterLiteral || '';
      },
    },
    listingSortList: {
      get() {
        const { pageData, AnalyticsDataKey } = store.getState();
        const { pageType } = pageData;
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        let filterLiteral = getFilterParams(store) || '';
        filterLiteral = filterLiteral ? sortfilterTypeSeparator + filterLiteral : '';
        const sortLiteral = getSortParams(store);
        if (
          pageType === 'browse' &&
          (clickActionAnalyticsData.checkoutClick === undefined ||
            !clickActionAnalyticsData.checkoutClick)
        )
          return sortLiteral ? sortLiteral + filterLiteral : 'sort=recommended';
        return '';
      },
    },
    listingDepartment: {
      get() {
        return getDepartmentList(store) || '';
      },
    },
    listingCategory: {
      get() {
        return getCategoryList(store) || '';
      },
    },
    listingCount: {
      get() {
        return getListingCount(store) || '';
      },
    },
    pageFullCategoryName: {
      get() {
        return getPageFullCategoryName(store) || '';
      },
    },

    externalReferrer: {
      get() {
        const { pageData } = store.getState();
        return (pageData && pageData.pageReferer) || '';
      },
    },
    campaignId: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'campaignId'], '');
      },
    },
    internalCampaignId: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'internalCampaignId'], '');
      },
    },
    internalCampaignIdList: {
      get() {
        return getIcidCampaignList(store.getState());
      },
    },
    internalCampaignIdFirstTouch: {
      get() {
        const analyticsObj = getSetSessionStorageObj('icid', false);
        const ftVal = (analyticsObj && analyticsObj.internalCampaignIdFirstTouch) || '';
        const flag = (analyticsObj && analyticsObj.flag) || false;
        if (ftVal !== '' && flag) return '';

        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', '') || '';
        const icidVal = clickActionAnalyticsData.internalCampaignId || '';
        if (icidVal !== '') {
          const obj = {
            internalCampaignIdFirstTouch: icidVal,
            flag: true,
          };
          getSetSessionStorageObj('icid', true, obj);
        }
        return icidVal;
      },
    },
    storeSearchCriteria: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'storeSearchCriteria'], '');
      },
    },
    storeSearchDistance: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'storeSearchDistance'], '');
      },
    },

    productFindingMethod: {
      get() {
        return getPageType(store) || '';
      },
    },

    pageLocale: {
      get() {
        const { session = {} } = store.getState();
        const { siteDetails } = session;
        return `${siteDetails.country}:${siteDetails.language}`;
      },
    },

    searchType: {
      get() {
        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.pageSearchType || '';
      },
    },
    searchTypeFirstTouch: {
      // eslint-disable-next-line complexity
      get() {
        const { pageType = '' } = store.getState().pageData;
        const analyticsObj = getSetSessionStorageObj('search', false);
        const ftVal = (analyticsObj && analyticsObj.searchTypeFirstTouch) || '';
        const flag = (analyticsObj && analyticsObj.searchTypeFlag) || false;
        if (ftVal !== '' && flag) return '';

        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', '') || '';
        const searchTerm = clickActionAnalyticsData.pageSearchType || '';
        if (searchTerm !== '' && pageType === 'search') {
          const obj = {
            searchTypeFirstTouch: searchTerm,
            searchTypeFlag: true,
            ...analyticsObj,
          };
          getSetSessionStorageObj('search', true, obj);
        }
        return (pageType === 'search' && searchTerm) || '';
      },
    },
    searchText: {
      get() {
        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', '') || '';
        let pageSearchText = clickActionAnalyticsData.pageSearchText || '';
        if (pageSearchText.includes('?')) {
          pageSearchText = pageSearchText.substr(0, pageSearchText.indexOf('?'));
        }
        return pageSearchText;
      },
    },
    searchTextFirstTouch: {
      get() {
        const analyticsObj = getSetSessionStorageObj('search', false);
        const ftVal = (analyticsObj && analyticsObj.searchTextFirstTouch) || '';
        const flag = (analyticsObj && analyticsObj.searchTextFlag) || false;
        if (ftVal !== '' && flag) return '';

        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', '') || '';
        let searchTerm = clickActionAnalyticsData.pageSearchText || '';
        if (searchTerm.includes('?')) {
          searchTerm = searchTerm.substr(0, searchTerm.indexOf('?'));
        }
        if (searchTerm !== '') {
          const obj = {
            searchTextFirstTouch: searchTerm,
            searchTextFlag: true,
            ...analyticsObj,
          };
          getSetSessionStorageObj('search', true, obj);
        }
        return searchTerm;
      },
    },
    searchResults: {
      get() {
        const state = store.getState();
        const { AnalyticsDataKey } = state;
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.listingCount || '';
      },
    },
    promoFilterCategoryTrail: {
      get() {
        const { AnalyticsDataKey } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.promoFilterCategoryTrail || '';
      },
    },
    eVar22: {
      get() {
        const { icid, search, navText } = getParamValues(store);
        const {
          AnalyticsDataKey,
          pageData: { pageType },
        } = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        const { products, var22 } = clickActionAnalyticsData;

        return var22Val({ icid, search, navText, pageType, products, var22 });
      },
    },
  };
};

export default generateBrowseDataLayer;
