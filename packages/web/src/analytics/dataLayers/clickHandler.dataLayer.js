// 9fbef606107a605d69c0edbcd8029e5d 
import { getEvents } from '@tcp/core/src/analytics/utils';
import { getParamValues } from './browse.dataLayer';

const getAnalyticsData = store => {
  const state = store.getState();
  return state.AnalyticsDataKey && state.AnalyticsDataKey.get('clickActionAnalyticsData');
};

const getCustomEvents = store => {
  const defaultData = getAnalyticsData(store);
  let customEvents = (defaultData && defaultData.customEvents) || [];
  const eventArr = getEvents(store);
  customEvents = [...customEvents, ...eventArr];
  return customEvents;
};

const getProductsData = store => {
  const defaultData = getAnalyticsData(store);
  return defaultData && defaultData.products;
};

const getEventName = store => {
  const defaultData = getAnalyticsData(store);
  return defaultData && defaultData.eventName;
};

const getCouponCode = store => {
  const defaultData = getAnalyticsData(store);
  return defaultData && defaultData.couponCode;
};

const getStoreSearchCriteria = store => {
  const defaultData = getAnalyticsData(store);
  return defaultData && defaultData.storeSearchCriteria;
};

const getStoreSearchDistance = store => {
  const defaultData = getAnalyticsData(store);
  return defaultData && defaultData.storeSearchDistance;
};

const getInternalCampaignId = store => {
  const { navText } = getParamValues(store);
  const defaultData = getAnalyticsData(store);
  return navText !== '' ? 'non internal campaign' : defaultData && defaultData.internalCampaignId;
};
const getSocialNetwork = store => {
  const defaultData = getAnalyticsData(store);
  return defaultData && defaultData.socialNetwork;
};

const getVirtualPageView = store => {
  const defaultData = getAnalyticsData(store);
  return defaultData && defaultData.virtualPageView;
};

const getClickEvent = store => {
  const defaultData = getAnalyticsData(store);
  return (defaultData && defaultData.clickEvent) || false;
};

const supressProps = store => {
  const defaultData = getAnalyticsData(store);
  return (defaultData && defaultData.supressProps) || [];
};

const var22 = store => {
  const defaultData = getAnalyticsData(store);
  return (defaultData && defaultData.var22) || '';
};

export const generateClickHandlerDataLayer = store => {
  return {
    eventData: {
      get() {
        return {
          customEvents: getCustomEvents(store),
          products: getProductsData(store) || '',
          eventName: getEventName(store) || '',
          couponCode: getCouponCode(store) || '',
          storeSearchCriteria: getStoreSearchCriteria(store) || '',
          storeSearchDistance: getStoreSearchDistance(store) || '',
          internalCampaignId: getInternalCampaignId(store) || '',
          socialNetwork: getSocialNetwork(store) || '',
          virtualPageView: getVirtualPageView(store) || false,
          clickEvent: getClickEvent(store),
          supressProps: supressProps(store),
          var22: var22(store),
        };
      },
    },
  };
};

export default generateClickHandlerDataLayer;
