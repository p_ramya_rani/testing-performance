// 9fbef606107a605d69c0edbcd8029e5d
import { all } from 'redux-saga/effects';
import BootstrapSaga from '@tcp/core/src/reduxStore/sagas/bootstrap';
import LabelsSaga from '@tcp/core/src/reduxStore/sagas/labels';
import SEODataSaga from '@tcp/core/src/reduxStore/sagas/seoData';
import LayoutSaga from '@tcp/core/src/reduxStore/sagas/layout';
import SearchPageSaga from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.saga';
import LoginPageSaga from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.saga';
import UserSaga from '@tcp/core/src/components/features/account/User/container/User.saga';
import LogOutPageSaga from '@tcp/core/src/components/features/account/Logout/container/LogOut.saga';
import ForgotPasswordSaga from '@tcp/core/src/components/features/account/ForgotPassword/container/ForgotPassword.saga';
import ResetPasswordSaga from '@tcp/core/src/components/features/account/ResetPassword/container/ResetPassword.saga';
import ChangePasswordSaga from '@tcp/core/src/components/features/account/ChangePassword/container/ChangePassword.saga';
import UpdateProfileSaga from '@tcp/core/src/components/features/account/AddEditPersonalInformation/container/AddEditPersonalInformation.saga';
import AddEditAddressSaga from '@tcp/core/src/components/common/organisms/AddEditAddress/container/AddEditAddress.saga';
import AddressBookSaga from '@tcp/core/src/components/features/account/AddressBook/container/AddressBook.saga';
import PaymentSaga from '@tcp/core/src/components/features/account/Payment/container/Payment.saga';
import CreateAccountSaga from '@tcp/core/src/components/features/account/CreateAccount/container/CreateAccount.saga';
import DeleteCardSaga from '@tcp/core/src/components/features/account/Payment/container/DeleteCard.saga';
import GiftCardBalanceSaga from '@tcp/core/src/components/features/account/Payment/container/GetCardBalance.saga';
import DefaultPaymentSaga from '@tcp/core/src/components/features/account/Payment/container/DefaultPayment.saga';
import { AddGiftCardSaga } from '@tcp/core/src/components/features/account/Payment/AddGiftCard/container/AddGiftCard.saga';
import AddedToBagSaga from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.saga';
import DeleteAddressSaga from '@tcp/core/src/components/features/account/AddressBook/container/DeleteAddress.saga';
import BonusPointsSaga from '@tcp/core/src/components/common/organisms/BonusPointsDays/container/BonusPointsDays.saga';
import { SetDefaultShippingAddressSaga } from '@tcp/core/src/components/features/account/AddressBook/container/DefaultShippingAddress.saga';
import AddressVerificationSaga from '@tcp/core/src/components/common/organisms/AddressVerification/container/AddressVerification.saga';
import BirthdaySavingsSaga from '@tcp/core/src/components/features/account/common/organism/BirthdaySavingsList/container/BirthdaySavingsList.saga';
import AccountSaga from '@tcp/core/src/components/features/account/Account/container/Account.saga';
import AccountHeaderSaga from '@tcp/core/src/components/features/account/common/organism/AccountHeader/container/AccountHeader.saga';
import AddEditCreditCardSaga from '@tcp/core/src/components/features/account/AddEditCreditCard/container/AddEditCreditCard.saga';
import CartPageSaga from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.saga';
import ProductListingSaga from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import NavigationSaga from '@tcp/core/src/components/features/content/Navigation/container/Navigation.saga';
import ProductDetailSaga from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.saga';
import QuickViewSaga from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.saga';
import FavoriteSaga from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.saga';
import CouponSaga from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.saga';
import CheckoutSaga from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.saga';
import BagPageSaga from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.saga';
import LoyaltyPageSaga from '@tcp/core/src/components/features/CnC/LoyaltyBanner/container/Loyalty.saga';
import TrackOrderSaga from '@tcp/core/src/components/features/account/TrackOrder/container/TrackOrder.saga';
import PointsHistorySaga from '@tcp/core/src/components/features/account/common/organism/PointsHistory/container/PointsHistory.saga';
import EarnExtraPointsSaga from '@tcp/core/src/components/features/account/common/organism/EarnExtraPointsTile/container/EarnExtraPointsTile.saga';
import EarnedPointsNotificationSaga from '@tcp/core/src/components/features/account/common/organism/EarnExtraPointsTile/container/EarnedPointsNotification.saga';
import OrderDetailsListSaga from '@tcp/core/src/components/features/account/OrderDetails/container/OrderDetails.saga';
import AddAirmilesBannerSaga from '@tcp/core/src/components/features/CnC/common/organism/AirmilesBanner/container/AirmilesBanner.saga';
import ApplyCreditCardSaga, {
  SubmitInstantCardApplication,
} from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.saga';
import SocialAccountSaga from '@tcp/core/src/components/common/organisms/SocialAccount/container/Social.saga';
import BillingPaymentSaga from '@tcp/core/src/components/features/CnC/Checkout/organisms/BillingPaymentForm/container/CreditCard.saga';
import GiftCardsSaga from '@tcp/core/src/components/features/CnC/Checkout/organisms/GiftCardsSection/container/GiftCards.saga';
import MailingAddressSaga from '@tcp/core/src/components/features/account/MyProfile/organism/MailingInformation/container/MailingAddress.saga';
import ProductTabListSaga from '@tcp/core/src/components/common/organisms/ProductTabList/container/ProductTabList.saga';
import StyliticsProductTabListSaga from '@tcp/core/src/components/common/organisms/StyliticsProductTabList/container/StyliticsProductTabList.saga';
import RecommendationsSaga from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.saga';
import QMRecommendationsModalSaga from '@tcp/core/src/components/common/molecules/QMRecommendationsModal/container/QMRecommendationModal.saga';
import StoreLocatorSaga from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.saga';
import StoreDetailSaga from '@tcp/core/src/components/features/storeLocator/StoreDetail/container/StoreDetail.saga';
import StoresInternationalSaga from '@tcp/core/src/components/features/storeLocator/StoresInternational/container/StoresInternational.saga';
import PickupStoreSaga from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.saga';
import ConfirmationPageSaga from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.saga';
import ProductPickup from '@tcp/core/src/components/common/organisms/ProductPickup/container/ProductPickup.saga';
import OutfitDetailsSaga from '@tcp/core/src/components/features/browse/OutfitDetails/container/OutfitDetails.saga';
import PointsClaimSaga from '@tcp/core/src/components/features/account/PointsClaim/container/PointsClaim.saga';
import NavigateXHRSaga from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.saga';
import OrdersSaga from '@tcp/core/src/components/features/account/Orders/container/Orders.saga';
import ExtraPointsSaga from '@tcp/core/src/components/features/account/ExtraPoints/container/ExtraPoints.saga';
import SearchBarSaga from '@tcp/core/src/components/common/molecules/SearchBar/SearchBar.saga';
import StoreListSaga from '@tcp/core/src/components/features/storeLocator/StoreList/container/StoreList.saga';
import SubscribeStoreSaga from '@tcp/core/src/components/features/account/MyPreferenceSubscription/container/MyPreferenceSubscription.saga';
import BundleProductSaga from '@tcp/core/src/components/features/browse/BundleProduct/container/BundleProduct.saga';
import EmailSignupForm from '@tcp/core/src/components/common/organisms/EmailSignupForm/container/EmailSignupForm.saga';
import SmsSignupForm from '@tcp/core/src/components/common/organisms/SmsSignupForm/container/SmsSignupForm.saga';
import EmailSmsSignupSaga from '@tcp/web/src/components/common/molecules/EmailSmsSignUp/container/EmailSmsSignUp.saga';
import AbtestSaga from '@tcp/core/src/components/common/molecules/abTest/abTest.saga';
import AnalyticsSaga from '@tcp/core/src/analytics/Analytics.saga';
import EddSaga from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.saga';
import ResendOrderConfirmationSaga from '@tcp/core/src/components/common/organisms/ResendOrderConfirmation/container/ResendOrderConfirmation.saga';
import RedirectSaga from '@tcp/core/src/components/features/content/RedirectContentPage/container/RedirectContentPage.saga';
import CustomerHelpSaga from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.saga';
import GiftCardSaga from '@tcp/core/src/components/features/GiftCard/container/GiftCard.saga';
import BrandTabs from '@tcp/web/src/components/features/content/Header/molecules/BrandTabs/container/BrandTabs.saga';
import EmailConfirmationSaga from '@tcp/core/src/components/features/account/CustomerHelp/organisms/EmailConfirmation/container/EmailConfirmation.saga';
import AccountConfirmationSaga from '@tcp/core/src/components/features/account/CustomerHelp/organisms/MergeAccounts/organisms/AccountConfirmation/container/AccountConfirmation.saga';
import MergeAccountOtpSaga from '@tcp/core/src/components/features/account/CustomerHelp/organisms/MergeAccounts/organisms/MergeAccountsOtp/container/MergeAccountOtp.saga';
import CountrySelectorSaga from '../../components/features/content/Header/molecules/CountrySelector/container/CountrySelector.saga';
import SiteMapSaga from '../../components/features/content/SiteMap/container/SiteMap.saga';

export default function* rootSaga() {
  yield all([
    BootstrapSaga(),
    LabelsSaga(),
    SEODataSaga(),
    LayoutSaga(),
    LoginPageSaga(),
    UserSaga(),
    LogOutPageSaga(),
    ForgotPasswordSaga(),
    SearchBarSaga(),
    AddEditAddressSaga(),
    AddressBookSaga(),
    DeleteAddressSaga(),
    AddedToBagSaga(),
    SetDefaultShippingAddressSaga(),
    AddressVerificationSaga(),
    PaymentSaga(),
    TrackOrderSaga(),
    EmailSignupForm(),
    EmailSmsSignupSaga(),
    SmsSignupForm(),
    DeleteCardSaga(),
    GiftCardBalanceSaga(),
    DefaultPaymentSaga(),
    AddEditCreditCardSaga(),
    SubmitInstantCardApplication(),
    AddGiftCardSaga(),
    AccountSaga(),
    BagPageSaga(),
    LoyaltyPageSaga(),
    CartPageSaga(),
    CreateAccountSaga(),
    ProductListingSaga(),
    ProductDetailSaga(),
    BonusPointsSaga(),
    CouponSaga(),
    CheckoutSaga(),
    AccountHeaderSaga(),
    CountrySelectorSaga(),
    PointsHistorySaga(),
    EarnExtraPointsSaga(),
    EarnedPointsNotificationSaga(),
    OrderDetailsListSaga(),
    ResetPasswordSaga(),
    ApplyCreditCardSaga(),
    ChangePasswordSaga(),
    UpdateProfileSaga(),
    BirthdaySavingsSaga(),
    GiftCardsSaga(),
    AddAirmilesBannerSaga(),
    MailingAddressSaga(),
    ProductTabListSaga(),
    StyliticsProductTabListSaga(),
    RecommendationsSaga(),
    QMRecommendationsModalSaga(),
    BillingPaymentSaga(),
    StoreLocatorSaga(),
    SocialAccountSaga(),
    SearchPageSaga(),
    PickupStoreSaga(),
    StoreDetailSaga(),
    ProductPickup(),
    QuickViewSaga(),
    PointsClaimSaga(),
    ConfirmationPageSaga(),
    NavigateXHRSaga(),
    OrdersSaga(),
    OutfitDetailsSaga(),
    FavoriteSaga(),
    ExtraPointsSaga(),
    SubscribeStoreSaga(),
    StoresInternationalSaga(),
    StoreListSaga(),
    BundleProductSaga(),
    NavigationSaga(),
    SiteMapSaga(),
    AbtestSaga(),
    AnalyticsSaga(),
    EddSaga(),
    ResendOrderConfirmationSaga(),
    RedirectSaga(),
    CustomerHelpSaga(),
    GiftCardSaga(),
    BrandTabs(),
    MergeAccountOtpSaga(),
    EmailConfirmationSaga(),
    AccountConfirmationSaga(),
  ]);
}
