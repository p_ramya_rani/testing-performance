// 9fbef606107a605d69c0edbcd8029e5d 
import { createMiddleware } from 'redux-beacon';
import logger from '@redux-beacon/logger';
import GoogleTagManager from '@redux-beacon/google-tag-manager';
import eventMapping from '../../analytics/eventMapping';

/**
 * Create the Redux-Beacon middleware instance.
 */
export default function create() {
  return createMiddleware(
    eventMapping,
    GoogleTagManager(),
    // Only use logger in dev mode
    process.env.NODE_ENV === 'development' && { logger }
  );
}
