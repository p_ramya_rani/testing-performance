// 9fbef606107a605d69c0edbcd8029e5d
import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import LoginPageReducer from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.reducer';
import ForgotPasswordReducer from '@tcp/core/src/components/features/account/ForgotPassword/container/ForgotPassword.reducer';
import AddressBookReducer from '@tcp/core/src/components/features/account/AddressBook/container/AddressBook.reducer';
import PaymentReducer from '@tcp/core/src/components/features/account/Payment/container/Payment.reducer';
import LabelReducer from '@tcp/core/src/reduxStore/reducers/labels';
import SEODataReducer from '@tcp/core/src/reduxStore/reducers/seoData';
import LayoutReducer from '@tcp/core/src/reduxStore/reducers/layout';
import SubNavigationReducer from '@tcp/core/src/reduxStore/reducers/subNavigation';
import ApiConfigReducer from '@tcp/core/src/reduxStore/reducers/apiConfig';
import SessionConfigReducer from '@tcp/core/src/reduxStore/reducers/sessionConfig';
import AddEditAddressReducer from '@tcp/core/src/components/common/organisms/AddEditAddress/container/AddEditAddress.reducer';
import AddEditCreditCardReducer from '@tcp/core/src/components/features/account/AddEditCreditCard/container/AddEditCreditCard.reducer';
import ModulesReducer from '@tcp/core/src/reduxStore/reducers/modules';
import AddGiftCardReducer from '@tcp/core/src/components/features/account/Payment/AddGiftCard/container/AddGiftCard.reducer';
import { createFilteredReducer } from '@tcp/core/src/utils/redux.util';
import AddressVerificationReducer from '@tcp/core/src/components/common/organisms/AddressVerification/container/AddressVerification.reducer';
import AccountReducer from '@tcp/core/src/components/features/account/Account/container/Account.reducer';
import CartItemTile from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.reducer';
import CartPage from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.reducer';
import CheckoutReducer from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.reducer';
import OverlayModalReducer from '@tcp/core/src/components/features/account/OverlayModal/container/OverlayModal.reducer';
import ProductListingReducer from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.reducer';
import CreateAccountReducer from '@tcp/core/src/components/features/account/CreateAccount/container/CreateAccount.reducer';
import BonusPointsDaysReducer from '@tcp/core/src/components/common/organisms/BonusPointsDays/container/BonusPointsDays.reducer';
import CouponsReducer from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.reducer';
import AirmilesBannerReducer from '@tcp/core/src/components/features/CnC/common/organism/AirmilesBanner/container/AirmilesBanner.reducer';
import AccountHeaderReducer from '@tcp/core/src/components/features/account/common/organism/AccountHeader/container/AccountHeader.reducer';
import PointsHistoryReducer from '@tcp/core/src/components/features/account/common/organism/PointsHistory/container/PointsHistory.reducer';
import EarnExtraPointsReducer from '@tcp/core/src/components/features/account/common/organism/EarnExtraPointsTile/container/EarnExtraPointsTile.reducer';
import OrderDetailsDataReducer from '@tcp/core/src/components/features/account/OrderDetails/container/OrderDetails.reducer';
import ResetPasswordReducer from '@tcp/core/src/components/features/account/ResetPassword/container/ResetPassword.reducer';
import TrackOrderReducer from '@tcp/core/src/components/features/account/TrackOrder/container/TrackOrder.reducer';
import ChangePasswordReducer from '@tcp/core/src/components/features/account/ChangePassword/container/ChangePassword.reducer';
import MyProfileReducer from '@tcp/core/src/components/features/account/MyProfile/container/MyProfile.reducer';
import UpdateProfileReducer from '@tcp/core/src/components/features/account/AddEditPersonalInformation/container/AddEditPersonalInformation.reducer';
import DeviceInfoReducer from '@tcp/core/src/reduxStore/reducers/deviceInfo';
import ApplyCardReducer from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.reducer';
import AddMailingAddressReducer from '@tcp/core/src/components/features/account/MyProfile/organism/MailingInformation/container/MailingAddress.reducer';
import ProductDetailReducer from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.reducer';
import QuickViewReducer from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.reducer';
import ApplyNowModalPLCCReducer from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.reducer';
import QMRecommendationModalReducer from '@tcp/core/src/components/common/molecules/QMRecommendationsModal/container/QMRecommendationModal.reducer';
import ProductTabListReducer from '@tcp/core/src/components/common/organisms/ProductTabList/container/ProductTabList.reducer';
import StyliticsProductTabListReducer from '@tcp/core/src/components/common/organisms/StyliticsProductTabList/container/StyliticsProductTabList.reducer';
import BirthdaySavingsListReducer from '@tcp/core/src/components/features/account/common/organism/BirthdaySavingsList/container/BirthdaySavingsList.reducer';
import PickupModalReducer from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.reducer';
import ProductPickupReducer from '@tcp/core/src/components/common/organisms/ProductPickup/container/ProductPickup.reducer';
import OutfitDetailReducer from '@tcp/core/src/components/features/browse/OutfitDetails/container/OutfitDetails.reducer';
import RecommendationsReducer from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.reducer';
import SearchBarReducer from '@tcp/core/src/components/common/molecules/SearchBar/SearchBar.reducer';
import StoreLocatorReducer from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.reducer';
import SocialReducer from '@tcp/core/src/components/common/organisms/SocialAccount/container/Social.reducer';
import SearchPageReducer from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.reducer';
import StoreDetailReducer from '@tcp/core/src/components/features/storeLocator/StoreDetail/container/StoreDetail.reducer';
import PointsClaimReducer from '@tcp/core/src/components/features/account/PointsClaim/container/PointsClaim.reducer';
import NavigateXHRReducer from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.reducer';
import orderConfirmationReducer from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.reducer';
import OrdersReducer from '@tcp/core/src/components/features/account/Orders/container/Orders.reducer';
import FavoriteReducer from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.reducer';
import ExtraPointsReducer from '@tcp/core/src/components/features/account/ExtraPoints/container/ExtraPoints.reducer';
import StoresInternationalReducer from '@tcp/core/src/components/features/storeLocator/StoresInternational/container/StoresInternational.reducer';
import StoreListReducer from '@tcp/core/src/components/features/storeLocator/StoreList/container/StoreList.reducer';
import MyPreferenceSubscriptionReducer from '@tcp/core/src/components/features/account/MyPreferenceSubscription/container/MyPreferenceSubscription.reducer';
import BundleProductReducer from '@tcp/core/src/components/features/browse/BundleProduct/container/BundleProduct.reducer';
import AnalyticsReducer from '@tcp/core/src/analytics/Analytics.reducer';
import AbTestReducer from '@tcp/core/src/components/common/molecules/abTest/abtest.reducer';
import EddReducer from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.reducer';
import RedirectPageReducer from '@tcp/core/src/components/features/content/RedirectContentPage/container/RedirectContentPage.reducer';
import LastUpdatedReducer from '@tcp/core/src/reduxStore/reducers/lastUpdated';
import CustomerHelpReducer from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.reducer';
import mergeAccountReducer from '@tcp/core/src/components/features/account/CustomerHelp/organisms/MergeAccounts/container/MergeAccounts.reducer';
import mergeAccountOtpReducer from '@tcp/core/src/components/features/account/CustomerHelp/organisms/MergeAccounts/organisms/MergeAccountsOtp/container/MergeAccountOtp.reducer';
import MergeAccountEmailAddressConfirmation from '@tcp/core/src/components/features/account/CustomerHelp/organisms/EmailConfirmation/container/EmailConfirmation.reducer';

import {
  APICONFIG_REDUCER_KEY,
  APPLY_PLCC_REDUCER_KEY,
  SESSIONCONFIG_REDUCER_KEY,
  COUNTRY_SELECTOR_REDUCER_KEY,
  HEADER_REDUCER_KEY,
  FOOTER_REDUCER_KEY,
  LABEL_REDUCER_KEY,
  SEO_DATA_REDUCER_KEY,
  LAYOUT_REDUCER_KEY,
  LOADER_REDUCER_KEY,
  LOGINPAGE_REDUCER_KEY,
  ADDRESSBOOK_REDUCER_KEY,
  ADDRESS_VERIFICATION_REDUCER_KEY,
  PAYMENT_REDUCER_KEY,
  ADDEDITADDRESS_REDUCER_KEY,
  EMAIL_SIGNUP_REDUCER_KEY,
  SMS_SIGNUP_REDUCER_KEY,
  PLCC_OFFER_REDUCER_KEY,
  MODULES_REDUCER_KEY,
  ADDEDITCREDITCARD_REDUCER_KEY,
  ADD_GIFT_CARD_REDUCER_KEY,
  ADDED_TO_BAG_REDUCER_KEY,
  ACCOUNT_REDUCER_KEY,
  CARTITEMTILE_REDUCER_KEY,
  CARTPAGE_REDUCER_KEY,
  FORGOTPASSWORD_REDUCER_KEY,
  OVERLAY_MODAL_REDUCER_KEY,
  NAVIGATION_REDUCER_KEY,
  PRODUCT_LISTING_REDUCER_KEY,
  CREATE_ACCOUNT_REDUCER_KEY,
  BONUS_POINTS_DAYS_REDUCER_KEY,
  COUPON_REDUCER_KEY,
  AIRMILES_BANNER_REDUCER_KEY,
  ACCOUNTHEADER_REDUCER_KEY,
  POINTS_HISTORY_REDUCER_KEY,
  EARNEXTRAPOINTS_REDUCER_KEY,
  ORDERDETAILS_REDUCER_KEY,
  RESET_PASSWORD_REDUCER_KEY,
  CHANGE_PASSWORD_REDUCER_KEY,
  UPDATE_PROFILE_REDUCER_KEY,
  MY_PROFILE_REDUCER_KEY,
  USER_REDUCER_KEY,
  CHECKOUT_REDUCER_KEY,
  DEVICE_INFO_REDUCER_KEY,
  TRACK_ORDER_REDUCER_KEY,
  TOAST_REDUCER_KEY,
  MAILING_ADDRESS_REDUCER_KEY,
  PRODUCT_DETAIL_REDUCER_KEY,
  OUTFIT_DETAILS_REDUCER_KEY,
  APPLY_NOW_MODAL_REDUCER_KEY,
  QMRECOMMENDATION_MODAL_REDUCER_KEY,
  PRODUCT_TAB_LIST_REDUCER_KEY,
  STYLITICS_PRODUCT_TAB_LIST_REDUCER_KEY,
  BIRTHDAY_SAVING_LIST_REDUCER_KEY,
  PICKUP_MODAL_REDUCER_KEY,
  PRODUCT_PICKUP_REDUCER_KEY,
  RECOMMENDATIONS_REDUCER_KEY,
  SEARCH_REDUCER_KEY,
  STORE_LOCATOR_REDUCER_KEY,
  SOCIAL_REDUCER_KEY,
  SLP_PAGE_REDUCER_KEY,
  STORE_DETAIL_REDUCER_KEY,
  QUICK_VIEW_REDUCER_KEY,
  POINTS_CLAIM_REDUCER_KEY,
  CONFIRMATION_REDUCER_KEY,
  ORDERS_REDUCER_KEY,
  FAVORITES_REDUCER_KEY,
  EXTRA_POINTS_REDUCER_KEY,
  STORES_INTL_REDUCER_KEY,
  STORE_LIST_REDUCER_KEY,
  MY_PREFERENCE_REDUCER_KEY,
  BUNDLEPRODUCT_REDUCER_KEY,
  ANALYTICS_DATA_KEY,
  SUB_NAVIGATION_REDUCER_KEY,
  SITEMAP_REDUCER_KEY,
  AB_TEST_STORE_PATTERN,
  NAVIGATE_XHR,
  EDD_REDUCER_KEY,
  REDIRECT_REDUCER_KEY,
  LAST_UPDATED_REDUCER_KEY,
  CUSTOMER_HELP_REDUCER_KEY,
  GIFT_CARD_BALANCE_REDUCER_KEY,
  EMAIL_SMS_SIGNUP_REDUCER_KEY,
  MERGE_ACCOUNT_OTP_REDUCER_KEY,
  MERGE_ACCOUNT_EMAIL_ADDRESS_CONFIRMATION_REDUCER_KEY,
  MERGE_ACCOUNT_ACCOUNT_CONFIRMATION_REDUCER_KEY,
  MERGE_ACCOUNT_REDUCER_KEY,
} from '@tcp/core/src/constants/reducer.constants';
import { TRACK_PAGE_VIEW, UPDATE_PAGE_DATA } from '@tcp/core/src/analytics';
import LoaderReducer from '@tcp/core/src/components/common/molecules/Loader/container/Loader.reducer';
import HeaderReducer from '@tcp/core/src/components/common/organisms/Header/container/Header.reducer';
import FooterReducer from '@tcp/core/src/components/common/organisms/Footer/container/Footer.reducer';
import NavigationReducer from '@tcp/core/src/components/features/content/Navigation/container/Navigation.reducer';
import AddedToBagReducer from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.reducer';
import UserReducer from '@tcp/core/src/components/features/account/User/container/User.reducer';
import ToastMessageReducer from '@tcp/core/src/components/common/atoms/Toast/container/Toast.reducer.native';
import SmsSignupFormReducer from '@tcp/core/src/components/common/organisms/SmsSignupForm/container/SmsSignupForm.reducer';
import EmailSignupFormReducer from '@tcp/core/src/components/common/organisms/EmailSignupForm/container/EmailSignupForm.reducer';
import giftCardBalanceReducer from '@tcp/core/src/components/features/GiftCard/container/GiftCard.reducer';
import accountConfirmationReducer from '@tcp/core/src/components/features/account/CustomerHelp/organisms/MergeAccounts/organisms/AccountConfirmation/container/AccountConfirmation.reducer';
import EmailSignupModalReducer from '../../components/common/molecules/EmailSignupModal/container/EmailSignupModal.reducer';
import EmailSmsSignupReducer from '../../components/common/molecules/EmailSmsSignUp/container/EmailSmsSignUp.reducer';
import CountrySelectorReducer from '../../components/features/content/Header/molecules/CountrySelector/container/CountrySelector.reducer';
import SiteMapReducer from '../../components/features/content/SiteMap/container/SiteMap.reducer';
import SmsSignupModalReducer from '../../components/common/molecules/SmsSignupModal/container/SmsSignupModal.reducer';
import PlccOfferModalReducer from '../../components/common/molecules/PlccOfferModal/container/PlccOfferModal.reducer';

const EmailSignupReducers = combineReducers({
  EmailSignupModalReducer,
  EmailSignupFormReducer,
});

const SmsSignupReducers = combineReducers({
  SmsSignupModalReducer,
  SmsSignupFormReducer,
});

const filteredAppConfigReducer = createFilteredReducer(ApiConfigReducer, APICONFIG_REDUCER_KEY);
// TODO: filteredSessionConfigReducer should be used, but issue with immutable map to be corrected
// const filteredSessionConfigReducer = createFilteredReducer(
//   SessionConfigReducer,
//   SESSIONCONFIG_REDUCER_KEY
// );

const filteredProductTabListReducer = createFilteredReducer(
  ProductTabListReducer,
  PRODUCT_TAB_LIST_REDUCER_KEY
);

const filteredStyliticsProductTabListReducer = createFilteredReducer(
  StyliticsProductTabListReducer,
  STYLITICS_PRODUCT_TAB_LIST_REDUCER_KEY
);

/**
 * TODO: This reducer is fragile. We should handle page
 * name changes in a cleaner way.
 *
 * @see RouteTracker.js
 */
function pageNameReducer(state = {}, action) {
  switch (action.type) {
    case TRACK_PAGE_VIEW: {
      const { props } = action.payload || {};
      const { pageData = {} } = (props && props.initialProps && props.initialProps.pageProps) || {};
      return { ...state, ...pageData };
    }
    case UPDATE_PAGE_DATA: {
      return action.payload;
    }
    default:
      return state;
  }
}

export default combineReducers({
  pageData: pageNameReducer,
  [GIFT_CARD_BALANCE_REDUCER_KEY]: giftCardBalanceReducer,
  [SOCIAL_REDUCER_KEY]: SocialReducer,
  [APICONFIG_REDUCER_KEY]: filteredAppConfigReducer,
  [APPLY_PLCC_REDUCER_KEY]: ApplyCardReducer,
  [SESSIONCONFIG_REDUCER_KEY]: SessionConfigReducer,
  [NAVIGATE_XHR]: NavigateXHRReducer,
  [HEADER_REDUCER_KEY]: HeaderReducer,
  [TOAST_REDUCER_KEY]: ToastMessageReducer,
  [FOOTER_REDUCER_KEY]: FooterReducer,
  [LABEL_REDUCER_KEY]: LabelReducer,
  [SEO_DATA_REDUCER_KEY]: SEODataReducer,
  [LAYOUT_REDUCER_KEY]: LayoutReducer,
  [MODULES_REDUCER_KEY]: ModulesReducer,
  [LOADER_REDUCER_KEY]: LoaderReducer,
  [SEARCH_REDUCER_KEY]: SearchBarReducer,
  [LOGINPAGE_REDUCER_KEY]: LoginPageReducer,
  [FORGOTPASSWORD_REDUCER_KEY]: ForgotPasswordReducer,
  [ADDRESSBOOK_REDUCER_KEY]: AddressBookReducer,
  [ADDRESS_VERIFICATION_REDUCER_KEY]: AddressVerificationReducer,
  [PAYMENT_REDUCER_KEY]: PaymentReducer,
  [ADDEDITADDRESS_REDUCER_KEY]: AddEditAddressReducer,
  form: reduxFormReducer,
  [EMAIL_SIGNUP_REDUCER_KEY]: EmailSignupReducers,
  [COUNTRY_SELECTOR_REDUCER_KEY]: CountrySelectorReducer,
  [SMS_SIGNUP_REDUCER_KEY]: SmsSignupReducers,
  [PLCC_OFFER_REDUCER_KEY]: PlccOfferModalReducer,
  [ADDEDITCREDITCARD_REDUCER_KEY]: AddEditCreditCardReducer,
  [ADD_GIFT_CARD_REDUCER_KEY]: AddGiftCardReducer,
  [ADDED_TO_BAG_REDUCER_KEY]: AddedToBagReducer,
  [ACCOUNT_REDUCER_KEY]: AccountReducer,
  [CARTITEMTILE_REDUCER_KEY]: CartItemTile,
  [CARTPAGE_REDUCER_KEY]: CartPage,
  [CHECKOUT_REDUCER_KEY]: CheckoutReducer,
  [OVERLAY_MODAL_REDUCER_KEY]: OverlayModalReducer,
  [NAVIGATION_REDUCER_KEY]: NavigationReducer,
  [PRODUCT_LISTING_REDUCER_KEY]: ProductListingReducer,
  [CREATE_ACCOUNT_REDUCER_KEY]: CreateAccountReducer,
  [BONUS_POINTS_DAYS_REDUCER_KEY]: BonusPointsDaysReducer,
  [COUPON_REDUCER_KEY]: CouponsReducer,
  [AIRMILES_BANNER_REDUCER_KEY]: AirmilesBannerReducer,
  [ACCOUNTHEADER_REDUCER_KEY]: AccountHeaderReducer,
  [POINTS_HISTORY_REDUCER_KEY]: PointsHistoryReducer,
  [EARNEXTRAPOINTS_REDUCER_KEY]: EarnExtraPointsReducer,
  [ORDERDETAILS_REDUCER_KEY]: OrderDetailsDataReducer,
  [RESET_PASSWORD_REDUCER_KEY]: ResetPasswordReducer,
  [CHANGE_PASSWORD_REDUCER_KEY]: ChangePasswordReducer,
  [UPDATE_PROFILE_REDUCER_KEY]: UpdateProfileReducer,
  [MY_PROFILE_REDUCER_KEY]: MyProfileReducer,
  [USER_REDUCER_KEY]: UserReducer,
  [DEVICE_INFO_REDUCER_KEY]: DeviceInfoReducer,
  [TRACK_ORDER_REDUCER_KEY]: TrackOrderReducer,
  [MAILING_ADDRESS_REDUCER_KEY]: AddMailingAddressReducer,
  [PRODUCT_DETAIL_REDUCER_KEY]: ProductDetailReducer,
  [QUICK_VIEW_REDUCER_KEY]: QuickViewReducer,
  [OUTFIT_DETAILS_REDUCER_KEY]: OutfitDetailReducer,
  [APPLY_NOW_MODAL_REDUCER_KEY]: ApplyNowModalPLCCReducer,
  [QMRECOMMENDATION_MODAL_REDUCER_KEY]: QMRecommendationModalReducer,
  [PRODUCT_TAB_LIST_REDUCER_KEY]: filteredProductTabListReducer,
  [STYLITICS_PRODUCT_TAB_LIST_REDUCER_KEY]: filteredStyliticsProductTabListReducer,
  [BIRTHDAY_SAVING_LIST_REDUCER_KEY]: BirthdaySavingsListReducer,
  [PICKUP_MODAL_REDUCER_KEY]: PickupModalReducer,
  [PRODUCT_PICKUP_REDUCER_KEY]: ProductPickupReducer,
  [RECOMMENDATIONS_REDUCER_KEY]: RecommendationsReducer,
  [STORE_LOCATOR_REDUCER_KEY]: StoreLocatorReducer,
  [SLP_PAGE_REDUCER_KEY]: SearchPageReducer,
  [STORE_DETAIL_REDUCER_KEY]: StoreDetailReducer,
  [POINTS_CLAIM_REDUCER_KEY]: PointsClaimReducer,
  [CONFIRMATION_REDUCER_KEY]: orderConfirmationReducer,
  [ORDERS_REDUCER_KEY]: OrdersReducer,
  [FAVORITES_REDUCER_KEY]: FavoriteReducer,
  [EXTRA_POINTS_REDUCER_KEY]: ExtraPointsReducer,
  [STORES_INTL_REDUCER_KEY]: StoresInternationalReducer,
  [STORE_LIST_REDUCER_KEY]: StoreListReducer,
  [MY_PREFERENCE_REDUCER_KEY]: MyPreferenceSubscriptionReducer,
  [BUNDLEPRODUCT_REDUCER_KEY]: BundleProductReducer,
  [ANALYTICS_DATA_KEY]: AnalyticsReducer,
  [SUB_NAVIGATION_REDUCER_KEY]: SubNavigationReducer,
  [SITEMAP_REDUCER_KEY]: SiteMapReducer,
  [AB_TEST_STORE_PATTERN]: AbTestReducer,
  [EDD_REDUCER_KEY]: EddReducer,
  [REDIRECT_REDUCER_KEY]: RedirectPageReducer,
  [LAST_UPDATED_REDUCER_KEY]: LastUpdatedReducer,
  [CUSTOMER_HELP_REDUCER_KEY]: CustomerHelpReducer,
  [EMAIL_SMS_SIGNUP_REDUCER_KEY]: EmailSmsSignupReducer,
  [MERGE_ACCOUNT_REDUCER_KEY]: mergeAccountReducer,
  [MERGE_ACCOUNT_OTP_REDUCER_KEY]: mergeAccountOtpReducer,
  [MERGE_ACCOUNT_ACCOUNT_CONFIRMATION_REDUCER_KEY]: accountConfirmationReducer,
  [MERGE_ACCOUNT_EMAIL_ADDRESS_CONFIRMATION_REDUCER_KEY]: MergeAccountEmailAddressConfirmation,
});
