/* eslint-disable */
// 9fbef606107a605d69c0edbcd8029e5d
/** TO CONVERT THIS FILE TO THE MINIFIED FILE -
 * Step 1: https://es6console.com/ (select presets -> es2015 and stage2)
 * Step 2: https://skalman.github.io/UglifyJS-online/
 */
function isIEBrowser() {
  var ua = window.navigator && window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older, return version number
    return true;
  }
  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    return true;
  }
  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    return true;
  }
  return false;
}

const removeExtension = (path) => (path ? path.replace(/\.\w{3}$/, '') : '');

if (!isIEBrowser()) {
  (function () {
    // variable declarations
    window.isUnbxdRequestTriggered = true;
    const url = window.location;
    let bucketingConfig;
    const PRODUCTS_PER_LOAD = 20;
    const breadCrumbs = [];
    const isUnbxdRequestTriggered = true;
    const facetCallResponseState = {};
    const FACETS_FIELD_KEY = {
      color: 'tcpcolor_ufilter',
      size: 'v_tcpsize_ufilter',
      age: 'age_group_ufilter',
      price: 'unbxd_price_range_ufilter',
      sort: 'sort',
      unbxdDisplayName: 'unbxddisplayname',
      aux_color: 'auxdescription_ufilter',
      aux_color_unbxd: 'auxdescription_uFilter',
      l1category: 'l1category',
      display: 'display_group_uFilter',
    };
    const FACETS_OPTIONS = {
      lowPriceProducts: '$10 and under',
    };

    //////////////////////////////////
    // Common functions
    const matchPath = function (url, param) {
      if (param === '/search' && url.indexOf(param) !== -1) {
        const searchTerm = url.split(`${param}/`);
        return {
          searchTerm: searchTerm[1] || url,
        };
      }
      if (
        (param === '/c?cid=' && url.indexOf(param) !== -1) ||
        (param === '/c/' && url.indexOf(param) !== -1)
      ) {
        const urlWithCat = url.split(param)[1];
        return {
          listingKey: urlWithCat,
        };
      }
      return url;
    };

    const matchValue = (isSearchPage, location) => {
      const categoryParam = '/c/';
      const params = isSearchPage ? '/search' : categoryParam;
      return matchPath(location, params);
    };

    const isMatchSearch = (isSearchPage, searchTerm, location) => {
      return matchValue(isSearchPage, location.pathname);
    };

    const getPlpBucketDetails = ({
      bucketingSeqConfigArg,
      categoryNameList,
      categoryPathMap,
      catNameL3,
      isUnbxdSequencing,
      categoryIdArg,
      clickedL2,
      isSearchPage,
    }) => {
      let requiredChildren;

      const bucketingSeqConfig = bucketingSeqConfigArg;
      let categoryId = categoryIdArg;
      if (operatorInstance.shouldApplyUnbxdLogic) {
        bucketingSeqConfig.requiredChildren = bucketingConfig.availableL3;
        bucketingSeqConfig.bucketingRequired = bucketingConfig.bucketingSeqScenario;
        bucketingSeqConfig.desiredL3 =
          !catNameL3 && bucketingConfig.L3Left.length
            ? bucketingConfig.L3Left[0] && bucketingConfig.L3Left[0].title
            : catNameL3;
        categoryId = categoryPathMap;
        if (isUnbxdSequencing) bucketingSeqConfig.bucketingSeq = 'A/B-Test';
      } else {
        if (categoryNameList[1]) {
          requiredChildren = categoryNavInfo[1].subCategories;
          const isBuktSeqReq = requiredChildren && requiredChildren.length;
          bucketingSeqConfig.requiredChildren = requiredChildren;

          if (isUnbxdSequencing) {
            if (!isBuktSeqReq) {
              bucketingSeqConfig.bucketingSeq = 'A/B-Test';
            }
          } else if (isBuktSeqReq) {
            const catId = categoryNameList && categoryNameList.length ? clickedL2.categoryId : null;
            bucketingSeqConfig.bucketingSeq = `sort_${catId} asc,pop_score desc`;
          }
        }
        categoryId = categoryNameList
          ? categoryNameList.map((item) => item.categoryId).join('>')
          : '';
      }
      return { bucketingSeqConfig, categoryId };
    };

    const routingInfoStoreView = {
      getOriginImgHostSetting: () => {
        return 'https://test4.childrensplace.com';
      },
    };

    const getProductImagePath = (id, excludeExtension, imageExtension) => {
      var imageName = (id && id.split('_')) || [];
      var imagePath = imageName[0];
      var extension = imageExtension ? `.${imageExtension}` : '.jpg';

      return {
        125: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        380: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        500: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        900: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
      };
    };
    const getImgPath = (id, excludeExtension, imageExtension) => {
      return {
        colorSwatch: null,
        productImages: getProductImagePath(id, excludeExtension, imageExtension),
      };
    };
    const getFacetSwatchImgPath = (id) => {
      const imgHostDomain = routingInfoStoreView.getOriginImgHostSetting();
      return `${imgHostDomain}/wcsstore/GlobalSAS/images/tcp/category/color-swatches/${id}.gif`;
    };
    const getProductsListingInfo = ({
      filtersAndSort,
      pageNumber,
      location,
      startProductCount,
      numberOfProducts,
      categoryPathMap,
      catNameL3,
      isLazyLoading,
      facetCallResponseState,
    }) => {
      const isSearchPage = true;
      const isUnbxdSequencing = true;
      const searchTerm = location.pathname.substr(11);
      const match = isMatchSearch(isSearchPage, searchTerm, location);
      const categoryNameList = typeof categoryNavInfo !== 'undefined' ? categoryNavInfo : '';
      const breadCrumb = breadCrumbs;
      const excludeBage = false;
      let categoryId;
      const clickedL2 = categoryNameList[1];
      const clickedl1 = categoryNameList[0];
      const bucketingSeqConfig = {};
      let filteredBucketingSeqConfig = {};

      if (operatorInstance.shouldApplyUnbxdLogic) {
        bucketingSeqConfig.desiredL2 = clickedL2 ? clickedL2.title : '';
        bucketingSeqConfig.desiredl1 = clickedl1 ? clickedl1.title : '';
      }

      filteredBucketingSeqConfig = getPlpBucketDetails({
        bucketingSeqConfigArg: bucketingSeqConfig,
        categoryNameList,
        categoryPathMap,
        catNameL3,
        isUnbxdSequencing,
        categoryIdArg: window.categoryId,
        clickedL2,
      });

      const abd = {
        seoKeywordOrCategoryIdOrSearchTerm: match && match.searchTerm,
        isSearch: isSearchPage,
        filtersAndSort,
        pageNumber,
        getImgPath: getImgPath,
        categoryId: filteredBucketingSeqConfig.categoryId,
        breadCrumbs: breadCrumb,
        bucketingSeqConfig: filteredBucketingSeqConfig.bucketingSeqConfig,
        getFacetSwatchImgPath: getFacetSwatchImgPath,
        isUnbxdSequencing,
        excludeBadge: excludeBage,
        startProductCount,
        numberOfProducts,
        cacheFiltersAndCount: false,
        extraParams: '',
        shouldApplyUnbxdLogic: operatorInstance.shouldApplyUnbxdLogic,
        categoryNameList,
        isLazyLoading,
        facetCallResponseState,
      };
      return abd;
    };

    // Common functions ends
    //////////////////////////////////

    //////////////////////////////////
    // Class Products Operator
    class ProductsOperator {
      constructor() {
        this.resetBucketingConfig();
        this.shouldApplyUnbxdLogic = true;
        this.bindAllClassMethodsToThis(this);
      }

      bindAllClassMethodsToThis(obj, namePrefix = '', isExclude = false) {
        const prototype = Object.getPrototypeOf(obj);
        for (let name of Object.getOwnPropertyNames(prototype)) {
          const descriptor = Object.getOwnPropertyDescriptor(prototype, name);
          const isGetter = descriptor && typeof descriptor.get === 'function';
          if (isGetter) continue;
          if (
            typeof prototype[name] === 'function' && name !== 'constructor' && isExclude
              ? !name.startsWith(namePrefix)
              : name.startsWith(namePrefix)
          ) {
            obj[name] = prototype[name].bind(obj);
          }
        }
      }

      get bucketingLogic() {
        return new BucketingBL();
      }

      resetBucketingConfig = () => {
        bucketingConfig = {
          start: 0,
          productsToFetchPerLoad: PRODUCTS_PER_LOAD,
          L3Left: [],
          currL2NameList: [],
          bucketingSeqScenario: false,
          availableL3: [],
        };
      };

      getNavAttributes = (navTree, breadcrumbCatIds, attribute) => {
        let extractCat = {};
        breadcrumbCatIds.forEach((breadCrumbId, index) => {
          if (index === 0) {
            extractCat = getCatById(navTree, breadCrumbId);
          } else if (index === 1 && extractCat.subCategories && extractCat.subCategories) {
            Object.values(extractCat.subCategories).some((l2Category) => {
              const extractL2Cat = getCatById(l2Category.items, breadCrumbId);
              if (extractL2Cat) {
                extractCat = extractL2Cat;
                return true;
              }
              return false;
            });
          } else if (index === 2) {
            extractCat = getCatById(extractCat.subCategories, breadCrumbId);
          }
        });
        return extractCat && extractCat.categoryContent
          ? extractCat.categoryContent[attribute]
          : '';
      };

      fetchFiltersAndCount = (filters, categoryId, categoryNameList, location, pageNumber) => {
        const extraParams = {
          'facet.multilevel': 'categoryPath',
          'f.categoryPath.nameId': true,
          'f.categoryPath.max.depth': 4,
        };
        return {
          seoKeywordOrCategoryIdOrSearchTerm: '',
          isSearch: '',
          filtersAndSort: filters,
          pageNumber,
          getImgPath: '',
          categoryId,
          breadCrumbs: '',
          bucketingSeqConfig: '',
          getFacetSwatchImgPath: getFacetSwatchImgPath,
          isUnbxdSequencing: '',
          excludeBadge: '',
          startProductCount: 0,
          numberOfProducts: 0,
          cacheFiltersAndCount: true,
          extraParams,
          shouldApplyUnbxdLogic: operatorInstance.shouldApplyUnbxdLogic,
          hasShortImage: false,
          categoryNameList,
          location,
          isFetchFiltersAndCountReq: true,
        };
      };

      getPlpCutomizersFromUrlQueryString(urlQueryString) {
        const queryParams = urlQueryString && urlQueryString.split('?')[1];
        const returnVal = {};
        const queryParamsArray = (queryParams && queryParams.split('&')) || [];
        queryParamsArray.forEach((queryParam) => {
          const qsArray = queryParam.split('=');
          const value = decodeURIComponent(qsArray[1]);
          const key = decodeURIComponent(qsArray[0]);
          returnVal[key] =
            key && (key.toLowerCase() === FACETS_FIELD_KEY.sort ? value : value.split(','));
        });
        return returnVal;
      }

      getProductListingBucketedData(
        location,
        sortBySelected,
        filterAndSortParam = {},
        pageNumber = ''
      ) {
        this.resetBucketingConfig();
        let filtersAndSort;
        let navigationTree;
        let categoryNameList;
        let clickedL2;
        let sortingAvailable;
        let isOutfitPage;
        ({
          filtersAndSort,
          navigationTree,
          categoryNameList,
          clickedL2,
          sortingAvailable,
          isOutfitPage,
          ...bucketingConfig
        } = this.bucketingLogic.doBucketingLogic(
          location,
          bucketingConfig,
          sortBySelected,
          filterAndSortParam,
          this.getPlpCutomizersFromUrlQueryString
        ));
        const requiredChildren = clickedL2 && categoryNavInfo[1].subCategories;
        const isBuktSeqReq = requiredChildren && requiredChildren.length;
        const displayableCatCount = isBuktSeqReq ? visibleCategoryCount(requiredChildren) : 0;
        if (
          operatorInstance.shouldApplyUnbxdLogic &&
          !sortingAvailable &&
          displayableCatCount &&
          !categoryNameList[2] &&
          !isOutfitPage
        ) {
          bucketingConfig.L3Left = [...requiredChildren];
          bucketingConfig.availableL3 = [...requiredChildren];
          bucketingConfig.bucketingSeqScenario = true;
          const categoryPathMapL2 = bucketingConfig.currL2NameList
            ? bucketingConfig.currL2NameList.map((item) => item.categoryId).join('>')
            : '';
          return this.fetchFiltersAndCount(
            filtersAndSort,
            categoryPathMapL2,
            categoryNameList,
            location,
            pageNumber
          );
        }
        return this.getProductsListingForUrlLocation(
          location,
          filterAndSortParam,
          (sortBySelected = ''),
          isBuktSeqReq && !categoryNavInfo[2] && !sortingAvailable
        );
      }

      getProductsListingForUrlLocation(location, sortParam, sortBySelected, isBuktSeqReq) {
        const pageNumber = !Number.isNaN(location.pageNumber)
          ? Math.max(parseInt(location.pageNumber, 10))
          : 1;
        const filtersAndSort = sortBySelected
          ? sortParam
          : this.getPlpCutomizersFromUrlQueryString(location.search);

        if (!isBuktSeqReq) {
          operatorInstance.shouldApplyUnbxdLogic = false;
        }
        return getProductsListingInfo({ filtersAndSort, pageNumber, location });
      }

      isExportBadge = (breadcrumbCatIds, navigationTree) => {
        return breadcrumbCatIds && breadcrumbCatIds.length
          ? this.getNavAttributes(navigationTree, breadcrumbCatIds, 'excludeAttribute')
          : '';
      };

      shortImage = (isSearchPage, breadcrumbCatIds, navigationTree) => {
        return '';
      };

      getSeoForSearch = (isSearchPage, match) => {
        return isSearchPage ? match : getSeoKeywordOrCategoryIdOrSearchTerm(match);
      };

      getBucketingSeqConfig = (isSearchPage, bucketingSeqConfig) => {
        return isSearchPage ? {} : bucketingSeqConfig;
      };

      getProductsListingFilters({ state, asPath, pageNumber, formData, location }) {
        const filtersAndSort = formData || this.getPlpCutomizersFromUrlQueryString(asPath);
        return getProductsListingInfo({
          filtersAndSort,
          pageNumber,
          location,
        });
      }

      updateBucketingConfig = (res) => {
        const updatedBucketingConfig = this.bucketingLogic.updateBucketingParamters(
          res,
          bucketingConfig
        );
        ({ ...bucketingConfig } = { ...updatedBucketingConfig });
        if (bucketingConfig.availableL3.length) {
          res.totalProductsCount = getTotalProductsCount(bucketingConfig.availableL3);
        }
      };
    }
    // Products Operator Class ends
    //////////////////////////////////

    //////////////////////////////////
    // Class Products abstractor
    class ProductsAbstractor {
      constructor(reqObj) {
        this.reqObj = reqObj;
      }
      getModifiedQueryString(facetValue, query) {
        return facetValue.length > 0 ? (query ? '&filter=' : '') + facetValue.join(' OR ') : '';
      }
      isUnbxdFacetKey(key) {
        return (
          key.toLowerCase() !== FACETS_FIELD_KEY.unbxdDisplayName &&
          key.toLowerCase() !== FACETS_FIELD_KEY.sort &&
          key !== FACETS_FIELD_KEY.l1category
        );
      }
      extractFilters(filtersAndSort) {
        const filterQuery = {};
        let query = '';
        const facetKeys = Object.keys(filtersAndSort);
        facetKeys.forEach((facetKey) => {
          let facetValue = filtersAndSort[facetKey];
          if (
            this.isUnbxdFacetKey(facetKey) &&
            facetValue &&
            facetValue.length > 0 &&
            facetKey.indexOf('uFilter') > -1
          ) {
            facetValue = facetValue.map((facet) => {
              const encodedFacet = encodeURIComponent(facet);
              const encodedFacetWithQuote = encodeURIComponent(`"${facet}"`);
              return `${facetKey}:"${encodedFacet}"`;
            });

            query += this.getModifiedQueryString(facetValue, query);
          } else if (facetKey === 'bopisStoreId') {
            query += this.getModifiedQueryString(facetValue, query);
          }
        });

        if (query !== '') filterQuery.filter = query;
        return filterQuery;
      }
      isNoUnbxdLogic(shouldApplyUnbxdLogic, isUnbxdSequencing) {
        return !shouldApplyUnbxdLogic && !isUnbxdSequencing;
      }
      isNoBucketing(bucketingSeqConfig) {
        return (
          bucketingSeqConfig &&
          bucketingSeqConfig.bucketingSeq &&
          bucketingSeqConfig.requiredChildren.length
        );
      }
      getProductAttributes() {
        return {
          merchant: 'TCPMerchantTagUSStore',
          sizes: 'TCPSizeUSStore',
          swatches: 'TCPSwatchesUSStore',
          onlineOnly: 'TCPWebOnlyFlagUSStore',
          clearance: 'TCPProductIndUSStore',
          inventory: 'TCPInventoryFlagUSStore',
          glowInTheDark: 'TCPGlowInDarkUSStore',
          limitedQuantity: 'TCPInventoryMessageUSStore',
          extendedSize: 'TCPFitMessageUSStore',
          onModelAltImages: 'TCPMarketingText1USStore',
          bossProductDisabled: 'TcpBossProductDisabled',
          bossCategoryDisabled: 'TcpBossCategoryDisabled',
          videoUrl: 'TCPMarketingText2USStore',
          matchingCategory: 'TCPProductFlagUSStore',
          matchingFamily: 'TCPMatchingFamilyUSStore',
          keepAlive: 'TCPOutOfStockFlagUSStore',
          bundleChecklist: 'TCPKitUSStore',
          bundleGrouping: 'TCPGroupingUSStore',
        };
      }
      indexBasedOnShopByColor(
        isShopByColorFilter,
        index,
        data,
        numberOfProducts,
        filtersAndSort,
        val
      ) {
        return isShopByColorFilter
          ? index % 2 === 0
          : index % 2 === 0 &&
              (data[index + 1] !== numberOfProducts || filtersAndSort.includes(val));
      }
      getDisplayName(keyValue, data, index) {
        return keyValue && keyValue.length > 1 ? keyValue[1] : data[index];
      }
      getFacetSwatchImgPath(id) {
        return `/wcsstore/GlobalSAS/images/tcp/category/color-swatches/${id}.gif`;
      }
      isLowPriceProduct(val) {
        return val && val.toLowerCase() === FACETS_OPTIONS.lowPriceProducts;
      }
      getFacetsMappingFromAPIData(
        filterMap,
        getFacetSwatchImgPath,
        numberOfProducts,
        filtersAndSort = []
      ) {
        const facet = [];
        if (filterMap && filterMap.values) {
          filterMap.values.forEach((val, index, data) => {
            const facetType = filterMap.facetName;
            const isShopByColorFilter = facetType === FACETS_FIELD_KEY.aux_color_unbxd;
            /*
             ** By deafult the condition is index % 2 === 0 for all filters/facets(e.g shop by colors, SIZE, gender price).
             ** For filters other than shop by color we need filters basis on the products we have in the result set to achive the same.
             ** we added condition ((data[index + 1] !== numberOfProducts) || filtersAndSort.includes(val)) with index % 2 === 0.
             */
            const condition = this.indexBasedOnShopByColor(
              isShopByColorFilter,
              index,
              data,
              numberOfProducts,
              filtersAndSort,
              val
            );
            if (condition) {
              let keyValue;
              switch (facetType.toLowerCase()) {
                case FACETS_FIELD_KEY.size:
                case FACETS_FIELD_KEY.age:
                  keyValue = data[index].split('_');
                  facet.push({
                    displayName: this.getDisplayName(keyValue, data, index),
                    id: data[index],
                    facetName: facetType,
                  });
                  break;
                case FACETS_FIELD_KEY.color:
                  facet.push({
                    displayName: data[index],
                    id: data[index],
                    imagePath: this.getFacetSwatchImgPath(data[index]),
                    facetName: facetType,
                  });
                  break;
                case FACETS_FIELD_KEY.aux_color:
                  facet.push({
                    displayName: data[index],
                    id: data[index],
                    imagePath: this.getFacetSwatchImgPath(
                      data[index].replace(/ /g, '_').toLowerCase()
                    ),
                    facetName: facetType,
                  });
                  break;
                case FACETS_FIELD_KEY.price:
                  keyValue = {
                    displayName: val,
                    id: val,
                    facetName: facetType,
                  };
                  if (this.isLowPriceProduct(val)) {
                    facet.unshift(keyValue);
                  } else {
                    facet.push(keyValue);
                  }
                  break;
                default:
                  facet.push({
                    displayName: data[index],
                    id: data[index],
                    facetName: facetType,
                  });
              }
            }
          });
        }
        return facet;
      }
      getFacetsAPIData(facets, getFacetSwatchImgPath, numberOfProducts, filtersAndSort) {
        facets.sort((a, b) => {
          // Sort facets on position field value
          const pos = b.position > a.position ? -1 : 0;
          return a.position > b.position ? 1 : pos;
        });
        const filters = {};
        facets.forEach((facet) => {
          filters[facet.facetName] = getFacetsMappingFromAPIData(
            facet,
            getFacetSwatchImgPath,
            numberOfProducts,
            filtersAndSort[facet.facetName]
          );
        });
        return filters;
      }
      getUnbxdDisplayName(facets) {
        var facetsName = {};
        facets.forEach((facet) => {
          facetsName[facet.facetName] = facet.displayName;
        });
        return facetsName;
      }
      getFiltersAfterProcessing(
        facetsRes,
        numberOfProducts,
        getFacetSwatchImgPath,
        filtersAndSort,
        l1category
      ) {
        let filters = {};
        // varruct facets from the api response
        const facetsList =
          facetsRes && facetsRes.text && facetsRes.text.list && facetsRes.text.list;
        if (facetsList) {
          const facets = this.getFacetsAPIData(
            facetsList,
            getFacetSwatchImgPath,
            numberOfProducts,
            filtersAndSort
          );
          const unbxdDisplayName = this.getUnbxdDisplayName(facetsList);
          filters = {
            ...facets,
            unbxdDisplayName,
            l1category,
          };
        }
        return filters;
      }
      attributeListMaker(attributes) {
        return (
          attributes &&
          attributes.split(`;`).map((attribute) => {
            var regexUrl = /((http|https):\/\/)?(([\w.-]*)\.([\w])).*/g;
            var isUrl = regexUrl.test(attribute);
            var match = attribute.match(regexUrl);
            var url = match && match[0].split('|');
            var attAndValue = attribute.split(`:`);
            return { identifier: attAndValue[0], value: isUrl ? url : attAndValue[1] };
          })
        );
      }
      colorSwatchFilter(colorSwatchesArray, id, color) {
        return colorSwatchesArray.filter((el) => {
          const duplicateSwatch = `${id}#${color}`;
          return el !== duplicateSwatch;
        });
      }
      convertToColorArray(colorSwatches, id, color) {
        if (!colorSwatches) return [];
        return colorSwatches === 'ImagePath'
          ? colorSwatches
          : this.colorSwatchFilter(colorSwatches.split('|'), id, color);
      }
      parseBoolean(bool) {
        return bool === true || bool === '1' || (bool || '').toUpperCase() === 'TRUE';
      }
      isGiftCard(product) {
        return !!(
          product &&
          ((product.style_partno && product.style_partno.toLowerCase() === 'giftcardbundle') ||
            product.giftcard === '1')
        );
      }
      isBopisProduct(isUSStore, product) {
        let isOnlineOnly;
        if (isUSStore) {
          isOnlineOnly =
            (product &&
              product.TCPWebOnlyFlagUSStore &&
              this.parseBoolean(product.TCPWebOnlyFlagUSStore)) ||
            false;
        } else {
          isOnlineOnly =
            (product &&
              product.TCPWebOnlyFlagCanadaStore &&
              this.parseBoolean(product.TCPWebOnlyFlagCanadaStore)) ||
            false;
        }
        return !isOnlineOnly;
      }
      isBopisEligibleProduct(isUSStore, swatchOfAvailableProduct, product) {
        return this.isBopisProduct(isUSStore, swatchOfAvailableProduct) && this.isGiftCard(product);
      }
      extractAttributeValue(item, attribute) {
        try {
          if (item.list_of_attributes) {
            var currItm = item.list_of_attributes;
            var itm = Array.isArray(currItm) ? currItm : attributeListMaker(currItm);
            var AttrIdentifier = itm.find((att) => att.identifier === attribute);
            return AttrIdentifier && AttrIdentifier.value;
          }
          if (item[attribute]) {
            return item[attribute];
          }
          return null;
        } catch (ex) {
          return '';
        }
      }
      isBossProductDisabled(product) {
        return (
          this.extractAttributeValue(product, this.getProductAttributes().bossProductDisabled) || 0
        );
      }
      isBopisProductDisabled(product) {
        return (
          this.extractAttributeValue(product, this.getProductAttributes().bossCategoryDisabled) || 0
        );
      }
      altImageArray(imagename, altImg) {
        try {
          const altImges = JSON.parse(altImg);
          return altImges[imagename].split(',').filter((img) => img);
        } catch (error) {
          return [];
        }
      }
      parseAltImagesForColor(imageBasePath, hasShortImage, altImgs, productImage) {
        try {
          const altImages = this.altImageArray(imageBasePath, altImgs);
          const shortImage = [];
          const normalImage = [];
          let availableImages;
          let imageExtension = '';

          if (productImage) {
            const productImageExt = productImage.split('.');
            imageExtension = productImageExt[productImageExt.length - 1];
          }

          altImages.forEach((image) => {
            if (image.indexOf('-s') < 0) {
              normalImage.push(image);
            } else {
              shortImage.push(image);
            }
          });

          if (hasShortImage && shortImage.length) {
            availableImages = [...shortImage, ...normalImage];
          } else {
            availableImages = [imageBasePath, ...normalImage];
          }

          return availableImages.map((img) => {
            const hasExtension = img.indexOf('.jpg') !== -1 || img.indexOf('.mp4') !== -1;
            const { productImages } = getImgPath(img, hasExtension, imageExtension);

            const isOnModalImage = parseInt(img.split('-')[1], 10) > 5;

            return {
              isOnModalImage,
              iconSizeImageUrl: productImages[125],
              listingSizeImageUrl: productImages[380],
              regularSizeImageUrl: productImages[500],
              bigSizeImageUrl: productImages[900],
              superSizeImageUrl: productImages[900],
            };
          });
        } catch (error) {
          return [];
        }
      }
      extractExtraImages(
        rawColors,
        altImgs,
        uniqueId,
        defaultColor,
        isGiftCard,
        hasShortImage,
        productImage
      ) {
        const colorsImageMap = {};
        let imageExtension = '';
        if (productImage) {
          const productImageExt = productImage.split('.');
          imageExtension = productImageExt[productImageExt.length - 1];
        }

        try {
          if (rawColors && rawColors !== '') {
            let colors = [];

            if (isGiftCard) {
              colors.push(rawColors);
            } else {
              colors = rawColors.split('|');
            }
            for (let color of colors) {
              let colorName = color.split('#')[1];
              let imageBasePath = color.split('#')[0];
              if (!colorName) {
                colorName = defaultColor;
                imageBasePath = uniqueId;
              }

              const { productImages } = getImgPath(imageBasePath, '', imageExtension);

              colorsImageMap[colorName] = {
                basicImageUrl: productImages[500],
                extraImages: this.parseAltImagesForColor(
                  imageBasePath,
                  hasShortImage,
                  altImgs,
                  productImage
                ),
              };
            }
          } else {
            const { productImages } = getImgPath(uniqueId, '', imageExtension);
            colorsImageMap[defaultColor] = {
              basicImageUrl: productImages[500],
              extraImages: this.parseAltImagesForColor(
                uniqueId,
                hasShortImage,
                altImgs,
                productImage
              ),
            };
          }
        } catch (error) {
          console.log(error);
        }
        return colorsImageMap;
      }
      numericStringToBool(str) {
        return !!+str;
      }
      isBossProduct(bossDisabledFlags) {
        const { bossCategoryDisabled, bossProductDisabled } = bossDisabledFlags;
        return !(
          this.numericStringToBool(bossCategoryDisabled) ||
          this.numericStringToBool(bossProductDisabled)
        );
      }
      getBundleBadge(hidePdpBadge, product, siteAttributes) {
        return !hidePdpBadge
          ? this.extractAttributeValue(product, siteAttributes.bundleChecklist)
          : '';
      }
      isMatchingFamily(matchingFamily, excludeBadge, siteAttributes) {
        return matchingFamily && excludeBadge !== siteAttributes.matchingFamily;
      }
      getAllLangvars(categoryType) {
        return Object.keys(categoryType).map((key) => categoryType[key]);
      }
      getClearanceString(categoryType) {
        return this.getAllLangvars(LANG_STRINGS.PRODUCTS.ATTRIBUTES[categoryType]);
      }
      isOnlineOrClearing(isOnlineOnly, categoryType) {
        return isOnlineOnly && !this.getClearanceString('ONLINE_ONLY').includes(categoryType);
      }
      checkIfClearance(clearanceOrNewArrival, categoryType) {
        return (
          clearanceOrNewArrival === 'Clearance' &&
          !this.getClearanceString('CLEARANCE').includes(categoryType)
        );
      }
      extractPrioritizedBadge(product, siteAttributes, categoryType, excludeBadge, hidePdpBadge) {
        const matchingCategory = this.extractAttributeValue(
          product,
          siteAttributes.matchingCategory
        );
        const matchingFamily = this.extractAttributeValue(product, siteAttributes.matchingFamily);
        const isGlowInTheDark = !!this.extractAttributeValue(product, siteAttributes.glowInTheDark);
        const isLimitedQuantity =
          this.extractAttributeValue(product, siteAttributes.limitedQuantity) ===
          'limited quantities';
        const isOnlineOnly = !!+this.extractAttributeValue(product, siteAttributes.onlineOnly);
        const clearanceOrNewArrival = this.extractAttributeValue(product, siteAttributes.clearance);
        var bundleBadge = this.getBundleBadge(hidePdpBadge, product, siteAttributes);
        var badges = {};

        if (this.isMatchingFamily(matchingFamily, excludeBadge, siteAttributes.matchingFamily)) {
          badges.matchBadge = matchingFamily;
        }

        if (bundleBadge) {
          badges.defaultBadge = bundleBadge;
        } else if (matchingCategory) {
          badges.defaultBadge = matchingCategory;
        } else if (isGlowInTheDark) {
          badges.defaultBadge = 'GLOW-IN-THE-DARK';
        } else if (isLimitedQuantity) {
          badges.defaultBadge = 'JUST A FEW LEFT!';
        } else if (this.isOnlineOrClearing(isOnlineOnly, categoryType)) {
          badges.defaultBadge = 'ONLINE EXCLUSIVE';
        } else if (this.checkIfClearance(clearanceOrNewArrival, categoryType)) {
          badges.defaultBadge = 'CLEARANCE';
        } else if (
          clearanceOrNewArrival === 'New Arrivals' &&
          !this.getClearanceString('NEW_ARRIVALS').includes(categoryType)
        ) {
          badges.defaultBadge = 'NEW!';
        }
        return badges;
      }
      getColorsMap({
        uniqueId,
        product,
        attributesNames,
        categoryType,
        excludeBadge,
        isBOPIS,
        bossDisabledFlags,
        defaultColor,
        isBundleProduct,
      }) {
        return [
          {
            colorProductId: uniqueId,
            imageName: product.imagename,
            productImage: product.productimage,
            miscInfo: {
              isClearance: this.extractAttributeValue(product, attributesNames.clearance),
              isBopisEligible: isBOPIS && this.isGiftCard(product),
              isBossEligible: this.isBossProduct(bossDisabledFlags) && this.isGiftCard(product),
              badge1: this.extractPrioritizedBadge(
                product,
                attributesNames,
                categoryType,
                excludeBadge,
                !isBundleProduct
              ),
              badge2: this.extractAttributeValue(product, attributesNames.extendedSize),
              badge3: this.extractAttributeValue(product, attributesNames.merchant),
              videoUrl: this.extractAttributeValue(product, attributesNames.videoUrl),
              hasOnModelAltImages: parseBoolean(
                this.extractAttributeValue(product, attributesNames.onModelAltImages)
              ),
              listPrice: product.min_list_price,
              offerPrice: product.min_offer_price,
              keepAlive: parseBoolean(product[attributesNames.keepAlive]),
            },
            color: {
              name: defaultColor,
              imagePath: getImgPath(isBundleProduct ? product.prodpartno : product.imagename)
                .colorSwatch,
              swatchImage: removeExtension(product.swatchimage),
            },
          },
        ];
      }
      getProductByColorId(products, colorDetails) {
        return products.find((product) => product.prodpartno === colorDetails[0]);
      }
      colorDetailsCondition(colorDetails, product, swatchOfAvailableProduct) {
        return colorDetails[0] !== product.imagename && swatchOfAvailableProduct !== undefined;
      }
      isBossEligibleProduct(bossDisabledFlags, product) {
        return isBossProduct(bossDisabledFlags) && isGiftCard(product);
      }
      getListPrice(swatchOfAvailableProduct) {
        return swatchOfAvailableProduct.min_list_price === swatchOfAvailableProduct.min_offer_price
          ? swatchOfAvailableProduct.min_offer_price
          : swatchOfAvailableProduct.min_list_price ||
              {
                value: null,
              }.value ||
              0;
      }
      getCategoryMap(catPath, l1) {
        var { length } = catPath;
        var catMap = {};
        for (var idx = 0; idx < length; idx += 1) {
          var temp = catPath[idx].split('>');
          catMap[temp[1]] = catMap[temp[1]] ? catMap[temp[1]] : [];
          if (temp[0] && l1 && temp[0] === l1) {
            catMap[temp[1]].push(temp[2]);
          }
        }
        return catMap;
      }
      getRequiredL3(shouldApplyUnbxdLogic, bucketingSeqConfig, idx) {
        return shouldApplyUnbxdLogic
          ? bucketingSeqConfig.desiredL3
          : bucketingSeqConfig.requiredChildren[idx].categoryContent.name;
      }
      checkIfL3Matches(product, requiredL3) {
        return product.categoryPath3.find((category) => category === requiredL3);
      }
      catMapExists(temp, catMap, bucketingSeqConfig) {
        const desiredL2Val =
          bucketingSeqConfig.desiredL2 && bucketingSeqConfig.desiredL2.split('|')[0];
        return temp && catMap[desiredL2Val] && catMap[desiredL2Val].indexOf(temp) !== -1;
      }
      getL3Category(shouldApplyUnbxdLogic, bucketingSeqConfig, product, catMap, childLength) {
        let categoryName;
        for (let idx = 0; idx < childLength; idx += 1) {
          const requiredL3 = this.getRequiredL3(shouldApplyUnbxdLogic, bucketingSeqConfig, idx);
          const temp = this.checkIfL3Matches(product, requiredL3);
          if (this.catMapExists(temp, catMap, bucketingSeqConfig)) {
            categoryName = temp;
          }
          if (categoryName) {
            break;
          }
        }
        return categoryName;
      }
      isUnbxdFacetKey(key) {
        return (
          key.toLowerCase() !== FACETS_FIELD_KEY.unbxdDisplayName &&
          key.toLowerCase() !== FACETS_FIELD_KEY.sort &&
          key !== FACETS_FIELD_KEY.l1category
        );
      }
      getAppliedFilters(filters, filterIds) {
        const appliedFilters = {};
        const facetKeys = filterIds ? Object.keys(filterIds) : [];

        facetKeys.forEach((facetKey) => {
          if (this.isUnbxdFacetKey(facetKey)) {
            if (facetKey === 'bopisStoreId') {
              appliedFilters[facetKey] = filterIds[facetKey];
            } else {
              appliedFilters[facetKey] = !filters[facetKey]
                ? []
                : filters[facetKey]
                    .filter((item) => filterIds[facetKey].indexOf(item.id) > -1)
                    .map((item) => item.id);
            }
          }
        });
        return appliedFilters;
      }
      getCurrentNavigationIds(res) {
        try {
          return res.searchMetaData.queryParams['p-id'].replace(/"/g, '').split(':')[1].split('>');
        } catch (error) {
          return [];
        }
      }
      getBreadCrumbTrail(breadCrumbs) {
        return breadCrumbs
          ? breadCrumbs.map((crumb) => ({
              displayName: crumb.displayName,
              urlPathSuffix: crumb.urlPathSuffix,
              categoryId: crumb.categoryId,
            }))
          : [];
      }
      parseProductInfo(
        productArr,
        {
          isUSStore,
          hasShortImage,
          attributesNames,
          categoryType,
          excludeBadge,
          bucketingSeqConfig,
          shouldApplyUnbxdLogic,
          response,
          res,
          resObj,
        }
      ) {
        const product = productArr;
        product.list_of_attributes = this.attributeListMaker(product.list_of_attributes);
        const defaultColor = product.auxdescription ? product.auxdescription : product.TCPColor;
        const { uniqueId } = product;
        const colors = isUSStore
          ? this.convertToColorArray(product.TCPSwatchesUSStore, uniqueId, defaultColor)
          : this.convertToColorArray(product.TCPSwatchesCanadaStore, uniqueId, defaultColor);
        const rawColors = isUSStore ? product.TCPSwatchesUSStore : product.TCPSwatchesCanadaStore;
        const isBOPIS = this.isBopisEligibleProduct(isUSStore, product);
        const isBundleProduct =
          (product.product_type && product.product_type.toLowerCase() === 'bundle') || false;
        const bossDisabledFlags = {
          bossProductDisabled: this.isBossProductDisabled(product),
          bossCategoryDisabled: this.isBopisProductDisabled(product),
        };
        const imageExtension = product.productimage || '';
        const imagesByColor = this.extractExtraImages(
          rawColors,
          product.alt_img,
          uniqueId,
          defaultColor,
          false,
          hasShortImage,
          imageExtension
        );

        const colorsMap = this.getColorsMap({
          uniqueId,
          product,
          attributesNames,
          categoryType,
          excludeBadge,
          isBOPIS,
          bossDisabledFlags,
          defaultColor,
          isBundleProduct,
        });

        if (Array.isArray(colors)) {
          colors.forEach((color) => {
            const colorDetails = color.split('#');
            const swatchOfAvailableProduct = this.getProductByColorId(
              res.response.products,
              colorDetails
            );
            if (this.colorDetailsCondition(colorDetails, product, swatchOfAvailableProduct)) {
              colorsMap.push({
                colorProductId: colorDetails[0],
                imageName: colorDetails[0],
                miscInfo: {
                  isBopisEligible: this.isBopisEligibleProduct(
                    isUSStore,
                    swatchOfAvailableProduct,
                    product
                  ),
                  isBossEligible: this.isBossEligibleProduct(bossDisabledFlags, product),
                  hasOnModelAltImages: parseBoolean(
                    this.extractAttributeValue(
                      swatchOfAvailableProduct,
                      attributesNames.onModelAltImages
                    )
                  ),
                  badge1: this.extractPrioritizedBadge(
                    swatchOfAvailableProduct,
                    attributesNames,
                    categoryType,
                    excludeBadge,
                    !isBundleProduct
                  ),
                  badge2: this.extractAttributeValue(
                    swatchOfAvailableProduct,
                    attributesNames.extendedSize
                  ),
                  badge3: this.extractAttributeValue(
                    swatchOfAvailableProduct,
                    attributesNames.merchant
                  ),
                  listPrice: this.getListPrice(swatchOfAvailableProduct),
                  offerPrice:
                    swatchOfAvailableProduct.min_offer_price ||
                    {
                      value: null,
                    }.value ||
                    0,
                  keepAlive: parseBoolean(swatchOfAvailableProduct[attributesNames.keepAlive]),
                },
                color: {
                  name: colorDetails[1],
                  imagePath: getImgPath(colorDetails[0]).colorSwatch,
                },
              });
            }
          });
        }
        let categoryName;
        const childLength =
          bucketingSeqConfig && bucketingSeqConfig.requiredChildren
            ? bucketingSeqConfig.requiredChildren.length
            : 0;
        const catMap =
          product.categoryPath3_fq &&
          this.getCategoryMap(product.categoryPath3_fq, bucketingSeqConfig.desiredl1);
        if (product.categoryPath3) {
          categoryName = this.getL3Category(
            shouldApplyUnbxdLogic,
            bucketingSeqConfig,
            product,
            catMap,
            childLength
          );
        }
        response.loadedProductsPages[0].push({
          productInfo: {
            generalProductId: product.prodpartno || product.generalProductId,
            name: product.product_name,
            pdpUrl: `${isBundleProduct ? '/b' : '/p'}/${product.seo_token || product.uniqueId}`,
            uniqueId: product.uniqueId,
            shortDescription: product.product_short_description,
            longDescription: product.product_short_description,
            isGiftCard: this.isGiftCard(product),
            listPrice:
              product.min_list_price === product.min_offer_price
                ? product.min_offer_price
                : product.min_list_price || { value: null }.value || 0,
            offerPrice: product.min_offer_price || { value: null }.value || 0,
            ratings: product.TCPBazaarVoiceRating || 0,
            reviewsCount:
              (product.TCPBazaarVoiceReviewCount &&
                parseInt(product.TCPBazaarVoiceReviewCount, 10)) ||
              0,
            bundleProduct: isBundleProduct,
            unbxdId: resObj.headers && resObj.headers['unbxd-request-id'],
            promotionalMessage: product.TCPLoyaltyPromotionTextUSStore || '',
            promotionalPLCCMessage: product.TCPLoyaltyPLCCPromotionTextUSStore || '',
            long_product_title: product.long_product_title || '',
            priceRange: {
              highListPrice: parseFloat(product.high_list_price) || 0,
              highOfferPrice: parseFloat(product.high_offer_price) || 0,
              lowListPrice:
                parseFloat(product.low_list_price) || parseFloat(product.min_list_price) || 0,
              lowOfferPrice:
                parseFloat(product.low_offer_price) || parseFloat(product.min_offer_price) || 0,
            },
            isSearchFlag: false,
          },
          miscInfo: {
            rating: parseFloat(product.TCPBazaarVoiceRating) || 0,
            categoryName,
          },
          colorsMap,
          imagesByColor,
          relatedSwatchImages:
            product.related_product_swatch_images &&
            product.related_product_swatch_images.split(','),
        });
      }
      getLoadedProductsCount(products) {
        const allProducts = products && products.loadedProductsPages;
        const totalProductCount =
          (allProducts && allProducts.reduce((sum, item) => item && item.length + sum, 0)) || 0;
        return totalProductCount || 0;
      }
      processResponse(
        res,
        url,
        breadCrumbs,
        shouldApplyUnbxdLogic,
        bucketingSeqConfig,
        resObj,
        filtersAndSort,
        isFacetCall,
        reqObject
      ) {
        if (!isFacetCall) {
          window.SEARCH_RESPONSE_INITIAL = {
            unbxdResponse: {
              body: res,
              headers: resObj.headers,
              reqObj: reqObject,
              bucketingConfig: bucketingConfig,
            },
          };
          const event = new CustomEvent('unbxdResRecieved', {
            detail: {
              unbxdResponse: {
                body: res,
                headers: resObj.headers,
                reqObj: reqObject,
                bucketingConfig: bucketingConfig,
              },
            },
          });
          if (elem) {
            elem.dispatchEvent(event);
          } else {
            window.isUnbxdRequestTriggered = false;
          }
          return null;
        }
        const scrollPoint = 0;
        const modifiedFiltersAndSort = filtersAndSort;
        if (res.redirect && typeof window !== 'undefined') {
          var redirectUrl = res.redirect.value;
          try {
            if (redirectUrl.length && redirectUrl.indexOf(window.location.hostname) > -1) {
              redirectUrl = redirectUrl.replace(window.location.origin, '');
              redirectUrl = redirectUrl.replace(/\/(us|ca)/, '');
            } else {
              window.location.assign(redirectUrl);
            }
          } catch (e) {
            console.log('error', e);
          }
        }

        const isDepartment = !breadCrumbs || breadCrumbs.length === 1;
        const attributesNames = this.getProductAttributes();
        const categoryType =
          breadCrumbs && breadCrumbs.length ? breadCrumbs[breadCrumbs.length - 1].displayName : '';
        const l1category = breadCrumbs && breadCrumbs.length ? breadCrumbs[0].displayName : '';
        let filters = this.getFiltersAfterProcessing(
          res.facets,
          res.response.numberOfProducts,
          getFacetSwatchImgPath,
          filtersAndSort,
          l1category
        );

        const availableL3List = res.facets && res.facets.multilevel && res.facets.multilevel.bucket;
        const availableL3InFilter =
          availableL3List && availableL3List.length && (availableL3List[0].values || []);
        let totalProductsCount = 0;
        let productListingCurrentNavIds;
        totalProductsCount =
          availableL3InFilter.reduce((sum, item) => item && item.count + sum, 0) ||
          res.response.numberOfProducts;
        if (shouldApplyUnbxdLogic && bucketingSeqConfig.bucketingRequired) {
          const productListingFilters = [];
          const productListingTotalCount = 0;
          filters = Object.keys(productListingFilters).length ? productListingFilters : filterMaps;
          totalProductsCount = productListingTotalCount || 0;
        }

        const unbxdId = resObj.headers && resObj.headers['unbxd-request-id'];
        const entityCategory = {};
        const categoryNameTop = '';
        let bannerInfo = {};
        res.response.isSearch = false;
        const response = {
          availableL3InFilter,
          currentListingSearchForText: '',
          currentListingSeoKey: '',
          currentListingId:
            breadCrumbs && breadCrumbs.length
              ? breadCrumbs[breadCrumbs.length - 1].urlPathSuffix
              : '',
          currentListingName: categoryType,
          currentListingDescription:
            breadCrumbs && breadCrumbs.length
              ? breadCrumbs[breadCrumbs.length - 1].longDescription
              : '',
          currentListingType:
            breadCrumbs && breadCrumbs.length
              ? breadCrumbs[breadCrumbs.length - 1].displayName
              : '',
          isDepartment,
          outfitStyliticsTag: null,
          filtersMaps: filters,
          appliedFiltersIds: this.getAppliedFilters(filters, modifiedFiltersAndSort),
          totalProductsCount,
          productsInCurrCategory: res.response.numberOfProducts,
          unbxdId,
          appliedSortId: '',
          currentNavigationIds: this.getCurrentNavigationIds(res),
          breadCrumbTrail: this.getBreadCrumbTrail(breadCrumbs),
          loadedProductsPages: [[]],
          searchResultSuggestions: '',
          unbxdBanners: {},
          entityCategory,
          categoryNameTop,
          isSearch: false,
        };
        if (res.response) {
          const isUSStore = false; // !isCanada(); will be required when processing is donee for products
          res.response.products.forEach((product) =>
            this.parseProductInfo(product, {
              isUSStore,
              attributesNames,
              categoryType,
              bucketingSeqConfig,
              shouldApplyUnbxdLogic,
              response,
              res,
              resObj,
            })
          );
          res.response.loadedProductCount = getLoadedProductsCount(res.response.products);
        }

        try {
          if (res.banner) {
            bannerInfo = JSON.parse(res.banner.banners[0].bannerHtml);
          }
        } catch (error) {
          console.log(error);
        }
        const processedResponse = { ...response, bannerInfo };
        return processedResponse;
      }
      getUpdatedL3(l3ReturnedByL2, availableL3) {
        const updatedAvailableL3 = [];
        if (l3ReturnedByL2 && l3ReturnedByL2.length) {
          availableL3.forEach((item) => {
            const itm = { ...item };
            for (let idx = 0; idx < l3ReturnedByL2.length; idx += 1) {
              if (itm.categoryId === l3ReturnedByL2[idx].id && itm.displayToCustomer) {
                itm.count = l3ReturnedByL2[idx].count;
                updatedAvailableL3.push(itm);
              }
            }
          });
        }
        return updatedAvailableL3;
      }
      processProductFilterAndCountData(res) {
        const facetCallResponseState = {
          filtersMaps: res.filtersMaps,
          totalProductsCount: res.totalProductsCount,
          currentNavigationIds: res.currentNavigationIds,
        };
        const { categoryNameList, filtersAndSort, pageNumber, location } = this.reqObj;
        const updatedAvailableL3 = this.getUpdatedL3(
          res.availableL3InFilter,
          bucketingConfig.availableL3
        );
        bucketingConfig.availableL3 = [...updatedAvailableL3];
        bucketingConfig.L3Left = [...updatedAvailableL3];

        if (!bucketingConfig.L3Left.length) {
          return null;
        }

        categoryNameList.push(bucketingConfig.L3Left[0]);
        const categoryPathMap = categoryNameList
          ? categoryNameList
              .map((item) => item && (item.categoryId || item.categoryContent.id))
              .join('>')
          : '';

        return getProductsListingInfo({
          filtersAndSort,
          pageNumber,
          location,
          startProductCount: bucketingConfig.start,
          numberOfProducts: bucketingConfig.productsToFetchPerLoad,
          categoryPathMap,
          facetCallResponseState,
        });
      }
      getProducts(isFacetCall) {
        const {
          seoKeywordOrCategoryIdOrSearchTerm,
          isSearch,
          filtersAndSort = {},
          pageNumber,
          categoryId,
          bucketingSeqConfig,
          isUnbxdSequencing,
          startProductCount,
          numberOfProducts,
          extraParams,
          shouldApplyUnbxdLogic,
        } = this.reqObj;
        const searchTerm = decodeURIComponent(seoKeywordOrCategoryIdOrSearchTerm);
        const { sort = null } = filtersAndSort;

        const facetsPayload = this.extractFilters(this.reqObj.filtersAndSort);
        const facetKeys = Object.keys(facetsPayload);
        var facetString = '';
        facetKeys.forEach((facetKey) => {
          facetString += `${facetKey}=${encodeURIComponent(facetsPayload[facetKey])}`;
        });

        const isOutfitPage = !isSearch && searchTerm && searchTerm.indexOf('-outfit') > -1;
        const row = numberOfProducts !== undefined ? numberOfProducts : PRODUCTS_PER_LOAD;
        const start =
          startProductCount !== undefined
            ? startProductCount
            : (pageNumber - 1) * PRODUCTS_PER_LOAD;

        const payload = {
          body: {
            ...facetsPayload,
            ...extraParams,
            start: 0,
            rows: row,
            variants: true,
            'variants.count': 0,
            version: 'V2',
            'facet.multiselect': true,
            selectedfacet: true,
            fields:
              'related_product_swatch_images,productimage,alt_img,style_partno,giftcard,swatchimage,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,low_offer_price, high_offer_price, low_list_price, high_list_price',
            pagetype: 'boolean',
            q: searchTerm,
          },
        };
        if (categoryId) {
          payload.body['p-id'] = `categoryPathId:"${categoryId}"`;
        }

        if (bucketingSeqConfig.bucketingRequired) {
          payload.body.facet = false;
        }
        if (this.isNoUnbxdLogic(shouldApplyUnbxdLogic, isUnbxdSequencing)) {
          if (this.isNoBucketing(bucketingSeqConfig)) {
            payload.body.sort = bucketingSeqConfig.bucketingSeq;
          }
        }
        if (sort) payload.body.sort = sort;

        var payloadKeys = Object.keys(payload.body);
        var queryString = '';
        payloadKeys.forEach((payloadKey) => {
          queryString += payloadKey + '=' + payload.body[payloadKey] + '&';
        });

        queryString = queryString.substring(0, queryString.length - 1);
        var filterAndCountReqUrl = unbxdUrl + '?' + queryString;

        fetch(filterAndCountReqUrl)
          .then((response) => {
            if (response.status !== 200) {
              console.log('Looks like there was a problem. Status Code: ' + response.status);
              return;
            }
            response.json().then((data) => {
              const abcd = this.processResponse(
                data,
                url,
                this.reqObj.breadCrumbs,
                shouldApplyUnbxdLogic,
                bucketingSeqConfig,
                response,
                this.reqObj.filtersAndSort,
                isFacetCall,
                this.reqObj
              );
              if (isFacetCall) {
                this.reqObj = this.processProductFilterAndCountData(abcd);
                this.getProducts(false);
              }
            });
          })
          .catch(function (err) {
            console.log('Fetch Error :-- ', err);
          });
      }
    }
    // Products abstractor Class ends
    //////////////////////////////////

    //////////////////////////////////
    // Class BucketingBL (Bucketing business logic)
    class BucketingBL {
      getMatchPath(isSearchPage, location) {
        const categoryParam = '/c/';
        const params = isSearchPage ? '/search' : categoryParam;
        return matchPath(location.pathname, params);
      }
      /**
       * @funtion updateBucketingParamters This function updates the start and the products to be fetched , after an L3 call is successfull.
       * @param {Object} res The response object of L3 Call.
       */
      updateBucketingParamters = (res, bucketingConfig) => {
        const temp = { ...bucketingConfig };
        const { productsToFetchPerLoad } = temp;
        const { start } = temp;
        const productsFecthedTillNow = start + productsToFetchPerLoad;
        const productsLeft = res.productsInCurrCategory - productsFecthedTillNow;
        temp.start = productsFecthedTillNow;
        if (productsToFetchPerLoad > productsLeft) {
          temp.productsToFetchPerLoad = productsLeft;
        }
        if (productsLeft <= 0) {
          temp.start = 0;
          temp.productsToFetchPerLoad = PRODUCTS_PER_LOAD;
          temp.L3Left.splice(0, 1);
        }
        return temp;
      };

      /**
       * @function doBucketingLogic This function does the logic work needed for bucketing
       */

      doBucketingLogic = (
        location = '',
        bucketingConfig,
        sortBySelected,
        filterAndSortParam,
        callback
      ) => {
        const isSearchPage = true;
        const temp = {};
        const bucketingConfigTemp = { ...bucketingConfig };
        temp.isSearchPage = isSearchPage;
        const match = this.getMatchPath(temp.isSearchPage, location);
        temp.categoryKey = temp.isSearchPage ? match.searchTerm : match.listingKey;
        temp.categoryNameList = typeof categoryNavInfo !== 'undefined' ? categoryNavInfo : '';
        temp.clickedL2 = temp.categoryNameList[1];
        bucketingConfigTemp.currL2NameList = [...temp.categoryNameList];
        bucketingConfigTemp.bucketingSeqScenario = false;
        if (sortBySelected) {
          temp.sortingAvailable = filterAndSortParam.sort;
          bucketingConfigTemp.start = 0;
          bucketingConfigTemp.productsToFetchPerLoad = PRODUCTS_PER_LOAD;
          temp.filtersAndSort = filterAndSortParam;
        } else {
          temp.filtersAndSort = (location && callback(location.search)) || {};
          temp.sortingAvailable = temp.filtersAndSort.sort;
        }
        const isSearch = match.searchTerm || match.listingKey;
        const searchTerm = decodeURIComponent(isSearch);
        temp.isOutfitPage = searchTerm && searchTerm.indexOf('-outfit') > -1;
        return { ...temp, ...bucketingConfigTemp };
      };

      /**
       * @function fetchPendingCallStack This function make a stack of all the pending promises which needs to be completed to fetch the products
       * according to the cached count. There is one limitation of UNBXD that we cannot order more than 100 products
       * over a single call. Now suppose one L3 is having 106 products to be fetched. We will make two calls for this L3.
       * One for 100 products and other for 6 products. We are making all the calls in parallel.
       */

      fetchPendingCallStack = (
        totalCount,
        MaxProducts,
        pendingPromisesStack,
        filtersAndSort,
        start,
        categoryPathMap,
        callback,
        location,
        catNameL3
      ) => {
        const countToFetch = totalCount < MaxProducts ? totalCount : MaxProducts;
        pendingPromisesStack.push(
          callback(filtersAndSort, '', location, start, countToFetch, categoryPathMap, catNameL3)
        );
        const countLeft = totalCount - countToFetch;
        if (countLeft > 0) {
          return this.fetchPendingCallStack(
            countLeft,
            MaxProducts,
            pendingPromisesStack,
            filtersAndSort,
            countToFetch,
            categoryPathMap,
            callback,
            location,
            catNameL3
          );
        }
        return pendingPromisesStack;
      };
    }
    // BucketingBL Class ends
    //////////////////////////////////

    const operatorInstance = new ProductsOperator();
    const sortBySelected = '';
    const formData = '';
    const reqObj = operatorInstance.getProductListingBucketedData(
      location,
      sortBySelected,
      formData,
      1
    );

    const abstractor = new ProductsAbstractor(reqObj);
    if (reqObj.isFetchFiltersAndCountReq) {
      abstractor.getProducts(true);
    } else {
      operatorInstance.shouldApplyUnbxdLogic = false;
      abstractor.getProducts(false);
    }
  })();
}
