/* eslint-disable */
// 9fbef606107a605d69c0edbcd8029e5d
/** TO CONVERT THIS FILE TO THE MINIFIED FILE -
 * Step 1: https://es6console.com/ (select presets -> es2015 and stage2)
 * Step 2: https://skalman.github.io/UglifyJS-online/
 */
function isIEBrowser() {
  var ua = window.navigator && window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older, return version number
    return true;
  }
  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    return true;
  }
  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    return true;
  }
  return false;
}

const removeExtension = (path) => (path ? path.replace(/\.\w{3}$/, '') : '');

if (!isIEBrowser()) {
  (function () {
    window.isUnbxdRequestTriggered = true;
    var url = window.location;
    var breadCrumbs = [];
    var bucketingConfig;
    var reqObj = {};
    var isUnbxdRequestTriggered = true;
    var PRODUCTS_PER_LOAD = window.firstPageProductsPerLoad || 20;
    var facetCallResponseState = {};

    function getImgPath(id, excludeExtension, imageExtension) {
      return {
        colorSwatch: null,
        productImages: getProductImagePath(id, excludeExtension, imageExtension),
      };
    }

    function getProductImgPath(id, excludeExtension) {
      const imgHostDomain = routingInfoStoreView.getOriginImgHostSetting();

      return {
        125: `${imgHostDomain}/wcsstore/GlobalSAS/images/tcp/products/125/${id}${
          excludeExtension ? '' : '.jpg'
        }`,
        380: `${imgHostDomain}/wcsstore/GlobalSAS/images/tcp/products/380/${id}${
          excludeExtension ? '' : '.jpg'
        }`,
        500: `${imgHostDomain}/wcsstore/GlobalSAS/images/tcp/products/500/${id}${
          excludeExtension ? '' : '.jpg'
        }`,
        900: `${imgHostDomain}/wcsstore/GlobalSAS/images/tcp/products/900/${id}${
          excludeExtension ? '' : '.jpg'
        }`,
      };
    }

    function getProductImagePath(id, excludeExtension, imageExtension) {
      const imageName = (id && id.split('_')) || [];
      const imagePath = imageName[0];
      const extension = imageExtension ? `.${imageExtension}` : '.jpg';

      return {
        125: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        380: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        500: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        900: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
      };
    }

    function getFacetSwatchImgPath(id) {
      const imgHostDomain = routingInfoStoreView.getOriginImgHostSetting();
      return `${imgHostDomain}/wcsstore/GlobalSAS/images/tcp/category/color-swatches/${id}.gif`;
    }

    function getMatchPath(isSearchPage, location) {
      const categoryParam = '/c/';
      const params = isSearchPage ? '/search/' : categoryParam;
      return matchPath(location.pathname, params);
    }

    if (typeof window.PLP_CATEGORY_NAVIGATION_INFO !== 'undefined') {
      var escapeCharacter = window.PLP_CATEGORY_NAVIGATION_INFO.replace(/\\\'/g, "'");
      var categoryNavInfo = (escapeCharacter && JSON.parse(escapeCharacter || '')) || [];

      var categoryIdArray = [];
      window.categoryId = '';
      categoryNavInfo.length &&
        categoryNavInfo.forEach((catItem, index) => {
          categoryIdArray.push(catItem.categoryId);
          breadCrumbs.push({
            categoryId: catItem.categoryId,
            displayName: catItem.title,
            urlPathSuffix: catItem.url,
          });
        });
    }

    var FACETS_FIELD_KEY = {
      color: 'tcpcolor_ufilter',
      size: 'v_tcpsize_ufilter',
      age: 'age_group_ufilter',
      price: 'unbxd_price_range_ufilter',
      sort: 'sort',
      unbxdDisplayName: 'unbxddisplayname',
      aux_color: 'auxdescription_ufilter',
      aux_color_unbxd: 'auxdescription_uFilter',
      l1category: 'l1category',
      display: 'display_group_uFilter',
    };

    function getCurrentNavigationIds(res) {
      try {
        return res.searchMetaData.queryParams['p-id'].replace(/"/g, '').split(':')[1].split('>');
      } catch (error) {
        return [];
      }
    }

    function getBreadCrumbTrail(breadCrumbs) {
      return breadCrumbs
        ? breadCrumbs.map((crumb) => ({
            displayName: crumb.displayName,
            urlPathSuffix: crumb.urlPathSuffix,
            categoryId: crumb.categoryId,
          }))
        : [];
    }

    function getLoadedProductsCount(products) {
      const allProducts = products && products.loadedProductsPages;
      const totalProductCount =
        (allProducts && allProducts.reduce((sum, item) => item && item.length + sum, 0)) || 0;
      return totalProductCount || 0;
    }

    window.categoryId = categoryIdArray.join('>');

    function matchPath(url, param) {
      if (param === '/search' && url.indexOf(param) !== -1) {
        return {
          searchTerm: url,
        };
      }
      if (
        (param === '/c?cid=' && url.indexOf(param) !== -1) ||
        (param === '/c/' && url.indexOf(param) !== -1)
      ) {
        const urlWithCat = url.split(param)[1];
        return {
          listingKey: urlWithCat,
        };
      }
      return url;
    }

    function getUpdatedL3(l3ReturnedByL2, availableL3) {
      const updatedAvailableL3 = [];
      if (l3ReturnedByL2 && l3ReturnedByL2.length) {
        availableL3.forEach((item) => {
          const itm = { ...item };
          for (let idx = 0; idx < l3ReturnedByL2.length; idx += 1) {
            if (itm.categoryId === l3ReturnedByL2[idx].id && itm.displayToCustomer) {
              itm.count = l3ReturnedByL2[idx].count;
              updatedAvailableL3.push(itm);
            }
          }
        });
      }
      return updatedAvailableL3;
    }

    function bindAllClassMethodsToThis(obj, namePrefix = '', isExclude = false) {
      const prototype = Object.getPrototypeOf(obj);
      for (let name of Object.getOwnPropertyNames(prototype)) {
        const descriptor = Object.getOwnPropertyDescriptor(prototype, name);
        const isGetter = descriptor && typeof descriptor.get === 'function';
        if (isGetter) continue;
        if (
          typeof prototype[name] === 'function' && name !== 'constructor' && isExclude
            ? !name.startsWith(namePrefix)
            : name.startsWith(namePrefix)
        ) {
          obj[name] = prototype[name].bind(obj);
        }
      }
    }

    function getPlpCutomizersFromUrlQueryString(urlQueryString) {
      const queryParams = urlQueryString && urlQueryString.split('?')[1];
      const returnVal = {};
      const queryParamsArray = (queryParams && queryParams.split('&')) || [];
      queryParamsArray.forEach((queryParam) => {
        const qsArray = queryParam.split('=');
        const value = decodeURIComponent(qsArray[1]);
        const key = decodeURIComponent(qsArray[0]);
        returnVal[key] =
          key && (key.toLowerCase() === FACETS_FIELD_KEY.sort ? value : value.split(','));
      });
      return returnVal;
    }

    /** THE LOGIC TO CONSTRUCT THE REQUEST */

    // THE BUCKETING LOGIC
    class BucketingBL {
      /**
       * @funtion updateBucketingParamters This function updates the start and the products to be fetched , after an L3 call is successfull.
       * @param {Object} res The response object of L3 Call.
       */
      updateBucketingParamters = (res, bucketingConfig) => {
        const temp = { ...bucketingConfig };
        const { productsToFetchPerLoad } = temp;
        const { start } = temp;
        const productsFecthedTillNow = start + productsToFetchPerLoad;
        const productsLeft = res.productsInCurrCategory - productsFecthedTillNow;
        temp.start = productsFecthedTillNow;
        if (productsToFetchPerLoad > productsLeft) {
          temp.productsToFetchPerLoad = productsLeft;
        }
        if (productsLeft <= 0) {
          temp.start = 0;
          temp.productsToFetchPerLoad = PRODUCTS_PER_LOAD;
          temp.L3Left.splice(0, 1);
        }
        return temp;
      };

      /**
       * @function doBucketingLogic This function does the logic work needed for bucketing
       */

      doBucketingLogic = (
        location = '',
        bucketingConfig,
        sortBySelected,
        filterAndSortParam,
        callback
      ) => {
        const temp = {};
        const bucketingConfigTemp = { ...bucketingConfig };
        temp.isSearchPage = false;
        const match = getMatchPath(temp.isSearchPage, location);
        temp.categoryKey = temp.isSearchPage ? match.searchTerm : match.listingKey;
        temp.categoryNameList = categoryNavInfo;
        temp.clickedL2 = temp.categoryNameList[1];
        bucketingConfigTemp.currL2NameList = [...temp.categoryNameList];
        bucketingConfigTemp.bucketingSeqScenario = false;
        if (sortBySelected) {
          temp.sortingAvailable = filterAndSortParam.sort;
          bucketingConfigTemp.start = 0;
          bucketingConfigTemp.productsToFetchPerLoad = PRODUCTS_PER_LOAD;
          temp.filtersAndSort = filterAndSortParam;
        } else {
          temp.filtersAndSort = (location && callback(location.search)) || {};
          temp.sortingAvailable = temp.filtersAndSort.sort;
        }
        const isSearchPage = false;
        const isSearch = match.searchTerm || match.listingKey;
        const searchTerm = decodeURIComponent(isSearch);
        temp.isOutfitPage = !isSearchPage && searchTerm && searchTerm.indexOf('-outfit') > -1;
        return { ...temp, ...bucketingConfigTemp };
      };

      /**
       * @function fetchPendingCallStack This function make a stack of all the pending promises which needs to be completed to fetch the products
       *                                 according to the cached count. There is one limitation of UNBXD that we cannot order more than 100 products
       *                                 over a single call. Now suppose one L3 is having 106 products to be fetched. We will make two calls for this L3.
       *                                 One for 100 products and other for 6 products. We are making all the calls in parallel.
       */

      fetchPendingCallStack = (
        totalCount,
        MaxProducts,
        pendingPromisesStack,
        filtersAndSort,
        start,
        categoryPathMap,
        callback,
        location,
        catNameL3
      ) => {
        const countToFetch = totalCount < MaxProducts ? totalCount : MaxProducts;
        pendingPromisesStack.push(
          callback(filtersAndSort, '', location, start, countToFetch, categoryPathMap, catNameL3)
        );
        const countLeft = totalCount - countToFetch;
        if (countLeft > 0) {
          return this.fetchPendingCallStack(
            countLeft,
            MaxProducts,
            pendingPromisesStack,
            filtersAndSort,
            countToFetch,
            categoryPathMap,
            callback,
            location,
            catNameL3
          );
        }
        return pendingPromisesStack;
      };
    }

    function visibleCategoryCount(items) {
      return items.reduce((acc, item) => {
        return item.displayToCustomer ? acc + 1 : acc;
      }, 0);
    }

    function matchValue(isSearchPage, location) {
      const categoryParam = '/c/';
      const params = isSearchPage ? '/search/' : categoryParam;
      return matchPath(location, params);
    }

    function isMatchSearch(isSearchPage, searchTerm, location) {
      return matchValue(isSearchPage, location.pathname);
    }

    function getProductsListingInfo({
      filtersAndSort,
      pageNumber,
      location,
      startProductCount,
      numberOfProducts,
      categoryPathMap,
      catNameL3,
      isLazyLoading,
      facetCallResponseState,
    }) {
      const isSearchPage = false;
      const searchTerm = location.pathname.substr(11);
      const match = isMatchSearch(isSearchPage, searchTerm, location);
      const categoryKey = match && match.listingKey;
      const categoryNameList = categoryNavInfo;
      const breadCrumb = breadCrumbs;
      const breadcrumbCatIds = breadCrumb.length
        ? breadCrumb.map((category) => category.categoryId)
        : [];
      const excludeBage = false;
      const isUnbxdSequencing = true;
      let categoryId;
      const clickedL2 = categoryNameList[1];
      const clickedl1 = categoryNameList[0];
      const bucketingSeqConfig = {};
      let filteredBucketingSeqConfig = {};

      if (!isSearchPage) {
        if (!isUnbxdSequencing || operatorInstance.shouldApplyUnbxdLogic) {
          bucketingSeqConfig.desiredL2 = clickedL2 ? clickedL2.title : '';
          bucketingSeqConfig.desiredl1 = clickedl1 ? clickedl1.title : '';
        }

        filteredBucketingSeqConfig = getPlpBucketDetails({
          bucketingSeqConfigArg: bucketingSeqConfig,
          categoryNameList,
          categoryPathMap,
          catNameL3,
          isUnbxdSequencing,
          categoryIdArg: window.categoryId,
          clickedL2,
        });
      }
      let parsedNavCategory = [];
      if (typeof window.PLP_CATEGORY_NAVIGATION_INFO !== 'undefined') {
        const currentNavCategory = window.PLP_CATEGORY_NAVIGATION_INFO.replace(/\\\'/g, "'");
        parsedNavCategory = (currentNavCategory && JSON.parse(currentNavCategory || '')) || [];
      }
      const currentCategory =
        parsedNavCategory.length && parsedNavCategory[parsedNavCategory.length - 1];
      const hasShortImage = currentCategory && currentCategory.isShortImage;

      const abd = {
        seoKeywordOrCategoryIdOrSearchTerm: match && match.listingKey,
        isSearch: isSearchPage,
        filtersAndSort,
        pageNumber,
        getImgPath: getImgPath,
        categoryId: filteredBucketingSeqConfig.categoryId,
        breadCrumbs: breadCrumb,
        bucketingSeqConfig: filteredBucketingSeqConfig.bucketingSeqConfig,
        getFacetSwatchImgPath: getFacetSwatchImgPath,
        isUnbxdSequencing,
        excludeBadge: excludeBage,
        startProductCount,
        numberOfProducts,
        cacheFiltersAndCount: false,
        extraParams: '',
        shouldApplyUnbxdLogic: operatorInstance.shouldApplyUnbxdLogic,
        categoryNameList,
        isLazyLoading,
        facetCallResponseState,
        hasShortImage,
      };
      return abd;
    }

    function getPlpBucketDetails({
      bucketingSeqConfigArg,
      categoryNameList,
      categoryPathMap,
      catNameL3,
      isUnbxdSequencing,
      categoryIdArg,
      clickedL2,
      isSearchPage,
    }) {
      let requiredChildren;

      const bucketingSeqConfig = bucketingSeqConfigArg;
      let categoryId = categoryIdArg;
      if (operatorInstance.shouldApplyUnbxdLogic) {
        bucketingSeqConfig.requiredChildren = bucketingConfig.availableL3;
        bucketingSeqConfig.bucketingRequired = bucketingConfig.bucketingSeqScenario;
        bucketingSeqConfig.desiredL3 =
          !catNameL3 && bucketingConfig.L3Left.length
            ? bucketingConfig.L3Left[0] && bucketingConfig.L3Left[0].title
            : catNameL3;
        categoryId = categoryPathMap;
        if (isUnbxdSequencing) bucketingSeqConfig.bucketingSeq = 'A/B-Test';
      } else {
        if (categoryNameList[1]) {
          requiredChildren = categoryNavInfo[1].subCategories;
          const isBuktSeqReq = requiredChildren && requiredChildren.length;
          bucketingSeqConfig.requiredChildren = requiredChildren;

          if (isUnbxdSequencing) {
            if (!isBuktSeqReq) {
              bucketingSeqConfig.bucketingSeq = 'A/B-Test';
            }
          } else if (isBuktSeqReq) {
            const catId = categoryNameList && categoryNameList.length ? clickedL2.categoryId : null;
            bucketingSeqConfig.bucketingSeq = `sort_${catId} asc,pop_score desc`;
          }
        }
        categoryId = categoryNameList
          ? categoryNameList.map((item) => item.categoryId).join('>')
          : '';
      }
      return { bucketingSeqConfig, categoryId };
    }

    //////////////////////////////////
    class ProductsOperator {
      constructor() {
        this.resetBucketingConfig();
        this.shouldApplyUnbxdLogic = true;

        bindAllClassMethodsToThis(this);
      }

      get bucketingLogic() {
        return new BucketingBL();
      }

      resetBucketingConfig = () => {
        bucketingConfig = {
          start: 0,
          productsToFetchPerLoad: PRODUCTS_PER_LOAD,
          L3Left: [],
          currL2NameList: [],
          bucketingSeqScenario: false,
          availableL3: [],
        };
      };

      getNavAttributes = (navTree, breadcrumbCatIds, attribute) => {
        let extractCat = {};
        breadcrumbCatIds.forEach((breadCrumbId, index) => {
          if (index === 0) {
            extractCat = getCatById(navTree, breadCrumbId);
          } else if (index === 1 && extractCat.subCategories && extractCat.subCategories) {
            Object.values(extractCat.subCategories).some((l2Category) => {
              const extractL2Cat = getCatById(l2Category.items, breadCrumbId);
              if (extractL2Cat) {
                extractCat = extractL2Cat;
                return true;
              }
              return false;
            });
          } else if (index === 2) {
            extractCat = getCatById(extractCat.subCategories, breadCrumbId);
          }
        });
        return extractCat && extractCat.categoryContent
          ? extractCat.categoryContent[attribute]
          : '';
      };

      fetchFiltersAndCount = (filters, categoryId, categoryNameList, location, pageNumber) => {
        const extraParams = {
          'facet.multilevel': 'categoryPath',
          'f.categoryPath.nameId': true,
          'f.categoryPath.max.depth': 4,
        };
        return {
          seoKeywordOrCategoryIdOrSearchTerm: '',
          isSearch: '',
          filtersAndSort: filters,
          pageNumber,
          getImgPath: '',
          categoryId,
          breadCrumbs: '',
          bucketingSeqConfig: '',
          getFacetSwatchImgPath: getFacetSwatchImgPath,
          isUnbxdSequencing: '',
          excludeBadge: '',
          startProductCount: 0,
          numberOfProducts: 0,
          cacheFiltersAndCount: true,
          extraParams,
          shouldApplyUnbxdLogic: operatorInstance.shouldApplyUnbxdLogic,
          hasShortImage: false,
          categoryNameList,
          location,
          isFetchFiltersAndCountReq: true,
        };
      };

      getProductListingBucketedData(
        location,
        sortBySelected,
        filterAndSortParam = {},
        pageNumber = ''
      ) {
        this.resetBucketingConfig();
        let filtersAndSort;
        let navigationTree;
        let categoryNameList;
        let clickedL2;
        let sortingAvailable;
        let isOutfitPage;
        ({
          filtersAndSort,
          navigationTree,
          categoryNameList,
          clickedL2,
          sortingAvailable,
          isOutfitPage,
          ...bucketingConfig
        } = this.bucketingLogic.doBucketingLogic(
          location,
          bucketingConfig,
          sortBySelected,
          filterAndSortParam,
          getPlpCutomizersFromUrlQueryString
        ));
        const requiredChildren = clickedL2 && categoryNavInfo[1].subCategories;
        const isBuktSeqReq = requiredChildren && requiredChildren.length;
        const displayableCatCount = isBuktSeqReq ? visibleCategoryCount(requiredChildren) : 0;
        if (
          operatorInstance.shouldApplyUnbxdLogic &&
          !sortingAvailable &&
          displayableCatCount &&
          !categoryNameList[2] &&
          !isOutfitPage
        ) {
          bucketingConfig.L3Left = [...requiredChildren];
          bucketingConfig.availableL3 = [...requiredChildren];
          bucketingConfig.bucketingSeqScenario = true;
          const categoryPathMapL2 = bucketingConfig.currL2NameList
            ? bucketingConfig.currL2NameList.map((item) => item.categoryId).join('>')
            : '';
          return this.fetchFiltersAndCount(
            filtersAndSort,
            categoryPathMapL2,
            categoryNameList,
            location,
            pageNumber
          );
        }
        return this.getProductsListingForUrlLocation(
          location,
          filterAndSortParam,
          (sortBySelected = ''),
          isBuktSeqReq && !categoryNavInfo[2] && !sortingAvailable
        );
      }

      getProductsListingForUrlLocation(location, sortParam, sortBySelected, isBuktSeqReq) {
        const pageNumber = !Number.isNaN(location.pageNumber)
          ? Math.max(parseInt(location.pageNumber, 10))
          : 1;
        const filtersAndSort = sortBySelected
          ? sortParam
          : getPlpCutomizersFromUrlQueryString(location.search);

        if (!isBuktSeqReq) {
          operatorInstance.shouldApplyUnbxdLogic = false;
        }
        return getProductsListingInfo({ filtersAndSort, pageNumber, location });
      }

      isExportBadge = (breadcrumbCatIds, navigationTree) => {
        return breadcrumbCatIds && breadcrumbCatIds.length
          ? this.getNavAttributes(navigationTree, breadcrumbCatIds, 'excludeAttribute')
          : '';
      };

      shortImage = (isSearchPage, breadcrumbCatIds, navigationTree) => {
        return '';
      };

      getSeoForSearch = (isSearchPage, match) => {
        return isSearchPage ? match : getSeoKeywordOrCategoryIdOrSearchTerm(match);
      };

      getBucketingSeqConfig = (isSearchPage, bucketingSeqConfig) => {
        return isSearchPage ? {} : bucketingSeqConfig;
      };

      getProductsListingFilters({ state, asPath, pageNumber, formData, location }) {
        const filtersAndSort = formData || getPlpCutomizersFromUrlQueryString(asPath);
        return getProductsListingInfo({
          filtersAndSort,
          pageNumber,
          location,
        });
      }

      updateBucketingConfig = (res) => {
        const updatedBucketingConfig = this.bucketingLogic.updateBucketingParamters(
          res,
          bucketingConfig
        );
        ({ ...bucketingConfig } = { ...updatedBucketingConfig });
        if (bucketingConfig.availableL3.length) {
          res.totalProductsCount = getTotalProductsCount(bucketingConfig.availableL3);
        }
      };
    }

    function isUnbxdFacetKey(key) {
      return (
        key.toLowerCase() !== FACETS_FIELD_KEY.unbxdDisplayName &&
        key.toLowerCase() !== FACETS_FIELD_KEY.sort &&
        key !== FACETS_FIELD_KEY.l1category
      );
    }
    function getModifiedQueryString(facetValue, query) {
      return facetValue.length > 0 ? (query ? '&filter=' : '') + facetValue.join(' OR ') : '';
    }

    function extractFilters(filtersAndSort) {
      const filterQuery = {};
      let query = '';
      const facetKeys = Object.keys(filtersAndSort);
      facetKeys.forEach((facetKey) => {
        let facetValue = filtersAndSort[facetKey];
        if (
          isUnbxdFacetKey(facetKey) &&
          facetValue &&
          facetValue.length > 0 &&
          facetKey.indexOf('uFilter') > -1
        ) {
          facetValue = facetValue.map((facet) => {
            const encodedFacet = encodeURIComponent(facet);
            const encodedFacetWithQuote = encodeURIComponent(`"${facet}"`);
            return `${facetKey}:"${encodedFacet}"`;
          });

          query += getModifiedQueryString(facetValue, query);
        } else if (facetKey === 'bopisStoreId') {
          query += getModifiedQueryString(facetValue, query);
        }
      });

      if (query !== '') filterQuery.filter = query;
      return filterQuery;
    }

    function isNoUnbxdLogic(shouldApplyUnbxdLogic, isUnbxdSequencing) {
      return !shouldApplyUnbxdLogic && !isUnbxdSequencing;
    }

    function isNotSearchAndBucketing(isSearch, bucketingSeqConfig) {
      return !isSearch && bucketingSeqConfig.bucketingSeq;
    }

    function isNoBucketing(bucketingSeqConfig) {
      return (
        bucketingSeqConfig &&
        bucketingSeqConfig.bucketingSeq &&
        bucketingSeqConfig.requiredChildren.length
      );
    }

    function getProducts(isFacetCall) {
      const {
        seoKeywordOrCategoryIdOrSearchTerm,
        isSearch,
        filtersAndSort = {},
        pageNumber,
        getImgPath,
        categoryId,
        breadCrumbs,
        bucketingSeqConfig,
        getFacetSwatchImgPath,
        isUnbxdSequencing,
        excludeBadge,
        startProductCount,
        numberOfProducts,
        cacheFiltersAndCount,
        extraParams,
        shouldApplyUnbxdLogic,
        hasShortImage,
        location,
        filterMaps,
        isLazyLoading,
        navigation,
      } = reqObj;
      const searchTerm = decodeURIComponent(seoKeywordOrCategoryIdOrSearchTerm);
      const { sort = null } = filtersAndSort;

      const facetsPayload = extractFilters(reqObj.filtersAndSort);
      const facetKeys = Object.keys(facetsPayload);
      var facetString = '';
      facetKeys.forEach((facetKey) => {
        facetString += `${facetKey}=${encodeURIComponent(facetsPayload[facetKey])}`;
      });

      const isOutfitPage = !isSearch && searchTerm && searchTerm.indexOf('-outfit') > -1;
      const row = numberOfProducts !== undefined ? numberOfProducts : PRODUCTS_PER_LOAD;
      const start =
        startProductCount !== undefined ? startProductCount : (pageNumber - 1) * PRODUCTS_PER_LOAD;

      const payload = {
        body: {
          ...facetsPayload,
          ...extraParams,
          start: 0,
          rows: row,
          variants: true,
          'variants.count': 0,
          version: 'V2',
          'facet.multiselect': true,
          selectedfacet: true,
          fields:
            'related_product_swatch_images,productimage,alt_img,style_partno,giftcard,swatchimage,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,low_offer_price, high_offer_price, low_list_price, high_list_price',
          pagetype: 'boolean',
        },
      };
      if (categoryId) {
        payload.body['p-id'] = `categoryPathId:"${categoryId}"`;
      }

      if (bucketingSeqConfig.bucketingRequired) {
        payload.body.facet = false;
      }
      if (isNoUnbxdLogic(shouldApplyUnbxdLogic, isUnbxdSequencing)) {
        if (isNoBucketing(bucketingSeqConfig)) {
          payload.body.sort = bucketingSeqConfig.bucketingSeq;
        }
      } /*else if (isNotSearchAndBucketing(isSearch, bucketingSeqConfig)) {
        payload.body.uc_param = bucketingSeqConfig.bucketingSeq;
      }*/
      if (sort) payload.body.sort = sort;

      var payloadKeys = Object.keys(payload.body);
      var queryString = '';
      payloadKeys.forEach((payloadKey) => {
        queryString += payloadKey + '=' + payload.body[payloadKey] + '&';
      });

      queryString = queryString.substring(0, queryString.length - 1);
      var filterAndCountReqUrl = unbxdUrl + '?' + queryString;

      fetch(filterAndCountReqUrl)
        .then(function (response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            window.isUnbxdRequestTriggered = false;
            return;
          }
          response.json().then(function (data) {
            const abcd = processResponse(
              data,
              url,
              reqObj.breadCrumbs,
              shouldApplyUnbxdLogic,
              bucketingSeqConfig,
              response,
              reqObj.filtersAndSort,
              isFacetCall,
              reqObj
            );
            if (isFacetCall) {
              reqObj = processProductFilterAndCountData(abcd);
              getProducts(false);
            }
          });
        })
        .catch(function (err) {
          window.isUnbxdRequestTriggered = false;
          console.log('Fetch Error :-- ', err);
        });
    }

    var operatorInstance = new ProductsOperator();
    var banner = {};
    var sortBySelected = '';
    var formData = '';
    var reqObj = operatorInstance.getProductListingBucketedData(
      location,
      sortBySelected,
      formData,
      1
    );

    if (reqObj.isFetchFiltersAndCountReq) {
      const res = getProducts(true);
    } else {
      operatorInstance.shouldApplyUnbxdLogic = false;
      getProducts(false);
    }

    var processedResponse;

    function processProductFilterAndCountData(res) {
      facetCallResponseState = {
        filtersMaps: res.filtersMaps,
        totalProductsCount: res.totalProductsCount,
        currentNavigationIds: res.currentNavigationIds,
      };
      const { categoryNameList, filtersAndSort, pageNumber, location } = reqObj;
      const updatedAvailableL3 = getUpdatedL3(res.availableL3InFilter, bucketingConfig.availableL3);
      bucketingConfig.availableL3 = [...updatedAvailableL3];
      bucketingConfig.L3Left = [...updatedAvailableL3];

      if (!bucketingConfig.L3Left.length) {
        return null;
      }

      categoryNameList.push(bucketingConfig.L3Left[0]);
      const categoryPathMap = categoryNameList
        ? categoryNameList
            .map((item) => item && (item.categoryId || item.categoryContent.id))
            .join('>')
        : '';

      return getProductsListingInfo({
        filtersAndSort,
        pageNumber,
        location,
        startProductCount: bucketingConfig.start,
        numberOfProducts: bucketingConfig.productsToFetchPerLoad,
        categoryPathMap,
        facetCallResponseState,
      });
    }

    function getAppliedFilters(filters, filterIds) {
      const appliedFilters = {};
      const facetKeys = filterIds ? Object.keys(filterIds) : [];

      facetKeys.forEach((facetKey) => {
        if (isUnbxdFacetKey(facetKey)) {
          if (facetKey === 'bopisStoreId') {
            appliedFilters[facetKey] = filterIds[facetKey];
          } else {
            appliedFilters[facetKey] = !filters[facetKey]
              ? []
              : filters[facetKey]
                  .filter((item) => filterIds[facetKey].indexOf(item.id) > -1)
                  .map((item) => item.id);
          }
        }
      });
      return appliedFilters;
    }

    // PROCESSS THE RESPONSE
    function processResponse(
      res,
      url,
      breadCrumbs,
      shouldApplyUnbxdLogic,
      bucketingSeqConfig,
      resObj,
      filtersAndSort,
      isFacetCall,
      reqObj
    ) {
      try {
        if (res.banner && ((shouldApplyUnbxdLogic && isFacetCall) || !shouldApplyUnbxdLogic)) {
          banner = res.banner;
        }
      } catch (error) {
        logger.error(error);
      }

      if (!isFacetCall) {
        window.PLP_RESPONSE_INITIAL = {
          unbxdResponse: {
            body: { ...res, banner },
            headers: resObj.headers,
            reqObj: reqObj,
            bucketingConfig: bucketingConfig,
          },
        };
        var event = new CustomEvent('unbxdResRecieved', {
          detail: {
            unbxdResponse: {
              body: { ...res, banner },
              headers: resObj.headers,
              reqObj: reqObj,
              bucketingConfig: bucketingConfig,
            },
          },
        });
        if (elem) {
          elem.dispatchEvent(event);
        } else {
          window.isUnbxdRequestTriggered = false;
        }
        return null;
      }
      var scrollPoint = 0;
      var modifiedFiltersAndSort = filtersAndSort;
      if (res.redirect && typeof window !== 'undefined') {
        var redirectUrl = res.redirect.value;
        try {
          if (redirectUrl.length && redirectUrl.indexOf(window.location.hostname) > -1) {
            redirectUrl = redirectUrl.replace(window.location.origin, '');
            redirectUrl = redirectUrl.replace(/\/(us|ca)/, '');
          } else {
            window.location.assign(redirectUrl);
          }
        } catch (e) {
          console.log('error', e);
        }
      }

      var isDepartment = !breadCrumbs || breadCrumbs.length === 1;
      var attributesNames = getProductAttributes();
      var categoryType =
        breadCrumbs && breadCrumbs.length ? breadCrumbs[breadCrumbs.length - 1].displayName : '';
      var l1category = breadCrumbs && breadCrumbs.length ? breadCrumbs[0].displayName : '';
      var filters = getFiltersAfterProcessing(
        res.facets,
        res.response.numberOfProducts,
        getFacetSwatchImgPath,
        filtersAndSort,
        l1category
      );

      var availableL3List = res.facets && res.facets.multilevel && res.facets.multilevel.bucket;
      var availableL3InFilter =
        availableL3List && availableL3List.length && (availableL3List[0].values || []);
      const displayToCustomerCategories =
        (categoryNavInfo &&
          categoryNavInfo[1] &&
          categoryNavInfo[1].subCategories &&
          categoryNavInfo[1].subCategories
            .filter((cat) => cat.displayToCustomer === false)
            .map(({ categoryId }) => categoryId)) ||
        [];
      var totalProductsCount =
        (availableL3InFilter &&
          availableL3InFilter.reduce(
            (sum, item) =>
              (item && displayToCustomerCategories.indexOf(item.id) === -1 && item.count) + sum,
            0
          )) ||
        res.response.numberOfProducts;
      if (shouldApplyUnbxdLogic && bucketingSeqConfig.bucketingRequired) {
        var productListingFilters = [];
        filters = Object.keys(productListingFilters).length ? productListingFilters : filterMaps;
      }

      var unbxdId = resObj.headers && resObj.headers['unbxd-request-id'];
      var entityCategory = {};
      var categoryNameTop = '';
      var bannerInfo = {};
      res.response.isSearch = false;
      var response = {
        availableL3InFilter,
        currentListingSearchForText: '',
        currentListingSeoKey: '',
        currentListingId:
          breadCrumbs && breadCrumbs.length
            ? breadCrumbs[breadCrumbs.length - 1].urlPathSuffix
            : '',
        currentListingName: categoryType,
        currentListingDescription:
          breadCrumbs && breadCrumbs.length
            ? breadCrumbs[breadCrumbs.length - 1].longDescription
            : '',
        currentListingType:
          breadCrumbs && breadCrumbs.length ? breadCrumbs[breadCrumbs.length - 1].displayName : '',
        isDepartment,
        outfitStyliticsTag: null,
        filtersMaps: filters,
        appliedFiltersIds: getAppliedFilters(filters, modifiedFiltersAndSort),
        totalProductsCount,
        productsInCurrCategory: res.response.numberOfProducts,
        unbxdId,
        appliedSortId: '',
        currentNavigationIds: getCurrentNavigationIds(res),
        breadCrumbTrail: getBreadCrumbTrail(breadCrumbs),
        loadedProductsPages: [[]],
        searchResultSuggestions: '',
        unbxdBanners: {},
        entityCategory,
        categoryNameTop,
        isSearch: false,
        banner,
      };
      if (res.response) {
        var isUSStore = false; // !isCanada(); will be required when processing is donee for products
        res.response.products.forEach((product) =>
          parseProductInfo(product, {
            isUSStore,
            attributesNames,
            categoryType,
            bucketingSeqConfig,
            shouldApplyUnbxdLogic,
            response,
            res,
            resObj,
          })
        );
        res.response.loadedProductCount = getLoadedProductsCount(res.response.products);
      }

      processedResponse = { ...response, banner };
      return processedResponse;
    }

    function getFacetsAPIData(facets, getFacetSwatchImgPath, numberOfProducts, filtersAndSort) {
      facets.sort((a, b) => {
        // Sort facets on position field value
        var pos = b.position > a.position ? -1 : 0;
        return a.position > b.position ? 1 : pos;
      });
      var filters = {};
      facets.forEach((facet) => {
        filters[facet.facetName] = getFacetsMappingFromAPIData(
          facet,
          getFacetSwatchImgPath,
          numberOfProducts,
          filtersAndSort[facet.facetName]
        );
      });
      return filters;
    }

    function getDisplayName(keyValue, data, index) {
      return keyValue && keyValue.length > 1 ? keyValue[1] : data[index];
    }

    function indexBasedOnShopByColor(
      isShopByColorFilter,
      index,
      data,
      numberOfProducts,
      filtersAndSort,
      val
    ) {
      return isShopByColorFilter
        ? index % 2 === 0
        : index % 2 === 0 && (data[index + 1] !== numberOfProducts || filtersAndSort.includes(val));
    }

    function parseBoolean(bool) {
      return bool === true || bool === '1' || (bool || '').toUpperCase() === 'TRUE';
    }

    function getFacetSwatchImgPath(id) {
      return `/wcsstore/GlobalSAS/images/tcp/category/color-swatches/${id}.gif`;
    }

    var FACETS_OPTIONS = {
      lowPriceProducts: '$10 and under',
    };

    function getUnbxdDisplayName(facets) {
      var facetsName = {};
      facets.forEach((facet) => {
        facetsName[facet.facetName] = facet.displayName;
      });
      return facetsName;
    }

    function getFacetsMappingFromAPIData(
      filterMap,
      getFacetSwatchImgPath,
      numberOfProducts,
      filtersAndSort = []
    ) {
      var facet = [];
      if (filterMap && filterMap.values) {
        filterMap.values.forEach((val, index, data) => {
          var facetType = filterMap.facetName;
          var isShopByColorFilter = facetType === FACETS_FIELD_KEY.aux_color_unbxd;
          /*
           ** By deafult the condition is index % 2 === 0 for all filters/facets(e.g shop by colors, SIZE, gender price).
           ** For filters other than shop by color we need filters basis on the products we have in the result set to achive the same.
           ** we added condition ((data[index + 1] !== numberOfProducts) || filtersAndSort.includes(val)) with index % 2 === 0.
           */
          var condition = indexBasedOnShopByColor(
            isShopByColorFilter,
            index,
            data,
            numberOfProducts,
            filtersAndSort,
            val
          );
          if (condition) {
            var keyValue;
            switch (facetType.toLowerCase()) {
              case FACETS_FIELD_KEY.size:
              case FACETS_FIELD_KEY.age:
                keyValue = data[index].split('_');
                facet.push({
                  displayName: getDisplayName(keyValue, data, index),
                  id: data[index],
                  facetName: facetType,
                });
                break;
              case FACETS_FIELD_KEY.color:
                facet.push({
                  displayName: data[index],
                  id: data[index],
                  imagePath: getFacetSwatchImgPath(data[index]),
                  facetName: facetType,
                });
                break;
              case FACETS_FIELD_KEY.aux_color:
                facet.push({
                  displayName: data[index],
                  id: data[index],
                  imagePath: getFacetSwatchImgPath(data[index].replace(/ /g, '_').toLowerCase()),
                  facetName: facetType,
                });
                break;
              case FACETS_FIELD_KEY.price:
                keyValue = {
                  displayName: val,
                  id: val,
                  facetName: facetType,
                };
                if (isLowPriceProduct(val)) {
                  facet.unshift(keyValue);
                } else {
                  facet.push(keyValue);
                }
                break;
              default:
                facet.push({
                  displayName: data[index],
                  id: data[index],
                  facetName: facetType,
                });
            }
          }
        });
      }
      return facet;
    }

    function isLowPriceProduct(val) {
      return val && val.toLowerCase() === FACETS_OPTIONS.lowPriceProducts;
    }

    function attributeListMaker(attributes) {
      return (
        attributes &&
        attributes.split(`;`).map((attribute) => {
          var regexUrl = /((http|https):\/\/)?(([\w.-]*)\.([\w])).*/g;
          var isUrl = regexUrl.test(attribute);
          var match = attribute.match(regexUrl);
          var url = match && match[0].split('|');
          var attAndValue = attribute.split(`:`);
          return { identifier: attAndValue[0], value: isUrl ? url : attAndValue[1] };
        })
      );
    }

    function isBopisEligibleProduct(isUSStore, swatchOfAvailableProduct, product) {
      return isBopisProduct(isUSStore, swatchOfAvailableProduct) && isGiftCard(product);
    }
    function isBossEligibleProduct(bossDisabledFlags, product) {
      return isBossProduct(bossDisabledFlags) && isGiftCard(product);
    }
    function isBossProductDisabled(product) {
      return extractAttributeValue(product, getProductAttributes().bossProductDisabled) || 0;
    }
    function isBopisProductDisabled(product) {
      return extractAttributeValue(product, getProductAttributes().bossCategoryDisabled) || 0;
    }

    function isBossProduct(bossDisabledFlags) {
      var { bossCategoryDisabled, bossProductDisabled } = bossDisabledFlags;
      return !(
        numericStringToBool(bossCategoryDisabled) || numericStringToBool(bossProductDisabled)
      );
    }

    function isBopisProduct(isUSStore, product) {
      var isOnlineOnly;
      if (isUSStore) {
        isOnlineOnly =
          (product &&
            product.TCPWebOnlyFlagUSStore &&
            parseBoolean(product.TCPWebOnlyFlagUSStore)) ||
          false;
      } else {
        isOnlineOnly =
          (product &&
            product.TCPWebOnlyFlagCanadaStore &&
            parseBoolean(product.TCPWebOnlyFlagCanadaStore)) ||
          false;
      }
      return !isOnlineOnly;
    }

    function isGiftCard(product) {
      return !!(
        product &&
        ((product.style_partno && product.style_partno.toLowerCase() === 'giftcardbundle') ||
          product.giftcard === '1')
      );
    }

    function altImageArray(imagename, altImg) {
      try {
        var altImges = JSON.parse(altImg);
        return altImges[imagename].split(',').filter((img) => img);
      } catch (error) {
        return [];
      }
    }

    function getImgPath(id, excludeExtension, imageExtension) {
      return {
        colorSwatch: null,
        productImages: getProductImagePath(id, excludeExtension, imageExtension),
      };
    }

    function getProductImgPath(id, excludeExtension) {
      return {
        125: `/wcsstore/GlobalSAS/images/tcp/products/125/${id}${excludeExtension ? '' : '.jpg'}`,
        380: `/wcsstore/GlobalSAS/images/tcp/products/380/${id}${excludeExtension ? '' : '.jpg'}`,
        500: `/wcsstore/GlobalSAS/images/tcp/products/500/${id}${excludeExtension ? '' : '.jpg'}`,
        900: `/wcsstore/GlobalSAS/images/tcp/products/900/${id}${excludeExtension ? '' : '.jpg'}`,
      };
    }

    function getProductImagePath(id, excludeExtension, imageExtension) {
      var imageName = (id && id.split('_')) || [];
      var imagePath = imageName[0];
      var extension = imageExtension ? `.${imageExtension}` : '.jpg';

      return {
        125: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        380: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        500: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
        900: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
      };
    }

    // inner function
    function parseAltImagesForColor(imageBasePath, hasShortImage, altImgs, productImage) {
      try {
        var altImages = altImageArray(imageBasePath, altImgs);
        var shortImage = [];
        var normalImage = [];
        var availableImages;
        var imageExtension = '';

        if (productImage) {
          var productImageExt = productImage.split('.');
          imageExtension = productImageExt[productImageExt.length - 1];
        }

        altImages.forEach((image) => {
          if (image.indexOf('-s') < 0) {
            normalImage.push(image);
          } else {
            shortImage.push(image);
          }
        });

        if (hasShortImage && shortImage.length) {
          availableImages = [...shortImage, ...normalImage];
        } else {
          availableImages = [imageBasePath, ...normalImage];
        }

        return availableImages.map((img) => {
          var hasExtension = img.indexOf('.jpg') !== -1 || img.indexOf('.mp4') !== -1;
          var { productImages } = getImgPath(img, hasExtension, imageExtension);

          var isOnModalImage = parseInt(img.split('-')[1], 10) > 5;

          return {
            isOnModalImage,
            iconSizeImageUrl: productImages[125],
            listingSizeImageUrl: productImages[380],
            regularSizeImageUrl: productImages[500],
            bigSizeImageUrl: productImages[900],
            superSizeImageUrl: productImages[900],
          };
        });
      } catch (error) {
        return [];
      }
    }

    function extractExtraImages(
      rawColors,
      altImgs,
      uniqueId,
      defaultColor,
      isGiftCard,
      hasShortImage,
      productImage
    ) {
      var colorsImageMap = {};
      var imageExtension = '';
      if (productImage) {
        var productImageExt = productImage.split('.');
        imageExtension = productImageExt[productImageExt.length - 1];
      }

      try {
        if (rawColors && rawColors !== '') {
          var colors = [];

          if (isGiftCard) {
            colors.push(rawColors);
          } else {
            colors = rawColors.split('|');
          }
          for (var color of colors) {
            var colorName = color.split('#')[1];
            var imageBasePath = color.split('#')[0];
            if (!colorName) {
              colorName = defaultColor;
              imageBasePath = uniqueId;
            }

            var { productImages } = getImgPath(imageBasePath, '', imageExtension);

            colorsImageMap[colorName] = {
              basicImageUrl: productImages[500],
              extraImages: parseAltImagesForColor(
                imageBasePath,
                hasShortImage,
                altImgs,
                productImage
              ),
            };
          }
        } else {
          var { productImages } = getImgPath(uniqueId, '', imageExtension);
          colorsImageMap[defaultColor] = {
            basicImageUrl: productImages[500],
            extraImages: parseAltImagesForColor(uniqueId, hasShortImage, altImgs, productImage),
          };
        }
      } catch (error) {
        console.log(error);
      }
      return colorsImageMap;
    }

    function getListPrice(swatchOfAvailableProduct) {
      return swatchOfAvailableProduct.min_list_price === swatchOfAvailableProduct.min_offer_price
        ? swatchOfAvailableProduct.min_offer_price
        : swatchOfAvailableProduct.min_list_price ||
            {
              value: null,
            }.value ||
            0;
    }

    function extractAttributeValue(item, attribute) {
      try {
        if (item.list_of_attributes) {
          var currItm = item.list_of_attributes;
          var itm = Array.isArray(currItm) ? currItm : attributeListMaker(currItm);
          var AttrIdentifier = itm.find((att) => att.identifier === attribute);
          return AttrIdentifier && AttrIdentifier.value;
        }
        if (item[attribute]) {
          return item[attribute];
        }
        return null;
      } catch (ex) {
        return '';
      }
    }

    function colorDetailsCondition(colorDetails, product, swatchOfAvailableProduct) {
      return colorDetails[0] !== product.imagename && swatchOfAvailableProduct !== undefined;
    }

    function getColorsMap({
      uniqueId,
      product,
      attributesNames,
      categoryType,
      excludeBadge,
      isBOPIS,
      bossDisabledFlags,
      defaultColor,
      isBundleProduct,
    }) {
      return [
        {
          colorProductId: uniqueId,
          imageName: product.imagename,
          productImage: product.productimage,
          miscInfo: {
            isClearance: extractAttributeValue(product, attributesNames.clearance),
            isBopisEligible: isBOPIS && isGiftCard(product),
            isBossEligible: isBossProduct(bossDisabledFlags) && isGiftCard(product),
            badge1: extractPrioritizedBadge(
              product,
              attributesNames,
              categoryType,
              excludeBadge,
              !isBundleProduct
            ),
            badge2: extractAttributeValue(product, attributesNames.extendedSize),
            badge3: extractAttributeValue(product, attributesNames.merchant),
            videoUrl: extractAttributeValue(product, attributesNames.videoUrl),
            hasOnModelAltImages: parseBoolean(
              extractAttributeValue(product, attributesNames.onModelAltImages)
            ),
            listPrice: product.min_list_price,
            offerPrice: product.min_offer_price,
            keepAlive: parseBoolean(product[attributesNames.keepAlive]),
          },
          color: {
            name: defaultColor,
            imagePath: getImgPath(isBundleProduct ? product.prodpartno : product.imagename)
              .colorSwatch,
            swatchImage: removeExtension(product.swatchimage),
          },
        },
      ];
    }

    var LANG_STRINGS = {
      PRODUCTS: {
        ATTRIBUTES: {
          CLEARANCE: {
            en: 'Clearance',
            fr: 'Liquidation',
            es: 'Liquidación',
          },
          ONLINE_ONLY: {
            en: 'Online Only',
            fr: 'Online Only',
            es: 'Solo en línea',
          },
          NEW_ARRIVALS: {
            en: 'New Arrivals',
            fr: 'Nouveautés',
            es: 'Novedades',
          },
        },
      },
    };

    function getAllLangvars(categoryType) {
      return Object.keys(categoryType).map((key) => categoryType[key]);
    }

    function getClearanceString(categoryType) {
      return getAllLangvars(LANG_STRINGS.PRODUCTS.ATTRIBUTES[categoryType]);
    }

    function getBundleBadge(hidePdpBadge, product, siteAttributes) {
      return !hidePdpBadge ? extractAttributeValue(product, siteAttributes.bundleChecklist) : '';
    }

    function isMatchingFamily(matchingFamily, excludeBadge, siteAttributes) {
      return matchingFamily && excludeBadge !== siteAttributes.matchingFamily;
    }

    function isOnlineOrClearing(isOnlineOnly, categoryType) {
      return isOnlineOnly && !getClearanceString('ONLINE_ONLY').includes(categoryType);
    }

    function checkIfClearance(clearanceOrNewArrival, categoryType) {
      return (
        clearanceOrNewArrival === 'Clearance' &&
        !getClearanceString('CLEARANCE').includes(categoryType)
      );
    }

    function extractPrioritizedBadge(
      product,
      siteAttributes,
      categoryType,
      excludeBadge,
      hidePdpBadge
    ) {
      var matchingCategory = extractAttributeValue(product, siteAttributes.matchingCategory);
      var matchingFamily = extractAttributeValue(product, siteAttributes.matchingFamily);
      var isGlowInTheDark = !!extractAttributeValue(product, siteAttributes.glowInTheDark);
      var isLimitedQuantity =
        extractAttributeValue(product, siteAttributes.limitedQuantity) === 'limited quantities';
      var isOnlineOnly = !!+extractAttributeValue(product, siteAttributes.onlineOnly);
      var clearanceOrNewArrival = extractAttributeValue(product, siteAttributes.clearance);
      var bundleBadge = getBundleBadge(hidePdpBadge, product, siteAttributes);
      var badges = {};

      if (isMatchingFamily(matchingFamily, excludeBadge, siteAttributes.matchingFamily)) {
        badges.matchBadge = matchingFamily;
      }

      if (bundleBadge) {
        badges.defaultBadge = bundleBadge;
      } else if (matchingCategory) {
        badges.defaultBadge = matchingCategory;
      } else if (isGlowInTheDark) {
        badges.defaultBadge = 'GLOW-IN-THE-DARK';
      } else if (isLimitedQuantity) {
        badges.defaultBadge = 'JUST A FEW LEFT!';
      } else if (isOnlineOrClearing(isOnlineOnly, categoryType)) {
        badges.defaultBadge = 'ONLINE EXCLUSIVE';
      } else if (checkIfClearance(clearanceOrNewArrival, categoryType)) {
        badges.defaultBadge = 'CLEARANCE';
      } else if (
        clearanceOrNewArrival === 'New Arrivals' &&
        !getClearanceString('NEW_ARRIVALS').includes(categoryType)
      ) {
        badges.defaultBadge = 'NEW!';
      }
      return badges;
    }

    function numericStringToBool(str) {
      return !!+str;
    }

    function getCategoryMap(catPath, l1) {
      var { length } = catPath;
      var catMap = {};
      for (var idx = 0; idx < length; idx += 1) {
        var temp = catPath[idx].split('>');
        catMap[temp[1]] = catMap[temp[1]] ? catMap[temp[1]] : [];
        if (temp[0] && l1 && temp[0] === l1) {
          catMap[temp[1]].push(temp[2]);
        }
      }
      return catMap;
    }
    function getRequiredL3(shouldApplyUnbxdLogic, bucketingSeqConfig, idx) {
      return shouldApplyUnbxdLogic
        ? bucketingSeqConfig.desiredL3
        : bucketingSeqConfig.requiredChildren[idx].categoryContent.name;
    }

    function checkIfL3Matches(product, requiredL3) {
      return product.categoryPath3.find((category) => category === requiredL3);
    }

    function catMapExists(temp, catMap, bucketingSeqConfig) {
      const desiredL2Val =
        bucketingSeqConfig.desiredL2 && bucketingSeqConfig.desiredL2.split('|')[0];
      return temp && catMap[desiredL2Val] && catMap[desiredL2Val].indexOf(temp) !== -1;
    }

    function getL3Category(
      shouldApplyUnbxdLogic,
      bucketingSeqConfig,
      product,
      catMap,
      childLength
    ) {
      let categoryName;
      for (let idx = 0; idx < childLength; idx += 1) {
        const requiredL3 = getRequiredL3(shouldApplyUnbxdLogic, bucketingSeqConfig, idx);
        const temp = checkIfL3Matches(product, requiredL3);
        if (catMapExists(temp, catMap, bucketingSeqConfig)) {
          categoryName = temp;
        }
        if (categoryName) {
          break;
        }
      }
      return categoryName;
    }

    function parseProductInfo(
      productArr,
      {
        isUSStore,
        hasShortImage,
        attributesNames,
        categoryType,
        excludeBadge,
        bucketingSeqConfig,
        shouldApplyUnbxdLogic,
        response,
        res,
        resObj,
      }
    ) {
      var product = productArr;
      product.list_of_attributes = attributeListMaker(product.list_of_attributes);
      var defaultColor = product.auxdescription ? product.auxdescription : product.TCPColor;
      var { uniqueId } = product;
      var colors = isUSStore
        ? convertToColorArray(product.TCPSwatchesUSStore, uniqueId, defaultColor)
        : convertToColorArray(product.TCPSwatchesCanadaStore, uniqueId, defaultColor);
      var rawColors = isUSStore ? product.TCPSwatchesUSStore : product.TCPSwatchesCanadaStore;
      var isBOPIS = isBopisEligibleProduct(isUSStore, product);
      var isBundleProduct =
        (product.product_type && product.product_type.toLowerCase() === 'bundle') || false;
      var bossDisabledFlags = {
        bossProductDisabled: isBossProductDisabled(product),
        bossCategoryDisabled: isBopisProductDisabled(product),
      };
      var imageExtension = product.productimage || '';
      var imagesByColor = extractExtraImages(
        rawColors,
        product.alt_img,
        uniqueId,
        defaultColor,
        false,
        hasShortImage,
        imageExtension
      );

      var colorsMap = getColorsMap({
        uniqueId,
        product,
        attributesNames,
        categoryType,
        excludeBadge,
        isBOPIS,
        bossDisabledFlags,
        defaultColor,
        isBundleProduct,
      });

      if (Array.isArray(colors)) {
        colors.forEach((color) => {
          var colorDetails = color.split('#');
          var swatchOfAvailableProduct = getProductByColorId(res.response.products, colorDetails);
          if (colorDetailsCondition(colorDetails, product, swatchOfAvailableProduct)) {
            colorsMap.push({
              colorProductId: colorDetails[0],
              imageName: colorDetails[0],
              miscInfo: {
                isBopisEligible: isBopisEligibleProduct(
                  isUSStore,
                  swatchOfAvailableProduct,
                  product
                ),
                isBossEligible: isBossEligibleProduct(bossDisabledFlags, product),
                hasOnModelAltImages: parseBoolean(
                  extractAttributeValue(swatchOfAvailableProduct, attributesNames.onModelAltImages)
                ),
                badge1: extractPrioritizedBadge(
                  swatchOfAvailableProduct,
                  attributesNames,
                  categoryType,
                  excludeBadge,
                  !isBundleProduct
                ),
                badge2: extractAttributeValue(
                  swatchOfAvailableProduct,
                  attributesNames.extendedSize
                ),
                badge3: extractAttributeValue(swatchOfAvailableProduct, attributesNames.merchant),
                listPrice: getListPrice(swatchOfAvailableProduct),
                offerPrice:
                  swatchOfAvailableProduct.min_offer_price ||
                  {
                    value: null,
                  }.value ||
                  0,
                keepAlive: parseBoolean(swatchOfAvailableProduct[attributesNames.keepAlive]),
              },
              color: {
                name: colorDetails[1],
                imagePath: getImgPath(colorDetails[0]).colorSwatch,
              },
            });
          }
        });
      }
      var categoryName;
      var childLength =
        bucketingSeqConfig && bucketingSeqConfig.requiredChildren
          ? bucketingSeqConfig.requiredChildren.length
          : 0;
      var catMap =
        product.categoryPath3_fq &&
        getCategoryMap(product.categoryPath3_fq, bucketingSeqConfig.desiredl1);
      if (product.categoryPath3) {
        categoryName = getL3Category(
          shouldApplyUnbxdLogic,
          bucketingSeqConfig,
          product,
          catMap,
          childLength
        );
      }
      var { isSearch } = response;
      response.loadedProductsPages[0].push({
        productInfo: {
          generalProductId: product.prodpartno || product.generalProductId,
          name: product.product_name,
          pdpUrl: `${isBundleProduct ? '/b' : '/p'}/${product.seo_token || product.uniqueId}`,
          uniqueId: product.uniqueId,
          shortDescription: product.product_short_description,
          longDescription: product.product_short_description,
          isGiftCard: isGiftCard(product),
          listPrice:
            product.min_list_price === product.min_offer_price
              ? product.min_offer_price
              : product.min_list_price || { value: null }.value || 0,
          offerPrice: product.min_offer_price || { value: null }.value || 0,
          ratings: product.TCPBazaarVoiceRating || 0,
          reviewsCount:
            (product.TCPBazaarVoiceReviewCount &&
              parseInt(product.TCPBazaarVoiceReviewCount, 10)) ||
            0,
          bundleProduct: isBundleProduct,
          unbxdId: resObj.headers && resObj.headers['unbxd-request-id'],
          promotionalMessage: product.TCPLoyaltyPromotionTextUSStore || '',
          promotionalPLCCMessage: product.TCPLoyaltyPLCCPromotionTextUSStore || '',
          long_product_title: product.long_product_title || '',
          priceRange: {
            highListPrice: parseFloat(product.high_list_price) || 0,
            highOfferPrice: parseFloat(product.high_offer_price) || 0,
            lowListPrice:
              parseFloat(product.low_list_price) || parseFloat(product.min_list_price) || 0,
            lowOfferPrice:
              parseFloat(product.low_offer_price) || parseFloat(product.min_offer_price) || 0,
          },
          isSearchFlag: false,
        },
        miscInfo: {
          rating: parseFloat(product.TCPBazaarVoiceRating) || 0,
          categoryName,
        },
        colorsMap,
        imagesByColor,
        relatedSwatchImages:
          product.related_product_swatch_images && product.related_product_swatch_images.split(','),
      });
    }

    function getProductByColorId(products, colorDetails) {
      return products.find((product) => product.prodpartno === colorDetails[0]);
    }
    function getProductAttributes() {
      var isUSStore = true;
      return isUSStore
        ? {
            merchant: 'TCPMerchantTagUSStore',
            sizes: 'TCPSizeUSStore',
            swatches: 'TCPSwatchesUSStore',
            onlineOnly: 'TCPWebOnlyFlagUSStore',
            clearance: 'TCPProductIndUSStore',
            inventory: 'TCPInventoryFlagUSStore',
            glowInTheDark: 'TCPGlowInDarkUSStore',
            limitedQuantity: 'TCPInventoryMessageUSStore',
            extendedSize: 'TCPFitMessageUSStore',
            onModelAltImages: 'TCPMarketingText1USStore',
            bossProductDisabled: 'TcpBossProductDisabled',
            bossCategoryDisabled: 'TcpBossCategoryDisabled',
            videoUrl: 'TCPMarketingText2USStore',
            matchingCategory: 'TCPProductFlagUSStore',
            matchingFamily: 'TCPMatchingFamilyUSStore',
            keepAlive: 'TCPOutOfStockFlagUSStore',
            bundleChecklist: 'TCPKitUSStore',
            bundleGrouping: 'TCPGroupingUSStore',
          }
        : {
            merchant: 'TCPMerchantTagCanadaStore',
            sizes: 'TCPSizeCanadaStore',
            swatches: 'TCPSwatchesCanadaStore',
            onlineOnly: 'TCPWebOnlyFlagCanadaStore',
            clearance: 'TCPProductIndCanadaStore',
            inventory: 'TCPInventoryFlagCanadaStore',
            glowInTheDark: 'TCPGlowInDarkUCanadaStore',
            limitedQuantity: 'TCPInventoryMessageCanadaStore',
            extendedSize: 'TCPFitMessageCanadaStore',
            onModelAltImages: 'TCPMarketingText1CanadaStore',
            videoUrl: 'TCPMarketingText2CanadaStore',
            matchingCategory: 'TCPProductFlagCAStore',
            matchingFamily: 'TCPMatchingFamilyCAStore',
            keepAlive: 'TCPOutOfStockFlagCanadaStore',
            bundleChecklist: 'TCPKitCanadaStore',
            bundleGrouping: 'TCPGroupingCanadaStore',
          };
    }

    function getFiltersAfterProcessing(
      facetsRes,
      numberOfProducts,
      getFacetSwatchImgPath,
      filtersAndSort,
      l1category
    ) {
      var filters = {};
      // varruct facets from the api response
      var facetsList = facetsRes && facetsRes.text && facetsRes.text.list && facetsRes.text.list;
      if (facetsList) {
        var facets = getFacetsAPIData(
          facetsList,
          getFacetSwatchImgPath,
          numberOfProducts,
          filtersAndSort
        );
        var unbxdDisplayName = getUnbxdDisplayName(facetsList);
        filters = {
          ...facets,
          unbxdDisplayName,
          l1category,
        };
      }
      return filters;
    }

    function colorSwatchFilter(colorSwatchesArray, id, color) {
      return colorSwatchesArray.filter((el) => {
        var duplicateSwatch = `${id}#${color}`;
        return el !== duplicateSwatch;
      });
    }

    function convertToColorArray(colorSwatches, id, color) {
      if (!colorSwatches) return [];
      return colorSwatches === 'ImagePath'
        ? colorSwatches
        : colorSwatchFilter(colorSwatches.split('|'), id, color);
    }
  })();
}
