// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { withRouter } from 'next/router'; //eslint-disable-line
import hoistNonReactStatic from 'hoist-non-react-statics';
import { fetchPageLayout, loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import { createLayoutPath, getAPIConfig } from '@tcp/core/src/utils';
import GetCandidGallery from '@tcp/core/src/components/common/molecules/GetCandidGallery/views/GetCandidGallery';
import StoresInternational from '@tcp/core/src/components/features/storeLocator/StoresInternational/container';
import Static from '../components/features/content/Static';
import STATIC_PAGE_CONSTANTS from '../components/features/content/Static/Static.constants';
import constants from '../constants';

const pathPrefix = STATIC_PAGE_CONSTANTS.STATIC_PAGE_PREFIX;
class Content extends React.Component {
  static async getInitialProps({ store, isServer, query, res = {} }) {
    const { contentType: urlPath } = query;
    let apiConfig;
    if (isServer) {
      const {
        locals: { apiConfig: config },
      } = res;
      apiConfig = config;
    } else {
      apiConfig = getAPIConfig();
    }
    store.dispatch(loadPageSEOData({ page: `/${pathPrefix}${urlPath}`, apiConfig }));
    const formattedUrlPath = createLayoutPath(urlPath);
    if (!isServer && urlPath) {
      const state = store.getState();
      if (
        !constants.staticPagesWithOwnTemplate.includes(urlPath) &&
        !state.Layouts[formattedUrlPath]
      ) {
        store.dispatch(fetchPageLayout({ pageName: `${pathPrefix}${urlPath}`, apiConfig }));
      }
    }
    return { urlPath };
  }

  render() {
    const { urlPath, setResStatusNotFound } = this.props;
    let contentComponent;
    switch (urlPath) {
      case 'mystyleplace':
        contentComponent = <GetCandidGallery />;
        break;
      case 'international-stores':
        contentComponent = <StoresInternational />;
        break;
      default:
        contentComponent = <Static urlPath={urlPath} setResStatusNotFound={setResStatusNotFound} />;
    }
    return contentComponent;
  }
}

Content.pageInfo = {
  pageId: 'content',
  staticPage: true,
  pathPrefix,
  paramName: 'contentType',
};

Content.propTypes = {
  urlPath: PropTypes.string.isRequired,
  setResStatusNotFound: PropTypes.func.isRequired,
};

const ContentWithRouter = withRouter(Content);

hoistNonReactStatic(ContentWithRouter, Content, {
  getInitialProps: true,
});

export default ContentWithRouter;
