// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
// Import styled components ServerStyleSheet
import { ServerStyleSheet } from 'styled-components';
import Safe from 'react-safe';

import { NAVIGATION_START } from '@tcp/core/src/constants/rum.constants';
import { getStoreRef } from '@tcp/core/src/utils/store.utils';
import { CHECKOUT_ROUTES } from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import {
  getIsChatBotEnabled,
  getABTestForChatBot,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
// _document is only rendered on the server side and not on the client side
// Event handlers like onClick can't be added to this file

// ./pages/_document.js
import Document, { Html, Head, Main, NextScript } from 'next/document';

// For SSR perf timing
import { getAPIConfig, getStaticFilePath, readCookie } from '@tcp/core/src/utils';
import { API_CONFIG } from '@tcp/core/src/services/config';
import langMap from '../config/languageMap';
import preconnectDomains from '../config/preconnectDomains';
import RenderPerf from '../components/common/molecules/RenderPerf';

const SafeScript = Safe.script;

// External Style Sheet
const CSSOverride = () => {
  return <link href={process.env.CSS_OVERRIDE_URL} rel="stylesheet" />;
};

const GTMScript = () => {
  return (
    <SafeScript>
      {`
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','${process.env.GTM_CONTAINER_ID}');
      `}
    </SafeScript>
  );
};

// Preload External Style Sheet
const PreLoadCSSOverride = () => {
  return <link rel="preload" href={process.env.CSS_OVERRIDE_URL} as="style" />;
};

const {
  sitesInfo: { FAVICON_URL_TCP, FAVICON_URL_GYM },
  brandIds: { tcp },
} = API_CONFIG;

const FAVICON_URL = process.env.BRANDID === tcp ? FAVICON_URL_TCP : FAVICON_URL_GYM;

function HotfixScript() {
  return (
    // eslint-disable-next-line react/jsx-pascal-case
    <SafeScript>
      {`
      window.TCP_HOTFIX_BROWSER = {};
      window.TCP_HOTFIX_PROPS = {};
      window.TCP_HOTFIX_SERVICE_REQUEST = [];
      `}
    </SafeScript>
  );
}

function SetAssetDomain() {
  return (
    <SafeScript>{`window.assetEndpointGlobal='${process.env.STATIC_ASSET_PATH}';`}</SafeScript>
  );
}

const appCookie = () => {
  return readCookie('fromMobileApp') || '';
};

function ShowHideAppDownloadBanner(props) {
  return (
    <SafeScript>
      {`
      var getProp = ${JSON.stringify(props)};
      var { isAppChannel, appCookie } = getProp;
      var isBagPage = window.location.href.includes('bag');

      function socialPage() {
        var socialPage = false;

        if (appCookie) return true;

        const staticPageUrl = [
          'twitter',
          'instagram',
          'hideHeaderFooter=true',
          'app-install',
        ];
        for (var i = 0; i < staticPageUrl.length; i += 1) {
          if (
            window &&
            window.location &&
            window.location.href.indexOf(staticPageUrl[i]) > -1
          ) {
            socialPage = true;
          }
        }
        return socialPage;
      }

      function nonCheckoutPage() {
        var isNonCheckoutPage = true;

        var {
          pickupPage,
          shippingPage,
          billingPage,
          reviewPage,
          internationalCheckout,
        } = getProp.checkoutRoute;

        var checkoutPageURL = [
          pickupPage.asPath,
          shippingPage.asPath,
          billingPage.asPath,
          reviewPage.asPath,
          internationalCheckout.asPath,
        ];

        for (var i = 0; i < checkoutPageURL.length; i += 1) {
          if (window.location.href.indexOf(checkoutPageURL[i]) > -1) {
            isNonCheckoutPage = false;
          }
        }

        return isNonCheckoutPage;
      }

      window.addEventListener('load', (event) => {
        var appBanner = document.querySelector('.app-banner-wrapper');
        const hideBanner = isBagPage || socialPage() || !nonCheckoutPage() || isAppChannel;
        if (appBanner && hideBanner === false) {
          appBanner.style.display = 'block';
        }
      });
    `}
    </SafeScript>
  );
}

function ABtestScript() {
  const { MONETATE_URL, MONETATE_CHANNEL } = process.env;
  let { MONETATE_ENABLED } = process.env;
  try {
    MONETATE_ENABLED = JSON.parse(MONETATE_ENABLED);
  } catch (e) {
    MONETATE_ENABLED = true;
  }

  if (MONETATE_ENABLED === true) {
    return (
      <SafeScript>
        {`function getPageName(e){return-1!==e.indexOf("/home/")?"homepage":-1!==e.indexOf("/store-locator")?"store-locator":-1!==e.indexOf("/store/")?"store":-1!==e.indexOf("/login")?"login":-1!==e.indexOf("/sitemap")?"sitemap":-1!==e.indexOf("/instagram")?"instagram":-1!==e.indexOf("/twitter")?"twitter":-1!==e.indexOf("/account/")?"account":-1!==e.indexOf("/favorites")?"favorites":-1!==e.indexOf("/c/")?"plp":-1!==e.indexOf("/p/")?"pdp":-1!==e.indexOf("/b/")?"bundle-product":-1!==e.indexOf("/search/")?"search":-1!==e.indexOf("/outfit/")?"outfit":-1!==e.indexOf("/place-card")?"place-card":-1!==e.indexOf("/bag")?"cart":-1!==e.indexOf("/checkout")?"checkout":"homepage"}function setAbTestScript(e){if(e.length){var t=document.getElementById("inject-abtest-script"),o=document.createRange().createContextualFragment(e);t.innerHTML="",t.appendChild(o)}}function readCookie(e){try{for(var t=e+"=",o=decodeURIComponent(document.cookie).split(";"),n=0;n<o.length;n+=1){for(var a=o[n];" "===a.charAt(0);)a=a.substring(1);if(0===a.indexOf(t))return a.substring(t.length,a.length)}return null}catch(e){return null}}var option={channel:"${MONETATE_CHANNEL}",events:[{eventType:"monetate:decision:DecisionRequest",requestId:String(Math.round(Math.random()*Math.pow(10,4))),includeReporting:!0},{eventType:"monetate:context:UserAgent",userAgent:navigator.userAgent},{eventType:"monetate:context:PageView",url:window.location.href,pageType:getPageName(window.location.href)},{eventType:"monetate:context:CustomVariables",customVariables:[{variable:"apiRequestForMonetate",value:"abtest"}]}]},monetateIdCookie=readCookie("mt.v");window&&window.localStorage.email_hash&&(window._dataLayer=window.localStorage.email_hash,option.customerId=window.localStorage.email_hash),monetateIdCookie&&(option.monetateId=monetateIdCookie);var xhr=new XMLHttpRequest;xhr.open("POST","${MONETATE_URL}"),xhr.send(JSON.stringify(option)),xhr.onload=function(){if(200===xhr.status)try{var e=JSON.parse(xhr.response).data.responses;if(!e.length)return;var t=e[0].actions;if(!t.length)return;for(var o="",n={},a=0;a<t.length;a++){var r=t[a];if(r.url&&(window.location.href=r.url),void 0===r.component&&r.json)for(var i=Object.keys(r.json),s=0;s<i.length;s++){var c=r.json[i[s]];"boolean"==typeof c||"object"==typeof c?n[i[s]]=c:"string"==typeof c&&-1!==c.indexOf("<script")&&(o=c)}}o&&setAbtestScript(o),console.log("parsedAbtestResponse:::::::",n),window.ABTest=n,window.ABTestCallComplete=!0}catch(e){console.log("exception::::::::::",e),window.ABTestCallComplete=!0,console.log("exception::::::::::",e)}},xhr.onerror=function(){console.log("error"),window.ABTestCallComplete=!0};`}
      </SafeScript>
    );
  }
  return '';
}

function GetCookieValue() {
  return (
    <SafeScript>
      {`
        !function(){var n,e=document.cookie.indexOf("mprTopHead_"+((n=function(n){for(var e=n+"=",o=decodeURIComponent(document.cookie).split(";"),t=0;t<o.length;t++){for(var r=o[t];" "==r.charAt(0);)r=r.substring(1);if(0==r.indexOf(e))return r.substring(e.length,r.length)}return""}("WC_USERACTIVITY_"))?n.split(",")[0]:"-1002"))>-1||!1;window.mpr30CookiePresent=e}();
      `}
    </SafeScript>
  );
}

function getPegaScripts(dangerousAsPath) {
  const { PEGA_MASHUP_SCRIPT_URL, PEGA_HELPER_FILE_NAME } = process.env;
  const storeRef = getStoreRef();
  let isChatBotEnabled = false;
  const store = (storeRef && storeRef.getState()) || {};
  let hasABTestForChatBot = false;
  if (Object.keys(store).length) {
    isChatBotEnabled = getIsChatBotEnabled(store);
    hasABTestForChatBot = getABTestForChatBot(store);
  }

  return isChatBotEnabled &&
    !hasABTestForChatBot &&
    !(
      dangerousAsPath.indexOf('size-chart') >= 0 &&
      dangerousAsPath.indexOf('hideHeaderFooter=true') >= 0
    ) ? (
    // eslint-disable-next-line
    <>
      <script defer type="text/javascript" src={PEGA_MASHUP_SCRIPT_URL} />
      <script
        defer
        type="text/javascript"
        src={getStaticFilePath(`scripts/chatbot/${PEGA_HELPER_FILE_NAME}.js`, 'noCookieLess')}
      />
    </>
  ) : (
    ''
  );
}

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;
    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
        });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    const { language, brandIdCMS, country, isAppChannel, envId } = getAPIConfig();
    const bodyAttributes = {
      'data-brand': brandIdCMS,
      'data-country': country,
    };
    const { dangerousAsPath = '' } = this.props;

    const isTargetEnabled = process.env.ENABLE_ADOBE_TARGET;

    return (
      <Html lang={langMap[language] || 'en'}>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <meta name="msvalidate.01" content="C19CE85E8339F59689D5E2BB0F246CEF" />

          {process.env.CSS_OVERRIDE_URL && <PreLoadCSSOverride />}

          {isTargetEnabled && (
            <link
              rel="preload"
              href={getStaticFilePath('scripts/at.min.js', 'noCookieLess')}
              as="script"
            />
          )}
          {process.env.GTM_CONTAINER_ID && <GTMScript />}
          <link rel="icon" href={getStaticFilePath(FAVICON_URL)} />
          <ABtestScript />
          {process.env.CSS_OVERRIDE_URL && <CSSOverride />}
          {preconnectDomains.map((domain) => (
            <link href={domain} rel="preconnect" crossOrigin="anonymous" />
          ))}
          <SetAssetDomain />
          {/* Empty global object definition for external hotfix sources to append */}
          <HotfixScript />
          <meta name="bv:userToken" content="" />
          <GetCookieValue />
        </Head>
        <body
          /* eslint-disable-next-line react-native/no-inline-styles */
          style={{
            position: 'relative',
            minHeight: '100%',
          }}
          {...bodyAttributes}
        >
          <Main />
          <ShowHideAppDownloadBanner
            appCookie={appCookie()}
            isAppChannel={isAppChannel}
            checkoutRoute={CHECKOUT_ROUTES}
          />
          <NextScript />
          {/* Set this in SSR for initial page view */}
          {getPegaScripts(dangerousAsPath)}
          <RenderPerf.Mark name={NAVIGATION_START} />
          {envId === 'INT' && (
            <script
              id="pega-wm-chat"
              src="https://widget.use1.chat.pega.digital/c31acc19-6320-46b9-a055-dea3393b348a/widget.js"
            />
          )}
        </body>
      </Html>
    );
  }
}

export default MyDocument;
