// 9fbef606107a605d69c0edbcd8029e5d
// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { getAPIConfig, isMobileApp } from '@tcp/core/src/utils';
import { getNavTree } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.selectors';
import SiteMapView from '@tcp/web/src/components/features/content/SiteMap/views';
import getSiteMapData from '@tcp/web/src/components/features/content/SiteMap/container/SiteMap.selectors';
import { fetchSiteMapData } from '@tcp/web/src/components/features/content/SiteMap/container/SiteMap.actions';
import { SEO_DATA } from '@tcp/core/src/reduxStore/constants';
import { loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import STATIC_PAGE_CONSTANTS from '../components/features/content/Static/Static.constants';

const pathPrefix = STATIC_PAGE_CONSTANTS.STATIC_PAGE_PREFIX;
SiteMapView.getInitialProps = async ({ store, isServer, query, res }, pageProps) => {
  let apiConfig;
  if (isServer) {
    const {
      locals: { apiConfig: config },
    } = res;
    apiConfig = config;
  } else {
    apiConfig = getAPIConfig();
  }
  const { contentType: urlPath } = query;
  store.dispatch(fetchSiteMapData({ page: `/${pathPrefix}${urlPath}`, apiConfig }));
  return {
    ...pageProps,
  };
};

SiteMapView.pageInfo = {
  pageId: 'sitemap',
};

SiteMapView.getInitActions = () => {
  return isMobileApp() ? [] : [(args) => loadPageSEOData({ page: SEO_DATA.sitemap, ...args })];
};

const mapStateToProps = (state) => {
  return {
    siteMapData: getSiteMapData(state),
    navTreeData: getNavTree(state),
  };
};

const mapDispatchToProps = () => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SiteMapView);
