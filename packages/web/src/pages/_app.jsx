/* eslint-disable max-lines, extra-rules/no-commented-out-code */
/* eslint-disable max-statements */
/* eslint-disable camelcase */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import App from 'next/app';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import { getBrandNameFromHref } from '@tcp/core/src/utils/utils';
import dynamic from 'next/dynamic';
import GlobalStyle from '@tcp/core/styles/globalStyles';
import getCurrentTheme from '@tcp/core/styles/themes';
import { BackToTop, AppBanner } from '@tcp/core/src/components/common/atoms';
import withLazyLoad from '@tcp/core/src/components/common/hoc/withLazyLoad';
import Grid from '@tcp/core/src/components/common/molecules/Grid';
import queryString from 'query-string';
import {
  bootstrapData,
  SetTcpSegmentMethodCall,
  fetchPageLayout,
  setAPIConfig,
  setBossBopisFlags,
  // loadComponentLabelsData,
  setNonInventoryStagSessionFlag,
} from '@tcp/core/src/reduxStore/actions';
import {
  getAPIConfig,
  isDevelopment,
  isGymboree,
  checkIfUserInfoCallRequired,
  isSafariBrowser,
  queryModulesThatNeedRefresh,
  extractPID,
  createLayoutPath,
  pathHasHyphenSlash,
  setBossBopisEnabled,
  getURIForDeepLink,
  parseBoolean,
  fetchStoreIdFromUrlPath,
  isMobileApp,
  getIfBotDevice,
} from '@tcp/core/src/utils';
import { initErrorReporter } from '@tcp/core/src/utils/errorReporter.util';
import { deriveSEOTags } from '@tcp/core/src/config/SEOTags.config';
import Loader from '@tcp/core/src/components/common/molecules/Loader';
import { openOverlayModal } from '@tcp/core/src/components/features/account/OverlayModal/container/OverlayModal.actions';
import { getProductDetails } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.actions';
import { getUserInfo } from '@tcp/core/src/components/features/account/User/container/User.actions';
import { CHECKOUT_ROUTES } from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import { getLocalStorage, setLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  getUserLoggedInState,
  getUserInfoFetchingState,
  getIsRegisteredUserCallDone,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import { getCookieToken } from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.selectors';
import { setNavigateCookieToken } from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.action';
import { getBootstrapError } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  HotfixBrowserContext,
  HotfixPropsContext,
} from '@tcp/core/src/components/common/context/HotfixContext';
import { readCookie, setCookie, generateSessionId } from '@tcp/core/src/utils/cookie.util';
import { API_CONFIG } from '@tcp/core/src/services/config';
import { setSessionStorage, getSessionStorage } from '@tcp/core/src/utils/utils.web';
import { fetchNavigationData } from '@tcp/core/src/components/features/content/Navigation/container/Navigation.actions';
import { getStoresByCoordinates } from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import { setAbTestData } from '@tcp/core/src/components/common/molecules/abTest/abTest.actions';
import DynamicModule from '@tcp/core/src/components/common/atoms/DynamicModule';
import { getCurrentStoreInfo } from '@tcp/core/src/components/features/storeLocator/StoreDetail/container/StoreDetail.actions';
import {
  getCurrentStore,
  formatCurrentStoreToObject,
} from '@tcp/core/src/components/features/storeLocator/StoreDetail/container/StoreDetail.selectors';
import HelpMenu from '@tcp/core/src/components/common/organisms/HelpMenu';
import GlobalModals from '../components/common/molecules/GlobalModals';
import { Header } from '../components/features/content';
import SEOTags from '../components/common/atoms';
import { configureStore } from '../reduxStore';
import RouteTracker from '../components/common/atoms/RouteTracker';
import UserTimingRouteHandler from '../components/common/atoms/UserTimingRouteHandler';
import AbTestPathTracker from '../components/common/atoms/AbTestPathTracker';
import constants from '../constants';
import setDeviceTier from '../utils/device-tiering';

const EXCLUDED_ROUTES_FOR_USER_INFO = ['/checkout'];

const Footer = dynamic(() =>
  import(
    /* webpackChunkName: "footer-chunk" */ '../components/features/content/Footer/container/Footer.container'
  )
);

/**
 * TO update the payload in case component needs to be loaded at server side
 * @param {} req
 * @param {*} payload
 * @param {*} Component
 */
const updatePayload = (req, payload, Component) => {
  let updatedPayload = { ...payload };
  const { pageInfo } = Component;
  const { staticPage, paramName, pathPrefix } = pageInfo || {};

  // This check ensures this block is executed once since Component is not available in first call
  if (pageInfo) {
    updatedPayload = {
      ...pageInfo,
      ...updatedPayload,
    };
    // This will check when page has to be rendered at server side and includes multiple urls
    if (staticPage && paramName) {
      // staticPage - this var will be passed inside component pageinfo
      // paramName - this keyword will have the variable name to page the page url from the request.
      const dynamicPageName = req.params ? req.params[paramName] : '';
      if (!constants.staticPagesWithOwnTemplate.includes(dynamicPageName) && dynamicPageName) {
        const pathName = `${pathPrefix}${dynamicPageName}`;
        updatedPayload = { ...updatedPayload, name: pathName };
      }
    }
    if (req && req.headers) {
      updatedPayload = {
        ...updatedPayload,
        pageData: {
          ...updatedPayload.pageData,
          pageReferer: req.headers.referer,
        },
      };
    }
  }
  return updatedPayload;
};

class TCPWebApp extends App {
  static siteConfigSet = false;

  constructor(props) {
    super(props);
    this.state = { prevPath: '' };
    this.theme = getCurrentTheme();
  }

  // eslint-disable-next-line complexity
  static async getInitialProps({ Component, ctx }) {
    const { req, res, store, isServer, query } = ctx;
    let reqUrl = '';

    // Error Page Handling
    if (res && res.statusCode && res.statusCode !== 200) {
      return {};
    }

    if (req) {
      ctx.req.device = {
        type: TCPWebApp.getDeviceType(req),
      };
    }
    const isBotDevice = getIfBotDevice(req);
    if (req) {
      const { url = '' } = req;
      reqUrl = url;
    }

    let productDetailState = true;
    let isValidLayoutPage = true;
    let isValidStorePage = true;
    let bootstrapPayload;

    if (isServer) {
      bootstrapPayload = TCPWebApp.createPayloadOfBootstrap(Component, ctx);
      const { apiConfig, originalUrl } = bootstrapPayload;
      let { name: layoutPageName } = bootstrapPayload;
      await store.dispatch(setAPIConfig(apiConfig));
      const asPath =
        originalUrl &&
        (originalUrl.indexOf('?') < 0
          ? originalUrl.split('/').pop()
          : originalUrl.substring(originalUrl.lastIndexOf('/') + 1, originalUrl.indexOf('?')));

      const brandName = getBrandNameFromHref(asPath);
      if (reqUrl.includes('/p/')) {
        const productID = reqUrl.split('/p/')[1];
        const productIDWithoutQuery = productID && productID.split('?')[0];
        const pid = extractPID(productIDWithoutQuery);

        const {
          unbxdUrl,
          unbxdSiteKey,
          unbxdApiKey,
          unbxdSiteKeyGYM,
          unbxdSiteKeyTCP,
          unbxdApiKeyTCP,
          unbxdApiKeyGYM,
        } = apiConfig;
        let unbxdApiKeyValue = '';
        let unbxdSiteKeyValue = '';
        let originalURL = '';
        if (brandName) {
          unbxdApiKeyValue = brandName === 'GYM' ? unbxdApiKeyGYM : unbxdApiKeyTCP;
          unbxdSiteKeyValue = brandName === 'GYM' ? unbxdSiteKeyGYM : unbxdSiteKeyTCP;
          originalURL = `${unbxdUrl}/${unbxdApiKeyValue}/${unbxdSiteKeyValue}`;
        } else {
          originalURL = `${unbxdUrl}/${unbxdApiKey}/${unbxdSiteKey}`;
        }

        if (TCPWebApp.getUnbxdKeys(unbxdSiteKey, unbxdApiKey, unbxdUrl)) {
          await new Promise((resolve) => {
            const payload = {
              productColorId: pid,
              resolve,
              originalURL,
              apiConfig,
              otherBrand: brandName,
            };
            return store.dispatch(getProductDetails({ ...payload }));
          });
          const getProductState = TCPWebApp.getProductState(store);
          productDetailState = TCPWebApp.getProductDetailsState(
            getProductState,
            productDetailState
          );
        }
      }

      if (reqUrl.includes('/store/')) {
        await TCPWebApp.triggerStorePageCall(store, query, apiConfig);
        isValidStorePage = TCPWebApp.isValidStoreData(store);
      }
      if (layoutPageName) {
        const { ssrHomeSlotRenderCount } = apiConfig;
        let isCSREnabledForHome = false;
        if (layoutPageName === 'home') {
          if (layoutPageName !== asPath && asPath.includes('home')) {
            layoutPageName = asPath;
          }
          ({ isCSREnabledForHome } = apiConfig);
        }
        await new Promise((resolve) => {
          return store.dispatch(
            fetchPageLayout({
              pageName: layoutPageName,
              layoutName: null,
              isClpPage: false,
              isCSREnabledForHome,
              resolve,
              ssrHomeSlotRenderCount,
              apiConfig,
            })
          );
        });
        const layoutState = TCPWebApp.getLayoutState(store);
        const stateLayoutPageName =
          layoutPageName && pathHasHyphenSlash(layoutPageName)
            ? createLayoutPath(layoutPageName)
            : layoutPageName;
        const contentSlots =
          layoutState && layoutState[stateLayoutPageName]
            ? layoutState[stateLayoutPageName].slots || []
            : [];
        if (!contentSlots.length) {
          isValidLayoutPage = false;
        }
      }
    }

    if (productDetailState && isValidLayoutPage && isValidStorePage) {
      let globalProps;
      try {
        globalProps = await TCPWebApp.loadGlobalData(Component, ctx, {}, bootstrapPayload);
      } catch (e) {
        globalProps = {};
      }

      let apiConfig;
      if (isServer) {
        const {
          locals: { apiConfig: config },
        } = res;
        apiConfig = config;
        const { graphql_endpoint_url_server } = apiConfig;
        logger.info(`GraphQL Endpoint Server: ${graphql_endpoint_url_server}`);
      } else {
        apiConfig = getAPIConfig();
        const { graphql_endpoint_url } = apiConfig;
        logger.info(`GraphQL Endpoint Server: ${graphql_endpoint_url}`);
      }
      const pageProps = await TCPWebApp.loadComponentData(
        Component,
        ctx,
        Object.assign(
          {
            setResStatusNotFound: () => {
              if (res) res.statusCode = 404;
            },
            isBotDevice,
            setResServerError: (errMssg) => {
              if (res) {
                res.statusCode = 500;
                res.setHeader('ErrorCode', errMssg);
              }
            },
          },
          globalProps
        ),
        apiConfig
      );
      return {
        pageProps,
      };
    }
    res.statusCode = 404;
    return {};
  }

  // eslint-disable-next-line complexity
  static createPayloadOfBootstrap(Component, { res, isServer, req }) {
    let payload = {};
    if (isServer) {
      const apiConfig = Object.assign({}, res.locals.apiConfig);
      const {
        device = {},
        originalUrl,
        headers: { cookie = {} },
      } = req;

      if (!isServer && cookie) {
        const { sessionCookieKey } = API_CONFIG;
        const sessionCookie = readCookie(sessionCookieKey, cookie);
        logger.info('QUANTUM METRIC SESSION ID:', sessionCookie);
        if (sessionCookie) {
          apiConfig.reqSessionCookie = sessionCookie;
        }
      }

      payload = {
        siteConfig: true,
        apiConfig,
        deviceType: device.type,
        originalUrl,
      };
      logger.info(
        'Global Data Loading, isPreview Mode ',
        apiConfig.isPreviewEnv,
        ',siteId',
        apiConfig.siteId
      );
      // Get initial props is getting called twice on server
      // This check ensures this block is executed once since Component is not available in first call
      // This will be called when we need to include the layout call in bootstrap.
      payload = updatePayload(req, payload, Component);
    }
    return payload;
  }

  static triggerStorePageCall = async (store, query, apiConfig) => {
    const { storeStr = '' } = query;
    return new Promise((resolve) => {
      const payload = {
        storeID: fetchStoreIdFromUrlPath(storeStr),
        resolve,
        apiConfig,
      };
      return store.dispatch(getCurrentStoreInfo(payload));
    });
  };

  static isValidStoreData = (store) => {
    const state = store.getState();
    const currentStoreInfo = getCurrentStore(state);
    const formatStore = formatCurrentStoreToObject(currentStoreInfo) || {};

    return Object.keys(formatStore).length;
  };

  static getDeviceType = (req) =>
    req && req.headers && req.headers['x-bot-user'] ? 'bot' : undefined;

  static getUnbxdKeys = (unbxdSiteKey, unbxdApiKey, unbxdUrl) =>
    unbxdSiteKey && unbxdApiKey && unbxdUrl;

  static getProductState = (store) => store.getState() && store.getState().ProductDetail;

  static getLayoutState = (store) => store.getState() && store.getState().Layouts;

  static getProductDetailsState = (getProductState, productDetailState) =>
    getProductState && getProductState.currentProduct && getProductState.currentProduct.productId
      ? productDetailState
      : false;

  // this function gets the cookietoken value passed from cross-domain in url
  getCookieToken = (asPath) => {
    // this expression is to get the query strings
    const reg = new RegExp('[?&]ct=([^&#]*)', 'i');
    const cookieToken = reg.exec(asPath);
    return cookieToken && cookieToken.length ? cookieToken[1] : null;
  };

  // this function will check if ResetPassword overlay needs to be displayed on page load
  // it will check for em and logonPasswordOld
  checkForResetPassword = () => {
    const { router, store } = this.props;
    const { asPath } = router || {};
    const { em, logonPasswordOld } = (router && router.query) || {};
    if (em && logonPasswordOld) {
      // eslint-disable-next-line no-unused-expressions
      'standalone' in window.navigator &&
        document.location.replace(
          `${
            isGymboree() ? 'gym' : 'tcp'
          }://change-password/?logonPasswordOld=${logonPasswordOld}&em=${em}`
        );
      store.dispatch(
        openOverlayModal({
          component: 'login',
          componentProps: {
            queryParams: {
              em,
              logonPasswordOld,
            },
            currentForm: 'resetPassword',
          },
        })
      );
    } else {
      store.dispatch(
        getUserInfo({
          loadFavStore: true,
          ct: this.getCookieToken(asPath),
          router,
          source: 'firstload', // flag passed in the first load of the application
        })
      );
    }
  };

  deepLinkingCheck = () => {
    const {
      router: { asPath = '' },
    } = this.props;
    const parsedURL = queryString.parseUrl(asPath);
    if (parsedURL) {
      const {
        url,
        query: { deepLink = '' },
      } = parsedURL;
      if (deepLink.toLowerCase() === 'true' && 'standalone' in window.navigator && url) {
        document.location.replace(`${isGymboree() ? 'gym' : 'tcp'}://${getURIForDeepLink(url)}`);
      }
    }
  };

  checkBootstrapErrors = () => {
    const { isServer = false, pageProps, store } = this.props;
    const bootstrapErrorObject = getBootstrapError(store.getState());
    const isErrorInBootstrap = bootstrapErrorObject && bootstrapErrorObject.isErrorInBootstrap;
    if (isErrorInBootstrap && isServer && pageProps && pageProps.setResServerError) {
      pageProps.setResServerError(bootstrapErrorObject.bootstrapErrorCode);
    }
  };

  checkForCidParam = () => {
    const { router } = this.props;
    let val = 'false';
    if (
      router &&
      router.asPath &&
      router.asPath.includes('cid') &&
      !router.asPath.includes('utm_') &&
      !router.asPath.includes('icid')
    ) {
      val = 'true';
    }
    if (typeof window !== 'undefined') {
      setSessionStorage({ key: 'cidParam', value: val });
    }
  };

  setEDDSession = () => {
    const { router } = this.props;
    let val = false;
    if (router && router.asPath && router.asPath.indexOf('showEDD') > -1) {
      val = true;
    }
    const isEddSessionEnabled = parseBoolean(getSessionStorage('EDD_ENABLED'));
    if (!isEddSessionEnabled) {
      setSessionStorage({ key: 'EDD_ENABLED', value: val });
    }
  };

  // this function will check if user not login overlay needs to be displayed on page load
  // it will check for login user
  checkForlogin = () => {
    const { router, store } = this.props;
    const { target } = (router && router.query) || {};
    const isUserLoggedIn = getUserLoggedInState(store.getState());
    const showFullPageAuth = getEnableFullPageAuth(store.getState());

    if (target === 'login' && isUserLoggedIn !== true && !showFullPageAuth) {
      store.dispatch(
        openOverlayModal({
          component: 'login',
          componentProps: 'login',
        })
      );
    }

    if (target === 'register' && isUserLoggedIn !== true && !showFullPageAuth) {
      store.dispatch(
        openOverlayModal({
          component: 'createAccount',
          componentProps: 'create-account',
        })
      );
    }
  };

  checkForCookieToken = (store) => {
    if (isSafariBrowser()) {
      const state = store.getState();
      const cookieTokenInStore = getCookieToken(state);
      const { brandId } = getAPIConfig();
      const cookieTokenInLocalStorage = getLocalStorage(`${brandId}_ct`);

      if (cookieTokenInLocalStorage && !cookieTokenInStore) {
        store.dispatch(
          setNavigateCookieToken({
            encodedcookie: cookieTokenInLocalStorage,
          })
        );
      }
    }
  };

  // Drop the 'LandingBrand' cookie
  setLandingBrandCookie = (brandId) => {
    const { landingSite } = API_CONFIG;
    const lsBrandCookie = readCookie(landingSite);
    if (lsBrandCookie) {
      return;
    }
    setCookie({
      key: landingSite,
      value: brandId,
    });
  };

  handleRouteChange = (url) => {
    const { store } = this.props;
    const isFetchingUserInfoData = getUserInfoFetchingState(store.getState());
    const isUserCallDone = getIsRegisteredUserCallDone(store.getState());
    if (
      !isUserCallDone &&
      checkIfUserInfoCallRequired(EXCLUDED_ROUTES_FOR_USER_INFO, url) &&
      !isFetchingUserInfoData
    ) {
      store.dispatch(getUserInfo());
    }
  };

  setAbTestestData = (store) => {
    if (window.ABTest && window.ABTestCallComplete) {
      const abtestObj = {
        abTest: window.ABTest || {},
        abTestComplete: true,
      };
      store.dispatch(setAbTestData(abtestObj));
      if (window.ABTest.isNonInventoryABTestEnabled) {
        store.dispatch(setNonInventoryStagSessionFlag(true));
      }
    } else {
      const dispatchTimer = setTimeout(() => {
        const abtestObj = {
          abTest: window.ABTest || {},
          abTestComplete: true,
        };
        store.dispatch(setAbTestData(abtestObj));
        if (window.ABTest && window.ABTest.isNonInventoryABTestEnabled) {
          store.dispatch(setNonInventoryStagSessionFlag(true));
        }
        clearTimeout(dispatchTimer);
      }, 1000);
    }
  };

  bindRouteChangeEvent = () => {
    const { router } = this.props;
    router.events.on('routeChangeComplete', this.handleRouteChange);
  };

  componentDidCatch(error) {
    const { sessionCookieKey } = API_CONFIG;
    logger.error('Error occurred in Web app.jsx file —> ComponentDidCatch: ', {
      error,
      extraData: {
        [sessionCookieKey]: generateSessionId(true),
        component: 'componentDidCatch inside _app.jsx',
      },
    });
  }

  componentDidMount() {
    const { store } = this.props;
    const stateCookie = readCookie('tcpState');
    const state = store.getState();
    const { session } = state;
    const { envId, raygunApiKey, channelId, isErrorReportingBrowserActive, brandId, isPreviewEnv } =
      getAPIConfig();

    // commented below line in support to validate getToken is handled from backend, in case if it won't work have to uncomment below code.
    // store.dispatch(getToken());

    try {
      if (isErrorReportingBrowserActive) {
        try {
          // eslint-disable-next-line global-require
          const rg4js = require('raygun4js');
          initErrorReporter({
            isServer: false,
            envId,
            raygunApiKey,
            channelId,
            isDevelopment: isDevelopment(),
            rg4js,
          });
        } catch (e) {
          // sometimes raygun is not initializing on client side and throwing exception,
          // which was blocking the subsequent did mount code.
          logger.error('error initializing raygun', e);
        }
      }

      if (!isPreviewEnv) {
        // skip this for preview
        logger.info('Checking for modules that need to be called on client side');
        queryModulesThatNeedRefresh(store);
      } else {
        logger.info('Skipping calls from client side as this is a preview environment.');
      }

      store.dispatch(
        fetchNavigationData({
          depth: 3,
          ignoreCache: true,
        })
      );

      this.checkForResetPassword();
      this.deepLinkingCheck();
      this.checkForlogin();
      this.bindRouteChangeEvent();
      this.checkForCidParam();
      this.checkForCookieToken(store);
      this.setLandingBrandCookie(brandId);
      store.dispatch(setBossBopisFlags(setBossBopisEnabled(stateCookie, session)));
      this.setAbTestestData(store);

      this.setEDDSession();

      window.testApp = (payload) => {
        store.dispatch(SetTcpSegmentMethodCall(payload));
      };

      window.onload = () => {
        window.ABTest = null;
      };

      window.deviceTier = setDeviceTier();
    } catch (e) {
      const { sessionCookieKey } = API_CONFIG;
      logger.error('Error occurred in Web app.jsx file —> ComponentDidMount: ', {
        error: e,
        extraData: {
          [sessionCookieKey]: generateSessionId(true),
          component: 'componentDidMount inside _app.jsx',
        },
      });
    }
  }

  componentDidUpdate() {
    this.checkForlogin();
  }

  componentWillUnmount() {
    const { router } = this.props;
    router.events.off('routeChangeComplete', this.handleRouteChange);
  }

  /**
   * This function parses cookie response
   */
  static parseCookieResponse = ({ name, value }) => {
    let itemValue;
    try {
      itemValue = JSON.parse(value);
    } catch (err) {
      itemValue = {};
    }
    return {
      [name]: itemValue,
    };
  };

  // eslint-disable-next-line complexity
  static loadGlobalData(Component, { store, isServer, req }, pageProps, payload) {
    const initialProps = pageProps;
    // getInitialProps of _App is called on every internal page navigation in spa.
    // This check is to avoid unnecessary api call in those cases
    // Get initial props is getting called twice on server
    // This check ensures this block is executed once since Component is not available in first call
    if (isServer) {
      initialProps.pageData = payload.pageData;
      store.dispatch(bootstrapData({ ...payload, headers: { ...req.headers } }));
      initialProps.BUILD_DATE = process && process.env && process.env.BUILD_DATE;
      initialProps.PAGE_GENERATION_DATE = new Date();
    }
    return initialProps;
  }

  static async loadComponentData(
    Component,
    { store, isServer, req = {}, query = '', res, asPath },
    pageProps,
    apiConfig
  ) {
    let compProps = {};
    if (Component.getInitialProps) {
      try {
        compProps = await Component.getInitialProps(
          { store, isServer, query, req, res, asPath },
          pageProps
        );
      } catch (e) {
        compProps = {};
      }
    }
    if (Component.getInitActions) {
      const actions = Component.getInitActions();
      if (isMobileApp()) {
        actions.forEach((action) => store.dispatch(action));
      } else {
        actions.forEach((action) => store.dispatch(action({ apiConfig })));
      }
    }
    return Object.assign({}, pageProps, compProps);
  }

  getSEOTags = (pageId, store, router) => {
    // Just a sample - any store specific data should be set in this
    if (pageId && pageId !== 'c' && pageId !== 'p') {
      const seoConfig = deriveSEOTags(pageId, store, router);
      return seoConfig ? <SEOTags seoConfig={seoConfig} pageId={pageId} router={router} /> : null;
    }
    return null;
  };

  checkNonCheckoutPage = (router) => {
    let isNonCheckoutPage = true;
    const { pickupPage, shippingPage, billingPage, reviewPage, internationalCheckout } =
      CHECKOUT_ROUTES;
    const checkoutPageURL = [
      pickupPage.asPath,
      shippingPage.asPath,
      billingPage.asPath,
      reviewPage.asPath,
      internationalCheckout.asPath,
    ];

    for (let i = 0; i < checkoutPageURL.length; i += 1) {
      if (router.asPath.indexOf(checkoutPageURL[i]) > -1) {
        isNonCheckoutPage = false;
      }
    }

    return isNonCheckoutPage;
  };

  checkIsListingPage = (router) => {
    const listPagePathnames = ['/ProductListing', '/SearchDetail'];
    return listPagePathnames.indexOf(router.pathname) !== -1;
  };

  checkIsBrowseAnchoredPage = (router) => {
    const browseAnchorPageNames = ['/ProductListing', '/SearchDetail', '/ProductDetail', '/Bag'];
    return browseAnchorPageNames.indexOf(router.pathname) !== -1;
  };

  checkStaticPage = (router) => {
    const fromMobileAppCookie = readCookie('fromMobileApp') || '';
    let socialPage = false;
    if (fromMobileAppCookie) {
      return true;
    }
    const staticPageUrl = ['twitter', 'instagram', 'hideHeaderFooter=true', 'app-install'];
    for (let i = 0; i < staticPageUrl.length; i += 1) {
      if (router && router.asPath.indexOf(staticPageUrl[i]) > -1) {
        socialPage = true;
      }
    }
    return socialPage;
  };

  checkOmrIDinURl = () => {
    let omRid = '';
    if (typeof window !== 'undefined') {
      const url = new URL(window.location.href);
      omRid = (url && url.searchParams && url.searchParams.get('om_rid')) || '';
      if (omRid) {
        setLocalStorage({ key: 'email_hash', value: omRid });
        /* eslint no-underscore-dangle: 0 */
        window._dataLayer.email_hash = omRid;
      }
    }
  };

  // eslint-disable-next-line complexity
  render() {
    const { Component, pageProps, store, router = {} } = this.props;
    const componentPageName = Component.pageInfo ? Component.pageInfo.name || '' : '';
    const { brandId = '', isAppChannel = false } = store.getState().APIConfig;
    const isNonCheckoutPage = this.checkNonCheckoutPage(router);
    const isListingPage = this.checkIsListingPage(router);
    const isStaticPage = this.checkStaticPage(router);
    const isBagPage = router.asPath.includes(CHECKOUT_ROUTES.bagPage.asPath);
    const isBrowseAnchoredPage = this.checkIsBrowseAnchoredPage(router);
    const checkSSR = !isAppChannel && !isStaticPage && pageProps;
    this.checkBootstrapErrors();
    const emailHashInLocalStorage = getLocalStorage('email_hash');
    const latlong = readCookie('tcpGeoLoc');
    if (latlong) {
      const [latitude, longitude] = latlong?.split('|');
      const inputparams = {
        coordinates: {
          lat: latitude,
          lng: longitude,
        },
        maxItems: 10,
        radius: 75,
        akamaiBasedLogic: true,
      };
      store.dispatch(getStoresByCoordinates(inputparams));
    }

    /* eslint no-underscore-dangle: 0 */
    if (emailHashInLocalStorage) {
      if (window) {
        window._dataLayer.email_hash = emailHashInLocalStorage;
      }
    } else {
      this.checkOmrIDinURl();
    }
    const FooterModule = pageProps && pageProps.isBotDevice ? Footer : withLazyLoad(Footer);
    return (
      <>
        <div className="dark-overlay" />
        <ThemeProvider theme={this.theme}>
          <Provider store={store}>
            <GlobalStyle />
            <AppBanner isBrowseAnchoredPage={isBrowseAnchoredPage} asPath={router.asPath} />
            <Grid wrapperClass={isNonCheckoutPage ? 'non-checkout-pages' : 'checkout-pages'}>
              {Component.pageInfo && Component.pageInfo.pageId
                ? this.getSEOTags(Component.pageInfo.pageId, store, router)
                : null}
              {!isAppChannel && !isStaticPage && (
                <>
                  <>
                    {isNonCheckoutPage ? (
                      <Header router={router} />
                    ) : (
                      <DynamicModule
                        key="checkout-header"
                        importCallback={() =>
                          import(
                            /* webpackChunkName: "checkout-header" */ '../components/features/content/CheckoutHeader'
                          )
                        }
                      />
                    )}
                  </>
                </>
              )}
              <Loader />

              {/* Provider for global hotfixes object */}
              <HotfixPropsContext.Provider value={global.TCP_HOTFIX_PROPS || {}}>
                <HotfixBrowserContext.Provider value={global.TCP_HOTFIX_BROWSER || {}}>
                  <div className={`${isNonCheckoutPage ? '' : 'full-background'}`}>
                    <div id="overlayWrapper" className="content-wrapper">
                      <div id="overlayComponent" />
                      <Component {...pageProps} pageName={componentPageName} />
                    </div>
                  </div>
                </HotfixBrowserContext.Provider>
              </HotfixPropsContext.Provider>
              {isListingPage && <BackToTop />}
              {<HelpMenu hideHelp={!isNonCheckoutPage || isBagPage || isStaticPage} />}
              {checkSSR && <FooterModule pageName={componentPageName} />}
              <GlobalModals />
            </Grid>
            {/* Inject route tracker to track number of page visits. */}
            {<RouteTracker brandId={brandId} router={router} />}
            <AbTestPathTracker />
          </Provider>
        </ThemeProvider>
        {/* Inject UX timer reporting if enabled. */}
        {process.env.PERF_TIMING && <UserTimingRouteHandler />}
      </>
    );
  }
}

export default withRedux(configureStore)(withReduxSaga(TCPWebApp));
