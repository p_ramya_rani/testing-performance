// 9fbef606107a605d69c0edbcd8029e5d
// This will only read from system vars and ./.env
require('dotenv').config();
const transpileModules = require('next-transpile-modules');
const withSourceMaps = require('@zeit/next-source-maps')();
const nextBuildId = require('next-build-id');
const path = require('path');
const webpack = require('webpack');

const isProductionBuild = process.env.NODE_ENV === 'production';
const isSourceMapsEnabled = process.env.SOURCE_MAPS_ENABLED === 'true';
const isAnalyzeBundles = process.env.ANALYZE === 'true';
const isReactProfilerEnabled = process.env.REACT_PROFILER_ENABLED === 'true';
const withTranspileModules = transpileModules(['@tcp', '../core/+/*.+.js']);

const confgWebpackPlugins = (isServer, buildId) => {
  const plugins = [];

  plugins.push(
    new webpack.IgnorePlugin({
      checkResource(resource) {
        return resource.indexOf('__mocks__') !== -1;
      },
    })
  );

  plugins.push(
    new webpack.DefinePlugin({
      'process.env.NEXT_BUILD_ID': JSON.stringify(buildId),
    })
  );

  if (!isProductionBuild) {
    // eslint-disable-next-line global-require
    const WebpackBar = require('webpackbar');
    plugins.push(
      new WebpackBar({
        name: isServer ? 'server' : 'client',
        color: isServer ? '#f7961f' : '#4ba0dd',
      })
    );
  }

  return plugins;
};

const configWebpackBundleAnalyzer = () => {
  // eslint-disable-next-line global-require
  const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
  return new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    defaultSizes: 'gzip',
    openAnalyzer: false,
    generateStatsFile: true,
    statsFilename: './analyze/stats.json',
    reportFilename: './analyze/client.html',
  });
};

let buildConfig = {
  // https://nextjs.org/docs/api-reference/next.config.js/configuring-the-build-id
  generateBuildId: () => process.env.RWD_WEB_BUILD_ID || nextBuildId({ dir: __dirname }),

  // https://nextjs.org/docs/api-reference/next.config.js/configuring-onDemandEntries
  onDemandEntries: isProductionBuild
    ? {}
    : {
        // Keep pages in the buffer for 5 mins
        maxInactiveAge: 5 * 60 * 1000,
        // Keep # pages buffer
        pagesBufferLength: 3,
      },

  // https://nextjs.org/docs/advanced-features/custom-server#disabling-file-system-routing
  useFileSystemPublicRoutes: false,

  // https://nextjs.org/docs/api-reference/next.config.js/environment-variables
  env: {
    NEXT_TELEMETRY_DISABLED: 1,
    PERF_TIMING: true, // TODO: change to process.env.PERF_TIMING
    ANALYTICS: true, // TODO: change to process.env.ANALYTICS
    DISABLE_LOGGER: isProductionBuild,
    BUILD_DATE: new Date(),
    FONTS_HOST: 'https://assets.theplace.com/rwd/', // TODO: Find a way get static asset path from APIconfig
    OPTIMISE_CART_DELETE_ENABLED: true,
  },

  // https://nextjs.org/docs/api-reference/next.config.js/custom-webpack-config
  webpack: (nextConfig, { buildId, isServer }) => {
    const config = nextConfig;

    // https://webpack.js.org/configuration/node/#node
    config.node = {
      __dirname: true,
    };

    // Fix browser bundle's `fs` dependency.
    // https://webpack.js.org/configuration/resolve/#resolvefallback
    if (!isServer) {
      config.resolve.alias.fs = path.resolve(__dirname, 'lib/fake/method.js');
    }

    // Use custom babel config.
    // https://github.com/vercel/next.js/issues/5125#issuecomment-423394719
    config.module.rules.forEach((origRule) => {
      const rule = origRule;
      if (rule.use && rule.use.loader === 'next-babel-loader') {
        rule.use.options.configFile = path.resolve('./.babelrc');
      }

      return rule;
    });

    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    config.plugins.push(...confgWebpackPlugins(isServer, buildId));

    if (!isServer && isAnalyzeBundles) {
      config.plugins.push(configWebpackBundleAnalyzer());
    }

    if (isReactProfilerEnabled) {
      config.resolve.alias['react-dom$'] = 'react-dom/profiling';
      config.resolve.alias['scheduler/tracing'] = 'scheduler/tracing-profiling';
    }

    return config;
  },
};

if (isProductionBuild && isSourceMapsEnabled) {
  buildConfig = withSourceMaps(buildConfig);
}

buildConfig = withTranspileModules(buildConfig);

module.exports = buildConfig;
