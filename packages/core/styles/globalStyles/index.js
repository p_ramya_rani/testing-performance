// 9fbef606107a605d69c0edbcd8029e5d 
import { createGlobalStyle } from 'styled-components';
import globalStyles from './commonStyles';
import gymboreeFontFaces from '../themes/Gymboree/fontFaces';
import tcpFontFaces from '../themes/TCP/fontFaces';

// eslint-disable-next-line no-unused-expressions
export default createGlobalStyle`
  ${props => (props.theme.brand === 'gym' ? gymboreeFontFaces : tcpFontFaces)}
  ${globalStyles}
`;
