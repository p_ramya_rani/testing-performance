// 9fbef606107a605d69c0edbcd8029e5d
const orange = {
  50: '#fef4e8',
  100: '#fdead2',
  300: '#fbd5a5',
  500: '#fac078',
  700: '#f9b662',
  800: '#f7971f',
  900: '#c25621',
  1000: '#dd7f4e',
};

export default orange;
