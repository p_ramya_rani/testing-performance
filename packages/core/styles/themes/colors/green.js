// 9fbef606107a605d69c0edbcd8029e5d
const green = {
  300: '#7dc24c',
  400: '#4b742d',
  500: '#26762c',
  600: '#19711f',
  200: '#d8edca',
  700: '#3c7f2e',
};

export default green;
