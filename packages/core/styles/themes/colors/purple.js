const purple = {
  50: '#ededfc',
  100: '#dbd9ff',
  300: '#beb3ff',
  500: '#a189ff',
  700: '#7b6cd3',
  800: '#7247b5',
  900: '#5c2c8f',
};

export default purple;
