// 9fbef606107a605d69c0edbcd8029e5d
// z-index
export default {
  zGoogleAutosuggest: 100000,
  zLoader: 1000,
  zOverlay: 900,
  zDrawer: 1200,
  zCondensedHeader: 1100,
  zModal: 1300,
  zModule: 200,
  zEnlargedImage: 100,
  zPLPFilterDropDown: 9,
  zIndexQuickView: 10000,
  zIndexMiniBag: 1200,
};
