// 9fbef606107a605d69c0edbcd8029e5d 
// z-index
export default {
  opacity: {
    high: 0.8, // Assumed value, not used yet, can be changed with upgrade in styleguide
    medium: 0.5,
    low: 0.3, // Assumed value, not used yet, can be changed with upgrade in styleguide
  },
};
