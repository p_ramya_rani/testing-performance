// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getFontsURL } from '@tcp/core/src/utils';

// TODO: issue on semibold 500 or 600 font; Can we set semibold === 500 or 600
export default css`
  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 500;
    font-display: swap;
    src: local('Montserrat Medium'), local('Montserrat-Medium'),
      url(${getFontsURL('fonts/Montserrat-500.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-500.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 600;
    font-display: swap;
    src: local('Montserrat SemiBold'), local('Montserrat-SemiBold'),
      url(${getFontsURL('fonts/Montserrat-600.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-600.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: local('Montserrat Bold'), local('Montserrat-Bold'),
      url(${getFontsURL('fonts/Montserrat-700.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-700.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 800;
    font-display: swap;
    src: local('Montserrat ExtraBold'), local('Montserrat-ExtraBold'),
      url(${getFontsURL('fonts/Montserrat-800.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-800.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 900;
    font-display: swap;
    src: local('Montserrat Black'), local('Montserrat-Black'),
      url(${getFontsURL('fonts/Montserrat-900.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-900.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 600;
    font-display: swap;
    src: local('Nunito SemiBold'), local('Nunito-SemiBold'),
      url(${getFontsURL('fonts/Nunito-600.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-600.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: local('Nunito Bold'), local('Nunito-Bold'),
      url(${getFontsURL('fonts/Nunito-700.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-700.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 800;
    font-display: swap;
    src: local('Nunito ExtraBold'), local('Nunito-ExtraBold'),
      url(${getFontsURL('fonts/Nunito-800.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-800.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 900;
    font-display: swap;
    src: local('Nunito Black'), local('Nunito-Black'),
      url(${getFontsURL('fonts/Nunito-900.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-900.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoCond';
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: local('TofinoCond Regular'), local('TofinoCond-Regular'),
      url(${getFontsURL('fonts/TofinoCond-Regular.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoCond-Regular.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoCond';
    font-style: normal;
    font-weight: 500;
    font-display: swap;
    src: local('TofinoCond Medium'), local('TofinoCond-Medium'),
      url(${getFontsURL('fonts/TofinoCond-Medium.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoCond-Medium.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoCond';
    font-style: normal;
    font-weight: 600;
    font-display: swap;
    src: local('TofinoCond Semibold'), local('TofinoCond-Semibold'),
      url(${getFontsURL('fonts/TofinoCond-Semibold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoCond-Semibold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoCond';
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: local('TofinoCond Bold'), local('TofinoCond-Bold'),
      url(${getFontsURL('fonts/TofinoCond-Bold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoCond-Bold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: local('TofinoWide Regular'), local('TofinoWide-Regular'),
      url(${getFontsURL('fonts/TofinoWide-Regular.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Regular.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 500;
    font-display: swap;
    src: local('TofinoWide Medium'), local('TofinoWide-Medium'),
      url(${getFontsURL('fonts/TofinoWide-Medium.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Medium.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 600;
    font-display: swap;
    src: local('TofinoWide Semibold'), local('TofinoWide-Semibold'),
      url(${getFontsURL('fonts/TofinoWide-Semibold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Semibold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: local('TofinoWide Bold'), local('TofinoWide-Bold'),
      url(${getFontsURL('fonts/TofinoWide-Bold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Bold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 900;
    font-display: swap;
    src: local('TofinoWide Black'), local('TofinoWide-Black'),
      url(${getFontsURL('fonts/TofinoWide-Black.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Black.woff')}) format('woff');
  }
`;
