// 9fbef606107a605d69c0edbcd8029e5d 
const PRIMARY_FONTS = 'Montserrat';
const SECONDARY_FONTS = 'Nunito';

export const FONTS = {
  primary: PRIMARY_FONTS,
  secondary: SECONDARY_FONTS,
};

export default {
  primary: PRIMARY_FONTS,
  secondary: SECONDARY_FONTS,
};

// Fonts in android read font file name
export const ANDROID_FONTS = {
  primary: 'montserrat',
  secondary: 'nunito',
};

export const IOS_FONTS = {
  primary: 'Montserrat',
  secondary: 'Nunito',
};
