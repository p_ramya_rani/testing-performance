// 9fbef606107a605d69c0edbcd8029e5d 
import typography from './typography';
import color from './color';

export { typography, color };
export default { typography, color };
