// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import { fromJS } from 'immutable';
import {
  SET_CLICK_PAYLOAD,
  RESET_CLICK_PAYLOAD,
  SET_PAGECATEGORY_DATA,
  SET_COMMON_ANALYTICS_DATA,
} from './actions';

const initialState = fromJS({
  clickActionAnalyticsData: null,
});

const AnalyticsReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_CLICK_PAYLOAD:
      return state
        .set('clickActionAnalyticsData', null)
        .set('clickActionAnalyticsData', action.payload);
    case RESET_CLICK_PAYLOAD:
      return state.set('clickActionAnalyticsData', null);
    case SET_PAGECATEGORY_DATA:
      return state.set('additionalData', action.payload);
    case SET_COMMON_ANALYTICS_DATA:
      return state.set('commonAnalyticsData', action.payload);
    default:
      // TODO: currently when initial state is hydrated on browser, List is getting converted to an JS Array
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default AnalyticsReducer;
