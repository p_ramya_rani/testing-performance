// 9fbef606107a605d69c0edbcd8029e5d 
import { put, takeLatest } from 'redux-saga/effects';
import { trackClickEventWithData, AnalyticsSaga } from '../Analytics.saga';
import { setClickAnalyticsData, TRACK_CLICK_WITH_DATA } from '../actions';

describe('Analytics saga', () => {
  describe('trackClickEventWithData', () => {
    it('should dispatch setClickAnalyticsData if clickData is present', () => {
      const clickData = {};
      const gen = trackClickEventWithData({ clickData });
      gen.next();
      gen.next();
      gen.next();
      const finalPutDescriptor = gen.next().value;
      expect(finalPutDescriptor).toEqual(put(setClickAnalyticsData({})));
    });
  });

  describe('AnalyticsSaga', () => {
    it('should return takeLatest effect', () => {
      const generator = AnalyticsSaga();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(TRACK_CLICK_WITH_DATA, trackClickEventWithData);
      expect(takeLatestDescriptor).toEqual(expected);
    });
  });
});
