// 9fbef606107a605d69c0edbcd8029e5d 
import { put, delay, takeLatest, select } from 'redux-saga/effects';
import { actionTypes as reduxFormActionTypes } from 'redux-form';
import _get from 'lodash/get';
import {
  trackClick,
  TRACK_CLICK_WITH_DATA,
  TRACK_FORM_ERROR,
  setClickAnalyticsData,
  trackFormError,
} from './actions';
import { getReduxFormData } from './Analytics.selectors';
import { setProp } from './utils';
import { isMobileApp } from '../utils';
import formDataConversions from './formDataConversions';

const getList3Val = ({ formName, formData }) => {
  const { formFields, formName: analyticsFormName } = formDataConversions[formName] || {};

  return Object.keys(formData).map(key => {
    const trackFieldSplit = key.split(':');
    return trackFieldSplit.length === 1 && formFields
      ? `${analyticsFormName}:${formFields[key] || key}`
      : key;
  });
};

function* trackFormErrorHandler({ payload }) {
  const { formName } = payload;
  const listVar3Values = getList3Val(payload);

  const { formName: analyticsFormName } = formDataConversions[formName] || {};

  if (analyticsFormName) {
    if (isMobileApp()) {
      yield put(
        setClickAnalyticsData({
          customEvents: ['event24'],
          pageErrorType: `form error:${analyticsFormName}`,
          list3VarValues: listVar3Values.join(','),
        })
      );
      yield put(trackClick({ name: 'form_error', module: 'global' }));
    } else {
      setProp('list3', listVar3Values);
      yield put(
        setClickAnalyticsData({
          customEvents: ['event24'],
          pageErrorType: `form error:${analyticsFormName}`,
        })
      );
      yield put(trackClick({}));
    }

    yield delay(100);
    yield put(setClickAnalyticsData({}));
  }
}

export function* trackClickEventWithData({ clickData, eventData = {} }) {
  try {
    if (clickData) {
      yield put(setClickAnalyticsData(clickData));
    }
    yield put(trackClick(eventData));
    if (clickData) {
      yield delay(100);
      return yield put(setClickAnalyticsData({}));
    }
    return null;
  } catch (err) {
    return null;
  }
}

export function* reduxFormSyncSubmitFailed(payload) {
  try {
    const {
      meta: { form: errorFormName },
    } = payload;
    const reduxFormState = yield select(getReduxFormData);

    const { values, registeredFields, fields, syncErrors } = reduxFormState[errorFormName];

    const errorFormFieldData = Object.keys(registeredFields).reduce((result, key) => {
      const resultData = result;
      if (_get(syncErrors, key) && _get(fields, key)) {
        resultData[key] = _get(values, key);
      }
      return resultData;
    }, {});

    return yield put(trackFormError({ formName: errorFormName, formData: errorFormFieldData }));
  } catch (err) {
    return null;
  }
}

export function* AnalyticsSaga() {
  yield takeLatest(TRACK_CLICK_WITH_DATA, trackClickEventWithData);
  yield takeLatest(TRACK_FORM_ERROR, trackFormErrorHandler);
  yield takeLatest(reduxFormActionTypes.SET_SUBMIT_FAILED, reduxFormSyncSubmitFailed);
}

export default AnalyticsSaga;
