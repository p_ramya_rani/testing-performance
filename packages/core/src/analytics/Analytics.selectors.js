// 9fbef606107a605d69c0edbcd8029e5d
export const getClickAnalyticsData = (state) => {
  return state.AnalyticsDataKey.get('clickActionAnalyticsData');
};

export const getReduxFormData = (state) => {
  return state.form;
};

export const getPageCategory = (state) => {
  return state.AnalyticsDataKey.getIn(['additionalData', 'pageCategory']);
};

export const getPageData = (state) => {
  return state && state.pageData;
};

export const getCommonAnalyticsData = (state) => {
  return state.AnalyticsDataKey.get('commonAnalyticsData');
};

export default { getClickAnalyticsData, getReduxFormData, getPageData, getCommonAnalyticsData };
