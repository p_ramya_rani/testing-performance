// 9fbef606107a605d69c0edbcd8029e5d 
export const ACQUISITION_ID = {
  FOOTER: {
    TCP_US: 'k32qvMXK',
    GYM_US_CHECKBOX: 'ldm7z2GQ',
    TCP_CA: 'F9dXvUaH',
    GYM_US: '3zwTyG7x',
    TCP_US_CHECKBOX: 'njY81gLq',
  },
  OVERLAY: {
    TCP_US: 'Cu18V47A',
    GYM_US_CHECKBOX: 'ZBkvoMp7',
    TCP_CA: 'OvvdDO0g',
    GYM_US: 'bVGWi5Cm',
    TCP_US_CHECKBOX: 'Ha9Vq1rU',
  },
  LANDING_PAGE: {
    TCP_US: 'PQiMAMom',
    GYM_US_CHECKBOX: '2VI65VFh',
    TCP_CA: 'teWViX10',
    GYM_US: 'b0eghGQX',
    TCP_US_CHECKBOX: 'kNIsPPPa',
  },
  ACCOUNT_PREFERENCES: {
    TCP_US_CHECKBOX: 'ug6v38H7',
    GYM_US_CHECKBOX: 'EJaJIzjp',
  },
};

export default {
  ACQUISITION_ID,
};
