// 9fbef606107a605d69c0edbcd8029e5d 
const MAP_DIRECTION_URL = 'https://maps.google.com/maps?daddr=';
const DISTANCE_API = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial`;

export default {
  MAP_DIRECTION_URL,
  DISTANCE_API,
};
