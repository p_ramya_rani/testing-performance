/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig, getBrand, getLabelValue, isCanada } from '@tcp/core/src/utils';
import { BRAND_NAMES } from '@tcp/core/src/constants/pageTitle.constants';
import PAGES from '../constants/pages.constants';
import urlConfig from './SEOCanonicalUrls.config';
import {
  getImagesToDisplay,
  getMapSliceForColorProductId,
} from '../components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';

const TCP_BASE_URL = 'https://www.childrensplace.com';
const TCP_TWITTER_SITE_TAG = '@childrensplace';
const TCP_TWITTER_SITE_CARD_TYPE = 'summary';

const GYM_BASE_URL = 'https://www.gymboree.com';
const GYM_TWITTER_SITE_TAG = '@Gymboree';
const OG_TYPE_WEBSITE = 'website';
const OG_TYPE_PRODUCT = 'product';
const GYM_TWITTER_SITE_CARD_TYPE = 'summary';
const GenericSeoPages = ['Home', 'Checkout', 'Account', 'Bag', 'StoreLocator'];
const STYLITICS_IMG_URL = 'https://content.stylitics.com/images/collage/';

const SEO_CONFIG = {
  canonical: TCP_BASE_URL,
  robots: {
    property: 'robots',
    content: 'index',
  },
  noRobots: {
    property: 'robots',
    content: 'noindex, nofollow',
  },
  viewport: {
    property: 'viewport',
    content: 'user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1',
  },
  keywords: {
    property: 'keywords',
    content: 'childrensplace',
  },
  TCP: {
    twitter: {
      cardType: 'summary',
      site: '@childrensplace',
    },
    openGraph: {
      url: TCP_BASE_URL,
      title: 'Open Graph Title',
      description: 'Open Graph Description',
    },
    hrefLangs: [
      {
        id: 'us-en',
        canonicalUrl: TCP_BASE_URL,
      },
    ],
    OTHER: {
      title: " | The Children's Place",
      description: '',
    },
  },
  GYM: {
    twitter: {
      cardType: 'summary',
      site: '@Gymboree',
    },
    openGraph: {
      url: GYM_BASE_URL,
      title: 'Open Graph Title',
      description: 'Open Graph Description',
    },
    hrefLangs: [
      {
        id: 'us-en',
        canonicalUrl: GYM_BASE_URL,
      },
    ],
    OTHER: {
      title: ' | Gymboree',
      description: '',
    },
  },
};

function getBrandDetails() {
  const { brandId, reqHostname: hostname, siteId, assetHostTCP, assetHostGYM } = getAPIConfig();
  const brand = brandId && brandId.toUpperCase();
  const BASE_URL = `https://${hostname}`;

  let BRAND_BASE_URL;
  let BRAND_TWITTER_SITE_TAG;
  let BRAND_LOGO_URL;
  let BRAND_TWITTER_SITE_CARD_TYPE;
  let BRAND_NAME;
  let BRAND_DEFAULT_TITLE_VAL;
  let BRAND_DEFAULT_DESCRIPTION_VAL;
  const BRAND_SITE = siteId;
  if (brand === 'TCP') {
    BRAND_NAME = brand;
    BRAND_BASE_URL = BASE_URL;
    BRAND_LOGO_URL = `${assetHostTCP}/w_1200,f_auto,q_auto,bo_200px_solid_rgb:afcee900/ecom/assets/static/images/favicon-tcp-cloud.png`;
    BRAND_TWITTER_SITE_TAG = TCP_TWITTER_SITE_TAG;
    BRAND_TWITTER_SITE_CARD_TYPE = TCP_TWITTER_SITE_CARD_TYPE;
    BRAND_DEFAULT_TITLE_VAL = SEO_CONFIG.TCP.OTHER.title;
    BRAND_DEFAULT_DESCRIPTION_VAL = SEO_CONFIG.TCP.OTHER.description;
  } else {
    BRAND_NAME = brand;
    BRAND_LOGO_URL = `${assetHostGYM}/w_1200,f_auto,q_auto,bo_200px_solid_rgb:afcee900/ecom/assets/static/images/favicon-gym-cloud.png`;
    BRAND_BASE_URL = BASE_URL;
    BRAND_TWITTER_SITE_TAG = GYM_TWITTER_SITE_TAG;
    BRAND_TWITTER_SITE_CARD_TYPE = GYM_TWITTER_SITE_CARD_TYPE;
    BRAND_DEFAULT_TITLE_VAL = SEO_CONFIG.GYM.OTHER.title;
    BRAND_DEFAULT_DESCRIPTION_VAL = SEO_CONFIG.GYM.OTHER.description;
  }

  return {
    BRAND_NAME,
    BRAND_BASE_URL,
    BRAND_TWITTER_SITE_TAG,
    BRAND_TWITTER_SITE_CARD_TYPE,
    BRAND_DEFAULT_TITLE_VAL,
    BRAND_DEFAULT_DESCRIPTION_VAL,
    BRAND_LOGO_URL,
    BRAND_SITE,
  };
}

const getAdditionalMetaTags = (property, content) => ({
  property,
  content,
});

const getOpenGraph = ({
  ogUrl,
  categoryUrl,
  ogTitle,
  pageTitle,
  ogDescription,
  description,
  ogType,
  ogImage,
  brandDetails,
}) => ({
  url: ogUrl || categoryUrl,
  title: ogTitle || pageTitle,
  description: ogDescription || description,
  type: ogType || OG_TYPE_WEBSITE,
  images: [{ url: ogImage || brandDetails.BRAND_LOGO_URL }],
});

const getMetaSEOTags = ({
  title,
  description,
  canonical,
  twitter,
  openGraph,
  hrefLangs,
  keywords,
  robots,
  path,
}) => {
  const brandDetails = getBrandDetails();
  const { originalPath } = getAPIConfig();
  const gRobot = path === PAGES.OUTFIT.toLowerCase() ? SEO_CONFIG.noRobots.content : robots;
  const noindex = path === PAGES.OUTFIT.toLowerCase();
  const nofollow = noindex;
  return {
    title,
    description,
    canonical: canonical || brandDetails.BRAND_BASE_URL + originalPath,
    twitter,
    openGraph,
    hrefLangs,
    noindex,
    nofollow,
    additionalMetaTags: [
      getAdditionalMetaTags(SEO_CONFIG.robots.property, gRobot),
      getAdditionalMetaTags(SEO_CONFIG.keywords.property, keywords),
      // Viewport has been coded in _document file, as it was not being applied for mobile
      // getAdditionalMetaTags(SEO_CONFIG.viewport.property, SEO_CONFIG.viewport.content),
    ],
  };
};

export const getPlpSeoTags = (getSeoMap, categoryKey, cid, imageURL) => {
  const brandDetails = getBrandDetails();
  const { seoTitle = '', seoMetaDesc = '', canonicalUrl = '' } = getSeoMap;

  const title = seoTitle;
  const description = seoMetaDesc;

  const apiConfigObj = getAPIConfig();
  const brandName = getBrand();
  const brandId = brandName && brandName.toUpperCase();
  const assetHost = apiConfigObj[`assetHost${brandId}`];
  const productAssetPath = apiConfigObj[`productAssetPath${brandId}`];
  const fullImageURL = imageURL && `${assetHost}/w_200/${productAssetPath}/${imageURL}`;

  const openGraph = {
    url: `${brandDetails.BRAND_BASE_URL}${categoryKey}`,
    title,
    description,
    type: OG_TYPE_WEBSITE,
    images: [{ url: fullImageURL || brandDetails.BRAND_LOGO_URL }],
  };
  const trimmedCategoryKey = categoryKey.replace(`/${apiConfigObj.siteId}/`, '');
  const hrefLangs = urlConfig({
    brand: brandDetails.BRAND_NAME,
    path: trimmedCategoryKey,
    withCountry: false,
  });

  const twitter = {
    cardType: `${brandDetails.BRAND_TWITTER_SITE_CARD_TYPE}`,
    site: `${brandDetails.BRAND_TWITTER_SITE_TAG}`,
  };

  let canonical;
  if (!canonicalUrl) {
    const catUrl =
      categoryKey.indexOf('?') > 1 ? categoryKey.split('?')[0] : categoryKey.split('#')[0];
    canonical = `${brandDetails.BRAND_BASE_URL}${catUrl}`;
  } else {
    canonical = canonicalUrl;
  }

  return getMetaSEOTags({
    title,
    description,
    canonical,
    twitter,
    openGraph,
    hrefLangs,
    keywords: SEO_CONFIG.keywords.content,
    robots: SEO_CONFIG.robots.content,
  });
};

// For account, bag and checkout, graphQL call has been removed for seo and are now being managed by labels
export const getSEOTags = ({ SEOData, labels, path, contentType, router }) => {
  let {
    pageTitle,
    description,
    canonicalUrl,
    keywords,
    twitterCard,
    twitterSite,
    ogTitle,
    ogUrl,
    ogType,
    ogDescription,
    ogImage,
  } = SEOData[path] || SEOData[contentType] || {};
  const { robotsInfo } = SEOData[path] || SEOData[contentType] || {};
  if (
    path === PAGES.BAG ||
    path === PAGES.CHECKOUT_PAGE ||
    path === PAGES.ACCOUNT_PAGE.toLowerCase()
  ) {
    pageTitle = getLabelValue(labels, `lbl_${path}_pageTitle`, 'seo', 'global', true);
    description = getLabelValue(labels, `lbl_${path}_description`, 'seo', 'global', true);
    canonicalUrl = getLabelValue(labels, `lbl_${path}_ canonicalUrl`, 'seo', 'global', true);
    keywords = getLabelValue(labels, `lbl_${path}_keywords`, 'seo', 'global', true);
    twitterCard = getLabelValue(labels, `lbl_${path}_twitterCard`, 'seo', 'global', true);
    twitterSite = getLabelValue(labels, `lbl_${path}_twitterSite`, 'seo', 'global', true);
    ogTitle = getLabelValue(labels, `lbl_${path}_ogTitle`, 'seo', 'global', true);
    ogUrl = getLabelValue(labels, `lbl_${path}_ogUrl`, 'seo', 'global', true);
    ogType = getLabelValue(labels, `lbl_${path}_ogType`, 'seo', 'global', true);
    ogDescription = getLabelValue(labels, `lbl_${path}_ogDescription`, 'seo', 'global', true);
    ogImage = getLabelValue(labels, `lbl_${path}_ogImage`, 'seo', 'global', true);
  }
  if (path === PAGES.OUTFIT) {
    const outfitId = router && router.query && router.query.outfitId;
    ogImage = `${STYLITICS_IMG_URL}${outfitId}/xl.jpg`;
  }

  return {
    pageTitle,
    description,
    canonicalUrl,
    keywords,
    twitterCard,
    twitterSite,
    ogTitle,
    ogUrl,
    ogType,
    ogDescription,
    ogImage,
    robotsInfo,
  };
};

const getGenericSeoTags = (store, router, categoryKey, path = 'home') => {
  const brandDetails = getBrandDetails();
  const { brandId } = getAPIConfig();
  const brand = brandId.toUpperCase();
  const { SEOData } = store.getState();
  const { contentType } = (router && router.query) || {};
  const { Labels: labels } = store.getState();
  const tags = getSEOTags({ SEOData, labels, path, contentType, router });
  const {
    pageTitle = '',
    description = '',
    canonicalUrl = '',
    keywords,
    twitterCard = '',
    twitterSite = '',
    ogTitle = '',
    ogUrl = '',
    ogType = '',
    ogDescription = '',
    ogImage = '',
    robotsInfo = '',
  } = tags || {};
  const extractcurrentPath = categoryKey.includes('?')
    ? categoryKey.split('?')[0]
    : categoryKey.split('#')[0];
  const pathUrl = extractcurrentPath.replace(/^(.+?)\/*?$/, '$1');
  const categoryUrl = `${brandDetails.BRAND_BASE_URL}${pathUrl}`;
  const openGraph = getOpenGraph({
    ogUrl,
    categoryUrl,
    ogTitle,
    pageTitle,
    ogDescription,
    description,
    ogType,
    ogImage,
    brandDetails,
    robotsInfo,
  });

  const canonicalUrlConfig = {
    brand,
    path: path === PAGES.CONTENT ? `${path}/${contentType}` : path,
  };
  const hrefLangs = urlConfig(canonicalUrlConfig);
  const twitter = {
    cardType: twitterCard || `${brandDetails.BRAND_TWITTER_SITE_CARD_TYPE}`,
    site: twitterSite || `${brandDetails.BRAND_TWITTER_SITE_TAG}`,
  };
  return getMetaSEOTags({
    title: pageTitle,
    description,
    canonical: canonicalUrl || categoryUrl,
    twitter,
    openGraph,
    hrefLangs,
    keywords,
    robots: robotsInfo || SEO_CONFIG.robots.content,
    path,
  });
};

const getPdpLabels = (labels) => {
  return {
    pdpSeoDesc: labels && labels.lblPdpSEODesc,
  };
};

const getPdpDescription = (
  isBundleProduct,
  labels,
  descriptionDynamicContent,
  productLongDescription
) => {
  let description;
  if (!isBundleProduct) {
    const pdpLabels = getPdpLabels(labels);
    const { pdpSeoDesc } = pdpLabels;
    const descriptionFinalContent =
      pdpSeoDesc && pdpSeoDesc.replace('$1', `${descriptionDynamicContent}`);
    description = !pdpSeoDesc ? `${productLongDescription}` : `${descriptionFinalContent}`;
  } else {
    description = `${productLongDescription}`;
  }
  return description;
};

const getPdpColorName = (productInfo) => {
  const { generalProductId, colorFitsSizesMap = [] } = productInfo;
  const colorName = colorFitsSizesMap.filter((res) => res.colorDisplayId === generalProductId);
  return colorName?.[0]?.color?.name || '';
};

export const getPdpSeoTags = (productInfo = {}, router, categoryKey, labels, isBundleProduct) => {
  const brandDetails = getBrandDetails();
  const categoryUrl = categoryKey && categoryKey.slice(4);

  const hrefLangs = urlConfig({
    brand: brandDetails.BRAND_NAME,
    path: categoryUrl,
    withCountry: false,
  });
  const twitter = {
    cardType: `${brandDetails.BRAND_TWITTER_SITE_CARD_TYPE}`,
    site: `${brandDetails.BRAND_TWITTER_SITE_TAG}`,
  };

  const catUrl =
    categoryKey.indexOf('?') > 1 ? categoryKey.split('?')[0] : categoryKey.split('#')[0];

  const canonical = `${brandDetails.BRAND_BASE_URL}${catUrl}`;

  const brand = getBrand();
  const productName = productInfo.name;
  const longProductTitle = productInfo.long_product_title;
  const productLongDescription = productInfo.product_long_description;
  const productTitle = longProductTitle || productName;
  const colorName = getPdpColorName(productInfo);
  const tcpTitle = `${BRAND_NAMES[brand]} ${isCanada() ? 'CA' : ''}`;
  const newtitle = productTitle ? `${productTitle} | ${tcpTitle}` : productTitle;
  const title = newtitle && colorName ? `${newtitle} - ${colorName}` : newtitle;
  const descriptionDynamicContent = `${productName} - ${colorName}`;
  const description = getPdpDescription(
    isBundleProduct,
    labels,
    descriptionDynamicContent,
    productLongDescription
  );

  let imagesToDisplay = [];
  if (productInfo && productInfo.imagesByColor) {
    const { colorFitsSizesMap, generalProductId, imagesByColor } = productInfo;
    imagesToDisplay = getImagesToDisplay({
      imagesByColor,
      curentColorEntry: getMapSliceForColorProductId(colorFitsSizesMap, generalProductId) || {},
      isAbTestActive: false,
      isFullSet: true,
    });
  }
  const brandName = getBrand();
  const brandId = brandName && brandName.toUpperCase();
  const apiConfigObj = getAPIConfig();
  const assetHost = apiConfigObj[`assetHost${brandId}`];
  const productAssetPath = apiConfigObj[`productAssetPath${brandId}`];

  const openGraph = {
    url: `${brandDetails.BRAND_BASE_URL}${categoryKey}`,
    title,
    description,
    type: OG_TYPE_PRODUCT,
    images: imagesToDisplay.map(({ regularSizeImageUrl }) => ({
      url: `${assetHost}/w_200/${productAssetPath}/${regularSizeImageUrl}`,
    })),
  };

  return getMetaSEOTags({
    title,
    description,
    canonical,
    twitter,
    openGraph,
    hrefLangs,
    keywords: SEO_CONFIG.keywords.content,
    robots: SEO_CONFIG.robots.content,
  });
};

export const deriveSEOTags = (pageId, store, router, imageURL) => {
  const asPath = router && router.asPath;
  // Please Note: Convert into switch case if you are adding more cases in this method.
  if (GenericSeoPages.indexOf(pageId) !== -1) {
    const categoryKey = asPath;
    return getGenericSeoTags(store, router, categoryKey, pageId.toLowerCase());
  }
  if (pageId === PAGES.PRODUCT_LISTING_PAGE) {
    const categoryKey = asPath;
    const { cid } = router.query;
    const getSeoMap = store; // We are passing the SeoMap value as the second parameter
    return getPlpSeoTags(getSeoMap, categoryKey, cid, imageURL);
  }
  if (pageId === PAGES.SEARCH_PAGE || pageId === PAGES.OUTFIT) {
    const categoryKey = `/${pageId}`;
    return getGenericSeoTags(store, router, categoryKey, pageId.toLowerCase());
  }
  if (pageId === PAGES.STORE) {
    const categoryKey = `/${pageId}`;
    return getGenericSeoTags(store, router, categoryKey, pageId.toLowerCase());
  }
  if (pageId === PAGES.PRODUCT_DESCRIPTION_PAGE) {
    let isBundleProduct = false;
    if (pageId === PAGES.BUNDLED_PRODUCT_DESCRIPTION_PAGE) {
      isBundleProduct = true;
    }
    const categoryKey = asPath;
    return getPdpSeoTags(store.productInfo, router, categoryKey, store.labels, isBundleProduct);
  }
  if (pageId === PAGES.STORELOCATOR) {
    const categoryKey = asPath;
    return getGenericSeoTags(store, router, categoryKey, PAGES.STOREPATH);
  }
  return getGenericSeoTags(store, router, asPath, pageId.toLowerCase());
};

export default {
  deriveSEOTags,
};
