// 9fbef606107a605d69c0edbcd8029e5d 
const badgeTranformation = 'g_west,w_0.22,fl_relative';
const IMG_DATA = {
  imgConfig: ['t_plp_img_m', 't_plp_img_t', 't_plp_img_d'],
  badgeConfig: [badgeTranformation, badgeTranformation, badgeTranformation],
};

const VID_DATA_PLP = {
  imgConfig: ['t_plp_vid_m', 't_plp_vid_t', 't_plp_vid_d'],
};

const VID_DATA_PLP_WEBP = {
  imgConfig: ['e_loop,t_plp_webp_m', 'e_loop,t_plp_webp_t', 'e_loop,t_plp_webp_d'],
};

export default {
  IMG_DATA,
  VID_DATA_PLP,
  VID_DATA_PLP_WEBP,
};
