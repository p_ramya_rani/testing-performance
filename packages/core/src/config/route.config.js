// 9fbef606107a605d69c0edbcd8029e5d
const ROUTE_PATH = {
  guestOrderDetails: ({ pathSuffix }) => `/track-order/${pathSuffix}`,
  home: '/home',
  homeOne: '/home-*',
  plp: {
    name: '/c',
    param: 'cid',
  },
  pdp: {
    name: '/p',
    param: 'pid',
  },
  outfit: {
    name: '/outfit',
  },
  search: {
    name: '/search',
    param: 'sq',
  },
  content: {
    name: '/content',
    param: 'contentType',
  },
  helpCenter: {
    name: '/help-center',
    param: 'pageName',
  },
  bundleDetail: {
    name: '/b',
    param: 'bid',
  },
  account: {
    name: '/account',
    params: ['id', 'subSection', 'orderId'],
  },
  storeLocator: {
    name: '/store-locator',
    param: '',
  },
  // TODO - Make all page constants and use them in ROUTES_LIST for mapping
};

// DO NOT USE IMPORT IN IT, This file is run on node as well.
// Although this is a web specific file but need to move it to core to be used by all core components as well.

const preRouteSlugs = ['/:siteId?'];
const ROUTES_LIST = [
  // NOTE - This list is being used to make ROUTING_MAP as well,
  // if changing any route, make sure MAP is not referring to it.
  // For ex: ROUTING_MAP.home or ROUTING_MAP.error are being referred
  {
    noSlugPath: 'home',
    path: '/home/:target?',
    resolver: '/index',
    params: ['target'],
  },
  {
    noSlugPath: 'home-one',
    path: '/home-:pageId(\\w+)/:target?',
    resolver: '/index',
    params: ['pageId', 'target'],
  },
  {
    noSlugPath: 'store-locator',
    path: '/store-locator',
    resolver: '/storeLocator',
  },
  {
    noSlugPath: 'store',
    path: '/store/:storeStr?',
    resolver: '/storeDetail',
    params: ['storeStr'],
  },
  {
    noSlugPath: 'login',
    path: '/login',
    resolver: '/login',
  },
  {
    noSlugPath: 'sitemap',
    path: '/sitemap',
    resolver: '/sitemap',
  },
  {
    noSlugPath: 'instagram',
    path: '/instagram',
    resolver: '/instagram',
  },
  {
    noSlugPath: 'twitter',
    path: '/twitter',
    resolver: '/twitter',
  },

  {
    noSlugPath: 'account',
    path: '/account/:id?/:subSection?/:orderId?',
    resolver: '/account',
    params: ['id', 'subSection', 'orderId'],
  },
  {
    noSlugPath: 'favorites',
    path: '/favorites',
    resolver: '/Favorites',
  },
  {
    noSlugPath: 'ds',
    path: '/ds',
    resolver: '/DeltaSyncSamplePage',
  },
  {
    noSlugPath: 'place-card/application',
    path: '/place-card/application',
    resolver: '/ApplyCardPage',
  },
  {
    noSlugPath: 'c',
    path: '/c/:cid/:pageNumber?',
    resolver: '/ProductListing',
    params: ['cid'],
  },
  {
    noSlugPath: 'p',
    path: '/p/:pid',
    resolver: '/ProductDetail',
    params: ['pid'],
  },
  {
    noSlugPath: 'b',
    path: '/b/:bid',
    resolver: '/BundleProduct',
    params: ['bid'],
  },
  {
    noSlugPath: 'search',
    path: '/search/:searchQuery?',
    resolver: '/SearchDetail',
    params: ['searchQuery'],
  },
  {
    noSlugPath: 'outfit',
    path: '/outfit/:outfitId/:vendorColorProductIdsList',
    resolver: '/OutfitDetails',
    params: ['outfitId', 'vendorColorProductIdsList'],
  },
  {
    noSlugPath: 'place-card',
    path: '/place-card',
    resolver: '/WebInstantCredit',
  },
  {
    noSlugPath: 'place-card-application',
    path: '/place-card/application',
    resolver: '/ApplyCardPage',
  },
  {
    noSlugPath: 'test',
    path: '/test',
    resolver: '/test',
  },
  {
    noSlugPath: 'bag',
    path: '/bag',
    resolver: '/Bag',
  },
  {
    noSlugPath: 'customer-help',
    path: '/customer-help/account-and-coupons/:reasonCode?/:emailId?',
    resolver: '/CustomerHelp',
    params: ['reasonCode', 'emailId'],
  },
  {
    noSlugPath: 'customer-help',
    path: '/customer-help/:section?/:orderId?/:emailId?/:reasonCode?',
    resolver: '/CustomerHelp',
    params: ['section', 'orderId', 'emailId', 'reasonCode'],
  },
  {
    noSlugPath: 'cookies-testing',
    path: '/cookies-testing',
    resolver: '/cookiesTesting',
  },
  {
    noSlugPath: 'checkout',
    path: '/checkout/:section?',
    resolver: '/Checkout',
    params: ['section'],
  },
  {
    noSlugPath: 'error',
    path: '/error',
    resolver: '/error',
  },
  {
    noSlugPath: 'international-checkout',
    path: '/international-checkout',
    resolver: '/InternationalCheckout',
  },
  {
    noSlugPath: 'content',
    path: '/content/:contentType',
    resolver: '/content',
    params: ['contentType'],
  },
  {
    noSlugPath: 'stores',
    path: '/stores',
    resolver: '/StoreList',
  },
  {
    noSlugPath: 'change-password',
    path: '/change-password',
    resolver: '/ChangePassword',
  },
  {
    noSlugPath: 'track-order',
    path: '/track-order/:orderId/:emailAddress/:fromEpsilon?',
    resolver: '/TrackOrder',
    params: ['orderId', 'emailAddress', 'fromEpsilon'],
  },
  {
    noSlugPath: 'help-center',
    path: '/help-center/:pageName#?',
    resolver: '/HelpCenter',
    params: ['pageName'],
  },
  {
    noSlugPath: 'app-install',
    path: '/app-install',
    resolver: '/AppInstall',
  },
  {
    noSlugPath: 'checkout-session-invalidate',
    path: '/checkout-session-invalidate',
    resolver: '/redirectContentPage',
  },
  {
    noSlugPath: 'gift-card-balance',
    path: '/gift-card-balance',
    resolver: '/giftCardBalance',
  },
];

const ROUTING_MAP = {};
ROUTES_LIST.forEach(({ noSlugPath, resolver }) => {
  ROUTING_MAP[noSlugPath] = resolver;
});

module.exports = { ROUTES_LIST, preRouteSlugs, ROUTING_MAP, ROUTE_PATH };
