// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable complexity, sonarjs/cognitive-complexity */
import superagent from 'superagent';
import Throttle from 'superagent-throttle';
import {
  getQMSessionIdFromDefaultPreference,
  getUserAgent,
  isIOS,
  isClient,
  isMobileApp,
  getCurrentRouteName,
  getSiteId,
  getAPIConfig,
  getAkamaiSensorData,
} from '@tcp/core/src/utils';
import { getStoreRef } from '@tcp/core/src/utils/store.utils';
import { API_CONFIG, securedAkamaiEndpoints, superAgentThrottleConfig } from '../../config';
import { readCookie } from '../../../utils/cookie.util';
import verifyErrorResponseHandler from './verifyErrorResponse';
import ErrorConstructor from '../../../utils/errorConstructor.util';
import { API_ERROR_MESSAGE, TCP_APP } from './config';
import endpoints from '../../endpoints';

const throttle = new Throttle(superAgentThrottleConfig);

/**
 * @summary this is meant to generate a new UID on each API call
 * @param {string} apiConfig - Api config to be utilized for brand/channel/locale config
 * @returns {string} returns generated traceId of User or else not-found string value

 */
const generateTraceId = apiConfig => {
  const apiConfigObj = apiConfig;
  let prefix;

  // Setting prefix of trace-id based on platform of user i.e. either mobile, browser, Node
  if (isMobileApp()) {
    prefix = 'MOBILE';
  } else if (isClient()) {
    prefix = 'CLIENT';
  } else {
    prefix = 'NODE';
  }
  const timeStamp = new Date().valueOf().toString();

  // On the Node Server traceIdCount can grow to Infinity, so we will reset it at 10000
  if (apiConfigObj.traceIdCount > 10000) {
    apiConfigObj.traceIdCount = 0;
  }

  const traceIdCount = apiConfigObj.traceIdCount + 1;
  const traceId = `${prefix}_${traceIdCount}_${timeStamp}`;
  return traceId || 'not-found';
};

/**
 * @summary this is ONLY used for tieing back logs to a user, this value should not be used for any other application use
 * @param {string} apiConfig - Api config to be utilized for brand/channel/locale config
 * @returns {string} returns derived QuantumMetricSessionID from cookie or else not-found string value
 */
const generateSessionId = apiConfig => {
  const { sessionCookieKey } = API_CONFIG;
  // TODO - Check if it works in Mobile app as well or else change it to isServer check
  const cookieValue = readCookie(sessionCookieKey, !isClient() ? apiConfig.cookie : null);
  return encodeURIComponent(cookieValue || 'not-found');
};

const shouldEnableAkamaiBMP = (reqObj, apiConfig) => {
  const { enableAkamaiBMP } = apiConfig;
  const path = reqObj && reqObj.webService && reqObj.webService.URI;
  return enableAkamaiBMP && path && securedAkamaiEndpoints.indexOf(path) > -1;
};

/**
 * @method getPathContext
 * @description Generate the path context based on contextual information such as
 * url for web, screen name for mobile.
 * Examples:
 * Web: gym-us-home
 * Mobile: gym-us-home
 *
 * @returns {string} Hyphen-separated path context (lower-cased)
 */
const getPathContext = () => {
  let pathContext = '';
  let splitPath = [];

  const hasTwoLevelRoutes = ['account', 'checkout', 'content', 'help-center'];
  const apiConfig = getAPIConfig();
  const storeRef = getStoreRef();
  const CHECKOUT = 'checkout';
  /**
   * Path Context for web consists of 2 parts:
   * 1. Brand ID
   * 2. Site ID --> splitPath[1]
   * 3. Page group (home, help-center, c, p, b, outfit etc.) --> splitPath[2]
   * 4. Sub page group name (for specific pages, like account, checkout etc.) --> splitPath[3]
   *
   * For Mobile - we are going with:
   * 1. Brand ID
   * 2. Site ID
   * 3. Route Name
   *
   */
  if (isClient()) {
    /* Web CSR Scenario */
    splitPath = window.location.pathname.split('/');
    if (splitPath.length >= 3 && hasTwoLevelRoutes.indexOf(splitPath[2]) > -1 && splitPath[3]) {
      pathContext = `${apiConfig.brandId}-${splitPath[1]}-${splitPath[2]}-${splitPath[3]}`;
    } else {
      pathContext = `${apiConfig.brandId}-${splitPath[1]}-${splitPath[2]}`;
    }
  } else if (
    /* For Checkout getCurrentRouteName always gives CheckoutPickup and routing is handled via redux state */
    getCurrentRouteName()
      .toLowerCase()
      .includes(CHECKOUT)
  ) {
    /* Getting the checkout stage from the redux state */
    const checkoutStage = storeRef && storeRef.getState().Checkout.getIn(['uiFlags', 'stage']);
    const checkoutState = `${CHECKOUT}-${checkoutStage}`;
    pathContext = `${apiConfig.brandId}-${getSiteId()}-${checkoutState}`;
  } else {
    pathContext = `${apiConfig.brandId}-${getSiteId()}-${getCurrentRouteName()}`;
  }

  return pathContext.toLowerCase();
};

/**
 * @summary This is to generate and return both the request params and the request URL.
 * @param {string} apiConfig - Api config to be utilized for brand/channel/locale config
 * @param {Object} reqObj - request param with endpoints and payload
 * @returns {Object} returns derived request object and request url
 */

const getRequestParams = async (apiConfig, reqObj) => {
  const { domain, catalogId, storeId, langId, isMobile, apiEndpoint, deviceID } = apiConfig;
  const deviceType = isMobile ? 'mobile' : 'desktop'; // TODO - Make it general for Mobile, APP, Desktop
  // Getting Session Id for Web and App (If web will be false will get fot App)
  const sessionId = getQMSessionIdFromDefaultPreference() || generateSessionId(apiConfig);
  const requestUrl = reqObj.skipApiIdentifier
    ? `${apiEndpoint}${reqObj.webService.URI}`
    : `${domain}${reqObj.webService.URI}`;
  const reqHeaders = {
    langId,
    storeId,
    Pragma: 'no-cache',
    Expires: 0,
    catalogId,
    deviceType,
    'Cache-Control': 'no-store, must-revalidate',
    'tcp-trace-request-id': generateTraceId(apiConfig),
    'tcp-trace-session-id': sessionId,
  };

  if (isMobileApp()) {
    reqHeaders.deviceType = 'App';
    const userAgentString = getUserAgent();
    const platform = isIOS() ? 'IOS' : 'Android';
    const customUserAgent = `My Place/18.0.0/${platform} deviceType/MobileApp`;
    // Added headers so that Akamai rule can differentiate between mobile web and mobile app
    // reqHeaders['User-Agent'] = `${userAgentString}/${TCP_APP}`;
    reqHeaders['User-Agent'] = `${userAgentString}/${TCP_APP}/${customUserAgent}`;
    reqHeaders['x-tcp-app-rwd'] = true;
    // header tcp-app-type, customUserAgent in UA is patch fix for Akamai BMP
    reqHeaders['tcp-app-type'] = 'mobile-app';

    if (shouldEnableAkamaiBMP(reqObj, apiConfig)) {
      const sensorData = await getAkamaiSensorData();
      if (sensorData) {
        reqHeaders['x-acf-sensor-data'] = sensorData;
      }
    }
    /*
      added as a temporary solution to distinguish GYM & TCP orders
      in future this will be removed and deviceType attribute will be altered once BE changes
      to accommodate the change is done
    */
    if (reqObj.webService.URI === endpoints.checkout.URI) {
      reqHeaders.mobileAppType =
        apiConfig.brandId === API_CONFIG.brandIds.gym ? 'GYMAPP' : 'TCPAPP';

      reqHeaders['gmp-device-id'] = deviceID;
    }
  }

  // TODO - Check if it works in Mobile app as well or else change it to isServer check
  if (apiConfig.cookie && !isClient()) {
    reqHeaders.Cookie = apiConfig.cookie;
  }

  reqHeaders.client_source = getPathContext();

  return {
    requestUrl,
    reqHeaders,
  };
};

/**
 * @summary This is to initialise superagent client to consume the stateful data.
 * @param {string} apiConfig - Api config to be utilized for brand/channel/locale config
 * @param {Object} reqObj - request param with endpoints and payload
 * @returns {Promise} Resolves with promise to consume the stateful api or reject in case of error
 */
const StatefulAPIClient = async (apiConfig, reqObj) => {
  const { requestUrl, reqHeaders } = await getRequestParams(apiConfig, reqObj);
  const reqTimeout = API_CONFIG.apiRequestTimeout;
  const requestType = reqObj.webService.method.toLowerCase();
  const request = superagent[requestType](requestUrl)
    .use(throttle.plugin())
    .set(reqHeaders)
    .accept(API_CONFIG.apiContentType)
    .timeout(reqTimeout);
  if (reqObj.header) {
    request.set(reqObj.header);
  }
  request.withCredentials();
  if (requestType === 'get') {
    request.query(reqObj.body);
  } else {
    request.send(reqObj.body);
  }
  const result = new Promise((resolve, reject) => {
    request
      .then(response => {
        const errorObject = verifyErrorResponseHandler(response);
        if (errorObject.errorCode) {
          throw new ErrorConstructor({
            ...errorObject,
            errorMsg: API_ERROR_MESSAGE,
            errorResponse: response.body,
            networkStatusCode: response.status,
          });
        }
        resolve(response);
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ err, reqObj, reqHeaders });
      });
  });
  result.abort = () => request.abort(); // allow callers to cancel the request by calling abort on the returned object.
  return result;
};

export default StatefulAPIClient;
