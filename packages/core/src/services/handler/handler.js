// 9fbef606107a605d69c0edbcd8029e5d
import logger from '@tcp/core/src/utils/loggerInstance';
import { API_CONFIG } from '@tcp/core/src/services/config';
import { generateSessionId } from '../../utils/cookie.util';
import { graphQLClient } from '../api.constants';
import QueryBuilder from './graphQL/queries/queryBuilder';
import {
  importGraphQLClientDynamically,
  getAPIConfig,
  getQMSessionIdFromDefaultPreference,
  isMobileApp,
  isServer,
} from '../../utils';
import StatefulAPIClient from './stateful/statefulClient';
import UnbxdAPIClient from './unbxd/unbxdClient';
import ExternalAPIClient from './external/externalClient';

/**
 * Logs error
 * @param {*} e error object
 */
const errorHandler = ({
  err,
  reqObj = {
    webService: {
      URI: 'GRAPHQL QUERY || URI_NOT_SENT_IN_ERROR_LOGGING',
    },
  },
  reqHeaders = {
    'tcp-trace-request-id': 'NO-TRACE-ID',
    'tcp-trace-session-id': 'NO-TRACE-ID',
  },
} = {}) => {
  // unbxd-request-id
  const { sessionCookieKey } = API_CONFIG;
  let unbxdReqId = 'N/A';
  const sessionId = isMobileApp() ? getQMSessionIdFromDefaultPreference() : generateSessionId();
  if (err && err.response && err.response.headers) {
    unbxdReqId = err.response.headers['unbxd-request-id'];
  }
  logger.error({
    error: err,
    errorTags: [`API Handler-${reqObj.webService.URI}`, sessionId],
    extraData: {
      ...reqObj,
      'trace-request-id': reqHeaders['tcp-trace-request-id'],
      'trace-session-id': reqHeaders['tcp-trace-session-id'],
      'unbxd-request-id': unbxdReqId,
      [sessionCookieKey]: !isMobileApp() && generateSessionId(true),
    },
  });
  throw err;
};

/**
 * Executes query on GraphQL Client
 * @param {*} query
 * @param {*} graphQLInterface
 */
const executeQuery = ({
  query,
  fetchPolicy,
  graphQLInterface,
  cacheTag,
  isContextual,
  apiConfig,
}) => {
  const { sessionCookieKey } = API_CONFIG;
  try {
    const client = graphQLInterface(apiConfig);
    return client.executeQuery(query, fetchPolicy, cacheTag, isContextual, apiConfig);
  } catch (err) {
    logger.error({
      error: err,
      errorTags: [`Execute Query - ${cacheTag}`],
      extraData: {
        [sessionCookieKey]: !isMobileApp() && generateSessionId(true),
      },
    });
    throw err;
  }
};

/**
 * Loads GraphQL client interface
 */
const loadGraphQLInterface = () => {
  const clientName = graphQLClient;
  return importGraphQLClientDynamically(clientName).then(
    ({ default: graphQLInterface }) => graphQLInterface
  );
};

/**
 * Executes a graph ql query
 * @param {*} QueryBuilder GraphQL query builder which returns query wrapped with graphql-tag, passed in form of a default export
 * @returns {Promise} Resolves with data or rejects with error object
 */
export const executeGraphQLQuery = ({
  query,
  fetchPolicy,
  cacheTag,
  isContextual,
  apiConfig: config,
}) => {
  let apiConfig = config;

  if (typeof config === 'undefined' || Object.keys(config).indexOf('brandId') < 0) {
    if (isServer()) {
      // send the log message only if server side error condition
      logger.error({
        error: `apiConfig is undefined or contains bad data for cacheTag: ${cacheTag}`,
        errorTags: ['executeGraphQLQuery', 'apiConfig', cacheTag],
        extraData: {
          cacheTag,
        },
      });
    }
    apiConfig = getAPIConfig();
  }

  return loadGraphQLInterface()
    .then((client) =>
      executeQuery({
        query,
        graphQLInterface: client,
        fetchPolicy,
        cacheTag,
        isContextual,
        apiConfig,
      })
    )
    .catch((err) => {
      const reqObj = {
        webService: {
          URI: 'GRAPHQL QUERY',
          query,
        },
      };
      errorHandler({
        err,
        reqObj,
      });
    });
};

/**
 * Fetches Queries based on passed module, then executes the query and returns a promise for query execution
 * @param {*} moduleName Module for which query needs to be executed
 * @returns {Promise} Resolves with data or rejects with error object
 */
export const fetchModuleDataFromGraphQL = async (modules, apiConfig, isContextual) => {
  logger.debug(`modules: ${JSON.stringify(modules)}\n\napiConfig: ${JSON.stringify(apiConfig)}`);
  const queryData = await QueryBuilder.getQuery(modules, apiConfig);
  return executeGraphQLQuery({
    query: queryData.query,
    fetchPolicy: queryData.cacheType,
    cacheTag: queryData.cacheTag,
    isContextual,
    apiConfig,
  });
};

/**
 * Just fetches Queries based on passed module for logging, and it doesn't executes the query
 * @param {*} moduleName Module for which query needs to be executed
 * @returns {Promise} Resolves with data or rejects with error object
 */
export const fetchRawDataFromGraphQL = async (modules, apiConfig) => {
  return QueryBuilder.getRawQuery(modules, apiConfig);
};

/**
 * @summary Fetches Queries based on passed module, then executes the query and returns a promise for query execution
 * @param {Object} reqObj request param with endpoints and payload
 * @returns {Promise} Resolves with unbxd or stateful client based on request object or returns null
 */
export const executeStatefulAPICall = (reqObj, errHandler) => {
  if (!reqObj.webService) {
    return null;
  }
  const apiConfigObj = getAPIConfig();
  return StatefulAPIClient(apiConfigObj, reqObj).catch(errHandler || errorHandler);
};

export const executeUnbxdAPICall = (reqObj, originalURL, xappURLConfig, config) => {
  if (!reqObj.webService) {
    return null;
  }
  const apiConfigObj = config || getAPIConfig();
  return UnbxdAPIClient(apiConfigObj, reqObj, originalURL, xappURLConfig).catch(errorHandler); // TODO - Make a new Instance and for GRAPHQL as well..
};

export const executeExternalAPICall = (reqObj) => {
  if (!reqObj.webService) {
    return null;
  }
  const apiConfigObj = getAPIConfig();
  return ExternalAPIClient(apiConfigObj, reqObj).catch(errorHandler);
};

export default {
  fetchModuleDataFromGraphQL,
  fetchRawDataFromGraphQL,
  executeGraphQLQuery,
};
