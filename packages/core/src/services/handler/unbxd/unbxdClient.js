// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable no-underscore-dangle */
import superagent from 'superagent';
import Throttle from 'superagent-throttle';
import RequestTime from 'superagent-node-http-timings';
import logger from '@tcp/core/src/utils/loggerInstance';
import { readCookie } from '../../../utils/cookie.util';
import { API_CONFIG, superAgentThrottleConfig } from '../../config';
import { isClient, isMobileApp, getBrand, getApiTimeOut, isServer } from '../../../utils';
import requestHotfixer from '../../plugins/requestHotfixer';

const throttle = new Throttle(superAgentThrottleConfig);

const modifyUnbxdUrl = (unboxKey) => {
  const temp = unboxKey.split('/');
  temp.splice(0, 1, 'sites');
  return temp.join('/');
};
/**
 * @summary This is to generate and return both the request params and the request URL.
 * @param {string} apiConfig - Api config to be utilized for brand/channel/locale config
 * @param {Object} reqObj - request param with endpoints and payload
 * @returns {Object} returns derived request object and request url
 */
const getRequestParams = (apiConfig, reqObj, originalURL, xappURLConfig = {}) => {
  const {
    webService: { URI, unbxdCustom, authHeaderRequired, clientTimeout, serverTimeout },
  } = reqObj;
  const { isFlipURL, drURLpath } = xappURLConfig;
  let { brand } = reqObj;
  if (!brand) {
    const { brand: brandName } = reqObj && reqObj.body;
    brand = brandName;
  }
  if (!brand) {
    const brandID = getBrand();
    brand = brandID && brandID.toUpperCase();
  }
  const brandUnboxKey = apiConfig[`unboxKey${brand}`];
  const unboxKey = unbxdCustom ? modifyUnbxdUrl(brandUnboxKey) : brandUnboxKey;
  const unbxdKey = isFlipURL ? drURLpath : apiConfig[`unbxd${brand}`];
  const originalUrlPath = `${originalURL}/${URI}`;
  const newUrlPath = `${unbxdKey}/${unboxKey}/${URI}`;
  const isUrlPathSame = originalUrlPath === newUrlPath;
  const requestUrl =
    !originalURL || !isUrlPathSame ? `${unbxdKey}/${unboxKey}/${URI}` : `${originalURL}/${URI}`;
  const unbxdAPIKey = apiConfig[`unbxdApiKey${brand}`];

  const reqHeaders = authHeaderRequired
    ? {
        Authorization: unbxdAPIKey,
      }
    : {};

  // TODO - Check if it works in Mobile app as well or else change it to isServer check
  if (apiConfig.cookie && !isClient()) {
    reqHeaders.Cookie = apiConfig.cookie;
  }

  const reqTimeout =
    getApiTimeOut({ clientTimeout, serverTimeout }) || API_CONFIG.apiRequestTimeout;
  return {
    requestUrl,
    reqHeaders,
    reqTimeout,
  };
};

const getRequestQuery = (request) => {
  if (request._query && request._query.length > 0) {
    request._query[0] = decodeURIComponent(request._query[0]);
  }

  return request._query[0];
};

const getRequestURLforMobile = (reqObj, requestUrl, requestType) => {
  const params = reqObj.body;
  const queryString = Object.keys(params)
    .map((key) => {
      let appendedValues = '';
      if (key === 'filter') {
        if (typeof params[key] === 'string') {
          appendedValues += `${key}=${params[key]}`;
        } else {
          params[key].forEach((item, index) => {
            if (index === params[key].length - 1) {
              appendedValues += `${key}=${item}`;
            } else {
              appendedValues += `${key}=${item}&`;
            }
          });
        }
      } else {
        appendedValues += `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
      }
      return appendedValues;
    })
    .join('&');

  let requestURL = requestUrl;

  if (requestType === 'get') {
    requestURL = `${requestUrl}?${queryString}`;
  }

  return requestURL;
};
/**
 * @summary This is to initialise superagent client to consume the unbxd data.
 * @param {string} apiConfig - Api config to be utilized for brand/channel/locale config
 * @param {Object} reqObj - request param with endpoints and payload
 * @returns {Promise} Resolves with promise to consume the unbxd api or reject in case of error
 */

// eslint-disable-next-line sonarjs/cognitive-complexity
const UnbxdAPIClient = (apiConfig, reqObj, originalURL, xappURLConfig) => {
  const { requestUrl, reqHeaders, reqTimeout } = getRequestParams(
    apiConfig,
    reqObj,
    originalURL,
    xappURLConfig
  );

  const requestType = reqObj.webService.method.toLowerCase();

  // Making custom URL for mobile devices for solving IOS crash issue.
  // Don't require to pass request object in request.query method of superagent
  // as in IOS, encodeURI is again executed on request url which encodes it again.
  // Therefore, for Mobileapp, encoding all the request object manually & for filters, the value is being encoded in  ProductLisitng.js
  const requestURL = isMobileApp()
    ? getRequestURLforMobile(reqObj, requestUrl, requestType)
    : requestUrl;

  const request = superagent[requestType](requestURL)
    .use(throttle.plugin())
    .set(reqHeaders)
    .accept(API_CONFIG.apiContentType)
    .timeout(reqTimeout)
    .use(requestHotfixer);

  if (!isMobileApp() && isServer()) {
    const logNetworkTime = new RequestTime(
      // eslint-disable-next-line consistent-return
      (err, res) => {
        if (err) {
          logger.error(`Error in unbxd call: ${err}`);
        } else {
          logger.info(`Unbxd call time: ${res.timings.total}ms`);
        }
      }
    );
    request.use(logNetworkTime);
  }

  if (reqObj.header) {
    request.set(reqObj.header);
  }

  // make the api call
  if (requestType === 'get') {
    const unbxdUID =
      !isMobileApp() && isClient() ? readCookie('unbxd.userId', document && document.cookie) : '';
    if (isClient() && unbxdUID) {
      // eslint-disable-next-line
      reqObj.body.uid = unbxdUID;
    } else {
      // eslint-disable-next-line
      // reqObj.body.uid = 'uid-1563946353348-89276';
    }

    if (!isMobileApp()) {
      request.query(reqObj.body);
      request._query[0] = getRequestQuery(request);
    }
  } else {
    request.send(reqObj.body);
  }

  const result = new Promise((resolve, reject) => {
    request
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ err, reqObj, reqHeaders });
      });
  });
  result.abort = () => request.abort(); // allow callers to cancel the request by calling abort on the returned object.
  return result;
};

export default UnbxdAPIClient;
