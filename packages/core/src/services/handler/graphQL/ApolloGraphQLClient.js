// 9fbef606107a605d69c0edbcd8029e5d
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import ApolloLinkTimeout from 'apollo-link-timeout';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createPersistedQueryLink } from 'apollo-link-persisted-queries';
import {
  isMobileApp,
  isClient,
  isServer,
  getCurrentRouteName,
  getSiteId,
  generateTraceId,
  getCustomHeaders,
} from '@tcp/core/src/utils';
import fetch from 'node-fetch';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getBrandSwitchedState } from '@tcp/core/src/reduxStore/selectors/global.selectors';

import { getStoreRef } from '@tcp/core/src/utils/store.utils';

import { setBrandSwitched } from '@tcp/core/src/reduxStore/actions';

import {
  getAPIConfig,
  getAmznTraceId,
  getQMSessionIdFromDefaultPreference,
} from '../../../utils/utils';
import { generateSessionId } from '../../../utils/cookie.util';
import {
  CACHE_TAG_HEADER,
  QUERY_TYPE_HEADER,
  TRACE_ID_HEADER,
  SESSION_ID_HEADER,
} from '../../../constants/headers.constants';

let apolloClient = null;

if (!process.browser) {
  global.fetch = fetch;
}

const getapiTimeout = (serverTimeout, clientTimeout) => {
  const apiConfigVal = isClient() ? clientTimeout : serverTimeout;
  return apiConfigVal || 30000;
};

/**
 * Class ApolloAppSyncClient which extends AWSAppSync client package
 */
class ApolloAppSyncClient extends ApolloClient {
  constructor(options) {
    super({ ...options });
    this.setCacheTimers();
  }

  /**
   * @function setCacheTimers
   * This method sets/updates the interval timers for clearing client cache
   * (Apollo store cache)
   *
   * @static
   * @memberof ApolloAppSyncClient
   */
  setCacheTimers = () => {
    const apiConfig = getAPIConfig();
    let cacheInterval = 0;
    try {
      cacheInterval = parseInt(apiConfig.GRAPHQL_CLIENT_CACHE_CLEAR_INTERVAL, 10);
    } catch (e) {
      logger.error({
        error: e,
        extraData: {
          methodName: 'ApolloAppSyncClient - setCacheTimers',
          cacheInterval: apiConfig.GRAPHQL_CLIENT_CACHE_CLEAR_INTERVAL,
        },
      });
      cacheInterval = 15;
    }
    if ((isClient() || isMobileApp()) && cacheInterval > 0) {
      if (this.clearStoreTimer) {
        clearInterval(this.clearStoreTimer);
      }

      this.clearStoreTimer = setInterval(async () => {
        await this.clearStore();
      }, cacheInterval * 60 * 1000); // cacheInterval is configured in minutes
    }
  };

  /**
   * Generate the path context based on contextual information such as
   * url for web, screen name for mobile
   *
   * Examples:
   * Web: layout-gym-us-web-home, seo-data-gym-us-web-help-center
   * Mobile: layout-gym-us-mobile-home, seo-data-gym-us-mobile-help-center
   *
   * @returns {string} Hyphen-separated path context (lower-cased)
   */
  getPathContext = (apiConfig) => {
    let pathContext = '';
    let splitPath = [];

    /**
     * Path Context for web consists of 2 parts:
     * 1: Site ID --> splitPath[1]
     * 2: Channel: "web"
     * 3. Page group (home, help-center, c, p, b, outfit etc.) --> splitPath[2]
     *
     * For Mobile - we are going with:
     * 1. Site ID
     * 2. Channel: "mobile"
     * 3. Route Name
     *
     * Page group/Route Name is the max level of granularity we are going with for cache tag path context
     */
    if (isServer()) {
      const { originalPath } = apiConfig;
      /* Web SSR Scenario */
      // Fix for DRP-5070 - do not use httpContext here as it gives wrong information when concurrent requests are processed
      splitPath = originalPath.split('/');
      pathContext = `${splitPath[1]}-web-${splitPath[2]}`;
    } else if (isClient()) {
      /* Web CSR Scenario */
      splitPath = window.location.pathname.split('/');
      pathContext = `${splitPath[1]}-web-${splitPath[2]}`;
    } else {
      /* Mobile App */
      pathContext = `${getSiteId()}-mobile-${getCurrentRouteName()}`;
    }

    return pathContext.toLowerCase();
  };

  /**
   * Generate the full cache tag string based on contextual information
   * such as Path Context, Brand Id etc.
   *
   * @param {string} cacheTagPrefix
   * @param {boolean} isContextual
   * @returns {string} Full cache tag string
   */
  generateCacheTag = (cacheTagPrefix, isContextual, apiConfig) => {
    // "module" not part of the following list - as there are dangling calls for module such as referred content etc.
    // For page level module calls - we're using "isContextual" param (details below)
    const PAGE_INFO_CACHE_TAGS = ['layout', 'seo-data', 'sub-navigation'];

    let enhancedCacheTag;

    // "isContextual" argument is sent as true for "page level module calls" and in future -
    // other calls which are bound to page context
    if (PAGE_INFO_CACHE_TAGS.indexOf(cacheTagPrefix) !== -1 || isContextual) {
      const pathContext = this.getPathContext(apiConfig);
      enhancedCacheTag = `${cacheTagPrefix}-${apiConfig.brandId}-${pathContext}`;
    } else {
      enhancedCacheTag = `${cacheTagPrefix}-${apiConfig.brandId}-${apiConfig.siteId}`;
    }
    return enhancedCacheTag;
  };

  /**
   * This function executes graphQL query
   * @param {*} query GraphQL query
   * @returns {Promise} Resolves with data or rejects with error object
   */
  executeQuery(query, fetchPolicy, cacheTag, isContextual, apiConfig) {
    let config;
    if (isServer()) {
      config = apiConfig;
    } else {
      config = getAPIConfig();
    }
    const rawQuery = query.loc.source.body;
    const headers = {};

    if (cacheTag) {
      const enhancedCacheTag = this.generateCacheTag(cacheTag, isContextual, config);
      headers[CACHE_TAG_HEADER] = enhancedCacheTag;
      headers[QUERY_TYPE_HEADER] = `${cacheTag}-${config.brandId}-${config.siteId}`;
    }

    return new Promise((resolve, reject) => {
      const startTime = new Date().getTime();
      const q = this.query({
        query,
        context: {
          headers,
        },
        fetchPolicy: isServer() ? 'no-cache' : fetchPolicy,
        errorPolicy: 'all',
      });

      q.then((data) => {
        const queryDuration = new Date().getTime() - startTime;
        if (config.enableGqlWarn && config.gqlWarnThreshold < queryDuration) {
          logger.info(
            `[SLOW-API] GraphQL slowness detected. Took ${queryDuration}msec to process below query:${rawQuery}`
          );
        }
        resolve(data); // resolve here so that receiver of promise can handle
      }).catch((e) => {
        reject(e); // reject here so that receiver of promise can handle
      });
    });
  }
}

const clientOptions = (config) => {
  const apiConfigObj = config || getAPIConfig();
  const {
    graphql_endpoint_url_server: graphqlEndpointServer,
    graphql_endpoint_url: graphqlEndpointClient,
    graphql_api_key: graphqlApiKey,
    graphql_region: graphqlRegion,
    graphql_auth_type: graphqlAuthType,
  } = apiConfigObj;

  const graphQLEndpoint = isServer() ? graphqlEndpointServer : graphqlEndpointClient;

  return {
    url: graphQLEndpoint,
    region: graphqlRegion,
    auth: {
      type: graphqlAuthType,
      apiKey: graphqlApiKey,
    },
    disableOffline: true,
  };
};

const additionalOptions = (config) => {
  const apiConfig = config || getAPIConfig();
  let disablePersistedQuery = false;
  let persistedQueryLink;

  if (apiConfig.graphql_persisted_query === 'false') {
    disablePersistedQuery = true;
  }

  // Getting Session Id for Web and App (If web will be false will get fot App)
  const sessionId =
    getQMSessionIdFromDefaultPreference() ||
    generateSessionId(true) ||
    apiConfig.reqSessionCookie ||
    'not-available';

  const customHeaders = isMobileApp() ? getCustomHeaders() : {};

  const httpLink = createHttpLink({
    headers: customHeaders,
    uri: clientOptions(apiConfig).url,
    fetchOptions: {
      mode: isClient() ? 'cors' : 'no-cors',
    },
  });

  const logRequestType = (request, headers) => {
    const { extensions } = request;
    let requestType;
    if (extensions && extensions.persistedQuery) {
      requestType = `Request: GET | sha256Hash: ${extensions.persistedQuery.sha256Hash}`;
    } else {
      requestType = 'Request: POST';
    }
    logger.info(
      `Apollo Request | Cache Tag: ${headers[CACHE_TAG_HEADER]} | Query Type: ${headers[QUERY_TYPE_HEADER]} | ${requestType}`
    );
  };

  const customHeaderLink = setContext((request, { headers = null }) => {
    logRequestType(request, headers);

    let traceId = generateTraceId();

    const amznTraceId = getAmznTraceId(traceId);
    if (amznTraceId) {
      traceId = `${traceId}_${amznTraceId}`;
    }

    return {
      headers: {
        ...headers,
        [TRACE_ID_HEADER]: traceId,
        [SESSION_ID_HEADER]: sessionId,
      },
    };
  });

  const headerLink = customHeaderLink.concat(httpLink);

  const apiTimeout = getapiTimeout(
    apiConfig.GRAPHQL_API_TIMEOUT,
    apiConfig.GRAPHQL_API_TIMEOUT_CLIENT
  );
  const timeoutLink = new ApolloLinkTimeout(apiTimeout);
  const withTimeOutLink = timeoutLink.concat(headerLink);

  if (!disablePersistedQuery) {
    persistedQueryLink = createPersistedQueryLink({ useGETForHashedQueries: true }).concat(
      withTimeOutLink
    );
  }

  return {
    link: disablePersistedQuery ? withTimeOutLink : persistedQueryLink,
    cache: new InMemoryCache(),
  };
};

const ApolloClientInstance = (apiConfig) => {
  const storeRef = getStoreRef();
  let isBrandSwitched = false;
  const store = (storeRef && storeRef.getState()) || {};
  if (Object.keys(store).length) {
    isBrandSwitched = getBrandSwitchedState(store);
  }

  if (isBrandSwitched || !apolloClient || isServer()) {
    const options = additionalOptions(apiConfig);
    apolloClient = new ApolloAppSyncClient(options);
    if (isBrandSwitched) {
      storeRef.dispatch(setBrandSwitched(false));
    }
  }
  return apolloClient;
};

export default ApolloClientInstance;
