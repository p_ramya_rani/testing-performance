import constants from '../constants';

const buildQuery = ({ url: page, brand, country, lang }) => `
  categorySeoData(${page ? `page: "${page}",` : ``} brand: "${brand}", country: "${country}", ${
  lang ? `lang: "${lang}"` : ``
}) {
    canonicalUrl
    errorMessage
    longDescription
    seoTitle
    seoMetaDesc
  }
`;

export default {
  getQuery: (data) => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_SEO_DATA,
    };
  },
};
