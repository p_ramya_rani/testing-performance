const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  set {
    val
    key
  }
  composites {
    headerText {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      icon {
        placement
        icon
      }
      link {
        url
        text
        title
        target
      }
    }
    promoBanner {
      link {
        url
        text
        title
        target
      }
      image {
        url
        alt
        title
      }
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
    }
    divTabs {
      text {
        text
      }
      smallCompImage {
        link {
          url
          text
          title
          target
        }
        image {
          url
          alt
          useTranslationText
          title
          width_d
          width_t
          width_m
          height_d
          height_t
          height_m
          url_img_fr
          url_m_fr
          url_t_fr
          url_img_es
          url_m_es
          url_t_es
        }
        video{
          url
          url_t
          url_m
          url_fr
          url_m_fr
          url_t_fr
          url_es
          url_m_es
          url_t_es
          height_d
          height_t
          height_m
          title
          autoplay
          controls
          loop
          muted
          inline
        }
      }
      linkClass {
        class
        url
        text
        title
        target
      }
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
