// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = ({ brand, country, channel }) => `
accountNavigation(brand: "${brand}", country: "${country}", channel: "${channel}") {
  errorMessage
  subSections {
    subSections {
      leafLink {
        url
        text
        title
        target
        external
      }
    }
    leafLink {
      url
      text
      title
      target
      external
    }
  }
  leafLink {
    url
    text
    title
    target
    external
  }
}
`;

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_ACCOUNT_NAVIGATION,
    };
  },
};
