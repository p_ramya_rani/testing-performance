const query = ({ slot, contentId, lang }) => `
  ${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
	contentId
	name
	type
  errorMessage
	set {
    val
    key
  }
}
`;
module.exports = {
  buildQuery: query,
};
