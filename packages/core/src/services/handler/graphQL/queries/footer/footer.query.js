// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = ({ type, brand, country, channel, lang }) => `
  footer: globalModule(
    type: "${type}",
    brand: "${brand}",
    country: "${country}",
    channel: "${channel}",
    lang: "${lang}"
  ) {
    errorMessage
    submodules {
      footerTop {
        composites {
          footerButtons {
            textItems {
              text
              style
            }
            link {
              url
              text
              title
              target
              action
            }
            richText {
              text
            }
          }
          socialLinks {
            url
            title
            text
            target
            external
            class
          }
        }
      }
      footerMiddle {
        composites {
          mprWrapper {
            linkClass {
              url
              title
              target
              text
              external
              class
              action
            }
            linkList {
              url
              target
              title
              text
              external
              action
            }
          }
          linkColumns {
            text {
              text
            }
            linkList {
              url
              target
              title
              text
              external
              action
            }
          }
        }
      }
      footerBottom {
        composites {
          linkList {
            url
            target
            title
            text
            external
            action
          }
          richTextList {
            text
          }
        }
      }
    }
  }
`;

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_FOOTER,
    };
  },
};
