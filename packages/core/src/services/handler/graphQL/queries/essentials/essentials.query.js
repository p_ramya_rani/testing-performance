// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../constants';

const buildQuery = ({ slot, contentId, lang }) => `
  ${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
	contentId
	name
	type
  errorMessage
	composites {
    mediaList {
      url
      alt
      title
      crop_d
      crop_t
      crop_m
    }
    multicolorText {
      text {
        text
      }
      color {
        color
      }
    }
    essentialsStepsWrapper {
      facet {
        facetId
      }
      essentialsStepType {
        itemType
      }
      textItems {
        text
        style
      }
      essentialsFormWrapper {
        facet {
          facetId
        }
        color {
          color
        }
        styled {
          text
          style
        }
        mediaList {
          url
          alt
          title
          crop_d
          crop_t
          crop_m
        }
      }
    }
  }
}
`;
export default {
  getQuery: (data) => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_MODULE,
    };
  },
};
