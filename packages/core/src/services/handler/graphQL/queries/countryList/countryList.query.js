// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = val => `
  query fetchCMSData {
    countryList(countryCode: "${val}") {
      errorMessage
      country {
        id: code
        displayName: name
      }
      currency {
        id: code
        displayName: name
      }
      exchangeRate {
        roundMethod
        exchangevalue: value
        merchantMargin
        quoteId
      }
    }
  }
`;

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_COUNTRY_LIST,
    };
  },
};
