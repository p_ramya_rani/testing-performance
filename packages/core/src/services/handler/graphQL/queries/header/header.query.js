// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = ({ type, brand, country, channel, lang }) => `
  header: globalModule(type: "${type}", brand: "${brand}", country: "${country}", channel: "${channel}", lang: "${lang}") {
    errorMessage
    submodules {
      topNavWrapper {
        composites {
          brand_tabs: brandTabs {
            url
            title
            target
            external
            class
          }

          top_promo_banner: topLoyaltyPromoBannerWrapper {
            text
            userType
          }

          promo_message_wrapper: promoMessageWrapper {
            richText {
              text
            }
            class {
              class
            }
            link {
              url
              title
              text
              target
              external
            }
          }
        }
      }

      promoHtmlBannerCarousel {
          composites {
            promoHtmlBanner {
                text
              }
          }
      }

      promoTextBannerCarousel {
        composites {
          promoTextBanner {
            linkClass {
              url
              title
              text
              target
              external
              class
            }
            textItems {
              text
              style
            }
          }
        }
      }

      loyaltyPromoBannerWrapper {
        composites {
          loyaltyPromoBanner {
            text
            userType
          }
        }
      }

    }
  }
`;

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_HEADER,
    };
  },
};
