// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = ({ brand, country, channel }) => `
lastUpdated(brand: "${brand}", country: "${country}", channel: "${channel}") {
  key
  value
  errorMessage
}
`;

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_LAST_UPDATED,
    };
  },
};
