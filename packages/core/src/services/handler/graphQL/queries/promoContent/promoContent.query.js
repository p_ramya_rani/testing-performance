// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../constants';

const buildQuery = ({ brand, country, channel, lang, category, path }) => `
contentLayout(brand: "${brand}", country: "${country}", channel: "${channel}", lang: "${lang}", category: "${category}", path: "${path}" ) {
  errorMessage
  key
  items {
    top {
      slots {
        contentId
        moduleName
        value
        name
        userType
      }
    }
    middle {
      slots {
        contentId
        moduleName
        value
        name
        userType
      }
    }
    bottom {
      slots {
        contentId
        moduleName
        value
        name
        userType
      }
    }
    bottomAplus {
      slots {
        contentId
        moduleName
        value
        name
        userType
      }
    }
  }
}
`;

export default {
  getQuery: (data) => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_CONTENT_LAYOUT,
    };
  },
};
