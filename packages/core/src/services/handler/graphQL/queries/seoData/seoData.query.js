// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = ({ page, brand, country, channel, lang }) => `
  seoData(${
    page ? `page: "${page}",` : ``
  } brand: "${brand}", country: "${country}", channel: "${channel}" ${
  lang ? `lang: "${lang}"` : ``
}) {
    errorMessage
    path
    pageTitle
    copyBody
    copyTitle
    keywords
    description
    robotsInfo
    canonicalUrl
    hrefLang
    thumbnailUrl
    twitterCard
    twitterSite
    twitterTitle
    twitterDescription
    twitterImageSrc
    ogTitle
    ogUrl
    ogType
    ogDescription
    ogImage
  }
`;

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_SEO_DATA,
    };
  },
};
