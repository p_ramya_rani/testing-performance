const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  set {
    val
    key
  }
  errorMessage
  composites {
    headerText {
      textItems {
        text
        style
      }
      icon {
        placement
        icon
      }
      link {
        url
        title
        target
      }
    }
    promoBanner {
      link {
        url
        title
      }
      textItems {
        text
        style
      }
    }
    mediaLinkedList {
      image {
        url
        title
        alt
        crop_d
        crop_t
        crop_m
      }
      link {
        url
        text
        target
        title
      }
    }
    ctaItems {
      button {
        url
        text
        target
        title
      }
      image {
        url
        title
        alt
        crop_d
        crop_t
        crop_m
      }
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
