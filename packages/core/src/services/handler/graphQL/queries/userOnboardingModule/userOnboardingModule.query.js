// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../constants';

const buildQuery = ({ contentId, cid, lang }) => `
  userOnboardingModule: moduleById(id: "${contentId || cid}" ${lang ? `, lang: "${lang}"` : ``}) {
  contentId
    name
    type
    composites {
      mediaList {
        url
        alt
        }
    textCarousel {
        headLine {
          text
        }
        textItems {
          text
        }
      }
    }
  }
`;

export default {
  getQuery: (data) => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_MODULE,
    };
  },
};
