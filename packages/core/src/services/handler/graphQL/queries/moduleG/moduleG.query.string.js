const query = ({ slot, contentId }) => `
${slot}: moduleById(id: "${contentId}") {
  contentId
  name
  type
  set {
    val
    key
  }
  errorMessage
  composites {
    headerText {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      icon {
        placement
        icon
      }
      link {
        url
        text
        title
        target
      }
    }
    promoBanner {
      link {
        url
        title
        target
      }
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
    }
    linkedImage {
      link {
        url
        text
        title
        target
      }
      image {
        url
        alt
        useTranslationText
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        crop_d
        crop_t
        crop_m
      }
    }
    divTabs {
      text {
        text
      }
      category {
        key
        val
      }
      singleCTAButton {
        url
        text
        target
        title
      }
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
