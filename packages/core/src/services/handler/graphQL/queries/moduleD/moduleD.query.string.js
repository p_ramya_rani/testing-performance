const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  errorMessage
  set {
    key
    val
  }
  composites {
    headerText {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      icon {
        placement
        icon
      }
      link {
        url
        title
        text
        target
      }
    }

    promoBanner {
      link {
        url
        title
        target
      }
      image {
        url
        alt
        title
      }
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
    }

    smallCompImage {
      link {
        url
        title
        text
        target
      }
      image {
        url
        url_m
        url_t
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        title
        alt
        useTranslationText
        crop_d
        crop_t
        crop_m
      }
      video{
        url
        url_t
        url_m
        url_fr
        url_m_fr
        url_t_fr
        url_es
        url_m_es
        url_t_es
        height_d
        height_t
        height_m
        title
        autoplay
        controls
        loop
        muted
        inline
      }
    }
    ctaItems {
      button {
        url
        text
        target
        title
      }
      image {
        url
        title
        alt
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        crop_d
        crop_t
        crop_m
      }
    }
  }
}`;
module.exports = {
  buildQuery: query,
};
