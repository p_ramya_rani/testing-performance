// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = data => {
  const queryData = Array.isArray(data) ? data : [data];
  let query = '';
  queryData.forEach(queryConfig => {
    const { category, subCategory, brand, country, channel, lang } = queryConfig;
    query += `${category || 'labels'}: labels(${category ? `category: "${category}",` : ``} ${
      subCategory ? `subCategory: "${subCategory}",` : ``
    } brand: "${brand}", country: "${country}", channel: "${channel}" ${
      lang ? `lang: "${lang}",` : ``
    }) {
        name
        errorMessage
        subcategories {
          name
          labels {
            key
            value
          }
          referred {
            name
            contentId
          }
        }
      }
    `;
  });
  return query;
};

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_LABELS,
    };
  },
};
