// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = ({ brand, country, channel, lang }) => `
  query fetchCMSData {
    siteMap (
      brand: "${brand}",
      country: "${country}",
      channel: "${channel}"
      lang: "${lang}"
    ) {
      content
      url
      categories {
        href
        name
        category {
          href
          name
          category {
            href
            name
          }
        }
      }
    }
  }
`;

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_SITEMAP,
    };
  },
};
