const IMAGE = `
image {
  url
  url_m
  url_t
  url_img_fr
  url_m_fr
  url_t_fr
  url_img_es
  url_m_es
  url_t_es
  width_d
  width_t
  width_m
  height_d
  height_t
  height_m
  alt
  useTranslationText
  crop_d
  crop_t
  crop_m
}`;
const COLOR = `
color {
  color
}`;

const VIDEO = `
video {
  url
  url_t
  url_m
  url_fr
  url_m_fr
  url_t_fr
  url_es
  url_m_es
  url_t_es
  height_d
  height_t
  height_m
  title
  autoplay
  controls
  loop
  muted
  inline
  hls
  poster_url
  poster_url_t
  poster_url_m
}`;

const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  set {
    val
    key
  }
  errorMessage
  columns {
    set {
      key
      val
    }
    border {
      set {
        key
        val
      }
      ${IMAGE}
      ${COLOR}
    }
    background {
      ${IMAGE}
      ${COLOR}
    }
    composites {
      headerTextLine {
        textItems {
          text
          style
          styledObj
          styledObjWeb
        }
        icon {
          placement
          icon
        }
        link {
          url
          title
          target
        }
        border {
          type
          name
          config {
            heightM
            heightT
            heightD
            heightA
            spacingBottomM
            spacingBottomT
            spacingBottomD
            spacingBottomA
            paddingM
            paddingT
            paddingD
            paddingA
          }
          construct {
            type
            ${COLOR}
            left {
              type
              ${COLOR}
            }
            text {
              type
              textItems {
                text
                style
                styledObj
              }
            }
            right {
              type
              ${COLOR}
            }
          }
        }
        background {
          ${IMAGE}
          ${COLOR}
        }
        stackingOrder
        variation
        position
        positionDTop
        positionDLeft
        positionDRight
        positionTTop
        positionTLeft
        positionTRight
        paddingD
        paddingDBottom
        paddingDLeft
        paddingDRight
        paddingT
        paddingTBottom
        paddingTLeft
        paddingTRight
        paddingM
        paddingMBottom
        paddingMLeft
        paddingMRight
      }
      largeCompImageSimpleCarouselComposite {
        ctaType
        ${IMAGE}
        ${VIDEO}
        singleCTAButton {
          url
          text
          target
          title
        }
        imageMobile {
          url
          alt
          useTranslationText
          width_m
          height_m
          crop_m
        }
        videoMobile {
          url
          url_m_fr
          url_m_es
          height_m
          title
          autoplay
          controls
          loop
          muted
          inline
        }
      }
      ctaItemsComposite {
        featured
        ctaType
        expandableTitle
        button {
          url
          text
          target
          title
        }
        ctaBackgroundImage {
          url
          url_t
          url_m
          title
          alt
          width_d
          width_t
          width_m
          height_d
          height_t
          height_m
          url_img_fr
          url_m_fr
          url_t_fr
          url_img_es
          url_m_es
          url_t_es
          crop_d
          crop_t
          crop_m
        }
        ${IMAGE}
      }
    }
    styled {
      text
      style
    }
    ${IMAGE}
    ${VIDEO}
    link {
      url
      text
      title
      target
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
