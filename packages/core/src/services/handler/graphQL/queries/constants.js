// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  TYPE_MODULE: 'module',
  TYPE_NAVIGATION: 'main-navigation',
  TYPE_LABELS: 'labels',
  TYPE_ACCOUNT_NAVIGATION: 'account-navigation',
  TYPE_SITEMAP: 'sitemap',
  TYPE_STORE_DETAILS: 'store-details',
  TYPE_SUB_NAVIGATION: 'sub-navigation',
  TYPE_HEADER: 'header',
  TYPE_FOOTER: 'footer',
  TYPE_LAYOUT: 'layout',
  TYPE_CONTENT_LAYOUT: 'content-layout',
  TYPE_SEO_DATA: 'seo-data',
  TYPE_XAPP_CONFIG: 'xapp-config',
  TYPE_COUNTRY_LIST: 'country-list',
  TYPE_LAST_UPDATED: 'last-updated',
};
