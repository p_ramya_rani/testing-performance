// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../constants';
import { buildQuery } from './moduleA.query.string';

export default {
  getQuery: (data) => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_MODULE,
    };
  },
};
