const IMAGE = `
image {
  url
  url_m
  url_t
  alt
  crop_d
  crop_t
  crop_m
}`;

const VIDEO = `
video {
  url
  url_t
  url_m
  title
  autoplay
  controls
  loop
  muted
  inline
}`;

const COLOR = `
color {
  color
}`;

const query = ({ pageName, path, brand, country, channel }) => `
${pageName || path}: pageByPath(
  path: "/${path}",
  brand: "${brand}",
  country: "${country}",
  channel: "${channel}",
  ) {
    errorMessage
    items {
      path
      layout {
        slots {
          name
          moduleName
          isHero
          contentId
          value
          spacing{
            spacingBottomM
            spacingBottomT
            spacingBottomD
            spacingBottomA
          }
          border {
            type
            name
            config {
              slot
              fromSlot
              toSlot
              appliesToW
              appliesToM
              heightM
              heightT
              heightD
              heightA
              spacingBottomM
              spacingBottomT
              spacingBottomD
              spacingBottomA
              paddingM
              paddingT
              paddingD
              paddingA
            }
            construct {
              type
              ${COLOR}
              ${IMAGE}
              ${VIDEO}
              left {
                type
                ${COLOR}
                ${IMAGE}
                ${VIDEO}
              }
              text {
                type
                textItems {
                  text
                  style
                  styledObj
                }
              }
              right {
                type
                ${COLOR}
                ${IMAGE}
                ${VIDEO}
              }
            }
          }
          background {
            type
            name
            config {
              slot
              fromSlot
              toSlot
              appliesToW
              appliesToM
            }
            construct {
              type
              ${COLOR}
            }
          }
        }
      }
    }
  }
`;
module.exports = {
  buildQuery: query,
};
