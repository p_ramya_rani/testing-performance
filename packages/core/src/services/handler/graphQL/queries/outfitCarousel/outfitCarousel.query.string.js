const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  set {
    val
    key
  }
  errorMessage
  composites {
    headLine {
      text
      style
    }
    subHeadLine {
      text
      style
    }
    mediaLinkedList {
      image {
        url
        title
        alt
        crop_d
        crop_t
        crop_m
      }
      link {
        url
        text
        target
        title
      }
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
