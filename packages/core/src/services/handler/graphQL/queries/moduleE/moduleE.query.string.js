const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  set {
    val
    key
  }
  composites {
    eyebrow {
      mediaLinkedList {
        video{
          url
          url_t
          url_m
          url_fr
          url_m_fr
          url_t_fr
          url_es
          url_m_es
          url_t_es
          height_d
          height_t
          height_m
          title
          autoplay
          controls
          loop
          muted
          inline
        }
        image {
          url
          url_m
          url_t
          alt
          width_d
          width_t
          width_m
          height_d
          height_t
          height_m
          url_img_fr
          url_m_fr
          url_t_fr
          url_img_es
          url_m_es
          url_t_es
          title
          crop_d
          crop_t
          crop_m
        }
        link {
          url
          text
          title
          target
        }
      }
      promoBanner {
        link {
          url
          title
          target
        }
        textItems {
          text
          style
          styledObj
          styledObjWeb
        }
      }
    }
    headerText {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      icon {
        placement
        icon
      }
      link {
        url
        title
        target
      }
    }
    headerText2 {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      icon {
        placement
        icon
      }
      link {
        url
        title
        target
      }
    }
    promoBanner {
      link {
        url
        title
        target
      }
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
    }
    linkedImage {
      image {
        url
        url_m
        url_t
        alt
        useTranslationText
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        title
        crop_d
        crop_t
        crop_m
      }
      link {
        url
        text
        title
        target
      }
      video{
        url
        url_t
        url_m
        url_fr
        url_m_fr
        url_t_fr
        url_es
        url_m_es
        url_t_es
        height_d
        height_t
        height_m
        title
        autoplay
        controls
        loop
        muted
        inline
      }
    }
    largeCompImageSimpleCarousel {
      image {
        url
        url_m
        url_t
        alt
        useTranslationText
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        title
        crop_d
        crop_t
        crop_m
      }
      video{
        url
        url_t
        url_m
        url_fr
        url_m_fr
        url_t_fr
        url_es
        url_m_es
        url_t_es
        height_d
        height_t
        height_m
        title
        autoplay
        controls
        loop
        muted
        inline
      }
      singleCTAButton {
        url
        text
        title
        target
      }
    }
    divCTALinks {
      image {
        url
        url_m
        url_t
        alt
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        title
        crop_d
        crop_t
        crop_m
      }
      video{
        url
        url_t
        url_m
        url_fr
        url_m_fr
        url_t_fr
        url_es
        url_m_es
        url_t_es
        height_d
        height_t
        height_m
        title
        autoplay
        controls
        loop
        muted
        inline
      }
      styled {
        text
        style
        styledObj
        styledObjWeb
      }
      link {
        url
        text
        title
        target
      }
    }
    ctaItems {
      button {
        url
        text
        target
        title
      }
      image {
        url
        url_m
        url_t
        title
        alt
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        crop_d
        crop_t
        crop_m
      }
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
