// 9fbef606107a605d69c0edbcd8029e5d
import gql from 'graphql-tag';
import logger from '@tcp/core/src/utils/loggerInstance';
import { importGraphQLQueriesDynamically, getAPIConfig } from '../../../../utils';
import { ENV_PREVIEW } from '../../../../constants/env.config';
import { moduleNames } from '../../../config';

/**
 * Builds query for GraphQL service
 */
const QueryBuilder = {
  /**
   * Async function which returns raw query for logging
   */
  getRawQuery: async (modules, apiConfig) => {
    const queriesList = await QueryBuilder.loadQueriesList(modules, apiConfig);
    return QueryBuilder.buildQuery(queriesList);
  },

  /**
   * Set the cache type for the query based on all received
   * cache types from different queries.
   * no-cache and network-only take precedence and prevent caching of the batch query
   * in case any one of them is set for this.
   */
  getCacheType: (cacheTypes) => {
    let cacheType;
    if (cacheTypes.has('no-cache')) {
      cacheType = 'no-cache';
    } else if (cacheTypes.has('network-only')) {
      cacheType = 'network-only';
    } else {
      cacheType = 'cache-first';
    }
    return cacheType;
  },

  /**
   * Set the cache tag for the query based on all received
   * cache tags from different queries.
   * [Special case added for bootstrap - combination of labels and header]
   */
  getCacheTag: (cacheTags) => {
    let cacheTag;
    if (cacheTags.has('labels') && cacheTags.has('header')) {
      cacheTag = 'bootstrap';
    } else {
      cacheTag = Array.from(cacheTags).join('|');
    }
    return cacheTag;
  },

  /**
   * Async function which returns query for single or list of modules
   */
  getQuery: async (modules, apiConfig) => {
    const queries = await QueryBuilder.loadQueriesList(modules, apiConfig);
    const finalQuery = QueryBuilder.buildQuery(queries.queriesList);
    return {
      query: QueryBuilder.wrapQuery(finalQuery),
      cacheType: QueryBuilder.getCacheType(queries.cacheTypes),
      cacheTag: QueryBuilder.getCacheTag(queries.cacheTags),
    };
  },
  /**
   * Function to add query meta for preview without individually modifying the module queries
   * @param {String} query Input GraphQl query
   * @returns {String}
   */
  addPreviewQueryMeta: (queryData, apiConfig) => {
    // We are picking preview fields from current store apiconfig if passed,
    // else picks from utils method which holds store reference
    const { isPreviewEnv, previewDate, previewEnvId } = apiConfig || getAPIConfig();
    const isPreview = !!(isPreviewEnv || previewEnvId === ENV_PREVIEW);
    if (isPreview) {
      const previewQueryData = Object.assign(queryData);
      let localQuery = previewQueryData.query ? previewQueryData.query : previewQueryData;
      let previewQueryMeta = `is_preview: "true"`;
      previewQueryMeta += previewDate ? `, preview_date: "${previewDate}"` : '';
      // For root components and labels
      localQuery = localQuery.replace(/(brand\s*:\s*\S*,{1,1})/g, `$1 ${previewQueryMeta},`);
      // For modules
      localQuery = localQuery.replace(/(\(\s*id\s*:\s*"\S*")/g, ` $1 ,${previewQueryMeta}`);
      // For country list
      localQuery = localQuery.replace(/(countryList\s*\{)/g, `countryList(${previewQueryMeta}) {`);

      previewQueryData.query = localQuery;
      return previewQueryData;
    }
    return queryData;
  },

  /**
   * Async function which dynamically loads query for a module
   * @param {String} module
   * @param {Object} data
   */
  loadModuleQuery: async (module, data, apiConfig) => {
    return importGraphQLQueriesDynamically(module).then(({ default: QueryModule }) => {
      const { accountNavigation, navigation, subNavigation } = moduleNames;
      logger.info(
        `Module queried: ${module} ${
          apiConfig?.isPreviewEnv === true ? '| Preview Mode: true' : ''
        }`
      );

      return module === navigation || module === accountNavigation || module === subNavigation
        ? QueryModule.getQuery(data)
        : QueryBuilder.addPreviewQueryMeta(QueryModule.getQuery(data), apiConfig);
    });
  },
  /**
   * This function wraps query with graphql-tag
   */
  wrapQuery: (query) => {
    return gql(query);
  },
  /**
   * This is a helper function which concatenates separate queries into one single query
   * @param {Array} queriesList
   */
  buildQuery: (queriesList) => {
    let finalQuery = `query fetchCMSData {`;
    queriesList.forEach((query) => {
      finalQuery += `

        ${query}

      `;
    });
    finalQuery += `}`;
    return finalQuery;
  },
  /**
   * This function fetches queries dynamically for a list of modules or a single module
   * @param {Object|Array} modules Either object or array of objects with below structure
   * {
   *    name: "moduleD",
   *    data: {
   *
   *    }
   * }
   */
  loadQueriesList: async (modules, apiConfig) => {
    let queriesList = [];
    const cacheTypes = new Set();
    const cacheTags = new Set();

    if (modules.length) {
      for (let i = 0; i < modules.length; i += 1) {
        const module = modules[i];
        // eslint-disable-next-line no-await-in-loop
        const queryData = await QueryBuilder.loadModuleQuery(module.name, module.data, apiConfig);
        queriesList.push(queryData);
      }
    } else {
      const queryData = await QueryBuilder.loadModuleQuery(modules.name, modules.data, apiConfig);
      queriesList.push(queryData);
    }

    queriesList = queriesList.map((queryData) => {
      let { query } = queryData;

      if (typeof queryData === 'string') {
        query = queryData;
      } else {
        const { cacheType, cacheTag } = queryData;
        if (cacheType) {
          cacheTypes.add(cacheType);
        }
        if (cacheTag) {
          cacheTags.add(cacheTag);
        }
      }

      return query;
    });

    return {
      queriesList,
      cacheTypes,
      cacheTags,
    };
  },
};

export default QueryBuilder;
