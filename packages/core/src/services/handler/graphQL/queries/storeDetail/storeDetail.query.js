// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../constants';

const buildQuery = ({ brand, country, stlocId }) => `
  query fetchCMSData {
    storeDetails(brand: "${brand}", country: "${country}", storeLocationId: ${stlocId}) {
      attribute
      city
      country
      addressLine
      description {
        displayStoreName
      }
      latitude
      longitude
      postalCode
      stateOrProvinceName
      storeName
      telephone1
      uniqueID
      x_brands
      x_entranceType
      x_mallType
      x_stloc
      fullImage
      distance
      isBopisAvailable
      recordSetComplete
      recordSetCount
      recordSetStartNumber
      recordSetTotal
      resourceId
      resourceName
      isCurrentStore
      isPreviousStore
      preferredStore
      x_isStoreBOSSEligible
      x_bossMaxDate
      x_bossMinDate
      errorMessage
    }
  }
`;

export default {
  getQuery: data => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_STORE_DETAILS,
    };
  },
};
