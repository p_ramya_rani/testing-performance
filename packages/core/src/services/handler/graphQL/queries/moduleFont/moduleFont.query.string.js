const query = ({ slot, contentId, cid, lang }) => `
${slot || `moduleFont`}: moduleById(id: "${contentId || cid}" ${lang ? `, lang: "${lang}"` : ``}) {
  contentId
  name
  type
  errorMessage
  set {
    key
    val
  }
  composites {
    richTextList {
      text
    }
  }
}
`;
const queryApp = ({ slot, contentId, cid, lang }) => `
${slot || `moduleFont`}: moduleById(id: "${contentId || cid}" ${lang ? `, lang: "${lang}"` : ``}) {
  contentId
  name
  type
  errorMessage
  set {
    key
    val
  }
}
`;
module.exports = {
  buildQueryWeb: query,
  buildQuery: queryApp,
};
