// 9fbef606107a605d69c0edbcd8029e5d
import { isMobileApp } from '@tcp/core/src/utils';
import constants from '../constants';
import { buildQuery, buildQueryWeb } from './moduleFont.query.string';

export default {
  getQuery: data => {
    return {
      query: isMobileApp() ? buildQuery(data) : buildQueryWeb(data),
      cacheTag: constants.TYPE_MODULE,
    };
  },
};
