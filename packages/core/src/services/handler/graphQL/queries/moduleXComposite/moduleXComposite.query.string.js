const query = ({ cids, lang }) => {
  let compositeQuery = '';
  cids.map(item => {
    compositeQuery += `${item.name}: moduleById(id: "${item.contentId}" ${
      lang ? `, lang: ${lang}` : ``
    }) {
    contentId
    name
    type
    errorMessage
    composites {
      richTextList {
        text
      }
    }
  }
    `;
    return compositeQuery;
  });
  return compositeQuery;
};
module.exports = {
  buildQuery: query,
};
