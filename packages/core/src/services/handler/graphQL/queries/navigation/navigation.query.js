// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../constants';

const buildSsrQuery = ({ brand, country, channel, lang }) => `
  navigation: mainNavigation(brand: "${brand}", country: "${country}", channel: "${channel}", lang: "${lang}") {
    errorMessage
    categoryContent {
      catgroupId
      name
      id
      seoToken
      displayToCustomer
    }
    subCategories {
      categoryContent {
        catgroupId
        name
        id
        seoToken
        groupIdentifierSequence
        groupIdentifier
        groupIdentifierName
        displayToCustomer
      }
      subCategories {
        categoryContent {
          catgroupId
          name
          id
          seoToken
          groupIdentifierSequence
          groupIdentifier
          groupIdentifierName
          displayToCustomer
        }
      }
    }
  }
`;

const buildQuery = ({ brand, country, channel, lang, depth }) => `
  navigation: mainNavigation(brand: "${brand}", country: "${country}", channel: "${channel}", lang: "${lang}") {
    errorMessage
    ${
      !depth || depth > 1
        ? `
    categoryContent {
      description
      catgroupId
      name
      id
      seoUrl
      seoToken
      displayToCustomer
      isShortImage
      sizeChartSelection
      excludeAttribute
      mainCategory {
        set {
          key
					val
        }
        sizesRange {
          text
        }
        promoBadge {
          text
          style
        }
        categoryFilterImage {
          url
          alt
          title
          crop_d
          crop_m
          crop_t
          position
        }
        categoryLayout {
          name
          columns {
            imageBanner {
              image {
                url
                alt
                title
                crop_d
                crop_m
                crop_t
              }
              link {
                url
                text
                title
                target
              }
            }
            shopBySize {
              text {
                text
              }
              linkList {
                url
                title
                text
                target
              }
            }
            textBanner {
              link {
                url
                text
                title
                target
                external
                action
              }
              category
              textItems {
                text
                style
              }
              set {
                key
                val
              }
            }
          }
        }
      }
    }`
        : `
    categoryContent {
      name
      id
    }`
    }
    ${
      depth || depth > 1
        ? `
    subCategories {
      categoryContent {
        seoToken
        seoUrl
        groupIdentifierSequence
        description
        groupIdentifier
        groupIdentifierName
        name
        id
        catgroupId
        displayToCustomer
        isShortImage
        sizeChartSelection
        excludeAttribute
        mainCategory {
          promoBadge {
            text
          }
          set {
            key
            val
          }
          categoryFilterImage {
            url
            alt
            title
            crop_d
            crop_m
            crop_t
            position
          }
        }
      }
      subCategories {
        categoryContent {
          seoToken
          seoUrl
          groupIdentifierSequence
          description
          groupIdentifier
          groupIdentifierName
          name
          id
          catgroupId
          displayToCustomer
          isShortImage
          excludeAttribute
          mainCategory {
            promoBadge {
              text
            }
            set {
              key
              val
            }
          }
          sizeChartSelection
          mainCategory {
            categoryFilterImage {
              url
              alt
              title
              crop_d
              crop_m
              crop_t
              position
            }
          }
        }
      }
    }
    `
        : ``
    }
  }
`;

export default {
  getQuery: (data) => {
    const { type } = data;
    if (type === 'navigation-ssr') {
      return {
        query: buildSsrQuery(data),
        cacheTag: constants.TYPE_NAVIGATION,
      };
    }
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_NAVIGATION,
    };
  },
};
