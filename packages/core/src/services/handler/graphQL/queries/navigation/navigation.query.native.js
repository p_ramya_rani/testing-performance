// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '../../../../../utils';
import { defaultBrand, MobileChannel, defaultCountry } from '../../../../api.constants';

import constants from '../constants';

const buildQuery = ({ brand, country, channel }) => `
  navigation: mainNavigation(brand: "${brand}", country: "${country}", channel: "${channel}") {
    categoryContent {
      seoToken
      isShortImage
      description
      catgroupId
      seoTitle
      seoUrl
      name
      id
      sizeChartSelection
      displayToCustomer
      mainCategory {
        set {
          key
					val
        }
        sizesRange {
          text
        }
        categoryImage {
          url
          alt
          title
          crop_d
          crop_m
          crop_t
          position
        }
        categoryFilterImage {
          url
          alt
          title
          crop_d
          crop_m
          crop_t
          position
        }
        categoryLayout {
          name
          columns {
            shopBySize {
              text {
                text
              }
              linkList {
                url
                title
                text
                target
              }
            }
          }
        }
      }
    }
    subCategories {
      categoryContent {
        seoToken
        seoUrl
        catgroupId
        groupIdentifierSequence
        description
        groupIdentifier
        groupIdentifierName
        name
        isShortImage
        id
        sizeChartSelection
        displayToCustomer
        mainCategory {
          promoBadge {
            text
          }
          set {
            key
            val
          }
          categoryFilterImage {
            url
            alt
            title
            crop_d
            crop_m
            crop_t
            position
          }
        }
      }
      subCategories {
        categoryContent {
          seoToken
          seoUrl
          catgroupId
          groupIdentifierSequence
          description
          groupIdentifier
          groupIdentifierName
          displayToCustomer
          mainCategory {
            set {
              key
              val
            }
            categoryFilterImage {
              url
              alt
              title
              crop_d
              crop_m
              crop_t
              position
            }
          }
          name
          id
          isShortImage
          sizeChartSelection
        }
      }
    }
  }
`;

export default {
  getQuery: () => {
    const apiConfig = getAPIConfig();

    return {
      query: buildQuery({
        brand: (apiConfig && apiConfig.brandIdCMS) || defaultBrand,
        channel: MobileChannel,
        country: (apiConfig && apiConfig.siteIdCMS) || defaultCountry,
      }),
      cacheTag: constants.TYPE_NAVIGATION,
    };
  },
};
