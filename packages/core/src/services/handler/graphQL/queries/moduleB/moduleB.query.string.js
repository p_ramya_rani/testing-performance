const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  set {
    key
    val
  }
  errorMessage
  composites {
    largeCompImage {
      headerText {
        textItems {
          text
          style
          styledObj
          styledObjWeb
        }
        icon {
          placement
          icon
        }
        link {
          url
          title
          target
          external
        }
      }
      promoBanner {
        link {
          url
          title
          text
          target
        }
        textItems {
          text
          style
          styledObj
          styledObjWeb
        }
      }
      linkedImage {
        link {
          url
          text
          title
          target
        }
        image {
          url
          url_m
          url_t
          title
          alt
          useTranslationText
          width_d
          width_t
          width_m
          height_d
          height_t
          height_m
          url_img_fr
          url_m_fr
          url_t_fr
          url_img_es
          url_m_es
          url_t_es
          crop_d
          crop_t
          crop_m
        }
        video{
          url
          url_t
          url_m
          url_fr
          url_m_fr
          url_t_fr
          url_es
          url_m_es
          url_t_es
          height_d
          height_t
          height_m
          title
          autoplay
          controls
          loop
          muted
          inline
          hls
          poster_url
          poster_url_t
          poster_url_m
        }
      }
      backgroundColor {
        colorD {
          color
        }
        colorM {
          color
        }
        imageD {
          url
          title
          alt
        }
        imageM {
          url
          title
          alt
        }
      }
    }
    ctaItems {
      button {
        url
        text
        title
        target
      }
      ctaBackgroundImage {
        url
        url_t
        url_m
        title
        alt
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        crop_d
        crop_t
        crop_m
      }
    }
    backgroundImage {
      link {
        url
        target
        title
      }
      image {
        url
        url_m
        url_t
        alt
        useTranslationText
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        crop_d
        crop_t
        crop_m
      }
      video{
        url
        url_t
        url_m
        url_fr
        url_m_fr
        url_t_fr
        url_es
        url_m_es
        url_t_es
        height_d
        height_t
        height_m
        title
        autoplay
        controls
        loop
        muted
        inline
        hls
        poster_url
        poster_url_t
        poster_url_m
      }
      imageMobile {
        url
        alt
        useTranslationText
        width_m
        height_m
        crop_m
      }
      videoMobile {
        url
        url_m_fr
        url_m_es
        height_m
        title
        autoplay
        controls
        loop
        muted
        inline
      }
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
