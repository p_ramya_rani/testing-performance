const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  errorMessage
  set {
    key
    val
  }
  composites {
    masonryGrid {
      promoBanner {
        textItems {
          text
          style
          styledObj
          styledObjWeb
        }
        link {
          url
          title
          target
        }
      }
      mediaLinkedList {
        image {
          url
          url_t
          url_m
          url_img_fr
          url_m_fr
          url_t_fr
          url_img_es
          url_m_es
          url_t_es
          title
          alt
          crop_d
          crop_t
          crop_m
        }
        video{
          url
          url_t
          url_m
          url_fr
          url_m_fr
          url_t_fr
          url_es
          url_m_es
          url_t_es
          height_d
          height_t
          height_m
          title
          autoplay
          controls
          loop
          muted
          inline
        }
        link {
          url
          text
          title
          target
        }
      }
      singleCTAButton {
        url
        text
        title
        target
      }
    }

    headerText {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      icon {
        placement
        icon
      }
      link {
        url
        text
        title
        target
      }
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
