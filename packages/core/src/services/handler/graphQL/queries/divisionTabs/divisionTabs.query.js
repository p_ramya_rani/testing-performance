// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../constants';

const buildQuery = ({ slot, contentId, lang }) => `
  ${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
		contentId
		name
		type
		set {
			val
			key
    }
    errorMessage
		composites {
			headLine {
				text
				style
			}
			buttonList {
				id: url
				label: text
				external
				title
				action
				target
			}
		}
  }
`;

export default {
  getQuery: (data) => {
    return {
      query: buildQuery(data),
      cacheTag: constants.TYPE_MODULE,
    };
  },
};
