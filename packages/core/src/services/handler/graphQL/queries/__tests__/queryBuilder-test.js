// 9fbef606107a605d69c0edbcd8029e5d 
import gql from 'graphql-tag';
import QueryBuilder from '../queryBuilder';
import LayoutQuery from '../layout/layout.query';
import { getAPIConfig } from '../../../../../utils';

jest.mock('../../../../../utils/utils.web');
jest.mock('../../../../../utils/utils');

describe('Query Builder', () => {
  const testQuery = LayoutQuery.getQuery({
    path: 'homepage',
  });
  let testBuildQuery = `query fetchCMSData {`;
  testBuildQuery += `

    ${testQuery.query}

  `;
  testBuildQuery += `}`;

  const testGqlQuery = gql(testBuildQuery);

  const testCacheGqlQuery = {
    cacheType: 'cache-first',
    query: testGqlQuery,
  };

  getAPIConfig.mockImplementation(() => ({
    isPreviewEnv: undefined,
    previewDate: undefined,
  }));

  it('getQuery', async () => {
    const query = await QueryBuilder.getQuery({
      name: 'layout',
      data: {
        path: 'homepage',
      },
    });
    expect(query).toMatchObject(testCacheGqlQuery);
  });

  it('loadModuleQuery', async () => {
    const queryData = await QueryBuilder.loadModuleQuery('layout', { path: 'homepage' });
    console.log(queryData);
    expect(queryData).toMatchObject(testQuery);
  });

  it('wrapQuery', () => {
    const query = QueryBuilder.wrapQuery(testBuildQuery);
    expect(query).toMatchObject(testGqlQuery);
  });

  it('buildQuery', () => {
    const query = QueryBuilder.buildQuery([testQuery]);
    expect(query).toEqual(expect.not.stringMatching(testBuildQuery));
  });

  it('loadQueriesList', async () => {
    const queriesData = await QueryBuilder.loadQueriesList({
      name: 'layout',
      data: {
        path: 'homepage',
      },
    });
    expect(queriesData.queriesList).toMatchObject([testQuery.query]);
  });

  it('loadQueriesList | array input', async () => {
    const queriesData = await QueryBuilder.loadQueriesList([
      {
        name: 'layout',
        data: {
          path: 'homepage',
        },
      },
    ]);

    expect(queriesData.queriesList).toMatchObject([testQuery.query]);
  });

  it('addPreviewQueryMeta', async () => {
    getAPIConfig.mockImplementation(() => ({
      isPreviewEnv: true,
      previewDate: '23102019',
    }));
    const queryData = await QueryBuilder.loadModuleQuery('layout', { path: 'homepage' });
    expect(queryData.query).toContain('is_preview');
    expect(queryData.query).toContain('preview_date');
  });

  it('addPreviewQueryMeta | no date or token', async () => {
    getAPIConfig.mockImplementation(() => ({
      isPreviewEnv: true,
      previewDate: undefined,
    }));
    const queryData = await QueryBuilder.loadModuleQuery('layout', { path: 'homepage' });
    expect(queryData.query).toContain('is_preview');
    expect(queryData.query).not.toContain('preview_date');
  });
});
