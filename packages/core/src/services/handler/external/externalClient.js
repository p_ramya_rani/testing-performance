// 9fbef606107a605d69c0edbcd8029e5d 
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import Throttle from 'superagent-throttle';
import { API_CONFIG, superAgentThrottleConfig } from '../../config';

const throttle = new Throttle(superAgentThrottleConfig);

/**
 * @summary This is to initialize superagent client to consume the JSONP external API.
 * @param {Object} reqObj - request param with endpoints and payload
 * @returns {Promise} Resolves with promise to consume the external json api.
 * Here since it is a jsonP, we receive both success and error in callback method.
 * Both success/Catch resolves the promise - which needs to be handled in abstractor calling the Service handler.
 */
const externalJsonpAPIClient = reqObj => {
  const { webService } = reqObj;
  const requestUrl = webService.URI;
  const requestType = webService.method.toLowerCase();
  return new Promise(resolve => {
    superagent[requestType](requestUrl)
      .use(throttle.plugin())
      .query(reqObj.body)
      .use(
        jsonp({
          timeout: webService.reqTimeout,
        })
      )
      .then(response => {
        resolve(response);
      })
      .catch(response => resolve(response));
  });
};

/**
 * @summary This is to initialize superagent client to consume the external API.
 * @param {string} apiConfig - Api config to be utilized for brand/channel/locale config
 * @param {Object} reqObj - request param with endpoints and payload
 * @returns {Promise} Resolves with promise to consume the external api or reject in case of error
 */
const ExternalAPIClient = (apiConfig, reqObj) => {
  // TODO - Keeping it generic for now, to check if it fulfills all the variations of external call
  const { apiRequestTimeout, apiContentType } = API_CONFIG;
  const {
    webService: { JSONP = false, URI, method = '', reqTimeout = apiRequestTimeout },
    header,
    body,
  } = reqObj;

  if (JSONP) {
    return externalJsonpAPIClient(reqObj);
  }

  const requestType = method.toLowerCase();
  const request = superagent[requestType](URI)
    .accept(apiContentType)
    .timeout(reqTimeout);

  if (header) {
    request.set(header);
  }
  if (requestType === 'get') {
    request.query(body);
  } else {
    request.send(body);
  }

  const result = new Promise((resolve, reject) => {
    request
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ err, reqObj });
      });
  });
  result.abort = () => request.abort(); // allow callers to cancel the request by calling abort on the returned object.
  return result;
};

export default ExternalAPIClient;
