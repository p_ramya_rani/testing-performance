// 9fbef606107a605d69c0edbcd8029e5d 
/**
 * This is a SuperAgent plugin for transforming requests via externally
 * defined transformations.
 * @see https://github.com/visionmedia/superagent#plugins
 */
export default function requestHotfixer(request) {
  const transformations = global.TCP_HOTFIX_SERVICE_REQUEST;

  // Don't do anything unless there are transformations
  if (!Array.isArray(transformations)) {
    return request;
  }

  transformations.forEach(transformation => {
    try {
      transformation(request);
    } catch (ex) {
      console.log(ex);
    }
  });

  return request;
}
