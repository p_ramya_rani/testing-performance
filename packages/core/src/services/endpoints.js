// 9fbef606107a605d69c0edbcd8029e5d
/* eslint max-lines: ["error", 650] */
import { API_METHODS, PRODUCTS_URI, SAVE_FOR_LATER } from './api.constants';

const endpoints = {
  graphQL: {
    int: 'https://niebw6f7lfeorgk6adlvicfnca.appsync-api.us-east-2.amazonaws.com/graphql',
    uat: 'https://vftt2fuvm5eu5jat3ounglwv3a.appsync-api.us-east-2.amazonaws.com/graphql',
    perf: 'https://563k2dzbojbsvnaaljusrz4y44.appsync-api.us-east-2.amazonaws.com/graphql',
    sandbox: 'https://lwmlzhk7g5grdgtbzozpgsb3lm.appsync-api.us-east-2.amazonaws.com/graphql',
  },
  getAddressList: {
    method: API_METHODS.GET,
    URI: 'v2/account/getAddressFromBook',
  },
  verifyAddress: {
    method: API_METHODS.GET,
    // TODO - Use MELISSA_KEY from config file
    URI: 'https://personator.melissadata.net/v3/WEB/ContactVerify/doContactVerify',
  },
  emailVerification: {
    method: 'post',
    URI: 'https://bpi.briteverify.com/api/public/v1/fullverify',
    JSONP: true,
    reqTimeout: 6000,
    authKey: 'b049d05a-f8ad-45ef-bd1b-c2f1931cb86c',
  },
  addSmsSignup: {
    method: 'post',
    URI: 'v2/vibes/smsSignUp',
  },
  addEmailSignup: {
    method: 'post',
    URI: 'v2/store/addSignUpEmail',
  },
  getOrderDetails: {
    method: API_METHODS.GET,
    URI: 'v2/checkout/getOrderDetails',
  },
  fullDetails: {
    method: API_METHODS.GET,
    URI: 'v2/checkout/cart',
  },
  addCreditCard: {
    method: API_METHODS.POST,
    URI: 'v2/account/addCreditCardDetails',
  },
  instantCreditApplication: {
    method: 'POST',
    URI: 'v2/ads_dms/processWIC',
  },
  savePlcc: {
    method: 'POST',
    URI: 'v2/account/savePLCC',
  },
  updateCreditCard: {
    method: API_METHODS.POST,
    URI: 'v2/account/modifyCreditCardDetails',
  },
  addAddress: {
    method: API_METHODS.POST,
    URI: 'v2/account/addAddress',
  },
  addPaymentInstruction: {
    method: 'POST',
    URI: 'v2/checkout/addPaymentInstruction',
  },
  deletePaymentInstruction: {
    method: 'POST',
    URI: 'v2/checkout/deletePaymentInstruction',
  },
  updateAddress: {
    method: 'put',
    URI: 'v2/wallet/updateAddress',
  },
  updateMultiSelectItemsToRemove: {
    method: 'PUT',
    URI: 'v2/cart/deleteMultipleOrderItems',
  },
  updateOrderItem: {
    method: 'PUT',
    URI: 'v2/cart/updateOrderItem',
  },
  createAccount: {
    method: API_METHODS.POST,
    URI: 'v2/wallet/addCustomerRegistration',
  },
  createAccountPLCC: {
    method: API_METHODS.POST,
    URI: 'v2/ads_dms/adsCreateAccount',
  },
  logon: {
    method: API_METHODS.POST,
    URI: 'v2/account/logon',
  },
  getRegisteredUserDetailsInfo: {
    method: API_METHODS.GET,
    URI: 'v2/account/getRegisteredUserInfo',
  },
  getListofDefaultWishlist: {
    method: API_METHODS.GET,
    URI: 'v2/wishlist/getWishListbyId?itemSummary=true',
  },
  getCouponList: {
    method: API_METHODS.GET,
    URI: 'v2/account/getAddressFromBook',
  },
  logout: {
    method: API_METHODS.DELETE,
    URI: 'v2/account/logout',
  },
  getPointsHistory: {
    method: API_METHODS.GET,
    URI: 'v2/wallet/getMyPointHistory',
  },
  bonusPoints: {
    method: API_METHODS.GET,
    URI: 'v2/account/bonusDay',
  },
  applyBonusPoints: {
    method: API_METHODS.POST,
    URI: 'v2/account/bonusDay',
  },
  addCoupons: {
    method: 'post',
    URI: 'v2/checkout/coupons',
  },
  addAirmilesBanner: {
    method: 'post',
    URI: 'v2/wallet/updateAirMilesInfo',
  },
  requestPassword: {
    method: 'put',
    URI: 'v2/account/resetPassword',
  },
  requestPasswordPost: {
    method: 'post',
    URI: 'v2/account/resetPassword',
  },
  updateProfileInfo: {
    method: 'put',
    URI: 'v2/account/updatesAccountDataForRegisteredUser',
  },
  getExtraPoints: {
    method: 'GET',
    URI: 'v2/account/points/waysToEarn',
  },
  getEarnedPointsNotication: {
    method: 'GET',
    URI: 'v2/wallet/points/nontransactional/current',
  },
  getGifCardBalance: {
    method: API_METHODS.POST,
    URI: 'v2/wallet/getGiftCardBalance',
  },
  setDefaultPayment: {
    method: API_METHODS.POST,
    URI: 'v2/account/modifyCreditCardDetails',
  },
  getCardList: {
    method: API_METHODS.GET,
    URI: 'v2/account/getCreditCardDetails',
  },
  addGiftCard: {
    method: API_METHODS.POST,
    URI: 'v2/account/addCreditCardDetails',
  },
  setDefaultShippingAddress: {
    method: API_METHODS.PUT,
    URI: 'v2/wallet/updateAddress',
  },
  deleteAddress: {
    method: API_METHODS.DELETE,
    URI: 'v2/account/deleteAddressDetails',
  },
  addShipToStore: {
    method: API_METHODS.POST,
    URI: 'v2/cart/addShipToStore',
  },
  getProductDetails: {
    method: 'GET',
    URI: PRODUCTS_URI.PRODUCTS,
    unbxd: true,
    unbxdCustom: true,
    authHeaderRequired: true,
    clientTimeout: 30000,
    serverTimeout: 15000,
  },
  getProductSkuInfo: {
    method: 'GET',
    URI: PRODUCTS_URI.PRODUCTS_BY_SEARCH,
    unbxd: true,
    clientTimeout: 30000,
    serverTimeout: 15000,
  },
  getProductsByOutfits: {
    method: 'GET',
    URI: PRODUCTS_URI.PRODUCTS_BY_OUTFITS,
  },
  getProductviewbyCategory: {
    method: 'GET',
    URI: PRODUCTS_URI.PRODUCTS_VIEW_BY_CATEGORY,
    clientTimeout: 30000,
    serverTimeout: 15000,
  },
  getProductsBySearchTerm: {
    method: 'GET',
    URI: PRODUCTS_URI.PRODUCTS_BY_SEARCH,
    unbxd: true,
    clientTimeout: 30000,
    serverTimeout: 15000,
  },
  getProductInfoById: {
    method: 'GET',
    URI: PRODUCTS_URI.PRODUCTS_BY_SEARCH,
    unbxd: true,
    clientTimeout: 30000,
    serverTimeout: 15000,
  },
  getProductInfoForTranslationByPartNumber: {
    method: 'GET',
    URI: PRODUCTS_URI.PRODUCTS,
    unbxd: true,
    unbxdCustom: true,
    authHeaderRequired: true,
    clientTimeout: 30000,
    serverTimeout: 15000,
  },
  deleteCreditCardOnAccount: {
    method: API_METHODS.POST,
    URI: 'v2/account/deleteCreditCardDetails',
  },
  addProductToCart: {
    method: API_METHODS.POST,
    URI: 'v2/cart/addProductToCart',
  },
  addOrderBopisItem: {
    method: API_METHODS.POST,
    URI: 'v2/bopis/createBopisOrder',
  },
  removeCouponOrPromo: {
    method: 'POST',
    URI: 'v2/checkout/removePromotionCode',
  },
  getContactPreferences: {
    method: 'GET',
    URI: 'v2/account/preferences',
  },
  setContactPreferences: {
    method: 'PUT',
    URI: 'v2/account/preferences',
  },
  getAllOffers: {
    method: API_METHODS.GET,
    URI: 'v2/wallet/getAllCoupons',
  },
  getCouponDetails: {
    method: API_METHODS.POST,
    URI: 'v2/wallet/getPromotionDetails',
  },
  giftOptionsCmd: {
    method: API_METHODS.GET,
    URI: 'v2/checkout/giftOptionsCmd',
  },
  orderLookUp: {
    method: API_METHODS.GET,
    URI: 'v2/account/orderLookUp',
  },
  getUnqualifiedItems: {
    method: 'GET',
    URI: 'v2/cart/getUnqualifiedItems',
  },
  getShipmentMethods: {
    method: 'GET',
    URI: 'v2/checkout/getShipmentMethods',
  },
  getDefaultShipmentMethods: {
    method: 'GET',
    URI: 'v2/checkout/getDefaultShipmentMethods',
  },
  updateShippingMethodSelection: {
    method: 'PUT',
    URI: 'v2/checkout/updateShippingMethodSelection',
  },
  getChildren: {
    method: API_METHODS.GET,
    URI: 'v2/account/getBirthdaySavings',
  },
  updateUserSurvey: {
    method: 'PUT',
    URI: 'v2/account/saveUserSurvey',
  },
  updatePaymentInstruction: {
    method: 'PUT',
    URI: 'v2/checkout/updatePaymentInstruction',
  },
  deleteChild: {
    method: 'POST',
    URI: 'v2/account/deleteBirthdaySavings',
  },
  addChild: {
    method: 'POST',
    URI: 'v2/account/addBirthdaySavings',
  },
  addGiftOptions: {
    method: 'POST',
    URI: 'v2/checkout/addGiftOptions',
  },
  internationalCheckoutSettings: {
    method: 'POST',
    URI: 'v2/checkout/internationalCheckout',
  },
  paypalLookUp: {
    method: 'GET',
    URI: 'v2/checkout/TCPPayPalCCLookUpRESTCmd',
  },
  paypalAuth: {
    method: 'GET',
    URI: 'v2/checkout/TCPPayPalCCAuthenticationRESTCmd',
  },
  searchBarApi: {
    method: 'GET',
    URI: PRODUCTS_URI.PRODUCTS_AUTOSUGGEST,
    unbxd: true,
    clientTimeout: 30000,
    serverTimeout: 15000,
  },
  getFavoriteStore: {
    method: 'GET',
    URI: 'v2/store/getFavouriteStoreLocation',
  },
  findStoresByCoordinates: {
    method: 'GET',
    URI: 'v2/store/findStoresbyLatitudeandLongitude',
  },
  setFavoriteStore: {
    method: 'POST',
    URI: 'v2/store/addFavouriteStoreLocation',
  },
  getSocialAccountsInfo: {
    method: 'GET',
    URI: 'v2/account/preferences/socialNew',
  },
  saveSocialAccountsInfo: {
    method: 'PUT',
    URI: 'v2/account/preferences/socialNew',
  },
  getVenmoClientToken: {
    method: 'GET',
    URI: 'v2/venmo/getVenmoClientToken',
  },
  getAppleClientToken: {
    method: 'GET',
    URI: 'v2/payment/apple-pay/getClientToken',
  },
  getAllSfl: {
    method: 'GET',
    URI: SAVE_FOR_LATER,
  },
  updateSflItem: {
    method: 'PUT',
    URI: SAVE_FOR_LATER,
  },
  deleteSflItem: {
    method: 'DELETE',
    URI: SAVE_FOR_LATER,
  },
  addSflItem: {
    method: 'POST',
    URI: SAVE_FOR_LATER,
  },
  getStoreandProductInventoryInfo: {
    method: 'GET',
    URI: 'v2/vendor/getStoreAndProductInventoryInfo',
  },
  checkout: {
    method: 'POST',
    URI: 'v2/checkout/addCheckout',
  },
  personalizedCoupons: {
    method: 'POST',
    URI: 'v2/coupons/getOffers',
  },
  getStoreInfo: {
    method: 'GET',
    URI: 'v2/store/info',
  },
  getNearByStore: {
    method: 'GET',
    URI: 'v2/store/nearBy',
  },
  getBOPISInventoryDetails: {
    method: 'POST',
    URI: 'v2/vendor/getBOPISInvetoryDetails',
  },
  getUserCartStoresAndInventory: {
    method: 'GET',
    URI: 'v2/bopis/getUserBopisStores',
  },
  claimPoints: {
    method: 'POST',
    URI: 'v2/account/points/claim',
  },
  navigateXHRService: {
    method: 'POST',
    URI: 'site/v1.0/navigatexhr',
  },
  postNavigateService: {
    method: 'POST',
    URI: 'site/v1.0/navigate',
  },
  getNavigateService: {
    method: 'GET',
    URI: 'site/v1.0/navigate',
  },
  getStyliticsProductViewById: {
    method: API_METHODS.GET,
    URI: 'https://widget-api.stylitics.com/api/outfits',
    serverTimeout: 15000,
    clientTimeout: 30000,
  },
  getDetailedOrderHistory: {
    method: 'GET',
    URI: 'v2/wallet/getPointsAndOrderHistory',
  },
  getAccountOrders: {
    method: 'GET',
    URI: 'v1/account/orders',
  },
  addOrUpdateWishlist: {
    method: 'PUT',
    URI: 'v2/wishlist/addOrUpdateWishlist',
  },
  getTwitterAuthToken: {
    method: 'GET',
    URI: 'v2/twitter/requestToken',
  },
  getTwitterAccessToken: {
    method: 'GET',
    URI: 'v2/twitter/accessToken',
  },
  getListofWishList: {
    method: 'GET',
    URI: 'v2/wishlist/getListOfWishlist',
  },
  getWishListbyId: {
    method: 'GET',
    URI: 'v2/wishlist/getWishListbyId',
  },
  createWishListForUser: {
    method: 'POST',
    URI: 'v2/wishlist/createWishListForUser',
  },
  moveWishListItem: {
    method: 'PUT',
    URI: 'v2/wishlist/moveItemToWishList',
  },
  deleteWishListForUser: {
    method: 'DELETE',
    URI: 'v2/wishlist/deleteWishListForUser',
  },
  editWishList: {
    method: 'PUT',
    URI: 'v2/wishlist/updateWishListForUser',
  },
  deleteWishListItemForUser: {
    method: 'DELETE',
    URI: 'v2/wishlist/deleteItemFromWishList',
  },
  shareWishListForUser: {
    method: 'POST',
    URI: 'v2/wishlist/shareWishListForUser',
  },
  getStoreLocationByCountry: {
    method: 'GET',
    URI: 'v2/store/getStoreLocationByCountry',
  },
  startExpressCheckout: {
    method: 'POST',
    URI: 'v2/checkout/expressCheckout',
  },
  getInstagramAccessToken: {
    method: 'GET',
    URI: 'v2/instagram/accessToken',
  },
  userGroup: {
    method: 'PUT',
    URI: 'v2/account/userGroup',
  },
  updateRTPSdata: {
    method: 'POST',
    URI: 'v2/ads_dms/constructOLPS',
  },
  prescreenApplication: {
    method: 'POST',
    URI: 'v2/ads_dms/processPreScreenAcceptance',
  },
  processPreScreenOffer: {
    method: 'POST',
    URI: 'v2/ads_dms/processMadeOffer',
  },
  getBazaarVoiceRatings: {
    method: API_METHODS.GET,
    URI: 'data/reviews.json?ApiVersion=5.4&Passkey=#pass-key#&Include=Products&Stats=reviews&Filter=ProductId:#product-id#&Limit=#limit#',
    timeout: 30000,
  },
  pdpApi: {
    method: 'GET',
    URI: 'pdp/v1.0/#style-no#?brand=#brand#&lang=#lang#&site=#site#',
  },
  pdpProductApi: {
    method: 'GET',
    URI: 'pdp/v1.0/products/#style-no#?brand=#brand#&lang=#lang#&site=#site#',
  },
  getAppUpdatedVersion: {
    method: 'GET',
    URI: '/rwd/mobileapp/AppVersion.json',
  },
  saveOrUpdateBillingInfo: {
    method: 'PUT',
    URI: 'v2/checkout/billingInformation',
  },
  eDDServiceApi: {
    method: 'POST',
    URI: 'edd/v1.0/item/edd',
  },
  eddPersist: {
    method: 'POST',
    URI: 'v2/checkout/persistEddNode',
  },
  sendOrderEmail: {
    method: 'POST',
    URI: 'v2/wallet/resendEmailConfirmation',
  },
  getLoyaltyPoints: {
    method: API_METHODS.POST,
    URI: 'loyalty/v1.0/loyaltyPoints',
  },
  getLoyaltyPointsMule: {
    method: API_METHODS.POST,
    URI: 'v2/cart/loyaltyPoints',
  },
  getItemLevelRefund: {
    method: API_METHODS.PUT,
    URI: 'v1/customerservice/refund/item',
  },
  initiateReturn: {
    method: API_METHODS.PUT,
    URI: 'v1/customerservice/return/order',
  },
  getShipmentStatus: {
    method: API_METHODS.POST,
    URI: 'v1/customerservice/shipmentStatus',
  },
  getRefundStatus: {
    method: 'GET',
    URI: 'v1/customerservice/refund/eligible/sla',
  },
  getCancelStatus: {
    method: API_METHODS.PUT,
    URI: 'v1/customerservice/cancel/order',
  },
  getShipmentDelayedRefundStatus: {
    method: API_METHODS.PUT,
    URI: 'v1/customerservice/refund/shipping',
  },
  getSFCCAuthToken: {
    method: 'POST',
    URI: 'stateful/account/v1/auth/token',
  },
  sfccLogon: {
    method: 'POST',
    URI: 'stateful/account/v1/auth/login',
  },
  createUser: {
    method: API_METHODS.POST,
    URI: 'stateful/account/v1/user',
  },
  logoutUser: {
    method: API_METHODS.POST,
    URI: 'stateful/account/v1/auth/logout',
  },
  changePassword: {
    method: API_METHODS.PUT,
    URI: 'stateful/account/v1/changePassword',
  },
  requestPasswordMS: {
    method: API_METHODS.POST,
    URI: 'stateful/account/v1/auth/reset-password',
  },
  emailSmsSignUp: {
    method: API_METHODS.POST,
    URI: 'v2/store/emailSmsSignUp',
  },
  cartSync: {
    method: 'GET',
    URI: 'v2/cart/sfccBasketSync',
  },
  mergeAccountValidateMember: {
    method: API_METHODS.POST,
    URI: 'stateful/loyalty/v1/points/order/validateMember',
  },
  mergeAccountValidateMemberWithLocking: {
    method: API_METHODS.POST,
    URI: 'stateful/account/v1/validateMember',
  },
  mergeAccountGenerateOTP: (emailAddress) => {
    return {
      method: API_METHODS.GET,
      URI: `stateful/account/v1/generate/otp?emailId=${emailAddress}&featureType=AccountMerge`,
    };
  },
  mergeAccountValidateOTP: {
    method: API_METHODS.POST,
    URI: 'stateful/account/v1/validate/otp',
  },
  phoneLookupAPI: {
    method: API_METHODS.POST,
    URI: 'v2/account/phoneLookup',
  },
};
export default endpoints;
