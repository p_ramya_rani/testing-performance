// 9fbef606107a605d69c0edbcd8029e5d 
export const API_METHODS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

export const PRODUCTS_URI = {
  PRODUCTS: 'products',
  PRODUCTS_BY_OUTFITS: 'tcpproduct/getProductsByOutfits',
  PRODUCTS_VIEW_BY_CATEGORY: 'category',
  PRODUCTS_BY_SEARCH: 'search',
  PRODUCTS_AUTOSUGGEST: 'autosuggest',
};

export const SAVE_FOR_LATER = 'v2/cart/saveForLater';

export const graphQLClient = 'graphQL';
export const defaultCountry = 'USA';
export const defaultBrand = 'TCP';
export const defaultChannel = 'desktop';
export const MobileChannel = 'mobile';
export const WebViewHeaderChannel = 'app';
export const BRAND_TYPE_TCP = 'tcp';
export const BRAND_TYPE_GYM = 'gym';

export const errorMessages = {
  bootstrapError: 'ERROR IN RESPONSE OF BOOTSTRAP QUERY IN BOOTSTRAP ABSTRACTOR > bootstrap()',
  bootstrapAbstractorError: 'ERROR DUE TO JS PARSING FAIL IN BOOTSTRAP ABSTRACTOR',
  bootstrapSagaError: 'ERROR DUE TO JS PARSING FAIL IN BOOTSTRAP SAGA',
  navigationSagaError: 'ERROR DUE TO JS PARSING FAIL IN NAVIGATION SAGA',
  bootstrapFirstLoadError: 'ERROR IN BOOTSTRAP FIRST LOAD',
  configurationFirstLoadError: 'ERROR IN CONFIGURATION FIRST LOAD',
  navigationFirstLoadError: 'ERROR IN NAVIGATION FIRST LOAD',
  layoutFirstLoadError: 'ERROR IN LAYOUT FIRST LOAD',
  bootstrapModulesError: 'ERROR IN ONE OF MODULES QUERY :: BOOTSTRAP ABSTRACTOR bootstrap()',
  headerDataError: 'INVALID or NULL HEADER DATA :: BOOTSTRAP ABSTRACTOR :: parsedResponse()',
  footerDataError: 'INVALID or NULL FOOTER DATA :: BOOTSTRAP ABSTRACTOR :: parsedResponse()',
  labelsDataError: 'INVALID or NULL LABELS DATA :: BOOTSTRAP ABSTRACTOR :: parsedResponse()',
  navigationDataError:
    'INVALID or NULL NAVIGATION DATA :: BOOTSTRAP ABSTRACTOR :: parsedResponse()',
  layoutDataError: 'INVALID or NULL LAYOUT DATA :: BOOTSTRAP ABSTRACTOR :: parsedResponse()',
  plpDataError: 'INVALID PLP DATA :: BOOTSTRAP ABSTRACTOR :: parsedResponse()',
  errorParsingError:
    'ERROR IN JS PARSING ERROR RESPONSE :: BOOTSTRAP ABSTRACTOR :: checkAndLogErrors()',
  layoutError: 'INVALID LAYOUT DATA :: LAYOUT ABSTRACTOR :: getLayoutData()',
  genericBootstrapDataError: 'INVALID DATA :: LAYOUT ABSTRACTOR :: getLayoutData()',
  collateModuleError: 'ERROR IN SLOT DATA FOR MODULE :: LAYOUT ABSTRACTOR :: collateModuleObject()',
  moduleProcessingError: 'INVALID MODULE DATA :: LAYOUT ABSTRACTOR :: processModuleData()',
  fetchModuleDataError: 'ERROR IN FETCHING MODULE DATA :: LAYOUT ABSTRACTOR :: getModulesData()',
  navigationError: 'INVALID NAVIGATION DATA :: NAVIGATION ABSTRACTOR :: processData()',
  headerDataErrorCode: 'INVALID_HEADER_DATA',
  footerDataErrorCode: 'INVALID_FOOTER_DATA',
  labelsDataErrorCode: 'INVALID_LABELS_DATA',
  navigationDataErrorCode: 'INVALID_NAVIGATION_DATA',
  layoutDataErrorCode: 'INVALID_LAYOUT_DATA',
};

export const errorComponents = {
  bootstrapComponent: 'Bootstrap Abstractor',
  layoutAbstractor: 'Layout Abstractor',
  navigationAbstractor: 'Navigation Abstractor',
};

export default {
  API_METHODS,
  errorMessages,
  errorComponents,
  graphQLClient,
  defaultCountry,
  defaultBrand,
  defaultChannel,
  MobileChannel,
  WebViewHeaderChannel,
};
