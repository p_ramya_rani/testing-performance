// 9fbef606107a605d69c0edbcd8029e5d 
import { executeExternalAPICall } from '../../handler';
import endpoints from '../../endpoints';
import {
  getStyliticsUserName,
  getStyliticsRegion,
  isCanada,
  isGymboree,
  getApiTimeOut,
} from '../../../utils';

const FROM_ACCOUNT_USA = 'gymboree-girls,gymboree-boys';
const FROM_ACCOUNT_CANADA = 'gymboree-girls-ca,gymboree-boys-ca';

/**
 * Abstractor layer for loading Product List Tabs data
 */
const Abstractor = {
  /**
   * @param {Object} params Should have {itemId, count}. itemId is unique id of
   *  stylitics product. Count is total
   * number of the product. Count is optional.
   * @return {Object} return Promise.
   */
  getData: params => {
    const { categoryId, count = 20, isRelatedOutfit } = params;
    const {
      getStyliticsProductViewById: { method, URI, clientTimeout, serverTimeout },
    } = endpoints;
    const styliticsRegion = getStyliticsRegion();
    const payload = {
      body: {
        username: getStyliticsUserName(),
        total: count,
      },
      webService: {
        method,
        URI,
        reqTimeout: getApiTimeOut({ serverTimeout, clientTimeout }),
      },
    };

    if (isGymboree())
      payload.body.from_accounts = isCanada() ? FROM_ACCOUNT_CANADA : FROM_ACCOUNT_USA;
    if (isRelatedOutfit) {
      payload.body.item_number = categoryId;
    } else {
      payload.body.tags = categoryId;
    }

    if (styliticsRegion) {
      payload.body.region = styliticsRegion;
    }

    return executeExternalAPICall(payload)
      .then(Abstractor.processData)
      .catch(Abstractor.handleError);
  },
  getOutfitData: outfitId => {
    const {
      getStyliticsProductViewById: { method, URI, clientTimeout, serverTimeout },
    } = endpoints;

    const payload = {
      webService: {
        method,
        URI: `${URI}/${outfitId}`,
        reqTimeout: getApiTimeOut({ serverTimeout, clientTimeout }),
      },
    };
    return executeExternalAPICall(payload)
      .then(Abstractor.processOutfitData)
      .catch(Abstractor.handleError);
  },
  processOutfitData: res => {
    return {
      largeImageUrl: res.body.large_image_url,
    };
  },

  processData: res => {
    const rootPath = '/outfit';

    return res.body.map(item => {
      const { image_url: imageUrl, large_image_url: largeImageUrl, id, items: subItems } = item;

      const items = subItems.map(subItem => {
        const { small_image_url: smallImageUrl, name, remote_id: remoteId } = subItem;

        return {
          smallImageUrl,
          name,
          remoteId,
        };
      });

      const subItemsId = subItems.map(({ remote_id: remoteId }) => remoteId).join('-');

      return {
        id,
        subItemsId,
        imageUrl,
        largeImageUrl,
        items,
        pdpUrl: `${rootPath}/${id}/${subItemsId}`,
      };
    });
  },
  // eslint-disable-next-line no-console
  handleError: e => console.log(e),
};
export default Abstractor;
