// 9fbef606107a605d69c0edbcd8029e5d
import { SubmissionError } from 'redux-form'; // ES6
import CheckoutConstants from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import { getFormattedError, getDynamicCodeErrorMessage } from '../../../utils/errorMessage.util';
import { constructCouponStructure } from './CartItemTile';
import { constructSingleCouponStructure } from '../customerHelp/customerHelp';

export const applyCouponToCart = ({ couponCode = '' }, errorsMapping, cartItemsCount) => {
  if (!cartItemsCount) {
    const errorMessages = {
      _error: {
        msg: errorsMapping.TCP_PROMOTION_COUPON_ON_EMPTY_CART,
      },
    };
    throw new SubmissionError(errorMessages);
  }

  const payload = {
    webService: endpoints.addCoupons,
    body: {
      promoCode: couponCode.trim().toUpperCase(),
    },
  };

  const setPlaceCashError = ({ err: errorObj }) => {
    const placeCash = CheckoutConstants.PLACE_CASH;

    const err = errorObj;
    const isPlaceCashError =
      err.response.body &&
      err.response.body.errors &&
      err.response.body.errors[0].errorParameters &&
      err.response.body.errors[0].errorParameters[1] === placeCash;
    if (isPlaceCashError) {
      err.response.body.errors[0].errorCode = CheckoutConstants.PLACE_CASH_ERROR;
      err.response.body.errors[0].errorKey = CheckoutConstants.PLACE_CASH_ERROR;
    }
    return isPlaceCashError;
  };

  return executeStatefulAPICall(payload)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      const isPlaceCashError = setPlaceCashError({ err });
      const error = getFormattedError(err, errorsMapping);
      getDynamicCodeErrorMessage(error, couponCode);

      error.errorMessages = error.errorMessages || {
        _error: 'Oh no! Something went wrong… Please try again in a few minutes.',
      };
      const { errorMessages } = error;
      // eslint-disable-next-line
      errorMessages._error = {
        // eslint-disable-next-line
        msg: errorMessages._error,
        isPlaceCashError,
      };
      throw new SubmissionError(error.errorMessages);
    });
};

export const removeCouponOrPromo = ({ couponCode = '' }) => {
  const payload = {
    header: {
      promoCode: couponCode.toUpperCase(),
    },
    webService: endpoints.removeCouponOrPromo,
  };
  return executeStatefulAPICall(payload).then((res) => {
    const error = getFormattedError(res);
    if (error) {
      return new SubmissionError(error.errorMessages || { _error: 'Oops... an error occured' });
    }
    return { success: true };
  });
};

export const getAllCoupons = (isSourceLogin = false) => {
  const payload = {
    webService: endpoints.getAllOffers,
    header: {
      source: isSourceLogin ? 'login' : '', // Only if we send this header with login as value the updated coupons will be fetched from DMS.
    },
  };
  return executeStatefulAPICall(payload).then((res) => {
    if (res.body && res.body.offers) {
      return constructCouponStructure(res.body.offers);
    }
    throw new Error('There is some error in fetching all coupons');
  });
};

export const getCouponDetails = ({ couponCode = '' }) => {
  // default header is promo
  let codeTypeHeader = 'promo';

  // Check if passed code is coupon,If it starts with ‘Y’ and its length is 14-18, it is coupon,
  // Otherwise it is promo.
  if (couponCode.startsWith('Y') && couponCode.length >= 14 && couponCode.length <= 18) {
    codeTypeHeader = 'csh';
  }
  const payload = {
    webService: endpoints.getCouponDetails,
    header: {
      codeType: codeTypeHeader,
    },
    body: {
      couponCode: [couponCode.trim().toUpperCase()],
    },
  };
  return executeStatefulAPICall(payload).then((res) => {
    if (res?.body?.CouponDetails[0]) {
      if (res.body.CouponDetails[0]?.errorCode) {
        throw new Error(res.body.CouponDetails[0].errorCode);
      }
      return constructSingleCouponStructure(res.body.CouponDetails[0]);
    }
    throw new Error('There is some error in fetching coupon details');
  });
};

export default {
  applyCouponToCart,
  removeCouponOrPromo,
  getAllCoupons,
  getCouponDetails,
};
