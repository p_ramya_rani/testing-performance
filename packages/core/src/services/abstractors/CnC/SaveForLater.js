// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */

import endpoints from '../../endpoints';
import { executeStatefulAPICall } from '../../handler';
import { isCanada as isCASite, getAPIConfig } from '../../../utils';
import {
  responseContainsErrors,
  ServiceResponseError,
  getFormattedError,
} from '../../../utils/errorMessage.util';
import { extractPrioritizedBadge, getCartProductAttributes } from '../../../utils/badge.util';
import { sanitizeEntity, flatCurrencyToCents, AVAILABILITY } from './CartItemTile';

/**
 * @function getTcpStyleQTYByLocation This function returns the appropriate multipack Badge qty based on the site (CA or US)
 * @param item {Object} The item to be worked on.
 * @return {String} multipack Badge qty
 */
export function getTcpStyleQTYByLocation(item) {
  return !isCASite()
    ? item.productInfo && item.productInfo.itemTCPStyleQTYUS
    : item.productInfo && item.productInfo.itemTCPStyleQTYCA;
}
/**
 * @function getTcpStyleTypeByLocation This function returns the appropriate multipack Badge type based on the site (CA or US)
 * @param item {Object} The item to be worked on.
 * @return {String} multipack Badge type
 */
export function getTcpStyleTypeByLocation(item) {
  return !isCASite()
    ? item.productInfo && item.productInfo.itemTCPStyleTypeUS
    : item.productInfo && item.productInfo.itemTCPStyleTypeCA;
}
export function getProductInfo(item, imageGenerator) {
  const { isGiftCard, itemAtributes, itemBrand, multipackCatentryId = null } = item;
  const sizeAndFit = itemAtributes;
  const returnProductInfo = {
    generalProductId: isGiftCard ? item.itemCatentryId.toString() : item.productId,
    productPartNumber: item.productInfo && item.productInfo.productPartNumber,
    skuId: isGiftCard ? item.productId : item.itemCatentryId.toString(),
    name: sanitizeEntity(item.productInfo.productName),
    imagePath: imageGenerator(item.productInfo.productPartNumber)
      ? imageGenerator(item.productInfo.productPartNumber).productImages[500]
      : '',
    pdpUrl: item.productUrl.replace(/&amp;/g, '&'),
    color: {
      name: item.productInfo.productColor
        ? item.productInfo.productColor
        : item.productInfo.productName,
      imagePath: imageGenerator(item.productInfo.productThumbnail)
        ? imageGenerator(item.productInfo.productThumbnail).colorSwatch
        : '',
    },
    isGiftCard,
    colorFitSizeDisplayNames: isGiftCard ? true : {}, // To Do when consuming this data { color: 'Design', size: 'Value' }
    itemBrand,
    multiPackItems: multipackCatentryId,
    tcpStyleQty: getTcpStyleQTYByLocation(item),
    tcpStyleType: getTcpStyleTypeByLocation(item),
  };
  if (sizeAndFit) {
    returnProductInfo.size = sizeAndFit.TCPSize;
    returnProductInfo.fit = sizeAndFit.TCPFit;
    returnProductInfo.upc = isGiftCard ? sizeAndFit.partNumber : sizeAndFit.UPC;
  } else {
    returnProductInfo.size = item.itemUnitDstPrice;
    returnProductInfo.fit = null;
  }
  return returnProductInfo;
}

export function deriveSflItemAvailability(item) {
  if (item.inventoryAvail > 0) {
    return AVAILABILITY.OK;
  }
  return AVAILABILITY.UNAVAILABLE;
}

export function getSflItemInventory(item) {
  return item.inventoryAvail;
}

export function getItemOfferPrice(item, sizeAndFit, isCanada) {
  const { isGiftCard } = item;
  let Price = '';
  if (isGiftCard && sizeAndFit) {
    Price = flatCurrencyToCents(sizeAndFit.TCPSize);
  } else {
    Price = isCanada
      ? flatCurrencyToCents(item.productInfo.offerPriceCAD)
      : flatCurrencyToCents(item.productInfo.offerPrice);
  }
  return Price;
}

export function formatSflItems(sflResponse, imageGenerator, currencyCode, isCanada, cartSflMerged) {
  const sflObject = {
    sflItems: [],
    cartSflMerged,
  };

  sflResponse.forEach((item) => {
    const sizeAndFit = item.itemAtributes;

    sflObject.sflItems.push({
      productInfo: getProductInfo(item, imageGenerator),
      itemInfo: {
        itemId: item.itemCatentryId.toString(),
        offerPrice: getItemOfferPrice(item, sizeAndFit, isCanada),
        listPrice: flatCurrencyToCents(item.productInfo.listPrice),
      },
      miscInfo: {
        // set badge info
        badge: extractPrioritizedBadge(item.productInfo, getCartProductAttributes()),
        availability: deriveSflItemAvailability(item),
        sflAvailInventory: getSflItemInventory(item),
      },
      itemStatus: {},
    });
  });

  return sflObject;
}

/**
 * @function updateSflItem
 * @summary This API is used to remove an item from the sfl item list
 * @param {String} oldProductCatEntryId - CatEntryId of the sfl item that needs to be deleted
 * @param {String} newProductCatEntryId - CatEntryId of the sfl item that needs to be added
 * @param {Boolean} isRememberedUser - Is the user remembered
 * @param {Boolean} isRegistered - Is the user registered
 * @param {Object} imageGenerator - Image generator
 * @param {String} currencyCode - Currency code
 * @return Updated SFL items list
 */
export function updateSflItem(
  oldProductCatEntryId,
  newProductCatEntryId,
  isRememberedUser,
  isRegistered,
  imageGenerator,
  currencyCode,
  isCanada
) {
  const payload = {
    body: {
      delCatentryId: oldProductCatEntryId,
      catentryId: newProductCatEntryId,
      isRememberedUser,
      isRegistered,
    },
    webService: endpoints.updateSflItem,
  };

  return executeStatefulAPICall(payload)
    .then((res) => {
      if (responseContainsErrors(res)) {
        throw new ServiceResponseError(res);
      }
      return formatSflItems(res.body.sflItems, imageGenerator, currencyCode, isCanada);
    })
    .catch((err) => {
      throw getFormattedError(err);
    });
}

export function addItemToSflList(
  catEntryId,
  isRememberedUser,
  isRegistered,
  imageGenerator,
  currencyCode,
  isCanada,
  isSflItemDelete = false
) {
  let payload = {
    body: {
      isRememberedUser,
      isRegistered,
    },
    webService: endpoints.addSflItem,
  };
  if (catEntryId && Array.isArray(catEntryId)) {
    const cateEntryObj = [];
    catEntryId.forEach((id) => {
      const { catEntryId: catId, multiPackItems: multipackCatentryId = null } = id;
      if (multipackCatentryId) {
        cateEntryObj.push({ catentryId: catId, multipackCatentryId });
      } else {
        cateEntryObj.push({ catentryId: id });
      }
    });
    payload.body.catentryIds = cateEntryObj;
  } else {
    payload.body.catentryIds = [{ catentryId: catEntryId }];
  }

  if (isSflItemDelete) {
    const apiConfig = getAPIConfig();
    payload = {
      header: {
        'X-Cookie': apiConfig.cookie,
        catentryId: catEntryId,
        isRememberedUser,
        isRegistered,
      },
      webService: endpoints.deleteSflItem,
    };
  }

  return executeStatefulAPICall(payload)
    .then((res) => {
      if (responseContainsErrors(res)) {
        throw new ServiceResponseError(res);
      } else {
        return formatSflItems(res.body.sflItems, imageGenerator, currencyCode, isCanada);
      }
    })
    .catch((err) => {
      throw getFormattedError(err);
    });
}

export function getSflItems(imageGenerator, currencyCode, isCanada) {
  const apiConfig = getAPIConfig();
  const payload = {
    header: {
      'X-Cookie': apiConfig.cookie,
    },
    webService: endpoints.getAllSfl,
  };

  return executeStatefulAPICall(payload)
    .then((response) => {
      if (responseContainsErrors(response)) {
        throw new ServiceResponseError(response);
      } else {
        return formatSflItems(
          response.body.sflItems,
          imageGenerator,
          currencyCode,
          isCanada,
          response.body.cartSflMerged
        );
      }
    })
    .catch((err) => {
      throw getFormattedError(err);
    });
}

export default {
  formatSflItems,
  addItemToSflList,
  deriveSflItemAvailability,
  getSflItems,
  updateSflItem,
  getTcpStyleTypeByLocation,
  getTcpStyleQTYByLocation,
};
