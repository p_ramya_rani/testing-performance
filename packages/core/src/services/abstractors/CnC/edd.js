// 9fbef606107a605d69c0edbcd8029e5d
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';

const getEddItem = (eddObj, item) => {
  if (eddObj) {
    return {
      UGNR: eddObj.Standard && eddObj.Standard[item],
      U1DS: eddObj.Rush && eddObj.Rush[item],
      U2DY: eddObj.Express && eddObj.Express[item],
      PMDC: eddObj.Priority && eddObj.Priority[item],
      U1AK: eddObj.AKHIPRRush && eddObj.AKHIPRRush[item],
      U2AK: eddObj.AKHIPRExpress && eddObj.AKHIPRExpress[item],
    };
  }
  return null;
};

const getMinMaxDate = (eddObj) => {
  let edddateObj = {};
  if (eddObj) {
    edddateObj = {
      UGNR: eddObj.Standard,
      U1DS: eddObj.Rush,
      U2DY: eddObj.Express,
      PMDC: eddObj.Priority,
      U1AK: eddObj.AKHIPRRush,
      U2AK: eddObj.AKHIPRExpress,
    };
  }
  return edddateObj;
};

export const formatEdd = (arr) => {
  const eddData = [];
  let maxRangeDate = {};
  let minRangeDate = {};
  if (arr) {
    arr.eDDResponse.forEach((itm) => {
      if (!itm.carrierEDD) {
        return;
      }
      eddData.push({
        orderItemId: itm.sku.orderItemId,
        parentSkuId: itm.sku.parentSkuId,
        itemPartNumber: itm.sku.skuId,
        quantity: itm.sku.skuQty,
        node: itm.nodeId,
        nodeProcessingTime: getEddItem(itm.carrierEDD, 'nodeProcessingTime'),
        transitTime: getEddItem(itm.carrierEDD, 'transitTime'),
        carrier: getEddItem(itm.carrierEDD, 'carrier'),
        shippingDates: getEddItem(itm.carrierEDD, 'edd'),
      });
    });
    maxRangeDate = getMinMaxDate(arr.carrierEDDMax);
    minRangeDate = getMinMaxDate(arr.carrierEDDMin);
  }
  return {
    itemsWithEdd: eddData,
    orderEddDates: {
      maxDates: maxRangeDate,
      minDates: minRangeDate,
    },
  };
};
/**
 * @function getEdd
 * @summary
 * @param {String}
 */
export const getEdd = (params) => {
  const payload = {
    webService: endpoints.eDDServiceApi,
    body: params,
    skipApiIdentifier: true,
  };
  return executeStatefulAPICall(payload)
    .then((res) => {
      return formatEdd(res.body && res.body.data);
    })
    .catch((err) => {
      throw err;
    });
};

export default { getEdd };
