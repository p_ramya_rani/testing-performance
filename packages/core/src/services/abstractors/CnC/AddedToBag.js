// 9fbef606107a605d69c0edbcd8029e5d
import ORDER_ITEM_TYPE from '@tcp/core/src/components/features/CnC/CartItemTile/CartItemTile.constants';
import { isMobileApp } from '@tcp/core/src/utils';
import { executeUnbxdAPICall, executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';

import {
  responseContainsErrors,
  ServiceResponseError,
  getFormattedError,
} from '../../../utils/errorMessage.util';

export const getUnboxResult = (endPoint, query) =>
  executeUnbxdAPICall({
    body: {
      rows: 20,
      variants: true,
      'variants.count': 100,
      version: 'V2',
      'facet.multiselect': true,
      selectedfacet: true,
      q: query,
      promotion: false,
      pagetype: 'boolean',
      fields:
        'productimage,alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,low_offer_price,high_offer_price,low_list_price,high_list_price,long_product_title',
    },
    webService: endPoint,
  }).then((res) => res.body.response.products);

export const getPlpProducts = () => getUnboxResult(endpoints.getProductsBySearchTerm, 'denim');
export const getGiftCardProducts = () =>
  getUnboxResult(endpoints.getProductsBySearchTerm, 'gift card');

export const flatCurrencyToCents = (currency = 0) => {
  try {
    return parseFloat(parseFloat(currency.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]).toFixed(2));
  } catch (e) {
    throw new Error(e);
  }
};

const formatOrderItems = (items) => {
  const orderItems = [];
  items.forEach((item) => {
    orderItems.push({
      itemInfo: {
        quantity: parseInt(item.qty, 10),
        itemId: item.orderItemId.toString(),
        itemPoints: item.itemPoints || item.itemPoints === 0 ? parseInt(item.itemPoints, 10) : null,
        // This code is misleading - itemPrice and itemDstPrice are not equal to list price/offer price
        // Backend returns the same value for both itemPrice and itemDstPrice UNLESS an explicit promotion is applied
        // Enhancement needed - Backend should return the actual prices and frontend should determine which values to display

        listPrice: flatCurrencyToCents(item.itemPrice),
        offerPrice: flatCurrencyToCents(item.itemDstPrice),
      },
    });
  });
  return orderItems;
};

const getOrderTypes = (items) => {
  let obj = {};

  if (items) {
    let isBossOrder = items.find((store) => store.orderItemType === ORDER_ITEM_TYPE.BOSS);
    let isBopisOrder = items.find((store) => store.orderItemType === ORDER_ITEM_TYPE.BOPIS);
    let isECOM = items.find((store) => store.orderItemType === ORDER_ITEM_TYPE.ECOM);

    isBopisOrder = !!isBopisOrder;
    isBossOrder = !!isBossOrder;
    isECOM = !!isECOM;
    const isPickupOrder = isBopisOrder || isBossOrder;

    obj = {
      isBopisOrder,
      isBossOrder,
      isECOM,
      isPickupOrder,
    };
  }
  return obj;
};

export const addMultipleProductsInEcom = (
  paramsArray,
  errorMapping,
  getErrorMessage,
  isNewReDesign
) => {
  const atbSuccessProducts = [];

  // eslint-disable-next-line sonarjs/cognitive-complexity
  return paramsArray.reduce((initialSynchPromise, params) => {
    const paramsProduct = params.product;
    const paramsProductId = params.productId;
    // deleting the product and productId from request as it is not required by the api
    // eslint-disable-next-line no-param-reassign
    delete params.product;
    // eslint-disable-next-line no-param-reassign
    delete params.productId;
    return initialSynchPromise.then(() => {
      return executeStatefulAPICall({ body: params, webService: endpoints.addProductToCart })
        .then((res) => {
          if (responseContainsErrors(res)) {
            throw new ServiceResponseError(res);
          }
          atbSuccessProducts.push({
            orderId: res.body.orderId && res.body.orderId[0],
            orderItemId: res.body.orderItemId && res.body.orderItemId[0],
            orderTotal: res.body.omniture && res.body.omniture.orderTotal,
            orderItemTotal: res.body.omniture && res.body.omniture.orderItemTotal,
            orderItems: res.body.orderItems && formatOrderItems(res.body.orderItems),
            estimatedRewards: res.body.userPoints && Math.round(res.body.userPoints),
            isBrierleyWorking: res.body.userPoints !== -1,
            grandTotal: flatCurrencyToCents(res.body.grandTotal),
            pointsToNextReward: res.body.pointsToNextReward || 0,
            totalItems: res.body.cartCount || 0,
            giftCardsTotal: res.body.totalGiftCardAmount,
            earnedReward: res.body.earnedReward,
            cartTotalAfterPLCCDiscount: res.body.cartTotalAfterPLCCDiscount,
            orderSubTotalDiscount: res.body.orderSubTotalDiscount,
            loyaltyOrderItems: res.body.orderItems,
            isElectiveBonus: res.body.isElectiveBonus,
            orderSubTotal: res.body.orderSubTotal,
            orderSubTotalBeforeDiscount: res.body.orderSubTotalBeforeDiscount,
            orderSubTotalWithoutGiftCard: res.body.orderSubTotalWithoutGiftCard,
            orderTotalAfterDiscount: res.body.orderTotalAfterDiscount,
            orderTotalListPrice: res.body.orderTotalListPrice,
            orderTotalSaving: res.body.orderTotalSaving,
            mprId: res.body.mprId,
            loyaltyCallEnabled: res.body.loyaltyCallEnabled,
            pointsApi: res.body.pointsApi,
            displayLowInvMonetateExp: res.body.displayLowInvMonetateExp,
            ...getOrderTypes(res.body.orderItems),
            ...paramsProduct,
          });
          return atbSuccessProducts;
        })
        .catch((err) => {
          if (isMobileApp() && isNewReDesign) {
            const errorData = getFormattedError(err, errorMapping);
            const errorMessage = getErrorMessage(errorData, errorMapping);

            //  Not throwing error when running on mobile app to add remaining products to cart.
            atbSuccessProducts.push({
              ...paramsProduct,
              error: errorData,
              errorMessage,
              errorProductId: paramsProductId,
              grandTotal:
                atbSuccessProducts.length > 0
                  ? atbSuccessProducts[atbSuccessProducts.length - 1].grandTotal
                  : '',
            });
            //  Grand total is required in above object for below scenario.
            //  The product with highest price is set to 'payloadData' in state. If highest priced product
            //  fails to add to cart, in that case that product will not get grandTotal. Which will result
            //  in empty subtotal on screen. To avoid this, grandTotal is added from previously added product.
            return atbSuccessProducts;
          }
          // eslint-disable-next-line no-throw-literal
          throw {
            error: getFormattedError(err, errorMapping),
            errorProductId: paramsProductId,
            atbSuccessProducts,
          };
        });
    });
  }, Promise.resolve());
};

export const addCartEcomItem = (params, errorMapping) =>
  executeStatefulAPICall({ body: params, webService: endpoints.addProductToCart })
    .then((res) => {
      if (responseContainsErrors(res)) {
        throw new ServiceResponseError(res);
      }
      return {
        orderId: res.body.orderId && res.body.orderId[0],
        orderItemId: res.body.orderItemId && res.body.orderItemId[0],
        orderTotal: res.body.omniture && res.body.omniture.orderTotal,
        orderItemTotal: res.body.omniture && res.body.omniture.orderItemTotal,
        orderItems: res.body.orderItems && formatOrderItems(res.body.orderItems),
        estimatedRewards: res.body.userPoints && Math.round(res.body.userPoints),
        isBrierleyWorking: res.body.userPoints !== -1,
        grandTotal: flatCurrencyToCents(res.body.grandTotal),
        pointsToNextReward: res.body.pointsToNextReward || 0,
        totalItems: res.body.cartCount || 0,
        giftCardsTotal: res.body.totalGiftCardAmount,
        earnedReward: res.body.earnedReward,
        cartTotalAfterPLCCDiscount: res.body.cartTotalAfterPLCCDiscount,
        orderSubTotalDiscount: res.body.orderSubTotalDiscount,
        loyaltyOrderItems: res.body.orderItems,
        isElectiveBonus: res.body.isElectiveBonus,
        orderSubTotal: res.body.orderSubTotal,
        orderSubTotalBeforeDiscount: res.body.orderSubTotalBeforeDiscount,
        orderSubTotalWithoutGiftCard: res.body.orderSubTotalWithoutGiftCard,
        orderTotalAfterDiscount: res.body.orderTotalAfterDiscount,
        orderTotalListPrice: res.body.orderTotalListPrice,
        orderTotalSaving: res.body.orderTotalSaving,
        mprId: res.body.mprId,
        loyaltyCallEnabled: res.body.loyaltyCallEnabled,
        pointsApi: res.body.pointsApi,
        displayLowInvMonetateExp: res.body.displayLowInvMonetateExp,
        ...getOrderTypes(res.body.orderItems),
      };
    })
    .catch((err) => {
      throw getFormattedError(err, errorMapping, true);
    });

export const addCartBopisItem = (params, errorMapping) =>
  executeStatefulAPICall({ body: params, webService: endpoints.addOrderBopisItem })
    .then((res) => {
      if (responseContainsErrors(res)) {
        throw new ServiceResponseError(res);
      }
      return {
        orderItemId: res.body.orderItemId,
        orderId: res.body.orderId,
        orderItems: res.body.orderItems && formatOrderItems(res.body.orderItems),
        estimatedRewards: res.body.userPoints && Math.round(res.body.userPoints),
        isBrierleyWorking: res.body.userPoints !== -1,
        grandTotal: flatCurrencyToCents(res.body.grandTotal),
        pointsToNextReward: res.body.pointsToNextReward || 0,
        totalItems: res.body.cartItemsCount || 0,
        giftCardsTotal: res.body.totalGiftCardAmount,
        earnedReward: res.body.earnedReward,
        cartTotalAfterPLCCDiscount: res.body.cartTotalAfterPLCCDiscount,
        orderSubTotalDiscount: res.body.orderSubTotalDiscount,
        loyaltyOrderItems: res.body.orderItems,
        orderSubTotal: res.body.orderSubTotal,
        orderSubTotalBeforeDiscount: res.body.orderSubTotalBeforeDiscount,
        orderSubTotalWithoutGiftCard: res.body.orderSubTotalWithoutGiftCard,
        orderTotalAfterDiscount: res.body.orderTotalAfterDiscount,
        orderTotalListPrice: res.body.orderTotalListPrice,
        orderTotalSaving: res.body.orderTotalSaving,
        mprId: res.body.mprId,
        loyaltyCallEnabled: res.body.loyaltyCallEnabled,
        pointsApi: res.body.pointsApi,
        ...getOrderTypes(res.body.orderItems),
      };
    })
    .catch((err) => {
      throw getFormattedError(err, errorMapping);
    });

export default {
  getPlpProducts,
  getGiftCardProducts,
  addCartEcomItem,
  addCartBopisItem,
  addMultipleProductsInEcom,
};
