// 9fbef606107a605d69c0edbcd8029e5d 
import logger from '@tcp/core/src/utils/loggerInstance';
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
// Will change res in this as soon the new data to be shown is known
const getFormattedData = res => {
  const { userState, clientToken } = res;
  return {
    appleClientTokenData: {
      userState,
      clientToken,
    },
  };
};

export const getAppleToken = ({ userState, orderId }) => {
  try {
    const payload = {
      header: {
        orderId,
        userState,
      },
      webService: endpoints.getAppleClientToken,
    };
    return executeStatefulAPICall(payload)
      .then(res => {
        const massagedData = getFormattedData(res.body);
        return massagedData || {};
      })
      .catch(err => {
        logger.info('Apple Pay Token not received', err);
        logger.error('Error: Occurred in getting apple-pay token - executeStateFul API Call', err);
      });
  } catch (err) {
    logger.error('Error: Occurred in getting apple-pay token', err);
    return null;
  }
};
export default getAppleToken;
