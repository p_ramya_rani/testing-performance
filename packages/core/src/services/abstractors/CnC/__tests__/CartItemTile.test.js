/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import {
  getOrderDetailsData,
  flatCurrencyToCents,
  getCurrentOrderFormatter,
  getProductImgPath,
  getSwatchImgPath,
  imageGenerator,
  constructCouponStructure,
  sanitizeEntity,
  deriveBossEligiblity,
  deriveItemAvailability,
  paypalAuthorizationAPI,
  startPaypalCheckoutAPI,
  getCartData,
  getUnqualifiedItems,
  toTimeString,
  getProductSkuInfoByUnbxd,
  removeItem,
  capitalize,
  updateItem,
  getProductInfoForTranslationData,
} from '../CartItemTile';
import {
  response,
  responseBoss,
  responseBopis,
  orderDetailsResponse,
  couponResponse,
  couponFormatResponse,
} from './mockData';
import { executeStatefulAPICall, executeUnbxdAPICall } from '../../../handler/handler';

jest.mock('../../../handler/handler', () => ({
  executeStatefulAPICall: jest.fn(),
  executeUnbxdAPICall: jest.fn(),
}));

// TODO - Include more test cases
describe('#getOrderPointSummary', () => {
  it('should return valid getOrderDetailsData response', () => {
    const result = 'foo';
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getOrderDetailsData().then((data) => {
      expect(data).toBe(result);
    });
  });

  it('should call setBrierleyOrderPointsTimeCache', () => {
    const result = {
      req: {
        header: {
          recalculate: true,
        },
      },
      body: {
        test: 'Test',
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getOrderDetailsData({}).then((data) => {
      expect(data).toBeDefined();
    });
  });

  it('should return valid flatCurrencyToCents currency', () => {
    const resultType = flatCurrencyToCents(123);
    expect(resultType).toEqual(123);
  });

  it('should return throw error with invalid input to flatCurrencyToCents currency', () => {
    expect(() => flatCurrencyToCents([])).toThrow("TypeError: Cannot read property '0' of null");
  });

  it('should return valid getCurrentOrderFormatter', () => {
    const result = getCurrentOrderFormatter(orderDetailsResponse, false, false);
    const {
      checkout: {
        shipping: {
          address: { addressId, ...rest },
          ...restShipping
        },
        ...restCheckout
      },
      ...restResp
    } = response;
    const responseData = {
      ...restResp,
      orderType: 'PURE_ECOM',
      checkout: { ...restCheckout, shipping: { address: rest, ...restShipping } },
    };
    expect(result).toEqual(responseData);
  });

  it('should return valid getCurrentOrderFormatter without sales tax', () => {
    const orderDetailsResponse1 = { ...orderDetailsResponse };
    orderDetailsResponse1.salesTax = null;
    const result = getCurrentOrderFormatter(orderDetailsResponse1, false, false);
    const {
      checkout: {
        shipping: {
          address: { addressId, ...rest },
          ...restShipping
        },
        ...restCheckout
      },
      ...restResp
    } = response;
    const responseData = {
      ...restResp,
      orderType: 'PURE_ECOM',
      isBrierleyWorking: true,
      totalTax: undefined,
      checkout: { ...restCheckout, shipping: { address: rest, ...restShipping } },
    };
    expect(result).toEqual(responseData);
  });

  it('should return valid getCurrentOrderFormatter for Boss Order', () => {
    const orderDetailsResponseBoss = { ...orderDetailsResponse, pointsToNextReward: -1 };
    orderDetailsResponseBoss.mixOrderDetails.data[0].orderType = 'BOSS';
    orderDetailsResponseBoss.orderItems[0].orderType = 'BOSS';
    orderDetailsResponseBoss.orderItems[0].stLocId = '1';
    orderDetailsResponseBoss.orderItems[0].orderItemType = 'BOSS';
    orderDetailsResponseBoss.orderItems[1].orderType = 'BOSS';
    orderDetailsResponseBoss.orderItems[2].orderType = 'BOSS';
    orderDetailsResponseBoss.orderItems[3].orderType = 'BOSS';
    orderDetailsResponseBoss.mixOrderDetails.data[0].shippingAddressDetails.stLocId = '1';
    orderDetailsResponseBoss.mixOrderDetails.data[0].shippingAddressDetails.bossMinDate =
      'Nov 4, 2020 7:05:03 AM';
    orderDetailsResponseBoss.mixOrderDetails.data[0].shippingAddressDetails.bossMaxDate =
      'Nov 4, 2020 7:05:03 AM';
    const result = getCurrentOrderFormatter(orderDetailsResponseBoss, false, false);

    expect(result).toEqual(responseBoss);

    orderDetailsResponseBoss.orderItems[0].stLocId = '';
    orderDetailsResponseBoss.mixOrderDetails.data[0].shippingAddressDetails.stLocId = '';
  });

  it('should return valid getCurrentOrderFormatter for BOPIS Order', () => {
    const orderDetailsResponseBoss = { ...orderDetailsResponse, pointsToNextReward: -1 };
    orderDetailsResponseBoss.mixOrderDetails.data[0].orderType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[0].orderType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[0].orderItemType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[1].orderType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[2].orderType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[3].orderType = 'BOPIS';
    const result = getCurrentOrderFormatter(orderDetailsResponseBoss, false, false);
    expect(result).toEqual(responseBopis);
  });

  it('should return valid getCurrentOrderFormatter for BOPIS Order when giftItem', () => {
    const orderDetailsResponseBoss = {
      ...orderDetailsResponse,
      pointsToNextReward: -1,
      mixOrderPaymentDetails: [],
    };
    orderDetailsResponseBoss.mixOrderDetails.data[0].orderType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[0].orderType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[0].giftItem = true;
    orderDetailsResponseBoss.orderItems[0].orderItemType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[1].orderType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[2].orderType = 'BOPIS';
    orderDetailsResponseBoss.orderItems[3].orderType = 'BOPIS';
    const result = getCurrentOrderFormatter(orderDetailsResponseBoss, false, false);
    const responseBOPIS = { ...responseBopis };
    responseBOPIS.orderItems[0].productInfo.generalProductId = '1301516';
    responseBOPIS.orderItems[0].productInfo.isGiftCard = true;
    responseBOPIS.orderItems[0].productInfo.skuId = '1297873';
    responseBOPIS.orderItems[0].productInfo.colorFitSizeDisplayNames = {
      color: 'Design',
      size: 'Value',
    };
    responseBOPIS.loyaltyPointsRequestPayload.loyaltyOrderItems[0].giftItem = true;
    expect(result).toEqual(responseBOPIS);
  });

  it('should return getSwatchImgPath=', () => {
    const resultType = getSwatchImgPath('12_12', 'extension');
    expect(resultType).toEqual('12/12_12');
  });

  it('should return getProductImgPath=', () => {
    const resultType = imageGenerator('12_10', 'extension');
    expect(resultType).toEqual({
      colorSwatch: '12/12_10',
      productImages: {
        125: '12/12_10',
        380: '12/12_10',
        500: '12/12_10',
        900: '12/12_10',
      },
    });
  });
  it('should return getProductImgPath=', () => {
    const resultType = getProductImgPath('12_10', 'extension');
    expect(resultType).toEqual({
      125: '12/12_10',
      380: '12/12_10',
      500: '12/12_10',
      900: '12/12_10',
    });
  });
  it('should return valid constructCouponStructure', () => {
    let result = constructCouponStructure(couponResponse);
    result = Object.assign({}, result[0], {
      expirationDateTimeStamp: '',
    });
    expect([result]).toEqual(couponFormatResponse);
  });

  it('should return valid constructCouponStructure response', () => {
    const temp = [
      {
        ...couponResponse[0],
        offerType: 'PC',
      },
    ];
    const expected = [
      {
        ...couponFormatResponse[0],
        offerType: 'PLACECASH',
        promotionType: 'PLACECASH',
        redemptionType: 'PLACECASH',
        originalOfferType: 'PC',
      },
    ];
    let result = constructCouponStructure(temp);
    result = Object.assign({}, result[0], {
      expirationDateTimeStamp: '',
    });
    expect([result]).toEqual(expected);
  });

  it('should return valid constructCouponStructure LOYALTY', () => {
    const temp = [
      {
        ...couponResponse[0],
        offerType: 'LOYALTY',
      },
    ];
    const expected = [
      {
        ...couponFormatResponse[0],
        offerType: 'rewards',
        promotionType: 'LOYALTY',
        redemptionType: 'LOYALTY',
        originalOfferType: 'LOYALTY',
      },
    ];
    let result = constructCouponStructure(temp);
    result = Object.assign({}, result[0], {
      expirationDateTimeStamp: '',
    });
    expect([result]).toEqual(expected);
  });

  it('should return valid constructCouponStructure PLACECASH', () => {
    const temp = [
      {
        ...couponResponse[0],
        offerType: 'PLACECASH',
      },
    ];
    const expected = [
      {
        ...couponFormatResponse[0],
        offerType: 'PLACECASH',
        promotionType: 'public',
        redemptionType: 'PLACECASH',
        isExpired: undefined,
        originalOfferType: 'PLACECASH',
      },
    ];
    let result = constructCouponStructure(temp);
    result = Object.assign({}, result[0], {
      expirationDateTimeStamp: '',
    });
    expect([result]).toEqual(expected);
  });

  it('should return valid constructCouponStructure saving', () => {
    const temp = [
      {
        ...couponResponse[0],
        offerType: 'OTHERS',
      },
    ];
    const expected = [
      {
        ...couponFormatResponse[0],
        offerType: 'saving',
        promotionType: 'public',
        redemptionType: undefined,
        isExpired: undefined,
        originalOfferType: 'OTHERS',
      },
    ];
    let result = constructCouponStructure(temp);
    result = Object.assign({}, result[0], {
      expirationDateTimeStamp: '',
    });
    expect([result]).toEqual(expected);
  });

  it('should return valid constructCouponStructure isApplied', () => {
    const temp = [
      {
        ...couponResponse[0],
        isApplied: true,
      },
    ];
    const expected = [
      {
        ...couponFormatResponse[0],
        offerType: 'saving',
        status: 'applied',
        labelStatus: 'REMOVE',
      },
    ];
    let result = constructCouponStructure(temp);
    result = Object.assign({}, result[0], {
      expirationDateTimeStamp: '',
    });
    expect([result]).toEqual(expected);
  });
});

describe('#sanitizeEntity', () => {
  it('should convert &amp; to &', () => {
    const resultType = sanitizeEntity('&amp;');
    expect(resultType).toEqual('&');
  });

  it('should convert &quot; to "', () => {
    const resultType = sanitizeEntity('&quot;');
    expect(resultType).toEqual('"');
  });

  it('should convert &ldquo to "', () => {
    const resultType = sanitizeEntity('&ldquo;');
    expect(resultType).toEqual('"');
  });

  it('should convert &acute; to "', () => {
    const resultType = sanitizeEntity('&acute;');
    expect(resultType).toEqual('"');
  });

  it('should convert &prime; to "', () => {
    const resultType = sanitizeEntity('&prime;');
    expect(resultType).toEqual('"');
  });

  it('should convert &bdquo; to "', () => {
    const resultType = sanitizeEntity('&bdquo;');
    expect(resultType).toEqual('"');
  });

  it('should convert &ldquot; to "', () => {
    const resultType = sanitizeEntity('&ldquot;');
    expect(resultType).toEqual('"');
  });

  it('should convert &lsquot; to "', () => {
    const resultType = sanitizeEntity('&lsquot;');
    expect(resultType).toEqual('"');
  });

  it('should convert %20 to  ', () => {
    const resultType = sanitizeEntity('%20');
    expect(resultType).toEqual(' ');
  });

  it('should return no string values', () => {
    const resultType = sanitizeEntity(10);
    expect(resultType).toEqual(10);
  });
});

describe('#deriveBossEligiblity', () => {
  it('shoould be falsy when itemTCpBossProductDisabled', () => {
    const item = {
      productInfo: {
        itemTcpBossProductDisabled: true,
      },
    };
    expect(deriveBossEligiblity(item, null)).toBeFalsy();
  });

  it('shoould be falsy when itemTcpBossCategoryDisabled', () => {
    const item = {
      productInfo: {
        itemTcpBossCategoryDisabled: true,
      },
    };
    expect(deriveBossEligiblity(item, null)).toBeFalsy();
  });

  it('shoould be falsy when bossIntlField', () => {
    const orderDetailsResponse1 = {
      bossIntlField: true,
    };
    const item = {
      productInfo: {},
    };
    expect(deriveBossEligiblity(item, orderDetailsResponse1)).toBeFalsy();
  });
});

describe('#deriveItemAvailability', () => {
  const orderDetails = {
    currencyCode: 'USD',
    bopisIntlField: '',
    bossIntlField: '',
  };
  const item = {
    productInfo: {
      articleOOSUS: null,
    },
    stLocId: '',
    orderItemType: '',
  };
  const store = {
    shippingAddressDetails: {
      isStoreBOSSEligible: true,
    },
  };
  const isRadialInvEnabled = true;

  it('should return OK', () => {
    const item1 = { ...item };
    item1.orderItemType = 'BOPIS';
    item1.stLocId = 'adf';
    orderDetails.bopisIntlField = false;
    item1.productInfo.articleOOSUS = false;
    expect(deriveItemAvailability(orderDetails, item1, store, isRadialInvEnabled)).toBe('OK');
  });

  it('should return UNAVAILABLE', () => {
    const item1 = { ...item };
    item1.qty = 10;
    item1.inventoryAvail = 5;
    expect(deriveItemAvailability(orderDetails, item1, store, isRadialInvEnabled)).toBe(
      'UNAVAILABLE'
    );
  });

  it('should return OK if inventoryAvail is more than 0', () => {
    const item1 = { ...item };
    item1.inventoryAvail = 5;
    expect(deriveItemAvailability(orderDetails, item1, store, isRadialInvEnabled)).toBe('OK');
  });

  it('should return UNAVAILABLE for Boss items', () => {
    const item1 = { ...item };
    item1.inventoryAvail = 5;
    item1.orderItemType = 'BOSS';
    item1.stLocId = 1;
    orderDetails.bossIntlField = true;
    expect(deriveItemAvailability(orderDetails, item1, store, isRadialInvEnabled)).toBe(
      'UNAVAILABLE'
    );
  });

  it('should return OK for Boss items with isRadialInvEnabled', () => {
    const item1 = { ...item };
    item1.inventoryAvail = 5;
    item1.orderItemType = 'BOSS';
    item1.stLocId = 1;
    orderDetails.bossIntlField = false;
    orderDetails.isStoreBOSSEligible = true;
    expect(deriveItemAvailability(orderDetails, item1, undefined, isRadialInvEnabled)).toBe('OK');
  });

  it('should return UNAVAILABLE for Boss items with isRadialInvEnabled', () => {
    const item1 = { ...item };
    item1.inventoryAvail = 5;
    item1.inventoryAvailBOSS = 0;
    item1.orderItemType = 'BOSS';
    item1.stLocId = 1;
    orderDetails.bossIntlField = false;
    orderDetails.isStoreBOSSEligible = true;
    expect(deriveItemAvailability(orderDetails, item1, store, true)).toBe('UNAVAILABLE');
  });

  it('should return REQ_QTY_UNAVAILABLE for Boss items with isRadialInvEnabled', () => {
    const item1 = { ...item };
    item1.inventoryAvail = 5;
    item1.inventoryAvailBOSS = 10;
    item1.qty = 20;
    item1.orderItemType = 'BOSS';
    item1.stLocId = 1;
    orderDetails.bossIntlField = false;
    orderDetails.isStoreBOSSEligible = true;
    expect(deriveItemAvailability(orderDetails, item1, store, true)).toBe('REQ_QTY_UNAVAILABLE');
  });

  it('should return BOSSINELIGIBLE for Boss items with isRadialInvEnabled', () => {
    const item1 = { ...item };
    item1.inventoryAvail = 5;
    item1.inventoryAvailBOSS = 10;
    item1.qty = 2;
    item1.productInfo.itemTcpBossProductDisabled = true;
    item1.orderItemType = 'BOSS';
    item1.stLocId = 1;
    orderDetails.bossIntlField = false;
    orderDetails.isStoreBOSSEligible = true;
    expect(deriveItemAvailability(orderDetails, item1, store, true)).toBe('BOSSINELIGIBLE');
  });

  it('should return OK for Boss items without isRadialInvEnabled', () => {
    const item1 = { ...item };
    item1.inventoryAvail = 5;
    item1.orderItemType = 'BOSS';
    item1.stLocId = 1;
    orderDetails.bossIntlField = false;
    orderDetails.isStoreBOSSEligible = true;
    expect(deriveItemAvailability(orderDetails, item1, store, false)).toBe('OK');
  });

  it('should return UNAVAILABLE for Boss items without isRadialInvEnabled', () => {
    const item1 = { ...item };
    item1.inventoryAvail = -1;
    item1.orderItemType = 'BOSS';
    item1.stLocId = 1;
    orderDetails.bossIntlField = false;
    orderDetails.isStoreBOSSEligible = true;
    expect(deriveItemAvailability(orderDetails, item1, store, false)).toBe('UNAVAILABLE');
  });

  it('should return UNAVAILABLE when inventoryAvail is zero', () => {
    const item1 = { ...item };
    item1.inventoryAvail = 0;
    item1.orderItemType = null;
    item1.stLocId = 1;
    orderDetails.bossIntlField = false;
    expect(deriveItemAvailability(orderDetails, item1, store, isRadialInvEnabled)).toBe(
      'UNAVAILABLE'
    );
  });
});

describe('#paypalAuthorizationAPI', () => {
  const payloadArgs = {
    tcpOrderId: '',
    centinelRequestPage: '',
    centinelPayload: '',
    centinelOrderId: '',
  };
  it('should return success true for paypalAuthorizationAPI', () => {
    const result = {
      success: true,
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    paypalAuthorizationAPI(payloadArgs).then((data) => {
      expect(data).toMatchObject(result);
    });
  });

  it('should return success true for paypalAuthorizationAPI when response is undefined', () => {
    const result = {
      success: true,
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(undefined));
    paypalAuthorizationAPI(payloadArgs).then((data) => {
      expect(data).toMatchObject(result);
    });
  });

  it('Should throw errors in case of server side error paypalAuthorizationAPI', () => {
    const result = {
      body: {
        errors: [
          {
            errorMessage: 'Test Error Message paypalAuthorizationAPI',
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    paypalAuthorizationAPI(payloadArgs).then((data) => {
      expect(data).toEqual('Test Error Message paypalAuthorizationAPI');
    });
  });
});

describe('#startPaypalCheckoutAPI', () => {
  const payloadArgs = {};
  it('should return success true', () => {
    const result = {
      success: true,
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    startPaypalCheckoutAPI(payloadArgs).then((data) => {
      expect(data).toMatchObject(result);
    });
  });

  it('Should throw errors in case of server side error startPaypalCheckoutAPI', () => {
    const result = {
      body: {
        errors: [
          {
            errorMessage: 'Test Error Message startPaypalCheckoutAPI',
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    startPaypalCheckoutAPI(payloadArgs).then((data) => {
      expect(data).toEqual('Test Error Message startPaypalCheckoutAPI');
    });
  });

  it('should return success true when result is undefined', () => {
    const result = {
      success: true,
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(undefined));
    startPaypalCheckoutAPI(payloadArgs).then((data) => {
      expect(data).toMatchObject(result);
    });
  });
});

describe('#getCartData', () => {
  it('should return valid getCartData response', () => {
    const result = 'foo';
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getCartData({}).then((data) => {
      expect(data).toBe(result);
    });
  });

  it('should call setBrierleyOrderPointsTimeCache', () => {
    const result = {
      req: {
        header: {
          recalculate: true,
        },
      },
      body: {
        test: 'Test',
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getCartData({}).then((data) => {
      expect(data).toBeDefined();
    });
  });

  it('Should throw errors in case of server side error getCartData', () => {
    const result = {
      body: {
        errors: [
          {
            errorMessage: 'Test Error Message getCartData',
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getCartData({}).then((data) => {
      expect(data).toEqual('Test Error Message getCartData');
    });
  });

  it('should return valid getCartData response with excludeCartItems', () => {
    const result = {
      req: {
        header: {
          recalculate: true,
        },
      },
      body: {
        test: 'Test',
        orderDetails: {
          orderDetailsResponse: 'test',
        },
        coupons: {
          test: 'test',
        },
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getCartData({ excludeCartItems: true, isCheckoutFlow: true }).then((data) => {
      expect(data).toBeDefined();
    });
  });

  it('should return valid getCartData response with isLoggedIn ', () => {
    const result1 = {
      req: {
        header: {
          recalculate: true,
        },
      },
      body: {
        test: 'Test',
        orderDetails: 'test',
        coupons: {
          offers: [
            {
              offer1: 'offer1',
            },
          ],
        },
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result1));
    getCartData({ isLoggedIn: true }).then((data) => {
      expect(data).toBeDefined();
    });
  });

  it('should return valid getCartData response for isCheckoutFlow', () => {
    const result = {
      req: {
        header: {
          recalculate: true,
        },
      },
      body: {
        test: 'Test',
        coupons: ['abc'],
        orderDetails: {
          orderDetailsResponse: 'test',
        },
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getCartData({ isCheckoutFlow: true }).then((data) => {
      expect(data).toBeDefined();
    });
  });
});

describe('#getProductSkuInfoByUnbxd', () => {
  it('should return valid getProductSkuInfoByUnbxd  response', () => {
    const result = {
      body: {
        response: {
          products: 'foo',
        },
      },
    };
    executeUnbxdAPICall.mockImplementation(() => Promise.resolve(result));
    getProductSkuInfoByUnbxd({}).then((data) => {
      expect(data).toBeDefined();
    });
  });

  it('Should throw errors in case of server side error getProductSkuInfoByUnbxd', () => {
    const result = {
      body: {
        errors: [
          {
            errorMessage: 'Test Error Message getProductSkuInfoByUnbxd',
          },
        ],
      },
    };
    executeUnbxdAPICall.mockImplementation(() => Promise.resolve(result));
    getProductSkuInfoByUnbxd().then((data) => {
      expect(data).toEqual('Test Error Message getProductSkuInfoByUnbxd');
    });
  });
});

describe('#removeItem', () => {
  it('should return valid removeItem response with getUnavailableOOSItems', () => {
    const result = 'foo';
    const input = {
      getUnavailableOOSItems: ['1', '2', '3'],
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    removeItem(input).then((data) => {
      expect(data).toBe(result);
    });
  });

  it('should return valid removeItem response with string value', () => {
    const result = 'foo';
    const input = 'abc';
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    removeItem(input).then((data) => {
      expect(data).toBe(result);
    });
  });

  it('should return valid removeItem response with array value', () => {
    const result = 'foo';
    const input = ['10'];
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    removeItem(input).then((data) => {
      expect(data).toBe(result);
    });
  });

  it('should return valid removeItem response with valid response', () => {
    const result = {
      body: {
        orderId: 1,
      },
    };
    const input = ['10'];
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    removeItem(input).then((data) => {
      expect(data).toBeDefined();
    });
  });
});

describe('#updateItem', () => {
  const result = 'foo';
  const payloadData = {
    callback: () => {},
    updateActionType: '',
    quantity: 10,
    multiPackCount: '2',
    multipackProduct: [
      {
        v_tcpsize: '2_10',
        v_item_catentry_id: 3845,
      },
    ],
    size: '10',
  };
  it('should return valid updateItem response', () => {
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    updateItem(payloadData).then((data) => {
      expect(data).toBeDefined();
    });
  });

  it('should return valid updateItem response without multipackproduct', () => {
    payloadData.multipackProduct = null;
    payloadData.callback = null;
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    updateItem(payloadData).then((data) => {
      expect(data).toBeDefined();
    });
  });

  it('should call pickUpItemUpdatePayload from updateItem', () => {
    payloadData.updateActionType = 'UpdatePickUpItem';
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    updateItem(payloadData).then((data) => {
      expect(data).toBeDefined();
    });
  });

  it('Should throw errors in case of server side error updateItem', () => {
    const resultError = {
      body: {
        errors: [
          {
            errorMessage: 'Test Error Message updateItem',
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(resultError));
    updateItem(payloadData).then((data) => {
      expect(data).toEqual('Test Error Message updateItem');
    });
  });
});

describe('#getUnqualifiedItems', () => {
  it('should return valid getCartData response', () => {
    const result = {
      body: {
        orderItemList: [
          {
            orderItemId: '2',
            isArticleOOSCA: true,
            isArticleOOSUS: true,
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getUnqualifiedItems({}).then((data) => {
      expect(data).toBe(result);
    });
  });

  it('Should throw errors in case of server side error getUnqualifiedItems', () => {
    const result = {
      body: {
        errors: [
          {
            errorMessage: 'Test Error Message getUnqualifiedItems',
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getUnqualifiedItems().then((data) => {
      expect(data).toEqual('Test Error Message getUnqualifiedItems');
    });
  });

  it('should return valid getCartData response when result is undefined', () => {
    const result = {
      body: {
        orderItemList: [
          {
            orderItemId: '2',
            isArticleOOSCA: true,
            isArticleOOSUS: true,
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(undefined));
    getUnqualifiedItems({}).then((data) => {
      expect(data).toBe(result);
    });
  });
});

describe('#toTimeString', () => {
  it('should not be undefined', () => {
    const date = new Date('2020-04-03 15:03:44');
    expect(toTimeString(date)).toBeDefined();
  });

  it('should not be defined for am time', () => {
    const date = new Date('2020-04-03 12:00:00');
    expect(toTimeString(date)).toBeDefined();
  });

  it('should return midnight', () => {
    const date = new Date('2020-04-03 23:59:00');
    expect(toTimeString(date)).toEqual('Midnight');
  });
});

describe('#capitalize', () => {
  it('should capitalize', () => {
    expect(capitalize('abc')).toBe('Abc');
  });
});

describe('#getProductInfoForTranslationData', () => {
  it('should return valid getProductInfoForTranslationData response', () => {
    const result = {
      body: {
        response: {
          products: 'foo',
        },
      },
    };
    executeUnbxdAPICall.mockImplementation(() => Promise.resolve(result));
    getProductInfoForTranslationData({}).then((data) => {
      expect(data).toBeDefined();
    });
  });
});
