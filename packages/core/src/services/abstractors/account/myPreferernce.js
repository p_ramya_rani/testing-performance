// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils/utils';
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';

export const myPreferenceModalSubmit = (mobileNumber, formData, brand) => {
  const apiConfig = getAPIConfig();
  let bodyData;

  switch (brand) {
    case 'gymboree':
      bodyData = {
        CustomerPreferencesGym: formData,
        gymSmsPhone: mobileNumber,
      };
      break;
    case 'snj':
      bodyData = {
        CustomerPreferencesSnj: formData,
        snjSmsPhone: mobileNumber,
      };
      break;
    default:
      bodyData = { CustomerPreferences: formData, smsPhone: mobileNumber };
  }

  const payload = {
    body: bodyData,
    header: {
      'X-Cookie': apiConfig.cookie,
      storeId: apiConfig.storeId,
    },
    webService: endpoints.setContactPreferences,
  };
  return executeStatefulAPICall(payload)
    .then(res => {
      return res.body;
    })
    .catch(err => {
      throw err;
    });
};

export const getUserSubscriptionPreference = () => {
  const apiConfig = getAPIConfig();
  const payload = {
    header: {
      'X-Cookie': apiConfig.cookie,
      storeId: apiConfig.storeId,
    },
    webService: endpoints.getContactPreferences,
  };
  return executeStatefulAPICall(payload)
    .then(res => {
      return res.body;
    })
    .catch(err => {
      throw err;
    });
};
