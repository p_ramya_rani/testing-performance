// 9fbef606107a605d69c0edbcd8029e5d
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';

const getFormattedError = (err) => {
  if (err.response && err.response.body && err.response.body.errors instanceof Array) {
    return err.response.body.errors[0].errorKey;
  }
  return 'genericError';
};

export const errorHandler = (err) => {
  throw getFormattedError(err);
};

const getWebService = () => {
  let apiService = {};

  const { phoneLookupAPI } = endpoints;

  apiService = phoneLookupAPI;

  return apiService;
};

export const phoneLookup = (params) => {
  const { logonId } = params;
  const payload = {
    webService: getWebService(),
    body: {
      emailAddress: logonId,
    },
  };

  return executeStatefulAPICall(payload)
    .then((res) => {
      if (res && res.body && res.body.errorMessage) {
        throw res.body;
      }
      return res.body;
    })
    .catch((err) => {
      throw err;
    });
};

export default phoneLookup;
