// 9fbef606107a605d69c0edbcd8029e5d 
import { getPayloadCookieArray, getFilteredCookies, getMobileCookies } from '../NavigateXHR';

describe('#navigateXHR', () => {
  it('should get empty array', () => {
    const arrCookie = [];
    expect(getPayloadCookieArray()).toMatchObject(arrCookie);
  });

  it('should get only the filtered cookies array', () => {
    const cookies =
      'tcp_email_signup_modal_persistent=true; WC_USERACTIVITY_-123=activity; tcp_sms_signup_modal_persistent=true; _mibhv=anon-1581917052435-3083563188_6917; s_ecid=MCMID%7C23026514002511302200937573498077170630; BVBRANDID=df1aadd8-7d26-4f42-8a2e-121df4bcfce4; _caid=399bf591-0368-402d-b136-931e4b6a2496; unbxd.userId=uid-1584000795337-38023; _fbp=fb.1.1584000795541.1945365033; candid_userid=7e500389-40c9-402f-a788-250a23f934af; QuantumMetricUserID=7cafe9ec06b0afae6cf7b7b28ab1fb24; _abck=898F3181D56E961237B5E4FDAEE63D50~0~YAAQvtocuH183aZwAQAA8Amz0gMo3b5lrbNcn97Z8Mz1cwPb3BabCvLVN+QD55IYIRgen66+mwNPsUqFMnRL1HILtcPmLmh5F2s8oLnpacevxiBD1ANDn/bDVvqJKpMDeNTJOdTqUqxBxxQ0tX+yKZcZrC7liYoVXKPNveJmWa9524DKp+eBCFXvglZ7HdCHCZDXYlPH1EaSD4teMfymPnB+bjsXFM2CP2AWqzKjypczor5uGEHjjzoG95Fks7J0MdSSdu/vUMcx6agdz93Pi6mg78pr+JvorWkl1JfcYsdy/PpLtYseFqICau8m5bcMyh9ucUmOv8em+If0GxEK~-1~-1~-1; s_fid=76435CE66FECF963-39770EC266FC823A; tcpGeoLoc=28.60|77.20; s_vi=[CS]v1|2F3F008C0515FB64-40000B8C82E3ED11[CE]; extole_access_token=GCH8S0LGN3E2C55203JV8LC7S3; orderItemsQtyList=1; sflItemsCount_CA=0; myPlaceAcctNbr=""; sflItemsCount_US=0; vibes_tag_person=a871f880-5c92-4f5d-b6a8-63e4a90b24da; vibes_tag_activity=89971445-000b-4248-9efb-0d1ac1d67399; vibes_tag_company=heXwByBd; tcpCountryDetail=IN; tcpState=DL; AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg=1; check=true; LandingBrand=tcp; cartItemsCount=0; ls-brand=TCP; IR_gbd=childrensplace.com; ipe_s=f10552e0-1f23-ef73-b0a8-0f96907c4650; ipe.30858.pageViewedDay=128; iShippingCookie=CA|en_US|CAD|1.0|1.0; WC_ACTIVEPOINTER=-1%2C10152; token=tcprwd; QuantumMetricSessionID=f5a70328e4df780471310a063871013a; AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg=-1712354808%7CMCIDTS%7C18389%7CMCMID%7C23026514002511302200937573498077170630%7CMCAAMLH-1589440506%7C12%7CMCAAMB-1589440506%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1588842906s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C4.3.0; BVBRANDSID=8646daba-1b25-46c3-8c34-63fe1b7d9102; JSESSIONID=0000mFZKO6NbgVOdjm2csZmw1T_:1dmq2gjvv; s_sq=%5B%5BB%5D%5D; s_sess=%20s_cmdl%3D1%3B%20s_cc%3Dtrue%3B; unbxd.visit=repeat; unbxd.visitId=visitId-1588840659175-58069; _cavisit=171ee487919|; ipe.30858.pageViewedCount=3; pv=4; mbox=PC#ad65d7e597a845bd8f50ac05397c1765.31_0#1652085600|session#efb280595c5b402a9a6cd43c8d0feebb#1588842511; s_pers=%20s_tbm180%3D1%7C1604327278751%3B%20v28%3Dgl%253Ahome%2520page%7C1588842458885%3B%20v14%3D1588840801262-Repeat%7C1591432801262%3B; IR_3971=1588840801444%7C0%7C1588840660476%7C%7C; ipe_30858_fov={"numberOfVisits":2,"sessionId":"f10552e0-1f23-ef73-b0a8-0f96907c4650","expiry":"2020-06-05T13:58:05.887Z","lastVisit":"2020-05-07T08:40:03.265Z"}; RT="z=1&dm=childrensplace.com&si=f6c7b8e1-0e0c-41ac-aa26-847deb970d33&ss=k9wip3q5&sl=1&tt=uar&bcn=%2F%2F684d0d3b.akstat.io%2F';
    const filteredCookies = [
      ' WC_USERACTIVITY_-123=activity',
      ' QuantumMetricUserID=7cafe9ec06b0afae6cf7b7b28ab1fb24',
      ' LandingBrand=tcp',
      ' cartItemsCount=0',
      ' iShippingCookie=CA|en_US|CAD|1.0|1.0',
      ' WC_ACTIVEPOINTER=-1%2C10152',
    ];
    expect(getFilteredCookies(cookies.split(';'))).toMatchObject(filteredCookies);
  });

  it('should return web compatible cookies', () => {
    const mobileCookies = {
      JSESSIONID: '0000GAtVLPA_bTEa3WhK1xBB5LL:19g21qutr',
      WC_ACTIVEPOINTER: '-1%2C10151',
      'WC_AUTHENTICATION_-1002': '-1002%2CWqffnmHt8h0uf3tS%2FTDWBVxpYF4%3D',
      WC_GENERIC_ACTIVITYDATA:
        '[13431745249%3Atrue%3Afalse%3A0%3AtHf55WLW3z%2BgJWhwhu9BrdfRzNU%3D][com.ibm.commerce.context.audit.AuditContext|1590675888520-5078][com.ibm.commerce.store.facade.server.context.StoreGeoCodeContext|null%26null%26null%26null%26null%26null][CTXSETNAME|Default][com.ibm.commerce.context.globalization.GlobalizationContext|-1%26USD%26-1%26USD][com.ibm.commerce.catalog.businesscontext.CatalogContext|null%26null%26false%26false%26false][com.ibm.commerce.context.ExternalCartContext|null][com.ibm.commerce.context.base.BaseContext|10151%26-1002%26-1002%26-1][com.ibm.commerce.context.entitlement.EntitlementContext|null%26null%26null%26null%26null%26null%26null][com.ibm.commerce.giftcenter.context.GiftCenterContext|null%26null%26null]',
      'WC_USERACTIVITY_-1002':
        '-1002%2C10151%2C0%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2CK3%2Fnlg0CcIFJYxzQYl6TMiS3HGSZF7OPU%2F8fDc0py5Ke9vk3NSPOaI5NgxtfkF1v%2FttzkPrHEFmaqIRQz5U2vb2m7rJEeWSDqcZH7i0wNgwjVZPswBuavEABO0kKu9WNhpPpOeMuoDx3qgIwoaI%2BRi67DOF90zQuokWjuRP7aeAstqsBTGZIUCqP6FdMKpKV%2B4Kx2AcGF2ox0oRVfFlsog%3D%3D',
      cartItemsCount: '0',
      tcpCountryDetail: 'IN',
      tcpState: 'DL',
    };
    const formattedCookies = [
      'JSESSIONID=0000GAtVLPA_bTEa3WhK1xBB5LL:19g21qutr',
      'WC_ACTIVEPOINTER=-1%2C10151',
      'WC_AUTHENTICATION_-1002=-1002%2CWqffnmHt8h0uf3tS%2FTDWBVxpYF4%3D',
      'WC_GENERIC_ACTIVITYDATA=[13431745249%3Atrue%3Afalse%3A0%3AtHf55WLW3z%2BgJWhwhu9BrdfRzNU%3D][com.ibm.commerce.context.audit.AuditContext|1590675888520-5078][com.ibm.commerce.store.facade.server.context.StoreGeoCodeContext|null%26null%26null%26null%26null%26null][CTXSETNAME|Default][com.ibm.commerce.context.globalization.GlobalizationContext|-1%26USD%26-1%26USD][com.ibm.commerce.catalog.businesscontext.CatalogContext|null%26null%26false%26false%26false][com.ibm.commerce.context.ExternalCartContext|null][com.ibm.commerce.context.base.BaseContext|10151%26-1002%26-1002%26-1][com.ibm.commerce.context.entitlement.EntitlementContext|null%26null%26null%26null%26null%26null%26null][com.ibm.commerce.giftcenter.context.GiftCenterContext|null%26null%26null]',
      'WC_USERACTIVITY_-1002=-1002%2C10151%2C0%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2CK3%2Fnlg0CcIFJYxzQYl6TMiS3HGSZF7OPU%2F8fDc0py5Ke9vk3NSPOaI5NgxtfkF1v%2FttzkPrHEFmaqIRQz5U2vb2m7rJEeWSDqcZH7i0wNgwjVZPswBuavEABO0kKu9WNhpPpOeMuoDx3qgIwoaI%2BRi67DOF90zQuokWjuRP7aeAstqsBTGZIUCqP6FdMKpKV%2B4Kx2AcGF2ox0oRVfFlsog%3D%3D',
      'cartItemsCount=0',
      'tcpCountryDetail=IN',
      'tcpState=DL',
    ];
    expect(getMobileCookies(mobileCookies)).toEqual(formattedCookies);
  });
});
