// 9fbef606107a605d69c0edbcd8029e5d
import mergeAccountEmailAddressConfirmation from '../EmailAddressConfirmation';
import { executeStatefulAPICall } from '../../../handler/handler';

jest.mock('../../../handler/handler', () => ({
  executeStatefulAPICall: jest.fn(),
}));

describe('#MergeAccountEmailAddressConfirmation', () => {
  const payloadArgs = {
    emailAddress: 'test@test.com',
  };

  it('get email confirmation success', () => {
    const result = {
      status: 'SUCCESS',
      timestamp: '2021-09-06T08:56:34.528516',
      data: {
        status: 'Active',
        email: 'test@test.com',
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    mergeAccountEmailAddressConfirmation(payloadArgs).then((data) => {
      expect(data).toMatchObject(result);
    });
  });

  it('Should throw errors in case of server side error', () => {
    const result = {
      body: {
        errors: {
          status: 'ERROR',
          timestamp: '2021-08-27T16:39:23.630167',
          errors: [
            {
              errorCode: 'Internal Server Error',
              errorMessage: 'Error occured while fetching the members details from Brierley',
              errorParameters: null,
              errorType: null,
            },
          ],
          exceptionType: 'RUNTIME_EXCEPTION',
        },
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.reject(result));
    mergeAccountEmailAddressConfirmation(payloadArgs).then((data) => {
      expect(data).toEqual('Error occured while fetching the members details from Brierley');
    });
  });

  it('Should throw errors in case of unable to connect to server', () => {
    const result = {};
    executeStatefulAPICall.mockImplementation(() => Promise.reject(result));
    mergeAccountEmailAddressConfirmation(payloadArgs).then((data) => {
      expect(data).toEqual('Your action could not be completed due to system error');
    });
  });
});
