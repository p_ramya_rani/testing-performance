/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import orderConfig from '@tcp/core/src/config/orderConfig';
import get from 'lodash/get';
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import { API_CONFIG } from '../../config';
import { getFormattedError } from '../../../utils/errorMessage.util';
import { decode } from '../../../utils/base64.utils';
import { extractFloat, sanitizeEntity, getTotalItemsCountInOrder } from '../../../utils/utils';
import {
  getTranslateDateInformation,
  parseStoreHours,
  getBrand,
  getAPIConfig,
  getFormattedDate,
  parseBoolean,
} from '../../../utils';
import {
  orderStatusMapper,
  orderResCancelledTxt,
  orderCancelledTxt,
  orderResCanceledTxt,
} from './ordersList.util';

/**
 * @function getOrderStatus
 * @summary
 * @param {String} status -
 * @return orderStatus
 */
export const getOrderStatus = (status) => {
  let orderStatus = status;
  if (status !== 'USBOSS') {
    orderStatus = orderStatusMapper[status] || status;
  }
  return orderStatus;
};

/**
 * @function getTranslatedDate
 * @summary
 * @param {String} orderdate -
 * @return formatted date
 */
export const getTranslatedDate = (dateStr) => {
  const { language } = getAPIConfig();
  const dateObj = getTranslateDateInformation(dateStr, language);
  return `${dateObj.month} ${dateObj.date}, ${dateObj.year}`;
};

/**
 * @function getPdpUrl
 * @summary This function will return the sanitized pdpUrl and append cookieToken required for safari cross domain pdp issue if product is of different domain
 * @param { Object } item item object
 * @param { string } cookieToken
 */
export const getPdpUrl = (item, cookieToken) => {
  const pdpUrl = sanitizeEntity(item.productURL);
  const brandId = getBrand();
  const isProductBrandOfSameDomain = item.brand
    ? brandId.toUpperCase() === item.brand.toUpperCase()
    : true;

  if (!cookieToken || isProductBrandOfSameDomain) {
    return pdpUrl;
  }

  return pdpUrl.indexOf('?') > -1 ? `${pdpUrl}&ct=${cookieToken}` : `${pdpUrl}?ct=${cookieToken}`;
};
const OrderBeingProcessed = 'order being processed';
const ReturnedInitiatedKey = 'return initiated';
const OrderShippedKey = 'Order Shipped';
const ShippedKey = 'Shipped';
const ReturnedKey = 'order returned';
const RefundedKey = 'refunded';
const InTransitKey = 'In Transit';
export const DeliveredKey = 'Delivered';
const OutForDeliveryKey = 'Out for Delivery';
const OrderPartiallyShippedKey = 'Order Partially Shipped';

const getThumbnailimgPath = (imgURL) => {
  const slashIndex = imgURL.lastIndexOf('/');
  const imagePath = imgURL.substring(slashIndex + 1);
  return imagePath ? `${imagePath.split('_')[0]}/${imagePath}` : '';
};

const formatItemInfo = (item) => ({
  listPrice: extractFloat(item.saleUnitPrice),
  offerPrice: extractFloat(item.paidUnitPrice),
  linePrice:
    extractFloat(item.paidUnitPrice) *
    (parseInt(item.quantityShipped, 10) || parseInt(item.quantity, 10)),
  quantity: parseInt(item.quantity, 10),
  quantityCanceled: parseInt(item.quantityCanceled, 10) || 0,
  quantityShipped: parseInt(item.quantityShipped, 10) || 0,
  quantityReturned: parseInt(item.quantityReturned, 10) || 0,
  quantityRefunded: parseInt(item.quantityRefunded, 10) || 0,
  quantityOOS: 0, // no support from backend
  itemBrand: item.brand ? item.brand : '',
  taxUnitPrice: extractFloat(item.taxUnitPrice),
});

const formatCartItems = (orderDetails, orderCart, cookieToken) => {
  const cart = orderCart || orderDetails.shoppingBag;
  return (
    cart &&
    cart.items &&
    cart.items.map((item) => ({
      productInfo: {
        appeasementApplied: item.appeasementApplied,
        fit: null,
        pdpUrl: getPdpUrl(item, cookieToken),
        name: sanitizeEntity(item.name),
        imagePath: item.imgURL.includes('https') ? item.imgURL : `//${item.imgURL}`,
        thumbnailImgPath: getThumbnailimgPath(item.imgURL),
        upc: item.upc,
        lineNumber: item.lineNumber,
        variantNo: item.variantNo,
        size: item.size,
        color: {
          name: item.color,
          imagePath: item.thumbnail
            ? `${item.thumbnail.split('_')[0]}/${item.thumbnail}_swatch.jpg`
            : '',
        },
      },
      itemInfo: formatItemInfo(item),
      trackingInfo:
        item.shipmentAndStatusInfo.length > 0
          ? item.shipmentAndStatusInfo.map((statusDetail) => {
              const statusInfo = statusDetail;
              if (statusInfo.status === orderResCancelledTxt) {
                statusInfo.status = orderResCanceledTxt;
              }
              return statusInfo;
            })
          : [
              {
                status:
                  orderDetails.orderStatus === orderResCancelledTxt
                    ? orderResCanceledTxt
                    : orderDetails.orderStatus,
              },
            ],
      isShippedItem: parseInt(item.quantity, 10) === parseInt(item.quantityShipped, 10),
    }))
  );
};

const isShippedStatus = (status) => {
  return (
    status === OrderShippedKey ||
    status === OrderPartiallyShippedKey ||
    status === ShippedKey ||
    status === InTransitKey ||
    status === DeliveredKey ||
    status === OrderBeingProcessed ||
    status === OutForDeliveryKey
  );
};

const formatShipments = (shippedItems, orderDetails) => {
  // Group shipped items by tracking number
  // Object used to prevent duplicates
  const shipmentsObj = {};

  shippedItems.forEach((item) => {
    if (
      item.trackingInfo.length === 0 &&
      (orderDetails.orderStatus === OrderShippedKey || orderDetails.orderStatus === ShippedKey)
    ) {
      item.trackingInfo.push({
        trackingNbr: 'N/A',
        trackingUrl: 'N/A',
        shipDate: 'N/A',
        quantity: item.quantity,
      });
    }
    item.trackingInfo.forEach((shipment) => {
      const { status = '' } = shipment || {};
      const statusToMatch = status.toLowerCase().trim();
      if (
        isShippedStatus(status) ||
        (shipment.quantity === item.itemInfo.quantityShipped &&
          statusToMatch !== ReturnedKey &&
          statusToMatch !== RefundedKey)
      ) {
        const key = shipment.trackingNbr;
        const items = (shipmentsObj[key] && shipmentsObj[key].items) || [];

        items.push({
          itemInfo: {
            ...item.itemInfo,
            quantity: shipment.quantity, // We only want to show quantity included in the shipment
            linePrice: item.itemInfo.offerPrice * parseInt(shipment.quantity, 10),
          },
          productInfo: item.productInfo,
          trackingInfo: item.trackingInfo,
        });

        shipmentsObj[key] = {
          trackingNumber: shipment.trackingNbr,
          trackingUrl: sanitizeEntity(shipment.trackingUrl),
          shippedDate: shipment.shipDate,
          status: OrderShippedKey,
          orderStatus: OrderShippedKey.toLowerCase(),
          items,
        };
      }
    });
  });
  return shipmentsObj;
};

// eslint-disable-next-line sonarjs/cognitive-complexity
const getRemainingItems = (orderDetails, cartItems) => {
  let remainingItems = [];
  if (orderDetails.orderType === 'ECOM') {
    const processingItems = cartItems.filter(
      ({ itemInfo }) => itemInfo.quantityCanceled + itemInfo.quantityShipped !== itemInfo.quantity
    );
    if (processingItems.length > 0) {
      const trackingInfo = {};
      processingItems.forEach((item) => {
        if (item.trackingInfo && item.trackingInfo.length > 0) {
          item.trackingInfo
            .filter((v) => {
              const { status = '' } = v || {};
              const statusToMatch = status.toLowerCase().trim();
              return (
                status !== OrderShippedKey &&
                status !== 'Order Partially Shipped' &&
                statusToMatch !== ReturnedKey &&
                statusToMatch !== RefundedKey
              );
            })
            .forEach(({ trackingUrl, trackingNbr: trackingNumber }) => {
              if (!trackingInfo[trackingUrl]) {
                trackingInfo[trackingUrl] = { trackingUrl, trackingNumber, items: [] };
              }
              trackingInfo[trackingUrl].items.push(item);
            });
        } else {
          trackingInfo[item.productInfo.upc] = {
            items: [item],
          };
        }
      });
      const orderStatus =
        (orderDetails.orderStatus || '').toLowerCase() === orderCancelledTxt
          ? orderCancelledTxt
          : 'order received';
      remainingItems = Object.keys(trackingInfo).map((key) => {
        const aTrackingItem = trackingInfo[key];
        return {
          items: aTrackingItem.items.map((item) => {
            // We only want to show quantity of processing items
            const quantity =
              item.itemInfo.quantity -
              (item.itemInfo.quantityCanceled + item.itemInfo.quantityShipped);
            return {
              itemInfo: {
                ...item.itemInfo,
                linePrice: item.itemInfo.offerPrice * parseInt(quantity, 10),
                quantity,
              },
              productInfo: item.productInfo,
              trackingInfo: item.trackingInfo,
            };
          }),
          status: orderStatus,
          orderStatus,
          trackingNumber: aTrackingItem.trackingNumber || null,
          trackingUrl: sanitizeEntity(aTrackingItem.trackingUrl) || null,
        };
      });
    }
  } else {
    const uncanceledItems = cartItems.filter(
      ({ itemInfo }) =>
        itemInfo.quantityCanceled !== itemInfo.quantity &&
        itemInfo.quantityRefunded !== itemInfo.quantity &&
        itemInfo.quantityReturned !== itemInfo.quantity
    );

    if (uncanceledItems.length > 0) {
      remainingItems = [{ items: uncanceledItems }];
    }
  }
  return remainingItems;
};

const formatPurchasedItems = (cartItems) => {
  const uncanceledItems = cartItems.filter(
    ({ itemInfo }) => itemInfo.quantityCanceled !== itemInfo.quantity
  );

  if (uncanceledItems.length > 0) {
    return [{ items: uncanceledItems }];
  }
  return [{ items: [] }];
};

const getCanceledItems = (cartItems) => {
  return cartItems
    .filter(({ itemInfo }) => itemInfo.quantityCanceled)
    .map((item) => ({
      productInfo: { ...item.productInfo },
      itemInfo: {
        ...item.itemInfo,
        linePrice: extractFloat(item.paidUnitPrice) * item.itemInfo.quantityCanceled,
      },
      trackingInfo: item.trackingInfo,
    }));
};

const getReturnedItems = (cartItems) => {
  const shipmentsarray = [];

  cartItems.forEach((item) => {
    const { trackingInfo = [] } = item || {};
    trackingInfo.forEach((shipment) => {
      const { status = '' } = shipment || {};
      const shipmentStatus = status.toLowerCase().trim();
      if (shipmentStatus === ReturnedKey || shipmentStatus === RefundedKey) {
        const items = [];
        items.push({
          itemInfo: {
            ...item.itemInfo,
            quantity: shipment.quantity, // We only want to show quantity included in the shipment
            linePrice: item.itemInfo.offerPrice * parseInt(shipment.quantity, 10),
          },
          productInfo: item.productInfo,
          trackingInfo: item.trackingInfo,
        });
        shipmentsarray.push({
          trackingNumber: shipment.trackingNbr,
          trackingUrl: sanitizeEntity(shipment.trackingUrl),
          shippedDate: shipment.shipDate,
          refundDate: shipment.refundDate,
          refundAmount: shipment.refundAmount,
          status: shipmentStatus,
          orderStatus: shipmentStatus,
          items,
        });
      }
    });
  });
  // Flattening the shipmentsObj by updating the key
  return shipmentsarray;
};

const getReturnedInitiatedItems = (cartItems) => {
  const shipmentsarray = [];
  cartItems.forEach((item) => {
    const { trackingInfo = [] } = item || {};
    trackingInfo.forEach((shipment) => {
      const { status = '', isCSHReturn } = shipment || {};
      const shipmentStatus = status.toLowerCase().trim();
      if (shipmentStatus === ReturnedInitiatedKey && isCSHReturn) {
        const items = [];
        items.push({
          itemInfo: {
            ...item.itemInfo,
            quantity: shipment.quantity, // We only want to show quantity included in the shipment
            linePrice: item.itemInfo.offerPrice * parseInt(shipment.quantity, 10),
          },
          productInfo: item.productInfo,
          trackingInfo: item.trackingInfo,
        });
        shipmentsarray.push({
          status: shipmentStatus,
          orderStatus: shipmentStatus,
          isCSHReturn: shipment.isCSHReturn,
          returnInitiatedDate: shipment.returnInitiatedDate,
          items,
        });
      }
    });
  });
  // Flattening the shipmentsObj by updating the key
  return shipmentsarray;
};

const formatOrderSummary = (orderDetails) => {
  const cartItems = formatCartItems(orderDetails, false) || [];
  // Get all shipped items
  const shippedItems = cartItems.filter((item) => item.trackingInfo !== null || item.isShippedItem);

  const shipmentsObj = formatShipments(shippedItems, orderDetails);

  // Convert shipmentsObj to an array
  const shipments = Object.keys(shipmentsObj).map((trackingNbr) => shipmentsObj[trackingNbr]);
  const remainingItems = getRemainingItems(orderDetails, cartItems);
  const canceledItems = getCanceledItems(cartItems);
  const canceledItemsArray = [];
  canceledItems.forEach((canceledObj) => {
    canceledItemsArray.push(canceledObj);
  });
  const purchasedItems = shipments.concat(remainingItems).filter((items) => {
    const { orderStatus } = items;
    return !orderStatus;
  });
  return {
    canceledItems: [{ items: canceledItemsArray }],
    purchasedItems,
    shippingAddress: orderDetails.orderSummary.shippingAddress,
  };
};

const getExpectedDeliveryDate = (orderSummary) => {
  return orderSummary ? getFormattedDate(orderSummary.estimatedDeliveryDate) : '';
};
/**
 * @function getOrderHistory
 * @summary
 * @param {String, String}
 */
export const getOrderHistory = (
  siteId,
  currentSiteId,
  reqParams = {},
  accountOrdersAPISwitch = false
) => {
  const { siteIds, companyIds } = API_CONFIG;
  const { itemdetails, pageNumber, pageSize } = reqParams;
  const { getAccountOrders, getDetailedOrderHistory } = endpoints;
  const endpoint = accountOrdersAPISwitch ? getAccountOrders : getDetailedOrderHistory;

  const payload = {
    header: {
      fromRest: true,
    },
    webService: endpoint,
  };

  if (siteId !== currentSiteId) {
    payload.header.companyId = siteId === siteIds.us ? companyIds.ca : companyIds.us;
  }

  if (itemdetails) {
    payload.header.itemdetails = itemdetails;
  }

  if (pageNumber) {
    payload.header.pageNumber = pageNumber;
  }
  if (pageSize) {
    payload.header.pageSize = pageSize;
  }

  return executeStatefulAPICall(payload)
    .then((res) => {
      const orders = res.body.getOrderHistoryResponse.domOrderBeans.map((order) => {
        const returnObj = {
          orderDate: getTranslatedDate(order.orderDate),
          orderDateWithoutFormatting: order.orderDate,
          orderNumber: order.orderNumber,
          orderStatus: getOrderStatus(order.orderStatus),
          status: order.orderStatus,
          currencySymbol: order.orderTotal.replace(/[0-9]|\.|,/gi, ''),
          orderTotal: extractFloat(order.orderTotal),
          orderTracking: order.orderTrackingNumber,
          orderTrackingUrl: order.orderTrackingURL,
          isEcomOrder: !orderConfig.NON_ECOM_ORDERS.includes(order.orderType),
          // check for orderType BOSS
          isBOSSOrder: order.orderType === 'USBOSS',
          isBopisOrder: order.orderType === orderConfig.ORDER_ITEM_TYPE.BOPIS,
          isCanadaOrder: ['CAECOM', 'CAROPIS', 'CABOPIS'].includes(order.orderType),
          orderDateApi: order.orderDate,
          imgPath: get(order, 'shoppingBag.items[0].imgURL', ''),
          itemCount: getTotalItemsCountInOrder(get(order, 'shoppingBag.items', [])),
          productName: sanitizeEntity(get(order, 'shoppingBag.items[0].name', '')),
          expectedDeliveryDate: getExpectedDeliveryDate(order.orderSummary),
        };
        if (itemdetails) {
          return {
            ...returnObj,
            shoppingBag: order.shoppingBag,
            orderSummary: formatOrderSummary(order),
            isActiveOrder: order.isActiveOrder,
          };
        }
        return returnObj;
      });

      let ordersList = orders;

      if (!itemdetails) {
        ordersList =
          orders &&
          orders.length > 0 &&
          orders.sort(
            (prev, next) => parseInt(next.orderNumber, 10) - parseInt(prev.orderNumber, 10)
          );
      }
      return {
        totalPages: 1,
        totalRecords:
          res.body.getOrderHistoryResponse && res.body.getOrderHistoryResponse.totalNoOfRecords,
        orders: ordersList,
      };
    })
    .catch((err) => {
      throw getFormattedError(err);
    });
};

/**
 * @function errorHandler function to handle all the server side errors.
 * @param {object} err - error object in case server side data send server side validation errors.
 * @returns {object} error object with appropirate error message
 */
const errorHandler = (err) => {
  if (err && err.errorResponse && err.errorResponse.errorMessage) {
    throw new Error(err.errorResponse.errorMessage);
  }
  throw new Error('genericError');
};

const getReturnedItem = (item) => {
  return item.allowCSHReturn && parseBoolean(item.allowCSHReturn) === false;
};

const getOrdersGrouping = (orderCart, cookieToken) => {
  let groupedItems = [];
  if (orderCart && Array.isArray(orderCart.items)) {
    groupedItems = orderCart.items.map((item) => ({
      productInfo: {
        appeasementApplied: item.appeasementApplied,
        giftItem: item?.giftItem,
        fit: null,
        pdpUrl: getPdpUrl(item, cookieToken),
        name: sanitizeEntity(item.name),
        imagePath: item.imgURL.includes('https') ? item.imgURL : `//${item.imgURL}`,
        thumbnailImgPath: getThumbnailimgPath(item.imgURL),
        upc: item.upc,
        lineNumber: item.lineNumber,
        variantNo: item.variantNo,
        size: item.size,
        color: {
          name: item.color,
          imagePath: item.thumbnail
            ? `${item.thumbnail.split('_')[0]}/${item.thumbnail}_swatch.jpg`
            : '',
        },
      },
      itemInfo: {
        listPrice: extractFloat(item.saleUnitPrice),
        offerPrice: extractFloat(item.paidUnitPrice),
        linePrice:
          extractFloat(item.paidUnitPrice) *
          (parseInt(item.quantityShipped, 10) || parseInt(item.quantity, 10)),
        quantity: parseInt(item.quantity, 10),
        quantityCanceled: parseInt(item.quantityCanceled, 10) || 0,
        quantityShipped: parseInt(item.quantityShipped, 10) || 0,
        quantityReturned: parseInt(item.quantityReturned, 10) || 0,
        quantityOOS: 0, // no support from backend
        itemBrand: item.brand ? item.brand : '',
        taxUnitPrice: extractFloat(item.taxUnitPrice),
      },
      trackingInfo: item.shipmentAndStatusInfo,
      isReturnedItem: getReturnedItem(item),
    }));
  }

  return groupedItems;
};

/**
 * @function getPaymentCards format card Details
 * @param {object} card - error object in case server side data send server side validation errors.
 * @returns {object} error object with appropirate error message
 */
const getPaymentCards = (card) => {
  let endingNumbers = '';
  const { cardType, chargedAmount, afterpay } = card;
  const cType = cardType && cardType.toUpperCase();
  if (cType === 'VENMO' && card.venmoUserId) {
    endingNumbers = card.venmoUserId;
  } else {
    endingNumbers = card.accountNo;
  }
  return {
    endingNumbers,
    cardType,
    chargedAmount,
    id: null, // we dont get, do we need this?
    afterpay,
  };
};

/**
 * @function getOrderInfoByOrderId
 * @summary
 * @param {type} paramName -
 * @return TDB
 */

/* eslint-disable complexity */
// eslint-disable-next-line sonarjs/cognitive-complexity
export const getOrderInfoByOrderId = (updatedPayload) => {
  const { orderId, emailAddress, fromEpsilon, fromCSH, orderLookUpAPISwitch } = updatedPayload;
  const orderDetailsApiEndpoint = endpoints.orderLookUp;
  if (orderLookUpAPISwitch) {
    orderDetailsApiEndpoint.URI = `${endpoints.getAccountOrders.URI}/${orderId}`;
  }
  const payload = {
    header: {
      emailId: fromEpsilon === 'ep_eml' ? decode(emailAddress) : decodeURIComponent(emailAddress),
      fromCSH,
    },
    webService: orderDetailsApiEndpoint,
  };
  if (!orderLookUpAPISwitch) {
    payload.header.orderId = orderId;
  }
  if (!updatedPayload.isGuest) {
    payload.header.fromPage = 'orderHistory';
  }
  return executeStatefulAPICall(payload)
    .then((res) => {
      const giftCardType = 'Gift Card';
      const { orderDetails } = res.body.orderLookupResponse;
      const shippingMethodValue = res.body.orderLookupResponse.orderSummary.shippingMethod;
      const expectedDeliveryDate = res.body.orderLookupResponse.orderSummary.estimatedDeliveryDate;
      const orderShipping = res.body.orderLookupResponse.orderSummary.shippingAddress;
      const orderBillingAddress = res.body.orderLookupResponse.orderSummary.billingAddress;
      const orderBilling = res.body.orderLookupResponse.amountBilled;
      const orderPayment = res.body.orderLookupResponse.paymentSummary.paymentList;
      const orderCart = res.body.orderLookupResponse.shoppingBag;
      const { orderType = '' } = orderDetails;
      const paymentCards = orderPayment.map((card) => getPaymentCards(card));

      const cartItems = formatCartItems(orderDetails, orderCart, updatedPayload.cookieToken) || [];

      // Get all shipped items
      const shippedItems = cartItems.filter(
        (item) => item.trackingInfo !== null || item.isShippedItem
      );

      // Group shipped items by tracking number
      // Object used to prevent duplicates

      const shipmentsObj = formatShipments(shippedItems, orderDetails);

      // Convert shipmentsObj to an array
      const shipments = Object.keys(shipmentsObj).map((trackingNbr) => shipmentsObj[trackingNbr]);

      // For BOPIS orders, remainingItems = uncanceled items
      // For Non-BOPIS orders, remainingItems = unshipped and uncanceled items (i.e. processsing items)
      const remainingItems = getRemainingItems(orderDetails, cartItems);

      const canceledItems = getCanceledItems(cartItems);

      const returnedItems = getReturnedItems(cartItems);
      const returnedInitiatedItems = getReturnedInitiatedItems(cartItems);
      const purchasedItems =
        orderLookUpAPISwitch && orderType === 'ECOM'
          ? formatPurchasedItems(cartItems)
          : shipments.concat(remainingItems);
      const outOfStockItems = []; // cartItems.filter((item) => item.itemInfo.quantityOOS);
      const orderDetailsReturn = {
        orderNumber: orderDetails.orderId,
        shippingMethod: shippingMethodValue,
        estimatedDeliveryDate: expectedDeliveryDate,
        emailAddress: res.body.orderLookupResponse.decryptedEmailId
          ? res.body.orderLookupResponse.decryptedEmailId
          : updatedPayload.emailAddress || '',
        orderDate: orderDetails.orderDate.replace('T', ' '),
        pickUpExpirationDate:
          res.body.orderLookupResponse.orderSummary.requestedDeliveryBy &&
          res.body.orderLookupResponse.orderSummary.requestedDeliveryBy.replace('T', ' '),
        pickedUpDate: (orderDetails.dateShipped || '').replace('T', ' '),
        shippedDate: (orderDetails.dateShipped || '').replace('T', ' '),
        orderStatus: orderDetails.orderStatus ? orderDetails.orderStatus.toLowerCase() : '',
        isCancellable: orderDetails.isCancellable ? orderDetails.isCancellable.toLowerCase() : '',
        encryptedEmailAddress: decodeURIComponent(res.body.orderLookupResponse.encryptedEmailId),
        status:
          orderDetails.orderType === orderConfig.ORDER_ITEM_TYPE.BOSS
            ? orderDetails.orderStatus
            : orderStatusMapper[orderDetails.orderStatus],
        trackingNumber: orderDetails.tracking,
        trackingUrl:
          orderDetails.trackingUrl !== 'N/A' ? sanitizeEntity(orderDetails.trackingUrl) : '#',
        isBopisOrder: orderDetails.orderType === orderConfig.ORDER_ITEM_TYPE.BOPIS,
        isBossOrder: orderDetails.orderType === orderConfig.ORDER_ITEM_TYPE.BOSS,
        orderType: orderDetails.orderType,
        enteredBy: orderDetails.enteredBy,
        bossMaxDate: orderShipping.bossMaxDate || null,
        bossMinDate: orderShipping.bossMinDate || null,
        summary: {
          currencySymbol: orderBilling.subtotal.replace(/[0-9]|\.|,|-/gi, ''),
          totalItems: orderCart.items.length
            ? orderCart.items.map((item) => parseInt(item.quantity, 10)).reduce((a, b) => a + b)
            : 0,
          subTotal: extractFloat(orderBilling.subtotal),
          purchasedItems: parseInt(orderDetails.totalQuantityPurchased, 10),
          shippedItems: parseInt(orderDetails.totalQuantityShipped, 10),
          canceledItems: parseInt(orderDetails.totalQuantityCanceled, 10),
          returnedItems: parseInt(orderDetails.totalQuantityReturned, 10),
          returnedInitiatedItems: parseInt(orderDetails.totalQuantityReturned, 10),
          returnedTotal: extractFloat(orderDetails.totalAmountReturned),
          couponsTotal: extractFloat(orderBilling.discount),
          shippingTotal: extractFloat(orderBilling.shipping),
          totalTax: extractFloat(orderBilling.tax),
          grandTotal: extractFloat(orderBilling.total),
          adjustmentCouponTotal: extractFloat(orderBilling.coupon),
          appeasementTotal: extractFloat(orderBilling.appeasement),
        },
        appliedGiftCards: paymentCards.filter((card) => card.cardType === giftCardType),
        canceledItems,
        returnedItems,
        returnedInitiatedItems,
        purchasedItems,
        purchasedItemsAll:
          orderLookUpAPISwitch && orderType === 'ECOM' ? shipments.concat(remainingItems) : [],
        groupedShoppingBagItems: fromCSH
          ? getOrdersGrouping(orderCart, updatedPayload.cookieToken)
          : [],
        outOfStockItems,
        checkout: {
          shippingAddress:
            orderDetails.orderType === 'ECOM'
              ? {
                  firstName: orderShipping.firstName,
                  lastName: orderShipping.lastName,
                  addressLine1: orderShipping.addressLine1,
                  addressLine2: orderShipping.addressLine2,
                  city: orderShipping.city,
                  state: orderShipping.state,
                  zipCode: orderShipping.zipCode,
                  country: orderShipping.country,
                }
              : null,
          pickUpStore:
            orderDetails.orderType !== 'ECOM'
              ? {
                  // NOTE: WE WILL NEVER SHOW AN ECOM ORDER AND BOPIS ORDER AT THE SAME TIME, SHIPPING IS SHARED IN THE SAME VAR AS PER BACKEND
                  basicInfo: {
                    id: null,
                    storeName: orderShipping.addressLine1.split('|')[0],
                    address: {
                      addressLine1: `${orderShipping.addressLine1.split('|')[1] || ''} ${
                        orderShipping.addressLine2
                      }`,
                      city: orderShipping.city,
                      state: orderShipping.state,
                      zipCode: orderShipping.zipCode,
                    },
                    phone: orderShipping.phone,
                  },
                  distance: null,
                  pickUpPrimary: {
                    firstName: orderShipping.firstName,
                    lastName: orderShipping.lastName,
                    emailAddress: orderShipping.email,
                  },
                  pickUpAlternative:
                    orderShipping.altFirstName && orderShipping.altFirstName.length > 0
                      ? {
                          firstName: orderShipping.altFirstName,
                          lastName: orderShipping.altLastName,
                          emailAddress: orderShipping.altEmail,
                        }
                      : null,
                }
              : null,
          billing: {
            card: paymentCards.find((card) => card.cardType !== giftCardType),
            sameAsShipping: false,
            // Backend returns billing address as an empty object for gift card orders
            billingAddress:
              Object.keys(orderBillingAddress).length > 0
                ? {
                    firstName: orderBillingAddress.firstName || '',
                    lastName: orderBillingAddress.lastName || '',
                    addressLine1: orderBillingAddress.addressLine1 || '',
                    addressLine2: orderBillingAddress.addressLine2 || '',
                    city: orderBillingAddress.city || '',
                    state: orderBillingAddress.state || '',
                    zipCode: orderBillingAddress.zipCode || '',
                    country: orderBillingAddress.country || '',
                  }
                : null,
          },
        },
      };

      // Parse Store Hours
      if (orderShipping.storeHours && orderShipping.storeHours.length) {
        orderDetailsReturn.checkout.pickUpStore.hours = {
          regularHours: parseStoreHours(orderShipping.storeHours),
        };
      }

      const trackingNumber =
        res && res.body && res.body.orderLookupResponse
          ? res.body.orderLookupResponse.orderDetails.tracking
          : null;
      return {
        trackOrderInfo: {
          success: true,
          trackingNumber: trackingNumber === 'N/A' ? null : trackingNumber,
          orderId: res.body.orderLookupResponse.orderDetails.orderId,
          encryptedEmailAddress: encodeURIComponent(res.body.orderLookupResponse.encryptedEmailId),
          pointsEarned: res.body.orderLookupResponse.pointsEarned,
        },
        orderDetailsReturn,
      };
    })
    .catch(errorHandler);
};

export default { getOrderHistory };
