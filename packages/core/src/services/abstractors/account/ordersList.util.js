// 9fbef606107a605d69c0edbcd8029e5d
export const orderReceivedLbl = 'lbl_orders_statusOrderReceived';
export const orderCancelledLbl = 'lbl_orders_statusOrderCancelled';
export const orderCancelledTxt = 'cancelled';
export const orderResCancelledTxt = 'Cancelled';
export const orderResCanceledTxt = 'Canceled';
export const orderProcessingLbl = 'lbl_orders_processing';

export const orderStatusMapper = {
  'Pending payment approval': orderReceivedLbl,
  'Order In Process': 'lbl_order_being_processed',
  'Order Expired': 'lbl_orders_statusOrderExpired',
  'Order Canceled': orderCancelledLbl,
  'Order Cancelled': orderCancelledLbl,
  'Order Shipped': 'lbl_orders_statusOrderShipped',
  Shipped: 'lbl_orders_shipped',
  'Partially Shipped': 'lbl_orders_statusOrderPartiallyShipped',
  'Order Partially Shipped': 'lbl_orders_statusOrderPartiallyShipped',
  'Order Received': orderReceivedLbl,
  'Ready for Pickup': 'lbl_orders_statusItemsReadyForPickup',
  'Reservation Received': orderReceivedLbl,
  'Picked Up': 'lbl_orders_statusItemsPickedUp',
  'Order Picked Up': 'lbl_orders_statusItemsPickedUp',
  Confirmed: orderReceivedLbl,
  Expired: 'lbl_orders_statusOrderExpired',
  Completed: orderReceivedLbl,
  Canceled: orderCancelledLbl,
  Cancelled: orderCancelledLbl,
  'N/A': 'lbl_orders_statusNa',
  'Please contact our Customer Service': 'lbl_orders_statusUserCallNeeded',
  Processing: 'lbl_order_being_processed',
  'Order Being Processed': 'lbl_order_being_processed',
  Delivered: 'lbl_order_delivered',
  'Partially Delivered': 'lbl_partially_delivered',
};
