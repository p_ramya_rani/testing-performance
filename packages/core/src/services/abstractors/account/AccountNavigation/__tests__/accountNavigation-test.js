// 9fbef606107a605d69c0edbcd8029e5d 
import AccountNavigationAbstractor from '../AccountNavigation';
import mock from '../mock';

jest.mock('../../../../handler/handler');

it('Account Navigation Abstractor | ', () => {
  AccountNavigationAbstractor.getData('AccountNavigation', {
    brand: 'TCP',
    country: 'USA',
    channel: 'Desktop',
  }).then(data => {
    expect(data).toMatchObject(AccountNavigationAbstractor.processData(mock));
  });
});
