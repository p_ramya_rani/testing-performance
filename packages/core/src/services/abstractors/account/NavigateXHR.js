// 9fbef606107a605d69c0edbcd8029e5d
import endpoints from '../../endpoints';
import { getAllCookies } from '../../../utils/cookie.util';
import {
  getAPIConfig,
  isMobileApp,
  getBrand,
  isSafariBrowser,
  isClient,
  isAndroid,
  removeURLParameter,
} from '../../../utils';

const BRAND_TO_UPDATE_COOKIE = { GYM: 'TCP', TCP: 'GYM' };

const ALLOWED_COOKIES = [
  'WC_ACTIVEPOINTER',
  'WC_AUTHENTICATION_',
  'WC_GENERIC_ACTIVITYDATA',
  'WC_PERSISTENT',
  'WC_SESSION_ESTABLISHED',
  'WC_USERACTIVITY_',
  'cartItemsCount',
  'iShippingCookie',
  'tcp_isregistered',
  'TCP_INTLPRC',
  'LandingBrand',
  'QuantumMetricUserID',
  'orderSession',
  'irclickid',
  'tcpSessionInfo',
];

export const getFilteredCookies = (cookies) => {
  return (cookies || []).filter((cookie) => {
    return ALLOWED_COOKIES.find((allowedCookieName) => {
      const name = (cookie && cookie.split('=')[0]).trim() || '';
      return allowedCookieName === name || name.startsWith(allowedCookieName);
    });
  });
};

const formattedCookiesLikeWeb = (mobileCookie) => {
  return (mobileCookie || []).map((cookie) => `${cookie[0]}=${cookie[1]}`);
};

export const getMobileCookies = (cookie) => {
  const mobileCookie = Object.entries(cookie);
  return formattedCookiesLikeWeb(mobileCookie);
};

export const getPayloadCookieArray = (cookie) => {
  const checkMobileApp = isMobileApp();
  const informationCookies =
    checkMobileApp && cookie ? getMobileCookies(cookie) : getAllCookies().split(';');
  const filteredCookies = getFilteredCookies(informationCookies);
  return filteredCookies.map((str) => {
    const obj = Object.create(null);
    obj.name = str && str.split('=')[0].trim();
    obj.path = '/';
    obj.value = str && str.split('=')[1].trim();

    return obj;
  });
};

const applicationJSON = 'application/json';

export const NavigateXHR = async (cookie, headers = {}, brandName = '', isSNJEnabled) => {
  const { crossDomain, domain } = getAPIConfig();
  const modifiedHeaders = { ...headers };
  const endPointNavigatePost = endpoints.postNavigateService.URI;
  let webServiceEndPoints = { ...endPointNavigatePost };
  webServiceEndPoints.URI = `${domain}${endPointNavigatePost}`;
  const brandId = getBrand();
  const brand = (brandName || brandId).toUpperCase();
  if (isSNJEnabled) {
    modifiedHeaders.brandIdToUpdateCookie = brand;
  } else {
    const isSafari = !isMobileApp() && isSafariBrowser();
    const endPointNavigateXHR = endpoints.navigateXHRService.URI;
    if (isSafari) {
      modifiedHeaders.brandIdToUpdateCookie = BRAND_TO_UPDATE_COOKIE[brand];
    } else {
      webServiceEndPoints = { ...endpoints.navigateXHRService };
      webServiceEndPoints.URI = `${crossDomain}/api/${endPointNavigateXHR}`;
    }
  }

  const userAgent =
    isMobileApp() && isAndroid()
      ? {
          'User-Agent': `tcpapp/My Place/18.0.0/Android deviceType/MobileApp`,
          'x-tcp-app-rwd': true,
          'tcp-app-type': 'mobile-app',
        }
      : {};

  const response = await fetch(webServiceEndPoints.URI, {
    method: 'post',
    headers: {
      'Content-Type': applicationJSON,
      Accept: applicationJSON,
      ...modifiedHeaders,
      ...userAgent,
    },
    credentials: 'include',
    body: JSON.stringify({ cookie: getPayloadCookieArray(cookie) }),
  });
  return response && response.json();
};

export const getNavigateXHR = ({ ct, isSNJEnabled }) => {
  // cookie-token-from-cross-domain
  const { domain } = getAPIConfig();
  const endPointNavigatePost = endpoints.postNavigateService.URI;
  const webServiceEndPoints = Object.assign({}, endpoints.getNavigateService);
  webServiceEndPoints.URI = `${domain}${endPointNavigatePost}`;
  const headers = {
    'Content-Type': applicationJSON,
    Accept: applicationJSON,
  };

  if (ct === 'logout') {
    headers.actionTaken = 'logout';
  }
  if (isSNJEnabled && isClient()) {
    const url = removeURLParameter(window?.location?.href, 'ct');
    window.history.replaceState(null, '', url);
  }
  return fetch(webServiceEndPoints.URI, {
    method: 'put',
    headers,
    credentials: 'include',
    body: JSON.stringify({ encodedCookie: ct }),
  })
    .then((response) => {
      return response;
    })
    .catch((err) => {
      throw err;
    });
};

export default { getPayloadCookieArray, NavigateXHR, getNavigateXHR };
