// 9fbef606107a605d69c0edbcd8029e5d
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';

export const LogoutApplication = (isSNJEnabled = false) => {
  const { logout, logoutUser } = endpoints;
  const payloadData = {
    webService: isSNJEnabled ? logoutUser : logout,
  };
  return executeStatefulAPICall(payloadData).then(res => {
    if (!res) {
      throw new Error('res body is null');
      // TODO - Set API Helper to filter if error exists in response
    }
    return res;
  });
};

export default { LogoutApplication };
