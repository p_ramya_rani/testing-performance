// 9fbef606107a605d69c0edbcd8029e5d
import { getPlaceSiteId } from '@tcp/core/src/utils';
import USER_CONSTANTS from '@tcp/core/src/components/features/account/User/User.constants';
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import { getAPIConfig } from '../../../utils';

export const createAccountApi = (payload) => {
  const { isSNJEnabled, isApplyPlccCardFlow } = payload;
  const apiConfig = getAPIConfig();
  const { createAccount, createAccountPLCC } = endpoints;
  const payloadData = {
    webService: isApplyPlccCardFlow ? createAccountPLCC : createAccount,
    body: {
      firstName: payload.firstName,
      lastName: payload.lastName,
      logonId: payload.emailAddress,
      logonPassword: payload.password,
      rememberCheck: payload.rememberMe || false,
      rememberMe: payload.rememberMe || false,
      response: payload.emailValidationStatus,
      xCreditCardId: payload.plccCardId || '',
      zipCode: payload.noCountryZip || payload.zipCode,
      catalogId: apiConfig.catalogId,
      langId: apiConfig.langId,
      storeId: apiConfig.storeId,
    },
    header: {},
  };
  if (payload.userId) {
    payloadData.body.userId = payload.userId;
  }

  if (isSNJEnabled) {
    payloadData.header.siteId = getPlaceSiteId();
  }
  payloadData.body.phone1 = payload.phoneNumber;
  payloadData.body.phone = payload.phoneNumber;

  payloadData.header = {
    ...payloadData.header,
    ordersession: payload.ordersession,
  };

  if (isApplyPlccCardFlow) {
    payloadData.body.xCreditCardId = payload.xCreditCardId;
    payloadData.body.isRegistered = payload.isRegistered;
    payloadData.body.isMprIdExist = payload.isMprIdExist;
    payloadData.body.isMprIdExist = payload.isMprIdExist;
    payloadData.body.coupon = payload.coupon;
    payloadData.body.logonPassword = '';
    payloadData.body.response = payload.emailValidationStatusForApi;
    payloadData.body.userId = USER_CONSTANTS.GENERIC_USER_ID;

    payloadData.header = {
      ...payloadData.header,
      ordersession: true,
    };
  }

  return executeStatefulAPICall(payloadData)
    .then((res) => {
      if (!res) {
        throw new Error('res body is null');
      }
      return res;
    })
    .catch((err) => {
      throw err;
    });
};

export default { createAccountApi };
