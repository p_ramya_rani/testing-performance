// 9fbef606107a605d69c0edbcd8029e5d
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import { getAPIConfig } from '../../../utils';

export const forgotPassword = (args) => {
  const apiConfig = getAPIConfig();
  const payload = {
    webService: endpoints.requestPassword,
    body: {
      langId: apiConfig.langId,
      catalogId: apiConfig.catalogId,
      storeId: apiConfig.storeId,
      target: args.target || 'email', // this needs to be moved to apiconfig
      ...args,
    },
  };
  return executeStatefulAPICall(payload)
    .then((res) => {
      if (!res.body) {
        throw new Error('res body is null');
        // TODO - Set API Helper to filter if error exists in response
      }
      return res.body.contact || [];
    })
    .catch((err) => {
      throw err;
    });
};

export default {
  forgotPassword,
};
