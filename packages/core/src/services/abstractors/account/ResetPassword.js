// 9fbef606107a605d69c0edbcd8029e5d
import queryString from 'query-string';
import { isClient, getPlaceSiteId } from '@tcp/core/src/utils';
import { executeStatefulAPICall } from '../../handler';
import { getAPIConfig } from '../../../utils';
import endpoints from '../../endpoints';

const getFormattedError = err => {
  if (err.response && err.response.body && err.response.body.errors instanceof Array) {
    return err.response.body.errors[0].errorKey;
  }
  return 'genericError';
};

export const errorHandler = err => {
  throw getFormattedError(err);
};

const getWebService = (isSNJEnabled, passwordValidationCode) => {
  let apiService = {};

  const { requestPassword, requestPasswordPost, requestPasswordMS } = endpoints;
  if (passwordValidationCode) {
    if (isSNJEnabled) {
      apiService = requestPasswordMS;
    } else {
      apiService = requestPasswordPost;
    }
  } else {
    apiService = requestPassword;
  }

  return apiService;
};

export const resetPassword = ({
  newPassword,
  logonPasswordVerify,
  logonPasswordOld,
  em,
  passwordValidationCode,
  logonId,
  recaptchaToken,
  isSNJEnabled = false,
}) => {
  const apiConfig = getAPIConfig();
  let parsedLogonPasswordOld = decodeURIComponent((logonPasswordOld || '').replace(/ /gi, '+'));
  let parsedEm = decodeURIComponent((em || '').trim());

  if (isClient()) {
    const queryObject = queryString.parse(window.location.search);
    parsedLogonPasswordOld = (queryObject.logonPasswordOld || '').replace(/ /gi, '+');
    parsedEm = (queryObject.em || '').trim();
  }

  const payload = {
    webService: getWebService(isSNJEnabled, passwordValidationCode),
    body: {
      storeId: apiConfig.storeId,
      catalogId: apiConfig.catalogId,
      langId: apiConfig.langId,
      logonPassword: newPassword,
      logonPasswordVerify,
      logonPasswordOld: parsedLogonPasswordOld,
      logonId: logonId || parsedEm,
      logonIdInput: parsedEm,
      stat: 'passwdconfirm',
      URL: 'ResetPasswordForm',
      formFlag: 'false', // if this is true the user will get an email
      errorViewName: 'ResetPasswordGuestErrorView',
      checkEmailAddress: '-',
      reLogonURL: 'ChangePassword',
      Relogon: 'Update',
      fromOrderId: '*',
      toOrderId: '.',
      deleteIfEmpty: '*',
      continue: '1',
      createIfEmpty: '1',
      calculationUsageId: '-1',
      updatePrices: '0',
      myAcctMain: '1',
      isPasswordReset: 'false',
    },
  };

  if (recaptchaToken) {
    payload.body.recaptchaToken = recaptchaToken;
  }
  if (passwordValidationCode) {
    payload.body.passwordValidationCode = passwordValidationCode;
  }

  if (isSNJEnabled) {
    payload.header = {
      siteId: getPlaceSiteId(),
    };
  }

  return executeStatefulAPICall(payload)
    .then(res => {
      if (res && res.body && res.body.errorMessage) {
        throw res.body;
      }
      return 'successMessage';
    })
    .catch(err => {
      throw err;
    });
};

export default resetPassword;
