// 9fbef606107a605d69c0edbcd8029e5d
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import EMAIL_CONFIRMATION_CONSTANTS from '../../../components/features/account/CustomerHelp/organisms/EmailConfirmation/EmailConfirmation.constants';

const getFormattedError = (err) => {
  const error = {};
  if (err?.response?.body && err?.response?.body?.errors instanceof Array) {
    error.errorMessage = err.response.body.errors[0].errorMessage;
    return error;
  }
  return error;
};

const mergeAccountEmailAddressConfirmation = (emailAddress) => {
  const payloadData = {
    webService: endpoints.mergeAccountValidateMember,
    body: {
      emailId: emailAddress,
    },
  };

  return executeStatefulAPICall(payloadData)
    .then((res) => {
      if (res?.body?.data?.status === EMAIL_CONFIRMATION_CONSTANTS.VICTIM_ALREADY_MERGED_STATUS) {
        // eslint-disable-next-line no-throw-literal
        throw { errorMessage: EMAIL_CONFIRMATION_CONSTANTS.VICTIM_ALREADY_MERGED_STATUS };
      }
      return res;
    })
    .catch((err) => {
      if (err.errorMessage === EMAIL_CONFIRMATION_CONSTANTS.VICTIM_ALREADY_MERGED_STATUS) throw err;
      throw getFormattedError(err);
    });
};

export default mergeAccountEmailAddressConfirmation;
