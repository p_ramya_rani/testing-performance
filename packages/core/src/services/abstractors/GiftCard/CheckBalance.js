// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import endpoints from '../../endpoints';
import { executeStatefulAPICall } from '../../handler';

const getGiftCardBalance = (giftCardDetails) => {
  const { giftCardNumber, giftCardPin, recaptchaToken } = giftCardDetails;

  const payload = {
    header: {
      storeId: getAPIConfig().storeId,
    },
    webService: endpoints.getGifCardBalance,
    body: {
      'recapture-response': recaptchaToken,
      giftCardNbr: giftCardNumber,
      giftCardPin,
    },
  };

  return executeStatefulAPICall(payload)
    .then((res) => res)
    .catch((err) => {
      logger.info('check gift card balance api failed', err);
      throw err.response;
    });
};

export default getGiftCardBalance;
