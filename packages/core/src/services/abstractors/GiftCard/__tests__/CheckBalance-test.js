// 9fbef606107a605d69c0edbcd8029e5d
import getGiftCardBalance from '../CheckBalance';
import { executeStatefulAPICall } from '../../../handler/handler';

jest.mock('../../../handler/handler', () => ({
  executeStatefulAPICall: jest.fn(),
}));

describe('#getGiftCardBalance', () => {
  const payloadArgs = {
    giftCardPin: '3456',
    giftCardNbr: '1231312312312312447',
    recaptchaToken: 'test',
  };

  it('get gift card balance', () => {
    const result = {
      giftCardAuthorizedAmt: '0',
      giftCardNbr: '***************2624',
      giftCardPin: '****',
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    getGiftCardBalance(payloadArgs).then((data) => {
      expect(data).toMatchObject(result);
    });
  });

  it('Should throw errors in case of server side error', () => {
    const result = {
      body: {
        errors: [
          {
            errorMessage: 'Test Error Message',
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.reject(result));
    getGiftCardBalance(payloadArgs).then((data) => {
      expect(data).toEqual('Test Error Message');
    });
  });

  it('Should throw errors in case of unable to connect to server', () => {
    const result = {};
    executeStatefulAPICall.mockImplementation(() => Promise.reject(result));
    getGiftCardBalance(payloadArgs).then((data) => {
      expect(data).toEqual('Your action could not be completed due to system error');
    });
  });
});
