// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable max-statements */
/* eslint-disable sonarjs/cognitive-complexity */
import logger from '@tcp/core/src/utils/loggerInstance';
import { SEARCH_REDIRECT_KEY, getSearchRedirectObj } from '@tcp/core/src/utils';
import processHelpers from './processHelpers';
import {
  isClient,
  routerPush,
  getSiteId,
  isMobileApp,
  isCanada,
  scrollPage,
  setSessionStorage,
  getSessionStorage,
  configureInternalNavigationFromCMSUrl,
} from '../../../utils';
import { getSeoHeaderCopyFromBreadCrumb } from '../../../components/features/browse/ProductListing/container/ProductListing.util';
import { getCategoryId, parseProductInfo, checkIfBundleProduct } from './productParser';
import { FACETS_FIELD_KEY } from './productListing.utils';
import {
  getProductsFilters,
  getTotalProductsCount,
  getCurrentListingIds,
} from '../../../components/features/browse/ProductListing/container/ProductListing.selectors';

const getAvailableL3List = (facets) => {
  return facets && facets.multilevel && facets.multilevel.bucket;
};
const getAppliedL3Filters = (availableL3List) => {
  return availableL3List && availableL3List.length && (availableL3List[0].values || []);
};
const isUnbxdLogicApplied = (shouldApplyUnbxdLogic, bucketingSeqConfig) => {
  return shouldApplyUnbxdLogic && bucketingSeqConfig.bucketingRequired;
};
const getCurrentListingId = (breadCrumbs) => {
  return breadCrumbs && breadCrumbs.length ? breadCrumbs[breadCrumbs.length - 1].urlPathSuffix : '';
};
const getCurrentListingDescription = (breadCrumbs) => {
  return breadCrumbs && breadCrumbs.length
    ? breadCrumbs[breadCrumbs.length - 1].longDescription
    : '';
};
const getCurrentListingType = (breadCrumbs) => {
  return breadCrumbs && breadCrumbs.length ? breadCrumbs[breadCrumbs.length - 1].displayName : '';
};
const getIsBopisFilterOn = (filters) => {
  return filters && filters.bopisStoreId ? filters.bopisStoreId.length > 0 : false;
};
const getOutfitStyliticsTag = (isOutfitPage, searchTerm) => {
  return isOutfitPage ? searchTerm : null;
};

const isDepartmentPage = (isSearch, breadCrumbs) => {
  return !isSearch && (!breadCrumbs || breadCrumbs.length === 1);
};
const isCategoryType = (breadCrumbs) => {
  return breadCrumbs && breadCrumbs.length ? breadCrumbs[breadCrumbs.length - 1].displayName : '';
};
const getL1Category = (breadCrumbs) => {
  return breadCrumbs && breadCrumbs.length ? breadCrumbs[0].displayName : '';
};
const getFiltersAfterProcessing = (
  facetsRes,
  numberOfProducts,
  getFacetSwatchImgPath,
  filtersAndSort,
  l1category,
  toggleSearchSizeFilter
) => {
  let filters = {};
  // Construct facets from the api response
  const facetsList = facetsRes && facetsRes.text && facetsRes.text.list && facetsRes.text.list;
  if (facetsList) {
    const facets = processHelpers.getFacetsAPIData(
      facetsList,
      getFacetSwatchImgPath,
      numberOfProducts,
      filtersAndSort,
      toggleSearchSizeFilter
    );
    const unbxdDisplayName = processHelpers.getUnbxdDisplayName(facetsList);
    filters = {
      ...facets,
      unbxdDisplayName, // Key Value object added for Facets DisplayName and FacetName mapping in UI components
      l1category,
    };
  }
  return filters;
};
const getQueryString = (keyValuePairs = {}) => {
  const params = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const key of Object.keys(keyValuePairs)) {
    if (keyValuePairs[key] === null) {
      params.push(encodeURIComponent(key));
    } else {
      // eslint-disable-next-line no-lonely-if
      if (Array.isArray(keyValuePairs[key])) {
        // eslint-disable-next-line no-restricted-syntax
        for (const value of keyValuePairs[key]) {
          params.push([encodeURIComponent(key), '[]=', encodeURIComponent(value)].join(''));
        }
      } else {
        params.push(
          [encodeURIComponent(key), '=', encodeURIComponent(keyValuePairs[key])].join('')
        );
      }
    }
  }
  return params.join('&');
};
// eslint-disable-next-line sonarjs/cognitive-complexity
const getPlpUrlQueryValues = (filtersAndSort, location) => {
  // NOTE: these are parameters on query string we don't handle (nor we need to)
  // just pass them to the abstractor
  let urlQueryValues = {};
  let routeURL = '?';

  if (filtersAndSort) {
    const { sort } = filtersAndSort;

    Object.keys(filtersAndSort).forEach((key) => {
      if (filtersAndSort[key].length > 0) {
        if (key.toLowerCase() === FACETS_FIELD_KEY.sort) {
          // this also covers the fake sort describing the default server sort (which we give a falsy value like 0)
          urlQueryValues.sort = sort;
        } else {
          urlQueryValues[key] = filtersAndSort[key].join(',');
        }
      }
    });

    Object.keys(filtersAndSort).forEach((key) => {
      if (
        (key.toLowerCase() === FACETS_FIELD_KEY.sort &&
          urlQueryValues.sort &&
          filtersAndSort.sort === '') ||
        (urlQueryValues[key] && filtersAndSort[key].length < 1)
      ) {
        // If sort has no value or recommended then no need to pass key in url and api
        delete urlQueryValues[key];
      }
    });
  }

  urlQueryValues = getQueryString(urlQueryValues);

  let displayPath = typeof window === 'undefined' ? location.pathname : window.location.pathname;
  const searchName = typeof window === 'undefined' ? location.search || '' : window.location.search;
  displayPath = `${displayPath}${searchName}`;
  const country = getSiteId();
  let urlPath = displayPath.replace(`/${country}`, '');
  urlPath = urlPath.split('?');
  urlPath = [...urlPath].shift();

  // TODO- To get query from getInitialProps.
  let urlPathCID = urlPath.split('/');
  urlPathCID = urlPathCID[urlPathCID.length - 1];
  urlPathCID = urlPathCID.split('?');
  urlPathCID = [...urlPathCID].shift();

  routeURL = `${routeURL}${urlQueryValues}`;

  routeURL = `${urlPath}${routeURL}`;

  routeURL = urlQueryValues === '' ? routeURL.substring(0, routeURL.length - 1) : routeURL;
  if (routeURL.includes('search')) {
    routerPush(`/search?searchQuery=${urlPathCID}`, routeURL, { shallow: true });
  } else if (routeURL.includes('/c/')) {
    routerPush(`/c?cid=${urlPathCID}`, routeURL, { shallow: true });
  }
  return routeURL;
};

function isFiltered(filtersAndSort) {
  let isFilterApplied = false;
  if (filtersAndSort) {
    Object.keys(filtersAndSort).forEach((key) => {
      if (filtersAndSort[key].length > 0 && key.toLowerCase() !== FACETS_FIELD_KEY.sort) {
        isFilterApplied = true;
      }
    });
  }
  return isFilterApplied;
}

function getProductCategory(res) {
  let productCategory = '';
  const products =
    res && res.response && res.response.products && res.response.products.length
      ? res.response.products
      : [];

  if (
    products.length &&
    products[0].categoryPath2_catMap &&
    products[0].categoryPath2_catMap.join().toLowerCase().includes('accessories')
  ) {
    productCategory = 'accessories';
  } else if (
    products.length &&
    products[0].categoryPath2_catMap &&
    products[0].categoryPath2_catMap.join().toLowerCase().includes('shoes')
  ) {
    productCategory = 'shoes';
  } else {
    productCategory = 'apparel';
  }
  return productCategory;
}

// eslint-disable-next-line complexity
const processResponse = (
  res,
  state,
  {
    isSearch,
    breadCrumbs,
    shouldApplyUnbxdLogic,
    getFacetSwatchImgPath,
    filtersAndSort,
    bucketingSeqConfig,
    getImgPath,
    excludeBadge,
    hasShortImage,
    isOutfitPage,
    searchTerm,
    sort,
    filterSortView,
    location,
    filterMaps = {},
    isLazyLoading,
    navigation,
    categoryFilterArray,
    isPrefetched,
    isRecommendationView,
    categoryNameList,
    brand,
  }
) => {
  const scrollPointInStorage = getSessionStorage('SCROLL_POINT');
  const scrollPoint = isClient() ? scrollPointInStorage && parseInt(scrollPointInStorage, 10) : 0;
  let updatedUrl = '';
  let modifiedFiltersAndSort = filtersAndSort;
  if (scrollPoint) {
    setSessionStorage({ key: 'SCROLL_EVENT', value: 1 });
  }
  if (
    isClient() &&
    scrollPoint &&
    getSessionStorage('pdp_back') === 'true' &&
    getSessionStorage('LAST_PAGE_PATH') === window.location.pathname
  ) {
    scrollPage(0, scrollPoint);
    setSessionStorage({ key: 'SCROLL_POINT', value: 0 });
    setSessionStorage({ key: 'pdp_back', value: false });
  }
  // if (this.apiHelper.responseContainsErrors(res)) {
  //  TODO - error handling throw new ServiceResponseError(res);
  // }
  const productCategory = getProductCategory(res.body);
  if (isClient() && window.location && window.location.href.includes('search')) {
    const searchRedirect = getSearchRedirectObj();
    if (searchRedirect && searchRedirect.redirectUrl !== window.location.href) {
      setSessionStorage({
        key: SEARCH_REDIRECT_KEY,
        value: JSON.stringify({ isDone: false, pc: productCategory }),
      });
    } else {
      setSessionStorage({
        key: SEARCH_REDIRECT_KEY,
        value: JSON.stringify({ ...searchRedirect, pc: productCategory }),
      });
    }
  }
  if (isClient() && res.body.redirect && typeof window !== 'undefined') {
    let redirectUrl = res.body.redirect.value;
    if (redirectUrl)
      setSessionStorage({
        key: SEARCH_REDIRECT_KEY,
        value: JSON.stringify({ isDone: true, searchTerm, redirectUrl, pc: productCategory }),
      });
    try {
      // If domain matches try routing within the site else page reload is fine
      if (redirectUrl.length && redirectUrl.indexOf(window.location.hostname) > -1) {
        redirectUrl = redirectUrl.replace(window.location.origin, '');
        redirectUrl = redirectUrl.replace(/\/(us|ca)/, '');
        if (redirectUrl.indexOf('content') > -1) {
          const cid = redirectUrl.replace('/content/', '');
          routerPush(`/content?contentType=${cid}`, redirectUrl); // try and avoid a hard reload
        } else {
          routerPush(configureInternalNavigationFromCMSUrl(redirectUrl, true), redirectUrl, {
            shallow: true,
          });
        }
      } else {
        window.location.assign(redirectUrl);
      }
    } catch (e) {
      logger.error(e);
    }
  }

  if (!isMobileApp() && isClient() && filterSortView && !isLazyLoading && !isPrefetched) {
    updatedUrl = getPlpUrlQueryValues(filtersAndSort, location);
  }

  if (isMobileApp()) {
    modifiedFiltersAndSort = processHelpers.getDecodedData(filtersAndSort);
  }

  const pendingPromises = [];
  // flags if we are oin an L1 plp. Such plp's have no products, and only show espots and recommendations.
  const isDepartment = isDepartmentPage(isSearch, breadCrumbs);
  const attributesNames = processHelpers.getProductAttributes();
  const categoryType = isCategoryType(breadCrumbs);
  const l1category = getL1Category(breadCrumbs);
  let totalProductsCount = 0;
  let productListingCurrentNavIds;
  // We will get the avaialable l3 list in L2 page call in bucekting scenario.
  const availableL3List = getAvailableL3List(res.body.facets);
  const availableL3InFilter = getAppliedL3Filters(availableL3List);

  const displayToCustomerCategories =
    (categoryNameList &&
      categoryNameList[1] &&
      categoryNameList[1].subCategories &&
      categoryNameList[1].subCategories
        .filter((cat) => cat.displayToCustomer === false)
        .map(({ categoryId }) => categoryId)) ||
    [];

  totalProductsCount =
    (availableL3InFilter &&
      availableL3InFilter.reduce(
        (sum, item) =>
          (item && displayToCustomerCategories.indexOf(item.id) === -1 && item.count) + sum,
        0
      )) ||
    res.body.response.numberOfProducts;
  const toggleSearchSizeFilter = !!(totalProductsCount === 1 && isSearch && searchTerm);

  let filters = getFiltersAfterProcessing(
    res.body.facets,
    res.body.response.numberOfProducts,
    getFacetSwatchImgPath,
    modifiedFiltersAndSort,
    l1category,
    toggleSearchSizeFilter
  );

  // This is the scenario when the subsequent L3 calls made in bucekting case. In this scenario we need to send back the filter and count, we cached
  // from the response of page L2 call.
  if (isUnbxdLogicApplied(shouldApplyUnbxdLogic, bucketingSeqConfig)) {
    // TODO - fix this - const { temp : { filters: newFilters, totalProductsCount:newTotalProductsCount }} = this.fetchCachedFilterAndCount();
    let productListingFilters = getProductsFilters(state);
    let productListingTotalCount = getTotalProductsCount(state);
    productListingCurrentNavIds = getCurrentListingIds(state);
    if (res.reqObj && res.reqObj.facetCallResponseState) {
      productListingFilters = res.reqObj.facetCallResponseState.filtersMaps;
      productListingTotalCount = res.reqObj.facetCallResponseState.totalProductsCount;
      productListingCurrentNavIds = res.reqObj.facetCallResponseState.currentNavigationIds;
    }
    filters = Object.keys(productListingFilters).length ? productListingFilters : filterMaps;
    totalProductsCount = productListingTotalCount || 0;
  }

  if (totalProductsCount === 1 && isSearch && searchTerm && !isFiltered(filters)) {
    const product = res.body.response.products && res.body.response.products[0];
    const productId = product && product.prodpartno;
    let productUrl = product && product.seo_token;
    if (!productUrl) {
      productUrl = productId;
    }
    const title = product.product_name;
    const isBundleProduct = checkIfBundleProduct(product);

    if (!isMobileApp()) {
      let initialAsPathParam;
      let initialToPathParam;
      if (!isBundleProduct) {
        initialToPathParam = `/p?pid=`;
        initialAsPathParam = `/p/`;
      } else {
        initialToPathParam = `/b?bid=`;
        initialAsPathParam = `/b/`;
      }
      const queryParams = brand ? `&navigateType=direct$brand=${brand}` : `&navigateType=direct`;

      routerPush(
        `${initialToPathParam}${productUrl}${queryParams}`,
        `${initialAsPathParam}${productUrl}${queryParams}`,
        {
          shallow: false,
        }
      );
      return res;
    }
    if (isMobileApp() && navigation) {
      const routeName = isBundleProduct ? 'BundleDetail' : 'ProductDetail';
      navigation.replace(routeName, {
        title,
        pdpUrl: productUrl,
        selectedColorProductId: productId,
        reset: true,
        navigateType: 'direct',
      });
      return null;
    }
  }

  // WHY DO WE NEED THIS??
  const unbxdId = res.headers && res.headers['unbxd-request-id'];
  // TODO - fix this - this.setUnbxdId(unbxdId);
  let entityCategory;
  let categoryNameTop = '';
  let bannerInfo = {};
  // Taking the first product in plp to get the categoryID to be sent to adobe
  if (processHelpers.hasProductsInResponse(res.body.response)) {
    const firstProduct = res.body.response.products[0];
    const categoryPath = processHelpers.getCategoryPath(firstProduct);
    const breadcrumbTopId = processHelpers.getBreadcrumbTopId(breadCrumbs);
    entityCategory = categoryPath
      ? processHelpers.parseCategoryEntity(categoryPath, breadCrumbs)
      : '';
    categoryNameTop = getCategoryId(categoryPath, breadcrumbTopId);
  }
  res.body.response.isSearch = isSearch;
  const response = {
    availableL3InFilter,
    currentListingSearchForText: processHelpers.getCurrentListingSearchForText(
      isSearch,
      searchTerm
    ),
    currentListingSeoKey: searchTerm,
    currentListingId: getCurrentListingId(breadCrumbs),
    currentListingName: categoryType,
    currentListingDescription: getCurrentListingDescription(breadCrumbs),
    currentListingType: getCurrentListingType(breadCrumbs), // need to store it because it will be needed to patch the information when getting additional product information
    isDepartment,
    // An L2 can be an outfits page, if so we need to store the 3rd party tag associated with this outfits page
    outfitStyliticsTag: getOutfitStyliticsTag(isOutfitPage, searchTerm), // DT-34042: dynamic outfit pages
    filtersMaps: filters,
    appliedFiltersIds: processHelpers.getAppliedFilters(filters, modifiedFiltersAndSort),
    totalProductsCount,
    productsInCurrCategory: res.body.response.numberOfProducts,
    unbxdId,
    appliedSortId: sort,
    currentNavigationIds:
      productListingCurrentNavIds || processHelpers.getCurrentNavigationIds(res),
    breadCrumbTrail: processHelpers.getBreadCrumbTrail(breadCrumbs),
    loadedProductsPages: [[]],
    searchResultSuggestions: processHelpers.getSearchResultsSuggestion(res.body.didYouMean),
    unbxdBanners: processHelpers.getUnbxdBanners(res.body.banner),
    entityCategory,
    categoryNameTop,
    isSearch,
    redirect: res.body.redirect,
    updatedUrl,
    categoryFilterArray,
    seoHeaderCopy: getSeoHeaderCopyFromBreadCrumb(breadCrumbs),
    isBopisFilterOn: getIsBopisFilterOn(modifiedFiltersAndSort),
  };
  if (res.body.response) {
    const isUSStore = !isCanada();
    res.body.response.products.forEach((product) =>
      parseProductInfo(product, state, {
        isUSStore,
        getImgPath,
        hasShortImage,
        attributesNames,
        categoryType,
        excludeBadge,
        bucketingSeqConfig,
        shouldApplyUnbxdLogic,
        response,
        res,
        isRecommendationView,
      })
    );
  }

  try {
    if (
      res.body.banner &&
      res.body.banner.banners &&
      res.body.banner.banners[0] &&
      res.body.banner.banners[0].bannerHtml
    ) {
      bannerInfo = JSON.parse(res.body.banner.banners[0].bannerHtml);
    }
  } catch (error) {
    logger.error(error);
  }
  return Promise.all(pendingPromises).then(() => {
    return { ...response, bannerInfo };
  });
};

export default processResponse;
