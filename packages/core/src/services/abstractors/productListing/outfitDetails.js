// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig, isClient } from '@tcp/core/src/utils';
import { executeUnbxdAPICall, executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import { hashValuesReplace, utilPdpArrayHeader } from '../../../utils/utils';
import { parseProductFromAPI } from './productDetail';
import logger from '../../../utils/loggerInstance';

const processOutfitResponse = (
  res,
  getImgPath,
  navTree,
  state,
  outfitId,
  largeImgUrl,
  productPartNumbers
) => {
  const orderedProducts = res.body.response.products.sort(
    (prev, next) => prev.requestedOrder - next.requestedOrder
  );
  const outFitProductsArray = [];
  orderedProducts
    .filter(product => !!product)
    .map((product, index) => {
      const productAndBreadcrumb = parseProductFromAPI(
        product,
        product.uniqueId,
        getImgPath,
        [],
        false,
        false,
        navTree,
        state
      );
      outFitProductsArray[index] = productAndBreadcrumb.product;
      return product;
    });

  return {
    outfitId,
    outfitImageUrl: `${largeImgUrl}`, // As per our vendor this URL will never change and always contain the images
    products: outFitProductsArray,
    unavailableCount: productPartNumbers.length - outFitProductsArray.length,
  };
};

const invokePdpProductsApi = (
  productPartNumbers,
  outfitId,
  getImgPath,
  navTree,
  state,
  largeImgUrl
) => {
  const apiConfig = getAPIConfig();
  const productNumber = productPartNumbers.join(',');
  if (apiConfig) {
    const pdpEndpoint = endpoints && endpoints.pdpProductApi;
    const fetchReviewURL = hashValuesReplace(
      pdpEndpoint.URI,
      utilPdpArrayHeader(productNumber, {
        brand: apiConfig && apiConfig.brandId && apiConfig.brandId.toUpperCase(),
        lang: apiConfig && apiConfig.language && apiConfig.language.toUpperCase(),
        site: apiConfig && apiConfig.siteId && apiConfig.siteId.toUpperCase(),
      })
    );

    const formattedPdpApiURL = `${fetchReviewURL}`;

    const payload = {
      webService: {
        URI: formattedPdpApiURL,
        method: pdpEndpoint && pdpEndpoint.method,
      },
      skipApiIdentifier: true,
    };

    return executeStatefulAPICall(payload)
      .then(res => {
        return processOutfitResponse(
          res,
          getImgPath,
          navTree,
          state,
          outfitId,
          largeImgUrl,
          productPartNumbers
        );
      })
      .catch(err => {
        logger.error('error: ', err);
      });
  }
  return null;
};

/**
 * @function getOutfitProdutsDetails
 * @summary This API is used for outfits. You can pass the URI that will be a list of concated part numbers.
 * vendorColorProductIdsList is a string of dash ('-') separated porduct id's of the outfits vendor
 */
const getOutfitProdutsDetails = (
  { outfitId, vendorColorProductIdsList, getImgPath, navTree, state, largeImgUrl },
  xappURLConfig,
  isPdpProductServiceEnabled,
  enablePDPServiceABTest
) => {
  const productPartNumbers = vendorColorProductIdsList.split('-');
  const payload = {
    body: {
      variants: true,
      'variants.count': 100,
      promotion: false,
      version: 'V2',
      pagetype: 'boolean',
      id: productPartNumbers.join(','),
      fields:
        'productimage,alt_img,style_partno,swatchimage,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,top_rated,TCPFit,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_variant,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,categoryPath2_catMap,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,TCPMultiPackUSStore,TCPMultiPackReferenceUSStore,TCPMultiPackCanadaStore,TCPMultiPackReferenceCanadaStore,TCPStyleTypeUS,TCPStyleQTYUS,TCPStyleTypeCA,TCPStyleQTYCA',
    },
    webService: endpoints && endpoints.getProductDetails,
  };

  if (
    (isClient() && isPdpProductServiceEnabled && enablePDPServiceABTest) ||
    (!isClient() && isPdpProductServiceEnabled)
  ) {
    return invokePdpProductsApi(
      productPartNumbers,
      outfitId,
      getImgPath,
      navTree,
      state,
      largeImgUrl
    );
  }
  return executeUnbxdAPICall(payload, null, xappURLConfig)
    .then(response => {
      return processOutfitResponse(
        response,
        getImgPath,
        navTree,
        state,
        outfitId,
        largeImgUrl,
        productPartNumbers
      );
    })
    .catch(err => {
      logger.error('error: ', err);
    });
};

export default getOutfitProdutsDetails;
