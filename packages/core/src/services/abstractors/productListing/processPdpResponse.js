// 9fbef606107a605d69c0edbcd8029e5d
import { isCanada } from '@tcp/core/src/utils';
import { MULTI_PACK_TYPE } from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import { extractAttributeValue } from './productParser';

const getIsGiftCard = (isGiftCard, baseProduct) => {
  return isGiftCard ? 'Gift Card' : baseProduct.product_name;
};

const getRawBreadCrumb = (categoryPathMap) => {
  return categoryPathMap && categoryPathMap.length > 0 ? categoryPathMap[0].split('|')[0] : '';
};

const getGeneralProductId = (colorIdOrSeoKeyword, colorFitsSizesMap, baseProduct) => {
  return (
    colorIdOrSeoKeyword ||
    (colorFitsSizesMap[0] && colorFitsSizesMap[0].productid) ||
    baseProduct.productid
  );
};

const getCatId = (categoryId) => {
  return categoryId || '';
};

const getLongDescription = (isBundleProduct, baseProduct) => {
  return isBundleProduct
    ? baseProduct.product_long_description
    : baseProduct.style_long_description;
};

const getColorFitSizeDisplayNames = (isGiftCard) => {
  return isGiftCard ? { color: 'Design', size: 'Value (USD)', size_alt: 'Value' } : null;
};

const getListPrice = (baseProduct) => {
  return parseFloat(baseProduct.min_list_price) || parseFloat(baseProduct.min_offer_price) || 0;
};

const getOfferPrice = (baseProduct) => {
  return parseFloat(baseProduct.min_offer_price) || 0;
};

const getHighListPrice = (baseProduct) => {
  return parseFloat(baseProduct.high_list_price) || 0;
};
const getHighOfferPrice = (baseProduct) => {
  return parseFloat(baseProduct.high_offer_price) || 0;
};

const getLowListPrice = (baseProduct) => {
  return parseFloat(baseProduct.low_list_price) || 0;
};

const getLowOfferPrice = (baseProduct) => {
  return parseFloat(baseProduct.low_offer_price) || 0;
};

const getOnlyListPrice = (baseProduct) => {
  return parseFloat(baseProduct.min_list_price) || 0;
};

const getRating = (isGiftCard, ratings) => {
  return isGiftCard ? 0 : ratings || 0;
};

const getReviewsCount = (isGiftCard, reviewsCount) => {
  return isGiftCard ? 0 : reviewsCount;
};

const multiPackStore = (productVariants) => {
  const getmultiPackStore = productVariants.find((element) =>
    !isCanada() ? !!element.TCPMultiPackUSStore : !!element.TCPMultiPackCanadaStore
  );
  if (getmultiPackStore) {
    return !isCanada()
      ? getmultiPackStore.TCPMultiPackUSStore
      : getmultiPackStore.TCPMultiPackCanadaStore;
  }
  return false;
};

const prodDescColorDetail = (productVariants) => {
  const colorList = productVariants.filter((element) => !!element.TCPStyleColorUS);
  return colorList.map((variantItem) => {
    return variantItem ? `${variantItem.TCPStyleColorUS}#${variantItem.uniqueId}` : false;
  });
};

export const getTCPMultiPackReferenceUSStore = (multipack) => {
  const newMultipackStore =
    multipack && multipack.includes('|') ? multipack.replace(/\|/g, ',') : multipack;
  return newMultipackStore || false;
};

const tcpMultiPackReferenceStore = (productVariants) => {
  const gettcpMultiPackReferenceStore = productVariants.filter((element) =>
    !isCanada()
      ? !!element.TCPMultiPackReferenceUSStore
      : !!element.TCPMultiPackReferenceCanadaStore
  );

  return gettcpMultiPackReferenceStore.map((variantItem) => {
    const element = !isCanada()
      ? variantItem.TCPMultiPackReferenceUSStore
      : variantItem.TCPMultiPackReferenceCanadaStore;
    return variantItem ? getTCPMultiPackReferenceUSStore(element) : false;
  });
};

const newCatList = (productVariants) => {
  return productVariants.map((variantItem) => {
    return variantItem.variants;
  });
};

const mapNewMultiPack = (product) => {
  const newMultipackList = product && product.length && product.split(';');
  const filtered = newMultipackList.filter((x) => x !== 'undefined');
  if (product && product.length) {
    return filtered
      .map((packItem) => {
        return packItem && Number(packItem.split('#')[1]).toString();
      })
      .sort((a, b) => {
        return a - b;
      });
  }
  return null;
};

const getTcpMultiPackStore = (product) => {
  if (!isCanada()) {
    return product.TCPMultiPackUSStore ? product.TCPMultiPackUSStore : false;
  }
  return product.TCPMultiPackCanadaStore ? product.TCPMultiPackCanadaStore : false;
};

const getMultiPackAllColorInfo = (productVariants) => {
  return productVariants.map((product) => ({
    TCPMultiPackUSStore: getTcpMultiPackStore(product),
    name: product.auxdescription,
    variants: product.variants,
    styleTypeUS: product.TCPStyleTypeUS,
    styleTypeCA: product.TCPStyleTypeCA,
    prodpartno: product.prodpartno,
    TCPMultiPackReferenceUSStore: !isCanada()
      ? getTCPMultiPackReferenceUSStore(product.TCPMultiPackReferenceUSStore)
      : getTCPMultiPackReferenceUSStore(product.TCPMultiPackReferenceCanadaStore),
  }));
};

const getMapStyleId = (product) => {
  const newArrayOfIds = product && product.length && product.split(';');
  const newArray = [];
  if (newArrayOfIds && newArrayOfIds.length) {
    newArrayOfIds.forEach((packItem) => {
      if (packItem !== 'undefined' && newArray.indexOf(packItem) < 0) {
        newArray.push(packItem);
      }
    });
  }
  return newArray;
};

const getcheckPack = (baseProduct, colorIdOrSeoKeyword) => {
  return baseProduct.TCPStyleTypeUS === MULTI_PACK_TYPE.SINGLE ||
    baseProduct.TCPStyleTypeCA === MULTI_PACK_TYPE.SINGLE
    ? colorIdOrSeoKeyword
    : baseProduct.uniqueId;
};

const getTcpStyleQTY = (baseProduct) => {
  return (
    (!isCanada() ? baseProduct.TCPStyleQTYUS : baseProduct.TCPStyleQTYCA) || baseProduct.tcpStyleQty
  );
};

const processPdpResponse = ({
  baseProduct,
  categoryPathMap,
  categoryPath2Map,
  colorIdOrSeoKeyword,
  colorFitsSizesMap,
  categoryId,
  isGiftCard,
  isBundleProduct,
  imagesByColor,
  reviewsCount,
  ratings,
  alternateSizes,
  breadCrumbs,
  category,
  productVariants,
  partId,
  multipackMappings,
  oldProductColorId,
  isMultiPack,
}) => {
  const getMultiProduct = multipackMappings;
  const checkPack = getcheckPack(baseProduct, colorIdOrSeoKeyword);
  const isFormId = !isMultiPack ? colorIdOrSeoKeyword : oldProductColorId;
  return {
    breadCrumbs,
    rawBreadCrumb: getRawBreadCrumb(categoryPathMap),
    product: {
      // generalProductId = color with matching seo OR colorIdOrSeoKeyword is its a number OR default to first color's ID (To Support Outfits)
      ratingsProductId: baseProduct.style_partno,
      // generalProductId = color with matching seo OR colorIdOrSeoKeyword is its a number OR default to first color's ID (To Support Outfits)
      generalProductId: getGeneralProductId(isFormId, colorFitsSizesMap, baseProduct),
      categoryId: getCatId(categoryId),
      category,
      name: getIsGiftCard(isGiftCard, baseProduct),
      pdpUrl: `/p/${colorIdOrSeoKeyword}`,
      shortDescription: baseProduct.product_short_description,
      longDescription: getLongDescription(isBundleProduct, baseProduct),
      imagesByColor,
      colorFitsSizesMap,
      isGiftCard,
      colorFitSizeDisplayNames: getColorFitSizeDisplayNames(isGiftCard),
      listPrice: getListPrice(baseProduct),
      offerPrice: getOfferPrice(baseProduct),
      highListPrice: getHighListPrice(baseProduct),
      highOfferPrice: getHighOfferPrice(baseProduct),
      lowListPrice: getLowListPrice(baseProduct),
      lowOfferPrice: getLowOfferPrice(baseProduct),
      onlyListPrice: getOnlyListPrice(baseProduct),
      ratings: getRating(isGiftCard, ratings),
      reviewsCount: getReviewsCount(isGiftCard, reviewsCount),
      // unbxdId: getUnbxdId(),
      unbxdProdId: baseProduct.uniqueId,
      unbxdRatings: baseProduct.TCPBazaarVoiceRating,
      unbxdReviewsCount: baseProduct.TCPBazaarVoiceReviewCount,
      alternateSizes,
      productId: checkPack,
      promotionalMessage: baseProduct.TCPLoyaltyPromotionTextUSStore || '',
      promotionalPLCCMessage: baseProduct.TCPLoyaltyPLCCPromotionTextUSStore || '',
      long_product_title: baseProduct.long_product_title || '',
      product_long_description: baseProduct.product_long_description || '',
      bundleProducts: baseProduct.products || [],
      categoryPathMap,
      categoryPath2Map,
      seoToken: baseProduct.seo_token,
      multiPackUSStore: multiPackStore(productVariants),
      tcpMultiPackReferenceUSStore: tcpMultiPackReferenceStore(productVariants),
      getMultiPackAllColor: getMultiPackAllColorInfo(productVariants),
      categoryIdList: baseProduct.variants,
      newCategoryIdList: newCatList(productVariants),
      partIdDetails: partId,
      productFamily: baseProduct.productfamily,
      TCPMultipackProductMapping: getMultiProduct
        ? [...new Set(mapNewMultiPack(getMultiProduct))]
        : null,
      TCPStyleType:
        (!isCanada() ? baseProduct.TCPStyleTypeUS : baseProduct.TCPStyleTypeCA) ||
        baseProduct.tcpStyleType,
      TCPmapNewStyleId: [...new Set(getMapStyleId(getMultiProduct))],
      isMultiPackResponse: isMultiPack,
      TCPStyleQTY: getTcpStyleQTY(baseProduct),
      TCPStyleColor: prodDescColorDetail(productVariants),
      allProductId: productVariants,
      primaryBrand: !isCanada()
        ? extractAttributeValue(baseProduct, 'TCPPrimaryBrandUSStore')
        : extractAttributeValue(baseProduct, 'TCPPrimaryBrandCanadaStore'),
      TCPColor: baseProduct.TCPColor,
    },
  };
};

export default processPdpResponse;
