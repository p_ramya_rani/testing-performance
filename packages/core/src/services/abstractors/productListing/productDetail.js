/* eslint-disable max-lines */
/* eslint-disable no-console */
/* eslint-disable max-params */
// 9fbef606107a605d69c0edbcd8029e5d
import layoutAbstractor from '@tcp/core/src/services/abstractors/bootstrap/layout';
import { getAPIConfig, isServer, isMobileApp, isClient } from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import handler, { executeExternalAPICall } from '@tcp/core/src/services/handler/handler';
import { hashValuesReplace, utilPdpArrayHeader, isCanada } from '../../../utils/utils';
import { executeUnbxdAPICall, executeStatefulAPICall } from '../../handler';
import { extractAttributeValue } from './productParser';
import endpoints from '../../endpoints';
import processHelpers from './processHelpers';
import { getNavTree } from '../../../components/features/browse/ProductDetail/container/ProductDetail.selectors';
import processResponse from './processPdpResponse';
import processHelperUtil from './ProductDetail.util';
import { getFormattedError } from '../../../utils/errorMessage.util';

const getInventoryDetails = (hasInventory, sizeVariant) => {
  if (!hasInventory) {
    return processHelperUtil.getInventoryStatus(sizeVariant);
  }
  return false;
};

const getMultiPackInfo = (multipackMappings, colorVariant) => {
  return multipackMappings + (multipackMappings !== '' ? ';' : '') + colorVariant.multipack_mapping;
};

const getColorReturn = (colorsFitsMap, color, newindex) => {
  let getColor = color;
  if (colorsFitsMap && color in colorsFitsMap) {
    getColor = `${color}-${newindex}`;
  }
  return getColor;
};

const getIsTCPMultipackItem = (colorVariant) => {
  return isCanada()
    ? colorVariant.TCPMultiPackReferenceCanadaStore
    : colorVariant.TCPMultiPackUSStore;
};
/* eslint-disable max-statements */
export const parseProductFromAPI = (
  product,
  colorIdOrSeoKeyword,
  dontFetchExtraImages,
  breadCrumbs,
  excludeBage,
  isBundleProduct,
  getImgPath,
  state,
  partId,
  oldProductColorId,
  isMultiPack
  // eslint-disable-next-line
) => {
  const baseProduct = processHelperUtil.getBaseProduct(product); // Getting multiple products as color variants
  const productVariants = processHelperUtil.getProductVariants(product);
  const isGiftCard = processHelpers.isGiftCard(baseProduct); // TBD: backend to confirm whether partNumber will always be giftCardBundle for gift cards.
  const productAttributes = processHelpers.getProductAttributes();

  baseProduct.tcpStyleType = extractAttributeValue(product, productAttributes.tcpStyle);
  baseProduct.tcpStyleQty = extractAttributeValue(product, productAttributes.tcpStyleQty);

  let hasFit = false;
  let hasInventory = false;
  let alternateSizes;
  let defaultColorAlternateSizes;
  let otherColorAlternateSizes;

  let hasAdditionalStyles = false;
  let imagesByColor = {};
  // const imagesByColor = extractExtraImages(rawColors, baseProduct.alt_img, getImgPath);

  // This color map is used as an intermediary step to help consolidate all sizes under fits
  const colorsFitsMap = {};
  let multipackMappings = '';
  // eslint-disable-next-line
  for (let colorVariant of productVariants) {
    const newindex = colorVariant.imagename;
    multipackMappings = getMultiPackInfo(multipackMappings, colorVariant);
    let color = processHelperUtil.getProductColorName(isGiftCard, colorVariant);
    const currentColorFitsSizesMap = {};
    let fitName = '';
    if (processHelperUtil.isColorVariant(colorVariant)) {
      // eslint-disable-next-line
      for (let sizeVariant of colorVariant.variants) {
        fitName = processHelperUtil.getFitName(sizeVariant);
        if (!currentColorFitsSizesMap[fitName]) {
          currentColorFitsSizesMap[fitName] = [];
        }
        hasInventory = getInventoryDetails(hasInventory, sizeVariant);
        currentColorFitsSizesMap[fitName].push({
          sizeName: processHelperUtil.getSizeName(sizeVariant),
          skuId: sizeVariant.v_item_catentry_id,
          listPrice: processHelperUtil.getListPriceProduct(sizeVariant),
          offerPrice: processHelperUtil.getOfferPriceProduct(sizeVariant),
          maxAvailable: sizeVariant.v_qty,
          maxAvailableBoss: sizeVariant.v_qty_boss,
          variantId: sizeVariant.variantId,
          variantNo: sizeVariant.v_variant,
          isMultiPack: getIsTCPMultipackItem(colorVariant),
        });
      }
    }
    hasFit = !!fitName || hasFit;
    let hasDefaultFit = false;
    const sortOptions = {
      regular: 1,
      slim: 2,
      plus: 3,
      husky: 4,
      other: 5,
    };
    const sortedKeys = processHelperUtil.getSortedKeys(currentColorFitsSizesMap, sortOptions);
    // eslint-disable-next-line
    color = getColorReturn(colorsFitsMap, color, newindex);
    colorsFitsMap[color] = sortedKeys.map((fitNameVal) => {
      const isDefaultFit = fitNameVal.toLowerCase() === 'regular';
      hasDefaultFit = processHelperUtil.getHasDefaultFit(hasDefaultFit, isDefaultFit);

      return {
        fitName: fitNameVal,
        isDefault: isDefaultFit,
        maxAvailable: processHelperUtil.validateQuantityAvailable(
          currentColorFitsSizesMap[fitNameVal]
        ),
        sizes: processHelperUtil.convertMultipleSizeSkusToAlternatives(
          currentColorFitsSizesMap[fitNameVal]
        ),
      };
    });

    if (processHelperUtil.setDefault(hasDefaultFit, colorsFitsMap, color)) {
      colorsFitsMap[color][0].isDefault = true;
    }

    if (processHelperUtil.getDefaultColorAlternateSizes(colorVariant, colorIdOrSeoKeyword)) {
      defaultColorAlternateSizes = colorVariant.additional_styles;
    }
    if (processHelperUtil.getIsAdditionalStyles(hasAdditionalStyles, colorVariant)) {
      otherColorAlternateSizes = colorVariant.additional_styles;
      hasAdditionalStyles = true;
    }
  }

  try {
    alternateSizes = processHelperUtil.getAlternateSizes(
      defaultColorAlternateSizes,
      otherColorAlternateSizes
    );
  } catch (err) {
    alternateSizes = {};
    console.error('API response coming for additional_styles key JSON format is incorrect', err);
  }
  // Generate the colorFitsSizeMap needed for mapping colors to fits/sizes

  const colorObj = processHelperUtil.getColorfitsSizesMap({
    productVariants,
    isGiftCard,
    breadCrumbs,
    getImgPath,
    imagesByColor,
    hasFit,
    isBundleProduct,
    colorsFitsMap,
    excludeBage,
    productAttributes,
    state,
  });
  const { itemColorMap: colorFitsSizesMap, imagesByColor: imagesByColorRes } = colorObj;
  imagesByColor = imagesByColorRes;
  const ratingsAndReviewObj = processHelperUtil.getReviewsCountProduct(
    product,
    colorIdOrSeoKeyword
  );
  const categoryPathMap = processHelperUtil.getCategoryPathMap(baseProduct);
  const categoryPath2Map = processHelperUtil.getCategoryPath2Map(baseProduct);
  const categoryPath = processHelperUtil.getCategory(baseProduct);
  const categoryId = processHelperUtil.getCategoryId(breadCrumbs, baseProduct, categoryPath);
  const category = processHelperUtil.getCategoryValue(baseProduct);
  return processResponse({
    baseProduct,
    categoryPathMap,
    categoryPath2Map,
    colorIdOrSeoKeyword,
    colorFitsSizesMap,
    categoryId,
    isGiftCard,
    isBundleProduct,
    imagesByColor,
    ...ratingsAndReviewObj,
    alternateSizes,
    breadCrumbs,
    category,
    productVariants,
    partId,
    multipackMappings,
    oldProductColorId,
    isMultiPack,
  });
};

/**
 * @function moduleResolver
 * @param {object} moduleObjects -  module data to make module graphQL call
 * @summary This will get the modules of the layout from CMS based on categoryId
 */
const moduleResolver = async (moduleObjects, apiConfig) => {
  const response = await layoutAbstractor.getModulesData(moduleObjects, apiConfig);
  return layoutAbstractor.processModuleData(response.data);
};

/**
 * @function formatSlotData
 * @param {array} slotItems -  list of slots
 * @param {string} language -  selected language
 * @summary Formats the slot as per the requirement of graphQL queryBuilder
 */
const formatSlotData = (slotItems, language) => {
  return slotItems.map((slot, index) => {
    return {
      name: slot.moduleName,
      data: {
        contentId: slot.contentId,
        slot: slot.name || `slot_${index + 1}`, // TODO: Remove Temporary Check for slot, as not supported from CMS yet
        lang: language !== 'en' ? language : '', // TODO: Remove Temporary Check for en support, as not supported from CMS yet
      },
    };
  });
};

/**
 * @function layoutResolver
 * @param {object} layoutConfig -  contains categoryId and pageName
 * @summary This will get the layout of the page from CMS based on categoryId
 */
export const layoutResolver = async ({ category, pageName, apiConfig }) => {
  let modules = {};
  const layout = {};
  try {
    const { channelId, siteIdCMS, brandIdCMS, language } = apiConfig;
    const moduleConfig = {
      name: 'promoContent',
      data: {
        brand: brandIdCMS,
        country: siteIdCMS,
        channel: channelId || 'Mobile',
        lang: language === 'en' ? '' : language,
        path: pageName,
        category,
      },
    };
    const {
      data: { contentLayout },
    } = await handler.fetchModuleDataFromGraphQL({ ...moduleConfig }, apiConfig);
    const moduleObjects = [];
    contentLayout.forEach((data) => {
      const dataItems = data.items;
      layout[data.key] = dataItems;
      Object.keys(dataItems).forEach((item) => {
        const slotItems = !!dataItems[item] && dataItems[item].slots;
        if (typeof slotItems === 'object') {
          moduleObjects.push(...slotItems);
        }
      });
    });
    modules = await moduleResolver(formatSlotData(moduleObjects, language), apiConfig);
  } catch (err) {
    logger.error(err);
  }
  return {
    layout,
    modules,
  };
};

const getFieldsValue = (isBotDevice) => {
  return isServer() && !isBotDevice
    ? 'productimage,alt_img,style_partno,swatchimage,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,low_offer_price,high_offer_price,low_list_price,high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,v_qty_boss,TCPMultiPackUSStore,TCPMultiPackCanadaStore,TCPMultiPackReferenceUSStore,TCPMultiPackReferenceCanadaStore,single_mapping,multipack_mapping,set_mapping,TCPStyleTypeUS,TCPStyleTypeCA,TCPStyleQTYCA,TCPStyleQTYUS,productfamily,TCPStyleColorCA,TCPStyleColorUS'
    : 'productimage,alt_img,style_partno,swatchimage,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,low_offer_price,high_offer_price,low_list_price,high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,TCPMultiPackUSStore,TCPMultiPackCanadaStore,TCPMultiPackReferenceUSStore,TCPMultiPackReferenceCanadaStore,single_mapping,multipack_mapping,set_mapping,TCPStyleTypeUS,TCPStyleTypeCA,TCPStyleQTYCA,TCPStyleQTYUS,productfamily,TCPStyleColorCA,TCPStyleColorUS';
};

const getFilterValue = (productId) => {
  const resValue = isMobileApp() ? encodeURIComponent(`"${productId}"`) : `"${productId}"`;
  return `prodpartno:${resValue}`;
};

const isGiftCard = (productId) =>
  productId === 'gift' || (productId || '').toLowerCase() === 'gift card';

const payloadFinder = (
  count,
  productId,
  brand,
  productColorId,
  fields,
  multipackReferencesAPICall,
  alternateBrand
) => {
  const oldPayload = {
    body: {
      variants: true,
      'variants.count': count,
      version: 'V2',
      rows: 20,
      pagetype: 'boolean',
      q: productId,
      promotion: false,
      fields,
      brand: alternateBrand || brand,
    },
    webService: endpoints.getProductInfoById,
    brand,
  };

  const newPayload = {
    body: {
      variants: true,
      'variants.count': count,
      version: 'V2',
      rows: 20,
      pagetype: 'boolean',
      id: productColorId,
      promotion: false,
      fields,
      brand: alternateBrand || brand,
    },
    webService: endpoints.getProductDetails,
    brand,
  };
  return multipackReferencesAPICall ? newPayload : oldPayload;
};
/**
 * @function getProductInfoById
 * @summary This will get product info and all color/sizes for that product
 */

const processApiResponse = (
  res,
  productColorId,
  breadCrumb,
  excludeBage,
  isBundleProduct,
  state,
  partId,
  oldProductColorId,
  isMultiPack
) => {
  return parseProductFromAPI(
    res.body.response.products,
    productColorId,
    false,
    breadCrumb,
    excludeBage,
    isBundleProduct,
    processHelperUtil.getImgPath,
    state,
    partId,
    oldProductColorId,
    isMultiPack
  );
};

const unbxdApiCall = (
  payload,
  xappURLConfig,
  productColorId,
  breadCrumb,
  excludeBage,
  isBundleProduct,
  state,
  partId,
  oldProductColorId,
  isMultiPack,
  originalURL,
  config
) => {
  return executeUnbxdAPICall(payload, originalURL, xappURLConfig, config)
    .then(
      (res) => {
        return processApiResponse(
          res,
          productColorId,
          breadCrumb,
          excludeBage,
          isBundleProduct,
          state,
          partId,
          oldProductColorId,
          isMultiPack
        );
      },
      null,
      xappURLConfig
    )
    .catch((err) => {
      // if (err && ((err.status >= 400 && err.status <= 404) || err.status === 500) && isClient()) {
      // TODO - handle it - window.location.href = getErrorPagePath(this.apiHelper._configOptions.siteId);
      // }
      console.log(err);
      logger.error({
        error: `Error while executing Unbxd API call - ${err}`,
        extraData: {
          componentName: 'productDetail',
          methodName: 'getProductInfoById',
          payload,
        },
      });
      throw getFormattedError(err);
      // TODO - handle it - throw this.apiHelper.getFormattedError(err);
    });
};

const invokePdpProductsApi = (
  productColorId,
  productPartNumbers,
  breadCrumb,
  excludeBage,
  isBundleProduct,
  state,
  partId,
  oldProductColorId,
  isMultiPack,
  xappURLConfig,
  alternateBrand
) => {
  const apiConfig = getAPIConfig();
  const productNumber = productPartNumbers;
  if (apiConfig) {
    const pdpEndpoint = endpoints && endpoints.pdpProductApi;
    const fetchReviewURL = hashValuesReplace(
      pdpEndpoint.URI,
      utilPdpArrayHeader(productNumber, {
        brand:
          (alternateBrand && alternateBrand.toUpperCase()) ||
          (apiConfig && apiConfig.brandId && apiConfig.brandId.toUpperCase()),
        lang: apiConfig && apiConfig.language && apiConfig.language.toUpperCase(),
        site: apiConfig && apiConfig.siteId && apiConfig.siteId.toUpperCase(),
      })
    );

    const formattedPdpApiURL = `${fetchReviewURL}`;

    const payload = {
      webService: {
        URI: formattedPdpApiURL,
        method: pdpEndpoint && pdpEndpoint.method,
      },
      skipApiIdentifier: true,
    };

    return executeStatefulAPICall(payload)
      .then(
        (pdpProductsResponse) => {
          return processApiResponse(
            pdpProductsResponse,
            productColorId,
            breadCrumb,
            excludeBage,
            isBundleProduct,
            state,
            partId,
            oldProductColorId,
            isMultiPack
          );
        },
        null,
        xappURLConfig
      )
      .catch((err) => {
        console.log(err);
        logger.error({
          error: `Error while executing PDP products API call - ${err}`,
          extraData: {
            componentName: 'productDetail.js',
            methodName: 'getProductInfoBystyleNumber',
            payload,
          },
        });
        throw getFormattedError(err);
      });
  }
  return null;
};

/* invoke pdp api changes */
// eslint-disable-next-line complexity
const invokePdpApi = (
  productId,
  productColorId,
  breadCrumb,
  excludeBage,
  isBundleProduct,
  state,
  partId,
  oldProductColorId,
  isMultiPack,
  xappURLConfig,
  multipackReferencesAPICall,
  config,
  brand
) => {
  if (multipackReferencesAPICall === true) {
    return invokePdpProductsApi(
      productId,
      productColorId,
      breadCrumb,
      excludeBage,
      isBundleProduct,
      state,
      partId,
      oldProductColorId,
      isMultiPack,
      xappURLConfig,
      brand
    );
  }
  const apiConfig = config || getAPIConfig();
  const styleNumber = productId;
  if (apiConfig) {
    const pdpEndpoint = endpoints && endpoints.pdpApi;
    const fetchReviewURL = hashValuesReplace(
      pdpEndpoint.URI,
      utilPdpArrayHeader(styleNumber, {
        brand:
          (brand && brand.toUpperCase()) ||
          (apiConfig && apiConfig.brandId && apiConfig.brandId.toUpperCase()),
        lang: apiConfig && apiConfig.language && apiConfig.language.toUpperCase(),
        site: apiConfig && apiConfig.siteId && apiConfig.siteId.toUpperCase(),
      })
    );

    const formattedPdpApiURL = `${fetchReviewURL}`;

    const payload = {
      webService: {
        URI: formattedPdpApiURL,
        method: pdpEndpoint.method,
      },
      skipApiIdentifier: true,
    };

    return executeStatefulAPICall(payload)
      .then(
        (pdpResponse) => {
          return processApiResponse(
            pdpResponse,
            productColorId,
            breadCrumb,
            excludeBage,
            isBundleProduct,
            state,
            partId,
            oldProductColorId,
            isMultiPack
          );
        },
        null,
        xappURLConfig
      )
      .catch((err) => {
        console.log(err);
        logger.error({
          error: `Error while executing PDP API call - ${err}`,
          extraData: {
            componentName: 'productDetail.js',
            methodName: 'getProductInfoByProdPartNumber',
            payload,
          },
        });
        throw getFormattedError(err);
      });
  }
  return null;
};
/* invoke pdp api changes ends */

const fieldsFinder = (isBundleProduct, isRadialInvEnabled, fields) => {
  let newFields = fields;
  if (!isBundleProduct) {
    if (isRadialInvEnabled) {
      newFields = newFields.concat(',v_qty_boss');
    }
  } else {
    newFields =
      'productimage,alt_img,style_partno,swatchimage,related_product_swatch_images,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,product_name,TCPColor,imagename,productid,uniqueId,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,product_long_description,seo_token,prodpartno,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,low_offer_price,high_offer_price,low_list_price,high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,TCPMultiPackUSStore,TCPMultiPackReferenceUSStore,TCPMultiPackReferenceCanadaStore,TCPMultiPackCanadaStore,single_mapping,multipack_mapping,set_mapping,TCPStyleTypeUS,TCPStyleTypeCA,TCPStyleQTYCA,TCPStyleQTYUS,productfamily,TCPStyleColorCA,TCPStyleColorUS';
  }
  return newFields;
};

const pdpApiCondition = (isPdpServiceEnabled, enablePDPServiceABTest) => {
  return (
    (isClient() && isPdpServiceEnabled && enablePDPServiceABTest) ||
    (!isClient() && isPdpServiceEnabled)
  );
};
/* This will send null if multipackReferencesAPICall else will send the original url received */
const formatOriginalURL = (multipackReferencesAPICall, originalURL) => {
  return multipackReferencesAPICall ? null : originalURL;
};

const getProductInfoById = (
  xappURLConfig,
  productColorId,
  state,
  brand,
  isBundleProduct,
  isBotDevice,
  originalURL,
  multipackReferencesAPICall,
  partId,
  oldProductColorId,
  isMultiPack,
  isPdpServiceEnabled,
  enablePDPServiceABTest,
  apiConfig,
  otherBrand
) => {
  // const isRadialInvEnabled = generalStoreView.getIsRadialInventoryEnabled(this.store.getState());
  // const location = routingInfoStoreView.getHistory(this.store.getState()).location;
  // const isBundleProduct = matchPath(location.pathname, { path: PAGES.productBundle.pathPattern });
  const isRadialInvEnabled = true;

  const breadCrumb = processHelperUtil.breadCrumbFactory(state);
  const categoryId =
    breadCrumb[breadCrumb.length - 1] && breadCrumb[breadCrumb.length - 1].categoryId;
  const navigationTree = getNavTree(state);
  const excludeBage = categoryId
    ? processHelperUtil.getNavAttributes(navigationTree, categoryId, 'excludeAttribute')
    : '';
  const alternateProdId =
    productColorId.indexOf('_') > -1 ? productColorId.split('_')[0] : productColorId;

  const productId =
    productColorId.indexOf('-') > -1 ? productColorId.split('-')[0] : alternateProdId;
  // eslint-disable-next-line no-param-reassign
  productColorId =
    productColorId.indexOf('-') > -1 ? productColorId.replace('-', '_') : productColorId; // As ProductColorId response has always _ rather than hyphen(-)
  let fields = getFieldsValue(isBotDevice);
  let count = 100;
  fields = fieldsFinder(isBundleProduct, isRadialInvEnabled, fields);
  if (
    fields ===
    'productimage,alt_img,style_partno,swatchimage,related_product_swatch_images,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,product_name,TCPColor,imagename,productid,uniqueId,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,product_long_description,seo_token,prodpartno,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,low_offer_price,high_offer_price,low_list_price,high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,TCPMultiPackUSStore,TCPMultiPackReferenceUSStore,TCPMultiPackReferenceCanadaStore,TCPMultiPackCanadaStore,single_mapping,multipack_mapping,set_mapping,TCPStyleTypeUS,TCPStyleTypeCA,TCPStyleQTYCA,TCPStyleQTYUS,productfamily,TCPStyleColorCA,TCPStyleColorUS'
  ) {
    count = 0;
  }
  const alternateBrand = otherBrand;
  const payload = payloadFinder(
    count,
    productId,
    brand,
    productColorId,
    fields,
    multipackReferencesAPICall,
    alternateBrand
  );

  if (isGiftCard(productId)) {
    payload.body.filter = 'giftcard:1';
    payload.body.sort = 'style_sequence asc';
  }
  if (isBundleProduct) {
    payload.body.filter = getFilterValue(productId);
  }
  payload.brand = alternateBrand;
  if (pdpApiCondition(isPdpServiceEnabled, enablePDPServiceABTest)) {
    return invokePdpApi(
      productId,
      productColorId,
      breadCrumb,
      excludeBage,
      isBundleProduct,
      state,
      partId,
      oldProductColorId,
      isMultiPack,
      xappURLConfig,
      multipackReferencesAPICall,
      apiConfig,
      alternateBrand
    );
  }
  const originalURLToSend = formatOriginalURL(multipackReferencesAPICall, originalURL);
  payload.brand = alternateBrand;
  return unbxdApiCall(
    payload,
    xappURLConfig,
    productColorId,
    breadCrumb,
    excludeBage,
    isBundleProduct,
    state,
    partId,
    oldProductColorId,
    isMultiPack,
    originalURLToSend,
    apiConfig
  );
};

const getReviewContent = (reviewStats, productId) => {
  const stats = {
    avgRating: 0,
    totalReviewCount: 0,
  };
  try {
    const rating = reviewStats.Includes.Products[productId].ReviewStatistics;
    stats.avgRating = rating.AverageOverallRating;
    stats.totalReviewCount = rating.TotalReviewCount;
    return stats;
  } catch {
    return stats;
  }
};

/**
 * @function getProductBVRatings
 * @param {object} productId -  productId of product to fetch its review
 * @summary This will get the review stats from bazar voice
 */
export const getProductBVReviewStats = async (productId) => {
  const apiConfig = getAPIConfig();
  if (apiConfig && apiConfig.BV_API_URL) {
    const serviceConfig = endpoints.getBazaarVoiceRatings;
    const utilArrayHeader = ({ pId, passKey, limit }) => {
      return [
        {
          key: '#product-id#',
          value: pId,
        },
        {
          key: '#pass-key#',
          value: passKey,
        },
        {
          key: '#limit#',
          value: limit,
        },
      ];
    };
    const fetchReviewURL = hashValuesReplace(
      serviceConfig.URI,
      utilArrayHeader({
        pId: productId,
        passKey: apiConfig.BV_API_KEY,
        limit: 1,
      })
    );

    const formattedBvApiURL = `${apiConfig.BV_API_URL}/${fetchReviewURL}`;

    const payload = {
      webService: {
        URI: formattedBvApiURL,
        method: serviceConfig.method,
        timeout: serviceConfig.timeout,
      },
    };

    return executeExternalAPICall(payload)
      .then((res) => {
        return getReviewContent(res.body, productId);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  return null;
};

export default getProductInfoById;
