// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils';
import { executeExternalAPICall, executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import { sanitizeEntity, parseStoreHours, isCanada } from '../../../utils';
import { formatPhoneNumber } from '../../../utils/formValidation/phoneNumber';
import {
  responseContainsErrors,
  ServiceResponseError,
  getFormattedError,
} from '../../../utils/errorMessage.util';

const ERROR_MESSAGES_BOPIS = {
  caPostalCode: 'Please enter a Canadian Postal Code',
  usZipCode: 'Please enter a US Zip Code',
  zeroResults: 'ZERO_RESULTS',
  noAddressFound: 'We were unable to find the address you typed, please try again',
  selectSize: 'Please select a size',
  storeSearchException: 'Oops, something went wrong, Please retry',
  apiTimeoutError: 'Oh no! Something went wrong… Please try again in a few minutes',
};

export const BOPIS_ITEM_AVAILABILITY = {
  AVAILABLE: 'OK',
  LIMITED: 'LIMITED',
  UNAVAILABLE: 'UNAVAILABLE',
};

export const STORE_TYPES = {
  RETAIL: 'Retail Store',
  OUTLET: 'Outlet',
};

const shouldFetchAddressLine = (store) => {
  return (
    store.addressLine && typeof store.addressLine === 'object' && !Array.isArray(store.addressLine)
  );
};

const fetchStoreType = (store, addressLine) => {
  return (
    store.storeType || (addressLine && addressLine[addressLine.length - 1]) || store.address3 || ''
  );
};

const getHoursOfOperation = (store, isFromDeafultCase) => {
  let hoursOfOperation;
  // Backend's API structure for stores are never the same, so i am checking a few differant places for store hours
  if (store.storehours) {
    if (isFromDeafultCase) {
      hoursOfOperation = store.storehours;
    } else {
      hoursOfOperation = store.storehours.storehours;
    }
  } else if (store.Attribute && store.Attribute[0]) {
    hoursOfOperation = JSON.parse(store.Attribute[0].displayValue || '{}').storehours;
  } else if (store.attribute) {
    hoursOfOperation = JSON.parse(store.attribute.displayValue || '{}').storehours;
  }
  return hoursOfOperation;
};

const getStoreType = (storeType) => {
  return STORE_TYPES[storeType] || (storeType === 'PLACE' && STORE_TYPES.RETAIL) || '';
};

const basicInfoId = (store) => {
  return (
    store.uniqueID ||
    store.uniqueId ||
    store.storeLocId ||
    store.storeUniqueID ||
    store.stLocId
  ).toString();
};

const getIsDefault = (preferredStore) => {
  return preferredStore || false;
};

const getStoreAddress = (store) => {
  return store.address1 || store.streetLine1 || (store.addressLine && store.addressLine[0]);
};

const getState = (store) => {
  return store.stateOrProvinceName || store.state;
};

const getZipcode = (store) => {
  return (store.zipCode || store.postalCode || store.postalcode || store.zipcode).toString().trim();
};

const getStoreName = (store) => {
  return (
    (store.description && store.description.displayStoreName) ||
    (store.Description && store.Description[0] && store.Description[0].displayStoreName) ||
    store.storeName ||
    store.name ||
    ''
  );
};
const getBasicInfo = (store, isFromDefaultList) => {
  let phoneNumber = store.telephone1 || store.phone || store.phone1 || store.telephone || '';
  if (typeof phoneNumber === 'number') {
    phoneNumber = phoneNumber.toString();
  }
  let address = getStoreAddress(store);
  if (isFromDefaultList) {
    address = store.addressLine;
  }

  return {
    id: basicInfoId(store),
    storeName: sanitizeEntity(getStoreName(store)),
    isDefault: getIsDefault(store.preferredStore),
    address: {
      addressLine1: sanitizeEntity(address),
      city: store.city,
      state: getState(store),
      country: store.country,
      zipCode: getZipcode(store),
    },
    phone: formatPhoneNumber(phoneNumber),
    coordinates: {
      lat: parseFloat(store.latitude),
      long: parseFloat(store.longitude),
    },
  };
};

const getProductAvailability = (store, altProductAvailability1, requestedQuantity) => {
  return store.itemAvailability && store.itemAvailability[0]
    ? {
        skuId: store.itemAvailability[0].itemId,
        status:
          store.itemAvailability[0].qty < requestedQuantity
            ? BOPIS_ITEM_AVAILABILITY.UNAVAILABLE
            : altProductAvailability1,
        quantity: store.itemAvailability[0].qty,
      }
    : {};
};

/** @function storeAPIParser
 *   @summary This is the main function that should be used when trying to parse backends apis that return store information
 *   @param {Object} store - The store object exactly how backend sends it in their API
 *   @param {Object} configs - options to apply, somtimes we want some conditionaly set values
 *   @param {Number} configs.requestedQuantity - if passed this will affect a store's product status flag if not passed we will take the as its status from the API, (itemAvailability.qty < requestedQuantity) ? AVAILABILITY.UNAVAILABLE : AVAILABILITY.OK
 */

function storeAPIParser(store, configs = { requestedQuantity: 0 }) {
  const { requestedQuantity } = configs;
  let addressLine;

  // Sometimes addressLine is returned as an array
  // Sometimes addressLine is returned as an object with numerical properties (WHY???)
  // If addressLine is object, convert to array
  if (shouldFetchAddressLine(store)) {
    addressLine = Object.keys(store.addressLine).map((key) => store.addressLine[key]);
  }

  // Sometimes storeType is explicitly defined
  // Sometimes storeType needs to be determined using address
  const storeType = fetchStoreType(store, addressLine);
  const hoursOfOperation = getHoursOfOperation(store);

  const distanceAlt = store.distanceFromUserToStore
    ? parseFloat(store.distanceFromUserToStore).toFixed(2)
    : null;

  const altProductAvailability2 =
    store.itemAvailability[0].itemStatus === 'UNAVAILABLE'
      ? BOPIS_ITEM_AVAILABILITY.UNAVAILABLE
      : BOPIS_ITEM_AVAILABILITY.LIMITED;

  const altProductAvailability1 =
    store.itemAvailability[0].itemStatus === 'AVAILABLE'
      ? BOPIS_ITEM_AVAILABILITY.AVAILABLE
      : altProductAvailability2;

  // Parse Store Info
  const storeFilteredInfo = {
    // Boss store info
    storeBossInfo: {
      isBossEligible: store.isStoreBOSSEligible,
      startDate: store.bossMinDate,
      endDate: store.bossMaxDate,
    },
    /** added storeType | also checking if the flag is undefined than the value should be true
     * for default searching without any restriction
     */
    pickupType: {
      isStoreBossSelected:
        store.isStoreBossSelected !== undefined ? store.isStoreBossSelected : true,
      isStoreBopisSelected:
        store.isStoreBopisSelected !== undefined ? store.isStoreBopisSelected : true,
    },
    distance: store.distance ? parseFloat(store.distance).toFixed(2) : distanceAlt,
    basicInfo: getBasicInfo(store),
    hours: {
      regularHours: [],
      holidayHours: [],
      regularAndHolidayHours: [],
    },
    features: {
      storeType: getStoreType(storeType),
      mallType: store.x_mallType,
      entranceType: store.x_entranceType,
    },
    productAvailability: getProductAvailability(store, altProductAvailability1, requestedQuantity),
  };

  // Parse Store Hours
  if (hoursOfOperation && hoursOfOperation.length) {
    storeFilteredInfo.hours.regularHours = parseStoreHours(hoursOfOperation);
  }

  return storeFilteredInfo;
}

function defaultStoresAPIParser(store, defaultStoreId) {
  if (!store || !store.properties) {
    return {};
  }
  const storeObject = store.properties;
  const tileQueryObj = storeObject.tilequery;

  let addressLine;
  if (shouldFetchAddressLine(storeObject)) {
    addressLine = Object.keys(store.addressLine).map((key) => store.addressLine[key]);
  }
  const storeType = fetchStoreType(storeObject, addressLine);
  const hoursOfOperation = getHoursOfOperation(JSON.parse(storeObject?.storehours), true);
  const distanceAlt = null;
  // Parse Store Info
  const storeFilteredInfo = {
    // Boss store info
    storeBossInfo: {
      isBossEligible: true,
      startDate: '',
      endDate: '',
    },

    /** added storeType | also checking if the flag is undefined than the value should be true
     * for default searching without any restriction
     */
    pickupType: {
      isStoreBossSelected: true,
      isStoreBopisSelected: true,
    },
    distance: tileQueryObj.distance ? parseFloat(tileQueryObj.distance).toFixed(2) : distanceAlt,
    basicInfo: getBasicInfo(storeObject, true),
    hours: {
      regularHours: [],
      holidayHours: [],
      regularAndHolidayHours: [],
    },
    features: {
      storeType: getStoreType(storeType),
      mallType: store.x_mallType,
      entranceType: store.x_entranceType,
    },
    productAvailability: {},
  };
  if (
    defaultStoreId &&
    storeObject.storeLocId &&
    Number(storeObject.storeLocId) === Number(defaultStoreId)
  ) {
    storeFilteredInfo.basicInfo = { ...storeFilteredInfo.basicInfo, isDefault: true };
  }

  // Parse Store Hours
  if (hoursOfOperation && hoursOfOperation.length) {
    storeFilteredInfo.hours.regularHours = parseStoreHours(hoursOfOperation);
  }

  return storeFilteredInfo;
}

export const submitGetBopisSearchByLatLng = ({ locationPromise }) => {
  return locationPromise.then((location) => {
    try {
      let errorMessage = '';
      if (location) {
        if (location.error === ERROR_MESSAGES_BOPIS.zeroResults) {
          return { errorMessage: ERROR_MESSAGES_BOPIS.noAddressFound };
        }
        // Validation for scenarios where location is coming as {} and code is breaking
        const { country } = location;
        if (typeof country === 'undefined') {
          return { location, errorMessage: '' };
        }
        // Validation to check if search is for same country, else show error message.
        if (country.toLowerCase() === 'us' && isCanada()) {
          errorMessage = ERROR_MESSAGES_BOPIS.caPostalCode;
        } else if (country.toLowerCase() === 'ca' && !isCanada()) {
          errorMessage = ERROR_MESSAGES_BOPIS.usZipCode;
        }
        return { location, errorMessage };
      }
      return { errorMessage: ERROR_MESSAGES_BOPIS.apiTimeoutError };
    } catch (e) {
      return { location, errorMessage: e };
    }
  });
};

const getErrorFromResponse = (err) => {
  let apiError = '';
  if (err && err === ERROR_MESSAGES_BOPIS.zeroResults) {
    apiError = { error: ERROR_MESSAGES_BOPIS.noAddressFound };
  }
  if (err && err.errors) {
    apiError = { error: err.errors };
  }
  return apiError;
};

export const getDefaultStoresPlusInventorybyLatLng = ({ lat, lng, defaultStore }) => {
  let defaultStoreId = null;
  if (defaultStore && defaultStore.basicInfo) {
    defaultStoreId = defaultStore.basicInfo.id;
  }
  const DEFAULT_RADIUS = 75;
  const apiUrl = `${
    getAPIConfig().mapboxStoreLocatorApiURL
  }/childrensplace.store-locator/tilequery/${lng},${lat}.json?radius=${
    DEFAULT_RADIUS * 1000
  }&limit=${25}&access_token=${getAPIConfig().mapBoxApiKey}`;
  const webserviceInfo = {
    URI: apiUrl,
    method: 'GET',
  };

  const payload = {
    webService: webserviceInfo,
  };

  let apiError = { error: ERROR_MESSAGES_BOPIS.noAddressFound };
  return executeExternalAPICall(payload)
    .then((res) => {
      const stores = res.body.features || [];
      if (stores && stores.length) {
        return {
          stores: stores.map((store) => defaultStoresAPIParser(store, defaultStoreId)),
        };
      }
      apiError = { error: ERROR_MESSAGES_BOPIS.noAddressFound };
      return { error: apiError.error, stores: [] };
    })
    .catch((err) => {
      apiError = getErrorFromResponse(err);
    });
};

/**
 * @function getStoresPlusInventorybyLatLng
 * @summary
 */
export const getStoresPlusInventorybyLatLng = ({
  skuId,
  quantity,
  distance,
  lat,
  lng,
  country,
  variantId,
}) => {
  const countryCode =
    !country || country === undefined || country === 'PR' || country === 'pr' ? 'US' : country;
  const payload = {
    header: {
      latitude: lat,
      longitude: lng,
      catentryId: skuId,
      country: countryCode,
      sType: 'BOPIS',
      itemPartNumber: variantId,
    },
    body: {
      latitude: lat,
      longitude: lng,
      catentryId: skuId,
      distance: distance || 75,
      country: countryCode,
      sType: 'BOPIS',
      itemPartNumber: variantId,
    },
    webService: endpoints.getStoreandProductInventoryInfo,
  };
  let apiError = { error: ERROR_MESSAGES_BOPIS.noAddressFound };
  return executeStatefulAPICall(payload)
    .then((res) => {
      const stores = res.body && res.body.result;
      if (stores && stores.length) {
        return {
          stores: stores.map((store) => storeAPIParser(store, { requestedQuantity: quantity })),
        };
      }
      apiError = { error: ERROR_MESSAGES_BOPIS.noAddressFound };
      return { error: apiError.error, stores: [] };
    })
    .catch((err) => {
      if (err && err === ERROR_MESSAGES_BOPIS.zeroResults) {
        // If google geo location is not able to find locations related to address search then responds with ZERO_RESULTS message
        apiError = { error: ERROR_MESSAGES_BOPIS.noAddressFound };
      } else if (err && err.errors) {
        apiError = { error: err.errors };
      }
    });
};

export const getCartStoresPlusInventory = ({ skuId, quantity, variantId }) => {
  const payload = {
    header: {
      catentryId: skuId,
      itemPartNumber: variantId,
    },
    webService: endpoints.getUserCartStoresAndInventory,
  };
  return executeStatefulAPICall(payload)
    .then((res) => {
      if (responseContainsErrors(res)) {
        throw new ServiceResponseError(res);
      } else {
        const stores = res.body.response;
        if (stores && stores.length) {
          return stores.map((store) => storeAPIParser(store, { requestedQuantity: quantity }));
        }
        return [];
      }
    })
    .catch((err) => {
      throw getFormattedError(err);
    });
};
