// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig, isClient } from '@tcp/core/src/utils';
import { getAlternateBrandName } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import { executeUnbxdAPICall, executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import { hashValuesReplace, utilPdpArrayHeader } from '../../../utils/utils';
import { parseProductFromAPI } from './productDetail';
import logger from '../../../utils/loggerInstance';
import { attributeListMaker, extractAttributeValue } from './productParser';
import processHelpers from './processHelpers';

const getProductId = (product) => {
  const productIdAlternate =
    product.prodpartno.indexOf('_') > -1 ? product.prodpartno.split('_')[0] : product.prodpartno;
  return product.prodpartno.indexOf('-') > -1
    ? product.prodpartno.split('-')[0]
    : productIdAlternate;
};

const getGroupingVal = (product, productId) => {
  const grpVal = extractAttributeValue(
    product,
    processHelpers.getProductAttributes().bundleGrouping
  );
  return (grpVal && grpVal.toLowerCase()) || productId;
};

const processBundleResponse = (res, getImgPath, breadCrumbs, navigationTree, state) => {
  const groupedResponse = {};
  const productIds = [];

  res.body.response.products.forEach((productItem) => {
    const product = productItem;
    const productId = getProductId(product);
    product.list_of_attributes = product.list_of_attributes
      ? attributeListMaker(product.list_of_attributes)
      : {};
    const groupingVal = getGroupingVal(product, productId);
    if (groupedResponse[groupingVal]) {
      groupedResponse[groupingVal].push(product);
    } else {
      groupedResponse[groupingVal] = [product];
      productIds.push(groupingVal);
    }
  });
  const formattedBundleProducts = productIds.map((bundleProduct) => {
    return parseProductFromAPI(
      groupedResponse[bundleProduct],
      groupedResponse[bundleProduct][0].seo_token,
      getImgPath,
      breadCrumbs,
      false,
      true,
      navigationTree,
      state
    ).product;
  });

  return formattedBundleProducts.map((bundleProduct) => {
    const availableFilter = [];
    const setFilters = (filter) => {
      if (filter && !availableFilter.includes(filter)) {
        availableFilter.push(filter.toLowerCase());
      }
    };

    bundleProduct.colorFitsSizesMap.forEach((product) => {
      if (product.familyType && product.familyType.includes('|')) {
        product.familyType.split('|').forEach((filter) => {
          setFilters(filter);
        });
      } else setFilters(product.familyType);
    });
    return {
      products: bundleProduct,
      availableFilters: availableFilter,
    };
  });
};

const invokePdpBundleProductsApi = (
  productPartNumbers,
  getImgPath,
  breadCrumbs,
  navigationTree,
  state
) => {
  const apiConfig = getAPIConfig();
  const productNumber = productPartNumbers.join(',');
  const alternateBrand = getAlternateBrandName(state);
  if (apiConfig) {
    const pdpEndpoint = endpoints && endpoints.pdpProductApi;
    const fetchReviewURL = hashValuesReplace(
      pdpEndpoint.URI,
      utilPdpArrayHeader(productNumber, {
        brand:
          (alternateBrand && alternateBrand.toUpperCase()) ||
          (apiConfig && apiConfig.brandId && apiConfig.brandId.toUpperCase()),
        lang: apiConfig && apiConfig.language && apiConfig.language.toUpperCase(),
        site: apiConfig && apiConfig.siteId && apiConfig.siteId.toUpperCase(),
      })
    );

    const formattedPdpApiURL = `${fetchReviewURL}`;

    const payload = {
      webService: {
        URI: formattedPdpApiURL,
        method: pdpEndpoint && pdpEndpoint.method,
      },
      skipApiIdentifier: true,
    };

    return executeStatefulAPICall(payload)
      .then((res) => {
        return processBundleResponse(res, getImgPath, breadCrumbs, navigationTree, state);
      })
      .catch((err) => {
        logger.error('error: ', err);
      });
  }
  return null;
};
/**
 * @function getBundleProductsDetails
 * @summary This API is used for bundles.
 */
const getBundleProductsDetails = (
  xappURLConfig,
  { bundleProducts, getImgPath, breadCrumbs, navigationTree },
  state,
  isPdpProductServiceEnabled,
  enablePDPServiceABTest,
  brand
) => {
  const fields =
    'productimage,alt_img,style_partno,swatchimage,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant, low_offer_price, high_offer_price,low_list_price,high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,v_unbxd_added_variant';
  const payload = {
    body: {
      variants: true,
      'variants.count': 100,
      version: 'V2',
      rows: bundleProducts.length,
      pagetype: 'boolean',
      id: bundleProducts.join(','),
      promotion: false,
      fields,
      brand,
    },
    webService: endpoints && endpoints.getProductDetails,
  };

  if (
    (isClient() && isPdpProductServiceEnabled && enablePDPServiceABTest) ||
    (!isClient() && isPdpProductServiceEnabled)
  ) {
    return invokePdpBundleProductsApi(
      bundleProducts,
      getImgPath,
      breadCrumbs,
      navigationTree,
      state
    );
  }
  return executeUnbxdAPICall(payload, null, xappURLConfig)
    .then((res) => {
      return processBundleResponse(res, getImgPath, breadCrumbs, navigationTree, state);
    })
    .catch((err) => {
      logger.error('error: ', err);
    });
};

export default getBundleProductsDetails;
