// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable sonarjs/cognitive-complexity */
import logger from '@tcp/core/src/utils/loggerInstance';
import layoutAbstractor from '@tcp/core/src/services/abstractors/bootstrap/layout';
import { executeUnbxdAPICall } from '../../handler';

import endpoints from '../../endpoints';
import utils, {
  bindAllClassMethodsToThis,
  isMobileApp,
  isCanada,
  getAPIConfig,
} from '../../../utils';
import processHelpers from './processHelpers';
import { PRODUCTS_PER_LOAD } from '../../../components/features/browse/ProductListing/container/ProductListing.constants';
import processResponse from './processResponse';

const apiHelper = {
  configOptions: {
    isUSStore: !isCanada(),
    siteId: utils.getSiteId(),
  },
};

const isOutfit = (isSearch, searchTerm) => {
  return !isSearch && searchTerm && searchTerm.indexOf('-outfit') > -1;
};

const getStart = (startProductCount, pageNumber, productCount) => {
  // We will be sending the start to getCategoryListingPage function in the bucketing scenario and we need to send that in UNBXD api.
  // Falsy check has not been placed as i need to send start 0 in L2 call in case of bucketing sequence.
  return startProductCount !== undefined
    ? startProductCount
    : (pageNumber - 1) * (productCount || PRODUCTS_PER_LOAD);
};

const isNoUnbxdLogic = (shouldApplyUnbxdLogic, isUnbxdSequencing) => {
  return !shouldApplyUnbxdLogic && !isUnbxdSequencing;
};

const isNoBucketing = (bucketingSeqConfig) => {
  return (
    bucketingSeqConfig &&
    bucketingSeqConfig.bucketingSeq &&
    bucketingSeqConfig.requiredChildren.length
  );
};

const getqParam = (searchTerm) => {
  let getDecode = searchTerm.indexOf('?') > 0 ? searchTerm.split('?')[0] : searchTerm;
  getDecode = isMobileApp() ? getDecode : escape(getDecode);
  return getDecode || '*';
};

const validateStartVal = (start) => {
  return !Number.isNaN(start) ? start : 0;
};

class ProductsDynamicAbstractor {
  constructor() {
    this.apiHelper = apiHelper;
    this.unbxdId = null;
    this.cachedFilters = null;
    this.cachedCount = 0;
    // create this-bound varsions of all methods of this class
    bindAllClassMethodsToThis(this);
  }

  /**
   * @function extractFilters
   * @summary To create UNBXD facets api query string from all selected facets
   * @param {object} filtersAndSort - selected filters and values object
   */
  extractFilters = (filtersAndSort) => {
    const filterQuery = {};
    let query = '';
    const facetKeys = Object.keys(filtersAndSort);
    facetKeys.forEach((facetKey) => {
      let facetValue = filtersAndSort[facetKey];
      if (
        processHelpers.isUnbxdFacetKey(facetKey) &&
        facetValue &&
        facetValue.length > 0 &&
        facetKey.indexOf('uFilter') > -1
      ) {
        facetValue = facetValue.map((facet) => {
          const encodedFacet = encodeURIComponent(facet);
          const encodedFacetWithQuote = encodeURIComponent(`"${facet}"`);
          return isMobileApp()
            ? `${facetKey}:${encodedFacetWithQuote}`
            : `${facetKey}:"${encodedFacet}"`;
        });

        query += this.getModifiedQueryString(facetValue, query);
      } else if (facetKey === 'bopisStoreId') {
        query += this.getModifiedQueryString(facetValue, query);
      }
    });

    if (query !== '') filterQuery.filter = query;
    return filterQuery;
  };

  // Function for getting encoded query for mobile device for solving the ios issue.
  getModifiedQueryString = (facetValue, query) => {
    if (isMobileApp()) {
      return facetValue.length > 0
        ? (query ? '&filter=' : '') + facetValue.join(encodeURI(' OR '))
        : '';
    }

    return facetValue.length > 0 ? (query ? '&filter=' : '') + facetValue.join(' OR ') : '';
  };

  // PLP to PDP then again back to PLP, maintainig autoscroll position by managing state with products count
  getSetAPIProductsCount = () => {
    // if totalProducts are greater than PRODUCTS_PER_LOAD limit it to PRODUCTS_PER_LOAD and update sessionStorage for auto scroll
    return PRODUCTS_PER_LOAD;
    // TODO - fix this function when caching is required for navigating back from PDP
  };

  /**
   * @function cacheFiltersAndCount DTN:6592, In bucekting scenario we make L2 call first to fetch the facets and the count,
   *           we need to cache them as we wont be asking  for these paramters in subsequent L3 calls.
   * @param {Object} filters The facets of the L2.
   * @param {Array} availableL3InFilter Available l3 in the current L2 which has been clicked.
   * @return {Number} the number of products in an L2.
   */

  cacheFiltersAndCount = () => {
    // TODO - fix this function when required to navigate back from PDP
  };

  handleValidationError = (e) => {
    logger.error(e);
  };

  getPlpOrSlpEndpoint = (isSearch) => {
    return isSearch ? endpoints.getProductsBySearchTerm : endpoints.getProductviewbyCategory;
  };

  getLengths = (state) => {
    if (!state.SearchListingPage) {
      return 0;
    }
    const loadedProductsPageData =
      state.SearchListingPage &&
      state.SearchListingPage.loadedProductsPages.map((a) => {
        return a.length;
      });

    return (
      loadedProductsPageData &&
      loadedProductsPageData.length > 0 &&
      loadedProductsPageData.reduce((a, b) => a + b, 0)
    );
  };

  getTotalProductsCount = (state) => {
    return state.SearchListingPage ? state.SearchListingPage.totalProductsCount : 0;
  };

  isMoreProductsApiCalled = (isSearch, pageNumber, state) => {
    if (isSearch && pageNumber >= 2) {
      const loadedProductsPagesLength = this.getLengths(state);
      const totalProductsCount = this.getTotalProductsCount(state);
      if (isSearch && totalProductsCount <= loadedProductsPagesLength) {
        return false;
      }
    }
    return true;
  };

  /**
   * @function parsedModuleData Parses the module and layout data
   * @param {Object} promoCombination Promotion Data to be parsed
   * @return {Object} layout and module parsed data
   */
  // eslint-disable-next-line no-unused-vars
  parsedModuleData = async (promoCombination, apiConfig, loadedModules) => {
    const moduleObjects = [];
    const slotsObject = {};
    let modules = {};
    const promoCombinationValues = (promoCombination && promoCombination.val) || {};
    try {
      const { language } = apiConfig;
      Object.keys(promoCombinationValues).forEach((slotType) => {
        promoCombinationValues[slotType].forEach((slot) => {
          /** Supports both older and newer impl. */
          if (slot.val.cid) {
            const moduleData = {
              name: slot.sub,
              moduleName: slot.val.sub,
              contentId: slot.val.cid,
              set: slot.set || {},
            };
            if (!loadedModules || loadedModules.indexOf(moduleData.contentId) < 0) {
              moduleObjects.push({
                name: moduleData.moduleName,
                data: {
                  contentId: moduleData.contentId,
                  name: moduleData.name,
                  slotNo: moduleData.slotNo,
                  slot: moduleData.slotNo,
                  lang: language !== 'en' ? language : '',
                },
              });
            }
            if (!Array.isArray(slotsObject[slotType])) {
              slotsObject[slotType] = [];
            }
            slotsObject[slotType].push(moduleData);
          } else if (Array.isArray(slot.val)) {
            /** Newer Implementation based on userType in in-grid slots */
            const slotNo = slot.sub;
            slot.val.forEach((subSlot) => {
              const moduleData = {
                slotNo,
                name: subSlot.sub,
                moduleName: subSlot.val?.sub,
                contentId: subSlot.val?.cid,
                set: subSlot.set || {},
              };
              if (!loadedModules || loadedModules.indexOf(moduleData.contentId) < 0) {
                moduleObjects.push({
                  name: moduleData.moduleName,
                  slotNo: moduleData.slotNo,
                  data: {
                    contentId: moduleData.contentId,
                    name: moduleData.name,
                    slotNo: moduleData.slotNo,
                    slot: `${moduleData.slotNo}_${moduleData.name}`,
                    lang: language !== 'en' ? language : '',
                  },
                });
              }
              if (!Array.isArray(slotsObject[slotType])) {
                slotsObject[slotType] = [];
              }
              slotsObject[slotType].push(moduleData);
            });
          }
        });
      });

      if (moduleObjects.length) {
        modules = await this.moduleResolver(moduleObjects, apiConfig);
      }
    } catch (err) {
      this.handleValidationError(err);
    }
    return {
      modules,
      layout: slotsObject,
    };
  };

  moduleResolver = async (moduleObjects, apiConfig) => {
    const response = await layoutAbstractor.getModulesData(moduleObjects, apiConfig);
    return layoutAbstractor.processModuleData(response.data);
  };

  // eslint-disable-next-line complexity
  getProducts = (
    reqObj,
    state,
    prefetchedResponse,
    productCount,
    xappURLConfig,
    isNonInvABTestEnabled,
    isSortCodeNeeded
  ) => {
    const reqObject =
      prefetchedResponse && prefetchedResponse.reqObj ? prefetchedResponse.reqObj : reqObj;
    const {
      seoKeywordOrCategoryIdOrSearchTerm,
      isSearch,
      filtersAndSort = {},
      pageNumber,
      getImgPath,
      categoryId,
      breadCrumbs,
      bucketingSeqConfig,
      getFacetSwatchImgPath,
      isUnbxdSequencing,
      excludeBadge,
      startProductCount,
      numberOfProducts,
      cacheFiltersAndCount,
      extraParams,
      shouldApplyUnbxdLogic,
      hasShortImage,
      location,
      filterMaps,
      isLazyLoading,
      navigation,
      categoryFilterArray,
      categoryNameList,
      brand,
    } = reqObject;
    const searchTerm = decodeURIComponent(seoKeywordOrCategoryIdOrSearchTerm);
    const { sort = null } = filtersAndSort;
    const facetsPayload = this.extractFilters(filtersAndSort);
    const isOutfitPage = isOutfit(isSearch, searchTerm);
    // We will be sending the rows to getCategoryListingPage function in the bucketing scenario and we need to send that in UNBXD api.
    // Falsy check has not been placed as i need to send row 0 in L2 call in case of bucketing sequence.
    const row =
      numberOfProducts !== undefined
        ? numberOfProducts
        : productCount || this.getSetAPIProductsCount();
    const start = getStart(startProductCount, pageNumber, productCount); // In UNBXD start is from zero but seo paging starts with 1
    const isMoreProductsApiCalled = this.isMoreProductsApiCalled(isSearch, pageNumber, state);

    if (!isMoreProductsApiCalled) {
      return false;
    }
    const apiConfigObj = getAPIConfig();
    const { isNonInvStagEnabled } = apiConfigObj || {};
    let payload;
    const categoryPathArray = categoryId && categoryId.split('>');
    const categoryPathArrayLength = categoryPathArray && categoryPathArray.length;
    const fieldsVal =
      'related_product_swatch_images,productimage,alt_img,style_partno,giftcard,swatchimage,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,low_offer_price, high_offer_price, low_list_price, high_list_price,TCPStyleTypeUS,TCPStyleTypeCA,TCPStyleQTYCA,TCPStyleQTYUS';
    if (!(isNonInvABTestEnabled && isNonInvStagEnabled)) {
      payload = {
        body: {
          ...facetsPayload,
          ...extraParams,
          start: validateStartVal(start),
          rows: row,
          variants: true,
          'variants.count': 0,
          version: 'V2',
          'facet.multiselect': true,
          selectedfacet: true,
          fields: fieldsVal,
          brand,
        },
        webService: this.getPlpOrSlpEndpoint(isSearch),
      };
    }
    if (
      isNonInvStagEnabled &&
      isNonInvABTestEnabled &&
      isSortCodeNeeded &&
      categoryPathArray &&
      categoryPathArrayLength >= 1
    ) {
      payload = {
        body: {
          ...facetsPayload,
          ...extraParams,
          start: validateStartVal(start),
          rows: row,
          variants: true,
          'variants.count': 0,
          version: 'V2',
          'facet.multiselect': true,
          selectedfacet: true,
          promotion: false,
          sort: `sort_${categoryPathArray[categoryPathArrayLength - 1]} asc`,
          fields: fieldsVal,
          brand,
        },

        webService: this.getPlpOrSlpEndpoint(isSearch),
      };
    } else if (isNonInvStagEnabled && isNonInvABTestEnabled) {
      payload = {
        body: {
          ...facetsPayload,
          ...extraParams,
          start: validateStartVal(start),
          rows: row,
          variants: true,
          'variants.count': 0,
          version: 'V2',
          'facet.multiselect': true,
          selectedfacet: true,
          promotion: false,
          fields: fieldsVal,
          brand,
        },
        webService: this.getPlpOrSlpEndpoint(isSearch),
      };
    }

    if (!isSearch) {
      payload.body.pagetype = 'boolean';
      if (categoryId) {
        payload.body['p-id'] = `categoryPathId:"${categoryId}"`;
      }
    }

    // If the current case is of bucketing scenario then we need to send facet as false in L3 call as we will be getting the same in L2 call.
    if (bucketingSeqConfig.bucketingRequired) {
      payload.body.facet = false;
    }
    /* Checking if we need to do bucketing or not. Bucketing is done only for those l2 levels that have a further L3. Only in that secnario we send sort
        paramter otherwise sending sort paramter in all other scenarios break the call */
    if (
      isNoUnbxdLogic(shouldApplyUnbxdLogic, isUnbxdSequencing) &&
      isNoBucketing(bucketingSeqConfig)
    ) {
      payload.body.sort = bucketingSeqConfig.bucketingSeq;
    }
    if (isSearch) {
      /* ----- Input is being encoded while entered this is causing an issue with superagent ---- */
      payload.body.q = getqParam(searchTerm);
    }
    if (sort) payload.body.sort = sort;

    if (prefetchedResponse) {
      return processResponse(prefetchedResponse, state, {
        isSearch,
        breadCrumbs,
        shouldApplyUnbxdLogic,
        cacheFiltersAndCount,
        getFacetSwatchImgPath,
        filtersAndSort,
        bucketingSeqConfig,
        getImgPath,
        excludeBadge,
        hasShortImage,
        isOutfitPage,
        searchTerm,
        sort,
        location,
        filterSortView: Object.keys(filtersAndSort).length > 0,
        filterMaps,
        isLazyLoading,
        navigation,
        categoryFilterArray,
        isPrefetched: true,
        brand,
      });
    }
    return executeUnbxdAPICall(payload, null, xappURLConfig)
      .then((res) =>
        processResponse(res, state, {
          isSearch,
          breadCrumbs,
          shouldApplyUnbxdLogic,
          cacheFiltersAndCount,
          getFacetSwatchImgPath,
          filtersAndSort,
          bucketingSeqConfig,
          getImgPath,
          excludeBadge,
          hasShortImage,
          isOutfitPage,
          searchTerm,
          sort,
          location,
          filterSortView: Object.keys(filtersAndSort).length > 0,
          filterMaps,
          isLazyLoading,
          navigation,
          categoryFilterArray,
          categoryNameList,
          brand,
        })
      )
      .catch((err) => {
        // if (err && ((err.status >= 400 && err.status <= 404) || err.status === 500) && isClient()) {
        // TODO - handle it - window.location.href = getErrorPagePath(this.apiHelper._configOptions.siteId);
        // }
        this.handleValidationError(err);
        // TODO - handle it - throw this.apiHelper.getFormattedError(err);
      });
  };

  getExtraFilterParams = (isFilterAbTestOn) => {
    const extraParams = {};
    if (isFilterAbTestOn) {
      extraParams['facet.field'] = 'v_tcp_groupedsize_uFilter';
      extraParams['f.v_tcp_groupedsize_uFilter.facet.limit'] = 1000;
    }
    return { ...extraParams };
  };
}

export default ProductsDynamicAbstractor;
