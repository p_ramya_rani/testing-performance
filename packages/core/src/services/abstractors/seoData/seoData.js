// 9fbef606107a605d69c0edbcd8029e5d 
import logger from '@tcp/core/src/utils/loggerInstance';
import handler from '../../handler';

/**
 * Abstractor layer for loading data from API for SEO data
 */
const Abstractor = {
  getData: (module, data, apiConfig) => {
    return handler
      .fetchModuleDataFromGraphQL({ name: module, data }, apiConfig)
      .then(response => {
        return response.data.seoData;
      })
      .then(Abstractor.processData)
      .catch(Abstractor.handleError);
  },
  processData: data => {
    const result = {};
    if (data) {
      data.forEach(({ path, ...rest }) => {
        const seoDataKey = path.split('/')[1];
        result[seoDataKey] = { ...rest };
      });
    }
    return result;
  },
  handleError: e => {
    logger.error(e);
    return {};
  },
};
export default Abstractor;
