// 9fbef606107a605d69c0edbcd8029e5d
import logger from '@tcp/core/src/utils/loggerInstance';
import { parseDate, compareDate } from '@tcp/core/src/utils/parseDate';
import {
  getPromotionType,
  BUTTON_LABEL_STATUS,
  constructDateFormat,
  getCouponType,
  COUPON_STATUS,
  COUPON_REDEMPTION_TYPE,
} from '@tcp/core/src/services/abstractors/CnC/CartItemTile';
import { isCanada } from '../../../utils';
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';

export const getItemLevelRefund = (payload) => {
  const requestPayload = {
    body: payload,
    webService: endpoints.getItemLevelRefund,
  };
  return executeStatefulAPICall(requestPayload)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      logger.info('Item level api failed', err);
      return err && err.errorResponse;
    });
};
export const getShipmentStatus = (payload) => {
  const requestPayload = {
    body: payload,
    webService: endpoints.getShipmentStatus,
  };
  return executeStatefulAPICall(requestPayload)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      logger.info('getShipmentStatus api failed', err);
      return err && err.errorResponse;
    });
};

export const getRefundStatus = ({
  orderId,
  orderPlacedDate,
  shippingMethod,
  orderDeliveredDate,
}) => {
  const requestPayload = {
    header: {
      orderId,
      orderPlacedDate,
      orderDeliveredDate: orderDeliveredDate || '',
      shippingMethod: shippingMethod.toUpperCase(),
    },
    webService: {
      ...endpoints.getRefundStatus,
      URI: `${endpoints.getRefundStatus.URI}/${orderId}`,
    },
  };
  return executeStatefulAPICall(requestPayload)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      logger.info('getRefundStatus api failed', err);
      return err;
    });
};

export const getCancelStatus = ({ orderNumber, loggedInUserEmail }) => {
  const requestPayload = {
    body: {
      orderId: orderNumber,
      emailId: loggedInUserEmail,
    },
    webService: endpoints.getCancelStatus,
  };
  return executeStatefulAPICall(requestPayload)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      logger.info('getCancelStatus api failed', err);
      return err;
    });
};

export default getItemLevelRefund;

export const getShipmentDelayedRefundStatus = ({ orderId, emailId, reasonCode }) => {
  const requestPayload = {
    body: {
      orderId,
      emailId,
      reasonCode,
    },
    webService: endpoints.getShipmentDelayedRefundStatus,
  };
  return executeStatefulAPICall(requestPayload)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      logger.info('getShipmentDelayedRefundStatus api failed', err);
      return err;
    });
};

const getFormattedError = (err) => {
  const error = {};
  if (err?.response?.body && err?.response?.body?.errors instanceof Array) {
    error.errorMessage = err.response.body.errors[0].errorMessage;
    return error;
  }
  return error;
};

export const mergeAccountValidateMember = (payload) => {
  const payloadData = {
    webService: endpoints.mergeAccountValidateMemberWithLocking,
    body: payload,
  };
  return executeStatefulAPICall(payloadData)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      logger.info('mergeAccountValidateMemberWithLocking api failed', err);
      throw getFormattedError(err);
    });
};

export const initiateReturn = (payload, isLoggedIn) => {
  const payloadData = {
    webService: endpoints.initiateReturn,
    body: payload,
    header: {
      ...(isCanada() && {
        companyId: 2,
      }),
      isLoggedInUser: isLoggedIn || false,
    },
  };
  return executeStatefulAPICall(payloadData)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      logger.info('initiateReturn api failed', err);
      let error = {};
      if (err?.response?.body) {
        error = err.response.body;
        return error;
      }
      return err;
    });
};

export const getOtpDetails = (payloadInfo) => {
  const { payload } = payloadInfo;
  const payloadData = {
    webService: endpoints.mergeAccountGenerateOTP(payload),
  };

  return executeStatefulAPICall(payloadData)
    .then((res) => res)
    .catch((err) => {
      throw err;
    });
};

export const checkOtpDetails = (payload) => {
  const payloadData = {
    webService: endpoints.mergeAccountValidateOTP,
    body: payload,
  };
  return executeStatefulAPICall(payloadData)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      logger.info('validate otp api failed', err);
      throw err;
    });
};

/**
 * @function constructSingleCouponStructure This function parses the coupon got from API
 *                                    to the desired format to be used by FE code
 * @param couponData {} The coupon details returnde by API
 * @return coupon {} coupon object used to render data on FE
 */
export const constructSingleCouponStructure = (couponData) => {
  const now = new Date();
  const oneDay = 24 * 60 * 60 * 1000;
  const expirationThreshold = 7;

  let labelStatus = couponData.isApplied ? BUTTON_LABEL_STATUS.REMOVE : BUTTON_LABEL_STATUS.APPLY;
  if (couponData.isOnHold === 'true') {
    labelStatus = BUTTON_LABEL_STATUS.REMOVE;
  }
  const startDate = parseDate(couponData.startDate);
  const endDate = parseDate(couponData.endDate);
  const isPlaceCash =
    couponData.offerType === COUPON_REDEMPTION_TYPE.PLACECASH || couponData.offerType === 'PC';
  const isExpiring =
    Math.round(Math.abs((endDate.getTime() - now.getTime()) / oneDay)) <= expirationThreshold;
  return {
    status: couponData.isApplied ? COUPON_STATUS.APPLIED : COUPON_STATUS.AVAILABLE,
    isExpiring,
    title: couponData.description,
    detailsOpen: false,
    labelStatus,
    expirationDate: constructDateFormat(endDate),
    effectiveDate: constructDateFormat(startDate),
    legalText: couponData.legalText,
    isStarted: isPlaceCash ? compareDate(now, startDate) : true,
    offerType: getCouponType(couponData.offerType),
    id: couponData.code.toUpperCase(),
    error: '',
    redemptionType: COUPON_REDEMPTION_TYPE[couponData.offerType],
    promotionType: getPromotionType(couponData.offerType),
    expirationDateTimeStamp: endDate,
    isExpired: Math.round((endDate.getTime() - now.getTime()) / oneDay) < 0,
    isOnHold: couponData.isOnHold || 'false',
  };
};
