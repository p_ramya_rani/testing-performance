// 9fbef606107a605d69c0edbcd8029e5d 
import CustomerHelpAbstractor from '../customerHelp';
import * as handler from '../../../handler/handler';

jest.mock('../../../../service/API');
jest.mock('../../../handler/handler');

handler.executeExternalAPICall = jest.fn();
handler.executeExternalAPICall.mockImplementation(() => {
  return Promise.resolve({
    body: {
      status: 'accept_all',
    },
  });
});
it('CustomerHelpAbstractor Abstractor | processData', () => {
  const payload = {
    orderId: '123456',
    emailId: 'dummy@email.com',
    reasonCode: 'ITEM_MISSING',
    status: 'success',
    caseId: 'ID-1234',
    amountRefunded: '',
    items: [
      {
        lineNumber: '423242342',
        discountAmt: '4',
      },
      {
        lineNumber: '423242343',
        refundedAmt: '4',
      },
    ],
  };
  return CustomerHelpAbstractor('tcp@gmail.com').then(() => {
    expect(payload).toEqual(payload);
  });
});
