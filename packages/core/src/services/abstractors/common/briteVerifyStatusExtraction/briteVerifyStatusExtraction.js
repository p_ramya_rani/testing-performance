// 9fbef606107a605d69c0edbcd8029e5d
import endpoints from '@tcp/core/src/services/endpoints';
import { getCustomHeaders, isMobileApp } from '@tcp/core/src/utils';
import { executeExternalAPICall } from '../../../handler';

function briteVerifyStatusExtraction(emailAddress) {
  const { URI, method, reqTimeout, authKey = '' } = endpoints.emailVerification;
  const header = isMobileApp() ? getCustomHeaders() : {};
  const payload = {
    webService: {
      method,
      URI: `${URI}`,
      reqTimeout,
    },
    header: authKey.length > 0 ? { Authorization: `ApiKey: ${authKey}`, ...header } : header,
    body: {
      address: emailAddress,
    },
  };
  return executeExternalAPICall(payload)
    .then((res) => {
      const response = res.body;
      if (!response) {
        throw new Error('no_response::false:false');
      }
      return `${response.status}::false:false`;
    })
    .catch(() => 'no_response::false:false');
}

export default briteVerifyStatusExtraction;
