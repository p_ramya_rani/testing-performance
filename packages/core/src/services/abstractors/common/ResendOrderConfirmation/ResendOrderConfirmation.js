// 9fbef606107a605d69c0edbcd8029e5d 
import { executeStatefulAPICall } from '../../../handler';
import endpoints from '../../../endpoints';
/**
 * Abstractor to pass the email id for resending order confirmation
 */
const Abstractor = {
  /**
   * @param {Object} reqBody body to the API
   * @return {Object} return promise.
   */
  sendOrderEmail: reqBody => {
    const reqObj = {
      webService: endpoints.sendOrderEmail,
      body: {
        ...reqBody,
      },
    };

    return executeStatefulAPICall(reqObj)
      .then(Abstractor.processSubscriptionData)
      .catch(Abstractor.handleSubscriptionError);
  },
  /**
   * @param {Object} res response object
   * @return {Object} returns response object.
   */
  processSubscriptionData: res => {
    return res.body.response || res.body;
  },
  handleSubscriptionError: () => {
    return {};
  },
};
export default Abstractor;
