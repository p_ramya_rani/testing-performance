// 9fbef606107a605d69c0edbcd8029e5d 
const plpUrl =
  'https://www.childrensplace.com/us/c/girls-kids-shorts?icid=hp_s3a_imagenbutton_g_050519_shorts';
const fmt = 'Family Tees';
const mmt = 'Mom & Me Tees';
export default {
  contentId: 'moduleD',
  name: 'moduleD',
  type: 'module',
  set: [
    {
      key: 'ctaType',
      val: 'stackedCTAButtons',
    },
  ],
  composites: {
    headerText: [
      {
        textItems: [
          {
            text: 'Mini me shop',
            color: 'color2',
            style: 'style1',
          },
        ],
        link: {
          url: plpUrl,
          title: '',
          target: '',
        },
      },
    ],
    promoBanner: [
      {
        link: {
          url: 'http://example.com',
          title: '',
          target: '',
          external: 0,
        },
        textItems: [
          {
            text: 'ALL TEES',
            style: 'style1',
          },
          {
            text: '67% OFF',
            style: 'style2',
          },
        ],
      },
    ],
    smallCompImage: [
      {
        link: {
          url: plpUrl,
          title: fmt,
          target: '',
          external: 0,
        },
        image: {
          url:
            'https://res.cloudinary.com/tcp-dam-test/image/upload/v1558543115/ecom/assets/content/tcp/us/home/moduled/US-HP-050519-MINIME1_h9cwcd.jpg',
          title: fmt,
          alt: fmt,
        },
      },
      {
        link: {
          url: plpUrl,
          text: mmt,
          title: mmt,
          target: '',
          external: 0,
        },
        image: {
          url:
            'https://res.cloudinary.com/tcp-dam-test/image/upload/v1558543115/ecom/assets/content/tcp/us/home/moduled/US-HP-050519-MINIME3_vmfhnu.jpg',
          title: mmt,
          alt: mmt,
        },
      },
      {
        link: {
          url: plpUrl,
          text: fmt,
          title: fmt,
          target: '',
          external: 0,
        },
        image: {
          url:
            'https://res.cloudinary.com/tcp-dam-test/image/upload/v1558543114/ecom/assets/content/tcp/us/home/moduled/US-HP-050519-MINIME4_iuzwmp.jpg',
          title: fmt,
          alt: fmt,
        },
      },
      {
        link: {
          url: plpUrl,
          text: mmt,
          title: mmt,
          target: '',
          external: 0,
        },
        image: {
          url:
            'https://res.cloudinary.com/tcp-dam-test/image/upload/v1558543114/ecom/assets/content/tcp/us/home/moduled/US-HP-050519-MINIME2_uwtbdd.jpg',
          title: mmt,
          alt: mmt,
        },
      },
    ],
    ctaItems: [
      {
        image: {
          url:
            'https://res.cloudinary.com/tcp-dam-test/image/upload/q_auto:best/v1558543115/ecom/assets/content/tcp/us/home/moduled/US-HP-050519-MINIME1_h9cwcd.jpg',
          alt: 'Girl',
          title: 'Girl',
          crop_d: '',
          crop_t: '',
          crop_m: '',
        },
        button: {
          url: '/c/',
          text: 'Girl',
          title: 'Girl',
          target: '',
          external: 0,
        },
      },
      {
        image: {
          url:
            'https://res.cloudinary.com/tcp-dam-test/image/upload/q_auto:best/v1558543115/ecom/assets/content/tcp/us/home/moduled/US-HP-050519-MINIME1_h9cwcd.jpg',
          alt: 'Toddler Girl alt',
          title: 'Toddler Girl title',
          crop_d: '',
          crop_t: '',
          crop_m: '',
        },
        button: {
          url: '/c/',
          text: 'Toddler Girl',
          title: 'Toddler Girl',
          target: '',
          external: 0,
        },
      },
      {
        image: {
          url:
            'https://test5.childrensplace.com/image/upload/v1565680164/sarah-doody-x_XipCfA3Qc-unsplash_e38rjo.jpg',
          alt: 'Boy',
          title: 'Boy',
          crop_d: '',
          crop_t: '',
          crop_m: '',
        },
        button: {
          url: '/c/',
          text: 'Boy',
          title: 'Boy',
          target: '',
          external: 0,
        },
      },
      {
        image: {
          url:
            'https://test5.childrensplace.com/image/upload/v1565680141/alexander-dummer-x4jRmkuDImo-unsplash_ptu9ul.jpg',
          alt: 'Toddler Boy alt',
          title: 'Toddler Boy title',
          crop_d: '',
          crop_t: '',
          crop_m: '',
        },
        button: {
          url: '/c/',
          text: 'Toddler Boy',
          title: 'Toddler Boy',
          target: '',
          external: 0,
        },
      },
      {
        image: {
          url:
            'https://test5.childrensplace.com/image/upload/v1565680140/christian-fickinger-MDIGo4Ez-0g-unsplash_jkpwgf.jpg',
          alt: 'Baby',
          title: 'Baby',
          crop_d: '',
          crop_t: '',
          crop_m: '',
        },
        button: {
          url: '/c/',
          text: 'Baby',
          title: 'Baby',
          target: '',
          external: 0,
        },
      },
    ],
    singleCTAButton: {
      url: plpUrl,
      target: '',
      text: 'SHOP ALL',
      title: 'SHOP ALL',
      external: 0,
    },
  },
};
