const monetateConstant = {
  DECISION_REQUEST: 'monetate:decision:DecisionRequest',
  USER_AGENT: 'monetate:context:UserAgent',
  PAGE_VIEW: 'monetate:context:PageView',
  CUSTOM_VARIABLES: 'monetate:context:CustomVariables',
  CART_CONTEXT: 'monetate:context:Cart',
  ENDCAP_CLICKS: 'monetate:record:EndcapClicks',
};

export default monetateConstant;
