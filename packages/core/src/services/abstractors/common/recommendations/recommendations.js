/* eslint-disable max-lines */
/* eslint-disable complexity */
/* eslint-disable no-underscore-dangle */
// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import Constants, {
  MAX_REC_ITEMS_PER_CAROUSEL,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import { readCookie, setCookie } from '../../../../utils/cookie.util';
import {
  getSiteId,
  isGymboree,
  isMobileApp,
  getValueFromAsyncStorage,
  setValueInAsyncStorage,
  getUserAgent,
} from '../../../../utils';
import { executeUnbxdAPICall, executeExternalAPICall } from '../../../handler';
import logger from '../../../../utils/loggerInstance';
import processResponse from '../../productListing/processResponse';
import endpoints from '../../../endpoints';
import monetateConstant from './constants';

const { DECISION_REQUEST, USER_AGENT, PAGE_VIEW, CUSTOM_VARIABLES, CART_CONTEXT, ENDCAP_CLICKS } =
  monetateConstant;

const RecommendationsAbstractor = {
  isUSStore: getSiteId() === 'us',
  formatResponseRecommendation: (event) => {
    let recs = '';
    let title = '';

    if (event.detail) {
      if (Array.isArray(event.detail)) {
        recs = event.detail;
      } else {
        recs = event.detail.recs || [];
        title = event.detail.title || '';
      }
    }

    return { recs, title };
  },
  // event listener callback that sets recommendations and clears itself
  getRecs: (resolve, xappURLConfig) => (event) => {
    const { recs, title } = RecommendationsAbstractor.formatResponseRecommendation(event);
    return resolve(RecommendationsAbstractor.parseProductResponse(recs, title, xappURLConfig));
  },
  getMcmId: () => {
    let mcmid;
    if (window._satellite && window._satellite.getVisitorId) {
      mcmid = window._satellite.getVisitorId().getMarketingCloudVisitorID();
    } else {
      const cookieArr = readCookie('AMCV_9A0A1C8B5329646E0A490D4D@AdobeOrg').split('|');
      mcmid = cookieArr.length ? cookieArr[cookieArr.indexOf('MCMID') + 1] : '';
    }

    return mcmid;
  },

  getRecommendationObject: (recommendations, title = '', xappURLConfig, socialProofItems) => {
    return RecommendationsAbstractor.getProductsPrices(
      recommendations.map((recommendation) => recommendation.generalProductId),
      xappURLConfig
    ).then((prices) => {
      const allPrices = prices.reduce((productArr, price) => {
        return { ...productArr, ...price };
      }, {});
      return {
        products: recommendations.reduce((productsList, recommendation) => {
          if (typeof allPrices[recommendation.generalProductId] !== 'undefined') {
            productsList.push({
              ...recommendation,
              ...allPrices[recommendation.generalProductId],
            });
          }
          return productsList;
        }, []),
        mainTitle: title,
        socialProofItems,
      };
    });
  },

  parseProductResponseForMonetate: (products, xappURLConfig, socialProofItems) => {
    const recommendations = products.map((product) => ({
      generalProductId: product.itemGroupId?.replace(/_(US|CA)$/, ''),
      actionId: product.actionId,
      pdpUrl: '',
      department: '',
      name: product.title,
      imagePath: product.imageLink,
      isRecentViewed: product.isRecentViewed,
      isPdpPlaProducts: product.isPdpPlaProducts,
      isAbPdpOos: product.isAbPdpOos,
      isPlpProducts: product.isPlpProducts,
      isAbCartCash: product.isAbCartCash,
      isAbCartReward: product.isAbCartReward,
      styleWithEligibility: product.isStyleWithDataAvailable,
      isFbt: product.isFbt,
      isOrderNotifications: product.isOrderNotifications,
      isNavLevel2Girls: product.isNavLevel2Girls,
      isNavLevel2Boys: product.isNavLevel2Boys,
      isNavLevel2ToddlerGirls: product.isNavLevel2ToddlerGirls,
      isNavLevel2ToddlerBoys: product.isNavLevel2ToddlerBoys,
      isNavLevel2Baby: product.isNavLevel2Baby,
      isInGridPlp: product.isInGridPlp,
      isInMultiGridPlp: product.isInMultiGridPlp,
    }));
    return RecommendationsAbstractor.getRecommendationObject(
      recommendations,
      '',
      xappURLConfig,
      socialProofItems
    );
  },

  parseProductResponse: (products, title, xappURLConfig) => {
    const recommendations = products
      .filter((product) => product.availability === 'In Stock')
      .map((product) => ({
        generalProductId: product.id.replace(/_(US|CA)$/, ''),
        pdpUrl: product.pdpURL,
        department: product.department,
        name: product.name,
        imagePath: product.imagePath,
      }));

    return RecommendationsAbstractor.getRecommendationObject(recommendations, title, xappURLConfig);
  },

  getData: (payload, xappURLConfig) => {
    const { MONETATE_ENABLED = true } = payload;
    if (MONETATE_ENABLED)
      return RecommendationsAbstractor.getRecommendationsData(payload, xappURLConfig);
    return RecommendationsAbstractor.getAdobeData(payload, xappURLConfig);
  },

  getMobileData: (payload, xappURLConfig) => {
    const { MONETATE_ENABLED = true } = payload;
    if (MONETATE_ENABLED)
      return RecommendationsAbstractor.getMobileDataFromMonetate(payload, xappURLConfig);
    return RecommendationsAbstractor.getMobileDataFromAdobe(payload, xappURLConfig);
  },

  filterStyleWithdata: (actions, styleWithEligibility) => {
    let data = null;
    const styleWithData =
      actions.length && actions.filter((action) => action.component === 'mpackStyleWith');
    if (styleWithEligibility && styleWithData && styleWithData.length && styleWithData[0].items) {
      data = styleWithData[0].items.map((item) => ({ ...item, isStyleWithDataAvailable: true }));
    }
    return data;
  },

  getAdobeData: (
    {
      itemPartNumber,
      page,
      siteId = getSiteId(),
      categoryName,
      mboxName = 'global_recs_mbox',
      excludedIds,
      otherMboxProps,
    },
    xappURLConfig
  ) => {
    const mcmid = RecommendationsAbstractor.getMcmId();

    return new Promise((resolve, reject) => {
      if (window.adobe && window.adobe.target) {
        window.adobe.target.getOffer({
          mbox: mboxName,
          marketingCloudVisitorId: mcmid || '',
          params: {
            'entity.id': itemPartNumber ? `${itemPartNumber}_${siteId.toUpperCase()}` : '',
            'entity.categoryId': categoryName || '',
            pageType: page || '',
            site: isGymboree() ? 'GYM' : 'TCP',
            excludedIds,
            ...otherMboxProps,
          },
          success: (offer) => {
            const eventName = `res-${mboxName}`;
            const offers = offer;

            if (offers && offers.length > 0) {
              offers[0].content = offers[0].content.replace('pdpRecs', eventName);
              window.addEventListener(
                eventName,
                RecommendationsAbstractor.getRecs(resolve, xappURLConfig),
                {
                  once: true,
                }
              );
              window.adobe.target.applyOffer({
                mbox: mboxName,
                offer: offers,
              });
            } else {
              // eslint-disable-next-line sonarjs/no-duplicate-string
              reject(new Error('Empty recommendation'));
            }
          },
          error() {
            reject(new Error('Adobe getOffer Error'));
          },
        });
      }
    });
  },

  getMobileDataFromAdobe: (
    { pageType, categoryName, partNumber, mbox = 'global_recs_mbox', excludedIds },
    xappURLConfig
  ) => {
    const ADOBE_CLIENT = isGymboree() ? 'gym' : 'tcp';
    const ADOBE_RECOMMENDATIONS_URL = `https://tcp.tt.omtrdc.net/rest/v1/mbox?client=tcp&site=${ADOBE_CLIENT}`;
    const ADOBE_RECOMMENDATIONS_IMPRESSION_ID = 1;
    const ADOBE_RECOMMENDATIONS_HOST = 'thechildrensplace';
    const region = getSiteId(); // TODO use `CA` for Canada
    const requestLocation = {
      impressionId: ADOBE_RECOMMENDATIONS_IMPRESSION_ID,
      host: ADOBE_RECOMMENDATIONS_HOST,
    };
    const apiConfigObj = getAPIConfig();
    const { envForRecommendation } = apiConfigObj;
    return executeExternalAPICall({
      webService: {
        method: 'POST',
        URI: ADOBE_RECOMMENDATIONS_URL,
      },
      body: {
        marketingCloudVisitorId: '',
        mbox,
        requestLocation,
        mboxParameters: {
          'entity.categoryId': categoryName || '',
          'entity.id': partNumber ? `${partNumber}_${region.toUpperCase()}` : '',
          pageType,
          region,
          excludedIds,
          tenant: isGymboree() ? 'GYM' : 'TCP',
          deviceType: isMobileApp() ? 'App' : 'Web',
          env: `${envForRecommendation}`,
        },
      },
    }).then((result) => {
      return RecommendationsAbstractor.parseProductResponse(
        result.body && result.body.content ? JSON.parse(result.body.content) : [],
        '',
        xappURLConfig
      );
    });
  },

  getCartLines: (cartItems) => {
    return cartItems.map((item) => ({
      sku: item.sku,
      pid: item.pid,
      quantity: item.quantity,
      currency: item.currency,
      value: String(item.value),
    }));
  },

  updateItems: (items, response, recType) => {
    return items.concat(
      response.items.slice(0, MAX_REC_ITEMS_PER_CAROUSEL).map((item) => ({
        ...item,
        ...recType,
        actionId: response.actionId,
        action: response,
      }))
    );
  },

  getRecommendationsData: (
    {
      itemPartNumber,
      page,
      siteId = getSiteId(),
      categoryName,
      excludedIds,
      otherMboxProps,
      cartItems,
      outfitItems,
      skuData,
      styleWithEligibility,
      isPlcc,
      outOfStock,
      isLoggedIn,
      isRecProduct,
      actionId,
      quickAdd,
    },
    xappURLConfig
  ) => {
    // eslint-disable-next-line sonarjs/cognitive-complexity
    return new Promise((resolve, reject) => {
      let productId = '';
      let extendedParams = {};
      if (skuData) {
        extendedParams = {
          style_qty: skuData.TCPStyleQTY ? skuData.TCPStyleQTY : '',
          style_type: skuData.TCPStyleType ? skuData.TCPStyleType : '',
          style_color: skuData.color && skuData.color.name ? skuData.color.name : '',
          product_family: skuData.productFamily ? skuData.productFamily : '',
        };
      }
      if (itemPartNumber) [productId] = itemPartNumber.split('&');
      const customParams = {
        'entity.id': productId ? `${productId}_${siteId.toUpperCase()}` : '',
        'entity.categoryId': categoryName || '',
        site: isGymboree() ? 'GYM' : 'TCP',
        apiRequestForMonetate: 'recommendation',
        isRegistered: isLoggedIn,
        hasPlcc: isPlcc || false, // Added false to send data in custom variables in case of undefined
        outOfStock: outOfStock || false,
        excludedIds,
        ...extendedParams,
        ...otherMboxProps,
      };
      if (page === 'cart' && (!Array.isArray(cartItems) || !cartItems.length)) {
        customParams.emptyCart = 'true';
      }
      if (page === 'cart') {
        customParams.quickAdd = quickAdd.toString();
      }
      const apiConfigObj = getAPIConfig();
      const { monetateUrl, monetateChannel } = apiConfigObj;
      const reqBody = {
        channel: monetateChannel,
        events: [
          {
            eventType: DECISION_REQUEST,
            // eslint-disable-next-line no-restricted-properties
            requestId: `${Math.round(Math.random() * Math.pow(10, 4))}`,
          },
          {
            eventType: USER_AGENT,
            userAgent: navigator.userAgent,
          },
          {
            eventType: PAGE_VIEW,
            url: window.location.href,
            pageType: page || '',
          },
          {
            eventType: CUSTOM_VARIABLES,
            customVariables: Object.keys(customParams).map((key) => ({
              variable: key,
              value: customParams[key],
            })),
          },
        ],
      };
      if (page === 'pdp' || page === 'added_to_bag') {
        reqBody.events.push({
          // eslint-disable-next-line sonarjs/no-duplicate-string
          eventType: 'monetate:context:ProductDetailView',
          products: [{ productId }],
        });
        if (isRecProduct) {
          reqBody.events.push({
            eventType: ENDCAP_CLICKS,
            endcapClicks: [{ actionId, products: [{ productId }] }],
          });
          // reqBody.events.push({
          //   eventType: 'monetate:record:EndcapImpressions',
          //   endcapImpressions: [{ actionId, products: [{ productId }] }],
          // });
        }
      } else if (page === 'cart' || page === 'checkout') {
        reqBody.events.push({
          eventType: CART_CONTEXT,
          cartLines: RecommendationsAbstractor.getCartLines(cartItems),
        });
      } else if (page === 'outfit') {
        reqBody.events.push({
          eventType: 'monetate:context:ProductDetailView',
          products: outfitItems,
        });
      }

      const monetateIdCookie = readCookie('mt.v');
      if (monetateIdCookie) reqBody.monetateId = monetateIdCookie;
      const emailHash = getLocalStorage('email_hash');
      if (emailHash) reqBody.customerId = emailHash;

      executeExternalAPICall({
        webService: {
          method: 'POST',
          URI: monetateUrl,
        },
        body: reqBody,
      }).then((result) => {
        const { monetateId } = result.body.meta;
        if (typeof monetateId !== 'undefined')
          setCookie({
            key: 'mt.v',
            value: monetateId,
            daysAlive: 1,
          });

        const { responses } = result.body.data;
        if (!responses.length) return reject(new Error('Empty recommendation'));
        const { actions } = responses[0];
        if (!actions.length) return reject(new Error('Empty recommendation'));

        let items = [];
        let socialProofItems = [];
        const isStyleWithDataRecieved = RecommendationsAbstractor.filterStyleWithdata(
          actions,
          styleWithEligibility
        );

        if (isStyleWithDataRecieved) {
          items = isStyleWithDataRecieved;
        } else {
          actions.forEach((action) => {
            if (action.actionType === 'monetate:action:SocialProofData') {
              socialProofItems = socialProofItems.concat({ socialProofMessage: action });
            } else {
              switch (action.component) {
                case `${page}2`:
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isRecentViewed: true,
                  });
                  break;
                case `${page}1`:
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isRecentViewed: false,
                  });
                  break;
                case 'fbt':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isFbt: true,
                  });
                  break;
                case 'ab_pdp_oos':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isAbPdpOos: true,
                  });
                  break;
                case 'ab_pdp_pla':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isPdpPlaProducts: true,
                  });
                  break;
                case 'ab_plp_personalized':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isPlpProducts: true,
                  });
                  break;
                case 'ab_cart_cash':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isAbCartCash: true,
                  });
                  break;
                case 'ab_cart_reward':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isAbCartReward: true,
                  });
                  break;
                case 'in_grid_plp':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isInGridPlp: true,
                  });
                  break;
                case 'in_multi_grid_plp':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isInMultiGridPlp: true,
                  });
                  break;
                default:
                  break;
              }
            }
          });
        }

        return resolve(
          RecommendationsAbstractor.parseProductResponseForMonetate(
            items,
            xappURLConfig,
            socialProofItems
          )
        );
      });
    });
  },

  sendEventsToMonetate: ({
    itemPartNumber,
    page,
    siteId = getSiteId(),
    categoryName,
    otherMboxProps,
    isPlcc,
    isLoggedIn,
    actionId,
  }) => {
    let productId = '';
    if (itemPartNumber) [productId] = itemPartNumber.split('&');
    const customParams = {
      'entity.id': productId ? `${productId}_${siteId.toUpperCase()}` : '',
      'entity.categoryId': categoryName || '',
      site: isGymboree() ? 'GYM' : 'TCP',
      apiRequestForMonetate: 'recommendation',
      isRegistered: isLoggedIn,
      hasPlcc: isPlcc || false, // Added false to send data in custom variables in case of undefined
      ...otherMboxProps,
    };
    const apiConfigObj = getAPIConfig();
    const { monetateUrl, monetateChannel } = apiConfigObj;
    const reqBody = {
      channel: monetateChannel,
      events: [
        {
          eventType: DECISION_REQUEST,
          requestId: `${Math.round(Math.random() * 10 ** 4)}`,
        },
        {
          eventType: USER_AGENT,
          userAgent: navigator.userAgent,
        },
        {
          eventType: PAGE_VIEW,
          url: window.location.href,
          pageType: page || '',
        },
        {
          eventType: CUSTOM_VARIABLES,
          customVariables: Object.keys(customParams).map((key) => ({
            variable: key,
            value: customParams[key],
          })),
        },
        {
          eventType: ENDCAP_CLICKS,
          endcapClicks: [{ actionId, products: [{ productId }] }],
        },
      ],
    };

    const monetateIdCookie = readCookie('mt.v');
    if (monetateIdCookie) reqBody.monetateId = monetateIdCookie;
    const emailHash = getLocalStorage('email_hash');
    if (emailHash) reqBody.customerId = emailHash;

    return executeExternalAPICall({
      webService: {
        method: 'POST',
        URI: monetateUrl,
      },
      body: reqBody,
    }).then(() => 'success');
  },

  getMobileDataFromMonetate: (
    {
      pageType,
      categoryName,
      partNumber,
      excludedIds,
      siteId = getSiteId(),
      cartItems,
      outfitItems,
      seo,
      seoPdp,
      skuData,
      TCPStyleType,
      isPlcc,
      styleWithEligibility,
      outOfStock,
      isUserLoggedIn,
      actionId,
      isRecProduct,
      quickAdd,
    },
    xappURLConfig
  ) => {
    // eslint-disable-next-line sonarjs/cognitive-complexity
    return new Promise(async (resolve, reject) => {
      const customParams = {
        'entity.id': partNumber ? `${partNumber}_${siteId.toUpperCase()}` : '',
        'entity.categoryId': categoryName || '',
        site: isGymboree() ? 'GYM' : 'TCP',
        hasPlcc: isPlcc || false,
        outOfStock: outOfStock || false,
        apiRequestForMonetate: 'recommendation',
        isRegistered: isUserLoggedIn,
        excludedIds,
        deviceType: 'Mobile',
        ...RecommendationsAbstractor.getExtendedParams(skuData),
      };
      if (pageType === 'cart') {
        customParams.quickAdd = quickAdd.toString();
      }
      const apiConfigObj = getAPIConfig();
      const { monetateUrl } = apiConfigObj;
      const reqBody = RecommendationsAbstractor.getMonetateReqBody({
        pageType,
        customParams,
        partNumber,
        cartItems,
        outfitItems,
        isMobile: true,
        seo,
        seoPdp,
        skuData,
        TCPStyleType,
      });
      if (pageType === 'PDP' && isRecProduct) {
        reqBody.events.push({
          eventType: ENDCAP_CLICKS,
          endcapClicks: [{ actionId, products: [{ productId: partNumber }] }],
        });
      }
      const monetateIdCookie = await getValueFromAsyncStorage('monetateId');
      if (monetateIdCookie) reqBody.monetateId = monetateIdCookie;
      const emailHash = await getValueFromAsyncStorage('email_hash');
      if (emailHash) reqBody.customerId = emailHash;
      let socialProofItems = [];

      executeExternalAPICall({
        webService: {
          method: 'POST',
          URI: monetateUrl,
        },
        body: reqBody,
      }).then(async (result) => {
        const { monetateId } = result.body.meta;
        if (typeof monetateId !== 'undefined')
          await setValueInAsyncStorage('monetateId', monetateId);
        const { responses } = result.body.data;
        if (!responses.length) return reject(new Error('Empty recommendation'));
        const { actions } = responses[0];
        if (!actions.length) return reject(new Error('Empty recommendation'));
        let items = [];
        const isStyleWithDataRecieved = RecommendationsAbstractor.filterStyleWithdata(
          actions,
          styleWithEligibility
        );

        if (isStyleWithDataRecieved) {
          items = isStyleWithDataRecieved;
        } else {
          actions.forEach((action) => {
            if (action.actionType === 'monetate:action:SocialProofData') {
              socialProofItems = socialProofItems.concat({ socialProofMessage: action });
            } else {
              switch (action.component) {
                case `${pageType}2`:
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isRecentViewed: true,
                  });
                  break;
                case `${pageType}1`:
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isRecentViewed: false,
                  });
                  break;
                case 'ab_pdp_oos':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isAbPdpOos: true,
                  });
                  break;
                case 'ab_plp_personalized':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isPlpProducts: true,
                  });
                  break;
                case 'ab_cart_cash':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isAbCartCash: true,
                  });
                  break;
                case 'ab_cart_reward':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isAbCartReward: true,
                  });
                  break;
                case 'order_notifications':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isOrderNotifications: true,
                  });
                  break;
                case 'navlevel2_girls':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isNavLevel2Girls: true,
                  });
                  break;
                case 'navlevel2_boys':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isNavLevel2Boys: true,
                  });
                  break;
                case 'navlevel2_toddler_girls':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isNavLevel2ToddlerGirls: true,
                  });
                  break;
                case 'navlevel2_toddler_boys':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isNavLevel2ToddlerBoys: true,
                  });
                  break;
                case 'navlevel2_baby':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isNavLevel2Baby: true,
                  });
                  break;
                case 'in_grid_plp':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isInGridPlp: true,
                  });
                  break;
                case 'in_multi_grid_plp':
                  items = RecommendationsAbstractor.updateItems(items, action, {
                    isInMultiGridPlp: true,
                  });
                  break;
                default:
                  break;
              }
            }
          });
        }
        return resolve(
          RecommendationsAbstractor.parseProductResponseForMonetate(
            items,
            xappURLConfig,
            socialProofItems
          )
        );
      });
    });
  },

  sendMobileEventsToMonetate: ({
    pageType,
    categoryName,
    partNumber,
    siteId = getSiteId(),
    isPlcc,
    isUserLoggedIn,
    actionId,
  }) => {
    const customParams = {
      'entity.id': partNumber ? `${partNumber}_${siteId.toUpperCase()}` : '',
      'entity.categoryId': categoryName || '',
      site: isGymboree() ? 'GYM' : 'TCP',
      hasPlcc: isPlcc || false,
      apiRequestForMonetate: 'recommendation',
      isRegistered: isUserLoggedIn,
      deviceType: 'Mobile',
    };
    const apiConfigObj = getAPIConfig();
    const { monetateChannel, monetateUrl } = apiConfigObj;
    const reqBody = {
      channel: monetateChannel,
      events: [
        {
          eventType: DECISION_REQUEST,
          requestId: `${Math.round(Math.random() * 10 ** 4)}`,
        },
        {
          eventType: USER_AGENT,
          userAgent: getUserAgent(),
        },
        {
          eventType: PAGE_VIEW,
          pageType: pageType || '',
        },
        {
          eventType: CUSTOM_VARIABLES,
          customVariables: Object.keys(customParams).map((key) => ({
            variable: key,
            value: customParams[key],
          })),
        },
        {
          eventType: ENDCAP_CLICKS,
          endcapClicks: [{ actionId, products: [{ productId: partNumber }] }],
        },
      ],
    };
    getValueFromAsyncStorage('monetateId').then((monetateIdCookie) => {
      if (monetateIdCookie) reqBody.monetateId = monetateIdCookie;
    });
    getValueFromAsyncStorage('email_hash').then((emailHash) => {
      if (emailHash) reqBody.customerId = emailHash;
    });

    return executeExternalAPICall({
      webService: {
        method: 'POST',
        URI: monetateUrl,
      },
      body: reqBody,
    }).then(() => 'success');
  },

  getExtendedParams: (skuData) => {
    let extendedParams = {};
    if (skuData) {
      extendedParams = {
        style_qty: skuData.TCPStyleQTY ? skuData.TCPStyleQTY : '',
        style_type: skuData.TCPStyleType ? skuData.TCPStyleType : '',
        style_color: skuData.color && skuData.color.name ? skuData.color.name : '',
        product_family: skuData.productFamily ? skuData.productFamily : '',
      };
    }
    return extendedParams;
  },

  getLocationUrl: (isMobile, webAppDomain, pageType) => {
    return isMobile ? `${webAppDomain}/mobileapp/${pageType.toLowerCase()}` : window.location.href;
  },

  getMonetateReqBody: ({
    pageType,
    customParams,
    partNumber,
    cartItems,
    outfitItems,
    isMobile,
    seo,
    seoPdp,
  }) => {
    const apiConfigObj = getAPIConfig();
    const { monetateChannel, webAppDomain } = apiConfigObj;

    const reqBody = {
      channel: monetateChannel,
      events: [
        {
          eventType: DECISION_REQUEST,
          // eslint-disable-next-line no-restricted-properties
          requestId: `${Math.round(Math.random() * Math.pow(10, 4))}`,
        },
        {
          eventType: USER_AGENT,
          userAgent: getUserAgent(),
        },
        {
          eventType: PAGE_VIEW,
          pageType: pageType || '',
        },
        {
          eventType: CUSTOM_VARIABLES,
          customVariables: Object.keys(customParams).map((key) => ({
            variable: key,
            value: customParams[key],
          })),
        },
      ],
    };
    reqBody.events[2].url = RecommendationsAbstractor.getLocationUrl(
      isMobile,
      webAppDomain,
      pageType
    );
    if (pageType === 'pdp') {
      if (isMobile && seoPdp)
        reqBody.events[2].url = `${webAppDomain}/mobileapp/${pageType.toLowerCase()}/${seoPdp}`;
      reqBody.events.push({
        eventType: 'monetate:context:ProductDetailView',
        products: [{ productId: partNumber }],
      });
    } else if (pageType === 'added_to_bag') {
      reqBody.events.push({
        eventType: 'monetate:context:ProductDetailView',
        products: [{ productId: partNumber }],
      });
    } else if (pageType === 'cart' || pageType === 'checkout') {
      if (!Array.isArray(cartItems) || !cartItems.length) {
        reqBody.events[3].customVariables.push({ variable: 'emptyCart', value: 'true' });
      }
      reqBody.events.push({
        eventType: CART_CONTEXT,
        cartLines: RecommendationsAbstractor.getCartLines(cartItems),
      });
    } else if (pageType === 'outfit') {
      reqBody.events.push({
        eventType: 'monetate:context:ProductDetailView',
        products: outfitItems,
      });
    } else if (pageType === 'plp' && seo && isMobile) {
      reqBody.events[2].url = `${webAppDomain}/mobileapp/${pageType.toLowerCase()}/${seo}`;
    }
    return reqBody;
  },

  handleValidationError: (e) => {
    logger.error(e);
  },
  getOriginImgHostSetting: () => {
    return 'https://test4.childrensplace.com';
  },
  getSwatchImgPath: (id, excludeExtension) => {
    const imgHostDomain = RecommendationsAbstractor.getOriginImgHostSetting();
    return `${imgHostDomain}/wcsstore/GlobalSAS/images/tcp/products/swatches/${id}${
      excludeExtension ? '' : '.jpg'
    }`;
  },

  handleWhichRecommendationClicked: (portalValue, page, sequence = '1') => {
    if (portalValue !== null && portalValue !== 'undefined' && page !== '') {
      let recommendationIdentifier = '';
      if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED) {
        recommendationIdentifier = `${page}2`;
      } else if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.PLP_PRODUCTS) {
        recommendationIdentifier = 'ab_plp_personalized';
      } else if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.OUT_OF_STOCK_PDP) {
        recommendationIdentifier = 'ab_pdp_oos';
      } else if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.CHECKOUT_SIMILAR_STYLES) {
        recommendationIdentifier = 'ab_pdp_pla';
      } else if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.ADD_PRODUCT_TO_GET_PLACECASH) {
        recommendationIdentifier = 'ab_cart_cash';
      } else if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.BAG_CART_REWARD_PLCC) {
        recommendationIdentifier = 'ab_cart_reward';
      } else if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.ORDER_NOTIFICATION) {
        recommendationIdentifier = 'order_notifications';
      } else {
        recommendationIdentifier = `${page ? `${page}${sequence}` : ''}`;
      }

      return recommendationIdentifier;
    }
    return false;
  },

  getProductImgPath: (id, excludeExtension) => {
    const imageName = id.split('_');
    const imagePath = imageName[0];
    return {
      125: `${imagePath}/${id}${excludeExtension ? '' : '.jpg'}`,
      380: `${imagePath}/${id}${excludeExtension ? '' : '.jpg'}`,
      500: `${imagePath}/${id}${excludeExtension ? '' : '.jpg'}`,
      900: `${imagePath}/${id}${excludeExtension ? '' : '.jpg'}`,
    };
  },

  getImgPath: (id, excludeExtension) => {
    return {
      colorSwatch: RecommendationsAbstractor.getSwatchImgPath(id, excludeExtension),
      productImages: RecommendationsAbstractor.getProductImgPath(id, excludeExtension),
    };
  },
  getFacetSwatchImgPath: (id) => {
    const imgHostDomain = RecommendationsAbstractor.getOriginImgHostSetting();
    return `${imgHostDomain}/wcsstore/GlobalSAS/images/tcp/category/color-swatches/${id}.gif`;
  },
  /*
   * @function getProductsPrices
   * @summary Auxiliar method to retrieve prices for product recommendations. It returns prices for specific product ids
   * @param {Array[String]} productIds - the ids of all the products to get prices for
   */
  getProductsPrices: (items, xappURLConfig) => {
    const productsLength = Math.ceil(items.length / MAX_REC_ITEMS_PER_CAROUSEL);
    const recProductsPromise = [];
    [...Array(productsLength)].forEach((_, index) => {
      const productIds = items.slice(
        index * MAX_REC_ITEMS_PER_CAROUSEL,
        index * MAX_REC_ITEMS_PER_CAROUSEL + MAX_REC_ITEMS_PER_CAROUSEL
      );
      const payload = {
        body: {
          id: productIds.join(','),
          variants: true,
          'variants.count': 100,
          fields:
            'productimage,alt_img,style_partno,swatchimage,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,imagename,productid,uniqueId,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant, low_offer_price, high_offer_price, low_list_price, high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore',
        },
        webService: endpoints.getProductDetails,
      };
      const recProducts = executeUnbxdAPICall(payload, null, xappURLConfig)
        .then((res) => {
          return processResponse(res, null, {
            getFacetSwatchImgPath: RecommendationsAbstractor.getFacetSwatchImgPath,
            getImgPath: RecommendationsAbstractor.getImgPath,
            isRecommendationView: true,
          });
        })
        .then((res) => {
          const price = {};
          res.loadedProductsPages[0].forEach((product) => {
            price[product.productInfo.uniqueId] = {
              ...product,
            };
          });
          return price;
        })
        .catch((err) => {
          RecommendationsAbstractor.handleValidationError(err);
        });
      recProductsPromise.push(recProducts);
    });
    return Promise.all(recProductsPromise);
  },

  getProductsByFilter: async (productIds, filters = [], rows = 10) => {
    try {
      const filteredProducts = {};
      const requestPayload = {
        body: {
          q: '*',
          filter: filters,
          rows,
          fields:
            'categoryPath2,productimage,alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,low_offer_price,high_offer_price,low_list_price,high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,TCPMerchantTagUSStore',
        },
        webService: endpoints.getProductInfoById,
      };
      const unbxdResponse = await executeUnbxdAPICall(requestPayload);
      const processedResponse = await processResponse(unbxdResponse, null, {
        getFacetSwatchImgPath: RecommendationsAbstractor.getFacetSwatchImgPath,
        getImgPath: RecommendationsAbstractor.getImgPath,
        isRecommendationView: true,
      });
      processedResponse.loadedProductsPages[0].forEach((product) => {
        filteredProducts[product.productInfo.uniqueId] = {
          ...product,
        };
      });
      return filteredProducts;
    } catch (error) {
      RecommendationsAbstractor.handleValidationError(error);
    }
    return {};
  },
  getSocialProofMsg: (socialProofItems = {}) => {
    let priorityMsg = '';
    let purchaseTypePresent = false;
    let viewTypePresent = false;
    let variation = '';
    Object.keys(socialProofItems).forEach((item) => {
      const messageOriginal = socialProofItems?.[item]?.socialProofMessage?.component;
      const proofOfType = socialProofItems?.[item]?.socialProofMessage?.data?.proofType;
      const count = socialProofItems?.[item]?.socialProofMessage?.data?.count;
      let message = messageOriginal?.replace('#', count);
      message = message?.split('_').pop();
      if (proofOfType?.includes('purchase')) {
        priorityMsg = message;
        variation = messageOriginal?.toLowerCase()?.includes('bottom') ? 'bottom' : 'top';
        purchaseTypePresent = true;
      } else if (proofOfType?.includes('view') && !purchaseTypePresent) {
        priorityMsg = message;
        viewTypePresent = true;
        if (messageOriginal?.toLowerCase()?.includes('top')) {
          variation = 'top';
        }
        if (messageOriginal?.toLowerCase()?.includes('bottom')) {
          variation = 'bottom';
        }
      } else if (proofOfType?.includes('cart') && !purchaseTypePresent && !viewTypePresent) {
        variation = messageOriginal?.toLowerCase()?.includes('bottom') ? 'bottom' : 'top';
        priorityMsg = message;
      }
    });
    return { priorityMsg, variation };
  },
};
export default RecommendationsAbstractor;
