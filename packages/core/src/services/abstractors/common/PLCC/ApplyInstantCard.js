// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable complexity */
/* eslint-disable sonarjs/cognitive-complexity */
import constants, {
  INVALID_PRESCREEN_CODE,
} from '@tcp/core/src/components/features/browse/ApplyCardPage/RewardsCard.constants';
import { executeStatefulAPICall } from '../../../handler';
import endpoints from '../../../endpoints';
import { getSiteId } from '../../../../utils';
import { API_CONFIG } from '../../../config';
import { getFormattedError } from '../../../../utils/errorMessage.util';

export const isCanada = () => {
  const siteId = getSiteId();
  return siteId === API_CONFIG.siteIds.ca;
};

export const getStatusCodeRes = (returnCode, body, args, errorsMapping) => {
  if (returnCode) {
    const {
      address,
      maskedAccountNumber,
      returnCodeDescription,
      addressId,
      isMprIdExist,
      isRegistered,
    } = body;
    switch (body.returnCode) {
      case '02':
        return {
          status: constants.APPLICATION_STATE_PENDING,
        };
      case '04':
        return {
          status: errorsMapping.ERR_REQUEST_TIMEOUT,
        };
      case '03':
      case '01':
        return {
          onFileCardId: body.xCardId && body.xCardId.toString(),
          cardNumber: body.cardNumber,
          cardType: 'PLACE CARD',
          isExpirationRequired: false,
          isCVVRequired: false,
          isDefault: false,

          address: address && {
            firstName: address.firstName,
            lastName: address.lastName,
            addressLine1: address.address1,
            addressLine2: address.address2,
            zipCode: address.zipCode,
            state: address.state,
            city: address.city,
            country: address.country,
          },

          emailAddress: args.emailAddress,
          phoneNumber: body.phoneNumber || args.phoneNumber,

          status: body.returnCode === '01' ? 'APPROVED' : 'EXISTING', // error code 03 = EXISTING but if we got to this point we assume! that the status is always exisiting if returnCode !== 1
          creditLimit: parseFloat((body.creditLimit || 0).toString().replace(/\$/gi, '')),
          // apr: parseFloat(credit.apr),
          couponCode: body.couponCode,
          savingAmount:
            (body.savingAmount && parseFloat(body.savingAmount.replace(/\$/gi, ''))) || 0,
          discount: parseFloat(body.percentOff), // '30%' but we'll need to calculate on it, so parseFloat
          maskedAccountNumber,
          returnCode,
          returnCodeDescription,
          addressId,
          isMprIdExist,
          isRegistered,
          xCardId: body.xCardId,
        };
      case '13007':
        return {
          status: INVALID_PRESCREEN_CODE,
        };
      default:
        return {
          status: 'Something went wrong.',
        };
    }
  }
  return false;
};
/**
 * This function will be used to create the request body for instant apply form
 * @param {*} args
 */

// eslint-disable-next-line complexity
const getInstantCardBody = (args, preScreenCode = '') => {
  const month = args.month < 10 ? `0${args.month}` : args.month;
  return {
    firstName: args.firstName || '',
    lastName: args.lastName || '',
    middleInitial: args.middleNameInitial || '',
    address1: args.addressLine1 || '',
    address2: args.addressLine2 || '',
    city: args.city || '',
    state: args.statewocountry || '',
    zipCode: args.noCountryZip || '',
    emailAddress: args.emailAddress || '',
    country: getSiteId() || '',
    ssn: args.ssNumber || '',
    prescreenId: args.preScreenCode || preScreenCode,
    mobilePhoneNumber: args.phoneNumberWithAlt ? args.phoneNumberWithAlt.replace(/\D/g, '') : '',
    phoneNumber: args.altPhoneNumber ? args.altPhoneNumber.replace(/\D/g, '') : '',
    birthdayDate: `${month}${args.date}${args.year}` || '',
    userId: args.userId || -1002,
    BF_ioBlackBox: args.BF_ioBlackBox || '',
  };
};

export const savePlcc = (approvedPLCCData) => {
  const {
    emailAddress,
    returnCode,
    returnCodeDescription,
    maskedAccountNumber,
    addressId,
    address: { firstName, lastName },
  } = approvedPLCCData;
  const payload = {
    webService: endpoints.savePlcc,
    body: {
      firstName,
      lastName,
      emailAddress,
      returnCode,
      returnCodeDescription,
      maskedAccountNumber,
      addressId,
      savePlccCard: true,
    },
  };
  return executeStatefulAPICall(payload)
    .then((res) => {
      return res.body;
    })
    .catch((err) => {
      return err;
    });
};

const applyInstantCard = (args, errorsMapping, preScreenCode, isExpressCheckout, isRTPSFlow) => {
  const payload = {
    // Overriding 'application/json' - specific to processWIC
    header: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    webService:
      isRTPSFlow || args.preScreenCode
        ? endpoints.prescreenApplication
        : endpoints.instantCreditApplication,
    body: getInstantCardBody(args, preScreenCode),
  };
  // We now have pre-screening on express checkout flow. We are to inform backend of this so that can auto apply any promos to the order
  if (isExpressCheckout) {
    payload.body.fromPage = 'expressCheckout';
  }
  return executeStatefulAPICall(payload)
    .then((res) => {
      const response = res.body;
      if (!response) {
        return {
          status: errorsMapping.INTERNAL_SERVER_ERROR,
        };
      }
      const body = res.body && res.body.response ? res.body.response : res.body;
      if (res.body.response === null && isRTPSFlow) {
        return {
          status: constants.APPLICATION_STATE_EXISTING,
        };
      }
      const returnCode = body.returnCode || body.errorCode;
      return getStatusCodeRes(returnCode, body, args, errorsMapping);
    })
    .catch((err) => {
      const error = getFormattedError(err, errorsMapping);
      error.errorMessages = error.errorMessages || {
        internalError: errorsMapping.INTERNAL_SERVER_ERROR,
      };
      const { errorMessages } = error;
      errorMessages.internalError = {
        // eslint-disable-next-line no-underscore-dangle
        status: errorMessages.internalError || errorMessages._error,
      };
      return error.errorMessages && error.errorMessages.internalError;
    });
};

export default applyInstantCard;
