// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable complexity */
import applyInstantCard, { isCanada, savePlcc, getStatusCodeRes } from '../ApplyInstantCard';
import { executeStatefulAPICall } from '../../../../handler';
import { getSiteId } from '../../../../../utils';

jest.mock('../../../../handler/handler', () => ({
  executeStatefulAPICall: jest.fn(),
}));
jest.mock('../../../../../utils', () => ({
  getSiteId: jest.fn(),
}));
const errorMessage = 'Test Error Messages';

describe('getStatusCodeRes', () => {
  it('should return status PENDING if returnCode is 02', () => {
    const bodyres = {
      returnCode: '02',
    };
    expect(getStatusCodeRes('02', bodyres)).toEqual({ status: 'PENDING' });
  });
  it('should return status INVALID_PRESCREEN_CODE if returnCode is 13007', () => {
    const bodyres = {
      returnCode: '13007',
    };
    expect(getStatusCodeRes('13007', bodyres)).toEqual({ status: 'INVALID_PRESCREEN_CODE' });
  });
});

describe('isCanada', () => {
  it('should return true if it is canada', () => {
    getSiteId.mockImplementation(() => 'ca');
    expect(isCanada()).toBe(true);
  });
  it('should return false if it is not canada', () => {
    getSiteId.mockImplementation(() => 'us');
    expect(isCanada()).toBe(false);
  });
});

describe('#savePlcc', () => {
  const approvedPLCCData = {
    emailAddress: 'email@gmail.com',
    returnCode: '02',
    returnCodeDescription: 'code description',
    maskedAccountNumber: 'XXXX0123',
    addressId: 'adressID',
    address: {
      firstName: 'firstName',
      lastName: 'lastName',
    },
  };
  it('should save plcc', () => {
    const result = {
      body: {
        addressId: '167302',
        nickName: 'sb_2019-07-26 02:30:48.116',
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    savePlcc(approvedPLCCData).then(data => {
      expect(data).toMatchObject(result);
    });
  });
  it('Should throw errors in case of server side error', () => {
    const result = {
      body: {
        errors: [
          {
            errorMessage,
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.reject(result));
    savePlcc(approvedPLCCData).then(data => {
      expect(data).toEqual({ body: { errors: [{ errorMessage: 'Test Error Messages' }] } });
    });
  });

  it('Should throw errors in case of unable to connect to server', () => {
    const result = {};
    executeStatefulAPICall.mockImplementation(() => Promise.reject(result));
    savePlcc(approvedPLCCData).then(data => {
      expect(data).toEqual({});
    });
  });
});

describe('#applyInstantCard', () => {
  const preScreenCode = 'preScreenCode';
  const args = {
    firstName: 'firstName',
    lastName: 'lastName',
    prescreenId: preScreenCode,
    mobilePhoneNumber: '1234567890',
  };
  const errorsMapping = {
    INTERNAL_SERVER_ERROR: 'internal server error',
  };
  it('should apply instant card', () => {
    const result = {
      body: {
        addressId: '167302',
        nickName: 'sb_2019-07-26 02:30:48.116',
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(result));
    applyInstantCard(args, errorsMapping, preScreenCode, true, true).then(data => {
      expect(data).toMatchObject(result);
    });
  });
  it('Should throw errors in case of server side error', () => {
    const result = {
      body: {
        errors: [
          {
            errorMessage,
          },
        ],
      },
    };
    executeStatefulAPICall.mockImplementation(() => Promise.reject(result));
    applyInstantCard(args, errorsMapping, preScreenCode, true, true).then(data => {
      expect(data).toEqual('Test Error Message');
    });
  });

  it('Should throw errors in case of unable to connect to server', () => {
    const result = {};
    executeStatefulAPICall.mockImplementation(() => Promise.reject(result));
    applyInstantCard(args, errorsMapping, preScreenCode, true, true).then(data => {
      expect(data).toEqual('Your action could not be completed due to system error');
    });
  });
});
