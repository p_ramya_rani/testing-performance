// 9fbef606107a605d69c0edbcd8029e5d
import { isGymboree } from '@tcp/core/src/utils/utils';
import { executeExternalAPICall, executeStatefulAPICall } from '../../../handler';
import { getAPIConfig, isCanada, isTCP, isMobileApp, getCustomHeaders } from '../../../../utils';

import { ACQUISITION_ID } from '../../../../config/acquisitionId.config';
import endpoints from '../../../endpoints';
/**
 * Abstractor layer for loading data from API for SMS and Email Signup
 */
const ErrorResponse = { success: false, error: 'Invalid' };
const SuccessResponse = (res) => {
  return {
    success: true,
    ...res,
  };
};
const NetworkErrorResponse = { success: true, status: 'valid' };

const Abstractor = {
  /**
   * @param {Object} reqBody body to the API
   * @return {Object} return promise.
   */
  subscribeEmail: (reqBody) => {
    const body = Abstractor.subscribeEmailBody(reqBody);
    const reqObj = {
      webService: endpoints.addEmailSignup,
      body,
    };

    return executeStatefulAPICall(reqObj)
      .then(Abstractor.processSubscriptionData)
      .catch(Abstractor.handleSubscriptionError);
  },
  /**
   * @param {Object} payload payload to the API
   * @return {Object} return promise.
   */
  subscribeSms: (payload = '') => {
    const { isTextOptInSecondBrand } = payload;
    const isGYM = isGymboree();
    const brandGYM = !!(isGYM || isTextOptInSecondBrand);
    const brandTCP = !!(!isGYM || isTextOptInSecondBrand);

    let subscriptionBrands = brandTCP ? 'TCP' : '';
    if (brandGYM) {
      subscriptionBrands = subscriptionBrands ? `${subscriptionBrands},GYM` : 'GYM';
    }

    const body = Abstractor.subscribeSmsBody(payload);

    const reqObj = {
      webService: endpoints.addSmsSignup,
      body,
      header: {
        brandSubscribe: subscriptionBrands,
      },
    };

    return executeStatefulAPICall(reqObj)
      .then(Abstractor.processSmsSubscriptionData)
      .catch(Abstractor.handleValidationError);
  },
  /**
   * @param {String} baseURI Base URL of the API
   * @return {Object} returns Promise.
   */
  verifyEmail: (emailAddress) => {
    const { URI, method, reqTimeout, authKey = '' } = endpoints.emailVerification;
    const header = isMobileApp() ? getCustomHeaders() : {};
    const payload = {
      webService: {
        URI: `${URI}`,
        method,
        reqTimeout,
      },
      header: authKey.length > 0 ? { Authorization: `ApiKey: ${authKey}`, ...header } : header,
      body: {
        email: emailAddress,
      },
    };
    return executeExternalAPICall(payload)
      .then(Abstractor.processData)
      .catch(Abstractor.handleNetworkCallError);
  },
  /**
   * @param {Object} res response object
   * @return {Object} returns success { success: true } or error ({ success: false, error: 'Invalid' }) object.
   */
  processSubscriptionData: (res) => {
    /**
     * The only way currently to confirm successful email subscription is that
     * we get redirectionurl with /email-confirmation on that.
     */
    if (
      res.body &&
      res.body.redirecturl &&
      res.body.redirecturl.indexOf('/email-confirmation') !== -1
    ) {
      return SuccessResponse(res.body);
    }
    return ErrorResponse;
  },
  /**
   * @param {Object} res response object
   * @return {Object} returns success { success: true } or error ({ success: false, error: 'Invalid' }) object.
   */
  processSmsSubscriptionData: (res) => {
    if (res.errors) {
      return ErrorResponse;
    }
    return SuccessResponse(res.body);
  },
  /**
   * @param {Object} res response object
   * @return {Object} returns success { success: true } or error ({ success: false, error: 'Invalid' }) object.
   */
  processData: (res) => {
    const validStatuses = ['valid', 'accept_all', 'unknown'];
    const invalidStatuses = ['invalid'];
    if (res.body && validStatuses.includes(res.body.status)) {
      return SuccessResponse(res.body);
    }
    if (res.body && invalidStatuses.includes(res.body.status)) {
      return ErrorResponse;
    }
    return NetworkErrorResponse;
  },
  /**
   * @return {Object} returns error ({ success: false, error: 'Invalid' }) object.
   */
  handleValidationError: () => {
    return ErrorResponse;
  },
  /**
   * @return {Object} returns error ({ success: false, error: 'Invalid' }) object.
   */
  handleSubscriptionError: () => {
    return ErrorResponse;
  },
  /**
   * @return {Object} returns error ({ success: true, status: 'valid' }) object.
   */
  handleNetworkCallError: () => {
    return NetworkErrorResponse;
  },
  processEmailSmsSubscription: (res, isEmail) => {
    if (
      isEmail &&
      res.body &&
      res.body.redirecturl &&
      res.body.redirecturl.indexOf('/email-confirmation') !== -1
    ) {
      return SuccessResponse(res.body);
    }
    if (res.body && res.body.errors) {
      return ErrorResponse;
    }
    return SuccessResponse(res.body);
  },
  emailSmsSignUp: (reqPayload) => {
    const { emailReqObj, smsReqObj, subscriptionBrands } = reqPayload;
    const emailBody = emailReqObj && Abstractor.subscribeEmailBody(emailReqObj);
    const smsBody = smsReqObj && Abstractor.subscribeSmsBody(smsReqObj);

    const reqObj = {
      webService: endpoints.emailSmsSignUp,
      body: {
        ...(emailBody && { email: emailBody }),
        ...(smsBody && { sms: smsBody }),
      },
      header: {
        brandSubscribe: subscriptionBrands,
      },
    };

    return executeStatefulAPICall(reqObj)
      .then((res) => Abstractor.processEmailSmsSubscription(res, !!emailReqObj))
      .catch(Abstractor.handleValidationError);
  },
  subscribeEmailBody: (reqBody) => {
    const { storeId, catalogId, langId, language = '', country = '' } = getAPIConfig();
    const body = {
      catalogId,
      storeId,
      langId,
      ...reqBody,
    };

    if (language.toLowerCase() === 'fr' && country.toLowerCase() === 'ca') {
      body.state = 'QC'; // /RWD-20847 Canada French identifiers are not being passed in Email Sign up (STATE=QC)
    }

    return body;
  },
  subscribeSmsBody: (payload) => {
    const { footerTopSmsSignup } = payload;
    let acquisitionId;

    if (isTCP() && isCanada()) {
      acquisitionId = ACQUISITION_ID.FOOTER.TCP_CA;
    }

    return {
      acquisition_id: acquisitionId,
      mobile_phone: {
        mdn: footerTopSmsSignup.replace(/\D/g, ''),
      },
      custom_fields: {
        src_cd: '1',
        sub_src_cd: 'sms_footer',
      },
    };
  },
};
export default Abstractor;
