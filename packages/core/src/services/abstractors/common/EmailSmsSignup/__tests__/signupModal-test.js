// 9fbef606107a605d69c0edbcd8029e5d
import SignupModalAbstractor from '../EmailSmsSignup';

jest.mock('../../../../../service/API');
jest.mock('../../../../handler/handler');

describe('SignupModalAbstractor', () => {
  test('Signup modal Abstractor Email subscription ', () => {
    return SignupModalAbstractor.subscribeEmail({ payload: 'URL' }).then((data) => {
      expect(data).toBeTruthy();
    });
  });

  test('Signup modal Abstractor SMS subscription ', () => {
    return SignupModalAbstractor.subscribeSms({ footerTopSmsSignup: '4083067249' }).then((data) => {
      expect(data.success).toBeTruthy();
    });
  });

  test('Signup modal Abstractor Email Verification ', () => {
    return SignupModalAbstractor.verifyEmail(
      'baseURL',
      'relURI',
      { payload: 'address=abf@gmail.com' },
      'post'
    ).then((data) => {
      expect(data.success).toBeTruthy();
    });
  });

  test('Signup modal Abstractor Email subscription with empty params', () => {
    return SignupModalAbstractor.subscribeEmail().then((data) => {
      expect(data.success).toBeFalsy();
    });
  });

  test('Signup modal Abstractor Email Verification with empty params', () => {
    return SignupModalAbstractor.verifyEmail().then((data) => {
      expect(data.success).toBeTruthy();
    });
  });

  test('Signup modal Abstractor combined subscription ', () => {
    return SignupModalAbstractor.emailSmsSignUp({
      emailReqObj: {
        emailaddr: 'abc@gmail.com',
        URL: 'email-confirmation',
        response: `valid:::false:false`,
        brandTCP: true,
        brandGYM: false,
        registrationType: 25,
      },
      subscriptionBrands: 'TCP',
    }).then((data) => {
      expect(data.success).toBeTruthy();
    });
  });
});
