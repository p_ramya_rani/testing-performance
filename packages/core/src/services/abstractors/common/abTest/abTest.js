/* eslint-disable react/destructuring-assignment */
// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import {
  readCookie,
  setCookie,
  getValueFromAsyncStorage,
  setValueInAsyncStorage,
  getUserAgent,
  getExperienceString,
  routerPush,
} from '../../../../utils';
import { executeExternalAPICall } from '../../../handler';
import PAGES from '../../../../constants/pages.constants';

const BAG_PAGE = 'BagPage';
export const abTestMonetate = { data: {} };

const ABTestAbstractors = {
  parseAbTestResponse: (offer) => {
    const abTestOffers = Array.isArray(offer) ? offer : [];
    let abtestScripts = '';
    const parsedAbtestResponse = abTestOffers
      .map((abtest) => {
        try {
          return abtest && abtest.content ? JSON.parse(abtest.content) : {};
        } catch (e) {
          if (abtest && abtest.content && abtest.content.indexOf('<script') >= 0) {
            abtestScripts += abtest.content;
          }
          return {};
        }
      })
      .reduce((accum, content) => {
        const key = Object.keys(content)[0];
        return key ? { ...accum, [key]: content[key] } : accum;
      }, {});
    return { parsedAbtestResponse, abtestScripts };
  },

  constructRouterURL: (path) => {
    if (!path) return null;
    if (path.includes('/c/')) {
      return `/c?cid=${path.split('/c/')[1]}`;
    }
    if (path.includes('/p/')) {
      return `/p?pid=${path.split('/p/')[1]}`;
    }
    if (path.includes('/search/')) {
      return `/search?searchQuery=${path.split('/search')[1]}`;
    }
    if (path.includes('/outfit/')) {
      const outfitPaths = path.split('/outfit')[1];
      return `outfit?outfitId=${outfitPaths.split('/')[0]}&vendorColorProductIdsList=${
        outfitPaths.split('/')[1]
      }`;
    }
    if (path.includes('/b/')) {
      return `/b?bid=${path.split('/b/')[1]}`;
    }
    return null;
  },

  fireRouteChange: (url) => {
    const route = new URL(url);
    const pathPassed = route.pathname.split('/us')[1];
    const pathToPush = ABTestAbstractors.constructRouterURL(pathPassed);
    if (pathToPush) routerPush(pathToPush, pathPassed);
  },

  getABObject: (actions) => {
    let abtestScripts = '';
    const parsedAbtestResponse = {};
    for (let i = 0; i < actions.length; i += 1) {
      const action = actions[i];
      if (action.url && typeof window !== 'undefined') {
        ABTestAbstractors.fireRouteChange(action.url);
      }
      // eslint-disable-next-line no-continue
      if (!(typeof action.component === 'undefined' && action.json)) continue;
      const keys = Object.keys(action.json);
      for (let j = 0; j < keys.length; j += 1) {
        const value = action.json[keys[j]];
        if (typeof value === 'boolean' || typeof value === 'object' || typeof value === 'number')
          parsedAbtestResponse[keys[j]] = value;
        else if (typeof value === 'string' && value.indexOf('<script') !== -1)
          abtestScripts = value;
      }
    }
    return { parsedAbtestResponse, abtestScripts };
  },

  extractOffersMonetate: (result) => {
    const { responses } = result.body.data;
    if (!responses.length) return {};
    const { actions } = responses[0];
    if (!actions.length) return {};
    return ABTestAbstractors.getABObject(actions);
  },

  getAllAbtestMonetate: ({ payload, quickAdd, displayLowInvMonetateExp }) => {
    const apiConfigObj = getAPIConfig();
    const { monetateUrl, monetateChannel } = apiConfigObj;
    const { pathname } = payload;
    const isBagPage = pathname.match(/\/bag$/g);
    const { reqHostname } = getAPIConfig();
    const host = window && window.location ? window.location.host : '';
    const url = `https://${reqHostname || host}${pathname}`;
    const bodyParameters = {
      channel: monetateChannel,
      events: [
        {
          eventType: 'monetate:decision:DecisionRequest',
          // eslint-disable-next-line no-restricted-properties
          requestId: `${Math.round(Math.random() * Math.pow(10, 4))}`,
          includeReporting: true,
        },
        {
          eventType: 'monetate:context:UserAgent',
          userAgent: navigator.userAgent,
        },
        {
          eventType: 'monetate:context:PageView',
          url,
          pageType: payload.page || '',
        },
        {
          eventType: 'monetate:context:CustomVariables',
          customVariables: [
            {
              variable: 'apiRequestForMonetate',
              value: 'abtest',
            },
            ...(isBagPage
              ? [
                  {
                    variable: 'quickAdd',
                    value: quickAdd.toString(),
                  },
                  {
                    variable: 'displayLowInvMonetateExp',
                    value: displayLowInvMonetateExp,
                  },
                ]
              : []),
          ],
        },
      ],
    };

    const monetateIdCookie = readCookie('mt.v');
    if (monetateIdCookie) bodyParameters.monetateId = monetateIdCookie;
    const emailHash = getLocalStorage('email_hash');
    if (emailHash) bodyParameters.customerId = emailHash;

    return executeExternalAPICall({
      webService: {
        method: 'POST',
        URI: monetateUrl,
      },
      body: bodyParameters,
    }).then((response) => {
      const { monetateId } = response.body.meta;
      if (typeof monetateId !== 'undefined')
        setCookie({
          key: 'mt.v',
          value: monetateId,
          daysAlive: 1,
        });

      const { responses } = response.body.data;
      const { actions } = responses[0];
      if (actions.length) {
        actions.forEach((action) => {
          if (action.impressionReporting) getExperienceString(...action.impressionReporting);
        });
      }

      return ABTestAbstractors.extractOffersMonetate(response);
    });
  },

  getAppAbtestMonetate: async ({ payload, quickAdd, displayLowInvMonetateExp }) => {
    const { parameters = {} } = payload || {};
    const currentScreenInfo = parameters.currentScreen;
    const currentScreen =
      (parameters.currentScreen && parameters.currentScreen.routeName) || PAGES.HOME_PAGE;
    const apiConfigObj = getAPIConfig();
    const { monetateUrl, monetateChannel } = apiConfigObj;
    const isBagPage = currentScreen === BAG_PAGE;
    const bodyParameters = {
      channel: monetateChannel,
      events: [
        {
          eventType: 'monetate:decision:DecisionRequest',
          // eslint-disable-next-line no-restricted-properties
          requestId: `${Math.round(Math.random() * Math.pow(10, 4))}`,
          includeReporting: true,
        },
        {
          eventType: 'monetate:context:UserAgent',
          userAgent: getUserAgent(),
        },
        {
          eventType: 'monetate:context:PageView',
          pageType: currentScreen,
        },
        {
          eventType: 'monetate:context:CustomVariables',
          customVariables: [
            {
              variable: 'deviceType',
              value: 'Mobile',
            },
            {
              variable: 'apiRequestForMonetate',
              value: 'abtest',
            },
            ...(isBagPage
              ? [
                  {
                    variable: 'quickAdd',
                    value: quickAdd.toString(),
                  },
                  {
                    variable: 'displayLowInvMonetateExp',
                    value: displayLowInvMonetateExp,
                  },
                ]
              : []),
          ],
        },
      ],
    };
    let seo = '';
    seo = currentScreenInfo
      ? await ABTestAbstractors.getSeoTokenForMobileABTest(currentScreen, currentScreenInfo)
      : '';

    if (seo) {
      bodyParameters.events[2].url = `${
        parameters.webAppDomain
      }/mobileapp/${currentScreen.toLowerCase()}/${seo}`;
    } else {
      bodyParameters.events[2].url = `${
        parameters.webAppDomain
      }/mobileapp/${currentScreen.toLowerCase()}`;
    }

    const monetateIdCookie = await getValueFromAsyncStorage('monetateId');
    if (monetateIdCookie) bodyParameters.monetateId = monetateIdCookie;
    const emailHash = await getValueFromAsyncStorage('email_hash');
    if (emailHash) bodyParameters.customerId = emailHash;

    return executeExternalAPICall({
      webService: {
        method: 'POST',
        URI: monetateUrl,
      },
      body: bodyParameters,
    }).then(async (response) => {
      abTestMonetate.data = response.body.data;
      const { monetateId } = response.body.meta;
      if (typeof monetateId !== 'undefined') await setValueInAsyncStorage('monetateId', monetateId);
      return ABTestAbstractors.extractOffersMonetate(response);
    });
  },

  getSeoTokenForMobileABTest: async (currentScreen, currentScreenInfo) => {
    const { params = {} } = currentScreenInfo;
    const { navigationObj = {} } = params;
    if (currentScreen === 'ProductDetail') return params.pdpUrl;
    if (currentScreen === 'ProductListing') {
      const { url = '' } = params;
      if (url !== '' && typeof url === 'string') return url.split('=')[1];
    }
    return ABTestAbstractors.getSeoForNavCategorieMobile(currentScreen, navigationObj);
  },

  getSeoForNavCategorieMobile: (currentScreen, navigationObj) => {
    if (currentScreen === 'NavMenuLevel2') {
      const { item = {} } = navigationObj;
      const { categoryContent = {} } = item;
      const { seoToken = {} } = categoryContent;
      if (navigationObj && item && categoryContent && seoToken) return seoToken;
    }
    if (currentScreen === 'NavMenuLevel3') {
      const { url = '' } = navigationObj[0];
      if (url !== '' && typeof url === 'string') return url.split('=')[1];
    }
    return false;
  },

  getABData: (payload) => {
    const {
      payload: { MONETATE_ENABLED = true },
    } = payload;
    if (MONETATE_ENABLED) return ABTestAbstractors.getAllAbtestMonetate(payload);
    return null;
  },

  getAppAbtest: (payload) => {
    const {
      payload: {
        parameters: { MONETATE_ENABLED = true },
      },
    } = payload;
    if (MONETATE_ENABLED) return ABTestAbstractors.getAppAbtestMonetate(payload);
    return null;
  },
};

export default ABTestAbstractors;
