// 9fbef606107a605d69c0edbcd8029e5d 
import { getNearByStoreApi, hasGymboreeStores } from '../storeDetail';
import { executeStatefulAPICall } from '../../../../handler';
import nearByMock from '../__mocks__/nearByStores';

jest.mock('../../../../handler', () => ({
  executeStatefulAPICall: jest.fn(),
}));

describe('Store Detail Abstractor', () => {
  test('getNearByStoreApi', () => {
    executeStatefulAPICall.mockImplementation(() => Promise.resolve(nearByMock));
    const payloadData = {
      storeLocationId: '',
      getNearby: true,
      latitude: 40.69112,
      longitude: -73.98625,
    };
    const nearByStores = getNearByStoreApi(payloadData);
    nearByStores.then(stores => {
      expect(stores.length).toEqual(0);
    });
  });
  test('hasGymboreeStores', () => {
    const isGymStores = hasGymboreeStores({
      x_brands: ['TCP', 'GYM'],
    });
    expect(isGymStores).toBeTruthy();
  });
});
