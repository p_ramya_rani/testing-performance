// 9fbef606107a605d69c0edbcd8029e5d 
import gql from 'graphql-tag';
import logger from '@tcp/core/src/utils/loggerInstance';
import { parseBoolean } from '@tcp/core/src/utils';
import handler, { executeStatefulAPICall } from '../../../handler';
import { parseStoreHours } from '../../../../utils/parseStoreHours';
import endpoints from '../../../endpoints';
import { getBasicInfo } from './storeLocator';
import storeDetailQuery from '../../../handler/graphQL/queries/storeDetail';

const STORE_TYPES = {
  RETAIL: 'Retail Store',
  OUTLET: 'Outlet',
};

const BRAND_TYPE = {
  gymboree: 'GYM',
  tcp: 'TCP',
};

const STORE_HOURS = 'STORE_HOURS_JSON';
const STORE_PLACE = 'PLACE';
/**
 * @function errorHandler function to handle all the server side errors.
 * @param {object} err - error object in case server side data send server side validation errors.
 * @returns {object} error object with appropirate error message
 */
const errorHandler = err => {
  return err || null;
};

/**
 * @function hasGymboreeStores to check and return a boolean value if Gymboree brand.
 * @param {object} storeInfo - store object
 * @returns {boolean} returns true is store is a gymboree store.
 */
export const hasGymboreeStores = storeInfo => {
  const gymStoreArr = storeInfo.x_brands || storeInfo.brands;
  return (
    (Array.isArray(gymStoreArr) && gymStoreArr.includes(BRAND_TYPE.gymboree)) ||
    gymStoreArr.includes(BRAND_TYPE.gymboree)
  );
};

const formatStoreType = storeInfo => {
  const { storeType, addressLine } = storeInfo;
  return storeType || (addressLine && addressLine[addressLine.length - 1]) || '';
};

const getStoreHours = storeInfo => {
  const storeHours = JSON.parse(storeInfo.attribute).filter(item => item.name === STORE_HOURS);
  return JSON.parse(storeHours[0].displayValue || '{}').storehours || [];
};

export const getCurrentStoreInfoApi = {
  getData: (brand, country, stlocId, apiConfig) => {
    const queryData = storeDetailQuery.getQuery({ brand, country, stlocId });
    const query = gql(queryData.query);
    return handler
      .executeGraphQLQuery({ query, cacheTag: queryData.cacheTag, isContextual: false, apiConfig })
      .then(getCurrentStoreInfoApi.processData)
      .catch(getCurrentStoreInfoApi.handleError);
  },
  processData: ({ data }) => {
    const { storeDetails: storeInfo } = data;
    const hoursOfOperation = storeInfo.attribute ? getStoreHours(storeInfo) : [];
    const storeType = formatStoreType(storeInfo);
    const {
      x_mallType: mallType,
      x_entranceType: entranceType,
      isBopisAvailable,
      x_bossMinDate: bossMinDate,
      x_bossMaxDate: bossMaxDate,
      x_isStoreBOSSEligible: isBossEligible,
      isStoreBossSelected,
      isStoreBopisSelected,
    } = storeInfo;
    const parsedResponse = {
      basicInfo: {
        ...getBasicInfo(storeInfo),
        x_stloc: storeInfo.x_stloc
          ? JSON.parse(storeInfo.x_stloc)
          : { heading: '', bodyCopy: '', pageTitle: '' },
      },
      isGym: hasGymboreeStores(storeInfo),
      hours: {
        regularHours: [],
        holidayHours: [],
        regularAndHolidayHours: [],
      },
      features: {
        storeType:
          STORE_TYPES[storeType] || (storeType === STORE_PLACE && STORE_TYPES.RETAIL) || '',
        mallType,
        entranceType,
        isBopisAvailable,
        bossMinDate,
        bossMaxDate,
      },
      storeBossInfo: {
        isBossEligible: parseBoolean(isBossEligible),
        startDate: bossMinDate,
        endDate: bossMaxDate,
      },
      /** added storeType | also checking if the flag is undefined than the value should be true
       * for default searching without any restriction
       */
      pickupType: {
        isStoreBossSelected: isStoreBossSelected !== undefined ? isStoreBossSelected : true,
        isStoreBopisSelected: isStoreBopisSelected !== undefined ? isStoreBopisSelected : true,
      },
    };
    if (hoursOfOperation.length) {
      parsedResponse.hours.regularHours = parseStoreHours(hoursOfOperation);
    }
    return parsedResponse;
  },
  handleError: e => {
    logger.error(e);
    return e || null;
  },
};

export const cleanStr = str => str.replace(/-| |\./g, '');

export const getNearByStoreApi = payload => {
  const { storeLocationId, getNearby, maxDistance, maxStoreCount, latitude, longitude } = payload;
  const payloadData = {
    header: {
      stlocId: storeLocationId,
      latitude,
      longitude,
    },
    body: {
      distance: maxDistance || 25,
      maxStores: maxStoreCount || 5,
    },
    webService: endpoints.getNearByStore,
  };
  return executeStatefulAPICall(payloadData)
    .then(res => {
      const response = res.body;
      const parsedStoreInfo = {};
      const nearByStores = getNearby && response.getStoreLocatorByLatLngResponse.result;
      const filteredNearByStores = getNearby && nearByStores;
      parsedStoreInfo.nearByStores = filteredNearByStores.map(fStore => {
        const nearbyStoreParsedInfo = {
          basicInfo: getBasicInfo(fStore),
          hours: {
            regularHours: [],
            holidayHours: [],
            regularAndHolidayHours: [],
          },
          isGym: hasGymboreeStores(fStore),
          features: {
            storeType:
              (fStore.storeType === 'PLACE' ? STORE_TYPES.RETAIL : STORE_TYPES[fStore.storeType]) ||
              '',
            redirectURL: `${cleanStr(fStore.storeName)}-${cleanStr(fStore.city)}-${cleanStr(
              fStore.streetLine1
            )}-${fStore.state}-${fStore.storeUniqueID}`,
          },
        };
        if (fStore.storehours.storehours) {
          nearbyStoreParsedInfo.hours.regularHours = parseStoreHours(fStore.storehours.storehours);
        }
        return nearbyStoreParsedInfo;
      });

      return parsedStoreInfo;
    })
    .catch(errorHandler);
};
