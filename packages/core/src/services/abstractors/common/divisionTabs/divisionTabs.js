// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils';
import handler from '../../../handler';

/**
 * Responsible for content fetching using content ID
 */
export const DivisionTabsDataAbstractor = {
  processData: async (result) =>
    (result &&
      result.data &&
      result.data.slot_1 &&
      result.data.slot_1.composites &&
      result.data.slot_1.composites.buttonList) ||
    [],

  getData: async (contentId, queryName = 'divisionTabs') => {
    const { language } = getAPIConfig();
    const moduleConfig = {
      name: queryName,
      data: {
        contentId,
        lang: language === 'en' ? '' : language,
        slot: 'slot_1',
      },
    };

    return handler.fetchModuleDataFromGraphQL(moduleConfig).then((response) => response);
  },
};

/**
 * Responsible for loading the rich text from CMS
 * @param {Array} cids - Content ID
 */
export const getDivisionTabs = async (contentId) => {
  const { getData, processData } = DivisionTabsDataAbstractor;
  let response = {};
  try {
    const result = await getData(contentId);
    response = await processData(result);
  } catch (error) {
    response = error;
  }
  return response;
};

export const getDivisionTabsCSH = async (contentId) => {
  const { getData, processData } = DivisionTabsDataAbstractor;
  let response = {};
  try {
    const result = await getData(contentId, 'divisionTabsCSH');
    response = await processData(result);
  } catch (error) {
    response = error;
  }
  return response;
};
