// 9fbef606107a605d69c0edbcd8029e5d 
import handler from '../../../handler';

/**
 * Responsible for content fetching using content ID
 */
export const UserOnboardingModuleAbstractor = {
  getData: async id => {
    const response = await handler.fetchModuleDataFromGraphQL({
      name: 'userOnboardingModule',
      data: { cid: id, type: 'userOnboardingModule' },
    });

    return response ? response.data.userOnboardingModule : {};
  },
};

export default UserOnboardingModuleAbstractor;
