// 9fbef606107a605d69c0edbcd8029e5d 
import * as utils from '@tcp/core/src/utils/utils';
import GetAppVersionAbstractor from '../getAppVersion';

describe('GetAppVersionAbstractor', () => {
  const payload = {
    body: {
      apps: [
        {
          id: 'gym',
          name: 'Gymboree',
          version: {
            ios: '1.0.0',
            android: '1.0.0',
          },
          urls: {
            ios: '',
            android: '',
          },
        },
        {
          id: 'tcp',
          name: "The Children's Place",
          version: {
            ios: '1.0.2',
            android: '1.0.2',
          },
          urls: {
            ios: '',
            android: '',
          },
        },
      ],
    },
  };

  const responseMockGym = {
    iosAppVersion: '1.0.0',
    androidAppVersion: '1.0.0',
    iOSAppUpdateUrl: '',
    androidAppUpdateUrl: '',
  };
  const responseMockTcp = {
    iosAppVersion: '1.0.2',
    androidAppVersion: '1.0.2',
    iOSAppUpdateUrl: '',
    androidAppUpdateUrl: '',
  };

  test('Get App Version Abstractor Version Info for Gymboree', () => {
    utils.isGymboree = jest.fn().mockReturnValue(true);
    expect(GetAppVersionAbstractor.getSuccessData(payload)).toMatchObject(responseMockGym);
  });

  test('Get App Version Abstractor Version Info for Tcp', () => {
    utils.isGymboree = jest.fn().mockReturnValue(false);
    expect(GetAppVersionAbstractor.getSuccessData(payload)).toMatchObject(responseMockTcp);
  });
});
