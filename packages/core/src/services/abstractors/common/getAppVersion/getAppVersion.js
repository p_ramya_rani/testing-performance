// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils';
import { executeExternalAPICall } from '../../../handler';
import { isGymboree } from '../../../../utils';
import endpoints from '../../../endpoints';

/**
 * Abstractor layer for loading data from API for App Version
 */
const Abstractor = {
  /**
   * @return {Object} return promise.
   */
  getAppVersion: () => {
    const { appUpdateDomain } = getAPIConfig();
    const { getAppUpdatedVersion } = endpoints;
    const webService = {
      ...getAppUpdatedVersion,
      URI: `${appUpdateDomain}${getAppUpdatedVersion.URI}`,
    };
    const reqObj = {
      webService,
    };

    return executeExternalAPICall(reqObj).then(Abstractor.getSuccessData);
  },

  /**
   * @param {Object} response object
   * @return {Object} returns  {  appVersion, iOSAppUpdateUrl, androidAppUpdateUrl }  object.
   */
  getSuccessData: (response) => {
    const { body } = response;
    const [gymApp, tcpApp] = body.apps;
    let appVersionInfo = tcpApp;

    if (isGymboree()) {
      appVersionInfo = gymApp;
    }

    const {
      version: {
        ios: iosAppVersion,
        android: androidAppVersion,
        androidCodepushVersion: androidCPVersion,
        iosCodepushVersion: iosCPVersion,
      },
      urls: { ios: iOSAppUpdateUrl, android: androidAppUpdateUrl },
    } = appVersionInfo;
    return {
      iosAppVersion,
      androidAppVersion,
      iOSAppUpdateUrl,
      androidAppUpdateUrl,
      androidCPVersion,
      iosCPVersion,
    };
  },
};

export default Abstractor;
