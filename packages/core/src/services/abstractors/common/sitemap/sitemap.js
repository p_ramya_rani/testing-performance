// 9fbef606107a605d69c0edbcd8029e5d 
import gql from 'graphql-tag';
import logger from '@tcp/core/src/utils/loggerInstance';
import handler from '../../../handler';
import siteMapQuery from '../../../handler/graphQL/queries/sitemap';

/**
 * Abstractor layer for getting sitemap data
 */
const Abstractor = {
  getData: (data, apiConfig) => {
    const queryData = siteMapQuery.getQuery(data);
    const query = gql(queryData.query);
    return handler
      .executeGraphQLQuery({ query, cacheTag: queryData.cacheTag, apiConfig })
      .then(Abstractor.processData)
      .catch(Abstractor.handleError);
  },
  processData: data => data,
  handleError: e => logger.error(e),
};

export default Abstractor;
