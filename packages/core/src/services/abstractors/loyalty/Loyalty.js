// 9fbef606107a605d69c0edbcd8029e5d 
import { executeStatefulAPICall } from '../../handler';
import endpoints from '../../endpoints';
import { responseContainsErrors, ServiceResponseError } from '../../../utils/errorMessage.util';

export const getComputedLoyaltyPoints = (params, pointsApiCall) =>
  executeStatefulAPICall({
    body: params,
    webService:
      pointsApiCall === 'brierley' ? endpoints.getLoyaltyPointsMule : endpoints.getLoyaltyPoints,
  })
    .then(res => {
      if (responseContainsErrors(res)) {
        throw new ServiceResponseError(res);
      }
      return {
        loyaltyUserPoints: res.body.data.userPoints,
        orderItemLoyalty: res.body.data.orderItems,
        pointsToNextReward: res.body.data.pointsToNextReward,
        earnedReward: res.body.data.earnedReward,
      };
    })
    .catch(err => {
      throw err;
    });

export default {
  getComputedLoyaltyPoints,
};
