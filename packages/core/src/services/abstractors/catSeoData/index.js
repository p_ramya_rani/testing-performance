import logger from '../../../utils/logger';
import handler from '../../handler';

const Abstractor = {
  getData: (module, data, apiConfig) => {
    return handler
      .fetchModuleDataFromGraphQL({ name: module, data }, apiConfig)
      .then((response) => response.data)
      .catch((error) => {
        logger.error(error);
        return {
          categorySeoData: {
            seoTitle: '',
            seoMetaDesc: '',
            seoCanonicalUrl: '',
            longDescription: '',
          },
        };
      });
  },
};

export default Abstractor;
