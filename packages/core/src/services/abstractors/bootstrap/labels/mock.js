// 9fbef606107a605d69c0edbcd8029e5d 
export default [
  {
    name: 'global',
    subcategories: [
      {
        name: 'footer',
        labels: [
          {
            key: 'connect_with_us',
            value: 'CONNECT WITH US:',
          },
          {
            key: 'reference_id',
            value: 'Reference ID: ',
          },
        ],
      },
    ],
  },
];
