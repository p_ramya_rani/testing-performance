// 9fbef606107a605d69c0edbcd8029e5d 
import handler from '../../../handler';

/**
 * Abstractor layer for loading data from API for Labels related components
 */
const Abstractor = {
  getData: (module, data, apiConfig) => {
    return handler
      .fetchModuleDataFromGraphQL({ name: module, data }, apiConfig)
      .then(response => {
        return response.data.labels;
      })
      .then(Abstractor.processData);
  },
  processData: data => {
    const result = {};
    data.map(labelData => {
      const { name: category, subcategories } = labelData;
      result[category] = {};
      subcategories.map(({ name: subcategory, labels, referred }) => {
        result[category][subcategory] = {};
        result[category][subcategory].referred = referred;
        labels.map(({ key, value }) => {
          result[category][subcategory][key] = value;
          return value;
        });
        return subcategory;
      });
      return labelData;
    });

    return result;
  },
};
export default Abstractor;
