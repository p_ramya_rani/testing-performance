// 9fbef606107a605d69c0edbcd8029e5d 
import LabelsAbstractor from '../labels';
import mock from '../mock';

jest.mock('../../../../handler/handler');

it('Label Abstractor | ', () => {
  LabelsAbstractor.getData('labels', {
    category: 'global',
    subCategory: 'footer',
    brand: 'TCP',
    country: 'USA',
    channel: 'Desktop',
  }).then(data => {
    expect(data).toMatchObject(mock);
  });
});
