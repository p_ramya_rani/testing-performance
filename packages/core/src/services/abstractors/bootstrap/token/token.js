import { getPlaceSiteId } from '@tcp/core/src/utils';
import endpoints from '../../../endpoints';
import { executeStatefulAPICall } from '../../../handler';
/**
 * Abstractor layer for loading data from API for SFCC token
 */
const Abstractor = {
  getData: async requestParams => {
    const { headers = {} } = requestParams || {};
    const payload = {
      webService: endpoints.getSFCCAuthToken,
      header: {
        ...headers,
        siteId: getPlaceSiteId(),
      },
    };
    // SFCC-TODO: Remove additional passed parameters(null, true)
    const response = await executeStatefulAPICall(payload, null, true);
    return response.body.data;
  },
};
export default Abstractor;
