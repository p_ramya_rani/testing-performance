import tokenAbstractor from '../token';

import { executeStatefulAPICall } from '../../../../handler/handler';

jest.mock('../../../../handler/handler', () => ({
  executeStatefulAPICall: jest.fn(),
}));

const tokenData = {
  token: 'eSghsdEWB3536Vfdh',
};
const apiResponse = {
  body: {
    data: tokenData,
  },
};

describe('Token Abstractor', () => {
  it('Get Token', () => {
    executeStatefulAPICall.mockImplementation(() => new Promise(resolve => resolve(apiResponse)));

    tokenAbstractor.getData().then(data => {
      expect(data).toMatchObject(tokenData);
    });
  });
});
