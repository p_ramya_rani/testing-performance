/* eslint-disable sonarjs/no-duplicate-string */
// 9fbef606107a605d69c0edbcd8029e5d
import NavigationAbstractor, { getCategoryOverrideName } from '../navigation';
import mock from './mockData';

jest.mock('../../../../handler/handler');

it('Navigation Abstractor | ', () => {
  NavigationAbstractor.getData({
    name: 'header',
    data: {
      type: 'header',
      brand: 'TCP',
      country: 'USA',
      channel: 'Desktop',
    },
  }).then(data => {
    expect(data).toMatchObject(mock);
  });
});

it('Navigation Abstractor | getCategoryOverrideName - to return original name when no alternate provided', () => {
  expect(getCategoryOverrideName({}, 'original name')).toEqual('original name');
});

it('Navigation Abstractor | getCategoryOverrideName - to return override name when provided', () => {
  expect(
    getCategoryOverrideName(
      {
        categoryContent: {
          mainCategory: {
            set: [
              {
                key: 'categoryName',
                val: 'category name',
              },
            ],
          },
        },
      },
      'original name'
    )
  ).toEqual('category name');
});
