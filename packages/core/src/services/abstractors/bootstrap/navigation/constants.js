// 9fbef606107a605d69c0edbcd8029e5d 
export const UNIDENTIFIED_GROUP = 'UNIDENTIFIED_GROUP';
export const UNIDENTIFIED_GROUP_SEQUENCE = 99;
export const BIRTHDAY_CATEGORY_ID = '458035';
export const BIRTHDAY_CATEGORY__PATH = 'birthday-shop';

export default {
  UNIDENTIFIED_GROUP,
  UNIDENTIFIED_GROUP_SEQUENCE,
  BIRTHDAY_CATEGORY_ID,
  BIRTHDAY_CATEGORY__PATH,
};
