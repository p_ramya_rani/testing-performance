// 9fbef606107a605d69c0edbcd8029e5d
import logger from '@tcp/core/src/utils/loggerInstance';
import { UNIDENTIFIED_GROUP, UNIDENTIFIED_GROUP_SEQUENCE } from './constants';
import handler from '../../../handler';
import {
  errorComponents,
  errorMessages,
  defaultBrand,
  defaultCountry,
  defaultChannel,
  MobileChannel,
} from '../../../api.constants';
import { generateSessionId } from '../../../../utils/cookie.util';
import { isMobileApp, getAPIConfig, parseBoolean } from '../../../../utils';
import { API_CONFIG } from '../../../config';

export const getCategoryOverrideName = (subCatL3, originalName) => {
  const setObj = subCatL3?.categoryContent?.mainCategory?.set?.filter(
    setEntry => setEntry.key === 'categoryName' && setEntry.val
  );
  return (setObj && setObj[0] && setObj[0].val) || originalName;
};

/**
 * This function submits errors to Raygun
 * @param {Object} errorArgs containing {errorMessage, errorObject, component, payload}
 */
const errorHandler = errorArgs => {
  const { errorMessage, errorObject, component, payload } = errorArgs;
  const { sessionCookieKey } = API_CONFIG;
  logger.error({
    error: errorMessage,
    errorTags: [component],
    extraData: {
      ...payload,
      errorComponent: component,
      errorObject,
      errorMessage,
      [sessionCookieKey]: !isMobileApp() && generateSessionId(true),
    },
  });
};

/**
 * Abstractor layer for loading data from API for Navigation
 */
const Abstractor = {
  getOverrideUrl: (mainCategory, onlyCid) => {
    let overrideCid = '';
    let absoluteURL = '';
    if (mainCategory && mainCategory.set) {
      const setObj = mainCategory.set;
      setObj.forEach(({ key, val }) => {
        if (key === 'sourceURL') {
          overrideCid = val && val.split('/c/').length > 1 && val.split('/c/')[1];
          if (!overrideCid && !isMobileApp()) {
            absoluteURL = val;
          }
        }
      });
    }
    if (overrideCid && !onlyCid) {
      overrideCid = `${overrideCid}#redirect`;
    }
    return { overrideCid, absoluteURL };
  },
  /**
   * This function generate URL for the link
   * @param {*} seoUrl This parameter takes the highest priority
   * @param {*} seoToken This parameter is appended to form url in format "/c/{seoToken}" and takes 2nd priority
   * @param {*} catgroupId This parameter is appended to form url in format "/c/{catgroupId}" and takes last priority
   */
  constructUrl: props => {
    if (!props) return '';
    const { seoUrl, seoToken, catgroupId, mainCategory } = props;
    let cid = seoToken || catgroupId;
    const { overrideCid, absoluteURL } = Abstractor.getOverrideUrl(mainCategory);
    if (overrideCid) {
      cid = overrideCid;
    }
    return (
      absoluteURL ||
      seoUrl ||
      `/${
        seoToken && seoToken.startsWith('content-')
          ? seoToken.replace(new RegExp('content-', 'g'), 'content/')
          : `c?cid=${cid}`
      }`
    );
  },
  getInitialCidVal: (seoToken, catgroupId) => seoToken || catgroupId,
  constructAsPathForUrl: props => {
    if (!props) return '';
    const { seoUrl, seoToken, catgroupId, mainCategory } = props;
    let cid = Abstractor.getInitialCidVal(seoToken, catgroupId);
    const { overrideCid, absoluteURL } = Abstractor.getOverrideUrl(mainCategory, true);
    if (overrideCid) {
      cid = overrideCid;
    }
    return (
      absoluteURL ||
      seoUrl ||
      `/${
        seoToken && seoToken.startsWith('content-')
          ? seoToken.replace(new RegExp('content-', 'g'), 'content/')
          : `c/${cid}`
      }` ||
      ''
    );
  },
  getRawData: (module, data) => {
    return handler.fetchRawDataFromGraphQL({ name: module, data });
  },
  getData: ({ module, data }, config) => {
    const apiConfig = config || getAPIConfig();
    const isMobileChannel = isMobileApp() || apiConfig.isAppChannel;
    const channelName = isMobileChannel ? MobileChannel : defaultChannel;
    const brand = (apiConfig && apiConfig.brandIdCMS) || defaultBrand;
    const channel = channelName;
    const country = (apiConfig && apiConfig.siteIdCMS) || defaultCountry;
    const lang = apiConfig.language !== 'en' ? apiConfig.language : '';
    const { filter } = data;

    return handler
      .fetchModuleDataFromGraphQL(
        {
          name: module,
          data: {
            ...data,
            brand,
            country,
            channel,
            lang,
          },
        },
        apiConfig
      )
      .then(response => {
        return response.data.navigation || response.data;
      })
      .then(Abstractor[filter] || Abstractor.processData);
  },
  getHiddenCategory: displayToCustomer => {
    return displayToCustomer === true || displayToCustomer === '0' || displayToCustomer === '';
  },
  processSubCategoryName: (l1Name = '', subCatName = '') => {
    const indexOfPipe = subCatName.indexOf('|');
    let labelToPrint = subCatName;
    if (indexOfPipe !== -1) {
      const splittedSubCatName = subCatName.split('|');
      const smallCaseL1Name = l1Name ? l1Name.toLowerCase() : '';
      const smallCaseSubCatName = splittedSubCatName[0] ? splittedSubCatName[0].toLowerCase() : '';
      if (smallCaseL1Name === smallCaseSubCatName) {
        [, labelToPrint] = splittedSubCatName;
      } else {
        [labelToPrint] = splittedSubCatName;
      }
    }
    return labelToPrint;
  },
  constructCategoryContent: listItem => {
    const { categoryContent } = listItem;
    if (categoryContent) {
      categoryContent.url = Abstractor.constructUrl(listItem.categoryContent);
      categoryContent.asPath = Abstractor.constructAsPathForUrl(listItem.categoryContent);
      categoryContent.displayToCustomer = Abstractor.getHiddenCategory(
        categoryContent.displayToCustomer
      );
      categoryContent.isShortImage = parseBoolean(categoryContent.isShortImage);
    }
    return categoryContent;
  },
  getCategory: ({ capitalName, accessoriesCat, shoesCat, groupIdentifierName }) => {
    const isAccessoriesOrShoesCategories =
      capitalName === accessoriesCat || capitalName === shoesCat;
    let category = '';
    if (isAccessoriesOrShoesCategories && groupIdentifierName) {
      category = 'Categories';
    } else {
      category = groupIdentifierName || UNIDENTIFIED_GROUP;
    }
    return category;
  },
  getL2GroupContent: (categoryContent, l1Name) => {
    const { groupIdentifierSequence, groupIdentifierName } = categoryContent;
    const capitalName = l1Name.toUpperCase();
    const accessoriesCat = 'Accessories'.toUpperCase();
    const shoesCat = 'Shoes'.toUpperCase();
    const category = Abstractor.getCategory({
      capitalName,
      accessoriesCat,
      shoesCat,
      groupIdentifierName,
    });
    const order = groupIdentifierSequence || UNIDENTIFIED_GROUP_SEQUENCE;
    const label = groupIdentifierName || '';
    return { category, order, label };
  },
  processSubCategories: listItem => {
    const { categoryContent } = listItem;
    const subCategories = {};
    (listItem.subCategories || []).map(subCategory => {
      const subCat = subCategory;
      const updatedL2Name = Abstractor.processSubCategoryName(
        categoryContent.name,
        subCat.categoryContent.name
      );
      subCat.categoryContent = {
        ...subCat.categoryContent,
        name: updatedL2Name,
      };
      const { category, order, label } = Abstractor.getL2GroupContent(
        subCat.categoryContent || {},
        categoryContent.name || ''
      );

      if (!subCategories[category]) {
        subCategories[category] = {
          label,
          order,
          items: [],
          hasSubCategory: false,
        };
      }
      subCat.hasL3 = subCategory.subCategories && subCategory.subCategories.length;
      subCat.url = Abstractor.constructUrl(subCategory.categoryContent);
      subCat.asPath = Abstractor.constructAsPathForUrl(subCategory.categoryContent);
      subCat.categoryContent.displayToCustomer = Abstractor.getHiddenCategory(
        subCategory.categoryContent.displayToCustomer
      );
      subCat.categoryContent.isShortImage = parseBoolean(subCategory.categoryContent.isShortImage);
      subCat.categoryContent.excludeAttribute = subCategory.categoryContent.excludeAttribute;
      subCat.categoryContent.canonicalUrl = subCategory.categoryContent.canonicalUrl;

      subCat.categoryFilters = [];
      subCat.subCategories.map(subCategoryL3 => {
        const subCatL3 = subCategoryL3;
        subCatL3.url = Abstractor.constructUrl(subCategoryL3.categoryContent);
        subCatL3.categoryContent.url = Abstractor.constructUrl(subCategoryL3.categoryContent);
        subCatL3.categoryContent.categoryId = subCategoryL3.categoryContent.catgroupId;
        subCatL3.asPath = Abstractor.constructAsPathForUrl(subCategoryL3.categoryContent);
        subCatL3.categoryContent.displayToCustomer = Abstractor.getHiddenCategory(
          subCategoryL3.categoryContent.displayToCustomer
        );
        const overrideCategoryName = getCategoryOverrideName(
          subCatL3,
          subCategoryL3.categoryContent.name
        );
        if (
          subCatL3.categoryContent &&
          subCatL3.categoryContent.displayToCustomer &&
          subCatL3.categoryContent.mainCategory
        ) {
          if (
            subCatL3.categoryContent.mainCategory.categoryFilterImage &&
            subCatL3.categoryContent.mainCategory.categoryFilterImage[0]
          ) {
            subCat.categoryFilters.push({
              ...subCatL3.categoryContent.mainCategory.categoryFilterImage[0],
              linkUrl: subCatL3.url,
              linkAspath: subCatL3.asPath,
              filterTitle: overrideCategoryName,
            });
          } else {
            subCat.categoryFilters.push({
              linkUrl: subCatL3.url,
              linkAspath: subCatL3.asPath,
              filterTitle: overrideCategoryName,
            });
          }
        }
        subCatL3.categoryContent.isShortImage = parseBoolean(
          subCategoryL3.categoryContent.isShortImage
        );
        subCatL3.categoryContent.excludeAttribute = subCategoryL3.categoryContent.excludeAttribute;
        subCatL3.categoryContent.canonicalUrl = subCategoryL3.categoryContent.canonicalUrl;
        return subCatL3;
      });
      if (subCat.categoryContent.displayToCustomer) {
        subCategories[category].hasSubCategory = true;
      }
      subCategories[category].items.push(subCat);
      return subCat;
    });

    return subCategories;
  },
  // eslint-disable-next-line sonarjs/cognitive-complexity
  processData: (navLinkList, navigationDepth = 2) => {
    let navigationData = [];
    try {
      navigationData =
        navLinkList && Array.isArray(navLinkList)
          ? navLinkList.map(listItem => {
              const categoryContent = Abstractor.constructCategoryContent(listItem);

              const result = {
                categoryContent,
                url: Abstractor.constructUrl(listItem.categoryContent),
                categoryId: listItem.categoryContent && listItem.categoryContent.catgroupId,
              };

              if (navigationDepth === 2) {
                result.subCategories = Abstractor.processSubCategories(listItem);
                result.hasL2 = listItem.subCategories && listItem.subCategories.length;
              }

              return result;
            })
          : [];
    } catch (error) {
      const { navigationAbstractor } = errorComponents;
      const { navigationError } = errorMessages;
      logger.debug(`${navigationError} | Error Response: ${JSON.stringify(error)}`);
      errorHandler({
        errorMessage: navigationError,
        errorObject: error,
        component: navigationAbstractor,
        payload: navLinkList,
      });
      throw new Error(navigationError);
    }
    return navigationData;
  },
  processNestedData: navLinkList => {
    const newNavigationObj = {};
    navLinkList.forEach(listItem => {
      const hasL2 = listItem.subCategories && listItem.subCategories.length;
      const categoryId = listItem.categoryContent && listItem.categoryContent.id;
      newNavigationObj[categoryId] = {
        categoryContent: Abstractor.constructCategoryContent(listItem),
        hasL2,
        subCategories: Abstractor.processSubCategories(listItem),
      };
    });
    return newNavigationObj;
  },
};

export default Abstractor;
