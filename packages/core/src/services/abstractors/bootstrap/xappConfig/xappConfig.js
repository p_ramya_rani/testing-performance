// 9fbef606107a605d69c0edbcd8029e5d 
import handler from '../../../handler';
import { isMobileApp } from '../../../../utils';
import {
  defaultBrand,
  defaultChannel,
  defaultCountry,
  MobileChannel,
} from '../../../api.constants';

/**
 * Abstractor layer for loading data from API for Labels related components
 */
const Abstractor = {
  getQueryParams: apiConfig => {
    const { brandIdCMS, siteIdCMS, isAppChannel } = apiConfig || {};
    const isMobileChannel = isMobileApp() || isAppChannel;
    const channel = isMobileChannel ? MobileChannel : defaultChannel;
    return {
      brand: brandIdCMS || defaultBrand,
      channel,
      country: siteIdCMS || defaultCountry,
    };
  },
  getData: async (module, apiConfig) => {
    const data = Abstractor.getQueryParams(apiConfig);
    return handler
      .fetchModuleDataFromGraphQL({ name: module, data }, apiConfig)
      .then(response => response.data)
      .then(Abstractor.processData);
  },
  processData: ({ configurationKey = [] }) => {
    const xappConfig = {};
    configurationKey.forEach(({ key, value }) => {
      xappConfig[key] = value;
    });
    return xappConfig;
  },
};
export default Abstractor;
