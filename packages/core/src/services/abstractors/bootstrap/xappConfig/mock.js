// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  configurationKey: [
    {
      key: 'ROPIS_ENABLED',
      value: 'TRUE',
    },
    {
      key: 'ROPIS_ENABLED_STATES',
      value: 'ZZ',
    },
    {
      key: 'ROPIS_MARKETING_PROMO_BOX_ENABLED',
      value: '1',
    },
  ],
};
