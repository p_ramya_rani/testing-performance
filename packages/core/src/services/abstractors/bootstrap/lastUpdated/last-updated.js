// 9fbef606107a605d69c0edbcd8029e5d 
import handler from '../../../handler';

/**
 * Abstractor layer for loading data from API for Last Updated related components
 */
const Abstractor = {
  getData: (module, data, apiConfig) => {
    return handler
      .fetchModuleDataFromGraphQL({ name: module, data }, apiConfig)
      .then(response => {
        return response.data;
      })
      .then(Abstractor.processData);
  },
  processData: ({ lastUpdated = [] }) => {
    if (!lastUpdated) {
      return null;
    }
    const result = {};
    lastUpdated.forEach(({ key, value }) => {
      result[key] = value;
    });
    return result;
  },
};
export default Abstractor;
