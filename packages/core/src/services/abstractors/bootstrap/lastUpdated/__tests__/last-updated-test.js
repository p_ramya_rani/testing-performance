// 9fbef606107a605d69c0edbcd8029e5d 
import LastUpdatedAbstractor from '../last-updated';
import mock from '../mock';

jest.mock('../../../../handler/handler');

it('Last Updated Abstractor | ', () => {
  LastUpdatedAbstractor.getData('lastUpdated', {
    brand: 'TCP',
    country: 'USA',
    channel: 'Desktop',
  }).then(data => {
    expect(data).toMatchObject(mock);
  });
});
