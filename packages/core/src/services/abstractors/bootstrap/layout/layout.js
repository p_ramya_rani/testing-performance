// 9fbef606107a605d69c0edbcd8029e5d 
import logger from '@tcp/core/src/utils/loggerInstance';
import { createLayoutPath, pathHasHyphenSlash } from '@tcp/core/src/utils';
import handler from '../../../handler';
import { errorComponents, errorMessages } from '../../../api.constants';
import { generateSessionId } from '../../../../utils/cookie.util';
import { isMobileApp } from '../../../../utils';
import { API_CONFIG } from '../../../config';

/**
 * This function submits errors to Raygun
 * @param {Object} errorArgs containing {errorMessage, errorObject, component, payload}
 */
const errorHandler = errorArgs => {
  const { errorMessage, errorObject, component, payload } = errorArgs;
  const { sessionCookieKey } = API_CONFIG;
  logger.error({
    error: errorMessage,
    errorTags: [component],
    extraData: {
      ...payload,
      errorComponent: component,
      errorObject,
      errorMessage,
      [sessionCookieKey]: !isMobileApp() && generateSessionId(true),
    },
  });
};

/**
 * Abstractor layer for loading data from API for Layout
 */
const LayoutAbstractor = {
  slotModuleMap: {},
  /**
   * This function loads data from graphQL service
   * @param {Object} params Object containing {page, brand, country, channel}
   */
  getLayoutData: async ({ page, brand, country, channel, pageName }, apiConfig) => {
    logger.info(`Executing Layout Query for ${page}: `);
    logger.debug(
      'Executing Layout Query with params: ',
      `page:${page}`,
      `brand:${brand}`,
      `country:${country}`,
      `channel:${channel}`
    );
    return handler
      .fetchModuleDataFromGraphQL(
        {
          name: 'layout',
          data: {
            path: page,
            brand,
            country,
            channel,
            pageName:
              pageName && pathHasHyphenSlash(pageName) ? createLayoutPath(pageName) : pageName,
          },
        },
        apiConfig
      )
      .then(response => {
        let result;
        try {
          const formattedPageName =
            pageName && pathHasHyphenSlash(pageName) ? createLayoutPath(pageName) : pageName;
          result = response.data[formattedPageName || page];
          logger.info('Layout Query Executed Successfully');
          logger.debug('Layout Query Result: ', JSON.stringify(result));
        } catch (error) {
          const { layoutAbstractor } = errorComponents;
          const { layoutError } = errorMessages;
          logger.debug(`${layoutError} | Error Response: ${JSON.stringify(error)}`);
          errorHandler({
            errorMessage: layoutError,
            errorObject: error,
            component: layoutAbstractor,
            payload: response.data,
          });
          throw new Error(layoutError);
        }

        return result;
      });
  },
  /**
   * This function loads data for a set of modules from graphQL service
   * @param {Array} modules Array of Objects with below structure
   * eg.{
        name: "layout",
        data: {
          contentId: "abcd",
          slot: "slot_1",
        },
      }
   *
   */
  getModulesData: async (modules, apiConfig) => {
    return handler.fetchModuleDataFromGraphQL(modules, apiConfig, true);
  },
  /**
   * Asynchronous function to get modules data from service for layouts
   * @param {Object} data Response object for layout query
   */
  getModulesFromLayout: async (data, language, page, apiConfig) => {
    logger.info('module received language ', language);
    // Adding Module 2 columns mock
    const layoutResponse = data.items;

    const moduleObjects = LayoutAbstractor.collateModuleObject(layoutResponse, language);
    return LayoutAbstractor.getModulesData(moduleObjects, apiConfig).then(response => {
      return LayoutAbstractor.processModuleData(response.data, page);
    });
  },
  /**
   * Processes data to create an array of content IDs with slot information
   * @param {*} items
   */
  // eslint-disable-next-line sonarjs/cognitive-complexity
  collateModuleObject: (items, language) => {
    const moduleIds = [];
    try {
      items.forEach(({ layout: { slots } }) => {
        slots.forEach(slot => {
          if (slot) {
            LayoutAbstractor.slotModuleMap[slot.name] = slot;
            if (slot.contentId) {
              const contentIds = slot.contentId.split(',');
              if (contentIds.length > 1) {
                const moduleNames = slot.value.split(',');
                contentIds.forEach((contentId, index) => {
                  const moduleName = moduleNames[index];
                  moduleIds.push({
                    name: moduleName,
                    data: {
                      contentId,
                      slot: `${slot.name}_${index}`,
                      lang: language !== 'en' ? language : '', // TODO: Remove Temporary Check for en support, as not supported from CMS yet
                    },
                  });
                });
              } else {
                moduleIds.push({
                  name: slot.moduleName,
                  data: {
                    contentId: slot.contentId,
                    slot: slot.name,
                    lang: language !== 'en' ? language : '', // TODO: Remove Temporary Check for en support, as not supported from CMS yet
                  },
                });
              }
            }
          }
        });
      });
    } catch (error) {
      const { layoutAbstractor } = errorComponents;
      const { collateModuleError } = errorMessages;
      logger.debug(`${collateModuleError} | Error Response: ${JSON.stringify(error)}`);
      errorHandler({
        errorMessage: collateModuleError,
        errorObject: error,
        component: layoutAbstractor,
        payload: items,
      });
    }
    return moduleIds;
  },
  checkForErrors: (data, slotKey, page) => {
    const { errorMessage } = data;
    if (errorMessage) {
      const { moduleName, contentId } = LayoutAbstractor.slotModuleMap[slotKey];
      logger.error(
        `Error occurred on page ${page} in module ${moduleName} {contentId: ${contentId}}-> ${errorMessage}`
      );
    }
  },
  processModuleData: (moduleData, page) => {
    const modulesObject = {};
    try {
      Object.keys(moduleData).forEach(slotKey => {
        if (moduleData && moduleData[slotKey]) {
          LayoutAbstractor.checkForErrors(moduleData[slotKey], slotKey, page);
          let { set } = moduleData[slotKey];
          if (!set) {
            set = [];
          }
          modulesObject[moduleData[slotKey].contentId] = {
            ...moduleData[slotKey].composites,
            moduleName: moduleData[slotKey].name,
            set,
            columns: moduleData[slotKey].columns || [],
          };
          set.forEach(({ key, val }) => {
            modulesObject[moduleData[slotKey].contentId][key] = val;
          });
        }
      });
    } catch (error) {
      const { layoutAbstractor } = errorComponents;
      const { moduleProcessingError } = errorMessages;
      logger.debug(`${moduleProcessingError} | Error Response: ${JSON.stringify(error)}`);
      errorHandler({
        errorMessage: moduleProcessingError,
        errorObject: error,
        component: layoutAbstractor,
        payload: moduleData,
      });
    }

    return modulesObject;
  },
};

export default LayoutAbstractor;
