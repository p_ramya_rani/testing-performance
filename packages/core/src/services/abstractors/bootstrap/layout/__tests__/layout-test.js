// 9fbef606107a605d69c0edbcd8029e5d
import layoutAbstractor from '../layout';
import HomePageLayoutMockResponse from '../mock';
import ModuleDMock from '../../../common/moduleD/mock';

jest.mock('../../../../handler/handler');

it('abstractor - layout | getLayoutData', () => {
  return layoutAbstractor
    .getLayoutData({
      page: 'homepage',
      brand: 'TCP',
      channel: 'Desktop',
      country: 'USA',
    })
    .then((data) => {
      expect(data).toMatchObject(HomePageLayoutMockResponse.homepage);
    });
});

it('abstractor - layout | getModulesData', () => {
  return layoutAbstractor
    .getModulesData([
      {
        name: 'moduleD',
      },
      {
        name: 'moduleH',
      },
    ])
    .then((data) => {
      expect(data.data.moduleD).toMatchObject(ModuleDMock);
    });
});

it('abstractor - layout | getModulesFromLayout', () => {
  return layoutAbstractor.getModulesFromLayout(HomePageLayoutMockResponse.homepage).then((data) => {
    expect(data.moduleD).toMatchObject(ModuleDMock.composites);
  });
});
