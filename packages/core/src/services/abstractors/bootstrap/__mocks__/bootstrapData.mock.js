// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  global: [
    {
      name: 'global',
      errorMessage: null,
      subcategories: [
        {
          name: 'AppUpdateModal',
          labels: [
            {
              key: 'lbl_appUpdate_title',
              value: 'New Version Available',
              __typename: 'KeyValue',
            },
          ],
          referred: [],
          __typename: 'LabelSubCategory',
        },
        {
          name: 'DownloadAppPromo',
          labels: [
            {
              key: 'lbl_download_cta',
              value: 'Download the App',
              __typename: 'KeyValue',
            },
          ],
          referred: [],
          __typename: 'LabelSubCategory',
        },
        {
          name: 'seo',
          labels: [
            {
              key: 'lbl_account_pageTitle',
              value: 'Account Overview | The children’s place',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_account_description',
              value:
                'Manage your The children’s place account and find all of the information on My Place Rewards, all in one place.',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_account_path',
              value: '/account',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_bag_pageTitle',
              value: 'Shopping Bag | The children’s place',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_bag_description',
              value:
                'View your The children’s place shopping bag and shop our selection of hand-me-down quality kids clothes, toddler clothes &amp; baby clothes.',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_bag_path',
              value: '/bag',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_checkout_pageTitle',
              value: 'Checkout | The children’s place',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_checkout_description',
              value:
                'Checkout and purchase quality kids clothes, toddler clothes and baby clothes from The children’s place.',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_checkout_path',
              value: '/checkout',
              __typename: 'KeyValue',
            },
          ],
          referred: [],
          __typename: 'LabelSubCategory',
        },
      ],
      __typename: 'LabelCategory',
    },
  ],
  header: {
    errorMessage: null,
    submodules: {
      topNavWrapper: {
        composites: {
          brand_tabs: [
            {
              url: '/home',
              title: "The Children's Place",
              target: '_self',
              external: 0,
              class: 'header__brand-tab--tcp',
              __typename: 'LinkClass',
            },
            {
              url: 'https://test-uat2.gymboree.com/us/home?token=gymrwd',
              title: 'Gymboree',
              target: '_self',
              external: 0,
              class: 'header__brand-tab-gymboree',
              __typename: 'LinkClass',
            },
          ],
          top_promo_banner: [
            {
              text:
                '<style>\r\n  .ecom-desktop {\r\n  }\r\n  .ecom-tablet {\r\n    display: none;\r\n  }\r\n  .ecom-mobile {\r\n    display: none;\r\n  }\r\n\r\n  @media only screen and (max-width: 1024px) {\r\n    .ecom-desktop {\r\n      display: none;\r\n    }\r\n\r\n    .ecom-tablet {\r\n      display: block;\r\n    }\r\n    .ecom-mobile {\r\n      display: none;\r\n    }\r\n  }\r\n  @media only screen and (max-width: 767px) {\r\n    .ecom-desktop {\r\n      display: none;\r\n    }\r\n    .ecom-tablet {\r\n      display: none;\r\n    }\r\n    .ecom-mobile {\r\n      display: block;\r\n    }\r\n  }\r\n</style>\r\n<div class="bs-grid">\r\n<a target="_modal" href="/" action="plccmodal" data-target="plccModal">\r\n    <img\r\n      class="bs-img ecom-desktop"\r\n      \r\n      src="https://assets.theplace.com/image/upload/b_white,f_auto,q_auto,dpr_2.0/ecom/assets/content/gym/us/mpr/PLCC/double-fs-desktop_updated.jpg"\r\n      alt=" PLCC"\r\n    />\r\n    \r\n    <img\r\n      class="bs-img ecom-tablet"\r\n      src="https://assets.theplace.com/image/upload/b_white,f_auto,q_auto,dpr_2.0/ecom/assets/content/gym/us/mpr/PLCC/regular-double-tablet_3x.jpg"\r\n      alt="PLCC"\r\n    />\r\n    <img\r\n      class="bs-img ecom-mobile"\r\n      src="https://assets.theplace.com/image/upload/b_white,f_auto,q_auto,dpr_2.0/ecom/assets/content/gym/us/mpr/PLCC/regular-double-mobile.jpg"\r\n      alt="PLCC"\r\n    />\r\n  </a>\r\n</div>',
              userType: 'plcc',
              __typename: 'LoyaltyPromo',
            },
          ],
          promo_message_wrapper: null,
          __typename: 'Composite',
        },
        __typename: 'SubmoduleComposite',
      },
      promoTextBannerCarousel: null,
      loyaltyPromoBannerWrapper: null,
      __typename: 'Submodule',
    },
    __typename: 'ModuleContent',
  },
  footer: {
    errorMessage: null,
    submodules: {
      footerTop: {
        composites: {
          footerButtons: [
            {
              link: {
                url: '/footer-button-1',
                text: '',
                title: '',
                target: '_modal',
                action: 'open_signup_modal',
                __typename: 'Link',
              },
              __typename: 'LinkedTextItems',
            },
          ],
          __typename: 'Composite',
        },
        __typename: 'SubmoduleComposite',
      },
      __typename: 'Submodule',
    },
    __typename: 'ModuleContent',
  },
  navigation: [
    {
      errorMessage: null,
      categoryContent: {
        description: '',
        catgroupId: '458031',
        name: 'Gift Guide',
        id: '458031',
        seoUrl: null,
        seoToken: 'gift-shop',
        longDescription:
          '<h1>Gift Shop</h1><p>The latest craze starts here! Check out <strong>trending kids clothes</strong> from The Childrens Place thatll have your whole crew setting the bar for style. Whether its the latest and greatest accessories, graphic tees, jeans, lunch boxes, denim, or shoes, weve got all those must-have looks for kids, including:</p><div class="read-more-target"><p>Tees with a twist. Shake up their everyday look with comfy tees packed with personality. Check out all the girls long sleeve graphic tops that feature glam embellishments like lace and sequins (just to name a few!).</p><p>In-demand denim. Cool, classic and always on-trend, <strong>kids denim</strong> from The Childrens Place delivers big style at prices youll love. Check out the latest looks designed to a tee, all in tons of trending washes, colors, fits and styles.</p><p>Little extras for a big impact. No outfit is complete without our stylish backpacks, lunchboxes, bags and other school accessories designed to keep em organized, while helping showcase their unique style! Jewelry, watches, sunglasses, hats, gloves, scarves its all here. Best of all, our accessories feature all the trending looks, from denim and faux fur, to camo, sequin, glitter, velvet, neon and foil designs too.</p><p>Everything else under the sun. From dresses to dress pants, outerwear to bathing suits, flip flops to holiday outfits, weve got the school day covered, as well as weekends, vacations and everything in between.</p><p>Designed for your busy life on-the-go, our easy-to-use <a href="https://www.childrensplace.com/us/content/mobile?icid=mbapp_glft_mbapplp_txt_070115_null">mobile app</a> lets you check product availability, locations, hours and so much more. Enjoy online shipping made easy with our <a href="https://www.childrensplace.com/us/content/free-shipping?icid=hp_na_f_na_073118_freeshipping">Free Shipping</a> every day. No minimum purchase required. Need to make a return? The Childrens Place offers Free Returns to any store/outlet (including online exclusive products). Discover even greater savings with our <a href="https://www.childrensplace.com/us/content/myplace-rewards-page">My Place Rewards</a> and earn points every time you shop at the PLACE that has kids covered!</p></div>',
        seoTitle: "Kids Christmas Gift Guide | The Children's Place | Free Shipping*",
        seoMetaDesc:
          "Check out The Children's Place for a great selection of Kids & Baby Gifts. Shop at the PLACE where big fashion meets little prices!",
        displayToCustomer: '0',
        isShortImage: '',
        sizeChartSelection: '',
        canonicalUrl: '',
        excludeAttribute: '',
        mainCategory: {
          set: [
            {
              key: 'showOnlyOnApp',
              val: 'true',
              __typename: 'KeyValPair',
            },
            {
              key: 'dispTyp',
              val: 'default',
              __typename: 'KeyValPair',
            },
            {
              key: 'accentColor',
              val: 'yellow',
              __typename: 'KeyValPair',
            },
          ],
          sizesRange: null,
          promoBadge: null,
          categoryFilterImage: null,
          categoryLayout: null,
          __typename: 'NavigationCategory',
        },
        __typename: 'NavigationCategoryContent',
      },
      __typename: 'MainNavigationCategory',
    },
    {
      errorMessage: null,
      categoryContent: {
        description: '',
        catgroupId: '488006',
        name: 'Holiday Dress Up',
        id: '488006',
        seoUrl: null,
        seoToken: 'kids-holiday-dress-up',
        longDescription:
          '<h1>Christmas Dress Up</h1><p>Dress your minis in adorable Christmas outfits for kids at The Childrens Place. From dresses and button down shirts to all the must-have accessories, shop the PLACE that has cute kids Christmas outfits covered! </p><div class="read-more-target"><p>Check out the latest kids holiday outfits for boys. Shop our classic boys oxford button down shirts and vests or all our boys v neck sweaters in solid color, stripe and argyle styles. Pair our boys dress shirts and sweaters with skinny dress pants and baby boy sneakers or comfy slip on loafers for a look you\'ll both love!</p><p>Looking for colorful girls holiday clothes? Shop our Mommy and Me jacquard matching dresses. Pair them with cable knit or metallic tights and any of our classic ballet flats in glitter cross strap, metallic, t-strap, rhinestud or faux patent leather styles. For head-to-toe style, add a beaded necklace and bracelet set and a metal or festive headband and she\'s ready for pictures!</p><p>Shopping our PLACE is better than ever! Download our <a href="https://www.childrensplace.com/us/content/mobile?icid=mbapp_glft_mbapplp_txt_070115_null">mobile app</a> and see the latest kids\' trends, check locations/hours, manage your <a href="https://www.childrensplace.com/us/content/myplace-rewards-page">My Place Rewards</a> account and more. Shopping online is easier than ever at <a href="https://www.childrensplace.com/us/home">The Children\'s Place</a>.</p></div>',
        seoTitle:
          "Kids Christmas Clothes & Holiday Outfits | The Children's Place | Free Shipping*",
        seoMetaDesc:
          "The season is here, get them ready for the holidays with our kids Christmas Dress up collection from The Children's Place, shop dresses, all things plaid, sweaters and Argyle.",
        displayToCustomer: '0',
        isShortImage: '',
        sizeChartSelection: '',
        canonicalUrl: '',
        excludeAttribute: '',
        mainCategory: {
          set: null,
          sizesRange: null,
          promoBadge: null,
          categoryFilterImage: null,
          categoryLayout: null,
          __typename: 'NavigationCategory',
        },
        __typename: 'NavigationCategoryContent',
      },
      __typename: 'MainNavigationCategory',
    },
  ],
};
