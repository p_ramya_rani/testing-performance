// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  global: [
    {
      name: 'global',
      errorMessage: null,
      subcategories: [
        {
          name: 'AppUpdateModal',
          labels: [
            {
              key: 'lbl_appUpdate_title',
              value: 'New Version Available',
              __typename: 'KeyValue',
            },
          ],
          referred: [],
          __typename: 'LabelSubCategory',
        },
        {
          name: 'DownloadAppPromo',
          labels: [
            {
              key: 'lbl_download_cta',
              value: 'Download the App',
              __typename: 'KeyValue',
            },
          ],
          referred: [],
          __typename: 'LabelSubCategory',
        },
        {
          name: 'seo',
          labels: [
            {
              key: 'lbl_account_pageTitle',
              value: 'Account Overview | The children’s place',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_account_description',
              value:
                'Manage your The children’s place account and find all of the information on My Place Rewards, all in one place.',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_account_path',
              value: '/account',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_bag_pageTitle',
              value: 'Shopping Bag | The children’s place',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_bag_description',
              value:
                'View your The children’s place shopping bag and shop our selection of hand-me-down quality kids clothes, toddler clothes &amp; baby clothes.',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_bag_path',
              value: '/bag',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_checkout_pageTitle',
              value: 'Checkout | The children’s place',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_checkout_description',
              value:
                'Checkout and purchase quality kids clothes, toddler clothes and baby clothes from The children’s place.',
              __typename: 'KeyValue',
            },
            {
              key: 'lbl_checkout_path',
              value: '/checkout',
              __typename: 'KeyValue',
            },
          ],
          referred: [],
          __typename: 'LabelSubCategory',
        },
      ],
      __typename: 'LabelCategory',
    },
  ],
};
