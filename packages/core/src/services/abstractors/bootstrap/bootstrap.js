/* eslint-disable max-lines */
/* eslint-disable camelcase */
// 9fbef606107a605d69c0edbcd8029e5d
import logger from '@tcp/core/src/utils/loggerInstance';
// import ProductListingAbstractor from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListingApiHandler';
import URL_PATTERN from '@tcp/core/src/constants/urlPatterns.constants';
import labelsAbstractor from './labels';
import headerAbstractor from './header';
import footerAbstractor from './footer';
import navigationAbstractor from './navigation';
import handler from '../../handler';
import {
  isMobileApp,
  createLayoutPath,
  pathHasHyphenSlash,
  isProduction,
  setBootstrapCachedData,
} from '../../../utils';
import {
  defaultBrand,
  defaultChannel,
  defaultCountry,
  MobileChannel,
  errorComponents,
  errorMessages,
} from '../../api.constants';
import { generateSessionId } from '../../../utils/cookie.util';
import { API_CONFIG } from '../../config';
import {
  LAST_SAVED_BOOTSTRAP_DATA_KEY,
  BOOTSTRAP_DATA_KEY,
} from '../../../constants/fallback-data/static.config';

export const shouldGetStoreLabels = (url) => {
  const { STORE_LOCATOR, STORE, STORES } = URL_PATTERN;
  return url.includes(STORE_LOCATOR) || url.includes(STORE) || url.includes(STORES);
};

export const getPLPSLPCLPlabels = (url) => {
  const { CATEGORY_LANDING, SEARCH_DETAIL } = URL_PATTERN;
  return url.includes(CATEGORY_LANDING) || url.includes(SEARCH_DETAIL);
};

export const labelsCategoriesToFetch = (originalUrl) => {
  const { CHECKOUT, ACCOUNT, BAG, PRODUCT_LIST, OUTFIT_DETAILS, BUNDLE_DETAIL } = URL_PATTERN;

  let labelsToFetch = ['global'];
  if (getPLPSLPCLPlabels(originalUrl)) {
    labelsToFetch = ['PLP', 'SLPs', 'CLPs', ...labelsToFetch];
  } else if (originalUrl.includes(BUNDLE_DETAIL)) {
    labelsToFetch = ['CollectionsLabels', 'ProductDetailPage', ...labelsToFetch];
  } else if (originalUrl.includes(PRODUCT_LIST)) {
    labelsToFetch = ['ProductDetailPage', ...labelsToFetch];
  } else if (originalUrl.includes(OUTFIT_DETAILS)) {
    labelsToFetch = ['OutfitLabels', ...labelsToFetch];
  } else if (originalUrl.includes(CHECKOUT)) {
    labelsToFetch = ['checkout', ...labelsToFetch];
  } else if (originalUrl.includes(ACCOUNT) || originalUrl.includes(BAG)) {
    labelsToFetch = ['account', 'checkout', ...labelsToFetch];
  } else if (shouldGetStoreLabels(originalUrl)) {
    labelsToFetch = ['StoreLocator', ...labelsToFetch];
  }

  return labelsToFetch;
};

/**
 * This function submits errors to Raygun
 * @param {Object} errorArgs containing {errorMessage, errorObject, component, payload}
 */
const errorHandler = (errorArgs) => {
  const { errorMessage, errorObject, component, payload } = errorArgs;
  const { sessionCookieKey } = API_CONFIG;
  logger.error({
    error: errorMessage,
    errorTags: [component],
    extraData: {
      payload: {
        ...payload,
      },
      errorComponent: component,
      errorObject,
      errorMessage,
      [sessionCookieKey]: !isMobileApp() && generateSessionId(true),
    },
  });
};

/**
 * Asynchronous function to fetch data from service for given array of moduleIds
 * @param {String} page Page name to be loaded, needs to be in sync with GraphQL query
 */
const fetchBootstrapData = async (bootstrapParams, apiConfig) => {
  return handler.fetchModuleDataFromGraphQL(bootstrapParams, apiConfig).then((response) => {
    logger.debug('Response from bootstrap graph query: ', JSON.stringify(response.data));
    if (response.data && response.data.errors) {
      response.data.errors.forEach((errorObj) => logger.error(JSON.stringify(errorObj.message)));
    }
    return response.data;
  });
};

export const createLabelsSSRData = ({ originalUrl, labels, brand, country, channel, lang }) => {
  let dataToReturn = [];
  const mobileLabelsConfig = {
    category: labels.category,
    subCategory: labels.subCategory,
    brand,
    country,
    channel,
    lang,
  };

  if (isMobileApp()) {
    dataToReturn = mobileLabelsConfig;
  } else {
    const requiredCategoriesForLabels = labelsCategoriesToFetch(originalUrl);

    logger.info('Labels category to fetch ', requiredCategoriesForLabels);
    dataToReturn = requiredCategoriesForLabels.map((category) => {
      return {
        category,
        brand,
        country,
        channel,
        lang,
      };
    });
  }

  return dataToReturn;
};

const generateBootstrapParams = (
  { page, labels, brand, country, channel, lang },
  bootstrapModules
  // temporarily disable category based labels
  // originalUrl
) => {
  logger.info('bootstrap received language ', lang);
  /**
   * Sets up query params for global modules requests - (labels, header, footer, navigation)
   */
  return bootstrapModules.map((module) => {
    let data = {};
    switch (module) {
      case 'labels':
        // temporarily disable category based labels
        // data = createLabelsSSRData({ originalUrl, labels, brand, country, channel, lang });
        data = {
          category: labels.category,
          subCategory: labels.subCategory,
          brand,
          country,
          channel,
          lang,
        };
        break;
      case 'header':
        data = {
          type: 'header',
          brand,
          country,
          channel,
          lang,
        };
        break;
      case 'footer':
        data = {
          type: 'footer',
          brand,
          country,
          channel,
          lang,
        };
        break;
      case 'navigation':
        data = {
          type: 'navigation-ssr',
          brand,
          country,
          channel,
          lang,
        };
        break;
      default:
        data = page;
    }

    return {
      name: module,
      data,
    };
  });
};

/**
 * Generate base bootstrap parameters
 */
const createBootstrapParams = (apiConfig, language) => {
  const isMobileChannel = isMobileApp() || apiConfig.isAppChannel;
  const channelName = isMobileChannel ? MobileChannel : defaultChannel;

  return {
    labels: {
      // TODO - GLOBAL-LABEL-CHANGE - STEP 1.2 -  Uncomment this line for only global data
      // TODO - Mobile app should also follows the same pattern
      // category: LABELS.global,
    },

    brand: (apiConfig && apiConfig.brandIdCMS) || defaultBrand,
    channel: channelName,
    country: (apiConfig && apiConfig.siteIdCMS) || defaultCountry,
    lang: language !== 'en' ? language : '', // TODO: Remove Temporary Check for en support as not supported from CMS yet
  };
};

/**
 * Get cached Data
 * @param {Array} pages
 */
export const retrieveCachedData = ({ cachedData, key, bootstrapData }) => {
  const cachedKeyData = cachedData[key];
  if (cachedKeyData) {
    logger.info(`BOOTSTRAP CACHE HIT: ${key}`);
    try {
      return JSON.parse(cachedKeyData);
    } catch (err) {
      logger.error(err);
    }
  }
  return JSON.parse(JSON.stringify(bootstrapData[key]));
};

export const getLabelsParsedData = ({ bootstrapData, bootstrapQueryParams }) => {
  const getLabelsDataConfig = bootstrapQueryParams.filter(
    (obj) => obj.name.toLowerCase() === 'labels'
  );

  let labelsCategoryLength = 0;
  const requestedLabels = [];
  getLabelsDataConfig.forEach((categoriesArray) => {
    const labelsConfig = categoriesArray.data || [];
    labelsCategoryLength = labelsConfig.length;
    labelsConfig.forEach((labelsDataObj) => {
      const labelKeys = labelsDataObj.category;
      try {
        if (bootstrapData[labelKeys]) {
          requestedLabels.push(...bootstrapData[labelKeys]);
        }
      } catch (err) {
        logger.error(err);
      }
    });
  });

  return labelsCategoryLength === requestedLabels.length ? requestedLabels : false;
};

export const shouldInitiateSSRCall = (originalUrl, deviceType) =>
  originalUrl.includes('/c/') && deviceType === 'bot' && typeof window === 'undefined';

const isErrorInBootstrap = (errorObject, apiConfig) => {
  if (isProduction()) {
    if (errorObject) {
      const { header_error, footer_error, navigation_error, labels_error } = errorObject;
      if (isMobileApp() || apiConfig.isAppChannel) {
        // header, footer, layout, navigation data should not break  mobile app
        return labels_error;
      }
      return header_error || footer_error || navigation_error || labels_error;
    }
    return true;
  }
  return false;
};

const getBootstrapErrorCode = (errorObject, errMssg) => {
  const { bootstrapErrorCode } = errorObject;
  return bootstrapErrorCode ? `${bootstrapErrorCode}, ${errMssg}` : errMssg;
};

const getBootstrapLayoutError = (pageName, bootstrapData) => {
  if (!pageName) {
    return { errorMessage: null };
  }
  const pageLayout = bootstrapData[pageName];
  if (
    pageLayout &&
    pageLayout.items &&
    pageLayout.items.length &&
    pageLayout.items[0].layout &&
    pageLayout.items[0].layout.slots &&
    pageLayout.items[0].layout.slots.length
  ) {
    return pageLayout;
  }
  return { errorMessage: errorMessages.layoutDataError };
};

const getBootstrapHeaderError = (header) => {
  if (header && header.submodules && Object.keys(header.submodules).length) {
    return header;
  }
  return { errorMessage: errorMessages.headerDataError };
};

const getBootstrapFooterError = (footer) => {
  if (footer && footer.submodules && Object.keys(footer.submodules).length) {
    return footer;
  }
  return { errorMessage: errorMessages.footerDataError };
};

const getBootstrapLabelError = (labels) => {
  if (labels && labels.length) {
    return labels;
  }
  return [{ errorMessage: errorMessages.labelsDataError }];
};

const getBootstrapNavigationError = (navigation) => {
  if (navigation && navigation.length) {
    return navigation;
  }
  return [{ errorMessage: errorMessages.navigationDataError }];
};

const getDataOrNullString = (data) => data || 'null';

/**
 * This function parses Error out of response
 * @param {*} bootstrapData Response from API call
 * @param {*} pageName Page Name
 */
// eslint-disable-next-line complexity
export const checkAndLogErrors = (bootstrapData, pageName, modules) => {
  const { labels, header, footer, navigation } = bootstrapData;

  const {
    headerDataErrorCode,
    footerDataErrorCode,
    labelsDataErrorCode,
    navigationDataErrorCode,
    layoutDataErrorCode,
  } = errorMessages;
  const errorObject = {
    header_error: 0,
    footer_error: 0,
    navigation_error: 0,
    labels_error: 0,
    layout_error: 0,
    bootstrapErrorCode: '',
  };

  try {
    const { errorMessage: headerErrorMessage } = modules.find((m) => m === 'header')
      ? getBootstrapHeaderError(header)
      : {};
    if (headerErrorMessage) {
      errorObject.header_error = 1;
      errorObject.header_error_message = headerErrorMessage;
      errorObject.bootstrapErrorCode = getBootstrapErrorCode(errorObject, headerDataErrorCode);
      logger.info(`Error occurred in header query ${JSON.stringify(getDataOrNullString(header))}`);
    }
    const { errorMessage: footerErrorMessage } = modules.find((m) => m === 'footer')
      ? getBootstrapFooterError(footer)
      : {};
    if (footerErrorMessage) {
      errorObject.footer_error = 1;
      errorObject.footer_error_message = footerErrorMessage;
      errorObject.bootstrapErrorCode = getBootstrapErrorCode(errorObject, footerDataErrorCode);
      logger.info(`Error occurred in footer query ${JSON.stringify(getDataOrNullString(footer))}`);
    }

    const [{ errorMessage: navigationErrorMessage }] = modules.find((m) => m === 'navigation')
      ? getBootstrapNavigationError(navigation)
      : [{}];
    if (navigationErrorMessage) {
      errorObject.navigation_error = 1;
      errorObject.navigation_error_message = navigationErrorMessage;
      errorObject.bootstrapErrorCode = getBootstrapErrorCode(errorObject, navigationDataErrorCode);
      logger.info(
        `Error occurred in navigation query ${JSON.stringify(getDataOrNullString(navigation))}`
      );
    }

    const [{ errorMessage: labelsErrorMessage }] = modules.find((m) => m === 'labels')
      ? getBootstrapLabelError(labels)
      : [{}];
    if (labelsErrorMessage) {
      errorObject.labels_error = 1;
      errorObject.labels_error_message = labelsErrorMessage;
      errorObject.bootstrapErrorCode = getBootstrapErrorCode(errorObject, labelsDataErrorCode);
      logger.info(`Error occurred in labels query ${JSON.stringify(getDataOrNullString(labels))}`);
    }

    const { errorMessage: layoutErrorMessage } = modules.find((m) => m === 'layout')
      ? getBootstrapLayoutError(pageName, bootstrapData)
      : {};

    if (layoutErrorMessage) {
      errorObject.layout_error = 1;
      errorObject.layout_error_message = layoutErrorMessage;
      errorObject.bootstrapErrorCode = getBootstrapErrorCode(errorObject, layoutDataErrorCode);
      const layoutResponse =
        (bootstrapData && pageName && bootstrapData[pageName]) || 'bootstrapData[pageName] is null';
      logger.info(`Error occurred in layout query ${JSON.stringify(layoutResponse)}`);
    }
  } catch (error) {
    const { errorParsingError } = errorMessages;
    logger.debug(
      `Error Occurred While Parsing error response | Response data: ${JSON.stringify(error)}`
    );
    // eslint-disable-next-line no-throw-literal
    throw {
      errorMessage: errorParsingError,
      errorObject: { ...errorObject, isCheckAndLogErrors: true },
    };
  }

  return errorObject;
};

/**
 * This function returns parsed response from the bootstrap API
 * @param {*} response
 * @param {*} fetchCachedDataParams
 * @param {*} bootstrapData
 */
/* eslint-disable */
const parsedResponse = async (
  response,
  fetchCachedDataParams,
  bootstrapData,
  state,
  originalUrl,
  deviceType,
  bootstrapQueryParams
) => {
  const apiConfig = (state && state.APIConfig) || {};
  try {
    response.header =
      bootstrapData.header &&
      headerAbstractor.processData(
        retrieveCachedData({ ...fetchCachedDataParams, key: 'header', apiConfig })
      );
  } catch (error) {
    const { bootstrapComponent } = errorComponents;
    const { headerDataError } = errorMessages;
    logger.debug(`Error occurred while processing header data: ${JSON.stringify(error)}`);
    errorHandler({
      errorMessage: new Error(headerDataError),
      errorObject: error,
      component: bootstrapComponent,
      payload: response,
    });
    throw {
      errMssg: 'Bootstrap Data --> Header Parsing With Error',
    };
  }

  try {
    response.footer =
      bootstrapData.footer &&
      footerAbstractor.processData(
        retrieveCachedData({ ...fetchCachedDataParams, key: 'footer', apiConfig })
      );
  } catch (error) {
    const { bootstrapComponent } = errorComponents;
    const { footerDataError } = errorMessages;
    logger.debug(`Error occurred while processing footer data: ${JSON.stringify(error)}`);
    errorHandler({
      errorMessage: new Error(footerDataError),
      errorObject: error,
      component: bootstrapComponent,
      payload: response,
    });
  }

  try {
    response.labels =
      bootstrapData.labels &&
      labelsAbstractor.processData(
        retrieveCachedData({ ...fetchCachedDataParams, key: 'labels', apiConfig })
      );
  } catch (error) {
    const { bootstrapComponent } = errorComponents;
    const { labelsDataError } = errorMessages;
    logger.debug(`Error occurred while processing labels data: ${JSON.stringify(error)}`);
    errorHandler({
      errorMessage: new Error(labelsDataError),
      errorObject: error,
      component: bootstrapComponent,
      payload: response,
    });
    throw {
      errMssg: 'Bootstrap Data --> Labels Parsing With Error',
    };
  }

  try {
    if (!isMobileApp()) {
      response.navigation = {};
      const navigationData =
        bootstrapData.navigation &&
        navigationAbstractor.processData(
          retrieveCachedData({ ...fetchCachedDataParams, key: 'navigation', apiConfig })
        );
      response.navigation.navigationData = navigationData;
    }
  } catch (error) {
    const { bootstrapComponent } = errorComponents;
    const { navigationDataError } = errorMessages;
    logger.debug(`Error occurred while processing navigation data: ${JSON.stringify(error)}`);
    errorHandler({
      errorMessage: new Error(navigationDataError),
      errorObject: error,
      component: bootstrapComponent,
      payload: response,
    });
  }

  if (isMobileApp()) {
    /* eslint-disable extra-rules/no-commented-out-code */
    /* console.log('bootstrapData', JSON.stringify(response)); */
    setBootstrapCachedData(LAST_SAVED_BOOTSTRAP_DATA_KEY, BOOTSTRAP_DATA_KEY, response);
  }

  return response;
};

/**
 * This function will be used to get the module list for web and for app.
 * for the web view in app, we don't need header,footer and navigation.
 * @param {*} apiConfig
 * @param {*} modules
 */
const getBootstrapModules = (apiConfig, modules) => {
  const webBootstrapModules = modules || ['labels', 'header', 'footer', 'navigation'];
  const appWebViewBootstrapModules = modules || ['header'];
  return apiConfig.isAppChannel ? appWebViewBootstrapModules : webBootstrapModules;
};

/**
 * Responsible for making all the http requests that need to be resolved before loading the application
 *  -   Layout
 *  -   Header
 *  -   Footer
 *  -   Labels
 * @param {String} pageName
 * @param {module} Array ['header', 'footer', 'layout', 'navigation']
 */

// eslint-disable-next-line complexity
const bootstrap = async (pageNameStr, modules, cachedData, state, originalUrl, deviceType) => {
  const pageName = pageNameStr || '';
  let response = {};
  let bootstrapQueryParams = null;
  let bootstrapData = null;
  const apiConfig = state.APIConfig || {};
  const { language } = apiConfig;
  const bootstrapParams = { page: pageName, ...createBootstrapParams(apiConfig, language) };

  /**
   * Config Responsible for making all the http requests that need to be resolved before loading the application
   *  -   Header
   *  -   Footer
   *  -   Labels
   *  -   Navigation
   */
  let bootstrapModules = getBootstrapModules(apiConfig, modules);

  try {
    logger.info(
      'Executing Bootstrap Query for global modules: ',
      JSON.stringify(bootstrapModules),
      ',IsPreview Mode',
      apiConfig.isPreviewEnv
    );
    logger.debug(
      'Executing Bootstrap Query with params: ',
      JSON.stringify(bootstrapParams),
      pageName
    );

    bootstrapQueryParams = generateBootstrapParams(bootstrapParams, bootstrapModules, originalUrl);

    bootstrapData = await fetchBootstrapData(bootstrapQueryParams, apiConfig);
    if (!isMobileApp() && !(bootstrapData.labels && bootstrapData.labels.length)) {
      bootstrapData.labels = getLabelsParsedData({ bootstrapData, bootstrapQueryParams });
    }

    const layoutPageName =
      pageName && pathHasHyphenSlash(pageName) ? createLayoutPath(pageName) : pageName;
    const errorObject = checkAndLogErrors(bootstrapData, layoutPageName, bootstrapModules);
    if (isErrorInBootstrap(errorObject, apiConfig)) {
      errorObject.isErrorInBootstrap = true;
      response = { ...errorObject };
      // eslint-disable-next-line no-throw-literal
      throw {
        errorMessage: 'Bootstrap Data With Error',
        errorObject,
      };
    }
    logger.info('Bootstrap Query Executed Successfully');
    logger.debug('Bootstrap Query Result: ', JSON.stringify(bootstrapData));

    const fetchCachedDataParams = { bootstrapData, cachedData };

    return parsedResponse(
      response,
      fetchCachedDataParams,
      bootstrapData,
      state,
      originalUrl,
      deviceType,
      bootstrapQueryParams
    );
  } catch (error) {
    const { bootstrapError, bootstrapAbstractorError } = errorMessages;
    const { errorObject } = error;
    logger.debug('Bootstrap Query response', JSON.stringify(bootstrapData));
    let bootstrapErrorResponse = {};
    if (response && !response.isErrorInBootstrap) {
      bootstrapErrorResponse.isErrorInBootstrap = true;
      bootstrapErrorResponse.bootstrapErrorMssg = bootstrapAbstractorError;
      bootstrapErrorResponse.bootstrapErrorCode = bootstrapAbstractorError;
    } else {
      bootstrapErrorResponse = {
        ...response,
      };
    }
    bootstrapErrorResponse.errorObject = errorObject;
    // eslint-disable-next-line no-throw-literal
    throw { bootstrapErrorResponse, errMessage: bootstrapError, error };
  }
};

export default bootstrap;
