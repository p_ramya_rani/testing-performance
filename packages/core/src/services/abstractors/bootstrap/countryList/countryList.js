// 9fbef606107a605d69c0edbcd8029e5d 
import gql from 'graphql-tag';
import logger from '@tcp/core/src/utils/loggerInstance';
import handler, { executeStatefulAPICall } from '../../../handler';
import countryListQuery from '../../../handler/graphQL/queries/countryList';

/**
 * Abstractor layer for loading Country List data
 */
const Abstractor = {
  getData: country => {
    const queryData = countryListQuery.getQuery(country || '');
    const query = gql(queryData.query);
    return handler
      .executeGraphQLQuery({ query, cacheTag: queryData.cacheTag })
      .then(Abstractor.processData)
      .catch(Abstractor.handleError);
  },
  submitData: payload => {
    return executeStatefulAPICall(payload)
      .then(res => {
        return res.body;
      })
      .catch(Abstractor.handleError);
  },
  processData: data => data,
  handleError: e => logger.error(e),
};
export default Abstractor;
