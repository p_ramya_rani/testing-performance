// 9fbef606107a605d69c0edbcd8029e5d 
import HeaderAbstractor from '../header';
import mock from '../mock';

jest.mock('../../../../handler/handler');

it('Header Abstractor | ', () => {
  HeaderAbstractor.getData('header', {
    type: 'header',
    brand: 'TCP',
    country: 'USA',
    channel: 'Desktop',
  }).then(data => {
    expect(data).toMatchObject(mock);
  });
});
