// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  submodules: {
    topNavWrapper: {
      composites: {
        brand_tabs: [
          {
            url: '/',
            title: 'TCP Link',
            target: '',
            external: 0,
            class: 'header__brand-tab-gymboree',
            __typename: 'LinkClass',
          },
          {
            url: 'https://www.gymboree.com/',
            title: 'Gymboree Link',
            target: '_blank',
            external: 0,
            class: 'header__brand-tab--tcp',
            __typename: 'LinkClass',
          },
        ],
        promo_message_wrapper: [
          {
            richText: {
              text: '<b>FREE SHIPPING EVERY TEST DAY!</b> No minimum purchase required.',
              __typename: 'Text',
            },
            link: {
              url: '/static/promo1',
              title: '',
              target: '',
              external: 0,
              __typename: 'Link',
            },
            __typename: 'PromoMessage',
          },
          {
            richText: {
              text: '<b>FREE SHIPPING EVERY DAY!</b> This is banner2',
              __typename: 'Text',
            },
            link: {
              url: '/static/promo2',
              title: '',
              target: '',
              external: 0,
              __typename: 'Link',
            },
            __typename: 'PromoMessage',
          },
          {
            richText: {
              text: '<b>FREE SHIPPING EVERY DAY!</b> Banner 3 comes now',
              __typename: 'Text',
            },
            link: {
              url: '/static/promo3',
              title: '',
              target: '',
              external: 0,
              __typename: 'Link',
            },
            __typename: 'PromoMessage',
          },
        ],
        __typename: 'Composite',
      },
      __typename: 'SubmoduleComposite',
    },
    promoTextBannerCarousel: {
      composites: {
        displayPromo: 'none',
        promoRichText: [
          {
            link: { url: 'https://www.google.com' },
            richText: { text: '<h3> this is my rich</h3>' },
          },
          {
            link: { url: 'https://www.google.com' },
            richText: { text: '<h3> this is my rich2</h3>' },
          },
        ],
        promoTextBanner: [
          {
            linkClass: {
              url: '/c/boys',
              title: '',
              target: '',
              external: 0,
              class: 'header__promo-text-banner1',
              __typename: 'LinkClass',
            },
            textItems: [
              {
                text: 'NEED IT NOW?',
                style: 'style2',
                color: 'color1',
                __typename: 'StyledText',
              },
              {
                text: 'Buy online, pickup in store.',
                style: null,
                color: 'color1',
                __typename: 'StyledText',
              },
            ],
            __typename: 'PromoTextBanner',
          },
          {
            linkClass: {
              url: 'https://www.childrensplace.com/us/home',
              title: '',
              target: '',
              external: 0,
              class: 'header__promo-text-banner',
              __typename: 'LinkClass',
            },
            textItems: [
              {
                text: 'EARN PLACE CASH!',
                style: 'style2',
                color: 'color2',
                __typename: 'StyledText',
              },
              {
                text: 'Get 10$ for every 20$ spent today.',
                style: null,
                color: 'color2',
                __typename: 'StyledText',
              },
            ],
            __typename: 'PromoTextBanner',
          },
          {
            linkClass: {
              url: 'https://www.childrensplace.com/us/home',
              title: '',
              target: '',
              external: 0,
              class: 'header__promo-text-banner',
              __typename: 'LinkClass',
            },
            textItems: [
              {
                text: 'NEED IT NOW?',
                style: 'style3',
                color: 'color3',
                __typename: 'StyledText',
              },
              {
                text: 'Buy online, pickup in store.',
                style: null,
                color: 'color1',
                __typename: 'StyledText',
              },
            ],
            __typename: 'PromoTextBanner',
          },
        ],
        __typename: 'Composite',
      },
      __typename: 'SubmoduleComposite',
    },
    loyaltyPromoBannerWrapper: {
      composites: {
        loyaltyPromoBanner: [
          {
            richText: {
              text:
                '<style>\r\n.header-loyalty-banner-wrapper {\r\n  height: 23px;\r\n  display: flex;\r\n  justify-content: space-between;\r\n}\r\n\r\n.header-loyalty-banner-wrapper__item {\r\n  display: flex;\r\n  align-items: center;\r\n}\r\n\r\n.header-loyalty-banner-wrapper__divider {\r\n  width: 1px;\r\n  height: 100%;\r\n  background-color: #d8d8d8;\r\n  margin: 0 8px;\r\n}\r\n\r\n.header-loyalty-banner-wrapper__text-content {\r\n  font-family: Arial;\r\n  font-size: 12px;\r\n  text-align: center;\r\n  color: #d8d8d8;\r\n}\r\n\r\n.content-3X {\r\n  width: auto;\r\n  font-size: 22px;\r\n  font-weight: bold;\r\n  padding: 0 8px;\r\n}\r\n\r\n.the-my-place-rewards {\r\n  width: 240px;\r\n  font-size: 10px;\r\n  line-height: 1.04;\r\n  text-align: left;\r\n  padding-right: 15px;\r\n}\r\n\r\n.header-loyalty-banner-wrapper__img {\r\n  height: 100%;\r\n  margin-right: 4px;\r\n}\r\n.header-loyalty-banner-wrapper__img img {\r\n  height: 100%;\r\n}\r\n\r\n@media (max-width: 767px) {\r\n  .hide-on-mobile {\r\n    display: none;\r\n  }\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  .header-loyalty-banner-wrapper__divider {\r\n    margin: 0 14px;\r\n  }\r\n  .the-my-place-rewards {\r\n    width: 320px;\r\n    font-size: 13px;\r\n  }\r\n  .header-loyalty-banner-wrapper__img {\r\n    height: 23px;\r\n    margin-right: 4px;\r\n  }\r\n}\r\n\r\n@media (min-width: 1200px) {\r\n\r\n  .header-loyalty-banner-wrapper__divider {\r\n    margin: 0 22px;\r\n  }\r\n  .header-loyalty-banner-wrapper {\r\n    height: 45px;\r\n  }\r\n  .header-loyalty-banner-wrapper__text-content {\r\n    font-size: 22px;\r\n  }\r\n\r\n  .content-3X {\r\n    font-size: 58px;\r\n  }\r\n\r\n  .the-my-place-rewards {\r\n    width: 570px;\r\n    font-size: 22px;\r\n  }\r\n  .header-loyalty-banner-wrapper__img {\r\n    height: 44px;\r\n    margin-right: 8px;\r\n  }\r\n\r\n}\r\n  </style>\r\n\r\n\r\n<div class="header-loyalty-banner-wrapper">\r\n  <div class="header-loyalty-banner-wrapper__item hide-on-mobile">\r\n    <div class="header-loyalty-banner-wrapper__text-content">\r\n      BONUS POINTS EVENTS\r\n    </div>\r\n  </div>\r\n\r\n  <div class="header-loyalty-banner-wrapper__divider hide-on-mobile"></div>\r\n\r\n  <div class="header-loyalty-banner-wrapper__item hide-on-mobile">\r\n    <div class="header-loyalty-banner-wrapper__text-content">\r\n      MARCH 7-APRIL 21, 2020\r\n    </div>\r\n  </div>\r\n  <div class="header-loyalty-banner-wrapper__divider hide-on-mobile"></div>\r\n  <div class="header-loyalty-banner-wrapper__item">\r\n    <div class="header-loyalty-banner-wrapper__img">\r\n          <img src="https://test5.childrensplace.com/image/upload//v1571658182/tcp-loyality.png" alt="Rewards">\r\n    </div>\r\n    <div class="header-loyalty-banner-wrapper__text-content content-3X">\r\n        3X\r\n  </div>\r\n  <div class="header-loyalty-banner-wrapper__text-content the-my-place-rewards">\r\n  THE MY PLACE REWARDS POINTS\r\nON ALL EASTER DRESS UPS & MATCHING FAMILY STYLES\r\n</div>\r\n\r\n  </div>\r\n\r\n</div>',
              userType: 'mpr',
            },
          },
          {
            richText: {
              text: '<div>PLCC Promo</div>',
              userType: 'plcc',
            },
          },
          {
            richText: {
              text: '<div>Guest Promo</div>',
              userType: 'guest',
            },
          },
        ],
      },
    },
    __typename: 'Submodule',
  },
  __typename: 'ModuleContent',
};
