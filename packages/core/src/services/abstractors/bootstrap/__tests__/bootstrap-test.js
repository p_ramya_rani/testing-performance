// 9fbef606107a605d69c0edbcd8029e5d 
import { errorMessages } from '@tcp/core/src/services/api.constants';
import { isMobileApp, pathHasHyphenSlash, isProduction } from '@tcp/core/src/utils';
import bootstrap, {
  retrieveCachedData,
  shouldInitiateSSRCall,
  checkAndLogErrors,
  labelsCategoriesToFetch,
  createLabelsSSRData,
  getLabelsParsedData,
} from '../bootstrap';
import labelsMock from '../labels/mock';
import headerMock from '../header/mock';
import footerMock from '../footer/mock';
import FooterAbstractor from '../footer';
import HeaderAbstractor from '../header';
import LabelsAbstractor from '../labels';
import BootStrapMockData from '../__mocks__/bootstrapData.mock';
import BootStrapGlobalLabelsMockData from '../__mocks__/bootstrapGlobalData.mock';

jest.mock('../layout/layout');
jest.mock('../../../handler/handler');

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  pathHasHyphenSlash: jest.fn(),
  isProduction: jest.fn(),
}));

describe('abstractor - bootstrap', () => {
  const abstractorBootstrap = () =>
    bootstrap('homepage', null, {}, { APIConfig: { language: 'en' } }, '/us/home').then(data => {
      expect(data.labels).toMatchObject(LabelsAbstractor.processData(labelsMock));
      expect(data.header).toMatchObject(HeaderAbstractor.processData(headerMock));
      expect(data.footer).toMatchObject(FooterAbstractor.processData(footerMock));
    });

  it('bootstrap - redis disabled', () => {
    isMobileApp.mockImplementation(() => false);
    pathHasHyphenSlash.mockImplementation(() => false);
    isProduction.mockImplementation(() => false);
    return abstractorBootstrap();
  });

  it('bootstrap - redis enabled', () => {
    global.redisClient = {
      connected: true,
    };
    isMobileApp.mockImplementation(() => false);
    return abstractorBootstrap();
  });
});

describe('retrieveCachedData', () => {
  it('retrieveCachedData - cachedData', () => {
    const cachedData = {
      test: 'val',
    };
    const key = 'test';
    const bootstrapData = {
      ...cachedData,
    };
    expect(bootstrapData[key]).toEqual(retrieveCachedData({ cachedData, key, bootstrapData }));
  });
});

describe('shouldInitiateSSRCall', () => {
  it('shouldInitiateSSRCall - default', () => {
    expect(shouldInitiateSSRCall('/us/c/boys-outfits', 'bot')).toBeFalsy();
  });
});

describe('labelsCategoriesToFetch', () => {
  const commonCategory = ['global'];
  it('labelsCategoriesToFetch - plp', () => {
    expect(labelsCategoriesToFetch('/us/c/boys-outfits')).toEqual([
      'PLP',
      'SLPs',
      'CLPs',
      ...commonCategory,
    ]);
  });

  it('labelsCategoriesToFetch - outfit', () => {
    expect(labelsCategoriesToFetch('/us/outfit/boysOutfits')).toEqual([
      'OutfitLabels',
      ...commonCategory,
    ]);
  });

  it('labelsCategoriesToFetch - checkout', () => {
    expect(labelsCategoriesToFetch('/us/checkout/shipping')).toEqual([
      'checkout',
      ...commonCategory,
    ]);
  });

  it('labelsCategoriesToFetch - account', () => {
    expect(labelsCategoriesToFetch('/us/account/shipping')).toEqual([
      'account',
      'checkout',
      ...commonCategory,
    ]);
  });

  it('labelsCategoriesToFetch - bag', () => {
    expect(labelsCategoriesToFetch('/us/bag')).toEqual(['account', 'checkout', ...commonCategory]);
  });

  it('labelsCategoriesToFetch - stores', () => {
    expect(labelsCategoriesToFetch('/us/stores')).toEqual(['StoreLocator', ...commonCategory]);
  });
});

describe('createLabelsSSRData', () => {
  const responseWeb = [
    {
      category: 'account',
      brand: 'TCP',
      country: 'USA',
      channel: 'Desktop',
      lang: '',
    },
    {
      category: 'checkout',
      brand: 'TCP',
      country: 'USA',
      channel: 'Desktop',
      lang: '',
    },
    {
      category: 'global',
      brand: 'TCP',
      country: 'USA',
      channel: 'Desktop',
      lang: '',
    },
  ];

  const responseApp = {
    category: '',
    subCategory: '',
    brand: 'TCP',
    country: 'USA',
    channel: 'Desktop',
    lang: '',
  };

  it('createLabelsSSRData - bag - web', () => {
    expect(
      createLabelsSSRData({
        originalUrl: '/us/bag',
        labels: { category: '', subCategory: '' },
        brand: 'TCP',
        country: 'USA',
        channel: 'Desktop',
        lang: '',
      })
    ).toEqual(responseWeb);
  });

  it('createLabelsSSRData - bag - App', () => {
    isMobileApp.mockImplementation(() => true);
    expect(
      createLabelsSSRData({
        originalUrl: '/us/bag',
        labels: { category: '', subCategory: '' },
        brand: 'TCP',
        country: 'USA',
        channel: 'Desktop',
        lang: '',
      })
    ).toEqual(responseApp);
  });
});

describe('getLabelsParsedData', () => {
  const bootstrapQueryParams = [
    {
      name: 'labels',
      data: [
        {
          category: 'global',
          brand: 'TCP',
          country: 'USA',
          channel: 'desktop',
          lang: '',
        },
      ],
    },
    {
      name: 'header',
      data: {
        type: 'header',
        brand: 'TCP',
        country: 'USA',
        channel: 'desktop',
        lang: '',
      },
    },
    {
      name: 'footer',
      data: {
        type: 'footer',
        brand: 'TCP',
        country: 'USA',
        channel: 'desktop',
        lang: '',
      },
    },
    {
      name: 'navigation',
      data: {
        brand: 'TCP',
        country: 'USA',
        channel: 'desktop',
        lang: '',
      },
    },
  ];
  it('getLabelsParsedData - bag - web', () => {
    expect(getLabelsParsedData({ bootstrapData: BootStrapMockData, bootstrapQueryParams })).toEqual(
      [...BootStrapGlobalLabelsMockData.global]
    );
  });
});

describe('checkAndLogErrors', () => {
  it('checkAndLogErrors - default', () => {
    const bootstrapData = {
      labels: [{ errorMessage: 'error' }],
      header: { errorMessage: 'error' },
      footer: { errorMessage: 'error' },
      home: { errorMessage: 'error' },
      navigation: [{ errorMessage: 'error' }],
    };
    const {
      headerDataErrorCode,
      footerDataErrorCode,
      labelsDataErrorCode,
      navigationDataErrorCode,
      layoutDataErrorCode,
      layoutDataError,
      headerDataError,
      footerDataError,
    } = errorMessages;
    const returnValue = {
      footer_error: 1,
      footer_error_message: footerDataError,
      header_error: 1,
      header_error_message: headerDataError,
      labels_error: 1,
      labels_error_message: 'error',
      layout_error: 1,
      layout_error_message: layoutDataError,
      navigation_error: 1,
      navigation_error_message: 'error',
      bootstrapErrorCode: `${headerDataErrorCode}, ${footerDataErrorCode}, ${navigationDataErrorCode}, ${labelsDataErrorCode}, ${layoutDataErrorCode}`,
    };
    expect(
      checkAndLogErrors(bootstrapData, 'home', [
        'footer',
        'header',
        'labels',
        'navigation',
        'layout',
      ])
    ).toEqual(returnValue);
  });
});
