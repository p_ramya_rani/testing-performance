// 9fbef606107a605d69c0edbcd8029e5d 
import * as sessionSelectors from './session.selectors';
import * as labelsSelectors from './labels.selectors';

export default {
  sessionSelectors,
  labelsSelectors,
};
