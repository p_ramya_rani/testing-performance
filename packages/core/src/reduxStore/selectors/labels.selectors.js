// 9fbef606107a605d69c0edbcd8029e5d 
export const getPlpTilesLabels = state => {
  let labels = {};
  if (state.Labels && state.Labels.PLP && state.Labels.PLP.plpTiles) {
    labels = state.Labels.PLP.plpTiles;
  }
  return labels;
};

export const getSLPLabels = state => {
  return state.Labels.SLPs && state.Labels.SLPs.SLPs_Sub;
};
