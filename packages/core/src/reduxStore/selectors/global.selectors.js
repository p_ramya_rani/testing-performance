// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';

import { APICONFIG_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';

export const getApiConfigState = state => {
  return state[APICONFIG_REDUCER_KEY];
};

export const getBrandSwitchedState = createSelector(
  getApiConfigState,

  apiConfig => apiConfig.isBrandSwitched || false
);

export const getIsLocationEnabled = createSelector(
  getApiConfigState,

  apiConfig => apiConfig.locationEnabled || false
);

export default {
  getBrandSwitchedState,

  getIsLocationEnabled,
};
