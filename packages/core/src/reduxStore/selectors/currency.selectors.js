// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';

const getSiteDetails = (state) => state.session.siteDetails;

const getDefaultCurrencyAttributes = {
  exchangevalue: 1,
  merchantMargin: 1,
  roundMethod: '',
};

export const getCurrentCurrency = (state) => {
  return state.session.siteDetails.currency;
};

export const getCurrencyAttributes = createSelector(getSiteDetails, (siteDetails) => {
  return (siteDetails && siteDetails.currencyAttributes) || getDefaultCurrencyAttributes;
});
