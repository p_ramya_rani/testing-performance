/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { API_CONFIG } from '@tcp/core/src/services/config';
import { isCanada, isClient, readCookie } from '@tcp/core/src/utils';
import { createSelector } from 'reselect';
import { SESSIONCONFIG_REDUCER_KEY } from '../../constants/reducer.constants';
import { defaultCountries, USA_VALUES } from '../../constants/site.constants';
import {
  getBrand,
  parseBoolean,
  isMobileApp,
  getABtestFromState,
  getAPIConfig,
  isIOS,
  isTCP,
  isUsOnly,
} from '../../utils';

const getSiteDetails = (state) => {
  return (state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails) || {};
};

export /**
 *
 * @function getCurrentCountry
 * @param {*} state
 * @description this selector gives current country selected.
 */
const getCurrentCountry = (state) => {
  return state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails.country;
};

export /**
 *
 * @function getIsInternationalShipping
 * @param {*} state
 * @description this selector gives whether current country is other than US/CA.
 */
const getIsInternationalShipping = (state) => {
  return (
    getCurrentCountry(state) !== defaultCountries[0].id &&
    getCurrentCountry(state) !== defaultCountries[1].id
  );
};

export const getIsRadialInventoryEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_RADIAL_BOSS_ENABLED
  );
};

export const getIsBrierleyPromoEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_BRIERLEY_PROMO_ENABLED
  );
};

export const getIsPLPCarouselEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_PLP_CAROUSEL_ENABLED)
  );
};

const getCrossBrandFlags = (state, key) => {
  const brandFlag = parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails[key]
  );
  const otherBrandFlag = parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].otherBrandSiteDetails[key]
  );

  return { brandFlag, otherBrandFlag };
};

export const getIsBossAppEnabled = (state) => {
  const brand = getBrand();
  const { brandFlag, otherBrandFlag } = getCrossBrandFlags(state, 'BOSS_ENABLED_APP');
  if (brand && brand.toUpperCase() === API_CONFIG.TCP_CONFIG_OPTIONS.brandId.toUpperCase()) {
    return { isBossEnabledAppTCP: brandFlag, isBossEnabledAppGYM: otherBrandFlag };
  }

  return { isBossEnabledAppTCP: otherBrandFlag, isBossEnabledAppGYM: brandFlag };
};

export const getIsBossEnabled = (state, brand = getBrand()) => {
  let isMobileAppFlag = true;
  if (isMobileApp()) {
    const isBOSSEnabledAppFlag = brand && `isBossEnabledApp${brand.toUpperCase()}`;
    isMobileAppFlag = getIsBossAppEnabled(state)[isBOSSEnabledAppFlag];
  }
  const isBOSSEnabled = brand && `isBOSSEnabled_${brand.toUpperCase()}`;
  return (
    isMobileAppFlag &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails[isBOSSEnabled]
  );
};

export const getIsBopisEnabled = (state, brand = getBrand()) => {
  const isBOPISEnabled = brand && `isBOPISEnabled_${brand.toUpperCase()}`;
  return (
    state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails[isBOPISEnabled]
  );
};

export const getIsBossClearanceProductEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.BOSS_ENABLED_CLEARANCE_PRODUCTS
  );
};

export const getIsBopisClearanceProductEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.BOPIS_ENABLED_CLEARANCE_PRODUCTS
  );
};

export const getCurrentCurrency = (state) => {
  return state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails.currency;
};

export const getIsPdpServiceEnabled = (state) => {
  if (isMobileApp()) {
    return (
      state &&
      state.session.siteDetails &&
      state.session.siteDetails.IS_PDP_SERVICE_ENABLED_APP &&
      parseBoolean(state.session.siteDetails.IS_PDP_SERVICE_ENABLED_APP)
    );
  }
  return (
    state &&
    state.session.siteDetails &&
    state.session.siteDetails.IS_PDP_SERVICE_ENABLED &&
    parseBoolean(state.session.siteDetails.IS_PDP_SERVICE_ENABLED)
  );
};

export const getCurrentCurrencySymbol = (state) => {
  const country = getCurrentCountry(state);
  if (country === 'US' || country === 'CA') {
    return '$';
  }
  const currency = getCurrentCurrency(state);
  return currency === USA_VALUES.currency ? USA_VALUES.currencySymbol : `${currency} `;
};

export const getRecalcOrderPointsInterval = (state) => {
  return parseInt(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.BRIERLEY_ORD_RECALC_CACHING_INTERVAL,
    10
  );
};

export const getTcpSegmentValue = (state) => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails.tcpSegment
  );
};

export const getIsKeepAliveProduct = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.KEEPALIVE_PRODUCTFLAG
  );
};

export const getIsKeepAliveProductApp = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.KEEPALIVE_PRODUCTFLAG_APP
  );
};

export const getIsForterEnabled = (state) => {
  return (
    !state[SESSIONCONFIG_REDUCER_KEY] ||
    (state[SESSIONCONFIG_REDUCER_KEY].siteDetails.FORTER_ENABLED !== 0 &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.FORTER_ENABLED !== '0')
  );
};

export const getABTestIsCSHEnabled = (state) => {
  return getABtestFromState(state.AbTest).isCSHEnabled;
};

export const getCurrentSiteLanguage = (state) => {
  return state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails.language;
};

export const getABTestIsCSHAppEnabled = (state) => {
  return getABtestFromState(state.AbTest).isCSHAppEnabled;
};

export const getIsCSHEnabled = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.CSH_ENABLED)
  );
};

const afterPayAppFeatureStatus = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_AFTERPAY_APP_FEATURE_ENABLED)
  );
};

export const getIsAfterPayEnabled = (state) => {
  if (!isMobileApp()) {
    const currentLang = getCurrentSiteLanguage(state);
    if (isCanada() || currentLang === 'es' || getIsInternationalShipping(state)) return false;
  }

  const AbTests = getABtestFromState(state.AbTest);
  const disableBNPL = AbTests && AbTests.disableBNPLFeature;

  if (disableBNPL) {
    return false;
  }

  if (isMobileApp()) {
    return !isCanada() && afterPayAppFeatureStatus(state);
  }

  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_AFTERPAY_FEATURE_ENABLED)
  );
};

export const getAfterPayMinAmount = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.AFTERPAY_MIN_ITEM_AMOUNT
  );
};

export const getAfterPayMinOrderAmount = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.AFTERPAY_MIN_ORDER_AMOUNT
  );
};

export const getAfterPayMaxOrderAmount = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.AFTERPAY_MAX_ORDER_AMOUNT
  );
};

export const getIsPaidNExpediteRefundEnable = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.PAID_EXPEDITE_REFUND_ENABLE)
  );
};

export const getIsPaidNExpediteRefundEnableApp = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.PAID_EXPEDITE_REFUND_ENABLE_APP)
  );
};

export const getIsCSHAppEnabled = (state) => {
  const abTestIsCSHEnabled = getABTestIsCSHAppEnabled(state);
  if (abTestIsCSHEnabled) {
    return true;
  }
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.CSH_APP_ENABLED)
  );
};

export const getBootstrapError = (state) => {
  return state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].bootstrapError;
};

export const getIsPayPalEnabled = (state) => {
  if (isMobileApp() && isIOS()) {
    return false;
  }
  const key = 'BOPIS_MIXCART_PAYPAL_ENABLED';
  return (
    state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails[key] === 'TRUE'
  );
};

export const getIsLogonRecaptchaEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails.LOGON_RECAPTCHA
  );
};

export const getIsResetRecaptchaEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.RESET_PASSWORD_RECAPTCHA_ENABLED
  );
};

export const getIsSuppressGetOrderDetails = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.SUPPRESS_GETORDERDETAILS_WITH_ATC_BRIERLEY
  );
};

export const getIsSmsUpdatesEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.TRANS_VIBES_SMS_ENABLED
  );
};

export const getRegisteredUserInfoCachingInterval = (state) => {
  const cachingTTL =
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.GETREGDUSERINFO_CACHING_INTERVAL;
  if (typeof cachingTTL !== 'undefined') {
    return parseInt(cachingTTL, 10);
  }

  return cachingTTL;
};

export const getIsRecapchaEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.GIFT_CARD_RECAPTCHA_ENABLED)
  );
};

export const getABTestIsShowPriceRange = (state) => {
  return getABtestFromState(state.AbTest).getPriceRange;
};

export const getABTestAppCategoryCarouselL3 = (state) => {
  return getABtestFromState(state.AbTest).appCategoryCarouselL3;
};

export const getDisableGymCrossBrandL1 = (state) => {
  return getABtestFromState(state.AbTest).disableGymCrossBrandL1;
};

export const getABTestAddToBagDrawer = (state) => {
  return getABtestFromState(state.AbTest).addToBagDrawerFlag;
};

export const getABTestPdpService = (state) => {
  return getABtestFromState(state.AbTest).enablePDPService;
};

export const getABTestSwapMpackPills = (state) => {
  return getABtestFromState(state.AbTest).disableSwappingMpackPills;
};

export const getIsShowPriceRange = (state) => {
  return state.session && state.session.siteDetails.PRICE_RANGE_SWITCH;
};

export const getIsSessionSharingEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_SESSION_SHARING_ENABLED)
  );
};

export const getMultiPackThreshold = (state) => {
  return state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.MULTIPACK_INVENTORY_THRESHOLD
    ? state.session.siteDetails.MULTIPACK_INVENTORY_THRESHOLD
    : false;
};

export const getIsShowPriceRangeForApp = (state) => {
  return state.session && state.session.siteDetails.PRICE_RANGE_SWITCH_APP;
};

export const getPreviewDate = (state) => state && state.APIConfig && state.APIConfig.previewDate;

export const downloadAppPromoEnabledFooter = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_APP_DOWNLOAD_FOOTER_ENABLED)
  );
};

export const getAppBanner = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_APP_BANNER_ENABLED)
  );
};

/**
 * @description get coupons timeout value from the xapp config in minutes, default is 15 for qa testing.
 * @param {object} state
 */
export const getCouponsTimeout = (state) =>
  (state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.COUPONS_UPDATE_TIMEOUT) ||
  15;

export const getIsBagCarouselEnabled = (state) => {
  return getABtestFromState(state.AbTest).isHomeBagCarosuelABTest;
};

export const getIsCategoryCarouselEnabled = (state) => {
  return getABtestFromState(state.AbTest).isHomeCategoryCarouselABTest;
};

export const getIsPlpCategoryFiltersEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_PLP_CATEGORY_FILTERS_ENABLED)
  );
};

export const getIsPerUnitPriceEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_PHYSICAL_MULTIPACK_ENABLED) &&
    parseBoolean(state.session.siteDetails.IS_PER_ITEM_PRICE_ENABLED)
  );
};

export const getIsCompleteTheLookTestEnabled = (state) => {
  return getABtestFromState(state.AbTest).pdpMobileWebDesign;
};
export const getCategoryFilterEnabledList = (state) => {
  return getABtestFromState(state.AbTest).plpSubCategoryFilters;
};

export const getIsAvoidGetOrderDetails = (state) => {
  const isInternationalShipping = isMobileApp() ? false : getIsInternationalShipping(state);
  const reducerKeyATB = 'IS_STORE_ATB_DETAILS_ENABLED';
  return (
    parseBoolean(
      state &&
        state[SESSIONCONFIG_REDUCER_KEY] &&
        state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
        state[SESSIONCONFIG_REDUCER_KEY].siteDetails[reducerKeyATB]
    ) && !isInternationalShipping
  );
};

export const getChangeFilterViewABTest = (state) => {
  return getABtestFromState(state.AbTest).changeFilterViewABTest;
};

export const getCartIconCheckoutNavigationABTest = (state) => {
  return getABtestFromState(state.AbTest).hideCartIconCheckoutNavigation;
};

export const getIsChangeRecalForEcom = (state) => {
  return parseBoolean(
    state &&
      state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails
        .CART_PAGE_RECALC_FOR_ECOM_AFTER_TIME_PERIOD_ENABLED
  );
};

export const getStoreSearchTypesParam = (state) => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.mapboxStoreSearchTypesParam
  );
};

export const getIsBopisOptionSelected = (state) => {
  return state.PickupModal && state.PickupModal.get('isBopisPickup');
};

export const getAutocompleteTypesParam = (state) => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.mapboxAutocompleteTypesParam
  );
};

export const getMapboxSwitch = (state) => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.mapboxSwitch === 'true'
  );
};

export const getFlipSwitchState = (state) => {
  return parseBoolean(
    state &&
      state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_UNBXD_FAILOVER_ENABLED
  );
};

export const getUnbxdDRUrlState = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.UNBXD_FAILOVER_DOMAIN
  );
};

export const getABTestOffStyleWith = (state) => {
  return parseBoolean(getABtestFromState(state.AbTest).disableStyleWithSectionATB);
};

export const getIsPhysicalMpackEnabled = (state) => {
  return parseBoolean(
    state &&
      state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_PHYSICAL_MULTIPACK_ENABLED
  );
};

export const getMultiColorNameEnabled = (state) => {
  return parseBoolean(
    state &&
      state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_MULTI_COLOR_NAME_ENABLED
  );
};

export const getStoreLocatorMapboxApi = (state) => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.STORE_LOCATOR_STORES_MAPBOX_API === 'true'
  );
};

export const getStoreLocatorMapboxTileIDs = (state) => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.STORE_LOCATOR_STORES_TILESET_ID
  );
};

export const getIsPlpPdpAnchoringEnabled = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_PLP_PDP_ANCHORING_ENABLED &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_PLP_PDP_ANCHORING_ENABLED)
  );
};

export const getABTestForImageSwatches = (state) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(getABtestFromState(state.AbTest).imageSwatchABTest);
    }, 500);
  });
};

export const getnewPLPExperienceAbTest = (state) => {
  return getABtestFromState(state.AbTest) && getABtestFromState(state.AbTest).newPLPExperience;
};

export const getIsNewQVEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_NEW_QV_DESIGN_ENABLED)
  );
};

export const getIsNewPDPEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_NEW_PDP_DESIGN_ENABLED)
  );
};

export const getIsImageSwatchDisabled = (state) => {
  return state && state.AbTest && state.AbTest.imageSwatchABTest;
};

export const getIsChatBotEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_CHAT_BOT_ENABLED
  );
};
export const getABTestForChatBot = (state) => {
  return getABtestFromState(state.AbTest).chatBotABTest;
};

export const getCSHOlderMonth = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseInt(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.CSH_FOR_OLDER_MONTH, 10)
  );
};

export const getCSHReturnItemOlderDays = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseInt(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.CSH_FOR_INITIATE_RETURN_DAYS, 10)
  );
};

export const getIsSeoHeaderEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_SEO_HEADER_ENABLED
  );
};

export const getABTestForPDPColorOrImageSwatchHover = (state) => {
  const abTestFromState = getABtestFromState(state.AbTest);
  return abTestFromState && abTestFromState.changePDPPrimaryImageOnHover;
};

export const getIsBabyL2NestingDisabled = (state) => {
  return getABtestFromState(state.AbTest) && getABtestFromState(state.AbTest).disableBabyL2Nesting;
};

export const getIsABLoaded = (state) => {
  return state && state.AbTest && state.AbTest.abTestComplete;
};

export const getIsPersistPointsApiEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.ENABLE_FE_CALC_LOYALTY_POINTS
  );
};

export const getLoyaltyPointsMultiplier = (state) => {
  const siteDetails =
    (state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails) || {};
  return {
    rewardsDollarPoints: parseInt(siteDetails.LOYALTY_POINTS_REWARD_POINTS || 100, 10),
    rewardsDollarAmount: parseInt(siteDetails.LOYALTY_POINTS_REWARD_AMOUNT || 5, 10),
  };
};

export const getMonetateProductsRefreshTime = (state) => {
  return (
    (state &&
      state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.MONETATE_PLP_PRODS_REFRESH_TIME) ||
    900
  );
};

export const getUnbxdProductsRefreshFrequency = (state) => {
  return (
    (state &&
      state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.UNBXD_PLP_PRODS_REFRESH_FREQUENCY) ||
    60
  );
};

export const getIsCouponSelfHelpEnabled = (state) => {
  return (
    (state &&
      state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_CANNOT_APPLY_COUPON_ENABLED)) ||
    false
  );
};

export const getIsDynamicBadgeEnabled = createSelector(getSiteDetails, (siteDetails) => {
  return {
    dynamicBadgeMPACKEnabled: parseBoolean(siteDetails.IS_DYNAMIC_BADGE_ENABLED),
    dynamicBadgeVirtualMPACKEnabled: parseBoolean(siteDetails.IS_DYNAMIC_BADGE_VMPACK_ENABLED),
    dynamicBadgeSetEnabled: parseBoolean(siteDetails.IS_DYNAMIC_BADGE_SET_ENABLED),
    dynamicBadgeVirtualSetEnabled: parseBoolean(siteDetails.IS_DYNAMIC_BADGE_VSET_ENABLED),
  };
});

export const getABTestForStickyFilter = (state) => {
  const abTestFromState = getABtestFromState(state.AbTest);
  return abTestFromState && abTestFromState.stickyFilterABTest;
};

export const getIsATBHidePoints = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isHidePointsEnabled;
};

export const getIsSNJEnabled = (state) => {
  const snjAlphaTesting = isClient() && readCookie('snjalphatesting');
  const isSNJEnabled =
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_SNJ_ENABLED);
  return parseBoolean(snjAlphaTesting) || isSNJEnabled;
};

export const getCartsSessionStatus = (state) => {
  return (state && state.session && state.session.cartsSessionStatus) || 0;
};

export const getIsShowNavAnimation = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_MICRO_ANIMATION_ENABLED
  );
};
export const getIsDisableBagAnchoring = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isDisableBagAnchoring;
};

/**
 *  Update : AB test check removed for isNewReDesignEnabled
 */
export const getIsNewReDesignEnabled = () => {
  if (!isMobileApp()) return false;

  const apiConfigObj = getAPIConfig();
  const { redesignEnvConfigEnabled } = apiConfigObj;
  if (redesignEnvConfigEnabled && redesignEnvConfigEnabled === 'false') {
    return false;
  }
  return true;
};

export const getIsATBModalBackAbTestNewDesign = (state) => {
  if (isMobileApp()) {
    const apiConfigObj = getAPIConfig();
    const { redesignEnvConfigEnabled } = apiConfigObj;
    if (redesignEnvConfigEnabled && redesignEnvConfigEnabled === 'false') {
      return false;
    }
    return true;
  }
  const getAbtests = getABtestFromState(state.AbTest);
  const atbKillswitch = parseBoolean(getSiteDetails(state).IS_ADDED_TO_BAG_REDESIGN_ENABLED);
  if (!atbKillswitch) {
    return true;
  }
  return getAbtests && getAbtests.isATBModalRedesignDisabled;
};

export const getIsHapticFeedbackEnabled = () => {
  const { hapticFeedbackDisabled } = getAPIConfig();
  if (hapticFeedbackDisabled) {
    return false;
  }
  return true;
};

export const getIsPDPSmoothScrollEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_PDP_SMOOTH_SCROLL_ENABLED
  );
};

export const getIsHpNewLayoutEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_HP_NEW_LAYOUT_ENABLED
  );
};

export const getIsHpNewDesignCTAEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_HP_NEW_DESIGN_CTA_ENABLED
  );
};

export const getIsHpNewModuleDesignEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_HP_NEW_DESIGN_MODULE_ENABLED
  );
};

export const getIsViewThisLookLabelABTestEnabled = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isViewThisLookLabelABTestEnabled;
};

export const getPageNameFromState = (state) => {
  const pageName = (state && state.pageData && state.pageData.pageName) || 'account';

  if (pageName.includes('account') || pageName.includes('static')) return 'account';
  if (pageName.includes('shopping')) return 'cart';
  if (pageName.includes('appliction') || pageName.includes('application')) return 'account';
  if (pageName.includes('product:')) return 'pdp';
  if (pageName.includes('browse:')) return 'plp';
  if (pageName.includes('search:')) return 'search';
  if (pageName.includes('myplace:wallet')) return 'myplace:wallet';
  return pageName;
};

export const getIsLiveChatEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.LIVE_CHAT_ENABLED
  );
};

export const getIsMobileLiveChatEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.LIVE_CHAT_ENABLED_APP
  );
};
export const getIsMergeMyPlaceAccount = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.MERGE_MY_PLACE_ACCOUNT_ENABLED
  );
};

export const getIsGiftCardBalance = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.EGIFTCARD_BALANCE_CHECK_ENABLED
  );
};

export const getIsLiveChatBackABisOff = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return !(getAbtests && getAbtests.isLiveChatDisabled);
};

export const getIsNewLoyaltyBanner = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.enablePlccOffBag;
};
export const getIsNonInventoryABTestEnabled = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isNonInventoryABTestEnabled;
};
export const getQuickAddThreshold = (state) => {
  return parseInt(getSiteDetails(state).QUICK_ADD_THRESHOLD, 10) || 0;
};
export const getOptimisticAddBagModal = (state) => {
  return parseInt(getSiteDetails(state).IS_OPTIMISTIC_ADD_TO_BAG, 10) || 0;
};

export const getIsFamilyOutfitABTestEnabled = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isFamilyOutfitABTestEnabled;
};

export const getIsNewBag = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return (
    parseBoolean(
      state[SESSIONCONFIG_REDUCER_KEY] &&
        state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_BAG_REDESIGN_ENABLED
    ) && !(getAbtests && getAbtests.isBagBackAbTestNewDesign)
  );
};

export const getIsShowLowInventoryBanner = (state) => {
  if (isMobileApp()) {
    return false;
  }
  const getAbtests = getABtestFromState(state.AbTest);
  return (
    parseBoolean(
      state[SESSIONCONFIG_REDUCER_KEY] &&
        state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_BAG_REDESIGN_ENABLED
    ) &&
    getAbtests &&
    getAbtests.isLowInventoyBannerAbTest &&
    isTCP() &&
    isUsOnly()
  );
};

export const getIsShowLowInventoryBannerForOldDesign = (state) => {
  if (isMobileApp()) {
    return false;
  }
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isLowInventoyBannerAbTest && isTCP() && isUsOnly();
};

export const getIsNonInvSessionFlag = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.stageNonInventorySessionFlag
  );
};

export const getIsShowBrandNameEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_SHOW_BRAND_NAME_ENABLED
  );
};

export const getIsNewMiniBag = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return (
    parseBoolean(
      state[SESSIONCONFIG_REDUCER_KEY] &&
        state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_MINIBAG_REDESIGN_ENABLED
    ) && !(getAbtests && getAbtests.disabledMiniBagNewDesign)
  );
};
export const getIsNewWhenWillMyOrderArriveEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_WHEN_WILL_MY_ORDER_ARRIVE_ENABLED
  );
};

/**
 *
 * @function getIsSingleAddToBagAPIEnabled
 * @param {*} state
 * @description this selector gives is single multi add to bag API is enabled
 */
export const getIsSingleAddToBagAPIEnabled = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails
        .ENABLE_ADDING_MULTILE_ITEMS_TO_BAG_IN_ONE_API_CALL
  );
};

export const getIsOtherBrandExpABTestEnabled = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isOtherBrandExpABTestEnabled;
};

export const getIsHideStickyCheckoutButton = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isHideStickyCheckoutAbTest;
};

export const getCreateAccountAfterGuestUserApplyForPLCCCard = (state) => {
  return parseBoolean(
    state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails
        .CREATE_ACCOUNT_AFTER_GUEST_USER_APPLY_FOR_PLCC_CARD
  );
};

export const getApplePayHpBannerAbTestEnabled = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.applePayHpBannerAbTestEnabled;
};

export const getBirthdaySavingsCallEnabled = (state) => {
  return parseInt(getSiteDetails(state).CALL_GET_BIRTHDAY_SAVING_SERVICE_ON_APP_LOAD, 10) || 0;
};

export const getIsBopisFilterEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_BOPIS_FILTER_DISPLAY_ENABLED)
  );
};

export const getIsHideMeatBallMenuAbTest = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && (getAbtests.isShowCTAAbTest || getAbtests.isShowEditCTAAbTest);
};

export const getIsShowEditCTAAbTest = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.isShowEditCTAAbTest;
};

export const getIsFBTAbTestEnabled = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests && getAbtests.enableFBTabTest;
};

export const getIsStickyPaymentAbTestEnabled = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return (
    (getAbtests && getAbtests.enableStickyPaymentAbTest && !getIsInternationalShipping(state)) ||
    false
  );
};

export const getIsStickyCheckoutAbTestEnabled = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return (
    (getAbtests && getAbtests.enableStickyCheckoutAbTest && !getIsInternationalShipping(state)) ||
    false
  );
};

export const getIsResetViaText = (state) => {
  if (isMobileApp()) {
    return parseBoolean(
      state[SESSIONCONFIG_REDUCER_KEY] &&
        state[SESSIONCONFIG_REDUCER_KEY].siteDetails.RESET_PASSWORD_SMS_OTP_ENABLED
    );
  }
  return (
    parseBoolean(
      state[SESSIONCONFIG_REDUCER_KEY] &&
        state[SESSIONCONFIG_REDUCER_KEY].siteDetails.RESET_PASSWORD_SMS_OTP_ENABLED
    ) &&
    (isUsOnly() || isCanada())
  );
};
