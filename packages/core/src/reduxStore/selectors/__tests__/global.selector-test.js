// 9fbef606107a605d69c0edbcd8029e5d
import {
  APICONFIG_REDUCER_KEY,
  SESSIONCONFIG_REDUCER_KEY,
} from '@tcp/core/src/constants/reducer.constants';
import { getBrandSwitchedState, getIsLocationEnabled } from '../global.selectors';
import {
  getIsSeoHeaderEnabled,
  getIsAfterPayEnabled,
  getAfterPayMinAmount,
  getAfterPayMinOrderAmount,
  getAfterPayMaxOrderAmount,
} from '../session.selectors';

describe('Global selectors', () => {
  const state = {
    [APICONFIG_REDUCER_KEY]: {
      locationEnabled: false,
      isBrandSwitched: false,
    },
  };

  it('getIsLocationEnabled - false', () => {
    expect(getIsLocationEnabled(state)).toEqual(false);
  });

  it('getBrandSwitchedState - false', () => {
    expect(getBrandSwitchedState(state)).toEqual(false);
  });

  const state2 = {
    [APICONFIG_REDUCER_KEY]: {
      locationEnabled: true,
      isBrandSwitched: true,
    },
  };

  it('getIsLocationEnabled - true', () => {
    expect(getIsLocationEnabled(state2)).toEqual(true);
  });

  it('getBrandSwitchedState - true', () => {
    expect(getBrandSwitchedState(state2)).toEqual(true);
  });
});
describe('should return getIsSeoHeaderEnabled ', () => {
  const siteDetails = {
    siteDetails: {
      IS_SEO_HEADER_ENABLED: true,
    },
  };
  const state3 = {
    [SESSIONCONFIG_REDUCER_KEY]: siteDetails,
  };
  it('should return getIsSeoHeaderEnabled value from session', () => {
    expect(getIsSeoHeaderEnabled(state3)).toEqual(true);
  });
});

describe('#getIsAfterPayEnabled ', () => {
  const siteDetails = {
    siteDetails: {
      IS_AFTERPAY_FEATURE_ENABLED: true,
      country: 'US',
    },
  };
  const state3 = {
    [SESSIONCONFIG_REDUCER_KEY]: siteDetails,
  };
  it('should return getIsAfterPayEnabled value from session', () => {
    expect(getIsAfterPayEnabled(state3)).toEqual(true);
  });

  it('should return false value for disableBNPLFeature abtest', () => {
    expect(getIsAfterPayEnabled({ ...state3, AbTest: { disableBNPLFeature: true } })).toEqual(
      false
    );
  });
});

describe('#getAfterPayMinAmount ', () => {
  const siteDetails = {
    siteDetails: {
      AFTERPAY_MIN_ITEM_AMOUNT: '10',
    },
  };
  const state3 = {
    [SESSIONCONFIG_REDUCER_KEY]: siteDetails,
  };
  it('should return getAfterPayMinAmount value from session', () => {
    expect(getAfterPayMinAmount(state3)).toEqual(siteDetails.siteDetails.AFTERPAY_MIN_ITEM_AMOUNT);
  });
});

describe('#getAfterPayMinOrderAmount', () => {
  const siteDetails = {
    siteDetails: {
      AFTERPAY_MIN_ORDER_AMOUNT: '15',
    },
  };
  const state3 = {
    [SESSIONCONFIG_REDUCER_KEY]: siteDetails,
  };
  it('should return getAfterPayMinOrderAmount value from session', () => {
    expect(getAfterPayMinOrderAmount(state3)).toEqual(
      siteDetails.siteDetails.AFTERPAY_MIN_ORDER_AMOUNT
    );
  });
});

describe('#getAfterPayMaxOrderAmount', () => {
  const siteDetails = {
    siteDetails: {
      AFTERPAY_MAX_ORDER_AMOUNT: '1000',
    },
  };
  const state4 = {
    [SESSIONCONFIG_REDUCER_KEY]: siteDetails,
  };
  it('should return getAfterPayMaxOrderAmount value from session', () => {
    expect(getAfterPayMaxOrderAmount(state4)).toEqual(
      siteDetails.siteDetails.AFTERPAY_MAX_ORDER_AMOUNT
    );
  });
});
