// 9fbef606107a605d69c0edbcd8029e5d
import { getFormSKUValue } from '../../utils/utils';

const getFormValues = (stateForm, formName) => {
  return stateForm[formName] && stateForm[formName].values;
};

const getAddedToBagFormValues = (stateForm, formName) => {
  const formsInState = stateForm && stateForm.form ? stateForm.form : stateForm;
  const formValues = getFormValues(formsInState, formName);
  return (
    formValues && {
      ...getFormSKUValue(formValues),
    }
  );
};

export default getAddedToBagFormValues;
