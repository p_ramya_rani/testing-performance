// 9fbef606107a605d69c0edbcd8029e5d
import GLOBAL_CONSTANTS from '../constants';

const ModulesReducer = (state = {}, action = '') => {
  switch (action.type) {
    case GLOBAL_CONSTANTS.LOAD_MODULES_DATA:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default ModulesReducer;
