// 9fbef606107a605d69c0edbcd8029e5d
import GLOBAL_CONSTANTS from '../constants';
import { API_CONFIG } from '../../services/config';

const { siteIds } = API_CONFIG;
const initialState = {
  bootstrapError: null,
  siteDetails: {
    tcpSegment: 'default',
    stageNonInventorySessionFlag: false,
  },
  otherBrandSiteDetails: {},
  siteOptions: {
    countriesMap: [],
    currenciesMap: [],
    sitesTable: {
      [siteIds.us]: {
        languages: [
          {
            id: 'en',
            displayName: 'English',
          },
          {
            id: 'es',
            displayName: 'Spanish',
          },
        ],
      },
      [siteIds.ca]: {
        languages: [
          {
            id: 'en',
            displayName: 'English',
          },
          {
            id: 'fr',
            displayName: 'French',
          },
        ],
      },
    },
  },
};

const getCurrency = (action) => {
  return action.payload.currency || 'USD';
};

// eslint-disable-next-line complexity
const SessionConfigReducer = (state = initialState, action = '') => {
  switch (action.type) {
    case GLOBAL_CONSTANTS.SET_XAPP_CONFIG:
      return { ...state, siteDetails: { ...state.siteDetails, ...action.payload } };
    case GLOBAL_CONSTANTS.SET_XAPP_CONFIG_OTHER_BRAND:
      return {
        ...state,
        otherBrandSiteDetails: { ...state.otherBrandSiteDetails, ...action.payload },
      };
    case GLOBAL_CONSTANTS.SET_BOSS_BOPIS_FLAGS:
      return { ...state, siteDetails: { ...state.siteDetails, ...action.payload } };
    case GLOBAL_CONSTANTS.SET_COUNTRY:
      return { ...state, siteDetails: { ...state.siteDetails, country: action.payload } };
    case GLOBAL_CONSTANTS.SET_CURRENCY:
      return {
        ...state,
        siteDetails: {
          ...state.siteDetails,
          currency: getCurrency(action),
          currencyAttributes: action.payload.currencyAttributes,
        },
      };
    case GLOBAL_CONSTANTS.SET_LANGUAGE:
      return { ...state, siteDetails: { ...state.siteDetails, language: action.payload } };
    case GLOBAL_CONSTANTS.COUNTRY_LIST_STORE_COUNTRIES_MAP:
      return { ...state, siteOptions: { ...state.siteOptions, countriesMap: action.payload } };
    case GLOBAL_CONSTANTS.COUNTRY_LIST_STORE_CURRENCIES_MAP:
      return { ...state, siteOptions: { ...state.siteOptions, currenciesMap: action.payload } };
    case GLOBAL_CONSTANTS.GET_SET_TCP_SEGMENT:
      return { ...state, siteDetails: { ...state.siteDetails, tcpSegment: action.payload } };
    case GLOBAL_CONSTANTS.BOOTSTRAP_API_ERROR:
      return { ...state, bootstrapError: { ...state.bootstrapError, ...action.payload } };
    case GLOBAL_CONSTANTS.SET_CARTS_SESSION_STATUS:
      return { ...state, cartsSessionStatus: action.payload };
    case GLOBAL_CONSTANTS.SET_NON_INVENTORY_STAGE_SESSION_FLAG:
      return {
        ...state,
        siteDetails: { ...state.siteDetails, stageNonInventorySessionFlag: action.payload },
      };
    default:
      return state;
  }
};

export default SessionConfigReducer;
