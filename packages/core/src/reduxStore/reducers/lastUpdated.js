// 9fbef606107a605d69c0edbcd8029e5d 
import GLOBAL_CONSTANTS from '../constants';

const LastUpdatedReducer = (state = {}, action = '') => {
  switch (action.type) {
    case GLOBAL_CONSTANTS.SET_LAST_UPDATED:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default LastUpdatedReducer;
