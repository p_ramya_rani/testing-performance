// 9fbef606107a605d69c0edbcd8029e5d
import GLOBAL_CONSTANTS from '../constants';

const ApiConfigReducer = (
  state = {
    isBrandSwitched: false,
    locationEnabled: false,
  },
  action = ''
) => {
  switch (action.type) {
    case GLOBAL_CONSTANTS.SET_API_CONFIG:
      return {
        ...state,
        ...action.payload,
      };
    case GLOBAL_CONSTANTS.SET_LOCATION_STATUS:
      return {
        ...state,
        locationEnabled: action.payload,
      };
    case GLOBAL_CONSTANTS.SET_DEVICE_ID:
      return {
        ...state,
        deviceID: action.payload,
      };
    case GLOBAL_CONSTANTS.SET_PREVIEW_DATE:
      return {
        ...state,
        ...{
          previewDate: action.payload,
        },
      };
    case GLOBAL_CONSTANTS.SET_IS_BRAND_SWITCHED:
      return {
        ...state,
        ...{
          isBrandSwitched: action.payload,
        },
      };
    case GLOBAL_CONSTANTS.SET_SFCC_AUTH_TOKEN:
      return {
        ...state,
        sfccAuthToken: action.payload,
      };
    default:
      return state;
  }
};

export default ApiConfigReducer;
