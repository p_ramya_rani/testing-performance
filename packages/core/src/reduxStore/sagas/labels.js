// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeEvery, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import isEmpty from 'lodash/isEmpty';
import labelAbstractor from '../../services/abstractors/bootstrap/labels';
import GLOBAL_CONSTANTS, { LABELS } from '../constants';
import { LABEL_REDUCER_KEY } from '../../constants/reducer.constants';
import { setLabelsData } from '../actions';
import { getAPIConfig } from '../../utils';
import { defaultBrand, defaultChannel, defaultCountry } from '../../services/api.constants';

/**
 *
 * @param {string} category
 * @param {string} subCategory
 * @param {boolean} retry - Check is we need to retry the labels API call, in case of exception or empty response from
 * the graph ql api.
 */
function* fetchLabels(category, subCategory, retry) {
  const apiConfig = getAPIConfig();
  const { language } = apiConfig;
  const labelParams = {
    category,
    subCategory,
    brand: (apiConfig && apiConfig.brandIdCMS) || defaultBrand,
    channel: defaultChannel,
    country: (apiConfig && apiConfig.siteIdCMS) || defaultCountry,
    lang: language !== 'en' ? language : '',
  };
  let data = yield call(labelAbstractor.getData, LABELS.labels, labelParams, apiConfig);
  // Retry to get labels data again, if no or empty response from the API.
  if (isEmpty(data) && retry) {
    data = yield call(labelAbstractor.getData, LABELS.labels, labelParams, apiConfig);
  }
  if (!isEmpty(data)) {
    yield put(
      setLabelsData({
        category,
        subCategory,
        data,
      })
    );
  }
}

function* fetchComponentLabel(action) {
  try {
    const { payload: { category = '', subCategory = '' } = {} } = action;
    const labelsSelector = state => {
      const categoryData = state[LABEL_REDUCER_KEY] && state[LABEL_REDUCER_KEY][category];
      if (categoryData && subCategory) {
        return categoryData[subCategory] || false;
      }
      return categoryData || false;
    };
    const isLabelsExist = yield select(labelsSelector);
    if (!isLabelsExist) {
      try {
        yield call(fetchLabels, category, subCategory, true);
      } catch (error) {
        logger.error(
          `Error occurred while processing labels data, retry getting labels : ${error}`
        );
        yield call(fetchLabels, category, subCategory, false);
      }
    }
  } catch (err) {
    logger.error(`Error occurred while processing labels data: ${err}`);
  }
}

function* LabelsSaga() {
  yield takeEvery(GLOBAL_CONSTANTS.LOAD_COMPONENT_LABELS_DATA, fetchComponentLabel);
}

export default LabelsSaga;
