// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable sonarjs/cognitive-complexity */
import { call, put, all, takeEvery, select } from 'redux-saga/effects';
import {
  createLayoutPath,
  pathHasHyphenSlash,
  isClient,
  getAPIConfig,
  isMobileApp,
  getValueFromAsyncStorage,
  setValueInAsyncStorage,
} from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getNavigationData } from '@tcp/core/src/services/abstractors/common/subNavigation';
import layoutAbstractor from '../../services/abstractors/bootstrap/layout';
import GLOBAL_CONSTANTS, { MODULES_CONSTANT } from '../constants';
import { loadLayoutData, loadModulesData, setSubNavigationData } from '../actions';
import {
  defaultBrand,
  defaultChannel,
  defaultCountry,
  MobileChannel,
} from '../../services/api.constants';

const getLayoutParams = (page, apiConfig, isClpPage) => {
  const layoutParams = {
    page,
    brand: (apiConfig && apiConfig.brandIdCMS) || defaultBrand,
    channel: isMobileApp() ? MobileChannel : defaultChannel,
    country: (apiConfig && apiConfig.siteIdCMS) || defaultCountry,
  };
  if (isClpPage || (page && pathHasHyphenSlash(page))) {
    layoutParams.pageName = createLayoutPath(page);
  }
  return layoutParams;
};

// eslint-disable-next-line complexity, max-statements
function* fetchPageLayout(action) {
  const {
    payload: {
      pageName: page,
      layoutName,
      isClpPage,
      isCSREnabledForHome = false,
      resolve = () => null,
      ssrHomeSlotRenderCount = 2,
      apiConfig: config,
    },
  } = action;

  try {
    const apiConfig = config || getAPIConfig();
    const layoutParams = getLayoutParams(page, apiConfig, isClpPage);
    const formattedPage = page && pathHasHyphenSlash(page) ? createLayoutPath(page) : page;
    const { language } = apiConfig;
    let placeHolderIdList = [];
    const slotFetchCount = parseInt(ssrHomeSlotRenderCount, 10);
    const isHomepage = page === 'home';
    let layoutData;
    let modulesData;
    let layoutNode;
    let skipUpdates = false;

    if (isMobileApp() && isHomepage) {
      const state = yield select();
      const { brandId } = apiConfig;
      const { lastUpdated } = state;
      const now = Date.now();
      const prevLastUpdatedStorage = yield call(
        getValueFromAsyncStorage,
        `${brandId}-last-updated`
      );
      const prevLastUpdated = (prevLastUpdatedStorage && JSON.parse(prevLastUpdatedStorage)) || {};
      const { layout: prevTimestamp = 0 } = prevLastUpdated;
      const { layout: currentTimestamp = now } = lastUpdated;
      if (parseInt(currentTimestamp, 10) >= parseInt(prevTimestamp, 10)) {
        layoutData = null;
        modulesData = null;
      } else {
        layoutData = state.Layouts.home;
        modulesData = state.Modules;
        skipUpdates = true;
      }
    }
    if (skipUpdates === false) {
      if (
        (!layoutData && !modulesData) ||
        (isCSREnabledForHome &&
          layoutNode &&
          layoutNode.slots &&
          layoutNode.slots.length === slotFetchCount)
      ) {
        layoutData = yield call(layoutAbstractor.getLayoutData, layoutParams, apiConfig);
        if (isCSREnabledForHome && !isClient()) {
          layoutData = {
            ...layoutData,
            items: [
              {
                layout: {
                  slots: [
                    ...layoutData.items[0].layout.slots.filter(
                      (slot, index) => index < slotFetchCount
                    ),
                  ],
                },
              },
            ],
          };
        }
        const { errorMessage } = layoutData;
        if (!errorMessage) {
          modulesData = yield call(
            layoutAbstractor.getModulesFromLayout,
            layoutData,
            language,
            layoutName || formattedPage,
            apiConfig
          );
          placeHolderIdList = Object.keys(modulesData).filter(
            (module) => modulesData[module].moduleName === MODULES_CONSTANT.placeholder
          );
          if (isMobileApp() && isHomepage) {
            const state = yield select();
            const { lastUpdated } = state;
            const { brandId } = apiConfig;
            const layoutKey = `${brandId}-layout`;
            const modulesKey = `${brandId}-modules`;
            const layoutForStorage = {
              home: layoutData,
            };
            const timestampForStorage = {
              ...lastUpdated,
              layout: `${new Date().getTime()}`,
            };
            setValueInAsyncStorage(layoutKey, JSON.stringify(layoutForStorage));
            setValueInAsyncStorage(modulesKey, JSON.stringify(modulesData));
            setValueInAsyncStorage(`${brandId}-last-updated`, JSON.stringify(timestampForStorage));
          }
        }
      }
      yield put(
        loadLayoutData(
          (layoutData.items[0] && layoutData.items[0].layout) || {},
          layoutName || formattedPage
        )
      );
      yield put(loadModulesData(modulesData));
      if (placeHolderIdList.length > 0) {
        const placeholderResult = yield all(
          placeHolderIdList.map((listItem) =>
            modulesData[listItem].moduleClassName === MODULES_CONSTANT.subNavigation
              ? call(
                  getNavigationData,
                  modulesData[listItem].val,
                  layoutParams.brand,
                  layoutParams.country,
                  layoutParams.channel,
                  apiConfig
                )
              : null
          )
        );
        yield all(
          placeholderResult.map((results) => put(setSubNavigationData(results.val, results.key)))
        );
      }
    }
    resolve();
  } catch (e) {
    logger.error(e);
    resolve();
  }
}

function* LayoutSaga() {
  yield takeEvery(GLOBAL_CONSTANTS.FETCH_PAGE_LAYOUT, fetchPageLayout);
}

export default LayoutSaga;
