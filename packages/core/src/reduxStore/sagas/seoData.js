// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeEvery, select } from 'redux-saga/effects';
import seoDataAbstractor from '../../services/abstractors/seoData';
import catSeoDataAbstractor from '../../services/abstractors/catSeoData';
import GLOBAL_CONSTANTS, { SEO_DATA } from '../constants';
import { SEO_DATA_REDUCER_KEY } from '../../constants/reducer.constants';
import { setSEOData } from '../actions';
import { setCategoryPageSeoData } from '../../components/features/browse/ProductListing/container/ProductListing.actions';
import PRODUCTLISTING_CONSTANTS from '../../components/features/browse/ProductListing/container/ProductListing.constants';
import { defaultChannel } from '../../services/api.constants';
import { removeHyphensFromUrl } from '../../utils/utils';

const ignoreSEODuplicates = ['store', 'help-center', 'content'];

const seoDataSelector = (state, page) => {
  const seoDataKey = page && page.split('/')[1]; // I used Split() here because it is also using in reducer to create seo key
  // If the data key is among those in the exclusion list, the SEO call should be triggered to get the next set of seo data
  if (ignoreSEODuplicates.indexOf(seoDataKey) !== -1) {
    return false;
  }
  const pageData = state[SEO_DATA_REDUCER_KEY] && state[SEO_DATA_REDUCER_KEY][seoDataKey];
  return pageData || false;
};

function* fetchPageSEOData(action) {
  const { payload: { page, apiConfig } = {} } = action;
  const state = yield select();
  const isSEODataExist = yield call(seoDataSelector, state, page);
  if (!isSEODataExist) {
    const { siteIdCMS: country, brandIdCMS: brand, language: lang } = apiConfig;
    const seoDataParams = {
      page,
      brand,
      channel: defaultChannel,
      country,
      lang: lang === 'en' ? '' : lang,
    };

    const data = yield call(seoDataAbstractor.getData, SEO_DATA.seoData, seoDataParams, apiConfig);
    yield put(
      setSEOData({
        page,
        data,
      })
    );
  }
}

function* fetchCategoryPageSeoData(action) {
  const {
    payload: { apiConfig, page },
  } = action;
  const { siteIdCMS: country, brandIdCMS: brand, language } = apiConfig;
  if (page && page.length) {
    const pageNameInStore = removeHyphensFromUrl(page); // this will remove everything before the category page name
    const state = yield select();
    const dataInState = yield call(seoDataSelector, state, page);
    if (!dataInState) {
      const response = yield call(
        catSeoDataAbstractor.getData,
        SEO_DATA.categorySeoData,
        { url: page, brand, country, lang: language === 'en' ? '' : language },
        apiConfig
      );
      yield put(
        setCategoryPageSeoData({
          page: pageNameInStore,
          response,
        })
      );
    }
  }
}

function* SEODataSaga() {
  yield takeEvery(GLOBAL_CONSTANTS.LOAD_PAGE_SEO_DATA, fetchPageSEOData);
  yield takeEvery(PRODUCTLISTING_CONSTANTS.LOAD_CATEGORY_PAGE_SEO_DATA, fetchCategoryPageSeoData);
}

export default SEODataSaga;
