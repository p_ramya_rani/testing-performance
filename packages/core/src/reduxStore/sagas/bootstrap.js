// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable max-statements */
/* eslint-disable complexity */
import { all, call, put, putResolve, takeLatest, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { setPlpProductsDataOnServer } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.actions';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getAPIConfig,
  getBootstrapCachedData,
  setBossBopisEnabled,
  readCookie as readCookieApp,
  getValueFromAsyncStorage,
  setValueInAsyncStorage,
} from '@tcp/core/src/utils';
import { ENV_PREVIEW } from '@tcp/core/src/constants/env.config';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { isBoostrapDone } from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.actions';
import { errorMessages } from '@tcp/core/src/services/api.constants';
import { setBossBopisFlags, setLastUpdated, setToken } from '@tcp/core/src/reduxStore/actions';
import bootstrapAbstractor from '../../services/abstractors/bootstrap';
import NavigationAbstractor from '../../services/abstractors/bootstrap/navigation';
import xappAbstractor from '../../services/abstractors/bootstrap/xappConfig';
import lastUpdatedAbstractor from '../../services/abstractors/bootstrap/lastUpdated';
import tokenAbstractor from '../../services/abstractors/bootstrap/token';
import { API_CONFIG } from '../../services/config';
import {
  loadLayoutData,
  loadLabelsData,
  loadModulesData,
  loadXappConfigData,
  loadXappConfigDataOtherBrand,
  setDeviceInfo,
  setCountry,
  setCurrency,
  getSetTcpSegment,
  setLanguage,
  setBootstrapError,
} from '../actions';
import { loadHeaderData } from '../../components/common/organisms/Header/container/Header.actions';
import { loadFooterData } from '../../components/common/organisms/Footer/container/Footer.actions';
import { loadNavigationData } from '../../components/features/content/Navigation/container/Navigation.actions';
import GLOBAL_CONSTANTS from '../constants';
import {
  LAST_SAVED_BOOTSTRAP_DATA_KEY,
  BOOTSTRAP_DATA_KEY,
} from '../../constants/fallback-data/static.config';
import { isMobileApp } from '../../utils';
import { generateSessionId, readCookie, setCookie } from '../../utils/cookie.util';
import {
  defaultBrand,
  defaultChannel,
  defaultCountry,
  MobileChannel,
} from '../../services/api.constants';

const { sessionCookieKey } = API_CONFIG;

const errorHandler = (errorArgs) => {
  const { errorMsg, payload, component, errorObject = '' } = errorArgs;
  logger.error({
    error: errorMsg,
    errorTags: [`Bootstrap Saga`, component],
    extraData: {
      ...payload,
      errorObject,
      [sessionCookieKey]: !isMobileApp() && generateSessionId(true),
    },
  });
};

// TODO - GLOBAL-LABEL-CHANGE - STEP 1.3 - Uncomment these references
// import GLOBAL_CONSTANTS, { LABELS } from '../constants';
// import { loadLayoutData, loadLabelsData, setLabelsData, loadModulesData, setAPIConfig } from '../actions';

const getQueryParams = (apiConfig) => {
  const { brandIdCMS, siteIdCMS, isAppChannel } = apiConfig || {};
  const isMobileChannel = isMobileApp() || isAppChannel;
  const channel = isMobileChannel ? MobileChannel : defaultChannel;
  return {
    brand: brandIdCMS || defaultBrand,
    channel,
    country: siteIdCMS || defaultCountry,
  };
};

function* fetchLastUpdated(apiConfig) {
  const data = getQueryParams(apiConfig);
  const lastUpdated = yield call(lastUpdatedAbstractor.getData, 'lastUpdated', data, apiConfig);
  yield putResolve(setLastUpdated(lastUpdated));
}

function checkXAppConfigErrorCondition(isMobile, shouldCallApi, xappConfig, xappConfigOtherBrand) {
  return isMobile && shouldCallApi && (!xappConfig || !xappConfigOtherBrand);
}

function* fetchXAppConfigData(apiConfig, isMobile) {
  const { brandIdCMS, brandId } = apiConfig;
  let xappConfig;
  let xappConfigOtherBrand;
  let shouldCallApi = false;
  if (isMobileApp()) {
    const now = Date.now();
    const { lastUpdated } = yield select() || {};
    const prevLastUpdatedStorage = yield call(getValueFromAsyncStorage, `${brandId}-last-updated`);
    const prevLastUpdated = prevLastUpdatedStorage && JSON.parse(prevLastUpdatedStorage);
    const { configurationKeys: prevTimestamp = 0 } = prevLastUpdated || {};
    const { configurationKeys: currentTimestamp = now } = lastUpdated || {};
    if (currentTimestamp > prevTimestamp) {
      shouldCallApi = true;
      [xappConfig, xappConfigOtherBrand] = yield all([
        call(xappAbstractor.getData, GLOBAL_CONSTANTS.XAPP_CONFIG_MODULE, apiConfig),
        call(xappAbstractor.getData, GLOBAL_CONSTANTS.XAPP_CONFIG_MODULE, {
          ...apiConfig,
          ...(brandIdCMS === API_CONFIG.TCP_CONFIG_OPTIONS.brandIdCMS
            ? API_CONFIG.GYM_CONFIG_OPTIONS
            : API_CONFIG.TCP_CONFIG_OPTIONS),
        }),
      ]);
      setValueInAsyncStorage(`${brandId}-last-updated`, JSON.stringify(lastUpdated));
      setValueInAsyncStorage(`${brandId}-xapp`, JSON.stringify(xappConfig));
    }
  } else {
    shouldCallApi = true;
    [xappConfig, xappConfigOtherBrand] = yield all([
      call(xappAbstractor.getData, GLOBAL_CONSTANTS.XAPP_CONFIG_MODULE, apiConfig),
      call(xappAbstractor.getData, GLOBAL_CONSTANTS.XAPP_CONFIG_MODULE, {
        ...apiConfig,
        ...(brandIdCMS === API_CONFIG.TCP_CONFIG_OPTIONS.brandIdCMS
          ? API_CONFIG.GYM_CONFIG_OPTIONS
          : API_CONFIG.TCP_CONFIG_OPTIONS),
      }),
    ]);
  }

  if (checkXAppConfigErrorCondition(isMobile, shouldCallApi, xappConfig, xappConfigOtherBrand)) {
    // eslint-disable-next-line no-throw-literal
    throw {
      errMssg: 'XAppConfig With Error',
    };
  }
  return { xappConfig, xappConfigOtherBrand };
}

function handleStaticResponse({ previousSavedData, staticData, errorMsg, errorObject, component }) {
  if (previousSavedData) {
    return { ...previousSavedData };
  }
  errorHandler({
    errorMsg,
    component,
    payload: { ...staticData },
    errorObject,
  });
  return { ...staticData };
}

// called only from the web...ignore for mobileapp
function* fetchSiteConfig() {
  const apiConfig = getAPIConfig();
  const stateCookie = readCookie('tcpState');
  const sessionState = yield select((state) => state.session);
  const { xappConfig, xappConfigOtherBrand } = yield fetchXAppConfigData(apiConfig);
  yield put(loadXappConfigData(xappConfig));
  yield put(loadXappConfigDataOtherBrand(xappConfigOtherBrand));
  const session = {
    ...sessionState,
    siteDetails: { ...sessionState.siteDetails, ...xappConfig },
    otherBrandSiteDetails: { ...sessionState.otherBrandSiteDetails, ...xappConfigOtherBrand },
  };
  yield put(setBossBopisFlags(setBossBopisEnabled(stateCookie, session)));
}

function* fetchLabels() {
  if (isMobileApp()) {
    const { lastUpdated } = yield select();
    const { brandId } = getAPIConfig();
    const now = Date.now();
    const prevLastUpdatedStorage = yield call(getValueFromAsyncStorage, `${brandId}-last-updated`);
    const prevLastUpdated = prevLastUpdatedStorage && JSON.parse(prevLastUpdatedStorage);
    const { labels: prevTimestamp = 0 } = prevLastUpdated;
    const { labels: currentTimestamp = now } = lastUpdated;
    if (currentTimestamp >= prevTimestamp) {
      const state = yield select();
      const result = yield call(bootstrapAbstractor, '', ['labels'], {}, state, '', null);
      yield put(loadLabelsData(result.labels));
      setValueInAsyncStorage(`${brandId}-last-updated`, JSON.stringify(lastUpdated));
      setValueInAsyncStorage(`${brandId}-labels`, JSON.stringify(result.labels));
    }
  } else {
    const state = yield select();
    const result = yield call(bootstrapAbstractor, '', ['labels'], {}, state, '', null);
    yield put(loadLabelsData(result.labels));
  }
}

function* fetchGlobalHeader() {
  const state = yield select();
  const result = yield call(bootstrapAbstractor, '', ['header'], {}, state, '', null);
  yield put(loadHeaderData(result.header));
  const navigationData = yield call(NavigationAbstractor.getData, {
    module: 'navigation',
    data: {
      depth: 2,
      ignoreCache: true,
    },
  });
  yield put(loadNavigationData(navigationData));
}

function* fetchFooter() {
  const state = yield select();
  const result = yield call(bootstrapAbstractor, '', ['footer'], {}, state, '', null);
  yield put(loadFooterData(result.footer));
}

const isMobileAppCached = (previewEnvId) => previewEnvId !== ENV_PREVIEW && isMobileApp();
/* eslint-disable sonarjs/cognitive-complexity */
// eslint-disable-next-line max-statements
function* bootstrap(params) {
  const {
    payload: {
      name: pageName,
      modules: modulesList,
      apiConfig,
      deviceType,
      siteConfig,
      originalUrl,
    },
  } = params;

  const processedCacheData = {};
  let result = {};

  const { previewEnvId } = apiConfig;
  try {
    if (siteConfig) {
      const { country, currency, language } = apiConfig;

      // putResolve is used to block the other actions till apiConfig is set in state, which is to be used by next bootstrap api calls
      yield putResolve(setDeviceInfo({ deviceType }));
      if (country) {
        yield put(setCountry(country));
      }

      if (currency) {
        yield put(setCurrency({ currency }));
      }

      if (language) {
        yield put(setLanguage(language));
      }

      if (isMobileApp()) {
        // yield put(setLoaderState(true));
      }

      // handle the async storage orchestration within fetchXAppConfigData
      const { xappConfig, xappConfigOtherBrand } = yield fetchXAppConfigData(
        apiConfig,
        isMobileApp()
      );
      yield put(loadXappConfigData(xappConfig));
      yield put(loadXappConfigDataOtherBrand(xappConfigOtherBrand));

      if (isMobileApp()) {
        const { session } = yield select();
        const stateCookie = yield call(readCookieApp, 'tcpState');
        yield put(setBossBopisFlags(setBossBopisEnabled(stateCookie, session)));
      }
    }

    const state = yield select();

    if (isMobileAppCached(previewEnvId)) {
      const { data } = yield call(
        getBootstrapCachedData,
        LAST_SAVED_BOOTSTRAP_DATA_KEY,
        BOOTSTRAP_DATA_KEY
      );
      if (data && data.header) {
        result = { ...data };
      } else {
        try {
          result = yield call(
            bootstrapAbstractor,
            pageName,
            modulesList,
            processedCacheData,
            state,
            originalUrl,
            deviceType
          );
        } catch (error) {
          const { data: previousSavedData, staticData } = yield call(
            getBootstrapCachedData,
            LAST_SAVED_BOOTSTRAP_DATA_KEY,
            BOOTSTRAP_DATA_KEY,
            true
          );
          const { bootstrapFirstLoadError } = errorMessages;
          result = handleStaticResponse({
            previousSavedData,
            staticData,
            errorMsg: bootstrapFirstLoadError,
            component: 'Bootstrap API',
            errorObject: error,
          });
        }
      }
    } else {
      result = yield call(
        bootstrapAbstractor,
        pageName,
        modulesList,
        processedCacheData,
        state,
        originalUrl,
        deviceType
      );
    }
    if (isMobileApp()) {
      const labels = yield fetchLabels();
      yield put(loadLabelsData(labels));
      yield put(setLoaderState(false));
      yield put(isBoostrapDone(false));
    }
    if (result.PLP) {
      const { layout, modules: plpModules, pageName: layoutName, res } = result.PLP;
      yield put(loadLayoutData(layout, layoutName));
      yield put(loadModulesData(plpModules));
      yield put(setPlpProductsDataOnServer(res));
    }
    const findStoreLabel =
      result.labels && result.labels.global && result.labels.global.header
        ? result.labels.global.header.lbl_header_storeDefaultTitle
        : '';

    logger.debug(`Putting labels data:: findStoreLabel: ${findStoreLabel}`);
    if (result && result.labels) {
      yield put(loadLabelsData(result.labels));
    }

    logger.info(
      'Find Store Label-',
      findStoreLabel,
      `,language - ${state.APIConfig.language}`,
      `,siteIdCMS - ${state.APIConfig.siteIdCMS}`
    );
    // TODO - GLOBAL-LABEL-CHANGE - STEP 1.4 - Remove loadLabelsData and uncomment this new code
    //  yield put(setLabelsData({ category:LABELS.global, data:result.labels
    // }));
    if (isMobileApp()) {
      yield put(isBoostrapDone(true));
    }

    logger.debug('Putting header data: ', result.header);
    yield put(loadHeaderData(result.header));

    if (!isMobileApp()) {
      logger.debug('Putting navigation data: ', result.navigation);
      yield put(loadNavigationData(result.navigation));
    }

    logger.debug('Putting footer data: ', result.footer);
    yield put(loadFooterData(result.footer));
  } catch (err) {
    const { bootstrapErrorResponse = null } = err;
    const { bootstrapSagaError } = errorMessages;
    if (!isMobileApp()) {
      // MobileApp will utilise static json and not show the Error Page
      const bootstrapErrorResObj = bootstrapErrorResponse || {
        isErrorInBootstrap: true,
        bootstrapErrorMssg: bootstrapSagaError,
        bootstrapErrorCode: 'Bootstrap-Saga-JS-parsing-Error',
      };
      yield put(setBootstrapError(bootstrapErrorResObj));
    } else {
      yield put(isBoostrapDone(true));
    }
    errorHandler({
      errorMsg: bootstrapSagaError,
      component: `Bootstrap API`,
      payload: { params, ...bootstrapErrorResponse },
    });
  }
}

function* setTcpSegment(tcpSegment) {
  try {
    const tcpSegmentValue = tcpSegment.payload;
    yield put(getSetTcpSegment(tcpSegmentValue));
    const tcpSegmentCookieValue = yield call(readCookie, 'tcpSegment');

    if ((tcpSegmentValue && tcpSegmentCookieValue !== tcpSegmentValue) || !tcpSegmentCookieValue) {
      yield call(setCookie, { key: 'tcpSegment', value: tcpSegmentValue });
    }
  } catch (err) {
    errorHandler({
      errorMsg: err,
      component: `Bootstrap - tcpSegment`,
      payload: tcpSegment,
    });
  }
  return null;
}

function* fetchToken() {
  try {
    const isSNJEnabled = yield select(getIsSNJEnabled);
    if (isSNJEnabled) {
      const { authorization = '' } = yield call(tokenAbstractor.getData);
      if (authorization) {
        yield put(setToken(authorization));
      }
    }
  } catch (err) {
    errorHandler({
      errorMsg: `Error while receiving SFCC token: ${err}`,
      component: `Bootstrap - token`,
    });
  }
  return null;
}

function* BootstrapSaga() {
  yield takeLatest(GLOBAL_CONSTANTS.BOOTSTRAP_API, bootstrap);
  yield takeLatest(GLOBAL_CONSTANTS.SET_TCP_SEGMENT_METHOD_CALL, setTcpSegment);
  yield takeLatest(GLOBAL_CONSTANTS.FETCH_LABELS, fetchLabels);
  yield takeLatest(GLOBAL_CONSTANTS.SITE_CONFIG, fetchSiteConfig);
  yield takeLatest(GLOBAL_CONSTANTS.FETCH_GLOBAL_HEADER, fetchGlobalHeader);
  yield takeLatest(GLOBAL_CONSTANTS.FETCH_FOOTER, fetchFooter);
  yield takeLatest(GLOBAL_CONSTANTS.FETCH_LAST_UPDATED, fetchLastUpdated);
  yield takeLatest(GLOBAL_CONSTANTS.FETCH_SFCC_AUTH_TOKEN, fetchToken);
}

export default BootstrapSaga;
