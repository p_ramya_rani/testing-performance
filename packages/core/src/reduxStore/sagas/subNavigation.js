// 9fbef606107a605d69c0edbcd8029e5d 
import { call, takeLatest, put, all } from 'redux-saga/effects';
import { getAPIConfig, isMobileApp } from '@tcp/core/src/utils';
import {
  defaultChannel,
  defaultBrand,
  defaultCountry,
  MobileChannel,
} from '@tcp/core/src/services/api.constants';
import { getNavigationData } from '@tcp/core/src/services/abstractors/common/subNavigation';
import { setSubNavigationData } from '../actions';
import GLOBAL_CONSTANTS from '../constants';

export function* getSubnavigation({ payload }) {
  const apiConfig = getAPIConfig();
  const isMobileChannel = isMobileApp() || apiConfig.isAppChannel;
  const channel = isMobileChannel ? MobileChannel : defaultChannel;

  try {
    const { brandIdCMS, siteIdCMS } = getAPIConfig();
    const results = yield all(
      payload.map(subNavKey =>
        call(
          getNavigationData,
          subNavKey,
          brandIdCMS || defaultBrand,
          siteIdCMS || defaultCountry,
          channel
        )
      )
    );
    yield all(results.map(res => put(setSubNavigationData(res.val, res.key))));
  } catch (err) {
    yield null;
  }
}

export function* SubNavigationSaga() {
  yield takeLatest(GLOBAL_CONSTANTS.GET_SUB_NAVIGATION_DATA, getSubnavigation);
}

export default SubNavigationSaga;
