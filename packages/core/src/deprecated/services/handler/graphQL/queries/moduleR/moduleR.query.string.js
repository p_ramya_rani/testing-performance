const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  set {
    val
    key
  }
  errorMessage
  composites {
    headerText {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      icon {
        placement
        icon
      }
      link {
        url
        title
        target
      }
    }
    promoBanner {
      link {
        url
        title
        target
      }
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
    }
    divTabs {
      text {
        text
      }
      category{
        val
        key
      }
      singleCTAButton {
        url
        text
        target
        title
      }
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
