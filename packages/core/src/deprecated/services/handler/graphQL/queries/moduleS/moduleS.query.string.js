const query = ({ slot, contentId, lang }) => `
${slot}: moduleById(${
  lang && lang.length > 0 && lang !== 'en'
    ? `id: "${contentId}", lang: "${lang}"`
    : `id: "${contentId}"`
}) {
  contentId
  name
  type
  set {
    val
    key
  }
  errorMessage
  composites {
    headerText {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      icon {
        placement
        icon
      }
      link {
        url
        text
        title
        target
      }
    }
    linkedImage {
      link {
        url
        text
        title
        target
      }
      image {
        url
        url_t
        url_m
        url_img_fr
        url_m_fr
        url_t_fr
        url_img_es
        url_m_es
        url_t_es
        width_d
        width_t
        width_m
        height_d
        height_t
        height_m
        alt
        crop_d
        crop_t
        crop_m
      }
      video{
        url
        url_t
        url_m
        url_fr
        url_m_fr
        url_t_fr
        url_es
        url_m_es
        url_t_es
        height_d
        height_t
        height_m
        title
        autoplay
        controls
        loop
        muted
        inline
      }
    }
    ribbonBanner {
      textItems {
        text
        style
        styledObj
        styledObjWeb
      }
      link {
        url
        text
        title
        target
      }
      ribbonPlacement
      ribbonClass
    }
    singleCTAButton {
      url
      text
      target
      title
    }
  }
}
`;
module.exports = {
  buildQuery: query,
};
