// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import ModuleH from '../views/ModuleH.native';
import { BodyCopy, Heading } from '../../../atoms';
import mock from '../../../../../services/abstractors/common/moduleH/mock';

describe('ModuleH component', () => {
  let ModuleHComponent;
  beforeEach(() => {
    ModuleHComponent = shallow(<ModuleH {...mock.composites} />);
  });

  it('renders correctly', () => {
    expect(ModuleHComponent).toMatchSnapshot();
  });

  it('renders correctly when isHpNewLayoutEnabled is true', () => {
    ModuleHComponent = shallow(
      <ModuleH {...mock.composites} isHpNewLayoutEnabled moduleClassName="mod-app-media-top-10" />
    );
    expect(ModuleHComponent).toMatchSnapshot();
  });

  it('should render Heading', () => {
    expect(ModuleHComponent.find(Heading)).toHaveLength(2);
  });

  it('should render Links', () => {
    expect(ModuleHComponent.find(BodyCopy)).toHaveLength(5);
  });

  it('should return Connect(WithTheme(SnapCarousel)) component value one', () => {
    expect(ModuleHComponent.find('Connect(WithTheme(SnapCarousel))')).toHaveLength(1);
  });
});
