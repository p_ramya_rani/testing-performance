// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { goToPlpTab } from '@tcp/core/src/utils';
import { Button, Anchor, Skeleton } from '../../../atoms';
import { getLocator, getProductUrlForDAM, styleOverrideEngine } from '../../../../../utils';
import {
  LAZYLOAD_HOST_NAME,
  getHeaderBorderRadius,
  getMediaBorderRadius,
} from '../../../../../utils/utils.app';
import moduleRConfig from '../moduleR.config';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';
import colors from '../../../../../../styles/themes/TCP/colors';

import {
  Container,
  HeaderContainer,
  PromoContainer,
  ImageItemWrapper,
  ButtonContainer,
  StyledImage,
  ImageContainer,
  StyledProductTabList,
} from '../styles/ModuleR.style.native';

import PromoBanner from '../../PromoBanner';
import LinkText from '../../LinkText';

const PRODUCT_IMAGE_WIDTH = 103;
const PRODUCT_IMAGE_HEIGHT = 127;

/**
 * @class ModuleR - global reusable component will display featured
 * category module with category links and featured product images
 * This component is plug and play at any given slot in layout by passing required data
 * @param {productTabList} productTabList the list of data for tabs
 * @param {headerText} headerText the list of data for header
 * @param {promoBanner} promoBanner promo banner data
 */
class ModuleR extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectedCategoryId: null,
      currentTabItem: {},
    };
  }

  onProductTabChange = (catId, tabItem) => {
    this.setState({
      selectedCategoryId: catId,
      currentTabItem: tabItem,
    });
  };

  /*
     Return Image Grid with and without Promo Banner. Promo Banner should be included
     in the selectedProductList in order to render in the grid.
  */
  getImageGrid = (selectedProductList) => {
    const { bannerPosition, navigation, icidParam } = this.props;

    return (
      <ImageContainer layout={bannerPosition}>
        {selectedProductList.map((productItem) => {
          // check if productItem is not a PromoBanner component. Else render the promon banner
          if (productItem.uniqueId) {
            const { uniqueId, productItemIndex, product_name: productName } = productItem;

            return (
              <ImageItemWrapper
                key={uniqueId}
                width={PRODUCT_IMAGE_WIDTH}
                isFullMargin={productItemIndex === selectedProductList.length - 1}
              >
                <Anchor
                  onPress={() => {
                    goToPlpTab(navigation);
                    return navigation.navigate('ProductDetail', {
                      title: productName,
                      pdpUrl: uniqueId,
                      selectedColorProductId: uniqueId,
                      reset: true,
                      internalCampaign: icidParam,
                    });
                  }}
                  navigation={navigation}
                  locator={`${getLocator('moduleR_product_image')}${productItemIndex}`}
                >
                  <StyledImage
                    alt={productName}
                    host={LAZYLOAD_HOST_NAME.HOME}
                    url={getProductUrlForDAM(uniqueId)}
                    height={PRODUCT_IMAGE_HEIGHT}
                    width={PRODUCT_IMAGE_WIDTH}
                    imgConfig={moduleRConfig.IMG_DATA.productImgConfig[0]}
                    isProductImage
                    isFastImage
                    resizeMode="stretch"
                    isHomePage
                  />
                </Anchor>
              </ImageItemWrapper>
            );
          }

          return <ImageItemWrapper width={PRODUCT_IMAGE_WIDTH}>{productItem}</ImageItemWrapper>;
        })}
      </ImageContainer>
    );
  };

  getCurrentCTAButton() {
    const { navigation } = this.props;
    const { currentTabItem: { singleCTAButton: currentSingleCTAButton } = {} } = this.state;
    return currentSingleCTAButton ? (
      <ButtonContainer>
        <Button
          width="225px"
          text={currentSingleCTAButton.text}
          url={currentSingleCTAButton.url}
          navigation={navigation}
        />
      </ButtonContainer>
    ) : null;
  }

  getDataStatus = (productTabList, selectedCategoryId) => {
    let dataStatus = true;
    if (productTabList && productTabList.completed) {
      dataStatus = productTabList.completed[selectedCategoryId];
    }
    return dataStatus;
  };

  getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  getHeaderWrapperStyle = (isHpNewLayoutEnabled, headerBorderRadiusOverride) => {
    return isHpNewLayoutEnabled
      ? {
          ...headerBorderRadiusOverride,
          backgroundColor: colors?.WHITE,
        }
      : {};
  };

  getMediaWrapperStyle = (isHpNewLayoutEnabled, mediaBorderRadiusOverride) => {
    return isHpNewLayoutEnabled
      ? {
          ...mediaBorderRadiusOverride,
          backgroundColor: colors?.WHITE,
        }
      : {};
  };

  getContainerStyle = (isHpNewLayoutEnabled) => {
    return isHpNewLayoutEnabled && HP_NEW_LAYOUT.MODULE_BOX_SHADOW;
  };

  render() {
    const {
      navigation,
      divTabs,
      productTabList,
      headerText,
      promoBanner,
      bannerPosition,
      moduleClassName,
      isHpNewLayoutEnabled,
    } = this.props;
    const { selectedCategoryId } = this.state;
    const styleOverrides = styleOverrideEngine(moduleClassName, 'Default');
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];

    let selectedProductList = productTabList[selectedCategoryId] || [];

    const promoComponentContainer = promoBanner && (
      <PromoContainer>
        <PromoBanner
          promoBanner={promoBanner}
          navigation={navigation}
          promoStyle={styleOverrides.promo}
        />
      </PromoContainer>
    );

    /* If the layout is default then we slice 8 images and put the Promo-Header Component
       in the middle of the Array so that we can render it while iterating the images.
       On the case of alt layout we will only show only images so sliced 9 images.
    */
    if (selectedProductList.length) {
      if (promoBanner && bannerPosition === 'center') {
        selectedProductList = selectedProductList.slice(0, 8);
        selectedProductList.splice(4, 0, promoComponentContainer);
      } else {
        selectedProductList = selectedProductList.slice(0, 9);
      }
    }
    const dataStatus = this.getDataStatus(productTabList, selectedCategoryId);

    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const headerWrapperStyle = this.getHeaderWrapperStyle(
      isHpNewLayoutEnabled,
      headerBorderRadiusOverride
    );
    const mediaWrapperStyle = this.getMediaWrapperStyle(
      isHpNewLayoutEnabled,
      mediaBorderRadiusOverride
    );

    return (
      <Container style={this.getContainerStyle(isHpNewLayoutEnabled)}>
        <View style={headerWrapperStyle}>
          <HeaderContainer>
            {headerText && (
              <LinkText
                navigation={navigation}
                headerText={headerText}
                renderComponentInNewLine
                useStyle
                headerStyle={headerStyle}
              />
            )}
          </HeaderContainer>
          {promoBanner && bannerPosition === 'top' ? promoComponentContainer : null}
        </View>
        <View style={mediaWrapperStyle}>
          <StyledProductTabList
            onProductTabChange={this.onProductTabChange}
            tabItems={divTabs}
            navigation={navigation}
            wrappedButtonTabs
          />
          {dataStatus ? (
            <Skeleton
              row={3}
              col={3}
              rowProps={{ justifyContent: 'space-around', marginTop: '10px', marginBottom: '10px' }}
            />
          ) : null}
          {this.getImageGrid(selectedProductList)}
          {this.getCurrentCTAButton()}
        </View>
      </Container>
    );
  }
}

ModuleR.defaultProps = {
  promoBanner: [],
  bannerPosition: 'center',
  moduleClassName: '',
  icidParam: '',
};

ModuleR.propTypes = {
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      textItems: PropTypes.array,
      link: PropTypes.object,
      icon: PropTypes.object,
    })
  ).isRequired,
  divTabs: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.object,
      category: PropTypes.object,
      singleCTAButton: PropTypes.object,
    })
  ).isRequired,
  productTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          uniqueId: PropTypes.string.isRequired,
          imageUrl: PropTypes.array.isRequired,
          seo_token: PropTypes.string,
        })
      )
    )
  ).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      textItems: PropTypes.array,
      link: PropTypes.object,
    })
  ),
  bannerPosition: PropTypes.string,
  moduleClassName: PropTypes.string,
  icidParam: PropTypes.string,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleR;
export { ModuleR as ModuleRVanilla };
