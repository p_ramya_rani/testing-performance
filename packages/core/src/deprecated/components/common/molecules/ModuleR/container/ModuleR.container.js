// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';

import ModuleJ from '../views';

export const mapStateToProps = (state, ownProps) => {
  const { ProductTabList } = state;
  const { icidParam = '' } = ownProps;

  return {
    productTabList: ProductTabList,
    icidParam,
  };
};

export default connect(mapStateToProps)(ModuleJ);
