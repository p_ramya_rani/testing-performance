// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import LinkText from '../../../../../components/common/molecules/LinkText/views/LinkText.jsx';
import Button from '../../../../../components/common/atoms/Button';
import { MODULE_VARIATION } from './ModuleS.config';

const StyledLinkText = styled(LinkText)`
  .link-text {
    span {
      color: ${(props) =>
        props.theme.isGymboree ? props.theme.colors.TEXT.DARK : props.theme.colors.WHITE};
    }
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    &.padding-LR-15 {
      padding: 0 15px;
    }
  }
  &.header-text {
    .medium_text_black {
      margin-bottom: 8px;
      ${(props) =>
        props.theme.isGymboree
          ? ''
          : 'display: inline-block;line-height: 0.94;letter-spacing: normal;'};
    }

    .link-text {
      margin-bottom: ${(props) =>
        props.ribbonPresent
          ? props.theme.spacing.ELEM_SPACING.MED
          : `${props.theme.spacing.ELEM_SPACING.XS} !important`};

      ${(props) =>
        props.textPosition === MODULE_VARIATION.TOP_CENTER
          ? `
        padding: 8px 0px;
        display: flex;
        justify-content: center;
        align-items: center;
        @media ${props.theme.mediaQuery.medium} {
          padding: 12px 16px;
        }
        @media ${props.theme.mediaQuery.smallOnly} {
          margin-bottom: 0 !important;
        }
      `
          : null}
    }

    .small_text_white_medium {
      font-weight: ${(props) => props.theme.typography.fontWeights.medium};
      display: block;
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        display: inline;
      }
    }
  }
`;
/**
 * ColWrapper : This will be used for TCP, Gym Variation 1 and GYM Variation 2 Padding.
 */
const ColWrapper = styled.div`
  position: relative;
  overflow: hidden;

  ${(props) =>
    props.textPosition === MODULE_VARIATION.TOP_CENTER
      ? `
      display: flex;
      flex-direction: column;
    `
      : `
      height: ${props.theme.isGymboree ? '420px' : '356px'};
      @media ${props.theme.mediaQuery.mediumOnly} {
        height: ${props.theme.isGymboree ? '480px' : '356px'};
      }
      @media ${props.theme.mediaQuery.large} {
        height: ${props.theme.isGymboree ? '616px' : '580px'};
      }
    `}

  .header-content {
    z-index: 1;
  }
  .header-text-top {
    position: absolute;
    transform: translate(-50%, 0);
    left: 50%;
    width: 100%;
  }

  .tb-btn {
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translate(-50%, 0);
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      bottom: 10px;
    }
  }
`;
/**
 * ImgContainer : This will be used for TCP and Gym Variation 1.
 */
// ToDo: Removed image height and handle same with image rendition fot tablet
const ImgContainer = styled.div`
  text-align: center;
  img {
    width: 100%;
    max-height: 100%;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: ${(props) => (props.theme.isGymboree ? '480px' : '356px')};
    }
  }

  img,
  .video-js .vjs-tech {
    ${(props) =>
      props.textPosition === MODULE_VARIATION.TOP_CENTER
        ? `
      height: ${props.theme.isGymboree ? '420px' : '356px'};
      @media ${props.theme.mediaQuery.mediumOnly} {
        height: ${props.theme.isGymboree ? '480px' : '356px'};
      }
      @media ${props.theme.mediaQuery.large} {
        height: ${props.theme.isGymboree ? '616px' : '580px'};
      }
      vertical-align: middle;
    `
        : ``}
  }
`;

/**
 * RibbonViewImgContainer : This will be used for TCP and Gym Variation 1.
 */
const RibbonViewImgContainer = styled.div`
  text-align: center;
  margin-bottom: 8px;
  height: 273px;
  img {
    max-height: 100%;
  }
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    height: 393px;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    height: 355px;
  }
`;

const getRightValue = (props) => {
  let right = '';
  if (props.textPosition === MODULE_VARIATION.RIGHT_CENTER) {
    right = props.theme.isGymboree ? '47px' : '25px';
  }
  return right;
};

const getLeftValue = (props) => {
  let left = '';
  if (props.textPosition === MODULE_VARIATION.LEFT_CENTER) {
    left = props.theme.isGymboree ? '47px' : '25px';
  }
  return left;
};

/**
 * Container : This will be used to place content to the right or left for TCP and GYM Variation 1.
 */
const Container = styled.div`
  ${(props) =>
    props.textPosition === MODULE_VARIATION.TOP_CENTER
      ? `
      position: relative;
    `
      : `
    position: absolute;
    top: 50%;
    right: ${props.textPosition === MODULE_VARIATION.RIGHT_CENTER ? '14px' : ''};
    left: ${props.textPosition === MODULE_VARIATION.LEFT_CENTER ? '14px' : ''};
    transform: translate(0%, -50%);
    width: 225px;
    text-align: center;
    @media ${props.theme.mediaQuery.mediumOnly} {
      width: 335px;
      right: ${getRightValue(props)};
      left: ${getLeftValue(props)};
    }
    @media ${props.theme.mediaQuery.large} {
      width: ${props.theme.isGymboree ? '272px' : '259px'};
      right: ${props.textPosition === MODULE_VARIATION.RIGHT_CENTER ? '51px' : ''};
      left: ${props.textPosition === MODULE_VARIATION.LEFT_CENTER ? '51px' : ''};
    }
  `}
`;
const StyledButton = styled(Button)`
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    width: 221px;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    width: 258px;
  }
`;
const ButtonContainer = styled.div`
  padding-top: ${(props) => (props.ribbonPresent ? '0px' : props.theme.spacing.ELEM_SPACING.XS)};
  width: 100%;
  text-align: center;

  ${(props) =>
    props.textPosition === MODULE_VARIATION.TOP_CENTER
      ? `
    @media ${props.theme.mediaQuery.smallOnly} {
      display: flex;
      justify-content: center;
      padding-top: 32px;
    }
  `
      : null}
`;
const StyledRibbonButton = styled(Button)`
  width: 225px;
  @media ${(props) => props.theme.mediaQuery.large} {
    width: 210px;
  }
`;
const StyledTextContainer = styled.div`
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    position: absolute;
    transform: translate(50%, -50%);
    right: 50%;
    width: 70%;
  }
`;

export {
  StyledLinkText as LinkText,
  ColWrapper,
  ImgContainer,
  RibbonViewImgContainer,
  Container,
  ButtonContainer,
  StyledButton as Button,
  StyledRibbonButton as RibbonButton,
  StyledTextContainer,
};

export default {
  LinkText: StyledLinkText,
  ColWrapper,
  ImgContainer,
  RibbonViewImgContainer,
  Container,
  ButtonContainer,
  Button: StyledButton,
  RibbonButton: StyledRibbonButton,
  StyledTextContainer,
};
