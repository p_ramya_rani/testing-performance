// 9fbef606107a605d69c0edbcd8029e5d
export const RIBBON_WIDTH = '250px';
export const RIBBON_HEIGHT = '71px';
export const BUTTON_WIDTH = '225px';
export const RIBBON_COLOR = '#c9182e';
export const TEXT_COLOR_WHITE = 'white';
export const TEXT_COLOR_GYM = '#1a1a1a';
export const MODULE_TCP_HEIGHT = 356;
export const MODULE_GYM_HEIGHT = 420;
export const MODULE_VIDEO_VARIATION_HEIGHT = 370;
export const IMAGE_HEIGHT_RIBBON_VAR = 365;
export const MODULE_VARIATION = {
  TOP_CENTER: 'topCenter',
  LEFT_CENTER: 'leftCenter',
  RIGHT_CENTER: 'rightCenter',
};

export default {
  IMG_DATA_TCP: {
    crops: ['t_mod_S_TCP_img_m', 't_mod_S_GYM_img_t', 't_mod_S_TCP_img_d'],
  },
  VID_DATA_TCP: {
    crops: ['t_mod_S_TCP_vid_m', 't_mod_S_TCP_vid_t', 't_mod_S_TCP_vid_d'],
  },
  IMG_DATA_GYM: {
    crops: ['t_mod_S_GYM_img_m', 't_mod_S_GYM_img_t', 't_mod_S_GYM_img_d'],
  },
  VID_DATA_GYM: {
    crops: ['t_mod_S_GYM_vid_m', 't_mod_S_GYM_vid_t', 't_mod_S_GYM_vid_d'],
  },
  IMG_DATA_GYM_RIBBON: {
    crops: ['t_mod_S_GYM_ribbon_img_m', 't_mod_S_GYM_ribbon_img_t', 't_mod_S_GYM_ribbon_img_d'],
  },
};
