// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import ModuleS from '../view/ModuleS.native';
import { mockV1 } from '../../../../../services/abstractors/common/moduleS/mock';

describe('ModuleSVanilla', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ModuleS {...mockV1.moduleS.composites} />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when isHpNewLayoutEnabled is true', () => {
    wrapper = shallow(<ModuleS {...mockV1.moduleS.composites} isHpNewLayoutEnabled />);
    expect(wrapper).toMatchSnapshot();
  });
});
