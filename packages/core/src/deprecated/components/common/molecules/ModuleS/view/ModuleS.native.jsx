// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable react-native/no-color-literals */
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import Button from '../../../../../../components/common/atoms/Button/views';
import Anchor from '../../../../../../components/common/atoms/Anchor/views';
import Ribbon from '../../../../../../components/common/molecules/Ribbon';
import LinkText from '../../../../../../components/common/molecules/LinkText';
import {
  isGymboree,
  getScreenWidth,
  LAZYLOAD_HOST_NAME,
  getLocator,
  getMediaBorderRadius,
  getHeaderBorderRadius,
} from '../../../../../utils/index.native';
import HP_NEW_LAYOUT from '../../../../../../constants/hpNewLayout.constants';
import colors from '../../../../../../styles/themes/TCP/colors';
import {
  ImageContainer,
  ButtonContainer,
  ModuleContainer,
  TCPOverlayTextContainer,
  GymboreeOverlayTextContainer,
  TopVariationButtonContainer,
  StyledImage,
  TCPTopTextContainer,
  GymTopTextContainer,
  HeaderViewWrapper,
  TopVariation,
} from '../ModuleS.style.native';
import config, {
  BUTTON_WIDTH,
  RIBBON_HEIGHT,
  RIBBON_WIDTH,
  MODULE_TCP_HEIGHT,
  MODULE_GYM_HEIGHT,
  MODULE_VARIATION,
  MODULE_VIDEO_VARIATION_HEIGHT,
  IMAGE_HEIGHT_RIBBON_VAR,
} from '../ModuleS.config';
import { styleOverrideEngine } from '../../../../../../utils';

const styles = {
  leftCenter: {
    left: 14,
  },
  rightCenter: {
    right: 14,
  },
};

/**
 * This function returns named transformations from config
 */
const getImageConfig = (hasRibbon) => {
  if (hasRibbon) {
    return config.IMG_DATA_GYM_RIBBON.crops[0];
  }

  return isGymboree() ? config.IMG_DATA_GYM.crops[0] : config.IMG_DATA_TCP.crops[0];
};

/**
 * This method returns Image width for the module
 * @param {*} hasRibbon
 */
const getImageWidth = (isHpNewLayoutEnabled) => {
  return isHpNewLayoutEnabled
    ? getScreenWidth() - 2 * HP_NEW_LAYOUT.BODY_PADDING
    : getScreenWidth();
};

/**
 * Module height and width.
 * Height is fixed for mobile : TCP & Gymb
 * Width can vary as per device width.
 */
const getImageHeight = (hasRibbon, hasVideo = false) => {
  if (hasRibbon) {
    return IMAGE_HEIGHT_RIBBON_VAR;
  }

  if (hasVideo) {
    return MODULE_VIDEO_VARIATION_HEIGHT;
  }

  return isGymboree() ? MODULE_GYM_HEIGHT : MODULE_TCP_HEIGHT;
};

const getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled && styleOverrides
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};

const getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled && styleOverrides
    ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
    : {};
};

/**
 * To Render the Dam Image or Video Component
 */
const renderDamImage = (
  link,
  imgData,
  videoData,
  navigation,
  hasRibbon,
  isHpNewLayoutEnabled,
  mediaBorderRadiusOverride
) => {
  const damImageComp = (
    <StyledImage
      width={getImageWidth(isHpNewLayoutEnabled)}
      height={getImageHeight(hasRibbon)}
      imgData={imgData}
      host={LAZYLOAD_HOST_NAME.HOME}
      isHomePage
      resizeMode="stretch"
      link={link}
      navigation={navigation}
      isFastImage
      overrideStyle={isHpNewLayoutEnabled && mediaBorderRadiusOverride}
      isBackground={false}
    />
  );
  const damVideoComp = (
    <StyledImage
      width={getImageWidth(isHpNewLayoutEnabled)}
      height={getImageHeight(hasRibbon)}
      host={LAZYLOAD_HOST_NAME.HOME}
      videoData={videoData}
      imgConfig={imgData && (imgData.crop_m || getImageConfig(hasRibbon))}
      isHomePage
      link={link}
      navigation={navigation}
      overrideStyle={isHpNewLayoutEnabled && mediaBorderRadiusOverride}
      isBackground={false}
    />
  );
  if (imgData && Object.keys(imgData).length > 0) {
    return (
      <Anchor url={link.url} navigation={navigation}>
        {damImageComp}
      </Anchor>
    );
  }
  return videoData && Object.keys(videoData).length > 0 ? (
    <React.Fragment>{damVideoComp}</React.Fragment>
  ) : null;
};

/**
 * This method return image wrapped inside an anchor tag
 * @param {*} props
 * @param {*} hasRibbon
 */
const getLinkedImage = (props, hasRibbon, styleOverrides) => {
  const {
    navigation,
    linkedImage: [{ image, link, video }],
    isHpNewLayoutEnabled,
  } = props;
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewLayoutEnabled,
    styleOverrides
  );

  let hasVideo = false;
  let videoData = {};
  if (video && video.url) {
    hasVideo = true;
    videoData = {
      ...video,
      videoWidth: getImageWidth(),
      videoHeight: getImageHeight(hasRibbon, hasVideo),
    };
  }
  return (
    <React.Fragment>
      {renderDamImage(
        link,
        image,
        videoData,
        navigation,
        hasRibbon,
        isHpNewLayoutEnabled,
        mediaBorderRadiusOverride
      )}
    </React.Fragment>
  );
};

const ButtonView = (props) => {
  const { singleCTAButton, navigation, textPosition } = props;
  const isTopVariation = textPosition === MODULE_VARIATION.TOP_CENTER;

  return singleCTAButton ? (
    <ButtonContainer isTopVariation={isTopVariation}>
      <Button
        width={BUTTON_WIDTH}
        accessibilityLabel={singleCTAButton.title}
        text={singleCTAButton.text}
        testID={getLocator('moduleD_button')}
        url={singleCTAButton.url}
        navigation={navigation}
        fill="WHITE"
      />
    </ButtonContainer>
  ) : null;
};

const HeaderView = (props) => {
  const { navigation, headerText, styleOverrides } = props;
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];

  return (
    headerText && (
      <LinkText
        className="header-title"
        type="heading"
        fontFamily="primary"
        fontWeight="black"
        navigation={navigation}
        headerText={headerText}
        locator="moduleS_header_text"
        textAlign="center"
        renderComponentInNewLine
        useStyle
        headerStyle={headerStyle}
      />
    )
  );
};

const isVideoVariation = (props) => {
  const {
    linkedImage: [{ video }],
  } = props;
  return video && video.url;
};

const getContainerHeight = (hasVideo = false) => {
  let height = isGymboree() ? MODULE_GYM_HEIGHT : MODULE_TCP_HEIGHT;

  if (hasVideo) {
    height = MODULE_VIDEO_VARIATION_HEIGHT;
  }

  return height;
};

const RibbonBannerVariation = (props) => {
  const {
    styleOverrides,
    ribbonBanner,
    hasVideo,
    isHpNewLayoutEnabled,
    mediaBorderRadiusOverride,
  } = props;
  const hasRibbon = ribbonBanner;
  const promoStyle = styleOverrides && styleOverrides['ribnText-color'];
  const ribbonBgColor = styleOverrides && styleOverrides.ribbon && styleOverrides.ribbon.tintColor;
  const margin =
    styleOverrides && styleOverrides.marginBottom && styleOverrides.marginBottom['margin-bottom'];
  const OverlayTopTextContainer = isGymboree() ? GymTopTextContainer : TCPTopTextContainer;
  const containerWrapperStyle = isHpNewLayoutEnabled
    ? {
        ...HP_NEW_LAYOUT.MODULE_BOX_SHADOW,
        backgroundColor: colors?.WHITE,
        ...mediaBorderRadiusOverride,
      }
    : {};
  return (
    <ModuleContainer
      containerHeight={getContainerHeight(hasVideo)}
      marginBottom={margin}
      style={containerWrapperStyle}
    >
      <OverlayTopTextContainer>
        <HeaderView {...props} styleOverrides={styleOverrides} />
      </OverlayTopTextContainer>
      <ImageContainer>{getLinkedImage(props, hasRibbon, styleOverrides)}</ImageContainer>
      <Ribbon
        {...props}
        width={RIBBON_WIDTH}
        height={RIBBON_HEIGHT}
        color={ribbonBgColor}
        promoStyle={promoStyle}
      />
      <TopVariationButtonContainer>
        <ButtonView {...props} hasRibbon />
      </TopVariationButtonContainer>
    </ModuleContainer>
  );
};

/**
 * helper classes for style override
 *
 * mod-header-fs-50 - override title size
 * mod-subHeader-fs-50 - override sub title size
 * mod-ribbon-blue-50 - override ribbon bgd color
 * mod-ribnText-color-#373 - overide ribbon text color
 * mod-title-#373 - override title color
 * mod-app-marginBottom-100 - add margin below the component
 *
 * For testing pass below string as 1st arg to styleOverrideEngine
 * 'mod-header-fs-50 mod-subHeader-fs-30 mod-ribbon-blue-50 mod-ribnText-color-#373 mod-title-#373 mod-marginBottom-10'
 */
const ModuleS = (props) => {
  const { ribbonBanner, textPosition, moduleClassName, isHpNewLayoutEnabled } = props;

  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleS');
  const marginBottom = '';
  const isTopVariation = textPosition === MODULE_VARIATION.TOP_CENTER;
  const hasVideo = isVideoVariation(props);
  const headerBorderRadiusOverride = getHeaderBorderRadiusOverride(
    isHpNewLayoutEnabled,
    styleOverrides
  );
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewLayoutEnabled,
    styleOverrides
  );
  const headerAndCtaWrapperStyle = isHpNewLayoutEnabled
    ? { ...headerBorderRadiusOverride, backgroundColor: colors?.WHITE, paddingBottom: 16 }
    : {};
  const mediaWrapperStyle = isHpNewLayoutEnabled ? { backgroundColor: colors?.WHITE } : {};
  if (ribbonBanner) {
    return (
      <RibbonBannerVariation
        {...props}
        styleOverrides={styleOverrides}
        hasVideo={hasVideo}
        mediaBorderRadiusOverride={mediaBorderRadiusOverride}
      />
    );
  }

  const OverlayTextContainer = isGymboree()
    ? GymboreeOverlayTextContainer
    : TCPOverlayTextContainer;
  return (
    <View style={isHpNewLayoutEnabled && HP_NEW_LAYOUT.MODULE_BOX_SHADOW}>
      <ModuleContainer
        containerHeight={getContainerHeight(hasVideo)}
        marginBottom={marginBottom}
        style={{ ...mediaWrapperStyle, ...mediaBorderRadiusOverride }}
      >
        {getLinkedImage(props, null, styleOverrides)}
        <OverlayTextContainer style={styles[`${textPosition}`]} textPosition={textPosition}>
          {!isTopVariation && (
            <>
              <HeaderView {...props} styleOverrides={styleOverrides} />
              <ButtonView {...props} />
            </>
          )}
        </OverlayTextContainer>
      </ModuleContainer>
      {isTopVariation && (
        <View style={headerAndCtaWrapperStyle}>
          <TopVariation>
            <HeaderViewWrapper style={styleOverrides['header-bg']}>
              <HeaderView {...props} styleOverrides={styleOverrides} />
            </HeaderViewWrapper>
          </TopVariation>
          <ButtonView {...props} />
        </View>
      )}
    </View>
  );
};

const ModulePropTypes = {
  headerText: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  ribbonBanner: PropTypes.shape({}).isRequired,
  singleCTAButton: PropTypes.shape({}).isRequired,
};

RibbonBannerVariation.propTypes = {
  ...ModulePropTypes,
  styleOverrides: PropTypes.shape({}),
};

RibbonBannerVariation.defaultProps = {
  styleOverrides: {},
};

ModuleS.propTypes = {
  headerText: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  ribbonBanner: PropTypes.shape({}).isRequired,
  singleCTAButton: PropTypes.shape({}).isRequired,
  moduleClassName: PropTypes.string,
  textPosition: PropTypes.string,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

ModuleS.defaultProps = {
  textPosition: '',
  moduleClassName: '',
};

ButtonView.propTypes = {
  ...ModulePropTypes,
};
ButtonView.defaultProps = {
  hasRibbon: false,
};

HeaderView.propTypes = ModulePropTypes;

getLinkedImage.propTypes = {
  ...ModulePropTypes,
  linkedImage: PropTypes.arrayOf(
    PropTypes.oneOfType(
      PropTypes.shape({
        image: PropTypes.shape({
          url: PropTypes.string,
          alt: PropTypes.string,
          title: PropTypes.string,
          crop_m: PropTypes.string,
        }),
        link: PropTypes.shape({
          url: PropTypes.string,
          text: PropTypes.string,
          title: PropTypes.string,
          target: PropTypes.string,
        }),
        video: PropTypes.shape({
          url: PropTypes.string,
        }),
      })
    )
  ).isRequired,
};

export default ModuleS;
