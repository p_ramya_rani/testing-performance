// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, DamImage } from '../../../atoms';
import {
  LinkText,
  ColWrapper,
  ImgContainer,
  Container,
  ButtonContainer,
  Button,
  RibbonButton,
  StyledTextContainer,
} from '../ModuleS.style';
import { getLocator, isGymboree, styleOverrideEngine } from '../../../../../../utils';
import imageCropConfigs, { MODULE_VARIATION } from '../ModuleS.config';
import RibbonComponent from '../../../../../../components/common/molecules/Ribbon';

/**
 * This function returns column size for grid on the basis of moduleWidth param
 * @param {*} moduleWidth
 */
const getColSize = () => {
  return {
    small: 6,
    medium: 8,
    large: 12,
  };
};
/**
 * This function return the image config for each theme.
 * @param {*} promoWrapper
 */
const { IMG_DATA_TCP, IMG_DATA_GYM, VID_DATA_GYM, VID_DATA_TCP } = imageCropConfigs;
const getImageConfig = () => {
  let imageConfig = IMG_DATA_TCP;
  if (isGymboree()) {
    imageConfig = IMG_DATA_GYM;
  }
  return imageConfig.crops;
};

const getVideoConfig = () => {
  let videoConfig = VID_DATA_TCP;
  if (isGymboree()) {
    videoConfig = VID_DATA_GYM;
  }
  return videoConfig.crops;
};
/**
 * This function return the  LinkText component to render the text.
 * @param {*} props
 */
const TextView = (props) => {
  const {
    headerText,
    ribbonBanner,
    ribbonPresent = false,
    styleOverrides,
    isTopVariation,
    textPosition,
  } = props;
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
  const addPaddingClass = !ribbonBanner && isGymboree();
  return (
    headerText && (
      <StyledTextContainer style={styleOverrides['header-bg']}>
        <LinkText
          type="heading"
          component="div"
          ribbonPresent={ribbonPresent}
          className={`${isTopVariation && !ribbonPresent ? 'header-text-top' : 'header-text'} ${
            addPaddingClass ? 'padding-LR-15' : ''
          } header-content`}
          headerText={headerText}
          data-locator={getLocator('moduleS_header_text')}
          headerStyle={headerStyle}
          textPosition={textPosition}
        />
      </StyledTextContainer>
    )
  );
};

const isRibbonView = (props) => {
  const { ribbonBanner } = props;
  return ribbonBanner && ribbonBanner.length > 0;
};

/**
 * This function return the  LinkText DamImage to render the image.
 * @param {*} props
 */
const ImgView = (props) => {
  const { linkedImage, isHomePage } = props;
  const [imageDetails] = linkedImage;
  const imageConfig = imageDetails.video ? getVideoConfig() : getImageConfig();
  return (
    imageDetails && (
      <DamImage
        imgData={imageDetails.image}
        link={imageDetails.link}
        data-locator={getLocator('moduleS_large_img')}
        imgConfigs={imageConfig}
        videoData={imageDetails.video}
        isHomePage={isHomePage}
        isOptimizedVideo
        lazyLoad
        isModule
      />
    )
  );
};
/**
 * This function returns the view to be displayed when ribbon is present.
 * @param {*} props
 */
const RibbonView = (props) => {
  const { singleCTAButton, ribbonBanner, styleOverrides, isTopVariation } = props;

  return (
    <ColWrapper>
      <TextView {...props} ribbonPresent={ribbonBanner} isTopVariation={isTopVariation} />
      <ImgContainer>
        <ImgView {...props} />
      </ImgContainer>
      <div className="tb-btn-section">
        <RibbonComponent
          width={240}
          height={71}
          ribbonBanner={ribbonBanner}
          locator="moduleS_promo_badge"
          styleOverrides={styleOverrides}
        />
        {singleCTAButton && (
          <ButtonContainer ribbonPresent>
            <RibbonButton
              cta={singleCTAButton}
              buttonVariation="fixed-width"
              className="tb-btn"
              data-locator={getLocator('moduleS_cta_btn')}
            >
              {singleCTAButton.text}
            </RibbonButton>
          </ButtonContainer>
        )}
      </div>
    </ColWrapper>
  );
};
/**
 * This function returns the view to be displayed when ribbon is not present for TCP and Gymboree Themes.
 * @param {*} props
 */
const WithOutRibbonView = (props) => {
  const { singleCTAButton, textPosition } = props;
  return (
    <ColWrapper textPosition={textPosition}>
      <ImgContainer textPosition={textPosition}>
        <ImgView {...props} />
      </ImgContainer>
      <Container className="tcp-variation-content" textPosition={textPosition}>
        <TextView {...props} />
        {singleCTAButton && (
          <ButtonContainer textPosition={textPosition}>
            <Button
              cta={singleCTAButton}
              buttonVariation="fixed-width"
              className="rl-button"
              data-locator={getLocator('moduleS_cta_btn')}
            >
              {singleCTAButton.text}
            </Button>
          </ButtonContainer>
        )}
      </Container>
    </ColWrapper>
  );
};

/**
 * This function returns Module to display the category banner
 * @param {*} props
 */

const ModuleS = (props) => {
  const { className, moduleClassName, textPosition } = props;
  const colSize = getColSize();
  const isTopVariation = textPosition === MODULE_VARIATION.TOP_CENTER;
  const ribbonPresent = isRibbonView(props);
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleS');

  return (
    <Row fullBleed className={`${className} ${moduleClassName} moduleS`}>
      <Col colSize={colSize}>
        {!ribbonPresent && <WithOutRibbonView {...props} styleOverrides={styleOverrides} />}
        {ribbonPresent && !isTopVariation && (
          <RibbonView {...props} styleOverrides={styleOverrides} isTopVariation={isTopVariation} />
        )}
      </Col>
    </Row>
  );
};
const ModulePropTypes = {
  headerText: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  ribbonBanner: PropTypes.shape({}).isRequired,
  singleCTAButton: PropTypes.shape({}).isRequired,
};
WithOutRibbonView.propTypes = { ...ModulePropTypes };
RibbonView.propTypes = { ...ModulePropTypes };
ModuleS.propTypes = {
  className: PropTypes.string.isRequired,
  ribbonBanner: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.shape({}))),
  moduleWidth: PropTypes.string,
  moduleClassName: PropTypes.string,
  textPosition: PropTypes.string,
};

ModuleS.defaultProps = {
  moduleWidth: 'half',
  ribbonBanner: null,
  moduleClassName: '',
  textPosition: '',
};

export default ModuleS;
