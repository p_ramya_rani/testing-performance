// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { DamImage } from '../../atoms';

export const RibbonContainer = styled.View`
  ${(props) => (props.position === 'right' ? `align-items: flex-end;` : '')};
`;

export const ImageContainer = styled.View`
  padding: 0 0 8px 0;
`;

export const ButtonContainer = styled.View`
  align-items: center;
  display: flex;
  ${(props) =>
    props.isTopVariation
      ? `padding-top: ${props.theme.spacing.LAYOUT_SPACING.MED};`
      : `padding-top: ${props.theme.spacing.ELEM_SPACING.SM};`}
`;

export const ModuleContainer = styled.View`
  position: relative;
  height: ${(props) => props.containerHeight}px;
  margin-bottom: ${(props) => (props.marginBottom ? props.marginBottom : 0)}px;
`;

export const TCPOverlayTextContainer = styled.View`
  position: absolute;
  top: 91;
  width: 225px;
`;

export const GymboreeOverlayTextContainer = styled.View`
  position: absolute;
  top: 154;
  width: 225px;
`;

export const TopVariationButtonContainer = styled.View`
  margin: 0 auto;
  position: absolute;
  left: 0;
  bottom: 10;
  right: 0;
`;

export const StyledImage = styled(DamImage)`
  height: ${(props) => props.height}px;
`;

export const TCPTopTextContainer = styled.View`
  margin: 0 auto;
  position: absolute;
  left: 0;
  top: 10;
  right: 0;
  z-index: 10;
`;
export const GymTopTextContainer = styled.View`
  margin: 0 auto;
  position: absolute;
  left: 0;
  top: 10;
  right: 0;
  z-index: 10;
`;

export const HeaderViewWrapper = styled.View`
  position: absolute;
  bottom: -25;
  width: 80%;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const TopVariation = styled.View`
  display: flex;
  align-items: center;
`;
