// 9fbef606107a605d69c0edbcd8029e5d
export const ctaTypes = {
  divImageCTACarousel: 'imageCTAList',
  stackedCTAButtonsExpandable: 'stackedCTAList',
  CTAButtonCarouselExpandable: 'scrollCTAList',
  stackedCTAButtons: 'stackedCTAList',
  CTAButtonCarousel: 'scrollCTAList',
  ctaAddNoButtons: 'NoButton',
};

export const ctaTypeProps = {
  stackedCTAButtonsExpandable: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: false,
        large: true,
      },
    },
  },
  CTAButtonCarouselExpandable: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: false,
        large: true,
      },
    },
  },
  ctaAddNoButtons: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: false,
        large: false,
      },
    },
  },
};

export default {
  crops: ['t_mod_img_txt_m', 't_mod_img_txt_t', 't_mod_img_txt_d'],
};
