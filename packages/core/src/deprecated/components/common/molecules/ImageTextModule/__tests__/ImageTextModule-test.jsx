// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ImageTextModuleVanilla as ImageTextModule } from '../views/ImageTextModule';
import mock from '../../../../../services/abstractors/common/ImageTextModule/mock';

describe('ImageTextModule Componenet', () => {
  let wrapper;

  it('should match snapshot', () => {
    wrapper = shallow(<ImageTextModule {...mock.composites} set={mock.set} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot without button list', () => {
    const composites = { ...mock.composites, ctaItems: [] };
    wrapper = shallow(<ImageTextModule {...composites} set={mock.set} />);
    expect(wrapper).toMatchSnapshot();
  });
});
