// 9fbef606107a605d69c0edbcd8029e5d
import format from 'date-fns/format';
import { en, es, fr } from 'date-fns/locale';

const locales = {
  en,
  es,
  fr,
};

/**
 * @function formatDateWithLocale converts en date/date string to language passed
 * @param date It can be a valid date string or date object.
 * @param dateFormat it can be a valid date Format For more information
 *  https://date-fns.org/v2.8.0/docs/format
 * @returns formatted date
 */
export const formatDate = (date, dateFormat) => {
  let dateString = date || new Date();
  if (typeof dateString === 'string') {
    dateString = new Date(dateString.replace(/\s+/g, 'T'));
  }

  return format(dateString, dateFormat);
};

/**
 * @function formatDateWithLocale converts en date/date string to language passed
 * @param date It can be a valid date string or date object.
 * @param dateFormat it can be a valid date Format For more information
 *  https://date-fns.org/v2.8.0/docs/format
 * @param language en, es, fr
 * @returns formatted date with locale translated
 */
export const formatDateWithLocale = (date, dateFormat, language) => {
  const locale = locales[language || 'en'];
  let dateString = date || new Date();

  if (typeof dateString === 'string') {
    dateString = new Date(dateString.replace(/\s+/g, 'T'));
  }
  return format(dateString, dateFormat, { locale });
};
