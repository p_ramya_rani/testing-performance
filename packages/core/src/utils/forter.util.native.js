// 9fbef606107a605d69c0edbcd8029e5d 
import {
  forterSDK,
  ForterActionType as ActionType,
  ForterNavigationType as NavigationType,
} from 'react-native-forter';
import env from 'react-native-config';
import logger from '@tcp/core/src/utils/loggerInstance';
import { trackError } from '@tcp/core/src/utils/errorReporter.util';
import { isIOS, isDevelopment } from '@tcp/core/src/utils';
import { setDeviceIDValue } from '@tcp/core/src/reduxStore/actions';
import { getIsForterEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';

let reduxStore;
let forterDeviceID;

const logForterError = (e, tag) => {
  const errorTag = tag || `Forter initialization error`;
  logger.info(errorTag, e);
  trackError({ error: e, errorTags: [errorTag] });
};

export const ForterActionType = ActionType;
export const ForterNavigationType = NavigationType;

const initializeForter = store => {
  reduxStore = store;
  const forterEnabled = store && getIsForterEnabled(store.getState());
  if (!forterEnabled) {
    return;
  }
  try {
    if (isDevelopment()) {
      forterSDK.setDevLogsEnabled();
    }
    forterSDK.getDeviceUniqueID(deviceID => {
      forterDeviceID = deviceID;
      store.dispatch(setDeviceIDValue(deviceID));
      forterSDK.init(
        env.RWD_APP_FORTER_SITE_ID,
        deviceID,
        successResult => {
          logger.info('Forter Initialized', successResult);
        },
        logForterError
      );
    });
  } catch (e) {
    logForterError(e);
  }
};

export const getForterDeviceID = () => {
  return forterDeviceID;
};

export const trackForterNavigation = (
  name,
  type,
  { pageId, pageCategory, otherInfo } = {},
  iOSOnly = false
) => {
  const forterEnabled = reduxStore && getIsForterEnabled(reduxStore.getState());
  if (!forterEnabled) {
    return;
  }
  try {
    if (!iOSOnly || isIOS()) {
      forterSDK.trackNavigation(name, type, pageId, pageCategory, otherInfo);
    }
  } catch (e) {
    logForterError(e, `Forter Navigation error`);
  }
};

export const trackForterAction = (type, data, iOSOnly) => {
  const forterEnabled = reduxStore && getIsForterEnabled(reduxStore.getState());
  if (!forterEnabled) {
    return;
  }
  try {
    if (!iOSOnly || isIOS()) {
      if (!data) {
        forterSDK.trackAction(type);
      } else if (typeof data === 'string') {
        forterSDK.trackActionWithMessage(type, data);
      } else {
        forterSDK.trackActionWithJSON(type, data);
      }
    }
  } catch (e) {
    logForterError(e, `Forter Action error`);
  }
};

export const trackForterCurrentLocation = (longitude, latitude) => {
  const forterEnabled = reduxStore && getIsForterEnabled(reduxStore.getState());
  if (!forterEnabled) {
    return;
  }
  try {
    forterSDK.trackCurrentLocation(longitude, latitude);
  } catch (e) {
    logForterError(e, `Forter trackCurrentLocation error`);
  }
};

export default initializeForter;
