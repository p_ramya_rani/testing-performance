// 9fbef606107a605d69c0edbcd8029e5d 
const raygun = require('raygun');
// Not to use logger utility as it itself uses error-reporter and creates cyclic dependency. Use console instead.
const httpContext = require('express-http-context');
const getQMRefID = require('./getQMSessionID');
const RAYGUN_TIMEOUT = require('./../constants/raygun.constants');
/**
 * Creates the error logger for the application
 * @returns {object} configuration to log error scenarios
 */
let raygunInstance = null;

const isServer = () => {
  return typeof window === 'undefined';
};

const setRaygunInstance = instance => {
  if (isServer()) {
    global.raygunInstance = instance;
    return instance;
  }
  raygunInstance = instance;
  return instance;
};

const getRaygunInstance = () => {
  if (isServer()) {
    return global.raygunInstance;
  }
  return raygunInstance;
};

const getSensitiveDataArray = () => {
  return ['paymentInstruction', 'logonPassword1'];
};

const initServerErrorReporter = (envId, raygunApiKey) => {
  const { SERVER: SERVER_RAYGUN_TIMEOUT } = RAYGUN_TIMEOUT;
  const raygunInst = new raygun.Client().init({
    apiKey: raygunApiKey,
    reportColumnNumbers: true,
    timeout: SERVER_RAYGUN_TIMEOUT,
  });
  raygunInst.setVersion(envId);
  raygunInst.setTags([envId]);
  setRaygunInstance(raygunInst);
  const message = `Initializing  ErrorReporter Raygun on Node Server: release = ${envId}`;
  console.log(message);
};

const initWebClientErrorReporter = (envId, raygunApiKey, channelId, rg4js) => {
  const { CLIENT: CLIENT_RAYGUN_TIMEOUT } = RAYGUN_TIMEOUT;

  if (rg4js && typeof rg4js === 'function') {
    rg4js('enableCrashReporting', true);
    rg4js('apiKey', raygunApiKey);
    rg4js('setVersion', envId);
    rg4js('withCustomData', { channel: channelId });
    rg4js('withTags', [envId]);
    rg4js('timeout', CLIENT_RAYGUN_TIMEOUT);
    /**
      * Commenting out this code since it logs the contents of sensitive information
        like billing and account details.
      * rg4js('setAutoBreadcrumbsXHRIgnoredHosts', ['hostname']) wont work since it will
        ignore the host and not a specific API

      rg4js('logContentsOfXhrCalls', true);
    */
    rg4js('filterSensitiveData', getSensitiveDataArray());
    rg4js('setFilterScope', 'all');
    if (typeof window !== 'undefined' && window.onBeforeSendRaygun) {
      rg4js('onBeforeSend', window.onBeforeSendRaygun);
    }
    setRaygunInstance(rg4js);
    const message = `Initializing  ErrorReporter Raygun on Web Client: release = ${envId} - channelId = ${channelId}`;
    console.log(message);
  }
};

const initErrorReporter = config => {
  const { envId, raygunApiKey, channelId, isServer: isNodeServer, isDevelopment, rg4js } = config;
  if (isDevelopment || !raygunApiKey) {
    return null;
  }
  if (getRaygunInstance()) {
    return getRaygunInstance();
  }
  if (isNodeServer) {
    initServerErrorReporter(envId, raygunApiKey);
  } else {
    initWebClientErrorReporter(envId, raygunApiKey, channelId, rg4js);
  }
  return getRaygunInstance();
};

const trackError = errorArgs => {
  if (!getRaygunInstance()) {
    return;
  }

  const { error } = errorArgs;
  let { errorTags, extraData } = errorArgs;

  errorTags = errorTags || [];

  extraData = extraData || {};
  const errorObject = typeof error === 'string' ? new Error(error) : error;
  if (isServer()) {
    getRaygunInstance().send(errorObject, { ...extraData }, () => {}, {}, [
      'node-server-error',
      ...errorTags,
      httpContext.get('RequestId'),
    ]);
  } else {
    getRaygunInstance()('send', {
      error: errorObject,
      tags: [...errorTags, getQMRefID()],
      customData: { ...extraData },
    });
  }
};

const getExpressMiddleware = () => {
  if (getRaygunInstance()) {
    return getRaygunInstance().expressHandler;
  }
  return null;
};

module.exports = {
  initErrorReporter,
  trackError,
  getExpressMiddleware,
};
