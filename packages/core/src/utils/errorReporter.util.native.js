// 9fbef606107a605d69c0edbcd8029e5d
import { getAppPublishingDate, getReferenceId } from '@tcp/core/src/utils/utils.app';
import { Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';

let raygunInstance = null;

const setRaygunInstance = (instance) => {
  raygunInstance = instance;
  return instance;
};

const getRaygunInstance = () => {
  return raygunInstance;
};

export const initAppErrorReporter = (config) => {
  const { raygunApiKey, appType, isDevelopment } = config;

  const {
    getIpAddress,
    getDeviceId,
    getModel,
    getBrand,
    getDeviceName,
    getSystemName,
    getSystemVersion,
    getBuildNumber,
    getTotalMemory,
    getFreeDiskStorage,
    getBatteryLevel,
    getPowerState,
    isBatteryCharging,
    isLocationEnabled,
  } = DeviceInfo || {};

  // Get App version with publishing date
  const appVersionWithPublishingDate = getAppPublishingDate();

  if (isDevelopment || !raygunApiKey) {
    return null;
  }

  if (getRaygunInstance()) {
    return getRaygunInstance();
  }
  // eslint-disable-next-line global-require
  Promise.all([require('raygun4js')]).then(([rg4js]) => {
    rg4js('enableCrashReporting', true);
    rg4js('apiKey', raygunApiKey);
    rg4js('setVersion', appVersionWithPublishingDate);
    rg4js('withCustomData', {
      brand: appType,
      iPAddress: getIpAddress(),
      model: getModel(),
      DeviceId: getDeviceId(),
      DeviceBrand: getBrand(),
      DeviceName: getDeviceName(),
      SystemName: getSystemName(),
      SystemVersion: getSystemVersion(),
      BuildNumber: getBuildNumber(),
      TotalMemory: getTotalMemory(),
      FreeDiskStorage: getFreeDiskStorage(),
      BatteryLevel: getBatteryLevel(),
      PowerState: getPowerState(),
      IsBatteryCharging: isBatteryCharging(),
      IsLocationEnabled: isLocationEnabled(),
    });
    rg4js('logContentsOfXhrCalls', true);
    setRaygunInstance(rg4js);
    rg4js('boot');
  });
  return getRaygunInstance();
};

export const trackError = (errorArgs) => {
  if (!getRaygunInstance()) {
    return;
  }

  const { error } = errorArgs;
  const errorObject = typeof error === 'string' ? new Error(error) : error;
  let { errorTags = [], extraData } = errorArgs;

  errorTags = [...errorTags, Platform.OS, getReferenceId()];

  extraData = extraData || {};

  getRaygunInstance()('send', {
    error: errorObject,
    tags: errorTags,
    customData: { ...extraData },
  });
};
