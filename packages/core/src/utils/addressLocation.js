// 9fbef606107a605d69c0edbcd8029e5d
import { executeExternalAPICall } from '@tcp/core/src/services/handler';
import { getCacheData, setCacheData } from './localCache.util';
import { getApiUrl } from './mapbox';

const logger = require('@tcp/core/src/utils/loggerInstance');

export const getAddressLocationInfo = async (address, mapboxStoreSearchTypesParam) => {
  const googleApiStoredDataObj = await getCacheData('geocode-response', address);
  if (googleApiStoredDataObj) {
    return new Promise((resolve) => {
      resolve({
        lat: googleApiStoredDataObj.lat,
        lng: googleApiStoredDataObj.lng,
        country: googleApiStoredDataObj.country,
      });
    });
  }
  const params = {
    isStoreSearch: true,
    mapboxTypesParam: mapboxStoreSearchTypesParam,
  };
  const apiUrl = await getApiUrl(address, params);
  const payload = {
    webService: {
      URI: apiUrl,
      method: 'GET',
    },
  };

  return executeExternalAPICall(payload)
    .then((res) => res.body)
    .then(
      async (result) => {
        let storeDataObject = {};
        if (
          result.features &&
          result.features.length > 0 &&
          result.features[0].geometry &&
          result.features[0].geometry.coordinates &&
          result.features[0].geometry.coordinates.length > 0
        ) {
          const [longitude, latitude] = result.features[0].geometry.coordinates;
          let countryCode = '';
          for (let i = 0; i < result.features[0].context.length; i += 1) {
            if (result.features[0].context[i].id.includes('country')) {
              countryCode = result.features[0].context[i].short_code.toUpperCase();
            }
          }
          const timeStamp = new Date().getTime();
          storeDataObject = {
            lat: latitude,
            lng: longitude,
            country: countryCode,
          };
          await setCacheData({
            key: 'geocode-response',
            storageKey: address,
            storageValue: { ...storeDataObject, timeStamp },
          });
        }
        return storeDataObject;
      },
      (error) => {
        logger.error('Unable to fetch data from mapbox', error);
      }
    );
};

export default getAddressLocationInfo;
