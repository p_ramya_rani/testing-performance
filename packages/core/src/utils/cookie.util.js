// 9fbef606107a605d69c0edbcd8029e5d 
import { isClient, getAPIConfig, readCookie, setCookie, isMobileApp } from './index';
import { API_CONFIG } from '../services/config';

export const CART_ITEM_COUNTER = 'cartItemsCount';
export const SFL_ITEM_COUNTER = 'sflItemsCount_US';
export const SFL_ITEM_COUNTER_CA = 'sflItemsCount_CA';

export { setCookie, readCookie };

/**
 * Remove cookie in browser based on the key
 * @param {string} key - Cookie key/name
 */
export const removeCookie = (key, isTCP = true) => {
  if (isClient()) {
    const domain = isTCP ? '.childrensplace.com' : '.gymboree.com';
    document.cookie = `${key}=;domain=${domain};path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
  }
};

export function getCartItemCount(isPromise = false) {
  const cookie = readCookie(CART_ITEM_COUNTER);
  if (isPromise && isMobileApp()) return cookie;
  return parseInt(cookie || 0, 10);
}

export function getSflItemCount(siteId) {
  // If CA site, fetch CA cookies. Else pick the US cookies for sflCount by default.
  if (siteId === 'CA') {
    return parseInt(readCookie(SFL_ITEM_COUNTER_CA) || 0, 10);
  }
  return parseInt(readCookie(SFL_ITEM_COUNTER) || 0, 10);
}

/**
 * @summary This returns the session Id created
 * from the Quantum cookie needed to tag Raygun errors.
 * @return  {String}  - string of cookies
 */
export function generateSessionId(showComplete) {
  const { sessionCookieKey } = API_CONFIG;

  const cookieValue = readCookie(sessionCookieKey, !isClient() ? getAPIConfig().cookie : null);

  return showComplete
    ? cookieValue
    : cookieValue &&
        `${cookieValue.substring(0, 4).toUpperCase()}-${cookieValue.substring(4, 8).toUpperCase()}`;
}
/**
 * @summary This returns all cookies in string format.
 * @return  {String}  - string of cookies
 */
export const getAllCookies = () => {
  return document.cookie;
};
