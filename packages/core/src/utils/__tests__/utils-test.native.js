// 9fbef606107a605d69c0edbcd8029e5d
import { PixelRatio } from 'react-native';
import CookieManager from 'react-native-cookies';
import { getPixelRatio, isCookiePresent, getBadgeParam, dayChange, getDate } from '../utils.app';

PixelRatio.get = jest.fn();

jest.mock('@tcp/core/src/utils', () => {
  const originalModule = jest.requireActual('@tcp/core/src/utils');
  return {
    ...originalModule,
    getAPIConfig: jest.fn(),
  };
});

describe('Utils.native ', () => {
  describe('getPixelRatio', () => {
    it('should return correct device in mdpi', () => {
      PixelRatio.get.mockReturnValue(1);
      expect(getPixelRatio()).toBe('mdpi');
    });

    it('should return correct device in hdpi', () => {
      PixelRatio.get.mockReturnValue(1.5);
      expect(getPixelRatio()).toBe('hdpi');
    });

    it('should return correct device in xhdpi', () => {
      PixelRatio.get.mockReturnValue(2);
      expect(getPixelRatio()).toBe('xhdpi');
    });

    it('should return correct device in xxhdpi', () => {
      PixelRatio.get.mockReturnValue(3);
      expect(getPixelRatio()).toBe('xxhdpi');
    });

    it('should return correct device in xxxhdpi', () => {
      PixelRatio.get.mockReturnValue(3.6);
      expect(getPixelRatio()).toBe('xxxhdpi');
    });
  });
});

describe('isCookiePresent', () => {
  it('should returns true if cookie present', () => {
    CookieManager.get = jest.fn().mockImplementation(() => {
      return {
        WC_PERSISTENT: 'abc',
      };
    });
    expect(isCookiePresent('WC_PERSISTENT')).resolves.toBeTruthy();
  });

  it('should returns true if cookie present and patern matches', () => {
    CookieManager.get = jest.fn().mockImplementation(() => {
      return {
        WC_USER_ACTIVITY_123456: 'abc',
      };
    });
    expect(isCookiePresent('WC_USER_ACTIVITY_')).resolves.toBeTruthy();
  });

  it('should returns false if cookie not present', () => {
    CookieManager.get = jest.fn().mockImplementation(() => {
      return {
        tcpState: 'DL',
      };
    });
    expect(isCookiePresent('WC_USER_ACTIVITY_')).resolves.toBeFalsy();
  });
});

describe('getBadgeParam', () => {
  it('return the badge param for MPACK', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: true,
        dynamicBadgeVirtualMPACKEnabled: true,
        dynamicBadgeSetEnabled: true,
        dynamicBadgeVirtualSetEnabled: true,
      },
      '0002',
      '2'
    );
    expect(badgeVal).toEqual('/l_ecom:assets:static:badge:pack2,g_west,w_0.22,fl_relative/');
  });
  it('return the badge param for VMPACK', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: true,
        dynamicBadgeVirtualMPACKEnabled: true,
        dynamicBadgeSetEnabled: true,
        dynamicBadgeVirtualSetEnabled: true,
      },
      '0005',
      '3'
    );
    expect(badgeVal).toEqual('/l_ecom:assets:static:badge:pack3,g_west,w_0.22,fl_relative/');
  });
  it('return the badge param for Set', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: true,
        dynamicBadgeVirtualMPACKEnabled: true,
        dynamicBadgeSetEnabled: true,
        dynamicBadgeVirtualSetEnabled: true,
      },
      '0003',
      '5'
    );
    expect(badgeVal).toEqual('/l_ecom:assets:static:badge:set5,g_west,w_0.22,fl_relative/');
  });
  it('return the badge param for VSET', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: true,
        dynamicBadgeVirtualMPACKEnabled: true,
        dynamicBadgeSetEnabled: true,
        dynamicBadgeVirtualSetEnabled: true,
      },
      '0006',
      '3'
    );
    expect(badgeVal).toEqual('/l_ecom:assets:static:badge:set3,g_west,w_0.22,fl_relative/');
  });
  it('return the empty badge param for MPACK', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: false,
        dynamicBadgeVirtualMPACKEnabled: false,
        dynamicBadgeSetEnabled: false,
        dynamicBadgeVirtualSetEnabled: false,
      },
      '0002',
      '2'
    );
    expect(badgeVal).toEqual('');
  });
  it('return the empty badge param for set', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: false,
        dynamicBadgeVirtualMPACKEnabled: false,
        dynamicBadgeSetEnabled: false,
        dynamicBadgeVirtualSetEnabled: false,
      },
      '0003',
      '2'
    );
    expect(badgeVal).toEqual('');
  });
  it('return the empty badge param for VMPACK', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: false,
        dynamicBadgeVirtualMPACKEnabled: false,
        dynamicBadgeSetEnabled: false,
        dynamicBadgeVirtualSetEnabled: false,
      },
      '0005',
      '2'
    );
    expect(badgeVal).toEqual('');
  });
  it('return the empty badge param for VSET', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: false,
        dynamicBadgeVirtualMPACKEnabled: false,
        dynamicBadgeSetEnabled: false,
        dynamicBadgeVirtualSetEnabled: false,
      },
      '0006',
      '2'
    );
    expect(badgeVal).toEqual('');
  });
  it('return the empty badge param for normal products', () => {
    const badgeVal = getBadgeParam(
      {
        dynamicBadgeMPACKEnabled: false,
        dynamicBadgeVirtualMPACKEnabled: false,
        dynamicBadgeSetEnabled: false,
        dynamicBadgeVirtualSetEnabled: false,
      },
      '0001',
      '1'
    );
    expect(badgeVal).toEqual('');
  });
});

describe('#getDate', () => {
  it('getDate Method should be Defined', () => {
    const date = getDate();
    expect(date).toBeDefined();
  });
});
describe('#dayChange', () => {
  it('dayChange Method should be Defined', () => {
    const day = dayChange();
    expect(day).toBeDefined();
  });
});
