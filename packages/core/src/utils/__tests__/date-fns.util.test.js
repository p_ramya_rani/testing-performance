// 9fbef606107a605d69c0edbcd8029e5d 
import { formatDate, formatDateWithLocale } from '../date-fns.util';

describe('formatDate', () => {
  const date = '2019-12-10 10:00:00';
  const baseDateFormat = 'MM/dd/yyyy';
  const namedDateFormat = 'MMM, dd yyyy';

  it('should format the date string correctly in mm/dd/yy', () => {
    expect(formatDate(date, baseDateFormat)).toEqual('12/10/2019');
  });

  it('should format the blank string correctly in mm/dd/yy to current date', () => {
    expect(formatDate('', baseDateFormat)).toEqual(formatDate(new Date(), baseDateFormat));
    expect(formatDate(null, baseDateFormat)).toEqual(formatDate(new Date(), baseDateFormat));
  });

  it('should format the date string correctly in mm/dd/yy', () => {
    const dateObj = new Date(date);
    expect(formatDate(dateObj, baseDateFormat)).toEqual('12/10/2019');
  });

  it('should format the date string correctly in MMM/dd/yy', () => {
    expect(formatDate(date, namedDateFormat)).toEqual('Dec, 10 2019');
  });

  it('should format the blank string correctly in mm/dd/yy to current date', () => {
    expect(formatDateWithLocale('', baseDateFormat, 'en')).toEqual(
      formatDate(new Date(), baseDateFormat)
    );
    expect(formatDateWithLocale(null, namedDateFormat, 'es')).toEqual(
      formatDateWithLocale(new Date(), namedDateFormat, 'es')
    );
  });

  it('should format the date string correctly in MMM/dd/yy in Spanish', () => {
    expect(formatDateWithLocale(date, namedDateFormat, 'es')).toEqual('dic, 10 2019');
  });

  it('should format the date correctly in MMM/dd/yy in Spanish', () => {
    const dateObj = new Date(date);
    expect(formatDateWithLocale(dateObj, namedDateFormat, 'es')).toEqual('dic, 10 2019');
  });
});
