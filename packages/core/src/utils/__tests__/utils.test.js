/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { getPrimaryImages } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import {
  getLabelValue,
  formatDate,
  isValidDate,
  childOptionsMap,
  formatPhoneNumber,
  formatPhone,
  getSiteId,
  getAddressFromPlace,
  extractFloat,
  getStoreHours,
  parseUTCDate,
  getOrderGroupLabelAndMessage,
  validateDiffInDaysNotification,
  getOrderStatusForNotification,
  convertToISOString,
  formatProductsData,
  getFormatedOrderDate,
  getTranslateDateInformation,
  addDays,
  generateTraceId,
  getAmznTraceId,
  isApplePayEligible,
  getAPIConfig,
  setBossBopisEnabled,
  getVideoUrl,
  getLocator,
  isTouchClient,
  isMobileApp,
  isServer,
  isClient,
  getStaticFilePath,
  getStaticPageFilePath,
  getIconPath,
  getFlagIconPath,
  capitalize,
  getPromotionalMessage,
  toTimeString,
  hashValuesReplace,
  utilPdpArrayHeader,
  safelyParseJSON,
  formatAddress,
  getBirthDateOptionMap,
  calculateAge,
  generateUniqueKeyUsingLabel,
  sanitizeEntity,
  checkPhone,
  parseBoolean,
  getFormSKUValue,
  getQueryParamfromCMSUrl,
  getDateInformation,
  buildStorePageUrlSuffix,
  getModifiedLanguageCode,
  isPastStoreHours,
  getCurrentStoreHours,
  getBopisOrderMessageAndLabel,
  getFormatedOrderTime,
  getAssetHostURL,
  changeImageURLToDOM,
  isAbsoluteUrl,
  getPaymentDetails,
  getStringAfterSplit,
  insertIntoString,
  getStyliticsUserName,
  getStyliticsRegion,
  canUseDOM,
  isValidObj,
  getProductUrlForDAM,
  getFormattedDate,
  getDateRange,
  getSelectedShipmentMsg,
  calculatePriceValue,
  getLabelsBasedOnPattern,
  convertNumToBool,
  getPageName,
  getQMSessionIdFromDefaultPreference,
  disableTcpApp,
  disableTcpAppProduct,
  parseStoreHours,
  isWebViewPage,
  pathHasHyphenSlash,
  createLayoutPath,
  getFontsURL,
  checkIfUserInfoCallRequired,
  getBootstrapCachedData,
  setBootstrapCachedData,
  getValueFromAsyncStorage,
  setValueFromAsyncStorage,
  removeMultipleFromAsyncStorage,
  clearLoginCookiesFromAsyncStore,
  checkForDuplicateAuthCookies,
  removeFileExtension,
  parseTrackingParamsInModuleClassNames,
  mergeUrlQueryParams,
  getCurrentRouteName,
  isValidOrderForReceipts,
  getMaskedEmail,
  getMultipackCount,
  filterMultiPack,
  flattenArray,
  resetApiConfig,
  triggerPostMessage,
  isCouponsUpdateRequired,
  cropVideoUrl,
  isIE11,
  isIEBrowser,
  getCacheKeyForRedis,
  getBrand,
  isTCP,
  isUpdateRequired,
  configureInternalNavigationFromCMSUrl,
  isCanada,
  isUsOnly,
  isGymboree,
  checkBossBopisEnabled,
  getApiTimeOut,
  regularProducts,
  checkOOS,
  getDecimalPrice,
  getDynamicBadgeQty,
  badgeTranformationPrefix,
  nullCheck,
  formatYmd,
  getOrderPackageStatus,
  getGroupedOrderItems,
  getTotalItemsCountInOrder,
  getMaxDateValue,
  getShipmentCount,
  getImageData,
  formatDateFromDay,
  getStoreHoursClosingTiming,
} from '../utils';

import storesMock from '../../components/common/molecules/StoreAddressTile/__mocks__/store.mock';
import constants from '../../components/features/account/OrderDetails/OrderDetails.constants';
import {
  openWindow,
  isCookiePresent,
  getURIForDeepLink,
  getSiteIdInCaps,
  getPlaceSiteId,
} from '../utils.web';

const formattedDate = '01/01/1970';
const formattedDateForDay = '2021-06-26';
const formattedDateForDayInput = 'Jun 26, 2021';
const formattedPhoneNumber = '(718) 243-1150';
const features = 'alt images full size image';
const pricingState = 'full price';
const dateString = '2019-09-17 19:59:00';

jest.mock('../utils', () => ({
  ...jest.requireActual('../utils'),
  getStaticFilePath: jest.fn(),
  isMobileApp: jest.fn(),
}));

describe('getLabelValue', () => {
  const labelState = {
    account: {
      payment: {
        lbl_name: 'test',
      },
    },
  };

  it('should return correct label value if category and subCategory both are passed', () => {
    const label = getLabelValue(labelState, 'lbl_name', 'payment', 'account');
    expect(label).toBe(labelState.account.payment.lbl_name);
  });

  it('should return labelKey if any of the category and subCategory is missing', () => {
    const label = getLabelValue(labelState, 'lbl_name');
    expect(label).toBe('lbl_name');
  });

  it('should return correct label value if labelState is category and subCategory is passed', () => {
    const label = getLabelValue(labelState.account, 'lbl_name', 'payment');
    expect(label).toBe(labelState.account.payment.lbl_name);
  });

  it('should return labelkey if incorrect params type are passed', () => {
    const label = getLabelValue('', 'common.account.lbl_last_name');
    expect(label).toBe('common.account.lbl_last_name');
  });

  it('should return label key if category is passed but not present in label state', () => {
    const label = getLabelValue(labelState, 'lbl_last_name', 'test', 'test');
    expect(label).toBe('lbl_last_name');
  });

  it('should return label key if category & subcategory is passed but subcategory is not present in label state', () => {
    const label = getLabelValue(labelState, 'lbl_last_name', 'test', 'account');
    expect(label).toBe('lbl_last_name');
  });

  it('should return label key if subcategory is passed but subcategory is not present in label state', () => {
    const label = getLabelValue(labelState.account, 'lbl_last_name', 'test');
    expect(label).toBe('lbl_last_name');
  });

  it('should return label key if subcategory is incorrectly passed', () => {
    const label = getLabelValue(labelState.account, 'lbl_last_name', 'lbl_last_name');
    expect(label).toBe('lbl_last_name');
  });
});

describe('formatDate', () => {
  xit('should format the date correctly in mm/dd/yy', () => {
    const date = new Date(formattedDate);
    expect(formatDate(date)).toEqual(formattedDate);
  });

  it('should format the date correctly for Day in mm/dd/yy', () => {
    const date = new Date(formattedDateForDayInput);
    expect(formatDateFromDay(date)).toEqual(formattedDateForDay);
  });

  it('should convert normal date string to ISO date string for Safari Handling', () => {
    const dateString1 = '11-09-2019 11:22:00';
    expect(convertToISOString(dateString1)).toBe('11-09-2019T11:22:00');
  });

  it('should not convert null date string to ISO date string for Safari Handling', () => {
    expect(convertToISOString('')).toBe('');
    expect(convertToISOString(null)).toBe(null);
  });
});

describe('isvalidDate', () => {
  it('should return true for valid date', () => {
    const date = new Date(formattedDate);
    expect(isValidDate(date)).toBeTruthy();
  });

  it('should return false for invalid date', () => {
    const date = new Date('');
    expect(isValidDate(date)).toBeFalsy();
  });
});

describe('resetApiConfig', () => {
  it('should reset the api config', () => {
    expect(resetApiConfig()).toBe(undefined);
  });
});

describe('triggerPostMessage', () => {
  it('should trigger the post message', () => {
    expect(triggerPostMessage()).toBe(undefined);
  });
});

describe('isCouponsUpdateRequired', () => {
  it('should return if the coupon update is required', () => {
    expect(isCouponsUpdateRequired(new Date('2019-09-17 11:43:00'), 2)).toBe(true);
  });
});

describe('disableTcpApp', () => {
  it('should return if tcp app is disabled', () => {
    const expectedResponse = disableTcpApp();
    expect(disableTcpApp()).toBe(expectedResponse);
  });
});

describe('disableTcpAppProduct', () => {
  it('should return true if tcp app product is disabled', () => {
    const expectedResponse = disableTcpAppProduct();
    expect(disableTcpAppProduct()).toBe(expectedResponse);
  });
});

describe('isUpdateRequired', () => {
  it('should return if the API fetch is required', () => {
    expect(isUpdateRequired(new Date('2019-12-07 11:43:00'), 2)).toBe(true);
  });
});

describe('parseStoreHours', () => {
  it('should parse Store Hours', () => {
    const hoursOfOperation = {
      key: {
        availability: [
          {
            unshift: {
              xyz: 'xyz',
            },
            from: '2019-08-17 11:43:00',
            to: '2019-09-17 23:43:00',
            status: 'open',
          },
        ],
        nick: 'nick',
      },
    };
    const expectedOutput = [
      {
        dayName: 'NICK',
        isClosed: false,
        openIntervals: [
          {
            fromHour: '2019-08-17 11:43:00',
            toHour: '2019  - 8 - 17 23:59:59',
          },
        ],
      },
    ];
    expect(parseStoreHours(hoursOfOperation)).toStrictEqual(expectedOutput);
  });
});

describe('cropVideoUrl', () => {
  const url = 'videoUrl/upload/path';
  it('should crop the video url', () => {
    expect(cropVideoUrl(url)).toStrictEqual('videoUrl/upload/path');
  });
});

describe('isIE11', () => {
  const isIE = isIE11();
  it('should return true if IE11', () => {
    expect(isIE11()).toBe(isIE);
  });
});

describe('isIEBrowser', () => {
  const isBrowser = isIEBrowser();
  it('should return true if IE browser', () => {
    expect(isIEBrowser()).toBe(isBrowser);
  });
});

describe('getCacheKeyForRedis', () => {
  const expectedCacheKey = getCacheKeyForRedis('cacheId');
  it('should return cache key for redis', () => {
    expect(getCacheKeyForRedis('cacheId')).toBe(expectedCacheKey);
  });
});

describe('getBrand', () => {
  const expectedBrand = getBrand();
  it('should return brand', () => {
    expect(getBrand()).toBe(expectedBrand);
  });
});

describe('isTCP', () => {
  const isTcpBrand = isTCP();
  it('should return true if it is tcp brand', () => {
    expect(isTCP()).toBe(isTcpBrand);
  });
});

describe('configureInternalNavigationFromCMSUrl', () => {
  const urlPDP = 'https://childrensplace.com/c/boys-tees';
  it('should return plp configured internal navigation', () => {
    expect(configureInternalNavigationFromCMSUrl(urlPDP)).toBe(
      'https://childrensplace.com/c/boys-tees'
    );
  });
  const urlPLP = 'https://childrensplace.com/p/boys-tees';
  it('should return pdp configured internal navigation', () => {
    expect(configureInternalNavigationFromCMSUrl(urlPLP)).toBe(
      'https://childrensplace.com/p/boys-tees'
    );
  });
  const urlOutfit = 'https://childrensplace.com/outfit/boys-tees';
  it('should return outfit configured internal navigation', () => {
    expect(configureInternalNavigationFromCMSUrl(urlOutfit)).toBe(
      'https://childrensplace.com/outfit/boys-tees'
    );
  });
  const urlBundle = 'https://childrensplace.com/b/boys-tees';
  it('should return bundled configured internal navigation', () => {
    expect(configureInternalNavigationFromCMSUrl(urlBundle)).toBe(
      'https://childrensplace.com/b/boys-tees'
    );
  });
  const urlSearch = 'https://childrensplace.com/search/boys-tees';
  it('should return search configured internal navigation', () => {
    expect(configureInternalNavigationFromCMSUrl(urlSearch)).toBe(
      'https://childrensplace.com/search/boys-tees'
    );
  });
  const urlContent = 'https://childrensplace.com/content/boys-tees';
  it('should return content configured internal navigation', () => {
    expect(configureInternalNavigationFromCMSUrl(urlContent)).toBe(
      'https://childrensplace.com/content/boys-tees'
    );
  });
  const urlHelp = 'https://childrensplace.com/help-center/boys-tees';
  it('should return help-center configured internal navigation', () => {
    expect(configureInternalNavigationFromCMSUrl(urlHelp)).toBe(
      'https://childrensplace.com/help-center/boys-tees'
    );
  });
});

describe('isCanada', () => {
  const isCountryCanada = isCanada();
  it('should return true if country is canada', () => {
    expect(isCanada()).toBe(isCountryCanada);
  });
});

describe('isUsOnly', () => {
  const isCountryUS = isUsOnly();
  it('should return true if country is US', () => {
    expect(isUsOnly()).toBe(isCountryUS);
  });
});

describe('isGymboree', () => {
  const isBrandGymboree = isGymboree();
  it('should return true if brand is Gymboree', () => {
    expect(isGymboree()).toBe(isBrandGymboree);
  });
});

describe('childOptionsMap', () => {
  const restoreDate = global.Date;
  beforeEach(() => {
    const mockDate = new Date(2019, 6, 24);
    global.Date = jest.fn(() => mockDate);
  });

  afterEach(() => {
    global.Date = restoreDate;
  });

  it('should return childOptionsMap mapping', () => {
    const childOptions = childOptionsMap();
    expect(childOptions).toEqual({
      genderMap: [
        { displayName: 'Boy', id: '01' },
        { displayName: 'Girl', id: '0' },
      ],
      yearsMap: [
        { displayName: '2019', id: '2019' },
        { displayName: '2018', id: '2018' },
        { displayName: '2017', id: '2017' },
        { displayName: '2016', id: '2016' },
        { displayName: '2015', id: '2015' },
        { displayName: '2014', id: '2014' },
        { displayName: '2013', id: '2013' },
        { displayName: '2012', id: '2012' },
        { displayName: '2011', id: '2011' },
        { displayName: '2010', id: '2010' },
        { displayName: '2009', id: '2009' },
        { displayName: '2008', id: '2008' },
        { displayName: '2007', id: '2007' },
        { displayName: '2006', id: '2006' },
        { displayName: '2005', id: '2005' },
        { displayName: '2004', id: '2004' },
        { displayName: '2003', id: '2003' },
        { displayName: '2002', id: '2002' },
      ],
    });
  });
});

describe('formatPhoneNumber', () => {
  it('should format the phone number correctly with area code in brackets', () => {
    const phone = formatPhoneNumber('7182431150');
    expect(phone).toBe(formattedPhoneNumber);
  });
});

describe('formatPhone', () => {
  it('should format the phone number', () => {
    const phone = formatPhone('71824311501', '-');
    expect(formatPhone('71824311501', '-')).toStrictEqual(phone);
  });
});

describe('getSiteId', () => {
  it('should return the site Id', () => {
    const expectedSiteId = getSiteId();
    expect(getSiteId()).toStrictEqual(expectedSiteId);
  });
});

describe('getSiteIdInCaps', () => {
  it('should return the site Id in capital letter', () => {
    const expectedSiteId = getSiteIdInCaps();
    expect(getSiteIdInCaps()).toStrictEqual(expectedSiteId);
  });
});

describe('getPlaceSiteId', () => {
  it('should return the site Id in capital letter', () => {
    const getPlaceSiteIdRes = getPlaceSiteId();
    expect(getPlaceSiteId()).toStrictEqual(getPlaceSiteIdRes);
  });
});

describe('parseBoolean', () => {
  it('should return bool as true', () => {
    expect(parseBoolean('1')).toBe(true);
  });
  it(' 1 should return true', () => {
    expect(parseBoolean(1)).toBe(true);
  });
  it('tRuE should return true', () => {
    expect(parseBoolean('tRuE')).toBe(true);
  });
});

describe('window onTouchClient', () => {
  it('return true if onTouchStart is in window', () => {
    const windowSpy = jest.spyOn(window, 'open');
    windowSpy.mockImplementation(() => ({
      notontouchstart: true,
    }));
    expect(isTouchClient()).toBe(false);
  });
});

describe('isServer', () => {
  it('return true if isServer', () => {
    const windowSpy = jest.spyOn(window, 'open');
    windowSpy.mockImplementation(() => ({
      notOntouchstart: true,
    }));
    isMobileApp.mockImplementation(() => false);
    expect(isServer()).toBe(false);
  });
});

describe('isClient', () => {
  it('return true if isClient', () => {
    const windowSpy = jest.spyOn(window, 'open');
    windowSpy.mockImplementation(() => ({
      notOntouchstart: true,
    }));
    isMobileApp.mockImplementation(() => false);
    expect(isClient()).toBe(true);
  });
});

describe('getIconPath', () => {
  it('should return the icon path', () => {
    expect(getIconPath('green-dollar')).toBe(
      'undefined/ecom/assets/static/icon/dollar-sign-icon.svg'
    );
  });
});

describe('getFlagIconPath', () => {
  it('should return the flag icon path', () => {
    expect(getFlagIconPath('FJ')).toBe('undefined/ecom/assets/static/icon/flags/fiji.svg');
  });
});

describe('getLocator', () => {
  it('should return the locator', () => {
    expect(getLocator('promo_item')).toBe('global_promobannerimg');
  });
});

describe('getStaticFilePath', () => {
  it('should return static file path', () => {
    getStaticFilePath.mockImplementation(() => '/staticPath');
    expect(getStaticPageFilePath('pageFile')).toBe(
      '/_next/static/version-not-available/pages/pageFile'
    );
  });
});

describe('capitalize', () => {
  it('should return uppercase string', () => {
    expect(capitalize('page')).toBe('Page');
  });
});

describe('getPromotionalMessage', () => {
  it('should return promotional PLCC message', () => {
    expect(
      getPromotionalMessage(true, {
        promotionalPLCCMessage: 'promotionalPLCCMessage',
      })
    ).toBe('promotionalPLCCMessage');
  });
  it('should return promotional message', () => {
    expect(getPromotionalMessage(false, { promotionalMessage: 'promotionalMessage' })).toBe(
      'promotionalMessage'
    );
  });
});

describe('calculateAge', () => {
  it('should return calculated age', () => {
    expect(calculateAge(7, 2007)).toBe('14 yo');
  });
  it('should return calculated age', () => {
    expect(calculateAge(7, 2015)).toBe('6 yo');
  });
});

describe('getTimeString', () => {
  const est = new Date('2019-09-17 11:43:00');
  it('should return time string', () => {
    expect(toTimeString(est, false)).toBe('11:43 am');
  });
  it('should return time string', () => {
    expect(toTimeString(est, true)).toBe('11 am');
  });
});

describe('hashValuesReplace', () => {
  const str = 'pdp/v1.0/#style-no#?brand=#brand#&lang=#lang#&site=#site#';
  const utilArr = [
    {
      key: '#style-no#',
      value: '3011176',
    },
    {
      key: '#brand#',
      value: 'TCP',
    },
    {
      key: '#lang#',
      value: 'EN',
    },
    {
      key: '#site#',
      value: 'US',
    },
  ];
  it('should return formatted url', () => {
    expect(hashValuesReplace(str, utilArr)).toBe('pdp/v1.0/3011176?brand=TCP&lang=EN&site=US');
  });
});

describe('utilPdpArrayHeader', () => {
  const prodPartNo = '3011176';
  const brand = 'TCP';
  const lang = 'EN';
  const site = 'US';
  const expectedUtilArr = [
    {
      key: '#style-no#',
      value: '3011176',
    },
    {
      key: '#brand#',
      value: 'TCP',
    },
    {
      key: '#lang#',
      value: 'EN',
    },
    {
      key: '#site#',
      value: 'US',
    },
  ];
  it('should return formatted util Pdp Header', () => {
    expect(utilPdpArrayHeader(prodPartNo, { brand, site, lang })).toStrictEqual(expectedUtilArr);
  });
});

describe('parseJSON', () => {
  const json = '{"result":true, "count":42}';
  it('should return parsed JSON', () => {
    expect(safelyParseJSON(json).result).toBe(true);
  });
  it('should return parsed JSON', () => {
    expect(safelyParseJSON(json).count).toBe(42);
  });
});

describe('send unique label', () => {
  it('should return unique label', () => {
    expect(generateUniqueKeyUsingLabel('label_\test')).toBe('label__est');
  });
});

describe('sanitizeEntity', () => {
  it('should return sanitized Entity', () => {
    expect(sanitizeEntity('tcp&amp;/gigym')).toBe('tcp&/gigym');
  });
});

describe('getDateInformation', () => {
  const est = new Date('2020-11-03 19:59:00');
  const expectedOutput = { date: 3, day: 'TUE', month: 'NOV' };
  it('should getDateInformation in uppercase', () => {
    expect(getDateInformation(est, true)).toStrictEqual(expectedOutput);
  });
});

describe('buildStorePageUrlSuffix', () => {
  const storeBasicInfo = {
    id: 1,
    storeName: 'store',
    address: {
      state: 'NJ',
      city: 'DL',
      zip: '07093',
    },
  };
  const expectedOutput = 'store-nj-dl--1';
  it('should buildStorePageUrlSuffix', () => {
    expect(buildStorePageUrlSuffix(storeBasicInfo)).toStrictEqual(expectedOutput);
  });
});

describe('checkPhone', () => {
  it('should remove -', () => {
    expect(checkPhone('91-92345')).toBe('9192345');
  });
  it('should remove ()', () => {
    expect(checkPhone('(9192345)')).toBe('9192345');
  });
});

describe('getFormSKUValue', () => {
  const formValue = {
    color: {
      name: 'Orange',
    },
    Size: {
      name: 'XS',
    },
    Quantity: {
      name: 1,
    },
    Fit: {
      name: 'Normal',
    },
  };
  const expectedValue = {
    color: 'Orange',
    fit: 'Normal',
    quantity: 1,
    size: 'XS',
  };
  it('should remove -', () => {
    expect(getFormSKUValue(formValue)).toStrictEqual(expectedValue);
  });
});

describe('getQueryParamfromCMSUrl', () => {
  const url = 'this?is/url';
  it('should getQueryParamfromCMSUrl -', () => {
    expect(getQueryParamfromCMSUrl(url, '/')).toStrictEqual('url');
  });
});

describe('isPastStoreHours', () => {
  const date1 = new Date('2019-09-17 18:59:00');
  const date2 = new Date('2019-09-17 20:59:00');
  it('is Past store hours should return true', () => {
    expect(isPastStoreHours(date1, date2)).toBe(true);
  });
});

describe('getFormatedOrderTime', () => {
  const date1 = new Date('2019-09-17 07:43:00');
  it('should return formetter order time', () => {
    expect(getFormatedOrderTime(date1)).toStrictEqual('7:43am');
  });
});

describe('getAssetHostURL', () => {
  it('should return asset host url', () => {
    const expectedHostUrl = getAssetHostURL();
    expect(getAssetHostURL()).toStrictEqual(expectedHostUrl);
  });
});

describe('changeImageURLToDOM', () => {
  it('should change image url to DOM', () => {
    const expectedDOM = changeImageURLToDOM('imagePath', 'cropParams');
    expect(changeImageURLToDOM('imagePath', 'cropParams')).toStrictEqual(expectedDOM);
  });
});

describe('getFormattedDate', () => {
  const date1 = new Date('2020-11-03 09:59:00');
  it.skip('should return formatted date', () => {
    expect(getFormattedDate(date1)).toStrictEqual('Tue, Nov 3');
  });
});

describe('getDateRange', () => {
  const orderEddDates = {
    minDates: {
      UGNR: '2019-11-02 09:59:00',
    },
    maxDates: {
      UGNR: '2020-11-02 09:59:00',
    },
  };
  it.skip('should return UGNR date range', () => {
    expect(getDateRange(orderEddDates, 'UGNR', true)).toStrictEqual(
      '2019-11-02 09:59:00 | 2020-11-02 09:59:00'
    );
  });
  const orderEddDatesUGNR = {
    minDates: {
      UGNR: '2020-11-04 09:59:00',
    },
    maxDates: {
      UGNR: '2020-11-06 09:59:00',
    },
  };
  it.skip('should return UGNR date range with withoutFormat as false', () => {
    expect(getDateRange(orderEddDatesUGNR, 'UGNR', false)).toStrictEqual('Wed, Nov 4 - Fri, Nov 6');
  });
  const orderEddDatesU2AK = {
    minDates: {
      U2AK: '2019-11-03 09:59:00',
    },
    maxDates: {
      U2AK: '2019-11-06 09:59:00',
    },
  };
  it.skip('should return U2AK date range', () => {
    expect(getDateRange(orderEddDatesU2AK, 'U2AK', true)).toStrictEqual(
      '2019-11-03 09:59:00 | 2019-11-06 09:59:00'
    );
  });
  const orderEddDatesPMDC = {
    minDates: {
      PMDC: '2021-11-02 09:59:00',
    },
    maxDates: {
      PMDC: '2021-11-03 09:59:00',
    },
  };
  it.skip('should return PMDC date range', () => {
    expect(getDateRange(orderEddDatesPMDC, 'PMDC', true)).toStrictEqual(
      '2021-11-02 09:59:00 | 2021-11-03 09:59:00'
    );
  });
});

describe('getSelectedShipmentMsg', () => {
  const selectedShipment = {
    displayName: 'Order123',
    shippingSpeed: 'fast',
  };
  it('should return selected shipping message', () => {
    expect(getSelectedShipmentMsg(selectedShipment, ['Rush', 'Express'])).toStrictEqual(
      'Order123 - fast'
    );
  });
  it('should return empty string when selectedShipment is empty', () => {
    expect(getSelectedShipmentMsg('', ['Rush', 'Express'])).toStrictEqual('');
  });
  it('should return empty string when shippingMethods is empty', () => {
    expect(getSelectedShipmentMsg('', [])).toStrictEqual('');
  });
});

describe('isAbsoluteUrl', () => {
  it('should return true', () => {
    expect(isAbsoluteUrl('https://childrensplace.com')).toBe(true);
  });
  it('should return false', () => {
    expect(isAbsoluteUrl('www.childrensplace.com')).toBe(false);
  });
});

describe('getPaymentDetails', () => {
  const orderDetailsData = {
    appliedGiftCards: [
      {
        cardType: 'Visa',
        endingNumbers: '1234',
      },
    ],
    checkout: {
      billing: 'rush',
      card: 'master',
    },
  };
  it('should return true', () => {
    expect(getPaymentDetails(orderDetailsData)).toStrictEqual(['Visa x1234']);
  });
});

describe('getStringAfterSplit', () => {
  it('should split by /', () => {
    expect(getStringAfterSplit('tcp/gymboree', '/')).toStrictEqual(['tcp', 'gymboree']);
  });
  it('should split by -', () => {
    expect(getStringAfterSplit('tcp-gymboree', '-')).toStrictEqual(['tcp', 'gymboree']);
  });
  it('should return null', () => {
    expect(getStringAfterSplit('', '-')).toBe(null);
  });
});

describe('insertIntoString', () => {
  it('should insertIntoString', () => {
    expect(insertIntoString('tcpgymboree', 1, 5, 'test')).toStrictEqual('ttestboree');
  });
});

describe('getStyliticsUserName', () => {
  it('should return stylitics username', () => {
    const expectedStyliticsName = getStyliticsUserName();
    expect(getStyliticsUserName()).toStrictEqual(expectedStyliticsName);
  });
});

describe('getStyliticsRegion', () => {
  it('should return stylitics Region', () => {
    const expectedRegion = getStyliticsRegion();
    expect(getStyliticsRegion()).toStrictEqual(expectedRegion);
  });
});

describe('canUseDOM', () => {
  it('should return if user canUseDOM', () => {
    const expectedOutput = canUseDOM();
    expect(canUseDOM()).toStrictEqual(expectedOutput);
  });
});

describe('isValidObj', () => {
  it('should return true if isValidObj', () => {
    expect(isValidObj({ key: 'value' })).toStrictEqual(true);
  });
  it('should return false if isValidObj', () => {
    expect(isValidObj({})).toStrictEqual(false);
  });
});

describe('getProductUrlForDAM', () => {
  it('should return productUrl for DAM', () => {
    expect(getProductUrlForDAM('0123_456')).toStrictEqual('0123/0123_456');
  });
});

describe('calculatePriceValue', () => {
  it('should return calculatePriceValue', () => {
    expect(calculatePriceValue(123)).toStrictEqual('$123.00');
  });
});

describe('convertNumToBool', () => {
  it('should convert number to boolean', () => {
    expect(convertNumToBool(123)).toBe(true);
  });
});

describe('getPageName', () => {
  const pageObj = {
    generalProductId: '0123_456',
    name: 'pageName',
  };
  it('should return pagename', () => {
    expect(getPageName(pageObj)).toStrictEqual('product:0123:pageName');
  });
});

describe('getQMSessionIdFromDefaultPreference', () => {
  it('should return empty string', () => {
    expect(getQMSessionIdFromDefaultPreference()).toStrictEqual('');
  });
});

describe('pathHasHyphenSlash', () => {
  it('should return false for empty string', () => {
    expect(pathHasHyphenSlash('')).toBe(false);
  });
  it('should return true string having slash', () => {
    expect(pathHasHyphenSlash('tcp/gym')).toStrictEqual(['/']);
  });
});

describe('createLayoutPath', () => {
  it('should return empty string for empty path', () => {
    expect(createLayoutPath('')).toBe('');
  });
  it('should return layout path', () => {
    expect(createLayoutPath('tcp/gym')).toStrictEqual('tcpgym');
  });
});

describe('getFontsURL', () => {
  const fontsHostUrl = 'https://assets.theplace.com/rwd/';
  it('should return fonts url', () => {
    expect(getFontsURL('filepath')).toStrictEqual(`${fontsHostUrl}filepath`);
  });
});

describe('checkIfUserInfoCallRequired', () => {
  const routeList = ['https://assets.theplace.com/rwd/', 'https:childrensplace.com/'];
  it('should return checkIfUserInfoCallRequired', () => {
    expect(checkIfUserInfoCallRequired(routeList, 'rwd')).toBe(true);
  });
});

describe('removeFileExtension', () => {
  const path = 'thechildrensplace.png';
  it('should return removed file extension', () => {
    expect(removeFileExtension(path)).toStrictEqual('thechildrensplace');
  });
});

describe('parseTrackingParamsInModuleClassNames', () => {
  it('should return removed file extension', () => {
    expect(parseTrackingParamsInModuleClassNames('moduleclassName+tcp')).toStrictEqual({
      trackingParams: '',
      trackingString: '',
    });
  });
});

describe('getCurrentRouteName', () => {
  const routerName = getCurrentRouteName();
  it('should return current route name', () => {
    expect(getCurrentRouteName()).toBe(routerName);
  });
});

describe('filterMultiPack', () => {
  const item = [
    {
      prodpartno: 'prodPartNo',
      variantId: 'variantId',
    },
  ];
  it('should return current route name', () => {
    expect(filterMultiPack(item)).toStrictEqual([
      {
        prodpartno: 'prodPartNo',
        variantId: 'variantId',
        vId: 'prodPartNo_variantId',
      },
    ]);
  });
});

describe('flattenArray', () => {
  const arr = [1, 2, 3, 4, 5];
  it('should return flattened array', () => {
    expect(flattenArray(arr)).toStrictEqual([1, 2, 3, 4, 5]);
  });
  it('should return empty array', () => {
    expect(flattenArray([])).toStrictEqual([]);
  });
});

describe('getMultipackCount', () => {
  it('should return 21 as multipack count', () => {
    expect(getMultipackCount(['us#10', 'cs#11'])).toBe(21);
  });
  it('should return 11 as multipack count', () => {
    expect(getMultipackCount(['us#11'])).toBe(11);
  });
});

describe('getMaskedEmail', () => {
  it('should return masked email', () => {
    expect(getMaskedEmail('thechildrensplace@gmail.com')).toStrictEqual('txxxxce@gmail.com');
  });
  it('should return empty email', () => {
    expect(getMaskedEmail('')).toStrictEqual('');
  });
});

describe('isValidOrderForReceipts', () => {
  it('should return false for expired order', () => {
    expect(isValidOrderForReceipts(12345, constants.STATUS_CONSTANTS.ORDER_EXPIRED)).toBe(false);
  });
  it('should return false for expired', () => {
    expect(isValidOrderForReceipts(12345, constants.STATUS_CONSTANTS.EXPIRED)).toBe(false);
  });
  it('should return false for cancelled order', () => {
    expect(isValidOrderForReceipts(12345, constants.STATUS_CONSTANTS.ORDER_CANCELED)).toBe(false);
  });
  it('should return false for cancelled', () => {
    expect(isValidOrderForReceipts(12345, constants.STATUS_CONSTANTS.CANCELLED)).toBe(false);
  });
  it('should return false for expired order', () => {
    expect(isValidOrderForReceipts(12345, 'Valid Order')).toBe(false);
  });
  it('should return false for empty order number', () => {
    expect(isValidOrderForReceipts('', 'Valid Order')).toBe(false);
  });
});

describe('mergeUrlQueryParams', () => {
  it('should return merged query params', () => {
    expect(mergeUrlQueryParams('url?tcp', 'token')).toStrictEqual('url?tcp&token');
  });
  it('should not return merged query params', () => {
    expect(mergeUrlQueryParams('urltcp', 'token')).toStrictEqual('urltcp?token');
  });
});

describe('following functions should return null', () => {
  it('getBootstrapCachedData should return null', () => {
    expect(getBootstrapCachedData()).toBe(null);
  });
  it('setBootstrapCachedData should return null', () => {
    expect(setBootstrapCachedData()).toBe(null);
  });
  xit('getValueFromAsyncStorage should return null', () => {
    expect(getValueFromAsyncStorage()).toBe(null);
  });
  it('setValueFromAsyncStorage should return null', () => {
    expect(setValueFromAsyncStorage()).toBe(null);
  });
  it('removeMultipleFromAsyncStorage should return null', () => {
    expect(removeMultipleFromAsyncStorage()).toBe(null);
  });
  it('clearLoginCookiesFromAsyncStore should return null', () => {
    expect(clearLoginCookiesFromAsyncStore()).toBe(null);
  });
  it('checkForDuplicateAuthCookies should return null', () => {
    expect(checkForDuplicateAuthCookies()).toBe(null);
  });
});

describe('isWebViewPage', () => {
  it('should return true if isWebViewPage', () => {
    expect(isWebViewPage('borderfree')).toBe(true);
  });
  it('should return true if isWebViewPage', () => {
    expect(isWebViewPage('help-center')).toBe(true);
  });
  it('should return true for isWebViewPage', () => {
    expect(isWebViewPage('content')).toBe(true);
  });
  it('should return true if it is WebViewPage', () => {
    expect(isWebViewPage('nowebpage')).toBe(false);
  });
  it('should return true for isWebViewPage', () => {
    expect(isWebViewPage('gift-cards')).toBe(true);
  });
});

describe('getLabelsBasedOnPattern', () => {
  const labels = {
    label_qwe: 'firstLabel',
    label_asd: 'secondLabel',
  };
  it('should return label based on pattern', () => {
    expect(getLabelsBasedOnPattern(labels, 'qwe')).toStrictEqual(['label_qwe']);
  });
  it('should return label based on pattern', () => {
    expect(getLabelsBasedOnPattern(labels, 'asd')).toStrictEqual(['label_asd']);
  });
});

describe('getBopisOrderMessageAndLabel', () => {
  it('should return orderIsReadyForPickup', () => {
    expect(
      getBopisOrderMessageAndLabel(constants.STATUS_CONSTANTS.ORDER_IN_PROCESS, [], true)
    ).toStrictEqual({
      label: 'lbl_orders_orderInProcess',
      message: 'lbl_orders_orderIsReadyForPickup',
    });
  });
  it('should return orderIsReadyForPickup', () => {
    expect(
      getBopisOrderMessageAndLabel(constants.STATUS_CONSTANTS.ORDER_RECEIVED, [], true)
    ).toStrictEqual({
      label: 'lbl_orders_orderInProcess',
      message: 'lbl_orders_orderIsReadyForPickup',
    });
  });
  const orderLabels = {
    lbl_orders_orderInProcess: 'Order in Process',
    lbl_orders_OrderReceived: 'Order Received',
    lbl_orders_orderIsReadyForPickup: 'Order Ready for Pickup',
    lbl_orders_processing: 'Order Processing',
  };
  it('should return message and label with isBBopis true', () => {
    expect(
      getBopisOrderMessageAndLabel(constants.STATUS_CONSTANTS.ORDER_RECEIVED, orderLabels, true)
    ).toStrictEqual({
      label: 'Order in Process',
      message: 'Order Ready for Pickup',
    });
  });
  it('should return message and label with isBopis false', () => {
    expect(
      getBopisOrderMessageAndLabel(constants.STATUS_CONSTANTS.ORDER_RECEIVED, orderLabels, false)
    ).toStrictEqual({ label: 'Order Received', message: 'Order Processing' });
  });
  it('should return message and label as null with default case', () => {
    expect(getBopisOrderMessageAndLabel('DEFAULT', orderLabels, false)).toStrictEqual({
      label: null,
      message: null,
    });
  });
});

describe('getCurrentStoreHours', () => {
  const date1 = new Date(dateString);
  it('should getCurrentStoreHours', () => {
    expect(getCurrentStoreHours([], date1)).toStrictEqual([]);
  });
  const intervals = [
    {
      openIntervals: [
        {
          toHour: '2020-11-03T01:43:53.005Z',
        },
      ],
    },
  ];
  it('should getCurrentStoreHours', () => {
    expect(getCurrentStoreHours(intervals, date1)).toStrictEqual([]);
  });
});

describe('formatAddress', () => {
  const userAddress = {
    firstName: 'tcp_first',
    lastName: 'tcp_last',
    address1: 'address1',
    address2: 'address2',
    city: 'NJ',
    state: 'DL',
    country: 'US',
    zip: '07093',
    phoneNumber: '123456',
  };
  const formattedAddress = {
    addressLine: ['address1', 'address2'],
    city: 'NJ',
    country: 'US',
    firstName: 'tcp_first',
    lastName: 'tcp_last',
    phone1: '123456',
    state: 'DL',
    zipCode: '07093',
  };
  it('should return formatted address', () => {
    expect(formatAddress(userAddress)).toStrictEqual(formattedAddress);
  });
});

describe('getBirthDateOptionMap', () => {
  const birthDateOptionMap = {
    daysMap: [
      { displayName: '1', id: '1' },
      { displayName: '2', id: '2' },
      { displayName: '3', id: '3' },
      { displayName: '4', id: '4' },
      { displayName: '5', id: '5' },
      { displayName: '6', id: '6' },
      { displayName: '7', id: '7' },
      { displayName: '8', id: '8' },
      { displayName: '9', id: '9' },
      { displayName: '10', id: '10' },
      { displayName: '11', id: '11' },
      { displayName: '12', id: '12' },
      { displayName: '13', id: '13' },
      { displayName: '14', id: '14' },
      { displayName: '15', id: '15' },
      { displayName: '16', id: '16' },
      { displayName: '17', id: '17' },
      { displayName: '18', id: '18' },
      { displayName: '19', id: '19' },
      { displayName: '20', id: '20' },
      { displayName: '21', id: '21' },
      { displayName: '22', id: '22' },
      { displayName: '23', id: '23' },
      { displayName: '24', id: '24' },
      { displayName: '25', id: '25' },
      { displayName: '26', id: '26' },
      { displayName: '27', id: '27' },
      { displayName: '28', id: '28' },
      { displayName: '29', id: '29' },
      { displayName: '30', id: '30' },
      { displayName: '31', id: '31' },
    ],
    monthsMap: [
      { displayName: 'Jan', id: '1' },
      { displayName: 'Feb', id: '2' },
      { displayName: 'Mar', id: '3' },
      { displayName: 'Apr', id: '4' },
      { displayName: 'May', id: '5' },
      { displayName: 'Jun', id: '6' },
      { displayName: 'Jul', id: '7' },
      { displayName: 'Aug', id: '8' },
      { displayName: 'Sep', id: '9' },
      { displayName: 'Oct', id: '10' },
      { displayName: 'Nov', id: '11' },
      { displayName: 'Dec', id: '12' },
    ],
    yearsMap: [
      { displayName: '1900', id: '1900' },
      { displayName: '1901', id: '1901' },
      { displayName: '1902', id: '1902' },
      { displayName: '1903', id: '1903' },
      { displayName: '1904', id: '1904' },
      { displayName: '1905', id: '1905' },
      { displayName: '1906', id: '1906' },
      { displayName: '1907', id: '1907' },
      { displayName: '1908', id: '1908' },
      { displayName: '1909', id: '1909' },
      { displayName: '1910', id: '1910' },
      { displayName: '1911', id: '1911' },
      { displayName: '1912', id: '1912' },
      { displayName: '1913', id: '1913' },
      { displayName: '1914', id: '1914' },
      { displayName: '1915', id: '1915' },
      { displayName: '1916', id: '1916' },
      { displayName: '1917', id: '1917' },
      { displayName: '1918', id: '1918' },
      { displayName: '1919', id: '1919' },
      { displayName: '1920', id: '1920' },
      { displayName: '1921', id: '1921' },
      { displayName: '1922', id: '1922' },
      { displayName: '1923', id: '1923' },
      { displayName: '1924', id: '1924' },
      { displayName: '1925', id: '1925' },
      { displayName: '1926', id: '1926' },
      { displayName: '1927', id: '1927' },
      { displayName: '1928', id: '1928' },
      { displayName: '1929', id: '1929' },
      { displayName: '1930', id: '1930' },
      { displayName: '1931', id: '1931' },
      { displayName: '1932', id: '1932' },
      { displayName: '1933', id: '1933' },
      { displayName: '1934', id: '1934' },
      { displayName: '1935', id: '1935' },
      { displayName: '1936', id: '1936' },
      { displayName: '1937', id: '1937' },
      { displayName: '1938', id: '1938' },
      { displayName: '1939', id: '1939' },
      { displayName: '1940', id: '1940' },
      { displayName: '1941', id: '1941' },
      { displayName: '1942', id: '1942' },
      { displayName: '1943', id: '1943' },
      { displayName: '1944', id: '1944' },
      { displayName: '1945', id: '1945' },
      { displayName: '1946', id: '1946' },
      { displayName: '1947', id: '1947' },
      { displayName: '1948', id: '1948' },
      { displayName: '1949', id: '1949' },
      { displayName: '1950', id: '1950' },
      { displayName: '1951', id: '1951' },
      { displayName: '1952', id: '1952' },
      { displayName: '1953', id: '1953' },
      { displayName: '1954', id: '1954' },
      { displayName: '1955', id: '1955' },
      { displayName: '1956', id: '1956' },
      { displayName: '1957', id: '1957' },
      { displayName: '1958', id: '1958' },
      { displayName: '1959', id: '1959' },
      { displayName: '1960', id: '1960' },
      { displayName: '1961', id: '1961' },
      { displayName: '1962', id: '1962' },
      { displayName: '1963', id: '1963' },
      { displayName: '1964', id: '1964' },
      { displayName: '1965', id: '1965' },
      { displayName: '1966', id: '1966' },
      { displayName: '1967', id: '1967' },
      { displayName: '1968', id: '1968' },
      { displayName: '1969', id: '1969' },
      { displayName: '1970', id: '1970' },
      { displayName: '1971', id: '1971' },
      { displayName: '1972', id: '1972' },
      { displayName: '1973', id: '1973' },
      { displayName: '1974', id: '1974' },
      { displayName: '1975', id: '1975' },
      { displayName: '1976', id: '1976' },
      { displayName: '1977', id: '1977' },
      { displayName: '1978', id: '1978' },
      { displayName: '1979', id: '1979' },
      { displayName: '1980', id: '1980' },
      { displayName: '1981', id: '1981' },
      { displayName: '1982', id: '1982' },
      { displayName: '1983', id: '1983' },
      { displayName: '1984', id: '1984' },
      { displayName: '1985', id: '1985' },
      { displayName: '1986', id: '1986' },
      { displayName: '1987', id: '1987' },
      { displayName: '1988', id: '1988' },
      { displayName: '1989', id: '1989' },
      { displayName: '1990', id: '1990' },
      { displayName: '1991', id: '1991' },
      { displayName: '1992', id: '1992' },
      { displayName: '1993', id: '1993' },
      { displayName: '1994', id: '1994' },
      { displayName: '1995', id: '1995' },
      { displayName: '1996', id: '1996' },
      { displayName: '1997', id: '1997' },
      { displayName: '1998', id: '1998' },
      { displayName: '1999', id: '1999' },
      { displayName: '2000', id: '2000' },
      { displayName: '2001', id: '2001' },
      { displayName: '2002', id: '2002' },
      { displayName: '2003', id: '2003' },
      { displayName: '2004', id: '2004' },
    ],
  };
  it('should return getBirthDateOptionMap', () => {
    expect(getBirthDateOptionMap()).toStrictEqual(birthDateOptionMap);
  });
});

describe('getVideoUrl', () => {
  it('should return videoFormat as mp4', () => {
    const videoUrl = getVideoUrl('https://childrensplace.video.mp4');
    expect(videoUrl).toEqual(['.mp4']);
  });
  it('should return videoFormat as webm', () => {
    const videoUrl = getVideoUrl('https://childrensplace.video.webm');
    expect(videoUrl).toEqual(['.webm']);
  });
  it('should return videoFormat as MP4', () => {
    const videoUrl = getVideoUrl('https://childrensplace.video.MP4');
    expect(videoUrl).toEqual(['.MP4']);
  });
  it('should return videoFormat as WEBM', () => {
    const videoUrl = getVideoUrl('https://childrensplace.video.WEBM');
    expect(videoUrl).toEqual(['.WEBM']);
  });
  it('should return videoFormat as false', () => {
    const videoUrl = getVideoUrl('');
    expect(videoUrl).toEqual(false);
  });
  it('should return videoFormat as null', () => {
    const videoUrl = getVideoUrl('https://childrensplace.video.png');
    expect(videoUrl).toEqual(null);
  });
});

describe('extractFloat', () => {
  it('should extract the floated for single decimal number', () => {
    const phone = extractFloat('$234.0');
    expect(phone).toBe(234);
  });
  it('should extract the floated for double decimal number', () => {
    const phone = extractFloat('$20.23');
    expect(phone).toBe(20.23);
  });
});

describe('getTranslateDateInformation', () => {
  it('should return the translated date information', () => {
    const date = new Date('2019-09-16 19:59:00');
    expect(getTranslateDateInformation(date)).toStrictEqual({
      date: 16,
      day: 'Mon',
      month: 'Sep',
      year: 2019,
    });
  });
});

describe('addDays', () => {
  it('should return the added date', () => {
    const date = new Date('2019-08-16 19:59:00');
    expect(addDays(date, 2)).toStrictEqual(new Date('2019-08-18 19:59:00'));
  });
});

describe('generateTraceId', () => {
  it('should return the generated trace id', () => {
    const expectedTraceId = generateTraceId();
    expect(expectedTraceId).toStrictEqual(expectedTraceId);
  });
});

describe('getAmznTraceId', () => {
  it('should return amazn trace id', () => {
    const expectedTraceId = getAmznTraceId();
    expect(getAmznTraceId()).toStrictEqual(expectedTraceId);
  });
});

describe('isApplePayEligible', () => {
  it('should return if apple pay is eligible', () => {
    const expectedEligibility = isApplePayEligible();
    expect(isApplePayEligible()).toStrictEqual(expectedEligibility);
  });
});

describe('getModifiedLanguageCode', () => {
  it('should extract the Language code as en_US', () => {
    expect(getModifiedLanguageCode('en')).toBe('en_US');
  });
  it('should extract the Language code as es_ES', () => {
    expect(getModifiedLanguageCode('es')).toBe('es_ES');
  });
  it('should extract the Language code as fr_FR', () => {
    expect(getModifiedLanguageCode('fr')).toBe('fr_FR');
  });
});

describe('getAddressFromPlace', () => {
  it('should return the initial address if address_components is undefined', () => {
    const address = getAddressFromPlace({}, '');
    expect(address.streetNumber).toBe('');
  });

  it('should return streetNumber correctly', () => {
    const address = getAddressFromPlace(
      {
        address_components: [
          {
            types: ['street_number'],
            short_name: '1000',
          },
          {
            types: ['route'],
            long_name: 'test',
          },
        ],
      },
      ''
    );
    expect(address.street).toBe('1000 test');
  });
});

describe('getStoreHours', () => {
  const { hours } = storesMock;
  const labels = {
    lbl_storelanding_opensAt: 'opens at',
    lbl_storelanding_openInterval: 'open until',
  };
  it('should return opens until toTime', () => {
    const storeTime = getStoreHours(hours, labels, new Date(dateString));
    expect(storeTime).toContain('open until 8 pm');
  });
  it('should return opens at fromTime', () => {
    const storeTime = getStoreHours(hours, labels, new Date('2019-09-17 21:00:00'));
    expect(storeTime).toContain('opens at 10 am');
  });
});
describe('getStoreHoursClosingTiming', () => {
  const { hours } = storesMock;
  const labels = {
    lbl_storeldetails_closing: 'Closes in',
    lbl_storelanding_openInterval: 'Open until',
    lbl_storelanding_opensAt: 'Opens tomorrow at ',
    lbl_storeldetails_closed: 'Closed',
  };

  it('should return opens until toTime', () => {
    const { storeLabel } = getStoreHoursClosingTiming(hours, labels, new Date(dateString));
    expect(storeLabel).toContain('Closes in');
  });
  it('should return opens at fromTime', () => {
    const { storeLabel } = getStoreHoursClosingTiming(
      hours,
      labels,
      new Date('2019-09-17 21:00:00')
    );
    expect(storeLabel).toContain('Opens tomorrow at');
  });
});

describe('getformated', () => {
  it('should return formated date', () => {
    const orderDate = '2019-10-11';
    const expectedFormatDate = getFormatedOrderDate(orderDate);
    const { language } = getAPIConfig();
    const { month, date, year } = getTranslateDateInformation(
      orderDate,
      language,
      {
        weekday: 'short',
      },
      {
        month: 'long',
      }
    );
    expect(expectedFormatDate).toEqual(`${month} ${date}, ${year}`);
  });
});

describe('getOrderGroupLabelAndMessage', () => {
  const labels = {
    lbl_orders_shippedOn: 'lbl_orders_shippedOn',
    lbl_orders_pickedUpOn: 'lbl_orders_pickedUpOn',
    lbl_orders_orderInProcess: 'lbl_orders_orderInProcess',
    lbl_orders_OrderReceived: 'lbl_orders_OrderReceived',
    lbl_orders_orderIsReadyForPickup: 'lbl_orders_orderIsReadyForPickup',
    lbl_orders_processing: 'lbl_orders_processing',
    lbl_orders_orderCancelMessage: 'lbl_orders_orderCancelMessage',
    lbl_orders_pleasePickupBy: 'lbl_orders_pleasePickupBy',
  };

  it('should return correct label and message if Order status is shipped', () => {
    const orderProps = {
      status: constants.STATUS_CONSTANTS.ORDER_SHIPPED,
      shippedDate: '2019-10-09',
      ordersLabels: labels,
    };

    const { language } = getAPIConfig();
    const { month, date, year } = getTranslateDateInformation(
      orderProps.shippedDate,
      language,
      {
        weekday: 'short',
      },
      {
        month: 'long',
      }
    );

    const label = labels.lbl_orders_shippedOn;
    const labelAndMessage = getOrderGroupLabelAndMessage(orderProps);
    expect(labelAndMessage.message).toBe(`${month} ${date}, ${year}`);
    expect(labelAndMessage.label).toBe(label);
  });

  it('should return correct label and message if Order status is Picked', () => {
    const orderProps = {
      status: constants.STATUS_CONSTANTS.ITEMS_PICKED_UP,
      pickedUpDate: '2019-10-10',
      ordersLabels: labels,
    };

    const { language } = getAPIConfig();
    const { month, date, year } = getTranslateDateInformation(
      orderProps.pickedUpDate,
      language,
      {
        weekday: 'short',
      },
      {
        month: 'long',
      }
    );

    const label = labels.lbl_orders_pickedUpOn;
    const labelAndMessage = getOrderGroupLabelAndMessage(orderProps);
    expect(labelAndMessage.message).toBe(`${month} ${date}, ${year}`);
    expect(labelAndMessage.label).toBe(label);
  });

  it('should return correct label and message if Bopis Order in Process', () => {
    const orderProps = {
      status: constants.STATUS_CONSTANTS.ORDER_IN_PROCESS,
      pickedUpDate: '2019-10-11',
      ordersLabels: labels,
      isBopisOrder: true,
    };

    const label = labels.lbl_orders_orderInProcess;
    const message = labels.lbl_orders_orderIsReadyForPickup;
    const labelAndMessage = getOrderGroupLabelAndMessage(orderProps);
    expect(labelAndMessage.message).toBe(message);
    expect(labelAndMessage.label).toBe(label);
  });

  it('should return correct label and message if Order in Process', () => {
    const orderProps = {
      status: constants.STATUS_CONSTANTS.ORDER_IN_PROCESS,
      pickedUpDate: '2019-10-12',
      ordersLabels: labels,
      isBopisOrder: false,
    };

    const label = labels.lbl_orders_OrderReceived;
    const message = labels.lbl_orders_processing;
    const labelAndMessage = getOrderGroupLabelAndMessage(orderProps);
    expect(labelAndMessage.message).toBe(message);
    expect(labelAndMessage.label).toBe(label);
  });

  it('should return correct label and message if Order is cancelled', () => {
    const orderProps = {
      status: constants.STATUS_CONSTANTS.ORDER_CANCELED,
      pickedUpDate: '2019-10-13',
      ordersLabels: labels,
      isBopisOrder: true,
    };

    const label = '';
    const message = labels.lbl_orders_orderCancelMessage;
    const labelAndMessage = getOrderGroupLabelAndMessage(orderProps);
    expect(labelAndMessage.message).toBe(message);
    expect(labelAndMessage.label).toBe(label);
  });

  it('should return correct label and message if Order Item is received', () => {
    const orderProps = {
      status: constants.STATUS_CONSTANTS.ITEMS_RECEIVED,
      pickedUpDate: '2019-10-14',
      ordersLabels: labels,
      isBopisOrder: true,
    };

    const label = labels.lbl_orders_orderInProcess;
    const message = labels.lbl_orders_orderIsReadyForPickup;
    const labelAndMessage = getOrderGroupLabelAndMessage(orderProps);
    expect(labelAndMessage.message).toBe(message);
    expect(labelAndMessage.label).toBe(label);
  });

  it('should return correct label and message if Order is ready for pickup', () => {
    const orderProps = {
      status: constants.STATUS_CONSTANTS.ITEMS_READY_FOR_PICKUP,
      pickedUpDate: '2019-10-15',
      pickUpExpirationDate: '2019-10-10',
      ordersLabels: labels,
      isBopisOrder: true,
    };

    const { language } = getAPIConfig();
    const { month, date, year } = getTranslateDateInformation(
      orderProps.pickUpExpirationDate,
      language,
      {
        weekday: 'short',
      },
      {
        month: 'long',
      }
    );

    const label = labels.lbl_orders_pleasePickupBy;
    const labelAndMessage = getOrderGroupLabelAndMessage(orderProps);
    expect(labelAndMessage.message).toBe(`${month} ${date}, ${year}`);
    expect(labelAndMessage.label).toBe(label);
  });

  it('should return null values if status is not matched', () => {
    const orderProps = {
      status: '',
      pickedUpDate: '2019-10-16',
      ordersLabels: labels,
    };
    const labelAndMessage = getOrderGroupLabelAndMessage(orderProps);
    expect(labelAndMessage.message).toBe(null);
    expect(labelAndMessage.label).toBe(null);
  });
});

describe('parseUTCDate', () => {
  it('default', () => {
    const returnDateValue = parseUTCDate('2019-10-15 20:00:00');
    expect(returnDateValue).toStrictEqual(new Date('2019-10-15T20:00:00.000Z'));
  });
});

describe('validateDiffInDaysNotification', () => {
  it('return true if order date is falls with in limit', () => {
    // differenceInDays.mockImplementation(() => 1);
    const returnValue = validateDiffInDaysNotification(null, 30);
    expect(returnValue).toEqual(true);
  });

  it('return false if order date is not falls with in limit', () => {
    // differenceInDays.mockImplementation(() => 16);
    const returnValue = validateDiffInDaysNotification('Oct 16, 2019', 15);
    expect(returnValue).toEqual(false);
  });
});

describe('getOrderStatusForNotification', () => {
  it('return Order Status', () => {
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_RECEIVED)).toEqual(
      'lbl_orders_statusOrderReceived'
    );
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_PROCESSING)).toEqual(
      'lbl_global_yourOrderIsProcessing'
    );
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_SHIPPED)).toEqual(
      'lbl_orders_statusOrderShipped'
    );
    expect(
      getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_PARTIALLY_SHIPPED)
    ).toEqual('lbl_orders_statusOrderPartiallyShipped');
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_CANCELED)).toEqual(
      'lbl_orders_statusOrderCancelled'
    );
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ITEMS_RECEIVED)).toEqual(
      'lbl_orders_statusOrderReceived'
    );
    expect(
      getOrderStatusForNotification(constants.STATUS_CONSTANTS.ITEMS_READY_FOR_PICKUP)
    ).toEqual('lbl_orders_statusItemsReadyForPickup');
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ITEMS_PICKED_UP)).toEqual(
      'lbl_orders_statusItemsPickedUp'
    );
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_EXPIRED)).toEqual(
      'lbl_orders_statusOrderExpired'
    );
    expect(
      getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_USER_CALL_NEEDED)
    ).toEqual('lbl_orders_statusOrderReceived');
    expect(
      getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_PROCESSING_AT_FACILITY)
    ).toEqual('lbl_global_yourOrderIsBeingProcessed');
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.EXPIRED_AND_REFUNDED)).toEqual(
      'lbl_global_yourOrderHasBeenExpiredRefunded'
    );
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_CANCELLED)).toEqual(
      'lbl_orders_statusOrderCancelled'
    );
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.LBL_CallNeeded)).toEqual(
      'lbl_orders_statusOrderReceived'
    );
    expect(
      getOrderStatusForNotification(constants.STATUS_CONSTANTS.SUCCESSFULLY_PICKED_UP)
    ).toEqual('lbl_orders_statusItemsPickedUp');
    expect(getOrderStatusForNotification(constants.STATUS_CONSTANTS.ORDER_IN_PROCESS)).toEqual(
      'lbl_orders_statusOrderReceived'
    );
  });
  it('status Not Matched', () => {
    const returnValue = getOrderStatusForNotification('Not Matched');
    expect(returnValue).toEqual(returnValue);
  });
});

describe('openWindow', () => {
  const sampleUrl = 'https://example.com';
  it('should open given url in a new window if _blank is passed', () => {
    jest.spyOn(window, 'open').mockImplementation(() => jest.fn());
    openWindow(sampleUrl);
    expect(window.open).toHaveBeenCalled();
    expect(window.open).toBeCalledWith(sampleUrl, '_blank', 'noopener');
  });
  it('should open given url in same window if _self is passed', () => {
    jest.spyOn(window, 'open').mockImplementation(() => jest.fn());
    openWindow(sampleUrl, '_self');
    expect(window.open).toHaveBeenCalled();
    expect(window.open).toBeCalledWith(sampleUrl, '_self', '');
  });
});

describe('formatProductData', () => {
  const productName = 'Girl School Uniform';
  const productsData = {
    ratingsProductId: '2100622',
    generalProductId: '3000935_IV',
    name: productName,
    colorFitsSizesMap: [
      {
        color: {
          name: 'TIDAL',
          family: 'BLUE',
        },
        name: null,
        colorProductId: '1281598',
        colorDisplayId: '3000935_IV',
        categoryEntity: 'Girl:School1 Uniforms',
      },
    ],
  };

  const productsArray = [
    {
      ratingsProductId: '2100622',
      generalProductId: '3000935_IV',
      name: productName,
      colorFitsSizesMap: [
        {
          color: {
            name: 'TIDAL',
            family: 'BLUE',
          },
          name: null,
          colorProductId: '1281598',
          colorDisplayId: '3000935_IV',
          categoryEntity: 'Girl:School1 Uniforms',
        },
      ],
    },
  ];

  const formattedProductsArray = [
    {
      colorId: '3000935_IV',
      color: ['TIDAL'],
      id: '3000935',
      listPrice: undefined,
      price: undefined,
      extPrice: undefined,
      features,
      rating: undefined,
      reviews: undefined,
      plpClick: false,
      position: 1,
      searchClick: false,
      recsProductId: '3000935_IV',
      recsPageType: '',
      name: productName,
      paidPrice: undefined,
      pricingState,
      quantity: undefined,
      size: undefined,
      type: undefined,
      outfitId: '',
      recsType: '',
    },
  ];
  const formattedProductsArraySBP = [
    {
      colorId: '3000935_IV',
      color: ['TIDAL'],
      id: '3000935',
      listPrice: undefined,
      price: undefined,
      extPrice: undefined,
      features,
      rating: undefined,
      reviews: undefined,
      plpClick: false,
      position: 1,
      searchClick: false,
      recsProductId: '3000935_IV',
      recsPageType: 'outfit',
      name: productName,
      paidPrice: undefined,
      pricingState,
      quantity: undefined,
      size: undefined,
      type: undefined,
      outfitId: true,
      recsType: true,
      sbp: true,
    },
  ];
  const formattedProducts = [
    {
      colorId: '3000935_IV',
      color: ['TIDAL'],
      id: '3000935',
      listPrice: undefined,
      price: undefined,
      extPrice: undefined,
      features,
      rating: undefined,
      reviews: undefined,
      plpClick: false,
      position: NaN,
      searchClick: false,
      recsProductId: '3000935_IV',
      recsPageType: '',
      outfitId: '',
      recsType: '',
      name: productName,
      paidPrice: undefined,
      pricingState,
      quantity: undefined,
      size: undefined,
      type: undefined,
    },
  ];
  const formattedProductsSBP = [
    {
      colorId: '3000935_IV',
      color: ['TIDAL'],
      id: '3000935',
      listPrice: undefined,
      price: undefined,
      extPrice: undefined,
      features,
      rating: undefined,
      reviews: undefined,
      plpClick: false,
      position: NaN,
      searchClick: false,
      recsProductId: '3000935_IV',
      recsPageType: 'outfit',
      name: productName,
      paidPrice: undefined,
      pricingState,
      quantity: undefined,
      size: undefined,
      type: undefined,
      outfitId: true,
      recsType: true,
      sbp: true,
    },
  ];
  it('should return formattedProducts for Array of products', () => {
    const products = formatProductsData(productsArray);
    expect(products).toEqual(formattedProductsArray);
  });

  it('should return formattedProducts for product object', () => {
    const products = formatProductsData(productsData);
    expect(products).toEqual(formattedProducts);
  });

  it('should return formattedProducts for Array of products for SBP', () => {
    const products = formatProductsData(productsArray, { data: null }, { getParam: () => true });
    expect(products).toEqual(formattedProductsArraySBP);
  });

  it('should return formattedProducts for product object for SBP', () => {
    const products = formatProductsData(productsData, { data: null }, { getParam: () => true });
    expect(products).toEqual(formattedProductsSBP);
  });
});

describe('isCookiePresent', () => {
  beforeAll(() => {
    Object.defineProperty(global.document, 'cookie', {
      value: '',
      writable: true,
    });
  });
  it('should returns true if cookie present', () => {
    global.document.cookie = 'WC_PERSISTENT=acd;';
    expect(isCookiePresent('WC_PERSISTENT')).toBeTruthy();
  });

  it('should returns true if cookie present and patern matches', () => {
    global.document.cookie = 'WC_USER_ACTIVITY_1234566=acd;';
    expect(isCookiePresent('WC_USER_ACTIVITY_')).toBeTruthy();
  });

  it('should returns false if cookie not present', () => {
    global.document.cookie = 'WC_PERSISTENT=acd;';
    expect(isCookiePresent('WC_USER_ACTIVITY_')).toBeFalsy();
  });
});

describe('checkBossBopisEnabled', () => {
  let sessionState = {};
  beforeAll(() => {
    sessionState = {
      siteDetails: {
        IS_BOSS_ENABLED: true,
        BOPIS_ENABLED: true,
        BOSS_ENABLED_STATES: 'NJ|AY|AS',
        BOPIS_ENABLED_STATES: 'NJ|NS|AY',
      },
      otherBrandSiteDetails: {
        IS_BOSS_ENABLED: true,
        BOPIS_ENABLED: true,
        BOSS_ENABLED_STATES: 'NJ|NY|AK|VR',
        BOPIS_ENABLED_STATES: 'NJ|NY|AS|VR',
      },
    };
  });
  it('should return boss flag as true or false for both brands', () => {
    const stateCookie = 'NJ';
    const checkForType = checkBossBopisEnabled(sessionState, stateCookie, true);
    expect(checkForType('BOSS')).toBe(true);
  });
  it('should return boss flag as true or false for both brands', () => {
    const stateCookie = 'NJ';
    const checkForType = checkBossBopisEnabled(sessionState, stateCookie, false);
    expect(checkForType('BOSS')).toBe(true);
  });
  it('should return bopis flag as true or false for both brands', () => {
    const stateCookie = 'NJ';
    const checkForType = checkBossBopisEnabled(sessionState, stateCookie, true);
    expect(checkForType('BOPIS')).toBe(true);
  });
  it('should return bopis flag as true or false for both brands', () => {
    const stateCookie = 'NJ';
    const checkForType = checkBossBopisEnabled(sessionState, stateCookie, false);
    expect(checkForType('BOPIS')).toBe(true);
  });
});

describe('setBossBopisEnabled', () => {
  let sessionState = {};
  beforeAll(() => {
    sessionState = {
      siteDetails: {
        IS_BOSS_ENABLED: true,
        BOPIS_ENABLED: true,
        BOSS_ENABLED_STATES: 'NJ|NY|AK',
        BOPIS_ENABLED_STATES: 'NJ|NY|AS',
      },
      otherBrandSiteDetails: {
        IS_BOSS_ENABLED: true,
        BOPIS_ENABLED: true,
        BOSS_ENABLED_STATES: 'NJ|NY|AK|VR',
        BOPIS_ENABLED_STATES: 'NJ|NY|AS|VR',
      },
    };
  });

  it('should return boss/bopis flag as true for both brands', () => {
    const stateCookie = 'NJ';
    expect(setBossBopisEnabled(stateCookie, sessionState)).toEqual({
      isBOSSEnabled_TCP: true,
      isBOSSEnabled_GYM: true,
      isBOPISEnabled_TCP: true,
      isBOPISEnabled_GYM: true,
    });
  });

  it('should return boss flag as true for both brands', () => {
    const stateCookie = 'AK';
    expect(setBossBopisEnabled(stateCookie, sessionState)).toEqual({
      isBOSSEnabled_TCP: true,
      isBOSSEnabled_GYM: true,
      isBOPISEnabled_TCP: false,
      isBOPISEnabled_GYM: false,
    });
  });

  it('should return bopis flag as true for both brands', () => {
    const stateCookie = 'AS';
    expect(setBossBopisEnabled(stateCookie, sessionState)).toEqual({
      isBOSSEnabled_TCP: false,
      isBOSSEnabled_GYM: false,
      isBOPISEnabled_TCP: true,
      isBOPISEnabled_GYM: true,
    });
  });

  it('should return boss/bopis flag as true for GYM brands', () => {
    const stateCookie = 'VR';
    expect(setBossBopisEnabled(stateCookie, sessionState)).toEqual({
      isBOSSEnabled_TCP: false,
      isBOSSEnabled_GYM: true,
      isBOPISEnabled_TCP: false,
      isBOPISEnabled_GYM: true,
    });
  });

  it('should return deepLink url', () => {
    const uriSchemeIdentifier = '/content/sms';
    const deepLinkURL = `${uriSchemeIdentifier}/?deepLinkURL=${window.location.href}`;
    expect(getURIForDeepLink(uriSchemeIdentifier)).toEqual(deepLinkURL);
  });

  it('should return clientTimeout as 30000', () => {
    expect(getApiTimeOut({ clientTimeout: 30000, serverTimeout: 15000 })).toEqual(30000);
  });
});
describe('should check for regularProducts', () => {
  it('should return  truthy flag for regularProducts', () => {
    expect(regularProducts(-1, null)).toBeTruthy();
  });
  it('should return  falsy  flag for regularProducts', () => {
    expect(regularProducts(1, null)).toBeFalsy();
  });
});
describe('should check for OOS products', () => {
  const multipackProduct = [
    {
      colorId: '3000935_IV',
      color: ['TIDAL'],
      id: '3000935',
      listPrice: undefined,
      price: undefined,
      extPrice: undefined,
      features: '',
      rating: undefined,
      reviews: undefined,
      plpClick: false,
      position: 1,
      searchClick: false,
      recsProductId: undefined,
      recsPageType: '',
      name: '',
      paidPrice: undefined,
      pricingState: '',
      quantity: undefined,
      size: undefined,
      type: undefined,
      outfitId: '',
      recsType: '',
      v_qty: 10,
    },
  ];

  it('should return null when multipack product is missing', () => {
    expect(checkOOS([])).toBeFalsy();
  });

  it('should return available flag if product is available', () => {
    expect(checkOOS(multipackProduct)).toEqual(['available']);
  });
  it('should return unavailable flag if product is not available', () => {
    const product = [
      {
        outfitId: '',
        recsType: '',
        v_qty: 0,
      },
    ];
    expect(checkOOS(product)).toEqual(['unavailable']);
  });
});

describe('it should return primary image', () => {
  it('should return images', () => {
    const props = {
      quickViewLabels: {},
      className: '',
      currentColorEntry: {
        color: {
          name: 'SMOKEB10',
          imagePath:
            'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/swatches/2100606_1137.jpg',
          family: 'GRAY',
        },
        pdpUrl: '/p/2100606_1137',
        colorProductId: '1118006',
        colorDisplayId: '2100606_1137',

        imageName: '2100606_1137',
        favoritedCount: 991,
        maxAvailable: 5435,
        maxAvailableBoss: 0,
        hasFits: false,
        miscInfo: {
          isBopisEligible: false,
          isBossEligible: false,
          badge1: {
            matchBadge: false,
          },
          badge2: '25% OFF',
          keepAlive: false,
        },
        fits: [
          {
            fitNameVal: '',
            isDefault: true,
            maxAvailable: 1.7976931348623157e308,
            sizes: [
              {
                sizeName: 'XS (4)',
                skuId: '1118990',
                listPrice: 24.95,
                offerPrice: 18.71,
                maxAvailable: 218,
                variantId: '00191755242934',
                variantNo: '2100606001',
                position: 0,
              },
              {
                sizeName: 'S (5/6)',
                skuId: '1119730',
                listPrice: 24.95,
                offerPrice: 18.71,
                maxAvailable: 1065,
                variantId: '00191755242941',
                variantNo: '2100606002',
                position: 1,
              },
              {
                sizeName: 'M (7/8)',
                skuId: '1118372',
                listPrice: 24.95,
                offerPrice: 18.71,
                maxAvailable: 1730,
                variantId: '00191755242958',
                variantNo: '2100606003',
                position: 2,
              },
              {
                sizeName: 'L (10/12)',
                skuId: '1118586',
                listPrice: 24.95,
                offerPrice: 18.71,
                maxAvailable: 1417,
                variantId: '00191755242965',
                variantNo: '2100606004',
                position: 3,
              },
              {
                sizeName: 'XL (14)',
                skuId: '1118690',
                listPrice: 24.95,
                offerPrice: 18.71,
                maxAvailable: 630,
                variantId: '00191755242972',
                variantNo: '2100606005',
                position: 4,
              },
              {
                sizeName: 'XXL (16)',
                skuId: '1118793',
                listPrice: 24.95,
                offerPrice: 18.71,
                maxAvailable: 375,
                variantId: '00191755242989',
                variantNo: '2100606006',
                position: 5,
              },
            ],
          },
        ],
        listPrice: 24.95,
        offerPrice: 18.71,
      },
      productInfo: {
        ratingsProductId: '2100606',
        generalProductId: '2100606_IV',
        categoryId: '47503>47539',
        name: 'Boys Uniform V Neck Sweater',
        pdpUrl: '/p/2100606_IV',
        shortDescription: 'School-approved style for your super cool dude!',
        longDescription:
          '<li>Made of 100% cotton in a sweater knit</li><li>Rib-knit V neck, sleeve cuffs and hem</li><li>Fully-fashioned sleeves for ease of movement</li><li>Pre-washed for added softness and to reduce shrinkage</li><li>Imported</li>',
        imagesByColor: {
          SMOKEB10: {
            basicImageUrl:
              'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2100606_1137.jpg',
            extraImages: [
              {
                isOnModalImage: false,
                iconSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/125/2100606_1137.jpg',
                listingSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/380/2100606_1137.jpg',
                regularSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2100606_1136.jpg',
                bigSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2100606_1137.jpg',
                superSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2100606_1137.jpg',
              },
              {
                isOnModalImage: false,
                iconSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/125/2100606_1137-1.jpg',
                listingSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/380/2100606_1137-1.jpg',
                regularSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2100606_1137-1.jpg',
                bigSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2100606_1137-1.jpg',
                superSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2100606_1137-1.jpg',
              },
            ],
          },
          TIDAL: {
            basicImageUrl:
              'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2100609_IV.jpg',
            extraImages: [
              {
                isOnModalImage: false,
                iconSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/125/2100606_IV.jpg',
                listingSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/380/2100606_IV.jpg',
                regularSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2100606_IV.jpg',
                bigSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2100606_IV.jpg',
                superSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2100606_IV.jpg',
              },
              {
                isOnModalImage: false,
                iconSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/125/2100606_IV-1.jpg',
                listingSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/380/2100606_IV-1.jpg',
                regularSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2100606_IV-1.jpg',
                bigSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2100606_IV-1.jpg',
                superSizeImageUrl:
                  'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2100606_IV-1.jpg',
              },
            ],
          },
        },
        colorFitsSizesMap: [
          {
            color: {
              name: 'SMOKEB10',
              imagePath:
                'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/swatches/2100606_1137.jpg',
              family: 'GRAY',
            },
            pdpUrl: '/p/2100606_1137',
            colorProductId: '1118006',
            colorDisplayId: '2100606_1137',
            categoryEntity: 'Boy:School Uniforms',
            imageName: '2100606_1137',
            favoritedCount: 991,
            maxAvailable: 5435,
            maxAvailableBoss: 0,
            hasFits: false,
            miscInfo: {
              isBopisEligible: false,
              isBossEligible: false,
              badge1: {
                matchBadge: false,
              },
              badge2: '25% OFF',
              keepAlive: false,
            },
            fits: [
              {
                fitNameVal: '',
                isDefault: true,
                maxAvailable: 1.7976931348623157e308,
                sizes: [
                  {
                    sizeName: 'XS (4)',
                    skuId: '1118990',
                    listPrice: 24.95,
                    offerPrice: 18.71,
                    maxAvailable: 218,
                    variantId: '00191755242934',
                    variantNo: '2100606001',
                    position: 0,
                  },
                  {
                    sizeName: 'S (5/6)',
                    skuId: '1119730',
                    listPrice: 24.95,
                    offerPrice: 18.71,
                    maxAvailable: 1065,
                    variantId: '00191755242941',
                    variantNo: '2100606002',
                    position: 1,
                  },
                  {
                    sizeName: 'M (7/8)',
                    skuId: '1118372',
                    listPrice: 24.95,
                    offerPrice: 18.71,
                    maxAvailable: 1730,
                    variantId: '00191755242958',
                    variantNo: '2100606003',
                    position: 2,
                  },
                  {
                    sizeName: 'L (10/12)',
                    skuId: '1118586',
                    listPrice: 24.95,
                    offerPrice: 18.71,
                    maxAvailable: 1417,
                    variantId: '00191755242965',
                    variantNo: '2100606004',
                    position: 3,
                  },
                  {
                    sizeName: 'XL (14)',
                    skuId: '1118690',
                    listPrice: 24.95,
                    offerPrice: 18.71,
                    maxAvailable: 630,
                    variantId: '00191755242972',
                    variantNo: '2100606005',
                    position: 4,
                  },
                  {
                    sizeName: 'XXL (16)',
                    skuId: '1118793',
                    listPrice: 24.95,
                    offerPrice: 18.71,
                    maxAvailable: 375,
                    variantId: '00191755242989',
                    variantNo: '2100606006',
                    position: 5,
                  },
                ],
              },
            ],
            listPrice: 24.95,
            offerPrice: 18.71,
          },
        ],
        isGiftCard: false,
        colorFitSizeDisplayNames: null,
        listPrice: 24.95,
        offerPrice: 18.71,
        highListPrice: 0,
        highOfferPrice: 0,
        lowListPrice: 24.95,
        lowOfferPrice: 18.71,
        ratings: 0,
        reviewsCount: 0,
        unbxdProdId: '2100606_IV',
        alternateSizes: {},
        productId: '2100606_IV',
        promotionalMessage: '',
        promotionalPLCCMessage: '',
        long_product_title: 'Boys Uniform Long Sleeve V Neck Sweater',
        bundleProducts: [],
      },
      plpLabels: {},
      currency: '',
      priceCurrency: '',
      currencyExchange: '',
      isCanada: false,
      isHasPlcc: false,
      isInternationalShipping: false,
      colorFitsSizesMap: [
        {
          color: {
            name: 'TIDAL',
            imagePath:
              'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/swatches/2100606_IV.jpg',
            family: 'BLUE',
          },
          pdpUrl: '/p/2100606_IV',
          colorProductId: '1118007',
          colorDisplayId: '2100606_IV',
          categoryEntity: 'Boy:School Uniforms',
          imageName: '2100606_IV',
          favoritedCount: 1529,
          maxAvailable: 10376,
          maxAvailableBoss: 0,
          hasFits: false,
          miscInfo: {
            isBopisEligible: false,
            isBossEligible: false,
            badge1: {
              matchBadge: false,
            },
            badge2: '25% OFF',
            keepAlive: false,
          },
          fits: [
            {
              fitNameVal: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'XS (4)',
                  skuId: '1118991',
                  listPrice: 24.95,
                  offerPrice: 18.71,
                  maxAvailable: 191,
                  variantId: '00191755242996',
                  variantNo: '2100606007',
                  position: 0,
                },
                {
                  sizeName: 'S (5/6)',
                  skuId: '1119731',
                  listPrice: 24.95,
                  offerPrice: 18.71,
                  maxAvailable: 2238,
                  variantId: '00191755243009',
                  variantNo: '2100606008',
                  position: 1,
                },
                {
                  sizeName: 'M (7/8)',
                  skuId: '1118373',
                  listPrice: 24.95,
                  offerPrice: 18.71,
                  maxAvailable: 3493,
                  variantId: '00191755243016',
                  variantNo: '2100606009',
                  position: 2,
                },
                {
                  sizeName: 'L (10/12)',
                  skuId: '1118587',
                  listPrice: 24.95,
                  offerPrice: 18.71,
                  maxAvailable: 2641,
                  variantId: '00191755243023',
                  variantNo: '2100606010',
                  position: 3,
                },
                {
                  sizeName: 'XL (14)',
                  skuId: '1118691',
                  listPrice: 24.95,
                  offerPrice: 18.71,
                  maxAvailable: 991,
                  variantId: '00191755243030',
                  variantNo: '2100606011',
                  position: 4,
                },
                {
                  sizeName: 'XXL (16)',
                  skuId: '1118794',
                  listPrice: 24.95,
                  offerPrice: 18.71,
                  maxAvailable: 822,
                  variantId: '00191755243047',
                  variantNo: '2100606012',
                  position: 5,
                },
              ],
            },
          ],
          listPrice: 24.95,
          offerPrice: 18.71,
        },
      ],
    };

    expect(
      getPrimaryImages({
        imagesByColor: props.productInfo.imagesByColor,
        curentColorEntry: props.currentColorEntry,
        isOnModelAbTestPdp: true,
        isFullSet: true,
      })
    ).toEqual([
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2100606_1136.jpg',
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2100606_IV.jpg',
    ]);
  });
  it('should return undefined when args is missing', () => {
    expect(getPrimaryImages({})).toBeUndefined();
  });
});

describe('getDecimalPrice', () => {
  it('should return lower value from decimal values', () => {
    expect(getDecimalPrice(1.126)).toEqual(1.12);
  });
  it('should return correct value from decimal values', () => {
    expect(getDecimalPrice(1.17)).toEqual(1.17);
  });
  it('should return correct value from non-decimal values', () => {
    expect(getDecimalPrice(17)).toEqual(17);
  });
});

describe('should return correct dynamic badge quantity', () => {
  it('should return correct quantity  when styleType is correct', () => {
    expect(getDynamicBadgeQty('0002', 10, { dynamicBadgeMPACKEnabled: true })).toEqual(10);
  });
  it('should return quantity as zero when styleType is different', () => {
    expect(getDynamicBadgeQty('0004', 10, { dynamicBadgeMPACKEnabled: true })).toEqual(0);
  });
});

describe('should return badge configs', () => {
  it('should return correct badge tranformation for mpack', () => {
    expect(badgeTranformationPrefix('0002')).toEqual('l_ecom:assets:static:badge:pack');
  });
  it('should return correct badge tranformation for set', () => {
    expect(badgeTranformationPrefix('0003')).toEqual('l_ecom:assets:static:badge:set');
  });
});
describe('get Order Package Status', () => {
  const shipmentData = {
    trackingDetails: [
      {
        trackingId: '123',
        shipmentStatus: 'Delivered',
      },
    ],
  };
  const trackingNumber = '123';
  it('should return Order Package Status', () => {
    expect(getOrderPackageStatus(trackingNumber, shipmentData)).toEqual('Delivered');
  });
});

describe('get Grouped Order Items', () => {
  const orderItems = [
    {
      orderStatus: 'Delivered',
      trackingUrl: '/test',
      trackingNumber: '4203115434221146002871291',
      trackingInfo: [
        {
          status: 'Order Shipped',
          trackingNbr: '4203115434221146002871291',
          trackingUrl: '/test',
          shipDate: '3/17/21 00:00',
          quantity: 2,
        },
      ],
    },
  ];
  const shipmentData = {
    trackingDetails: [
      {
        trackingId: '142031154342211460028712913',
        shipmentStatus: 'Delivered',
      },
    ],
  };

  it('get Grouped Order Items', () => {
    expect(getGroupedOrderItems(orderItems, shipmentData).length).toEqual(1);
  });
});

describe('  getTotalItemsCountInOrder', () => {
  const orderItems = [{ quantity: 5 }, { quantity: 5 }, { quantity: 1 }, { quantity: 0 }];
  const totalOrderItemCount = 11;
  it('should return 11 order count', () => {
    expect(getTotalItemsCountInOrder(orderItems)).toEqual(totalOrderItemCount);
  });

  it('should return 0 order count', () => {
    expect(getTotalItemsCountInOrder([])).toEqual(0);
  });

  it('should return 0 order count', () => {
    expect(getTotalItemsCountInOrder([{ quantity: 0 }])).toEqual(0);
  });
});

describe('nullCheck', () => {
  const obj = {
    one: {
      two: 'two',
    },
  };
  it('should return null ', () => {
    expect(nullCheck(obj, ['three'])).toBeUndefined();
  });
  it('should return correct value', () => {
    expect(nullCheck(obj, ['one', 'two'])).toEqual('two');
  });
});

describe('formatYmd', () => {
  const ymdFormattedDate = '1969-12-31';
  // date-fns format having issue in mockConstructor
  it.skip('should format the date correctly in yyyy-MM-dd', () => {
    const date = new Date(`${ymdFormattedDate}T00:00:00`);
    expect(formatYmd(date)).toEqual(ymdFormattedDate);
  });
});

describe('getMaxDateValue', () => {
  const ymdFormattedDate = '2021-04-12';
  // date-fns format having issue in mockConstructor
  it.skip('should format the maximum date correctly in yyyy-MM-dd', () => {
    const datesValue = '2021-04-12 | 2021-01-13';
    expect(getMaxDateValue(datesValue)).toEqual(ymdFormattedDate);
  });
});

describe('getShipmentCount', () => {
  const items = [
    { orderStatus: 'In-Progress' },
    { orderStatus: 'In Transit' },
    { orderStatus: 'Delivered' },
  ];
  it('should return 2 as on is in progress', () => {
    expect(getShipmentCount(items)).toEqual(2);
  });
});

describe('getImageData', () => {
  const imageUrl1 = getVideoUrl('https://childrensplace.video.png');
  const imageUrl2 = getVideoUrl('https://childrensplace.video.mp4');
  const badgeTranformation = 'g_west,w_0.22,fl_relative';
  const IMG_DATA = {
    IMG_DATA_PLP: {
      imgConfig: ['t_plp_img_m', 't_plp_img_t', 't_plp_img_d'],
      badgeConfig: [badgeTranformation, badgeTranformation, badgeTranformation],
    },
    VID_DATA_PLP: {
      imgConfig: ['t_plp_vid_m', 't_plp_vid_t', 't_plp_vid_d'],
    },
    VID_DATA_PLP_WEBP: {
      imgConfig: ['e_loop,t_plp_webp_m', 'e_loop,t_plp_webp_t', 'e_loop,t_plp_webp_d'],
    },
  };
  it('should return VID_DATA_PLP_WEBP if imageUrl is containing video', () => {
    expect(getImageData(imageUrl1)).toEqual(IMG_DATA.IMG_DATA_PLP);
    expect(getImageData(imageUrl2)).toEqual(IMG_DATA.VID_DATA_PLP_WEBP);
  });
});
