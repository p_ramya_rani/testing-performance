// 9fbef606107a605d69c0edbcd8029e5d 
import { getAPIConfig } from '@tcp/core/src/utils';
import { executeExternalAPICall } from '@tcp/core/src/services/handler';
import constants from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.constants';

const { GOOGLE_SEARCH_API_ENDPOINT } = constants;

export const getAddressLocationInfoGmaps = async address => {
  const googleSearchAPIKey = getAPIConfig().googleMapsApiKey;
  const apiUrl = `${GOOGLE_SEARCH_API_ENDPOINT}${address}&inputtype=textquery&fields=geometry&key=${googleSearchAPIKey}`;

  const payload = {
    webService: {
      URI: apiUrl,
      method: 'GET',
    },
  };

  return executeExternalAPICall(payload).then(res => {
    try {
      const { lat, lng } = res.body.candidates[0].geometry.location;
      return {
        lat,
        lng,
        country: 'US',
      };
    } catch (error) {
      return error;
    }
  });
};

export default getAddressLocationInfoGmaps;
