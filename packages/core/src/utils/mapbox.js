// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils';
import { executeExternalAPICall } from '@tcp/core/src/services/handler';
import { readCookie } from '@tcp/core/src/utils/cookie.util';

const logger = require('@tcp/core/src/utils/loggerInstance');

const FILTERED_ACCURACY = ['intersection', 'street'];

async function getProximity() {
  let longLat = '';
  const proximity = await readCookie('tcpGeoLoc');
  if (proximity) {
    const longLatArr = proximity.split('|');
    longLat = `${longLatArr[1]},${longLatArr[0]}`;
  }
  // return "-74.0651,40.7808";
  return longLat;
}

export async function getApiUrl(data, params) {
  const { componentRestrictions, isStoresSearchForm, mapboxTypesParam } = params;
  let apiURL = `${getAPIConfig().mapBoxApiURL}/${data}.json?access_token=${
    getAPIConfig().mapBoxApiKey
  }`;
  const proximity = await getProximity();
  if (proximity) {
    apiURL = `${apiURL}&proximity=${proximity}`;
  }
  if (mapboxTypesParam) {
    apiURL = `${apiURL}&types=${mapboxTypesParam}`;
  }
  if (!isStoresSearchForm) {
    apiURL = `${apiURL}&autocomplete=true`;
    const country = componentRestrictions ? componentRestrictions.country[0] : 'US,CA';
    if (country !== '') {
      apiURL = `${apiURL}&country=${country}`;
    }
  }
  return apiURL;
}

function getPredictionValues(prediction, i) {
  const formattedData = {};
  if (prediction.context[i].id.includes('postcode')) {
    formattedData.zip = prediction.context[i].text;
  }
  if (prediction.context[i].id.includes('region') && prediction.context[i].short_code) {
    const tempState = prediction.context[i].short_code.split('-');
    if (tempState[1]) {
      formattedData.state = tempState[1].trim();
    }
  }
  if (prediction.context[i].id.includes('country')) {
    formattedData.country = prediction.context[i].text;
  }
  if (prediction.context[i].id.includes('locality') || prediction.context[i].id.includes('place')) {
    formattedData.city = prediction.context[i].text;
  }
  return formattedData;
}

export const formatPredictionValues = (prediction) => {
  let predictionData = {
    id: prediction.id,
    place_name: prediction.place_name,
  };
  if (prediction.properties && prediction.properties.address) {
    predictionData.addressline = prediction.properties.address;
  } else {
    predictionData.addressline = prediction.text;
    if (prediction.address) {
      predictionData.addressline = `${prediction.address} ${predictionData.addressline}`;
    }
  }
  if (prediction.geometry) {
    predictionData.coordinates = prediction.geometry.coordinates;
  }
  if (prediction.context) {
    for (let i = 0; i < prediction.context.length; i += 1) {
      if (prediction.context[i]) {
        const formattedData = getPredictionValues(prediction, i);
        predictionData = { ...predictionData, ...formattedData };
      }
    }
  }
  return predictionData;
};

const getFilteredResponse = (features) => {
  try {
    return features.filter((feature) => !FILTERED_ACCURACY.includes(feature.properties.accuracy));
  } catch (err) {
    logger.error('getFilteredResponse: Error in filtering mapbox response ', err);
    return features;
  }
};

export const fetchData = async (data, props) => {
  const params = {
    componentRestrictions: props.componentRestrictions,
    isStoreSearch: false,
    mapboxTypesParam: props.mapboxAutocompleteTypesParam,
  };
  const apiUrl = await getApiUrl(data, params);

  const payload = {
    webService: {
      URI: apiUrl,
      method: 'GET',
    },
  };

  return executeExternalAPICall(payload)
    .then((res) => res.body)
    .then(
      (result) => {
        if (result.features) {
          return getFilteredResponse(result.features);
        }
        return [];
      },
      (error) => {
        logger.error('Unable to fetch data from mapbox', error);
        return [];
      }
    );
};
