// 9fbef606107a605d69c0edbcd8029e5d 
import { getAPIConfig } from '@tcp/core/src/utils/utils';
import { executeExternalAPICall } from '@tcp/core/src/services/handler';

const logger = require('@tcp/core/src/utils/loggerInstance');

export const getAddressLocationInfo = async address => {
  const apiUrl = `${getAPIConfig().mapBoxApiURL}/${address}.json?access_token=${
    getAPIConfig().mapBoxApiKey
  }`;

  const payload = {
    webService: {
      URI: apiUrl,
      method: 'GET',
    },
  };

  return executeExternalAPICall(payload)
    .then(res => res.body)
    .then(
      async result => {
        let storeDataObject = {};
        if (
          result.features !== undefined &&
          result.features.length > 0 &&
          result.features[0].geometry !== undefined &&
          result.features[0].geometry.coordinates !== undefined &&
          result.features[0].geometry.coordinates.length > 0
        ) {
          const [longitude, latitude] = result.features[0].geometry.coordinates;
          let countryCode = '';
          for (let i = 0; i < result.features[0].context.length; i += 1) {
            if (result.features[0].context[i].id.includes('country')) {
              countryCode = result.features[0].context[i].short_code.toUpperCase();
            }
          }
          storeDataObject = {
            lat: latitude,
            lng: longitude,
            country: countryCode,
          };
        }
        return storeDataObject;
      },
      error => {
        logger.error('Unable to fetch data from mapbox', error);
      }
    );
};

export default getAddressLocationInfo;
