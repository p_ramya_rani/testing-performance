/* eslint-disable max-lines */

/* eslint-disable global-require */
/* eslint-disable import/no-unresolved */
// 9fbef606107a605d69c0edbcd8029e5d
import { NavigationActions, StackActions } from 'react-navigation';
import { Dimensions, Linking, Platform, PixelRatio, StyleSheet } from 'react-native';
import IovationModule from '@tcp/core/src/components/common/atoms/IovationModule/NativeIovationModule.native';
import uniqBy from 'lodash/uniqBy';
import isEmpty from 'lodash/isEmpty';
import isFinite from 'lodash/isFinite';
import BuildConfig from 'react-native-build-config';
import DefaultPreference from 'react-native-default-preference';
import UserAgent from 'react-native-user-agent';
import CookieManager from 'react-native-cookies';
import get from 'lodash/get';
import forOwn from 'lodash/forOwn';
import logger from '@tcp/core/src/utils/loggerInstance';
import { ROUTE_PATH } from '@tcp/core/src/config/route.config';
import AsyncStorage from '@react-native-community/async-storage';
import AkamaiBMP from 'react-native-akamaibmp';
import theme from '@tcp/core/styles/themes/TCP';
import { RECOMMENDATION_GA_KEY } from '@tcp/core/src/constants/analytics';
import DeviceInfo from 'react-native-device-info';
import { APP_TYPE } from '../../../mobileapp/src/components/common/hoc/ThemeWrapper.constants';
import {
  getAPIConfig,
  getBrand,
  configureInternalNavigationFromCMSUrl,
  appendToken,
} from './utils';
import config from '../components/common/atoms/Anchor/config.native';
import { API_CONFIG } from '../services/config';
import googleMapConstants from '../constants/googleMap.constants';
import { formatDateWithLocale } from './date-fns.util';
import { Config } from './customConfig';
import staticData from '../constants/fallback-data/bootstrap';
import { BRAND_TYPE_TCP } from '../services/api.constants';

let currentAppAPIConfig = null;
let tcpAPIConfig = null;
let gymAPIConfig = null;
let sessionId = '';

const ASYNC_CACHE_TTL = 60;

export const RICHTEXT_NAVIGATION_MAP = {
  '/pointsClaimForm': 'PointsClaimPage',
  '/bag': 'BagPage',
};

// Host name to be used for lazyload scrollview
export const LAZYLOAD_HOST_NAME = {
  HOME: 'lazyload-home',
  PLP: 'lazyload-plp',
  PDP: 'lazyload-pdp',
  ACCOUNT: 'lazyload-account',
  WALLET: 'lazyload-wallet',
};

const BrandSuffix = {
  tcp: 'TCP',
  gym: 'GYM',
  snj: 'SNJ',
};

export const isMobileApp = () => {
  return typeof navigator !== 'undefined' && navigator.product === 'ReactNative';
};

export const isServer = () => {
  return typeof window === 'undefined' && !isMobileApp();
};

export function isClient() {
  return typeof window !== 'undefined' && !isMobileApp();
}

export const filterMultiPack = (getFilterData) => {
  const newgetFilterData = getFilterData.map((obj) => {
    if (!('vId' in obj) && obj.prodpartno && obj.variantId) {
      return {
        ...obj,
        vId: `${obj.prodpartno}_${obj.variantId}`,
      };
    }
    return obj;
  });
  return uniqBy(newgetFilterData, (item) => {
    return item.vId;
  });
};

export const importGraphQLClientDynamically = (module) => {
  return new Promise((resolve, reject) => {
    switch (module) {
      case 'graphQL':
        resolve(require('../services/handler/graphQL'));
        break;
      default:
        reject();
        break;
    }
  });
};
export const importOtherGraphQLQueries = ({ query, resolve, reject }) => {
  switch (query) {
    case 'promoList':
      resolve(require('../services/handler/graphQL/queries/promoList'));
      break;
    case 'AccountNavigation':
      resolve(require('../services/handler/graphQL/queries/AccountNavigation'));
      break;
    case 'subNavigation':
      resolve(require('../services/handler/graphQL/queries/subNavigation'));
      break;
    default:
      reject();
      break;
  }
};

// eslint-disable-next-line complexity
export const importMoreGraphQLQueries = ({ query, resolve, reject }) => {
  switch (query) {
    case 'userOnboardingModule':
      resolve(require('../services/handler/graphQL/queries/userOnboardingModule'));
      break;
    case 'moduleX':
      resolve(require('../services/handler/graphQL/queries/moduleX'));
      break;
    case 'moduleXComposite':
      resolve(require('../services/handler/graphQL/queries/moduleXComposite'));
      break;
    case 'moduleA':
      resolve(require('../services/handler/graphQL/queries/moduleA'));
      break;
    case 'moduleM':
      resolve(require('../services/handler/graphQL/queries/moduleM'));
      break;
    case 'moduleN':
      resolve(require('../services/handler/graphQL/queries/moduleN'));
      break;
    case 'moduleB':
      resolve(require('../services/handler/graphQL/queries/moduleB'));
      break;
    case 'moduleJ':
      resolve(require('../services/handler/graphQL/queries/moduleJ'));
      break;
    case 'moduleQ':
      resolve(require('../services/handler/graphQL/queries/moduleQ'));
      break;
    case 'moduleT':
      resolve(require('../services/handler/graphQL/queries/moduleT'));
      break;
    case 'moduleG':
      resolve(require('../services/handler/graphQL/queries/moduleG'));
      break;
    case 'moduleE':
      resolve(require('../services/handler/graphQL/queries/moduleE'));
      break;
    case 'categoryPromo':
      resolve(require('../services/handler/graphQL/queries/categoryPromo'));
      break;
    case 'promotion':
      resolve(require('../services/handler/graphQL/queries/promotion'));
      break;
    default:
      importOtherGraphQLQueries({
        query,
        resolve,
        reject,
      });
  }
};

export const importGraphQLQueriesDynamically = (query) => {
  // TODO - disabling the complexity till we find a better approach for this on Mobile app
  // eslint-disable-next-line complexity
  return new Promise((resolve, reject) => {
    switch (query) {
      case 'footer':
        resolve(require('../services/handler/graphQL/queries/footer'));
        break;
      case 'header':
        resolve(require('../services/handler/graphQL/queries/header'));
        break;
      case 'navigation':
        resolve(require('../services/handler/graphQL/queries/navigation'));
        break;
      case 'layout':
        resolve(require('../services/handler/graphQL/queries/layout'));
        break;
      case 'labels':
        resolve(require('../services/handler/graphQL/queries/labels'));
        break;
      case 'moduleD':
        resolve(require('../services/handler/graphQL/queries/moduleD'));
        break;
      case 'moduleK':
        resolve(require('../services/handler/graphQL/queries/moduleK'));
        break;
      case 'moduleL':
        resolve(require('../services/handler/graphQL/queries/moduleL'));
        break;
      case 'xappConfig':
        // eslint-disable-next-line global-require
        resolve(require('../services/handler/graphQL/queries/xappConfig'));
        break;
      case 'divisionTabs':
        resolve(require('../services/handler/graphQL/queries/divisionTabs'));
        break;
      case 'outfitCarousel':
        resolve(require('../services/handler/graphQL/queries/outfitCarousel'));
        break;
      case 'jeans':
        resolve(require('../services/handler/graphQL/queries/jeans'));
        break;
      case 'promoContent':
        resolve(require('../services/handler/graphQL/queries/promoContent'));
        break;
      case 'essentials':
        resolve(require('../services/handler/graphQL/queries/essentials'));
        break;
      case 'moduleDynamic':
        resolve(require('../services/handler/graphQL/queries/moduleDynamic'));
        break;
      case 'module_container':
        resolve(require('../services/handler/graphQL/queries/module_container'));
        break;
      case 'moduleFont':
        resolve(require('../services/handler/graphQL/queries/moduleFont'));
        break;
      case 'lastUpdated':
        resolve(require('../services/handler/graphQL/queries/lastUpdated'));
        break;
      default:
        importMoreGraphQLQueries({
          query,
          resolve,
          reject,
        });
    }
  });
};

const discSmall = require('../../../mobileapp/src/assets/images/discover-small.png');
const masterCard = require('../../../mobileapp/src/assets/images/mastercard-small.png');
const amexCard = require('../../../mobileapp/src/assets/images/amex-small.png');
const visaSmall = require('../../../mobileapp/src/assets/images/visa-small.png');
const placeCard = require('../../../mobileapp/src/assets/images/TCP-CC-small.png');
const giftCardSmall = require('../../../mobileapp/src/assets/images/TCP-gift-small.png');
const venmoCard = require('../../../mobileapp/src/assets/images/new-venmo-bill.png');
const applepay = require('../../../mobileapp/src/assets/images/new-applepay-bill.png');
const paypal = require('../../../mobileapp/src/assets/images/new-paypal.png');
const creditCard = require('../../../mobileapp/src/assets/images/new-credit-card.png');
const applepaysmall = require('../../../mobileapp/src/assets/images/applepay-small.png');
const applepayicon = require('../../../mobileapp/src/assets/images/apple-pay-icon.png');
const afterpayIconBlackTransparent = require('../../../mobileapp/src/assets/images/afterpay-logo-black-transparent.png');
const paypalIconTransparent = require('../../../mobileapp/src/assets/images/new-paypal-transparent.png');
const applepayIconTransparent = require('../../../mobileapp/src/assets/images/apple-pay-transparent.png');
const creditCardIconTransparent = require('../../../mobileapp/src/assets/images/cc-transparent.png');
const venmoIconTransparent = require('../../../mobileapp/src/assets/images/venmo-transparent.png');

// eslint-disable-next-line complexity
export const getIconCard = (icon) => {
  switch (icon) {
    case 'disc-small':
      return discSmall;
    case 'mc-small':
      return masterCard;
    case 'amex-small':
      return amexCard;
    case 'visa-small':
      return visaSmall;
    case 'gift-card-small':
      return giftCardSmall;
    case 'place-card-small':
      return placeCard;
    case 'venmo-blue-acceptance-mark':
      return venmoCard;
    case 'applepay-bill':
      return applepay;
    case 'applepay-small':
      return applepaysmall;
    case 'apple-pay-icon':
      return applepayicon;
    case 'after-pay-icon':
      return afterpayIconBlackTransparent;
    case 'paypal-icon':
      return paypal;
    case 'credit-card':
      return creditCard;
    case 'new-paypal-transparent':
      return paypalIconTransparent;
    case 'apple-pay-transparent':
      return applepayIconTransparent;
    case 'cc-transparent':
      return creditCardIconTransparent;
    case 'venmo-transparent':
      return venmoIconTransparent;
    default:
      return visaSmall;
  }
};

export const isProduction = () => {
  return !__DEV__;
};

export const isDevelopment = () => {
  return __DEV__;
};

export const UrlHandler = (url) => {
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      Linking.openURL(url);
    }
  });
};

export const setCookie = () => {
  // TODO - work on it to set cookie from Mobile APP
  return null;
};

/**
 * This function reads cookie for mobile app
 */
export const readCookie = (key) => {
  const apiConfigObj = getAPIConfig();
  return new Promise((resolve, reject) => {
    if (apiConfigObj && apiConfigObj.domain) {
      CookieManager.get(apiConfigObj.domain)
        .then((response) => {
          const keyValue = key ? response[key] : response;
          resolve(keyValue);
        })
        .catch((e) => reject(e));
    } else {
      reject();
    }
  });
};

/**
 * @summary This is to check and do a pattern match from the cookie string if the string is present.
 * @param {string} key
 * @returns {Promise} whether cookie key is present or not
 */
export const isCookiePresent = (pattern) => {
  const apiConfigObj = getAPIConfig();
  return new Promise((resolve, reject) => {
    if (apiConfigObj && apiConfigObj.domain) {
      CookieManager.get(apiConfigObj.domain)
        .then((response) => {
          const isPresent = Object.keys(response).find((key) => key.includes(pattern));
          resolve(isPresent);
        })
        .catch((e) => reject(e));
    } else {
      reject();
    }
  });
};

/**
 * @param {url} string
 * @returns {type} string
 */
const getLandingPage = (url) => {
  const { URL_PATTERN } = config;
  if (url.includes(URL_PATTERN.PRODUCT_LIST)) {
    return URL_PATTERN.PRODUCT_LIST;
  }
  if (url.includes(URL_PATTERN.CATEGORY_LANDING)) {
    return URL_PATTERN.CATEGORY_LANDING;
  }
  if (url.includes(URL_PATTERN.OUTFIT_DETAILS)) {
    return URL_PATTERN.OUTFIT_DETAILS;
  }
  if (url.includes(URL_PATTERN.SEARCH_DETAIL)) {
    return URL_PATTERN.SEARCH_DETAIL;
  }
  if (url.includes(URL_PATTERN.BUNDLE_DETAIL)) {
    return URL_PATTERN.BUNDLE_DETAIL;
  }
  if (url.includes(URL_PATTERN.STORE_LOCATOR)) {
    return URL_PATTERN.STORE_LOCATOR;
  }
  return null;
};

/* eslint-disable no-unused-vars */
export const getSessionStorage = (key) => {
  return null;
};

/* eslint-disable no-unused-vars */
export const removeSessionStorage = (key) => {
  return null;
};

/**
 * @function: navigateToNestedRoute
 * This method responsible for navigate between different stacks/routes. Now don’t need to make the same routes entry in the multiple stacks
 * @param {Object} _navigation - navigation
 * @param {Object} _stackName - navigation stack
 * @param {Object} _routeName - route name
 * @param {Object} params - params
 */
export const navigateToNestedRoute = (_navigation, _stackName, _routeName, params) => {
  return (
    _navigation &&
    _navigation.dispatch &&
    _navigation.dispatch(
      NavigationActions.navigate({
        routeName: _stackName,
        action: NavigationActions.navigate({
          routeName: _routeName,
          params,
        }),
      })
    )
  );
};
/**
 * @param {string} url
 * @param {function} navigation
 * Returns navigation to the parsed URL based on  the url param
 */

const getHeaderText = (urlText) => {
  let titleText = (urlText && urlText.length > 1 && urlText[1]) || '';

  if (titleText && titleText.indexOf('?') > -1) {
    titleText = titleText.split('?');
    titleText = titleText && titleText.length > 0 && titleText[0];
  }
  return titleText.replace(/[\W_]+/g, ' ').replace('icid', '');
};

/**
 * @param {string} extractedURL
 * @return {string} function returns icid value.
 */

const getIcid = (extractedURL) => {
  let icid;
  if (extractedURL && extractedURL.indexOf('icid') !== -1) {
    const [splittedKey, icidParam] = extractedURL.split('=');
    if (splittedKey === 'icid') {
      icid = icidParam;
    }
  }
  return icid;
};

/**
 * @function resetNavigationStack
 * This function resets data from navigation stack and navigates to Home
 *
 */
export const resetNavigationStack = (navigation) => {
  navigation.dispatch(
    StackActions.reset({
      index: 0,
      key: null,
      actions: [
        NavigationActions.navigate({
          routeName: 'Home',
        }),
      ],
    })
  );
};

export const isAndroid = () => Platform.OS === 'android';
export const isIOS = () => Platform.OS === 'ios';

export const isDeviceHasNotch = () => {
  return DeviceInfo && DeviceInfo.hasNotch();
};

export const goToPlpTab = (navigation) => {
  if (isAndroid()) {
    navigateToNestedRoute(navigation, 'PlpStack', 'Navigation');
  }
};

const getSearchString = (text) => {
  let searchString = text && text[1] && text[1].split('?')[0];
  if (searchString.indexOf('&icid') > -1) {
    searchString = searchString.replace('&icid', '');
  }
  return searchString;
};

// eslint-disable-next-line complexity
export const navigateToPage = (url, navigation, extraParams = {}) => {
  const { URL_PATTERN } = config;
  const { navigate } = navigation;
  const urlValue = url || '';

  // Passing customURL for PLP & PDP pages
  // now we are passing single value of cid as customURL other query params we remove
  // because it's brancking the page & API calls
  const customURL = (urlValue && urlValue.split('&')[0]) || urlValue;
  const category = getLandingPage(urlValue);
  const text = urlValue.split('=');
  const titleSplitValue = getHeaderText(text);
  const extractedURL = urlValue && urlValue.split('&')[1];
  const icid = getIcid(extractedURL);

  switch (category) {
    case URL_PATTERN.PRODUCT_LIST:
      /**
       * /p/Rainbow--The-Birthday-Girl--Graphic-Tee-2098277-10
       * If url starts with “/p” → Create and navigate to a page in stack for Products (Blank page with a Text - “Product List”)
       */
      goToPlpTab(navigation);
      return navigate('ProductDetail', {
        pdpUrl: customURL,
        title: titleSplitValue,
        reset: true,
        ...(icid && {
          icid,
        }),
        ...extraParams,
      });

    case URL_PATTERN.CATEGORY_LANDING:
      /**
       * /c/* - If url starts with “/c” (* can be anything in url) → Select “CATEGORY_LANDING” tab in tabbar and Open CATEGORY_LANDING page
       */
      goToPlpTab(navigation);
      return navigate('ProductListing', {
        url: customURL,
        title: titleSplitValue,
        reset: true,
        ...(icid && {
          icid,
        }),
        ...extraParams,
      });
    case URL_PATTERN.BUNDLE_DETAIL: {
      goToPlpTab(navigation);
      return navigation.navigate('BundleDetail', {
        title: titleSplitValue,
        pdpUrl: titleSplitValue,
        selectedColorProductId: titleSplitValue,
        reset: true,
        ...(icid && {
          icid,
        }),
        ...extraParams,
      });
    }
    case URL_PATTERN.OUTFIT_DETAILS: {
      goToPlpTab(navigation);
      const outfitIdPart = (url && url.split('/outfit/')) || [];
      const outfitIds = (outfitIdPart[1] && outfitIdPart[1].split('/')) || [];
      return navigation.navigate('OutfitDetail', {
        title: 'COMPLETE THE LOOK',
        outfitId: outfitIds[0],
        vendorColorProductIdsList: outfitIds[1],
        ...extraParams,
        reset: true,
        ...(icid && {
          icid,
        }),
      });
    }
    case URL_PATTERN.SEARCH_DETAIL: {
      goToPlpTab(navigation);
      const searchString = decodeURIComponent(getSearchString(text));
      return navigation.navigate('SearchDetail', {
        title: searchString,
        ...extraParams,
        ...(icid && {
          icid,
        }),
      });
    }
    case URL_PATTERN.STORE_LOCATOR: {
      return navigation.navigate('StoreLanding', {
        title: 'FIND A STORE',
      });
    }
    default:
      return null;
  }
};

/**
 * @function getScreenWidth function returns screen width.
 * @return {number} function returns width of device vieport.
 */
export const getScreenWidth = () => {
  return parseInt(Dimensions.get('screen').width, 10);
};

/**
 * @function getScreenHeight function returns screen height.
 * @return {number} function returns height of device viewport.
 */
export const getScreenHeight = () => {
  return parseInt(Dimensions.get('screen').height, 10);
};

const getFAutoValue = (isVideoUrl) => {
  return isVideoUrl ? '' : ',f_auto';
};

const createRelativeUrlPath = (
  basePath,
  namedTransformation,
  dprValue,
  url,
  isVideoUrl = false
) => {
  return `${basePath}/${
    namedTransformation ? `${namedTransformation},` : ''
  }dpr_${dprValue}${getFAutoValue(isVideoUrl)}/${url}`;
};

/**
 * @function cropImageUrl function appends or replaces the cropping value in the URL
 * @param {string} url the image url
 * @param {string} crop the crop parameter
 * @param {string} namedTransformation the namedTransformation parameter
 * @return {string} function returns new Url with the crop value
 */
export const cropImageUrl = (url, crop, namedTransformation, isVideoUrl = false) => {
  const apiConfigObj = getAPIConfig();
  const brandName = getBrand();
  const brandId = brandName && brandName.toUpperCase();
  const assetHost = apiConfigObj[`assetHost${brandId}`];
  const basePath = assetHost || '';
  let URL = url;
  let dprValue = PixelRatio.get();
  const fAutoValue = getFAutoValue(isVideoUrl);
  dprValue = dprValue ? `${dprValue}.0`.slice(0, 3) : '2.0';

  // Image path transformation in case of absolute image URL
  if (/^http/.test(url)) {
    const [urlPath = '', urlData = ''] = url && url.split('/upload');
    const imgPath = urlPath && urlPath.replace(/^\//, '');
    if (urlPath && crop) {
      URL = `${imgPath}/upload/${crop}${fAutoValue}/${urlData.replace(/^\//, '')}`;
    }
    if (namedTransformation) {
      URL = `${imgPath}/upload/${namedTransformation},dpr_${dprValue}${fAutoValue}/${urlData.replace(
        /^\//,
        ''
      )}`;
    }
  } else {
    // Image path transformation in case of relative image URL
    URL = createRelativeUrlPath(basePath, namedTransformation, dprValue, url, isVideoUrl);
  }
  return URL;
};

/**
 * @function getValueFromAsyncStorage
 * This method retrieves value for input key from asyncstorage
 * @param key
 *
 * @returns: value from async storage
 */
export const getValueFromAsyncStorage = async (key) => {
  try {
    return await AsyncStorage.getItem(key);
  } catch (error) {
    // Error retrieving data
    return null;
  }
};

/**
 * @function setValueInAsyncStorage
 * This method saves the input key and value in asyncstorage
 * @param key: key to be saved
 * @param value: value for key
 *
 */
export const setValueInAsyncStorage = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    // Error saving data
  }
};

/**
 * @function removeMultipleFromAsyncStorage
 * This method remove the multiple input keys and values from asyncstorage
 * @param key: key to be saved
 * @param value: value for key
 *
 */
export const removeMultipleFromAsyncStorage = (keyArr) => {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.multiRemove(keyArr);
      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
};

export const removeValueFromAsyncStorage = (key) => {
  AsyncStorage.removeItem(key);
};

export const clearLoginCookiesFromAsyncStore = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const cookiesToDelete = [
        'tcp_isregistered',
        'usid',
        'userToken',
        'basketId',
        'cartItems',
        'cartItemsCount',
      ];
      const asyncKeys = await AsyncStorage.getAllKeys();
      const wcKeys = asyncKeys.filter((key) => {
        return key.startsWith('WC_') || cookiesToDelete.includes(key);
      });
      await AsyncStorage.multiRemove(wcKeys);
      resolve(true);
    } catch (err) {
      reject(err);
    }
  });
};

export const clearStateCookiesFromAsyncStore = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const asyncKeys = await AsyncStorage.getAllKeys();
      const wcKeys = asyncKeys.filter((key) => {
        return key === 'tcpState';
      });
      await AsyncStorage.multiRemove(wcKeys);
      resolve(true);
    } catch (err) {
      reject(err);
    }
  });
};

export const checkForDuplicateAuthCookies = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const asyncKeys = await AsyncStorage.getAllKeys();
      const wcAuthKeys = asyncKeys.filter((key) => key.startsWith('WC_AUTHENTICATION_'));
      resolve(wcAuthKeys.length > 1);
    } catch (err) {
      reject(err);
    }
  });
};

export const setSessionStorage = (arg) => {
  const { key, value } = arg;
  setValueInAsyncStorage(key, value);
};

/**
 * Returns data set in window else from state
 * @param {string} key - state
 * @returns {string} - abtests object
 */
export const getABtestFromState = (AbTests) => {
  return (AbTests && AbTests.parsedAbtestResponse) || {};
};

export const getBootstrapCachedData = async (lastSavedKey, dataKey, errored = false) => {
  const brand = getBrand();
  const brandLastSavedKey = `${lastSavedKey}_${brand}`;
  const brandDataKey = `${dataKey}_${brand}`;
  try {
    const lastSaved = await AsyncStorage.getItem(brandLastSavedKey);
    let data = null;
    if (lastSaved) {
      const lastSavedTime = new Date(lastSaved).getTime();
      const currentTime = new Date().getTime();
      const diffInMins = (currentTime - lastSavedTime) / 60000;
      if (diffInMins < ASYNC_CACHE_TTL || errored) {
        const rawData = await AsyncStorage.getItem(brandDataKey);
        data = JSON.parse(rawData);
      }
    }
    return {
      data,
      staticData: staticData[brand][dataKey],
    };
  } catch (error) {
    return {
      data: null,
      staticData: staticData[brand][dataKey],
    };
  }
};

export const setBootstrapCachedData = async (lastSavedKey, dataKey, data) => {
  try {
    const brand = getBrand();
    const brandSavedKey = `${lastSavedKey}_${brand}`;
    const brandDataKey = `${dataKey}_${brand}`;
    const currentDate = new Date();
    setValueInAsyncStorage(brandSavedKey, currentDate.toString());
    setValueInAsyncStorage(brandDataKey, JSON.stringify(data));
  } catch (error) {
    // eslint-disable-next-line
    console.log('Error occurred --> Saving data in Async Storage');
  }
};

export const validateExternalUrl = (url) => {
  const isExternal = url && (url.indexOf('http') || url.indexOf('https') !== true);
  if (isExternal === true) {
    return true;
  }
  return false;
};

const getRegion = (configVal, country) => {
  return configVal && country;
};

const getWebViewBvUrl = (envConfig, apiSiteInfo) => {
  return envConfig.RWD_APP_BV_WEB_VIEW_URL || apiSiteInfo.BV_WEB_VIEW_URL;
};

const getBvEnvironment = (envConfig, apiSiteInfo) => {
  return envConfig.RWD_APP_BV_ENV || apiSiteInfo.BV_ENV;
};

const getBvSpotlightInstance = (envConfig, apiSiteInfo) => {
  return envConfig.RWD_APP_BV_SPOTLIGHT_INSTANCE || apiSiteInfo.BV_SPOTLIGHT_INSTANCE;
};

const getBvSpotlightEnvironment = (envConfig, apiSiteInfo) => {
  return envConfig.RWD_APP_BV_SPOTLIGHT_ENV || apiSiteInfo.BV_SPOTLIGHT_ENV;
};

const getPlatform = () => {
  const os = Platform.OS;
  if (os === 'android') {
    return 'ANDROID';
  }
  return 'IOS';
};

/* ICS-280 Making this change to do code push. The env key name should be updated later */
const getMonetateChannel = (envConfig, appTypeSuffix) => {
  return envConfig[`MONETATE_CHANNEL_${appTypeSuffix === BrandSuffix.tcp ? appTypeSuffix : 'GMP'}`];
};

const getCodePushAppVersion = (envConfig, appTypeSuffix, os) => {
  return envConfig[`RWD_APP_CODE_PUSH_VERSION_${appTypeSuffix}_${os}`] || envConfig.RWD_APP_VERSION;
};

export const getCurrentAppVersion = () => {
  const { codePushVersionIos, codePushVersionAndroid } = getAPIConfig();
  return isIOS() ? codePushVersionIos : codePushVersionAndroid;
};

const getChatBotPageUrl = (envConfig, appTypeSuffix) => {
  let chatBotUrl = 'https://www.childrensplace.com/rwd/chatbot.html';
  if (envConfig.RWD_APP_CHAT_BOT_URL)
    chatBotUrl = `${envConfig[`RWD_APP_WEB_DOMAIN_${appTypeSuffix}`]}/${
      envConfig.RWD_APP_CHAT_BOT_URL
    }`;
  return chatBotUrl;
};

export const getEGiftCardPageUrl = () => {
  const { webAppDomain, siteId, brandId } = getAPIConfig();
  const baseURL =
    (typeof window !== 'undefined' && window.location && window.location.origin) || webAppDomain;
  const token = brandId === BRAND_TYPE_TCP ? '?token=tcprwd&' : '?token=gymrwd&';
  return `${baseURL}/${siteId}/content/giftcard${appendToken() ? token : ''}`;
};

export const getHelpCenterPageUrl = () => {
  const { webAppDomain, siteId, brandId } = getAPIConfig();
  const baseURL =
    (typeof window !== 'undefined' && window.location && window.location.origin) || webAppDomain;
  return `${baseURL}/${siteId}/help-center/contact-us}`;
};

const getChatBotVersion = (envConfig) => {
  return envConfig.RWD_APP_CHAT_BOT_VERSION || '1';
};

const getChatBotEnv = (envConfig) => {
  return envConfig.RWD_APP_CHAT_BOT_ENV || 'prod';
};

const getSbpSizesURL = (envConfig, appTypeSuffix) => {
  return `${envConfig[`SBP_API_DOMAIN_${appTypeSuffix}`]}/${envConfig.SBP_SIZES}`;
};

const getSbpInterestsURL = (envConfig, appTypeSuffix) => {
  return `${envConfig[`SBP_API_DOMAIN_${appTypeSuffix}`]}/${envConfig.SBP_INTERESTS}`;
};

const getSbpProfilesURL = (envConfig, appTypeSuffix) => {
  return `${envConfig[`SBP_API_DOMAIN_${appTypeSuffix}`]}/${envConfig.SBP_API_URL_ENDPOINT}`;
};

const isNewOnboardingDisabled = (appTypeSuffix) => appTypeSuffix !== BrandSuffix.tcp;

/**
 * function getAPIInfoFromEnv
 * @param {*} apiSiteInfo
 * @param {*} envConfig
 * @param {*} appTypeSuffix
 * @returns
 */
const getAPIInfoFromEnv = (apiSiteInfo, envConfig, appTypeSuffix) => {
  const siteIdKey = `RWD_APP_SITE_ID_${appTypeSuffix}`;
  const country = envConfig[siteIdKey] && envConfig[siteIdKey].toUpperCase();
  logger.info(
    'unboxKey',
    `${envConfig[`RWD_APP_UNBXD_SITE_KEY_${country}_EN_${appTypeSuffix}`]}/${
      envConfig[`RWD_APP_UNBXD_SITE_KEY_${country}_EN_${appTypeSuffix}`]
    }`
  );
  const apiEndpoint = envConfig[`RWD_APP_API_DOMAIN_${appTypeSuffix}`] || ''; // TO ensure relative URLs for MS APIs
  const unbxdApiKeyTCP = envConfig[`RWD_APP_UNBXD_API_KEY_${country}_EN_TCP`];
  const unbxdApiKeyGYM = envConfig[`RWD_APP_UNBXD_API_KEY_${country}_EN_GYM`];
  const unbxdApiKeySNJ = envConfig[`RWD_APP_UNBXD_API_KEY_${country}_EN_SNJ`];
  const recommendationsAPI =
    envConfig[`RWD_APP_RECOMMENDATIONS_API_${country}_EN_${appTypeSuffix}`];

  return {
    traceIdCount: 0,
    langId: envConfig[`RWD_APP_LANGID_${appTypeSuffix}`] || apiSiteInfo.langId,
    MELISSA_KEY: envConfig[`RWD_APP_MELISSA_KEY_${appTypeSuffix}`] || apiSiteInfo.MELISSA_KEY,
    //  bazar voice config
    BV_API_KEY: envConfig[`RWD_APP_BV_API_KEY_${appTypeSuffix}`],
    BV_API_URL: envConfig.RWD_APP_BV_API_URL || apiSiteInfo.BV_URL,
    BV_WEB_VIEW_URL: getWebViewBvUrl(envConfig, apiSiteInfo),
    BV_ENVIRONMENT: getBvEnvironment(envConfig, apiSiteInfo),
    APP_ANALYTICS_ID: envConfig[`RWD_APP_ANALYTICS_ID_${appTypeSuffix}`],
    BV_SPOTLIGHT_ENVIRONMENT: getBvSpotlightEnvironment(envConfig, apiSiteInfo),
    BV_SUBMISSION_URL: apiSiteInfo.BV_SUBMISSION_URL,
    BV_INSTANCE: apiSiteInfo[`BV_${appTypeSuffix}_INSTANCE`],
    BV_SPOTLIGHT_INSTANCE: getBvSpotlightInstance(envConfig, apiSiteInfo),
    BV_SHARED_KEY: apiSiteInfo.BV_SHARED_KEY,
    GRAPHQL_CLIENT_CACHE_CLEAR_INTERVAL: +envConfig.RWD_APP_GRAPHQL_CACHE_CLEAR_INTERVAL_MINS,
    GRAPHQL_API_TIMEOUT: +envConfig.GRAPHQL_API_TIMEOUT,
    GRAPHQL_API_TIMEOUT_CLIENT: +envConfig.GRAPHQL_API_TIMEOUT,
    assetHostTCP: envConfig.RWD_APP_DAM_HOST_TCP || apiSiteInfo.assetHost,
    productAssetPathTCP: envConfig.RWD_APP_DAM_PRODUCT_IMAGE_PATH_TCP,
    assetHostGYM: envConfig.RWD_APP_DAM_HOST_GYM || apiSiteInfo.assetHost,
    assetHostSNJ: envConfig.RWD_APP_DAM_HOST_SNJ || apiSiteInfo.assetHost,
    productAssetPathSNJ: envConfig.RWD_APP_DAM_PRODUCT_IMAGE_PATH_SNJ,
    assetHost: envConfig[`RWD_APP_ASSETHOST_${appTypeSuffix}`],
    instagramStatus: envConfig.RWD_INSTAGRAM_STATUS,
    productAssetPathGYM: envConfig.RWD_APP_DAM_PRODUCT_IMAGE_PATH_GYM,
    domain: `${apiEndpoint}/${envConfig[`RWD_APP_API_IDENTIFIER_${appTypeSuffix}`]}/`,
    apiEndpoint: `${apiEndpoint}/`,
    unbxdTCP: envConfig.RWD_APP_UNBXD_DOMAIN_TCP || apiSiteInfo.unbxd,
    unbxdGYM: envConfig.RWD_APP_UNBXD_DOMAIN_GYM || apiSiteInfo.unbxd,
    unbxdSNJ: envConfig.RWD_APP_UNBXD_DOMAIN_SNJ || apiSiteInfo.unbxd,
    unboxKeyTCP: `${unbxdApiKeyTCP}/${envConfig[`RWD_APP_UNBXD_SITE_KEY_${country}_EN_TCP`]}`,
    unbxdApiKeyTCP,
    unboxKeyGYM: `${unbxdApiKeyGYM}/${envConfig[`RWD_APP_UNBXD_SITE_KEY_${country}_EN_GYM`]}`,
    unbxdApiKeyGYM,
    unboxKeySNJ: `${unbxdApiKeySNJ}/${envConfig[`RWD_APP_UNBXD_SITE_KEY_${country}_EN_SNJ`]}`,
    unbxdApiKeySNJ,
    previewEnvId: envConfig[`RWD_APP_PREVIEW_ENV_${appTypeSuffix}`],
    CANDID_API_KEY: envConfig[`RWD_APP_CANDID_API_KEY_${appTypeSuffix}`],
    CANDID_API_URL: envConfig[`RWD_APP_CANDID_URL_${appTypeSuffix}`],
    RAYGUN_API_KEY: envConfig[`RWD_APP_RAYGUN_API_KEY_${appTypeSuffix}`],
    RWD_APP_VERSION: envConfig.RWD_APP_VERSION,
    isErrorReportingActive: envConfig.RWD_APP_ERROR_REPORTING_ACTIVE,
    googleApiKey: envConfig[`RWD_APP_GOOGLE_MAPS_API_KEY_${appTypeSuffix}_${getPlatform()}`],
    googleMapsApiKey: envConfig[`RWD_APP_GOOGLE_MAPS_API_KEY_${appTypeSuffix}`],
    mapBoxApiKey: envConfig.RWD_APP_MAPBOX_API_KEY,
    mapBoxApiURL: envConfig.RWD_APP_MAPBOX_API_URL,
    mapboxStoreLocatorApiURL: envConfig.RWD_APP_MAPBOX_STORE_LOCATOR_API_URL,
    paypalEnv: envConfig[`RWD_APP_PAYPAL_ENV_${appTypeSuffix}`],
    paypalStaticUrl: envConfig[`RWD_APP_PAYPAL_STATIC_DOMAIN_${appTypeSuffix}`],
    instakey: envConfig[`RWD_APP_INSTAGRAM_${appTypeSuffix}`],
    crossDomain: envConfig[`RWD_APP_CROSS_DOMAIN_${appTypeSuffix}`],
    TWITTER_CONSUMER_KEY: envConfig[`RWD_APP_TWITTER_CONSUMER_KEY_${appTypeSuffix}`],
    TWITTER_CONSUMER_SECRET: envConfig[`RWD_APP_TWITTER_CONSUMER_SECRET_${appTypeSuffix}`],
    RECOMMENDATIONS_API: recommendationsAPI,
    styliticsUserNameTCP: envConfig.RWD_APP_STYLITICS_USERNAME_TCP,
    styliticsUserNameGYM: envConfig.RWD_APP_STYLITICS_USERNAME_GYM,
    styliticsRegionTCP: getRegion(envConfig.RWD_APP_STYLITICS_REGION_TCP, country),
    styliticsRegionGYM: getRegion(envConfig.RWD_APP_STYLITICS_REGION_GYM, country),
    host: envConfig[`RWD_APP_HOST_${appTypeSuffix}`],
    webAppDomain: envConfig[`RWD_APP_WEB_DOMAIN_${appTypeSuffix}`],
    webAppToken: envConfig[`RWD_APP_WEB_TOKEN_${appTypeSuffix}`],
    brandId: `${appTypeSuffix}`,
    adobeDeliveryApi: envConfig[`RWD_APP_ADOBE_DELIVERY_API_${appTypeSuffix}`],
    disableTcpBrand: envConfig.RWD_APP_DISABLE_TCP_BRAND === 'true',
    appUpdateDomain: envConfig.RWD_APP_UPDATE_DOMAIN,
    enableGqlWarn: envConfig.RWD_APP_ENABLE_GQL_WARN,
    gqlWarnThreshold: envConfig.RWD_APP_GQL_WARN_THRESHOLD,
    envForRecommendation: envConfig.RWD_APP_RECOMMENDATIONS_ENV,
    enableAkamaiBMP: envConfig.RWD_APP_ENABLE_AKAMAI_BMP === 'true',
    forceUpdateConfig: envConfig.RWD_APP_FORCE_UPDATE_CONFIG === 'true',
    codePushVersionIos: getCodePushAppVersion(envConfig, appTypeSuffix, 'IOS'),
    codePushVersionAndroid: getCodePushAppVersion(envConfig, appTypeSuffix, 'ANDROID'),
    applePayMerchantId: envConfig.RWD_APP_APPLE_PAY_ID,
    monetateUrl: envConfig.MONETATE_URL,
    monetateChannel: getMonetateChannel(envConfig, appTypeSuffix),
    monetateEnabled: envConfig.MONETATE_ENABLED,
    sbpSizes: getSbpSizesURL(envConfig, appTypeSuffix),
    sbpInterests: getSbpInterestsURL(envConfig, appTypeSuffix),
    sbpEndPoint: getSbpProfilesURL(envConfig, appTypeSuffix),
    sbpEnvConfigEnabled: envConfig.SBP_ENABLED,
    chatBotVersion: getChatBotVersion(envConfig),
    chatBotEnv: getChatBotEnv(envConfig),
    chatBotPageUrl: getChatBotPageUrl(envConfig, appTypeSuffix),
    shouldAppendToken: !!envConfig.ADD_TOKEN,
    merkleUrl: envConfig.MERKLE_API_URL,
    merkleWebViewUrl: envConfig.MERKLE_WEBVIEW_URL,
    redesignEnvConfigEnabled: envConfig.REDESIGN_ENABLED,
    afterpayButtonKey: envConfig.AFTERPAY_BUTTON_MERCHANT_KEY,
    afterpayApiBaseUrl: envConfig.AFTERPAY_API_BASE_URL,
    afterpayShopDirectoryId: envConfig.AFTERPAY_SHOP_DIRECTORY_ID,
    isRedesignOnboardingDisabled: isNewOnboardingDisabled(appTypeSuffix),
    isRedesignCodePushAnimationDisabled: false,
    snjMinOrderNo: envConfig.SNJ_MIN_ORDER_NO,
    hapticFeedbackDisabled: envConfig.HAPTIC_DISABLE,
  };
};

/**
 * getGraphQLApiFromEnv
 *
 * @param {*} apiSiteInfo
 * @param {*} envConfig
 * @param {*} appTypeSuffix
 * @returns
 */
const getGraphQLApiFromEnv = (apiSiteInfo, envConfig, appTypeSuffix) => {
  const graphQlEndpoint = envConfig[`RWD_APP_GRAPHQL_API_ENDPOINT_${appTypeSuffix}`];
  return {
    graphql_region: envConfig[`RWD_APP_GRAPHQL_API_REGION_${appTypeSuffix}`],
    graphql_endpoint_url: `${graphQlEndpoint}/${
      envConfig[`RWD_APP_GRAPHQL_API_IDENTIFIER_${appTypeSuffix}`]
    }`,
    graphql_persisted_query: envConfig.RWD_APP_GQL_PQ,
    graphql_auth_type: envConfig[`RWD_APP_GRAPHQL_API_AUTH_TYPE_${appTypeSuffix}`],
    graphql_api_key: envConfig[`RWD_APP_GRAPHQL_API_KEY_${appTypeSuffix}`] || '',
  };
};

/**
 * function createAPIConfigForApp
 * This method creates and returns api config for input apptype
 * @param {*} envConfig
 * @param {*} appTypeSuffix
 * @returns api config for input app type
 */
export const createAPIConfigForApp = (envConfig, appTypeSuffix) => {
  // TODO - use cookie as well..
  const siteIdKey = `RWD_APP_SITE_ID_${appTypeSuffix}`;
  const brandIdKey = `RWD_APP_BRANDID_${appTypeSuffix}`;
  const isCASite = envConfig[siteIdKey] === API_CONFIG.siteIds.ca;
  const isGYMSite = envConfig[brandIdKey] === API_CONFIG.brandIds.gym;
  const countryConfig = isCASite ? API_CONFIG.CA_CONFIG_OPTIONS : API_CONFIG.US_CONFIG_OPTIONS;
  const brandConfig = isGYMSite ? API_CONFIG.GYM_CONFIG_OPTIONS : API_CONFIG.TCP_CONFIG_OPTIONS;
  const apiSiteInfo = API_CONFIG.sitesInfo;
  const basicConfig = getAPIInfoFromEnv(apiSiteInfo, envConfig, appTypeSuffix);
  const graphQLConfig = getGraphQLApiFromEnv(apiSiteInfo, envConfig, appTypeSuffix);
  const catalogId =
    API_CONFIG.CATALOGID_CONFIG[isGYMSite ? 'Gymboree' : BrandSuffix.tcp][
      isCASite ? 'Canada' : 'USA'
    ];
  const enableErrorBoundary = envConfig[`RWP_APP_ENABLE_ERROR_BOUNDARY_${appTypeSuffix}`];

  return {
    ...basicConfig,
    ...graphQLConfig,
    ...countryConfig,
    ...brandConfig,
    isMobile: false,
    cookie: null,
    catalogId,
    language: '',
    enableErrorBoundary,
  };
};

/**
 * @method - updateCustomConfig
 * @description - Update config from dynamic code push
 * @param {object} currentAppConfig
 */
const updateCustomConfig = (currentAppConfig) => {
  const customAppConfig = Config;
  const updatedConfig = currentAppConfig;

  const currentBrandCustomConfig =
    currentAppConfig && currentAppConfig.brandId ? customAppConfig[currentAppConfig.brandId] : {};
  forOwn(currentBrandCustomConfig, (value, key) => {
    updatedConfig[key] = currentBrandCustomConfig[key];
  });

  const commonCustomConfig = customAppConfig.common;
  forOwn(commonCustomConfig, (value, key) => {
    updatedConfig[key] = commonCustomConfig[key];
  });

  return updatedConfig;
};

/**
 * getCurrentAPIConfig
 * This method returns current api config
 */
const getCurrentAPIConfig = (envConfig, isTCPBrand) => {
  if (isTCPBrand) {
    // return tcp config
    tcpAPIConfig = tcpAPIConfig || createAPIConfigForApp(envConfig, BrandSuffix.tcp);
    currentAppAPIConfig = tcpAPIConfig;
  } else {
    // return gym config
    gymAPIConfig = gymAPIConfig || createAPIConfigForApp(envConfig, BrandSuffix.gym);
    currentAppAPIConfig = gymAPIConfig;
  }
  return currentAppAPIConfig.forceUpdateConfig
    ? updateCustomConfig(currentAppAPIConfig)
    : currentAppAPIConfig;
};

/**
 * createAPIConfig
 * This method returns current api config, creates new if not already created
 */
export const createAPIConfig = (envConfig, appType) => {
  const { RWD_APP_BRANDID_TCP: tcpBrandId } = envConfig;
  const isTCPBrand = appType === tcpBrandId;
  return getCurrentAPIConfig(envConfig, isTCPBrand);
};

/**
 * switchAPIConfig
 * This method switches api config on brand switch in app
 */
export const switchAPIConfig = (envConfig) => {
  // return second api config stored in local
  const isPrevConfigTCP = currentAppAPIConfig === tcpAPIConfig;
  return getCurrentAPIConfig(envConfig, !isPrevConfigTCP);
};
export const getSiteId = () => {
  const { siteId } = getAPIConfig();
  return siteId;
};

export const getSiteIdInCaps = () => {
  const siteId = getSiteId();
  return (siteId && siteId.toUpperCase()) || '';
};

export const getPlaceSiteId = () => {
  const siteIdInCaps = getSiteIdInCaps();
  return `PLACE_${siteIdInCaps}`;
};

export const bindAllClassMethodsToThis = (obj, namePrefix = '', isExclude = false) => {
  const prototype = Object.getPrototypeOf(obj);
  // eslint-disable-next-line
  for (let name of Object.getOwnPropertyNames(prototype)) {
    const descriptor = Object.getOwnPropertyDescriptor(prototype, name);
    const isGetter = descriptor && typeof descriptor.get === 'function';
    // eslint-disable-next-line
    if (isGetter) continue;
    if (
      typeof prototype[name] === 'function' && name !== 'constructor' && isExclude
        ? !name.startsWith(namePrefix)
        : name.startsWith(namePrefix)
    ) {
      // eslint-disable-next-line
      obj[name] = prototype[name].bind(obj);
    }
  }
};

/**
 * getPixelRatio
 * This method returns the PixelRatio for different devices ( Android & ISO)
 */
export const getPixelRatio = () => {
  // for android iPhone iPhone 6 Plus, 7 Plus, 8 Plus , X, XS, XS Max ,Pixel, Pixel 2 devices. (Note: PixelRatio = 3 ).
  let devicepixel = 'xxhdpi';

  if (PixelRatio.get() === 1) {
    // for android devices mdpi.
    devicepixel = 'mdpi';
    return devicepixel;
  }
  if (PixelRatio.get() === 1.5) {
    // for android devices hdpi
    devicepixel = 'hdpi';
    return devicepixel;
  }
  if (PixelRatio.get() === 2) {
    // for android & iPhone 4, 4S ,iPhone 5, 5C, 5S ,iPhone 6, 7, 8 ,iPhone XR devices .
    devicepixel = 'xhdpi';
    return devicepixel;
  }
  if (PixelRatio.get() > 3.5) {
    // for android devices, Nexus 6 , Samsung7 , Pixel XL, Pixel 2 XL, xxxhdpi Android devices.
    devicepixel = 'xxxhdpi';
    return devicepixel;
  }
  return devicepixel;
};

export default {
  getSiteId,
};

/**
 * INFO: Use this function only after accessibility props is set.
 * This adds unique identifier as testId or accessibiliyLabel when the build
 * type is of automation variant. For dev, alpha, release builds
 * it will return an empty object and won't override anything.
 */
const isAutomation = false;
export function setTestId(id) {
  if (id === false) {
    return {};
  }
  if (isAutomation) {
    return Platform.select({
      ios: {
        testID: id,
      },
      android: {
        accessibilityLabel: id,
      },
    });
  }
  return {};
}

/**
 * Avoid breaking of the app if author accidentally pass invalid color from the CMS.
 * Return null if color is invalid else return the color.
 * @param {String} color Color string to validate
 */
export const validateColor = (color) => {
  let colorSheet = {
    viewColor: {
      color: null,
    },
  };
  try {
    colorSheet = StyleSheet.create({
      // eslint-disable-next-line react-native/no-unused-styles
      viewColor: {
        color,
      },
    });
  } catch (e) {
    // eslint-disable-next-line no-console
    console.warn(`Invalid color: ${JSON.stringify(color)}`);
  }

  return colorSheet.viewColor.color;
};
/**
 * @method getTranslatedMomentDate
 * @desc returns day, month and day of the respective date provided
 * @param {string} date date which is to be mutated
 * @param {upperCase} locale use for convert locate formate
 * @param {upperCase} formate use for convert locate formate
 */
export const getTranslatedMomentDate = (dateInput, language) => {
  const dateInputParsed = dateInput ? new Date(dateInput) : new Date();
  return {
    day: formatDateWithLocale(dateInputParsed, 'EEE', language),
    month: formatDateWithLocale(dateInputParsed, 'MMM', language),
    date: formatDateWithLocale(dateInputParsed, 'dd', language),
    year: formatDateWithLocale(dateInputParsed, 'yyyy', language),
  };
};

/**
 * @function createGoogleMapUrl - returns map apps url.
 * @param {String} lat - lattitude
 * @param {String} long - longitude
 */
export const createGoogleMapUrl = (lat = '', long = '', label = '') => {
  return Platform.select({
    ios: `maps:${lat},${long}?q=${label}`,
    android: `geo:${lat},long?q=${label}`,
  });
};

/**
 * @function mapHandler - checks if map application is present in phone, opens app,
 * otherwise opens the map in mobile browser.
 * @param {Object} store - store info
 */
export const mapHandler = (store) => {
  const {
    basicInfo: { address, coordinates },
  } = store;
  const { addressLine1, city, state, zipCode } = address;
  const browserUrl = `${googleMapConstants.OPEN_STORE_DIR_WEB}${addressLine1}, ${city}, ${state}, ${zipCode}`;
  if (!coordinates) {
    return Linking.openURL(browserUrl);
  }
  const { lat, long } = coordinates;
  const mapLabel = `${addressLine1}, ${city}, ${state}`;
  const url = createGoogleMapUrl(lat, long, mapLabel);
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      return Linking.openURL(url);
    }
    return Linking.openURL(browserUrl);
  });
  return Linking.openURL(browserUrl);
};

/**
 * @method getTranslateDateInformation
 * @desc returns day, month and day of the respective date provided
 * @param {string} date date which is to be mutated
 * @param {upperCase} locale use for convert locate formate
 */
export const getTranslateDateInformation = (date, language) => {
  // TODO: In web, we are using Intl to translate date, but Intl is not yet supported in Android
  // so for now, created this method which in turn will call getTranslatedMomentDate which supports Android
  // To fix this, need to add fallback package for Intl
  return getTranslatedMomentDate(date, language);
};

export const convertMillisecondToTime = (value) => {
  let time = '';
  if (value) {
    const currentDate = new Date();
    const pastDate = new Date(value);
    const timeDiff = Math.abs(currentDate.getTime() - pastDate.getTime());
    const minutes = Math.ceil(timeDiff / (1000 * 60));
    time =
      // eslint-disable-next-line no-nested-ternary
      minutes > 60
        ? Math.round(minutes / 60) < 24
          ? `${Math.round(minutes / 60)}hrs`
          : `${Math.round(minutes / 60 / 24)}d`
        : `${Math.round(minutes)}mins`;
  }
  return time;
};

export const onBack = (navigation) => {
  const goBackRoute = get(navigation, 'state.params.backTo', false);
  const isReset = get(navigation, 'state.params.reset', false);
  const resetTabStack = get(navigation, 'state.params.resetTabStack', false);
  if (resetTabStack) {
    navigation.popToTop();
    navigation.navigate(goBackRoute);
  } else if (isReset) {
    navigation.pop();
  } else if (goBackRoute) {
    navigation.navigate(goBackRoute);
  } else {
    navigation.goBack(null);
  }
};
/**
 * @method formatPhnNumber
 * @desc returns phone number after stripping space and new line characters
 * @param {string} phnNumber phone number which needs modification
 */
export const formatPhnNumber = (phnNumber) =>
  phnNumber.replace(/\n /g, '').replace(/ /g, '').replace(')', ') ');

/**
 * @method scrollToViewBottom
 * @desc scroll to view full content
 * @param {object}
 */
export const scrollToViewBottom = ({ height, pageY, callBack, currentScrollValue }) => {
  const headerHeight = 100;
  const footerHeight = 100;
  const windowHeight = getScreenHeight() - (headerHeight + footerHeight);
  const availableSpaceInBottom = windowHeight - (pageY - footerHeight);
  const scrollToBottom = height > availableSpaceInBottom;
  if (scrollToBottom) {
    callBack.scrollTo({
      x: 0,
      y: currentScrollValue + (height - availableSpaceInBottom),
      animated: true,
    });
  }
};

export const extractPID = (navigation) => {
  const pid = (navigation && navigation.getParam('pdpUrl')) || '';
  // TODO - fix this to extract the product ID from the page.
  const id = pid && pid.split('-');
  let productId = id && id.length > 1 ? `${id[id.length - 2]}_${id[id.length - 1]}` : pid;
  if (
    (id.indexOf('Gift') > -1 || id.indexOf('gift') > -1) &&
    (id.indexOf('Card') > -1 || id.indexOf('card') > -1)
  ) {
    productId = 'gift';
  }
  return productId;
};

export const constructPageName = (navigation) => {
  const pid = (navigation && navigation.getParam('pdpUrl')) || '';
  const outfitId = (navigation && navigation.getParam('outfitId')) || null;
  const productId = outfitId ? outfitId.toString() : extractPID(navigation);

  // Build a page name for tracking
  let pageName = '';
  if (productId) {
    const productIdParts = productId.split('_');
    pageName = `product:${productIdParts[0]}:${pid
      .replace(productIdParts[0], '')
      .replace(productIdParts[1], '')
      .split('-')
      .join(' ')
      .trim()
      .toLowerCase()}`;
  }
  return pageName;
};

export const logCustomEvent = (event) => {
  const { name, value, properties } = event;
  let customEvent = null;
  if (!Number.isNaN(Number(value))) {
    customEvent = new CustomEvent(name, value);
  } else {
    customEvent = new CustomEvent(name);
  }
  if (properties) {
    Object.keys(properties).forEach((key) => {
      customEvent.addProperty(key, properties[key]);
    });
  }
};

/**
 * @method getQMSessionIdFromDefaultPreference
 * @desc Getting QM session Id from Default Preferences, And function will return QM id if that will exist else blank string.
 */
export const getQMSessionIdFromDefaultPreference = () => {
  if (!sessionId) {
    if (Platform.OS === 'android') DefaultPreference.setName('NativeStorage');
    DefaultPreference.get('QuantumMetricSessionCookie').then((value) => {
      sessionId = value;
    });
  }
  return sessionId;
};

export const getReferenceId = () => {
  const cookieValue = getQMSessionIdFromDefaultPreference();
  return (
    cookieValue &&
    `${cookieValue.substring(0, 4).toUpperCase()}-${cookieValue.substring(4, 8).toUpperCase()}`
  );
};

/**
 * @method setQMSessionIdFromDefaultPreference
 * @desc Sessting QM session id from default preferences
 * @param  quantumeMetricId
 */
export const setQMSessionIdFromDefaultPreference = (qmId = '') => {
  sessionId = qmId;
};

export const getAppVersion = () => {
  return getCurrentAppVersion();
};

export const getUserAgent = () => UserAgent.getUserAgent();

export const getCustomHeaders = () => {
  const userAgentString = getUserAgent();
  const platform = isIOS() ? 'IOS' : 'Android';
  const customUserAgent = `My Place/18.0.0/${platform} deviceType/MobileApp`;

  // Added headers so that Akamai rule can differentiate between mobile web and mobile app
  return {
    'User-Agent': `${userAgentString}/tcpapp/${customUserAgent}`,
    'x-tcp-app-rwd': true,
    'tcp-app-type': 'mobile-app', // header tcp-app-type, customUserAgent in UA is patch fix for Akamai BMP
  };
};

export const getAppPublishingDate = () => {
  return Platform.OS === 'android'
    ? BuildConfig.PUBLISH_DATE
    : `${BuildConfig.PublishingDate}.${BuildConfig.CFBundleShortVersionString}`;
};

const addExtraParams = (link = '') => {
  const extraParams = {};
  if (link.includes(ROUTE_PATH.search.name)) {
    extraParams.isForceUpdate = true;
  }
  return extraParams;
};

/**
 * @function handleNavigationUrl - opens an external url or navigate to internal route
 * @param {string} link - href in anchor of richtext
 * @param {string} target - action type to identify navigation destination
 */
export const handleNavigationUrl = (navigation, link, target) => {
  const externalUrl = new RegExp('^(?:[a-z]+:)?//', 'i');
  const { webAppDomain, siteId = 'us' } = getAPIConfig();

  if (externalUrl.test(link)) {
    Linking.openURL(link);
  } else {
    switch (target) {
      case '_self':
        navigation.navigate(RICHTEXT_NAVIGATION_MAP[link]);
        break;
      case '_blank':
        Linking.openURL(`${webAppDomain}/${siteId}${link}`);
        break;
      default:
        navigateToPage(
          configureInternalNavigationFromCMSUrl(link),
          navigation,
          addExtraParams(link)
        );
        break;
    }
  }
};
/**
 * To identify whether the link clicked in moduleX has login or register link or not
 * @param {*} link
 */

export const checkForLoginRegisterLink = (link) => {
  if (/home\/register/.test(link)) {
    return 'create-account';
  }
  if (/home\/login/.test(link)) {
    return 'login';
  }
  if (/place-card/.test(link)) {
    return 'plcc';
  }

  return false;
};

/**
 * Open The Web View Screen when user openWebView props is passed as true
 * @param {*} url
 */
export const redirectToInAppView = (
  url,
  navigation,
  routeName,
  fromEspot = false,
  otherParams = {}
) => {
  if (navigation) {
    navigation.navigate('InAppView', {
      url,
      fromEspot,
      backTo: routeName,
      ...(otherParams && { ...otherParams }),
    });
  }
};

/**
 * @function compareVersion
 * @description Compare the versions of the app.
 * @param {*} oldVer Old version
 * @param {*} newVer New version
 */
export const compareVersion = (oldVer, newVer) => {
  const oldVersions = oldVer.split('.');
  const newVersions = newVer.split('.');
  const newVersion = parseInt(newVersions[0], 10) || 0;
  const oldVersion = parseInt(oldVersions[0], 10) || 0;
  if (newVersion > oldVersion) return true;
  return false;
};

export const getAkamaiSensorData = () => {
  return new Promise((resolve) => {
    if (AkamaiBMP && AkamaiBMP.getSensorData) {
      AkamaiBMP.getSensorData((sd) => {
        resolve(sd);
      });
    } else {
      resolve('');
    }
  });
};

/**
 *
 * @method goToPdpPage
 * @description navigate to pdp from bag
 * @param {*} productDetail - details of product for pdp
 * @param {*} navigation - navigation
 */
export const goToPdpPage = (productDetail, updateAppTypeHandler, navigation, otherParams) => {
  const currentAppBrand = getBrand();
  const { productColorId, name, pdpUrl, brandId } = productDetail;
  const isProductBrandOfSameDomain = currentAppBrand.toUpperCase() === brandId.toUpperCase();
  const pdpAsPathUrl = pdpUrl.split('/p/')[1];
  if (!isProductBrandOfSameDomain) {
    updateAppTypeHandler({
      type: currentAppBrand.toLowerCase() === APP_TYPE.TCP ? APP_TYPE.GYMBOREE : APP_TYPE.TCP,
      params: {
        title: name,
        pdpUrl: pdpAsPathUrl,
        selectedColorProductId: productColorId,
        reset: true,
        ...otherParams,
      },
    });
  } else {
    navigation.navigate('ProductDetail', {
      title: name,
      pdpUrl: pdpAsPathUrl,
      selectedColorProductId: productColorId,
      reset: true,
      ...otherParams,
    });
  }
};

/**
 * Check whether deep linking is used
 * @param {*} URL
 */
export const isURISchemeUsed = (URL) => {
  if (URL.indexOf('tcp://') !== -1 || URL.indexOf('gym://') !== -1) {
    return true;
  }
  return false;
};

export const getEddSessionState = () => {};

export const getDefaultAccentColor = (isGymApp) => {
  const {
    colors: { DEFAULT_ACCENT_GYM, DEFAULT_ACCENT_TCP },
  } = theme;

  return isGymApp ? DEFAULT_ACCENT_GYM : DEFAULT_ACCENT_TCP;
};

const IMAGES_BADGE_PARAMS = '/l_ecom:assets:static:badge:';
const IMAGES_BADGE_SIZE_PARAMS = ',g_west,w_0.22,fl_relative/';

export const getBadgeParam = (isDynamicBadgeEnabled, styleType, styleQty) => {
  if (isDynamicBadgeEnabled) {
    switch (styleType) {
      case '0003':
        return isDynamicBadgeEnabled.dynamicBadgeSetEnabled
          ? `${IMAGES_BADGE_PARAMS}set${styleQty}${IMAGES_BADGE_SIZE_PARAMS}`
          : '';
      case '0006':
        return isDynamicBadgeEnabled.dynamicBadgeVirtualSetEnabled
          ? `${IMAGES_BADGE_PARAMS}set${styleQty}${IMAGES_BADGE_SIZE_PARAMS}`
          : '';
      case '0002':
        return isDynamicBadgeEnabled.dynamicBadgeMPACKEnabled
          ? `${IMAGES_BADGE_PARAMS}pack${styleQty}${IMAGES_BADGE_SIZE_PARAMS}`
          : '';
      case '0005':
        return isDynamicBadgeEnabled.dynamicBadgeVirtualMPACKEnabled
          ? `${IMAGES_BADGE_PARAMS}pack${styleQty}${IMAGES_BADGE_SIZE_PARAMS}`
          : '';
      default:
        return '';
    }
  }
  return '';
};

export const getRecommendationsObj = async (prodId) => {
  const recObj = await getValueFromAsyncStorage(RECOMMENDATION_GA_KEY);
  const recommObj = await JSON.parse(recObj);
  if (prodId && recommObj && recommObj[prodId]) {
    return recommObj[prodId];
  }
  return recommObj || {};
};

export const setRecommendationsObj = async (recObj) => {
  if (recObj && Object.keys(recObj).length) {
    const prevDataObj = await getValueFromAsyncStorage(RECOMMENDATION_GA_KEY);
    const prevData = JSON.parse(prevDataObj);
    if (prevData && prevData[recObj.recsProductId]) return;
    const recsObj = {
      [recObj.recsProductId]: recObj,
      ...prevData,
    };
    await setValueInAsyncStorage(RECOMMENDATION_GA_KEY, JSON.stringify(recsObj));
  }
};

export const clearRecommendationsObj = () => {
  removeValueFromAsyncStorage(RECOMMENDATION_GA_KEY);
};

const parseAndReturnNumber = (position, variation) => {
  const defaultReturnValue = 0;
  if (!isEmpty(position)) {
    const parsedReturnValue = parseInt(position[variation], 10);
    if (isFinite(parsedReturnValue)) return parsedReturnValue;
    return defaultReturnValue;
  }
  return defaultReturnValue;
};

export const getMediaBorderRadius = (top, bottom) => {
  return {
    borderTopLeftRadius: parseAndReturnNumber(top, 'media-top'),
    borderTopRightRadius: parseAndReturnNumber(top, 'media-top'),
    borderBottomLeftRadius: parseAndReturnNumber(bottom, 'media-bottom'),
    borderBottomRightRadius: parseAndReturnNumber(bottom, 'media-bottom'),
  };
};

export const getCtaBorderRadius = (top, bottom) => {
  const {
    colors: { WHITE },
  } = theme;
  return {
    borderTopLeftRadius: parseAndReturnNumber(top, 'cta-top'),
    borderTopRightRadius: parseAndReturnNumber(top, 'cta-top'),
    borderBottomLeftRadius: parseAndReturnNumber(bottom, 'cta-bottom'),
    borderBottomRightRadius: parseAndReturnNumber(bottom, 'cta-bottom'),
    backgroundColor: WHITE,
    borderWidth: 0.5,
    borderTopWidth: 0,
  };
};

export const getHeaderBorderRadius = (top, bottom) => {
  return {
    borderTopLeftRadius: parseAndReturnNumber(top, 'header-top'),
    borderTopRightRadius: parseAndReturnNumber(top, 'header-top'),
    borderBottomLeftRadius: parseAndReturnNumber(bottom, 'header-bottom'),
    borderBottomRightRadius: parseAndReturnNumber(bottom, 'header-bottom'),
  };
};

export const getSearchRedirectObj = () => {};

export const getBrandIcon = ({ itemBrand }) => {
  const gymboreeImage = require('../../../mobileapp/src/assets/images/gymboree-logo.png');
  const tcpImage = require('../../../mobileapp/src/assets/images/tcp-logo.png');
  const snjImage = require('../../../mobileapp/src/assets/images/snj-logo.png');
  switch (itemBrand) {
    case BrandSuffix.tcp:
      return tcpImage;
    case BrandSuffix.gym:
      return gymboreeImage;
    case BrandSuffix.snj:
      return snjImage;
    default:
      return tcpImage;
  }
};

export const getBlackBoxData = () => {
  return new Promise((resolve, reject) => {
    IovationModule.getBlackBoxData(
      (data) => {
        resolve(data);
      },
      (err) => {
        reject(err);
      }
    );
  });
};

export const cleanUpNonStdQueryString = (url) => url;

export const removeRecFromStorage = () => {};

export const isBagRoute = () => {};

export const getDate = () => {
  const date = new Date().getDate();
  const month = new Date().getMonth() + 1;
  const year = new Date().getFullYear();

  return `${date}/${month}/${year}`;
};

export const dayChange = async () => {
  const storedDate = await getValueFromAsyncStorage('CURRENT_DATE');
  if (storedDate === null) {
    await setValueInAsyncStorage('CURRENT_DATE', JSON.stringify(getDate()));
  }
  const slicedStoredDate = storedDate.slice(1, -1);
  const storedCounter = await getValueFromAsyncStorage('COUNTER');
  if (storedCounter === null) {
    const initialCounter = 1;
    await setValueInAsyncStorage('COUNTER', JSON.stringify(initialCounter));
  }
  // eslint-disable-next-line radix
  let parsedStoredCounter = parseInt(storedCounter);
  if (parsedStoredCounter > 6) {
    parsedStoredCounter = 1;
    await setValueInAsyncStorage('COUNTER', JSON.stringify(parsedStoredCounter));
  }
  if (slicedStoredDate.localeCompare(getDate()) !== 0) {
    parsedStoredCounter += 1;
    await setValueInAsyncStorage('CURRENT_DATE', JSON.stringify(getDate()));
    await setValueInAsyncStorage('COUNTER', JSON.stringify(parsedStoredCounter));
  }
};
export const routerReplace = () => {};
