// 9fbef606107a605d69c0edbcd8029e5d
/**
 *
 * @param {String} phoneNumber Phone Number to validate
 * @return {Boolean}
 */
export const validatePhoneNumber = (phoneNumber) => {
  return /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/i.test(phoneNumber);
};

/**
 *
 * @param {String} phoneNumber phoneNumber to convert
 * @return {String} Returns phoneNumber in format (XXX) XXX-XXXX
 */
export const formatPhoneNumber = (phoneNumber, previousValue) => {
  let normalizePhoneNum = phoneNumber.replace(/[\s()-]/g, '');
  const re = /^[0-9\b]+$/;
  if (!re.test(normalizePhoneNum)) {
    return normalizePhoneNum === '' ? '' : previousValue;
  }
  if (normalizePhoneNum.length > 3) {
    normalizePhoneNum = `(${normalizePhoneNum.slice(0, 3)}) ${normalizePhoneNum.slice(3)}`;
  }
  if (normalizePhoneNum.length > 9) {
    normalizePhoneNum = `${normalizePhoneNum.slice(0, 9)}-${normalizePhoneNum.slice(9)}`;
  }
  return normalizePhoneNum;
};
/**
 *
 * @param {String} value value to convert
 * @return {String} Returns value without spaces,tab and newlines
 */
export const removeSpaces = (value) => {
  return value && value.replace(/[\s]/g, '');
};

export default { validatePhoneNumber, formatPhoneNumber, removeSpaces };
