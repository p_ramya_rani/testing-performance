// 9fbef606107a605d69c0edbcd8029e5d 
const validatorDefaultMessages = {
  genericError: 'Please enter a valid value',
  required: 'This field is required',
};

export default validatorDefaultMessages;
