// 9fbef606107a605d69c0edbcd8029e5d 
export const ForterActionType = {};
export const ForterNavigationType = {};

const initializeForter = () => {
  // to be implemented
};

export const getForterDeviceID = () => {
  // to be implemented
};

export const trackForterNavigation = () => {
  // to be implemented
};

export const trackForterAction = () => {
  // to be implemented
};

export const trackForterCurrentLocation = () => {
  // to be implemented
};

export default initializeForter;
