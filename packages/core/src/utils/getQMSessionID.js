// 9fbef606107a605d69c0edbcd8029e5d 
/**
 *
 * TODO:
 * Copy the read cookie method from cookie.util.js file
 * Wee have issue in import on server files even in node version 12.x
 * Once we implement the 12.x changes, We will remove this file.
 *
 */

const QUNATUM_SESSION_ID = 'QuantumMetricSessionID';

const readCookie = key => {
  try {
    const name = `${key}=`;
    const decodedCookie = decodeURIComponent(document.cookie).split(';');
    for (let i = 0; i < decodedCookie.length; i += 1) {
      let c = decodedCookie[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return null;
  } catch (error) {
    return null;
  }
};

const getQMRefID = () => {
  const cookieValue = readCookie(QUNATUM_SESSION_ID);
  return cookieValue
    ? `${cookieValue.substring(0, 4).toUpperCase()}-${cookieValue.substring(4, 8).toUpperCase()}`
    : '';
};

module.exports = getQMRefID;
