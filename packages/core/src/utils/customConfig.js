// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @description: File to update configuration from code push functionality
 * Please do not commit anything here. This is purely for dynamic code push for the app.
 */
const Config = {
  tcp: {
    codePushVersionIos: '47.0.0',
    codePushVersionAndroid: '47.0.0',
  },
  gym: {
    codePushVersionIos: '27.0.0',
    codePushVersionAndroid: '27.0.0',
  },
  common: {},
  // Sample Data to create dynamic config
  // RWD_APP_API_DOMAIN_TCP: 'https://test6.childrensplace.com',
  // RWD_APP_API_IDENTIFIER_TCP: 'api',
  // RWD_APP_GRAPHQL_API_IDENTIFIER_TCP: 'graphql',
};

/**
 * @method - Code Push method to execute custom function during app resume or start
 */
const customFunction = () => {};

export { Config, customFunction };
