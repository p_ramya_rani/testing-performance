/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d

import differenceInDays from 'date-fns/differenceInDays';
import differenceInMinutes from 'date-fns/differenceInMinutes';
import format from 'date-fns/format';
import isValid from 'date-fns/isValid';
import parseISO from 'date-fns/parseISO';
import groupBy from 'lodash/groupBy';
import uniqBy from 'lodash/uniqBy';
import icons from '../config/icons';
import locators from '../config/locators';
import flagIcons from '../config/flagIcons';
import { API_CONFIG } from '../services/config';
import { getStoreRef, resetStoreRef } from './store.utils';
import { APICONFIG_REDUCER_KEY } from '../constants/reducer.constants';
import { parseDate } from './parseDate';
import { ROUTE_PATH } from '../config/route.config';
import constants from '../components/features/account/OrderDetails/OrderDetails.constants';
import createThemeColorPalette from '../../styles/themes/createThemeColorPalette';
import { MODULES, DEVICE_TYPE } from '../constants/modules.constants';
import {
  ORDER_ITEM_STATUS,
  AFTERPAY,
  GENERIC_ERROR,
} from '../components/features/account/CustomerHelp/CustomerHelp.constants';
import IMG_DATA_PLP from '../config/plpImage.config';
import isOptimizedDeleteJson from '../config/featureKillSwitch.config.json';
import { BOPIS_ITEM_AVAILABILITY_STATUS } from '../components/common/organisms/PickupStoreModal/PickUpStoreModal.constants';

// setting the apiConfig subtree of whole state in variable; Do we really need it ?
let apiConfig = null;
const buildId = process.env.NEXT_BUILD_ID || 'version-not-available';

const httpContext = !process.browser ? require('express-http-context') : '';

/**
 * This function returns the path of icons in static/images folder
 * @param {*} icon | String - Identifier for icons in assets
 */
export const getLocator = (locator) => {
  return locators[locator];
};

export const getRecommendation = (recommendationProduct) => {
  return recommendationProduct && recommendationProduct.length;
};

export const getVideoUrl = (url) => {
  if (url) {
    return String(url).match(/\.(mp4|webm|WEBM|MP4)$/g);
  }
  return false;
};

export const isMobileApp = () => {
  return typeof navigator !== 'undefined' && navigator.product === 'ReactNative';
};

export function isClient() {
  return typeof window !== 'undefined' && !isMobileApp();
}

const resetData = (resetProductDetailsDynamicData) => {
  return isClient() && resetProductDetailsDynamicData();
};

export const checkClosePickupModal = (closePickupModal) => {
  return closePickupModal && closePickupModal({ isModalOpen: false });
};

const displayModal = (params) => {
  const {
    fromPDP,
    isPDP,
    resetProductDetailsDynamicData,
    getDetails,
    newPackIdList,
    generalProductId,
    isPickup,
    onPickUpOpenClick,
    getQickViewSingleLoad,
    onQuickViewOpenClick,
    fromBagPage,
    productInfoFromBag,
    setInitialTCPStyleQty,
    fromPills,
    fromPage,
    alternateBrand,
    isBopisPickup,
  } = params;
  if (fromPDP || isPDP) {
    resetData(resetProductDetailsDynamicData);
    getDetails({
      productColorId: newPackIdList,
      isMultiPack: true,
      generalProductId,
      getDetailonClick: true,
      otherBrand: alternateBrand,
    });
  } else if (isPickup) {
    onPickUpOpenClick({
      productColorId: newPackIdList,
      colorProductId: newPackIdList,
      isMultiPack: true,
      generalProductId,
      getDetailonClick: true,
      isProductPickup: true,
      setInitialTCPStyleQty,
      fromPills,
      otherBrand: alternateBrand,
      isBopisPickup,
    });
  } else {
    getQickViewSingleLoad({ initalQuickViewLoad: false });
    onQuickViewOpenClick({
      colorProductId: newPackIdList,
      isMultiPack: true,
      generalProductId,
      getDetailonClick: true,
      fromBagPage,
      orderInfo: productInfoFromBag,
      fromPage,
      otherBrand: alternateBrand,
    });
  }
};

const closeModal = (isPickup, closePickupModal) => {
  if (!isPickup) {
    checkClosePickupModal(closePickupModal);
  }
};

export const onTabClickDetails = ({
  setSelectedMultipack,
  getDetails,
  currentProduct,
  onQuickViewOpenClick,
  fromPDP,
  fromPage,
  isPDP,
  e,
  item,
  singlePack,
  closePickupModal,
  setInitialTCPStyleQty,
  resetProductDetailsDynamicData,
  getQickViewSingleLoad,
  getDisableSelectedTab,
  isPickup,
  onPickUpOpenClick,
  fromBagPage,
  productInfoFromBag,
  availableTCPmapNewStyleId = [],
  fromPills,
  alternateBrand,
  isBopisPickup,
}) => {
  const { generalProductId } = currentProduct;
  const getUpdatedItem = item === singlePack ? 1 : item;
  closeModal(isPickup, closePickupModal);
  const dataVallue = () => {
    const { TCPmapNewStyleId } = currentProduct;
    const allStyleId = TCPmapNewStyleId.concat(availableTCPmapNewStyleId);
    let uniquePackItems = [];
    const filteredTCPmapNewStyleId = allStyleId.filter(
      (ele) => ele && ele !== null && ele !== 'undefined'
    );
    if (filteredTCPmapNewStyleId && filteredTCPmapNewStyleId.length > 0) {
      const findPackItem = filteredTCPmapNewStyleId.filter((element) => {
        const itemValue = item === singlePack ? 1 : item;
        return Number(element.split('#')?.[1]) === itemValue;
      });
      const packItemsArray = findPackItem.map((itemValue) => {
        return itemValue && itemValue.split('_')?.[0];
      });
      uniquePackItems = packItemsArray.filter(
        (ele, index) => packItemsArray.indexOf(ele) === index
      );
    }
    return Number(setInitialTCPStyleQty) === getUpdatedItem ? [generalProductId] : uniquePackItems;
  };
  const newPackIdList = dataVallue().join(' ');
  getDisableSelectedTab({ disabledTab: false });
  const params = {
    fromPDP,
    isPDP,
    resetProductDetailsDynamicData,
    getDetails,
    newPackIdList,
    generalProductId,
    isPickup,
    onPickUpOpenClick,
    getQickViewSingleLoad,
    onQuickViewOpenClick,
    fromBagPage,
    productInfoFromBag,
    setInitialTCPStyleQty,
    fromPills,
    fromPage,
    alternateBrand,
    isBopisPickup,
  };
  displayModal(params);
  e.preventDefault();
  setSelectedMultipack(item);
};

export const isIOSDevice = () => {
  return (
    typeof navigator !== 'undefined' &&
    navigator.platform &&
    !!navigator.platform.match(/iPhone|iPod|iPad/)
  );
};

export const checkMultiPackType = ({ TCPMultipackProductMapping, MULTI_PACK_TYPE, singlePack }) => {
  let filteredTCPMultipackProductMapping =
    TCPMultipackProductMapping &&
    TCPMultipackProductMapping.length &&
    TCPMultipackProductMapping.filter((ele) => ele && ele !== null && ele !== 'undefined');

  filteredTCPMultipackProductMapping =
    filteredTCPMultipackProductMapping && filteredTCPMultipackProductMapping.length > 0
      ? filteredTCPMultipackProductMapping.map((element) =>
          element === singlePack ? element : Number(element)
        )
      : filteredTCPMultipackProductMapping;

  filteredTCPMultipackProductMapping.sort((a, b) => a - b);
  const singlePackIndex = filteredTCPMultipackProductMapping.indexOf(
    Number(MULTI_PACK_TYPE.SINGLE)
  );
  if (singlePackIndex > -1) {
    filteredTCPMultipackProductMapping[singlePackIndex] = singlePack;
  }
  return filteredTCPMultipackProductMapping;
};

export const getSelectedMultipack = (
  selectedMultipack,
  TCPStyleQTY,
  MULTI_PACK_TYPE,
  singlePack
) => {
  let multipackToSelect = TCPStyleQTY || selectedMultipack;
  multipackToSelect =
    Number(multipackToSelect) === Number(MULTI_PACK_TYPE.SINGLE) || multipackToSelect === singlePack
      ? singlePack
      : Number(multipackToSelect);

  return multipackToSelect;
};

export const findMultipacks = (
  TCPStyleType,
  TCPMultipackProductMapping,
  singlePack,
  TCPStyleQTY,
  MULTI_PACK_TYPE,
  singlePageLoad
) => {
  if (TCPMultipackProductMapping?.indexOf(TCPStyleQTY) < 0 && singlePageLoad) {
    TCPMultipackProductMapping.push(TCPStyleQTY);
  }
  return checkMultiPackType({
    TCPStyleType,
    TCPMultipackProductMapping,
    MULTI_PACK_TYPE,
    singlePack,
  });
};

export const isSafariBrowser = () => {
  return (
    isClient() &&
    navigator &&
    navigator.userAgent &&
    navigator.userAgent.indexOf('Safari') !== -1 &&
    !window.chrome
  );
};

export const getSafariVersion = () => {
  let versionString = '0.0.0';
  if (isClient() && navigator && navigator.userAgent) {
    versionString = navigator.userAgent.match(/OS (\d)?\d_\d(_\d)?/i);
    if (versionString) {
      versionString = versionString[0].replace(/_/g, '.').replace('OS ', '');
    }
  }
  return versionString;
};

/**
 * @see ServerToClientRenderPatch.jsx - Do not use this to determine rendering of a component or part of a component. The server
 *  side and client side hydration should be the same. If this is needed use ServerToClientRenderPatch.jsx.
 */
export function isTouchClient() {
  return typeof window !== 'undefined' && !!('ontouchstart' in window);
}

export const isServer = () => {
  return typeof window === 'undefined' && !isMobileApp();
};

/**
 * @summary Get the api config if already created or else creates one.
 * @returns {Object} apiConfig - Api config to be utilized for brand/channel/locale config
 */
export const getAPIConfig = () => {
  // When apiConfig is null (the very first time) or is an empty object, derive value from store..
  const validApiConfigObj = !apiConfig || (apiConfig && !Object.keys(apiConfig).length);
  // This check is to make sure that same instance of apiConfig for different country/brand ssr requests
  const deriveApiConfigObj = validApiConfigObj || !isClient();

  if (deriveApiConfigObj) {
    apiConfig = (getStoreRef() && getStoreRef().getState()[APICONFIG_REDUCER_KEY]) || {};
    if (isClient()) {
      resetStoreRef(); // This is to make module variable reduxStore as null
    }
  }

  return apiConfig;
};

/**
 * @function resetApiConfig
 * This method resets locally stored api config
 *
 */
export const resetApiConfig = () => {
  apiConfig = null;
};

/**
 * This function returns the static files path with the buildId in place
 * @param {String} filePath path inside the /static directory
 */
export const getStaticFilePath = (filePath, noCookieLess) => {
  const apiConfigObj = getAPIConfig();
  const assetEndpoint =
    typeof window === 'undefined'
      ? process.env.STATIC_ASSET_PATH
      : window.assetEndpointGlobal || apiConfigObj.staticAssetHost;

  if (!filePath) return filePath;

  // Following Regex to test if filePath is absolute and return the same value if true
  if (/^(?:[a-z]+:)?\/\//i.test(filePath)) {
    return filePath;
  }

  if (assetEndpoint && !noCookieLess) {
    return `${assetEndpoint}/static/${buildId}/${filePath}`;
  }
  return `/static/${buildId}/${filePath}`;
};

/**
 * This function returns the static files path with the buildId in place
 * @param {String} filePath path inside the /static directory
 */
export const getStaticPageFilePath = (pageFile) => {
  return `/_next${getStaticFilePath(`pages/${pageFile}`, 'noCookieLess')}`;
};

/**
 * This function returns the static images path which have been moved to image server
 * @param {String} imagepath path inside the /static directory
 */
export const getImageFilePath = (props) => {
  const { assetHostTCP } = props ? props.theme : getAPIConfig();
  const imageHostPath = assetHostTCP || process.env.WEB_DAM_HOST;
  return `${imageHostPath}/ecom/assets/static/icon`;
};

/**
 * This function returns the path of icons in static/images folder
 * @param {*} icon | String - Identifier for icons in assets
 */
export const getIconPath = (icon, props) => {
  return props
    ? `${getImageFilePath(props)}/${icons[icon]}`
    : `${getImageFilePath()}/${icons[icon]}`;
};

/**
 * This function returns the path of flag icons in static/images/flag folder
 * @param {*} icon | String - Country Code Identifier eg. US for USA
 */
export const getFlagIconPath = (code) => {
  return `${getImageFilePath()}/${flagIcons[code]}`;
};

export const getCacheKeyForRedis = (cacheId) => {
  const { brandId, siteId, channelId = 'WEB', envId } = getAPIConfig();
  const keySep = '_';
  return `${envId}${keySep}${brandId}${keySep}${siteId}${keySep}${channelId}${keySep}${cacheId}`;
};

export const getBrand = () => {
  return getAPIConfig().brandId;
};

export const getBrandNameFromHref = (asPath) => {
  const asPathSplitArray = asPath && asPath.split('$');
  const brandFromPath = asPathSplitArray && asPathSplitArray[1];
  const brandSplitArray = brandFromPath && brandFromPath.split('=');
  const brandSubString = brandSplitArray && brandSplitArray[1];
  const brandNameSplitArray = brandSubString && brandSubString.split('?');
  return brandNameSplitArray && brandNameSplitArray[0];
};

export const isTCP = () => {
  const { brandId } = getAPIConfig();
  return brandId === API_CONFIG.brandIds.tcp;
};

export const isSNJ = () => {
  const { brandId } = getAPIConfig();
  return brandId === API_CONFIG.brandIds.snj;
};

export const isCanada = () => {
  const { siteId } = getAPIConfig();
  return siteId === API_CONFIG.siteIds.ca;
};

export const isUsOnly = () => {
  const { siteId } = getAPIConfig();
  return siteId === API_CONFIG.siteIds.us;
};

export const bindAllClassMethodsToThis = (obj, namePrefix = '', isExclude = false) => {
  const prototype = Object.getPrototypeOf(obj);
  // eslint-disable-next-line
  for (let name of Object.getOwnPropertyNames(prototype)) {
    const descriptor = Object.getOwnPropertyDescriptor(prototype, name);
    const isGetter = descriptor && typeof descriptor.get === 'function';
    // eslint-disable-next-line
    if (isGetter) continue;
    if (
      typeof prototype[name] === 'function' && name !== 'constructor' && isExclude
        ? !name.startsWith(namePrefix)
        : name.startsWith(namePrefix)
    ) {
      // eslint-disable-next-line
      obj[name] = prototype[name].bind(obj);
    }
  }
};

export const capitalize = (string) => {
  return string.replace(/\b\w/g, (l) => l.toUpperCase());
};

export function capitalizeWords(text = '') {
  const wordsArray = text.toLowerCase().split(' ');
  let capsArray = [];
  if (wordsArray && wordsArray.length) {
    capsArray = wordsArray.map((word = ' ') => {
      return word[0] && word[0].toUpperCase() + word.slice(1);
    });
  }
  return capsArray.join(' ');
}

/**
 * @method getPromotionalMessage - this function checks whether the user is PLCC or not and
 *         returns the message respectively
 * @param isPlcc  boolean value for plcc user
 * @param {handlers}  the messages containing both plcc user message and non-plcc user message
 */
export const getPromotionalMessage = (isPlcc, handlers) => {
  if (!!handlers.promotionalPLCCMessage || !!handlers.promotionalMessage) {
    return isPlcc ? handlers.promotionalPLCCMessage : handlers.promotionalMessage;
  }
  return null;
};

export const toTimeString = (est, perfect = false) => {
  let hh = est.getHours();
  let mm = est.getMinutes();
  const ampm = hh >= 12 ? ' pm' : ' am';
  hh %= 12;
  hh = hh > 0 ? hh : 12;
  mm = mm < 10 ? `0${mm}` : mm;
  if (hh === 11 && mm === 59 && ampm === ' pm') {
    return 'Midnight';
  }
  return !perfect ? `${hh}:${mm}${ampm}` : `${hh}${ampm}`;
};

/**
 * This function will format the Date Object in mm/dd/yy format
 * @param {object} date to be formatted
 */
export const formatDate = (date) => {
  const month = `0${date.getMonth() + 1}`.substr(-2);
  const dateStr = `0${date.getDate()}`.substr(-2);
  const year = date.getFullYear().toString();
  return `${month}/${dateStr}/${year}`;
};

/**
 * This function will format the YYYY-MM-DD Date from the native browser date picker into MM/DD/YYYY format
 * @param {object} date to be formatted
 */
export const formatDateFromBrowser = (date) => {
  const month = date.substr(5, 2);
  const dateStr = date.substr(8, 2);
  const year = date.substr(0, 4);
  return `${month}/${dateStr}/${year}`;
};

/**
 * This function will check for valid date
 * @param {object} date to be check
 */
/* eslint-disable no-restricted-globals */
export const isValidDate = (date) => {
  return date instanceof Date && !isNaN(date);
};

export const isGymboree = () => {
  const { brandId } = getAPIConfig();
  return brandId === API_CONFIG.brandIds.gym;
};

export const safelyParseJSON = (json) => {
  let parsed;
  try {
    parsed = JSON.parse(json);
  } catch (e) {
    parsed = {};
  }
  return parsed;
};

const GOOGLE_PLACE_PARTS = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  sublocality_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name',
};

const GOOGLE_PLACE_PARTS_APP = {
  street_number: 'shortName',
  route: 'shortName',
  locality: 'shortName',
  administrative_area_level_1: 'shortName',
  sublocality_level_1: 'shortName',
  country: 'shortName',
  postal_code: 'shortName',
};

const addressTypeMap = {
  street_number: 'streetNumber',
  route: 'streetName',
  locality: 'city',
  sublocality_level_1: 'city',
  administrative_area_level_1: 'state',
  country: 'country',
  postal_code: 'zip',
};

export const getAddressFromPlace = (place, inputValue) => {
  const address = {
    streetNumber: '',
    streetName: '',
    street: '',
    city: '',
    state: '',
    country: '',
    zip: '',
  };
  if (typeof place.address_components === 'undefined') {
    return address;
  }

  const PLACE_PARTS = isMobileApp() ? GOOGLE_PLACE_PARTS_APP : GOOGLE_PLACE_PARTS;

  for (let i = 0; i < place.address_components.length; i += 1) {
    const addressType = place.address_components[i].types[0];
    if (PLACE_PARTS[addressType]) {
      const val = place.address_components[i][PLACE_PARTS[addressType]];
      address[addressTypeMap[addressType]] = val;
    }
  }
  if (!address.streetNumber) {
    const regex = RegExp(`^(.*)${address.streetName.split(' ', 1)[0]}`);
    const result = regex.exec(inputValue);
    const inputNum = Array.isArray(result) && result[1] && Number(result[1]);

    if (!isNaN(inputNum) && parseInt(inputNum, 10) === inputNum) {
      address.streetNumber = inputNum;
    }
  }

  address.street = `${address.streetNumber} ${address.streetName}`.trim();

  return address;
};

export const formatAddress = (useraddress) => {
  const address = useraddress || {};
  return {
    firstName: address.firstName,
    lastName: address.lastName,
    addressLine: [address.address1, address.address2 || ''],
    city: address.city,
    state: address.state,
    country: address.country,
    zipCode: address.zip,
    phone1: address.phoneNumber,
  };
};

export const formatPhoneNumber = (phone) => {
  if (phone) return `(${phone.slice(0, 3)}) ${phone.slice(3, 6)}-${phone.slice(6, 15)}`;
  return '';
};

export const hashValuesReplace = (str, utilArr) => {
  let finalString = str;
  utilArr.map((obj) => {
    finalString = finalString && finalString.replace(obj.key, !obj.value ? '' : obj.value);
    return finalString;
  });
  return finalString;
};

export const utilPdpArrayHeader = (productNumber, { brand, lang, site }) => {
  return [
    {
      key: '#style-no#',
      value: productNumber,
    },
    {
      key: '#brand#',
      value: brand,
    },
    {
      key: '#lang#',
      value: lang,
    },
    {
      key: '#site#',
      value: site,
    },
  ];
};

export const MONTH_SHORT_FORMAT = {
  JAN: 'Jan',
  FEB: 'Feb',
  MAR: 'Mar',
  APR: 'Apr',
  MAY: 'May',
  JUN: 'Jun',
  JUL: 'Jul',
  AUG: 'Aug',
  SEP: 'Sep',
  OCT: 'Oct',
  NOV: 'Nov',
  DEC: 'Dec',
};

export const getBirthDateOptionMap = () => {
  const monthOptionsMap = [
    {
      id: '1',
      displayName: MONTH_SHORT_FORMAT.JAN,
    },
    {
      id: '2',
      displayName: MONTH_SHORT_FORMAT.FEB,
    },
    {
      id: '3',
      displayName: MONTH_SHORT_FORMAT.MAR,
    },
    {
      id: '4',
      displayName: MONTH_SHORT_FORMAT.APR,
    },
    {
      id: '5',
      displayName: MONTH_SHORT_FORMAT.MAY,
    },
    {
      id: '6',
      displayName: MONTH_SHORT_FORMAT.JUN,
    },
    {
      id: '7',
      displayName: MONTH_SHORT_FORMAT.JUL,
    },
    {
      id: '8',
      displayName: MONTH_SHORT_FORMAT.AUG,
    },
    {
      id: '9',
      displayName: MONTH_SHORT_FORMAT.SEP,
    },
    {
      id: '10',
      displayName: MONTH_SHORT_FORMAT.OCT,
    },
    {
      id: '11',
      displayName: MONTH_SHORT_FORMAT.NOV,
    },
    {
      id: '12',
      displayName: MONTH_SHORT_FORMAT.DEC,
    },
  ];

  const yearOptionsMap = [];
  const dayOptionsMap = [];
  const nowYear = new Date().getFullYear();

  for (let i = 1900; i < nowYear - 17; i += 1) {
    yearOptionsMap.push({
      id: i.toString(),
      displayName: i.toString(),
    });
  }

  for (let i = 1; i < 32; i += 1) {
    if (i <= 9) {
      i = 0 + i;
    }
    dayOptionsMap.push({
      id: i.toString(),
      displayName: i.toString(),
    });
  }

  return {
    daysMap: dayOptionsMap,
    monthsMap: monthOptionsMap,
    yearsMap: yearOptionsMap,
  };
};

/**
 * @function calculateAge
 * @param { string } month
 * @param { string } year
 * This function will calculate the age based on the month and year of birth and will add 'mo' or 'yo' based on age in months or years
 *
 */
export const calculateAge = (month, year) => {
  const currentDate = new Date();
  const currentMonth = currentDate.getMonth() + 1;
  const currentYear = currentDate.getFullYear();
  let age = currentYear - year;
  if (currentYear.toString() === year && month > currentMonth) {
    return '0 mo';
  }
  if (month > currentMonth && age > 0) {
    age -= 1;
  }
  if (age === 0) {
    if (month > currentMonth) {
      age = `${12 - month + currentMonth} mo`;
    } else {
      age = `${currentMonth - month} mo`;
    }
  } else {
    age += ' yo';
  }
  return age;
};

export const childOptionsMap = () => {
  const currentYear = new Date().getFullYear();
  const yearOptionsMap = Array(18)
    .fill(currentYear)
    .map((e, index) => {
      const year = e - index;
      return {
        id: year.toString(),
        displayName: year.toString(),
      };
    });
  return {
    genderMap: [
      {
        id: '01',
        displayName: 'Boy',
      },
      {
        id: '0',
        displayName: 'Girl',
      },
    ],
    yearsMap: yearOptionsMap,
  };
};

const returnValue = (condition, conditionTrueValue, conditionFalseValue) => {
  return condition ? conditionTrueValue : conditionFalseValue;
};

/**
 *
 * @param {object} labelState object in which key needs to be searched
 * @param {string} labelKey string whose value
 * @param {string} subCategory label subCategory
 * @param {string} category label category
 * @param {boolean} isreferredContent is label referred content (moduleXContent)
 * This function will return label value if labelKey is present in the object
 * or labelKey itself if its not present in the labelState.
 */
export const getLabelValue = (
  labelState,
  labelKey,
  subCategory,
  category,
  isreferredContent = false
) => {
  if (typeof labelState !== 'object') {
    return typeof labelKey !== 'string' ? returnValue(isreferredContent, [], '') : labelKey; // for incorrect params return empty string
  }
  let labelValue = '';

  // if category is passed, then subCategory should also be present for ex. getLabelValue(labels, 'lbl_success_message', 'payment', 'account'), where labels = [reduxStore].Labels
  if (
    category &&
    typeof labelState[category] === 'object' &&
    typeof labelState[category][subCategory] === 'object'
  ) {
    labelValue = labelState[category][subCategory][labelKey];
  } else if (subCategory && typeof labelState[subCategory] === 'object') {
    // in case label object contain category, then only subCategory is needed for ex. get getLabelValue(labels, 'lbl_success_message', 'payment') where labels = [reduxStore].Labels.account
    labelValue = labelState[subCategory][labelKey];
  } else {
    // in case label object contain category & subCategory both, for ex. get getLabelValue(labels, 'lbl_success_message') where labels = [reduxStore].Labels.account.payment
    labelValue = labelState ? labelState[labelKey] : '';
  }

  return typeof labelValue === 'string'
    ? labelValue
    : returnValue(isreferredContent, labelValue, labelKey);
};

// eslint-disable-next-line
export const getErrorSelector = (state, labels, errorKey) => {
  const errorParameters = state && state.getIn(['errorParameters', '0']);
  const errorCode = state && (state.get('errorKey') || state.get('errorCode'));
  if (
    (errorParameters && getLabelValue(labels, `${errorKey}_${errorParameters}`)) ||
    (errorCode && getLabelValue(labels, `${errorKey}_${errorCode}`))
  ) {
    if (errorParameters) {
      return getLabelValue(labels, `${errorKey}_${errorParameters}`);
    }
    if (`${errorKey}_${errorCode}` === getLabelValue(labels, `${errorKey}_${errorCode}`)) {
      return GENERIC_ERROR;
    }
    return getLabelValue(labels, `${errorKey}_${errorCode}`);
  }
  const defaultError =
    (state && state.getIn(['errorMessage', '_error'])) || getLabelValue(labels, `${errorKey}`);
  return defaultError !== errorKey ? defaultError : GENERIC_ERROR;
};

export const generateUniqueKeyUsingLabel = (label) => {
  return label.replace(/\s/g, '_');
};

export const sanitizeEntity = (string) => {
  return string && typeof string === 'string'
    ? string
        .replace(/&amp;/gi, '&')
        .replace(/&quot;/gi, '"')
        .replace(/&ldquo;/gi, '"')
        .replace(/&acute;/gi, '"')
        .replace(/&prime;/gi, '"')
        .replace(/&bdquo;/gi, '"')
        .replace(/&ldquot;/gi, '"')
        .replace(/\\u0027/gi, "'")
        .replace(/&lsquot;/gi, '"')
        .replace(/%20/gi, ' ')
    : string;
};

export const checkPhone = (phone) => {
  return phone.replace('(', '').replace(')', '').replace('-', '').replace(' ', '').trim() || phone;
};

export const formatPhone = (phoneNum, hyphenFormat) => {
  let phone = phoneNum;
  let countryCode = '';
  if (typeof phone === 'number') {
    phone = phone.toString();
  }

  phone = checkPhone(phone);

  if (hyphenFormat && phone.length === 11) {
    countryCode = phone.substr(0, 1);
    phone = phone.substr(1); // Skip the first number for the phone number as it is the code
  }
  if (phone.length === 10) {
    const arrayPhone = [];
    let prefix;
    for (let i = 0; i < phone.length; i += 1) {
      arrayPhone.push(phone.substr(i, 1));
    }
    if (hyphenFormat) {
      prefix = `${arrayPhone[0]} ${arrayPhone[1]} ${arrayPhone[2]}-`;
    } else {
      prefix = `( ${arrayPhone[0]} ${arrayPhone[1]} ${arrayPhone[2]} + )`;
    }

    const sufix = `${arrayPhone[3]}
      ${arrayPhone[4]}
      ${arrayPhone[5]}
      -
      ${arrayPhone[6]}
      ${arrayPhone[7]}
      ${arrayPhone[8]}
      ${arrayPhone[9]}`;
    if (hyphenFormat) {
      phone = (countryCode ? `${countryCode}-` : '') + prefix + sufix;
    } else {
      phone = `${prefix} ${sufix}`;
    }
    return phone;
  }

  return phone;
};

export const getSiteId = () => {
  const paths = window.location.pathname.split('/', 2);
  return paths[1];
};
// eslint-disable-next-line
export const parseStoreHours = (hoursOfOperation) => {
  let carryOverClosingHour;
  const result = [];
  const keysOfHoursOfOperation = Object.keys(hoursOfOperation);
  keysOfHoursOfOperation.forEach((key) => {
    const day = hoursOfOperation[key];
    // store was opened on the previous date and closing today,
    // so we need to push it as the first opening time of today
    if (carryOverClosingHour) {
      const date = carryOverClosingHour.split(' ')[0];
      day.availability.unshift({
        from: `${date} 00:00:00`,
        to: carryOverClosingHour,
      });
      carryOverClosingHour = null;
    }

    const storeHours = {
      dayName: day.nick.toUpperCase() || '',
      openIntervals: day.availability.map((availability) => {
        const parsableFromDate = availability.from.replace('T', ' ');
        let parsableToDate = availability.to.replace('T', ' ');
        const fromDate = parseDate(parsableFromDate);
        const toDate = parseDate(parsableToDate);
        const isSameDay =
          fromDate.getFullYear() === toDate.getFullYear() &&
          fromDate.getMonth() === toDate.getMonth() &&
          fromDate.getDate() === toDate.getDate();

        if (!isSameDay) {
          // save carry over for next day
          carryOverClosingHour = parsableToDate;
          // set closing hour at 23.59.59 of today
          parsableToDate = `${fromDate.getFullYear()}  - ${
            fromDate.getMonth() + 1
          } - ${fromDate.getDate()} 23:59:59`;
        }

        return {
          fromHour: parsableFromDate,
          toHour: parsableToDate,
        };
      }),
      isClosed: day.availability[0].status === 'closed',
    };

    result.push(storeHours);
  });

  return result;
};

export const parseBoolean = (bool) => {
  return bool === true || bool === '1' || bool === 1 || (bool || '').toUpperCase() === 'TRUE';
};

export const getFormSKUValue = (formValue) => {
  return {
    color: (typeof formValue.color === 'object' && formValue.color.name) || formValue.color,
    size: (typeof formValue.Size === 'object' && formValue.Size.name) || formValue.Size,
    quantity:
      (typeof formValue.Quantity === 'object' && formValue.Quantity.name) || formValue.Quantity,
    fit:
      (formValue.Fit && typeof formValue.Fit === 'object' && formValue.Fit.name) || formValue.Fit,
  };
};

/**
 * This function returns query params for internal navigation from cms url
 * @param {*} url
 */
export const getQueryParamfromCMSUrl = (url, route) => {
  const urlItems = url.split(route);
  // replacing question(?) mark with ampersand(&) for common queryParam separator.
  return urlItems[1].replace('?', '&');
};

/**
 * This function configure url for Next/Link using CMS defined url string
 */
// skipHttp - it is used to skip the following types  of sample urls which are complete and have been configured in unbx
// and need to be returned as 'to path' like '/content', '/help-center' etc in router push method
// https://xxx.childrensplace.com/ca/help-center/faq
// https://xxx.childrensplace.com/ca/help-center/#returnExchangePolicyli
/* eslint-disable complexity */
// eslint-disable-next-line sonarjs/cognitive-complexity
export const configureInternalNavigationFromCMSUrl = (url, skipHttp = false) => {
  if (!skipHttp && /^http/.test(url)) {
    return url;
  }
  const plpRoute = `${ROUTE_PATH.plp.name}/`;
  const pdpRoute = `${ROUTE_PATH.pdp.name}/`;
  const searchRoute = `${ROUTE_PATH.search.name}/`;
  const staticContentRoute = `${ROUTE_PATH.content.name}/`;
  const helpCenterRoute = `${ROUTE_PATH.helpCenter.name}/`;
  const bundleDetailRoute = `${ROUTE_PATH.bundleDetail.name}/`;
  const outfitRoute = `${ROUTE_PATH.outfit.name}/`;
  const homeRoute = `${ROUTE_PATH.home}/`;
  const accountRoute = `${ROUTE_PATH.account.name}/`;
  const storeLocatorRoute = `${ROUTE_PATH.storeLocator.name}`;

  if (url.includes(storeLocatorRoute)) {
    return storeLocatorRoute;
  }
  if (url.includes(plpRoute)) {
    const queryParam = getQueryParamfromCMSUrl(url, plpRoute);
    return `${ROUTE_PATH.plp.name}?${ROUTE_PATH.plp.param}=${queryParam}`;
  }
  if (url.includes(pdpRoute)) {
    const queryParam = getQueryParamfromCMSUrl(url, pdpRoute);
    return `${ROUTE_PATH.pdp.name}?${ROUTE_PATH.pdp.param}=${queryParam}`;
  }
  if (url.includes(searchRoute)) {
    const queryParam = getQueryParamfromCMSUrl(url, searchRoute);
    return `${ROUTE_PATH.search.name}?${ROUTE_PATH.search.param}=${queryParam}`;
  }
  if (url.includes(staticContentRoute)) {
    const queryParam = getQueryParamfromCMSUrl(url, staticContentRoute);
    return `${ROUTE_PATH.content.name}?${ROUTE_PATH.content.param}=${queryParam}`;
  }
  if (url.includes(helpCenterRoute)) {
    const queryParam = getQueryParamfromCMSUrl(url, helpCenterRoute);
    return `${ROUTE_PATH.helpCenter.name}?${ROUTE_PATH.helpCenter.param}=${queryParam}`;
  }
  if (url.includes(bundleDetailRoute)) {
    const queryParam = getQueryParamfromCMSUrl(url, bundleDetailRoute);
    return `${ROUTE_PATH.bundleDetail.name}?${ROUTE_PATH.bundleDetail.param}=${queryParam}`;
  }
  if (url.includes(outfitRoute) && !isMobileApp()) {
    const outfitParams = url.split('/');
    return (
      outfitParams.length &&
      `${ROUTE_PATH.outfit.name}?outfitId=${
        outfitParams[outfitParams.length - 2]
      }&vendorColorProductIdsList=${outfitParams[outfitParams.length - 1]}`
    );
  }
  if (url.includes(homeRoute)) {
    const queryParam = getQueryParamfromCMSUrl(url, homeRoute);
    return `${ROUTE_PATH.home}?target=${queryParam}`;
  }
  if (url.includes(accountRoute)) {
    const queryParam = getQueryParamfromCMSUrl(url, accountRoute);
    const accountParams = url.split('/');
    const accountParamsCount = accountParams && accountParams.length;
    let updatedQueryParams = '';
    if (accountParamsCount > 1) {
      for (let i = 2; i < accountParams.length; i += 1) {
        updatedQueryParams += `${ROUTE_PATH.account.params[i - 2]}=${accountParams[i]}${
          i === accountParamsCount ? '' : '&'
        }`;
      }
    }
    return `${ROUTE_PATH.account.name}?${updatedQueryParams}${queryParam}`;
  }
  return url;
};

const WEEK_DAYS = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
const WEEK_DAYS_SMALL = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const MONTHS = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
const MONTHS_SMALL = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

/**
 * @method getDateInformation
 * @desc returns day, month and day of the respective date provided
 * @param {string} date date which is to be mutated
 * @param {upperCase} date determines case
 */

export const getDateInformation = (date, upperCase) => {
  const currentDate = date ? new Date(date) : new Date();
  return {
    // added a case for upper and lower case values
    day: upperCase ? WEEK_DAYS[currentDate.getDay()] : WEEK_DAYS_SMALL[currentDate.getDay()],
    month: upperCase ? MONTHS[currentDate.getMonth()] : MONTHS_SMALL[currentDate.getMonth()],
    date: currentDate.getDate(),
  };
};

export function buildStorePageUrlSuffix(storeBasicInfo) {
  const { id, storeName, address } = storeBasicInfo;
  return [storeName, address.state, address.city, address.zipCode, id]
    .join('-')
    .toLowerCase()
    .replace(/ /g, '');
}

export const extractFloat = (currency) => {
  try {
    return !currency
      ? 0
      : parseFloat(parseFloat(currency.toString().match(/[+-]?\d+(\.\d+)?/g)[0]).toFixed(2));
  } catch (e) {
    return 0;
  }
};

/* @method flattenArray - this function takes takes array of array and merge into single array
 * @param arr { Array } Array of Array
 * @return {Array}  return array
 */
export const flattenArray = (arr) => {
  return arr.reduce((flat, toFlatten) => {
    return flat.concat(Array.isArray(toFlatten) ? flattenArray(toFlatten) : toFlatten);
  }, []);
};

export const getModifiedLanguageCode = (id) => {
  switch (id) {
    case 'en':
      return 'en_US';
    case 'es':
      return 'es_ES';
    case 'fr':
      return 'fr_FR';
    default:
      return id;
  }
};

/**
 * @method getTranslateDateInformation
 * @desc returns day, month and day of the respective date provided
 * @param {string} date date which is to be mutated
 * @param {upperCase} locale use for convert locate formate
 */
export const getTranslateDateInformation = (
  date,
  language,
  dayOption = {
    weekday: 'short',
  },
  monthOption = {
    month: 'short',
  }
) => {
  // replaced - with / to fix date parsing issue in Safari
  const dateStr = typeof date === 'string' ? date.replace(/-/g, '/') : date;
  const localeType = language ? getModifiedLanguageCode(language).replace('_', '-') : 'en';
  const currentDate = dateStr ? new Date(dateStr) : new Date();
  return {
    day: new Intl.DateTimeFormat(localeType, dayOption).format(currentDate),
    month: new Intl.DateTimeFormat(localeType, monthOption).format(currentDate),
    date: currentDate.getDate(),
    year: currentDate.getFullYear(),
  };
};

const getTranslateDateMobileInformation = (date) => {
  // replaced - with / to fix date parsing issue in Safari
  const dateStr = typeof date === 'string' ? date.replace(/-/g, '/') : date;
  const currentDate = dateStr ? new Date(dateStr) : new Date();
  return {
    day: WEEK_DAYS_SMALL[currentDate.getDay()],
    month: MONTHS_SMALL[currentDate.getMonth()],
    date: currentDate.getDate(),
    year: currentDate.getFullYear(),
  };
};

export const mobileOrderDateFormatted = (orderDate) => {
  const { day, month, date } = getTranslateDateMobileInformation(orderDate);

  return `${day}, ${month} ${date}`;
};
export const getMobileFormatedOrderDate = (orderDate) => {
  const { year, month, date } = getTranslateDateMobileInformation(orderDate);

  return `${month} ${date}, ${year}`;
};

export const orderDateFormatted = (orderDate) => {
  const { language } = getAPIConfig();
  const { day, month, date } = getTranslateDateInformation(
    orderDate,
    language,
    {
      weekday: 'short',
    },
    {
      month: 'short',
    }
  );

  return `${day}, ${month} ${date}`;
};

/**
 * Helper for proper quotations in script string output.
 * This is a template literal tag function.
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
 */
export function stringify(strings, ...values) {
  return strings.reduce(
    (result, str, i) => result + str + (i < values.length ? JSON.stringify(values[i]) : ''),
    ''
  );
}

/**
 * Function to add number of days to a date
 * @param {Date} date The date object
 * @param {number} days The number of days to be added
 * @returns {Date} The future date
 */
export const addDays = (date, days) => {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
};

/**
 * Check if
 * @param {Date} date1 The date one object
 * @param {Date} date2 The date two object
 */
export const isPastStoreHours = (date1, date2) => {
  const date1HH = date1.getHours();
  const date2HH = date2.getHours();
  if (date2HH > date1HH) {
    return true;
  }
  if (date2HH === date1HH) {
    const date1MM = date1.getMinutes();
    const date2MM = date2.getMinutes();
    if (date2MM > date1MM) {
      return true;
    }
    return false;
  }

  return false;
};

/**
 * Function to parse the store timing in correct format
 * @param {String} dateString Non UTC Format Date
 */
export const parseUTCDate = (dateString) => {
  const dateParams = dateString.replace(/ UTC/, '').split(/[\s-:]/);
  dateParams[1] = (parseInt(dateParams[1], 10) - 1).toString();

  return new Date(Date.UTC(...dateParams));
};

/**
 * Function to get the stores hours based on the current date
 * @param {Array} intervals The store hours array
 * @param {Date} currentDate The current date to be checked against
 */
export const getCurrentStoreHours = (intervals = [], currentDate = new Date()) => {
  let selectedInterval = intervals.filter((hour) => {
    const toInterval = hour && hour.openIntervals[0] && hour.openIntervals[0].toHour;
    const parsedDate = new Date(parseUTCDate(toInterval));
    return (
      parsedDate.getDate() === currentDate.getDate() &&
      parsedDate.getMonth() === currentDate.getMonth() &&
      parsedDate.getFullYear() === currentDate.getFullYear()
    );
  });
  // Fallback for Date and month not matching.
  // We check day and year instead.
  if (!selectedInterval.length) {
    selectedInterval = intervals.filter((hour) => {
      const toInterval = hour && hour.openIntervals[0] && hour.openIntervals[0].toHour;
      const parsedDate = new Date(parseUTCDate(toInterval));
      return (
        parsedDate.getDay() === currentDate.getDay() &&
        parsedDate.getFullYear() === currentDate.getFullYear()
      );
    });
  }
  return selectedInterval;
};

/**
 * Function to get the store opening or open until hours data
 * @param {object} hours The hours object of the store
 * @param {object} labels The store locator labels
 * @param {object} currentDate The date to be compared with
 * @returns {string} The time when the store next opens or time it is open till
 */

const getTimeInHourAndMinute = (selectedDateToHour, currentDate) => {
  const date = currentDate.getDate();
  const month = currentDate.getMonth();
  const year = currentDate.getFullYear();
  const storeDateTime = selectedDateToHour;
  let selectedDate = storeDateTime.setDate(date);
  selectedDate = storeDateTime.setMonth(month);
  selectedDate = storeDateTime.setFullYear(year);
  let diff = currentDate - selectedDate;
  if (diff < 0) {
    diff *= -1;
  }
  const hh = Math.floor(diff / 1000 / 60 / 60);
  diff -= hh * 1000 * 60 * 60;
  const mm = Math.floor(diff / 1000 / 60);
  let time = `${hh} hrs`;
  if (mm) {
    time = ` ${hh} hrs ${mm} mins`;
  }
  return time;
};

export const getStoreHoursClosingTiming = (
  hours = {
    regularHours: [],
    holidayHours: [],
    regularAndHolidayHours: [],
  },
  labels = {},
  currentDate = {}
) => {
  const { regularHours, holidayHours, regularAndHolidayHours } = hours;
  const intervals = [...regularHours, ...holidayHours, ...regularAndHolidayHours];
  const selectedInterval = getCurrentStoreHours(intervals, currentDate);
  let storeLabel = '';
  const storeTimeLabel = '';
  try {
    if (selectedInterval && selectedInterval[0] && selectedInterval[0].isClosed) {
      storeLabel = getLabelValue(labels, 'lbl_storeldetails_closed');
      return {
        storeLabel,
        storeTimeLabel,
      };
    }

    const selectedDateToHour = parseDate(selectedInterval[0].openIntervals[0].toHour);

    if (!isPastStoreHours(selectedDateToHour, currentDate)) {
      const closingInLabel = getLabelValue(labels, 'lbl_storeldetails_closing');
      return {
        storeLabel: closingInLabel,
        storeTimeLabel: getTimeInHourAndMinute(selectedDateToHour, currentDate),
      };
    }

    const openAtLable = getLabelValue(labels, 'lbl_storelanding_opensAt');
    const selectedDateFromHour = parseDate(selectedInterval[0].openIntervals[0].fromHour);
    return {
      storeLabel: openAtLable,
      storeTimeLabel: toTimeString(selectedDateFromHour, true),
    };
  } catch (err) {
    return {
      storeLabel: '',
      storeTimeLabel: '',
    };
  }
};

export const getStoreHours = (
  hours = {
    regularHours: [],
    holidayHours: [],
    regularAndHolidayHours: [],
  },
  labels = {},
  currentDate = {},
  returnType
) => {
  const { regularHours, holidayHours, regularAndHolidayHours } = hours;

  const intervals = [...regularHours, ...holidayHours, ...regularAndHolidayHours];
  const selectedInterval = getCurrentStoreHours(intervals, currentDate);
  const isReturnTypeObject = returnType === 'object';
  try {
    if (selectedInterval && selectedInterval[0] && selectedInterval[0].isClosed) {
      return getLabelValue(labels, 'lbl_storeldetails_closed');
    }
    const openUntilLabel = getLabelValue(labels, 'lbl_storelanding_openInterval');
    const opensAtLabel = getLabelValue(labels, 'lbl_storelanding_opensAt');
    const selectedDateToHour = parseDate(selectedInterval[0].openIntervals[0].toHour);
    if (!isPastStoreHours(selectedDateToHour, currentDate)) {
      if (isReturnTypeObject) {
        return {
          lblText: openUntilLabel,
          time: toTimeString(selectedDateToHour, true),
        };
      }
      return `${openUntilLabel} ${toTimeString(selectedDateToHour, true)}`;
    }
    const selectedDateFromHour = parseDate(selectedInterval[0].openIntervals[0].fromHour);
    // Handle the other scenarion
    if (isReturnTypeObject) {
      return {
        lblText: openUntilLabel,
        time: toTimeString(selectedDateToHour, true),
      };
    }
    return `${opensAtLabel} ${toTimeString(selectedDateFromHour, true)}`;
  } catch (err) {
    if (isReturnTypeObject) {
      return {
        lblText: '',
        time: '',
      };
    }
    // Show empty incase no data found.
    return '';
  }
};
/**
 * @summary this is meant to generate a new UID on each API call
 * @param {string} apiConfig - Api config to be utilized for brand/channel/locale config
 * @returns {string} returns generated traceId of User or else not-found string value

 */
export const generateTraceId = () => {
  const apiConfigObj = getAPIConfig();
  let prefix;

  // Setting prefix of trace-id based on platform of user i.e. either mobile, browser, Node
  if (isMobileApp()) {
    prefix = 'MOBILE';
  } else if (isClient()) {
    prefix = 'CLIENT';
  } else {
    prefix = 'NODE';
  }
  const timeStamp = `${Date.now()}`;

  // On the Node Server traceIdCount can grow to Infinity, so we will reset it at 10000
  if (apiConfigObj.traceIdCount > 10000) {
    apiConfigObj.traceIdCount = 0;
  }

  const traceIdCount = apiConfigObj.traceIdCount + 1;
  const traceId = `${prefix}_${traceIdCount}_${timeStamp}`;
  return traceId || 'not-found';
};

/**
 * @summary this is meant to extract the x-amzn-trace-id available in request header
 * @returns {string} returns x-amzn-trace-id
 */
export const getAmznTraceId = () => {
  if (isServer()) {
    const amznTraceIdInRequest = httpContext.get('TraceId');
    if (amznTraceIdInRequest) {
      const amznTraceIdTokens = amznTraceIdInRequest.split('Root=');
      if (amznTraceIdTokens && amznTraceIdTokens.length > 1) {
        return amznTraceIdTokens[1];
      }
    }
  }
  return '';
};

export const getBopisOrderMessageAndLabel = (status, ordersLabels, isBopisOrder) => {
  let label;
  let message;

  switch (status) {
    case constants.STATUS_CONSTANTS.ORDER_IN_PROCESS:
    case constants.STATUS_CONSTANTS.ORDER_RECEIVED:
    case constants.STATUS_CONSTANTS.ORDER_USER_CALL_NEEDED:
      label = isBopisOrder
        ? getLabelValue(ordersLabels, 'lbl_orders_orderInProcess')
        : getLabelValue(ordersLabels, 'lbl_orders_OrderReceived');
      message = isBopisOrder
        ? getLabelValue(ordersLabels, 'lbl_orders_orderIsReadyForPickup')
        : getLabelValue(ordersLabels, 'lbl_orders_processing');
      break;
    default:
      label = null;
      message = null;
      break;
  }
  return {
    label,
    message,
  };
};

/**
 * Function to get formated Date for Orders in MMMM dd,yyyy format with locale
 * @param {object} orderDate order date
 * @returns {object} return formated date
 */
export const getFormatedOrderDate = (orderDate, shortMonth = false) => {
  const { language } = getAPIConfig();
  const parsedISODate = parseISO(orderDate);
  if (isMobileApp()) {
    return isValid(parsedISODate) ? format(parsedISODate, 'MMMM dO, yyyy') : '';
  }
  const { month, date, year } = getTranslateDateInformation(
    orderDate,
    language,
    {
      weekday: 'short',
    },
    {
      month: shortMonth ? 'short' : 'long',
    }
  );

  return `${month} ${date}, ${year}`;
};

const getOrderGroupLabelExtended = (
  status,
  ordersLabels,
  pickUpExpirationDate,
  pickedUpDate,
  isBopisOrder
) => {
  let label;
  let message;
  switch (status) {
    case constants.STATUS_CONSTANTS.ITEMS_READY_FOR_PICKUP:
      label = getLabelValue(ordersLabels, 'lbl_orders_pleasePickupBy');
      message = getFormatedOrderDate(pickUpExpirationDate);
      break;
    case constants.STATUS_CONSTANTS.ORDER_PICKED_UP:
    case constants.STATUS_CONSTANTS.ITEMS_PICKED_UP:
      label = getLabelValue(ordersLabels, 'lbl_orders_pickedUpOn');
      message = getFormatedOrderDate(pickedUpDate);
      break;
    default:
      ({ label, message } = getBopisOrderMessageAndLabel(status, ordersLabels, isBopisOrder));
      break;
  }
  return {
    label,
    message,
  };
};

/**
 * Function to get formated time for Orders in hh:mmaa format with locale
 * @param {object} orderDate order date
 * @returns {object} return formated time
 */
export const getFormatedOrderTime = (dateToFormat) => {
  const { language } = getAPIConfig();
  const localeType = language ? getModifiedLanguageCode(language).replace('_', '-') : 'en';
  const currentDate = dateToFormat ? new Date(dateToFormat) : new Date();
  const options = {
    hour: 'numeric',
    minute: 'numeric',
    hour12: true,
  };
  const timeString =
    Intl !== undefined ? Intl.DateTimeFormat(localeType, options).format(currentDate) : '';
  return timeString.replace(/\s+/g, '').toLowerCase();
};

/**
 * Function to get Order Detail Group Header label and Message
 * @param {object} orderProps orderProps contain status, shippedDate, pickedDate, ordersLabels

 * @returns {object} label and message for order group
 */
export const getOrderGroupLabelAndMessage = (orderProps) => {
  let label;
  let message;
  const { status, shippedDate, pickedUpDate, ordersLabels, isBopisOrder, pickUpExpirationDate } =
    orderProps;

  switch (status) {
    case constants.STATUS_CONSTANTS.ORDER_SHIPPED:
    case constants.STATUS_CONSTANTS.ORDER_PARTIALLY_SHIPPED:
      label = getLabelValue(ordersLabels, 'lbl_orders_shippedOn');
      message =
        shippedDate === constants.STATUS_CONSTANTS.NA
          ? shippedDate
          : getFormatedOrderDate(shippedDate);
      break;
    case constants.STATUS_CONSTANTS.ORDER_CANCELED:
    case constants.STATUS_CONSTANTS.ORDER_CANCELLED:
    case constants.STATUS_CONSTANTS.ORDER_EXPIRED:
      label = '';
      message = getLabelValue(ordersLabels, 'lbl_orders_orderCancelMessage');
      break;
    case constants.STATUS_CONSTANTS.ITEMS_RECEIVED:
      label = getLabelValue(ordersLabels, 'lbl_orders_orderInProcess');
      message = getLabelValue(ordersLabels, 'lbl_orders_orderIsReadyForPickup');
      break;
    default:
      ({ label, message } = getOrderGroupLabelExtended(
        status,
        ordersLabels,
        pickUpExpirationDate,
        pickedUpDate,
        isBopisOrder
      ));
      break;
  }

  return {
    label,
    message,
  };
};

export const isAbsoluteUrl = (url) => {
  const pattern = /^https?:\/\//i;
  return pattern.test(url);
};

export const getStringAfterSplit = (stringToSplit, splitByCharacter) => {
  return stringToSplit ? stringToSplit.split(splitByCharacter) : null;
};

export const getAssetHostURL = () => {
  const brandName = getBrand();
  const brandId = brandName && brandName.toUpperCase();
  const apiConfigObj = getAPIConfig();
  return apiConfigObj[`assetHost${brandId}`];
};

export const changeImageURLToDOM = (imgPath, cropParams) => {
  const brandName = getBrand();
  const brandId = brandName && brandName.toUpperCase();
  const apiConfigObj = getAPIConfig();
  const assetHost = apiConfigObj[`assetHost${brandId}`];
  const productAssetPath = apiConfigObj[`productAssetPath${brandId}`];
  return `${assetHost}/${cropParams}/${productAssetPath}/${imgPath}`;
};

/**
 * The insertIntoString() method changes the content of a string by removing a range of
 * characters and/or adding new characters.
 * @param {String} string base string to work on
 * @param {number} start Index at which to start changing the string.
 * @param {number} delCount An integer indicating the number of old chars to remove.
 * @param {string} newSubStr The String that is spliced in.
 * @return {string} A new string with the spliced substring.
 */
export const insertIntoString = (string, idx, rem, str) => {
  return string.slice(0, idx) + str + string.slice(idx + Math.abs(rem));
};

export const getStyliticsUserName = () => {
  const { styliticsUserNameTCP, styliticsUserNameGYM } = getAPIConfig();
  if (isTCP()) {
    return styliticsUserNameTCP;
  }
  return styliticsUserNameGYM;
};

export const getStyliticsRegion = () => {
  const { styliticsRegionTCP, styliticsRegionGYM } = getAPIConfig();
  if (isTCP()) {
    return styliticsRegionTCP;
  }
  return styliticsRegionGYM;
};

export const isValidObj = (obj) => obj && Object.keys(obj).length > 0;

export const canUseDOM = () => {
  return typeof window !== 'undefined' && window.document && window.document.createElement;
};
export const regularProducts = (availableFlag, multipackProduct) => {
  return availableFlag === -1 && multipackProduct === null;
};

export const checkOOS = (multipackProduct) => {
  if (multipackProduct && multipackProduct.length) {
    return multipackProduct.map((finalData) => {
      return finalData.v_qty === 0 || !finalData ? 'unavailable' : 'available';
    });
  }
  return null;
};

export const getProductUrlForDAM = (uniqueId) => {
  return `${uniqueId.split('_')[0]}/${uniqueId}`;
};

/**
 *
 * Get labels based on pattern
 * @param {Object} object of labels
 * @param {String} string pattern
 * @return {Array} return string array for labels
 */
export const getLabelsBasedOnPattern = (labels, pattern) => {
  const regex = new RegExp(pattern);
  return Object.keys(labels).filter((labelKey) => regex.test(labelKey));
};

/**
 * @description - This method calculate Price based on the given value
 */
export const calculatePriceValue = (
  price,
  currencySymbol = '$',
  currencyExchangeValue = 1,
  defaultReturn = 0
) => {
  let priceValue = defaultReturn;
  if (price && price > 0) {
    priceValue = `${currencySymbol}${(price * currencyExchangeValue).toFixed(2)}`;
  }
  return priceValue;
};
export const orderStatusMapperForNotification = {
  [constants.STATUS_CONSTANTS.ORDER_RECEIVED]: 'lbl_orders_statusOrderReceived',
  [constants.STATUS_CONSTANTS.ORDER_PROCESSING]: 'lbl_global_yourOrderIsProcessing',
  [constants.STATUS_CONSTANTS.ORDER_SHIPPED]: 'lbl_orders_statusOrderShipped',
  [constants.STATUS_CONSTANTS.ORDER_PARTIALLY_SHIPPED]: 'lbl_orders_statusOrderPartiallyShipped',
  [constants.STATUS_CONSTANTS.ORDER_CANCELED]: 'lbl_orders_statusOrderCancelled',
  [constants.STATUS_CONSTANTS.ITEMS_RECEIVED]: 'lbl_orders_statusOrderReceived',
  [constants.STATUS_CONSTANTS.ITEMS_READY_FOR_PICKUP]: 'lbl_orders_statusItemsReadyForPickup',
  [constants.STATUS_CONSTANTS.ITEMS_PICKED_UP]: 'lbl_orders_statusItemsPickedUp',
  [constants.STATUS_CONSTANTS.ORDER_EXPIRED]: 'lbl_orders_statusOrderExpired',
  [constants.STATUS_CONSTANTS.ORDER_USER_CALL_NEEDED]: 'lbl_orders_statusOrderReceived',
  [constants.STATUS_CONSTANTS.ORDER_PROCESSING_AT_FACILITY]: 'lbl_global_yourOrderIsBeingProcessed',
  [constants.STATUS_CONSTANTS.LBL_NA]: constants.STATUS_CONSTANTS.NA,
  /* Status added for BOSS */
  [constants.STATUS_CONSTANTS.EXPIRED_AND_REFUNDED]: 'lbl_global_yourOrderHasBeenExpiredRefunded',
  [constants.STATUS_CONSTANTS.ORDER_CANCELLED]: 'lbl_orders_statusOrderCancelled',
  [constants.STATUS_CONSTANTS.LBL_CallNeeded]: 'lbl_orders_statusOrderReceived',
  [constants.STATUS_CONSTANTS.SUCCESSFULLY_PICKED_UP]: 'lbl_orders_statusItemsPickedUp',
  [constants.STATUS_CONSTANTS.ORDER_IN_PROCESS]: 'lbl_orders_statusOrderReceived',
};

/**
 * @function getOrderStatusForNotification
 * @summary
 * @param {String} status -
 * @return orderStatus
 */
export const getOrderStatusForNotification = (status = '') => {
  const orderStatus =
    orderStatusMapperForNotification[status] ||
    orderStatusMapperForNotification[status.toLowerCase()] ||
    status;

  return orderStatus !== constants.STATUS_CONSTANTS.NA ? orderStatus : '';
};

/**
 * @function validateDiffInDaysNotification
 * @summary
 * @param {Date}  orderDateParam
 * @return true if date false between limit range
 */
export const validateDiffInDaysNotification = (
  orderDateParam,
  limitOfDaysToDisplayNotification
) => {
  const orderDate = orderDateParam ? new Date(orderDateParam) : new Date();
  if (differenceInDays(new Date(), orderDate) <= limitOfDaysToDisplayNotification) {
    return true;
  }
  return false;
};

/**
 * To convert from string to number.
 * @param {*} val
 */
export const convertNumToBool = (val) => {
  return !!parseInt(val, 10);
};

/**
 * To Trigger the webview post message when any anchor is clicked.
 * this will work for internal routes only.
 */
export const triggerPostMessage = (url) => {
  const { isAppChannel } = getAPIConfig();
  if (isClient() && url && window && window.ReactNativeWebView && isAppChannel) {
    let updatedUrl = url;
    const { siteId } = getAPIConfig();
    if (updatedUrl.includes(`/${siteId}`)) {
      updatedUrl = updatedUrl.replace(`/${siteId}`, '');
    }
    const postMessageFn = window.ReactNativeWebView;
    postMessageFn.postMessage(updatedUrl);
  }
};
/*
 * @function cropVideoUrl function appends or replaces the cropping value in the URL
 * @param {string} url the image url
 * @return {string} function returns new Url with the crop value
 */
export const cropVideoUrl = (url) => {
  // Video path transformation in case of absolute image URL
  if (/^http/.test(url)) {
    const [urlPath = '', urlData = ''] = url && url.split('/upload');
    const imgPath = urlPath && urlPath.replace(/^\//, '');
    // cropVideoUrl is called from different places hence we need to check
    if (!url.includes('##format##')) {
      // Updating the path with placeholder ##format## to update the format from VideoPlayer
      return `${imgPath}/upload/##format##/${urlData.replace(/^\//, '')}`;
    }
    return `${imgPath}/upload/${urlData.replace(/^\//, '')}`;
  }
  const brandName = getBrand();
  if (brandName) {
    const apiConfigObj = getAPIConfig();
    let assetHost = apiConfigObj[`assetHost${brandName.toUpperCase()}`];
    // cropVideoUrl is called from different places hence we need to check
    if (!url.includes('##format##')) {
      // Updating the path with placeholder ##format## to update the format from VideoPlayer
      assetHost = `${assetHost}/##format##`;
    }
    const basePath = assetHost ? assetHost.replace('/image/', '/video/') : '';
    return `${basePath}/${url}`;
  }
  return url;
};
/*
 * This function returns the true if the browser is IE11
 */
export const isIE11 = () => {
  // true on IE11
  // false on Edge and other IEs/browsers.
  return !!window.MSInputMethodContext && !!document.documentMode;
};

export const isIEBrowser = () => {
  if (isClient()) {
    const ua = window.navigator && window.navigator.userAgent;
    const msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older, return version number
      return true;
    }
    const trident = ua.indexOf('Trident/');
    if (trident > 0) {
      return true;
    }
    const edge = ua.indexOf('Edge/');
    if (edge > 0) {
      return true;
    }
  }
  return false;
};

/**
 * @function convertToISOString Converts date string to ISO string
 * @param {date} date passed date string
 * @return replaces the space with T to handle the IOS and Safari
 */
export const convertToISOString = (date) => (date ? date.replace(/\s+/g, 'T') : date);

const pricingState = (product) => {
  return product && product.offerPrice < product.listPrice ? 'on sale' : 'full price';
};

export const getColorMap = (productItemData) => {
  return (
    productItemData &&
    productItemData.colorFitsSizesMap &&
    productItemData.colorFitsSizesMap.map((productTile) => {
      return productTile.color.name || '';
    })
  );
};

const getParameter = (navigation, param) => {
  return (navigation && navigation.getParam(param)) || '';
};

const getExtendedObject = (customData) => {
  let customObj = {};
  if (customData) {
    customObj = {
      ...(customData.itemBrand && {
        prodBrand: customData.itemBrand,
      }),
      ...(customData.subTotal && {
        cartPrice: customData.subTotal,
      }),
      ...(customData.cartAddType && {
        addType: customData.cartAddType,
      }),
      ...(customData.sku && {
        sku: customData.sku,
      }),
    };
  }
  return customObj;
};

const getProductId = (productItemData) => {
  return (
    (productItemData &&
      productItemData.generalProductId &&
      productItemData.generalProductId.split('_')[0]) ||
    null
  );
};

const getAddTypeData = (productMiscItem, customData) => {
  let type = null;
  type = (productMiscItem && productMiscItem.categoryName) || (customData && customData.addType);
  return type;
};

const getItemPosition = (customData, index) => {
  let pos = index;
  if (customData && customData.index) {
    pos = customData.index;
  }
  return pos + 1;
};

const getSelectedColorData = (colorFitsSizesMap, colorId) => {
  return (
    colorFitsSizesMap &&
    colorFitsSizesMap.filter((colorItem) => {
      return colorItem.colorDisplayId === colorId;
    })
  );
};

const getFeatures = (productItemData, selectedColorProductId) => {
  const { isGiftCard, colorFitsSizesMap } = productItemData || {};
  const selectedColorElement = getSelectedColorData(colorFitsSizesMap, selectedColorProductId);
  const hasFits =
    selectedColorElement && selectedColorElement[0] && selectedColorElement[0].hasFits;

  let features = '';

  if (!isGiftCard) {
    features = 'alt images full size image';
  }
  if (hasFits) {
    features = `${features} size chart`;
  }
  return features;
};

const getFromPage = (fromPage) => {
  return fromPage ? 'CTL' : 'completeTheLook';
};

const getRecsType = (viaModule, fromPage) => {
  return (
    viaModule && (viaModule === 'Stylistics_Recommendation' ? getFromPage(fromPage) : viaModule)
  );
};

const getFormattedProductsItem = (
  productObj,
  customData,
  navigation,
  selectedColorProductId,
  fromPage
) => {
  const clickOrigin = getParameter(navigation, 'clickOrigin');
  const outfitId = getParameter(navigation, 'outfitId');
  const viaModule = getParameter(navigation, 'viaModule');
  const { productItemData, index, productMiscItem } = productObj;
  let productItem = null;
  const colorName = getColorMap(productItemData);
  const productId = getProductId(productItemData);
  let pageOutfit = 'outfit';
  if (fromPage && viaModule === 'Stylistics_Recommendation') pageOutfit = fromPage;
  const recObj = {
    recsProductId: productItemData.generalProductId,
    recsPageType: outfitId ? pageOutfit : clickOrigin,
    recsType: getRecsType(viaModule, fromPage),
  };
  const size =
    (customData && customData.selectedSize && customData.selectedSize.name) ||
    (customData && customData.selectedSizeValue);

  productItem =
    (productItemData &&
      productItemData.generalProductId && {
        colorId: productItemData.generalProductId,
        color: colorName,
        id: productId,
        outfitId,
        price: productItemData.listPrice,
        listPrice: productItemData.listPrice,
        extPrice: productItemData.offerPrice,
        rating: productItemData.ratings,
        reviews: productItemData.reviewsCount,
        plpClick: clickOrigin === 'browse' || false,
        searchClick: clickOrigin === 'search' || false,
        name: productItemData.name,
        paidPrice: productItemData.offerPrice,
        quantity: customData && customData.selectedQuantity,
        size,
        position: getItemPosition(customData, index),
        type: getAddTypeData(productMiscItem, customData),
        pricingState: pricingState(productItemData),
        features: getFeatures(productItemData, selectedColorProductId),
        ...getExtendedObject(customData),
        ...recObj,
      }) ||
    {};
  return productItem;
};

const formatProductsItem = (
  productObj,
  customData,
  navigation,
  isProductArray,
  selectedColorProductId,
  isSBP,
  fromPage
) => {
  const productData = [];
  let productItem = getFormattedProductsItem(
    productObj,
    customData,
    navigation,
    selectedColorProductId,
    fromPage
  );
  if (isSBP) productItem = { ...productItem, sbp: true };
  productData.push(productItem);
  return isProductArray ? productItem : productData;
};

export const formatProductsData = (
  products,
  customData,
  navigation,
  selectedColorProductId,
  fromPage
) => {
  const isSBP = (navigation && navigation.getParam('isSBP')) || '';
  if (Array.isArray(products)) {
    let productItemData = {};
    let productMiscItem;
    return products.map((product, index) => {
      // Needed this check as structure of this object is differnt for search
      productItemData = product.productInfo ? product.productInfo : product;
      productMiscItem = product.miscInfo ? product.miscInfo : {};
      if (product.productInfo && product.colorsMap) {
        productItemData.colorFitsSizesMap = product.colorsMap;
      }
      return formatProductsItem(
        {
          productItemData,
          index,
          productMiscItem,
        },
        customData,
        navigation,
        true,
        selectedColorProductId,
        isSBP,
        fromPage
      );
    });
  }
  return formatProductsItem(
    {
      productItemData: products,
    },
    customData,
    navigation,
    false,
    selectedColorProductId,
    isSBP,
    fromPage
  );
};

export const getPageName = (productObj) => {
  const productId = productObj.generalProductId && productObj.generalProductId.split('_')[0];
  return productId ? `product:${productId}:${productObj.name}` : '';
};

/**
 * @method getQMSessionIdFromDefaultPreference
 * @desc Returning blank for web as its no value required only puting function here as using this from comman file,mainly using this function in Utils.app for App.
 */
export const getQMSessionIdFromDefaultPreference = () => {
  return '';
};

/**
 * To find that url has to open in web view or not.
 * @param {*} url
 */

export const isWebViewPage = (url) => {
  let isWebView = false;
  const urlList = ['help-center', 'content', 'borderfree', 'gift-cards'];
  for (let i = 0; i < urlList.length; i += 1) {
    if (url.includes(urlList[i])) {
      isWebView = true;
      break;
    }
  }
  return isWebView;
};

export const disableTcpApp = () => {
  const { disableTcpBrand } = getAPIConfig();
  return disableTcpBrand && isMobileApp() && isGymboree();
};

export const disableTcpAppProduct = (productBrand = '') => {
  return disableTcpApp() && productBrand && productBrand.toLowerCase() === API_CONFIG.brandIds.tcp;
};

/**
 * This function will check whether the url has hyphen and slash or not.
 * @param {*} path
 */

export const pathHasHyphenSlash = (path) => {
  const pathName = path;
  if (!pathName) {
    return false;
  }
  return pathName.match(/\W|_/g);
};

/**
 * This function will replace the slash and hyphen
 * slashes will be replaced with empty char
 * hyphen
 * @param {*} path
 */
export const createLayoutPath = (path) => {
  if (!path) {
    return '';
  }
  const pathName = path ? path.replace('/', '') : '';
  return (
    pathName &&
    pathName.replace(/-([a-z])/g, (g) => {
      return g[1].toUpperCase();
    })
  );
};

/**
 * @description - Get fonts URL for web and MobileApp WebView
 * @param {string} filePath - fonts name and path
 */
export const getFontsURL = (filePath) => {
  // Added fallback in case fonts url is not updated
  const fontsHostUrl = 'https://assets.theplace.com/rwd/';
  return `${fontsHostUrl}${filePath}`;
};

/**
 * @description - This function will check if user info call is required, will be called on every route change for app and web
 * @param { Array } routeList - list of excluded routes
 * @param { string } currentRoute - current url or screenName
 */
export const checkIfUserInfoCallRequired = (routeList, currentRoute) => {
  const isExcluded = routeList.some((route) => {
    const regex = new RegExp(route);
    return regex.test(currentRoute || '');
  });

  return !isExcluded;
};

export const getBootstrapCachedData = () => null;

export const setBootstrapCachedData = () => null;
export const getValueFromAsyncStorage = () => null;
export const setValueFromAsyncStorage = () => null;
export const setValueInAsyncStorage = () => null;
export const removeValueFromAsyncStorage = () => null;
export const removeMultipleFromAsyncStorage = () => null;
export const clearLoginCookiesFromAsyncStore = () => null;
export const checkForDuplicateAuthCookies = () => null;

export const removeExtension = (path) =>
  path ? path.replace(/\.(mp4|webm|webp|mov|WEBM|MP4|MOV)$/, '') : '';

export const removeFileExtension = (filePath) => {
  const newPath = filePath.replace('##format##,', '');
  return newPath ? newPath.split('.').slice(0, -1).join('.') : '';
};

const getFontSize = (fontSize) => {
  const numFontSize = Number(fontSize);
  return Number.isNaN(numFontSize) ? 0 : numFontSize;
};

const getStyleOverride = (referenceClassNames, classOverrides, constantsMod) => {
  const overrideConfig = {};
  const colorPallete = createThemeColorPalette();
  const setFontSize = (attribute, font) => {
    const fontSize = isMobileApp() ? getFontSize(font) : `${font}px`;
    if (overrideConfig[attribute]) {
      overrideConfig[attribute].fontSize = fontSize;
    } else {
      overrideConfig[attribute] = {
        fontSize,
      };
    }
  };

  // TODO: Refactor the code to remove complexity
  // eslint-disable-next-line sonarjs/cognitive-complexity
  classOverrides.forEach((className) => {
    // eslint-disable-next-line complexity
    referenceClassNames.forEach((referenceClass) => {
      if (className.includes(referenceClass)) {
        const realClass = className.split(`${referenceClass}-`)[1];
        const styleAttributes = realClass && realClass.split('-');
        const colorName = styleAttributes[0];
        const colorCode = styleAttributes[1];

        const overrideProperty = colorCode
          ? colorPallete[colorName] && colorPallete[colorName][colorCode]
          : colorPallete[colorName] || colorName;

        switch (referenceClass) {
          case constantsMod.MOD_RIBBON:
            overrideConfig[referenceClass] = {
              tintColor: overrideProperty,
            };
            break;
          case constantsMod.MOD_BACKGROUND:
          case constantsMod.MOD_NAV_BACKGROUND:
          case constantsMod.MOD_TOP_DIVIDER:
          case constantsMod.MOD_BOTTOM_DIVIDER:
          case constantsMod.MOD_OVERLAY_BACKGROUND_COLOR:
          case constantsMod.MOD_OVERLAY_RULER_COLOR:
          case constantsMod.MOD_HEADER_TEXT_BACKGROUND:
            overrideConfig[referenceClass] = {
              backgroundColor: overrideProperty,
            };
            break;
          case constantsMod.MOD_HEADER_FS:
            setFontSize('title', overrideProperty);
            break;
          case constantsMod.MOD_SUB_HEADER_FS:
            setFontSize('subTitle', overrideProperty);
            break;
          case constantsMod.MOD_PROMO_FS:
            setFontSize('promo', overrideProperty);
            break;
          case constantsMod.MOD_PROMO_SUB_HEADER_FS:
            setFontSize('subPromoTitle', overrideProperty);
            break;
          case constantsMod.MOD_PROMO_SUB_HEADER_FW: {
            const attribute = 'subPromoTitle';
            if (overrideConfig[attribute]) {
              overrideConfig[attribute].fontWeight = overrideProperty;
            } else {
              overrideConfig[attribute] = {
                fontWeight: overrideProperty,
              };
            }
            break;
          }
          case constantsMod.MOD_MARGIN_BOTTOM:
            overrideConfig[referenceClass] = {
              'margin-bottom': overrideProperty,
            };
            break;
          case constantsMod.MOD_MARGIN_TOP:
            overrideConfig[referenceClass] = {
              'margin-top': overrideProperty,
            };
            break;
          case constantsMod.MOD_TEXT_WIDTH:
            overrideConfig[referenceClass] = {
              width: `${overrideProperty || 100}%`,
            };
            break;
          case constantsMod.MOD_IMAGE_HEIGHT:
          case constantsMod.MOD_IMG_CMP_HEIGHT:
          case constantsMod.MOD_IMG_COL1_HEIGHT:
          case constantsMod.MOD_IMG_COL2_HEIGHT:
          case constantsMod.MOD_IMG_COL3_HEIGHT:
          case constantsMod.MOD_IMG_COL4_HEIGHT:
            overrideConfig[referenceClass] = {
              height: overrideProperty,
            };
            break;
          case constantsMod.MOD_SLIDER_FADE:
            overrideConfig[referenceClass] = {
              speed: overrideProperty,
            };
            break;
          case constantsMod.MOD_HEADER_TOP:
          case constantsMod.MOD_HEADER1_COL1_TOP:
          case constantsMod.MOD_HEADER1_COL2_TOP:
          case constantsMod.MOD_HEADER1_COL3_TOP:
          case constantsMod.MOD_HEADER1_COL4_TOP:
          case constantsMod.MOD_HEADER2_COL1_TOP:
          case constantsMod.MOD_HEADER2_COL2_TOP:
          case constantsMod.MOD_HEADER2_COL3_TOP:
          case constantsMod.MOD_HEADER2_COL4_TOP:
            overrideConfig[referenceClass] = {
              'header-top': overrideProperty,
            };
            break;

          case constantsMod.MOD_HEADER1_COL1_TOP_LEFT:
          case constantsMod.MOD_HEADER1_COL2_TOP_LEFT:
          case constantsMod.MOD_HEADER1_COL3_TOP_LEFT:
          case constantsMod.MOD_HEADER1_COL4_TOP_LEFT:
          case constantsMod.MOD_HEADER2_COL1_TOP_LEFT:
          case constantsMod.MOD_HEADER2_COL2_TOP_LEFT:
          case constantsMod.MOD_HEADER2_COL3_TOP_LEFT:
          case constantsMod.MOD_HEADER2_COL4_TOP_LEFT:
            overrideConfig[referenceClass] = {
              'header-top-left': overrideProperty,
            };
            break;
          case constantsMod.MOD_HEADER1_COL1_TOP_RIGHT:
          case constantsMod.MOD_HEADER1_COL2_TOP_RIGHT:
          case constantsMod.MOD_HEADER1_COL3_TOP_RIGHT:
          case constantsMod.MOD_HEADER1_COL4_TOP_RIGHT:
          case constantsMod.MOD_HEADER2_COL1_TOP_RIGHT:
          case constantsMod.MOD_HEADER2_COL2_TOP_RIGHT:
          case constantsMod.MOD_HEADER2_COL3_TOP_RIGHT:
          case constantsMod.MOD_HEADER2_COL4_TOP_RIGHT:
            overrideConfig[referenceClass] = {
              'header-top-right': overrideProperty,
            };
            break;
          case constantsMod.MOD_HEADER1_COL1_BOTTOM_LEFT:
          case constantsMod.MOD_HEADER1_COL2_BOTTOM_LEFT:
          case constantsMod.MOD_HEADER1_COL3_BOTTOM_LEFT:
          case constantsMod.MOD_HEADER1_COL4_BOTTOM_LEFT:
          case constantsMod.MOD_HEADER2_COL1_BOTTOM_LEFT:
          case constantsMod.MOD_HEADER2_COL2_BOTTOM_LEFT:
          case constantsMod.MOD_HEADER2_COL3_BOTTOM_LEFT:
          case constantsMod.MOD_HEADER2_COL4_BOTTOM_LEFT:
            overrideConfig[referenceClass] = {
              'header-bottom-left': overrideProperty,
            };
            break;
          case constantsMod.MOD_HEADER1_COL1_BOTTOM_RIGHT:
          case constantsMod.MOD_HEADER1_COL2_BOTTOM_RIGHT:
          case constantsMod.MOD_HEADER1_COL3_BOTTOM_RIGHT:
          case constantsMod.MOD_HEADER1_COL4_BOTTOM_RIGHT:
          case constantsMod.MOD_HEADER2_COL1_BOTTOM_RIGHT:
          case constantsMod.MOD_HEADER2_COL2_BOTTOM_RIGHT:
          case constantsMod.MOD_HEADER2_COL3_BOTTOM_RIGHT:
          case constantsMod.MOD_HEADER2_COL4_BOTTOM_RIGHT:
            overrideConfig[referenceClass] = {
              'header-bottom-right': overrideProperty,
            };
            break;

          case constantsMod.MOD_HEADER_BOTTOM:
          case constantsMod.MOD_HEADER1_COL1_BOTTOM:
          case constantsMod.MOD_HEADER1_COL2_BOTTOM:
          case constantsMod.MOD_HEADER1_COL3_BOTTOM:
          case constantsMod.MOD_HEADER1_COL4_BOTTOM:
          case constantsMod.MOD_HEADER2_COL1_BOTTOM:
          case constantsMod.MOD_HEADER2_COL2_BOTTOM:
          case constantsMod.MOD_HEADER2_COL3_BOTTOM:
          case constantsMod.MOD_HEADER2_COL4_BOTTOM:
            overrideConfig[referenceClass] = {
              'header-bottom': overrideProperty,
            };
            break;

          case constantsMod.MOD_MEDIA_TOP:
          case constantsMod.MOD_MEDIA_COL1_TOP:
          case constantsMod.MOD_MEDIA_COL2_TOP:
          case constantsMod.MOD_MEDIA_COL3_TOP:
          case constantsMod.MOD_MEDIA_COL4_TOP:
            overrideConfig[referenceClass] = {
              'media-top': overrideProperty,
            };
            break;

          case constantsMod.MOD_MEDIA_COL1_TOP_LEFT:
          case constantsMod.MOD_MEDIA_COL2_TOP_LEFT:
          case constantsMod.MOD_MEDIA_COL3_TOP_LEFT:
          case constantsMod.MOD_MEDIA_COL4_TOP_LEFT:
            overrideConfig[referenceClass] = {
              'media-top-left': overrideProperty,
            };
            break;
          case constantsMod.MOD_MEDIA_COL1_TOP_RIGHT:
          case constantsMod.MOD_MEDIA_COL2_TOP_RIGHT:
          case constantsMod.MOD_MEDIA_COL3_TOP_RIGHT:
          case constantsMod.MOD_MEDIA_COL4_TOP_RIGHT:
            overrideConfig[referenceClass] = {
              'media-top-right': overrideProperty,
            };
            break;

          case constantsMod.MOD_MEDIA_BOTTOM:
          case constantsMod.MOD_MEDIA_COL1_BOTTOM:
          case constantsMod.MOD_MEDIA_COL2_BOTTOM:
          case constantsMod.MOD_MEDIA_COL3_BOTTOM:
          case constantsMod.MOD_MEDIA_COL4_BOTTOM:
            overrideConfig[referenceClass] = {
              'media-bottom': overrideProperty,
            };
            break;

          case constantsMod.MOD_MEDIA_COL1_BOTTOM_LEFT:
          case constantsMod.MOD_MEDIA_COL2_BOTTOM_LEFT:
          case constantsMod.MOD_MEDIA_COL3_BOTTOM_LEFT:
          case constantsMod.MOD_MEDIA_COL4_BOTTOM_LEFT:
            overrideConfig[referenceClass] = {
              'media-bottom-left': overrideProperty,
            };
            break;
          case constantsMod.MOD_MEDIA_COL1_BOTTOM_RIGHT:
          case constantsMod.MOD_MEDIA_COL2_BOTTOM_RIGHT:
          case constantsMod.MOD_MEDIA_COL3_BOTTOM_RIGHT:
          case constantsMod.MOD_MEDIA_COL4_BOTTOM_RIGHT:
            overrideConfig[referenceClass] = {
              'media-bottom-right': overrideProperty,
            };
            break;

          case constantsMod.MOD_CTA_TOP:
          case constantsMod.MOD_CTA_COL1_TOP:
          case constantsMod.MOD_CTA_COL2_TOP:
          case constantsMod.MOD_CTA_COL3_TOP:
          case constantsMod.MOD_CTA_COL4_TOP:
          case constantsMod.MOD_CTA_COL5_TOP:
            overrideConfig[referenceClass] = {
              'cta-top': overrideProperty,
            };
            break;
          case constantsMod.MOD_CTA_BOTTOM:
          case constantsMod.MOD_CTA_COL1_BOTTOM:
          case constantsMod.MOD_CTA_COL2_BOTTOM:
          case constantsMod.MOD_CTA_COL3_BOTTOM:
          case constantsMod.MOD_CTA_COL4_BOTTOM:
          case constantsMod.MOD_CTA_COL5_BOTTOM:
            overrideConfig[referenceClass] = {
              'cta-bottom': overrideProperty,
            };
            break;
          default:
            if (overrideConfig[referenceClass]) {
              overrideConfig[referenceClass].color = overrideProperty;
            } else {
              overrideConfig[referenceClass] = {
                color: overrideProperty,
              };
            }
        }
      }
    });
  });

  return overrideConfig;
};

/**
 * Creates styles which can override on the base of classnames provided from CMS for Mobile App
 * List of Color and Codes is as per the style guide reference
 * Supported Classnames
 *  mod-ribbon-(color)-(code)
 *  mod-bg-(color)-(code)
 *  mod-nav-bg-(color)-(code)
 *  mod-title-(color)-(code) or mod-title-#ffffff
 *  mod-subTitle-(color)-(code) or mod-subTitle-#ffffff
 *  mod-promo-(color)-(code) or mod-promo-#ffffff
 * To setup the color or background color for web or app specific we use the unique word which says type
 *  mod-web-subTitle-(color)-(code) or mod-web-subTitle-#ffffff
 *  mod-app-subTitle-(color)-(code) or mod-app-subTitle-#ffffff
 * To setup the font size for web or app specific we use below format
 *  mod-offer-fs-10
 *  mod-header-fs-10
 *  mod-subHeader-fs-15
 * To setup the top or bottom divider color we do as below
 *  mod-top-div-(color)-(code) or mod-top-div-#ffffff
 *  mod-bottom-div-(color)-(code) or mod-bottom-div-#ffffff
 * @param {*} classNames Space separated classnames eg. (mod-ribbon-orange-500 mod-bg-pink-500 mod-nav-bg-orange-300 mod-title-#efefef)
 */
export const styleOverrideEngine = (classNames, module) => {
  const constantsMod = MODULES[module] || {};
  const referenceClassNames = Object.values(constantsMod);
  if (!classNames)
    return referenceClassNames.reduce((acc, val) => {
      return { ...acc, [val]: {} };
    }, {});
  const classList = classNames.split(' ');
  const extractClassList =
    groupBy(classList, (classes) => {
      if (classes.includes(DEVICE_TYPE.MOBILE)) {
        return DEVICE_TYPE.MOBILE;
      }
      return classes.includes(DEVICE_TYPE.WEB) ? DEVICE_TYPE.WEB : DEVICE_TYPE.GENERIC;
    }) || {};
  const deviceClasses = (isMobileApp() ? extractClassList.app : extractClassList.web) || [];
  const classOverrides = [...(extractClassList.generic || []), ...deviceClasses];

  return getStyleOverride(referenceClassNames, classOverrides, constantsMod);
};

/**
 * This method extract the tracking parameter from the moduleClassName which comes in Global
 * modules data. Currently we don't have any field in CMS to append analytics product image
 * URLs. We will use this until we get proper solution from the CMS which would be input
 * field in CMS divTab component.
 * @param {String} moduleClassName CMS Module Class name of Global Module
 */
export const parseTrackingParamsInModuleClassNames = (moduleClassName = '') => {
  const [trackingString = '', trackingParams = ''] =
    moduleClassName.match(/tracking_params="(.+)"/) || [];

  return {
    trackingString,
    trackingParams,
  };
};

/**
 *
 * @param {String} url url where we need to merge the another query params
 * @param {String} queryParams query params to merge
 */
export const mergeUrlQueryParams = (url, queryParams = '') => {
  if (url.indexOf('?') !== -1) {
    return `${url}${queryParams && '&'}${queryParams.replace(/^(&|\?)/, '')}`;
  }
  return `${url}${queryParams && '?'}${queryParams.replace(/^(&|\?)/, '')}`;
};

/**
 * This function returns the current route name
 */
export const getCurrentRouteName = () => {
  return global.CURRENT_ROUTE_NAME || '';
};

/**
 * @method getDateInformation
 * @desc returns day, month and day of the respective date provided
 * @param {string} date date which is to be mutated
 */

export const getFormattedDate = (date, hideWeekDay) => {
  const currentDate = date && new Date(date);
  if (hideWeekDay) {
    return currentDate && `${MONTHS_SMALL[currentDate.getUTCMonth()]} ${currentDate.getUTCDate()}`;
  }
  return (
    currentDate &&
    `${WEEK_DAYS_SMALL[currentDate.getUTCDay()]}, ${
      MONTHS_SMALL[currentDate.getUTCMonth()]
    } ${currentDate.getUTCDate()}`
  );
};

export const formatDateFromDay = (date) => {
  const currentDate = new Date(date);
  let month = `${currentDate.getMonth() + 1}`;
  let day = `${currentDate.getDate()}`;
  const year = currentDate.getFullYear();

  if (month.length < 2) month = `0${month}`;
  if (day.length < 2) day = `0${day}`;

  return [year, month, day].join('-');
};

const getSelectedShipment = (orderEddDates, selectedShipment) => {
  const hasMinMaxDates = orderEddDates && orderEddDates.minDates && orderEddDates.maxDates;
  let selectedShipmentMethod;
  if (selectedShipment) {
    selectedShipmentMethod = selectedShipment;
  } else if (hasMinMaxDates && orderEddDates.minDates.UGNR && orderEddDates.minDates.UGNR) {
    selectedShipmentMethod = 'UGNR';
  } else if (hasMinMaxDates && orderEddDates.minDates.PMDC && orderEddDates.maxDates.PMDC) {
    selectedShipmentMethod = 'PMDC';
  } else if (hasMinMaxDates && orderEddDates.minDates.U2AK && orderEddDates.maxDates.U2AK) {
    selectedShipmentMethod = 'U2AK';
  }
  return selectedShipmentMethod;
};

export const getDateRange = (orderEddDates, selectedShipment, withoutFormat) => {
  const selectedShipmentMethod = getSelectedShipment(orderEddDates, selectedShipment);
  const minDate =
    orderEddDates && orderEddDates.minDates && orderEddDates.minDates[selectedShipmentMethod];
  const maxDate =
    orderEddDates && orderEddDates.maxDates && orderEddDates.maxDates[selectedShipmentMethod];
  const minDateObj = new Date(minDate);
  const maxDateObj = new Date(maxDate);
  let rangeDate = null;
  if (minDate && maxDate) {
    if (withoutFormat) {
      rangeDate = `${minDate} | ${maxDate}`;
    } else {
      rangeDate = `${getFormattedDate(minDateObj)} - ${getFormattedDate(maxDateObj)}`;
    }
  }
  if (minDateObj.getTime() === maxDateObj.getTime()) {
    if (withoutFormat) {
      rangeDate = minDate;
    } else {
      rangeDate = getFormattedDate(minDate);
    }
  }
  return rangeDate;
};

export const getSelectedShipmentMsg = (selectedShipment, shipmentMethods) => {
  let msgStr = '';
  if (shipmentMethods && selectedShipment) {
    const shippingSpeed = selectedShipment.shippingSpeed
      ? `- ${selectedShipment.shippingSpeed}`
      : '';
    msgStr = `${selectedShipment.displayName || ''} ${shippingSpeed}`;
  }
  return msgStr;
};

export const isCouponsUpdateRequired = (couponsFetchTimestamp, couponsFetchTimeout) => {
  const lastUpdatedDateTime = couponsFetchTimestamp ? new Date(couponsFetchTimestamp) : new Date();
  return differenceInMinutes(new Date(), lastUpdatedDateTime) > couponsFetchTimeout;
};

/**
 * @summary This method serves the purpose of checking boss or bopis enabled flag to set in state.
 * @param {Object} sessionState Session Redcuer state slice
 * @param {string} stateCookie Statae Cookie value set by Akamai
 * @param {boolean} isCrossBrand Specifies the other brand details
 */
export const checkBossBopisEnabled = (sessionState, stateCookie, isCrossBrand) => (type) => {
  const { siteDetails, otherBrandSiteDetails } = sessionState;

  const xAppConfig = isCrossBrand ? otherBrandSiteDetails : siteDetails;
  const globalFlag = type === 'BOSS' ? 'IS_BOSS_ENABLED' : 'BOPIS_ENABLED';
  return (
    parseBoolean(xAppConfig[globalFlag]) &&
    `|${xAppConfig[`${type}_ENABLED_STATES`]}|`.indexOf(`|${stateCookie || ''}|`) >= 0
  );
};

export const isUpdateRequired = (fetchTimestamp, fetchTimeout) => {
  const lastUpdatedDateTime = fetchTimestamp ? new Date(fetchTimestamp) : new Date();
  return differenceInMinutes(new Date(), lastUpdatedDateTime) > fetchTimeout;
};

/**
 * @method getPaymentDetails - payment details info
 * @param orderDetailsData
 */
export const getPaymentDetails = (orderDetailsData) => {
  const { appliedGiftCards } = orderDetailsData;
  let paymentModes = [];
  if (appliedGiftCards && appliedGiftCards.length) {
    paymentModes = appliedGiftCards.map((giftCard) => {
      const { cardType, endingNumbers } = giftCard;
      return `${cardType || ''} x${endingNumbers ? endingNumbers.slice(-4) : ''}`;
    });
  }

  const {
    checkout: { billing },
  } = orderDetailsData;
  const { card } = billing;
  if (!card) return paymentModes;

  if (card.afterpay) {
    paymentModes.push(AFTERPAY);
  } else if (card.endingNumbers) {
    paymentModes.push(`${card.cardType || ''} x${card.endingNumbers.slice(-4)}`);
  } else if (card.cardType) {
    paymentModes.push(card.cardType);
  }

  return paymentModes;
};

export const setBossBopisEnabled = (stateCookie, sessionState) => {
  const bossBopisFlags = {};
  const currentBrand = (getBrand() || 'TCP').toLowerCase();
  API_CONFIG.brands.forEach((brand) => {
    const isCrosseBrand = currentBrand !== brand;
    const checkForType = checkBossBopisEnabled(sessionState, stateCookie, isCrosseBrand);
    bossBopisFlags[`isBOSSEnabled_${brand.toUpperCase()}`] = checkForType('BOSS');
    bossBopisFlags[`isBOPISEnabled_${brand.toUpperCase()}`] = checkForType('BOPIS');
  });
  return bossBopisFlags;
};

export const isValidOrderForReceipts = ({ orderNumber, orderStatus }) => {
  if (
    !orderNumber ||
    orderStatus === constants.STATUS_CONSTANTS.ORDER_EXPIRED ||
    orderStatus === constants.STATUS_CONSTANTS.EXPIRED ||
    orderStatus === constants.STATUS_CONSTANTS.ORDER_CANCELED ||
    orderStatus === constants.STATUS_CONSTANTS.ORDER_CANCELLED ||
    orderStatus === constants.STATUS_CONSTANTS.CANCELLED
  ) {
    return false;
  }
  return true;
};

/**
 * @summary This method helps to trim the extra characters as maxLength not working on some android devices.
 * @param {number} max maximum characters allowed
 * @param {string} value value of the field or input
 * @return {string} returns the value after triming the extra characters
 */
export const trimMaxCharacters = (max) => (value) => {
  if (value && value.length > max) {
    return value.substring(0, max);
  }
  return value;
};

export const getMaskedEmail = (email) => {
  if (!email) {
    return '';
  }
  const id = email.toLowerCase().split('@');
  const username = id[0].split('');
  return `${username[0]}xxxx${username[username.length - 2]}${username[username.length - 1]}@${
    id[1]
  }`;
};

export const getMultipackCount = (newMultiPackCount) => {
  let getMultiPack = 0;
  newMultiPackCount.forEach((newMultipackItem) => {
    const packItemVal = newMultipackItem && newMultipackItem.split('#')[1];
    if (packItemVal) {
      getMultiPack += parseInt(packItemVal, 10);
    }
  });
  return getMultiPack;
};

export const filterMultiPack = (getFilterData) => {
  const newgetFilterData = getFilterData.map((obj) => {
    if (!('vId' in obj) && obj.prodpartno && obj.variantId) {
      return {
        ...obj,
        vId: `${obj.prodpartno}_${obj.variantId}`,
      };
    }
    return obj;
  });
  return uniqBy(newgetFilterData, (item) => {
    return item.vId;
  });
};
let isApplePaySupportedBrowser;
export const isApplePayEligible = () => {
  if (isApplePaySupportedBrowser === undefined) {
    isApplePaySupportedBrowser =
      canUseDOM() &&
      (isSafariBrowser() || isIOSDevice()) &&
      window.ApplePaySession &&
      window.ApplePaySession.canMakePayments();
  }
  return isApplePaySupportedBrowser;
};

export const filterQuantityLabels = (filters, slpLabels) => {
  const isCountry = `${isCanada() ? 'CA' : 'US'}`;
  let quantityFilters = filters ? filters[`TCPStyleQTY${isCountry}_uFilter`] : [];
  if (quantityFilters && quantityFilters.length > 0) {
    quantityFilters = quantityFilters.map((ele) => ({
      ...ele,
      displayName:
        ele.displayName && ele.displayName.indexOf('P') < 0
          ? `${Number(ele.displayName)}${slpLabels ? slpLabels.lbl_multipack_pack_text : ''}`
          : ele.displayName,
    }));
    quantityFilters = quantityFilters.filter((quantity) => Number(quantity.id) !== 1);
  }
  return { ...filters, [`TCPStyleQTY${isCountry}_uFilter`]: quantityFilters };
};

export const getApiTimeOut = ({ clientTimeout, serverTimeout }) =>
  isServer() ? serverTimeout : clientTimeout;

export const getDecimalPrice = (num) => {
  return Number(num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
};

const STYLE_MAPS = {
  '0002': 'MPACK',
  '0005': 'VirtualMPACK',
  '0003': 'Set',
  '0006': 'VirtualSet',
};

export const getDynamicBadgeQty = (styleType, styleQty, isDynamicBadgeEnabled) => {
  const styleMapName = STYLE_MAPS[styleType] || '';
  const styleDynamicBadgeEnabled =
    (isDynamicBadgeEnabled && isDynamicBadgeEnabled[`dynamicBadge${styleMapName}Enabled`]) || '';
  return styleMapName && styleDynamicBadgeEnabled ? styleQty : 0;
};

export const badgeTranformationPrefix = (styleType) =>
  styleType === '0003' || styleType === '0006'
    ? 'l_ecom:assets:static:badge:set'
    : 'l_ecom:assets:static:badge:pack';

export const ifElseExecute = (ifFunc, elseFunc, condition) => (condition ? ifFunc() : elseFunc());

/**
 * @summary This method serves the purpose of checking an object and whether a particular is there or not
 * @param {Object} result Object for which nested value is required
 * @param {string} identifierArray all the nested keys that needs to be checked
 * @param {boolean} returnElement default return value
 */
export const nullCheck = (result, identifierArray, returnElement) => {
  if (result) {
    let obj = result;
    const existenceCheck = identifierArray.every((key) => {
      if (!obj[key]) {
        return false;
      }
      obj = obj[key];
      return true;
    });
    if (existenceCheck) {
      return obj;
    }
  }
  return returnElement;
};

/**
 * Function to get formatted Date for Orders in  yyyy-MM-dd format
 * @param {string} date
 * @returns {string} return formatted date
 */
export const formatYmd = (date) => {
  try {
    let parsedDate;
    if (
      typeof date === 'string' &&
      (date?.indexOf('st') > -1 ||
        date?.indexOf('nd') > -1 ||
        date?.indexOf('rd') > -1 ||
        date?.indexOf('th') > -1)
    ) {
      parsedDate = format(parseISO(date), 'MMMM dddO, yyyy');
    }
    parsedDate = parseISO(date);
    return isValid(parsedDate) ? format(parsedDate, 'yyyy-MM-dd') : '';
  } catch (e) {
    return '';
  }
};

export const getMaxDateValue = (dates) => {
  let maxDate = dates.split(' ').join('').split('|');
  const group = maxDate.map((date) => new Date(`${date}T00:00:00`));
  maxDate = new Date(Math.max.apply(null, group));
  return formatYmd(maxDate);
};

export const getOrderPackageStatus = (trackingNumber, shipmentData) => {
  const { trackingDetails } = shipmentData || [];
  const trackingInfo =
    trackingDetails && trackingDetails.filter((item) => item.trackingId === trackingNumber);
  return trackingInfo && trackingInfo.length && trackingInfo[0].shipmentStatus;
};

export const pushItems = (orderItems, trackPacketInfo, orderItem) => {
  const { productInfo, itemStatus, quantity } = orderItem;
  const { lineNumber, appeasementApplied } = productInfo || {};
  const { trackingNumber } = trackPacketInfo || {};
  let item;
  switch (itemStatus) {
    case ORDER_ITEM_STATUS.SHIPPED:
      item = {
        ...orderItem,
        itemInfo: { ...orderItem.itemInfo, quantity },
        lineNumber: `${lineNumber}_${trackingNumber}_3`,
        orderItemStatus: appeasementApplied
          ? ORDER_ITEM_STATUS.REFUNDED
          : ORDER_ITEM_STATUS.SHIPPED,
      };
      break;
    case ORDER_ITEM_STATUS.RETURNED:
      item = {
        ...orderItem,
        itemInfo: { ...orderItem.itemInfo, quantity },
        lineNumber: `${lineNumber}_${trackingNumber}_1`,
        orderItemStatus: ORDER_ITEM_STATUS.RETURNED,
      };
      break;
    case ORDER_ITEM_STATUS.CANCELED:
    case ORDER_ITEM_STATUS.CANCELLED:
      item = {
        ...orderItem,
        itemInfo: { ...orderItem.itemInfo, quantity },
        lineNumber: `${lineNumber}_${trackingNumber}_2`,
        orderItemStatus: ORDER_ITEM_STATUS.CANCELED,
      };
      break;
    default:
      item = {
        ...orderItem,
        itemInfo: { ...orderItem.itemInfo, quantity },
        lineNumber: `${lineNumber}_${trackingNumber}_4`,
        orderItemStatus: itemStatus,
      };
      break;
  }

  const packets = orderItems.filter((packet) => packet.trackingNumber === trackingNumber);
  if (packets && packets.length > 0) {
    packets[0].items.push(item);
  } else {
    orderItems.push({ ...trackPacketInfo, items: [item] });
  }
};

// eslint-disable-next-line sonarjs/cognitive-complexity
export const getGroupedOrderItems = (orderItems, shipmentData) => {
  const inProcess = [];
  const delivered = [];
  const inTransit = [];
  const returnInitiated = [];
  const cancelled = [];

  if (orderItems && orderItems.length) {
    orderItems.forEach((orderItem) => {
      const { trackingInfo } = orderItem;
      if (trackingInfo && trackingInfo.length > 0) {
        trackingInfo.forEach((trackData) => {
          const {
            status,
            actualDeliveryDate,
            carrierDeliveryDate,
            trackingNbr,
            trackingUrl,
            shipDate,
            quantity,
          } = trackData || {};
          const shipmentStatus = getOrderPackageStatus(trackingNbr, shipmentData);
          if (trackingNbr && shipmentStatus === ORDER_ITEM_STATUS.SHIPMENT_API_DELIVERED) {
            const trackPacketInfo = {
              orderStatus: ORDER_ITEM_STATUS.SHIPMENT_API_DELIVERED,
              packageStatus: status,
              actualDeliveryDate,
              carrierDeliveryDate,
              shipDate,
              trackingNumber: trackingNbr,
              trackingUrl,
              shippedDate: shipDate,
            };
            pushItems(delivered, trackPacketInfo, { ...orderItem, itemStatus: status, quantity });
          } else if (
            trackingNbr &&
            status !== ORDER_ITEM_STATUS.ORDER_BEING_PROCESSED &&
            status !== ORDER_ITEM_STATUS.CANCELED
          ) {
            const trackPacketInfo = {
              orderStatus: ORDER_ITEM_STATUS.IN_TRANSIT,
              packageStatus: status,
              actualDeliveryDate,
              carrierDeliveryDate,
              shipDate,
              trackingNumber: trackingNbr,
              trackingUrl,
              shippedDate: shipDate,
            };
            pushItems(inTransit, trackPacketInfo, { ...orderItem, itemStatus: status, quantity });
          } else if (status === ORDER_ITEM_STATUS.RETURN_INITIATED) {
            const trackPacketInfo = {
              orderStatus: ORDER_ITEM_STATUS.RETURN_INITIATED,
              packageStatus: status,
              actualDeliveryDate,
              carrierDeliveryDate,
              shipDate,
              trackingNumber: trackingNbr,
              trackingUrl,
              shippedDate: shipDate,
            };
            pushItems(returnInitiated, trackPacketInfo, {
              ...orderItem,
              itemStatus: status,
              quantity,
            });
          } else if (status === ORDER_ITEM_STATUS.CANCELED) {
            const trackPacketInfo = {
              orderStatus: ORDER_ITEM_STATUS.CANCELED,
              packageStatus: status,
              shippedDate: shipDate,
            };
            pushItems(cancelled, trackPacketInfo, {
              ...orderItem,
              itemStatus: status,
              quantity,
            });
          } else {
            const trackPacketInfo = {
              orderStatus: ORDER_ITEM_STATUS.IN_PROGRESS,
              packageStatus: status,
              shippedDate: shipDate,
            };
            pushItems(inProcess, trackPacketInfo, { ...orderItem, itemStatus: status, quantity });
          }
        });
      } else {
        const trackPacketInfo = {
          orderStatus: ORDER_ITEM_STATUS.IN_PROGRESS,
          packageStatus: status,
        };
        pushItems(inProcess, trackPacketInfo, orderItem);
      }
    });

    return [...delivered, ...inTransit, ...inProcess, ...returnInitiated, ...cancelled];
  }
  return [];
};

export const getTotalItemsCountInOrder = (orderItems) => {
  let totalItemsCountInOrder = 0;
  if (Array.isArray(orderItems)) {
    orderItems.forEach((orderItem) => {
      const { quantity } = orderItem;
      if (quantity) {
        totalItemsCountInOrder += quantity;
      }
    });
  }

  return totalItemsCountInOrder;
};

export const getShipmentCount = (items) => {
  let nonShipmentItems = 0;
  try {
    if (Array.isArray(items)) {
      items.forEach((item) => {
        if (
          item.orderStatus === ORDER_ITEM_STATUS.IN_PROGRESS ||
          item.orderStatus === ORDER_ITEM_STATUS.RETURN_INITIATED ||
          item.orderStatus === ORDER_ITEM_STATUS.CANCELED
        ) {
          nonShipmentItems += 1;
        }
      });
    }

    return items.length - nonShipmentItems;
  } catch {
    return nonShipmentItems;
  }
};

/**
 * Function to get image data for Quickview and Added to bag popup and convert it to image format if video
 * @param {object}
 * @returns {object} return object with product image information
 */
export const getImageData = (imgURL) => {
  const isVideoUrl = getVideoUrl(imgURL);
  const getVideoTransformation = isSafariBrowser()
    ? IMG_DATA_PLP.VID_DATA_PLP
    : IMG_DATA_PLP.VID_DATA_PLP_WEBP;
  return isVideoUrl ? getVideoTransformation : IMG_DATA_PLP.IMG_DATA;
};

export const appendToken = () => {
  let retVal = false;
  if (isServer()) {
    retVal = !!process.env.ADD_TOKEN;
  } else {
    const { shouldAppendToken } = getAPIConfig();
    retVal = !!shouldAppendToken;
  }
  return retVal;
};

export const getUrlWithHttp = (url) => url && url.replace(/(^\/\/)/, 'https:$1');

/**
 *
 * @param {*} url - string
 * @returns string
 * @example "toddler-boys-tops-and-toddler-boys-shirts-graphic-tees" => "ToddlerBoysTopsAndToddlerBoysShirtsGraphicTees"
 */
export const removeHyphensFromUrl = (url) =>
  url
    ? url
        .split(/[-\s]/)
        .map((part) => part[0]?.toUpperCase() + part?.slice(1).toLowerCase())
        .filter((part) => typeof part !== 'undefined' && part !== 'undefined')
        .join('')
    : '';

/* eslint-disable prefer-destructuring */
export const setSelectedOutfitColor = () => {
  const colorPallete = createThemeColorPalette();
  let selectedOutfitColor = 'black';
  if (isTCP()) {
    selectedOutfitColor = colorPallete.blue[500];
  } else if (isGymboree()) {
    selectedOutfitColor = colorPallete.orange[500];
  }
  return selectedOutfitColor;
};

export const hasTrackingNum = (trackingNumber) => trackingNumber && trackingNumber !== 'N/A';

export const getTrimmedTrackingNum = (orderTrackingNumber) =>
  orderTrackingNumber && orderTrackingNumber.toUpperCase().trim();

export const SEARCH_REDIRECT_KEY = 'searchRedirect';

export const getSFCCClass = (spcBag) => (spcBag ? 'rounded-container' : '');

export const isOptimizedDeleteEnabled = () => {
  const { isOptimiseCartDeleteEnabled } = isOptimizedDeleteJson;
  return isMobileApp() ? isOptimiseCartDeleteEnabled : process.env.OPTIMISE_CART_DELETE_ENABLED;
};

// function to return url without ct token
export function removeURLParameter(url, parameter) {
  // prefer to use l.search if you have a location/link object
  const urlparts = url.split('?');
  if (urlparts.length >= 2) {
    const prefix = `${encodeURIComponent(parameter)}=`;
    const pars = urlparts[1].split(/[&;]/g);
    // reverse iteration as may be destructive
    for (let i = pars.length - 1; i >= 0; i -= 1) {
      // idiom for string.startsWith
      if (pars[i].lastIndexOf(prefix, 0) !== -1) {
        pars.splice(i, 1);
      }
    }
    return `${urlparts[0]}${pars && pars.length ? `?${pars.join('&')}` : ''}`;
  }
  return url;
}

export const extractPID = (router, navigation) => {
  let pid;
  if (navigation) {
    pid = navigation.getParam('pdpUrl');
  } else {
    const [prodId] = (router.query.bid || '').split('&');
    pid = prodId || '';
  }

  const id = pid && pid.split('-');
  return id && id.length > 1 ? id[id.length - 1] : pid;
};

export const getProductStatusAndAvailability = (status) => {
  let isAvaialble = false;
  let productStatus = '';
  if (!status) {
    return { isAvaialble, productStatus };
  }
  isAvaialble = true;
  productStatus = capitalizeWords(status);
  const statusUpperCase = status.toUpperCase().trim();

  if (statusUpperCase === BOPIS_ITEM_AVAILABILITY_STATUS.OK) {
    isAvaialble = true;
    productStatus = BOPIS_ITEM_AVAILABILITY_STATUS.INSTOCK;
  }
  if (statusUpperCase === BOPIS_ITEM_AVAILABILITY_STATUS.AVAILABLE) {
    isAvaialble = true;
    productStatus = BOPIS_ITEM_AVAILABILITY_STATUS.INSTOCK;
  }
  if (statusUpperCase === BOPIS_ITEM_AVAILABILITY_STATUS.UNAVAILABLE) {
    isAvaialble = false;
    productStatus = BOPIS_ITEM_AVAILABILITY_STATUS.OUTOFSTOCK;
  }
  return { isAvaialble, productStatus };
};
export const createUUID = () => {
  let dt = new Date().getTime();
  const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
  return uuid.replace(/[xy]/g, (char) => {
    // eslint-disable-next-line no-bitwise
    const random = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    // eslint-disable-next-line no-bitwise
    return (char === 'x' ? random : (random & 0x3) | 0x8).toString(16);
  });
};

export default {
  getVideoUrl,
  getOrderStatusForNotification,
  validateDiffInDaysNotification,
  getPromotionalMessage,
  getStaticFilePath,
  getStaticPageFilePath,
  getIconPath,
  getFlagIconPath,
  getLocator,
  getBrand,
  getBrandNameFromHref,
  isClient,
  isMobileApp,
  isIOSDevice,
  isServer,
  getAPIConfig,
  isCanada,
  bindAllClassMethodsToThis,
  isGymboree,
  isTCP,
  isSNJ,
  getAddressFromPlace,
  formatAddress,
  formatPhoneNumber,
  getLabelValue,
  getCacheKeyForRedis,
  calculateAge,
  generateUniqueKeyUsingLabel,
  getErrorSelector,
  isValidDate,
  formatDate,
  parseStoreHours,
  parseBoolean,
  sanitizeEntity,
  getFormSKUValue,
  configureInternalNavigationFromCMSUrl,
  getDateInformation,
  buildStorePageUrlSuffix,
  extractFloat,
  getModifiedLanguageCode,
  getTranslateDateInformation,
  stringify,
  getAssetHostURL,
  changeImageURLToDOM,
  getStoreHours,
  generateTraceId,
  insertIntoString,
  getStyliticsUserName,
  getStyliticsRegion,
  canUseDOM,
  getLabelsBasedOnPattern,
  calculatePriceValue,
  getProductUrlForDAM,
  convertNumToBool,
  triggerPostMessage,
  cropVideoUrl,
  isIE11,
  isIEBrowser,
  formatProductsData,
  getPageName,
  isWebViewPage,
  disableTcpApp,
  disableTcpAppProduct,
  pathHasHyphenSlash,
  createLayoutPath,
  getFontsURL,
  checkIfUserInfoCallRequired,
  getBootstrapCachedData,
  setBootstrapCachedData,
  removeFileExtension,
  styleOverrideEngine,
  parseTrackingParamsInModuleClassNames,
  mergeUrlQueryParams,
  isSafariBrowser,
  getFormattedDate,
  getPaymentDetails,
  getDateRange,
  isValidOrderForReceipts,
  trimMaxCharacters,
  getSafariVersion,
  getMaskedEmail,
  getStringAfterSplit,
  isApplePayEligible,
  filterQuantityLabels,
  getApiTimeOut,
  getDecimalPrice,
  hashValuesReplace,
  utilPdpArrayHeader,
  getDynamicBadgeQty,
  badgeTranformationPrefix,
  nullCheck,
  ifElseExecute,
  getGroupedOrderItems,
  getOrderPackageStatus,
  formatYmd,
  getMaxDateValue,
  addDays,
  getImageData,
  appendToken,
  getUrlWithHttp,
  setSelectedOutfitColor,
  hasTrackingNum,
  getTrimmedTrackingNum,
  SEARCH_REDIRECT_KEY,
  removeExtension,
  removeHyphensFromUrl,
  removeURLParameter,
  extractPID,
  getImageFilePath,
  createUUID,
};
