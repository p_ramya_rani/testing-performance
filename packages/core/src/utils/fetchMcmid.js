// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable no-underscore-dangle */
import { isClient } from '@tcp/core/src/utils';
import { readCookie } from './cookie.util';
import { ADOBE_VENDOR_KEY } from '../constants/Adobe.constants';

export const getMcmId = () => {
  let mcmid;
  if (window._satellite && window._satellite.getVisitorId) {
    mcmid = window._satellite.getVisitorId().getMarketingCloudVisitorID();
  } else {
    const adobeCookie = readCookie(ADOBE_VENDOR_KEY);
    const adobeCookieArr = adobeCookie ? adobeCookie.split('|') : [];

    mcmid = adobeCookieArr.length ? adobeCookieArr[adobeCookieArr.indexOf('MCMID') + 1] : '';
  }

  return mcmid;
};

export const getSessionId = () => {
  const ids = {};
  if (isClient() && window._satellite) {
    const mboxCookie = window._satellite.readCookie('mbox');
    const mboxCookieArr = mboxCookie ? mboxCookie.split('|') : [];
    mboxCookieArr.forEach(arr => {
      const idsArr = arr.split('#');
      if (idsArr && idsArr.length) {
        if (arr.includes('PC#')) {
          ids.tntId = idsArr[idsArr.indexOf('PC') + 1];
        } else if (arr.includes('session#')) {
          ids.sessionId = idsArr[idsArr.indexOf('session') + 1];
        }
      }
    });
  }

  return ids;
};
export default { getMcmId, getSessionId };
