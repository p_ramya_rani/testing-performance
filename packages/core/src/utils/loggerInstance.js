// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable no-underscore-dangle */
const LoggerClass = require('./logger');
const trackErrorClass = require('./errorReporter.util');

const INTEGRATE_LOG_WITH_ERROR_REPORTER_FLAG = true; // set it false to disable reporting with logging feature
const LOGLEVEL_INTEGRATED_WITH_ERROR_REPORTER = ['error']; // add other log levels where tracking along with logging is required
const noArgumentPassedError = `JS Error - Unexpectedly An Error Occurred In JS Execution With No Details`;
const Logger = new LoggerClass();

const { loggerClass = null, loggerInstance } = Logger.initializeLogger();
const loggerWithErrorReporter = {};

/*
*
Standard way to use logger should be to send trackError arguments as details with caught error object in it -
For example -
logger.error('custom error-mssg string - with track error object - ',{
  error: err, //  error caught in try/catch
  extraData: {
    component: 'ErrorReporter',
    payloadRecieved: { payload:'payload'},
  },
})

However, Different variations of logger.error existed in code and needed to be supported  -

logger.error(undefined)
logger.error('Only string message')
logger.error(err)
logger.error('error string mssg - with catch error object - ',err)
logger.error('error string mssg - with track error as string - ',{
  error: 'custom string message',
  extraData: {
    component: 'ErrorReporter',
    payloadRecieved: { payload:'payload'},
  },
})
logger.error('error string mssg - with track error as object - ',{
  error: err,
  extraData: {
    component: 'ErrorReporter',
    payloadRecieved: { payload:'payload'},
  },
})
*/

const getErrorDetailsFromObj = error => {
  const { name = '', message = '', response = {} } = error;
  if (response && response.req && response.req._data && response.req._data.paymentInstruction) {
    response.req._data.paymentInstruction = [];
  }
  return `${name} - ${message} - ${JSON.stringify(error)}`;
};

const getLoggerMessages = onlyLoggerArgs => onlyLoggerArgs.length && onlyLoggerArgs.join(' ');

const deriveErrorMssgs = (errorReporterArgument, onlyLoggerArgs) => {
  let logErrorMessage = '';
  let errorReporterMssg = '';
  if (!onlyLoggerArgs.length && !errorReporterArgument) {
    // handling scenario where no error detail is provided
    logErrorMessage = noArgumentPassedError;
    errorReporterMssg = logErrorMessage;
  } else if (errorReporterArgument) {
    const { error = null } = errorReporterArgument || {};
    if (!error) {
      // handling scenario where directly JS Error Object is passed
      logErrorMessage = getErrorDetailsFromObj(errorReporterArgument);
      errorReporterMssg = errorReporterArgument;
    } else {
      // where track error Object is passed
      logErrorMessage = typeof error === 'string' ? error : getErrorDetailsFromObj(error);
      errorReporterMssg = error;
    }
  } else {
    // when no error object is passed
    errorReporterMssg = getLoggerMessages(onlyLoggerArgs);
  }
  return {
    logErrorMessage,
    errorReporterMssg,
  };
};

LOGLEVEL_INTEGRATED_WITH_ERROR_REPORTER.forEach(item => {
  try {
    loggerWithErrorReporter[item] = (...args) => {
      let errorReporterArgument = null;
      const onlyLoggerArgs = [];
      args.forEach(arg => {
        if (typeof arg === 'object' && !errorReporterArgument) {
          errorReporterArgument = arg;
        } else if (arg) {
          onlyLoggerArgs.push(arg);
        }
      });

      // any argument passed as object consider it for Reporting data,
      // In case of no object argument, error string will be reported as error message
      const { logErrorMessage, errorReporterMssg } = deriveErrorMssgs(
        errorReporterArgument,
        onlyLoggerArgs
      );
      // push additional caught error logging in logger statement
      onlyLoggerArgs.push(logErrorMessage);

      // in case of only logging required, pass the object param as reportError as false
      if (
        INTEGRATE_LOG_WITH_ERROR_REPORTER_FLAG &&
        (!errorReporterArgument || errorReporterArgument.reportError !== false)
      ) {
        const { extraData: extraDetails } = errorReporterArgument || {};
        trackErrorClass.trackError({
          ...errorReporterArgument,
          error: errorReporterMssg,
          extraData: {
            ...extraDetails,
            requestIdInfo: LoggerClass.getRequestIdInfo && LoggerClass.getRequestIdInfo(),
            loggedMessage: getLoggerMessages(onlyLoggerArgs), // passing logger message in extraData as well
          },
        });
      }
      loggerInstance[item](...onlyLoggerArgs);
    };
  } catch (err) {
    console.log('JS Error occurred in loggerInstance while logging error', err);
  }
});

module.exports = Object.assign(
  Object.create((loggerClass && loggerClass.prototype) || {}),
  loggerInstance,
  loggerWithErrorReporter
);
