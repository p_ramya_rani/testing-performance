/* eslint-disable max-lines */
// eslint-disable-next-line import/no-unresolved
/* eslint-disable complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import Router from 'next/router';
import smoothScroll from 'smoothscroll-polyfill';
import isEmpty from 'lodash/isEmpty';
import isFinite from 'lodash/isFinite';
import {
  disableBodyScroll as disableBodyScrollLib,
  enableBodyScroll as enableBodyScrollLib,
  clearAllBodyScrollLocks,
} from 'body-scroll-lock';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import logger from '@tcp/core/src/utils/loggerInstance';
import { setProp } from '@tcp/core/src/analytics/utils';
import { trackError } from '@tcp/core/src/utils/errorReporter.util';
import {
  siteConfigData,
  fetchHeaderData,
  fetchFooterData,
  fetchLabelsData,
} from '@tcp/core/src/reduxStore/actions';
import { ENV_PRODUCTION, ENV_DEVELOPMENT } from '../constants/env.config';
import icons from '../config/icons';
import { breakpoints, mediaQuery } from '../../styles/themes/TCP/mediaQuery';
import {
  getAPIConfig,
  isClient,
  isServer,
  getBrand,
  SEARCH_REDIRECT_KEY,
  isTCP,
  isGymboree,
} from './utils';
import { API_CONFIG } from '../services/config';
import { defaultCountries, defaultCurrencies } from '../constants/site.constants';
import { ROUTING_MAP, ROUTE_PATH } from '../config/route.config';
import googleMapConstants from '../constants/googleMap.constants';
import CONSTANTS from '../components/common/molecules/SearchBar/SearchBar.constants';
import { deviceTiers } from '../../../web/src/constants/constants';
import internalEndpoints from '../components/features/account/common/internalEndpoints';

const RECOMMENDATION_GA_KEY = 'reccObj';
const EVENT_ADD_TO_CART = 'add to cart';
const EVENT_PRODUCT_DETAIL = 'productdetail';
const EVENTS_INCLUDE_METRIC50 = [EVENT_PRODUCT_DETAIL];
export const DIMENSION92_TEXT = 'm-';

const MONTH_SHORT_FORMAT = {
  JAN: 'Jan',
  FEB: 'Feb',
  MAR: 'Mar',
  APR: 'Apr',
  MAY: 'May',
  JUN: 'Jun',
  JUL: 'Jul',
  AUG: 'Aug',
  SEP: 'Sep',
  OCT: 'Oct',
  NOV: 'Nov',
  DEC: 'Dec',
};

const FIXED_HEADER = {
  LG_HEADER: 70,
  SM_HEADER: 50,
  LG_CONDENSED_HEADER: 145,
};

const MARGIN_BREADCRUMB = 55;

export const setSessionStorage = (arg) => {
  const { key, value } = arg;
  if (isClient()) {
    try {
      return window.sessionStorage.setItem(key, value);
    } catch {
      return null;
    }
  }
  return null;
};

export const extractPID = (pid) => {
  const id = pid && pid.split('-');
  let productId = id && id.length > 1 ? `${id[id.length - 2]}_${id[id.length - 1]}` : pid;
  if (
    (id.indexOf('Gift') > -1 || id.indexOf('gift') > -1) &&
    (id.indexOf('Card') > -1 || id.indexOf('card') > -1)
  ) {
    productId = 'gift';
  }
  return productId;
};

export const getSessionStorage = (key) => {
  if (isClient()) {
    try {
      return window.sessionStorage.getItem(key);
    } catch {
      return null;
    }
  }
  return null;
};

export const removeSessionStorage = (key) => {
  if (isClient()) {
    try {
      return window.sessionStorage.removeItem(key);
    } catch {
      return null;
    }
  }
  return null;
};
// Return empty user-agent in case of web
export const getUserAgent = () => '';

export const getCustomHeaders = () => {
  return {};
};

export const importGraphQLClientDynamically = (module) => {
  return import(`../services/handler/${module}`);
};

export const getUrlParameter = (name) => {
  const replacedName = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]');
  const regex = new RegExp(`[\\?&]${replacedName}=([^&#]*)`);
  const results = regex.exec(window.location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

export const importGraphQLQueriesDynamically = (query) => {
  return import(`../services/handler/graphQL/queries/${query}`);
};

export const getLocationOrigin = () => {
  return window.location.origin;
};

export const canUseDOM = () => {
  return typeof window !== 'undefined' && window.document && window.document.createElement;
};

export const isProduction = () => {
  return process.env.NODE_ENV === ENV_PRODUCTION;
};

export const isDevelopment = () => {
  return process.env.NODE_ENV === ENV_DEVELOPMENT;
};

export const getSiteId = () => {
  const { siteId } = getAPIConfig();
  return siteId;
};

const isCompleteHTTPUrl = (url) => /^(http|https):\/\//.test(url);

const getRouteHref = (noSlugPath) => {
  const pathArray = noSlugPath ? noSlugPath.replace(/\//, '&').split('&') : ['', ROUTING_MAP.home];
  const pathValue = pathArray[1];
  return ROUTING_MAP[pathValue] || ROUTING_MAP.home;
};

/**
 * @summary This is to return the Page (inside of Pages folder) which is mapped to the route path
 * for ex: /home will return /index file name.
 * @param {String || Object} toPath - list of color options
 * @returns {String || Object} Mapped actual page href path
 */
export const getMappedPageHref = (toPath = '') => {
  if (typeof toPath === 'string') {
    if (isCompleteHTTPUrl(toPath)) return toPath;
    const [noSlugPath = '/', query = ''] = toPath.split('?');
    const mappedToHref = getRouteHref(noSlugPath);
    return query ? `${mappedToHref}?${query}` : mappedToHref;
  }
  const { pathname = '', query } = toPath;
  if (isCompleteHTTPUrl(pathname)) return pathname;
  const mappedToHref = getRouteHref(pathname);
  return {
    pathname: mappedToHref,
    query,
  };
};

/**
 * @summary This is to return the asPath with additional slug values appended
 * @param {String} as - asPath
 * @param {String} siteId - siteId dynamic value to be appended
 * @returns {String} Path with slug value appended
 */
export const getAsPathWithSlug = (as, siteId = getSiteId()) => {
  return isCompleteHTTPUrl(as) ? as : `/${siteId}${as}`;
};

export const routerPush = (href, as, query, siteId = getSiteId()) => {
  const relHref = getMappedPageHref(href);
  const asPath = getAsPathWithSlug(as, siteId);
  return Router.push(relHref, asPath, { query });
};

export const routerReplace = (href, as, query, siteId = getSiteId()) => {
  const relHref = getMappedPageHref(href);
  const asPath = getAsPathWithSlug(as, siteId);
  return Router.replace(relHref, asPath, { query });
};

/**
 * This common function works for finding key in an object.
 * Please refer Account.jsx in core/src/components/features/account/Account/Account.jsx
 */
export const getObjectValue = (obj, defaultVal, ...params) => {
  if (!obj) return defaultVal;

  let objRef = obj;
  const paramsLen = params.length;
  for (let i = 0; i < paramsLen; i += 1) {
    if (objRef[params[i]]) {
      objRef = objRef[params[i]];
    } else {
      objRef = null;
      break;
    }
  }
  return objRef || defaultVal;
};

export const createUrlSearchParams = (query = {}) => {
  const queryParams = [];
  const keys = Object.keys(query);
  for (let i = 0, l = keys.length; i < l; i += 1) {
    queryParams.push(`${keys[i]}=${query[keys[i]]}`);
  }
  return queryParams.join('&');
};

export function getHostName() {
  return window.location.hostname;
}

export const buildUrl = (options) => {
  if (typeof options === 'object') {
    const { pathname, query } = options;
    let url = pathname;
    if (typeof query === 'object') {
      url += `?${createUrlSearchParams(query)}`;
    }
    return url;
  }
  return options;
};

export const getIconCard = (icon) => {
  switch (icon) {
    case 'disc-small':
      return icons.dicoveryCard;
    case 'mc-small':
      return icons.masterCard;
    case 'amex-small':
      return icons.amexCard;
    case 'visa-small':
      return icons.visaSmall;
    case 'gift-card-small':
      return icons.giftCardSmall;
    case 'place-card-small':
      return icons.plccCard;
    case 'venmo-blue-acceptance-mark':
      return icons.venmoCard;
    default:
      return icons.visaSmall;
  }
};

export const getCreditCardExpirationOptionMap = () => {
  const expMonthOptionsMap = [
    { id: '1', displayName: MONTH_SHORT_FORMAT.JAN },
    { id: '2', displayName: MONTH_SHORT_FORMAT.FEB },
    { id: '3', displayName: MONTH_SHORT_FORMAT.MAR },
    { id: '4', displayName: MONTH_SHORT_FORMAT.APR },
    { id: '5', displayName: MONTH_SHORT_FORMAT.MAY },
    { id: '6', displayName: MONTH_SHORT_FORMAT.JUN },
    { id: '7', displayName: MONTH_SHORT_FORMAT.JUL },
    { id: '8', displayName: MONTH_SHORT_FORMAT.AUG },
    { id: '9', displayName: MONTH_SHORT_FORMAT.SEP },
    { id: '10', displayName: MONTH_SHORT_FORMAT.OCT },
    { id: '11', displayName: MONTH_SHORT_FORMAT.NOV },
    { id: '12', displayName: MONTH_SHORT_FORMAT.DEC },
  ];

  const expYearOptionsMap = [];
  const nowYear = new Date().getFullYear();
  for (let i = nowYear; i < nowYear + 11; i += 1) {
    expYearOptionsMap.push({ id: i.toString(), displayName: i.toString() });
  }

  return {
    monthsMap: expMonthOptionsMap,
    yearsMap: expYearOptionsMap,
  };
};

/**
 * Checks if a specific element is in the current view port or not
 */

export const checkInViewPort = (elem) => {
  if (!elem) return false;
  const rects = elem && elem.getBoundingClientRect();

  return (
    rects.top >= 0 &&
    rects.left >= 0 &&
    rects.right <= (window.innerWidth || document.documentElement.clientWidth) &&
    rects.bottom <= (window.innerHeight || document.documentElement.clientHeight)
  );
};

/**
 * Calculates browser width and height, and informs the current viewport as per the defined viewport settings
 */
export const getViewportInfo = () => {
  if (typeof window === 'undefined') return null;
  if (window.viewportInfo) return window.viewportInfo;

  const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  const height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  const isMobile = width < parseInt(breakpoints.medium, 10);
  const isTablet = !isMobile && width < parseInt(breakpoints.large, 10);
  const isDesktop = !isMobile && !isTablet;

  window.viewportInfo = {
    width,
    height,
    isMobile,
    isTablet,
    isDesktop,
  };

  return window.viewportInfo;
};

/**
 * Show Dark Overlay in background
 */
export const showOverlay = () => {
  const className = 'dark-overlay';
  if (typeof window !== 'undefined' && document.getElementsByClassName(className)[0]) {
    document.getElementsByClassName(className)[0].style.display = 'block';
  }
};

/**
 * Remove Dark Overlay from background
 */
export const closeOverlay = () => {
  const className = 'dark-overlay';
  if (typeof window !== 'undefined' && document.getElementsByClassName(className)[0]) {
    document.getElementsByClassName(className)[0].style.display = 'none';
  }
};

export const bindAllClassMethodsToThis = (obj, namePrefix = '', isExclude = false) => {
  const prototype = Object.getPrototypeOf(obj);
  // eslint-disable-next-line
  for (let name of Object.getOwnPropertyNames(prototype)) {
    const descriptor = Object.getOwnPropertyDescriptor(prototype, name);
    const isGetter = descriptor && typeof descriptor.get === 'function';
    // eslint-disable-next-line
    if (isGetter) continue;
    if (
      typeof prototype[name] === 'function' && name !== 'constructor' && isExclude
        ? !name.startsWith(namePrefix)
        : name.startsWith(namePrefix)
    ) {
      // eslint-disable-next-line
      obj[name] = prototype[name].bind(obj);
    }
  }
};

export const scrollPage = (x = 0, y = 0) => {
  if (window) {
    const timer = window.setTimeout(() => {
      window.scrollTo(x, y);
      clearTimeout(timer);
    }, 0);
  }
};

export const scrollTopElement = (elem) => {
  if (window) {
    document.getElementById(elem).scrollTop = 0;
  }
};

export const refreshPage = () => {
  if (isClient() && window) {
    window.location.reload();
  }
};

/**
 * 2019-11-05: Hotfix PR needed to address issue with this
 * array of countries increasing in size with each call
 * (up to over 400k items in some pages).
 *
 * @TODO RCA is re-rendering of the App component itself.
 * Need to fix that. This is just a symptom.
 */
export const getCountriesMap = (data) => {
  return [
    ...defaultCountries,
    ...data.map((value) =>
      Object.assign({}, value.country, {
        siteId: 'us',
        currencyId: value.currency.id,
      })
    ),
  ];
};

export const getCurrenciesMap = (data) => {
  const currencies = defaultCurrencies;
  data.map((value) => currencies.push(Object.assign({}, value.currency, value.exchangeRate)));
  return currencies.filter(
    (currency, index, self) => index === self.findIndex((cur) => cur.id === currency.id)
  );
};

export const onlyCurrencyChanged = (
  newCountry,
  oldCountry,
  newLanguage,
  oldLanguage,
  newCurrency,
  oldCurrency
) => {
  return newCountry === oldCountry && newLanguage === oldLanguage && newCurrency !== oldCurrency;
};

export const languageRedirect = (
  newCountry,
  oldCountry,
  newSiteId,
  newLanguage,
  oldLanguage,
  newCurrency,
  oldCurrency
) => {
  const { protocol, host } = window.location;
  let baseDomain = host.replace(`${oldLanguage}.`, '');

  /**
   * Replacing www. to blank as we have URLs with www. for en language hence removing www. in all cases except en
   */
  const stringToReplace = 'www.';
  if (newLanguage !== 'en' && baseDomain.indexOf(stringToReplace) !== -1) {
    baseDomain = baseDomain.replace(stringToReplace, '');
  }

  let hostURL = '';
  /*
   As per production implementation, if the country and language both get changed at the same time, the language will not append in the URL.
   By default, the English language will entertain.
  */
  if (newLanguage === 'en') {
    hostURL = `${protocol}//`;
  } else {
    hostURL = `${protocol}//${newLanguage}.`;
  }

  if (hostURL && !(newLanguage === oldLanguage && newCountry === oldCountry)) {
    window.location = `${hostURL}${baseDomain}${getAsPathWithSlug(
      ROUTE_PATH.home,
      newSiteId || getSiteId()
    )}`;
  }

  if (
    onlyCurrencyChanged(newCountry, oldCountry, newLanguage, oldLanguage, newCurrency, oldCurrency)
  ) {
    refreshPage();
  }
};

/*
 *
 * @param {object} event the HTML element's element
 * @param {number} key key for which the event needs to be triggered
 * @param {function} method method passed which is to be invoked.
 * @description this method invokes the parameter method received when respective
 * keybord key is triggered
 */
export const handleGenericKeyDown = (event, key, method) => {
  if (event.keyCode === key) {
    method();
  }
};

const getBvSharedKey = (processEnv, apiSiteInfo) => {
  return processEnv.BV_SHARED_KEY || apiSiteInfo.BV_SHARED_KEY;
};

const getUnbxApiKey = (apiSiteInfo, processEnv, countryKey, language) => {
  const unbxdApiKeyTCP = processEnv[`UNBXD_API_KEY${countryKey}_${language.toUpperCase()}_TCP`];
  const unbxdApiKeyGYM = processEnv[`UNBXD_API_KEY${countryKey}_${language.toUpperCase()}_GYM`];
  const unbxdApiKeySNJ = processEnv[`UNBXD_API_KEY${countryKey}_${language.toUpperCase()}_SNJ`];
  const unbxdTCP = processEnv.UNBXD_DOMAIN_TCP || apiSiteInfo.unbxd;
  const unbxdGYM = processEnv.UNBXD_DOMAIN_GYM || apiSiteInfo.unbxd;
  const unbxdSNJ = processEnv.UNBXD_DOMAIN_SNJ || apiSiteInfo.unbxd;
  const apiEndPoint = processEnv.API_DOMAIN || '';
  return {
    apiEndPoint,
    unbxdApiKeyTCP,
    unbxdApiKeyGYM,
    unbxdApiKeySNJ,
    unbxdTCP,
    unbxdGYM,
    unbxdSNJ,
  };
};

const getAPIInfoFromEnv = (apiSiteInfo, processEnv, countryKey = '', language = '') => {
  const unBxObj = getUnbxApiKey(apiSiteInfo, processEnv, countryKey, language);
  const apiEndpoint = unBxObj?.apiEndPoint;
  const unbxdApiKeyTCP = unBxObj?.unbxdApiKeyTCP;
  const unbxdApiKeyGYM = unBxObj?.unbxdApiKeyGYM;
  const unbxdApiKeySNJ = unBxObj?.unbxdApiKeySNJ;
  const webDomainName = processEnv.DOMAIN_NAME || '';
  const assetEndpoint = processEnv.STATIC_ASSET_PATH;

  /**
   * BRANDID is in all env / properties files. If this is turning up as TCP always please check env / properties file.
   */
  return {
    traceIdCount: 0,
    langId: processEnv.LANGID || apiSiteInfo.langId,
    locale: `${language.toLowerCase()}${countryKey.toUpperCase()}`, // for some reason countryKey has an underscore already
    MELISSA_KEY: processEnv.MELISSA_KEY || apiSiteInfo.MELISSA_KEY,
    BV_API_KEY: processEnv.BV_API_KEY || apiSiteInfo.BV_API_KEY,
    BV_API_URL: processEnv.BV_API_URL || apiSiteInfo.BV_URL,
    BV_SHARED_KEY: getBvSharedKey(processEnv, apiSiteInfo),
    GRAPHQL_CLIENT_CACHE_CLEAR_INTERVAL: +processEnv.GQL_PURGE_TIME_MINS,
    GRAPHQL_API_TIMEOUT: +processEnv.GRAPHQL_API_TIMEOUT,
    GRAPHQL_API_TIMEOUT_CLIENT: +processEnv.GRAPHQL_API_TIMEOUT_CLIENT,
    assetHostTCP: processEnv.WEB_DAM_HOST || apiSiteInfo.assetHost,
    assetHostGYM: processEnv.WEB_DAM_HOST || apiSiteInfo.assetHost,
    assetHostSNJ: processEnv.WEB_DAM_HOST || apiSiteInfo.assetHost,
    fontsAssetPath: processEnv.FONTS_HOST,
    productAssetPathTCP: processEnv.DAM_PRODUCT_IMAGE_PATH_TCP,
    productAssetPathGYM: processEnv.DAM_PRODUCT_IMAGE_PATH_GYM,
    productAssetPathSNJ: processEnv.DAM_PRODUCT_IMAGE_PATH_SNJ,
    domain: `${apiEndpoint}/${processEnv.API_IDENTIFIER}/`,
    apiEndpoint: `${apiEndpoint}/`,
    assetHost: `${apiEndpoint}/`,
    staticAssetHost: `${assetEndpoint}`,
    facebookShareURL: apiSiteInfo.FACEBOOK_SHARE_URL,
    unbxdTCP: unBxObj?.unbxdTCP,
    unbxdGYM: unBxObj?.unbxdGYM,
    unbxdSNJ: unBxObj?.unbxdSNJ,
    fbkey: processEnv.FACEBOOKKEY,
    instakey: processEnv.INSTAGRAM,
    webDomainName,
    unboxKeyTCP: `${unbxdApiKeyTCP}/${
      processEnv[`UNBXD_SITE_KEY${countryKey}_${language.toUpperCase()}_TCP`]
    }`,
    unbxdApiKeyTCP,
    unboxKeyGYM: `${unbxdApiKeyGYM}/${
      processEnv[`UNBXD_SITE_KEY${countryKey}_${language.toUpperCase()}_GYM`]
    }`,
    unbxdApiKeyGYM,
    unboxKeySNJ: `${unbxdApiKeySNJ}/${
      processEnv[`UNBXD_SITE_KEY${countryKey}_${language.toUpperCase()}_SNJ`]
    }`,
    unbxdApiKeySNJ,
    envId: processEnv.ENV_ID,
    previewEnvId: processEnv.PREVIEW_ENV,
    BAZAARVOICE_SPOTLIGHT: processEnv.BV_SPOTLIGHT_API_KEY,
    BAZAARVOICE_REVIEWS: processEnv.BV_PRODUCT_REVIEWS_API_KEY,
    CANDID_API_KEY: process.env.CANDID_API_KEY,
    CANDID_API_URL: process.env.CANDID_URL,
    googleApiKey: process.env.GOOGLE_MAPS_API_KEY,
    mapBoxApiKey: process.env.MAPBOX_API_KEY,
    mapBoxApiURL: process.env.MAPBOX_API_URL,
    mapboxStoreLocatorApiURL: process.env.MAPBOX_STORE_LOCATOR_API_URL,
    ACQUISITION_ID: process.env.ACQUISITION_ID,
    raygunApiKey: processEnv.RAYGUN_API_KEY,
    isErrorReportingNodeActive: processEnv.IS_ERROR_REPORTING_NODE_ACTIVE,
    isErrorReportingBrowserActive: processEnv.IS_ERROR_REPORTING_BROWSER_ACTIVE,
    channelId: API_CONFIG.channelIds.Desktop, // TODO - Make it dynamic for all 3 platforms
    borderFree: processEnv.BORDERS_FREE,
    borderFreeComm: processEnv.BORDERS_FREE_COMM,
    paypalEnv: processEnv.PAYPAL_ENV,
    paypalStaticUrl: processEnv.RWD_APP_PAYPAL_STATIC_DOMAIN,
    crossDomain: processEnv.CROSS_DOMAIN,
    brandSpecificCrossDomain: {
      TCP: processEnv.CROSS_DOMAIN_TCP || '',
      GYM: processEnv.CROSS_DOMAIN_GYM || '',
      SNJ: processEnv.CROSS_DOMAIN_SNJ || '',
    },
    styliticsUserNameTCP: processEnv.STYLITICS_USERNAME_TCP,
    styliticsUserNameGYM: processEnv.STYLITICS_USERNAME_GYM,
    styliticsRegionTCP: processEnv.STYLITICS_REGION_TCP && countryKey.split('_')[1],
    styliticsRegionGYM: processEnv.STYLITICS_REGION_GYM,
    damCloudName: processEnv.RWD_CLOUDINARY_CLOUD_NAME,
    analyticsUrl: processEnv.ANALYTICS_SCRIPT_URL,
    RWD_WEB_SNARE_SCRIPT_URL: processEnv.SNARE_SCRIPT_URL,
    instagramStatus: processEnv.RWD_INSTAGRAM_STATUS,
    enableGqlWarn: processEnv.ENABLE_GQL_WARN || apiSiteInfo.GQL_WARN_ENABLED || false,
    gqlWarnThreshold: processEnv.GQL_WARN_THRESHOLD || apiSiteInfo.GQL_WARN_THRESHOLD || 10000,
    queryOverridesUrl: processEnv.QUERY_OVERRIDES_JSON || '',
    monetateUrl: processEnv.MONETATE_URL,
    monetateChannel: processEnv.MONETATE_CHANNEL,
    isCustomerServiceHelpEnable: processEnv.IS_CSH_ENABLED,
    afterpayButtonKey: processEnv.AFTERPAY_BUTTON_MERCHANT_KEY,
    isNonInvStagEnabled: processEnv.IS_NON_INV_STAG_ENABLED,
    isCandidEnabled: processEnv.CANDID_ENABLED,
    snjMinOrderNo: process.env.SNJ_MIN_ORDER_NO,
  };
};

const getModulesThatNeedRefresh = async () => {
  try {
    const { queryOverridesUrl = '', locale } = await getAPIConfig();
    if (queryOverridesUrl.length < 1) return [];
    // eslint-disable-next-line no-undef
    const pageGenDate = __NEXT_DATA__.props.initialProps.pageProps.PAGE_GENERATION_DATE;
    const pageTimestamp = new Date(pageGenDate).getTime();
    const response = await fetch(queryOverridesUrl);
    if (await !response.ok) {
      logger.error('Error fetching query overrides JSON');
      return [];
    }
    const moduleData = await response.json();
    const entries = moduleData && moduleData[locale] && moduleData[locale].entries;
    return entries.filter((module) => module.timestamp > pageTimestamp);
  } catch (e) {
    logger.error('Error occurred while getting modules to load client side: ', e);
    return [];
  }
};

export const queryModulesThatNeedRefresh = async (store) => {
  const modules = await getModulesThatNeedRefresh();
  for (let i = 0; i < modules.length; i += 1) {
    logger.info(`Requesting ${modules[i].name} from client side`);
    switch (modules[i].name) {
      case 'labels':
        store.dispatch(fetchLabelsData());
        break;
      case 'config':
        store.dispatch(siteConfigData());
        break;
      case 'header':
        store.dispatch(fetchHeaderData());
        break;
      case 'footer':
        store.dispatch(fetchFooterData());
        break;
      default:
        break;
    }
  }
};

const getGraphQLApiFromEnv = (apiSiteInfo, processEnv, relHostname) => {
  /**
   * graphQlEndpoint is used for the client side graphQL requests and can be override by x-gql-ak header.
   * graphQlEndpointServer iss used for the server graphQL requests only and can be override by x-gql-server header.
   *
   */
  const {
    GRAPHQL_API_ENDPOINT,
    GRAPHQL_API_ENDPOINT_SERVER,
    GRAPHQL_API_REGION,
    GRAPHQL_API_IDENTIFIER,
  } = processEnv;
  const { overrideGraphqlEndpoint, overRiddenGraphqlEndpointServer } = apiSiteInfo;
  const graphQlEndpoint = overrideGraphqlEndpoint || GRAPHQL_API_ENDPOINT || relHostname;
  const graphQlEndpointServer =
    overRiddenGraphqlEndpointServer || GRAPHQL_API_ENDPOINT_SERVER || relHostname;
  const graphQLEndpointURL = `${graphQlEndpoint}/${GRAPHQL_API_IDENTIFIER}`;
  return {
    graphql_region: GRAPHQL_API_REGION,
    graphql_endpoint_url: graphQLEndpointURL,
    graphql_endpoint_url_server: isServer()
      ? `${graphQlEndpointServer}/${GRAPHQL_API_IDENTIFIER}`
      : graphQLEndpointURL,
    graphql_persisted_query: processEnv.GQL_PQ,
    graphql_auth_type: processEnv.GRAPHQL_API_AUTH_TYPE,
    graphql_api_key: processEnv.GRAPHQL_API_KEY || '',
  };
};
/*
 * @method numericStringToBool
 * @description this method returns the bool value of string numeric passed
 * @param {string} str the  string numeric value
 */
export const numericStringToBool = (str) => !!+str;
// Parse boolean out of string true|false
export const parseBoolean = (bool) => {
  return bool === true || bool === '1' || (bool || '').toUpperCase() === 'TRUE';
};
/**
 *
 * @param {object} bossDisabledFlags carries the boss disability flags -
 * bossCategoryDisabled,
 * bossProductDisabled
 * @returns the disability boolean value
 */
export const isBossProduct = (bossDisabledFlags) => {
  const { bossCategoryDisabled, bossProductDisabled } = bossDisabledFlags;
  return !(numericStringToBool(bossCategoryDisabled) || numericStringToBool(bossProductDisabled));
};
/**
 * @function isBopsProduct
 * @param {*} isUSStore
 * @param {*} product
 * @summary This BOPIS logic is to validate if product/color variant is eligible for BOPIS
 * product is a color variant object of a product.
 */
export const isBopisProduct = (isUSStore, product) => {
  let isOnlineOnly;
  if (isUSStore) {
    isOnlineOnly =
      (product.TCPWebOnlyFlagUSStore && parseBoolean(product.TCPWebOnlyFlagUSStore)) || false;
  } else {
    isOnlineOnly =
      (product.TCPWebOnlyFlagCanadaStore && parseBoolean(product.TCPWebOnlyFlagCanadaStore)) ||
      false;
  }
  return !isOnlineOnly;
};
export const createAPIConfig = (resLocals) => {
  // TODO - Get data from env config - Brand, MellisaKey, BritverifyId, AcquisitionId, Domains, Asset Host, Unbxd Domain;
  // TODO - use isMobile and cookie as well..

  const {
    country,
    currency,
    language = 'en',
    siteId,
    brandId,
    hostname,
    host: reqHostname,
    originalPath,
    overRiddenGraphqlEndpoint,
    overRiddenGraphqlEndpointServer,
    isCSREnabledForHome,
    ssrHomeSlotRenderCount,
  } = resLocals;
  const isCASite = siteId === API_CONFIG.siteIds.ca;
  const isGYMSite = brandId === API_CONFIG.brandIds.gym;
  const countryConfig = isCASite ? API_CONFIG.CA_CONFIG_OPTIONS : API_CONFIG.US_CONFIG_OPTIONS;
  const brandConfig = isGYMSite ? API_CONFIG.GYM_CONFIG_OPTIONS : API_CONFIG.TCP_CONFIG_OPTIONS;
  const catalogId =
    API_CONFIG.CATALOGID_CONFIG[isGYMSite ? 'Gymboree' : 'TCP'][isCASite ? 'Canada' : 'USA'];
  const apiSiteInfo = API_CONFIG.sitesInfo;
  apiSiteInfo.overrideGraphqlEndpoint = overRiddenGraphqlEndpoint;
  apiSiteInfo.overRiddenGraphqlEndpointServer = overRiddenGraphqlEndpointServer;
  const processEnv = process.env;
  const relHostname = apiSiteInfo.proto + apiSiteInfo.protoSeparator + hostname;
  const basicConfig = getAPIInfoFromEnv(
    apiSiteInfo,
    processEnv,
    countryConfig && countryConfig.countryKey,
    language
  );
  const graphQLConfig = getGraphQLApiFromEnv(apiSiteInfo, processEnv, relHostname);
  return {
    ...basicConfig,
    ...graphQLConfig,
    ...countryConfig,
    ...brandConfig,
    catalogId,
    isMobile: false,
    cookie: null,
    country,
    currency,
    language,
    hostname,
    reqHostname,
    originalPath,
    isCSREnabledForHome,
    ssrHomeSlotRenderCount,
  };
};

export const routeToStoreDetails = (storeDetail, refresh = false) => {
  const {
    basicInfo: {
      id,
      storeName,
      address: { city, state, zipCode },
    },
  } = storeDetail;
  const storeParams = `${storeName.replace(/\s/g, '').toLowerCase()}-${state.toLowerCase()}-${city
    .replace(/\s/g, '')
    .toLowerCase()}-${zipCode}-${id}`;
  const url = `/store/${storeParams}`;
  let routerHandler = null;
  if (isClient())
    routerHandler = () =>
      routerPush(refresh ? window.location.href : `/store?storeStr=${storeParams}`, url);
  return {
    routerHandler,
    url,
    storeParams,
  };
};

export const setCookie = (args) => {
  const { key, value, daysAlive } = args;
  const isBrowser = isClient();

  if (isBrowser && window.satellite && window.satellite.setCookie) {
    window.satellite.setCookie(key, value, daysAlive);
  } else if (isBrowser) {
    const date = new Date();
    date.setTime(date.getTime() + daysAlive * 24 * 60 * 60 * 1000);
    document.cookie = `${key}=${value};expires=${date.toUTCString()};path=/`;
  }
};

/**
 * @summary This is to read cookie from a browser. In case of node, we pass the cookie in string.
 * @param {string} key - Cookie key/name
 * @param {string} cookieString - Cookie value in string to be used in case of Node
 * @returns {string} Resolves with the key of cookie to return the value or null in case key does not match
 */
export const readCookie = (key, cookieString) => {
  try {
    if (isClient() || cookieString) {
      const name = `${key}=`;
      const decodedCookie = decodeURIComponent(cookieString || document.cookie).split(';');
      for (let i = 0; i < decodedCookie.length; i += 1) {
        let c = decodedCookie[i];
        while (c.charAt(0) === ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
          return c.substring(name.length, c.length);
        }
      }
      return null;
    }
    return null;
  } catch (error) {
    return null;
  }
};

/**
 * @summary This is to check and do a pattern match from the cookie string if the string is present.
 * @param {string} key
 * @returns {boolean} whether cookie key is present or not
 */
export const isCookiePresent = (key) => {
  try {
    return isClient() && decodeURIComponent(document.cookie).indexOf(key) >= 0;
  } catch (error) {
    return false;
  }
};

/**
 * Returns data stored in localstorage
 * @param {string} key - Localstorage item key
 * @returns {string} - Localstorage item data
 */
export const getLocalStorage = (key) => {
  if (isClient) {
    try {
      return window.localStorage.getItem(key);
    } catch {
      return readCookie(key);
    }
  } else {
    return readCookie(key);
  }
};

/**
 * Set key/value data to localstorage
 * @param {Object} arg - Key/Value paired data to be set in localstorage
 */
export const setLocalStorage = (arg) => {
  const { key, value } = arg;

  if (isClient()) {
    try {
      return window.localStorage.setItem(key, value);
    } catch {
      return setCookie(arg);
    }
  } else {
    return setCookie(arg);
  }
};

export const viewport = () => {
  if (!window) return null;

  return {
    small: window.matchMedia(mediaQuery.smallOnly).matches,
    medium: window.matchMedia(mediaQuery.mediumOnly).matches,
    large: window.matchMedia(mediaQuery.large).matches,
  };
};

export const fetchStoreIdFromUrlPath = (url) => {
  const currentStoreUrl = url || document.location.href;
  const pathSplit = currentStoreUrl.split('-');
  return pathSplit[pathSplit.length - 1];
};

export const urlContainsQuery = (url) => {
  return /\?.+=.*/.test(url);
};

export const scrollToParticularElement = (element) => {
  const fixedHeaderOffset = getViewportInfo().isDesktop
    ? FIXED_HEADER.LG_HEADER
    : FIXED_HEADER.SM_HEADER;
  if (isClient()) {
    const elementPositionFromTop = element.getBoundingClientRect().top;
    const calculatedOffset = elementPositionFromTop - fixedHeaderOffset;
    window.scrollTo({
      top: calculatedOffset,
      behavior: 'smooth',
    });
  }
};

// BELOW METHOD SCROLL TO PARTICULAR ELEMENT EVEN WHEN WHOLE PARENT PAGE IS SCROLLED TILL BOTTOM. IT CALCULATES PORTION OF PAGE WHICH IS ALREADY SCROLLED AS WELL.
export const scrollToElement = (element) => {
  const rect = element.getBoundingClientRect();
  const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  window.scrollTo({
    top: rect.top + scrollTop - FIXED_HEADER.LG_CONDENSED_HEADER,
    behavior: 'smooth',
  });
};

const getExtraPadding = (isBagpage) => (isBagpage ? 20 : 0);
const getMarginBreadcrumb = (isScrollEnabled) => (!isScrollEnabled ? MARGIN_BREADCRUMB : 0);

export const scrollToBreadCrumb = (isBagPage, isScrollEnabled) => {
  if (isClient()) {
    try {
      const headerElement = document.querySelector('header');
      const headerHeight =
        typeof window !== 'undefined' && headerElement && headerElement.offsetHeight;

      const headerStyles = headerElement && window.getComputedStyle(headerElement);
      const totalMargin =
        parseInt(headerStyles.marginBottom, 10) + parseInt(headerStyles.marginTop, 10);

      const topMostHeaderElem = headerElement && headerElement.children[0];
      const bottomMostHeaderElem =
        headerElement &&
        headerElement.children &&
        headerElement.children.length > 1 &&
        headerElement.children[headerElement.children.length - 1];

      const marginTopFirstElem =
        topMostHeaderElem && parseInt(window.getComputedStyle(topMostHeaderElem).marginTop, 10);
      const marginBottomLastElem =
        bottomMostHeaderElem &&
        parseInt(window.getComputedStyle(bottomMostHeaderElem).marginBottom, 10);

      const appBannerHeader =
        typeof window !== 'undefined' && window.document.querySelector('.app-banner-wrapper');
      const appBannerHeight = (appBannerHeader && appBannerHeader.offsetHeight) || 0;
      const extraPadding = getExtraPadding(isBagPage);
      const marginBreadcrumb = getMarginBreadcrumb(isScrollEnabled);

      const totalHeaderHeight =
        headerHeight +
        totalMargin +
        marginTopFirstElem +
        marginBottomLastElem -
        marginBreadcrumb +
        appBannerHeight -
        extraPadding;
      if (headerHeight > 0) {
        window.scrollTo({
          top: totalHeaderHeight,
        });
      }
    } catch (e) {
      // do nothing
    }
  }
};

/**
 * openWindow - opens a window with the URL and attributes passed in
 * @param {string} arg url - URL to open,
 * @param {string} arg target - where to open the new window; defaults to _blank
 * @param {string} arg attrs - what attributes to pass to window.open; defaults to empty string
 * @return {Object} Object handle for the new window
 */
export const openWindow = (url, target = '_blank', attrs = '') => {
  let windowAttributes = attrs;
  if (target === '_blank') {
    windowAttributes = windowAttributes.concat('noopener');
  }
  logger.info(`Opening ${url} in window with target=${target} and attributes=${windowAttributes}`);
  return window.open(url, target, windowAttributes);
};

export const getDirections = (address) => {
  const { addressLine1, city, state, zipCode } = address;
  return openWindow(
    `${googleMapConstants.OPEN_STORE_DIR_WEB}${addressLine1},%20${city},%20${state},%20${zipCode}`,
    '_blank'
  );
};

/**
 * To Identify whether the device is ios for web.
 */

export const isiOSWeb = () => {
  const userAgent =
    typeof navigator !== 'undefined' && (navigator.userAgent || navigator.vendor || window.opera);
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return true;
  }
  return false;
};

/**
 * To Identify whether the device is ios for Mobile and Desktop.
 */

export const isAppleDevice = () => {
  const userAgent =
    typeof navigator !== 'undefined' && (navigator.userAgent || navigator.vendor || window.opera);
  if (/iPad|iPhone|iPod|Mac/.test(userAgent) && !window.MSStream) {
    return true;
  }
  return false;
};

/**
 * To Identify whether the device is Android for web.
 */

export const isAndroidWeb = () => {
  const userAgent =
    typeof navigator !== 'undefined' && (navigator.userAgent || navigator.vendor || window.opera);
  if (/Android/.test(userAgent)) {
    return true;
  }
  return false;
};

/**
 * To Identify whether the device is Android for web.
 */
export const isMobileWeb = () => {
  return (isClient() && isiOSWeb()) || (isClient() && isAndroidWeb());
};

/**
 * This function will remove all the body scroll locks.
 */
export const removeBodyScrollLocks = () => {
  if (isiOSWeb() && isClient()) {
    clearAllBodyScrollLocks();
  }
  if (isAndroidWeb() && isClient()) {
    const [body] = document.getElementsByTagName('body');
    body.classList.remove('disableBodyScroll');
  }
};
/**
 * Enable Body Scroll, Moving it to common utils and putting a check of Mobile app at one place instead of containers.
 */
export const enableBodyScroll = (targetElem) => {
  if (isClient()) {
    if (isiOSWeb() && targetElem) {
      enableBodyScrollLib(targetElem);
      return;
    }
    const [body] = document.getElementsByTagName('body');
    body.classList.remove('disableBodyScroll');
  }
};

/**
 * Disable Body Scroll
 */
export const disableBodyScroll = (targetElem) => {
  if (isClient()) {
    if (isiOSWeb() && targetElem) {
      disableBodyScrollLib(targetElem);
      return;
    }
    const [body] = document.getElementsByTagName('body');
    body.classList.add('disableBodyScroll');
  }
};

export const constructToPath = (url) => {
  let toPath = url;
  if (url) {
    if (url.indexOf('/outfit/') !== -1) {
      const outfitParams = url && url.split('/');
      toPath =
        outfitParams &&
        outfitParams.length > 1 &&
        `/outfit?outfitId=${outfitParams[outfitParams.length - 2]}&vendorColorProductIdsList=${
          outfitParams[outfitParams.length - 1]
        }`;
    } else if (url.indexOf('/c/') !== -1) {
      toPath = url.replace('/c/', '/c?cid=');
    } else if (url.indexOf('/p/') !== -1) {
      toPath = url.replace('/p/', '/p?pid=');
    }
  }
  return toPath;
};

/* Parse query parameters to an object. For instance, string returned from location.search */
export const getQueryParamsFromUrl = (url) => {
  let queryString = url || '';
  let keyValPairs = [];
  const params = {};
  queryString = queryString.replace(/.*?\?/, '');

  if (queryString.length) {
    keyValPairs = queryString.split('&');
    const resultingArray = Object.values(keyValPairs);

    resultingArray.filter((item, index) => {
      const key = item.split('=')[0];
      if (typeof params[key] === 'undefined') params[key] = [];
      params[key].push(resultingArray[index].split('=')[1]);
      return params;
    });
  }
  return params;
};

/* Returns an array of icid by querying to dom anchor element which has icid on it. */
export const internalCampaignProductAnalyticsList = () => {
  const aTags = document.getElementsByTagName('a') || [];
  const internalCampaignId = 'icid';
  return Array.prototype.slice
    .call(aTags)
    .filter((tag) => {
      return tag.href.indexOf(internalCampaignId) !== -1;
    })
    .map((tag) => {
      const queryParamsFromUrl = getQueryParamsFromUrl(tag.href);
      return (
        queryParamsFromUrl &&
        queryParamsFromUrl[internalCampaignId] &&
        queryParamsFromUrl[internalCampaignId].join(',')
      );
    });
};

/**
 * Returns data object for PLP and Category List page view. Should be called in client is available.
 * @param {Array} breadCrumbTrail An array of breadCrumbs which reside in ProductListing State
 */
export const getProductListingPageTrackData = (
  breadCrumbTrail,
  extras = {},
  searchType = '',
  initialValues = {},
  icid,
  storeId,
  viaPromoFilter
) => {
  const { isDesktop } = getViewportInfo();
  const breadCrumbNames = breadCrumbTrail.map((breadCrumb) => breadCrumb.displayName.toLowerCase());
  let listingSubCategory = breadCrumbNames[2];
  if (breadCrumbNames.length < 3) {
    listingSubCategory = null;
  }
  const pageName = `browse:${breadCrumbNames.join(':')}`;
  let pageNavigationText = isDesktop ? 'topmenu-' : 'hamburger-';
  pageNavigationText = `${pageNavigationText}${breadCrumbNames.join('-')}`;

  const { filterCount, products, shopall, totalProductsCount } = extras;
  const { sort } = initialValues;
  let customEvents = ['event91', 'event92', 'event82', 'event80'];
  let pageSearchText = '';
  let pageShortName = '';
  let internalCampaignId = '';
  let promoFilterCategoryTrail = '';
  const supressProps = ['eVar94'];
  if (shopall) {
    pageNavigationText = `${pageNavigationText}-${shopall}`;
  }

  if (searchType === CONSTANTS.ANALYTICS_TYPEAHEAD_CATEGORY || filterCount) {
    setProp('prop23', `${breadCrumbNames.join(':')}`);
    pageNavigationText = '';
    customEvents = ['event91', 'event92', 'event80'];
    pageSearchText = breadCrumbNames.join('>');
  }
  if (searchType === CONSTANTS.NAVIGATION_CATEGORY) {
    setProp('prop23', `${breadCrumbNames.join(':')}`);
    pageSearchText = '';
  }

  if (sort) {
    pageNavigationText = '';
    customEvents = ['event91', 'event92', 'event80'];
  }
  if (searchType !== CONSTANTS.NAVIGATION_CATEGORY) {
    pageShortName = 'search:results';
  }

  if (filterCount) {
    setProp('eVar22', 'browser');
  }

  if (icid) {
    internalCampaignId = icid;
  }

  if (sort || filterCount) {
    supressProps.push('eVar97');
  }

  if (viaPromoFilter) {
    customEvents.push('event180');
    promoFilterCategoryTrail = breadCrumbNames.join('>');
  }
  return {
    pageName,
    pageType: 'browse',
    pageSection: `browse:${breadCrumbNames[0]}`,
    pageSubSection: pageName,
    internalCampaignIdList: internalCampaignProductAnalyticsList(),
    customEvents,
    pageNavigationText,
    departmentList: breadCrumbNames[0],
    categoryList: breadCrumbNames[1],
    listingSubCategory,
    pageSearchText,
    listingCount: products ? totalProductsCount : '',
    pageSearchType: searchType,
    pageShortName,
    internalCampaignId,
    supressProps,
    storeId,
    promoFilterCategoryTrail,
    ...extras,
  };
};

export const logCustomEvent = () => {
  // do nothing
};

export const getIfBotDevice = (req) => {
  return req && req.device && req.device.type === 'bot';
};

/* Parse query parameters to an object. For instance, string returned from location.search */
export const getIcidValue = (path) => {
  const internalcId = 'icid';
  const queryParams = getQueryParamsFromUrl(path);
  const icidObj = queryParams[internalcId];
  return icidObj && icidObj.length ? icidObj[0] : null;
};

/**
 * Returns data set in window else from state
 * @param {string} key - state
 * @returns {string} - abtests object
 */
export const getABtestFromState = (AbTests) => {
  let extractAbtests = null;
  if (isClient()) {
    const isEmptyABtest = isEmpty(window.ABTest);
    extractAbtests = isEmptyABtest ? AbTests : window.ABTest;
  }
  return extractAbtests || {};
};

/**
 * Returns categoryName for analytics tracking
 * @param {object}  - breadCrumbs
 * @returns {string} - categoryName
 */
export const getCategoryName = (breadCrumbs) => {
  let categoryName = '';
  if (breadCrumbs.length > 1) {
    categoryName = breadCrumbs[1].displayName;
  } else if (breadCrumbs.length === 1) {
    categoryName = breadCrumbs[0].displayName;
  }
  return categoryName ? categoryName.toLowerCase() : '';
};

export const isIOS = () => false;

export const isAndroid = () => false;

/**
 * @method getAkamaiSensorData
 * @desc Returning blank for web
 */
export const getAkamaiSensorData = () => {
  return '';
};

export const setValueInAsyncStorage = () => {};

export const isCouponsUpdateRequired = () => {};

export const clearRecommendationsObj = () => {};
export const getRecommendationsObj = () => {};

const parseAndReturnNumber = (position, variation) => {
  const defaultReturnValue = 0;
  if (!isEmpty(position)) {
    const parsedReturnValue = parseInt(position[variation], 10);
    if (isFinite(parsedReturnValue)) return parsedReturnValue;
    return defaultReturnValue;
  }
  return defaultReturnValue;
};

export const getByomMediaBorderRadius = (TLeft, TRight, BLeft, BRight) => {
  return {
    borderTopLeftRadius: parseAndReturnNumber(TLeft, 'media-top-left'),
    borderTopRightRadius: parseAndReturnNumber(TRight, 'media-top-right'),
    borderBottomLeftRadius: parseAndReturnNumber(BLeft, 'media-bottom-left'),
    borderBottomRightRadius: parseAndReturnNumber(BRight, 'media-bottom-right'),
  };
};

export const getByomHeadereBorderRadius = (TLeft, TRight, BLeft, BRight) => {
  return {
    borderTopLeftRadius: parseAndReturnNumber(TLeft, 'header-top-left'),
    borderTopRightRadius: parseAndReturnNumber(TRight, 'header-top-right'),
    borderBottomLeftRadius: parseAndReturnNumber(BLeft, 'header-bottom-left'),
    borderBottomRightRadius: parseAndReturnNumber(BRight, 'header-bottom-right'),
  };
};

export const getMediaBorderRadius = (top, bottom) => {
  return {
    borderTopLeftRadius: parseAndReturnNumber(top, 'media-top'),
    borderTopRightRadius: parseAndReturnNumber(top, 'media-top'),
    borderBottomLeftRadius: parseAndReturnNumber(bottom, 'media-bottom'),
    borderBottomRightRadius: parseAndReturnNumber(bottom, 'media-bottom'),
  };
};

export const getHeaderBorderRadius = (top, bottom) => {
  return {
    borderTopLeftRadius: parseAndReturnNumber(top, 'header-top'),
    borderTopRightRadius: parseAndReturnNumber(top, 'header-top'),
    borderBottomLeftRadius: parseAndReturnNumber(bottom, 'header-bottom'),
    borderBottomRightRadius: parseAndReturnNumber(bottom, 'header-bottom'),
  };
};

export const getCtaBorderRadius = (top, bottom) => {
  return {
    borderTopLeftRadius: parseAndReturnNumber(top, 'cta-top'),
    borderTopRightRadius: parseAndReturnNumber(top, 'cta-top'),
    borderBottomLeftRadius: parseAndReturnNumber(bottom, 'cta-bottom'),
    borderBottomRightRadius: parseAndReturnNumber(bottom, 'cta-bottom'),
  };
};

export const createBVUrl = () => {
  const langMap = {
    en: 'en',
    es: 'es-US',
    fr: 'fr-CA',
  };
  const { language, BAZAARVOICE_REVIEWS, country } = getAPIConfig();
  let locale = '';
  if (langMap[language] === 'en' && country === 'US') {
    locale = 'en_US';
  } else if (langMap[language] === 'en' && country === 'CA') {
    locale = 'en_CA';
  } else {
    locale = langMap[language] && langMap[language].replace('-', '_');
  }
  return `${BAZAARVOICE_REVIEWS}/${locale}/bv.js`;
};

/**
 * Return URI for deep link
 *
 * @param {*} uriSchemeIdentifier
 */
export const getURIForDeepLink = (uriSchemeIdentifier) =>
  `${uriSchemeIdentifier}/?deepLinkURL=${window.location.href}`;

const getFAutoValue = (isVideoUrl) => {
  return isVideoUrl ? '' : ',f_auto';
};

const createRelativeUrlPath = (
  basePath,
  namedTransformation,
  dprValue,
  url,
  isVideoUrl = false
) => {
  return `${basePath}/${
    namedTransformation ? `${namedTransformation},` : ''
  }dpr_${dprValue}${getFAutoValue(isVideoUrl)}/${url}`;
};

/**
 * @function cropImageUrl function appends or replaces the cropping value in the URL
 * @param {string} url the image url
 * @param {string} crop the crop parameter
 * @param {string} namedTransformation the namedTransformation parameter
 * @return {string} function returns new Url with the crop value
 */
export const cropImageUrl = (url, crop, namedTransformation, isVideoUrl = false) => {
  const apiConfigObj = getAPIConfig();
  const brandName = getBrand();
  const brandId = brandName && brandName.toUpperCase();
  const assetHost = apiConfigObj[`assetHost${brandId}`];
  const basePath = assetHost || '';
  let URL = url;

  let dprValue = isClient() && window && Math.floor(window.devicePixelRatio);
  const fAutoValue = getFAutoValue(isVideoUrl);
  dprValue = dprValue ? `${dprValue}.0`.slice(0, 3) : '2.0';

  // Image path transformation in case of absolute image URL
  if (/^http/.test(url)) {
    const [urlPath = '', urlData = ''] = url && url.split('/upload');
    const imgPath = urlPath && urlPath.replace(/^\//, '');
    if (urlPath && crop) {
      URL = `${imgPath}/upload/${crop}${fAutoValue}/${urlData.replace(/^\//, '')}`;
    }
    if (namedTransformation) {
      URL = `${imgPath}/upload/${namedTransformation},dpr_${dprValue}${fAutoValue}/${urlData.replace(
        /^\//,
        ''
      )}`;
    }
  } else {
    // Image path transformation in case of relative image URL
    URL = createRelativeUrlPath(basePath, namedTransformation, dprValue, url, isVideoUrl);
  }
  return URL;
};

export const isSafariMobileBrowser = () => {
  return (
    isClient() &&
    isiOSWeb() &&
    isMobileWeb() &&
    navigator &&
    navigator.userAgent &&
    navigator.vendor &&
    navigator.vendor.indexOf('Apple') > -1 &&
    navigator.userAgent.indexOf('CriOS') === -1 &&
    navigator.userAgent.indexOf('FxiOS') === -1
  );
};

// eslint-disable-next-line sonarjs/cognitive-complexity
export const getPageNameUsingURL = (URL = '') => {
  if (URL.indexOf('/home/') !== -1) return 'homepage';
  if (URL.indexOf('/store-locator') !== -1) return 'store-locator';
  if (URL.indexOf('/store/') !== -1) return 'store';
  if (URL.indexOf('/login') !== -1) return 'login';
  if (URL.indexOf('/sitemap') !== -1) return 'sitemap';
  if (URL.indexOf('/instagram') !== -1) return 'instagram';
  if (URL.indexOf('/twitter') !== -1) return 'twitter';
  if (URL.indexOf('/account/') !== -1) return 'account';
  if (URL.indexOf('/favorites') !== -1) return 'favorites';
  if (URL.indexOf('/c/') !== -1) return 'plp';
  if (URL.indexOf('/p/') !== -1) return 'pdp';
  if (URL.indexOf('/b/') !== -1) return 'bundle-product';
  if (URL.indexOf('/search/') !== -1) return 'search';
  if (URL.indexOf('/outfit/') !== -1) return 'outfit';
  if (URL.indexOf('/place-card') !== -1) return 'place-card';
  if (URL.indexOf('/bag') !== -1) return 'cart';
  if (URL.indexOf('/checkout') !== -1) return 'checkout';
  return 'homepage';
};

const nonInteractionMap = ['addtocart', 'purchases'];

const addProductDetailsToStorage = (payload) => {
  if (!payload) return;

  const prevData = JSON.parse(getLocalStorage(RECOMMENDATION_GA_KEY));
  if (prevData && prevData[payload.dimension20]) return;
  setLocalStorage({ key: RECOMMENDATION_GA_KEY, value: JSON.stringify(payload) });

  // also adding in session storage because GTM uses from here
  const currData = JSON.parse(getSessionStorage(RECOMMENDATION_GA_KEY));
  if (currData && currData[payload.dimension20]) return;
  setSessionStorage({ key: RECOMMENDATION_GA_KEY, value: JSON.stringify(payload) });
};

export const getStoredProductDetails = () => {
  return JSON.parse(getLocalStorage(RECOMMENDATION_GA_KEY) || '{}');
};

export const removeStoredProductDetails = () => {
  if (isClient()) {
    try {
      window.sessionStorage.removeItem(RECOMMENDATION_GA_KEY);
      window.localStorage.removeItem(RECOMMENDATION_GA_KEY);
    } catch (error) {
      console.error('Unable to remove storage data', error);
    }
  }
};

export const appendProductToStorage = (cd20) => {
  if (cd20) {
    const savedProduct = getStoredProductDetails();

    const currentProduct = {
      [cd20]: {
        d92: window.d92,
      },
      ...savedProduct,
    };
    addProductDetailsToStorage(currentProduct);
  }
};

const deleteMetric50 = (productsObj) => {
  const productData = productsObj;

  if (productsObj.length) {
    productData.forEach((el) => {
      const product = el;
      delete product.metric50;
    });
  }
  return productData;
};

const updateRecValues = (item, savedProduct, eventName) => {
  const product = item;

  if (savedProduct.d92 || savedProduct.dimension92)
    product.dimension92 = savedProduct.d92 || savedProduct.dimension92;
  else delete product.dimension92;

  if (EVENTS_INCLUDE_METRIC50.includes(eventName)) product.metric50 = '1';
  else delete product.metric50;
};

const addRequiredDimensions = (eName, productsObj) => {
  const productData = productsObj;
  const eventName = eName.toLowerCase();

  if (productData.length === 0) return productData;

  productData.forEach((el) => {
    const product = el;

    const savedProduct = getStoredProductDetails();
    if (savedProduct && savedProduct[el.dimension89]) {
      const temp = savedProduct[el.dimension89];
      updateRecValues(product, temp, eventName);
    }
  });

  return productData;
};

// eslint-disable-next-line complexity
const constructEEObject = (args) => {
  const {
    eventName = '',
    eventType = '',
    pageName = '',
    currCode = '',
    eventLabel = '',
    step = 1,
    actionField = {},
    cm2 = '0',
    cm9 = '0',
    cm11 = '0',
    cm12 = '0',
  } = args;

  let { eventAction, productsObj = [] } = args;

  if (typeof eventAction === 'undefined') eventAction = eventName;

  productsObj = deleteMetric50(productsObj);
  productsObj = addRequiredDimensions(eventName, productsObj);

  const event = {
    event: eventName,
    eventCategory: 'Ecommerce',
    eventAction,
    eventLabel: eventName === 'checkout' ? eventLabel : '',
    eventNonInteraction: nonInteractionMap.includes(eventName) ? 0 : 1,
    ecommerce: {
      [eventType]: {
        actionField: {
          list: pageName,
        },
        products: productsObj,
      },
    },
  };

  if (eventType === 'impressions') event.ecommerce[eventType] = [...productsObj];
  if (eventName.toLowerCase() === EVENT_ADD_TO_CART) event.ecommerce.CM2 = cm2;

  if (eventName !== 'checkout') event.ecommerce.currencyCode = currCode;
  else if (eventType !== 'impressions' && eventType === 'checkout') {
    if (step === 1) {
      event.ecommerce.cm9 = cm9;
      event.ecommerce.cm11 = '0';
      event.ecommerce.cm12 = '0';
    } else if (step === 2) {
      event.ecommerce.cm9 = '0';
      event.ecommerce.cm11 = cm11;
      event.ecommerce.cm12 = '0';
    } else if (step === 3) {
      event.ecommerce.cm9 = '0';
      event.ecommerce.cm11 = '0';
      event.ecommerce.cm12 = cm12;
    }
    event.ecommerce[eventType].actionField = {
      step,
      option: eventLabel,
    };
  }
  if (eventType === 'purchase') event.ecommerce[eventType].actionField = actionField;

  return event;
};

export const getExperienceString = (args) => {
  const argsArr = Object.keys(args);
  if (argsArr.length) {
    const monetateObj = JSON.parse(getSessionStorage('monObj')) || {};
    monetateObj[args.experience_name] = args.variant_label;
    setSessionStorage({ key: 'monObj', value: JSON.stringify(monetateObj) });
  }
};

export const fireEnhancedEcomm = (args) => {
  try {
    if (!window.dataLayer || !args) return;

    let eventName = '';
    if (!Array.isArray(args) && args.eventName) eventName = args.eventName.toLowerCase();
    else if (Array.isArray(args) && args[0] && args[0].eventName)
      eventName = args[0].eventName.toLowerCase();

    if (eventName === EVENT_PRODUCT_DETAIL && args.productsObj && args.productsObj.length) {
      window.d92 = args.productsObj[0].dimension92;
    }

    if (eventName === EVENT_ADD_TO_CART) {
      args.productsObj.forEach((el) => {
        const { dimension20, dimension89 } = el;
        appendProductToStorage(dimension20 || dimension89);
      });
    }

    const { isCustomEvent, event } = args;
    let obj = {};
    if (isCustomEvent) {
      obj = event;
    } else if (Array.isArray(args)) {
      args.forEach((arg) => {
        obj = constructEEObject(arg);
      });
    } else {
      obj = constructEEObject(args);
    }
    window.dataLayer.push(obj);

    if (eventName === 'purchases') {
      removeStoredProductDetails();
    }
  } catch (err) {
    trackError({
      error: err,
      errorTags: ['GA-Analytics', `${args.eventType}`, 'EE_Script'],
      extraData: {
        eventType: `${args.eventType}`,
        payload: args,
      },
    });
  }
};

export const addRecommendationsObj = (prodId) => {
  if (!prodId) return {};
  const recommObj = JSON.parse(getLocalStorage(RECOMMENDATION_GA_KEY));
  if (recommObj && recommObj[prodId]) return recommObj[prodId];
  return {};
};

export const getEddSessionState = (isEddABTest) => {
  const isEddSessionEnabled = parseBoolean(getSessionStorage('EDD_ENABLED'));
  if (!isEddSessionEnabled) {
    setSessionStorage({ key: 'EDD_ENABLED', value: isEddABTest });
  }
  return parseBoolean(isEddABTest || isEddSessionEnabled);
};

export const getPageGenerationDate = () =>
  // eslint-disable-next-line no-undef
  __NEXT_DATA__.props.initialProps.pageProps.PAGE_GENERATION_DATE;

export const getStyleArray = (textItems) => {
  if (Array.isArray(textItems)) {
    const styleArr = [];
    textItems.map((textItem) => {
      const modStyle = textItem && textItem.style;
      return modStyle && modStyle.includes('price_point_style_')
        ? styleArr.push(modStyle)
        : styleArr;
    });
    return styleArr;
  }
  return [];
};

export const getOsDetails = () => {
  if (typeof navigator !== 'undefined') {
    const { userAgent = '' } = navigator;
    if (/Android/.test(userAgent)) {
      const os = userAgent.match(/Android\s\d+/)[0];
      return { os: 'Android', version: os.replace(/\D/, '') };
    }
    if (/(iPhone|iPad)/.test(userAgent)) {
      const os = userAgent.match(/OS\s[\d_]+/)[0];
      return { os: 'iOS', version: os.replace('OS', '').replace('_', '.') };
    }
  }
  return null;
};

export const getDeviceClass = () => {
  const { effectiveType = '4g' } =
    typeof navigator !== 'undefined' && 'connection' in navigator ? navigator.connection : {};
  const { deviceMemory = 4 } = typeof navigator !== 'undefined' ? navigator : {};
  const osDetails = getOsDetails();
  if (osDetails) {
    const { os, version } = osDetails;
    if (os === 'Android' && ['9', '10', '11'].indexOf(version) > -1 && deviceMemory < 4) {
      return deviceTiers.LOW;
    }
  }
  if (deviceMemory >= 4 && effectiveType === '4g') {
    return deviceTiers.HIGH;
  }
  return deviceTiers.LOW;
};

export const getDimension2Value = (cd92) => {
  let d92 = cd92;
  if (d92) {
    d92 = d92.replace('monetate-', 'm-');
  }

  if (d92 && d92.includes(DIMENSION92_TEXT))
    return Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS;

  let cd2 = '';
  const path = isClient() ? window.location.href : null;
  if (path) {
    if (path.includes('/outfit/')) cd2 = 'outfit';
    else if (path.includes('content')) cd2 = 'content page';
    else if (path.includes('/b/')) cd2 = 'bundle';
    else if (path.includes('/p/') && !d92) cd2 = 'product';
  }
  if (!cd2 && d92) cd2 = Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS;
  return cd2;
};

export const removeRecFromStorage = (generalProductId) => {
  if (generalProductId) {
    const savedProduct = getStoredProductDetails();
    if (savedProduct && Object.keys(savedProduct).length > 0) {
      delete savedProduct[generalProductId];
      setLocalStorage({ key: RECOMMENDATION_GA_KEY, value: JSON.stringify(savedProduct) });
    }
  }
};

export const getSiteIdInCaps = () => {
  const siteId = getSiteId();
  return (siteId && siteId.toUpperCase()) || '';
};

export const getPlaceSiteId = () => {
  const siteIdInCaps = getSiteIdInCaps();
  return `PLACE_${siteIdInCaps}`;
};

export const getAfterPaySize = () => {
  let textSize = 'sm';
  if (isClient() && getViewportInfo().isMobile) {
    textSize = 'xs';
  }
  return textSize;
};

export const getSearchRedirectObj = () => {
  const searchRedirect = getSessionStorage(SEARCH_REDIRECT_KEY);
  if (searchRedirect) {
    const parsedData = JSON.parse(searchRedirect);
    if (parsedData) return parsedData;
  }
  return {
    isDone: false,
  };
};

export const getProductCategory = (breadCrumbs) => {
  const apparelText = 'apparel';
  if (!breadCrumbs || !breadCrumbs.length) return apparelText;

  let category = '';
  const formattedCategory = breadCrumbs.map(
    (listing) => listing.displayName && listing.displayName.toLowerCase()
  );

  if (!formattedCategory || !formattedCategory.length) return apparelText;

  if (formattedCategory.includes('accessories')) {
    category = 'accessories';
  } else if (formattedCategory.includes('shoes')) {
    category = 'shoes';
  } else {
    category = apparelText;
  }
  return category;
};

export const getGAPageName = (pathNameUrl) => {
  const lastPageUrl = !pathNameUrl ? getSessionStorage('LAST_PAGE_PATH') : pathNameUrl;

  if (lastPageUrl) {
    if (lastPageUrl.includes('/home')) return 'Home Page';
    if (lastPageUrl.includes('/search/')) return 'Search Detail Page';
    if (lastPageUrl.includes('/bag')) return 'Bag Page';
    if (lastPageUrl.includes('/outfit/')) return 'Outfit PDP';
    if (lastPageUrl.includes('/confirmation')) return 'Confirmation Page';
    if (lastPageUrl.includes('/p/')) return 'PDP';
  }
  return 'Product Listing Page';
};

export const WebViewRedirectURL = () => {
  if (isClient() && window.history && window.history.back) {
    window.history.back();
  } else {
    routerPush('/', '/home');
  }
};

export const smoothScrolling = (id, top = 0) => {
  smoothScroll.polyfill();
  const element = document.getElementById(id);
  if (element) {
    let offset = document?.getElementById('condensedHeader')?.getBoundingClientRect().height + 20;
    if (Number.isNaN(offset)) {
      offset = 90;
    }
    const elementPosition =
      element.getBoundingClientRect().top - document.body.getBoundingClientRect().top;
    const offsetPosition = elementPosition - offset;

    window.scrollTo({
      top: offsetPosition - top,
      behavior: 'smooth',
    });
  }
};

export const crossBrandUrl = (url) => {
  return (
    (isTCP() && url.includes('gymboree.com')) ||
    (isTCP() && url.includes('sugarandjade.com')) ||
    (isGymboree() && url.includes('childrensplace.com')) ||
    (isGymboree() && url.includes('sugarandjade.com'))
  );
};

export const crossBrandRedirect = (url, linkName, analyticsData) => {
  const isCrossBrandUrl = crossBrandUrl(url);
  if (isCrossBrandUrl) {
    const analyticsObj = JSON.parse(getSessionStorage('AnalyticsObj')) || {};
    const navigationData = `${analyticsData.toLowerCase()}-${linkName.toLowerCase()}`;
    const e97Obj = (analyticsObj && analyticsObj.e97) || {
      e97FirstTouch: navigationData,
      flag: true,
    };
    e97Obj.e97CrossNav = navigationData;
    analyticsObj.e97 = e97Obj;
    const anaEncoded = encodeURI(JSON.stringify(analyticsObj));
    return `${url}&analyticsObj=${anaEncoded}` || url;
  }
  return url;
};

export const cleanUpNonStdQueryString = (url) => {
  try {
    if (url && typeof url === 'string') {
      const first = url.split('&');
      return first[0];
    }
    return url;
  } catch (e) {
    return url;
  }
};

export const navigateToOrderDetailPage = (
  isLoggedIn,
  orderPage,
  orderNumber,
  encryptedEmailAddress
) => {
  if (isLoggedIn) {
    routerPush(
      `${orderPage.link}&orderId=${orderNumber}&fromOrderHelp=true`,
      `${orderPage.path}/${orderNumber}`
    );
  } else {
    routerPush(
      `${internalEndpoints.trackOrder.link}&orderId=${orderNumber}&email=${encryptedEmailAddress}`,
      `${internalEndpoints.trackOrder.path}/${orderNumber}/${encryptedEmailAddress}`
    );
  }
};
export const isBagRoute = () => Router.pathname.toLowerCase() === '/bag';

export default {
  importGraphQLClientDynamically,
  importGraphQLQueriesDynamically,
  isProduction,
  isDevelopment,
  getObjectValue,
  createUrlSearchParams,
  buildUrl,
  getCreditCardExpirationOptionMap,
  getSiteId,
  routerPush,
  bindAllClassMethodsToThis,
  scrollPage,
  scrollTopElement,
  urlContainsQuery,
  getCountriesMap,
  getCurrenciesMap,
  languageRedirect,
  handleGenericKeyDown,
  getLocalStorage,
  setLocalStorage,
  viewport,
  fetchStoreIdFromUrlPath,
  canUseDOM,
  scrollToParticularElement,
  scrollToElement,
  getDirections,
  isMobileWeb,
  removeBodyScrollLocks,
  enableBodyScroll,
  disableBodyScroll,
  isAndroidWeb,
  internalCampaignProductAnalyticsList,
  getQueryParamsFromUrl,
  getProductListingPageTrackData,
  logCustomEvent,
  setCookie,
  readCookie,
  getIfBotDevice,
  getIcidValue,
  getABtestFromState,
  getCategoryName,
  queryModulesThatNeedRefresh,
  isIOS,
  getAkamaiSensorData,
  createBVUrl,
  getURIForDeepLink,
  cropImageUrl,
  isSafariMobileBrowser,
  getPageNameUsingURL,
  fireEnhancedEcomm,
  addRecommendationsObj,
  getExperienceString,
  getPageGenerationDate,
  deviceTiers,
  getStoredProductDetails,
  removeStoredProductDetails,
  getDimension2Value,
  removeRecFromStorage,
  getAfterPaySize,
  getSearchRedirectObj,
  getProductCategory,
  getGAPageName,
  WebViewRedirectURL,
  smoothScrolling,
  crossBrandUrl,
  crossBrandRedirect,
  cleanUpNonStdQueryString,
  navigateToOrderDetailPage,
  isBagRoute,
};
