// 9fbef606107a605d69c0edbcd8029e5d 
import { isClient } from './index';
/**
 * Set key/value data to localstorage
 * @param {Object} arg - Key/Value paired data to be set in localstorage
 */
export const setLocalStorage = arg => {
  const { key, value } = arg;
  if (isClient()) {
    try {
      return window.localStorage.setItem(key, value);
    } catch {
      return null;
    }
  }
  // return setCookie(arg);
  return null;
};

/**
 * Returns data stored in localstorage
 * @param {string} key - Localstorage item key
 * @returns {string} - Localstorage item data
 */
export const getLocalStorage = key => {
  if (isClient()) {
    try {
      return window.localStorage.getItem(key);
    } catch {
      return null;
    }
  }
  // return readCookie(key);
  return null;
};
