// 9fbef606107a605d69c0edbcd8029e5d 
const prefixStyleItem = prefixStyleName => item => {
  const newItem = item;
  const { style } = item;
  if (style.indexOf(prefixStyleName) === -1) {
    newItem.style = `${prefixStyleName}_${style}`;
  }
};

const prefixTextItemStyleName = prefixStyleName => item => {
  const { textItems } = item;
  textItems.forEach(prefixStyleItem(prefixStyleName));
};

const updateHeaderAndPromoComponentStyle = prefixStyleName => item => {
  const { headerText, promoBanner } = item;
  if (headerText) {
    headerText.forEach(prefixTextItemStyleName(prefixStyleName));
  }
  if (promoBanner) {
    promoBanner.forEach(prefixTextItemStyleName(prefixStyleName));
  }
};

/**
 * This function will update the headerText and promoBanner
 * style name If moduleClassName has spacial string to update the class name.
 * @param {Object} module module data object
 */
export const updateHeaderPromoStyleOverride = module => {
  const { moduleClassName = '', largeCompImageCarousel, divCTALinks, largeCompImage } =
    module || {};

  const prefixStyleMatch = moduleClassName.match(/text_style_prefix_([\w]+)/);
  if (prefixStyleMatch) {
    const prefixStyleName = prefixStyleMatch[1];
    updateHeaderAndPromoComponentStyle(prefixStyleName)(module);

    if (largeCompImageCarousel) {
      largeCompImageCarousel.forEach(updateHeaderAndPromoComponentStyle(prefixStyleName));
    }

    if (largeCompImage) {
      largeCompImage.forEach(updateHeaderAndPromoComponentStyle(prefixStyleName));
    }

    if (divCTALinks) {
      divCTALinks.forEach(item => {
        const { styled } = item;
        prefixStyleItem(prefixStyleName)(styled);
      });
    }
  }

  return module;
};

export default {
  updateHeaderPromoStyleOverride,
};
