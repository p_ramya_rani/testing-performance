// 9fbef606107a605d69c0edbcd8029e5d 
import LogHandler from '../logger/clientLogger';

describe('LogHandler', () => {
  test('default', () => {
    const loggerClass = new LogHandler();
    const { loggerInstance: logger } = loggerClass.initializeLogger();
    logger.error('mock log');
    logger.warn('mock log');
    logger.info({ test: 'mock' });
    logger.debug('mock log');
    logger.log('mock log');
    logger.trace('mock log');
    logger.fatal('mock log');
    expect(typeof logger).toBe('object');
  });
});
