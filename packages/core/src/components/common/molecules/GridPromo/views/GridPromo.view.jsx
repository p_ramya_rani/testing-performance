// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import ModuleX from '@tcp/core/src/components/common/molecules/ModuleX';
import PropTypes from 'prop-types';
import { Button, BodyCopy, Anchor } from '../../../atoms';
import styles from '../styles/GridPromo.style';
import withStyles from '../../../hoc/withStyles';

const getSplitText = (val) => {
  return (val && val[0] && val[0].text) || '';
};

// eslint-disable-next-line complexity
const GridPromo = (props) => {
  const { className, promoObj = {}, variation } = props;
  const { textItems, subHeadLine, promoWrapper, moduleName } = promoObj;

  // isMounted state is needed to ensure the <Suspense> only renders client-side
  // We don't want to render this in SSR (and can't because NextJS will error).
  const [isMounted, setMounted] = useState(false);

  // Set mounted flag on mount
  useEffect(() => {
    setMounted(true);
  }, []);

  const headingLine = getSplitText(textItems);
  const headLineParts = headingLine.split('|');

  const descriptionLine = getSplitText(subHeadLine);
  const descriptionParts = descriptionLine.split('|');

  if (moduleName === 'moduleX') {
    return isMounted ? <ModuleX data={promoObj} {...promoObj} /> : null;
  }

  if (variation === 'horizontal') {
    return (
      <div className={`${className} horizontal-promo`}>
        {headLineParts &&
          headLineParts.map((line) => {
            return (
              <BodyCopy color="black" fontFamily="secondary" fontSize="fs24" textAlign="center">
                {line}
              </BodyCopy>
            );
          })}
      </div>
    );
  }
  return (
    <div className={`${className} product-tile promo-div`}>
      <div className="headline-wrapper">
        {headLineParts &&
          headLineParts.map((line) => {
            return (
              <>
                <BodyCopy
                  Component="span"
                  className={`${className} highlighted-text`}
                  color="black"
                  fontFamily="secondary"
                  fontSize={['fs28', 'fs32', 'fs36']}
                  fontWeight="black"
                  textAlign="center"
                >
                  {line}
                </BodyCopy>
                <br />
              </>
            );
          })}
      </div>
      <div className="middle-text-wrapper">
        <BodyCopy
          color="white"
          fontFamily="secondary"
          fontSize={['fs18', 'fs20', 'fs24']}
          fontWeight="black"
          textAlign="center"
        >
          {textItems && textItems[1] && textItems[1].text}
        </BodyCopy>
        <BodyCopy
          color="white"
          fontFamily="secondary"
          fontSize={['fs18', 'fs20', 'fs24']}
          fontWeight="black"
          textAlign="center"
        >
          {textItems && textItems[2] && textItems[2].text}
        </BodyCopy>
      </div>
      <div className="description-wrapper">
        {descriptionParts &&
          descriptionParts.map((description) => {
            return (
              <BodyCopy
                color="white"
                fontFamily="secondary"
                fontSize={['fs10', 'fs12', 'fs12']}
                textAlign="center"
              >
                {description}
              </BodyCopy>
            );
          })}
      </div>
      <div className="cta-wrapper">
        {promoWrapper &&
          promoWrapper.map((cta) => {
            if (cta.type === 'button') {
              return (
                <Button buttonVariation="variable-width" fill="BLUE">
                  {cta.text}
                </Button>
              );
            }
            return (
              <Anchor
                to={cta.url}
                asPath={cta.url}
                target={cta.target}
                title={cta.title}
                anchorVariation="white"
              >
                {cta.text}
              </Anchor>
            );
          })}
      </div>
    </div>
  );
};

GridPromo.propTypes = {
  className: PropTypes.string,
  promoObj: PropTypes.shape({
    textItems: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string,
      })
    ).isRequired,
    subHeadLine: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string,
      })
    ),
    promoWrapper: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string,
      })
    ),
    mediaWrapper: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string,
      })
    ),
  }).isRequired,
  variation: PropTypes.string,
};

GridPromo.defaultProps = {
  className: '',
  variation: 'vertical',
};

export default withStyles(GridPromo, styles);
export { GridPromo as GridPromoVanilla };
