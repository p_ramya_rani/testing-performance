// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  getIconPath,
  configureInternalNavigationFromCMSUrl,
  isClient,
  getViewportInfo,
} from '@tcp/core/src/utils';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/views/Carousel';
import withStyles from '../../../hoc/withStyles';
import styles from '../styles/JeansModule.style';
import { Anchor, DamImage, BodyCopy, Image } from '../../../atoms';
import theme from '../../../../../../styles/themes/TCP';

const { breakpoints } = theme;

const CAROUSEL_OPTIONS = {
  autoplay: false,
  arrows: true,
  fade: false,
  speed: 1000,
  lazyLoad: false,
  dots: false,
  swipe: true,
  slidesToShow: 6,
  slidesToScroll: 1,
  infinite: false,
  responsive: [
    {
      breakpoint: parseInt(breakpoints.medium, 10) - 1,
      settings: {
        slidesToShow: 2.5,
        slidesToScroll: 1,
        arrows: false,
      },
    },
    {
      breakpoint: parseInt(breakpoints.large, 10) - 1,
      settings: {
        slidesToShow: 4.25,
        arrows: false,
        swipeToSlide: false,
        slidesToScroll: 4.25,
      },
    },
  ],
};

export class JeansModule extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    accessibility: PropTypes.shape({
      lbl_tick_icon: PropTypes.string,
      lbl_plus_icon: PropTypes.string,
    }),
    headLine: PropTypes.shape([]),
    imageTileWrapper: PropTypes.shape([]),
  };

  static defaultProps = {
    headLine: [],
    imageTileWrapper: [],
    className: '',
    accessibility: {},
  };

  renderItems = (imageTileWrapper, accessibility, className = '') => {
    return imageTileWrapper.map(
      ({ imageStyled, headLine, subHeadLine, textList, singleCTAButton }, index) => {
        const toPath = configureInternalNavigationFromCMSUrl(singleCTAButton.url);
        return (
          <div className={`jeans-carousel ${className}`} key={index.toString()}>
            <Anchor
              className="image-link"
              to={toPath}
              asPath={singleCTAButton.url}
              dataLocator="dummy-datalocator"
            >
              <DamImage
                className="carousel-image"
                imgData={{
                  alt: imageStyled[0].image.alt,
                  url: imageStyled[0].image.url,
                }}
              />
              <BodyCopy className="image-text" fontSize="fs18" fontWeight="extrabold">
                {imageStyled[0].styled.text}
              </BodyCopy>
              <div className="overlapping-section">
                <div className="text-container">
                  <BodyCopy
                    className="text-header"
                    textAlign="center"
                    fontSize="fs20"
                    fontWeight="black"
                  >
                    {headLine[0].text}
                  </BodyCopy>
                  <BodyCopy
                    className="text-subheader"
                    textAlign="center"
                    fontWeight="semibold"
                    fontSize="fs12"
                  >
                    {subHeadLine[0].text}
                  </BodyCopy>
                  {textList.map((textItem) => {
                    return (
                      <div className="text-line">
                        <Image
                          className="done-button"
                          src={getIconPath('confirmation-check')}
                          alt={accessibility.lbl_tick_icon}
                        />
                        <BodyCopy
                          className="text-item"
                          fontSize="fs14"
                          fontWeight="extrabold"
                          textAlign="center"
                          fontFamily="secondary"
                        >
                          {textItem.text}
                        </BodyCopy>
                      </div>
                    );
                  })}
                  <Image
                    className={`${imageStyled[0].styled.style} plus-icon`}
                    src={getIconPath('plus')}
                    alt={accessibility.lbl_plus_icon}
                  />
                </div>
                <Anchor
                  className="shop-now-link"
                  to={toPath}
                  asPath={singleCTAButton.url}
                  dataLocator="dummy-datalocator"
                  fontSizeVariation="large"
                  fontFamily="secondary"
                >
                  {singleCTAButton.text}
                </Anchor>
              </div>
            </Anchor>
          </div>
        );
      }
    );
  };

  render() {
    const { headLine: titleHeadLine, imageTileWrapper, className, accessibility } = this.props;
    let isMobile = false;
    let isTablet = false;

    if (isClient()) {
      ({ isMobile, isTablet } = getViewportInfo());
    }

    return (
      <div className={className}>
        {titleHeadLine && titleHeadLine[0] ? (
          <div className="title-section">
            <BodyCopy
              fontSize="fs20"
              fontWeight="semibold"
              fontFamily="secondary"
              color="white"
              textAlign="center"
            >
              {titleHeadLine[0].text}
            </BodyCopy>
          </div>
        ) : null}
        {isMobile || isTablet ? (
          <div className="smooth-scroll-list">
            {this.renderItems(imageTileWrapper, accessibility, 'smooth-scroll-list-item')}
          </div>
        ) : (
          <Carousel
            className="carousel-item"
            options={CAROUSEL_OPTIONS}
            carouselConfig={{
              autoplay: false,
              customArrowLeft: getIconPath('carousel-big-carrot'),
              customArrowRight: getIconPath('carousel-big-carrot'),
            }}
          >
            {this.renderItems(imageTileWrapper, accessibility)}
          </Carousel>
        )}
      </div>
    );
  }
}

export { JeansModule as JeansModuleVanilla };
export default withStyles(JeansModule, styles);
