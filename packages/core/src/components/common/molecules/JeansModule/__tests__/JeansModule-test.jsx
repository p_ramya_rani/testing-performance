// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { getViewportInfo } from '@tcp/core/src/utils';
import { JeansModuleVanilla as JeanModule } from '../views/JeansModule.view';

beforeEach(() => jest.resetModules());

jest.mock('@tcp/core/src/utils', () => ({
  getViewportInfo: jest.fn(),
  getStaticFilePath: jest.fn,
  isMobile: jest.fn(),
  isTablet: jest.fn(),
  isClient: () => true,
  getIconPath: (icon) => `/static/${icon}`,
  isMobileApp: () => false,
  isCanada: jest.fn(),
  getSiteId: jest.fn(),
}));

describe('Jeans module', () => {
  const props = {
    fullBleed: true,
    data: {
      headLine: [
        {
          text: 'THE POLO SHOP',
          style: '',
          __typename: 'StyledText',
        },
      ],
      imageTileWrapper: [
        {
          imageStyled: [
            {
              image: {
                url: '/w_250,f_auto,q_auto/ecom/assets/content/tcp/us/plp/girl-polo/image-1_3x.png',
                alt: 'PIQUE',
                title: 'PIQUE',
                crop_d: '',
                crop_t: '',
                crop_m: '',
                __typename: 'Image',
              },
              styled: {
                text: 'PIQUE',
                __typename: 'StyledText',
              },
              __typename: 'StyledLinkedImage',
            },
          ],
          headLine: [
            {
              text: 'PIQUE',
              style: '',
              __typename: 'StyledText',
            },
          ],
          subHeadLine: [
            {
              text: 'CLASSIC STYLE WITH ADDED STRETCH',
              style: '',
              __typename: 'StyledText',
            },
          ],
          textList: [
            {
              text: 'comfy, stretch pique',
              __typename: 'Text',
            },
            {
              text: 'rib-knit collar',
              __typename: 'Text',
            },
          ],
          singleCTAButton: {
            url: '/c/girls-pique-polo-shirts?icid=plp_s1_image_g_060920_pique_polo',
            text: 'SHOP NOW',
            target: '',
            title: '',
            __typename: 'Button',
          },
          __typename: 'ImageTileWrapper',
        },
      ],
      __typename: 'Composite',
      moduleName: 'jeans',
      set: [
        {
          val: 'left',
          key: 'moduleClassName',
          __typename: 'KeyValPair',
        },
        {
          val: 'black',
          key: 'bgColor',
          __typename: 'KeyValPair',
        },
      ],
      moduleClassName: 'left',
      bgColor: 'black',
    },
    asPath: '/us/c/uniform-shop-girls-school-uniform-polo-shirts',
    accessibility: {
      lbl_tick_icon: 'lbl_tick_icon',
      lbl_plus_icon: 'lbl_plus_icon',
      previousPrice: 'Previous Price',
    },
  };

  it('should render carousel for desktop', () => {
    getViewportInfo.mockImplementation(() => ({
      isMobile: false,
      isTablet: false,
    }));
    const wrapper = shallow(<JeanModule {...props} />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.carousel-item')).toHaveLength(1);
  });

  it('should render custom scroll for mobile', () => {
    getViewportInfo.mockImplementation(() => ({
      isMobile: true,
      isTabler: false,
    }));
    const wrapper = shallow(<JeanModule {...props} />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.smooth-scroll-list')).toHaveLength(1);
  });

  it('should render custom scroll for tablet', () => {
    getViewportInfo.mockImplementation(() => ({
      isMobile: false,
      isTablet: true,
    }));
    const wrapper = shallow(<JeanModule {...props} />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.smooth-scroll-list')).toHaveLength(1);
  });
});
