// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .address-field {
    height: ${props =>
      props.variation === 'primary' ? props.theme.spacing.FORM_FIELD_HEIGHT : 'auto'};
  }
  input {
    background-color: ${props =>
      props.variation === 'secondary' || props.grayTextBox
        ? props.theme.colorPalette.gray[300]
        : ''};
  }
  .country-selector {
    position: relative;
    > div {
      height: auto;
      > select {
        margin: 0;
      }
    }
  }
  .zip-code {
    @media ${props => props.theme.mediaQuery.smallMax} {
      margin-right: 0;
      margin-left: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .zip-code-ab-test {
    @media ${props => props.theme.mediaQuery.smallMax} {
      margin-right: 0;

      input {
        width: calc(50% - ${props => props.theme.spacing.ELEM_SPACING.MED});
      }
    }
  }

  .city-ab-test {
    @media ${props => props.theme.mediaQuery.smallMax} {
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .change-country-link {
    font-size: ${props => props.theme.typography.fontSizes.fs10};
    display: block;
    position: absolute;
    bottom: -12px;
    z-index: 1;
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      bottom: -2px;
    }
    @media ${props => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }
  ${props => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default styles;
