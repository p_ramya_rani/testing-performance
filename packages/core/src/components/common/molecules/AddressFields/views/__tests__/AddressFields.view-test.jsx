// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { AddressFields } from '../AddressFields.view';

describe('AddressFields component', () => {
  it('should render correctly', () => {
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
    };
    const component = shallow(<AddressFields {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly with zip code AB Test enabled', () => {
    const props = {
      formName: 'checkoutShipping',
      addressFormLabels: {},
      formSection: 'shippingAddress',
      zipCodeABTestEnabled: true,
      getZipCodeSuggestedAddress: jest.fn(),
    };
    const component = shallow(<AddressFields {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should fetch the Suggested Address for city and state', () => {
    const props = {
      formName: 'checkoutShipping',
      addressFormLabels: {},
      formSection: 'shippingAddress',
      zipCodeFirst: true,
      getZipCodeSuggestedAddress: jest.fn(),
      showPhoneNumber: false,
      showDefaultCheckbox: false,
      variation: 'secondary',
      formMetaInfo: {
        address: {
          zipCode: {
            touched: true,
            active: true,
          },
        },
      },
      formSyncError: {
        address: {
          zipCode: '',
        },
      },
    };
    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.find('[name="zipCode"]').simulate('change', { target: { value: '07094' } });
    expect(props.getZipCodeSuggestedAddress).toHaveBeenCalled();
  });

  it('should not fetch the Suggested Address for city and state', () => {
    const props = {
      formName: 'checkoutShipping',
      addressFormLabels: {},
      formSection: 'shippingAddress',
      getZipCodeSuggestedAddress: jest.fn(),
      showPhoneNumber: false,
      showDefaultCheckbox: false,
      variation: 'secondary',
      formMetaInfo: {
        address: {
          zipCode: {
            touched: true,
            active: true,
          },
        },
      },
      formSyncError: {
        address: {
          zipCode: '',
        },
      },
    };
    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.find('[name="zipCode"]').simulate('change', { target: { value: '07094' } });
    expect(props.getZipCodeSuggestedAddress).not.toHaveBeenCalled();
  });

  it('should not fetch the Suggested Address for city and state', () => {
    const props = {
      formName: 'checkoutShipping',
      addressFormLabels: {},
      formSection: 'shippingAddress',
      zipCodeABTestEnabled: true,
      getZipCodeSuggestedAddress: jest.fn(),
      showPhoneNumber: false,
      showDefaultCheckbox: false,
      variation: 'secondary',
      formMetaInfo: {
        address: {
          zipCode: {
            touched: true,
            active: true,
          },
        },
      },
      formSyncError: {
        address: {
          zipCode: '',
        },
      },
    };
    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US', zipCodeValue: '0709', zipCodeDirty: true });
    expect(props.getZipCodeSuggestedAddress).not.toHaveBeenCalled();
  });

  it('should render correctly without phoneNumber and ', () => {
    const props = {
      formName: 'creditCard',
      formSection: 'address',
      addressFormLabels: {},
      showPhoneNumber: false,
      showDefaultCheckbox: false,
    };
    const component = shallow(<AddressFields {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly without phoneNumber and variation secondary and country ca', () => {
    const props = {
      formName: 'creditCard',
      formSection: 'address',
      addressFormLabels: {},
      showPhoneNumber: false,
      showDefaultCheckbox: false,
      variation: 'secondary',
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'CA' });
    expect(component).toMatchSnapshot();
  });
  it('should render correctly without phoneNumber and variation secondary and country US', () => {
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      showPhoneNumber: false,
      showDefaultCheckbox: false,
      variation: 'primary',
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    expect(component).toMatchSnapshot();
  });
  it('should not called updateEDD method for empty Cart', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: ['NY'],
      isEditing: false,
      address: { state: 'NY', zipCode: '10011' },
      cartOrderItems: [],
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called updateEDD method if eddAllowedState is false', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: '',
      isEditing: false,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called updateEDD method if zipcode is null', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: '',
      isEditing: false,
      address: { state: 'NY', zipCode: null },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called updateEDD method in editing mode', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called updateEDD method if edd is disabled', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const mockedgetStateEligibility = jest.fn().mockReturnValue(true);
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      getStateEligibility: mockedgetStateEligibility,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: false,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called triggerEddUpdate method if edd is disabled', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const mockedgetStateEligibility = jest.fn().mockReturnValue(true);
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      getStateEligibility: mockedgetStateEligibility,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: false,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    jest.spyOn(component.instance(), 'triggerEddUpdate').mockImplementation(() => jest.fn());
    component.instance().changeShipmentMethods();
    expect(component.instance().triggerEddUpdate).not.toHaveBeenCalled();
  });

  it('should called triggerEddUpdate method if edd is enabled', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const mockedgetStateEligibility = jest.fn().mockReturnValue(true);
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      getStateEligibility: mockedgetStateEligibility,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    jest.spyOn(component.instance(), 'triggerEddUpdate').mockImplementation(() => jest.fn());
    component.instance().changeShipmentMethods();
    expect(component.instance().triggerEddUpdate).toHaveBeenCalled();
  });

  it('should not called triggerEddUpdate with getShippingMethod method', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const mockedgetStateEligibility = jest.fn().mockReturnValue(true);
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      getStateEligibility: mockedgetStateEligibility,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    jest.spyOn(component.instance(), 'triggerEddUpdate').mockImplementation(() => jest.fn());
    component.instance().getShippingMethod('10001');
    expect(component.instance().triggerEddUpdate).not.toHaveBeenCalled();
  });

  it('should render with zip error', () => {
    const props = {
      formName: 'creditCard',
      formSection: 'address',
      addressFormLabels: {},
      showPhoneNumber: false,
      showDefaultCheckbox: false,
      variation: 'secondary',
      formMetaInfo: {
        address: {
          zipCode: {
            touched: true,
            active: true,
          },
        },
      },
      formSyncError: {
        address: {
          zipCode: 'Please enter a valid zip code',
        },
      },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'CA' });
    expect(component).toMatchSnapshot();
  });

  it('should render without zip error', () => {
    const props = {
      formName: 'creditCard',
      formSection: 'address',
      addressFormLabels: {},
      showPhoneNumber: false,
      showDefaultCheckbox: false,
      variation: 'secondary',
      formMetaInfo: {
        address: {
          zipCode: {
            touched: true,
            active: true,
          },
        },
      },
      formSyncError: {
        address: {
          zipCode: '',
        },
      },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'CA' });
    expect(component).toMatchSnapshot();
  });
});
