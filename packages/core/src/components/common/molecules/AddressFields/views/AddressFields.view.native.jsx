/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { getAddressFromPlace, getLabelValue } from '@tcp/core/src/utils';
import { formatPhoneNumber } from '@tcp/core/src/utils/formValidation/phoneNumber';
import { getEcomSkuParams } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { Field, change } from 'redux-form';
import { GooglePlacesInput } from '@tcp/core/src/components/common/atoms/AutoSuggest/AutoCompleteComponent.native';
import { GooglePlacesInputGmaps } from '@tcp/core/src/components/common/atoms/AutoSuggest/AutoCompleteComponentGmaps.native';
import validatorMethods from '@tcp/core/src/utils/formValidation/validatorMethods';
import TextBox from '../../../atoms/TextBox';
import Select from '../../../atoms/Select';
import getStandardConfig from '../../../../../utils/formValidation/validatorStandardConfig';
import {
  InputFieldPhoneNumber,
  InputFieldHalf,
  StateZipCodeContainer,
} from '../styles/AddressFields.style.native';
import {
  countriesOptionsMap,
  CAcountriesStatesTable,
  UScountriesStatesTable,
} from '../../../organisms/AddressForm/CountriesAndStates.constants';
import { API_CONFIG } from '../../../../../services/config';

export class AddressFields extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    addressFormLabels: PropTypes.shape({}).isRequired,
    formName: PropTypes.string.isRequired,
    formSection: PropTypes.string,
    loadShipmentMethods: PropTypes.func.isRequired,
    disableCountry: PropTypes.bool,
    showUserName: PropTypes.bool,
    showPhoneNumber: PropTypes.bool,
    showEmailAddress: PropTypes.bool,
    initialValues: PropTypes.shape({}),
    isGuest: PropTypes.bool,
    createRefParent: PropTypes.func,
    address: PropTypes.shape({}),
    cartOrderItems: PropTypes.shape([]),
    updateEdd: PropTypes.func,
    isEDD: PropTypes.bool,
    eddAllowedStatesArr: PropTypes.shape([]),
    currentOrderId: PropTypes.string,
    isEditing: PropTypes.bool,
    addressLine1: PropTypes.string,
    setAddressLine1: PropTypes.func,
    zipCodeABTestEnabled: PropTypes.bool,
    getZipCodeSuggestedAddress: PropTypes.func,
    resetShippingEddData: PropTypes.func,
    mapboxAutocompleteTypesParam: PropTypes.string,
    mapboxSwitch: PropTypes.bool,
    zipCodeFirst: PropTypes.bool,
  };

  static defaultProps = {
    formSection: '',
    disableCountry: false,
    showUserName: true,
    showPhoneNumber: true,
    showEmailAddress: true,
    initialValues: {},
    isGuest: true,
    createRefParent: () => {},
    address: {},
    cartOrderItems: [],
    updateEdd: () => {},
    isEDD: false,
    eddAllowedStatesArr: [],
    currentOrderId: '',
    isEditing: false,
    addressLine1: '',
    setAddressLine1: () => {},
    zipCodeABTestEnabled: false,
    getZipCodeSuggestedAddress: () => {},
    resetShippingEddData: () => {},
    mapboxAutocompleteTypesParam: '',
    mapboxSwitch: true,
    zipCodeFirst: false,
  };

  static addressValidationConfig = getStandardConfig([
    'firstName',
    'lastName',
    'addressLine1',
    'addressLine2',
    'city',
    'state',
    'zipCode',
    'country',
    'phoneNumber',
    'emailAddress',
  ]);

  constructor(props) {
    super(props);
    const {
      siteIds: { us },
    } = API_CONFIG;
    this.state = {
      country: us.toUpperCase(),
    };
  }

  getStateEligibility = (eddAllowedStatesArr, state) => {
    return eddAllowedStatesArr && eddAllowedStatesArr.length > 0
      ? eddAllowedStatesArr.indexOf(state) > -1
      : false;
  };

  triggerEddUpdate = (zipCode, state) => {
    const {
      updateEdd,
      cartOrderItems,
      eddAllowedStatesArr,
      currentOrderId,
      isEditing,
      resetShippingEddData,
    } = this.props;
    const eddAllowedState = this.getStateEligibility(eddAllowedStatesArr, state);
    const skuData = (cartOrderItems && getEcomSkuParams(cartOrderItems)) || [];
    if (typeof updateEdd === 'function' && cartOrderItems && !isEditing && skuData.length) {
      const orderId = currentOrderId.toString();
      if (eddAllowedState && zipCode) {
        updateEdd({
          zipCode,
          state,
          orderId,
          skuList: skuData,
          ignoreCache: true,
        });
      } else {
        resetShippingEddData();
      }
    }
  };

  handlePlaceSelected = (address) => {
    const { dispatch, formName, formSection, loadShipmentMethods, isEDD } = this.props;
    dispatch(change(formName, `${formSection}.city`, address?.city));
    dispatch(change(formName, `${formSection}.state`, address?.state));
    dispatch(change(formName, `${formSection}.zipCode`, address?.zip));
    dispatch(change(formName, `${formSection}.addressLine1`, address.addressline));
    this.changeZipCode(address.zip);
    if (loadShipmentMethods) {
      const addressField1 = address.addressline || address.street;
      loadShipmentMethods({ state: address.state, addressField1, formName });
      if (isEDD) {
        this.triggerEddUpdate(address && address.zip, address.state);
      }
    }
  };

  handlePlaceSelectedGmaps = (place, inputValue) => {
    const { dispatch, formName, formSection, loadShipmentMethods, isEDD } = this.props;
    const address = getAddressFromPlace(place, inputValue);
    dispatch(change(formName, `${formSection}.city`, address.city));
    dispatch(change(formName, `${formSection}.zipCode`, address.zip));
    dispatch(change(formName, `${formSection}.state`, address.state));
    dispatch(change(formName, `${formSection}.addressLine1`, address.street));
    if (loadShipmentMethods) {
      const addressField1 = address.streetName || address.street;
      loadShipmentMethods({ state: address.state, addressField1, formName });
      if (isEDD) {
        this.triggerEddUpdate(address && address.zip, address.state);
      }
    }
  };

  getInitialAddressLine1 = (initialValues, addressLine1) => {
    // handlePlaceSelected is not being invoked when user comes back from 'AddressVerification' modal and so
    // we are maintaining a local state for 'addressLine1' in 'CreditCardForm.view'.
    if (addressLine1) return addressLine1;
    return (initialValues && initialValues.address && initialValues.address.addressLine1) || '';
  };

  changeShipmentMethods = (value) => {
    const { loadShipmentMethods, formName, address, isEDD } = this.props;
    const { zipCode, state } = address;
    const stateVal = value || state;
    if (loadShipmentMethods) {
      loadShipmentMethods({ state: value, addressField1: address.addressLine1, formName });
      if (isEDD) {
        this.triggerEddUpdate(zipCode, stateVal);
      }
    }
  };

  changeZipCode = (value) => {
    const { country } = this.state;
    const {
      getZipCodeSuggestedAddress,
      formName,
      formSection,
      loadShipmentMethods,
      isEDD,
      cartOrderItems,
      currentOrderId,
      isEditing,
      eddAllowedStatesArr,
      zipCodeFirst,
    } = this.props;

    const skuData = (cartOrderItems && getEcomSkuParams(cartOrderItems)) || [];
    const orderId = currentOrderId.toString();

    if (zipCodeFirst && validatorMethods.zipcode(value, null, null, [country])) {
      getZipCodeSuggestedAddress({
        zipCodeValue: value,
        formName,
        formSection,
        shouldLoadShipmentMethods: loadShipmentMethods,
        skuData,
        orderId,
        isEDD,
        isEditing,
        eddAllowedStatesArr,
      });
    }
  };

  renderCity = (addressFormLabels, createRefParent) => (
    <Field
      id="city"
      name="city"
      label={getLabelValue(addressFormLabels, 'city')}
      component={TextBox}
      dataLocator="addnewaddress-city"
      inputRef={(node) => createRefParent(node, 'city')}
    />
  );

  renderState = (addressFormLabels, createRefParent, isCA) => {
    return (
      <Field
        id="state"
        name="state"
        component={Select}
        placeholder={getLabelValue(addressFormLabels, isCA ? 'province' : 'stateLbl')}
        heading={getLabelValue(addressFormLabels, isCA ? 'province' : 'stateLbl')}
        options={isCA ? CAcountriesStatesTable : UScountriesStatesTable}
        onValueChange={this.changeShipmentMethods}
        ref={(node) => createRefParent(node, 'state')}
      />
    );
  };

  renderZipCode = (addressFormLabels, createRefParent, isCA) => (
    <Field
      id="zipCode"
      name="zipCode"
      label={getLabelValue(addressFormLabels, isCA ? 'postalCode' : 'zipCode')}
      maxLength={isCA ? 7 : 5}
      component={TextBox}
      dataLocator="addnewaddress-zipcode"
      keyboardType="numeric"
      onChangeText={this.changeZipCode}
      inputRef={(node) => createRefParent(node, 'zipCode')}
    />
  );

  renderCityStateZipCode = () => {
    const { country } = this.state;
    const isCA = country === API_CONFIG.siteIds.ca.toUpperCase();
    const { addressFormLabels, createRefParent, zipCodeFirst } = this.props;
    return zipCodeFirst ? (
      <>
        <StateZipCodeContainer>
          <InputFieldHalf>
            {this.renderZipCode(addressFormLabels, createRefParent, isCA)}
          </InputFieldHalf>
        </StateZipCodeContainer>
        <StateZipCodeContainer>
          <InputFieldHalf>
            {this.renderCity(addressFormLabels, createRefParent, isCA)}
          </InputFieldHalf>
          <InputFieldHalf>
            {this.renderState(addressFormLabels, createRefParent, isCA)}
          </InputFieldHalf>
        </StateZipCodeContainer>
      </>
    ) : (
      <>
        {this.renderCity(addressFormLabels, createRefParent, isCA)}
        <StateZipCodeContainer>
          <InputFieldHalf>
            {this.renderState(addressFormLabels, createRefParent, isCA)}
          </InputFieldHalf>
          <InputFieldHalf>
            {this.renderZipCode(addressFormLabels, createRefParent, isCA)}
          </InputFieldHalf>
        </StateZipCodeContainer>
      </>
    );
  };

  render() {
    const {
      addressFormLabels,
      formSection,
      setAddressLine1,
      addressLine1,
      disableCountry,
      showUserName,
      showPhoneNumber,
      showEmailAddress,
      isGuest,
      createRefParent,
      initialValues,
      zipCodeABTestEnabled,
      mapboxSwitch,
      mapboxAutocompleteTypesParam,
    } = this.props;
    const { country } = this.state;
    let component = TextBox;
    if (!zipCodeABTestEnabled) {
      if (mapboxSwitch) {
        component = GooglePlacesInput;
      } else {
        component = GooglePlacesInputGmaps;
      }
    }
    return (
      <View>
        {showUserName && (
          <>
            <Field
              name="firstName"
              id="firstName"
              label={getLabelValue(addressFormLabels, 'firstName')}
              type="text"
              component={TextBox}
              maxLength={50}
              dataLocator="addnewaddress-firstname"
              inputRef={(node) => createRefParent(node, 'firstName')}
            />
            <Field
              id="lastName"
              name="lastName"
              label={getLabelValue(addressFormLabels, 'lastName')}
              component={TextBox}
              dataLocator="addnewaddress-lastname"
              inputRef={(node) => createRefParent(node, 'lastName')}
            />
          </>
        )}
        <Field
          label={getLabelValue(addressFormLabels, 'addressLine1')}
          headerTitle={getLabelValue(addressFormLabels, 'addressLine1')}
          component={component}
          onValueChange={(data, inputValue) => {
            if (mapboxSwitch) {
              this.handlePlaceSelected(data);
            } else {
              this.handlePlaceSelectedGmaps(data, inputValue);
            }
          }}
          onChangeText={(addressLines) => setAddressLine1(addressLines)}
          initialValue={this.getInitialAddressLine1(initialValues, addressLine1)}
          dataLocator="addnewaddress-addressl1"
          componentRestrictions={{ ...{ country: [country] } }}
          id="addressLine1"
          name="addressLine1"
          inputRef={(node) => createRefParent(node, 'addressLine1')}
          mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
        />
        <Field
          id="addressLine2"
          name="addressLine2"
          maxLength={30}
          label={getLabelValue(addressFormLabels, 'addressLine2')}
          component={TextBox}
          dataLocator="addnewaddress-addressl2"
          inputRef={(node) => createRefParent(node, 'addressLine2')}
        />
        {this.renderCityStateZipCode()}
        <Field
          id="country"
          name="country"
          component={Select}
          heading={getLabelValue(addressFormLabels, 'country')}
          options={countriesOptionsMap}
          onValueChange={(itemValue) => {
            this.setState({ country: itemValue });
          }}
          disabled={disableCountry}
          inputRef={(node) => createRefParent(node, 'country')}
        />
        {showPhoneNumber && (
          <InputFieldPhoneNumber>
            <Field
              id="phoneNumber"
              name="phoneNumber"
              label={getLabelValue(addressFormLabels, 'phoneNumber')}
              component={TextBox}
              dataLocator="addnewaddress-phnumber"
              keyboardType="phone-pad"
              inputRef={(node) => createRefParent(node, 'phoneNumber')}
              normalize={formatPhoneNumber}
            />
          </InputFieldPhoneNumber>
        )}
        {showEmailAddress && isGuest && (
          <Field
            label={getLabelValue(addressFormLabels, 'email')}
            name="emailAddress"
            id={`${formSection}.emailAddress`}
            component={TextBox}
            dataLocator="email-address-field"
            enableSuccessCheck={false}
            inputRef={(node) => createRefParent(node, 'emailAddress')}
          />
        )}
      </View>
    );
  }
}

export default AddressFields;
