/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getAddressFromPlace, trimMaxCharacters } from '@tcp/core/src/utils';
import validatorMethods from '@tcp/core/src/utils/formValidation/validatorMethods';
import { Field, change } from 'redux-form';
import { getEcomSkuParams } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { AutoCompleteComponent } from '@tcp/core/src/components/common/atoms/AutoSuggest/AutoCompleteComponent';
import { AutoCompleteComponentGmaps } from '@tcp/core/src/components/common/atoms/AutoSuggest/AutoCompleteComponentGmaps';
import TextBox from '../../../atoms/TextBox';
import SelectBox from '../../../atoms/Select';
import InputCheckbox from '../../../atoms/InputCheckbox';
import Row from '../../../atoms/Row';
import Col from '../../../atoms/Col';
import getStandardConfig from '../../../../../utils/formValidation/validatorStandardConfig';
import {
  countriesOptionsMap,
  CAcountriesStatesTable,
  UScountriesStatesTable,
} from '../../../organisms/AddressForm/CountriesAndStates.constants';
import styles from '../styles/AddressFields.style';
import Anchor from '../../../atoms/Anchor';
import { getSiteId } from '../../../../../utils/utils.web';
import { API_CONFIG } from '../../../../../services/config';
import { removeSpaces, formatPhoneNumber } from '../../../../../utils/formValidation/phoneNumber';

export class AddressFields extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    addressFormLabels: PropTypes.shape({}).isRequired,
    isMakeDefaultDisabled: PropTypes.bool,
    formName: PropTypes.string.isRequired,
    showDefaultCheckbox: PropTypes.bool,
    showPhoneNumber: PropTypes.bool,
    showUserName: PropTypes.bool,
    formSection: PropTypes.string,
    className: PropTypes.string,
    variation: PropTypes.string,
    loadShipmentMethods: PropTypes.func.isRequired,
    handleShipIntClick: PropTypes.func.isRequired,
    isGuest: PropTypes.bool,
    address: PropTypes.shape({}),
    cartOrderItems: PropTypes.shape([]),
    updateEdd: PropTypes.func,
    isEDD: PropTypes.bool,
    eddAllowedStatesArr: PropTypes.shape([]),
    currentOrderId: PropTypes.string,
    isEditing: PropTypes.bool,
    formMetaInfo: PropTypes.shape({}),
    formSyncError: PropTypes.shape({}),
    resetShippingEddData: PropTypes.func,
  };

  static addressValidationConfig = getStandardConfig([
    'firstName',
    'lastName',
    'addressLine1',
    'addressLine2',
    'city',
    'state',
    'zipCode',
    'country',
    'phoneNumber',
    'emailAddress',
  ]);

  constructor(props) {
    super(props);
    this.state = {
      country: getSiteId() && getSiteId().toUpperCase(),
    };
    this.activeZipProp = 'address.zipCode.active';
    this.activeZipErrorProp = 'address.zipCode';
  }

  StateCountryChange = (e) => {
    this.setState({
      country: e.target.value ? e.target.value : '',
    });
  };

  getStateEligibility = (eddAllowedStatesArr, state) => {
    return eddAllowedStatesArr && eddAllowedStatesArr.length > 0
      ? eddAllowedStatesArr.indexOf(state) > -1
      : false;
  };

  triggerEddUpdate = (zipCode, state) => {
    const {
      updateEdd,
      cartOrderItems,
      eddAllowedStatesArr,
      currentOrderId,
      isEditing,
      resetShippingEddData,
    } = this.props;
    const eddAllowedState = this.getStateEligibility(eddAllowedStatesArr, state);
    const skuData = (cartOrderItems && getEcomSkuParams(cartOrderItems)) || [];
    if (typeof updateEdd === 'function' && cartOrderItems && !isEditing && skuData.length) {
      const orderId = currentOrderId.toString();
      if (eddAllowedState && zipCode) {
        updateEdd({
          zipCode,
          state,
          orderId,
          skuList: skuData,
          ignoreCache: true,
        });
      } else {
        resetShippingEddData();
      }
    }
  };

  handlePlaceSelectedGmaps = (place, inputValue) => {
    const { dispatch, formName, formSection, loadShipmentMethods, isEDD } = this.props;
    const address = getAddressFromPlace(place, inputValue);
    dispatch(change(formName, `${formSection ? 'address.' : ''}city`, address.city));
    dispatch(change(formName, `${formSection ? 'address.' : ''}zipCode`, address.zip));
    dispatch(change(formName, `${formSection ? 'address.' : ''}state`, address.state));
    dispatch(change(formName, `${formSection ? 'address.' : ''}addressLine1`, address.street));
    // Load Shipment Methods when address selected from an autocomplete
    if (loadShipmentMethods) {
      const addressField1 = address.streetName || address.street;
      loadShipmentMethods({ state: address.state, addressField1, formName });
      if (isEDD) {
        this.triggerEddUpdate(address && address.zip, address.state);
      }
    }
  };

  handlePlaceSelected = (address) => {
    const { dispatch, formName, formSection, loadShipmentMethods, zipCodeFirst } = this.props;
    dispatch(change(formName, `${formSection ? 'address.' : ''}zipCode`, address.zip));
    dispatch(change(formName, `${formSection ? 'address.' : ''}addressLine1`, address.addressline));
    if (!zipCodeFirst) {
      dispatch(change(formName, `${formSection ? 'address.' : ''}city`, address.city));
      dispatch(change(formName, `${formSection ? 'address.' : ''}state`, address.state));
    }
    this.changeZipCode(address.zip);
    // Load Shipment Methods when address selected from an autocomplete
    if (loadShipmentMethods) {
      const addressField1 = address.streetName || address.street;
      loadShipmentMethods({ state: address.state, addressField1, formName });
    }
  };

  changeShipmentMethods = (e, value) => {
    const { loadShipmentMethods, formName, address, isEDD } = this.props;
    const { country } = this.state;
    const stateVal = value || address.state;
    if (loadShipmentMethods && country === API_CONFIG.siteIds.us.toUpperCase()) {
      loadShipmentMethods({ state: value, addressField1: address.addressLine1, formName });
      if (isEDD) {
        this.triggerEddUpdate(address.zipCode, stateVal);
      }
    }
  };

  getShippingMethod = (zipCodeValue) => {
    const { loadShipmentMethods, formName, address } = this.props;
    const { country } = this.state;
    if (loadShipmentMethods && country === API_CONFIG.siteIds.ca.toUpperCase()) {
      loadShipmentMethods({
        zipCode: zipCodeValue,
        addressField1: address.addressLine1,
        formName,
      });
    }
  };

  renderCountrySelector = () => {
    const { addressFormLabels, formSection, handleShipIntClick, variation, zipCodeFirst } =
      this.props;
    return (
      <>
        <Col
          colSize={{
            small: 6,
            medium: 4,
            large: zipCodeFirst && variation === 'primary' ? 6 : 3,
          }}
          ignoreGutter={{ small: true }}
          className="country-selector"
        >
          <Field
            id={`${formSection}.country`}
            placeholder={addressFormLabels.country}
            title={addressFormLabels.country}
            name="country"
            component={SelectBox}
            options={countriesOptionsMap}
            onChange={this.StateCountryChange}
            dataLocator="addnewaddress-country"
            enableSuccessCheck={false}
            disabled={variation === 'secondary'}
          />
          {variation === 'secondary' && (
            <Anchor
              fontSizeVariation="small"
              underline
              noLink
              href="#"
              title=""
              anchorVariation="primary"
              dataLocator="shipping internationally"
              target="_self"
              className="change-country-link"
              onClick={handleShipIntClick}
            >
              {addressFormLabels.shipInternationally}
            </Anchor>
          )}
        </Col>
      </>
    );
  };

  changeZipCode = (event) => {
    const zipCodeValue = typeof event === 'object' ? event.target.value : event;
    const { country } = this.state;
    const {
      getZipCodeSuggestedAddress,
      formName,
      formSection,
      loadShipmentMethods,
      cartOrderItems,
      currentOrderId,
      isEDD,
      isEditing,
      eddAllowedStatesArr,
      address,
      zipCodeFirst,
    } = this.props;
    const addressField1 = address.addressLine1;
    const skuData = (cartOrderItems && getEcomSkuParams(cartOrderItems)) || [];
    const orderId = currentOrderId.toString();
    if (validatorMethods.zipcode(zipCodeValue, null, null, [country])) {
      if (zipCodeFirst) {
        getZipCodeSuggestedAddress({
          zipCodeValue,
          addressField1,
          formName,
          formSection,
          shouldLoadShipmentMethods: loadShipmentMethods,
          skuData,
          orderId,
          isEDD,
          isEditing,
          eddAllowedStatesArr,
        });
      }
      this.getShippingMethod(zipCodeValue);
    }
  };

  renderCity = () => {
    const { variation, addressFormLabels, formSection, zipCodeLoader, zipCodeFirst } = this.props;

    return (
      <Col
        ignoreGutter={{ small: true }}
        colSize={{
          small: zipCodeFirst ? 3 : 6,
          medium: 4,
          large: variation === 'primary' ? 6 : 3,
        }}
        className={zipCodeFirst ? 'city-ab-test' : ''}
      >
        <Field
          id={`${formSection}.city`}
          placeholder={addressFormLabels.city}
          name="city"
          component={TextBox}
          dataLocator="addnewaddress-city"
          className="address-field"
          enableSuccessCheck={false}
          disabled={zipCodeLoader}
          autocomplete="address-level2"
        />
      </Col>
    );
  };

  renderState = () => {
    const { variation, addressFormLabels, formSection, zipCodeFirst } = this.props;
    const { country } = this.state;
    const isCA = country === API_CONFIG.siteIds.ca.toUpperCase();
    const primaryMedium = zipCodeFirst ? 4 : 2;

    return (
      <Col
        ignoreGutter={{
          small: zipCodeFirst,
          large: zipCodeFirst && variation === 'primary',
        }}
        colSize={{ small: 3, medium: variation === 'primary' ? primaryMedium : 4, large: 3 }}
        className={zipCodeFirst ? '' : 'margin-right'}
      >
        <Field
          id={`${formSection}.state`}
          placeholder={isCA ? addressFormLabels.province : addressFormLabels.stateLbl}
          title={isCA ? addressFormLabels.province : addressFormLabels.stateLbl}
          name="state"
          component={SelectBox}
          options={isCA ? CAcountriesStatesTable : UScountriesStatesTable}
          dataLocator="addnewaddress-state"
          className="address-field"
          enableSuccessCheck={false}
          onChange={this.changeShipmentMethods}
          autocomplete="address-level1"
        />
      </Col>
    );
  };

  renderZip = () => {
    const { variation, addressFormLabels, formSection, zipCodeFirst } = this.props;
    const { country } = this.state;
    const isCA = country === API_CONFIG.siteIds.ca.toUpperCase();
    const primaryMedium = zipCodeFirst ? 4 : 2;

    return (
      <Col
        colSize={{
          small: zipCodeFirst ? 6 : 3,
          medium: variation === 'primary' ? primaryMedium : 4,
          large: 3,
        }}
        className={variation === 'secondary' && !zipCodeFirst ? 'zip-code' : 'zip-code-ab-test'}
      >
        <Field
          placeholder={isCA ? addressFormLabels.postalCode : addressFormLabels.zipCode}
          id={`${formSection}.zipCode`}
          name="zipCode"
          maxLength={isCA ? 7 : 5}
          component={TextBox}
          dataLocator="addnewaddress-zipcode"
          className="address-field"
          enableSuccessCheck={false}
          onChange={this.changeZipCode}
          autocomplete="postal-code"
        />
      </Col>
    );
  };

  renderCityStateZipCode = () => {
    const { zipCodeFirst } = this.props;
    return (
      <>
        {zipCodeFirst ? (
          <>
            {this.renderZip()}
            {this.renderCity()}
            {this.renderState()}
          </>
        ) : (
          <>
            {this.renderCity()}
            {this.renderState()}
            {this.renderZip()}
          </>
        )}
      </>
    );
  };

  renderAddressFields = () => {
    const {
      variation,
      addressFormLabels,
      formSection,
      zipCodeABTestEnabled,
      mapboxSwitch,
      mapboxAutocompleteTypesParam,
      zipCodeFirst,
    } = this.props;
    const { country } = this.state;
    let component = TextBox;
    if (!zipCodeABTestEnabled) {
      if (mapboxSwitch) {
        component = AutoCompleteComponent;
      } else {
        component = AutoCompleteComponentGmaps;
      }
    }
    return (
      <>
        <Row fullBleed>
          <Col
            ignoreGutter={{ small: true }}
            colSize={{ small: 6, medium: variation === 'secondary' ? 8 : 4, large: 6 }}
          >
            <Field
              id={`${formSection}.addressLine1`}
              placeholder={addressFormLabels.addressLine1}
              component={component}
              name="addressLine1"
              maxLength={30}
              onPlaceSelected={
                mapboxSwitch ? this.handlePlaceSelected : this.handlePlaceSelectedGmaps
              }
              componentRestrictions={Object.assign({}, { country: [country] })}
              dataLocator="addnewaddress-addressl1"
              className="address-field"
              types={['address']}
              normalize={trimMaxCharacters(30)}
              enableSuccessCheck={false}
              apiFields="address_components"
              autocomplete="address-line1"
              mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
            />
          </Col>
          <Col colSize={{ small: 6, medium: variation === 'secondary' ? 8 : 4, large: 6 }}>
            <Field
              placeholder={addressFormLabels.addressLine2}
              name="addressLine2"
              maxLength={30}
              id={`${formSection}.addressLine2`}
              component={TextBox}
              normalize={trimMaxCharacters(30)}
              dataLocator="addnewaddress-addressl2"
              className="address-field"
              enableSuccessCheck={false}
              autocomplete="address-line2"
            />
          </Col>
        </Row>
        <Row fullBleed>
          {this.renderCityStateZipCode()}
          {(variation === 'secondary' || (zipCodeFirst && variation === 'primary')) &&
            this.renderCountrySelector()}
        </Row>
      </>
    );
  };

  render() {
    const {
      isMakeDefaultDisabled,
      showDefaultCheckbox,
      showPhoneNumber,
      className,
      addressFormLabels,
      variation,
      formSection,
      isGuest,
      showUserName,
      zipCodeFirst,
    } = this.props;
    return (
      <div className={className}>
        {showUserName && (
          <Row fullBleed>
            <Col
              ignoreGutter={{ small: true }}
              colSize={{ small: 6, medium: variation === 'secondary' ? 8 : 4, large: 6 }}
            >
              <Field
                placeholder={addressFormLabels.firstName}
                name="firstName"
                id={`${formSection}.firstName`}
                type="text"
                component={TextBox}
                dataLocator="addnewaddress-firstname"
                className="address-field"
                enableSuccessCheck={false}
                autocomplete="given-name"
              />
            </Col>
            <Col colSize={{ small: 6, medium: variation === 'secondary' ? 8 : 4, large: 6 }}>
              <Field
                placeholder={addressFormLabels.lastName}
                name="lastName"
                id={`${formSection}.lastName`}
                component={TextBox}
                dataLocator="addnewaddress-lastname"
                className="address-field"
                enableSuccessCheck={false}
                autocomplete="family-name"
              />
            </Col>
          </Row>
        )}
        {this.renderAddressFields()}
        {variation === 'primary' && !zipCodeFirst ? (
          <Row fullBleed>
            <Col colSize={{ small: 6, medium: 4, large: 6 }} ignoreGutter={{ small: true }}>
              <Field
                id={`${formSection}.country`}
                placeholder={addressFormLabels.country}
                title={addressFormLabels.country}
                name="country"
                component={SelectBox}
                options={countriesOptionsMap}
                onChange={this.StateCountryChange}
                dataLocator="addnewaddress-country"
                className="address-field"
                enableSuccessCheck={false}
              />
            </Col>
            {showPhoneNumber && (
              <Col colSize={{ small: 6, medium: 4, large: 6 }}>
                <Field
                  placeholder={addressFormLabels.phoneNumber}
                  name="phoneNumber"
                  id={`${formSection}.phoneNumber`}
                  component={TextBox}
                  dataLocator="addnewaddress-phnumber"
                  type="tel"
                  className="address-field"
                  enableSuccessCheck={false}
                  autocomplete="tel"
                />
              </Col>
            )}
          </Row>
        ) : null}
        {showDefaultCheckbox && (
          <Row fullBleed className="elem-mb-XL">
            <Col colSize={{ small: 4, medium: 4, large: 6 }} offsetLeft={{ small: 1 }}>
              <Field
                name="primary"
                component={InputCheckbox}
                dataLocator="addnewaddress-setdefaddress"
                disabled={isMakeDefaultDisabled}
                className="address-field"
              >
                {addressFormLabels.setDefaultMsg}
              </Field>
            </Col>
          </Row>
        )}
        {variation === 'secondary' ? (
          <Row fullBleed>
            <Col colSize={{ small: 6, medium: 8, large: 6 }}>
              <Field
                placeholder={addressFormLabels.phoneNumber}
                name="phoneNumber"
                id={`${formSection}.phoneNumber`}
                component={TextBox}
                dataLocator="addnewaddress-phnumber"
                type="tel"
                className="address-field"
                enableSuccessCheck={false}
                normalize={formatPhoneNumber}
                autocomplete="tel"
              />
            </Col>
            {isGuest && (
              <Col colSize={{ small: 6, medium: 8, large: 6 }}>
                <Field
                  placeholder={addressFormLabels.email}
                  name="emailAddress"
                  id={`${formSection}.emailAddress`}
                  component={TextBox}
                  dataLocator="email-address-field"
                  enableSuccessCheck={false}
                  autocomplete="email"
                  normalize={removeSpaces}
                />
              </Col>
            )}
          </Row>
        ) : null}
      </div>
    );
  }
}

AddressFields.propTypes = {
  zipCodeABTestEnabled: PropTypes.bool,
  getZipCodeSuggestedAddress: PropTypes.func,
  zipCodeLoader: PropTypes.bool,
  mapboxAutocompleteTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  zipCodeFirst: PropTypes.bool,
};
AddressFields.defaultProps = {
  isMakeDefaultDisabled: false,
  showDefaultCheckbox: true,
  showPhoneNumber: true,
  showUserName: true,
  formSection: '',
  className: '',
  variation: 'primary',
  isGuest: true,
  address: { zipCode: '' },
  zipCodeABTestEnabled: false,
  getZipCodeSuggestedAddress: () => {},
  zipCodeLoader: false,
  cartOrderItems: [],
  updateEdd: () => {},
  isEDD: false,
  eddAllowedStatesArr: [],
  currentOrderId: '',
  isEditing: false,
  formMetaInfo: {},
  formSyncError: {},
  resetShippingEddData: () => {},
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  zipCodeFirst: false,
};

export default withStyles(AddressFields, styles);
