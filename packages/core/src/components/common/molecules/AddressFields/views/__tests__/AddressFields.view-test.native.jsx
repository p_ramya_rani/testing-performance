// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AddressFields } from '../AddressFields.view.native';

describe('AddressFields component', () => {
  it('should render correctly', () => {
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      dispatch: jest.fn(),
    };
    const component = shallow(<AddressFields {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly with zip code AB Test enabled', () => {
    const props = {
      formName: 'checkoutShipping',
      addressFormLabels: {},
      formSection: 'shippingAddress',
      zipCodeABTestEnabled: true,
      getZipCodeSuggestedAddress: jest.fn(),
    };
    const component = shallow(<AddressFields {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly without phoneNumber and ', () => {
    const props = {
      formName: 'creditCard',
      formSection: 'address',
      addressFormLabels: {},
      dispatch: jest.fn(),
    };
    const component = shallow(<AddressFields {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly without phoneNumber and variation secondary and country ca', () => {
    const props = {
      formName: 'creditCard',
      formSection: 'address',
      addressFormLabels: {},
      dispatch: jest.fn(),
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'CA' });
    expect(component).toMatchSnapshot();
  });
  it('should render correctly without phoneNumber and variation secondary and country US', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      dispatch: jest.fn(),
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadShipmentMethods).toHaveBeenCalled();
    expect(component).toMatchSnapshot();
  });
  it('should not called updateEDD method', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: ['NY'],
      isEditing: false,
      address: { state: 'NY', zipCode: '10011' },
      cartOrderItems: [],
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called updateEDD method if eddAllowedState is false', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: '',
      isEditing: false,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called updateEDD method if zipcode is null', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: '',
      isEditing: false,
      address: { state: 'NY', zipCode: null },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called updateEDD method in editing mode', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called updateEDD method if edd is disabled', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: false,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    component.instance().changeShipmentMethods();
    expect(mockedloadUpdateEdd).not.toHaveBeenCalled();
  });

  it('should not called triggerEddUpdate method if edd is disabled', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: false,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    jest.spyOn(component.instance(), 'triggerEddUpdate').mockImplementation(() => jest.fn());
    component.instance().changeShipmentMethods();
    expect(component.instance().triggerEddUpdate).not.toHaveBeenCalled();
  });

  it('should called triggerEddUpdate method if edd is enabled', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    jest.spyOn(component.instance(), 'triggerEddUpdate').mockImplementation(() => jest.fn());
    component.instance().changeShipmentMethods();
    expect(component.instance().triggerEddUpdate).toHaveBeenCalled();
  });

  it('should not called triggerEddUpdate method with changeZipCode', () => {
    const mockedcheckPOBoxAddress = jest.fn();
    const mockedloadShipmentMethods = jest.fn();
    const mockedloadUpdateEdd = jest.fn();
    const props = {
      formName: 'creditCard',
      addressFormLabels: {},
      formSection: 'address',
      checkPOBoxAddress: mockedcheckPOBoxAddress,
      loadShipmentMethods: mockedloadShipmentMethods,
      updateEdd: mockedloadUpdateEdd,
      dispatch: jest.fn(),
      isEDD: true,
      eddAllowedStatesArr: ['NY'],
      isEditing: true,
      address: { state: 'NY', zipCode: '10011' },
    };

    const component = shallow(<AddressFields {...props} />);
    component.setState({ country: 'US' });
    jest.spyOn(component.instance(), 'triggerEddUpdate').mockImplementation(() => jest.fn());
    component.instance().changeZipCode('10001');
    expect(component.instance().triggerEddUpdate).not.toHaveBeenCalled();
  });
});
