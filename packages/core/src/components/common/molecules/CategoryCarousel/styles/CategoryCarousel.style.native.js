// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const Wrapper = styled.View`
  width: 100%;
  margin: ${props => props.theme.spacing.ELEM_SPACING.MED} 0
    ${props => props.theme.spacing.ELEM_SPACING.MED}
    ${props => props.theme.spacing.ELEM_SPACING.MED};
  padding-right: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;
export const ImageTouchableOpacity = styled.TouchableOpacity`
  flex-direction: row;
  align-items: flex-start;
`;

export const Container = styled.View`
  width: ${props => props.width}px;
  flex-direction: column;
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const TextWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;
