// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import * as utils from '@tcp/core/src/utils/utils';
import { CategoryCarouselVanilla as CategoryCarousel } from '../views/CategoryCarousel.view.native';

utils.getBrand = jest.fn().mockReturnValue('tcp');

describe('BagCarouselModule component', () => {
  let component;
  const navItems = {
    data: {
      navigation: {
        nav: [
          {
            categoryContent: {
              isUnique: true,
              isShortImage: false,
              longDescription: 'long description 1',
              productCount: 1853,
              description: 'Sizes 4-141',
              groupIdentifier: '',
              name: 'Girl',
              id: '47511',
              displayToCustomer: true,
              mainCategory: {
                contentId: '47511',
                name: 'GIRL',
                set: [{ val: 'primary1' }],
                promoBadges: [],
                categoryImage: [
                  {
                    url:
                      'https://res.cloudinary.com/tcp-dam-test/image/upload/v1558543115/ecom/assets/content/tcp/us/home/moduled/US-HP-050519-MINIME1_h9cwcd.jpg',
                    alt: 'Image Alt text attribute value',
                    title: 'Image Title attribute value',
                    crop_d: 'c_crop,g_face:center,q_auto:best,w_393',
                    crop_t: 'c_crop,g_face:center,q_auto:best,w_932',
                    crop_m: 'c_crop,g_face:center,q_auto:best,w_961',
                  },
                ],
                sizesRange: [
                  {
                    text: 'SIZES 4-161',
                  },
                ],
                categoryLayouts: [],
              },
            },
          },
        ],
      },
    },
  };
  beforeEach(() => {
    const props = {
      navigation: {
        navigate: jest.fn(),
      },
      navigationMenu: navItems,
      isEnabled: false,
    };
    component = shallow(<CategoryCarousel {...props} />);
  });
  it('should be defined', () => {
    expect(component).toBeDefined();
  });
  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
  it('should call item renderer', () => {
    const props = {
      navigation: {
        navigate: jest.fn(),
      },
      navigationMenu: navItems,
      isEnabled: true,
    };
    component = shallow(<CategoryCarousel {...props} />);
    expect(component).toBeDefined();
  });
  it('should render component when itemRenderer is called', () => {
    const item = component.instance().itemRenderer({ item: navItems.data.navigation.nav[0] });
    expect(item).toMatchSnapshot();
  });
  it('should call isGiftCardLink fn', () => {
    const item = component
      .instance()
      .isGiftCardLink({ item: navItems.data.navigation.nav[0] }, 'Girl');
    expect(item).toBeFalsy();
  });
  it('should call getSizeInfo fn', () => {
    const item = component
      .instance()
      .getSizeInfo(navItems.data.navigation.nav[0].categoryContent.mainCategory);
    expect(item).toEqual('SIZES 4-161');
  });
});
