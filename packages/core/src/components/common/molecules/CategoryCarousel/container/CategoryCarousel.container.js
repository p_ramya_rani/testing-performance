// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { trackClick, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import { getIsCategoryCarouselEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import CategoryCarousel from '../views';

const mapStateToProps = state => {
  return {
    navigationMenu: (state.Navigation && state.Navigation.navigationData) || [],
    accessibilityLabels:
      (state.Labels && state.Labels.global && state.Labels.global.accessibility) || {},
    isEnabled: getIsCategoryCarouselEnabled(state),
  };
};

function mapDispatchToProps(dispatch) {
  return {
    trackNavigationClick: payload => {
      const { analyticsData, clickData } = payload;
      dispatch(setClickAnalyticsData(analyticsData));
      dispatch(trackClick(clickData));
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryCarousel);
