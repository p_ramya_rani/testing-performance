// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import { getBrand, isGymboree } from '@tcp/core/src/utils/utils';
import { getDefaultAccentColor } from '@tcp/core/src/utils';
import { BodyCopy, DamImage } from '../../../atoms';
import {
  Wrapper,
  ImageTouchableOpacity,
  TextWrapper,
  Container,
} from '../styles/CategoryCarousel.style.native';
import constants from '../container/CategoryCarousel.constants';
import NavMenuLevel1Constants from '../../../../../../../mobileapp/src/components/features/content/Navigation/molecules/NavMenuLevel1/NavMenuLevel1.constants';

class CategoryCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.isGymboree = isGymboree();
    this.defaultAccentColor = getDefaultAccentColor(this.isGymboree);
  }

  /**
   * @function isGiftCardLink
   * This function will be used to find whether its gift card link or not.
   */
  isGiftCardLink = ({ item }, name = '') => {
    const nameToCheck = name.toUpperCase().replace(/\s/g, '');
    return (
      NavMenuLevel1Constants.GIFT_CARD_NAME.includes(nameToCheck) ||
      NavMenuLevel1Constants.GIFT_CARD_CATEGORY_ID === item.categoryId
    );
  };

  /**
   * @function trackNavClick
   * This will be used to trigger the Analytics for L1 Menu.
   */
  trackNavClick = (key, itemname) => {
    const { trackNavigationClick } = this.props;
    const analyticsData = { pageNavigationText: `Topmenu-${itemname}`, customEvents: ['event82'] };
    const clickData = { name: key, module: 'global' };
    trackNavigationClick({ analyticsData, clickData });
  };

  getAttributeList = cmsAttributeSet => {
    const attributeList = {};
    if (cmsAttributeSet) {
      cmsAttributeSet.forEach(attribute => {
        attributeList[attribute.key] = attribute.val;
      });
    }
    return attributeList;
  };

  /**
   * @function ShowL2Navigation populates the L2 menu for the L1 link that has been clicked
   * @param {object} item Details of the L1 menu item that has been clicked
   */
  showL2Navigation = (item, name) => {
    const {
      navigation: { navigate },
      accessibilityLabels,
    } = this.props;

    const {
      item: {
        categoryContent: { mainCategory },
      },
    } = item;

    const accentColor = this.getAccentColor(mainCategory);

    this.trackNavClick('navigation_l1', name);
    if (this.isGiftCardLink(item, name)) {
      return navigate('GiftCardPage', {
        title: name,
      });
    }
    return navigate('NavMenuLevel2', {
      navigationObj: item,
      l1Title: name,
      accessibilityLabels,
      resetTabStack: true,
      backTo: 'Home',
      trackNavClickAction: this.trackNavClick,
      accentColor,
    });
  };

  /**
   * This will be used to get the Size info
   */

  getSizeInfo = mainCategory => {
    let size = '';
    const sizesRange = mainCategory && mainCategory.sizesRange && mainCategory.sizesRange[0];
    if (sizesRange) {
      size = sizesRange.text;
    }

    return size;
  };

  /**
   * This will be used to get the image URL
   */

  getImageURL = url => {
    let imageUrl = '';
    if (url) {
      return url;
    }
    imageUrl = this.isGymboree
      ? '/ecom/assets/content/tcp/us/app/clearance_circular.png'
      : '/ecom/assets/content/tcp/us/app/clearance_4_.jpg';
    return imageUrl;
  };

  /**
   * This will be used to get the accent color
   */

  getAccentColor = mainCategory => {
    const { set } = mainCategory;
    let accentColor = '';

    const attributeList = this.getAttributeList(set);
    accentColor = attributeList.accentColor || this.defaultAccentColor;

    return accentColor;
  };

  /**
   * @function itemRenderer
   * This function will be used to render flat list item
   */
  itemRenderer = item => {
    const {
      item: {
        categoryContent: { displayToCustomer, name },
      },
    } = item;
    let {
      item: {
        categoryContent: { mainCategory },
      },
    } = item;

    if (!mainCategory) {
      mainCategory = {
        categoryImage: [],
        set: [],
      };
    }
    const { categoryImage } = mainCategory;
    if (!displayToCustomer) return null;
    const brandId = getBrand();
    const catSize = this.getSizeInfo(mainCategory);
    const imageObj = (categoryImage && categoryImage[0]) || {};
    const imageWidth = constants[`${brandId && brandId.toUpperCase()}_IMAGE_WIDTH`];
    const imageHeight = constants[`${brandId && brandId.toUpperCase()}_IMAGE_HEIGHT`];
    const imageUrl = this.getImageURL(imageObj.url);
    const accentColor = this.getAccentColor(mainCategory);
    const ImageStyle = {
      backgroundColor: accentColor,
      width: imageWidth,
      height: imageHeight,
    };
    return (
      <ImageTouchableOpacity onPress={() => this.showL2Navigation(item, name)}>
        <Container width={imageWidth}>
          <DamImage
            alt={imageObj.alt}
            url={imageUrl}
            resizeMode="cover"
            width={imageWidth}
            height={imageHeight}
            isFastImage={imageObj.url || false}
            {...(this.isGymboree ? { borderRadius: imageWidth } : '')}
            customStyle={ImageStyle}
          />
          <TextWrapper>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs12"
              fontWeight="extrabold"
              textAlign="center"
              text={name}
              color="BLACK"
            />
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs10"
              lineHeight="16px"
              fontWeight="medium"
              textAlign="center"
              text={catSize}
              color="BLACK"
            />
          </TextWrapper>
        </Container>
      </ImageTouchableOpacity>
    );
  };

  render() {
    const { navigationMenu = [], isEnabled } = this.props;
    if (!navigationMenu.length || !isEnabled) {
      return null;
    }
    return (
      <Wrapper>
        <FlatList
          initialNumToRender={3}
          initialScrollIndex={0}
          refreshing
          data={navigationMenu}
          horizontal
          showsHorizontalScrollIndicator={false}
          listKey={(_, index) => index.toString()}
          renderItem={this.itemRenderer}
        />
      </Wrapper>
    );
  }
}

CategoryCarousel.propTypes = {
  navigationMenu: PropTypes.shape([]).isRequired,
  accessibilityLabels: PropTypes.shape({}),
  navigation: PropTypes.func,
  trackNavigationClick: PropTypes.func,
  isEnabled: PropTypes.bool.isRequired,
};

CategoryCarousel.defaultProps = {
  accessibilityLabels: {},
  navigation: () => {},
  trackNavigationClick: () => {},
};

export default CategoryCarousel;
export { CategoryCarousel as CategoryCarouselVanilla };
