// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import CollapsibleContainer from '@tcp/core/src/components/common/molecules/CollapsibleContainer';
import StoreAddressTile from '@tcp/core/src/components/common/molecules/StoreAddressTile';
import { propTypes } from '@tcp/core/src/components/common/molecules/StoreAddressTile/views/prop-types';
import { Row, Col, BodyCopy } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import style, { collapsibleStyles, tileStyles } from '../styles/StoreLocations.style';

export const CollapsibleLocations = withStyles(CollapsibleContainer, collapsibleStyles);
export const LocationTile = withStyles(StoreAddressTile, tileStyles);

class StoreLocations extends PureComponent {
  getCollapsibleHeader() {
    const { labels } = this.props;
    return (
      <BodyCopy
        className="collapsible-header-text"
        fontSize="fs12"
        component="span"
        fontWeight="extrabold"
        color="text.primary"
        fontFamily="secondary"
      >
        {getLabelValue(labels, 'lbl_storedetails_locations_title')}
      </BodyCopy>
    );
  }

  getCollapsibleContent() {
    const { stores, children } = this.props;
    return (
      <div className="collapsible-content">
        {stores.map((store) => this.getAddressTitle(store))}
        {children}
      </div>
    );
  }

  getAddressTitle(store) {
    const { labels, openStoreDetails, type } = this.props;
    const tileLabels = {
      ...labels,
      lbl_storelanding_getdirections_link: getLabelValue(
        labels,
        'lbl_storedetails_locations_details_btn'
      ),
    };
    return (
      <LocationTile
        labels={tileLabels}
        store={store}
        isPageTitle
        openStoreDirections={(event) => openStoreDetails(event, store)}
        locatorGetDirections="open-store-details"
        type={type}
      />
    );
  }

  render() {
    const { children, className, stores, labels } = this.props;

    return (
      <div className={className}>
        <div className="location__lg">
          <BodyCopy
            className="locations-title"
            fontSize="fs16"
            component="h2"
            fontWeight="extrabold"
          >
            {getLabelValue(labels, 'lbl_storedetails_locations_title')}
          </BodyCopy>
          <Row fullBleed>
            {stores.map((store) => (
              <Col key={store.basicInfo.storeName} colSize={{ small: 6, medium: 4, large: 3 }}>
                {this.getAddressTitle(store)}
              </Col>
            ))}
          </Row>
        </div>
        <div className="location__sm">
          <CollapsibleLocations
            header={this.getCollapsibleHeader()}
            body={this.getCollapsibleContent()}
          />
        </div>

        {children}
      </div>
    );
  }
}

StoreLocations.propTypes = {
  className: PropTypes.string.isRequired,
  children: PropTypes.arrayOf(PropTypes.node),
  labels: PropTypes.shape({
    lbl_storedetails_locations_details_btn: PropTypes.string,
    lbl_storedetails_locations_title: PropTypes.string,
  }).isRequired,
  stores: PropTypes.arrayOf(PropTypes.shape(propTypes.store)),
  openStoreDetails: PropTypes.func,
  type: PropTypes.string,
};

StoreLocations.defaultProps = {
  children: null,
  stores: [],
  openStoreDetails: null,
  type: '',
};

export default withStyles(StoreLocations, style);
