// 9fbef606107a605d69c0edbcd8029e5d 
import tileLabels from '../../StoreAddressTile/__mocks__/labels.mock';

const labels = {
  StoreLocator: {
    ...tileLabels.StoreLocator,
    lbl_storedetails_locations_details_btn: 'See store details',
    lbl_storedetails_locations_title: 'Other locations near you',
    lbl_storedetails_locations_more_store: 'See more stores',
  },
};

export default labels;
