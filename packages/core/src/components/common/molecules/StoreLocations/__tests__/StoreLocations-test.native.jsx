// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import StoreLocations from '../views/StoreLocations.native';
import labelsMock from '../__mocks__/labels.mock';

describe('StoreLocations component', () => {
  it('StoreLocations component renders correctly without props', () => {
    const component = shallow(<StoreLocations labels={labelsMock.StoreLocator} />);
    expect(component).toMatchSnapshot();
  });
});
