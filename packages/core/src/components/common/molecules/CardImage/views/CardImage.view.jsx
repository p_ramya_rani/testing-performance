// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getIconPath } from '../../../../../utils';
import { cardIconMapping } from '../../../../features/account/common/molecule/CardTile/views/CardTile.utils';
import BodyCopy from '../../../atoms/BodyCopy';
import withStyles from '../../../hoc/withStyles';
import CardStyle from '../../Card/Card.style';
import constants from '../../../../features/CnC/Checkout/organisms/BillingPaymentForm/container/CreditCard.constants';

const CardImage = ({ card, cardNumber, afterPayInProgress }) => {
  let cardTopMargin = 'elem-mt-XL';
  if (card.defaultInd) {
    cardTopMargin = 'elem-mt-XS';
  } else if (
    card.ccType !== constants.ACCEPTED_CREDIT_CARDS.GIFT_CARD &&
    card.ccType !== constants.ACCEPTED_CREDIT_CARDS.VENMO
  ) {
    cardTopMargin = 'elem-mt-MED';
  }
  const cardIcon = afterPayInProgress
    ? getIconPath(cardIconMapping.afterpay)
    : getIconPath(cardIconMapping[card.ccBrand]);
  return (
    <>
      <div className={`cardImage-wrapper ${cardTopMargin}`}>
        <div className={`cardImage-img-wrapper ${afterPayInProgress ? 'afterpay-box' : ''}`}>
          <img
            className={`cardImage-img ${afterPayInProgress ? 'cardImage-img-afterpay' : ''}`}
            data-locator="cardLogo"
            alt={card.ccType}
            src={cardIcon}
          />
        </div>
        <BodyCopy component="p" fontFamily="secondary" className="cardImage-card-number">
          {card.ccType === constants.ACCEPTED_CREDIT_CARDS.APPLE || afterPayInProgress
            ? ''
            : cardNumber}
        </BodyCopy>
      </div>
    </>
  );
};

CardImage.propTypes = {
  card: PropTypes.shape({}),
  cardNumber: PropTypes.string,
  afterPayInProgress: PropTypes.bool,
};
CardImage.defaultProps = {
  cardNumber: '',
  card: '',
  afterPayInProgress: false,
};
export default withStyles(CardImage, CardStyle);
export { CardImage as CardImageVanilla };
