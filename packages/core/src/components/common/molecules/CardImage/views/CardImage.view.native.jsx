// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getIconCard } from '@tcp/core/src/utils/index.native';
import { cardIconMapping } from '../../../../features/account/common/molecule/CardTile/views/CardTile.utils';
import BodyCopy from '../../../atoms/BodyCopy';
import Image from '../../../atoms/Image';
import { Container, ImageWrapper } from '../styles/CardImage.style';

const CardImage = ({ card, cardNumber, fontSize = 'fs16', afterPayInProgress }) => {
  const cardIcon = afterPayInProgress
    ? getIconCard('after-pay-icon')
    : getIconCard(cardIconMapping[card.ccBrand]);
  return (
    <Container>
      <ImageWrapper afterPayInProgress={afterPayInProgress}>
        <Image source={cardIcon} alt={card.ccBrand} width="47" height="30" dataLocator="cardLogo" />
      </ImageWrapper>
      <BodyCopy fontSize={fontSize} fontFamily="secondary" text={cardNumber} />
    </Container>
  );
};

CardImage.propTypes = {
  card: PropTypes.shape({}),
  cardNumber: PropTypes.string,
  fontSize: PropTypes.string,
  afterPayInProgress: PropTypes.bool,
};
CardImage.defaultProps = {
  cardNumber: '',
  card: '',
  fontSize: '',
  afterPayInProgress: false,
};

export default CardImage;
