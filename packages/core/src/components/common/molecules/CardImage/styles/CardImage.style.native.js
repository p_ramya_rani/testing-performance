// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export const Container = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const ImageWrapper = styled.View`
  border: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
  border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  background-color: ${(props) =>
    props.afterPayInProgress ? props.theme.colors.AFTERPAY.MINT : props.theme.colors.WHITE};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  ${(props) =>
    props.afterPayInProgress
      ? `padding-left: ${props.theme.spacing.ELEM_SPACING.XXS};
         width:  56px;`
      : ''}
`;
