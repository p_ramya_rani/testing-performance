// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable no-console */
/* eslint-disable react-native/split-platform-components */
/* eslint-disable no-unused-vars */
import React from 'react';
import { Linking, Platform, BackHandler, NativeModules } from 'react-native';
import logger from '@tcp/core/src/utils/loggerInstance';
import DeviceInfo from 'react-native-device-info';
import {
  getScreenWidth,
  getScreenHeight,
  setValueInAsyncStorage,
  getValueFromAsyncStorage,
  isAndroid,
  isIOS,
} from '@tcp/core/src/utils/utils.app';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { getAppVersion } from '@tcp/core/src/utils';
import ModalNative from '../../Modal/view/Modal.native';
import {
  Wrapper,
  Container,
  StyledImage,
  Touchable,
  StyledBodyCopy,
  StyledButton,
  StyledAnchor,
  MessageContainer,
  ShadowContainer,
  StyledLine,
  StyledBodyCopyDescription,
  SafeAreaViewStyle,
} from '../styles/AppUpdatePrompt.style.native';
import { getPixelRatio } from '../../../../../utils/utils.app';

const closeImage = require('../../../../../../src/assets/close.png');
const updateAppLifestyle = require('../../../../../../src/assets/update-app-lifestyle.png');

const { RNRestart } = NativeModules;

/**
 * Module height and width.
 */
const WIDTH = getScreenWidth();
const HEIGHT = getScreenHeight();
const LAST_VERSION_UPDATE_KEY = 'last-latest-update';

class AppUpdatePrompt extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { isModalOpen: false, isSoftUpdate: false, modelName: null };
  }

  /**
   * @componentDidMount : Load app version in this function
   */
  componentDidMount() {
    DeviceInfo.getDeviceName().then(modelName => {
      this.setState({ modelName });
    });

    try {
      const { getAppUpdateInfo } = this.props;
      getAppUpdateInfo();
    } catch (err) {
      logger.error(err);
    }
  }

  /**
   *
   * @componentDidUpdate : In this function check app update status
   */
  componentDidUpdate(prevProps, prevState) {
    try {
      const { modalActionComplete } = this.state;
      const {
        appVersion: { iosAppVersion, androidAppVersion },
        onModalActionComplete,
      } = this.props;
      const {
        appVersion: {
          iosAppVersion: previousIosAppVersion,
          androidAppVersion: previousAndroidAppVersion,
        },
      } = prevProps;
      const previousAppVersion =
        Platform.OS === 'ios' ? previousIosAppVersion : previousAndroidAppVersion;
      const latestVersion = this.getLatestAppVersion();
      if (latestVersion && previousAppVersion !== latestVersion) {
        this.getUpdateStatus(latestVersion);
      }

      if (modalActionComplete && prevState.modalActionComplete !== modalActionComplete) {
        onModalActionComplete();
      }
    } catch (err) {
      logger.error(err);
    }
  }

  getLatestAppVersion = () => {
    const {
      appVersion: { iosAppVersion, androidAppVersion },
    } = this.props;
    return Platform.OS === 'ios' ? iosAppVersion : androidAppVersion;
  };

  /**
   * @toggleModal : To manage the modal state .
   */
  toggleModal = () => {
    const { isModalOpen } = this.state;
    this.setState({
      isModalOpen: !isModalOpen,
      modalActionComplete: !!isModalOpen,
    });
  };

  /*
   * @updateApp : To update app version in android and iOS
   */
  updateApp = () => {
    this.openMarketStore();
  };

  /**
   * @method openMarketStore
   * @description - Open OS specific app update
   */
  openMarketStore = () => {
    try {
      const {
        appVersion: { iOSAppUpdateUrl, androidAppUpdateUrl },
      } = this.props;
      if (Platform.OS === 'ios' && iOSAppUpdateUrl) {
        Linking.openURL(iOSAppUpdateUrl);
      } else if (Platform.OS === 'android' && androidAppUpdateUrl) {
        Linking.openURL(androidAppUpdateUrl);
      }
    } catch (err) {
      this.setState({
        isModalOpen: false,
      });
    }
  };

  /**
   * To check if app update is required or not
   */
  getUpdateStatus = latestVersion => {
    try {
      const currentAppVersion = getAppVersion();
      const splitLatest = this.formatLatestVersion(latestVersion.replace(/"/g, ''))
        .trim()
        .split('.');
      const currentSplit = currentAppVersion.split('.');
      const radixParams = 10;

      if (parseInt(splitLatest[0], radixParams) > parseInt(currentSplit[0], radixParams)) {
        // Hard Update Requires
        this.toggleModal();
      } else if (
        parseInt(splitLatest[0], radixParams) === parseInt(currentSplit[0], radixParams) &&
        (parseInt(splitLatest[1], radixParams) > parseInt(currentSplit[1], radixParams) ||
          parseInt(splitLatest[2], radixParams) > parseInt(currentSplit[2], radixParams))
      ) {
        this.checkForSoftUpdate(latestVersion);
      } else {
        this.setState({ modalActionComplete: true });
      }
    } catch (err) {
      this.setState({
        isModalOpen: false,
      });
    }
  };

  checkForSoftUpdate = latestVersion => {
    getValueFromAsyncStorage(LAST_VERSION_UPDATE_KEY).then(data => {
      if (data !== latestVersion) {
        // Soft Update Required
        this.setState({ isSoftUpdate: true });
        this.toggleModal();
      } else {
        this.setState({ modalActionComplete: true });
      }
    });
  };

  /**
   * Format app version
   */

  formatLatestVersion = latestVersion => {
    if (latestVersion.match(/\d(\.\d){2}/)) {
      return latestVersion;
    }
    let newLatestVersion = latestVersion;
    newLatestVersion += '.0';
    return this.formatLatestVersion(newLatestVersion);
  };

  /*
   * @updateAppLater : To update app version in android and iOS
   */
  updateAppLater = () => {
    this.closeModal();
  };

  /**
   * @requestPermission : To close the modal in android and ios .
   */
  closeModal = () => {
    const { isSoftUpdate } = this.state;
    const latestVersion = this.getLatestAppVersion;
    this.toggleModal();
    setValueInAsyncStorage(LAST_VERSION_UPDATE_KEY, latestVersion);
    if (isAndroid() && isSoftUpdate === false) {
      BackHandler.exitApp();
      RNRestart.Restart();
    }
  };

  render() {
    const { labels } = this.props;
    const { isModalOpen, isSoftUpdate, modelName } = this.state;
    let imageHeight = getPixelRatio() === 'xhdpi' ? '275px' : '320px';
    if (modelName && modelName.includes('Samsung Galaxy S7')) {
      imageHeight = '245px';
    } else if (modelName === 'iPhone X') {
      imageHeight = '290px';
    }

    return labels && Object.keys(labels).length ? (
      <ModalNative
        backButtonClose={false}
        isOpen={isModalOpen}
        onRequestClose={this.closeModal}
        customTransparent
      >
        <SafeAreaViewStyle>
          <Container height={HEIGHT}>
            <StyledImage
              source={updateAppLifestyle}
              width={WIDTH}
              height={imageHeight}
              position="absolute"
              bottom="0"
              resizeMode="cover"
            />

            {isSoftUpdate ? (
              <Touchable accessibilityRole="button" onPress={this.closeModal}>
                <StyledImage source={closeImage} width="15px" height="15px" tintColor="black" />
              </Touchable>
            ) : null}

            <MessageContainer>
              <StyledBodyCopy
                text={getLabelValue(labels, 'lbl_appUpdate_title', 'AppUpdateModal', 'global')}
                textAlign="center"
                fontWeight="black"
                fontFamily="primary"
                fontSize="fs24"
                margin="15px 70px 0px 70px"
              />

              <StyledLine height="2" width="103" />

              <StyledBodyCopyDescription
                text={getLabelValue(labels, 'lbl_appUpdate_body', 'AppUpdateModal', 'global')}
                textAlign="center"
                margin="32px 15px 15px 15px"
                fontFamily="secondary"
                fontSize="fs14"
              />

              <StyledButton
                text={getLabelValue(labels, 'lbl_appUpdate_update', 'AppUpdateModal', 'global')}
                marginTop="15px"
                width="225px"
                height="42px"
                backgroundColor="#2e6a91"
                color="#f9f9f9"
                onPress={this.updateApp}
              />

              {isSoftUpdate ? (
                <StyledButton
                  text={getLabelValue(labels, 'lbl_appUpdate_later', 'AppUpdateModal', 'global')}
                  marginTop="22px"
                  width="225px"
                  backgroundColor="#f9f9f9"
                  color="#4a4a4a"
                  onPress={this.updateAppLater}
                />
              ) : null}
            </MessageContainer>
          </Container>
        </SafeAreaViewStyle>
      </ModalNative>
    ) : null;
  }
}

AppUpdatePrompt.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  getAppUpdateInfo: PropTypes.func.isRequired,
  appVersion: PropTypes.string.isRequired,
  onModalActionComplete: PropTypes.func,
};

AppUpdatePrompt.defaultProps = {
  onModalActionComplete: () => {},
};

export default AppUpdatePrompt;
export { AppUpdatePrompt as AppUpdatePromptVanilla };
