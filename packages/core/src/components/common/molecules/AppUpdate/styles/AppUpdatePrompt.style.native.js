// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { Image, BodyCopy, Button, Anchor } from '../../../atoms';

export const Wrapper = styled.View`
  align-items: center;
  background-color: ${props => props.theme.colorPalette.white};
  height: 290px;
  width: ${props => props.width};
  padding: 12px;
`;

export const Container = styled.View`
  background-color: ${props => props.theme.colors.TEXT.PALEGRAY};
  height: ${props => props.height}px;
`;

export const StyledLine = styled.View`
  height: ${props => props.height}px;
  width: ${props => props.width}px;
  background-color: #439ad4;
  margin-top: 16px;
`;

export const StyledImage = styled(Image)`
  /* stylelint-disable-next-line */
  ${props => (props.marginTop ? `margin-top: ${props.marginTop}` : ``)};
  ${props => (props.tintColor ? `tint-color: ${props.tintColor}` : ``)};
  ${props => (props.position ? `position: ${props.position}` : ``)};
  ${props => (props.bottom ? `bottom: ${props.bottom}` : ``)};
`;

export const StyledBodyCopy = styled(BodyCopy)`
  ${props => (props.marginTop ? `margin-top: ${props.marginTop}` : ``)};
`;

export const StyledBodyCopyDescription = styled(BodyCopy)`
  ${props => (props.marginTop ? `margin-top: ${props.marginTop}` : ``)};
  font-weight: normal;
  font-style: normal;
  text-align: center;
`;

export const Touchable = styled.TouchableOpacity`
  right: 0;
  padding-top: 20px;
  padding-right: 20px;
  position: absolute;
`;

export const StyledButton = styled(Button)`
  ${props => (props.marginTop ? `margin-top: ${props.marginTop}` : ``)};
  ${props => (props.marginTop ? `margin-top: ${props.marginTop}` : ``)};
  ${props => (props.backgroundColor ? `background-color: ${props.backgroundColor}` : ``)};
  ${props => (props.color ? `color: ${props.color}` : ``)};
`;

export const StyledAnchor = styled(Anchor)`
  ${props => (props.marginTop ? `margin-top: ${props.marginTop}` : ``)};
  ${props => (props.height ? `height: ${props.height}` : ``)};
`;

export const MessageContainer = styled.View`
  align-items: center;
  justify-content: center;
  margin-left: 12px;
  margin-right: 12px;
  margin-top: 20px;
`;

export const ShadowContainer = styled.View`
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.6);
  height: ${props => props.height}px;
`;

export const SafeAreaViewStyle = styled.SafeAreaView`
  flex: 1;
  background: ${props => props.theme.colors.TEXT.PALEGRAY};
`;

export default {
  Wrapper,
  Container,
  StyledImage,
  Touchable,
  StyledBodyCopy,
  StyledButton,
  StyledAnchor,
  MessageContainer,
  ShadowContainer,
  StyledLine,
  StyledBodyCopyDescription,
  SafeAreaViewStyle,
};
