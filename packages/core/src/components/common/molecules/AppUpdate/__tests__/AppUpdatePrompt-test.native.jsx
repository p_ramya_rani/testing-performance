// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import DeviceInfo from 'react-native-device-info';
import { AppUpdatePromptVanilla } from '../views/AppUpdatePrompt.native';

describe('AppUpdatePromptVanilla native component', () => {
  let component;
  const props = {
    appVersion: { iosAppVersion: '0.1.0', androidAppVersion: '0.1.0' },
    getAppUpdateInfo: jest.fn(),
    labels: {},
  };
  beforeEach(() => {
    DeviceInfo.getDeviceName = () => Promise.resolve('iPhone X');
    component = shallow(<AppUpdatePromptVanilla {...props} />);
  });

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot();
  });

  it('To check App version format with 2 digits', () => {
    const latestVersion = '1.0';
    expect(component.instance().formatLatestVersion(latestVersion)).toBe('1.0.0');
  });
  it('To check App version format with 3 digits', () => {
    const latestVersion = '1.0.0';
    expect(component.instance().formatLatestVersion(latestVersion)).toBe('1.0.0');
  });
  it('To verify App update is required', () => {
    const latestVersion = '1.0.0';
    DeviceInfo.getVersion = jest.fn().mockReturnValue('0.0.1');

    component.setState({ isModalOpen: false });
    component.instance().getUpdateStatus(latestVersion);
    expect(component.state('isModalOpen')).toBe(false);
  });
});
