// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';

import { APP_VERSION_KEY } from '@tcp/core/src/constants/reducer.constants';

export const getAppVersionState = state => {
  return state[APP_VERSION_KEY];
};

export const getAppVersionInfo = createSelector(
  getAppVersionState,
  AppVersion => {
    return AppVersion;
  }
);

export default {
  getAppVersionState,
  getAppVersionInfo,
};
