// 9fbef606107a605d69c0edbcd8029e5d 
import APP_VERSION_CONSTANTS from './AppVersion.constants';

export const appVersionRequest = () => {
  return {
    type: APP_VERSION_CONSTANTS.APP_VERSION_REQUEST,
  };
};

export const appVersionSuccess = payload => {
  return {
    payload,
    type: APP_VERSION_CONSTANTS.APP_VERSION_SUCCESS,
  };
};

export default {
  appVersionRequest,
  appVersionSuccess,
};
