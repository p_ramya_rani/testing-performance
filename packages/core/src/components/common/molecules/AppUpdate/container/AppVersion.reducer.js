// 9fbef606107a605d69c0edbcd8029e5d 
import APP_VERSION_CONSTANTS from './AppVersion.constants';

const AppVersionReducer = (state = {}, action = {}) => {
  if (action.type === APP_VERSION_CONSTANTS.APP_VERSION_SUCCESS) {
    return { ...state, ...action.payload };
  }
  return state;
};

export default AppVersionReducer;
