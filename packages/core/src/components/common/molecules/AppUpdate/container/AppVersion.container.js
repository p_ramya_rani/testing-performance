// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { appVersionRequest } from './AppVersion.actions';
import { getAppVersionInfo } from './AppVersion.selectors';
import AppUpdatePromptNative from '../views/AppUpdatePrompt.native';

export const mapDispatchToProps = dispatch => {
  return {
    getAppUpdateInfo: () => {
      dispatch(appVersionRequest());
    },
  };
};

const mapStateToProps = state => {
  return {
    appVersion: getAppVersionInfo(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppUpdatePromptNative);
