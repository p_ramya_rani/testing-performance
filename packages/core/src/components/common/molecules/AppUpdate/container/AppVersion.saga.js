// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import getAppVersionAbstractor from '@tcp/core/src/services/abstractors/common/getAppVersion';
import APP_VERSION_CONSTANTS from './AppVersion.constants';
import { appVersionSuccess } from './AppVersion.actions';

export function* getAppVersion() {
  try {
    const appVersionRes = yield call(getAppVersionAbstractor.getAppVersion);
    if (appVersionRes) {
      yield put(appVersionSuccess(appVersionRes));
    }
  } catch (err) {
    logger.error(err);
  }
}

function* getAppVersionSaga() {
  yield takeLatest(APP_VERSION_CONSTANTS.APP_VERSION_REQUEST, getAppVersion);
}

export default getAppVersionSaga;
