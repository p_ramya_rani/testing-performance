// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { goToPlpTab } from '@tcp/core/src/utils';
import ListItem from '../../../features/browse/ProductListing/molecules/ProductListItem';
import { getMapSliceForColorProductId } from '../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { getPromotionalMessage } from '../../../features/browse/ProductListing/molecules/ProductList/utils/utility';

const PRODUCT_IMAGE_WIDTH = 162;
// eslint-disable-next-line
const onAddToBag = data => {};

// eslint-disable-next-line
const onFavorite = item => {};

const onOpenPDPPageHandler = props => (pdpUrl, selectedColorIndex) => {
  const { title, navigation, page, viaModuleName, item } = props;
  const { actionId } = item || {};
  goToPlpTab(navigation);
  navigation.navigate('ProductDetail', {
    title,
    pdpUrl,
    selectedColorProductId: selectedColorIndex,
    reset: true,
    viaModuleName,
    viaModule: 'mbox_Recommendation',
    clickOrigin: page,
    actionId,
  });
};

const ModuleP = props => {
  const {
    isMatchingFamily,
    currencyExchange,
    currencySymbol,
    isPlcc,
    onQuickViewOpenClick,
    item,
    viaModule,
    labels,
    showPriceRange,
    page,
    paddings,
    portalValue,
    productImageWidth,
    isCardTypeTiles,
    isDynamicBadgeEnabled,
    setOpen = () => {},
    trackAnalyticsOnAddToBagButtonClick,
    ...otherProps
  } = props;
  const productWidth = productImageWidth || PRODUCT_IMAGE_WIDTH;
  const { colorsMap, productInfo } = item;
  const { promotionalMessage, promotionalPLCCMessage } = productInfo;
  const { colorProductId } = colorsMap[0];
  const colorsExtraInfo = colorsMap[0].color.name;
  // get default zero index color entry
  const curentColorEntry = getMapSliceForColorProductId(colorsMap, colorProductId);
  // get product color and price info of default zero index item
  const currentColorMiscInfo =
    colorsExtraInfo[curentColorEntry.color.name] || curentColorEntry.miscInfo || {};
  const { badge1, badge2 } = currentColorMiscInfo;
  // get default top badge data
  const topBadge = isMatchingFamily && badge1.matchBadge ? badge1.matchBadge : badge1.defaultBadge;

  // get default Loyalty message
  const loyaltyPromotionMessage = getPromotionalMessage(isPlcc, {
    promotionalMessage,
    promotionalPLCCMessage,
  });

  return (
    <ListItem
      paddings={paddings}
      item={item}
      isMatchingFamily={isMatchingFamily}
      badge1={topBadge}
      badge2={badge2}
      isPlcc={isPlcc}
      loyaltyPromotionMessage={loyaltyPromotionMessage}
      onAddToBag={onAddToBag}
      onFavorite={onFavorite}
      currencyExchange={currencyExchange}
      currencySymbol={currencySymbol}
      onGoToPDPPage={onOpenPDPPageHandler(props)}
      onQuickViewOpenClick={onQuickViewOpenClick}
      trackAnalyticsOnAddToBagButtonClick={trackAnalyticsOnAddToBagButtonClick}
      fullWidth
      productImageWidth={productWidth}
      viaModule={viaModule}
      portalValue={portalValue}
      labelsPlpTiles={labels}
      isSwipeEnable={false}
      showPriceRange={showPriceRange}
      fromPage={page}
      setOpen={setOpen}
      isDynamicBadgeEnabled={isDynamicBadgeEnabled}
      isCardTypeTiles={isCardTypeTiles}
      {...otherProps}
    />
  );
};

ModuleP.propTypes = {
  isMatchingFamily: PropTypes.bool,
  currencySymbol: PropTypes.string,
  paddings: PropTypes.string,
  currencyExchange: PropTypes.arrayOf(PropTypes.shape({})),
  isPlcc: PropTypes.bool,
  onQuickViewOpenClick: PropTypes.func,
  item: PropTypes.shape({}).isRequired,
  viaModule: PropTypes.string,
  labels: PropTypes.shape({
    lbl_plpTiles_shop_collection: PropTypes.string,
    lbl_add_to_bag: PropTypes.string,
  }).isRequired,
  showPriceRange: PropTypes.bool,
  page: PropTypes.string,
  isHidePLPAddToBag: PropTypes.bool,
  isRecommendations: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  portalValue: PropTypes.shape({}),
  trackAnalyticsOnAddToBagButtonClick: PropTypes.func,
  setOpen: PropTypes.func,
  productImageWidth: PropTypes.number,
  isCardTypeTiles: PropTypes.bool,
};

ModuleP.defaultProps = {
  onQuickViewOpenClick: () => {},
  currencyExchange: [{ exchangevalue: 1 }],
  isMatchingFamily: true,
  isPlcc: false,
  paddings: '12px 0 12px 0',
  productImageWidth: 0,
  isCardTypeTiles: false,
  currencySymbol: '$',
  viaModule: '',
  showPriceRange: false,
  page: '',
  isHidePLPAddToBag: false,
  isRecommendations: false,
  isDynamicBadgeEnabled: {},
  portalValue: {},
  trackAnalyticsOnAddToBagButtonClick: () => {},
  setOpen: () => {},
};

export default ModuleP;
