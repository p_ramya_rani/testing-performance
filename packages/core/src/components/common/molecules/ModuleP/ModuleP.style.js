// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  ${(props) =>
    props.inGridPlpRecommendation !== true
      ? `
      .item-container-inner {
        height: ${props.isPromoAvailable ? '417px' : 'auto'};
        @media ${props.theme.mediaQuery.medium} {
          height: ${props.isPromoAvailable ? '521px' : 'auto'};
        }
        @media ${props.theme.mediaQuery.large} {
          height: ${props.isPromoAvailable ? '535px' : 'auto'};
        }
      }
      margin: 0 10px;
      .product-image-container {
        height: 186px;
        width: 100%;
        a {
          height: 100%;
          display: block;
        }
        img {
          height: auto;
          width: 100%;
          top: 50%;
          left: 50%;
          transform: translate3d(-50%, -50%, 0);
        }
      }
      .container-price {
        text-align: left;
        min-height: 40px;
      }
      @media ${props.theme.mediaQuery.medium} {
        .product-image-container {
          height: 267px;
        }
      }
      @media ${props.theme.mediaQuery.large} {
        .product-image-container {
          height: 267px;
        }
      }
    `
      : ``}
`;
