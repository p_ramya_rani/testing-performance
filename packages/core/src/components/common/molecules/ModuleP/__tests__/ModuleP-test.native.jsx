// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ModuleP from '../ModuleP.native';

const props = {
  key: 'test',
  loadedProductCount: 4,
  generalProductId: 'sfsdsdgsfgds',
  item: {
    productInfo: {},
    miscInfo: {},
    colorsMap: [
      {
        color: { name: '' },
        colorProductId: '',
        miscInfo: {
          badge1: { matchBadge: '' },
        },
      },
    ],
    imagesByColor: {},
    sqnNmbr: 123,
  },
  isPLPredesign: false,
  productsBlock: {},
  onPickUpOpenClick: () => {},
  className: 'test',
  labels: {},
  sequenceNumber: 12312,
};

describe('ModuleP component', () => {
  it('ModuleP component renders correctly with props', () => {
    const component = shallow(<ModuleP {...props} />);
    expect(component).toMatchSnapshot();
  });
});
