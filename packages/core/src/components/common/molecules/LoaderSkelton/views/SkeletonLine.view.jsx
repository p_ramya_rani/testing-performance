// 9fbef606107a605d69c0edbcd8029e5d 
/* istanbul ignore file */
import React from 'react';
import styled from 'styled-components';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';

const WrappedComponent = styled.div`
  display: ${props => (!props.isGiftCard ? 'inline-block' : 'none')};
  height: ${props => (props.height ? props.height : '100%')};
  width: ${props => (props.width ? props.width : '100%')};
  min-height: ${props => (props.height ? props.height : '15px')};
  background-color: #d8d8d8;
  overflow: hidden;
  position: relative;
  &:before {
    content: '';
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: -500px;
    background-image: linear-gradient(
      90deg,
      rgba(255, 255, 255, 0),
      rgba(255, 255, 255, 0.6),
      rgba(255, 255, 255, 0)
    );
    animation: progress 0.5s ease-in-out infinite;
    @keyframes progress {
      0% {
        left: -500px;
      }
      100% {
        left: 500px;
      }
    }
  }
  ${props => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

const SkeletonLine = props => <WrappedComponent {...props} />;

export default errorBoundary(SkeletonLine);
