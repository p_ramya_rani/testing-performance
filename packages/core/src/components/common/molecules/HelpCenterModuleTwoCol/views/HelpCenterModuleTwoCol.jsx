// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import { PropTypes } from 'prop-types';
import placeholder from '@tcp/core/src/components/common/molecules/Placeholder';
import divisionTabs from '@tcp/core/src/components/common/molecules/HelpTabs';
import accordion from '@tcp/core/src/components/common/molecules/AccordionModule';
import moduleX from '@tcp/core/src/components/common/molecules/ModuleX';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import withStyles from '../../../hoc/withStyles';
import errorBoundary from '../../../hoc/withErrorBoundary';
import style from '../styles/HelpCenterModuleTwoCol.style';

const modules = { placeholder, divisionTabs, accordion, moduleX };

function DynamicColumns(properties) {
  const { slot: slots, ...others } = properties;
  return (
    slots &&
    slots
      .filter(
        (slot) =>
          slot &&
          (slot.moduleName === 'placeholder' ||
            slot.moduleName === 'divisionTabs' ||
            slot.moduleName === 'accordion' ||
            slot.moduleName === 'moduleX')
      )
      .map((slotData) => {
        const Module = modules[slotData.moduleName];
        const largeColSize = slotData.moduleName === 'accordion' ? 10 : 9;
        const colOffset =
          slotData.moduleName === 'accordion' ? {} : { large: 1, small: 0, medium: 0 };
        return (
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: slotData.moduleName === 'placeholder' ? 2 : largeColSize,
            }}
            offsetRight={slotData.moduleName === 'placeholder' ? {} : colOffset}
          >
            <Module halfWidth {...slotData} {...others} />
          </Col>
        );
      })
  );
}

const HelpCenterModuleTwoCol = (props) => {
  const { className } = props;

  return (
    <Row className={`${className} helpCenterModuleTwoCol`}>
      <DynamicColumns {...props} />
    </Row>
  );
};

HelpCenterModuleTwoCol.propTypes = {
  className: PropTypes.string.isRequired,
};
export default withStyles(errorBoundary(HelpCenterModuleTwoCol), style);
export { HelpCenterModuleTwoCol as HelpCenterModuleTwoColVanilla };
