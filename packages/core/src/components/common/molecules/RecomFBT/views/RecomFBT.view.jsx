// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import FBTAddToBag from '@tcp/core/src/components/common/molecules/FBTAddToBag/container/FBTAddToBag.container';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getCartItemInfo } from '@tcp/core/src/components/features/CnC/AddedToBag/util/utility';
import { routerPush, getIconPath } from '@tcp/core/src/utils';
import {
  getMapSliceForColor,
  getMapSliceForColorProductId,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { isGymboree } from '@tcp/core/src/utils/utils';

import ProductImagesWrapper from '../../../../features/browse/ProductDetail/molecules/ProductImagesWrapper/views/ProductImagesWrapper.view';
import ProductFBT from '../../../../features/browse/ProductDetail/molecules/Product/views/ProductFBT.view';
import withStyles from '../../../hoc/withStyles';
import style from '../styles/RecomFBT.style';

class RecomStyleWith extends PureComponent {
  constructor(props) {
    super(props);
    const {
      productItem: { colorFitsSizesMap, generalProductId, offerPrice },
    } = this.props;

    this.state = {
      currentColorEntry: getMapSliceForColorProductId(colorFitsSizesMap, generalProductId) || {},
      currentGiftCardValue: offerPrice,
    };
    const { currentColorEntry } = this.state;
    const colorName = currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;

    this.formValues = {
      Fit: '',
      Size: '',
      color: colorName,
      Quantity: 1,
    };
  }

  componentWillUnmount() {
    const { countUpdated, resetCountUpdate } = this.props;
    if (countUpdated === true) resetCountUpdate();
  }

  handleAddToBag = (selectedSizeValue) => {
    const { addToBagEcomAction, productItem } = this.props;
    const productInfo = productItem;
    const fromFBT = true;
    const formVals = {
      fit: this.formValues.Fit,
      size: this.formValues.Size || selectedSizeValue,
      color: this.formValues.color,
      quantity: this.formValues.Quantity || 1,
    };

    let cartItemInfo = getCartItemInfo(productInfo, formVals, fromFBT);
    cartItemInfo = {
      ...cartItemInfo,
      fromStyleWith: true,
      fromFbt: true,
    };
    addToBagEcomAction(cartItemInfo);
  };

  onChangeColor = (e, selectedSize, selectedFit, selectedQuantity) => {
    const { productItem } = this.props;
    const { colorFitsSizesMap } = productItem;
    const { currentGiftCardValue } = this.state;
    this.setState({
      currentColorEntry: getMapSliceForColor(colorFitsSizesMap, e),
      currentGiftCardValue:
        (getMapSliceForColor(colorFitsSizesMap, e) &&
          getMapSliceForColor(colorFitsSizesMap, e).offerPrice) ||
        currentGiftCardValue,
    });
    this.formValues = {
      Fit: selectedFit,
      Size: selectedSize,
      color: e,
      Quantity: selectedQuantity,
    };
  };

  onChangeSize = (selectedColor, e, selectedFit, selectedQuantity) => {
    this.setState({ currentGiftCardValue: e });
    this.formValues = {
      Fit: selectedFit,
      Size: e,
      color: selectedColor,
      Quantity: selectedQuantity,
    };
  };

  navigateToPDP = (e, pdpToPath, currentColorPdpUrl) => {
    e.preventDefault();
    routerPush(pdpToPath, currentColorPdpUrl);
  };

  render() {
    const {
      productItem,
      currencySymbol,
      plpLabels,
      headerLabel,
      className,
      addToBagError,
      addToBagErrorId,
      isPDPSmoothScrollEnabled,
      addedToBagData,
      addedToBagGhostLoaderState,
      isComplete,
      updateFbtStatus,
      isFBTDisplayed,
    } = this.props;
    let imagesToDisplay = [];
    const { currentColorEntry } = this.state;
    const selectedColorProductId = currentColorEntry && currentColorEntry.colorProductId;
    const selectedColor =
      currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;
    const { imagesByColor } = productItem;

    imagesToDisplay = imagesByColor[selectedColor];

    const productData = {
      currentProduct: productItem,
    };
    return (
      <div className={`${className} ${isFBTDisplayed ? '' : 'is-hidden'}`}>
        <BodyCopy
          className="section-styled-heading"
          fontSize="fs16"
          component="h2"
          fontFamily="secondary"
          fontWeight="semibold"
          id="recommendation"
        >
          {headerLabel}
        </BodyCopy>
        <div className="fbt-contentbox">
          <div className="fbt-content-img">
            <BodyCopy component="div" textAlign="center">
              <ProductImagesWrapper
                productName=""
                isGiftCard={false}
                images={[
                  {
                    regularSizeImageUrl: imagesToDisplay && imagesToDisplay.basicImageUrl,
                  },
                ]}
                selectedColorProductId={selectedColorProductId}
                pdpLabels={plpLabels}
                currentProduct={productItem}
                isCompleteTheLookTestEnabled={false}
                isStyleWith
                isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
              />
              <div className="brand-image">
                <Image
                  src={
                    isGymboree()
                      ? getIconPath('header__brand-tab-gymboree')
                      : getIconPath(`header__brand-tab--tcp`)
                  }
                />
              </div>
            </BodyCopy>
          </div>
          <div className="fbt-content">
            <ProductFBT productDetails={productData} currencySymbol={currencySymbol} />
            <FBTAddToBag
              handleFormSubmit={this.handleAddToBag}
              errorOnHandleSubmit={addToBagError}
              currentProduct={productItem}
              plpLabels={plpLabels}
              onChangeColor={this.onChangeColor}
              onChangeSize={this.onChangeSize}
              renderReceiveProps={false}
              initialFormValues={this.formValues}
              isKeepAliveEnabled
              isStyleWith
              customFormName="styleWithForm"
              addToBagErrorId={addToBagErrorId}
              productInfoFromBag={addedToBagData}
              isFbt
              isLoading={addedToBagGhostLoaderState}
              isComplete={isComplete}
              updateFbtStatus={updateFbtStatus}
            />
          </div>
        </div>
      </div>
    );
  }
}

RecomStyleWith.propTypes = {
  productItem: PropTypes.shape({}).isRequired,
  currencySymbol: PropTypes.string,
  styleHeaderLabel: PropTypes.string,
  plpLabels: PropTypes.shape({}).isRequired,
  addToBagEcomAction: PropTypes.func,
  className: PropTypes.string,
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
  isPDPSmoothScrollEnabled: PropTypes.bool,
};

RecomStyleWith.defaultProps = {
  currencySymbol: '',
  addToBagEcomAction: () => {},
  styleHeaderLabel: '',
  className: '',
  addToBagError: '',
  addToBagErrorId: '',
  isPDPSmoothScrollEnabled: false,
};

export { RecomStyleWith as RecomStyleWithVanilla };
export default withStyles(RecomStyleWith, style);
