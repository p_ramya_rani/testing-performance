// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  border-radius: 12px;
  background-color: #f3f3f3;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin: 30px 0 20px;

  .section-styled-heading {
    margin: 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
    font-weight: 700;
  }

  .fbt-contentbox {
    display: flex;
  }
  .fbt-content-img {
    max-width: 98px;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }
  .fbt-content {
    width: 100%;
  }
  .brand-image {
    width: 60%;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.XXS} auto 0;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: ${(props) => props.theme.spacing.ELEM_SPACING.XS} auto 0;
      width: 55%;
    }
  }
`;
