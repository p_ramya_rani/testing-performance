// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/views/Carousel';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import mock from '../../../../../services/abstractors/common/moduleL/mock';
import { ModuleLVanilla as ModuleL } from '../views/ModuleL';

describe('ModuleL component', () => {
  let ModuleLComp;
  const props = {
    ...mock.moduleL.composites,
    set: mock.moduleL.set,
  };

  beforeEach(() => {
    const wrapper = shallow(<ModuleL {...props} />).get(0);
    ModuleLComp = shallow(wrapper);
  });

  it('renders correctly', () => {
    expect(ModuleLComp).toMatchSnapshot();
  });

  it('should render header', () => {
    expect(ModuleLComp.find(LinkText)).toHaveLength(1);
  });

  it('should render promo text banner', () => {
    expect(ModuleLComp.find(PromoBanner)).toHaveLength(1);
  });

  it('should render carousel', () => {
    expect(ModuleLComp.find(Carousel)).toHaveLength(1);
  });

  it('should render ModuleL for mobile', () => {
    expect(ModuleLComp.find('.moduleL__mobile-web-container')).toHaveLength(1);
  });
});
