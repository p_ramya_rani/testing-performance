// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mock from '../../../../../services/abstractors/common/moduleL/mock';
import { ModuleLVanilla } from '../views/ModuleL.native';

describe('ModuleLVanilla', () => {
  let component;

  beforeEach(() => {
    component = shallow(<ModuleLVanilla {...mock.moduleL.composites} />);
  });

  it('ModuleL should be defined', () => {
    expect(component).toBeDefined();
  });

  it('ModuleL should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('ModuleL should render correctly when isHpNewLayoutEnabled is true', () => {
    component = shallow(
      <ModuleLVanilla
        {...mock.moduleL.composites}
        moduleClassName="mod-app-media-bottom-10 mod-app-header-top-10"
        isHpNewLayoutEnabled
      />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render View', () => {
    expect(component.find('Styled(View)')).toHaveLength(2);
  });
});
