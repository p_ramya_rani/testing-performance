// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { NotificationWrapper, SectionStyle } from '../Notification.style.native';
import withStyles from '../../../hoc/withStyles.native';
import { BodyCopy, Image } from '../../../atoms';

const successImg = require('../../../../../../../mobileapp/src/assets/images/circle-check-fill.png');
const infoImg = require('../../../../../../../mobileapp/src/assets/images/circle-info-fill.png');
const errorImg = require('../../../../../../../mobileapp/src/assets/images/circle-error-fill.png');
const alertTriangleImg = require('../../../../../../../mobileapp/src/assets/images/alert-triangle.png');
const warningImg = require('../../../../../../../mobileapp/src/assets/images/circle-warning-fill.png');

const getImageByStatus = (status, isErrorTriangleIcon) => {
  switch (status) {
    case 'success':
      return successImg;
    case 'info':
      return infoImg;
    case 'error':
      return isErrorTriangleIcon ? alertTriangleImg : errorImg;
    case 'warning':
      return warningImg;
    default:
      return successImg;
  }
};
// Notification component will show error on the top of the page for form level or api error
const Notification = ({
  message,
  children,
  status,
  disableSpace,
  isWhiteBgColor,
  fontWeight,
  isErrorTriangleIcon,
  ...otherProps
}) => {
  const imageSrc = getImageByStatus(status, isErrorTriangleIcon);
  return (
    <View>
      <NotificationWrapper
        isWhiteBgColor={isWhiteBgColor}
        status={status}
        disableSpace={disableSpace}
        {...otherProps}
      >
        <Image height="25px" width="25px" source={imageSrc} alt={status} />
        {message ? (
          <ViewWithSpacing spacingStyles="padding-left-MED padding-right-MED">
            <BodyCopy
              fontSize="fs14"
              mobilefontFamily={['secondary']}
              fontWeight={fontWeight}
              text={message}
            />
          </ViewWithSpacing>
        ) : null}
        {children || null}
      </NotificationWrapper>
    </View>
  );
};

Notification.propTypes = {
  message: PropTypes.string.isRequired,
  children: PropTypes.node,
  status: PropTypes.string,
  disableSpace: PropTypes.bool,
  isWhiteBgColor: PropTypes.bool,
  fontWeight: PropTypes.string,
  isErrorTriangleIcon: PropTypes.bool,
};

Notification.defaultProps = {
  children: null,
  status: 'error',
  disableSpace: false,
  isWhiteBgColor: false,
  fontWeight: 'regular',
  isErrorTriangleIcon: false,
};

export default withStyles(Notification, SectionStyle);
export { Notification as NotificationVanilla };
