// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components/native';

const getPageStyle = props => {
  const { theme, marginValues } = props;
  return `
 margin: ${marginValues || `auto  ${theme.spacing.APP_LAYOUT_SPACING.XS}`};
  justify-content: center;

  `;
};
const SectionStyle = css`
  ${getPageStyle}
`;

const getTextBaseStyle = props => {
  const { theme, status, disableSpace, isWhiteBgColor } = props;
  return `
    color: ${theme.colorPalette.red[500]};

    ${status === 'success' ? `border: 2px solid ${props.theme.colorPalette.green[500]};` : ''};
      ${status === 'error' ? `border: 2px solid ${props.theme.colorPalette.red[500]};` : ''};
      ${status === 'info' ? `border: 2px solid ${props.theme.colorPalette.gray[600]};` : ''};
      ${status === 'warning' ? `border: 2px solid #ffab00;` : ''};
    padding:${theme.spacing.ELEM_SPACING.MED};
    margin: ${!disableSpace ? theme.spacing.ELEM_SPACING.XS : 0};
    justify-content: ${!disableSpace ? 'space-between' : 'flex-start'};
    align-items: flex-start;
    ${isWhiteBgColor && `background-color: ${props.theme.colors.WHITE};`}
    `;
};

const NotificationWrapper = styled.View`
  flex-direction: row;
  ${getTextBaseStyle};
`;

export { NotificationWrapper, SectionStyle };
