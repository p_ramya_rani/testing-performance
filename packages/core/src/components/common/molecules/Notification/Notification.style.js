// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const NotificationStyle = css`
  align-items: flex-start;
  display: flex;

  ${(props) =>
    props.status === 'success' ? `border: 2px solid ${props.theme.colorPalette.green[500]};` : ''};
  ${(props) =>
    props.status === 'error' ? `border: 2px solid ${props.theme.colorPalette.red[500]};` : ''};
  ${(props) =>
    props.status === 'info' ? `border: 2px solid ${props.theme.colorPalette.gray[600]};` : ''};
  ${(props) => (props.status === 'warning' ? `border: 2px solid #ffab00;` : '')};

  background-color: ${(props) => props.theme.colorPalette.white};

  .notification__image {
    height: 24px;
  }
  .using-unverified {
    margin-top: 10px;
  }
`;
export default NotificationStyle;
