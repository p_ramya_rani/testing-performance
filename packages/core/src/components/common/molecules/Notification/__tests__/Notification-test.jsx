// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { NotificationVanilla } from '../views/Notification';

describe('Notification Component', () => {
  it('should render notification component with status success', () => {
    const status = 'success';
    const colSize = { large: 10, medium: 8, small: 6 };
    const tree = shallow(<NotificationVanilla status={status} colSize={colSize} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render notification component with status error', () => {
    const status = 'error';
    const colSize = { large: 10, medium: 8, small: 6 };
    const tree = shallow(<NotificationVanilla status={status} colSize={colSize} />);
    expect(tree).toMatchSnapshot();
  });

  it('should render notification component with status error with alert triangle icon', () => {
    const status = 'error';
    const colSize = { large: 10, medium: 8, small: 6 };
    const tree = shallow(
      <NotificationVanilla status={status} colSize={colSize} isErrorTriangleIcon />
    );
    expect(tree).toMatchSnapshot();
  });
  it('should render notification component with status info', () => {
    const status = 'info';
    const colSize = { large: 10, medium: 8, small: 6 };
    const tree = shallow(<NotificationVanilla status={status} colSize={colSize} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render notification component with status warning', () => {
    const status = 'warning';
    const colSize = { large: 10, medium: 8, small: 6 };
    const tree = shallow(<NotificationVanilla status={status} colSize={colSize} />);
    expect(tree).toMatchSnapshot();
  });
});
