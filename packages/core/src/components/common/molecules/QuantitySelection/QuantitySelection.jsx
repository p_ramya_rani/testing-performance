// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getViewportInfo, isClient } from '@tcp/core/src/utils';
import styles from './styles/QuantitySelection.style';

const pdpRedesignWidthCheck = (isNewPDPEnabled, isMobile) => {
  return isNewPDPEnabled && !isMobile;
};

const getParentClassName = (className, smallerWidthCheck) => {
  return `container ${className} ${smallerWidthCheck ? 'pdpRedesign' : ''}`;
};

const decreaseDisableClass = (value) => {
  return value - 1 ? 'enabled' : 'disabled';
};

const increaseDisableClass = (value, options) => {
  return value + 1 < options.length + 1 ? 'enabled' : 'disabled';
};

const soldItemOverlay = (isSoldOutItem) => {
  return isSoldOutItem && <span className="soldout-overlay" />;
};

const isEnterKey = (e) => {
  return e.keyCode === 13;
};

const handleOnclickDecrease = (value, onChange) => {
  if (value - 1) {
    onChange(value - 1);
  }
};

const handleOnclickIncrease = (value, options, onChange) => {
  if (value + 1 < options.length + 1) {
    onChange(value + 1);
  }
};

const QuantitySelection = (props) => {
  const {
    input: { name, ariaLabel, value },
    id,
    placeholder,
    input,
    options,
    dataLocator,
    disabled,
    title,
    input: { onChange },
    showLoader,
    className,
    isNewPDPEnabled,
    isSoldOutItem,
    fromBagPage,
  } = props;
  const isMobile = isClient() ? getViewportInfo()?.isMobile : null;
  const smallerWidthCheck = pdpRedesignWidthCheck(isNewPDPEnabled, isMobile);
  const parentClassName = getParentClassName(className, smallerWidthCheck);
  if (!value && !fromBagPage) {
    onChange(1);
  }
  return (
    <div className={parentClassName}>
      {soldItemOverlay(isSoldOutItem)}
      <div
        className={`decreaseButton ${smallerWidthCheck ? 'pdpRedesign' : ''} ${decreaseDisableClass(
          value
        )}`}
        onClick={() => {
          handleOnclickDecrease(value, onChange);
        }}
        onKeyDown={(e) => {
          if (isEnterKey(e)) {
            handleOnclickDecrease(value, onChange);
          }
        }}
        role="button"
        tabIndex={0}
      >
        <div className="decreaseButtonContent" />
      </div>
      {showLoader ? (
        <div className="spinner-container">
          <div className="spc-spinner">
            <div />
            <div />
          </div>
        </div>
      ) : (
        <div
          {...input}
          id={id}
          aria-label={ariaLabel || title || placeholder}
          className="selected_qty_value"
          name={name}
          value={value || placeholder}
          data-locator={dataLocator}
          disabled={disabled}
        >
          {value || placeholder}
        </div>
      )}
      <div
        className={`increaseButton ${smallerWidthCheck ? 'pdpRedesign' : ''} ${increaseDisableClass(
          value,
          options
        )}`}
        onClick={() => {
          handleOnclickIncrease(value, options, onChange);
        }}
        onKeyDown={(e) => {
          if (isEnterKey(e)) {
            handleOnclickIncrease(value, options, onChange);
          }
        }}
        role="button"
        tabIndex="0"
      >
        <div className="horizontalBarVertical" />
        <div className="horizontalBar" />
      </div>
    </div>
  );
};

QuantitySelection.propTypes = {
  keepAlive: PropTypes.bool,
  id: PropTypes.string,
  className: PropTypes.string,
  ariaLabel: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  meta: PropTypes.shape({}).isRequired,
  input: PropTypes.shape({}).isRequired,
  options: PropTypes.shape([]).isRequired,
  dataLocator: PropTypes.string,
  disabled: PropTypes.bool,
  title: PropTypes.string,
  preText: PropTypes.string,
  forceSelect: PropTypes.bool,
  showLoader: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  isSoldOutItem: PropTypes.bool,
};

QuantitySelection.defaultProps = {
  keepAlive: false,
  id: '',
  ariaLabel: '',
  name: '',
  type: 'text',
  placeholder: '',
  dataLocator: '',
  disabled: false,
  title: '',
  className: '',
  preText: '',
  forceSelect: '',
  showLoader: false,
  isNewPDPEnabled: false,
  isSoldOutItem: false,
};

export default withStyles(QuantitySelection, styles);
