// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import QuantitySelection from '../QuantitySelection';

describe('QuantitySelection component', () => {
  it('QuantitySelection component renders correctly', () => {
    const props = {
      input: {
        name: '',
      },
      theme: {
        colors: {
          PRIMARY: '#fff',
        },
        colorPalette: {
          white: '#fff',
        },
      },
    };
    const component = shallow(<QuantitySelection {...props} />);
    expect(component).toMatchSnapshot();
  });
});
