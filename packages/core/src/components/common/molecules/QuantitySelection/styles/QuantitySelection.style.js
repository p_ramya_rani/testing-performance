import { css } from 'styled-components';

const styles = css`
  .spinner-container {
    display: block;
    justify-content: center;
    align-items: center;
    z-index: 999;
    width: 32px;
  }
  .soldout-overlay {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background: ${(props) => props.theme.colorPalette.white};
    opacity: 0.5;
    z-index: 9;
  }
  .spc-spinner {
    display: inline-block;
    position: relative;
    width: 5px;
    height: 5px;
  }
  .spc-spinner div {
    position: absolute;
    width: 10px;
    height: 10px;
    border-radius: 100%;
    background: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
    top: -3px;
  }
  .spc-spinner div:nth-child(1) {
    left: 0px;
    animation: quantity-spinner-animation 1.2s infinite;
  }
  .spc-spinner div:nth-child(2) {
    animation: quantity-spinner-animation 1.2s infinite;
    animation-delay: 0.2s;
  }
  @keyframes quantity-spinner-animation {
    0% {
      transform: scale(0);
      opacity: 0;
    }
    10% {
      transform: scale(0);
      opacity: 0;
    }
    15% {
      transform: scale(0.3);
      opacity: 0.3;
    }
    20% {
      transform: scale(0.6);
      opacity: 0.6;
    }
    25% {
      transform: scale(0.9);
      opacity: 0.9;
    }
    30% {
      transform: scale(1);
      opacity: 1;
    }
    70% {
      transform: translateX(20px);
      opacity: 1;
    }
    75% {
      transform: translateX(20px) scale(0.9);
      opacity: 0.9;
    }
    80% {
      transform: translateX(20px) scale(0.6);
      opacity: 0.6;
    }
    85% {
      transform: translateX(20px) scale(0.3);
      opacity: 0.3;
    }
    90% {
      transform: translateX(20px) scale(0);
      opacity: 0;
    }
    100% {
      transform: translateX(20px);
      opacity: 0;
    }
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default styles;
