// 9fbef606107a605d69c0edbcd8029e5d
/** @module ProductColorChipsSelector
 * @summary renders a LabeledRadioButtonGroup with radio buttons looking like images to select the
 * color for a product.
 *
 * Any extra props other than <code>colorFitsSizesMap, isShowZeroInventoryEntries, className</code>),
 * e.g., <code>title, disabled</code>, passed to this component will be passed along to the rendered <code>LabeledRadioButtonGroup</code> element.
 *
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import isEqual from 'lodash/isEqual';
import Constants from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/views/ProductChip.constants';
import { getAPIConfig, removeExtension } from '@tcp/core/src/utils';
import withStyles from '../../../hoc/withStyles';
import LabeledRadioButtonGroup from '../../LabeledRadioButtonGroup';
import styles from '../styles/ProductColorChipSelector.style';

const getColorImageUrlByName = (colorEntry) => {
  return colorEntry && colorEntry.get('imagePath');
};

const getColor = (colorEntry) => {
  return colorEntry && colorEntry.get('color');
};

const getColorName = (color) => {
  return color && color.get('name');
};

const getSwatchImagePath = (color) => {
  return color && color.get('swatchImage');
};
const getImagename = (swatchProductImage) => {
  const extension = '.jpg';
  const swatchImageNameParts = swatchProductImage && swatchProductImage.split('/');
  let imgName = swatchImageNameParts && swatchImageNameParts.length > 1 && swatchImageNameParts[1];
  imgName = imgName && imgName.replace(/\.(mp4|webm|WEBM|MP4)$/, extension);
  return imgName || '';
};

const getSwatchIdWhite = (name, isImageSwatchDisabled) => {
  if (
    (name === Constants.COLOR_CHIP.PRODUCTS_WHITE ||
      name === Constants.COLOR_CHIP.PRODUCTS_SIMPLYWHT) &&
    isImageSwatchDisabled
  ) {
    return true;
  }
  return false;
};

const getImageChipsOptionsMap = (
  colorFitsSizesMap,
  flagObj,
  isGiftCard,
  imagesByColor,
  itemBrand,
  imageSwatchesToDisplay,
  isStyleWith
) => {
  const apiConfig = getAPIConfig();
  let { brandId } = apiConfig;
  brandId = flagObj.primaryBrand || itemBrand || flagObj.alternateBrand || brandId;
  const assetPrefix = apiConfig[`productAssetPath${brandId.toUpperCase()}`];
  const assetHost = apiConfig[`assetHost${brandId.toUpperCase()}`];
  return (
    colorFitsSizesMap &&
    colorFitsSizesMap.map((colorEntry) => {
      const color = getColor(colorEntry);
      const name = getColorName(color);
      const imageColorNames = Object.keys(imagesByColor);
      const colorName = color.get('name');
      const colorIndex = imageColorNames.indexOf(colorName);
      const swatchProductImage = imageSwatchesToDisplay && imageSwatchesToDisplay[colorIndex];
      const imgName = getImagename(swatchProductImage);
      let imgUrl = imgName && `${imgName.split('_')[0]}/${imgName}`;
      let imgConfig = 't_pdp_imgswatch,f_auto,q_auto';

      if (isGiftCard && color && color.get('imagePath')) {
        imgUrl = color.get('imagePath');
        imgConfig = 'w_125';
      }
      return {
        value: name,
        title: name,
        content: (
          <span className="color-title-container" title={name}>
            {!isStyleWith && <span className="color-name">{name}</span>}
            {imgUrl && (
              <img
                className={`color-image ${
                  (flagObj.isNewQVEnabled && !flagObj.isPDP) || flagObj.isNewPDPEnabled
                    ? 'quick-view-drawer-redesign'
                    : ''
                }`}
                src={removeExtension(`${assetHost}/${imgConfig}/${assetPrefix}/${imgUrl}`)}
                alt={name}
              />
            )}
          </span>
        ),
        disabled: flagObj.isDisableZeroInventoryEntries && colorEntry.maxAvailable <= 0,
      };
    })
  );
};

const getColorsChipsOptionsMap = (
  colorFitsSizesMap,
  flagObj,
  isGiftCard,
  imagesByColor,
  itemBrand,
  isStyleWith,
  isImageSwatchDisabled
) => {
  const apiConfig = getAPIConfig();
  let { brandId } = apiConfig;
  brandId = flagObj.primaryBrand || itemBrand || flagObj.alternateBrand || brandId;
  const assetPrefix = apiConfig[`productAssetPath${brandId.toUpperCase()}`];
  const assetHost = apiConfig[`assetHost${brandId.toUpperCase()}`];
  return (
    colorFitsSizesMap &&
    colorFitsSizesMap.map((colorEntry) => {
      const color = getColor(colorEntry);
      const name = getColorName(color);
      const swatchImagePath = removeExtension(getSwatchImagePath(color));
      let colorChipImgUrl = swatchImagePath
        ? `${swatchImagePath.split('_')[0]}/${swatchImagePath}`
        : getColorImageUrlByName(isStyleWith ? color : colorEntry);
      let colorChipImgConfig = 't_swatch_img,w_50,h_50,c_thumb,g_auto:0,f_auto,q_auto';

      if (isGiftCard && color && color.get('imagePath')) {
        colorChipImgUrl = color.get('imagePath');
        colorChipImgConfig = 'w_125';
      }

      return {
        value: name,
        title: name,
        content: (
          <span className="color-title-container " title={name}>
            {!isStyleWith && <span className="color-name">{name}</span>}
            {colorChipImgUrl && (
              <img
                className={`${
                  getSwatchIdWhite(name, isImageSwatchDisabled) ? 'white-chip-border' : ''
                } color-image`}
                src={`${assetHost}/${colorChipImgConfig}/${assetPrefix}/${colorChipImgUrl}`}
                alt={name}
              />
            )}
          </span>
        ),
        disabled: flagObj.isDisableZeroInventoryEntries && colorEntry.maxAvailable <= 0,
      };
    })
  );
};

const arePropsEqual = (prevProps, nextProps) => {
  return (
    isEqual(prevProps.colorFitsSizesMap, nextProps.colorFitsSizesMap) &&
    isEqual(prevProps.isABTestLoaded, nextProps.isABTestLoaded) &&
    isEqual(prevProps.newColorList, nextProps.newColorList) &&
    isEqual(
      prevProps.input.value.name || prevProps.input.value,
      nextProps.input.value.name || nextProps.input.value
    ) &&
    isEqual(nextProps.isImageSwatchDisabled, prevProps.isImageSwatchDisabled)
  );
};

const ProductColorChipsSelector = (props) => {
  const {
    colorFitsSizesMap,
    isShowZeroInventoryEntries,
    isDisableZeroInventoryEntries,
    className,
    isGiftCard,
    imagesByColor,
    itemBrand,
    relatedSwatchImages,
    imageSwatchesToDisplay,
    isNewQVEnabled,
    isNewPDPEnabled,
    isImageSwatchDisabled,
    isABTestLoaded,
    isPDP,
    isStyleWith,
    isBundleProduct,
    newColorList,
    primaryBrand,
    alternateBrand,
    ...otherProps
  } = props;
  const flagObj = {
    isDisableZeroInventoryEntries,
    isPDP,
    isBundleProduct,
    isNewQVEnabled,
    isNewPDPEnabled,
    primaryBrand,
    alternateBrand,
  };

  const imageChipOptionsMap = getImageChipsOptionsMap(
    colorFitsSizesMap,
    flagObj,
    isGiftCard,
    imagesByColor,
    itemBrand,
    imageSwatchesToDisplay,
    isStyleWith
  );

  const colorChipOptionsMap = getColorsChipsOptionsMap(
    colorFitsSizesMap,
    flagObj,
    isGiftCard,
    imagesByColor,
    itemBrand,
    isStyleWith,
    isImageSwatchDisabled
  );

  return (
    <>
      <LabeledRadioButtonGroup
        className={`${className} ${
          isNewQVEnabled || isNewPDPEnabled ? 'new-quick-view-drawer' : ''
        } image-chips-selector`}
        optionsMap={imageChipOptionsMap}
        colorSelector
        newColorList={newColorList}
        shouldNotSplitLabel={isGiftCard}
        {...otherProps}
      />
      <LabeledRadioButtonGroup
        className={`${className} color-chips-selector`}
        optionsMap={colorChipOptionsMap}
        colorSelector
        newColorList={newColorList}
        shouldNotSplitLabel={isGiftCard}
        {...otherProps}
      />
    </>
  );
};

ProductColorChipsSelector.propTypes = {
  /**
   * The available color fit and size options for the product to select a color for
   * Organized in a three level nesting (similar to a site navigation) with L1 being the color,
   * L2 being the fit, and L3 being the size
   */
  // colorFitsSizesMap: COLOR_FITS_SIZES_MAP_PROP_TYPE.isRequired,

  /** flags if to show colors with zero inventory in all fits and sizes */
  isShowZeroInventoryEntries: PropTypes.bool,

  /** flags if to disable sizes with zero inventory. Defaults to true. */
  isDisableZeroInventoryEntries: PropTypes.bool,

  title: PropTypes.string,

  colorFitsSizesMap: PropTypes.shape([]).isRequired,
  className: PropTypes.string.isRequired,
  isGiftCard: PropTypes.bool.isRequired,
  imagesByColor: PropTypes.shape([]).isRequired,
  itemBrand: PropTypes.shape({}).isRequired,
  relatedSwatchImages: PropTypes.shape([]).isRequired,
  isStyleWith: PropTypes.bool,
  imageSwatchesToDisplay: PropTypes.shape([]).isRequired,
  isImageSwatchDisabled: PropTypes.bool,
  isABTestLoaded: PropTypes.bool,
  isPDP: PropTypes.bool,
  isNewQVEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  isBundleProduct: PropTypes.bool,
  newColorList: PropTypes.shape([]),
};

ProductColorChipsSelector.defaultProps = {
  title: 'COLOR: ',
  isShowZeroInventoryEntries: true,
  isDisableZeroInventoryEntries: true,
  isStyleWith: false,
  isImageSwatchDisabled: false,
  isABTestLoaded: false,
  isPDP: false,
  isBundleProduct: false,
  newColorList: [],
  isNewQVEnabled: false,
  isNewPDPEnabled: false,
};

export default React.memo(withStyles(ProductColorChipsSelector, styles), arePropsEqual);
