// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .color-chips-selector-container,
  .image-chips-selector-container {
    .inline-error-message {
      white-space: normal;
    }
  }

  &.color-chips-selector-container {
    display: ${(props) =>
      !props.isImageSwatchDisabled &&
      ((props.isPDP &&
        (!props.isABTestLoaded || !props.isImageSwatchDisabled || props.isNewPDPEnabled)) ||
        (!props.isPDP && props.isNewQVEnabled))
        ? 'none'
        : 'block'};
  }

  &.image-chips-selector-container {
    visibility: ${(props) => (props.isPDP && !props.isABTestLoaded ? 'hidden' : '')};
    display: ${(props) =>
      (props.isPDP && props.isABTestLoaded && props.isImageSwatchDisabled) ||
      (!props.isPDP && !props.isNewQVEnabled) ||
      (props.isNewQVEnabled && props.isImageSwatchDisabled)
        ? 'none'
        : ''};
  }

  .color-chips-selector-items-list,
  .image-chips-selector-items-list {
    position: relative;
    width: 100%;
    margin-top: ${(props) => (props.isPDP ? 0 : props.theme.spacing.ELEM_SPACING.SM)};
    cursor: auto;
  }

  .color-chips-selector-item {
    width: 30px;
    height: 30px;
    padding: 0px;
    display: inline-block;
    vertical-align: middle;
    margin: 10px ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0 0;
    border-radius: 50%;
    line-height: 25px;
    position: relative;
    cursor: pointer;

    .input-radio-title {
      line-height: 0px;
      position: absolute;
      top: 0;
    }

    /* First color's chip */
    &:first-of-type {
      margin-left: 0px;
    }

    .input-radio-icon-unchecked,
    .input-radio-icon-checked {
      top: auto;
      width: 30px;
      height: 30px;
      vertical-align: middle;

      input[type='radio'] {
        left: 0px;
        z-index: 1;
        width: 100%;
        height: 100%;
        cursor: pointer;
        -webkit-appearance: none;
        display: none;
      }

      input[type='radio']:focus {
        border-radius: 15px;
        box-shadow: 0 0 0 2pt ${(props) => props.theme.colorPalette.blue.A100};
        margin-top: -3px;
        margin-left: -3px;
        width: 30px;
        height: 30px;
        outline: 0;
      }

      &:before,
      &:after {
        display: none;
      }

      @media ${(props) => props.theme.mediaQuery.large} {
        width: 23px;
        height: 23px;
      }
    }

    /* Container of title name and image */
    .color-title-container {
      display: inline-block;
    }

    /* Color name */
    .color-name {
      font-size: 0;
      position: absolute;
    }

    /* Image color of item */
    .color-image {
      border-radius: 50%;
      position: relative;
      width: 30px;
      height: 30px;
      cursor: pointer;

      @media ${(props) => props.theme.mediaQuery.large} {
        width: 23px;
        height: 23px;
      }
    }

    /* When the input is checked, the image color has black border (selected) */
    .input-radio-icon-checked + .input-radio-title .color-image {
      border: 2px solid ${(props) => props.theme.colors.BLACK} !important;
      width: 26px;
      height: 26px;
      padding: 1px;

      @media ${(props) => props.theme.mediaQuery.large} {
        width: 19px;
        height: 19px;
      }
    }
    .input-radio-title .color-image.error {
      border: 12px solid ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 23px;
      height: 23px;
    }
  }

  /* new style for image chips*/

  .image-chips-selector-items-list {
    overflow-x: scroll;
    overflow-y: hidden;
    width: 100%;
    white-space: nowrap;
    -ms-overflow-style: none;
    scrollbar-width: none;
    @media ${(props) => props.theme.mediaQuery.medium} {
      white-space: normal;
      width: 100%;
    }
  }
  .image-chips-selector-items-list::-webkit-scrollbar {
    display: none;
  }
  .image-chips-selector-item {
    width: 54px;
    height: 58px;
    display: inline-block;
    vertical-align: middle;
    margin: 5px 10px 5px 0;
    line-height: 25px;
    position: relative;
    cursor: pointer;

    .input-radio-title {
      line-height: 0px;
      position: absolute;
      top: 0;
    }

    /* First color's chip */
    .input-radio-icon-unchecked,
    .input-radio-icon-checked {
      top: auto;
      width: 54px;
      height: 58px;
      vertical-align: middle;
      /* stylelint-disable */
      input[type='radio'] {
        left: 0px;
        z-index: 1;
        width: 100%;
        height: 100%;
        cursor: pointer;
        -webkit-appearance: none;
        display: none;
      }
    }

    /* Container of title name and image */
    .color-title-container {
      display: inline-block;
    }
    /* Color name */
    .color-name {
      font-size: 0;
      position: absolute;
    }
    /* Image color of item */
    .color-image {
      position: relative;
      width: 54px;
      height: 58px;
      cursor: pointer;
      border-radius: 8px;
      border: solid 1px ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
      background-color: ${(props) => props.theme.colors.WHITE};

      @media ${(props) => props.theme.mediaQuery.large} {
        width: 54px;
        height: 58px;
      }
    }
    .color-image.quick-view-drawer-redesign {
      position: relative;
      width: 43px;
      height: 53px;
      cursor: pointer;
      border: none;
      background-color: ${(props) => props.theme.colors.WHITE};

      @media ${(props) => props.theme.mediaQuery.large} {
        width: 43px;
        height: 53px;
      }
    }

    .color-image.white-chip-border {
      border: solid 1px ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    }
    .input-radio-title .color-image.error {
      border: 12px solid ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    }
    /* When the input is checked, the image color has black border (selected) */
    .input-radio-icon-checked + .input-radio-title .color-image {
      border: solid 1px ${(props) => props.theme.colors.PRIMARY.DARK};
      border-radius: 8px;
      background-color: ${(props) => props.theme.colors.WHITE};
      box-shadow: 2px 2px 4px 0 rgba(0, 0, 0, 0.2);
    }
    /* When the input is checked, the image color has blue border (selected) and enlarged */
    .input-radio-icon-checked + .input-radio-title .color-image.quick-view-drawer-redesign {
      border: solid 3px ${(props) => props.theme.colors.BRAND.PRIMARY};
      outline: solid 3px transparent;
      border-radius: 6px;
      background-color: ${(props) => props.theme.colors.WHITE};
      box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.12);
      width: 49px;
      height: 61px;
      padding: 0 3px;
      position: relative;
      left: -7px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        top: -4px;
      }
      @media ${(props) => props.theme.mediaQuery.medium} {
        top: -5px;
      }
    }
  }
  .image-chips-selector-item.new-quick-view-drawer {
    margin: 5px 4px 5px 7px;
  }
  .size-and-fit-detail-item.quick-view-drawer-redesign.item-selected-option {
    margin-top: 3px;
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default styles;
