// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { View } from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import LabeledRadioButton from '@tcp/core/src/components/common/atoms/LabeledRadioButton';
import Address from '../../Address';
import AddressOptionsContainer from '../AddressOption.styles.native';

const AddressOption = ({
  showInput,
  address,
  onChange,
  isSelected,
  showWarning,
  showProminentWarning,
}) => {
  if (showInput) {
    return (
      <AddressOptionsContainer>
        <RadioForm>
          <LabeledRadioButton onPress={onChange} checked={isSelected} obj={{}} index={0} />
        </RadioForm>

        <Address address={address} showName showPhone={false} showCountry={false} />
      </AddressOptionsContainer>
    );
  }
  return (
    <View>
      <Address
        address={address}
        showName
        showPhone={false}
        showCountry={false}
        showWarning={showWarning}
        regularName={showWarning}
        showProminentWarning={showProminentWarning}
      />
    </View>
  );
};

AddressOption.propTypes = {
  onChange: PropTypes.func,
  inputProps: PropTypes.shape({}),
  showInput: PropTypes.bool,
  address: PropTypes.shape({}),
  isSelected: PropTypes.bool,
  showWarning: PropTypes.number,
};

AddressOption.defaultProps = {
  onChange: () => {},
  inputProps: {},
  showInput: true,
  address: {},
  isSelected: false,
  showWarning: 0,
};

export default AddressOption;
