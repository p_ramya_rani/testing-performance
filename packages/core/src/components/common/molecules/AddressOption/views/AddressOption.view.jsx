// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Address from '../../Address';

const AddressOption = ({
  className,
  value,
  name,
  showInput,
  address,
  onChange,
  isSelected,
  inputProps,
  fontWeight,
  showWarning,
  showProminentWarning,
}) => {
  if (showInput) {
    return (
      <label className={className} htmlFor={value}>
        <input
          id={value}
          value={value}
          type="radio"
          name={name}
          checked={isSelected}
          onChange={onChange}
          className="elem-mr-MED"
          {...inputProps}
        />
        <Address address={address} fontWeight={fontWeight} showPhone={false} showCountry={false} />
      </label>
    );
  }
  return (
    <div className={className}>
      <Address
        address={address}
        fontWeight={fontWeight}
        showPhone={false}
        showCountry={false}
        showWarning={showWarning}
        showProminentWarning={showProminentWarning}
      />
    </div>
  );
};

AddressOption.propTypes = {
  className: PropTypes.string.isRequired,
  value: PropTypes.string,
  name: PropTypes.string,
  showInput: PropTypes.bool.isRequired,
  address: PropTypes.shape({}).isRequired,
  onChange: PropTypes.func,
  isSelected: PropTypes.bool.isRequired,
  inputProps: PropTypes.shape({}),
  fontWeight: PropTypes.string,
  showWarning: PropTypes.number,
};

AddressOption.defaultProps = {
  value: '',
  name: '',
  onChange: () => {},
  inputProps: {},
  fontWeight: '',
  showWarning: 0,
};

export default AddressOption;
