// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import { isServer } from '@tcp/core/src/utils';

const ModuleFont = props => {
  const {
    className,
    richTextList: [{ text }],
    dataLocator,
    isNativeView,
  } = props;

  return (
    isServer && (
      <React.Fragment>
        <Espot
          className={`${className} moduleFont`}
          richTextHtml={text}
          dataLocator={dataLocator || `moduleFont`}
          isNativeView={isNativeView}
        />
      </React.Fragment>
    )
  );
};

ModuleFont.propTypes = {
  className: PropTypes.string.isRequired,
  richTextList: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.string)).isRequired,
  dataLocator: PropTypes.string.isRequired,
  isNativeView: PropTypes.bool,
};

ModuleFont.defaultProps = {
  isNativeView: false,
};
export default ModuleFont;
