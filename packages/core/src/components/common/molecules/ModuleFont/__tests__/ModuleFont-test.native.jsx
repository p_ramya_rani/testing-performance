// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ModuleFont from '../ModuleFont.native';

describe('Module Font component', () => {
  let ModuleFontComp;

  beforeEach(() => {
    ModuleFontComp = shallow(<ModuleFont />);
  });
  it('renders correctly', () => {
    expect(ModuleFontComp).toMatchSnapshot();
  });
});
