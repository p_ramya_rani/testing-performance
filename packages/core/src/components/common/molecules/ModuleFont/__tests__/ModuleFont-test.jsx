// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ModuleFont from '../ModuleFont';

const props = {
  className: 'test-class',
  richTextList: [
    {
      text: "<div class='test-class'>I am test </div>",
    },
  ],
  dataLocator: 'data-locator',
};

describe('Module Font component', () => {
  let ModuleFontComp;

  beforeEach(() => {
    ModuleFontComp = shallow(<ModuleFont {...props} />);
  });
  it('renders correctly', () => {
    expect(ModuleFontComp).toMatchSnapshot();
  });
});
