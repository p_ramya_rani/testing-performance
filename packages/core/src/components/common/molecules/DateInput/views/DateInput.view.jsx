// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import TextBox from '../../../atoms/TextBox';
import styles from '../styles/DateInput.style';

/**
 * React component to render DateInput form field.
 * Used the native browser date picker to render datePicker component
 */
export class DateInput extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    placeholder: PropTypes.string,
    maxDate: PropTypes.string,
    input: PropTypes.shape({
      value: PropTypes.string,
      onChange: PropTypes.func,
    }),
  };

  static defaultProps = {
    className: '',
    placeholder: '',
    maxDate: '',
    input: {
      value: '',
    },
  };

  render() {
    const { className, placeholder, maxDate, ...otherProps } = this.props;
    const today = new Date().toISOString().substr(0, 10);

    return (
      <>
        <TextBox type="date" placeholder={placeholder} maxDate={today} {...otherProps} />
      </>
    );
  }
}

export default withStyles(DateInput, styles);
