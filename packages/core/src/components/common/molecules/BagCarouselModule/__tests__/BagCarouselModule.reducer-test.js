// 9fbef606107a605d69c0edbcd8029e5d 
import BagCarouselModuleReducer from '../container/BagCarouselModule.reducer';

describe('BagCarouselModuleReducer tests', () => {
  it('should handle product add success', () => {
    const initialState = { bagCarouselModuleData: { orderTotal: 0, products: [] } };

    expect(
      Object.keys(
        BagCarouselModuleReducer(initialState, {
          type: '@@BagCarouselModule-SET_BAG_CAROUSEL_MODULE_ITEM',
          payload: {
            products: [
              {
                imagePath: '30105.jpb',
                pdpUrl: '/p/30105',
                productPartNumber: '12345',
                name: 'Girl Top',
                itemBrand: 'tcp',
                itemPartNumber: '32123',
              },
            ],
            orderTotal: 10,
          },
        })
      ).length
    ).toBe(1);
  });
});
