// 9fbef606107a605d69c0edbcd8029e5d 
import { BAG_CAROUSEL_MODULE_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import getBagCarouselModuleData from '../container/BagCarouselModule.selector';

describe('#BagCarouselModule  selector', () => {
  let storeState;
  beforeEach(() => {
    const initialState = {
      bagCarouselModuleData: { orderTotal: 0, products: [] },
    };
    const state = {
      ...initialState,
      bagCarouselModuleData: {
        products: [
          {
            imagePath: '30105.jpb',
            pdpUrl: '/p/30105',
            productPartNumber: '12345',
            name: 'Girl Top',
            itemBrand: 'tcp',
            itemPartNumber: '32123',
          },
        ],
        orderTotal: 10,
      },
    };
    storeState = {
      [BAG_CAROUSEL_MODULE_REDUCER_KEY]: state,
    };
  });

  it('#getBagCarouselModuleData should return getBagCarouselModuleData state', () => {
    expect(getBagCarouselModuleData(storeState)).toEqual({
      products: [
        {
          imagePath: '30105.jpb',
          pdpUrl: '/p/30105',
          productPartNumber: '12345',
          name: 'Girl Top',
          itemBrand: 'tcp',
          itemPartNumber: '32123',
        },
      ],
      orderTotal: 10,
    });
  });
});
