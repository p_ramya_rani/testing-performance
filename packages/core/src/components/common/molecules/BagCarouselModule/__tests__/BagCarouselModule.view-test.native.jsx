// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import * as utils from '@tcp/core/src/utils/utils';
import { BagCarouselModuleVanilla as BagCarouselModule } from '../views/BagCarouselModule.view.native';

utils.getBrand = jest.fn().mockReturnValue('tcp');

describe('BagCarouselModule component', () => {
  let component;
  const props = {
    productDetail: {
      itemInfo: {
        name: 'Boys Basic Skinny Jeans 1',
        qty: '1',
        size: '1',
        price: 123,
        myPlacePoints: 123,
        isGiftItem: true,
        fit: 'regular',
        itemBrand: 'TCP',
        color: 'red',
      },
      productInfo: { skuId: '123', productPartNumber: 123, pdpUrl: '' },
      miscInfo: {
        badge: '',
      },
    },
    labels: {},
    navigation: {},
  };
  beforeEach(() => {
    component = shallow(<BagCarouselModule {...props} />);
  });
  it('should be defined', () => {
    expect(component).toBeDefined();
  });
  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
  it('should call item renderer', () => {
    const imgSource = {
      item: {
        pdpUrl: '/p/abc',
        productPartNumber: '12345',
        itemBrand: 'tcp',
        name: 'Bot',
        imagePath: '20159.jpg',
      },
    };

    expect(component.instance().itemRenderer(imgSource)).not.toBeNull();
  });
});
