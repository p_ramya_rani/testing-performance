// 9fbef606107a605d69c0edbcd8029e5d 
import {
  setBagCarouselModuleData,
  setBagCarouselModuleItem,
} from '../container/BagCarouselModule.actions';
import BAG_CAROUSEL_CONSTANTS from '../container/BagCarouselModule.constants';

describe('#BagCarouselModuleAction', () => {
  it('setBagCarouselModuleData', () => {
    expect(setBagCarouselModuleData()).toEqual({
      type: BAG_CAROUSEL_CONSTANTS.SET_BAG_CAROUSEL_MODULE_DATA,
      payload: undefined,
    });
  });
  it('setBagCarouselModuleItem', () => {
    expect(setBagCarouselModuleItem()).toEqual({
      type: BAG_CAROUSEL_CONSTANTS.SET_BAG_CAROUSEL_MODULE_ITEM,
      payload: undefined,
    });
  });
});
