// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import BAG_CAROUSEL_CONSTANTS from './BagCarouselModule.constants';

const INITIAL_STATE = fromJS({
  bagCarouselModuleData: { orderTotal: 0, products: [] },
});

const BagCarouselModuleReducer = (state = INITIAL_STATE, action) => {
  if (action.type === BAG_CAROUSEL_CONSTANTS.SET_BAG_CAROUSEL_MODULE_ITEM) {
    const { products, orderTotal } = action.payload;
    return { ...state, bagCarouselModuleData: { orderTotal, products } };
  }
  return state;
};

export default BagCarouselModuleReducer;
