// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { getIsBagCarouselEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { updateAppTypeWithParams } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import { getLabelValue } from '@tcp/core/src/utils';
import BagCarouselModule from '../views';
import getBagCarouselModuleData from './BagCarouselModule.selector';
import constants from './BagCarouselModule.constants';

export const mapStateToProps = state => {
  const viewBtnIcidParam = getLabelValue(
    state.Labels,
    'lbl_view_button_icid',
    'bagCarouselModule',
    'global'
  );
  const productIcidParam = getLabelValue(
    state.Labels,
    'lbl_product_icid',
    'bagCarouselModule',
    'global'
  );
  const threSholdValue = getLabelValue(
    state.Labels,
    'lbl_threshold_amount',
    'bagCarouselModule',
    'global'
  );
  return {
    labels: {
      heading: getLabelValue(state.Labels, 'lbl_bag_carousel_title', 'bagCarouselModule', 'global'),
      buttonText: getLabelValue(
        state.Labels,
        'lbl_bag_carousel_view_bag',
        'bagCarouselModule',
        'global'
      ),
      viewBtnIcidParam: viewBtnIcidParam === 'lbl_view_button_icid' ? '' : viewBtnIcidParam,
      productIcidParam: productIcidParam === 'lbl_product_icid' ? '' : productIcidParam,
    },
    isBagCarouselEnabled: getIsBagCarouselEnabled(state),
    cartOrderItems: getBagCarouselModuleData(state),
    threSholdValue:
      threSholdValue === 'lbl_threshold_amount'
        ? constants.DEFAULT_THRESHOLD_AMOUNT
        : parseInt(threSholdValue, 10),
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    updateAppTypeHandler: payload => {
      dispatch(updateAppTypeWithParams(payload));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BagCarouselModule);
