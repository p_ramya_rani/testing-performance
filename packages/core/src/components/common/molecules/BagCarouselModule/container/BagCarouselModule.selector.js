// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { BAG_CAROUSEL_MODULE_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';

const getBagCarouselModuleState = state => {
  return state[BAG_CAROUSEL_MODULE_REDUCER_KEY];
};

const getBagCarouselModuleData = createSelector(
  getBagCarouselModuleState,
  BagCarouselModule => BagCarouselModule && BagCarouselModule.bagCarouselModuleData
);

export default getBagCarouselModuleData;
