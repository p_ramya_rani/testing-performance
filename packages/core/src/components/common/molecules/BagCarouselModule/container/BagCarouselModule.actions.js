// 9fbef606107a605d69c0edbcd8029e5d 
import BAG_CAROUSEL_CONSTANTS from './BagCarouselModule.constants';

/**
 * To Call the saga and perform other operations based on action
 * @param {} payload
 */
const setBagCarouselModuleData = payload => ({
  type: BAG_CAROUSEL_CONSTANTS.SET_BAG_CAROUSEL_MODULE_DATA,
  payload,
});

/**
 * To get the value from async storage.
 * @param {} payload
 */
const getBagCarouselModuleFromAsyncData = payload => ({
  type: BAG_CAROUSEL_CONSTANTS.GET_BAG_CAROUSEL_MODULE_DATA_FROM_STORAGE,
  payload,
});

/**
 * To remove the value from async storage
 * @param {} payload
 */
const resetBagCarouselModuleData = payload => ({
  type: BAG_CAROUSEL_CONSTANTS.RESET_BAG_CAROUSEL_MODULE_DATA,
  payload,
});

/**
 * To set the data in Store
 * @param {*} payload
 */
const setBagCarouselModuleItem = payload => ({
  type: BAG_CAROUSEL_CONSTANTS.SET_BAG_CAROUSEL_MODULE_ITEM,
  payload,
});

export {
  setBagCarouselModuleData,
  setBagCarouselModuleItem,
  getBagCarouselModuleFromAsyncData,
  resetBagCarouselModuleData,
};
