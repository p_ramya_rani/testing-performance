// 9fbef606107a605d69c0edbcd8029e5d 
import { takeLatest, put, call, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getIsSessionSharingEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getBrand,
  getValueFromAsyncStorage,
  setValueInAsyncStorage,
  removeMultipleFromAsyncStorage,
} from '@tcp/core/src/utils';

import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import BAG_CAROUSEL_CONSTANTS from './BagCarouselModule.constants';
import { setBagCarouselModuleItem } from './BagCarouselModule.actions';

export function* bagCarouselModuleAsyncStorage({ payload }) {
  try {
    const isSessionSharedEnabled = yield select(getIsSessionSharingEnabled);
    const isUserLoggedIn = yield select(getUserLoggedInState);
    // If the session is shared then bag carousel for both the brands will have same data.
    const asyncKey =
      !isSessionSharedEnabled && !isUserLoggedIn
        ? `${BAG_CAROUSEL_CONSTANTS.BAG_CAROUSEL_ASYNC_STORAGE_KEY}_${getBrand()}`
        : BAG_CAROUSEL_CONSTANTS.BAG_CAROUSEL_ASYNC_STORAGE_KEY;
    const result = yield call(getValueFromAsyncStorage, asyncKey);
    if (result) {
      yield put(setBagCarouselModuleItem(JSON.parse(result)));
    }
  } catch (err) {
    logger.error('Error: Error in Getting Bag Carousel Module Data From Async Storage', {
      error: err,
      extraData: {
        component: 'BagCarouselModuleSagaBagCarouselModuleSaga Saga - Bag Carousel Module',
        payloadRecieved: payload,
      },
    });
  }
}

/**
 * @function resetBagCarouselData
 * This Function will be used to set the item in redux store and in store.
 */

export function* resetBagCarouselData({ payload }) {
  try {
    const asyncKeyArr = [
      BAG_CAROUSEL_CONSTANTS.BAG_CAROUSEL_ASYNC_STORAGE_KEY,
      `${BAG_CAROUSEL_CONSTANTS.BAG_CAROUSEL_ASYNC_STORAGE_KEY}_tcp`,
      `${BAG_CAROUSEL_CONSTANTS.BAG_CAROUSEL_ASYNC_STORAGE_KEY}_gym`,
    ];
    yield call(removeMultipleFromAsyncStorage, asyncKeyArr);
    yield put(setBagCarouselModuleItem({ orderTotal: 0, products: [] }));
  } catch (err) {
    logger.error('Error: Error in Deleting Bag Carousel Module Data From Async Storage', {
      error: err,
      extraData: {
        component: 'RecentlyViewed Saga - Bag Carousel Module',
        payloadRecieved: payload,
      },
    });
  }
}

/**
 * @function bagCarouselModule
 * This Function will be used to set the item in redux store and in store.
 */

export function* bagCarouselModule({ payload }) {
  try {
    const products = [];
    const { orderItems, grandTotal, giftCardsTotal } = payload;
    orderItems.forEach(productItem => {
      const { productInfo = {} } = productItem;
      const { imagePath, pdpUrl, productPartNumber, name, itemBrand, itemPartNumber } = productInfo;
      products.push({
        imagePath,
        pdpUrl,
        productPartNumber,
        name,
        itemBrand,
        itemPartNumber,
      });
    });
    const bagData = { products, orderTotal: grandTotal - giftCardsTotal };
    const isSessionSharedEnabled = yield select(getIsSessionSharingEnabled);
    const isUserLoggedIn = yield select(getUserLoggedInState);
    // If the session is shared then bag carousel for both the brands will have same data.
    const asyncKey =
      !isSessionSharedEnabled && !isUserLoggedIn
        ? `${BAG_CAROUSEL_CONSTANTS.BAG_CAROUSEL_ASYNC_STORAGE_KEY}_${getBrand()}`
        : BAG_CAROUSEL_CONSTANTS.BAG_CAROUSEL_ASYNC_STORAGE_KEY;
    setValueInAsyncStorage(asyncKey, JSON.stringify(bagData));
    yield put(setBagCarouselModuleItem(bagData));
  } catch (err) {
    logger.error('Error: Error in Saving Bag Carousel Module Item', {
      error: err,
      extraData: {
        component: 'BagCarouselModuleSaga Saga - Bag Carousel Module',
        payloadRecieved: payload,
      },
    });
  }
}

function* BagCarouselModuleSaga() {
  yield takeLatest(BAG_CAROUSEL_CONSTANTS.SET_BAG_CAROUSEL_MODULE_DATA, bagCarouselModule);
  yield takeLatest(
    BAG_CAROUSEL_CONSTANTS.GET_BAG_CAROUSEL_MODULE_DATA_FROM_STORAGE,
    bagCarouselModuleAsyncStorage
  );
  yield takeLatest(BAG_CAROUSEL_CONSTANTS.RESET_BAG_CAROUSEL_MODULE_DATA, resetBagCarouselData);
}

export default BagCarouselModuleSaga;
