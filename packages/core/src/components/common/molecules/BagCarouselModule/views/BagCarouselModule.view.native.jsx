// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import { goToPdpPage } from '@tcp/core/src/utils/index.native';
import { Button, DamImage } from '../../../atoms';
import {
  Wrapper,
  StyledBodyCopy,
  ButtonContainer,
  ImageTouchableOpacity,
  Container,
} from '../styles/BagCarouselModule.style.native';
import { getLocator } from '../../../../../utils/index.native';
import constants from '../container/BagCarouselModule.constants';

class BagCarouselModule extends React.Component {
  /**
   * @function itemRenderer
   * This function will be used to render flat list item
   */
  itemRenderer = imgSource => {
    const { index, item = {} } = imgSource;
    const { navigation, labels, updateAppTypeHandler } = this.props;
    const { pdpUrl, productPartNumber, itemBrand, name, imagePath } = item;
    return (
      <ImageTouchableOpacity
        onPress={() => {
          goToPdpPage(
            { productColorId: productPartNumber, name, pdpUrl, brandId: itemBrand },
            updateAppTypeHandler,
            navigation,
            {
              icidParam: labels.productIcidParam || constants.DEFAULT_PRODUCT_ICID,
              viaModule: 'bag_carousel_module',
            }
          );
        }}
        accessible={index}
        accessibilityRole="image"
        accessibilityLabel={`image ${index + 1}`}
      >
        <DamImage
          isProductImage
          alt={name}
          url={imagePath}
          resizeMode="stretch"
          width={constants.PRODUCT_IMAGE_WIDTH}
          height={constants.PRODUCT_IMAGE_HEIGHT}
          itemBrand={itemBrand && itemBrand.toLowerCase()}
        />
      </ImageTouchableOpacity>
    );
  };

  render() {
    const { labels, navigation, cartOrderItems, isBagCarouselEnabled, threSholdValue } = this.props;
    if (
      !isBagCarouselEnabled ||
      threSholdValue > cartOrderItems.orderTotal ||
      !cartOrderItems.products.length
    ) {
      return null;
    }
    const productData = cartOrderItems.products;
    return productData && productData.length > 0 ? (
      <Wrapper>
        <StyledBodyCopy
          text={labels.heading}
          textAlign="center"
          fontWeight="medium"
          fontFamily="primary"
          fontSize="fs20"
          lineHeight="20px"
        />
        <Container>
          <FlatList
            initialNumToRender={3}
            initialScrollIndex={0}
            refreshing
            data={productData}
            horizontal
            showsHorizontalScrollIndicator={false}
            listKey={(_, index) => index.toString()}
            renderItem={this.itemRenderer}
          />
        </Container>
        <ButtonContainer>
          <Button
            width="225px"
            navigation={navigation}
            text={labels.buttonText}
            testID={getLocator('bag_carousel_cta_btn')}
            onPress={() => {
              navigation.navigate('BagPage', {
                icidParam: labels.viewBtnIcidParam || constants.DEFAULT_VIEW_BUTTOM_ICID,
                viaModule: 'bag_carousel_module',
              });
            }}
          />
        </ButtonContainer>
      </Wrapper>
    ) : null;
  }
}

BagCarouselModule.propTypes = {
  labels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  updateAppTypeHandler: PropTypes.func,
  cartOrderItems: PropTypes.shape({}),
  threSholdValue: PropTypes.number,
  isBagCarouselEnabled: PropTypes.bool,
};

BagCarouselModule.defaultProps = {
  labels: {},
  navigation: {},
  cartOrderItems: {
    products: [],
    orderTotal: 0,
  },
  updateAppTypeHandler: () => {},
  isBagCarouselEnabled: false,
  threSholdValue: 0,
};

export default BagCarouselModule;
export { BagCarouselModule as BagCarouselModuleVanilla };
