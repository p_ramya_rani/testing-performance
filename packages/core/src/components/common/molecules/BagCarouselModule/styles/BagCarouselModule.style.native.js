// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { BodyCopy } from '../../../atoms';

export const Wrapper = styled.View`
  width: 100%;
  margin-top: ${props => props.theme.spacing.MODULE_SPACING.MED};
  margin-bottom: ${props => props.theme.spacing.MODULE_SPACING.MED};
`;

export const StyledBodyCopy = styled(BodyCopy)`
  width: 100%;
  margin-bottom: 16px;
`;

export const ButtonContainer = styled.View`
  align-items: center;
`;

export const ImageTouchableOpacity = styled.TouchableOpacity`
  margin-right: 16px;
`;

export const Container = styled.View`
  padding-left: 16px;
  margin-bottom: 16px;
`;

export default {
  Wrapper,
  StyledBodyCopy,
  ButtonContainer,
  ImageTouchableOpacity,
};
