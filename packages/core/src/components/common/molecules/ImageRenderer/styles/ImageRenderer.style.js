// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

export const PortalContainer = styled.div`
  position: absolute;
  box-sizing: border-box;
  pointer-events: none;
  width: ${props => props.width}px;
  height: ${props => props.height}px;
  top: 0;
  left: 0;
  overflow: hidden;
  opacity: ${props => (props.isActive ? 1 : 0)};
  transition: opacity 0.4s ease;
  z-index: 100;
  border: 1px solid ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
  box-shadow: 0 ${props => props.theme.spacing.ELEM_SPACING.XXS}
    ${props => props.theme.spacing.ELEM_SPACING.XS} rgba(0, 0, 0, 0.5);
`;

export default PortalContainer;
