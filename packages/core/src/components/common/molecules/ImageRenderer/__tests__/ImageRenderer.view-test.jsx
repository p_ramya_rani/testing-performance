// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';

import { ImageRendererVanilla } from '../views/ImageRenderer.view';

describe('ImageRenderer component', () => {
  it('should renders correctly', () => {
    const props = {
      onImageLoad: () => {},
      imgData: {
        alt: 'Sample Image',
        url:
          'https://test1.theplace.com/image/upload/w_500,d_plpDefault.png,f_auto,q_auto/v1/ecom/assets/products/tcp/3014463/3014463_6B.jpg',
      },
      largeImgData: {
        alt: 'Sample Image',
        url:
          'https://test1.theplace.com/image/upload/w_900,d_plpDefault.png,f_auto,q_auto/v1/ecom/assets/products/tcp/3014463/3014463_6B.jpg',
      },
      className: 'class-name',
      active: true,
      elementDimensions: { width: 438, height: 544 },
      itemDimensions: { width: 900, height: 1119 },
      itemRef: () => {},
      cursorStyle: 'crosshair',
    };
    const component = shallow(<ImageRendererVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
