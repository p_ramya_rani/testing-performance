// 9fbef606107a605d69c0edbcd8029e5d 
import ImageRendererUtils from '../views/ImageRenderer.utils';

describe('ImageRendererUtils', () => {
  it('should return dimensions correctly', () => {
    const props = {
      itemPosition: { x: 318, y: 177 },
      elementDimensions: { width: 438, height: 544 },
      itemDimensions: { width: 900, height: 1119 },
    };
    const { previewSize, position, zoomContainerSize, smallImageSize } =
      ImageRendererUtils(props) || {};
    expect(previewSize).toEqual({
      width: 213,
      height: 264,
      left: 211,
      right: 424,
      top: 45,
      bottom: 309,
    });
    expect(position).toEqual({ x: -434.28, y: -92.41071428571428 });
    expect(zoomContainerSize).toEqual({ width: 438, height: 544 });
    expect(smallImageSize).toEqual({ width: 438, height: 544 });
  });
});
