// 9fbef606107a605d69c0edbcd8029e5d 
import logger from '../../../../../utils/loggerInstance';

const imageRendererUtil = props => {
  try {
    const { itemPosition, elementDimensions, itemDimensions, productSectionWidth } = props;

    const invertNumber = (min, max, num) => {
      return max + min - num;
    };

    const convertRange = (oldMin, oldMax, newMin, newMax, oldValue) => {
      const percent = (oldValue - oldMin) / (oldMax - oldMin);
      const result = percent * (newMax - newMin) + newMin;
      return result || 0;
    };

    const zoomContainerDimensions = {
      width: elementDimensions.width,
      height: elementDimensions.height,
    };

    const smallImageSize = {
      width: elementDimensions.width,
      height: elementDimensions.height,
    };

    const previewSize = {
      width: Math.floor(
        smallImageSize.width * (zoomContainerDimensions.width / itemDimensions.width)
      ),
      height: Math.floor(
        smallImageSize.height * (zoomContainerDimensions.height / itemDimensions.height)
      ),
    };

    let position = { x: 0, y: 0 };
    const itemPositionAdj = { ...itemPosition };

    const previewOffset = {
      x: previewSize.width / 2,
      y: previewSize.height / 2,
    };

    itemPositionAdj.x = Math.max(previewOffset.x, itemPositionAdj.x);
    itemPositionAdj.x = Math.min(smallImageSize.width - previewOffset.x, itemPositionAdj.x);
    itemPositionAdj.y = Math.max(previewOffset.y, itemPositionAdj.y);
    itemPositionAdj.y = Math.min(smallImageSize.height - previewOffset.y, itemPositionAdj.y);

    position = { ...itemPositionAdj };

    const zoomContainerSize = {
      width: productSectionWidth || elementDimensions.width,
      height: elementDimensions.height,
    };

    position.x = convertRange(
      previewOffset.x,
      smallImageSize.width - previewOffset.x,
      itemDimensions.width * -1 + zoomContainerSize.width,
      0,
      position.x
    );
    position.y = convertRange(
      previewOffset.y,
      smallImageSize.height - previewOffset.y,
      itemDimensions.height * -1 + zoomContainerSize.height,
      0,
      position.y
    );

    position.x = invertNumber(itemDimensions.width * -1 + zoomContainerSize.width, 0, position.x);
    position.y = invertNumber(itemDimensions.height * -1 + zoomContainerSize.height, 0, position.y);

    previewSize.left = Math.floor(itemPositionAdj.x - previewOffset.x) || 0;
    previewSize.right = Math.floor(itemPositionAdj.x + previewOffset.x) || 0;
    previewSize.top = Math.floor(itemPositionAdj.y - previewOffset.y) || 0;
    previewSize.bottom = Math.floor(itemPositionAdj.y + previewOffset.y) || 0;

    return { previewSize, position, zoomContainerSize, smallImageSize };
  } catch (err) {
    logger.error('ImageRendererUtil: ', err);
    return {};
  }
};

export default imageRendererUtil;
