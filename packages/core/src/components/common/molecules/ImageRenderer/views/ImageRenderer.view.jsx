// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { getAPIConfig, removeExtension } from '@tcp/core/src/utils';
import ImageOverlayRenderer from '../../ImageOverlayRenderer/views/ImageOverlayRenderer.view';
import { DamImage, BodyCopy } from '../../../atoms';
import { PortalContainer } from '../styles/ImageRenderer.style';
import ImageRendererUtil from './ImageRenderer.utils';

const ImageRenderer = (props) => {
  const {
    active,
    elementDimensions,
    itemDimensions,
    itemRef,
    cursorStyle,
    onImageLoad,
    onLargeImageLoad,
    imgData,
    largeImgData,
    className,
    imgConfig,
    dynamicBadgeOverlayQty,
    badgeConfig,
    tcpStyleType,
    primaryBrand,
    alternateBrand,
  } = props;
  const [isMounted, setisMounted] = useState(false);
  useEffect(() => {
    setisMounted(true);
  }, []);

  const legalSize = itemDimensions.width > elementDimensions.width;
  const isActive = legalSize && active;
  const portalId = isMounted && document.getElementById('portal');
  const {
    previewSize = {},
    position = {},
    zoomContainerSize = {},
    smallImageSize = {},
  } = isMounted ? ImageRendererUtil(props) : {};
  const CustomStyle = {
    width: '100%',
    display: 'block',
    cursor: legalSize ? cursorStyle : 'default',
  };

  const largeImageStyle = {
    position: 'absolute',
    boxSizing: 'border-box',
    display: 'block',
    top: 0,
    left: 0,
    transform: `translate(${position.x}px, ${position.y}px)`,
    zIndex: '1',
    visibility: 'visible',
    width: 'auto',
  };

  const getBrandName = (primaryBrandName, alternateBrandName) => {
    return primaryBrandName || alternateBrandName;
  };

  const PortalContent = () => {
    let { brandId } = getAPIConfig();
    const { staticAssetHost } = getAPIConfig();
    if (primaryBrand) {
      brandId = primaryBrand;
    }
    if (alternateBrand) {
      brandId = alternateBrand && alternateBrand.toLowerCase();
    }
    // FIX ME: ref is not being passed currectly to StyledImage in Damimage, so had to do this was as a work around.
    const imgSource = `${staticAssetHost}/image/upload/w_900,d_plpDefault.png,f_auto,q_auto,dpr_2.0/v1/ecom/assets/products/${brandId}/${removeExtension(
      largeImgData.url
    )}`;
    return active ? (
      <PortalContainer
        width={zoomContainerSize.width}
        height={zoomContainerSize.height}
        isActive={isActive}
      >
        <img
          style={largeImageStyle}
          onLoad={onLargeImageLoad}
          src={imgSource}
          alt={largeImgData.alt}
          ref={itemRef}
        />
      </PortalContainer>
    ) : null;
  };
  const brandName = getBrandName(primaryBrand, alternateBrand);
  return (
    <BodyCopy component="div" className={className}>
      {isMounted && portalId ? ReactDOM.createPortal(<PortalContent />, portalId) : null}
      <DamImage
        style={CustomStyle}
        onLoad={onImageLoad}
        isProductImage
        imgData={imgData}
        imgConfigs={imgConfig}
        lazyLoad={false}
        dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
        badgeConfig={badgeConfig}
        tcpStyleType={tcpStyleType}
        primaryBrand={brandName}
      />
      {isMounted && (
        <ImageOverlayRenderer
          previewWidth={previewSize.width}
          previewHeight={previewSize.height}
          previewPosLeft={previewSize.left}
          previewPosRight={previewSize.right}
          previewPosTop={previewSize.top}
          previewPosBottom={previewSize.bottom}
          imageWidth={smallImageSize.width}
          imageHeight={smallImageSize.height}
          active={isActive}
          primaryBrand={brandName}
        />
      )}
    </BodyCopy>
  );
};

ImageRenderer.propTypes = {
  itemPosition: PropTypes.shape({}).isRequired,
  active: PropTypes.bool.isRequired,
  elementDimensions: PropTypes.shape({}).isRequired,
  itemDimensions: PropTypes.shape({}).isRequired,
  itemRef: PropTypes.func,
  cursorStyle: PropTypes.string.isRequired,
  onImageLoad: PropTypes.func,
  onLargeImageLoad: PropTypes.func,
  imgData: PropTypes.shape({}).isRequired,
  largeImgData: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  imgConfig: PropTypes.shape([]).isRequired,
  dynamicBadgeOverlayQty: PropTypes.number,
  badgeConfig: PropTypes.arrayOf(PropTypes.string).isRequired,
  tcpStyleType: PropTypes.string,
};

ImageRenderer.defaultProps = {
  itemRef: () => {},
  onImageLoad: () => {},
  onLargeImageLoad: () => {},
  dynamicBadgeOverlayQty: 0,
  tcpStyleType: '',
};

export default ImageRenderer;
export { ImageRenderer as ImageRendererVanilla };
