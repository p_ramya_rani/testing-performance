// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  img {
    max-height: 427px;
    width: 100%;
    height: 100%;
    margin: 0 auto;
    object-fit: contain;
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    img {
      max-height: 436px;
      height: 100%;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    img {
      max-height: ${(props) => (props.isPlpQV ? '328px' : '703px')};
      width: 100%;
      height: 100%;
    }
  }
`;

export default styles;
