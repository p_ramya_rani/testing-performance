// 9fbef606107a605d69c0edbcd8029e5d
// Temp fix for DAM image in PLP
import React from 'react';
import PropTypes from 'prop-types';
import ExecutionEnvironment from 'exenv';
import ReactInputPosition, { MOUSE_ACTIVATION } from 'react-input-position';
import { getVideoUrl, isSafariBrowser, getDynamicBadgeQty } from '@tcp/core/src/utils/utils';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { HERO_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import useBooleanState from '@tcp/core/src/hooks/useBooleanState';
import { DamImage, Anchor, BodyCopy } from '../../atoms';
import withStyles from '../../hoc/withStyles';
import styles from './ProductDetailImage.style';
import { getLocator } from '../../../../utils';
import config from './config';
import ImageRenderer from '../ImageRenderer/views/ImageRenderer.view';

const getVideoTransformation = ({ VID_DATA_CLIENT, VID_DATA_CLIENT_WEBP }) =>
  isSafariBrowser() ? VID_DATA_CLIENT : VID_DATA_CLIENT_WEBP;

const NonZoomImage = (imgProps) => {
  const {
    className,
    isMobile,
    imageUrl,
    imageName,
    onOpenSimpleFullSize,
    fromSkeleton,
    dynamicBadgeOverlayQty,
    badgeConfig,
    tcpStyleType,
    fromPLPPage,
    isNewQVEnabled,
    primaryBrand,
    alternateBrand,
    isStyleWith,
  } = imgProps;
  const [isImageLoaded, handleImageLoaded] = useBooleanState(false);
  const imgData = {
    alt: imageName,
    url: imageUrl,
  };
  const imageWrapperHeight = {
    minHeight: isNewQVEnabled ? '200px' : '380px',
  };

  if (isStyleWith) {
    imageWrapperHeight.minHeight = '100px';
  }

  const imgConfig = fromPLPPage ? config.IMG_DATA_CLIENT : config.IMG_DATA;
  const isVideoUrl = getVideoUrl(imgData.url);
  const skeletonConfig = isVideoUrl ? getVideoTransformation(config) : imgConfig;
  const mainConfig = isVideoUrl ? config.VID_DATA : imgConfig;

  return (
    <>
      {' '}
      {!isMobile ? (
        <DamImage
          className={`${className} full-size-desktop-image`}
          data-locator={getLocator('pdp_main_image')}
          imgData={imgData}
          itemProp="contentUrl"
          isProductImage
          lazyLoad={false}
          imgConfigs={fromSkeleton ? skeletonConfig.imgConfig : mainConfig.imgConfig}
          onLoad={handleImageLoaded}
          isOptimizedVideo={fromSkeleton}
          dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
          badgeConfig={badgeConfig}
          tcpStyleType={tcpStyleType}
          primaryBrand={primaryBrand || alternateBrand}
        />
      ) : (
        <BodyCopy style={imageWrapperHeight}>
          <Anchor aria-label="view full size image" onClick={onOpenSimpleFullSize}>
            <DamImage
              className="full-size-desktop-image"
              data-locator={getLocator('pdp_main_image')}
              imgData={imgData}
              itemProp="contentUrl"
              isProductImage
              lazyLoad={false}
              imgConfigs={fromSkeleton ? skeletonConfig.imgConfig : mainConfig.imgConfig} // TODO check for imageConfig in case of having cloudinary name form for bundle
              onLoad={handleImageLoaded}
              isOptimizedVideo={fromSkeleton}
              dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
              badgeConfig={badgeConfig}
              tcpStyleType={tcpStyleType}
              primaryBrand={primaryBrand || alternateBrand}
            />
          </Anchor>
        </BodyCopy>
      )}
      {isImageLoaded && <RenderPerf.Measure name={HERO_VISIBLE} />}
    </>
  );
};

const ProductDetailImage = (props) => {
  const {
    imageName,
    imageUrl,
    zoomImageUrl,
    className,
    isZoomEnabled,
    onOpenSimpleFullSize,
    isMobile,
    imageConfig,
    fromSkeleton,
    TCPStyleQTY,
    TCPStyleType,
    isDynamicBadgeEnabled,
    fromPLPPage,
    isNewQVEnabled,
    primaryBrand,
    alternateBrand,
    isStyleWith,
  } = props;
  const [isImageLoaded, handleImageLoaded] = useBooleanState(false);
  let productSectionWidth;
  if (ExecutionEnvironment.canUseDOM) {
    productSectionWidth =
      document.getElementById('productDetailsSection') &&
      document.getElementById('productDetailsSection').offsetWidth;
  }
  const imgData = {
    alt: imageName,
    url: imageUrl,
  };
  const largeImgData = {
    alt: imageName,
    url: zoomImageUrl,
  };
  const style = { order: '0', width: '100%', position: 'relative' };
  const { imgConfig, badgeConfig } = fromPLPPage ? config.IMG_DATA_CLIENT : config.IMG_DATA;
  const { badgeConfig: badgeConfigMobile } = config.IMG_DATA_CLIENT;

  const dynamicBadgeOverlayQty = getDynamicBadgeQty(
    TCPStyleType,
    TCPStyleQTY,
    isDynamicBadgeEnabled
  );

  return (
    <div itemScope itemType="http://schema.org/ImageObject" className={className} title={imageName}>
      {isZoomEnabled && !fromSkeleton && !isMobile ? (
        <>
          <ReactInputPosition
            className="input-position"
            style={style}
            mouseActivationMethod={MOUSE_ACTIVATION.HOVER}
            trackItemPosition
            linkItemToActive
          >
            <ImageRenderer
              cursorStyle="crosshair"
              onImageLoad={handleImageLoaded}
              imgData={imgData}
              largeImgData={largeImgData}
              imgConfig={imgConfig}
              productSectionWidth={productSectionWidth}
              dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
              tcpStyleType={TCPStyleType}
              badgeConfig={badgeConfig}
              primaryBrand={primaryBrand}
              alternateBrand={alternateBrand}
            />
          </ReactInputPosition>
          {isImageLoaded && <RenderPerf.Measure name={HERO_VISIBLE} />}
        </>
      ) : (
        <NonZoomImage
          badgeConfig={badgeConfigMobile}
          tcpStyleType={TCPStyleType}
          {...{
            className,
            isMobile,
            imageUrl,
            imageName,
            onOpenSimpleFullSize,
            imageConfig,
            fromSkeleton,
            dynamicBadgeOverlayQty,
            fromPLPPage,
            isNewQVEnabled,
            primaryBrand,
            alternateBrand,
            isStyleWith,
          }}
        />
      )}
    </div>
  );
};

ProductDetailImage.propTypes = {
  imageName: PropTypes.string,

  /** the image url */
  imageUrl: PropTypes.string.isRequired,
  isMobile: PropTypes.bool.isRequired,

  /** zoom image url */
  zoomImageUrl: PropTypes.string.isRequired,

  className: PropTypes.string,
  isZoomEnabled: PropTypes.bool,
  onOpenSimpleFullSize: PropTypes.func,
  imageConfig: PropTypes.arrayOf(PropTypes.string),
  fromSkeleton: PropTypes.bool,
  TCPStyleQTY: PropTypes.number,
  TCPStyleType: PropTypes.string,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  fromPLPPage: PropTypes.bool,
  isNewQVEnabled: PropTypes.bool,
};

ProductDetailImage.defaultProps = {
  className: '',
  imageName: '',
  isZoomEnabled: true,
  onOpenSimpleFullSize: () => {},
  imageConfig: [],
  fromSkeleton: false,
  TCPStyleQTY: 0,
  TCPStyleType: '',
  isDynamicBadgeEnabled: false,
  fromPLPPage: false,
  isNewQVEnabled: false,
};

export default withStyles(ProductDetailImage, styles);
export { ProductDetailImage as ProductDetailImageVanilla };
