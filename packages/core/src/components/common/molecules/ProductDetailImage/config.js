// 9fbef606107a605d69c0edbcd8029e5d
const badgeTranformation = 'g_west,w_0.22,fl_relative';
const badgeTranformationBag = 'g_west,w_0.22,fl_relative'; // Added new const specific to bagpage so that badge size can be increased on bag page

export default {
  IMG_DATA: {
    imgConfig: ['t_pdp_img_m,f_auto', 't_pdp_img_t,f_auto', 't_pdp_img_d,f_auto'],
    badgeConfig: [badgeTranformation, badgeTranformation, badgeTranformation],
  },

  IMG_DATA_CLIENT: {
    imgConfig: ['t_plp_img_m,f_auto', 't_plp_img_t,f_auto', 't_plp_img_d,f_auto'],
    badgeConfig: [badgeTranformation, badgeTranformation, badgeTranformation],
  },

  IMG_DATA_CLIENT_BAGPAGE: {
    imgConfig: ['t_t_cart_item_tile_m', 't_t_cart_item_tile_t', 't_t_cart_item_tile_d'],
    badgeConfig: [badgeTranformationBag, badgeTranformationBag, badgeTranformationBag],
  },

  VID_DATA: {
    imgConfig: ['t_pdp_vid_m', 't_pdp_vid_t', 't_pdp_vid_d'],
  },

  VID_DATA_CLIENT: {
    imgConfig: ['t_plp_vid_m', 't_plp_vid_t', 't_plp_vid_d'],
  },

  VID_DATA_CLIENT_WEBP: {
    imgConfig: ['e_loop,t_plp_webp_m', 'e_loop,t_plp_webp_t', 'e_loop,t_plp_webp_d'],
  },
};
