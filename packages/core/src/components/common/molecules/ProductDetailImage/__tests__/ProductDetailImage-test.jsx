// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';

import { ProductDetailImageVanilla } from '../ProductDetailImage';

describe('ProductDetailImage component', () => {
  it('should renders correctly', () => {
    const props = {
      imageName: '',
      imageUrl: '',
      zoomImageUrl: '',
      className: '',
    };
    const component = shallow(<ProductDetailImageVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly when isDynamicBadgeEnabled kill switch is disabled', () => {
    const props = {
      imageName: '',
      imageUrl: '',
      zoomImageUrl: '',
      className: '',
      isDynamicBadgeEnabled: false,
      TCPStyleQTY: 0,
      TCPStyleType: '0002',
    };
    const component = shallow(<ProductDetailImageVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly when isDynamicBadgeEnabled kill switch is enabled', () => {
    const props = {
      imageName: '',
      imageUrl: '',
      zoomImageUrl: '',
      className: '',
      isDynamicBadgeEnabled: true,
      TCPStyleQTY: 10,
      TCPStyleType: '0002',
    };
    const component = shallow(<ProductDetailImageVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
