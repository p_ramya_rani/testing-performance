// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const StyledGrid = css`
  ${props => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default StyledGrid;
