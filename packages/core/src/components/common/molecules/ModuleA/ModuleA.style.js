// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { cropImageUrl, getImageFilePath } from '@tcp/core/src/utils';
import CarouselResponsive from '@tcp/core/src/components/common/molecules/Carousel/views/CarouselResponsive';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';

const getCtaBgImage = (bgImageObj) => {
  const {
    ctaBackgroundImage: { url: ctaDesktopUrl, url_t: ctaTabletUrl, url_m: ctaMobileUrl },
  } = bgImageObj || {};
  return {
    desktop: ctaDesktopUrl && cropImageUrl(ctaDesktopUrl),
    tablet: ctaTabletUrl ? cropImageUrl(ctaTabletUrl) : cropImageUrl(ctaDesktopUrl),
    mobile: ctaMobileUrl ? cropImageUrl(ctaMobileUrl) : cropImageUrl(ctaDesktopUrl),
  };
};

const StyledLinkText = styled(LinkText)`
  display: inline-block;

  .link-text {
    margin-bottom: 0;
  }
`;

const StyledCarousel = styled(CarouselResponsive)`
  .control-arrow {
    background-size: 100% 100%;
    height: 42px;
    width: 13px;
    z-index: 1;
  }
  .control-dots {
    bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG_2};
    z-index: 1;
  }
  @media only screen and (max-width: 767px) {
    .control-arrow.control-prev,
    .control-arrow.control-next {
      display: none;
    }
  }
`;

const StyledButtonListContainer = styled.div`
  ${(props) =>
    props.ctaBgImageObj && getCtaBgImage(props.ctaBgImageObj)
      ? `
        background-image: url(${getCtaBgImage(props.ctaBgImageObj).mobile});
        @media ${props.theme.mediaQuery.large} {
          background-image: url(${getCtaBgImage(props.ctaBgImageObj).desktop});
        }
        @media ${props.theme.mediaQuery.mediumOnly} {
          background-image: url(${getCtaBgImage(props.ctaBgImageObj).tablet});
        }
      `
      : null};

  ${(props) =>
    props.hasBackgroundImage
      ? `
      left: 0%;
      right: 0%;
      bottom: 0;
      @media ${props.theme.mediaQuery.medium} {
          bottom: 16px;
          position: absolute;

        }
      @media ${props.theme.mediaQuery.smallOnly} {
        &&.button-list-container.stackedCTAList {
            padding: 16px;
          }
        }
    `
      : ``};
`;

const style = css`
  position: relative;
  ${(props) =>
    props.isBorderWidth
      ? `
      margin-left:0;
      margin-right:0;
      width:100%;`
      : ``}

  .bg_position {
    @media ${(props) => props.theme.mediaQuery.medium} {
      position: absolute;
      height: 100%;
    }
  }

  &.gymboree-module-a {
    z-index: 0;
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
  }

  .control-dots li:before {
    border: 0.5px solid ${(props) => props.theme.colorPalette.white};
  }

  .control-dots li.selected:before {
    border: 1px solid ${(props) => props.theme.colorPalette.gray[700]};
  }

  .banner-slide {
    position: relative;
    img {
      width: 100%;
    }
  }

  .single-image-header {
    left: 50%;
    position: relative;
    transform: translate(-50%);
  }

  .banner-content {
    position: absolute;
    transform: translateY(-50%);
    top: 50%;
    left: 0;
    width: 100%;
    line-height: 14px;
  }

  .bg_banner-content {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      position: relative;
      transform: none;
    }
  }

  .stacked-cta-wrapper-class {
    padding-top: 16px;
    padding-right: 20px;
    padding-bottom: 16px;
    padding-left: 20px;
    color: ${(props) => props.theme.colorPalette.gray[800]};
  }

  .large_text_normal {
    color: ${(props) => props.theme.colorPalette.gray[900]};
  }

  &.gymboree-module-a .banner-content {
    transform: translate(-50%, 0);
    top: 8.7%;
    left: 50%;
    width: 60%;

    @media ${(props) => props.theme.mediaQuery.medium} {
      left: 14px;
      width: 255px;
      top: 50%;
      transform: translate(0%, -50%);
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      line-height: 90px;
      top: 50%;
      left: 120px;
      transform: translate(0%, -50%);
      width: 470px;

      .link-text-wrapper {
        width: 100%;
      }
    }
  }

  .tcp_carousel_wrapper {
    position: relative;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      overflow: hidden;
    }
  }

  .moduleA_carousel_wrapper {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 100%;
    }
  }

  .moduleA__bg-img-wrapper {
    width: 100%;
    img {
      width: 100%;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      position: absolute;
      height: 100%;
      img {
        height: 100%;
      }
    }
  }

  .tcp_carousel_wrapper .slider-wrapper,
  .banner-slide {
    min-height: 200px;

    @media ${(props) => props.theme.mediaQuery.medium} {
      min-height: 296px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      min-height: 400px;
    }
  }

  .button-list-wrapper {
    padding: 0;
  }

  .button-list-container {
    padding: 16px 0;
  }

  .button-list-container.stackedCTAList {
    padding: 0;
  }

  .button-list-container.imageCTAList .image-comp,
  .button-list-container.linkCTAList .link-button-wrapper-class {
    color: ${(props) =>
      props.theme.isGymboree
        ? props.theme.colors.BUTTON.WHITE
        : props.theme.colors.BUTTON[props.fill || 'WHITE'].TEXT};
  }
  .button-list-container.linkCTAList .link-button-wrapper-class {
    border-bottom-color: #ffffff;
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) =>
        props.theme.isGymboree ? `font-weight: ${props.theme.typography.fontWeights.bold};` : ''}
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .button-list-container.stackedCTAList {
      padding: 16px 0;

      .wrapped-button-text .stacked-button .cta-button-text {
        padding: 11px 0;
      }
    }
  }

  .moduleA__ribbonBanner {
    font-size: 14px;
    padding-left: 10px;
  }

  .ribbon-container {
    background: transparent url('${(props) => getImageFilePath(props)}/module-a-ribbon-right.png')
      no-repeat right 0;
    background-size: 100% auto;
    position: absolute;
    background-position: left;
    right: 0;
    bottom: 12px;
    width: 174px;
    height: 54px;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 1;

    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 196px;
      height: 61px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 266px;
      height: 71px;
    }
  }

  &.gymboree-module-a .imageCTAList .image-comp,
  &.gymboree-module-a .linkCTAList .link-button-wrapper-class {
    color: ${(props) => props.theme.colors.BUTTON[props.fill || 'BLACK'].TEXT};
  }

  &.gymboree-module-a .control-dots {
    bottom: 30px;
    left: 40px;
    justify-content: flex-start;
  }

  .moduleA__promoBanner {
    line-height: unset;
  }

  &.gymboree-module-a .moduleA__promoBanner {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  &.tcp-module-a {
    .moduleA__promoBanner {
      line-height: normal;
    }
  }

  &.tcp-module-a .moduleA__promoBanner .promo-text-link {
    display: flex;
    flex-direction: column;

    .promo-text.extra_large_text_regular {
      color: #fa579f;
      text-shadow: 0 3px 6px rgba(0, 0, 0, 0.5);
      font-weight: 700;
      font-family: TofinoCond;
      font-size: 130px;
      width: 50%;
      margin: 0px auto 8px;
      line-height: 0.76;
      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: 260px;
        line-height: 0.9;
        width: 100%;
      }
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        font-size: 160px;
        line-height: 0.9;
        width: 100%;
      }
    }
  }

  &.tcp-module-a.page-home .spaced_text {
    font-family: TofinoCond;
    font-weight: 700;
    font-size: 60px;
    letter-spacing: normal;
    line-height: 1;
    text-shadow: 0 3px 6px rgba(0, 0, 0, 0.5);
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 88px;
    }
  }

  &.tcp-module-a.page-home .percentage_wrapped_extra_large {
    font-family: TofinoCond;
    width: auto;
    @media ${(props) => props.theme.mediaQuery.medium} {
      transform: none;
    }
  }

  &.tcp-module-a.page-home .percentage_wrapped_extra_large-0,
  &.tcp-module-a.page-home .percentage_wrapped_extra_large-1,
  &.tcp-module-a.page-home .percentage_wrapped_extra_large-2 {
    color: #fa579f;
    text-shadow: 0 3px 6px rgba(0, 0, 0, 0.5);
    font-weight: 700;
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 260px;
    }
  }

  &.tcp-module-a.page-home .moduleA__promoBanner .small_text_normal {
    font-family: Nunito;
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 18px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    &.gymboree-module-a .moduleA__promoBanner {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }

  &.left-aligned-ribbon {
    .control-dots {
      justify-content: flex-end;
      right: 15px;
      left: auto;
    }

    .ribbon-container {
      background: transparent url('${(props) => getImageFilePath(props)}/module-a-ribbon-left.png')
        no-repeat 0 0;
      background-size: 100% auto;
      background-position: right;
      right: auto;
      left: 0;
    }

    .moduleA__ribbonBanner {
      padding-right: 10px;
      padding-left: 0px;
    }
  }
`;

export { StyledLinkText as LinkText, StyledCarousel as Carousel, style, StyledButtonListContainer };

export default {
  LinkText: StyledLinkText,
  StyledCarousel: CarouselResponsive,
  style,
  StyledButtonListContainer,
};
