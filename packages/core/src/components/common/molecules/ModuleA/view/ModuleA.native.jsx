// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable no-shadow */
// @flow
import React, { useState } from 'react';
import { View } from 'react-native';
import { isIOS } from '@tcp/core/src/utils/utils.app';
import ButtonList from '../../ButtonList';
import Carousel from '../../Carousel';
import { DamImage, Image, Anchor } from '../../../atoms';
import LinkText from '../../LinkText';
import PromoBanner from '../../PromoBanner';
import {
  isGymboree,
  getScreenWidth,
  LAZYLOAD_HOST_NAME,
  styleOverrideEngine,
} from '../../../../../utils/index.native';
import {
  Container,
  ButtonContainer,
  ContainerView,
  DivImageCTAContainer,
  ButtonLinksContainer,
  HeaderWrapper,
  MessageContainer,
  RibbonBannerHeight,
  RibbonBannerWidth,
  PromoBannerWrapper,
  HeaderView,
  PromoRibbonWrapper,
  PromoBannerWrapperHalfView,
  WrapTextContainerView,
} from '../ModuleA.style.native';
import config from '../config';
import {
  ModulePropType,
  ModuleAdefaultProps,
  ribbonViewPropType,
  ribbonViewdefaultProps,
  RenderDamImagepropTypes,
} from './ModuleAProtoType';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';
import {
  getMediaBorderRadiusOverride,
  getCtaBorderRadiusOverride,
  computeModuleVideoWidth,
  computeModuleWidth,
} from './ModuleA.native.util';
/**
 * Module height and width.
 * Height is fixed for mobile : TCP & Gymb
 * Width can vary as per device width.
 */
const MODULE_TCP_HEIGHT = 412;
const MODULE_GYM_HEIGHT = 500;
const MODULE_WIDTH = getScreenWidth();
const ribbonLeftImage = require('../../../../../../../mobileapp/src/assets/images/module-a-ribbon-left.png');
const ribbonRightImage = require('../../../../../../../mobileapp/src/assets/images/module-a-ribbon-right.png');
// TODO: keys will be changed once we get the actual data from CMS
const { ctaTypes, IMG_DATA_TCP, IMG_DATA_GYM } = config;

const ribbonView = ({ ribbonBanner, navigation, position, ribbonStyleOverride }) => {
  let ribbonConfig = {
    width: RibbonBannerHeight,
    height: RibbonBannerWidth,
    source: ribbonRightImage,
  };
  if (position === 'right') {
    ribbonConfig = {
      ...ribbonConfig,
      source: ribbonLeftImage,
    };
  }
  const { ribbonStyle = {}, promoStyle } = ribbonStyleOverride || {};

  return (
    <ContainerView>
      {ribbonBanner && (
        <PromoRibbonWrapper viewdirection={position}>
          <Image {...ribbonConfig} style={ribbonStyle} />
          <MessageContainer>
            <PromoBanner
              promoBanner={ribbonBanner}
              navigation={navigation}
              locator="moduleA_promoribbonbanner_text"
              promoStyle={promoStyle}
            />
          </MessageContainer>
        </PromoRibbonWrapper>
      )}
    </ContainerView>
  );
};

const renderDamImage = (
  imgData,
  videoData,
  ignoreLazyLoadImage,
  { heightOverride, mediaBorderRadiusOverride, isHpNewLayoutEnabled },
  borderWidth,
  link,
  navigation
) => {
  const MODULE_OVERRIDE_HEIGHT = heightOverride && parseFloat(heightOverride.height);
  const COMPUTED_MODULE_WIDTH = computeModuleWidth(isHpNewLayoutEnabled, borderWidth, MODULE_WIDTH);
  return (
    <DamImage
      width={COMPUTED_MODULE_WIDTH}
      height={
        isGymboree()
          ? MODULE_OVERRIDE_HEIGHT || MODULE_GYM_HEIGHT
          : MODULE_OVERRIDE_HEIGHT || MODULE_TCP_HEIGHT
      }
      imgData={imgData}
      host={ignoreLazyLoadImage ? '' : LAZYLOAD_HOST_NAME.HOME}
      crop={imgData && imgData.crop_m}
      videoData={videoData}
      imgConfig={isGymboree() ? IMG_DATA_GYM.crops[0] : IMG_DATA_TCP.crops[0]}
      isFastImage
      resizeMode="stretch"
      isHomePage
      link={link}
      navigation={navigation}
      overrideStyle={isHpNewLayoutEnabled && mediaBorderRadiusOverride}
      isBackground={false}
    />
  );
};

const getAccessibilityLabel = (headerText, promoBanner) => {
  if (headerText) {
    const { textItems } = headerText[0];
    return textItems ? textItems.map(({ text }) => text).join(', ') : '';
  }
  if (promoBanner) {
    const { textItems } = promoBanner[0];
    return textItems ? textItems.map(({ text }) => text).join(', ') : '';
  }
  return '';
};

const getMediaImage = (imageMobile, videoMobile, image) => {
  if (!videoMobile) return imageMobile || image;
  return null;
};

const getMediaVideo = (imageMobile, videoMobile, video) => {
  if (!imageMobile) return videoMobile || video;
  return null;
};

const renderView = (
  item,
  navigation,
  position,
  ignoreLazyLoadImage,
  ribbonStyleOverride,
  headerStyle,
  borderWidth
) => {
  let PromoBannerComponent;
  let HeaderComponent;
  const { promoStyle, heightOverride, mediaBorderRadiusOverride, isHpNewLayoutEnabled } =
    ribbonStyleOverride || {};
  const {
    item: {
      headerText,
      promoBanner,
      ribbonBanner,
      linkedImage: [{ image, video, imageMobile, videoMobile, link }],
    },
  } = item;
  const textWidth = headerStyle && headerStyle.textWidth;
  const mediaImage = getMediaImage(imageMobile, videoMobile, image);
  const mediaVideo = getMediaVideo(imageMobile, videoMobile, video);

  if (isGymboree()) {
    PromoBannerComponent = PromoBannerWrapper;
    HeaderComponent = HeaderView;
  } else {
    PromoBannerComponent = PromoBannerWrapperHalfView;
    HeaderComponent = textWidth ? WrapTextContainerView : ContainerView;
  }
  const videoData = mediaVideo && {
    ...mediaVideo,
    videoWidth: computeModuleVideoWidth(isHpNewLayoutEnabled, MODULE_WIDTH),
    videoHeight: isGymboree() ? MODULE_GYM_HEIGHT : MODULE_TCP_HEIGHT,
  };

  return (
    <Anchor
      accessibilityLabel={getAccessibilityLabel(headerText, promoBanner)}
      url={link && link.url}
      navigation={navigation}
    >
      <ContainerView>
        {renderDamImage(
          mediaImage,
          videoData,
          ignoreLazyLoadImage,
          { heightOverride, mediaBorderRadiusOverride, isHpNewLayoutEnabled },
          borderWidth,
          link,
          navigation
        )}
        <HeaderWrapper>
          <HeaderComponent textWidth={textWidth} isIOS={isIOS()}>
            {headerText && (
              <LinkText
                navigation={navigation}
                headerText={headerText}
                locator="moduleA_header_text"
                textAlign="center"
                useStyle
                isModuleA
                headerStyle={headerStyle}
              />
            )}
          </HeaderComponent>
          <PromoBannerComponent>
            {promoBanner && (
              <PromoBanner
                promoBanner={promoBanner}
                navigation={navigation}
                locator="moduleA_promobanner_text"
                promoStyle={promoStyle}
              />
            )}
          </PromoBannerComponent>
        </HeaderWrapper>

        {ribbonView({ ribbonBanner, navigation, position, ribbonStyleOverride })}
      </ContainerView>
    </Anchor>
  );
};

const renderCarousel = (
  largeCompImageCarousel,
  navigation,
  position,
  ignoreLazyLoadImage,
  setCurCarouselIndex,
  ribbonStyleOverride,
  headerStyle,
  borderWidth
  // eslint-disable-next-line max-params
) => {
  let config = {};
  if (isGymboree()) {
    config = {
      height: MODULE_GYM_HEIGHT,
      buttonPosition: position,
    };
  } else {
    config = {
      height: MODULE_TCP_HEIGHT,
    };
  }
  const { isHpNewLayoutEnabled } = ribbonStyleOverride;
  return (
    <ContainerView>
      {largeCompImageCarousel && largeCompImageCarousel.length > 1 ? (
        <Carousel
          {...config}
          data={largeCompImageCarousel}
          renderItem={item =>
            renderView(
              item,
              navigation,
              position,
              ignoreLazyLoadImage,
              ribbonStyleOverride,
              headerStyle,
              borderWidth
            )
          }
          width={
            isHpNewLayoutEnabled ? MODULE_WIDTH - 2 * HP_NEW_LAYOUT.BODY_PADDING : MODULE_WIDTH
          }
          carouselConfig={{
            autoplay: true,
          }}
          onSnapToItem={index => setCurCarouselIndex(index)}
          showDots
          overlap
          isModule
        />
      ) : (
        <View>
          {renderView(
            { item: largeCompImageCarousel[0] },
            navigation,
            position,
            ignoreLazyLoadImage,
            ribbonStyleOverride,
            headerStyle,
            borderWidth
          )}
        </View>
      )}
    </ContainerView>
  );
};

/**
 * This method return the ButtonList View according to the different variation .
 *  @ctaType are four types : 'imageCTAList' ,'stackedCTAList','scrollCTAList','linkCTAList'.
 *  @naviagtion is used to navigate the page.
 */
const renderButtonList = (
  ctaType,
  navigation,
  ctaItems,
  locator,
  buttonVariation,
  color,
  { styleOverrides, isHpNewLayoutEnabled }
) => {
  const ctaBorderRadiusOverride = styleOverrides && getCtaBorderRadiusOverride(styleOverrides);

  return (
    ctaItems && (
      <ButtonList
        buttonListVariation={ctaType}
        navigation={navigation}
        buttonsData={ctaItems}
        locator={locator}
        color={color}
        buttonColor={styleOverrides && styleOverrides.button}
        overrideStyle={isHpNewLayoutEnabled && ctaBorderRadiusOverride}
      />
    )
  );
};

/* Returns the position of the ribbon in current slide. */
const getRibbonPosition = (largeCompImageCarousel, curSlideIndex) => {
  const curCarouselSlide = largeCompImageCarousel && largeCompImageCarousel[curSlideIndex];
  let ribbonPosition = 'left';

  if (curCarouselSlide && curCarouselSlide.ribbonBanner) {
    const [ribbonBanner = {}] = curCarouselSlide.ribbonBanner;
    const { ribbonPlacement } = ribbonBanner;

    /*
      Need to refactor the code. Currently if you set 'left' the ribbon is being aligned to
      right and vice versa.
    */
    if (ribbonPlacement === 'left') {
      ribbonPosition = 'right';
    }
  }

  return ribbonPosition;
};

/**
 * @param {object} props : Props for Module A multi type of banner list, button list, header text.
 * @desc This is Module A global component. It has capability to display
 * featured content module with 1 backckground color tiles ,links and a CTA Button list.
 * Author can surface teaser content leading to corresponding pages.
 * To manage the TCP And Gymboree View .
 */

const ModuleA = props => {
  const {
    navigation,
    largeCompImageCarousel,
    ctaItems,
    ctaType,
    ignoreLazyLoadImage,
    moduleClassName,
    borderWidth,
    isHpNewLayoutEnabled,
  } = props;
  const ctaTypeValue = ctaTypes[ctaType];
  const [curCarouselSlideIndex, setCurCarouselIndex] = useState(0);
  const ribbonPosition = getRibbonPosition(largeCompImageCarousel, curCarouselSlideIndex);
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleA') || {};
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
  const promoStyle = [styleOverrides.promo, styleOverrides.subPromoTitle];
  const heightOverride = styleOverrides.imgHeight;
  headerStyle.textWidth = styleOverrides.textWidth;
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(styleOverrides);

  return (
    <Container style={isHpNewLayoutEnabled && HP_NEW_LAYOUT.MODULE_BOX_SHADOW}>
      {renderCarousel(
        largeCompImageCarousel,
        navigation,
        ribbonPosition,
        ignoreLazyLoadImage,
        setCurCarouselIndex,
        {
          ribbonStyle: styleOverrides.ribbon,
          promoStyle,
          heightOverride,
          mediaBorderRadiusOverride,
          isHpNewLayoutEnabled,
        },
        headerStyle,
        borderWidth
      )}

      {ctaTypeValue === ctaTypes.divImageCTACarouse && (
        <DivImageCTAContainer style={styleOverrides['nav-bg']}>
          {renderButtonList(
            ctaTypeValue,
            navigation,
            ctaItems,
            'moduleA_cta_links',
            undefined,
            'black',
            { styleOverrides }
          )}
        </DivImageCTAContainer>
      )}

      {ctaTypeValue === ctaTypes.stackedCTAButtons && (
        <ContainerView style={styleOverrides['nav-bg']}>
          {renderButtonList(
            ctaTypeValue,
            navigation,
            ctaItems,
            'stacked_cta_list',
            'fixed-width',
            'gray',
            { styleOverrides, isHpNewLayoutEnabled }
          )}
        </ContainerView>
      )}

      {ctaTypeValue === ctaTypes.CTAButtonCarousel && (
        <ButtonContainer style={styleOverrides['nav-bg']}>
          {renderButtonList(
            ctaTypeValue,
            navigation,
            ctaItems,
            'scroll_cta_list',
            undefined,
            'gray',
            { styleOverrides }
          )}
        </ButtonContainer>
      )}

      {ctaTypeValue === ctaTypes.linkList && (
        <ButtonLinksContainer style={styleOverrides['nav-bg']}>
          {renderButtonList(
            ctaTypeValue,
            navigation,
            ctaItems,
            'link_cta_list',
            undefined,
            'gray',
            { styleOverrides }
          )}
        </ButtonLinksContainer>
      )}
    </Container>
  );
};

ribbonView.propTypes = ribbonViewPropType;
ribbonView.defaultProps = ribbonViewdefaultProps;
ModuleA.propTypes = ModulePropType;
ModuleA.defaultProps = ModuleAdefaultProps;
renderDamImage.propTypes = RenderDamImagepropTypes;

export default ModuleA;
export { ModuleA as ModuleAVanilla };
