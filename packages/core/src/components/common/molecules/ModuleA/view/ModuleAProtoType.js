import PropTypes from 'prop-types';

export const ModulePropType = {
  navigation: PropTypes.shape({}).isRequired,
  largeCompImageCarousel: PropTypes.shape({}).isRequired,
  ctaItems: PropTypes.shape([]).isRequired,
  ctaType: PropTypes.string.isRequired,
  ignoreLazyLoadImage: PropTypes.string.isRequired,
  moduleClassName: PropTypes.string.isRequired,
  borderWidth: PropTypes.number,
  isInitialModule: PropTypes.bool.isRequired,
};

export const ModuleAdefaultProps = {
  borderWidth: 0,
};

export const ribbonViewPropType = {
  ribbonBanner: PropTypes.shape([]).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  position: PropTypes.string.isRequired,
  ribbonStyleOverride: PropTypes.shape({
    tintColor: PropTypes.string,
  }),
};
export const ribbonViewdefaultProps = {
  ribbonStyleOverride: null,
};

export const RenderDamImagepropTypes = {
  mediaImage: PropTypes.string.isRequired,
  videoData: PropTypes.string.isRequired,
  ignoreLazyLoadImage: PropTypes.string.isRequired,
  heightOverride: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  borderWidth: PropTypes.string.isRequired,
  navigation: PropTypes.string.isRequired,
  isInitialModule: PropTypes.bool.isRequired,
};
