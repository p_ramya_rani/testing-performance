/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import useImageLoadedState from '@tcp/web/src/hooks/useImageLoadedState';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { HERO_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import ButtonList from '@tcp/core/src/components/common/molecules/ButtonList';
import { getCtaBorderRadius, getMediaBorderRadius } from '@tcp/core/src/utils/utils.web';
import {
  getIconPath,
  getLocator,
  isGymboree,
  styleOverrideEngine,
  isClient,
  getViewportInfo,
} from '@tcp/core/src/utils';
import { Col, Row, DamImage } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import { Carousel, LinkText, style, StyledButtonListContainer } from '../ModuleA.style';

import config from '../config';

const { ctaTypes, CAROUSEL_OPTIONS, IMG_DATA_TCP, IMG_DATA_GYM } = config;
const bigCarrotIcon = 'carousel-big-carrot';
const bigCarrotIconGym = 'carousel-big-carrot-white';

/**
 * This component is used for the initial slide only,
 * so that the load timing of the image within can be
 * measured with the Performance API.
 */
const FirstCarouselSlide = ({ isHeroSlot, ...others }) => {
  const imageRef = useRef();
  const imageLoaded = useImageLoadedState(imageRef);
  return (
    <>
      <DamImage lazyLoad forwardedRef={imageRef} {...others} isModule />
      {isHeroSlot && imageLoaded && <RenderPerf.Measure name={HERO_VISIBLE} />}
    </>
  );
};

FirstCarouselSlide.defaultProps = {
  isHeroSlot: false,
};
FirstCarouselSlide.propTypes = {
  isHeroSlot: PropTypes.bool,
};

// Helper for determining when FirstCarouselSlide should be rendered
function getSlideComponent(slideIndex) {
  return slideIndex === 0 ? FirstCarouselSlide : DamImage;
}

class ModuleA extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      curSlideIndex: 0,
      isHeroVisible: false,
    };
  }

  componentDidMount() {
    const { isHeroSlot } = this.props;
    if (isHeroSlot) {
      this.setState({ isHeroVisible: true });
    }
  }

  getCtaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom'])
      : {};
  };

  getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  /* Return className for the ribbon placement */
  getRibbonClass = () => {
    const { largeCompImageCarousel } = this.props;
    const { curSlideIndex } = this.state;
    const curCarouselSlide = largeCompImageCarousel[curSlideIndex];
    let ribbonAlignedClass = '';

    if (curCarouselSlide && curCarouselSlide.ribbonBanner) {
      const [ribbonBanner = {}] = curCarouselSlide.ribbonBanner;
      if (ribbonBanner.ribbonPlacement === 'left') {
        ribbonAlignedClass = 'left-aligned-ribbon';
      }
    }

    return ribbonAlignedClass;
  };

  getSlideContent = ({
    headerText,
    promoBanner,
    ribbonBanner,
    headerStyle,
    promoStyle,
    isLinkList,
    i,
    largeCompImageCarousel,
    hasBackgroundImage,
  }) => {
    const { isHeroSlot } = this.props;
    const { isHeroVisible } = this.state;
    return (
      <>
        <div className={`banner-content ${hasBackgroundImage ? `bg_banner-content` : ``}`}>
          {headerText && (
            <>
              <LinkText
                type="heading"
                component="h2"
                fontSize={isLinkList ? ['fs32', 'fs32', 'fs64'] : 'fs16'}
                headerText={headerText}
                color={isGymboree() ? 'white' : ''}
                className={
                  largeCompImageCarousel.length > 1 || isGymboree()
                    ? 'link-text-wrapper'
                    : 'link-text-wrapper single-image-header'
                }
                dataLocator={`${getLocator('moduleA_header_text')}${i}`}
                headerStyle={headerStyle}
              />
              {isHeroSlot && isHeroVisible && <RenderPerf.Measure name={HERO_VISIBLE} />}
            </>
          )}
          {promoBanner && (
            <PromoBanner
              promoBanner={promoBanner}
              className="moduleA__promoBanner"
              data-locator={`${getLocator('moduleA_promobanner_text')}${i}`}
              fontSize="fs48"
              promoStyle={promoStyle}
            />
          )}
        </div>
        {ribbonBanner && (
          <div className="ribbon-container">
            <PromoBanner
              promoBanner={ribbonBanner}
              className="moduleA__ribbonBanner"
              data-locator={`${getLocator('moduleA_ribbon_promobanner_text')}${i}`}
              fontSize="fs48"
            />
          </div>
        )}
      </>
    );
  };

  renderDifferentImageOnMobile = (showDifferentImageOnMobile, showVideoOnMobile) => {
    const { isMobile } = getViewportInfo() || {};
    return isClient() && isMobile && showDifferentImageOnMobile && !showVideoOnMobile;
  };

  renderSlideComponent = ({
    SlideComponent,
    showDifferentImageOnMobile,
    showVideoOnMobile,
    imgMData,
    link,
    imageConfig,
    imgData,
    videoData,
    videoMData,
    others,
    i,
  }) => {
    return this.renderDifferentImageOnMobile(showDifferentImageOnMobile, showVideoOnMobile) ? (
      <SlideComponent
        imgData={imgMData}
        link={link}
        imgConfigs={imageConfig}
        data-locator={`${getLocator('moduleA_image')}${i}`}
        lazyLoad
        isOptimizedVideo
        isModule
        {...others}
      />
    ) : (
      <SlideComponent
        imgData={imgData}
        link={link}
        imgConfigs={imageConfig}
        data-locator={`${getLocator('moduleA_image')}${i}`}
        videoData={videoData}
        showVideoOnMobile={showVideoOnMobile}
        videoDataMobile={videoMData}
        lazyLoad
        isOptimizedVideo
        isModule
        {...others}
      />
    );
  };

  getImage = (
    largeCompImageCarousel,
    headerStyle,
    { promoStyle, mediaBorderRadiusOverride, hasBackgroundImage },
    isLinkList,
    i,
    item
  ) => {
    const { ...others } = this.props;
    let SlideComponent = DamImage;
    const {
      headerText,
      linkedImage: linkImageArr,
      promoBanner,
      ribbonBanner,
    } = item || largeCompImageCarousel[0];
    const linkedImage = linkImageArr && linkImageArr[0];
    const imageConfig = isGymboree() ? IMG_DATA_GYM.crops : IMG_DATA_TCP.crops;

    if (largeCompImageCarousel.length > 1) {
      SlideComponent = getSlideComponent(i);
    }
    const showDifferentImageOnMobile = linkedImage && linkedImage.imageMobile;
    const showVideoOnMobile = linkedImage && !!linkedImage.videoMobile;

    const { imageMobile, link, image, video, videoMobile } = linkedImage || {};

    const imgData =
      !isEmpty(image) && !isEmpty(mediaBorderRadiusOverride)
        ? Object.assign(image, mediaBorderRadiusOverride)
        : image;
    const videoData =
      !isEmpty(video) && !isEmpty(mediaBorderRadiusOverride)
        ? Object.assign(video, mediaBorderRadiusOverride)
        : video;
    const imgMData =
      !isEmpty(imageMobile) && !isEmpty(mediaBorderRadiusOverride)
        ? Object.assign(imageMobile, mediaBorderRadiusOverride)
        : imageMobile;
    const videoMData =
      !isEmpty(videoMobile) && !isEmpty(mediaBorderRadiusOverride)
        ? Object.assign(videoMobile, mediaBorderRadiusOverride)
        : videoMobile;

    return (
      <div key={i.toString()} className="banner-slide">
        {this.renderSlideComponent({
          SlideComponent,
          showDifferentImageOnMobile,
          showVideoOnMobile,
          imgMData,
          link,
          imageConfig,
          imgData,
          videoData,
          videoMData,
          others,
          i,
        })}
        {this.getSlideContent({
          headerText,
          promoBanner,
          ribbonBanner,
          headerStyle,
          promoStyle,
          isLinkList,
          i,
          largeCompImageCarousel,
          hasBackgroundImage,
        })}
      </div>
    );
  };

  getCarousel = (
    carouselConfig,
    largeCompImageCarousel,
    headerStyle,
    { promoStyle, mediaBorderRadiusOverride },
    isLinkList,
    sliderFadeOptions
  ) => {
    const overrideCarouselOptions = !isEmpty(sliderFadeOptions)
      ? {
          fade: true,
          cssEase: 'linear',
          infinite: true,
          autoplaySpeed: parseFloat(sliderFadeOptions.speed),
          speed: 500,
        }
      : {};

    return (
      <Carousel
        options={{ ...CAROUSEL_OPTIONS, ...overrideCarouselOptions }}
        carouselConfig={carouselConfig}
        isModule
      >
        {largeCompImageCarousel.map((item, i) => {
          // Use a special component for the first slide (for performance measurement)
          return this.getImage(
            largeCompImageCarousel,
            headerStyle,
            { promoStyle, mediaBorderRadiusOverride },
            isLinkList,
            i,
            item
          );
        })}
      </Carousel>
    );
  };

  RenderBackgroundImage = ({ backgroundImage, styleOverrides, isHpNewModuleDesignEnabled }) => {
    const { image, video, imageMobile, videoMobile, link } = backgroundImage[0] || {};
    const { ...others } = this.props;
    const imageConfig = isGymboree() ? IMG_DATA_GYM.crops : IMG_DATA_TCP.crops;
    const showDifferentImageOnMobile = imageMobile;
    const showVideoOnMobile = !!videoMobile;
    const SlideComponent = DamImage;
    const i = 'BgImg';
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );
    const imgData =
      !isEmpty(image) && !isEmpty(mediaBorderRadiusOverride)
        ? Object.assign(image, mediaBorderRadiusOverride)
        : image;
    const videoData =
      !isEmpty(video) && !isEmpty(mediaBorderRadiusOverride)
        ? Object.assign(video, mediaBorderRadiusOverride)
        : video;
    const imgMData =
      !isEmpty(imageMobile) && !isEmpty(mediaBorderRadiusOverride)
        ? Object.assign(imageMobile, mediaBorderRadiusOverride)
        : imageMobile;
    const videoMData =
      !isEmpty(videoMobile) && !isEmpty(mediaBorderRadiusOverride)
        ? Object.assign(videoMobile, mediaBorderRadiusOverride)
        : videoMobile;

    return (
      <div className="moduleA__bg-img-wrapper">
        {this.renderSlideComponent({
          SlideComponent,
          showDifferentImageOnMobile,
          showVideoOnMobile,
          imgMData,
          link,
          imageConfig,
          imgData,
          videoData,
          videoMData,
          others,
          i,
        })}
      </div>
    );
  };

  RenderModuleA = ({
    largeCompImageCarousel,
    carouselConfig,
    headerStyle,
    promoStyle,
    isLinkList,
    buttonListCtaType,
    ctaBgImage,
    ctaItemFilteredObj,
    styleOverrides,
    sliderFadeOptions,
    btnOverrideStyle,
    isHpNewDesignCTAEnabled,
    isHpNewModuleDesignEnabled,
    hasBackgroundImage,
  }) => {
    const { isMobile } = isClient() && getViewportInfo();
    const ctaBorderRadiusOverride = !isMobile
      ? this.getCtaBorderRadiusOverride(isHpNewModuleDesignEnabled, styleOverrides)
      : {};
    const navBgStyleOverrides = styleOverrides['nav-bg'] || {};
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );

    return (
      <Col
        colSize={{
          small: 6,
          medium: 8,
          large: 12,
        }}
        className={`moduleA_carousel_wrapper ${hasBackgroundImage ? `bg_position` : ``}`}
      >
        <div>
          <div>
            {largeCompImageCarousel.length > 1
              ? this.getCarousel(
                  carouselConfig,
                  largeCompImageCarousel,
                  headerStyle,
                  { promoStyle, mediaBorderRadiusOverride },
                  isLinkList,
                  sliderFadeOptions
                )
              : this.getImage(
                  largeCompImageCarousel,
                  headerStyle,
                  { promoStyle, mediaBorderRadiusOverride, hasBackgroundImage },
                  isLinkList,
                  1
                )}
          </div>
          <StyledButtonListContainer
            className={`button-list-container ${buttonListCtaType}`}
            ctaBgImageObj={ctaBgImage}
            style={Object.assign(navBgStyleOverrides, ctaBorderRadiusOverride)}
            hasBackgroundImage={hasBackgroundImage}
          >
            {ctaItemFilteredObj && (
              <ButtonList
                buttonsData={ctaItemFilteredObj}
                buttonListVariation={buttonListCtaType}
                dataLocatorDivisionImages={getLocator('moduleA_cta_image')}
                dataLocatorTextCta={getLocator('moduleA_cta_links')}
                style={btnOverrideStyle}
                isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
              />
            )}
          </StyledButtonListContainer>
        </div>
      </Col>
    );
  };

  render() {
    const {
      largeCompImageCarousel,
      ctaItems,
      ctaType,
      className,
      accessibility: { playIconButton, pauseIconButton, previousButton, nextIconButton } = {},
      fullBleed,
      moduleClassName,
      page,
      isHpNewDesignCTAEnabled,
      backgroundImage,
      isHpNewModuleDesignEnabled,
    } = this.props;

    const buttonListCtaType = ctaTypes[ctaType];
    const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleA');
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const promoStyle = styleOverrides.promo;
    const btnOverrideStyle = styleOverrides && {
      ...styleOverrides.button,
      'border-bottom-color': styleOverrides.button && styleOverrides.button.color,
    };
    const sliderFadeOptions = styleOverrides['slider-fade'];
    const isLinkList = buttonListCtaType === 'linkCTAList';
    const carouselIcon = isGymboree() ? bigCarrotIconGym : bigCarrotIcon;
    const ctaItemFilteredObj = ctaItems && ctaItems.filter((item) => item.button);
    const ctaBgImageFilteredObj = ctaItems && ctaItems.filter((item) => item.ctaBackgroundImage);
    const ctaBgImage = ctaBgImageFilteredObj && ctaBgImageFilteredObj.length !== 0 && ctaBgImageFilteredObj[0];
    const hasBackgroundImage = backgroundImage && backgroundImage.length > 0;

    const carouselConfig = {
      autoplay: true,
      dataLocatorPlay: getLocator('moduleA_play_button'),
      dataLocatorPause: getLocator('moduleA_pause_button'),
      customArrowLeft: getIconPath(carouselIcon),
      customArrowRight: getIconPath(carouselIcon),
      dataLocatorCarousel: 'carousel_banner',
    };

    CAROUSEL_OPTIONS.prevArrow = (
      <button
        type="button"
        aria-label={previousButton}
        data-locator="moduleA_left_arrow"
        className="control-prev"
      />
    );
    CAROUSEL_OPTIONS.nextArrow = (
      <button
        type="button"
        aria-label={nextIconButton}
        data-locator="moduleA_right_arrow"
        className="control-next"
      />
    );
    CAROUSEL_OPTIONS.afterChange = (slideIndex) => {
      this.setState({ curSlideIndex: slideIndex });
    };
    carouselConfig.autoplay = carouselConfig.autoplay && largeCompImageCarousel.length > 1;
    carouselConfig.pauseIconButtonLabel = pauseIconButton;
    carouselConfig.playIconButtonLabel = playIconButton;
    return (
      <Row
        className={`${className} ${moduleClassName} page-${page} ${
          isGymboree() ? 'gymboree-module-a' : 'tcp-module-a'
        } ${this.getRibbonClass()} moduleA`}
        fullBleed={fullBleed || { small: true, medium: true, large: false }}
      >
        {hasBackgroundImage &&
          this.RenderBackgroundImage({
            backgroundImage,
            styleOverrides,
            isHpNewModuleDesignEnabled,
          })}
        {this.RenderModuleA({
          largeCompImageCarousel,
          carouselConfig,
          headerStyle,
          promoStyle,
          isLinkList,
          buttonListCtaType,
          ctaBgImage,
          ctaItemFilteredObj,
          styleOverrides,
          sliderFadeOptions,
          btnOverrideStyle,
          isHpNewDesignCTAEnabled,
          isHpNewModuleDesignEnabled,
          hasBackgroundImage,
        })}
      </Row>
    );
  }
}

ModuleA.defaultProps = {
  accessibility: {},
  fullBleed: false,
  moduleClassName: '',
  page: '',
  isHeroSlot: false,
  backgroundImage: {},
};

ModuleA.propTypes = {
  isHeroSlot: PropTypes.bool,
  accessibility: PropTypes.shape({
    playIconButton: PropTypes.string,
    pauseIconButton: PropTypes.string,
    previousButton: PropTypes.string,
    nextIconButton: PropTypes.string,
  }),
  className: PropTypes.string.isRequired,
  largeCompImageCarousel: PropTypes.arrayOf(
    PropTypes.shape({
      headerText: PropTypes.arrayOf(
        PropTypes.shape({
          link: PropTypes.object,
          textItems: PropTypes.array,
        })
      ),
      linkedImage: PropTypes.arrayOf(PropTypes.shape({})),
      promoBanner: PropTypes.arrayOf(PropTypes.shape({})),
      ribbonBanner: PropTypes.arrayOf(PropTypes.shape({})),
    })
  ).isRequired,
  ctaItems: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  ctaType: PropTypes.oneOf(['stackedCTAButtons', 'linkCTAList', 'scrollCTAList', 'imageCTAList'])
    .isRequired,
  fullBleed: PropTypes.bool,
  moduleClassName: PropTypes.string,
  page: PropTypes.string,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  backgroundImage: PropTypes.shape({}),
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

export default errorBoundary(withStyles(ModuleA, style));
export { ModuleA as ModuleAVanilla };
