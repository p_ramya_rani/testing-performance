// 9fbef606107a605d69c0edbcd8029e5d
import { getMediaBorderRadius, getCtaBorderRadius } from '@tcp/core/src/utils/utils.app';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';

export const getCtaBorderRadiusOverride = styleOverrides => {
  return getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom']);
};

export const getMediaBorderRadiusOverride = styleOverrides => {
  return getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom']);
};

export const computeModuleVideoWidth = (isHpNewLayoutEnabled, MODULE_WIDTH) => {
  return isHpNewLayoutEnabled ? MODULE_WIDTH - 2 * HP_NEW_LAYOUT.BODY_PADDING : MODULE_WIDTH;
};

export const computeModuleWidth = (isHpNewLayoutEnabled, borderWidth, MODULE_WIDTH) => {
  return isHpNewLayoutEnabled
    ? MODULE_WIDTH - (borderWidth || 0) - 2 * HP_NEW_LAYOUT.BODY_PADDING
    : MODULE_WIDTH - (borderWidth || 0);
};
