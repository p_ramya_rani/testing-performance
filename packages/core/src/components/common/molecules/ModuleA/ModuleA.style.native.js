// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const Container = styled.View``;

export const HeaderWrapper = styled.View`
  position: absolute;
  z-index: ${props => props.theme.zindex.zOverlay};
  align-items: center;
  width: 100%;
  margin-top: ${props => props.theme.spacing.LAYOUT_SPACING.XS};
  padding: 0 ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const PromoTextBannerWrapper = styled.View``;

export const PromoBannerWrapper = styled.View`
  padding-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
  padding-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const PromoBannerWrapperHalfView = styled.View`
  width: 100%;
`;

export const ButtonContainer = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: 0;
`;
export const ContainerView = styled.View`
  width: 100%;
`;

// adding widht for iOS only Android text wrapping is not center aligned
export const WrapTextContainerView = styled.View`
  justify-content: center;
  align-items: center;
  ${props => (props.isIOS ? props.textWidth : {})}
  margin: 30px 0 30px 0;
`;

export const Border = styled.View`
  height: 0.5px;
  background: ${props =>
    props.background === 'text'
      ? props.theme.colors.BUTTON.WHITE.BORDER
      : props.theme.colorPalette.gray[700]};
`;

export const DivImageCTAContainer = styled.View``;

export const ButtonLinksContainer = styled.View`
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  background: #00b7c8;
`;

export const PromoRibbonWrapper = styled.View`
  width: 174px;
  height: 54px;
  bottom: 10px;
  position: absolute;
  z-index: ${props => props.theme.zindex.zOverlay}
    ${props => (props.viewdirection === 'right' ? ` left:0; ` : '')};
  ${props => (props.viewdirection === 'left' ? `right:0; ` : '')};
`;

export const MessageContainer = styled.View`
  padding: ${props => props.theme.spacing.ELEM_SPACING.SM};
  position: absolute;
  right: 0;
  z-index: ${props => props.theme.zindex.zOverlay};
`;

export const HeaderView = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
`;

/**
 * RibbonBanner height and width.
 * Height is fixed for mobile : TCP & Gymb
 * Width can vary as per deign.
 */

export const RibbonBannerHeight = '200px';
export const RibbonBannerWidth = '54px';

export default {
  Container,
  HeaderWrapper,
  PromoTextBannerWrapper,
  ButtonContainer,
  Border,
  ContainerView,
  DivImageCTAContainer,
  ButtonLinksContainer,
  MessageContainer,
  RibbonBannerHeight,
  RibbonBannerWidth,
  PromoBannerWrapper,
  HeaderView,
  PromoRibbonWrapper,
  WrapTextContainerView,
};
