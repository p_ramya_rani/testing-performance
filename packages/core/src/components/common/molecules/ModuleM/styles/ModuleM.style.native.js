// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { DamImage } from '../../../atoms';
/*
    This method is responsible to return the last image index
    of row on the basis of images length
  */
const getIndexes = totalImages => {
  const indexes = [];
  if (totalImages === 2 || totalImages === 3) {
    indexes.push(1);
  } else if (totalImages === 4 || totalImages === 5) {
    indexes.push(2);
  } else {
    indexes.push(2, 5);
  }
  return indexes;
};

const getButtonPadding = props => {
  if (props.theme.isGymboree) {
    const padding =
      props.imageCount === 3
        ? props.theme.spacing.ELEM_SPACING.XXL
        : props.theme.spacing.ELEM_SPACING.SM;
    return `padding: 0 ${padding}`;
  }
  return '';
};

const getMarginRight = props => {
  let marginRight = 0;
  if (props.imageCount === 4) {
    marginRight = '10px';
  } else {
    marginRight = getIndexes(props.imageCount).includes(props.tileIndex) ? `0` : `10px`;
  }
  return marginRight;
};

export const StyledDamImage = styled(DamImage)`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  border-radius: ${props => (props.theme.isGymboree ? `50px` : `0`)};
`;

export const Container = styled.View`
  width: 100%;
`;

export const HeaderContainer = styled.View`
  align-items: center;
  margin-bottom: 5px;
  padding: 0 14px;
  color: #f7971f;
`;

export const SecondHeaderContainer = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-bottom: 12px;
  ${props => (props.theme.isGymboree ? '' : 'padding: 0 10px 12px')}
`;

export const PromoContainer = styled.View`
  align-items: center;
  background: ${props => props.theme.colorPalette.blue[500]};
  padding: ${props => props.theme.spacing.ELEM_SPACING.XS} 0;
`;

export const ButtonTabsContainer = styled.View`
  display: flex;
  align-items: center;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const ButtonContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  ${getButtonPadding};
  background-color: ${props => props.theme.colorPalette.blue[500]};
  height: ${props => props.imageDimension}px;
  width: ${props => props.imageDimension}px;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const ImageContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: ${props => (props.theme.isGymboree ? 'space-around' : 'center')};
  padding: 0 14px;
`;

export const Tile = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
  margin-right: ${props => getMarginRight(props)};
  max-width: ${props => props.maxWidth};
`;

export default {
  Container,
  HeaderContainer,
  SecondHeaderContainer,
  PromoContainer,
  ButtonTabsContainer,
  ButtonContainer,
  ImageContainer,
  Tile,
  StyledDamImage,
};
