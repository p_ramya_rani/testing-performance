// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components';
import Col from '../../../atoms/Col';

const getImageRoundFlexStyle = props => {
  const {
    theme: {
      spacing: { ELEM_SPACING },
      mediaQuery: { medium, large, xlarge },
    },
    isFlexBox,
  } = props;

  return `
  display: flex;
  justify-content: center;
  margin-bottom: ${ELEM_SPACING.LRG};

  img {
    height: 103px;
    width: 103px;
    border-radius: 100%;

    @media ${medium} {
      height: ${isFlexBox ? 137 : 170}px;
      width: ${isFlexBox ? 137 : 170}px;;
    }

    @media ${large} {
      height: ${isFlexBox ? 143 : 180}px;;
      width: ${isFlexBox ? 143 : 180}px;;
    }

    @media ${xlarge} {
      height: ${isFlexBox ? 139 : 210}px;;;
      width: ${isFlexBox ? 139 : 210}px;;;
    }
   }
  `;
};

export const ImageGrid = styled(Col)`
  border-radius: ${props => (props.round ? '100%' : 0)};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
  @media ${props => props.theme.mediaQuery.smallOnly} {
    margin-right: 2.5%;
  }
  @media ${props => props.theme.mediaQuery.large} {
    margin-bottom: 56px;
  }

  img {
    height: 100%;
    width: 100%;
  }
`;

export const ImageRoundFlex = styled.div`
  ${getImageRoundFlexStyle}
`;

export const CtaButtonWrapper = styled(Col)`
  background: ${props => props.theme.colorPalette.blue[500]};
  font-family: ${props => props.theme.typography.fonts.primary};
  font-size: ${props => props.theme.typography.fontSizes.fs36};
  font-weight: ${props => props.theme.fonts.fontWeight.black};
  letter-spacing: ${props => props.theme.typography.letterSpacings.ls222};
  color: ${props => props.theme.colors.WHITE};
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
  @media ${props => props.theme.mediaQuery.large} {
    margin-bottom: 56px;
  }

  @media ${props => props.theme.mediaQuery.mediumOnly} {
    font-size: ${props => props.theme.typography.fontSizes.fs26};
  }

  @media ${props => props.theme.mediaQuery.smallOnly} {
    font-size: ${props => props.theme.typography.fontSizes.fs20};
  }

  .moduleM__shopAllBtn {
    font-family: ${props => props.theme.typography.fonts.primary};
    padding: ${props => props.theme.spacing.ELEM_SPACING.XXXL} 61px;
    text-align: center;
    display: flex;
    justify-content: center;
  }
`;

export default css`
  ${props =>
    props.isBorderWidth
      ? `
      .topview {
        margin-left:0;
        margin-right:0;
        width:100%;
      }`
      : ``}
  .image-items-container {
    justify-content: center;
    margin: ${props => props.theme.spacing.ELEM_SPACING.LRG} auto 0;
    @media ${props => props.theme.mediaQuery.large} {
      width: 100%;
    }
  }
  .moduleM__productName {
    display: flex;
    justify-content: center;
  }

  .promoBanner {
    background: ${props => props.theme.colorPalette.blue[500]};
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    padding: ${props => props.theme.spacing.ELEM_SPACING.XS} 0 0;

    @media ${props => props.theme.mediaQuery.large} {
      padding: ${props => props.theme.spacing.ELEM_SPACING.XXS} 0;
    }

    .promo-text {
      display: block;

      @media ${props => props.theme.mediaQuery.large} {
        display: inline;
      }
    }
  }

  .image-items-container-category {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin: 0 auto;
    max-width: 400px;

    > .imagecategory__list {
      margin-right: 8.5px;
      margin-left: 8.5px;
    }

    @media ${props => props.theme.mediaQuery.medium} {
      max-width: 700px;
    }

    @media ${props => props.theme.mediaQuery.large} {
      margin: 0 auto 25px;
      max-width: 1000px;
      > .imagecategory__list {
        margin-right: 9px;
        margin-left: 9px;
      }
    }

    @media ${props => props.theme.mediaQuery.xlarge} {
      max-width: 1400px;
      > .imagecategory__list {
        margin-right: 15px;
        margin-left: 15px;
      }
    }
  }

  .image-items-container-category__flex.image-items-container-category__flex {
    > .imagecategory__list {
      margin-right: 7px;
      margin-left: 7px;
    }

    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: 8px;
      max-width: 900px;
    }

    @media ${props => props.theme.mediaQuery.large} {
      margin-top: 0;
      max-width: 900px;
      > .imagecategory__list {
        margin-right: 7px;
        margin-left: 7px;
      }
    }

    @media ${props => props.theme.mediaQuery.xlarge} {
      max-width: 100%;
      > .imagecategory__list {
        margin-right: 9.5px;
        margin-left: 9.5px;
      }
    }
  }

  .image-items-container__flex {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .image-items-container__flex--item {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
    margin-right: 14px;
    img {
      width: 142px;
      height: 142px;

      @media ${props => props.theme.mediaQuery.mediumOnly} {
        height: 137px;
        width: 137px;
      }

      @media ${props => props.theme.mediaQuery.smallOnly} {
        height: 115px;
        width: 103px;
      }
    }
  }
  .image-items-container__flex--item.widthAuto {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      height: auto;
      width: 44%;
      margin-left: 10px;
      margin-right: 0;
    }
    img {
      @media ${props => props.theme.mediaQuery.smallOnly} {
        width: 100%;
        height: auto;
      }
    }
  }
  .moduleM__shopAllBtnWrapper {
    background: ${props => props.theme.colorPalette.blue[500]};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs36};
    font-weight: ${props => props.theme.fonts.fontWeight.black};
    letter-spacing: ${props => props.theme.typography.letterSpacings.ls222};
    color: ${props => props.theme.colors.WHITE};
    display: flex;
    justify-content: center;
    align-items: center;
    height: 142px;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
    width: 142px;

    .moduleM__shopAllBtn {
      font-family: ${props => props.theme.typography.fonts.primary};
      padding: ${props => props.theme.spacing.ELEM_SPACING.XXXL} 61px;
      text-align: center;
    }

    @media ${props => props.theme.mediaQuery.mediumOnly} {
      font-size: ${props => props.theme.typography.fontSizes.fs26};
      height: 137px;
      width: 137px;
    }

    @media ${props => props.theme.mediaQuery.smallOnly} {
      font-size: ${props => props.theme.typography.fontSizes.fs20};
      height: 115px;
      width: 103px;
    }
    &.widthAuto {
      @media ${props => props.theme.mediaQuery.smallOnly} {
        height: auto;
        width: 45%;
        margin-left: 12px;
        margin-bottom: 58px;
      }
    }
  }

  .product-tab-list {
    display: flex;
    justify-content: center;
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      margin-top: 0;
    }
    > :not(:last-child) {
      border-right: 1px solid #c3c3c3;
    }

    .product-tab-list__item {
      padding: 0px ${props => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
  .promo-header .link-text {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }
  .moduleM__productImage {
    height: 100%;
    width: 100%;
  }

  .header-container {
    margin-right: 0;
  }

  .moduleM__productContainer {
    height: 100%;
  }

  h2 {
    font-size: 0px;
  }

  &.brand-tcp.page-home .promoBanner .small_white_text_semibold,
  &.brand-tcp.page-home .promoBanner .extra_large_white_text_black {
    font-family: TofinoWide;
  }

  &.brand-tcp.page-home .promoBanner .extra_large_white_text_black {
    @media ${props => props.theme.mediaQuery.smallMax} {
      font-size: 62px;
      margin-top: -8px;
    }
  }

  &.brand-tcp.page-home .moduleM__productName {
    font-family: Nunito;
    margin-top: 4px;
  }

  &.brand-tcp.page-home .moduleM__shopAllBtn {
    font-family: TofinoWide;
    font-weight: 700;
    line-height: 1.1;
  }
  &.brand-tcp.page-home {
    .link-text .large_text_black {
      font-family: TofinoCond;
      font-weight: 700;
      font-size: 48px;

      @media ${props => props.theme.mediaQuery.mediumOnly} {
        font-size: 62px;
      }
      @media ${props => props.theme.mediaQuery.large} {
        font-size: 88px;
      }
    }
    .link-text .medium_text_subpromo {
      font-family: Nunito;
    }
  }
`;
