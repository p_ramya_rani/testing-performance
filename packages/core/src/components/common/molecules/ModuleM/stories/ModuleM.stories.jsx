// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { storiesOf } from '@storybook/react';
import mockM from '@tcp/core/src/services/abstractors/common/moduleM/mock';
import ModuleM from '../views/ModuleM';

storiesOf('ModuleM', module).add('Basic', () => (
  <ModuleM {...mockM.moduleM.composites} type="flex1" />
));
