// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mock from '@tcp/core/src/services/abstractors/common/moduleM/mock';
import ModuleM from '../views/ModuleM.native';

describe('ModuleM component', () => {
  it('ModuleM component renders correctly with props', () => {
    const component = shallow(<ModuleM {...mock.moduleM.composites} />);
    expect(component).toMatchSnapshot();
  });

  it('ModuleM component renders correctly when isHpNewLayoutEnabled is true', () => {
    const component = shallow(<ModuleM {...mock.moduleM.composites} isHpNewLayoutEnabled />);
    expect(component).toMatchSnapshot();
  });
});
