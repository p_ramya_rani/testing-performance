const singleCTAButtonConfigured = (singleCTAButton, selectedProductList) => {
  let noOfGridItems =
    singleCTAButton && singleCTAButton.text && selectedProductList.length !== 4
      ? selectedProductList.length - 1
      : selectedProductList.length;
  let noCTA = true;
  if (singleCTAButton && singleCTAButton.text) {
    noOfGridItems += 1;
    noCTA = false;
  }
  return {
    noOfGridItems,
    noCTA,
  };
};

export default singleCTAButtonConfigured;
