// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { getLocator, isGymboree } from '../../../../../utils';
import {
  getScreenWidth,
  styleOverrideEngine,
  getHeaderBorderRadius,
  getMediaBorderRadius,
} from '../../../../../utils/index.native';
import { Anchor, BodyCopy } from '../../../atoms';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';
import colors from '../../../../../../styles/themes/TCP/colors';
import {
  Container,
  HeaderContainer,
  SecondHeaderContainer,
  PromoContainer,
  ButtonTabsContainer,
  ButtonContainer,
  ImageContainer,
  Tile,
  StyledDamImage,
} from '../styles/ModuleM.style.native';
import LinkText from '../../LinkText';
import PromoBanner from '../../PromoBanner';
import ButtonTabs from '../../ButtonTabs';
import config from '../moduleM.config';

const MODULE_WIDTH = getScreenWidth();

/**
 * @class ModuleM - global reusable component will display display a featured
 * category module with category links and featured product images
 * This component is plug and play at any given slot in layout by passing required data
 * @param {headerText} headerText the list of data for header
 * @param {promoBanner} promoBanner promo banner data
 * @param {divTabs} divTabs division tabs data
 */
class ModuleM extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentTabItem: 'tablList-0',
    };
  }

  componentDidMount() {
    const { divTabs } = this.props;
    const tabItems = this.getButtonTabItems(divTabs);
    const { id } = tabItems.length && tabItems[0];
    this.setState({ currentTabItem: id });
  }

  /*
    Calculate image dimensions dynamically
    as per screen width
  */
  getImageDimension = (totalImages, isHpNewLayoutEnabled) => {
    const { OFFSET, GUTTER_SPACE } = config;
    const moduleWidth = isHpNewLayoutEnabled
      ? MODULE_WIDTH - OFFSET - 2 * HP_NEW_LAYOUT.BODY_PADDING
      : MODULE_WIDTH - OFFSET;
    const divider = 3;
    if (isGymboree()) {
      const gutter = totalImages <= divider ? GUTTER_SPACE : GUTTER_SPACE * 2;
      return parseInt((moduleWidth - gutter) / divider, 10);
    }
    const isHalfWidthLayout = totalImages <= divider;
    const gutter = GUTTER_SPACE * 2 + (isHalfWidthLayout ? 10 : 20);
    const tempimageSize = moduleWidth - gutter;
    return parseInt(isHalfWidthLayout ? tempimageSize / 2 : tempimageSize / divider, 10);
  };

  onTabChange = id => {
    this.setState({ currentTabItem: id });
  };

  /*
    Create a required data object for the ButtonTabs components
  */
  getButtonTabItems = tabItems => {
    return tabItems.map((item, index) => {
      const {
        text: { text },
      } = item;
      return { label: text, id: `tablList-${index}` };
    });
  };

  /*
    Create a required data object for the Image list
  */
  getImageItems = data => {
    return data.map((item, index) => {
      const { smallCompImage, linkClass } = item;
      return { id: `tablList-${index}`, smallCompImage, linkClass };
    });
  };
  /**
   * To Render the Dam Image or Video Component
   */

  renderDamImage = (link, imgData, videoData, navigation, index, imageDimension) => {
    const { IMG_DATA } = config;
    const damImageComp = (
      <StyledDamImage
        alt={imgData && imgData.alt}
        url={imgData && imgData.url}
        testID={`${getLocator('moduleM_image')}${index}`}
        height={imageDimension}
        width={imageDimension}
        videoData={videoData}
        imgConfig={IMG_DATA.productImgConfig[0]}
        isFastImage
        resizeMode="stretch"
        isHomePage
      />
    );
    if (imgData && Object.keys(imgData).length > 0) {
      return (
        <Anchor url={link.url} navigation={navigation}>
          {damImageComp}
        </Anchor>
      );
    }
    return videoData && Object.keys(videoData).length > 0 ? (
      <React.Fragment>{damImageComp}</React.Fragment>
    ) : null;
  };

  getConfig = () => {
    return {
      headerfontSize: isGymboree() ? 'fs20' : 'fs48',
      headerFontWeight: isGymboree() ? 'regular' : 'bold',
      headerLineHeight: isGymboree() ? { lineHeight: 20 } : { lineHeight: 48 },
    };
  };

  getBackgroundStyle = styleOverrides => {
    return (styleOverrides && styleOverrides.bground) || {};
  };

  getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? styleOverrides &&
          getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  // eslint-disable-next-line
  render() {
    const {
      headerText,
      promoBanner,
      divTabs,
      navigation,
      moduleClassName,
      borderWidth = 0,
      isHpNewLayoutEnabled,
    } = this.props;
    const { currentTabItem } = this.state;
    const tabItems = this.getButtonTabItems(divTabs);
    const images = this.getImageItems(divTabs);
    let currentTabData = '';
    if (currentTabItem) {
      currentTabData = images.find(obj => obj.id === currentTabItem);
    }
    const { smallCompImage, linkClass } = currentTabData;
    const totalImages = smallCompImage && smallCompImage.length;
    const imageDimension = this.getImageDimension(totalImages, isHpNewLayoutEnabled);
    const { headerfontSize, headerFontWeight, headerLineHeight } = this.getConfig();
    const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleM');
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const backgroundStyle = this.getBackgroundStyle(styleOverrides);
    const dimensionReducer = (totalImages === 4 ? 5 : 0) + parseInt(borderWidth / 2, 10);
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const containerStyle = isHpNewLayoutEnabled
      ? {
          ...HP_NEW_LAYOUT.MODULE_BOX_SHADOW,
        }
      : {};
    const headerWrapperStyle = isHpNewLayoutEnabled
      ? {
          ...headerBorderRadiusOverride,
          backgroundColor: colors?.WHITE,
        }
      : {};
    const mediaWrapperStyle = isHpNewLayoutEnabled
      ? {
          ...mediaBorderRadiusOverride,
          backgroundColor: colors?.WHITE,
        }
      : {};
    return (
      <Container style={containerStyle}>
        <View style={headerWrapperStyle}>
          <HeaderContainer>
            {headerText && headerText[0] && (
              <LinkText
                navigation={navigation}
                headerText={[headerText[0]]}
                testID={getLocator('moduleM_header_text_0')}
                fontFamily={isGymboree() ? 'primary' : 'tertiary'}
                fontSize={headerfontSize}
                textAlign="center"
                color="orange.800"
                fontWeight={headerFontWeight}
                type="heading"
                headerStyle={[headerStyle[0]]}
                style={headerLineHeight}
              />
            )}
          </HeaderContainer>
          {headerText && headerText[1] && (
            <SecondHeaderContainer>
              <LinkText
                navigation={navigation}
                headerText={[headerText[1]]}
                testID={getLocator('moduleM_header_text_1')}
                headerStyle={[headerStyle[1]]}
                color="text.primary"
                fontFamily="secondary"
                fontSize="fs14"
                lineHeight="16px"
                fontWeight="regular"
                textAlign="center"
              />
            </SecondHeaderContainer>
          )}
          {promoBanner && (
            <PromoContainer style={backgroundStyle}>
              <PromoBanner
                testID={getLocator('moduleM_promobanner_text')}
                promoBanner={promoBanner}
                navigation={navigation}
                promoStyle={styleOverrides.promo}
              />
            </PromoContainer>
          )}
          {tabItems.length > 1 ? (
            <ButtonTabsContainer>
              <ButtonTabs
                selectedTabId={currentTabItem}
                onTabChange={this.onTabChange}
                tabs={tabItems}
                navigation={navigation}
              />
            </ButtonTabsContainer>
          ) : null}
        </View>

        {smallCompImage && smallCompImage.length > 0 ? (
          <ImageContainer style={mediaWrapperStyle}>
            {smallCompImage.map(({ image, link, video }, index) => {
              const videoData = video && {
                ...video,
                videoWidth: imageDimension,
                videoHeight: imageDimension,
              };
              const imgData = image || {};
              return (
                <Tile
                  maxWidth={isGymboree() ? 116 : imageDimension - dimensionReducer}
                  tileIndex={index}
                  imageCount={totalImages}
                  key={index.toString()}
                  accessible
                  accessibilityLabel={link && link.text}
                  accessibilityRole="link"
                >
                  {this.renderDamImage(
                    link,
                    imgData,
                    videoData,
                    navigation,
                    index,
                    isGymboree() ? 116 : imageDimension - dimensionReducer
                  )}
                  {!!link.text && (
                    <Anchor
                      url={link.url}
                      navigation={navigation}
                      testID={`${getLocator('moduleM_textlink')}${index}`}
                    >
                      <BodyCopy
                        fontFamily={isGymboree() ? 'primary' : 'secondary'}
                        fontSize={isGymboree() ? 'fs15' : 'fs14'}
                        fontWeight="regular"
                        color="gray.900"
                        text={link.text}
                        textAlign="center"
                        // eslint-disable-next-line react-native/no-inline-styles
                        style={{
                          marginTop: isGymboree() ? 10 : 4,
                        }}
                      />
                    </Anchor>
                  )}
                </Tile>
              );
            })}
            {linkClass ? (
              <Anchor
                url={linkClass.url}
                navigation={navigation}
                testID={getLocator('moduleM_cta_btn')}
              >
                <ButtonContainer
                  style={backgroundStyle}
                  imageCount={totalImages}
                  imageDimension={imageDimension - dimensionReducer}
                >
                  <BodyCopy
                    mobileFontFamily={isGymboree() ? 'primary' : 'tertiary'}
                    fontSize="fs20"
                    fontWeight="extrabold"
                    color="white"
                    letterSpacing="ls1"
                    text={linkClass.text}
                    textAlign="center"
                  />
                </ButtonContainer>
              </Anchor>
            ) : null}
          </ImageContainer>
        ) : null}
      </Container>
    );
  }
}

ModuleM.defaultProps = {
  promoBanner: [],
  moduleClassName: '',
  borderWidth: 0,
};

ModuleM.propTypes = {
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  navigation: PropTypes.shape({}).isRequired,
  divTabs: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.object,
      category: PropTypes.object,
      smallCompImage: PropTypes.array,
    })
  ).isRequired,
  singleCTAButton: PropTypes.shape({}).isRequired,
  moduleClassName: PropTypes.string,
  borderWidth: PropTypes.number,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleM;
