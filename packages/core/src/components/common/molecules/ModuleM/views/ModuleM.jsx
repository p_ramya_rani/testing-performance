// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import Grid from '@tcp/core/src/components/common/molecules/Grid';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import {
  Anchor,
  Col,
  DamImage,
  Row,
  BodyCopy,
  Button,
} from '@tcp/core/src/components/common/atoms';
import {
  getLocator,
  isGymboree,
  styleOverrideEngine,
  getBrand,
  getViewportInfo,
  isClient,
  configureInternalNavigationFromCMSUrl,
} from '@tcp/core/src/utils';
import { getHeaderBorderRadius, getMediaBorderRadius } from '@tcp/core/src/utils/utils.web';
import style, { ImageGrid, CtaButtonWrapper, ImageRoundFlex } from '../styles/ModuleM.style';
import config from '../moduleM.config';
import singleCTAButtonConfigured from './ModuleM.util';

const isImageLastElementOfGrid = (noOfGridItems, index) => {
  return noOfGridItems === index;
};

const getFirstElementIndex = (rows, rowFirstElement, index, rowMaxImages) => {
  return rows === 2 && index + 1 > rowMaxImages && rowFirstElement !== 0 ? 0 : rowFirstElement;
};

const getRowMaxImages = (imageData, viewportKey) => {
  return imageData ? imageData.rowMaxImages[viewportKey] : 0;
};

const getHeaderBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
    : {};
};

const getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};
/**
 * @class ModuleM - global reusable component will display display a featured
 * category module with category links and featured product images
 * This component is plug and play at any given slot in layout by passing required data
 * @param {headerText} headerText the list of data for header
 * @param {promoBanner} promoBanner promo banner data
 * @param {divTabs} divTabs division tabs data
 */
export class ModuleM extends React.PureComponent {
  constructor(props) {
    super(props);
    const { divTabs } = props;
    const viewportInfo = isClient() ? getViewportInfo() : null;
    this.state = {
      isMobile: viewportInfo && viewportInfo.isMobile,
      isTablet: viewportInfo && viewportInfo.isTablet,
      productCategoryImageList: divTabs && divTabs[0] ? divTabs[0].smallCompImage : [],
      singleCTAButton: divTabs && divTabs[0] ? divTabs[0].linkClass : {},
      activeTab: 'tablList-0',
    };
    this.gridImageRef = React.createRef();
  }

  /**
   * Returns module header
   */
  getHeaderText = (headerText, headerStyle) => {
    return (
      <div className="promo-header-wrapper">
        <LinkText
          component="h2"
          type="heading"
          headerText={headerText}
          className="promo-header"
          dataLocator={getLocator('moduleM_header_text')}
          headerStyle={headerStyle}
        />
      </div>
    );
  };

  /**
   * Returns promo banner for module
   */
  getPromoBanner = (promoBanner, promoStyle, backgroundStyle) => {
    return (
      promoBanner && (
        <PromoBanner
          promoBanner={promoBanner}
          className="promoBanner"
          dataLocator={getLocator('moduleM_promobanner_text')}
          promoStyle={promoStyle}
          backgroundStyle={backgroundStyle}
        />
      )
    );
  };

  /**
   * Returns viewport keys ie. sm|md|lg
   */
  getViewportKey = () => {
    let gutterViewportKey;
    const { isMobile, isTablet } = this.state;
    if (isMobile) gutterViewportKey = 'sm';
    else if (isTablet) gutterViewportKey = 'md';
    else gutterViewportKey = 'lg';

    return gutterViewportKey;
  };

  getNoOfGridItems = (noOfGridItems) => {
    return noOfGridItems === 4 ? noOfGridItems - 1 : noOfGridItems;
  };

  getToPathForImage = (link) => {
    return link.url && configureInternalNavigationFromCMSUrl(link.url);
  };

  /**
   *  Renders image grid for tab item
   */
  getProductImageGrid = (selectedProductList, backgroundStyle) => {
    const { singleCTAButton } = this.state;
    const { isHomePage } = this.props;
    const { noOfGridItems } = singleCTAButtonConfigured(singleCTAButton, selectedProductList);
    const viewportKey = this.getViewportKey();
    const imageData = config[`images${this.getNoOfGridItems(noOfGridItems)}`];
    const rowMaxImages = getRowMaxImages(imageData, viewportKey);
    let rowFirstElement = 0;
    let rowLastElement = rowMaxImages - 1;
    let rows = 1;
    if (!isClient()) {
      return null;
    }
    return (
      <Row className="image-items-container" noLastMargin>
        {selectedProductList &&
          selectedProductList.map((productItem, index) => {
            const { image, link, video } = productItem;
            /**
             * Calculate each row first element and last element and apply margin left and right respectively.
             */
            if (index + 1 === rowMaxImages * rows) {
              rows += 1;
              rowLastElement = index;
            }

            if (index === rowLastElement + 1) {
              rowFirstElement = index;
            }

            if (isImageLastElementOfGrid(noOfGridItems, index)) {
              rowLastElement = index;
            }
            // First element of second row
            rowFirstElement = getFirstElementIndex(rows, rowFirstElement, index, rowMaxImages);

            return (
              <ImageGrid
                colSize={imageData.colSize}
                length={noOfGridItems}
                offsetLeft={index === rowFirstElement ? imageData.offsetLeft : {}}
                offsetRight={rowLastElement === index ? imageData.offsetRight : {}}
                ignoreNthRule
                ignoreGutter={
                  rowLastElement === index ? { large: true, medium: true, small: true } : {}
                }
                isHomePage={isHomePage}
                lazyLoad
                isModule
              >
                <Anchor
                  to={this.getToPathForImage(link)}
                  asPath={link.url}
                  target={link?.target}
                  dataLocator={`${getLocator('moduleM_image')}_${index}`}
                  className="moduleM__productContainer"
                >
                  <div ref={this.gridImageRef} className="moduleM__productImage">
                    <DamImage
                      imgConfigs={config.IMG_DATA.productImgConfig}
                      imgData={image}
                      link={{ ...link, className: 'moduleM__productContainer' }}
                      videoData={video}
                      isHomePage={isHomePage}
                      isOptimizedVideo
                      lazyLoad
                      isModule
                    />
                  </div>
                  <BodyCopy
                    component="div"
                    className="moduleM__productName"
                    fontSize="fs15"
                    color="text.primary"
                    fontFamily="primary"
                  >
                    {link.text}
                  </BodyCopy>
                </Anchor>
              </ImageGrid>
            );
          })}

        {singleCTAButton && selectedProductList.length > 0 && (
          <CtaButtonWrapper
            length={selectedProductList.length}
            colSize={config[`images${selectedProductList.length}`].colSize}
            ignoreNthRule
            offsetRight={{}}
            ignoreGutter={{ large: true, medium: true, small: true }}
            isNotInlineBlock
            className="moduleM__gridButton"
            customStyle={backgroundStyle}
          >
            <Anchor
              to={configureInternalNavigationFromCMSUrl(singleCTAButton.url)}
              asPath={singleCTAButton.url}
              title={singleCTAButton.text}
              dataLocator={`${getLocator('moduleM_cta_btn')}`}
              className="moduleM__shopAllBtn"
            >
              {singleCTAButton.text}
            </Anchor>
          </CtaButtonWrapper>
        )}
      </Row>
    );
  };

  /**
   *  Renders flex image grid for tab item
   */
  getProductImageFlex = (selectedProductList, backgroundStyle) => {
    const { singleCTAButton } = this.state;
    const { isHomePage } = this.props;
    return (
      <div className="image-items-container__flex">
        {selectedProductList &&
          selectedProductList.map((product, index) => {
            const { link, image } = product;
            return (
              <div
                className={`image-items-container__flex--item ${
                  selectedProductList.length === 3 ? `widthAuto` : ``
                }`}
              >
                <Anchor
                  to={link.url}
                  asPath={link.url}
                  target={link?.target}
                  dataLocator={`${getLocator('moduleM_image')}_${index}`}
                >
                  <DamImage
                    imgConfigs={config.IMG_DATA.productImgConfig}
                    imgData={image}
                    link={link}
                    isHomePage={isHomePage}
                    lazyLoad
                    isModule
                  />
                  <BodyCopy
                    component="div"
                    className="moduleM__productName"
                    fontSize="fs15"
                    color="text.primary"
                    fontFamily="secondary"
                  >
                    {link.text}
                  </BodyCopy>
                </Anchor>
              </div>
            );
          })}
        {singleCTAButton ? (
          <div
            style={backgroundStyle}
            className={`moduleM__shopAllBtnWrapper ${
              selectedProductList.length === 3 ? `widthAuto` : ``
            }`}
          >
            <Anchor
              to={configureInternalNavigationFromCMSUrl(singleCTAButton.url)}
              asPath={singleCTAButton.url}
              title={singleCTAButton.text}
              dataLocator={`${getLocator('moduleM_cta_btn')}`}
              className="moduleM__shopAllBtn"
            >
              {singleCTAButton.text}
            </Anchor>
          </div>
        ) : null}
      </div>
    );
  };

  /**
   *  Renders image grid for tab item
   *  specific for Gymboree
   */
  getCategoryImageList = (isFlexBox, selectedProductList) => {
    const { isHomePage } = this.props;
    return (
      selectedProductList && (
        <div
          className={`image-items-container-category ${
            isFlexBox ? 'image-items-container-category__flex' : ''
          }`}
        >
          {selectedProductList &&
            selectedProductList.map((productItem, index) => {
              const { image, link } = productItem;
              return (
                <ImageRoundFlex className="imagecategory__list" isFlexBox={isFlexBox}>
                  <Anchor
                    to={link.url}
                    asPath={link.url}
                    target={link?.target}
                    dataLocator={`${getLocator('moduleM_image')}_${index}`}
                  >
                    <DamImage
                      imgConfigs={config.IMG_DATA.productImgConfig}
                      imgData={image}
                      link={link}
                      isHomePage={isHomePage}
                      lazyLoad
                      isModule
                    />
                    <BodyCopy
                      component="div"
                      className="moduleM__productName"
                      fontSize="fs15"
                      color="text.primary"
                      fontFamily="secondary"
                    >
                      {link.text}
                    </BodyCopy>
                  </Anchor>
                </ImageRoundFlex>
              );
            })}
        </div>
      )
    );
  };

  /**
   *  Returns type of grid item
   */
  getProductImageList = (isFlexBox, list, backgroundStyle) => {
    return isFlexBox
      ? this.getProductImageFlex(list, backgroundStyle)
      : this.getProductImageGrid(list, backgroundStyle);
  };

  onTabChange = (id, imageList, linkClass) => {
    this.setState({
      productCategoryImageList: imageList,
      singleCTAButton: linkClass,
      activeTab: id,
    });
  };

  /**
   *  Render button tab list
   */
  createProductTabList = (tabList) => {
    const { isHpNewDesignCTAEnabled } = this.props;
    return (
      tabList &&
      tabList.map((list, index) => {
        const { text: listItemText, smallCompImage, linkClass, id } = list;
        const { activeTab } = this.state;
        return (
          <div
            key={`modMTabs-${listItemText.text}`}
            className="product-tab-list__item"
            data-locator={`${getLocator('moduleM_cta_links')}_${index}`}
          >
            <Button
              active={id === activeTab}
              buttonVariation="mini-nav"
              onClick={() => this.onTabChange(id, smallCompImage, linkClass)}
              isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
            >
              {listItemText.text}
            </Button>
          </div>
        );
      })
    );
  };

  /**
   *  Creates button tab list data
   */
  createTabList = (tabList) =>
    tabList.map((list, index) => Object.assign({}, list, { id: `tablList-${index}` }));

  render() {
    const {
      className,
      headerText,
      promoBanner,
      divTabs,
      flexbox,
      moduleClassName,
      page,
      isHpNewModuleDesignEnabled,
    } = this.props;
    const { productCategoryImageList } = this.state;
    const isFlexBox = parseInt(flexbox, 10) === 1;
    const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleM');
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const backgroundStyle = styleOverrides.bground || {};
    const headerBorderRadiusOverride = getHeaderBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );

    return (
      <Grid className={`${className} ${moduleClassName} moduleM brand-${getBrand()} page-${page}`}>
        <Row fullBleed={{ small: true, medium: true }} className="topview">
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: 12,
            }}
            className="header-container"
            style={headerBorderRadiusOverride}
          >
            {headerText && this.getHeaderText(headerText, headerStyle)}
            {promoBanner && this.getPromoBanner(promoBanner, styleOverrides.promo, backgroundStyle)}
          </Col>
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: 12,
            }}
            className="product-tab-list"
          >
            {divTabs &&
              divTabs.length > 1 &&
              this.createProductTabList(this.createTabList(divTabs))}
          </Col>
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: 12,
            }}
            style={mediaBorderRadiusOverride}
          >
            {isGymboree()
              ? this.getCategoryImageList(isFlexBox, productCategoryImageList)
              : this.getProductImageList(isFlexBox, productCategoryImageList, backgroundStyle)}
          </Col>
        </Row>
      </Grid>
    );
  }
}

ModuleM.propTypes = {
  className: PropTypes.string,
  headerText: PropTypes.shape([]),
  promoBanner: PropTypes.shape([]),
  divTabs: PropTypes.shape([]),
  flexbox: PropTypes.string,
  singleCTAButton: PropTypes.shape({}),
  ctaItems: PropTypes.shape({}),
  moduleClassName: PropTypes.string,
  page: PropTypes.string,
  isHomePage: PropTypes.bool.isRequired,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

ModuleM.defaultProps = {
  className: '',
  headerText: [],
  promoBanner: [],
  divTabs: [],
  type: 0,
  singleCTAButton: [],
  ctaItems: [],
  moduleClassName: '',
  page: '',
};

const styledModuleM = errorBoundary(withStyles(ModuleM, style));
styledModuleM.defaultProps = ModuleM.defaultProps;
export default styledModuleM;
export { ModuleM as ModuleMVanilla };
