// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mock from '../../../../../services/abstractors/common/moduleQ/mock';
import StyliticsProductTabList from '../../../organisms/StyliticsProductTabList';
import { ModuleQVanilla as ModuleQ } from '../views/ModuleQ';

const styliticsDomainURL = 'https://content.stylitics.com/images/collage';

let ModuleQComp;
const styliticsProductTabList = {
  '2044392_10': [
    {
      id: 138548,
      imageUrl: `${styliticsDomainURL}/138548/lookbook.png`,
      largeImageUrl: `${styliticsDomainURL}/138548/original.png`,
      items: [
        {
          smallImageUrl: `${styliticsDomainURL}/936320/small.png`,
          name: 'Girls Uniform Cardigan',
          remoteId: '2101602_054',
        },
        {
          smallImageUrl: `${styliticsDomainURL}/277231/small.png`,
          name: 'Girls Uniform Pintuck Poplin Button Down Shirt 1',
          remoteId: '2044392_10',
        },
        {
          smallImageUrl: `${styliticsDomainURL}/358504/small.png`,
          name: 'Girls Uniform Ponte Knit Pull On Jeggings',
          remoteId: '2110252_IV',
        },
        {
          smallImageUrl: `${styliticsDomainURL}/934353/small.png`,
          name: 'Girls Uniform T Strap Sneakers',
          remoteId: '2623363_IV',
        },
        {
          smallImageUrl: `${styliticsDomainURL}/938041/small.png`,
          name: 'Girls Digital Watch',
          remoteId: '2079174_BQ',
        },
      ],
      pdpUrl:
        'https://www.childrensplace.com/us/outfit/138548/2101602_054-2044392_10-2110252_IV-2623363_IV-2079174_BQ',
    },
    {
      id: 141076,
      imageUrl: `${styliticsDomainURL}/141076/lookbook.png`,
      largeImageUrl: `${styliticsDomainURL}/141076/original.png`,
      items: [
        {
          smallImageUrl: `${styliticsDomainURL}/277231/small.png`,
          name: 'Girls Uniform Pintuck Poplin Button Down Shirt',
          remoteId: '2044392_10',
        },
        {
          smallImageUrl: `${styliticsDomainURL}/276960/small.png`,
          name: 'Girls Uniform Skinny Chino Pants',
          remoteId: '2045419_9S',
        },
        {
          smallImageUrl: `${styliticsDomainURL}/934353/small.png`,
          name: 'Girls Uniform T Strap Sneakers',
          remoteId: '2623363_IV',
        },
        {
          smallImageUrl: `${styliticsDomainURL}/938041/small.png`,
          name: 'Girls Digital Watch',
          remoteId: '2079174_BQ',
        },
        {
          smallImageUrl: `${styliticsDomainURL}/938048/small.png`,
          name: 'Girls Uniform Windbreaker',
          remoteId: '3001063_IV',
        },
      ],
      pdpUrl:
        'https://www.childrensplace.com/us/outfit/141076/2044392_10-2045419_9S-2623363_IV-2079174_BQ-3001063_IV',
    },
  ],
};
const singleCTAButton = {
  url: 'http://www.childrensplace.com',
  text: 'SHOP ALL 3',
  title: 'SHOP ALL',
  target: '',
  external: 0,
  action: '',
};

beforeEach(() => {
  const wrapper = shallow(
    <ModuleQ
      {...mock.moduleQ.composites}
      styliticsProductTabList={styliticsProductTabList}
      singleCTAButton={singleCTAButton}
    />
  );
  ModuleQComp = wrapper;
});

describe('ModuleQ component', () => {
  it('renders correctly', () => {
    expect(ModuleQComp).toMatchSnapshot();
  });

  it('Module has header', () => {
    expect(ModuleQComp.find('.moduleQ-header')).toHaveLength(1);
  });

  it('Module has promo banner', () => {
    expect(ModuleQComp.find('.moduleQ-promo')).toHaveLength(1);
  });

  it('Module has tab list', () => {
    expect(ModuleQComp.find(StyliticsProductTabList)).toHaveLength(1);
  });

  it('Module has carousel', () => {
    expect(ModuleQComp.find('.moduleQ__carousel-wrapper')).toHaveLength(1);
  });

  it('Module renders carousel items', () => {
    ModuleQComp.instance().getSlideItem(styliticsProductTabList['2044392_10'][0], '1');
  });

  it('Module should set current item state', () => {
    ModuleQComp.instance().onProductTabChange('2044392_10', styliticsProductTabList['2044392_10']);
  });
});
