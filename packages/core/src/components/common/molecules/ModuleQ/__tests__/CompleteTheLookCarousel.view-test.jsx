// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';

let props = {};
beforeEach(() => {
  const pdpUrlValue = '/p/2100610_IV';
  const categoryIdValue = '47503>47544';
  const pdpSeoUrlValue = 'Boys-Uniform-Long-Sleeve-Cardigan-2100610-IV';
  props = {
    className: 'class sc-hUMlYv ecVuvR',
    currentCatId: '2100606_IV',
    productId: 834677,
    outfitProducts: [
      {
        ratingsProductId: '2100610',
        generalProductId: '2100610_IV',
        categoryId: categoryIdValue,
        category: '',
        name: 'Boys Uniform Cardigan',
        pdpUrl: '/p/2100611_IV',
        colorFitsSizesMap: [
          {
            color: {
              name: 'TIDAL',
              imagePath: null,
              family: 'BLUE',
              swatchImage: '2100610_IV_swatch.jpg',
            },
            pdpUrl: pdpUrlValue,
            pdpSeoUrl: pdpSeoUrlValue,
            name: null,
            colorProductId: '1118010',
            colorDisplayId: '2100610_IV',
            categoryEntity: '',
            imageName: '2100610_IV',
            favoritedCount: 1732,
            maxAvailable: 12048,
            maxAvailableBoss: 0,
            hasFits: false,
            miscInfo: {
              isBopisEligible: true,
              isBossEligible: true,
              badge1: {},
              badge2: '60% OFF',
              keepAlive: false,
              isInDefaultWishlist: false,
            },
            listPrice: 29.95,
            offerPrice: 29.95,
            highListPrice: 0,
            highOfferPrice: 0,
            lowListPrice: 0,
            lowOfferPrice: 0,
          },
        ],
        isGiftCard: false,
        colorFitSizeDisplayNames: null,
        listPrice: 29.95,
        offerPrice: 29.95,
        highListPrice: 0,
        highOfferPrice: 0,
        lowListPrice: 0,
        lowOfferPrice: 0,
        onlyListPrice: 29.95,
        ratings: 4.5,
        reviewsCount: 13,
      },
      {
        ratingsProductId: '2036097',
        generalProductId: '2036097_82',
        categoryId: '458031>491009',
        category: '',
        name: 'Boys Uniform Braided Belt',
        pdpUrl: '/p/2036097_82',
        colorFitsSizesMap: [
          {
            color: {
              name: 'BROWN',
              imagePath: null,
              family: 'BROWN',
              swatchImage: '2036097_82_swatch.jpg',
            },
            pdpUrl: '/p/2036097_82',
            pdpSeoUrl: 'Boys-Uniform-Braided-Belt-2036097-82',
            name: null,
            colorProductId: '518667',
            colorDisplayId: '2036097_82',
            categoryEntity: '',
            imageName: '2036097_82',
            favoritedCount: 2149,
            maxAvailable: 10011973,
            maxAvailableBoss: 0,
            hasFits: false,
            miscInfo: {
              isBopisEligible: true,
              isBossEligible: true,
              badge1: {},
              badge2: '50% OFF',
              keepAlive: false,
              isInDefaultWishlist: false,
            },
            fits: [
              {
                fitName: '',
                isDefault: true,
                maxAvailable: 1.7976931348623157e308,
                sizes: [
                  {
                    sizeName: '8-16',
                    skuId: '1179047',
                    listPrice: 9.95,
                    offerPrice: 9.95,
                    maxAvailable: 6563,
                    variantId: '00191755836621',
                    variantNo: '2036097003',
                    position: 0,
                  },
                  {
                    sizeName: '4-7',
                    skuId: '872407',
                    listPrice: 9.95,
                    offerPrice: 9.95,
                    maxAvailable: 5420,
                    variantId: '00889705459265',
                    variantNo: '2036097001',
                    position: 1,
                  },
                  {
                    sizeName: '8-14',
                    skuId: '872472',
                    listPrice: 9.95,
                    offerPrice: 9.95,
                    maxAvailable: 9999990,
                    variantId: '00889705459272',
                    variantNo: '2036097002',
                    position: 2,
                  },
                ],
              },
            ],
            listPrice: 9.95,
            offerPrice: 9.95,
            highListPrice: 0,
            highOfferPrice: 0,
            lowListPrice: 0,
            lowOfferPrice: 0,
          },
        ],
        isGiftCard: false,
        colorFitSizeDisplayNames: null,
        listPrice: 9.95,
        offerPrice: 9.95,
        highListPrice: 0,
      },
    ],
    productInfo: {
      ratingsProductId: '2100606',
      generalProductId: '2100606_IV',
      categoryId: categoryIdValue,
      category: '',
      name: 'Boys Uniform V-Neck Sweater',
      pdpUrl: '/p/2100606_IV',
      shortDescription: 'School-approved style for your super cool dude!',
      longDescription:
        '<li>Made of 100% cotton in a sweater knit</li><li>Rib-knit V neck, sleeve cuffs and hem</li><li>Fully-fashioned sleeves for ease of movement</li><li>Pre-washed for added softness and to reduce shrinkage</li><li>Imported</li>',
      imagesByColor: {
        SMOKEB10: {
          extraImages: [
            {
              isShortImage: false,
              isOnModalImage: false,
            },
          ],
        },
      },
      colorFitsSizesMap: [
        {
          color: {
            name: 'TIDAL',
            imagePath: null,
            family: 'BLUE',
            swatchImage: '2100606_IV_swatch.jpg',
          },
          pdpUrl: '/p/2100606_IV',
          pdpSeoUrl: 'Boys-Uniform-Long-Sleeve-V-Neck-Sweater-2100606-IV',
          name: null,
          colorProductId: '1118007',
          colorDisplayId: '2100606_IV',
          categoryEntity: '',
          imageName: '2100606_IV',
          favoritedCount: 2115,
          maxAvailable: 13429,
          maxAvailableBoss: 5552,
          hasFits: false,
          miscInfo: {
            isBopisEligible: true,
            isBossEligible: true,
            badge1: {},
            badge2: '60% OFF',
            keepAlive: false,
            isInDefaultWishlist: false,
          },
          fits: [
            {
              fitName: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'XS (4)',
                  skuId: '1118991',
                  listPrice: 24.95,
                  offerPrice: 24.95,
                  maxAvailable: 387,
                  maxAvailableBoss: 153,
                  variantId: '00191755242996',
                  variantNo: '2100606007',
                  position: 0,
                },
                {
                  sizeName: 'S (5/6)',
                  skuId: '1119731',
                  listPrice: 24.95,
                  offerPrice: 24.95,
                  maxAvailable: 2980,
                  maxAvailableBoss: 1203,
                  variantId: '00191755243009',
                  variantNo: '2100606008',
                  position: 1,
                },
              ],
            },
          ],
          listPrice: 24.95,
          offerPrice: 24.95,
          highListPrice: 0,
          highOfferPrice: 0,
          lowListPrice: 24.95,
          lowOfferPrice: 0,
        },
        {
          color: {
            name: 'SMOKEB10',
            imagePath: null,
            family: 'GRAY',
            swatchImage: '2100606_1137_swatch.jpg',
          },
          pdpUrl: '/p/2100606_1137',
          pdpSeoUrl: 'Boys-Uniform-Long-Sleeve-V-Neck-Sweater-2100606-1137',
          name: null,
          colorProductId: '1118006',
          colorDisplayId: '2100606_1137',
          categoryEntity: '',
          imageName: '2100606_1137',
          favoritedCount: 1462,
          maxAvailable: 7168,
          maxAvailableBoss: 1449,
          hasFits: false,
          miscInfo: {
            isBopisEligible: true,
            isBossEligible: true,
            badge1: {},
            badge2: '60% OFF',
            keepAlive: false,
            isInDefaultWishlist: false,
          },
          fits: [
            {
              fitName: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'XS (4)',
                  skuId: '1118990',
                  listPrice: 24.95,
                  offerPrice: 24.95,
                  maxAvailable: 196,
                  maxAvailableBoss: 62,
                  variantId: '00191755242934',
                  variantNo: '2100606001',
                  position: 0,
                },
              ],
            },
          ],
          listPrice: 24.95,
          offerPrice: 24.95,
          highListPrice: 0,
          highOfferPrice: 0,
          lowListPrice: 24.95,
          lowOfferPrice: 0,
        },
      ],
      isGiftCard: false,
      colorFitSizeDisplayNames: null,
      listPrice: 24.95,
      offerPrice: 24.95,
      highListPrice: 0,
      highOfferPrice: 0,
      lowListPrice: 24.95,
      lowOfferPrice: 0,
      onlyListPrice: 24.95,
      ratings: 0,
      reviewsCount: 0,
      unbxdProdId: '2100606_IV',
      alternateSizes: {},
      productId: '2100606_IV',
      promotionalMessage:
        "<span class='product-loyalty\u001apromotionText user-tier-theme'> EARN DOUBLE/TRIPLE POINTS </span> on Holiday Dressy!",
      promotionalPLCCMessage: '',
      long_product_title: 'Boys Uniform Long Sleeve V-Neck Sweater',
      product_long_description:
        '<li>Made of 100% cotton in a sweater knit</li><li>Rib-knit V neck, sleeve cuffs and hem</li><li>Fully-fashioned sleeves for ease of movement</li><li>Pre-washed for added softness and to reduce shrinkage</li><li>Imported</li>',
      bundleProducts: [],
      categoryPathMap: [
        '47503>47544>54134|BOY>Boy>Sweaters & Cardigans',
        '47503>47539>54121|BOY>School Uniforms>Tops',
        '476001>488006>488009|Family Shop>Holiday Dress Up>Big & Lil Bro',
        '476001>488006>488007|Family Shop>Holiday Dress Up>All Matching Family',
      ],
      seoToken: 'Boys-Uniform-Long-Sleeve-V-Neck-Sweater-2100606-IV',
      isSSRData: false,
      bazaarVoice: {
        avgRating: 4.7368421052631575,
        totalReviewCount: 19,
      },
    },
    currentProduct: [
      {
        ratingsProductId: '2100610',
        generalProductId: '2100610_IV',
        categoryId: categoryIdValue,
        category: '',
        name: 'Boys Uniform Cardigan',
        pdpUrl: pdpUrlValue,
        shortDescription: 'A school (and weekend)approved style!',
        longDescription: '',
        colorFitsSizesMap: [
          {
            color: {
              name: 'TIDAL',
              imagePath: null,
              family: 'BLUE',
              swatchImage: '2100610_IV_swatch.jpg',
            },
            pdpUrl: pdpUrlValue,
            pdpSeoUrl: pdpSeoUrlValue,
            name: null,
            colorProductId: '1118010',
            colorDisplayId: '2100610_IV',
            categoryEntity: '',
            imageName: '2100610_IV',
            favoritedCount: 1732,
            maxAvailable: 12048,
            maxAvailableBoss: 0,
            hasFits: false,
            miscInfo: {
              isBopisEligible: true,
              isBossEligible: true,
              badge1: {},
              badge2: '60% OFF',
              keepAlive: false,
              isInDefaultWishlist: false,
            },
            fits: [
              {
                fitName: '',
                isDefault: true,
                maxAvailable: 1.7976931348623157e308,
                sizes: [
                  {
                    sizeName: 'XS (4)',
                    skuId: '1118994',
                    listPrice: 29.95,
                    offerPrice: 29.95,
                    maxAvailable: 273,
                    variantId: '00191755243177',
                    variantNo: '2100610001',
                    position: 0,
                  },
                ],
              },
            ],
            listPrice: 29.95,
            offerPrice: 29.95,
            highListPrice: 0,
            highOfferPrice: 0,
            lowListPrice: 0,
            lowOfferPrice: 0,
          },
        ],
        isGiftCard: false,
        colorFitSizeDisplayNames: null,
        listPrice: 29.95,
        offerPrice: 29.95,
        highListPrice: 0,
        highOfferPrice: 0,
        lowListPrice: 0,
        seoToken: pdpSeoUrlValue,
      },
    ],
    isOutfitPage: true,
    errorOnHandleSubmit: null,
  };
});

describe('CompleteTheLookCaraousel component', () => {
  it('renders correctly', () => {
    const componentInstance = shallow(<styledCompleteTheLook {...props} />);
    expect(componentInstance).toMatchSnapshot();
  });
  it('renders OOS correctly', () => {
    const componentInstance = shallow(<styledCompleteTheLook {...props} outfitProducts={null} />);
    expect(componentInstance).toMatchSnapshot();
  });
});
