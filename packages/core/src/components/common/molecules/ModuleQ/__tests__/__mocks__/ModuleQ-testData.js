// 9fbef606107a605d69c0edbcd8029e5d
export default {
  completed: {
    '3013117_499': false,
  },
  '3013117_499': [
    {
      id: 909089,
      subItemsId: '3013117_499-2092425_DA-3011476_BQ-3012094_10-3013595_WJ',
      imageUrl: 'https://content.stylitics.com/images/collage/909089',
      largeImageUrl: 'https://content.stylitics.com/images/collage/909089',
      items: [],
      pdpUrl: '/outfit/909089/3013117_499-2092425_DA-3011476_BQ-3012094_10-3013595_WJ',
    },
    {
      id: 909090,
      subItemsId: '3013117_499-2044139_757-3011497_BQ-3011006_BQ-3010872_87',
      imageUrl: 'https://content.stylitics.com/images/collage/909090',
      largeImageUrl: 'https://content.stylitics.com/images/collage/909090',
      items: [],
      pdpUrl: '/outfit/909090/3013117_499-2044139_757-3011497_BQ-3011006_BQ-3010872_87',
    },
  ],
  errors: {
    '3013117_499': null,
  },
};
