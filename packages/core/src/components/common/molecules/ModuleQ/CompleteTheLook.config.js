// 9fbef606107a605d69c0edbcd8029e5d
export default {
  TOTAL_IMAGES: 7,
  CAROUSEL_OPTIONS: {
    autoplay: false,
    arrows: true,
    centerMode: false,
    centerPadding: '0px',
    fade: false,
    speed: 200,
    swipeToSlide: true,
    swipe: true,
    infinite: true,
    slidesToShow: 4,

    APP: {
      SMALL_SIZE: {
        PRODUCT_IMAGE_WIDTH_SMALL: 147,
        PRODUCT_IMAGE_HEIGHT_SMALL: 184,
        MODULE_HEIGHT_SMALL: 200,
        ITEM_WIDTH_SMALL: 200,
      },
      PRODUCT_IMAGE_WIDTH: 207,
      PRODUCT_IMAGE_HEIGHT: 244,
      MODULE_HEIGHT: 260,
      ITEM_WIDTH: 260,
    },
  },
  IMG_DATA: {
    productImgConfig: ['t_mod_Q_product_m', 't_mod_Q_img_product_t', 't_mod_Q_img_product_d'],
  },
};
