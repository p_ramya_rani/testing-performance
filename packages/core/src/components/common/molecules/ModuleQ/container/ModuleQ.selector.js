// 9fbef606107a605d69c0edbcd8029e5d
import { getABtestFromState } from '@tcp/core/src/utils';

export const getStyliticsProductTabListSelector = (state) => state.StyliticsProductTabList;

export const getLabel = (state) => {
  const { global: { modules } = {} } = state.Labels;
  let content;
  if (modules) {
    content = modules.lbl_moduleQ_shop_this_look || '';
  }
  return content;
};

export const getCTLNewDesignLabel = (state) => {
  const { global: { modules } = {} } = state.Labels;
  let content;
  if (modules) {
    content = modules.lbl_moduleQ_shop_this_look_redesign || 'lbl_shop_this_look_redesign';
  }
  return content;
};

export const getCTLImageABTest = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests.showSmallSizeCTLImage;
};

export default {
  getStyliticsProductTabListSelector,
  getLabel,
  getCTLImageABTest,
  getCTLNewDesignLabel,
};
