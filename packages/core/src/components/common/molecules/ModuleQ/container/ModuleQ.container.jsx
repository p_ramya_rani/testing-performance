// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCurrencyAttributes } from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import CompleteTheLook from '../views/CompleteTheLook.view';
import ModuleQView from '../views';
import {
  getIsKeepAliveProduct,
  getCurrentCurrency,
  getIsInternationalShipping,
  getIsViewThisLookLabelABTestEnabled,
  getIsNewPDPEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import {
  getOutfitDetails,
  setOutfitCarouselModalState,
} from '../../../../features/browse/OutfitDetails/container/OutfitDetails.actions';
import { isPlccUser } from '../../../../features/account/User/container/User.selectors';
import {
  getOutfitProducts,
  getShowAddedToBagCTA,
  getOutfitDetailFormValues,
  getIsOutfitAddedCTAEnabled,
  getOutfitCarouselAddedCTA,
} from '../../../../features/browse/OutfitDetails/container/OutfitDetails.selectors';
import { OUTFIT_LISTING_FORM } from '../../../../../constants/reducer.constants';
import {
  getAddedToBagError,
  addedToBagMsgSelector,
} from '../../../../features/CnC/AddedToBag/container/AddedToBag.selectors';
import {
  getLabel,
  getStyliticsProductTabListSelector,
  getCTLImageABTest,
  getCTLNewDesignLabel,
} from './ModuleQ.selector';
import { addToCartEcom } from '../../../../features/CnC/AddedToBag/container/AddedToBag.actions';
import {
  prodDetails,
  getPlpLabels,
  getAlternateSizes,
  getCurrentProduct,
  getSizeChartDetails,
  getPDPLoadingState,
} from '../../../../features/browse/ProductDetail/container/ProductDetail.selectors';
import getAddedToBagFormValues from '../../../../../reduxStore/selectors/form.selectors';
import { getCartItemInfo } from '../../../../features/CnC/AddedToBag/util/utility';
import { isCanada } from '../../../../../utils';
import { getLabelsOutOfStock } from '../../../../features/browse/ProductListing/container/ProductListing.selectors';

const handleAddToBag = (
  addToBagEcom,
  productInfo,
  generalProductId,
  currentState,
  isEnabledOutfitAddedCTA,
  fromOutfitCarouselDetails = false
) => {
  const formName = `${OUTFIT_LISTING_FORM}-${generalProductId}`;
  const formValues = getAddedToBagFormValues(currentState, formName);
  let cartItemInfo = getCartItemInfo(productInfo, formValues);
  cartItemInfo = {
    ...cartItemInfo,
    fromCompleteTheLook: false,
    isOutfitPage: true,
    skipAddToBagModal: isEnabledOutfitAddedCTA || fromOutfitCarouselDetails,
    fromOutfitCarouselDetails,
  };
  addToBagEcom(cartItemInfo);
};

export const ModuleQ = ({
  productDetails,
  isRelatedOutfit,
  addToBagEcom,
  currentState,
  addBtnMsg,
  addToBagError,
  isEnabledOutfitAddedCTA,
  isSBP,
  isNewReDesignCTL,
  onRequestClose,
  fromPage,
  isViewThisLookLabelEnabled,
  pdpLabels,
  ...otherProps
}) =>
  isRelatedOutfit ? (
    <CompleteTheLook
      handleAddToBag={handleAddToBag}
      addToBagEcom={addToBagEcom}
      currentState={currentState}
      productDetails={productDetails}
      addBtnMsg={addBtnMsg}
      addToBagError={addToBagError}
      isSBP={isSBP}
      isNewReDesignCTL={isNewReDesignCTL}
      isEnabledOutfitAddedCTA={isEnabledOutfitAddedCTA}
      isRelatedOutfit
      fromPage={fromPage}
      onRequestClose={onRequestClose}
      isViewThisLookLabelEnabled={isViewThisLookLabelEnabled}
      pdpLabels={pdpLabels}
      {...otherProps}
    />
  ) : (
    <ModuleQView {...otherProps} />
  );

export const mapStateToProps = (state, ownProps) => {
  const { moduleClassName = '', icidParam = '' } = ownProps;
  const showOutfitCarouselAddedCTA = getOutfitCarouselAddedCTA(state);
  return {
    styliticsProductTabList: getStyliticsProductTabListSelector(state),
    shopThisLookLabel: getLabel(state),
    shopThisLookLabelNewDesign: getCTLNewDesignLabel(state),
    showSmallImage: getCTLImageABTest(state),
    outfitProducts: getOutfitProducts(state),
    icidParam,
    moduleClassName,
    addToBagError: getAddedToBagError(state),
    productDetails: prodDetails(state),
    plpLabels: getPlpLabels(state),
    formValues: getOutfitDetailFormValues(state),
    isKeepAliveEnabled: getIsKeepAliveProduct(state),
    alternateSizes: getAlternateSizes(state),
    productInfo: getCurrentProduct(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    sizeChartDetails: getSizeChartDetails(state),
    isLoading: getPDPLoadingState(state),
    showAddedToBagCta: getShowAddedToBagCTA(state),
    showOutfitCarouselAddedCTA,
    priceCurrency: getCurrentCurrency(state),
    currentState: state,
    currencyAttributes: getCurrencyAttributes(state),
    isCanada: isCanada(),
    isPlcc: isPlccUser(state),
    isInternationalShipping: getIsInternationalShipping(state),
    addBtnMsg: addedToBagMsgSelector(state),
    isEnabledOutfitAddedCTA: !isCanada() && getIsOutfitAddedCTAEnabled(state),
    isViewThisLookLabelEnabled: getIsViewThisLookLabelABTestEnabled(state),
    isNewPDPEnabled: getIsNewPDPEnabled(state),
  };
};

function mapDispatchToProps(dispatch) {
  return {
    getOutfit: (payload) => {
      dispatch(getOutfitDetails(payload));
    },
    addToBagEcom: (payload) => {
      dispatch(addToCartEcom(payload));
    },
    setOutfitCarouselState: (payload) => dispatch(setOutfitCarouselModalState(payload)),
  };
}

ModuleQ.propTypes = {
  isRelatedOutfit: PropTypes.bool,
  productDetails: PropTypes.shape({}).isRequired,
  addToBagEcom: PropTypes.func.isRequired,
  currentState: PropTypes.shape({}).isRequired,
  addBtnMsg: PropTypes.string.isRequired,
  addToBagError: PropTypes.string.isRequired,
  isEnabledOutfitAddedCTA: PropTypes.bool.isRequired,
  isSBP: PropTypes.bool,
  isNewReDesignCTL: PropTypes.bool,
  onRequestClose: PropTypes.func,
  fromPage: PropTypes.string,
  isViewThisLookLabelEnabled: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  isNewPDPEnabled: PropTypes.bool,
};

ModuleQ.defaultProps = {
  isRelatedOutfit: false,
  isSBP: false,
  isNewReDesignCTL: false,
  onRequestClose: () => {},
  fromPage: '',
  isViewThisLookLabelEnabled: false,
  pdpLabels: {},
  isNewPDPEnabled: false,
};
export default connect(mapStateToProps, mapDispatchToProps)(ModuleQ);
