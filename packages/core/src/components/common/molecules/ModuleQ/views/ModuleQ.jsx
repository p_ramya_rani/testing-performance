// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  getIconPath,
  getLocator,
  styleOverrideEngine,
  mergeUrlQueryParams,
  configureInternalNavigationFromCMSUrl,
} from '@tcp/core/src/utils';
import { getHeaderBorderRadius, getMediaBorderRadius } from '@tcp/core/src/utils/utils.web';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/views/Carousel';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import { Anchor, Button, BodyCopy, Col, Image, Row } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import StyliticsProductTabList from '@tcp/core/src/components/common/organisms/StyliticsProductTabList';
import moduleQStyle, { StyledSkeleton } from '../styles/ModuleQ.style';
import config from '../ModuleQ.config';
import constant from '../ModuleQ.constant';

/**
 * @class ModuleQ - global reusable component will display a featured content
 * module with 2-4 tabs of outfits carousels of up to 7 outfits each
 * This component is plug and play at any given slot in layout by passing required data
 * @param {headerText} headerText the list of data for header texts
 * @param {promoBanner} promoBanner the list of data for promo banner
 * @param {divTabs} divTabs the list of data for product tabs
 */
class ModuleQ extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentCatId: '',
      currentTabItem: [],
    };
  }

  getHeaderBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  /* Return the offset object for main wrapper */
  getWrapperOffset = (fullBleed) => {
    return {
      small: 0,
      medium: 0,
      large: fullBleed ? 0.5 : 2,
    };
  };

  onProductTabChange = (catId, tabItem) => {
    this.setState({ currentCatId: catId, currentTabItem: [tabItem] });
  };

  /** This method is to add protocol to image url
   * since image is coming from unbox without protocol
   * and it breaks on local without protocol
   * TODO: It will be removed refactored later
   */
  getUrlWithHttp = (url) => url.replace(/(^\/\/)/, 'https:$1');

  /**
   * This function is being called to render carousel items.
   * @param {object} item image object for the carousel
   * @param {integer} index Index for the carousel item
   */
  getSlideItem = (item, index, icidParam) => {
    const { id, items, largeImageUrl, pdpUrl } = item;
    const { shopThisLookLabel, page } = this.props;
    const looksImages = items.slice(0, 2);
    const hiddenImagesCount = items.length - looksImages.length;
    const outfitParams = pdpUrl && pdpUrl.split('/');

    const pdpAsPathWithTrackingParams = mergeUrlQueryParams(pdpUrl, icidParam);

    const { RECOMMENDATION } = constant;
    const outfitUrl =
      outfitParams &&
      outfitParams.length > 1 &&
      `/outfit?outfitId=${outfitParams[outfitParams.length - 2]}&vendorColorProductIdsList=${
        outfitParams[outfitParams.length - 1]
      }&viaModule=${RECOMMENDATION}&viaPage=${page}`;
    return (
      <div>
        <Anchor
          key={id}
          className="moduleQ-image-link"
          to={outfitUrl}
          asPath={pdpAsPathWithTrackingParams}
          dataLocator={`${getLocator('moduleQ_product_image')}${index}`}
        >
          <div className="looks-large-image">
            <Image alt={id} src={this.getUrlWithHttp(largeImageUrl)} />
            <div className="shop-this-look-link">
              <Anchor
                to={outfitUrl}
                asPath={pdpAsPathWithTrackingParams}
                withCaret
                centered
                tabIndex={-1}
              >
                <BodyCopy component="span" color="gray.900" fontFamily="secondary" fontSize="fs12">
                  {shopThisLookLabel}
                </BodyCopy>
              </Anchor>
            </div>
          </div>
          <div className="looks-images-wrapper">
            {looksImages.map(({ smallImageUrl, name, remoteId }) => {
              return (
                <div className="looks-image">
                  <Image key={remoteId} alt={name} src={this.getUrlWithHttp(smallImageUrl)} />
                </div>
              );
            })}
            {hiddenImagesCount > 0 ? (
              <div className="looks-image looks-image-last">
                <BodyCopy
                  color="gray.900"
                  fontFamily="secondary"
                  fontSize="fs22"
                  fontWeight="extrabold"
                >
                  {`+${hiddenImagesCount}`}
                </BodyCopy>
              </div>
            ) : null}
          </div>
        </Anchor>
      </div>
    );
  };

  getCurrentCtaButton = () => {
    const { currentTabItem } = this.state;
    const { isHpNewDesignCTAEnabled } = this.props;
    if (!currentTabItem || !currentTabItem.length) {
      return null;
    }
    const { singleCTAButton: currentSingleCTAButton } = currentTabItem.length && currentTabItem[0];

    return currentSingleCTAButton ? (
      <div className="moduleQ_cta">
        <Anchor
          to={configureInternalNavigationFromCMSUrl(currentSingleCTAButton.url)}
          target={currentSingleCTAButton.target}
          title={currentSingleCTAButton.title}
          asPath={currentSingleCTAButton.url}
          dataLocator={getLocator('moduleQ_cta_btn')}
        >
          <Button
            buttonVariation="variable-width"
            className="cta-btn"
            isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
          >
            {currentSingleCTAButton.text}
          </Button>
        </Anchor>
      </div>
    ) : null;
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      className,
      bgClass,
      divTabs,
      headerText,
      promoBanner,
      styliticsProductTabList,
      hideTabs,
      selectedColorProductId,
      showRelatedOutfitHeader,
      fullBleed,
      moduleClassName,
      getCompleteTheLookSlot,
      icidParam,
      isHpNewModuleDesignEnabled,
    } = this.props;
    const { currentCatId } = this.state;
    const { CAROUSEL_OPTIONS, TOTAL_IMAGES, MINIMUM_IMAGES } = config;
    let selectedProductList = styliticsProductTabList[currentCatId] || [];
    selectedProductList = selectedProductList.slice(0, TOTAL_IMAGES);
    const showCarousel = selectedProductList && selectedProductList.length >= MINIMUM_IMAGES;

    const IconPath = getIconPath('carousel-big-carrot');
    let dataStatus = true;
    if (styliticsProductTabList && styliticsProductTabList.completed) {
      dataStatus = styliticsProductTabList.completed[currentCatId];
    }

    if (selectedProductList.length === MINIMUM_IMAGES) {
      CAROUSEL_OPTIONS.initialSlide = 1;
    }

    if (showCarousel && showRelatedOutfitHeader) {
      showRelatedOutfitHeader(true);
      getCompleteTheLookSlot(selectedProductList[0]);
    }
    const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleQ');
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const backgroundColor = styleOverrides.bground || {};
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );

    return (
      <Row
        customStyle={backgroundColor}
        className={`moduleQ ${className} ${bgClass} ${moduleClassName}`}
        fullBleed={{ small: true, medium: true, large: false }}
      >
        <Col
          colSize={{
            small: 6,
            medium: 8,
            large: 12,
          }}
        >
          <Row centered>
            <Col
              colSize={{
                small: 6,
                medium: 8,
                large: 12,
              }}
              style={headerBorderRadiusOverride}
            >
              {headerText && (
                <LinkText
                  component="h2"
                  type="heading"
                  headerText={headerText}
                  className="moduleQ-header"
                  headingClass="moduleQ-promo-header"
                  dataLocator={getLocator('moduleQ_header_text')}
                  headerStyle={headerStyle}
                />
              )}
              {!hideTabs && promoBanner && (
                <PromoBanner
                  promoBanner={promoBanner}
                  className="moduleQ-promo"
                  dataLocator={getLocator('moduleQ_promobanner_text')}
                  promoStyle={styleOverrides.promo}
                />
              )}
            </Col>
            <div>
              <StyliticsProductTabList
                onProductTabChange={this.onProductTabChange}
                tabItems={divTabs}
                selectedColorProductId={selectedColorProductId}
                dataLocator={getLocator('moduleQ_cta_link')}
              />
            </div>
          </Row>
          <Row fullBleed={{ small: true, medium: true }}>
            <Col
              className="moduleQ__carousel-wrapper"
              colSize={{
                small: 6,
                medium: 8,
                large: fullBleed ? 11 : 8,
              }}
              style={mediaBorderRadiusOverride}
              offsetLeft={this.getWrapperOffset(fullBleed)}
              offsetRight={this.getWrapperOffset(fullBleed)}
            >
              {dataStatus ? (
                <StyledSkeleton
                  col={3}
                  colSize={{ small: 2, medium: 2, large: 4 }}
                  removeLastMargin
                  showArrows
                />
              ) : null}
              {showCarousel ? (
                <Carousel
                  key={currentCatId.toString()}
                  options={CAROUSEL_OPTIONS}
                  carouselConfig={{
                    autoplay: false,
                    variation: 'big-arrows',
                    customArrowLeft: IconPath,
                    customArrowRight: IconPath,
                  }}
                >
                  {selectedProductList.map((item, index) =>
                    this.getSlideItem(item, index, icidParam)
                  )}
                </Carousel>
              ) : null}
            </Col>
          </Row>
          {showCarousel && this.getCurrentCtaButton()}
        </Col>
      </Row>
    );
  }
}

ModuleQ.defaultProps = {
  shopThisLookLabel: '',
  icidParam: '',
  bgClass: '',
  className: '',
  promoBanner: [],
  hideTabs: false,
  selectedColorProductId: '',
  showRelatedOutfitHeader: null,
  fullBleed: false,
  moduleClassName: '',
  getCompleteTheLookSlot: () => {},
  page: '',
};

ModuleQ.propTypes = {
  bgClass: PropTypes.string,
  shopThisLookLabel: PropTypes.string,
  className: PropTypes.string,
  divTabs: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.object,
      category: PropTypes.object,
      singleCTAButton: PropTypes.object,
    })
  ).isRequired,
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  styliticsProductTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          uniqueId: PropTypes.string.isRequired,
          imageUrl: PropTypes.array.isRequired,
          seo_token: PropTypes.string,
        })
      )
    )
  ).isRequired,
  hideTabs: PropTypes.bool,
  selectedColorProductId: PropTypes.string,
  showRelatedOutfitHeader: PropTypes.func,
  fullBleed: PropTypes.bool,
  moduleClassName: PropTypes.string,
  getCompleteTheLookSlot: PropTypes.func,
  page: PropTypes.string,
  icidParam: PropTypes.string,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

const styledModuleQ = withStyles(errorBoundary(ModuleQ), moduleQStyle);
styledModuleQ.defaultProps = ModuleQ.defaultProps;
export default styledModuleQ;
export { ModuleQ as ModuleQVanilla };
