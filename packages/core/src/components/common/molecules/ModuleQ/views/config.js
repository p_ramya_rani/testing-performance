// 9fbef606107a605d69c0edbcd8029e5d
import theme from '../../../../../../styles/themes/TCP';

const { breakpoints } = theme;
export default {
  CAROUSEL_OPTIONS: {
    autoplay: false,
    arrows: true,
    fade: false,
    speed: 0,
    dots: false,
    dotsClass: 'slick-dots',
    swipe: true,
    slidesToShow: 3.5,
    infinite: false,
    lazyLoad: 'progressive',
    responsive: [
      {
        breakpoint: parseInt(breakpoints.medium, 10) - 1,
        settings: {
          slidesToShow: 2.5,
          slidesToScroll: 1,
          arrows: false,
        },
      },
      {
        breakpoint: parseInt(breakpoints.large, 10) - 1,
        settings: {
          slidesToShow: 4.25,
          arrows: false,
          swipeToSlide: false,
          slidesToScroll: 4.25,
        },
      },
    ],
  },
  imageData: {
    imgConfig: [`t_outfit_img_m`, `t_outfit_img_t`, `t_outfit_img_d`],
    imgConfigVideo: [`t_pdp_vid_m`, `t_pdp_vid_t`, `t_pdp_vid_d`],
  },
};
