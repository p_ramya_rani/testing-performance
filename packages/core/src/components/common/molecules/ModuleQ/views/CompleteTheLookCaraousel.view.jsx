/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { configureInternalNavigationFromCMSUrl, getVideoUrl } from '@tcp/core/src/utils';
import { OUTFIT_LISTING_FORM } from '@tcp/core/src/constants/reducer.constants';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/views/Carousel';
import { Anchor, Image, Row, Col, DamImage } from '../../../atoms';
import config from './config';
import { getIconPath, isClient, getViewportInfo } from '../../../../../utils';
import errorBoundary from '../../../hoc/withErrorBoundary';
import {
  getMapSliceForColorProductId,
  getMapSliceForColor,
  getPrices,
} from '../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import SIZE_CHART_LINK_POSITIONS from '../../ProductAddToBag/container/ProductAddToBag.config';
import ProductAddToBagContainer from '../../ProductAddToBag/container/ProductAddToBag.container';
import withStyles from '../../../hoc/withStyles';
import styles, { modalStyles } from '../styles/CompleteTheLookCarousel.style';
import Modal from '../../Modal';
import theme from '../../../../../../styles/themes/TCP';

const { breakpoints } = theme;

const CAROUSEL_OPTIONS = {
  autoplay: false,
  arrows: true,
  fade: false,
  speed: 0,
  dots: false,
  swipe: true,
  slidesToShow: 3.5,
  infinite: false,
  lazyLoad: true,
  responsive: [
    {
      breakpoint: parseInt(breakpoints.medium, 10) - 1,
      settings: {
        slidesToShow: 3.5,
        slidesToScroll: 1,
        arrows: false,
      },
    },
  ],
};

class CompleteTheLookCarousel extends React.PureComponent {
  constructor(props) {
    super(props);
    const {
      productInfo: { colorFitsSizesMap, generalProductId, offerPrice },
    } = this.props;
    const { currentColorEntry } = this.state;
    const colorName = currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;
    this.formValues = {
      Fit: '',
      Size: '',
      color: colorName,
      Quantity: 1,
    };
    this.state = {
      currentPdpUrl: '',
      currentProductId: -1,
      currentProduct: {},
      currentColorEntry: getMapSliceForColorProductId(colorFitsSizesMap, generalProductId) || {},
      currentGiftCardValue: offerPrice,
      outfitCarouselClass: '',
    };

    this.analyticsTriggered = false;
  }

  componentDidUpdate() {
    const { outfitProducts } = this.props;
    const { currentProductId } = this.state;
    if (currentProductId === -1) {
      this.defaultProduct(outfitProducts);
    }
  }

  getUrlWithHttp = (url) => url && url.replace(/(^\/\/)/, 'https:$1');

  outfitClass = (currentProduct, outfitCarouselClass, item) => {
    const remoteId = item && item.remoteId;
    return item &&
      (currentProduct.generalProductId === remoteId ||
        outfitCarouselClass === remoteId.split('_')[0])
      ? 'selected-product'
      : '';
  };

  getSlideItem = (item, outfitProducts, carouselClass) => {
    const { smallImageUrl } = item;
    const { currentProduct, outfitCarouselClass } = this.state;
    const currentProductDescription =
      !!outfitProducts &&
      outfitProducts.find(
        (currentProductid) => currentProductid.generalProductId === item.remoteId
      );
    const imagesByColor = !!currentProductDescription && currentProductDescription.imagesByColor;
    const url = !!imagesByColor && Object.values(imagesByColor)[0].basicImageUrl;
    const altKeyInfo = item && item.remoteId.split('_')[0];
    const productClassName = !currentProductDescription
      ? 'product-out-of-stock '
      : 'product-in-stock ';
    const { imageData } = config;
    const isVideoUrl = getVideoUrl(url);
    const mainConfig = isVideoUrl ? imageData.imgConfigVideo : imageData.imgConfig;
    const listItemClassName = !currentProductDescription ? 'soldout' : carouselClass;
    const outfitInfo = this.outfitClass(currentProduct, outfitCarouselClass, item);

    return (
      <div className={`${listItemClassName} ${outfitInfo}`}>
        <Anchor
          key={altKeyInfo}
          handleLinkClick={(e) => {
            e.preventDefault();
            this.updatePdpUrl(currentProductDescription, altKeyInfo);
          }}
          noLink
        >
          {!currentProductDescription && <div className="line1" />}
          <div>
            <DamImage
              className={productClassName}
              imgData={{
                alt: altKeyInfo,
                isStyliticImg: !currentProductDescription,
                url: !currentProductDescription ? this.getUrlWithHttp(smallImageUrl) : url,
              }}
              itemProp="contentUrl"
              imgConfigs={mainConfig}
              isProductImage
              isCompleteLookCaraousel
            />
          </div>
        </Anchor>
      </div>
    );
  };

  renderItemList = (
    showCarousel,
    currentCatId,
    IconPath,
    smallItemsList,
    outfitProducts,
    currentImageIndex
  ) => {
    if (!showCarousel || !isClient()) {
      return null;
    }

    const carouselArrow = 'rightarrow';
    return (
      <>
        <div className="smooth-scroll-list">
          <div className="full-screen-wrap">
            <Carousel
              options={CAROUSEL_OPTIONS}
              sliderImageIndex={currentImageIndex}
              className="carousel-item"
              carouselConfig={{
                autoplay: false,
                customArrowLeft: getIconPath(carouselArrow),
                customArrowRight: getIconPath(carouselArrow),
              }}
            >
              {smallItemsList.map((item) => (
                <>{this.getSlideItem(item, outfitProducts, 'smooth-scroll-list-item')}</>
              ))}
            </Carousel>
          </div>
        </div>
      </>
    );
  };

  defaultProduct = (outfitProducts) => {
    if (!!outfitProducts && outfitProducts.length > 0) {
      this.setState({
        currentPdpUrl: outfitProducts[0].pdpUrl,
        currentProductId: outfitProducts[0].generalProductId,
        currentProduct: outfitProducts[0],
      });
    }
  };

  updatePdpUrl = (currentItem, altKeyInfo) => {
    this.setState({
      outfitCarouselClass: altKeyInfo,
    });
    if (!!currentItem && !!currentItem.pdpUrl && !!currentItem.productId) {
      this.setState({
        currentPdpUrl: currentItem.pdpUrl,
        currentProduct: currentItem,
      });
    } else {
      this.setState({
        currentPdpUrl: '',
        currentProduct: '',
      });
    }
  };

  getKeepAlive = () => {
    const { isKeepAliveEnabled } = this.props;
    const { currentColorEntry } = this.state;
    return (
      isKeepAliveEnabled &&
      currentColorEntry &&
      currentColorEntry.miscInfo &&
      currentColorEntry.miscInfo.keepAlive
    );
  };

  onChangeColor = (e, selectedSize, selectedFit, selectedQuantity) => {
    const {
      productInfo: { colorFitsSizesMap },
    } = this.props;
    const { currentGiftCardValue, currentProduct } = this.state;
    this.setState({
      currentColorEntry: getMapSliceForColor(colorFitsSizesMap, e),
      currentGiftCardValue:
        (getMapSliceForColor(colorFitsSizesMap, e) &&
          getMapSliceForColor(colorFitsSizesMap, e).offerPrice) ||
        currentGiftCardValue,
    });
    this.formValues = {
      Fit: selectedFit,
      Size: selectedSize,
      color: e,
      Quantity: selectedQuantity,
    };
    currentProduct.formValue = this.formValues;
    this.setState({
      currentProduct,
    });
  };

  onChangeSize = (selectedColor, selectedSize, selectedFit, selectedQuantity) => {
    const { currentProduct } = this.state;
    this.formValues = {
      Fit: selectedFit,
      Size: selectedSize,
      color: selectedColor,
      Quantity: selectedQuantity,
    };
    currentProduct.formValue = this.formValues;
    this.setState({
      selectedSize,
      currentProduct,
    });
  };

  onFitChange = (selectedFit) => {
    const { currentProduct } = this.state;
    this.formValues.Fit = selectedFit;
    currentProduct.formValue = this.formValues;
    this.setState({
      selectedFit,
      currentProduct,
    });
  };

  getSelectedColorData = (colorFitsSizesMap, selectedColor = {}) => {
    return (
      colorFitsSizesMap &&
      colorFitsSizesMap.filter((colorItem) => {
        const {
          color: { name },
        } = colorItem;
        return (selectedColor.name || selectedColor) === name;
      })
    );
  };

  getPricesForProduct = (outfitProduct, colorName, selectedFit, selectedSize) =>
    outfitProduct && getPrices(outfitProduct, colorName, selectedFit, selectedSize);

  getOutfitProduct = (product) => {
    if (product && product.colorFitsSizesMap) {
      const { selectedSize, selectedFit, selectedColor } = this.state;
      const { colorFitsSizesMap } = product;
      const selectedColorObject = this.getSelectedColorData(colorFitsSizesMap, selectedColor);
      const selectedColorArray = selectedColorObject && selectedColorObject[0];
      const selectedColorId = !selectedColorArray ? '' : selectedColorArray.colorProductId;
      const colorProduct = getMapSliceForColorProductId(colorFitsSizesMap, selectedColorId);
      const colorName = colorProduct && colorProduct.color && colorProduct.color.name;
      return this.getPricesForProduct(product, colorName, selectedFit, selectedSize);
    }
    return {};
  };

  render() {
    const {
      className,
      styliticsProductTabList,
      currentCatId,
      productId,
      productInfo,
      errorOnHandleSubmit,
      plpLabels,
      handleAddToBag,
      isKeepAliveEnabled,
      outOfStockLabels,
      outfitProducts,
      closeModal,
      showAddedToBagCta,
      currencySymbol,
      currencyAttributes,
      isCanada,
      isPlcc,
      addToBagEcom,
      currentState,
      isInternationalShipping,
      showOutfitCarouselAddedCTA,
      addBtnMsg,
      isEnabledOutfitAddedCTA,
    } = this.props;
    const { currentPdpUrl, currentProduct, currentImageIndex } = this.state;

    const { isGiftCard } = productInfo;
    const sizeChartLinkVisibility = !isGiftCard ? SIZE_CHART_LINK_POSITIONS.AFTER_SIZE : null;
    const selectedProductList = styliticsProductTabList[currentCatId] || [];
    const currentProductDescription = selectedProductList.find(
      (currentProductMap) => currentProductMap.id === productId
    );
    const smallItemsList = currentProductDescription.items;
    const showCarousel = selectedProductList.length > 0;
    const IconPath = getIconPath('leftarrow');

    CAROUSEL_OPTIONS.beforeChange = (current, next) => {
      this.setState({ currentImageIndex: next });
    };
    const prices = this.getOutfitProduct(currentProduct);
    const { listPrice = '', offerPrice = '' } = prices;
    const { isMobile, isTablet, isDesktop } = getViewportInfo();
    return (
      <div>
        <Modal
          fixedWidth
          isOpen="true"
          isCompleteLookMobile={isMobile}
          onRequestClose={closeModal}
          heading="COMPLETE THE LOOK"
          overlayClassName="TCPModal__Overlay"
          className={`TCPModal__Content, ${className}`}
          closeIconDataLocator="added-to-bg-close"
          aria={{
            labelledby: 'Added To Bag',
            describedby: 'Added To Bag Modal',
          }}
          data-locator="addedToBag-modal"
          inheritedStyles={modalStyles}
          innerContentClassName="atb-innerContent"
        >
          {(isDesktop || isTablet) && (
            <Image src={currentProductDescription.largeImageUrl} className="large-image" />
          )}

          <Row fullBleed={{ small: true, medium: true }}>
            <Col
              className="completeLook_carousel-wrapper"
              colSize={{
                small: 6,
                medium: 8,
                large: 12,
              }}
            >
              {this.renderItemList(
                showCarousel,
                currentCatId,
                IconPath,
                smallItemsList,
                outfitProducts,
                currentImageIndex
              )}
              {outfitProducts && currentProduct ? (
                <li key={currentProduct.generalProductId}>
                  <ProductAddToBagContainer
                    handleFormSubmit={() => {
                      handleAddToBag(
                        addToBagEcom,
                        currentProduct,
                        currentProduct.generalProductId,
                        currentState,
                        isEnabledOutfitAddedCTA,
                        true
                      );
                    }}
                    currentProduct={currentProduct}
                    plpLabels={plpLabels}
                    isOutfitPage
                    errorOnHandleSubmit={errorOnHandleSubmit}
                    isPickup
                    sizeChartLinkVisibility={sizeChartLinkVisibility}
                    isKeepAliveEnabled={isKeepAliveEnabled}
                    outOfStockLabels={outOfStockLabels}
                    onChangeColor={this.onChangeColor}
                    onChangeSize={this.onChangeSize}
                    onFitChange={this.onFitChange}
                    customFormName={OUTFIT_LISTING_FORM}
                    showAddedToBagCta={showAddedToBagCta}
                    initialFormValues={currentProduct.formValue}
                    addBtnMsg={addBtnMsg}
                    isOutfitCarousel
                    currencySymbol={currencySymbol}
                    currencyAttributes={currencyAttributes}
                    listPrice={listPrice}
                    offerPrice={offerPrice}
                    isCanada={isCanada}
                    productDetailsToPath={configureInternalNavigationFromCMSUrl(currentPdpUrl)}
                    showOutfitCarouselAddedCTA={showOutfitCarouselAddedCTA}
                    isPlcc={isPlcc}
                    isInternationalShipping={isInternationalShipping}
                    closeModal={closeModal}
                  />
                </li>
              ) : (
                <div className="out-of-stock">{outOfStockLabels.outOfStockCaps}</div>
              )}
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
}

CompleteTheLookCarousel.defaultProps = {
  className: '',
  currentCatId: '',
  shopThisLookLabel: '',
  productId: '',
};
CompleteTheLookCarousel.propTypes = {
  closeModal: PropTypes.func.isRequired,
  className: PropTypes.string,
  currentCatId: PropTypes.string,
  productId: PropTypes.string,
  currencySymbol: PropTypes.string,
  isPlcc: PropTypes.bool.isRequired,
  currencyAttributes: PropTypes.shape({}).isRequired,
  isCanada: PropTypes.bool.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  styliticsProductTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          imageUrl: PropTypes.array.isRequired,
          seo_token: PropTypes.string,
        })
      )
    )
  ).isRequired,
  productInfo: PropTypes.shape({}).isRequired,
  errorOnHandleSubmit: PropTypes.string,
  plpLabels: PropTypes.shape({}).isRequired,
  isKeepAliveEnabled: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  addToBagEcom: PropTypes.func.isRequired,
  handleAddToBag: PropTypes.func.isRequired,
  currentState: PropTypes.shape({}).isRequired,
  showAddedToBagCta: PropTypes.shape({}).isRequired,
  outfitProducts: PropTypes.shape({}).isRequired,
  addBtnMsg: PropTypes.string.isRequired,
  showOutfitCarouselAddedCTA: PropTypes.bool,
  isEnabledOutfitAddedCTA: PropTypes.bool.isRequired,
};

CompleteTheLookCarousel.defaultProps = {
  showOutfitCarouselAddedCTA: false,
};

const styledCompleteTheLook = withStyles(errorBoundary(CompleteTheLookCarousel), styles);
styledCompleteTheLook.defaultProps = CompleteTheLookCarousel.defaultProps;
export default styledCompleteTheLook;
