// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React, { useState, useEffect } from 'react';
import { FlatList } from 'react-native-gesture-handler';
import PropTypes from 'prop-types';

import Button from '../../../atoms/Button/views/Button';
import Skeleton from '../../../atoms/Skeleton/container';
import CarouselSlideView from '../../../atoms/CarouselSlide/views/CarouselSlide.native';
import { getLocator } from '../../../../../utils/index.native';
import config from '../CompleteTheLook.config';
import {
  Container,
  ImageSlidesWrapper,
  ButtonContainer,
  StyledProductTabList,
  CompleteTheLookRedesign,
  CompleteTheLookTitleRedesign,
} from '../styles/ModuleQ.style.native';
import CustomButton from '../../../atoms/ButtonRedesign';
import ModuleQConstant from '../ModuleQ.constant';

const rightArrowImage = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icons-medium-left.png');

const { TOTAL_IMAGES } = config;

function getDataStatus(selectedProductList, currentCatId) {
  let dataStatus = true;
  if (selectedProductList && selectedProductList.completed) {
    dataStatus = selectedProductList.completed[currentCatId];
  }
  return dataStatus;
}

const CompleteTheLook = (props) => {
  const [selectedCategoryId, setSelectedCategoryId] = useState(null);
  const [selectedTabItem, setSelectedTabItem] = useState(null);
  const {
    styliticsProductTabList,
    navigation,
    shopThisLookLabel,
    shopThisLookLabelNewDesign,
    selectedColorProductId,
    isRelatedOutfit,
    showRelatedOutfitHeader,
    triggerStylyticsAnalytics,
    showSmallImage,
    isNewReDesignCTL,
    onRequestClose,
    fromPage,
    brand,
  } = props;

  let selectedProductList;
  useEffect(() => {
    if (
      showRelatedOutfitHeader &&
      selectedCategoryId &&
      selectedProductList &&
      selectedProductList.length
    ) {
      showRelatedOutfitHeader(true);
    } else if (
      showRelatedOutfitHeader &&
      selectedCategoryId &&
      selectedProductList &&
      selectedProductList.length === 0
    ) {
      showRelatedOutfitHeader(false);
    }
  }, [selectedCategoryId, styliticsProductTabList]);

  const { singleCTAButton: selectedSingleCTAButton } = selectedTabItem || {};
  selectedProductList = styliticsProductTabList[selectedCategoryId] || [];
  selectedProductList = selectedProductList.slice(0, TOTAL_IMAGES);

  const selectedProductCarouselList = selectedProductList.map((item, index) => {
    return { ...item, productItemIndex: index };
  });

  const onCompleteTheLook = (productItem) => {
    const { RECOMMENDATION, COMPLETETHELOOK } = ModuleQConstant;
    const { subItemsId, id } = productItem;
    const isSBP = navigation.getParam('isSBP');
    triggerStylyticsAnalytics(selectedProductList);
    onRequestClose();
    navigation.navigate('OutfitDetail', {
      title: COMPLETETHELOOK,
      outfitId: id,
      isCTL: true,
      vendorColorProductIdsList: subItemsId,
      viaModule: RECOMMENDATION,
      isSBP,
      fromPage,
      selectedProductId: selectedCategoryId,
      brand,
    });
  };

  const renderCarouselSlide = (slideProps) => {
    const { item, index } = slideProps;
    const { COMPLETETHELOOK } = ModuleQConstant;
    return isNewReDesignCTL ? (
      <CompleteTheLookRedesign index={index}>
        <CarouselSlideView
          productItem={item}
          navigation={navigation}
          completeTheLookMainTile={shopThisLookLabel}
          showSmallImage={showSmallImage}
          selectedProductId={selectedCategoryId}
          showShadow={!isNewReDesignCTL}
          onRequestClose={onRequestClose}
          ctlTitle={COMPLETETHELOOK}
          fromPage={fromPage}
          isCTL={true}
          onPress={() => onCompleteTheLook(item)}
        />
        <CompleteTheLookTitleRedesign>
          <CustomButton
            text={shopThisLookLabelNewDesign}
            fontSize="fs14"
            onPress={() => onCompleteTheLook(item)}
            fontFamily="secondary"
            fontWeight="regular"
            imageRightSize={16}
            imageRightName={rightArrowImage}
            imageRightShow
            imageAlignRight
          />
        </CompleteTheLookTitleRedesign>
      </CompleteTheLookRedesign>
    ) : (
      <CarouselSlideView
        productItem={item}
        navigation={navigation}
        completeTheLookMainTile={shopThisLookLabel}
        showSmallImage={showSmallImage}
        selectedProductId={selectedCategoryId}
        ctlTitle={COMPLETETHELOOK}
        isCTL={true}
        onPress={() => onCompleteTheLook(item)}
      />
    );
  };

  const onProductTabChange = (categoryId, tabItem) => {
    setSelectedCategoryId(categoryId);
    setSelectedTabItem(tabItem);
  };
  const dataStatus = getDataStatus(styliticsProductTabList, selectedCategoryId);

  return (
    <Container isRelatedOutfit={isRelatedOutfit}>
      <StyledProductTabList
        onProductTabChange={onProductTabChange}
        navigation={navigation}
        selectedColorProductId={selectedColorProductId}
        testID={getLocator('completeLook_cta_link')}
        isRelatedOutfit={isRelatedOutfit}
        isPDPPageCompleteTheLook
      />

      {dataStatus ? (
        <Skeleton
          row={1}
          col={3}
          width={250}
          height={300}
          rowProps={{ justifyContent: 'center', marginTop: '20px' }}
          showArrows
        />
      ) : null}

      <ImageSlidesWrapper>
        {selectedProductList.length ? (
          <FlatList
            data={selectedProductCarouselList}
            renderItem={renderCarouselSlide}
            refreshing={false}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(_, index) => index.toString()}
            directionalLockEnabled
          />
        ) : null}
      </ImageSlidesWrapper>

      {selectedSingleCTAButton ? (
        <ButtonContainer>
          <Button
            width="225px"
            text={selectedSingleCTAButton.text}
            url={selectedSingleCTAButton.url}
            navigation={navigation}
            testID={getLocator('completeLook_cta_btn')}
          />
        </ButtonContainer>
      ) : null}
    </Container>
  );
};

CompleteTheLook.defaultProps = {
  selectedColorProductId: '',
  isRelatedOutfit: false,
  shopThisLookLabel: '',
  showRelatedOutfitHeader: () => {},
  triggerStylyticsAnalytics: () => {},
  showSmallImage: false,
  isNewReDesignCTL: false,
  shopThisLookLabelNewDesign: '',
  onRequestClose: () => {},
  fromPage: '',
  brand: '',
};

CompleteTheLook.propTypes = {
  styliticsProductTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          imageUrl: PropTypes.array.isRequired,
          items: PropTypes.array,
          largeImageUrl: PropTypes.string,
          pdpUrl: PropTypes.string,
        })
      )
    )
  ).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  selectedColorProductId: PropTypes.string,
  isRelatedOutfit: PropTypes.bool,
  shopThisLookLabel: PropTypes.string,
  showRelatedOutfitHeader: PropTypes.func,
  triggerStylyticsAnalytics: PropTypes.func,
  showSmallImage: PropTypes.bool,
  isNewReDesignCTL: PropTypes.bool,
  shopThisLookLabelNewDesign: PropTypes.string,
  onRequestClose: PropTypes.func,
  fromPage: PropTypes.string,
  brand: PropTypes.string,
};

export default CompleteTheLook;
export { CompleteTheLook as CompleteTheLookVanilla };
