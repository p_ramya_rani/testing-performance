// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/views/Carousel';
import Grid from '@tcp/core/src/components/common/molecules/Grid';
import { Anchor, Col, Image, Row, BodyCopy } from '../../../atoms';
import errorBoundary from '../../../hoc/withErrorBoundary';
import withStyles from '../../../hoc/withStyles';
import StyliticsProductTabList from '../../../organisms/StyliticsProductTabList';
import {
  getIconPath,
  getLocator,
  mergeUrlQueryParams,
  getViewportInfo,
  isClient,
} from '../../../../../utils';
import { getPrices } from '../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import CompleteTheLookCaraousel from './CompleteTheLookCaraousel.view';
import styles from '../styles/CompleteTheLook.style';
import theme from '../../../../../../styles/themes/TCP';
import constant from '../ModuleQ.constant';

const { breakpoints } = theme;
/**
 * @class CompleteTheLook - will display Complete The Look content

 */
class CompleteTheLook extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentCatId: '',
      showOutfitCarousel: false,
      productId: '',
    };
  }

  showOutfitCarouselModal = (outfitId) => {
    const { getOutfit, styliticsProductTabList, setOutfitCarouselState } = this.props;
    const { currentCatId } = this.state;
    const selectedProductList = styliticsProductTabList[currentCatId] || [];
    const currentProductDescription = selectedProductList.find(
      (currentProduct) => currentProduct.id === outfitId
    );
    const vendorColorProductIdsList = currentProductDescription.subItemsId;
    getOutfit({ outfitId, vendorColorProductIdsList, fromOutFitPage: true });
    setOutfitCarouselState({ outfitCarouselModal: true });
    this.setState({
      showOutfitCarousel: true,
      productId: outfitId,
    });
  };

  /* Return the offset object for main wrapper */
  getWrapperOffset = (fullBleed) => {
    return {
      small: 0,
      medium: 0,
      large: fullBleed ? 0.5 : 2,
    };
  };

  onProductTabChange = (catId) => {
    this.setState({ currentCatId: catId });
  };

  /** This method is to add protocol to image url
   * since image is coming from unbox without protocol
   * and it breaks on local without protocol
   */
  getUrlWithHttp = (url) => url && url.replace(/(^\/\/)/, 'https:$1');

  /**
   * This function is being called to render carousel items.
   * @param {object} item image object for the carousel
   * @param {integer} index Index for the carousel item
   */

  getSlideItem = (item, index, icidParam) => {
    const { id, largeImageUrl, pdpUrl } = item;
    const { shopThisLookLabel, page, isViewThisLookLabelEnabled, pdpLabels } = this.props;
    const { viewThisLook } = pdpLabels;
    const { RECOMMENDATION } = constant;
    const outfitParams = pdpUrl && pdpUrl.split('/');
    const outfitUrl =
      outfitParams &&
      outfitParams.length > 1 &&
      `/outfit?outfitId=${outfitParams[outfitParams.length - 2]}&vendorColorProductIdsList=${
        outfitParams[outfitParams.length - 1]
      }&viaModule=${RECOMMENDATION}&viaPage=${page}`;
    const pdpAsPathWithTrackingParams = mergeUrlQueryParams(pdpUrl, icidParam);

    return (
      <div>
        <Anchor
          key={id}
          to={outfitUrl}
          asPath={pdpAsPathWithTrackingParams}
          dataLocator={`${getLocator('completeLook_product_image')}${index}`}
        >
          <div
            className={`${
              isViewThisLookLabelEnabled ? 'view-look-text-block image-wrapper' : 'image-wrapper'
            }`}
          >
            {isViewThisLookLabelEnabled ? (
              <BodyCopy
                fontSize="fs12"
                fontFamily="Nunito"
                fontWeight="bold"
                textAlign="center"
                className="view-look-text"
              >
                {viewThisLook}
              </BodyCopy>
            ) : null}
            <Image
              alt={id}
              src={this.getUrlWithHttp(largeImageUrl)}
              className={`${isViewThisLookLabelEnabled ? 'view-look-img' : ''}`}
            />
            {isViewThisLookLabelEnabled ? null : (
              <Anchor withCaret noLink centered className="shop-look-text">
                <BodyCopy fontFamily="secondary" fontSize="fs18">
                  {shopThisLookLabel}
                </BodyCopy>
              </Anchor>
            )}
          </div>
        </Anchor>
      </div>
    );
  };

  renderItemList = (
    showCarousel,
    currentCatId,
    CAROUSEL_OPTIONS,
    IconPath,
    selectedProductList,
    icidParam
  ) => {
    if (!showCarousel || !isClient()) {
      return null;
    }
    const isMobile = isClient() ? getViewportInfo().isMobile : null;
    if (isMobile) {
      return (
        <div className="smooth-scroll-list">
          {selectedProductList.map((item, index) => (
            <div className="smooth-scroll-list-item slick-slide">
              <div>{this.getSlideItem(item, index, icidParam)}</div>
            </div>
          ))}
        </div>
      );
    }
    return (
      <Carousel
        key={currentCatId.toString()}
        options={CAROUSEL_OPTIONS}
        carouselConfig={{
          autoplay: false,
          variation: 'big-arrows',
          customArrowLeft: IconPath,
          customArrowRight: IconPath,
        }}
      >
        {selectedProductList.map((item, index) => this.getSlideItem(item, index, icidParam))}
      </Carousel>
    );
  };

  closeDrawer = (e) => {
    const { setOutfitCarouselState } = this.props;
    if (e) {
      e.preventDefault();
    }
    setOutfitCarouselState({ outfitCarouselModal: false });
    this.setState({
      showOutfitCarousel: false,
    });
  };

  getPricesForProduct = (outfitProduct, colorProduct, selectedFit, selectedSize) =>
    outfitProduct && getPrices(outfitProduct, colorProduct.color.name, selectedFit, selectedSize);

  getSelectedColorData = (colorFitsSizesMap, selectedColor = {}) => {
    return (
      colorFitsSizesMap &&
      colorFitsSizesMap.filter((colorItem) => {
        const {
          color: { name },
        } = colorItem;
        return (selectedColor.name || selectedColor) === name;
      })
    );
  };

  render() {
    const {
      styliticsProductTabList,
      selectedColorProductId,
      showRelatedOutfitHeader,
      isRelatedOutfit,
      moduleClassName,
      className,
      icidParam,
      triggerStylyticsAnalytics,
      outfitProducts,
      productInfo,
      addToBagError,
      plpLabels,
      isKeepAliveEnabled,
      outOfStockLabels,
      handleAddToBag,
      addToBagEcom,
      currentState,
      showAddedToBagCta,
      priceCurrency,
      currencyAttributes,
      isCanada,
      isPlcc,
      isInternationalShipping,
      isEnabledOutfitAddedCTA,
      showOutfitCarouselAddedCTA,
      setOutfitCarouselState,
      addBtnMsg,
      isNewPDPEnabled,
    } = this.props;

    const { currentCatId, showOutfitCarousel, productId } = this.state;
    const selectedProductList = styliticsProductTabList[currentCatId] || [];
    const showCarousel = selectedProductList.length > 0;
    const IconPath = getIconPath('carousel-big-carrot');
    const slides = selectedProductList.length > 3 ? 4 : selectedProductList.length;
    const slidesMobile = selectedProductList.length > 1 ? 2 : selectedProductList.length;
    const CAROUSEL_OPTIONS = {
      autoplay: false,
      arrows: true,
      centerMode: false,
      centerPadding: '0px',
      fade: false,
      speed: 200,
      swipeToSlide: true,
      swipe: true,
      infinite: true,
      slidesToShow: slides,
      responsive: [
        {
          breakpoint: parseInt(breakpoints.medium, 10) - 1,
          settings: {
            slidesToShow: slidesMobile,
            slidesToScroll: 2,
            arrows: false,
            swipeToSlide: true,
            infinite: true,
            centerMode: true,
            centerPadding: '30px',
          },
        },
        {
          breakpoint: parseInt(breakpoints.large, 10) - 1,
          settings: {
            slidesToShow: isNewPDPEnabled ? 3.5 : slidesMobile,
            slidesToScroll: 1,
            centerPadding: '197px',
            arrows: false,
            swipeToSlide: true,
            infinite: !isNewPDPEnabled,
          },
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: isNewPDPEnabled ? 3.5 : slidesMobile,
            slidesToScroll: 1,
            centerPadding: '197px',
            arrows: false,
            swipeToSlide: true,
            infinite: !isNewPDPEnabled,
          },
        },
        {
          breakpoint: 1195,
          settings: {
            slidesToShow: isNewPDPEnabled ? 4.5 : slidesMobile,
            slidesToScroll: 1,
            centerPadding: '197px',
            arrows: false,
            swipeToSlide: true,
            infinite: !isNewPDPEnabled,
          },
        },
      ],
    };
    if (showCarousel && showRelatedOutfitHeader) {
      showRelatedOutfitHeader(true);
      triggerStylyticsAnalytics(selectedProductList);
    }

    return (
      <Grid className={`${className} ${moduleClassName}`} isNewPDPEnabled={isNewPDPEnabled}>
        <Row centered>
          <div>
            <StyliticsProductTabList
              onProductTabChange={this.onProductTabChange}
              selectedColorProductId={selectedColorProductId}
              dataLocator={getLocator('completeLook_cta_link')}
              isRelatedOutfit={isRelatedOutfit}
              isPDPPageCompleteTheLook
              isNewPDPEnabled
            />
          </div>
        </Row>
        <Row fullBleed={{ small: true, medium: true }}>
          <Col
            className="completeLook_carousel-wrapper"
            colSize={{
              small: 6,
              medium: 8,
              large: 12,
            }}
          >
            {this.renderItemList(
              showCarousel,
              currentCatId,
              CAROUSEL_OPTIONS,
              IconPath,
              selectedProductList,
              icidParam
            )}
          </Col>
        </Row>

        {showOutfitCarousel && (
          <CompleteTheLookCaraousel
            className="class"
            currentCatId={currentCatId}
            styliticsProductTabList={styliticsProductTabList}
            productId={productId}
            outfitProducts={outfitProducts}
            closeModal={this.closeDrawer}
            productInfo={productInfo}
            handleAddToBag={handleAddToBag}
            currentProduct={outfitProducts}
            plpLabels={plpLabels}
            addToBagEcom={addToBagEcom}
            currentState={currentState}
            isOutfitPage
            errorOnHandleSubmit={addToBagError}
            isPickup
            isKeepAliveEnabled={isKeepAliveEnabled}
            outOfStockLabels={outOfStockLabels}
            showAddedToBagCta={showAddedToBagCta}
            showOutfitCarouselAddedCTA={showOutfitCarouselAddedCTA}
            currencySymbol={priceCurrency}
            currencyAttributes={currencyAttributes}
            isCanada={isCanada}
            isPlcc={isPlcc}
            isInternationalShipping={isInternationalShipping}
            addBtnMsg={addBtnMsg}
            isOutfitCarousel
            isEnabledOutfitAddedCTA={isEnabledOutfitAddedCTA}
            setOutfitCarouselState={setOutfitCarouselState}
            isNewPDPEnabled={isNewPDPEnabled}
          />
        )}
      </Grid>
    );
  }
}

CompleteTheLook.defaultProps = {
  className: '',
  shopThisLookLabel: '',
  selectedColorProductId: '',
  showRelatedOutfitHeader: null,
  isRelatedOutfit: false,
  fullBleed: false,
  moduleClassName: '',
  icidParam: '',
  page: '',
  isViewThisLookLabelEnabled: false,
  pdpLabels: {},
  isNewPDPEnabled: false,
};

CompleteTheLook.propTypes = {
  styliticsProductTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          uniqueId: PropTypes.string.isRequired,
          imageUrl: PropTypes.array.isRequired,
          seo_token: PropTypes.string,
        })
      )
    )
  ).isRequired,
  selectedColorProductId: PropTypes.string,
  outfitProducts: PropTypes.shape({}),
  showRelatedOutfitHeader: PropTypes.func,
  priceCurrency: PropTypes.string,
  isPlcc: PropTypes.bool.isRequired,
  currencyAttributes: PropTypes.shape({}).isRequired,
  isCanada: PropTypes.bool.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  isRelatedOutfit: PropTypes.bool,
  moduleClassName: PropTypes.string,
  icidParam: PropTypes.string,
  className: PropTypes.string,
  shopThisLookLabel: PropTypes.string,
  page: PropTypes.string,
  triggerStylyticsAnalytics: PropTypes.func.isRequired,
  productInfo: PropTypes.shape({}).isRequired,
  addToBagError: PropTypes.string,
  productDetails: PropTypes.shape({}).isRequired,
  plpLabels: PropTypes.shape({}).isRequired,
  isKeepAliveEnabled: PropTypes.bool.isRequired,
  alternateSizes: PropTypes.shape({}).isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  sizeChartDetails: PropTypes.shape({}).isRequired,
  addToBagEcom: PropTypes.func.isRequired,
  getOutfit: PropTypes.func.isRequired,
  handleAddToBag: PropTypes.func.isRequired,
  currentState: PropTypes.shape({}).isRequired,
  setOutfitCarouselState: PropTypes.func.isRequired,
  showAddedToBagCta: PropTypes.shape({}).isRequired,
  showOutfitCarouselAddedCTA: PropTypes.shape({}).isRequired,
  addBtnMsg: PropTypes.string.isRequired,
  isEnabledOutfitAddedCTA: PropTypes.bool.isRequired,
  isViewThisLookLabelEnabled: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  isNewPDPEnabled: PropTypes.bool,
};

const styledCompleteTheLook = withStyles(errorBoundary(CompleteTheLook), styles);
styledCompleteTheLook.defaultProps = CompleteTheLook.defaultProps;
export default styledCompleteTheLook;
export { CompleteTheLook as CompleteTheLookVanilla };
