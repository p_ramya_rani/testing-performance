// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { LAZYLOAD_HOST_NAME, isAndroid } from '@tcp/core/src/utils';
import colors from '@tcp/core/styles/themes/TCP/colors';

import { Button, Anchor, BodyCopy, Skeleton } from '../../../atoms';
import {
  getLocator,
  getScreenWidth,
  styleOverrideEngine,
  getMediaBorderRadius,
} from '../../../../../utils/index.native';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';

import { Carousel } from '../..';
import config from '../ModuleQ.config';
import constant from '../ModuleQ.constant';

import {
  Container,
  ImageItemWrapper,
  ImageSlidesWrapper,
  ImageSlideWrapper,
  OutfitItemsWrapper,
  ButtonContainer,
  StyledImage,
  OutfitItemTileWrapper,
  OutfitMainImageWrapper,
  OutfitMainTileWrapper,
  PromoContainer,
  HeaderContainer,
  SecondHeaderContainer,
  MessageContainer,
  Wrapper,
  StyledProductTabList,
  RestOutfitItemCountWrapper,
} from '../styles/ModuleQ.style.native';

import PromoBanner from '../../PromoBanner';
import LinkText from '../../LinkText';

const MODULE_WIDTH = getScreenWidth();
const style = { backgroundColor: colors.WHITE };
const { TOTAL_IMAGES, CAROUSEL_OPTIONS } = config;
const { RECOMMENDATION } = constant;
const {
  PRODUCT_IMAGE_WIDTH,
  PRODUCT_IMAGE_HEIGHT,
  MODULE_HEIGHT,
  OUTFIT_ITEM_IMAGE_HEIGHT,
  OUTFIT_ITEM_IMAGE_WIDTH,
  LOOP_CLONES_PER_SIDE,
  INACTIVE_SLIDE_SCALE,
  INACTIVE_SLIDE_OPACITY,
  ITEM_WIDTH,
} = CAROUSEL_OPTIONS.APP;
const getUrlWithHttp = (url) => url.replace(/(^\/\/)/, 'https:$1');

/**
 * This function is being called through snap carousel render function.
 * @param {Object} productItem SnapCarousel data item
 * @param {Object} navigation Navigation object required for children
 * @param {String} moduleQMainTile label required for all slides main tile.
 */
function getCarouselSlide({
  productItem,
  navigation,
  moduleQMainTile,
  ignoreLazyLoadImage,
  hostLazyLoad,
  showImage,
  icidParam,
  selectedProductId,
}) {
  getCarouselSlide.propTypes = {
    productItem: PropTypes.shape({}).isRequired,
    navigation: PropTypes.shape({}).isRequired,
    moduleQMainTile: PropTypes.string.isRequired,
    ignoreLazyLoadImage: PropTypes.bool.isRequired,
    hostLazyLoad: PropTypes.string.isRequired,
    showImage: PropTypes.bool.isRequired,
    icidParam: PropTypes.string.isRequired,
    selectedProductId: PropTypes.string.isRequired,
  };
  const { imageUrl, items, subItemsId, productItemIndex, id } = productItem;
  const totalOutfitItemsToShow = 2;
  const outfitItemsToShow = items.slice(0, totalOutfitItemsToShow);
  return (
    <ImageSlideWrapper>
      <ImageItemWrapper>
        <Anchor
          navigation={navigation}
          testID={`${getLocator('moduleQ_product_image')}${productItemIndex}`}
          onPress={() =>
            navigation.navigate('OutfitDetail', {
              title: 'COMPLETE THE LOOK',
              outfitId: id,
              vendorColorProductIdsList: subItemsId,
              viaModule: RECOMMENDATION,
              internalCampaign: icidParam,
              selectedProductId,
            })
          }
        >
          <OutfitMainTileWrapper>
            <OutfitMainImageWrapper height={PRODUCT_IMAGE_HEIGHT} width={PRODUCT_IMAGE_WIDTH}>
              <StyledImage
                alt={moduleQMainTile}
                host={ignoreLazyLoadImage ? '' : hostLazyLoad || LAZYLOAD_HOST_NAME.HOME}
                url={showImage && getUrlWithHttp(imageUrl)}
                height={PRODUCT_IMAGE_HEIGHT}
                width={PRODUCT_IMAGE_WIDTH}
                isFastImage
                resizeMode="stretch"
              />
            </OutfitMainImageWrapper>
            <BodyCopy
              text={`${moduleQMainTile}  ›`}
              fontSize="fs12"
              fontFamily="secondary"
              textAlign="center"
              textColor="gray.900"
            />
          </OutfitMainTileWrapper>
          <OutfitItemsWrapper>
            {outfitItemsToShow.map((item) => {
              const { name: alt, remoteId, smallImageUrl } = item;

              return (
                <OutfitItemTileWrapper
                  height={OUTFIT_ITEM_IMAGE_HEIGHT}
                  width={OUTFIT_ITEM_IMAGE_WIDTH}
                >
                  <StyledImage
                    key={remoteId}
                    alt={alt}
                    host={ignoreLazyLoadImage ? '' : hostLazyLoad || LAZYLOAD_HOST_NAME.HOME}
                    url={showImage && getUrlWithHttp(smallImageUrl)}
                    height={OUTFIT_ITEM_IMAGE_HEIGHT}
                    width={OUTFIT_ITEM_IMAGE_WIDTH}
                    isFastImage
                    resizeMode="contain"
                  />
                </OutfitItemTileWrapper>
              );
            })}
            <OutfitItemTileWrapper>
              <RestOutfitItemCountWrapper
                width={OUTFIT_ITEM_IMAGE_WIDTH}
                height={OUTFIT_ITEM_IMAGE_HEIGHT}
              >
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs22"
                  textAlign="center"
                  fontWeight="extrabold"
                  text={`+${items.length - totalOutfitItemsToShow}`}
                />
              </RestOutfitItemCountWrapper>
            </OutfitItemTileWrapper>
          </OutfitItemsWrapper>
        </Anchor>
      </ImageItemWrapper>
    </ImageSlideWrapper>
  );
}

function getDataStatus(selectedProductList, currentCatId) {
  let dataStatus = true;
  if (selectedProductList && selectedProductList.completed) {
    dataStatus = selectedProductList.completed[currentCatId];
  }
  return dataStatus;
}

const showActiveImages = (index, currentIndexCarousel) =>
  isAndroid() ? index >= currentIndexCarousel + 1 && index <= currentIndexCarousel + 3 : true;

const getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};

const getContainerStyle = (isHpNewLayoutEnabled) =>
  isHpNewLayoutEnabled
    ? {
        ...HP_NEW_LAYOUT.MODULE_BOX_SHADOW,
      }
    : {};

const getMediaStyle = (isHpNewLayoutEnabled, mediaBorderRadiusOverride) =>
  isHpNewLayoutEnabled
    ? {
        ...mediaBorderRadiusOverride,
      }
    : {};

const getComputedWidth = (isHpNewLayoutEnabled) =>
  isHpNewLayoutEnabled ? MODULE_WIDTH - 2 * HP_NEW_LAYOUT.BODY_PADDING : MODULE_WIDTH;

// eslint-disable-next-line complexity
const ModuleQ = (props) => {
  const [selectedCategoryId, setSelectedCategoryId] = useState(null);
  const [selectedTabItem, setSelectedTabItem] = useState(null);
  const [currentIndexCarousel, setCurrentIndexCarousel] = useState(0);

  const {
    styliticsProductTabList,
    navigation,
    headerText,
    promoBanner,
    divTabs,
    autoplayInterval,
    shopThisLookLabel,
    hostLazyLoad,
    ignoreLazyLoadImage,
    hideTabs,
    selectedColorProductId,
    showRelatedOutfitHeader,
    moduleClassName,
    icidParam,
    isHpNewLayoutEnabled,
  } = props;

  const { singleCTAButton: selectedSingleCTAButton } = selectedTabItem || {};
  let selectedProductList = styliticsProductTabList[selectedCategoryId] || [];
  selectedProductList = selectedProductList.slice(0, TOTAL_IMAGES);

  const showData = hideTabs ? selectedProductList && selectedProductList.length : true;
  /* Add productItemIndex for the testIDs */
  const selectedProductCarouselList = selectedProductList.map((item, index) => {
    return { ...item, productItemIndex: index };
  });

  if (selectedProductList && selectedProductList.length && showRelatedOutfitHeader) {
    showRelatedOutfitHeader(true);
  }

  const renderCarouselSlide = (slideProps) => {
    const { item, index } = slideProps;
    const showImage = showActiveImages(index, currentIndexCarousel);

    return getCarouselSlide({
      productItem: item,
      navigation,
      moduleQMainTile: shopThisLookLabel,
      ignoreLazyLoadImage,
      hostLazyLoad,
      showImage,
      icidParam,
      selectedProductId: selectedCategoryId,
    });
  };

  const onProductTabChange = (categoryId, tabItem) => {
    setSelectedCategoryId(categoryId);
    setSelectedTabItem(tabItem);
  };
  const dataStatus = getDataStatus(styliticsProductTabList, selectedCategoryId);
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleQ');
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
  const backgroundColor = styleOverrides.bground || {};
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewLayoutEnabled,
    styleOverrides
  );
  const containerStyle = getContainerStyle(isHpNewLayoutEnabled);

  return (
    <Container
      style={{
        ...containerStyle,
        ...getMediaStyle(isHpNewLayoutEnabled, mediaBorderRadiusOverride),
        ...backgroundColor,
      }}
      bgClass="pink-bg"
    >
      {!hideTabs ? (
        <MessageContainer>
          {headerText && (
            <Wrapper>
              {headerText[0] && (
                <HeaderContainer>
                  <LinkText
                    navigation={navigation}
                    fontFamily="primary"
                    fontSize="fs32"
                    fontWeight="semibold"
                    headerText={[headerText[0]]}
                    testID={getLocator('moduleQ_header_text_0')}
                    color="pink.500"
                    useStyle
                    headerStyle={[headerStyle[0]]}
                  />
                </HeaderContainer>
              )}
              {headerText[1] && (
                <SecondHeaderContainer>
                  <LinkText
                    navigation={navigation}
                    fontFamily="secondary"
                    fontSize="fs14"
                    textAlign="center"
                    headerText={[headerText[1]]}
                    testID={getLocator('moduleQ_header_text_1')}
                    renderComponentInNewLine
                    useStyle
                    headerStyle={[headerStyle[1]]}
                  />
                </SecondHeaderContainer>
              )}
            </Wrapper>
          )}
          {promoBanner && (
            <PromoContainer>
              <PromoBanner
                testID={getLocator('moduleQ_promobanner_text')}
                promoBanner={promoBanner}
                navigation={navigation}
                promoStyle={styleOverrides.promo}
              />
            </PromoContainer>
          )}
        </MessageContainer>
      ) : null}
      <StyledProductTabList
        showData={showData}
        onProductTabChange={onProductTabChange}
        tabItems={divTabs}
        navigation={navigation}
        selectedColorProductId={selectedColorProductId}
        testID={getLocator('moduleQ_cta_link')}
      />

      {dataStatus ? (
        <Skeleton
          row={1}
          col={3}
          width={250}
          height={300}
          rowProps={{ justifyContent: 'center', marginTop: '20px' }}
          showArrows
        />
      ) : null}

      <ImageSlidesWrapper hideTabs={hideTabs} divTabs={divTabs}>
        {selectedProductList.length ? (
          <Carousel
            data={selectedProductCarouselList}
            renderItem={renderCarouselSlide}
            height={MODULE_HEIGHT}
            options={{
              loopClonesPerSide: LOOP_CLONES_PER_SIDE,
              inactiveSlideScale: INACTIVE_SLIDE_SCALE,
              inactiveSlideOpacity: INACTIVE_SLIDE_OPACITY,
              sliderWidth: getComputedWidth(isHpNewLayoutEnabled),
              itemWidth: ITEM_WIDTH,
              autoplay: CAROUSEL_OPTIONS.autoplay,
            }}
            autoplayInterval={autoplayInterval * 1000}
            paginationProps={{
              containerStyle: { paddingVertical: 5 },
            }}
            onSnapToItem={setCurrentIndexCarousel}
            showDots
            isModule
          />
        ) : null}
      </ImageSlidesWrapper>

      {selectedSingleCTAButton ? (
        <ButtonContainer>
          <Button
            style={style}
            width="225px"
            text={selectedSingleCTAButton.text}
            url={selectedSingleCTAButton.url}
            navigation={navigation}
            testID={getLocator('moduleQ_cta_btn')}
          />
        </ButtonContainer>
      ) : null}
    </Container>
  );
};

ModuleQ.defaultProps = {
  promoBanner: null,
  autoplayInterval: 1,
  shopThisLookLabel: '',
  ignoreLazyLoadImage: false,
  hostLazyLoad: '',
  hideTabs: false,
  selectedColorProductId: '',
  headerText: [],
  showRelatedOutfitHeader: null,
  moduleClassName: '',
  icidParam: '',
};

ModuleQ.propTypes = {
  autoplayInterval: PropTypes.number,
  shopThisLookLabel: PropTypes.string,
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  styliticsProductTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          imageUrl: PropTypes.array.isRequired,
          items: PropTypes.array,
          largeImageUrl: PropTypes.string,
          pdpUrl: PropTypes.string,
        })
      )
    )
  ).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  divTabs: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.object,
      category: PropTypes.object,
      singleCTAButton: PropTypes.object,
    })
  ).isRequired,
  ignoreLazyLoadImage: PropTypes.bool,
  hostLazyLoad: PropTypes.string,
  hideTabs: PropTypes.bool,
  selectedColorProductId: PropTypes.string,
  showRelatedOutfitHeader: PropTypes.func,
  moduleClassName: PropTypes.string,
  icidParam: PropTypes.string,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleQ;
export { ModuleQ as ModuleQVanilla };
