// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '../../../../../utils';
/* stylelint-disable */
export const carousalStyle = css`
  .slick-disabled {
    pointer-events: none;
    background-image: url('${(props) => getIconPath('right-disable-carousel-carrot', props)}');
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .slick-dots {
      display: none;
    }
  }
`;

export const carouselStyle = css`
  .smooth-scroll-list-item {
    overflow-x: scroll;
    display: inline-block;
    vertical-align: top;
    white-space: normal;
  }
  .completeLook_carousel-wrapper {
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }

  .image-wrapper {
    position: relative;
  }
  .slick-slide > div {
    padding: 1px 4px;
  }
  .slick-slide img {
    box-shadow: rgba(0, 0, 0, 0.15) 0px 3px 4px 1px;
  }
  .slick-slide:hover .shop-look-text {
    opacity: 1;
  }
  .slick-slide {
    max-width: ${(props) => (props.showSmallImage ? `45%` : '70%')};
  }
  .slick-list {
    padding-left: 0px !important;
  }
  @media ${(props) => props.theme.mediaQuery.medium} {
    .slick-slide > div {
      padding: 1px 12px;
    }
    .slick-list {
      margin: 0 38px;
    }
    .slick-slide {
      max-width: 50%;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .slick-slide > div {
      padding: 1px 15px;
    }
    .slick-slide {
      max-width: 25%;
    }
    .slick-list {
      margin: 0 60px;
    }
    .slick-slider {
      margin: 0 35px;
    }
  }
`;

export const modalStyles = css`
  div.TCPModal__InnerContent.atb-innerContent {
    right: 0;
    left: auto;
    top: ${(props) => (props.isCompleteLookMobile ? 'auto' : '0')};
    bottom: 0;
    transform: none;
    box-shadow: 0 4px 8px 0 rgba(163, 162, 162, 0.5);
    border-top-left-radius: ${(props) => (props.isCompleteLookMobile ? '8px' : '0')};
    border-top-right-radius: ${(props) => (props.isCompleteLookMobile ? '8px' : '0')};
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 20px
      ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    height: auto;
    ${(props) =>
      !props.isCompleteLookMobile
        ? `
        width: 350px;
      `
        : ''};
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        !props.isCompleteLookMobile
          ? `
              width: 375px;
            `
          : 'width: 380px; top:0'};
    }

    .Modal-Header {
      background-color: ${(props) => props.theme.colors.TEXT.PALEGRAY};
      margin: 0 -${(props) => props.theme.spacing.ELEM_SPACING.MED};
      padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
    .Modal_Heading {
      font-size: 14px;
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      line-height: 43px;
      border: none;
      margin-bottom: 0;
      padding: 0;
      display: inline;
    }

    .close-modal {
      right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      width: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
  }
`;

const styles = css`
  .added-to-bg-close {
    top: 21px;
  }
  .addedToBagWrapper {
    overflow-y: auto;
    overflow-x: hidden;
    height: calc(100% - 43px);
    padding-right: 15px;
  }
  .continue-shopping {
    text-align: center;
    margin: 24px 0;
  }
  .recommendationWrapper {
    margin: 0 20px;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 0;
    }
  }
  .loyaltyAddedToBagWrapper {
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .out-of-stock {
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy6}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    display: flex;
    align-items: center;
    justify-content: center;
    color: ${(props) => props.theme.colors.BLACK};
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .view-product-details {
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    display: flex;
    color: ${(props) => props.theme.colors.BLACK};
  }
  .large-image {
    width: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL};
    height: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL};
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20px;
    box-shadow: 0 2px 4px 0 rgba(117, 117, 117, 0.5);
  }
  .image-rectangle {
    width: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    height: 88px;
  }
  .slick-slide .selected-product {
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    border: solid 2px ${(props) => props.theme.colors.BRAND.PRIMARY};
  }
  .product-out-of-stock {
    opacity: 0.3;
  }
  .soldout {
    border: 1px solid ${(props) => props.theme.colorPalette.gray[2200]};
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
  .line1 {
    top: 0;
    left: 0;
    width: 220px;
    height: 45px;
    border-bottom: 1px solid ${(props) => props.theme.colorPalette.gray[2200]};
    position: absolute;
    transform: translateY(0px) translateX(-61px) rotate(51deg);
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      width: 245px;
      transform: translateY(0px) translateX(-76px) rotate(53deg);
    }
  }
  .carousel-drawer {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  }
  .size-chart {
    display: none;
  }
  .color-selector {
    display: none;
  }

  .price-container {
    display: flex;
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .list-badge-container {
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .select-value-wrapper {
    margin-top: -25px;
  }
  .select-value-wrapper div {
    width: calc(100% + 10px);
  }
  .completeLook_carousel-wrapper {
    min-height: 560px;
  }
  .smooth-scroll-list-item,
  .soldout {
    display: flex;
    vertical-align: top;
    white-space: normal;
    margin: 0 6px;
    padding: 5px;
    height: 102px;
    max-width: 91px;
    overflow: hidden;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      height: 118px;
    }
  }
  .smooth-scroll-list-item a,
  .soldout a {
    display: flex;
    align-items: center;
    position: relative;
  }
  .smooth-scroll-list {
    min-height: 140px;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      min-height: 158px;
    }
  }
  .full-screen-wrap {
    position: absolute;
    width: calc(100% - 15px);
    right: 0;
  }
  .full-screen-wrap .slick-next {
    right: 0px;
  }
  .smooth-scroll-list::-webkit-scrollbar {
    display: none;
  }
  .carousel-item {
    margin-top: 20px;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-bottom: 10px;
    }
    .slick-arrow {
      width: 50px;
      height: 50px;
    }
    .slick-disabled {
      opacity: 0.5;
    }
    .slick-prev {
      left: -${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .size-selector {
    margin-top: 20px;
  }
  .size-and-fit-detail-items-list {
    margin-top: 0;
  }
  .product-price-container {
    margin-top: 20px;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-top: 10px;
    }
  }
  .edit-form-css {
    margin-bottom: 15px;
  }
  .outfit-button-wrapper .button-wrapper {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-top: 10px;
    }
  }
  .dotted-line {
    background-image: url('${(props) => getIconPath('dotted-line', props)}');
    height: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    opacity: 1;
    border: none;
    background-repeat: repeat-x;
    width: auto;
  }
  .slick-list {
    margin-left: -${(props) => props.theme.spacing.ELEM_SPACING.MED};
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .price-container {
    padding-top: 0;
  }
`;

export default styles;
