// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

import { Image } from '../../../atoms';
import ProductTabList from '../../../organisms/StyliticsProductTabList';

const TILE_SHADOW = `
  shadow-opacity: 0.15;
  shadow-radius: 2px;
  shadow-color: ${(props) => props.theme.colorPalette.black};
  shadow-offset: 0px 4px;
  elevation: 2;
`;

const getAdditionalStyle = (props) => {
  const { isRelatedOutfit } = props;
  return {
    ...(!isRelatedOutfit && {
      'margin-bottom': props.theme.spacing.LAYOUT_SPACING.SM,
    }),
    ...(!isRelatedOutfit && {
      paddingTop: props.theme.spacing.ELEM_SPACING.LRG,
      paddingBottom: props.theme.spacing.ELEM_SPACING.LRG,
    }),
  };
};

const applyBackgroundClassStyle = (props) => {
  if (props.bgClass === 'yellow-bg') {
    return `
      background-color: #F5F5BE;
    `;
  }

  if (props.bgClass === 'gray-bg') {
    return `
      background-color: #f1f0f0;
    `;
  }

  if (props.bgClass === 'pink-bg') {
    return `
      background-color: #fff7fb;
    `;
  }
  return '';
};

export const Container = styled.View`
  width: 100%;
  ${applyBackgroundClassStyle}
  ${getAdditionalStyle}
`;

export const ImageSlidesWrapper = styled.View`
  margin-top: ${(props) =>
    props.hideTabs || (props.divTabs && props.divTabs.length <= 1)
      ? '0'
      : props.theme.spacing.ELEM_SPACING.MED};
`;

export const ImageSlideWrapper = styled.View`
  flex-direction: row;
  padding: ${(props) => (props.showData ? props.theme.spacing.ELEM_SPACING.LRG : '8px')};
`;

export const OutfitItemsWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const OutfitMainImageWrapper = styled.View`
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  ${(props) => (props.height ? `height: ${props.height};` : '')}
  ${(props) => (props.width ? `width: ${props.width};` : '')}
`;

export const OutfitMainTileWrapper = styled.View`
  padding: ${(props) =>
    `${props.theme.spacing.ELEM_SPACING.XS} ${props.theme.spacing.ELEM_SPACING.MED} ${props.theme.spacing.ELEM_SPACING.XS}`};
  background-color: ${(props) => props.theme.colorPalette.white};
  ${TILE_SHADOW}
`;

export const OutfitItemTileWrapper = styled.View`
  padding: 4px 3px;
  background-color: ${(props) => props.theme.colorPalette.white};
  ${(props) => (props.height ? `height: ${props.height};` : '')}
  ${(props) => (props.width ? `width: ${props.width};` : '')}
  ${TILE_SHADOW}
`;

export const RestOutfitItemCountWrapper = styled.View`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  justify-content: center;
`;

export const StyledImage = styled(Image)`
  /* stylelint-disable-next-line */
  resize-mode: contain;
  ${(props) => (props.height ? `height: ${props.height};` : '')}
  ${(props) => (props.width ? `width: ${props.width};` : '')}
`;

export const ImageItemWrapper = styled.View`
  flex-direction: row;
`;

export const ButtonContainer = styled.View`
  align-items: center;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const Wrapper = styled.View`
  width: 100%;
`;

export const PromoContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

export const HeaderContainer = styled.View`
  align-items: center;
  width: 100%;
`;

export const SecondHeaderContainer = styled.View`
  padding: ${(props) =>
    `${props.theme.spacing.ELEM_SPACING.SM} ${props.theme.spacing.ELEM_SPACING.XL} ${props.theme.spacing.ELEM_SPACING.LRG}`};
  width: 100%;
  align-items: center;
  justify-content: center;
`;

export const MessageContainer = styled.View`
  align-items: center;
  justify-content: center;
  width: 100%;

  ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

export const StyledProductTabList = styled(ProductTabList)`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const CompleteTheLookRedesign = styled.View`
  background-color: ${(props) => props.theme.colors.WHITE};
  flex-direction: column;
  margin: 0 20px 22px 0px;
  border-radius: 16px;
  box-shadow: ${(props) => props.theme.shadow.NEW_DESIGN.BOX_SHADOW};
  elevation: 10;
  ${(props) => (props.index === 0 ? `margin-left: ${props.theme.spacing.ELEM_SPACING.REG}` : '')}
`;

export const CompleteTheLookTitleRedesign = styled.View`
  margin: -45px 10px 10px 0;
  padding-top: 5px;
`;

export default {
  Container,
  ImageItemWrapper,
  ButtonContainer,
  ImageSlidesWrapper,
  ImageSlideWrapper,
  OutfitItemsWrapper,
  OutfitMainTileWrapper,
  OutfitItemTileWrapper,
  OutfitMainImageWrapper,
  RestOutfitItemCountWrapper,
  StyledImage,
  PromoContainer,
  MessageContainer,
  StyledProductTabList,
  Wrapper,
  CompleteTheLookRedesign,
  CompleteTheLookTitleRedesign,
};
