// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .smooth-scroll-list {
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
    width: 100%;
    -ms-overflow-style: none;
  }
  .smooth-scroll-list::-webkit-scrollbar {
    display: none;
  }
  .smooth-scroll-list-item {
    display: inline-block;
    vertical-align: top;
    white-space: normal;
  }
  .completeLook_carousel-wrapper {
    ${(props) =>
      props.isNewPDPEnabled
        ? `
        .slick-prev {
          left: 0;
        }
        .slick-next {
          right: 0;
        }
      
      `
        : ''}
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `.slick-track{
            margin-left:0
          };`
          : ''}
    }
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-bottom: ${(props) =>
        props.showCarousel ? props.theme.spacing.ELEM_SPACING.MED : '0px'};
    }
  }
  .view-look-text-block {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          width:186px
        `
          : ''}
    }
  }
  .view-look-text {
    color: ${(props) => props.theme.colors.WHITE};
    padding: 1px 6px;
    background-color: ${(props) => props.theme.colorPalette.blue[800]};
    display: inline-block;
    position: absolute;
    left: 0;
    border-bottom-right-radius: 6px;
    ${(props) =>
      props.isNewPDPEnabled &&
      `
        border-top-left-radius: 6px;
      `}
    font-family: Nunito;
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
  }
  .shop-look-text {
    display: none;
    @media ${(props) => props.theme.mediaQuery.large} {
      opacity: 0;
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      background-color: rgba(255, 255, 255, 0.7);
      justify-content: center;
      align-items: center;
      display: flex;
    }
  }
  .image-wrapper {
    position: relative;
  }
  .slick-slide > div {
    padding: 1px 4px;
  }
  .slick-slide img {
    box-shadow: rgba(0, 0, 0, 0.15) 0px 3px 4px 1px;
    ${(props) =>
      props.isNewPDPEnabled
        ? `
        box-shadow:none;
        border-radius: 6px;
        border: solid 1px ${props.theme.colors.PRIMARY.LIGHTGRAY};
      `
        : ''}
  }
  .slick-slide:hover .shop-look-text {
    opacity: 1;
  }
  .slick-slide:hover .view-look-img {
    border: solid 1px ${(props) => props.theme.colorPalette.blue[800]};
  }
  .slick-slide {
    max-width: ${(props) => (props.showSmallImage ? `45%` : '70%')};
  }
  .slick-list {
    padding-left: 0px !important;
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .slick-slide > div {
      padding: 1px 12px;
    }
    .slick-list {
      margin: 0 38px;
    }
    .slick-slide {
      max-width: 50%;
    }
  }
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    .slick-list {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          margin: 0px 10px;
     `
          : ``}
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .slick-slide > div {
      padding: 1px 15px;
    }
    .slick-slide {
      max-width: 25%;
    }
    .slick-list {
      margin: 0 60px;
    }
    .slick-slider {
      margin: 0 35px;
    }
  }
`;
