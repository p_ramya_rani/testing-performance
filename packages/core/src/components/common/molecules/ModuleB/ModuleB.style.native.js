// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { bannerPositionTypes } from './config';
import Button from '../../atoms/Button';

export const Container = styled.View``;

const getBackgroundColor = (backgroundColorObj) => {
  const [{ colorM }] = backgroundColorObj || [{}];
  return {
    mobile: colorM?.color,
  };
};

export const HeaderWrapper = styled.View`
  ${(props) =>
    props.bannerPosition === 'overlay'
      ? `
    position: absolute;
    z-index: ${props.theme.zindex.zOverlay};
    align-items: center;
    background: ${
      props.overlayBgColor && props.overlayBgColor.backgroundColor
        ? props.overlayBgColor.backgroundColor
        : props.theme.colorPalette.white
    };
    min-width: 224px;
    /* this should be 229px. However, because of android issue keeping it as 234 for now */
    min-height: 234px;
    border: solid 8px #52cbf5;
    padding:0 16px;
    display: flex;
    flex-direction: column;
    flex: 1;
  `
      : `
    padding-top: ${props.hasBgImage ? '0' : props.theme.spacing.LAYOUT_SPACING.XXS};
    width: 100%;
    background-color: ${
      getBackgroundColor(props.backgroundColorObj).mobile || props.theme.colorPalette.white
    };
  `};
`;

export const ButtonContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: 0;
`;
export const MainContainerView = styled.View`
  justify-content: center;
  align-items: center;
  margin-bottom: ${(props) =>
    props.isHpNewLayoutEnabled ? '0' : props.theme.spacing.ELEM_SPACING.SM};

  ${(props) =>
    props.isHpNewLayoutEnabled && props.bannerPosition !== bannerPositionTypes.overlay
      ? `
    margin-left: 0; margin-right: 0;
  `
      : `margin-left: 14px;
  margin-right: 14px;`}
`;
export const ContainerView = styled.View`
  margin-top: ${(props) =>
    props.isHpNewLayoutEnabled || props.noMarginTop ? '0' : props.theme.spacing.ELEM_SPACING.XS};
  margin-bottom: ${(props) =>
    props.isHpNewLayoutEnabled || props.noMarginTop ? '0' : props.theme.spacing.ELEM_SPACING.XS};
`;

export const CarouselView = styled.View`
  flex: 1;
`;

export const Border = styled.View`
  height: 0.5px;
  background: ${(props) =>
    props.background === 'text'
      ? props.theme.colors.BUTTON.WHITE.BORDER
      : props.theme.colorPalette.gray[700]};
`;

export const DivImageCTAContainer = styled.View``;
export const SeparatorView = styled.View`
  height: 1px;
  width: 150px;
  margin: 8px auto;
  background: ${(props) =>
    props.overlayRulerColor && props.overlayRulerColor.backgroundColor
      ? props.overlayRulerColor.backgroundColor
      : props.theme.colorPalette.black};
`;

export const StackedCTAView = styled.View`
  padding: 10px;
  justify-content: center;
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: white;
  border-top-left-radius: ${(props) =>
    props.ctaBorderRadiusOverride && props.ctaBorderRadiusOverride.borderTopLeftRadius
      ? props.ctaBorderRadiusOverride.borderTopLeftRadius
      : 0};
  border-top-right-radius: ${(props) =>
    props.ctaBorderRadiusOverride && props.ctaBorderRadiusOverride.borderTopRightRadius
      ? props.ctaBorderRadiusOverride.borderTopRightRadius
      : 0};
  border-bottom-right-radius: ${(props) =>
    props.ctaBorderRadiusOverride && props.ctaBorderRadiusOverride.borderBottomRightRadius
      ? props.ctaBorderRadiusOverride.borderBottomRightRadius
      : 0};
  border-bottom-left-radius: ${(props) =>
    props.ctaBorderRadiusOverride && props.ctaBorderRadiusOverride.borderBottomLeftRadius
      ? props.ctaBorderRadiusOverride.borderBottomLeftRadius
      : 0};
`;

export const StyledButton = styled(Button)`
  margin: 0 2px;
`;

export default {
  Container,
  HeaderWrapper,
  ButtonContainer,
  Border,
  ContainerView,
  DivImageCTAContainer,
  SeparatorView,
  MainContainerView,
  CarouselView,
};
