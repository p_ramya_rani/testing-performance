// 9fbef606107a605d69c0edbcd8029e5d
import { breakpoints } from '../../../../../styles/themes/TCP/mediaQuery';

export const ctaTypes = {
  divImageCTACarousel: 'imageCTAList',
  stackedCTAButtonsExpandable: 'stackedCTAList',
  CTAButtonCarouselExpandable: 'scrollCTAList',
  stackedCTAButtons: 'stackedCTAList',
  CTAButtonCarousel: 'scrollCTAList',
  ctaAddNoButtons: 'NoButton',
};

export const ctaTypeProps = {
  stackedCTAButtonsExpandable: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: true,
        large: true,
      },
    },
  },
  CTAButtonCarouselExpandable: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: true,
        large: true,
      },
    },
  },
  ctaAddNoButtons: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: false,
        large: false,
      },
    },
  },
};

export const bannerPositionTypes = {
  top: 'top',
  topAlt: 'topAlt',
  bottom: 'bottom',
  overlay: 'overlay',
};

export const MODULE_WIDTH_HALF = 'half';

export default {
  ctaIdentifiers: {
    STACK_CTA: 'stackedCTAButtons',
    SCROLL_CTA: 'CTAButtonCarouselExpandable',
    IMAGE_CTA: 'divImageCTACarousel',
  },
};

export const IMG_DATA = {
  imgOverlayConfig: ['t_mod_B_img_overlay_m', 't_mod_B_img_overlay_t', 't_mod_B_img_overlay_d'],
  imgDefaultConfig: ['t_mod_B_img_default_m', 't_mod_B_img_default_t', 't_mod_B_img_default_d'],
};

export const CAROUSEL_OPTIONS = {
  autoplay: true,
  arrows: true,
  autoplaySpeed: 4000,
  fade: false,
  speed: 1000,
  dots: true,
  dotsClass: 'slick-dots',
  swipe: true,
  responsive: [
    {
      breakpoint: breakpoints.values.lg,
      settings: {
        arrows: false,
      },
    },
  ],
};

export const BUTTON_WIDTH = '50%';
