// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import ButtonList from '@tcp/core/src/components/common/molecules/ButtonList';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import {
  getLocator,
  styleOverrideEngine,
  getBrand,
  isClient,
  getViewportInfo,
} from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import {
  getCtaBorderRadius,
  getHeaderBorderRadius,
  getMediaBorderRadius,
} from '@tcp/core/src/utils/utils.web';
import ImageBanner, { renderImage } from './ModuleB-imageBanner';
import style, { CtaButtonWrapper } from '../ModuleB.style';
import {
  ctaTypes,
  ctaTypeProps,
  MODULE_WIDTH_HALF,
  IMG_DATA,
  bannerPositionTypes,
} from '../config';
/**
 * This function returns button list variation on the basis of CTA Type
 * @param {*} ctaType
 */
const getButtonListVariation = (ctaType) => {
  const buttonTypes = {
    ...ctaTypes,
  };
  return buttonTypes[ctaType];
};

/**
 * This function returns props from button list variation on the basis of CTA Type
 * @param {*} ctaType
 */
const getButtonListVariationProps = (ctaType) => {
  const buttonTypeProps = {
    ...ctaTypeProps,
  };
  return buttonTypeProps[ctaType];
};

/**
 * This function returns column size for grid on the basis of moduleWidth param
 * @param {*} moduleWidth
 */
const getColSize = () => {
  return {
    small: 6,
    medium: 8,
    large: 12,
  };
};

const RenderBackgroundImage = (backgroundImage, mediaBorderRadiusOverride, others) => {
  const { image, video, link } = backgroundImage[0] || {};
  const imageData =
    !isEmpty(image) && !isEmpty(mediaBorderRadiusOverride)
      ? Object.assign(image, mediaBorderRadiusOverride)
      : image;
  const imgConfig = IMG_DATA.imgDefaultConfig;
  const index = 'BgImg';
  return (
    <div className="moduleB__bg-img-wrapper">
      {renderImage(imageData, link, video, imgConfig, index, others)}
    </div>
  );
};

const getCtaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom'])
    : {};
};

const getHeaderBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
    : {};
};

const getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};

const evaluateBgPosition = (bannerPosition) =>
  bannerPosition === bannerPositionTypes.top || bannerPosition === bannerPositionTypes.topAlt;

/**
 * This is Module B component implementation
 */
// eslint-disable-next-line
const ModuleB = (props) => {
  const {
    className,
    ctaItems,
    largeCompImage: [{ headerText, promoBanner, linkedImage, backgroundColor }],
    ctaType,
    bannerPosition,
    expandableTitle,
    moduleClassName,
    page,
    isHpNewDesignCTAEnabled,
    backgroundImage,
    isHpNewModuleDesignEnabled,
    ...others
  } = props;
  const { isMobile } = isClient() && getViewportInfo();
  const buttonListCtaType = getButtonListVariation(ctaType);
  const buttonListProps = getButtonListVariationProps(ctaType);
  const colSize = getColSize();
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleB');
  const { title, subTitle, promo, border } = styleOverrides || {};
  const headerStyle = [title, subTitle];
  const ctaItemFilteredObj = ctaItems && ctaItems.filter((item) => item.button);
  const ctaBgImageFilteredObj = ctaItems && ctaItems.filter((item) => item.ctaBackgroundImage);
  const ctaBgImage = ctaBgImageFilteredObj.length !== 0 && ctaBgImageFilteredObj[0];
  const ctaBorderRadiusOverride = !isMobile
    ? getCtaBorderRadiusOverride(isHpNewModuleDesignEnabled, styleOverrides)
    : {};
  const headerBorderRadiusOverride = getHeaderBorderRadiusOverride(
    isHpNewModuleDesignEnabled,
    styleOverrides
  );
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewModuleDesignEnabled,
    styleOverrides
  );
  const navBgStyleOverrides = styleOverrides['nav-bg'] || {};

  const dualVariation =
    ctaItemFilteredObj && ctaItemFilteredObj.length < 3
      ? null
      : buttonListProps && buttonListProps.dualVariation;

  const imageBannerProps = {
    bannerPosition,
    promoBanner: {
      headerText,
      promoBanner,
      headerStyle,
      promoStyle: promo,
      border,
      backgroundColor,
      headerBorderRadiusOverride,
    },
    linkedImage,
    styleOverrides,
    mediaBorderRadiusOverride,
    ...others,
  };
  const hasBackgroundImage = backgroundImage && backgroundImage.length > 0;

  return (
    <div
      className={`${className} ${moduleClassName} moduleB brand-${getBrand()} page-${page} ${
        hasBackgroundImage ? `bg_position` : ``
      }`}
    >
      {hasBackgroundImage &&
        RenderBackgroundImage(backgroundImage, mediaBorderRadiusOverride, others)}

      <Row
        className={
          hasBackgroundImage && evaluateBgPosition(bannerPosition) ? 'bg_top--variation' : ''
        }
        fullBleed={isHpNewModuleDesignEnabled && { small: true, medium: false, large: false }}
      >
        <Col colSize={colSize}>
          <ImageBanner {...imageBannerProps} hasBackgroundImage={hasBackgroundImage} />
        </Col>
      </Row>

      <Row
        fullBleed={{
          small: true,
          medium: false,
          desktop: false,
        }}
      >
        <CtaButtonWrapper
          colSize={colSize}
          ctaBgImageObj={ctaBgImage}
          style={Object.assign(navBgStyleOverrides, ctaBorderRadiusOverride)}
          hasBackgroundImage={hasBackgroundImage}
        >
          {ctaItemFilteredObj && (
            <ButtonList
              className="button-list"
              buttonsData={ctaItemFilteredObj}
              buttonListVariation={buttonListCtaType}
              dataLocatorDropDown={getLocator('moduleB_dropdown')}
              dataLocatorDivisionImages={getLocator('moduleB_cta_image')}
              dataLocatorTextCta={getLocator('moduleB_cta_links')}
              dropdownLabel={expandableTitle}
              dualVariation={dualVariation}
              isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
            />
          )}
        </CtaButtonWrapper>
      </Row>
    </div>
  );
};

ModuleB.propTypes = {
  className: PropTypes.string.isRequired,
  moduleWidth: PropTypes.string,
  ctaItems: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.shape({}))).isRequired,
  largeCompImage: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.shape({}))).isRequired,
  ctaType: PropTypes.string.isRequired,
  bannerPosition: PropTypes.string.isRequired,
  expandableTitle: PropTypes.string.isRequired,
  moduleClassName: PropTypes.string,
  page: PropTypes.string,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  backgroundImage: PropTypes.shape([]).isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

ModuleB.defaultProps = {
  moduleWidth: MODULE_WIDTH_HALF,
  moduleClassName: '',
  page: '',
};

export { ModuleB as ModuleBVanilla };
export default errorBoundary(withStyles(ModuleB, style));
