// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { getStyleArray } from '@tcp/core/src/utils/utils.web';
import { DamImage, Anchor } from '@tcp/core/src/components/common/atoms';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import {
  getLocator,
  configureInternalNavigationFromCMSUrl,
  getIconPath,
} from '@tcp/core/src/utils';
import { bannerPositionTypes, IMG_DATA, CAROUSEL_OPTIONS } from '../config';
import { Carousel } from '../ModuleB-imageBanner.style';

const bigCarrotIcon = 'carousel-big-carrot';

// Carousel Next and Prev Arrows
const CarouselArrow = ({ arrowType, label, onClick }) => {
  return (
    <button
      type="button"
      tabIndex="0"
      aria-label={label}
      data-locator={`moduleB_${arrowType}_arrow`}
      className={`carousel-nav-arrow-button carousel-nav-${arrowType}-button`}
      onClick={onClick}
    >
      <svg width="15" height="52" role="img">
        <path
          fill="#1A1A1A"
          d="M.113 50.539a1 1 0 001.774.923l13-25a1 1 0 000-.923l-13-25a1 1 0 10-1.774.923L12.873 26 .113 50.54z"
        />
      </svg>
    </button>
  );
};

CarouselArrow.propTypes = {
  arrowType: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

/**
 * This function renders Promo Banner and Header Text
 * @param {*} param0
 */
const renderPromoBanner = (promoBanner, isModBOverlay, hasBackgroundImage) => {
  const borderColor = (promoBanner && promoBanner.border) || {};
  const borderStyle = borderColor.color ? { border: `8px solid ${borderColor.color}` } : {};
  const [{ textItems }] = (promoBanner && promoBanner.promoBanner) || [{}];
  const styleArray = getStyleArray(textItems);
  const backgroundColorObj = (!isModBOverlay && promoBanner && promoBanner.backgroundColor) || [{}];
  const overrideWrapperStyle =
    (!isModBOverlay && promoBanner && promoBanner.headerBorderRadiusOverride) || {};
  return (
    promoBanner.promoBanner && (
      <PromoBanner
        style={Object.assign(borderStyle, overrideWrapperStyle)}
        className={`promo-banner ${hasBackgroundImage ? `bg_promo-banner` : ``}`}
        dataLocatorHeader={getLocator('moduleB_header_text')}
        data-locator={getLocator('moduleB_promo_banner_text')}
        variation="header_and_promo"
        isModBOverlay={isModBOverlay}
        styleArray={styleArray}
        backgroundColorObj={backgroundColorObj}
        {...promoBanner}
      />
    )
  );
};

export const renderImage = (image, link, video, imgConfig, index, props) => {
  const { isHomePage = false } = props || {};
  const navigationUrl = link;
  const isMediaOverridePassed = (props && props?.mediaBorderRadiusOverride) || false;
  const imgData = !isEmpty(image) ? image : {};
  const videoData = !isEmpty(video) ? video : {};
  navigationUrl.to = configureInternalNavigationFromCMSUrl(link?.url);
  navigationUrl.asPath = link?.url;
  const damImageComp = (
    <DamImage
      imgData={
        isMediaOverridePassed ? Object.assign(imgData, props.mediaBorderRadiusOverride) : imgData
      }
      videoData={
        isMediaOverridePassed
          ? Object.assign(videoData, props.mediaBorderRadiusOverride)
          : videoData
      }
      className="image"
      dataLocator={`${getLocator('moduleB_carousel_image_img')}${index + 1}`}
      imgConfigs={imgConfig}
      noSourcePictureTag
      isOptimizedVideo
      isHomePage={isHomePage}
      link={link}
      lazyLoad
      isModule
    />
  );
  return (
    <div className="image-container" data-locator={getLocator('moduleB_image')}>
      {image ? (
        <Anchor {...navigationUrl} className="image-link">
          {damImageComp}
        </Anchor>
      ) : null}
      {video ? <React.Fragment>{damImageComp}</React.Fragment> : null}
    </div>
  );
};

const renderCarouselComponent = (linkedImage, imgConfig, props) => {
  const { accessibility, styleOverrides } = props || {};
  const sliderFadeOptions = styleOverrides['slider-fade'];
  const { playIconButton, pauseIconButton, previousButton, nextIconButton } = accessibility || {};
  const carouselIcon = bigCarrotIcon;

  const carouselConfig = {
    autoplay: true,
    dataLocatorPlay: getLocator('moduleB_play_button'),
    dataLocatorPause: getLocator('moduleB_pause_button'),
    customArrowLeft: getIconPath(carouselIcon),
    customArrowRight: getIconPath(carouselIcon),
    dataLocatorCarousel: 'carousel_banner',
  };

  CAROUSEL_OPTIONS.prevArrow = <CarouselArrow arrowType="prev" label={previousButton} />;
  CAROUSEL_OPTIONS.nextArrow = <CarouselArrow arrowType="next" label={nextIconButton} />;
  carouselConfig.autoplay = carouselConfig.autoplay && linkedImage.length > 1;
  carouselConfig.pauseIconButtonLabel = pauseIconButton;
  carouselConfig.playIconButtonLabel = playIconButton;

  const overrideCarouselOptions = !isEmpty(sliderFadeOptions)
    ? {
        fade: true,
        cssEase: 'linear',
        infinite: true,
        autoplaySpeed: parseFloat(sliderFadeOptions.speed),
        speed: 500,
      }
    : {};

  return (
    <Carousel
      options={{ ...CAROUSEL_OPTIONS, ...overrideCarouselOptions }}
      carouselConfig={carouselConfig}
      isModule
    >
      {linkedImage.map(({ image, link, video }, index) => {
        return renderImage(image, link, video, imgConfig, index, props);
      })}
    </Carousel>
  );
};

/**
 * This function renders Linked Image component
 * @param {*} param0
 */
const renderMedia = (linkedImage, imgConfig, props) => {
  if (!linkedImage) {
    return null;
  }
  if (linkedImage?.length > 1) {
    return renderCarouselComponent(linkedImage, imgConfig, props);
  }
  const [{ image, link, video }] = linkedImage;
  return renderImage(image, link, video, imgConfig, 0, props);
};

/**
 * This component loads Image and Promo banner component in different stacks
 * Stacks -
 *  - Top
 *  - Top Alt
 *  - Overlay
 *  - Bottom
 * @param {*} props
 */
const ImageBanner = (props) => {
  const { bannerPosition, promoBanner, linkedImage, hasBackgroundImage } = props;
  let isModBOverlay = false;

  switch (bannerPosition) {
    case bannerPositionTypes.top:
      return (
        <div className="banner-top-variation">
          {renderPromoBanner(promoBanner, isModBOverlay, hasBackgroundImage)}
          {renderMedia(linkedImage, IMG_DATA.imgDefaultConfig, props)}
        </div>
      );
    case bannerPositionTypes.topAlt:
      return (
        <div className="banner-top-alt-variation">
          {renderPromoBanner(promoBanner, isModBOverlay)}
          {renderMedia(linkedImage, IMG_DATA.imgDefaultConfig, props)}
        </div>
      );
    case bannerPositionTypes.overlay:
      isModBOverlay = true;
      return (
        <div className="banner-overlay-variation">
          {renderMedia(linkedImage, IMG_DATA.imgOverlayConfig, props)}
          {renderPromoBanner(promoBanner, isModBOverlay)}
        </div>
      );
    case bannerPositionTypes.bottom:
      return (
        <div className="banner-bottom-variation">
          {renderMedia(linkedImage, IMG_DATA.imgDefaultConfig, props)}
          {renderPromoBanner(promoBanner, isModBOverlay)}
        </div>
      );
    default:
      return false;
  }
};

ImageBanner.propTypes = {
  bannerPosition: PropTypes.string.isRequired,
  promoBanner: PropTypes.shape({}).isRequired,
  linkedImage: PropTypes.shape({}).isRequired,
};

export default ImageBanner;
