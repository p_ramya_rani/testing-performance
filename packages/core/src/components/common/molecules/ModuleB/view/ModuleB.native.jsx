// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable no-shadow */
import React from 'react';
import { View, ImageBackground } from 'react-native';
import PropTypes from 'prop-types';
import ButtonList from '../../ButtonList';
import Carousel from '../../Carousel';
import { DamImage, Anchor } from '../../../atoms';
import LinkText from '../../LinkText';
import PromoBanner from '../../PromoBanner';
import {
  isGymboree,
  getScreenWidth,
  LAZYLOAD_HOST_NAME,
  styleOverrideEngine,
  getMediaBorderRadius,
  getHeaderBorderRadius,
  getCtaBorderRadius,
} from '../../../../../utils/index.native';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';

import {
  Container,
  ButtonContainer,
  ContainerView,
  DivImageCTAContainer,
  HeaderWrapper,
  SeparatorView,
  MainContainerView,
  CarouselView,
  StackedCTAView,
  StyledButton,
} from '../ModuleB.style.native';
import { ctaTypes, bannerPositionTypes, IMG_DATA, BUTTON_WIDTH } from '../config';
import { generateUniqueKeyUsingLabel, cropImageUrl } from '../../../../../utils';

/**
 * Module height and width.
 * Height is fixed for mobile : TCP & Gymb
 * Width can vary as per device width.
 */
const MODULE_HEIGHT_WITHOUT_OVERLAY = 340;
const MODULE_HEIGHT_WITH_OVERLAY = 435;
const MARGIN = 12;
const MODULE_WIDTH = getScreenWidth() - MARGIN * 2;
const NEW_LAYOUT_MODULE_WIDTH = getScreenWidth() - 2 * HP_NEW_LAYOUT.BODY_PADDING;

/**
 * @function getBackgroundUrl
 * renders image url
 *
 * @param {*} bgObj
 * @returns
 */
const getBackgroundUrl = (bgObj) => {
  const [{ imageM }] = bgObj || [{}];
  return imageM?.url;
};

const applyNewLayoutStyle = (isHpNewLayoutEnabled, headerBorderRadiusOverride) =>
  isHpNewLayoutEnabled && headerBorderRadiusOverride;

/**
 * @function renderHeaderAndBanner
 * renders header component with link text and promobanner
 *
 * @param {*} item
 * @param {*} navigation
 * @returns
 */
const renderHeaderAndBanner = (item, navigation) => {
  const {
    item: { headerText, promoBanner, backgroundColor },
    bannerPosition,
    headerStyle,
    promoStyle,
    borderStyle,
    overlayBgColor,
    overlayRulerColor,
    headerBorderRadiusOverride,
    isHpNewLayoutEnabled,
  } = item;
  const isBannerOverlayVariation = bannerPosition === bannerPositionTypes.overlay;
  const style = isBannerOverlayVariation
    ? {
        fontFamily: 'primary',
        fontSize: 'fs12',
        fontWeight: 'regular',
      }
    : {
        fontFamily: 'quaternary',
        fontSize: 'fs20',
      };
  const borderRadiusStyle = !isBannerOverlayVariation && headerBorderRadiusOverride;
  const hasBackgroundImage = promoBanner && backgroundColor && !backgroundColor?.imageM;
  const imgUrl = hasBackgroundImage && getBackgroundUrl(backgroundColor);
  const imageUri = imgUrl && cropImageUrl(imgUrl);
  const image = {
    uri: imageUri,
  };
  const imgBgStyle = {
    flex: 1,
    width: '100%',
  };

  return (
    <HeaderWrapper
      bannerPosition={bannerPosition}
      overlayBgColor={overlayBgColor}
      style={{ ...borderStyle, ...borderRadiusStyle }}
      backgroundColorObj={!isBannerOverlayVariation && backgroundColor}
      hasBgImage={hasBackgroundImage}
    >
      {headerText && (isBannerOverlayVariation || bannerPosition === bannerPositionTypes.top) && (
        <ContainerView>
          <LinkText
            type="heading"
            {...style}
            navigation={navigation}
            headerText={headerText}
            locator="moduleB_header_text"
            textAlign="center"
            renderComponentInNewLine
            useStyle
            headerStyle={headerStyle}
          />
          {bannerPosition === bannerPositionTypes.top ? null : (
            <SeparatorView overlayRulerColor={overlayRulerColor} />
          )}
        </ContainerView>
      )}
      <ContainerView noMarginTop>
        {hasBackgroundImage ? (
          <ImageBackground
            source={image}
            style={imgBgStyle}
            resizeMode="stretch"
            imageStyle={applyNewLayoutStyle(isHpNewLayoutEnabled, headerBorderRadiusOverride)}
          >
            {promoBanner && (
              <PromoBanner
                promoBanner={promoBanner}
                navigation={navigation}
                bannerPosition={bannerPosition}
                locator="moduleB_promobanner_text"
                promoStyle={promoStyle}
              />
            )}
          </ImageBackground>
        ) : (
          promoBanner && (
            <PromoBanner
              promoBanner={promoBanner}
              navigation={navigation}
              bannerPosition={bannerPosition}
              locator="moduleB_promobanner_text"
              promoStyle={promoStyle}
            />
          )
        )}
      </ContainerView>
    </HeaderWrapper>
  );
};

/**
 * To Render the Dam Image
 */
const renderDamImage = (
  link,
  imgData,
  videoData,
  navigation,
  bannerPosition,
  moduleHeight,
  { isHpNewLayoutEnabled, mediaBorderRadiusOverride }
) => {
  const damImageComp = (
    <DamImage
      width={isHpNewLayoutEnabled ? NEW_LAYOUT_MODULE_WIDTH : MODULE_WIDTH}
      videoData={videoData}
      height={moduleHeight}
      imgData={imgData}
      host={LAZYLOAD_HOST_NAME.HOME}
      alt={imgData && imgData.alt}
      crop={imgData && imgData.crop_m}
      imgConfig={
        bannerPosition === bannerPositionTypes.overlay
          ? IMG_DATA.imgOverlayConfig[0]
          : IMG_DATA.imgDefaultConfig[0]
      }
      isFastImage
      resizeMode="stretch"
      isHomePage
      link={link}
      navigation={navigation}
      overrideStyle={isHpNewLayoutEnabled && mediaBorderRadiusOverride}
      isBackground={false}
    />
  );
  if (imgData && Object.keys(imgData).length > 0) {
    return (
      <Anchor url={link.url} navigation={navigation}>
        {damImageComp}
      </Anchor>
    );
  }
  return videoData && Object.keys(videoData).length > 0 ? (
    <React.Fragment>{damImageComp}</React.Fragment>
  ) : null;
};

const renderView = (
  item,
  navigation,
  bannerPosition,
  moduleHeight,
  { isHpNewLayoutEnabled, mediaBorderRadiusOverride }
) => {
  const {
    item: { image, link, video },
  } = item;
  const videoData = video &&
    video.url && {
      videoWidth: isHpNewLayoutEnabled ? NEW_LAYOUT_MODULE_WIDTH : MODULE_WIDTH,
      videoHeight: moduleHeight,
      ...video,
    };
  const imgData = image || {};

  return (
    <View>
      {renderDamImage(link, imgData, videoData, navigation, bannerPosition, moduleHeight, {
        isHpNewLayoutEnabled,
        mediaBorderRadiusOverride,
      })}
    </View>
  );
};

const renderCarousel = (
  linkedImage,
  navigation,
  bannerPosition,
  moduleHeight,
  { isHpNewLayoutEnabled, mediaBorderRadiusOverride }
) => {
  return linkedImage && linkedImage.length > 1 ? (
    <CarouselView>
      <Carousel
        height={moduleHeight}
        data={linkedImage}
        renderItem={(item) =>
          renderView(item, navigation, bannerPosition, moduleHeight, {
            isHpNewLayoutEnabled,
            mediaBorderRadiusOverride,
          })
        }
        width={isHpNewLayoutEnabled ? NEW_LAYOUT_MODULE_WIDTH : MODULE_WIDTH}
        carouselConfig={{
          autoplay: true,
        }}
        showDots
        overlap
        isModule
      />
    </CarouselView>
  ) : (
    linkedImage &&
      renderView({ item: linkedImage[0] }, navigation, bannerPosition, moduleHeight, {
        isHpNewLayoutEnabled,
        mediaBorderRadiusOverride,
      })
  );
};

const getModuleHeight = (overrideHeight, defaultHeight) => {
  return overrideHeight || defaultHeight;
};

/**
 * @function renderMediaComponent
 * renders Media component with header and banner
 * handles bannerPosition - top/bottom/overlay
 * display header and banner as overlay over image if bannerPosition is overlay
 *
 * @param {*} item
 * @param {*} navigation
 * @returns
 */
const renderMediaComponent = (item, navigation) => {
  const {
    item: { linkedImage },
    bannerPosition,
    heightOverride,
    isHpNewLayoutEnabled,
    mediaBorderRadiusOverride,
  } = item;

  // set module height as same as screen width to make it a square for bannerPosition as overlay
  const MODULE_OVERRIDE_HEIGHT = heightOverride && parseFloat(heightOverride.height);
  const moduleHeight =
    bannerPosition === bannerPositionTypes.overlay
      ? getModuleHeight(MODULE_OVERRIDE_HEIGHT, MODULE_HEIGHT_WITH_OVERLAY)
      : getModuleHeight(MODULE_OVERRIDE_HEIGHT, MODULE_HEIGHT_WITHOUT_OVERLAY);

  return (
    <MainContainerView bannerPosition={bannerPosition} isHpNewLayoutEnabled={isHpNewLayoutEnabled}>
      {bannerPosition === bannerPositionTypes.top || bannerPosition === bannerPositionTypes.overlay
        ? renderHeaderAndBanner(item, navigation)
        : null}
      {renderCarousel(linkedImage, navigation, bannerPosition, moduleHeight, {
        isHpNewLayoutEnabled,
        mediaBorderRadiusOverride,
      })}
      {bannerPosition === bannerPositionTypes.bottom
        ? renderHeaderAndBanner(item, navigation)
        : null}
    </MainContainerView>
  );
};

/**
 * This method return the ButtonList View according to the different variation .
 *  @ctaType are four types : 'imageCTAList' ,'stackedCTAList','scrollCTAList','linkCTAList'.
 *  @naviagtion is used to navigate the page.
 */
const renderButtonList = (
  ctaType,
  navigation,
  ctaItems,
  locator,
  color,
  buttonVariation,
  ctaBorderRadiusOverride = {}
) => {
  if (isGymboree() && ctaType === ctaTypes.stackedCTAButtons && ctaItems.length < 3) {
    return (
      <StackedCTAView ctaBorderRadiusOverride={ctaBorderRadiusOverride}>
        {ctaItems.map((item) => (
          <StyledButton
            uniqueKey={() => generateUniqueKeyUsingLabel(item.button.text)}
            accessibilityLabel={item.button.text}
            text={item.button.text}
            navigation={navigation}
            color={color}
            url={item.button.url}
            width={BUTTON_WIDTH}
          />
        ))}
      </StackedCTAView>
    );
  }

  return (
    ctaItems && (
      <ButtonList
        buttonListVariation={ctaType}
        navigation={navigation}
        buttonsData={ctaItems}
        locator={locator}
        color={color}
        buttonVariation={buttonVariation}
        overrideStyle={ctaBorderRadiusOverride}
      />
    )
  );
};

const getCtaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled
    ? getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom'])
    : {};
};

const getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};

const getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled
    ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
    : {};
};

/**
 * @param {object} props : Props for Module B multi type of banner list, button list, header text.
 * @desc This is Module B global component. It has capability to display
 * featured content module with 1 background color tiles ,links and a CTA Button list.
 */

const ModuleB = (props) => {
  // TODO: All items need to be derived from props once cms integration is done
  const {
    ctaItems,
    largeCompImage,
    ctaType: ctaItemsType,
    bannerPosition,
    navigation,
    moduleClassName,
    isHpNewLayoutEnabled,
  } = props;
  const {
    title,
    subTitle,
    promo,
    subPromoTitle,
    overlayBgColor,
    overlayRulerColor,
    border,
    imgHeight: heightOverride,
    ...otherStyleOverrides
  } = styleOverrideEngine(moduleClassName, 'ModuleB') || {};
  const headerStyle = [title, subTitle];
  const promoStyleOverride = [promo, subPromoTitle];
  const borderStyle = border && border.color ? { borderWidth: 8, borderColor: border.color } : {};
  const ctaType = ctaTypes[ctaItemsType];
  const headerBorderRadiusOverride = getHeaderBorderRadiusOverride(
    isHpNewLayoutEnabled,
    otherStyleOverrides
  );
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewLayoutEnabled,
    otherStyleOverrides
  );
  const ctaBorderRadiusOverride = getCtaBorderRadiusOverride(
    isHpNewLayoutEnabled,
    otherStyleOverrides
  );

  let bannerPositionInterpreted = bannerPosition;
  if (bannerPosition === 'topAlt') {
    bannerPositionInterpreted = 'top';
  }

  return largeCompImage ? (
    <Container style={isHpNewLayoutEnabled && HP_NEW_LAYOUT.MODULE_BOX_SHADOW}>
      {renderMediaComponent(
        {
          item: largeCompImage[0],
          bannerPosition: bannerPositionInterpreted,
          headerStyle,
          promoStyle: promoStyleOverride,
          borderStyle,
          overlayBgColor,
          overlayRulerColor,
          heightOverride,
          isHpNewLayoutEnabled,
          headerBorderRadiusOverride,
          mediaBorderRadiusOverride,
        },
        navigation
      )}

      {ctaType === ctaTypes.divImageCTACarousel && (
        <DivImageCTAContainer>
          {renderButtonList(ctaType, navigation, ctaItems, 'moduleB_cta_links', 'black')}
        </DivImageCTAContainer>
      )}

      {ctaType === ctaTypes.stackedCTAButtons && (
        <ContainerView isHpNewLayoutEnabled={isHpNewLayoutEnabled}>
          {renderButtonList(
            ctaType,
            navigation,
            ctaItems,
            'stacked_cta_list',
            'gray',
            undefined,
            ctaBorderRadiusOverride
          )}
        </ContainerView>
      )}

      {ctaType === ctaTypes.CTAButtonCarousel && (
        <ButtonContainer>
          {renderButtonList(ctaType, navigation, ctaItems, 'scroll_cta_list', 'gray')}
        </ButtonContainer>
      )}
    </Container>
  ) : (
    <Container />
  );
};

ModuleB.propTypes = {
  ctaItems: PropTypes.shape([]).isRequired,
  largeCompImage: PropTypes.shape([]).isRequired,
  ctaType: PropTypes.string.isRequired,
  bannerPosition: PropTypes.number.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  moduleClassName: PropTypes.string.isRequired,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleB;
export { ModuleB as ModuleBVanilla };
