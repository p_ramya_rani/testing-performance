// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import CarouselResponsive from '@tcp/core/src/components/common/molecules/Carousel/views/CarouselResponsive';

const StyledCarousal = styled(CarouselResponsive)`
  .control-arrow {
    background-size: 100% 100%;
    height: 42px;
    width: 13px;
    z-index: 1;
  }
  .control-dots {
    bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG_2};
    z-index: 1;
  }
  .tcp_carousel__play_pause_button {
    margin-top: -55px;
    bottom: auto;
  }

  @media only screen and (max-width: 767px) {
    .control-arrow.control-prev,
    .control-arrow.control-next {
      display: none;
    }
  }
`;

export default css``;

export { StyledCarousal as Carousel };
