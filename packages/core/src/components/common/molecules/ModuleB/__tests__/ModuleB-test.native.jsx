// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ModuleBVanilla } from '../view/ModuleB.native';
import mock from '../../../../../services/abstractors/common/moduleB/mock';

describe('ModuleBVanilla', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ModuleBVanilla {...mock.composites} />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when isHpNewLayoutEnabled is true', () => {
    wrapper = shallow(<ModuleBVanilla {...mock.composites} isHpNewLayoutEnabled />);
    expect(wrapper).toMatchSnapshot();
  });
});
