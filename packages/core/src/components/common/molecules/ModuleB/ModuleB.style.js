// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { cropImageUrl } from '@tcp/core/src/utils';
import Col from '@tcp/core/src/components/common/atoms/Col';

const getCtaBgImage = (bgImageObj) => {
  const {
    ctaBackgroundImage: { url: ctaDesktopUrl, url_t: ctaTabletUrl, url_m: ctaMobileUrl },
  } = bgImageObj || {};
  return {
    desktop: ctaDesktopUrl && cropImageUrl(ctaDesktopUrl),
    tablet: ctaTabletUrl ? cropImageUrl(ctaTabletUrl) : cropImageUrl(ctaDesktopUrl),
    mobile: ctaMobileUrl ? cropImageUrl(ctaMobileUrl) : cropImageUrl(ctaDesktopUrl),
  };
};

export const CtaButtonWrapper = styled(Col)`
  ${(props) =>
    props.ctaBgImageObj && getCtaBgImage(props.ctaBgImageObj)
      ? `
        background-image: url(${getCtaBgImage(props.ctaBgImageObj).mobile});
        @media ${props.theme.mediaQuery.large} {
          background-image: url(${getCtaBgImage(props.ctaBgImageObj).desktop});
        }
        @media ${props.theme.mediaQuery.mediumOnly} {
          background-image: url(${getCtaBgImage(props.ctaBgImageObj).tablet});
        }
      `
      : null};

  ${(props) =>
    props.hasBackgroundImage
      ? `
    @media ${props.theme.mediaQuery.large} {
      position: absolute;
      bottom: ${props.theme.spacing.LAYOUT_SPACING.XL};
    }
    @media ${props.theme.mediaQuery.mediumOnly} {
      position: absolute;
      bottom: ${props.theme.spacing.LAYOUT_SPACING.LRG};
    }
  `
      : ``};
`;

export default css`
  .promo-banner {
    background: ${(props) => props.theme.colorPalette.white};
  }

  .button-list {
    margin-top: 16px;
    padding-bottom: 0;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding-bottom: 10px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-bottom: 16px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => (props.isHpNewModuleDesignEnabled ? `margin-top: 0;` : ``)};
    }
  }

  &.bg_position {
    position: relative;
  }

  .bg_top--variation {
    position: absolute;
    top: 0;
  }

  .bg_promo-banner {
    background: transparent;
  }

  .moduleB__bg-img-wrapper {
    width: 100%;
    img {
      vertical-align: middle;
    }
  }

  .banner-top-variation,
  .banner-top-alt-variation,
  .banner-bottom-variation {
    .promo-banner-header {
      display: none;
    }
    .image-container {
      height: auto;
      text-align: center;
      .image {
        max-height: 100%;
        width: 100%;
      }
      img {
        vertical-align: middle;
      }
    }
  }

  .banner-top-variation,
  .banner-top-alt-variation {
    .promo-banner {
      padding-bottom: 15px;
      padding-top: 5px;
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        padding-top: 24px;
      }
    }
  }
  .banner-bottom-variation {
    padding-bottom: 4px;
    .promo-banner {
      padding-top: 8px;
    }
  }
  .banner-overlay-variation {
    position: relative;
    .image-container {
      position: relative;

      height: auto;
    }
    .promo-banner {
      min-width: 186px;
      opacity: 0.9;
      padding: 0 16px 10px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate3d(-50%, -50%, 0);
    }
    .small_text_semibold,
    .small_text_black {
      line-height: 1;
      letter-spacing: 1px;
    }
    .small_text_black {
      padding-bottom: 5px;
    }
    .promo-text-link {
      display: inline-block;
      margin-top: 12px;
    }
  }

  &.brand-tcp.page-home .link-text .style1,
  &.brand-tcp.page-home .link-text .style2 {
    font-family: TofinoWide;
  }

  &.brand-tcp.page-home .link-text .style2 {
    font-weight: 900;
  }

  &.brand-tcp.page-home .link-text .style1 {
    font-weight: normal;
  }

  &.brand-tcp.page-home .promo-text.small_text_black,
  &.brand-tcp.page-home .promo-text.style10 {
    font-family: TofinoWide;
  }

  &.brand-tcp.page-home .promo-text.small_text_semibold {
    font-family: TofinoWide;
    font-weight: 500;
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 38px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .banner-top-variation,
    .banner-top-alt-variation,
    .banner-bottom-variation {
      .image-container {
        height: auto;
        text-align: center;
      }
      .image {
        max-height: 100%;
      }
    }
    .banner-bottom-variation {
      .promo-banner {
        padding-top: 16px;
      }
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .banner-top-variation,
    .banner-bottom-variation {
      .promo-text-link {
        display: flex;
        justify-content: center;
        width: max-content;
      }
      .small_text_semibold,
      .small_text_black {
        margin: 32px 10px 0 0;
      }
    }
    .banner-top-alt-variation {
      .promo-banner {
        padding-top: 10px;
      }
      .small_text_semibold,
      .small_text_black {
        display: block;
      }
    }
    .banner-top-variation {
      .promo-banner {
        padding-top: 40px;
        padding-bottom: 24px;
      }
      .promo-text-link {
        display: inline;
      }
    }
    .banner-bottom-variation {
      padding-bottom: 40px;
      .promo-banner {
        padding-top: 24px;
      }
    }
    .banner-top-variation,
    .banner-top-alt-variation,
    .banner-bottom-variation {
      .image-container {
        height: auto;
        text-align: center;
      }
      .image {
        max-height: 100%;
      }
    }
    .banner-overlay-variation {
      .promo-banner {
        min-width: 270px;
        padding: 0 16px 15px;
      }
      .image-container {
        height: auto;
      }
      .small_text_semibold,
      .small_text_black {
        line-height: 1;
        letter-spacing: 1px;
      }
      .small_text_black {
        display: inline-block;
      }
    }
  }

  &.brand-tcp.page-home .banner-overlay-variation {
    .promo-text.style10 .style10-0 {
      padding-right: 2px;
    }
    .col-2 {
      padding-left: 6px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      .promo-text.style10 .style10-3 {
        font-size: 16px;
        line-height: 22px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      .link-text .style1,
      .link-text .style2 {
        font-size: 16px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      .promo-text.style10 .style10-0 {
        padding-right: 4px;
      }
      .promo-text.style10 .style10-2 {
        font-size: 62px;
      }
      .promo-text.style10 .style10-3 {
        font-size: 24px;
        margin-top: 6px;
      }
    }
  }

  &.brand-tcp.page-home .banner-top-variation {
    .small_text_semibold {
      letter-spacing: normal;
    }
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      .image-container {
        height: auto;
      }
    }
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      .style10-0 {
        margin-top: -4px;
      }
      .style10-1 {
        font-size: 62px;
      }
      .style10-2 {
        font-size: 28px;
      }
      .style10-3 {
        font-size: 16px;
        margin-left: 5px;
        margin-top: 10px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      .col-2 {
        margin-left: 4px;
      }
      .style10-3 {
        margin-top: 10px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      .small_text_semibold {
        margin-top: auto;
      }
      .promo-banner {
        padding-bottom: 48px;
      }
      .promo-text.style10 .style10-0 {
        font-size: 36px;
      }
      .promo-text.style10 .style10-1 {
        font-size: 88px;
      }
      .promo-text.style10 .style10-2 {
        font-size: 48px;
      }
      .promo-text.style10 .style10-3 {
        font-size: 24px;
      }
    }
  }
  &.brand-tcp.page-home .promo-text.percentage_all_wrapped_normal_tab {
    margin-left: 0;
    .percentage_all_wrapped_normal_tab-2 {
      bottom: 19px;
      font-size: 22px;
    }
    .percentage_all_wrapped_normal_tab-1 {
      font-size: 45px;
      top: 16px;
    }
    .percentage_all_wrapped_normal_tab-0 {
      font-size: 82px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-left: 40px;
      .percentage_all_wrapped_normal_tab-0 {
        font-size: 114px;
      }
      .percentage_all_wrapped_normal_tab-1 {
        font-size: 65px;
        top: 20px;
      }
      .percentage_all_wrapped_normal_tab-2 {
        bottom: 27px;
        font-size: 30px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      display: block;
      margin-left: -15px;
      .percentage_all_wrapped_normal_tab-0 {
        font-size: 88px;
      }
      .percentage_all_wrapped_normal_tab-1 {
        font-size: 44px;
        top: 15px;
      }
      .percentage_all_wrapped_normal_tab-2 {
        bottom: 22px;
        font-size: 28px;
      }
    }
  }
`;
