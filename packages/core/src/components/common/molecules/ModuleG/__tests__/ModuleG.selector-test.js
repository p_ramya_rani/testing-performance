import { getLabel } from '../container/ModuleG.selector';

describe('#ModuleG selector', () => {
  const state = {
    Labels: {
      global: {
        modules: {
          lbl_moduleG_add_to_bag: 'lbl_moduleG_add_to_bag',
        },
      },
    },
  };

  it('Module G selector label', () => {
    expect(getLabel(state)).toEqual('lbl_moduleG_add_to_bag');
  });
});
