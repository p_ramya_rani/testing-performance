// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { openQuickViewWithValues } from '../../../organisms/QuickViewModal/container/QuickViewModal.actions';
import { getLabel } from './ModuleG.selector';

import ModuleG from '../views';

export const mapStateToProps = (state, ownProps) => {
  const { ProductTabList } = state;
  const { moduleClassName = '', icidParam = '' } = ownProps;

  return {
    productTabList: ProductTabList,
    addtoBagLabel: getLabel(state),
    moduleClassName,
    icidParam,
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    onQuickViewOpenClick: payload => {
      dispatch(openQuickViewWithValues(payload));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModuleG);
