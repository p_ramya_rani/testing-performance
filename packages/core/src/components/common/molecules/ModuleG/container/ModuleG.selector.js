// 9fbef606107a605d69c0edbcd8029e5d 
export const getLabel = state => {
  const { global: { modules } = {} } = state.Labels;
  let content;
  if (modules) {
    content = modules.lbl_moduleG_add_to_bag || '';
  }
  return content;
};

export default {
  getLabel,
};
