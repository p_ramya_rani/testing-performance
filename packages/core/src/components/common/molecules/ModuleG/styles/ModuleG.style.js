// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { Skeleton } from '../../../atoms';

export default css`
  .wrapper {
    position: relative;
    overflow: hidden;
  }

  .moduleG__carousel-wrapper {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};

    .slick-list {
      margin-right: -22%;
      box-sizing: border-box;
      width: 100%;

      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-right: -15%;
      }

      @media ${(props) => props.theme.mediaQuery.large} {
        margin-right: auto;
      }
    }
  }

  .focusAreaView {
    pointer-events: none;
    box-sizing: border-box;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    box-shadow: rgba(0, 0, 0, 0.1) 0 2px 10px 2px;
    z-index: 1;
    height: calc(100% - 32px);
    padding: 12px 10px;
    width: 60%;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 21.1%;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 15%;
    }
    &::before {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      width: calc(100% - 24px);
      height: 1px;
      background: ${(props) => props.theme.colors.BLACK};
      @media ${(props) => props.theme.mediaQuery.medium} {
        width: calc(100% - 16px);
      }
      @media ${(props) => props.theme.mediaQuery.large} {
        width: calc(100% - 32px);
      }
    }
    .focusArea-plus {
      width: 33px;
      height: 34px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      z-index: 1;
      display: flex;
      align-items: center;
      justify-content: center;
      background: ${(props) => props.theme.colors.WHITE};
      border-radius: 50%;
      border: 0.5px solid #000;
      padding: 7px;
      box-sizing: border-box;
      img {
        height: 17px;
      }
    }
  }

  .moduleG_carousel_img {
    width: 100%;
    object-fit: contain;
  }

  .image-link {
    display: flex;
    box-sizing: border-box;
    justify-content: center;
    margin: 0 auto;
    max-width: calc(100% - 15px);
    @media ${(props) => props.theme.mediaQuery.medium} {
      max-width: calc(100% - 27px);
      padding: 9px 7px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      max-width: calc(100% - 15px);
      padding: 10px 8px;
    }
    @media ${(props) => props.theme.mediaQuery.xlarge} {
      max-width: calc(100% - 25px);
      padding: 12px 10px;
    }
  }
  .carousel-bottom-link {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    text-align: center;
  }

  .promo-header a:nth-child(1) {
    > .link-text {
      margin-bottom: 0;
    }
  }

  &.brand-tcp.page-home .wrapper {
    margin-top: 6px;
  }

  .shopall_footerlink {
    font-size: ${(props) => props.theme.fonts.fontSize.promo1.small}px;
  }

  .right_chevron_arrow {
    margin-left: 9px;
  }

  .promo-header a:nth-child(2) {
    > .link-text {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }

  &.brand-tcp.page-home .promo-header a:nth-child(1) {
    > .link-text {
      margin-bottom: -18px;
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        margin-bottom: -10px;
      }
    }
  }

  &.brand-tcp.page-home .percentage_all_wrapped_normal_tab {
    font-family: TofinoWide;
  }

  &.brand-tcp.page-home .link-text .fixed_medium_text_black {
    font-family: TofinoWide;
    font-weight: 500;
    font-size: 20px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: 38px;
    }
  }

  &.brand-tcp.page-home .link-text .extra_large_text_black {
    font-family: TofinoWide;
    font-size: 62px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: 88px;
    }
  }
`;

export const StyledSkeleton = styled(Skeleton)`
  justify-content: center;
  margin-bottom: 10px;
  @media ${(props) => props.theme.mediaQuery.large} {
    .left-carousel {
      left: 10px;
    }
    .right-carousel {
      right: 10px;
    }
  }
  .skeleton-col {
    height: 200px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      &:nth-child(n + 4) {
        display: none;
      }
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: 185px;
      &:nth-child(n + 5) {
        display: none;
      }
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      height: 239px;
      &:nth-child(n + 6) {
        display: none;
      }
    }
  }
`;
