// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { storiesOf } from '@storybook/react';
import mockG from '@tcp/core/src/services/abstractors/common/moduleG/mock';
import ModuleG from '../container';

const productData = [
  {
    prodpartno: '2057021_IV',
    uniqueId: '2057021_IV',
    product_name: 'Toddler Boys Long Sleeve Blazer',
    seo_token: 'Toddler-Boys-Long-Sleeve-Blazer-2057021-IV',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2057021_IV.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Long-Sleeve-Blazer-2057021-IV',
    pdpAsPath: '/p/Toddler-Boys-Long-Sleeve-Blazer-2057021-IV',
  },
  {
    uniqueId: '2046341_1137',
    product_name: 'Toddler Boys Long Sleeve Basic Polo',
    seo_token: 'Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-1137',
    prodpartno: '2046341_1137',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2046341_1137.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-1137',
    pdpAsPath: '/p/Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-1137',
  },
  {
    uniqueId: '2046341_10',
    product_name: 'Toddler Boys Long Sleeve Basic Polo',
    seo_token: 'Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-10',
    prodpartno: '2046341_10',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2046341_10.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-10',
    pdpAsPath: '/p/Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-10',
  },
  {
    uniqueId: '2046341_01',
    product_name: 'Toddler Boys Long Sleeve Basic Polo',
    seo_token: 'Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-01',
    prodpartno: '2046341_01',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2046341_01.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-01',
    pdpAsPath: '/p/Toddler-Boys-Long-Sleeve-Basic-Polo-2046341-01',
  },
  {
    uniqueId: '2045122_NN',
    product_name: 'Toddler Boys Long Sleeve Basic Tee',
    seo_token: 'Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-NN',
    prodpartno: '2045122_NN',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2045122_NN.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-NN',
    pdpAsPath: '/p/Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-NN',
  },
  {
    uniqueId: '2045122_10',
    product_name: 'Toddler Boys Long Sleeve Basic Tee',
    seo_token: 'Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-10',
    prodpartno: '2045122_10',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2045122_10.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-10',
    pdpAsPath: '/p/Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-10',
  },
  {
    uniqueId: '2045122_01',
    product_name: 'Toddler Boys Long Sleeve Basic Tee',
    seo_token: 'Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-01',
    prodpartno: '2045122_01',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2045122_01.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-01',
    pdpAsPath: '/p/Toddler-Boys--Long-Sleeve-Basic-Tee-2045122-01',
  },
  {
    uniqueId: '2045121_10',
    product_name: 'Toddler Boys Short Sleeve Basic Tee',
    seo_token: 'Toddler-Boys-Short-Sleeve-Basic-Tee-2045121-10',
    prodpartno: '2045121_10',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2045121_10.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Short-Sleeve-Basic-Tee-2045121-10',
    pdpAsPath: '/p/Toddler-Boys-Short-Sleeve-Basic-Tee-2045121-10',
  },
  {
    uniqueId: '2045121_01',
    product_name: 'Toddler Boys Short Sleeve Basic Tee',
    seo_token: 'Toddler-Boys-Short-Sleeve-Basic-Tee-2045121-01',
    prodpartno: '2045121_01',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2045121_01.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Short-Sleeve-Basic-Tee-2045121-01',
    pdpAsPath: '/p/Toddler-Boys-Short-Sleeve-Basic-Tee-2045121-01',
  },
  {
    uniqueId: '2045118_NN',
    product_name: 'Toddler Boys Long Sleeve Full-Zip Hooded Sweatshirt',
    seo_token: 'Toddler-Boys-Long-Sleeve-Full-Zip-Hooded-Sweatshirt-2045118-NN',
    prodpartno: '2045118_NN',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2045118_NN.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Long-Sleeve-Full-Zip-Hooded-Sweatshirt-2045118-NN',
    pdpAsPath: '/p/Toddler-Boys-Long-Sleeve-Full-Zip-Hooded-Sweatshirt-2045118-NN',
  },
  {
    uniqueId: '2045118_1137',
    product_name: 'Toddler Boys Long Sleeve Full-Zip Hooded Sweatshirt',
    seo_token: 'Toddler-Boys-Long-Sleeve-Full-Zip-Hooded-Sweatshirt-2045118-1137',
    prodpartno: '2045118_1137',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2045118_1137.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Long-Sleeve-Full-Zip-Hooded-Sweatshirt-2045118-1137',
    pdpAsPath: '/p/Toddler-Boys-Long-Sleeve-Full-Zip-Hooded-Sweatshirt-2045118-1137',
  },
  {
    uniqueId: '2044764_1028',
    product_name: 'Toddler Boys Short Sleeve Oxford Button-Down Shirt',
    seo_token: 'Toddler-Boys-Short-Sleeve-Oxford-Button-Down-Shirt-2044764-1028',
    prodpartno: '2044764_1028',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2044764_1028.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Short-Sleeve-Oxford-Button-Down-Shirt-2044764-1028',
    pdpAsPath: '/p/Toddler-Boys-Short-Sleeve-Oxford-Button-Down-Shirt-2044764-1028',
  },
  {
    uniqueId: '2044764_10',
    product_name: 'Toddler Boys Short Sleeve Oxford Button-Down Shirt',
    seo_token: 'Toddler-Boys-Short-Sleeve-Oxford-Button-Down-Shirt-2044764-10',
    prodpartno: '2044764_10',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2044764_10.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Short-Sleeve-Oxford-Button-Down-Shirt-2044764-10',
    pdpAsPath: '/p/Toddler-Boys-Short-Sleeve-Oxford-Button-Down-Shirt-2044764-10',
  },
  {
    uniqueId: '2000049_1137',
    product_name: 'Toddler Boys Short Sleeve Basic Polo',
    seo_token: 'Toddler-Boys-Short-Sleeve-Basic-Polo-2000049-1137',
    prodpartno: '2000049_1137',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2000049_1137.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Short-Sleeve-Basic-Polo-2000049-1137',
    pdpAsPath: '/p/Toddler-Boys-Short-Sleeve-Basic-Polo-2000049-1137',
  },
  {
    uniqueId: '2000049_01',
    product_name: 'Toddler Boys Short Sleeve Basic Polo',
    seo_token: 'Toddler-Boys-Short-Sleeve-Basic-Polo-2000049-01',
    prodpartno: '2000049_01',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2000049_01.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Short-Sleeve-Basic-Polo-2000049-01',
    pdpAsPath: '/p/Toddler-Boys-Short-Sleeve-Basic-Polo-2000049-01',
  },
  {
    uniqueId: '1125070_1028',
    product_name: 'Toddler Boys Long Sleeve Oxford Shirt',
    seo_token: 'Toddler-Boys-Long-Sleeve-Oxford-Shirt-1125070-1028',
    prodpartno: '1125070_1028',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/1125070_1028.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Long-Sleeve-Oxford-Shirt-1125070-1028',
    pdpAsPath: '/p/Toddler-Boys-Long-Sleeve-Oxford-Shirt-1125070-1028',
  },
  {
    uniqueId: '1125070_10',
    product_name: 'Toddler Boys Long Sleeve Oxford Shirt',
    seo_token: 'Toddler-Boys-Long-Sleeve-Oxford-Shirt-1125070-10',
    prodpartno: '1125070_10',
    imageUrl: [
      'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/1125070_10.jpg',
    ],
    pdpUrl: '/p?pid=Toddler-Boys-Long-Sleeve-Oxford-Shirt-1125070-10',
    pdpAsPath: '/p/Toddler-Boys-Long-Sleeve-Oxford-Shirt-1125070-10',
  },
];

const store = createStore(() => ({
  ProductTabList: {
    ['47503']: [...productData],
    ['47501']: [...productData],
    errors: {
      ['47503']: null,
      ['47501']: null,
    },
    completed: {
      ['47503']: false,
      ['47501']: false,
    },
  },
  Labels: { global: { modules: { lbl_moduleG_add_to_bag: 'Add to Bag' } } },
}));

storiesOf('ModuleG', module).add('Basic', () => {
  return (
    <Provider store={store}>
      <ModuleG {...mockG.moduleG.composites} />
    </Provider>
  );
});
