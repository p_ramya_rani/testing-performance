// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import ProductTabList from '../../organisms/ProductTabList';
import { Image, Anchor } from '../../atoms';
import { isAndroid } from '../../../../utils/utils.app';

const getDeviceShadow = () => {
  if (isAndroid()) {
    return `
    border-width: 0;
    z-index:-600;
    elevation: 4;
     `;
  }
  return `
  border-width: 1;
  background-color: transparent;
  `;
};

export const ShadowWrapper = styled.View`
  width: 100%;
  align-items: center;
  position: absolute;
  z-index: 600;
`;

export const Container = styled.View`
  width: 100%;
`;

export const ImageSlidesWrapper = styled.View`
  height: 204px;
`;

export const ImageSlideWrapper = styled.View`
  flex-direction: row;
`;

export const StyledImage = styled(Image)`
  /* stylelint-disable-next-line */
  resize-mode: contain;
`;

export const StyledAnchor = styled(Anchor)`
  padding: 16px 10px;
`;

export const ImageItemWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ButtonContainer = styled.View`
  align-items: center;
  margin-top: ${props => props.theme.spacing.LAYOUT_SPACING.XS};
`;

export const Wrapper = styled.View`
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-bottom: ${props => (props.theme.isGymboree ? '10px' : '0')};
`;

export const PromoContainer = styled.View`
  margin-top: ${props => (props.theme.isGymboree ? props.theme.spacing.ELEM_SPACING.XXS : '0')};
`;

export const SecondHeaderContainer = styled.View`
  ${props =>
    props.layout === 'alt' ? `margin-bottom: ${props.theme.spacing.ELEM_SPACING.SM};` : ''};
`;

export const ImageContainer = styled.View`
  margin-top: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
  ${props => (props.layout === 'alt' ? `display:none ` : ``)};
`;

export const MessageContainer = styled.View`
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const ProductTabListContainer = styled.View`
  margin-top: ${props => (props.theme.isGymboree ? props.theme.spacing.LAYOUT_SPACING.XS : '10px')};
  ${props =>
    props.tabCount > 1 ? `margin-bottom: ${props.theme.spacing.LAYOUT_SPACING.MED};` : ''};
`;

export const MiddleContainer = styled.View`
  margin-left: 105px;
  margin-right: 105px;
  margin-top: 10px;
  margin-bottom: 10px;
  align-items: center;
`;

export const Border = styled.View`
  width: 100%;
  height: 0.5px;
  border: 0.5px solid ${props => props.theme.colorPalette.black};
  position: absolute;
  margin-top: 20px;
`;

export const Circle = styled.View`
  width: 40px;
  height: 40px;
  border: 1px solid ${props => props.theme.colorPalette.gray['900']};
  background: ${props => props.theme.colorPalette.white};
  border-radius: 20;
`;

export const StyledCustomImage = styled(Image)`
  width: 22px;
  height: 22px;
  position: absolute;
  margin: 9px 10px 10px 10px;
`;

export const StyledProductTabList = styled(ProductTabList)`
  margin-top: ${props => props.theme.spacing.LAYOUT_SPACING.XS};
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.MED};
`;

/* stylelint-disable-next-line */
export const SHADOW = styled.View`
  /* stylelint-disable-next-line */
  shadow-opacity: 4;
  /* stylelint-disable-next-line */
  shadow-radius: 4px;
  /* stylelint-disable-next-line */
  shadow-color: ${props => props.theme.colorPalette.black};
  /* stylelint-disable-next-line */
  shadow-offset: 1px 1px;
  width: ${props => (props.isHpNewLayoutEnabled ? '48%' : '55%')};
  height: 488px;
  position: absolute;
  justify-content: center;
  border-color: ${props => props.theme.colorPalette.white};
  margin-top: -12px;
  ${getDeviceShadow};
`;

export const ShadowContainer = styled.View`
  width: 100%;
  align-items: center;
`;

export default {
  Container,
  ImageItemWrapper,
  ButtonContainer,
  ImageSlidesWrapper,
  ImageSlideWrapper,
  StyledImage,
  PromoContainer,
  ImageContainer,
  MessageContainer,
  ProductTabListContainer,
  Border,
  Wrapper,
  StyledAnchor,
  MiddleContainer,
  Circle,
  StyledCustomImage,
  SHADOW,
  ShadowContainer,
  StyledProductTabList,
  ShadowWrapper,
};
