// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { View, Dimensions } from 'react-native';

import { ImageSlideWrapper } from '../ModuleG.style.native';
import { getLocator, getProductUrlForDAM, goToPlpTab } from '../../../../../utils/index.native';
import moduleGConfig from '../moduleG.config';
import { Anchor, DamImage } from '../../../atoms';

const PRODUCT_IMAGE_WIDTH = 164;
const PRODUCT_IMAGE_HEIGHT = 204;
const { width: screenWidth } = Dimensions.get('window');

const CarouselSlideCommon = props => {
  const {
    navigation,
    item: { item: itemValue },
    visible,
    icidParam,
  } = props;
  const { productItemIndex, product_name: productName, uniqueId } = itemValue;
  return (
    <ImageSlideWrapper accessible accessibilityLabel={productName} accessibilityRole="link">
      <View style={{ width: screenWidth, height: PRODUCT_IMAGE_HEIGHT }}>
        <Anchor
          onPress={() => {
            goToPlpTab(navigation);
            return navigation.navigate('ProductDetail', {
              title: productName,
              pdpUrl: uniqueId,
              selectedColorProductId: uniqueId,
              reset: true,
              internalCampaign: icidParam,
            });
          }}
          navigation={navigation}
          testID={`${getLocator('moduleG_product_image')}${productItemIndex}`}
        >
          <DamImage
            url={visible && uniqueId && getProductUrlForDAM(uniqueId)}
            height={PRODUCT_IMAGE_HEIGHT}
            width={PRODUCT_IMAGE_WIDTH}
            alt={productName}
            imgConfig={moduleGConfig.IMG_DATA.productImgConfig[0]}
            isProductImage
            isFastImage
            resizeMode="stretch"
            isHomePage
          />
        </Anchor>
      </View>
    </ImageSlideWrapper>
  );
};
CarouselSlideCommon.defaultProps = {
  icidParam: '',
};

CarouselSlideCommon.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  item: PropTypes.shape({}).isRequired,
  visible: PropTypes.bool.isRequired,
  icidParam: PropTypes.string,
};

export default CarouselSlideCommon;
