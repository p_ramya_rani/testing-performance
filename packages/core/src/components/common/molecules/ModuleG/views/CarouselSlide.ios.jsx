// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import CarouselSlideCommon from './CarouselSlideCommon.native';

class CarouselSlide extends PureComponent {
  render() {
    return <CarouselSlideCommon {...this.props} visible />;
  }
}

export default CarouselSlide;
