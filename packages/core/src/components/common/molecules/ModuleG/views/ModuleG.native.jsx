/* eslint-disable max-lines */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
// 9fbef606107a605d69c0edbcd8029e5d

import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Button, Skeleton } from '../../../atoms';
import {
  getLocator,
  getScreenWidth,
  styleOverrideEngine,
  isGymboree,
  getMediaBorderRadius,
  getHeaderBorderRadius,
} from '../../../../../utils/index.native';
import moduleGConfig from '../moduleG.config';
import { Carousel } from '../..';
import CarouselSlide from './CarouselSlide';
import {
  Container,
  ImageSlidesWrapper,
  ButtonContainer,
  PromoContainer,
  MessageContainer,
  Wrapper,
  StyledAnchor,
  MiddleContainer,
  Border,
  Circle,
  StyledCustomImage,
  SHADOW,
  ShadowContainer,
  ProductTabListContainer,
  ShadowWrapper,
} from '../ModuleG.style.native';
import ProductTabList from '../../../organisms/ProductTabList';
import PromoBanner from '../../PromoBanner';
import LinkText from '../../LinkText';
import colors from '../../../../../../styles/themes/TCP/colors';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';

const { MINIMUM_IMAGES } = moduleGConfig;
const MODULE_HEIGHT = 204;
const MODULE_WIDTH = getScreenWidth() + 41;
const TOTAL_IMAGES = 15;
const LOOP_CLONES_PER_SIDE = 7;
const INACTIVE_SLIDE_SCALE = 1;
const INACTIVE_SLIDE_OPACITY = 0.8;
const ITEM_WIDTH = 204;
const plusIcon = require('../../../../../../src/assets/plus.png');

const IS_SCROLLVIEW = true;

/**
 * @param {object} props : Props for Module G multi type of banner list, button list, header text.
 * @desc This is Module G global component. It has capability to display
 * featured content module with links and a CTA Button list.
 * Author can surface teaser content leading to corresponding pages.
 */
// TODO: keys will be changed once we get the actual data from CMS
class ModuleG extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedCategoryId: [],
      currentIndexFirstCarousel: 0,
      currentIndexSecondCarousel: 0,
    };
    this.updateFirstCurrentIndex = this.updateFirstCurrentIndex.bind(this);
    this.updateSecondCurrentIndex = this.updateSecondCurrentIndex.bind(this);
  }

  onProductTabChange = catId => {
    this.setState({
      selectedCategoryId: catId,
    });
  };

  getImagesData = () => {
    const { selectedCategoryId } = this.state;
    const { productTabList } = this.props;
    let data = [];
    data = selectedCategoryId.map(item => [...data, ...(productTabList[item] || [])]);
    data = data.slice(0, TOTAL_IMAGES);
    if (Object.keys(productTabList).length) {
      return data;
    }
    return [];
  };

  getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  onAddToBagClick = () => {
    const { onQuickViewOpenClick } = this.props;
    const { currentIndexFirstCarousel, currentIndexSecondCarousel } = this.state;
    const data = this.getImagesData();
    if (data && data.length === 2)
      onQuickViewOpenClick([
        {
          colorProductId:
            data[0][currentIndexFirstCarousel] && data[0][currentIndexFirstCarousel].prodpartno,
        },
        {
          colorProductId:
            data[1][currentIndexSecondCarousel] && data[1][currentIndexSecondCarousel].prodpartno,
        },
      ]);
  };

  /**
   * @param {object} props : Props for renderCarouselSlide accept slideProps & parallaxProps and render the image sliding view.
   */
  renderCarouselSlide = item => {
    if (!item) {
      return null;
    }
    const { navigation, icidParam } = this.props;
    return <CarouselSlide navigation={navigation} item={item} icidParam={icidParam} />;
  };

  getDataStatus = selectedCategoryId => {
    const { productTabList = {} } = this.props;
    let dataStatus = true;
    if (productTabList && productTabList.completed) {
      dataStatus = productTabList.completed[selectedCategoryId];
    }
    return dataStatus;
  };

  getMiddleContainer = (dataLength, type) => {
    const { isHpNewLayoutEnabled } = this.props;
    if (!dataLength) {
      return null;
    }
    if (type === 'shadow') {
      return (
        <ShadowContainer>
          <SHADOW isHpNewLayoutEnabled={isHpNewLayoutEnabled} />
        </ShadowContainer>
      );
    }
    return (
      <MiddleContainer>
        <Border />
        <Circle />
        <StyledCustomImage source={plusIcon} />
      </MiddleContainer>
    );
  };

  renderHeader = (headerText, promoBanner, navigation, headerStyle, promoStyle) => {
    return (
      <MessageContainer>
        <Wrapper>
          {headerText && headerText.length > 0 && (
            <LinkText
              navigation={navigation}
              headerText={headerText}
              locator={getLocator('moduleG_header_text')}
              useStyle
              headerStyle={headerStyle}
            />
          )}
        </Wrapper>
        {promoBanner && (
          <PromoContainer>
            <PromoBanner
              testID={getLocator('moduleG_promobanner_text')}
              promoBanner={promoBanner}
              navigation={navigation}
              promoStyle={promoStyle}
              showTextUnderLine
              underLineStyle={
                isGymboree()
                  ? {
                      borderBottomColor: 'orange',
                      borderBottomWidth: 2,
                    }
                  : {}
              }
            />
          </PromoContainer>
        )}
      </MessageContainer>
    );
  };

  renderFirstCarousel = (isSkeletonHidden, firstCarouseProductList) => {
    const { isHpNewLayoutEnabled } = this.props;
    return isSkeletonHidden && firstCarouseProductList && firstCarouseProductList.length > 0 ? (
      <ImageSlidesWrapper>
        <View>
          <Carousel
            data={firstCarouseProductList}
            renderItem={this.renderCarouselSlide}
            height={MODULE_HEIGHT}
            options={{
              loopClonesPerSide: LOOP_CLONES_PER_SIDE,
              inactiveSlideScale: INACTIVE_SLIDE_SCALE,
              inactiveSlideOpacity: INACTIVE_SLIDE_OPACITY,
              sliderWidth: isHpNewLayoutEnabled
                ? MODULE_WIDTH - 41 - 2 * HP_NEW_LAYOUT.BODY_PADDING
                : MODULE_WIDTH,
              itemWidth: isHpNewLayoutEnabled
                ? ITEM_WIDTH - 2 * HP_NEW_LAYOUT.BODY_PADDING - 5
                : ITEM_WIDTH,
              autoplay: false,
            }}
            paginationProps={{
              containerStyle: { paddingVertical: 5 },
            }}
            onSnapToItem={this.updateFirstCurrentIndex}
            isUseScrollView={IS_SCROLLVIEW}
            isModule
          />
        </View>
      </ImageSlidesWrapper>
    ) : null;
  };

  renderSecondCarousel = (isSkeletonHidden, secondCarouseProductList) => {
    const { isHpNewLayoutEnabled } = this.props;
    return isSkeletonHidden && secondCarouseProductList && secondCarouseProductList.length ? (
      <ImageSlidesWrapper>
        <View>
          <Carousel
            data={secondCarouseProductList}
            renderItem={this.renderCarouselSlide}
            height={MODULE_HEIGHT}
            options={{
              loopClonesPerSide: LOOP_CLONES_PER_SIDE,
              inactiveSlideScale: INACTIVE_SLIDE_SCALE,
              inactiveSlideOpacity: INACTIVE_SLIDE_OPACITY,
              sliderWidth: isHpNewLayoutEnabled
                ? MODULE_WIDTH - 41 - 2 * HP_NEW_LAYOUT.BODY_PADDING
                : MODULE_WIDTH,
              itemWidth: isHpNewLayoutEnabled
                ? ITEM_WIDTH - 2 * HP_NEW_LAYOUT.BODY_PADDING - 5
                : ITEM_WIDTH,
              autoplay: false,
            }}
            paginationProps={{
              containerStyle: { paddingVertical: 5 },
            }}
            onSnapToItem={this.updateSecondCurrentIndex}
            isUseScrollView={IS_SCROLLVIEW}
            isModule
          />
        </View>
      </ImageSlidesWrapper>
    ) : null;
  };

  renderShadowContainer = showShadowContainer => {
    const { isHpNewLayoutEnabled } = this.props;
    return showShadowContainer ? (
      <ShadowWrapper pointerEvents="none">
        <SHADOW isHpNewLayoutEnabled={isHpNewLayoutEnabled} />
      </ShadowWrapper>
    ) : null;
  };

  renderMiddleContainer = (showShadowContainer, showPlusButton) => {
    return showShadowContainer ? (
      <MiddleContainer>
        <Border />
        <Circle />
        {showPlusButton ? <StyledCustomImage source={plusIcon} /> : null}
      </MiddleContainer>
    ) : null;
  };

  renderSkeleton = dataStatus => {
    return dataStatus ? (
      <Skeleton
        row={1}
        col={3}
        width={203}
        height={200}
        rowProps={{ justifyContent: 'center', marginTop: '20px', marginBottom: '20px' }}
        colProps={{ margin: '0 20px' }}
        showArrows
      />
    ) : null;
  };

  renderbuttonContainer = (productExists, selectedSingleCTAButton) => {
    const { navigation, addtoBagLabel } = this.props;
    return productExists ? (
      <ButtonContainer>
        <Button
          width="225px"
          text={addtoBagLabel}
          accessibilityLabel={addtoBagLabel}
          onPress={this.onAddToBagClick}
          navigation={navigation}
          testID={getLocator('moduleG_cta_btn')}
        />
        {selectedSingleCTAButton && Object.keys(selectedSingleCTAButton).length ? (
          <StyledAnchor
            anchorVariation="primary"
            accessibilityLabel={selectedSingleCTAButton.text}
            text={selectedSingleCTAButton.text}
            fontSizeVariation="xlarge"
            url={selectedSingleCTAButton.url}
            navigation={navigation}
            visible
          />
        ) : null}
      </ButtonContainer>
    ) : null;
  };

  updateFirstCurrentIndex = index => {
    this.setState({ currentIndexFirstCarousel: index });
  };

  updateSecondCurrentIndex = index => {
    this.setState({ currentIndexSecondCarousel: index });
  };

  renderCarouselView = (
    dataStatus,
    showPlusButton,
    firstCarouseProductList,
    secondCarouseProductList
  ) => {
    return (
      firstCarouseProductList &&
      firstCarouseProductList.length > MINIMUM_IMAGES &&
      secondCarouseProductList &&
      secondCarouseProductList.length > MINIMUM_IMAGES && (
        <View>
          {this.renderShadowContainer(!dataStatus)}
          {this.renderSkeleton(dataStatus)}
          {this.renderFirstCarousel(!dataStatus, firstCarouseProductList)}
          {this.renderMiddleContainer(!dataStatus, showPlusButton)}
          {this.renderSkeleton(dataStatus)}
          {this.renderSecondCarousel(!dataStatus, secondCarouseProductList)}
        </View>
      )
    );
  };

  getContainerStyle = isHpNewLayoutEnabled =>
    isHpNewLayoutEnabled && HP_NEW_LAYOUT.MODULE_BOX_SHADOW;

  /**
   * @param {object} props : Props for renderView multi type of banner list, button list, header text.
   * @desc This is Method return the complete View with CTA Button .
   */
  renderView = (
    selectedProductList,
    selectedSingleCTAButton = {},
    headerStyle,
    promoStyle,
    styleOverrides
  ) => {
    const {
      navigation,
      headerText,
      promoBanner,
      divTabs,
      productTabList = {},
      isHpNewLayoutEnabled,
    } = this.props;
    const firstCarouseProductList = selectedProductList[0];
    const secondCarouseProductList = selectedProductList[1];
    const { selectedCategoryId } = this.state;
    const dataStatus =
      this.getDataStatus(selectedCategoryId[0]) || this.getDataStatus(selectedCategoryId[1]);
    let showPlusButton = false;
    const productTabsCompletionInfo = Object.assign({}, productTabList);
    // eslint-disable-next-line
    if (productTabsCompletionInfo.hasOwnProperty('completed')) {
      selectedCategoryId.forEach(id => {
        // eslint-disable-next-line
        if (productTabsCompletionInfo.completed.hasOwnProperty(id)) {
          showPlusButton = true;
        } else {
          showPlusButton = false;
        }
      });
    }
    let productExists = false;
    if (selectedCategoryId.length) {
      selectedCategoryId.forEach(id => {
        if (!productExists) {
          productExists =
            Object.keys(productTabList).length > 2 &&
            productTabList[id] &&
            productTabList[id].length > 0;
        }
      });
    }
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const bgColorStyle = { backgroundColor: colors?.WHITE };
    const headerWrapperStyle = isHpNewLayoutEnabled
      ? {
          ...bgColorStyle,
          ...headerBorderRadiusOverride,
        }
      : {};
    const productTabListStyle = isHpNewLayoutEnabled ? { ...bgColorStyle } : {};
    const mediaWrapperStyle = isHpNewLayoutEnabled
      ? {
          ...bgColorStyle,
          ...mediaBorderRadiusOverride,
        }
      : {};
    return (
      <Container style={this.getContainerStyle(isHpNewLayoutEnabled)}>
        <MessageContainer style={headerWrapperStyle}>
          {this.renderHeader(headerText, promoBanner, navigation, headerStyle, promoStyle)}
        </MessageContainer>
        <View style={productTabListStyle}>
          <ProductTabListContainer tabCount={divTabs ? divTabs.length : 1}>
            <ProductTabList
              onProductTabChange={this.onProductTabChange}
              tabItems={divTabs}
              navigation={navigation}
              testID={getLocator('moduleG_cta_link')}
            />
          </ProductTabListContainer>
        </View>
        <View style={mediaWrapperStyle}>
          {this.renderCarouselView(
            dataStatus,
            showPlusButton,
            firstCarouseProductList,
            secondCarouseProductList
          )}
          {this.renderbuttonContainer(productExists, selectedSingleCTAButton)}
        </View>
      </Container>
    );
  };

  /**
   * @desc This is Method return the view with TAB List.
   */
  render() {
    const { selectedCategoryId = [] } = this.state;
    const { divTabs, productTabList = {}, moduleClassName } = this.props;
    const selectedProdLists = [];
    const styleOverrides = styleOverrideEngine(moduleClassName, 'Default');
    if (styleOverrides.title) {
      styleOverrides.title.fontSize = 35;
      styleOverrides.title.fontWeight = '600';
      styleOverrides.title.lineHeight = 60;
    }
    if (styleOverrides.subTitle) {
      styleOverrides.subTitle.lineHeight = 30;
    }

    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const promoStyle = styleOverrides.promo;

    selectedCategoryId.forEach((catId, index) => {
      const productList = productTabList[selectedCategoryId[index]] || [];
      selectedProdLists.push(productList.slice(0, TOTAL_IMAGES));
    });
    let selectedSingleCTAButton;
    const processedDivTabs = Object.assign([], divTabs);
    const tabs = {};
    processedDivTabs.forEach((tab, index) => {
      const tabList = tab.category.map(cat => cat.val);
      tabs[index] = tabList;
    });
    divTabs.forEach((tab, index) => {
      if (JSON.stringify(tabs[index]) === JSON.stringify(selectedCategoryId)) {
        selectedSingleCTAButton = tab.singleCTAButton;
      }
    });

    const defaultCats = divTabs.length && divTabs[0].category.map(divTab => divTab.val);
    const isDefaultCategoriesLoaded = defaultCats.every(category => {
      /*
        completed === false here means that the product data has been loaded. The name is confusing
        for some reason.
      */
      return productTabList.completed && productTabList.completed[category] === false;
    });

    if (isDefaultCategoriesLoaded) {
      // Render whole module only if the default categories meet following requirement
      const shouldRenderModule = defaultCats.every(category => {
        return productTabList[category].length > MINIMUM_IMAGES;
      });
      return (
        shouldRenderModule &&
        this.renderView(
          selectedProdLists,
          selectedSingleCTAButton,
          headerStyle,
          promoStyle,
          styleOverrides
        )
      );
    }

    // Don't render view and only load data to verify default category data check.
    // Because productTabList state is populated by <ProductTabList /> only.
    return <ProductTabList tabItems={[divTabs[0]]} hideView />;
  }
}

ModuleG.defaultProps = {
  promoBanner: [],
  addtoBagLabel: '',
  moduleClassName: '',
  icidParam: '',
};

ModuleG.propTypes = {
  onQuickViewOpenClick: PropTypes.func.isRequired,
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  productTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          uniqueId: PropTypes.string.isRequired,
          imageUrl: PropTypes.array.isRequired,
          seo_token: PropTypes.string,
        })
      )
    )
  ).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  divTabs: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.object,
      category: PropTypes.object,
      singleCTAButton: PropTypes.object,
    })
  ).isRequired,
  addtoBagLabel: PropTypes.string,
  moduleClassName: PropTypes.string,
  icidParam: PropTypes.string,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleG;
export { ModuleG as ModuleGVanilla };
