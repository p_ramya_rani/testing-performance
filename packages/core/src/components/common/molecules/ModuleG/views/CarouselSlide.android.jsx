// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InViewPort from '@tcp/core/src/components/common/atoms/InViewPort';
import CarouselSlideCommon from './CarouselSlideCommon.native';

class CarouselSlide extends PureComponent {
  static propTypes = {
    navigation: PropTypes.shape({}).isRequired,
    item: PropTypes.shape({}).isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  checkVisible = isVisible => {
    const { visible } = this.state;
    if (isVisible) {
      if (!visible) {
        this.setState({ visible: true });
      }
    } else if (visible) {
      this.setState({ visible: false });
    }
  };

  render() {
    const { navigation } = this.props;
    const { visible } = this.state;
    return (
      <InViewPort
        onChange={isVisible => this.checkVisible(isVisible)}
        delay={300}
        navigation={navigation}
      >
        <CarouselSlideCommon {...this.props} visible={visible} />
      </InViewPort>
    );
  }
}

export default CarouselSlide;
