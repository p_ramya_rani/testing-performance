// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import Grid from '@tcp/core/src/components/common/molecules/Grid';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/views/Carousel';
import { getHeaderBorderRadius, getMediaBorderRadius } from '@tcp/core/src/utils/utils.web';
import { Anchor, Button, Col, DamImage, Image, Row } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ProductTabList from '@tcp/core/src/components/common/organisms/ProductTabList';
import {
  // configureInternalNavigationFromCMSUrl,
  getIconPath,
  getLocator,
  getProductUrlForDAM,
  configureInternalNavigationFromCMSUrl,
  styleOverrideEngine,
  mergeUrlQueryParams,
  getBrand,
} from '@tcp/core/src/utils';
import moduleGStyle, { StyledSkeleton } from '../styles/ModuleG.style';
import moduleGConfig from '../moduleG.config';

const { CAROUSEL_OPTIONS, TOTAL_IMAGES, MINIMUM_IMAGES } = moduleGConfig;
const firstCarouselOption = { ...CAROUSEL_OPTIONS };
const secondCarouselOption = { ...CAROUSEL_OPTIONS };

class ModuleG extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentCatId: [],
      firstCarouselNext: 0,
      secondCarouselNext: 0,
    };
  }

  getHeaderBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  onTabChange = (catId) => {
    this.setState({ currentCatId: catId });
  };

  getImagesData = () => {
    const { currentCatId } = this.state;
    const { productTabList } = this.props;
    let data = [];
    data = currentCatId.map((item) => [...data, ...(productTabList[item] || [])]);
    data = data.slice(0, TOTAL_IMAGES);
    if (Object.keys(productTabList).length) {
      return data;
    }
    return [];
  };

  onAddToBagClick = () => {
    const { onQuickViewOpenClick } = this.props;
    const { firstCarouselNext, secondCarouselNext } = this.state;
    const data = this.getImagesData();
    onQuickViewOpenClick([
      {
        colorProductId: data.length && data[0][firstCarouselNext].prodpartno,
      },
      {
        colorProductId: data.length && data[1][secondCarouselNext].prodpartno,
      },
    ]);
  };

  getCurrentCtaButton = () => {
    const { currentCatId } = this.state;
    const { divTabs, productTabList, addtoBagLabel, isHpNewDesignCTAEnabled } = this.props;
    let currentSingleCTAButton = {};

    const processedDivTabs = Object.assign([], divTabs);
    const tabs = {};
    processedDivTabs.forEach((tab, index) => {
      const tabList = tab.category.map((cat) => cat.val);
      tabs[index] = tabList;
    });

    divTabs.forEach((tab, index) => {
      if (JSON.stringify(tabs[index]) === JSON.stringify(currentCatId)) {
        currentSingleCTAButton = tab.singleCTAButton;
      }
    });

    currentSingleCTAButton = currentSingleCTAButton || {};

    let productExists = false;
    if (currentCatId.length) {
      currentCatId.forEach((id) => {
        if (!productExists) {
          productExists =
            Object.keys(productTabList).length > 2 &&
            productTabList[id] &&
            productTabList[id].length > 0;
        }
      });
    }
    return productExists ? (
      <>
        <Row centered>
          <Col
            colSize={{
              small: 4,
              medium: 2,
              large: 2,
            }}
          >
            <Button
              onClick={() => this.onAddToBagClick()}
              buttonVariation="fixed-width"
              className="cta-btn"
              dataLocator={getLocator('moduleG_add_to_bag_btn')}
              isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
            >
              {addtoBagLabel}
            </Button>
          </Col>
        </Row>
        {currentSingleCTAButton &&
        Object.keys(currentSingleCTAButton).length &&
        currentSingleCTAButton.text ? (
          <Row centered>
            <Col
              colSize={{
                small: 6,
                medium: 8,
                large: 12,
              }}
              className="carousel-bottom-link"
            >
              <Anchor
                to={configureInternalNavigationFromCMSUrl(currentSingleCTAButton.url)}
                target={currentSingleCTAButton.target}
                title={currentSingleCTAButton.title}
                asPath={currentSingleCTAButton.url}
                dataLocator={getLocator('moduleG_shop_all_link')}
              >
                <span className="shopall_footerlink">{currentSingleCTAButton.text}</span>
                <span className="right_chevron_arrow" aria-hidden>
                  {/* Empty alt and Aria-Hidden. Previous Span contain image desc. Screen reader users will have the burden of redundant content announced. */}
                  <Image src={getIconPath('smallright')} alt="" />
                </span>
              </Anchor>
            </Col>
          </Row>
        ) : null}
      </>
    ) : null;
  };

  getHeaderText = (headerStyle) => {
    const { headerText, layout } = this.props;
    return headerText && layout !== 'alt' ? (
      <div className="promo-header-wrapper">
        <LinkText
          component="h2"
          type="heading"
          headerText={headerText}
          className="promo-header"
          dataLocator={getLocator('moduleG_header_text')}
          headerStyle={headerStyle}
        />
      </div>
    ) : (
      <LinkText
        component="div"
        headerText={headerText}
        className="promo-header"
        dataLocator={getLocator('moduleG_header_text')}
        headerStyle={headerStyle}
      />
    );
  };

  getPromoBanner = (promoStyle) => {
    const { promoBanner } = this.props;
    return (
      promoBanner && (
        <PromoBanner
          promoBanner={promoBanner}
          className="promoBanner"
          dataLocator={getLocator('moduleG_promobanner_text')}
          promoStyle={promoStyle}
        />
      )
    );
  };

  renderCarousel = (type, currentCatId) => {
    const { productTabList, icidParam, isHomePage } = this.props;
    let data = productTabList[currentCatId] || [];
    data = data.slice(0, TOTAL_IMAGES);
    let dataStatus = true;
    if (productTabList && productTabList.completed) {
      dataStatus = productTabList.completed[currentCatId];
    }
    firstCarouselOption.beforeChange = (current, next) => {
      this.setState({ firstCarouselNext: next });
    };
    secondCarouselOption.beforeChange = (current, next) => {
      this.setState({ secondCarouselNext: next });
    };
    const carouselOption = type === 'top' ? firstCarouselOption : secondCarouselOption;
    if (dataStatus) {
      return (
        <StyledSkeleton
          col={6}
          colSize={{ small: 2, medium: 2, large: 2 }}
          showArrows
          removeLastMargin
        />
      );
    }
    if (data.length > 0) {
      return (
        <Col
          className={`moduleG__carousel-wrapper moduleG__carousel-${type}`}
          colSize={{
            small: 6,
            medium: 8,
            large: 10,
          }}
          offsetLeft={{
            small: 0,
            medium: 0,
            large: 1,
          }}
          offsetRight={{
            small: 0,
            medium: 0,
            large: 1,
          }}
        >
          {data.length > 0 ? (
            <Carousel
              options={carouselOption}
              carouselConfig={{
                autoplay: false,
                variation: 'big-arrows',
                customArrowLeft: getIconPath('carousel-big-carrot'),
                customArrowRight: getIconPath('carousel-big-carrot'),
              }}
              isModule
            >
              {data.map(({ pdpUrl, pdpAsPath, product_name: productName, uniqueId }, index) => {
                const pdpAsPathWithTrackingParams = mergeUrlQueryParams(pdpAsPath, icidParam);

                const newPdpUrl = pdpUrl && `${pdpUrl}&dataSource=module_G_${currentCatId}`;
                return (
                  <div key={index.toString()}>
                    <Anchor
                      className="image-link"
                      to={newPdpUrl}
                      asPath={pdpAsPathWithTrackingParams}
                      dataLocator={`${getLocator('moduleG_image')}${index}`}
                    >
                      <DamImage
                        imgData={{ url: getProductUrlForDAM(uniqueId), alt: productName }}
                        imgConfigs={moduleGConfig.IMG_DATA.productImgConfig}
                        isProductImage
                        className="moduleG_carousel_img"
                        isHomePage={isHomePage}
                        isModule
                      />
                    </Anchor>
                  </div>
                );
              })}
            </Carousel>
          ) : null}
        </Col>
      );
    }

    return null;
  };

  /**
   * @function - getModuleDisplayFlags
   *
   * @arguments - divTabs - contains tab formation details.
   *              productTabList - contains products referenced by tab and associated catIds.
   *
   */
  getModuleDisplayFlags = (divTabs = [], productTabList = {}) => {
    const defaultCats =
      divTabs.length &&
      divTabs[0].category.reduce((a, b) => {
        return a.concat(b.val);
      }, []);

    const catIdExists = defaultCats.every((cat) =>
      Object.prototype.hasOwnProperty.call(productTabList, cat)
    );

    const showModule = defaultCats.every(
      (cat) =>
        Object.prototype.hasOwnProperty.call(productTabList, cat) && productTabList[cat].length >= 7
    );

    return { catIdExists, showModule };
  };

  /**
   * @function - showPlusSeparator
   *
   * @arguments - currentCatId - currently selected category id.
   *              productTabList - contains products referenced by tab and associated catIds.
   *
   */
  showPlusSeparator = (currentCatId, productTabList = {}) => {
    let showPlusButton = false;
    if (productTabList && Object.prototype.hasOwnProperty.call(productTabList, 'completed')) {
      currentCatId.forEach((id) => {
        if (Object.prototype.hasOwnProperty.call(productTabList, id)) {
          showPlusButton = true;
        } else {
          showPlusButton = false;
        }
      });
    }
    return showPlusButton;
  };

  render() {
    const {
      className,
      productTabList = {},
      divTabs,
      moduleClassName,
      page,
      isHpNewModuleDesignEnabled,
    } = this.props;
    const { currentCatId } = this.state;
    const topCarouselData = productTabList[currentCatId[0]] || [];
    const bottomCarouselData = productTabList[currentCatId[1]] || [];
    const productTabsCompletionInfo = Object.assign({}, productTabList);
    const showCarousels =
      topCarouselData.length >= MINIMUM_IMAGES && bottomCarouselData.length >= MINIMUM_IMAGES;

    // Fetches the flags to either show or hide module G content and carousels.
    const { catIdExists, showModule } = this.getModuleDisplayFlags(
      divTabs,
      productTabsCompletionInfo
    );
    const showPlusButton = this.showPlusSeparator(currentCatId, productTabsCompletionInfo);
    const styleOverrides = styleOverrideEngine(moduleClassName, 'Default');
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const promoStyle = styleOverrides.promo;
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );

    return (
      <React.Fragment>
        <Grid
          className={`${className} ${moduleClassName} moduleG brand-${getBrand()} page-${page}`}
        >
          <Row>
            <Col
              colSize={{
                small: 6,
                medium: 8,
                large: 12,
              }}
              style={headerBorderRadiusOverride}
            >
              {showModule ? (
                <React.Fragment>
                  {this.getHeaderText(headerStyle)}
                  {this.getPromoBanner(promoStyle)}
                </React.Fragment>
              ) : null}
              {!catIdExists || showModule ? (
                <ProductTabList
                  onProductTabChange={this.onTabChange}
                  tabItems={divTabs}
                  dataLocator={getLocator('moduleG_cta_link')}
                />
              ) : null}
            </Col>
          </Row>
          {showCarousels && (
            <Row
              className="wrapper"
              fullBleed={{ small: true, medium: true, large: false }}
              customStyle={mediaBorderRadiusOverride}
            >
              {this.renderCarousel('top', currentCatId[0])}

              <div className="focusAreaView">
                <span className="focusArea-plus" aria-hidden>
                  {showPlusButton ? <Image src={getIconPath('plus-icon-thick')} alt="" /> : null}
                </span>
              </div>
              {/* carousel bottom */}
              {this.renderCarousel('bottom', currentCatId[1])}
            </Row>
          )}
          {showModule && this.getCurrentCtaButton()}
        </Grid>
      </React.Fragment>
    );
  }
}

ModuleG.defaultProps = {
  productTabList: {},
  divTabs: [],
  promoBanner: [],
  layout: 'default',
  moduleClassName: '',
  addtoBagLabel: '',
  icidParam: '',
  page: '',
};

ModuleG.propTypes = {
  className: PropTypes.string.isRequired,
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  onQuickViewOpenClick: PropTypes.func.isRequired,
  productTabList: PropTypes.shape({}),
  layout: PropTypes.string.isRequired,
  divTabs: PropTypes.arrayOf(PropTypes.object),
  moduleClassName: PropTypes.string,
  addtoBagLabel: PropTypes.string,
  icidParam: PropTypes.string,
  page: PropTypes.string,
  isHomePage: PropTypes.bool.isRequired,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

const styledModuleG = withStyles(errorBoundary(ModuleG), moduleGStyle);
styledModuleG.defaultProps = ModuleG.defaultProps;
export default styledModuleG;
export { ModuleG as ModuleGVanilla };
