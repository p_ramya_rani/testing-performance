// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable no-console */
/* eslint-disable react-native/split-platform-components */
/* eslint-disable no-unused-vars */
import React from 'react';
import { Platform, Linking, PermissionsAndroid } from 'react-native';
import { getAkamaiSensorData } from '@tcp/core/src/utils';
import { check, PERMISSIONS } from 'react-native-permissions';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import Geolocation from 'react-native-geolocation-service';
import {
  getScreenWidth,
  getScreenHeight,
  setValueInAsyncStorage,
  isAndroid,
} from '@tcp/core/src/utils/utils.app';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { trackForterCurrentLocation } from '@tcp/core/src/utils/forter.util';

import ModalNative from '../../Modal/view/Modal.native';
import { getBrand } from '../../../../../utils/utils';
import {
  Wrapper,
  Container,
  StyledImage,
  Touchable,
  StyledBodyCopy,
  StyledButton,
  StyledAnchor,
  MessageContainer,
  ShadowContainer,
} from '../styles/LocationAccessPrompt.native';

const locationImage = require('../../../../../../src/assets/location.png');
const closeImage = require('../../../../../../src/assets/close.png');

/**
 * Module height and width.
 */
const PROPMT_WIDTH = getScreenWidth() - 60;
const HEIGHT = getScreenHeight();

/**
 * Module LOCATION_ACCESS_KEY & LOCATION_ACCESS_VALUE
 */
const LOCATION_ACCESS_KEY = 'location-access-key';
const LOCATION_ACCESS_VALUE = 'tcp_location-access-value';

const checkLocationPermission = Platform.select({
  ios: () => check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE),
  android: () => check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION),
});

class LocationAccessPrompt extends React.PureComponent {
  constructor() {
    super();
    this.state = { isModalOpen: false, modalActionComplete: false };
  }

  /**
   * @componentDidMount : In this finction to manage the userlogin or not.
   *
   * @isBrandSwitched : In componentDidMount to check the Brand change or not .
   */
  async componentDidMount() {
    const { isBrandSwitched, checkFirst } = this.props;
    if (!isBrandSwitched) {
      this.checkLocationAccess();
    }
    await checkFirst('HomeStack');
  }

  componentDidUpdate(oldProps, oldState) {
    const { isUserLoggedIn, onModalActionComplete, isBrandSwitched } = this.props;
    const { modalActionComplete } = this.state;
    const { isUserLoggedIn: oldIsUserLoggedIn } = oldProps;
    if (isUserLoggedIn !== oldIsUserLoggedIn && !isBrandSwitched) {
      this.checkLocationAccess();
    }

    if (modalActionComplete && oldState.modalActionComplete !== modalActionComplete) {
      onModalActionComplete();
    }
  }

  /**
   * @checkLocationAccess : To manage the location prompt visibility through State and AsyncStorage .
   */

  checkLocationAccess = () => {
    const { isUserLoggedIn } = this.props;
    if (isUserLoggedIn) {
      const latLong = readCookie('tcpGeoLoc');
      if (latLong) {
        latLong.then((res) => {
          if (res) {
            const [lat, long] = res.split('|');
            trackForterCurrentLocation(Number(long), Number(lat));
            this.setState({
              isModalOpen: false,
              modalActionComplete: true,
            });
          }
        });
      }
    } else {
      this.setState({ modalActionComplete: true });
    }
  };

  /**
   * @toggleModal : To manage the modal state .
   */
  toggleModal = async () => {
    const { isModalOpen } = this.state;
    this.setState({
      isModalOpen: !isModalOpen,
      modalActionComplete: !!isModalOpen,
    });
  };

  /**
   * @androidPermissions : To manage the android permissions .
   */
  androidPermissions = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message: '',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.getLocation();
        console.log('You can use the location');
      } else {
        console.log('Location permission denied');
      }
      this.setState({
        isModalOpen: false,
      });
    } catch (err) {
      this.setState({
        isModalOpen: false,
      });
    }
  };

  getLocation = () => {
    const { isLocationEnabled, setLocationFlag } = this.props;
    Geolocation.getCurrentPosition(
      (position) => {
        const { coords: { longitude, latitude } = {} } = position;
        if (!isLocationEnabled && setLocationFlag) {
          setLocationFlag(true);
          trackForterCurrentLocation(longitude, latitude);
        }
        this.close();
      },
      (error) => {
        Linking.openURL('app-settings:');
      },
      {
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
      }
    );
  };

  /**
   * @iosPermissions : To manage the ios permissions .
   */
  iosPermissions = () => {
    this.getLocation();
  };

  /**
   * @requestPermission : To manage location permission in android and ios .
   */
  requestPermission = () => {
    if (isAndroid()) {
      this.androidPermissions();
      this.close();
    } else {
      this.iosPermissions();
    }
  };

  /**
   * @requestPermission : To close the modal in android and ios .
   */
  close = () => {
    this.toggleModal();
    setValueInAsyncStorage(LOCATION_ACCESS_KEY, LOCATION_ACCESS_VALUE);
  };

  render() {
    const { labels } = this.props;
    const { isModalOpen } = this.state;

    return labels && Object.keys(labels).length ? (
      <ModalNative isOpen={isModalOpen} onRequestClose={this.toggleModal} customTransparent>
        <ShadowContainer height={HEIGHT}>
          <Container>
            <Wrapper width={PROPMT_WIDTH}>
              <StyledImage source={locationImage} width="35px" height="35px" marginTop="15px" />
              <Touchable accessibilityRole="button" onPress={this.toggleModal}>
                <StyledImage source={closeImage} width="15px" height="15px" />
              </Touchable>
              <MessageContainer>
                <StyledBodyCopy
                  text={getLabelValue(
                    labels,
                    'lbl_locationAccess_findItInStoreToday',
                    'StoreLanding',
                    'StoreLocator'
                  )}
                  textAlign="center"
                  fontWeight="black"
                  fontFamily="secondary"
                  fontSize="fs18"
                  marginTop="15px"
                />

                <StyledBodyCopy
                  text={getLabelValue(
                    labels,
                    'lbl_locationAccess_desc',
                    'StoreLanding',
                    'StoreLocator'
                  )}
                  textAlign="center"
                  fontWeight="black"
                  fontFamily="secondary"
                  fontSize="fs12"
                  marginTop="15px"
                />

                <StyledBodyCopy
                  text={getLabelValue(
                    labels,
                    'lbl_locationAccess_coolRight',
                    'StoreLanding',
                    'StoreLocator'
                  )}
                  textAlign="center"
                  fontWeight="black"
                  fontFamily="secondary"
                  fontSize="fs12"
                />

                <StyledButton
                  text={getLabelValue(
                    labels,
                    'lbl_locationAccess_turnOnLocation',
                    'StoreLanding',
                    'StoreLocator'
                  )}
                  fill="BLACK"
                  marginTop="12px"
                  width="260px"
                  onPress={() => this.requestPermission()}
                />

                <StyledAnchor
                  text={getLabelValue(
                    labels,
                    'lbl_locationAccess_maybelater',
                    'StoreLanding',
                    'StoreLocator'
                  )}
                  underline
                  fontSizeVariation="large"
                  marginTop="18px"
                  marginBottom="12px"
                  onPress={this.close}
                />
              </MessageContainer>
            </Wrapper>
          </Container>
        </ShadowContainer>
      </ModalNative>
    ) : null;
  }
}

LocationAccessPrompt.propTypes = {
  isUserLoggedIn: PropTypes.bool,
  isBrandSwitched: PropTypes.bool,
  isLocationEnabled: PropTypes.bool.isRequired,
  setLocationFlag: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  onModalActionComplete: PropTypes.func,
};

LocationAccessPrompt.defaultProps = {
  isUserLoggedIn: false,
  onModalActionComplete: () => {},
  isBrandSwitched: false,
};

export { LocationAccessPrompt as LocationAccessPromptVanilla };
export default LocationAccessPrompt;
