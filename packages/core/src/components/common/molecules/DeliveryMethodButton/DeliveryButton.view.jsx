import React from 'react';
import PropTypes from 'prop-types';
import PickupPromotionBanner from '@tcp/core/src/components/common/molecules/PickupPromotionBanner';
import BodyCopy from '../../atoms/BodyCopy';
import styles from '../DeliveryMethods/styles/DeliveryMethods.style';
import withStyles from '../../hoc/withStyles';

const DeliveryButton = (props) => {
  const {
    heading,
    subheading,
    badge,
    isPickUpSelected,
    value,
    pickupSelectionUpdate,
    buttonSize,
    checkForOOSForVariant,
    isAllSizeDisabled,
  } = props;

  const isDisableButton = checkForOOSForVariant || isAllSizeDisabled;

  return (
    <div
      className={`delivery-button ${isPickUpSelected && !isDisableButton ? 'selected' : ''}${
        isDisableButton ? ' disable-delivery-methods' : ''
      } ${value}  ${buttonSize}`}
      onClick={() => (!isDisableButton ? pickupSelectionUpdate(value) : null)}
      onKeyDown={() => (!isDisableButton ? pickupSelectionUpdate(value) : null)}
      role="button"
      tabIndex="0"
    >
      <BodyCopy
        fontWeight="extrabold"
        fontFamily="secondary"
        component="span"
        className="header-pickup-line"
        textAlign="center"
      >
        {heading}
      </BodyCopy>
      <BodyCopy
        fontFamily="secondary"
        fontWeight={`${isPickUpSelected && !checkForOOSForVariant ? 'extrabold' : 'regular'}`}
        textAlign="center"
        className={`sub-header-pickup ${subheading === 'Free shipping' ? 'text-Capitalize' : ''}${
          subheading === 'Unavailable' || subheading === 'lbl_unavlbl'
            ? 'unavailable-sub-header'
            : ''
        }`}
      >
        {subheading}
      </BodyCopy>
      {badge ? (
        <BodyCopy fontSize="fs12" fontFamily="secondary" className="badge-container">
          <BodyCopy component="span" className="badge" fontWeight="bold">
            <PickupPromotionBanner bossBanner isPickupMobilePromotion />
          </BodyCopy>
        </BodyCopy>
      ) : null}
    </div>
  );
};

DeliveryButton.propTypes = {
  heading: PropTypes.string,
  subheading: PropTypes.string,
  badge: PropTypes.string,
  isPickUpSelected: PropTypes.bool,
  value: PropTypes.string,
  pickupSelectionUpdate: PropTypes.func,
  isAllSizeDisabled: PropTypes.bool,
  checkForOOSForVariant: PropTypes.bool,
  buttonSize: PropTypes.string.isRequired,
  itemBrand: PropTypes.string,
};

DeliveryButton.defaultProps = {
  heading: '',
  itemBrand: '',
  subheading: '',
  badge: null,
  isPickUpSelected: false,
  value: 'home',
  pickupSelectionUpdate: () => {},
  isAllSizeDisabled: false,
  checkForOOSForVariant: false,
};

export default withStyles(DeliveryButton, styles);
