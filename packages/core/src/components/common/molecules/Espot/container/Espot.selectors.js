// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';

export const getLabels = state => {
  return state && state.Labels;
};

export const getEspotLabels = createSelector(
  getLabels,
  labels => {
    return {
      login: getLabelValue(labels, 'lbl_overview_login_text', 'accountOverview', 'account'),
      createAccount: getLabelValue(
        labels,
        'lbl_overview_createAccount',
        'accountOverview',
        'account'
      ),
    };
  }
);
