// 9fbef606107a605d69c0edbcd8029e5d 
import { getEspotLabels } from '../Espot.selectors';

describe('Espot selectors', () => {
  it('getEsotSelectors should return correct labels if present', () => {
    const state = {
      Labels: {
        account: {
          accountOverview: {
            lbl_overview_login_text: 'login',
            lbl_overview_createAccount: 'create Account',
          },
        },
      },
    };

    expect(getEspotLabels(state).login).toBe('login');
  });

  it('getEsotSelectors should return label key if state is not present', () => {
    expect(getEspotLabels().login).toBe('lbl_overview_login_text');
  });

  it('getEsotSelectors should return label key if state is present but specified labels are not present', () => {
    expect(
      getEspotLabels({
        Labels: {},
      }).login
    ).toBe('lbl_overview_login_text');
  });
});
