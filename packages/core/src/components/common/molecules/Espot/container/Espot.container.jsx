// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';

import { openOverlayModal } from '@tcp/core/src/components/features/account/OverlayModal/container/OverlayModal.actions';
import { toggleNeedHelpModalState } from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import { closeMiniBag } from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import BagPageSelectors from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import BAG_PAGE_ACTIONS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';

import { selectOrder } from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.actions';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import { getEspotLabels } from './Espot.selectors';
import { toggleApplyNowModal } from '../../ApplyNowPLCCModal/container/ApplyNowModal.actions';
import EspotComponent from '../views';

export const triggerFetchHelpContent = props => {
  const { fetchNeedHelpContent, needHelpContentId, toggleNeedHelpModal } = props;
  fetchNeedHelpContent([needHelpContentId]);
  toggleNeedHelpModal();
};

export const mapDispatchToProps = dispatch => {
  return {
    togglePlccModal: payload => {
      dispatch(
        toggleApplyNowModal({
          isModalOpen: payload || false,
        })
      );
    },
    openOverlay: payload => {
      dispatch(toggleApplyNowModal(false));
      dispatch(closeMiniBag());
      dispatch(openOverlayModal(payload));
    },
    toggleNeedHelpModal: () => {
      dispatch(toggleNeedHelpModalState());
    },
    fetchNeedHelpContent: contentIds => {
      dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
    },
    getHelpContent: payload => {
      return triggerFetchHelpContent(payload);
    },
    selectOrderCustomerHelp: () => dispatch(selectOrder({})),
    trackAnalyticsClick: (eventData, payload) => {
      const timer = setTimeout(() => {
        dispatch(setClickAnalyticsData(eventData));
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 200);
    },
  };
};

export const mapStateToProps = state => {
  return {
    isLoggedIn: getUserLoggedInState(state),
    labels: getEspotLabels(state),
    needHelpContentId: BagPageSelectors.getNeedHelpContent(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EspotComponent);
