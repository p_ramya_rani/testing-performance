// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import RichText from '@tcp/core/src/components/common/atoms/RichText';
import {
  getCreateAccountPayload,
  getLoginPayload,
  getMPRImageClickPayload,
  getApplyOrAcceptPayload,
  getJoinTheClubPayload,
  getLoyaltyLocation,
  setLoyaltyLocation,
} from '@tcp/core/src/constants/analytics';
import {
  getAPIConfig,
  routerPush,
  configureInternalNavigationFromCMSUrl,
} from '@tcp/core/src/utils';
import {
  CUSTOMER_HELP_GA,
  CUSTOMER_HELP_PAGE_TEMPLATE,
  CUSTOMER_HELP_ROUTES,
  CSH_PAGE_TARGET_FOR_HELPCENTER,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import internalEndpoints from '@tcp/core/src/components/features/account/common/internalEndpoints';
import { routeToPage } from '../../../../features/account/CustomerHelp/util/utility';

class Espot extends PureComponent {
  /**
   * @function onClickHandler
   * @param {string} link - href key defined in anchor of richtext
   * @param {string} target - action type
   * @param {string} action - data-target of anchor for modal identification
   * @returns {function}  - function to open modal or navigate to a path
   */
  onClickHandler = (link, target, action, clickedText) => {
    switch (target) {
      case '_modal':
        this.handleModal(action, clickedText);
        break;
      default:
        this.handleUrl(link, target, clickedText);
    }
  };

  prepareAndSendLoyaltyPayload = (trackAnalyticsClick, clickedOption) => {
    const payload = clickedOption(getLoyaltyLocation());
    if (payload) trackAnalyticsClick(payload, { name: payload.eventName });
  };

  /**
   * @function handleModal
   * @param {string} action - action to identify data-target of modal
   * @returns {function} calls function received from prop to open a modal
   */
  handleModal = (action) => {
    const {
      togglePlccModal,
      openOverlay,
      getHelpContent,
      redirectToFullPageAuth,
      selectOrderCustomerHelp,
      trackAnalyticsClick,
      loyaltyLocation,
    } = this.props;
    const { pathname = 'referrer' } = (window && window.location) || {};
    setLoyaltyLocation(loyaltyLocation);

    switch (action) {
      case 'plccModal':
        togglePlccModal(true);
        this.prepareAndSendLoyaltyPayload(trackAnalyticsClick, getMPRImageClickPayload);
        break;
      case 'login':
        this.prepareAndSendLoyaltyPayload(trackAnalyticsClick, getLoginPayload);
        if (redirectToFullPageAuth) {
          routerPush(`/home?target=login&successtarget=${pathname}`, '/home/login');
        } else {
          openOverlay({
            component: 'login',
            variation: 'primary',
          });
        }
        break;
      case 'create-account':
        this.prepareAndSendLoyaltyPayload(trackAnalyticsClick, getCreateAccountPayload);
        if (redirectToFullPageAuth) {
          routerPush(`/home?target=register&successtarget=${pathname}`, '/home/register');
        } else {
          openOverlay({
            component: 'createAccount',
            variation: 'primary',
          });
        }
        break;
      case 'isCouponHelpModalOpen':
        getHelpContent(this.props);
        break;
      case CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER:
        trackAnalyticsClick({ orderHelpLocation: CUSTOMER_HELP_GA.BANNER_CTA_TRACK.CD126 });
        selectOrderCustomerHelp();
        break;
      case CSH_PAGE_TARGET_FOR_HELPCENTER:
        routeToPage(CUSTOMER_HELP_ROUTES.LANDING_PAGE);
        break;
      default:
        break;
    }
  };

  trackLoyaltyClicks = (link, clickedText) => {
    if (!link) return;

    const { trackAnalyticsClick, loyaltyLocation } = this.props;
    const url = link.toLowerCase();
    setLoyaltyLocation(loyaltyLocation);

    if (clickedText && clickedText.toLowerCase() === 'join the club' && url.includes('register')) {
      this.prepareAndSendLoyaltyPayload(trackAnalyticsClick, getJoinTheClubPayload);
    } else if (url.includes('register')) {
      this.prepareAndSendLoyaltyPayload(trackAnalyticsClick, getCreateAccountPayload);
    } else if (url.includes('login')) {
      this.prepareAndSendLoyaltyPayload(trackAnalyticsClick, getLoginPayload);
    } else if (url.includes('application')) {
      this.prepareAndSendLoyaltyPayload(trackAnalyticsClick, getApplyOrAcceptPayload);
    }
  };

  /**
   * @function handleUrl - opens an external url or navigate to internal route
   * @param {string} link - href in anchor of richtext
   * @param {string} target - action type to identify navigation window
   */
  handleUrl = (link, target, clickedText) => {
    const externalUrl = new RegExp('^(?:[a-z]+:)?//', 'i');
    const { siteId } = getAPIConfig();

    this.trackLoyaltyClicks(link, clickedText);

    if (externalUrl.test(link)) {
      window.open(link, '_blank', 'noopener');
    } else {
      switch (target) {
        case '_self':
          this.richTextInternalRoute(link, siteId, '');
          break;
        case '_appmodal':
          this.richTextInternalRoute(link, siteId, '');
          break;
        case '_blank':
          window.open(`/${siteId}${link}`, 'noopener', '_blank');
          break;
        default:
          this.richTextInternalRoute(configureInternalNavigationFromCMSUrl(link), siteId, link);
          break;
      }
    }
  };

  /**
   * Routes to an endpoint from rich text link click
   * @param {string} link - internal link to be navigated
   */
  richTextInternalRoute = (url, siteId, path = '') => {
    let asPath = path;
    let asUrl = url;
    const siteIdRegex = new RegExp(`^/${siteId}`);
    switch (url) {
      case '/pointsClaimForm':
        routerPush(internalEndpoints.pointsClaimPage.link, internalEndpoints.pointsClaimPage.path);
        break;
      case '/bag':
        routerPush('/bag', '/bag');
        break;
      case '/changePasswordPage':
        routerPush(`/change-password?page=reset`, `/change-password?page=reset`);
        break;
      default:
        asPath = asPath.replace(siteIdRegex, ''); // checks if site id is already existing in the asPath
        asUrl = asUrl.replace(siteIdRegex, '');
        routerPush(asUrl, asPath);
        break;
    }
  };

  render() {
    const { richTextHtml, isNativeView, ...otherProps } = this.props;
    return (
      <div>
        <RichText
          richTextHtml={richTextHtml}
          isNativeView={isNativeView}
          actionHandler={this.onClickHandler}
          fromEspot
          {...otherProps}
        />
      </div>
    );
  }
}

Espot.propTypes = {
  togglePlccModal: PropTypes.func.isRequired,
  richTextHtml: PropTypes.string.isRequired,
  openOverlay: PropTypes.func.isRequired,
  toggleNeedHelpModal: PropTypes.func.isRequired,
  isNativeView: PropTypes.bool,
  getHelpContent: PropTypes.func,
  redirectToFullPageAuth: PropTypes.bool,
  selectOrderCustomerHelp: PropTypes.func.isRequired,
  trackAnalyticsClick: PropTypes.func,
  loyaltyLocation: PropTypes.string,
};

Espot.defaultProps = {
  isNativeView: false,
  getHelpContent: () => {},
  redirectToFullPageAuth: false,
  trackAnalyticsClick: () => {},
  loyaltyLocation: '',
};

export default Espot;
