// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import RichText from '@tcp/core/src/components/common/atoms/RichText';
import OpenLoginModal from '@tcp/core/src/components/features/account/LoginPage/container/LoginModal.container';
import { handleNavigationUrl, checkForLoginRegisterLink } from '@tcp/core/src/utils/utils.app';
import { redirectToInAppView } from '@tcp/core/src/utils/index.native';
import { isWebViewPage } from '@tcp/core/src/utils';

const viewStyle = {
  flex: 1,
};

export class Espot extends PureComponent {
  static propTypes = {
    richTextHtml: PropTypes.string.isRequired,
    togglePlccModal: PropTypes.func.isRequired,
    navigation: PropTypes.func.isRequired,
    toggleNeedHelpModal: PropTypes.func.isRequired,
    isNativeView: PropTypes.bool,
    labels: PropTypes.shape({}),
    isLoggedIn: PropTypes.bool,
    onRequestClose: PropTypes.func,
    routeName: PropTypes.string,
    getHelpContent: PropTypes.func,
  };

  static defaultProps = {
    isNativeView: true,
    labels: {},
    isLoggedIn: false,
    onRequestClose: () => {},
    routeName: '',
    getHelpContent: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      showLoginModal: false,
      loginType: '',
      modalHeaderLbl: '',
      espotShowResetPassword: false,
    };
  }

  /**
   * @function onPressHandler
   * @param {string} link - href key n
   * @param {string} target - action type
   * @param {string} action - data-target of anchor
   * @returns {function}  - function to open modal or navigate to a path
   */
  // eslint-disable-next-line consistent-return
  onPressHandler = (link, target, action) => {
    const { navigation, onRequestClose, routeName } = this.props;
    const loginRegisterLink = checkForLoginRegisterLink(link, action); // check if href is of login or register page, open modal in that case
    if (onRequestClose) {
      onRequestClose({ state: false });
    }
    if (loginRegisterLink) {
      this.openModal(loginRegisterLink);
    }
    if (link && isWebViewPage(link)) {
      redirectToInAppView(link, navigation, routeName, true);
      return false;
    }

    switch (target) {
      case '_modal':
        this.openModal(action);
        break;
      case '_appmodal':
        this.openModal(action);
        break;
      default:
        handleNavigationUrl(navigation, link, target);
    }
  };

  /**
   * @function openModal
   * @param {string} action - action to identify data-target of modal
   * @returns {function} calls function received from prop to open a modal
   */
  openModal = action => {
    const { togglePlccModal, navigation, getHelpContent } = this.props;
    switch (action) {
      case 'plccModal':
        navigation.navigate('ApplyNow');
        togglePlccModal(true);
        break;
      case 'isCouponHelpModalOpen':
        getHelpContent(this.props);
        break;
      case 'create-account':
        this.showCreateAccountModal();
        break;
      case 'login':
        this.showLoginModal();
        break;
      case 'changePasswordPage':
        this.showResetPasswordModal();
        break;
      default:
        break;
    }
  };

  showCreateAccountModal = () => {
    const { labels } = this.props;
    this.setState({
      showLoginModal: true,
      loginType: 'createAccount',
      modalHeaderLbl: labels.createAccount,
    });
  };

  showLoginModal = () => {
    const { labels } = this.props;
    this.setState({
      showLoginModal: true,
      loginType: 'login',
      modalHeaderLbl: labels.login,
    });
  };

  showResetPasswordModal = () => {
    const { labels } = this.props;
    this.setState({
      showLoginModal: true,
      loginType: 'login',
      modalHeaderLbl: labels.login,
      espotShowResetPassword: true,
    });
  };

  setLoginModal = params => {
    this.setState({
      showLoginModal: params && params.state,
    });
  };

  render() {
    const { richTextHtml, navigation, isNativeView, isLoggedIn, ...otherProps } = this.props;
    const { showLoginModal, loginType, modalHeaderLbl, espotShowResetPassword } = this.state;
    return (
      <View style={viewStyle}>
        <RichText
          source={richTextHtml}
          isNativeView={isNativeView}
          actionHandler={this.onPressHandler}
          navigation={navigation}
          fromEspot
          {...otherProps}
        />
        {showLoginModal && (
          <OpenLoginModal
            component={loginType}
            openState={showLoginModal}
            setLoginModalMountState={this.setLoginModal}
            isUserLoggedIn={isLoggedIn}
            modalHeaderLbl={modalHeaderLbl}
            espotShowResetPassword={espotShowResetPassword}
          />
        )}
      </View>
    );
  }
}

export default withNavigation(Espot);
