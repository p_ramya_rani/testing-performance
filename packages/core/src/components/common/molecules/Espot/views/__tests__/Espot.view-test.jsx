// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { routeToPage } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import { shallow } from 'enzyme';
import Espot from '../Espot.view';

jest.mock('@tcp/core/src/components/features/account/CustomerHelp/util/utility', () => ({
  routeToPage: jest.fn(),
}));
jest.mock('@tcp/core/src/utils/utils.web', () => ({
  routerPush: jest.fn(),
  fireEnhancedEcomm: jest.fn(),
}));
jest.mock('next/router', () => ({ push: jest.fn() }));

describe('Espot component', () => {
  let component;
  const togglePlccModalSpy = jest.fn();
  const openOverlaySpy = jest.fn();
  const selectOrder = jest.fn();
  beforeEach(() => {
    const props = {
      richTextHtml: '<b>test</b>',
      togglePlccModal: togglePlccModalSpy,
      openOverlay: openOverlaySpy,
      selectOrderCustomerHelp: selectOrder,
    };
    component = shallow(<Espot {...props} />);
  });

  it('should renders correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('onClickHandler instance should not call openOverlay prop if action is login', () => {
    component.setProps({ redirectToFullPageAuth: true });
    component.instance().onClickHandler('', '_modal', 'login');
    expect(openOverlaySpy).not.toBeCalled();
  });

  it('onClickHandler instance should not call openOverlay prop if action is create-account', () => {
    component.setProps({ redirectToFullPageAuth: true });
    component.instance().onClickHandler('', '_modal', 'create-account');
    expect(openOverlaySpy).not.toBeCalled();
    component.setProps({ redirectToFullPageAuth: false });
  });

  it('onClickHandler instance should call togglePlccModal prop if action is plccModal', () => {
    component.instance().onClickHandler('', '_modal', 'plccModal');
    expect(togglePlccModalSpy).toBeCalled();
  });

  it('onClickHandler instance should call openOverlay prop if action is login', () => {
    component.instance().onClickHandler('', '_modal', 'login');
    expect(openOverlaySpy).toBeCalled();
  });

  it('onClickHandler instance should call openOverlay prop if action is create-account', () => {
    component.instance().onClickHandler('', '_modal', 'create-account');
    expect(openOverlaySpy).toBeCalled();
  });

  it('should call handleUrl for target blank', () => {
    component.instance().onClickHandler('/pointsClaimForm', '_blank', '');

    const richTextInternalRoute = jest.spyOn(component.instance(), 'richTextInternalRoute');

    component.instance().handleUrl('/pointsClaim', '_blank');

    component.instance().richTextInternalRoute('/pointsClaim');
    expect(richTextInternalRoute).toHaveBeenCalled();
  });

  it('should call handleUrl for target self', () => {
    component.instance().onClickHandler('/pointHistoryPage', '_self', '');

    const richTextInternalRoute = jest.spyOn(component.instance(), 'richTextInternalRoute');

    component.instance().handleUrl('/pointHistory', '_self');

    component.instance().richTextInternalRoute('/pointHistory');
    expect(richTextInternalRoute).toHaveBeenCalled();
  });
  it('onClickHandler instance should call selectOrderCustomerHelp prop if action is select-order', () => {
    component.instance().onClickHandler('', '_modal', 'select-order');
    expect(selectOrder).toBeCalled();
  });

  it('onClickHandler instance should call routeToPage if action is cshLanding', () => {
    component.instance().onClickHandler('', '_modal', 'cshLanding');
    expect(routeToPage).toBeCalled();
  });
});
