// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
// TODO: Espot import is causing circular dependency and test suite is failing, commenting for now
// import Espot from '../Espot.view.native';

describe.skip('Espot component', () => {
  let component;
  const togglePlccModalSpy = jest.fn();
  beforeEach(() => {
    const props = {
      richTextHtml: '<b>test</b>',
      togglePlccModal: togglePlccModalSpy,
      navigation: {
        navigate: () => {},
      },
    };
    // eslint-disable-next-line
    component = shallow(<EspotComponent {...props} />);
  });

  it('should renders correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('onPressHandler instance should call togglePlccModal prop if action is plccModal', () => {
    component.instance().onPressHandler('', '_modal', 'plccModal');
    expect(togglePlccModalSpy).toBeCalled();
  });

  it('onPressHandler instance should set login modal state correctly if target is create-account', () => {
    component.instance().onPressHandler('', '_modal', 'create-account');
    expect(component.state('loginType')).toBe('createAccount');
  });

  it('onPressHandler instance should set login modal state correctly if target is login', () => {
    component.instance().onPressHandler('', '_modal', 'login');
    expect(component.state('loginType')).toBe('login');
  });

  it('onPressHandler instance should set login modal state correctly if link is for register', () => {
    component.instance().onPressHandler('/us/home/register');
    expect(component.state('loginType')).toBe('createAccount');
  });

  it('onPressHandler instance should set login modal state correctly if link is for login', () => {
    component.instance().onPressHandler('/us/home/login');
    expect(component.state('loginType')).toBe('login');
  });
});
