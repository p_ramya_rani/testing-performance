// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import { getVideoUrl } from '@tcp/core/src/utils';
import {
  imageData,
  videoData,
} from '@tcp/core/src/components/features/browse/OutfitDetails/config';
import PaginationDots from '@tcp/core/src/components/common/molecules/PaginationDots';

import logger from '@tcp/core/src/utils/loggerInstance';
import { DamImage } from '../../../atoms';
import { Container, ImageTouchableOpacity } from '../styles/ImageCarousel.style.native';
import OutOfStockWaterMark from '../../../../features/browse/ProductDetail/molecules/OutOfStockWaterMark';

class ImageCarousel extends React.PureComponent {
  constructor(props) {
    super(props);
    const { setActiveIndex, setDefaultIndex } = props;
    this.state = {
      activeSlideIndex: setActiveIndex,
      defaultIndex: setDefaultIndex,
    };
    this.flatListRef = React.createRef();
  }

  componentDidMount() {
    const { defaultIndex } = this.state;
    setTimeout(() => {
      this.scrollToIndex(this.flatListRef, defaultIndex);
    }, 50);
  }

  componentDidUpdate(prevProps) {
    const { PaginationDotsIndex, selectedItemIndex } = this.props;
    if (PaginationDotsIndex !== prevProps.PaginationDotsIndex) {
      selectedItemIndex(PaginationDotsIndex);
      this.scrollToIndex(this.flatListRef, PaginationDotsIndex);
    }
  }

  // this method when swipe image and return changed view
  onViewableItemsChanged = ({ changed }) => {
    const len = (changed && changed.length) || 0;
    for (let i = 0; i < len; i += 1) {
      const item = changed[i];
      const { isViewable, index } = item;
      if (isViewable) {
        this.setActiveSlideIndex(index);
        break;
      }
    }
  };

  // this method set current visible image
  setActiveSlideIndex = (index) => {
    const { getActiveIndex } = this.props;
    const { activeSlideIndex } = this.state;
    if (index !== activeSlideIndex) {
      this.setState(
        {
          activeSlideIndex: index,
        },
        () => {
          if (getActiveIndex) getActiveIndex(index);
        }
      );
    }
  };

  renderNormalImage = (imgSource) => {
    const {
      onImageClick,
      imageWidth,
      imageHeight,
      imageUrlKey,
      keepAlive,
      outOfStockLabels,
      dynamicBadgeOverlayQty,
      isDynamicBadgeEnabled,
      tcpStyleType,
      isFastImage,
      selectedItemIndex,
      disabledTouch,
      isNewReDesignEnabled,
      zoomImages,
      primaryBrand,
      alternateBrand,
    } = this.props;
    const { activeSlideIndex } = this.state;
    const { index, item } = imgSource;
    const imgUrl = (item && item[imageUrlKey]) || '';
    const isVideoUrl = imgUrl && getVideoUrl(imgUrl);
    const mainConfig = isVideoUrl ? videoData.imgConfig : imageData.imgConfig;
    selectedItemIndex(activeSlideIndex);
    return (
      <ImageTouchableOpacity
        onPress={onImageClick}
        accessible={index === activeSlideIndex}
        accessibilityRole="image"
        accessibilityLabel={`product image ${index + 1}`}
        disabled={disabledTouch}
      >
        <DamImage
          zoomImages={zoomImages}
          key={index.toString()}
          url={imgUrl}
          isProductImage
          width={imageWidth}
          height={imageHeight}
          resizeMode="stretch"
          imgConfig={mainConfig}
          isFastImage={isFastImage !== 'false'}
          isOptimizedVideo={!!isVideoUrl}
          dynamicBadgeOverlayQty={index === 0 && isDynamicBadgeEnabled && dynamicBadgeOverlayQty}
          tcpStyleType={tcpStyleType}
          badgeConfig={imageData.badgeConfig}
          isNewReDesignEnabled={isNewReDesignEnabled}
          primaryBrand={primaryBrand || alternateBrand}
        />

        {keepAlive && (
          <OutOfStockWaterMark label={outOfStockLabels.outOfStockCaps} fontSize="fs12" />
        )}
      </ImageTouchableOpacity>
    );
  };

  renderPaginationDots = () => {
    const { imageUrls, isNewReDesignEnabled } = this.props;
    const { activeSlideIndex } = this.state;
    return (
      <PaginationDots
        numberOfDots={imageUrls.length}
        selectedIndex={activeSlideIndex}
        onPress={this.onPageChange}
        accessibilityLabels={{}}
        isSBP={false}
        isNewReDesignEnabled={isNewReDesignEnabled}
        isChildCarousel={true}
      />
    );
  };

  // this method call when tap on the pagination dots and navigate to clicked image
  onPageChange = (dotClickedIndex) => {
    const { imageUrls } = this.props;
    if (this.flatListRef && dotClickedIndex < imageUrls.length) {
      this.scrollToIndex(this.flatListRef, dotClickedIndex);
    }
  };

  scrollToIndex = (ref, index = 0) => {
    if (ref !== null && ref.current !== null) {
      try {
        ref.current.scrollToIndex({ index, viewPosition: 0.5 });
      } catch (error) {
        logger.log(error.message);
      }
    }
  };

  scrollToIndexFailed = (info, flatListRef) => {
    if (this.flatListRef !== null) {
      setTimeout(
        () =>
          flatListRef.current.scrollToIndex({
            index: info.index,
            animated: true,
            viewPosition: 0.5,
          }),
        50
      );
    }
  };

  render() {
    const { imageUrls, margins, isNewReDesignEnabled } = this.props;
    return (
      <Container margins={margins}>
        <FlatList
          ref={this.flatListRef}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={{
            itemVisiblePercentThreshold: 50,
          }}
          initialNumToRender={1}
          initialScrollIndex={0}
          refreshing={false}
          data={imageUrls}
          pagingEnabled
          horizontal
          showsHorizontalScrollIndicator={false}
          listKey={(_, index) => index.toString()}
          renderItem={this.renderNormalImage}
          onScrollToIndexFailed={(info) => this.scrollToIndexFailed(info, this.flatListRef)}
        />
        {isNewReDesignEnabled && this.renderPaginationDots()}
      </Container>
    );
  }
}

ImageCarousel.propTypes = {
  imageUrls: PropTypes.arrayOf(
    PropTypes.shape({
      item: PropTypes.shape({
        regularSizeImageUrl: PropTypes.string.isRequired,
      }),
    })
  ),
  onImageClick: PropTypes.func,
  margins: PropTypes.string,
  imageWidth: PropTypes.number,
  imageHeight: PropTypes.number,
  getActiveIndex: PropTypes.func,
  selectedItemIndex: PropTypes.func,
  setActiveIndex: PropTypes.number,
  setDefaultIndex: PropTypes.number,
  PaginationDotsIndex: PropTypes.number,
  imageUrlKey: PropTypes.string,
  keepAlive: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({
    outOfStockCaps: PropTypes.string,
  }),
  dynamicBadgeOverlayQty: PropTypes.number,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  tcpStyleType: PropTypes.string,
  isFastImage: PropTypes.string,
  disabledTouch: PropTypes.bool,
  isNewReDesignEnabled: PropTypes.bool,
};

ImageCarousel.defaultProps = {
  imageUrls: [],
  margins: null,
  onImageClick: () => {},
  selectedItemIndex: () => {},
  imageWidth: 103, // thumb nail image size i.e. collection pdp thumb image size
  imageHeight: 127, // thumb nail image size i.e. collection pdp thumb image size
  getActiveIndex: null,
  setActiveIndex: 0,
  setDefaultIndex: 0,
  PaginationDotsIndex: 0,
  disabledTouch: false,
  imageUrlKey: 'listingSizeImageUrl',
  keepAlive: false,
  outOfStockLabels: {
    outOfStockCaps: '',
  },
  dynamicBadgeOverlayQty: 0,
  isDynamicBadgeEnabled: {},
  tcpStyleType: '',
  isFastImage: 'true',
  isNewReDesignEnabled: false,
};

export default ImageCarousel;
export { ImageCarousel as ImageCarouselVanilla };
