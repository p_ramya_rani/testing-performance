// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const ImageTouchableOpacity = styled.TouchableOpacity`
  justify-content: center;
  padding-right: 10px;
`;

const Container = styled.View`
  justify-content: center;
  width: 100%;
`;

export { Container, ImageTouchableOpacity };
