// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import ModuleO from '../ModuleO.native';
import ListItem from '../../../../features/browse/ProductListing/molecules/ProductListItem';

const navigate = jest.fn();
const setOpen = jest.fn();

const props = {
  key: 'test',
  loadedProductCount: 4,
  generalProductId: 'sfsdsdgsfgds',
  item: {
    productInfo: {},
    miscInfo: {},
    colorsMap: [
      {
        color: { name: '' },
        colorProductId: '',
        miscInfo: {
          badge1: { matchBadge: '' },
        },
      },
    ],
    imagesByColor: {},
    sqnNmbr: 123,
  },
  isPLPredesign: false,
  productsBlock: {},
  onPickUpOpenClick: () => {},
  className: 'test',
  labels: {},
  sequenceNumber: 12312,
  setOpen,
  navigation: {
    navigate,
  },
};

describe('ModuleO component', () => {
  it('ModuleO component renders correctly with props', () => {
    const component = shallow(<ModuleO {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('ModuleO component renders correctly with sbp props', () => {
    const component = shallow(<ModuleO {...props} isSBP />);
    expect(component).toMatchSnapshot();
  });
  it('ModuleO component calls correct nav methods', () => {
    const componentAvecSBP = shallow(<ModuleO {...props} isSBP />);
    componentAvecSBP
      .find(ListItem)
      .first()
      .props()
      .onGoToPDPPage();
    expect(navigate).toHaveBeenCalledTimes(1);
    const componentSansSBP = shallow(<ModuleO {...props} />);
    componentSansSBP
      .find(ListItem)
      .first()
      .props()
      .onGoToPDPPage();
    expect(navigate).toHaveBeenCalledTimes(2);
  });
});
