// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  margin: 0 10px;
  &.price-only {
    .added-to-bag {
      display: none;
    }
  }
  .top-badge-container,
  .extended-sizes-text,
  .fav-icon-wrapper,
  .product-title-container,
  .color-chips-container,
  .loyalty-text-container {
    display: none;
  }
  .product-image-container {
    height: ${(props) => (props.isAddedToBag ? 'auto' : '185px')};
    width: 100%;
    a {
      display: block;
      height: 100%;
    }
    img {
      height: auto;
      width: 100%;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate3d(-50%, -50%, 0);
      @media ${(props) => props.theme.mediaQuery.mediumMax} {
        height: ${((props) => props.isNewPDPEnabled && props.page === 'pdp') && '90%'};
      }
    }
  }
  .container-price {
    text-align: left;
    min-height: 40px;
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .product-image-container {
      height: ${(props) =>
        props.isAddedToBag ? `auto` : `${props.isPdpPlaProducts ? `190px` : `267px`}`};
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .product-image-container {
      height: ${(props) =>
        props.isAddedToBag ? `auto` : `${props.isPdpPlaProducts ? `206px` : `286px`}`};
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .product-image-container {
      height: ${(props) => (props.page === 'cart' && !props.bagItemCount ? `330px` : ``)};
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .product-image-container {
      height: ${(props) => (props.page === 'checkout' ? `358px` : ``)};
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .product-image-container {
      height: ${(props) =>
        props.page === 'outfit' || (!props.isNewPDPEnabled && props.page === 'pdp') ? `352px` : ``};
    }
  }
`;
