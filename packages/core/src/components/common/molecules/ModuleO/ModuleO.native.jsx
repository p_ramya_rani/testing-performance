// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import { goToPlpTab } from '@tcp/core/src/utils';
import ListItem from '../../../features/browse/ProductListing/molecules/ProductListItem';
import { getMapSliceForColorProductId } from '../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { getPromotionalMessage } from '../../../features/browse/ProductListing/molecules/ProductList/utils/utility';
import { Container } from './ModuleO.style.native';

const PRODUCT_IMAGE_WIDTH = 162;
// eslint-disable-next-line
const onAddToBag = (data) => {};

// eslint-disable-next-line
const onFavorite = (item) => {};

const onOpenPDPPageHandler = (props) => (pdpUrl, selectedColorIndex) => {
  const {
    title,
    navigation,
    page,
    viaModuleName,
    closeATBModal = () => {},
    portalValue,
    isSBP,
    item,
    handleClose = () => {},
    item: { actionId } = {},
  } = props;
  if (
    page !== Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP &&
    page !== Constants.RECOMMENDATIONS_PAGES_MAPPING.ADDED_TO_BAG &&
    page !== Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG
  ) {
    goToPlpTab(navigation);
  }
  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.ORDER_NOTIFICATION)
    handleClose({ isOpen: false });
  closeATBModal();
  if (isSBP)
    navigation.navigate('ProductDetailSBP', {
      title,
      pdpUrl,
      selectedColorProductId: selectedColorIndex,
      reset: true,
      viaModule: 'mbox_Recommendation',
      viaModuleName,
      clickOrigin: page,
      isSBP,
      retrievedProductInfo: null,
      actionId,
    });
  else
    navigation.navigate('ProductDetail', {
      title,
      pdpUrl,
      selectedColorProductId: selectedColorIndex,
      selectedColor: item?.colorsMap?.[0]?.color?.name,
      reset: true,
      viaModule: 'mbox_Recommendation',
      viaModuleName,
      portalValue,
      clickOrigin: page,
      actionId,
    });
};

const ModuleO = (props) => {
  const {
    isMatchingFamily,
    currencyExchange,
    currencySymbol,
    paddings,
    isPlcc,
    onQuickViewOpenClick,
    trackAnalyticsOnAddToBagButtonClick,
    item,
    priceOnly,
    viaModule,
    labels,
    showPriceRange,
    page,
    isSBP,
    isHidePLPAddToBag,
    isPlaceCashReward,
    isRecommendations,
    productImageWidth,
    productImageHeight,
    portalValue,
    navigation,
    isNavL2,
    isCardTypeTiles,
    outOfStock,
    isDynamicBadgeEnabled,
    setOpen = () => {},
    index,
    quickViewLoader,
    quickViewProductId,
    fireHapticFeedback,
    ...otherprops
  } = props;
  const productWidth = productImageWidth || PRODUCT_IMAGE_WIDTH;

  const newPage = isSBP ? 'sbpPDP' : page;
  const { colorsMap, productInfo } = item;
  const { promotionalMessage, promotionalPLCCMessage } = productInfo;
  const { colorProductId } = colorsMap[0];

  const colorsExtraInfo = colorsMap[0].color.name;

  // get default zero index color entry
  const curentColorEntry = getMapSliceForColorProductId(colorsMap, colorProductId);
  // get product color and price info of default zero index item
  const currentColorMiscInfo =
    colorsExtraInfo[curentColorEntry.color.name] || curentColorEntry.miscInfo || {};
  const { badge1, badge2 } = currentColorMiscInfo;
  // get default top badge data
  const topBadge = isMatchingFamily && badge1.matchBadge ? badge1.matchBadge : badge1.defaultBadge;

  // get default Loyalty message
  const loyaltyPromotionMessage = isPlcc
    ? getPromotionalMessage(isPlcc, {
        promotionalMessage,
        promotionalPLCCMessage,
      })
    : '';
  return (
    <Container isCardTypeTiles={isCardTypeTiles} index={index}>
      <ListItem
        {...otherprops}
        paddings={paddings}
        item={item}
        portalValue={portalValue}
        isMatchingFamily={isMatchingFamily}
        badge1={topBadge}
        badge2={badge2}
        isPlcc={isPlcc}
        loyaltyPromotionMessage={isRecommendations ? '' : loyaltyPromotionMessage}
        onAddToBag={onAddToBag}
        onFavorite={onFavorite}
        currencyExchange={currencyExchange}
        currencySymbol={currencySymbol}
        onGoToPDPPage={onOpenPDPPageHandler(props)}
        onQuickViewOpenClick={onQuickViewOpenClick}
        trackAnalyticsOnAddToBagButtonClick={trackAnalyticsOnAddToBagButtonClick}
        fullWidth
        renderPriceAndBagOnly
        renderPriceOnly={priceOnly}
        productImageWidth={productWidth}
        viaModule={viaModule}
        labelsPlpTiles={labels}
        isSwipeEnable={false}
        showPriceRange={showPriceRange}
        fromPage={newPage}
        isSBP={isSBP}
        isHidePLPAddToBag={isHidePLPAddToBag}
        isPlaceCashReward={isPlaceCashReward}
        isRecommendations={isRecommendations}
        navigation={navigation}
        isNavL2={isNavL2}
        isCardTypeTiles={isCardTypeTiles}
        outOfStock={outOfStock}
        productImageHeight={productImageHeight}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        setOpen={setOpen}
        quickViewLoader={quickViewLoader}
        quickViewProductId={quickViewProductId}
        fireHapticFeedback={fireHapticFeedback}
      />
    </Container>
  );
};

ModuleO.propTypes = {
  isMatchingFamily: PropTypes.bool,
  currencySymbol: PropTypes.string,
  currencyExchange: PropTypes.arrayOf(PropTypes.shape({})),
  isPlcc: PropTypes.bool,
  onQuickViewOpenClick: PropTypes.func,
  trackAnalyticsOnAddToBagButtonClick: PropTypes.func,
  item: PropTypes.shape({}).isRequired,
  priceOnly: PropTypes.bool,
  viaModule: PropTypes.string,
  labels: PropTypes.shape({
    lbl_plpTiles_shop_collection: PropTypes.string,
    lbl_add_to_bag: PropTypes.string,
  }).isRequired,
  showPriceRange: PropTypes.bool,
  isSBP: PropTypes.bool,
  page: PropTypes.string,
  isHidePLPAddToBag: PropTypes.bool,
  isPlaceCashReward: PropTypes.bool,
  isRecommendations: PropTypes.bool,
  productImageWidth: PropTypes.number,
  productImageHeight: PropTypes.number,
  portalValue: PropTypes.string,
  navigation: PropTypes.shape({}),
  isNavL2: PropTypes.bool,
  outOfStock: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  paddings: PropTypes.string,
  setOpen: PropTypes.func,
  isCardTypeTiles: PropTypes.bool,
  index: PropTypes.number,
  quickViewLoader: PropTypes.bool,
  quickViewProductId: PropTypes.string,
};

ModuleO.defaultProps = {
  onQuickViewOpenClick: () => {},
  trackAnalyticsOnAddToBagButtonClick: () => {},
  currencyExchange: [{ exchangevalue: 1 }],
  isMatchingFamily: true,
  isPlcc: false,
  currencySymbol: '$',
  priceOnly: false,
  viaModule: '',
  showPriceRange: false,
  page: '',
  isSBP: false,
  isHidePLPAddToBag: false,
  isPlaceCashReward: false,
  isRecommendations: false,
  productImageWidth: 0,
  productImageHeight: 0,
  portalValue: '',
  outOfStock: false,
  navigation: {},
  isNavL2: false,
  isDynamicBadgeEnabled: {},
  paddings: '0px 0 0px 12px',
  setOpen: () => {},
  isCardTypeTiles: false,
  index: -1,
  quickViewLoader: false,
  quickViewProductId: '',
};

export default ModuleO;
