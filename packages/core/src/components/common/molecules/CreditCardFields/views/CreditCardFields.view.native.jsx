// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { Platform, processColor } from 'react-native';
import logger from '@tcp/core/src/utils/loggerInstance';
import { CardIOModule, CardIOUtilities } from 'react-native-awesome-card-io';
import { isIOS } from '@tcp/core/src/utils';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import CameraAccessPrompt from '@tcp/core/src/components/common/molecules/CameraAccess/container/Camera.container';
import CustomIcon from '../../../atoms/Icon';
import { ICON_NAME } from '../../../atoms/Icon/Icon.constants';
import CreditCardNumber from '../../../atoms/CreditCardNumber';
import TextBox from '../../../atoms/TextBox';
import { isGymboree } from '../../../../../utils';

import {
  PaymentContainer,
  CardContainer,
  ExpiryContainer,
  ExpiryMonth,
  ExpiryYear,
  CardTextboxStyle,
  CvvCode,
  CvvTextboxStyle,
  CVVInfo,
  StyledImageWrapper,
  StyledLabel,
} from '../styles/CreditCardFields.styles.native';
import Select from '../../../atoms/Select';

const colorPallete = createThemeColorPalette();

/**
 *
 *
 * @class CreditCardFields
 * @extends {PureComponent}
 * @description view component to render credit card form fields.
 */
export class CreditCardFields extends React.PureComponent {
  constructor(props) {
    super(props);
    const { isEdit, selectedCard, selectedExpYear, selectedExpMonth } = props;
    if (isEdit && selectedCard) {
      const { expMonth, expYear } = selectedCard;
      this.state = {
        selectedYear: expYear,
        selectedMonth: expMonth && expMonth.trim(), // expMonth value for few cards coming with extra space. if no expMonth then default will be 1st from the options
      };
    } else {
      this.state = {
        selectedYear: selectedExpYear,
        selectedMonth: selectedExpMonth,
      };
    }
  }

  /* eslint-disable-next-line */
  UNSAFE_componentWillMount() {
    if (Platform.OS === 'ios') {
      CardIOUtilities.preload();
    }
  }

  /**
   * @function setGuideColor
   * @description customize the color of cardScan brackets.
   */
  setGuideColor = () => {
    if (Platform.OS === 'ios') {
      return isGymboree()
        ? processColor(colorPallete.orange[800])
        : processColor(colorPallete.userTheme.plcc);
    }
    return isGymboree() ? colorPallete.orange[800] : colorPallete.userTheme.plcc;
  };

  /**
   * @pageNaviagte : To manage to page navigate .
   */

  manageScan = permission => {
    request(permission).then(data => {
      if (data === 'granted') {
        this.scanCard();
      }
      this.closeModal();
    });
  };

  /**
   * @managePermission : To manage the permission of Android and IOS.
   */

  managePermission = permission => {
    check(permission)
      .then(result => {
        // eslint-disable-next-line default-case
        switch (result) {
          case RESULTS.UNAVAILABLE:
            break;
          case RESULTS.DENIED:
            this.manageScan(permission);
            break;
          case RESULTS.GRANTED:
            this.manageScan(permission);
            break;
          case RESULTS.BLOCKED:
            this.setState({ isOpen: true });
            break;
        }
      })
      .catch(error => {
        logger.error('camera permission error: ', error);
      });
  };

  /**
   * @function scanCard
   * @description on click on camera icon scan card will call
   */
  scanCard = async () => {
    const { updateCardDetails, creditFieldLabels } = this.props;
    const config = {
      hideCardIOLogo: true,
      guideColor: this.setGuideColor(),
      requireCVV: true,
      requireExpiry: true,
      scanInstructions: creditFieldLabels.cameraText,
    };
    const card = await CardIOModule.scanCard(config);
    if (card && card.cardNumber) {
      const { cardNumber, expiryYear, expiryMonth, cvv } = card;
      this.setState(
        {
          selectedMonth: expiryMonth.toString(),
          selectedYear: expiryYear.toString(),
        },
        () => {
          // Dropdown accepting values as string for month and year
          updateCardDetails(cardNumber, expiryYear.toString(), expiryMonth.toString(), cvv);
        }
      );
    }
  };

  handleChange = (event, value) => {
    const { getCardType, change } = this.props;
    if (getCardType) {
      this.cardType = getCardType(value);
      if (change) {
        change('cardType', getCardType(value));
      }
    }
  };

  /**
   * @toggleModal : To manage the modal state .
   */
  closeModal = () => {
    this.setState({
      isOpen: false,
    });
  };

  /**
   * @scanHandler : To manage the permission according to OS .
   */
  scanHandler = () => {
    const permission = isIOS() ? PERMISSIONS.IOS.CAMERA : PERMISSIONS.ANDROID.CAMERA;
    this.managePermission(permission);
  };

  /**
   * @function render
   * @description render method to be called of component
   */
  render() {
    const {
      cardTypeImgUrl,
      isPLCCEnabled,
      cardType,
      expMonthOptionsMap,
      expYearOptionsMap,
      dto,
      updateExpiryDate,
      isEdit,
      creditCard,
      creditFieldLabels,
      cvvInfo,
      showCvv,
      cameraIcon,
      onCardFocus,
      isExpirationRequired,
      createRefParent,
      getCardType,
    } = this.props;
    const { selectedMonth, selectedYear, isOpen } = this.state;
    if (!getCardType || this.cardType === undefined || this.cardType === null) {
      this.cardType = cardType;
    }
    return (
      <PaymentContainer>
        <CardContainer>
          <Field
            label={creditFieldLabels.creditCardNumber}
            name="cardNumber"
            id="cardNumber"
            keyboardType="numeric"
            component={CreditCardNumber}
            dataLocator="cardNbrTxtBox"
            onChange={this.handleChange}
            cardTypeImgUrl={cardTypeImgUrl}
            isPLCCEnabled={isPLCCEnabled}
            cardType={this.cardType}
            enableSuccessCheck={false}
            isEdit={isEdit}
            val={isEdit ? dto.accountNo : ''}
            creditCard={creditCard}
            customStyle={CardTextboxStyle}
            cameraIcon={cameraIcon}
            onCardFocus={onCardFocus}
            inputRef={node => createRefParent(node, 'cardNumber')}
          />
          {cameraIcon && !this.cardType && (
            <StyledImageWrapper onPress={this.scanHandler}>
              <CustomIcon
                name={ICON_NAME.camera}
                size="fs25"
                color="gray.900"
                dataLocator="pdp_fast_shipping_icon"
              />
            </StyledImageWrapper>
          )}
        </CardContainer>
        {isExpirationRequired && (
          <ExpiryContainer showCvv={showCvv}>
            <ExpiryMonth>
              <Field
                placeholder={creditFieldLabels.expMonth}
                heading={creditFieldLabels.expMonth}
                component={Select}
                name="expMonth"
                options={expMonthOptionsMap}
                dataLocator="expMonthDropDown"
                onValueChange={itemValue => {
                  this.setState({ selectedMonth: itemValue });
                  updateExpiryDate(itemValue, selectedYear);
                }}
                isAddNewCC
                ref={node => createRefParent(node, 'expMonth')}
              />
            </ExpiryMonth>
            <ExpiryYear>
              <Field
                placeholder={creditFieldLabels.expYear}
                heading={creditFieldLabels.expYear}
                component={Select}
                name="expYear"
                options={expYearOptionsMap}
                onValueChange={itemValue => {
                  this.setState({ selectedYear: itemValue });
                  updateExpiryDate(selectedMonth, itemValue);
                }}
                isAddNewCC
                ref={node => createRefParent(node, 'expYear')}
              />
            </ExpiryYear>
            {showCvv && (
              <CvvCode>
                <StyledLabel>{creditFieldLabels.cvvCode}</StyledLabel>
                <Field
                  label=""
                  name="cvvCode"
                  id="cvvCode"
                  keyboardType="numeric"
                  type="text"
                  component={TextBox}
                  dataLocator="cvvTxtBox"
                  customStyle={CvvTextboxStyle}
                  isCVVField
                  inputRef={node => createRefParent(node, 'cvvCode')}
                />
                <Field name="cardType" id="cardType" component={TextBox} type="hidden" />
                <CVVInfo>{cvvInfo}</CVVInfo>
              </CvvCode>
            )}
          </ExpiryContainer>
        )}
        {isOpen && <CameraAccessPrompt closeModal={this.closeModal} />}
      </PaymentContainer>
    );
  }
}

CreditCardFields.propTypes = {
  creditFieldLabels: PropTypes.shape({}),
  cardTypeImgUrl: PropTypes.string,
  isPLCCEnabled: PropTypes.bool,
  cardType: PropTypes.string,
  dto: PropTypes.shape({}),
  selectedCard: PropTypes.shape({}),
  showCvv: PropTypes.bool,
  cameraIcon: PropTypes.bool,
  onCardFocus: PropTypes.func,
  isExpirationRequired: PropTypes.bool,
  createRefParent: PropTypes.func,

  isEdit: PropTypes.bool.isRequired,
  selectedExpYear: PropTypes.string.isRequired,
  selectedExpMonth: PropTypes.string.isRequired,
  updateCardDetails: PropTypes.func.isRequired,
  getCardType: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired,
  expMonthOptionsMap: PropTypes.shape([]).isRequired,
  expYearOptionsMap: PropTypes.shape([]).isRequired,
  updateExpiryDate: PropTypes.func.isRequired,
  creditCard: PropTypes.shape({}).isRequired,
  cvvInfo: PropTypes.shape({}).isRequired,
};

CreditCardFields.defaultProps = {
  creditFieldLabels: {
    creditCardNumber: '',
    expMonth: '',
    expYear: '',
    cvvCode: '',
    cameraText: '',
  },
  cardTypeImgUrl: '',
  cardType: '',
  isPLCCEnabled: true,
  dto: {},
  selectedCard: null,
  showCvv: true,
  cameraIcon: false,
  onCardFocus: () => {},
  isExpirationRequired: true,
  createRefParent: () => {},
};

export default CreditCardFields;
export { CreditCardFields as CreditCardFieldsVanilla };
