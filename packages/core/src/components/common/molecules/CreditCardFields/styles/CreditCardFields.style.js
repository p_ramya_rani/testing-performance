// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .cvv-field {
    position: relative;
  }
  .cvv-icon {
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
    position: absolute;
    right: 0;
    top: 24px;
  }
  input {
    background-color: ${props =>
      props.variation === 'secondary' ? props.theme.colorPalette.gray[300] : ''};
  }
  .exp-year-field {
    margin-right: auto;
  }
  .select-field {
    height: auto;
  }

  @media ${props => props.theme.mediaQuery.smallMax} {
    .exp-year-field {
      width: 30%;
    }
    .exp-month-field {
      width: 30%;
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
`;

export default styles;
