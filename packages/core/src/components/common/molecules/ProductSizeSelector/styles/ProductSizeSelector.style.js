// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .size-and-fit-detail-container {
    .size-and-fit-detail-title-msg {
      font-size: ${(props) => props.theme.fonts.fontSize.body.small.secondary}px;
      font-weight: ${(props) => props.theme.fonts.fontWeight.medium};
      margin-left: 25px;
      vertical-align: middle;
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    }
  }

  .size-and-fit-detail-items-list {
    margin-top: ${(props) => (props.isPDP ? '6px' : props.theme.spacing.ELEM_SPACING.XS)};
    width: calc(100% + 10px);
    position: relative;
    left: -5px;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    display: flex;
    flex-flow: wrap;
  }

  /* Radio Button for size */
  .size-and-fit-detail-item {
    padding: 0px;
    margin: 3px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    }
    cursor: pointer;

    .input-radio-title {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
        ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
        ${(props) => props.theme.spacing.ELEM_SPACING.XS_6}
        ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      border: 1px solid ${(props) => props.theme.colorPalette.gray[900]};
      display: flex;
      flex: 1;
      justify-content: center;
      align-items: center;
      text-transform: capitalize;
      font-size: ${(props) => props.theme.fonts.fontSize.anchor.small}px;
      color: ${(props) => props.theme.colorPalette.gray[900]};
      border-radius: 6px;
      text-align: center;
      width: auto;
      min-width: 70px;
      height: 16px;
      letter-spacing: 0.42px;
    }

    .input-radio-title:hover {
      font-size: ${(props) => props.theme.typography.fontSizes.fs10};
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
      line-height: ${(props) => props.theme.fonts.lineHeight.normal};
      border-radius: 6px;
      border: solid 1px ${(props) => props.theme.colors.PRIMARY.DARK};
      background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
      color: ${(props) => props.theme.colors.TEXT.DARKERGRAY};
    }
    &.item-disabled-option {
      outline: none;
    }
    &.item-disabled-option .input-radio-title {
      ${(props) =>
        !props.newQVDesign
          ? `color: ${props.theme.colors.PRIMARY.LIGHTERGRAY}; cursor: initial; border: solid 1px ${props.theme.colors.PRIMARY.LIGHTERGRAY}`
          : ''}
    }

    .input-radio-icon-unchecked,
    .input-radio-icon-checked {
      width: auto;
      height: auto;
      top: auto;
      position: absolute;
      vertical-align: middle;

      input {
        -webkit-appearance: none;
        display: none;
      }

      &:before,
      &:after {
        display: none;
      }
    }

    .input-radio-icon-checked + .input-radio-title {
      background: ${(props) =>
        props.newQVDesign ? props.theme.colors.WHITE : props.theme.colors.PRIMARY.DARK};
      color: ${(props) =>
        props.newQVDesign ? props.theme.colors.BLACK : props.theme.colors.WHITE};
      ${(props) =>
        props.newQVDesign
          ? `border: solid 3px ${props.theme.colors.BRAND.PRIMARY}`
          : `border: solid 1px ${props.theme.colors.PRIMARY.DARK}`};
      outline: ${(props) => (props.newQVDesign ? 'solid 3px transparent' : '')};
      font-size: ${(props) =>
        props.newQVDesign
          ? props.theme.typography.fontSizes.fs14
          : props.theme.typography.fontSizes.fs10};
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      font-weight: ${(props) =>
        props.newQVDesign ? props.theme.fonts.fontWeight.bold : props.theme.fonts.fontWeight.black};
      line-height: ${(props) => props.theme.fonts.lineHeight.normal};
      border-radius: 6px;
      box-shadow: ${(props) =>
        props.newQVDesign ? '0 5px 12px 0 rgba(0, 0, 0, 0.12)' : 'inherit'};
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        font-size: ${(props) =>
          props.newQVDesign
            ? props.theme.typography.fontSizes.fs12
            : props.theme.typography.fontSizes.fs10};
      }
    }
  }
  .quick-view-drawer-redesign.size-and-fit-detail-item {
    margin: 5px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin: 6px;
    }
  }
  .quick-view-drawer-redesign.input-radio-title {
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    border: 1px solid gray;
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    color: ${(props) => props.theme.colors.BLACK};
    height: 16px;
    width: auto;
    min-width: 54px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      padding: 9px ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      height: 12px;
      min-width: 34px;
    }
  }
  .quick-view-drawer-redesign.input-radio-title:hover {
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    color: ${(props) => props.theme.colors.BLACK};
    height: 16px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      padding: 9px ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      height: 12px;
    }
  }
  &.item-disabled-option .quick-view-drawer-redesign.input-radio-title {
    border: 1px dashed ${(props) => props.theme.colors.PRIMARY.GRAY};
    opacity: 0.3;
    color: ${(props) => props.theme.colors.BLACK};
  }
  .quick-view-drawer-redesign.input-radio-icon-unchecked,
  .quick-view-drawer-redesign.input-radio-icon-checked {
    width: auto;
    height: auto;
    top: auto;
    position: absolute;
    vertical-align: middle;

    input {
      -webkit-appearance: none;
      display: none;
    }

    &:before,
    &:after {
      display: none;
    }
  }
`;

export default styles;
