// 9fbef606107a605d69c0edbcd8029e5d
/** @module ProductSizeSelector
 * @summary renders either a LabeledSelect or a LabeledRadioButtonGroup with radio buttons looking like chips to select the
 * size for a product.
 *
 * Any extra props other than <code>sizesMap, isRenderChips, isShowZeroInventoryEntries, className</code>),
 * e.g., <code>title, disabled</code>, passed to this component will be passed along to the rendered <code>LabeledRadioButtonGroup</code>
 * or <code>LabeledSelect</code> element.
 *
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import LabeledRadioButtonGroup from '../../LabeledRadioButtonGroup';
import withStyles from '../../../hoc/withStyles';
import styles from '../styles/ProductSizeSelector.style';

const getSizesOptionsMap = (
  sizesMap,
  isDisableZeroInventoryEntries,
  isRenderChips,
  keepAlive,
  isLoading,
  checkForOOSForVariant,
  isPickUpSelected
) => {
  if (isRenderChips) {
    return sizesMap.map((sizeEntry) => ({
      value: sizeEntry.get('displayName'),
      title: sizeEntry.get('displayName'),
      content: <span>{sizeEntry.get('displayName')}</span>,
      disabled: !isLoading
        ? (!isPickUpSelected &&
            isDisableZeroInventoryEntries &&
            sizeEntry.get('maxAvailable') <= 0) ||
          keepAlive ||
          checkForOOSForVariant
        : isLoading,
    }));
  }
  return null;
};

class ProductSizeSelector extends React.PureComponent {
  static propTypes = {
    /** the sizes to chose from */
    sizesMap: PropTypes.arrayOf(
      PropTypes.shape({
        sizeName: PropTypes.string.isRequire,
        maxAvailable: PropTypes.number.isRequired, // the maximum value of any nested maxAvailable value
      })
    ).isRequired,

    /** flags to render a LabeledRadioButtonGroup displaying chips instead of a LabeledSelect for size selection */
    isRenderChips: PropTypes.bool,

    /** flags if to show sizes with zero inventory */
    isShowZeroInventoryEntries: PropTypes.bool,

    /** flags if to disable sizes with zero inventory. Defaults to true. */
    isDisableZeroInventoryEntries: PropTypes.bool,

    title: PropTypes.string,
    keepAlive: PropTypes.bool,
    className: PropTypes.string,
    isLoading: PropTypes.bool.isRequired,
    checkForOOSForVariant: PropTypes.bool,
    newQVDesign: PropTypes.bool,
    isErrorMessageDisplayed: PropTypes.bool,
    errorMessage: PropTypes.string,
    isNewPDPEnabled: PropTypes.bool,
    isSKUValidated: PropTypes.bool,
    isFit: PropTypes.bool,
  };

  static defaultProps = {
    title: 'Size: ',
    isDisableZeroInventoryEntries: true,
    isShowZeroInventoryEntries: true,
    isRenderChips: true,
    keepAlive: false,
    className: '',
    checkForOOSForVariant: false,
    newQVDesign: false,
    isErrorMessageDisplayed: false,
    errorMessage: '',
    isNewPDPEnabled: false,
    isSKUValidated: false,
    isFit: false,
  };

  render() {
    const {
      sizesMap,
      isRenderChips,
      isShowZeroInventoryEntries,
      isDisableZeroInventoryEntries,
      className,
      keepAlive,
      isLoading,
      checkForOOSForVariant,
      newQVDesign,
      isErrorMessageDisplayed,
      errorMessage,
      isNewPDPEnabled,
      isSKUValidated,
      isFit,
      isPickUpSelected,
      ...otherProps
    } = this.props;

    const optionsMap =
      sizesMap &&
      getSizesOptionsMap(
        sizesMap,
        isDisableZeroInventoryEntries,
        isRenderChips,
        keepAlive,
        isLoading,
        checkForOOSForVariant,
        isPickUpSelected
      );

    return (
      sizesMap && (
        <LabeledRadioButtonGroup
          className={`${className} ${
            newQVDesign ? 'quick-view-drawer-redesign' : ''
          } size-and-fit-detail`}
          optionsMap={optionsMap}
          {...otherProps}
          shouldNotSplitLabel
          newQVDesign={newQVDesign}
          isErrorMessageDisplayed={isErrorMessageDisplayed}
          errorMessage={errorMessage}
          isNewPDPEnabled={isNewPDPEnabled}
          isSKUValidated={isSKUValidated}
          isFit={isFit}
        />
      )
    );
  }
}

export default withStyles(ProductSizeSelector, styles);
