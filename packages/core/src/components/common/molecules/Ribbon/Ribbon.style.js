// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';

export const RibbonWrapper = styled.div`
  bottom: 58px;
  position: absolute;
  right: ${(props) => (props.position === 'right' ? '0' : '')};
  left: ${(props) => (props.position === 'left' ? '0' : '')};
`;

export const RibbonPromoBanner = styled(PromoBanner)`
  .style1 {
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs36};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    color: ${(props) => props.theme.colorPalette.white};
  }
`;

export const RibbonContainer = styled.div`
  background: transparent url(${(props) => props.imgPath}) no-repeat 0 0;
  background-size: cover;
  right: 0;
  width: ${(props) => props.width}px;
  height: ${(props) => props.height}px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;
  margin-bottom: 12px;
`;
