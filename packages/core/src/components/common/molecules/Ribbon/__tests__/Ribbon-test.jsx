// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import Ribbon from '../Ribbon';

describe('RibbonComponent', () => {
  let wrapper;
  it('should match snapshot', () => {
    wrapper = shallow(
      <Ribbon width="200" height="70" color="white" locator="ribbon_test_locator" />
    );
    expect(wrapper).toMatchSnapshot();
  });
  it('should call the getConfig getImgPath', () => {
    wrapper = shallow(
      <Ribbon
        ribbonBanner={[
          {
            textItems: [[Object]],
            link: { url: '/c/girls-clothes', text: 'Girls', title: 'Girls', target: '' },
            ribbonPlacement: 'right',
            ribbonClass: 'ribbon-color-cherry',
          },
        ]}
        width="200"
        height="70"
        color="white"
        locator="ribbon_test_locator"
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
