// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import Ribbon from '../Ribbon.native';

describe('RibbonComponentVanilla', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Ribbon navigation={{}} width="200px" height="70px" color="white" />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
