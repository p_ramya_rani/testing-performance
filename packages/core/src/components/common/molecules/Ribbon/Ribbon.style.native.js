// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const RibbonContainer = styled.View`
  position: absolute;
  bottom: 88;
  ${props => (props.position === 'right' ? `right: 0;` : `left: 0;`)};
`;

export const PromoTextContainer = styled.View`
  position: absolute;
  top: 11px;
  ${props => (props.position === 'right' ? `right: 30px;` : `left: 30px;`)};
`;
