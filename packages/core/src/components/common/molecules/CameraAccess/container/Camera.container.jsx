// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import CameraAccessPrompt from '../views';

const mapStateToProps = state => {
  const qrScannerLabels = state && state.Labels && state.Labels.global.qrScanner;
  return { qrScannerLabels };
};

export default connect(mapStateToProps)(CameraAccessPrompt);
