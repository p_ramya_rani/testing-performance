import React from 'react';
import { shallow } from 'enzyme';
import CameraPrompt from '../CameraAccessPrompt.native';

describe('CameraPrompt component', () => {
  it('renders correctly', () => {
    const props = {
      qrScannerLabels: {},
    };
    const component = shallow(<CameraPrompt {...props} />);
    expect(component).toMatchSnapshot();
  });
});
