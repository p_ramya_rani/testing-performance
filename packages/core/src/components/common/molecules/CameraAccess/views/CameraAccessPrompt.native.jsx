// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { openSettings } from 'react-native-permissions';
import { getScreenWidth, getScreenHeight } from '@tcp/core/src/utils/utils.app';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ModalNative from '../../Modal/view/Modal.native';
import {
  Wrapper,
  Container,
  StyledImage,
  Touchable,
  StyledBodyCopy,
  StyledButton,
  MessageContainer,
  ShadowContainer,
} from '../styles/CameraAccessPrompt.native';

const closeImage = require('../../../../../../src/assets/close.png');

/**
 * Module height and width.
 */
const PROPMT_WIDTH = getScreenWidth() - 60;
const HEIGHT = getScreenHeight();

const close = closeModal => {
  closeModal();
};

/**
 * @settings : To open the setting modal in ios .
 */

const settings = closeModal => {
  closeModal();
  openSettings();
};

/**
 * @CameraAccessPrompt : To open the modal in ios and android .
 */
const CameraAccessPrompt = props => {
  const { qrScannerLabels, closeModal } = props;
  return qrScannerLabels && Object.keys(qrScannerLabels).length ? (
    <ModalNative isOpen onRequestClose={close} customTransparent>
      <ShadowContainer height={HEIGHT}>
        <Container>
          <Wrapper width={PROPMT_WIDTH}>
            <Touchable accessibilityRole="button" onPress={() => close(closeModal)}>
              <StyledImage source={closeImage} width="15px" height="15px" />
            </Touchable>
            <MessageContainer>
              <StyledBodyCopy
                text={getLabelValue(qrScannerLabels, 'lbl_qrscan_allow_camera_access')}
                textAlign="center"
                fontWeight="black"
                fontFamily="secondary"
                fontSize="fs18"
                marginTop="55px"
              />

              <StyledBodyCopy
                text={getLabelValue(qrScannerLabels, 'lbl_qrscan_for_qr_scanner')}
                textAlign="center"
                fontWeight="black"
                fontFamily="secondary"
                fontSize="fs12"
                marginTop="15px"
              />

              <StyledBodyCopy
                text={getLabelValue(qrScannerLabels, 'lbl_qrscan_description')}
                textAlign="center"
                fontFamily="secondary"
                fontSize="fs12"
                marginTop="10px"
              />

              <StyledBodyCopy
                text={getLabelValue(qrScannerLabels, 'lbl_qrscan_sub_description')}
                textAlign="center"
                fontFamily="secondary"
                fontSize="fs12"
              />

              <StyledButton
                text={getLabelValue(qrScannerLabels, 'lbl_qrscan_allow_camera_access')}
                fill="BLACK"
                marginTop="12px"
                width="260px"
                onPress={() => settings(closeModal)}
              />
            </MessageContainer>
          </Wrapper>
        </Container>
      </ShadowContainer>
    </ModalNative>
  ) : null;
};

CameraAccessPrompt.propTypes = {
  qrScannerLabels: PropTypes.shape({}).isRequired,
  closeModal: PropTypes.shape({}).isRequired,
};

export { CameraAccessPrompt as CameraAccessPromptVanilla };
export default CameraAccessPrompt;
