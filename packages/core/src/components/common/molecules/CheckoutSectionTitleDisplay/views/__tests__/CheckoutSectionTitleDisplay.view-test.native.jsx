// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import CheckoutSectionTitleDisplay from '../CheckoutSectionTitleDisplay.view.native';

describe('CheckoutSectionTitleDisplay', () => {
  it('should render correctly', () => {
    const tree = shallow(<CheckoutSectionTitleDisplay title="hello" />);
    expect(tree).toMatchSnapshot();
  });
});
