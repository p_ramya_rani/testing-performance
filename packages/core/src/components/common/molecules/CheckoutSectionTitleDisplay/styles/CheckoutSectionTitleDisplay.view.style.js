// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  width: 100%;
  .checkoutSectionTitle {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
    margin-left: 0px;
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    border-radius: 0.5px;
    border-bottom: 3px solid ${props => props.theme.colorPalette.black};
    @media ${props => props.theme.mediaQuery.mediumMax} {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
`;

export default styles;
