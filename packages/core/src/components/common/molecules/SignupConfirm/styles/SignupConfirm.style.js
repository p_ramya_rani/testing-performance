// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const SignupConfirmStyle = css`
  margin-top: 90px;
  .confirmation-label {
    margin: 5px auto 0;
    ::after {
      content: '';
      height: 2px;
      width: 100px;
      background: ${props =>
        props.theme.isGymboree
          ? props.theme.colorPalette.primary.main
          : props.theme.colors.TEXT.DARKERBLUE};
      margin: 10px auto 48px;
      display: block;
    }
  }
  .confirmation-image {
    margin: 0 auto 15px;
    display: flex;
    width: 50px;
    height: 50px;
  }
  .first-label {
    margin: 0 60px 5px;
  }
  .redeem-label {
    margin: 0 60px 26px;
  }
  .tnc-label {
    font-size: ${props => props.theme.typography.fontSizes.fs10};
    text-align: center;
    margin: 0px 24px 12px;
  }
  @media ${props => props.theme.mediaQuery.medium} {
    .first-label {
      margin: 0 78px 12px;
    }
    .redeem-label {
      margin: 0 88px 26px;
    }
    .tnc-label {
      margin: 0 78px 183px;
    }
  }
  @media ${props => props.theme.mediaQuery.large} {
    margin-top: 31px;
    .confirmation-label {
      margin: 2px auto;
      ::after {
        margin: 24px auto 48px;
      }
    }
    .first-label {
      margin-bottom: 24px;
      margin-top: 0px;
      margin-right: 82px;
      margin-left: 98px;
      padding-top: 9px;
    }
    .tnc-label {
      margin: 0 148px 111px;
    }
  }
  .thank-you__label {
    font-kerning: none;
  }
`;

export default SignupConfirmStyle;
