// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
// import { Anchor, DamImage, Button } from '../../../atoms';

export const Container = styled.View`
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.SM};
  min-height: 100%;
  position: relative;
`;

export const TextCarouselWrapper = styled.View`
  align-items: center;
  justify-content: center;
`;

export const SingleTextView = styled.View`
  padding-bottom: 70px;
`;

export const ButtonsWrapper = styled.View`
  margin: 0 3%;
  justify-content: center;
  margin-bottom: 20px;
`;

export const LoaderContainer = styled.View`
  align-items: center;
`;

export const TextCarouselView = styled.View`
  background-color: ${props => props.theme.colors.WHITE};
  display: flex;
  flex: 1;
  justify-content: space-between;
  width: 100%;
  bottom: 0;
  margin-bottom: 25px;
`;

export const CarouselWrapper = styled.View`
  flex: 2.5;
  display: flex;
  justify-content: center;
`;

export const ActionsWrapper = styled.View`
  flex: 1.5;
  display: flex;
  justify-content: flex-start;
`;

export default {
  Container,
  TextCarouselWrapper,
  LoaderContainer,
  SingleTextView,
  TextCarouselView,
  CarouselWrapper,
  ActionsWrapper,
};
