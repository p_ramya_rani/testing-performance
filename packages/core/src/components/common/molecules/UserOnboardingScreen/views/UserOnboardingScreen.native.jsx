// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { View, Modal, Image } from 'react-native';

import GuestLoginOverview from '@tcp/core/src/components/features/account/common/molecule/GuestLoginModule';
import Spinner from '@tcp/core/src/components/common/atoms/Spinner';
import ClickTracker from '../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import { BodyCopy, Anchor } from '../../../atoms';
import Carousel from '../../Carousel';
import names from '../../../../../constants/eventsName.constants';

import { getBrand, getScreenWidth } from '../../../../../utils/index.native';

import {
  Container,
  TextCarouselWrapper,
  ButtonsWrapper,
  LoaderContainer,
  SingleTextView,
  TextCarouselView,
  CarouselWrapper,
  ActionsWrapper,
} from '../styles/UserOnboardingScreen.style.native';
import config from '../UserOnboardingScreen.config';
import useShowMainModal from '../hooks/useShowMainModal.native';
import useUserOnboardingModuleData from '../hooks/useUserOnboardingModuleData.native';

/**
 * Module height and width.
 * Height is fixed for mobile : TCP & Gymb
 * Width can vary as per device width.
 */
const HERO_CAROUSEL_MODULE_HEIGHT = 317;
const CAROUSEL_MODULE_WIDTH = getScreenWidth();

const { CAROUSEL_OPTIONS } = config;

/* Carousel slide render function for Hero Image Carousel */
const renderHeroImgCarousel = (carouselImg) => {
  const { item } = carouselImg;

  // TODO: Need to update GraphQL for a new Schema to get video support
  // const { image, video } = item;

  /*  const videoData = video &&
    video.url && {
      ...video,
      videoWidth: CAROUSEL_MODULE_WIDTH,
      videoHeight: HERO_CAROUSEL_MODULE_HEIGHT,
    }; */

  return (
    // eslint-disable-next-line react-native/no-inline-styles
    <Image source={item.image} style={{ width: '100%' }} />
  );
  // videoData = { videoData };
};

/* Carousel slide render function for Promo Text Carousel */
const renderPromoTextCarousel = (carouselPromoText) => {
  const { item } = carouselPromoText;
  const {
    headLine: [headLine],
    textItems,
  } = item;

  return (
    <TextCarouselWrapper>
      <BodyCopy
        text={headLine.text}
        fontSize="fs32"
        fontFamily="primary"
        textAlign="center"
        fontWeight="medium"
        margin="24px 0"
        color="text.primary"
      />
      {textItems.map((description) => {
        return (
          <BodyCopy
            text={description.text}
            fontSize="fs14"
            fontFamily="secondary"
            textAlign="center"
            fontWeight="extrabold"
            color="text.primary"
          />
        );
      })}
    </TextCarouselWrapper>
  );
};

const UserOnboardingScreen = (props) => {
  const {
    userOnboardingLabels,
    navigation,
    overviewLabels,
    isUserLoggedIn,
    onModalActionComplete,
    userOnboardingModuleId,
    trackClickAction,
    setClickAnalyticsDataAction,
  } = props;

  const brandId = getBrand();

  const { showMainModal, setShowMainModal, setModalActionComplete } = useShowMainModal(
    overviewLabels,
    isUserLoggedIn,
    onModalActionComplete,
    userOnboardingModuleId
  );
  const moduleData = useUserOnboardingModuleData(
    userOnboardingModuleId,
    showMainModal,
    setShowMainModal
  );

  const page = 'home page';
  const commonVariables = {
    pageName: page,
    pageType: page,
    pageSection: page,
    pageSubSection: page,
    pageSubSubSection: page,
    pageShortName: page,
  };
  const shoppingAnalyticsData = {
    ...commonVariables,
    pageNavigationText: names.screenNames.shopping,
    customEvents: 'event156',
  };

  const loginAnalyticsData = {
    ...commonVariables,
    pageNavigationText: names.screenNames.login,
    customEvents: ['event155', 'event126'],
  };

  const joinAnalyticsData = {
    ...commonVariables,
    pageNavigationText: names.screenNames.join,
    customEvents: ['event154', 'event125'],
  };

  const { textCarousel } = moduleData || [];

  const mediaList =
    brandId === 'tcp'
      ? [
          { image: require('../../../../../../../mobileapp/onboarding/tcp/ONBOARDING1.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/tcp/ONBOARDING2.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/tcp/ONBOARDING3.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/tcp/ONBOARDING4.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/tcp/ONBOARDING5.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/tcp/ONBOARDING6.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/tcp/ONBOARDING7.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/tcp/ONBOARDING8.jpg') },
        ]
      : [
          { image: require('../../../../../../../mobileapp/onboarding/gym/ONBOARDING1.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/gym/ONBOARDING2.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/gym/ONBOARDING3.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/gym/ONBOARDING4.jpg') },
          { image: require('../../../../../../../mobileapp/onboarding/gym/ONBOARDING5.jpg') },
        ];

  return (
    <Modal visible={showMainModal}>
      {moduleData ? (
        <Container>
          {/* ------ Hero Carousel Image Start */}
          {mediaList.length > 1 ? (
            <View pointerEvents="none">
              <Carousel
                height={HERO_CAROUSEL_MODULE_HEIGHT}
                data={mediaList}
                renderItem={renderHeroImgCarousel}
                width={CAROUSEL_MODULE_WIDTH}
                options={{
                  loop: true,
                  autoplayInterval: CAROUSEL_OPTIONS.speed,
                }}
              />
            </View>
          ) : (
            <View>{renderHeroImgCarousel({ item: mediaList[0] })}</View>
          )}
          {/* ------ Hero Carousel Image End ------- */}
          {/* ------ Carousel Text Start */}
          <TextCarouselView>
            <CarouselWrapper>
              {textCarousel.length > 1 ? (
                <Carousel
                  data={textCarousel}
                  width={CAROUSEL_MODULE_WIDTH}
                  renderItem={renderPromoTextCarousel}
                  paginationProps={{
                    containerStyle: {
                      paddingVertical: 0,
                      paddingTop: 46,
                      paddingBottom: 24,
                    },
                  }}
                  options={{
                    autoplay: false,
                  }}
                  showDots
                />
              ) : (
                <SingleTextView>
                  {renderPromoTextCarousel({ item: textCarousel[0] })}
                </SingleTextView>
              )}
              {/* ------ Carousel Text End ------- */}
            </CarouselWrapper>
            <ActionsWrapper>
              <ButtonsWrapper>
                <GuestLoginOverview
                  fromOnBoarding
                  loginAnalyticsData={loginAnalyticsData}
                  joinAnalyticsData={joinAnalyticsData}
                  hideLogoutText
                  loggedInWrapperStyle="margin:0"
                  labels={overviewLabels}
                  navigation={navigation}
                  isUserLoggedIn={isUserLoggedIn}
                  setClickAnalyticsDataAction={setClickAnalyticsDataAction}
                  trackClickAction={trackClickAction}
                />
              </ButtonsWrapper>
              <ClickTracker
                as={Anchor}
                clickData={shoppingAnalyticsData}
                name={names.screenNames.shopping}
                module="global"
                underline
                anchorVariation="primary"
                fontSizeVariation="xlarge"
                text={userOnboardingLabels.startShoppingButtonLabel}
                onPress={() => {
                  setShowMainModal(false);
                  setModalActionComplete(true);
                }}
              />
            </ActionsWrapper>
          </TextCarouselView>
        </Container>
      ) : (
        <LoaderContainer>
          <Spinner />
        </LoaderContainer>
      )}
    </Modal>
  );
};

UserOnboardingScreen.defaultProps = {
  userOnboardingLabels: {},
  overviewLabels: null,
  isUserLoggedIn: false,
  onModalActionComplete: () => {},
  userOnboardingModuleId: '',
  setClickAnalyticsDataAction: () => {},
  trackClickAction: () => {},
};

UserOnboardingScreen.propTypes = {
  userOnboardingLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}).isRequired,
  overviewLabels: PropTypes.arrayOf(PropTypes.object),
  isUserLoggedIn: PropTypes.bool,
  onModalActionComplete: PropTypes.func,
  userOnboardingModuleId: PropTypes.string,
  setClickAnalyticsDataAction: PropTypes.func,
  trackClickAction: PropTypes.func,
};

export default UserOnboardingScreen;
export { UserOnboardingScreen as UserOnboardingScreenVanilla };
