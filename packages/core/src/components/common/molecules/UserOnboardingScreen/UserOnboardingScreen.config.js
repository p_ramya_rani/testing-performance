// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  CAROUSEL_OPTIONS: {
    autoplay: true,
    speed: 2000,
  },
  IMG_DATA: {
    heroImgConfig: [
      't_user_onboarding_hero_img_m',
      't_user_onboarding_hero_img_t',
      't_user_onboarding_hero_img_d',
    ],
  },
};
