// 9fbef606107a605d69c0edbcd8029e5d 
import { useState, useEffect } from 'react';
import UserOnboardingModuleAbstractor from '@tcp/core/src/services/abstractors/common/userOnboardingModule';

const useUserOnboardingModuleData = (moduleId, showMainModal, setShowMainModal) => {
  const [moduleData, setModuleData] = useState(null);

  useEffect(() => {
    if (moduleId) {
      UserOnboardingModuleAbstractor.getData(moduleId)
        .then(data => {
          setModuleData(data.composites);
        })
        .catch(() => {
          setShowMainModal(false);
        });
    }
  }, [moduleId, showMainModal]);

  return moduleData;
};

export default useUserOnboardingModuleData;
