// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Espot from '@tcp/core/src/components/common/molecules/Espot';

const ModuleX = props => {
  const {
    className,
    richTextList: [{ text }],
    dataLocator,
    moduleClassName,
    isNativeView,
    loyaltyLocation,
  } = props;

  return (
    <React.Fragment>
      <Espot
        className={`${className} ${moduleClassName} moduleX`}
        richTextHtml={text}
        dataLocator={dataLocator || `moduleX`}
        isNativeView={isNativeView}
        loyaltyLocation={loyaltyLocation}
      />
    </React.Fragment>
  );
};

ModuleX.propTypes = {
  className: PropTypes.string.isRequired,
  richTextList: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.string)).isRequired,
  dataLocator: PropTypes.string.isRequired,
  moduleClassName: PropTypes.string,
  isNativeView: PropTypes.bool,
  loyaltyLocation: PropTypes.string,
};

ModuleX.defaultProps = {
  moduleClassName: '',
  isNativeView: false,
  loyaltyLocation: '',
};
export default ModuleX;
