// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import get from 'lodash/get';

import { styleOverrideEngine } from '../../../../utils';

const getCustomStyle = styleOverrides => {
  const { marginBottom, marginTop } = styleOverrides;
  const marginBottomVal = (marginBottom && marginBottom['margin-bottom']) || 0;
  const marginTopVal = (marginTop && marginTop['margin-top']) || 0;

  return {
    marginBottom: parseInt(marginBottomVal, 10),
    marginTop: parseInt(marginTopVal, 10),
  };
};

const ModuleX = props => {
  const {
    richTextList: [{ text: html }],
    navigation,
    isNativeView,
    moduleClassName,
  } = props;
  let route = get(navigation, 'state.routeName', false);
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleX') || {};

  /* this condition can be removed if same behavior is intented for all page reticting it home for now */
  route = route === 'Home' ? route : '';
  return (
    <View style={getCustomStyle(styleOverrides)}>
      <Espot
        richTextHtml={html}
        navigation={navigation}
        isNativeView={isNativeView}
        routeName={route}
      />
    </View>
  );
};

ModuleX.propTypes = {
  richTextList: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.string)).isRequired,
  navigation: PropTypes.shape({}),
  isNativeView: PropTypes.bool,
  moduleClassName: PropTypes.string,
};

ModuleX.defaultProps = {
  navigation: {},
  isNativeView: false,
  moduleClassName: '',
};
export default ModuleX;
