// 9fbef606107a605d69c0edbcd8029e5d 
import * as React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Banner404 from '../views/Banner404.view';

const props = {
  labels: {
    header404TCP_PLP: 'Heading',
    subHeader404TCP_PLP: 'SubHeading',
  },
  pageName: 'plpTCP',
};

test('matches snapshot', () => {
  const wrapper = shallow(<Banner404 {...props} />);
  expect(toJson(wrapper)).toMatchSnapshot();
});
