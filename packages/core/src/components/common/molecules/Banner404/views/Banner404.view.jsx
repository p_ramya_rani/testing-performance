// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '../../../atoms';

const getHeaderForPage = (labels, page) => {
  if (page === 'plpTCP') return labels.header404TCP_PLP;
  if (page === 'plpGYM') return labels.header404GYM_PLP;
  if (page === 'categoryGYM') return labels.header404GYM_CLP;
  if (page === 'categoryTCP') return labels.header404TCP_CLP;
  return null;
};

const getSubHeaderForPage = (labels, page) => {
  if (page === 'plpTCP') return labels.subHeader404TCP_PLP;
  if (page === 'plpGYM') return labels.subHeader404GYM_PLP;
  if (page === 'categoryGYM') return labels.subHeader404GYM_CLP;
  if (page === 'categoryTCP') return labels.subHeader404TCP_CLP;
  return null;
};

const containerStyles = {
  margin: '18px auto 48px auto',
  padding: '37px 0',
  border: 'solid 1px #1a1a1a',
  borderRadius: '16px',
  wordWrap: 'break-word',
};

const headerStyles = {
  plpTCP: {
    margin: '0 0 6px',
    fontFamily: 'Nunito',
    fontSize: '36px',
    fontWeight: 'bold',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#1a1a1a',
  },
  plpCatGYM: {
    margin: '0 0 10px',
    fontFamily: 'Montserrat',
    fontSize: '36px',
    fontWeight: '500',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#1a1a1a',
  },
  plpMobileTCP: {
    margin: '0 0 5px',
    fontFamily: 'Nunito',
    fontSize: '20px',
    fontWeight: 'bold',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#1a1a1a',
  },
  plpCatMobileGYM: {
    margin: '0 0 5px',
    fontFamily: 'Montserrat',
    fontSize: '20px',
    fontWeight: '500',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#1a1a1a',
  },
};

const subHeaderStyles = {
  plpTCP: {
    margin: '6px 0 0',
    fontFamily: 'Nunito',
    fontSize: '26px',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#1a1a1a',
  },
  plpCatGYM: {
    margin: '10px 0 0',
    fontFamily: 'Montserrat',
    fontSize: '26px',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#1a1a1a',
  },
  plpCatMobileTCP: {
    margin: '5px 0 0',
    fontFamily: 'Nunito',
    fontSize: '16px',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: '1.25',
    letterSpacing: 'normal',
    color: '#1a1a1a',
  },
  plpCatMobileGYM: {
    margin: '5px 0 0',
    fontFamily: 'Montserrat',
    fontSize: '16px',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: '1.25',
    letterSpacing: 'normal',
    color: '#1a1a1a',
  },
};

const getHeaderStyles = (page, isMobileWeb) => {
  if (page === 'plpTCP' || page === 'categoryTCP') {
    if (isMobileWeb) return headerStyles.plpMobileTCP;
    return headerStyles.plpTCP;
  }
  if (page === 'plpGYM' || page === 'categoryGYM') {
    if (isMobileWeb) return headerStyles.plpCatMobileGYM;
    return headerStyles.plpCatGYM;
  }
  return null;
};

const getSubHeaderStyles = (page, isMobileWeb) => {
  if (page === 'plpTCP' || page === 'categoryTCP') {
    if (isMobileWeb) return subHeaderStyles.plpMobileTCP;
    return subHeaderStyles.plpTCP;
  }
  if (page === 'plpGYM' || page === 'categoryGYM') {
    if (isMobileWeb) return subHeaderStyles.plpCatMobileGYM;
    return subHeaderStyles.plpCatGYM;
  }
  return null;
};

const Banner404 = ({ labels, pageName, isMobileWeb }) => {
  return (
    <div style={containerStyles}>
      <BodyCopy style={getHeaderStyles(pageName, isMobileWeb)} textAlign="center">
        {getHeaderForPage(labels, pageName)}
      </BodyCopy>
      <BodyCopy style={getSubHeaderStyles(pageName, isMobileWeb)} textAlign="center">
        {getSubHeaderForPage(labels, pageName)}
      </BodyCopy>
    </div>
  );
};

Banner404.propTypes = {
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  pageName: PropTypes.string,
  isMobileWeb: PropTypes.bool,
};

Banner404.defaultProps = {
  labels: {},
  pageName: '',
  isMobileWeb: false,
};

export { Banner404 as Banner404Vanilla };
export default Banner404;
