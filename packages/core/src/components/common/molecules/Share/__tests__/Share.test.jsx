import React from 'react';
import { shallow } from 'enzyme';
import { ShareVanilla as Share } from '../views/Share';

describe('Share component', () => {
  const props = {
    ratingsProductId: '',
  };

  it('should match snapshot', () => {
    const component = shallow(<Share {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call toggle', () => {
    const toggleSocial = jest.fn();
    const component = shallow(<Share {...props} />);
    expect(component.state('showSocialIcons')).toBe(false);
    component.find("[className='share-icon-new-pdp']").simulate('click');
    toggleSocial.mockImplementation();
    expect(component.state('showSocialIcons')).toBe(true);
    expect(component).toMatchSnapshot();
  });
});
