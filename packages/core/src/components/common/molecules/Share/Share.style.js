import { css } from 'styled-components';

const ShareStyle = css`
  &.social-share-new-pdp {
    width: 190px;
    height: 44px;
    border-radius: 6px;
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.25);
    background-color: ${(props) => props.theme.colors.WHITE};
    position: absolute;
    right: 0;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      right: 42px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      right: 28px;
    }
  }
  &.share-icon-wrapper-new-pdp {
    width: 41px;
    height: 41px;
    margin: 1px 1px 2px 3px;
    padding: 0 0.1px 0.1px 0;
    opacity: 0.9;
    border-radius: 10px;
    background-color: ${(props) => props.theme.colors.WHITE};
    right: 46px;
    @media ${(props) => props.theme.mediaQuery.large} {
      right: 0px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      right: 28px;
    }
    position: absolute;
  }
  .share-icon-wrapper-new-pdp.expanded {
    right: 2px;
  }
  .share-icon-new-pdp {
    width: 26px;
    height: 26px;
    margin: 7px 9px 8px 9px;
    cursor: pointer;
  }
  .social-icons-new-pdp {
    width: 111px;
    height: 25px;
    margin: 0 3px 0 0;
    padding: 9px 17px 10px;
    border-radius: 6px 0 0 6px;
    background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  }
  .facebook-new-pdp {
    width: 25px;
    height: 25px;
    cursor: pointer;
  }
  .twitter-new-pdp {
    width: 25px;
    height: 25px;
    cursor: pointer;
    margin-left: 13px;
    margin-right: 18px;
  }
  .pinterest-new-pdp {
    width: 32px;
    height: 25px;
    cursor: pointer;
  }
`;

export default ShareStyle;
