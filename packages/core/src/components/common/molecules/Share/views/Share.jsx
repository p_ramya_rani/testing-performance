import React from 'react';
import PropTypes from 'prop-types';
import SocialConnect from '@tcp/core/src/components/common/organisms/ProductImages/views/SocialConnect.view';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath } from '@tcp/core/src/utils';
import { Image } from '@tcp/core/src/components/common/atoms';
import ShareStyle from '../Share.style';

class Share extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSocialIcons: false,
    };
    this.toggleSocial = this.toggleSocial.bind(this);
  }

  toggleSocial() {
    this.setState((prevState) => ({
      showSocialIcons: !prevState.showSocialIcons,
    }));
  }

  render() {
    const shareIconPath = getIconPath('share-icon-3x');
    const { showSocialIcons } = this.state;
    const {
      className,
      isFacebookEnabled,
      isPinterestEnabled,
      isTwitterEnabled,
      accessibilityLabels,
      ratingsProductId,
    } = this.props;
    const share = (
      <div
        className={`${className} share-icon-wrapper-new-pdp ${showSocialIcons ? 'expanded' : ''}`}
      >
        <Image onClick={this.toggleSocial} src={shareIconPath} className="share-icon-new-pdp" />
      </div>
    );
    const social = (
      <SocialConnect
        className="social-icons-new-pdp"
        isFacebookEnabled={isFacebookEnabled}
        isPinterestEnabled={isPinterestEnabled}
        isTwitterEnabled={isTwitterEnabled}
        accessibilityLabels={accessibilityLabels}
        ratingsProductId={ratingsProductId}
        newShareIcon
        isNewPDPEnabled
      />
    );
    const socialShare = (
      <div className={`${className} social-share-new-pdp`}>
        {share}
        {social}
      </div>
    );

    return showSocialIcons ? socialShare : share;
  }
}

Share.defaultProps = {
  className: '',
  isTwitterEnabled: true,
  isFacebookEnabled: true,
  isPinterestEnabled: true,
  accessibilityLabels: {},
};

Share.propTypes = {
  className: PropTypes.string,
  isTwitterEnabled: PropTypes.bool,
  isFacebookEnabled: PropTypes.bool,
  isPinterestEnabled: PropTypes.bool,
  accessibilityLabels: PropTypes.shape({}),
  ratingsProductId: PropTypes.string.isRequired,
};

export default withStyles(Share, ShareStyle);
export { Share as ShareVanilla };
