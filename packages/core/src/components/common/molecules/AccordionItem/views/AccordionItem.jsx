// 9fbef606107a605d69c0edbcd8029e5d 
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import AccordionHeader from '../../AccordionHeader';

import styles from '../AccordionItem.style';
import withStyles from '../../../hoc/withStyles';

/**
 * @function AccordionItem The accordion component accepts the header
 * title which would go in the accordion header and a body of the accordion
 * as is passed in the children of the accordion.
 * @param {string} className The class name for the component
 * @param {string} titleText The title text for the accordion header
 * @param {array} listArray The array of list to determine the active or inactive state
 * @param {number} index The index of the accordion in the accordion list.
 * @param {node} children The children node for the accordion as passed on the accordionList.
 */
const AccordionItem = ({
  className,
  titleText,
  updateAccordionState,
  index,
  children,
  activeClass,
  appliedFilterComponent,
  filterLength,
  showFilterSelectedCount,
}) => {
  return (
    <Fragment>
      <AccordionHeader
        className={`${className} accordion ${activeClass}`}
        titleText={titleText}
        updateAccordionState={updateAccordionState}
        index={index}
        appliedFilterComponent={appliedFilterComponent}
        filterLength={filterLength}
        showFilterSelectedCount={showFilterSelectedCount}
      />
      {children}
    </Fragment>
  );
};

AccordionItem.defaultProps = {
  showFilterSelectedCount: false,
};

AccordionItem.propTypes = {
  className: PropTypes.string.isRequired,
  titleText: PropTypes.string.isRequired,
  updateAccordionState: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
  activeClass: PropTypes.string.isRequired,
  appliedFilterComponent: PropTypes.shape([]).isRequired,
  filterLength: PropTypes.number.isRequired,
  showFilterSelectedCount: PropTypes.bool,
};

export default withStyles(AccordionItem, styles);
export { AccordionItem as AccordionVanilla };
