// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const Container = styled.View`
  margin-top: 3px;
  flex-direction: row;
  border: solid 1px ${props => props.theme.colors.PRIMARY.DARK};
  background-color: ${props => props.theme.colors.PRIMARY.PALEGRAY};
  width: 100%;
`;

export const ItemView = styled.TouchableOpacity`
  padding: 5px 14px 0px 14px;
  margin: 14px 7px;
  border-radius: ${props => props.theme.spacing.ELEM_SPACING.MED};
  justify-content: center;
  border-color: ${props => props.theme.colorPalette.gray[2300]};
  background-color: ${props => props.theme.colorPalette.white};
  ${props => (props.isSelected ? `border: solid 1px ${props.selectedColor};` : '')};
  width: 138px;
`;

export const getVerticalTextStyle = () => {
  return {
    width: 312,
    height: 18,
    textAlign: 'center',
    transform: [{ rotate: '-90deg' }],
    justifyContent: 'center',
  };
};

export const VerticalBanner = styled.View`
  width: 30px;
  height: 251px;
  background: #1a1a1a;
  justify-content: center;
  align-items: center;
  text-align: center;
  margin-right: 7px;
`;

export const TitleText = styled.Text`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  font-family: ${props => props.theme.typography.fonts.primary};
  font-size: ${props => props.theme.typography.fontSizes.fs14};
  font-weight: ${props => props.theme.typography.fontWeights.extrabold};
  color: ${props => props.theme.colorPalette.gray[900]};
  text-align: center;
  width: 118px;
  height: 12px;
  flex: 1;
  flex-wrap: wrap;
`;

export const getContentContainerStyle = (listLeftMargin, listRightMargin) => {
  return {
    paddingRight: listRightMargin,
    paddingLeft: listLeftMargin,
  };
};
