// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const leftCarouselPath = getIconPath('arrowWithBg');

const styles = css`
  width: 100%;
  .l3-nav-carousel-container {
    display: inline-block;
    width: 94px;
    height: 144px;
    border-radius: ${props => props.theme.spacing.ELEM_SPACING.XS_6};
    box-shadow: 0 ${props => props.theme.spacing.ELEM_SPACING.XXXS}
      ${props => props.theme.spacing.ELEM_SPACING.XS} 0 rgba(0, 0, 0, 0.15);
    margin: ${props => props.theme.spacing.ELEM_SPACING.XXXS} 14px
      ${props => props.theme.spacing.ELEM_SPACING.MED}
      ${props => props.theme.spacing.ELEM_SPACING.XXXS};
    vertical-align: top;
    :first-child {
      margin-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
    }
    @media ${props => props.theme.mediaQuery.medium} {
      width: 100px;
      height: 131px;
      :first-child {
        margin-left: 13px;
      }
    }
    @media ${props => props.theme.mediaQuery.large} {
      width: 92px;
      height: 121px;
      :first-child {
        margin-left: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
      }
    }
    &:hover,
    &:focus,
    &.selected-filter {
      box-shadow: 0 ${props => props.theme.spacing.ELEM_SPACING.XXXS}
        ${props => props.theme.spacing.ELEM_SPACING.XS} 0 rgba(0, 0, 0, 0.15);
      border: solid ${props => props.theme.spacing.ELEM_SPACING.XXXS}
        ${props => props.theme.colorPalette.blue[500]};
      @media ${props => props.theme.mediaQuery.large} {
        width: 88px;
      }
    }
  }
  .l3-nav-carousel-container-new {
    background: ${props => props.theme.colors.WHITE};
    display: inline-block;
    width: 138px;
    height: 223px;
    box-shadow: 0 ${props => props.theme.spacing.ELEM_SPACING.XXXS}
      ${props => props.theme.spacing.ELEM_SPACING.XS} 0 rgba(0, 0, 0, 0.15);
    margin: ${props => props.theme.spacing.ELEM_SPACING.XXXS}
      ${props => props.theme.spacing.ELEM_SPACING.XXS}
      ${props => props.theme.spacing.ELEM_SPACING.MED}
      ${props => props.theme.spacing.ELEM_SPACING.XXXS};
    vertical-align: top;
    outline: ${props => props.theme.spacing.ELEM_SPACING.XXXS} solid transparent;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      width: 96px;
      height: 158px;
      :first-child {
        margin-left: 40px;
      }
      :last-child {
        margin-right: 10px;
      }
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      margin: 2px 12px 16px;
      :first-child {
        margin-left: 46px;
      }
      :last-child {
        margin-right: 45px;
      }
    }
    @media ${props => props.theme.mediaQuery.large} {
      margin: 2px 12px 16px;
      width: 181px;
      height: 275px;
      margin-top: 8px;
      margin-bottom: 24px;
      &:hover,
      &:focus,
      &.selected-filter-new {
        box-shadow: 0 ${props => props.theme.spacing.ELEM_SPACING.XXXS}
          ${props => props.theme.spacing.ELEM_SPACING.XS} 0 rgba(0, 0, 0, 0.15);
        outline: solid ${props => props.theme.spacing.ELEM_SPACING.XXXS}
          ${props => props.theme.colorPalette.blue[500]};
        height: 275px;
      }
    }

    &.selected-filter-new {
      box-shadow: 0 ${props => props.theme.spacing.ELEM_SPACING.XXXS}
        ${props => props.theme.spacing.ELEM_SPACING.XS} 0 rgba(0, 0, 0, 0.15);
      outline: solid ${props => props.theme.spacing.ELEM_SPACING.XXXS}
        ${props => props.theme.colorPalette.blue[500]};
    }
  }
  .container-carousel {
    overflow-x: scroll;
    overflow-y: hidden;
    width: 100vw;
    white-space: nowrap;
    -ms-overflow-style: none;
    scrollbar-width: none;
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    @media ${props => props.theme.mediaQuery.large} {
      overflow-x: hidden;
      width: calc(100% - 10px);
    }
  }
  .container-carousel.new-container {
    @media ${props => props.theme.mediaQuery.large} {
      width: calc(100% - 138px);
      height: 307px;
      margin-left: 80px;
    }
  }
  .container-carousel.new-container.few-items {
    display: flex;
    justify-content: center;
  }
  .container-carousel::-webkit-scrollbar {
    display: none;
  }
  .image-container {
    width: 77px;
    height: 97px;
    margin: ${props => props.theme.spacing.ELEM_SPACING.XS}
      ${props => props.theme.spacing.ELEM_SPACING.XS}
      ${props => props.theme.spacing.ELEM_SPACING.XXS};
    @media ${props => props.theme.mediaQuery.medium} {
      width: 59px;
      height: 74px;
    }
    @media ${props => props.theme.mediaQuery.large} {
      width: 55px;
      height: 68px;
      margin: 9px 19px ${props => props.theme.spacing.ELEM_SPACING.XXS} 18px;
    }
  }
  .image-container-new {
    width: 118px;
    height: 147px;
    margin: 5px ${props => props.theme.spacing.ELEM_SPACING.XS}
      ${props => props.theme.spacing.ELEM_SPACING.XXS};
    @media ${props => props.theme.mediaQuery.smallOnly} {
      width: 84px;
      height: 104px;
    }
    @media ${props => props.theme.mediaQuery.medium} {
      margin: 4px 11px;
    }
    @media ${props => props.theme.mediaQuery.large} {
      width: 160px;
      height: 199px;
      margin: 6px 19px 0px 11px;
    }
  }
  .item-container {
    white-space: nowrap;
    margin-left: -14px;
    position: relative;
    @media ${props => props.theme.mediaQuery.large} {
      margin-left: 0;
    }
  }
  .item-container-new {
    white-space: nowrap;
    margin-left: -14px;
    position: relative;
    outline: 1px solid ${props => props.theme.colors.PRIMARY.DARK};
    background: ${props => props.theme.colors.PRIMARY.PALEGRAY};
    margin-bottom: 10px;
    overflow-x: hidden;
    width: 1170px;
    @media ${props => props.theme.mediaQuery.large} {
      margin-left: 0;
      width: calc(100% - 4px);
    }
    @media ${props => props.theme.mediaQuery.smallOnly} {
      width: 99.5vw;
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      width: 96.4vw;
      margin-left: -2px;
    }
  }
  .title-container {
    white-space: normal;
    height: 38px;
  }
  .title-container-new {
    white-space: normal;
    height: 36px;
    color: ${props => props.theme.colors.PRIMARY.DARK};
    width: 160px;
    margin-left: 10px;
    margin-top: 21px;
    margin-bottom: 12px;
    justify-content: center;
    display: flex;
    align-items: center;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      margin-left: 8px;
      width: 84px;
      height: 28px;
      margin-top: 18px;
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      width: 118px;
    }
  }
  .right-arrow {
    display: none;
    z-index: 1;
    @media ${props => props.theme.mediaQuery.large} {
      display: block;
      right: -24px;
      position: absolute;
      top: 22px;
      width: 76px;
      height: 76px;
      background: url(${leftCarouselPath}) center center no-repeat;
      background-size: 100% 100%;
      transform: rotate(180deg);
    }
  }
  .right-arrow-new {
    display: none;
    @media ${props => props.theme.mediaQuery.large} {
      display: block;
      right: 6px;
      position: absolute;
      top: 120px;
      width: 76px;
      height: 76px;
      background: url(${leftCarouselPath}) center center no-repeat;
      background-size: 100% 100%;
      transform: rotate(180deg);
    }
  }
  .left-arrow {
    display: none;
    z-index: 1;
    @media ${props => props.theme.mediaQuery.large} {
      display: block;
      left: -28px;
      position: absolute;
      top: 22px;
      width: 76px;
      height: 76px;
      background: url(${leftCarouselPath}) center center no-repeat;
      background-size: 100% 100%;
    }
  }
  .left-arrow-new {
    display: none;
    @media ${props => props.theme.mediaQuery.large} {
      display: block;
      left: 28px;
      position: absolute;
      top: 120px;
      width: 76px;
      height: 76px;
      background: url(${leftCarouselPath}) center center no-repeat;
      background-size: 100% 100%;
    }
  }
  .image-link-new {
    border-bottom: 1px solid ${props => props.theme.colors.PRIMARY.DARK};
    display: block;
    padding-bottom: 10px;
    height: 100%;
  }
  .title-section {
    width: 192px;
    height: 32px;
    background: #000;
    position: absolute;
    margin-top: -10px;
    margin-left: -14px;
    transform: rotate(270deg);
    transform-origin: 0 0;
    line-height: 30px;
    text-decoration: uppercase;
    font-size: 16px;
    overflow: hidden;
    white-space: nowrap;
    @media ${props => props.theme.mediaQuery.medium} {
      width: 257px;
      margin: -10px 0px 0px -3px;
    }
    @media ${props => props.theme.mediaQuery.large} {
      margin-top: -9px;
      margin-left: auto;
      width: 325px;
    }
  }
  .plp-carousel-banner-text {
    margin-top: 4px;
  }
`;

export default styles;
