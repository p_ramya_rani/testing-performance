// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import categoryFilters4, { accessibilityData } from './mock';
import { L3NavigationCarouselVanilla as L3NavigationCarousel } from '../views/L3NavigationCarousel.view.native';

let PromoFilterComp;

jest.mock('@tcp/core/src/utils', () => ({
  isClient: () => {
    return true;
  },
  getViewportInfo: () => ({
    isDesktop: true,
  }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  getIconPath: jest.fn(),
  isMobileApp: jest.fn(),
  getAsPathWithSlug: jest.fn(),
  getMappedPageHref: jest.fn(),
  buildUrl: jest.fn(),
  getVideoUrl: jest.fn(),
  isSafariBrowser: jest.fn(),
  getBrand: jest.fn(),
}));

const navigation = {
  navigate: () => {},
  getParam: () => {
    return {};
  },
};

describe('L3NavigationCarousel component', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
        showCarousel
        variation="desktop"
        navigation={navigation}
      />
    );
    PromoFilterComp = shallow(wrapper.get(0));

    expect(PromoFilterComp).toMatchSnapshot();
  });

  it('doesnt render when showCarousel is not true', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
        navigation={navigation}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('it renders correctly when showPLPGenericCarousel is true', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
        showCarousel
        showPLPGenericCarousel
        navigation={navigation}
      />
    );
    PromoFilterComp = shallow(wrapper.get(0));

    expect(PromoFilterComp).toMatchSnapshot();
  });
  it('does not render  when showPLPGenericCarousel is true showCarousel is not true', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
        showPLPGenericCarousel
        navigation={navigation}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
