// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import categoryFilters4, { accessibilityData } from './mock';
import { L3NavigationCarouselVanilla as L3NavigationCarousel } from '../views/L3NavigationCarousel.view';

let PromoFilterComp;
const rightArrow = '.right-arrow';

jest.mock('@tcp/core/src/utils', () => ({
  isClient: () => {
    return true;
  },
  getViewportInfo: () => ({
    isDesktop: true,
  }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  getIconPath: jest.fn(),
  isMobileApp: jest.fn(),
  getAsPathWithSlug: jest.fn(),
  getMappedPageHref: jest.fn(),
  buildUrl: jest.fn(),
  getVideoUrl: jest.fn(),
  isSafariBrowser: jest.fn(),
  getBrand: jest.fn(),
}));

describe('L3NavigationCarousel component', () => {
  const funcGetEl = document.getElementById;
  beforeEach(() => {
    document.getElementById = function getElementById() {
      return {
        offsetWidth: '100',
        scrollTo: () => {},
      };
    };
  });

  afterEach(() => {
    document.getElementById = funcGetEl;
  });

  it('renders correctly for desktop', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
        showCarousel
        variation="desktop"
      />
    );
    wrapper.setState({ showLeftArrow: true, showRightArrow: true });
    PromoFilterComp = shallow(wrapper.get(0));

    expect(PromoFilterComp).toMatchSnapshot();
  });

  it('doesnt render when showCarousel is not true', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('scrolls correctly when click on right button clicked', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
        showCarousel
        variation="desktop"
      />
    );
    wrapper.setState({ showLeftArrow: true, showRightArrow: true });
    wrapper.find(rightArrow).simulate('click');
    PromoFilterComp = shallow(wrapper.get(0));

    expect(PromoFilterComp).toMatchSnapshot();
    expect(wrapper.find(rightArrow).length).toBe(1);
  });
  it('it renders correctly when showPLPGenericCarousel is true', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
        showCarousel
        showPLPGenericCarousel
      />
    );
    wrapper.setState({ showLeftArrow: true, showRightArrow: true });
    PromoFilterComp = shallow(wrapper.get(0));

    expect(PromoFilterComp).toMatchSnapshot();
  });
  it('does not render  when showPLPGenericCarousel is true showCarousel is not true', () => {
    const wrapper = shallow(
      <L3NavigationCarousel
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
        showPLPGenericCarousel
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
