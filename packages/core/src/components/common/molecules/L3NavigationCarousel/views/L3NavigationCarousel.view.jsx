// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isClient, getViewportInfo } from '@tcp/core/src/utils';
import { IMG_DATA_PLP_CAT_FILTERS } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/config';
import withStyles from '../../../hoc/withStyles';
import { Anchor, DamImage, BodyCopy, Button } from '../../../atoms';
import styles from '../styles/L3NavigationCarousel.style';

let SINGLE_ITEM_WIDTH = 205;
const { isMobile, isTablet } = getViewportInfo() || {};
if (isMobile) {
  SINGLE_ITEM_WIDTH = 160;
} else if (isTablet) {
  SINGLE_ITEM_WIDTH = 162;
}
export class L3NavigationCarousel extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    categoryFilters: PropTypes.shape(
      PropTypes.arrayOf(
        PropTypes.oneOfType(
          PropTypes.shape({
            image: PropTypes.shape({}),
            link: PropTypes.shape({}),
          })
        )
      )
    ).isRequired,
    asPath: PropTypes.string.isRequired,
    accessibilityLabels: PropTypes.shape({}).isRequired,
    showCarousel: PropTypes.bool,
    variation: PropTypes.string,
    carouselTitle: PropTypes.string,
    showPLPGenericCarousel: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    showCarousel: false,
    variation: '',
    carouselTitle: '',
    showPLPGenericCarousel: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      showLeftArrow: false,
      showRightArrow: false,
      reFocus: false,
      promoFilterRef: {},
    };
    this.promoFilter = React.createRef();
  }

  componentDidMount() {
    const { showPLPGenericCarousel } = this.props;
    if (!showPLPGenericCarousel) {
      this.updateSliderButtons();
    } else {
      this.updateSliderButtonsPLPGenericCarousel(true);
    }
  }

  componentDidUpdate(prevProps) {
    const { showPLPGenericCarousel, asPath, categoryFilters } = this.props;
    const shouldResetFocus =
      prevProps.asPath !== asPath || prevProps.categoryFilters?.length !== categoryFilters?.length;
    if (shouldResetFocus) {
      this.state.reFocus = true;
    }
    if (showPLPGenericCarousel) {
      this.updateSliderButtonsPLPGenericCarousel(shouldResetFocus);
    } else {
      this.updateSliderButtons();
    }
  }

  updateRefocus = () => {
    this.setState({ reFocus: false });
  };

  updateSliderButtons = (newScrollLeftPos) => {
    const { current: container } = this.promoFilter;
    if (!container) {
      return false;
    }
    let isLastSlideVisible = false;
    let isFirstSlideVisible = false;

    const scrollPos = newScrollLeftPos || container.scrollLeft;

    const containerWidth = container.offsetWidth;
    const fullContainerWidth = container.scrollWidth;

    if (
      (container.scrollLeft > 0 && container.scrollLeft <= scrollPos) ||
      containerWidth === fullContainerWidth
    ) {
      isLastSlideVisible = true;
    }
    if (container.scrollLeft === 0) {
      isFirstSlideVisible = true;
    }
    this.setState({
      showRightArrow: !isLastSlideVisible,
      showLeftArrow: !isFirstSlideVisible,
    });
    return false;
  };

  scrollCarousel = (direction) => {
    if (isClient()) {
      const { current: container } = this.promoFilter;
      if (container) {
        const containerWidth = container.offsetWidth;
        const fullContainerWidth = container.scrollWidth;
        const scrollLeftPos = container.scrollLeft;
        let newScrollLeftPos = scrollLeftPos;
        if (direction === 'left' && scrollLeftPos > 0) {
          newScrollLeftPos = Math.round(scrollLeftPos - containerWidth);
          if (newScrollLeftPos < 0) {
            newScrollLeftPos = 0;
          }
        }
        if (direction === 'right' && scrollLeftPos < fullContainerWidth) {
          newScrollLeftPos = Math.round(scrollLeftPos + containerWidth);
        }
        container.scrollTo({ left: newScrollLeftPos });
        this.updateSliderButtons(newScrollLeftPos);
      }
    }
  };

  updateSliderButtonsPLPGenericCarousel = (shouldResetFocus) => {
    const { current: container } = this.promoFilter;
    const { reFocus } = this.state;
    if (!container) {
      return false;
    }
    let isLastSlideVisible = false;
    let isFirstSlideVisible = false;
    const elements = Array.from(container.children);
    const selectedItem = elements.find(
      (item) => item && item.className && item.className.includes('selected-filter-new')
    );

    const containerWidth = container.offsetWidth;
    const fullContainerWidth = container.scrollWidth;
    if ((shouldResetFocus || reFocus) && selectedItem) {
      this.updateRefocus();
      if (
        selectedItem.offsetLeft - container.scrollLeft + SINGLE_ITEM_WIDTH > containerWidth ||
        container.scrollLeft > selectedItem.offsetLeft
      ) {
        const modVal = Math.floor(selectedItem.offsetLeft / SINGLE_ITEM_WIDTH);
        const newScrollLeftPos = SINGLE_ITEM_WIDTH * modVal;
        container.scrollTo({ left: newScrollLeftPos });
      }
    }
    if (container.scrollLeft + containerWidth > fullContainerWidth - 10) {
      isLastSlideVisible = true;
    }
    if (container.scrollLeft === 0) {
      isFirstSlideVisible = true;
    }
    this.setState({
      showRightArrow: !isLastSlideVisible,
      showLeftArrow: !isFirstSlideVisible,
    });
    return false;
  };

  scrollPLPGenericCarousel = (direction) => {
    if (isClient()) {
      const { current: container } = this.promoFilter;
      if (container) {
        const fullContainerWidth = container.scrollWidth;
        const scrollLeftPos = container.scrollLeft;
        let newScrollLeftPos = scrollLeftPos;
        if (direction === 'left' && scrollLeftPos > 0) {
          newScrollLeftPos = Math.round(scrollLeftPos - SINGLE_ITEM_WIDTH);
          if (newScrollLeftPos < 0) {
            newScrollLeftPos = 0;
          }
        }
        if (direction === 'right' && scrollLeftPos < fullContainerWidth) {
          newScrollLeftPos = Math.round(scrollLeftPos + SINGLE_ITEM_WIDTH);
        }
        container.scrollTo({ left: newScrollLeftPos });

        this.updateSliderButtonsPLPGenericCarousel(false);
      }
    }
  };

  renderItems = (categoryFilters, className, itemClassName, pageAsPath) => {
    const pageAsPathWithoutQuery = pageAsPath.split('?')[0];
    return (
      categoryFilters &&
      categoryFilters.map(({ url, title, filterTitle, linkUrl, linkAspath }) => {
        const toPath = linkUrl;
        const asPath = `${linkAspath}?viaModule=promoFilter`;
        const slideClass = 'image-container';
        let itemProps = { className: itemClassName };
        let isSelected = false;
        if (`/c/${pageAsPathWithoutQuery}` === linkAspath) {
          isSelected = true;
          itemProps = { className: `${itemClassName} selected-filter` };
        }
        return (
          <div key={filterTitle} {...itemProps}>
            <div className={slideClass}>
              <Anchor className="image-link" to={toPath} asPath={asPath}>
                <DamImage
                  className={`${className} carousel-image`}
                  imgConfigs={IMG_DATA_PLP_CAT_FILTERS.imgConfig}
                  imgData={{
                    alt: title,
                    url,
                  }}
                />
              </Anchor>
            </div>
            <div className="title-container">
              <Anchor to={toPath} asPath={asPath}>
                <BodyCopy
                  fontSize="fs12"
                  fontFamily="secondary"
                  textAlign="center"
                  color={isSelected ? 'black' : 'gray.1900'}
                  fontWeight={isSelected ? 'bold' : 'regular'}
                  className="multiline-ellipsis"
                >
                  {filterTitle}
                </BodyCopy>
              </Anchor>
            </div>
          </div>
        );
      })
    );
  };

  renderPlpGenericCarouselItems = (categoryFilters, className, itemClassName, pageAsPath) => {
    const pageAsPathWithoutQuery = pageAsPath.split('?')[0];
    return (
      categoryFilters &&
      categoryFilters.map(({ url, title, filterTitle, linkUrl, linkAspath }) => {
        const toPath = linkUrl;
        const asPath = `${linkAspath}?viaModule=promoFilter`;
        const slideClass = 'image-container-new';
        let itemProps = { className: itemClassName };
        if (`/c/${pageAsPathWithoutQuery}` === linkAspath) {
          itemProps = { className: `${itemClassName} selected-filter-new` };
        }
        return (
          <div key={filterTitle} {...itemProps}>
            <div className={slideClass}>
              <Anchor className="image-link-new" to={toPath} asPath={asPath}>
                <DamImage
                  className={`${className} carousel-image-new`}
                  imgConfigs={IMG_DATA_PLP_CAT_FILTERS.imgConfig}
                  imgData={{
                    alt: title,
                    url,
                  }}
                />
              </Anchor>
            </div>
            <Anchor className="title-container-new" to={toPath} asPath={asPath}>
              <BodyCopy
                fontSize={`${isMobile ? 'fs12' : 'fs14'}`}
                fontFamily="secondary"
                textAlign="center"
                fontWeight="extrabold"
                className="multiline-ellipsis"
              >
                {filterTitle.toUpperCase()}
              </BodyCopy>
            </Anchor>
          </div>
        );
      })
    );
  };

  getCarouselClassName = (categoryFilters, showPLPGenericCarousel, carouselClass) => {
    const { promoFilterRef } = this.state;
    const { current: container } = this.promoFilter;

    let newClass = carouselClass;
    const containerUpdated = container || promoFilterRef?.current;
    if (showPLPGenericCarousel) {
      newClass += ' new-container';
      if (containerUpdated?.scrollWidth <= containerUpdated?.offsetWidth) {
        newClass += ' few-items';
      }
    }
    return newClass;
  };

  renderPromoFilterItems = (categoryFilters, className, asPath, showPLPGenericCarousel) => {
    const { showLeftArrow, showRightArrow } = this.state;
    const { accessibilityLabels } = this.props;
    let carouselClass = 'container-carousel';
    if (!showLeftArrow && !showRightArrow) {
      carouselClass = 'container-carousel no-carousel';
    }
    carouselClass = this.getCarouselClassName(
      categoryFilters,
      showPLPGenericCarousel,
      carouselClass
    );

    return (
      <div className={`${showPLPGenericCarousel ? 'item-container-new' : 'item-container'}`}>
        {showLeftArrow && (
          <Button
            className={`${showPLPGenericCarousel ? 'left-arrow-new' : 'left-arrow'}`}
            type="button"
            onClick={() =>
              showPLPGenericCarousel
                ? this.scrollPLPGenericCarousel('left')
                : this.scrollCarousel('left')
            }
          />
        )}
        <div className={carouselClass} ref={this.promoFilter}>
          {showPLPGenericCarousel
            ? this.renderPlpGenericCarouselItems(
                categoryFilters,
                className,
                'l3-nav-carousel-container-new',
                asPath
              )
            : this.renderItems(categoryFilters, className, 'l3-nav-carousel-container', asPath)}
        </div>
        {showRightArrow && (
          <Button
            className={`${showPLPGenericCarousel ? 'right-arrow-new' : 'right-arrow'}`}
            type="button"
            aria-label={accessibilityLabels.lbl_right_arrow}
            onClick={() =>
              showPLPGenericCarousel
                ? this.scrollPLPGenericCarousel('right')
                : this.scrollCarousel('right')
            }
          />
        )}
      </div>
    );
  };

  render() {
    const {
      categoryFilters,
      className,
      asPath,
      showCarousel,
      variation,
      carouselTitle,
      showPLPGenericCarousel,
    } = this.props;
    const viewportObj = isClient() && getViewportInfo();

    if (
      !showCarousel ||
      (variation === 'desktop' && !viewportObj.isDesktop) ||
      (variation === 'mobile' && viewportObj.isDesktop)
    ) {
      return null;
    }
    if (this.promoFilter)
      this.setState({
        promoFilterRef: this.promoFilter,
      });
    return (
      <div className={className}>
        {this.renderPromoFilterItems(categoryFilters, className, asPath, showPLPGenericCarousel)}
        {showPLPGenericCarousel && (
          <div className="title-section">
            <BodyCopy
              fontWeight="bold"
              fontFamily="secondary"
              color="white"
              textAlign="center"
              className="plp-carousel-banner-text "
            >
              {carouselTitle}
            </BodyCopy>
          </div>
        )}
      </div>
    );
  }
}
export { L3NavigationCarousel as L3NavigationCarouselVanilla };
export default withStyles(L3NavigationCarousel, styles);
