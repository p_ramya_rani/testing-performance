// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useRef } from 'react';
import { PropTypes } from 'prop-types';
import { FlatList } from 'react-native';
import { withTheme } from 'styled-components';
import get from 'lodash/get';
import { BodyCopy, DamImage } from '@tcp/core/src/components/common/atoms';
import LineComp from '@tcp/core/src/components/common/atoms/Line';
import { isTCP, setSelectedOutfitColor } from '@tcp/core/src/utils/utils';
import { IMG_DATA_PLP_CAT_FILTERS } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/config';
import { findCategoryIdandName } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import {
  Container,
  ItemView,
  getContentContainerStyle,
  getVerticalTextStyle,
  VerticalBanner,
  TitleText,
} from '../styles/L3NavigationCarousel.style.native';

const scrollToIndex = (ref, index = 0) => {
  if (ref && ref.current && index > -1) {
    ref.current.scrollToIndex({ index, viewPosition: 0.5 });
  }
};

/**
 * @function renderItem populates the menu item conditionally
 * @param {object} item menu item object
 * @param {object} section contains the section title of the menu item
 */

const setBrandColor = () => (isTCP() ? 'blue.500' : 'orange.500');

const renderItem = (navigate, getParam, completeNavObj, title) => listProps => {
  const { item, index } = listProps;
  let { url } = item;
  const { filterTitle, linkUrl } = item;
  const isRedirectUrl = url && url.includes('#redirect');

  const navigateToNextScreen = () => {
    if (isRedirectUrl) {
      const categoryIds = linkUrl && linkUrl.split('c?cid=')[1];
      const categoryObj = findCategoryIdandName(
        completeNavObj,
        categoryIds.replace('#redirect', ''),
        null,
        true
      );
      const { url: newUrl } = categoryObj[0];

      url = newUrl;
    }
    const subCategories = getParam('navigationObj');
    return navigate('ProductListing', {
      navigationObj: subCategories,
      url: linkUrl,
      title: filterTitle,
      reset: true,
    });
  };

  const getImageUrl = itemObj => {
    return get(itemObj, 'url', '');
  };
  const getImageTitle = itemObj => {
    return get(itemObj, 'filterTitle', '');
  };
  const imgUrl = getImageUrl(item) || '';
  const brandColor = setSelectedOutfitColor();
  const brandLineColor = setBrandColor();

  return (
    <ItemView
      accessible={index}
      accessibilityRole="image"
      accessibilityLabel={`image ${index + 1}`}
      isSelected={title === item.filterTitle}
      onPress={() => navigateToNextScreen()}
      selectedColor={brandColor}
    >
      <DamImage
        key={index.toString()}
        imgConfigs={IMG_DATA_PLP_CAT_FILTERS.imgConfig}
        imgData={{
          alt: title,
          url: imgUrl,
        }}
        width={110}
        height={147}
      />
      <LineComp
        marginTop={12}
        borderColor={title === item.filterTitle ? brandLineColor : 'black'}
      />
      <TitleText>{getImageTitle(item)}</TitleText>
    </ItemView>
  );
};

const getSelectedIndex = (title, subCategories) => {
  return subCategories.findIndex(obj => obj.filterTitle === title);
};

/**
 * @param {object} props Props passed from Stack navigator screen and the parent L1
 */

const NavigationCarousel = props => {
  const {
    navigation: { navigate, getParam },
    title,
    listLeftMargin,
    listRightMargin,
    carouselTitle,
    categoryFilters,
  } = props;

  const flatListRef = useRef();
  const completeNavObj = getParam('completeNavObj');

  useEffect(() => {
    const selectedIndex = getSelectedIndex(title, categoryFilters);
    setTimeout(() => {
      scrollToIndex(flatListRef, selectedIndex);
    }, 50);
  }, [title, flatListRef]);

  return (
    <Container>
      <VerticalBanner>
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs16"
          fontWeight="semibold"
          color="white"
          text={carouselTitle}
          style={getVerticalTextStyle()}
        />
      </VerticalBanner>
      <FlatList
        ref={flatListRef}
        contentContainerStyle={getContentContainerStyle(listLeftMargin, listRightMargin)}
        scrollToOverflowEnabled
        initialNumToRender={categoryFilters.length + 1}
        onScrollToIndexFailed={info => {
          if (flatListRef !== null && info.index > -1) {
            setTimeout(
              () =>
                flatListRef.current.scrollToIndex({
                  index: info.index,
                  animated: true,
                  viewPosition: 0.5,
                }),
              50
            );
          }
        }}
        refreshing={false}
        data={categoryFilters}
        horizontal
        showsHorizontalScrollIndicator={false}
        listKey={(_, index) => index.toString()}
        renderItem={renderItem(navigate, getParam, completeNavObj, title)}
      />
    </Container>
  );
};

NavigationCarousel.propTypes = {
  navigation: PropTypes.shape({
    getParam: PropTypes.func.isRequired,
    navigate: PropTypes.func,
    goBack: PropTypes.func,
  }).isRequired,
  title: PropTypes.string.isRequired,
  listLeftMargin: PropTypes.number,
  listRightMargin: PropTypes.number,
  carouselTitle: PropTypes.string,
  categoryFilters: PropTypes.shape([]),
};
NavigationCarousel.defaultProps = {
  listLeftMargin: 0,
  listRightMargin: 0,
  carouselTitle: '',
  categoryFilters: [],
};

export { NavigationCarousel as L3NavigationCarouselVanilla };
export default withTheme(NavigationCarousel);
