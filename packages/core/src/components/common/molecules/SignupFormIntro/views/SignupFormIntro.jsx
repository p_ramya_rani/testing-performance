// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { isGymboree } from '@tcp/core/src/utils';
import { Col } from '@tcp/core/src/components/common/atoms';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import SignupFormIntroStyle from '../styles/SignupFormIntro.style';

const SignupFormIntro = ({ className, formViewConfig, noModal }) => (
  <Fragment>
    <Col
      colSize={{ small: 6, medium: 8, large: 12 }}
      ignoreGutter={{ large: true }}
      className={className}
    >
      <section id="sign-up-modal-form-intro-view">
        <BodyCopy
          fontSize={['fs18', 'fs18', 'fs22']}
          fontFamily="secondary"
          textAlign="center"
          className="sign-up__label"
        >
          {formViewConfig.lbl_SignUp_signUpForLabel}
        </BodyCopy>
        <BodyCopy
          fontSize={noModal ? 'fs28' : ['fs28', 'fs28', 'fs38']}
          fontFamily="primary"
          fontWeight="black"
          textAlign="center"
          className="offer-type__label"
        >
          {formViewConfig.lbl_SignUp_offerTypeLabel}
        </BodyCopy>
        <BodyCopy fontFamily="primary" className="flash-text" textAlign="center" component="div">
          <BodyCopy
            fontSize="fs48"
            component="span"
            className={!isGymboree() ? 'get-text' : 'first-text'}
            color="primary.main"
          >
            {formViewConfig.lbl_SignUp_getTextLabel}
          </BodyCopy>
          {!isGymboree() && (
            <>
              <BodyCopy
                fontSize="fs36"
                component="span"
                fontWeight="black"
                className="dollar-text"
                color="primary.main"
              >
                {formViewConfig.lbl_SignUp_dollarTextLabel}
              </BodyCopy>
              <BodyCopy
                fontSize="fs48"
                component="span"
                fontWeight="black"
                className="ten-text"
                color="primary.main"
              >
                {formViewConfig.lbl_SignUp_tenTextLabel}
              </BodyCopy>
            </>
          )}
          <BodyCopy
            fontSize="fs48"
            textAlign="center"
            className={!isGymboree() ? 'off-text' : 'know-text'}
            color="primary.main"
          >
            {formViewConfig.lbl_SignUp_offTextLabel}
            <span className="super-text">
              <BodyCopy fontSize="fs18" component="span" color="primary.main">
                {formViewConfig.lbl_SignUp_offTextLabel_suptext}
              </BodyCopy>
            </span>
          </BodyCopy>
        </BodyCopy>
        <BodyCopy
          className="desc-text"
          fontSize={noModal ? ['fs16', 'fs22', 'fs16'] : ['fs16', 'fs18', 'fs16']}
          fontFamily={isGymboree() ? 'primary' : 'secondary'}
          fontWeight="semibold"
          textAlign="center"
        >
          {formViewConfig.lbl_SignUp_nextPurchaseLabel}
        </BodyCopy>
      </section>
    </Col>
  </Fragment>
);

SignupFormIntro.propTypes = {
  formViewConfig: PropTypes.shape({}),
  className: PropTypes.string,
  noModal: PropTypes.bool,
};

SignupFormIntro.defaultProps = {
  formViewConfig: {},
  className: '',
  noModal: false,
};

export { SignupFormIntro as SignupFormIntroVanilla };
export default withStyles(SignupFormIntro, SignupFormIntroStyle);
