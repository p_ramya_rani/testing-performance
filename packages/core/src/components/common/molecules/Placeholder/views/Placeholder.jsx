// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import LeftNavigation from '@tcp/core/src/components/common/molecules/LeftNavigation';

function RenderDynamicModule(properties) {
  const modules = {
    'help-center-left-navigation': LeftNavigation,
  };

  const { className, val, selectedPage, pageName } = properties;
  const Module = modules[val];
  return (
    <Module
      data={properties[val]}
      className={className}
      selectedPage={selectedPage}
      defaultPage={pageName}
    />
  );
}

const Placeholder = props => {
  return <RenderDynamicModule {...props} />;
};

Placeholder.propTypes = {
  props: PropTypes.shape({}),
};

Placeholder.defaultProps = {
  props: {},
};

export default Placeholder;
