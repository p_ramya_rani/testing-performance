// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const leftCarouselPath = getIconPath('carousel-big-carrot-left');

const styles = css`
  .image-container {
    border-radius: 2px;
    box-shadow: 0 1px 4px 0 rgba(88, 88, 88, 0.5);
    margin: 2px 10px 8px 2px;
    padding: 4px;
    &:hover,
    &:focus,
    &.selected-filter {
      box-shadow: 0 2px 4px 0 rgba(167, 164, 164, 0.5);
      border: solid 1px ${props => props.theme.colorPalette.blue[700]};
    }
  }
  .title-container {
    text-align: center;
    white-space: normal;
    vertical-align: top;
    margin: 0 10px 16px 0;
    height: 37px;
  }
  .container-carousel {
    overflow-x: scroll;
    overflow-y: hidden;
    width: 100%;
    white-space: nowrap;
    -ms-overflow-style: none;
    scrollbar-width: none;
  }
  .container-carousel::-webkit-scrollbar {
    display: none;
  }

  .item-container {
    position: relative;
  }
  .filter-carousel-inner-container {
    display: inline-block;
    width: 30%;
    max-width: 30%;
    scroll-snap-type: x mandatory;
    vertical-align: top;
    @media ${props => props.theme.mediaQuery.medium} {
      width: 24%;
    }
    @media ${props => props.theme.mediaQuery.large} {
      width: 20%;
    }
  }

  .left-arrow,
  .right-arrow {
    display: none;
  }

  @media ${props => props.theme.mediaQuery.large} {
    .container-carousel {
      overflow: hidden;
      margin-left: 46px;
      display: inline-block;
      width: calc(100% - 92px);
      &.no-carousel {
        margin-left: 0;
        width: 100%;
      }
    }
    .left-arrow,
    .right-arrow {
      display: inline-block;
      background: url(${leftCarouselPath}) center center no-repeat;
      transform: inherit;
      height: 52px;
      border: none;
      width: 15px;
      margin: 0;
      position: absolute;
      top: 30%;
    }
    .right-arrow {
      transform: rotate(180deg);
      right: 0;
    }
  }
`;

export default styles;
