// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import categoryFilters4, { categoryFilters5, accessibilityData } from './mock';
import { PromoFilterVanilla as PromoFilter } from '../views/PromoFilter.view';

let PromoFilterComp;
const rightArrow = '.right-arrow';

jest.mock('@tcp/core/src/utils', () => ({
  isClient: () => {
    return true;
  },
  getViewportInfo: jest.fn(),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  getIconPath: jest.fn(),
  isMobileApp: jest.fn(),
  getAsPathWithSlug: jest.fn(),
  getMappedPageHref: jest.fn(),
  buildUrl: jest.fn(),
  getVideoUrl: jest.fn(),
  isSafariBrowser: jest.fn(),
  getBrand: jest.fn(),
}));

describe('Promo filter component', () => {
  const funcGetEl = document.getElementById;
  beforeEach(() => {
    document.getElementById = function getElementById() {
      return {
        offsetWidth: '100',
        scrollTo: () => {},
      };
    };
  });

  afterEach(() => {
    document.getElementById = funcGetEl;
  });

  it('renders correctly', () => {
    const wrapper = shallow(
      <PromoFilter
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
      />
    );
    wrapper.setState({ showLeftArrow: true, showRightArrow: true });
    PromoFilterComp = shallow(wrapper.get(0));

    expect(PromoFilterComp).toMatchSnapshot();
  });

  it('scrolls correctly when click on right button clicked', () => {
    const wrapper = shallow(
      <PromoFilter
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniforms"
        accessibilityLabels={accessibilityData}
      />
    );
    wrapper.setState({ showLeftArrow: true, showRightArrow: true });
    wrapper.find(rightArrow).simulate('click');
    PromoFilterComp = shallow(wrapper.get(0));

    expect(PromoFilterComp).toMatchSnapshot();
    expect(wrapper.find(rightArrow).length).toBe(1);
  });

  it('scrolls correctly when left button clicked', () => {
    const wrapper = shallow(
      <PromoFilter
        categoryFilters={categoryFilters4}
        className="promofilter"
        asPath="toddler-boy-school-uniformss"
        accessibilityLabels={accessibilityData}
      />
    );

    wrapper.setState({ showLeftArrow: true, showRightArrow: true });
    wrapper.find('.left-arrow').simulate('click');
    PromoFilterComp = shallow(wrapper.get(0));

    expect(PromoFilterComp).toMatchSnapshot();
    expect(wrapper.find('.left-arrow').length).toBe(1);
  });

  it('click on left button and scroll correctly', () => {
    const wrapper = shallow(
      <PromoFilter
        categoryFilters={categoryFilters5}
        className="promofilter"
        asPath="toddler-boy-school-uniformss"
        accessibilityLabels={accessibilityData}
      />
    );

    wrapper.setState({ showLeftArrow: true, showRightArrow: true });
    expect(wrapper.find(rightArrow).length).toBe(1);
    wrapper.find(rightArrow).simulate('click');
    PromoFilterComp = shallow(wrapper.get(0));
    expect(wrapper.find(rightArrow).length).toBe(1);
    expect(PromoFilterComp).toMatchSnapshot();
  });
});
