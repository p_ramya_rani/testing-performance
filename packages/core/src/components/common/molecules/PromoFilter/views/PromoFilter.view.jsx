// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isClient } from '@tcp/core/src/utils';
import { IMG_DATA_PLP_CAT_FILTERS } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/config';
import withStyles from '../../../hoc/withStyles';
import { Anchor, DamImage, BodyCopy, Button } from '../../../atoms';
import styles from '../styles/PromoFilter.style';

export class PromoFilter extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    categoryFilters: PropTypes.shape(
      PropTypes.arrayOf(
        PropTypes.oneOfType(
          PropTypes.shape({
            image: PropTypes.shape({}),
            link: PropTypes.shape({}),
          })
        )
      )
    ).isRequired,
    asPath: PropTypes.string.isRequired,
    accessibilityLabels: PropTypes.shape({}).isRequired,
  };

  static defaultProps = {
    className: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      showLeftArrow: false,
      showRightArrow: false,
    };
    this.promoFilter = React.createRef();
  }

  componentDidMount() {
    this.updateSliderButtons();
  }

  isLastSlide = (currentScrollPos, noOfSlides, widthOfEachSlide) => {
    const noOfSlidesVisible = 5;
    const noOfSlidesScrolled = noOfSlidesVisible + Math.round(currentScrollPos / widthOfEachSlide);
    if (noOfSlides === noOfSlidesScrolled) {
      return true;
    }
    return false;
  };

  isFirstSlide = (currentScrollPos) => currentScrollPos === 0;

  updateSliderButtons = (widthOfEachSlide, newScrollLeftPos = 0) => {
    const { categoryFilters } = this.props;
    const numberOfSlides = categoryFilters && categoryFilters.length;
    const isLastSlide = widthOfEachSlide
      ? this.isLastSlide(newScrollLeftPos, numberOfSlides, widthOfEachSlide)
      : numberOfSlides <= 5;
    const isFirstSlide = this.isFirstSlide(newScrollLeftPos);
    this.setState({
      showRightArrow: !isLastSlide,
      showLeftArrow: !isFirstSlide,
    });
  };

  scrollCarousel = (direction) => {
    if (isClient()) {
      const { current: container } = this.promoFilter;
      if (container) {
        const containerWidth = container.offsetWidth;
        const fullContainerWidth = container.scrollWidth;
        const widthOfEachSlide = containerWidth / 5;
        const scrollLeftPos = container.scrollLeft;
        let newScrollLeftPos = scrollLeftPos;
        if (direction === 'left' && scrollLeftPos > 0) {
          newScrollLeftPos = Math.round(scrollLeftPos - widthOfEachSlide);
        }
        if (direction === 'right' && scrollLeftPos < fullContainerWidth) {
          newScrollLeftPos = Math.round(scrollLeftPos + widthOfEachSlide);
        }
        container.scrollTo({ left: newScrollLeftPos });
        this.updateSliderButtons(widthOfEachSlide, newScrollLeftPos);
      }
    }
  };

  renderItems = (categoryFilters, className, itemClassName, pageAsPath) => {
    const itemProps = { className: itemClassName };
    const pageAsPathWithoutQuery = pageAsPath.split('?')[0];
    return (
      categoryFilters &&
      categoryFilters.map(({ url, title, filterTitle, linkUrl, linkAspath }) => {
        const toPath = linkUrl;
        const asPath = `${linkAspath}?viaModule=promoFilter`;
        let slideClass = 'image-container';
        let isSelected = false;
        if (`/c/${pageAsPathWithoutQuery}` === linkAspath) {
          slideClass = 'image-container selected-filter';
          isSelected = true;
        }
        return (
          <div key={filterTitle} {...itemProps}>
            <div className={slideClass}>
              <Anchor className="image-link" to={toPath} asPath={asPath}>
                <DamImage
                  className={`${className} carousel-image`}
                  imgConfigs={IMG_DATA_PLP_CAT_FILTERS.imgConfig}
                  imgData={{
                    alt: title,
                    url,
                  }}
                />
              </Anchor>
            </div>
            <div className="title-container">
              <Anchor to={toPath} asPath={asPath}>
                <BodyCopy
                  fontSize={['fs12', 'fs13', 'fs14']}
                  fontFamily="secondary"
                  textAlign="center"
                  color={isSelected ? 'black' : 'gray.1900'}
                  fontWeight={isSelected ? 'bold' : 'regular'}
                  className="multiline-ellipsis"
                >
                  {filterTitle}
                </BodyCopy>
              </Anchor>
            </div>
          </div>
        );
      })
    );
  };

  renderPromoFilterItems = (categoryFilters, className, asPath) => {
    const { showLeftArrow, showRightArrow } = this.state;
    const { accessibilityLabels } = this.props;
    let carouselClass = 'container-carousel';
    if (!showLeftArrow && !showRightArrow) {
      carouselClass = 'container-carousel no-carousel';
    }
    return (
      <div className="item-container">
        {showLeftArrow && (
          <Button
            className="left-arrow"
            type="button"
            onClick={() => this.scrollCarousel('left')}
          />
        )}
        <div className={carouselClass} ref={this.promoFilter}>
          {this.renderItems(categoryFilters, className, 'filter-carousel-inner-container', asPath)}
        </div>
        {showRightArrow && (
          <Button
            className="right-arrow"
            type="button"
            aria-label={accessibilityLabels.lbl_right_arrow}
            onClick={() => this.scrollCarousel('right')}
          />
        )}
      </div>
    );
  };

  render() {
    const { categoryFilters, className, asPath } = this.props;
    return (
      <div className={className}>
        {this.renderPromoFilterItems(categoryFilters, className, asPath)}
      </div>
    );
  }
}
export { PromoFilter as PromoFilterVanilla };
export default withStyles(PromoFilter, styles);
