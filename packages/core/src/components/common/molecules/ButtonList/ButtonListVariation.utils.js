import spacing from '@tcp/core/styles/themes/TCP/spacing';

const getFeaturedCheckedValue = (index, buttonsDataLength, featured) => {
  let isFirstItemFeatured = false;
  let isLastItemFeatured = false;

  if (index === 0 && featured) {
    isFirstItemFeatured = true;
  }
  if (index === buttonsDataLength - 1 && featured) {
    isLastItemFeatured = true;
  }

  return { isFirstItemFeatured, isLastItemFeatured };
};

// eslint-disable-next-line
const getButtonRadiusStyle = (index, featured, buttonsDataLength, buttonListVariation) => {
  const BORDER_RADIUS = spacing.ELEM_SPACING.XS_6;
  const { isFirstItemFeatured, isLastItemFeatured } = getFeaturedCheckedValue(
    index,
    buttonsDataLength,
    featured
  );

  if (buttonListVariation === 'scrollCTAList') {
    return `border-radius: ${BORDER_RADIUS};`;
  }
  if (buttonsDataLength === 1 && index === 0 && (featured || featured === undefined)) {
    return `border-top-left-radius: ${BORDER_RADIUS}; border-top-right-radius: ${BORDER_RADIUS}; border-bottom-left-radius: ${BORDER_RADIUS}; border-bottom-right-radius: ${BORDER_RADIUS};`;
  }
  if (buttonsDataLength === 2) {
    if (index === 0 && featured) {
      return `border-top-left-radius: ${BORDER_RADIUS}; border-top-right-radius: ${BORDER_RADIUS};`;
    }
    if (index === 1 && featured) {
      return `border-bottom-left-radius: ${BORDER_RADIUS}; border-bottom-right-radius: ${BORDER_RADIUS};`;
    }
    if (index === 0 && !featured) {
      return `border-top-left-radius: ${BORDER_RADIUS}; border-bottom-left-radius: ${BORDER_RADIUS};`;
    }
    if (index === 1 && !featured) {
      return `border-top-right-radius: ${BORDER_RADIUS}; border-bottom-right-radius: ${BORDER_RADIUS};`;
    }
  }
  if (buttonsDataLength > 2) {
    if (buttonsDataLength % 2 === 0) {
      if (buttonsDataLength === 4 && (isFirstItemFeatured || isLastItemFeatured)) {
        if (index === 0 && featured) {
          return `border-top-left-radius: ${BORDER_RADIUS}; border-top-right-radius: ${BORDER_RADIUS};`;
        }
        if (index === buttonsDataLength - 1 && featured) {
          return `border-bottom-left-radius: ${BORDER_RADIUS}; border-bottom-right-radius: ${BORDER_RADIUS};`;
        }
      }
      if (buttonsDataLength === 4 && !isFirstItemFeatured && !isLastItemFeatured) {
        if (index === 0) {
          return `border-top-left-radius: ${BORDER_RADIUS};`;
        }
        if (index === 1) {
          return `border-top-right-radius: ${BORDER_RADIUS}`;
        }
        if (index === 2) {
          return `border-bottom-left-radius: ${BORDER_RADIUS};`;
        }
        if (index === 3) {
          return `border-bottom-right-radius: ${BORDER_RADIUS}`;
        }
      }

      if (buttonsDataLength === 6 || buttonsDataLength === 8) {
        if (index === 0) {
          return `border-top-left-radius: ${BORDER_RADIUS}; border-top-right-radius: ${BORDER_RADIUS};`;
        }
        if (index === buttonsDataLength - 1) {
          return `border-bottom-left-radius: ${BORDER_RADIUS}; border-bottom-right-radius: ${BORDER_RADIUS};`;
        }
      }
    }
    if (buttonsDataLength % 2 !== 0) {
      if (index === 0) {
        return `border-top-left-radius: ${BORDER_RADIUS};`;
      }
      if (index === 1) {
        return `border-top-right-radius: ${BORDER_RADIUS};`;
      }
      if (index === buttonsDataLength - 1) {
        return `border-bottom-left-radius: ${BORDER_RADIUS}; border-bottom-right-radius: ${BORDER_RADIUS};`;
      }
    }
  }
  return `border-radius: 0;`;
};

export default getButtonRadiusStyle;
