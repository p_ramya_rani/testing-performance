// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { FlatList, View } from 'react-native';
import isEmpty from 'lodash/isEmpty';
import HP_NEW_LAYOUT from '@tcp/core/src/constants/hpNewLayout.constants';
import { getScreenWidth } from '../../../../../utils/utils.app';
import { Button, Anchor, BodyCopy, DamImage } from '../../../atoms';
import {
  Wrapper,
  Container,
  ScrollViewContainer,
  DivImageContainer,
  TextLinksViewContainer,
  ContainerView,
  SeparatorView,
  Border,
} from '../ButtonList.styles.native';

/**
 * These are button styles.
 */
const BORDER_WIDTH = 0.5;
const buttonWidth = { width: getScreenWidth() / 2, borderWidth: BORDER_WIDTH };
const buttonFullWidth = { width: getScreenWidth(), borderWidth: BORDER_WIDTH };

// Key extractor for flat list
const keyExtractor = (_, index) => index.toString();

/**
 * This function is used to render button either full-width or half
 */
const renderItem = (
  item,
  navigation,
  showFullWidth,
  locator,
  buttonVariation,
  color,
  overrideStyle
) => {
  const { button } = item;
  const removeLeftBottomBorder = {
    borderLeftWidth: 0,
    borderBottomWidth: 0,
  };
  const updatedButtonFullWidth = !isEmpty(overrideStyle)
    ? {
        ...buttonFullWidth,
        borderWidth: 1,
        ...removeLeftBottomBorder,
        width: getScreenWidth() - 2 * HP_NEW_LAYOUT.BODY_PADDING,
        borderRightWidth: 0,
      }
    : { ...buttonFullWidth };
  const updatedButtonWidth = !isEmpty(overrideStyle)
    ? {
        ...buttonWidth,
        borderWidth: 1,
        width: (getScreenWidth() - 2 * HP_NEW_LAYOUT.BODY_PADDING) / 2,
        ...removeLeftBottomBorder,
      }
    : { ...buttonWidth };

  return (
    <Button
      locator={locator}
      accessibilityRole="button"
      accessibilityLabel={button?.text}
      buttonVariation={buttonVariation}
      text={button?.text}
      color={color}
      style={showFullWidth ? updatedButtonFullWidth : updatedButtonWidth}
      url={button?.url}
      navigation={navigation}
      noCurve
      overrideStyle={overrideStyle}
    />
  );
};

/**
 * This function is used to render Even number of Buttons into Grid
 */
const renderEvenButtonGrid = (
  updatedCtxButton,
  navigation,
  locator,
  buttonVariation,
  color,
  overrideStyle
) => {
  return (
    <View>
      <FlatList
        numColumns={2}
        keyExtractor={keyExtractor}
        data={updatedCtxButton}
        renderItem={({ item }) =>
          renderItem(item, navigation, false, locator, buttonVariation, color, overrideStyle)
        }
      />
    </View>
  );
};

const renderFeaturedCTAList = (
  ctxButton,
  navigation,
  locator,
  buttonVariation,
  color,
  overrideStyle
) => {
  const showFullWidth = true;
  const displayItems = {};
  let count = 0;
  ctxButton.forEach((ctBtn) => {
    if (ctBtn.featured) {
      count += 1;
      displayItems[`${count}`] = ctBtn;
      count += 1;
    } else {
      if (!displayItems[`${count}`]) {
        displayItems[`${count}`] = [];
      }
      displayItems[`${count}`].push(ctBtn);
    }
  });

  const keys = Object.keys(displayItems);

  return keys.map((key) => {
    if (Array.isArray(displayItems[key])) {
      return renderEvenButtonGrid(
        displayItems[key],
        navigation,
        locator,
        buttonVariation,
        color,
        overrideStyle
      );
    }
    return renderItem(
      displayItems[key],
      navigation,
      showFullWidth,
      locator,
      buttonVariation,
      color,
      overrideStyle
    );
  });
};

/**
 * This function is used to render Odd number of Buttons into Grid
 */

const renderOddButtonGrid = (
  ctxButton,
  navigation,
  locator,
  buttonVariation,
  color,
  overrideStyle
) => {
  const updatedCtxButton = ctxButton.slice();
  const item = updatedCtxButton.pop();
  const showFullWidth = true;
  return (
    <Container>
      {renderEvenButtonGrid(
        updatedCtxButton,
        navigation,
        locator,
        buttonVariation,
        color,
        overrideStyle
      )}
      {renderItem(item, navigation, showFullWidth, locator, buttonVariation, color, overrideStyle)}
    </Container>
  );
};

/**
 * This function is used to render a single button in scroll Button view .
 */
const renderItemScrollCTAList = (
  item,
  navigation,
  locator,
  color,
  buttonVariation,
  buttonColor
) => {
  const {
    item: { button },
  } = item;

  return (
    <ScrollViewContainer keyboardShouldPersistTaps="handled">
      <Button
        locator={locator}
        accessibilityRole="button"
        accessibilityLabel={button.text}
        buttonVariation={buttonVariation || 'variable-width'}
        color={buttonColor || color}
        text={button.text}
        url={button.url}
        navigation={navigation}
      />
    </ScrollViewContainer>
  );
};

/**
 * This function is used to generate Scroll ButtonList view .
 */
const renderScrollCTAList = (
  ctxButton,
  navigation,
  locator,
  color,
  buttonVariation,
  buttonColor
) => {
  const isHorizontalScroll = true;
  const isScrollIndicator = false;
  return (
    <FlatList
      showsHorizontalScrollIndicator={isScrollIndicator}
      horizontal={isHorizontalScroll}
      keyExtractor={keyExtractor}
      data={ctxButton}
      renderItem={(item) =>
        renderItemScrollCTAList(
          item,
          navigation,
          locator,
          color,
          buttonVariation,
          buttonColor.color
        )
      }
    />
  );
};

/**
 * This function is used to generate links for LinkText view .
 */
const renderItemCTAList = (item, navigation, locator, buttonColor) => {
  const style = {
    borderBottomWidth: 2,
    borderColor: (buttonColor && buttonColor.color) || 'white',
  };
  const overrideStyle = { ...buttonColor };

  const {
    item: {
      button: { text, url },
    },
    index,
  } = item;

  return (
    <TextLinksViewContainer>
      <Anchor
        overrideStyle={overrideStyle}
        key={index.toString()}
        locator={locator}
        accessibilityRole="link"
        accessibilityLabel={text}
        text={text}
        anchorVariation="white"
        fontSizeVariation="large"
        url={url}
        navigation={navigation}
        customStyle={style}
        centered
      />
    </TextLinksViewContainer>
  );
};

/**
 * This function is used to generate LinkText view .
 */
const renderCTAList = (ctxButton, navigation, locator, buttonColor) => {
  const isHorizontalScroll = true;
  const isScrollIndicator = false;

  return (
    <Wrapper>
      <FlatList
        showsHorizontalScrollIndicator={isScrollIndicator}
        horizontal={isHorizontalScroll}
        keyExtractor={keyExtractor}
        data={ctxButton}
        renderItem={(item) => renderItemCTAList(item, navigation, locator, buttonColor)}
      />
    </Wrapper>
  );
};

/**
 * This function is used to generate links for DivImageCTA view .
 */
const renderItemImageCTAList = (item, navigation, locator, color) => {
  const style = { borderRadius: 60 / 2 };
  const bodycopyStyle = { marginTop: 20, width: 75, color: '#1a1a1a' };
  const {
    item: { image, button },
    index,
  } = item;
  let textColor = color;
  if (color === 'gray') {
    textColor = 'gray.900';
  }

  return (
    <Anchor url={button.url} navigation={navigation} locator={locator}>
      <DivImageContainer>
        {image && <DamImage url={image.url} height={60} width={60} style={style} />}
        <BodyCopy
          key={index.toString()}
          accessibilityRole="text"
          accessibilityLabel={button.text}
          fontFamily="secondary"
          fontSize="fs14"
          color={textColor}
          fontWeight="extrabold"
          letterSpacing="black"
          text={button.text}
          textAlign="center"
          style={bodycopyStyle}
        />
      </DivImageContainer>
    </Anchor>
  );
};

const renderSeparatorView = () => {
  return <SeparatorView />;
};

/**
 * This function is used to generate DivImageCTA view .
 */
const renderImageCTAList = (ctxButton, navigation, locator, color) => {
  const isHorizontalScroll = true;
  const isScrollIndicator = false;
  return (
    <FlatList
      showsHorizontalScrollIndicator={isScrollIndicator}
      horizontal={isHorizontalScroll}
      keyExtractor={keyExtractor}
      data={ctxButton}
      renderItem={(item) => renderItemImageCTAList(item, navigation, locator, color)}
      ItemSeparatorComponent={renderSeparatorView}
    />
  );
};

const getButtonListBorderStyle = (ctaBorderRadius) => {
  return ctaBorderRadius && { ...ctaBorderRadius };
};

/**
 * @param {object} props : Props for ButtonList
 * @desc This is a buttonlist component. The four variations of buttonlist are:
 * linktext, tackedCTAButton, scrollCTAButton and divImageCTA
 * buttonsData: Takes the list of linktext, tackedCTAButton, scrollCTAButton and divImageCTA button .
 */

const ButtonList = ({
  locator,
  buttonListVariation,
  navigation,
  buttonsData,
  color,
  buttonVariation,
  buttonColor = {},
  isContainerModule,
  overrideStyle,
}) => {
  if (buttonListVariation === 'stackedCTAList') {
    const isEvenButtonGrid = buttonsData.length % 2 === 0;
    return (
      <Container>
        <Border background={color} style={getButtonListBorderStyle(overrideStyle)}>
          {isContainerModule &&
            renderFeaturedCTAList(
              buttonsData,
              navigation,
              locator,
              buttonVariation,
              buttonColor.color,
              overrideStyle
            )}
          {isEvenButtonGrid &&
            !isContainerModule &&
            renderEvenButtonGrid(
              buttonsData,
              navigation,
              locator,
              buttonVariation,
              buttonColor.color,
              overrideStyle
            )}
          {!isEvenButtonGrid &&
            !isContainerModule &&
            renderOddButtonGrid(
              buttonsData,
              navigation,
              locator,
              buttonVariation,
              buttonColor.color,
              overrideStyle
            )}
        </Border>
      </Container>
    );
  }

  if (buttonListVariation === 'scrollCTAList') {
    return (
      <ContainerView>
        {renderScrollCTAList(buttonsData, navigation, locator, color, buttonVariation, buttonColor)}
      </ContainerView>
    );
  }

  if (buttonListVariation === 'linkCTAList') {
    return <Container>{renderCTAList(buttonsData, navigation, locator, buttonColor)}</Container>;
  }

  if (buttonListVariation === 'imageCTAList') {
    return (
      <ContainerView>{renderImageCTAList(buttonsData, navigation, locator, color)}</ContainerView>
    );
  }

  return null;
};

ButtonList.propTypes = {
  buttonsData: PropTypes.shape([]).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  buttonListVariation: PropTypes.string.isRequired,
  locator: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  buttonVariation: PropTypes.string.isRequired,
  buttonColor: PropTypes.shape({}).isRequired,
  isContainerModule: PropTypes.bool,
  overrideStyle: PropTypes.shape({}).isRequired,
};

ButtonList.defaultProps = { isContainerModule: false };

export default ButtonList;
export { ButtonList as ButtonListVanilla };
