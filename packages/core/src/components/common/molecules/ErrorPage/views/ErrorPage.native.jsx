// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { getScreenWidth, getReferenceId } from '@tcp/core/src/utils/utils.app';
import { BodyCopy } from '../../../atoms';
import {
  SafeAreaViewStyle,
  Container,
  ImageContainer,
  TextContainer,
  StyledImage,
  SessionContainer,
} from '../styles/ErrorPage.style.native';

const WIDTH = getScreenWidth();
const updateAppLifestyle = require('../../../../../../src/assets/update-app-lifestyle.png');

export class ErrorPage extends PureComponent {
  render() {
    const referenceId = getReferenceId();

    return (
      <SafeAreaViewStyle>
        <Container>
          <ImageContainer>
            <StyledImage
              source={updateAppLifestyle}
              width={WIDTH}
              height="290px"
              position="absolute"
              top="0"
            />
          </ImageContainer>
          <TextContainer>
            <BodyCopy
              text="Oops !!"
              fontSize="fs32"
              fontFamily="primary"
              textAlign="center"
              fontWeight="medium"
              color="text.primary"
            />
            <BodyCopy
              text="Something went wrong."
              fontSize="fs16"
              fontFamily="primary"
              textAlign="center"
              fontWeight="medium"
              color="text.primary"
            />
            <BodyCopy
              text="Please relaunch the app."
              fontSize="fs16"
              fontFamily="primary"
              textAlign="center"
              fontWeight="medium"
              color="text.primary"
            />
            {referenceId && (
              <SessionContainer>
                <BodyCopy
                  text={`Reference ID: ${referenceId}`}
                  fontSize="fs10"
                  fontFamily="primary"
                  textAlign="center"
                  fontWeight="medium"
                  color="text.primary"
                />
              </SessionContainer>
            )}
          </TextContainer>
        </Container>
      </SafeAreaViewStyle>
    );
  }
}

export default ErrorPage;
