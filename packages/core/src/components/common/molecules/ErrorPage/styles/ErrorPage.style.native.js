// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { Image } from '../../../atoms';

export const SafeAreaViewStyle = styled.SafeAreaView`
  flex: 1;
`;

export const Container = styled.View`
  flex: 1;
`;

export const ImageContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const TextContainer = styled.View`
  flex: 2;
  align-items: center;
  justify-content: center;
`;

export const SessionContainer = styled.View`
  position: absolute;
  bottom: 20px;
`;

export const StyledImage = styled(Image)`
  ${props => (props.marginTop ? `margin-top: ${props.marginTop}` : ``)};
  ${props => (props.tintColor ? `tint-color: ${props.tintColor}` : ``)};
  ${props => (props.position ? `position: ${props.position}` : ``)};
  ${props => (props.top ? `top: ${props.top}` : ``)};
`;
export default {
  SafeAreaViewStyle,
  Container,
  ImageContainer,
  TextContainer,
  StyledImage,
  SessionContainer,
};
