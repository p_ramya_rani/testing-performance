// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import LineComp from '@tcp/core/src/components/common/atoms/Line';
import { Button } from '@tcp/core/src/components/common/atoms';
import { StyledText } from '@tcp/core/styles/globalStyles/StyledText.style';

// new styled for AB test modal
const TopBar = styled(LineComp)`
  border-radius: 52px;
  width: 92px;
  margin: 3px auto 15px auto;
  ${(props) => props.overrideCSS}
`;

const LineWrapper = styled(LineComp)`
  width: 100%;
`;

const ImageWrapper = styled.View`
  position: absolute;
  left: 10;
  top: 20;
  ${(props) => props.overrideCSS}
`;

const CrossImageWrapper = styled.View`
  position: absolute;
  right: 20;
  top: 20;
  ${(props) => props.overrideCSS}
`;

const StyledTouchableOpacity = styled.TouchableOpacity`
  padding: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS}
    ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
    ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  ${(props) => props.overrideCSS}
`;

const StyledCrossImage = styled.Image`
  width: ${(props) => (props.isFromFilterPills ? '20px' : '18px')};
  height: ${(props) => (props.isFromFilterPills ? '20px' : '18px')};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  ${(props) => props.overrideCSS}
`;

const ModalTitleNew = styled(StyledText)`
  margin-bottom: 15px;
  text-align: center;
  letter-spacing: 1px;
  color: ${(props) => props.titleStyles.color || props.theme.colorPalette.gray[900]};
`;

const ClearAllButton = styled(Button)`
  position: absolute;
  right: 0;
  top: 10;
`;

export {
  TopBar,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  ModalTitleNew,
  ClearAllButton,
  LineWrapper,
  CrossImageWrapper,
};
