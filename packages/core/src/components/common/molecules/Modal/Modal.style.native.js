// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { Platform } from 'react-native';

const getAdditionalStyle = (props) => {
  const { margins, paddings, modalHeadingMargin, childrenMargins } = props;
  return {
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
    ...(modalHeadingMargin && { margin: modalHeadingMargin }),
    ...(childrenMargins && { margin: childrenMargins }),
  };
};

const StyledCrossImage = styled.Image`
  ${(props) => (props.iconTypeArrow ? `height: ${props.theme.spacing.ELEM_SPACING.LRG}` : null)}
  width: ${(props) =>
    props.iconTypeArrow
      ? props.theme.spacing.ELEM_SPACING.REG
      : props.theme.spacing.ELEM_SPACING.MED};

  ${(props) =>
    !props.rightAlignCrossIcon ? `margin-right : ${props.theme.spacing.ELEM_SPACING.XS}` : ''};
`;

const StyledTouchableOpacity = styled.TouchableOpacity`
  ${(props) => (props.isNewReDesignTopLeftIcon ? `left: -5` : ``)}
  padding: 10px 4px 12px 12px;
`;

const ModalCustomWrapper = styled.SafeAreaView`
  ${(props) =>
    props.transparentModal === 'transparent-captcha'
      ? `
      background-color: rgba(0,0,0,.5);
`
      : ``}
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

const ModalHeading = styled.View`
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding-top: 5px;
  width: ${(props) => (props.fullWidth ? '100%' : '90%')};
  ${(props) =>
    props.stickyCloseIcon
      ? `
      position: relative;
  `
      : ``};
  ${getAdditionalStyle};
`;

const LineWrapper = styled.View`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  padding-right: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
`;

const RowWrapper = styled.View`
  margin: ${Platform.OS === 'ios' ? '0' : (props) => props.theme.spacing.ELEM_SPACING.MED}
    ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0
    ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  width: ${(props) => (props.fullWidth ? '100%' : '94% ')};

  flex-direction: row;
  ${(props) =>
    props.isOverlay
      ? `
    position: absolute;
    z-index: 1;
  `
      : ``}
  ${(props) =>
    props.stickyCloseIcon
      ? `
      margin : ${props.theme.spacing.ELEM_SPACING.SM} ${props.theme.spacing.ELEM_SPACING.SM} 0 ${props.theme.spacing.ELEM_SPACING.XXXS}
      `
      : ``}
  ${(props) =>
    props.customHeaderMargin
      ? `
      margin: ${props.customHeaderMargin}
      `
      : ``}
`;

const ImageWrapper = styled.View`
  ${(props) =>
    props.stickyCloseIcon
      ? `
      position: absolute;
      right: 0;
      ${props.isBTSWebView ? `top: -10px` : ``}
  `
      : `
      right: 20`}
`;

const CancelTouchableOpacityWrapper = styled.View`
  position: absolute;
  left: 16px;
  margin-top: 7px;
`;

const Heading = styled.View`
  background-color: ${(props) => props.theme.colors.WHITE};
  ${(props) => (props.headerStyle && props.headerStyle.head ? { ...props.headerStyle.head } : '')}
`;

const ViewContainer = styled.View`
  ${getAdditionalStyle};
`;
const ScrollView = styled.ScrollView`
  ${getAdditionalStyle};
`;

const ChildrenContainer = styled.View`
  ${getAdditionalStyle};
`;

const CustomScrollView = styled.ScrollView`
  background: ${(props) => (props.isFromChangeStore ? props.theme.colors.WHITE : 'transparent')};
  margin-top: ${(props) => (props.isFromChangeStore ? '0px' : '60px')};
`;
const WrapperView = styled.View`
  padding-bottom: 130px;
`;

const ModalOutsideTouchable = styled.TouchableWithoutFeedback`
  flex: 1;
`;

const ModalCloseTouchable = styled.TouchableOpacity`
  position: absolute;
  right: 20;
  padding: 8px 8px 8px 8px;
`;

const ModalCurvedOverlay = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.5);
`;

const ModalCurvedContent = styled.View`
  background: ${(props) => props.theme.colorPalette.white};
  width: 100%;
  border-top-left-radius: 30;
  border-top-right-radius: 30;
  height: 100%;
  ${(props) => props.overrideCSS}
`;
export const ComponentWrapper = styled.View`
  flex: 1;
  background: rgba(0, 0, 0, 0);
  height: 80%;
`;

export {
  StyledCrossImage,
  ImageWrapper,
  StyledTouchableOpacity,
  ModalHeading,
  LineWrapper,
  RowWrapper,
  ModalCustomWrapper,
  Heading,
  ViewContainer,
  ScrollView,
  ChildrenContainer,
  CustomScrollView,
  ModalOutsideTouchable,
  ModalCloseTouchable,
  ModalCurvedOverlay,
  ModalCurvedContent,
  CancelTouchableOpacityWrapper,
  WrapperView,
};
