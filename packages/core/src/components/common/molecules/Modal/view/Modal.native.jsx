// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Modal, KeyboardAvoidingView, Platform, StyleSheet, View } from 'react-native';
import ModalBox from 'react-native-modalbox';
import Loader from '@tcp/core/src/components/common/molecules/Loader';
import { isAndroid } from '@tcp/core/src/utils';
import {
  ModalCustomWrapper,
  ViewContainer,
  ScrollView,
  ChildrenContainer,
  CustomScrollView,
  ModalOutsideTouchable,
  ModalCurvedOverlay,
  ModalCurvedContent,
  WrapperView,
} from '../Modal.style.native';
import ModalNativeHeader from './Modal.header.native';
import ModalHeaderCurved from './Modal.header.curved.native';

const CustomScrollStyle = { flex: 1 };
const white = '#fff';
const borderTopColor = '#e3e3e3';
const styles = StyleSheet.create({
  btnWrapper: {
    alignItems: 'center',
    backgroundColor: white,
    borderTopColor,
    borderTopWidth: 1,
    bottom: 0,
    height: 110,
    justifyContent: 'center',
    left: 0,
    paddingBottom: 0,
    paddingTop: 0,
    position: 'absolute',
    right: 0,
    width: '100%',
  },
});

const isTransparentBackground = (customTransparent, roundedModal) =>
  customTransparent || roundedModal || false;

const renerBopisModal = (children, isOpen, otherProps) => {
  const { heading, onRequestClose, customTransparent, isFromChangeStore, stickyfooter } =
    otherProps;
  const style = {
    height: '90%',
    borderRadius: 16,
  };
  return (
    <>
      {isOpen && (
        <ModalBox
          animationDuration={400}
          position="bottom"
          isOpen={isOpen}
          entry="bottom"
          swipeToClose={false}
          onClosed={onRequestClose}
          coverScreen
          useNativeDriver
          style={style}
        >
          <>
            <ModalHeaderCurved
              labelTitle={heading}
              onRequestClose={onRequestClose}
              onActionClick={onRequestClose}
              overrideCSS={{
                styledCrossImage: 'margin: 0',
                styledTouchableOpacity: 'padding: 0',
              }}
              hideTopBar
              isFromFilterPills={isFromChangeStore}
            />
            <CustomScrollView isFromChangeStore={isFromChangeStore}>
              {children}
              <WrapperView />
            </CustomScrollView>

            <View style={styles.btnWrapper}>{stickyfooter}</View>
          </>
          {customTransparent && children}
          {!isAndroid() && <Loader />}
        </ModalBox>
      )}
    </>
  );
};

// How To use this react native modal
// import this component in your file.
// It takes four properties: isOpen, onRequestClose, heading and children

const ModalNative = ({ isOpen, children, isOverlay, inheritedStyles, ...otherProps }) => {
  const {
    heading,
    onRequestClose,
    animationType = 'slide',
    headingAlign,
    headingFontFamily,
    headerStyle,
    headingFontWeight,
    fontSize,
    iconType,
    fullWidth,
    customTransparent,
    stickyCloseIcon,
    transparentModal,
    horizontalBar = true,
    showHeader = true,
    borderColor = 'black',
    rightAlignCrossIcon,
    noscroll,
    customHeaderMargin,
    modalHeadingMargin,
    margins,
    paddings,
    childrenMargins,
    setModalReference,
    showOnlyCloseIcon,
    roundedModal,
    clickOutsideToClose,
    overrideCSS,
    isNewReDesignTopLeftIcon,
    isBTSWebView,
    overrideCustomStyle,
    showTextForClose,
    textForClose,
    isFromChangeStore,
  } = otherProps;
  let behavior = null;
  if (Platform.OS === 'ios') {
    behavior = 'padding';
  }

  let Component = ScrollView;
  if (noscroll) {
    Component = ViewContainer;
  }

  const onScrollEndDrag = (event) => {
    const isScrolled =
      event &&
      event.nativeEvent &&
      event.nativeEvent.contentOffset &&
      event.nativeEvent.contentOffset.y < 0;

    if (isScrolled) {
      onRequestClose();
    }
  };

  if (isFromChangeStore) {
    return renerBopisModal(children, isOpen, otherProps);
  }
  return (
    <>
      {isOpen && (
        <Modal
          transparent={isTransparentBackground(customTransparent, roundedModal)}
          visible={isOpen}
          animationType={animationType}
          onRequestClose={onRequestClose}
        >
          {!customTransparent && !roundedModal && (
            <ModalCustomWrapper
              transparentModal={transparentModal}
              inheritedStyles={inheritedStyles}
            >
              <KeyboardAvoidingView behavior={behavior} enabled>
                <Component
                  showsVerticalScrollIndicator={false}
                  keyboardShouldPersistTaps="handled"
                  stickyHeaderIndices={[0]}
                  margins={margins}
                  paddings={paddings}
                  ref={(ref) => {
                    if (ref && setModalReference && !noscroll) {
                      setModalReference(ref);
                    }
                  }}
                  collapsable={false}
                >
                  <ModalNativeHeader
                    heading={heading}
                    onRequestClose={onRequestClose}
                    headingAlign={headingAlign}
                    headingFontFamily={headingFontFamily}
                    headerStyle={headerStyle}
                    headingFontWeight={headingFontWeight}
                    fontSize={fontSize}
                    iconType={iconType}
                    fullWidth={fullWidth}
                    stickyCloseIcon={stickyCloseIcon}
                    horizontalBar={horizontalBar}
                    showHeader={showHeader}
                    borderColor={borderColor}
                    rightAlignCrossIcon={rightAlignCrossIcon}
                    customHeaderMargin={customHeaderMargin}
                    modalHeadingMargin={modalHeadingMargin}
                    showOnlyCloseIcon={showOnlyCloseIcon}
                    isNewReDesignTopLeftIcon={isNewReDesignTopLeftIcon}
                    isLoginOrSignup
                    showTextForClose={showTextForClose}
                    textForClose={textForClose}
                    isBTSWebView={isBTSWebView}
                    overrideCustomStyle={overrideCustomStyle}
                  />

                  <ChildrenContainer childrenMargins={childrenMargins}>
                    {children}
                  </ChildrenContainer>
                </Component>
              </KeyboardAvoidingView>
            </ModalCustomWrapper>
          )}
          {roundedModal && (
            <>
              <ModalOutsideTouchable
                accessibilityRole="button"
                activeOpacity={1}
                onPressOut={clickOutsideToClose ? onRequestClose : () => {}}
              >
                <ModalCurvedOverlay />
              </ModalOutsideTouchable>
              <CustomScrollView
                collapsable={false}
                onScrollEndDrag={(e) => onScrollEndDrag(e)}
                contentContainerStyle={CustomScrollStyle}
              >
                <ModalNativeHeader
                  roundedModal={roundedModal}
                  heading={heading}
                  onRequestClose={onRequestClose}
                  headingAlign={headingAlign}
                  headingFontFamily={headingFontFamily}
                  headerStyle={headerStyle}
                  headingFontWeight={headingFontWeight}
                  fontSize={fontSize}
                  iconType={iconType}
                  fullWidth={fullWidth}
                  stickyCloseIcon={stickyCloseIcon}
                  horizontalBar={horizontalBar}
                  showHeader={showHeader}
                  borderColor={borderColor}
                  rightAlignCrossIcon={rightAlignCrossIcon}
                  customHeaderMargin={customHeaderMargin}
                  modalHeadingMargin={modalHeadingMargin}
                  showOnlyCloseIcon={showOnlyCloseIcon}
                  isNewReDesignTopLeftIcon={isNewReDesignTopLeftIcon}
                />

                <ModalCurvedContent overrideCSS={overrideCSS.modalCurvedContent}>
                  {children}
                </ModalCurvedContent>
              </CustomScrollView>
            </>
          )}
          {customTransparent && children}
          {!isAndroid() && <Loader />}
        </Modal>
      )}
    </>
  );
};

ModalNative.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  isOverlay: PropTypes.bool,
  inheritedStyles: PropTypes.string,
  stickyCloseIcon: PropTypes.string,
  showOnlyCloseIcon: PropTypes.bool,
  roundedModal: PropTypes.bool,
  clickOutsideToClose: PropTypes.bool,
  overrideCSS: PropTypes.shape({}),
  isFromChangeStore: PropTypes.bool,
};

ModalNative.defaultProps = {
  isOverlay: false,
  inheritedStyles: '',
  stickyCloseIcon: true,
  showOnlyCloseIcon: false,
  roundedModal: false,
  clickOutsideToClose: false,
  overrideCSS: {},
  isFromChangeStore: false,
};

export default ModalNative;
