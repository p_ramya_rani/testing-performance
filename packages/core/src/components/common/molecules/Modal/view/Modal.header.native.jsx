// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Platform, TouchableOpacity } from 'react-native';
import LineComp from '@tcp/core/src/components/common/atoms/Line';
import ToastContainer from '@tcp/core/src/components/common/atoms/Toast/container/Toast.container.native';
import {
  StyledCrossImage,
  StyledTouchableOpacity,
  ModalHeading,
  RowWrapper,
  ImageWrapper,
  CancelTouchableOpacityWrapper,
  Heading,
} from '../Modal.style.native';
import { BodyCopyWithSpacing } from '../../../atoms/styledWrapper';
import { BodyCopy } from '../../../atoms';

const closeIcon = require('../../../../../../../mobileapp/src/assets/images/icons-standard-close.png');
const arrowIcon = require('../../../../../../../mobileapp/src/assets/images/carrot-large-left.png');
const arrowIconNewDesign = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icon-left-black-big.png');

const getCloseIcon = ({
  stickyCloseIcon,
  onRequestClose,
  headerStyle,
  iconType,
  isOverlay,
  rightAlignCrossIcon,
  isBTSWebView,
  otherProps = {},
}) => {
  const { isNewReDesignTopLeftIcon } = otherProps;
  let iconSrc = closeIcon;
  if (isNewReDesignTopLeftIcon && iconType === 'arrow') {
    iconSrc = arrowIconNewDesign;
  } else if (iconType === 'arrow') {
    iconSrc = arrowIcon;
  }
  const iconTypeArrow = iconType === 'arrow';
  return (
    <ImageWrapper stickyCloseIcon={stickyCloseIcon} style={headerStyle} isBTSWebView={isBTSWebView}>
      <StyledTouchableOpacity
        onPress={onRequestClose}
        accessibilityRole="button"
        accessibilityLabel="close"
        isOverlay={isOverlay}
        isNewReDesignTopLeftIcon={isNewReDesignTopLeftIcon}
      >
        <StyledCrossImage
          iconTypeArrow={iconTypeArrow}
          rightAlignCrossIcon={rightAlignCrossIcon}
          source={iconSrc}
        />
      </StyledTouchableOpacity>
    </ImageWrapper>
  );
};

const geLine = (horizontalBar, borderColor, borderWidth) => {
  return (
    <>
      {horizontalBar ? (
        <LineComp marginTop={5} borderWidth={borderWidth || 2} borderColor={borderColor} />
      ) : null}
    </>
  );
};

const getHeadingAlignment = (isLoginOrSignup, headingAlign) => {
  if (isLoginOrSignup) {
    return 'center';
  }
  return headingAlign;
};

const ModalHeaderNative = ({ isOverlay, ...otherProps }) => {
  const {
    heading,
    onRequestClose,
    headingAlign,
    headingFontFamily,
    headerStyle,
    headingFontWeight,
    fontSize,
    iconType,
    fullWidth,
    stickyCloseIcon,
    horizontalBar = true,
    showHeader = true,
    borderColor = 'black',
    rightAlignCrossIcon,
    customHeaderMargin,
    modalHeadingMargin,
    showOnlyCloseIcon,
    isLoginOrSignup,
    isBTSWebView,
    showTextForClose,
    overrideCustomStyle,
    textForClose,
  } = otherProps;

  const isIOS = Platform.OS === 'ios';

  const { borderWidth } = overrideCustomStyle;

  const showContentOnHeader = () => {
    return showTextForClose ? (
      <CancelTouchableOpacityWrapper>
        <TouchableOpacity
          onPress={onRequestClose}
          accessibilityRole="button"
          accessibilityLabel="close"
        >
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            text={textForClose || 'Cancel'}
            fontWeight="semibold"
            color="gray.900"
          />
        </TouchableOpacity>
      </CancelTouchableOpacityWrapper>
    ) : (
      getCloseIcon({
        onRequestClose,
        headerStyle,
        iconType,
        isOverlay,
        stickyCloseIcon,
        otherProps,
        rightAlignCrossIcon,
        isBTSWebView,
      })
    );
  };

  const getHeaderElements = () => {
    return Platform.OS === 'android'
      ? getCloseIcon({
          onRequestClose,
          headerStyle,
          iconType,
          isOverlay,
          stickyCloseIcon,
          otherProps,
          isBTSWebView,
          rightAlignCrossIcon,
        })
      : showContentOnHeader();
  };

  return (
    <>
      <ToastContainer shouldShowSafeArea={false} />
      <Heading headerStyle={headerStyle}>
        {showHeader && !showOnlyCloseIcon && (
          <RowWrapper
            stickyCloseIcon={stickyCloseIcon}
            isOverlay={isOverlay}
            customHeaderMargin={customHeaderMargin}
            isLoginOrSignup
            fullWidth={fullWidth}
          >
            {heading && (
              <ModalHeading
                stickyCloseIcon={stickyCloseIcon}
                fullWidth={fullWidth}
                modalHeadingMargin={modalHeadingMargin}
                isLoginOrSignup
              >
                <BodyCopyWithSpacing
                  fontFamily={headingFontFamily || 'primary'}
                  fontWeight={headingFontWeight || 'extrabold'}
                  headingAlign={headingAlign}
                  textAlign={getHeadingAlignment(isLoginOrSignup, headingAlign)}
                  fontSize={fontSize || 'fs16'}
                  text={heading}
                  numberOfLines={1}
                />
              </ModalHeading>
            )}
            {getHeaderElements()}
          </RowWrapper>
        )}

        {isIOS ? geLine(horizontalBar, borderColor, borderWidth) : null}
        {showOnlyCloseIcon &&
          getCloseIcon({
            onRequestClose,
            headerStyle,
            iconType,
            isOverlay,
            stickyCloseIcon,
            rightAlignCrossIcon,
            isBTSWebView,
          })}
      </Heading>
    </>
  );
};
ModalHeaderNative.propTypes = {
  isOverlay: PropTypes.bool,
  inheritedStyles: PropTypes.string.isRequired,
  stickyCloseIcon: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  headerStyle: PropTypes.shape({}).isRequired,
  iconType: PropTypes.string.isRequired,
  rightAlignCrossIcon: PropTypes.bool.isRequired,
  showOnlyCloseIcon: PropTypes.bool,
  overrideCustomStyle: PropTypes.shape({}),
};

getCloseIcon.propTypes = {
  isOverlay: PropTypes.bool.isRequired,
  stickyCloseIcon: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  headerStyle: PropTypes.shape({}).isRequired,
  iconType: PropTypes.string.isRequired,
  rightAlignCrossIcon: PropTypes.bool.isRequired,
  otherProps: PropTypes.shape({}),
  isBTSWebView: PropTypes.bool,
};

getCloseIcon.defaultProps = {
  otherProps: {},
  isBTSWebView: false,
};

ModalHeaderNative.defaultProps = {
  isOverlay: false,
  showOnlyCloseIcon: false,
  overrideCustomStyle: { borderWidth: 0 },
};

export default ModalHeaderNative;
