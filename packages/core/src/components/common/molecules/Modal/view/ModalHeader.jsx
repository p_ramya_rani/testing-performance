// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import ModalCloseIcon from './ModalCloseIcon';
import { BodyCopy } from '../../../../../../styles/themes/TCP/typotheme';

const ModalHeader = ({
  closeFunc,
  heading,
  closeIconDataLocator,
  headingStyle,
  closeIconLeftAligned,
  dataLocatorHeader,
  inheritedStyles,
}) => {
  return (
    <div className="Modal-Header">
      <ModalCloseIcon
        className="close-modal"
        closeFunc={closeFunc}
        closeIconDataLocator={closeIconDataLocator}
        closeIconLeftAligned={closeIconLeftAligned}
        inheritedStyles={inheritedStyles}
      />
      {heading && (
        <BodyCopy data-locator={dataLocatorHeader} {...headingStyle}>
          {heading}
        </BodyCopy>
      )}
    </div>
  );
};

ModalHeader.propTypes = {
  closeFunc: PropTypes.func.isRequired,
  heading: PropTypes.string.isRequired,
  closeIconDataLocator: PropTypes.string.isRequired,
  headingStyle: PropTypes.string,
  closeIconLeftAligned: PropTypes.bool.isRequired,
  dataLocatorHeader: PropTypes.string.isRequired,
  inheritedStyles: PropTypes.string.isRequired,
};

ModalHeader.defaultProps = {
  headingStyle: {
    bodySize: 'five',
    fontWeight: 'black',
    tag: 'h2',
    className: 'Modal_Heading',
  },
};

export default ModalHeader;
