// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import {
  TopBar,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  ModalTitleNew,
  ClearAllButton,
  LineWrapper,
  CrossImageWrapper,
} from '../Modal.header.curved.style.native';

const closeIcon = require('../../../../../../../mobileapp/src/assets/images/close.png');
const closeIconNew = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/filter-Icons/clear/clear.png');

const ModalHeaderCurved = (props) => {
  const {
    labelTitle,
    titleStyles,
    onRequestClose,
    labelAction,
    onActionClick,
    actionStyles,
    overrideCSS,
    hideTopBar,
    isFromFilterPills,
  } = props;
  return (
    <>
      <TopBar
        borderWidth={hideTopBar ? 0 : 2}
        borderColor="black"
        small
        overrideCSS={overrideCSS.topBar}
      />
      {labelTitle && !isFromFilterPills ? (
        <ModalTitleNew
          fontFamily={titleStyles.fontFamily}
          fontWeight={titleStyles.fontWeight}
          titleStyles={titleStyles}
          {...titleStyles}
        >
          {labelTitle}
        </ModalTitleNew>
      ) : null}

      {labelTitle && isFromFilterPills ? (
        <ModalTitleNew titleStyles={titleStyles}>
          <BodyCopy
            fontSize="fs16"
            fontFamily="secondary"
            fontWeight="extrabold"
            text={labelTitle}
            color="gray.900"
          />
        </ModalTitleNew>
      ) : null}

      {!isFromFilterPills ? (
        <ImageWrapper stickyCloseIcon overrideCSS={overrideCSS.imageWrapper}>
          <StyledTouchableOpacity
            onPress={() => onRequestClose()}
            accessibilityRole="button"
            accessibilityLabel="close"
            isOverlay
            overrideCSS={overrideCSS.styledTouchableOpacity}
            hitSlop={{ top: 20, right: 20, bottom: 20, left: 20 }}
          >
            <StyledCrossImage source={closeIcon} overrideCSS={overrideCSS.styledCrossImage} />
          </StyledTouchableOpacity>
        </ImageWrapper>
      ) : null}
      {isFromFilterPills ? (
        <CrossImageWrapper stickyCloseIcon overrideCSS={overrideCSS.imageWrapper}>
          <StyledTouchableOpacity
            onPress={() => onRequestClose()}
            accessibilityRole="button"
            accessibilityLabel="close"
            isOverlay
            overrideCSS={overrideCSS.styledTouchableOpacity}
            hitSlop={{ top: 20, right: 20, bottom: 20, left: 20 }}
          >
            <StyledCrossImage
              isFromFilterPills={isFromFilterPills}
              source={closeIconNew}
              overrideCSS={overrideCSS.styledCrossImage}
            />
          </StyledTouchableOpacity>
        </CrossImageWrapper>
      ) : (
        <ClearAllButton
          color={actionStyles.color || 'black'}
          buttonVariation="link-button"
          text={labelAction}
          fontSize={actionStyles.fontSize || 'fs14'}
          fontWeight="regular"
          fontFamily="secondary"
          accessibilityLabel={labelAction}
          customBorderStyle={actionStyles.borderStyle || '0'}
          onPress={onActionClick}
          customTextStyle={actionStyles}
        />
      )}

      {labelTitle && !isFromFilterPills ? (
        <LineWrapper borderWidth="0.5" marginTop="18" borderColor="gray.500" />
      ) : null}
    </>
  );
};

ModalHeaderCurved.propTypes = {
  labelTitle: PropTypes.string,
  onRequestClose: PropTypes.func,
  labelAction: PropTypes.string,
  onActionClick: PropTypes.func,
  titleStyles: PropTypes.shape({}),
  actionStyles: PropTypes.shape({}),
  overrideCSS: PropTypes.shape({}),
  hideTopBar: PropTypes.bool,
  isFromFilterPills: PropTypes.bool,
};

ModalHeaderCurved.defaultProps = {
  labelTitle: false,
  onRequestClose: () => {},
  labelAction: '',
  onActionClick: () => {},
  titleStyles: {},
  actionStyles: {},
  overrideCSS: {},
  hideTopBar: false,
  isFromFilterPills: false,
};

export default ModalHeaderCurved;
