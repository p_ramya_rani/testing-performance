// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { getLabelValue } from '@tcp/core/src/utils';
import { openPickupModalWithValues } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import {
  fetchRecommendationsDataMonetate,
  fetchRecommendationsDataAdobe,
  sendEventsToMonetate,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.actions';
import {
  getProducts,
  getLoadedProductsCount,
  getLabelsProductListing,
  getFirstSuggestedProduct,
  getHomePageRecsModuleOLabel,
  getHomePageRecsModulePLabel,
  getMonetateEnabledFlag,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import { openQuickViewWithValues } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import RecommendationsView from '@tcp/web/src/components/common/molecules/Recommendations/Recommendations';
import {
  getIsDynamicBadgeEnabled,
  getIsPDPSmoothScrollEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';

function getRecommendationProducts(state, ownProps, partNumber, reduxKey, isSequenceTwo) {
  const { isSuggestedItem } = ownProps;

  let { page } = ownProps;
  if (page === 'home page') page = 'homepage';

  const MONETATE_ENABLED = getMonetateEnabledFlag();
  let products = [];
  if (MONETATE_ENABLED === true)
    products = isSuggestedItem
      ? getFirstSuggestedProduct(state, partNumber)
      : getProducts(state, page || 'homepageTest');
  else {
    products = isSuggestedItem
      ? getFirstSuggestedProduct(state, partNumber)
      : getProducts(state, reduxKey);
    return products;
  }

  if (!Array.isArray(products)) return [];
  return products.filter((product) => {
    return isSequenceTwo ? product.isRecentViewed : !product.isRecentViewed;
  });
}

const mapStateToProps = (state, ownProps) => {
  const { portalValue, excludedIds, partNumber, sequence } = ownProps;
  const MONETATE_ENABLED = getMonetateEnabledFlag(state);
  let { page } = ownProps;
  if (page === 'home page') page = 'homepage';
  const newPortalValue = portalValue;
  const abTestConfig = null;
  const labelSuffix = `${page}_${sequence}`;
  const isSequenceTwo = sequence === '2';
  const moduleOHeaderLabel = getHomePageRecsModuleOLabel(state, isSequenceTwo, getLabelValue);

  const modulePHeaderLabel = getHomePageRecsModulePLabel(state, isSequenceTwo, getLabelValue);

  const reduxKey = `${page}_${newPortalValue || 'global'}productsData`;
  const ctaText = abTestConfig ? `CTA_TEXT_${labelSuffix}` : 'CTA_TEXT';
  const ctaTitle = abTestConfig ? `CTA_TITLE_${labelSuffix}` : 'CTA_TITLE';
  const ctaUrl = abTestConfig ? `CTA_URL_${labelSuffix}` : 'CTA_URL';

  return {
    categoryName: ownProps.categoryName,
    products: getRecommendationProducts(state, ownProps, partNumber, reduxKey, isSequenceTwo),
    moduleOHeaderLabel,
    modulePHeaderLabel,
    ctaText: getLabelValue(
      state.Labels,
      ctaText,
      Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
      'global'
    ),
    ctaTitle: getLabelValue(
      state.Labels,
      ctaTitle,
      Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
      'global'
    ),
    ctaUrl: getLabelValue(
      state.Labels,
      ctaUrl,
      Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
      'global'
    ),
    ariaPrevious: getLabelValue(state.Labels, 'previousButton', 'accessibility', 'global'),
    ariaNext: getLabelValue(state.Labels, 'nextIconButton', 'accessibility', 'global'),
    loadedProductCount: getLoadedProductsCount(state),
    labels: getLabelsProductListing(state),
    currency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    reduxKey,
    excludedIds,
    portalValue: newPortalValue,
    showPriceRange: ownProps.showPriceRange,
    cartItems: [],
    isBagLoading: false,
    isOutfitLoading: false,
    outfitItems: [],
    MONETATE_ENABLED,
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    isPDPSmoothScrollEnabled: getIsPDPSmoothScrollEnabled(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadRecommendationsMonetate: (action) => {
      dispatch(fetchRecommendationsDataMonetate(action));
    },
    loadSendEventsToMonetate: (action) => {
      dispatch(sendEventsToMonetate(action));
    },
    loadRecommendationsAdobe: (action) => {
      dispatch(fetchRecommendationsDataAdobe(action));
    },
    onPickUpOpenClick: (payload) => {
      dispatch(openPickupModalWithValues(payload));
    },
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RecommendationsView);
