// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .plcc-offer-type__label {
    font-size: 32px;
    padding-top: 4px;
    ::after {
      content: '';
      height: 2px;
      width: 100px;
      background: ${props =>
        props.theme.isGymboree
          ? props.theme.colorPalette.primary.main
          : props.theme.colors.TEXT.DARKERBLUE};
      margin: 12px auto 32px;
      display: block;
      @media ${props => props.theme.mediaQuery.medium} {
        margin: 16px auto 32px;
      }
    }
  }
  .off-text {
    font-size: 68px;
    margin-top: -30px;
    line-height: 1.4;
  }

  .super-text {
    > span {
      line-height: ${props => props.theme.typography.fontSizes.fs18};
      vertical-align: top;
      padding-top: 15px;
      display: inline-block;
    }
  }
  .get-text,
  .ten-text {
    font-size: 66px;
    @media ${props => props.theme.mediaQuery.smallMax} {
      font-size: 44px;
    }
  }
  .first-text {
    display: inline-block;
    line-height: 0;
    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: 32px;
    }
  }
  .know-text {
    display: inline-block;
    line-height: 0;
  }
  .flash-text {
    line-height: 48px;
    display: block;
    margin-top: 30px;
    @media ${props => props.theme.mediaQuery.medium} {
      margin-right: 3px;
    }
    @media ${props => props.theme.mediaQuery.large} {
      margin-left: 0;
      margin-right: 0px;
    }
    @media ${props => props.theme.mediaQuery.smallMax} {
      display: block;
      margin-top: 30px;
    }
  }
  .desc-text {
    max-width: 285px;
    margin: 16px auto 48px;
  }
  .footerLinks {
    display: flex;
    font-size: 11px;
    justify-content: space-evenly;
    max-width: 200px;
    margin: 0 auto;
  }
  .footerLink {
    font-size: 11px;
  }
  .dollar-text {
    vertical-align: top;
    line-height: 66px;
  }
  @media ${props => props.theme.mediaQuery.large} {
    padding: 0px 0 4px;
    .plcc-offer-type__label {
      ::after {
        margin-bottom: 0;
      }
    }
  }
  .sign-up__label {
    display: none;
  }
  ${props =>
    props.noModal &&
    `
    #sign-up-modal-form-intro-view {
      padding-top: 32px;
    }
  `}
`;
