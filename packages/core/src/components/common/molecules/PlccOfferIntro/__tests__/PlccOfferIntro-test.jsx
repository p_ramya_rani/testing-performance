// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PlccOfferIntroVanilla } from '../views/PlccOfferIntro';

describe('PlccOfferIntro component', () => {
  it('renders correctly', () => {
    const props = {
      className: '',
      formViewConfig: {},
    };
    const component = shallow(<PlccOfferIntroVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
