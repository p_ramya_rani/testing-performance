// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Col, RichText, Anchor } from '@tcp/core/src/components/common/atoms';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import PlccOfferIntroStyle from '../styles/PlccOfferIntro.style';

const PlccOfferIntro = ({ className, formViewConfig }) => (
  <Fragment>
    <Col
      colSize={{ small: 6, medium: 8, large: 12 }}
      ignoreGutter={{ large: true }}
      className={className}
    >
      <section id="sign-up-modal-form-intro-view">
        <BodyCopy
          fontSize="fs32"
          fontFamily="primary"
          fontWeight="black"
          textAlign="center"
          className="plcc-offer-type__label"
        >
          {formViewConfig.lbl_PLCCOffer_modal_header}
        </BodyCopy>
        <BodyCopy
          fontFamily="primary"
          className="flash-text"
          fontWeight="black"
          textAlign="center"
          component="div"
        >
          <BodyCopy
            fontSize="fs48"
            fontWeight="black"
            component="span"
            className="first-text"
            color="text.dark"
          >
            {formViewConfig.lbl_PLCCOffer_modal_sub_header_1}
          </BodyCopy>
          <BodyCopy
            fontSize="fs48"
            fontWeight="black"
            textAlign="center"
            className="know-text"
            color="text.dark"
          >
            {formViewConfig.lbl_PLCCOffer_modal_sub_header_2}
          </BodyCopy>
        </BodyCopy>
        <BodyCopy
          className="desc-text"
          fontSize="fs16"
          fontFamily="secondary"
          fontWeight="black"
          textAlign="center"
        >
          <RichText
            className="rtpsTop"
            richTextHtml={formViewConfig.lbl_PLCCOffer_takeover_applyNowSubText}
          />
        </BodyCopy>
        <div className="footerLinks">
          <Anchor
            className="footerLink"
            url={formViewConfig.lbl_PLCCModal_rewardsProgramLink}
            target="_blank"
            data-locator="plcc_rewards_terms"
            fontSizeVariation="small"
            anchorVariation="primary"
            underline
          >
            {formViewConfig.lbl_PLCCModal_rewardsProgramText}
          </Anchor>
          <Anchor
            className="footerLink"
            url={formViewConfig.lbl_PLCCModal_faqLink}
            target="_blank"
            data-locator="plcc_faq"
            fontSizeVariation="small"
            anchorVariation="primary"
            underline
          >
            {formViewConfig.lbl_PLCCModal_faqText}
          </Anchor>
          <Anchor
            className="linkIconSeperator footerLink"
            url={formViewConfig.lbl_PLCCModal_detailsLink}
            target="_blank"
            fontSizeVariation="small"
            anchorVariation="primary"
            underline
          >
            {formViewConfig.lbl_PLCCForm_details}
          </Anchor>
        </div>
      </section>
    </Col>
  </Fragment>
);

PlccOfferIntro.propTypes = {
  formViewConfig: PropTypes.shape({}),
  className: PropTypes.string,
};

PlccOfferIntro.defaultProps = {
  formViewConfig: {},
  className: '',
};

export { PlccOfferIntro as PlccOfferIntroVanilla };
export default withStyles(PlccOfferIntro, PlccOfferIntroStyle);
