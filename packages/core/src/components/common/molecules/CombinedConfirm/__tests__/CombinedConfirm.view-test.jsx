import React from 'react';
import { shallow } from 'enzyme';
import { CombinedConfirmVanilla } from '../views/CombinedConfirm.view';

describe('CombinedConfirm Component', () => {
  it('should renders correctly when subscription is success', () => {
    const props = {
      className: 'classname',
      singleSignUpLabels: {
        successHeading1: 'successHeading1',
        successHeading2: 'successHeading2',
        successMessage: 'successMessage',
        legalText: 'legalText',
      },
    };
    const component = shallow(<CombinedConfirmVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
