import { css } from 'styled-components';

const SignupConfirmStyle = css`
  margin: 31px 22px 0;
  @media ${(props) => props.theme.mediaQuery.large} {
    margin-right: 55px;
    margin-left: 55px;
  }
  .confirmation-label {
    margin: 5px auto 0;
    ::after {
      content: '';
      height: 2px;
      width: 100px;
      background: ${(props) =>
        props.theme.isGymboree
          ? props.theme.colorPalette.primary.main
          : props.theme.colors.TEXT.DARKERBLUE};
      margin: 10px auto 20px;
      display: block;
    }
  }
  .confirmation-image {
    margin: 0 5px 0;
    display: flex;
    width: 50px;
    height: 50px;
  }
  .first-label {
    width: 100%;
    margin: 28px 0 22px;
  }
  .redeem-label {
    margin: 0 60px 26px;
  }
  .tnc-label {
    font-size: ${(props) => props.theme.typography.fontSizes.fs10};
    text-align: center;
  }
  @media ${(props) => props.theme.mediaQuery.medium} {
    .first-label {
      margin: 20px 0 20px;
    }
    .redeem-label {
      margin: 0 88px 26px;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    margin-top: 34px;
    .confirmation-label {
      margin: 2px auto;
      ::after {
        margin: 10px auto 19px;
      }
    }
    .first-label {
      margin-bottom: 20px;
      margin-top: 20px;
    }
  }
  .thank-you__label {
    font-kerning: none;
  }
  .combined-icons-wrapper {
    display: flex;
    justify-content: center;
  }
`;

export default SignupConfirmStyle;
