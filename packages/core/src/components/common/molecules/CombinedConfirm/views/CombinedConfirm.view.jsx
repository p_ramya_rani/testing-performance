import React from 'react';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import RichText from '@tcp/core/src/components/common/atoms/RichText';
import { Image } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { isGymboree, getIconPath } from '@tcp/core/src/utils';
import Styles from '../styles/CombinedConfirm.style';

const CombinedConfirm = ({ className, singleSignUpLabels }) => {
  const color = isGymboree() ? 'orange' : 'blue';
  const emailIcon = `dot-email-${color}`;
  const chatIcon = `chat-${color}`;

  return (
    <div className={className} id="combined-modal-confirm-view">
      <BodyCopy
        fontSize={['fs16', 'fs28', 'fs35']}
        fontFamily="primary"
        fontWeight="black"
        textAlign="center"
        className="thank-you__label"
        color="gray.900"
        data-locator="combined_thank_you_message"
      >
        {singleSignUpLabels.successHeading1}
      </BodyCopy>
      <BodyCopy
        fontSize={['fs15', 'fs18', 'fs21']}
        fontFamily="secondary"
        textAlign="center"
        color="gray.900"
        className="confirmation-label"
        data-locator="combined_copy_text_01"
      >
        {singleSignUpLabels.successHeading2}
      </BodyCopy>
      <div className="combined-icons-wrapper">
        <Image
          src={getIconPath(emailIcon)}
          alt="email-icon"
          color="gray.900"
          className="confirmation-image"
          aria-hidden="true"
          data-locator="e-mail_icon"
        />
        <Image
          src={getIconPath(chatIcon)}
          alt="chat-icon"
          className="confirmation-image"
          aria-hidden="true"
          data-locator="sms_icon"
        />
      </div>
      <BodyCopy
        fontSize={['fs16', 'fs22', 'fs22']}
        fontFamily="secondary"
        textAlign="center"
        fontWeight="semibold"
        color="primary.dark"
        className="first-label"
        data-locator="combined_copy_text_02"
      >
        {singleSignUpLabels.successMessage}
      </BodyCopy>
      <RichText
        className="tnc-label"
        fontFamily="secondary"
        richTextHtml={singleSignUpLabels.legalText}
      />
    </div>
  );
};

CombinedConfirm.propTypes = {
  className: PropTypes.string,
  singleSignUpLabels: PropTypes.shape({}),
};

CombinedConfirm.defaultProps = {
  className: '',
  singleSignUpLabels: {},
};

export { CombinedConfirm as CombinedConfirmVanilla };
export default withStyles(CombinedConfirm, Styles);
