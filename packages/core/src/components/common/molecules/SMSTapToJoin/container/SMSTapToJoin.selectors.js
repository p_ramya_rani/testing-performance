// 9fbef606107a605d69c0edbcd8029e5d
import { getLabelValue } from '@tcp/core/src/utils';

const getTapToJoinLabels = state => {
  return {
    footer: {
      SMSBody: getLabelValue(state.Labels, `lbl_footer_SMS_bodyText`, 'emailSignup', 'global'),
      SMSUserText1: getLabelValue(
        state.Labels,
        `lbl_footer_SMS_userText1`,
        'emailSignup',
        'global'
      ),
      SMSUserText2: getLabelValue(
        state.Labels,
        `lbl_footer_SMS_userText2`,
        'emailSignup',
        'global'
      ),
      SMSUserText3: getLabelValue(
        state.Labels,
        `lbl_footer_SMS_userText3`,
        'emailSignup',
        'global'
      ),
      SMSNumber: getLabelValue(state.Labels, `lbl_footer_SMS_textNumber`, 'emailSignup', 'global'),
    },
    landing: {
      SMSBody: getLabelValue(state.Labels, `lbl_landing_SMS_bodyText`, 'emailSignup', 'global'),
      SMSUserText1: getLabelValue(
        state.Labels,
        `lbl_landing_SMS_userText1`,
        'emailSignup',
        'global'
      ),
      SMSUserText2: getLabelValue(
        state.Labels,
        `lbl_landing_SMS_userText2`,
        'emailSignup',
        'global'
      ),
      SMSUserText3: getLabelValue(
        state.Labels,
        `lbl_landing_SMS_userText3`,
        'emailSignup',
        'global'
      ),
      SMSNumber: getLabelValue(state.Labels, `lbl_landing_SMS_textNumber`, 'emailSignup', 'global'),
    },
    overlay: {
      SMSBody: getLabelValue(state.Labels, `lbl_overlay_SMS_bodyText`, 'emailSignup', 'global'),
      SMSUserText1: getLabelValue(
        state.Labels,
        `lbl_overlay_SMS_userText1`,
        'emailSignup',
        'global'
      ),
      SMSUserText2: getLabelValue(
        state.Labels,
        `lbl_overlay_SMS_userText2`,
        'emailSignup',
        'global'
      ),
      SMSUserText3: getLabelValue(
        state.Labels,
        `lbl_overlay_SMS_userText3`,
        'emailSignup',
        'global'
      ),
      SMSNumber: getLabelValue(state.Labels, `lbl_overlay_SMS_textNumber`, 'emailSignup', 'global'),
    },
    SMSButtonText: getLabelValue(state.Labels, `lbl_sms_button_text`, 'emailSignup', 'global'),
    SMSSignUpLegalText: getLabelValue(
      state.Labels,
      `lbl_sms_signup_legal_text`,
      'emailSignup',
      'global'
    ),
    SMSHomeBodyText: getLabelValue(state.Labels, `lbl_home_SMS_bodyText`, 'emailSignup', 'global'),
    SMSNavBodyText: getLabelValue(state.Labels, `lbl_nav_SMS_bodyText`, 'emailSignup', 'global'),
  };
};

export default {
  getTapToJoinLabels,
};
