// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import SMSTapToJoin from '../views';
import SMSTapToJoinSelector from './SMSTapToJoin.selectors';

const mapStateToProps = state => {
  return {
    tapToJoinLabels: SMSTapToJoinSelector.getTapToJoinLabels(state),
  };
};

export default connect(mapStateToProps)(SMSTapToJoin);
