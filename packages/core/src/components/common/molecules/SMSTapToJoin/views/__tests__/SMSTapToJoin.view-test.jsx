// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { SMSTapToJoinVanilla } from '../SMSTapToJoin.view';

jest.mock('@tcp/core/src/utils', () => ({
  isGymboree: () => false,
  isClient: () => true,
  getIconPath: jest.fn(),
  isAndroidWeb: () => true,
}));

const props = {
  className: 'abf84ur 48fjoeh',
  smsFromPage: 'footer',
  tapToJoinLabels: {
    footer: {
      SMSBody: 'lbl_overlay_SMS_bodyText',
      SMSUserText1: 'lbl_overlay_SMS_userText1',
      SMSUserText2: 'lbl_overlay_SMS_userText2',
      SMSUserText3: 'lbl_overlay_SMS_userText3',
      SMSNumber: '12345',
    },
    landing: {
      SMSBody: 'lbl_overlay_SMS_bodyText',
      SMSUserText1: 'lbl_overlay_SMS_userText1',
      SMSUserText2: 'lbl_overlay_SMS_userText2',
      SMSUserText3: 'lbl_overlay_SMS_userText3',
      SMSNumber: '12345',
    },
    overlay: {
      SMSBody: 'lbl_overlay_SMS_bodyText',
      SMSUserText1: 'lbl_overlay_SMS_userText1',
      SMSUserText2: 'lbl_overlay_SMS_userText2',
      SMSUserText3: 'lbl_overlay_SMS_userText3',
      SMSNumber: '12345',
    },
    SMSButtonText: 'lbl_sms_button_text',
    SMSSignUpLegalText: 'lbl_sms_signup_legal_text',
  },
};
describe('SMSTapToJoin', () => {
  it('should render correctly', () => {
    const tree = shallow(<SMSTapToJoinVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly for landing', () => {
    const newProps = {
      ...props,
      smsFromPage: 'landing',
    };
    const tree = shallow(<SMSTapToJoinVanilla {...newProps} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly for overlay', () => {
    const newProps = {
      ...props,
      smsFromPage: 'overlay',
    };
    const tree = shallow(<SMSTapToJoinVanilla {...newProps} />);
    expect(tree).toMatchSnapshot();
  });
});
