// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Button, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import withStyles from '../../../hoc/withStyles';
import styles, { FOOTER } from '../styles/SMSTapToJoin.style';
import { getIconPath } from '../../../../../utils';

const renderBodyText = (isFooterPage, SMSUserText1, SMSUserText2, isGym) => {
  return (
    <div className="tap-to-join-text-copy">
      {SMSUserText1 && (
        <BodyCopy
          component={isFooterPage ? 'span' : 'div'}
          color={isFooterPage ? 'gray.800' : 'gray.900'}
          fontWeight={isFooterPage ? 'black' : 'extrabold'}
          fontFamily="secondary"
          fontSize="fs14"
          className="sms_body_text_join"
          dataLocator="sms_promo_text"
        >
          {SMSUserText1}
        </BodyCopy>
      )}
      {!(isFooterPage && isGym) && (
        <BodyCopy
          component="span"
          fontFamily="secondary"
          fontWeight={isFooterPage ? 'black' : 'extrabold'}
          fontSize={isFooterPage ? 'fs14' : 'fs32'}
          className="sms_body_text_off"
          dataLocator="sms_promo_text"
        >
          {SMSUserText2}
        </BodyCopy>
      )}
    </div>
  );
};
const SMSTapToJoin = (props) => {
  const { className, smsFromPage, tapToJoinLabels, isGym } = props;
  const { SMSUserText1, SMSUserText2, SMSUserText3, SMSBody, SMSNumber } =
    tapToJoinLabels[smsFromPage] || {};
  const smsTemplate = `sms:${SMSNumber}?&body=${SMSBody}`;
  const isFooterPage = smsFromPage === FOOTER;
  return (
    <div className={className}>
      <div className="tap-to-join-wrapper">
        {!isFooterPage && (
          <Image
            src={getIconPath('blue-sms-icon')}
            className="tap-to-join-blue-image"
            alt="tcp to join"
          />
        )}
        {renderBodyText(isFooterPage, SMSUserText1, SMSUserText2, isGym)}
      </div>
      {isFooterPage && (
        <Image
          src={getIconPath('black-sms-icon')}
          className="tap-to-join-black-image"
          alt="tcp to join"
        />
      )}
      {!isFooterPage && (
        <BodyCopy
          component="div"
          fontFamily="secondary"
          fontWeight="normal"
          fontSize="fs14"
          className="sms_body_text_extra"
          dataLocator="sms_promo_text"
        >
          {SMSUserText3}
        </BodyCopy>
      )}
      <div id="vibes_tap2join_box">
        <div className="button-container">
          <a href={smsTemplate} className="anchor">
            <Button
              buttonVariation="fixed-width"
              fill={isFooterPage ? 'WHITE' : 'BLUE'}
              className="tapToJoin-cta"
            >
              {tapToJoinLabels.SMSButtonText}
            </Button>
          </a>
        </div>
      </div>
    </div>
  );
};

SMSTapToJoin.propTypes = {
  className: PropTypes.string,
  smsFromPage: PropTypes.string,
  tapToJoinLabels: PropTypes.shape({}),
  isGym: PropTypes.bool,
};

SMSTapToJoin.defaultProps = {
  className: '',
  smsFromPage: 'footer',
  tapToJoinLabels: {},
  isGym: false,
};

export default withStyles(SMSTapToJoin, styles);

export { SMSTapToJoin as SMSTapToJoinVanilla };
