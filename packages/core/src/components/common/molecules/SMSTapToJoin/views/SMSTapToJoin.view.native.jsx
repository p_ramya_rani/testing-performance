// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Linking } from 'react-native';
import { BodyCopy, Image, RichText } from '@tcp/core/src/components/common/atoms';
import {
  SMSTapToJoinWrapper,
  SMSTopTextWrapper,
  SMSTopText,
  SMSButtonWrapper,
  AnchorView,
  RichTextWrapper,
} from '../styles/SMSTapToJoin.style.native';
import { isGymboree } from '../../../../../utils';

const blueSMSIcon = require('../../../../../../../mobileapp/src/assets/images/smsblue.png');
const blackSMSIcon = require('../../../../../../../mobileapp/src/assets/images/smsblack.png');

const renderBodyText = (SMSUserText1, SMSUserText2, isGym) => {
  return (
    <SMSTopText>
      {SMSUserText1 && (
        <BodyCopy
          color="gray.900"
          fontWeight="extrabold"
          fontFamily="primary"
          fontSize="fs16"
          dataLocator="sms_promo_text"
          text={SMSUserText1.toUpperCase()}
        />
      )}
      {!isGym && (
        <BodyCopy
          color="blue.500"
          fontFamily="primary"
          fontWeight="extrabold"
          fontSize="fs16"
          dataLocator="sms_promo_text"
          text={SMSUserText2.toUpperCase()}
        />
      )}
    </SMSTopText>
  );
};
const SMSTapToJoin = props => {
  const { smsFromPage, tapToJoinLabels, navigation, isHomePage } = props;
  const { SMSUserText1, SMSUserText2, SMSUserText3, SMSNumber } =
    tapToJoinLabels[smsFromPage] || {};
  const isGym = isGymboree();
  const smsBodyText = isHomePage ? tapToJoinLabels.SMSHomeBodyText : tapToJoinLabels.SMSNavBodyText;
  const smsTemplate = `sms:${SMSNumber}?&body=${smsBodyText}`;
  let height = '51px';
  let width = '42px';
  if (isGym) {
    height = '33px';
    width = '27px';
  }
  return (
    <SMSTapToJoinWrapper>
      <SMSTopTextWrapper className="tap-to-join-wrapper">
        {renderBodyText(SMSUserText1, SMSUserText2, isGym)}
      </SMSTopTextWrapper>
      {!isGym && (
        <BodyCopy
          fontFamily="secondary"
          fontWeight="regular"
          fontSize="fs12"
          dataLocator="sms_promo_text"
          text={SMSUserText3}
        />
      )}
      <SMSButtonWrapper>
        <Image source={isGym ? blackSMSIcon : blueSMSIcon} width={width} height={height} />
        <AnchorView
          anchorVariation="primary"
          navigation={navigation}
          onPress={() => Linking.openURL(smsTemplate)}
          text={tapToJoinLabels.SMSButtonText}
          isGym={isGym}
        />
      </SMSButtonWrapper>
      <RichTextWrapper>
        <RichText source={tapToJoinLabels.SMSSignUpLegalText} />
      </RichTextWrapper>
    </SMSTapToJoinWrapper>
  );
};

SMSTapToJoin.propTypes = {
  smsFromPage: PropTypes.string,
  tapToJoinLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  isHomePage: PropTypes.bool,
};

SMSTapToJoin.defaultProps = {
  smsFromPage: 'footer',
  tapToJoinLabels: {},
  navigation: {},
  isHomePage: false,
};

export default SMSTapToJoin;
