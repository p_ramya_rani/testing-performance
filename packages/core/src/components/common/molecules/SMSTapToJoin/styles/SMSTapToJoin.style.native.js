// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { Anchor } from '@tcp/core/src/components/common/atoms';
import { Dimensions } from 'react-native';

const window = Dimensions.get('window');

const SMSTopTextWrapper = styled.View`
  display: flex;
  flex-direction: row;
`;

const SMSTopText = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const SMSButtonWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  flex-direction: row;
  align-items: center;
`;

const AnchorView = styled(Anchor)`
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  background-color: ${props => props.theme.colorPalette.blue.C900};
  color: ${props => props.theme.colorPalette.white};
  padding: ${props => props.theme.spacing.ELEM_SPACING.MED} 51px;
  text-transform: uppercase;
  font-weight: 800;
  font-size: 14px;
  ${props =>
    props.isGym
      ? `
  padding: ${props.theme.spacing.ELEM_SPACING.SM} 51px 11px 52px;
  border-radius: ${props.theme.spacing.APP_LAYOUT_SPACING.XS};
  overflow: hidden;
  `
      : ''}
`;

const SMSTapToJoinWrapper = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 14px 0;
`;

const RichTextWrapper = styled.View`
  width: ${window.width - 40};
  padding: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS} 17px
    ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  height: 230px;
`;

export {
  SMSTapToJoinWrapper,
  SMSTopTextWrapper,
  SMSTopText,
  SMSButtonWrapper,
  AnchorView,
  RichTextWrapper,
};
