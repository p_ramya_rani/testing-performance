// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export const FOOTER = 'footer';

const styles = css`
  text-align: center;
  width: 100%;
  .button-container {
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    -webkit-tap-highlight-color: transparent;
    margin: ${props =>
        props.smsFromPage === FOOTER
          ? props.theme.spacing.ELEM_SPACING.MED
          : props.theme.spacing.ELEM_SPACING.XL}
      auto ${props => (props.smsFromPage === FOOTER ? '20px' : 0)};
  }

  .tapToJoin-cta {
    width: 200px;
    height: 51px;
    -webkit-tap-highlight-color: transparent;
    font-weight: ${props => props.theme.fonts.fontWeight.black};
  }

  .tapToJoin-cta:focus {
    background-color: ${props => props.theme.colors.WHITE};
    outline: none;
  }

  .tapToJoin-cta:hover {
    border: solid 1px ${props => props.theme.colors.TEXT.GRAY};
    background-color: ${props => props.theme.colors.PRIMARY.PALEGRAY};
  }

  .anchor {
    text-decoration: none;
    display: inline-block;
  }

  .tap-to-join-wrapper {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .tap-to-join-blue-image {
    width: 43px;
    height: 57px;
  }
  .tap-to-join-black-image {
    padding-top: 25px;
    width: 31px;
    height: 36px;
  }
  .tap-to-join-text-copy {
    padding-left: 18px;
    text-transform: uppercase;
    ${props => (props.smsFromPage !== FOOTER ? 'max-width: 210px;' : '')}
    text-align: left;
    flex-wrap: wrap;
  }
  .sms_body_text_extra {
    text-align: center;
    margin-top: 16px;
  }
  .sms_body_text_off {
    letter-spacing: -0.3px;
    color: ${props => props.theme.colorPalette.primary.main};
  }
`;

export default styles;
