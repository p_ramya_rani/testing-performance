/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import Grid from '@tcp/core/src/components/common/molecules/Grid';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/views/Carousel';
import { Anchor, Button, Col, DamImage, Row } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ProductTabList from '@tcp/core/src/components/common/organisms/ProductTabList';
import {
  getIconPath,
  getLocator,
  getProductUrlForDAM,
  getBrand,
  styleOverrideEngine,
  getViewportInfo,
  mergeUrlQueryParams,
  isClient,
} from '@tcp/core/src/utils';
import { getHeaderBorderRadius } from '@tcp/core/src/utils/utils.web';
import moduleJStyle, { StyledSkeleton } from '../styles/ModuleJ.style';
import moduleJConfig from '../moduleJ.config';

class ModuleJ extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentCatId: '',
      currentTabItem: [],
      promoImage1: null,
      promoLink1: null,
      promoVideo1: null,
      promoImage2: null,
      promoLink2: null,
      promoVideo2: null,
    };
  }

  getHeaderBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  setDefaultMediaLinkedList = () => {
    const { mediaLinkedList } = this.props;

    const promoMediaLinkedList = mediaLinkedList || [];
    const {
      image: promoImage1,
      link: promoLink1,
      video: promoVideo1,
    } = promoMediaLinkedList[0] || {};
    const {
      image: promoImage2,
      link: promoLink2,
      video: promoVideo2,
    } = promoMediaLinkedList[1] || {};

    this.setState({
      promoImage1,
      promoLink1,
      promoVideo1,
      promoImage2,
      promoLink2,
      promoVideo2,
    });
  };

  onTabChange = (catId, tabItem) => {
    const { mediaLinkedList } = tabItem;
    const promoMediaLinkedList = mediaLinkedList || [];
    const {
      image: promoImage1,
      link: promoLink1,
      video: promoVideo1,
    } = promoMediaLinkedList[0] || {};
    const {
      image: promoImage2,
      link: promoLink2,
      video: promoVideo2,
    } = promoMediaLinkedList[1] || {};

    if ((promoImage1 || promoVideo1) && (promoImage2 || promoVideo2)) {
      this.setState({
        promoImage1,
        promoLink1,
        promoVideo1,
        promoImage2,
        promoLink2,
        promoVideo2,
      });
    } else {
      this.setDefaultMediaLinkedList();
    }
    this.setState({ currentCatId: catId, currentTabItem: [tabItem] });
  };

  getCurrentCtaButton = () => {
    const { currentTabItem } = this.state;
    const { isHpNewDesignCTAEnabled } = this.props;
    if (!currentTabItem || !currentTabItem.length) {
      return null;
    }
    const { singleCTAButton: currentSingleCTAButton } = currentTabItem.length && currentTabItem[0];
    return currentSingleCTAButton ? (
      <Row centered>
        <Col
          colSize={{
            small: 4,
            medium: 2,
            large: 2,
          }}
        >
          <Button
            buttonVariation="fixed-width"
            className="cta-btn"
            cta={currentSingleCTAButton}
            dataLocator={getLocator('moduleJ_cta_btn')}
            isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
          >
            {currentSingleCTAButton.text}
          </Button>
        </Col>
      </Row>
    ) : null;
  };

  getHeaderText = (headerStyle) => {
    const { headerText, layout } = this.props;
    return headerText && layout !== 'alt' ? (
      <div className="promo-header-wrapper">
        <LinkText
          component="h2"
          type="heading"
          headerText={headerText}
          className="promo-header eyebrow"
          dataLocator={getLocator('moduleJ_header_text')}
          headerStyle={headerStyle}
        />
      </div>
    ) : (
      <LinkText
        component="h2"
        headerText={headerText}
        className="promo-header"
        dataLocator={getLocator('moduleJ_header_text')}
        headerStyle={headerStyle}
      />
    );
  };

  getPromoBanner = (promoStyle) => {
    const { promoBanner } = this.props;
    return (
      promoBanner && (
        <PromoBanner
          promoBanner={promoBanner}
          className="promoBanner"
          dataLocator={getLocator('moduleJ_promobanner_text')}
          promoStyle={promoStyle}
        />
      )
    );
  };

  renderItem = (data, itemClassName = '') => {
    const { icidParam, isHomePage } = this.props;
    const { currentCatId } = this.state;
    return data.map(({ uniqueId, pdpUrl, pdpAsPath, product_name: productName }, index) => {
      const asPathWithTrackingParam = mergeUrlQueryParams(pdpAsPath, icidParam);
      const newPdpUrl = pdpUrl && `${pdpUrl}&dataSource=module_J_${currentCatId}`;
      return (
        <div key={index.toString()} className={itemClassName}>
          <Anchor
            className="image-link"
            to={newPdpUrl}
            asPath={asPathWithTrackingParam}
            dataLocator={`${getLocator('moduleJ_product_image')}${index}`}
          >
            <DamImage
              imgData={{ url: getProductUrlForDAM(uniqueId), alt: productName }}
              imgConfigs={moduleJConfig.IMG_DATA.productImgConfig}
              isProductImage
              isHomePage={isHomePage}
              isOptimizedVideo
              lazyLoad
              isModule
            />
          </Anchor>
        </div>
      );
    });
  };
  /**
   * this Funtion will be used to render carousel for desktop and tablet.
   * this will render scroll list for mobile
   */

  renderProducts = (data, CAROUSEL_OPTIONS, iconPath) => {
    if (!data || !isClient()) {
      return null;
    }
    const isMobile = isClient() ? getViewportInfo().isMobile : null;
    if (isMobile) {
      return (
        <div className="smooth-scroll-list">{this.renderItem(data, 'smooth-scroll-list-item')}</div>
      );
    }
    return (
      <Carousel
        options={CAROUSEL_OPTIONS}
        carouselConfig={{
          autoplay: false,
          variation: 'big-arrows',
          customArrowLeft: iconPath,
          customArrowRight: iconPath,
        }}
        isModule
      >
        {this.renderItem(data)}
      </Carousel>
    );
  };

  getBorderStyle = (border) => {
    return border ? { borderTop: `3px solid ${border}` } : {};
  };

  render() {
    const {
      className,
      productTabList = {},
      layout,
      divTabs,
      moduleClassName,
      page,
      isHomePage,
      isHpNewModuleDesignEnabled,
    } = this.props;
    const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleJ');
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const borderStyle = this.getBorderStyle(styleOverrides.border);
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );
    const { currentCatId } = this.state;
    const { promoImage1, promoLink1, promoVideo1, promoImage2, promoLink2, promoVideo2 } =
      this.state;
    const { CAROUSEL_OPTIONS, IMG_DATA, TOTAL_IMAGES } = moduleJConfig;
    let data = productTabList[currentCatId];
    data = data ? data.slice(0, TOTAL_IMAGES) : [];
    const iconPath = getIconPath('carousel-big-carrot');
    let dataStatus = true;
    if (productTabList && productTabList.completed) {
      dataStatus = productTabList.completed[currentCatId];
    }

    return (
      <Grid
        className={`${className} ${moduleClassName} moduleJ layout-${layout} brand-${getBrand()} page-${page}`}
      >
        {layout !== 'alt' ? (
          <div>
            <Row fullBleed={{ small: true, medium: true }} className="topview">
              <Col
                customStyle={borderStyle}
                className="topbar"
                colSize={{
                  small: 6,
                  medium: 8,
                  large: 12,
                }}
                style={headerBorderRadiusOverride}
              />
            </Row>
            <Row fullBleed={{ small: true, medium: true }} className="topview">
              <Col
                className="promo-image-left"
                colSize={{
                  small: 6,
                  medium: 2,
                  large: 3,
                }}
              >
                <DamImage
                  imgConfigs={IMG_DATA.promoImgConfig}
                  imgData={promoImage1}
                  data-locator={`${getLocator('moduleJ_promobanner_img')}${1}`}
                  link={promoLink1}
                  videoData={promoVideo1}
                  isHomePage={isHomePage}
                  isOptimizedVideo
                  lazyLoad
                  isModule
                />
              </Col>
              <Col
                className="promo"
                colSize={{
                  small: 6,
                  medium: 4,
                  large: 6,
                }}
                ignoreGutter={{
                  small: true,
                }}
              >
                {this.getHeaderText(headerStyle)}
                {this.getPromoBanner(styleOverrides?.promo)}
                <div className="product-tab-list">
                  <ProductTabList
                    onProductTabChange={this.onTabChange}
                    tabItems={divTabs}
                    dataLocator={getLocator('moduleJ_cta_link')}
                  />
                </div>
              </Col>
              <Col
                className="promo-image-right"
                colSize={{
                  small: 0,
                  medium: 2,
                  large: 3,
                }}
                hideCol={{
                  small: true,
                }}
              >
                <DamImage
                  className="promo-img"
                  imgConfigs={IMG_DATA.promoImgConfig}
                  imgData={promoImage2}
                  data-locator={`${getLocator('moduleJ_promobanner_img')}${2}`}
                  link={promoLink2}
                  videoData={promoVideo2}
                  isHomePage={isHomePage}
                  isOptimizedVideo
                  lazyLoad
                  isModule
                />
              </Col>
            </Row>
          </div>
        ) : (
          <Row fullBleed={{ small: true, medium: true, large: true }} className="topview">
            <Col
              className="promo-alt"
              colSize={{
                small: 6,
                medium: 8,
                large: 12,
              }}
              ignoreGutter={{
                small: true,
                medium: true,
                large: true,
              }}
              style={headerBorderRadiusOverride}
            >
              {this.getHeaderText(headerStyle)}
              {this.getPromoBanner(styleOverrides?.promo)}
            </Col>
            <Col
              colSize={{
                small: 6,
                medium: 8,
                large: 12,
              }}
            >
              <ProductTabList
                onProductTabChange={this.onTabChange}
                tabItems={divTabs}
                dataLocator={getLocator('moduleJ_cta_link')}
              />
            </Col>
          </Row>
        )}
        <Row className="product-image">
          <Col
            className="moduleJ__carousel-wrapper"
            colSize={{
              small: 6,
              medium: 8,
              large: 10,
            }}
            offsetLeft={{
              small: 0,
              medium: 0,
              large: 1,
            }}
            offsetRight={{
              small: 0,
              medium: 0,
              large: 1,
            }}
          >
            {dataStatus ? (
              <StyledSkeleton
                col={6}
                colSize={{ small: 2, medium: 2, large: 2 }}
                showArrows
                removeLastMargin
              />
            ) : null}
            {this.renderProducts(data, CAROUSEL_OPTIONS, iconPath)}
          </Col>
        </Row>
        {this.getCurrentCtaButton()}
      </Grid>
    );
  }
}

ModuleJ.defaultProps = {
  mediaLinkedList: [],
  promoBanner: [],
  layout: 'default',
  moduleClassName: '',
  page: '',
  icidParam: '',
};

ModuleJ.propTypes = {
  className: PropTypes.string.isRequired,
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  productTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          uniqueId: PropTypes.string.isRequired,
          imageUrl: PropTypes.array.isRequired,
          seo_token: PropTypes.string,
        })
      )
    )
  ).isRequired,
  mediaLinkedList: PropTypes.arrayOf(
    PropTypes.shape({
      image: PropTypes.object,
      link: PropTypes.object,
    })
  ),
  layout: PropTypes.string.isRequired,
  divTabs: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.object,
      category: PropTypes.object,
      singleCTAButton: PropTypes.object,
    })
  ).isRequired,
  moduleClassName: PropTypes.string,
  page: PropTypes.string,
  icidParam: PropTypes.string,
  isHomePage: PropTypes.bool.isRequired,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

const styledModuleJ = withStyles(errorBoundary(ModuleJ), moduleJStyle);
styledModuleJ.defaultProps = ModuleJ.defaultProps;
export default styledModuleJ;
export { ModuleJ as ModuleJVanilla };
