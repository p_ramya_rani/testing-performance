// 9fbef606107a605d69c0edbcd8029e5d 
import PropTypes, { bool, string, array, object, shape, arrayOf } from 'prop-types';

const defaultProps = {
  mediaLinkedList: [],
  promoBanner: [],
  layout: 'default',
  moduleClassName: '',
  lowQualityEnabled: false,
  page: '',
  icidParam: '',
};

const propTypes = {
  className: string.isRequired,
  headerText: arrayOf(
    shape({
      link: object,
      textItems: array,
    })
  ).isRequired,
  promoBanner: arrayOf(
    shape({
      link: object,
      textItems: array,
    })
  ),
  productTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      arrayOf(
        shape({
          uniqueId: string.isRequired,
          imageUrl: array.isRequired,
          seo_token: string,
        })
      )
    )
  ).isRequired,
  mediaLinkedList: arrayOf(
    shape({
      image: object,
      link: object,
    })
  ),
  layout: string.isRequired,
  divTabs: arrayOf(
    shape({
      text: object,
      category: object,
      singleCTAButton: object,
    })
  ).isRequired,
  moduleClassName: string,
  page: string,
  icidParam: string,
  lowQualityEnabled: bool,
  isHomePage: PropTypes.bool.isRequired,
};

export { propTypes, defaultProps };
