// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable no-useless-constructor */
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, View } from 'react-native';
import { LAZYLOAD_HOST_NAME, isAndroid } from '@tcp/core/src/utils';
import InViewPort from '@tcp/core/src/components/common/atoms/InViewPort';
import colors from '../../../../../../styles/themes/TCP/colors';
import { Button, Anchor, DamImage, Skeleton } from '../../../atoms';
import {
  getLocator,
  validateColor,
  getProductUrlForDAM,
  getScreenWidth,
  getBrand,
  styleOverrideEngine,
  goToPlpTab,
  getMediaBorderRadius,
  getHeaderBorderRadius,
} from '../../../../../utils/index.native';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';

import moduleJConfig from '../moduleJ.config';

import {
  Container,
  ImageItemWrapper,
  ImageSlidesWrapper,
  ImageSlideWrapper,
  ButtonContainer,
  StyledImage,
  PromoContainer,
  HeaderContainer,
  SecondHeaderContainer,
  ImageContainer,
  MessageContainer,
  Border,
  Wrapper,
  StyledProductTabList,
} from '../styles/ModuleJ.style.native';

import PromoBanner from '../../PromoBanner';
import LinkText from '../../LinkText';

const PRODUCT_IMAGE_WIDTH = 89;
const PRODUCT_IMAGE_HEIGHT = 110;
const PRODUCT_IMAGE_PER_SLIDE = 4;
const { IMG_DATA, TOTAL_IMAGES } = moduleJConfig;
const LARGE_PRODUCT_IMAGE = 310;
class ModuleJ extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectedCategoryId: null,
      selectedTabItem: {},
      currentIndexCarousel: 0,
      visible: !isAndroid(),
      promoImage1: null,
      promoVideo1: null,
      promoLink2: null,
    };

    this.brand = getBrand();
  }

  setDefaultMediaLinkedList = () => {
    const { mediaLinkedList } = this.props;
    const promoMediaLinkedList = mediaLinkedList || [];
    const { image: promoImage1, video: promoVideo1 } = promoMediaLinkedList[0] || {};
    const { link: promoLink2 } = promoMediaLinkedList[1] || {};

    this.setState({
      promoImage1,
      promoVideo1,
      promoLink2,
    });
  };

  onProductTabChange = (catId, tabItem) => {
    const { mediaLinkedList } = tabItem;
    const promoMediaLinkedList = mediaLinkedList || [];
    const { image: promoImage1, video: promoVideo1 } = promoMediaLinkedList[0] || {};
    const { link: promoLink2 } = promoMediaLinkedList[1] || {};

    if (promoImage1 || promoVideo1) {
      this.setState({
        promoImage1,
        promoVideo1,
        promoLink2,
      });
    } else {
      this.setDefaultMediaLinkedList();
    }

    this.setState({
      selectedCategoryId: catId.toString(),
      selectedTabItem: tabItem,
    });
  };

  getOptimizedImageUrl = (index, currentIndexCarousel, uniqueId) => {
    return getProductUrlForDAM(uniqueId);
  };

  renderCarouselSlide = slideProps => {
    const { item, index } = slideProps;
    const { navigation, productTabList, icidParam, borderWidth = 0 } = this.props;
    const { selectedCategoryId, currentIndexCarousel, visible } = this.state;
    let selectedProductList = productTabList[selectedCategoryId];
    selectedProductList = selectedProductList ? selectedProductList.slice(0, TOTAL_IMAGES) : [];
    const borderWidthAdjustment = borderWidth && borderWidth + 2;
    return (
      <ImageSlideWrapper>
        {item.map(productItem => {
          const { uniqueId, product_name: productName, productItemIndex } = productItem;
          return (
            <ImageItemWrapper
              key={uniqueId}
              isFullMargin={productItemIndex === selectedProductList.length - 1}
              accessible
              accessibilityLabel={productName}
              accessibilityRole="link"
            >
              <Anchor
                onPress={() => {
                  goToPlpTab(navigation);
                  return navigation.navigate('ProductDetail', {
                    title: productName,
                    pdpUrl: uniqueId,
                    selectedColorProductId: uniqueId,
                    reset: true,
                    internalCampaign: icidParam,
                  });
                }}
                navigation={navigation}
                testID={`${getLocator('moduleJ_product_image')}${productItemIndex}`}
              >
                <StyledImage
                  alt={productName}
                  host={LAZYLOAD_HOST_NAME.HOME}
                  url={visible && this.getOptimizedImageUrl(index, currentIndexCarousel, uniqueId)}
                  height={PRODUCT_IMAGE_HEIGHT}
                  width={PRODUCT_IMAGE_WIDTH - borderWidthAdjustment}
                  imgConfig={IMG_DATA.productImgConfig[0]}
                  isProductImage
                  isFastImage
                />
              </Anchor>
            </ImageItemWrapper>
          );
        })}
      </ImageSlideWrapper>
    );
  };

  updateCurrentIndex = index => {
    this.setState({ currentIndexCarousel: index });
  };

  renderCarousel = () => {
    const { selectedCategoryId } = this.state;
    const { productTabList } = this.props;
    let selectedProductList = productTabList[selectedCategoryId] || [];
    selectedProductList = selectedProductList.slice(0, TOTAL_IMAGES);

    const selectedProductCarouselList = selectedProductList.reduce(
      (list, item, index) => {
        const lastList = list[list.length - 1];
        if (lastList.length === PRODUCT_IMAGE_PER_SLIDE) {
          list.push([{ ...item, productItemIndex: index }]);
        } else {
          lastList.push({ ...item, productItemIndex: index });
        }

        return list;
      },
      [[]]
    );
    let dataStatus = true;
    if (productTabList && productTabList.completed) {
      dataStatus = productTabList.completed[selectedCategoryId];
    }
    if (dataStatus) {
      return (
        <Skeleton
          row={1}
          col={3}
          width={PRODUCT_IMAGE_WIDTH}
          height={PRODUCT_IMAGE_HEIGHT}
          rowProps={{ justifyContent: 'space-around', marginTop: '10px' }}
        />
      );
    }
    return (
      <ImageSlidesWrapper>
        {selectedProductList.length ? (
          <FlatList
            data={selectedProductCarouselList}
            renderItem={this.renderCarouselSlide}
            refreshing={false}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(_, index) => index.toString()}
          />
        ) : null}
      </ImageSlidesWrapper>
    );
  };

  /**
   * To Render the Dam Image or Video Component
   */
  renderDamImage = (mediaLinkedList, imgData, videoData, navigation) => {
    const { visible, promoLink2 } = this.state;

    const damImageComp = (
      <DamImage
        imgData={visible ? imgData : null}
        videoData={visible ? videoData : null}
        height={LARGE_PRODUCT_IMAGE}
        testID={`${getLocator('moduleJ_promobanner_img')}${1}`}
        alt={imgData && imgData.alt}
        imgConfig={IMG_DATA.promoImgConfig[0]}
        isFastImage
        isHomePage
      />
    );
    if (imgData && Object.keys(imgData).length > 0) {
      return (
        <Anchor navigation={navigation} url={promoLink2?.url}>
          {damImageComp}
        </Anchor>
      );
    }
    return videoData && Object.keys(videoData).length > 0 ? (
      <React.Fragment>{damImageComp}</React.Fragment>
    ) : null;
  };

  renderImageContainer = () => {
    const { mediaLinkedList, navigation, layout } = this.props;
    const { promoImage1, promoVideo1 } = this.state;
    const videoData = promoVideo1 && {
      ...promoVideo1,
      videoWidth: getScreenWidth(),
      videoHeight: 310,
    };
    const imgData = promoImage1 || {};
    return (
      <ImageContainer layout={layout}>
        {this.renderDamImage(mediaLinkedList, imgData, videoData, navigation)}
      </ImageContainer>
    );
  };

  checkVisible = isVisible => {
    const { visible } = this.state;
    if (isVisible) {
      if (!visible) {
        this.setState({ visible: true });
      }
    } else if (visible) {
      this.setState({ visible: false });
    }
  };

  getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled && styleOverrides
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  render() {
    const { selectedTabItem: { singleCTAButton: selectedSingleCTAButton } = {} } = this.state;
    const {
      navigation,
      layout,
      headerText,
      promoBanner,
      divTabs,
      bgColor,
      moduleClassName,
      isHpNewLayoutEnabled,
    } = this.props;
    const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleJ');
    const borderStyle = styleOverrides.border ? styleOverrides.border.color : '';
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const containerStyle = isHpNewLayoutEnabled
      ? {
          ...HP_NEW_LAYOUT.MODULE_BOX_SHADOW,
        }
      : {};
    const productTabStyle = isHpNewLayoutEnabled
      ? { ...mediaBorderRadiusOverride, backgroundColor: colors?.WHITE }
      : {};

    const renderMainView = () => (
      <Container style={containerStyle}>
        <MessageContainer
          layout={layout}
          bgColor={validateColor(bgColor)}
          brand={this.brand}
          style={{ ...headerBorderRadiusOverride }}
        >
          <Wrapper>
            <Border borderColor={borderStyle} layout={layout} />
            <HeaderContainer layout={layout} style={layout !== 'alt' && headerBorderRadiusOverride}>
              {headerText && [headerText[0]] && (
                <LinkText
                  navigation={navigation}
                  headerText={[headerText[0]]}
                  testID={getLocator('moduleJ_header_text_0')}
                  useStyle
                  renderComponentInNewLine
                  headerStyle={[styleOverrides.title]}
                />
              )}
            </HeaderContainer>

            <SecondHeaderContainer>
              {headerText && [headerText[1]] && (
                <LinkText
                  navigation={navigation}
                  headerText={[headerText[1]]}
                  testID={getLocator('moduleJ_header_text_1')}
                  renderComponentInNewLine
                  useStyle
                  headerStyle={[styleOverrides.subTitle]}
                />
              )}
            </SecondHeaderContainer>
          </Wrapper>

          {promoBanner && (
            <PromoContainer layout={layout}>
              <PromoBanner
                testID={getLocator('moduleJ_promobanner_text')}
                promoBanner={promoBanner}
                navigation={navigation}
                promoStyle={styleOverrides.promo}
              />
            </PromoContainer>
          )}
        </MessageContainer>
        <View style={productTabStyle}>
          <StyledProductTabList
            onProductTabChange={this.onProductTabChange}
            tabItems={divTabs}
            navigation={navigation}
            testID={getLocator('moduleJ_cta_link')}
          />

          {this.renderImageContainer()}

          {this.renderCarousel()}

          {selectedSingleCTAButton ? (
            <ButtonContainer>
              <Button
                width="225px"
                text={selectedSingleCTAButton.text}
                url={selectedSingleCTAButton.url}
                navigation={navigation}
                testID={getLocator('moduleJ_cta_btn')}
              />
            </ButtonContainer>
          ) : null}
        </View>
      </Container>
    );
    return isAndroid() ? (
      <InViewPort
        onChange={isVisible => this.checkVisible(isVisible)}
        delay={300}
        navigation={navigation}
      >
        {renderMainView()}
      </InViewPort>
    ) : (
      renderMainView()
    );
  }
}

ModuleJ.defaultProps = {
  bgColor: '',
  promoBanner: [],
  moduleClassName: '',
  icidParam: '',
  borderWidth: 0,
};

ModuleJ.propTypes = {
  bgColor: PropTypes.string,
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  productTabList: PropTypes.oneOfType(
    PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          uniqueId: PropTypes.string.isRequired,
          imageUrl: PropTypes.array.isRequired,
          seo_token: PropTypes.string,
        })
      )
    )
  ).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  layout: PropTypes.string.isRequired,
  mediaLinkedList: PropTypes.arrayOf(
    PropTypes.shape({
      image: PropTypes.object,
      link: PropTypes.object,
    })
  ).isRequired,
  divTabs: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.object,
      category: PropTypes.object,
      singleCTAButton: PropTypes.object,
    })
  ).isRequired,
  moduleClassName: PropTypes.string,
  icidParam: PropTypes.string,
  borderWidth: PropTypes.number,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleJ;
export { ModuleJ as ModuleJVanilla };
