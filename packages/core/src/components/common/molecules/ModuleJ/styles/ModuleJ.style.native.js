// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

import { DamImage } from '../../../atoms';
import ProductTabList from '../../../organisms/ProductTabList';

export const Container = styled.View`
  width: 100%;
`;

export const ImageSlidesWrapper = styled.View`
  height: 142px;
`;

export const ImageSlideWrapper = styled.View`
  flex-direction: row;
`;

export const StyledImage = styled(DamImage)`
  /* stylelint-disable-next-line */
  resize-mode: contain;
`;

export const ImageItemWrapper = styled.View`
  flex-direction: row;
  margin: ${props =>
    props.isFullMargin
      ? `${props.theme.spacing.ELEM_SPACING.MED}`
      : `${props.theme.spacing.ELEM_SPACING.MED} ${props.theme.spacing.ELEM_SPACING.XS}`};
`;

export const ButtonContainer = styled.View`
  align-items: center;
`;

export const Wrapper = styled.View`
  width: 100%;
  align-items: center;
  justify-content: center;
`;

export const PromoContainer = styled.View`
  ${props =>
    props.layout === 'alt'
      ? `margin-top: ${props.theme.spacing.ELEM_SPACING.SM}; `
      : `margin-top: ${props.theme.spacing.ELEM_SPACING.XXS};`};
`;

export const HeaderContainer = styled.View`
  align-items: center;

  ${props =>
    props.layout === 'alt'
      ? ` `
      : `background:white;
 margin-left:30px;
 margin-right:30px;
 padding: 0 12px;
 width:100%;
 `};
`;

export const SecondHeaderContainer = styled.View`
  ${props =>
    props.layout === 'alt' ? `margin-bottom: ${props.theme.spacing.ELEM_SPACING.SM};` : ''};
`;

export const Border = styled.View`
  width: 100%;
  height: 1px;
  top: 8px;
  position: absolute;
  ${props =>
    props.layout === 'alt' ? `` : `height: 3px;background-color:${props.borderColor || '#a87db2'}`};
`;

export const ImageContainer = styled.View`
  margin-top: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
  ${props => (props.layout === 'alt' ? `display:none ` : ``)};
`;

export const MessageContainer = styled.View`
  align-items: center;
  justify-content: center;
  width: 100%;
  background: ${props =>
    props.layout === 'alt' && props.bgColor ? props.bgColor : props.theme.colorPalette.white};
  ${props =>
    props.brand !== 'tcp'
      ? `
    padding-top: ${props.theme.spacing.LAYOUT_SPACING.SM};
    ${props.theme.spacing.LAYOUT_SPACING.SM};
    padding-bottom: ${
      props.layout === 'alt'
        ? props.theme.spacing.LAYOUT_SPACING.SM
        : props.theme.spacing.ELEM_SPACING.XXS
    };
  `
      : ``}
`;

export const StyledProductTabList = styled(ProductTabList)`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export default {
  Container,
  ImageItemWrapper,
  ButtonContainer,
  ImageSlidesWrapper,
  ImageSlideWrapper,
  StyledImage,
  PromoContainer,
  ImageContainer,
  MessageContainer,
  StyledProductTabList,
  Border,
  Wrapper,
};
