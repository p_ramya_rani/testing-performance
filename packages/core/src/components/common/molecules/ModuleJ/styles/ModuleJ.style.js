// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';

import { Skeleton } from '../../../atoms';

export default css`
  .smooth-scroll-list {
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
    width: 100%;
    -ms-overflow-style: none;
  }
  .smooth-scroll-list::-webkit-scrollbar {
    display: none;
  }
  .smooth-scroll-list-item {
    display: inline-block;
    vertical-align: top;
    margin-right: 16px;
    white-space: normal;
    &:last-child {
      margin-right: 0;
    }
  }
  .topview {
    position: relative;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex-direction: column;
    }
  }

  &.layout-default .topview {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex-direction: column-reverse;
    }
  }

  .promo-alt {
    background-color: ${(props) =>
      props.bgColor ? props.bgColor : props.theme.colorPalette.white};
    margin-bottom: 15px;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XL} 0;

    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: 14px;
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL} 0;
    }
  }

  &.brand-tcp .promo-alt {
    padding: 0;
  }

  .eyebrow a:first-child h2 {
    font-size: 20px;
    font-weight: normal;
  }

  .topbar {
    border-top: 3px solid ${(props) => props.theme.colorPalette.gray['500']};
    position: absolute;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};

    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
  }

  &.brand-tcp .topbar {
    border-top: 3px solid #a87db2;
  }

  .promo-header-wrapper {
    ${(props) =>
      !props.promoBanner
        ? `
      margin-bottom: ${props.theme.spacing.ELEM_SPACING.LRG};

      @media ${props.theme.mediaQuery.medium} {
        margin-bottom: 92px;
      }

      @media ${props.theme.mediaQuery.large} {
        margin-bottom: 130px;
      }
    `
        : ``};
  }

  .promo-header a:first-child > h2 {
    padding: 0 12px;
    ${(props) =>
      props.layout === 'default' ? `background-color: ${props.theme.colorPalette.white};` : ''}
    display: ${(props) => (props.layout !== 'alt' ? 'inline' : 'flow-root')};
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 0 20px;
    }
  }

  .promo {
    position: relative;
    text-align: center;
  }

  &.brand-tcp .promo {
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-right: 32px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: -38px;
    }
    @media ${(props) => props.theme.mediaQuery.xlarge} {
      margin-right: -18px;
    }
  }

  .link-text {
    margin-bottom: 0;
  }

  .promoBanner {
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM} auto;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin: 0;
    }
  }

  &.brand-tcp .promoBanner {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin: 0 auto;
    }
  }

  .promo-header-wrapper:first-child {
    padding: 0 0;
    display: inline-block;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-top: -10px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: -6px;
    }
  }

  &.layout-alt .promoBanner {
    margin: 0 auto 0;
    width: auto;
  }

  .image-link {
    display: block;
    align-items: center;
    width: 89px;
    height: 110px;

    img {
      width: 100%;
    }

    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 146px;
      height: 180px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 142px;
      height: 175px;
    }

    @media ${(props) => props.theme.mediaQuery.xlarge} {
      width: 175px;
      height: 217px;
    }
  }

  .promo-image-left {
    margin-top: 30px;
  }

  &.brand-tcp .promo-image-left {
    img {
      width: 100%;
    }

    @media ${(props) => props.theme.mediaQuery.medium} {
      a {
        width: 177px;
      }
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      a {
        width: 381px;
      }
      margin-top: 54px;
    }
  }

  .promo-image-right {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};

    @media ${(props) => props.theme.mediaQuery.medium} {
      text-align: right;
      margin-top: 30px;
    }
  }

  &.brand-tcp .promo-image-right {
    img {
      width: 100%;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      text-align: right;
      a {
        width: 177px;
      }
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      a {
        width: 381px;
      }
      margin-top: 54px;
    }
  }

  .promo-text:nth-child(2) {
    line-height: 64px;
    margin-top: -4px;
  }

  &.layout-default .promo-text:nth-child(2) {
    line-height: 0.9;
  }

  &.layout-alt .promo-text:nth-child(2) {
    line-height: 48px;
    margin-top: 6px;
  }

  .promo-img {
    width: 100%;

    @media ${(props) => props.theme.mediaQuery.medium} {
      width: auto;
    }
  }

  .stacked-cta-wrapper-class {
    padding-top: 16px;
    padding-right: 20px;
    padding-bottom: 16px;
    padding-left: 20px;
  }

  .moduleJ__carousel-wrapper {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
      overflow: hidden;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 100%;
      overflow: hidden;
    }

    .slick-list {
      margin-right: -22%;

      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-right: -15%;
      }

      @media ${(props) => props.theme.mediaQuery.large} {
        margin-right: auto;
      }
    }
  }

  &.layout-alt .moduleJ__carousel-wrapper {
    margin-top: 0;
  }

  .cta-btn {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  .product-tab-list {
    margin-top: 27px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: 14px;
    }
  }

  &.brand-tcp .product-tab-list {
    margin-top: 14px;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 35px;
    }
  }

  .product-image {
    margin-top: 16px;
    margin-bottom: 16px;
    width: calc(100% - 14px);
    margin-right: 0;
    @media ${(props) => props.theme.mediaQuery.large} {
      width: calc(100% - 26px);
      margin-right: 13px;
    }
  }

  &.brand-tcp.page-home .medium_text_regular,
  &.brand-tcp.page-home .fixed_medium_text_black {
    font-family: TofinoWide;
  }
  &.brand-tcp.page-home .promo-alt {
    h2 {
      line-height: 24px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        line-height: normal;
      }
    }
  }

  &.brand-tcp.page-home {
    .medium_text_regular {
      font-family: TofinoWide;
      font-size: 20px;
      font-weight: 500;
      font-stretch: normal;
      line-height: 1.2;
      letter-spacing: normal;

      @media ${(props) => props.theme.mediaQuery.medium} {
        font-size: 38px;
        line-height: 1;
      }
    }
    .promoBanner .medium_text_regular {
      font-weight: 500;
      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-bottom: 8px;
      }
    }

    .promoBanner .fixed_medium_text_black {
      font-size: 62px;
      font-weight: 900;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.36;
      letter-spacing: normal;
      margin-top: 0;
      @media ${(props) => props.theme.mediaQuery.large} {
        margin-top: 8px;
        font-size: 88px;
      }
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        font-size: 64px;
        line-height: 1;
        margin-top: 8px;
      }
    }
    .link-text .small_text_normal {
      font-family: Nunito;
      font-size: 14px;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.36;
      letter-spacing: normal;
      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: 20px;
      }
    }

    .link-text .style1 {
      font-family: TofinoCond;
      font-weight: 700;
      font-size: 48px;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.3;
      letter-spacing: normal;
      color: ${(props) => props.theme.colors.PRIMARY.DARK};

      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: 88px;
      }
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        font-size: 62px;
      }
    }
    .promo-text.medium_text_regular {
      font-family: TofinoWide;
      font-size: 38px;
      font-weight: 500;
      font-stretch: normal;
      line-height: 0.63;
      letter-spacing: normal;

      @media ${(props) => props.theme.mediaQuery.smallMax} {
        font-size: 20px;
        line-height: 1.2;
        margin-bottom: -6px;
      }
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        font-size: 20px;
      }
    }
  }
`;
export const StyledSkeleton = styled(Skeleton)`
  margin-top: 5px;
  @media ${(props) => props.theme.mediaQuery.large} {
    .left-carousel {
      left: -25px;
    }
    .right-carousel {
      right: -25px;
    }
  }
  .skeleton-col {
    height: 110px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      &:nth-child(n + 4) {
        display: none;
      }
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: 180px;
      &:nth-child(n + 5) {
        display: none;
      }
    }
    @media ${(props) => props.theme.mediaQuery.largeOnly} {
      height: 175px;
    }

    @media ${(props) => props.theme.mediaQuery.xlarge} {
      height: 217px;
    }
  }
`;
