// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import NotificationPrompt from '../views/NotificationPrompt.view.native';

describe('NotificationPromptVanilla native component', () => {
  let component;
  const props = {
    labels: {},
  };
  beforeEach(() => {
    component = shallow(<NotificationPrompt {...props} />);
  });

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot();
  });
});
