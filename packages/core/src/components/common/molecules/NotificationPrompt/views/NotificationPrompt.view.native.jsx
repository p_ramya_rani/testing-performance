// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { getScreenWidth, getScreenHeight, getAppVersion } from '@tcp/core/src/utils/utils.app';
import PropTypes from 'prop-types';
import { Linking, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';
import { checkNotifications, RESULTS, requestNotifications } from 'react-native-permissions';
import ButtonRedesign from '@tcp/core/src/components/common/atoms/ButtonRedesign';

import {
  compareVersion,
  getValueFromAsyncStorage,
  setValueInAsyncStorage,
  isAndroid,
  isIOS,
} from '@tcp/core/src/utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import ModalNative from '../../Modal/view/Modal.native';
import {
  Wrapper,
  Container,
  ButtonWrapper,
  MessageContainer,
  TextContainer,
  ShadowContainer,
  HeadingContainer,
} from '../styles/NotificationPrompt.style.native';

/**
 * Module height and width.
 */
const PROPMT_WIDTH = getScreenWidth();
const HEIGHT = getScreenHeight();

const pushNotificationLottie = require('../../../../../../../mobileapp/src/assets/json/lottiePushNotification.json');
const pushNotificationLottieRedesign = require('../../../../../../../mobileapp/src/assets/json/lottiePushNotificationRedesign.json');

class NotificationPrompt extends React.PureComponent {
  constructor() {
    super();
    const { height: windowHeight } = Dimensions.get('window');
    const lottieHeight = 0.5 * windowHeight;
    const lottieStyleObj = { height: lottieHeight, alignItems: 'center' };
    this.state = {
      notificationModal: false,
      lottieStyleObj,
    };
  }

  componentDidMount() {
    const { launchTime, onModalActionComplete } = this.props;
    if (launchTime) {
      getValueFromAsyncStorage('firstTimeNotificationPrompt').then((result) => {
        if (result === null) {
          setValueInAsyncStorage('firstTimeNotificationPrompt', 'true');
          // check for permission
          checkNotifications().then(({ status }) => {
            if (status !== RESULTS.GRANTED) {
              this.setState({ notificationModal: true });
            } else {
              // for granted - android by default
              setValueInAsyncStorage('maybeLaterNotification', 'false').then(() => {
                onModalActionComplete();
              });
            }
          });
        } else {
          onModalActionComplete();
        }
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { shouldUpdateLatestAppVersion: prevShouldUpdateLatestAppVersion } = prevProps;
    const currentVersion = getAppVersion();
    const { shouldUpdateLatestAppVersion } = this.props;
    if (
      shouldUpdateLatestAppVersion &&
      prevShouldUpdateLatestAppVersion !== shouldUpdateLatestAppVersion
    ) {
      checkNotifications().then(({ status }) => {
        if (status !== RESULTS.GRANTED) {
          AsyncStorage.getItem('appVersion').then((result) => {
            if (compareVersion(result, currentVersion)) {
              AsyncStorage.setItem('appVersion', currentVersion);
              this.setState({ notificationModal: true });
            }
          });
        }
      });
    }
  }

  /**
   * @toggleModal : To manage the modal state .
   */
  toggleModal = () => {
    const { notificationModal } = this.state;
    const { onModalActionComplete } = this.props;
    this.setState({
      notificationModal: !notificationModal,
    });
    if (onModalActionComplete) {
      onModalActionComplete();
    }
  };

  maybeLater = () => {
    setValueInAsyncStorage('maybeLaterNotification', 'true').then(() => {
      this.toggleModal();
    });
  };

  openSettings = () => {
    checkNotifications().then(({ status }) => {
      if (status === RESULTS.BLOCKED || isAndroid()) {
        Linking.openSettings();
        this.toggleModal();
      } else if (isIOS()) {
        const { notificationModal } = this.state;
        const { onModalActionComplete } = this.props;
        this.setState({
          notificationModal: !notificationModal,
        });
        requestNotifications(['alert', 'sound', 'badge', 'lockScreen', 'notificationCenter']).then(
          () => {
            if (onModalActionComplete) {
              setValueInAsyncStorage('maybeLaterNotification', 'false').then(() => {
                onModalActionComplete();
              });
            }
          }
        );
      }
    });
  };

  getHeading = (showPNSRedesign, labels) => {
    const heading =
      getLabelValue(labels, 'lbl_notificationPrompt_heading', 'notification', 'global') +
      getLabelValue(labels, 'lbl_notificationPrompt_heading_2', 'notification', 'global').split(
        '...'
      )[0];
    return showPNSRedesign ? (
      <BodyCopyWithSpacing
        text={heading}
        color={showPNSRedesign ? 'white' : 'blue.900'}
        textAlign="center"
        fontFamily="primary"
        fontWeight="medium"
        fontSize="fs30"
        margin="0 10px 15px 10px"
      />
    ) : (
      <TextContainer>
        <BodyCopyWithSpacing
          text={getLabelValue(labels, 'lbl_notificationPrompt_heading', 'notification', 'global')}
          color={showPNSRedesign ? 'white' : 'blue.900'}
          textAlign="center"
          fontFamily="primary"
          fontWeight="medium"
          fontSize="fs30"
        />
        <BodyCopyWithSpacing
          text={getLabelValue(labels, 'lbl_notificationPrompt_heading_2', 'notification', 'global')}
          color={showPNSRedesign ? 'white' : 'blue.900'}
          textAlign="center"
          fontFamily="primary"
          fontWeight="medium"
          fontSize="fs30"
          spacingStyles="margin-bottom-SM"
        />
      </TextContainer>
    );
  };

  render() {
    const { labels, showPNSRedesign } = this.props;
    const { notificationModal, lottieStyleObj } = this.state;
    return labels && Object.keys(labels).length ? (
      <ModalNative isOpen={notificationModal} onRequestClose={this.maybeLater} customTransparent>
        <ShadowContainer height={HEIGHT}>
          <Container>
            <Wrapper width={PROPMT_WIDTH} height={HEIGHT} showPNSRedesign={showPNSRedesign}>
              <LottieView
                source={showPNSRedesign ? pushNotificationLottieRedesign : pushNotificationLottie}
                autoPlay
                style={lottieStyleObj}
                loop={false}
              />
              <MessageContainer>
                {this.getHeading(showPNSRedesign, labels)}
                <HeadingContainer>
                  <BodyCopyWithSpacing
                    text={getLabelValue(
                      labels,
                      'lbl_notificationPrompt_first',
                      'notification',
                      'global'
                    )}
                    color={showPNSRedesign ? 'white' : 'blue.900'}
                    margin="0px -20px"
                    textAlign="center"
                    fontWeight="regular"
                    fontFamily="secondary"
                    fontSize="fs16"
                  />
                </HeadingContainer>
                <ButtonRedesign
                  text={getLabelValue(
                    labels,
                    'lbl_notificationPrompt_button',
                    'notification',
                    'global'
                  )}
                  fill="BLUE"
                  borderRadius="16px"
                  fontFamily="secondary"
                  fontWeight="bold"
                  fontSize="fs16"
                  showShadow
                  width="262px"
                  height="46px"
                  paddings="5px"
                  onPress={() => this.openSettings()}
                />
                <ButtonWrapper>
                  <ButtonRedesign
                    text={getLabelValue(
                      labels,
                      'lbl_notificationPrompt_later',
                      'notification',
                      'global'
                    )}
                    fill="WHITE"
                    fontFamily="secondary"
                    fontWeight="bold"
                    borderRadius="16px"
                    showShadow
                    fontSize="fs16"
                    width="262px"
                    height="46px"
                    paddings="6px"
                    onPress={this.maybeLater}
                  />
                </ButtonWrapper>
              </MessageContainer>
            </Wrapper>
          </Container>
        </ShadowContainer>
      </ModalNative>
    ) : null;
  }
}

NotificationPrompt.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  shouldUpdateLatestAppVersion: PropTypes.bool.isRequired,
  launchTime: PropTypes.bool.isRequired,
  onModalActionComplete: PropTypes.func,
};

NotificationPrompt.defaultProps = {
  onModalActionComplete: () => {},
};

export default NotificationPrompt;
