// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { Image } from '../../../atoms';

export const Wrapper = styled.View`
  align-items: center;
  background-color: ${(props) =>
    props.showPNSRedesign ? props.theme.colors.PRIMARY.LIGHTBLUE : props.theme.colorPalette.white};
  height: ${(props) => props.height};
  width: ${(props) => props.width};
  padding: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
  justify-content: flex-end;
`;

export const Container = styled.View`
  align-items: center;
  justify-content: center;
`;

export const StyledImage = styled(Image)`
  ${(props) => (props.marginTop ? `margin-top: ${props.marginTop}` : ``)};
`;

export const Touchable = styled.TouchableOpacity`
  right: 0;
  padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
  padding-right: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
  position: absolute;
`;

export const ButtonWrapper = styled.View`
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
  margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
`;

export const AnchorWrapper = styled.View`
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
  margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
`;

export const MessageContainer = styled.View`
  align-items: center;
  justify-content: flex-end;
`;

export const TextContainer = styled.View`
  align-items: center;
  margin: 0px 68px;
`;

export const HeadingContainer = styled.View`
  align-items: center;
  margin: 0px 34px 68px;
`;

export const ShadowContainer = styled.View`
  align-items: center;
  justify-content: center;
  height: ${(props) => props.height}px;
  background-color: rgba(0, 0, 0, 0.6);
`;

export const PointsWrapper = styled.View`
  flex-direction: column;
  justify-content: flex-start;
`;

export default {
  Wrapper,
  Container,
  StyledImage,
  Touchable,
  ButtonWrapper,
  MessageContainer,
  ShadowContainer,
  PointsWrapper,
  AnchorWrapper,
};
