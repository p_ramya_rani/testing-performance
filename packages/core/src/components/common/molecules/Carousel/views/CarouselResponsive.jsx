// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import PropTypes from 'prop-types';
import { Image } from '../../../atoms';
import { getImageFilePath, getIconPath } from '../../../../../utils';
import CarouselStyle from '../CarouselResponsive.style';
import withStyles from '../../../hoc/withStyles';
import errorBoundary from '../../../hoc/withErrorBoundary';

/**
 * @function Carousel component that creates carousel using
 * third party 'react-slick'
 */
class CarouselResponsive extends React.Component {
  constructor(props) {
    super(props);
    this.getPlayButton = this.getPlayButton.bind(this);
    this.togglePlay = this.togglePlay.bind(this);
    this.state = {
      autoplay: this.props.carouselConfig.autoplay,
      uniqueId: Math.random() * 1000,
      // uniqueId is generated when a new carousel is instantiated and is passed to slider to keep them unique
      // key could also be passed from the module from the component(like in moduleH) which will overwrite this key
    };
  }

  getPlayIcon = () => {
    const { isModule } = this.props;
    return isModule ? `${getImageFilePath()}/play-accessible.svg` : getIconPath('icon-pause');
  };

  getPauseIcon = () => {
    const { isModule } = this.props;
    return isModule ? `${getImageFilePath()}/pause-accessible.svg` : getIconPath('icon-play');
  };

  /**
   * @function getPlayComponent function gets DOM reference of slider component.
   * @param {[Object]} element [Event object of click].
   * @return {node} function returns slider element.
   */
  getPlayButton(wrapperConfig) {
    const { autoplay } = this.state;
    const { playIconButtonLabel, pauseIconButtonLabel, dataLocatorPause, dataLocatorPlay } =
      wrapperConfig;

    const buttonClass = 'tcp_carousel__play_pause_button';
    return autoplay ? (
      <button
        className={buttonClass}
        data-locator={dataLocatorPause}
        onClick={this.togglePlay}
        aria-label={pauseIconButtonLabel}
      >
        <Image
          className="tcp_carousel__play_pause_button_icon"
          aria-hidden="true"
          src={this.getPauseIcon()}
        />
      </button>
    ) : (
      <button
        className={buttonClass}
        data-locator={dataLocatorPlay}
        onClick={this.togglePlay}
        aria-label={playIconButtonLabel}
      >
        <Image
          className="tcp_carousel__play_pause_button_icon"
          aria-hidden="true"
          src={this.getPlayIcon()}
        />
      </button>
    );
  }

  /**
   * @function updateState function updates component state
   * after each click on play pause button.
   */
  togglePlay() {
    const { autoplay } = this.state;
    this.setState({ autoplay: !autoplay });
  }

  /**
   * @function render  Used to render the JSX of the component
   * @param {object} options Customized caroused configs from parent wrapper
   * @param {node} children address object
   * @param {object} carouselConfig Carousel wrapper config to enable customization
   * of functionalities like play pause, change carousel theme etc.
   */
  render() {
    const { options, children, carouselConfig, className } = this.props;
    const { autoplay, uniqueId } = this.state;

    const settings = {
      autoPlay: autoplay,
      showThumbs: false,
      infiniteLoop: true,
      showStatus: false,
      emulateTouch: true,
    };
    const fadeProp = options?.fade
      ? {
          animationHandler: 'fade',
          interval: options?.autoplaySpeed,
          transitionTime: options?.speed,
          dynamicHeight: true,
        }
      : {};
    return (
      <div
        className={`${className} tcp_carousel_wrapper`}
        carouselConfig={carouselConfig}
        data-locator={carouselConfig.dataLocatorCarousel}
      >
        <Carousel className="tcp_carousel" key={uniqueId} {...settings} {...fadeProp}>
          {!children ? null : children}
        </Carousel>
        {carouselConfig.autoplay && options.autoplay && this.getPlayButton(carouselConfig)}
      </div>
    );
  }
}

CarouselResponsive.defaultProps = {
  isModule: false,
  isPlpQV: false,
};

CarouselResponsive.propTypes = {
  options: PropTypes.shape({}).isRequired,
  nextProps: PropTypes.shape({}).isRequired,
  children: PropTypes.shape({}).isRequired,
  carouselConfig: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  playIconButtonLabel: PropTypes.string.isRequired,
  pauseIconButtonLabel: PropTypes.string.isRequired,
  sliderImageIndex: PropTypes.number.isRequired,
  labels: PropTypes.shape({}).isRequired,
  isModule: PropTypes.bool,
  isPlpQV: PropTypes.bool,
};

export default errorBoundary(withStyles(CarouselResponsive, CarouselStyle));
export { CarouselResponsive as CarouselResponsiveVanilla };
