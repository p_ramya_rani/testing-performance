/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { View } from 'react-native';
import { withTheme } from 'styled-components/native';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { getLocator } from '../../../../../utils';
import { Image } from '../../../atoms';
import config from '../Carousel.config.native';
import CustomIcon from '../../../atoms/Icon';
import { ICON_NAME } from '../../../atoms/Icon/Icon.constants';
import AccessibilityRoles from '../../../../../constants/AccessibilityRoles.constant';

import {
  ControlsWrapper,
  PlayPauseButtonView,
  Touchable,
  TouchableView,
  Icon,
  Container,
  PaginationWrapper,
  ControlsWrapperLeft,
  ControlsWrapperRight,
  AccessibleDescriptionView,
} from '../Carousel.native.style';

/**
 * Import play pause image icons.
 * Note: React native imports images using require.
 */
const playIcon = require('../../../../../../../mobileapp/src/assets/images/play.png');
const pauseIcon = require('../../../../../../../mobileapp/src/assets/images/pause.png');
const modulePlayIcon = require('../../../../../../../mobileapp/src/assets/images/playPause/play-accessible-MB.png');
const modulePauseIcon = require('../../../../../../../mobileapp/src/assets/images/playPause/stop-accessible-MB.png');

// /**
//  * Next & Prev icons listing.
//  */
const prevIcon = require('../../../../../../../mobileapp/src/assets/images/carrot-large-right.png');
const nextIcon = require('../../../../../../../mobileapp/src/assets/images/carrot-large-left.png');
const prevIconDark = require('../../../../../../../mobileapp/src/assets/images/carrot-small-rights.png');
const nextIconDark = require('../../../../../../../mobileapp/src/assets/images/carrot-small-left.png');

/**
 * Default settings for Carousel.
 */
const defaults = { ...config.CAROUSEL_APP_DEFAULTS };

/**
 * Style for play pause icons.
 */
const { playIconHeight, playIconWidth, modulePlayIconHeight, modulePlayIconWidth } = {
  ...config.CAROUSEL_APP_CONFIG,
};

/**
 * @function Carousel component that creates carousel using
 * third party library 'react-native-snap-carousel'
 */
class SnapCarousel extends React.PureComponent {
  constructor(props) {
    super(props);
    const { autoplay } = props;
    this.state = {
      autoplay,
      activeSlide: 0,
    };
    this.getPlayButton = this.getPlayButton.bind(this);
    this.getOverlapComponent = this.getOverlapComponent.bind(this);
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.carouselRef = this.updateRef.bind(this, 'carousel');
  }

  getPagination() {
    const { activeSlide } = this.state;
    const {
      data,
      theme: { colorPalette },
      paginationProps,
    } = this.props;

    const {
      containerStyle: containerStyleOverride,
      dotContainerStyle: dotContainerStyleOverride,
      dotStyle: dotStyleOverride,
      inactiveDotStyle: inactiveDotStyleOverride,
    } = paginationProps;

    /* eslint-disable react-native/no-inline-styles */
    return (
      <Pagination
        dotsLength={data.length}
        activeDotIndex={activeSlide}
        containerStyle={{
          paddingVertical: 22,
          paddingHorizontal: 10,
          ...containerStyleOverride,
        }}
        dotContainerStyle={{ marginHorizontal: 4, ...dotContainerStyleOverride }}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 0,
          padding: 0,
          borderColor: colorPalette.gray[700],
          borderWidth: 1,
          backgroundColor: colorPalette.white,
          ...dotStyleOverride,
        }}
        inactiveDotStyle={{
          backgroundColor: colorPalette.gray[700],
          width: 6,
          height: 6,
          ...inactiveDotStyleOverride,
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={1}
      />
    );
    /* eslint-enable react-native/no-inline-styles */
  }

  /**
   * @function getPlayComponent function gets DOM reference of slider component.
   * @param {[Object]} element [Event object of click].
   * @return {node} function returns slider element.
   */
  getPlayButton(carouselConfig) {
    const {
      labels: { accessibility },
      isModule,
    } = this.props;
    const { autoplay } = this.state;

    return autoplay ? (
      <Touchable
        accessibilityRole={AccessibilityRoles.Button}
        onPress={this.pause}
        isModule={isModule}
      >
        <Image
          source={isModule ? modulePauseIcon : pauseIcon}
          alt={accessibility.playIconButton}
          height={isModule ? modulePlayIconHeight : playIconHeight}
          width={isModule ? modulePlayIconWidth : playIconWidth}
          testID={getLocator(carouselConfig.dataLocatorPause)}
        />
      </Touchable>
    ) : (
      <Touchable
        accessibilityRole={AccessibilityRoles.Button}
        onPress={this.play}
        isModule={isModule}
      >
        <Image
          source={isModule ? modulePlayIcon : playIcon}
          alt={accessibility.pauseIconButton}
          height={isModule ? modulePlayIconHeight : playIconHeight}
          width={isModule ? modulePlayIconWidth : playIconWidth}
          testID={getLocator(carouselConfig.dataLocatorPlay)}
        />
      </Touchable>
    );
  }

  /**
   * @function getOverlapComponent This function return the Play Or Pause Button with pagination.
   * @Component is configurable : leftBottom , rightBottom  and centerBottom .
   */

  getOverlapComponent(carouselConfig, buttonPosition) {
    const { isModule } = this.props;
    let overlapComponent;
    if (buttonPosition === 'right') {
      overlapComponent = (
        <View>
          <ControlsWrapperRight>
            {carouselConfig.autoplay && (
              <PlayPauseButtonView>{this.getPlayButton(carouselConfig)}</PlayPauseButtonView>
            )}
            {this.getPagination()}
          </ControlsWrapperRight>
        </View>
      );
    } else if (buttonPosition === 'left') {
      overlapComponent = (
        <View>
          <ControlsWrapperLeft>
            {carouselConfig.autoplay && (
              <PlayPauseButtonView>{this.getPlayButton(carouselConfig)}</PlayPauseButtonView>
            )}
            {this.getPagination()}
          </ControlsWrapperLeft>
        </View>
      );
    } else {
      overlapComponent = (
        <View>
          <ControlsWrapper isModule={isModule}>
            {carouselConfig.autoplay && (
              <PlayPauseButtonView>{this.getPlayButton(carouselConfig)}</PlayPauseButtonView>
            )}
            {this.getPagination()}
          </ControlsWrapper>
        </View>
      );
    }

    return overlapComponent;
  }

  /**
   * @function getBottomView This function return the Play Or Pause Button.
   */
  getBottomView(carouselConfig, showDots, iconBottomMargin) {
    return (
      <PaginationWrapper iconBottomMargin={iconBottomMargin}>
        {carouselConfig.autoplay && (
          <PlayPauseButtonView>{this.getPlayButton(carouselConfig)}</PlayPauseButtonView>
        )}
        {showDots ? this.getPagination() : null}
      </PaginationWrapper>
    );
  }

  /* Return the Prev and Next Icon */
  getNavIcons = (darkArrow, activeSlide, settings, data) => {
    let iconTypePre = darkArrow ? prevIconDark : prevIcon;
    let iconTypeNext = darkArrow ? nextIconDark : nextIcon;

    if (settings.loop === false && darkArrow && activeSlide < 1) {
      iconTypeNext = nextIcon;
    }
    if (settings.loop === false && darkArrow && data.length - 1 <= activeSlide) {
      iconTypePre = prevIcon;
    }

    return { iconTypeNext, iconTypePre };
  };

  /**
   * To manage the direction of the carousel
   */

  manageSlide = (direction) => {
    if (direction === 'next') {
      return this.carousel.snapToPrev();
    }
    return this.carousel.snapToNext();
  };

  /**
   * Called on slide item
   */

  onSnapToItemHandler = (index) => {
    const { onSnapToItem } = this.props;
    this.setState({ activeSlide: index });
    onSnapToItem(index);
  };

  getSliderWidth = (width) => {
    const { sliderWidth } = this.props;

    return sliderWidth || width;
  };

  getItemWidth = (width) => {
    const { itemWidth } = this.props;

    return itemWidth || width;
  };

  /**
   * @function This function render custom icons
   * also update component state.
   */

  renderIcon = (iconName) => {
    return <CustomIcon name={iconName} size="fs19" color="gray.900" />;
  };

  /**
   * @function This function check which icon need to be draw
   */
  getIcon = (useLeftArrowIcon, useRightArrowIcon, imageSource) => {
    if (useLeftArrowIcon) {
      return this.renderIcon(ICON_NAME.chevronLeft);
    }
    if (useRightArrowIcon) {
      return this.renderIcon(ICON_NAME.chevronRight);
    }

    if (imageSource) {
      return <Icon source={imageSource} />;
    }

    return null;
  };

  /**
   * @function headerRightArrow function to get style for right arrow
   * for headerpromo.
   */
  headerRightArrow = () => {
    const { isHeaderPromo } = this.props;
    return isHeaderPromo ? { position: 'absolute', left: '5%', top: '35%', zIndex: 1 } : {};
  };

  /**
   * @function headerLefttArrow function to get style for right arrow
   * for headerpromo
   */
  headerLefttArrow = () => {
    const { isHeaderPromo } = this.props;
    return isHeaderPromo ? { position: 'absolute', right: '5%', top: '35%' } : {};
  };

  /**
   * @function play function enable autoplay for carousel
   * also update component state.
   */
  play() {
    this.carousel.startAutoplay();
    this.togglePlay();
  }

  /**
   * @function pause function stops/pause autoplay for carousel
   * also update component state.
   */
  pause() {
    this.carousel.stopAutoplay();
    this.togglePlay();
  }

  /**
   * @function togglePlay function updates component state
   * after each tap of play pause button.
   */
  togglePlay() {
    const { autoplay } = this.state;
    this.setState({ autoplay: !autoplay });
  }

  updateRef(ref, name) {
    this[ref] = name;
  }

  render() {
    const {
      carouselConfig,
      data,
      height,
      width,
      renderItem,
      slideStyle,
      variation,
      vertical,
      autoplayInterval,
      showDots,
      overlap,
      buttonPosition,
      darkArrow,
      options,
      hasParallaxImages,
      loop,
      activeSlideAlignment,
      iconBottomMargin,
      inactiveSlideOpacity,
      isUseLeftArrowIcon,
      isUseRightArrowIcon,
      labels,
      isUseScrollView,
      enableMomentum,
      enableSnap,
      setRef,
    } = this.props;
    if (!data) {
      return null;
    }

    const { autoplay, activeSlide } = this.state;
    const settings = { ...defaults, autoplay, loop, ...options };

    const { iconTypeNext, iconTypePre } = this.getNavIcons(darkArrow, activeSlide, settings, data);
    let carouselWidth = width - 64;
    if (hasParallaxImages) {
      carouselWidth = width - 80;
    }

    if (variation === 'show-arrow') {
      // reduce left and right arrow width from the total width to fix center alignment issue
      return (
        <View>
          <Container>
            <TouchableView
              style={{ ...this.headerRightArrow() }}
              accessibilityRole={AccessibilityRoles.Button}
              accessibilityLabel={labels.accessibility.ariaPrevious}
              testID={getLocator('global_promobanner_right_arrow')}
              onPress={() => this.manageSlide('next')}
            >
              {this.getIcon(isUseLeftArrowIcon, false, iconTypeNext)}
            </TouchableView>
            <Carousel
              data={data}
              onSnapToItem={this.onSnapToItemHandler}
              renderItem={renderItem}
              sliderWidth={this.getSliderWidth(carouselWidth)}
              itemWidth={this.getItemWidth(carouselWidth)}
              sliderHeight={height}
              itemHeight={height}
              slideStyle={slideStyle}
              autoplayInterval={autoplayInterval}
              ref={this.carouselRef}
              hasParallaxImages={hasParallaxImages}
              inactiveSlideOpacity={inactiveSlideOpacity}
              {...settings}
              activeSlideAlignment={activeSlideAlignment}
            />
            <TouchableView
              style={{ ...this.headerLefttArrow() }}
              accessibilityRole={AccessibilityRoles.Button}
              accessibilityLabel={labels.accessibility.ariaNext}
              testID={getLocator('global_promobanner_left_arrowRight')}
              onPress={() => this.manageSlide('prev')}
            >
              {this.getIcon(false, isUseRightArrowIcon, iconTypePre)}
            </TouchableView>
          </Container>
          {data.length > 1 && showDots ? this.getPagination() : null}
        </View>
      );
    }

    return (
      <View>
        <AccessibleDescriptionView
          accessible
          accessibilityLabel={labels.accessibility.carouselDescription}
          accessibilityRole={AccessibilityRoles.Text}
        />
        <Carousel
          ref={(c) => {
            this.carousel = c;
            if (setRef) {
              setRef(c);
            }
          }}
          onSnapToItem={(index) => this.onSnapToItemHandler(index)}
          data={data}
          renderItem={renderItem}
          sliderWidth={this.getSliderWidth(width)}
          itemWidth={
            hasParallaxImages ? this.getItemWidth(carouselWidth) : this.getItemWidth(width)
          }
          sliderHeight={height}
          itemHeight={height}
          slideStyle={slideStyle}
          vertical={vertical}
          autoplayInterval={autoplayInterval}
          hasParallaxImages={hasParallaxImages}
          activeSlideAlignment={activeSlideAlignment}
          inactiveSlideOpacity={inactiveSlideOpacity}
          useScrollView={isUseScrollView}
          enableMomentum={enableMomentum}
          enableSnap={enableSnap}
          {...settings}
        />

        {data.length > 1 && (
          <View>
            {showDots && overlap
              ? this.getOverlapComponent(carouselConfig, buttonPosition, iconBottomMargin)
              : this.getBottomView(carouselConfig, showDots, iconBottomMargin)}
          </View>
        )}
      </View>
    );
  }
}

SnapCarousel.defaultProps = {
  onSnapToItem: () => {},
  showDots: false,
  autoplay: true,
  hidePlayStopButton: false,
  overlap: false,
  darkArrow: false,
  hasParallaxImages: false,
  paginationProps: {},
  carouselConfig: {},
  data: [],
  renderItem: () => {},
  slideStyle: {},
  theme: {},
  variation: '',
  vertical: false,
  autoplayInterval: null,
  buttonPosition: '',
  width: null,
  height: null,
  options: {},
  loop: true,
  sliderWidth: 0,
  itemWidth: 0,
  activeSlideAlignment: 'center',
  iconBottomMargin: null,
  inactiveSlideOpacity: 0.7,
  isUseLeftArrowIcon: false,
  isUseRightArrowIcon: false,
  isUseScrollView: false,
  labels: {
    accessibility: {
      ariaPrevious: 'Previous',
      ariaNext: 'Next',
    },
  },
  isHeaderPromo: false,
  enableMomentum: false,
  enableSnap: true,
  isModule: false,
};

SnapCarousel.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  renderItem: PropTypes.func,
  width: PropTypes.number,
  height: PropTypes.number,
  slideStyle: PropTypes.shape({}),
  theme: PropTypes.shape({}),
  variation: PropTypes.oneOf(['', 'show-arrow']),
  vertical: PropTypes.bool,
  autoplayInterval: PropTypes.number,
  buttonPosition: PropTypes.oneOf(['', 'left', 'right']),
  carouselConfig: PropTypes.shape({}),
  onSnapToItem: PropTypes.func,
  showDots: PropTypes.bool,
  darkArrow: PropTypes.bool,
  overlap: PropTypes.bool,
  hidePlayStopButton: PropTypes.bool,
  autoplay: PropTypes.bool,
  paginationProps: PropTypes.shape({}),
  hasParallaxImages: PropTypes.bool,
  iconBottomMargin: PropTypes.string,
  options: PropTypes.shape({}),
  loop: PropTypes.bool,
  sliderWidth: PropTypes.number,
  itemWidth: PropTypes.number,
  activeSlideAlignment: PropTypes.string,
  inactiveSlideOpacity: PropTypes.number,
  isUseLeftArrowIcon: PropTypes.bool,
  isUseRightArrowIcon: PropTypes.bool,
  labels: PropTypes.shape({}),
  isUseScrollView: PropTypes.bool,
  isHeaderPromo: PropTypes.bool,
  enableMomentum: PropTypes.bool,
  enableSnap: PropTypes.bool,
  isModule: PropTypes.bool,
  setRef: PropTypes.func.isRequired,
};

export default withTheme(SnapCarousel);
export { SnapCarousel as SnapCarouselVanilla };
