// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import dynamic from 'next/dynamic';

import withLazyLoad from '@tcp/core/src/components/common/hoc/withLazyLoad';

const CarouselContainer = dynamic(() =>
  import(/* webpackChunkName: "carouselContainer" */ './Carousel.container')
);

export default withLazyLoad(CarouselContainer);
