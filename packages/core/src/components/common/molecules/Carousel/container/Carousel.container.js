// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { getLabelValue } from '@tcp/core/src/utils';
import CarouselView from '../views';

const mapStateToProps = state => {
  return {
    labels: {
      accessibility: {
        ariaNext: getLabelValue(state.Labels, 'nextIconButton', 'accessibility', 'global'),
        ariaPrevious: getLabelValue(state.Labels, 'previousButton', 'accessibility', 'global'),
        pauseIconButton: getLabelValue(state.Labels, 'pauseIconButton', 'accessibility', 'global'),
        playIconButton: getLabelValue(state.Labels, 'playIconButton', 'accessibility', 'global'),
        carouselDescription: getLabelValue(
          state.Labels,
          'carouselDescription',
          'accessibility',
          'global'
        ),
      },
    },
  };
};

export default connect(mapStateToProps)(CarouselView);
