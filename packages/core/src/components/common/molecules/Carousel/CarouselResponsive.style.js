// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '../../../../utils';

const darkArrow = getIconPath('icon-carrot-black-small');
const darkArrowLarge = getIconPath('icon-carrot-grey-large');
const lightArrow = getIconPath('icon-carrot-white');
const CarouselStyle = css`
  .carousel .control-arrow {
    background-color: #ffffff !important;
    top: 50%;
    margin-top: -13px;
    font-size: 18px;
    font-size: 0;
    line-height: 0;
    position: absolute;
    display: block;
    width: 38px;
    height: 30px;
    padding: 45px 14px;
    transform: translate(0, -50%);
    cursor: pointer;
    color: transparent;
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
    outline: none;
    background-size: auto auto;
    z-index: 1;
  }
  @media only screen and (max-width: 767px) {
    .control-next.control-arrow,
    .control-prev.control-arrow {
      background: url(${(props) =>
        props.carouselConfig.type === 'dark' ? lightArrow : darkArrow});
      background-repeat: no-repeat;
      background-position: center;
    }
  }
  @media only screen and (min-width: 768px) {
    .control-prev.control-arrow {
      background: url(${(props) =>
          props.carouselConfig.type === 'dark'
            ? lightArrow
            : props.carouselConfig.customArrowLeft || darkArrowLarge})
        no-repeat center center;
    }
    .control-arrow.control-next {
      background: url(${(props) =>
          props.carouselConfig.type !== 'dark'
            ? props.carouselConfig.customArrowRight || darkArrowLarge
            : lightArrow})
        no-repeat center center;
    }
  }
  .control-prev.control-arrow {
    left: 0;
    transform: rotateY(180deg) translate(0, -50%);
  }
  .control-next.control-arrow {
    right: 0;
  }
  .carousel {
    position: relative;
    width: 100%;
  }
  .carousel * {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }
  .carousel img {
    width: 100%;
    display: inline-block;
    pointer-events: none;
  }
  .carousel .carousel {
    position: relative;
  }
  .carousel .thumbs-wrapper {
    margin: 20px;
    overflow: hidden;
  }
  .carousel .thumbs {
    -webkit-transition: all 0.15s ease-in;
    -moz-transition: all 0.15s ease-in;
    -ms-transition: all 0.15s ease-in;
    -o-transition: all 0.15s ease-in;
    transition: all 0.15s ease-in;
    -webkit-transform: translate3d(0, 0, 0);
    -moz-transform: translate3d(0, 0, 0);
    -ms-transform: translate3d(0, 0, 0);
    -o-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    position: relative;
    list-style: none;
    white-space: nowrap;
  }
  .carousel .thumb {
    -webkit-transition: border 0.15s ease-in;
    -moz-transition: border 0.15s ease-in;
    -ms-transition: border 0.15s ease-in;
    -o-transition: border 0.15s ease-in;
    transition: border 0.15s ease-in;
    display: inline-block;
    margin-right: 6px;
    white-space: nowrap;
    overflow: hidden;
    border: 3px solid #fff;
    padding: 2px;
  }
  .carousel .thumb:focus {
    border: 3px solid #ccc;
    outline: none;
  }
  .carousel .thumb.selected,
  .carousel .thumb:hover {
    border: 3px solid #333;
  }
  .carousel .thumb img {
    vertical-align: top;
  }
  .carousel.carousel-slider {
    position: relative;
    margin: 0;
    overflow: hidden;
  }
  .carousel .slider-wrapper {
    overflow: hidden;
    margin: auto;
    width: 100%;
    -webkit-transition: height 0.15s ease-in;
    -moz-transition: height 0.15s ease-in;
    -ms-transition: height 0.15s ease-in;
    -o-transition: height 0.15s ease-in;
    transition: height 0.15s ease-in;
  }
  .carousel .slider {
    margin: 0;
    padding: 0;
    position: relative;
    list-style: none;
    width: 100%;
  }
  .carousel .slider.animated {
    -webkit-transition: all 0.35s ease-in-out;
    -moz-transition: all 0.35s ease-in-out;
    -ms-transition: all 0.35s ease-in-out;
    -o-transition: all 0.35s ease-in-out;
    transition: all 0.35s ease-in-out;
  }
  .carousel .slide {
    min-width: 100%;
    margin: 0;
    position: relative;
    text-align: center;
  }
  .carousel .slide img {
    width: 100%;
    vertical-align: top;
    border: 0;
  }
  .carousel .slide iframe {
    display: inline-block;
    width: calc(100% - 80px);
    margin: 0 40px 40px;
    border: 0;
  }
  .carousel .slide .legend {
    position: absolute;
    bottom: 40px;
    left: 50%;
    margin-left: -45%;
    width: 90%;
    border-radius: 10px;
    background: #000;
    color: #fff;
    padding: 10px;
    font-size: 12px;
    text-align: center;
    opacity: 0.25;
    -webkit-transition: opacity 0.35s ease-in-out;
    -moz-transition: opacity 0.35s ease-in-out;
    -ms-transition: opacity 0.35s ease-in-out;
    -o-transition: opacity 0.35s ease-in-out;
    transition: opacity 0.35s ease-in-out;
  }
  .carousel .slider-wrapper.axis-horizontal .slider {
    -ms-box-orient: horizontal;
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -moz-flex;
    display: -webkit-flex;
    display: flex;
  }
  .carousel .slider-wrapper.axis-horizontal .slider .slide {
    flex-direction: column;
  }
  .carousel .slider-wrapper.axis-vertical {
    -ms-box-orient: horizontal;
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -moz-flex;
    display: -webkit-flex;
    display: flex;
  }
  .carousel .slider-wrapper.axis-vertical .slider {
    -webkit-flex-direction: column;
    flex-direction: column;
  }
  .control-dots {
    position: absolute;
    bottom: 16px;
    left: 50px;
    width: 100%;
    margin: 0;
    list-style: none;
    text-align: center;
    display: flex;
    align-items: center;
    z-index: 1;
    ${(props) =>
      props.isModule
        ? `
      justify-content: flex-start;
      padding-left: ${props.theme.spacing.ELEM_SPACING.XS};
      @media only screen and (min-width: 768px) {
        padding-left: ${props.theme.spacing.ELEM_SPACING.REG};
      }
    `
        : `
      padding: 0;
      justify-content: center;
    `}
  }
  @media (min-width: 960px) {
    .control-dots {
      bottom: 0;
    }
  }
  .carousel-status {
    position: absolute;
    top: 0;
    right: 0;
    padding: 5px;
    font-size: 10px;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.9);
    color: #fff;
  }
  .carousel:hover .slide .legend {
    opacity: 1;
  }

  .carousel-root {
    position: relative;
    padding: 0px ${(props) => (props.carouselConfig.arrow === 'small' ? '38px' : '0px')};
    line-height: 14px;
    text-align: center;
    display: block;
    box-sizing: border-box;
    user-select: none;
    outline: none;
    -webkit-touch-callout: none;
    touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
  }
  /************************
  * @@@ Carousel Theme
  ************************/
  .control-prev,
  .control-next {
    font-size: 0;
    line-height: 0;
    position: absolute;
    top: 50%;
    display: block;
    width: ${(props) => (props.carouselConfig.arrow === 'small' ? '38px' : '20px')};
    height: ${(props) => (props.carouselConfig.arrow === 'small' ? '30px' : '20px')};
    padding: 0;
    transform: translate(0, -50%);
    cursor: pointer;
    color: transparent;
    border: none;
    outline: none;
    background-size: auto auto;
    z-index: 1;
  }
  .control-dots ul {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .control-dots li {
    position: relative;
    margin: 0 2px;
    width: 6px;
    height: 6px;
    font-size: 0;
    line-height: 0;
    display: block;
    cursor: pointer;
    color: transparent;
    border: 0;
    outline: none;
    background: transparent;
    padding: 0;

    &::before {
      content: '';
      box-sizing: border-box;
      position: absolute;
      top: 0;
      left: 0;
      width: 6px;
      height: 6px;
      border-radius: 50%;
      background: ${(props) => props.theme.colorPalette.gray[700]};
    }
  }

  li.selected {
    margin: 0;
    width: 10px;
    height: 10px;
    &:before {
      width: 10px;
      height: 10px;
      background: ${(props) => props.theme.colors.WHITE};
      border: 1px solid ${(props) => props.theme.colorPalette.gray[700]};
    }
  }

  .tcp_carousel__play_pause_button {
    background: transparent;
    border: 0;
    padding: 0 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    bottom: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
    cursor: pointer;
    position: absolute;
  }

  .tcp_carousel__play_pause_button + ul {
    margin-left: ${(props) => (props.isModule ? 0 : props.theme.spacing.ELEM_SPACING.SM)};
  }

  .tcp_carousel__play_pause_button_icon {
    ${(props) =>
      props.isModule
        ? `
      width: 40px;
      height: 40px;
    `
        : `
      background-color: ${props.theme.colors.WHITE};
      border-radius: 50%;
    `}
  }

  ${(props) =>
    props.carouselConfig.variation === 'big-arrows'
      ? `
    .control-next {
      height: 52px;
      right: -68px;
      width: 15px;
    }
    .control-prev {
      height: 52px;
      left: -68px;
      width: 15px;
    }
    `
      : ''}
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default CarouselStyle;
