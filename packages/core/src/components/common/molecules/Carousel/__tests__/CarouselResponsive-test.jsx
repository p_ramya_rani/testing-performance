// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CarouselResponsiveVanilla } from '../views/CarouselResponsive';

const playButton = '.tcp_carousel__play_pause_button';

describe('Carousel component', () => {
  it('show play button if autoplay: true prop is passed', () => {
    const props = {
      carouselConfig: {
        autoplay: true,
      },
      options: {
        autoplay: true,
      },
    };
    const wrapper = shallow(<CarouselResponsiveVanilla {...props} />);
    const getPlayIcon = jest.spyOn(wrapper.instance(), 'getPlayIcon');
    expect(wrapper.find(playButton)).toHaveLength(1);
    wrapper.find(playButton).simulate('click', { preventDefault: jest.fn() });
    expect(getPlayIcon).toHaveBeenCalled();
  });

  it('hide play button if autoplay: false prop is passed', () => {
    const props = {
      carouselConfig: {
        autoplay: false,
      },
      options: {
        autoplay: true,
      },
    };
    const wrapper = shallow(<CarouselResponsiveVanilla {...props} />);
    expect(wrapper.find(playButton)).toHaveLength(0);
  });

  it('show pause-accessible icon if isModuele prop is passed', () => {
    const props = {
      carouselConfig: {
        autoplay: true,
      },
      options: {
        autoplay: true,
      },
      isModule: true,
    };
    const wrapper = shallow(<CarouselResponsiveVanilla {...props} />);
    const getPlayIcon = jest.spyOn(wrapper.instance(), 'getPlayIcon');
    expect(wrapper.find(playButton)).toHaveLength(1);
    wrapper.find(playButton).simulate('click', { preventDefault: jest.fn() });
    expect(getPlayIcon).toHaveBeenCalled();
  });
  it('show fade on each slider if fade options are passed', () => {
    const props = {
      carouselConfig: {
        autoplay: true,
      },
      options: {
        autoplay: true,
        fade: true,
        autoplaySpeed: 5000,
        speed: 1000,
      },
    };
    const wrapper = shallow(<CarouselResponsiveVanilla {...props} />);
    expect(wrapper).toHaveLength(1);
  });
  it('show only children prop if autoplay is not passed', () => {
    const props = {
      carouselConfig: {
        autoplay: false,
      },
      children: [],
    };
    const wrapper = shallow(<CarouselResponsiveVanilla {...props} />);
    expect(wrapper).toHaveLength(1);
  });
});
