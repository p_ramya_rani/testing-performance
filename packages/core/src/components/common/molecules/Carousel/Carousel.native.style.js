// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import config from './Carousel.config.native';

const {
  playIconHeight,
  playIconWidth,
  modulePlayIconHeight,
  modulePlayIconWidth,
} = config.CAROUSEL_APP_CONFIG;

export const AccessibleDescriptionView = styled.Text`
  width: 1px;
  height: 1px;
`;

export const Touchable = styled.TouchableOpacity`
  position: absolute;
  bottom: ${props => (props.isModule ? `8px` : `12px`)};
  left: 50%;
  height: ${props => (props.isModule ? modulePlayIconHeight : playIconHeight)}px;
  width: ${props => (props.isModule ? modulePlayIconWidth : playIconWidth)}px;
  transform: translateX(-15px);
  z-index: ${props => props.theme.zindex.zOverlay};
  ${props =>
    !props.isModule
      ? `
    background-color: ${props.theme.colorPalette.white};
    border-radius: 15px;`
      : null}
`;

export const TouchableView = styled.TouchableOpacity`
  align-items: center;
`;

export const Icon = styled.Image`
  width: 6px;
  height: 10px;
`;

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ControlsWrapper = styled.View`
  flex-direction: row;
  justify-content: ${props => (props.isModule ? `flex-start` : `center`)};
  position: absolute;
  bottom: 0;
  width: 100%;
  ${props => (props.isModule ? `margin-left: 20px;` : null)}
`;

export const ControlsWrapperLeft = styled.View`
  flex-direction: row;
  justify-content: center;
  position: absolute;
  bottom: 10px;
  margin-left: 20px;
`;

export const ControlsWrapperRight = styled.View`
  flex-direction: row;
  position: absolute;
  bottom: 10px;
  right: 0;
`;

export const PlayPauseButtonView = styled.View`
  width: 30px;
`;

export const PaginationWrapper = styled.View`
  flex-direction: row;
  justify-content: center;
  bottom: ${props => (props.iconBottomMargin ? props.iconBottomMargin : '1px')};
`;

export default {
  Touchable,
  TouchableView,
  Icon,
  Container,
  PaginationWrapper,
  ControlsWrapperLeft,
  ControlsWrapperRight,
};
