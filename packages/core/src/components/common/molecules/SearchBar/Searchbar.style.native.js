// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { getScreenWidth } from '../../../../utils/index.native';
import Image from '../../atoms/Image';

const styles = css`
  margin: -${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS} 0px ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  justify-content: center;
`;

const getAdditionalStyle = (props) => {
  const { width } = props;
  return {
    ...(width ? { width } : { width: getScreenWidth() - 29 }),
  };
};

export const TouchableOpacityContainer = styled.TouchableOpacity`
  border-radius: ${(props) =>
    props.showSearchBarInline || props.isFromHomePage ? '20px' : '15px'};
  background: ${(props) =>
    props.showSearchBarInline || props.isFromHomePage
      ? props.theme.colorPalette.white
      : props.theme.colorPalette.gray[300]};
  height: ${(props) => (props.showSearchBarInline || props.isFromHomePage ? '40px' : '30px')};
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  ${getAdditionalStyle}
`;

export const TouchableQRContainer = styled.TouchableOpacity`
  height: 30px;
  align-items: center;
  justify-content: center;
`;

export const ViewQRContainer = styled.View`
  flex-direction: row;
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const ViewContainer = styled.View`
  align-items: center;
  margin-top: 32px;
`;

export const ImageTextContainer = styled.View`
  flex: 1;
  flex-direction: row;
`;

export const StyledSearchIcon = styled(Image)`
  ${(props) => (props.showSearchBarInline ? 'margin-right: 5px;' : ``)}
`;

export default styles;
