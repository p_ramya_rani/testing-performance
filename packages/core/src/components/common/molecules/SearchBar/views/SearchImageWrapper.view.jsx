// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getLabelValue } from '@tcp/core/src/utils/utils';

import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { SearchImageComp } from '@tcp/web/src/components/features/content/Header/molecules/CondensedHeader/Inline-Svg-Icons';
import SearchBarStyle from '../SearchBar.style';

import SearchLayoutWrapper from './SearchLayoutWrapper.view';

/**
 * This component produces a Search Bar component for Header
 * Expects textItems array consisting of objects in below format
 * {
 *    style: "",
 *    text: ""
 * }
 * This component uses BodyCopy atom and accepts all properties of BodyCopy
 * @param {*} props
 */
class SearchImageWrapper extends React.PureComponent {
  render() {
    const {
      fromCondensedHeader,
      className,
      openSearchBar,
      labels,
      showProduct,
      isLatestSearchResultsExists,
      latestSearchResults,
      searchResults,
      isSearchOpen,
      closeSearchBar,
      redirectToSuggestedUrl,
      setSearchState,
      setDataInLocalStorage,
      redirectToSearchPage,
      startSearch,
      closeModalSearch,
      commonCloseClick,
      toggleSearchResults,
      closeSearchLayover,
    } = this.props;
    return (
      <React.Fragment>
        {!isSearchOpen && !fromCondensedHeader && (
          <div id="search-input-wrapper" className="search-input-wrapper">
            <div className="searchBar-input-wrapper">
              <div id="search-input-form" className={`${className} searchBar-input-form`}>
                <label htmlFor="searchBar-input" className="visuallyhidden">
                  <input
                    id="searchBar-input"
                    ref={this.searchBarInput}
                    onClick={openSearchBar}
                    className="searchBar-input"
                    maxLength="50"
                    autoComplete="off"
                    placeHolder={getLabelValue(labels, 'lbl_what_looking_for')}
                    readOnly
                  />
                  <p>{getLabelValue(labels, 'lbl_what_looking_for')}</p>
                </label>
              </div>

              <span
                id="search-image-typeAhead"
                className="searchBar-image-typeAhead icon-small"
                data-locator="search-icon"
                height="25px"
                onClick={openSearchBar}
                role="presentation"
              >
                <SearchImageComp classNamePrefix="search-image" />
              </span>
            </div>
          </div>
        )}
        {isSearchOpen && (
          <SearchLayoutWrapper
            showProduct={showProduct}
            closeSearchBar={closeSearchBar}
            isLatestSearchResultsExists={isLatestSearchResultsExists}
            latestSearchResults={latestSearchResults}
            labels={labels}
            searchResults={searchResults}
            redirectToSuggestedUrl={redirectToSuggestedUrl}
            setSearchState={setSearchState}
            setDataInLocalStorage={setDataInLocalStorage}
            redirectToSearchPage={redirectToSearchPage}
            startSearch={startSearch}
            closeModalSearch={closeModalSearch}
            commonCloseClick={commonCloseClick}
            toggleSearchResults={toggleSearchResults}
            closeSearchLayover={closeSearchLayover}
            isSearchOpen={isSearchOpen}
          />
        )}
      </React.Fragment>
    );
  }
}

SearchImageWrapper.propTypes = {
  className: PropTypes.string.isRequired,
  openSearchBar: PropTypes.func.isRequired,
  closeSearchBar: PropTypes.func.isRequired,
  closeSearchLayover: PropTypes.func.isRequired,
  redirectToSuggestedUrl: PropTypes.func.isRequired,
  setSearchState: PropTypes.func.isRequired,
  setDataInLocalStorage: PropTypes.func.isRequired,
  redirectToSearchPage: PropTypes.func.isRequired,
  startSearch: PropTypes.func.isRequired,
  toggleSearchResults: PropTypes.func.isRequired,
  closeModalSearch: PropTypes.func.isRequired,
  commonCloseClick: PropTypes.func.isRequired,
  showProduct: PropTypes.bool,
  isLatestSearchResultsExists: PropTypes.bool,
  isSearchOpen: PropTypes.bool,
  fromCondensedHeader: PropTypes.bool,
  searchResults: PropTypes.shape({
    trends: PropTypes.shape({}),
    categories: PropTypes.shape({}),
    products: PropTypes.shape({}),
  }),
  labels: PropTypes.shape({
    lbl_search_whats_trending: PropTypes.string,
    lbl_search_recent_search: PropTypes.string,
    lbl_search_looking_for: PropTypes.string,
    lbl_search_product_matches: PropTypes.string,
  }),
  latestSearchResults: PropTypes.shape([]),
};

SearchImageWrapper.defaultProps = {
  showProduct: false,
  isSearchOpen: false,
  fromCondensedHeader: false,
  searchResults: {
    trends: {},
    categories: {},
    products: {},
  },
  labels: PropTypes.shape({
    lbl_search_whats_trending: '',
    lbl_search_recent_search: '',
    lbl_search_looking_for: '',
    lbl_search_product_matches: '',
    lbl_what_looking_for: '',
  }),
  isLatestSearchResultsExists: false,
  latestSearchResults: [],
};

export default connect()(withStyles(SearchImageWrapper, SearchBarStyle));
