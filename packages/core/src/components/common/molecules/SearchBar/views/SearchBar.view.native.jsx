// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { BodyCopy } from '../../../atoms';
import QRScannerIcon from '../../../atoms/QRScannerIcon';

import styles, {
  TouchableOpacityContainer,
  ViewContainer,
  TouchableQRContainer,
  ViewQRContainer,
  ImageTextContainer,
  StyledSearchIcon,
} from '../Searchbar.style.native';
import withStyles from '../../../hoc/withStyles';

const searchIcon = require('../../../../../../../mobileapp/src/assets/images/search_icon_app.png');

const AlignIconTextWrapper = ({ children, showSearchBarInline, fromHomePage }) => {
  let component = null;
  if (showSearchBarInline || fromHomePage) {
    component = <ImageTextContainer>{children}</ImageTextContainer>;
  } else {
    component = children;
  }
  return component;
};

/**
 * This component produces a Search Bar component for Header
 * This component uses BodyCopy atom and accepts all properties of BodyCopy
 * @param {*} props
 */
class SearchBar extends React.PureComponent {
  onSearchFocus = () => {
    const { openSearchProductPage } = this.props;
    if (openSearchProductPage) openSearchProductPage();
  };

  render() {
    const { showCustomizedSearch, labels, navigation, showSearchBarInline, isFromHomePage } =
      this.props;
    if (showCustomizedSearch) {
      return (
        <ViewContainer>
          <TouchableOpacityContainer
            width="200px"
            activeOpacity={1}
            onPress={this.onSearchFocus}
            accessibilityRole="search"
          >
            <BodyCopy
              fontWeight="regular"
              fontFamily="secondary"
              fontSize="fs12"
              text={getLabelValue(labels, 'lbl_looking_for')}
              color="gray.900"
            />
            <StyledSearchIcon source={searchIcon} height={16} width={16} />
          </TouchableOpacityContainer>
        </ViewContainer>
      );
    }

    return (
      <ViewQRContainer {...this.props}>
        <TouchableOpacityContainer
          activeOpacity={1}
          width={isFromHomePage ? '93%' : '95%'}
          onPress={this.onSearchFocus}
          accessibilityRole="search"
          showSearchBarInline={showSearchBarInline}
          isFromHomePage={isFromHomePage}
        >
          <AlignIconTextWrapper
            showSearchBarInline={showSearchBarInline}
            fromHomePage={isFromHomePage}
          >
            <StyledSearchIcon
              source={searchIcon}
              height={16}
              width={16}
              marginRight={5}
              showSearchBarInline={showSearchBarInline}
            />
            <BodyCopy
              fontWeight="regular"
              mobileFontFamily="secondary"
              fontSize={isFromHomePage ? 'fs14' : 'fs12'}
              text={Object.keys(labels).length ? getLabelValue(labels, 'lbl_looking_for') : ''}
              color="gray.900"
            />
          </AlignIconTextWrapper>
          <TouchableQRContainer activeOpacity={1} width="50px">
            <QRScannerIcon height={16} width={16} navigation={navigation} />
          </TouchableQRContainer>
        </TouchableOpacityContainer>
      </ViewQRContainer>
    );
  }
}

SearchBar.propTypes = {
  openSearchProductPage: PropTypes.func,
  showCustomizedSearch: PropTypes.bool,
  labels: PropTypes.shape({
    lbl_search_whats_trending: PropTypes.string,
    lbl_search_recent_search: PropTypes.string,
    lbl_search_looking_for: PropTypes.string,
    lbl_search_product_matches: PropTypes.string,
  }),
  navigation: PropTypes.shape({}).isRequired,
  showSearchBarInline: PropTypes.bool,
  isFromHomePage: PropTypes.string,
};

SearchBar.defaultProps = {
  openSearchProductPage: null,
  showCustomizedSearch: false,
  labels: PropTypes.shape({
    lbl_search_whats_trending: '',
    lbl_search_recent_search: '',
    lbl_search_looking_for: '',
    lbl_search_product_matches: '',
  }),
  showSearchBarInline: false,
  isFromHomePage: false,
};

export default withStyles(SearchBar, styles);
export { SearchBar as SearchBarVanilla };
