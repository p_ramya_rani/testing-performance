// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSiteId, isGymboree } from '@tcp/core/src/utils/utils';
import { BodyCopy, Anchor, DamImage } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import { isMobileApp } from '@tcp/core/src/utils';
import { currencyConversion } from '@tcp/core/src/components/features/CnC/CartItemTile/utils/utils';
import {
  getIsKeepAliveProduct,
  getIsKeepAliveProductApp,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import SearchBarStyle from '../SearchBar.style';
import { routerPush } from '../../../../../utils/index';
import OutOfStockWaterMark from '../../../../features/browse/ProductDetail/molecules/OutOfStockWaterMark';
import { getLabelsOutOfStock } from '../../../../features/browse/ProductListing/container/ProductListing.selectors';
import { unsetPLPBreadCrumb } from '../../../../features/browse/ProductListing/container/ProductListing.actions';

/**
 * This component produces a Search Bar component for Header
 * Expects textItems array consisting of objects in below format
 * {
 *    style: "",
 *    text: ""
 * }
 * This component uses BodyCopy atom and accepts all properties of BodyCopy
 * @param {*} props
 */
class LookingForProductDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.generateDamUrl = this.generateDamUrl.bind(this);
  }

  redirectToProductUrl = (productUrl) => {
    const { closeSearchLayover, unsetPLPBreadCrumbAction } = this.props;
    const imageSearch = true;
    closeSearchLayover();
    unsetPLPBreadCrumbAction({ breadCrumbTrail: null });
    if (productUrl.indexOf('/p/') !== -1) {
      routerPush(
        `/p?pid=${productUrl.split('/p/')[1]}&imageSearch=${imageSearch}`,
        `${productUrl}`,
        {
          shallow: false,
        }
      );
    } else {
      routerPush(
        `/b?bid=${productUrl.split('/b/')[1]}&imageSearch=${imageSearch}`,
        `${productUrl}`,
        {
          shallow: false,
        }
      );
    }
  };

  generateDamUrl = (itemUrl) => {
    const fileNameFull =
      itemUrl && itemUrl[0] ? itemUrl[0].substring(itemUrl[0].lastIndexOf('/') + 1) : '';
    const fileNameNoExt =
      fileNameFull.lastIndexOf('.') > 0
        ? fileNameFull.substring(0, fileNameFull.lastIndexOf('.'))
        : fileNameFull;
    const prodNum = fileNameNoExt.split('_')[0];
    return `${prodNum}/${fileNameFull}`;
  };

  render() {
    const { searchResults, isKeepAliveEnabled, outOfStockLabels, currencyAttributes } = this.props;

    return (
      <React.Fragment>
        <BodyCopy className="matchProductBody" lineHeight="39" component="div">
          <ul>
            {searchResults &&
              searchResults.autosuggestProducts &&
              searchResults.autosuggestProducts.map((item) => {
                const keepAlive = isKeepAliveEnabled && item.keepAlive;
                const { primaryBrand } = item;
                return (
                  <BodyCopy component="li" key={item.id} className="productBox">
                    <ClickTracker
                      clickData={{
                        customEvents: ['event100', 'event20'],
                        var22: 'internal search',
                        pageSearchText: item.name,
                        pageSearchType: 'image search',
                        listingCount: 4,
                      }}
                      as={Anchor}
                      className="suggestion-label out-of-stock-wrapper"
                      noLink
                      to={`/${getSiteId()}${item.productUrl}`}
                      onClick={(e) => {
                        e.preventDefault();
                        this.redirectToProductUrl(`${item.productUrl}`);
                      }}
                    >
                      <DamImage
                        className="autosuggest-image"
                        imgData={{
                          alt: `${item.name}`,
                          url: `${this.generateDamUrl(item.imageUrl)}`,
                        }}
                        imgConfigs={[`t_search_img_m`, `t_search_img_t`, `t_search_img_d`]}
                        isProductImage
                        height="25px"
                        lazyLoad={false}
                        primaryBrand={primaryBrand}
                      />
                      {keepAlive && (
                        <OutOfStockWaterMark
                          label={outOfStockLabels.outOfStockCaps}
                          fontSizes={['fs12', 'fs16', 'fs12']}
                        />
                      )}
                      <BodyCopy
                        component="div"
                        color={isGymboree() ? 'gray.900' : 'red.500'}
                        fontWeight="bold"
                        fontSize="fs14"
                        textAlign="center"
                        className="product-price"
                      >
                        <PriceCurrency
                          price={
                            currencyAttributes &&
                            currencyAttributes.exchangevalue &&
                            currencyConversion(item.offerPrice, currencyAttributes)
                          }
                        />
                      </BodyCopy>
                    </ClickTracker>
                  </BodyCopy>
                );
              })}
          </ul>
        </BodyCopy>
      </React.Fragment>
    );
  }
}

LookingForProductDetail.propTypes = {
  closeSearchLayover: PropTypes.func.isRequired,
  unsetPLPBreadCrumbAction: PropTypes.func.isRequired,
  searchResults: PropTypes.shape({
    trends: PropTypes.shape({}),
    categories: PropTypes.shape({}),
    products: PropTypes.shape({}),
  }),
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  currencyAttributes: PropTypes.shape({}).isRequired,
};

LookingForProductDetail.defaultProps = {
  searchResults: {
    trends: {},
    categories: {},
    products: {},
  },
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
};

const mapStateToProps = (state) => {
  return {
    isKeepAliveEnabled: isMobileApp()
      ? getIsKeepAliveProductApp(state)
      : getIsKeepAliveProduct(state),
    outOfStockLabels: getLabelsOutOfStock(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    unsetPLPBreadCrumbAction: (payload) => {
      dispatch(unsetPLPBreadCrumb(payload));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(LookingForProductDetail, SearchBarStyle));
