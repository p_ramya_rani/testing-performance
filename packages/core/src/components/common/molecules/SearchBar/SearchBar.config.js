// 9fbef606107a605d69c0edbcd8029e5d 
const searchApiConfig = {
  categoryCount: 4,
  topQueriesCount: 4,
  productsCounts: 4,
  suggestionsCount: 4,
};

export default { searchApiConfig };
