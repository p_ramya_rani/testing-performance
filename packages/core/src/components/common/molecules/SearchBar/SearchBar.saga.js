// 9fbef606107a605d69c0edbcd8029e5d
import { takeLatest, call, put } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { makeSearch, makePopularSearch } from '@tcp/core/src/services/abstractors/common/searchBar';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import SEARCH_CONSTANTS from './SearchBar.constants';
import { setSearchResult, setShowMoreProductFlag, setPopularSearch } from './SearchBar.actions';

export function* getSearchResult({ payload }) {
  const suggestionsCount = {
    category: 4,
    keywords: 4,
    promotedTopQueries: 4,
  };

  const isHideBundleProduct = false;
  const payloadData = {
    searchTerm: payload.searchText,
    suggestionsCount,
    isHideBundleProduct,
    slpLabels: payload.slpLabels,
  };
  const xappURLConfig = yield call(getUnbxdXappConfigs);
  try {
    const response = yield call(makeSearch, payloadData, xappURLConfig);
    if (
      response.autosuggestList &&
      response.autosuggestList[0].suggestions.length < 1 &&
      response.autosuggestProducts.length < 1
    ) {
      yield put(setSearchResult(response));
      yield put(setShowMoreProductFlag(false));
    } else {
      yield put(setSearchResult(response));
      yield put(setShowMoreProductFlag(true));
    }
  } catch (err) {
    logger.error('Error: error in fetching Search bar results ', {
      error: err,
      extraData: {
        component: 'SearchBar Saga - getSearchResult',
        payloadData: payload,
      },
    });
  }
}

export function* getPopularSearch({ payload }) {
  const payloadData = {
    slpLabels: payload.slpLabels,
  };

  const xappURLConfig = yield call(getUnbxdXappConfigs);
  try {
    const response = yield call(makePopularSearch, payloadData, xappURLConfig);

    yield put(setPopularSearch(response));
  } catch (err) {
    logger.error('Error: error in fetching Search bar results ', {
      error: err,
      extraData: {
        component: 'SearchBar Saga - getPopularSearch',
        payloadData: '',
      },
    });
  }
}

function* SearchBarSaga() {
  yield takeLatest(SEARCH_CONSTANTS.START_SEARCH, getSearchResult);
  yield takeLatest(SEARCH_CONSTANTS.POPULAR_SEARCH, getPopularSearch);
}

export default SearchBarSaga;
