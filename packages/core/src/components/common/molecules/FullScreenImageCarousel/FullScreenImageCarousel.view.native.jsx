// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { Dimensions } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import isEqual from 'lodash/isEqual';
import { HAPTICFEEDBACK_EVENTS } from '@tcp/core/src/components/common/atoms/hapticFeedback/HapticFeedback.constants';
import PaginationDots from '../PaginationDots';
import ModalNative from '../Modal';
import {
  getScreenHeight,
  getScreenWidth,
  getAPIConfig,
  isAndroid,
} from '../../../../utils/index.native';
import {
  ModalCarousel,
  PaginationContainer,
  getImageViewerStyle,
} from './styles/FullScreenImageCarousel.style.native';
import ImageCarousel from '../ImageCarousel';
import commonConfig from '../ProductDetailImage/config';
import ProductVariantSelector from '../ProductVariantSelector';

const dimensions = Dimensions.get('window');
const imageHeight = dimensions.height * 0.5;
const imageWidth = dimensions.width * 0.98;
let isVideoAvailable = false;
class FullScreenImageCarousel extends React.PureComponent {
  constructor(props) {
    super(props);
    const { parentActiveIndex, imageUrls, formValues } = this.props;
    const { color } = formValues || {};
    this.state = {
      zoomImage: true,
      activeSlideIndex: parentActiveIndex || 0,
      currentSwatch: color,
      imageUrlsInState: imageUrls,
      counter: 0,
    };
    this.flatListRef = null;
    this.isAndroidDevice = isAndroid();
  }

  componentDidMount() {
    this.setState((prevState) => ({ counter: prevState.counter + 1 }));
  }

  componentWillUnmount() {
    const { onCloseModal } = this.props;
    onCloseModal();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { imageUrls, isNewReDesignFullScreenImageCarousel, colorFitsSizesMap } = nextProps;
    if (!isNewReDesignFullScreenImageCarousel) return null;
    const getImageUrlSecondPartAfterSplit = (text, splitBy = '-') => {
      if (text && splitBy) return text.split(splitBy).pop();
      return '';
    };
    const checkVideoAvailable = (extension) => {
      const extensionArray = ['mp4', 'webm', 'gif', 'mov'];
      return extensionArray.includes(extension);
    };
    if (!isEqual(prevState.imageUrls, imageUrls)) {
      const { currentSwatch } = prevState;
      const selectedSwatch = colorFitsSizesMap.find((color) => color.color.name === currentSwatch);
      const imageName = selectedSwatch?.imageName;
      const imageId = imageName && imageName.split('_')[0];
      const imageFirstPart = `${imageId}/${imageName}`;

      const newImageUrls =
        imageUrls &&
        imageUrls.map((item) => {
          if (item) {
            if (item.iconSizeImageUrl && item.iconSizeImageUrl.indexOf('-') === -1) {
              const extension = getImageUrlSecondPartAfterSplit(item.iconSizeImageUrl, '.');
              const imageUrlFinal = `${imageFirstPart}.${extension}`;
              isVideoAvailable = checkVideoAvailable(extension);
              return {
                isShortImage: false,
                isOnModalImage: false,
                iconSizeImageUrl: `${imageUrlFinal}`,
                listingSizeImageUrl: `${imageUrlFinal}`,
                regularSizeImageUrl: `${imageUrlFinal}`,
                bigSizeImageUrl: `${imageUrlFinal}`,
                superSizeImageUrl: `${imageUrlFinal}`,
              };
            }
            return {
              isShortImage: false,
              isOnModalImage: false,
              iconSizeImageUrl: `${imageFirstPart}-${getImageUrlSecondPartAfterSplit(
                item.iconSizeImageUrl
              )}`,
              listingSizeImageUrl: `${imageFirstPart}-${getImageUrlSecondPartAfterSplit(
                item.listingSizeImageUrl
              )}`,
              regularSizeImageUrl: `${imageFirstPart}-${getImageUrlSecondPartAfterSplit(
                item.regularSizeImageUrl
              )}`,
              bigSizeImageUrl: `${imageFirstPart}-${getImageUrlSecondPartAfterSplit(
                item.bigSizeImageUrl
              )}`,
              superSizeImageUrl: `${imageFirstPart}-${getImageUrlSecondPartAfterSplit(
                item.superSizeImageUrl
              )}`,
            };
          }
          return {};
        });
      return { imageUrlsInState: newImageUrls, imageUrls };
    }
    return null;
  }

  toggleModal = () => {
    const { zoomImage } = this.state;
    this.setState({ zoomImage: !zoomImage, activeSlideIndex: 0 });
  };

  // this method call when tap on the pagination dots and navigate to clicked image
  onPageChange = (dotClickedIndex) => {
    this.setState({ activeSlideIndex: dotClickedIndex });
  };

  getImageUrlSecondPartAfterSplit = (text, splitBy = '-') => {
    if (text && splitBy) return text.split(splitBy).pop();
    return '';
  };

  onImageColorSwatchClicked = (name) => {
    const { getCurrentSelectedUrls, comingFromFullScreen, fireHapticFeedback } = this.props;
    this.setState((prevState) => ({ currentSwatch: name, counter: prevState.counter + 1 }));
    getCurrentSelectedUrls(name);
    comingFromFullScreen();
    fireHapticFeedback(HAPTICFEEDBACK_EVENTS.SOFT);
  };

  renderImageSwatches = () => {
    const { isNewReDesignFullScreenImageCarousel, colorFitsSizesMap, formValues } = this.props;
    if (isNewReDesignFullScreenImageCarousel && colorFitsSizesMap.length > 0) {
      return (
        <ProductVariantSelector
          data={colorFitsSizesMap}
          isNewReDesignImageSwatches
          formValues={formValues}
          whileInFullScreen
          changeProductImageFullScreen={this.onImageColorSwatchClicked}
        />
      );
    }
    return null;
  };

  callbackIndex = (index) => {
    this.setState({ activeSlideIndex: index });
  };

  render() {
    const {
      imageUrls,
      isSBP,
      isNewReDesignFullScreenImageCarousel,
      zoomImages,
      primaryBrand,
      alternateBrand,
    } = this.props;

    const { zoomImage, activeSlideIndex, imageUrlsInState } = this.state;
    const fullWidth = {
      width: '100%',
      alignItems: isNewReDesignFullScreenImageCarousel ? 'flex-start' : 'flex-end',
    };

    const config = 'w_900,dpr_2.0';
    const apiConfigObj = getAPIConfig();

    let { brandId } = apiConfigObj;

    const changeHeight = this.isAndroidDevice ? 1.1 : 1.2;
    if (primaryBrand) {
      brandId = primaryBrand;
    }
    if (alternateBrand) {
      brandId = alternateBrand;
    }
    const brandName = brandId && brandId.toUpperCase();
    const assetHost = apiConfigObj[`assetHost${brandName}`];
    const productAssetPath = apiConfigObj[`productAssetPath${brandName}`];
    const imageObjects = imageUrlsInState.map((item) => ({
      url: `${assetHost}/${config}/${productAssetPath}/${item.regularSizeImageUrl}`,
      width: isSBP ? getScreenWidth() + 40 : getScreenWidth() + 200,
      height: getScreenHeight() / changeHeight,
      props: {
        accessible: true,
        accessibilityRole: 'image',
        accessibilityLabel: `product image ${activeSlideIndex + 1}`,
      },
    }));
    const numberOfImages = (imageUrls && imageUrls.length) || 0;
    if (activeSlideIndex >= numberOfImages - 1) {
      this.setState({ activeSlideIndex: numberOfImages - 1 });
    }
    const modalMargins = this.isAndroidDevice ? '0' : '12px 0 0 0';
    const isFullScreenAndroid = this.isAndroidDevice;
    return (
      <ModalNative
        isOpen={zoomImage}
        onRequestClose={this.toggleModal}
        overlayClassName="TCPModal__Overlay"
        closeIconDataLocator=""
        closeIconLeftAligned={false}
        horizontalBar={false}
        headerStyle={fullWidth}
        isOverlay
        noscroll
        stickyCloseIcon={false}
        margins={modalMargins}
        iconType={isNewReDesignFullScreenImageCarousel ? 'arrow' : null}
        isNewReDesignTopLeftIcon={isNewReDesignFullScreenImageCarousel}
      >
        <ModalCarousel
          height={
            isNewReDesignFullScreenImageCarousel ? getScreenHeight() / 1.45 : getScreenHeight()
          }
        >
          {isVideoAvailable ? (
            <ImageCarousel
              zoomImages={zoomImages}
              imageUrls={imageUrls}
              imgConfig={commonConfig.IMG_DATA_CLIENT.imgConfig[0]}
              videoConfig={commonConfig.VID_DATA.imgConfig[0]}
              imageWidth={imageWidth}
              imageHeight={imageHeight}
              selectedItemIndex={(index) => this.callbackIndex(index)}
              setDefaultIndex={activeSlideIndex}
              PaginationDotsIndex={activeSlideIndex}
              disabledTouch
              primaryBrand={primaryBrand}
              alternateBrand={alternateBrand}
            />
          ) : (
            <ImageViewer
              imageUrls={imageObjects}
              backgroundColor="white"
              style={getImageViewerStyle(isNewReDesignFullScreenImageCarousel)}
              saveToLocalByLongPress={false}
              doubleClickInterval={500}
              renderIndicator={() => null}
              onChange={(index) => {
                this.setState({ activeSlideIndex: index });
              }}
              index={activeSlideIndex}
              enablePreload
              enableImageZoom
              primaryBrand={primaryBrand}
              alternateBrand={alternateBrand}
            />
          )}

          {numberOfImages > 1 && (
            <PaginationContainer
              isNewReDesignPaginationContainer={isNewReDesignFullScreenImageCarousel}
            >
              <PaginationDots
                numberOfDots={numberOfImages}
                selectedIndex={activeSlideIndex}
                onPress={this.onPageChange}
                isFullScreenAndroid={isFullScreenAndroid}
                isNewReDesignEnabled={isNewReDesignFullScreenImageCarousel}
              />
            </PaginationContainer>
          )}
        </ModalCarousel>
        {this.renderImageSwatches()}
      </ModalNative>
    );
  }
}

FullScreenImageCarousel.propTypes = {
  imageUrls: PropTypes.arrayOf(Object),
  parentActiveIndex: PropTypes.number,
  onCloseModal: PropTypes.func,
  isSBP: PropTypes.bool,
  currentColor: PropTypes.shape({}),
  getCurrentSelectedUrls: PropTypes.func,
  isNewReDesignFullScreenImageCarousel: PropTypes.bool,
  colorFitsSizesMap: PropTypes.arrayOf(Object),
  comingFromFullScreen: PropTypes.func,
  formValues: PropTypes.shape({}),
  fireHapticFeedback: PropTypes.func,
};

FullScreenImageCarousel.defaultProps = {
  imageUrls: [],
  parentActiveIndex: null,
  onCloseModal: () => {},
  isSBP: false,
  currentColor: {},
  getCurrentSelectedUrls: () => {},
  isNewReDesignFullScreenImageCarousel: false,
  colorFitsSizesMap: [],
  comingFromFullScreen: () => {},
  formValues: {},
  fireHapticFeedback: () => {},
};

export default FullScreenImageCarousel;
