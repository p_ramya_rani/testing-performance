// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export const getImageViewerStyle = (isNewReDesignFullScreenImageCarousel) => {
  if (isNewReDesignFullScreenImageCarousel) {
    return {
      marginBottom: 40,
      marginLeft: 0,
      marginRight: 0,
    };
  }
  return {
    marginBottom: 100,
    marginLeft: 16,
    marginRight: 16,
  };
};

export const ModalCarousel = styled.View`
  justify-content: center;
  height: ${(props) => props.height};
`;

export const PaginationContainer = styled.View`
  justify-content: center;
  flex-direction: row;
  align-items: center;
  width: 100%;
  position: absolute;
  bottom: ${(props) => (props.isNewReDesignPaginationContainer ? `-15` : `80`)};
`;

export const SwatchContainer = styled.View`
  width: 100%;
  height: 100%;
  margin-top: 20px;
  padding-top: 8px;
  align-items: center;
`;

export const SwatchItemContainer = styled.View`
  ${(props) =>
    props.name === props.currentSwatch
      ? `
      border: solid 1px ${
        props.theme.isGymboree
          ? props.theme.colors.DEFAULT_ACCENT_GYM
          : props.theme.colorPalette.blue[500]
      };
      width: 59px;
      height: 65px;
      margin: ${props.theme.spacing.APP_LAYOUT_SPACING.XXS} 0;
      padding: ${props.theme.spacing.ELEM_SPACING.XXXS} ${props.theme.spacing.ELEM_SPACING.XXS};
      background-color: ${props.theme.colors.WHITE};
      box-shadow: ${props.theme.shadow.NEW_DESIGN.BOX_SHADOW};
      border-radius: ${props.theme.spacing.ELEM_SPACING.XS};
      elevation: 10;
    `
      : `
      padding: 1px ${props.theme.spacing.ELEM_SPACING.XXS};
      margin: ${props.theme.spacing.APP_LAYOUT_SPACING.XXS} 0 ${props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    `}
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.REG}
`;
