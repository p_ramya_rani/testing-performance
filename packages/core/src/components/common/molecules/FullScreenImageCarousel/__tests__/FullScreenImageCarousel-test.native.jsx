// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';

import FullScreenImageCarousel from '../FullScreenImageCarousel.view.native';

describe('FullScreenImageCarousel should render correctly', () => {
  let wrapper;
  let component;

  const props = {
    getCurrentSelectedUrls: jest.fn(),
    comingFromFullScreen: jest.fn(),
    fireHapticFeedback: jest.fn(),
  };

  beforeEach(() => {
    wrapper = shallow(<FullScreenImageCarousel imageUrls={[{ regularSizeImageUrl: '//' }]} />);
    component = shallow(<FullScreenImageCarousel {...props} />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot for ios', () => {
    expect(wrapper.find('ImageViewer').length).toBe(1);
  });

  it('should close modal on toggle', () => {
    wrapper.instance().toggleModal();
    expect(wrapper.instance().state.zoomImage).toBeFalsy();
  });

  it('should return second part of image url', () => {
    const text = '12345-abc';
    const splitBy = '-';
    const expectedResult = 'abc';
    expect(wrapper.instance().getImageUrlSecondPartAfterSplit(text, splitBy)).toEqual(
      expectedResult
    );
  });

  it('onImageColorSwatchClicked should call three functions passed as props', () => {
    component.instance().onImageColorSwatchClicked();
    expect(props.getCurrentSelectedUrls).toHaveBeenCalled();
    expect(props.comingFromFullScreen).toHaveBeenCalled();
    expect(props.fireHapticFeedback).toHaveBeenCalled();
  });

  it('renderImageSwatches should render correct UI', () => {
    component.setProps({
      isNewReDesignFullScreenImageCarousel: true,
      colorFitsSizesMap: [
        {
          color: {
            name: 'BLUE',
            swatchImage: '12345.abc',
          },
          imageName: '12345_abc',
        },
      ],
    });
    expect(component.instance().renderImageSwatches()).toMatchSnapshot();
  });

  it('renderImageSwatches should return null if not new design', () => {
    component.setProps({
      isNewReDesignFullScreenImageCarousel: false,
      colorFitsSizesMap: [],
    });
    expect(component.instance().renderImageSwatches()).toBeNull();
  });

  it('getDerivedStateFromProps should return null if not new design', () => {
    const nextProps = {
      imageUrls: [],
      isNewReDesignFullScreenImageCarousel: false,
      colorFitsSizesMap: [],
    };
    const prevState = { imageUrls: [] };
    const result = component.instance().constructor.getDerivedStateFromProps(nextProps, prevState);
    expect(result).toBeNull();
  });
});
