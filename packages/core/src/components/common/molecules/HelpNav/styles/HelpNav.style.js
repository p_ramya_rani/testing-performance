// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  #launcher {
    display: ${props => (props.hasABTestForChatBot ? 'none' : 'block')};
  }
  .menu-link {
    padding-bottom: 18px;
    max-width: 140px;
    @media ${props => props.theme.mediaQuery.large} {
      padding-bottom: 14px;
    }
    cursor: default;
  }

  &.show-help {
    width: 162px;
    margin: ${props => props.theme.spacing.ELEM_SPACING.XXL} 0 0;
    padding: ${props => props.theme.spacing.ELEM_SPACING.LRG}
      ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS}
      ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    border-radius: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS} 0 0
      ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.2);
    z-index: 100;
    position: fixed;
    bottom: ${props => (props.orderDetailsSticky ? '74px' : '10px')};
    @media ${props => props.theme.mediaQuery.medium} {
      bottom: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    }
    @media ${props => props.theme.mediaQuery.large} {
      bottom: 22px;
    }
    right: 0px;
    transform: translate(0, -50%);
    background-color: ${props => props.theme.colors.WHITE};
    cursor: pointer;
    animation: slide-in 0.5s forwards;
  }

  &.shift-up {
    bottom: 110px;
  }

  &.hide-help {
    animation: slide-out 0.5s forwards;
    display: none;
  }

  .close-button {
    width: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
    height: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
    padding: 0;
    margin: 0;
    background: transparent;
    border: none;
    position: absolute;
    top: 18px;
    right: 18px;
  }

  @keyframes slide-in {
    0% {
      transform: translateX(100%);
    }
    100% {
      transform: translateX(0%);
    }
  }

  @keyframes slide-out {
    0% {
      transform: translateX(0%);
    }
    100% {
      transform: translateX(100%);
    }
  }
`;
