// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { HelpNavVanilla } from '../views/HelpNav.view';
import { routerPush } from '../../../../../utils';

jest.mock('../../../../../utils', () => ({
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  configureInternalNavigationFromCMSUrl: jest.fn(),
  isCanada: jest.fn(),
  selectOrderAction: jest.fn(),
  fireEnhancedEcomm: jest.fn(),
}));

const menuLink = '.menu-link';

describe('FloatingHelpMenu component', () => {
  const props = {
    className: 'help-nav',
    helpWithOrderLabels: {
      returnPolicyText: 'Return Policy',
      returnPolicyUrl: '/help-center/returns-exchanges#returnsandexchanges',
      orderStatusHelpText: 'Contact Us About Your Order',
      orderStatusHelpUrl: '/content/order-status-help',
    },
    helpMenuLabels: {
      helpCenterUrl: '/help-center/faq',
      helpCenterText: 'Help Center',
      orderStatusUrl: '/help-center/order-help#orderstatus',
      orderStatusText: 'Order Status',
      leaveFeedbackUrl: '/test',
      leaveFeedbackText: 'Leave Feedback',
      needHelpText: 'Need help?',
      needHelpImg:
        'https://test1.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/clock_3x.png',
    },
    isLoggedIn: false,
    isCSHEnabled: true,
    openTrackOrderOverlay: jest.fn().mockImplementation(() => {}),
    selectOrderAction: jest.fn().mockImplementation(() => {}),
  };

  it('should render correctly', () => {
    const component = shallow(<HelpNavVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call the overlay for the third menu click', () => {
    const event = {
      preventDefault: () => {},
    };

    const component = shallow(<HelpNavVanilla {...props} />);
    component
      .find(menuLink)
      .at(2)
      .props()
      .children.props.handleLinkClick(event);
    expect(props.openTrackOrderOverlay).toHaveBeenCalled();
  });

  it('should render order assistance', () => {
    const component = shallow(<HelpNavVanilla {...props} />);
    const event = {
      preventDefault: () => {},
    };
    component
      .find(menuLink)
      .at(3)
      .props()
      .children.props.handleLinkClick(event);
    expect(props.selectOrderAction).toHaveBeenCalled();
  });

  it.only('should call the handleLinkClick for the fourth menu click', () => {
    const component = shallow(<HelpNavVanilla {...props} />);
    const clonedComponent = Object.assign(
      {},
      component
        .find(menuLink)
        .at(4)
        .props().children.props
    );
    clonedComponent.handleLinkClick = jest.fn(() => true);
    expect(clonedComponent.handleLinkClick()).toEqual(true);
  });
});

describe('FloatingHelpMenu component', () => {
  const props = {
    className: 'help-nav',
    helpWithOrderLabels: {
      returnPolicyText: 'Return Policy',
      returnPolicyUrl: '/help-center/returns-exchanges#returnsandexchanges',
      orderStatusHelpText: 'Contact Us About Your Order',
      orderStatusHelpUrl: '/content/order-status-help',
    },
    helpMenuLabels: {
      helpCenterUrl: '/help-center/faq',
      helpCenterText: 'Help Center',
      orderStatusUrl: '/help-center/order-help#orderstatus',
      orderStatusText: 'Order Status',
      leaveFeedbackUrl: '/test',
      leaveFeedbackText: 'Leave Feedback',
      needHelpText: 'Need help?',
      needHelpImg:
        'https://test1.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/clock_3x.png',
    },

    isLoggedIn: true,
    openTrackOrderOverlay: jest.fn().mockImplementation(() => {}),
    flyoutOpen: true,
  };

  routerPush.mockImplementation(() => {});

  it('should call the overlay for the third menu click', () => {
    const event = {
      preventDefault: () => {},
    };

    const component = shallow(<HelpNavVanilla {...props} />);
    component
      .find(menuLink)
      .at(2)
      .props()
      .children.props.handleLinkClick(event);
    expect(routerPush).toHaveBeenCalled();
  });
});
