// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Anchor, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import {
  configureInternalNavigationFromCMSUrl,
  routerPush,
  getIconPath,
  isCanada,
} from '@tcp/core/src/utils';
import {
  CUSTOMER_HELP_GA,
  CUSTOMER_HELP_ROUTES,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import style from '../styles/HelpNav.style';
import { routeToPage } from '../../../../features/account/CustomerHelp/util/utility';

const HelpNav = ({
  className,
  flyoutOpen,
  helpMenuLabels = {},
  helpWithOrderLabels,
  isLoggedIn,
  openTrackOrderOverlay,
  toggleMenu,
  stickyFooter,
  isCSHEnabled,
  trackAnalyticsClick,
  mergeAccountReset,
  resetEmailConfirmationRequest,
}) => {
  const { closeText } = helpMenuLabels;
  const closeImgPath = getIconPath('modal-close');

  const HelpNavItems = [
    {
      title: helpMenuLabels.helpCenterText,
      url: helpMenuLabels.helpCenterUrl,
    },
    {
      title: helpWithOrderLabels.returnPolicyText,
      url: helpWithOrderLabels.returnPolicyUrl,
    },
    {
      title: helpMenuLabels.orderStatusText,
      handleLinkClick: (e) => {
        e.preventDefault();
        if (!isLoggedIn) openTrackOrderOverlay({ state: true });
        else routerPush('/account?id=orders', '/account?id=orders');
      },
    },
    {
      title: helpMenuLabels.customerSupportText,
      url: !isCanada() && !isCSHEnabled ? helpMenuLabels.orderStatusContactUrl : '',
      handleLinkClick: isCSHEnabled
        ? (e) => {
            e.preventDefault();
            mergeAccountReset();
            resetEmailConfirmationRequest();
            trackAnalyticsClick({ orderHelpLocation: CUSTOMER_HELP_GA.HELP_NAV_TRACK.CD126 });
            routeToPage(CUSTOMER_HELP_ROUTES.LANDING_PAGE);
          }
        : isCSHEnabled,
      hideMenuItem: isCanada() && !isCSHEnabled,
    },
    {
      title: helpMenuLabels.leaveFeedbackText,
      handleLinkClick: (e) => {
        e.preventDefault();
        // iperceptions_128721.ipeCC() has came from iperception, so not handling eslint for this line
        // eslint-disable-next-line no-undef, camelcase
        return iperceptions_128721 && iperceptions_128721.ipeCC();
      },
    },
  ];
  return (
    <nav
      id="help-nav"
      className={`${className} ${stickyFooter ? 'shift-up' : ''} ${
        flyoutOpen ? 'show-help' : 'hide-help'
      }`}
    >
      <button className="close-button" aria-label={closeText} onClick={toggleMenu}>
        <Image src={closeImgPath} alt={closeText} />
      </button>
      {HelpNavItems.map((item) => {
        const { title, url, handleLinkClick, hideMenuItem } = item;
        if (hideMenuItem) {
          return null;
        }
        return (
          <div className="menu-link">
            <Anchor
              to={url ? configureInternalNavigationFromCMSUrl(url) : ''}
              title={title}
              asPath={url || ''}
              noLink={!!handleLinkClick}
              handleLinkClick={(e) => handleLinkClick(e)}
            >
              <BodyCopy
                fontSize="fs14"
                fontFamily="secondary"
                fontWeight="semibold"
                textAlign="left"
                color="blue.C900"
              >
                {title}
              </BodyCopy>
            </Anchor>
          </div>
        );
      })}
    </nav>
  );
};

HelpNav.propTypes = {
  className: PropTypes.string,
  flyoutOpen: PropTypes.bool,
  helpWithOrderLabels: PropTypes.shape({}),
  helpMenuLabels: PropTypes.shape({}),
  isLoggedIn: PropTypes.bool,
  openTrackOrderOverlay: PropTypes.func,
  toggleMenu: PropTypes.func,
  stickyFooter: PropTypes.bool,
  isCSHEnabled: PropTypes.bool,
  trackAnalyticsClick: PropTypes.func,
  mergeAccountReset: PropTypes.func,
  resetEmailConfirmationRequest: PropTypes.func,
};

HelpNav.defaultProps = {
  className: '',
  flyoutOpen: '',
  helpWithOrderLabels: {},
  helpMenuLabels: {},
  isLoggedIn: false,
  openTrackOrderOverlay: null,
  toggleMenu: null,
  stickyFooter: false,
  isCSHEnabled: false,
  trackAnalyticsClick: () => {},
  mergeAccountReset: () => {},
  resetEmailConfirmationRequest: () => {},
};

export default withStyles(HelpNav, style);
export { HelpNav as HelpNavVanilla };
