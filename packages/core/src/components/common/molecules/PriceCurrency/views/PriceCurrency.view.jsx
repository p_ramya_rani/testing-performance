// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';

class PriceCurrency extends React.Component {
  render() {
    const { currency, currencySymbol, price, preFixLabel, postFixLabel } = this.props;
    const space = currency === 'USD' || currency === 'CAD' ? '' : ' ';
    return `${preFixLabel}${currencySymbol}${(currency && space) || ''}${(price || 0).toFixed(
      2
    )}${postFixLabel}`;
  }
}

PriceCurrency.propTypes = {
  currency: PropTypes.string.isRequired,
  currencySymbol: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  preFixLabel: PropTypes.string,
  postFixLabel: PropTypes.string,
};

PriceCurrency.defaultProps = {
  preFixLabel: '',
  postFixLabel: '',
};

export default errorBoundary(PriceCurrency);
export { PriceCurrency as PriceCurrencyVanilla };
