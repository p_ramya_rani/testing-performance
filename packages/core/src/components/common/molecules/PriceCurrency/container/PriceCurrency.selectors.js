// 9fbef606107a605d69c0edbcd8029e5d 
import { SESSIONCONFIG_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';

export const getCurrency = state => {
  return state[SESSIONCONFIG_REDUCER_KEY].siteDetails.currency;
};

export default getCurrency;
