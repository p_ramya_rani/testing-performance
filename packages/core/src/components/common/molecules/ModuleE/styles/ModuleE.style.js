// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel/views/Carousel';

const StyledCarousal = styled(Carousel)`
  .carousel-nav-arrow-button {
    position: absolute;
    background: transparent;
    border: 0;
    z-index: 1;
    top: 50%;
    transform: translate(0, -50%);
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
    padding: ${(props) => `${props.theme.spacing.ELEM_SPACING.LRG} 14px`};
    background-color: ${(props) => props.theme.colorPalette.white};
    cursor: pointer;
  }
  .carousel-nav-next-button {
    right: 0;
  }
  .carousel-nav-prev-button {
    left: 0;
    transform: rotate(180deg) translate(0, 50%);
  }
  .slick-dots {
    bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }
  .slick-slide {
    position: relative;
  }

  .slick-list,
  .slick-list img {
    min-height: 200px;
    max-height: 373px;

    @media ${(props) => props.theme.mediaQuery.medium} {
      min-height: 300px;
      max-height: 406px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      min-height: 300px;
      max-height: 695px;
    }
  }
`;

export default css`
  ${(props) =>
    props.isBorderWidth
      ? `
    margin-left:0;
    margin-right:0;
    width:100%;`
      : ``}
  .medium_text_regular {
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs20};
    }
  }

  .carousel-image-layout {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .carousel-cta-button {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .tcp_carousel_wrapper {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .carousel-cta-button-with-eybrow-img {
    margin-bottom: 0;
  }

  .carousel-cta-link {
    position: absolute;
    left: 50%;
    transform: translate(-50%, 0);
    bottom: 54px;
  }
  .button-list-container-alternate {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  h3.link-text {
    margin-block-start: 0;
    margin-block-end: 0;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .small-composite-image-container {
    @media ${(props) => props.theme.mediaQuery.large} {
      position: relative;
    }
  }

  .small-composite-image-1 {
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.large} {
      position: absolute;
      bottom: 0;
    }
  }
  .small-composite-image {
    text-align: center;
  }
  .promo-area-image-link {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    line-height: 0;
    display: block;
  }

  .image-comp {
    color: ${(props) => props.theme.colorPalette.gray[900]};
  }

  .link-button-wrapper-class {
    color: ${(props) => props.theme.colorPalette.gray[900]};
  }

  .small-composite-image img {
    max-height: 164px;

    @media ${(props) => props.theme.mediaQuery.medium} {
      max-height: 225px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      max-height: 248px;
    }
  }

  .promo-area-image-link-spaced {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .module-e-divider {
    width: 100%;
    height: 6px;
    background-color: ${(props) => props.theme.colorPalette.gray[500]};

    @media ${(props) => props.theme.mediaQuery.medium} {
      height: 12px;
    }
  }
  .module-e-divider-bottom {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .module-e-img-full-width {
    width: 100%;
  }

  &.brand-tcp.page-home .module-e-header-text {
    margin-top: 15px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: -18px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: -24px;
    }
    &.mobile-header-text {
      display: none;
      @media ${(props) => props.theme.mediaQuery.smallMax} {
        display: block;
        width: 100%;
        margin-bottom: 10px;
        margin-top: 10px;
      }
    }
  }

  .module-e-header-text .link-text {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};

    @media ${(props) => props.theme.mediaQuery.smallMax} {
      width: 70%;
      margin: 0 auto;
    }
  }

  .mobile-header-text .link-text {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      width: 100%;
    }
  }

  &.brand-tcp.page-home .link-text .large_text_black {
    font-family: TofinoCond;
    font-weight: 700;
    line-height: normal;
    font-size: 88px;

    @media ${(props) => props.theme.mediaQuery.smallMax} {
      font-size: 48px;
      line-height: 0.6;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      font-size: 64px;
    }
  }

  &.brand-tcp.page-home .percentage_all_wrapped_normal_tab {
    font-family: TofinoWide;
  }

  &.brand-tcp.page-home .medium_text_regular_tab {
    font-family: Nunito;
    font-weight: 400;
    letter-spacing: normal;
    font-size: 18px;

    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      font-size: 22px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 36px;
      margin-top: -10px;
    }
  }

  &.brand-tcp.page-home .medium_text_regular {
    font-family: TofinoWide;
    letter-spacing: normal;
  }
  &.brand-tcp.page-home {
    .promo-text.percentage_all_wrapped_normal_tab {
      font-family: TofinoWide;
      font-weight: 900;
      line-height: 0.9;
      margin-top: 0;
      margin-bottom: 8px;
    }
    .percentage_all_wrapped_normal_tab-0 {
      font-size: 62px;
      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: 88px;
      }
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        font-size: 64px;
      }
    }
    .percentage_all_wrapped_normal_tab-1 {
      font-size: 36px;
      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: 48px;
      }
    }
    .percentage_all_wrapped_normal_tab-2 {
      font-size: 18px;
      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: 24px;
      }
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallMax} {
    .carousel-cta-button-with-eybrow-img {
      border-bottom: 0;
    }

    ${(props) =>
      props.headerText2
        ? `
        .button-list-container-alternate {
          margin-bottom:10px;
        }
        `
        : ''}
  }
  @media ${(props) => props.theme.mediaQuery.medium} {
    .button-list-container-alternate {
      padding-bottom: 0;
      padding-left: 12px;
    }
    .carousel-cta-button-with-eybrow-img {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
    .stacked-button-list-wrapper .stacked-button,
    .scroll-button-list-wrapper .scroll-button {
      min-width: 134px;
      margin-left: 15px;
      &:nth-child(1) {
        margin-left: 0;
      }
      span {
        width: 134px;
        padding-right: 4px;
        padding-left: 4px;
      }
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .promo-area-image-link {
      margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0;
    }

    .button-list-container-alternate {
      margin-bottom: 0;
      padding-left: 0;
    }

    .small-composite-image-0 {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }

    .stacked-button-list-wrapper .stacked-button,
    .scroll-button-list-wrapper .scroll-button {
      min-width: 142px;
      margin-left: 15px;
      margin-right: 0;
      padding: 0;
      &:nth-child(1) {
        margin-left: 0;
      }
      button {
        width: 142px;
      }
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .scroll-button-list-wrapper .scroll-button,
    .stacked-button-list-wrapper .stacked-button .cta-button-text {
      max-width: 174px;
      span {
        width: 174px;
      }
    }
  }
`;

export { StyledCarousal as Carousel };
