/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import isEmpty from 'lodash/isEmpty';
import ButtonList from '../../ButtonList';
import Carousel from '../../Carousel';
import { DamImage, Anchor } from '../../../atoms';
import LinkText from '../../LinkText';
import PromoBanner from '../../PromoBanner';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';
import colors from '../../../../../../styles/themes/TCP/colors';
import { safelyParseJSON } from '../../../../../utils/utils';
import { getLocator } from '../../../../../utils';
import {
  getScreenWidth,
  LAZYLOAD_HOST_NAME,
  styleOverrideEngine,
  getHeaderBorderRadius,
  getMediaBorderRadius,
  getCtaBorderRadius,
} from '../../../../../utils/index.native';
import {
  Container,
  ContainerView,
  HeaderWrapper,
  HeaderView,
  EyeBrowContainer,
  StyledImage,
  TopPromoWrapper,
  StyledButton,
  // Border,
  ImageContainer,
  StyledAnchor,
  ImageWrapper,
  HeaderViewContainer,
  StackCTAButtonWrapper,
  PromoAreaWrapper,
  BorderTopAndBottom,
  FloatingButton,
  StackCTAWrapper,
  CompositeImageHeaderView,
} from '../styles/ModuleE.style.native';
import config from '../moduleE.config';

/**
 * Module height and width.
 * Height is fixed for mobile : TCP & Gymb
 * Width can vary as per device width.
 */

const MODULE_DEFAULT_HEIGHT = 350;
const MODULE_WIDTH = getScreenWidth();
const MODULE_EYEBROW_WIDTH = 117;
const MODULE_EYEBROW_HEIGHT = 18;
const MODULE_PROMO_EYEBROW_WIDTH = getScreenWidth() - 234;
const PROMO_IMAGE_HEIGHT = '67px';

/**
 * These are button width.
 */
const buttonWidth = 164;

// TODO: keys will be changed once we get the actual data from CMS
const { ctaTypes, IMG_DATA } = config;

/**
 * These are button width.
 */

const BUTTON_VARIATION_FULLWIDTH = 'fullwidth';
// const BUTTON_VARIATION_FULLWIDTH_MARGIN = 'fullwidth_margin';

/**
 * @param {object} props : Props for Module E multi type of banner list, button list, header text.
 * @desc This is Module E global component. It has capability to display
 * featured content module with  tiles ,links and a CTA Button list.
 * Author can surface teaser content leading to corresponding pages.
 * To manage the TCP And Gymboree View .
 */

class ModuleE extends React.PureComponent {
  constructor(props) {
    super(props);
    const { isHpNewLayoutEnabled } = this.props;
    this.state = {
      curCarouselIndex: 0,
    };
    this.computedModuleWidth = isHpNewLayoutEnabled
      ? MODULE_WIDTH - 2 * HP_NEW_LAYOUT.BODY_PADDING
      : MODULE_WIDTH;
  }

  /**
   * This renderButton method return the button according button variation .
   *  @naviagtion is used to navigate the page.
   */
  renderButton = (navigation, buttonVariation) => {
    const { curCarouselIndex } = this.state;
    const { largeCompImageSimpleCarousel } = this.props;
    const {
      singleCTAButton: { text, url },
    } = largeCompImageSimpleCarousel[curCarouselIndex];

    if (buttonVariation === BUTTON_VARIATION_FULLWIDTH) {
      return (
        <StackCTAButtonWrapper marginTop="0">
          <StyledButton text={text} url={url} navigation={navigation} noBorderBottom />
        </StackCTAButtonWrapper>
      );
    }

    return (
      <StackCTAWrapper>
        <StyledButton text={text} url={url} navigation={navigation} />
      </StackCTAWrapper>
    );
  };

  /**
   * To Render the Dam Image or Video Component
   */
  renderDamImage = (singleCTAButton, imgData, videoData, navigation, borderWidth = 0) => {
    const damImageComp = (
      <DamImage
        width={this.computedModuleWidth - borderWidth}
        videoData={videoData}
        height={MODULE_DEFAULT_HEIGHT}
        imgData={imgData}
        host={LAZYLOAD_HOST_NAME.HOME}
        crop={imgData.crop_m}
        imgConfig={IMG_DATA.carouselImgConfig[0]}
        isFastImage
        isHomePage
        link={singleCTAButton}
        navigation={navigation}
      />
    );
    if (imgData && Object.keys(imgData).length > 0) {
      return (
        <Anchor navigation={navigation} url={singleCTAButton.url}>
          {damImageComp}
        </Anchor>
      );
    }
    return videoData && Object.keys(videoData).length > 0 ? (
      <React.Fragment>{damImageComp}</React.Fragment>
    ) : null;
  };

  /**
   * This renderView method return the images .
   *  @naviagtion is used to navigate the page.
   *  @carouselCtaType is used to manage the type of carouselCtabuttons .
   */
  renderView = (item, navigation, carouselCtaType, borderWidth) => {
    const {
      item: { image, singleCTAButton, video },
    } = item;

    const videoData = video &&
      video.url && {
        ...video,
        videoWidth: this.computedModuleWidth,
        videoHeight: MODULE_DEFAULT_HEIGHT,
      };
    const imgData = image || {};
    return (
      <View>
        {this.renderDamImage(singleCTAButton, imgData, videoData, navigation, borderWidth)}
        {carouselCtaType === 'link' ? (
          <FloatingButton>
            <StyledAnchor
              text={singleCTAButton.text}
              url={singleCTAButton.url}
              navigation={navigation}
              underline
              centered
              fontSizeVariation="large"
              anchorVariation="custom"
              colorName="gray.900"
            />
          </FloatingButton>
        ) : null}
      </View>
    );
  };

  /**
   * This method return the largeCompImageSimpleCarousel .
   *  @naviagtion is used to navigate the page.
   *  @carouselCtaType is used to manage the type of carouselCtabuttons .
   */

  renderCarousel = (largeCompImageSimpleCarousel, navigation, carouselCtaType, borderWidth) => {
    return (
      <ContainerView marginBottom="0" marginTop="5px">
        {largeCompImageSimpleCarousel.length > 1 ? (
          <Carousel
            height={MODULE_DEFAULT_HEIGHT}
            data={largeCompImageSimpleCarousel}
            renderItem={(item) => this.renderView(item, navigation, carouselCtaType, borderWidth)}
            onSnapToItem={(index) => this.setState({ curCarouselIndex: index })}
            width={this.computedModuleWidth}
            carouselConfig={{
              autoplay: true,
            }}
            showDots
            overlap
            isModule
          />
        ) : (
          <View>
            {this.renderView(
              { item: largeCompImageSimpleCarousel[0] },
              navigation,
              carouselCtaType,
              borderWidth
            )}
          </View>
        )}
      </ContainerView>
    );
  };

  /**
   * This method return the ButtonList View according to the different variation .
   *  @ctaType are four types : 'imageCTAList' ,'stackedCTAList','scrollCTAList','linkCTAList'.
   *  @naviagtion is used to navigate the page.
   */
  renderButtonList = (
    ctaTypeValue,
    navigation,
    ctaItems,
    locator,
    color,
    ctaBorderRadiusOverride = {}
  ) => {
    return (
      ctaItems && (
        <View>
          <ButtonList
            buttonListVariation={ctaTypeValue}
            navigation={navigation}
            buttonsData={ctaItems}
            locator={locator}
            color={color}
            overrideStyle={ctaBorderRadiusOverride}
          />
        </View>
      )
    );
  };

  /**
   * This method return the headerText and promoBanner .
   *  @naviagtion is used to navigate the page.
   */

  renderHeaderPromo = (navigation, headerText, promoBanner, headerStyle, promoStyle) => {
    return (
      <HeaderWrapper>
        <HeaderView>
          {headerText && (
            <LinkText
              type="heading"
              fontFamily="primary"
              fontWeight="black"
              navigation={navigation}
              headerText={headerText}
              locator="moduleE_header_text"
              textAlign="center"
              useStyle
              renderComponentInNewLine
              color="#a87db2"
              headerStyle={headerStyle}
            />
          )}
        </HeaderView>
        <View>
          {promoBanner && (
            <PromoBanner
              promoBanner={promoBanner}
              navigation={navigation}
              locator="moduleE_promobanner_text"
              promoStyle={promoStyle}
            />
          )}
        </View>
      </HeaderWrapper>
    );
  };

  renderCompositeImageHeader = (navigation, headerText2, styleOverrides = {}) => {
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    return (
      headerText2 && (
        <HeaderWrapper>
          <CompositeImageHeaderView>
            <LinkText
              type="heading"
              fontFamily="primary"
              fontWeight="black"
              navigation={navigation}
              headerText={headerText2}
              locator="moduleE_header_text"
              textAlign="center"
              useStyle
              renderComponentInNewLine
              color="#a87db2"
              headerStyle={headerStyle}
            />
          </CompositeImageHeaderView>
        </HeaderWrapper>
      )
    );
  };

  getEyeBrowImageWidth = (eyebrow) => {
    return (
      (eyebrow &&
        eyebrow.mediaLinkedList &&
        eyebrow.mediaLinkedList[0] &&
        eyebrow.mediaLinkedList[0].image &&
        eyebrow.mediaLinkedList[0].image.width_m) ||
      MODULE_EYEBROW_WIDTH
    );
  };

  getEyeBrowImageHeight = (eyebrow) => {
    return (
      (eyebrow &&
        eyebrow.mediaLinkedList &&
        eyebrow.mediaLinkedList[0] &&
        eyebrow.mediaLinkedList[0].image &&
        eyebrow.mediaLinkedList[0].image.height_m) ||
      MODULE_EYEBROW_HEIGHT
    );
  };

  /**
   * This method return the renderEyeBrow method to manage all image .
   *  @naviagtion is used to navigate the page.
   */
  renderEyeBrow = (eyebrow, naviagtion, promoStyle) => {
    return (
      <View>
        {eyebrow && (
          <EyeBrowContainer>
            <StyledImage
              width={this.getEyeBrowImageWidth(eyebrow)}
              height={this.getEyeBrowImageHeight(eyebrow)}
              url={
                eyebrow &&
                eyebrow.mediaLinkedList &&
                eyebrow.mediaLinkedList[0] &&
                eyebrow.mediaLinkedList[0].image.url
              }
              imgConfig={IMG_DATA.eyeBrowImgConfig[0]}
              isHomePage
            />
            <TopPromoWrapper width={MODULE_PROMO_EYEBROW_WIDTH}>
              <PromoBanner
                naviagtion={naviagtion}
                promoBanner={eyebrow && eyebrow.promoBanner}
                promoStyle={promoStyle}
              />
            </TopPromoWrapper>
            <StyledImage
              width={MODULE_EYEBROW_WIDTH}
              height={MODULE_EYEBROW_HEIGHT}
              url={
                eyebrow &&
                eyebrow.mediaLinkedList &&
                eyebrow.mediaLinkedList[1] &&
                eyebrow.mediaLinkedList[1].image.url
              }
              imgConfig={IMG_DATA.eyeBrowImgConfig[0]}
              isHomePage
            />
          </EyeBrowContainer>
        )}
      </View>
    );
  };

  getDamImageHeight = (imgData) => {
    return (imgData && parseFloat(imgData.height_m)) || `${buttonWidth}px`;
  };

  getDamImageWidth = (imgData) => {
    return (imgData && parseFloat(imgData.width_m)) || `${buttonWidth}px`;
  };

  /**
   * To Render the Styled Image or Video Component
   */
  renderStyledDamImage = (link, imgData, videoData, navigation, index) => {
    const damImageComp = (
      <StyledImage
        url={imgData && imgData.url}
        videoData={videoData}
        height={this.getDamImageHeight(imgData)}
        width={this.getDamImageWidth(imgData)}
        testID={`${getLocator('moduleE_product_img')}${index}`}
        alt={imgData && imgData.alt}
        imgConfig={IMG_DATA.smallImgConfig[0]}
        isHomePage
      />
    );
    if (imgData && Object.keys(imgData).length > 0) {
      return (
        <StyledAnchor url={link ? link.url : ''} navigation={navigation} key={index.toString()}>
          {damImageComp}
        </StyledAnchor>
      );
    }
    return videoData && Object.keys(videoData).length > 0 ? (
      <React.Fragment>{damImageComp}</React.Fragment>
    ) : null;
  };

  /**
   * This method return the renderMediaLinkedImage method to manage all product image with cropping rule .
   *  @naviagtion is used to navigate the page.
   */
  smallCompositeImage = (divCTALinks, navigation, headerText2) => {
    const compositeImageFixColors = ['#e96d97', '#2dc2f5'];

    return (
      <ImageContainer headerText2={headerText2}>
        {divCTALinks.map(({ image, link, styled, video }, index) => {
          const divCtaLinkHeaderText = [{ textItems: [{ ...styled }], link }];
          const { styledObj } = styled;
          const parsedStyledObj = styledObj && safelyParseJSON(styledObj);
          const videoData = video &&
            video.url && {
              videoWidth: buttonWidth,
              videoHeight: 202,
              ...video,
            };
          return (
            <ImageWrapper tileIndex={index}>
              {this.renderStyledDamImage(link, image, videoData, navigation, index)}
              <HeaderViewContainer>
                <LinkText
                  type="heading"
                  navigation={navigation}
                  headerText={divCtaLinkHeaderText}
                  locator="moduleE_small_header_text"
                  style={
                    !isEmpty(parsedStyledObj)
                      ? parsedStyledObj
                      : {
                          color: compositeImageFixColors[index],
                        }
                  }
                  useStyle
                  renderComponentInNewLine
                />
              </HeaderViewContainer>
              <StyledButton text={link.text} navigation={navigation} url={link.url} />
            </ImageWrapper>
          );
        })}
      </ImageContainer>
    );
  };

  /**
   * This method return the PromoArea .
   */
  renderPromoArea = (linkedImage, navigation, carouselCtaType) => {
    if (linkedImage && linkedImage.length > 0) {
      return (
        <PromoAreaWrapper type={carouselCtaType}>
          {linkedImage.map(({ image, link }, index) => {
            return (
              <View>
                <StyledAnchor
                  url={link ? link.url : ''}
                  navigation={navigation}
                  key={index.toString()}
                >
                  <StyledImage
                    imgData={image}
                    height={PROMO_IMAGE_HEIGHT}
                    width={this.computedModuleWidth}
                    testID={`${getLocator('moduleE_promoarea_img')}${index}`}
                    alt={image && image.alt}
                    imgConfig={IMG_DATA.promoAreaImgConfig[0]}
                    isHomePage
                  />
                </StyledAnchor>
              </View>
            );
          })}
        </PromoAreaWrapper>
      );
    }
    return null;
  };

  /**
   * This method return the renderDivider according to pos.
   */
  renderDivider = (pos, dividerStyle = {}) => {
    const { isHpNewLayoutEnabled } = this.props;
    return (
      <BorderTopAndBottom
        style={dividerStyle}
        pos={pos}
        isHpNewLayoutEnabled={isHpNewLayoutEnabled}
      />
    );
  };

  /**
   * This method return the renderTopView method to manage all renderHeaderPromo, renderEyeBrow, renderTopAndBottomBorder .
   *  @naviagtion is used to navigate the page.
   */
  renderTopView = (eyebrow, headerText, promoBanner, navigation, styleOverrides = {}) => {
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const { isHpNewLayoutEnabled } = this.props;
    return (
      <View>
        {!eyebrow && this.renderDivider('bottom', styleOverrides['bottom-div'])}
        {eyebrow &&
          !isHpNewLayoutEnabled &&
          this.renderEyeBrow(eyebrow, navigation, styleOverrides.eyeBrow)}
        {this.renderHeaderPromo(
          navigation,
          headerText,
          promoBanner,
          headerStyle,
          styleOverrides.promo
        )}
      </View>
    );
  };

  getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  getBackgroundColorStyle = () => {
    return { backgroundColor: colors?.WHITE };
  };

  getContainerStyle = (isHpNewLayoutEnabled) =>
    isHpNewLayoutEnabled
      ? {
          ...HP_NEW_LAYOUT.MODULE_BOX_SHADOW,
        }
      : {};

  getHeaderAndPromoStyle = (isHpNewLayoutEnabled, headerBorderRadiusOverride) =>
    isHpNewLayoutEnabled
      ? {
          ...headerBorderRadiusOverride,
          ...this.getBackgroundColorStyle(),
        }
      : {};

  getMediaStyle = (isHpNewLayoutEnabled, mediaBorderRadiusOverride) =>
    isHpNewLayoutEnabled
      ? {
          ...mediaBorderRadiusOverride,
          ...this.getBackgroundColorStyle(),
        }
      : {};

  getCtaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom'])
      : {};
  };

  /**
   * This @renderTopView method return the eyebrow , headerText, promobanner, naigation according to pos.
   * This @renderPromoArea method return the linkedImage, navigation,carouselCtaType according to pos .
   * This @renderCarousel method return the largeCompImageSimpleCarousel, navigation, carouselCtaType according to pos .
   * This @renderButton method return the CTAButton, navigation, according to pos .
   * This @renderButtonList method return the ctaTypeValue, navigation, ctaItems, according to pos .
   * This @smallCompositeImage method return the divCTALinks, navigation, according to pos .
   * This @renderDivider method return the divider according to pos .
   */

  render() {
    const {
      navigation,
      largeCompImageSimpleCarousel,
      ctaItems,
      eyebrow: eyebrows,
      headerText,
      headerText2,
      promoBanner,
      divCTALinks,
      linkedImage,
      ctaType,
      carouselCtaType,
      moduleClassName,
      borderWidth,
      isHpNewLayoutEnabled,
    } = this.props;
    const eyebrow = eyebrows ? eyebrows[0] : null;
    const ctaTypeValue = ctaTypes[ctaType];
    const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleE');
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const containerStyle = this.getContainerStyle(isHpNewLayoutEnabled);
    const headerAndPromoStyle = this.getHeaderAndPromoStyle(
      isHpNewLayoutEnabled,
      headerBorderRadiusOverride
    );
    const ctaBorderRadiusOverride = this.getCtaBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const mediaWrapperStyle = this.getMediaStyle(isHpNewLayoutEnabled, mediaBorderRadiusOverride);
    return (
      <Container style={containerStyle}>
        <View style={headerAndPromoStyle}>
          {this.renderTopView(eyebrow, headerText, promoBanner, navigation, styleOverrides)}
          {linkedImage && this.renderPromoArea(linkedImage, navigation, carouselCtaType)}
        </View>
        <View style={mediaWrapperStyle}>
          {largeCompImageSimpleCarousel &&
            this.renderCarousel(
              largeCompImageSimpleCarousel,
              navigation,
              carouselCtaType,
              borderWidth
            )}
          {carouselCtaType === 'button' && !eyebrow
            ? this.renderButton(navigation, BUTTON_VARIATION_FULLWIDTH)
            : null}
          {carouselCtaType === 'button' && eyebrow
            ? this.renderButton(navigation, BUTTON_VARIATION_FULLWIDTH)
            : null}
          {ctaItems &&
            this.renderButtonList(
              ctaTypeValue,
              navigation,
              ctaItems,
              'moduleE_cta_button',
              'gray',
              ctaBorderRadiusOverride
            )}
          {this.renderCompositeImageHeader(navigation, headerText2, styleOverrides)}
          {this.smallCompositeImage(divCTALinks, navigation, headerText2)}
          {!eyebrow && this.renderDivider('top', styleOverrides['top-div'])}
        </View>
      </Container>
    );
  }
}

ModuleE.defaultProps = {
  eyebrow: null,
  headerText: [],
  headerText2: [],
  promoBanner: null,
  navigation: {},
  ctaItems: [],
  largeCompImageSimpleCarousel: [],
  divCTALinks: [],
  linkedImage: [],
  ctaType: '',
  carouselCtaType: '',
  moduleClassName: '',
  borderWidth: 0,
};

ModuleE.propTypes = {
  largeCompImageSimpleCarousel: PropTypes.shape([]),
  headerText: PropTypes.shape([]),
  headerText2: PropTypes.shape([]),
  promoBanner: PropTypes.shape([]),
  ctaItems: PropTypes.shape([]),
  navigation: PropTypes.shape({}),
  eyebrow: PropTypes.shape([]),
  divCTALinks: PropTypes.shape([]),
  linkedImage: PropTypes.shape([]),
  ctaType: PropTypes.string,
  carouselCtaType: PropTypes.string,
  moduleClassName: PropTypes.string,
  borderWidth: PropTypes.number,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleE;
export { ModuleE as ModuleEVanilla };
