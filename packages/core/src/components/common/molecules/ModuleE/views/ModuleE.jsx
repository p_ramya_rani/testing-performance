/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import ButtonList from '@tcp/core/src/components/common/molecules/ButtonList';
import { Col, DamImage, Row, Button, Anchor } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  getLocator,
  getIconPath,
  isGymboree,
  styleOverrideEngine,
  getBrand,
  configureInternalNavigationFromCMSUrl,
  isClient,
} from '@tcp/core/src/utils';
import { getHeaderBorderRadius, getMediaBorderRadius } from '@tcp/core/src/utils/utils.web';
import moduleEStyle, { Carousel } from '../styles/ModuleE.style';
import config from '../moduleE.config';

const bigCarrotIcon = 'carousel-big-carrot';
const bigCarrotIconGym = 'carousel-big-carrot-white';

const getHeaderBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
    : {};
};

const getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};

// Carousel Next and Prev Arrows
const CarouselArrow = ({ arrowType, label, onClick }) => {
  return (
    <button
      type="button"
      tabIndex="0"
      aria-label={label}
      data-locator={`moduleE_${arrowType}_arrow`}
      className={`carousel-nav-arrow-button carousel-nav-${arrowType}-button`}
      onClick={onClick}
    >
      <svg width="15" height="52" role="img">
        <path
          fill="#1A1A1A"
          d="M.113 50.539a1 1 0 001.774.923l13-25a1 1 0 000-.923l-13-25a1 1 0 10-1.774.923L12.873 26 .113 50.54z"
        />
      </svg>
    </button>
  );
};

CarouselArrow.propTypes = {
  arrowType: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

// Build Top and Bottom divider
const Divider = ({ position, disabled, dividerStyle }) => {
  return disabled ? null : (
    <Col
      colSize={{ small: 6, medium: 8, large: 12 }}
      ignoreGutter={{ small: true, medium: true, large: true }}
    >
      <Row fullBleed={{ small: true, medium: true, large: true }}>
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <div style={dividerStyle} className={`module-e-divider module-e-divider-${position}`} />
        </Col>
      </Row>
    </Col>
  );
};
Divider.defaultProps = {
  dividerStyle: {},
};
Divider.propTypes = {
  position: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  dividerStyle: PropTypes.shape({}),
};

/* Returns Promo Area Image. */
const getLinkedImage = (linkedImage, carouselCtaType, IMG_DATA, isHomePage) => {
  if (!linkedImage) {
    return null;
  }
  const to = configureInternalNavigationFromCMSUrl(linkedImage.link.url);
  return (
    <Anchor
      dataLocator={`${getLocator('moduleE_promo_area_img_cta')}`}
      className={`promo-area-image-link ${
        carouselCtaType === 'button' ? 'promo-area-image-link-spaced' : ''
      }`}
      to={to}
      asPath={linkedImage.link.url}
      target={linkedImage.link.target}
      title={linkedImage.link.title}
    >
      <DamImage
        imgConfigs={IMG_DATA.promoAreaImgConfig}
        imgData={linkedImage.image}
        videoData={linkedImage.video}
        dataLocator={`${getLocator('moduleE_promo_area_img')}`}
        className="module-e-img-full-width"
        isHomePage={isHomePage}
        isOptimizedVideo
        lazyLoad
        isModule
      />
    </Anchor>
  );
};

/* Null check for eyebrow object  */
const getEyebrowObj = (eyebrow) => {
  return (eyebrow && eyebrow[0]) || {};
};

const getImage = (imgParams) => {
  const {
    image,
    video,
    singleCTAButton,
    IMG_DATA,
    carouselCtaType,
    index,
    VIDEO_DATA,
    isHomePage,
    elementClass = '',
  } = imgParams;
  const { url, text, target } = singleCTAButton || {};
  const damConfigs =
    video && Object.keys(video).length > 0
      ? VIDEO_DATA.carouselVideoConfig
      : IMG_DATA.carouselImgConfig;

  return (
    <div className={elementClass}>
      <DamImage
        imgConfigs={damConfigs}
        imgData={image}
        dataLocator={`${getLocator('moduleE_carousel_image_img')}${index + 1}`}
        className="module-e-img-full-width"
        link={singleCTAButton}
        videoData={video}
        isHomePage={isHomePage}
        isOptimizedVideo
        lazyLoad
        isModule
      />
      {carouselCtaType === 'link' && singleCTAButton && (
        <Anchor
          dataLocator={`${getLocator('moduleE_carousel_image_link_cta')}${index + 1}`}
          className="carousel-cta-link"
          url={url}
          title={text}
          target={target}
          underline
          anchorVariation="primary"
          fontSizeVariation="large"
        >
          {text}
        </Anchor>
      )}
    </div>
  );
};
const renderItem = (
  largeCompImageSimpleCarousel,
  IMG_DATA,
  VIDEO_DATA,
  carouselCtaType,
  isHomePage,
  elementClass
) =>
  largeCompImageSimpleCarousel.map(({ image, video, singleCTAButton }, index) => {
    return getImage({
      image,
      video,
      singleCTAButton,
      IMG_DATA,
      carouselCtaType,
      index,
      VIDEO_DATA,
      isHomePage,
      elementClass,
    });
  });

const getModuleECarousel = (
  largeCompImageSimpleCarousel,
  CAROUSEL_OPTIONS,
  carouselConfig,
  IMG_DATA,
  carouselCtaType,
  VIDEO_DATA,
  { isHomePage, sliderFadeOptions }
) => {
  const overrideCarouselOptions = !isEmpty(sliderFadeOptions)
    ? {
        fade: true,
        cssEase: 'linear',
        infinite: true,
        autoplaySpeed: parseFloat(sliderFadeOptions.speed),
        speed: 50,
      }
    : {};
  return (
    <Carousel
      options={{ ...CAROUSEL_OPTIONS, ...overrideCarouselOptions }}
      carouselConfig={carouselConfig}
      isModule
    >
      {renderItem(largeCompImageSimpleCarousel, IMG_DATA, VIDEO_DATA, carouselCtaType, isHomePage)}
    </Carousel>
  );
};

const getCarouselOrImage = (
  largeCompImageSimpleCarousel,
  CAROUSEL_OPTIONS,
  carouselConfig,
  IMG_DATA,
  carouselCtaType,
  VIDEO_DATA,
  { isHomePage, sliderFadeOptions }
) => {
  const { image, video, singleCTAButton } = largeCompImageSimpleCarousel[0] || {};
  return largeCompImageSimpleCarousel.length > 1
    ? getModuleECarousel(
        largeCompImageSimpleCarousel,
        CAROUSEL_OPTIONS,
        carouselConfig,
        IMG_DATA,
        carouselCtaType,
        VIDEO_DATA,
        { isHomePage, sliderFadeOptions }
      )
    : getImage({
        image,
        video,
        singleCTAButton,
        IMG_DATA,
        carouselCtaType,
        index: 0,
        VIDEO_DATA,
        isHomePage,
      });
};

const ctaItemsClient = (ctaItems) => {
  return ctaItems && isClient();
};
const renderSmallCompositeImageHeader = (headerText2, headerStyle) => {
  return headerText2 ? (
    <Row
      fullBleed={{
        small: false,
        medium: false,
        large: true,
      }}
    >
      {/* ---------- Header Text Start ----------- */}
      <LinkText
        className="module-e-header-text mobile-header-text"
        headerText={headerText2}
        component="h2"
        type="heading"
        dataLocator={getLocator('moduleE_header_text')}
        headerStyle={headerStyle}
      />
    </Row>
  ) : null;
};

const ModuleE = (props) => {
  const {
    className,
    eyebrow,
    headerText,
    headerText2,
    promoBanner,
    largeCompImageSimpleCarousel,
    ctaItems,
    ctaType,
    carouselCtaType,
    divCTALinks,
    linkedImage: linkedImages,
    accessibility: { playIconButton, pauseIconButton, previousButton, nextIconButton } = {},
    moduleClassName,
    page,
    isHomePage,
    isHpNewDesignCTAEnabled,
    isHpNewModuleDesignEnabled,
  } = props;

  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleE');
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];

  const sliderFadeOptions = styleOverrides['slider-fade'];
  const linkedImage = linkedImages && linkedImages[0];
  const { mediaLinkedList: eyebrowMediaLinkedList, promoBanner: eyebrowPromoBanner } =
    getEyebrowObj(eyebrow);

  const { ctaTypes, IMG_DATA, CAROUSEL_OPTIONS, VIDEO_DATA } = config;
  const buttonListCtaType = ctaTypes[ctaType];
  const carouselIcon = isGymboree() ? bigCarrotIconGym : bigCarrotIcon;
  const [curCarouselSlideIndex, setCurCarouselSlideIndex] = useState(0);
  const carouselConfig = {
    autoplay: true,
    dataLocatorPlay: getLocator('moduleE_play_button'),
    dataLocatorPause: getLocator('moduleE_pause_button'),
    customArrowLeft: getIconPath(carouselIcon),
    customArrowRight: getIconPath(carouselIcon),
    dataLocatorCarousel: 'carousel_banner',
  };
  const headerBorderRadiusOverride = getHeaderBorderRadiusOverride(
    isHpNewModuleDesignEnabled,
    styleOverrides
  );
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewModuleDesignEnabled,
    styleOverrides
  );

  CAROUSEL_OPTIONS.prevArrow = <CarouselArrow arrowType="prev" label={previousButton} />;
  CAROUSEL_OPTIONS.nextArrow = <CarouselArrow arrowType="next" label={nextIconButton} />;
  carouselConfig.autoplay = carouselConfig.autoplay && largeCompImageSimpleCarousel.length > 1;
  carouselConfig.pauseIconButtonLabel = pauseIconButton;
  carouselConfig.playIconButtonLabel = playIconButton;

  CAROUSEL_OPTIONS.afterChange = (index) => {
    setCurCarouselSlideIndex(index);
  };

  return (
    <Row
      className={`${className} ${moduleClassName} brand-${getBrand()} page-${page}`}
      fullBleed={{ small: true, medium: true }}
    >
      <Divider disabled={!!eyebrow} position="top" dividerStyle={styleOverrides['top-div']} />
      <Col
        colSize={{
          small: 6,
          medium: 8,
          large: 12,
        }}
        ignoreGutter
      >
        {/* ---------- Eyebrow Image Start ----------- */}
        {eyebrow && (
          <Row fullBleed>
            <Col
              colSize={{
                small: 2,
                medium: 3,
                large: 5,
              }}
              ignoreGutter
            >
              <DamImage
                imgConfigs={IMG_DATA.eyeBrowImgConfig}
                imgData={eyebrowMediaLinkedList[0].image}
                dataLocator={`${getLocator('moduleE_eyebrowImage_img')}_left`}
                link={eyebrowMediaLinkedList[0].link}
                className="module-e-img-full-width"
                videoData={eyebrowMediaLinkedList[0].video}
                isHomePage={isHomePage}
                isOptimizedVideo
                lazyLoad
                isModule
              />
            </Col>
            <Col colSize={{ small: 2, medium: 2, large: 2 }} ignoreGutter>
              <PromoBanner
                promoBanner={eyebrowPromoBanner}
                dataLocator={getLocator('moduleE_eyebrow_text')}
                promoStyle={styleOverrides.eyeBrow}
              />
            </Col>
            <Col colSize={{ small: 2, medium: 3, large: 5 }} ignoreGutter>
              <DamImage
                imgConfigs={IMG_DATA.eyeBrowImgConfig}
                imgData={eyebrowMediaLinkedList[1].image}
                dataLocator={`${getLocator('moduleE_eyebrowImage_img')}_right`}
                link={eyebrowMediaLinkedList[1].link}
                className="module-e-img-full-width"
                isHomePage={isHomePage}
                isOptimizedVideo
                videoData={eyebrowMediaLinkedList[1].video}
                lazyLoad
                isModule
              />
            </Col>
          </Row>
        )}
        {/* ---------- Eyebrow Image End ----------- */}
        <div style={headerBorderRadiusOverride}>
          {/* ---------- Header Text Start ----------- */}
          <LinkText
            className="module-e-header-text"
            headerText={headerText}
            component="h2"
            type="heading"
            dataLocator={getLocator('moduleE_header_text')}
            headerStyle={headerStyle}
          />
          {/* ---------- Header Text End ----------- */}
          {/* ---------- Promo Text Start ----------- */}
          {promoBanner && (
            <PromoBanner
              promoBanner={promoBanner}
              dataLocator={getLocator('moduleE_promo_text')}
              promoStyle={styleOverrides.promo}
            />
          )}
          {/* ---------- Promo Text End ----------- */}
        </div>
        {/* ----------- Promo Image Area Start----------- */}
        {getLinkedImage(linkedImage, carouselCtaType, IMG_DATA, isHomePage)}
        {/* ---------- Carousel Image layout Start ----------- */}
        <Row fullBleed className="carousel-image-layout" style={mediaBorderRadiusOverride}>
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: 8,
            }}
          >
            {getCarouselOrImage(
              largeCompImageSimpleCarousel,
              CAROUSEL_OPTIONS,
              carouselConfig,
              IMG_DATA,
              carouselCtaType,
              VIDEO_DATA,
              { isHomePage, sliderFadeOptions }
            )}
            {carouselCtaType === 'button' &&
              largeCompImageSimpleCarousel[curCarouselSlideIndex].singleCTAButton && (
                <Row
                  fullBleed={{
                    small: !!eyebrow,
                    medium: false,
                    large: true,
                  }}
                >
                  <Col
                    colSize={{
                      small: 6,
                      medium: 8,
                      large: 12,
                    }}
                  >
                    <Button
                      buttonVariation="fixed-width"
                      dataLocator={`${getLocator(
                        'moduleE_carousel_image_button_cta'
                      )}${curCarouselSlideIndex}`}
                      className={`carousel-cta-button ${
                        eyebrow ? 'carousel-cta-button-with-eybrow-img' : ''
                      }`}
                      cta={largeCompImageSimpleCarousel[curCarouselSlideIndex].singleCTAButton}
                      isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
                    >
                      {largeCompImageSimpleCarousel[curCarouselSlideIndex].singleCTAButton.text}
                    </Button>
                  </Col>
                </Row>
              )}
            {ctaItemsClient(ctaItems) && (
              <ButtonList
                buttonsData={ctaItems}
                buttonListVariation={buttonListCtaType}
                dataLocatorDivisionImages={getLocator('moduleE_cta_image')}
                dataLocatorTextCta={getLocator('moduleE_cta_links')}
                dataLocatorDropDown={getLocator('moduleE_dropdown')}
                className="button-list-container-alternate"
                isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
              />
            )}
          </Col>
          {/* ---------- Small Composite Image Start ----------- */}
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: 4,
            }}
            className="small-composite-image-container"
          >
            {/* ---------- show second header for mobile view only ----------- */}
            {renderSmallCompositeImageHeader(headerText2, headerStyle)}
            <Row
              fullBleed={{
                small: false,
                medium: false,
                large: true,
              }}
            >
              {divCTALinks.map(({ image, link, video, styled }, index) => {
                const divCtaLinkHeaderText = [{ textItems: [{ ...styled }], link }];

                return (
                  <Col
                    colSize={{
                      small: 3,
                      medium: 4,
                      large: 12,
                    }}
                    ignoreGutter={{
                      large: true,
                    }}
                  >
                    <div className={`small-composite-image small-composite-image-${index}`}>
                      <DamImage
                        imgConfigs={video ? IMG_DATA.smallVidConfig : IMG_DATA.smallImgConfig}
                        imgData={image}
                        dataLocator={`${getLocator('moduleE_small_img')}${index + 1}`}
                        className="module-e-img-full-width"
                        link={link}
                        videoData={video}
                        isHomePage={isHomePage}
                        lazyLoad
                        isModule
                      />
                      <LinkText
                        className="small-composite-image-header"
                        headerText={divCtaLinkHeaderText}
                        component="h3"
                        type="heading"
                        dataLocator={`${getLocator('moduleE_small_img_header_text')}${index + 1}`}
                      />
                      <Button
                        buttonVariation="variable-width"
                        dataLocator={`${getLocator('moduleE_small_img_cta')}${index + 1}`}
                        className="carousel-cta"
                        fullWidth={false}
                        cta={link}
                        isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
                      >
                        {link.text}
                      </Button>
                    </div>
                  </Col>
                );
              })}
            </Row>
          </Col>
          {/* ---------- Small Composite Image End ----------- */}
        </Row>
        {/* ---------- Carousel Image layout End ----------- */}
      </Col>
      <Divider disabled={eyebrow} position="bottom" dividerStyle={styleOverrides['bottom-div']} />
    </Row>
  );
};

ModuleE.defaultProps = {
  className: '',
  promoBanner: null,
  eyebrow: null,
  ctaType: 'stackedCTAButtons',
  ctaItems: [],
  linkedImage: null,
  moduleClassName: '',
  page: '',
  headerText2: '',
};

ModuleE.propTypes = {
  className: PropTypes.string,
  accessibility: PropTypes.shape({}).isRequired,
  headerText: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  promoBanner: PropTypes.arrayOf(PropTypes.shape({})),
  headerText2: PropTypes.arrayOf(PropTypes.shape({})),
  eyebrow: PropTypes.arrayOf(PropTypes.shape({})),
  largeCompImageSimpleCarousel: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  divCTALinks: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  linkedImage: PropTypes.arrayOf(PropTypes.shape({})),
  ctaItems: PropTypes.arrayOf(PropTypes.object),
  ctaType: PropTypes.oneOf(['stackedCTAButtons', 'linkCTAList', 'scrollCTAList', 'imageCTAList']),
  carouselCtaType: PropTypes.oneOf(['button', 'link']).isRequired,
  moduleClassName: PropTypes.string,
  page: PropTypes.string,
  isHomePage: PropTypes.bool.isRequired,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

export default withStyles(ModuleE, moduleEStyle);
export { ModuleE as ModuleEVanilla };
