// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, DamImage, Anchor } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import {
  createUUID,
  styleOverrideEngine,
  isClient,
  getViewportInfo,
} from '@tcp/core/src/utils/index';
import { getCtaBorderRadius, getByomHeadereBorderRadius } from '@tcp/core/src/utils/utils.web';
import TextComponent from '../molecules/TextComponent';
import BodyCopyStyles from '../../PageSlots/Border/styles/Border.style';
import ContainerStyles, {
  StyledButton,
  StyledColumn,
  StyledContainer,
  StyledCTAItems,
  StyledCTAContainer,
  StyledCTAColumn,
} from '../styles/ModuleContainer.style';
import CTAItems from '../molecules/CTAItems';
import ImageComponent from '../molecules/ImageComponent';

const SPACINGM = 'spacingM';
const SPACINGT = 'spacingT';
const SPACINGD = 'spacingD';
const VARIATION_OVERLAY = 'overlay';

const BORDER_RADIUS_OVERRIDE_MAP = {
  column_1: ['col1-cta-top', 'col1-cta-bottom'],
  column_2: ['col2-cta-top', 'col2-cta-bottom'],
  column_3: ['col3-cta-top', 'col3-cta-bottom'],
  column_4: ['col4-cta-top', 'col4-cta-bottom'],
  column_5: ['col5-cta-top', 'col5-cta-bottom'],
};

const HEADER_BORDER_RADIUS_OVERRIDE_MAP = {
  column_1: [
    'col1-header1-top-left',
    'col1-header1-top-right',
    'col1-header1-bottom-left',
    'col1-header1-bottom-right',
  ],
  column_2: [
    'col1-header2-top-left',
    'col1-header2-top-right',
    'col1-header2-bottom-left',
    'col1-header2-bottom-right',
  ],
  column_3: [
    'col1-header3-top-left',
    'col1-header3-top-right',
    'col1-header3-bottom-left',
    'col1-header3-bottom-right',
  ],
  column_4: [
    'col1-header4-top-left',
    'col1-header4-top-right',
    'col1-header4-bottom-left',
    'col1-header4-bottom-right',
  ],
};

const getCtaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides, overrideMap) => {
  if (isHpNewModuleDesignEnabled && styleOverrides && overrideMap) {
    return getCtaBorderRadius(styleOverrides[overrideMap[0]], styleOverrides[overrideMap[1]]);
  }
  return {};
};
const getHeaderBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides, overrideMap) => {
  if (isHpNewModuleDesignEnabled && styleOverrides && overrideMap) {
    return getByomHeadereBorderRadius(
      styleOverrides[overrideMap[0]],
      styleOverrides[overrideMap[1]],
      styleOverrides[overrideMap[2]],
      styleOverrides[overrideMap[3]]
    );
  }
  return {};
};

const renderStyledText = (styledProps) => {
  if (!styledProps) return null;
  const { text, style } = styledProps;
  return <BodyCopy className={style}>{text}</BodyCopy>;
};

const renderHeaderTextLine = (headerTextLine, otherProps, key) => {
  if (!headerTextLine || !headerTextLine.length) return null;
  const { navigation } = otherProps;
  return headerTextLine.map((headerTextItem, index) => (
    // eslint-disable-next-line react/no-array-index-key
    <TextComponent
      headerTextLine={headerTextItem}
      navigation={navigation}
      // eslint-disable-next-line react/no-array-index-key
      key={`${key}-${index}`}
    />
  ));
};

const renderCTAItem = (ctaItem) => {
  if (!ctaItem) return null;
  const { button } = ctaItem;
  return (
    <StyledButton buttonVariation="fixed-width" cta={button}>
      {button.text}
    </StyledButton>
  );
};

const renderCTAItemComposite = (ctaItems, otherProps) => {
  const { styleOverrides, isHpNewModuleDesignEnabled, colName } = otherProps;
  const { isMobile } = isClient() && getViewportInfo();
  const ctaItemFilteredObj = ctaItems && ctaItems.filter((item) => item.button);
  const ctaTypeValue = ctaItemFilteredObj[0].ctaType;
  const ctaBgImageFilteredObj = ctaItems && ctaItems.filter((item) => item.ctaBackgroundImage);
  const ctaBgImage = ctaBgImageFilteredObj.length !== 0 && ctaBgImageFilteredObj[0];
  const ctaBorderRadiusOverride = !isMobile
    ? getCtaBorderRadiusOverride(
        isHpNewModuleDesignEnabled,
        styleOverrides,
        BORDER_RADIUS_OVERRIDE_MAP[colName]
      )
    : {};
  const navBgStyleOverrides = styleOverrides['nav-bg'] || {};

  return ctaItemFilteredObj ? (
    <StyledCTAContainer
      className="styled-cta-container"
      ctaBgImageObj={ctaBgImage}
      ctaTypeValue={ctaTypeValue}
      style={Object.assign(navBgStyleOverrides, ctaBorderRadiusOverride)}
    >
      <StyledCTAItems
        className="styled-cta-items"
        ctaTypeValue={ctaTypeValue}
        ctaEnableVariation={ctaItemFilteredObj}
        style={Object.assign(navBgStyleOverrides, ctaBorderRadiusOverride)}
      >
        <CTAItems ctaItems={ctaItemFilteredObj} styleOverrides={styleOverrides} {...otherProps} />
      </StyledCTAItems>
    </StyledCTAContainer>
  ) : null;
};

const renderLargeCompImageSimpleCarousel = (largeCompImageSimpleCarousel, otherProps) => {
  return largeCompImageSimpleCarousel ? (
    <ImageComponent largeCompImageSimpleCarousel={largeCompImageSimpleCarousel} {...otherProps} />
  ) : null;
};

const renderCompositeData = (composite, composites, otherProps, key) => {
  switch (composite) {
    case 'headerTextLine':
      return (
        composites.headerTextLine &&
        renderHeaderTextLine(composites.headerTextLine, otherProps, key)
      );
    case 'ctaItem':
      return composites.ctaItem && renderCTAItem(composites.ctaItem);
    case 'ctaItemsComposite':
      return composites.ctaItemsComposite
        ? renderCTAItemComposite(composites.ctaItemsComposite, otherProps)
        : null;
    case 'largeCompImageSimpleCarouselComposite':
      return composites.largeCompImageSimpleCarouselComposite
        ? renderLargeCompImageSimpleCarousel(
            composites.largeCompImageSimpleCarouselComposite,
            otherProps
          )
        : null;
    default:
      return <></>;
  }
};

const renderComposites = (composites, otherProps) => {
  const compositeKeys = composites && Object.keys(composites);
  return (
    compositeKeys &&
    compositeKeys.map((composite, index) => {
      const key = `${composite}-${index}`;
      return renderCompositeData(composite, composites, otherProps, key);
    })
  );
};

// const renderRichText = richText => {
//   const { text: html } = richText;
//   return (
//     <View>
//       <Espot richTextHtml={html} routeName="Home" isNativeView={false} />
//     </View>
//   );
// };

const renderButton = (buttonProps) => {
  if (!buttonProps) return null;
  const { text } = buttonProps;
  return (
    <StyledButton buttonVariation="fixed-width" cta={buttonProps}>
      {text}
    </StyledButton>
  );
};

const renderImage = (imageProps) => {
  if (!imageProps) return null;
  return <DamImage imgData={imageProps} width="100%" />;
};

const renderVideo = (videoProps) => {
  if (!videoProps) return null;
  return <DamImage videoData={videoProps} isHomePage isOptimizedVideo />;
};

const renderLink = (link) => {
  return link && <Anchor accessibilityLabel={link.text} url={link && link.url} text={link.text} />;
};

const renderColumnData = (colKey, column, otherProps) => {
  switch (colKey) {
    case 'styled':
      return renderStyledText(column.styled);
    case 'button':
      return renderButton(column.button);
    case 'image':
      return renderImage(column.image);
    case 'video':
      return renderVideo(column.video);
    case 'link':
      return renderLink(column.link);
    case 'composites':
      return renderComposites(column.composites, otherProps);
    // case 'richText':
    //   return renderRichText(column.richText);
    case 'set':
    default:
      return <></>;
  }
};

const getColumnSpacing = (columnSet) => {
  return {
    spacingM: columnSet[SPACINGM],
    spacingT: columnSet[SPACINGT],
    spacingD: columnSet[SPACINGD],
  };
};

const flattenColumnSet = (column) => {
  const columnSet = {};
  const { set } = column;
  if (set && Array.isArray(set)) {
    let i;
    const setLength = set.length;
    for (i = 0; i < setLength; i += 1) {
      columnSet[set[i].key] = set[i].val;
    }
  }
  return columnSet;
};

const getBkgImgUrl = (background) => {
  const { url: bkgImgDUrl, url_t: bkgImgTUrl, url_m: bkgImgMUrl } = background.image || {};
  return { bkgImgDUrl, bkgImgTUrl, bkgImgMUrl };
};
const getBkgColor = (background) => {
  return background.color ? background.color.color : null;
};

const getVariation = (headerTextLine) => {
  const [
    {
      variation,
      position,
      positionDLeft,
      positionDTop,
      positionDRight,
      positionTLeft,
      positionTTop,
      positionTRight,
    },
  ] = headerTextLine || [{}];

  return {
    variation,
    position,
    positionDLeft,
    positionDTop,
    positionDRight,
    positionTLeft,
    positionTTop,
    positionTRight,
  };
};

const renderColumns = (columns, otherProps) => {
  const { isTablet, isDesktop } = isClient() && getViewportInfo();
  const ZERO_WIDTH_COL = '0.00';

  // eslint-disable-next-line
  return columns.map((column) => {
    const columnSet = flattenColumnSet(column);
    const { width: columnWidthVal, widthM: columnWidthMVal, colName } = columnSet;
    const columnKeys = Object.keys(column);
    const columnSpacing = getColumnSpacing(columnSet);
    const border = (column && column.border) || {};
    const { url: desktopUrl, url_m: mobileUrl, url_t: tabletUrl } = border.image || {};
    const { color: borderColor } = border.color || {};
    const borderSet = flattenColumnSet(border);
    const { width, widthT, widthM } = borderSet || {};
    const borderUrl = { desktopUrl, mobileUrl, tabletUrl };
    const borderWidth = { width, widthT, widthM };
    const isCtaItems = column && column.composites && column.composites.ctaItemsComposite;
    const headerVariationValue =
      column &&
      column.composites &&
      column.composites.headerTextLine !== null &&
      getVariation(column.composites.headerTextLine);
    const isOverlayVariation = headerVariationValue?.variation === VARIATION_OVERLAY;
    const background = (column && column.background) || {};
    const isDesktopView = isDesktop && columnWidthVal !== ZERO_WIDTH_COL;
    const isTabletView = isTablet && columnWidthVal !== ZERO_WIDTH_COL;
    const isMobileView = !isDesktop && !isTablet && columnWidthMVal !== ZERO_WIDTH_COL;
    const { styleOverrides, isHpNewModuleDesignEnabled } = otherProps;
    const headerBorderRadiusOverride = getHeaderBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides,
      HEADER_BORDER_RADIUS_OVERRIDE_MAP[colName]
    );
    const headerOverrideStyle = isOverlayVariation ? {} : headerBorderRadiusOverride;

    /* eslint-disable no-param-reassign */
    otherProps.colName = colName;
    return (
      <>
        {isDesktopView && (
          <StyledColumn
            width={columnWidthVal}
            spacing={columnSpacing}
            className="styled-column"
            isOverlayVariation={isOverlayVariation}
            headerVariationValue={headerVariationValue}
            isCtaItems={isCtaItems}
            borderUrl={borderUrl}
            borderWidth={borderWidth}
            borderColor={borderColor}
            bkgImgUrl={getBkgImgUrl(background)}
            bkgColor={getBkgColor(background)}
            style={headerOverrideStyle}
            // eslint-disable-next-line react/no-array-index-key
            key={createUUID()}
          >
            {columnKeys.map((colKey) => {
              return renderColumnData(colKey, column, otherProps);
            })}
          </StyledColumn>
        )}
        {isTabletView && (
          <StyledColumn
            widthT={columnWidthVal}
            spacing={columnSpacing}
            className="styled-column"
            isOverlayVariation={isOverlayVariation}
            headerVariationValue={headerVariationValue}
            isCtaItems={isCtaItems}
            borderUrl={borderUrl}
            borderWidth={borderWidth}
            borderColor={borderColor}
            bkgImgUrl={getBkgImgUrl(background)}
            bkgColor={getBkgColor(background)}
            style={headerOverrideStyle}
            // eslint-disable-next-line react/no-array-index-key
            key={createUUID()}
          >
            {columnKeys.map((colKey) => {
              return renderColumnData(colKey, column, otherProps);
            })}
          </StyledColumn>
        )}
        {isMobileView && (
          <StyledColumn
            widthM={columnWidthMVal}
            spacing={columnSpacing}
            className="styled-column"
            isCtaItems={isCtaItems}
            borderUrl={borderUrl}
            borderWidth={borderWidth}
            borderColor={borderColor}
            bkgImgUrl={getBkgImgUrl(background)}
            bkgColor={getBkgColor(background)}
            style={headerOverrideStyle}
            // eslint-disable-next-line react/no-array-index-key
            key={createUUID()}
          >
            {columnKeys.map((colKey) => {
              return renderColumnData(colKey, column, otherProps);
            })}
          </StyledColumn>
        )}
      </>
    );
  });
};

const separateCTAOnlyColumn = (columns) => {
  const allColumnsArr = [];
  const ctaColumn = [];
  const columnsLength = columns.length;
  let i;
  for (i = 0; i < columnsLength; i += 1) {
    const columnSet = flattenColumnSet(columns[i]);
    const { colName } = columnSet;
    if (colName === 'column_5') {
      ctaColumn.push(columns[i]);
    } else {
      allColumnsArr.push(columns[i]);
    }
  }
  return {
    allColumns: allColumnsArr,
    ctaOnlyColumn: ctaColumn,
  };
};

const ModuleContainer = (props) => {
  const {
    columns,
    moduleClassName = '',
    className,
    isHpNewDesignCTAEnabled,
    isHpNewModuleDesignEnabled,
  } = props;
  const { allColumns, ctaOnlyColumn } = separateCTAOnlyColumn(columns);
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleContainer') || {};
  const otherProps = { styleOverrides, isHpNewDesignCTAEnabled, isHpNewModuleDesignEnabled };
  return (
    <>
      <StyledContainer className={`${className} ${moduleClassName} styled-container`}>
        {renderColumns(allColumns, otherProps)}
      </StyledContainer>
      {ctaOnlyColumn.length > 0 && (
        <StyledCTAColumn
          className={`${className} styled-cta-only-column`}
          style={styleOverrides['nav-bg']}
        >
          {renderColumns(ctaOnlyColumn, otherProps)}
        </StyledCTAColumn>
      )}
    </>
  );
};

ModuleContainer.propTypes = {
  moduleClassName: PropTypes.string,
  columns: PropTypes.arrayOf(PropTypes.any),
  className: PropTypes.string,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

ModuleContainer.defaultProps = {
  moduleClassName: '',
  columns: [],
  className: '',
};
export default errorBoundary(withStyles(ModuleContainer, `${BodyCopyStyles}${ContainerStyles}`));
export { ModuleContainer as ModuleContainerVanilla };
