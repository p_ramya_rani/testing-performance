// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import { ImageBackground, View } from 'react-native';
import PropTypes from 'prop-types';
import { BodyCopy, Button, DamImage, Anchor } from '@tcp/core/src/components/common/atoms';
import { styleOverrideEngine } from '@tcp/core/src/utils/index.native';
import { cropImageUrl, getHeaderBorderRadius } from '@tcp/core/src/utils';
import HP_NEW_LAYOUT from '@tcp/core/src/constants/hpNewLayout.constants';
import bodyCopyStyles from '../../PageSlots/Border/bodyCopyStyles/bodyCopyStyles.native';
import {
  StyledColumn,
  StyledContainer,
  StyledColumnItem,
  StyledColumnBkgColor,
  StyledCTAColumn,
} from '../styles/ModuleContainer.style.native';
import CTAItems from '../molecules/CTAItems';
import ImageComponent from '../molecules/ImageComponent';
import TextComponent from '../molecules/TextComponent';
import getHeight from '../../PageSlots/borderThicknessConfig';

const MOBILE = 'mobile';
const WIDTH_MOBILE = 'widthM';
const SPACING_APP = 'spacingA';

const HEADER_BORDER_RADIUS_OVERRIDE_MAP = {
  column_1: ['col1-header1-top', 'col1-header1-bottom'],
  column_2: ['col2-header1-top', 'col2-header1-bottom'],
  column_3: ['col3-header1-top', 'col3-header1-bottom'],
  column_4: ['col4-header1-top', 'col4-header1-bottom'],
};

const renderStyledText = (styledProps, otherProps) => {
  if (!styledProps) return null;
  const { navigation } = otherProps;
  const { text, style, styledObj } = styledProps;
  const StyleBodyCopy = bodyCopyStyles[style] ? bodyCopyStyles[style] : BodyCopy;
  return <StyleBodyCopy text={text} style={{ ...styledObj }} navigation={navigation} />;
};

const renderButton = (buttonProps, otherProps) => {
  if (!buttonProps) return null;
  const { navigation } = otherProps;
  return <Button {...buttonProps} navigation={navigation} />;
};

const renderImage = (imageProps, otherProps) => {
  if (!imageProps) return null;
  const { navigation } = otherProps;
  return <DamImage imgData={imageProps} height={400} width="100%" navigation={navigation} />;
};

const renderVideo = (videoProps, otherProps) => {
  if (!videoProps) return null;
  const { navigation } = otherProps;
  const videoStyle = {
    width: '100%',
  };
  return (
    <StyledColumnItem style={videoStyle}>
      <DamImage videoData={videoProps} height={400} navigation={navigation} />
    </StyledColumnItem>
  );
};

const renderHeaderTextLine = (headerTextLine, otherProps) => {
  if (!headerTextLine || (headerTextLine && !headerTextLine.length)) return null;
  const { navigation, borderWidth } = otherProps;
  return (
    <StyledColumnItem>
      {headerTextLine.map((headerTextItem, index) => (
        <TextComponent
          headerTextLine={headerTextItem}
          navigation={navigation}
          borderWidth={borderWidth}
          index={index}
          {...otherProps}
        />
      ))}
    </StyledColumnItem>
  );
};

const renderCTAItem = (ctaItem, otherProps) => {
  if (!ctaItem) return null;
  const { navigation } = otherProps;
  const { button } = ctaItem;
  return (
    <StyledColumnItem>
      <Button {...button} navigation={navigation} />
    </StyledColumnItem>
  );
};

const renderCTAItemComposite = (ctaItems, otherProps) => {
  const { navigation, styleOverrides, isFifthColumn } = otherProps;
  return ctaItems ? (
    <StyledColumnItem>
      <CTAItems
        ctaItems={ctaItems}
        navigation={navigation}
        styleOverrides={styleOverrides}
        isFifthColumn={isFifthColumn}
        {...otherProps}
      />
    </StyledColumnItem>
  ) : null;
};

const renderLargeCompImageSimpleCarousel = (largeCompImageSimpleCarousel, otherProps) => {
  const { navigation, borderWidth, styleOverrides } = otherProps;
  return largeCompImageSimpleCarousel ? (
    <StyledColumnItem>
      <ImageComponent
        largeCompImageSimpleCarousel={largeCompImageSimpleCarousel}
        navigation={navigation}
        borderWidth={borderWidth}
        styleOverrides={styleOverrides}
        {...otherProps}
      />
    </StyledColumnItem>
  ) : null;
};

const renderCompositeData = (composite, composites, otherProps) => {
  switch (composite) {
    case 'headerTextLine':
      return (
        composites.headerTextLine && renderHeaderTextLine(composites.headerTextLine, otherProps)
      );
    case 'ctaItem':
      return composites.ctaItem && renderCTAItem(composites.ctaItem, otherProps);
    case 'ctaItemsComposite':
      return composites.ctaItemsComposite
        ? renderCTAItemComposite(composites.ctaItemsComposite, otherProps)
        : null;
    case 'largeCompImageSimpleCarouselComposite':
      return composites.largeCompImageSimpleCarouselComposite
        ? renderLargeCompImageSimpleCarousel(
            composites.largeCompImageSimpleCarouselComposite,
            otherProps
          )
        : null;
    default:
      return <></>;
  }
};

const renderComposites = (composites, otherProps) => {
  const compositeKeys = composites && Object.keys(composites);
  return (
    compositeKeys &&
    compositeKeys.map((composite) => {
      return renderCompositeData(composite, composites, otherProps);
    })
  );
};

// const renderRichText = richText => {
//   const { text: html } = richText;
//   return (
//     <View>
//       <Espot richTextHtml={html} routeName="Home" isNativeView={false} />
//     </View>
//   );
// };

const renderLink = (link, otherProps) => {
  const { navigation } = otherProps;
  return (
    link && (
      <Anchor
        accessibilityLabel={link.text}
        url={link && link.url}
        text={link.text}
        navigation={navigation}
      />
    )
  );
};

const renderColumnData = (colKey, column, otherProps) => {
  switch (colKey) {
    case 'styled':
      return <StyledColumnItem>{renderStyledText(column.styled, otherProps)}</StyledColumnItem>;
    case 'button':
      return <StyledColumnItem>{renderButton(column.button, otherProps)}</StyledColumnItem>;
    case 'image':
      return renderImage(column.image, otherProps);
    case 'video':
      return renderVideo(column.video, otherProps);
    case 'link':
      return <StyledColumnItem>{renderLink(column.link, otherProps)}</StyledColumnItem>;
    case 'composites':
      return renderComposites(column.composites, otherProps);
    // case 'richText':
    //   return renderRichText(column.richText);
    case 'set':
    default:
      return <></>;
  }
};

const flattenColumnSet = (column) => {
  const columnSet = {};
  if (column.set && Array.isArray(column.set)) {
    column.set.forEach((item) => {
      columnSet[item.key] = item.val;
    });
  }
  return columnSet;
};

const getBkgMedia = (column) => {
  const background = (column && column.background) || {};
  const { url: bkgImgDUrl, url_m: bkgImgMUrl } = background.image || {};
  const { color } = background.color || {};

  return {
    bkgColor: color,
    bkgImgSource: { uri: (bkgImgDUrl || bkgImgMUrl) && cropImageUrl(bkgImgMUrl || bkgImgDUrl) },
  };
};

const getBorderImgStyle = ({ uri }, borderWidth) => {
  return {
    flex: 1,
    resizeMode: 'stretch',
    width: '100%',
    paddingTop: uri && borderWidth,
    paddingBottom: uri && borderWidth,
  };
};

const getBkgImgStyle = () => {
  return {
    flex: 1,
    resizeMode: 'stretch',
    width: '100%',
  };
};

const getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides, overrideMap) => {
  if (isHpNewLayoutEnabled && styleOverrides && overrideMap) {
    return getHeaderBorderRadius(styleOverrides[overrideMap[0]], styleOverrides[overrideMap[1]]);
  }
  return {};
};

const renderColumns = (columns, otherProps) => {
  return columns.map((column) => {
    const columnSet = flattenColumnSet(column);
    const columnWidthVal = columnSet[WIDTH_MOBILE];
    const columnKeys = Object.keys(column);
    const columnSpacing = columnSet[SPACING_APP];
    const border = (column && column.border) || {};
    const { url: Url, url_m: mobileUrl } = border.image || {};
    const { color: borderColor } = border.color || {};
    const borderSet = flattenColumnSet(border);
    const { widthM } = borderSet || {};
    const borderWidth = widthM && getHeight(widthM, MOBILE);
    const image = {
      uri: mobileUrl || Url,
    };
    const { colName } = columnSet;
    const { styleOverrides, isHpNewLayoutEnabled } = otherProps;
    const headerBorderRadiusOverride = getHeaderBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides,
      HEADER_BORDER_RADIUS_OVERRIDE_MAP[colName]
    );

    const imageSource = getBkgMedia(column).bkgImgSource;

    return (
      <StyledColumn
        width={columnWidthVal}
        spacing={columnSpacing}
        borColor={borderColor}
        borWidth={borderWidth}
      >
        <ImageBackground
          source={borderWidth && image}
          style={getBorderImgStyle(image, borderWidth)}
        >
          <StyledColumnBkgColor
            bkgColor={getBkgMedia(column).bkgColor}
            borWidth={borderWidth}
            isBkgImg={image.uri}
            style={headerBorderRadiusOverride}
            isHpNewLayoutEnabled={isHpNewLayoutEnabled}
          >
            {imageSource && imageSource.uri ? (
              <ImageBackground
                source={getBkgMedia(column).bkgImgSource}
                style={getBkgImgStyle()}
                resizeMode="stretch"
                imageStyle={headerBorderRadiusOverride}
              >
                {columnKeys.map((colKey) => {
                  return renderColumnData(colKey, column, { ...otherProps, borderWidth, colName });
                })}
              </ImageBackground>
            ) : (
              columnKeys.map((colKey) => {
                return renderColumnData(colKey, column, { ...otherProps, borderWidth, colName });
              })
            )}
          </StyledColumnBkgColor>
        </ImageBackground>
      </StyledColumn>
    );
  });
};

const separateCTAOnlyColumn = (columns, setAllColumns, setCtaOnlyColumn) => {
  const allColumnsArr = [];
  const ctaColumn = [];
  columns.forEach((column) => {
    const columnSet = flattenColumnSet(column);
    const { colName } = columnSet;
    if (colName === 'column_5') {
      ctaColumn.push(column);
    } else {
      allColumnsArr.push(column);
    }
  });
  setAllColumns(allColumnsArr);
  setCtaOnlyColumn(ctaColumn);
};

const ModuleContainer = (props) => {
  const { columns = [], moduleClassName = '', navigation = {}, isHpNewLayoutEnabled } = props;
  const [allColumns, setAllColumns] = useState([]);
  const [ctaOnlyColumn, setCtaOnlyColumn] = useState([]);
  React.useEffect(() => {
    separateCTAOnlyColumn(columns, setAllColumns, setCtaOnlyColumn);
  }, [columns]);
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleContainer') || {};
  const isFifthColumn = ctaOnlyColumn && ctaOnlyColumn.length > 0;
  const otherProps = { navigation, styleOverrides, isFifthColumn, isHpNewLayoutEnabled };
  return (
    <View style={isHpNewLayoutEnabled && HP_NEW_LAYOUT.MODULE_BOX_SHADOW}>
      <StyledContainer moduleClassName={moduleClassName}>
        {renderColumns(allColumns, otherProps)}
      </StyledContainer>
      {ctaOnlyColumn.length > 0 && (
        <StyledCTAColumn>{renderColumns(ctaOnlyColumn, otherProps)}</StyledCTAColumn>
      )}
    </View>
  );
};

ModuleContainer.propTypes = {
  moduleClassName: PropTypes.string,
  columns: PropTypes.shape([]),
  navigation: PropTypes.shape({}).isRequired,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

ModuleContainer.defaultProps = {
  moduleClassName: '',
  columns: [],
};
export default ModuleContainer;
export { ModuleContainer as ModuleContainerVanilla };
