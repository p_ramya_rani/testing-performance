// 9fbef606107a605d69c0edbcd8029e5d
const columnData = [
  {
    set: [
      {
        key: 'width',
        val: '50.00',
      },
      {
        key: 'widthT',
        val: 'none',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
    ],
    composites: {
      headerTextLine: [
        {
          textItems: [
            {
              text: 'test',
              style: 'extra_large_text_regular',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/test',
            text: 'sometest',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          border: {
            name: 'horizontalRule',
            config: {
              heightD: 'medium',
              heightT: 'medium',
              heightM: 'medium',
              heightA: 'medium',
              spacingBottomD: 'medium',
              spacingBottomT: 'medium',
              spacingBottomM: 'medium',
              spacingBottomA: 'medium',
              paddingD: 'medium',
              paddingT: 'medium',
              paddingM: 'medium',
              paddingA: 'medium',
            },
            construct: {
              left: {
                type: 'color',
                color: {
                  color: '#33F6FF',
                },
              },
              text: {
                type: 'textItems',
                textItems: [
                  {
                    text: 'test middle',
                    style: 'small_text_regular',
                  },
                ],
              },
              right: {
                type: 'color',
                color: {
                  color: '#33F6FF',
                },
              },
            },
          },
          background: {
            image: {
              url: '/text',
              title: 'test',
              alt: 'test',
              width_d: '',
              width_t: '',
              width_m: '',
              height_d: '',
              height_t: '',
              height_m: '',
              crop_d: '',
              crop_t: '',
              crop_m: '',
              url_t: '',
              url_m: '',
            },
          },
          stackingOrder: 'stacked',
          paddingD: 'none',
          paddingDBottom: 'none',
          paddingDLeft: 'medium',
          paddingDRight: 'medium',
          paddingT: 'none',
          paddingTBottom: 'none',
          paddingTLeft: 'none',
          paddingTRight: 'none',
          paddingM: 'none',
          paddingMBottom: 'none',
          paddingMLeft: 'none',
          paddingMRight: 'none',
        },
        {
          textItems: [
            {
              text: 'Sub-header',
              style: 'medium_text_regular',
            },
            {
              text: 'Header',
              style: 'large_text_black',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/link',
            text: 'Text',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          border: {
            name: 'color',
            config: {
              heightD: 'small',
              heightT: 'small',
              heightM: 'small',
              heightA: 'small',
              spacingBottomD: 'none',
              spacingBottomT: 'none',
              spacingBottomM: 'none',
              spacingBottomA: 'none',
              paddingD: 'xxxLarge',
              paddingT: 'xxxLarge',
              paddingM: 'xxxLarge',
              paddingA: 'xxxLarge',
            },
            construct: {
              color: {
                color: '#33F6FF',
              },
            },
          },
          background: {
            color: null,
            image: {
              url: '/ecom/assets/content/tcp/us/homepage/050620/image_3x.png',
              title: 'Main',
              alt: 'Main',
              crop_d: '',
              crop_t: '',
              crop_m: '',
              url_t: '',
              url_m:
                'https://test1.theplace.com/image/upload/t_mod_B_img_default_d,f_auto,q_auto,dpr_1.0/ecom/assets/content/tcp/us/homepage/050620/image_3x.png',
            },
          },
          stackingOrder: 'flow',
          paddingD: 'small',
          paddingDBottom: 'small',
          paddingDLeft: 'small',
          paddingDRight: 'small',
          paddingT: 'medium',
          paddingTBottom: 'medium',
          paddingTLeft: 'medium',
          paddingTRight: 'medium',
          paddingM: 'medium',
          paddingMBottom: 'medium',
          paddingMLeft: 'medium',
          paddingMRight: 'medium',
        },
      ],
    },
    border: {
      set: [
        {
          key: 'width',
          val: 'small',
        },
        {
          key: 'widthT',
          val: 'small',
        },
        {
          key: 'widthM',
          val: 'small',
        },
      ],
      image: {
        url: 'https://test1.theplace.com/video/upload/v1594339016/ecom/assets/content/tcp/us/border-test/borders-desktop-white-bg.gif',
        title: 'Border',
        alt: 'Border',
        crop_d: '',
        crop_t: '',
        crop_m: '',
        url_t: null,
        url_m: null,
      },
    },
    background: {
      image: {
        url: 'https://test1.theplace.com/image/upload/v1594339015/ecom/assets/content/tcp/us/border-test/borders-desktop.gif',
        title: 'q',
        alt: 'q',
        crop_d: '',
        crop_t: '',
        crop_m: '',
        url_t: '',
        url_m:
          'https://assets.theplace.com/image/upload/ecom/assets/content/tcp/us/homepage/080420/PLUS-MB_3x.png',
      },
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '50.00',
      },
      {
        key: 'widthT',
        val: 'none',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
    ],
    composites: {
      headerTextLine: [
        {
          textItems: [
            {
              text: '$20_%_OFF',
              style: 'price_point_style_8',
            },
            {
              text: 'PRICE POINT',
              style: 'small_text_black',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/link',
            text: 'Text',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          background: null,

          stackingOrder: 'flow',
          paddingD: 'small',
          paddingDBottom: 'small',
          paddingDLeft: 'small',
          paddingDRight: 'small',
          paddingT: 'medium',
          paddingTBottom: 'medium',
          paddingTLeft: 'medium',
          paddingTRight: 'medium',
          paddingM: 'medium',
          paddingMBottom: 'medium',
          paddingMLeft: 'medium',
          paddingMRight: 'medium',
        },
        {
          textItems: [
            {
              text: '$_99_99_-99_99',
              style: 'price_point_style_2',
            },
            {
              text: 'PRICE POINT',
              style: 'small_text_black',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/link',
            text: 'Text',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          background: {
            color: null,
            image: {
              url: '/ecom/assets/content/tcp/us/homepage/050620/image_3x.png',
              title: 'Main',
              alt: 'Main',
              crop_d: '',
              crop_t: '',
              crop_m: '',
              url_t: '',
              url_m:
                'https://test1.theplace.com/image/upload/t_mod_B_img_default_d,f_auto,q_auto,dpr_1.0/ecom/assets/content/tcp/us/homepage/050620/image_3x.png',
            },
          },
          stackingOrder: 'flow',
          paddingD: 'small',
          paddingDBottom: 'small',
          paddingDLeft: 'small',
          paddingDRight: 'small',
          paddingT: 'medium',
          paddingTBottom: 'medium',
          paddingTLeft: 'medium',
          paddingTRight: 'medium',
          paddingM: 'medium',
          paddingMBottom: 'medium',
          paddingMLeft: 'medium',
          paddingMRight: 'medium',
        },
      ],
    },
    border: {
      set: [
        {
          key: 'width',
          val: 'small',
        },
        {
          key: 'widthT',
          val: 'small',
        },
        {
          key: 'widthM',
          val: 'small',
        },
      ],
      color: {
        color: '#33F6FF',
      },
      image: null,
    },
    background: {
      color: {
        color: '#33F6FF',
      },
    },
  },
];

export default columnData;
