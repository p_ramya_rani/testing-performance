// 9fbef606107a605d69c0edbcd8029e5d
const columnData = [
  {
    set: [
      {
        key: 'width',
        val: '50.00',
      },
      {
        key: 'widthT',
        val: 'none',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'none',
      },
      {
        key: 'spacingT',
        val: 'none',
      },
      {
        key: 'spacingM',
        val: 'none',
      },
      {
        key: 'spacingA',
        val: 'none',
      },
    ],
    composites: {
      largeCompImageSimpleCarouselComposite: [
        {
          image: {
            url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
            title: 'Image 1',
            alt: 'Image 1',
            crop_d: '',
            crop_t: '',
            crop_m: '',
            url_t: '',
            url_m: null,
          },
          singleCTAButton: {
            url: '/c/toddler-boy-clothes',
            text: 'SHOP STYLE',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'link',
        },
        {
          image: {
            url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
            title: 'Image 2',
            alt: 'Image 2',
            crop_d: '',
            crop_t: '',
            crop_m: '',
            url_t: '',
            url_m: null,
          },
          singleCTAButton: {
            url: '/c/toddler-boy-clothes',
            text: 'SHOP LINK',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'link',
        },
      ],
    },
    border: {
      set: [
        {
          key: 'width',
          val: 'none',
        },
        {
          key: 'widthT',
          val: 'small',
        },
        {
          key: 'widthM',
          val: 'small',
        },
      ],
      color: null,
      image: {
        url: 'https://test1.theplace.com/video/upload/v1594339016/ecom/assets/content/tcp/us/border-test/borders-desktop-white-bg.gif',
        title: 'Border',
        alt: 'Border',
        crop_d: '',
        crop_t: '',
        crop_m: '',
        url_t: null,
        url_m:
          'https://test1.theplace.com/image/upload/w_768,f_auto,q_auto/ecom/assets/content/tcp/us/homepage/050520/denim.png',
      },
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '50.00',
      },
      {
        key: 'widthT',
        val: 'none',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'none',
      },
      {
        key: 'spacingT',
        val: 'none',
      },
      {
        key: 'spacingM',
        val: 'none',
      },
      {
        key: 'spacingA',
        val: 'none',
      },
    ],
    composites: {
      largeCompImageSimpleCarouselComposite: [
        {
          image: {
            url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
            title: 'Image 1',
            alt: 'Image 1',
            crop_d: '',
            crop_t: '',
            crop_m: '',
            url_t: '',
            url_m: '',
          },
          singleCTAButton: {
            url: '/c/toddler-boy-clothes',
            text: 'SHOP STYLE',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'link',
        },
      ],
    },
    border: {
      set: [
        {
          key: 'width',
          val: 'none',
        },
        {
          key: 'widthT',
          val: 'small',
        },
        {
          key: 'widthM',
          val: 'small',
        },
      ],
      color: {
        color: '#000000',
      },
      image: null,
    },
  },
];

export default columnData;
