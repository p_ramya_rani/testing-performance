// 9fbef606107a605d69c0edbcd8029e5d
const columnsData = [
  {
    set: [
      {
        key: 'width',
        val: '33.33',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
    ],
    border: {
      set: null,
      image: null,
    },
    composites: {
      headerTextLine: [
        {
          textItems: [
            {
              text: 'some test',
              style: 'spaced_text',
            },
            {
              text: 'Sub Header Text',
              style: 'medium_text_black',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/search/graphic-tees?icid=hp_s2_text_all_051120_shorts',
            text: "Let's See",
            title: '',
            target: '',
            external: 0,
            action: '',
          },
        },
      ],
    },
    styled: {
      text: 'Simple Text',
      style: 'medium_text_regular',
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '50.00',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
    ],
    image: {
      url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
      title: 'Sample',
      alt: 'Sample',
      crop_d: '',
      crop_t: '',
      crop_m: '',
      url_t: '',
      url_m: '',
    },
    border: {
      set: null,
      image: null,
    },
    composites: {
      ctaItem: {
        button: {
          url: '/c/girls-clothing-school-uniforms?icid=hp_s8_button_g_070820_uniform',
          text: 'GIRL',
          title: '',
          target: '',
          external: 0,
          action: '',
        },
      },
    },
    richText: {
      text: '<style>\r\n    .mpr-ecom-mobile {\r\n      display: none;\r\n    }\r\n    .mpr-ecom-desktop{\r\n        margin: auto;\r\n        display: block;\r\n    }\r\n\r\n    @media only screen and (max-width: 767px) {\r\n      .mpr-ecom-desktop {\r\n        display: none;\r\n      }\r\n      .mpr-ecom-mobile {\r\n        display: block;\r\n        width: 100%;\r\n      }\r\n    }\r\n  </style>\r\n  <div class="gift-card">\r\n    <a href="/us/content/double-up?icid=hp_na_image_all_040220_double-up">\r\n      <img\r\n        class="bs-img mpr-ecom-desktop fitiingbrn"\r\n        src="https://assets.theplace.com/image/upload/q_auto/ecom/assets/content/tcp/us/homepage/032420/TCP-HP-BANNER.gif"\r\n        alt="MPR - DOUBLE UP"/>\r\n      <img\r\n        class="bs-img mpr-ecom-mobile"\r\n        src="https://assets.theplace.com/image/upload/q_auto/ecom/assets/content/tcp/us/homepage/032420/TCP-MB-BANNER.gif"\r\n        alt="MPR - DOUBLE UP"/>\r\n    </a>\r\n  </div>\r\n  ',
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '33.33',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
    ],
    border: {
      set: null,
      image: null,
    },
    composites: {
      ctaItemsComposite: [
        {
          button: {
            url: '/search/girl-all?icid=hp_s2_button_g_072120_main',
            text: 'GIRLS',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          featured: 'true',
          ctaType: 'linkList',
        },
        {
          button: {
            url: '/search/boy-all?icid=hp_s2_button_b_072120_main',
            text: 'BOYS',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'linkList',
        },
      ],
    },
    video: {
      url: '/v1574204032/SampleBunnyVideo.mp4',
      url_t: '',
      url_m: '',
      title: 'Sample Bunny',
      autoplay: '1',
      controls: '0',
      loop: '1',
      muted: '1',
      inline: '1',
    },
    link: {
      url: '/search/graphic-tees?icid=hp_s2_text_all_051120_shorts',
      text: 'ALL SHORTS',
      title: '',
      target: '',
      external: 0,
      action: '',
    },
  },
];

export default columnsData;
