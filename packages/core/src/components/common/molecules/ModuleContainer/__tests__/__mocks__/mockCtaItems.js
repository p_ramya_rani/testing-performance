// 9fbef606107a605d69c0edbcd8029e5d
export const columnDataWithCTAButtonCarousel = [
  {
    set: [
      {
        key: 'width',
        val: '50.00',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
    ],
    border: {
      set: null,
      image: null,
    },
    composites: {
      headerTextLine: [
        {
          textItems: [
            {
              text: 'some test',
              style: 'spaced_text',
            },
            {
              text: 'Sub Header Text',
              style: 'medium_text_black',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/search/graphic-tees?icid=hp_s2_text_all_051120_shorts',
            text: "Let's See",
            title: '',
            target: '',
            external: 0,
            action: '',
          },
        },
      ],
    },
    styled: {
      text: 'Simple Text',
      style: 'medium_text_regular',
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '50.00',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
    ],
    image: {
      url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
      title: 'Sample',
      alt: 'Sample',
      crop_d: '',
      crop_t: '',
      crop_m: '',
      url_t: '',
      url_m: '',
    },
    border: {
      set: null,
      image: null,
    },
    composites: {
      ctaItem: {
        button: {
          url: '/c/girls-clothing-school-uniforms?icid=hp_s8_button_g_070820_uniform',
          text: 'GIRL',
          title: '',
          target: '',
          external: 0,
          action: '',
        },
      },
    },
    richText: {
      text: '<style>\r\n    .mpr-ecom-mobile {\r\n      display: none;\r\n    }\r\n    .mpr-ecom-desktop{\r\n        margin: auto;\r\n        display: block;\r\n    }\r\n\r\n    @media only screen and (max-width: 767px) {\r\n      .mpr-ecom-desktop {\r\n        display: none;\r\n      }\r\n      .mpr-ecom-mobile {\r\n        display: block;\r\n        width: 100%;\r\n      }\r\n    }\r\n  </style>\r\n  <div class="gift-card">\r\n    <a href="/us/content/double-up?icid=hp_na_image_all_040220_double-up">\r\n      <img\r\n        class="bs-img mpr-ecom-desktop fitiingbrn"\r\n        src="https://assets.theplace.com/image/upload/q_auto/ecom/assets/content/tcp/us/homepage/032420/TCP-HP-BANNER.gif"\r\n        alt="MPR - DOUBLE UP"/>\r\n      <img\r\n        class="bs-img mpr-ecom-mobile"\r\n        src="https://assets.theplace.com/image/upload/q_auto/ecom/assets/content/tcp/us/homepage/032420/TCP-MB-BANNER.gif"\r\n        alt="MPR - DOUBLE UP"/>\r\n    </a>\r\n  </div>\r\n  ',
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '100.00',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
      {
        key: 'colName',
        val: 'column_5',
      },
    ],
    composites: {
      ctaItemsComposite: [
        {
          button: {
            url: '/search/girl-all?icid=hp_s2_button_g_072120_main',
            text: 'GIRL',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          featured: 'true',
          expandableTitle: 'Here You',
          ctaType: 'CTAButtonCarousel',
        },
        {
          button: {
            url: '/search/boy-all?icid=hp_s2_button_b_072120_main',
            text: 'BOY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'CTAButtonCarousel',
          expandableTitle: 'Here You',
        },
        {
          button: {
            url: '/search/toddler-girl-all?icid=hp_s2_button_tg_072120_main',
            text: 'TODDLER GIRL',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'CTAButtonCarousel',
          expandableTitle: 'Here You',
        },
        {
          button: {
            url: '/search/toddler-boy-all?icid=hp_s2_button_tb_072120_main',
            text: 'TODDLER BOY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'CTAButtonCarousel',
          expandableTitle: 'Here You',
        },
        {
          button: {
            url: '/search/baby-all?icid=hp_s2_button_tb_072120_main',
            text: 'BABY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'CTAButtonCarousel',
          expandableTitle: 'Here You',
        },
      ],
    },
  },
];

export const columnDataWithLinkListCTA = [
  {
    set: [
      {
        key: 'width',
        val: '100.00',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
    ],
    border: {
      set: null,
      image: null,
    },
    composites: {
      headerTextLine: [
        {
          textItems: [
            {
              text: 'some test',
              style: 'spaced_text',
            },
            {
              text: 'Sub Header Text',
              style: 'medium_text_black',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/search/graphic-tees?icid=hp_s2_text_all_051120_shorts',
            text: "Let's See",
            title: '',
            target: '',
            external: 0,
            action: '',
          },
        },
      ],
    },
    styled: {
      text: 'Simple Text',
      style: 'medium_text_regular',
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '100.00',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
      {
        key: 'colName',
        val: 'column_5',
      },
    ],
    composites: {
      ctaItemsComposite: [
        {
          button: {
            url: '/search/girl-all?icid=hp_s2_button_g_072120_main',
            text: 'GIRL',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          featured: 'true',
          ctaType: 'linkList',
        },
        {
          button: {
            url: '/search/boy-all?icid=hp_s2_button_b_072120_main',
            text: 'BOY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'linkList',
        },
      ],
    },
  },
];

export const columnDataWithStackedCTAButtons = [
  {
    set: [
      {
        key: 'width',
        val: '100.00',
      },
      {
        key: 'widthM',
        val: '34.00',
      },
      {
        key: 'spacingD',
        val: 'none',
      },
      {
        key: 'spacingT',
        val: 'none',
      },
      {
        key: 'spacingM',
        val: 'none',
      },
      {
        key: 'spacingA',
        val: 'none',
      },
    ],
    border: {
      set: null,
      image: null,
    },
    composites: {
      headerTextLine: [
        {
          textItems: [
            {
              text: 'some test',
              style: 'spaced_text',
            },
            {
              text: 'Sub Header Text',
              style: 'medium_text_black',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/search/graphic-tees?icid=hp_s2_text_all_051120_shorts',
            text: "Let's See",
            title: '',
            target: '',
            external: 0,
            action: '',
          },
        },
      ],
    },
    styled: {
      text: 'Simple Text',
      style: 'medium_text_regular',
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '100.00',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
      {
        key: 'colName',
        val: 'column_5',
      },
    ],
    composites: {
      ctaItemsComposite: [
        {
          button: {
            url: '/search/girl-all?icid=hp_s2_button_g_072120_main',
            text: 'GIRL',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          featured: 'true',
          ctaType: 'stackedCTAButtons',
          expandableTitle: 'Here You',
        },
        {
          button: {
            url: '/search/boy-all?icid=hp_s2_button_b_072120_main',
            text: 'BOY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'stackedCTAButtons',
          expandableTitle: 'Here You',
        },
        {
          button: {
            url: '/search/toddler-girl-all?icid=hp_s2_button_tg_072120_main',
            text: 'TODDLER GIRL',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'stackedCTAButtons',
          expandableTitle: 'Here You',
        },
        {
          button: {
            url: '/search/toddler-boy-all?icid=hp_s2_button_tb_072120_main',
            text: 'TODDLER BOY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'stackedCTAButtons',
          expandableTitle: 'Here You',
        },
        {
          button: {
            url: '/search/baby-all?icid=hp_s2_button_tb_072120_main',
            text: 'BABY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'stackedCTAButtons',
          expandableTitle: 'Here You',
        },
        {
          button: {
            url: '/search/baby-all?icid=hp_s2_button_tb_072120_main',
            text: 'BABY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          ctaType: 'stackedCTAButtons',
          expandableTitle: 'Here You',
          featured: 'true',
        },
      ],
    },
  },
];

export const columnDataWithDivImageCTACarousel = [
  {
    set: [
      {
        key: 'width',
        val: '100.00',
      },
      {
        key: 'widthM',
        val: '54.00',
      },
      {
        key: 'spacingD',
        val: 'none',
      },
      {
        key: 'spacingT',
        val: 'none',
      },
      {
        key: 'spacingM',
        val: 'none',
      },
      {
        key: 'spacingA',
        val: 'none',
      },
    ],
    border: {
      set: null,
      image: null,
    },
    composites: {
      headerTextLine: [
        {
          textItems: [
            {
              text: 'some test',
              style: 'spaced_text',
            },
            {
              text: 'Sub Header Text',
              style: 'medium_text_black',
            },
          ],
          icon: {
            icon: '',
            placement: '',
          },
          link: {
            url: '/search/graphic-tees?icid=hp_s2_text_all_051120_shorts',
            text: "Let's See",
            title: '',
            target: '',
            external: 0,
            action: '',
          },
        },
      ],
    },
    styled: {
      text: 'Simple Text',
      style: 'medium_text_regular',
    },
  },
  {
    set: [
      {
        key: 'width',
        val: '100.00',
      },
      {
        key: 'widthM',
        val: '100.00',
      },
      {
        key: 'spacingD',
        val: 'medium',
      },
      {
        key: 'spacingT',
        val: 'medium',
      },
      {
        key: 'spacingM',
        val: 'medium',
      },
      {
        key: 'spacingA',
        val: 'medium',
      },
      {
        key: 'colName',
        val: 'column_5',
      },
    ],
    composites: {
      ctaItemsComposite: [
        {
          button: {
            url: '/search/girl-all?icid=hp_s2_button_g_072120_main',
            text: 'GIRL',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          image: {
            url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
            title: 'Sample',
            alt: 'Sample',
            crop_d: '',
            crop_t: '',
            crop_m: '',
            url_t: '',
            url_m: '',
          },
          featured: 'true',
          ctaType: 'divImageCTACarousel',
        },
        {
          button: {
            url: '/search/boy-all?icid=hp_s2_button_b_072120_main',
            text: 'BOY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          image: {
            url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
            title: 'Sample',
            alt: 'Sample',
            crop_d: '',
            crop_t: '',
            crop_m: '',
            url_t: '',
            url_m: '',
          },
          ctaType: 'divImageCTACarousel',
        },
        {
          button: {
            url: '/search/toddler-girl-all?icid=hp_s2_button_tg_072120_main',
            text: 'TODDLER GIRL',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          image: {
            url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
            title: 'Sample',
            alt: 'Sample',
            crop_d: '',
            crop_t: '',
            crop_m: '',
            url_t: '',
            url_m: '',
          },
          ctaType: 'divImageCTACarousel',
        },
        {
          button: {
            url: '/search/toddler-boy-all?icid=hp_s2_button_tb_072120_main',
            text: 'TODDLER BOY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          image: {
            url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
            title: 'Sample',
            alt: 'Sample',
            crop_d: '',
            crop_t: '',
            crop_m: '',
            url_t: '',
            url_m: '',
          },
          ctaType: 'divImageCTACarousel',
        },
        {
          button: {
            url: '/search/baby-all?icid=hp_s2_button_tb_072120_main',
            text: 'BABY',
            title: '',
            target: '',
            external: 0,
            action: '',
          },
          image: {
            url: '/ecom/assets/content/tcp/us/homepage/042320/tile-2_3x.png',
            title: 'Sample',
            alt: 'Sample',
            crop_d: '',
            crop_t: '',
            crop_m: '',
            url_t: '',
            url_m: '',
          },
          ctaType: 'divImageCTACarousel',
        },
      ],
    },
  },
];
