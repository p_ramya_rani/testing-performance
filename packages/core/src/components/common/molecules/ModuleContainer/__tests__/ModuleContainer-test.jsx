// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mockData from './__mocks__/mockModuleContainer';
import { ModuleContainerVanilla as ModuleContainer } from '../view/ModuleContainer';

describe('ModuleComponent component', () => {
  let useEffect;
  const mockUseEffect = () => {
    useEffect.mockImplementationOnce((f) => f());
  };
  beforeEach(() => {
    useEffect = jest.spyOn(React, 'useEffect');
    mockUseEffect();
  });
  it('should render correctly', async () => {
    const wrapper = shallow(<ModuleContainer columns={mockData} />).get(0);
    expect(wrapper).toMatchSnapshot();
  });
});
