// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import Button from '@tcp/core/src/components/common/atoms/Button';
import { cropImageUrl } from '@tcp/core/src/utils';
import getBottomMargin from '../../PageSlots/spacingConfig';
import getHeight from '../../PageSlots/borderThicknessConfig';
import getTextPadding from '../../PageSlots/textPaddingConfig';

const MOBILE = 'mobile';
const TABLET = 'tablet';
const DESKTOP = 'desktop';
const STACKED_CTA_BUTTONS = 'stackedCTAButtons';
const LINK_LIST = 'linkList';
const ALIGN_LEFT = 'left';
const ALIGN_RIGHT = 'right';

const getCtaVariation = (props, CtaVariationObj) => {
  if (CtaVariationObj.length === 7) {
    return `
      @media ${props.theme.mediaQuery.large} {
        .stacked-button-list-wrapper {
          display: grid;
          grid-gap: 20px;
          .stacked-button {
            margin: 0;
          }
        }
        .stacked-button:first-child {
          grid-column: 1/7;
          span {
            width: 100% !important;
          }
        }
      }
      @media ${props.theme.mediaQuery.mediumOnly} {
        .stacked-button-list-wrapper {
          display: grid;
          grid-gap: 10px;
          .stacked-button {
            margin: 0;
          }
        }
        .stacked-button:first-child {
          grid-column: 1/7;
        }
        .stacked-button {
          span {
            width: 100% !important;
          }
        }
      }
    `;
  }
  if (CtaVariationObj.length === 8) {
    return `
      @media ${props.theme.mediaQuery.large} {
        .stacked-button-list-wrapper {
          display: grid;
          grid-gap: 10px;
          .stacked-button {
            min-width: 185px;
            margin: 0;
          }
        }
        .stacked-button:first-child {
          grid-column: 1/8;
          span {
            width: 100% !important;
          }
        }
        .stacked-button:not(:first-child) {
          span {
            width: 185px !important;
          }
        }
      }
      @media ${props.theme.mediaQuery.mediumOnly} {
        .stacked-button-list-wrapper {
          display: grid;
          grid-gap: 8px;
          .stacked-button {
            margin: 0;
          }
        }
        .stacked-button:first-child {
          grid-column: 1/8;
          span {
            width: 100% !important;
          }
        }
        .stacked-button:not(:first-child) {
          span {
            width: 95px !important;
          }
        }
      }
    `;
  }
  return '';
};

const getVariationDesktopAlignment = (variationObj) => {
  const { position, positionDLeft, positionDTop, positionDRight } = variationObj;

  if (position === ALIGN_LEFT) {
    return `
      top: ${positionDTop}%;
      left: ${positionDLeft}%;
    `;
  }
  if (position === ALIGN_RIGHT) {
    return `
      top: ${positionDTop}%;
      right: ${positionDRight}%;
    `;
  }
  return `
    top: ${positionDTop}%;
    left: ${positionDLeft}%;
  `;
};

const getVariationTabletAlignment = (variationObj) => {
  const { position, positionTLeft, positionTTop, positionTRight } = variationObj;

  if (position === ALIGN_LEFT) {
    return `
      top: ${positionTTop}%;
      left: ${positionTLeft}%;
    `;
  }
  if (position === ALIGN_RIGHT) {
    return `
      top: ${positionTTop}%;
      right: ${positionTRight}%;
    `;
  }
  return `
    top: ${positionTTop}%;
    left: ${positionTLeft}%;
  `;
};

const getCtaBgImage = (bgImageObj) => {
  const {
    ctaBackgroundImage: { url: ctaDesktopUrl, url_t: ctaTabletUrl, url_m: ctaMobileUrl },
  } = bgImageObj || {};
  return {
    desktop: ctaDesktopUrl && cropImageUrl(ctaDesktopUrl),
    tablet: ctaTabletUrl ? cropImageUrl(ctaTabletUrl) : cropImageUrl(ctaDesktopUrl),
    mobile: ctaMobileUrl ? cropImageUrl(ctaMobileUrl) : cropImageUrl(ctaDesktopUrl),
  };
};

export default css``;

const StyledButton = styled(Button)`
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    width: 221px;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    width: 258px;
  }
`;

const StyledColumn = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  @media ${(props) => props.theme.mediaQuery.medium} {
    flex: calc(${(props) => (props.width ? props.width : 100)} / 100);
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    flex: calc(${(props) => (props.width ? props.width : 100)} / 100);
  }

  margin-bottom: ${(props) => getBottomMargin(props.spacing.spacingM, MOBILE)}px;
  @media ${(props) => props.theme.mediaQuery.medium} {
    margin-bottom: 0;
    :not(:last-child) {
      margin-right: ${(props) => getBottomMargin(props.spacing.spacingT, TABLET)}px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    margin-bottom: 0;
    :not(:last-child) {
      margin-right: ${(props) => getBottomMargin(props.spacing.spacingD, DESKTOP)}px;
    }
  }

  min-width: 0;
  border-style: solid;
  ${(props) =>
    props.borderUrl.mobileUrl || props.borderUrl.desktopUrl
      ? `border-image-source: url(${props.borderUrl.mobileUrl || props.borderUrl.desktopUrl});
      border-image-slice: 20;
      border-image-repeat: round;`
      : ``};

  border-width: ${(props) =>
    props.borderWidth.widthM ? getHeight(props.borderWidth.widthM, MOBILE) : '0'}px;

  width: ${(props) =>
      props.borderWidth && props.borderWidth.widthM && getHeight(props.borderWidth.widthM, MOBILE)
        ? `calc(${props.widthM}% - ${2 * getHeight(props.borderWidth.widthM, MOBILE)}px);`
        : `${props.widthM}%;`}
    ${(props) =>
      props.bkgImgUrl.bkgImgMUrl || props.bkgImgUrl.bkgImgDUrl
        ? `background-image: url(
    ${cropImageUrl(
      props.bkgImgUrl.bkgImgMUrl ? props.bkgImgUrl.bkgImgMUrl : props.bkgImgUrl.bkgImgDUrl
    )});
    background-size: cover;
    background-repeat: no-repeat;
    `
        : ''}
    ${(props) => (props.bkgColor ? `background-color:${props.bkgColor};` : '')}
    ${(props) => (props.borderColor ? `border-color: ${props.borderColor}` : ``)};

  @media ${(props) => props.theme.mediaQuery.medium} {
    ${(props) =>
      props.borderUrl.tabletUrl || props.borderUrl.desktopUrl
        ? `border-image-source: url(${props.borderUrl.tabletUrl || props.borderUrl.desktopUrl});
        border-image-slice: 20;
        border-image-repeat: round;`
        : ``};

    border-width: ${(props) =>
      props.borderWidth.widthT ? getHeight(props.borderWidth.widthT, TABLET) : '0'}px;

    display: flex;
    width: ${(props) =>
        props.borderWidth && props.borderWidth.widthT && getHeight(props.borderWidth.widthT, TABLET)
          ? `calc(${props.widthT}% - ${2 * getHeight(props.borderWidth.widthT, TABLET)}px);`
          : `${props.widthT}%;`}
      ${(props) =>
        props.bkgImgUrl.bkgImgTUrl || props.bkgImgUrl.bkgImgDUrl
          ? `background-image: url(
      ${cropImageUrl(
        props.bkgImgUrl.bkgImgTUrl ? props.bkgImgUrl.bkgImgTUrl : props.bkgImgUrl.bkgImgDUrl
      )});
     `
          : ''};

    ${(props) =>
      props.isOverlayVariation
        ? `
          position: absolute;
          z-index:1;
          ${getVariationTabletAlignment(props.headerVariationValue)}
        `
        : ''};
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    ${(props) =>
      props.borderUrl.desktopUrl
        ? `border-image-source: url(${props.borderUrl.desktopUrl});
        border-image-slice: 20;
        border-image-repeat: round;`
        : ``};
    border-width: ${(props) =>
      props.borderWidth.width ? getHeight(props.borderWidth.width, DESKTOP) : '0'}px;

    display: flex;
    width: ${(props) =>
        props.borderWidth && props.borderWidth.width && getHeight(props.borderWidth.width, DESKTOP)
          ? `calc(${props.width}% - ${2 * getHeight(props.borderWidth.width, DESKTOP)}px);`
          : `${props.width}%;`}
      ${(props) =>
        props.bkgImgUrl.bkgImgDUrl
          ? `background-image: url(
        ${cropImageUrl(props.bkgImgUrl.bkgImgDUrl ? props.bkgImgUrl.bkgImgDUrl : '')});
        `
          : ''};
    ${(props) =>
      props.isOverlayVariation
        ? `
      position: absolute;
      z-index: 1;
      ${getVariationDesktopAlignment(props.headerVariationValue)}`
        : ''};
  }
`;

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: center;
  height: 100%;
  flex-wrap: wrap;
  position: relative;
  @media ${(props) => props.theme.mediaQuery.medium} {
    flex-wrap: nowrap;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    flex-wrap: nowrap;
  }

  .styled-column > *:not(:last-child) {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  /* Hide scrollbar for IE, Edge and Firefox */
  .styled-cta-container {
    -ms-overflow-style: none; /* IE and Edge */
    scrollbar-width: none; /* Firefox */
  }

  /* Hide scrollbar for Chrome, Safari and Opera */
  .styled-cta-container::-webkit-scrollbar {
    display: none;
  }
`;

const StyledCTAContainer = styled.div`
  width: 100%;
  overflow: auto;
  flex-direction: row;
  display: flex;
  align-items: center;
  justify-content: center;

  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    padding-top: ${getTextPadding('medium', TABLET)}px;
    display: ${(props) => (props.ctaTypeValue === STACKED_CTA_BUTTONS ? 'table' : 'flex')};
    ${(props) => (props.ctaTypeValue === 'CTAButtonCarousel' ? `display: contents;` : '')}
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    padding-top: ${getTextPadding('medium', DESKTOP)}px;
    display: ${(props) => (props.ctaTypeValue === STACKED_CTA_BUTTONS ? 'table' : 'flex')};
    ${(props) => (props.ctaTypeValue === 'CTAButtonCarousel' ? `display: contents;` : '')}
  }
  ${(props) =>
    props.ctaBgImageObj && getCtaBgImage(props.ctaBgImageObj)
      ? `
        background-image: url(${getCtaBgImage(props.ctaBgImageObj).mobile});
        @media ${props.theme.mediaQuery.large} {
          background-image: url(${getCtaBgImage(props.ctaBgImageObj).desktop});
        }
        @media ${props.theme.mediaQuery.mediumOnly} {
          background-image: url(${getCtaBgImage(props.ctaBgImageObj).tablet});
        }
      `
      : null};
`;

const StyledCTAItems = styled.div`
  display: flex;
  min-width: 0;

  ${(props) => (props.ctaTypeValue === LINK_LIST ? `width: 100%; justify-content: center;` : '')};

  ${(props) => (props.ctaTypeValue === STACKED_CTA_BUTTONS ? `flex: 1;` : '')};
  .stacked-button-list-wrapper {
    flex: 1;
  }

  ${(props) => props.ctaEnableVariation && getCtaVariation(props, props.ctaEnableVariation)}

  .dropdown-button-wrapper {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding-top: 10px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: 16px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    display: ${(props) => (props.ctaTypeValue === STACKED_CTA_BUTTONS ? 'block' : 'flex')};
    padding-bottom: 10px;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    display: ${(props) => (props.ctaTypeValue === STACKED_CTA_BUTTONS ? 'block' : 'flex')};
    padding-bottom: 16px;
  }
`;

const StyledCTAColumn = styled.div``;

export {
  StyledButton,
  StyledColumn,
  StyledContainer,
  StyledCTAItems,
  StyledCTAContainer,
  StyledCTAColumn,
};
