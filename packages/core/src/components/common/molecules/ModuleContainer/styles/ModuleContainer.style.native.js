// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import getBottomMargin from '../../PageSlots/spacingConfig';

const MOBILE = 'mobile';
const ZERO_WIDTH_COL = '0.00';

const StyledColumn = styled.View`
  ${props =>
    props.width === ZERO_WIDTH_COL
      ? `
    display:none;`
      : `display:flex;`}
  align-items: center;
  justify-content: center;
  width: ${props => (props.width ? props.width : 100)}%;
  margin-bottom: ${props => getBottomMargin(props.spacing, MOBILE)}px;
  ${props =>
    props.borColor
      ? `
  border: ${props.borWidth || 0}px solid ${props.borColor}
  `
      : ``}
`;

const StyledColumnBkgColor = styled.View`
  ${props =>
    props.isBkgImg
      ? `
      margin-left: ${props.borWidth || 0}px;
      margin-right: ${props.borWidth || 0}px;`
      : ``}

  ${props =>
    props.bkgColor
      ? `
   background-color:${props.bkgColor};`
      : `background-color: ${props.isHpNewLayoutEnabled ? `transparent` : `white`};`}
`;

const StyledColumnItem = styled.View`
  flex-grow: 1;
  display: flex;
  align-items: center;
`;

const StyledContainer = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

const StyledCTAColumn = styled.View``;

export { StyledColumn, StyledContainer, StyledColumnItem, StyledColumnBkgColor, StyledCTAColumn };
