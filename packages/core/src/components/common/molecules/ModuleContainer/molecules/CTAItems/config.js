// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  ctaTypes: {
    stackedCTAButtons: 'stackedCTAList',
    linkList: 'linkCTAList',
    CTAButtonCarousel: 'scrollCTAList',
    divImageCTACarousel: 'imageCTAList',
    // Specific types with expandable text
    stackedCTAButtonsExpandable: 'stackedCTAList',
    CTAButtonCarouselExpandable: 'scrollCTAList',
  },

  ctaTypeProps: {
    stackedCTAList: {
      dualVariation: {
        name: 'dropdownButtonCTA',
        displayProps: {
          small: false,
          medium: true,
          large: true,
        },
      },
    },
    scrollCTAList: {
      dualVariation: {
        name: 'dropdownButtonCTA',
        displayProps: {
          small: false,
          medium: true,
          large: true,
        },
      },
    },
    ctaAddNoButtons: {
      dualVariation: {
        name: 'dropdownButtonCTA',
        displayProps: {
          small: false,
          medium: false,
          large: false,
        },
      },
    },
  },
};
