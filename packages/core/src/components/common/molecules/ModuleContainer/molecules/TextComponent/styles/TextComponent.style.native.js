// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import getTextPadding from '../../../../PageSlots/textPaddingConfig';

const MOBILE = 'mobile';

const StyledContainer = styled.View`
  flex: 1;
  ${props =>
    props.paddingObj.paddingM
      ? `padding-top: ${getTextPadding(props.paddingObj.paddingM, MOBILE)}px`
      : ''};
  ${props =>
    props.paddingObj.paddingMBottom
      ? `padding-bottom: ${getTextPadding(props.paddingObj.paddingMBottom, MOBILE)}px`
      : ''};
  ${props =>
    props.paddingObj.paddingMLeft
      ? `padding-left: ${getTextPadding(props.paddingObj.paddingMLeft, MOBILE)}px`
      : ''};
  ${props =>
    props.paddingObj.paddingMRight
      ? `padding-right: ${getTextPadding(props.paddingObj.paddingMRight, MOBILE)}px`
      : ''};

  ${props =>
    props.color
      ? `
  background-color:${props.color.color};
  `
      : `background-color: ${props.isHpNewLayoutEnabled ? `transparent` : `white`};`}
  width:100%;
`;

export const StyledBorder = styled.View`
  ${props =>
    props.config
      ? `
    padding: 0 ${getTextPadding(props.config.paddingM, MOBILE)}px;
  `
      : ''}
`;

export default StyledContainer;
