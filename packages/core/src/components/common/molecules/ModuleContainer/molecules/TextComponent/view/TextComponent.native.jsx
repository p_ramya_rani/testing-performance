// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import pick from 'lodash/pick';
import { ImageBackground } from 'react-native';
import PropTypes from 'prop-types';
import {
  cropImageUrl,
  getScreenWidth,
  getHeaderBorderRadius,
} from '@tcp/core/src/utils/index.native';
import { LinkText, PromoBanner } from '@tcp/core/src/components/common/molecules';
import Border from '../../../../PageSlots/Border/Border';
import { removeEmpty } from '../../../utils/utils';
import StyledContainer, { StyledBorder } from '../styles/TextComponent.style.native';

const PRICE_POINT_STYLE_PREFIX = 'price_point_style_';
const STACKED = 'stacked';
const FLOW = 'flow';

const HEADER_BORDER_RADIUS_OVERRIDE_MAP = {
  column_1: {
    0: ['col1-header1-top', 'col1-header1-bottom'],
    1: ['col1-header2-top', 'col1-header2-bottom'],
  },
  column_2: {
    0: ['col2-header1-top', 'col2-header1-bottom'],
    1: ['col2-header2-top', 'col2-header2-bottom'],
  },
  column_3: {
    0: ['col3-header1-top', 'col3-header1-bottom'],
    1: ['col3-header2-top', 'col3-header2-bottom'],
  },
  column_4: {
    0: ['col4-header1-top', 'col4-header1-bottom'],
    1: ['col4-header2-top', 'col4-header2-bottom'],
  },
};

const renderBorder = border => {
  const { config } = border || {};
  return (
    border && (
      <StyledBorder config={config}>
        <Border border={border} InModuleContainer />
      </StyledBorder>
    )
  );
};

export const getHeaderBorderRadiusOverride = (
  isHpNewLayoutEnabled,
  styleOverrides,
  overrideMap
) => {
  return (
    isHpNewLayoutEnabled &&
    styleOverrides &&
    overrideMap &&
    getHeaderBorderRadius(styleOverrides[overrideMap[0]], styleOverrides[overrideMap[1]])
  );
};

const applyNewLayoutStyle = (isHpNewLayoutEnabled, headerBorderRadiusOverride) =>
  isHpNewLayoutEnabled && headerBorderRadiusOverride;
const TextComponent = props => {
  const {
    headerTextLine,
    navigation,
    borderWidth,
    styleOverrides,
    colName,
    isHpNewLayoutEnabled,
    index,
  } = props;
  const { textItems, stackingOrder, background, border } = headerTextLine;
  const paddingObj = removeEmpty(
    pick(headerTextLine, ['paddingM', 'paddingMBottom', 'paddingMLeft', 'paddingMRight'])
  );
  const MODULE_WIDTH = getScreenWidth() - 2 * borderWidth;
  let isPromoBannerStyle = false;
  const image = background ? background.image : {};
  const color = background ? background.color : {};
  const { url: Url, url_m: mobileUrl, crop_m: crop } = image || {};
  const imgBgStyle = {
    flex: 1,
    resizeMode: 'stretch',
    width: '100%',
  };
  const imageUri = {
    uri: (mobileUrl || Url) && cropImageUrl(mobileUrl || Url, crop),
  };
  const headerBorderRadiusOverride = getHeaderBorderRadiusOverride(
    isHpNewLayoutEnabled,
    styleOverrides,
    HEADER_BORDER_RADIUS_OVERRIDE_MAP[colName][index]
  );

  if (textItems) {
    textItems.forEach(({ style }) => {
      if (style && style.includes(PRICE_POINT_STYLE_PREFIX)) {
        isPromoBannerStyle = true;
      }
    });
  }

  return (
    <StyledContainer
      paddingObj={paddingObj}
      color={color}
      moduleWidth={MODULE_WIDTH}
      style={isHpNewLayoutEnabled && headerBorderRadiusOverride}
      isHpNewLayoutEnabled={isHpNewLayoutEnabled}
    >
      <ImageBackground
        source={imageUri}
        style={imgBgStyle}
        imageStyle={applyNewLayoutStyle(isHpNewLayoutEnabled, headerBorderRadiusOverride)}
      >
        {renderBorder(border)}
        {isPromoBannerStyle ? (
          <PromoBanner
            promoBanner={[headerTextLine]}
            navigation={navigation}
            locator="moduleContainer_promobanner_text"
            renderInSameLine={stackingOrder === FLOW}
            isContainerModuleText
          />
        ) : (
          <LinkText
            headerText={[headerTextLine]}
            locator="moduleContainer_header_text"
            textAlign="center"
            useStyle
            renderComponentInNewLine={stackingOrder === STACKED}
            navigation={navigation}
            isContainerModuleText
          />
        )}
      </ImageBackground>
    </StyledContainer>
  );
};

TextComponent.propTypes = {
  headerTextLine: PropTypes.shape({}),
  navigation: PropTypes.shape({}).isRequired,
  borderWidth: PropTypes.number,
  styleOverrides: PropTypes.shape({}).isRequired,
  colName: PropTypes.string.isRequired,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
  index: PropTypes.number.isRequired,
};

TextComponent.defaultProps = { headerTextLine: {}, borderWidth: 0 };

export default TextComponent;
export { TextComponent as TextComponentVanilla };
