// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLocator } from '@tcp/core/src/utils/index';
import ButtonList from '@tcp/core/src/components/common/molecules/ButtonList';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { style } from '../styles/CTAItems.style';
import config from '../config';

const STACKED_CTA_LIST = 'stackedCTAList';

const { ctaTypes, ctaTypeProps } = config;

const getButtonListVariation = (ctaType) => {
  const buttonTypes = {
    ...ctaTypes,
  };
  return buttonTypes[ctaType];
};

const getButtonListVariationProps = (ctaType) => {
  const buttonTypeProps = {
    ...ctaTypeProps,
  };
  return buttonTypeProps[ctaType];
};

const getBtnOverrideStyle = (styleOverrides) => {
  return (
    styleOverrides && {
      ...styleOverrides.button,
      'border-bottom-color': styleOverrides.button && styleOverrides.button.color,
    }
  );
};

const CTAItems = (props) => {
  const { ctaItems, styleOverrides, className, isHpNewDesignCTAEnabled } = props;
  const ctaTypeValue = ctaItems[0].ctaType;
  const { backgroundColor } = styleOverrides['nav-bg'] || {};
  const buttonListCtaType = getButtonListVariation(ctaTypeValue);
  const buttonListProps = getButtonListVariationProps(buttonListCtaType);
  const dualVariation =
    ctaItems && ctaItems.length < 3 ? null : buttonListProps && buttonListProps.dualVariation;
  const { expandableTitle } = ctaItems[0];
  const btnOverrideStyle = getBtnOverrideStyle(styleOverrides);

  return buttonListCtaType === STACKED_CTA_LIST || buttonListCtaType === 'scrollCTAList' ? (
    <>
      {ctaItems && (
        <ButtonList
          className="button-list"
          buttonsData={ctaItems}
          buttonListVariation={buttonListCtaType}
          dropdownLabel={expandableTitle}
          dualVariation={expandableTitle && dualVariation}
          dataLocatorDropDown={getLocator('moduleContainer_dropdown')}
          dataLocatorDivisionImages={getLocator('moduleContainer_cta_image')}
          dataLocatorTextCta={getLocator('moduleContainer_cta_links')}
          isCtaComponent
          isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
        />
      )}
    </>
  ) : (
    <div className={`${className} button-list-container ${buttonListCtaType}`}>
      {ctaItems && (
        <ButtonList
          buttonsData={ctaItems}
          dualVariation
          buttonListVariation={buttonListCtaType}
          dataLocatorDivisionImages={getLocator('moduleContainer_cta_image')}
          dataLocatorTextCta={getLocator('moduleContainer_cta_links')}
          bgColor={backgroundColor}
          style={btnOverrideStyle}
          isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
        />
      )}
    </div>
  );
};

CTAItems.propTypes = {
  ctaItems: PropTypes.arrayOf(PropTypes.any).isRequired,
  styleOverrides: PropTypes.shape({}),
  className: PropTypes.string,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
};

CTAItems.defaultProps = { styleOverrides: {}, className: '' };

export default withStyles(CTAItems, style);
export { CTAItems as CTAItemsVanilla };
