// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mockData from '../../../__tests__/__mocks__/mockTextCmp';
import { TextComponentVanilla as TextComponent } from '../view/TextComponent.native';

describe('TextComponent component', () => {
  it('should render correctly', () => {
    const { headerTextLine } = mockData[0].composites;
    const wrapper = shallow(
      <TextComponent
        headerTextLine={headerTextLine[0]}
        navigation={{}}
        index={0}
        colName="column_1"
      />
    ).get(0);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly when isHpNewLayoutEnabled is true', () => {
    const { headerTextLine } = mockData[0].composites;
    const wrapper = shallow(
      <TextComponent
        headerTextLine={headerTextLine[0]}
        navigation={{}}
        index={0}
        colName="column_1"
        isHpNewLayoutEnabled
      />
    ).get(0);
    expect(wrapper).toMatchSnapshot();
  });
});
