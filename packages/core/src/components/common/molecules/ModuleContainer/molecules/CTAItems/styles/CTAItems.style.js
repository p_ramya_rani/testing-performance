// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const LINK_LIST = 'linkList';

const style = css`
  position: relative;

  ${props => (props.ctaItems[0] && props.ctaItems[0].ctaType === LINK_LIST ? `width: 100%` : ``)};

  .button-list-wrapper {
    padding: 0;
  }

  .button-list-container {
    padding: 16px 0;
  }

  .cta-button-text {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .link-button-list-wrapper {
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
    @media ${props => props.theme.mediaQuery.medium} {
      padding-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
      padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    }
  }

  .button-list-container.stackedCTAList {
    padding: 0;
  }

  .link-button-wrapper-class,
  .image-comp,
  .imageCTAList,
  .linkCTAList {
    color: ${props => props.theme.colors.TEXT.DARKERGRAY};
  }

  .img-wrapper:nth-child(1) {
    margin-left: 16px;
  }

  .button-list-container.linkCTAList .link-button-wrapper-class {
    border-bottom-color: ${props => props.theme.colors.WHITE};
    @media ${props => props.theme.mediaQuery.large} {
      ${props =>
        props.theme.isGymboree ? `font-weight: ${props.theme.typography.fontWeights.bold};` : ''}
    }
  }

  @media ${props => props.theme.mediaQuery.medium} {
    .button-list-container.stackedCTAList {
      padding: 16px 0 0;

      .wrapped-button-text .stacked-button .cta-button-text {
        padding: 11px 0;
      }
    }
  }
`;

export { style };

export default {
  style,
};
