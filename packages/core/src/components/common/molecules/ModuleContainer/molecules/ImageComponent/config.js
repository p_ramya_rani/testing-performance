// 9fbef606107a605d69c0edbcd8029e5d 
import { breakpoints } from '../../../../../../../styles/themes/TCP/mediaQuery';

export default {
  CAROUSEL_OPTIONS: {
    autoplay: true,
    arrows: true,
    autoplaySpeed: 4000,
    fade: false,
    speed: 1000,
    dots: true,
    dotsClass: 'slick-dots',
    swipe: true,
    responsive: [
      {
        breakpoint: breakpoints.values.lg,
        settings: {
          arrows: false,
        },
      },
    ],
  },
  IMG_DATA: {
    carouselImgConfig: [
      't_mod_E_carousel_img_m',
      't_mod_E_carousel_img_t',
      't_mod_E_carousel_img_d',
    ],
  },
  VIDEO_DATA: {
    carouselVideoConfig: [
      't_mod_E_carousel_video_m',
      't_mod_E_carousel_video_t',
      't_mod_E_carousel_video_d',
    ],
  },
};
