// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const ButtonContainer = styled.View`
  margin-top: ${props =>
    props.isFifthColumn || props.isHpNewLayoutEnabled ? 0 : props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: 0;
  flex-grow: 1;
  background: ${props => (props.isHpNewLayoutEnabled ? 'transparent' : 'white')};
`;

export const DivImageCTAContainer = styled.View`
  flex-grow: 1;
`;

export const ButtonLinksContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  display: flex;
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  background: #00b7c8;
  margin-left: 0;
  margin-right: 0;
  width: 100%;
`;

export default {
  ButtonContainer,
  DivImageCTAContainer,
  ButtonLinksContainer,
};
