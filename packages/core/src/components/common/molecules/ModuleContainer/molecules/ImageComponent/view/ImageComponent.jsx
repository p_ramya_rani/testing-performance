// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  getLocator,
  getIconPath,
  isGymboree,
  isClient,
  getViewportInfo,
} from '@tcp/core/src/utils';
import { Row, Col, Anchor, DamImage, Button } from '@tcp/core/src/components/common/atoms';
import { getByomMediaBorderRadius } from '@tcp/core/src/utils/utils.web';
import ImageComponentStyle, { Carousel } from '../styles/ImageComponent.style';

import config from '../config';

const MEDIA_BORDER_RADIUS_OVERRIDE_MAP = {
  column_1: [
    'col1-media-top-left',
    'col1-media-top-right',
    'col1-media-bottom-left',
    'col1-media-bottom-right',
  ],
  column_2: [
    'col2-media-top-left',
    'col2-media-top-right',
    'col2-media-bottom-left',
    'col2-media-bottom-right',
  ],
  column_3: [
    'col3-media-top-left',
    'col3-media-top-right',
    'col3-media-bottom-left',
    'col3-media-bottom-right',
  ],
  column_4: [
    'col4-media-top-left',
    'col4-media-top-right',
    'col4-media-bottom-left',
    'col4-media-bottom-right',
  ],
};

const getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides, overrideMap) => {
  if (isHpNewModuleDesignEnabled && styleOverrides && overrideMap) {
    return getByomMediaBorderRadius(
      styleOverrides[overrideMap[0]],
      styleOverrides[overrideMap[1]],
      styleOverrides[overrideMap[2]],
      styleOverrides[overrideMap[3]]
    );
  }
  return {};
};

const bigCarrotIcon = 'carousel-big-carrot';
const bigCarrotIconGym = 'carousel-big-carrot-white';

// Carousel Next and Prev Arrows
const CarouselArrow = ({ arrowType, label, onClick }) => {
  return (
    <button
      type="button"
      tabIndex="0"
      aria-label={label}
      data-locator={`moduleContainer_${arrowType}_arrow`}
      className={`carousel-nav-arrow-button carousel-nav-${arrowType}-button`}
      onClick={onClick}
    >
      <svg width="15" height="52" role="img">
        <path
          fill="#1A1A1A"
          d="M.113 50.539a1 1 0 001.774.923l13-25a1 1 0 000-.923l-13-25a1 1 0 10-1.774.923L12.873 26 .113 50.54z"
        />
      </svg>
    </button>
  );
};

CarouselArrow.propTypes = {
  arrowType: PropTypes.string.isRequired,
  label: PropTypes.string,
  onClick: PropTypes.func,
};

const renderDifferentImageOnMobile = (showDifferentImageOnMobile, showVideoOnMobile) => {
  const { isMobile } = getViewportInfo() || {};
  return isClient() && isMobile && showDifferentImageOnMobile && !showVideoOnMobile;
};

const getImage = (
  { image, imageMobile, video, videoMobile, singleCTAButton, mediaBorderRadiusOverride },
  IMG_DATA,
  carouselCtaType,
  index,
  isOnlyImage
) => {
  const imgData =
    !isEmpty(image) && !isEmpty(mediaBorderRadiusOverride)
      ? Object.assign(image, mediaBorderRadiusOverride)
      : image;
  const videoData =
    !isEmpty(video) && !isEmpty(mediaBorderRadiusOverride)
      ? Object.assign(video, mediaBorderRadiusOverride)
      : video;
  const imgMData =
    !isEmpty(imageMobile) && !isEmpty(mediaBorderRadiusOverride)
      ? Object.assign(imageMobile, mediaBorderRadiusOverride)
      : imageMobile;
  const videoMData =
    !isEmpty(videoMobile) && !isEmpty(mediaBorderRadiusOverride)
      ? Object.assign(videoMobile, mediaBorderRadiusOverride)
      : videoMobile;

  const { url, text, target } = singleCTAButton || {};
  const showDifferentImageOnMobile = !!imageMobile;
  const showVideoOnMobile = !!videoMobile;

  return (
    <div className={`${isOnlyImage ? 'single-image' : null}`}>
      {renderDifferentImageOnMobile(showDifferentImageOnMobile, showVideoOnMobile) ? (
        <DamImage
          imgData={imgMData}
          dataLocator={`${getLocator('moduleContainer_carousel_image_img')}${index + 1}`}
          className="module-container-img-full-width"
          link={singleCTAButton}
          isOptimizedVideo
          isHomePage
          lazyLoad
          isModule
        />
      ) : (
        <DamImage
          imgData={imgData}
          dataLocator={`${getLocator('moduleContainer_carousel_image_img')}${index + 1}`}
          className="module-container-img-full-width"
          link={singleCTAButton}
          videoData={videoData}
          showVideoOnMobile={showVideoOnMobile}
          videoDataMobile={videoMData}
          isOptimizedVideo
          isHomePage
          lazyLoad
          isModule
        />
      )}
      {carouselCtaType === 'link' && singleCTAButton && (
        <Anchor
          dataLocator={`${getLocator('moduleContainer_carousel_image_link_cta')}${index + 1}`}
          className="carousel-cta-link"
          url={url}
          title={text}
          target={target}
          underline
          anchorVariation="primary"
          fontSizeVariation="large"
        >
          {text}
        </Anchor>
      )}
    </div>
  );
};

const getModuleContainerCarousel = (
  largeCompImageSimpleCarousel,
  CAROUSEL_OPTIONS,
  carouselConfig,
  IMG_DATA,
  carouselCtaType,
  mediaBorderRadiusOverride
) => {
  return (
    <Carousel options={CAROUSEL_OPTIONS} carouselConfig={carouselConfig} isModule>
      {largeCompImageSimpleCarousel.map(({ image, video, singleCTAButton }, index) => {
        return getImage(
          { image, video, singleCTAButton, mediaBorderRadiusOverride },
          IMG_DATA,
          carouselCtaType,
          index
        );
      })}
    </Carousel>
  );
};

const getCarouselOrImage = (
  largeCompImageSimpleCarousel,
  CAROUSEL_OPTIONS,
  carouselConfig,
  IMG_DATA,
  carouselCtaType,
  mediaBorderRadiusOverride
) => {
  const { image, imageMobile, video, videoMobile, singleCTAButton } =
    largeCompImageSimpleCarousel[0] || {};
  return largeCompImageSimpleCarousel.length > 1
    ? getModuleContainerCarousel(
        largeCompImageSimpleCarousel,
        CAROUSEL_OPTIONS,
        carouselConfig,
        IMG_DATA,
        carouselCtaType,
        mediaBorderRadiusOverride
      )
    : getImage(
        { image, imageMobile, video, videoMobile, singleCTAButton, mediaBorderRadiusOverride },
        IMG_DATA,
        carouselCtaType,
        0,
        true
      );
};

const ImageComponent = (props) => {
  const {
    largeCompImageSimpleCarousel,
    className,
    styleOverrides,
    isHpNewModuleDesignEnabled,
    colName,
  } = props;
  const carouselCtaType = largeCompImageSimpleCarousel[0].ctaType;
  const { playIconButton, pauseIconButton, previousButton, nextIconButton } = {};
  const { IMG_DATA, CAROUSEL_OPTIONS } = config;
  const carouselIcon = isGymboree() ? bigCarrotIconGym : bigCarrotIcon;
  const [curCarouselSlideIndex, setCurCarouselSlideIndex] = useState(0);
  const sliderFadeOptions = styleOverrides['slider-fade'];
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewModuleDesignEnabled,
    styleOverrides,
    MEDIA_BORDER_RADIUS_OVERRIDE_MAP[colName]
  );

  const carouselConfig = {
    autoplay: true,
    dataLocatorPlay: getLocator('moduleContainer_play_button'),
    dataLocatorPause: getLocator('moduleContainer_pause_button'),
    customArrowLeft: getIconPath(carouselIcon),
    customArrowRight: getIconPath(carouselIcon),
    dataLocatorCarousel: 'carousel_banner',
  };

  CAROUSEL_OPTIONS.prevArrow = <CarouselArrow arrowType="prev" label={previousButton} />;
  CAROUSEL_OPTIONS.nextArrow = <CarouselArrow arrowType="next" label={nextIconButton} />;
  carouselConfig.autoplay = carouselConfig.autoplay && largeCompImageSimpleCarousel.length > 1;
  carouselConfig.pauseIconButtonLabel = pauseIconButton;
  carouselConfig.playIconButtonLabel = playIconButton;

  CAROUSEL_OPTIONS.afterChange = (index) => {
    setCurCarouselSlideIndex(index);
  };

  const overrideCarouselOptions = !isEmpty(sliderFadeOptions)
    ? {
        fade: true,
        cssEase: 'linear',
        infinite: true,
        autoplaySpeed: parseFloat(sliderFadeOptions.speed),
        speed: 50,
      }
    : {};

  return (
    <Row fullBleed className={`${className} carousel-image-layout`}>
      <Col
        colSize={{
          small: 6,
          medium: 8,
          large: 12,
        }}
      >
        {getCarouselOrImage(
          largeCompImageSimpleCarousel,
          { ...CAROUSEL_OPTIONS, ...overrideCarouselOptions },
          carouselConfig,
          IMG_DATA,
          carouselCtaType,
          mediaBorderRadiusOverride
        )}
        {carouselCtaType === 'button' &&
          largeCompImageSimpleCarousel[curCarouselSlideIndex].singleCTAButton && (
            <Row
              fullBleed={{
                small: true,
                medium: true,
                large: true,
              }}
            >
              <Col
                colSize={{
                  small: 6,
                  medium: 8,
                  large: 12,
                }}
              >
                <Button
                  buttonVariation="fixed-width"
                  dataLocator={`${getLocator(
                    'moduleContainer_carousel_image_button_cta'
                  )}${curCarouselSlideIndex}`}
                  className="carousel-cta-button"
                  cta={largeCompImageSimpleCarousel[curCarouselSlideIndex].singleCTAButton}
                >
                  {largeCompImageSimpleCarousel[curCarouselSlideIndex].singleCTAButton.text}
                </Button>
              </Col>
            </Row>
          )}
      </Col>
    </Row>
  );
};

ImageComponent.propTypes = {
  largeCompImageSimpleCarousel: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
  styleOverrides: PropTypes.shape({}),
  className: PropTypes.string,
};

ImageComponent.defaultProps = { styleOverrides: {}, className: '' };

export default withStyles(ImageComponent, ImageComponentStyle);
export { ImageComponent as ImageComponentVanilla };
