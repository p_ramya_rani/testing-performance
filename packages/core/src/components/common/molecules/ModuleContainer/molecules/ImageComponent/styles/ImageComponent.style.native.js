// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { Anchor, Button } from '@tcp/core/src/components/common/atoms';

export const ContainerView = styled.View`
  justify-content: center;
  flex: 1;
  display: flex;
  align-items: center;
  background-color: ${props => (props.isHpNewLayoutEnabled ? `transparent` : `white`)};
`;

export const StyledAnchor = styled(Anchor)`
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const FloatingButton = styled.View`
  margin-top: 10px;
  position: absolute;
  margin-bottom: 50px;
  bottom: 0;
`;

export const StackCTAButtonWrapper = styled.View`
  background-color: white;
  width: 100%;
`;

export const StyledButton = styled(Button)`
  margin-left: ${props => props.borderWidth || 0}px;
  margin-right: ${props => props.borderWidth || 0}px;
`;

export default {
  ContainerView,
  StyledAnchor,
  FloatingButton,
  StackCTAButtonWrapper,
  StyledButton,
};
