// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components';
import { cropImageUrl } from '@tcp/core/src/utils';
import getTextPadding from '../../../../PageSlots/textPaddingConfig';

const MOBILE = 'mobile';
const TABLET = 'tablet';
const DESKTOP = 'desktop';

const getDesktopWidth = paddingObj => {
  let widthD;
  const { paddingDLeft, paddingDRight } = paddingObj || {};
  if (paddingDLeft && paddingDRight) {
    widthD = `calc(100% - ${getTextPadding(paddingDRight, DESKTOP) +
      getTextPadding(paddingDLeft, DESKTOP)}px)`;
  } else if (paddingDRight) {
    widthD = `calc(100% - ${getTextPadding(paddingDRight, DESKTOP)}px)`;
  } else if (paddingDLeft) {
    widthD = `calc(100% - ${getTextPadding(paddingDLeft, DESKTOP)}px)`;
  } else {
    widthD = '100%';
  }
  return widthD;
};

const getTabletWidth = paddingObj => {
  let widthT;
  const { paddingTLeft, paddingTRight } = paddingObj || {};
  if (paddingTLeft && paddingTRight) {
    widthT = `calc(100% - ${getTextPadding(paddingTRight, TABLET) +
      getTextPadding(paddingTLeft, TABLET)}px)`;
  } else if (paddingTRight) {
    widthT = `calc(100% - ${getTextPadding(paddingTRight, TABLET)}px)`;
  } else if (paddingTLeft) {
    widthT = `calc(100% - ${getTextPadding(paddingTLeft, TABLET)}px)`;
  } else {
    widthT = '100%';
  }
  return widthT;
};

const getMobileWidth = paddingObj => {
  let widthM;
  const { paddingMLeft, paddingMRight } = paddingObj || {};
  if (paddingMLeft && paddingMRight) {
    widthM = `calc(100% - ${getTextPadding(paddingMRight, MOBILE) +
      getTextPadding(paddingMLeft, MOBILE)}px)`;
  } else if (paddingMRight) {
    widthM = `calc(100% - ${getTextPadding(paddingMRight, MOBILE)}px)`;
  } else if (paddingMLeft) {
    widthM = `calc(100% - ${getTextPadding(paddingMLeft, MOBILE)}px)`;
  } else {
    widthM = '100%';
  }
  return widthM;
};

export const StyledTextWrapper = styled.div`
  ${props =>
    props.image
      ? `background-image: url(
    ${cropImageUrl(props.image.url_m ? props.image.url_m : props.image.url)});
    background-size: cover;
    background-repeat: no-repeat;
    `
      : ''}
  ${props => (props.color ? `background-color:${props.color.color};` : '')}

  ${props =>
    props.paddingObj.paddingM
      ? `padding-top: ${getTextPadding(props.paddingObj.paddingM, MOBILE)}px`
      : ''};
  ${props =>
    props.paddingObj.paddingMBottom
      ? `padding-bottom: ${getTextPadding(props.paddingObj.paddingMBottom, MOBILE)}px`
      : ''};
  ${props =>
    props.paddingObj.paddingMLeft
      ? `padding-left: ${getTextPadding(props.paddingObj.paddingMLeft, MOBILE)}px`
      : ''};
  ${props =>
    props.paddingObj.paddingMRight
      ? `padding-right: ${getTextPadding(props.paddingObj.paddingMRight, MOBILE)}px`
      : ''};

  width: ${props => getMobileWidth(props.paddingObj)};

  @media ${props => props.theme.mediaQuery.mediumOnly} {
    ${props =>
      props.image
        ? `background-image: url(
    ${cropImageUrl(props.image.url_t ? props.image.url_t : props.image.url)});
    `
        : ''}

    ${props =>
      props.paddingObj.paddingT
        ? `padding-top: ${getTextPadding(props.paddingObj.paddingT, TABLET)}px`
        : ''};
    ${props =>
      props.paddingObj.paddingTBottom
        ? `padding-bottom: ${getTextPadding(props.paddingObj.paddingTBottom, TABLET)}px`
        : ''};
    ${props =>
      props.paddingObj.paddingTLeft
        ? `padding-left: ${getTextPadding(props.paddingObj.paddingTLeft, TABLET)}px`
        : ''};
    ${props =>
      props.paddingObj.paddingTRight
        ? `padding-right: ${getTextPadding(props.paddingObj.paddingTRight, TABLET)}px`
        : ''};

    width: ${props => getTabletWidth(props.paddingObj)};
  }
  @media ${props => props.theme.mediaQuery.large} {
    ${props =>
      props.image
        ? `background-image: url(
    ${cropImageUrl(props.image.url ? props.image.url : '')});
    `
        : ''}

    ${props =>
      props.paddingObj.paddingD
        ? `padding-top: ${getTextPadding(props.paddingObj.paddingD, DESKTOP)}px`
        : ''};
    ${props =>
      props.paddingObj.paddingDBottom
        ? `padding-bottom: ${getTextPadding(props.paddingObj.paddingDBottom, DESKTOP)}px`
        : ''};
    ${props =>
      props.paddingObj.paddingDLeft
        ? `padding-left: ${getTextPadding(props.paddingObj.paddingDLeft, DESKTOP)}px`
        : ''};
    ${props =>
      props.paddingObj.paddingDRight
        ? `padding-right: ${getTextPadding(props.paddingObj.paddingDRight, DESKTOP)}px`
        : ''};

    width: ${props => getDesktopWidth(props.paddingObj)};
  }
  .promo-text-link,
  .link-text {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: ${props => (props.stackingOrder === 'flow' ? 'row' : 'column')};
  }
  .promo-text {
    &:not(:last-child) {
      ${props => (props.stackingOrder === 'flow' ? 'margin-right: 24px;' : '')}
    }
  }
  .link-text span:not(:last-child) {
    ${props => (props.stackingOrder === 'flow' ? 'margin-right: 24px;' : '')}
  }

  &&& {
    margin-bottom: 0;
  }
`;

export const StyledBorder = styled.div`
  ${props =>
    props.config
      ? `
    padding: 0 ${getTextPadding(props.config.paddingM, MOBILE)}px;
    width: calc(100% - 2*${getTextPadding(props.config.paddingM, MOBILE)}px);
    @media ${props.theme.mediaQuery.mediumOnly} {
      padding: 0 ${getTextPadding(props.config.paddingT, TABLET)}px;
      width: calc(100% - 2*${getTextPadding(props.config.paddingT, TABLET)}px);
    }
    @media ${props.theme.mediaQuery.large} {
      padding: 0 ${getTextPadding(props.config.paddingD, DESKTOP)}px;
      width: calc(100% - 2*${getTextPadding(props.config.paddingD, DESKTOP)}px);
    }
  `
      : ''}
`;

export default css``;
