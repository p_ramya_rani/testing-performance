// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { DamImage, Anchor } from '@tcp/core/src/components/common/atoms';
import {
  getScreenWidth,
  LAZYLOAD_HOST_NAME,
  getMediaBorderRadius,
} from '@tcp/core/src/utils/index.native';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel';
import HP_NEW_LAYOUT from '@tcp/core/src/constants/hpNewLayout.constants';
import {
  ContainerView,
  StyledAnchor,
  FloatingButton,
  StackCTAButtonWrapper,
  StyledButton,
} from '../styles/ImageComponent.style.native';

const MODULE_DEFAULT_HEIGHT = 400;

const IMG_HEIGHT_OVERRIDE_MAP = {
  column_1: 'col1-imgHeight',
  column_2: 'col2-imgHeight',
  column_3: 'col3-imgHeight',
  column_4: 'col4-imgHeight',
};

const BORDER_RADIUS_OVERRIDE_MAP = {
  column_1: ['col1-media-top', 'col1-media-bottom'],
  column_2: ['col2-media-top', 'col2-media-bottom'],
  column_3: ['col3-media-top', 'col3-media-bottom'],
  column_4: ['col4-media-top', 'col4-media-bottom'],
};

/**
 * Render DamImage
 * @param {object} singleCTAButton - button config
 * @param {object} imgData - image config
 * @param {object} videoData - video config
 * @param {object} navigation - navigation object
 */
const renderDamImage = (
  singleCTAButton,
  imgData,
  videoData,
  navigation,
  MODULE_WIDTH,
  { overrideHeight, mediaBorderRadiusOverride }
) => {
  const damImageComp = (
    <DamImage
      width={MODULE_WIDTH}
      videoData={videoData}
      height={overrideHeight || MODULE_DEFAULT_HEIGHT}
      imgData={imgData}
      host={LAZYLOAD_HOST_NAME.HOME}
      crop={imgData && imgData.crop_m}
      resizeMode="stretch"
      isFastImage
      isHomePage
      navigation={navigation}
      link={singleCTAButton}
      overrideStyle={mediaBorderRadiusOverride}
      isBackground={false}
    />
  );
  if (imgData && Object.keys(imgData).length > 0) {
    return (
      <Anchor navigation={navigation} url={singleCTAButton.url}>
        {damImageComp}
      </Anchor>
    );
  }
  return videoData && Object.keys(videoData).length > 0 ? (
    <React.Fragment>{damImageComp}</React.Fragment>
  ) : null;
};

const getMediaImage = (imageMobile, videoMobile, image) => {
  if (!videoMobile) return imageMobile || image;
  return null;
};

const getMediaVideo = (imageMobile, videoMobile, video) => {
  if (!imageMobile) return videoMobile || video;
  return null;
};

/**
 * Render View
 * @param {object} item - carousel item object
 * @param {object} navigation - navigation object
 * @param {string} carouselCtaType - type of the carousel cta
 */
const renderView = (
  item,
  navigation,
  carouselCtaType,
  MODULE_WIDTH,
  { overrideHeight, mediaBorderRadiusOverride }
) => {
  const {
    item: { image, imageMobile, singleCTAButton, video, videoMobile },
  } = item;

  const mediaImage = getMediaImage(imageMobile, videoMobile, image);
  const mediaVideo = getMediaVideo(imageMobile, videoMobile, video);

  return (
    <View>
      {renderDamImage(singleCTAButton, mediaImage, mediaVideo, navigation, MODULE_WIDTH, {
        overrideHeight,
        mediaBorderRadiusOverride,
      })}
      {carouselCtaType === 'link' ? (
        <FloatingButton>
          <StyledAnchor
            text={singleCTAButton.text}
            url={singleCTAButton.url}
            navigation={navigation}
            underline
            centered
            fontSizeVariation="large"
            anchorVariation="custom"
            colorName="gray.900"
          />
        </FloatingButton>
      ) : null}
    </View>
  );
};

/**
 * Render Carousel
 * @param {object} largeCompImageSimpleCarousel - config object for the image carousel
 * @param {object} navigation - navigation object
 * @param {string} carouselCtaType - type of carousel cta
 */
const renderCarousel = (
  largeCompImageSimpleCarousel,
  navigation,
  carouselCtaType,
  setCurCarouselIndex,
  MODULE_WIDTH,
  { overrideHeight, mediaBorderRadiusOverride, isHpNewLayoutEnabled }
) => {
  return (
    <ContainerView isHpNewLayoutEnabled={isHpNewLayoutEnabled}>
      {largeCompImageSimpleCarousel.length > 1 ? (
        <Carousel
          height={overrideHeight || MODULE_DEFAULT_HEIGHT}
          data={largeCompImageSimpleCarousel}
          renderItem={(item) =>
            renderView(item, navigation, carouselCtaType, MODULE_WIDTH, {
              overrideHeight,
              mediaBorderRadiusOverride,
            })
          }
          onSnapToItem={(index) => setCurCarouselIndex(index)}
          width={MODULE_WIDTH}
          carouselConfig={{
            autoplay: true,
          }}
          showDots
          overlap
          isModule
        />
      ) : (
        <View>
          {renderView(
            { item: largeCompImageSimpleCarousel[0] },
            navigation,
            undefined,
            MODULE_WIDTH,
            { overrideHeight, mediaBorderRadiusOverride }
          )}
        </View>
      )}
    </ContainerView>
  );
};

const renderButton = (navigation, largeCompImageSimpleCarousel, curCarouselIndex, borderWidth) => {
  const {
    singleCTAButton: { text, url },
  } = largeCompImageSimpleCarousel[curCarouselIndex];

  return (
    <StackCTAButtonWrapper>
      <StyledButton text={text} url={url} navigation={navigation} borderWidth={borderWidth} />
    </StackCTAButtonWrapper>
  );
};

export const getMediaBorderRadiusOverride = (styleOverrides, overrideMap) => {
  return getMediaBorderRadius(styleOverrides[overrideMap[0]], styleOverrides[overrideMap[1]]);
};

/**
 * Image Component
 * @param {object} props - props from the parent component (MODULE CONTAINER)
 * @returns Image Component.
 */
const ImageComponent = (props) => {
  const {
    largeCompImageSimpleCarousel,
    navigation,
    borderWidth,
    styleOverrides,
    colName,
    isHpNewLayoutEnabled,
  } = props;
  const carouselCtaType = largeCompImageSimpleCarousel[0].ctaType;
  const MODULE_WIDTH = isHpNewLayoutEnabled
    ? getScreenWidth() - 2 * borderWidth - 2 * HP_NEW_LAYOUT.BODY_PADDING
    : getScreenWidth() - 2 * borderWidth;
  const { imgCmpHeight: { height } = {} } = styleOverrides || {};
  const { height: colImgHeight } = styleOverrides[IMG_HEIGHT_OVERRIDE_MAP[colName]] || {};
  const overrideHeight = parseFloat(colImgHeight || height);
  const [curCarouselIndex, setCurCarouselIndex] = useState(0);
  const mediaBorderRadiusOverride =
    isHpNewLayoutEnabled &&
    styleOverrides &&
    getMediaBorderRadiusOverride(styleOverrides, BORDER_RADIUS_OVERRIDE_MAP[colName]);

  return (
    <>
      {renderCarousel(
        largeCompImageSimpleCarousel,
        navigation,
        carouselCtaType,
        setCurCarouselIndex,
        MODULE_WIDTH,
        { overrideHeight, mediaBorderRadiusOverride, isHpNewLayoutEnabled }
      )}
      {carouselCtaType === 'button'
        ? renderButton(navigation, largeCompImageSimpleCarousel, curCarouselIndex, borderWidth)
        : null}
    </>
  );
};

ImageComponent.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  largeCompImageSimpleCarousel: PropTypes.shape([]).isRequired,
  styleOverrides: PropTypes.shape({}),
  borderWidth: PropTypes.number,
  colName: PropTypes.string.isRequired,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

ImageComponent.defaultProps = { styleOverrides: {}, borderWidth: 0 };

export default ImageComponent;
export { ImageComponent as ImageComponentVanilla };
