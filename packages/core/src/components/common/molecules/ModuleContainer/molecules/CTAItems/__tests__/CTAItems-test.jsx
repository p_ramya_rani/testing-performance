// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import {
  columnDataWithCTAButtonCarousel,
  columnDataWithLinkListCTA,
} from '../../../__tests__/__mocks__/mockCtaItems';
import { CTAItemsVanilla as CTAItems } from '../view/CTAItems';

describe('CTA component', () => {
  it('should render correctly', () => {
    const styleOverrides = {};
    const wrapper = shallow(
      <CTAItems
        ctaItems={columnDataWithCTAButtonCarousel[2].composites.ctaItemsComposite}
        navigation={{}}
        styleOverrides={styleOverrides}
      />
    ).get(0);
    expect(wrapper).toMatchSnapshot();
  });
  it('should render link list button correctly', () => {
    const styleOverrides = { button: { color: 'red' } };
    const wrapper = shallow(
      <CTAItems
        ctaItems={columnDataWithLinkListCTA[1].composites.ctaItemsComposite}
        navigation={{}}
        styleOverrides={styleOverrides}
      />
    ).get(0);
    expect(wrapper).toMatchSnapshot();
  });
});
