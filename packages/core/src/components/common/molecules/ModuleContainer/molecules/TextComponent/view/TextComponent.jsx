// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import pick from 'lodash/pick';
import PropTypes from 'prop-types';

import { LinkText, PromoBanner } from '@tcp/core/src/components/common/molecules';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { removeEmpty } from '../../../utils/utils';
import TextComponentStyles, {
  StyledTextWrapper,
  StyledBorder,
} from '../styles/TextComponent.style';
import Border from '../../../../PageSlots/Border/Border';

const PRICE_POINT_STYLE_PREFIX = 'price_point_style_';

const TextComponent = (props) => {
  const { headerTextLine, navigation, className } = props;
  const { textItems, stackingOrder, background = {}, border } = headerTextLine;
  const { config } = border || {};
  const paddingObj = removeEmpty(
    pick(headerTextLine, [
      'paddingD',
      'paddingDBottom',
      'paddingDLeft',
      'paddingDRight',
      'paddingT',
      'paddingTBottom',
      'paddingTLeft',
      'paddingTRight',
      'paddingM',
      'paddingMBottom',
      'paddingMLeft',
      'paddingMRight',
    ])
  );
  let isPromoBannerStyle = false;
  const image = background ? background.image : null;
  const color = background ? background.color : null;
  if (textItems) {
    textItems.forEach(({ style }) => {
      if (style && style.includes(PRICE_POINT_STYLE_PREFIX)) {
        isPromoBannerStyle = true;
      }
    });
  }

  return (
    <StyledTextWrapper
      image={image}
      color={color}
      paddingObj={paddingObj}
      stackingOrder={stackingOrder}
      className={className}
    >
      {border && (
        <StyledBorder className="styled-border" config={config}>
          <Border border={border} InModuleContainer />
        </StyledBorder>
      )}
      {isPromoBannerStyle ? (
        <PromoBanner
          promoBanner={[headerTextLine]}
          navigation={navigation}
          locator="moduleContainer_promobanner_text"
          isContainerModuleText
        />
      ) : (
        <LinkText
          headerText={[headerTextLine]}
          locator="moduleContainer_header_text"
          textAlign="center"
          useStyle
          navigation={navigation}
        />
      )}
    </StyledTextWrapper>
  );
};

TextComponent.propTypes = {
  headerTextLine: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}),
  className: PropTypes.string,
};

TextComponent.defaultProps = {
  className: '',
};

export default withStyles(TextComponent, TextComponentStyles);
export { TextComponent as TextComponentVanilla };
