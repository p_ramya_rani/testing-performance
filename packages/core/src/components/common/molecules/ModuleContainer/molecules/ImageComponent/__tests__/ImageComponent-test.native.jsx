// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import mockData from '../../../__tests__/__mocks__/mockImageCmp';
import { ImageComponentVanilla as ImageComponent } from '../view/ImageComponent.native';

describe('ImageComponent component', () => {
  it('should render correctly', () => {
    const styleOverrides = { imgCmpHeight: { height: '200' } };
    const wrapper = shallow(
      <ImageComponent
        navigation={{}}
        styleOverrides={styleOverrides}
        borderWidth={0}
        largeCompImageSimpleCarousel={mockData[0].composites.largeCompImageSimpleCarouselComposite}
      />
    ).get(0);
    expect(wrapper).toMatchSnapshot();
  });
});
