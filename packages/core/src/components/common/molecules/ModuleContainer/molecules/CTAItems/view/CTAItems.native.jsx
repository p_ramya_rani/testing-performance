// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import ButtonList from '@tcp/core/src/components/common/molecules/ButtonList';
import { getCtaBorderRadius } from '@tcp/core/src/utils/utils.app';
import config from '../config';
import {
  ButtonLinksContainer,
  ButtonContainer,
  DivImageCTAContainer,
} from '../styles/CTAItems.style.native';

const BORDER_RADIUS_OVERRIDE_MAP = {
  column_1: ['col1-cta-top', 'col1-cta-bottom'],
  column_2: ['col2-cta-top', 'col2-cta-bottom'],
  column_3: ['col3-cta-top', 'col3-cta-bottom'],
  column_4: ['col4-cta-top', 'col4-cta-bottom'],
  column_5: ['col5-cta-top', 'col5-cta-bottom'],
};

const { ctaTypes } = config;

const getButtonListVariation = (ctaType) => {
  const buttonTypes = {
    ...ctaTypes,
  };
  return buttonTypes[ctaType];
};

const renderButtonList = (
  ctaType,
  navigation,
  ctaItems,
  locator,
  buttonVariation,
  color,
  { styleOverrides, ctaBorderRadiusOverride }
) => {
  return (
    ctaItems && (
      <ButtonList
        buttonListVariation={ctaType}
        navigation={navigation}
        buttonsData={ctaItems}
        locator={locator}
        color={color}
        isContainerModule
        buttonColor={styleOverrides && styleOverrides.button}
        overrideStyle={ctaBorderRadiusOverride}
      />
    )
  );
};

export const getCtaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides, overrideMap) => {
  if (isHpNewLayoutEnabled && styleOverrides && overrideMap) {
    return getCtaBorderRadius(styleOverrides[overrideMap[0]], styleOverrides[overrideMap[1]]);
  }
  return {};
};

const CTAItems = (props) => {
  const { ctaItems, navigation, styleOverrides, isFifthColumn, isHpNewLayoutEnabled, colName } =
    props;
  const ctaTypeValue = ctaItems[0].ctaType;
  const buttonListCtaType = getButtonListVariation(ctaTypeValue);
  const ctaBorderRadiusOverride = getCtaBorderRadiusOverride(
    isHpNewLayoutEnabled,
    styleOverrides,
    BORDER_RADIUS_OVERRIDE_MAP[colName]
  );

  return (
    <>
      {buttonListCtaType === ctaTypes.divImageCTACarousel && (
        <DivImageCTAContainer>
          {renderButtonList(
            buttonListCtaType,
            navigation,
            ctaItems,
            'moduleA_cta_links',
            'black',
            undefined,
            { styleOverrides }
          )}
        </DivImageCTAContainer>
      )}

      {buttonListCtaType === ctaTypes.stackedCTAButtons && (
        <ButtonContainer isFifthColumn={isFifthColumn} isHpNewLayoutEnabled={isHpNewLayoutEnabled}>
          {renderButtonList(
            buttonListCtaType,
            navigation,
            ctaItems,
            'stacked_cta_list',
            'fixed-width',
            'gray',
            { styleOverrides, ctaBorderRadiusOverride }
          )}
        </ButtonContainer>
      )}

      {buttonListCtaType === ctaTypes.CTAButtonCarousel && (
        <ButtonContainer isFifthColumn={isFifthColumn} isHpNewLayoutEnabled={isHpNewLayoutEnabled}>
          {renderButtonList(
            buttonListCtaType,
            navigation,
            ctaItems,
            'scroll_cta_list',
            'gray',
            undefined,
            { styleOverrides }
          )}
        </ButtonContainer>
      )}

      {buttonListCtaType === ctaTypes.linkList && (
        <ButtonLinksContainer style={styleOverrides && styleOverrides['nav-bg']}>
          {renderButtonList(
            buttonListCtaType,
            navigation,
            ctaItems,
            'link_cta_list',
            'gray',
            undefined,
            { styleOverrides }
          )}
        </ButtonLinksContainer>
      )}
    </>
  );
};

CTAItems.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  ctaItems: PropTypes.shape({}).isRequired,
  styleOverrides: PropTypes.shape({}),
  isFifthColumn: PropTypes.bool,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
  colName: PropTypes.string.isRequired,
};

CTAItems.defaultProps = { styleOverrides: {}, isFifthColumn: false };

export default CTAItems;
export { CTAItems as CTAItemsVanilla };
