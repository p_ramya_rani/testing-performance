// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import InAppWebView from '../../../../../../../mobileapp/src/components/features/content/InAppWebView/views/InAppWebView.view';
import { getEGiftCardPageUrl } from '../../../../../utils/utils.app';

const GiftWebViewViewNative = (props) => {
  const { navigation } = props;
  return (
    <InAppWebView pageUrl={getEGiftCardPageUrl()} navigation={navigation} isExternalCampaignFired />
  );
};

GiftWebViewViewNative.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
};

GiftWebViewViewNative.defaultProps = {};

export default React.memo(GiftWebViewViewNative);
