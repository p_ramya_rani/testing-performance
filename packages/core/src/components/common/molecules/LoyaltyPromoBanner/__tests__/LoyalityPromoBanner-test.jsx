import React from 'react';
import { shallow } from 'enzyme';
import ModuleX from '@tcp/core/src/components/common/molecules/ModuleX';
import { LoyaltyPromoBannerVanilla } from '../view/LoyaltyPromoBanner';

describe('LoyaltyPromoBanner-Web', () => {
  let component;
  const props = {
    navigation: {},
    richTextList: [{ text: 'dummy', userType: 'guest' }],
    isUserGuest: true,
    isUserPlcc: false,
  };

  beforeEach(() => {
    component = shallow(<LoyaltyPromoBannerVanilla {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should return loyalty-promo-close-btn', () => {
    const displayComp = component.find("[className='loyalty-promo-close-btn']");
    expect(displayComp.length).toBe(1);
  });

  it('should return loyalty-promo-close-btn-icon', () => {
    const displayComp = component.find("[className='loyalty-promo-close-btn-icon']");
    expect(displayComp.length).toBe(1);
  });

  it('should return ModuleX component value one', () => {
    expect(component.find(ModuleX)).toHaveLength(1);
  });
});
