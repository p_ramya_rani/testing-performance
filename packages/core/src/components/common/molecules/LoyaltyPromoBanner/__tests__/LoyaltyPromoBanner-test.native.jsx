// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { LoyaltyPromoBannerVanilla } from '../view/LoyaltyPromoBanner.native';
import { Touchable } from '../LoyaltyPromoBanner.style.native';

describe('LoyaltyPromoBanner', () => {
  let component;
  const props = {
    navigation: {},
    richTextList: [{ text: 'dummy', userType: 'guest' }],
    isUserGuest: true,
    isUserPlcc: false,
  };

  beforeEach(() => {
    component = shallow(<LoyaltyPromoBannerVanilla {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
  it('should call correctly', () => {
    component.instance().validateView();
    expect(component.state('bannerClosed')).toBe(true);
  });

  it('should return Styled(View) component value one', () => {
    expect(component.find('Styled(View)')).toHaveLength(1);
  });

  it('should return Touchable component value one', () => {
    expect(component.find(Touchable)).toHaveLength(1);
  });

  it('should return Styled(Styled(ImageComp)) component value one', () => {
    expect(component.find('Styled(Styled(ImageComp))')).toHaveLength(1);
  });
});
