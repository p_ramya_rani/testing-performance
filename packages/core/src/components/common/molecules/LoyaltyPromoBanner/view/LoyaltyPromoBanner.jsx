// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ModuleX from '@tcp/core/src/components/common/molecules/ModuleX';
import { isServer } from '@tcp/core/src/utils';
import { Row, Col } from '../../../atoms';
import { readCookie, setCookie } from '../../../../../utils/cookie.util';
import withStyles from '../../../hoc/withStyles';
import style from '../LoyaltyPromoBanner.style';

/**
 * Check if required UUID cookie available else return default.
 */
const getUUID = (uuidCookieString) => {
  const UUID = readCookie(uuidCookieString);
  return UUID ? UUID.split(',')[0] : '-1002';
};

const getUserType = (isUserGuest, isUserPlcc) => {
  let userType = '';
  if (isUserGuest || isServer()) {
    userType = 'guest';
  } else if (isUserPlcc) {
    userType = 'plcc';
  } else {
    userType = 'mpr';
  }
  return userType;
};

const getPromoContent = (richTextList, userType) => {
  return richTextList.find((promo) => promo && promo.userType === userType);
};

const getPromoDisplay = (richText, className, closeButtonHandler) => {
  return (
    <>
      {richText ? (
        <div className={`${className} content-wrapper`}>
          <Row>
            <Col
              colSize={{
                small: 6,
                medium: 8,
                large: 12,
              }}
            >
              <ModuleX richTextList={[{ text: richText }]} loyaltyLocation="global-header" />
              <button
                aria-label="close"
                className="loyalty-promo-close-btn"
                onClick={closeButtonHandler}
              >
                <svg
                  className="loyalty-promo-close-btn-icon"
                  viewBox="0 0 25 25"
                  aria-hidden="true"
                  role="img"
                >
                  <path
                    fill="#a0a0a0"
                    fillRule="nonzero"
                    d="M14.107 12.5l10.56-10.56A1.136 1.136 0 1 0 23.06.333L12.5 10.893 1.94.333A1.136 1.136 0 1 0 .333 1.94l10.56 10.56L.333 23.06a1.136 1.136 0 1 0 1.607 1.607l10.56-10.56 10.56 10.56c.222.222.513.333.804.333a1.136 1.136 0 0 0 .803-1.94L14.107 12.5z"
                  />
                </svg>
              </button>
            </Col>
          </Row>
        </div>
      ) : null}
    </>
  );
};

/* istanbul ignore next */
const LoyaltyPromoBanner = (props) => {
  const { className, cookieID, richTextList, isUserGuest, isUserPlcc } = props;
  const cookieName = `${cookieID}_${getUUID('WC_USERACTIVITY_')}`;
  const cookieValFromHead = !isServer() ? window.mpr30CookiePresent : false;
  const [bannerClosed, setBannerClosed] = useState(cookieValFromHead);
  const userType = getUserType(isUserGuest, isUserPlcc);
  const richTextObj = richTextList && getPromoContent(richTextList, userType);
  const richText = richTextObj && richTextObj.text;
  const [isClient, setIsClient] = useState(false);

  useEffect(() => {
    if (window.mpr30CookiePresent && !bannerClosed) {
      setBannerClosed(true);
    }
  }, [bannerClosed]);

  useEffect(() => {
    if (!isServer()) setIsClient(true);
  }, [isClient]);

  const closeButtonHandler = () => {
    const currentDate = new Date();
    setCookie({
      key: cookieName,
      value: currentDate.toGMTString(),
      daysAlive: 10,
    });
    if (typeof window === 'object') window.mpr30CookiePresent = true;
    setBannerClosed(true);
  };

  if (!isClient) return getPromoDisplay(richText, className, closeButtonHandler);
  return getPromoDisplay(
    richText,
    `${className} ${bannerClosed ? 'bannerClosed' : ''}`,
    closeButtonHandler
  );
};

LoyaltyPromoBanner.propTypes = {
  className: PropTypes.string.isRequired,
  cookieID: PropTypes.string.isRequired,
  richTextList: PropTypes.arrayOf(PropTypes.object),
  isUserGuest: PropTypes.bool.isRequired,
  isUserPlcc: PropTypes.bool.isRequired,
};

LoyaltyPromoBanner.defaultProps = {
  richTextList: [{ text: '', userType: '' }],
};

export default withStyles(LoyaltyPromoBanner, style);
export { LoyaltyPromoBanner as LoyaltyPromoBannerVanilla };
