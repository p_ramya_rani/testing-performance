// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import PropTypes from 'prop-types';
import { RichText } from '../../../atoms';
import { Wrapper, StyledImage, Touchable } from '../LoyaltyPromoBanner.style.native';

const crossImage = require('../../../../../../src/assets/close.png');

const cookieName = '@mprAboveHead_-1002:key';
const date = new Date();
const currentDateValue = date.getTime();
const daysAlive = 10;

class LoyaltyPromoBanner extends React.PureComponent {
  /**
   * To manage the state of icons on the
   * basis of visible & hide .
   */
  constructor(props) {
    super(props);
    this.state = {
      bannerClosed: false,
    };
  }

  componentDidMount() {
    this.isDisplay();
  }

  /**
   * To save the date
   */
  setDate = () => {
    const expiteDateValue = date.setTime(date.getTime() + daysAlive * 24 * 60 * 60 * 1000);
    try {
      AsyncStorage.setItem(cookieName, expiteDateValue.toString());
    } catch (error) {
      // eslint-disable-next-line rule
      console.info('error', error);
    }
  };

  /**
   * To fetch the date
   */
  retrieveDate = async () => {
    try {
      return await AsyncStorage.getItem(cookieName);
    } catch (error) {
      // Error retrieving data
      return null;
    }
  };

  /**
   * To check the visiblity of the banner
   */
  isDisplay = () => {
    this.retrieveDate().then(data => {
      if (data !== null) {
        if (currentDateValue <= parseInt(data, 10)) {
          this.setState({
            bannerClosed: true,
          });
        } else {
          this.setState({
            bannerClosed: false,
          });
        }
      }
    });
  };

  /**
   * This function validate the iconView.
   */
  validateView = () => {
    this.setDate();
    this.setState({ bannerClosed: true });
  };

  /**
   * This function returns user type.
   */
  getUserType = (isUserGuest, isUserPlcc) => {
    let userType = '';
    if (isUserGuest) {
      userType = 'guest';
    } else if (isUserPlcc) {
      userType = 'plcc';
    } else {
      userType = 'mpr';
    }
    return userType;
  };

  /**
   * This function returns promo content for
   * specific user type.
   */
  getPromoContent = (richTextList, userType) => {
    return richTextList.find(promo => promo && promo.userType === userType);
  };

  /**
   * To render the view
   */
  render() {
    const { richTextList, isUserGuest, isUserPlcc } = this.props;

    const { bannerClosed } = this.state;
    const userType = this.getUserType(isUserGuest, isUserPlcc);
    const richTextObj = richTextList && this.getPromoContent(richTextList, userType);
    const richText = richTextObj && richTextObj.text;
    return (
      <View>
        {bannerClosed ? (
          <View />
        ) : (
          <>
            {richText && (
              <Wrapper>
                <RichText
                  source={{
                    html: richText,
                  }}
                />
                <Touchable
                  accessibilityRole="button"
                  accessibilityLabels="close"
                  onPress={this.validateView}
                >
                  <StyledImage source={crossImage} />
                </Touchable>
              </Wrapper>
            )}
          </>
        )}
      </View>
    );
  }
}

LoyaltyPromoBanner.propTypes = {
  richTextList: PropTypes.arrayOf(PropTypes.object),
  navigation: PropTypes.shape({}).isRequired,
  isUserGuest: PropTypes.bool.isRequired,
  isUserPlcc: PropTypes.bool.isRequired,
};

LoyaltyPromoBanner.defaultProps = {
  richTextList: [{ text: '', userType: '' }],
};

export default LoyaltyPromoBanner;
export { LoyaltyPromoBanner as LoyaltyPromoBannerVanilla };
