/* eslint-disable max-lines */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
// 9fbef606107a605d69c0edbcd8029e5d
// @flow
import React from 'react';
import { Text, View } from 'react-native';
import { Anchor, BodyCopy, Heading } from '../../../atoms';
import { StyledText } from '../../../../../../styles/globalStyles/StyledText.style';
import { StyledImage } from '../LinkText.style.native';
import { isGymboree, safelyParseJSON } from '../../../../../utils/utils';
import AccessibilityRoles from '../../../../../constants/AccessibilityRoles.constant';

const SAME_LINE_CLASS_NAME = 'style3';
const heartIcon = require('../../../../../../../mobileapp/src/assets/images/heart-icon.png');

export const icons = {
  'header-icon__heart': heartIcon,
};

export const bodyCopyStyles = {
  // small text with regular font
  style1: props => (
    <BodyCopy
      color="text.primary"
      fontFamily="tertiary"
      fontSize="fs48"
      fontWeight="medium"
      textAlign="center"
      {...props}
    />
  ),
  // small text with extrabold font
  style2: props => (
    <BodyCopy
      fontFamily="primary"
      fontSize="fs28"
      fontWeight="semibold"
      textAlign="center"
      {...props}
    />
  ),
  // same as 'gymboree_title_text' but to show the text on the same line
  style3: props => (
    <BodyCopy
      fontSize="fs32"
      fontWeight="semibold"
      fontFamily="primary"
      textAlign="center"
      color={isGymboree() ? 'white' : 'text.primary'}
      {...props}
    />
  ),
  // small text with normal font
  small_text_normal: props => (
    <BodyCopy color="gray.900" fontFamily="primary" fontSize="fs14" textAlign="center" {...props} />
  ),
  // large text with bold font
  large_text_black: props => {
    const { text, style } = props;
    const LARGE_TEXT_BLACK_STYLE = { lineHeight: isGymboree() ? 47 : 50, ...style };
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="primary"
        fontSize="fs48"
        fontWeight="black"
        textAlign="center"
        style={LARGE_TEXT_BLACK_STYLE}
        text={text}
        letterSpacing="ls2"
      />
    );
  },
  // large text with bold font
  medium_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs32"
      fontWeight="black"
      textAlign="center"
      {...props}
    />
  ),
  tcp_homepage_medium_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="quaternary"
      fontSize="fs20"
      fontWeight="medium"
      textAlign="center"
      lineHeight="24px"
      {...props}
    />
  ),
  // large text with bold
  medium_text_regular: props => {
    const { style, text } = props;
    const LARGE_TEXT_REGULAR_LINEHEIGHT = { lineHeight: 31, ...style };

    return (
      <BodyCopy
        color="gray.900"
        fontFamily="quaternary"
        fontSize="fs20"
        fontWeight="medium"
        textAlign="center"
        style={LARGE_TEXT_REGULAR_LINEHEIGHT}
        text={text}
      />
    );
  },
  medium_text_subpromo: props => {
    return (
      <BodyCopy
        color="gray.900"
        fontFamily={isGymboree() ? 'primary' : 'secondary'}
        fontSize="fs14"
        fontWeight="regular"
        textAlign="center"
        {...props}
      />
    );
  },
  spaced_text_regular_black: props => {
    return (
      <BodyCopy
        color="gray.900"
        fontFamily="primary"
        fontSize="fs20"
        fontWeight="medium"
        textAlign="center"
        {...props}
      />
    );
  },
  gymboree_title_text: props => {
    return (
      <BodyCopy
        fontSize="fs32"
        fontWeight="semibold"
        fontFamily="primary"
        textAlign="center"
        color={isGymboree() ? 'white' : 'text.primary'}
        {...props}
      />
    );
  },
  gymboree_description: props => (
    <BodyCopy
      fontSize="fs16"
      fontWeight="semibold"
      color="white"
      fontFamily="primary"
      textAlign="center"
      {...props}
    />
  ),
  gym_homepage_gymboree_description: props => (
    // For Module A
    <BodyCopy
      fontFamily="secondary"
      fontSize="fs18"
      fontWeight="regular"
      lineHeight="22px"
      {...props}
    />
  ),
  fixed_medium_text_black: props => {
    return (
      <BodyCopy
        fontSize="fs20"
        lineHeight="22px"
        fontWeight="semibold"
        color="text.primary"
        fontFamily="quaternary"
        textAlign="center"
        {...props}
      />
    );
  },
  tcp_homepage_mod_a_spaced_text: props => {
    const { text, style } = props;
    const shadowStyle = {
      textShadowColor: 'rgba(0, 0, 0, 0.5)',
      textShadowOffset: { width: 0, height: 3 },
      textShadowRadius: 6,
      elevation: 4,
      fontSize: 63,
      lineHeight: 66,
      ...style,
      marginTop: 80,
    };

    return (
      <BodyCopy
        fontWeight="bold"
        color="white"
        fontFamily="tertiary"
        textAlign="center"
        style={shadowStyle}
        text={text}
      />
    );
  },
  tcp_homepage_spaced_text: props => {
    const { text, style } = props;
    const shadowStyle = {
      textShadowColor: 'rgba(0, 0, 0, 0.5)',
      textShadowOffset: { width: 0, height: 3 },
      textShadowRadius: 6,
      elevation: 4,
      fontSize: 56,
      ...style,
    };

    return (
      <BodyCopy
        fontWeight="semibold"
        color="gray.600"
        fontFamily="tertiary"
        textAlign="center"
        lineHeight="82px"
        style={shadowStyle}
        text={text}
      />
    );
  },
  tcp_homepage_large_text_black: props => {
    const { text, style } = props;
    const bodyCopyStyle = {
      lineHeight: 50,
      height: 43,
      ...style,
    };
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="tertiary"
        fontSize="fs48"
        fontWeight="bold"
        textAlign="center"
        style={bodyCopyStyle}
        text={text}
      />
    );
  },
  tcp_homepage_medium_text_regular: props => {
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="quaternary"
        fontSize="fs20"
        fontWeight="medium"
        textAlign="center"
        lineHeight="32px"
        {...props}
      />
    );
  },
  tcp_homepage_large_text_bold: props => {
    const { text, style } = props;
    const bodyCopyStyle = {
      marginTop: -15,
      ...style,
    };
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="quaternary"
        fontSize="fs62"
        fontWeight="black"
        lineHeight="84px"
        style={bodyCopyStyle}
        text={text}
      />
    );
  },
  tcp_homepage_small_text_normal: props => {
    return (
      <BodyCopy
        fontSize="fs14"
        color="text.primary"
        fontFamily="secondary"
        textAlign="center"
        lineHeight="17px"
        {...props}
      />
    );
  },
  tcp_homepage_mod_b_style1: props => {
    const { text, style, isModuleA } = props;
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="quaternary"
        fontSize="fs12"
        textAlign="center"
        text={text}
        // eslint-disable-next-line react-native/no-inline-styles
        style={
          isModuleA
            ? { paddingLeft: 20, paddingRight: 20, paddingBottom: 5, ...style }
            : { lineHeight: 17 }
        }
      />
    );
  },
  tcp_homepage_mod_b_style2: props => {
    const { text, style } = props;
    const bodyCopyStyle = {
      lineHeight: 17,
      ...style,
    };

    return (
      <BodyCopy
        color="text.primary"
        fontFamily="quaternary"
        fontWeight="black"
        fontSize="fs12"
        textAlign="center"
        text={text}
        style={bodyCopyStyle}
      />
    );
  },
  tcp_homepage_fixed_medium_text_black: props => {
    const { text, style } = props;

    return (
      <BodyCopy
        color="gray.300"
        fontFamily="quaternary"
        fontWeight="black"
        fontSize="fs15"
        textAlign="center"
        text={text}
        style={style}
      />
    );
  },
  tcp_homepage_medium_text_subpromo: props => {
    const { text, style } = props;
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="secondary"
        fontSize="fs14"
        lineHeight="16px"
        fontWeight="regular"
        textAlign="center"
        text={text}
        style={style}
      />
    );
  },
  tcp_homepage_style1: props => {
    const { text, style } = props;
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="tertiary"
        fontWeight="bold"
        fontSize="fs48"
        textAlign="center"
        text={text}
        style={{ paddingLeft: 0, paddingRight: 0, paddingBottom: 5, height: 58, ...style }}
      />
    );
  },
  tcp_homepage_spaced_text_regular_black: props => {
    const { text, style } = props;
    const copyStyle = { marginTop: 10, ...style };
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="primary"
        fontWeight="black"
        fontSize="fs20"
        textAlign="center"
        text={text}
        style={copyStyle}
      />
    );
  },
};

// =====================================================================
// TODO: Need to remove this patch when we add styles to CMS drop down
// =====================================================================
Object.keys(bodyCopyStyles).forEach(styleKey => {
  const modBAltRegExp = /tcp_homepage_mod_b_alt/;
  const modBRegExp = /tcp_homepage_mod_b/;
  const tcpHomeRegExp = /tcp_homepage_/;

  if (modBAltRegExp.test(styleKey)) {
    const modBAltTcpKey = styleKey.replace(modBAltRegExp, 'tcp_homepage');
    bodyCopyStyles[modBAltTcpKey] = bodyCopyStyles[modBAltTcpKey] || bodyCopyStyles[styleKey];
  }

  if (tcpHomeRegExp.test(styleKey) && !modBRegExp.test(styleKey)) {
    const tcpHomeModBKey = styleKey.replace(tcpHomeRegExp, 'tcp_homepage_mod_b_');
    const tcpHomeModBAltKey = styleKey.replace(tcpHomeRegExp, 'tcp_homepage_mod_b_alt_');
    bodyCopyStyles[tcpHomeModBKey] = bodyCopyStyles[tcpHomeModBKey] || bodyCopyStyles[styleKey];
    bodyCopyStyles[tcpHomeModBAltKey] =
      bodyCopyStyles[tcpHomeModBAltKey] || bodyCopyStyles[styleKey];
  }
});
// ======================================================================

/**
 * This method is to return image icon
 */
const getIcon = icon => icon && <StyledImage source={icons[icon]} />;

/**
 * This method is to return image icon
 * in middle of text
 */
const finalText = (str, placement, icon) => {
  const text = str.split(' ');
  if (text.length > 1 && icon && placement && placement === 'middle') {
    return (
      <>
        {text[0]}
        {getIcon(icon)}
        {text.slice(1, text.length).join(' ')}
      </>
    );
  }
  return str;
};

const getWrapStyle = isContainerModuleText => {
  return isContainerModuleText ? { flexWrap: 'wrap', flex: 1 } : null;
};

/**
 * This component creates a link with styled text
 * Text can be configured to be inside a heading tag to some other element.
 * This component uses BodyCopy atom and Heading atom and differentiates based on type="heading"
 * @param {*} props
 * type="heading" if Heading is required
 * type="bodycopy" if BodyCopy is required
 * accepts all parameters for BodyCopy and Heading atom
 *
 */

const getTextItems = (textItems, icon, useStyle, compProps, isModuleA, titleStyle = {}, others) => {
  const { headerTextStyles = {}, isContainerModuleText } = others;
  const { placement, icon: iconName } = icon || {};
  return (
    textItems &&
    // eslint-disable-next-line complexity
    textItems.map(({ text, style, styledObj }, index) => {
      const headerTextStyle = Array.isArray(headerTextStyles) ? headerTextStyles[index] : {};
      const parsedStyledObj = styledObj && safelyParseJSON(styledObj);
      const wrapStyle = getWrapStyle(isContainerModuleText);
      // use embedded style to render BodyCopy if useStyle is true
      if (style && useStyle) {
        let texts = text;
        /* TO-DO: Make this inline configuarable for all modules or
        We should have an attribute from cms which says whether to display the text inline for all modules. */
        if (isModuleA && index) {
          texts = style === SAME_LINE_CLASS_NAME ? ` ${text}` : `\n${text}`;
        }
        // In LinkText upcomming style not match from listed style then show the default style .
        const StyleBodyCopy = bodyCopyStyles[style] ? bodyCopyStyles[style] : BodyCopy;
        return (
          <StyleBodyCopy
            style={{
              ...titleStyle,
              ...headerTextStyle,
              ...wrapStyle,
              ...parsedStyledObj,
            }}
            accessibilityRole={AccessibilityRoles.Text}
            accessibilityLabel={text}
            text={texts}
            isModuleA={isModuleA}
            key={index.toString()}
            {...compProps}
          />
        );
      }
      return (
        <StyledText
          style={{ ...titleStyle, ...headerTextStyle, ...wrapStyle, ...parsedStyledObj }}
          accessibilityRole={AccessibilityRoles.Text}
          accessibilityLabel={text}
          key={index.toString()}
        >
          {iconName && placement && placement === 'left' && getIcon(iconName)}
          {index ? ` ${text}` : finalText(text, placement, iconName)}
          {iconName && placement && placement === 'right' && getIcon(iconName)}
        </StyledText>
      );
    })
  );
};

const getTextStyle = isModuleA => {
  return isModuleA
    ? { textAlign: 'center' }
    : {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      };
};

const getInlineText = (textItemsComponents, isModuleA, isContainerModuleText) => {
  return isContainerModuleText ? (
    <View style={getTextStyle(isModuleA)}>{textItemsComponents}</View>
  ) : (
    <Text style={getTextStyle(isModuleA)}>{textItemsComponents}</Text>
  );
};

const LinkText = props => {
  const {
    locator,
    type,
    headerText,
    navigation,
    renderComponentInNewLine = false,
    useStyle = false,
    isModuleA,
    headerStyle,
    headerTextStyles,
    isContainerModuleText = false,
    ...otherProps
  } = props;

  let Component;
  let compProps = {};

  if (type === 'heading') {
    Component = Heading;
    compProps = {
      navigation,
      Component,
      ...otherProps,
    };
  } else {
    Component = BodyCopy;
    compProps = {
      navigation,
      Component,
      ...otherProps,
    };
  }

  return headerText.map((item, index) => {
    const { link, icon, textItems } = item || {};
    const titleStyle = Array.isArray(headerStyle) ? headerStyle[index] : {};
    const textItemsComponents = getTextItems(
      textItems,
      icon,
      useStyle,
      compProps,
      isModuleA,
      titleStyle,
      { headerTextStyles, isContainerModuleText }
    );
    const accessibilityLabel = textItems ? textItems.map(({ text }) => text).join(', ') : '';
    if (useStyle) {
      return (
        <Anchor
          accessibilityLabel={accessibilityLabel}
          url={link && link.url}
          navigation={navigation}
        >
          {renderComponentInNewLine
            ? textItemsComponents
            : getInlineText(textItemsComponents, isModuleA, isContainerModuleText)}
        </Anchor>
      );
    }
    return (
      <Anchor
        accessibilityLabel={accessibilityLabel}
        key={index.toString()}
        url={link && link.url}
        navigation={navigation}
      >
        <Component {...compProps} text={textItemsComponents} locator={locator} />
      </Anchor>
    );
  });
};

export default LinkText;
