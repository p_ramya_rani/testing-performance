// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import withStyles from '../../../hoc/withStyles';
import BodyCopy from '../../../atoms/BodyCopy';

import {
  WrapperStyle,
  Container,
  CardSavingHeader,
  CardRewardHeader,
  CardPublicCashHeader,
  HeaderBox,
  CouponTitle,
  CouponExpired,
  CouponBody,
  CouponRow,
  CouponCal,
  CouponDesc,
  CouponDuration,
  CouponAnchor,
} from '../styles/CouponCard.style.native';
import Anchor from '../../../atoms/Anchor';
import CustomButton from '../../../atoms/Button';
import ErrorMessage from '../../../../features/CnC/common/molecules/ErrorMessage';

import { COUPON_REDEMPTION_TYPE } from '../../../../../services/abstractors/CnC/CartItemTile';

export class CouponCard extends React.Component {
  componentDidUpdate(prevProps) {
    const { coupon, toastMessage, labels } = this.props;
    const prevCouponError = prevProps.coupon && prevProps.coupon.error;
    if (!prevCouponError && coupon.error && coupon.offerType === COUPON_REDEMPTION_TYPE.PLACECASH) {
      toastMessage(labels.placeCashErrorMessage);
    }
  }

  RenderCardSavingHeader = (type, dataLocator) => {
    const { labels, coupon } = this.props;
    return (
      <CardSavingHeader>
        <HeaderBox>
          <CouponTitle>
            <BodyCopy
              data-locator={dataLocator}
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="semibold"
              text={type}
              color="white"
            />
          </CouponTitle>
        </HeaderBox>
        {coupon.isExpiring && (
          <CouponExpired>
            <BodyCopy
              data-locator={dataLocator}
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="semibold"
              text={labels.EXPIRING_SOON}
              color="white"
            />
          </CouponExpired>
        )}
      </CardSavingHeader>
    );
  };

  RenderCardRewardHeader = (type, dataLocator) => {
    const { labels, coupon } = this.props;
    return (
      <CardRewardHeader>
        <HeaderBox>
          <CouponTitle>
            <BodyCopy
              data-locator={dataLocator}
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="semibold"
              text={type}
              color="white"
            />
          </CouponTitle>
        </HeaderBox>
        {coupon.isExpiring && (
          <CouponExpired>
            <BodyCopy
              data-locator={dataLocator}
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="semibold"
              text={labels.EXPIRING_SOON}
              color="white"
            />
          </CouponExpired>
        )}
      </CardRewardHeader>
    );
  };

  RenderCardPublicHeader = (type, dataLocator) => {
    const { labels, coupon } = this.props;
    return (
      <CardPublicCashHeader>
        <HeaderBox>
          <CouponTitle>
            <BodyCopy
              data-locator={dataLocator}
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="semibold"
              text={type}
              color="white"
            />
          </CouponTitle>
        </HeaderBox>
        {coupon.isExpiring && (
          <CouponExpired>
            <BodyCopy
              data-locator={dataLocator}
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="semibold"
              text={labels.EXPIRING_SOON}
              color="white"
            />
          </CouponExpired>
        )}
      </CardPublicCashHeader>
    );
  };

  RenderValidText = (coupon) => {
    return (
      <CouponDuration>
        <BodyCopy
          data-locator={`coupon_${coupon.status}_cartValidValidity`}
          text={`Valid ${coupon.effectiveDate} - ${coupon.expirationDate}`}
        />
      </CouponDuration>
    );
  };

  RenderUseByText = (coupon) => {
    return (
      <CouponDuration>
        <BodyCopy
          data-locator={`coupon_${coupon.status}_cartUseByValidity`}
          text={`Use by ${coupon.expirationDate}`}
        />
      </CouponDuration>
    );
  };

  RenderApplyButton = () => {
    const { coupon, onApply, isFetching } = this.props;
    const { status, isStarted, labelStatus, isOnHold = 'false' } = coupon || {};
    if (isOnHold !== 'true' && status === 'available' && isStarted) {
      return (
        <CustomButton
          fill="DARK"
          type="submit"
          data-locator=""
          text={labelStatus}
          disableButton={isFetching}
          onPress={() => {
            if (!isFetching) {
              onApply(coupon);
            }
          }}
        />
      );
    }
    return null;
  };

  RenderRemoveButton = () => {
    const { coupon, onRemove, isFetching } = this.props;
    return (
      <CustomButton
        fill="WHITE"
        type="submit"
        data-locator=""
        text={coupon.labelStatus}
        disableButton={isFetching}
        onPress={() => {
          onRemove(coupon);
        }}
      />
    );
  };

  RenderNotification = (offerType) => {
    const { couponsOnHold, labels } = this.props;
    const message = offerType === 'PLCB' ? labels.onHoldCouponError : labels.onHoldAppleCouponError;
    return (
      <>
        {couponsOnHold && (
          <Notification
            status="warning"
            fontWeight="extrabold"
            isErrorTriangleIcon
            marginValues="12px 0"
            message={message}
          />
        )}
      </>
    );
  };

  handleDefaultLinkClick = (event) => {
    event.preventDefault();
    const { coupon, couponDetailClick } = this.props;
    couponDetailClick(coupon);
  };

  render() {
    const { labels, coupon, handleErrorCoupon } = this.props;
    if (coupon.error) {
      handleErrorCoupon(coupon);
    }
    const { isOnHold = 'false', originalOfferType } = coupon;
    return (
      <View>
        <ErrorMessage
          error={coupon.error}
          isNativeView={false}
          isEspot={coupon.offerType === COUPON_REDEMPTION_TYPE.PLACECASH}
        />
        <WrapperStyle>
          <Container>
            {coupon.offerType === COUPON_REDEMPTION_TYPE.SAVING &&
              this.RenderCardSavingHeader(
                labels.SAVINGS_TEXT,
                `${coupon.status}_PublicValidityLbl`
              )}
            {coupon.offerType === COUPON_REDEMPTION_TYPE.REWARDS &&
              this.RenderCardRewardHeader(
                labels.REWARDS_TEXT,
                `${coupon.status}_RewardValidityLbl`
              )}
            {coupon.offerType === COUPON_REDEMPTION_TYPE.PLACECASH &&
              this.RenderCardPublicHeader(
                labels.PLACE_CASH_TEXT,
                `${coupon.status}_PlaceCashValidityLbl`
              )}
            <CouponBody>
              <CouponRow>
                <CouponCal>
                  <CouponDesc>{`${coupon.title}`}</CouponDesc>
                  <View>
                    {coupon.offerType === COUPON_REDEMPTION_TYPE.SAVING &&
                      this.RenderUseByText(coupon)}
                    {coupon.offerType === COUPON_REDEMPTION_TYPE.REWARDS &&
                      this.RenderUseByText(coupon)}
                    {coupon.offerType === COUPON_REDEMPTION_TYPE.PLACECASH &&
                      this.RenderValidText(coupon)}
                  </View>
                  <CouponAnchor>
                    <Anchor
                      fontSizeVariation="small"
                      fontFamily="secondary"
                      underline
                      anchorVariation="primary"
                      onPress={this.handleDefaultLinkClick}
                      noLink
                      to="/#"
                      dataLocator=""
                      text={labels.DETAILS_BUTTON_TEXT}
                    />
                  </CouponAnchor>
                </CouponCal>
                <CouponCal>
                  {this.RenderApplyButton(coupon)}
                  {(coupon.status === 'applied' || isOnHold === 'true') &&
                    this.RenderRemoveButton()}
                </CouponCal>
              </CouponRow>
            </CouponBody>
          </Container>
          {this.RenderNotification(originalOfferType)}
        </WrapperStyle>
      </View>
    );
  }
}

CouponCard.defaultProps = {
  couponsOnHold: false,
};

CouponCard.propTypes = {
  coupon: PropTypes.shape({
    error: PropTypes.string,
  }).isRequired,
  labels: PropTypes.shape({}).isRequired,
  onApply: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  onRemove: PropTypes.func.isRequired,
  couponDetailClick: PropTypes.func.isRequired,
  handleErrorCoupon: PropTypes.func.isRequired,
  toastMessage: PropTypes.func.isRequired,
  couponsOnHold: PropTypes.bool,
};

export default withStyles(CouponCard);
export { CouponCard as CouponCardVanilla };
