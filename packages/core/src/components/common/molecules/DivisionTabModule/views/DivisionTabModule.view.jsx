// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import ButtonTabs from '../../ButtonTabs';
import { BodyCopy } from '../../../atoms';
import styles from '../styles/DivisionTabModule.style';
import { routerPush, getSiteId, configureInternalNavigationFromCMSUrl } from '../../../../../utils';

const onTabChange = (url) => {
  const cmsValidatedUrl = configureInternalNavigationFromCMSUrl(url);
  routerPush(cmsValidatedUrl, url);
};

export class DivisionTabModule extends PureComponent {
  static propTypes = {
    data: PropTypes.shape({}),
    className: PropTypes.string,
    asPath: PropTypes.string,
  };

  static defaultProps = {
    data: {},
    className: '',
    asPath: '',
  };

  render() {
    const { data, className, asPath } = this.props;
    const { headLine = [] } = data || {};
    const siteId = getSiteId();
    const pathWithoutSiteId = asPath.replace(`/${siteId}`, '');
    return (
      <div className={className}>
        <BodyCopy
          className="heading"
          fontSize={['fs16', 'fs16', 'fs16']}
          fontFamily="secondary"
          fontWeight="semibold"
        >
          {headLine && headLine[0]?.text}
        </BodyCopy>
        <ButtonTabs
          className="button-tabs"
          selectedTabId={pathWithoutSiteId}
          onTabChange={onTabChange}
          tabs={data.buttonList}
          dataLocator=""
          isDivisionTabModule
        />
        <div className="outfit-separator" />
      </div>
    );
  }
}

export { DivisionTabModule as DivisionTabModuleVanilla };
export default withStyles(DivisionTabModule, styles);
