// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { navigateToPage } from '@tcp/core/src/utils/index.native';
import {
  ViewStyleWrapper,
  ButtonTabWrapper,
  Container,
  Wrapper,
  Border,
} from '../styles/DivisionTabModule.style.native';
import withStyles from '../../../hoc/withStyles';
import ButtonTabs from '../../ButtonTabs';
import { BodyCopy } from '../../../atoms';
import styles from '../styles/DivisionTabModule.style';
import { getSiteId, configureInternalNavigationFromCMSUrl } from '../../../../../utils';

const onTabChange = (routeUrl, navigation) => {
  const cmsValidatedUrl = configureInternalNavigationFromCMSUrl(routeUrl);
  navigateToPage(cmsValidatedUrl, navigation);
};

export class DivisionTabModule extends PureComponent {
  static propTypes = {
    data: PropTypes.shape({}),
    navigation: PropTypes.shape({}),
    asPath: PropTypes.string,
  };

  static defaultProps = {
    data: {},
    navigation: {},
    asPath: '',
  };

  render() {
    const { data, asPath, navigation } = this.props;
    const { headLine = [] } = data || {};
    const siteId = getSiteId();
    const pathWithoutSiteId = asPath.replace(`/${siteId}`, '');
    return (
      <ViewStyleWrapper>
        <Container>
          <Border />
          <Wrapper>
            <BodyCopy
              fontSize="fs16"
              fontFamily="secondary"
              fontWeight="semibold"
              text={headLine[0] && headLine[0].text}
              textAlign="center"
            />
          </Wrapper>
          <Border />
        </Container>
        <ButtonTabWrapper>
          <ButtonTabs
            wrappedButtonTabs
            selectedTabId={pathWithoutSiteId}
            onTabChange={(url) => onTabChange(url, navigation)}
            tabs={data.buttonList}
          />
        </ButtonTabWrapper>
      </ViewStyleWrapper>
    );
  }
}

export { DivisionTabModule as DivisionTabModuleVanilla };
export default withStyles(DivisionTabModule, styles);
