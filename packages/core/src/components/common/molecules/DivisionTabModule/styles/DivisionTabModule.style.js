// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const DivisionTabStyles = css`
  .outfit-separator {
    @media ${props => props.theme.mediaQuery.large} {
      background-color: ${props => props.theme.colorPalette.gray[1500]};
      height: 1px;
      margin-top: 45px;
    }
  }
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.SM};
  .heading {
    position: relative;
    overflow: hidden;
    text-align: center;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .heading:before,
  .heading:after {
    position: absolute;
    top: 51%;
    overflow: hidden;
    width: 50%;
    height: 1px;
    content: '\a0';
    background-color: ${props => props.theme.colorPalette.gray[600]};
  }
  .heading:after {
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }
  .heading:before {
    margin-left: calc(-50% - ${props => props.theme.spacing.ELEM_SPACING.SM});
  }
  .button-tabs {
    flex-wrap: wrap;
  }
`;

export default DivisionTabStyles;
