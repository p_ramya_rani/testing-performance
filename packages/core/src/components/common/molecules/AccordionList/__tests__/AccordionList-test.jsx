// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import AccordionList from '../views/AccordionList';

describe('AccordionList component', () => {
  it('renders correctly with 1 accordion open', () => {
    const props = {
      accordionItems: [
        {
          header: {
            text: 'list item 11',
          },
        },
        {
          header: {
            text: 'list item 1',
          },
        },
      ],
      className: 'accordion-list1',
      defaultOpenIndex: 1,
      children: ['<div className="abcd1"></div>', '<div className="defg1"></div>'],
    };
    const component = shallow(<AccordionList {...props} />);
    expect(component).toMatchSnapshot();
    expect(component.find({ activeClass: 'active' })).toHaveLength(1);
    expect(component.find({ activeClass: 'inactive' })).toHaveLength(1);
  });

  it('renders correctly with no accordion open', () => {
    const props = {
      accordionItems: [
        {
          header: {
            text: 'list item 12',
          },
        },
        {
          header: {
            text: 'list item 23',
          },
        },
      ],
      className: 'accordion-list2',
      children: ['<div className="abcd2"></div>', '<div className="defg2"></div>'],
    };
    const component = shallow(<AccordionList {...props} />);
    expect(component).toMatchSnapshot();
    expect(component.find({ activeClass: 'active' })).toHaveLength(2);
    expect(component.find({ activeClass: 'inactive' })).toHaveLength(0);
  });
});
