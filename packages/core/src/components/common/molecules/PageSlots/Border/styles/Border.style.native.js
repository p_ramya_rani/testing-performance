// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';
import getBottomMargin from '../../spacingConfig';
import getTextPadding from '../../textPaddingConfig';
import getHeight from '../../borderThicknessConfig';

const APP = 'app';

const getRightBorder = (isRightBorder, height, VIEWPORT) => {
  return isRightBorder ? getHeight(height, VIEWPORT) : 0;
};

const getLeftBorder = (isLeftBorder, height, VIEWPORT) => {
  return isLeftBorder ? getHeight(height, VIEWPORT) : 0;
};

const getBorderProperties = (props, VIEWPORT, height = 0) => {
  const { borderMedia } = props;
  const border = borderMedia && borderMedia.border;
  const color = borderMedia && borderMedia.color;
  return border
    ? `

  border-right-width: ${getRightBorder(border.right, height, VIEWPORT)}px;
  border-left-width: ${getLeftBorder(border.left, height, VIEWPORT)}px;
  border-right-color: ${color};
  border-left-color: ${color};
  `
    : ``;
};

const StyleColorBorder = styled.View`
  flex: 1;
  margin: 0;
  padding: 0;
  background-color: ${props => props.color};
  height: ${props => props.heightApp}px;
`;

const StyledText = styled.Text`
  display: flex;
  align-items: center;
  text-align: center;
  flex-wrap: wrap;
  justify-content: center;
  max-width: 70%;
  padding-left: ${props => getTextPadding(props.textPadding, APP)}px;
  padding-right: ${props => getTextPadding(props.textPadding, APP)}px;
`;

const StyledMediaBorder = styled.View`
  flex: 1;
  margin: 0;
  padding: 0;
`;
const StyledContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;

  ${props =>
    props.borderMedia && props.borderMedia.border && !props.borderMedia.border.bottom
      ? `
      padding-bottom: ${getBottomMargin(props.spacingBottom.spacingBottomA, APP)};`
      : `margin-bottom: ${getBottomMargin(props.spacingBottom.spacingBottomA, APP)};`}

  ${props =>
    getBorderProperties(
      props,
      APP,
      props.borderMedia && props.borderMedia.config && props.borderMedia.config.heightA
    )};
`;

export { StyleColorBorder, StyledContainer, StyledMediaBorder, StyledText };
