// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { getViewportInfo } from '@tcp/core/src/utils';
import { BodyCopy, DamImage } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import BorderStyle, {
  StyledContainer,
  StyleColorBorder,
  StyledMediaBorder,
  StyledVideoBorder,
  StyledText,
} from './styles/Border.style';
import getHeight from '../borderThicknessConfig';

const HORIZONTAL_RULE = 'horizontalRule';
const BORDER_RULE = 'border';
const COLOR_RULE = 'color';
const MEDIA_RULE = 'media';
const IMAGE_RULE = 'image';
const VIDEO_RULE = 'video';
const TEXT_ITEMS = 'textItems';
const MOBILE = 'mobile';
const TABLET = 'tablet';
const DESKTOP = 'desktop';
const TRUE = 'true';

class Border extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      height: {},
      spacingBottom: {},
      appliesToViewPort: {},
    };
  }

  componentDidMount() {
    const {
      border: { config },
      InModuleContainer,
    } = this.props;
    const {
      appliesToW,
      appliesToM,
      heightM,
      heightT,
      heightD,
      paddingM,
      paddingT,
      paddingD,
      spacingBottomM,
      spacingBottomT,
      spacingBottomD,
    } = config || {};
    const height = {
      heightM,
      heightT,
      heightD,
    };
    const spacingBottom = {
      spacingBottomM,
      spacingBottomT,
      spacingBottomD,
    };
    const textPadding = {
      paddingM,
      paddingT,
      paddingD,
    };
    const appliesToViewPort = {
      appliesToW: InModuleContainer ? true : appliesToW === TRUE,
      appliesToM: InModuleContainer ? true : appliesToM === TRUE,
    };
    this.setState({
      height,
      spacingBottom,
      textPadding,
      appliesToViewPort,
    });
  }

  renderColorRule = (config) => {
    const { height } = this.state;
    return <StyleColorBorder height={height} color={config.color} />;
  };

  renderTextRule = (config) => {
    const { textPadding } = this.state;
    if (config && Array.isArray(config)) {
      const textItems = config.map((textItem) => {
        const { style, text } = textItem;
        return (
          <BodyCopy component="span" className={style}>
            {text}
          </BodyCopy>
        );
      });

      return <StyledText textPadding={textPadding}>{textItems}</StyledText>;
    }
    return <></>;
  };

  renderImageRule = (config) => {
    const { height } = this.state;
    return <StyledMediaBorder height={height} config={config} />;
  };

  getVideoHeight = () => {
    const { height } = this.state;
    let videoHeight = getHeight(height.heightM, MOBILE);
    if (getViewportInfo() && getViewportInfo().isTablet) {
      videoHeight = getHeight(height.heightT, TABLET);
    }
    if (getViewportInfo() && getViewportInfo().isDesktop) {
      videoHeight = getHeight(height.heightD, DESKTOP);
    }
    return videoHeight;
  };

  renderVideoRule = (config) => {
    const { height } = this.state;
    return (
      <StyledVideoBorder height={height}>
        <DamImage videoData={config} height={this.getVideoHeight()} />
      </StyledVideoBorder>
    );
  };

  renderPartRule = (construct) => {
    return (
      <>
        {construct && construct.type === COLOR_RULE && this.renderColorRule(construct.color)}
        {construct && construct.type === IMAGE_RULE && this.renderImageRule(construct.image)}
        {construct && construct.type === VIDEO_RULE && this.renderVideoRule(construct.video)}
      </>
    );
  };

  renderHorizontalRule = (border, borderMedia, appliesToViewPort) => {
    const { construct } = border;
    const { className, InModuleContainer } = this.props;
    const { spacingBottom } = this.state;
    return (
      <StyledContainer
        spacingBottom={spacingBottom}
        className={className}
        borderMedia={borderMedia}
        appliesToViewPort={appliesToViewPort}
        InModuleContainer={InModuleContainer}
      >
        {this.renderPartRule(construct.left)}
        {construct.text &&
          construct.text.type === TEXT_ITEMS &&
          this.renderTextRule(construct.text.textItems)}
        {this.renderPartRule(construct.right)}
      </StyledContainer>
    );
  };

  render() {
    const { border, borderMedia, InModuleContainer } = this.props;
    const { name = '', construct = {} } = border;
    const { spacingBottom, appliesToViewPort } = this.state;

    if (name === HORIZONTAL_RULE || (name === BORDER_RULE && construct.type !== COLOR_RULE)) {
      return this.renderHorizontalRule(border, borderMedia, appliesToViewPort);
    }
    if (name === MEDIA_RULE) {
      if (construct.type === IMAGE_RULE) {
        return (
          <StyledContainer
            spacingBottom={spacingBottom}
            borderMedia={borderMedia}
            appliesToViewPort={appliesToViewPort}
          >
            {this.renderImageRule(construct.image)}
          </StyledContainer>
        );
      }
      if (construct.type === VIDEO_RULE) {
        return (
          <StyledContainer
            spacingBottom={spacingBottom}
            borderMedia={borderMedia}
            appliesToViewPort={appliesToViewPort}
          >
            {this.renderVideoRule(construct.video)}
          </StyledContainer>
        );
      }
    }
    if (name === COLOR_RULE || (name === BORDER_RULE && construct.type === COLOR_RULE)) {
      return (
        <StyledContainer
          spacingBottom={spacingBottom}
          borderMedia={borderMedia}
          appliesToViewPort={appliesToViewPort}
          InModuleContainer={InModuleContainer}
        >
          {this.renderColorRule(construct.color)}
        </StyledContainer>
      );
    }
    return <></>;
  }
}

Border.defaultProps = {
  className: '',
  borderMedia: {},
  InModuleContainer: false,
};

Border.propTypes = {
  border: PropTypes.shape({}).isRequired,
  borderMedia: PropTypes.shape({}),
  className: PropTypes.string,
  InModuleContainer: PropTypes.bool,
};

export default withStyles(Border, BorderStyle);
