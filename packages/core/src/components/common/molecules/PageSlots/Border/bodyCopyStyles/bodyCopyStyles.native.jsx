// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable react/prop-types */
import React from 'react';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import { isGymboree } from '@tcp/core/src/utils/utils';

const bodyCopyStyles = {
  // small text with regular font
  style1: props => (
    <BodyCopy
      color="text.primary"
      fontFamily="tertiary"
      fontSize="fs48"
      fontWeight="medium"
      textAlign="center"
      {...props}
    />
  ),
  // small text with extrabold font
  style2: props => (
    <BodyCopy
      fontFamily="primary"
      fontSize="fs28"
      fontWeight="semibold"
      textAlign="center"
      {...props}
    />
  ),
  // same as 'gymboree_title_text' but to show the text on the same line
  style3: props => (
    <BodyCopy
      fontSize="fs32"
      fontWeight="semibold"
      fontFamily="primary"
      textAlign="center"
      color={isGymboree() ? 'white' : 'text.primary'}
      {...props}
    />
  ),
  // small text with normal font
  small_text_normal: props => (
    <BodyCopy color="gray.900" fontFamily="primary" fontSize="fs14" textAlign="center" {...props} />
  ),
  // large text with bold font
  large_text_black: props => {
    const { text, style } = props;
    const LARGE_TEXT_BLACK_STYLE = { lineHeight: isGymboree() ? 47 : 50, ...style };
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="primary"
        fontSize="fs48"
        fontWeight="black"
        textAlign="center"
        style={LARGE_TEXT_BLACK_STYLE}
        text={text}
        letterSpacing="ls2"
      />
    );
  },
  // large text with bold font
  medium_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs32"
      fontWeight="black"
      textAlign="center"
      {...props}
    />
  ),
  // large text with bold
  medium_text_regular: props => {
    const { style, text } = props;
    const LARGE_TEXT_REGULAR_LINEHEIGHT = { lineHeight: 31, ...style };

    return (
      <BodyCopy
        color="gray.900"
        fontFamily="quaternary"
        fontSize="fs20"
        fontWeight="medium"
        textAlign="center"
        style={LARGE_TEXT_REGULAR_LINEHEIGHT}
        text={text}
      />
    );
  },
  medium_text_subpromo: props => {
    return (
      <BodyCopy
        color="gray.900"
        fontFamily={isGymboree() ? 'primary' : 'secondary'}
        fontSize="fs14"
        fontWeight="regular"
        textAlign="center"
        {...props}
      />
    );
  },
  spaced_text_regular_black: props => {
    return (
      <BodyCopy
        color="gray.900"
        fontFamily="primary"
        fontSize="fs20"
        fontWeight="medium"
        textAlign="center"
        {...props}
      />
    );
  },
  gymboree_title_text: props => {
    return (
      <BodyCopy
        fontSize="fs32"
        fontWeight="semibold"
        fontFamily="primary"
        textAlign="center"
        color={isGymboree() ? 'white' : 'text.primary'}
        {...props}
      />
    );
  },
  gymboree_description: props => (
    <BodyCopy
      fontSize="fs16"
      fontWeight="semibold"
      color="white"
      fontFamily="primary"
      textAlign="center"
      {...props}
    />
  ),
  gym_homepage_gymboree_description: props => (
    // For Module A
    <BodyCopy
      fontFamily="secondary"
      fontSize="fs18"
      fontWeight="regular"
      lineHeight="22px"
      {...props}
    />
  ),
  fixed_medium_text_black: props => {
    return (
      <BodyCopy
        fontSize="fs20"
        lineHeight="22px"
        fontWeight="semibold"
        color="text.primary"
        fontFamily="quaternary"
        textAlign="center"
        {...props}
      />
    );
  },
  text_normal: props => (
    <BodyCopy fontSize="fs16" color="white" fontFamily="primary" textAlign="center" {...props} />
  ),
  style5: props => (
    <BodyCopy
      fontSize="fs64"
      fontWeight="black"
      color="white"
      fontFamily="primary"
      lineHeight="64px"
      textAlign="center"
      {...props}
    />
  ),
  // TODO: Remove .style10 when currency_up_style is available in CMS
  small_text_bold: props => (
    <BodyCopy
      fontSize="fs14"
      fontWeight="black"
      color="black"
      fontFamily="primary"
      textAlign="center"
      lineHeight="16px"
      {...props}
    />
  ),
  ribbon_default_text: props => (
    <BodyCopy
      fontSize="fs14"
      color="white"
      fontFamily="secondary"
      textAlign="center"
      lineHeight="14px"
      {...props}
    />
  ),
  small_text_black: props => (
    <BodyCopy
      color="text.primary"
      fontFamily="primary"
      fontSize="fs20"
      textAlign="center"
      lineHeight="22px"
      letterSpacing="ls1"
      fontWeight="medium"
      {...props}
    />
  ),
  small_text_semibold: props => (
    <BodyCopy
      color="text.primary"
      fontFamily="primary"
      fontSize="fs20"
      fontWeight="semibold"
      textAlign="center"
      lineHeight="20px"
      letterSpacing="ls2"
      {...props}
    />
  ),
  small_white_text_semibold: props => (
    <BodyCopy
      color="white"
      fontFamily="primary"
      fontSize="fs20"
      fontWeight="semibold"
      textAlign="center"
      lineHeight="20px"
      letterSpacing="ls2"
      {...props}
    />
  ),
  medium_text_semibold: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs32"
      textAlign="center"
      lineHeight="47px"
      letterSpacing="ls2"
      {...props}
    />
  ),
  extra_large_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs48"
      textAlign="center"
      lineHeight="47px"
      fontWeight="black"
      {...props}
    />
  ),
  extra_large_white_text_black: props => (
    <BodyCopy
      color="white"
      fontFamily="primary"
      fontSize="fs48"
      textAlign="center"
      fontWeight="black"
      {...props}
    />
  ),
  extra_large_text_regular: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs48"
      textAlign="center"
      lineHeight="47px"
      {...props}
    />
  ),
  large_text_bold: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs20"
      fontWeight="regular"
      textAlign="center"
      letterSpacing="ls2"
      {...props}
    />
  ),
  extrabold_text_regular: props => (
    <BodyCopy
      fontSize="fs42"
      color="gray.900"
      fontFamily="primary"
      fontWeight="black"
      textAlign="center"
      {...props}
    />
  ),
  extrabold_text_regular_secondary: props => (
    <BodyCopy
      fontSize="fs48"
      color="white"
      fontFamily="secondary"
      fontWeight="black"
      textAlign="center"
      {...props}
    />
  ),
  mod_k_subheader: props => (
    <BodyCopy
      fontFamily="secondary"
      fontSize="fs22"
      textAlign="center"
      lineHeight="30px"
      fontWeight="regular"
      {...props}
    />
  ),
  spaced_text_only_mobile: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontWeight="medium"
      fontSize="fs20"
      textAlign="center"
      letterSpacing="ls2"
      {...props}
    />
  ),
  percentage_inline_promo_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontWeight="black"
      fontSize="fs48"
      textAlign="center"
      {...props}
    />
  ),
};

export default bodyCopyStyles;
