// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isAndroid } from '@tcp/core/src/utils/utils.app';
import { BodyCopy, DamImage } from '@tcp/core/src/components/common/atoms';
import bodyCopyStyles from './bodyCopyStyles/bodyCopyStyles.native';
import {
  StyledContainer,
  StyleColorBorder,
  StyledMediaBorder,
  StyledText,
} from './styles/Border.style.native';
import getHeight from '../borderThicknessConfig';

const APP = 'app';
const HORIZONTAL_RULE = 'horizontalRule';
const BORDER_RULE = 'border';
const COLOR_RULE = 'color';
const MEDIA_RULE = 'media';
const IMAGE_RULE = 'image';
const VIDEO_RULE = 'video';
const TEXT_ITEMS = 'textItems';

class Border extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderColorRule = (construct, config) => {
    const { heightA } = config;
    return <StyleColorBorder heightApp={getHeight(heightA, APP)} color={construct.color} />;
  };

  renderTextRule = (construct, config) => {
    const { paddingA } = config;
    if (construct && Array.isArray(construct)) {
      const textItems = construct.map(textItem => {
        const { style, text, styledObj = {} } = textItem;
        const StyleBodyCopy = bodyCopyStyles[style] ? bodyCopyStyles[style] : BodyCopy;
        return <StyleBodyCopy text={`${text} `} style={{ ...styledObj }} />;
      });

      return <StyledText textPadding={paddingA}>{textItems}</StyledText>;
    }
    return <></>;
  };

  renderImageRule = (construct, config) => {
    const { heightA } = config;
    const IMAGE_URL = construct.url_m ? construct.url_m : construct.url;
    return (
      <StyledMediaBorder>
        <DamImage
          url={IMAGE_URL}
          resizeMode="stretch"
          width="100%"
          height={getHeight(heightA, APP)}
          isFastImage
        />
      </StyledMediaBorder>
    );
  };

  renderVideoRule = (construct, config) => {
    const { heightA } = config;
    const VID_DATA_PLP_WEBP_APP = 'e_loop,dpr_2.0';
    const VID_DATA_PLP_GIF_APP = 'dpr_2.0';
    const videoTransformation = isAndroid() ? VID_DATA_PLP_WEBP_APP : VID_DATA_PLP_GIF_APP;
    const transformationFormatPrefix = isAndroid() ? '.webp' : '.gif';
    let IMAGE_URL = construct.url_m ? construct.url_m : construct.url;
    IMAGE_URL = IMAGE_URL.replace(/\.(mp4|webm|WEBM|MP4)$/, transformationFormatPrefix);
    return (
      <StyledMediaBorder>
        <DamImage
          url={IMAGE_URL}
          resizeMode="cover"
          width="100%"
          height={getHeight(heightA, APP)}
          isFastImage
          videoTransformation={videoTransformation}
          isBorderVideo
        />
      </StyledMediaBorder>
    );
  };

  renderPartRule = (construct, config) => {
    return (
      <>
        {construct &&
          construct.type === COLOR_RULE &&
          this.renderColorRule(construct.color, config)}
        {construct &&
          construct.type === IMAGE_RULE &&
          this.renderImageRule(construct.image, config)}
      </>
    );
  };

  renderHorizontalRule = border => {
    const { construct, config } = border;
    const { spacingBottomA } = config;
    return (
      <StyledContainer spacingBottom={spacingBottomA}>
        {this.renderPartRule(construct.left, config)}
        {construct.text &&
          construct.text.type === TEXT_ITEMS &&
          this.renderTextRule(construct.text.textItems, config)}
        {this.renderPartRule(construct.right, config)}
      </StyledContainer>
    );
  };

  showColorRule = (name, construct) => {
    return name === BORDER_RULE && construct.type !== COLOR_RULE;
  };

  getConfig = config => config || {};

  render() {
    const { border, borderMedia } = this.props;
    const { name = '', construct = {}, config = {} } = border;
    const { spacingBottomA } = this.getConfig(config);

    if (name === HORIZONTAL_RULE || this.showColorRule(name, construct)) {
      return this.renderHorizontalRule(border);
    }
    if (name === MEDIA_RULE && construct.type === IMAGE_RULE) {
      return (
        <StyledContainer spacingBottom={spacingBottomA} borderMedia={borderMedia}>
          {this.renderImageRule(construct.image, config)}
        </StyledContainer>
      );
    }
    if (name === MEDIA_RULE && construct.type === VIDEO_RULE) {
      return (
        <StyledContainer spacingBottom={spacingBottomA} borderMedia={borderMedia}>
          {this.renderVideoRule(construct.video, config)}
        </StyledContainer>
      );
    }
    if (name === COLOR_RULE || (name === BORDER_RULE && construct.type === COLOR_RULE)) {
      return (
        <StyledContainer spacingBottom={spacingBottomA} borderMedia={borderMedia}>
          {this.renderColorRule(construct.color, config)}
        </StyledContainer>
      );
    }
    return <></>;
  }
}

Border.defaultProps = {
  borderMedia: {},
};

Border.propTypes = {
  border: PropTypes.shape({}).isRequired,
  borderMedia: {},
};

export default Border;
