/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { getAssetHostURL } from '@tcp/core/src/utils';
import getBottomMargin from '../../spacingConfig';
import getTextPadding from '../../textPaddingConfig';
import getHeight from '../../borderThicknessConfig';

const MOBILE = 'mobile';
const TABLET = 'tablet';
const DESKTOP = 'desktop';

const StyleColorBorder = styled.hr`
  flex: 1;
  margin: 0;
  padding: 0;
  background-color: ${props => props.color};
  border-radius: 0;
  border-width: 0;
  height: ${props => getHeight(props.height.heightM, MOBILE)}px;
  @media ${props => props.theme.mediaQuery.medium} {
    height: ${props => getHeight(props.height.heightT, TABLET)}px;
  }
  @media ${props => props.theme.mediaQuery.large} {
    height: ${props => getHeight(props.height.heightD, DESKTOP)}px;
  }
`;

const StyledText = styled.div`
  display: flex;
  align-items: center;
  text-align: center;
  flex-wrap: wrap;
  justify-content: center;
  max-width: 70%;
  padding: 0 ${props => props.textPadding && getTextPadding(props.textPadding.paddingM, MOBILE)}px;
  @media ${props => props.theme.mediaQuery.medium} {
    padding: 0 ${props => props.textPadding && getTextPadding(props.textPadding.paddingT, TABLET)}px;
  }
  @media ${props => props.theme.mediaQuery.large} {
    padding: 0
      ${props => props.textPadding && getTextPadding(props.textPadding.paddingD, DESKTOP)}px;
  }
  span:not(:last-of-type)::after {
    content: ' ';
    white-space: pre;
  }
`;

const getURL = (config, breakpoint) => {
  if (!config) return ``;
  let partURL = config.url_m ? config.url_m : config.url;
  if (breakpoint === TABLET) {
    partURL = config.url_t ? config.url_t : config.url;
  } else if (breakpoint === DESKTOP) {
    partURL = config.url;
  }
  return `${getAssetHostURL()}${partURL}`;
};

const StyledMediaBorder = styled.div`
  flex: 1;
  background-repeat: round;
  margin: 0;
  padding: 0;
  background-image: url(${props => getURL(props.config)});
  height: ${props => getHeight(props.height.heightM, MOBILE)}px;
  @media ${props => props.theme.mediaQuery.medium} {
    background-image: url(${props => getURL(props.config, TABLET)});
    height: ${props => getHeight(props.height.heightT, TABLET)}px;
  }
  @media ${props => props.theme.mediaQuery.large} {
    background-image: url(${props => getURL(props.config, DESKTOP)});
    height: ${props => getHeight(props.height.heightD, DESKTOP)}px;
  }
`;

const StyledVideoBorder = styled.div`
  flex: 1;
  margin: 0;
  padding: 0;
  .video-play-button {
    display: none;
  }
  video.vjs-tech {
    object-fit: cover;
    height: ${props => getHeight(props.height.heightM, MOBILE)}px;
    @media ${props => props.theme.mediaQuery.medium} {
      height: ${props => getHeight(props.height.heightT, TABLET)}px;
    }
    @media ${props => props.theme.mediaQuery.large} {
      height: ${props => getHeight(props.height.heightD, DESKTOP)}px;
    }
  }
`;

const getRightBorder = (isRightBorder, height, VIEWPORT) => {
  return isRightBorder ? getHeight(height, VIEWPORT) : 0;
};

const getLeftBorder = (isLeftBorder, height, VIEWPORT) => {
  return isLeftBorder ? getHeight(height, VIEWPORT) : 0;
};

const getBorderType = (imageUrl, color) => {
  return imageUrl
    ? `
   border-image: url(${imageUrl}) 30 stretch;

  `
    : ` border-color: ${color};`;
};

const getBorderProperties = (props, imageUrl, VIEWPORT, height = 0) => {
  const { borderMedia } = props;
  const border = borderMedia && borderMedia.border;
  const color = borderMedia && borderMedia.color;
  return border
    ? `
  border-right: ${getRightBorder(border.right, height, VIEWPORT)}px solid transparent;
  border-left: ${getLeftBorder(border.left, height, VIEWPORT)}px solid transparent;
  ${getBorderType(imageUrl, color)}
  `
    : ``;
};

const renderMobileBorder = props => {
  return props.borderMedia && props.borderMedia.appliesToM
    ? getBorderProperties(
        props,
        props.borderMedia && props.borderMedia.urlM,
        MOBILE,
        props.borderMedia && props.borderMedia.config.heightM
      )
    : ``;
};
const renderTabletBorder = props => {
  return props.borderMedia && props.borderMedia.appliesToW
    ? getBorderProperties(
        props,
        props.borderMedia && props.borderMedia.urlT,
        TABLET,
        props.borderMedia && props.borderMedia.config.heightT
      )
    : ``;
};
const renderDesktopBorder = props => {
  return props.borderMedia && props.borderMedia.appliesToW
    ? getBorderProperties(
        props,
        props.borderMedia && props.borderMedia.url,
        DESKTOP,
        props.borderMedia && props.borderMedia.config.heightD
      )
    : ``;
};

const StyledContainer = styled.div`
  @media ${props => props.theme.mediaQuery.smallOnly} {
    ${props =>
      props.appliesToViewPort && props.appliesToViewPort.appliesToM
        ? `
    display: flex;
    `
        : `display: none;
    `}
  }

  @media ${props => props.theme.mediaQuery.medium} {
    ${props =>
      props.appliesToViewPort && props.appliesToViewPort.appliesToW
        ? `
      display: flex;
      `
        : `display: none;
      `}
  }

  @media ${props => props.theme.mediaQuery.large} {
    ${props =>
      props.appliesToViewPort && props.appliesToViewPort.appliesToW
        ? `
        display: flex;
        `
        : `display: none;
        `}
  }

  align-items: center;
  ${props =>
    props.borderMedia && props.borderMedia.border && !props.borderMedia.border.bottom
      ? `
        padding-bottom: ${getBottomMargin(props.spacingBottom.spacingBottomM, MOBILE)}px;
        @media ${props.theme.mediaQuery.mediumOnly} {
          padding-bottom: ${getBottomMargin(props.spacingBottom.spacingBottomT, TABLET)}px;
        }
        @media ${props.theme.mediaQuery.large} {
          padding-bottom: ${getBottomMargin(props.spacingBottom.spacingBottomD, DESKTOP)}px;
        }`
      : `margin-bottom: ${getBottomMargin(props.spacingBottom.spacingBottomM, MOBILE)}px;
        @media ${props.theme.mediaQuery.mediumOnly} {
          margin-bottom: ${getBottomMargin(props.spacingBottom.spacingBottomT, TABLET)}px;
        }
        @media ${props.theme.mediaQuery.large} {
          margin-bottom: ${getBottomMargin(props.spacingBottom.spacingBottomD, DESKTOP)}px;
        }`}
  ${props =>
    props.borderMedia && props.borderMedia.config
      ? `
      @media ${props.theme.mediaQuery.smallOnly} {
      ${renderMobileBorder(props)};
      }
      @media ${props.theme.mediaQuery.medium} {
        ${renderTabletBorder(props)};
      }
      @media ${props.theme.mediaQuery.large} {
        ${renderDesktopBorder(props)};
      }`
      : ``}

  ${props => (props.InModuleContainer ? `justify-content: center; width: 100%;` : null)}
`;

/**
 * BodyCopy Styles for the Text Styles
 */
export default css`
  .gymboree_title_text {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.black};
    font-size: 32px;
    line-height: 1.05;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: 64px;
    }
  }

  .gymboree_description {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
    font-size: ${props => props.theme.typography.fontSizes.fs16};
    display: block;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs28};
    }
  }

  .spaced_text {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
    font-size: ${props =>
      props.theme.isGymboree
        ? props.theme.typography.fontSizes.fs20
        : props.theme.typography.fontSizes.fs22};
    line-height: normal;
    letter-spacing: 2px;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: 64px;
      letter-spacing: 4px;
      font-weight: 500;
    }
  }

  .small_text_normal {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs14};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs20};
    }
  }
  .medium_text_black {
    color: ${props => props.theme.colorPalette.gray['900']};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.black};
    font-size: ${props => props.theme.typography.fontSizes.fs32};
    letter-spacing: -0.5px;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs48};
    }
  }

  .large_text_black {
    color: ${props => props.theme.colorPalette.gray['900']};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.black};
    font-size: ${props => props.theme.typography.fontSizes.fs48};
    letter-spacing: -0.5px;
    line-height: 46px;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs64};
    }
  }

  .medium_text_white_black {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.black};
    font-size: ${props => props.theme.typography.fontSizes.fs32};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs48};
    }
  }

  .medium_text_regular {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs32};
    font-weight: ${props => props.theme.typography.fontWeights.medium};
    letter-spacing: 2px;
    text-align: center;
    color: ${props => props.theme.colorPalette.black};
  }

  .small_text_white_medium {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.semibold};
    font-size: ${props => props.theme.typography.fontSizes.fs20};
    letter-spacing: 2px;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      letter-spacing: normal;
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      display: inline;
    }
    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs32};
    }
  }

  .white_large_text_half {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
    font-size: ${props => props.theme.typography.fontSizes.fs20};
    letter-spacing: 2px;
    color: ${props => props.theme.colorPalette.white};
    text-align: center;
    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs32};
    }
  }

  .spaced_text_regular_black {
    display: block;
    color: ${props => props.theme.colorPalette.primary};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
    font-size: ${props => props.theme.typography.fontSizes.fs20};
    letter-spacing: 2px;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs32};
    }
  }

  .medium_text_subpromo {
    color: ${props => props.theme.colorPalette.gray['900']};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props =>
      props.theme.isGymboree
        ? props.theme.typography.fontSizes.fs20
        : props.theme.typography.fontSizes.fs14};
    font-weight: normal;
    text-align: center;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props =>
        props.theme.isGymboree
          ? props.theme.typography.fontSizes.fs32
          : props.theme.typography.fontSizes.fs20};
    }
  }

  .gym_homepage_gymboree_description {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
    font-size: ${props => props.theme.typography.fontSizes.fs16};
    display: block;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs28};
    }
  }

  .style1 {
    color: ${props => props.theme.colorPalette.gray['900']};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs42};

    @media ${props => props.theme.mediaQuery.medium} {
      font-size: 70px;
    }

    @media ${props => props.theme.mediaQuery.large} {
      font-size: 60px;
    }
  }
  .style2 {
    color: ${props => props.theme.colorPalette.gray['900']};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.black};
    font-size: 70px;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: 60px;
    }
  }

  .small_text_semibold {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs20};
    font-weight: ${props => props.theme.typography.fontWeights.semibold};
    letter-spacing: ${props => props.theme.typography.letterSpacings.ls222};
    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs32};
      font-weight: ${props => props.theme.typography.fontWeights.semibold};
      letter-spacing: ${props => props.theme.typography.letterSpacings.ls222};
    }
  }

  .small_text_black {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs20};
    font-weight: ${props => props.theme.typography.fontWeights.black};
    letter-spacing: ${props => props.theme.typography.letterSpacings.ls222};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs36};
      font-weight: ${props => props.theme.typography.fontWeights.black};
      letter-spacing: ${props => props.theme.typography.letterSpacings.ls222};
    }
  }

  .text_normal {
    font-size: ${props => props.theme.typography.fontSizes.fs16};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
    text-align: center;
    color: ${props => props.theme.colorPalette.white};
    display: block;

    @media ${props => props.theme.mediaQuery.large} {
      display: inline-block;
      font-size: ${props => props.theme.typography.fontSizes.fs26};
      text-align: left;
      margin-left: 10px;
      position: relative;
      line-height: normal;
      top: 6px;
    }
  }

  .small_text_bold {
    color: ${props => props.theme.colorPalette.text.primary};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.semibold};
    font-size: ${props => props.theme.typography.fontSizes.fs14};
    line-height: normal;
    display: inline;
    vertical-align: top;

    @media ${props => props.theme.mediaQuery.medium} {
      font-size: ${props => props.theme.typography.fontSizes.fs14};
      line-height: normal;
    }

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs20};
      line-height: normal;
    }
  }

  .ribbon_default_text {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.secondary};
    font-weight: ${props => props.theme.typography.fontWeights.normal};
    font-size: ${props => props.theme.typography.fontSizes.fs14};
    display: block;

    @media ${props => props.theme.mediaQuery.medium} {
      font-size: ${props => props.theme.typography.fontSizes.fs14};
    }

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs18};
    }
  }

  .extra_large_text_regular {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs48};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: 86px;
    }
  }

  .extra_large_text_black {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs48};
    font-weight: ${props => props.theme.typography.fontWeights.black};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: 86px;
    }
  }

  .medium_text_semibold {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs32};
    font-weight: ${props => props.theme.typography.fontWeights.semibold};
    letter-spacing: ${props => props.theme.typography.letterSpacings.ls2};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs64};
    }
  }

  .fixed_medium_text_black {
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs64};
    font-weight: ${props => props.theme.typography.fontWeights.black};
  }

  .large_text_bold {
    color: ${props => props.theme.colorPalette.gray['900']};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.black};
    font-size: ${props => props.theme.typography.fontSizes.fs48};
    letter-spacing: -0.5px;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs64};
    }
  }

  .spaced_text_only_mobile {
    display: block;
    font-family: ${props => props.theme.typography.fonts.primary};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
    font-size: ${props => props.theme.typography.fontSizes.fs20};
    color: ${props => props.theme.colorPalette.gray[900]};
    letter-spacing: 2px;

    @media ${props => props.theme.mediaQuery.large} {
      display: inline-block;
      font-size: ${props => props.theme.typography.fontSizes.fs64};
      letter-spacing: 0;
    }
  }

  .small_white_text_semibold {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs20};
    font-weight: ${props => props.theme.typography.fontWeights.medium};
    line-height: 20px;
    letter-spacing: 2px;
    text-align: center;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs64};
      letter-spacing: 0;
    }
  }

  .extra_large_white_text_black {
    color: ${props => props.theme.colorPalette.white};
    font-family: ${props => props.theme.typography.fonts.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs48};
    font-weight: ${props => props.theme.typography.fontWeights.black};
    text-align: center;

    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs64};
    }
  }

  .mod_k_subheader {
    font-size: ${props => props.theme.typography.fontSizes.fs22};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
    line-height: 30px;
    font-family: ${props => props.theme.typography.fonts.secondary};
  }
`;

export { StyleColorBorder, StyledContainer, StyledMediaBorder, StyledText, StyledVideoBorder };
