// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';

import PageSlots from '..';

const slotsCmsDataMock = [
  {
    key: '0',
    data: { slotData: 'slot C Data' },
    moduleName: 'moduleC',
    name: 'slot_3',
  },
  {
    key: '1',
    data: { slotData: 'slot A Data' },
    moduleName: 'moduleA',
    name: 'slot_1',
  },
  {
    key: '2',
    data: { slotData: 'slotB Data' },
    moduleName: 'moduleB',
    name: 'slot_2',
  },
];

const ModuleA = () => <div>Module A</div>;
const ModuleB = () => <div>Module B</div>;
const ModuleC = () => <div>Module C</div>;
const ModuleX = () => <div>Module X</div>;

const modulesDataMock = {
  moduleA: ModuleA,
  moduleB: ModuleB,
  moduleC: ModuleC,
};

describe('PageSlots component', () => {
  it('Should renders slots according to the data', () => {
    const component = shallow(<PageSlots slots={slotsCmsDataMock} modules={modulesDataMock} />).get(
      0
    );
    expect(component).toMatchSnapshot();
  });

  it('Should render slots even if the component is not available', () => {
    const component = shallow(
      <PageSlots slots={slotsCmsDataMock} modules={{ moduleX: ModuleX, moduleA: ModuleA }} />
    ).get(0);
    expect(component).toMatchSnapshot();
  });

  it('Should render slots even if data does not send required slot', () => {
    const component = shallow(
      <PageSlots
        slots={[
          {
            key: '0',
            data: { slotData: 'slot B Data' },
            moduleName: 'moduleB',
            name: 'slot_1',
          },
        ]}
        modules={modulesDataMock}
      />
    ).get(0);
    expect(component).toMatchSnapshot();
  });

  it('Should render null if no slot match', () => {
    const component = shallow(
      <PageSlots
        slots={[
          {
            key: '0',
            data: { slotData: 'slot X Data' },
            moduleName: 'moduleX',
            name: 'slot_1',
          },
        ]}
        modules={modulesDataMock}
      />
    ).get(0);
    expect(component).toMatchSnapshot();
  });

  it('Should render not render component if data is null or undefined', () => {
    //  data prop is undefined in slots
    const component = shallow(
      <PageSlots
        slots={[
          {
            key: '0',
            moduleName: 'moduleA',
            name: 'slot_1',
          },
        ]}
        modules={modulesDataMock}
      />
    ).get(0);
    expect(component).toMatchSnapshot();
  });

  it('Should render null if there is blank slots data', () => {
    const component = shallow(<PageSlots slots={[]} modules={modulesDataMock} />).get(0);
    expect(component).toMatchSnapshot();
  });

  it('Should verify if other props getting render in component', () => {
    const component = shallow(
      <PageSlots
        extraProp="extraProp"
        slots={[
          {
            key: '0',
            data: { slotData: 'slot B Data' },
            moduleName: 'moduleA',
            name: 'slot_1',
          },
          {
            key: '1',
            data: { slotData: 'slot A Data' },
            moduleName: 'moduleB',
            name: 'slot_2',
          },
        ]}
        modules={modulesDataMock}
      />
    ).get(0);
    expect(component).toMatchSnapshot();
  });
});
