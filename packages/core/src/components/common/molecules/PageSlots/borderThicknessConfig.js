// 9fbef606107a605d69c0edbcd8029e5d 
const height = {
  mobile: {
    none: 0,
    xSmall: 2,
    small: 3,
    medium: 6,
    large: 9,
    xLarge: 12,
    xxLarge: 16,
    xxxLarge: 20,
    mega: 24,
  },
  tablet: {
    none: 0,
    xSmall: 2,
    small: 6,
    medium: 9,
    large: 12,
    xLarge: 16,
    xxLarge: 20,
    xxxLarge: 24,
    mega: 32,
  },
  desktop: {
    none: 0,
    xSmall: 2,
    small: 9,
    medium: 12,
    large: 16,
    xLarge: 20,
    xxLarge: 24,
    xxxLarge: 28,
    mega: 48,
  },
  app: {
    none: 0,
    xSmall: 2,
    small: 3,
    medium: 6,
    large: 9,
    xLarge: 12,
    xxLarge: 16,
    xxxLarge: 20,
    mega: 24,
  },
};

const getHeight = (value, breakpoint) => {
  const defaultValue = 'medium';
  if (value) {
    return height[breakpoint][value];
  }
  return height[breakpoint][defaultValue];
};

export default getHeight;
