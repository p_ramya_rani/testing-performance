// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import getBottomMargin from '../spacingConfig';
import getHeight from '../borderThicknessConfig';

const APP = 'app';

const getTopBorder = (isTopBorder, height, VIEWPORT) => {
  return isTopBorder ? getHeight(height, VIEWPORT) : 0;
};
const getRightBorder = (isRightBorder, height, VIEWPORT) => {
  return isRightBorder ? getHeight(height, VIEWPORT) : 0;
};
const getBottomBorder = (isBottomBorder, height, VIEWPORT) => {
  return isBottomBorder ? getHeight(height, VIEWPORT) : 0;
};
const getLeftBorder = (isLeftBorder, height, VIEWPORT) => {
  return isLeftBorder ? getHeight(height, VIEWPORT) : 0;
};

const getBorderWidth = props => {
  return props.borderMedia && props.borderMedia.config && props.borderMedia.config.heightA;
};

const getBorderProperties = (props, VIEWPORT, height = 0) => {
  const { borderMedia } = props;
  const border = borderMedia && borderMedia.border;
  const color = borderMedia && borderMedia.color;
  return color && borderMedia.appliesToM
    ? `
  border-top-width: ${getTopBorder(border.top, height, VIEWPORT)}px;
  border-right-width: ${getRightBorder(border.right, height, VIEWPORT)}px;
  border-bottom-width: ${getBottomBorder(border.bottom, height, VIEWPORT)}px;
  border-left-width: ${getLeftBorder(border.left, height, VIEWPORT)}px;
  border-top-color: ${color};
  border-right-color: ${color};
  border-bottom-color: ${color};
  border-left-color: ${color};
  `
    : ``;
};

const StyledModuleWrapper = styled.View`

  ${props =>
    props.borderMedia &&
    props.borderMedia.appliesToM &&
    props.borderMedia.border &&
    props.borderMedia.url
      ? `
      opacity:1;
      background-color:white;
      margin: ${getHeight(getBorderWidth(props), APP)}px;`
      : ``}

  ${props =>
    props.borderMedia &&
    props.borderMedia.appliesToM &&
    props.borderMedia.border &&
    !props.borderMedia.border.bottom
      ? `
      padding-bottom: ${getBottomMargin(props.spacing.spacingBottomA, APP)};`
      : ``}
  ${props =>
    !props.borderMedia
      ? `
      margin-bottom: ${getBottomMargin(props.spacing.spacingBottomA, APP)};`
      : ``}

  ${props => getBorderProperties(props, APP, getBorderWidth(props))};
`;

export default StyledModuleWrapper;
