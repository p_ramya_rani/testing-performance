// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import getBottomMargin from '../spacingConfig';
import getHeight from '../borderThicknessConfig';

const MOBILE = 'mobile';
const TABLET = 'tablet';
const DESKTOP = 'desktop';

const getTopBorder = (isTopBorder, height, VIEWPORT) => {
  return isTopBorder ? getHeight(height, VIEWPORT) : 0;
};
const getRightBorder = (isRightBorder, height, VIEWPORT) => {
  return isRightBorder ? getHeight(height, VIEWPORT) : 0;
};
const getBottomBorder = (isBottomBorder, height, VIEWPORT) => {
  return isBottomBorder ? getHeight(height, VIEWPORT) : 0;
};
const getLeftBorder = (isLeftBorder, height, VIEWPORT) => {
  return isLeftBorder ? getHeight(height, VIEWPORT) : 0;
};

const getBorderType = (imageUrl, color) => {
  return imageUrl
    ? `
   border-image: url(${imageUrl}) 30 stretch;
   border-color: initial;
  `
    : ` border-color: ${color};`;
};

const getBorderProperties = (props, imageUrl, VIEWPORT, height = 0) => {
  const { borderMedia } = props;
  const border = borderMedia && borderMedia.border;
  const color = borderMedia && borderMedia.color;
  return border
    ? `
  border-top: ${getTopBorder(border.top, height, VIEWPORT)}px solid transparent;
  border-right: ${getRightBorder(border.right, height, VIEWPORT)}px solid transparent;
  border-bottom: ${getBottomBorder(border.bottom, height, VIEWPORT)}px solid transparent;
  border-left: ${getLeftBorder(border.left, height, VIEWPORT)}px solid transparent;
  ${getBorderType(imageUrl, color)}
  `
    : ``;
};

const getBackgroundProperties = (props) => {
  const { backgroundMedia } = props;
  const bgColor = backgroundMedia && backgroundMedia.bgColor;
  return bgColor ? `background: ${bgColor};` : ``;
};

const StyledModuleWrapper = styled.div`
  ${(props) =>
    (props.borderMedia && props.borderMedia.border && !props.borderMedia.border.bottom) ||
    (props.backgroundMedia && props.backgroundMedia.bgColor)
      ? `
        padding-bottom: ${getBottomMargin(props.spacing.spacingBottomM, MOBILE)}px;
        @media ${props.theme.mediaQuery.mediumOnly} {
          padding-bottom: ${getBottomMargin(props.spacing.spacingBottomT, TABLET)}px;
        }
        @media ${props.theme.mediaQuery.large} {
          padding-bottom: ${getBottomMargin(props.spacing.spacingBottomD, DESKTOP)}px;
        }`
      : `margin-bottom: ${getBottomMargin(props.spacing.spacingBottomM, MOBILE)}px;
        @media ${props.theme.mediaQuery.mediumOnly} {
          margin-bottom: ${getBottomMargin(props.spacing.spacingBottomT, TABLET)}px;
        }
        @media ${props.theme.mediaQuery.large} {
          margin-bottom: ${getBottomMargin(props.spacing.spacingBottomD, DESKTOP)}px;
        }`}

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    ${(props) =>
      props.borderMedia &&
      props.borderMedia.appliesToM &&
      getBorderProperties(
        props,
        props.borderMedia && props.borderMedia.urlM,
        MOBILE,
        props.borderMedia && props.borderMedia.config.heightM
      )};

    ${(props) =>
      props.backgroundMedia && props.backgroundMedia.appliesToM && getBackgroundProperties(props)};
  }
  @media ${(props) => props.theme.mediaQuery.medium} {
    ${(props) =>
      props.borderMedia &&
      props.borderMedia.appliesToW &&
      getBorderProperties(
        props,
        props.borderMedia && props.borderMedia.urlT,
        TABLET,
        props.borderMedia && props.borderMedia.config.heightT
      )};

    ${(props) =>
      props.backgroundMedia && props.backgroundMedia.appliesToW && getBackgroundProperties(props)};
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    ${(props) =>
      props.borderMedia &&
      props.borderMedia.appliesToW &&
      getBorderProperties(
        props,
        props.borderMedia && props.borderMedia.url,
        DESKTOP,
        props.borderMedia && props.borderMedia.config.heightD
      )};

    ${(props) =>
      props.backgroundMedia && props.backgroundMedia.appliesToW && getBackgroundProperties(props)};
  }
`;

export default StyledModuleWrapper;
