// 9fbef606107a605d69c0edbcd8029e5d
const BEFORE = 'before';
const AFTER = 'after';
const TRUE = 'true';

const isOnlySingleSlot = ({ fromSlot, toSlot }, name) => {
  return fromSlot === name && toSlot === name;
};

const isEnclosingStartSlot = ({ fromSlot, toSlot }, name) => {
  return fromSlot === name && toSlot !== name;
};

const isEnclosingEndSlot = ({ fromSlot, toSlot }, name) => {
  return fromSlot !== name && toSlot === name;
};

const getSlotNumber = (slotName) => {
  return slotName && parseInt(slotName.split('_')[1], 10);
};

const isEnclosingMiddleSlot = ({ fromSlot, toSlot }, name) => {
  const currentSlotNumber = getSlotNumber(name);
  return getSlotNumber(fromSlot) < currentSlotNumber && getSlotNumber(toSlot) > currentSlotNumber;
};
const getDesktopImageURL = (construct) => {
  return construct.image && construct.image.url;
};
const getTabImageURL = (construct) => {
  return construct.image && (construct.image.url_t || construct.image.url);
};

const getMobileImageURL = (construct) => {
  return construct.image && (construct.image.url_m || construct.image.url);
};

export const renderRulerEncloseBorder = (slot, borderType, RulerPosition) => {
  const { border, name } = slot;
  return (
    border &&
    border
      .filter((bor) => bor.type === borderType)
      .map((bor) => {
        const { construct, config } = bor;
        const url = getDesktopImageURL(construct);
        const urlT = getTabImageURL(construct);
        const urlM = getMobileImageURL(construct);
        const color = construct.color && construct.color.color;

        let borderStyle = {
          url,
          urlT,
          urlM,
          config,
          color,
          border: { top: false, right: false, bottom: false, left: false },
          appliesToM: config.appliesToM === TRUE,
          appliesToW: config.appliesToW === TRUE,
        };

        if (
          (isEnclosingStartSlot(config, name) && RulerPosition === AFTER) ||
          (isEnclosingEndSlot(config, name) && RulerPosition === BEFORE) ||
          isEnclosingMiddleSlot(config, name)
        ) {
          // apply border on right and left side only
          borderStyle = {
            ...borderStyle,
            border: {
              ...borderStyle.border,
              right: true,
              left: true,
            },
          };
        }
        return borderStyle;
      })[0]
  );
};

export const renderSlotEncloseBorder = (slot, borderType) => {
  const { border, name } = slot;
  return (
    border &&
    border
      .filter((bor) => bor.type === borderType)
      .map((bor) => {
        const { construct, config } = bor;
        const url = getDesktopImageURL(construct);
        const urlT = getTabImageURL(construct);
        const urlM = getMobileImageURL(construct);
        const color = construct.color && construct.color.color;
        let borderStyle = {
          url,
          urlT,
          urlM,
          config,
          color,
          border: { top: false, right: false, bottom: false, left: false },
          appliesToM: config.appliesToM === TRUE,
          appliesToW: config.appliesToW === TRUE,
        };
        // check only single slot to be enclosed
        if (isOnlySingleSlot(config, name)) {
          // apply border on all four sides
          borderStyle = {
            ...borderStyle,
            border: { ...borderStyle.border, top: true, right: true, bottom: true, left: true },
          };
        }
        if (isEnclosingStartSlot(config, name)) {
          // apply border on all sides except bottom side
          borderStyle = {
            ...borderStyle,
            border: { ...borderStyle.border, top: true, right: true, left: true },
          };
        }
        if (isEnclosingEndSlot(config, name)) {
          // apply border on all sides except top side
          borderStyle = {
            ...borderStyle,
            border: { ...borderStyle.border, right: true, bottom: true, left: true },
          };
        }
        if (isEnclosingMiddleSlot(config, name)) {
          // apply border on right and left sides only
          borderStyle = {
            ...borderStyle,
            border: { ...borderStyle.border, right: true, left: true },
          };
        }
        return borderStyle;
      })[0]
  );
};

export const renderEnclosingBackground = (slot) => {
  const { background, name } = slot;

  return (
    background &&
    background.map((bg) => {
      const { construct, config } = bg;
      const bgColor = construct.color && construct.color.color;
      let bgStyle = {
        config,
        bgColor,
        appliesToM: config.appliesToM === TRUE,
        appliesToW: config.appliesToW === TRUE,
      };
      if (
        isOnlySingleSlot(config, name) ||
        isEnclosingStartSlot(config, name) ||
        isEnclosingEndSlot(config, name) ||
        isEnclosingMiddleSlot(config, name)
      ) {
        bgStyle = {
          ...bgStyle,
        };
      }
      return bgStyle;
    })[0]
  );
};
