// 9fbef606107a605d69c0edbcd8029e5d 
const spaceConfig = {
  desktop: {
    small: 32,
    medium: 48,
    large: 64,
    none: 0,
  },
  tablet: {
    small: 24,
    medium: 32,
    large: 48,
    none: 0,
  },
  mobile: {
    small: 16,
    medium: 24,
    large: 32,
    none: 0,
  },
  app: {
    small: 16,
    medium: 24,
    large: 32,
    none: 0,
  },
};

const getBottomMargin = (value, breakpoint) => {
  const defaultValue = 'medium';
  if (value) {
    return spaceConfig[breakpoint][value];
  }
  return spaceConfig[breakpoint][defaultValue];
};

export default getBottomMargin;
