// 9fbef606107a605d69c0edbcd8029e5d
import React, { Component } from 'react';
import { ImageBackground } from 'react-native';
import { PropTypes } from 'prop-types';
import BagCarouselModule from '@tcp/core/src/components/common/molecules/BagCarouselModule';
import logger from '@tcp/core/src/utils/loggerInstance';
import StyledModuleWrapper from './styles';
import Border from './Border/Border.native';
import { renderRulerEncloseBorder, renderSlotEncloseBorder } from './EnclosingBorder.utils';
import getHeight from './borderThicknessConfig';
import getBottomMargin from './spacingConfig';

const BEFORE = 'before';
const AFTER = 'after';
const ENCLOSE = 'enclose';
const TRUE = 'true';

let imgBgStyle = {
  flex: 1,
  resizeMode: 'stretch',
  justifyContent: 'center',
  width: '100%',
};

const isBottomBorder = (borderMedia, isHpNewLayoutEnabled) =>
  !isHpNewLayoutEnabled &&
  borderMedia &&
  borderMedia.appliesToM &&
  borderMedia.border &&
  borderMedia.border.bottom;

const getBorderWidth = (borderMedia, isHpNewLayoutEnabled) =>
  getHeight(
    (!isHpNewLayoutEnabled && borderMedia && borderMedia.config && borderMedia.config.heightA) ||
      'none',
    'app'
  ) * 2;
class PageSlots extends Component {
  static propTypes = {
    modules: PropTypes.shape({}).isRequired,
    slots: PropTypes.arrayOf(
      PropTypes.shape({
        contentId: PropTypes.string,
        data: PropTypes.shape({}),
        moduleName: PropTypes.string,
        name: PropTypes.string,
      })
    ).isRequired,
    isHpNewLayoutEnabled: PropTypes.bool.isRequired,
    loadMoreSlots: PropTypes.bool,
  };

  shouldComponentUpdate(nextProps) {
    const { slots, modules, isHpNewLayoutEnabled, loadMoreSlots } = this.props;
    const {
      slots: nextSlots,
      modules: nextModules,
      isHpNewLayoutEnabled: nextIsHpNewLayoutEnabled,
      loadMoreSlots: nextloadMoreSlots,
    } = nextProps;
    if (
      slots.length !== nextSlots.length ||
      loadMoreSlots !== nextloadMoreSlots ||
      modules.length !== nextModules.length ||
      isHpNewLayoutEnabled !== nextIsHpNewLayoutEnabled
    ) {
      return true;
    }
    return false;
  }

  renderBagCarousel = (slots, index, otherProps) => {
    return index === 2 || slots.length < 3 ? <BagCarouselModule {...otherProps} /> : null;
  };

  renderBorder = (slot, borderType) => {
    const { isHpNewLayoutEnabled } = this.props;
    return (
      <>
        {!isHpNewLayoutEnabled &&
          slot.border &&
          slot.border
            .filter((bor) => bor.type === borderType)
            .map((bor) => {
              return bor && bor.config.appliesToM === TRUE ? (
                <Border
                  border={bor}
                  borderMedia={renderRulerEncloseBorder(slot, ENCLOSE, borderType)}
                />
              ) : null;
            })}
      </>
    );
  };

  shouldBorderRender = (slot, isHpNewLayoutEnabled) =>
    !isHpNewLayoutEnabled && renderSlotEncloseBorder(slot, ENCLOSE);

  getBorderMediaUrl = (borderMedia, isHpNewLayoutEnabled) =>
    (!isHpNewLayoutEnabled && borderMedia && borderMedia.appliesToM && borderMedia.urlM) || '';

  render() {
    const {
      slots: slotsData = '',
      modules,
      isHpNewLayoutEnabled,
      loadMoreSlots,
      ...others
    } = this.props;
    let slots;
    try {
      slots = JSON.parse(slotsData);
    } catch (e) {
      logger.error('Error in PageSlots.native.jsx :: render :: ', e);
      slots = [];
    }
    slots = loadMoreSlots ? slots : slots.splice(0, 10);
    return slots.map((slot, index) => {
      const key = index;
      if (slot && slot.moduleName) {
        const Module = modules[slot.moduleName];
        const { data: slotData, accessibility, spacing } = slot;
        const borderMedia = this.shouldBorderRender(slot, isHpNewLayoutEnabled);
        const imgBgSource = {
          uri: this.getBorderMediaUrl(borderMedia, isHpNewLayoutEnabled),
        };
        const borderWidth = getBorderWidth(borderMedia, isHpNewLayoutEnabled);
        if (isBottomBorder(borderWidth, isHpNewLayoutEnabled)) {
          imgBgStyle = {
            ...imgBgStyle,
            marginBottom: getBottomMargin(spacing.spacingBottomA, 'app'),
          };
        }
        return (
          Module &&
          slotData && (
            <>
              <ImageBackground source={imgBgSource} style={imgBgStyle}>
                {this.renderBorder(slot, BEFORE)}
                <StyledModuleWrapper
                  moduleName={slot.moduleName}
                  spacing={spacing || {}}
                  borderMedia={borderMedia}
                >
                  <Module
                    key={key}
                    accessibility={accessibility}
                    borderWidth={borderWidth}
                    isHpNewLayoutEnabled={isHpNewLayoutEnabled}
                    {...slotData}
                    {...others}
                  />

                  {this.renderBagCarousel(slots, index, others)}
                </StyledModuleWrapper>
                {this.renderBorder(slot, AFTER)}
              </ImageBackground>
            </>
          )
        );
      }
      return null;
    });
  }
}

export default PageSlots;
