// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import StyledModuleWrapper from './styles';
import Border from './Border/Border';
import {
  renderRulerEncloseBorder,
  renderSlotEncloseBorder,
  renderEnclosingBackground,
} from './EnclosingBorder.utils';

const BEFORE = 'before';
const AFTER = 'after';
const ENCLOSE = 'enclose';

const isBorderWidth = (borderMedia) => {
  const configObj = borderMedia && borderMedia.config;

  return configObj && (configObj.heightD || configObj.heightT || configObj.heightM);
};

class PageSlots extends PureComponent {
  static propTypes = {
    modules: PropTypes.shape({}).isRequired,
    slots: PropTypes.arrayOf(
      PropTypes.shape({
        contentId: PropTypes.string,
        data: PropTypes.shape({}),
        moduleName: PropTypes.string,
        name: PropTypes.string,
      })
    ).isRequired,
  };

  renderBorder = (slot, borderType) => {
    return (
      <>
        {slot.border &&
          slot.border
            .filter((bor) => bor.type === borderType)
            .map((bor) => {
              return (
                <Border
                  border={bor}
                  borderMedia={renderRulerEncloseBorder(slot, ENCLOSE, borderType)}
                />
              );
            })}
      </>
    );
  };

  render() {
    const { slots = [], modules, isHpNewModuleDesignEnabled, ...others } = this.props;
    const { isHomePage } = others;
    let isHeroSlot = false;
    return slots.map((slot, index) => {
      const key = index;
      isHeroSlot = isHomePage && slot.isHero;
      if (slot && slot.moduleName) {
        const Module = modules[slot.moduleName];
        const { data: slotData, accessibility, spacing } = slot;
        const borderMedia = renderSlotEncloseBorder(slot, ENCLOSE);
        const backgroundMedia = renderEnclosingBackground(slot);
        return (
          Module &&
          slotData && (
            <>
              {this.renderBorder(slot, BEFORE)}
              <StyledModuleWrapper
                spacing={spacing || {}}
                borderMedia={borderMedia}
                backgroundMedia={backgroundMedia}
              >
                <Module
                  isHeroSlot={isHeroSlot}
                  key={key}
                  accessibility={accessibility}
                  isBorderWidth={isBorderWidth(borderMedia) || false}
                  isHpNewModuleDesignEnabled={isHpNewModuleDesignEnabled}
                  {...slotData}
                  {...others}
                />
              </StyledModuleWrapper>
              {this.renderBorder(slot, AFTER)}
            </>
          )
        );
      }
      return null;
    });
  }
}

export default PageSlots;
