// 9fbef606107a605d69c0edbcd8029e5d 
const textPadding = {
  mobile: {
    none: 0,
    small: 6,
    medium: 8,
    large: 12,
    xLarge: 16,
    xxLarge: 20,
    xxxLarge: 24,
    mega: 32,
  },
  tablet: {
    none: 0,
    small: 8,
    medium: 12,
    large: 16,
    xLarge: 20,
    xxLarge: 24,
    xxxLarge: 28,
    mega: 48,
  },
  desktop: {
    none: 0,
    small: 12,
    medium: 16,
    large: 20,
    xLarge: 24,
    xxLarge: 28,
    xxxLarge: 32,
    mega: 64,
  },
  app: {
    none: 0,
    small: 6,
    medium: 8,
    large: 12,
    xLarge: 16,
    xxLarge: 20,
    xxxLarge: 24,
    mega: 32,
  },
};

const getTextPadding = (value, breakpoint) => {
  const defaultValue = 'medium';
  if (value) {
    return textPadding[breakpoint][value];
  }
  return textPadding[breakpoint][defaultValue];
};

export default getTextPadding;
