// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const StyledModuleWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.MODULE_SPACING.MED};
`;

export default StyledModuleWrapper;
