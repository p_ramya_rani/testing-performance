// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  display: flex;
  height: auto;
  justify-content: center;
  ${props =>
    props.isBorderWidth
      ? `
      margin-left:0;
      margin-right:0;
      width:100%;`
      : ``}

  .moduleN {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    max-height: 541px;
  }
  @media ${props => props.theme.mediaQuery.smallMax} {
    .moduleB {
      margin-bottom: ${props => props.theme.spacing.MODULE_SPACING.MED};
    }
  }

  @media ${props => props.theme.mediaQuery.mediumMax} {
    .moduleH,
    .moduleN,
    .moduleS {
      margin-bottom: ${props => props.theme.spacing.MODULE_SPACING.MED};
    }
  }
  @media ${props => props.theme.mediaQuery.large} {
    .moduleH,
    .moduleN {
      width: 100%;
    }
  }
`;
