// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import { PropTypes } from 'prop-types';
import { View } from 'react-native';
import ModuleB from '@tcp/core/src/components/common/molecules/ModuleB';
import ModuleN from '@tcp/core/src/components/common/molecules/ModuleN';
import style from '../ModuleTwoCol.style';
import errorBoundary from '../../../hoc/withErrorBoundary';
import withStyles from '../../../hoc/withStyles';
import { StyledModuleWrapper } from '../ModuleTwoCol.style.native';

const modulesMap = {
  moduleB: ModuleB,
  moduleN: ModuleN,
};

const DynamicColumns = (props) => {
  const { slot: slots, ...others } = props;

  return (
    slots &&
    slots.map((slotData) => {
      const Module = slotData ? modulesMap[slotData.moduleName] : null;
      return Module ? (
        <StyledModuleWrapper>
          <Module {...slotData} {...others} />
        </StyledModuleWrapper>
      ) : null;
    })
  );
};

const ModuleTwoColumns = (props) => {
  const { className } = props;

  return (
    <View className={`${className} moduleTwoCol`}>
      <DynamicColumns {...props} />
    </View>
  );
};

ModuleTwoColumns.propTypes = {
  className: PropTypes.string.isRequired,
};
export default withStyles(errorBoundary(ModuleTwoColumns), style);
