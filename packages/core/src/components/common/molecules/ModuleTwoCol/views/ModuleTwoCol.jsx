// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import { PropTypes } from 'prop-types';
import dynamic from 'next/dynamic';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import withStyles from '../../../hoc/withStyles';
import errorBoundary from '../../../hoc/withErrorBoundary';
import style from '../ModuleTwoCol.style';

const moduleB = dynamic(() =>
  import(
    /* webpackChunkName: "moduleB" */ '@tcp/core/src/components/common/molecules/ModuleB/view/ModuleB'
  )
);
const moduleN = dynamic(() =>
  import(
    /* webpackChunkName: "moduleN" */ '@tcp/core/src/components/common/molecules/ModuleN/views/ModuleN'
  )
);

const modules = { moduleB, moduleN };

function DynamicColumns(properties) {
  const { slot: slots, ...others } = properties;

  return (
    slots &&
    slots.map((slotData) => {
      const Module = slotData ? modules[slotData.moduleName] : null;
      return Module ? (
        <Col colSize={{ small: 6, medium: slotData.moduleName === 'moduleB' ? 4 : 8, large: 6 }}>
          <Module halfWidth {...slotData} {...others} />
        </Col>
      ) : null;
    })
  );
}

const ModuleTwoColumns = (props) => {
  const { className, ...others } = props;

  return (
    <Row className={`${className} moduleTwoCol`} fullBleed={{ small: true, medium: true }}>
      <DynamicColumns {...others} />
    </Row>
  );
};

ModuleTwoColumns.propTypes = {
  className: PropTypes.string.isRequired,
};
export default withStyles(errorBoundary(ModuleTwoColumns), style);
