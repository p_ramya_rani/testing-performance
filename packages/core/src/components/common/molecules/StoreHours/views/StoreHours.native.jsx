// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import StoreHoursRoot, {
  TimingsList,
  TimingsItem,
  TimingsText,
  StoreHoursTitle,
} from '../styles/StoreHours.style.native';

function storeHours(item, labels) {
  if (item.isClosed) {
    return (
      <TimingsText textAlign="right">
        {getLabelValue(labels, 'lbl_storeldetails_closed')}
      </TimingsText>
    );
  }
  return !Array.isArray(item.value) ? (
    <TimingsText textAlign="right">{item.value}</TimingsText>
  ) : (
    Array.isArray(item.value) &&
      item.value.map(val => (
        <TimingsText textAlign="right" key={`${item.id}-${val}`}>
          {val}
        </TimingsText>
      ))
  );
}

const StoreHours = ({ children, title, storeTiming, labels }) => {
  return (
    <StoreHoursRoot>
      <StoreHoursTitle>{title}</StoreHoursTitle>
      {storeTiming.length > 0 && (
        <TimingsList>
          {storeTiming.map(item => (
            <TimingsItem key={item.id}>
              <TimingsText textAlign="left">{item.label}</TimingsText>
              {storeHours(item, labels)}
            </TimingsItem>
          ))}
        </TimingsList>
      )}
      {children}
    </StoreHoursRoot>
  );
};

StoreHours.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node),
  title: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  storeTiming: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      label: PropTypes.string,
      value: PropTypes.string,
    })
  ),
};

StoreHours.defaultProps = {
  children: null,
  storeTiming: [],
};

export default StoreHours;
