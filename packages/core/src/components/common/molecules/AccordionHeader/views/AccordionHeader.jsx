// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import BodyCopy from '../../../atoms/BodyCopy';

const AccordionHeader = ({
  className,
  titleText,
  updateAccordionState,
  index,
  appliedFilterComponent,
  filterLength,
  showFilterSelectedCount,
}) => {
  return (
    <>
      <BodyCopy
        data-locator={`accordion-${index}`}
        tabIndex="0"
        className={className}
        onClick={updateAccordionState}
        onKeyPress={updateAccordionState}
        data-index={index}
        component="p"
        fontFamily="secondary"
        fontSize="fs13"
        lineHeight="lh115"
        color="text.primary"
      >
        {titleText}
        {showFilterSelectedCount && filterLength && filterLength > 0 ? ` (${filterLength})` : null}
        {filterLength && filterLength > 0 ? (
          <BodyCopy
            className="filter-count"
            data-index={index}
            component="span"
            fontFamily="secondary"
            fontSize="fs13"
            lineHeight="lh115"
            color="text.primary"
          >
            {`(${filterLength})`}
          </BodyCopy>
        ) : null}
      </BodyCopy>

      {appliedFilterComponent && appliedFilterComponent[titleText]}
    </>
  );
};

AccordionHeader.propTypes = {
  className: PropTypes.string.isRequired,
  titleText: PropTypes.string.isRequired,
  updateAccordionState: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  appliedFilterComponent: PropTypes.shape([]).isRequired,
  filterLength: PropTypes.number.isRequired,
  showFilterSelectedCount: PropTypes.bool.isRequired,
};
export default AccordionHeader;
