// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .moduleD_button {
    margin: 0 auto 32px;
    width: 225px;
    padding: 15px 0;

    @media ${props => props.theme.mediaQuery.medium} {
      padding: 11px 0;
      width: 162px;
    }

    @media ${props => props.theme.mediaQuery.large} {
      margin: 0 auto 48px;
      width: 210px;
    }
  }
  .moduleD_header {
    margin-bottom: 16px;
  }
  .moduleD_tile {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  }
  .moduleD_link {
    text-align: center;
    color: ${props => props.theme.colorPalette.text.primary};
    font-size: ${props => props.theme.typography.fontSizes.fs14};
  }
  .moduleD_image {
    width: 100%;
  }
  .moduleD__promo-banner {
    margin-bottom: 16px;
  }

  .button-list-wrapper {
    padding: 0;
  }

  .button-list-container {
    padding: 16px 0;
  }

  .button-list-container.stackedCTAList {
    padding: 0;
  }

  .button-list-container.imageCTAList .image-comp,
  .button-list-container.linkCTAList .link-button-wrapper-class {
    color: ${props =>
      props.theme.isGymboree
        ? props.theme.colors.BUTTON.WHITE
        : props.theme.colors.BUTTON[props.fill || 'WHITE'].TEXT};
  }
  .button-list-container.linkCTAList .link-button-wrapper-class {
    border-bottom-color: #ffffff;
    @media ${props => props.theme.mediaQuery.large} {
      ${props =>
        props.theme.isGymboree ? `font-weight: ${props.theme.typography.fontWeights.bold};` : ''}
    }
  }

  @media ${props => props.theme.mediaQuery.medium} {
    .button-list-container.stackedCTAList {
      padding: 16px 0 0;

      .wrapped-button-text .stacked-button .cta-button-text {
        padding: 11px 0;
      }
    }
  }

  &.brand-tcp.page-home .fixed_medium_text_black {
    font-family: TofinoCond;
    font-weight: 700;
    font-size: 32px;
    @media ${props => props.theme.mediaQuery.medium} {
      font-size: 48px;
    }
    @media ${props => props.theme.mediaQuery.large} {
      font-size: 88px;
    }
  }
`;
