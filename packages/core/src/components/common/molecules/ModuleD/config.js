// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  IMG_DATA_2: {
    imgConfig: [`t_mod_D_img2_m`, `t_mod_D_img2_t`, `t_mod_D_img2_d`],
  },
  IMG_DATA_4: {
    imgConfig: [`t_mod_D_img4_m`, `t_mod_D_img4_t`, `t_mod_D_img4_d`],
  },
  IMG_DATA_6: {
    imgConfig: [`t_mod_D_img6_m`, `t_mod_D_img6_t`, `t_mod_D_img6_d`],
  },
  ctaTypes: {
    stackedCTAButtons: 'stackedCTAList',
    linkList: 'linkCTAList',
    CTAButtonCarousel: 'scrollCTAList',
    divImageCTACarousel: 'imageCTAList',
    ctaAddNoButtons: 'NoButton',
  },
};
