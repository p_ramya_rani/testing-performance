// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import ModuleDNative from '../view/ModuleD.native';

describe('ModuleDNative', () => {
  let ModuleDComponent;

  beforeEach(() => {
    ModuleDComponent = shallow(<ModuleDNative />);
  });

  it('should be defined', () => {
    expect(ModuleDNative).toBeDefined();
  });

  it('should render correctly', () => {
    expect(ModuleDComponent).toMatchSnapshot();
  });

  it('should render Module D correctly when isHpNewLayoutEnabled is true', () => {
    ModuleDComponent = shallow(<ModuleDNative isHpNewLayoutEnabled />);
    expect(ModuleDComponent).toMatchSnapshot();
  });
});
