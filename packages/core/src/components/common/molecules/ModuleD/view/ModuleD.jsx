// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import Grid from '@tcp/core/src/components/common/molecules/Grid';
import ButtonList from '@tcp/core/src/components/common/molecules/ButtonList';
import { Anchor, Col, DamImage, Row } from '@tcp/core/src/components/common/atoms';
import {
  getLocator,
  configureInternalNavigationFromCMSUrl,
  styleOverrideEngine,
  getBrand,
} from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { getMediaBorderRadius } from '@tcp/core/src/utils/utils.web';
import config from '../config';
import style from '../ModuleD.style';

const colSize2Elements = {
  small: 3,
  medium: 3,
  large: 4,
};

const colSize4Elements = {
  small: 3,
  medium: 2,
  large: 3,
};

const colSize6Elements = {
  small: 3,
  medium: 3,
  large: 2,
};

const ignoreGutter = [
  {},
  { small: true },
  {},
  { small: true, medium: true },
  {},
  { small: true, medium: true },
];

const getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};

const ModuleD = (props) => {
  const {
    className,
    headerText,
    promoBanner,
    smallCompImage,
    fullBleed,
    ctaType,
    moduleClassName,
    page,
    ctaItems,
    isHpNewDesignCTAEnabled,
    isHpNewModuleDesignEnabled,
    ...others
  } = props;

  let colSize;
  let imgDataConfig;
  const checkPromo = promoBanner && promoBanner.length;
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleD');
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
  const headerTextStyles = [
    styleOverrides.headerText1,
    styleOverrides.headerText2,
    styleOverrides.headerText3,
  ];
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewModuleDesignEnabled,
    styleOverrides
  );
  const promoStyle = styleOverrides.promo;

  const { ctaTypes } = config;
  const buttonListCtaType = ctaTypes[ctaType];

  if (smallCompImage && smallCompImage.length === 2) {
    colSize = colSize2Elements;
    imgDataConfig = config.IMG_DATA_2.imgConfig;
  } else if (smallCompImage && smallCompImage.length === 4) {
    colSize = colSize4Elements;
    imgDataConfig = config.IMG_DATA_4.imgConfig;
  } else {
    colSize = colSize6Elements;
    imgDataConfig = config.IMG_DATA_6.imgConfig;
  }
  return (
    <Grid className={`${className} ${moduleClassName} moduleD brand-${getBrand()} page-${page}`}>
      <div style={mediaBorderRadiusOverride}>
        {headerText && (
          <LinkText
            headerText={headerText}
            headingClass="moduleD_header"
            component="h2"
            type="heading"
            textAlign="center"
            dataLocator="moduleD_headerlink"
            promo={checkPromo}
            headerStyle={headerStyle}
            headerTextStyles={headerTextStyles}
          />
        )}
        {checkPromo && (
          <PromoBanner
            promoBanner={promoBanner}
            className="moduleD__promo-banner"
            fontSize="fs48"
            data-locator={getLocator('moduleD_promobanner')}
            promoStyle={promoStyle}
          />
        )}
        <Row centered fullBleed={fullBleed} className="moduleD__image-container-section">
          {smallCompImage &&
            smallCompImage.map((item, index) => {
              return (
                item.link && (
                  <Col
                    key={index.toString()}
                    colSize={colSize}
                    ignoreGutter={ignoreGutter[index]}
                    className="moduleD_tile"
                  >
                    <div className="moduleD__image-container">
                      <DamImage
                        className="moduleD_image"
                        dataLocator={`${getLocator('moduleD_image')}${index + 1}`}
                        imgConfigs={imgDataConfig}
                        imgData={item.image}
                        videoData={item.video}
                        link={{
                          className: 'moduleD_textlink',
                          ...item.link,
                        }}
                        lazyLoad
                        isModule
                        {...others}
                      />
                    </div>
                    <div className="moduleD_link">
                      <Anchor
                        withCaret
                        centered
                        className="moduleD_textlink"
                        to={configureInternalNavigationFromCMSUrl(item.link.url)}
                        asPath={item.link.url}
                        target={item.link.target}
                        title={item.link.title}
                        dataLocator={`${getLocator('moduleD_textlink')}${index + 1}`}
                      >
                        {item.link.text}
                      </Anchor>
                    </div>
                  </Col>
                )
              );
            })}
        </Row>
        <div className={`button-list-container ${buttonListCtaType}`}>
          {ctaItems && (
            <ButtonList
              buttonsData={ctaItems}
              buttonListVariation={buttonListCtaType}
              dataLocatorDivisionImages={getLocator('moduleD_cta_image')}
              dataLocatorTextCta={getLocator('moduleD_cta_links')}
              isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
            />
          )}
        </div>
      </div>
    </Grid>
  );
};

ModuleD.defaultProps = {
  promoBanner: [],
  fullBleed: false,
  moduleClassName: '',
  page: '',
  ctaItems: [],
  ctaType: 'stackedCTAButtons',
};

ModuleD.propTypes = {
  className: PropTypes.string.isRequired,
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.object,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.object,
    })
  ),
  smallCompImage: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      image: PropTypes.object,
    })
  ).isRequired,
  fullBleed: PropTypes.bool,
  moduleClassName: PropTypes.string,
  page: PropTypes.string,
  ctaItems: PropTypes.arrayOf(PropTypes.shape({})),
  ctaType: PropTypes.oneOf(['stackedCTAButtons', 'linkCTAList', 'scrollCTAList', 'imageCTAList']),
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

export default errorBoundary(withStyles(ModuleD, style));
export { ModuleD as ModuleDVanilla };
