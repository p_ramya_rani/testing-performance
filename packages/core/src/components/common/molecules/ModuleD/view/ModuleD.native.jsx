// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { FlatList, View } from 'react-native';
import PropTypes from 'prop-types';
import HP_NEW_LAYOUT from '@tcp/core/src/constants/hpNewLayout.constants';
import {
  getLocator,
  LAZYLOAD_HOST_NAME,
  styleOverrideEngine,
  isGymboree,
  getMediaBorderRadius,
  getCtaBorderRadius,
} from '../../../../../utils/index.native';
import ButtonList from '../../ButtonList';
import { Anchor, DamImage } from '../../../atoms';
import PromoBanner from '../../PromoBanner';
import spacing from '../../../../../../styles/themes/TCP/spacing';
import colors from '../../../../../../styles/themes/TCP/colors';
import {
  Tile,
  Wrapper,
  HeaderContainer,
  ListContainer,
  ContainerView,
  DAMImageCTAContainer,
  ButtonLinksContainer,
  ButtonContainer,
} from '../ModuleD.style.native';
import LinkText from '../../LinkText';
import config from '../config';

const imageSize = 164;
const keyExtractor = (_, index) => index.toString();

/**
 * To Render the Dam Image or Video Component
 */
const renderDamImage = (link, imgData, videoData, navigation, ignoreLazyLoadImage, index) => {
  const damImageComp = (
    <DamImage
      alt={imgData && imgData.alt}
      videoData={videoData}
      testID={`${getLocator('moduleD_image')}${index + 1}`}
      imgData={imgData}
      crop={imgData && imgData.crop_m}
      height={imageSize}
      marginBottom={parseInt(spacing.ELEM_SPACING.XS, 10)}
      width={imageSize}
      imgConfig={config.IMG_DATA_2.imgConfig[0]}
      host={ignoreLazyLoadImage ? '' : LAZYLOAD_HOST_NAME.HOME}
      isFastImage
      resizeMode="stretch"
      isHomePage
      link={link}
      navigation={navigation}
    />
  );
  if (imgData && Object.keys(imgData).length > 0) {
    return (
      <Anchor url={link.url} navigation={navigation}>
        {damImageComp}
      </Anchor>
    );
  }
  return videoData && Object.keys(videoData).length > 0 ? (
    <React.Fragment>{damImageComp}</React.Fragment>
  ) : null;
};

/**
 * @function renderItem : Render method for Flatlist.
 * @desc This method is rendering Module D items.
 *
 * @param {Object} item : Single object to render inside Flatlist.
 * @return {node} function returns module D single element item.
 */
const renderItem = (item, navigation, ignoreLazyLoadImage) => {
  const {
    item: { image, link = {}, video = {} },
    index,
  } = item;
  const anchorEnable = true;
  const videoData = video &&
    video.url && {
      videoWidth: imageSize,
      videoHeight: imageSize,
      ...video,
    };
  return (
    <Tile
      tileIndex={index}
      key={index.toString()}
      accessible
      accessibilityLabel={link && link.text}
      accessibilityRole="link"
    >
      {renderDamImage(link, image, videoData, navigation, ignoreLazyLoadImage, index)}
      <Anchor
        testID={`${getLocator('moduleD_textlink')}${index + 1}`}
        fontSizeVariation="large"
        text={link.text}
        visible={anchorEnable}
        url={link.url}
        navigation={navigation}
        anchorVariation="primary"
      />
    </Tile>
  );
};

/**
 * This method return the ButtonList View according to the different variation .
 *  @ctaType are four types : 'imageCTAList' ,'stackedCTAList','scrollCTAList','linkCTAList'.
 *  @naviagtion is used to navigate the page.
 */
const renderButtonList = (
  ctaType,
  navigation,
  ctaItems,
  locator,
  buttonVariation,
  color,
  ctaBorderRadiusOverride
) => {
  return (
    ctaItems && (
      <ButtonList
        buttonListVariation={ctaType}
        navigation={navigation}
        buttonsData={ctaItems}
        locator={locator}
        color={color}
        overrideStyle={ctaBorderRadiusOverride}
      />
    )
  );
};

const renderButtonSection = (
  ctaTypes,
  ctaTypeValue,
  navigation,
  ctaItems,
  { styleOverrides, ctaBorderRadiusOverride }
) => {
  if (ctaTypeValue === ctaTypes.divImageCTACarouse) {
    return (
      <DAMImageCTAContainer>
        {renderButtonList(ctaTypeValue, navigation, ctaItems, 'moduleA_cta_links', 'black')}
      </DAMImageCTAContainer>
    );
  }

  if (ctaTypeValue === ctaTypes.stackedCTAButtons) {
    return (
      <ContainerView>
        {renderButtonList(
          ctaTypeValue,
          navigation,
          ctaItems,
          'stacked_cta_list',
          'fixed-width',
          'gray',
          ctaBorderRadiusOverride
        )}
      </ContainerView>
    );
  }

  if (ctaTypeValue === ctaTypes.CTAButtonCarousel) {
    return (
      <ButtonContainer style={styleOverrides['nav-bg']}>
        {renderButtonList(ctaTypeValue, navigation, ctaItems, 'scroll_cta_list', 'gray')}
      </ButtonContainer>
    );
  }

  if (ctaTypeValue === ctaTypes.linkList) {
    return (
      <ButtonLinksContainer style={styleOverrides['nav-bg']}>
        {renderButtonList(ctaTypeValue, navigation, ctaItems, 'link_cta_list', 'gray')}
      </ButtonLinksContainer>
    );
  }
  return null;
};

const getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled
    ? styleOverrides &&
        getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};

const getCtaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled
    ? styleOverrides && getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom'])
    : {};
};

/**
 *
 * @function ModuleD: Props for Module D multi grid banner.
 * @desc This is Module D global component. It has capability to display
 * featured content module with 2, 4, or 6 images tiles and a CTA.
 * Author can surface teaser content leading to corresponding pages.
 *
 * Props: Includes composites of headerText, smallCompImage and singleCTAButton.
 * @prop {array} headerText: Data for header text and link.
 * @prop {array} smallCompImage: Data for images and their links.
 * @prop {object} singleCTAButton: Data for CTA button and its target.
 * @prop {array} promoBanner: Data for Promo Banner.
 * @prop {object} navigation: Naviation object.
 */

const ModuleD = props => {
  const {
    smallCompImage,
    headerText,
    promoBanner,
    navigation,
    ignoreLazyLoadImage,
    moduleClassName,
    ctaType,
    ctaItems,
    isHpNewLayoutEnabled,
  } = props;

  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleD') || {};
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
  const headerTextStyles = [
    styleOverrides.headerText1,
    styleOverrides.headerText2,
    styleOverrides.headerText3,
  ];
  const promoStyle = styleOverrides.promo;
  const { ctaTypes } = config;
  const ctaTypeValue = ctaTypes[ctaType];
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewLayoutEnabled,
    styleOverrides
  );
  const ctaBorderRadiusOverride = getCtaBorderRadiusOverride(isHpNewLayoutEnabled, styleOverrides);

  const wrapperStyle = isHpNewLayoutEnabled
    ? {
        ...mediaBorderRadiusOverride,
        backgroundColor: colors?.WHITE,
      }
    : {};

  return (
    <Wrapper style={isHpNewLayoutEnabled && HP_NEW_LAYOUT.MODULE_BOX_SHADOW}>
      <View style={wrapperStyle}>
        <HeaderContainer>
          {headerText && (
            <LinkText
              headerText={headerText}
              navigation={navigation}
              fontFamily={isGymboree() ? 'primary' : 'tertiary'}
              fontSize="fs32"
              letterSpacing={isGymboree() ? 'ls167' : undefined}
              textAlign="center"
              color="text.primary"
              fontWeight="bold"
              type="heading"
              testID={getLocator('moduleD_headerlink')}
              headerStyle={headerStyle}
              headerTextStyles={headerTextStyles}
            />
          )}
          {promoBanner && (
            <PromoBanner
              promoBanner={promoBanner}
              testID={getLocator('moduleD_promobanner')}
              navigation={navigation}
              promoStyle={promoStyle}
            />
          )}
        </HeaderContainer>

        <ListContainer>
          {smallCompImage && (
            <FlatList
              numColumns={2}
              data={smallCompImage}
              keyExtractor={keyExtractor}
              renderItem={item => renderItem(item, navigation, ignoreLazyLoadImage)}
            />
          )}
        </ListContainer>
      </View>

      {renderButtonSection(ctaTypes, ctaTypeValue, navigation, ctaItems, {
        styleOverrides,
        ctaBorderRadiusOverride,
      })}
    </Wrapper>
  );
};

ModuleD.defaultProps = {
  promoBanner: [],
  ignoreLazyLoadImage: false,
  moduleClassName: '',
  ctaItems: 'stackedCTAButtons',
  ctaType: [],
};

ModuleD.propTypes = {
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.object,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.object,
    })
  ),
  smallCompImage: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      image: PropTypes.object,
    })
  ).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  ignoreLazyLoadImage: PropTypes.bool,
  moduleClassName: PropTypes.string,
  ctaItems: PropTypes.arrayOf(PropTypes.shape({})),
  ctaType: PropTypes.oneOf(['stackedCTAButtons', 'linkCTAList', 'scrollCTAList', 'imageCTAList']),
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleD;
