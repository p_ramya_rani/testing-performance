// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const ButtonWrapper = styled.View`
  align-items: center;
  display: flex;
`;

export const Heading = styled.Text`
  color: ${props => props.theme.colors.PRIMARY.DARK};
  font-size: ${props => props.theme.fonts.fontSize.heading.large.h3}px;
  font-weight: bold;
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.XS};
  margin-top: ${props => props.theme.spacing.LAYOUT_SPACING.SM};
  text-align: center;
  text-transform: uppercase;
`;

export const Wrapper = styled.View`
  display: flex;
`;

export const ContainerView = styled.View``;

export const DAMImageCTAContainer = styled.View``;

export const ButtonContainer = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const ButtonLinksContainer = styled.View`
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  background: #003057;
`;

export const Tile = styled.View`
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
  ${props => (props.tileIndex % 2 === 0 ? `margin-right: 14px ` : `margin-left: 5px`)};
`;

export const HeaderContainer = styled.View`
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
`;

export const ListContainer = styled.View`
  align-items: center;
`;

export default {
  ButtonWrapper,
  Heading,
  Wrapper,
  Tile,
  HeaderContainer,
  ListContainer,
};
