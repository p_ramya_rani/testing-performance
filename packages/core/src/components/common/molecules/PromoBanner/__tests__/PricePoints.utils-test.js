import { getPricePointColText, getPricePointRowText } from '../PricePoints.utils';

describe('#PricePoints utils', () => {
  const text = 'some random text for testing utils';

  it('Check for style 4', () => {
    expect(getPricePointColText('style_4', text)).toEqual({
      part1: 'some',
      part3: 'random',
      part4: 'text',
      part5: 'for',
    });
  });

  it('Check for style 8', () => {
    expect(getPricePointColText('style_8', text)).toEqual({
      part3: 'some',
      part4: 'random',
      part5: 'text',
    });
  });

  it('Check for style 5', () => {
    expect(getPricePointColText('style_5', text)).toEqual({
      part1: 'some',
      part2: 'random',
      part3: 'text',
      part4: 'for',
      part5: 'testing',
    });
  });

  it('Check for style 3', () => {
    expect(getPricePointRowText('style_3', text)).toEqual({
      part1: 'some',
      part4: 'random',
      part5: 'text',
      part6: 'for',
    });
  });

  it('Check for style 5', () => {
    expect(getPricePointRowText('style_5', text)).toEqual({
      part1: 'some',
      part2: 'random',
      part4: 'text',
      part5: 'for',
    });
  });

  it('Check for style 16', () => {
    expect(getPricePointRowText('style_16', text)).toEqual({
      part1: 'some',
      part4: 'random',
      part5: 'text',
      part6: 'for',
    });
  });
});
