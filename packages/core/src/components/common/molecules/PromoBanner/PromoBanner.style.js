/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { cropImageUrl } from '@tcp/core/src/utils';
import PricePoint from './PricePoints.style';

const mediumTextRegular = (props, variation) => {
  return `
    {
      color: ${props.theme.colorPalette.gray['900']};
      display: block;
      font-family: ${props.theme.typography.fonts.primary};
      font-size: ${props.theme.typography.fontSizes.fs20};
      font-weight: ${props.theme.typography.fontWeights.semibold};
      line-height: ${props.theme.typography.lineHeights.lh107};
      letter-spacing: ${props.theme.typography.letterSpacings.ls1};

      ${
        variation === 'tab'
          ? `
        @media ${props.theme.mediaQuery.large} {
          font-size: ${props.theme.typography.fontSizes.fs32};
        }
      `
          : `
      @media ${props.theme.mediaQuery.medium} {
        font-size: ${props.theme.typography.fontSizes.fs32};
      }
      `
      }
    }
  `;
};

const percentageAllWrappedNormal = (props, variation) => {
  const className = variation
    ? `percentage_all_wrapped_normal${variation}`
    : `percentage_all_wrapped_normal`;
  return `
    {
      color: ${props.theme.colorPalette.gray['900']};
      display: flex;
      justify-content: center;
      font-family: ${props.theme.typography.fonts.primary};
      font-size: ${props.theme.typography.fontSizes.fs48};
      font-weight: ${props.theme.typography.fontWeights.black};
      text-align: center;
      line-height: 1;

      .${className}-1 {
        font-size: ${props.theme.typography.fontSizes.fs28};
      }

      .${className}-2 {
        font-size: ${props.theme.typography.fontSizes.fs18};
      }

      @media ${props.theme.mediaQuery.medium} {
        margin-top: -${props.theme.spacing.ELEM_SPACING.XL};

        .${className}-0 {
          font-size: 152px;
        }
        .${className}-1 {
          font-size: 88px;
        }
        .${className}-2 {
          font-size: ${props.theme.typography.fontSizes.fs48};
        }
      }
    }
  `;
};

export const StyledPromoDiv = styled.div`
  ${(props) => props.customStyle || ''}
`;

export const StyledPromoSpan = styled.span`
  ${(props) => props.customStyle || ''}
`;

const getBackground = (backgroundColorObj) => {
  const [{ colorD, colorM, imageD, imageM }] = backgroundColorObj || [{}];

  return {
    desktop: colorD?.color,
    mobile: colorM?.color,
    imageD: imageD?.url && cropImageUrl(imageD?.url),
    imageM: imageM?.url && cropImageUrl(imageM?.url),
  };
};

// TODO: Remove style10 when currency_up_style is added to CMS
export default css`
  font-family: ${(props) => props.theme.typography.fonts.primary};
  text-align: center;

  &&& {
    ${(props) =>
      props.backgroundColorObj && getBackground(props.backgroundColorObj).desktop
        ? `background: ${getBackground(props.backgroundColorObj).desktop}; `
        : null};
    ${(props) =>
      props.backgroundColorObj && getBackground(props.backgroundColorObj).mobile
        ? `@media ${props.theme.mediaQuery.smallOnly} {
        background: ${getBackground(props.backgroundColorObj).mobile};
      }`
        : null};
    ${(props) =>
      props.backgroundColorObj && getBackground(props.backgroundColorObj).imageD
        ? `background: transparent url(${
            getBackground(props.backgroundColorObj).imageD
          }) no-repeat 0 0;
       background-size: cover;
      `
        : null};
    ${(props) =>
      props.backgroundColorObj && getBackground(props.backgroundColorObj).imageM
        ? `@media ${props.theme.mediaQuery.smallOnly} {
        background: transparent url(${
          getBackground(props.backgroundColorObj).imageM
        }) no-repeat 0 0;
        background-size: cover;
      `
        : null};
  }

  .promo-text {
    display: block;

    @media ${(props) => props.theme.mediaQuery.medium} {
      display: inline;
    }
  }
  .style1 {
    color: ${(props) => props.theme.colorPalette.gray['900']};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs42};

    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: 70px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 60px;
    }
  }
  .style2 {
    color: ${(props) => props.theme.colorPalette.gray['900']};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    font-size: 70px;

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 60px;
    }
  }

  .promo-banner-header {
    display: block;
    border-bottom: 1px solid ${(props) => props.theme.colorPalette.gray[900]};
    padding-bottom: 5px;
    margin: 8px 18px;
    .link-text {
      margin: 0;
    }
    .style1,
    .style2 {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      display: block;
    }
    .style2 {
      font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 12px 38px 15px 38px;
      padding-bottom: 7px;
      .style1,
      .style2 {
        font-size: ${(props) => props.theme.typography.fontSizes.fs16};
      }
    }
  }

  .currency_up_style,
  .style10 {
    display: inline-flex;
    justify-content: center;

    .col-1,
    .col-2 {
      display: flex;
    }
    .col-2 {
      flex-direction: column;
    }
    .currency_up_style-0,
    .style10-0 {
      font-size: ${(props) => props.theme.typography.fontSizes.fs28};
      font-weight: ${(props) => props.theme.typography.fontWeights.black};
      line-height: 1.3;
    }
    .currency_up_style-1,
    .style10-1 {
      line-height: 0.85;
      font-size: 86px;
      font-weight: 900;
    }
    .currency_up_style-2,
    .style10-2 {
      font-size: 48px;
      font-weight: ${(props) => props.theme.typography.fontWeights.black};
      line-height: 1;
    }
    .currency_up_style-3,
    .style10-3 {
      font-size: ${(props) => props.theme.typography.fontSizes.fs28};
      font-weight: ${(props) => props.theme.typography.fontWeights.black};
      line-height: 0.6;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      .col-1,
      .col-2 {
        display: flex;
      }
      .col-2 {
        flex-direction: column;
      }
      .currency_up_style-0,
      .style10-0 {
        font-size: ${(props) => props.theme.typography.fontSizes.fs38};
        font-weight: ${(props) => props.theme.typography.fontWeights.black};
        line-height: 1.3;
      }
      .currency_up_style-1,
      .style10-1 {
        line-height: 0.8;
        font-size: 118px;
        font-weight: ${(props) => props.theme.typography.fontWeights.black};
      }
      .currency_up_style-2,
      .style10-2 {
        font-size: 66px;
        font-weight: ${(props) => props.theme.typography.fontWeights.black};
        line-height: 1;
      }
      .currency_up_style-3,
      .style10-3 {
        font-size: ${(props) => props.theme.typography.fontSizes.fs40};
        font-weight: ${(props) => props.theme.typography.fontWeights.black};
        line-height: 0.6;
      }
    }
  }

  .small_text_semibold {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs20};
    font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
    letter-spacing: ${(props) => props.theme.typography.letterSpacings.ls222};
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs32};
      font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
      letter-spacing: ${(props) => props.theme.typography.letterSpacings.ls222};
    }
  }

  .small_text_black {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs20};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    letter-spacing: ${(props) => props.theme.typography.letterSpacings.ls222};

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs36};
      font-weight: ${(props) => props.theme.typography.fontWeights.black};
      letter-spacing: ${(props) => props.theme.typography.letterSpacings.ls222};
    }
  }

  .text_normal {
    font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.regular};
    text-align: center;
    color: ${(props) => props.theme.colorPalette.white};
    display: block;

    @media ${(props) => props.theme.mediaQuery.large} {
      display: inline-block;
      font-size: ${(props) => props.theme.typography.fontSizes.fs26};
      text-align: left;
      margin-left: 10px;
      position: relative;
      line-height: normal;
      top: 6px;
    }
  }

  /* Module-A and N % style promo text */
  .percentage_wrapped_large {
    color: ${(props) => props.theme.colorPalette.white};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    font-size: ${(props) => props.theme.typography.fontSizes.fs64};
    line-height: 1;
    display: flex;
    justify-content: center;

    .percentage_wrapped_large__wrapped_text {
      display: flex;
      flex-direction: column;
      justify-content: center;
    }

    .percentage_wrapped_large-1 {
      font-size: ${(props) => props.theme.typography.fontSizes.fs42};
    }

    .percentage_wrapped_large-2 {
      font-size: ${(props) => props.theme.typography.fontSizes.fs18};
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      margin-left: 10px;

      .percentage_wrapped_large__wrapped_text {
        flex-direction: row;
      }

      .percentage_wrapped_large-0,
      .percentage_wrapped_large-1,
      .percentage_wrapped_large-2 {
        font-size: ${(props) => props.theme.typography.fontSizes.fs64};
        white-space: pre;
      }
    }
  }

  .percentage_wrapped_extra_large {
    color: ${(props) => props.theme.colorPalette.pink['400']};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    font-size: 153px;
    line-height: 0.91;
    width: 300px;
    text-align: left;
    display: flex;
    justify-content: center;

    .percentage_wrapped_extra_large__wrapped_text {
      display: flex;
      flex-direction: column;
      justify-content: center;
      text-align: center;
    }

    .percentage_wrapped_extra_large-1 {
      font-size: 99px;
      line-height: 1.01;
    }

    .percentage_wrapped_extra_large-2 {
      font-size: 43px;
      bottom: 8px;
    }

    @media ${(props) => props.theme.mediaQuery.medium} {
      width: auto;
      text-align: center;
      line-height: 0.6;

      .percentage_wrapped_extra_large__wrapped_text {
        flex-direction: row;
      }

      .percentage_wrapped_extra_large-0,
      .percentage_wrapped_extra_large-1,
      .percentage_wrapped_extra_large-2 {
        font-size: 122px;
        line-height: 1;
        white-space: pre;
      }
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      .percentage_wrapped_extra_large-0,
      .percentage_wrapped_extra_large-1,
      .percentage_wrapped_extra_large-2 {
        font-size: 204px;
      }
    }
  }

  .small_text_bold {
    color: ${(props) => props.theme.colorPalette.text.primary};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    line-height: normal;
    display: inline;
    vertical-align: top;

    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      line-height: normal;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs20};
      line-height: normal;
    }
  }

  .small_text_normal {
    color: ${(props) => props.theme.colorPalette.text.primary};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.regular};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    line-height: normal;
    display: inline;
    vertical-align: top;

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs20};
      line-height: normal;
    }
  }

  /* Gymboree Ribbon text */
  .ribbon_default_text {
    color: ${(props) => props.theme.colorPalette.white};
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-weight: ${(props) => props.theme.typography.fontWeights.normal};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    display: block;

    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs18};
    }
  }

  .gymboree_description {
    color: ${(props) => props.theme.colorPalette.white};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.regular};
    font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    display: block;

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs28};
    }
  }

  /*
  * Module J Promo Banner styles
  *********************************/
  .extra_large_text_regular {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs48};

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 86px;
    }
  }
  .extra_large_text_black {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs48};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 86px;
    }
  }
  .medium_text_semibold {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs32};
    font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
    letter-spacing: ${(props) => props.theme.typography.letterSpacings.ls2};

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs64};
    }
  }
  .fixed_medium_text_black {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs64};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
  }

  /*
  * Module R Promo Banner styles
  *********************************/
  .medium_text_regular {
    ${(props) => mediumTextRegular(props)}
  }
  /*
  * Module S Promo Banner styles
  *********************************/
  .medium_text_black {
    display: block;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs36};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    letter-spacing: normal;
  }

  .percentage_all_wrapped_normal {
    ${(props) => percentageAllWrappedNormal(props)};
  }

  /*
  * Module T Promo Banner styles
  *********************************/
  .medium_text_regular_tab {
    ${(props) => mediumTextRegular(props, 'tab')};
  }

  .large_text_bold {
    color: ${(props) => props.theme.colorPalette.gray['900']};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    font-size: ${(props) => props.theme.typography.fontSizes.fs48};
    letter-spacing: -0.5px;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs64};
    }
  }

  .percentage_all_wrapped_normal_tab {
    ${(props) => percentageAllWrappedNormal(props, '_tab')};

    .percentage_all_wrapped_normal_tab__wrapped_text {
      display: flex;
      flex-direction: column;
      justify-content: center;
    }

    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      .percentage_all_wrapped_normal_tab-0 {
        font-size: 62px;
      }
      .percentage_all_wrapped_normal_tab-1 {
        font-size: ${(props) => props.theme.typography.fontSizes.fs36};
        white-space: normal;
      }
      .percentage_all_wrapped_normal_tab-2 {
        font-size: ${(props) => props.theme.typography.fontSizes.fs18};
      }
    }
  }

  /*
  * Module N Promo Banner styles Gymboree
  *********************************/
  .percentage_inline_promo,
  .percentage_inline_promo_black {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    font-size: ${(props) => props.theme.typography.fontSizes.fs48};
    letter-spacing: 0px;
    color: ${(props) => props.theme.colorPalette.white};
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs64};
    }
  }

  .percentage_inline_promo_black {
    color: ${(props) => props.theme.colorPalette.gray[900]};
  }

  .spaced_text_regular_black {
    display: block;
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.regular};
    font-size: ${(props) => props.theme.typography.fontSizes.fs20};
    color: ${(props) => props.theme.colorPalette.gray[900]};
    letter-spacing: 2px;

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs32};
    }
  }

  .spaced_text_only_mobile {
    display: block;
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.regular};
    font-size: ${(props) => props.theme.typography.fontSizes.fs20};
    color: ${(props) => props.theme.colorPalette.gray[900]};
    letter-spacing: 2px;

    @media ${(props) => props.theme.mediaQuery.large} {
      display: inline-block;
      font-size: ${(props) => props.theme.typography.fontSizes.fs64};
      letter-spacing: 0;
    }
  }

  /** Global Navigation Clearance Tab Style */
  .percentage_wrapped_nav {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    color: ${(props) => props.theme.colorPalette.black};
    font-size: 81.4px;
    letter-spacing: 0.13px;
    text-shadow: 3px 3px 0 rgba(0, 0, 0, 0.2);
    line-height: normal;
    text-align: left;
    margin: 0 auto;
    position: relative;
    display: inline-block;
    .percentage_wrapped_nav-1 {
      font-size: 42.4px;
      position: absolute;
      top: 8px;
      letter-spacing: 0.09px;
    }
    .percentage_wrapped_nav-2 {
      font-size: 32.6px;
      position: absolute;
      bottom: 12px;
      letter-spacing: 0.07px;
    }
  }
  .percentage_nav_inline {
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    color: ${(props) => props.theme.colorPalette.black};
    font-size: 36.5px;
    letter-spacing: 0.11px;
    text-shadow: 2px 2px 0 rgba(0, 0, 0, 0.2);
    line-height: normal;
  }
  /**
   * Module G Promo Banner Style
   *
  */
  .large_text_black {
    color: ${(props) => props.theme.colorPalette.gray['900']};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    font-size: ${(props) => props.theme.typography.fontSizes.fs48};
    letter-spacing: normal;
    line-height: normal;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs64};
    }
  }
  /*
  * Module M Promo Banner styles
  *********************************/
  .small_white_text_semibold {
    color: ${(props) => props.theme.colorPalette.white};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs20};
    font-weight: ${(props) => props.theme.typography.fontWeights.medium};
    line-height: 20px;
    letter-spacing: 2px;
    text-align: center;

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs64};
      letter-spacing: 0;
    }
  }

  .extra_large_white_text_black {
    color: ${(props) => props.theme.colorPalette.white};
    font-family: ${(props) => props.theme.typography.fonts.primary};
    font-size: ${(props) => props.theme.typography.fontSizes.fs48};
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    text-align: center;

    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs64};
    }
  }

  /*
  * Module K Promo Banner styles
  *********************************/
  .mod_k_subheader {
    font-size: ${(props) => props.theme.typography.fontSizes.fs22};
    font-weight: ${(props) => props.theme.typography.fontWeights.regular};
    line-height: 30px;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
  }

  ${PricePoint}
`;
