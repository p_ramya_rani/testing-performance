// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

import { BodyCopy } from '../../../../atoms';

const StyledBodyCopy = styled(BodyCopy)`
  ${({ lineHeight }) => (lineHeight ? `line-height: ${lineHeight}` : '')}
  text-align: center;
`;

export { StyledBodyCopy as BodyCopy };

export const FirstColDirectionView = styled.View`
  ${props => (props.isRowDirection ? 'flex-direction: row;' : 'flex-direction: column;')}
  align-items: flex-start;
`;
export const LastColDirectionView = styled.View`
  ${props => (props.isRowDirection ? 'flex-direction: row;' : 'flex-direction: column;')}
  align-items: flex-start;
`;

export const TopAlignedView = styled.View`
  flex-direction: row;
  justify-content: center;
  ${props => (props.renderInSameLine && !props.isLastTextItem ? `padding-right: 10px` : null)};
  ${props => (props.isContainerModuleText ? null : `width: 100%`)};

  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
`;

export default {
  StyledBodyCopy: BodyCopy,
  FirstColDirectionView,
  LastColDirectionView,
  TopAlignedView,
};
