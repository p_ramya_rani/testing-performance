/* eslint-disable react-native/no-inline-styles, max-lines, react/prop-types */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import { PropTypes } from 'prop-types';
import { Anchor } from '../../../atoms';
import { safelyParseJSON } from '../../../../../utils/utils';
import { BodyCopy, Container, ContainerView, StyledTextView } from '../PromoBanner.style.native';

import CurrencyUpPromoBanner from './CurrencyUpPromoBanner';

import PricePointsStyles from './PricePointStyles.native';

const TYPE_PROM_TAB = 'type-promo-tab';

/* bodyCopyStyles is a array of BodyCopy component with key of style1,style2,style3 etc.
    The keys are coming from CMS */
export const bodyCopyStyles = {
  // large_text_normal
  style1: props => (
    <BodyCopy color="text.primary" fontFamily="primary" fontSize="fs42" {...props} />
  ),
  // large_text_bold
  style2: props => (
    <BodyCopy
      color="text.primary"
      fontFamily="primary"
      fontSize="fs42"
      textAlign="center"
      lineHeight="42px"
      fontWeight="medium"
      {...props}
    />
  ),
  style3: props => (
    <BodyCopy
      fontSize="fs64"
      fontWeight="black"
      color="black"
      fontFamily="primary"
      lineHeight="64px"
      textAlign="center"
      {...props}
    />
  ),
  text_normal: props => (
    <BodyCopy fontSize="fs16" color="white" fontFamily="primary" textAlign="center" {...props} />
  ),
  style5: props => (
    <BodyCopy
      fontSize="fs64"
      fontWeight="black"
      color="white"
      fontFamily="primary"
      lineHeight="64px"
      textAlign="center"
      {...props}
    />
  ),
  percentage_wrapped_large: props => (
    <PercentageStyle colorVariation="white" variation="type-promo" {...props} />
  ),
  percentage_wrapped_large_black: props => (
    <PercentageStyle colorVariation="black" variation="type-promo" {...props} />
  ),
  percentage_all_wrapped_normal_tab: props => (
    <PercentageStyle colorVariation="black" variation={TYPE_PROM_TAB} {...props} />
  ),
  percentage_wrapped_extra_large: props => <PercentagePinkStyle {...props} />,
  currency_up_style: props => <CurrencyUpPromoBanner {...props} />,
  // TODO: Remove .style10 when currency_up_style is available in CMS
  style10: props => <CurrencyUpPromoBanner {...props} />,
  small_text_bold: props => (
    <BodyCopy
      fontSize="fs14"
      fontWeight="black"
      color="black"
      fontFamily="primary"
      textAlign="center"
      lineHeight="16px"
      {...props}
    />
  ),
  small_text_normal: props => (
    <BodyCopy
      fontSize="fs14"
      color="gray.900"
      fontFamily="primary"
      textAlign="left"
      lineHeight="16px"
      {...props}
    />
  ),
  ribbon_default_text: props => (
    <BodyCopy
      fontSize="fs14"
      color="white"
      fontFamily="secondary"
      textAlign="center"
      lineHeight="14px"
      {...props}
    />
  ),
  gymboree_description: props => (
    <BodyCopy
      fontSize="fs16"
      fontWeight="regular"
      color="white"
      fontFamily="primary"
      textAlign="center"
      {...props}
    />
  ),
  small_text_black: props => (
    <BodyCopy
      color="text.primary"
      fontFamily="primary"
      fontSize="fs20"
      textAlign="center"
      lineHeight="22px"
      letterSpacing="ls1"
      fontWeight="medium"
      {...props}
    />
  ),
  small_text_semibold: props => (
    <BodyCopy
      color="text.primary"
      fontFamily="primary"
      fontSize="fs20"
      fontWeight="semibold"
      textAlign="center"
      lineHeight="20px"
      letterSpacing="ls2"
      {...props}
    />
  ),
  small_white_text_semibold: props => (
    <BodyCopy
      color="white"
      fontFamily="primary"
      fontSize="fs20"
      fontWeight="semibold"
      textAlign="center"
      lineHeight="20px"
      letterSpacing="ls2"
      {...props}
    />
  ),
  medium_text_semibold: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs32"
      textAlign="center"
      lineHeight="47px"
      letterSpacing="ls2"
      {...props}
    />
  ),
  extra_large_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs48"
      textAlign="center"
      lineHeight="47px"
      fontWeight="black"
      {...props}
    />
  ),
  extra_large_white_text_black: props => (
    <BodyCopy
      color="white"
      fontFamily="primary"
      fontSize="fs48"
      textAlign="center"
      fontWeight="black"
      {...props}
    />
  ),
  extra_large_text_regular: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs48"
      textAlign="center"
      lineHeight="47px"
      {...props}
    />
  ),
  large_text_bold: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs20"
      fontWeight="regular"
      textAlign="center"
      letterSpacing="ls2"
      {...props}
    />
  ),
  fixed_medium_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs64"
      textAlign="center"
      lineHeight="64px"
      fontWeight="black"
      {...props}
    />
  ),
  medium_text_regular: props => <MediumTextRegular {...props} />,
  medium_text_regular_tab: props => <MediumTextRegular {...props} />,
  extrabold_text_regular: props => (
    <BodyCopy
      fontSize="fs42"
      color="gray.900"
      fontFamily="primary"
      fontWeight="black"
      textAlign="center"
      {...props}
    />
  ),

  percentage_all_wrapped_normal: props => <PercentageAllWrappedNormal {...props} />,
  extrabold_text_regular_secondary: props => (
    <BodyCopy
      fontSize="fs48"
      color="white"
      fontFamily="secondary"
      fontWeight="black"
      textAlign="center"
      {...props}
    />
  ),
  mod_k_subheader: props => (
    <BodyCopy
      fontFamily="secondary"
      fontSize="fs22"
      textAlign="center"
      lineHeight="30px"
      fontWeight="regular"
      {...props}
    />
  ),
  medium_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs20"
      textAlign="center"
      lineHeight="20px"
      fontWeight="medium"
      letterSpacing="ls2"
      {...props}
    />
  ),

  tcp_homepage_medium_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="quaternary"
      fontSize="fs20"
      textAlign="center"
      lineHeight="20px"
      fontWeight="medium"
      {...props}
    />
  ),

  spaced_text_only_mobile: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontWeight="medium"
      fontSize="fs20"
      textAlign="center"
      letterSpacing="ls2"
      {...props}
    />
  ),
  percentage_inline_promo_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontWeight="black"
      fontSize="fs48"
      textAlign="center"
      {...props}
    />
  ),

  spaced_text_regular_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontWeight="medium"
      fontSize="fs20"
      textAlign="center"
      {...props}
    />
  ),
  large_text_black: props => (
    <BodyCopy
      color="gray.900"
      fontFamily="primary"
      fontSize="fs48"
      textAlign="center"
      fontWeight="black"
      {...props}
    />
  ),
  percentage_inline_promo: props => (
    <BodyCopy
      color="white"
      fontFamily="primary"
      fontWeight="black"
      fontSize="fs48"
      textAlign="center"
      {...props}
    />
  ),
  tcp_homepage_small_text_black: props => {
    return (
      <BodyCopy
        color="gray.600"
        fontFamily="primary"
        fontWeight="black"
        fontSize="fs20"
        textAlign="center"
        {...props}
      />
    );
  },
  tcp_homepage_extra_large_text_regular: props => {
    const { text, style } = props;

    const bodyCopyStyle = {
      paddingTop: 20,
      fontSize: 130,
      textShadowColor: 'rgba(0, 0, 0, 0.5)',
      textShadowOffset: { width: 0, height: 3 },
      textShadowRadius: 6,
      elevation: 4,
      ...style,
    };

    /**
     * This could be more simple by using only single BodyCopy
     * if we decide to get the "60 % OFF" as two textItems.
     * For instance, "60%" and "OFF"
     */
    return (
      <BodyCopy
        color="gray.600"
        fontWeight="bold"
        fontFamily="tertiary"
        textAlign="center"
        lineHeight="115px"
        style={bodyCopyStyle}
        text={text}
      />
    );
  },
  tcp_homepage_small_text_normal: props => (
    <BodyCopy
      fontSize="fs12"
      color="text.primary"
      fontFamily="quaternary"
      textAlign="center"
      lineHeight="17px"
      {...props}
    />
  ),
  tcp_homepage_percentage_all_wrapped_normal_tab: props => {
    const { text, style } = props;

    const strArray = text && text.split(' ');
    const bodyCopyStyleTab = { height: 30, ...style };
    const bodyCopyStyle1Tab = { height: 54, marginTop: 8, fontSize: 62, ...style };

    return (
      <Container>
        <BodyCopy
          fontWeight="black"
          color="text.primary"
          fontFamily="quaternary"
          textAlign="center"
          lineHeight="64px"
          style={bodyCopyStyle1Tab}
          text={strArray && strArray[0]}
        />
        <ContainerView>
          <BodyCopy
            fontSize="fs36"
            fontWeight="black"
            color="text.primary"
            fontFamily="quaternary"
            text={strArray && strArray[1]}
            lineHeight="38px"
            style={bodyCopyStyleTab}
          />
          <BodyCopy
            fontSize="fs18"
            fontWeight="black"
            color="text.primary"
            fontFamily="quaternary"
            textAlign="center"
            lineHeight="20px"
            text={strArray && strArray[2]}
            style={{ ...style }}
          />
        </ContainerView>
      </Container>
    );
  },
  tcp_homepage_medium_text_regular_tab: props => {
    return (
      <BodyCopy
        fontSize="fs18"
        color="text.primary"
        fontFamily="secondary"
        textAlign="center"
        lineHeight="20px"
        {...props}
      />
    );
  },
  tcp_homepage_fixed_medium_text_black: props => {
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="quaternary"
        fontSize="fs48"
        textAlign="center"
        lineHeight="50px"
        fontWeight="black"
        {...props}
      />
    );
  },
  tcp_homepage_medium_text_regular: props => {
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="quaternary"
        fontSize="fs20"
        fontWeight="medium"
        textAlign="center"
        {...props}
      />
    );
  },
  tcp_homepage_mod_b_small_text_black: props => {
    const { style } = props;
    const marginStyle = {
      marginLeft: 20,
      marginRight: 20,
    };
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="quaternary"
        fontSize="fs20"
        fontWeight="black"
        textAlign="center"
        lineHeight="20px"
        {...props}
        style={{ ...style, ...marginStyle }}
      />
    );
  },

  tcp_homepage_mod_b_style10: props => {
    const fontFamily = 'quaternary';
    const fontWeight = 'black';
    return (
      <CurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: { fontSize: 28, lineHeight: 30, marginRight: 3 },
        }}
        text2Override={{ fontFamily, style: { fontSize: 86, lineHeight: 88 } }}
        text3Override={{
          fontFamily,
          fontWeight,
          style: { fontSize: 48, lineHeight: 50, height: 40, marginLeft: 6, marginTop: 0 },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: { fontSize: 16, lineHeight: 18, marginLeft: 3, marginTop: 5 },
        }}
        flexDirectionDefaultView={{ alignLeft: true }}
        {...props}
      />
    );
  },
  tcp_homepage_mod_a_style10: props => {
    const shadowStyle = {
      textShadowColor: 'rgba(0, 0, 0, 0.5)',
      textShadowOffset: { width: 0, height: 3 },
      textShadowRadius: 6,
      elevation: 4,
    };
    const commonStyle = {
      ...shadowStyle,
      color: 'white',
    };
    const commonFonts = {
      fontWeight: 'bold',
      fontFamily: 'tertiary',
    };

    return (
      <CurrencyUpPromoBanner
        text1Override={{
          ...commonFonts,
          style: { fontSize: 66, lineHeight: 70, ...commonStyle },
        }}
        text2Override={{
          ...commonFonts,
          style: { fontSize: 140, lineHeight: 140, ...commonStyle },
        }}
        text3Override={{
          ...commonFonts,
          style: {
            fontSize: 66,
            height: 65,
            lineHeight: 69,
            textAlign: 'left',
            ...commonStyle,
          },
        }}
        text4Override={{
          ...commonFonts,
          style: { fontSize: 43, lineHeight: 45, ...commonStyle },
        }}
        {...props}
      />
    );
  },
  tcp_homepage_mod_b_alt_small_text_semibold: props => {
    return (
      <BodyCopy
        color="text.primary"
        fontFamily="quaternary"
        fontSize="fs20"
        fontWeight="medium"
        textAlign="center"
        lineHeight="22px"
        {...props}
      />
    );
  },
  tcp_homepage_mod_b_alt_style10: props => {
    const fontFamily = 'quaternary';
    return (
      <CurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          style: { fontSize: 28, lineHeight: 30, marginRight: 0 },
        }}
        text2Override={{
          fontFamily,
          style: { fontSize: 62, lineHeight: 64, height: 50, marginLeft: 0 },
        }}
        text3Override={{
          fontFamily,
          style: { fontSize: 28, lineHeight: 30, height: 25, marginLeft: 4 },
        }}
        text4Override={{
          fontFamily,
          style: { fontSize: 16, lineHeight: 18, marginLeft: 4 },
        }}
        flexDirectionDefaultView={{ alignLeft: true }}
        {...props}
      />
    );
  },
  tcp_homepage_small_white_text_semibold: props => (
    <BodyCopy
      color="white"
      fontFamily="quaternary"
      fontSize="fs20"
      fontWeight="medium"
      textAlign="center"
      lineHeight="24px"
      {...props}
    />
  ),
  tcp_homepage_extra_large_white_text_black: props => {
    const { style } = props;
    return (
      <BodyCopy
        color="white"
        fontFamily="quaternary"
        fontSize="fs62"
        lineHeight="66px"
        textAlign="center"
        fontWeight="black"
        {...props}
        style={{ height: 55, marginTop: 10, ...style }}
      />
    );
  },
  tcp_homepage_percentage_wrapped_large: props => {
    const { text, style } = props;

    const strArray = text && text.split(' ');
    const bodyCopyStyle = { height: 32, marginTop: -5 };
    const bodyCopyStylePercent = { height: 18 };
    const bodyCopyStyle1 = { height: 58, marginTop: 8, fontSize: 62 };

    return (
      <Container>
        <BodyCopy
          fontWeight="black"
          color="white"
          fontFamily="quaternary"
          textAlign="center"
          lineHeight="64px"
          style={{ ...bodyCopyStyle1, ...style }}
          text={strArray && strArray[0]}
        />
        <ContainerView>
          <BodyCopy
            fontSize="fs36"
            fontWeight="black"
            color="white"
            fontFamily="quaternary"
            text={strArray && strArray[1]}
            lineHeight="38px"
            style={{ ...bodyCopyStyle, ...style }}
          />
          <BodyCopy
            fontSize="fs18"
            fontWeight="black"
            color="white"
            fontFamily="quaternary"
            textAlign="center"
            lineHeight="22px"
            style={{ ...bodyCopyStylePercent, ...style }}
            text={strArray && strArray[2]}
          />
        </ContainerView>
      </Container>
    );
  },
  tcp_homepage_small_text_white_medium: props => {
    const { text, style } = props;
    return (
      <BodyCopy
        color="white"
        fontFamily="secondary"
        fontSize="fs18"
        lineHeight="20px"
        fontWeight="bold"
        textAlign="center"
        text={text}
        style={style}
      />
    );
  },

  ...PricePointsStyles,
};

// =====================================================================
// TODO: Need to remove this patch when we add styles to CMS drop down
// =====================================================================
Object.keys(bodyCopyStyles).forEach(styleKey => {
  const modBAltRegExp = /tcp_homepage_mod_b_alt/;
  const modBRegExp = /tcp_homepage_mod_b/;
  const tcpHomeRegExp = /tcp_homepage_/;

  if (modBAltRegExp.test(styleKey)) {
    const modBAltTcpKey = styleKey.replace(modBAltRegExp, 'tcp_homepage');
    bodyCopyStyles[modBAltTcpKey] = bodyCopyStyles[modBAltTcpKey] || bodyCopyStyles[styleKey];
  }

  if (tcpHomeRegExp.test(styleKey) && !modBRegExp.test(styleKey)) {
    const tcpHomeModBKey = styleKey.replace(tcpHomeRegExp, 'tcp_homepage_mod_b_');
    const tcpHomeModBAltKey = styleKey.replace(tcpHomeRegExp, 'tcp_homepage_mod_b_alt_');
    bodyCopyStyles[tcpHomeModBKey] = bodyCopyStyles[tcpHomeModBKey] || bodyCopyStyles[styleKey];
    bodyCopyStyles[tcpHomeModBAltKey] =
      bodyCopyStyles[tcpHomeModBAltKey] || bodyCopyStyles[styleKey];
  }
});
// ======================================================================

/**
 * This component produces a Promo Text banner
 * Expects textItems array consisting of objects in below format
 * {
 *    style: "",
 *    text: ""
 * }
 * This component accepts BodyCopy styles in array return matching BodyCopy style with
 * the key provided by CMS
 * @param {*} props
 */
// eslint-disable-next-line sonarjs/cognitive-complexity
const PromoBanner = props => {
  const {
    locator,
    navigation,
    promoBanner: [{ textItems, link }],
    bannerPosition,
    promoStyle = {},
    showTextUnderLine,
    underLineStyle,
    renderInSameLine,
    isContainerModuleText,
    ...otherProps
  } = props;

  const accessibilityLabel = textItems ? textItems.map(({ text }) => text).join(', ') : '';
  const pricePointStylePrefix = 'price_point_style_';
  const getPaddingStyle = (style, index) => {
    const isPricePointStyle = style && style.includes(pricePointStylePrefix);
    return !isPricePointStyle && renderInSameLine && index !== textItems.length - 1
      ? { paddingRight: 10 }
      : {};
  };

  const renderTextItems = () => {
    return (
      textItems &&
      textItems.map(({ text, style, styledObj }, index) => {
        const parsedStyledObj = styledObj && safelyParseJSON(styledObj);
        const promoStyleObj = Array.isArray(promoStyle) ? promoStyle[index] : { ...promoStyle };
        // In PromoBanner upcoming style not match from listed style then show the default style .
        const strStyle =
          style && style.includes(pricePointStylePrefix)
            ? `${pricePointStylePrefix}${style.split(pricePointStylePrefix)[1]}`
            : style;
        const StyleBodyCopy = bodyCopyStyles[strStyle] ? bodyCopyStyles[strStyle] : BodyCopy;
        const paddingStyle = getPaddingStyle(style, index);
        const wrapStyle = isContainerModuleText ? { flexWrap: 'wrap', flexShrink: 1 } : null;
        return StyleBodyCopy ? (
          <StyleBodyCopy
            style={{ ...promoStyleObj, ...paddingStyle, ...wrapStyle, ...parsedStyledObj }}
            text={index ? `${text}` : text}
            locator={locator}
            {...otherProps}
            key={index.toString()}
            styleClass={strStyle}
            bannerPosition={bannerPosition}
            renderInSameLine={renderInSameLine}
            isLastTextItem={index === textItems.length - 1}
            isContainerModuleText={isContainerModuleText}
          />
        ) : null;
      })
    );
  };
  return [
    <ContainerView>
      {textItems && (
        <Anchor
          accessibilityLabel={accessibilityLabel}
          url={link ? link.url : ''}
          navigation={navigation}
        >
          {isContainerModuleText ? (
            <StyledTextView renderInSameLine={renderInSameLine}>{renderTextItems()}</StyledTextView>
          ) : (
            renderTextItems()
          )}

          {showTextUnderLine && <View style={underLineStyle} />}
        </Anchor>
      )}
    </ContainerView>,
  ];
};

/**
 * This function return the Promobanner Percentage Style
 * Color is 'Black' and Split by the space ' ' key. Font size is also small.
 */
const PercentageAllWrappedNormal = props => {
  const { text, style } = props;

  const strArray = text && text.split(' ');
  const containerStyle = { marginTop: 5 };
  const bodyCopyStyle = { height: 24, width: 28, marginTop: 0, ...style };
  const bodyCopyStyle1 = { height: 38, marginTop: 0, ...style };
  const bodyCopyStyle2 = { height: 20, ...style };

  return (
    <Container style={containerStyle}>
      <BodyCopy
        fontSize="fs48"
        fontWeight="black"
        color="text.primary"
        fontFamily="primary"
        textAlign="center"
        lineHeight="48px"
        style={bodyCopyStyle1}
        text={strArray && strArray[0]}
      />
      <ContainerView>
        <BodyCopy
          fontSize="fs28"
          fontWeight="black"
          color="text.primary"
          fontFamily="primary"
          lineHeight="28px"
          text={strArray && strArray[1]}
          style={bodyCopyStyle}
        />
        <BodyCopy
          fontSize="fs18"
          fontWeight="black"
          color="text.primary"
          fontFamily="primary"
          textAlign="center"
          lineHeight="18px"
          text={strArray && strArray[2]}
          style={bodyCopyStyle2}
        />
      </ContainerView>
    </Container>
  );
};

PercentageAllWrappedNormal.propTypes = {
  text: PropTypes.string.isRequired,
  style: PropTypes.shape({}),
};

PercentageAllWrappedNormal.defaultProps = {
  style: {},
};

/**
 * This function return the Promobanner Percentage Style
 * Color is 'White' and Split by the space ' ' key .
 */

const PercentageStyle = props => {
  const { text, colorVariation, variation, propOverride, style } = props;

  const strArray = text && text.split(' ');
  const bodyCopyStyle = { height: 34 };
  const bodyCopyStyleTab = { height: 32 };
  const bodyCopyStyle1 = { height: 58, marginTop: 8, fontSize: 64 };
  const bodyCopyStyle1Tab = { height: 54, marginTop: 8, fontSize: 62 };

  return (
    <Container>
      <BodyCopy
        fontWeight="black"
        color={colorVariation}
        fontFamily="primary"
        textAlign="center"
        lineHeight="64px"
        style={{ ...(variation === TYPE_PROM_TAB ? bodyCopyStyle1Tab : bodyCopyStyle1), ...style }}
        text={strArray && strArray[0]}
        {...propOverride}
      />
      <ContainerView>
        <BodyCopy
          fontSize={variation === TYPE_PROM_TAB ? 'fs36' : 'fs42'}
          fontWeight="black"
          color={colorVariation}
          fontFamily="primary"
          text={strArray && strArray[1]}
          lineHeight={variation === TYPE_PROM_TAB ? '38px' : '44px'}
          style={{ ...(variation === TYPE_PROM_TAB ? bodyCopyStyleTab : bodyCopyStyle), ...style }}
          {...propOverride}
        />
        <BodyCopy
          fontSize="fs18"
          fontWeight="black"
          color={colorVariation}
          fontFamily="primary"
          textAlign="center"
          lineHeight="20px"
          text={strArray && strArray[2]}
          {...propOverride}
          style={style}
        />
      </ContainerView>
    </Container>
  );
};

PercentageStyle.propTypes = {
  text: PropTypes.string.isRequired,
  colorVariation: PropTypes.string.isRequired,
  variation: PropTypes.string.isRequired,
  style: PropTypes.shape({}),
};

PercentageStyle.defaultProps = {
  style: {},
};
/**
 * This function return the Promobanner Percentage Style
 * Color is 'Pink' and Split by the space ' ' key .
 */
const PercentagePinkStyle = props => {
  const { text, style } = props;

  const strArray = text && text.split(' ');
  const bodyCopyStyle = { height: 85, fontSize: 98, color: '#f797d6', ...style };
  const bodyCopyStyle1 = { height: 131, marginTop: 8, fontSize: 154, color: '#f791cf', ...style };
  const bodyCopyStyle2 = { height: 42, color: '#f791cf', ...style };

  return (
    <Container>
      <BodyCopy
        fontWeight="black"
        fontFamily="primary"
        textAlign="center"
        lineHeight="155px"
        style={bodyCopyStyle1}
        text={strArray && strArray[0]}
      />
      <ContainerView>
        <BodyCopy
          fontWeight="black"
          fontFamily="primary"
          text={strArray && strArray[1]}
          lineHeight="99px"
          style={bodyCopyStyle}
        />
        <BodyCopy
          fontSize="fs42"
          fontWeight="black"
          fontFamily="primary"
          textAlign="center"
          lineHeight="42px"
          text={strArray && strArray[2]}
          style={bodyCopyStyle2}
        />
      </ContainerView>
    </Container>
  );
};

PercentagePinkStyle.propTypes = {
  text: PropTypes.string.isRequired,
  style: PropTypes.shape({}),
};

PercentagePinkStyle.defaultProps = {
  style: {},
};

/**
 * This function return the Promobanner Style
 * Style contains regular primary font
 */
const MediumTextRegular = props => {
  return (
    <BodyCopy
      fontSize="fs20"
      color="gray.900"
      fontFamily="primary"
      fontWeight="semibold"
      textAlign="center"
      letterSpacing="ls2"
      lineHeight="20px"
      {...props}
    />
  );
};

PromoBanner.propTypes = {
  ribbonBanner: PropTypes.arrayOf(PropTypes.object).isRequired,
  promoBanner: PropTypes.arrayOf(PropTypes.object).isRequired,
  bodyCopyStyles: PropTypes.arrayOf(PropTypes.object).isRequired,
  showTextUnderLine: PropTypes.string,
  underLineStyle: PropTypes.shape({}),
};

PromoBanner.defaultProps = {
  showTextUnderLine: false,
  underLineStyle: {},
};

export default PromoBanner;
export { PromoBanner as PromoBannerVanilla };
