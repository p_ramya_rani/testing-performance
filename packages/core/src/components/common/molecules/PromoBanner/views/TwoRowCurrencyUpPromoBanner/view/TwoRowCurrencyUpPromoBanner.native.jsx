// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes } from 'prop-types';
import {
  BodyCopy,
  TopAlignedView,
  FlexDirectionBottomView,
  FlexDirectionLastColView,
} from '../TwoRowCurrencyUpPromoBanner.style.native';
import { getPricePointRowText } from '../../../PricePoints.utils';

const renderTopRow = (style, strObj, text1Override, text2Override, text3Override) => {
  const bodyCopyTextStyle1 = { fontSize: 28, ...style };
  const bodyCopyTextStyle2 = { fontSize: 86, ...style };
  const bodyCopyTextStyle3 = { fontSize: 48, height: 45, textAlign: 'left', ...style };
  return (
    <>
      {strObj && strObj.part1 && (
        <BodyCopy
          fontWeight="black"
          color="black"
          fontFamily="primary"
          textAlign="center"
          lineHeight="34px"
          style={bodyCopyTextStyle1}
          text={strObj.part1}
          {...text1Override}
        />
      )}
      {strObj && strObj.part2 && (
        <BodyCopy
          fontWeight="black"
          color="black"
          fontFamily="primary"
          textAlign="center"
          lineHeight="88px"
          style={bodyCopyTextStyle2}
          text={strObj.part2}
          {...text2Override}
        />
      )}
      {strObj && strObj.part3 && (
        <BodyCopy
          fontWeight="black"
          color="black"
          fontFamily="primary"
          lineHeight="50px"
          text={strObj.part3}
          style={bodyCopyTextStyle3}
          {...text3Override}
        />
      )}
    </>
  );
};
const renderBottomRow = (
  style,
  strObj,
  text4Override,
  text5Override,
  text6Override,
  flexDirectionLastColView
) => {
  const bodyCopyTextStyle4 = { fontSize: 28, ...style };
  const bodyCopyTextStyle5 = { fontSize: 48, height: 45, textAlign: 'left', ...style };
  const bodyCopyTextStyle6 = { fontSize: 28, ...style };
  return (
    <>
      {strObj && strObj.part4 && (
        <BodyCopy
          fontWeight="black"
          color="black"
          fontFamily="primary"
          textAlign="center"
          text={strObj.part4}
          style={bodyCopyTextStyle4}
          {...text4Override}
        />
      )}
      <FlexDirectionLastColView {...flexDirectionLastColView}>
        {strObj && strObj.part5 && (
          <BodyCopy
            fontWeight="black"
            color="black"
            fontFamily="primary"
            lineHeight="50px"
            text={strObj.part5}
            style={bodyCopyTextStyle5}
            {...text5Override}
          />
        )}
        {strObj && strObj.part6 && (
          <BodyCopy
            fontSize="fs42"
            fontWeight="black"
            color="black"
            fontFamily="primary"
            textAlign="center"
            lineHeight="28px"
            text={strObj.part6}
            style={bodyCopyTextStyle6}
            {...text6Override}
          />
        )}
      </FlexDirectionLastColView>
    </>
  );
};
/**
 * This component produces a Currency up Promo Text banner
 * This component accepts text item from CMS
 *
 * It first splits the input text with spaces and then displays differents items in different styles
 * There are 4 items expected in the array - e.g $ 7 80 &up - All are displayed in different styles
 *
 * @param {*} props
 */
const TwoRowCurrencyUpPromoBanner = props => {
  const {
    text,
    style,
    styleClass,
    text1Override,
    text2Override,
    text3Override,
    text4Override,
    text5Override,
    text6Override,
    flexDirectionBottomView,
    flexDirectionLastColView,
    topAlignedView,
    renderInSameLine,
    isContainerModuleText,
    isLastTextItem,
  } = props;

  const strObj = getPricePointRowText(styleClass, text);

  return (
    <TopAlignedView
      {...topAlignedView}
      renderInSameLine={renderInSameLine}
      isContainerModuleText={isContainerModuleText}
      isLastTextItem={isLastTextItem}
    >
      {renderTopRow(style, strObj, text1Override, text2Override, text3Override)}
      <FlexDirectionBottomView {...flexDirectionBottomView}>
        {renderBottomRow(
          style,
          strObj,
          text4Override,
          text5Override,
          text6Override,
          flexDirectionLastColView
        )}
      </FlexDirectionBottomView>
    </TopAlignedView>
  );
};

TwoRowCurrencyUpPromoBanner.propTypes = {
  text: PropTypes.string,
  text1Override: PropTypes.shape({}),
  text2Override: PropTypes.shape({}),
  text3Override: PropTypes.shape({}),
  text4Override: PropTypes.shape({}),
  text5Override: PropTypes.shape({}),
  text6Override: PropTypes.shape({}),
  flexDirectionDefaultView: PropTypes.shape({}),
  style: PropTypes.shape({}),
  flexDirectionBottomView: PropTypes.shape({}),
  flexDirectionLastColView: PropTypes.shape({}),
  topAlignedView: PropTypes.shape({}),
  styleClass: PropTypes.string,
  renderInSameLine: PropTypes.bool,
  isContainerModuleText: PropTypes.bool,
  isLastTextItem: PropTypes.bool,
};

TwoRowCurrencyUpPromoBanner.defaultProps = {
  text: '',
  text1Override: {},
  text2Override: {},
  text3Override: {},
  text4Override: {},
  text5Override: {},
  text6Override: {},
  flexDirectionDefaultView: {},
  style: {},
  flexDirectionBottomView: {},
  flexDirectionLastColView: {},
  topAlignedView: {},
  styleClass: '',
  renderInSameLine: false,
  isContainerModuleText: false,
  isLastTextItem: false,
};

export default TwoRowCurrencyUpPromoBanner;
