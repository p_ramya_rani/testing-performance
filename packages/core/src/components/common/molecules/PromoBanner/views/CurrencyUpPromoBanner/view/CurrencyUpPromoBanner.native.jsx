// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes } from 'prop-types';
import {
  BodyCopy,
  FlexDirectionDefaultView,
  TopAlignedView,
} from '../CurrencyUpPromoBanner.style.native';

/**
 * This component produces a Currency up Promo Text banner
 * This component accepts text item from CMS
 *
 * It first splits the input text with spaces and then displays differents items in different styles
 * There are 4 items expected in the array - e.g $ 7 80 &up - All are displayed in different styles
 *
 * @param {*} props
 */
const CurrencyUpPromoBanner = props => {
  const {
    text,
    style,
    text1Override,
    text2Override,
    text3Override,
    text4Override,
    flexDirectionDefaultView,
  } = props;

  const strArray = text && text.split(' ');
  const bodyCopyTextStyle1 = { fontSize: 28, ...style };
  const bodyCopyTextStyle2 = { fontSize: 86, ...style };
  const bodyCopyTextStyle3 = { fontSize: 48, height: 45, textAlign: 'left', ...style };
  const bodyCopyTextStyle4 = { fontSize: 28, ...style };

  return (
    <TopAlignedView>
      <BodyCopy
        fontWeight="black"
        color="black"
        fontFamily="primary"
        textAlign="center"
        lineHeight="34px"
        style={bodyCopyTextStyle1}
        text={strArray && strArray[0]}
        {...text1Override}
      />
      <BodyCopy
        fontWeight="black"
        color="black"
        fontFamily="primary"
        textAlign="center"
        lineHeight="88px"
        style={bodyCopyTextStyle2}
        text={strArray && strArray[1]}
        {...text2Override}
      />
      <FlexDirectionDefaultView {...flexDirectionDefaultView}>
        <BodyCopy
          fontWeight="black"
          color="black"
          fontFamily="primary"
          lineHeight="50px"
          text={strArray && strArray[2]}
          style={bodyCopyTextStyle3}
          {...text3Override}
        />
        <BodyCopy
          fontSize="fs42"
          fontWeight="black"
          color="black"
          fontFamily="primary"
          textAlign="center"
          lineHeight="30px"
          text={strArray && `${strArray[3]} ${strArray[4]}`}
          style={bodyCopyTextStyle4}
          {...text4Override}
        />
      </FlexDirectionDefaultView>
    </TopAlignedView>
  );
};

CurrencyUpPromoBanner.propTypes = {
  text: PropTypes.string,
  text1Override: PropTypes.shape({}),
  text2Override: PropTypes.shape({}),
  text3Override: PropTypes.shape({}),
  text4Override: PropTypes.shape({}),
  flexDirectionDefaultView: PropTypes.shape({}),
  style: PropTypes.shape({}),
};

CurrencyUpPromoBanner.defaultProps = {
  text: '',
  text1Override: {},
  text2Override: {},
  text3Override: {},
  text4Override: {},
  flexDirectionDefaultView: {},
  style: {},
};

export default CurrencyUpPromoBanner;
