// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { Anchor, BodyCopy } from '../../../atoms';
import errorBoundary from '../../../hoc/withErrorBoundary';
import LinkText from '../../LinkText';
import withStyles from '../../../hoc/withStyles';
import PromoBannerStyle, { StyledPromoDiv, StyledPromoSpan } from '../PromoBanner.style';
import { configureInternalNavigationFromCMSUrl } from '../../../../../utils';
import { promoAnalyticsValue } from '../../../../../constants/analytics';
import { getPricePointColText, getPricePointRowText } from '../PricePoints.utils';

/**
 * Currency & Up variation of Promo Banner
 * @param {*} props
 */
const renderCurrencyUpVariation = (style, text, promoStyle, styledObjWeb) => {
  const textItems = text.split(' ');

  return (
    <BodyCopy component="div" className={`promo-text ${style}`} fontFamily="primary">
      <StyledPromoDiv style={promoStyle} className="col-1" customStyle={styledObjWeb}>
        <span className={`${style}-0`}>{textItems[0]}</span>
        <span className={`${style}-1`}>{textItems[1]}</span>
      </StyledPromoDiv>
      <StyledPromoDiv style={promoStyle} className="col-2" customStyle={styledObjWeb}>
        <span className={`${style}-2`}>{textItems[2]}</span>
        <span className={`${style}-3`}>{`${textItems[3]}${textItems[4]}`}</span>
      </StyledPromoDiv>
    </BodyCopy>
  );
};

/**
 * Three columns Currency & Up variation of Promo Banner
 * @param {*} props
 */
const renderThreeColCurrencyUpVariation = (style, text, promoStyle, styledObjWeb) => {
  const { part1, part2, part3, part4, part5 } = getPricePointColText(style, text);

  return (
    <BodyCopy
      component="div"
      className={`promo-text price-point price-point-columns ${style}`}
      fontFamily="primary"
    >
      {(part1 || part2) && (
        <StyledPromoDiv
          style={promoStyle}
          styleName={style}
          className="col-1"
          customStyle={styledObjWeb}
        >
          {part1 && <span className={`${style}-0`}>{`${part1}`}</span>}
          {part2 && <span className={`${style}-1`}>{`${part2}`}</span>}
        </StyledPromoDiv>
      )}
      <StyledPromoDiv
        style={promoStyle}
        className="col-2"
        styleName={style}
        customStyle={styledObjWeb}
      >
        <span className={`${style}-2`}>{part3}</span>
      </StyledPromoDiv>
      {(part4 || part5) && (
        <StyledPromoDiv
          style={promoStyle}
          styleName={style}
          className="col-3"
          customStyle={styledObjWeb}
        >
          {part4 && <span className={`${style}-3`}>{`${part4}`}</span>}
          {part5 && <span className={`${style}-4`}>{`${part5}`}</span>}
        </StyledPromoDiv>
      )}
    </BodyCopy>
  );
};
/**
 * First row PricePoint of Promo Banner
 * @param {*} props
 */
const getPricePointRow1 = (style, text, promoStyle, part1, part2, part3, styledObjWeb) => {
  return (
    <StyledPromoDiv
      style={promoStyle}
      styleName={style}
      className="row-1"
      customStyle={styledObjWeb}
    >
      {part1 && <span className={`${style}-0`}>{`${part1}`}</span>}
      {part2 && <span className={`${style}-1`}>{`${part2}`}</span>}
      {part3 && <span className={`${style}-2`}>{`${part3}`}</span>}
    </StyledPromoDiv>
  );
};
/**
 * second row  PricePoint of Promo Banner
 * @param {*} props
 */
const getPricePointRow2 = (style, text, promoStyle, part4, part5, part6, styledObjWeb) => {
  return (
    <>
      {style === 'price_point_style_15' || style === 'price_point_style_16' ? (
        <StyledPromoDiv
          style={promoStyle}
          styleName={style}
          className="row-2"
          customStyle={styledObjWeb}
        >
          <div style={promoStyle} className="col-1">
            {part4 && <span className={`${style}-3`}>{`${part4}`}</span>}
          </div>
          <div style={promoStyle} className="col-2">
            {part5 && <span className={`${style}-4`}>{`${part5}`}</span>}
            {part6 && <span className={`${style}-5`}>{`${part6}`}</span>}
          </div>
        </StyledPromoDiv>
      ) : (
        <StyledPromoDiv
          style={promoStyle}
          styleName={style}
          className="row-2"
          customStyle={styledObjWeb}
        >
          {part4 && <span className={`${style}-3`}>{`${part4}`}</span>}
          {part5 && <span className={`${style}-4`}>{`${part5}`}</span>}
          {part6 && <span className={`${style}-5`}>{`${part6}`}</span>}
        </StyledPromoDiv>
      )}
    </>
  );
};

/**
 * Currency & Up variation of Promo Banner
 * @param {*} props
 */
const renderTwoRowCurrencyUpVariation = (style, text, promoStyle, styledObjWeb) => {
  const { part1, part2, part3, part4, part5, part6 } = getPricePointRowText(style, text);

  return (
    <BodyCopy
      component="div"
      className={`promo-text price-point price-point-rows ${style}`}
      fontFamily="primary"
    >
      {(part1 || part2 || part3) &&
        getPricePointRow1(style, text, promoStyle, part1, part2, part3, styledObjWeb)}
      {(part4 || part5 || part6) &&
        getPricePointRow2(style, text, promoStyle, part4, part5, part6, styledObjWeb)}
    </BodyCopy>
  );
};

/**
 * Currency & Up variation of Promo Banner
 * @param {*} props
 */
const renderPricePointVariation = (style, text, promoStyle, styledObjWeb) => {
  const styleClassPrefix = 'price_point_style_';
  const columnStyleVariationNumbers = [
    `${styleClassPrefix}1`,
    `${styleClassPrefix}2`,
    `${styleClassPrefix}4`,
    `${styleClassPrefix}6`,
    `${styleClassPrefix}8`,
    `${styleClassPrefix}10`,
    `${styleClassPrefix}13`,
    `${styleClassPrefix}14`,
    `${styleClassPrefix}17`,
  ];
  const rowStyleVariationNumbers = [
    `${styleClassPrefix}3`,
    `${styleClassPrefix}5`,
    `${styleClassPrefix}7`,
    `${styleClassPrefix}12`,
    `${styleClassPrefix}15`,
    `${styleClassPrefix}16`,
  ];
  const isColVariation = columnStyleVariationNumbers.includes(style);
  const isRowVariation = rowStyleVariationNumbers.includes(style);

  if (isColVariation) {
    return renderThreeColCurrencyUpVariation(style, text, promoStyle, styledObjWeb);
  }
  if (isRowVariation) {
    return renderTwoRowCurrencyUpVariation(style, text, promoStyle, styledObjWeb);
  }
  return null;
};

const isPencentageTab = (style) => {
  return (
    style === 'percentage_wrapped_extra_large' ||
    style === 'percentage_wrapped_large' ||
    style === 'percentage_all_wrapped_normal' ||
    style === 'percentage_all_wrapped_normal_tab' ||
    style === 'percentage_wrapped_nav'
  );
};
/**
 * This component produces a Promo Text banner
 * Expects textItems array consisting of objects in below format
 * {
 *    style: "",
 *    text: ""
 * }
 * This component uses BodyCopy atom and accepts all properties of BodyCopy
 * @param {*} props
 */
const PromoBanner = (props) => {
  const {
    headerText,
    promoBanner: [{ textItems, link }],
    className,
    dataLocatorHeader,
    headerStyle,
    promoStyle = {},
    backgroundStyle,
    ...otherProps
  } = props;

  const navigationUrl = link || {};
  navigationUrl.to = link && configureInternalNavigationFromCMSUrl(link.url);
  navigationUrl.asPath = link && link.url;
  navigationUrl.target = link && link.target;
  navigationUrl.title = link?.title;

  return (
    <BodyCopy component="div" className={className} customStyle={backgroundStyle} {...otherProps}>
      <React.Fragment>
        {headerText && (
          <LinkText
            component="div"
            dataLocator={dataLocatorHeader}
            className="promo-banner-header"
            fontFamily="primary"
            headerText={headerText}
            headerStyle={headerStyle}
          />
        )}
        <ClickTracker
          as={Anchor}
          {...navigationUrl}
          className="promo-text-link"
          clickData={{
            customEvents: ['event80', 'event81'],
            internalCampaignId: promoAnalyticsValue,
          }}
        >
          {textItems.map(({ text, style, styledObjWeb = '' }, index) => {
            let promoText;
            /* this need to be fixed once we have 5 items for module A or unlimited textItems creation in CMS */
            if (isPencentageTab(style)) {
              const percentageTexts = text.split(' ');
              promoText = (
                <StyledPromoDiv
                  style={promoStyle}
                  className={`promo-text ${style}`}
                  customStyle={styledObjWeb}
                >
                  <div className={`${style}-0`}>
                    {percentageTexts[0] && percentageTexts[0].trim()}
                  </div>
                  <div className={`${style}__wrapped_text`}>
                    <div className={`${style}-1`}>{`${percentageTexts[1]} `}</div>
                    <div className={`${style}-2`}>
                      {percentageTexts[2] && percentageTexts[2].trim()}
                    </div>
                  </div>
                </StyledPromoDiv>
              );
            } else if (style === 'currency_up_style' || style === 'style10') {
              promoText = renderCurrencyUpVariation(style, text, promoStyle, styledObjWeb);
            } else if (style.includes('price_point_style_')) {
              promoText = renderPricePointVariation(style, text, promoStyle, styledObjWeb);
            } else {
              promoText = (
                <StyledPromoSpan
                  style={promoStyle}
                  className={`promo-text ${style}`}
                  customStyle={styledObjWeb}
                >
                  {index ? ` ${text}` : text}
                </StyledPromoSpan>
              );
            }

            return promoText;
          })}
        </ClickTracker>
      </React.Fragment>
    </BodyCopy>
  );
};

PromoBanner.propTypes = {
  promoBanner: PropTypes.arrayOf(
    PropTypes.oneOfType(
      PropTypes.shape({
        textItems: PropTypes.arrayOf(
          PropTypes.oneOfType(
            PropTypes.shape({
              style: PropTypes.string,
              text: PropTypes.string,
            })
          )
        ),
      })
    )
  ).isRequired,
  className: PropTypes.string.isRequired,
  dataLocatorHeader: PropTypes.string,
  headerText: PropTypes.arrayOf(
    PropTypes.oneOfType(
      PropTypes.shape({
        textItems: PropTypes.arrayOf(
          PropTypes.oneOfType(
            PropTypes.shape({
              style: PropTypes.string,
              text: PropTypes.string,
              styledObjWeb: PropTypes.string,
            })
          )
        ),
        link: PropTypes.shape({
          url: PropTypes.string,
          text: PropTypes.string,
          target: PropTypes.string,
          title: PropTypes.string,
        }),
      })
    )
  ),
  promoStyle: PropTypes.shape({}),
  headerStyle: PropTypes.arrayOf(PropTypes.shape({})),
  backgroundStyle: PropTypes.shape({}),
  isModBOverlay: PropTypes.bool,
};

PromoBanner.defaultProps = {
  headerText: '',
  dataLocatorHeader: '',
  promoStyle: {},
  headerStyle: [],
  backgroundStyle: {},
  isModBOverlay: false,
};

export { PromoBanner as PromoBannerVanilla };
export default withStyles(errorBoundary(PromoBanner), PromoBannerStyle);
