// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

import { BodyCopy } from '../../../../atoms';

const StyledBodyCopy = styled(BodyCopy)`
  ${({ lineHeight }) => (lineHeight ? `line-height: ${lineHeight}` : '')}
  text-align: center;
`;

export { StyledBodyCopy as BodyCopy };

export const FlexDirectionBottomView = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  margin-top: ${props => (props.isModBOverlay ? '-15px' : '-10px')};
`;
export const FlexDirectionLastColView = styled.View`
  ${props => (props.isColumnDirection ? 'flex-direction: column;' : 'flex-direction: row;')}
`;
export const TopAlignedView = styled.View`
  flex-direction: column;
  align-items: center;
  ${props => (props.renderInSameLine && !props.isLastTextItem ? `padding-right: 10px` : null)};
  ${props => (props.isContainerModuleText ? null : `width: 100%`)};
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
`;

export default {
  StyledBodyCopy: BodyCopy,
  FlexDirectionLastColView,
  FlexDirectionBottomView,
  TopAlignedView,
};
