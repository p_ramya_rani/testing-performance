/* eslint-disable react-native/no-inline-styles, max-lines, react/prop-types */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import ThreeColCurrencyUpPromoBanner from './ThreeColCurrencyUpPromoBanner';
import TwoRowCurrencyUpPromoBanner from './TwoRowCurrencyUpPromoBanner';

const fontFamily = 'quaternary';
const fontWeight = 'black';

let isModBOverlay = false;
const getModBOverlay = (bannerPosition) => {
  isModBOverlay = bannerPosition === 'overlay';
};
const fontSize112 = {
  fontSize: 112,
  lineHeight: 152,
};
const fontSize88 = {
  fontSize: 88,
  lineHeight: 120,
};
const fontSize62 = {
  fontSize: 62,
  lineHeight: 84,
};
const fontSize64 = {
  fontSize: 64,
  lineHeight: 87,
};
const fontSize48 = {
  fontSize: 48,
  lineHeight: 65,
};
const fontSize36 = {
  fontSize: 36,
  lineHeight: 49,
};
const fontSize32 = {
  fontSize: 32,
  lineHeight: 49,
};
const fontSize24 = {
  fontSize: 24,
  lineHeight: 33,
};
const fontSize28 = {
  fontSize: 28,
  lineHeight: 38,
};
const fontSize18 = {
  fontSize: 18,
  lineHeight: 24,
};
const fontSize16 = {
  fontSize: 16,
  lineHeight: 22,
};

const getFontSize = (fontSizeForModB, fontSizeForOtherMod) => {
  return isModBOverlay ? fontSizeForModB : fontSizeForOtherMod;
};
const getMarginTop = (marginTopForModB, marginTopForOtherMod) => {
  return {
    marginTop: isModBOverlay ? marginTopForModB : marginTopForOtherMod,
  };
};
const getMarginRight = (marginRightForModB, marginRightForOtherMod) => {
  return {
    marginRight: isModBOverlay ? marginRightForModB : marginRightForOtherMod,
  };
};
const getMarginLeft = (marginLeftForModB, marginLeftForOtherMod) => {
  return {
    marginRight: isModBOverlay ? marginLeftForModB : marginLeftForOtherMod,
  };
};

const PricePointsStyles = {
  price_point_style_1: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize36, fontSize24),
            marginRight: -10,
            ...getMarginRight(-8, -10),
            width: 40,
            ...getMarginTop(1, -7),
            ...style,
          },
        }}
        text2Override={{
          fontFamily,
          style: { ...getFontSize(fontSize112, fontSize62), ...getMarginTop(-27, -22), ...style },
        }}
        text3Override={{
          fontFamily,
          style: {
            ...getFontSize(fontSize48, fontSize36),
            ...getMarginTop(-4, -14),
            ...getMarginLeft(2, 4),
            ...style,
          },
        }}
        firstColDirectionView={{ isRowDirection: true }}
        {...props}
      />
    );
  },
  price_point_style_2: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize36, fontSize24),
            marginRight: -10,
            ...getMarginRight(-8, -10),
            width: 40,
            ...getMarginTop(1, -7),
            ...style,
          },
        }}
        text2Override={{
          fontFamily,
          style: { ...getFontSize(fontSize112, fontSize62), ...getMarginTop(-27, -22), ...style },
        }}
        text3Override={{
          fontFamily,
          style: {
            ...getFontSize(fontSize48, fontSize36),
            ...getMarginTop(-4, -14),
            ...getMarginLeft(2, 4),
            ...style,
          },
        }}
        text4Override={{
          fontFamily,
          style: { ...getFontSize(fontSize112, fontSize62), ...getMarginTop(-27, -22), ...style },
        }}
        text5Override={{
          fontFamily,
          style: {
            ...getFontSize(fontSize48, fontSize36),
            ...getMarginTop(-4, -14),
            ...getMarginLeft(2, 4),
            ...style,
          },
        }}
        text6Override={{
          fontFamily,
          style: {
            ...getFontSize(fontSize48, fontSize36),
            ...getMarginTop(-4, -14),
            ...getMarginLeft(2, 4),
            ...style,
          },
        }}
        firstColDirectionView={{ isRowDirection: true }}
        lastColDirectionView={{ isRowDirection: true }}
        {...props}
      />
    );
  },

  price_point_style_3: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <TwoRowCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: { ...getFontSize(fontSize28, fontSize16), marginTop: 0, ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: { ...getFontSize(fontSize48, fontSize36), marginLeft: 5, marginTop: 0, ...style },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize112, fontSize62),
            marginLeft: 2,
            ...getMarginTop(-23, -10),
            ...style,
          },
        }}
        text6Override={{
          fontFamily,
          fontWeight,
          style: { ...getFontSize(fontSize48, fontSize36), marginLeft: 5, marginTop: 0, ...style },
        }}
        flexDirectionBottomView={{ isModBOverlay }}
        {...props}
      />
    );
  },
  price_point_style_4: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize36, fontSize24),
            marginRight: 3,
            ...getMarginTop(-4, 0),
            ...style,
          },
        }}
        text3Override={{
          fontFamily,
          style: { ...getFontSize(fontSize88, fontSize62), ...getMarginTop(-25, -15), ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize48, fontSize24),
            marginLeft: 3,
            ...getMarginTop(-10, -2),
            ...style,
          },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: { ...getFontSize(fontSize24, fontSize24), marginLeft: 3, marginTop: -8, ...style },
        }}
        firstColDirectionView={{ isRowDirection: false }}
        {...props}
      />
    );
  },
  price_point_style_5: (props) => {
    const { style, text } = props;
    const trimmedTextItems = text.trim();
    const textItems = trimmedTextItems.includes('_')
      ? trimmedTextItems.split('_')
      : trimmedTextItems.split(' ');
    const singleRowSingleDigit = textItems.length === 3 && textItems[1].length === 1;
    const singleRowDoubleDigit = textItems.length === 3 && textItems[1].length === 2;
    return (
      <TwoRowCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: !singleRowDoubleDigit
            ? { ...fontSize24, marginTop: 0, marginLeft: -65, ...style }
            : { ...fontSize24, marginTop: 0, marginLeft: -115, ...style },
        }}
        text2Override={{
          fontFamily,
          style: { ...fontSize62, marginTop: -47, ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: !singleRowSingleDigit && {
            ...fontSize36,
            marginLeft: 100,
            marginTop: -65,
            ...style,
          },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize16, marginTop: -8, marginLeft: -75, ...style },
        }}
        {...props}
      />
    );
  },
  price_point_style_6: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize36, fontSize24),
            marginRight: 0,
            ...getMarginTop(-5, 0),
            ...style,
          },
        }}
        text3Override={{
          fontFamily,
          style: { ...getFontSize(fontSize88, fontSize62), ...getMarginTop(-30, -15), ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize48, fontSize36),
            marginLeft: 0,
            ...getMarginTop(-12, -6),
            ...style,
          },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize16, fontSize18),
            marginLeft: 0,
            ...getMarginTop(-8, -12),
            ...style,
          },
        }}
        firstColDirectionView={{ isRowDirection: false }}
        {...props}
      />
    );
  },
  price_point_style_7: (props) => {
    const { style } = props;
    return (
      <TwoRowCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize24, marginTop: 0, marginLeft: -90, ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize62, marginLeft: 30, marginTop: -38, ...style },
        }}
        text5Override={{
          fontFamily,
          style: { ...fontSize36, marginTop: -28, width: 60, ...style },
        }}
        text6Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize16, marginLeft: -80, marginTop: 27, ...style },
        }}
        {...props}
      />
    );
  },
  price_point_style_8: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text3Override={{
          fontFamily,
          style: { ...getFontSize(fontSize112, fontSize62), ...getMarginTop(-30, -15), ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize64, fontSize36),
            marginLeft: 0,
            ...getMarginTop(-12, -6),
            ...style,
          },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize32, fontSize18),
            marginLeft: 0,
            ...getMarginTop(-25, -10),
            ...style,
          },
        }}
        firstColDirectionView={{ isRowDirection: false }}
        {...props}
      />
    );
  },
  price_point_style_10: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize62, marginTop: 0, ...style },
        }}
        text2Override={{
          fontFamily,
          style: { ...fontSize62, marginTop: 0, ...style },
        }}
        text3Override={{
          fontFamily,
          style: { ...fontSize62, marginTop: 0, ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize36, marginLeft: 3, marginTop: 10, ...style },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize18, marginLeft: 3, marginTop: -12, ...style },
        }}
        firstColDirectionView={{ isRowDirection: true }}
        {...props}
      />
    );
  },
  price_point_style_13: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize36, fontSize24),
            marginRight: 0,
            marginTop: 0,
            ...style,
          },
        }}
        text2Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize36, fontSize24),
            marginRight: 0,
            ...getMarginTop(-12, -8),
            ...style,
          },
        }}
        text3Override={{
          fontFamily,
          style: { ...getFontSize(fontSize88, fontSize62), ...getMarginTop(-18, -14), ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: { ...getFontSize(fontSize48, fontSize36), marginLeft: 0, marginTop: -5, ...style },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize24, fontSize18),
            marginLeft: 0,
            marginTop: -10,
            ...style,
          },
        }}
        {...props}
      />
    );
  },
  price_point_style_14: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: {
            ...fontSize24,
            marginRight: 3,
            width: 40,
            marginTop: 0,
            ...style,
          },
        }}
        text2Override={{
          fontFamily,
          fontWeight,
          style: {
            ...fontSize24,
            marginRight: 3,
            width: 40,
            marginTop: -7,
            ...style,
          },
        }}
        text3Override={{
          fontFamily,
          style: { ...fontSize62, marginTop: -14, ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize62, marginLeft: 3, marginTop: -12, ...style },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize62, marginLeft: 3, marginTop: -12, ...style },
        }}
        lastColDirectionView={{ isRowDirection: true }}
        {...props}
      />
    );
  },
  price_point_style_15: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <TwoRowCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: { ...getFontSize(fontSize28, fontSize16), marginTop: 0, ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize112, fontSize62),
            marginLeft: 5,
            ...getMarginTop(-23, -10),
            ...style,
          },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize64, fontSize36),
            ...getMarginLeft(-2, 0),
            ...getMarginTop(-8, -2),
            ...style,
          },
        }}
        text6Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize32, fontSize18),

            ...getMarginLeft(-2, 0),
            ...getMarginTop(-21, -12),
            ...style,
          },
        }}
        flexDirectionBottomView={{ isModBOverlay }}
        flexDirectionLastColView={{ isColumnDirection: true }}
        {...props}
      />
    );
  },
  price_point_style_16: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <TwoRowCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: { ...fontSize16, marginTop: 0, ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: {
            ...fontSize62,
            marginLeft: 5,
            marginTop: 0,
            ...style,
          },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: {
            ...fontSize62,
            marginLeft: 2,
            marginTop: -2,
            ...style,
          },
        }}
        text6Override={{
          fontFamily,
          fontWeight,
          style: {
            ...fontSize62,
            marginLeft: 5,
            marginTop: 0,

            ...style,
          },
        }}
        flexDirectionLastColView={{ isColumnDirection: false }}
        {...props}
      />
    );
  },
  price_point_style_17: (props) => {
    const { style, bannerPosition } = props;
    getModBOverlay(bannerPosition);
    return (
      <ThreeColCurrencyUpPromoBanner
        text1Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize36, fontSize24),
            marginRight: 0,
            ...getMarginTop(-5, 0),
            ...style,
          },
        }}
        text3Override={{
          fontFamily,
          style: { ...getFontSize(fontSize88, fontSize62), ...getMarginTop(-30, -15), ...style },
        }}
        text4Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize48, fontSize36),
            marginLeft: 0,
            ...getMarginTop(-12, -6),
            ...style,
          },
        }}
        text5Override={{
          fontFamily,
          fontWeight,
          style: {
            ...getFontSize(fontSize16, fontSize18),
            marginLeft: 0,
            ...getMarginTop(-7, -12),
            ...style,
          },
        }}
        firstColDirectionView={{ isRowDirection: false }}
        {...props}
      />
    );
  },
};

export default PricePointsStyles;
