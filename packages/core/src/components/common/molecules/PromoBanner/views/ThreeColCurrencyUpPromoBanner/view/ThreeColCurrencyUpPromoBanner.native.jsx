// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes } from 'prop-types';
import {
  BodyCopy,
  TopAlignedView,
  FirstColDirectionView,
  LastColDirectionView,
} from '../ThreeColCurrencyUpPromoBanner.style.native';

import { getPricePointColText } from '../../../PricePoints.utils';

const renderFirstColumn = (
  strObj,
  bodyCopyTextStyle1,
  bodyCopyTextStyle2,
  text1Override,
  text2Override
) => {
  return (
    <>
      {strObj && strObj.part1 && (
        <BodyCopy
          fontWeight="black"
          color="black"
          fontFamily="primary"
          textAlign="center"
          lineHeight="34px"
          style={bodyCopyTextStyle1}
          text={strObj.part1}
          {...text1Override}
        />
      )}
      {strObj && strObj.part2 && (
        <BodyCopy
          fontWeight="black"
          color="black"
          fontFamily="primary"
          textAlign="center"
          lineHeight="88px"
          style={bodyCopyTextStyle2}
          text={strObj.part2}
          {...text2Override}
        />
      )}
    </>
  );
};

const renderLastColumn = (
  strObj,
  bodyCopyTextStyle4,
  bodyCopyTextStyle5,
  text4Override,
  text5Override
) => {
  return (
    <>
      {strObj && strObj.part4 ? (
        <BodyCopy
          fontSize="fs42"
          fontWeight="black"
          color="black"
          fontFamily="primary"
          textAlign="center"
          lineHeight="28px"
          text={strObj.part4}
          style={bodyCopyTextStyle4}
          {...text4Override}
        />
      ) : null}
      {strObj && strObj.part5 ? (
        <BodyCopy
          fontSize="fs42"
          fontWeight="black"
          color="black"
          fontFamily="primary"
          textAlign="center"
          lineHeight="28px"
          text={strObj.part5}
          style={bodyCopyTextStyle5}
          {...text5Override}
        />
      ) : null}
    </>
  );
};
/**
 * This component produces a Currency up Promo Text banner
 * This component accepts text item from CMS
 *
 * It first splits the input text with spaces and then displays differents items in different styles
 * There are 4 items expected in the array - e.g $ 7 80 &up - All are displayed in different styles
 *
 * @param {*} props
 */
const ThreeColCurrencyUpPromoBanner = props => {
  const {
    text,
    style,
    styleClass,
    text1Override,
    text2Override,
    text3Override,
    text4Override,
    text5Override,
    lastColDirectionView,
    firstColDirectionView,

    topAlignedView,
    renderInSameLine,
    isContainerModuleText,
    isLastTextItem,
  } = props;

  const strObj = getPricePointColText(styleClass, text);

  const bodyCopyTextStyle1 = { fontSize: 28, ...style };
  const bodyCopyTextStyle2 = { fontSize: 86, ...style };
  const bodyCopyTextStyle3 = { fontSize: 48, height: 45, textAlign: 'left', ...style };
  const bodyCopyTextStyle4 = { fontSize: 28, ...style };
  const bodyCopyTextStyle5 = { fontSize: 28, ...style };

  return (
    <TopAlignedView
      {...topAlignedView}
      renderInSameLine={renderInSameLine}
      isContainerModuleText={isContainerModuleText}
      isLastTextItem={isLastTextItem}
    >
      <FirstColDirectionView {...firstColDirectionView}>
        {renderFirstColumn(
          strObj,
          bodyCopyTextStyle1,
          bodyCopyTextStyle2,
          text1Override,
          text2Override
        )}
      </FirstColDirectionView>

      {strObj && strObj.part3 && (
        <BodyCopy
          fontWeight="black"
          color="black"
          fontFamily="primary"
          lineHeight="50px"
          text={strObj.part3}
          style={bodyCopyTextStyle3}
          {...text3Override}
        />
      )}
      <LastColDirectionView {...lastColDirectionView}>
        {renderLastColumn(
          strObj,
          bodyCopyTextStyle4,
          bodyCopyTextStyle5,
          text4Override,
          text5Override
        )}
      </LastColDirectionView>
    </TopAlignedView>
  );
};

ThreeColCurrencyUpPromoBanner.propTypes = {
  text: PropTypes.string,
  text1Override: PropTypes.shape({}),
  text2Override: PropTypes.shape({}),
  text3Override: PropTypes.shape({}),
  text4Override: PropTypes.shape({}),
  text5Override: PropTypes.shape({}),
  flexDirectionDefaultView: PropTypes.shape({}),
  style: PropTypes.shape({}),
  firstColDirectionView: PropTypes.shape({}),
  lastColDirectionView: PropTypes.shape({}),
  topAlignedView: PropTypes.shape({}),
  styleClass: PropTypes.string,
  renderInSameLine: PropTypes.bool,
  isContainerModuleText: PropTypes.bool,
  isLastTextItem: PropTypes.bool,
};

ThreeColCurrencyUpPromoBanner.defaultProps = {
  text: '',
  text1Override: {},
  text2Override: {},
  text3Override: {},
  text4Override: {},
  text5Override: {},
  flexDirectionDefaultView: {},
  style: {},
  firstColDirectionView: {},
  lastColDirectionView: {},

  topAlignedView: {},
  styleClass: '',
  renderInSameLine: false,
  isContainerModuleText: false,
  isLastTextItem: false,
};

export default ThreeColCurrencyUpPromoBanner;
