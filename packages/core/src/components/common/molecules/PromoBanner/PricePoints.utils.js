// 9fbef606107a605d69c0edbcd8029e5d
export const getPricePointColText = (style, text) => {
  const trimmedTextItems = text.trim();
  const textItems = trimmedTextItems.includes('_')
    ? trimmedTextItems.split('_')
    : trimmedTextItems.split(' ');

  const isPricePointStyle4 = style.includes('_4');
  const isPricePointStyle6 = style.includes('_6');
  const isPricePointStyle8 = style.includes('_8');
  const isPricePointStyle17 = style.includes('_17');

  if (isPricePointStyle4 || isPricePointStyle6 || isPricePointStyle17) {
    return {
      part1: textItems[0],
      part3: textItems[1],
      part4: textItems[2],
      part5: textItems[3],
    };
  }
  if (isPricePointStyle8) {
    return {
      part3: textItems[0],
      part4: textItems[1],
      part5: textItems[2],
    };
  }
  return {
    part1: textItems[0],
    part2: textItems[1],
    part3: textItems[2],
    part4: textItems[3],
    part5: textItems[4],
  };
};

export const getPricePointRowText = (style, text) => {
  const trimmedTextItems = text.trim();
  const textItems = trimmedTextItems.includes('_')
    ? trimmedTextItems.split('_')
    : trimmedTextItems.split(' ');

  const isPricePointStyle3 = style.includes('_3');
  const isPricePointStyle5 = style.includes('_5');
  const isPricePointStyle7 = style.includes('_7');
  const isPricePointStyle12 = style.includes('_12');
  const isPricePointStyle15 = style.includes('_15');
  const isPricePointStyle16 = style.includes('_16');

  if (isPricePointStyle3) {
    return {
      part1: textItems[0],
      part4: textItems[1],
      part5: textItems[2],
      part6: textItems[3],
    };
  }
  if (isPricePointStyle5) {
    return {
      part1: textItems[0],
      part2: textItems[1],
      part4: textItems[2],
      part5: textItems[3],
    };
  }
  if (isPricePointStyle7 || isPricePointStyle12 || isPricePointStyle15 || isPricePointStyle16) {
    return {
      part1: textItems[0],
      part4: textItems[1],
      part5: textItems[2],
      part6: textItems[3],
    };
  }

  return textItems;
};
