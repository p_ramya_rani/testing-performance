/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
/* stylelint-disable */
/* eslint-disable sonarjs/cognitive-complexity */
import { css } from 'styled-components';
import { getStyleArray } from '@tcp/core/src/utils/utils.web';

const getCommonPriceStyles = (props) => {
  return `
  .price_point_style_4,
  .price_point_style_6,
  .price_point_style_8,
  .price_point_style_17 {
    justify-content: center;
    .col-1,
    .col-3 {
      display: flex;
      flex-direction: column;
    }
    .col-2 {
      margin-top: ${props.isModBOverlay ? '-32px' : '-6px;'};
    }
    .col-3 {
      margin-top: ${props.isModBOverlay ? '-34px' : '-1px;'};
      align-items: baseline;
    }
    @media ${props.theme.mediaQuery.large} {
      .col-2 {
        margin-top: ${props.isModBOverlay ? '-32px' : '-3px;'};
      }
      .col-3 {
        margin-top: ${props.isModBOverlay ? '-10px' : '-4px;'};
      }
    }
  }`;
};

// eslint-disable-next-line complexity
const extractPricePointStyle = (props, styleName) => {
  switch (styleName) {
    case 'price_point_style_1':
      return `
      .price_point_style_1 {
        justify-content: center;
        .col-1,
        .col-2 {
          display: flex;
          align-items: flex-start;
        }
        .price_point_style_1-0 {
          font-size: ${
            props.isModBOverlay
              ? props.theme.typography.fontSizes.fs36
              : props.theme.typography.fontSizes.fs24
          };
          line-height: ${props.isModBOverlay ? '49px' : '33px'};
        }

        .price_point_style_1-1 {
          font-size: ${props.isModBOverlay ? '112px' : '62px'};
          line-height: ${props.isModBOverlay ? '152px' : '84px'};
          margin-top: ${props.isModBOverlay ? '-27px' : '-15px'};
          margin-left: 2px;
        }
        .price_point_style_1-2 {
          font-size: ${
            props.isModBOverlay
              ? props.theme.typography.fontSizes.fs48
              : props.theme.typography.fontSizes.fs36
          };
          line-height: ${props.isModBOverlay ? '65px' : '49px'};
          margin-top: ${props.isModBOverlay ? '-7px' : '-4px'};
          margin-left: 2px;
        }
        @media ${props.theme.mediaQuery.large} {
          .price_point_style_1-0 {
            font-size: ${props.isModBOverlay ? '48px' : props.theme.typography.fontSizes.fs36};
            line-height: ${props.isModBOverlay ? '65px' : '49px'};
          }
          .price_point_style_1-1 {
            font-size: ${props.isModBOverlay ? '152px' : '88px'};
            line-height: ${props.isModBOverlay ? '207px' : '120px'};
            margin-top: ${props.isModBOverlay ? '-39px' : '-21px'};
            margin-left: ${props.isModBOverlay ? '4px' : '0'};
          }
          .price_point_style_1-2 {
            font-size: ${props.isModBOverlay ? '88px' : props.theme.typography.fontSizes.fs48};
            line-height: ${props.isModBOverlay ? '120px' : '65px'};
            margin-top: ${props.isModBOverlay ? '-20px' : '-5px'};
            margin-left: 0;
          }
        }
      }`;
    case 'price_point_style_2':
      return `
        .price_point_style_2 {
          justify-content: center;
          .col-1,
          .col-2,
          .col-3 {
            display: flex;
            align-items: flex-start;
          }
          .price_point_style_2-0 {
            font-size: ${props.isModBOverlay ? '36px' : '24px'};
            line-height: ${props.isModBOverlay ? '49px' : '33px'};
          }

          .price_point_style_2-1,
          .price_point_style_2-3 {
            font-size: ${props.isModBOverlay ? '112px' : '62px'};
            line-height: ${props.isModBOverlay ? '152px' : '84px'};
            margin-top: ${props.isModBOverlay ? '-32px' : '-15px'};
            margin-left: 2px;
          }
          .price_point_style_2-2,
          .price_point_style_2-4 {
            font-size: ${props.isModBOverlay ? '48px' : '36px'};
            line-height: ${props.isModBOverlay ? '65px' : '49px'};
            margin-top: ${props.isModBOverlay ? '-7px' : '-4px'};
            margin-left: 2px;
          }
          @media ${props.theme.mediaQuery.large} {
            .price_point_style_2-0 {
              font-size: ${props.isModBOverlay ? '48px' : props.theme.typography.fontSizes.fs36};
              line-height: ${props.isModBOverlay ? '65px' : '49px'};
            }
            .price_point_style_2-1,
            .price_point_style_2-3 {
              font-size: ${props.isModBOverlay ? '152px' : '88px'};
              line-height: ${props.isModBOverlay ? '207px' : '120px'};
              margin-top: ${props.isModBOverlay ? '-42px' : '-21px'};
            }
            .price_point_style_2-2,
            .price_point_style_2-4 {
              font-size: ${props.isModBOverlay ? '86px' : props.theme.typography.fontSizes.fs48};
              line-height: ${props.isModBOverlay ? '118px' : '65px'};
              margin-top: ${props.isModBOverlay ? '-15px' : '-5px'};
              margin-left: ${props.isModBOverlay ? '-4px' : '0'};
            }
          }
        }`;
    case 'price_point_style_3':
      return `
          .price_point_style_3 {
            flex-direction: column;
            justify-content: center;
            align-items: center;
            .row-1 {
              margin-bottom: 6px;
              font-size: initial;
            }
            .row-2 {
              display: flex;
              justify-content: center;
              align-items: flex-start;
            }
            .price_point_style_3-0,
            .price_point_style_3-1 {
              font-size: ${props.isModBOverlay ? '28px' : props.theme.typography.fontSizes.fs16};
              line-height: ${props.isModBOverlay ? '38px' : '22px'};
              white-space: nowrap;
            }
            .price_point_style_3-1 {
              margin-left: 6px;
            }
            .price_point_style_3-3 {
              font-size: ${props.isModBOverlay ? '36px' : '24px'};
              line-height: ${props.isModBOverlay ? '49px' : '33px'};
              margin-top: ${props.isModBOverlay ? '-4px' : '-6px'};
            }
            .price_point_style_3-4 {
              font-size: ${props.isModBOverlay ? '112px' : '62px'};
              line-height: ${props.isModBOverlay ? '152px' : '84px'};
              margin-top: ${props.isModBOverlay ? '-30px' : '-18px'};
            }
            .price_point_style_3-5 {
              font-size: ${props.isModBOverlay ? '48px' : '36px'};
              line-height: ${props.isModBOverlay ? '65px' : '49px'};
              margin-top: -9px;
              margin-left: 3px;
            }

            @media ${props.theme.mediaQuery.large} {
              .row-1 {
                margin-bottom: ${props.isModBOverlay ? '10' : '5px'};
              }
              .price_point_style_3-0,
              .price_point_style_3-1 {
                font-size: ${props.isModBOverlay ? '36px' : props.theme.typography.fontSizes.fs24};
                line-height: ${props.isModBOverlay ? '49px' : '33px'};
              }
              .price_point_style_3-3 {
                font-size: ${props.isModBOverlay ? '48px' : '36px'};
                line-height: ${props.isModBOverlay ? '65px' : '49px'};
                margin-top: ${props.isModBOverlay ? '-18px' : '-9px'};
              }
              .price_point_style_3-4 {
                font-size: ${props.isModBOverlay ? '152px' : '88px'};
                line-height: ${props.isModBOverlay ? '207px' : '120px'};
                margin-top: ${props.isModBOverlay ? '-50px' : '-28px'};
              }
              .price_point_style_3-5 {
                font-size: ${props.isModBOverlay ? '88px' : '48px'};
                line-height: ${props.isModBOverlay ? '120px' : '65px'};
                margin-top: ${props.isModBOverlay ? '-29px' : '-14px'};
                margin-left: ${props.isModBOverlay ? '-4' : '0'};
              }
            }
          }`;
    case 'price_point_style_5':
      return `
            .price_point_style_5 {
              align-items: center;
              .row-1,
              .row-2 {
                display: flex;
                flex-direction: row;
                align-items: flex-start;
              }
              .row-2 {
                margin-top: ${props.isModBOverlay ? '-32px' : '-16px'};
              }
              .price_point_style_5-0 {
                font-size: ${props.isModBOverlay ? '36px' : '24px'};
                line-height: ${props.isModBOverlay ? '49px' : '33px'};
                margin-top: ${props.isModBOverlay ? '0' : '-2px'};
                margin-top: 5px;
              }

              .price_point_style_5-1 {
                font-size: ${props.isModBOverlay ? '112px' : '62px'};
                line-height: ${props.isModBOverlay ? '152px' : '84px'};
                margin-top: ${props.isModBOverlay ? '-25px' : '-14px'};
              }
              .price_point_style_5-3 {
                font-size: ${props.isModBOverlay ? '36px' : '24px'};
                line-height: ${props.isModBOverlay ? '49px' : '33px'};
                margin-top: ${props.isModBOverlay ? '-90px' : '-50px'};
                margin-left: ${props.isModBOverlay ? '125px' : '75px'};
              }
              .price_point_style_5-3:only-child,
              .price_point_style_5-4 {
                font-size: ${props.isModBOverlay ? '24px' : '16px'};
                line-height: ${props.isModBOverlay ? '33px' : '22px'};
                margin-left: ${props.isModBOverlay ? '35px' : '25px'};
                margin-top: 0;
              }
              .price_point_style_5-4 {
                margin-left: ${props.isModBOverlay ? '-95px' : '-65px'};
              }

              @media ${props.theme.mediaQuery.large} {
                .row-2 {
                  margin-top: ${props.isModBOverlay ? '-42px' : '-30px'};
                }
                .price_point_style_5-0 {
                  font-size: ${props.isModBOverlay ? '48px' : '36px'};
                  line-height: ${props.isModBOverlay ? '65px' : '49px'};
                  margin-top: 5px;
                }

                .price_point_style_5-1 {
                  font-size: ${props.isModBOverlay ? '152px' : '88px'};
                  line-height: ${props.isModBOverlay ? '207px' : '120px'};
                  margin-top: ${props.isModBOverlay ? '-36px' : '-14px'};
                }
                .price_point_style_5-3 {
                  font-size: ${props.isModBOverlay ? '48px' : '36px'};
                  line-height: 0;
                  margin-top: ${props.isModBOverlay ? '-90px' : '-45px'};
                  margin-left: ${props.isModBOverlay ? '152px' : '110px'};
                }
                .price_point_style_5-3:only-child,
                .price_point_style_5-4 {
                  font-size: ${props.isModBOverlay ? '32px' : '24px'};
                  line-height: ${props.isModBOverlay ? '44px' : '33px'};
                  margin-left: ${props.isModBOverlay ? '35px' : '25px'};
                  margin-top: 0;
                }
                .price_point_style_5-4 {
                  margin-left: ${props.isModBOverlay ? '-145px' : '-98px'};
                }
              }
            }`;
    case 'price_point_style_13':
    case 'price_point_style_14':
      return `
        .price_point_style_13,
        .price_point_style_14 {
          justify-content: center;
          .col-1,
          .col-3 {
            display: flex;
            flex-direction: column;
            text-align: center;
          }
          .col-2 {
            margin-top: ${props.isModBOverlay ? '-8px' : '0'};
            margin-left: 0;
          }
          .col-1 {
            margin-left: -6px;
          }
          .col-3 {
            margin-left: -2px;
          }
          .price_point_style_13-0,
          .price_point_style_14-0,
          .price_point_style_13-1,
          .price_point_style_14-1 {
            font-size: ${props.isModBOverlay ? '36px' : '24px'};
            line-height: ${props.isModBOverlay ? '49px' : '33px'};
            margin-top: 14px;
          }
          .price_point_style_13-1,
          .price_point_style_14-1 {
            margin-top: ${props.isModBOverlay ? '-18px' : '-10px'};
          }

          .price_point_style_13-2,
          .price_point_style_14-2,
          .price_point_style_14-3,
          .price_point_style_14-4 {
            font-size: ${props.isModBOverlay ? '88px' : '62px'};
            line-height: ${props.isModBOverlay ? '120px' : '84px'};
          }
          .price_point_style_13-3 {
            font-size: ${props.isModBOverlay ? '48px' : '36px'};
            line-height: ${props.isModBOverlay ? '65px' : '49px'};
            margin-top: 8px;
          }
          .price_point_style_13-4 {
            font-size: ${props.isModBOverlay ? '24px' : '18px'};
            line-height: ${props.isModBOverlay ? '33px' : '24px'};
            margin-top: ${props.isModBOverlay ? '-16px' : '-12px'};
          }
          .price_point_style_14-4 {
            margin-left: 10px;
          }

          @media ${props.theme.mediaQuery.large} {
            .col-1 {
              margin-left: ${props.isModBOverlay ? '-10px' : '-2px'};
            }
            .col-2 {
              margin-top: ${props.isModBOverlay ? '-10px' : '0'};
            }
            .col-3 {
              margin-left: ${props.isModBOverlay ? '-6px' : '-2px'};
            }
            .price_point_style_13-0,
            .price_point_style_14-0,
            .price_point_style_13-1,
            .price_point_style_14-1 {
              font-size: ${props.isModBOverlay ? '64px' : '36px'};
              line-height: ${props.isModBOverlay ? '87px' : '49px'};
              margin-top: 20px;
            }
            .price_point_style_13-1,
            .price_point_style_14-1 {
              margin-top: ${props.isModBOverlay ? '-30px' : '-12px'};
            }
            .price_point_style_13-2,
            .price_point_style_14-2,
            .price_point_style_14-3,
            .price_point_style_14-4 {
              font-size: ${props.isModBOverlay ? '152px' : '88px'};
              line-height: ${props.isModBOverlay ? '207px' : '120px'};
            }
            .price_point_style_13-3 {
              font-size: ${props.isModBOverlay ? '88px' : '48px'};
              line-height: ${props.isModBOverlay ? '120px' : '65px'};
              margin-top: 14px;
            }
            .price_point_style_13-4 {
              font-size: ${props.isModBOverlay ? '42px' : '24px'};
              line-height: ${props.isModBOverlay ? '58px' : '33px'};
              margin-top: ${props.isModBOverlay ? '-32px' : '-11px'};
            }
            .price_point_style_14-3 {
              margin-left: 0;
            }
          }
        }
        ${
          styleName === 'price_point_style_14'
            ? `
        .price_point_style_14 {
          .col-3 {
            flex-direction: row;
            align-items: flex-start;
          }
        }`
            : ''
        }`;
    case 'price_point_style_15':
    case 'price_point_style_16':
      return `
        .price_point_style_15,
        .price_point_style_16 {
          flex-direction: column;
          align-items: center;
          .row-2 {
            display: flex;
            flex-direction: row;
            margin-top: ${props.isModBOverlay ? '-28px' : '-15px'};
          }

          .col-2 {
            display: flex;
            flex-direction: column;
            margin-top: ${props.isModBOverlay ? '24px' : '14px'};
            margin-left: 0;
          }

          .price_point_style_15-0,
          .price_point_style_16-0,
          .price_point_style_15-1,
          .price_point_style_16-1 {
            font-size: ${props.isModBOverlay ? '28px' : '16px'};
            line-height: ${props.isModBOverlay ? '38px' : '22px'};
            margin-top: 12px;
          }

          .price_point_style_15-1,
          .price_point_style_16-1 {
            margin-top: -10px;
          }
          .price_point_style_15-3,
          .price_point_style_16-2,
          .price_point_style_16-3,
          .price_point_style_16-4,
          .price_point_style_16-5 {
            font-size: 62px;
            line-height: 84px;
          }
          .price_point_style_15-3 {
            font-size: ${props.isModBOverlay ? '112px' : '62px'};
            line-height: ${props.isModBOverlay ? '152px' : '84px'};
            margin-top: 6px;
          }
          .price_point_style_15-4 {
            font-size: ${props.isModBOverlay ? '64px' : '36px'};
            line-height: ${props.isModBOverlay ? '87px' : '49px'};
            margin-top: ${props.isModBOverlay ? '-8px' : '-6px'};
          }
          .price_point_style_15-5 {
            font-size: ${props.isModBOverlay ? '32px' : '18px'};
            line-height: ${props.isModBOverlay ? '44px' : '24px'};
            margin-top: ${props.isModBOverlay ? '-23px' : '-13px'};
          }

          @media ${props.theme.mediaQuery.large} {
            .row-2 {
              margin-top: ${props.isModBOverlay ? '-40px' : '-25px'};
            }
            .col-2 {
              margin-top: ${props.isModBOverlay ? '30px' : '21px'};
            }
            .price_point_style_15-0,
            .price_point_style_16-0,
            .price_point_style_15-1,
            .price_point_style_16-1 {
              font-size: ${props.isModBOverlay ? '36px' : '24px'};
              line-height: ${props.isModBOverlay ? '49px' : '33px'};
            }
            .price_point_style_15-1,
            .price_point_style_16-1 {
              margin-top: -16px;
            }
            .price_point_style_15-3 {
              font-size: ${props.isModBOverlay ? '152px' : '88px'};
              line-height: ${props.isModBOverlay ? '207px' : '120px'};
              margin-top: 12px;
            }
            .price_point_style_16-2,
            .price_point_style_16-3,
            .price_point_style_16-4,
            .price_point_style_16-5 {
              font-size: 88px;
              line-height: 120px;
              margin-top: 12px;
            }

            .price_point_style_15-4 {
              font-size: ${props.isModBOverlay ? '88px' : '48px'};
              line-height: ${props.isModBOverlay ? '120px' : '65px'};
              margin-top: ${props.isModBOverlay ? '-8px' : '-6px'};
            }
            .price_point_style_15-5 {
              font-size: ${props.isModBOverlay ? '42px' : '24px'};
              line-height: ${props.isModBOverlay ? '58px' : '33px'};
              margin-top: ${props.isModBOverlay ? '-32px' : '-15px'};
            }

            .price_point_style_16-3 {
              margin-left: -6px;
            }
            .price_point_style_16-4 {
              margin-right: 10px;
            }
          }
        }
        ${
          styleName === 'price_point_style_16'
            ? `
        .price_point_style_16 {
          .col-1 {
            margin-top: 0;
            @media ${props.theme.mediaQuery.large} {
              margin-top: 18px;
            }
          }
          .col-2 {
            flex-direction: row;
            margin-top: 0;
            @media ${props.theme.mediaQuery.large} {
              margin-top: 6px;
            }
          }
        }`
            : ''
        }`;
    case 'price_point_style_4':
      return `
        ${getCommonPriceStyles(props)}
        .price_point_style_4 {
          .price_point_style_4-0,
          .price_point_style_4-1 {
            font-size: ${props.isModBOverlay ? '36px' : props.theme.typography.fontSizes.fs24};
            line-height: ${props.isModBOverlay ? '49px' : '33px'};
          }
          .price_point_style_4-2 {
            font-size: ${props.isModBOverlay ? '112px' : '62px'};
            line-height: ${props.isModBOverlay ? '152px' : '84px'};
          }

          .price_point_style_4-3 {
            font-size: ${props.isModBOverlay ? '36px' : '24px'};
            line-height: ${props.isModBOverlay ? '49px' : '33px'};
            margin-top: ${props.isModBOverlay ? '28px' : '6px'};
          }
          .price_point_style_4-4 {
            font-size: ${props.isModBOverlay ? '36px' : '24px'};
            line-height: ${props.isModBOverlay ? '49px' : '33px'};
            margin-top: ${props.isModBOverlay ? '-14px' : '-10px'};
          }
          .price_point_style_4-0 {
            margin-top: ${props.isModBOverlay ? '-4px' : '8px'};
            margin-right: 0;
          }
          .price_point_style_4-1 {
            margin-top: -10px;
          }
          @media ${props.theme.mediaQuery.large} {
            .col-2 {
              margin-top: ${props.isModBOverlay ? '-39px' : '-14px;'};
            }
            .col-3 {
              margin-top: ${props.isModBOverlay ? '-6px' : '-4px;'};
            }
            .price_point_style_4-0,
            .price_point_style_4-1,
            .price_point_style_4-3,
            .price_point_style_4-4 {
              font-size: ${props.isModBOverlay ? '48px' : props.theme.typography.fontSizes.fs36};
              line-height: ${props.isModBOverlay ? '65px' : '49px'};
            }
            .price_point_style_4-2 {
              font-size: ${props.isModBOverlay ? '152px' : '88px'};
              line-height: ${props.isModBOverlay ? '207px' : '120px'};
              margin-left: ${props.isModBOverlay ? '4px' : '0'};
            }

            .price_point_style_4-0,
            .price_point_style_4-3 {
              margin-top: 6px;
            }
            .price_point_style_4-1 {
              margin-top: -10px;
            }

            .price_point_style_4-4 {
              margin-top: -15px;
            }
          }
        }`;
    case 'price_point_style_6':
    case 'price_point_style_17':
      return `
        ${getCommonPriceStyles(props)}
        .price_point_style_6,
        .price_point_style_17 {
          margin-top: 10px;
          .col-2 {
            margin-top: ${props.isModBOverlay ? '-28px' : '-6px;'};
          }
          .price_point_style_6-0,
          .price_point_style_17-0 {
            font-size: ${props.isModBOverlay ? '36px' : props.theme.typography.fontSizes.fs24};
            line-height: ${props.isModBOverlay ? '49px' : '33px'};
            margin-top: ${props.isModBOverlay ? '-8px' : '8px'};
            margin-right: 0;
          }
          .price_point_style_6-2,
          .price_point_style_17-2 {
            font-size: ${props.isModBOverlay ? '88px' : '62px'};
            line-height: ${props.isModBOverlay ? '120px' : '84px'};
          }
          .price_point_style_6-3,
          .price_point_style_17-3 {
            font-size: ${props.isModBOverlay ? '48px' : '36px'};
            line-height: ${props.isModBOverlay ? '65px' : '49px'};
            margin-top: ${props.isModBOverlay ? '18px' : '4px'};
          }
          .price_point_style_6-4 {
            font-size: ${props.isModBOverlay ? '24px' : '18px'};
            line-height: ${props.isModBOverlay ? '33px' : '24px'};
            margin-top: ${props.isModBOverlay ? '-16px' : '-12px'};
            white-space: nowrap;
          }
          .price_point_style_17-4 {
            font-size: ${props.isModBOverlay ? '16px' : '18px'};
            line-height: ${props.isModBOverlay ? '22px' : '24px'};
            margin-top: -12px;
            white-space: nowrap;
          }

          @media ${props.theme.mediaQuery.large} {
            margin-top: 0;
            .price_point_style_6-0,
            .price_point_style_17-0 {
              font-size: ${props.isModBOverlay ? '48px' : props.theme.typography.fontSizes.fs36};
              line-height: ${props.isModBOverlay ? '65px' : '49px'};
              margin-top: ${props.isModBOverlay ? '8px' : '16px'};
            }
            .price_point_style_6-2,
            .price_point_style_17-2 {
              font-size: ${props.isModBOverlay ? '152px' : '88px'};
              line-height: ${props.isModBOverlay ? '207px' : '120px'};
              margin-left: ${props.isModBOverlay ? '4px' : '0'};
            }
            .price_point_style_6-3,
            .price_point_style_17-3 {
              font-size: ${props.isModBOverlay ? '86px' : props.theme.typography.fontSizes.fs48};
              line-height: ${props.isModBOverlay ? '118px' : '65px'};
              margin-top: ${props.isModBOverlay ? '0' : '14px'};
            }
            .price_point_style_6-4 {
              font-size: ${props.isModBOverlay ? '48px' : props.theme.typography.fontSizes.fs24};
              line-height: ${props.isModBOverlay ? '65px' : '33px'};
              margin-top: ${props.isModBOverlay ? '-34px' : '-18px'};
              white-space: nowrap;
            }
            .price_point_style_17-4 {
              font-size: ${props.isModBOverlay ? '28px' : props.theme.typography.fontSizes.fs24};
              line-height: ${props.isModBOverlay ? '38px' : '33px'};
              margin-top: ${props.isModBOverlay ? '-26px' : '-18px'};
              white-space: nowrap;
            }
            .price_point_style_17-2 {
              margin-left: ${props.isModBOverlay ? '-6px' : '0'};
            }
          }
        }
        ${
          styleName === 'price_point_style_17'
            ? `
        .price_point_style_17 {
          .col-2 {
            margin-top: ${props.isModBOverlay ? '-32px' : '-6px;'};
          }
        }`
            : ''
        }`;
    case 'price_point_style_8':
      return `
        ${getCommonPriceStyles(props)}
        .price_point_style_8 {
          margin-top: 10px;
          .col-3 {
            margin-left: 0;
          }
          .price_point_style_8-2 {
            font-size: ${props.isModBOverlay ? '112px' : '62px'};
            line-height: ${props.isModBOverlay ? '152px' : '84px'};
          }
          .price_point_style_8-3 {
            font-size: ${props.isModBOverlay ? '64px' : props.theme.typography.fontSizes.fs36};
            line-height: ${props.isModBOverlay ? '87px' : '49px'};
            margin-top: ${props.isModBOverlay ? '18px' : '4px'};
          }
          .price_point_style_8-4 {
            font-size: ${props.isModBOverlay ? '32px' : '18px'};
            line-height: ${props.isModBOverlay ? '44px' : '24px'};
            margin-top: ${props.isModBOverlay ? '-25px' : '-12px'};
          }

          @media ${props.theme.mediaQuery.large} {
            margin-top: 0;
            .col-3 {
              margin-left: 0;
            }
            .price_point_style_8-2 {
              font-size: ${props.isModBOverlay ? '152px' : '88px'};
              line-height: ${props.isModBOverlay ? '207px' : '120px'};
              margin-left: 0;
            }
            .price_point_style_8-3 {
              font-size: ${props.isModBOverlay ? '88px' : '48px'};
              line-height: ${props.isModBOverlay ? '120px' : '65px'};
              margin-top: ${props.isModBOverlay ? '0' : '12px'};
            }
            .price_point_style_8-4 {
              font-size: ${props.isModBOverlay ? '42px' : '24px'};
              line-height: ${props.isModBOverlay ? '58px' : '33px'};
              margin-top: ${props.isModBOverlay ? '-34px' : '-16px'};
            }
          }
        }`;
    case 'price_point_style_10':
      return `
        .price_point_style_10,
        .promoBanner.promo-text.price_point_style_10 {
          justify-content: center;
          .col-1 {
            flex-direction: row;
            margin-top: -12px;
            .price_point_style_10-1 {
              margin-top: 0;
            }
          }
          .col-2 {
            margin-top: -10px;
          }
          .col-3 {
            display: flex;
            flex-direction: column;
          }
          .price_point_style_10-0,
          .price_point_style_10-1,
          .price_point_style_10-2 {
            font-size: 62px;
            line-height: 84px;
          }
          .price_point_style_10-3 {
            font-size: 36px;
            line-height: 49px;
            margin-top: -2px;
          }
          .price_point_style_10-4 {
            font-size: 18px;
            line-height: 24px;
            margin-top: -13px;
          }
          @media ${props.theme.mediaQuery.large} {
            .price_point_style_10-0,
            .price_point_style_10-1,
            .price_point_style_10-2 {
              font-size: 88px;
              line-height: 120px;
            }
            .price_point_style_10-3 {
              font-size: 48px;
              line-height: 65px;
              margin-top: 6px;
            }
            .price_point_style_10-4 {
              font-size: 24px;
              line-height: 33px;
              margin-top: -16px;
            }
          }
        }`;
    case 'price_point_style_19':
      return `
        .price_point_style_19 {
          justify-content: center;

          .col-1,
          .col-3 {
            display: flex;
            flex-direction: column;
          }
          .col-2 {
            margin-top: -2px;
          }
          .currency_up_style-0,
          .price_point_style_19-0 {
            font-size: ${props.theme.typography.fontSizes.fs28};

            line-height: 1.3;
          }
          .currency_up_style-0,
          .currency_up_style-1,
          .price_point_style_19-0,
          .price_point_style_19-1 {
            font-size: ${props.theme.typography.fontSizes.fs24};
            line-height: 33px;

            margin-top: 12px;
          }
          .currency_up_style-1,
          .price_point_style_19-1 {
            margin-top: -10px;
          }
          .currency_up_style-2,
          .price_point_style_19-2 {
            font-size: 62px;
            line-height: 84px;
          }
          .currency_up_style-3,
          .price_point_style_19-3 {
            font-size: ${props.theme.typography.fontSizes.fs36};
            line-height: 49px;

            margin-top: 6px;
          }
          .currency_up_style-4,
          .price_point_style_19-4 {
            font-size: ${props.theme.typography.fontSizes.fs18};
            line-height: 24px;

            margin-top: -10px;
          }

          @media ${props.theme.mediaQuery.large} {
            .currency_up_style-0,
            .currency_up_style-1,
            .price_point_style_19-0,
            .price_point_style_19-1 {
              font-size: ${props.theme.typography.fontSizes.fs36};
              line-height: 49px;
              margin-top: 15px;
            }
            .currency_up_style-1,
            .price_point_style_19-1 {
              margin-top: -10px;
            }
            .currency_up_style-2,
            .price_point_style_19-2 {
              font-size: 88px;
              line-height: 120px;
            }
            .currency_up_style-3,
            .price_point_style_19-3 {
              font-size: ${props.theme.typography.fontSizes.fs48};

              line-height: 65px;
              margin-top: 12px;
            }
            .currency_up_style-4,
            .price_point_style_19-4 {
              font-size: ${props.theme.typography.fontSizes.fs24};

              line-height: 33px;
              margin-top: -12px;
            }
          }
        }`;
    case 'price_point_style_7':
      return `
        .price_point_style_7 {
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: center;
          .row-1,
          .row-2 {
            display: flex;
          }
          .price_point_style_7-0 {
            font-size: ${props.isModBOverlay ? '36px' : '24px'};
            margin-top: ${props.isModBOverlay ? '-40px;' : '-28px'};
          }
          .price_point_style_7-3 {
            font-size: ${props.isModBOverlay ? '112px' : '62px'};
          }
          .price_point_style_7-4 {
            font-size: ${props.isModBOverlay ? '36px' : '24px'};
            margin-top: ${props.isModBOverlay ? '30px' : '15px'};
          }
          .price_point_style_7-5 {
            font-size: ${props.isModBOverlay ? '24px' : '16px'};
            margin-top: ${props.isModBOverlay ? '120px' : '70px'};
            margin-left: ${props.isModBOverlay ? '-110px' : '-80px'};
          }
          @media ${props.theme.mediaQuery.large} {
            .price_point_style_7-0 {
              font-size: ${props.isModBOverlay ? '48px' : '36px'};
              margin-top: ${props.isModBOverlay ? '-55px;' : '-30px'};
            }
            .price_point_style_7-3 {
              font-size: ${props.isModBOverlay ? '152px' : '88px'};
            }
            .price_point_style_7-4{
              font-size: ${props.isModBOverlay ? '48px' : '36px'};
              margin-top: ${props.isModBOverlay ? '40px' : '20px'};
            }
            .price_point_style_7-5 {
              font-size: ${props.isModBOverlay ? '32px' : '24px'};
              margin-top: ${props.isModBOverlay ? '160px' : '90px'};
              margin-left: ${props.isModBOverlay ? '-125px' : '-118px'};
            }
          }
        }
      `;
    default:
      return '';
  }
};
const getPricePointStyles = (props) => {
  const { promoBanner } = props;
  const [{ textItems }] = promoBanner || [{}];

  const styleArray = getStyleArray(textItems);
  const styles = styleArray
    ? Array.isArray(styleArray) &&
      styleArray.map((styleName) => extractPricePointStyle(props, styleName))
    : [];
  return Array.isArray(styles) && styles.join();
};
const PricePoint = css`
  .price-point {
    display: flex;
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    font-family: 'TofinoWide';
    justify-content: center;
  }
  .price-point-rows {
    flex-direction: column;
  }
  .price-point-columns {
    flex-direction: row;
  }
  ${(props) => getPricePointStyles(props)};
`;
export default PricePoint;
