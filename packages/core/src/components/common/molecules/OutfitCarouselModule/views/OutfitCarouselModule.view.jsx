// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isClient } from '@tcp/core/src/utils';
import withStyles from '../../../hoc/withStyles';
import { Anchor, DamImage, BodyCopy } from '../../../atoms';
import styles from '../styles/OutfitCarouselModule.style';
import {
  getIconPath,
  styleOverrideEngine,
  getViewportInfo,
  configureInternalNavigationFromCMSUrl,
} from '../../../../../utils';
import theme from '../../../../../../styles/themes/TCP';
import Carousel from '../../Carousel';

const { breakpoints } = theme;

const CAROUSEL_OPTIONS = {
  autoplay: false,
  arrows: true,
  centerMode: false,
  fade: false,
  speed: 300,
  lazyLoad: false,
  dots: false,
  swipe: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  infinite: true,
  responsive: [
    {
      breakpoint: parseInt(breakpoints.medium, 10) - 1,
      settings: {
        slidesToShow: 2.5,
        slidesToScroll: 1,
        arrows: false,
        swipeToSlide: true,
        lazyLoad: false,
      },
    },
    {
      breakpoint: parseInt(breakpoints.large, 10) - 1,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        swipeToSlide: true,
      },
    },
  ],
};

export class OutfitCarouselModule extends PureComponent {
  static propTypes = {
    data: PropTypes.shape({}),
    className: PropTypes.string,
    moduleClassName: PropTypes.string,
    headLine: PropTypes.shape(
      PropTypes.arrayOf(
        PropTypes.oneOfType(
          PropTypes.shape({
            text: PropTypes.string,
          })
        )
      )
    ).isRequired,
    subHeadLine: PropTypes.shape(
      PropTypes.arrayOf(
        PropTypes.oneOfType(
          PropTypes.shape({
            text: PropTypes.string,
          })
        )
      )
    ).isRequired,
    mediaLinkedList: PropTypes.shape(
      PropTypes.arrayOf(
        PropTypes.oneOfType(
          PropTypes.shape({
            image: PropTypes.shape({}),
            link: PropTypes.shape({}),
          })
        )
      )
    ).isRequired,
    isHomePage: PropTypes.bool.isRequired,
    viaModule: PropTypes.string,
  };

  static defaultProps = {
    data: {},
    className: '',
    moduleClassName: '',
    viaModule: '',
  };

  renderItems = (mediaLinkedList, className, viaModule, itemClassName = '') => {
    const itemProps = itemClassName ? { className: itemClassName } : {};
    return (
      mediaLinkedList &&
      mediaLinkedList.map(({ image, link }, index) => {
        const internalPath = configureInternalNavigationFromCMSUrl(link.url);
        const toPath = `${internalPath}&viaModule=${viaModule}&viaPage=CLP`;
        const asPath = link.url;
        return (
          <div key={`${itemClassName}${index.toString()}`} {...itemProps}>
            <Anchor
              className="image-link"
              to={toPath}
              asPath={asPath}
              dataLocator="dummy-datalocator"
            >
              {image && (
                <DamImage
                  className={`${className} carousel-image`}
                  imgData={{
                    alt: image?.title,
                    url: image?.url,
                  }}
                  imgConfigs={[`w_310`, `w_300`, `w_300`]}
                  loadAuto
                />
              )}
            </Anchor>
            <Anchor
              className="image-link"
              to={toPath}
              asPath={asPath}
              fontSizeVariation="small"
              dataLocator="dummy-datalocator"
            >
              <BodyCopy
                component="pre"
                fontFamily="secondary"
                fontSize="fs14"
                fontWeight="normal"
                lineHeight="14px"
                textAlign="center"
              >
                {link?.text ? `${link.text}  >` : ''}
              </BodyCopy>
            </Anchor>
          </div>
        );
      })
    );
  };

  renderOutfitCarousel = (mediaLinkedList, className, viaModule) => {
    const { isMobile } = isClient() ? getViewportInfo() : { isMobile: false };
    return isMobile ? (
      <div>
        <div className="container-carousel">
          {this.renderItems(mediaLinkedList, className, '', 'carousel-inner-container')}
        </div>
      </div>
    ) : (
      <div>
        <Carousel
          options={CAROUSEL_OPTIONS}
          carouselConfig={{
            autoplay: false,
            customArrowLeft: getIconPath('carousel-big-carrot'),
            customArrowRight: getIconPath('carousel-big-carrot'),
          }}
        >
          {this.renderItems(mediaLinkedList, className, viaModule, '')}
        </Carousel>
      </div>
    );
  };

  render() {
    const { data, className, moduleClassName, viaModule } = this.props;
    let { headLine, subHeadLine, mediaLinkedList } = this.props;
    if (data) {
      const { headLine: hl, subHeadLine: sl, mediaLinkedList: ml } = this.props;
      headLine = hl;
      subHeadLine = sl;
      mediaLinkedList = ml;
    }
    const styleOverrides = styleOverrideEngine(moduleClassName, 'Default');
    return (
      <div className={`${className} content-outfit`}>
        <BodyCopy
          style={styleOverrides.title || {}}
          fontSize={['fs16', 'fs16', 'fs28']}
          fontWeight="black"
          className="heading"
        >
          {headLine && headLine[0].text}
        </BodyCopy>
        <BodyCopy
          fontFamily="secondary"
          style={styleOverrides.subTitle || {}}
          fontSize={['fs15', 'fs15', 'fs22']}
          fontWeight="regular"
          className="subheading"
        >
          {subHeadLine && subHeadLine[0].text}
        </BodyCopy>
        {this.renderOutfitCarousel(mediaLinkedList, className, viaModule)}
      </div>
    );
  }
}
export { OutfitCarouselModule as OutfitCarouselModuleVanilla };
export default withStyles(OutfitCarouselModule, styles);
