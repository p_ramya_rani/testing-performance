// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import {
  configureInternalNavigationFromCMSUrl,
  navigateToPage,
  styleOverrideEngine,
} from '@tcp/core/src/utils';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel';
import { getScreenWidth, getMediaBorderRadius } from '@tcp/core/src/utils/index.native';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';
import colors from '../../../../../../styles/themes/TCP/colors';
import { Container, ImageTouchableOpacity } from '../styles/OutfitCarouselModule.style.native';
import { BodyCopy, DamImage } from '../../../atoms';

const horizontalMargin = 10;
const sliderWidth = getScreenWidth();
const slideWidth = sliderWidth / 2.5 - 10;
const itemWidth = slideWidth + horizontalMargin;
const itemHeight = 304;

const OUTFIT_CAROUSEL_MODULE_WIDTH = itemWidth;

export class OutfitCarouselModule extends PureComponent {
  static propTypes = {
    data: PropTypes.shape({}),
    navigation: PropTypes.shape({}),
    moduleClassName: PropTypes.string,
    headLine: PropTypes.string,
    subHeadLine: PropTypes.string,
    mediaLinkedList: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.shape({}))),
    breadCrumbs: PropTypes.shape({}),
    isHpNewLayoutEnabled: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    data: null,
    navigation: {},
    moduleClassName: '',
    headLine: '',
    subHeadLine: '',
    mediaLinkedList: [],
    breadCrumbs: {},
  };

  getImageUrl = (item) => {
    return get(item, 'image.url', '');
  };

  getImageTitle = (item) => {
    return get(item, 'link.text', '');
  };

  getTitle = () => {
    const { data } = this.props;
    const { headLine } = this.props;
    let text = headLine && headLine[0].text;
    if (data) {
      text = get(data, 'headLine[0].text', '');
    }
    return text;
  };

  getSubTitle = () => {
    const { data } = this.props;
    const { subHeadLine } = this.props;
    let text = subHeadLine && subHeadLine[0].text;
    if (data) {
      text = get(data, 'subHeadLine[0].text', '');
    }
    return text;
  };

  getBannerData = () => {
    const { data } = this.props;
    let { mediaLinkedList: bannerData } = this.props;
    if (data) {
      const mediaLinkedList = get(data, 'mediaLinkedList', null);
      bannerData = mediaLinkedList;
    }
    return bannerData;
  };

  navigateToNextScreen = (item) => {
    const { navigation, breadCrumbs } = this.props;
    const url = get(item, 'link.url', '');
    const cmsValidatedUrl = configureInternalNavigationFromCMSUrl(url);
    const viaModule = this.getCategoryName();
    navigateToPage(cmsValidatedUrl, navigation, {
      viaModule,
      breadCrumbs,
      isForceUpdate: true,
    });
  };

  getCategoryName = () => {
    const { breadCrumbs } = this.props;
    let categoryName = '';

    if (breadCrumbs) {
      if (breadCrumbs.length > 1) {
        categoryName = breadCrumbs[1].displayName;
      } else if (breadCrumbs.length === 1) {
        categoryName = breadCrumbs[0].displayName;
      }
    }
    return categoryName && categoryName.toLowerCase();
  };

  itemRenderer = (itemData) => {
    // const { imageWidth, imageHeight, itemMargin, itemPadding, itemBackgroundColor } = props;
    const { isHpNewLayoutEnabled } = this.props;
    const { index, item } = itemData;
    const imgUrl = this.getImageUrl(item) || '';
    return (
      <ImageTouchableOpacity
        onPress={() => this.navigateToNextScreen(item)}
        accessible
        accessibilityRole="link"
        accessibilityLabel={item && item.image && item.image.alt}
        itemMargin={0}
        key={index.toString()}
        width={slideWidth}
        isHpNewLayoutEnabled={isHpNewLayoutEnabled}
      >
        <DamImage
          key={index.toString()}
          url={imgUrl}
          height={274}
          width={OUTFIT_CAROUSEL_MODULE_WIDTH}
          isFastImage
        />
        <BodyCopy
          width="125"
          margin="16px 0 0 0"
          dataLocator="lbl_promo_jeans_img_title"
          mobileFontFamily="primary"
          fontSize="fs10"
          fontWeight="regular"
          color="gray.900"
          textAlign="center"
          text={this.getImageTitle(item)}
        />
      </ImageTouchableOpacity>
    );
  };

  getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  getContainerStyle = (isHpNewLayoutEnabled, styleOverrides) => {
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    return isHpNewLayoutEnabled
      ? {
          ...HP_NEW_LAYOUT.MODULE_BOX_SHADOW,
          ...mediaBorderRadiusOverride,
          backgroundColor: colors?.WHITE,
        }
      : {};
  };

  render() {
    const { moduleClassName, isHpNewLayoutEnabled } = this.props;
    const bannerData = this.getBannerData();
    const styleOverrides = styleOverrideEngine(moduleClassName, 'Default');
    const titleStyle = styleOverrides.title || {};
    const subTitleStyle = styleOverrides.subTitle || {};

    return (
      <Container style={this.getContainerStyle(isHpNewLayoutEnabled, styleOverrides)}>
        <BodyCopy
          style={titleStyle}
          width="125"
          dataLocator="lbl_promo_jeans_img_title"
          mobileFontFamily="primary"
          fontSize="fs30"
          fontWeight="semibold"
          color="gray.900"
          textAlign="center"
          text={this.getTitle()}
        />
        <BodyCopy
          style={subTitleStyle}
          width="125"
          margin="10px 0 16px 0"
          dataLocator="lbl_promo_jeans_img_title"
          mobileFontFamily="secondary"
          fontSize="fs22"
          fontWeight="regular"
          color="gray.900"
          textAlign="center"
          lineHeight="30px"
          text={this.getSubTitle()}
        />
        <Carousel
          data={bannerData}
          renderItem={this.itemRenderer}
          height={itemHeight}
          sliderWidth={
            isHpNewLayoutEnabled ? sliderWidth - 2 * HP_NEW_LAYOUT.BODY_PADDING : sliderWidth
          }
          itemWidth={itemWidth}
          loop
          activeSlideAlignment="start"
          inactiveSlideOpacity={1}
          autoplay={false}
          options={{
            autoplay: false,
          }}
          sampleTestingText="OutFitCarousel"
          isUseScrollView
          enableMomentum
          enableSnap={false}
        />
      </Container>
    );
  }
}

export { OutfitCarouselModule as OutfitCarouselModuleVanilla };
export default OutfitCarouselModule;
