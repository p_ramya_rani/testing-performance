// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const OutfitCarouselStyle = css`
  .container-carousel {
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
    width: 100%;
    -ms-overflow-style: none;
  }
  .container-carousel::-webkit-scrollbar {
    display: none;
  }
  .carousel-inner-container {
    display: inline-block;
    width: 177px;
  }

  @media ${props => props.theme.mediaQuery.large} {
    margin-left: -15px;
    margin-right: -15px;
  }
  .heading {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
    text-align: center;
  }
  .subheading {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
    text-align: center;
    padding-left: 28px;
    padding-right: 28px;
  }
  .carousel-image {
    margin: 0 auto 16px;
  }
  .slick-slide > div {
    padding: 0 4px;
  }
  .slick-list {
    margin: 0;
    padding-left: 20% !important;
    @media ${props => props.theme.mediaQuery.medium} {
      margin: 0 ${props => props.theme.spacing.ELEM_SPACING.MED};
      padding-left: 0 !important;
    }
  }
  .slick-arrow {
    top: calc(50% - 20px);
  }
  @media ${props => props.theme.mediaQuery.medium} {
    .slick-slide > div {
      padding: 0 12px;
    }
    .slick-prev,
    .slick-next {
      height: 52px;
    }
    .slick-list {
      margin: 0 38px;
    }
    .subheading {
      padding-left: 190px;
      padding-right: 190px;
    }
  }
  @media ${props => props.theme.mediaQuery.large} {
    .slick-slide > div {
      padding: 0 15px;
    }
    .slick-list {
      margin: 0 60px;
    }
    .slick-slider {
      margin: 0 35px;
    }
    .heading {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    }
    .subheading {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
      padding-left: 290px;
      padding-right: 290px;
    }
    .carousel-image {
      margin: 0 auto ${props => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
`;

export default OutfitCarouselStyle;
