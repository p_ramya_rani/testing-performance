// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const getAdditionalStyle = (props) => {
  const { margin, width, slideWidth, paddingHorizontal } = props;
  return {
    ...(margin && { margin }),
    ...(width && { width }),
    ...(slideWidth && { width: slideWidth }),
    ...(paddingHorizontal && { paddingHorizontal }),
  };
};

const Container = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  width: 100%;
  justify-content: center;
`;

const ImageTouchableOpacity = styled.TouchableOpacity`
  height: ${(props) => (props.isHpNewLayoutEnabled ? '318px' : '302px')};
  display: flex;
  justify-content: center;
  align-items: center;
  ${getAdditionalStyle};
`;

export { Container, ImageTouchableOpacity };
