// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import mock from './mock';
import { OutfitCarouselModuleVanilla as OutfitCarouselModule } from '../views/OutfitCarouselModule.view';

describe('OutfitCarouselModule component', () => {
  it('should default variant correctly', () => {
    const wrapper = shallow(<OutfitCarouselModule data={mock.OutfitCarouselModule} />).get(0);
    expect(wrapper).toMatchSnapshot();
  });
});
