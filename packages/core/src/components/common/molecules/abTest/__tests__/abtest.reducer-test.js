// 9fbef606107a605d69c0edbcd8029e5d 
import { SET_AB_TEST_DATA } from '../abTest.constants';
import AbTestReducer from '../abtest.reducer';

const mockAbData = {
  abTest: true,
};

describe('set Ab Test Data', () => {
  it('should trigger SET AB test action', () => {
    const setAbTestData = {
      type: SET_AB_TEST_DATA,
      payload: mockAbData,
    };

    expect(AbTestReducer({}, setAbTestData)).toEqual({});
  });

  it('should return the initial state', () => {
    expect(AbTestReducer(mockAbData, {})).toEqual(mockAbData);
  });
});
