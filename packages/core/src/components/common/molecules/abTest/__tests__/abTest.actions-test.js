// 9fbef606107a605d69c0edbcd8029e5d 
import { FETCH_AB_TEST_DATA, SET_AB_TEST_DATA } from '../abTest.constants';
import { fetchAbTestData, setAbTestData } from '../abTest.actions';

describe('ab Test Actions', () => {
  it('fetch AB test data as ', () => {
    expect(fetchAbTestData().type).toBe(FETCH_AB_TEST_DATA);
  });

  it('set AB Test Data as ', () => {
    expect(setAbTestData().type).toBe(SET_AB_TEST_DATA);
  });
});
