// 9fbef606107a605d69c0edbcd8029e5d
import { put, takeLatest, call } from 'redux-saga/effects';
import AbtestSaga, { fetchAbTestData } from '../abTest.saga';
import { FETCH_AB_TEST_DATA } from '../abTest.constants';
import { setAbTestData } from '../abTest.actions';
import ABTestAbstractors from '../../../../../services/abstractors/common/abTest/abTest';

describe('fetch AB Test Saga', () => {
  it('fetch Ab Test Saga effect with empty payload', () => {
    const payload = {
      quickAdd: false,
    };
    const generator = fetchAbTestData({ payload });
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    const takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(call(ABTestAbstractors.getABData, payload));
  });
  it('fetch Ab Test Saga effect', () => {
    const payload = {
      payload: {
        setAbtestScript: () => {},
        MONETATE_ENABLED: true,
      },
    };
    const generator = fetchAbTestData({ payload });
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    let takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      call(ABTestAbstractors.getABData, { ...payload, quickAdd: false })
    );
    takeLatestDescriptor = generator.next({ abtestScripts: true }).value;
    expect(takeLatestDescriptor).toEqual(put(setAbTestData({ abTest: {}, abTestComplete: true })));
  });
});

describe('AB Test Saga', () => {
  it('Ab Test Saga effect', () => {
    const generator = AbtestSaga();
    let takeLatestDescriptor;
    function expectValue(action, value) {
      takeLatestDescriptor = generator.next().value;
      expect(takeLatestDescriptor).toEqual(takeLatest(action, value));
    }
    expectValue(FETCH_AB_TEST_DATA, fetchAbTestData);
  });
});
