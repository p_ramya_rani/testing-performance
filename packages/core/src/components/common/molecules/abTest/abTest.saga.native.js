// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLatest, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import ABTestAbstractors from '../../../../services/abstractors/common/abTest/abTest';
import { setAbTestData } from './abTest.actions';
import { FETCH_AB_TEST_DATA } from './abTest.constants';
import {
  getPointsToNextReward,
  getLowInventoryMonetateExp,
} from '../../../features/CnC/AddedToBag/container/AddedToBag.selectors';
import { getQuickAddThreshold } from '../../../../reduxStore/selectors/session.selectors';
import { getIsGuest } from '../../../features/account/User/container/User.selectors';

/**
 * @function fetchAbTestData
 * @description This function will call adobe target to get all A/B tests.
 */
function* fetchAbTestData({ payload }) {
  try {
    const pointsToNextReward = yield select(getPointsToNextReward);
    const quickAddThreshold = yield select(getQuickAddThreshold);
    const displayLowInvMonetateExp = yield select(getLowInventoryMonetateExp);
    const isGuest = yield select(getIsGuest);
    const quickAdd = !isGuest && pointsToNextReward < quickAddThreshold;
    const result = yield call(ABTestAbstractors.getAppAbtest, {
      ...payload,
      quickAdd,
      displayLowInvMonetateExp,
    });
    yield put(setAbTestData({ abTest: result }));
  } catch (err) {
    logger.error('fetchAbTestData:', err);
  }
}

/**
 * @function AbtestSaga
 * @description watcher function for getAllAbtest.
 */
function* AbtestSaga() {
  yield takeLatest(FETCH_AB_TEST_DATA, fetchAbTestData);
}

export default AbtestSaga;
