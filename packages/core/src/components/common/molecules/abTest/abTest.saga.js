// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLatest, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { setNonInventoryStagSessionFlag } from '@tcp/core/src/reduxStore/actions';
import { setFBTFlag } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';
import ABTestAbstractors from '../../../../services/abstractors/common/abTest/abTest';
import { setAbTestData } from './abTest.actions';
import { FETCH_AB_TEST_DATA } from './abTest.constants';
import {
  getPointsToNextReward,
  getLowInventoryMonetateExp,
} from '../../../features/CnC/AddedToBag/container/AddedToBag.selectors';
import { getQuickAddThreshold } from '../../../../reduxStore/selectors/session.selectors';
import { getIsGuest } from '../../../features/account/User/container/User.selectors';

/**
 * @function fetchAbTestData
 * @description This function will call adobe target to get all A/B tests.
 */
export function* fetchAbTestData({ payload }) {
  try {
    const pointsToNextReward = yield select(getPointsToNextReward);
    const quickAddThreshold = yield select(getQuickAddThreshold);
    const displayLowInvMonetateExp = yield select(getLowInventoryMonetateExp);
    const isGuest = yield select(getIsGuest);
    const quickAdd = !isGuest && pointsToNextReward < quickAddThreshold;
    const result = yield call(ABTestAbstractors.getABData, {
      ...payload,
      quickAdd,
      displayLowInvMonetateExp,
    });
    const {
      payload: { setAbtestScript },
    } = payload;
    setAbtestScript(result.abtestScripts);
    yield put(setAbTestData({ abTest: result.parsedAbtestResponse || {}, abTestComplete: true }));
    if (result && result.parsedAbtestResponse && result.parsedAbtestResponse.enableFBTabTest) {
      yield put(setFBTFlag());
    }
    if (
      result &&
      result.parsedAbtestResponse &&
      result.parsedAbtestResponse.isNonInventoryABTestEnabled
    ) {
      yield put(setNonInventoryStagSessionFlag(true));
    }
  } catch (err) {
    logger.error('fetchAbTestData: -', {
      error: err,
      extraData: {
        component: 'fetchAbTestData - Saga',
        payloadRecieved: payload,
      },
    });
  }
}

/**
 * @function AbtestSaga
 * @description watcher function for getAllAbtest.
 */
function* AbtestSaga() {
  yield takeLatest(FETCH_AB_TEST_DATA, fetchAbTestData);
}

export default AbtestSaga;
