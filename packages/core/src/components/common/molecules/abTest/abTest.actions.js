// 9fbef606107a605d69c0edbcd8029e5d 
import { FETCH_AB_TEST_DATA, SET_AB_TEST_DATA } from './abTest.constants';

export const fetchAbTestData = payload => {
  return {
    type: FETCH_AB_TEST_DATA,
    payload,
  };
};

export const setAbTestData = payload => {
  return {
    type: SET_AB_TEST_DATA,
    payload,
  };
};

export default { fetchAbTestData, setAbTestData };
