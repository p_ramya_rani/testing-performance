// 9fbef606107a605d69c0edbcd8029e5d 
import { SET_AB_TEST_DATA } from './abTest.constants';

const initialState = {};

const AbTestReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_AB_TEST_DATA:
      return { ...action.payload.abTest, abTestComplete: action.payload.abTestComplete };
    default:
      return state;
  }
};

export default AbTestReducer;
