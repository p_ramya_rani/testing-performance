// 9fbef606107a605d69c0edbcd8029e5d 
import { AB_TEST_STORE_PATTERN } from '@tcp/core/src/constants/reducer.constants';

export const FETCH_AB_TEST_DATA = `${AB_TEST_STORE_PATTERN}FETCH_AB_TEST_DATA`;
export const SET_AB_TEST_DATA = `${AB_TEST_STORE_PATTERN}SET_AB_TEST_DATA`;

export default {
  FETCH_AB_TEST_DATA,
  SET_AB_TEST_DATA,
};
