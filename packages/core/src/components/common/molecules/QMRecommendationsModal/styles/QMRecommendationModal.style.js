// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export const modalStyles = css`
  .close-modal {
    margin-right: 5px;
  }
  div.TCPModal__InnerContent.innerContent {
    padding: 21px;
    overflow-y: auto;
  }
  div.TCPModal__InnerContent {
    text-align: center;
    div > h2.apply-now-heading {
      ::after {
        content: '§';
        display: inline-block;
        height: 10px;
        width: 10px;
        vertical-align: top;
        font-size: ${props => props.theme.fonts.fontSize.heading.large.h6}px;
      }
    }
  }

  .header-image {
    display: initial;
    justify-content: center;
    border: none;
    width: 97px;
    height: auto;
    object-fit: contain;
    margin-bottom: 40px;
  }

  .qm-title {
    line-height: 1;
    margin-bottom: 24px;
  }

  .qm-sub-title {
    margin-bottom: 30px;
    display: block;
  }

  .qm-recommendation-modal > div {
    margin: 0px;
  }

  .qm-recommendation-modal .visit-help-cent {
    padding: 0;
    min-width: 252px;
    width: auto;
    height: 50px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 32px;
  }

  .qm-recommendation-horizontal-line {
    width: 100px;
    margin: 0 auto 24px;
    height: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
    border: none;
    background-color: ${props => props.theme.colors.BRAND.PRIMARY};
  }

  .qm-recommendation-reward .part1 {
    display: inline-flex;
  }

  .qm-recommendation-reward .part2 {
    display: inline-flex;
    margin-left: 15px;
  }
  .qm-recommendation-reward span {
    color: ${props => props.theme.colors.BRAND.PRIMARY};
    font-size: ${props => props.theme.typography.fontSizes.fs64};
    font-weight: 900;
  }

  .qm-recommendation-reward .title {
    font-weight: 200;
    padding-right: 0px;
  }

  .qm-recommendation-reward .dollor {
    font-size: ${props => props.theme.typography.fontSizes.fs36};
    top: 10px;
    position: relative;
    margin-left: 17px;
  }

  .qm-recommendation-reward-parent {
    width: 100%;
    margin: 0 auto 24px;
  }

  .qm-date-pretext,
  .qm-date {
    margin: 10px 2px 16px;
    color: ${props => props.theme.colors.PRIMARY.DARK};
    display: inline-block;
  }
  .qm-desc {
    text-align: left;
    color: ${props => props.theme.colors.PRIMARY.DARK};
    margin: 0 auto 20px;
    display: block;
    width: 90%;
  }

  .QMModal__Content .TCPModal__InnerContent {
    width: 50%;
    max-width: 852px;
    padding: 32px;
    box-sizing: border-box;
  }

  .qm-recommendation-link {
    display: inline-flex;
    justify-content: center;
    margin-bottom: 25px;
  }

  .qm-recommendation-link > div {
    margin: 0 10px;
    width: auto;
  }

  @media ${props => props.theme.mediaQuery.mediumOnly} {
    .QMModal__Content .TCPModal__InnerContent {
      max-width: 460px;
      padding: 32px;
      width: 60%;
    }

    .qm-recommendation-link {
      flex-direction: column;
      margin: 0;
    }

    .qm-recommendation-link a {
      margin-bottom: 24px;
      display: block;
    }

    .qm-title {
      line-height: 1.19;
    }

    .qm-recommendation-reward .part2 {
      margin-left: 0px;
    }
  }
  @media ${props => props.theme.mediaQuery.smallOnly} {
    .QMModal__Content .TCPModal__InnerContent {
      max-height: 460px;
      height: auto;
      bottom: 0;
      top: auto;
      transform: none;
      left: 0;
      width: 100%;
      max-width: 420px;
      padding: 32px 14px;
    }

    .qm-desc {
      width: 100%;
      margin-bottom: 0px;
    }

    .qm-title {
      line-height: 1.36;
      margin-bottom: 16px;
      margin-top: 30px;
    }

    .header-image {
      display: none;
    }

    .qm-recommendation-link {
      flex-direction: column;
      margin: 0;
    }

    .qm-recommendation-link a {
      margin-bottom: 24px;
      display: block;
    }

    .qm-recommendation-modal .visit-help-cent {
      height: 42px;
    }

    .qm-recommendation-reward .part2 {
      margin-left: 0px;
    }

    .qm-sub-title {
      margin-bottom: 24px;
    }

    .qm-recommendation-horizontal-line {
      margin: 0 auto 10px;
    }

    .qm-recommendation-reward-parent {
      margin: 0 auto 15px;
    }
  }
`;

export default modalStyles;
