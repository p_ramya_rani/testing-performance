import React from 'react';
import { shallow } from 'enzyme';
import { routerPush } from '@tcp/core/src/utils';
import { StyledQMRecommendationModalVanilla } from '../QMRecommendationModal/views/QMRecommendationModal.view';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  getBrand: () => 'tcp',
  getImageFilePath: jest.fn(),
}));

describe('QMRecommendation component', () => {
  const props = {
    className: '',
    isModalOpen: true,
    closeModal: jest.fn(),
    labels: {
      guestButtonLink: 'sample',
    },
    isUserLogged: false,
    errorCode: '',
    isShowQMModalGuest: true,
    isShowQMModalLogged: false,
  };
  it('should render correctly', () => {
    const component = shallow(<StyledQMRecommendationModalVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should click toggleModal1', () => {
    const toggleModal = jest.fn();
    const component = shallow(<StyledQMRecommendationModalVanilla {...props} />);
    component.find("[dataLocator='qm__link1_detailslink']").simulate('click');
    toggleModal.mockImplementation();
    expect(props.closeModal).toBeCalled();
  });

  it('should click toggleModal2', () => {
    const toggleModal = jest.fn();
    const component = shallow(<StyledQMRecommendationModalVanilla {...props} />);
    component.find("[dataLocator='qm__link2_detailslink']").simulate('click');
    toggleModal.mockImplementation();
    expect(props.closeModal).toBeCalled();
  });

  it('should click toggleModal3', () => {
    const toggleModal = jest.fn();
    const component = shallow(<StyledQMRecommendationModalVanilla {...props} />);
    component.find("[dataLocator='qm__link3_detailslink']").simulate('click');
    toggleModal.mockImplementation();
    expect(props.closeModal).toBeCalled();
  });

  it('should call onLinkRedirect when isUserLogged is false', () => {
    const mockedRouterPush = jest.fn();
    routerPush.mockImplementation(mockedRouterPush);
    const onLinkRedirect = jest.fn();
    const component = shallow(<StyledQMRecommendationModalVanilla {...props} />);
    component
      .find("[className='visit-help-cent']")
      .simulate('click', { preventDefault: jest.fn() });
    onLinkRedirect.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith('/help-center?pageName=faq', '/help-center/faq');
  });

  it('should call onLinkRedirect when isUserLogged is true', () => {
    const props1 = {
      className: '',
      isModalOpen: true,
      closeModal: jest.fn(),
      labels: {
        guestLink1: 'sample',
        guestLink1Title: 'sample',
        guestButtonLink: 'sample',
      },
      isUserLogged: true,
      errorCode: 'COUPON_ASSIGNED_TO_WALLET',
      isShowQMModalGuest: true,
      isShowQMModalLogged: true,
    };
    const mockedRouterPush = jest.fn();
    routerPush.mockImplementation(mockedRouterPush);
    const onLinkRedirect = jest.fn();
    const component = shallow(<StyledQMRecommendationModalVanilla {...props1} />);
    component
      .find("[className='visit-help-cent']")
      .simulate('click', { preventDefault: jest.fn() });
    onLinkRedirect.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith('/account?id=wallet', '/account/wallet');
  });

  it('should render null compoent when isModalOpen is false', () => {
    const props1 = {
      className: '',
      isModalOpen: false,
      closeModal: jest.fn(),
      labels: {
        guestLink1: 'sample',
        guestLink1Title: 'sample',
        guestButtonLink: 'sample',
      },
      isUserLogged: false,
      errorCode: 'COUPON_ASSIGNED_TO_WALLET',
      isShowQMModalGuest: false,
      isShowQMModalLogged: false,
    };
    const component = shallow(<StyledQMRecommendationModalVanilla {...props1} />);
    expect(component.type()).toEqual(null);
  });
});
