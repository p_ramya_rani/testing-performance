// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import StyledQMRecommendationModal from './QMRecommendationModal.view';
import QMRECOMMENDATION_MODAL_CONSTANTS from '../../container/QMRecommendationModal.constants';

/**
 * @class ApplyNowModalWrapper - Invokes apply plccc node application
 * A Modal will be opened by clicking apply now button
 */

class QMRecommendationModalWrapper extends React.Component {
  componentDidMount() {
    window.invokeQMRecommendationModal = this.openQMmodal;
    return true;
  }

  componentDidUpdate(prevProps) {
    const { fetchUserLoggedInCouponCode, isModalOpen, user, errorCode } = this.props;
    const prev = prevProps.isModalOpen ? 'true' : 'false';
    const cur = isModalOpen ? 'true' : 'false';
    if (user && isModalOpen && errorCode === '' && cur !== prev) {
      const action = {
        page: 'QMRecommendationModal',
      };
      fetchUserLoggedInCouponCode(action);
    }

    if (
      errorCode !== prevProps.errorCode &&
      errorCode === QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_LOAD_SUCCESS_ERROR_CODE
    ) {
      this.openQMmodal();
    }
  }

  openQMmodal = () => {
    const { toggleModal } = this.props;
    toggleModal({ isModalOpen: true });
  };

  closeModal = () => {
    const { toggleModal } = this.props;
    toggleModal({ isModalOpen: false });
  };

  openModal = e => {
    e.preventDefault();
    const { toggleModal } = this.props;
    toggleModal({ isModalOpen: true });
  };

  render() {
    const {
      className,
      labels,
      isModalOpen,
      user,
      errorCode,
      isShowQMModalGuest,
      isShowQMModalLogged,
    } = this.props;
    return (
      <div className={className}>
        <React.Fragment>
          {isModalOpen ? (
            <StyledQMRecommendationModal
              isModalOpen={isModalOpen}
              closeModal={this.closeModal}
              labels={labels}
              isShowQMModalGuest={isShowQMModalGuest}
              isShowQMModalLogged={isShowQMModalLogged}
              isUserLogged={user}
              errorCode={errorCode}
            />
          ) : null}
        </React.Fragment>
      </div>
    );
  }
}

QMRecommendationModalWrapper.propTypes = {
  className: PropTypes.string,
  isModalOpen: PropTypes.bool,
  user: PropTypes.bool,
  isShowQMModalGuest: PropTypes.bool,
  isShowQMModalLogged: PropTypes.bool,
  labels: PropTypes.shape({}),
  errorCode: PropTypes.string,
  toggleModal: PropTypes.func.isRequired,
  fetchUserLoggedInCouponCode: PropTypes.func.isRequired,
};

QMRecommendationModalWrapper.defaultProps = {
  labels: {},
  errorCode: '',
  className: '',
  isModalOpen: false,
  user: false,
  isShowQMModalGuest: false,
  isShowQMModalLogged: false,
};

export default QMRecommendationModalWrapper;
