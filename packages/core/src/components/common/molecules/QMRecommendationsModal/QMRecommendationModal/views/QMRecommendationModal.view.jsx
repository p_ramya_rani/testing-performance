// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import config from '@tcp/core/src/components/features/account/CreateAccount/config';
import internalEndpoints from '@tcp/core/src/components/features/account/common/internalEndpoints';
import { Anchor, BodyCopy, Row, Col, Image, RichText } from '../../../../atoms';
import Modal from '../../../Modal';
import withStyles from '../../../../hoc/withStyles';
import { getBrand, routerPush, getImageFilePath } from '../../../../../../utils';
import styles, { modalStyles } from '../../styles/QMRecommendationModal.style';
import QMRECOMMENDATION_MODAL_CONSTANTS from '../../container/QMRecommendationModal.constants';

/**
 * @constant QMRecommendationModal - Opens a Modal containing modal to open apply plcc modal.
 */

const getGuestLinksColValue = (labels) => {
  let linkWidth = 12;
  if (labels.guestLink1 !== '' && labels.guestLink2 !== '' && labels.guestLink3 !== '') {
    linkWidth = 4;
  } else if (
    (labels.guestLink1 !== '' && labels.guestLink2 !== '') ||
    (labels.guestLink2 !== '' && labels.guestLink3 !== '') ||
    (labels.guestLink1 !== '' && labels.guestLink3 !== '')
  ) {
    linkWidth = 6;
  }
  return linkWidth;
};

const isShowQMModal = (
  isModalOpen,
  isShowQMModalGuest,
  isShowQMModalLogged,
  isUserLogged,
  errorCode,
  closeModal
) => {
  if (
    isUserLogged &&
    errorCode !== QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_LOAD_SUCCESS_ERROR_CODE
  )
    closeModal();

  if (
    isShowQMModalLogged &&
    isModalOpen &&
    isUserLogged &&
    errorCode === QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_LOAD_SUCCESS_ERROR_CODE
  )
    return true;

  if (isShowQMModalGuest && isModalOpen && !isUserLogged) return true;

  return false;
};

const onLinkRedirect = ({ e, link, path, closeModal }) => {
  e.preventDefault();
  routerPush(link, path);
  closeModal();
};

const getSiteSettings = (type, labels = {}) => {
  if (type === 'link')
    return getBrand() === 'tcp' ? labels.userWalletTCPLink : labels.userWalletGYMLink;
  if (type === 'logo') return getBrand() === 'tcp' ? config.tcp : config.gym;
  return '';
};

const toggleModal = (closeModal) => {
  closeModal();
};

const constructLoggedUserContent = (labels, closeModal) => {
  return (
    <Row className="qm-recommendation-modal">
      <Col colSize={{ large: 12, medium: 8, small: 6 }}>
        <Image
          {...getSiteSettings('logo')}
          src={`${getImageFilePath()}/${getSiteSettings('logo').imgSrc}`}
          className="header-image"
        />
      </Col>
      <Col colSize={{ large: 12, medium: 8, small: 6 }}>
        <BodyCopy
          fontFamily="primary"
          fontSize={['fs28', 'fs32', 'fs38']}
          className="qm-title"
          fontWeight="black"
          textAlign="center"
          color="black"
        >
          {labels.userTitle}
        </BodyCopy>
      </Col>
      <Col colSize={{ large: 12, medium: 8, small: 6 }}>
        <BodyCopy
          fontFamily="primary"
          fontSize={['fs16', 'fs18', 'fs18']}
          className="qm-sub-title"
          textAlign="center"
          color="black"
          component="span"
        >
          {labels.userSubTitle}
        </BodyCopy>
      </Col>
      <Col colSize={{ large: 12, medium: 8, small: 6 }}>
        <Row className="qm-recommendation-coupon-code">
          <Col colSize={{ large: 12, medium: 12, small: 12 }}>
            <hr className="qm-recommendation-horizontal-line" />
          </Col>
          <Col
            className="qm-recommendation-reward-parent"
            colSize={{ large: 12, medium: 6, small: 6 }}
          >
            <RichText className="qm-recommendation-reward" richTextHtml={labels.userOfferText} />
          </Col>
          <Col className="copy-to-clipboard-text" colSize={{ large: 12, medium: 12, small: 12 }}>
            <Anchor
              anchorVariation="button"
              buttonVariation="fixed-width"
              title={labels.userWalletBtnTitle}
              className="visit-help-cent"
              fontSize="fs14"
              fill="BLUE"
              centered
              href="#"
              onClick={(e) =>
                onLinkRedirect({
                  e,
                  link: internalEndpoints.myWalletPage.link,
                  path: internalEndpoints.myWalletPage.path,
                  closeModal,
                })
              }
            >
              {labels.userWalletBtnTitle}
            </Anchor>
          </Col>
        </Row>
      </Col>
      {labels.userDescription !== '' && (
        <Col colSize={{ large: 12, medium: 8, small: 6 }}>
          <BodyCopy
            fontFamily="primary"
            fontSize="fs12"
            className="qm-desc"
            textAlign="center"
            color="black"
            component="span"
          >
            {labels.userDescription}
          </BodyCopy>
        </Col>
      )}
    </Row>
  );
};

const constructGuestUserContent = (labels, closeModal) => {
  const linkWidth = getGuestLinksColValue(labels);

  return (
    <Row className="qm-recommendation-modal">
      <Col colSize={{ large: 12, medium: 8, small: 6 }}>
        <Image
          {...getSiteSettings('logo')}
          src={`${getImageFilePath()}/${getSiteSettings('logo').imgSrc}`}
          className="header-image"
        />
      </Col>
      <Col colSize={{ large: 12, medium: 8, small: 6 }}>
        <BodyCopy
          fontFamily="primary"
          fontSize={['fs28', 'fs32', 'fs38']}
          className="qm-title"
          fontWeight="black"
          textAlign="center"
          color="black"
        >
          {labels.guestTitle}
        </BodyCopy>
      </Col>
      <Col colSize={{ large: 12, medium: 8, small: 6 }}>
        <BodyCopy
          fontFamily="primary"
          fontSize={['fs16', 'fs18', 'fs18']}
          className="qm-sub-title"
          textAlign="center"
          color="black"
          component="span"
        >
          {labels.guestSubTitle}
        </BodyCopy>
      </Col>
      <Col colSize={{ large: 12, medium: 8, small: 6 }}>
        {labels.guestButtonLink !== '' && (
          <Anchor
            anchorVariation="button"
            buttonVariation="fixed-width"
            title={labels.guestButtonTitle}
            className="visit-help-cent"
            fontSize="fs14"
            fill="BLUE"
            centered
            href="#"
            onClick={(e) =>
              onLinkRedirect({
                e,
                link: internalEndpoints.helpCenterFAQ.link,
                path: internalEndpoints.helpCenterFAQ.path,
                closeModal,
              })
            }
          >
            {labels.guestButtonTitle}
          </Anchor>
        )}
      </Col>
      <Col colSize={{ large: 12, medium: 12, small: 12 }}>
        <Row className="qm-recommendation-link">
          {labels.guestLink1 !== '' && (
            <Col colSize={{ large: `${linkWidth}`, medium: 12, small: 12 }}>
              <Anchor
                fontSizeVariation="medium"
                underline
                url={labels.guestLink1}
                anchorVariation="primary"
                fontFamily="primary"
                title={labels.guestLink1Title}
                dataLocator="qm__link1_detailslink"
                className="link1-title"
                fontSize="fs14"
                target="_blank"
                onClick={() => toggleModal(closeModal)}
              >
                {labels.guestLink1Title}
              </Anchor>
            </Col>
          )}
          {labels.guestLink2 !== '' && (
            <Col colSize={{ large: `${linkWidth}`, medium: 12, small: 12 }}>
              <Anchor
                fontSizeVariation="medium"
                underline
                url={labels.guestLink2}
                anchorVariation="primary"
                fontFamily="primary"
                title={labels.guestLink2Title}
                dataLocator="qm__link2_detailslink"
                className="link2-title"
                fontSize="fs14"
                target="_blank"
                onClick={() => toggleModal(closeModal)}
              >
                {labels.guestLink2Title}
              </Anchor>
            </Col>
          )}

          {labels.guestLink3 !== '' && (
            <Col colSize={{ large: `${linkWidth}`, medium: 12, small: 12 }}>
              <Anchor
                fontSizeVariation="medium"
                underline
                url={labels.guestLink3}
                anchorVariation="primary"
                fontFamily="primary"
                title={labels.guestLink3Title}
                dataLocator="qm__link3_detailslink"
                className="link3-title"
                fontSize="fs14"
                target="_blank"
                onClick={() => toggleModal(closeModal)}
              >
                {labels.guestLink3Title}
              </Anchor>
            </Col>
          )}
        </Row>
      </Col>
    </Row>
  );
};

const StyledQMRecommendationModal = ({
  className,
  isModalOpen,
  closeModal,
  labels,
  isUserLogged,
  errorCode,
  isShowQMModalGuest,
  isShowQMModalLogged,
}) => {
  const openModal = isShowQMModal(
    isModalOpen,
    isShowQMModalGuest,
    isShowQMModalLogged,
    isUserLogged,
    errorCode,
    closeModal
  );

  return openModal ? (
    <Modal
      fixedWidth
      isOpen={isModalOpen}
      onRequestClose={closeModal}
      overlayClassName="TCPModal__Overlay"
      className={`${className} TCPModal__Content QMModal__Content`}
      standardHeight
      inheritedStyles={modalStyles}
      shouldCloseOnOverlayClick={false}
      innerContentClassName="innerContent"
      contentLabel={labels.userTitle}
    >
      <div className="Modal__Content__Wrapper">
        {isUserLogged
          ? constructLoggedUserContent(labels, closeModal)
          : constructGuestUserContent(labels, closeModal)}
      </div>
    </Modal>
  ) : (
    ''
  );
};

StyledQMRecommendationModal.propTypes = {
  className: PropTypes.string.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  labels: PropTypes.shape({}),
  isUserLogged: PropTypes.bool,
  isShowQMModalGuest: PropTypes.bool,
  isShowQMModalLogged: PropTypes.bool,
  errorCode: PropTypes.string,
};
StyledQMRecommendationModal.defaultProps = {
  labels: {},
  errorCode: '',
  isUserLogged: false,
  isShowQMModalGuest: false,
  isShowQMModalLogged: false,
};

export default withStyles(StyledQMRecommendationModal, styles);
export { StyledQMRecommendationModal as StyledQMRecommendationModalVanilla };
