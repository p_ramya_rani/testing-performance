// 9fbef606107a605d69c0edbcd8029e5d 
import { getAPIConfig } from '@tcp/core/src/utils';
import { executeExternalAPICall } from '@tcp/core/src/services/handler';

const QMRecommendationAbstractor = {
  getUserQMCouponDetails: () => {
    const apiConfigObj = getAPIConfig();
    const payload = {
      webService: {
        method: 'GET',
        URI: `${apiConfigObj.domain}v2/ads_dms/assignCoupon`,
      },
      header: {
        storeId: apiConfigObj.storeId,
        cookie: apiConfigObj.cookie,
      },
    };

    return executeExternalAPICall(payload).then(res => {
      const response = res.body;
      if (!response) {
        throw new Error('res body is null');
        // TODO - Set API Helper to filter if error exists in response
      }
      return { response };
    });
  },
};

export default QMRecommendationAbstractor;
