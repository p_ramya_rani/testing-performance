// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeEvery } from 'redux-saga/effects';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import QMRECOMMENDATION_MODAL_CONSTANTS from './QMRecommendationModal.constants';
import QMRecommendationAbstractor from './QMRecommendationModal';
import { loadUserQMCoupon } from './QMRecommendationModal.actions';
import logger from '../../../../../utils/loggerInstance';

function* fetchUserCouponData(action) {
  const { payload } = action;
  const xappURLConfig = yield call(getUnbxdXappConfigs);
  const reduxKey = 'QMRecommendationModal';
  try {
    const result = yield call(
      QMRecommendationAbstractor.getUserQMCouponDetails,
      payload,
      xappURLConfig
    );
    yield put(
      loadUserQMCoupon({
        reduxKey,
        result,
      })
    );
  } catch (e) {
    logger.log('Error has occurred: ', {
      error: e,
      extraData: {
        component: 'QMRecommendations Saga - fetchUserCouponData',
        payloadRecieved: payload,
      },
    });
  }
}

function* QMRecommendationsModalSaga() {
  yield takeEvery(
    QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_FETCH_USER_CODE,
    fetchUserCouponData
  );
}

export default QMRecommendationsModalSaga;
