// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import QMRECOMMENDATION_MODAL_CONSTANTS from './QMRecommendationModal.constants';

const initialState = fromJS({
  isModalOpen: false,
});

const getDefaultState = state => {
  // TODO: currently when initial state is hydrated on browser, List is getting converted to an JS Array
  if (state instanceof Object) {
    return fromJS(state);
  }
  return state;
};

const QMRecommendationModalReducer = (state = initialState, action) => {
  switch (action.type) {
    case QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_TOGGLE:
      return state.set('modalStatus', action.payload);
    case QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_LOAD_QMCODE:
      return state.set('QMRecommendationModal', action.payload.result.response);
    default:
      return getDefaultState(state);
  }
};

export default QMRecommendationModalReducer;
