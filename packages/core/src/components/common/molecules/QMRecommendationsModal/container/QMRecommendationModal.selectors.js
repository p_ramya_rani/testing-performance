// 9fbef606107a605d69c0edbcd8029e5d 
import { QMRECOMMENDATION_MODAL_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { getLabelValue, getABtestFromState } from '../../../../../utils';

export const getQMRecommendationsModalState = state => {
  return state[QMRECOMMENDATION_MODAL_REDUCER_KEY];
};

export const getIsQMModalOpen = state => {
  const modalStatus = getQMRecommendationsModalState(state).get('modalStatus');
  return modalStatus && modalStatus.isModalOpen;
};

export const getIsShowQMModalGuest = state => {
  return getABtestFromState(state.AbTest).showQMModalGuest;
};

export const getIsShowQMModalLoggedIn = state => {
  return getABtestFromState(state.AbTest).showQMModalLoggedIn;
};

export const getRequestErrorCode = state => {
  const quntamMatrix = getQMRecommendationsModalState(state).get('QMRecommendationModal');
  return quntamMatrix && quntamMatrix.errorCode;
};

export const getLabels = state => {
  return {
    userTitle: getLabelValue(state.Labels, 'lbl_qm_user_title', 'Quantum', 'global'),
    userSubTitle: getLabelValue(state.Labels, 'lbl_qm_user_sub_title', 'Quantum', 'global'),
    userDescription: getLabelValue(state.Labels, 'lbl_qm_user_description', 'Quantum', 'global'),
    userCopyButton: getLabelValue(state.Labels, 'lbl_qm_user_copy_btn_txt', 'Quantum', 'global'),
    userCopiedButton: getLabelValue(
      state.Labels,
      'lbl_qm_user_copied_btn_txt',
      'Quantum',
      'global'
    ),
    userWalletBtnTitle: getLabelValue(
      state.Labels,
      'lbl_qm_user_wallet_btn_title',
      'Quantum',
      'global'
    ),
    userWalletTCPLink: getLabelValue(
      state.Labels,
      'lbl_qm_user_wallet_tcp_link',
      'Quantum',
      'global'
    ),
    userWalletGYMLink: getLabelValue(
      state.Labels,
      'lbl_qm_user_wallet_gym_link',
      'Quantum',
      'global'
    ),
    userQuantamGetText: getLabelValue(state.Labels, 'lbl_qm_user_get_text', 'Quantum', 'global'),
    userQuantamOffText: getLabelValue(state.Labels, 'lbl_qm_user_off_text', 'Quantum', 'global'),
    userOfferValue: getLabelValue(
      state.Labels,
      'lbl_qm_user_offer_amount_value',
      'Quantum',
      'global'
    ),
    userMinAmountForOffer: getLabelValue(
      state.Labels,
      'lbl_qm_user_minimum_amount_for_offer_value',
      'Quantum',
      'global'
    ),
    userOfferText: getLabelValue(state.Labels, 'lbl_qm_user_offer_text', 'Quantum', 'global'),
    userDatePreText: getLabelValue(state.Labels, 'lbl_qm_user_date_pretext', 'Quantum', 'global'),
    guestTitle: getLabelValue(state.Labels, 'lbl_qm_guest_title', 'Quantum', 'global'),
    guestSubTitle: getLabelValue(state.Labels, 'lbl_qm_guest_sub_title', 'Quantum', 'global'),
    guestButtonTitle: getLabelValue(state.Labels, 'lbl_qm_guest_btn_title', 'Quantum', 'global'),
    guestButtonLink: getLabelValue(state.Labels, 'lbl_qm_guest_btn_link', 'Quantum', 'global'),
    guestLink1: getLabelValue(state.Labels, 'lbl_qm_guest_link1', 'Quantum', 'global'),
    guestLink1Title: getLabelValue(state.Labels, 'lbl_qm_guest_link1_title', 'Quantum', 'global'),
    guestLink2: getLabelValue(state.Labels, 'lbl_qm_guest_link2', 'Quantum', 'global'),
    guestLink2Title: getLabelValue(state.Labels, 'lbl_qm_guest_link2_title', 'Quantum', 'global'),
    guestLink3: getLabelValue(state.Labels, 'lbl_qm_guest_link3', 'Quantum', 'global'),
    guestLink3Title: getLabelValue(state.Labels, 'lbl_qm_guest_link3_title', 'Quantum', 'global'),
  };
};
