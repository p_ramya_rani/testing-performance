// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { toggleQMRecommendationModal, fetchUserQMCoupon } from './QMRecommendationModal.actions';

import {
  getLabels,
  getIsQMModalOpen,
  getRequestErrorCode,
  getIsShowQMModalGuest,
  getIsShowQMModalLoggedIn,
} from './QMRecommendationModal.selectors';
import QMRecommendationModalWrapper from '../QMRecommendationModal/views/QMRecommendationWrapper';

export const mapDispatchToProps = dispatch => {
  return {
    toggleModal: payload => {
      dispatch(toggleQMRecommendationModal(payload));
    },
    fetchUserLoggedInCouponCode: payload => {
      dispatch(fetchUserQMCoupon(payload));
    },
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    isModalOpen: getIsQMModalOpen(state),
    labels: getLabels(state),
    labelText: ownProps.labelText,
    user: getUserLoggedInState(state),
    errorCode: getRequestErrorCode(state),
    isShowQMModalGuest: getIsShowQMModalGuest(state),
    isShowQMModalLogged: getIsShowQMModalLoggedIn(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QMRecommendationModalWrapper);
