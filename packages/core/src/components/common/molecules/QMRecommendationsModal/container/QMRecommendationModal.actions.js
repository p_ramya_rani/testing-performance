// 9fbef606107a605d69c0edbcd8029e5d 
import QMRECOMMENDATION_MODAL_CONSTANTS from './QMRecommendationModal.constants';

export const toggleQMRecommendationModal = payload => {
  return {
    payload,
    type: QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_TOGGLE,
  };
};

export const fetchUserQMCoupon = payload => {
  return {
    type: QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_FETCH_USER_CODE,
    payload,
  };
};

export const loadUserQMCoupon = payload => {
  return {
    type: QMRECOMMENDATION_MODAL_CONSTANTS.QMRECOMMENDATION_MODAL_LOAD_QMCODE,
    payload,
  };
};

export default {
  toggleQMRecommendationModal,
  fetchUserQMCoupon,
  loadUserQMCoupon,
};
