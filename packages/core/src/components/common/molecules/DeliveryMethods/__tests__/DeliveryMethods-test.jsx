import React from 'react';

import { shallow } from 'enzyme';

import { DeliveryMethodsVanilla } from '../views/DeliveryMethods.view';

// Skipping this suite due its dependency on the current date, which will
// fail when snapshot is more that day old.
describe('DeliveryMethods component', () => {
  it('renders DeliveryMethods', () => {
    const component = shallow(<DeliveryMethodsVanilla />);

    expect(component).toMatchSnapshot();
  });

  it('renders DeliveryMethods for online exclusive with boss enabled', () => {
    const component = shallow(
      <DeliveryMethodsVanilla
        isOnlineExclusive={true}
        isBossEnabled={true}
        checkForOOSForVariant={true}
      />
    );

    expect(component).toMatchSnapshot();
  });
});
