import { css } from 'styled-components';

const styles = css`
  .delivery-button {
    position: relative;
    margin-right: 12px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    box-sizing: border-box;
    width: 172px;
    height: 60px;
    border-radius: 6px;
    border: solid 1px ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    cursor: pointer;
    &.disable-delivery-methods {
      opacity: 0.5;
      cursor: default;
    }
    .header-pickup-line {
      color: ${(props) => props.theme.colors.PRIMARY.DARK};
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        padding: 0 2px;
      }
    }
    .sub-header-pickup {
      color: ${(props) => props.theme.colors.NOTIFICATION.SUCCESS};
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
    .sub-header-pickup.text-Capitalize {
      text-transform: capitalize;
    }
    .sub-header-pickup.unavailable-sub-header {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      color: ${(props) => props.theme.colors.PRIMARY.DARK};
      font-weight: normal;
    }
    .badge-container {
      position: absolute;
      width: 100%;
      bottom: -9px;
      display: flex;
      flex-direction: row;
      justify-content: center;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        bottom: -8px;
      }
      .badge {
        color: ${(props) => props.theme.colors.PRIMARY.DARK};
        background-color: #fee800;
        display: block;
        text-align: center;
        border-radius: 6px;
      }
      .banner-wrapper {
        padding-left: 0px;
      }
      .triangle-left {
        display: none;
      }
      .promo-wrapper {
        background-color: transparent;
        width: 100%;
        padding-right: 0px;
      }
      .richtextCss {
        margin-left: 0px;
        width: 100%;
      }
      .richtextCss > div > div > div {
        margin-bottom: 2px;
      }
    }

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 101px;
      height: 56px;
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      .sub-header-pickup {
        font-size: 11px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 100%;
      height: 80px;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .badge {
      width: 104px;
    }
  }
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    .badge {
      width: 84px;
    }
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .badge {
      width: 79px;
      font-size: ${(props) => props.theme.typography.fontSizes.fs10};
    }
  }
  .triangle-top {
    display: none;
  }
  .delivery-button.boss {
    margin-right: 0px;
  }

  .delivery-button.selected {
    box-shadow: 0 5px 12px 0 rgba(0, 0, 0, 0.12);
    border: ${(props) =>
      !props.checkForOOSForVariant && `solid 3px ${props.theme.colors.BRAND.PRIMARY}`};
    outline: solid 3px transparent;
    box-sizing: border-box;
    .badge-container {
      bottom: -11px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        bottom: -10px;
      }
    }
  }
  .delivery-section {
    display: flex;
    justify-content: flex-start;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 15px 0px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      justify-content: space-between;
      margin-right: 4px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      justify-content: space-evenly;
      padding: 0px 10px;
    }
  }

  .delivery-button.button-2 {
    width: 264px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 170px;
    }
  }
`;

export default styles;
