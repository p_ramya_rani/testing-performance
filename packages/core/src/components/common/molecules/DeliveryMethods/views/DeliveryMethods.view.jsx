// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getDateInformation, isCanada } from '@tcp/core/src/utils';
import BodyCopy from '../../../atoms/BodyCopy';
import styles from '../styles/DeliveryMethods.style';
import withStyles from '../../../hoc/withStyles';
import DeliveryButton from '../../DeliveryMethodButton/DeliveryButton.view';

const hideBopisCheck = (isOnlineExclusive, isBopisEnabled) => {
  return isOnlineExclusive || !isBopisEnabled;
};
function DeliveryMethods(props) {
  const {
    isPickUpSelected,
    pickupSelectionUpdate,
    className,
    quickViewLabels,
    isOnlineExclusive,
    isBossEnabled,
    checkForOOSForVariant,
    isAllSizeDisabled,
    isBopisEnabled,
    userDefaultStore,
    isStoreBopisEligible,
    itemBrand,
  } = props;
  const hideBopis = hideBopisCheck(isOnlineExclusive, isBopisEnabled);
  const hideBoss = isCanada() || !isBossEnabled;
  const maxBossDate = getDateInformation(
    userDefaultStore?.storeBossInfo?.endDate || userDefaultStore?.features?.bossMaxDate
  );
  const minBossDate = getDateInformation(
    userDefaultStore?.storeBossInfo?.startDate || userDefaultStore?.features?.bossMinDate
  );
  const minMaxBossRangeString = userDefaultStore
    ? `${minBossDate?.month} ${minBossDate?.date} - ${maxBossDate?.month} ${maxBossDate?.date}`
    : '';
  const buttonSize = hideBopis || hideBoss ? 'button-2' : 'button-3';
  if (isOnlineExclusive && !isBossEnabled) {
    return null;
  }
  return (
    <div className={`${className}`}>
      <BodyCopy
        className="fulfillment-section-heading quick-view-drawer-redesign"
        fontSize="fs14"
        fontFamily="secondary"
        fontWeight="extrabold"
      >
        {quickViewLabels?.deliveryMethod}
      </BodyCopy>
      <div className={` delivery-section`}>
        <DeliveryButton
          heading={quickViewLabels?.shipToHome}
          subheading={
            checkForOOSForVariant || isAllSizeDisabled
              ? quickViewLabels?.unavailable
              : quickViewLabels?.freeShipping
          }
          isPickUpSelected={isPickUpSelected === 'home'}
          pickupSelectionUpdate={pickupSelectionUpdate}
          value="home"
          buttonSize={buttonSize}
          checkForOOSForVariant={checkForOOSForVariant}
          isAllSizeDisabled={isAllSizeDisabled}
        />
        {!hideBopis && (
          <DeliveryButton
            heading={quickViewLabels?.pickUp}
            subheading={
              checkForOOSForVariant ? quickViewLabels?.unavailable : quickViewLabels?.getItToday
            }
            isPickUpSelected={isPickUpSelected === 'bopis'}
            pickupSelectionUpdate={pickupSelectionUpdate}
            value="bopis"
            buttonSize={buttonSize}
            checkForOOSForVariant={checkForOOSForVariant}
            isStoreBopisEligible={isStoreBopisEligible}
          />
        )}
        {!hideBoss && (
          <DeliveryButton
            heading={quickViewLabels?.norushpickUp}
            subheading={
              checkForOOSForVariant || isAllSizeDisabled
                ? quickViewLabels?.unavailable
                : minMaxBossRangeString
            }
            badge={quickViewLabels?.extraPercentOff}
            isPickUpSelected={isPickUpSelected === 'boss'}
            pickupSelectionUpdate={pickupSelectionUpdate}
            value="boss"
            buttonSize={buttonSize}
            checkForOOSForVariant={checkForOOSForVariant}
            isAllSizeDisabled={isAllSizeDisabled}
            itemBrand={itemBrand}
          />
        )}
      </div>
    </div>
  );
}

DeliveryMethods.propTypes = {
  className: PropTypes.string,
  itemBrand: PropTypes.string,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  pickupSelectionUpdate: PropTypes.func,
  isPickUpSelected: PropTypes.string,
  isOnlineExclusive: PropTypes.bool,
  isBossEnabled: PropTypes.bool,
  miscInfo: PropTypes.shape({}),
  isAllSizeDisabled: PropTypes.bool,
  checkForOOSForVariant: PropTypes.bool,
  userDefaultStore: PropTypes.shape({}),
};

DeliveryMethods.defaultProps = {
  className: '',
  itemBrand: '',
  isPickUpSelected: '',
  pickupSelectionUpdate: () => {},
  isOnlineExclusive: false,
  isBossEnabled: false,
  miscInfo: {},
  isAllSizeDisabled: false,
  checkForOOSForVariant: false,
  userDefaultStore: {},
};

export default withStyles(DeliveryMethods, styles);
export { DeliveryMethods as DeliveryMethodsVanilla };
