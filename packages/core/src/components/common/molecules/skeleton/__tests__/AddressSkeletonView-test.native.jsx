// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';

import AddressSkeletonView from '../AddressSkeleton.view.native';

describe('SearchBar native should render correctly', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<AddressSkeletonView />);
  });

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
