/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import capitalize from 'lodash/capitalize';
import names from '@tcp/core/src/constants/eventsName.constants';
import { getDynamicBadgeQty } from '@tcp/core/src/utils';
import { IMG_DATA_PLP } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/config';
import { HAPTICFEEDBACK_EVENTS } from '@tcp/core/src/components/common/atoms/hapticFeedback/HapticFeedback.constants';
import Grid from '../../Grid';
import { Button, BodyCopy, DamImage } from '../../../atoms';
import { BUTTON_VARIATION } from '../../../atoms/Button/Button.constants';
import { BTNREDESIGN_VARIATION } from '../../../atoms/ButtonRedesign/ButtonRedesign.constants';
import CustomButton from '../../../atoms/ButtonRedesign';
import withStyles from '../../../hoc/withStyles';
import { getAPIConfig, isAndroid } from '../../../../../utils/index.native';
import SizeChart from '../../ProductAddToBag/molecules/SizeChart/container';
import styles, {
  SelectedValueContainer,
  ProductVariantSelectorStyle,
  SwatchContainer,
  SwatchItemContainer,
  NewDesignContainer,
  ImageSwatchProductContainer,
  ImageSwatchProductDisabledContainer,
  SwatchContainerNew,
  PreviewImageMainContainer,
  PreviewImageContainer,
  PreviewImageSeparator,
  OfferPriceAndFavoriteIconContainer,
  SizeChartLinkWrapper,
} from '../styles/ProductVariantSelector.style.native';
import ErrorDisplay from '../../../atoms/ErrorDisplay';
import LinkImageIcon from '../../../../features/browse/ProductListing/atoms/LinkImageIcon';
import { showSBPLabels } from '../../../../../../../mobileapp/src/components/features/shopByProfile/PLP/helper';
import { getDecimalPrice } from '../../../../../utils/utils';
import { RowContainer } from '../../../../features/browse/ProductDetail/molecules/ProductSummary/styles/ProductSummary.style.native';
import { SINGLE_TEXT } from '../../../../../constants/rum.constants';

const disableSwatches = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/disabled_image_swatches.png');

/**
 * This class returns Product variant selector for Product Add To Bag Page
 * First item is Key: Value pair for Item type and its selected value
 * Second item is the Grid of possible values
 *
 * @class ProductVariantSelector
 * @extends {React.PureComponent}
 */
class ProductVariantSelector extends React.PureComponent {
  /**
   * @function renderGridItem
   * @returns default grid item in flatlist with a button with selection capability
   *
   * @memberof ProductVariantSelector
   */

  constructor(props) {
    super(props);
    this.renderGridItem = this.renderGridItem.bind(this);
    this.handleItemChange = this.handleItemChange.bind(this);
    this.setValue = this.setValue.bind(this);
    this.flatListRef = null;
    this.sizesFlatListRef = null;

    this.state = {
      counter: 0,
      swatchImageURL: '',
      index: 0,
    };
  }

  componentDidMount() {
    this.setState({ counter: 1 });
  }

  componentDidUpdate(prevProps, prevState) {
    const { data } = this.props;
    const { counter, index } = this.state;

    if (counter === 1 && index < data.length && prevState.index !== index) {
      setTimeout(
        () => this.flatListRef && this.flatListRef.scrollToIndex({ index, animated: false }),
        50
      );
    }
  }

  setValue(value) {
    const { input, selectSize } = this.props;
    if (input && input.value.name !== value.name && input.onChange) {
      // if the value to select is not the current value of this component
      // notify our listeners that the user wants the value of this component to change
      input.onChange(value);
      if (selectSize) {
        selectSize(value);
      }
    }
  }

  getImgUrl = (item, swatchImageUrl, isGiftCard, swatchImage) => {
    const giftColor = item && item.color;
    if (isGiftCard && giftColor && giftColor.imagePath) {
      return giftColor.imagePath;
    }
    return swatchImageUrl && `${swatchImageUrl[0]}/${swatchImage}`;
  };

  getColorImageUrlByName = (imagesByColor, name) => {
    return (imagesByColor && imagesByColor[name] && imagesByColor[name].basicImageUrl) || '';
  };

  updateImageColorSwathes = (name, index) => {
    const {
      selectColor,
      comingFromPDPSwatch,
      fireHapticFeedback,
      whileInFullScreen,
      changeProductImageFullScreen,
    } = this.props;
    this.setState((prevState) => ({ counter: prevState.counter + 1 }));
    if (whileInFullScreen) {
      changeProductImageFullScreen(name);
    } else {
      selectColor(name, index);
      comingFromPDPSwatch();
      fireHapticFeedback(HAPTICFEEDBACK_EVENTS.SOFT);
    }
  };

  getSelectedSwatchUrl = (swatchImage, imageName) => {
    const { primaryBrand, alternateBrand } = this.props;
    const configURL = 't_pdp_imgswatch_m,f_auto,q_auto,dpr_3.0';
    const apiConfigObj = getAPIConfig();
    let { brandId } = apiConfigObj;
    if (primaryBrand) {
      brandId = primaryBrand;
    }
    if (alternateBrand) {
      brandId = alternateBrand;
    }
    const brandName = brandId && brandId.toUpperCase();
    const assetHost = apiConfigObj[`assetHost${brandName}`];
    const productAssetPath = apiConfigObj[`productAssetPath${brandName}`];
    let imageSwatchExtension = swatchImage && swatchImage.split('.').pop();
    if (!imageSwatchExtension) {
      imageSwatchExtension = 'jpg';
    }
    const imageId = imageName && imageName.split('_')[0];
    return `${assetHost}/${configURL}/${productAssetPath}/${imageId}/${imageName}.${imageSwatchExtension}`;
  };

  getImagePreviewUrl = (swatchImage, imageName) => {
    let imageSwatchExtension = swatchImage && swatchImage.split('.').pop();
    if (!imageSwatchExtension) {
      imageSwatchExtension = 'jpg';
    }
    const imageId = imageName && imageName.split('_')[0];
    return `${imageId}/${imageName}.${imageSwatchExtension}`;
  };

  renderImageColorSwatches = ({ item: { color, imageName }, index }) => {
    const { keepAlive, formValues } = this.props;
    const { name, swatchImage } = color;
    const isSelected = formValues && formValues.color === name;
    const swatchImageURLData = this.getSelectedSwatchUrl(swatchImage, imageName);
    if (isSelected) {
      const previewImage = this.getImagePreviewUrl(swatchImage, imageName);
      this.setState({ swatchImageURL: previewImage, index });
    }

    const ImageStyle = {
      width: 44,
      height: 54,
    };
    const ImageSelectedStyle = {
      width: 49,
      height: 59,
    };

    return (
      <SwatchItemContainer
        name={name}
        disabled={keepAlive}
        currentSwatch={isSelected}
        height={65}
        width={59}
        index={index}
      >
        <TouchableOpacity
          onPress={() => this.updateImageColorSwathes(name, index)}
          accessibilityRole="image"
          activeOpacity={1}
        >
          <ImageSwatchProductContainer disabled={keepAlive} selected={isSelected}>
            <DamImage
              url={swatchImageURLData}
              resizeMode="stretch"
              isFastImage
              width={isSelected ? ImageSelectedStyle.width : ImageStyle.width}
              height={isSelected ? ImageSelectedStyle.height : ImageStyle.height}
            />
          </ImageSwatchProductContainer>

          {keepAlive && (
            <ImageSwatchProductDisabledContainer>
              <DamImage
                url={disableSwatches}
                resizeMode="stretch"
                isFastImage
                width={isSelected ? ImageSelectedStyle.width : ImageStyle.width}
                height={isSelected ? ImageSelectedStyle.height : ImageStyle.height}
              />
            </ImageSwatchProductDisabledContainer>
          )}
        </TouchableOpacity>
      </SwatchItemContainer>
    );
  };

  renderColor = ({ item, index }) => {
    const {
      color: { name, swatchImage },
    } = item;
    const { selectedColor, isGiftCard, imagesByColor, itemBrand, primaryBrand, alternateBrand } =
      this.props;
    const isSelected = (selectedColor && name === selectedColor.name) || false;
    const borderWidth = 2;
    const componentWidth = isGiftCard ? 103 : 30;
    const componentHeight = isGiftCard ? 128 : 30;
    const imageWidth = isSelected ? componentWidth - borderWidth : componentWidth;
    const imageHeight = isSelected ? componentHeight - borderWidth : componentHeight;

    const swatchImageUrl = swatchImage && swatchImage.split('_');
    const imageUrl = swatchImageUrl
      ? this.getImgUrl(item, swatchImageUrl, isGiftCard, swatchImage)
      : this.getColorImageUrlByName(imagesByColor, name);

    return (
      <LinkImageIcon
        uri={imageUrl}
        selected={isSelected}
        onPress={() => this.updateImageColorSwathes(name, index)}
        width={componentWidth}
        height={componentHeight}
        borderRadius={!isGiftCard ? 15 : 0}
        borderWidth={borderWidth}
        imageWidth={imageWidth}
        imageHeight={imageHeight}
        itemBrand={itemBrand}
        isGiftCard={isGiftCard}
        resizeMode="cover"
        name={name}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
      />
    );
  };

  productCurrentlySelected = (selectedProducts, currentProduct, id, isOutfitPage) => {
    if (id === 'size' && isOutfitPage) {
      let productSelected = false;
      if (selectedProducts?.length > 0) {
        selectedProducts.forEach((product) => {
          if (product.outfitProduct.generalProductId === currentProduct.generalProductId) {
            productSelected = true;
          }
        });
      }
      return productSelected;
    }
    return true;
  };

  renderGridItem = ({ item }) => {
    const {
      selectedItem,
      selectItem,
      itemNameKey,
      isDisableZeroInventoryEntries,
      keepAlive,
      isSBPEnabled,
      sbpSizeClick,
      isOutfitPage,
      selectedProducts,
      currentProduct,
      id,
      isFromBundlePage,
      isFromChangeStore,
      isShowDisabledSize,
    } = this.props;
    const itemValue = item[itemNameKey] ? String(item[itemNameKey]) : '';
    const isSelected =
      ((selectedItem && item[itemNameKey] === selectedItem) || false) &&
      this.productCurrentlySelected(selectedProducts, currentProduct, id, isOutfitPage);
    const { disabled } = item;
    const isDisabled =
      isDisableZeroInventoryEntries && !isFromChangeStore && !isShowDisabledSize ? disabled : false;

    return (
      <Button
        buttonVariation={
          isOutfitPage ? BUTTON_VARIATION.mobileAppRoundedFilter : BUTTON_VARIATION.mobileAppFilter
        }
        text={itemValue.toUpperCase()}
        onPress={() => {
          const value = {
            name: item[itemNameKey],
          };
          this.handleItemChange(value);
          selectItem(item[itemNameKey]);
          if (isSBPEnabled) {
            const payload = {
              clickData: {
                customEvents: ['event28'],
              },
              name: names.screenNames.sbp_size_drop_down_click_pdp,
              module: 'browse',
            };
            sbpSizeClick(payload);
          }
        }}
        selected={!isDisabled && !keepAlive && isSelected}
        data-locator=""
        accessibilityLabel={itemValue}
        disableButton={isDisabled || keepAlive}
        withNoLineHeight
        isFromBundlePage={isFromBundlePage}
      />
    );
  };

  handleNewItemChange = (item) => {
    const { itemNameKey, selectItem } = this.props;
    const value = {
      name: item[itemNameKey],
    };
    this.handleItemChange(value);
    selectItem(item[itemNameKey], item?.disabled);
  };

  getPillMargins = (sizeCharacterLength) => {
    return sizeCharacterLength > 0 ? '14px 0px' : '10px';
  };

  showPillText = (itemValue) => {
    const { currentProduct, isFromFit } = this.props;
    const { isGiftCard } = currentProduct;
    if (isGiftCard) {
      return `$${itemValue}`;
    }
    return isFromFit ? capitalize(itemValue) : itemValue.toUpperCase();
  };

  renderNewGridItem = ({ item, index }) => {
    const {
      selectedItem,
      itemNameKey,
      isDisableZeroInventoryEntries,
      keepAlive,
      isOutfitPage,
      selectedProducts,
      currentProduct,
      id,
      sizeCharacterLength,
      isFromChangeStore,
      isShowDisabledSize,
    } = this.props;
    const itemValue = item[itemNameKey] ? String(item[itemNameKey]) : '';
    const isSelected =
      ((selectedItem && item[itemNameKey] === selectedItem) || false) &&
      this.productCurrentlySelected(selectedProducts, currentProduct, id, isOutfitPage);
    const { disabled } = item;
    const isDisabled =
      isDisableZeroInventoryEntries && !isFromChangeStore && !isShowDisabledSize ? disabled : false;
    const selected = !isDisabled && !keepAlive && isSelected;
    return (
      <CustomButton
        onPress={() => this.handleNewItemChange(item)}
        text={this.showPillText(itemValue)}
        fontSize="fs12"
        fontWeight="regular"
        fontFamily="secondary"
        paddings={this.getPillMargins(sizeCharacterLength)}
        textPadding="0px 5px"
        margin={`0px 5px 6px ${index === 0 ? '16px' : '8px'}`}
        showShadow={selected}
        borderRadius="16px"
        showBorder
        wrapContent={false}
        buttonVariation={BTNREDESIGN_VARIATION.mobileAppIsNewReDesignPlanButtons}
        selected={selected}
        disableButton={isDisabled || keepAlive}
        sizeCharacterLength={sizeCharacterLength}
      />
    );
  };

  checkIfSelectedItemIsAvaiableInData = () => {
    const { data, itemNameKey, selectedItem } = this.props;
    if (!itemNameKey) return true;
    return data && data.filter((item) => item[itemNameKey] === selectedItem).length > 0;
  };

  filterColorVal = (newColorList) => {
    return newColorList && newColorList.length && newColorList.join(' / ');
  };

  getItemVal = (itemValue) => {
    const { currentProduct } = this.props;
    const { isGiftCard } = currentProduct;
    if (isGiftCard && itemValue) return `$${itemValue}`;
    return itemValue ? String(itemValue) : '';
  };

  isUpdatingInputValue = (data, input) =>
    data &&
    data.length === 1 &&
    data[0].displayName &&
    input &&
    input.value &&
    input.value.name !== data[0].displayName;

  showProfileFavoriteColorSBP = (titleValue, isItemAvailable, itemValueStr) => {
    const {
      childProfile,
      showAllColors,
      profileFavoriteColorSection,
      title,
      locators,
      error,
      renderColorItem,
      keyExtractor,
      selectedItem,
      componentWidth,
      separatorWidth,
      data,
      sbpLabels,
    } = this.props;
    const { key, value } = locators || {};
    const allAvailableColorsLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_all_available_color_pdp',
      'All Available Colors'
    );
    return (
      <View {...this.props}>
        {!showAllColors ? (
          <SelectedValueContainer>
            <BodyCopy
              fontWeight="black"
              color="gray.900"
              fontFamily="secondary"
              fontSize="fs14"
              text={titleValue.toUpperCase()}
              dataLocator={key}
            />
            {isItemAvailable ? (
              <BodyCopy
                fontWeight="regular"
                color="gray.900"
                fontFamily="secondary"
                fontSize="fs14"
                text={!childProfile ? '' : itemValueStr.toUpperCase()}
                dataLocator={value}
              />
            ) : null}
          </SelectedValueContainer>
        ) : null}
        {childProfile.favorite_colors && !showAllColors && profileFavoriteColorSection ? (
          <View style={ProductVariantSelectorStyle.view1}>
            <Text style={ProductVariantSelectorStyle.text1}>
              {`${childProfile.name}'s Favorite Colors`}
            </Text>
          </View>
        ) : (
          <View style={ProductVariantSelectorStyle.view2}>
            <Text style={ProductVariantSelectorStyle.text2}>{allAvailableColorsLabel}</Text>
          </View>
        )}
        <Grid
          name={title}
          data={data}
          renderItem={renderColorItem ? this.renderColor : this.renderGridItem}
          keyExtractor={keyExtractor}
          extraData={selectedItem}
          componentWidth={componentWidth}
          separatorWidth={separatorWidth}
        />
        <ErrorDisplay error={error} />
      </View>
    );
  };

  listKeyFunction = (_, index) => index.toString();

  callScrollToIndexFailed = (info) => {
    const wait = new Promise((resolve) => setTimeout(resolve, 500));
    wait.then(() => {
      if (
        this.flatListRef !== null &&
        this.flatListRef.current !== null &&
        typeof this.flatListRef.current.scrollToIndex === 'function'
      ) {
        this.flatListRef.current.scrollToIndex({ index: info.index, animated: true });
      }
    });
  };

  getIndexOfFirstSwatchOnLoad = () => {
    const { data, selectedItem } = this.props;
    let index = null;
    if (data && selectedItem) {
      index = data.findIndex((element) => {
        if (element.color && element.color.name === selectedItem) {
          return true;
        }
        return null;
      });
    }
    return index;
  };

  onLayout = () => {
    const indexVal = this.getIndexOfFirstSwatchOnLoad();
    setTimeout(
      () =>
        indexVal &&
        indexVal > -1 &&
        this.flatListRef &&
        this.flatListRef.scrollToIndex({ index: indexVal, animated: false }),
      100
    );
  };

  renderImageSwatches = () => {
    const { data } = this.props;
    if (data && data.length > 1) {
      return (
        <SwatchContainer onLayout={() => this.onLayout()}>
          <FlatList
            ref={(ref) => {
              this.flatListRef = ref;
            }}
            refreshing={false}
            data={data}
            horizontal
            onScrollToIndexFailed={(info) => this.callScrollToIndexFailed(info)}
            showsHorizontalScrollIndicator={false}
            listKey={(_, index) => this.listKeyFunction(_, index)}
            renderItem={this.renderImageColorSwatches}
          />
        </SwatchContainer>
      );
    }
    return <></>;
  };

  getStylesForView = () => {
    const { isNewReDesignEnabled, style } = this.props;
    if (isNewReDesignEnabled) return ProductVariantSelectorStyle.view3;
    return style;
  };

  resetSizesFlatlist = () => {
    if (!this.sizesFlatListRef) return;
    this.sizesFlatListRef.scrollToIndex({ index: 0, animated: !isAndroid() });
  };

  getStylesForFlatList = () => {
    return {
      paddingBottom: 10,
      height: 65,
    };
  };

  handleItemChange(value) {
    this.setValue(value);
  }

  renderImageSwatchesNew = (data) => {
    if (data && data.length > 1) {
      return (
        <SwatchContainerNew onLayout={() => this.onLayout()}>
          <FlatList
            ref={(ref) => {
              this.flatListRef = ref;
            }}
            refreshing={false}
            data={data}
            horizontal
            onScrollToIndexFailed={(info) => this.callScrollToIndexFailed(info)}
            showsHorizontalScrollIndicator={false}
            listKey={(_, index) => this.listKeyFunction(_, index)}
            renderItem={this.renderImageColorSwatches}
          />
        </SwatchContainerNew>
      );
    }
    return null;
  };

  renderGridValues = () => {
    const {
      title,
      data,
      keyExtractor,
      selectedItem,
      componentWidth,
      separatorWidth,
      renderColorItem,
      isOutfitPage,
      isNewReDesignEnabled,
      isFromChangeStore,
    } = this.props;

    return (
      <>
        {isNewReDesignEnabled || isFromChangeStore ? (
          <NewDesignContainer>
            <FlatList
              ref={(ref) => {
                this.sizesFlatListRef = ref;
              }}
              data={data}
              renderItem={this.renderNewGridItem}
              keyExtractor={(item) => item.id}
              horizontal
              showsHorizontalScrollIndicator={false}
              style={this.getStylesForFlatList()}
            />
          </NewDesignContainer>
        ) : (
          <Grid
            name={title}
            data={data}
            renderItem={renderColorItem ? this.renderColor : this.renderGridItem}
            keyExtractor={keyExtractor}
            extraData={selectedItem}
            componentWidth={componentWidth}
            separatorWidth={separatorWidth}
            isOutfitPage={isOutfitPage}
          />
        )}
      </>
    );
  };

  renderImagePreview = () => {
    const { swatchImageURL } = this.state;
    const { currentProduct, isDynamicBadgeEnabled, primaryBrand, alternateBrand } = this.props;
    const { TCPStyleType, TCPStyleQTY } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );

    return (
      <>
        <PreviewImageMainContainer>
          <DamImage
            url={swatchImageURL}
            isProductImage
            imgConfig={IMG_DATA_PLP.imgConfig}
            height={136}
            width={111}
            resizeMode="restore"
            primaryBrand={primaryBrand || alternateBrand}
            dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
            tcpStyleType={TCPStyleType}
            badgeConfig={IMG_DATA_PLP.badgeConfig}
          />
          <PreviewImageContainer>{this.renderPricesSection()}</PreviewImageContainer>
        </PreviewImageMainContainer>
        <PreviewImageSeparator />
      </>
    );
  };

  renderPricePercentageDiscountLabel = (value) => {
    if (value) {
      return (
        <BodyCopy
          margin="0 0 0 5px"
          dataLocator="plp_filter_size_range"
          mobileFontFamily="secondary"
          fontSize="fs12"
          fontWeight="semibold"
          color="red.500"
          text={value}
          accessibilityLabel={`discount ${value}`}
        />
      );
    }
    return null;
  };

  getListPrice = (showPriceRange, bundleProduct, lowListPriceValue, listPriceValue) =>
    showPriceRange || bundleProduct ? lowListPriceValue : listPriceValue;

  renderPricesSection = () => {
    const { isGiftCard, badge2 } = this.props;
    return (
      <View>
        <OfferPriceAndFavoriteIconContainer>
          {this.renderOfferPrice()}
        </OfferPriceAndFavoriteIconContainer>
        {!isGiftCard && this.renderListPrice(badge2)}
      </View>
    );
  };

  offerPriceData = () => {
    const {
      offerPrice,
      showPriceRange,
      isBundleProduct,
      selectedSize,
      highOfferPrice,
      pdpLabels,
      selectedMultipack,
      currentProduct,
    } = this.props;

    const { TCPMultipackProductMapping, TCPStyleQTY } = currentProduct;

    const offerPricePickValue = offerPrice || currentProduct?.offerPrice;
    const offerPriceValue = this.calculatePriceValue(offerPricePickValue);
    const highOfferPriceValue = this.calculatePriceValue(
      highOfferPrice || currentProduct?.highOfferPrice,
      null
    );

    const text =
      !selectedSize && highOfferPriceValue && (showPriceRange || isBundleProduct)
        ? `${offerPriceValue} - ${highOfferPriceValue}`
        : offerPriceValue;
    const selectedMpackCount = TCPStyleQTY || selectedMultipack;
    const ifSingleText =
      selectedMultipack === pdpLabels?.lblPdpMultipackSingle ? 1 : selectedMpackCount;
    const perUnitStr = pdpLabels && pdpLabels?.lblPdpPerUnit;
    const multiPackSingle = (pdpLabels && pdpLabels?.lblPdpMultipackSingle) || SINGLE_TEXT;

    let singleMultipackCheck;
    if (Number.isNaN(selectedMultipack)) {
      if (selectedMultipack.toLowerCase() === multiPackSingle.toLowerCase()) {
        singleMultipackCheck = true;
      }
    } else {
      singleMultipackCheck = false;
    }

    const showPerPrice =
      selectedMpackCount &&
      String(selectedMpackCount) !== '1' &&
      !singleMultipackCheck &&
      TCPMultipackProductMapping &&
      TCPMultipackProductMapping.length;

    const perPrice = offerPricePickValue / parseInt(ifSingleText, 10).toFixed(2);
    const priceWithDecimal = getDecimalPrice(perPrice);

    return { text, priceWithDecimal, showPerPrice, perUnitStr };
  };

  renderOfferPrice = () => {
    const apiConfigObj = getAPIConfig();
    const { brandId } = apiConfigObj;
    const { text, priceWithDecimal, showPerPrice, perUnitStr } = this.offerPriceData();
    return (
      <RowContainer>
        <BodyCopy
          dataLocator="pdp_current_product_price"
          fontFamily="secondary"
          fontSize="fs22"
          fontWeight="black"
          color={brandId === 'tcp' ? 'red.600' : 'black'}
          text={text}
          margin="0px 0px 0px 5px"
        />
        {this.renderPerItemPrice(priceWithDecimal, showPerPrice, perUnitStr)}
      </RowContainer>
    );
  };

  renderPerItemPrice = (perPrice, showPerPrice, perUnitStr) => {
    const { isPerUnitPriceEnabled } = this.props;
    if (showPerPrice && isPerUnitPriceEnabled) {
      return (
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs13"
          fontWeight="semibold"
          color="gray.800"
          text={`${this.calculatePriceValue(perPrice)}${perUnitStr}`}
        />
      );
    }
    return null;
  };

  renderListPriceDash = (value) => {
    if (value) {
      return (
        <BodyCopy
          dataLocator="pdp_discounted_product_price"
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="semibold"
          color="gray.600"
          text=" - "
        />
      );
    }
    return null;
  };

  renderListPriceLabels = (value) => {
    if (value) {
      return (
        <BodyCopy
          dataLocator="pdp_discounted_product_price"
          margin="0 0 0 5px"
          textDecoration="line-through"
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="semibold"
          color="gray.600"
          text={value}
        />
      );
    }
    return null;
  };

  calculatePriceValue = (price, defaultReturn = 0) => {
    let priceValue = defaultReturn;
    const { currencySymbol, currencyExchange } = this.props;
    const currency = currencySymbol === 'USD' ? '$' : currencySymbol;
    if (price && price > 0) {
      priceValue = `${currency}${(price * currencyExchange).toFixed(2)}`;
    }

    return priceValue;
  };

  renderListPrice = (badge2) => {
    const {
      showPriceRange,
      isBundleProduct,
      selectedSize,
      highListPrice,
      listPrice,
      offerPrice,
      currentProduct,
    } = this.props;
    const listPriceValue = this.calculatePriceValue(listPrice);
    const highPrice = highListPrice || currentProduct.highListPrice;
    const highListPriceValue = this.calculatePriceValue(highPrice, null);

    if (
      listPrice > offerPrice ||
      (showPriceRange && (highListPrice || currentProduct?.highListPrice > listPrice))
    ) {
      return (
        <RowContainer>
          {this.renderListPriceLabels(listPriceValue)}
          {!selectedSize &&
            (showPriceRange || isBundleProduct) &&
            this.renderListPriceDash(highListPriceValue)}
          {!selectedSize &&
            (showPriceRange || isBundleProduct) &&
            this.renderListPriceLabels(highListPriceValue)}
          {this.renderPricePercentageDiscountLabel(badge2)}
        </RowContainer>
      );
    }
    return null;
  };

  renderSizeGuideLink = () => {
    const {
      isSBP,
      sizeChartDetails,
      pdpLabels,
      currentProduct: { categoryPath2Map },
      selectedFit,
      id,
    } = this.props;
    if (id === 'size') {
      return (
        <SizeChartLinkWrapper>
          <SizeChart
            sizeChartDetails={sizeChartDetails}
            isSBP={isSBP}
            isNewReDesignProductSummary={true}
            pdpLabels={pdpLabels}
            selectedFit={selectedFit}
            categoryPath2Map={categoryPath2Map}
          />
        </SizeChartLinkWrapper>
      );
    }
    return null;
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity,complexity
  render() {
    const {
      title,
      itemValue,
      data,
      error,
      locators,
      id,
      input,
      newColorList,
      isSBP,
      isNewReDesignImageSwatches,
      isOutfitPage,
      isNewReDesignEnabled,
      isComingFromPDPSwatch,
      resetSizes,
      isFromPLP,
      isFromOutFit,
      fromPdpQuickDraw,
      isFromChangeStore,
    } = this.props;
    const { key, value } = locators || {};
    const isItemAvailable = this.checkIfSelectedItemIsAvaiableInData();
    const titleValue = isItemAvailable && !isNewReDesignEnabled ? `${title}: ` : `${title} `;
    const itemValueStr = !this.filterColorVal(newColorList)
      ? this.getItemVal(itemValue)
      : this.filterColorVal(newColorList);
    if (!data || data.length === 0) return null;

    if (!isComingFromPDPSwatch) {
      this.setState({ counter: 1 });
    }
    const isPlpData = isFromPLP && data && data.length > 0;
    const hasData = isFromPLP && data ? data.length > 1 : true;

    if (isPlpData && !isFromOutFit) {
      let selectedSwatchImageUrl = '';
      if (data && data.length === 1) {
        const { imageName, color } = data[0];
        selectedSwatchImageUrl =
          color?.swatchImage && this.getImagePreviewUrl(color?.swatchImage, imageName);
      }
      this.setState({ swatchImageURL: selectedSwatchImageUrl });
    }

    if (id === 'fit' && this.isUpdatingInputValue(data, input)) {
      const updatedValue = {
        name: data[0].displayName,
      };
      input.onChange(updatedValue);
    }

    if (isSBP) {
      return this.showProfileFavoriteColorSBP(titleValue, isItemAvailable, itemValueStr);
    }

    if (isNewReDesignImageSwatches) {
      return this.renderImageSwatches();
    }

    if (resetSizes && isNewReDesignEnabled) this.resetSizesFlatlist();

    const showImagePreview = !isFromChangeStore && !fromPdpQuickDraw && isPlpData && !isFromOutFit;
    return (
      <View {...this.props} style={this.getStylesForView()}>
        {showImagePreview && this.renderImagePreview()}
        {hasData && (
          <SelectedValueContainer {...this.props}>
            <BodyCopy
              fontWeight="bold"
              color="gray.900"
              fontFamily="secondary"
              letterSpacing={isNewReDesignEnabled ? 'ls05' : ''}
              fontSize="fs14"
              text={isOutfitPage || isNewReDesignEnabled ? titleValue : capitalize(titleValue)}
              dataLocator={key}
            />
            {isItemAvailable ? (
              <BodyCopy
                fontWeight="semibold"
                color="gray.900"
                fontFamily="secondary"
                fontSize="fs14"
                letterSpacing={isNewReDesignEnabled ? 'ls05' : ''}
                text={
                  isOutfitPage || isNewReDesignEnabled ? itemValueStr : capitalize(itemValueStr)
                }
                dataLocator={value}
              />
            ) : null}
            {this.renderSizeGuideLink()}
          </SelectedValueContainer>
        )}
        {isFromPLP ? this.renderImageSwatchesNew(data) : this.renderGridValues(hasData)}
        <ErrorDisplay error={error} />
      </View>
    );
  }
}

/* PropTypes declaration */
ProductVariantSelector.propTypes = {
  isDynamicBadgeEnabled: PropTypes.shape({}),
  title: PropTypes.string.isRequired,
  itemValue: PropTypes.string,
  data: PropTypes.arrayOf(Object),
  keyExtractor: PropTypes.func,
  selectColor: PropTypes.func,
  selectedItem: PropTypes.string,
  componentWidth: PropTypes.number,
  separatorWidth: PropTypes.number,
  selectItem: PropTypes.func,
  itemNameKey: PropTypes.string,
  selectedColor: PropTypes.string,
  error: PropTypes.string,
  locators: PropTypes.instanceOf(Object),
  input: PropTypes.instanceOf(Object),
  renderColorItem: PropTypes.bool,
  isGiftCard: PropTypes.bool,
  isDisableZeroInventoryEntries: PropTypes.bool,
  keepAlive: PropTypes.bool,
  itemBrand: PropTypes.string.isRequired,
  imagesByColor: PropTypes.shape({}).isRequired,
  sbpSizeClick: PropTypes.func.isRequired,
  selectSize: PropTypes.func,
  id: PropTypes.string,
  newColorList: PropTypes.bool.isRequired,
  childProfile: PropTypes.shape({}),
  isSBP: PropTypes.bool,
  showAllColors: PropTypes.bool,
  profileFavoriteColorSection: PropTypes.bool,
  isSBPEnabled: PropTypes.bool,
  isNewReDesignEnabled: PropTypes.bool.isRequired,
  sbpLabels: PropTypes.shape({}).isRequired,
  isOutfitPage: PropTypes.bool,
  selectedProducts: PropTypes.shape([]),
  currentProduct: PropTypes.shape({
    categoryPath2Map: PropTypes.shape([]),
  }),
  isNewReDesignImageSwatches: PropTypes.bool,
  formValues: PropTypes.shape({}),
  comingFromPDPSwatch: PropTypes.func,
  isComingFromPDPSwatch: PropTypes.bool,
  style: PropTypes.shape({}),
  sizeCharacterLength: PropTypes.number,
  resetSizes: PropTypes.bool,
  fireHapticFeedback: PropTypes.func,
  fromPage: PropTypes.string,
  selectedFit: PropTypes.shape({}),
  pdpLabels: PropTypes.shape({}),
  sizeChartDetails: PropTypes.string,
  isFromChangeStore: PropTypes.bool,
  whileInFullScreen: PropTypes.bool,
  changeProductImageFullScreen: PropTypes.func,
  isShowDisabledSize: PropTypes.bool,
};

ProductVariantSelector.defaultProps = {
  itemValue: '',
  data: [],
  keyExtractor: null,
  selectedItem: null,
  componentWidth: 80,
  separatorWidth: 8,
  selectItem: null,
  itemNameKey: '',
  error: null,
  locators: null,
  input: null,
  selectedColor: '',
  selectColor: null,
  renderColorItem: false,
  isGiftCard: false,
  isDisableZeroInventoryEntries: true,
  keepAlive: false,
  selectSize: null,
  id: '',
  childProfile: {},
  isSBP: false,
  showAllColors: false,
  profileFavoriteColorSection: false,
  isSBPEnabled: false,
  isOutfitPage: false,
  selectedProducts: [],
  currentProduct: {},
  isNewReDesignImageSwatches: false,
  formValues: {},
  comingFromPDPSwatch: () => {},
  isComingFromPDPSwatch: true,
  style: {},
  sizeCharacterLength: 0,
  resetSizes: false,
  fireHapticFeedback: () => {},
  isDynamicBadgeEnabled: {},
  fromPage: '',
  selectedFit: {},
  pdpLabels: {},
  sizeChartDetails: '',
  isFromChangeStore: false,
  whileInFullScreen: false,
  changeProductImageFullScreen: () => {},
  isShowDisabledSize: false,
};

/* export class with styles */
export default withStyles(ProductVariantSelector, styles);

export { ProductVariantSelector as ProductVariantSelectorVanilla };
