// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';

import { ProductVariantSelectorVanilla } from '../views/ProductVariantSelector.native';

describe('ProductVariantSelector native should render correctly', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ProductVariantSelectorVanilla title="color" />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should call render grid', () => {
    wrapper.setProps({ renderColorItem: false, data: [{}], title: 'Color' });
    const Grid = wrapper.find('Grid');
    expect(Grid.length).toBe(1);
  });

  it('should call default render grid item method', () => {
    const item = { name: 'WHITE' };
    const selectItem = jest.fn();
    wrapper.setProps({
      data: [{}],
      title: 'Color',
      selectedItem: item,
      selectItem,
      itemNameKey: 'name',
      itemValue: 'WHITE',
    });
    const Grid = wrapper.find('Grid');
    const gridItem = shallow(Grid.props().renderItem({ item }));
    gridItem.props().onPress();
    expect(selectItem).toHaveBeenCalled();
  });

  it('should return correct margin when char length is greater than 0', () => {
    const argument = 3;
    const result = '14px 0px';
    expect(wrapper.instance().getPillMargins(argument)).toEqual(result);
  });

  it('should return correct margin when char length is 0', () => {
    const argument = 0;
    const result = '10px';
    expect(wrapper.instance().getPillMargins(argument)).toEqual(result);
  });

  it('should return correct copy if product is a gift card', () => {
    const argument = 25;
    wrapper.setProps({
      currentProduct: {
        isGiftCard: true,
      },
    });
    const result = '$25';
    expect(wrapper.instance().showPillText(argument)).toEqual(result);
  });

  it('should return correct copy if product is NOT a gift card', () => {
    const argument = 'xl';
    const result = 'XL';
    expect(wrapper.instance().showPillText(argument)).toEqual(result);
  });

  it('should show correct item value for a gift card', () => {
    const argument = 50;
    wrapper.setProps({
      currentProduct: {
        isGiftCard: true,
      },
    });
    const result = '$50';
    expect(wrapper.instance().getItemVal(argument)).toEqual(result);
  });

  it('should show correct item value if product is NOT a gift card', () => {
    const argument = 'XXL';
    const result = 'XXL';
    expect(wrapper.instance().getItemVal(argument)).toEqual(result);
  });

  it('should get the index of first swatch selected', () => {
    const testData = [
      { color: { name: 'BLUE' } },
      { color: { name: 'RED' } },
      { color: { name: 'GREEN' } },
    ];
    const selectedItem = 'RED';
    wrapper.setProps({
      data: testData,
      selectedItem,
    });
    const result = 1;
    expect(wrapper.instance().getIndexOfFirstSwatchOnLoad()).toEqual(result);
  });

  it('should return correct styling for redesign', () => {
    const result = { marginBottom: 1 };
    wrapper.setProps({
      isNewReDesignEnabled: true,
    });
    expect(wrapper.instance().getStylesForView()).toEqual(result);
  });
});
