// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { StyleSheet } from 'react-native';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import fonts from '@tcp/core/styles/themes/TCP/fonts';
import colors from '../../../../../../styles/themes/TCP/colors';

const colorPallete = createThemeColorPalette();
const nero = colorPallete.text.primary;

const styles = css`
  margin-bottom: 10px;
`;

export const SelectedValueContainer = styled.View`
  flex-direction: row;
  margin-bottom: 12px;
  margin-top: ${(props) => (!props.fromPdpQuickDraw ? '5px' : '0px')};
`;

export const NewDesignContainer = styled.View`
  margin: 0 -16px 0 -16px;
`;

export const ProductVariantSelectorStyle = StyleSheet.create({
  text1: {
    color: nero,
    fontFamily: fonts.secondaryFontExtraBoldFamily,
    fontSize: fonts.fontSize.body.bodytext.copy2,
  },
  text2: {
    color: nero,
    fontFamily: fonts.secondaryFontExtraBoldFamily,
    fontSize: fonts.fontSize.body.bodytext.copy2,
  },
  view1: {
    marginBottom: '3%',
  },
  view2: {
    marginBottom: '3%',
    marginTop: '-3%',
  },
  view3: {
    marginBottom: 1,
  },
});

export const SwatchContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin: 0 -14px;
`;

export const ImageSwatchProductContainer = styled.View`
  ${(props) =>
    props.disabled
      ? `position: absolute;
      opacity: ${props.theme.opacity.opacity.medium};
      `
      : ``};
  justify-content: center;
  align-self: center;

  ${(props) =>
    !props.selected
      ? `margin-top: 5px;
      `
      : ``};
`;

export const ImageSwatchProductDisabledContainer = styled.View``;

export const SwatchItemContainer = styled.View`
  ${(props) =>
    props.currentSwatch
      ? `
      border: solid 3px ${
        props.theme.isGymboree
          ? props.theme.colors.DEFAULT_ACCENT_GYM
          : props.theme.colorPalette.blue[500]
      };
      margin: ${props.theme.spacing.APP_LAYOUT_SPACING.XXS} 0 ;
      margin-left: ${props.theme.spacing.APP_LAYOUT_SPACING.XXS};
      background-color: ${props.theme.colors.WHITE};
      box-shadow: ${props.theme.shadow.NEW_DESIGN.BOX_SHADOW}
      border-radius: ${props.theme.spacing.ELEM_SPACING.XS};
      elevation: 5;
      overflow:hidden;
    `
      : `
      margin: ${props.theme.spacing.APP_LAYOUT_SPACING.XXS} 0 ${props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    `}
  margin-left: ${(props) =>
    props.index === 0
      ? props.theme.spacing.ELEM_SPACING.REG
      : props.theme.spacing.ELEM_SPACING.XXS};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

export const SwatchContainerNew = styled.View`
  margin: -12px -14px;
`;

export const PreviewImageMainContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const PreviewImageContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin: 6px;
`;

export const PreviewImageSeparator = styled.View`
  height: 1px;
  background-color: ${colors.PRIMARY.PALEGRAY};
  margin: 16px;
`;

export const OfferPriceAndFavoriteIconContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const RowContainer = styled.View`
  flex-direction: row;
`;

export const SizeChartLinkWrapper = styled.View`
  right: 0;
  position: absolute;
`;

export default styles;
