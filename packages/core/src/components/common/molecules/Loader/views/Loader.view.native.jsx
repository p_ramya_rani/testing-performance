// 9fbef606107a605d69c0edbcd8029e5d
import { Modal } from 'react-native';
import Spinner from '@tcp/core/src/components/common/atoms/Spinner';
import { isAndroid } from '@tcp/core/src/utils';
import React from 'react';
import PropTypes from 'prop-types';
import SpinnerWrapper from '../Loader.style.native';

const SpinnerBody = () => (
  <SpinnerWrapper>
    <Spinner />
  </SpinnerWrapper>
);

const Loader = (props) => {
  const { loaderState, isSplashVisible, quickViewLoader } = props;
  if (quickViewLoader) return null;
  if (loaderState && !isSplashVisible) {
    return isAndroid() ? (
      <Modal transparent visible>
        <SpinnerBody />
      </Modal>
    ) : (
      <SpinnerBody />
    );
  }
  return null;
};
Loader.propTypes = {
  loaderState: PropTypes.bool,
  isSplashVisible: PropTypes.bool,
  quickViewLoader: PropTypes.bool,
};

Loader.defaultProps = {
  loaderState: false,
  isSplashVisible: false,
  quickViewLoader: false,
};

export default Loader;
export { Loader as LoaderVanilla };
