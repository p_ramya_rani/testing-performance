// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import LoaderView from '../views';
import { getQuickViewLoader } from './Loader.selector';

const mapStateToProps = (state) => {
  const { PageLoader } = state;
  return {
    loaderState: PageLoader && PageLoader.loaderState,
    miniBagLoaderState: PageLoader && PageLoader.miniBagLoaderState,
    addedToBagLoaderState: PageLoader && PageLoader.addedToBagLoaderState,
    addedToBagGhostLoaderState: PageLoader && PageLoader.addedToBagGhostLoaderState,
    quickViewLoader: getQuickViewLoader(state),
  };
};

export default connect(mapStateToProps, {})(LoaderView);
