export const getQuickViewLoader = (state) => {
  return state?.PageLoader?.quickViewLoader;
};

export default { getQuickViewLoader };
