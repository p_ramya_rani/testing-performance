// 9fbef606107a605d69c0edbcd8029e5d
import LOADER_CONSTANTS from '../Loader.constants';

const INITIAL_STATE = {
  loaderState: false,
  miniBagLoaderState: false,
  addedToBagLoaderState: false,
  isExternalCampaignFired: false,
  loyaltyAPICallStatus: false,
  aTbAPICallStatus: false,
  quickViewLoader: false,
  shippingLoader: false,
  billingToReviewLoader: false,
  checkoutLoader: false,
};

const LoaderReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOADER_CONSTANTS.SET_LOADER_STATE:
      return { ...state, loaderState: action.payload };
    case LOADER_CONSTANTS.SET_LOYALTY_API_CALL_STATUS:
      return { ...state, loyaltyAPICallStatus: action.payload };
    case LOADER_CONSTANTS.SET_ATB_API_CALL_STATUS:
      return { ...state, aTbAPICallStatus: action.payload };
    case LOADER_CONSTANTS.SET_SECTION_LOADER_STATE: {
      if (action.payload.section === 'minibag') {
        return { ...state, miniBagLoaderState: action.payload.miniBagLoaderState };
      }
      if (action.payload.section === 'addedtobag') {
        return { ...state, addedToBagLoaderState: action.payload.addedToBagLoaderState };
      }
      if (action.payload.section === 'shippingLoader') {
        return { ...state, shippingLoader: action.payload.shippingLoader };
      }
      if (action.payload.section === 'billingToReview') {
        return { ...state, billingToReviewLoader: action.payload.billingToReviewLoader };
      }
      if (action.payload.section === 'checkout') {
        return { ...state, checkoutLoader: action.payload.checkoutLoader };
      }
      return state;
    }
    case LOADER_CONSTANTS.SET_EXTERNAL_CAMPAIGN_STATE:
      return { ...state, isExternalCampaignFired: action.payload };
    case LOADER_CONSTANTS.SET_GHOST_LOADER_STATE:
      return { ...state, addedToBagGhostLoaderState: action.payload.addedToBagLoaderState };
    case LOADER_CONSTANTS.SET_GHOST_LOADER_STATE_COMPLETE:
      return {
        ...state,
        addedToBagGhostLoaderCompleteState: action.payload.addedToBagLoaderCompleteState,
      };
    case LOADER_CONSTANTS.SET_LOADER_QUICK_VIEW:
      return {
        ...state,
        quickViewLoader: action.payload,
      };
    default:
      return state;
  }
};

export default LoaderReducer;
