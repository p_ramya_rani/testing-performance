// 9fbef606107a605d69c0edbcd8029e5d
import LOADER_CONSTANTS from '../Loader.constants';

export const setLoaderState = (payload) => {
  return {
    payload,
    type: LOADER_CONSTANTS.SET_LOADER_STATE,
  };
};

export const setLoyaltyApiCallStatus = (payload) => {
  return {
    payload,
    type: LOADER_CONSTANTS.SET_LOYALTY_API_CALL_STATUS,
  };
};

export const setATBApiCallStatus = (payload) => {
  return {
    payload,
    type: LOADER_CONSTANTS.SET_ATB_API_CALL_STATUS,
  };
};

export const setSectionLoaderState = (payload) => {
  return {
    payload,
    type: LOADER_CONSTANTS.SET_SECTION_LOADER_STATE,
  };
};

export const setGhostLoaderState = (payload) => {
  return {
    payload,
    type: LOADER_CONSTANTS.SET_GHOST_LOADER_STATE,
  };
};

export const setGhostLoaderStateComplete = (payload) => {
  return {
    payload,
    type: LOADER_CONSTANTS.SET_GHOST_LOADER_STATE_COMPLETE,
  };
};

export const setExternalCampaignState = (payload) => {
  return {
    payload,
    type: LOADER_CONSTANTS.SET_EXTERNAL_CAMPAIGN_STATE,
  };
};

export const setLoaderForQuickView = (payload) => {
  return {
    type: LOADER_CONSTANTS.SET_LOADER_QUICK_VIEW,
    payload,
  };
};

export default setLoaderState;
