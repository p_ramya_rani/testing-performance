// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils';
import withStyles from '../../../hoc/withStyles';
import { RichText } from '../../../atoms';
import CONSTANTS from '../../../../features/CnC/Checkout/Checkout.constants';
import {
  FullBleedBannerStyle,
  TriangleBanner,
  LeftTriangle,
  TriangleBannerText,
  TopTriangle,
  TriangleBannerTextNew,
} from '../styles/PromotionBanner.style.native';

/**
 *
 * @function modifiedBannerText
 * @description this method replcae label's dynamic value of tcpsegment with another respective label.
 * @param {*} label
 * @returns
 * @memberof PromotionBanner
 */
export const modifiedBannerText = (label, props) => {
  const { bossBanner, labels, tcpSegmentValue, itemBrand } = props;
  const brandName = itemBrand && itemBrand.toLowerCase();
  const pickupType = bossBanner
    ? CONSTANTS.ORDER_ITEM_TYPE.BOSS.toLowerCase()
    : CONSTANTS.ORDER_ITEM_TYPE.BOPIS.toLowerCase();
  const labelKey =
    label &&
    label.replace(
      /\$tcpSegmentValue\$/,
      labels[`lbl_banner_${pickupType}_disc_${brandName}_${tcpSegmentValue}`]
        ? labels[`lbl_banner_${pickupType}_disc_${brandName}_${tcpSegmentValue}`]
        : labels[`lbl_banner_${pickupType}_disc_${brandName}_default`]
    );
  return getLabelValue(labels, labelKey);
};

const PromotionBanner = (props) => {
  const { labels, fullBleed, isPickupMobilePromotion, isfromNewdesign } = props;
  if (isfromNewdesign && labels.lbl_banner_boss_text) {
    return (
      <TriangleBannerTextNew>
        <RichText
          source={{
            html: modifiedBannerText(labels.lbl_banner_boss_text, props),
          }}
        />
      </TriangleBannerTextNew>
    );
  }

  return (
    <>
      {fullBleed ? (
        <>
          {labels.lbl_fullBleed_banner_boss_text ? (
            <FullBleedBannerStyle>
              <RichText
                source={{
                  html: modifiedBannerText(labels.lbl_fullBleed_banner_boss_text, props),
                }}
              />
            </FullBleedBannerStyle>
          ) : null}
        </>
      ) : (
        <>
          {labels.lbl_banner_boss_text ? (
            <TriangleBanner>
              {!isPickupMobilePromotion ? <LeftTriangle /> : <TopTriangle />}
              <TriangleBannerText>
                <RichText
                  source={{
                    html: modifiedBannerText(labels.lbl_banner_boss_text, props),
                  }}
                />
              </TriangleBannerText>
            </TriangleBanner>
          ) : null}
        </>
      )}
    </>
  );
};

PromotionBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  fullBleed: PropTypes.bool,
  isPickupMobilePromotion: PropTypes.bool,
  isfromNewdesign: PropTypes.bool,
};

PromotionBanner.defaultProps = {
  fullBleed: false,
  isPickupMobilePromotion: false,
  isfromNewdesign: false,
};

export default withStyles(PromotionBanner);
export { PromotionBanner as PromotionBannerVanilla };
