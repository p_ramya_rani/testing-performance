// 9fbef606107a605d69c0edbcd8029e5d 
const getPickupPromotionBannerLabels = state => {
  const labelValue = state.Labels && state.Labels.global && state.Labels.global.bossPromotions;
  return labelValue || {};
};

export default {
  getPickupPromotionBannerLabels,
};
