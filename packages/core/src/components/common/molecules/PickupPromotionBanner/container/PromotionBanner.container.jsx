// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import promotionBannerSelectors from './PromotionBanner.selectors';
import PromotionBanner from '../views/PromotionBanner.view';
import {
  getTcpSegmentValue,
  getIsBossEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import { getBrand, isCanada } from '../../../../../utils';

export const PromotionBannerContainer = ({
  labels,
  tcpSegmentValue,
  bossBanner,
  fullBleed,
  itemBrand = getBrand(),
  isPickupMobilePromotion,
  isCountryCanada,
  isBossEnabled,
  isATBModalBackAbTestNewDesign,
  isfromNewdesign,
}) =>
  !isCountryCanada && isBossEnabled ? (
    <PromotionBanner
      labels={labels}
      isfromNewdesign={isfromNewdesign}
      tcpSegmentValue={tcpSegmentValue}
      bossBanner={bossBanner}
      fullBleed={fullBleed}
      itemBrand={itemBrand}
      isPickupMobilePromotion={isPickupMobilePromotion}
      isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
    />
  ) : null;

export const mapStateToProps = (state) => {
  return {
    labels: promotionBannerSelectors.getPickupPromotionBannerLabels(state),
    tcpSegmentValue: getTcpSegmentValue(state),
    isCountryCanada: isCanada(),
    isBossEnabled: getIsBossEnabled(state),
  };
};

PromotionBannerContainer.propTypes = {
  bossBanner: PropTypes.bool,
  labels: PropTypes.shape({}).isRequired,
  fullBleed: PropTypes.bool,
  tcpSegmentValue: PropTypes.string.isRequired,
  itemBrand: PropTypes.string.isRequired,
  isPickupMobilePromotion: PropTypes.bool,
  isCountryCanada: PropTypes.bool,
  isBossEnabled: PropTypes.bool,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
};

PromotionBannerContainer.defaultProps = {
  bossBanner: false,
  fullBleed: false,
  isPickupMobilePromotion: false,
  isCountryCanada: false,
  isBossEnabled: true,
  isATBModalBackAbTestNewDesign: false,
};
export default connect(mapStateToProps)(PromotionBannerContainer);
