// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import APPLY_NOW_MODAL_CONSTANTS from './ApplyNowModal.constants';

const initialState = fromJS({
  isModalOpen: false,
});

const ApplyNowModalPLCCReducer = (state = initialState, action) => {
  if (action.type === APPLY_NOW_MODAL_CONSTANTS.APPLY_NOW_MODAL_TOGGLE) {
    return state.set('modalStatus', action.payload);
  }
  if (state instanceof Object) {
    return fromJS(state);
  }
  return state;
};

export default ApplyNowModalPLCCReducer;
