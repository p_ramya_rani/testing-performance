// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';

import { getCreateAccountAfterGuestUserApplyForPLCCCard } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { toggleApplyNowModal } from './ApplyNowModal.actions';
import {
  getLabels,
  getIsModalOpen,
  getIsPLCCModalOpen,
  getRtpsMessages,
} from './ApplyNowModal.selectors';
import CheckoutSelectors from '../../../../features/CnC/Checkout/container/Checkout.selector';
import CHECKOUT_ACTIONS from '../../../../features/CnC/Checkout/container/Checkout.action';
import ApplyNowModalWrapper from '../views';
import BagPageSelectors from '../../../../features/CnC/BagPage/container/BagPage.selectors';
import {
  resetPLCCResponse,
  fetchModuleX,
} from '../../../../features/browse/ApplyCardPage/container/ApplyCard.actions';
import { trackPageView, setClickAnalyticsData } from '../../../../../analytics/actions';

export const mapDispatchToProps = (dispatch) => {
  return {
    toggleModal: (payload) => {
      dispatch(toggleApplyNowModal(payload));
    },
    resetPLCCApplicationStatus: (payload) => {
      dispatch(resetPLCCResponse(payload));
    },
    fetchModuleXContent: (payload) => {
      dispatch(fetchModuleX(payload));
    },
    setIsRTPSFlow: (payload) => {
      dispatch(CHECKOUT_ACTIONS.setIsRTPSFlow(payload));
    },
    submitAcceptOrDeclinePlcc: (payload) => {
      dispatch(CHECKOUT_ACTIONS.submitAcceptOrDeclinePlccOffer(payload));
    },
    trackPageLoad: (payload) => {
      const { customEvents, ...restPayload } = payload;

      dispatch(
        setClickAnalyticsData({
          customEvents,
        })
      );
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...restPayload,
                },
              },
            },
          },
        })
      );

      const analyticsTimer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(analyticsTimer);
      }, 300);
    },
  };
};

const mapStateToProps = (state, ownProps) => {
  const { ApplyCardPage = {} } = state;
  const { navigation } = ownProps;
  const applyCardLabels = getRtpsMessages(state);
  const { rtpsCongratsMsg, rtpsOptOutMsg, rtpsTextTerms, rtpsTop, wicTop } = applyCardLabels;
  const isPlccOpen = navigation && navigation.getParam('isPlccModalOpen');

  return {
    isModalOpen: getIsModalOpen(state),
    labels: getLabels(state),
    isPLCCModalOpen: isPlccOpen || getIsPLCCModalOpen(state),
    plccBenefitsList: ApplyCardPage.plccData && ApplyCardPage.plccData.plcc_rewards_list,
    rtpsCongratsMsg,
    rtpsOptOutMsg,
    rtpsTextTerms,
    rtpsTop,
    wicTop,
    labelText: ownProps.labelText,
    isRtpsFlow: CheckoutSelectors.getIsRtpsFlow(state),
    cartOrderItems: BagPageSelectors.getOrderItems(state),
    isNewPLCCCardFlow: getCreateAccountAfterGuestUserApplyForPLCCCard(state),
    isCreateAdsAccountFailed: ApplyCardPage && ApplyCardPage.isCreateAdsAccountFailed,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplyNowModalWrapper);
