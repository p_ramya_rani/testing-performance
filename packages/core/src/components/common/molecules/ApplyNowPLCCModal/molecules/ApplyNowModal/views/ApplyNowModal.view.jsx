// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  getLearnMorePayload,
  getApplyOrAcceptPayload,
  getPLCCNoThanksPayload,
} from '@tcp/core/src/constants/analytics';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { RichText, Anchor, BodyCopy, Button, Row, Col, Image } from '../../../../../atoms';
import Modal from '../../../../Modal';
import withStyles from '../../../../../hoc/withStyles';
import { getLocator } from '../../../../../../../utils';
import styles, { modalStyles } from '../../../styles/ApplyNowModal.style';
import ApplyNowPLCCModal from '../../ApplyNowPLCCModal/views/ApplyNowPLCCModal';

/**
 * @constant ApplyNowModal - Opens a Modal containing modal to open apply plcc modal.
 */
const StyledApplyNowModal = ({
  className,
  isModalOpen,
  closePLCCModal,
  isPLCCModalOpen,
  openPLCCModal,
  closeModal,
  labels,
  plccBenefitsList,
  isRtpsFlow,
  rtpsOptOutMsg,
  rtpsTop,
  wicTop,
  rtpsTextTerms,
  isCreateAdsAccountFailed,
  isNewPLCCCardFlow,
}) => {
  const rewardImageUrl = () => {
    return getLabelValue(labels, 'lbl_PLCCModal_oneEqualsTwoPoints')
      ? getLabelValue(labels, 'lbl_PLCCForm_double_reward')
      : getLabelValue(labels, 'lbl_PLCCForm_single_reward');
  };
  return !isPLCCModalOpen ? (
    <Modal
      fixedWidth
      isOpen={isModalOpen}
      onRequestClose={closeModal}
      overlayClassName="TCPModal__Overlay"
      className={`${className} TCPModal__Content`}
      dataLocator={getLocator('plcc_apply_now_modal')}
      dataLocatorHeader={getLocator('plcc_apply_now_close_btn')}
      maxWidth="724px"
      standardHeight
      inheritedStyles={modalStyles}
      shouldCloseOnOverlayClick={false}
      innerContentClassName="innerContent"
      shouldCloseOnEsc={!isRtpsFlow}
      contentLabel={getLabelValue(labels, 'lbl_PLCCModal_applyNowHeaderText')}
    >
      <div className="Modal__Content__Wrapper">
        <Row fullBleed className="submit_plcc_form">
          <Col ignoreGutter={{ small: true }} colSize={{ large: 12, medium: 8, small: 6 }}>
            <RichText className="rtpsTop" richTextHtml={isRtpsFlow ? rtpsTop : wicTop} />
          </Col>
        </Row>

        <Row fullBleed className="ApplyNow__link__Wrapper">
          <Col ignoreGutter={{ small: true }} colSize={{ large: 12, medium: 8, small: 6 }}>
            <ClickTracker isLoyaltyClick clickData={getApplyOrAcceptPayload(null)}>
              <Button
                buttonVariation="fixed-width"
                fill="BLUE"
                type="submit"
                className="ApplyNow__link"
                onClick={openPLCCModal}
                data-locator={getLocator('plcc_apply_btn')}
              >
                {!isRtpsFlow
                  ? getLabelValue(labels, 'lbl_PLCCModal_applyNowCTA')
                  : getLabelValue(labels, 'lbl_PLCC_interested')}
              </Button>
            </ClickTracker>
          </Col>
        </Row>
        <Row className="learn_more_link_wrapper">
          {!isRtpsFlow ? (
            <Col
              ignoreGutter={{ small: true }}
              colSize={{ large: 10, medium: 8, small: 6 }}
              data-locator="plcc_modal_learn_more_link"
              aria-label="learn_more_link"
              className="learn_more_link"
            >
              <Anchor
                url={getLabelValue(labels, 'lbl_PLCCModal_learnMoreLink')}
                fontSizeVariation="large"
                anchorVariation="secondary"
                underline
                target="_blank"
              >
                <ClickTracker isLoyaltyClick clickData={getLearnMorePayload(null)}>
                  {getLabelValue(labels, 'lbl_PLCCModal_learnMoreText')}
                </ClickTracker>
              </Anchor>
            </Col>
          ) : (
            <Col
              ignoreGutter={{ small: true }}
              colSize={{ large: 10, medium: 8, small: 6 }}
              data-locator="plcc_modal_no_thanks_link"
              aria-label="no_thanks_link"
              className="learn_more_link"
            >
              <Anchor
                fontSizeVariation="large"
                anchorVariation="secondary"
                underline
                noLink
                target="_blank"
                handleLinkClick={(e) => {
                  e.preventDefault();
                  closeModal();
                }}
              >
                <ClickTracker isLoyaltyClick clickData={getPLCCNoThanksPayload(null)}>
                  {getLabelValue(labels, 'lbl_PLCC_noThanks')}
                </ClickTracker>
              </Anchor>
            </Col>
          )}
        </Row>
        {isRtpsFlow && (
          <Row>
            <Col colSize={{ large: 12, medium: 8, small: 6 }}>
              <RichText className="opt_out_msg" richTextHtml={rtpsOptOutMsg} />
            </Col>
          </Row>
        )}
        <div className="separator" />
        <Image
          className="offer_info_icon"
          data-locator="plcc_modal_logo"
          url={rewardImageUrl()}
          offerType={getLabelValue(labels, 'lbl_PLCCModal_oneEqualsTwoPoints')}
        />
        <BodyCopy
          fontFamily="primary"
          fontSize="fs28"
          className="benefits-text"
          fontWeight="black"
          textAlign="center"
          color="black"
        >
          {getLabelValue(labels, 'lbl_PLCCModal_benefitsText')}
        </BodyCopy>
        {plccBenefitsList && (
          <RichText className="rewards__benefits" richTextHtml={plccBenefitsList} />
        )}
        <div className="footerLinks">
          <BodyCopy component="span" fontSize="fs10" fontFamily="secondary">
            {getLabelValue(labels, 'lbl_PLCCModal_linksTextPrefix')}
          </BodyCopy>
          <Anchor
            className="linkIconSeperator"
            url={getLabelValue(labels, 'lbl_PLCCModal_detailsLink')}
            target="_blank"
            fontSizeVariation="large"
            anchorVariation="primary"
            underline
          >
            {getLabelValue(labels, 'lbl_PLCCForm_details')}
          </Anchor>
          <Anchor
            className="footerLink"
            url={getLabelValue(labels, 'lbl_PLCCModal_faqLink')}
            target="_blank"
            data-locator="plcc_faq"
            fontSizeVariation="large"
            anchorVariation="primary"
            underline
          >
            {getLabelValue(labels, 'lbl_PLCCModal_faqText')}
          </Anchor>
          <Anchor
            className="footerLink"
            url={getLabelValue(labels, 'lbl_PLCCModal_rewardsProgramLink')}
            target="_blank"
            data-locator="plcc_rewards_terms"
            fontSizeVariation="large"
            anchorVariation="primary"
            underline
          >
            {getLabelValue(labels, 'lbl_PLCCModal_rewardsProgramText')}
          </Anchor>
        </div>
        <div className="separator" />
        {isRtpsFlow && (
          <Row>
            <Col colSize={{ large: 12, medium: 8, small: 6 }}>
              <RichText className="text-terms" richTextHtml={rtpsTextTerms} />
            </Col>
          </Row>
        )}
      </div>
    </Modal>
  ) : (
    <ApplyNowPLCCModal
      className={className}
      isPLCCModalOpen={isPLCCModalOpen}
      closePLCCModal={closePLCCModal}
      isRtpsFlow={isRtpsFlow}
      labels={labels}
      isCreateAdsAccountFailed={isCreateAdsAccountFailed}
      isNewPLCCCardFlow={isNewPLCCCardFlow}
    />
  );
};

StyledApplyNowModal.propTypes = {
  className: PropTypes.string.isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  isPLCCModalOpen: PropTypes.bool.isRequired,
  openPLCCModal: PropTypes.func.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  plccBenefitsList: PropTypes.string.isRequired,
  rtpsTop: PropTypes.string.isRequired,
  wicTop: PropTypes.string.isRequired,
  rtpsOptOutMsg: PropTypes.string.isRequired,
  rtpsTextTerms: PropTypes.string.isRequired,
  isRtpsFlow: PropTypes.bool.isRequired,
  labels: PropTypes.shape({
    apply_now_header: PropTypes.string.isRequired,
    apply_now_subheader: PropTypes.string.isRequired,
    applynow_cta: PropTypes.string.isRequired,
    learn_more_link: PropTypes.string.isRequired,
    apply_now_learn_more: PropTypes.string.isRequired,
    apply_now_benefits_header: PropTypes.string.isRequired,
    apply_now_links_text: PropTypes.string.isRequired,
    details_link: PropTypes.string.isRequired,
    apply_now_details: PropTypes.string.isRequired,
    faq_link: PropTypes.string.isRequired,
    apply_now_faq: PropTypes.string.isRequired,
    rewards_program_link: PropTypes.string.isRequired,
    apply_now_rewardTerms: PropTypes.string.isRequired,
    oneequalstwopointsoffer: PropTypes.bool.isRequired,
  }).isRequired,
};

export default withStyles(StyledApplyNowModal, styles);
export { StyledApplyNowModal as StyledApplyNowModalVanilla };
