// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View, SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import ModalNativeHeader from '../../../Modal/view/Modal.header.native';
import { Button, RichText } from '../../../../atoms';
import {
  ImageContainer,
  StyledBodyCopy,
  ScrollViewContainer,
  ButtonWrapper,
  StyledAnchor,
  BottomContainer,
  RichTextContainer,
  Container,
  StyledImage,
} from '../../styles/ApplyNowView.style.native';
import { getLabelValue } from '../../../../../../utils/utils';
import {
  getScreenHeight,
  getPixelRatio,
  redirectToInAppView,
} from '../../../../../../utils/utils.app';
import ApplyCardLayoutView from '../../../../../features/browse/ApplyCardPage';
import ClickTracker from '../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import names from '../../../../../../constants/eventsName.constants';

const viewHeight = {
  height: getScreenHeight(),
  paddingBottom: getPixelRatio() === 'xhdpi' || getPixelRatio() === 'mdpi' ? 0 : 60,
};
const plccTextHeight = getScreenHeight() - 86;
/**
 * @class - ApplyNowModalWrapper
 *
 * @description - used for showing the apply now modal for plcc application flow
 */
class ApplyNowModalWrapper extends React.PureComponent {
  componentDidMount() {
    const { labels, fetchModuleXContent } = this.props;
    if (labels && labels.referred) {
      fetchModuleXContent(labels.referred);
    }
  }

  renderPLCCBenefitList = (plccBenefitsList) =>
    plccBenefitsList ? (
      <Container>
        <RichTextContainer>
          <RichText source={{ html: plccBenefitsList }} />
        </RichTextContainer>
      </Container>
    ) : null;

  /**
   * @function - toggleApplyCardModal
   *
   * @description - used for toggling the apply card modal state.
   */

  toggleApplyCardModal = () => {
    const { resetPLCCApplicationStatus, toggleModal, isRtpsFlow, submitAcceptOrDeclinePlcc } =
      this.props;
    toggleModal({ isModalOpen: false, isPLCCModalOpen: true });
    resetPLCCApplicationStatus({ status: null });
    if (isRtpsFlow) {
      submitAcceptOrDeclinePlcc(true);
    }
  };

  setRTPSFlow = () => {
    const { setIsRTPSFlow, isRtpsFlow, submitAcceptOrDeclinePlcc } = this.props;
    /* istanbul ignore else */
    if (isRtpsFlow && setIsRTPSFlow) {
      submitAcceptOrDeclinePlcc(false);
      setIsRTPSFlow(false);
    }
  };

  /**
   * @function - closeModal
   *
   * @description - closing the apply card modal and finalizing the call.
   */
  closeModal = () => {
    const { toggleModal, resetPLCCApplicationStatus, navigation } = this.props;
    navigation.goBack();
    toggleModal({ isModalOpen: false });
    resetPLCCApplicationStatus({ status: null });
    this.setRTPSFlow();
  };

  closePlccModal = () => {
    const { toggleModal, navigation } = this.props;
    navigation.goBack();
    toggleModal({ isPLCCModalOpen: false });
    this.setRTPSFlow();
  };

  getTermsAndCond = () => {
    const { isRtpsFlow, rtpsTextTerms } = this.props;
    return (
      isRtpsFlow && (
        <Container>
          <RichTextContainer>
            <RichText source={{ html: rtpsTextTerms }} />
          </RichTextContainer>
        </Container>
      )
    );
  };

  getModalHeader = () => {
    const { isPLCCModalOpen, labels } = this.props;
    return (
      <ModalNativeHeader
        heading={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
        onRequestClose={isPLCCModalOpen ? this.closePlccModal : this.closeModal}
        fontSize="fs14"
        customHeaderMargin="25px 25px 0 25px"
      />
    );
  };

  getPageData = () => {
    const { isRtpsFlow } = this.props;
    return {
      pageName: isRtpsFlow ? 'checkout:shipping' : 'account',
      pageNameLoyalty: isRtpsFlow ? 'rtps-shipping' : 'account',
    };
  };

  redirectToInAppView = () => {
    const { labels, navigation } = this.props;
    /* Adding 100ms delay to finish the analytics call */
    setTimeout(() => {
      redirectToInAppView(getLabelValue(labels, 'lbl_PLCCModal_learnMoreLink'), navigation);
    }, 100);
  };
  /**
   * @function render  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */

  render() {
    // eslint-disable-next-line react/prop-types
    const {
      labels,
      plccBenefitsList,
      isPLCCModalOpen,
      isModalOpen,
      isRtpsFlow,
      rtpsOptOutMsg,
      navigation,
      cartOrderItems,
      rtpsTop,
      wicTop,
    } = this.props;
    const rewardImageUrl = getLabelValue(labels, 'oneequalstwopointsoffer')
      ? getLabelValue(labels, 'lbl_PLCCForm_double_reward')
      : getLabelValue(labels, 'lbl_PLCCForm_single_reward');
    const productsData = BagPageUtils.formatBagProductsData(cartOrderItems);
    const pageNameData = this.getPageData();
    return isPLCCModalOpen || isModalOpen ? (
      <SafeAreaView>
        <View style={viewHeight}>
          {this.getModalHeader()}
          {isPLCCModalOpen && (
            <ApplyCardLayoutView
              applyCard={isPLCCModalOpen}
              closePLCCModal={this.closePlccModal}
              isRtpsFlow={isRtpsFlow}
              navigation={navigation}
              loyaltyLocation={pageNameData.pageNameLoyalty}
            />
          )}
          {isModalOpen && (
            <ScrollViewContainer viewHeight={plccTextHeight} keyboardShouldPersistTaps="handled">
              <Container>
                <RichTextContainer>
                  <RichText source={{ html: isRtpsFlow ? rtpsTop : wicTop }} />
                </RichTextContainer>
              </Container>
              <ButtonWrapper>
                <ClickTracker
                  as={Button}
                  name={names.screenNames.loyalty_apply_or_accept_offer_click}
                  clickData={{
                    customEvents: ['event131', 'PLCC_Prescreen_Modal_Yes_e47'],
                    products: productsData,
                    pageData: pageNameData,
                    loyaltyLocation: pageNameData.pageNameLoyalty,
                  }}
                  fill="BLUE"
                  type="submit"
                  color="white"
                  text={
                    !isRtpsFlow
                      ? getLabelValue(labels, 'lbl_PLCCModal_applyNowCTA')
                      : getLabelValue(labels, 'lbl_PLCC_interested')
                  }
                  width="90%"
                  onPress={this.toggleApplyCardModal}
                />
              </ButtonWrapper>

              {!isRtpsFlow ? (
                <ClickTracker
                  as={StyledAnchor}
                  name={names.screenNames.loyalty_learn_more_click}
                  clickData={{
                    customEvents: ['event130'],
                    loyaltyLocation: pageNameData.pageNameLoyalty,
                  }}
                  onPress={this.redirectToInAppView}
                  fontSizeVariation="xlarge"
                  anchorVariation="secondary"
                  underlineBlue
                  text={getLabelValue(labels, 'lbl_PLCCModal_learnMoreText')}
                  paddingTop="23px"
                />
              ) : (
                <ClickTracker
                  as={StyledAnchor}
                  name={names.screenNames.loyalty_plcc_no_thanks_click}
                  clickData={{
                    customEvents: ['PLCC_Prescreen_Modal_No_e48', 'event133'],
                    products: productsData,
                    pageData: pageNameData,
                    loyaltyLocation: pageNameData.pageNameLoyalty,
                  }}
                  onPress={() => {
                    this.closeModal();
                  }}
                  fontSizeVariation="xlarge"
                  anchorVariation="secondary"
                  underlineBlue
                  text={getLabelValue(labels, 'lbl_PLCC_noThanks')}
                  noLink
                  paddingTop="23px"
                />
              )}
              {isRtpsFlow && (
                <Container>
                  <RichTextContainer>
                    <RichText source={{ html: rtpsOptOutMsg }} />
                  </RichTextContainer>
                </Container>
              )}
              <ImageContainer marginTop="28px">
                <StyledImage url={rewardImageUrl} width="95%" height="60px" />
              </ImageContainer>
              <StyledBodyCopy
                fontFamily="primary"
                fontSize="fs28"
                fontWeight="black"
                textAlign="center"
                color="black"
                text={getLabelValue(labels, 'lbl_PLCCModal_benefitsText')}
                paddingTop="9px"
              />
              {this.renderPLCCBenefitList(plccBenefitsList)}
              <BottomContainer>
                <StyledBodyCopy
                  fontSize="fs10"
                  fontFamily="secondary"
                  text={getLabelValue(labels, 'lbl_PLCCModal_linksTextPrefix')}
                  paddingRight="4px"
                />

                <StyledAnchor
                  url={getLabelValue(labels, 'lbl_PLCCModal_detailsLink')}
                  navigation={navigation}
                  fontSizeVariation="medium"
                  anchorVariation="primary"
                  underline
                  text={getLabelValue(labels, 'lbl_PLCCForm_details')}
                  paddingRight="28px"
                />

                <StyledAnchor
                  url={getLabelValue(labels, 'lbl_PLCCModal_faqLink')}
                  navigation={navigation}
                  target="_blank"
                  locator="plcc_faq"
                  fontSizeVariation="medium"
                  anchorVariation="primary"
                  underline
                  text={getLabelValue(labels, 'lbl_PLCCModal_faqText')}
                  paddingRight="28px"
                />

                <StyledAnchor
                  url={getLabelValue(labels, 'lbl_PLCCModal_rewardsProgramLink')}
                  navigation={navigation}
                  target="_blank"
                  data-locator="plcc_rewards_terms"
                  fontSizeVariation="medium"
                  anchorVariation="primary"
                  underline
                  text={getLabelValue(labels, 'lbl_PLCCModal_rewardsProgramText')}
                />
              </BottomContainer>
              {this.getTermsAndCond()}
            </ScrollViewContainer>
          )}
        </View>
      </SafeAreaView>
    ) : null;
  }
}

ApplyNowModalWrapper.propTypes = {
  labels: PropTypes.shape({
    apply_now_link_modal: PropTypes.string,
  }).isRequired,
  fetchModuleXContent: PropTypes.func.isRequired,
  resetPLCCApplicationStatus: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  rtpsOptOutMsg: PropTypes.string.isRequired,
  rtpsTextTerms: PropTypes.string.isRequired,
  setIsRTPSFlow: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool,
  submitAcceptOrDeclinePlcc: PropTypes.func.isRequired,
  isPLCCModalOpen: PropTypes.bool,
  plccBenefitsList: PropTypes.shape({}).isRequired,
  isModalOpen: PropTypes.bool,
  cartOrderItems: PropTypes.shape([]),
  rtpsTop: PropTypes.string.isRequired,
  wicTop: PropTypes.string.isRequired,
};

ApplyNowModalWrapper.defaultProps = {
  isRtpsFlow: false,
  isPLCCModalOpen: false,
  isModalOpen: false,
  cartOrderItems: [],
};

export default ApplyNowModalWrapper;
