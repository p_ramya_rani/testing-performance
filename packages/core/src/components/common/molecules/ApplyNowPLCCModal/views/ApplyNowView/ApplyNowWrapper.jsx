// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';

import StyledApplyNowModal from '../../molecules/ApplyNowModal/views/ApplyNowModal.view';

/**
 * @class ApplyNowModalWrapper - Invokes apply plccc node application
 * A Modal will be opened by clicking apply now button
 */

class ApplyNowModalWrapper extends React.Component {
  componentDidMount() {
    const {
      labels,
      fetchModuleXContent,
      resetPLCCApplicationStatus,
      isModalOpen,
      isPLCCModalOpen,
    } = this.props;

    if (labels && labels.referred && (isModalOpen || isPLCCModalOpen)) {
      fetchModuleXContent(labels.referred);
    }
    resetPLCCApplicationStatus({ status: null });
  }

  componentDidUpdate(prevProps) {
    const { isModalOpen, isPLCCModalOpen, labels, fetchModuleXContent, isRtpsFlow, trackPageLoad } =
      this.props;

    if (
      ((!prevProps.isModalOpen && isModalOpen) ||
        (!prevProps.isPLCCModalOpen && isPLCCModalOpen)) &&
      labels &&
      labels.referred
    ) {
      fetchModuleXContent(labels.referred);
    }

    if (!prevProps.isModalOpen && isModalOpen && isRtpsFlow) {
      trackPageLoad({ customEvents: ['event72'] });
    }
  }

  setRTPSFlow = () => {
    const { setIsRTPSFlow, isRtpsFlow, submitAcceptOrDeclinePlcc } = this.props;
    /* istanbul ignore else */
    if (isRtpsFlow && setIsRTPSFlow) {
      submitAcceptOrDeclinePlcc(false);
      setIsRTPSFlow(false);
    }
  };

  closeModal = () => {
    const { toggleModal } = this.props;
    toggleModal({ isModalOpen: false });
    this.setRTPSFlow();
  };

  closePLCCModal = () => {
    const { toggleModal, resetPLCCApplicationStatus } = this.props;
    toggleModal({ isPLCCModalOpen: false, pageCategory: '' });
    resetPLCCApplicationStatus({ status: null });
    this.setRTPSFlow();
  };

  openModal = (e) => {
    e.preventDefault();
    const { toggleModal, resetPLCCApplicationStatus } = this.props;
    toggleModal({ isModalOpen: true });
    resetPLCCApplicationStatus({ status: null });
  };

  openPLCCModal = (e) => {
    e.preventDefault();
    const { toggleModal, isRtpsFlow, submitAcceptOrDeclinePlcc, resetPLCCApplicationStatus } =
      this.props;
    toggleModal({ isModalOpen: false, isPLCCModalOpen: true });
    resetPLCCApplicationStatus({ status: null });
    /* istanbul ignore else */
    if (isRtpsFlow) {
      submitAcceptOrDeclinePlcc(true);
    }
  };

  render() {
    const {
      className,
      labels,
      isModalOpen,
      isPLCCModalOpen,
      plccBenefitsList,
      isRtpsFlow,
      rtpsCongratsMsg,
      rtpsOptOutMsg,
      rtpsTextTerms,
      rtpsTop,
      wicTop,
      submitAcceptOrDeclinePlcc,
      cartOrderItems,
      isNewPLCCCardFlow,
      isCreateAdsAccountFailed,
    } = this.props;
    return (
      <div className={className}>
        <React.Fragment>
          {isModalOpen || isPLCCModalOpen ? (
            <StyledApplyNowModal
              isModalOpen={isModalOpen}
              isPLCCModalOpen={isPLCCModalOpen}
              openPLCCModal={this.openPLCCModal}
              closePLCCModal={this.closePLCCModal}
              closeModal={this.closeModal}
              labels={labels}
              plccBenefitsList={plccBenefitsList}
              isRtpsFlow={isRtpsFlow}
              rtpsCongratsMsg={rtpsCongratsMsg}
              rtpsOptOutMsg={rtpsOptOutMsg}
              rtpsTop={rtpsTop}
              wicTop={wicTop}
              rtpsTextTerms={rtpsTextTerms}
              submitAcceptOrDeclinePlcc={submitAcceptOrDeclinePlcc}
              cartOrderItems={cartOrderItems}
              isNewPLCCCardFlow={isNewPLCCCardFlow}
              isCreateAdsAccountFailed={isCreateAdsAccountFailed}
            />
          ) : null}
        </React.Fragment>
      </div>
    );
  }
}

ApplyNowModalWrapper.propTypes = {
  className: PropTypes.string.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  isPLCCModalOpen: PropTypes.bool.isRequired,
  labels: PropTypes.shape({
    apply_now_link_modal: PropTypes.string,
  }).isRequired,
  toggleModal: PropTypes.func.isRequired,
  resetPLCCApplicationStatus: PropTypes.func.isRequired,
  fetchModuleXContent: PropTypes.func.isRequired,
  plccBenefitsList: PropTypes.string.isRequired,
  rtpsCongratsMsg: PropTypes.string.isRequired,
  rtpsOptOutMsg: PropTypes.string.isRequired,
  rtpsTop: PropTypes.string.isRequired,
  wicTop: PropTypes.string.isRequired,
  rtpsTextTerms: PropTypes.string.isRequired,
  setIsRTPSFlow: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool,
  submitAcceptOrDeclinePlcc: PropTypes.func.isRequired,
  cartOrderItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  trackPageLoad: PropTypes.func.isRequired,
  isNewPLCCCardFlow: PropTypes.bool,
  isCreateAdsAccountFailed: PropTypes.bool,
};

ApplyNowModalWrapper.defaultProps = {
  isRtpsFlow: false,
  isNewPLCCCardFlow: false,
  isCreateAdsAccountFailed: false,
};

export default ApplyNowModalWrapper;
