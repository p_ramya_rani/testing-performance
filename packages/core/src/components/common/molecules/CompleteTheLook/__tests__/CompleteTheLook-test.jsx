import React from 'react';

import { shallow } from 'enzyme';

import { CompleteTheLookVanilla } from '../views/CompleteTheLook';

describe('CompleteTheLook component', () => {
  it('should renders correctly ', () => {
    const props = {
      className: 'classname',
      scrollToCompleteLookSection: () => {},
      pdpLabels: {},
    };
    const component = shallow(<CompleteTheLookVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
