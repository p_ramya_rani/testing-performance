import { css } from 'styled-components';

const CompleteTheLookStyle = css`
  &.complete-the-look-new-pdp {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: flex;
      width: 165px;
      height: 23px;
      padding: 9px 11px 11px 18px;
      border: solid 1px ${(props) => props.theme.colorPalette.blue[900]};
      border-radius: 50px;
      background-color: ${(props) => props.theme.colors.WHITE};
      cursor: pointer;
      position: absolute;
      bottom: 0px;
      right: 0px;
      animation: complete-the-look-slide-in 0.5s ease-in 3s forwards;
    }
  }
  .complete-the-look-text-new-pdp {
    height: 13px;
    margin: 6px 0px 4px 0;
    color: ${(props) => props.theme.colorPalette.blue[800]};
    line-height: 1;
    display: inline-block;
    animation: text-disapear 0.5s ease-in 3s forwards;
  }
  .complete-the-look-icon-new-pdp {
    width: 24px;
    height: 23px;
    float: right;
    position: relative;
    animation: icon-slide-in 0.5s ease-in 3s forwards;
  }
  @keyframes text-disapear {
    0% {
      opacity: 1;
      width: 125px;
      height: 13px;
    }
    50% {
      width: 0;
      height: 0;
      opacity: 0;
    }
    100% {
      width: 0;
      height: 0;
      opacity: 0;
    }
  }
  @keyframes complete-the-look-slide-in {
    0% {
      width: 167px;
      height: 23px;
      padding: 9px 11px 11px 18px;
    }
    90% {
      width: 45px;
      height: 35px;
      padding: 6px 7px 7px 6px;
    }
    100% {
      width: 35px;
      height: 35px;
      padding: 6px 7px 7px 6px;
    }
  }
  @keyframes icon-slide-in {
    0% {
      padding: 0;
    }
    100% {
      padding: 6px 5px 8px;
    }
  }
`;

export default CompleteTheLookStyle;
