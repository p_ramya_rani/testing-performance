import React from 'react';
import PropTypes, { func } from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath } from '@tcp/core/src/utils';
import { Image } from '@tcp/core/src/components/common/atoms';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import CompleteTheLookStyle from '../styles/CompleteTheLook.style';

class CompleteTheLook extends React.Component {
  constructor(props) {
    super(props);
    this.state = { animate: false };
  }

  componentDidMount() {
    this.setState({ animate: true });
  }

  render() {
    const { className, scrollToCompleteLookSection, pdpLabels } = this.props;
    const { animate } = this.state;
    const { completeTheLook } = pdpLabels;
    const completeLookIconPath = getIconPath('standard_clothes_3x');

    return (
      <div
        role="button"
        tabIndex={0}
        onKeyDown={this.showOutfitPDP}
        className={animate ? `${className} complete-the-look-new-pdp` : className}
        onClick={scrollToCompleteLookSection}
      >
        <BodyCopy
          className="complete-the-look-text-new-pdp"
          fontSize="fs14"
          component="p"
          fontFamily="secondary"
          fontWeight="bold"
        >
          {`${completeTheLook}!`}
        </BodyCopy>
        <Image src={completeLookIconPath} className="complete-the-look-icon-new-pdp" />
      </div>
    );
  }
}

CompleteTheLook.defaultProps = {
  className: '',
  scrollToCompleteLookSection: () => {},
  pdpLabels: {},
};

CompleteTheLook.propTypes = {
  className: PropTypes.string,
  scrollToCompleteLookSection: func,
  pdpLabels: PropTypes.shape({}),
};

export default withStyles(CompleteTheLook, CompleteTheLookStyle);
export { CompleteTheLook as CompleteTheLookVanilla };
