// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';

const StyleHeadingContainer = styled.View`
  position: relative;
  border-top-width: 1px;
  border-color: ${props => props.theme.colors.BORDER.NORMAL};
  margin: 0 ${props => props.theme.spacing.ELEM_SPACING.MED};
`;
const StyleHeading = styled.Text`
  top: -11px;
  background: ${props => props.theme.colorPalette.white};
  width: 110px;
`;

const StyleWithContainer = styled.View`
  display: flex;
  flex-direction: row;
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.XL};
`;

const StyleWithImage = styled.View`
  display: flex;
  align-items: center;
  width: 45%;
`;

const StyleWithContent = styled(ViewWithSpacing)`
  width: 55%;
  padding-left: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export {
  StyleHeadingContainer,
  StyleHeading,
  StyleWithContainer,
  StyleWithImage,
  StyleWithContent,
};
