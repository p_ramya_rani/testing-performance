// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .section-styled-heading {
    margin: ${props => props.theme.spacing.ELEM_SPACING.MED} 0
      ${props => props.theme.spacing.ELEM_SPACING.XL} 0;
    border-bottom: 1px solid ${props => props.theme.colorPalette.gray[500]};
    position: relative;

    span {
      position: absolute;
      top: -10px;
      display: inline-block;
      padding: 0 ${props => props.theme.spacing.ELEM_SPACING.XS} 0 0;
      background: ${props => props.theme.colorPalette.white};
    }
  }
`;
