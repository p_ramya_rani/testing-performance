// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Anchor } from '@tcp/core/src/components/common/atoms';
import ProductSummary from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductSummary';
import {
  getMapSliceForColor,
  getMapSliceForColorProductId,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { getCartItemInfo } from '@tcp/core/src/components/features/CnC/AddedToBag/util/utility';
import ProductAddToBagContainer from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.container';
import ImageCarousel from '@tcp/core/src/components/features/browse/ProductListing/molecules/ImageCarousel';
import {
  StyleWithContainer,
  StyleWithImage,
  StyleWithContent,
  StyleHeadingContainer,
  StyleHeading,
} from '../styles/RecomStyleWith.style.native';

class RecomStyleWith extends PureComponent {
  constructor(props) {
    super(props);
    const {
      productItem: { colorFitsSizesMap, generalProductId },
    } = this.props;

    this.state = {
      currentColorEntry: getMapSliceForColorProductId(colorFitsSizesMap, generalProductId) || {},
    };
    const { currentColorEntry } = this.state;
    const colorName = currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;

    this.formValues = {
      Fit: '',
      Size: '',
      color: colorName,
      Quantity: 1,
    };
  }

  onChangeColor = e => {
    const {
      currentProduct: { colorFitsSizesMap },
    } = this.props;
    const currentColorEntry = getMapSliceForColor(colorFitsSizesMap, e);
    this.setState({
      currentColorEntry,
    });
  };

  onChangeSize = (selectedColor, e, selectedFit, selectedQuantity) => {
    this.formValues = {
      Fit: selectedFit,
      Size: e,
      color: selectedColor,
      Quantity: selectedQuantity,
    };
  };

  handleAddToBag = () => {
    const { addToBagEcom, productItem } = this.props;

    const productInfo = productItem;
    const formVals = {
      fit: this.formValues.Fit,
      size: this.formValues.Size,
      color: this.formValues.color,
      quantity: this.formValues.Quantity,
    };

    let cartItemInfo = getCartItemInfo(productInfo, formVals);
    cartItemInfo = {
      ...cartItemInfo,
      fromStyleWith: true,
    };
    addToBagEcom(cartItemInfo);
  };

  render() {
    const {
      productItem,
      currency,
      plpLabels,
      headerLabel,
      productData,
      handleSubmit,
      navigation,
      closeATBModal,
      addToBagError,
      addToBagErrorId,
    } = this.props;
    const { currentColorEntry } = this.state;
    const selectedColorProductId = currentColorEntry && currentColorEntry.colorProductId;
    const currentProduct = productItem;
    return (
      <>
        <StyleHeadingContainer>
          <StyleHeading>
            <BodyCopy
              mobileFontFamily="secondary"
              fontWeight="semibold"
              fontSize="fs16"
              text={headerLabel}
              color="black"
            />
          </StyleHeading>
        </StyleHeadingContainer>
        <StyleWithContainer>
          <StyleWithImage>
            <ImageCarousel item={productData} noCarousel isStyleWith selectedColorIndex={0} />
            <Anchor
              fontSizeVariation="medium"
              fontFamily="secondary"
              underline
              anchorVariation="primary"
              onPress={() => {
                closeATBModal();
                return navigation.navigate('ProductDetail', {
                  title: productItem && productItem.name,
                  pdpUrl: productItem.unbxdProdId,
                  selectedColorProductId,
                  reset: true,
                });
              }}
              text={plpLabels && plpLabels.viewProductDetails}
            />
          </StyleWithImage>
          <StyleWithContent>
            <ProductSummary
              productData={currentProduct}
              selectedColorProductId={selectedColorProductId}
              currencySymbol={currency}
              isGiftCard={currentProduct.isGiftCard}
              renderRatingReview
              isStyleWith
            />
            <ProductAddToBagContainer
              currentProduct={currentProduct}
              plpLabels={plpLabels}
              handleFormSubmit={this.handleAddToBag}
              selectedColorProductId={selectedColorProductId}
              onChangeColor={this.onChangeColor}
              handleSubmit={handleSubmit}
              onChangeSize={this.onChangeSize}
              scrollToTarget={this.scrollPageToTarget}
              isStyleWith
              errorOnHandleSubmit={addToBagError}
              customFormName="styleWithForm"
              addToBagErrorId={addToBagErrorId}
            />
          </StyleWithContent>
        </StyleWithContainer>
      </>
    );
  }
}

RecomStyleWith.propTypes = {
  productItem: PropTypes.shape({}).isRequired,
  currency: PropTypes.string,
  headerLabel: PropTypes.string,
  plpLabels: PropTypes.shape({}).isRequired,
  addToBagEcom: PropTypes.func,
  currentProduct: PropTypes.shape({}),
  productData: PropTypes.shape({}),
  handleSubmit: PropTypes.func,
  navigation: PropTypes.shape({}),
  closeATBModal: PropTypes.func,
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
};

RecomStyleWith.defaultProps = {
  currency: '',
  addToBagEcom: () => {},
  headerLabel: '',
  currentProduct: {},
  productData: {},
  handleSubmit: () => {},
  navigation: {},
  closeATBModal: () => {},
  addToBagError: '',
  addToBagErrorId: '',
};

export default RecomStyleWith;
