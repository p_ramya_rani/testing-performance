// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ProductAddToBagContainer from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.container';
import { Row, Col, BodyCopy, Anchor } from '@tcp/core/src/components/common/atoms';
import { getCartItemInfo } from '@tcp/core/src/components/features/CnC/AddedToBag/util/utility';
import { routerPush } from '@tcp/core/src/utils';
import {
  getMapSliceForColor,
  getMapSliceForColorProductId,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import ProductImagesWrapper from '../../../../features/browse/ProductDetail/molecules/ProductImagesWrapper/views/ProductImagesWrapper.view';
import Product from '../../../../features/browse/ProductDetail/molecules/Product/views/Product.view';
import withStyles from '../../../hoc/withStyles';
import style from '../styles/RecomStyleWith.style';

class RecomStyleWith extends PureComponent {
  constructor(props) {
    super(props);
    const {
      productItem: { colorFitsSizesMap, generalProductId, offerPrice },
    } = this.props;

    this.state = {
      currentColorEntry: getMapSliceForColorProductId(colorFitsSizesMap, generalProductId) || {},
      currentGiftCardValue: offerPrice,
    };
    const { currentColorEntry } = this.state;
    const colorName = currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;

    this.formValues = {
      Fit: '',
      Size: '',
      color: colorName,
      Quantity: 1,
    };
  }

  handleAddToBag = () => {
    const { addToBagEcomAction, productItem } = this.props;

    const productInfo = productItem;
    const formVals = {
      fit: this.formValues.Fit,
      size: this.formValues.Size,
      color: this.formValues.color,
      quantity: this.formValues.Quantity,
    };

    let cartItemInfo = getCartItemInfo(productInfo, formVals);
    cartItemInfo = {
      ...cartItemInfo,
      fromStyleWith: true,
    };
    addToBagEcomAction(cartItemInfo);
  };

  onChangeColor = (e, selectedSize, selectedFit, selectedQuantity) => {
    const { productItem } = this.props;
    const { colorFitsSizesMap } = productItem;
    const { currentGiftCardValue } = this.state;
    this.setState({
      currentColorEntry: getMapSliceForColor(colorFitsSizesMap, e),
      currentGiftCardValue:
        (getMapSliceForColor(colorFitsSizesMap, e) &&
          getMapSliceForColor(colorFitsSizesMap, e).offerPrice) ||
        currentGiftCardValue,
    });
    this.formValues = {
      Fit: selectedFit,
      Size: selectedSize,
      color: e,
      Quantity: selectedQuantity,
    };
  };

  onChangeSize = (selectedColor, e, selectedFit, selectedQuantity) => {
    this.setState({ currentGiftCardValue: e });
    this.formValues = {
      Fit: selectedFit,
      Size: e,
      color: selectedColor,
      Quantity: selectedQuantity,
    };
  };

  navigateToPDP = (e, pdpToPath, currentColorPdpUrl) => {
    e.preventDefault();
    routerPush(pdpToPath, currentColorPdpUrl);
  };

  render() {
    const {
      productItem,
      currencySymbol,
      plpLabels,
      styleHeaderLabel,
      className,
      addToBagError,
      addToBagErrorId,
      isPDPSmoothScrollEnabled,
    } = this.props;
    let imagesToDisplay = [];
    const { currentColorEntry } = this.state;
    const selectedColorProductId = currentColorEntry && currentColorEntry.colorProductId;
    const selectedColor =
      currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;
    const { imagesByColor, pdpUrl, unbxdProdId } = productItem;

    imagesToDisplay = imagesByColor[selectedColor];

    const productData = {
      currentProduct: productItem,
    };
    return (
      <div className={className}>
        <BodyCopy
          className="section-styled-heading"
          fontSize="fs16"
          component="h2"
          fontFamily="secondary"
          fontWeight="semibold"
          id="recommendation"
        >
          <BodyCopy
            color="black"
            fontSize="fs16"
            fontFamily="secondary"
            fontWeight="semibold"
            component="span"
          >
            {styleHeaderLabel}
          </BodyCopy>
        </BodyCopy>
        <Row>
          <Col className="product-breadcrumb-wrapper" colSize={{ small: 3, medium: 4, large: 6 }}>
            <BodyCopy component="div" textAlign="center">
              <ProductImagesWrapper
                productName=""
                isGiftCard={false}
                images={[
                  {
                    regularSizeImageUrl: imagesToDisplay && imagesToDisplay.basicImageUrl,
                  },
                ]}
                selectedColorProductId={selectedColorProductId}
                pdpLabels={plpLabels}
                currentProduct={productItem}
                isCompleteTheLookTestEnabled={false}
                isStyleWith
                isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
              />
              <Anchor
                className="view-product-details"
                underline
                fontSizeVariation="medium"
                noLink
                onClick={(e) => {
                  this.navigateToPDP(e, `/p?pid=${unbxdProdId}`, pdpUrl);
                }}
              >
                {plpLabels && plpLabels.viewProductDetails}
              </Anchor>
            </BodyCopy>
          </Col>
          <Col className="product-breadcrumb-wrapper" colSize={{ small: 3, medium: 4, large: 6 }}>
            <Product productDetails={productData} currencySymbol={currencySymbol} isStyleWith />
            <ProductAddToBagContainer
              handleFormSubmit={this.handleAddToBag}
              errorOnHandleSubmit={addToBagError}
              currentProduct={productItem}
              plpLabels={plpLabels}
              onChangeColor={this.onChangeColor}
              onChangeSize={this.onChangeSize}
              renderReceiveProps={false}
              initialFormValues={this.formValues}
              isKeepAliveEnabled
              isStyleWith
              customFormName="styleWithForm"
              addToBagErrorId={addToBagErrorId}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

RecomStyleWith.propTypes = {
  productItem: PropTypes.shape({}).isRequired,
  currencySymbol: PropTypes.string,
  styleHeaderLabel: PropTypes.string,
  plpLabels: PropTypes.shape({}).isRequired,
  addToBagEcomAction: PropTypes.func,
  className: PropTypes.string,
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
  isPDPSmoothScrollEnabled: PropTypes.bool,
};

RecomStyleWith.defaultProps = {
  currencySymbol: '',
  addToBagEcomAction: () => {},
  styleHeaderLabel: '',
  className: '',
  addToBagError: '',
  addToBagErrorId: '',
  isPDPSmoothScrollEnabled: false,
};

export { RecomStyleWith as RecomStyleWithVanilla };
export default withStyles(RecomStyleWith, style);
