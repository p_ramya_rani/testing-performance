// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';

import ImageCarousel from '@tcp/core/src/components/features/browse/ProductListing/molecules/ImageCarousel';
import ProductSummary from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductSummary';
import ProductAddToBagContainer from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.container';
import RecomStyleWith from '../views/RecomStyleWith.view.native';

describe('RecomStyleWith component', () => {
  const props = {
    productItem: [
      {
        name: '',
        imagesByColor: '',
        id: 12,
      },
    ],
    plpLabels: [],
    addToBagEcomAction: jest.fn(),
    selectedImageIndex: 0,
  };
  it('should renders correctly', () => {
    const component = shallow(<RecomStyleWith {...props} />);
    expect(component).toMatchSnapshot();
  });

  const newProps = {
    ...props,
    isStyleWith: true,
    noCarousel: true,
    selectedColorIndex: 0,
    item: {
      colorsMap: [],
      imagesByColor: [],
      skuInfo: '123',
      productInfo: [
        {
          name: '',
          imagesByColor: '',
          id: 12,
          colorProductId: '1234',
        },
      ],
    },
    productData: [
      {
        name: '',
        imagesByColor: '',
        id: 12,
        colorProductId: '1234',
      },
    ],
    selectedColorProductId: '1234',
    currencySymbol: 'USD',
    isGiftCard: false,
    renderRatingReview: true,
  };

  let imageComponent;

  beforeEach(() => {
    imageComponent = shallow(<ImageCarousel {...newProps} />);
  });

  it('should return ImageCarousel component', () => {
    expect(imageComponent).toHaveLength(1);
  });

  let productSummaryComp;

  beforeEach(() => {
    productSummaryComp = shallow(<ProductSummary {...newProps} />);
  });

  it('should return ProductSummary component', () => {
    expect(productSummaryComp).toHaveLength(1);
  });

  let productATBComp;

  beforeEach(() => {
    productATBComp = shallow(<ProductAddToBagContainer {...newProps} />);
  });

  it('should return ProductAddToBagContainer component', () => {
    expect(productATBComp).toHaveLength(1);
  });
});
