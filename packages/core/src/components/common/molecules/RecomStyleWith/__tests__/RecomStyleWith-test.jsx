// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { RecomStyleWithVanilla } from '../views/RecomStyleWith.view';

describe('RecomStyleWith component', () => {
  it('should renders correctly', () => {
    const props = {
      plpLabels: [],
      productItem: {
        imagesByColor: [],
      },
      addToBagEcomAction: jest.fn(),
      selectedImageIndex: 0,
    };
    const component = shallow(<RecomStyleWithVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
