// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { fromJS } from 'immutable';
import { Row, Button, Image, Col } from '@tcp/core/src/components/common/atoms';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import DamImage from '@tcp/core/src/components/common/atoms/DamImage';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import MiniBagSelect from '@tcp/web/src/components/features/CnC/MiniBag/molecules/MiniBagSelectBox/MiniBagSelectBox';
import ColorSelector from '@tcp/web/src/components/features/CnC/MiniBag/molecules/ColorSelect/views/ColorSelect.view';
import { getIconPath } from '@tcp/core/src/utils';
import CONSTANTS from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import style, { buttonCustomStyles } from './styles/ProductCustomizeForm.style';

class ProductCustomizeForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedColor: '',
      selectedFit: '',
      selectedSize: '',
      selectedQuantity: '',
      isErrorMessageDisplayed: false,
      fitChanged: false,
      persistSelectedFit: '',
    };
  }

  componentDidMount() {
    const { initialValues } = this.props;
    this.setState({
      selectedColor: initialValues.color,
      selectedFit: initialValues.Fit,
      selectedSize: initialValues.Size,
      selectedQuantity: initialValues.Qty,
      persistSelectedFit: initialValues.Fit,
    });
  }

  getFormValues = () => {
    const { selectedColor, selectedFit, selectedSize, selectedQuantity } = this.state;
    return {
      color: selectedColor,
      fit: selectedFit,
      quantity: selectedQuantity,
      size: selectedSize,
    };
  };

  getSkuId = () => {
    const { selectedColor, selectedFit, selectedSize } = this.state;
    const { colorFitsSizesMap } = this.props;
    const colorItem = this.getSelectedColorData(colorFitsSizesMap, selectedColor);
    const hasFits = colorItem.getIn([0, 'hasFits']);
    let fit;
    let sizeItem;
    if (hasFits) {
      fit = colorItem
        .getIn([0, 'fits'])
        .find((fitItems) => fitItems.get('fitName') === selectedFit);
      sizeItem = fit && fit.get('sizes');
    } else {
      fit = colorItem.getIn([0, 'fits']);
      sizeItem = fit && fit.getIn([0, 'sizes']);
    }
    return sizeItem && sizeItem.find((size) => size.get('sizeName') === selectedSize).get('skuId');
  };

  getSelectedSize = () => {
    const { selectedSize } = this.state;
    return selectedSize;
  };

  getSelectedColorData = (colorFitsSizesMap, color) => {
    return (
      colorFitsSizesMap &&
      colorFitsSizesMap.filter((colorItem) => {
        return colorItem.getIn(['color', 'name']) === color.name && colorItem;
      })
    );
  };

  getColorOptions = (colorFitsSizesMap, itemBrand) => {
    return (
      colorFitsSizesMap &&
      colorFitsSizesMap.map((colorItem) => {
        const imgData = {
          alt: '',
          url: colorItem.getIn(['color', 'imagePath']),
        };
        const imgConfig = CONSTANTS.SWATCH_TRANSFORMATION;
        const imgDataConfig = [`${imgConfig}`, `${imgConfig}`, `${imgConfig}`];

        return {
          title: (
            <React.Fragment>
              {/* Empty alt. Next Span contain image desc. Screen reader users will have the burden of redundant content announced. */}
              {/* <img
              alt=""
              className="selected-color-image"
              src={endpoints.global.baseURI + colorItem.getIn(['color', 'imagePath'])}
            /> */}
              <DamImage
                className="color-image"
                imgData={imgData}
                imgConfigs={imgDataConfig}
                isProductImage
                itemBrand={itemBrand}
              />
              <span>{colorItem.getIn(['color', 'name'])}</span>
            </React.Fragment>
          ),
          content: (
            <React.Fragment>
              {/* Empty alt. Next Span contain image desc. Screen reader users will have the burden of redundant content announced. */}
              {/* <img alt="" src={endpoints.global.baseURI + colorItem.getIn(['color', 'imagePath'])} /> */}
              <DamImage
                className="color-image"
                imgData={imgData}
                imgConfigs={imgDataConfig}
                isProductImage
                itemBrand={itemBrand}
              />
              <span>{colorItem.getIn(['color', 'name'])}</span>
            </React.Fragment>
          ),
          value: colorItem.getIn(['color', 'name']),
        };
      })
    );
  };

  getFitOptions = (colorItem) => {
    return (
      (colorItem &&
        colorItem.get('fits').map((fit) => ({
          displayName: fit.get('fitName'),
          id: fit.get('fitName'),
        }))) ||
      []
    );
  };

  getSizes = (fit) => {
    const { editableProductMultiPackInfo = {} } = this.props;
    const { multiPackInfo: { multiPackInventoryThreshold, multiPackCount } = {} } =
      editableProductMultiPackInfo;

    return fit
      .get('sizes')
      .filter((size) => {
        if (multiPackInventoryThreshold && multiPackCount) {
          return (
            size.get('v_qty') > 0 &&
            size.get('v_qty') / parseInt(multiPackCount, 10) > multiPackInventoryThreshold
          );
        }
        return size.get('maxAvailable') > 0;
      })
      .map((size) => ({
        displayName: size.get('sizeName'),
        id: size.get('sizeName'),
      }));
  };

  getSizeOptions = (colorItem, selectedFit, fitChanged) => {
    let sizeOptions = [];

    if (colorItem) {
      colorItem.get('fits').forEach((fit) => {
        if (selectedFit) {
          if (fit.get('fitName') === selectedFit) {
            sizeOptions = this.getSizes(fit);
            if (fitChanged) {
              sizeOptions = sizeOptions.unshift({
                displayName: 'Select',
                id: 'Select',
              });
            }
          }
        } else {
          sizeOptions = this.getSizes(fit);
        }
      });
    }

    return sizeOptions;
  };

  colorChange = (e) => {
    this.setState({
      selectedColor: { name: e },
    });
  };

  fitChange = (e) => {
    const { selectedSize, persistSelectedFit } = this.state;
    if (persistSelectedFit !== e.target.value) {
      this.setState({
        selectedFit: e.target.value,
        fitChanged: true,
      });
    } else {
      this.setState({
        selectedFit: e.target.value,
        fitChanged: false,
      });
    }
    if (selectedSize !== 'Select') {
      this.displayErrorMessage(false);
    }
  };

  sizeChange = (e) => {
    const { selectedFit } = this.state;
    this.setState({
      persistSelectedFit: selectedFit,
      selectedSize: e.target.value,
      fitChanged: false,
    });
    if (e.target.value !== 'Select') {
      this.displayErrorMessage(false);
    }
  };

  quantityChange = (e) => {
    this.setState({
      selectedQuantity: e.target.value,
    });
  };

  getQuantityList = () => {
    const quantityArray = new Array(15).fill(1);
    return quantityArray.map((val, index) => ({ displayName: index + 1, id: index + 1 }));
  };

  getSizeLabel = (productDetail, labels) => {
    return productDetail.itemInfo.isGiftItem === true ? `${labels.value}` : `${labels.size}`;
  };

  getColorLabel = (productDetail, labels) => {
    return productDetail.itemInfo.isGiftItem === true ? `${labels.design}` : `${labels.color}`;
  };

  displayErrorMessage = (displayError) => {
    this.setState({
      isErrorMessageDisplayed: displayError,
    });
  };

  getColorFitsSizesMap = () => {
    const { colorFitsSizesMap, editableProductMultiPackInfo } = this.props;

    if (editableProductMultiPackInfo && editableProductMultiPackInfo.length > 0) {
      const { colorFitsSizesMap: colorFitsSizesMapMultiPack } = editableProductMultiPackInfo;
      return fromJS(colorFitsSizesMapMultiPack);
    }

    return colorFitsSizesMap;
  };

  render() {
    const { item, labels, formVisiblity, itemBrand, newMiniBag } = this.props;
    const { selectedColor, selectedFit, selectedQuantity, isErrorMessageDisplayed, fitChanged } =
      this.state;

    const colorFitsSizesMap = this.getColorFitsSizesMap();

    const colorList = this.getColorOptions(colorFitsSizesMap, itemBrand);

    const selectedColorOption =
      colorList && colorList.find((color) => color.value === selectedColor.name);
    const selectedColorElement = this.getSelectedColorData(colorFitsSizesMap, selectedColor);
    const hasFits = selectedColorElement && selectedColorElement.getIn([0, 'hasFits']);
    const fitList = hasFits && this.getFitOptions(selectedColorElement.get(0));
    const sizeList =
      selectedColorElement &&
      (hasFits
        ? this.getSizeOptions(selectedColorElement.get(0), selectedFit, fitChanged)
        : this.getSizeOptions(selectedColorElement.get(0)));

    const { className } = this.props;
    const { handleSubmit } = this.props;
    const { itemId } = item.itemInfo;
    const quantity = selectedQuantity || '1';
    const { itemPartNumber } = item.productInfo;
    const { variantNo } = item.productInfo;
    return (
      <form className={className} noValidate>
        <Row className="edit-form-css">
          <Col colSize={{ small: 6, medium: 8, large: 10 }}>
            <div className="select-value-wrapper">
              <div className="color-selector">
                <Field
                  width={87}
                  id="color"
                  component={ColorSelector}
                  selectListTitle={this.getColorLabel(item, labels)}
                  name={selectedColor}
                  selectedColorOption={selectedColorOption}
                  options={colorList}
                  onChange={this.colorChange}
                  dataLocator="addnewaddress-state"
                />
              </div>
              {hasFits && (
                <div className="fit-selector">
                  <Field
                    width={69}
                    id="fit"
                    name="Fit"
                    component={MiniBagSelect}
                    options={fitList}
                    onChange={this.fitChange}
                    dataLocator="addnewaddress-state"
                  />
                </div>
              )}
              <div className="size-selector">
                <Field
                  width={65}
                  className={isErrorMessageDisplayed ? 'size-field-error' : 'size-field'}
                  id="size"
                  name={this.getSizeLabel(item, labels)}
                  component={MiniBagSelect}
                  options={sizeList}
                  onChange={this.sizeChange}
                  dataLocator="addnewaddress-state"
                />
                {isErrorMessageDisplayed && (
                  <BodyCopy
                    className="size-error"
                    fontSize="fs12"
                    component="div"
                    fontFamily="secondary"
                    fontWeight="regular"
                    role="alert"
                    aria-live="assertive"
                  >
                    <Image
                      alt="Error"
                      className="error-image"
                      src={getIconPath('circle-alert-fill')}
                      data-locator="productcustomizeform-error-icon"
                    />
                    {labels.errorSize}
                  </BodyCopy>
                )}
              </div>
              <div className="qty-selector">
                <Field
                  width={32}
                  id="quantity"
                  name="Qty"
                  component={MiniBagSelect}
                  options={this.getQuantityList()}
                  onChange={this.quantityChange}
                  dataLocator="addnewaddress-state"
                />
              </div>
            </div>
          </Col>
          <Col colSize={{ small: 2, medium: 2, large: 2 }}>
            <div className="button-wrapper">
              <Button
                inheritedStyles={buttonCustomStyles}
                type="submit"
                onClick={(e) => {
                  e.preventDefault();
                  if (fitChanged) {
                    this.displayErrorMessage(fitChanged);
                  } else {
                    this.displayErrorMessage(fitChanged);
                    handleSubmit(
                      itemId,
                      this.getSkuId(),
                      quantity,
                      itemPartNumber,
                      variantNo,
                      this.getSelectedSize(),
                      this.getFormValues()
                    );
                  }
                }}
                className={`${newMiniBag ? 'new-minibag-update' : ''}`}
              >
                {/* <u>{labels.update}</u> */}
                <u>Update</u>
              </Button>
              <Button
                className={`button-cancel ${newMiniBag ? 'new-minibag-cancel' : ''}`}
                inheritedStyles={buttonCustomStyles}
                onClick={() => {
                  formVisiblity();
                }}
              >
                <u>{labels.cancel}</u>
              </Button>
            </div>
          </Col>
        </Row>
      </form>
    );
  }
}

ProductCustomizeForm.propTypes = {
  initialValues: PropTypes.shape([]).isRequired,
  colorFitsSizesMap: PropTypes.shape([]).isRequired,
  item: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  formVisiblity: PropTypes.bool.isRequired,
  itemBrand: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  editableProductMultiPackInfo: PropTypes.shape({}),
};

ProductCustomizeForm.defaultProps = {
  editableProductMultiPackInfo: {},
};

export { ProductCustomizeForm as ProductCustomizeFormVanilla };

export default connect()(
  reduxForm({
    form: 'ProductCustomizeForm',
    enableReinitialize: true,

    // a unique identifier for this form..
  })(withStyles(ProductCustomizeForm, style))
);
