// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../hoc/withStyles.native';
import { DotContainer, DotTouchableOpacity, DotComp } from '../styles/PaginationDots.style.native';

class PaginationDots extends React.PureComponent {
  renderDots = () => {
    const {
      numberOfDots,
      selectedIndex,
      size,
      selectedSize,
      borderRadius,
      selectedBorderRadius,
      backgroundColor,
      selectedBackgroundColor,
      borderWidth,
      selectedBorderWidth,
      borderColor,
      selectedBorderColor,
      dotLeftMargin,
      dotRightMargin,
      accessibilityLabels,
      isFullScreenAndroid,
      isSBP,
      isNewReDesignEnabled,
      isChildCarousel,
    } = this.props;
    const dots = [];

    for (let i = 0; i < numberOfDots; i += 1) {
      const accState = i === selectedIndex;
      dots.push(
        <DotTouchableOpacity
          key={i}
          onPress={() => this.onPageChange(i)}
          accessibilityRole="button"
          accessibilityState={{ selected: accState }}
          accessibilityLabel={`${accessibilityLabels.lbl_show_page} ${i + 1} ${
            accessibilityLabels.lbl_product_image
          }`}
          accessibilityHint={accessibilityLabels.lbl_product_hint}
        >
          <DotComp
            // eslint-disable-next-line
            selected={i === selectedIndex}
            size={size}
            selectedSize={selectedSize}
            borderRadius={borderRadius}
            selectedBorderRadius={selectedBorderRadius}
            backgroundColor={backgroundColor}
            selectedBackgroundColor={selectedBackgroundColor}
            borderWidth={borderWidth}
            selectedBorderWidth={selectedBorderWidth}
            borderColor={borderColor}
            selectedBorderColor={selectedBorderColor}
            dotLeftMargin={dotLeftMargin}
            dotRightMargin={dotRightMargin}
            isFullScreenAndroid={isFullScreenAndroid}
            isSBP={isSBP}
            isNewReDesignEnabled={isNewReDesignEnabled}
            isChildCarousel={isChildCarousel}
          />
        </DotTouchableOpacity>
      );
    }
    return dots;
  };

  onPageChange = (clickItemIndex) => {
    const { onPress } = this.props;
    if (onPress) {
      onPress(clickItemIndex);
    }
  };

  render() {
    const { marginTop } = this.props;
    return <DotContainer marginTop={marginTop}>{this.renderDots()}</DotContainer>;
  }
}

PaginationDots.propTypes = {
  numberOfDots: PropTypes.number.isRequired,
  selectedIndex: PropTypes.number.isRequired,
  onPress: PropTypes.func,
  size: PropTypes.number,
  selectedSize: PropTypes.number,
  borderRadius: PropTypes.number,
  selectedBorderRadius: PropTypes.number,
  backgroundColor: PropTypes.string,
  selectedBackgroundColor: PropTypes.string,
  borderWidth: PropTypes.number,
  selectedBorderWidth: PropTypes.number,
  borderColor: PropTypes.string,
  selectedBorderColor: PropTypes.string,
  dotLeftMargin: PropTypes.number,
  dotRightMargin: PropTypes.number,
  theme: PropTypes.shape({}),
  accessibilityLabels: PropTypes.shape({}),
  marginTop: PropTypes.number,
  isFullScreenAndroid: PropTypes.bool,
  isSBP: PropTypes.bool,
  isNewReDesignEnabled: PropTypes.bool,
  isChildCarousel: PropTypes.bool,
};

PaginationDots.defaultProps = {
  onPress: () => {},
  size: 8,
  selectedSize: 8,
  borderRadius: 4,
  selectedBorderRadius: 4,
  backgroundColor: '#e1e1e1',
  selectedBackgroundColor: '#1a1a1a',
  borderWidth: 1,
  selectedBorderWidth: 1,
  borderColor: '#e1e1e1',
  selectedBorderColor: '#1a1a1a',
  dotLeftMargin: 2,
  dotRightMargin: 2,
  theme: {},
  accessibilityLabels: {},
  marginTop: 0,
  isFullScreenAndroid: false,
  isSBP: false,
  isNewReDesignEnabled: false,
  isChildCarousel: false,
};

export default withStyles(PaginationDots);
export { PaginationDots as PaginationDotsVanilla };
