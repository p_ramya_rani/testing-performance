// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const DotContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-self: center;
  align-items: center;
  margin-top: ${(props) => props.marginTop};
`;

const DotTouchableOpacity = styled.TouchableOpacity`
  padding: 1px;
`;

const newReDesign = (sbpBgColor, props) => {
  const { isChildCarousel } = props;
  const normalWidth = isChildCarousel ? 30 : 70;
  const normalHeight = 1;
  return {
    width: normalWidth,
    height: normalHeight,
    background: sbpBgColor,
  };
};

const getSbpDesign = (isSBP, sbpBgColor) => {
  const sbpWidth = 70;
  const sbpHeight = 3;
  return {
    width: sbpWidth,
    height: sbpHeight,
    background: sbpBgColor,
  };
};

const calculateDotStyle = (props) => {
  const { theme, isSBP, isNewReDesignEnabled } = props;
  const { colorPalette } = theme;
  const width = !props.size ? 8 : props.size;
  const height = !props.size ? 8 : props.size;
  const background = !props.backgroundColor ? colorPalette.gray[700] : props.backgroundColor;
  const borderColor = !props.borderColor ? colorPalette.white : props.borderColor;
  const borderRadius = !props.borderRadius ? 4 : props.borderRadius;
  const borderWidth = !props.borderWidth ? 1 : props.borderWidth;
  // SBP MERGE JIRA SBP-902 STYLING
  const sbpBgColor = colorPalette.gray[2400];
  if (isSBP) return getSbpDesign(isSBP, sbpBgColor);
  if (isNewReDesignEnabled) return newReDesign(sbpBgColor, props);

  return { width, height, background, borderColor, borderRadius, borderWidth };
};

const calculateSelectedDotStyle = (props) => {
  const { theme, isSBP, isNewReDesignEnabled } = props;
  const { colorPalette } = theme;
  const width = !props.selectedSize ? 10 : props.selectedSize;
  const height = !props.selectedSize ? 10 : props.selectedSize;
  const background = !props.selectedBackgroundColor
    ? colorPalette.gray[700]
    : props.selectedBackgroundColor;
  const borderColor = !props.selectedBorderColor
    ? colorPalette.gray[700]
    : props.selectedBorderColor;
  const borderRadius = !props.selectedBorderRadius ? 4 : props.selectedBorderRadius;
  const borderWidth = !props.selectedBorderWidth ? 2 : props.selectedBorderWidth;
  // SBP MERGE JIRA SBP-902 STYLING
  const sbpBgColor = colorPalette.gray[900];
  if (isSBP) return getSbpDesign(isSBP, sbpBgColor);
  if (isNewReDesignEnabled) return newReDesign(sbpBgColor, props);

  return {
    width,
    height,
    background,
    borderColor,
    borderRadius,
    borderWidth,
  };
};

const getDotStyle = (props) => {
  const { selected, isSBP, theme, isNewReDesignEnabled } = props;
  const { spacing } = theme;
  const dotLeftMargin = !props.dotLeftMargin ? spacing.ELEM_SPACING.XXXS : props.dotLeftMargin;
  const dotRightMargin = !props.dotRightMargin ? spacing.ELEM_SPACING.XXXS : props.dotRightMargin;
  const dotmarginBottom = !props.isFullScreenAndroid ? '0' : '50px';
  const values = selected ? calculateSelectedDotStyle(props) : calculateDotStyle(props);

  // SBP MERGE JIRA SBP-902 STYLING
  if (isSBP || isNewReDesignEnabled) {
    return `
      width: ${values.width};
      height: ${values.height};
      background: ${values.background};
      margin-left: -3;
      margin-right: -3;
      margin-bottom: 25;
      margin-top: 20;
      border-width: 0;
  `;
  }

  if (isNewReDesignEnabled) {
    return `
      width: ${values.width};
      height: ${values.height};
      background: ${values.background};
      margin-left: -3;
      margin-right: -3;
      margin-bottom: 10;
      margin-top: 10;
      border-width: 0;
  `;
  }

  return `
    width: ${values.width};
    height: ${values.height};
    border-radius: ${values.borderRadius};
    background: ${values.background};
    border-width: ${values.borderWidth};
    border-color: ${values.borderColor};
    margin-left: ${dotLeftMargin};
    margin-right: ${dotRightMargin};
    margin-bottom: ${dotmarginBottom};
`;
};

const DotComp = styled.View`
  ${getDotStyle}
`;

export { DotContainer, DotTouchableOpacity, DotComp };
