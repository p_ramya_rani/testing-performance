// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import EmailSignupForm from '@tcp/core/src/components/common/organisms/EmailSignupForm';
import SmsSignupForm from '@tcp/core/src/components/common/organisms/SmsSignupForm';
import { getAssetHostURL } from '@tcp/core/src/utils';

const modules = {
  email_signup: EmailSignupForm,
  sms_signup: SmsSignupForm,
};

function DynamicColumns(properties) {
  const Module = modules[properties.formType];
  return <Module {...properties} assetHost={getAssetHostURL()} smsFromPage="landing" />;
}

const ModuleFormWrapper = (props) => {
  const { className } = props;

  return (
    <div className={`${className} moduleFormWrapper`}>
      <DynamicColumns {...props} />
    </div>
  );
};

ModuleFormWrapper.propTypes = {
  className: PropTypes.string,
};

ModuleFormWrapper.defaultProps = {
  className: '',
};

export default ModuleFormWrapper;
