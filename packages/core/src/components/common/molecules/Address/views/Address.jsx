// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles from '../styles/Address.style';
import BodyCopy from '../../../atoms/BodyCopy';

const getAddressfromDiffLines = (address, dataLocatorPrefix) => {
  return (
    <React.Fragment>
      <BodyCopy
        fontFamily="secondary"
        tag="p"
        data-locator={dataLocatorPrefix ? `${dataLocatorPrefix}-addressline1` : ''}
        className="address text-break "
      >
        {address.addressLine1}
      </BodyCopy>
      <BodyCopy
        fontFamily="secondary"
        tag="p"
        data-locator={dataLocatorPrefix ? `${dataLocatorPrefix}-addressline2` : ''}
        className="address text-break "
      >
        {address.addressLine2}
      </BodyCopy>
    </React.Fragment>
  );
};

const getAddessLines = ({ address, dataLocatorPrefix, showWarning }) => {
  return address.addressLine
    .filter(al => al && al.trim() !== '')
    .map((addressLine, index) => {
      const isWarning = showWarning === index + 1;
      return (
        <div className="warning-container">
          {isWarning && <BodyCopy component="p" className="warning-icon" />}
          <BodyCopy
            component="p"
            data-locator={dataLocatorPrefix ? `${dataLocatorPrefix}-addressl${index}` : ''}
            fontFamily="secondary"
            fontWeight={isWarning ? 'extrabold' : 'normal'}
            className="address text-break "
          >
            {addressLine}
          </BodyCopy>
        </div>
      );
    });
};

const getFormattedAddress = (address, dataLocatorPrefix, showWarning) => {
  const isWarning = showWarning === 3;
  return (
    <div className="warning-container">
      {isWarning && <BodyCopy component="p" className="warning-icon" />}
      <BodyCopy
        component="p"
        data-locator={dataLocatorPrefix ? `${dataLocatorPrefix}-cityfullname` : ''}
        fontFamily="secondary"
        fontWeight={isWarning ? 'extrabold' : 'normal'}
        className="address text-break"
      >
        {`${address.city ? `${address.city}, ` : ''}${address.state ? `${address.state} ` : ''}${
          address.zipCode
        }`}
      </BodyCopy>
    </div>
  );
};

const getUserName = ({ address, isDefault, showDefault }) => {
  return `${address.firstName} ${address.lastName}${isDefault && showDefault ? ' (Default)' : ''}`;
};

/**
 * @function Address The address component will render an address
 * that is constructed from the address prop passed.
 * @param {string} className The class name for the component
 * @param {object} address address object
 */

export const Address = ({
  address,
  className,
  dataLocatorPrefix,
  fontWeight,
  showPhone,
  showCountry,
  isDefault,
  showName,
  showDefault,
  parentDataLocator,
  showWarning,
}) => {
  return address ? (
    <BodyCopy
      component="div"
      fontSize="fs14"
      color="text.primary"
      className={className}
      dataLocator={parentDataLocator}
    >
      {showName && (
        <BodyCopy
          component="p"
          id={address.addressId}
          fontWeight={fontWeight}
          fontFamily="secondary"
          className="addressTile__name address text-break"
          data-locator={dataLocatorPrefix ? `${dataLocatorPrefix}-fullname` : ''}
        >
          {getUserName({ address, isDefault, showDefault })}
        </BodyCopy>
      )}
      <BodyCopy component="div" className="addressLine">
        {address.addressLine
          ? getAddessLines({ address, dataLocatorPrefix, showWarning })
          : getAddressfromDiffLines(address, dataLocatorPrefix)}
        {getFormattedAddress(address, dataLocatorPrefix, showWarning)}
        {showCountry && address.country && (
          <BodyCopy component="p" fontFamily="secondary" className="address text-break">
            {address.country}
          </BodyCopy>
        )}
        {showPhone && address.phone1 && (
          <BodyCopy component="p" fontFamily="secondary" className="address text-break">
            {address.phone1}
          </BodyCopy>
        )}
      </BodyCopy>
    </BodyCopy>
  ) : null;
};

Address.propTypes = {
  address: PropTypes.shape({}),
  className: PropTypes.string,
  dataLocatorPrefix: PropTypes.string,
  fontWeight: PropTypes.string,
  showPhone: PropTypes.bool,
  showCountry: PropTypes.bool,
  isDefault: PropTypes.bool,
  showName: PropTypes.bool,
  showDefault: PropTypes.bool,
  parentDataLocator: PropTypes.string,
  showWarning: PropTypes.number,
};

Address.defaultProps = {
  showPhone: true,
  showCountry: true,
  isDefault: false,
  showName: true,
  address: null,
  className: '',
  dataLocatorPrefix: '',
  fontWeight: 'regular',
  showDefault: true,
  parentDataLocator: 'address-details',
  showWarning: 0,
};

export default withStyles(Address, styles);
