// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import BodyCopy from '../../../atoms/BodyCopy';
import { getScreenWidth } from '../../../../../utils/index.native';
import { WarningImage, LineWrapper } from '../styles/Address.style.native';

const warningImg = require('../../../../../../../mobileapp/src/assets/images/circle-warning-fill.png');
const triangleWarningImg = require('../../../../../../../mobileapp/src/assets/images/alert-triangle.png');

const style = {
  maxWidth: getScreenWidth() / 2,
};

const getAddressfromDiffLines = (address, fontSize) => {
  return (
    <React.Fragment>
      {address.addressLine1 ? (
        <BodyCopy
          fontSize={fontSize}
          fontFamily="secondary"
          fontWeight="regular"
          text={address.addressLine1}
          color="gray.900"
        />
      ) : null}
      {address.addressLine2 ? (
        <BodyCopy
          fontSize={fontSize}
          fontFamily="secondary"
          fontWeight="regular"
          text={address.addressLine2}
          color="gray.900"
        />
      ) : null}
    </React.Fragment>
  );
};

const getAddessLines = ({ address, fontSize, showWarning, showProminentWarning }) => {
  return address.addressLine
    .filter((al) => al && al.trim() !== '')
    .map((addressLine, index) => {
      const isWarning = showWarning === index + 1;
      return (
        <LineWrapper>
          {isWarning && (
            <WarningImage
              height="14px"
              width="16px"
              source={showProminentWarning ? triangleWarningImg : warningImg}
            />
          )}
          <BodyCopy
            fontSize={fontSize}
            fontFamily="secondary"
            fontWeight={isWarning ? 'black' : 'regular'}
            text={addressLine}
            color="gray.900"
          />
        </LineWrapper>
      );
    });
};

const getNameFromAddress = (address, showDefaultText, regularName) => {
  const name = `${address.firstName} ${address.lastName} ${showDefaultText ? '(Default)' : ''}`;
  return (
    <BodyCopy
      fontSize="fs14"
      fontFamily="secondary"
      fontWeight={regularName ? 'regular' : 'extrabold'}
      text={name}
      color="gray.900"
      style={style}
    />
  );
};

const renderCityStateZipCode = (fontSize, showWarning, address, showProminentWarning) => {
  const isWarning = showWarning === 3;
  return (
    <LineWrapper>
      {isWarning && (
        <WarningImage
          height="14px"
          width="16px"
          source={showProminentWarning ? triangleWarningImg : warningImg}
        />
      )}
      <BodyCopy
        fontSize={fontSize}
        fontFamily="secondary"
        fontWeight={isWarning ? 'black' : 'regular'}
        text={`${address.city ? `${address.city}, ` : ''}${
          address.state ? `${address.state} ` : ''
        }${address.zipCode}`}
        color="gray.900"
      />
    </LineWrapper>
  );
};

/**
 * @function Address The address component will render an address
 * that is constructed from the address prop passed.
 * @param {string} className The class name for the component
 * @param {object} address address object
 */

const Address = ({
  address,
  dataLocatorPrefix,
  showPhone,
  showCountry,
  showName,
  showDefaultText,
  fontSize,
  regularName,
  showWarning,
  showProminentWarning,
}) => {
  return address ? (
    <View style={style}>
      {showName && getNameFromAddress(address, showDefaultText, regularName)}
      {address.addressLine
        ? getAddessLines({
            address,
            dataLocatorPrefix,
            fontSize,
            showWarning,
            showProminentWarning,
          })
        : getAddressfromDiffLines(address, fontSize)}
      {renderCityStateZipCode(fontSize, showWarning, address, showProminentWarning)}
      {showCountry && !!address.country && (
        <BodyCopy
          fontSize={fontSize}
          fontFamily="secondary"
          fontWeight="regular"
          text={address.country}
          color="gray.900"
        />
      )}
      {showPhone && !!address.phone1 && (
        <BodyCopy
          fontSize={fontSize}
          fontFamily="secondary"
          fontWeight="regular"
          text={address.phone1}
          color="gray.900"
        />
      )}
    </View>
  ) : null;
};

Address.propTypes = {
  showName: PropTypes.bool,
  showDefaultText: PropTypes.bool,
  fontSize: PropTypes.string,
  regularName: PropTypes.bool,
  showWarning: PropTypes.number,
  address: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    addressLine: PropTypes.array,
    city: PropTypes.string,
    state: PropTypes.string,
    zipCode: PropTypes.string,
    country: PropTypes.string,
    phone1: PropTypes.string,
  }),
  dataLocatorPrefix: PropTypes.string,
  showPhone: PropTypes.bool,
  showCountry: PropTypes.bool,
};

Address.defaultProps = {
  showPhone: true,
  showCountry: true,
  showName: true,
  showDefaultText: false,
  fontSize: 'fs14',
  regularName: false,
  showWarning: 0,
  address: {},
  dataLocatorPrefix: '',
};

export default Address;
