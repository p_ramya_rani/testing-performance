// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import Image from '../../../atoms/Image';

const AddressViewWrapper = styled.View`
  height: 130px;
  width: 100%;
`;

export const LineWrapper = styled.View`
  position: relative;
`;

export const WarningImage = styled(Image)`
  position: absolute;
  left: -20px;
`;

export default AddressViewWrapper;
