// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const styles = css`
  .warning-container {
    position: relative;
  }
  .warning-icon {
    position: absolute;
    left: -20px;
    background: transparent
      url(${(props) =>
        props.showProminentWarning
          ? getIconPath('alert-triangle')
          : getIconPath('circle-warning-fill')})
      no-repeat 0 0;
    background-size: contain;
    border: none;
    height: 14px;
    width: 16px;
    margin-right: 7px;
    flex-shrink: 0;
  }
  .addressLine {
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        props.singleLineAddress
          ? `
        display: flex;
        flex-wrap: wrap;

        & > p {
          margin-right: ${props.theme.spacing.ELEM_SPACING.XXS}
        }
      `
          : ``}
    }
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default styles;
