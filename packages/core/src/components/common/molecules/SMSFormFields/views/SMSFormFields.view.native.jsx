// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { Field, change } from 'redux-form';
import { View } from 'react-native';
import getStandardConfig from '../../../../../utils/formValidation/validatorStandardConfig';
import TextBox from '../../../atoms/TextBox';
import InputCheckbox from '../../../atoms/InputCheckbox';
import {
  PhoneWrapper,
  PhoneFieldWrapper,
  StyledText,
  StyledCheckbox,
  StyledMsgWrapper,
  SMSFormFieldsWrapper,
} from '../styles/SMSFormFields.style.native';
import RichText from '../../../atoms/RichText';
import { formatPhoneNumber } from '../../../../../utils/formValidation/phoneNumber';

class SMSFormFields extends React.PureComponent {
  static smsFormFieldsConfig = getStandardConfig(['phoneNumber']);

  handleChange = () => {
    const { dispatch, addressPhoneNo, formName, formSection, handleSMSChange } = this.props;
    /* istanbul ignore else */
    if (dispatch) {
      dispatch(change(formName, `${formSection}.phoneNumber`, addressPhoneNo));
    }
    if (handleSMSChange) {
      handleSMSChange();
    }
  };

  render() {
    const {
      isOrderUpdateChecked,
      labels,
      formSection,
      smsOrderUpdatesRichText,
      createRefParent,
    } = this.props;
    return (
      <SMSFormFieldsWrapper>
        <StyledCheckbox>
          <Field
            name="sendOrderUpdate"
            component={InputCheckbox}
            dataLocator="hide-show-checkbox"
            enableSuccessCheck={false}
            rightText={labels.orderUpdates}
            fontSize="fs14"
            fontFamily="secondary"
            fontWeight="regular"
            marginTopZero
            onChange={() => this.handleChange()}
            isChecked={isOrderUpdateChecked}
          />
        </StyledCheckbox>
        {isOrderUpdateChecked && (
          <View>
            <PhoneWrapper>
              <StyledText> +1 </StyledText>
              <PhoneFieldWrapper>
                <Field
                  label="Phone Number"
                  name="phoneNumber"
                  id={`${formSection}.phoneNumber`}
                  component={TextBox}
                  maxLength={50}
                  keyboardType="phone-pad"
                  dataLocator="phone-number-field"
                  enableSuccessCheck={false}
                  className="phone-field"
                  marginBottom={false}
                  inputRef={node => createRefParent(node, 'phoneNumber')}
                  normalize={formatPhoneNumber}
                />
              </PhoneFieldWrapper>
            </PhoneWrapper>
            <StyledMsgWrapper>
              {smsOrderUpdatesRichText && <RichText source={{ html: smsOrderUpdatesRichText }} />}
            </StyledMsgWrapper>
          </View>
        )}
      </SMSFormFieldsWrapper>
    );
  }
}

SMSFormFields.propTypes = {
  isOrderUpdateChecked: PropTypes.bool,
  labels: PropTypes.shape({}).isRequired,
  dispatch: PropTypes.func,
  addressPhoneNo: PropTypes.number,
  formName: PropTypes.string,
  formSection: PropTypes.string,
  handleSMSChange: PropTypes.string,
  smsOrderUpdatesRichText: PropTypes.string,
  createRefParent: PropTypes.func,
};

SMSFormFields.defaultProps = {
  isOrderUpdateChecked: false,
  dispatch: () => {},
  addressPhoneNo: null,
  formName: '',
  formSection: '',
  handleSMSChange: null,
  smsOrderUpdatesRichText: PropTypes.string,
  createRefParent: () => {},
};

export default SMSFormFields;
