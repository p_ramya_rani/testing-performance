// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { StyleSheet } from 'react-native';
import { Anchor } from '../../../atoms';

const getAdditionalStyle = (props) => {
  const { margins } = props;
  return {
    ...(margins && { margin: margins }),
  };
};

const styles = css`
  ${(props) =>
    props.isNewReDesignProductSummary
      ? `
  margin-top: 0;
  margin-bottom: 0;
  `
      : `
  margin-top: ${props.theme.spacing.LAYOUT_SPACING.XS};
  margin-bottoms ${props.isOutfitPage ? '0' : props.theme.spacing.LAYOUT_SPACING.SM}
  `}
`;

export const RowViewContainer = styled.View`
  flex-direction: row;
  align-items: center;
  ${getAdditionalStyle};
`;

export const RowSelectContainer = styled.View`
  flex: 0.8;
`;

export const NewContainerForPadding = styled.View`
  padding: 0;
`;

const bottomStyle = (props) => {
  let value = props.theme.spacing.ELEM_SPACING.XXL;
  const comingFromPage = (props?.fromPage && props?.fromPage?.toLowerCase()) || '';
  if (props.isNewReDesignProductSummary) {
    value = props.theme.spacing.ELEM_SPACING.MED;
  } else if (comingFromPage === 'plp' || comingFromPage === 'pdp' || comingFromPage === 'slp') {
    value = 0;
  }
  return value;
};

export const MultipackTabWrapperTab = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 100%;
  margin-bottom: ${() => bottomStyle};
  ${getAdditionalStyle};
`;

export const MultiPackBtn = styled.View`
  flex: 1;
  border: ${(props) => (props.softDisablePill === 'soft-disable' ? 'dotted' : 'solid')} 1px
    ${(props) => props.theme.colorPalette.gray[900]};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  width: auto;
  margin-right: 10px;
  border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  ${(props) =>
    props.index === props.activeIndex
      ? `
    background: ${props.theme.colorPalette.black};
    color:${props.theme.colorPalette.white};
          `
      : ''};
  ${(props) => (props.disabled ? `opacity:0.3 ` : '')};
  ${(props) =>
    props.softDisablePill === 'soft-disable' && props.index !== props.activeIndex
      ? `opacity:0.5 `
      : ''};
`;

export const MultiPackBtnRedesign = styled.View`
  margin: 5px 0 0px 0;
`;

export const StyledAnchor = styled(Anchor)`
  padding: ${(props) => props.paddingTop || '0px'} ${(props) => props.paddingRight || '0px'}
    ${(props) => props.paddingBottom || '0px'} ${(props) => props.paddingLeft || '0px'};
  align-items: center;
  justify-content: center;
`;

export const ErrorMsg = styled.Text`
  color: ${(props) => props.theme.colorPalette.red[500]};
`;

export const SimilarLink = styled.View`
  background: ${(props) => props.theme.colorPalette.gray[300]};
  height: 80px;
  flex-direction: row;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  position: relative;
  top: -${(props) => props.theme.spacing.ELEM_SPACING.XXL};
  ${getAdditionalStyle};
`;

export const SoldoutWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  width: 50%;
  ${getAdditionalStyle};
`;

export const AnchorWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  width: 50%;
`;

export const SizeViewContainer = styled.View`
  position: relative;
  ${(props) => (props.isSBP ? `margin-bottom: 40px` : '')}
`;

export const ProductAddToBagStyle = StyleSheet.create({
  hitSlop: { bottom: 6, left: 12, right: 12, top: 6 },
  image: {
    height: 90,
    width: 90,
  },
  plpAddToBtnInDrawer: {
    marginBottom: 10,
    marginTop: 15,
  },
  view1: {
    position: 'absolute',
    right: -25,
    top: -10,
    transform: [{ rotateY: '180deg' }],
  },
  view2: {
    marginBottom: '5%',
  },
  view3: {
    height: 15,
  },
});

export const QuantitySizeViewContainer = styled.View`
  margin-bottom: ${(props) => (props.isFromBundlePage ? '16px' : '25px')};
  padding-left: 0px;
  padding-right: 0px;
  ${(props) =>
    props.isOutfitPage
      ? `width: 60%; margin-top: ${props.theme.spacing.ELEM_SPACING.SM}; margin-bottom: ${props.theme.spacing.ELEM_SPACING.SM}`
      : ``}
`;

export const ProductQuantityButton = styled.View`
  flex-direction: row;
  flex: ${(props) => (props.isFromBundlePage ? 0.4 : 0.85)};
  padding: 9px 0px;
  border: 1px solid ${(props) => props.theme.colors.PRIMARY.LIGHTERGRAY};
  border-radius: ${(props) => (props.theme.isGymboree ? '23px' : '16px')};
`;

export const ProductQuantityPlusButton = styled.View`
  flex: 0.45;
  justify-content: center;
  align-items: center;
`;

export const ProductQuantityTextButton = styled.View`
  flex: 0.3;
  justify-content: center;
`;
export const ProductQuantityMinusButton = styled.View`
  flex: 0.45;
  justify-content: center;
  align-items: center;
`;

export const ProductQuantitySizeView = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const VerticalLine = styled.View`
  height: 80%;
  width: 1;
  margin-top: 5px;
  background-color: ${(props) => props.theme.colors.PRIMARY.LIGHTERGRAY};
`;

const additionalStylingQuantityAddToBagContainer = (props) => {
  const { fromPage } = props;
  if (fromPage === 'PDP') {
    return `margin-bottom: ${props.theme.spacing.ELEM_SPACING.SM}`;
  }
  return ``;
};

export const QuantityAddToBagContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  height: 80px;
  ${additionalStylingQuantityAddToBagContainer}
`;

export const AddToBagWrapper = styled.View``;

export const QuantityButtonWrapper = styled.View`
  width: 40%;
`;

export const AddToBagButtonWrapper = styled.View`
  width: 60%;
`;

const additionalAtbButtonBigStyling = (props) => {
  const { isPdpNewReDesign, isGiftCard } = props;
  if (isPdpNewReDesign && isGiftCard) {
    return `
      bottom: 15px
      margin-top: 15px
    `;
  }
  return ``;
};

export const AtbButtonBig = styled.TouchableOpacity`
  bottom: 36px;
  height: 100%;
  ${(props) =>
    props.isPdpNewReDesign
      ? `
  margin-top: 36px;
  `
      : `
  opacity: ${props.disabled ? 0.3 : 1};
  `}
  ${additionalAtbButtonBigStyling}
`;

export const EditSectionHeadingWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
  margin-top: 10px;
`;
export const TextWrapper = styled.View`
  flex-direction: row;
`;
export const TextWrapperMain = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
export const TextWrapperNew = styled.View`
  flex-direction: row;
  margin-top: 5px;
  margin-left: 2px;
`;
export const SizeTextWrapper = styled.View`
  margin-top: 12px;
  margin-bottom: 12px;
`;
export const EditWrapper = styled.View`
  margin-top: 10px;
  margin-bottom: 10px;
`;
export const DIVIDERLINE = styled.View`
  margin-top: 5px;
  height: 1px;
  background-color: ${(props) => props.theme.colorPalette.gray['2700']};
`;
export default styles;
