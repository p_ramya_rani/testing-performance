/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const styles = css`
  margin-top: 10px;
  width: 100%;
  .edit-form-css {
    display: flex;
    flex: ${(props) => (props.disableSwappingMpackPills ? 1 : 'unset')};
    width: 100%;
    align-items: flex-start;
    margin-left: 0;
    margin-right: 0;
    margin-top: 14px;
  }
  .multipack-pills-css {
    margin-top: 0;
  }
  .multipack-tab-edit-css {
    margin-right: -14px;
    overflow-x: auto;
    overflow-y: hidden;
    white-space: nowrap;
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-right: -14px;
      width: auto;
    }
  }
  .edit-form-css-width {
    width: 100%;
  }
  .outfit-button-wrapper-desktop {
    display: none;
  }
  .new-gift-card-pdp {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `border-right: ${props.theme.spacing.ELEM_SPACING.SM} solid  ${props.theme.colors.PRIMARY.PALEGRAY};
        border-left: ${props.theme.spacing.ELEM_SPACING.SM} solid  ${props.theme.colors.PRIMARY.PALEGRAY};
        margin: 0px -${props.theme.spacing.ELEM_SPACING.REG};background: ${props.theme.colors.PRIMARY.PALEGRAY};`
          : ``}
    }
  }
  .dotted-line {
    border: 1px dashed ${(props) => props.theme.colors.BLACK};
    opacity: 0.59;
  }
  .button-wrapper {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: 0px;
    }
  }
  .button-wrapper.quick-view-drawer-redesign {
    margin: 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-bottom: 13px;
    }
  }
  .button-wrapper.quick-view-drawer-redesign.giftCard {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `border: ${props.theme.spacing.ELEM_SPACING.SM} solid  ${props.theme.colors.PRIMARY.PALEGRAY};
        margin: 0px -${props.theme.spacing.ELEM_SPACING.REG};
        padding: ${props.theme.spacing.ELEM_SPACING.XS};
        border-radius: ${props.theme.spacing.ELEM_SPACING.MED_1};background: ${props.theme.colors.WHITE};
        margin-top: ${props.theme.spacing.ELEM_SPACING.LRG};`
          : ``}
    }
  }
  .multipack-tab button.multipack-bg {
    color: ${(props) => props.theme.colors.WHITE};
    background-color: ${(props) => props.theme.colorPalette.gray[900]};
    opacity: 1;
  }

  .multipack-tab::-webkit-scrollbar {
    display: none;
  }
  button.soft-disable {
    border: 1px dotted gray;
    opacity: 0.4;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    button.soft-disable:hover {
      opacity: 1;
      border: 1px dotted rgba(128, 128, 128, 0.6);
    }
  }
  ul.multipack-tab {
    padding-top: ${(props) => (props.isPDP ? '10px' : props.theme.spacing.ELEM_SPACING.LRG)};
    padding-bottom: ${(props) => (props.isPDP ? '6px' : props.theme.spacing.ELEM_SPACING.MED)};
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    line-height: normal;
    letter-spacing: normal;
    text-align: left;
    color: ${(props) => props.theme.colorPalette.gray[900]};
    @media ${(props) => props.theme.mediaQuery.medium} {
      white-space: normal;
      width: 100%;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      display: flex;
      flex-wrap: wrap;
      flex: 1;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .multipack-tab-edit-css.tooltip-style {
      display: inline-table;
    }
  }
  .multipack-tab li {
    display: inline-block;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    vertical-align: top;
    white-space: normal;
  }
  .multipack-tab.quick-view-drawer-redesign {
    padding-top: 10px;
    padding-bottom: 0px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
      padding-bottom: 10px;
    }
  }
  .multipack-tab.quick-view-drawer-redesign li {
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM}
      ${(props) => props.theme.spacing.ELEM_SPACING.SM}
      ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin: 0 10px 0 0;
    }
  }
  .add-to-bag-section-wrapper {
    bottom: 0;
    position: sticky;
    background-color: ${(props) => props.theme.colors.WHITE};
    padding: 20px 22px 19px;
    box-shadow: 0 0 6px 0 rgb(0 0 0 / 22%);
    margin: 0px -6px -8px -22px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin: 0px 0px -8px -17px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin: 0px -13px 0px -13px;
    }
  }
  .newPDPWrapper {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        !props?.currentProduct?.isGiftCard &&
        ` border-radius: 6px;
      box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.12);
      background: ${props.theme.colors.WHITE};
      margin-bottom: 12px;`}
    }
  }
  .button-find-in-store-quick-view-drawer-redesign {
    width: 100%;
    border-radius: 6px;
    background-color: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    color: ${(props) => props.theme.colors.WHITE};
    border: none;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED} 20px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 20px;
      ${(props) => (props.isNewPDPEnabled ? 'margin-right: 10px;' : '')}
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `width: 232px; max-width:232px; max-height: 42px;
         justify-content:center; align-items:center;border-radius: 6px;top:17px;
         background: ${props.theme.colors.TEXT.DARKBLUE}; padding-top: 12px;`
          : ``};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.isNewPDPEnabled ? `width: 357px; max-width:357px;` : ``)};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding: 15px 20px;
      ${(props) =>
        props.isNewPDPEnabled
          ? `max-width: none;
          width: 100%;`
          : ``};
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `width: 100%;border-radius: 6px;height: 42px;
      margin-top: ${props.theme.spacing.ELEM_SPACING.MED};`
          : ``};
    }
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .button-find-in-store-quick-view-drawer-redesign.disable-add-to-bag {
      ${(props) =>
        props.isNewPDPEnabled &&
        'width: 100%;border-radius: 6px;background-color: rgba(22, 47, 66, 0.6);'}
    }
  }
  .btn-find-in-store-wrapper-quick-view-drawer-redesign {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => (props.isNewPDPEnabled ? `0px 0px 13px 0px` : `0px 0px 13px 5px`)};
    }
  }
  .multipack-tab button {
    border: solid 1px
      ${(props) =>
        props.isNewQVEnabled
          ? props.theme.colors.PRIMARY.GRAY
          : props.theme.colorPalette.gray[900]};
    color: ${(props) => props.theme.colorPalette.gray[900]};
    background: none;
    outline: none;
    cursor: pointer;
  }
  .multipack-tab button:disabled {
    opacity: 0.3;
    cursor: not-allowed;
  }
  .mpack-btn {
    font-size: 10px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
    padding: 4px;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    display: inline-block;
    text-transform: capitalize;
    text-align: center;
    letter-spacing: 0.42px;
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    height: 32px;
    min-width: 80px;
  }

  .mpack-btn.quick-view-drawer-redesign {
    height: 42px;
    min-width: 78px;
    font-size: 14px;
    padding: 12px 11px 11px 12px;
    outline: solid 3px transparent;
    color: ${(props) => props.theme.colors.BLACK};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 32px;
      padding: 7px 12px 6px;
    }
  }

  .mpack-btn.enable {
    font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  }

  .mpack-btn.multipack-bg {
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
  }

  .mpack-btn.quick-view-drawer-redesign.multipack-bg {
    color: ${(props) => props.theme.colors.BLACK};
    background-color: ${(props) => props.theme.colorPalette.white};
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    border-radius: 6px;
    box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.12);
    border: solid 3px ${(props) => props.theme.colors.BRAND.PRIMARY};
    outline: solid 3px transparent;
    border-style: solid !important;
    padding-top: 5px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-top: 9px;
    }
  }

  .mpack-btn-swap {
    font-weight: bold;
    padding: 12px 28px 13px 29px;
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    height: 45px;
    min-width: 110px;
  }
  .check {
    display: inline-block;
    transform: rotate(45deg);
    height: 15px;
    width: 7px;
    border-bottom: 3px solid ${(props) => props.theme.colorPalette.green[500]};
    border-right: 3px solid ${(props) => props.theme.colorPalette.green[500]};
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .add-to-bag-button.add-to-bag-button.disable-add-to-bag {
      ${(props) =>
        props.isNewPDPEnabled &&
        'background-color: rgba(22, 47, 66, 0.6);border-radius: 6px;white-space: pre;'}
    }
  }
  .add-to-bag-button.add-to-bag-button.disable-add-to-bag {
    ${(props) => props.isNewPDPEnabled && 'white-space: pre;'}
  }

  .add-to-bag-button.add-to-bag-button {
    width: 100%;
    margin-top: ${(props) => (props.socialProofMessage ? '0px' : '')};
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `max-width:232px; width: 232px; max-height: 42px;
         justify-content:center; align-items:center;border-radius: 6px;top:17px;
         background: ${props.theme.colors.TEXT.DARKBLUE}; padding-top: 12px;white-space: pre;`
          : ``};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.isNewPDPEnabled ? `width: 357px; max-width:357px;` : ``)};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) => (props.isNewPDPEnabled ? `max-width: none; width: 100%;` : ``)};
    }

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
      ${(props) => (props.socialProofMessage?.variation === 'bottom' ? 'margin-top:11px;' : '')};
      ${(props) =>
        props.isNewPDPEnabled
          ? `margin-right:10px;max-height: 42px;border-radius: 6px;
        margin-top: ${props.theme.spacing.ELEM_SPACING.MED};white-space: pre;`
          : ``};
    }
    :focus {
      background-color: ${(props) => props.theme.colorPalette.blue.C900};
    }
  }
  .sticky-atc-wrapper {
    height: 80px;
    position: fixed;
    bottom: 0;
    width: 100%;
    z-index: ${(props) => props.theme.zindex.zCondensedHeader};
    background-color: ${(props) => props.theme.colorPalette.white};
    left: 0;
    .social-proof-wrapper {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
    .social-proof-text {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        ${(props) =>
          !props.top
            ? `margin: 0px  ${props.theme.spacing.ELEM_SPACING.REG}; display: flex;width: 372px;justify-content: center;`
            : ''};
      }
    }
  }
  .sticky-atc-wrapper.socialProof {
    height: auto;
    padding-bottom: 4px;
  }
  .sticky-atc-wrapper .stickyATC {
    margin-top: 20px;
    left: 14px;
    width: 93% !important;
    display: block;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => props.isNewPDPEnabled && `max-width: unset;`}
    }
  }
  .new-pdp-qty-selector {
    margin-right: 10px;
  }

  .sticky-atc-wrapper .new-pdp-qty-selector {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }

  .social-proof-message-atb .stickyATC {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => (!props.top ? 'margin-top:0px;' : '')};
    }
  }

  .pdp-redesign-wrapper {
    display: flex;
    padding-bottom: 14px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-left: ${(props) => (!props?.currentProduct?.isGiftCard ? '10px' : '')};
    }
  }
  .pdp-redesign-wrapper.store-btn-redesign {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => (props.isNewPDPEnabled ? `padding-bottom:27px` : ``)};
    }
  }

  .add-to-bag-button {
    max-width: 450px;
    text-align: center;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
    font-size: ${(props) => props.theme.fonts.fontSize.listmenu.large}px;
    color: ${(props) => props.theme.colorPalette.white};
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    font-stretch: normal;
    line-height: normal;
    font-style: normal;
    letter-spacing: ${(props) => props.theme.typography.letterSpacings.ls1};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    padding-left: 123px;
    padding-right: 124px;
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};

    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        !props.isQuickView
          ? `padding-left: 90px;
              padding-right: 89px;`
          : `padding-left: 65px;
                  padding-right: 65px;`};
    }

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-left: 90px;
      padding-right: 89px;
      ${(props) =>
        props.isNewPDPEnabled &&
        `padding-left: 58px;
      padding-right: 57px;`}
    }

    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 100%;
      padding-left: 10px;
      padding-right: 10px;
      padding-bottom: 15px;
      padding-top: 15px;
    }

    &:hover {
      background-color: ${(props) => props.theme.colors.BUTTON[props.fill || 'BLUE'].HOVER};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-left: 0px;
      padding-right: ${(props) =>
        props.isNewPDPEnabled
          ? props.theme.spacing.ELEM_SPACING.LRG_2
          : props.theme.spacing.ELEM_SPACING.XXL};
      padding-left: ${(props) =>
        !props.isNewPDPEnabled
          ? props.theme.spacing.ELEM_SPACING.XXL
          : props.theme.spacing.ELEM_SPACING.LRG_2};
      padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .style-with-btn {
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    .add-to-bag-button {
      position: absolute;
      left: 0;
    }
  }

  .added-to-bag {
    background-color: ${(props) =>
      props.addBtnMsg || props.showOutfitCarouselAddedCTA
        ? 'white'
        : props.theme.colorPalette.blue.C900};
    ${(props) =>
      props.addBtnMsg || props.showOutfitCarouselAddedCTA
        ? `border:1px solid ${props.theme.colorPalette.green[500]}`
        : ' '};
    color: ${(props) =>
      props.addBtnMsg || props.showOutfitCarouselAddedCTA
        ? props.theme.colorPalette.green[500]
        : props.theme.colorPalette.white};
    padding-top: 10px;
    padding-left: initial;
    padding-right: initial;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-top: 6px;
    }
  }

  .added-to-bag-sticky {
    background-color: ${(props) => props.theme.colorPalette.white};
    border: 1px solid ${(props) => props.theme.colorPalette.green[500]};
    color: ${(props) => props.theme.colorPalette.black};
    padding-top: 10px;
    padding-left: initial;
    padding-right: initial;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-top: 6px;
      width: 93% !important;
      left: 14px;
      margin-top: 18px;
    }
  }
  .added-to-bag:hover,
  .added-to-bag-sticky:hover {
    background-color: ${(props) => props.theme.colorPalette.white};
  }
  .select-value-wrapper {
    display: flex;
    flex: 1;
    flex-wrap: wrap;
    .select__input {
      background-repeat: no-repeat;
      background-position: right center;
      height: auto;
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      padding-left: 0px;
    }
    .customSelectTitle {
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      img {
        border: solid 0.4px ${(props) => props.theme.colors.BLACK};
        margin-right: 5px;
        border-radius: 5px;
      }
    }
    .button-wrapper {
      display: flex;
      flex-direction: column;
      justify-content: space-between;

      button[type='submit'] {
        font-size: ${(props) => props.theme.fonts.fontSize.body.small.secondary}px;
      }
    }

    .size-find-in-store {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      text-decoration: underline;
      cursor: pointer;

      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      }
    }

    .size-unavailable {
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
      border-bottom: solid 1px ${(props) => props.theme.colorPalette.gray[500]};
      border-top: solid 1px ${(props) => props.theme.colorPalette.gray[500]};
      width: 100%;
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};

      .unavailable-text {
        color: ${(props) => props.theme.colorPalette.gray[900]};
        font-size: ${(props) => props.theme.typography.fontSizes.fs12};
        margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      }
    }
  }
  .size-unavailable-anchor {
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-weight: ${(props) => props.theme.fonts.fontWeight.regular};
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  }
  .color-selector {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    cursor: auto;
    width: 100%;
    .dropdownDivOverFlow {
      display: inline-block;
      overflow-y: auto;
      max-height: 150px;
      border: 2px solid ${(props) => props.theme.colors.PRIMARY.DARK};
      .dropdownUlBorder {
        border: none;
      }
    }
  }
  .please-enter-an-error {
    position: relative;
  }

  .fit-selector {
    margin-top: ${(props) => (props.isNewQVEnabled ? '15px' : '34px')};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) =>
        props.isNewQVEnabled ? `${props.theme.spacing.ELEM_SPACING.MED_1}` : '34px'};
    }
    .input-radio-title {
      margin-bottom: 0;
    }
  }
  .fit-selector-atb {
    margin-top: 0;
    width: 100%;
  }
  .size-selector {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    width: 100%;
    position: relative;
    margin-top: ${(props) => (props.isPDP ? '26px' : '32px')};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) => props.isNewQVEnabled && `${props.theme.spacing.ELEM_SPACING.MED_1}`};
    }
    .size-chart {
      position: absolute;
      right: 0;
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      text-decoration: underline;
      cursor: pointer;
    }
  }
  .size-chart-btn-pdp {
    position: absolute;
    margin-left: 62px;
    font-size: 12px;
    text-decoration: underline;
    cursor: pointer;
  }

  .size-chart-quick-view-drawer-redesign {
    margin-left: 12px;
    height: 44px;
    max-width: 208px;
    display: flex;
    padding: 12px 11px 12px 16px;
    border-radius: 6px;
    background-color: ${(props) => props.theme.colors.WHITE};
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-left: 33px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 38px;
      min-height: 38px;
      max-width: 160px;
      padding: 9px 11px 9px 16px;
      ${(props) => props.isNewPDPEnabled && `right: 0px; position: absolute;`}
    }
  }
  .size-chart-pdp-redesign {
    display: flex;
    border-radius: 6px;
    border: none;
  }
  .qty-selector-qv {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: flex;
      flex-wrap: wrap;
    }
  }
  .size-chart-quick-view-drawer-redesign:focus {
    background-color: ${(props) => props.theme.colors.WHITE};
    outline: none;
  }
  .size-chart-quick-view-drawer-redesign:hover {
    background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  }
  .size-chart-ruler {
    margin-right: 7px;
  }
  .size-chart-label {
    white-space: nowrap;
    text-transform: capitalize;
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    @media ${(props) => props.theme.mediaQuery.medium} {
      color: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
      ${(props) =>
        props.isNewPDPEnabled ? `font-size: ${props.theme.typography.fontSizes.fs14};` : ''}
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled ? `font-size: ${props.theme.typography.fontSizes.fs12};` : ''}
    }
  }
  .view-product-details-qv-redesign {
    ${(props) => (props.isNewQVEnabled ? `display:flex; justify-content:center;` : ``)}
    ${(props) => !props.isPDP && `${props.fromBagPage ? 'padding:6px 0' : 'padding:24px 0'};`}
  }
  .link-redirect {
    ${(props) => (props.isNewQVEnabled ? `display:inline-block;` : ``)}
  }
  .size-chart-arrow {
    margin-left: 54px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: 16px;
    }
  }
  .fulfillment-section-wrapper.gift-card-new-pdp {
    ${(props) => (props.isNewPDPEnabled ? `display: none;` : '')}
  }
  .quantity-size-chart {
    display: flex;
    padding-top: 16px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      justify-content: space-between;
      flex-grow: 100;
    }
  }
  .size-selector-atb {
    margin-top: 0;
    width: 100%;
  }

  .size-error {
    margin-top: ${(props) => (props.isNewQVEnabled ? '10px' : props.theme.spacing.ELEM_SPACING.XS)};
    color: ${(props) => props.theme.colors.NOTIFICATION.ERROR};
    width: 100%;
    font-size: ${(props) => props.theme.fonts.fontSize.body.small.secondary}px;
    font-weight: 800;
    display: inline-flex;
    position: absolute;
    top: -8px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => props.isNewPDPEnabled && `top: -12px;`}
    }
  }
  .size-error-section {
    display: flex;
    flex: 1;
  }
  .default-error {
    display: inline-flex;
    margin-top: ${(props) => (props.isNewQVEnabled ? '24px' : '')};
  }
  .size-error-message {
    margin-top: -2px;
    color: ${(props) => props.theme.colors.NOTIFICATION.ERROR};
    width: 100%;
    font-size: ${(props) => props.theme.fonts.fontSize.body.small.secondary}px;
    font-weight: 800;
  }
  .size-and-fit-detail-title,
  .color-chips-selector-title,
  .image-chips-selector-title,
  .color-chips-selector-title-name,
  .image-chips-selector-title-name,
  .size-and-fit-detail-title-name {
    font-size: ${(props) => props.theme.fonts.fontSize.listmenu.large}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: ${(props) => props.theme.colors.PRIMARY.DARK};
    text-transform: uppercase;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  }
  .size-and-fit-detail-title.quick-view-drawer-redesign {
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    padding-right: 16px;
  }
  .image-chips-selector-container.image-chips-selector {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-right: -14px;
    }
  }
  .oos-similar-block {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: -14px;
      margin-right: -14px;
      background: ${(props) => props.theme.colorPalette.gray[300]};
    }
  }
  .oos-similar-items {
    position: relative;
    padding-bottom: 29px;
    padding-top: 29px;
    border-top: solid 1px ${(props) => props.theme.colorPalette.gray[1600]};
    border-left: 0;
    border-right: 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      border-top: none;
      margin-left: 14px;
      margin-right: 14px;
      margin-bottom: 20px;
    }
  }
  .oos-similar-items-title {
    font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    font-weight: bold;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs20};
    }
  }
  .oos-similar-items-link {
    position: absolute;
    right: 0;
    color: ${(props) => props.theme.colors.TEXT.BLUE};
    text-decoration: underline;
    cursor: pointer;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
  }
  .color-chips-selector-title-name,
  .image-chips-selector-title-name,
  .size-and-fit-detail-title-name {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: normal;
    margin-left: 6px;
    font-stretch: normal;
  }
  .qty-selector,
  .product-property-section {
    display: inline-flex;
    margin-top: ${(props) => (props.isPDP ? '22px' : '29px')};
    width: 100%;
    #quantity {
      font-size: ${(props) => props.theme.fonts.fontSize.anchor.xlarge}px;
      padding-top: 0;
      padding-bottom: 0;
      width: 48px;
      margin-top: -22px;
    }
    p {
      display: inline-block;
      vertical-align: top;
    }
  }
  .qty-selector.socialProof {
    margin-bottom: 10px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      max-width: 130px;
    }
  }
  .social-proof-wrapper-qty {
    white-space: nowrap;
    display: flex;
    width: 590px;
  }
  .social-proof-desktop-bottom {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      .social-proof-wrapper {
        margin: 24px 12px 0 12px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      .social-proof-wrapper,
      .social-proof-text {
        margin-left: 0;
      }
    }
  }
  .pdp-qty {
    font-size: ${(props) => props.theme.fonts.fontSize.listmenu.large}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    color: #3b3b3b;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    padding-top: 9px;
    text-transform: uppercase;
  }
  .pdp-qty.quick-view-drawer-redesign {
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    color: ${(props) => props.theme.colors.PRIMARY.DARK};
    padding-top: 32px;
  }

  .pdp-qty + p {
    margin-top: 7px;
    height: ${(props) => (props.isPDP ? '28px' : '73px')};
    ${(props) =>
      props.isPickup && props.isNewQVEnabled && !props.isPDP
        ? 'height: 20px; margin-top: 9px;'
        : ''}
  }
  .container {
    width: 207px;
    height: 42px;
    border: 1px solid ${(props) => props.theme.colors.TEXT.GRAY};
    border-radius: 8px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 107px;
      height: ${(props) => (!props.isNewPDPEnabled ? '36px' : '40px')};
    }
  }
  .container.pdpRedesign {
    width: 110px;
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 171px;
    }
  }

  .horizontalBar,
  .horizontalBarVertical {
    width: 16px;
    height: 1px;
    background-color: ${(props) => props.theme.colorPalette.black};
    position: absolute;
    top: 50%;
    left: 50%;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 14px;
    }
  }
  .horizontalBarVertical {
    transform: rotate(90deg);
  }

  .increaseButton.pdpRedesign.disabled > .horizontalBar {
    ${(props) => props.isNewPDPEnabled && `background-color:${props.theme.colors.PRIMARY.GRAY};`}
  }
  .increaseButton.pdpRedesign.disabled > .horizontalBarVertical {
    ${(props) => props.isNewPDPEnabled && `background-color:${props.theme.colors.PRIMARY.GRAY};`}
  }

  .decreaseButton {
    width: 38px;
    height: 27px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    margin-left: 10px;
    &.disabled {
      pointer-events: none;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 30px;
      height: 26px;
      margin-left: 0px;
      padding-right: 6px;
    }
  }
  .decreaseButton.pdpRedesign {
    margin-left: 0px;
  }
  .increaseButton.pdpRedesign {
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
  #quantity-new-qv {
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-left: ${(props) => (props.isNewPDPEnabled ? props.theme.spacing.ELEM_SPACING.XS : '')};
    }
  }
  .selected_qty_value {
    font-size: 16px;
    color: ${(props) => props.theme.colorPalette.black};
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
  }
  .increaseButton {
    width: 38px;
    height: 27px;
    position: relative;
    cursor: pointer;
    margin-right: 6px;
    &.disabled {
      pointer-events: none;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 30px;
      height: 26px;
    }
  }

  .decreaseButtonContent {
    height: 1px;
    width: 19px;
    background-color: ${(props) => props.theme.colorPalette.black};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 12px;
    }
  }
  .decreaseButton.pdpRedesign.disabled > .decreaseButtonContent {
    ${(props) => props.isNewPDPEnabled && `background-color:${props.theme.colors.PRIMARY.GRAY};`}
  }
  .size-field {
    height: auto;
    width: auto;
  }
  .quick-view-drawer-redesign.size-and-fit-detail-items-list {
    width: calc(100% + 10px);
  }
  .size-and-fit-detail-item.quick-view-drawer-redesign.item-selected-option {
    margin-top: 3px;
  }
  .product-size-error {
    margin-top: 30px;
  }
  .size-field-error {
    height: auto;
    .select__input {
      border-bottom: 2px solid ${(props) => props.theme.colors.NOTIFICATION.ERROR};
    }
  }
  .error-image {
    height: 12px;
    padding-right: 6px;
  }

  .tick-icon {
    width: 27px;
    height: 29px;
    display: inline-block;
    vertical-align: middle;
    position: relative;
    margin-right: 5px;
    background: url('${(props) => getIconPath('check-green', props)}');
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;
export const giftCardDesignStyle = css`
  .color-chips-selector-item {
    height: 126px;
    width: 103px;
    border-radius: 0;

    .input-radio-title {
      height: 100%;
      display: flex;
      align-items: center;
    }

    /* Image color of item */
    .color-image {
      border-radius: 0;
      height: auto;
      width: 100%;
    }

    /* When the input is checked, the image color has black border (selected) */
    .input-radio-icon-checked + .input-radio-title {
      border: 1px solid ${(props) => props.theme.colors.BORDER.NORMAL};
      padding: 2px 2px;
    }

    .input-radio-icon-checked + .input-radio-title .color-image {
      border-radius: 0;
      border: 0;
      height: auto;
      width: 100%;
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .color-chips-selector-item {
      height: 79px;
      width: 66px;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .color-chips-selector-item {
      height: 109px;
      width: 90px;
    }
  }
`;

export default styles;
