// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components/native';
import { StyleSheet } from 'react-native';
import { colorNero } from '../../../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.styles';

const style = css`
  padding-top: 10px;
`;

export const SizeChartButton = styled.View`
  ${props => (props.isSBP ? `position: relative` : `position: absolute`)};
  ${props => (props.isSBP ? `width: 110` : ``)};
  ${props => (props.isSBP ? `` : `right: 0`)};
  z-index: ${props => props.theme.zindex.zLoader};
  border: ${props => (props.isSBP ? `1px solid ${colorNero}` : `none`)};
  border-radius: ${props => (props.isSBP ? `6px` : `0px`)};
`;

export const SizeChartStyle = StyleSheet.create({
  image: {
    height: 17,
    marginRight: '2%',
    width: 20,
  },
  view1: {
    flexDirection: 'row',
    padding: 5,
  },
});

export default style;
