// 9fbef606107a605d69c0edbcd8029e5d
/** @module AlternateSizes
 * @summary renders a Alternate sizes for products of other category.
 *
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import { ScrollView } from 'react-native-gesture-handler';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { Anchor, BodyCopy } from '@tcp/core/src/components/common/atoms';
import CustomButton from '@tcp/core/src/components/common/atoms/ButtonRedesign';

import {
  AlternateSizeButton,
  AlternateSizeLink,
  AnchorWrapper,
  AlternateSizeButtonRedesign,
  TitleContainer,
  OtherSizePillsContainer,
} from './styles/AlternateSizes.style.native';
import { showSBPLabels } from '../../../../../../../../mobileapp/src/components/features/shopByProfile/PLP/helper';
import {
  colorNero,
  fontFamilyNunitoBlack,
} from '../../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.styles';

const rightArrowImage = require('../../../../../../../../mobileapp/src/assets/images/isNewRedesign/icons-medium-left.png');
const babySizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-baby.png');
const bigGirlSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-girl.png');
const bigBoySizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-boy.png');
const toddlerGirlSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-girl.png');
const toddlerBoySizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-toddler-boy.png');
const momSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-women.png');
const dadSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-men.png');
const dogSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-dog.png');
const dollSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-dog.png');
const bigKidSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-toddler-unisex.png');
const toddlerSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-toddler-unisex.png');
const AdultSizes = require('../../../../../../../../mobileapp/src/assets/images/otherSizes/icons-women.png');

const icons = [
  { name: 'baby', var: babySizes },
  { name: 'big girl', var: bigGirlSizes },
  { name: 'big boy', var: bigBoySizes },
  { name: 'toddler girl', var: toddlerGirlSizes },
  { name: 'toddler boy', var: toddlerBoySizes },
  { name: 'mom', var: momSizes },
  { name: 'dad', var: dadSizes },
  { name: 'dog', var: dogSizes },
  { name: 'doll', var: dollSizes },
  { name: 'big kid', var: bigKidSizes },
  { name: 'toddler', var: toddlerSizes },
  { name: 'adult', var: AdultSizes },
];
export class AlternateSizes extends React.PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    buttonsList: PropTypes.objectOf(PropTypes.any).isRequired,
    navigation: PropTypes.shape({}).isRequired,
    isSBP: PropTypes.bool,
    sbpLabels: PropTypes.shape({}),
    isNewReDesignEnabled: PropTypes.bool,
  };

  static defaultProps = {
    isSBP: false,
    sbpLabels: {},
    isNewReDesignEnabled: false,
  };

  setScrollViewRef = (element) => {
    this.scrollViewRef = element;
  };

  navigateToOtherDepartment = (navigationRoute, url) => {
    const { navigation } = this.props;
    navigation.navigate(navigationRoute, {
      pdpUrl: url,
      reset: true,
      departmentChange: true,
    });
  };

  getAlternativeSizeIcon = (str) => {
    const results = icons.map((value) => {
      if (str.toLowerCase().startsWith(value.name)) {
        return value.var;
      }
      return null;
    });
    const icon = results.filter(Boolean);
    return icon.length > 0 ? icon[0] : AdultSizes;
  };

  renderAlternativeSizeRedesign = (isSBP) => {
    const { title, buttonsList } = this.props;
    const navigationRoute = isSBP ? 'ProductDetailSBP' : 'ProductDetail';
    return (
      <AlternateSizeButtonRedesign>
        <TitleContainer>
          <BodyCopy
            fontFamily="secondary"
            fontWeight="bold"
            fontSize="fs16"
            color="gray.900"
            text={title}
            letterSpacing="ls1"
          />
        </TitleContainer>
        <OtherSizePillsContainer>
          <ScrollView ref={this.setScrollViewRef} horizontal showsHorizontalScrollIndicator={false}>
            {Object.keys(buttonsList).map((item) => {
              return (
                <CustomButton
                  onPress={() => this.navigateToOtherDepartment(navigationRoute, buttonsList[item])}
                  text={item.replace(' Sizes', '')}
                  fontSize="fs14"
                  fontWeight="bold"
                  fontFamily="secondary"
                  paddings="10px"
                  textPadding="0 8px"
                  margin="10px 5px"
                  showShadow
                  imageLeftSize={24}
                  imageLeftName={this.getAlternativeSizeIcon(item)}
                  imageLeftShow
                  imageRightSize={18}
                  imageRightName={rightArrowImage}
                  imageRightShow
                  borderRadius="16px"
                  showBorder
                  wrapContent
                />
              );
            })}
          </ScrollView>
        </OtherSizePillsContainer>
      </AlternateSizeButtonRedesign>
    );
  };

  render() {
    const { title, buttonsList, navigation, isSBP, sbpLabels, isNewReDesignEnabled } = this.props;
    const navigationRoute = isSBP ? 'ProductDetailSBP' : 'ProductDetail';
    const otherSizesAvailableLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_other_sizes_available',
      'OTHER SIZES AVAILABLE:'
    );

    if (isNewReDesignEnabled) {
      return this.renderAlternativeSizeRedesign(isSBP);
    }

    return (
      <AlternateSizeButton isSBP={isSBP}>
        <BodyCopyWithSpacing
          mobilefontFamily={isSBP ? fontFamilyNunitoBlack : ['secondary']}
          fontWeight="semibold"
          fontSize={isSBP ? 'fs14' : 'fs12'}
          color={isSBP ? colorNero : 'black'}
          text={isSBP ? otherSizesAvailableLabel : title}
          spacingStyles="padding-right-XS"
        />
        <AlternateSizeLink isSBP={isSBP}>
          {Object.keys(buttonsList).map((item, index) => {
            return (
              <AnchorWrapper
                noRightBorder={index === Object.keys(buttonsList).length - 1}
                isSBP={isSBP}
              >
                <Anchor
                  accessibilityRole="link"
                  accessibilityLabel={item}
                  text={item}
                  anchorVariation="custom"
                  colorName="gray.900"
                  fontSizeVariation="medium"
                  centered
                  underline
                  onPress={() =>
                    navigation.navigate(navigationRoute, {
                      pdpUrl: buttonsList[item],
                      reset: true,
                    })
                  }
                />
              </AnchorWrapper>
            );
          })}
        </AlternateSizeLink>
      </AlternateSizeButton>
    );
  }
}

export default AlternateSizes;
