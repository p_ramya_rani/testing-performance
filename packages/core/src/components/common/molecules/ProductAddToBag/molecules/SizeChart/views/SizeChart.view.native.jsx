// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Dimensions, View, Image } from 'react-native';
import { isIOS } from '@tcp/core/src/utils';
import PropTypes from 'prop-types';
import { WebView } from 'react-native-webview';
import { Anchor } from '@tcp/core/src/components/common/atoms';
import { WebViewHeaderChannel } from '@tcp/core/src/services/api.constants';
import Modal from '../../../../Modal';
import { SizeChartButton, SizeChartStyle } from '../styles/SizeChart.style.native';
import { getSizeChartUrl, getAnchoringString } from './sizeChartUtils';
import { showSBPLabels } from '../../../../../../../../../mobileapp/src/components/features/shopByProfile/PLP/helper';
import { colorNero } from '../../../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.styles';

const sizeChartImage = require('../../../../../../../../../mobileapp/src/assets/images/shopByProfile/elements/size-chart_2020-09-17/size-chart.png');

const bottomMargin = 80;
export const screenHeight = Math.floor(Dimensions.get('window').height - bottomMargin);
class SizeChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };
  }

  toggleModal = () => {
    const { isOpen } = this.state;
    this.setState({
      isOpen: !isOpen,
    });
  };

  render() {
    const { isOpen } = this.state;
    const {
      labels,
      isSBP,
      sbpLabels,
      isNewReDesignProductSummary,
      pdpLabels,
      categoryPath2Map,
      selectedFit,
    } = this.props;

    const queryParam = getAnchoringString(categoryPath2Map, selectedFit);
    const sizeChartUrl = queryParam ? `${getSizeChartUrl()}?${queryParam}` : getSizeChartUrl();
    const sizeChartLabel = showSBPLabels(sbpLabels, 'lbl_sbp_size_chart', 'SIZE CHART');
    const styleWithHeight = { backgroundColor: 'transparent', height: screenHeight };
    return (
      <>
        {isNewReDesignProductSummary ? (
          <Anchor
            accessibilityRole="link"
            accessibilityLabel={pdpLabels.sizeGuide}
            text={pdpLabels.sizeGuide}
            fontWeight="regular"
            fontFamily="secondary"
            color="gray.900"
            fontSize="fs14"
            textAlign="right"
            underline
            onPress={this.toggleModal}
            noLink
            to="/#"
            dataLocator=""
          />
        ) : (
          <SizeChartButton isSBP={isSBP}>
            {isSBP ? (
              <View style={SizeChartStyle.view1}>
                <Image source={sizeChartImage} style={SizeChartStyle.image} />
                <Anchor
                  nolink
                  href="#"
                  title=""
                  accessibilityRole="link"
                  accessibilityLabel="Size Chart"
                  text={isSBP ? sizeChartLabel : labels.sizeChart}
                  anchorVariation="custom"
                  onPress={this.toggleModal}
                  colorName={isSBP ? colorNero : 'gray.900'}
                  fontSizeVariation="medium"
                  centered
                  underline={!isSBP}
                />
              </View>
            ) : (
              <Anchor
                nolink
                href="#"
                title=""
                accessibilityRole="link"
                accessibilityLabel="Size Chart"
                text={labels.sizeChart}
                anchorVariation="custom"
                onPress={this.toggleModal}
                colorName="gray.900"
                fontSizeVariation="medium"
                centered
                underline
              />
            )}
          </SizeChartButton>
        )}
        <Modal
          isOpen={isOpen}
          overlayClassName="TCPModal__Overlay"
          className="TCPModal__Content"
          fixedWidth
          fullWidth
          onRequestClose={this.toggleModal}
          widthConfig={{ small: '375px', medium: '600px', large: '704px' }}
          heightConfig={{ minHeight: '534px', height: '620px', maxHeight: '650px' }}
          headingAlign="center"
          stickyHeader
          heading="Size Chart"
          stickyCloseIcon
          horizontalBar={false}
        >
          <WebView
            androidHardwareAccelerationDisabled={true}
            originWhitelist={['*']}
            source={{
              uri: sizeChartUrl,
              headers: { 'x-tcp-channel': WebViewHeaderChannel },
            }}
            style={styleWithHeight}
            mixedContentMode="always"
            scrollEnabled
            domStorageEnabled
            thirdPartyCookiesEnabled
            startInLoadingState
            allowUniversalAccessFromFileURLs
            javaScriptEnabled
            automaticallyAdjustContentInsets
            useWebKit={isIOS()}
          />
        </Modal>
      </>
    );
  }
}

SizeChart.propTypes = {
  labels: PropTypes.shape({}),
  isSBP: PropTypes.bool,
  sbpLabels: PropTypes.shape({}),
  isNewReDesignProductSummary: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  categoryPath2Map: PropTypes.shape([]),
  selectedFit: PropTypes.string,
};

SizeChart.defaultProps = {
  labels: {
    sizeChart: 'Size Chart',
  },
  isSBP: false,
  isNewReDesignProductSummary: false,
  sbpLabels: {},
  pdpLabels: {},
  categoryPath2Map: [],
  selectedFit: '',
};

export default SizeChart;
