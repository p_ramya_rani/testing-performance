// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const modalstyles = css`
  .Modal_Heading {
    border-bottom: 0px;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    margin-top: 0;
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: block;
    }
    font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
    font-size: ${(props) => props.theme.typography.fontSizes.fs22};
    text-align: center;
  }
  .Modal-Header {
    padding-top: 14px;
    z-index: ${(props) => props.theme.zindex.zLoader};
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
  div.TCPModal__InnerContent {
    overflow: hidden;
    padding: 0 24px;
  }
`;

const modalstylesRedesign = css`
  .Modal_Heading:before {
    content: '';
    position: absolute;
    height: 46px;
    margin: auto;
    z-index: -1;
    background-color: #f4f4f4;
    bottom: 39px;
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.medium} {
      bottom: 38px;
      left: -17px;
      padding-right: 17px;
    }
  }
  .Modal_Heading {
    border-bottom: none;
    padding-bottom: 26px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    display: flex;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    justify-content: center;
    margin-bottom: 0px;
    align-items: flex-start;
    height: 45px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: flex;
      justify-content: center;
      padding-top: 5px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-top: 0px;
    }
  }
  .Modal-Header {
    padding-top: 7px;
    z-index: ${(props) => props.theme.zindex.zLoader};
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-top: 0px;
    }
  }
  div.TCPModal__InnerContent.size-chart-innerContent {
    right: 0;
    left: auto;
    top: 0;
    bottom: 0;
    transform: none;
    box-shadow: 0 4px 8px 0 rgba(163, 162, 162, 0.5);
    padding: 7px 0 0 17px;
    width: 350px;
    overflow-x: hidden;

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 375px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 481px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
      padding: 7px 0 20px 0px;
    }

    .close-modal {
      background: url('${(props) => getIconPath('left-arrow-icon', props)}');
      background-size: 100% 100%;
      left: 0px;
      top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      @media ${(props) => props.theme.mediaQuery.medium} {
        top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        left: 10px;
      }
    }
  }
`;

export { modalstyles, modalstylesRedesign };
