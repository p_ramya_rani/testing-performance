// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

const style = css`
  padding-top: 10px;
`;

export const AlternateSizeButton = styled.View`
  flex-direction: ${props => (props.isSBP ? `column` : `row`)};
  justify-content: center;
  border-top-width: ${props => (props.isSBP ? 0 : `1px`)};
  border-bottom-width: ${props => (props.isSBP ? 0 : `1px`)};
  padding-top: 13px;
  padding-bottom: 6.5px;
  border-color: ${props => props.theme.colorPalette.gray[500]};
  ${props => (props.isSBP ? `height: 80` : ``)};
`;

export const AlternateSizeLink = styled.View`
  flex-wrap: wrap;
  flex-direction: row;
  flex: 1;
  ${props => (props.isSBP ? `margin-top: 10px` : ``)};
  ${props => (props.isSBP ? `margin-left: -8px` : ``)};
`;

export const AnchorWrapper = styled.View`
  margin-bottom: 6.5px;
  padding: 0 ${props => props.theme.spacing.ELEM_SPACING.XS};
  border-right-width: ${props => (props.noRightBorder || props.isSBP ? 0 : '1px')};
  border-right-color: ${props => props.theme.colorPalette.gray[500]};
`;

export const AlternateSizeButtonRedesign = styled.View`
  margin: 18px 0 0 0;
  shadow-color: ${props => props.theme.colorPalette.black};
  box-shadow: ${props => props.theme.shadow.NEW_DESIGN.BOX_SHADOW};
  elevation: 10;
  border-radius: 20px;
  background: ${props => props.theme.colors.WHITE};
`;

export const TitleContainer = styled.View`
  padding: 18px 0 4px 16px;
`;

export const OtherSizePillsContainer = styled.View`
  margin: 0 0 15px 10px;
`;

export default style;
