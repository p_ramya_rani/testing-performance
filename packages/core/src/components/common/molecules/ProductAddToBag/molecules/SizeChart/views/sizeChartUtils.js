// 9fbef606107a605d69c0edbcd8029e5d
import { getAPIConfig } from '@tcp/core/src/utils';
import { BRAND_TYPE_TCP } from '@tcp/core/src/services/api.constants';
import { appendToken } from '../../../../../../../utils/utils';

export const parseData = (sizeChartData) => {
  const dataObj = {};
  if (sizeChartData && sizeChartData.length) {
    const values = sizeChartData.split('|');
    for (let indx = 0; indx < values.length; indx += 1) {
      dataObj[`data-l${+(indx + 1)}`] = values[indx].toLowerCase();
    }
  }
  return dataObj;
};

export const getQueryString = (dataObj) =>
  Object.keys(dataObj)
    .map((key) => `${key}=${dataObj[key]}`)
    .join('&');

export const getUrl = () => {
  const { webAppDomain, siteId, brandId } = getAPIConfig();
  const baseURL =
    (typeof window !== 'undefined' && window.location && window.location.origin) || webAppDomain;
  const token = brandId === BRAND_TYPE_TCP ? 'token=tcprwd&' : 'token=gymrwd&';
  return `${baseURL}/${siteId}/content/size-chart?${
    appendToken() ? token : ''
  }hideHeaderFooter=true`;
};

export const getSizeChartUrl = () => {
  const { appUpdateDomain, brandId } = getAPIConfig();
  return `${appUpdateDomain}/rwd/${brandId}_size_chart.html`;
};

export const CategoryMapping = {
  Girl: ['Tops', 'Dresses', 'Bottoms', 'Shoes', 'Accessories'],
  Boy: ['Tops', 'Bottoms', 'Shoes', 'Accessories'],
  'Toddler Girl': ['Tops', 'Dresses', 'Bottoms', 'Shoes', 'Accessories'],
  'Toddler Boy': ['Tops', 'Dresses', 'Bottoms', 'Shoes', 'Accessories'],
  Baby: ['Apparel', 'Shoes', 'Accessories'],
  Adults: ['Women', 'Men', 'Unisex'],
  Sleep: ['Cotton', 'Snug Fit'],
  Shoes: ['Baby', 'Toddler Girls', 'Toddler Boys', 'Girls', 'Boys', 'Adults'],
  Accessories: [
    'Belts',
    'Gloves',
    'Mittens',
    'Hats',
    'Socks',
    'Sunglasses',
    'Ties',
    'Bow',
    'Tights',
    'Legwear',
    'Underwear',
  ],
  Dog: ['ALL'],
};

const l1CategoryAvailable = ['Girl', 'Boy', 'Baby', 'Adults', 'Sleep', 'Dog'];

const getCatName = (catName) => (catName ? catName.split('>') : []);

const getProdL1Cat = (categoryPath2Map) => {
  return categoryPath2Map.map((category) => {
    const [, catName] = category.split('|');
    const [catPath1] = getCatName(catName);
    return catPath1;
  });
};

const getCatPathCount = (categoryPath2Map) => {
  return l1CategoryAvailable.reduce((duplicateCount, catPath) => {
    const prodL1Cat = getProdL1Cat(categoryPath2Map);
    return prodL1Cat.includes(catPath) ? duplicateCount + 1 : duplicateCount;
  }, 0);
};

const getCategoryIdentifier = (categoryPath2Map) => {
  let l1Category = '';
  let l2Category = '';
  let defaultL2cat = '';

  // eslint-disable-next-line no-unused-expressions
  categoryPath2Map?.some((catpath) => {
    const [, catName] = catpath.split('|');
    const [catPath1, catPath2 = ''] = getCatName(catName);
    const subCategory = CategoryMapping[catPath1];
    if (subCategory) {
      l1Category = catPath1;
      if (!defaultL2cat) {
        [defaultL2cat] = subCategory;
      }
      return subCategory.some((category) => {
        if (catPath2.includes(category)) {
          l2Category = category;
          return true;
        }
        return false;
      });
    }
    return false;
  });
  return { l1Category, l2Category: l2Category || defaultL2cat };
};

export const getAnchoringParam = (categoryPath2Map) => {
  if (Array.isArray(categoryPath2Map) && categoryPath2Map.length) {
    const catPathCount = getCatPathCount(categoryPath2Map);

    if (catPathCount !== 1) return {};
    return getCategoryIdentifier(categoryPath2Map);
  }
  return {};
};

export const getAnchoringString = (categoryPath2Map, selectedFit) => {
  const { l1Category, l2Category } = getAnchoringParam(categoryPath2Map);
  let catString = '';
  let fitString = '';

  if (l1Category) {
    catString = `l1Category=${l1Category}&l2Category=${l2Category}`;
    if (selectedFit && selectedFit.name) {
      fitString = `&selectedFit=${selectedFit.name}`;
    }
  }
  return catString + fitString;
};
