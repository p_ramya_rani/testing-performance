// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getIconPath } from '@tcp/core/src/utils';
import { Image, Button } from '@tcp/core/src/components/common/atoms';
import Modal from '../../../../Modal';
import Anchor from '../../../../../atoms/Anchor';
import { modalstyles, modalstylesRedesign } from '../styles/SizeChart.style';
import { parseData, getQueryString, getUrl, getAnchoringString } from './sizeChartUtils';

class SizeChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };
  }

  toggleModal = (e) => {
    e.preventDefault();
    const { isOpen } = this.state;
    this.setState({
      isOpen: !isOpen,
    });
  };

  render() {
    const { isOpen } = this.state;
    const {
      sizeChartDetails,
      labels,
      categoryPath2Map,
      selectedFit,
      newQVDesign,
      quickViewLabels,
      isNewPDPEnabled,
      isQuantitySection,
    } = this.props;

    const dataObj = parseData(sizeChartDetails);
    const queryString = getQueryString(dataObj);
    const anchoringParam = getAnchoringString(categoryPath2Map, selectedFit);
    const queryParam = anchoringParam || queryString;
    const url = getUrl();
    const urlWithQueryString = queryParam ? `${url}&${queryParam}` : url;
    const sizeChartRuler = getIconPath('size-chart-ruler');
    const sizeChartArrow = getIconPath('size-chart-right-arrow');
    return (
      <>
        {isNewPDPEnabled && !isQuantitySection && (
          <div className="size-chart-btn-pdp">
            <Anchor noLink {...dataObj} onClick={this.toggleModal}>
              <div className="size-chart-pdp-redesign">
                <Image
                  className="size-chart-ruler"
                  src={sizeChartRuler}
                  alt="size-chart-ruler"
                  width="20px"
                  height="18px"
                />
                <div className="size-chart-label">{quickViewLabels?.sizeGuide}</div>
              </div>
            </Anchor>
          </div>
        )}
        {newQVDesign && !isNewPDPEnabled ? (
          <Anchor noLink {...dataObj} onClick={this.toggleModal}>
            <div className="size-chart-btn">
              <Button
                className="size-chart-quick-view-drawer-redesign"
                buttonVariation="fixed-width"
                fullWidth="true"
              >
                <Image
                  className="size-chart-ruler"
                  src={sizeChartRuler}
                  alt="size-chart-ruler"
                  width="20px"
                  height="18px"
                />
                <div className="size-chart-label">{quickViewLabels?.sizeGuide}</div>
                <Image
                  className="size-chart-arrow"
                  src={sizeChartArrow}
                  alt="size-chart-arrow"
                  width="13px"
                  height="18px"
                />
              </Button>
            </div>
          </Anchor>
        ) : (
          !isNewPDPEnabled && (
            <Anchor noLink className="size-chart" {...dataObj} onClick={this.toggleModal}>
              {labels.sizeChart}
            </Anchor>
          )
        )}
        {isOpen &&
          (newQVDesign || isNewPDPEnabled ? (
            <Modal
              isOpen={isOpen}
              overlayClassName="TCPModal__Overlay"
              className="TCPModal__Content"
              fixedWidth
              fullWidth
              onRequestClose={this.toggleModal}
              headingAlign="center"
              stickyHeader
              heading={labels.sizeChart}
              stickyCloseIcon
              horizontalBar={false}
              inheritedStyles={modalstylesRedesign}
              innerContentClassName="size-chart-innerContent"
            >
              <iframe
                src={urlWithQueryString}
                title="Size Chart"
                width="100%"
                height="86%"
                scrolling="yes"
                frameBorder="0"
              />
            </Modal>
          ) : (
            <Modal
              isOpen={isOpen}
              overlayClassName="TCPModal__Overlay"
              className="TCPModal__Content"
              fixedWidth
              fullWidth
              onRequestClose={this.toggleModal}
              widthConfig={{ small: '375px', medium: '600px', large: '704px' }}
              heightConfig={{ minHeight: '534px', height: '620px', maxHeight: '650px' }}
              headingAlign="center"
              stickyHeader
              heading={labels.sizeChart}
              stickyCloseIcon
              horizontalBar={false}
              inheritedStyles={modalstyles}
            >
              <iframe
                src={urlWithQueryString}
                title="Size Chart"
                width="100%"
                height="86%"
                scrolling="yes"
                frameBorder="0"
              />
            </Modal>
          ))}
      </>
    );
  }
}

SizeChart.propTypes = {
  sizeChartDetails: PropTypes.string,
  labels: PropTypes.shape({}),
  categoryPath2Map: PropTypes.shape([]),
  selectedFit: PropTypes.string,
  newQVDesign: PropTypes.bool,
  quickViewLabels: PropTypes.shape({}),
  isNewPDPEnabled: PropTypes.bool,
  isQuantitySection: PropTypes.bool,
};

SizeChart.defaultProps = {
  sizeChartDetails: '',
  labels: {
    sizeChart: 'Size Chart',
  },
  categoryPath2Map: [],
  selectedFit: '',
  newQVDesign: false,
  quickViewLabels: {},
  isNewPDPEnabled: false,
  isQuantitySection: false,
};

export default SizeChart;
