// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getSizeChartDetails } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.selectors';
import SizeChart from '../views';

class SizeChartContainer extends React.PureComponent {
  render() {
    const {
      sizeChartDetails,
      isSBP,
      sbpLabels,
      categoryPath2Map,
      selectedFit,
      isNewReDesignProductSummary,
      pdpLabels,
      newQVDesign,
      quickViewLabels,
      isNewPDPEnabled,
      isQuantitySection,
    } = this.props;
    return (
      <SizeChart
        sizeChartDetails={sizeChartDetails}
        isSBP={isSBP}
        sbpLabels={sbpLabels}
        pdpLabels={pdpLabels}
        categoryPath2Map={categoryPath2Map}
        selectedFit={selectedFit}
        isNewReDesignProductSummary={isNewReDesignProductSummary}
        newQVDesign={newQVDesign}
        quickViewLabels={quickViewLabels}
        isNewPDPEnabled={isNewPDPEnabled}
        isQuantitySection={isQuantitySection}
      />
    );
  }
}

SizeChartContainer.propTypes = {
  sizeChartDetails: PropTypes.string,
  isSBP: PropTypes.bool,
  sbpLabels: PropTypes.shape({}).isRequired,
  categoryPath2Map: PropTypes.shape([]),
  selectedFit: PropTypes.string,
  isNewReDesignProductSummary: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  newQVDesign: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  isQuantitySection: PropTypes.bool,
};

SizeChartContainer.defaultProps = {
  sizeChartDetails: '',
  isSBP: false,
  isNewReDesignProductSummary: false,
  categoryPath2Map: [],
  selectedFit: '',
  pdpLabels: {},
  newQVDesign: false,
  isNewPDPEnabled: false,
  isQuantitySection: false,
};

function mapStateToProps(state) {
  return {
    sizeChartDetails: getSizeChartDetails(state),
    sbpLabels: state && state.Labels && state.Labels.global && state.Labels.global.ShopByProfile,
  };
}

export default connect(mapStateToProps, null)(SizeChartContainer);
