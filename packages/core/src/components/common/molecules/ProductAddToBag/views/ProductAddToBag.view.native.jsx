/* eslint-disable max-lines */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View, ScrollView, Image, findNodeHandle } from 'react-native';
import { ScrollView as ScrollViewGH } from 'react-native-gesture-handler';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import PropTypes from 'prop-types';
import { ICON_NAME } from '@tcp/core/src/components/common/atoms/Icon/Icon.constants';
import ProductPickupContainer from '@tcp/core/src/components/common/organisms/ProductPickup';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import isEqual from 'lodash/isEqual';
import capitalize from 'lodash/capitalize';

import {
  getMapSliceForColorProductId,
  getSkuId,
  setSelectedPack,
  filterCurrentSelectedProduct,
  getSoftClass,
  getFormValue,
  checkAvailablePill,
  getSoftDisableMutiPack,
  getPrices,
  getPricesWithRange,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import {
  formatProductsData,
  getBrand,
  isTCP,
  onTabClickDetails,
  getSelectedMultipack,
  findMultipacks,
  isGymboree,
} from '@tcp/core/src/utils/utils';
import { HAPTICFEEDBACK_EVENTS } from '@tcp/core/src/components/common/atoms/hapticFeedback/HapticFeedback.constants';
import { getRecommendationsObj } from '../../../../../utils/utils.app';
import ProductVariantSelector from '../../ProductVariantSelector';
import withStyles from '../../../hoc/withStyles';
import styles, {
  RowViewContainer,
  SizeViewContainer,
  SoldoutWrapper,
  AnchorWrapper,
  SimilarLink,
  StyledAnchor,
  MultipackTabWrapperTab,
  MultiPackBtn,
  RowSelectContainer,
  MultiPackBtnRedesign,
  ErrorMsg,
  ProductAddToBagStyle,
  ProductQuantityButton,
  ProductQuantitySizeView,
  ProductQuantityPlusButton,
  ProductQuantityTextButton,
  ProductQuantityMinusButton,
  VerticalLine,
  NewContainerForPadding,
  QuantitySizeViewContainer,
  QuantityAddToBagContainer,
  AddToBagWrapper,
  QuantityButtonWrapper,
  AddToBagButtonWrapper,
  AtbButtonBig,
  EditSectionHeadingWrapper,
  TextWrapper,
  TextWrapperMain,
  TextWrapperNew,
  SizeTextWrapper,
  EditWrapper,
  DIVIDERLINE,
} from '../styles/ProductAddToBag.style.native';
import names from '../../../../../constants/eventsName.constants';
import { Button, BodyCopy } from '../../../atoms';
import CustomButton from '../../../atoms/ButtonRedesign';
import { BTNREDESIGN_VARIATION } from '../../../atoms/ButtonRedesign/ButtonRedesign.constants';
import { BUTTON_VARIATION } from '../../../atoms/Button/Button.constants';
import { NativeDropDown } from '../../../atoms/index.native';
import Select from '../../../atoms/Select';
import SizeChart from '../molecules/SizeChart/container';
import AlternateSizes from '../molecules/AlternateSizes';
import SIZE_CHART_LINK_POSITIONS, { MULTI_PACK_TYPE } from '../container/ProductAddToBag.config';
import Themes from '../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.theme';
import LineComp from '../../../atoms/Line';
import { parseBoolean } from '../../../../../services/abstractors/productListing/productParser';
import { PICKUP_LABELS } from '../../../organisms/PickupStoreModal/PickUpStoreModal.constants';

const iconMinus = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icon-minus.png');
const iconPlus = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icon-plus.png');
const iconMinusNew = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/minus-Icon/minus_icon.png');

class ProductAddToBag extends React.PureComponent {
  /* Have to define empty constructor because test case fail with error 'TypeError: Cannot read property 'find' of undefined'. So if using PureComponent then mendatory to define constructor */
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      showToastMessage: true,
      fitVal: '',
      sizeVal: '',
      resetSizes: false,
      mPackXPositions: {},
      multipackToSelect: null,
      sizeSelectionRefreshed: false,
      isEditEnabled: false,
    };
    this.mPackBtnScrollViewRef = null;
  }

  componentWillMount() {
    const {
      TCPMultipackProductMapping,
      selectedMultipack,
      TCPStyleQTY,
      setSelectedMultipack,
      isPDP,
    } = this.props;

    /* eslint-disable no-extra-boolean-cast */
    const multipackToShow = !!selectedMultipack ? selectedMultipack : Number(TCPStyleQTY);
    if (isPDP && TCPMultipackProductMapping) {
      setSelectedPack(setSelectedMultipack, multipackToShow, TCPMultipackProductMapping);
    }
  }

  componentDidMount() {
    const { sizeList, selectedSize } = this.props;
    this.validateSelectedSize(sizeList, selectedSize);
    this.scrollToItem();
  }

  componentWillUpdate(nextProps) {
    const { TCPStyleType, TCPMultipackProductMapping: prevMultiPackMappings } = this.props;
    if (
      (TCPStyleType === MULTI_PACK_TYPE.SINGLE ||
        (prevMultiPackMappings && prevMultiPackMappings.indexOf('1') > -1)) &&
      nextProps.TCPMultipackProductMapping?.indexOf('1') < 0
    ) {
      nextProps.TCPMultipackProductMapping.unshift('1');
    }
  }

  /**
   *
   * @function getButtonLabel
   * @returns Returns label of button on basis of update/addtobag scenarios
   * @memberof ProductAddToBag
   */
  getButtonLabel = () => {
    const {
      fromBagPage,
      plpLabels,
      pdpLabels,
      keepAlive,
      outOfStockLabels,
      isFavoriteEdit,
      isNewReDesignSizeFit,
    } = this.props;
    const { addToBag, update, saveProduct } = plpLabels;
    let addToBagLabel = addToBag;
    if (fromBagPage) {
      addToBagLabel = update;
    } else if (isFavoriteEdit) {
      addToBagLabel = saveProduct;
    } else if (isNewReDesignSizeFit) {
      addToBagLabel = pdpLabels.addToBagPlpDrawer;
    }
    return keepAlive ? outOfStockLabels.outOfStockCaps : addToBagLabel;
  };

  validateSelectedSize = (sizeList, selectedSize) => {
    const { name: sizeName = '' } = selectedSize || {};
    const sizeSelectionRef =
      sizeList &&
      sizeList.length > 0 &&
      sizeList.filter((sizeItem) => {
        return !sizeItem.disabled && sizeItem.displayName === sizeName;
      }).length > 0;

    this.setState({
      sizeSelectionRefreshed: !sizeSelectionRef,
    });
  };

  componentDidUpdate = (prevProps) => {
    const {
      errorOnHandleSubmit,
      multiErrorOnSubmit,
      isErrorMessageDisplayed,
      plpLabels: { errorMessage },
      toastMessage,
      displayErrorMessage,
      sizeList,
      clearAddToBagError,
      isNewReDesignSizeFit,
      selectedSize,
      clearMultipleAddToBagError,
    } = this.props;
    const { resetSizes } = this.state;
    const changeInFormValidationError =
      isErrorMessageDisplayed && isErrorMessageDisplayed !== prevProps.isErrorMessageDisplayed;
    if (
      (errorOnHandleSubmit && errorOnHandleSubmit !== prevProps.errorOnHandleSubmit) ||
      (multiErrorOnSubmit && multiErrorOnSubmit !== prevProps.multiErrorOnSubmit)
    ) {
      this.setState({
        showToastMessage: true,
      });
    }
    if (errorOnHandleSubmit || changeInFormValidationError) {
      if (changeInFormValidationError) {
        // This is to provide functionality to trigger error from container as well.
        toastMessage(errorMessage); // calling this method directly as state's showToastMessage is never reset and toast message is not displayed second time
        if (isNewReDesignSizeFit) {
          clearAddToBagError(); // Clearing the error message for new redesign for not showing again
          clearMultipleAddToBagError();
        }
        return displayErrorMessage(false); // calling this method to call didUpdate when isErrorMessageDisplayed is set true again
      }
      return this.onToastMessage(errorOnHandleSubmit);
    }

    if (multiErrorOnSubmit) {
      return this.onToastMessage(multiErrorOnSubmit);
    }

    if (
      !isEqual(prevProps?.selectedSize, selectedSize) ||
      !isEqual(prevProps?.sizeList, sizeList)
    ) {
      this.validateSelectedSize(sizeList, selectedSize);
    }
    if (!isEqual(prevProps?.sizeList, sizeList)) {
      this.setState({ resetSizes: true });
    } else if (resetSizes) {
      this.setState({ resetSizes: false });
    }

    this.resetSizesFlatlist();

    return null;
  };

  resetSizesFlatlist = () => {
    const { isNewReDesignProductSummary } = this.props;
    const isFromNewDesign = this.getNewDesignFlagValueAsPerPage();

    if (isNewReDesignProductSummary || isFromNewDesign) this.setMPackScrollViewScrollTo();
  };

  /**
   * @function getEventData
   * @returns getEventData
   *
   * @memberof ProductAddToBag
   */
  getEventData = () => {
    const { isOutfitPage, navigation, cartsSessionStatus } = this.props;
    const source = (navigation && navigation.getParam('source')) || '';

    const eventsData = ['scAdd', 'event61'];
    if (cartsSessionStatus) {
      eventsData.push('scOpen');
    }
    if (isOutfitPage) {
      eventsData.push('event77', 'event85');
    }
    if (source === 'Candid') {
      eventsData.push(names.eVarPropEvent.event154);
    }
    return eventsData;
  };

  addedTobagMsgAction = () => {
    const { addedTobagMsgDispatch } = this.props;
    setTimeout(() => addedTobagMsgDispatch(false), 2000);
  };

  getSelectedSizeValue = (selectedSize, formValues) => {
    return (
      (selectedSize && selectedSize.name) || (formValues && formValues[0] && formValues[0].size)
    );
  };

  handleAddToBag = async ({
    fitChanged,
    displayErrorMessage,
    handleFormSubmit,
    productData,
    setAnalyticsData,
    currentProduct,
    pageShortName,
    pageName,
  }) => {
    if (fitChanged) {
      displayErrorMessage(fitChanged);
    } else {
      handleFormSubmit();
    }
    const productColorId = productData && productData[0] && productData[0].colorId;
    const recObjs = await getRecommendationsObj();
    const recObj = (recObjs && recObjs[productColorId]) || {};
    const productDataRecs = [
      {
        ...productData[0],
        ...recObj,
      },
    ];

    setAnalyticsData({
      customEvents: this.getEventData(),
      CartAddProductId: currentProduct ? currentProduct.generalProductId : '',
      products: productDataRecs,
      linkName: 'cart add',
      pageShortName,
      pageName,
      pageType: 'product',
      pageSection: 'product',
      pageSubSection: 'product',
    });
  };

  isAtbDisabled = () => {
    const { selectedSize, formValues } = this.props;
    const selectedSizeValue = this.getSelectedSizeValue(selectedSize, formValues);
    if (!selectedSizeValue) return true;
    if (typeof selectedSizeValue === 'string') return !selectedSizeValue;
    if (typeof selectedSizeValue === 'object' && 'name' in selectedSizeValue)
      return !selectedSizeValue.name;
    return true;
  };

  getBorderRadius = () => {
    if (isGymboree()) return '23px';
    return '16px';
  };

  isQualifiedRoute = (page) => {
    if (
      page === 'pdp' ||
      page === 'plp' ||
      page === 'productdetail' ||
      page === 'slp' ||
      page === 'sbppdp' ||
      page === 'homepage' ||
      page === 'checkout' ||
      page === 'cart'
    )
      return true;
    return false;
  };

  checkaddtobag = ({
    showAddedToBagCta,
    addBtnMsg,
    addedToBagLabel,
    keepAlive,
    fitChanged,
    displayErrorMessage,
    handleFormSubmit,
    setAnalyticsData,
    currentProduct,
    productData,
    pageShortName,
    pageName,
    revertAtbButton,
    isStyleWith,
    isNewReDesignSizeFit,
    showAddToBagCTA,
    fromPage,
    closeQuickViewSizeModal,
  }) => {
    const { sizeSelectionRefreshed } = this.state;

    const { fireHapticFeedback } = this.props;

    const onHandleAddToBag = () => {
      if (isNewReDesignSizeFit && fromPage && this.isQualifiedRoute(fromPage.toLowerCase()))
        fireHapticFeedback(HAPTICFEEDBACK_EVENTS.SOFT);
      closeQuickViewSizeModal();
      this.handleAddToBag({
        fitChanged,
        displayErrorMessage,
        handleFormSubmit,
        productData,
        setAnalyticsData,
        currentProduct,
        pageShortName,
        pageName,
      });
    };
    if (showAddToBagCTA && isNewReDesignSizeFit && this.getNewDesignFlagValueAsPerPage()) {
      return (
        <AddToBagWrapper>
          <CustomButton
            fontSize="fs16"
            fontWeight="regular"
            fontFamily="secondary"
            paddings="12px"
            textPadding="3px 5px"
            margin="0px 0px"
            borderRadius={this.getBorderRadius}
            showShadow
            fill="BLUE"
            wrapContent={false}
            text={this.getButtonLabel()}
            onPress={onHandleAddToBag}
            disabled={this.isAtbDisabled() || sizeSelectionRefreshed}
          />
        </AddToBagWrapper>
      );
    }
    if (showAddedToBagCta) {
      revertAtbButton();
    }
    if (showAddedToBagCta || addBtnMsg) {
      return (
        <>
          <Button
            margin="16px 0 0 0"
            color="black"
            buttonVariation="success-button"
            text={addedToBagLabel}
            fontSize="fs10"
            fontWeight="extrabold"
            fontFamily="secondary"
            accessibilityLabel="Added to Bag"
            showIcon
            iconName={ICON_NAME.check}
            iconColor="green.500"
            iconSize="fs18"
            iconFirst
          />
          {addBtnMsg && this.addedTobagMsgAction()}
        </>
      );
    }
    return (
      <Button
        margin={`16px 0 0 ${isStyleWith ? '-98%' : '0'}`}
        color="white"
        fill="BLUE"
        disableButton={keepAlive}
        text={this.getButtonLabel()}
        fontSize="fs10"
        fontWeight="extrabold"
        fontFamily="secondary"
        onPress={onHandleAddToBag}
        locator="pdp_color_swatch"
        accessibilityLabel="Add to Bag"
      />
    );
  };

  /**
   * @function renderAddToBagButton
   * @returns Add To Bag Butyon
   *
   * @memberof ProductAddToBag
   */
  renderAddToBagButton = () => {
    const {
      handleFormSubmit,
      fitChanged,
      displayErrorMessage,
      keepAlive,
      currentProduct,
      selectedQuantity,
      selectedSize,
      selectedColor,
      selectedFit,
      itemBrand,
      ledgerSummaryData,
      pageAddType,
      setAnalyticsData,
      showAddedToBagCta,
      revertAtbButton,
      addedToBagLabel,
      addBtnMsg,
      isStyleWith,
      formValues,
      isNewReDesignSizeFit,
      showAddToBagCTA,
      fromPage,
      closeQuickViewSizeModal,
    } = this.props;

    const brandName = itemBrand || getBrand();
    const { subTotal } = ledgerSummaryData;
    const { name: colorName } = selectedColor || {};
    const { name: fitName = '' } = selectedFit || {};
    const selectedSizeValue = this.getSelectedSizeValue(selectedSize, formValues);
    const { name: sizeName = '' } = selectedSize || {};
    const productData = formatProductsData(currentProduct, {
      selectedQuantity,
      selectedSizeValue,
      selectedColor,
      itemBrand: brandName,
      subTotal: subTotal > 0 ? subTotal : currentProduct.listPrice,
      cartAddType: pageAddType,
      sku: getSkuId(currentProduct.colorFitsSizesMap, colorName, fitName, sizeName),
    });
    const { generalProductId, name } = currentProduct;

    const productId = generalProductId && generalProductId.split('_')[0];
    const productName = name && name.toLowerCase();
    const pageShortName = productId ? `product:${productId}:${productName}` : '';
    const pageName = pageShortName;
    return this.checkaddtobag({
      showAddedToBagCta,
      addBtnMsg,
      addedToBagLabel,
      keepAlive,
      fitChanged,
      displayErrorMessage,
      handleFormSubmit,
      setAnalyticsData,
      currentProduct,
      productData,
      pageShortName,
      pageName,
      revertAtbButton,
      isStyleWith,
      isNewReDesignSizeFit,
      showAddToBagCTA,
      fromPage,
      closeQuickViewSizeModal,
    });
  };

  renderAlternateSizes = (alternateSizes) => {
    const { className, navigation, plpLabels, hideAlternateSizes, isSBP, sbpLabels } = this.props;
    const sizeAvailable = plpLabels && plpLabels.sizeAvailable ? plpLabels.sizeAvailable : '';
    const visibleAlternateSizes =
      !hideAlternateSizes && alternateSizes && Object.keys(alternateSizes).length > 0;
    return (
      visibleAlternateSizes && (
        <AlternateSizes
          title={`${sizeAvailable}:`}
          buttonsList={alternateSizes}
          className={className}
          navigation={navigation}
          isSBP={isSBP}
          sbpLabels={sbpLabels}
        />
      )
    );
  };

  shouldRenderAlternateSizes = (alternateSizes, isPickup, isStyleWith) => {
    return !isPickup && !isStyleWith && this.renderAlternateSizes(alternateSizes);
  };

  renderUnavailableLink = () => {
    const {
      currentProduct,
      currentProduct: { colorFitsSizesMap },
      plpLabels,
      selectedColorProductId,
      onCloseClick,
      keepAlive,
      isFromBagProductSfl,
      isPickup,
      isBundleProduct,
    } = this.props;
    let isBopisEligibleFlag;
    const sizeUnavailable = plpLabels && plpLabels.sizeUnavalaible ? plpLabels.sizeUnavalaible : '';
    const currentColorEntry = getMapSliceForColorProductId(
      colorFitsSizesMap,
      selectedColorProductId
    );
    if (currentColorEntry) {
      isBopisEligibleFlag =
        currentColorEntry.miscInfo && currentColorEntry.miscInfo.isBopisEligible;
    }
    return !isFromBagProductSfl && !isPickup && !isBundleProduct && isBopisEligibleFlag ? (
      <ProductPickupContainer
        productInfo={currentProduct}
        formName={`ProductAddToBag-${currentProduct.generalProductId}`}
        sizeUnavailable={sizeUnavailable}
        isAnchor
        miscInfo={currentColorEntry && currentColorEntry.miscInfo}
        onPickupClickAddon={onCloseClick}
        keepAlive={keepAlive}
      />
    ) : null;
  };

  shouldRenderUnavailableLink = (isStyleWith, quickViewPickup) => {
    return quickViewPickup() && !isStyleWith && this.renderUnavailableLink();
  };

  renderSimilarlink = (checkForOOSForVariant) => {
    const {
      plpLabels: { soldOut, seeSimilarItems },
      isRecommendationAvailable,
    } = this.props;
    return (
      checkForOOSForVariant &&
      isRecommendationAvailable &&
      isTCP() && (
        <SimilarLink>
          <SoldoutWrapper>
            <BodyCopy
              accessibilityRole="text"
              fontFamily="secondary"
              fontSize="fs16"
              fontWeight="regular"
              letterSpacing="black"
              textAlign="left"
              text={soldOut}
            />
          </SoldoutWrapper>
          <AnchorWrapper>
            <StyledAnchor
              fontSizeVariation="medium"
              colorName="blue.A100"
              anchorVariation="secondary"
              underlineBlue
              href="#"
              locator="pdp_anchor_recommendation"
              onPress={this.onRecommendation}
              text={seeSimilarItems}
            />
          </AnchorWrapper>
        </SimilarLink>
      )
    );
  };

  onQuantityValueChange = (selectedQuantity) => {
    const { onQuantityChange, form } = this.props;
    if (onQuantityChange) {
      onQuantityChange(selectedQuantity, form);
    }
  };

  onQuantityValueIncrease = () => {
    const { onQuantityChange, form, selectedQuantity, quantityList, fireHapticFeedback } =
      this.props;
    const quantityLength = quantityList.length;
    if (onQuantityChange && selectedQuantity < quantityLength) {
      onQuantityChange(selectedQuantity + 1, form);
      fireHapticFeedback(HAPTICFEEDBACK_EVENTS.SOFT);
    }
  };

  onQuantityValueDecrease = () => {
    const { onQuantityChange, form, selectedQuantity, fireHapticFeedback } = this.props;
    if (onQuantityChange && selectedQuantity !== 1) {
      onQuantityChange(selectedQuantity - 1, form);
      fireHapticFeedback(HAPTICFEEDBACK_EVENTS.SOFT);
    }
  };

  onFitValueChange = (selectedFit) => {
    const { selectFit, form } = this.props;
    this.setState({ fitVal: selectedFit });
    if (selectFit) {
      selectFit(selectedFit, form);
    }
  };

  onSizeValueChange = (selectedSize) => {
    const { selectSize, form } = this.props;
    this.setState({ sizeVal: selectedSize });
    if (selectSize) {
      selectSize(selectedSize, form);
    }
  };

  onToastMessage = (errorMessage) => {
    const { toastMessage, isNewReDesignSizeFit, clearAddToBagError, clearMultipleAddToBagError } =
      this.props;
    const { showToastMessage } = this.state;
    if (showToastMessage) {
      toastMessage(errorMessage);
      if (isNewReDesignSizeFit) {
        clearAddToBagError();
        clearMultipleAddToBagError();
      }
      this.setState({
        showToastMessage: false,
      });
    }
  };

  getQtyMarginStyle = () => {
    const { isBundleProduc } = this.props;
    return !isBundleProduc ? '16px 0 16px 0' : '32px 0 40px 0';
  };

  profileFavoriteColorsSBP = () => {
    const { childProfile, colorList } = this.props;
    const profileFavColors = childProfile.favorite_colors
      ? childProfile.favorite_colors.split(',')
      : [];

    return colorList.filter((colorObj) =>
      profileFavColors.includes(colorObj?.color?.family?.toLowerCase())
    );
  };

  filteredColorListSBP = () => {
    const { childProfile, colorList } = this.props;
    const profileFavColors = childProfile.favorite_colors
      ? childProfile.favorite_colors.split(',')
      : [];

    return colorList.filter(
      (colorObj) => !profileFavColors.includes(colorObj?.color?.family?.toLowerCase())
    );
  };

  onRecommendation = () => {
    const { scrollToTarget } = this.props;
    scrollToTarget('onRecommendation');
  };

  onClickMultipackBtn = (e, index, item, multipackToSelect, singlePack) => {
    const {
      setSelectedMultipack,
      getDetails,
      currentProduct,
      onQuickViewOpenClick,
      fromPDP,
      isPDP,
      singlePageLoad,
      closePickupModal,
      setInitialTCPStyleQty,
      getQickViewSingleLoad,
      getDisableSelectedTab,
      fromBagPage,
      productInfoFromBag,
      availableTCPmapNewStyleId,
      isPickup,
      onPickUpOpenClick,
      onMultiPackChange,
      fromPage,
      alternateBrand,
    } = this.props;
    if (item !== multipackToSelect) {
      onTabClickDetails({
        setSelectedMultipack,
        getDetails,
        currentProduct,
        onQuickViewOpenClick,
        fromPDP,
        isPDP,
        e,
        item,
        singlePack,
        singlePageLoad,
        setInitialTCPStyleQty,
        getQickViewSingleLoad,
        getDisableSelectedTab,
        fromBagPage,
        productInfoFromBag,
        availableTCPmapNewStyleId,
        closePickupModal,
        isPickup,
        onPickUpOpenClick,
        fromPage,
        alternateBrand,
      });
      onMultiPackChange();
    }
  };

  getsoftClass = (softDisableMutiPack) => {
    return softDisableMutiPack ? [...new Set(getSoftClass(softDisableMutiPack))] : [];
  };

  selectedVal = (setInitialTCPStyleQty, TCPStyleQTY) => {
    const { fromBagPage } = this.props;
    return !fromBagPage && setInitialTCPStyleQty ? setInitialTCPStyleQty : TCPStyleQTY;
  };

  checkLengthOfSizeChar = (sizeList) => {
    if (!sizeList || sizeList.length === 0) return 0;
    const longestNameLength = sizeList.reduce((longestLength, size) => {
      return size?.displayName?.length > longestLength ? size?.displayName?.length : longestLength;
    }, 0);
    const longestSize = sizeList.filter((size) => size?.displayName?.length === longestNameLength);
    return longestSize?.[0]?.displayName.length;
  };

  multiPackRedesignButton = (
    index,
    item,
    multipackToSelect,
    singlePack,
    pdpLabels,
    softDisablePill,
    isNewReDesignProductSummary
  ) => {
    const multipackStatus = item === multipackToSelect;
    const isFromNewDesign = this.getNewDesignFlagValueAsPerPage();
    return (
      <CustomButton
        onPress={(e) => this.onClickMultipackBtn(e, index, item, multipackToSelect, singlePack)}
        text={`${item}${item !== singlePack ? pdpLabels.lblPdpMultipackPack : ''}`}
        fontSize="fs12"
        fontWeight="regular"
        fontFamily="secondary"
        paddings="10px"
        textPadding="0px 5px"
        margin={
          !isNewReDesignProductSummary && isFromNewDesign ? '-10px 5px 6px 5px' : '10px 5px 6px 5px'
        }
        showShadow={multipackStatus}
        borderRadius="16px"
        showBorder
        wrapContent={false}
        buttonVariation={BTNREDESIGN_VARIATION.mobileAppIsNewReDesignPlanButtons}
        selected={multipackStatus}
        buttonSoftDisable={softDisablePill === 'soft-disable' && !multipackStatus}
        sizeCharacterLength={7}
        onLayout={(event) => {
          const {
            nativeEvent: { layout },
          } = event;
          const { mPackXPositions } = this.state;

          this.setState({
            mPackXPositions: {
              ...mPackXPositions,
              [item]: layout.x,
            },
          });
        }}
      />
    );
  };

  setMPackScrollViewScrollTo = () => {
    const { mPackXPositions, multipackToSelect } = this.state;
    if (multipackToSelect && mPackXPositions[multipackToSelect] && this.mPackBtnScrollViewRef) {
      this.mPackBtnScrollViewRef.scrollTo({
        x: mPackXPositions[multipackToSelect],
        y: 0,
        animated: true,
      });
    }
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  MultipackTabWrapper = () => {
    const {
      TCPStyleType,
      pdpLabels,
      selectedMultipack,
      TCPStyleQTY,
      singlePageLoad,
      initialMultipackMapping,
      TCPMultipackProductMapping,
      setInitialTCPStyleQty,
      disableMultiPackTab,
      currentProduct: { allProductId },
      initialFormValues,
      initialValues,
      isNewReDesignProductSummary,
    } = this.props;
    const isFromNewDesign = this.getNewDesignFlagValueAsPerPage();
    const getInitialFormVal = getFormValue(initialFormValues, initialValues);
    const getSelectedProductInformation = filterCurrentSelectedProduct(
      allProductId,
      getInitialFormVal
    );
    const softDisableMutiPack = getSoftDisableMutiPack(getSelectedProductInformation);
    const singlePack = pdpLabels && pdpLabels.lblPdpMultipackSingle;
    const getSelectedVal = this.selectedVal(setInitialTCPStyleQty, TCPStyleQTY);
    const multipackToSelect = getSelectedMultipack(
      selectedMultipack,
      TCPStyleQTY,
      MULTI_PACK_TYPE,
      singlePack
    );

    this.setState({ multipackToSelect });

    const getUniqueVal =
      initialMultipackMapping && TCPMultipackProductMapping
        ? [
            ...new Set([
              ...TCPMultipackProductMapping,
              ...initialMultipackMapping,
              ...[TCPStyleQTY],
            ]),
          ]
        : TCPMultipackProductMapping;
    const findCheckMultipack = getUniqueVal?.length
      ? findMultipacks(
          TCPStyleType,
          getUniqueVal,
          singlePack,
          getSelectedVal,
          MULTI_PACK_TYPE,
          singlePageLoad
        )
      : [];

    return (
      <>
        {findCheckMultipack && findCheckMultipack.length > 1 && (
          <MultipackTabWrapperTab {...this.props}>
            {findCheckMultipack.map((item, index) => {
              const selectedMultiPackVal =
                Number(selectedMultipack) === 1 ? item : selectedMultipack;
              const softClass = this.getsoftClass(softDisableMutiPack);
              const getSingleKey = pdpLabels && pdpLabels.lblPdpMultipackSingle;
              const buttonDisabled = disableMultiPackTab && item === selectedMultipack;
              const softDisablePill = checkAvailablePill(
                item,
                softClass,
                multipackToSelect,
                getSingleKey
              );

              return isNewReDesignProductSummary || isFromNewDesign ? (
                this.multiPackRedesignButton(
                  index,
                  item,
                  multipackToSelect,
                  singlePack,
                  pdpLabels,
                  softDisablePill,
                  buttonDisabled,
                  isNewReDesignProductSummary
                )
              ) : (
                <MultiPackBtn
                  disabled={disableMultiPackTab && item === selectedMultipack}
                  key={item}
                  index={item}
                  activeIndex={multipackToSelect}
                  softDisablePill={softDisablePill}
                  ref={(ref) => this.setItemRef(ref, item === multipackToSelect)}
                >
                  <Button
                    onPress={(e) =>
                      this.onClickMultipackBtn(e, index, item, multipackToSelect, singlePack)
                    }
                    text={`${item}${
                      item !== singlePack ? pdpLabels && pdpLabels.lblPdpMultipackPack : ''
                    }`}
                    color={`${item === multipackToSelect ? 'white' : 'black'}`}
                    fontSize="fs12"
                    fontWeight="extrabold"
                    fontFamily="secondary"
                    paddings="10px"
                    buttonVariation={BUTTON_VARIATION.borderless}
                    disabled={disableMultiPackTab && item === selectedMultiPackVal}
                  />
                </MultiPackBtn>
              );
            })}
          </MultipackTabWrapperTab>
        )}
      </>
    );
  };

  setColorFitSizeDisplayNames = () => {
    const { currentProduct, pdpLabels } = this.props;
    const { isGiftCard } = currentProduct;
    const { sizeSelectionLabel, sizeDrawerValue } = pdpLabels || {};
    if (isGiftCard) return sizeDrawerValue;
    return sizeSelectionLabel;
  };

  renderSizes = () => {
    const {
      sizeList,
      selectedSize,
      selectSize,
      plpLabels: { size, fit, color },
      isDisableZeroInventoryEntries,
      sizeChartLinkVisibility,
      keepAlive,
      itemBrand,
      sizeChartDetails,
      isStyleWith,
      isSBPEnabled,
      sbpSizeClick,
      isOutfitPage,
      selectedProducts,
      currentProduct,
      isNewReDesignSizeFit,
      currentProduct: { categoryPath2Map },
      selectedFit,
      fireHapticFeedback,
      primaryBrand,
      fromPdpQuickDraw,
      isSBP,
      pdpLabels,
      isFromBundlePage,
      isFromChangeStore,
      isBundleProduct,
      alternateBrand,
      isShowDisabledSize,
    } = this.props;
    const isNewDesignFlag =
      (isNewReDesignSizeFit && this.getNewDesignFlagValueAsPerPage()) || isFromChangeStore;
    const dropDownStyle = {
      width: 200,
    };
    const { sizeVal, resetSizes } = this.state;
    const { name: sizeName = '' } = selectedSize || {};
    let { colorFitSizeDisplayNames } = this.props;

    colorFitSizeDisplayNames = {
      color,
      fit,
      size,
      ...colorFitSizeDisplayNames,
    };
    if (isNewDesignFlag) {
      colorFitSizeDisplayNames.size = this.setColorFitSizeDisplayNames();
    }
    const sizeTitle = isFromBundlePage
      ? capitalize(colorFitSizeDisplayNames.size)
      : colorFitSizeDisplayNames.size;
    if (isStyleWith && sizeList && sizeList.length) {
      return (
        <RowViewContainer style={dropDownStyle}>
          <RowSelectContainer>
            <Field
              id="stylesize"
              name="Stylesize"
              placeholder={size}
              selectedValue={sizeVal}
              component={Select}
              heading={`${size}: `}
              options={sizeList}
              onValueChange={this.onSizeValueChange}
            />
          </RowSelectContainer>
        </RowViewContainer>
      );
    }
    return (
      <SizeViewContainer isSBP={isSBPEnabled}>
        {!isOutfitPage &&
          !isBundleProduct &&
          !isSBPEnabled &&
          sizeChartLinkVisibility === SIZE_CHART_LINK_POSITIONS.AFTER_SIZE && (
            <SizeChart
              sizeChartDetails={sizeChartDetails}
              selectedFit={selectedFit}
              categoryPath2Map={categoryPath2Map}
            />
          )}
        <Field
          id="size"
          name="Size"
          component={ProductVariantSelector}
          title={sizeTitle}
          itemValue={sizeName}
          renderItem={this.renderSize}
          data={sizeList}
          selectedItem={sizeName}
          selectItem={selectSize}
          itemBrand={itemBrand}
          itemNameKey="displayName"
          locators={{ key: 'pdp_size_label', value: 'pdp_size_value' }}
          isDisableZeroInventoryEntries={isDisableZeroInventoryEntries}
          keepAlive={keepAlive}
          isSBPEnabled={isSBPEnabled}
          sbpSizeClick={sbpSizeClick}
          isOutfitPage={isOutfitPage}
          selectedProducts={selectedProducts}
          currentProduct={currentProduct}
          isNewReDesignEnabled={isNewDesignFlag}
          sizeCharacterLength={this.checkLengthOfSizeChar(sizeList)}
          resetSizes={resetSizes}
          fireHapticFeedback={fireHapticFeedback}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
          fromPdpQuickDraw={fromPdpQuickDraw}
          isSBP={isSBP}
          sizeChartDetails={sizeChartDetails}
          pdpLabels={pdpLabels}
          selectedFit={selectedFit}
          isFromBundlePage={isFromBundlePage}
          isFromChangeStore={isFromChangeStore}
          isShowDisabledSize={isShowDisabledSize}
        />
      </SizeViewContainer>
    );
  };

  renderFits = () => {
    const {
      fitList,
      selectedFit,
      selectFit,
      plpLabels: { size, fit, color },
      pdpLabels,
      keepAlive,
      itemBrand,
      isStyleWith,
      isOutfitPage,
      isNewReDesignSizeFit,
      fireHapticFeedback,
      primaryBrand,
      fromPdpQuickDraw,
      isFromBundlePage,
      isFromChangeStore,
      alternateBrand,
    } = this.props;

    const isNewDesignFlag = isNewReDesignSizeFit && this.getNewDesignFlagValueAsPerPage();
    const dropDownStyle = {
      width: 200,
    };
    const { fitVal } = this.state;
    const { fitSelectionLabel } = pdpLabels || {};
    const { name: fitName = '' } = selectedFit || {};
    let { colorFitSizeDisplayNames } = this.props;
    colorFitSizeDisplayNames = {
      color,
      fit,
      size,
      ...colorFitSizeDisplayNames,
    };

    if (isNewDesignFlag) {
      colorFitSizeDisplayNames.fit = fitSelectionLabel;
    }

    const fitTitle = isFromBundlePage
      ? capitalize(colorFitSizeDisplayNames.fit)
      : colorFitSizeDisplayNames.fit;
    if (isStyleWith && fitList && fitList.length) {
      return (
        <RowViewContainer style={dropDownStyle}>
          <RowSelectContainer>
            <Field
              id="stylefit"
              name="Stylefit"
              placeholder={fit}
              component={Select}
              selectedValue={fitVal}
              heading={`${fit}: `}
              options={fitList}
              onValueChange={this.onFitValueChange}
            />
          </RowSelectContainer>
        </RowViewContainer>
      );
    }
    const selectedFitName = fitName[0]?.toUpperCase() + fitName?.split('').slice(1).join('');
    return (
      <Field
        id="fit"
        name="Fit"
        component={ProductVariantSelector}
        title={fitTitle}
        itemValue={selectedFitName}
        data={fitList}
        selectedItem={fitName}
        selectItem={selectFit}
        itemNameKey="displayName"
        itemBrand={itemBrand}
        locators={{ key: 'pdp_fit_label', value: 'pdp_fit_value' }}
        keepAlive={keepAlive}
        isOutfitPage={isOutfitPage}
        isNewReDesignEnabled={isNewDesignFlag}
        fireHapticFeedback={fireHapticFeedback}
        primaryBrand={primaryBrand}
        isFromFit={true}
        fromPdpQuickDraw={fromPdpQuickDraw}
        isFromChangeStore={isFromChangeStore}
        alternateBrand={alternateBrand}
      />
    );
  };

  renderQuantity = () => {
    const {
      quantityList,
      plpLabels: { quantity },
      selectedQuantity,
      isFromBagProductSfl,
      isStyleWith,
    } = this.props;
    const qunatityText = `${quantity}: `;

    const quantityDropDownStyle = {
      width: 200,
    };

    if (!isFromBagProductSfl && !isStyleWith) {
      return (
        <RowViewContainer style={quantityDropDownStyle} margins={this.getQtyMarginStyle()}>
          <BodyCopy
            fontWeight="black"
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs14"
            text={qunatityText}
          />

          <Field
            component={NativeDropDown}
            data={quantityList}
            id="quantity"
            selectedValue={selectedQuantity}
            onValueChange={this.onQuantityValueChange}
            heading={qunatityText}
            name="Quantity"
            textAlignLeft
            lightGrayColor
            iconRight
          />
        </RowViewContainer>
      );
    }
    return null;
  };

  showProfileIconSBP = () => {
    const { childProfile, onlySizeAndFit } = this.props;
    const { favorite_colors: favoriteColors } = childProfile;
    return favoriteColors && !onlySizeAndFit && this.profileFavoriteColorsSBP().length > 0 ? (
      <View style={ProductAddToBagStyle.view1}>
        <Image
          source={Themes[childProfile.theme || 0].iconNoBackground}
          style={ProductAddToBagStyle.image}
        />
      </View>
    ) : null;
  };

  showProfileFavoriteColorsSBP = (colorFitSizeDisplayNames, colorName) => {
    const {
      isSBP,
      childProfile,
      itemBrand,
      selectedColor,
      selectSize,
      isGiftCard,
      imagesByColor,
      selectColor,
      fireHapticFeedback,
      primaryBrand,
      alternateBrand,
    } = this.props;

    return isSBP && childProfile.favorite_colors && this.profileFavoriteColorsSBP().length > 0 ? (
      <Field
        id="color"
        name="color"
        itemValue={colorName}
        component={ProductVariantSelector}
        title={colorFitSizeDisplayNames.color}
        renderColorItem
        data={this.profileFavoriteColorsSBP()}
        selectedItem={colorName}
        selectedColor={selectedColor}
        selectColor={selectColor}
        componentWidth={30}
        separatorWidth={16}
        locators={{ key: 'pdp_color_label', value: 'pdp_color_value' }}
        isGiftCard={isGiftCard}
        itemBrand={itemBrand}
        imagesByColor={imagesByColor}
        selectSize={selectSize}
        isSBP={isSBP}
        childProfile={childProfile}
        showAllColors={this.profileFavoriteColorsSBP().length === 0}
        profileFavoriteColorSection
        fireHapticFeedback={fireHapticFeedback}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
      />
    ) : null;
  };

  showMultiPackButton = (findCheckMultipack) => {
    const {
      isOutfitPage,
      isPickup,
      isStyleWith,
      isPhysicalMpackEnabled,
      isSBPEnabled,
      disableSwappingMpackPills,
    } = this.props;

    if (
      !isOutfitPage &&
      !isPickup &&
      !isStyleWith &&
      isPhysicalMpackEnabled &&
      !isSBPEnabled &&
      !disableSwappingMpackPills &&
      findCheckMultipack &&
      findCheckMultipack.length > 1
    ) {
      return true;
    }
    return false;
  };

  renderNewReDesignMultiPack = () => {
    const {
      TCPStyleType,
      pdpLabels,
      selectedMultipack,
      TCPStyleQTY,
      singlePageLoad,
      initialMultipackMapping,
      TCPMultipackProductMapping,
      setInitialTCPStyleQty,
    } = this.props;

    const singlePack = pdpLabels && pdpLabels.lblPdpMultipackSingle;
    const getSelectedVal = this.selectedVal(setInitialTCPStyleQty, TCPStyleQTY);
    const multipackToSelect = getSelectedMultipack(
      selectedMultipack,
      TCPStyleQTY,
      MULTI_PACK_TYPE,
      singlePack
    );

    let getUniqueVal;
    if (initialMultipackMapping && TCPMultipackProductMapping)
      getUniqueVal = [...new Set([...TCPMultipackProductMapping, ...initialMultipackMapping])];
    else getUniqueVal = TCPMultipackProductMapping || initialMultipackMapping;

    const findCheckMultipack = getUniqueVal?.length
      ? findMultipacks(
          TCPStyleType,
          getUniqueVal,
          singlePack,
          getSelectedVal,
          MULTI_PACK_TYPE,
          singlePageLoad
        )
      : [];

    if (this.showMultiPackButton(findCheckMultipack)) {
      return (
        <NewContainerForPadding>
          <MultiPackBtnRedesign>
            <BodyCopy
              fontWeight="bold"
              color="gray.900"
              fontFamily="secondary"
              fontSize="fs14"
              text={`${pdpLabels.lblPdpMultipackTitle} ${multipackToSelect}${
                multipackToSelect !== singlePack ? pdpLabels && pdpLabels.lblPdpMultipackPack : ''
              }`}
            />
          </MultiPackBtnRedesign>
          <ScrollViewGH
            ref={(ref) => {
              this.mPackBtnScrollViewRef = ref;
            }}
            horizontal
            showsHorizontalScrollIndicator={false}
          >
            {this.MultipackTabWrapper()}
          </ScrollViewGH>
        </NewContainerForPadding>
      );
    }
    return null;
  };

  renderAddToBagRedesign = (colorFitSizeDisplayNames) => {
    const {
      selectedColor,
      selectSize,
      selectColor,
      showColorChips,
      isGiftCard,
      imagesByColor,
      itemBrand,
      isSBP,
      childProfile,
      onlySizeAndFit,
      isFromBagProductSfl,
      isNewReDesignProductSummary,
      sbpLabels,
      formValues,
      isComingFromFullScreen,
      comingFromPDPSwatch,
      isComingFromPDPSwatch,
      keepAlive,
      fireHapticFeedback,
      primaryBrand,
      isQuantityRow,
      alternateBrand,
    } = this.props;
    const { name: colorName } = selectedColor || {};
    const { color: selectedColorFromPDP } = formValues || {};
    const { color: colorNameFromFullScreenCarousel } = selectedColorFromPDP || {};

    return (
      <View>
        {showColorChips && !onlySizeAndFit && (
          <Field
            id="color"
            name="color"
            itemValue={colorName}
            component={ProductVariantSelector}
            title={colorFitSizeDisplayNames.color}
            renderColorItem
            data={this.filteredColorListSBP()}
            selectedItem={colorName}
            selectedColor={selectedColor}
            selectColor={selectColor}
            componentWidth={30}
            separatorWidth={16}
            locators={{ key: 'pdp_color_label', value: 'pdp_color_value' }}
            isGiftCard={isGiftCard}
            itemBrand={itemBrand}
            imagesByColor={imagesByColor}
            selectSize={selectSize}
            isSBP={isSBP}
            keepAlive={keepAlive}
            isNewReDesignImageSwatches={isNewReDesignProductSummary}
            childProfile={childProfile}
            showAllColors={this.profileFavoriteColorsSBP().length > 0}
            sbpLabels={sbpLabels}
            selectedColorFromPDP={colorNameFromFullScreenCarousel}
            isComingFromFullScreen={isComingFromFullScreen}
            isComingFromPDPSwatch={isComingFromPDPSwatch}
            comingFromPDPSwatch={comingFromPDPSwatch}
            formValues={formValues}
            fireHapticFeedback={fireHapticFeedback}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
          />
        )}
        {this.renderNewReDesignMultiPack()}
        {!isFromBagProductSfl && !isQuantityRow && this.renderQuantityReDesign()}
      </View>
    );
  };

  renderQuantityReDesign = () => {
    const { selectedQuantity, quantityList, isFromBundlePage, isOutfitPage } = this.props;
    const disableMinus = selectedQuantity === 1;
    const disablePlus = quantityList.length === selectedQuantity;
    const isCTLPage = isOutfitPage && !isFromBundlePage;
    return (
      <View>
        <QuantitySizeViewContainer isFromBundlePage={isFromBundlePage} isOutfitPage={isCTLPage}>
          <ProductQuantitySizeView>
            <ProductQuantityButton isFromBundlePage={isFromBundlePage}>
              <ProductQuantityMinusButton>
                <CustomButton
                  onPress={this.onQuantityValueDecrease}
                  imageLeftSize={18}
                  imageLeftName={disableMinus ? iconMinus : iconMinusNew}
                  imageLeftShow
                  paddings="0px 12px"
                  textPadding="0px"
                  hitSlop={ProductAddToBagStyle.hitSlop}
                  disabled={disableMinus}
                />
              </ProductQuantityMinusButton>
              <VerticalLine />
              <ProductQuantityTextButton>
                <BodyCopy
                  fontWeight="black"
                  color="blue.800"
                  fontFamily="secondary"
                  fontSize="fs18"
                  textAlign="center"
                  text={selectedQuantity}
                />
              </ProductQuantityTextButton>
              <VerticalLine />
              <ProductQuantityPlusButton>
                <CustomButton
                  onPress={this.onQuantityValueIncrease}
                  imageLeftSize={18}
                  imageLeftName={iconPlus}
                  imageLeftShow
                  paddings="0px 12px"
                  hitSlop={ProductAddToBagStyle.hitSlop}
                  textPadding="0px"
                  disabled={disablePlus}
                />
              </ProductQuantityPlusButton>
            </ProductQuantityButton>
          </ProductQuantitySizeView>
        </QuantitySizeViewContainer>
      </View>
    );
  };

  getNewDesignFlagValueAsPerPage = () => {
    const { fromPage } = this.props;
    const comingFromPage = (fromPage && fromPage.toLowerCase()) || '';
    return (
      comingFromPage === 'plp' ||
      comingFromPage === 'pdp' ||
      comingFromPage === 'slp' ||
      comingFromPage === 'homepage' ||
      comingFromPage === 'sbppdp' ||
      comingFromPage === 'checkout' ||
      comingFromPage === 'cart'
    );
  };

  renderAddToBagSBP = (colorFitSizeDisplayNames) => {
    const {
      selectedColor,
      plpLabels: { quantity },
      selectSize,
      selectColor,
      showColorChips,
      isGiftCard,
      alternateSizes,
      isPickup,
      imagesByColor,
      itemBrand,
      isSBP,
      childProfile,
      onlySizeAndFit,
      isFromBagProductSfl,
      quantityList,
      selectedQuantity,
      onlyColor,
      sizeChartLinkVisibility,
      sizeChartDetails,
      sbpLabels,
      selectedFit,
      currentProduct: { categoryPath2Map },
      fireHapticFeedback,
      primaryBrand,
      alternateBrand,
    } = this.props;
    const qunatityText = `${quantity}: `;
    const { name: colorName } = selectedColor || {};
    const quantityDropDownStyle = {
      width: 200,
    };
    return (
      <View {...this.props}>
        {this.showProfileIconSBP()}
        {this.showProfileFavoriteColorsSBP(colorFitSizeDisplayNames, colorName)}
        {showColorChips && !onlySizeAndFit && (
          <Field
            id="color"
            name="color"
            itemValue={colorName}
            component={ProductVariantSelector}
            title={colorFitSizeDisplayNames.color}
            renderColorItem
            data={this.filteredColorListSBP()}
            selectedItem={colorName}
            selectedColor={selectedColor}
            selectColor={selectColor}
            componentWidth={30}
            separatorWidth={16}
            locators={{ key: 'pdp_color_label', value: 'pdp_color_value' }}
            isGiftCard={isGiftCard}
            itemBrand={itemBrand}
            imagesByColor={imagesByColor}
            selectSize={selectSize}
            isSBP={isSBP}
            childProfile={childProfile}
            showAllColors={this.profileFavoriteColorsSBP().length > 0}
            sbpLabels={sbpLabels}
            fireHapticFeedback={fireHapticFeedback}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
          />
        )}
        {!isFromBagProductSfl && (
          <View style={ProductAddToBagStyle.view2}>
            <LineComp borderColor="gray.500" />
            <RowViewContainer style={quantityDropDownStyle} margins={this.getQtyMarginStyle()}>
              <BodyCopy
                fontWeight="black"
                color="gray.900"
                fontFamily="secondary"
                fontSize="fs14"
                text={qunatityText}
              />

              <Field
                component={NativeDropDown}
                data={quantityList}
                id="quantity"
                selectedValue={selectedQuantity}
                onValueChange={this.onQuantityValueChange}
                heading={qunatityText}
                name="Quantity"
                textAlignLeft
                lightGrayColor
                iconRight
              />
            </RowViewContainer>
            <LineComp marginTop={12} borderColor="gray.500" />
          </View>
        )}
        {onlyColor ? (
          <View style={ProductAddToBagStyle.view3} />
        ) : (
          <SizeViewContainer>
            {sizeChartLinkVisibility === SIZE_CHART_LINK_POSITIONS.AFTER_SIZE && (
              <SizeChart
                sizeChartDetails={sizeChartDetails}
                isSBP={isSBP}
                selectedFit={selectedFit}
                categoryPath2Map={categoryPath2Map}
              />
            )}
          </SizeViewContainer>
        )}
        {!onlySizeAndFit && <View>{!isPickup && this.renderAlternateSizes(alternateSizes)}</View>}
      </View>
    );
  };

  setScrollViewRef = (element) => {
    this.scrollViewRef = element;
  };

  scrollToItem = () => {
    requestAnimationFrame(() => {
      if (this.itemCardRef && this.scrollViewRef) {
        this.itemCardRef.measureLayout(findNodeHandle(this.scrollViewRef), (x) => {
          this.scrollViewRef.scrollTo({ x, y: 0, animated: true });
        });
      }
    });
  };

  setItemRef = (element, isActive) => {
    if (isActive) this.itemCardRef = element;
  };

  getcolorFitSizeDisplayNames = () => {
    const {
      colorFitSizeDisplayNames,
      plpLabels: { size, fit, color },
    } = this.props;
    if (colorFitSizeDisplayNames) {
      return {
        color,
        fit,
        size,
        ...colorFitSizeDisplayNames,
      };
    }
    return {
      color,
      fit,
      size,
    };
  };

  getProductPrices = (showPriceRange) => {
    const { currentProduct, selectedSize, selectedFit, selectedColor } = this.props;

    return !showPriceRange
      ? getPrices(currentProduct, selectedColor.name, selectedFit, selectedSize)
      : getPricesWithRange(currentProduct, selectedColor.name, selectedFit, selectedSize);
  };

  renderColorSwatches = (colorFitSizeDisplayNames) => {
    const {
      selectedColor,
      selectColor,
      isGiftCard,
      imagesByColor,
      itemBrand,
      fireHapticFeedback,
      formValues,
      newColorList,
      selectSize,
      colorList,
      primaryBrand,
      currentProduct,
      currencySymbol,
      isNewReDesignProductTile,
      retrievedProductInfo,
      selectedMultipack,
      isBundleProduct,
      pdpLabels,
      badge2,
      selectedSize,
      isDynamicBadgeEnabled,
      fromPage,
      isFromOutFit,
      fromPdpQuickDraw,
      isFromChangeStore,
      alternateBrand,
      ...otherProps
    } = this.props;
    const { isShowPriceRangeKillSwitch, showPriceRangeForABTest } = otherProps;
    const { name: sizeName = '' } = selectedSize || {};
    const { name: colorName } = selectedColor || {};
    const currentProductPrices = this.getProductPrices(false);
    const currentGiftCardValue = currentProduct?.offerPrice || retrievedProductInfo?.offerPrice;
    const showPriceRange = parseBoolean(isShowPriceRangeKillSwitch) && showPriceRangeForABTest;
    const offerPrice = isGiftCard
      ? parseInt(currentGiftCardValue, 10)
      : currentProductPrices.offerPrice;

    const listPrice = isGiftCard
      ? parseInt(currentGiftCardValue, 10)
      : currentProductPrices.listPrice;

    const isSelectedSize =
      typeof sizeName === 'object' || (typeof sizeName === 'string' && sizeName === '');
    const isFromNewDesign = this.getNewDesignFlagValueAsPerPage();
    const isFromPLPValue = isFromChangeStore || isFromNewDesign;

    return (
      <Field
        id="color"
        name="color"
        itemValue={colorName}
        component={ProductVariantSelector}
        title={colorFitSizeDisplayNames.color}
        renderColorItem
        data={colorList}
        selectedItem={colorName}
        selectedColor={selectedColor}
        selectColor={selectColor}
        componentWidth={30}
        separatorWidth={16}
        locators={{ key: 'pdp_color_label', value: 'pdp_color_value' }}
        isGiftCard={isGiftCard}
        itemBrand={itemBrand}
        imagesByColor={imagesByColor}
        keepAlive={false}
        showAllColors={this.profileFavoriteColorsSBP().length > 0}
        isComingFromFullScreen={false}
        formValues={formValues}
        fireHapticFeedback={fireHapticFeedback}
        newColorList={newColorList}
        selectSize={selectSize}
        primaryBrand={primaryBrand}
        currencyExchange={1}
        currencySymbol={currencySymbol}
        productInfo={currentProduct}
        showPriceRange={showPriceRange}
        isNewReDesignProductTile={isNewReDesignProductTile}
        offerPrice={offerPrice}
        listPrice={listPrice}
        selectedMultipack={selectedMultipack}
        isBundleProduct={isBundleProduct}
        currentProduct={currentProduct}
        pdpLabels={pdpLabels}
        badge2={badge2}
        selectedSize={!isSelectedSize}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        isFromOutFit={isFromOutFit}
        isFromPLP={isFromPLPValue}
        fromPdpQuickDraw={fromPdpQuickDraw}
        isFromChangeStore={isFromChangeStore}
        alternateBrand={alternateBrand}
      />
    );
  };

  renderOldSwatchesAndMultipackData = (isFromNewDesign, colorFitSizeDisplayNames) => {
    const {
      checkForOOSForVariant,
      isStyleWith,
      isPhysicalMpackEnabled,
      isSBPEnabled,
      disableSwappingMpackPills,
      isOutfitPage,
      hideMultipackPills,
      isNewReDesignSizeFit,
      showColorChips,
    } = this.props;

    const showMultipack =
      !isOutfitPage &&
      !isStyleWith &&
      isPhysicalMpackEnabled &&
      !hideMultipackPills &&
      !isSBPEnabled &&
      disableSwappingMpackPills;

    const showMultipackData =
      !isOutfitPage &&
      !isStyleWith &&
      isPhysicalMpackEnabled &&
      !hideMultipackPills &&
      !isSBPEnabled &&
      !isNewReDesignSizeFit &&
      !disableSwappingMpackPills;

    return (
      <>
        {showMultipack && (
          <ScrollView ref={this.setScrollViewRef} horizontal>
            {this.MultipackTabWrapper()}
          </ScrollView>
        )}
        {!isOutfitPage && !isStyleWith && this.renderSimilarlink(checkForOOSForVariant)}
        {!isOutfitPage &&
          showColorChips &&
          !isFromNewDesign &&
          this.renderColorSwatches(colorFitSizeDisplayNames)}
        {showMultipackData && (
          <ScrollView ref={this.setScrollViewRef} horizontal>
            {this.MultipackTabWrapper()}
          </ScrollView>
        )}
      </>
    );
  };

  renderAddToBagButtonSizeDrawer = () => {
    const {
      atbDisabled,
      isGiftCard,
      getButtonBorderRadius,
      pdpLabels,
      handleSizeClick,
      selectedSizeDrawerTab,
      labels,
      isSelectedSizeDisabled,
      selectedSize,
      formValues,
    } = this.props;
    const selectedSizeValue = this.getSelectedSizeValue(selectedSize, formValues);

    const isSizeDisable =
      selectedSizeDrawerTab === 0 && selectedSizeValue && isSelectedSizeDisabled;
    const buttonLabel =
      selectedSizeDrawerTab === 0
        ? pdpLabels.addToBag
        : labels.lbl_Product_pickup_PICKUP_IN_STORE_DM;
    return (
      <AtbButtonBig
        disabled={atbDisabled || isSizeDisable}
        isPdpNewReDesign
        isGiftCard={isGiftCard}
      >
        <CustomButton
          disabled={atbDisabled || isSizeDisable}
          fontSize="fs16"
          fontWeight="extrabold"
          fontFamily="secondary"
          paddings="10px"
          textPadding="4px 5px"
          margin="0"
          borderRadius={getButtonBorderRadius}
          showShadow
          fill="BLUE"
          wrapContent={false}
          text={buttonLabel}
          onPress={handleSizeClick}
        />
      </AtbButtonBig>
    );
  };

  renderQuantityData = () => {
    const { hideQuatity, isOutfitPage, isFromBundlePage } = this.props;
    const isFromNewDesign = this.getNewDesignFlagValueAsPerPage();
    const quantityCheck = isOutfitPage || hideQuatity;
    if ((isFromNewDesign && !isOutfitPage) || isFromBundlePage) {
      return this.renderQuantityReDesign();
    }
    return !quantityCheck && this.renderQuantity();
  };

  renderQuantityAddToBag = (isFromNewDesign) => {
    const { showAddToBagCTA, fromPage, isOutfitPage } = this.props;
    return isFromNewDesign ? (
      <QuantityAddToBagContainer fromPage={fromPage}>
        <QuantityButtonWrapper>{this.renderQuantityData()}</QuantityButtonWrapper>
        <AddToBagButtonWrapper>
          {!isOutfitPage && showAddToBagCTA && this.renderAddToBagButton()}
          {!isOutfitPage && !showAddToBagCTA && fromPage === 'PDP'
            ? this.renderAddToBagButtonSizeDrawer()
            : null}
        </AddToBagButtonWrapper>
      </QuantityAddToBagContainer>
    ) : (
      <View>{!isOutfitPage && showAddToBagCTA && this.renderAddToBagButton()}</View>
    );
  };

  enableDisableEdit = () => {
    const { isEditEnabled } = this.state;
    this.setState({ isEditEnabled: !isEditEnabled });
  };

  renderEdit = (colorFitSizeDisplayNames) => {
    const { fitList, selectedColor, selectedSize, formValues, selectedFit } = this.props;
    const fitTitle = `${capitalize(colorFitSizeDisplayNames.fit)}: `;
    const colorTitle = `${capitalize(colorFitSizeDisplayNames.color)}: `;
    const sizeTitle = `${capitalize(colorFitSizeDisplayNames.size)}: `;
    const { name: colorName } = selectedColor || {};
    let selectedSizeValue = this.getSelectedSizeValue(selectedSize, formValues);
    if (typeof selectedSizeValue !== 'string') {
      selectedSizeValue = '';
    }
    const { name: fitName = '' } = selectedFit || {};
    const selectedFitName = fitName[0]?.toUpperCase() + fitName?.split('').slice(1).join('');

    return (
      <EditWrapper>
        <TextWrapperMain>
          <BodyCopy
            fontSize="fs8"
            fontWeight="bold"
            fontFamily="secondary"
            text=""
            color="gray.900"
          />
        </TextWrapperMain>
        <TextWrapperMain>
          <TextWrapper>
            <BodyCopy
              fontSize="fs12"
              fontWeight="bold"
              fontFamily="secondary"
              text={colorTitle}
              color="gray.900"
            />
            <BodyCopy
              fontSize="fs12"
              fontWeight="semibold"
              fontFamily="secondary"
              text={capitalize(colorName)}
              color="gray.900"
            />
          </TextWrapper>
          <TextWrapper>
            <BodyCopy
              fontSize="fs12"
              fontWeight="bold"
              fontFamily="secondary"
              text={sizeTitle}
              color="gray.900"
            />
            <BodyCopy
              fontSize="fs12"
              fontWeight="semibold"
              fontFamily="secondary"
              text={capitalize(selectedSizeValue)}
              color="gray.900"
            />
          </TextWrapper>

          <BodyCopy
            onPress={() => this.enableDisableEdit()}
            fontSize="fs14"
            fontWeight="regular"
            fontFamily="secondary"
            textDecoration="underline"
            text={PICKUP_LABELS.EDIT}
          />
        </TextWrapperMain>
        {fitList && fitList.length > 0 ? (
          <TextWrapperNew>
            <BodyCopy
              fontSize="fs12"
              fontWeight="bold"
              fontFamily="secondary"
              text={fitTitle}
              color="gray.900"
            />
            <BodyCopy
              fontSize="fs12"
              fontWeight="semibold"
              fontFamily="secondary"
              text={capitalize(selectedFitName)}
              color="gray.900"
            />
          </TextWrapperNew>
        ) : null}
      </EditWrapper>
    );
  };

  renderCancel = () => {
    return (
      <EditSectionHeadingWrapper>
        <BodyCopy
          textAlign="left"
          fontWeight="bold"
          fontSize="fs16"
          fontFamily="secondary"
          color="gray.900"
          text={PICKUP_LABELS.EDIT_YOUR_SELECTION_TEXT}
        />
        <BodyCopy
          onPress={() => this.enableDisableEdit()}
          fontSize="fs14"
          fontWeight="regular"
          fontFamily="secondary"
          color="gray.900"
          text={PICKUP_LABELS.CANCEL}
          textDecoration="underline"
        />
      </EditSectionHeadingWrapper>
    );
  };

  renderSizeText = () => {
    return (
      <>
        <SizeTextWrapper>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            textAlign="left"
            fontWeight="bold"
            color="gray.900"
            text={PICKUP_LABELS.SELECT_SIZE_TEXT}
          />
        </SizeTextWrapper>
      </>
    );
  };

  renderNewBOPIS = (colorFitSizeDisplayNames) => {
    const { isStyleWith, errorOnHandleSubmit, isOutfitPage, selectedSize } = this.props;
    const { isEditEnabled } = this.state;
    const { name: sizeName = '' } = selectedSize || {};
    const isSizeName = typeof sizeName === 'object' || sizeName === '';
    return (
      <>
        {!isEditEnabled && this.renderEdit(colorFitSizeDisplayNames)}
        {!isEditEnabled && isSizeName && this.renderSizeText()}
        {isEditEnabled && (
          <>
            {this.renderCancel()}
            {this.renderColorSwatches(colorFitSizeDisplayNames)}
            {this.renderNewReDesignMultiPack()}
            {this.renderFits()}
          </>
        )}
        {(isSizeName || isEditEnabled) && this.renderSizes()}
        {!isOutfitPage && <ErrorMsg>{isStyleWith && errorOnHandleSubmit}</ErrorMsg>}
        <DIVIDERLINE />
      </>
    );
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      alternateSizes,
      isPickup,
      quickViewPickup,
      isStyleWith,
      errorOnHandleSubmit,
      isSBP,
      hideQuatity,
      isNewReDesignProductSummary,
      isOutfitPage,
      fromPdpQuickDraw,
      isFromBundlePage,
      isFromChangeStore,
    } = this.props;

    const colorFitSizeDisplayNames = this.getcolorFitSizeDisplayNames();
    const quantityCheck = isOutfitPage || hideQuatity;

    if (isSBP && !isOutfitPage) {
      return this.renderAddToBagSBP(colorFitSizeDisplayNames);
    }

    if (isFromChangeStore) {
      return this.renderNewBOPIS(colorFitSizeDisplayNames);
    }

    if (isNewReDesignProductSummary) {
      return this.renderAddToBagRedesign(colorFitSizeDisplayNames);
    }

    const isFromNewDesign = this.getNewDesignFlagValueAsPerPage();
    return (
      <View {...(!isFromBundlePage && { ...this.props })}>
        {this.renderOldSwatchesAndMultipackData(isFromNewDesign, colorFitSizeDisplayNames)}
        {isFromNewDesign && this.renderColorSwatches(colorFitSizeDisplayNames)}
        {!fromPdpQuickDraw && this.renderNewReDesignMultiPack()}
        {this.renderFits()}
        {this.renderSizes()}
        {quantityCheck
          ? null
          : this.shouldRenderAlternateSizes(alternateSizes, isPickup, isStyleWith)}
        {quantityCheck ? null : this.shouldRenderUnavailableLink(isStyleWith, quickViewPickup)}
        {isFromNewDesign ? null : this.renderQuantityData()}
        {!isOutfitPage && <ErrorMsg>{isStyleWith && errorOnHandleSubmit}</ErrorMsg>}
        {isOutfitPage && !isFromBundlePage ? this.renderQuantityReDesign() : null}
        {this.renderQuantityAddToBag(isFromNewDesign)}
      </View>
    );
  }
}

/* PropTypes declaration */
ProductAddToBag.propTypes = {
  colorList: PropTypes.arrayOf(Object),
  fitList: PropTypes.arrayOf(Object),
  sizeList: PropTypes.arrayOf(Object),
  selectedColor: PropTypes.instanceOf(Object),
  selectedFit: PropTypes.string,
  selectedSize: PropTypes.instanceOf(Object),
  quantityList: PropTypes.arrayOf(Object),
  plpLabels: PropTypes.instanceOf(Object),
  isErrorMessageDisplayed: PropTypes.bool,
  showAddToBagCTA: PropTypes.bool,
  handleFormSubmit: PropTypes.func,
  selectedQuantity: PropTypes.number,
  currentProduct: PropTypes.shape({}).isRequired,
  selectedColorProductId: PropTypes.number.isRequired,
  toastMessage: PropTypes.func,
  fireHapticFeedback: PropTypes.func,
  clearAddToBagError: PropTypes.func,
  clearMultipleAddToBagError: PropTypes.func,
  isBundleProduct: PropTypes.bool,
  isFromBagProductSfl: PropTypes.bool,
  isQuantityRow: PropTypes.bool,
  isFavoriteEdit: PropTypes.bool,
  sizeChartDetails: PropTypes.shape([]).isRequired,
  fromBagPage: PropTypes.bool.isRequired,
  keepAlive: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  isOutfitPage: PropTypes.bool.isRequired,
  selectColor: PropTypes.string.isRequired,
  isGiftCard: PropTypes.bool.isRequired,
  showColorChips: PropTypes.bool.isRequired,
  quickViewColorSwatchesCss: PropTypes.string.isRequired,
  isPDP: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  imagesByColor: PropTypes.shape({}).isRequired,
  relatedSwatchImages: PropTypes.shape({}).isRequired,
  itemBrand: PropTypes.string.isRequired,
  selectFit: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  hideAlternateSizes: PropTypes.shape([]).isRequired,
  onCloseClick: PropTypes.func.isRequired,
  isPickup: PropTypes.bool.isRequired,
  sizeChartLinkVisibility: PropTypes.number.isRequired,
  selectSize: PropTypes.func.isRequired,
  isDisableZeroInventoryEntries: PropTypes.bool.isRequired,
  isSKUValidated: PropTypes.bool.isRequired,
  onQuantityChange: PropTypes.func.isRequired,
  fitChanged: PropTypes.bool.isRequired,
  displayErrorMessage: PropTypes.func.isRequired,
  setAnalyticsData: PropTypes.func.isRequired,
  showAddedToBagCta: PropTypes.bool.isRequired.isRequired,
  revertAtbButton: PropTypes.func.isRequired,
  addedToBagLabel: PropTypes.func.isRequired,
  alternateSizes: PropTypes.shape([]).isRequired,
  isATBErrorMessageDisplayed: PropTypes.bool.isRequired,
  quickViewPickup: PropTypes.func.isRequired,
  storeId: PropTypes.string.isRequired,
  colorFitSizeDisplayNames: PropTypes.shape([]).isRequired,
  ledgerSummaryData: PropTypes.bool.isRequired,
  pageAddType: PropTypes.bool.isRequired,
  form: PropTypes.bool.isRequired,
  isBundleProduc: PropTypes.bool.isRequired,
  errorOnHandleSubmit: PropTypes.func.isRequired,
  multiErrorOnSubmit: PropTypes.string,
  navigation: PropTypes.shape({}).isRequired,
  addedTobagMsgDispatch: PropTypes.func.isRequired,
  addBtnMsg: PropTypes.string.isRequired,
  scrollToTarget: PropTypes.func.isRequired,
  checkForOOSForVariant: PropTypes.bool.isRequired,
  isRecommendationAvailable: PropTypes.bool.isRequired,
  TCPMultipackProductMapping: PropTypes.shape([]).isRequired,
  TCPStyleType: PropTypes.string.isRequired,
  setSelectedMultipack: PropTypes.func,
  pdpLabels: PropTypes.instanceOf(Object).isRequired,
  getDetails: PropTypes.shape({}).isRequired,
  onQuickViewOpenClick: PropTypes.func.isRequired,
  fromPDP: PropTypes.bool.isRequired,
  selectedMultipack: PropTypes.string.isRequired,
  isStyleWith: PropTypes.bool,
  TCPStyleQTY: PropTypes.number.isRequired,
  isPhysicalMpackEnabled: PropTypes.bool,
  singlePageLoad: PropTypes.bool.isRequired,
  closePickupModal: PropTypes.func.isRequired,
  initialMultipackMapping: PropTypes.shape({}).isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  getQickViewSingleLoad: PropTypes.func.isRequired,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  newColorList: PropTypes.bool.isRequired,
  hideQuatity: PropTypes.bool,
  isSBP: PropTypes.bool,
  childProfile: PropTypes.shape({}),
  onlyColor: PropTypes.bool,
  onlySizeAndFit: PropTypes.bool,
  initialFormValues: PropTypes.shape({}).isRequired,
  initialValues: PropTypes.shape({}).isRequired,
  modalClose: PropTypes.func,
  checkVisible: PropTypes.func,
  isSBPEnabled: PropTypes.bool,
  sbpSizeClick: PropTypes.func,
  disableSwappingMpackPills: PropTypes.bool,
  productInfoFromBag: PropTypes.shape({}),
  sbpLabels: PropTypes.shape({}),
  availableTCPmapNewStyleId: PropTypes.shape([]),
  selectedProducts: PropTypes.shape([]),
  formValues: PropTypes.shape({}),
  cartsSessionStatus: PropTypes.number.isRequired,
  isNewReDesignSizeFit: PropTypes.bool.isRequired,
  isNewReDesignProductSummary: PropTypes.bool,
  onPickUpOpenClick: PropTypes.func,
  hideMultipackPills: PropTypes.bool,
  isComingFromFullScreen: PropTypes.bool,
  comingFromPDPSwatch: PropTypes.func,
  isComingFromPDPSwatch: PropTypes.bool,
  currentColorEntry: PropTypes.shape({}),
  onMultiPackChange: PropTypes.func,
  fromPage: PropTypes.string,
  isFromBundlePage: PropTypes.bool,
  closeQuickViewSizeModal: PropTypes.func,
  atbDisabled: PropTypes.bool,
  handleSizeClick: PropTypes.func.isRequired,
  getButtonBorderRadius: PropTypes.number.isRequired,
  isFromChangeStore: PropTypes.bool,
  isShowDisabledSize: PropTypes.bool,
  selectedSizeDrawerTab: PropTypes.number,
  labels: PropTypes.shape({
    lbl_Product_pickup_PICKUP_IN_STORE_DM: 'Pickup In Store',
  }).isRequired,
  isSelectedSizeDisabled: PropTypes.bool,
};

ProductAddToBag.defaultProps = {
  colorList: [],
  fitList: [],
  sizeList: [],
  selectedColor: null,
  selectedFit: '',
  selectedSize: null,
  quantityList: [],
  plpLabels: {},
  isErrorMessageDisplayed: false,
  multiErrorOnSubmit: '',
  handleFormSubmit: null,
  selectedQuantity: 1,
  showAddToBagCTA: true,
  toastMessage: () => {},
  fireHapticFeedback: () => {},
  clearAddToBagError: () => {},
  clearMultipleAddToBagError: () => {},
  isBundleProduct: false,
  isFromBagProductSfl: false,
  isQuantityRow: false,
  isFavoriteEdit: false,
  setSelectedMultipack: () => {},
  isStyleWith: false,
  isPhysicalMpackEnabled: false,
  isSBP: false,
  childProfile: {},
  onlySizeAndFit: false,
  onlyColor: false,
  hideQuatity: false,
  modalClose: () => {},
  checkVisible: () => {},
  isSBPEnabled: false,
  sbpSizeClick: () => {},
  disableSwappingMpackPills: true,
  productInfoFromBag: {},
  sbpLabels: {},
  availableTCPmapNewStyleId: [],
  selectedProducts: [],
  formValues: {},
  isDynamicBadgeEnabled: {},
  isNewReDesignProductSummary: false,
  hideMultipackPills: false,
  onPickUpOpenClick: () => {},
  isComingFromFullScreen: false,
  comingFromPDPSwatch: () => {},
  isComingFromPDPSwatch: false,
  currentColorEntry: {},
  onMultiPackChange: () => {},
  fromPage: '',
  isFromBundlePage: false,
  closeQuickViewSizeModal: () => {},
  atbDisabled: false,
  isFromChangeStore: false,
  selectedSizeDrawerTab: 0,
  isShowDisabledSize: false,
  isSelectedSizeDisabled: false,
};

/* export view with redux form */
export default compose(
  connect((state, props) => {
    const formName = props.customFormName || PRODUCT_ADD_TO_BAG;
    return {
      form: `${formName}-${props.generalProductId}`,
      enableReinitialize: true,
    };
  }),
  reduxForm({
    destroyOnUnmount: false,
  })
)(withStyles(ProductAddToBag, styles));

export { ProductAddToBag as ProductAddToBagVanilla };
