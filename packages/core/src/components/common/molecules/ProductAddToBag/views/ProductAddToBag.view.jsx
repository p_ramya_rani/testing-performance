/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import throttle from 'lodash/throttle';
import isEqual from 'lodash/isEqual';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import SocialProofMessage from '@tcp/core/src/components/features/browse/ProductDetail/molecules/SocialProofMessage/container/SocialProofMessage.container';
import { fromJS } from 'immutable';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import SelectBox from '@tcp/core/src/components/common/atoms/Select';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import MiniBagSelect from '@tcp/web/src/components/features/CnC/MiniBag/molecules/MiniBagSelectBox/MiniBagSelectBox';
import { Row, Button, Image, Col, BodyCopy, Anchor } from '@tcp/core/src/components/common/atoms';
import {
  getIconPath,
  checkInViewPort,
  getViewportInfo,
  isClient,
  isSafariBrowser,
  addRecommendationsObj,
  onTabClickDetails,
  getSelectedMultipack,
  findMultipacks,
  isMobileApp,
  getAPIConfig,
  getBrand,
  getSiteId,
  getLocator,
} from '@tcp/core/src/utils';
import isEmpty from 'lodash/isEmpty';
import ReactTooltip from '@tcp/core/src/components/common/atoms/ReactToolTip';
import { CALL_TO_ACTION_VISIBLE, CONTROLS_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import ProductPickupContainer from '@tcp/core/src/components/common/organisms/ProductPickup';
import { isTier3Device } from '@tcp/web/src/utils/device-tiering';
import {
  getMapSliceForColorProductId,
  getSkuId,
  getFeatures,
  setSelectedPack,
  filterCurrentSelectedProduct,
  getSoftClass,
  getFormValue,
  checkAvailablePill,
  getSoftDisableMutiPack,
  getPrimaryImages,
  getMapSliceForColor,
  getProductListToPath,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import Product from '@tcp/core/src/components/features/browse/ProductDetail/molecules/Product/views/Product.view';
import ProductColorChipsSelector from '../../ProductColorChipSelector';
import ProductPrice from '../../../../features/browse/ProductDetail/molecules/ProductPrice/ProductPrice';
import ProductSizeSelector from '../../ProductSizeSelector';
import AlternateSizes from '../molecules/AlternateSizes';
import styles, { giftCardDesignStyle } from '../styles/ProductAddToBag.style';
import SizeChart from '../molecules/SizeChart/container';
import SIZE_CHART_LINK_POSITIONS, { MULTI_PACK_TYPE } from '../container/ProductAddToBag.config';
import { getGAPageName } from '../../../../../utils/utils.web';
import { getCartItemInfo } from '../../../../features/CnC/AddedToBag/util/utility';

const DELIVERY_METHODS = {
  HOME: 'home',
  BOPIS: 'bopis',
  BOSS: 'boss',
};
const DEFAULT_EVENTS = 'scAdd,scOpen,event85,event61';
// to get Error Message displayed in case any error comes on Add To card
export const ErrorComp = (errorMessage, showAddToBagCTA) => {
  return errorMessage ? (
    <BodyCopy
      className={!showAddToBagCTA ? 'size-error' : 'default-error'}
      fontSize="fs12"
      component="div"
      fontFamily="secondary"
      fontWeight="regular"
      role="alert"
      aria-live="assertive"
    >
      <Image
        alt="Error"
        className="error-image"
        src={getIconPath('circle-alert-fill')}
        data-locator="productcustomizeform-error-icon"
      />
      <BodyCopy
        className="size-error-message"
        fontSize="fs12"
        component="div"
        fontFamily="secondary"
        fontWeight="regular"
      >
        {` ${errorMessage}`}
      </BodyCopy>
    </BodyCopy>
  ) : null;
};

const ProductDetailSectionSkeleton = () => {
  return (
    <div className="product-property-section">
      <LoaderSkelton height="40px" />
    </div>
  );
};

const ProductDetailMoreSizeSkeleton = () => {
  return (
    <div className="product-property-section">
      <LoaderSkelton height="40px" width="60%" />
    </div>
  );
};

class ProductAddToBag extends React.Component {
  constructor(props) {
    super(props);
    this.isMobile = isClient() ? getViewportInfo().isMobile : null;
    this.isTablet = isClient() ? getViewportInfo().isTablet : null;
    this.ATC_BUTTON = 'add-to-bag-button';
    this.quickViewDrawerClass = 'quick-view-drawer-redesign';
    this.disableClass = 'disable-add-to-bag';
    const isPickUpSelected = props.isBopisFilterOn ? DELIVERY_METHODS.BOPIS : DELIVERY_METHODS.HOME;
    this.state = {
      showStickyATC: (isClient() && getViewportInfo().isMobile) || false,
      showStickyPickUp: false,
      IsCallToActionVisible: false,
      quantity: 1,
      isPickUpSelected,
    };
    this.pickupSelectionUpdate = this.pickupSelectionUpdate.bind(this);
    if (props.isPDP && this.isMobile) {
      this.handleScrollThrottle = throttle(this.handleScroll, 100);
    }
    this.mTabRef = React.createRef();
    this.mlistRef = React.createRef();
  }

  componentWillMount() {
    const {
      TCPMultipackProductMapping,
      selectedMultipack,
      TCPStyleQTY,
      setSelectedMultipack,
      isPDP,
      isModalOpen,
    } = this.props;

    /* eslint-disable no-extra-boolean-cast */
    const multipackToShow = !!selectedMultipack ? selectedMultipack : Number(TCPStyleQTY);

    if (isPDP && !isModalOpen) {
      setSelectedPack(setSelectedMultipack, multipackToShow, TCPMultipackProductMapping);
    }
  }

  componentDidMount() {
    const { showAddToBagCTA, setStickyFooterState, isPDP } = this.props;
    const { showStickyATC, showStickyPickUp } = this.state;
    if (showAddToBagCTA) {
      this.setState({ IsCallToActionVisible: true });
    }
    if (isPDP && this.isMobile) {
      setStickyFooterState(showStickyATC || showStickyPickUp);
      window.addEventListener('scroll', this.handleScrollThrottle);
    }
    this.updateMultipackPill();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentWillUpdate(nextProps) {
    const {
      TCPStyleType,
      TCPMultipackProductMapping: prevMultiPackMappings,
      setStickyFooterState,
      isPDP,
      setAddToPickupError,
      addToPickupError,
      isOpenAddedToBag,
    } = this.props;
    const { showStickyATC, showStickyPickUp } = this.state;
    if (addToPickupError && !isOpenAddedToBag) {
      setAddToPickupError('');
    }
    if (
      (TCPStyleType === MULTI_PACK_TYPE.SINGLE ||
        (prevMultiPackMappings && prevMultiPackMappings.indexOf('1') > -1)) &&
      nextProps.TCPMultipackProductMapping?.indexOf('1') < 0
    ) {
      nextProps.TCPMultipackProductMapping.unshift('1');
    }
    if (isPDP && this.isMobile) {
      setStickyFooterState(showStickyATC || showStickyPickUp);
      window.addEventListener('scroll', this.handleScrollThrottle);
    }
  }

  componentWillUnmount() {
    const { setStickyFooterState, isPDP } = this.props;
    if (isPDP && this.isMobile) {
      window.removeEventListener('scroll', this.handleScrollThrottle);
      setStickyFooterState(false);
    }
  }

  handleScroll = () => {
    const { setStickyFooterState } = this.props;
    const { isPickUpSelected, showStickyATC, showStickyPickUp } = this.state;
    const isPickupActive = isPickUpSelected !== DELIVERY_METHODS.HOME;

    const atcButtonWrapper = document.getElementsByClassName('outfit-button-wrapper')[0];
    const bossButtonWrapper = document.getElementsByClassName(
      'btn-find-in-store-wrapper-quick-view-drawer-redesign'
    )[0];
    const anchoringButton = isPickupActive ? bossButtonWrapper : atcButtonWrapper;
    const atcButton =
      anchoringButton &&
      anchoringButton.getElementsByClassName(
        isPickupActive ? 'button-find-in-store-quick-view-drawer-redesign' : this.ATC_BUTTON
      )[0];
    const footer = document.getElementsByClassName('footer-bottom')[0];
    const isVisible = checkInViewPort(atcButton);
    const { isMobile } = getViewportInfo();

    const stickyBtn = isPickupActive
      ? this.renderStickyPickUp(true)
      : this.renderStickyATCButton(true);
    if (footer && isMobile && stickyBtn) {
      footer.classList.add('marginBottom');
    } else if (footer && isMobile && footer.classList.contains('marginBottom')) {
      footer.classList.remove('marginBottom');
    }

    const stickyATBState = isMobile && !isVisible;

    if (isPickupActive) {
      if (showStickyPickUp !== stickyATBState) {
        this.setState({ showStickyPickUp: stickyATBState });
        setStickyFooterState(stickyATBState && stickyBtn);
      }
    } else if (showStickyATC !== stickyATBState) {
      this.setState({ showStickyATC: stickyATBState });
      setStickyFooterState(stickyATBState && stickyBtn);
    }
  };

  getButtonLabel = () => {
    const {
      fromBagPage,
      plpLabels,
      keepAlive,
      outOfStockLabels = {},
      isFavoriteEdit,
      addedToBagLabel = '',
      checkForOOSForVariant,
      isNewPDPEnabled,
    } = this.props;
    const { addToBag, update, saveProduct } = plpLabels;
    let addToBagLabel = addToBag;

    if (fromBagPage) {
      addToBagLabel = update;
    } else if (isFavoriteEdit) {
      addToBagLabel = saveProduct;
    }
    if (isNewPDPEnabled) {
      return keepAlive || checkForOOSForVariant ? outOfStockLabels?.outOfStockCaps : addToBagLabel;
    }
    return keepAlive || checkForOOSForVariant
      ? outOfStockLabels?.outOfStockCaps
      : addToBagLabel || addedToBagLabel;
  };

  renderOutfitButton = () => {
    const {
      currentProduct,
      currentProduct: { colorFitsSizesMap },
      selectedColorProductId,
      isOutfitPage,
      keepAlive,
      isOutfitCarousel,
      quickViewLabels,
      isNewPDPEnabled,
      checkForOOSForVariant,
    } = this.props;
    let isBopisEligibleFlag;
    const currentColorEntry =
      getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId) || {};
    const isAllSizeDisabled = this.checkIfAllSizeIsDisabled();
    if (currentColorEntry) {
      isBopisEligibleFlag =
        currentColorEntry.miscInfo && currentColorEntry.miscInfo.isBopisEligible;
    }
    return isOutfitPage && !isOutfitCarousel && isBopisEligibleFlag ? (
      <div className="outfit-pickup">
        <ProductPickupContainer
          productInfo={currentProduct}
          formName={`ProductAddToBag-${currentProduct.generalProductId}`}
          miscInfo={currentColorEntry && currentColorEntry.miscInfo}
          isOutfitVariant
          keepAlive={keepAlive}
          quickViewLabels={quickViewLabels}
          isNewPDPEnabled={isNewPDPEnabled}
          checkForOOSForVariant={checkForOOSForVariant}
          isAllSizeDisabled={isAllSizeDisabled}
        />
      </div>
    ) : (
      <div className="outfit-pickup" />
    );
  };

  skeletonCheck = (isLoading, isPDP, setInitialQuickViewLoad) => {
    return isLoading && !isPDP && setInitialQuickViewLoad;
  };

  colorListNullCheck = (showColorChips, colorList) => {
    const { isNewPDPEnabled } = this.props;
    return isNewPDPEnabled
      ? showColorChips && colorList && colorList.size > 1
      : showColorChips && colorList && colorList.size > 0;
  };

  getPageName = (productInfo) => {
    let pageName = '';
    const productId = productInfo && productInfo?.generalProductId?.split('_')[0];
    const productName = productInfo && productInfo?.name?.toLowerCase();
    if (productId) {
      pageName = `product:${productId}:${productName}`;
    }
    return {
      pageName,
    };
  };

  getIsStoreBopisEligible = (bopisItemInventory) => {
    return (
      bopisItemInventory &&
      bopisItemInventory.inventoryResponse &&
      bopisItemInventory.inventoryResponse.length > 0 &&
      bopisItemInventory.inventoryResponse[0].quantity > 0
    );
  };

  checkForSizeMismatchBetweenFits = (colorFitsSizesMap, formValues) => {
    const filteredColorElement =
      colorFitsSizesMap &&
      colorFitsSizesMap.find((colorElement) => {
        return (
          (colorElement && colorElement.color && colorElement.color.name) ===
          (formValues && formValues.color)
        );
      });
    const filteredSizeArray =
      filteredColorElement &&
      filteredColorElement.fits &&
      filteredColorElement.fits.find((fitsItem) => {
        return (fitsItem && fitsItem.fitName) === (formValues && formValues.fit);
      });
    const selectedSizeAvail =
      filteredSizeArray &&
      filteredSizeArray.sizes &&
      filteredSizeArray.sizes.some((sizeDetails) => {
        return (sizeDetails && sizeDetails.sizeName) === (formValues && formValues.size);
      });
    return !selectedSizeAvail;
  };

  handlePickupFormSubmit = (e) => {
    const { formValues, displayErrorMessage, currentProduct } = this.props;
    const updatedForm = Array.isArray(formValues) ? formValues[0] : formValues;
    const { isPickUpSelected } = this.state;
    const { colorFitsSizesMap } = currentProduct;
    const selectedSizeValue = updatedForm?.size;
    const stickyBtn = this.renderStickyPickUp();
    const isSizeDisabled = this.checkIfSizeIsDisabled();
    const isBoss = isPickUpSelected === DELIVERY_METHODS.BOSS;
    let isSizeMisMatchBetweenFits = false;
    if (
      colorFitsSizesMap &&
      updatedForm &&
      updatedForm.color &&
      selectedSizeValue &&
      updatedForm.fit !== null
    ) {
      isSizeMisMatchBetweenFits = this.checkForSizeMismatchBetweenFits(
        colorFitsSizesMap,
        updatedForm
      );
    }
    e.preventDefault();
    if (!selectedSizeValue || (isBoss && isSizeDisabled) || isSizeMisMatchBetweenFits) {
      displayErrorMessage(true);
      if (stickyBtn) this.scrollIntoView();
    } else {
      displayErrorMessage(false);
      this.handlePickupModalClick(selectedSizeValue, isBoss);
    }
  };

  /**
   * @method handlePickupModalClick -
   * method is responsible for invoking the method for open pickup modal
   */

  handlePickupModalClick = (selectedSizeValue, isBoss) => {
    const {
      onPickUpOpenClick,
      currentProduct,
      onPickupClickAddon,
      itemBrand,
      availableTCPmapNewStyleId,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      selectedColorProductId,
      alternateBrandFromUrl,
      alternateBrand,
      userDefaultStore,
      bopisItemInventory,
    } = this.props;
    const { isPickUpSelected } = this.state;
    const isStoreBopisEligible = this.getIsStoreBopisEligible(bopisItemInventory);
    const isBopis = isPickUpSelected === DELIVERY_METHODS.BOPIS;
    const isPickUpEligible =
      (isBoss && !this.checkIfAllSizeIsDisabled()) || (isBopis && isStoreBopisEligible);
    if (
      userDefaultStore &&
      userDefaultStore.basicInfo.id &&
      selectedSizeValue &&
      isPickUpEligible
    ) {
      this.handleAddItemToCartInPickup(isBopis, isBoss);
    } else {
      let isBopisEligibleFlag;
      let isBossEligibleFlag;

      const { colorFitsSizesMap } = currentProduct;
      const currentColorEntry =
        getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId) || {};
      if (currentColorEntry) {
        isBopisEligibleFlag =
          currentColorEntry.miscInfo && currentColorEntry.miscInfo.isBopisEligible;
        isBossEligibleFlag =
          currentColorEntry.miscInfo && currentColorEntry.miscInfo.isBossEligible;
      }
      onPickUpOpenClick({
        generalProductId: currentProduct.generalProductId,
        isBopisCtaEnabled: isBopisEligibleFlag,
        isBossCtaEnabled: isBossEligibleFlag,
        currentProduct: { ...currentProduct, itemBrand },
        colorProductId: currentProduct.generalProductId,
        isProductPickup: true,
        availableTCPmapNewStyleId,
        availableTCPMultipackProductMapping: initialMultipackMapping,
        initialSelectedQty: setInitialTCPStyleQty,
        setInitialTCPStyleQty,
        alternateBrand: alternateBrandFromUrl || alternateBrand,
        isBopisPickup: isBopis,
      });
      if (onPickupClickAddon) {
        onPickupClickAddon();
      }
    }
  };

  showFindInStore = () => {
    const { userDefaultStore, showPickupInfo } = this.props;
    return showPickupInfo && userDefaultStore;
  };

  getImageSwatches = (imagesByColor, updatedColorEntry, currentColorEntry) => {
    return imagesByColor
      ? getPrimaryImages({
          imagesByColor,
          curentColorEntry: !updatedColorEntry ? currentColorEntry : updatedColorEntry,
          isAbTestActive: false,
          isFullSet: true,
        })
      : null;
  };

  selectColourOnHover = (e, isNewQVEnabled) => {
    const { selectColorToHover, isPDP } = this.props;
    const notMobileTablet = !this.isMobile && !this.isTablet;
    return notMobileTablet && (!isNewQVEnabled || isPDP) ? selectColorToHover(e) : undefined;
  };

  renderColorList = (colorList, colorTitle, imageSwatchesToDisplay, newColorList) => {
    const {
      selectColor,
      isGiftCard,
      showColorChips,
      quickViewColorSwatchesCss,
      isPDP,
      isLoading,
      imagesByColor,
      relatedSwatchImages,
      itemBrand,
      isStyleWith,
      isImageSwatchDisabled,
      isBundleProduct,
      selectColorToHoverOut,
      setInitialQuickViewLoad,
      isABTestLoaded,
      currentProduct,
      formValues,
      isNewQVEnabled,
      isPickup,
      isNewPDPEnabled,
      primaryBrand,
      alternateBrand,
      fromBagPage,
    } = this.props;
    const { generalProductId, colorFitsSizesMap } = currentProduct;
    const getFormName = Array.isArray(formValues) ? formValues[0]?.color : formValues?.color;
    let imageSwatchesToDisplayOnQV = [];
    const currentColorEntry =
      getMapSliceForColorProductId(colorFitsSizesMap, generalProductId) || {};
    const updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, getFormName);
    imageSwatchesToDisplayOnQV = this.getImageSwatches(
      imagesByColor,
      updatedColorEntry,
      currentColorEntry
    );
    return this.skeletonCheck(isLoading, isPDP, setInitialQuickViewLoad) ? (
      <ProductDetailSectionSkeleton primaryBrand={primaryBrand} />
    ) : (
      showColorChips && this.colorListNullCheck(showColorChips, colorList) && (
        <div className="color-selector">
          <Field
            width={87}
            name="color"
            component={ProductColorChipsSelector}
            isGiftCard={isGiftCard}
            colorFitsSizesMap={colorList}
            onChange={selectColor}
            dataLocator="addnewaddress-state"
            title={!isStyleWith ? `${colorTitle}:` : ''}
            inheritedStyles={isGiftCard && isPDP ? giftCardDesignStyle : quickViewColorSwatchesCss}
            imagesByColor={imagesByColor}
            itemBrand={itemBrand}
            relatedSwatchImages={relatedSwatchImages}
            isStyleWith={isStyleWith}
            imageSwatchesToDisplay={
              isNewQVEnabled ? imageSwatchesToDisplayOnQV : imageSwatchesToDisplay
            }
            isImageSwatchDisabled={!isNewQVEnabled || isPickup ? isImageSwatchDisabled : false}
            isPDP={isPDP}
            isBundleProduct={isBundleProduct}
            isABTestLoaded={isABTestLoaded}
            onMouseOver={(e) => this.selectColourOnHover(e, isNewQVEnabled)}
            onFocus={(e) => this.selectColourOnHover(e, isNewQVEnabled)}
            onMouseLeave={selectColorToHoverOut}
            newColorList={newColorList}
            isNewQVEnabled={isNewQVEnabled}
            isPickup={isPickup}
            isNewPDPEnabled={isNewPDPEnabled}
            newQVDesign={isNewQVEnabled}
            primaryBrand={primaryBrand}
            fromBagPage={fromBagPage}
            alternateBrand={alternateBrand}
          />
        </div>
      )
    );
  };

  renderFitList = (fitList, fitTitle) => {
    const {
      selectFit,
      keepAlive,
      selectedFit,
      isStyleWith,
      isPDP,
      checkForOOSForVariant,
      isNewQVEnabled,
    } = this.props;
    const newQVDesign = isNewQVEnabled;
    return this.productDetailSkeletonCheck() ? (
      <ProductDetailSectionSkeleton />
    ) : (
      fitList && fitList.size > 0 && (
        <div className={`fit-selector ${isStyleWith ? 'fit-selector-atb' : ''}`}>
          {!isStyleWith && (
            <Field
              width={69}
              id="fit"
              name="Fit"
              component={ProductSizeSelector}
              sizesMap={fitList}
              onChange={selectFit}
              dataLocator="addnewaddress-state"
              title={`${fitTitle}${newQVDesign ? '' : ':'}`}
              keepAlive={keepAlive}
              selectedFit={selectedFit}
              isPDP={isPDP}
              checkForOOSForVariant={checkForOOSForVariant}
              newQVDesign={newQVDesign}
              isFit
            />
          )}
          {isStyleWith && (
            <Field
              onChange={selectFit}
              placeholder="Select Fit"
              id="fit"
              name="Fit"
              component={SelectBox}
              dataLocator="fit-selector"
              options={fitList}
              className="field borderSelectBox"
              enableSuccessCheck={false}
              selectedFit={selectedFit}
              preText={`${fitTitle}${newQVDesign ? '' : ':'} `}
              forceSelect
              isPDP={isPDP}
              checkForOOSForVariant={checkForOOSForVariant}
            />
          )}
        </div>
      )
    );
  };

  renderAlternateSizes = (alternateSizes) => {
    const {
      className,
      hideAlternateSizes,
      isLoading,
      isPDP,
      otherAvailableSizeLabel,
      setInitialQuickViewLoad,
    } = this.props;
    const sizeAvailable = otherAvailableSizeLabel || '';
    const visibleAlternateSizes =
      !hideAlternateSizes && alternateSizes && Object.keys(alternateSizes).length > 0;
    return isLoading && !isPDP && setInitialQuickViewLoad ? (
      <ProductDetailMoreSizeSkeleton />
    ) : (
      visibleAlternateSizes && (
        <AlternateSizes
          title={`${sizeAvailable}:`}
          buttonsList={alternateSizes}
          className={className}
        />
      )
    );
  };

  renderUnavailableLink = () => {
    const {
      currentProduct,
      currentProduct: { colorFitsSizesMap },
      plpLabels,
      onCloseClick,
      selectedColorProductId,
      keepAlive,
      isFromBagProductSfl,
      isPickup,
      quickViewLabels,
      isNewPDPEnabled,
      checkForOOSForVariant,
    } = this.props;
    const sizeUnavailable = plpLabels && plpLabels.sizeUnavalaible ? plpLabels.sizeUnavalaible : '';
    const currentColorEntry = getMapSliceForColorProductId(
      colorFitsSizesMap,
      selectedColorProductId
    );
    const isAllSizeDisabled = this.checkIfAllSizeIsDisabled();
    return isFromBagProductSfl && isPickup ? (
      <ProductPickupContainer
        productInfo={currentProduct}
        formName={`ProductAddToBag-${currentProduct.generalProductId}`}
        isAnchor
        sizeUnavailable={sizeUnavailable}
        onPickupClickAddon={onCloseClick}
        miscInfo={currentColorEntry && currentColorEntry.miscInfo}
        keepAlive={keepAlive}
        quickViewLabels={quickViewLabels}
        isNewPDPEnabled={isNewPDPEnabled}
        checkForOOSForVariant={checkForOOSForVariant}
        isAllSizeDisabled={isAllSizeDisabled}
      />
    ) : null;
  };

  getSizeList = (sizeList, colorFitSizeDisplayNames, errorMessage, currentColorEntry) => {
    const {
      isErrorMessageDisplayed,
      selectSize,
      isDisableZeroInventoryEntries,
      keepAlive,
      isSKUValidated,
      isStyleWith,
      isLoading,
      isPDP,
      checkForOOSForVariant,
      isPickup,
      isNewQVEnabled,
      isNewPDPEnabled,
      addToPickupError,
    } = this.props;
    const { isPickUpSelected } = this.state;
    const newQVDesign = isNewQVEnabled;
    const isBopisEligible = this.getBopisAvailability(currentColorEntry);
    if (isStyleWith) {
      return (
        <Field
          onChange={selectSize}
          placeholder="Select Size"
          name="Size"
          id="size"
          component={SelectBox}
          dataLocator="size-selector"
          options={sizeList}
          className="field borderSelectBox"
          enableSuccessCheck={false}
          preText={`${colorFitSizeDisplayNames.size}${isNewQVEnabled ? '' : ':'} `}
          forceSelect
        />
      );
    }
    return (
      <Field
        width={49}
        className={isErrorMessageDisplayed || isSKUValidated ? 'size-field-error' : 'size-field'}
        id="size"
        name="Size"
        component={ProductSizeSelector}
        sizesMap={sizeList}
        onChange={selectSize}
        dataLocator="addnewaddress-state"
        title={`${colorFitSizeDisplayNames.size}${isNewQVEnabled ? '' : ':'}`}
        isDisableZeroInventoryEntries={isDisableZeroInventoryEntries}
        keepAlive={keepAlive}
        isLoading={isLoading}
        isPDP={isPDP}
        checkForOOSForVariant={!isPickup ? checkForOOSForVariant : false}
        newQVDesign={newQVDesign}
        isErrorMessageDisplayed={isErrorMessageDisplayed}
        errorMessage={errorMessage || addToPickupError}
        isNewPDPEnabled={isNewPDPEnabled}
        isSKUValidated={isSKUValidated}
        isPickUpSelected={isBopisEligible && isPickUpSelected === DELIVERY_METHODS.BOPIS}
      />
    );
  };

  checkIfSizesAvailable = (sizeList) => sizeList && sizeList.size > 0;

  checkforProductDetailSkelton = (isLoading, isPDP, setInitialQuickViewLoad) => {
    return isLoading && !isPDP && setInitialQuickViewLoad;
  };

  newQVDesignCheck = (isNewQVEnabled, isPDP) => {
    return isNewQVEnabled && !isPDP;
  };

  atbPickupOrErrorCheck = (error1, error2) => {
    return error1 || error2;
  };

  renderSizeList = (
    sizeList,
    colorFitSizeDisplayNames,
    errorMessage,
    addToBagError,
    currentColorEntry
  ) => {
    const {
      sizeChartLinkVisibility,
      isErrorMessageDisplayed,
      sizeChartDetails,
      isLoading,
      plpLabels,
      isSKUValidated,
      isPDP,
      isStyleWith,
      setInitialQuickViewLoad,
      selectedFit,
      currentProduct: { categoryPath2Map },
      isNewQVEnabled,
      quickViewLabels,
      isNewPDPEnabled,
      isATBErrorMessageDisplayed,
      showAddToBagCTA,
      addToPickupError,
    } = this.props;
    const newQVDesign = this.newQVDesignCheck(isNewQVEnabled, isPDP);
    return this.checkforProductDetailSkelton(isLoading, isPDP, setInitialQuickViewLoad) ? (
      <ProductDetailSectionSkeleton />
    ) : (
      this.checkIfSizesAvailable(sizeList) && (
        <div className={`size-selector ${isStyleWith ? 'size-selector-atb' : ''}`}>
          {!newQVDesign &&
            !isNewPDPEnabled &&
            sizeChartLinkVisibility === SIZE_CHART_LINK_POSITIONS.AFTER_SIZE && (
              <SizeChart
                sizeChartDetails={sizeChartDetails}
                labels={{ sizeChart: plpLabels.sizeChart }}
                selectedFit={selectedFit}
                categoryPath2Map={categoryPath2Map}
                quickViewLabels={quickViewLabels}
                isNewPDPEnabled={isNewPDPEnabled}
              />
            )}

          {isPDP &&
            isNewPDPEnabled &&
            sizeChartLinkVisibility === SIZE_CHART_LINK_POSITIONS.AFTER_SIZE && (
              <SizeChart
                sizeChartDetails={sizeChartDetails}
                labels={{ sizeChart: plpLabels.sizeChart }}
                selectedFit={selectedFit}
                categoryPath2Map={categoryPath2Map}
                newQVDesign={newQVDesign}
                quickViewLabels={quickViewLabels}
                isNewPDPEnabled={isNewPDPEnabled}
              />
            )}

          {this.getSizeList(
            sizeList,
            colorFitSizeDisplayNames,
            this.atbPickupOrErrorCheck(errorMessage, addToPickupError),
            currentColorEntry
          )}
          {!isNewQVEnabled || isNewPDPEnabled ? (
            <div className="please-enter-an-error">
              {!addToPickupError &&
                this.atbPickupOrErrorCheck(isErrorMessageDisplayed, isSKUValidated) &&
                ErrorComp(this.atbPickupOrErrorCheck(errorMessage, addToPickupError))}
            </div>
          ) : null}
          {isNewPDPEnabled ? (
            <div className="please-enter-an-error">
              {(isATBErrorMessageDisplayed || addToPickupError) &&
                ErrorComp(
                  this.atbPickupOrErrorCheck(addToBagError, addToPickupError),
                  showAddToBagCTA
                )}
            </div>
          ) : null}
        </div>
      )
    );
  };

  quantityChange = (e, v) => {
    const { onQuantityChange, displayErrorMessage } = this.props;
    displayErrorMessage(false);
    onQuantityChange(v);
  };

  renderQuantitySelector = () => {
    const {
      isFromBagProductSfl,
      quantityList,
      isLoading,
      isPDP,
      keepAlive,
      setInitialQuickViewLoad,
      sizeChartLinkVisibility,
      sizeChartDetails,
      selectedFit,
      currentProduct: { categoryPath2Map },
      isNewQVEnabled,
      quickViewLabels,
      socialProofMessage,
      isNewPDPEnabled,
      isPickup,
      fromBagPage,
    } = this.props;
    const newQVDesign = isNewQVEnabled && !isPickup;
    const socialProofMessageCheck =
      !isNewPDPEnabled && isPDP && !this.isMobile && socialProofMessage?.variation === 'bottom';
    return isLoading && !isPDP && setInitialQuickViewLoad ? (
      <ProductDetailSectionSkeleton />
    ) : (
      !isFromBagProductSfl && (
        <div className={`${socialProofMessageCheck ? 'social-proof-wrapper-qty' : ''}`}>
          <div
            className={`${newQVDesign ? 'qty-selector-qv' : 'qty-selector'} ${
              socialProofMessageCheck ? 'socialProof' : ''
            }`}
          >
            <Field
              width={32}
              id={`${newQVDesign ? 'quantity-new-qv' : 'quantity'}`}
              name="Quantity"
              component={MiniBagSelect}
              options={quantityList}
              onChange={this.quantityChange}
              dataLocator="addnewaddress-state"
              keepAlive={keepAlive}
              newQVDesign={newQVDesign}
              sizeChartLinkVisibility={sizeChartLinkVisibility}
              sizeChartDetails={sizeChartDetails}
              selectedFit={selectedFit}
              categoryPath2Map={categoryPath2Map}
              quickViewLabels={quickViewLabels}
              isNewPDPEnabled={isNewPDPEnabled}
              fromBagPage={fromBagPage}
              isQuantitySection
            />
          </div>
          {socialProofMessageCheck && (
            <SocialProofMessage socialProofMessage={socialProofMessage} />
          )}
        </div>
      )
    );
  };

  isSelectedProductCTA = () => {
    const { currentProduct, showAddedToBagCta, generalProductId } = this.props;
    const { productId, unbxdProdId } = currentProduct || {};
    return (
      (showAddedToBagCta && showAddedToBagCta === generalProductId) ||
      showAddedToBagCta === productId ||
      showAddedToBagCta === unbxdProdId
    );
  };

  showAddedToBagCTA = (
    isProductCTA,
    addBtnMsg,
    outfitCarouselState,
    isEnabledOutfitAddedCTA,
    isPDPAddedToBag
  ) =>
    isProductCTA &&
    addBtnMsg &&
    !outfitCarouselState &&
    (isEnabledOutfitAddedCTA || isPDPAddedToBag);

  addedToMsgWrapper = (sticky) => {
    const {
      addBtnMsg,
      hasLoyaltyAbTestFlag,
      addedToBagLabel,
      outfitCarouselState,
      showAddtoBagDrawer,
      isPDP,
      isEnabledOutfitAddedCTA,
      isNewPDPEnabled,
    } = this.props;
    const isPDPAddedToBag = (isPDP && showAddtoBagDrawer) || hasLoyaltyAbTestFlag;
    const showAddedCTA = this.showAddedToBagCTA(
      this.isSelectedProductCTA(),
      addBtnMsg,
      outfitCarouselState,
      isEnabledOutfitAddedCTA,
      isPDPAddedToBag
    );
    return (
      <>
        {showAddedCTA && !isNewPDPEnabled && (
          <Button
            className={`add-to-bag-button ${sticky ? 'added-to-bag-sticky' : 'added-to-bag'}`}
          >
            <div className="check" />
            {addedToBagLabel}
          </Button>
        )}
        {addBtnMsg && !outfitCarouselState && this.addedToBagMsgAction()}
      </>
    );
  };

  addedToMsgWrapperOutfit = () => {
    const { addedToBagLabel, showOutfitCarouselAddedCTA, isNewPDPEnabled } = this.props;
    return (
      <>
        {showOutfitCarouselAddedCTA && !isNewPDPEnabled && (
          <Button className="add-to-bag-button added-to-bag">
            <div className="check" />
            {addedToBagLabel}
          </Button>
        )}
        {showOutfitCarouselAddedCTA && this.addedToBagMsgAction(true)}
      </>
    );
  };

  addedToBagMsgAction = (isOutfit = false) => {
    const { addedTobagMsgDispatch, outfitCarouselAddedToBagMsgDispatch } = this.props;
    const bagMsgDispatchTimeout = setTimeout(() => {
      if (isOutfit) {
        outfitCarouselAddedToBagMsgDispatch(false);
      } else {
        addedTobagMsgDispatch(false);
      }
      clearTimeout(bagMsgDispatchTimeout);
    }, 2000);
  };

  getColor = (product, selectedColorProductId) => {
    return (
      (product &&
        product.colorFitsSizesMap &&
        product.colorFitsSizesMap
          .filter((productTile) => {
            return productTile.colorDisplayId === selectedColorProductId;
          })
          .map((productTile) => productTile.color.name)
          .join('')) ||
      ''
    );
  };

  getProdCategory = (category) => {
    let prodCategory = '';
    if (category === 'accessories') {
      prodCategory = 'accessories';
    } else if (category === 'shoes') {
      prodCategory = 'shoes';
    } else {
      prodCategory = 'apparel';
    }
    return prodCategory;
  };

  getTrackProps = (props) => {
    const {
      currentProduct = {},
      selectedSize = {},
      selectedFit,
      selectedColor = {},
      pageData,
      itemBrand,
      currency,
      isPDP,
      isOutfitPage,
    } = props;
    const {
      generalProductId,
      name,
      offerPrice,
      listPrice,
      colorFitsSizesMap,
      reviewsCount,
      ratings,
      isGiftCard,
      productId,
    } = currentProduct;
    const prodId = generalProductId && generalProductId.split('_')[0];
    const productName = name && name.toLowerCase();

    let trackingCategories = {};
    const { pageName = '' } = pageData || {};
    const [pageType, departmentList, categoryList, listingSubCategory] = pageName.split(':');
    if (pageType === 'browse') {
      trackingCategories = {
        departmentList,
        categoryList,
        listingSubCategory,
      };
    }

    const addToBagTrackProduct = {
      id: prodId,
      colorId: generalProductId,
      brand: itemBrand,
      name: productName,
      price: offerPrice,
      CurrCurrency: currency,
      listPrice,
      prodColor: this.getColor(currentProduct, generalProductId),
      extPrice: listPrice,
      pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
      size: selectedSize.name,
      sku: getSkuId(colorFitsSizesMap, selectedColor.name, selectedFit, selectedSize.name),
      reviews: reviewsCount,
      rating: ratings,
      category: this.getProdCategory(categoryList),
      productFeatures: getFeatures(colorFitsSizesMap, generalProductId, isGiftCard),
      fromPDP: isPDP,
      fromPageName: isClient() ? getGAPageName(window.location.pathname) : '',
      isOutfitPage,
      ...addRecommendationsObj(productId),
    };
    return {
      addToBagTrackProduct,
      pageShortName: productId ? `product:${productId}:${productName}` : '',
      productId,
      outfitPageShortName: productId ? `outfit:${productId}:${productName}` : '',
      trackingCategories,
    };
  };

  scrollIntoView = () => {
    const errorDiv = isSafariBrowser()
      ? document.getElementsByClassName('product-summary-wrapper')[0]
      : document.getElementsByClassName('please-enter-an-error')[0];
    const { isMobile } = getViewportInfo();
    if (errorDiv && isMobile) errorDiv.scrollIntoView({ behavior: 'smooth', block: 'center' });
  };

  revertAddedToBagAction = async () => {
    const {
      revertCarouselAtbButton,
      showAddedToBagCta,
      outfitCarouselState,
      revertAtbButton,
      showOutfitCarouselAddedCTA,
    } = this.props;
    if (showAddedToBagCta && !outfitCarouselState) {
      revertAtbButton();
    }
    if (showOutfitCarouselAddedCTA) {
      revertCarouselAtbButton();
    }
  };

  getProductId = (products) => {
    return products && products.length && products[0].sku;
  };

  checkIfSizeIsDisabled = () => {
    const { selectedSize, sizeList } = this.props;
    const disabledSizeArray = sizeList.filter((sizeItem) => {
      return (
        (sizeItem && sizeItem.displayName) === (selectedSize && selectedSize.name) &&
        sizeItem &&
        sizeItem.disabled
      );
    });
    return disabledSizeArray && disabledSizeArray.length > 0;
  };

  checkIfAllSizeIsDisabled = () => {
    const { sizeList } = this.props;
    const disabledSizeArray = sizeList?.filter((sizeItem) => {
      return sizeItem?.disabled === false;
    });
    return disabledSizeArray && disabledSizeArray.length === 0;
  };

  renderFulFilmentSection = (
    isMultiItemQVModal,
    fromBagPage,
    product,
    currentColorEntry,
    isFavoriteEdit,
    className,
    { itemBrand, availableTCPmapNewStyleId, initialMultipackMapping, setInitialTCPStyleQty }
  ) => {
    const {
      isNewQVEnabled,
      productInfoFromBag,
      pdpUrlFromBag,
      quickViewLabels,
      onCloseClick,
      isNewPDPEnabled,
      isPickup,
      checkForOOSForVariant,
      alternateBrandFromUrl,
    } = this.props;
    const fromPickup = !isMobileApp();
    const { isPickUpSelected } = this.state;
    const { isGiftCard } = product;
    const isAllSizeDisabled = this.checkIfAllSizeIsDisabled();

    return (
      !isMultiItemQVModal &&
      !isFavoriteEdit &&
      product &&
      currentColorEntry && (
        <BodyCopy
          component="div"
          className={`${className} fulfillment-section-wrapper ${
            isNewQVEnabled ? this.quickViewDrawerClass : ''
          } ${isGiftCard ? 'gift-card-new-pdp' : ''} `}
        >
          <ProductPickupContainer
            productInfo={product}
            itemBrand={itemBrand}
            formName={`ProductAddToBag-${product.generalProductId}`}
            miscInfo={currentColorEntry.miscInfo}
            onPickupClickAddon={() => onCloseClick(true)}
            availableTCPmapNewStyleId={availableTCPmapNewStyleId}
            initialMultipackMapping={initialMultipackMapping}
            setInitialTCPStyleQty={setInitialTCPStyleQty}
            isNewQVEnabled={isNewQVEnabled && !isPickup}
            currentColorEntry={currentColorEntry}
            productInfoFromBag={productInfoFromBag}
            fromBagPage={fromBagPage}
            pdpUrlFromBag={pdpUrlFromBag}
            onCloseClick={() => onCloseClick(fromPickup)}
            quickViewLabels={quickViewLabels}
            pickupSelectionUpdate={this.pickupSelectionUpdate}
            isPickUpSelected={isPickUpSelected}
            isNewPDPEnabled={isNewPDPEnabled}
            checkForOOSForVariant={checkForOOSForVariant}
            alternateBrand={alternateBrandFromUrl}
            isAllSizeDisabled={isAllSizeDisabled}
          />
        </BodyCopy>
      )
    );
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  renderAddToBagButton = (uniqueKeyId, analyticsData, className) => {
    const {
      fitChanged,
      displayErrorMessage,
      handleFormSubmit,
      keepAlive,
      selectedSize,
      unbxdId,
      selectedQuantity,
      isLoading,
      formValues,
      setAnalyticsData,
      checkForOOSForVariant,
      isNewQVEnabled,
      isPDP,
      socialProofMessage,
      isNewPDPEnabled,
    } = this.props;

    const { products } = analyticsData;
    const productId = this.getProductId(products);
    const productVariant = products && products.length && products[0].colorId;
    const isSizeDisabled = this.checkIfSizeIsDisabled();
    let selectedSizeValue = selectedSize?.name;
    if (!selectedSizeValue && typeof formValues?.[0]?.size === 'string') {
      selectedSizeValue = formValues?.[0]?.size;
    }
    const stickyBtn = this.renderStickyATCButton();
    const socialProofMessageCheck =
      !isNewPDPEnabled && this.isMobile && isPDP && socialProofMessage?.variation === 'bottom';
    return (
      <div
        className={`${isNewPDPEnabled ? 'pdp-redesign-wrapper' : ''} ${
          socialProofMessageCheck ? 'social-proof-message-atb' : ''
        }`}
      >
        {socialProofMessageCheck ? (
          <SocialProofMessage socialProofMessage={socialProofMessage} />
        ) : null}
        {isNewPDPEnabled && <div className="new-pdp-qty-selector">{this.qtySelector()}</div>}

        <Button
          type="submit"
          uniqueKey={uniqueKeyId}
          className={`${className} ${isNewQVEnabled ? this.quickViewDrawerClass : ''}`}
          disabled={
            !isLoading
              ? keepAlive || checkForOOSForVariant || this.checkIfAllSizeIsDisabled()
              : isLoading
          }
          unbxdattr="AddToCart"
          unbxdparam_sku={productId}
          unbxdparam_variant={productVariant}
          unbxdparam_qty={selectedQuantity}
          unbxdparam_requestId={unbxdId}
          onClick={(e) => {
            e.preventDefault();
            if (fitChanged || !selectedSizeValue || isSizeDisabled) {
              displayErrorMessage(true);
              if (stickyBtn) this.scrollIntoView();
            } else {
              handleFormSubmit();
            }
            setAnalyticsData(analyticsData);
          }}
        >
          {this.getButtonLabel()}
        </Button>
      </div>
    );
  };

  getAddToBagOutfitCarouselButton = (uniqueKeyId, analyticsData, className) => {
    const { isLoading, isPDP, showOutfitCarouselAddedCTA, setInitialQuickViewLoad } = this.props;
    if (isLoading && !isPDP && setInitialQuickViewLoad) {
      return <ProductDetailSectionSkeleton />;
    }
    return !showOutfitCarouselAddedCTA ? (
      this.renderAddToBagButton(uniqueKeyId, analyticsData, className)
    ) : (
      <></>
    );
  };

  getAddToBagButton = (uniqueKeyId, analyticsData, className) => {
    const { isLoading, isPDP, addBtnMsg, setInitialQuickViewLoad } = this.props;
    if (isLoading && !isPDP && setInitialQuickViewLoad) {
      return <ProductDetailSectionSkeleton />;
    }
    return !addBtnMsg || !this.isSelectedProductCTA() ? (
      this.renderAddToBagButton(uniqueKeyId, analyticsData, className)
    ) : (
      <></>
    );
  };

  getAddToBagButtonAction = (uniqueKeyId, clickDataAnalytics, className) => {
    const { isOutfitCarousel } = this.props;
    return !isOutfitCarousel
      ? this.getAddToBagButton(uniqueKeyId, clickDataAnalytics, className)
      : this.getAddToBagOutfitCarouselButton(uniqueKeyId, clickDataAnalytics, className);
  };

  shouldRenderStickyButton = () => {
    const { isOutfitPage, hideStickyABTest, keepAlive, isBundleProduct, isPDP } = this.props;
    return isPDP && !hideStickyABTest && !isBundleProduct && !isOutfitPage && !keepAlive;
  };

  renderStickyATCButton = (fromScroll) => {
    const { showStickyATC } = this.state;
    return (this.shouldRenderStickyButton() && (fromScroll || showStickyATC)) || false;
  };

  renderStickyPickUp = (fromScroll) => {
    const { showStickyPickUp } = this.state;
    return (this.shouldRenderStickyButton() && (fromScroll || showStickyPickUp)) || false;
  };

  selectedVal = (setInitialTCPStyleQty, TCPStyleQTY) => {
    const { fromBagPage } = this.props;
    return !fromBagPage && setInitialTCPStyleQty ? setInitialTCPStyleQty : TCPStyleQTY;
  };

  getToolTipDirection = (isDisabled, isPillSelected) => {
    if (isDisabled && !isPillSelected && !this.isMobile && !this.isTablet) {
      return 'center-for-mpack';
    }
    return '';
  };

  renderPillsWithToolTip = (params) => {
    const {
      item,
      multipackToSelect,
      mpackBg,
      direction,
      isDisabled,
      index,
      singlePack,
      pdpLabels,
      getToolTipLabel,
      newQVDesign,
      disableSwappingMpackPills,
      softDisablePill,
    } = params;

    return (
      <ReactTooltip
        fontFamily="secondary"
        message={getToolTipLabel}
        minWidth="179px"
        direction={direction}
      >
        <BodyCopy
          component="li"
          key={item}
          className={item === multipackToSelect ? `${mpackBg}-wrapper` : null}
        >
          <button
            className={`${disableSwappingMpackPills ? 'mpack-btn' : 'mpack-btn-swap'} ${
              newQVDesign ? this.quickViewDrawerClass : ''
            } ${softDisablePill} ${item === multipackToSelect ? `${mpackBg} ` : ''}`}
            onClick={(e) => this.onClickMultipackBtn(e, index, item, singlePack)}
            disabled={isDisabled}
          >
            {item}
            {item !== singlePack ? pdpLabels && `${pdpLabels.lblPdpMultipackPack}` : ''}
          </button>
        </BodyCopy>
      </ReactTooltip>
    );
  };

  renderPillsWithoutToolTip = (params) => {
    const {
      item,
      multipackToSelect,
      mpackBg,
      disableSwappingMpackPills,
      softDisablePill,
      index,
      singlePack,
      isDisabled,
      pdpLabels,
      newQVDesign,
    } = params;

    return (
      <BodyCopy
        component="li"
        key={item}
        className={item === multipackToSelect ? `${mpackBg}-wrapper` : null}
      >
        <button
          className={`${disableSwappingMpackPills ? 'mpack-btn' : 'mpack-btn-swap'} ${
            newQVDesign ? this.quickViewDrawerClass : ''
          } ${softDisablePill} ${item === multipackToSelect ? `${mpackBg} ` : ''}`}
          onClick={(e) => this.onClickMultipackBtn(e, index, item, singlePack)}
          disabled={isDisabled}
        >
          {item}
          {item !== singlePack ? pdpLabels && `${pdpLabels.lblPdpMultipackPack}` : ''}
        </button>
      </BodyCopy>
    );
  };

  MultipackButton = ({
    item,
    multipackToSelect,
    singlePack,
    pdpLabels,
    index,
    disableMultiPackTab,
    selectedMultipack,
    softDisableMutiPack,
    disableSwappingMpackPills,
    mpackBg,
    newQVDesign,
  }) => {
    const selectedMultiPackVal = Number(selectedMultipack) === 1 ? item : selectedMultipack;
    const softClass = softDisableMutiPack ? [...new Set(getSoftClass(softDisableMutiPack))] : [];
    const getSingleKey = pdpLabels && pdpLabels.lblPdpMultipackSingle;
    const getToolTipLabel = pdpLabels && pdpLabels.lblUnavailableColour;
    const softDisablePill = checkAvailablePill(item, softClass, multipackToSelect, getSingleKey);
    const isDisabled = disableMultiPackTab && item === selectedMultiPackVal;
    const direction = this.getToolTipDirection(
      softDisablePill === 'soft-disable',
      item === multipackToSelect
    );
    return disableSwappingMpackPills && direction !== ''
      ? this.renderPillsWithToolTip({
          item,
          multipackToSelect,
          mpackBg,
          direction,
          isDisabled,
          index,
          singlePack,
          pdpLabels,
          getToolTipLabel,
          newQVDesign,
          disableSwappingMpackPills,
          softDisablePill,
        })
      : this.renderPillsWithoutToolTip({
          item,
          multipackToSelect,
          mpackBg,
          disableSwappingMpackPills,
          softDisablePill,
          index,
          singlePack,
          isDisabled,
          pdpLabels,
          newQVDesign,
        });
  };

  MultipackTabWrapper = (disableSwappingMpackPills) => {
    const {
      initialMultipackMapping,
      TCPStyleType,
      pdpLabels,
      selectedMultipack,
      TCPStyleQTY,
      singlePageLoad,
      TCPMultipackProductMapping,
      disableMultiPackTab,
      setInitialTCPStyleQty,
      currentProduct: { allProductId },
      initialFormValues,
      initialValues,
      isNewQVEnabled,
    } = this.props;
    const newQVDesign = isNewQVEnabled;
    const getInitialFormVal = getFormValue(initialFormValues, initialValues);
    const getSelectedProductInformation = filterCurrentSelectedProduct(
      allProductId,
      getInitialFormVal
    );
    const softDisableMutiPack = getSoftDisableMutiPack(getSelectedProductInformation);
    const singlePack = pdpLabels && pdpLabels.lblPdpMultipackSingle;
    const getSelectedVal = this.selectedVal(setInitialTCPStyleQty, TCPStyleQTY);
    const getUniqueVal =
      TCPMultipackProductMapping && initialMultipackMapping
        ? [
            ...new Set([
              ...TCPMultipackProductMapping,
              ...initialMultipackMapping,
              ...[TCPStyleQTY],
            ]),
          ]
        : TCPMultipackProductMapping;
    const multipackToSelect = getSelectedMultipack(
      selectedMultipack,
      TCPStyleQTY,
      MULTI_PACK_TYPE,
      singlePack
    );
    const findCheckMultipack = getUniqueVal?.length
      ? findMultipacks(
          TCPStyleType,
          getUniqueVal,
          singlePack,
          getSelectedVal,
          MULTI_PACK_TYPE,
          singlePageLoad
        )
      : [];
    const findCheck = findCheckMultipack && findCheckMultipack.length > 1;
    const mpackBg = 'multipack-bg';
    return (
      <>
        {findCheck && (
          <>
            <ul
              className={`multipack-tab ${newQVDesign ? 'quick-view-drawer-redesign' : ''}`}
              ref={this.mlistRef}
            >
              {findCheckMultipack.map((item, index) => {
                return (
                  (item === singlePack || !Number.isNaN(item)) &&
                  this.MultipackButton({
                    item,
                    multipackToSelect,
                    singlePack,
                    pdpLabels,
                    index,
                    disableMultiPackTab,
                    selectedMultipack,
                    softDisableMutiPack,
                    disableSwappingMpackPills,
                    mpackBg,
                    newQVDesign,
                  })
                );
              })}
            </ul>
          </>
        )}
      </>
    );
  };

  renderAddedToBagView = (outfitCarouselState, isOutfitCarousel) =>
    outfitCarouselState && isOutfitCarousel
      ? this.addedToMsgWrapperOutfit()
      : this.addedToMsgWrapper();

  getNewQVClass = (isNewQVEnabled) => {
    return isNewQVEnabled ? this.quickViewDrawerClass : '';
  };

  getBagPageGiftCardUrl = ({ isProductBrandOfSameDomain, crossDomain, pdpUrlFromBag }) => {
    return isProductBrandOfSameDomain ? pdpUrlFromBag : `${crossDomain}${pdpUrlFromBag}`;
  };

  getCurrentColorPdpUrl = (productInfo, currentColorEntry) => {
    const { seoToken, pdpUrl } = productInfo || {};
    const { pdpSeoUrl, pdpUrl: currentPdpUrl } = currentColorEntry || {};
    const currentColorPdpUrl = pdpSeoUrl || seoToken;
    return currentColorPdpUrl ? `/p/${currentColorPdpUrl}` : pdpUrl || currentPdpUrl;
  };

  getToPath = ({
    pdpToPath,
    isGiftCard,
    toPath,
    fromBagPage,
    isProductBrandOfSameDomain,
    currentColorPdpUrl,
    alternateBrand,
  }) => {
    const toPathNew = alternateBrand ? `${toPath}$brand=${alternateBrand}` : `${toPath}`;
    const currentColorPdpUrlNew = alternateBrand
      ? `${currentColorPdpUrl}$brand=${alternateBrand}`
      : `${currentColorPdpUrl}`;
    const pdpToPathNew = alternateBrand ? `${pdpToPath}$brand=${alternateBrand}` : `${pdpToPath}`;
    if (isProductBrandOfSameDomain) {
      if (fromBagPage && isGiftCard) {
        return toPathNew;
      }
      return currentColorPdpUrlNew;
    }
    return pdpToPathNew;
  };

  getBagPageGiftCardToPath = ({ isProductBrandOfSameDomain, crossDomain, pdpUrlFromBag }) => {
    return isProductBrandOfSameDomain
      ? getProductListToPath(pdpUrlFromBag)
      : `${crossDomain}${pdpUrlFromBag}`;
  };

  // eslint-disable-next-line complexity, sonarjs/cognitive-complexity
  renderAddToBagSection = (isBundleProduct, storeId, errorOnHandleSubmit) => {
    const {
      pageShortName,
      productId,
      outfitPageShortName,
      addToBagTrackProduct,
      trackingCategories,
    } = this.getTrackProps(this.props);
    const {
      isOutfitCarousel,
      outfitCarouselState,
      isStyleWith,
      socialProofMessage,
      isPDP,
      isNewQVEnabled,
      productInfo,
      isNewPDPEnabled,
    } = this.props;
    const { isGiftCard } = productInfo;

    const analyticsData = {
      eventName: 'cart add',
      pageShortName,
      products: [addToBagTrackProduct],
      customEvents: ['event61'],
      pageNavigationText: '',
      storeId,
      ...trackingCategories,
    };
    const socialProofMessageCheck =
      !isNewPDPEnabled && isPDP && socialProofMessage?.variation === 'bottom';
    const giftCardNewPdpCheck = isGiftCard && isNewQVEnabled;
    const styleWithClass = 'style-with-btn';
    return (
      <Row
        fullBleed
        className={`${errorOnHandleSubmit && !isNewQVEnabled ? 'product-size-error' : ''}`}
      >
        <Col
          colSize={{ small: 12, medium: 12, large: 12 }}
          className={`outfit-button-wrapper-desktop `}
        >
          {!isBundleProduct && !isOutfitCarousel && this.renderOutfitButton()}
          {giftCardNewPdpCheck ? (
            <div className={`${giftCardNewPdpCheck ? 'new-gift-card-pdp' : ''}`}>
              <div
                className={`button-wrapper ${
                  isStyleWith ? `${styleWithClass}` : ''
                } ${this.getNewQVClass(isNewQVEnabled)} ${
                  isGiftCard && isNewQVEnabled ? 'giftCard' : ''
                }`}
              >
                {this.renderAddedToBagView(outfitCarouselState, isOutfitCarousel)}
                {this.getAddToBagButtonAction(
                  `desktop_${productId}`,
                  {
                    customEvents: [DEFAULT_EVENTS],
                    eventName: 'cart add',
                    pageShortName,
                    pageName: outfitPageShortName,
                    products: [addToBagTrackProduct],
                    storeId,
                    pageNavigationText: '',
                    ...trackingCategories,
                  },
                  this.ATC_BUTTON
                )}
              </div>
            </div>
          ) : (
            <div
              className={`button-wrapper ${
                isStyleWith ? `${styleWithClass}` : ''
              } ${this.getNewQVClass(isNewQVEnabled)} ${
                isGiftCard && isNewQVEnabled ? 'giftCard' : ''
              }`}
            >
              {this.renderAddedToBagView(outfitCarouselState, isOutfitCarousel)}
              {this.getAddToBagButtonAction(
                `desktop_${productId}`,
                {
                  customEvents: [DEFAULT_EVENTS],
                  eventName: 'cart add',
                  pageShortName,
                  pageName: outfitPageShortName,
                  products: [addToBagTrackProduct],
                  storeId,
                  pageNavigationText: '',
                  ...trackingCategories,
                },
                this.ATC_BUTTON
              )}
            </div>
          )}
        </Col>
        <Col colSize={{ small: 6, medium: 8, large: 12 }} className={`outfit-button-wrapper `}>
          {giftCardNewPdpCheck ? (
            <div className={`${giftCardNewPdpCheck ? 'new-gift-card-pdp' : ''}`}>
              <div
                className={`button-wrapper ${
                  isStyleWith ? `${styleWithClass}` : ''
                } ${this.getNewQVClass(isNewQVEnabled)} ${isGiftCard ? 'giftCard' : ''}`}
              >
                {this.renderAddedToBagView(outfitCarouselState, isOutfitCarousel)}
                {this.getAddToBagButtonAction(productId, analyticsData, this.ATC_BUTTON)}
                {this.renderStickyATCButton() && (
                  <div
                    className={`sticky-atc-wrapper ${socialProofMessageCheck ? 'socialProof' : ''}`}
                  >
                    {this.addedToMsgWrapper('sticky')}
                    {this.getAddToBagButtonAction(
                      productId,
                      analyticsData,
                      `${this.ATC_BUTTON} stickyATC`
                    )}
                  </div>
                )}
              </div>
              {!isBundleProduct && this.renderOutfitButton()}
            </div>
          ) : (
            <>
              <div
                className={`button-wrapper ${
                  isStyleWith ? `${styleWithClass}` : ''
                } ${this.getNewQVClass(isNewQVEnabled)} ${isGiftCard ? 'giftCard' : ''}`}
              >
                {this.renderAddedToBagView(outfitCarouselState, isOutfitCarousel)}
                {this.getAddToBagButtonAction(productId, analyticsData, this.ATC_BUTTON)}
                {this.renderStickyATCButton() && (
                  <div
                    className={`sticky-atc-wrapper ${socialProofMessageCheck ? 'socialProof' : ''}`}
                  >
                    {this.addedToMsgWrapper('sticky')}
                    {this.getAddToBagButtonAction(
                      productId,
                      analyticsData,
                      `${this.ATC_BUTTON} stickyATC`
                    )}
                  </div>
                )}
              </div>
              {!isBundleProduct && this.renderOutfitButton()}
            </>
          )}
        </Col>
      </Row>
    );
  };

  renderSimilarlink = (
    checkForOOSForVariant,
    isRecommendationAvailable,
    seeSimilarItems,
    soldOut
  ) => {
    return (
      <>
        {checkForOOSForVariant && isRecommendationAvailable && (
          <div className="oos-similar-block">
            <div className="oos-similar-items">
              <span className="oos-similar-items-title">{soldOut}</span>
              <a href="#recommendation" className="oos-similar-items-link">
                {seeSimilarItems}
              </a>
            </div>
          </div>
        )}
      </>
    );
  };

  /**
   * This just holds the logic for rendering a UX timer
   */
  setCTAPerformanceTimer = () => {
    const { IsCallToActionVisible } = this.state;
    return IsCallToActionVisible ? <RenderPerf.Measure name={CALL_TO_ACTION_VISIBLE} /> : null;
  };

  getProductSummary = (keepAlive, handleMetaProps, isInWishList) => {
    const {
      productDetailsPDP,
      productInfo,
      currencySymbol,
      pdpLabels,
      currencyAttributes,
      onAddItemToFavorites,
      isLoggedIn,
      outOfStockLabels,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      isLoading,
      wishList,
      hidePriceDisplay,
      isPerUnitPriceEnabled,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      TCPStyleQTY,
      deleteFavItemInProgressFlag,
      checkForOOSForVariant,
      currentProduct: { colorFitsSizesMap },
      selectedColorProductId,
      getGiftCardValue,
      isShowBrandNameEnabled,
      openModal,
      isPlcc,
      formValues,
      handleRatingClick,
      ...otherProps
    } = this.props;
    const currentColorEntry =
      getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId) || {};
    const { colorDisplayId, colorProductId } = currentColorEntry || {};
    const { isGiftCard, primaryBrand } = productInfo;
    return (
      <div className="product-summary-wrapper">
        <Product
          {...otherProps}
          isGiftCard={isGiftCard}
          productDetails={productDetailsPDP}
          currencySymbol={currencySymbol}
          selectedColorProductId={colorProductId}
          currencyAttributes={currencyAttributes}
          onAddItemToFavorites={onAddItemToFavorites}
          isLoggedIn={isLoggedIn}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          className="hide-on-mobile"
          AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
          removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
          isLoading={isLoading}
          isInWishList={isInWishList}
          isHasPlcc={isPlcc}
          hidePriceDisplay={hidePriceDisplay}
          pdpLabels={pdpLabels}
          isPerUnitPriceEnabled={isPerUnitPriceEnabled}
          isBrierleyPromoEnabled={isBrierleyPromoEnabled}
          TCPStyleQTY={TCPStyleQTY}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          wishList={wishList}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          isAfterPayDisplay={!checkForOOSForVariant}
          isOutfitPage={false}
          colorDisplayId={colorDisplayId}
          primaryBrand={primaryBrand}
          isShowBrandNameEnabled={isShowBrandNameEnabled}
          openModal={openModal}
          formValues={formValues}
          handleRatingClick={handleRatingClick}
        />
        {isGiftCard ? (
          <div className="product-price-desktop-view">
            {isLoading ? (
              <LoaderSkelton />
            ) : (
              <ProductPrice
                offerPrice={parseInt(getGiftCardValue, 10)}
                listPrice={parseInt(getGiftCardValue, 10)}
                hideItemProp // prop is used to hide unwanted duplicate itemProp as we have duplicate ProductPrice component for web and mobile on PDP pages
                currencySymbol={currencySymbol}
                currencyAttributes={currencyAttributes}
                isGiftCard={isGiftCard}
                isPlcc={isPlcc}
                pdpLabels={pdpLabels}
                isPerUnitPriceEnabled={isPerUnitPriceEnabled}
                TCPStyleQTY={TCPStyleQTY}
                wishList={wishList}
                isAfterPayDisplay
              />
            )}
          </div>
        ) : null}
        {isGiftCard ? (
          <div className="product-price-mobile-view">
            {isLoading ? (
              <LoaderSkelton />
            ) : (
              <ProductPrice
                listPrice={parseInt(getGiftCardValue, 10)}
                offerPrice={parseInt(getGiftCardValue, 10)}
                currencyAttributes={currencyAttributes}
                currencySymbol={currencySymbol}
                isPlcc={isPlcc}
                isPerUnitPriceEnabled={isPerUnitPriceEnabled}
                pdpLabels={pdpLabels}
                TCPStyleQTY={TCPStyleQTY}
                isAfterPayDisplay
              />
            )}
          </div>
        ) : null}
      </div>
    );
  };

  onClickMultipackBtn = (e, index, item, singlePack) => {
    const {
      setSelectedMultipack,
      getDetails,
      currentProduct,
      onQuickViewOpenClick,
      fromPDP,
      isPDP,
      closePickupModal,
      setInitialTCPStyleQty,
      resetProductDetailsDynamicData,
      getQickViewSingleLoad,
      getDisableSelectedTab,
      isPickup,
      onPickUpOpenClick,
      fromBagPage,
      productInfoFromBag,
      availableTCPmapNewStyleId,
      alternateBrand,
      isPickUpSelected,
      fromPage,
    } = this.props;
    e.preventDefault();
    const targetClassList = e.target?.classList;
    const targetClassArray = targetClassList ? [...targetClassList] : [];
    if (targetClassArray?.indexOf('multipack-bg') < 0) {
      onTabClickDetails({
        setSelectedMultipack,
        getDetails,
        currentProduct,
        onQuickViewOpenClick,
        fromPDP,
        isPDP,
        e,
        item,
        singlePack,
        setInitialTCPStyleQty,
        resetProductDetailsDynamicData,
        getQickViewSingleLoad,
        getDisableSelectedTab,
        closePickupModal,
        isPickup,
        onPickUpOpenClick,
        fromBagPage,
        productInfoFromBag,
        availableTCPmapNewStyleId,
        fromPills: true,
        alternateBrand,
        isBopisPickup: isPickUpSelected === 'bopis',
        fromPage,
      });
    }
  };

  qtySelector = () => {
    const { quantityList, isFromBagProductSfl, isOutfitCarousel, isStyleWith } = this.props;
    return (
      !isOutfitCarousel &&
      !isStyleWith &&
      this.renderQuantitySelector(
        isFromBagProductSfl,
        MiniBagSelect,
        quantityList,
        this.quantityChange
      )
    );
  };

  getProductCartError = (addToBagErrorId) => {
    const { currentProduct, errorOnHandleSubmit } = this.props;
    if (!addToBagErrorId) {
      return errorOnHandleSubmit;
    }
    return addToBagErrorId && addToBagErrorId === currentProduct.unbxdProdId
      ? errorOnHandleSubmit
      : '';
  };

  renderPills = (isPhysicalMpackEnabled, hideMultipackPills, disableSwappingMpackPills) => {
    const { isPDP } = this.props;
    const modalClass = isPDP ? '' : 'multipack-pills-css';
    return disableSwappingMpackPills && isPhysicalMpackEnabled && !hideMultipackPills ? (
      <div className={`edit-form-css ${modalClass}`}>
        <div
          className={`multipack-tab-edit-css ${disableSwappingMpackPills ? 'tooltip-style' : ''}`}
          ref={this.mTabRef}
        >
          {this.MultipackTabWrapper(disableSwappingMpackPills)}
        </div>
      </div>
    ) : null;
  };

  checkInWishlist = (wishList, productId) =>
    wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;

  shouldRenderColorList = () => {
    const { isModalOpen, isOutfitCarousel, newColorList, isOutfitPage } = this.props;
    if (isOutfitPage) return true;
    if (!isModalOpen && !((newColorList && newColorList.length) || isOutfitCarousel)) return false;
    return true;
  };

  getGender = (breadcrumbs) => {
    const gender =
      breadcrumbs && breadcrumbs[0] && breadcrumbs[0].displayName
        ? breadcrumbs[0].displayName.toLowerCase()
        : '';
    return gender === 'home' ? '' : gender;
  };

  trackBossBopisClick = (isBopisSelected, isBossSelected, productFormData) => {
    const {
      currentProduct,
      setAnalyticsData,
      storeId,
      breadCrumbs,
      userDefaultStore,
      trackClickDispatch,
    } = this.props;
    const { color, size, fit, quantity } = productFormData;
    const { listPrice, offerPrice, reviewsCount, ratings, generalProductId = '' } = currentProduct;
    let sku = '';
    let storeSearchDistance = '';
    let storeSearchCriteria = '';
    if (userDefaultStore) {
      sku = userDefaultStore?.productAvailability?.skuId;
      storeSearchDistance = userDefaultStore.distance;
      storeSearchCriteria = userDefaultStore?.basicInfo?.address?.zipCode;
    }
    const trackProduct = {
      colorId: generalProductId,
      color,
      id: generalProductId.split('_')[0],
      price: offerPrice,
      extPrice: listPrice,
      listPrice,
      pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
      rating: ratings,
      reviews: reviewsCount,
      size,
      fit,
      quantity,
      sku,
      storeId,
      gender: this.getGender(breadCrumbs),
    };

    // Checking BOSS BOPIS for analytics
    let customEventsVal = '';
    let eventName = DEFAULT_EVENTS;
    if (isBopisSelected) {
      customEventsVal += 'event132';
      eventName = 'bopis cta click on pickup modal';
    }
    if (isBossSelected) {
      customEventsVal += 'event133';
      eventName = 'boss cta click on pickup modal';
    }

    trackClickDispatch({
      name: 'pickup_store_select',
      module: 'browse',
    });

    const analyticsTimer = setTimeout(() => {
      setAnalyticsData({
        customEvents: [customEventsVal],
        storeSearchCriteria,
        storeSearchDistance,
        eventName,
        products: [trackProduct],
        storeId,
        clickEvent: true,
        pageNavigationText: '',
      });
      clearTimeout(analyticsTimer);
    }, 300);
  };

  userStoreFromStorage = () => {
    return JSON.parse(getLocalStorage('defaultStore')) || '';
  };

  handleAddItemToCartInPickup = (isBopis, isBoss) => {
    const {
      addItemToCartInPickup,
      onCloseClick,
      currentProduct,
      fromPage: pageName,
      selectedSize,
      selectedFit,
      selectedColor,
      selectedQuantity,
      storeId,
      alternateBrand,
      alternateBrandFromUrl,
    } = this.props;
    const primaryBrand = currentProduct && currentProduct.primaryBrand;
    const brand = primaryBrand || alternateBrandFromUrl || alternateBrand || getBrand();
    const storeIdFromStorage = this.userStoreFromStorage()?.basicInfo?.id;
    const emptyFunc = () => {};
    const productFormData = {
      color: selectedColor && selectedColor.name,
      fit: selectedFit && selectedFit.name,
      size: selectedSize && selectedSize.name,
      wishlistItemId: false,
      quantity: selectedQuantity,
      isBoss,
      brand,
      storeLocId: storeId || storeIdFromStorage,
    };
    const productInfo = getCartItemInfo(currentProduct, productFormData);
    const payload = {
      productInfo,
      callback: onCloseClick || emptyFunc,
      fromPage: pageName || 'pdp',
    };
    this.trackBossBopisClick(isBopis, isBoss, productFormData);
    return addItemToCartInPickup(payload);
  };

  productDetailSkeletonCheck() {
    const { isLoading, isPDP, setInitialQuickViewLoad } = this.props;
    return isLoading && !isPDP && setInitialQuickViewLoad;
  }

  pickupSelectionUpdate(value) {
    const { setIsBopisSelected } = this.props;
    this.setState({ isPickUpSelected: value });
    setIsBopisSelected(value === DELIVERY_METHODS.BOPIS);
  }

  updateMultipackPill() {
    const { current } = this.mlistRef;
    const { className = '' } = current || {};

    if (className.includes('multipack-tab')) {
      const elements = Array.from(this.mlistRef.current.children);
      const index =
        elements &&
        elements.findIndex(
          (item) => item && item.className && item.className.includes('multipack-bg-wrapper')
        );
      const offsetLeftSize =
        this.mlistRef.current.children[index] && this.mlistRef.current.children[index].offsetLeft;
      if (this.mTabRef.current.scrollLeft !== offsetLeftSize) {
        this.mTabRef.current.scrollLeft = index >= 0 ? offsetLeftSize - 14 : 14;
      }
    }
  }

  getBopisAvailability = (currentColorEntry) => {
    let isOnlineExclusive;
    let isBopisEligibleFlag;
    if (currentColorEntry) {
      isBopisEligibleFlag = currentColorEntry?.miscInfo?.isBopisEligible;
      isOnlineExclusive = currentColorEntry?.miscInfo?.badge1?.defaultBadge;
    }
    return !isOnlineExclusive && isBopisEligibleFlag;
  };

  renderPickupButton(newPDPCheck, update, currentColorEntry) {
    const { isLoading, currentProduct, fromBagPage, quickViewLabels, checkForOOSForVariant } =
      this.props;
    const { pageName } = this.getPageName(currentProduct);
    const { isPickUpSelected } = this.state;
    const isPickupActive = isPickUpSelected === DELIVERY_METHODS.BOPIS;
    const isAllSizeDisabled = this.checkIfAllSizeIsDisabled();
    const isBopisEligibleFlag = this.getBopisAvailability(currentColorEntry);
    const isDisabled = isPickupActive
      ? checkForOOSForVariant || !isBopisEligibleFlag
      : checkForOOSForVariant || isAllSizeDisabled;
    return (
      <ClickTracker
        clickData={{
          customEvents: ['event131'],
          pageName,
        }}
        className={`${
          newPDPCheck ? 'pdp-redesign-wrapper store-btn-redesign' : ''
        } btn-find-in-store-wrapper-quick-view-drawer-redesign`}
      >
        {newPDPCheck && <div className="new-pdp-qty-selector">{this.qtySelector()}</div>}
        <Button
          className={`button-find-in-store-quick-view-drawer-redesign ${
            isLoading ? this.disableClass : ''
          }`}
          disabled={isLoading || isDisabled}
          buttonVariation="fixed-width"
          onClick={this.handlePickupFormSubmit}
        >
          {!fromBagPage && (
            <>
              {this.showFindInStore()
                ? quickViewLabels?.pickItUpInStore
                : quickViewLabels?.selectStore}
            </>
          )}

          {fromBagPage && <>{this.showFindInStore() ? update : quickViewLabels?.selectStore}</>}
        </Button>
      </ClickTracker>
    );
  }

  // eslint-disable-next-line
  render() {
    const {
      plpLabels,
      className,
      showAddToBagCTA,
      alternateSizes,
      isPickup,
      isBundleProduct,
      isATBErrorMessageDisplayed,
      quickViewPickup,
      storeId,
      isOutfitCarousel,
      currencySymbol,
      currencyAttributes,
      listPrice,
      offerPrice,
      isCanada,
      isPlcc,
      productDetailsToPath,
      currentProduct,
      isInternationalShipping,
      closeModal,
      isStyleWith,
      isPhysicalMpackEnabled,
      imageSwatchesToDisplay,
      checkForOOSForVariant,
      isRecommendationAvailable,
      addToBagErrorId,
      newColorList,
      hideMultipackPills,
      disableSwappingMpackPills,
      isAfterPayDisplay,
      isNewQVEnabled,
      fromBagPage,
      isFavoriteEdit,
      itemBrand,
      availableTCPmapNewStyleId,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      currentProduct: { colorFitsSizesMap },
      selectedColorProductId,
      isMultiItemQVModal,
      productInfoFromBag,
      pdpUrlFromBag,
      goToPDPPage,
      quickViewLabels,
      isFamilyOutfitEnabled,
      isNewPDPEnabled,
      isPDP,
      selectedSize,
      formValues,
      keepAlive,
      outOfStockLabels,
      pdpProductDetails,
      onAddItemToFavorites,
      isLoggedIn,
      wishList,
      resetCountUpdate,
      socialProofMessage,
      countUpdated,
      pdpLabels,
      isPerUnitPriceEnabled,
      isBrierleyPromoEnabled,
      TCPStyleQTY,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      primaryBrand,
      middlePromos,
      isShowBrandNameEnabled,
      openModal,
      alternateBrand,
      addToPickupError,
      handleRatingClick,
      ...otherProps
    } = this.props;
    const isLowTier = isTier3Device();
    const { isPickUpSelected } = this.state;
    const currentColorEntry =
      getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId) || {};
    const { colorDisplayId } = currentColorEntry || {};
    const isInWishList = this.checkInWishlist(
      wishList,
      colorDisplayId || pdpProductDetails?.currentProduct?.generalProductId
    );
    const currentColorPdpUrl = this.getCurrentColorPdpUrl(currentProduct, currentColorEntry);
    const apiConfigObj = getAPIConfig();
    const { crossDomain } = apiConfigObj;
    const currentSiteBrand = getBrand();
    const isProductBrandOfSameDomain = !isEmpty(productInfoFromBag)
      ? currentSiteBrand?.toUpperCase() === productInfoFromBag?.itemBrand?.toUpperCase()
      : true;
    let toPath = isProductBrandOfSameDomain
      ? `/${getSiteId()}${currentColorPdpUrl}`
      : `${crossDomain}/${getSiteId()}${currentColorPdpUrl}`;
    let pdpToPath = isProductBrandOfSameDomain
      ? getProductListToPath(currentColorPdpUrl)
      : `${crossDomain}/${getSiteId()}${currentColorPdpUrl}`;

    toPath = alternateBrand ? `${toPath}$brand=${alternateBrand}` : `${toPath}`;
    pdpToPath = alternateBrand ? `${pdpToPath}$brand=${alternateBrand}` : `${pdpToPath}`;
    const { isGiftCard } = currentProduct;
    if (fromBagPage && isGiftCard) {
      const tempUrl = this.getBagPageGiftCardUrl({
        isProductBrandOfSameDomain,
        crossDomain,
        pdpUrlFromBag,
      });
      const pdpPath = tempUrl.split('/');
      pdpPath.shift();
      toPath = `/${pdpPath.join('/')}`;
      pdpToPath = this.getBagPageGiftCardToPath({
        isProductBrandOfSameDomain,
        crossDomain,
        pdpUrlFromBag,
      });
    }
    const { seoToken } = currentProduct;
    const addToBagError = this.getProductCartError(addToBagErrorId);
    let { sizeList, fitList, colorList, colorFitSizeDisplayNames } = this.props;
    colorFitSizeDisplayNames = {
      color: 'Color',
      fit: 'Fit',
      size: 'Size',
      ...colorFitSizeDisplayNames,
    };

    if (sizeList) {
      sizeList = fromJS(sizeList);
      fitList = fromJS(fitList);
    }

    colorList = fromJS(colorList);

    const {
      errorMessage,
      fit: fitTitle,
      viewProductDetails = '',
      soldOut,
      seeSimilarItems,
      update,
    } = plpLabels;
    const hideQtyPdpCheck = isNewPDPEnabled;
    const socialProofBottomEnabled =
      isNewPDPEnabled && isPDP && socialProofMessage?.variation === 'bottom';

    return (
      <form className={className} noValidate>
        {this.renderPills(isPhysicalMpackEnabled, hideMultipackPills, disableSwappingMpackPills)}
        <Row className="edit-form-css">
          <Col colSize={{ small: 12, medium: 12, large: 12 }} className="edit-form-css-width">
            {this.renderSimilarlink(
              checkForOOSForVariant,
              isRecommendationAvailable,
              seeSimilarItems,
              soldOut
            )}
            <div className="select-value-wrapper">
              {this.shouldRenderColorList() &&
                this.renderColorList(
                  colorList,
                  colorFitSizeDisplayNames.color,
                  imageSwatchesToDisplay,
                  newColorList
                )}

              {isNewPDPEnabled ? (
                <div className={`${isNewPDPEnabled ? 'new-pdp-product-detail-below-wrapper' : ''}`}>
                  <Product
                    {...otherProps}
                    keepAlive={keepAlive}
                    outOfStockLabels={outOfStockLabels}
                    isGiftCard={isGiftCard}
                    productDetails={pdpProductDetails}
                    currencySymbol={currencySymbol}
                    selectedColorProductId={selectedColorProductId}
                    currencyAttributes={currencyAttributes}
                    onAddItemToFavorites={onAddItemToFavorites}
                    isLoggedIn={isLoggedIn}
                    reviewOnTop
                    hideItemProp // prop is used to hide unwanted duplicate itemProp as we have duplicate ProductPrice component for web and mobile on PDP pages
                    isHasPlcc={isPlcc}
                    isInWishList={isInWishList}
                    resetCountUpdate={resetCountUpdate}
                    socialProofMessage={socialProofMessage}
                    countUpdated={countUpdated}
                    pdpLabels={pdpLabels}
                    isPerUnitPriceEnabled={isPerUnitPriceEnabled}
                    checkForOOSForVariant={checkForOOSForVariant}
                    isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                    TCPStyleQTY={TCPStyleQTY}
                    onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                    deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                    wishList={wishList}
                    isLowTier={isLowTier}
                    isOutfitPage={false}
                    colorDisplayId={colorDisplayId}
                    isNewPDPEnabled={isNewPDPEnabled}
                    primaryBrand={primaryBrand}
                    isShowBrandNameEnabled={isShowBrandNameEnabled}
                    openModal={openModal}
                    formValues={formValues}
                    alternateBrand={alternateBrand}
                    handleRatingClick={handleRatingClick}
                  />
                </div>
              ) : null}

              {isNewPDPEnabled ? (
                <div className={`${isNewPDPEnabled ? 'product-summary-new-pdp' : ''}`}>
                  {this.getProductSummary(keepAlive, true, isInWishList)}
                </div>
              ) : null}

              {this.renderPills(
                isPhysicalMpackEnabled,
                hideMultipackPills,
                !disableSwappingMpackPills
              )}
              {this.renderFitList(fitList, fitTitle)}
              {this.renderSizeList(
                sizeList,
                colorFitSizeDisplayNames,
                errorMessage || addToPickupError,
                addToBagError || addToPickupError,
                currentColorEntry
              )}
              {!isPickup && !isFamilyOutfitEnabled && this.renderAlternateSizes(alternateSizes)}
              {quickViewPickup() && this.renderUnavailableLink()}
              {!hideQtyPdpCheck && this.qtySelector()}
            </div>
            <RenderPerf.Measure name={CONTROLS_VISIBLE} />
          </Col>
        </Row>
        <div className="please-enter-an-error">
          {(isATBErrorMessageDisplayed || addToPickupError) &&
            !isNewPDPEnabled &&
            ErrorComp(addToBagError || addToPickupError, showAddToBagCTA)}
        </div>

        {isOutfitCarousel && !isStyleWith && (
          <>
            <hr className="dotted-line" />
            <ProductPrice
              isOutfitPage
              currencySymbol={currencySymbol}
              currencyAttributes={currencyAttributes}
              listPrice={listPrice}
              offerPrice={offerPrice}
              isCanada={isCanada}
              isInternationalShipping={isInternationalShipping}
              isPlcc={isPlcc}
              isAfterPayDisplay={isAfterPayDisplay}
            />
          </>
        )}
        <div className={`${isNewPDPEnabled ? 'newPDPWrapper' : ''}`}>
          {isNewQVEnabled && !isPickup && (
            <div className="fulfillment-section-wrapper-quick-view-drawer-redesign">
              {this.renderFulFilmentSection(
                isMultiItemQVModal,
                fromBagPage,
                currentProduct,
                currentColorEntry,
                isFavoriteEdit,
                className,
                {
                  itemBrand,
                  availableTCPmapNewStyleId,
                  initialMultipackMapping,
                  setInitialTCPStyleQty,
                }
              )}
              <div className="view-product-details-qv-redesign">
                <Anchor
                  dataLocator={getLocator('quick_view_View_Product_details')}
                  className="link-redirect"
                  noLink
                  to={toPath}
                  onClick={(e) =>
                    goToPDPPage(
                      e,
                      pdpToPath,
                      this.getToPath({
                        pdpToPath,
                        isGiftCard,
                        toPath,
                        fromBagPage,
                        currentColorPdpUrl,
                        isProductBrandOfSameDomain,
                        alternateBrand,
                      })
                    )
                  }
                >
                  {!isPDP && (
                    <BodyCopy
                      className="product-link-fulfillment-quick-view-drawer-redesign"
                      fontSize="fs12"
                      fontFamily="secondary"
                      fontWeight="extrabold"
                    >
                      {quickViewLabels?.viewFullProductDetails}
                    </BodyCopy>
                  )}
                </Anchor>
              </div>
            </div>
          )}
          {socialProofBottomEnabled ? (
            <div className="social-proof-desktop-bottom">
              <SocialProofMessage socialProofMessage={socialProofMessage} />
            </div>
          ) : null}
          {showAddToBagCTA && isNewQVEnabled && (
            <div
              className={`${
                isNewQVEnabled && !isNewPDPEnabled ? 'add-to-bag-section-wrapper' : ''
              }`}
            >
              {isPickUpSelected === DELIVERY_METHODS.HOME
                ? this.renderAddToBagSection(
                    isBundleProduct,
                    storeId,
                    addToBagError || addToPickupError
                  )
                : this.renderPickupButton(hideQtyPdpCheck, update, currentColorEntry)}
              {this.renderStickyPickUp() && (
                <div className="sticky-atc-wrapper">
                  {this.renderPickupButton(hideQtyPdpCheck, update, currentColorEntry)}
                </div>
              )}
              {this.setCTAPerformanceTimer()}
            </div>
          )}
        </div>
        {showAddToBagCTA && !isNewQVEnabled && (
          <>
            {this.renderAddToBagSection(
              isBundleProduct,
              storeId,
              addToBagError || addToPickupError
            )}
            {this.setCTAPerformanceTimer()}
          </>
        )}
        {productDetailsToPath && (
          <Anchor
            onClick={() => closeModal()}
            className="view-product-details"
            to={productDetailsToPath}
            asPath={`/p/${seoToken}`}
            underline
            fontSizeVariation="large"
          >
            {viewProductDetails}
          </Anchor>
        )}
      </form>
    );
  }
}

export default compose(
  connect((state, props) => {
    const formName = props.customFormName || PRODUCT_ADD_TO_BAG;
    return {
      form: `${formName}-${props.generalProductId}`,
      enableReinitialize: true,
    };
  }),
  reduxForm()
)(withStyles(ProductAddToBag, styles));

ProductAddToBag.propTypes = {
  fromBagPage: PropTypes.bool.isRequired,
  plpLabels: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  keepAlive: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  isFavoriteEdit: PropTypes.bool.isRequired,
  currentProduct: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  selectedColorProductId: PropTypes.number.isRequired,
  isOutfitPage: PropTypes.bool.isRequired,
  selectColor: PropTypes.string.isRequired,
  isGiftCard: PropTypes.bool.isRequired,
  showColorChips: PropTypes.bool.isRequired,
  quickViewColorSwatchesCss: PropTypes.string.isRequired,
  isPDP: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
  imagesByColor: PropTypes.shape({}).isRequired,
  relatedSwatchImages: PropTypes.shape({}).isRequired,
  isMultiItemQVModal: PropTypes.bool.isRequired,
  itemBrand: PropTypes.string.isRequired,
  selectFit: PropTypes.func.isRequired,
  selectedFit: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  hideAlternateSizes: PropTypes.shape([]).isRequired,
  onCloseClick: PropTypes.func,
  isFromBagProductSfl: PropTypes.bool.isRequired,
  isPickup: PropTypes.bool.isRequired,
  sizeChartLinkVisibility: PropTypes.number.isRequired,
  isErrorMessageDisplayed: PropTypes.bool.isRequired,
  selectSize: PropTypes.func.isRequired,
  isDisableZeroInventoryEntries: PropTypes.bool.isRequired,
  sizeChartDetails: PropTypes.string.isRequired,
  isSKUValidated: PropTypes.bool.isRequired,
  onQuantityChange: PropTypes.func.isRequired,
  quantityList: PropTypes.shape([]).isRequired,
  fitChanged: PropTypes.bool.isRequired,
  currencySymbol: PropTypes.string.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  currencyAttributes: PropTypes.shape({}).isRequired,
  isCanada: PropTypes.bool.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  listPrice: PropTypes.string.isRequired,
  offerPrice: PropTypes.string.isRequired,
  displayErrorMessage: PropTypes.func.isRequired,
  handleFormSubmit: PropTypes.func.isRequired,
  setAnalyticsData: PropTypes.func.isRequired,
  showAddedToBagCta: PropTypes.bool.isRequired,
  revertAtbButton: PropTypes.func.isRequired,
  addedToBagLabel: PropTypes.string.isRequired,
  selectedSize: PropTypes.string.isRequired,
  unbxdId: PropTypes.string.isRequired,
  selectedQuantity: PropTypes.number.isRequired,
  hideStickyABTest: PropTypes.func.isRequired,
  isBundleProduct: PropTypes.bool.isRequired,
  errorOnHandleSubmit: PropTypes.func.isRequired,
  showAddToBagCTA: PropTypes.bool.isRequired,
  alternateSizes: PropTypes.shape([]).isRequired,
  isATBErrorMessageDisplayed: PropTypes.bool.isRequired,
  quickViewPickup: PropTypes.func.isRequired,
  outfitCarouselAddedToBagMsgDispatch: PropTypes.func.isRequired,
  storeId: PropTypes.string.isRequired,
  sizeList: PropTypes.shape([]).isRequired,
  fitList: PropTypes.shape([]).isRequired,
  colorList: PropTypes.shape([]).isRequired,
  colorFitSizeDisplayNames: PropTypes.shape([]).isRequired,
  addedTobagMsgDispatch: PropTypes.shape([]).isRequired,
  addBtnMsg: PropTypes.bool.isRequired,
  hasLoyaltyAbTestFlag: PropTypes.bool,
  showOutfitCarouselAddedCTA: PropTypes.bool,
  outfitCarouselState: PropTypes.bool,
  isOutfitCarousel: PropTypes.bool,
  isEnabledOutfitAddedCTA: PropTypes.bool,
  showAddtoBagDrawer: PropTypes.bool,
  revertCarouselAtbButton: PropTypes.func.isRequired,
  generalProductId: PropTypes.string,
  productDetailsToPath: PropTypes.string,
  closeModal: PropTypes.func.isRequired,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  TCPStyleType: PropTypes.string.isRequired,
  setSelectedMultipack: PropTypes.func,
  isStyleWith: PropTypes.bool,
  pdpLabels: PropTypes.instanceOf(Object).isRequired,
  getDetails: PropTypes.shape({}).isRequired,
  onQuickViewOpenClick: PropTypes.func.isRequired,
  fromPDP: PropTypes.bool.isRequired,
  selectedMultipack: PropTypes.string.isRequired,
  TCPStyleQTY: PropTypes.number.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  isPhysicalMpackEnabled: PropTypes.bool,
  imageSwatchesToDisplay: PropTypes.shape([]).isRequired,
  isImageSwatchDisabled: PropTypes.bool,
  isNewQVEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  isABTestLoaded: PropTypes.bool,
  setStickyFooterState: PropTypes.func.isRequired,
  checkForOOSForVariant: PropTypes.bool.isRequired,
  isRecommendationAvailable: PropTypes.bool,
  closePickupModal: PropTypes.func.isRequired,
  otherAvailableSizeLabel: PropTypes.string,
  selectColorToHover: PropTypes.func,
  selectColorToHoverOut: PropTypes.func,
  TCPMultipackProductMapping: PropTypes.shape([]).isRequired,
  singlePageLoad: PropTypes.bool.isRequired,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  resetProductDetailsDynamicData: PropTypes.shape([]).isRequired,
  setInitialQuickViewLoad: PropTypes.bool.isRequired,
  getQickViewSingleLoad: PropTypes.func.isRequired,
  addToBagErrorId: PropTypes.string,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  newColorList: PropTypes.shape([]),
  initialFormValues: PropTypes.shape({}).isRequired,
  initialValues: PropTypes.shape({}).isRequired,
  formValues: PropTypes.shape({}),
  onPickUpOpenClick: PropTypes.func,
  hideMultipackPills: PropTypes.bool,
  disableSwappingMpackPills: PropTypes.bool,
  productInfoFromBag: PropTypes.shape({}),
  availableTCPmapNewStyleId: PropTypes.shape([]),
  isAfterPayDisplay: PropTypes.bool,
  pdpUrlFromBag: PropTypes.string,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  goToPDPPage: PropTypes.func,
  /** User's preferred store information */
  userDefaultStore: PropTypes.shape({
    basicInfo: PropTypes.shape({
      /** store id identifier */
      id: PropTypes.string.isRequired,
      /** store Name */
      storeName: PropTypes.string.isRequired,
    }).isRequired,
  }),
  showPickupInfo: PropTypes.bool,
  labels: PropTypes.shape({
    lbl_Product_pickup_BOPIS_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: PropTypes.string,
    lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOSS_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOSS_ONLY_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_FIND_STORE: PropTypes.string,
    lbl_Product_pickup_FREE_SHIPPING: PropTypes.string,
    lbl_Product_pickup_NO_MIN_PURCHASE: PropTypes.string,
    lbl_Product_pickup_PICKUP_IN_STORE: PropTypes.string,
    lbl_Product_pickup_PRODUCT_BOPIS: PropTypes.string,
    lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: PropTypes.string,
    lbl_Product_pickup_CHANGE_STORE: PropTypes.string,
  }),
  onPickupClickAddon: PropTypes.func,
  socialProofMessage: PropTypes.string,
  isFamilyOutfitEnabled: PropTypes.bool,
  pdpProductDetails: PropTypes.shape({}),
  onAddItemToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  wishList: PropTypes.func,
  resetCountUpdate: PropTypes.func,
  countUpdated: PropTypes.bool,
  isPerUnitPriceEnabled: PropTypes.bool,
  isBrierleyPromoEnabled: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  productDetailsPDP: PropTypes.shape({}),
  productInfo: PropTypes.shape({}),
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  hidePriceDisplay: PropTypes.bool,
  getGiftCardValue: PropTypes.string,
  isShowBrandNameEnabled: PropTypes.bool,
  openModal: PropTypes.func,
  setIsBopisSelected: PropTypes.func,
  breadCrumbs: PropTypes.string,
  trackClickDispatch: PropTypes.func,
  addToPickupError: PropTypes.string,
  setAddToPickupError: PropTypes.func,
  isOpenAddedToBag: PropTypes.bool,
  handleRatingClick: PropTypes.func,
  isBopisFilterOn: PropTypes.bool.isRequired,
};

ProductAddToBag.defaultProps = {
  productDetailsToPath: '',
  generalProductId: '',
  isEnabledOutfitAddedCTA: false,
  isOutfitCarousel: false,
  outfitCarouselState: false,
  showAddtoBagDrawer: false,
  showOutfitCarouselAddedCTA: false,
  isPDP: false,
  hasLoyaltyAbTestFlag: false,
  setSelectedMultipack: () => {},
  isStyleWith: false,
  isPhysicalMpackEnabled: true,
  isImageSwatchDisabled: false,
  isNewQVEnabled: false,
  isNewPDPEnabled: false,
  isABTestLoaded: false,
  isRecommendationAvailable: false,
  otherAvailableSizeLabel: '',
  selectColorToHover: () => {},
  selectColorToHoverOut: () => {},
  addToBagErrorId: '',
  newColorList: [],
  formValues: {},
  onPickUpOpenClick: () => {},
  hideMultipackPills: false,
  disableSwappingMpackPills: false,
  productInfoFromBag: {},
  availableTCPmapNewStyleId: [],
  isAfterPayDisplay: false,
  pdpUrlFromBag: '',
  goToPDPPage: () => {},
  userDefaultStore: {
    basicInfo: {
      id: '',
      storeName: '',
    },
  },
  showPickupInfo: false,
  labels: {
    lbl_Product_pickup_BOPIS_AVAILABLE: 'Pick up TODAY!',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: 'husky',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: 'plus',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: 'slim',
    lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: 'Item available for pickup TODAY',
    lbl_Product_pickup_BOSS_AVAILABLE: 'Or choose NO RUSH Pickup ',
    lbl_Product_pickup_BOSS_ONLY_AVAILABLE: 'Choose NO RUSH Pickup ',
    lbl_Product_pickup_FIND_STORE: 'Find In Store',
    lbl_Product_pickup_FREE_SHIPPING: 'FREE Shipping Every Day!',
    lbl_Product_pickup_NO_MIN_PURCHASE: 'No Minimum Purchase Required.',
    lbl_Product_pickup_PICKUP_IN_STORE: 'PICK UP IN STORE',
    lbl_Product_pickup_PRODUCT_BOPIS: 'Select Store',
    lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: 'Select Store',
    lbl_Product_pickup_CHANGE_STORE: '(Change Store)',
    lbl_Product_pickup_UNAVAILABLE_IN_STORES: 'UNAVAILABLE IN STORES',
  },
  onPickupClickAddon: () => {},
  socialProofMessage: '',
  isFamilyOutfitEnabled: false,
  pdpProductDetails: {},
  onAddItemToFavorites: () => {},
  isLoggedIn: false,
  wishList: () => {},
  resetCountUpdate: () => {},
  countUpdated: false,
  isPerUnitPriceEnabled: false,
  isBrierleyPromoEnabled: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  productDetailsPDP: {},
  productInfo: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  hidePriceDisplay: false,
  getGiftCardValue: '',
  isShowBrandNameEnabled: false,
  openModal: () => {},
  setIsBopisSelected: () => {},
  breadCrumbs: '',
  trackClickDispatch: () => {},
  addToPickupError: '',
  setAddToPickupError: () => {},
  isOpenAddedToBag: false,
  onCloseClick: () => {},
  handleRatingClick: () => {},
};

export { ProductAddToBag as ProductAddToBagVanilla };
