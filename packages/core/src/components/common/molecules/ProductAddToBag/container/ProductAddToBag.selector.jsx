// 9fbef606107a605d69c0edbcd8029e5d 
import { getABtestFromState } from '@tcp/core/src/utils';

const getStickyATCABTest = state => {
  const getAbtests = getABtestFromState(state.AbTest);
  return getAbtests.hideStickyATC;
};

export default getStickyATCABTest;
