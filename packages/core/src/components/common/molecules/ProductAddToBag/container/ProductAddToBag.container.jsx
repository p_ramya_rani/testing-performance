/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d

import React from 'react';
import { PropTypes } from 'prop-types';
import { change } from 'redux-form';
import { connect } from 'react-redux';
import isEqual from 'lodash/isEqual';
import { setLocalStorage, getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import {
  setPickupModal,
  openPickupModalWithValues,
  setIsSkuEditFlag,
} from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import * as PickupSelectors from '@tcp/core/src/components/common/organisms/ProductPickup/container/ProductPickup.selectors';
import {
  validateSkuDetails,
  isBOSSProductOOS,
  isProductOOS,
} from '@tcp/core/src/components/common/organisms/ProductPickup/util';
import { isPlccUser } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getLabelValue, fireEnhancedEcomm, routerPush } from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import { getLedgerSummaryData } from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import { trackForterAction, ForterActionType } from '@tcp/core/src/utils/forter.util';
import {
  setAddedToBagButton,
  addedTobagMsg,
  setOutfitCarouselAddedButton,
  clearAddToCartMultipleItemErrorState,
  setStickyFooter,
  setPDPLargeImageURL,
  addItemToCartBopis,
  AddToPickupError,
} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';

import { closeQuickViewModal } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import { getMapSliceForColor } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import {
  updateMultipackSelection,
  updateSelectedSizeStatus,
} from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.actions';
import { getProducts } from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import {
  addedToBagMsgSelector,
  loyaltyAbTestFlagSelector,
  getPDPlargeImageColorSelector,
  getAddedToPickupError,
  isOpenAddedToBag,
} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';

import {
  getABTestAddToBagDrawer,
  getIsPhysicalMpackEnabled,
  getIsImageSwatchDisabled,
  getIsABLoaded,
  getIsDynamicBadgeEnabled,
  getABTestSwapMpackPills,
  getCartsSessionStatus,
  getIsNewReDesignEnabled,
  getIsRadialInventoryEnabled,
  getIsPdpServiceEnabled,
  getABTestPdpService,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getUnbxdId,
  getLabelsOutOfStock,
  getIsBopisFilterOn,
} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.selectors';
import {
  getSelectedMultipack,
  getPlpLabels,
  getAvailableSizeLabel,
  getPDPLabels,
  getBreadCrumbs,
  getSelectedSizeDisabled,
} from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.selectors';
import { getFromPage } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import { getCurrentCurrency } from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import {
  getAlternateBrandName,
  getIsBopisFilterOn as getIsBopisFilterOnSlp,
} from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import CONSTANT from './ProductAddToBag.constants';
import { isMobileApp, isCanada } from '../../../../../utils';
import ProductAddToBag from '../views/ProductAddToBag.view';
import getStickyATCABTest from './ProductAddToBag.selector';
import {
  getIsOutfitAddedCTAEnabled,
  getOutfitCarouselState,
  getShowAddedToBagCTA,
  getOutfitCarouselAddedCTA,
  getSelectedProductList,
} from '../../../../features/browse/OutfitDetails/container/OutfitDetails.selectors';
import {
  getPageName,
  getPageSection,
  getPageSubSection,
  getUserStoreId,
} from '../../../organisms/PickupStoreModal/molecules/PickupStoreSelectionForm/container/PickupStoreSelectionForm.selectors';

const userStoreFromStorage = (userDefaultStore) => {
  let userStore = userDefaultStore;
  if (userStore) {
    setLocalStorage({ key: 'defaultStore', value: JSON.stringify(userStore) });
  } else {
    userStore = JSON.parse(getLocalStorage('defaultStore')) || '';
  }
  return userStore;
};

/**
 * This class is a container of Product Add to bag view
 *
 * @class ProductAddToBagContainer
 * @extends {React.PureComponent}
 */
class ProductAddToBagContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    const { currentProduct, selectedColorProductId, size, isSBP, sbpFit } = props;
    this.initialValuesForm = this.getInitialValues(currentProduct, selectedColorProductId);
    this.sbpInitialValues(size, isSBP, sbpFit);

    this.displayATBErrorMessage = this.displayATBErrorMessage.bind(this);
    this.state = {
      selectedColor: this.initialValuesForm && this.initialValuesForm.color,
      selectedFit: this.initialValuesForm && this.initialValuesForm.Fit,
      selectedSize: this.initialValuesForm && this.initialValuesForm.Size,
      selectedQuantity: this.initialValuesForm && this.initialValuesForm.Quantity,
      isErrorMessageDisplayed: false,
      isATBErrorMessageDisplayed: false,
      fitChanged: true,
      persistSelectedFit: '',
      isBopisSelected: false,
      keepAlive:
        this.initialColorFitsSizesMapEntry &&
        this.initialColorFitsSizesMapEntry.miscInfo &&
        this.initialColorFitsSizesMapEntry.miscInfo.keepAlive,
    };
    this.quantityListHolder = [];
    this.fitListHolder = [];
    this.sizeListHolder = [];
    this.initialValuesFormHolder = {};
    this.selectedColorHolder = {};
  }

  /* eslint-disable-next-line */
  UNSAFE_componentWillReceiveProps(nextProps) {
    const {
      currentProduct,
      selectedColorProductId,
      productInfoFromBag,
      errorOnHandleSubmit,
      size,
      isSBP,
      sbpFit,
      selectedSizeFromOutfitProduct,
    } = nextProps;
    const {
      currentProduct: prevCurrentProduct,
      selectedColorProductId: prevSelectedColorProductId,
      productInfoFromBag: prevProductInfoFromBag,
      errorOnHandleSubmit: prevErrorOnHandleSubmit,
    } = this.props;

    if (
      (currentProduct && currentProduct !== prevCurrentProduct) ||
      selectedColorProductId !== prevSelectedColorProductId
    ) {
      // update selected color once map is received from api
      this.setState(
        this.getStateValuesFromProps(currentProduct, selectedColorProductId, nextProps)
      );
    }
    if (selectedSizeFromOutfitProduct) {
      this.setState({
        selectedSize: { name: selectedSizeFromOutfitProduct },
      });
    }
    if (productInfoFromBag && productInfoFromBag !== prevProductInfoFromBag) {
      this.setState({
        selectedColor: { name: productInfoFromBag.selectedColor },
        selectedFit: { name: productInfoFromBag.selectedFit },
        selectedSize: { name: productInfoFromBag.selectedSize },
        selectedQuantity: productInfoFromBag.selectedQty,
      });
    }

    if (isSBP && size) {
      this.setState({ selectedSize: { name: size } });
      this.initialValuesForm.Size = { name: size };
    }

    if (isSBP && sbpFit) {
      this.setState({ selectedFit: { name: sbpFit } });
      this.initialValuesForm.Fit = { name: sbpFit };
    }

    if (errorOnHandleSubmit && prevErrorOnHandleSubmit !== errorOnHandleSubmit) {
      this.displayATBErrorMessage(true);
    }
  }

  updateColorInState = (updatedSelectedColor) => {
    this.setState({
      selectedColor: updatedSelectedColor,
    });
  };

  componentWillUpdate(nextProps, nextState) {
    const { isQuickView, formValues, initialFormValues } = nextProps;
    const { selectedColor } = nextState;
    const initalSelectedColor = this.getInitalValue(isQuickView, formValues, initialFormValues);
    const updatedSelectedColor = { name: !initalSelectedColor ? '' : initalSelectedColor };
    const currentColor = updatedSelectedColor && updatedSelectedColor.name;
    if (currentColor && currentColor !== (selectedColor && selectedColor.name)) {
      this.updateColorInState(updatedSelectedColor);
    }
  }

  getStateValuesFromProps = (currentProduct, selectedColorProductId, nextProps) => {
    this.initialValuesForm = this.getInitialValues(
      currentProduct,
      selectedColorProductId,
      nextProps
    );
    return {
      selectedColor: this.initialValuesForm && this.initialValuesForm.color,
      selectedFit: this.initialValuesForm && this.initialValuesForm.Fit,
      selectedSize: this.initialValuesForm && this.initialValuesForm.Size,
      selectedQuantity: this.initialValuesForm && this.initialValuesForm.Quantity,
      persistSelectedFit: this.initialValuesForm && this.initialValuesForm.Fit,
    };
  };

  sbpInitialValues = (size, isSBP, sbpFit) => {
    if (isSBP && size) {
      this.initialValuesForm.Size = { name: size };
    }
    if (isSBP && sbpFit) {
      this.initialValuesForm.Fit = { name: sbpFit };
    }
  };

  /**
   * @function getDefaultColor
   * @returns default color at 0 index in color array.
   *
   * @memberof ProductAddToBagContainer
   */
  getInitialValues = (currentProduct, selectedColorProductId, nextProps) => {
    const { colorFitsSizesMap } = currentProduct;
    return (
      colorFitsSizesMap && this.defaultSizesSet(currentProduct, selectedColorProductId, nextProps)
    );
  };

  defaultSizesSet = (currentProduct, selectedColorProductId, nextProps) => {
    if (currentProduct) {
      return this.getInitialAddToBagFormValues(currentProduct, selectedColorProductId, nextProps);
    }

    return null;
  };

  getMapSliceForColorProductId = (colorFitsSizesMap, colorProductId, selectedColorProductId) => {
    const colorIdToMatch = selectedColorProductId || colorProductId;
    const selectedProduct =
      colorFitsSizesMap &&
      colorFitsSizesMap.find(
        (entry) =>
          entry.colorProductId === colorIdToMatch || entry.colorDisplayId === colorIdToMatch
      );
    return selectedProduct || (colorFitsSizesMap.length > 0 ? colorFitsSizesMap[0] : null);
  };

  getDefaultFitForColorSlice = (colorFitsSizesMapEntry, ignoreQtyCheck = false) => {
    const { sbpFit } = this.props;
    try {
      if (sbpFit) return { fitName: sbpFit.toLowerCase() };
    } catch (error) {
      logger.error(error);
    }
    return (
      colorFitsSizesMapEntry.fits.find(
        (fit) => !ignoreQtyCheck && fit.isDefault && fit.maxAvailable > 0
      ) ||
      colorFitsSizesMapEntry.fits.find((fit) => !ignoreQtyCheck && fit.maxAvailable > 0) ||
      colorFitsSizesMapEntry.fits[0]
    );
  };

  // eslint-disable-next-line complexity
  getDefaultSizeForProduct = (colorFitsSizesMap, initialFormValues) => {
    const { fromPDP, sbpSize } = this.props;

    if (sbpSize) return sbpSize;

    const firstSizeName = colorFitsSizesMap[0]
      ? colorFitsSizesMap[0].fits[0].sizes[0].sizeName
      : '';

    if (initialFormValues && !fromPDP) {
      return initialFormValues.Size;
    }

    // eslint-disable-next-line no-restricted-syntax
    for (const colorEnrtry of colorFitsSizesMap) {
      if (
        colorEnrtry.fits.length > 1 ||
        colorEnrtry.fits[0].sizes.length > 1 ||
        colorEnrtry.fits[0].sizes[0].sizeName !== firstSizeName
      ) {
        return (initialFormValues && initialFormValues.Size) || '';
      }
    }
    return firstSizeName;
  };

  getColor = (colorFitsSizesMapEntry) => {
    return (
      colorFitsSizesMapEntry && colorFitsSizesMapEntry.color && colorFitsSizesMapEntry.color.name
    );
  };

  initialAddToBagFormValuesSBP = (
    colorFitsSizesMapEntry,
    sbpFormValues,
    initialFormValues,
    currentProduct,
    giftCardDefaultSize
  ) => {
    return {
      color: {
        name: this.getColor(colorFitsSizesMapEntry),
      },
      Fit:
        colorFitsSizesMapEntry && colorFitsSizesMapEntry.hasFits
          ? {
              name: !initialFormValues
                ? this.getDefaultFitForColorSlice(colorFitsSizesMapEntry).fitName
                : initialFormValues.Fit,
            }
          : null,
      Size: {
        name: currentProduct.isGiftCard
          ? giftCardDefaultSize // on gift card we need something selected, otherwise no price would show up
          : this.getDefaultSizeForProduct(currentProduct.colorFitsSizesMap, initialFormValues),
      },
      Quantity: !sbpFormValues ? 1 : sbpFormValues.quantity,
    };
  };

  getGiftCardDefaultSize = (initialFormValues, currentProduct) => {
    return initialFormValues && initialFormValues.Size
      ? initialFormValues.Size
      : currentProduct.colorFitsSizesMap &&
          currentProduct.colorFitsSizesMap[0] &&
          currentProduct.colorFitsSizesMap[0].fits[0].sizes[0].sizeName;
  };

  getColorFitsSizesMapEntry = (currentProduct, selectedColorProductId) => {
    return currentProduct
      ? this.getMapSliceForColorProductId(
          currentProduct.colorFitsSizesMap,
          currentProduct.generalProductId,
          selectedColorProductId
        )
      : {};
  };

  // eslint-disable-next-line complexity
  getInitialAddToBagFormValues = (currentProduct, selectedColorProductId, nextProps) => {
    const colorFitsSizesMapEntry = this.getColorFitsSizesMapEntry(
      currentProduct,
      selectedColorProductId
    );
    this.initialColorFitsSizesMapEntry = colorFitsSizesMapEntry;
    let { initialFormValues } = nextProps && nextProps.renderReceiveProps ? nextProps : this.props;

    const { fromBagPage, isFavoriteEdit, isSBPEnabled, sbpFormValues } = this.props;

    if (fromBagPage || isFavoriteEdit) {
      const { productInfoFromBag } = this.props;
      initialFormValues = {
        color: productInfoFromBag.selectedColor,
        Size: productInfoFromBag.selectedSize,
        Fit: productInfoFromBag.selectedFit,
        Quantity: productInfoFromBag.selectedQty,
      };
    }

    const giftCardDefaultSize = this.getGiftCardDefaultSize(initialFormValues, currentProduct);

    if (isSBPEnabled && sbpFormValues) {
      this.initialAddToBagFormValuesSBP(
        colorFitsSizesMapEntry,
        sbpFormValues,
        initialFormValues,
        currentProduct,
        giftCardDefaultSize
      );
    }
    const initialAddToBagFormValuesTemp = {
      color: {
        name: this.getColor(colorFitsSizesMapEntry),
      },
      Fit:
        colorFitsSizesMapEntry && colorFitsSizesMapEntry.hasFits
          ? {
              name: !initialFormValues
                ? this.getDefaultFitForColorSlice(colorFitsSizesMapEntry).fitName
                : initialFormValues.Fit,
            }
          : null,
      Size: {
        name: currentProduct.isGiftCard
          ? giftCardDefaultSize // on gift card we need something selected, otherwise no price would show up
          : this.getDefaultSizeForProduct(currentProduct.colorFitsSizesMap, initialFormValues),
      },
      Quantity: !initialFormValues ? 1 : initialFormValues.Quantity,
    };
    if (!isEqual(initialAddToBagFormValuesTemp, this.initialValuesFormHolder)) {
      this.initialValuesFormHolder = initialAddToBagFormValuesTemp;
    }
    return this.initialValuesFormHolder;
  };

  triggerGAanalytics = () => {
    const { trackAnalyticsClick } = this.props;
    if (!isMobileApp()) {
      const obj = {
        event: 'event101',
        eventName: 'styleWithInteraction',
      };
      fireEnhancedEcomm({
        isCustomEvent: true,
        event: obj,
      });
    }
    if (isMobileApp()) {
      trackAnalyticsClick(
        { name: 'stylewith_interaction', module: 'browse' },
        { customEvents: ['event101'] }
      );
    }
  };

  updateSelectedSize = () => {
    const {
      currentProduct: { colorFitsSizesMap },
      isNewReDesignSizeFit,
      sizeChange,
    } = this.props;
    const sizeList = this.getSizeList(colorFitsSizesMap);
    if (sizeList.length === 1) {
      this.setState({
        selectedSize: {
          name: sizeList[0].displayName,
        },
      });
    } else if (isNewReDesignSizeFit) {
      this.setState({ selectedSize: { name: '' } });
      const form = this.getFormNameProductAddToBag();
      sizeChange({ form, size: '' });
    }
  };

  getFormNameProductAddToBag = () => {
    const { currentProduct } = this.props;
    if (currentProduct) return `ProductAddToBag-${currentProduct.generalProductId}`;
    return '';
  };

  fitChange = (e) => {
    const { persistSelectedFit } = this.state;
    const { onFitChange, isStyleWith, sizeChange, isNewReDesignSizeFit, isFromChangeStore } =
      this.props;
    const fitVal = isStyleWith && !isMobileApp() ? e.target.value : e;
    if (persistSelectedFit !== e) {
      this.setState(
        {
          selectedFit: {
            name: fitVal,
          },
          fitChanged: true,
          isErrorMessageDisplayed: false,
        },
        this.updateSelectedSize
      );
    } else {
      this.setState(
        {
          selectedFit: {
            name: fitVal,
          },
          fitChanged: false,
          isErrorMessageDisplayed: false,
        },
        this.updateSelectedSize
      );
    }

    if (onFitChange) {
      if (isFromChangeStore && isMobileApp()) {
        this.onUpdateFit(e);
      }
      onFitChange(fitVal);
      if (isNewReDesignSizeFit) {
        const form = this.getFormNameProductAddToBag();
        sizeChange({ form, size: '' });
      }
    }
    this.setState({
      isATBErrorMessageDisplayed: false,
    });
    if (isStyleWith) {
      this.triggerGAanalytics();
    }
  };

  colorChange = (e, colorIndex) => {
    if (isMobileApp()) {
      trackForterAction(ForterActionType.TAP, 'ITEM_COLOR_CHANGE');
    }
    const { selectedSize, selectedFit, selectedQuantity, isBopisSelected } = this.state;
    const {
      onChangeColor,
      currentProduct: { colorFitsSizesMap },
      isNewReDesignSizeFit,
      currentProduct,
      isFromChangeStore,
    } = this.props;
    const selectedColor = this.getSelectedColorData(colorFitsSizesMap, e);
    const selectedColorArray = selectedColor && selectedColor[0];
    this.setState({
      selectedColor: { name: e },
      selectedSize,
      isErrorMessageDisplayed: false,
      fitChanged: selectedSize.name === '',
      keepAlive: selectedColorArray && selectedColorArray.miscInfo.keepAlive,
    });
    // props for any custom action to call
    if (onChangeColor) {
      if (isMobileApp() && isFromChangeStore) {
        const generalProductId = currentProduct && currentProduct.generalProductId;
        this.onUpdateColor(e, generalProductId);
      }
      onChangeColor(
        e,
        selectedSize && selectedSize.name,
        selectedFit && selectedFit.name,
        selectedQuantity,
        colorIndex
      );
      if (isNewReDesignSizeFit && !isBopisSelected) {
        this.getSizeOptions(selectedColorArray, selectedFit, true);
      }
    }
  };

  PDPlargeImageChangeOnHover = (e) => {
    const { setPDPLargeImage } = this.props;
    setPDPLargeImage(e?.target?.offsetParent?.innerText);
  };

  PDPlargeImageResetOnHoverOut = () => {
    const { setPDPLargeImage } = this.props;
    setPDPLargeImage('');
  };

  checkHasFits = (selectedColorElement) => {
    return (
      selectedColorElement &&
      selectedColorElement.length > 0 &&
      selectedColorElement[0].hasFits &&
      selectedColorElement[0].fits &&
      selectedColorElement[0].fits[0].fitName
    );
  };

  /**
   * @function getSizeList
   * @returns size list for selected fit
   *
   * @memberof ProductAddToBagContainer
   */
  getSizeList = (colorFitsSizesMap, updatedSelectedColor) => {
    const { selectedColor, selectedFit } = this.state;
    const selectedProductAvailable =
      colorFitsSizesMap &&
      colorFitsSizesMap.find(
        (entry) => entry.color.name === (updatedSelectedColor && updatedSelectedColor.name)
      );
    const newSelectedProductName = selectedProductAvailable ? updatedSelectedColor : selectedColor;

    const selectedColorElement = this.getSelectedColorData(
      colorFitsSizesMap,
      newSelectedProductName
    );
    const hasFits = this.checkHasFits(selectedColorElement);

    const sizeListTemp =
      selectedColorElement &&
      selectedColorElement.length > 0 &&
      (hasFits
        ? this.getSizeOptions(selectedColorElement[0], selectedFit)
        : this.getSizeOptions(selectedColorElement[0]));

    if (!isEqual(sizeListTemp, this.sizeListHolder)) {
      this.sizeListHolder = sizeListTemp;
    }

    return this.sizeListHolder;
  };

  getSizeOptions = (colorItem, selectedFit, isColorChange = false) => {
    const { isNewReDesignSizeFit } = this.props;
    const { isBopisSelected } = this.state;
    const { fits } = colorItem;
    let sizeOptions = [];
    if (colorItem && fits && fits.length) {
      fits.forEach((fit) => {
        if (selectedFit) {
          if (fit.fitName === selectedFit.name) {
            sizeOptions = this.getFitSizeMap(fit);
          }
        } else {
          sizeOptions = this.getFitSizeMap(fit);
        }
      });
    }

    if (!isBopisSelected) this.getErrorCheck(sizeOptions, selectedFit);
    sizeOptions =
      colorItem.hasFits && sizeOptions && sizeOptions.length === 0
        ? this.getSizeForSecondOption(fits)
        : sizeOptions;

    if (isNewReDesignSizeFit && !isBopisSelected) this.deselectSize(sizeOptions, isColorChange);

    return sizeOptions;
  };

  deselectSize = (sizeOptions, isColorChange = false) => {
    const { formValues, onChangeSize, isShowDisabledSize } = this.props;
    if (formValues && formValues.size && typeof formValues.size === 'string') {
      const selSize = sizeOptions.find((size) => size.displayName === formValues.size);
      if (
        (selSize?.disabled || (isColorChange && !selSize)) &&
        onChangeSize &&
        !isShowDisabledSize
      ) {
        onChangeSize(null, '');
      }
    }
  };

  setIsBopisSelected = (isBopisSelected) => {
    this.setState({ isBopisSelected });
  };

  getSizeForSecondOption = (fits) => {
    let sizeOptions = [];
    fits.forEach((fit) => {
      sizeOptions = this.getFitSizeMap(fit);
    });
    this.setState({
      selectedFit: {
        name: fits[0].fitName,
      },
    });
    return sizeOptions;
  };

  getMultipackObj = () => {
    const { getAllNewMultiPack } = this.props;
    if (getAllNewMultiPack && getAllNewMultiPack.length) {
      return getAllNewMultiPack.reduce((allPack, multipack) => {
        if (!multipack || !multipack.variants) return allPack;
        const newMultipack = allPack;
        if (!newMultipack[multipack.prodpartno]) {
          const updatedVariants = multipack.variants.reduce((allVariants, variant) => {
            return variant && !allVariants[variant.v_tcpsize]
              ? { ...allVariants, [variant.v_tcpsize]: { ...variant } }
              : allVariants;
          }, {});
          newMultipack[multipack.prodpartno] = { ...multipack, variants: updatedVariants };
        }
        return newMultipack;
      }, {});
    }
    return getAllNewMultiPack;
  };

  getSelectedProd = (getMultiPackAllColor) => {
    const { selectedColor } = this.state;
    return Array.isArray(getMultiPackAllColor)
      ? getMultiPackAllColor.find(
          (product) =>
            product.name === selectedColor.name ||
            `${product.name}-${product.prodpartno}` === selectedColor.name
        )
      : '';
  };

  getIsPDPMicroServiceEnabled = () => {
    const { isPDPServiceEnabled, isABTestPdpService } = this.props;
    return isPDPServiceEnabled && isABTestPdpService;
  };

  getCorrectSizeNameWithPrefix = (variantsObject, sizeId) => {
    const variantKeys = Object.keys(variantsObject);
    const isPDPMicroServiceEnabled = this.getIsPDPMicroServiceEnabled();
    const correctSizeName =
      variantKeys &&
      variantKeys.filter((item) => {
        if (isPDPMicroServiceEnabled) {
          return item === sizeId;
        }
        return item && item.split('_')[1] === sizeId;
      });
    return correctSizeName && correctSizeName[0];
  };

  getIsSizeUnavailable = (multipackObj, currentMultipack, size, thresholdCount) => {
    const { selectedQuantity } = this.state;
    let maxAvailable = 0;

    const isSizeNotAvailable = multipackObj
      ? currentMultipack.some((multipack) => {
          const [partNo, quantity] = multipack.split('#');
          const correctSizeName = this.getCorrectSizeNameWithPrefix(
            multipackObj[partNo] && multipackObj[partNo].variants,
            size.id
          );
          const availableQty =
            multipackObj[partNo] &&
            multipackObj[partNo].variants &&
            multipackObj[partNo].variants[correctSizeName]
              ? multipackObj[partNo].variants[correctSizeName].v_qty
              : 0;
          if (maxAvailable === 0 || availableQty < maxAvailable) {
            maxAvailable = availableQty;
          }
          return (
            availableQty / parseInt(quantity, 10) - parseInt(selectedQuantity, 10) < thresholdCount
          );
        })
      : true;

    return {
      isSizeNotAvailable,
      maxAvailable,
    };
  };

  getIsVirtualStyle = (selectedProd) =>
    isCanada()
      ? selectedProd.styleTypeCA === '0002' || selectedProd.styleTypeCA === '0003'
      : selectedProd.styleTypeUS === '0002' || selectedProd.styleTypeUS === '0003';

  getSizeId = (size) => {
    return size && size.id;
  };

  getSelectedProductDetails = (selectedProd, isPDPMicroServiceEnabled, size) => {
    return (
      selectedProd &&
      selectedProd.variants &&
      selectedProd.variants.filter((item) => {
        const correctSizeName = isPDPMicroServiceEnabled
          ? item && item.v_tcpsize
          : item && item.v_tcpsize && item.v_tcpsize.split('_')[1];
        return correctSizeName === this.getSizeId(size);
      })
    );
  };

  isSizeAvailable = (size) => {
    try {
      const { multiPackThreshold, currentProduct: { getMultiPackAllColor } = {} } = this.props;
      const isPDPMicroServiceEnabled = this.getIsPDPMicroServiceEnabled();

      const thresholdCount = multiPackThreshold ? parseInt(multiPackThreshold, 10) : false;
      const selectedProd = this.getSelectedProd(getMultiPackAllColor);
      const isVirtualStyle = selectedProd && this.getIsVirtualStyle(selectedProd);
      if (isVirtualStyle) {
        const selectedProductDetails = this.getSelectedProductDetails(
          selectedProd,
          isPDPMicroServiceEnabled,
          size
        );
        const availableQuantity =
          selectedProductDetails && selectedProductDetails[0] && selectedProductDetails[0].v_qty;
        if (availableQuantity !== 8888 && availableQuantity > 0) {
          return availableQuantity;
        }
      }
      const currentMultipack =
        selectedProd && selectedProd.TCPMultiPackReferenceUSStore
          ? selectedProd.TCPMultiPackReferenceUSStore.split(',')
          : [];
      if (thresholdCount) {
        const multipackObj = this.getMultipackObj();
        const { isSizeNotAvailable, maxAvailable } = this.getIsSizeUnavailable(
          multipackObj,
          currentMultipack,
          size,
          thresholdCount
        );
        return !isSizeNotAvailable ? maxAvailable : 0;
      }
      return false;
    } catch (err) {
      return false;
    }
  };

  oldFitMap = (fit) => {
    return fit.sizes.map((size) => ({
      displayName: size.sizeName,
      id: size.sizeName,
      maxAvailable: size.maxAvailable,
      disabled: size.maxAvailable <= 0,
      multipackVariant: size.isMultiPack,
    }));
  };

  newFitMap = (oldFit) => {
    return oldFit.map((size) => {
      if (size.multipackVariant) {
        const { displayName, disabled } = size;
        const maxAvailableValue = this.isSizeAvailable(size);
        return {
          displayName,
          id: displayName,
          maxAvailable: maxAvailableValue,
          disabled,
        };
      }
      return null;
    });
  };

  getFitSizeMap = (fit) => {
    const { multipackProduct } = this.props;
    const checkMultipack = !!multipackProduct && multipackProduct.length > 0;
    const oldFit = this.oldFitMap(fit);
    const newFit = checkMultipack && this.newFitMap(oldFit);
    const data =
      checkMultipack &&
      newFit.filter((element) => {
        return element !== null;
      });
    const getData = data.length > 0 ? data : oldFit;
    return getData.map((newsize) => ({
      displayName: newsize.displayName,
      id: newsize.displayName,
      maxAvailable: newsize.maxAvailable,
      disabled: newsize.maxAvailable <= 0,
    }));
  };

  getErrorCheck = (sizeOptions = [], selectedFit) => {
    const { selectedSize } = this.state;

    const option = this.getOptionArray(sizeOptions);

    const fitOption = this.getFitOptionsArray(selectedFit, selectedSize, sizeOptions);

    this.setErrorState(fitOption);

    if (
      option &&
      option.length > 0 &&
      selectedSize &&
      option[0].displayName === selectedSize.name
    ) {
      this.setState({
        fitChanged: true,
      });
    } else if (selectedSize.name && !selectedFit) {
      this.setState({
        fitChanged: false,
      });
      const selectedSizeObj = sizeOptions.filter(
        (sizeItem) => (sizeItem && sizeItem.displayName) === (selectedSize && selectedSize.name)
      );
      if (!(selectedSizeObj && selectedSizeObj[0] && selectedSizeObj[0].disabled)) {
        this.displayErrorMessage(false);
      }
    }
  };

  getOptionArray = (sizeOptions) =>
    sizeOptions.length > 0 && sizeOptions.filter((item) => item.maxAvailable <= 0);

  getFitOptionsArray = (selectedFit, selectedSize, sizeOptions) =>
    selectedFit &&
    selectedSize.name &&
    sizeOptions.length > 0 &&
    sizeOptions.filter((item) => item.displayName === selectedSize.name);

  setErrorState = (fitOption) => {
    if (fitOption && fitOption.length === 0) {
      this.setState({
        fitChanged: true,
      });
    } else if (fitOption && fitOption.length === 1) {
      this.setState({
        fitChanged: false,
      });
    }
  };

  displayErrorMessage = (displayError) => {
    this.setState({
      isErrorMessageDisplayed: displayError,
    });
  };

  displayATBErrorMessage = (displayError) => {
    this.setState({
      isATBErrorMessageDisplayed: displayError,
    });
  };

  /**
   * @function getQuantityList
   * @returns quantity list with labels and values to render dropdown
   *
   * @memberof ProductAddToBagContainer
   */
  getQuantityList = () => {
    const quantityArray = new Array(15).fill(1);
    const quantityArrayTemp = quantityArray.map((val, index) => ({
      displayName: index + 1,
      id: index + 1,
    }));
    if (!isEqual(this.quantityListHolder, quantityArrayTemp)) {
      this.quantityListHolder = quantityArrayTemp;
    }
    return this.quantityListHolder;
  };

  getSelectedColorData = (colorFitsSizesMap, selectedColor = {}) => {
    return (
      colorFitsSizesMap &&
      colorFitsSizesMap.filter((colorItem) => {
        const {
          color: { name },
        } = colorItem;
        return (selectedColor.name || selectedColor) === name;
      })
    );
  };

  getFitOptions = (colorFitsSizesMap, selectedColor, updatedSelectedColor) => {
    const selectedProductAvailable =
      colorFitsSizesMap &&
      colorFitsSizesMap.find(
        (entry) => entry.color.name === (updatedSelectedColor && updatedSelectedColor.name)
      );
    const newSelectedProductName = selectedProductAvailable ? updatedSelectedColor : selectedColor;
    const colorItem = this.getSelectedColorData(colorFitsSizesMap, newSelectedProductName);
    const { fits, hasFits } = (colorItem && colorItem[0]) || {};
    const fitsTemp =
      (fits &&
        fits[0].fitName &&
        hasFits &&
        fits.map((fit) => ({
          displayName: fit.fitName,
          id: fit.fitName,
        }))) ||
      [];
    if (!isEqual(fitsTemp, this.fitListHolder)) {
      this.fitListHolder = fitsTemp;
    }
    return this.fitListHolder;
  };

  getSelectedSize = (sizeVal, prevSelectedSize, isWebStyleWith, isOutfitPage) => {
    return sizeVal === prevSelectedSize.name && !isWebStyleWith && isOutfitPage ? '' : sizeVal;
  };

  onUpdateSize = (size, productId) => {
    const { updateSize } = this.props;
    const form = this.getFormNameProductAddToBag(productId);
    updateSize({ form, size });
  };

  onUpdateColor = (color, newProductId) => {
    const { updateColor } = this.props;
    const form = this.getFormNameProductAddToBag(newProductId);
    updateColor({ form, color });
  };

  onUpdateFit = (fit) => {
    const { updateFit } = this.props;
    const form = this.getFormNameProductAddToBag();
    updateFit({ form, fit });
  };

  sizeChange = (e, disabled) => {
    if (isMobileApp()) {
      trackForterAction(ForterActionType.TAP, 'ITEM_SIZE_CHANGE');
    }
    const {
      selectedColor,
      selectedFit,
      selectedQuantity,
      selectedSize: prevSelectedSize,
    } = this.state;
    const {
      onChangeSize,
      getSKUValidated,
      isStyleWith,
      isOutfitPage,
      currentProduct,
      isFromChangeStore,
      updateSizeStatus,
    } = this.props;
    const isWebStyleWith = isStyleWith && !isMobileApp();
    const sizeVal = isWebStyleWith ? e.target.value : e;

    this.setState({
      persistSelectedFit: selectedFit,
      selectedSize: {
        name: this.getSelectedSize(sizeVal, prevSelectedSize, isWebStyleWith, isOutfitPage),
      },
      fitChanged: false,
      isATBErrorMessageDisplayed: false,
    });

    if (e !== 'Select') {
      this.displayErrorMessage(false);
      if (getSKUValidated) {
        getSKUValidated(false);
      }
    }
    updateSizeStatus(disabled);
    if (onChangeSize) {
      if (isMobileApp() && isFromChangeStore) {
        const generalProductId = currentProduct && currentProduct.generalProductId;
        this.onUpdateSize(sizeVal, generalProductId);
      }
      onChangeSize(
        selectedColor && selectedColor.name,
        sizeVal,
        selectedFit && selectedFit.name,
        selectedQuantity
      );
    }
    if (isStyleWith) {
      this.triggerGAanalytics();
    }
  };

  quantityChange = (selectedQuantity, form) => {
    const { quantityChange, updateQuantityChange } = this.props;
    quantityChange({ form, selectedQuantity });
    this.setState({ selectedQuantity, isATBErrorMessageDisplayed: false });
    if (updateQuantityChange) {
      updateQuantityChange(selectedQuantity);
    }
  };

  setPreSelectedValuesForProduct = (productInfoFromBag) => {
    const { selectedFit, selectedQty, selectedSize, selectedColor } = productInfoFromBag;
    this.initialValuesForm.Fit = selectedFit;
    this.initialValuesForm.Quantity = selectedQty || 1;
    this.initialValuesForm.Size = selectedSize;
    this.initialValuesForm.color = selectedColor;
  };

  quickViewPickup = () => {
    const { isPickup, isMultiItemQVModal, isBundleProduct, isFavoriteEdit } = this.props;
    const isQuickViewPickup = !isPickup && !isBundleProduct && !isMultiItemQVModal;
    if (isFavoriteEdit) {
      return isFavoriteEdit && !isFavoriteEdit;
    }
    return isQuickViewPickup;
  };

  checkOOS = (multipackProduct) => {
    if (multipackProduct && multipackProduct.length > 0) {
      return multipackProduct.map((finalData) => {
        return finalData.v_qty === 0
          ? CONSTANT.PRODUCT_ADD_TO_BAG_UNAVAILABLE
          : CONSTANT.PRODUCT_ADD_TO_BAG_AVAILABLE;
      });
    }
    return null;
  };

  regularProduct = (availableFlag, multipackProduct) => {
    return availableFlag === -1 && multipackProduct === null;
  };

  getCurrentProductId = (updatedColorEntry, selectedColorProductId) => {
    return updatedColorEntry ? updatedColorEntry.colorDisplayId : selectedColorProductId;
  };

  checkForOOSForAllVariants = () => {
    const {
      currentProduct: { categoryIdList },
      multipackProduct,
    } = this.props;
    let availableFlagMultipack;
    if (categoryIdList && categoryIdList.length > 0) {
      const isOOSForAllVariantsMultipack = this.checkOOS(multipackProduct);
      if (multipackProduct && multipackProduct.length > 0) {
        availableFlagMultipack = isOOSForAllVariantsMultipack.findIndex(
          (variantQuantity) => variantQuantity === CONSTANT.PRODUCT_ADD_TO_BAG_AVAILABLE
        );
      }
      const isOOSForAllVariantsRegular = categoryIdList.map((categoryItem) => {
        return categoryItem.v_qty === 0
          ? CONSTANT.PRODUCT_ADD_TO_BAG_UNAVAILABLE
          : CONSTANT.PRODUCT_ADD_TO_BAG_AVAILABLE;
      });

      const availableFlag = isOOSForAllVariantsRegular.findIndex(
        (variantQuantity) => variantQuantity === CONSTANT.PRODUCT_ADD_TO_BAG_AVAILABLE
      );
      const regularProduct = this.regularProduct(availableFlag, multipackProduct);
      if (regularProduct || (availableFlag === -1 && availableFlagMultipack === -1)) {
        return true;
      }
    }
    return false;
  };

  goToPDPPage = (e, pdpToPath, currentColorPdpUrl) => {
    e.preventDefault();
    const { onCloseClick } = this.props;
    routerPush(pdpToPath, currentColorPdpUrl);
    onCloseClick();
  };

  getInitalValue = (
    isQuickView,
    formValues,
    initialFormValues,
    selectedColor,
    isMultiItemQVModal
  ) => {
    let initalSelectedColor;
    if (isQuickView && !isMultiItemQVModal) {
      initalSelectedColor =
        formValues && formValues[0] && formValues[0].color ? formValues[0].color : '';
    } else {
      initalSelectedColor =
        formValues && formValues.color
          ? formValues.color
          : initialFormValues && initialFormValues.color;
    }

    return initalSelectedColor || (selectedColor && selectedColor.name);
  };

  getUpdatedColor = (initalSelectedColor, selectedColor) => {
    return { name: initalSelectedColor || (selectedColor && selectedColor.name) || '' };
  };

  getAddType = (isOutfitPage, isPDP, isBundleProduct) => {
    let pageAddType = 'browse';
    if (isOutfitPage) {
      pageAddType = 'outfit';
    }
    if (isPDP) {
      pageAddType = 'product';
    }
    if (isBundleProduct) {
      pageAddType = 'productbundle';
    }
    return pageAddType;
  };

  getFromPageVal = () => {
    const { isPDP, pageName, fromPage } = this.props;
    const isPDPFlag = isPDP && pageName === 'PDP';
    if (isPDPFlag) return pageName;
    return fromPage;
  };

  /**
   * @function render
   *
   * @returns ProductAddToBag view
   * @memberof ProductAddToBagContainer
   */

  getAlternateBrand = (alternateBrand, alternateBrandFromState) => {
    return alternateBrand || alternateBrandFromState;
  };

  getSelectedColorId = (selectedColorProductId, getNewId) => {
    return selectedColorProductId || getNewId;
  };

  render() {
    const {
      currentProduct,
      currentProduct: {
        colorFitsSizesMap,
        colorFitSizeDisplayNames,
        isGiftCard,
        imagesByColor,
        relatedSwatchImages,
        TCPMultipackProductMapping,
        TCPStyleType,
        TCPStyleQTY,
        tcpMultiPackReferenceUSStore,
        multiPackUSStore,
      },
      plpLabels,
      pdpLabels,
      handleFormSubmit,
      errorOnHandleSubmit,
      selectedColorProductId,
      customFormName,
      showAddToBagCTA = true,
      showColorChips = true,
      fromBagPage,
      productInfoFromBag,
      customSubmitButtonStyle,
      colorFitsSizesMap: favColorFitsSizesMap,
      isOutfitPage,
      formRef,
      formEnabled,
      quickViewColorSwatchesCss,
      isPDP,
      isDisableZeroInventoryEntries,
      alternateSizes,
      sizeChartLinkVisibility,
      navigation,
      isPickup,
      onCloseClick,
      isBundleProduct,
      outOfStockLabels,
      isKeepAliveEnabled,
      isFavoriteEdit,
      sizeChartDetails,
      isMultiItemQVModal,
      pageNameProp,
      pageSectionProp,
      pageSubSectionProp,
      itemBrand,
      showAddedToBagCta,
      revertAtbButton,
      addedToBagLabel,
      addBtnMsg,
      hasLoyaltyAbTestFlag,
      addedTobagMsgDispatch,
      showAddtoBagDrawer,
      isOutfitCarousel,
      multipackProduct,
      currencySymbol,
      currencyAttributes,
      listPrice,
      offerPrice,
      isPlcc,
      isInternationalShipping,
      showOutfitCarouselAddedCTA,
      outfitCarouselState,
      isEnabledOutfitAddedCTA,
      closeModal,
      scrollToTarget,
      isRecommendationAvailable,
      multipackSelectionAction,
      isStyleWith,
      getDetails,
      isPhysicalMpackEnabled,
      checkForOOSForVariant,
      imageSwatchesToDisplay,
      isImageSwatchDisabled,
      setStickyFooterState,
      getRecommendationProduct,
      closePickupModal,
      initialMultipackMapping,
      setPDPLargeImage,
      largeImageNameOnHover,
      setInitialTCPStyleQty,
      resetProductDetailsDynamicData,
      setInitialQuickViewLoad,
      getQickViewSingleLoad,
      disableMultiPackTab,
      getDisableSelectedTab,
      newColorList,
      isSBP,
      childProfile,
      isSBPEnabled,
      sbpSizeClick,
      initialFormValues,
      formValues,
      isQuickView,
      isDynamicBadgeEnabled,
      onPickUpOpenClick,
      hideMultipackPills,
      disableSwappingMpackPills,
      sbpLabels,
      availableTCPmapNewStyleId,
      selectedSizeFromOutfitProduct,
      isNewReDesignProductSummary,
      userDefaultStore,
      isRadialInventoryEnabled,
      labels,
      onPickupClickAddon,
      currentColorEntry,
      isNewQVEnabled,
      isNewPDPEnabled,
      socialProofMessage,
      onMultiPackChange,
      closeQuickViewSizeModal,
      isFamilyOutfitEnabled,
      quickViewLabels,
      isABTestPdpService,
      isPDPServiceEnabled,
      getGiftCardValue,
      isShowBrandNameEnabled,
      openModal,
      alternateBrand,
      alternateBrandFromUrl,
      alternateBrandFromState,
      isFromBundlePage,
      isSocialProofingABTestEnabled,
      selectedSizeDrawerTab,
      multiErrorOnSubmit,
      clearMultipleAddToBagError,
      addToPickupError,
      setAddToPickupError,
      isOpenAddedToBagValue,
      handleRatingClick,
      isFromChangeStore,
      isShowDisabledSize,
      isSelectedSizeDisabled,
      isBopisFilterOn,
      ...otherProps
    } = this.props;

    const itemValues = {
      showDefaultSizeMsg: false,
      formValues,
    };
    this.isSkuResolved = validateSkuDetails(currentProduct, itemValues);
    // RAD-74 Replace Outbound1 Inventory check to Outbound2(Radial/Boss Inventory)
    const validateBossOOS = isRadialInventoryEnabled ? isBOSSProductOOS : isProductOOS;
    const isbossInventoryAvailable = !validateBossOOS(currentProduct.colorFitsSizesMap, itemValues);
    const showPickupInfo = !userDefaultStore || !this.isSkuResolved || isbossInventoryAvailable;
    const filteredColorFitsList = colorFitsSizesMap?.filter((item) => item.maxAvailable === 0);
    const setOOSForAllVariantsFlag = (
      colorFitsSizesMapArr,
      filteredColorFitsListArr,
      isPartialOrFullVmpack
    ) => {
      let oOSForAllVariantsFlag = false;
      if (
        (colorFitsSizesMapArr && colorFitsSizesMapArr.length) ===
          (filteredColorFitsListArr && filteredColorFitsListArr.length) &&
        !isPartialOrFullVmpack
      ) {
        oOSForAllVariantsFlag = true;
      }
      return oOSForAllVariantsFlag;
    };
    const checkForOOSForAllVariantsFlag = setOOSForAllVariantsFlag(
      colorFitsSizesMap,
      filteredColorFitsList,
      tcpMultiPackReferenceUSStore && multiPackUSStore
    );
    const {
      selectedColor,
      selectedFit,
      selectedSize,
      fitChanged,
      isErrorMessageDisplayed,
      isATBErrorMessageDisplayed,
      selectedQuantity,
      keepAlive,
    } = this.state;
    if (fromBagPage) {
      this.setPreSelectedValuesForProduct(productInfoFromBag);
    }
    const initalSelectedColor = this.getInitalValue(
      isQuickView,
      formValues,
      initialFormValues,
      selectedColor,
      isMultiItemQVModal
    );
    const productColorFitsSizesMap = colorFitsSizesMap || favColorFitsSizesMap;
    const updatedSelectedColor = { name: initalSelectedColor || '' };
    if (!isEqual(updatedSelectedColor, this.selectedColorHolder)) {
      this.selectedColorHolder = updatedSelectedColor;
    }
    const updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, initalSelectedColor);
    const getNewId = this.getCurrentProductId(updatedColorEntry, selectedColorProductId);
    const initialValues = this.getInitialValues(currentProduct, getNewId);
    const generalProductId = currentProduct && currentProduct.generalProductId;
    const primaryBrand = currentProduct && currentProduct.primaryBrand;

    const alternateBrandValue = this.getAlternateBrand(alternateBrand, alternateBrandFromState);
    return (
      <ProductAddToBag
        {...otherProps}
        clearMultipleAddToBagError={clearMultipleAddToBagError}
        isPickup={isPickup}
        colorList={productColorFitsSizesMap}
        fitList={this.getFitOptions(productColorFitsSizesMap, selectedColor, updatedSelectedColor)}
        sizeList={this.getSizeList(productColorFitsSizesMap, updatedSelectedColor)}
        selectSize={this.sizeChange}
        selectFit={this.fitChange}
        selectColor={this.colorChange}
        selectedColor={this.selectedColorHolder}
        isSkuResolved={this.isSkuResolved}
        labels={labels}
        showPickupInfo={showPickupInfo}
        selectedFit={selectedFit}
        selectedSize={selectedSize}
        quantityList={this.getQuantityList()}
        plpLabels={plpLabels}
        pdpLabels={pdpLabels}
        fitChanged={fitChanged}
        isErrorMessageDisplayed={isErrorMessageDisplayed}
        isATBErrorMessageDisplayed={isATBErrorMessageDisplayed}
        initialValues={initialValues}
        displayErrorMessage={this.displayErrorMessage}
        displayATBErrorMessage={this.displayATBErrorMessage}
        selectedQuantity={selectedQuantity}
        onQuantityChange={this.quantityChange}
        generalProductId={generalProductId}
        handleFormSubmit={handleFormSubmit}
        errorOnHandleSubmit={errorOnHandleSubmit}
        multiErrorOnSubmit={multiErrorOnSubmit}
        currentProduct={currentProduct}
        selectedColorProductId={this.getSelectedColorId(selectedColorProductId, getNewId)}
        customFormName={customFormName}
        showAddToBagCTA={showAddToBagCTA}
        showColorChips={showColorChips}
        fromBagPage={fromBagPage}
        inheritedStyles={customSubmitButtonStyle}
        colorFitSizeDisplayNames={colorFitSizeDisplayNames}
        isGiftCard={isGiftCard}
        isOutfitPage={isOutfitPage}
        isFromBundlePage={isFromBundlePage}
        ref={formRef}
        formEnabled={formEnabled}
        quickViewColorSwatchesCss={quickViewColorSwatchesCss}
        isPDP={isPDP}
        isDisableZeroInventoryEntries={isDisableZeroInventoryEntries}
        alternateSizes={alternateSizes}
        sizeChartLinkVisibility={sizeChartLinkVisibility}
        navigation={navigation}
        onCloseClick={onCloseClick}
        isBundleProduct={isBundleProduct}
        keepAlive={isKeepAliveEnabled && keepAlive}
        keepAliveFlag={keepAlive}
        outOfStockLabels={outOfStockLabels}
        isFavoriteEdit={isFavoriteEdit}
        sizeChartDetails={sizeChartDetails}
        isMultiItemQVModal={isMultiItemQVModal}
        quickViewPickup={this.quickViewPickup}
        pageNameProp={pageNameProp}
        pageSectionProp={pageSectionProp}
        pageSubSectionProp={pageSubSectionProp}
        imagesByColor={imagesByColor}
        itemBrand={itemBrand}
        relatedSwatchImages={relatedSwatchImages}
        pageAddType={this.getAddType(isOutfitPage, isPDP, isBundleProduct)}
        showAddedToBagCta={showAddedToBagCta}
        showOutfitCarouselAddedCTA={showOutfitCarouselAddedCTA}
        revertAtbButton={revertAtbButton}
        addedToBagLabel={addedToBagLabel}
        addBtnMsg={addBtnMsg}
        hasLoyaltyAbTestFlag={hasLoyaltyAbTestFlag}
        addedTobagMsgDispatch={addedTobagMsgDispatch}
        showAddtoBagDrawer={showAddtoBagDrawer}
        isOutfitCarousel={isOutfitCarousel}
        multipackProduct={multipackProduct}
        currencySymbol={currencySymbol}
        currencyAttributes={currencyAttributes}
        listPrice={listPrice}
        offerPrice={offerPrice}
        isCanada={isCanada()}
        outfitCarouselState={outfitCarouselState}
        isPlcc={isPlcc}
        isInternationalShipping={isInternationalShipping}
        isEnabledOutfitAddedCTA={isEnabledOutfitAddedCTA}
        closeModal={closeModal}
        scrollToTarget={scrollToTarget}
        checkForOOSForVariant={checkForOOSForAllVariantsFlag}
        isRecommendationAvailable={isRecommendationAvailable}
        TCPMultipackProductMapping={TCPMultipackProductMapping}
        TCPStyleType={TCPStyleType}
        TCPStyleQTY={TCPStyleQTY}
        setSelectedMultipack={multipackSelectionAction}
        isStyleWith={isStyleWith}
        getDetails={getDetails}
        isPhysicalMpackEnabled={isPhysicalMpackEnabled}
        imageSwatchesToDisplay={imageSwatchesToDisplay}
        isImageSwatchDisabled={isImageSwatchDisabled}
        setStickyFooterState={setStickyFooterState}
        closePickupModal={closePickupModal}
        initialMultipackMapping={initialMultipackMapping}
        selectColorToHover={this.PDPlargeImageChangeOnHover}
        selectColorToHoverOut={this.PDPlargeImageResetOnHoverOut}
        setInitialTCPStyleQty={setInitialTCPStyleQty}
        resetProductDetailsDynamicData={resetProductDetailsDynamicData}
        setInitialQuickViewLoad={setInitialQuickViewLoad}
        getQickViewSingleLoad={getQickViewSingleLoad}
        disableMultiPackTab={disableMultiPackTab}
        getDisableSelectedTab={getDisableSelectedTab}
        newColorList={newColorList}
        formValues={formValues}
        childProfile={childProfile}
        isSBP={isSBP}
        isNewReDesignProductSummary={isNewReDesignProductSummary}
        isSBPEnabled={isSBPEnabled}
        sbpSizeClick={sbpSizeClick}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        onPickUpOpenClick={onPickUpOpenClick}
        hideMultipackPills={hideMultipackPills}
        disableSwappingMpackPills={disableSwappingMpackPills}
        sbpLabels={sbpLabels}
        productInfoFromBag={productInfoFromBag}
        availableTCPmapNewStyleId={availableTCPmapNewStyleId}
        goToPDPPage={this.goToPDPPage}
        userDefaultStore={userDefaultStore}
        currentColorEntry={currentColorEntry}
        isNewQVEnabled={isNewQVEnabled}
        isNewPDPEnabled={isNewPDPEnabled}
        socialProofMessage={socialProofMessage}
        onMultiPackChange={onMultiPackChange}
        fromPage={this.getFromPageVal()}
        closeQuickViewSizeModal={closeQuickViewSizeModal}
        isFamilyOutfitEnabled={isFamilyOutfitEnabled}
        quickViewLabels={quickViewLabels}
        primaryBrand={primaryBrand}
        getGiftCardValue={getGiftCardValue}
        isShowBrandNameEnabled={isShowBrandNameEnabled}
        openModal={openModal}
        alternateBrandFromUrl={alternateBrandFromUrl}
        alternateBrand={alternateBrandValue}
        setIsBopisSelected={this.setIsBopisSelected}
        isFromChangeStore={isFromChangeStore}
        selectedSizeDrawerTab={selectedSizeDrawerTab}
        addToPickupError={addToPickupError}
        isOpenAddedToBag={isOpenAddedToBagValue}
        setAddToPickupError={setAddToPickupError}
        isShowDisabledSize={isShowDisabledSize}
        isSelectedSizeDisabled={isSelectedSizeDisabled}
        handleRatingClick={handleRatingClick}
        isBopisFilterOn={isBopisFilterOn}
      />
    );
  }
}

function mapStateToProps(state, ownProps) {
  const { pageData } = state;
  const { setInitialTCPStyleQty, isPDP } = ownProps;
  const userDefaultStore = PickupSelectors.getDefaultStore(state);
  const geoDefaultStore = PickupSelectors.getGeoDefaultStore(state);
  const defaultStore =
    userStoreFromStorage(userDefaultStore) || userDefaultStore || geoDefaultStore || null;
  return {
    labels: {
      ...PickupSelectors.getLabels(state),
      ...PickupSelectors.getAccessibilityLabels(state),
    },
    userDefaultStore: defaultStore,
    pageNameProp: getPageName(state),
    pageSectionProp: getPageSection(state),
    pageSubSectionProp: getPageSubSection(state),
    pageData,
    currency: getCurrentCurrency(state),
    storeId: getUserStoreId(state),
    ledgerSummaryData: getLedgerSummaryData(state),
    addedToBagLabel: getLabelValue(
      state.Labels,
      'lbl_header_addedToBag',
      'addedToBagModal',
      'global'
    ),
    unbxdId: getUnbxdId(state),
    hideStickyABTest: getStickyATCABTest(state),
    showAddtoBagDrawer: getABTestAddToBagDrawer(state),
    isEnabledOutfitAddedCTA: !isCanada() && getIsOutfitAddedCTAEnabled(state),
    outfitCarouselState: getOutfitCarouselState(state),
    addBtnMsg: addedToBagMsgSelector(state),
    hasLoyaltyAbTestFlag: loyaltyAbTestFlagSelector(state),
    showAddedToBagCta: getShowAddedToBagCTA(state),
    showOutfitCarouselAddedCTA: getOutfitCarouselAddedCTA(state),
    selectedMultipack: getSelectedMultipack(state),
    isPhysicalMpackEnabled: getIsPhysicalMpackEnabled(state),
    isImageSwatchDisabled: isPDP ? getIsImageSwatchDisabled(state) : true,
    isABTestLoaded: getIsABLoaded(state),
    getRecommendationProduct: getProducts(state, 'pdp'),
    plpLabels: getPlpLabels(state),
    pdpLabels: getPDPLabels(state),
    otherAvailableSizeLabel: getAvailableSizeLabel(state),
    largeImageNameOnHover: getPDPlargeImageColorSelector(state),
    setInitialTCPStyleQty,
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    disableSwappingMpackPills: getABTestSwapMpackPills(state),
    selectedProducts: getSelectedProductList(state),
    cartsSessionStatus: getCartsSessionStatus(state),
    isNewReDesignSizeFit: getIsNewReDesignEnabled(state),
    isRadialInventoryEnabled: getIsRadialInventoryEnabled(state),
    fromPage: getFromPage(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    isPDPServiceEnabled: getIsPdpServiceEnabled(state),
    isABTestPdpService: getABTestPdpService(state),
    alternateBrandFromState: getAlternateBrandName(state),
    isPlcc: isPlccUser(state),
    bopisItemInventory: PickupSelectors.getBopisItemInventory(state),
    breadCrumbs: getBreadCrumbs(state),
    addToPickupError: getAddedToPickupError(state),
    isOpenAddedToBagValue: isOpenAddedToBag(state),
    isSelectedSizeDisabled: getSelectedSizeDisabled(state),
    isBopisFilterOn: getIsBopisFilterOn(state) || getIsBopisFilterOnSlp(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setAnalyticsData: (payload) => {
      dispatch(setClickAnalyticsData(payload));
    },
    onPickUpOpenClick: (payload) => {
      if (payload.fromPills) {
        dispatch(setPickupModal(payload));
      } else {
        dispatch(openPickupModalWithValues(payload));
      }
    },
    closeQuickViewSizeModal: () => {
      dispatch(closeQuickViewModal({ isModalOpen: false }));
    },
    addedTobagMsgDispatch: (payload) => {
      dispatch(addedTobagMsg(payload));
    },
    outfitCarouselAddedToBagMsgDispatch: (payload) => {
      dispatch(setOutfitCarouselAddedButton(payload));
    },
    quantityChange: (payload) => {
      dispatch(change(payload.form, 'Quantity', payload.selectedQuantity));
    },
    sizeChange: (payload) => {
      dispatch(change(payload.form, 'Size', payload.size));
    },
    revertAtbButton: () => {
      const addBagTimer = setTimeout(() => {
        dispatch(setAddedToBagButton(''));
        clearTimeout(addBagTimer);
      }, 2000);
    },
    revertCarouselAtbButton: () => {
      const carouselAddTimer = setTimeout(() => {
        dispatch(setOutfitCarouselAddedButton(false));
        clearTimeout(carouselAddTimer);
      }, 2000);
    },
    multipackSelectionAction: (payload) => {
      dispatch(updateMultipackSelection(payload));
    },
    setStickyFooterState: (payload) => {
      dispatch(setStickyFooter(payload));
    },
    setPDPLargeImage: (payload) => {
      dispatch(setPDPLargeImageURL(payload));
    },
    addItemToCartInPickup: (payload) => {
      dispatch(addItemToCartBopis(payload));
    },
    setAddToPickupError: (payload) => {
      dispatch(AddToPickupError(payload));
    },
    trackClickDispatch: (payload) => dispatch(trackClick(payload)),
    trackAnalyticsClick: (payload, eventData) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 100);
    },
    clearMultipleAddToBagError: () => {
      dispatch(clearAddToCartMultipleItemErrorState());
    },
    updateSize: (payload) => {
      dispatch(change(payload.form, 'Size', payload.size));
      dispatch(setIsSkuEditFlag({ isSkuSelected: true }));
    },
    updateColor: (payload) => {
      dispatch(change(payload.form, 'color', payload.color));
    },
    updateFit: (payload) => {
      dispatch(change(payload.form, 'Fit', payload.fit));
    },
    updateSizeStatus: (payload) => {
      dispatch(updateSelectedSizeStatus(payload));
    },
  };
}

ProductAddToBagContainer.propTypes = {
  plpLabels: PropTypes.instanceOf(Object).isRequired,
  pdpLabels: PropTypes.instanceOf(Object).isRequired,
  showAddToBagCTA: PropTypes.bool.isRequired,
  handleFormSubmit: PropTypes.func.isRequired,
  currentProduct: PropTypes.shape({}).isRequired,
  selectedColorProductId: PropTypes.number.isRequired,
  toastMessage: PropTypes.func.isRequired,
  clearAddToBagError: PropTypes.func,
  clearMultipleAddToBagError: PropTypes.func,
  isBundleProduct: PropTypes.bool.isRequired,
  isFromBagProductSfl: PropTypes.bool.isRequired,
  isFavoriteEdit: PropTypes.bool.isRequired,
  sizeChartDetails: PropTypes.shape([]).isRequired,
  fromBagPage: PropTypes.bool.isRequired,
  keepAlive: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  isOutfitPage: PropTypes.bool.isRequired.isRequired,
  showColorChips: PropTypes.bool.isRequired.isRequired,
  quickViewColorSwatchesCss: PropTypes.string.isRequired,
  isPDP: PropTypes.bool.isRequired,
  currencySymbol: PropTypes.string.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  currencyAttributes: PropTypes.shape({}).isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  listPrice: PropTypes.string.isRequired,
  offerPrice: PropTypes.string.isRequired,
  itemBrand: PropTypes.string.isRequired,
  onCloseClick: PropTypes.func,
  isPickup: PropTypes.bool.isRequired,
  sizeChartLinkVisibility: PropTypes.number.isRequired,
  selectSize: PropTypes.func.isRequired,
  isDisableZeroInventoryEntries: PropTypes.bool.isRequired,
  showAddedToBagCta: PropTypes.bool.isRequired,
  revertAtbButton: PropTypes.func.isRequired,
  addedToBagLabel: PropTypes.func.isRequired,
  alternateSizes: PropTypes.shape([]).isRequired,
  errorOnHandleSubmit: PropTypes.func.isRequired,
  multiErrorOnSubmit: PropTypes.string,
  navigation: PropTypes.shape({}).isRequired,
  productInfoFromBag: PropTypes.shape({}).isRequired,
  fromPDP: PropTypes.bool.isRequired,
  onFitChange: PropTypes.func.isRequired,
  onChangeColor: PropTypes.func.isRequired,
  onChangeSize: PropTypes.func.isRequired,
  updateQuantityChange: PropTypes.func,
  getSKUValidated: PropTypes.func.isRequired,
  quantityChange: PropTypes.func.isRequired,
  isMultiItemQVModal: PropTypes.bool.isRequired,
  customFormName: PropTypes.string.isRequired,
  customSubmitButtonStyle: PropTypes.shape({}).isRequired,
  colorFitsSizesMap: PropTypes.shape({}).isRequired,
  favColorFitsSizesMap: PropTypes.shape({}).isRequired,
  formRef: PropTypes.shape({}).isRequired,
  formEnabled: PropTypes.bool.isRequired,
  isKeepAliveEnabled: PropTypes.bool.isRequired,
  pageNameProp: PropTypes.string.isRequired,
  pageSectionProp: PropTypes.string.isRequired,
  pageSubSectionProp: PropTypes.string.isRequired,
  addedTobagMsgDispatch: PropTypes.func.isRequired,
  addBtnMsg: PropTypes.string.isRequired,
  hasLoyaltyAbTestFlag: PropTypes.bool,
  showAddtoBagDrawer: PropTypes.bool.isRequired,
  isOutfitCarousel: PropTypes.bool,
  showOutfitCarouselAddedCTA: PropTypes.bool,
  outfitCarouselState: PropTypes.bool,
  isEnabledOutfitAddedCTA: PropTypes.bool.isRequired,
  multipackProduct: PropTypes.string.isRequired,
  multiPackThreshold: PropTypes.string.isRequired,
  multiPackCount: PropTypes.string.isRequired,
  closeModal: PropTypes.func.isRequired,
  getAllNewMultiPack: PropTypes.shape({}).isRequired,
  partIdInfo: PropTypes.shape({}).isRequired,
  scrollToTarget: PropTypes.func.isRequired,
  isRecommendationAvailable: PropTypes.bool.isRequired,
  multipackSelectionAction: PropTypes.func,
  isStyleWith: PropTypes.bool,
  getDetails: PropTypes.shape({}).isRequired,
  isPhysicalMpackEnabled: PropTypes.bool,
  checkForOOSForVariant: PropTypes.bool.isRequired,
  imageSwatchesToDisplay: PropTypes.shape([]).isRequired,
  isImageSwatchDisabled: PropTypes.bool,
  setStickyFooterState: PropTypes.func.isRequired,
  getRecommendationProduct: PropTypes.bool.isRequired,
  closePickupModal: PropTypes.func.isRequired,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  setPDPLargeImage: PropTypes.func,
  largeImageNameOnHover: PropTypes.string,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  resetProductDetailsDynamicData: PropTypes.shape([]).isRequired,
  trackAnalyticsClick: PropTypes.func,
  setInitialQuickViewLoad: PropTypes.bool.isRequired,
  getQickViewSingleLoad: PropTypes.func.isRequired,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  newColorList: PropTypes.shape([]),
  childProfile: PropTypes.shape({}).isRequired,
  sbpSizeClick: PropTypes.func.isRequired,
  isSBP: PropTypes.bool,
  sbpSize: PropTypes.bool,
  isSBPEnabled: PropTypes.bool,
  sbpFormValues: PropTypes.shape({}).isRequired,
  size: PropTypes.string,
  sbpFit: PropTypes.string,
  initialFormValues: PropTypes.shape({}).isRequired,
  initialValues: PropTypes.shape({}).isRequired,
  isQuickView: PropTypes.bool.isRequired,
  formValues: PropTypes.shape({}).isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  onPickUpOpenClick: PropTypes.func,
  hideMultipackPills: PropTypes.bool,
  disableSwappingMpackPills: PropTypes.bool,
  sbpLabels: PropTypes.shape({}),
  availableTCPmapNewStyleId: PropTypes.shape([]),
  selectedSizeFromOutfitProduct: PropTypes.string.isRequired,
  cartsSessionStatus: PropTypes.number.isRequired,
  isNewReDesignSizeFit: PropTypes.bool.isRequired,
  isNewReDesignProductSummary: PropTypes.bool,
  userDefaultStore: PropTypes.shape({
    basicInfo: PropTypes.shape({
      /** store id identifier */
      id: PropTypes.string.isRequired,
      /** store Name */
      storeName: PropTypes.string.isRequired,
    }).isRequired,
  }),
  isRadialInventoryEnabled: PropTypes.bool,
  labels: PropTypes.shape({
    lbl_Product_pickup_BOPIS_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: PropTypes.string,
    lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOSS_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOSS_ONLY_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_FIND_STORE: PropTypes.string,
    lbl_Product_pickup_FREE_SHIPPING: PropTypes.string,
    lbl_Product_pickup_NO_MIN_PURCHASE: PropTypes.string,
    lbl_Product_pickup_PICKUP_IN_STORE: PropTypes.string,
    lbl_Product_pickup_PRODUCT_BOPIS: PropTypes.string,
    lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: PropTypes.string,
    lbl_Product_pickup_CHANGE_STORE: PropTypes.string,
  }),
  onPickupClickAddon: PropTypes.func,
  currentColorEntry: PropTypes.shape({}),
  isNewQVEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  socialProofMessage: PropTypes.string,
  onMultiPackChange: PropTypes.func,
  sizeChange: PropTypes.func,
  fromPage: PropTypes.string,
  pageName: PropTypes.string,
  closeQuickViewSizeModal: PropTypes.func,
  isFamilyOutfitEnabled: PropTypes.bool,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  isPDPServiceEnabled: PropTypes.bool,
  isABTestPdpService: PropTypes.bool,
  getGiftCardValue: PropTypes.string,
  isShowBrandNameEnabled: PropTypes.bool,
  openModal: PropTypes.func,
  isFromBundlePage: PropTypes.bool,
  isFromChangeStore: PropTypes.bool,
  selectedSizeDrawerTab: PropTypes.number,
  addToPickupError: PropTypes.string,
  isOpenAddedToBag: PropTypes.bool,
  setAddToPickupError: PropTypes.func,
  updateSizeStatus: PropTypes.func,
  isShowDisabledSize: PropTypes.bool,
  isSelectedSizeDisabled: PropTypes.bool,
  handleRatingClick: PropTypes.func,
  isBopisFilterOn: PropTypes.bool,
};

ProductAddToBagContainer.defaultProps = {
  isOutfitCarousel: false,
  showOutfitCarouselAddedCTA: false,
  outfitCarouselState: false,
  hasLoyaltyAbTestFlag: false,
  multipackSelectionAction: () => {},
  isStyleWith: false,
  isPhysicalMpackEnabled: true,
  isImageSwatchDisabled: false,
  setPDPLargeImage: () => {},
  largeImageNameOnHover: '',
  trackAnalyticsClick: () => {},
  newColorList: [],
  isSBP: false,
  sbpSize: false,
  isSBPEnabled: false,
  multiErrorOnSubmit: '',
  size: '',
  sbpFit: '',
  isDynamicBadgeEnabled: false,
  onPickUpOpenClick: () => {},
  hideMultipackPills: false,
  disableSwappingMpackPills: false,
  sbpLabels: {},
  availableTCPmapNewStyleId: [],
  isNewReDesignProductSummary: false,
  userDefaultStore: {
    basicInfo: {
      id: '',
      storeName: '',
    },
  },
  isRadialInventoryEnabled: false,
  labels: {
    lbl_Product_pickup_BOPIS_AVAILABLE: 'Pick up TODAY!',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: 'husky',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: 'plus',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: 'slim',
    lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: 'Item available for pickup TODAY',
    lbl_Product_pickup_BOSS_AVAILABLE: 'Or choose NO RUSH Pickup ',
    lbl_Product_pickup_BOSS_ONLY_AVAILABLE: 'Choose NO RUSH Pickup ',
    lbl_Product_pickup_FIND_STORE: 'Find In Store',
    lbl_Product_pickup_FREE_SHIPPING: 'FREE Shipping Every Day!',
    lbl_Product_pickup_NO_MIN_PURCHASE: 'No Minimum Purchase Required.',
    lbl_Product_pickup_PICKUP_IN_STORE: 'PICK UP IN STORE',
    lbl_Product_pickup_PRODUCT_BOPIS: 'Select Store',
    lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: 'Select Store',
    lbl_Product_pickup_CHANGE_STORE: '(Change Store)',
    lbl_Product_pickup_UNAVAILABLE_IN_STORES: 'UNAVAILABLE IN STORES',
  },
  onPickupClickAddon: () => {},
  currentColorEntry: {},
  isNewQVEnabled: false,
  isNewPDPEnabled: false,
  socialProofMessage: '',
  onMultiPackChange: () => {},
  sizeChange: () => {},
  clearAddToBagError: () => {},
  clearMultipleAddToBagError: () => {},
  fromPage: '',
  pageName: '',
  closeQuickViewSizeModal: () => {},
  isFamilyOutfitEnabled: false,
  isPDPServiceEnabled: false,
  isABTestPdpService: false,
  getGiftCardValue: '',
  isShowBrandNameEnabled: false,
  openModal: () => {},
  isFromBundlePage: false,
  isFromChangeStore: false,
  selectedSizeDrawerTab: 0,
  addToPickupError: '',
  isOpenAddedToBag: false,
  setAddToPickupError: () => {},
  onCloseClick: () => {},
  handleRatingClick: () => {},
  updateQuantityChange: () => {},
  updateSizeStatus: () => {},
  isShowDisabledSize: false,
  isSelectedSizeDisabled: false,
  isBopisFilterOn: false,
};

/* Export container */

export default connect(mapStateToProps, mapDispatchToProps)(ProductAddToBagContainer);

export { ProductAddToBagContainer as ProductAddToBagContainerVanilla };
