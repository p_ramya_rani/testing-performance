// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';

import { ProductAddToBagVanilla } from '../views/ProductAddToBag.view.native';

describe('ProductAddToBagVanilla native should render correctly', () => {
  let wrapper;
  let component;
  let comp;
  let redesignComp;
  const props = {
    TCPMultipackProductMapping: [2, 3],
    currentProduct: {
      generalProductId: '',
      name: 'product',
    },
    isFromBundlePage: false,
    selectedColorProductId: 123,
    fromBagPage: true,
    fitSelectionLabel: '',
    sizeSelectionLabel: '',
    quickViewPickup: jest.fn(),
    selectedQuantity: { name: 6 },
    selectedSize: { name: 6 },
    selectedColor: 'black',
    ledgerSummaryData: { subTotal: 1234 },
  };

  const newprops = {
    ...props,
    isPhysicalMpackEnabled: true,
  };

  const newprops2 = {
    ...props,
    fromBagPage: true,
    productInfoFromBag: {
      itemBrand: 'tcp',
    },
  };

  const newProps3 = {
    TCPMultipackProductMapping: [2, 3],
    currentProduct: {
      generalProductId: '',
      name: 'product',
      isGiftCard: true,
    },
    selectedColorProductId: 123,
    fromBagPage: true,
    fitSelectionLabel: '',
    sizeSelectionLabel: '',
    quickViewPickup: jest.fn(),
    selectedQuantity: { name: 6 },
    selectedSize: { name: 6 },
    selectedColor: 'black',
    ledgerSummaryData: { subTotal: 1234 },
    isNewReDesignProductSummary: true,
    pdpLabels: {
      sizeSelectionLabel: 'Select a Size',
      sizeDrawerValue: 'Select a Value',
    },
  };

  beforeEach(() => {
    wrapper = shallow(<ProductAddToBagVanilla {...props} />);
  });

  beforeEach(() => {
    component = shallow(<ProductAddToBagVanilla {...newprops} />);
  });

  beforeEach(() => {
    comp = shallow(<ProductAddToBagVanilla {...newprops2} />);
  });

  beforeEach(() => {
    redesignComp = shallow(<ProductAddToBagVanilla {...newProps3} />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot for edit mpack from bagpage', () => {
    expect(comp).toMatchSnapshot();
  });

  it('should match snapshot for new design', () => {
    expect(redesignComp).toMatchSnapshot();
  });

  it('should render expected number of components', () => {
    expect(wrapper.find('View').length).toBe(2);
    expect(wrapper.find('Field').length).toBe(3);
    expect(component.find('View').length).toBe(2);
  });

  it('should render SBP component', () => {
    component.setProps({
      isSBP: true,
      showColorChips: true,
      onlySizeAndFit: false,
      isFromBagProductSfl: false,
      colorFitSizeDisplayNames: 'blue',
      selectedColor: { name: 'blue' },
      childProfile: {
        favorite_colors: 'blue, white',
        colorList: {
          color: {
            family: ['White, black, red, green, yellow, orange, blue'],
          },
        },
      },
    });
    expect(component).toBeDefined();
  });

  it('should call the setMPackScrollViewScrollTo', () => {
    redesignComp.setProps({ isNewReDesignProductSummary: true });
    redesignComp.instance().setMPackScrollViewScrollTo = jest.fn();
    redesignComp.instance().resetSizesFlatlist();
    expect(redesignComp.instance().setMPackScrollViewScrollTo).toBeCalled();
  });

  it('isAtbDisabled should return true', () => {
    expect(redesignComp.instance().isAtbDisabled()).toBeTruthy();
  });

  it('should increase quantity onQuantityValueIncrease', () => {
    const testProps = {
      onQuantityChange: jest.fn(),
      form: true,
      quantityList: [{}, {}],
      selectedQuantity: 1,
    };
    redesignComp.setProps(testProps);
    redesignComp.instance().onQuantityValueIncrease();
    expect(testProps.onQuantityChange).toHaveBeenCalledWith(
      testProps.selectedQuantity + 1,
      testProps.form
    );
  });

  it('should decrease quantity onQuantityValueIncrease', () => {
    const testProps = {
      onQuantityChange: jest.fn(),
      form: true,
      selectedQuantity: 4,
    };
    redesignComp.setProps(testProps);
    redesignComp.instance().onQuantityValueDecrease();
    expect(testProps.onQuantityChange).toHaveBeenCalledWith(
      testProps.selectedQuantity - 1,
      testProps.form
    );
  });

  it('should clear bag error onToastMessage method', () => {
    const testProps = {
      isNewReDesignSizeFit: true,
      clearAddToBagError: jest.fn(),
    };
    redesignComp.setProps(testProps);
    redesignComp.setState({
      showToastMessage: true,
    });
    redesignComp.instance().onToastMessage();
    expect(testProps.clearAddToBagError).toHaveBeenCalled();
  });

  it('should return 0 if no size list is provided', () => {
    const sizeList = [];
    expect(redesignComp.instance().checkLengthOfSizeChar(sizeList)).toBe(0);
  });

  it('should show different copy if product is a giftcard', () => {
    expect(redesignComp.instance().setColorFitSizeDisplayNames()).toBe('Select a Value');
  });

  it('should show multipack button', () => {
    const testProps = {
      isOutfitPage: false,
      isPickup: false,
      isStyleWith: false,
      isPhysicalMpackEnabled: true,
      isSBPEnabled: false,
      disableSwappingMpackPills: false,
    };

    const argument = [{}, {}];
    redesignComp.setProps(testProps);
    expect(redesignComp.instance().showMultiPackButton(argument)).toBeTruthy();
  });

  it('should not show multipack button', () => {
    const testProps = {
      isOutfitPage: false,
      isPickup: false,
      isStyleWith: false,
      isPhysicalMpackEnabled: true,
      isSBPEnabled: false,
      disableSwappingMpackPills: false,
    };

    const argument = [];
    redesignComp.setProps(testProps);
    expect(redesignComp.instance().showMultiPackButton(argument)).toBeFalsy();
  });

  it('should return getcolorFitSizeDisplayNames', () => {
    const testProps = {
      colorFitSizeDisplayNames: [],
      plpLabels: {
        size: 'XL',
        fit: 'Regular',
        color: 'BLUE',
      },
    };
    const result = {
      color: testProps.plpLabels.color,
      size: testProps.plpLabels.size,
      fit: testProps.plpLabels.fit,
      ...testProps.colorFitSizeDisplayNames,
    };
    redesignComp.setProps(testProps);
    expect(redesignComp.instance().getcolorFitSizeDisplayNames()).toEqual(result);
  });
});
