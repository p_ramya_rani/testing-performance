// 9fbef606107a605d69c0edbcd8029e5d 
import getStickyATCABTest from '../container/ProductAddToBag.selector';

describe('#ProductAddToBag Selectors', () => {
  const state = {
    AbTest: {
      hideStickyATC: true,
    },
  };

  it('#getPickupPromotionBannerLabels should return labels', () => {
    expect(getStickyATCABTest(state)).toEqual(true);
  });
});
