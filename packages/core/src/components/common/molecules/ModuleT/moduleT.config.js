// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  ctaTypes: {
    stackedCTAButtons: 'stackedCTAList',
    linkList: 'linkCTAList',
    CTAButtonCarousel: 'scrollCTAList',
    divImageCTACarousel: 'imageCTAList',
    ctaAddNoButtons: 'NoButton',
  },
  IMG_DATA: {
    promoImgConfig: ['t_mod_T_img_m', 't_mod_T_img_t', 't_mod_T_img_d'],
  },
};
