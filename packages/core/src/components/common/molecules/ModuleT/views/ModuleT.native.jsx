// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { getHeaderBorderRadius, getCtaBorderRadius } from '@tcp/core/src/utils/utils.app';
import HP_NEW_LAYOUT from '../../../../../constants/hpNewLayout.constants';
import { Anchor, DamImage } from '../../../atoms';
import { getLocator, styleOverrideEngine } from '../../../../../utils';
import config from '../moduleT.config';
import PromoBanner from '../../PromoBanner';
import LinkText from '../../LinkText';
import ButtonList from '../../ButtonList';
import {
  Container,
  PromoContainer,
  HeaderContainer,
  ImageContainer,
  MessageContainer,
  Wrapper,
  ButtonContainer,
  ButtonLinksContainer,
  Border,
  ImageWrapper,
} from '../ModuleT.style.native';
import colors from '../../../../../../styles/themes/TCP/colors';

// TODO: keys will be changed once we get the actual data from CMS
const { IMG_DATA, ctaTypes } = config;

/**
 * These are button width & height.
 */
const buttonWidth = 164;
const buttonHeight = 202;

/**
 * @param {object} props : Props for Module T multi type of banner list, button list, header text.
 * @desc This is Module T global component. It has capability to display
 * featured content module with links and a CTA Button list.
 * Author can surface teaser content leading to corresponding pages.
 */

// TODO: keys will be changed once we get the actual data from CMS

class ModuleT extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getHeaderBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  getCtaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
    return isHpNewLayoutEnabled
      ? getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom'])
      : {};
  };

  /**
   * This method return the ButtonList View according to the different variation .
   *  @ctaType are four types : 'imageCTAList' ,'stackedCTAList','scrollCTAList','linkCTAList'.
   *  @naviagtion is used to navigate the page.
   */
  renderButtonListItem = (
    ctaType,
    navigation,
    ctaItems,
    locator,
    buttonVariation,
    color,
    ctaBorderRadiusOverride
  ) => {
    return (
      ctaItems && (
        <ButtonList
          buttonListVariation={ctaType}
          navigation={navigation}
          buttonsData={ctaItems}
          locator={locator}
          color={color}
          overrideStyle={ctaBorderRadiusOverride}
        />
      )
    );
  };

  /**
   * This method return the renderButtonList method to manage all the different type variation .
   *  @ctaType are four types : 'imageCTAList' ,'stackedCTAList','scrollCTAList','linkCTAList'.
   *  @naviagtion is used to navigate the page.
   */
  renderButtonList(ctaType, navigation, ctaItems, ctaBorderRadiusOverride) {
    const ctaTypeValue = ctaTypes[ctaType];
    return (
      <View>
        {ctaTypeValue === ctaTypes.divImageCTACarousel && (
          <View>
            {this.renderButtonListItem(
              ctaTypeValue,
              navigation,
              ctaItems,
              'moduleT_cta_links',
              undefined,
              'black'
            )}
          </View>
        )}

        {ctaTypeValue === ctaTypes.stackedCTAButtons && (
          <View>
            {isEmpty(ctaBorderRadiusOverride) && <Border />}
            {this.renderButtonListItem(
              ctaTypeValue,
              navigation,
              ctaItems,
              'stacked_cta_list',
              'fixed-width',
              'gray',
              ctaBorderRadiusOverride
            )}
            {isEmpty(ctaBorderRadiusOverride) && <Border />}
          </View>
        )}

        {ctaTypeValue === ctaTypes.scrollCTAList && (
          <ButtonContainer>
            {this.renderButtonListItem(
              ctaTypeValue,
              navigation,
              ctaItems,
              'scroll_cta_list',
              undefined,
              'gray'
            )}
          </ButtonContainer>
        )}

        {ctaTypeValue === ctaTypes.linkList && (
          <ButtonLinksContainer>
            {this.renderButtonListItem(
              ctaTypeValue,
              navigation,
              ctaItems,
              'link_cta_list',
              undefined,
              'gray'
            )}
          </ButtonLinksContainer>
        )}
      </View>
    );
  }

  /**
   * To Render the Dam Image or Video Component
   */
  renderDamImage = (link, imgData, videoData, navigation, index) => {
    const damImageComp = (
      <DamImage
        imgData={imgData}
        videoData={videoData}
        height={buttonHeight}
        width={buttonWidth}
        testID={`${getLocator('moduleT_product_img')}${index}`}
        alt={imgData && imgData.alt}
        imgConfig={IMG_DATA.promoImgConfig[0]}
        isFastImage
        resizeMode="stretch"
        isHomePage
        link={link}
        navigation={navigation}
      />
    );
    if (imgData && Object.keys(imgData).length > 0) {
      return (
        <Anchor url={link ? link.url : ''} navigation={navigation} key={index.toString()}>
          {damImageComp}
        </Anchor>
      );
    }
    return videoData && Object.keys(videoData).length > 0 ? (
      <React.Fragment>{damImageComp}</React.Fragment>
    ) : null;
  };

  /**
   * This method return the renderMediaLinkedImage method to manage all product image with cropping rule .
   *  @naviagtion is used to navigate the page.
   */
  renderMediaLinkedImage = (mediaLinkedList, navigation) => {
    return (
      <ImageContainer>
        {mediaLinkedList.map(({ image, link, video }, index) => {
          const videoData = video &&
            video.url && {
              videoWidth: buttonWidth,
              videoHeight: 202,
              ...video,
            };
          return (
            <ImageWrapper tileIndex={index}>
              {this.renderDamImage(link, image, videoData, navigation, index)}
            </ImageWrapper>
          );
        })}
      </ImageContainer>
    );
  };

  render() {
    const {
      navigation,
      mediaLinkedList,
      headerText,
      promoBanner,
      ctaItems,
      ctaType,
      moduleClassName,
      isHpNewLayoutEnabled,
    } = this.props;
    const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleT') || {};
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const ctaBorderRadiusOverride = this.getCtaBorderRadiusOverride(
      isHpNewLayoutEnabled,
      styleOverrides
    );
    const newLayoutContainerStyle = isHpNewLayoutEnabled
      ? {
          ...HP_NEW_LAYOUT.MODULE_BOX_SHADOW,
        }
      : {};
    const headerAndMediaStyle = isHpNewLayoutEnabled
      ? {
          backgroundColor: colors?.WHITE,
          ...headerBorderRadiusOverride,
        }
      : {};
    return (
      <Container style={newLayoutContainerStyle}>
        <View style={headerAndMediaStyle}>
          <MessageContainer style={headerBorderRadiusOverride}>
            <Wrapper>
              <HeaderContainer>
                {headerText && (
                  <LinkText
                    navigation={navigation}
                    headerText={headerText}
                    testID={getLocator('moduleT_header_text')}
                    textAlign="center"
                    renderComponentInNewLine
                    useStyle
                    headerStyle={headerStyle}
                  />
                )}
              </HeaderContainer>
            </Wrapper>

            {promoBanner && (
              <PromoContainer>
                <PromoBanner
                  testID={getLocator('moduleT_promobanner_text')}
                  promoBanner={promoBanner}
                  navigation={navigation}
                  promoStyle={styleOverrides.promo}
                />
              </PromoContainer>
            )}
          </MessageContainer>
          {this.renderMediaLinkedImage(mediaLinkedList, navigation)}
        </View>
        {this.renderButtonList(ctaType, navigation, ctaItems, ctaBorderRadiusOverride)}
      </Container>
    );
  }
}

ModuleT.defaultProps = {
  promoBanner: [],
  moduleClassName: '',
};

ModuleT.propTypes = {
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.object,
      textItems: PropTypes.array,
    })
  ),
  ctaItems: PropTypes.shape([]).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  mediaLinkedList: PropTypes.arrayOf(
    PropTypes.shape({
      image: PropTypes.object,
      link: PropTypes.object,
    })
  ).isRequired,
  ctaType: PropTypes.oneOf(['stackedCTAButtons', 'linkCTAList', 'scrollCTAList', 'imageCTAList'])
    .isRequired,
  moduleClassName: PropTypes.string,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

export default ModuleT;
export { ModuleT as ModuleTVanilla };
