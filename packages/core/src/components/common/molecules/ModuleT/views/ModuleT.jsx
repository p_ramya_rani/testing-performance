// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  getStyleArray,
  getHeaderBorderRadius,
  getMediaBorderRadius,
} from '@tcp/core/src/utils/utils.web';
import ButtonList from '@tcp/core/src/components/common/molecules/ButtonList';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import Grid from '@tcp/core/src/components/common/molecules/Grid';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import { Col, DamImage, Row } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getLocator, styleOverrideEngine } from '@tcp/core/src/utils';
import moduleTStyle from '../styles/ModuleT.style';
import config from '../moduleT.config';

/**
 * @class ModuleT - global reusable component will display
 * category images and button CTAs so that end customers can directly navigate to the promotes category pages
 * This component is plug and play at any given slot in layout by passing required data
 * @param {ctaItems} ctaItems the list of data for buttons
 * @param {headerText} headerText the list of data for header
 * @param {promoBanner} promoBanner promo banner data
 * @param {mediaLinkedList} mediaLinkedList promo images list
 */
class ModuleT extends React.PureComponent {
  /* This method returns module's Header  */
  getHeader = (headerStyle) => {
    const { headerText } = this.props;
    return (
      headerText && (
        <LinkText
          component="h2"
          type="heading"
          headerText={headerText}
          className="header"
          headingClass="moduleT-header"
          headerStyle={headerStyle}
          dataLocator={getLocator('moduleT_header_text')}
        />
      )
    );
  };

  getHeaderBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getHeaderBorderRadius(styleOverrides['header-top'], styleOverrides['header-bottom'])
      : {};
  };

  getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
    return isHpNewModuleDesignEnabled && styleOverrides
      ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
      : {};
  };

  /* This method returns module promo banner  */
  getPromoBanner = (promoStyle) => {
    const { promoBanner } = this.props;
    const [{ textItems }] = promoBanner || [{}];
    const styleArray = getStyleArray(textItems);
    return (
      promoBanner && (
        <PromoBanner
          promoBanner={promoBanner}
          className="promoBanner"
          promoStyle={promoStyle}
          dataLocator={getLocator('moduleT_promobanner_text')}
          styleArray={styleArray}
        />
      )
    );
  };

  getColSizeLarge = (imageToEdge) => {
    return imageToEdge ? 4 : 3;
  };

  getColSizeMedium = (imageToEdge) => {
    return imageToEdge ? 3 : 2;
  };

  getOffset = (imageToEdge) => {
    return imageToEdge ? 0 : 1;
  };

  render() {
    const {
      className,
      ctaItems,
      ctaType,
      mediaLinkedList,
      moduleClassName,
      imageToEdge,
      isHomePage,
      isHpNewDesignCTAEnabled,
      isHpNewModuleDesignEnabled,
    } = this.props;
    const styleOverrides = styleOverrideEngine(moduleClassName, 'Default') || {};
    const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
    const { ctaTypes, IMG_DATA } = config;
    const promoMediaLinkedList = mediaLinkedList || [];
    const headerBorderRadiusOverride = this.getHeaderBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );
    const mediaBorderRadiusOverride = this.getMediaBorderRadiusOverride(
      isHpNewModuleDesignEnabled,
      styleOverrides
    );
    const {
      image: promoImage1,
      link: promoLink1,
      video: promoVideo1,
    } = promoMediaLinkedList[0] || {};
    const {
      image: promoImage2,
      link: promoLink2,
      video: promoVideo2,
    } = promoMediaLinkedList[1] || {};
    const buttonListCtaType = ctaTypes[ctaType];
    return (
      <Grid className={`${className} ${moduleClassName} moduleT`}>
        <div className="smallOnlyView" style={headerBorderRadiusOverride}>
          {this.getHeader(headerStyle)}
          {this.getPromoBanner(styleOverrides.promo)}
        </div>
        <Row className="moduleT-promo-wrapper" style={mediaBorderRadiusOverride}>
          <Col
            className="promo-image-left"
            colSize={{
              small: 3,
              medium: this.getColSizeMedium(imageToEdge),
              large: this.getColSizeLarge(imageToEdge),
            }}
            offsetLeft={{
              small: 0,
              medium: this.getOffset(imageToEdge),
              large: this.getOffset(imageToEdge),
            }}
            ignoreGutter={{
              medium: true,
            }}
          >
            {(promoImage1 || promoVideo1) && (
              <DamImage
                imgConfigs={IMG_DATA.promoImgConfig}
                imgData={promoImage1}
                data-locator={`${getLocator('moduleT_promobanner_img')}${1}`}
                link={promoLink1}
                videoData={promoVideo1}
                isHomePage={isHomePage}
                isOptimizedVideo
                lazyLoad
                isModule
              />
            )}
          </Col>
          <Col
            className="header-promo-wrapper"
            colSize={{
              small: 0,
              medium: 2,
              large: 4,
            }}
            hideCol={{
              small: true,
            }}
            ignoreGutter={{
              medium: true,
            }}
          >
            {this.getHeader(headerStyle)}
            {this.getPromoBanner(styleOverrides.promo)}
          </Col>
          <Col
            className="promo-image-right"
            colSize={{
              small: 3,
              medium: this.getColSizeMedium(imageToEdge),
              large: this.getColSizeLarge(imageToEdge),
            }}
            offsetRight={{
              small: 0,
              medium: this.getOffset(imageToEdge),
              large: this.getOffset(imageToEdge),
            }}
            ignoreGutter={{
              medium: true,
            }}
          >
            {(promoImage2 || promoVideo2) && (
              <DamImage
                imgConfigs={IMG_DATA.promoImgConfig}
                imgData={promoImage2}
                data-locator={`${getLocator('moduleT_promobanner_img')}${2}`}
                link={promoLink2}
                videoData={promoVideo2}
                isHomePage={isHomePage}
                isOptimizedVideo
                lazyLoad
                isModule
              />
            )}
          </Col>
        </Row>
        <div className={`button-list-container ${buttonListCtaType}`}>
          {ctaItems && (
            <ButtonList
              buttonsData={ctaItems}
              buttonListVariation={buttonListCtaType}
              dataLocatorDivisionImages={getLocator('moduleT_cta_image')}
              dataLocatorTextCta={getLocator('moduleT_cta_links')}
              className="button-list-container-alternate"
              btnCompClass="btn-list-color"
              isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
            />
          )}
        </div>
      </Grid>
    );
  }
}

ModuleT.defaultProps = {
  className: '',
  promoBanner: [],
  mediaLinkedList: [],
  moduleClassName: '',
  imageToEdge: false,
};

ModuleT.propTypes = {
  className: PropTypes.string,
  headerText: PropTypes.arrayOf(
    PropTypes.shape({
      textItems: PropTypes.array,
      link: PropTypes.object,
      icon: PropTypes.object,
    })
  ).isRequired,
  promoBanner: PropTypes.arrayOf(
    PropTypes.shape({
      textItems: PropTypes.array,
      link: PropTypes.object,
    })
  ),
  mediaLinkedList: PropTypes.arrayOf(
    PropTypes.shape({
      image: PropTypes.object,
      link: PropTypes.object,
      video: PropTypes.object,
    })
  ),
  ctaItems: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  ctaType: PropTypes.oneOf(['stackedCTAButtons', 'linkCTAList', 'scrollCTAList', 'imageCTAList'])
    .isRequired,
  moduleClassName: PropTypes.string,
  isHomePage: PropTypes.bool.isRequired,
  imageToEdge: PropTypes.bool,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

export default withStyles(ModuleT, moduleTStyle);
export { ModuleT as ModuleTVanilla };
