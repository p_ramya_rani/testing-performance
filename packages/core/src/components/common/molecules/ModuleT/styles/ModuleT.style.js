// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  margin-left: auto;
  margin-right: auto;

  @media ${props => props.theme.mediaQuery.medium} {
    .smallOnlyView {
      display: none;
    }
  }

  @media ${props => props.theme.mediaQuery.mediumOnly} {
    .header-promo-wrapper {
      margin: 0 15px;
      width: 26%;
    }
  }

  .moduleT-promo-wrapper {
    display: flex;
    flex-direction: row;
    align-items: center;
  }

  .moduleT-header {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }

  .promoBanner {
    margin: 0 auto ${props => props.theme.spacing.ELEM_SPACING.SM};

    @media ${props => props.theme.mediaQuery.mediumOnly} {
      .medium_text_black {
        line-height: 1;
      }
    }
  }

  .button-list-wrapper {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
    padding-bottom: 0;

    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }

    .stacked-button:last-child {
      margin-right: 0;
    }
  }
  .button-list-container {
    ${props =>
      props.theme.isGymboree
        ? `
      background-color: ${props.theme.colorPalette.blue.C900};
      padding: ${props.theme.spacing.ELEM_SPACING.MED} 0;
      margin-top: ${props.theme.spacing.ELEM_SPACING.SM};

      @media ${props.theme.mediaQuery.medium} {
        margin-top: ${props.theme.spacing.ELEM_SPACING.MED};
      }

      .button-list-wrapper {
        padding: 0;
        margin: 0;
      }
    `
        : `
      .image-comp {
        color: ${props.theme.colorPalette.black};
      }
    `}
  }

  .btn-list-color {
    color: ${props =>
      props.theme.isGymboree
        ? props.theme.colorPalette.white
        : props.theme.colorPalette.text.primary};
  }
`;
