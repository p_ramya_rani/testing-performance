// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import moduleTMockData from '../../../../../services/abstractors/common/moduleT/mock';
import { ModuleTVanilla } from '../views/ModuleT.native';

describe('ModuleTVanilla', () => {
  let ModuleTComponent;

  beforeEach(() => {
    ModuleTComponent = shallow(<ModuleTVanilla {...moduleTMockData.moduleT.composites} />);
  });

  it('ModuleT should be defined', () => {
    expect(ModuleTComponent).toBeDefined();
  });

  it('ModuleT should render correctly', () => {
    expect(ModuleTComponent).toMatchSnapshot();
  });

  it('ModuleT should render correctly when isHpNewLayoutEnabled is true', () => {
    ModuleTComponent = shallow(
      <ModuleTVanilla {...moduleTMockData.moduleT.composites} isHpNewLayoutEnabled />
    );
    expect(ModuleTComponent).toMatchSnapshot();
  });
});
