// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import mock from '../../../../../services/abstractors/common/moduleN/mock';
import { ModuleNVanilla as ModuleN } from '../views/ModuleN';

describe('N component', () => {
  let ModuleNComp;

  it('renders correctly', () => {
    ModuleNComp = shallow(<ModuleN {...mock.moduleN.composites} set={mock.moduleN.set} />);
    expect(ModuleNComp).toMatchSnapshot();
  });

  it('renders correctly without ctaItems', () => {
    const composites = { ...mock.moduleN.composites, ctaItems: [] };
    ModuleNComp = shallow(<ModuleN {...composites} set={mock.moduleN.set} />);
    expect(ModuleNComp).toMatchSnapshot();
  });

  it('renders correctly with ctaType as CTAButtonCarouselExpandable', () => {
    const composites = { ...mock.moduleN.composites, ctaType: 'CTAButtonCarouselExpandable' };
    ModuleNComp = shallow(<ModuleN {...composites} set={mock.moduleN.set} />);
    expect(ModuleNComp).toMatchSnapshot();
  });

  it('renders correctly with ctaType as null', () => {
    const composites = { ...mock.moduleN.composites, ctaType: null };
    ModuleNComp = shallow(<ModuleN {...composites} set={mock.moduleN.set} />);
    expect(ModuleNComp).toMatchSnapshot();
  });
});
