// 9fbef606107a605d69c0edbcd8029e5d 
export const config = {
  ctaTypes: {
    stackedCTAButtons: 'stackedCTAList',
    linkList: 'linkCTAList',
    CTAButtonCarousel: 'scrollCTAList',
    divImageCTACarousel: 'imageCTAList',
    stackedCTAButtonsExpandable: 'stackedCTAList',
    CTAButtonCarouselExpandable: 'scrollCTAList',
    ctaAddNoButtons: 'NoButton',
  },
};

export const ctaTypeProps = {
  stackedCTAButtonsExpandable: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: true,
        large: true,
      },
    },
  },
  CTAButtonCarouselExpandable: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: true,
        large: true,
      },
    },
  },
  ctaAddNoButtons: {
    dualVariation: {
      name: 'dropdownButtonCTA',
      displayProps: {
        small: false,
        medium: false,
        large: false,
      },
    },
  },
};

export const IMG_DATA_TCP = {
  crops: ['t_mod_N_img_m', 't_mod_N_img_t', 't_mod_N_img_d'],
};
export const IMG_DATA_GYM = {
  crops: ['t_mod_N_GYM_img_m', 't_mod_N_GYM_img_t', 't_mod_N_GYM_img_d'],
};
