// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const Container = styled.View``;
export const PromoTextBannerWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;
export const ButtonContainer = styled.View`
  margin-top: ${props => (props.isLinkList ? 0 : props.theme.spacing.LAYOUT_SPACING.XXS)};
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
`;
export const ContainerView = styled.View``;
export const Border = styled.View`
  height: 1px;
  background: ${props =>
    props.background === 'red'
      ? props.theme.colorPalette.secondary.dark
      : props.theme.colorPalette.primary.dark};
`;

export const HeaderContainer = styled.View`
  margin-top: ${props => (props.theme.isGymboree ? props.theme.spacing.LAYOUT_SPACING.SM : '9px')};
`;

export const DivImageCTAContainer = styled.View``;

export const Wrapper = styled.View`
  ${props =>
    props.bIsBGMedia
      ? `
      position:relative;
      display:flex;
      justify-content:center;
      align-items:center;`
      : `background: ${props.background || props.theme.colorPalette.red[400]};`}
`;
export const InnerWrapper = styled.View`
  ${props =>
    props.bIsBGMedia
      ? `
      position:absolute;
      `
      : ''}
`;

export default {
  Container,
  PromoTextBannerWrapper,
  ButtonContainer,
  Border,
  ContainerView,
  DivImageCTAContainer,
  HeaderContainer,
  Wrapper,
  InnerWrapper,
};
