// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { cropImageUrl } from '@tcp/core/src/utils';

const getCtaBgImage = (bgImageObj) => {
  const {
    ctaBackgroundImage: { url: ctaDesktopUrl, url_t: ctaTabletUrl, url_m: ctaMobileUrl },
  } = bgImageObj || {};
  return {
    desktop: ctaDesktopUrl && cropImageUrl(ctaDesktopUrl),
    tablet: ctaTabletUrl ? cropImageUrl(ctaTabletUrl) : cropImageUrl(ctaDesktopUrl),
    mobile: ctaMobileUrl ? cropImageUrl(ctaMobileUrl) : cropImageUrl(ctaDesktopUrl),
  };
};

export const StyledButtonContainer = styled.div`
  ${(props) =>
    props.ctaBgImageObj && getCtaBgImage(props.ctaBgImageObj)
      ? `
        background-image: url(${getCtaBgImage(props.ctaBgImageObj).mobile});
        @media ${props.theme.mediaQuery.large} {
          background-image: url(${getCtaBgImage(props.ctaBgImageObj).desktop});
        }
        @media ${props.theme.mediaQuery.mediumOnly} {
          background-image: url(${getCtaBgImage(props.ctaBgImageObj).tablet});
        }
      `
      : null};
`;

export default css`
  background-color: ${(props) =>
    props.image || props.video ? 'transparent' : props.theme.colorPalette.red['400']};
  .button-desktop {
    display: none;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: block;
    }
  }
  .button-mobile {
    display: block;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .content-wrapper {
    ${(props) =>
      props.image || props.video
        ? `
        .video-play-button {
          display:none;
        }
          display:flex;
          justify-content:center;
          align-items:center;
          position:relative;
          margin-bottom: -4px;
        `
        : ''}
  }

  .content-overlay {
    ${(props) =>
      props.image || props.video
        ? `
        position:absolute;

        `
        : ''}
  }
  .heading-wrapper {
    padding-top: 32px;

    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-top: 24px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      text-align: center;
    }
  }

  .button-list-wrapper {
    padding-top: 24px;
    padding-bottom: 24px;
  }
  .stacked-button-list-wrapper {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      padding-bottom: 0px;
      padding-top: 0px;
    }
  }

  .text_normal {
    margin-top: 0px;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: -14px;
    }
  }

  .stacked-cta-wrapper-class {
    padding-top: 16px;
    padding-right: 20px;
    padding-bottom: 16px;
    padding-left: 20px;
    color: ${(props) =>
      props.theme.isGymboree &&
      props.ctaType === 'stackedCTAButtons' &&
      props.theme.colorPalette.red['300']};
  }

  .dropdown-button {
    margin-top: 32px;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 48px;
    }
  }

  h2.link-text {
    margin: 0;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-bottom: 5px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      display: inline;
      margin-bottom: 0;
    }
  }

  .percentage_inline_promo {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      font-size: ${(props) =>
        props.theme.isGymboree ? props.theme.typography.fontSizes.fs42 : ''};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-left: 10px;
    }
  }

  .promo-link {
    color: ${(props) => props.theme.colorPalette.white};
  }

  .moduleN-heading-wrapper {
    display: inline-block;
    margin-top: 14px;

    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-top: 17px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 46px 6px 0 0;
    }
  }

  .ModuleN-heading {
    @media ${(props) => props.theme.mediaQuery.large} {
      display: inline-block;
      padding-bottom: 8px;
    }
  }

  &.brand-tcp .link-text .white_large_text_half,
  &.brand-tcp .moduleN__promo-banner .percentage_wrapped_large {
    font-family: TofinoWide;
    font-weight: 900;
  }

  &.brand-tcp .link-text .white_large_text_half {
    font-size: 32px;
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: 64px;
    }
  }

  &.brand-tcp .moduleN__promo-banner .small_text_white_medium {
    font-family: Nunito;
    font-weight: 700;
    line-height: 24px;
    font-size: 18px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      line-height: 27px;
      font-size: 20px;
    }
  }

  &.brand-tcp {
    .percentage_wrapped_large-0 {
      font-size: 62px;
    }
    .percentage_wrapped_large-1 {
      font-size: 36px;
    }
    .percentage_wrapped_large-2 {
      font-size: 18px;
    }
  }

  .percentage_wrapped_large__wrapped_text {
    flex-direction: column;
  }

  .percentage_wrapped_large__wrapped_text div {
    white-space: normal;
  }

  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    &.brand-tcp .wrapped-button-text .stacked-button .cta-button-text {
      padding: 11px 0;
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallMax} {
    &.brand-tcp .heading-wrapper {
      padding-top: 14px;
      padding-bottom: 24px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .heading-wrapper {
      text-align: center;
      display: ${(props) => (props.halfWidth ? 'block' : 'flex')};
      align-items: center;
      justify-content: center;
      padding-bottom: 0px;
      padding-top: 24px;
    }
    .heading,
    .moduleN__promo-banner {
      display: block;
      margin-bottom: 0px;
    }

    &.brand-tcp {
      .percentage_wrapped_large__wrapped_text {
        flex-direction: column;
      }
      .moduleN__promo-banner {
        margin-top: -16px;
      }
      .percentage_wrapped_large-0 {
        font-size: 88px;
      }
      .percentage_wrapped_large-1 {
        font-size: 48px;
        white-space: normal;
      }
      .percentage_wrapped_large-2 {
        font-size: 24px;
        margin-left: 4px;
      }
    }
  }
  .ModuleN_Button .link-button-wrapper-class {
    border-bottom-color: ${(props) => props.theme.colorPalette.white};
  }
`;
