// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { HERO_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import ButtonList from '@tcp/core/src/components/common/molecules/ButtonList';
import LinkText from '@tcp/core/src/components/common/molecules/LinkText';
import PromoBanner from '@tcp/core/src/components/common/molecules/PromoBanner';
import {
  getLocator,
  styleOverrideEngine,
  getBrand,
  isGymboree,
  isClient,
  getViewportInfo,
} from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Col, DamImage } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import { getCtaBorderRadius, getMediaBorderRadius } from '@tcp/core/src/utils/utils.web';
import style, { StyledButtonContainer } from '../ModuleN.style';
import { ctaTypeProps, config, IMG_DATA_TCP, IMG_DATA_GYM } from '../config';

const { ctaTypes } = config;

const getButtonListVariationProps = (ctaType) => {
  const buttonTypeProps = {
    ...ctaTypeProps,
  };
  return buttonTypeProps[ctaType];
};

const getMappedPromoBanner = (promoBanner) => {
  const promoTexts = promoBanner ? promoBanner[0] : { textItems: [] };
  return promoTexts.textItems.map((item) => {
    return { ...promoTexts, textItems: [item] };
  });
};

const getButtonList = (
  ctaItems,
  ctaType,
  expandableTitle,
  dualVariation,
  buttonColor,
  isHpNewDesignCTAEnabled
) =>
  ctaItems ? (
    <ButtonList
      buttonListVariation={ctaTypes[ctaType]}
      buttonsData={ctaItems}
      fill="RED"
      dataLocatorDivisionImages={getLocator('moduleN_image')}
      dataLocatorDropDown={getLocator('moduleN_dropdown')}
      dropdownLabel={expandableTitle}
      dataLocatorTextCta={getLocator('moduleN_cta_links')}
      dualVariation={dualVariation}
      buttonColor={buttonColor}
      isHpNewDesignCTAEnabled={isHpNewDesignCTAEnabled}
    />
  ) : (
    ``
  );

const setDualVariation = (ctaType, ctaItems, buttonListProps) => {
  let dualVariation = null;
  if (ctaType === 'stackedCTAButtonsExpandable' || ctaType === 'CTAButtonCarouselExpandable') {
    dualVariation =
      ctaItems && ctaItems.length < 3 ? null : buttonListProps && buttonListProps.dualVariation;
  }
  return dualVariation;
};

const renderDamImage = (imageData, videoData, isHomePage) => {
  return (
    <DamImage
      videoData={videoData}
      imgData={imageData}
      crop={imageData && imageData.crop_m}
      imgConfig={isGymboree() ? IMG_DATA_GYM.crops : IMG_DATA_TCP.crops}
      isHomePage={isHomePage}
      isOptimizedVideo
      lazyLoad
      isModule
    />
  );
};

const getCtaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom'])
    : {};
};

const getMediaBorderRadiusOverride = (isHpNewModuleDesignEnabled, styleOverrides) => {
  return isHpNewModuleDesignEnabled && styleOverrides
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};
// eslint-disable-next-line
const ModuleN = (props) => {
  /**
   * This just holds the logic for rendering a UX timer
   */
  function SetPerformanceTimer(isHero) {
    const [state, setState] = useState(false);
    useEffect(() => {
      if (isHero) setState(true);
    }, [isHero]);
    return state ? <RenderPerf.Measure name={HERO_VISIBLE} /> : null;
  }

  const {
    className,
    ctaItems,
    headerText,
    promoBanner,
    ctaType,
    expandableTitle,
    moduleClassName,
    page,
    image: imageData,
    video: videoData,
    isHomePage,
    isHeroSlot,
    isHpNewDesignCTAEnabled,
    isHpNewModuleDesignEnabled,
  } = props;
  const { isMobile } = isClient() && getViewportInfo();
  const ctaItemFilteredObj = ctaItems && ctaItems.filter((item) => item.button);
  const ctaBgImageFilteredObj = ctaItems && ctaItems.filter((item) => item.ctaBackgroundImage);
  const ctaBgImage = ctaBgImageFilteredObj.length !== 0 && ctaBgImageFilteredObj[0];
  const buttonListProps = getButtonListVariationProps(ctaType);
  const dualVariation = setDualVariation(ctaType, ctaItemFilteredObj, buttonListProps);
  const mappedPromoBanner = getMappedPromoBanner(promoBanner);
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleN');
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
  const bIsBGMedia = imageData || videoData;
  const ctaBorderRadiusOverride = !isMobile
    ? getCtaBorderRadiusOverride(isHpNewModuleDesignEnabled, styleOverrides)
    : {};
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewModuleDesignEnabled,
    styleOverrides
  );

  return (
    <Row
      className={`${className} ${moduleClassName} moduleN brand-${getBrand()} page-${page}`}
      fullBleed={{ small: true, medium: true, large: true }}
      data-locator={getLocator('moduleN_promobanner_img')}
      customStyle={videoData || imageData ? {} : styleOverrides.bground}
      style={mediaBorderRadiusOverride}
    >
      <Col
        className="moduleN-innerContent"
        colSize={{
          small: 6,
          medium: 8,
          large: 12,
        }}
      >
        <div className="content-wrapper">
          {bIsBGMedia && renderDamImage(imageData, videoData, isHomePage)}
          <SetPerformanceTimer isHero={isHeroSlot} />
          <div className="content-overlay">
            <div className="heading-wrapper">
              {headerText && (
                <LinkText
                  headerText={headerText}
                  component="h2"
                  textAlign="center"
                  type="heading"
                  color="white"
                  className="ModuleN-heading"
                  dataLocator={getLocator('moduleN_header_text')}
                  headerStyle={headerStyle}
                />
              )}
              {mappedPromoBanner && mappedPromoBanner[0] && (
                <PromoBanner
                  promoBanner={[mappedPromoBanner[0]]}
                  className="moduleN__promo-banner"
                  color="white"
                  data-locator={getLocator('moduleN_promobanner_text')}
                  promoStyle={styleOverrides.promo}
                />
              )}
            </div>
            {mappedPromoBanner && mappedPromoBanner[1] && (
              <PromoBanner
                promoBanner={[mappedPromoBanner[1]]}
                className="moduleN__promo-banner"
                color="white"
                data-locator={getLocator('moduleN_promobanner_text')}
                promoStyle={styleOverrides.promo}
              />
            )}
            <StyledButtonContainer
              ctaBgImageObj={ctaBgImage}
              className="ModuleN_Button button-desktop"
              style={ctaBorderRadiusOverride}
            >
              {getButtonList(
                ctaItemFilteredObj,
                ctaType,
                expandableTitle,
                dualVariation,
                styleOverrides.button,
                isHpNewDesignCTAEnabled
              )}
            </StyledButtonContainer>
          </div>
        </div>
        <StyledButtonContainer
          ctaBgImageObj={ctaBgImage}
          className="ModuleN_Button button-mobile"
          style={ctaBorderRadiusOverride}
        >
          {getButtonList(
            ctaItemFilteredObj,
            ctaType,
            expandableTitle,
            dualVariation,
            styleOverrides.button
          )}
        </StyledButtonContainer>
      </Col>
    </Row>
  );
};

ModuleN.defaultProps = {
  className: '',
  ctaItems: [],
  headerText: [],
  promoBanner: [],
  ctaType: 'stackedCTAButtons',
  expandableTitle: '',
  moduleClassName: '',
  page: '',
  image: {},
  video: {},
  isHeroSlot: false,
};

ModuleN.propTypes = {
  className: PropTypes.string,
  ctaItems: PropTypes.arrayOf(PropTypes.shape({})),
  headerText: PropTypes.arrayOf(PropTypes.shape({})),
  promoBanner: PropTypes.arrayOf(PropTypes.shape({})),
  ctaType: PropTypes.string,
  expandableTitle: PropTypes.string,
  moduleClassName: PropTypes.string,
  page: PropTypes.string,
  image: PropTypes.shape({}),
  video: PropTypes.shape({}),
  isHomePage: PropTypes.string.isRequired,
  isHeroSlot: PropTypes.bool,
  isHpNewDesignCTAEnabled: PropTypes.bool.isRequired,
  isHpNewModuleDesignEnabled: PropTypes.bool.isRequired,
};

export default errorBoundary(withStyles(ModuleN, style));
export { ModuleN as ModuleNVanilla };
