// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getCtaBorderRadius, getMediaBorderRadius } from '@tcp/core/src/utils/utils.app';
import HP_NEW_LAYOUT from '@tcp/core/src/constants/hpNewLayout.constants';
import LinkText from '../../LinkText';
import PromoBanner from '../../PromoBanner';
import ButtonList from '../../ButtonList/views/ButtonList.native';
import {
  Container,
  PromoTextBannerWrapper,
  ButtonContainer,
  ContainerView,
  DivImageCTAContainer,
  HeaderContainer,
  Wrapper,
  InnerWrapper,
} from '../ModuleN.styles.native';
import { config, IMG_DATA_TCP, IMG_DATA_GYM } from '../config';
import { isGymboree, styleOverrideEngine } from '../../../../../utils';
import { DamImage } from '../../../atoms';
import { getScreenWidth, LAZYLOAD_HOST_NAME } from '../../../../../utils/index.native';

const getMarginFromStyleOverrides = styleOverrides => {
  const marginBottom =
    styleOverrides && styleOverrides.marginBottom && styleOverrides.marginBottom['margin-bottom'];
  const marginTop =
    styleOverrides && styleOverrides.marginTop && styleOverrides.marginTop['margin-top'];
  return {
    marginBottom: (marginBottom && parseFloat(marginBottom)) || 0,
    marginTop: (marginTop && parseFloat(marginTop)) || 0,
  };
};

/**
 * @param {object} props : Props for Module N multi type of banner list, button list, header text.
 * @desc This is Module N global component. It has capability to display
 * featured content module with 1 background color tiles ,links and a CTA Button list.
 * Author can surface teaser content leading to corresponding pages.
 */

// TODO: keys will be changed once we get the actual data from CMS

const { ctaTypes } = config;

const renderDamImage = (
  imageData,
  videoData,
  isHomePage,
  borderWidth = 0,
  { heightOverride, mediaBorderRadiusOverride, isHpNewLayoutEnabled }
) => {
  const MODULE_OVERRIDE_HEIGHT = heightOverride && parseFloat(heightOverride.height);
  const MODULE_DEFAULT_HEIGHT = MODULE_OVERRIDE_HEIGHT || 350;
  const MODULE_WIDTH = getScreenWidth() - borderWidth;
  const NEW_LAYOUT_MODULE_WIDTH = getScreenWidth() - borderWidth - 2 * HP_NEW_LAYOUT.BODY_PADDING;

  return (
    <DamImage
      width={isHpNewLayoutEnabled ? NEW_LAYOUT_MODULE_WIDTH : MODULE_WIDTH}
      videoData={videoData}
      height={MODULE_DEFAULT_HEIGHT}
      imgData={imageData}
      host={LAZYLOAD_HOST_NAME.HOME}
      crop={imageData && imageData.crop_m}
      imgConfig={isGymboree() ? IMG_DATA_GYM.crops[0] : IMG_DATA_TCP.crops[0]}
      isFastImage
      isHomePage={isHomePage}
      resizeMode="stretch"
      overrideStyle={mediaBorderRadiusOverride}
      isBackground={false}
    />
  );
};
const renderHeader = (navigation, headerText, headerStyle) => {
  return (
    <LinkText
      type="heading"
      fontFamily={isGymboree() ? 'secondary' : 'quaternary'}
      fontSize={isGymboree() ? 'fs20' : 'fs32'}
      fontWeight={isGymboree() ? 'semibold' : 'black'}
      letterSpacing={isGymboree() ? 'ls222' : 'ls167'}
      textAlign="center"
      color="white"
      navigation={navigation}
      headerText={headerText}
      locator="moduleN_header_text"
      headerStyle={headerStyle}
    />
  );
};

const getCtaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled && styleOverrides
    ? getCtaBorderRadius(styleOverrides['cta-top'], styleOverrides['cta-bottom'])
    : {};
};

const getMediaBorderRadiusOverride = (isHpNewLayoutEnabled, styleOverrides) => {
  return isHpNewLayoutEnabled && styleOverrides
    ? getMediaBorderRadius(styleOverrides['media-top'], styleOverrides['media-bottom'])
    : {};
};

// eslint-disable-next-line complexity
const ModuleN = props => {
  const {
    ctaItems,
    headerText,
    navigation,
    promoBanner,
    ctaType,
    moduleClassName,
    image: imageData,
    video: videoData,
    isHomePage,
    borderWidth,
    isHpNewLayoutEnabled,
  } = props;
  const ctaTypeVal = ctaTypes[ctaType];
  const styleOverrides = styleOverrideEngine(moduleClassName, 'ModuleN');
  const headerStyle = [styleOverrides.title, styleOverrides.subTitle];
  const heightOverride = styleOverrides.imgHeight;
  const { backgroundColor } = styleOverrides.bground || {};
  const marginStyle = getMarginFromStyleOverrides(styleOverrides);
  const bIsBGMedia = imageData || videoData;
  const mediaBorderRadiusOverride = getMediaBorderRadiusOverride(
    isHpNewLayoutEnabled,
    styleOverrides
  );
  const ctaBorderRadiusOverride = getCtaBorderRadiusOverride(isHpNewLayoutEnabled, styleOverrides);
  const boxShadowStyle = isHpNewLayoutEnabled && HP_NEW_LAYOUT.MODULE_BOX_SHADOW;
  return (
    <Container style={{ ...marginStyle, ...boxShadowStyle }}>
      <Wrapper
        background={backgroundColor}
        bIsBGMedia={bIsBGMedia}
        style={mediaBorderRadiusOverride}
      >
        {bIsBGMedia &&
          renderDamImage(imageData, videoData, isHomePage, borderWidth, {
            heightOverride,
            mediaBorderRadiusOverride,
            isHpNewLayoutEnabled,
          })}
        <InnerWrapper bIsBGMedia={bIsBGMedia}>
          <HeaderContainer>
            {headerText && renderHeader(navigation, headerText, headerStyle)}
          </HeaderContainer>
          <PromoTextBannerWrapper>
            {promoBanner && (
              <PromoBanner
                promoBanner={promoBanner}
                navigation={navigation}
                locator="moduleN_promobanner_text"
                promoStyle={styleOverrides.promo}
              />
            )}
          </PromoTextBannerWrapper>
        </InnerWrapper>

        {ctaTypeVal === ctaTypes.linkList && (
          <ButtonContainer isLinkList>
            {ctaItems && (
              <ButtonList
                buttonListVariation={ctaTypeVal}
                navigation={navigation}
                buttonsData={ctaItems}
                locator="moduleN_cta_links"
                buttonColor={styleOverrides.button}
              />
            )}
          </ButtonContainer>
        )}
      </Wrapper>

      {ctaTypeVal === ctaTypes.divImageCTACarousel && (
        <DivImageCTAContainer>
          {ctaItems && (
            <ButtonList
              buttonListVariation={ctaTypeVal}
              navigation={navigation}
              buttonsData={ctaItems}
              locator="moduleN_cta_links"
              color="white"
            />
          )}
        </DivImageCTAContainer>
      )}

      {ctaTypeVal === ctaTypes.stackedCTAButtons && (
        <ContainerView>
          {ctaItems && (
            <ButtonList
              buttonListVariation={ctaTypeVal}
              navigation={navigation}
              buttonsData={ctaItems}
              locator="moduleN_cta_links"
              color="red"
              buttonVariation="cautionary"
              buttonColor={styleOverrides.button}
              overrideStyle={{
                ...ctaBorderRadiusOverride,
              }}
            />
          )}
        </ContainerView>
      )}

      {ctaTypeVal === ctaTypes.CTAButtonCarousel && (
        <ButtonContainer>
          {ctaItems && (
            <ButtonList
              buttonListVariation={ctaTypeVal}
              navigation={navigation}
              buttonsData={ctaItems}
              locator="moduleN_cta_links"
              color="red"
              buttonVariation="cautionary"
              buttonColor={styleOverrides.button}
            />
          )}
        </ButtonContainer>
      )}
    </Container>
  );
};

ModuleN.propTypes = {
  ctaItems: PropTypes.bool.isRequired,
  ctaType: PropTypes.bool.isRequired,
  navigation: PropTypes.bool.isRequired,
  moduleClassName: PropTypes.bool.isRequired,
  headerText: PropTypes.bool.isRequired,
  promoBanner: PropTypes.bool.isRequired,
  image: PropTypes.shape({}).isRequired,
  video: PropTypes.shape({}).isRequired,
  isHomePage: PropTypes.bool.isRequired,
  borderWidth: PropTypes.number,
  isHpNewLayoutEnabled: PropTypes.bool.isRequired,
};

ModuleN.defaultProps = {
  borderWidth: 0,
};

export default ModuleN;
export { ModuleN as ModuleNVanilla };
