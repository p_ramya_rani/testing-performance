/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import isEqual from 'lodash/isEqual';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { fromJS } from 'immutable';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  Row,
  Button,
  Image,
  Col,
  BodyCopy,
  SelectBox,
} from '@tcp/core/src/components/common/atoms';
import {
  getIconPath,
  getViewportInfo,
  isClient,
  addRecommendationsObj,
  onTabClickDetails,
} from '@tcp/core/src/utils';
import ReactTooltip from '@tcp/core/src/components/common/atoms/ReactToolTip';
import { CALL_TO_ACTION_VISIBLE, CONTROLS_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import {
  getMapSliceForColorProductId,
  getSkuId,
  getFeatures,
  setSelectedPack,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import Product from '@tcp/core/src/components/features/browse/ProductDetail/molecules/Product/views/Product.view';
import ProductPrice from '../../../../features/browse/ProductDetail/molecules/ProductPrice/ProductPrice';
import styles from '../styles/FBTAddToBag.style';
import { getGAPageName } from '../../../../../utils/utils.web';

// to get Error Message displayed in case any error comes on Add To card
export const ErrorComp = (errorMessage, showAddToBagCTA) => {
  return errorMessage ? (
    <BodyCopy
      className={!showAddToBagCTA ? 'size-error' : 'default-error'}
      fontSize="fs12"
      component="div"
      fontFamily="secondary"
      fontWeight="regular"
      role="alert"
      aria-live="assertive"
    >
      <Image
        alt="Error"
        className="error-image"
        src={getIconPath('circle-alert-fill')}
        data-locator="productcustomizeform-error-icon"
      />
      <BodyCopy
        className="size-error-message"
        fontSize="fs12"
        component="div"
        fontFamily="secondary"
        fontWeight="regular"
      >
        {` ${errorMessage}`}
      </BodyCopy>
    </BodyCopy>
  ) : null;
};

const ProductDetailSectionSkeleton = () => {
  return (
    <div className="product-property-section">
      <LoaderSkelton height="40px" />
    </div>
  );
};

class ProductAddToBag extends React.Component {
  constructor(props) {
    super(props);
    this.isMobile = isClient() ? getViewportInfo().isMobile : null;
    this.isTablet = isClient() ? getViewportInfo().isTablet : null;
    this.ATC_BUTTON = 'add-to-bag-button';
    this.quickViewDrawerClass = 'quick-view-drawer-redesign';
    this.disableClass = 'disable-add-to-bag';
    this.state = {
      showStickyATC: (isClient() && getViewportInfo().isMobile) || false,
      IsCallToActionVisible: false,
      quantity: 1,
      isPickUpSelected: false,
    };
    this.mTabRef = React.createRef();
    this.mlistRef = React.createRef();
  }

  componentWillMount() {
    const {
      TCPMultipackProductMapping,
      selectedMultipack,
      TCPStyleQTY,
      setSelectedMultipack,
      isPDP,
      isModalOpen,
      updateFbtStatus,
      checkForOOSForVariant,
    } = this.props;

    /* eslint-disable no-extra-boolean-cast */
    const multipackToShow = !!selectedMultipack ? selectedMultipack : Number(TCPStyleQTY);

    if (isPDP && !isModalOpen) {
      setSelectedPack(setSelectedMultipack, multipackToShow, TCPMultipackProductMapping);
    }
    if (checkForOOSForVariant) {
      updateFbtStatus(false);
    } else {
      updateFbtStatus(true);
    }
  }

  componentDidMount() {
    const { showAddToBagCTA } = this.props;
    if (showAddToBagCTA) {
      this.setState({ IsCallToActionVisible: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  getButtonLabel = () => {
    const {
      fromBagPage,
      plpLabels,
      keepAlive,
      outOfStockLabels = {},
      isFavoriteEdit,
      addedToBagLabel = '',
      checkForOOSForVariant,
    } = this.props;
    const { addToBag, update, saveProduct } = plpLabels;
    let addToBagLabel = addToBag;

    if (fromBagPage) {
      addToBagLabel = update;
    } else if (isFavoriteEdit) {
      addToBagLabel = saveProduct;
    }
    return keepAlive || checkForOOSForVariant
      ? outOfStockLabels?.outOfStockCaps
      : addToBagLabel || addedToBagLabel;
  };

  skeletonCheck = (isLoading, isPDP, setInitialQuickViewLoad) => {
    return isLoading && !isPDP && setInitialQuickViewLoad;
  };

  colorListNullCheck = (showColorChips, colorList) => {
    return showColorChips && colorList && colorList.size > 0;
  };

  getPageName = (productInfo) => {
    let pageName = '';
    const productId = productInfo && productInfo?.generalProductId?.split('_')[0];
    const productName = productInfo && productInfo?.name?.toLowerCase();
    if (productId) {
      pageName = `product:${productId}:${productName}`;
    }
    return {
      pageName,
    };
  };

  getSizeList = (sizeList) => {
    const { selectSize, selectedSize } = this.props;
    return (
      <>
        <SelectBox
          customWidth="77px"
          customHeight="40px"
          downArrowBottom="17px"
          downArrowRight="12px"
          name="Size"
          id="Fbtsize"
          component={SelectBox}
          options={sizeList}
          onChange={selectSize}
          input={{ value: selectedSize.name }}
          isFbt
          forceSelect={sizeList.size > 1}
          placeholder="Select Size"
          meta
        />
      </>
    );
  };

  checkIfSizesAvailable = (sizeList) => sizeList && sizeList.size > 0;

  checkforProductDetailSkelton = (isLoading, isPDP, setInitialQuickViewLoad) => {
    return isLoading && !isPDP && setInitialQuickViewLoad;
  };

  renderSizeList = (sizeList, colorFitSizeDisplayNames, errorMessage) => {
    const {
      isErrorMessageDisplayed,
      isLoading,
      isSKUValidated,
      isPDP,
      isStyleWith,
      setInitialQuickViewLoad,
      isNewQVEnabled,
      isNewPDPEnabled,
    } = this.props;
    return this.checkforProductDetailSkelton(isLoading, isPDP, setInitialQuickViewLoad) ? (
      <ProductDetailSectionSkeleton />
    ) : (
      this.checkIfSizesAvailable(sizeList) && (
        <div className={`size-selector ${isStyleWith ? 'size-selector-atb' : ''}`}>
          {this.getSizeList(sizeList, colorFitSizeDisplayNames, errorMessage)}
          {!isNewQVEnabled || isNewPDPEnabled ? (
            <div className="please-enter-an-error">
              {(isErrorMessageDisplayed || isSKUValidated) && ErrorComp(errorMessage)}
            </div>
          ) : null}
        </div>
      )
    );
  };

  isSelectedProductCTA = () => {
    const { currentProduct, showAddedToBagCta, generalProductId } = this.props;
    const { productId, unbxdProdId } = currentProduct || {};
    return (
      (showAddedToBagCta && showAddedToBagCta === generalProductId) ||
      showAddedToBagCta === productId ||
      showAddedToBagCta === unbxdProdId
    );
  };

  showAddedToBagCTA = (
    isProductCTA,
    addBtnMsg,
    outfitCarouselState,
    isEnabledOutfitAddedCTA,
    isPDPAddedToBag
  ) =>
    isProductCTA &&
    addBtnMsg &&
    !outfitCarouselState &&
    (isEnabledOutfitAddedCTA || isPDPAddedToBag);

  getColor = (product, selectedColorProductId) => {
    return (
      (product &&
        product.colorFitsSizesMap &&
        product.colorFitsSizesMap
          .filter((productTile) => {
            return productTile.colorDisplayId === selectedColorProductId;
          })
          .map((productTile) => productTile.color.name)
          .join('')) ||
      ''
    );
  };

  getProdCategory = (category) => {
    let prodCategory = '';
    if (category === 'accessories') {
      prodCategory = 'accessories';
    } else if (category === 'shoes') {
      prodCategory = 'shoes';
    } else {
      prodCategory = 'apparel';
    }
    return prodCategory;
  };

  getTrackProps = (props) => {
    const {
      currentProduct = {},
      selectedSize = {},
      selectedFit,
      selectedColor = {},
      pageData,
      itemBrand,
      currency,
      isPDP,
      isOutfitPage,
    } = props;
    const {
      generalProductId,
      name,
      offerPrice,
      listPrice,
      colorFitsSizesMap,
      reviewsCount,
      ratings,
      isGiftCard,
      productId,
    } = currentProduct;
    const prodId = generalProductId && generalProductId.split('_')[0];
    const productName = name && name.toLowerCase();

    let trackingCategories = {};
    const { pageName = '' } = pageData || {};
    const [pageType, departmentList, categoryList, listingSubCategory] = pageName.split(':');
    if (pageType === 'browse') {
      trackingCategories = {
        departmentList,
        categoryList,
        listingSubCategory,
      };
    }

    const addToBagTrackProduct = {
      id: prodId,
      colorId: generalProductId,
      brand: itemBrand,
      name: productName,
      price: offerPrice,
      CurrCurrency: currency,
      listPrice,
      prodColor: this.getColor(currentProduct, generalProductId),
      extPrice: listPrice,
      pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
      size: selectedSize.name,
      sku: getSkuId(colorFitsSizesMap, selectedColor.name, selectedFit, selectedSize.name),
      reviews: reviewsCount,
      rating: ratings,
      category: this.getProdCategory(categoryList),
      productFeatures: getFeatures(colorFitsSizesMap, generalProductId, isGiftCard),
      fromPDP: isPDP,
      fromPageName: isClient() ? getGAPageName(window.location.pathname) : '',
      isOutfitPage,
      ...addRecommendationsObj(productId),
    };
    return {
      addToBagTrackProduct,
      pageShortName: productId ? `product:${productId}:${productName}` : '',
      productId,
      outfitPageShortName: productId ? `outfit:${productId}:${productName}` : '',
      trackingCategories,
    };
  };

  getProductId = (products) => {
    return products && products.length && products[0].sku;
  };

  checkIfSizeIsDisabled = () => {
    const { selectedSize, sizeList } = this.props;
    const disabledSizeArray = sizeList.filter((sizeItem) => {
      return (
        (sizeItem && sizeItem.displayName) === (selectedSize && selectedSize.name) &&
        sizeItem &&
        sizeItem.disabled
      );
    });
    return disabledSizeArray && disabledSizeArray.length > 0;
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  renderAddToBagButton = (uniqueKeyId, analyticsData, className) => {
    const {
      displayErrorMessage,
      handleFormSubmit,
      keepAlive,
      selectedSize,
      unbxdId,
      selectedQuantity,
      isLoading,
      formValues,
      setAnalyticsData,
      checkForOOSForVariant,
      isNewQVEnabled,
      isNewPDPEnabled,
      isComplete,
      addedToBagLabel,
    } = this.props;

    const { products } = analyticsData;
    const productId = this.getProductId(products);
    const productVariant = products && products.length && products[0].colorId;
    const selectedSizeValue = selectedSize?.name || formValues?.[0]?.size;
    return (
      <div>
        {!isLoading && (
          <Button
            type="submit"
            uniqueKey={uniqueKeyId}
            className={`${className} ${isNewQVEnabled ? this.quickViewDrawerClass : ''} ${
              !selectedSizeValue ? this.disableClass : ''
            }`}
            disabled={
              !isLoading
                ? keepAlive || checkForOOSForVariant || (isNewPDPEnabled && !selectedSizeValue)
                : isLoading
            }
            unbxdattr="AddToCart"
            unbxdparam_sku={productId}
            unbxdparam_variant={productVariant}
            unbxdparam_qty={selectedQuantity}
            unbxdparam_requestId={unbxdId}
            onClick={(e) => {
              e.preventDefault();
              if (!selectedSizeValue) {
                displayErrorMessage(true);
              } else {
                handleFormSubmit(selectedSizeValue);
              }
              setAnalyticsData(analyticsData);
            }}
          >
            {this.getButtonLabel()}
          </Button>
        )}

        {(isLoading || isComplete) && (
          <Button type="button" className={className}>
            <div className="circle-loader-wrapper">
              <div className={`circle-loader ${isComplete ? 'load-complete' : ''}`}>
                <div className="checkmark draw"> </div>
              </div>
              <span
                className={`${isComplete ? 'load-complete-label-done' : 'load-complete-label'}`}
              >
                {addedToBagLabel}
              </span>
            </div>
          </Button>
        )}
      </div>
    );
  };

  getAddToBagButton = (uniqueKeyId, analyticsData, className) => {
    const { isLoading, isPDP, addBtnMsg, setInitialQuickViewLoad } = this.props;
    if (isLoading && !isPDP && setInitialQuickViewLoad) {
      return <ProductDetailSectionSkeleton />;
    }
    return !addBtnMsg || !this.isSelectedProductCTA() ? (
      this.renderAddToBagButton(uniqueKeyId, analyticsData, className)
    ) : (
      <></>
    );
  };

  getAddToBagButtonAction = (uniqueKeyId, clickDataAnalytics, className) => {
    return this.getAddToBagButton(uniqueKeyId, clickDataAnalytics, className);
  };

  selectedVal = (setInitialTCPStyleQty, TCPStyleQTY) => {
    const { fromBagPage } = this.props;
    return !fromBagPage && setInitialTCPStyleQty ? setInitialTCPStyleQty : TCPStyleQTY;
  };

  getToolTipDirection = (isDisabled, isPillSelected) => {
    if (isDisabled && !isPillSelected && !this.isMobile && !this.isTablet) {
      return 'center-for-mpack';
    }
    return '';
  };

  renderPillsWithToolTip = (params) => {
    const {
      item,
      multipackToSelect,
      mpackBg,
      direction,
      isDisabled,
      index,
      singlePack,
      pdpLabels,
      getToolTipLabel,
      newQVDesign,
      disableSwappingMpackPills,
      softDisablePill,
    } = params;
    const { isNewPDPEnabled } = this.props;
    return (
      <ReactTooltip
        fontFamily="secondary"
        message={getToolTipLabel}
        minWidth="179px"
        direction={direction}
      >
        <BodyCopy
          component="li"
          key={item}
          className={item === multipackToSelect ? `${mpackBg}-wrapper` : null}
        >
          <button
            className={`${disableSwappingMpackPills ? 'mpack-btn' : 'mpack-btn-swap'} ${
              newQVDesign ? this.quickViewDrawerClass : ''
            } ${softDisablePill} ${item === multipackToSelect ? `${mpackBg} ` : ''}`}
            onClick={(e) => this.onClickMultipackBtn(e, index, item, singlePack)}
            disabled={isDisabled}
          >
            {isNewPDPEnabled ? `${item}` : `${item}`.toUpperCase()}
            {item !== singlePack
              ? pdpLabels &&
                `${
                  isNewPDPEnabled
                    ? pdpLabels.lblPdpMultipackPack
                    : `${pdpLabels.lblPdpMultipackPack}`.toUpperCase()
                }`
              : ''}
          </button>
        </BodyCopy>
      </ReactTooltip>
    );
  };

  renderPillsWithoutToolTip = (params) => {
    const {
      item,
      multipackToSelect,
      mpackBg,
      disableSwappingMpackPills,
      softDisablePill,
      index,
      singlePack,
      isDisabled,
      pdpLabels,
      newQVDesign,
    } = params;
    const { isNewPDPEnabled } = this.props;
    return (
      <BodyCopy
        component="li"
        key={item}
        className={item === multipackToSelect ? `${mpackBg}-wrapper` : null}
      >
        <button
          className={`${disableSwappingMpackPills ? 'mpack-btn' : 'mpack-btn-swap'} ${
            newQVDesign ? this.quickViewDrawerClass : ''
          } ${softDisablePill} ${item === multipackToSelect ? `${mpackBg} ` : ''}`}
          onClick={(e) => this.onClickMultipackBtn(e, index, item, singlePack)}
          disabled={isDisabled}
        >
          {isNewPDPEnabled ? `${item}` : `${item}`.toUpperCase()}
          {item !== singlePack
            ? pdpLabels &&
              `${
                isNewPDPEnabled
                  ? pdpLabels.lblPdpMultipackPack
                  : `${pdpLabels.lblPdpMultipackPack}`.toUpperCase()
              }`
            : ''}
        </button>
      </BodyCopy>
    );
  };

  renderAddToBagSection = (isBundleProduct, storeId, errorOnHandleSubmit) => {
    const { pageShortName, productId, addToBagTrackProduct, trackingCategories } =
      this.getTrackProps(this.props);
    const { isNewQVEnabled } = this.props;
    const analyticsData = {
      eventName: 'cart add',
      pageShortName,
      products: [addToBagTrackProduct],
      customEvents: ['event61'],
      pageNavigationText: '',
      storeId,
      ...trackingCategories,
    };
    return (
      <Row
        fullBleed
        className={`${errorOnHandleSubmit && !isNewQVEnabled ? 'product-size-error' : ''}`}
      >
        <Col colSize={{ small: 6, medium: 8, large: 12 }} className="outfit-button-wrapper">
          <div className="button-wrapper">
            {this.getAddToBagButtonAction(productId, analyticsData, this.ATC_BUTTON)}
          </div>
        </Col>
      </Row>
    );
  };

  /**
   * This just holds the logic for rendering a UX timer
   */
  setCTAPerformanceTimer = () => {
    const { IsCallToActionVisible } = this.state;
    return IsCallToActionVisible ? <RenderPerf.Measure name={CALL_TO_ACTION_VISIBLE} /> : null;
  };

  getProductSummary = (keepAlive, handleMetaProps, isInWishList) => {
    const {
      productDetailsPDP,
      productInfo,
      currencySymbol,
      pdpLabels,
      currencyAttributes,
      onAddItemToFavorites,
      isLoggedIn,
      outOfStockLabels,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      isLoading,
      wishList,
      hidePriceDisplay,
      isPerUnitPriceEnabled,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      TCPStyleQTY,
      deleteFavItemInProgressFlag,
      checkForOOSForVariant,
      currentProduct: { colorFitsSizesMap },
      selectedColorProductId,
      getGiftCardValue,
      ...otherProps
    } = this.props;
    const { isPlcc } = otherProps;
    const currentColorEntry =
      getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId) || {};
    const { colorDisplayId, colorProductId } = currentColorEntry || {};
    const { isGiftCard, primaryBrand } = productInfo;
    return (
      <div className="product-summary-wrapper">
        <Product
          {...otherProps}
          isGiftCard={isGiftCard}
          productDetails={productDetailsPDP}
          currencySymbol={currencySymbol}
          selectedColorProductId={colorProductId}
          currencyAttributes={currencyAttributes}
          onAddItemToFavorites={onAddItemToFavorites}
          isLoggedIn={isLoggedIn}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          className="hide-on-mobile"
          AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
          removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
          isLoading={isLoading}
          isInWishList={isInWishList}
          isHasPlcc={isPlcc}
          hidePriceDisplay={hidePriceDisplay}
          pdpLabels={pdpLabels}
          isPerUnitPriceEnabled={isPerUnitPriceEnabled}
          isBrierleyPromoEnabled={isBrierleyPromoEnabled}
          TCPStyleQTY={TCPStyleQTY}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          wishList={wishList}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          isAfterPayDisplay={!checkForOOSForVariant}
          isOutfitPage={false}
          colorDisplayId={colorDisplayId}
          primaryBrand={primaryBrand}
        />
        {isGiftCard ? (
          <div className="product-price-desktop-view">
            {isLoading ? (
              <LoaderSkelton />
            ) : (
              <ProductPrice
                offerPrice={parseInt(getGiftCardValue, 10)}
                listPrice={parseInt(getGiftCardValue, 10)}
                hideItemProp // prop is used to hide unwanted duplicate itemProp as we have duplicate ProductPrice component for web and mobile on PDP pages
                currencySymbol={currencySymbol}
                currencyAttributes={currencyAttributes}
                isGiftCard={isGiftCard}
                isPlcc={isPlcc}
                pdpLabels={pdpLabels}
                isPerUnitPriceEnabled={isPerUnitPriceEnabled}
                TCPStyleQTY={TCPStyleQTY}
                wishList={wishList}
                isAfterPayDisplay
              />
            )}
          </div>
        ) : null}
        {isGiftCard ? (
          <div className="product-price-mobile-view">
            {isLoading ? (
              <LoaderSkelton />
            ) : (
              <ProductPrice
                listPrice={parseInt(getGiftCardValue, 10)}
                offerPrice={parseInt(getGiftCardValue, 10)}
                currencyAttributes={currencyAttributes}
                currencySymbol={currencySymbol}
                isPlcc={isPlcc}
                isPerUnitPriceEnabled={isPerUnitPriceEnabled}
                pdpLabels={pdpLabels}
                TCPStyleQTY={TCPStyleQTY}
                isAfterPayDisplay
              />
            )}
          </div>
        ) : null}
      </div>
    );
  };

  onClickMultipackBtn = (e, index, item, singlePack) => {
    const {
      setSelectedMultipack,
      getDetails,
      currentProduct,
      onQuickViewOpenClick,
      fromPDP,
      isPDP,
      closePickupModal,
      setInitialTCPStyleQty,
      resetProductDetailsDynamicData,
      getQickViewSingleLoad,
      getDisableSelectedTab,
      isPickup,
      onPickUpOpenClick,
      fromBagPage,
      productInfoFromBag,
      availableTCPmapNewStyleId,
    } = this.props;
    e.preventDefault();
    const targetClassList = e.target?.classList;
    const targetClassArray = targetClassList ? [...targetClassList] : [];
    if (targetClassArray?.indexOf('multipack-bg') < 0) {
      onTabClickDetails({
        setSelectedMultipack,
        getDetails,
        currentProduct,
        onQuickViewOpenClick,
        fromPDP,
        isPDP,
        e,
        item,
        singlePack,
        setInitialTCPStyleQty,
        resetProductDetailsDynamicData,
        getQickViewSingleLoad,
        getDisableSelectedTab,
        closePickupModal,
        isPickup,
        onPickUpOpenClick,
        fromBagPage,
        productInfoFromBag,
        availableTCPmapNewStyleId,
        fromPills: true,
      });
    }
  };

  getProductCartError = (addToBagErrorId) => {
    const { currentProduct, errorOnHandleSubmit } = this.props;
    if (!addToBagErrorId) {
      return errorOnHandleSubmit;
    }
    return addToBagErrorId && addToBagErrorId === currentProduct.unbxdProdId
      ? errorOnHandleSubmit
      : '';
  };

  checkInWishlist = (wishList, productId) =>
    wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;

  productDetailSkeletonCheck() {
    const { isLoading, isPDP, setInitialQuickViewLoad } = this.props;
    return isLoading && !isPDP && setInitialQuickViewLoad;
  }

  // eslint-disable-next-line
  render() {
    const {
      plpLabels,
      className,
      showAddToBagCTA,
      isBundleProduct,
      isATBErrorMessageDisplayed,
      storeId,
      addToBagErrorId,
      isNewQVEnabled,
      currentProduct: { colorFitsSizesMap },
      selectedColorProductId,
      isNewPDPEnabled,
      keepAlive,
      pdpProductDetails,
      wishList,
    } = this.props;
    const currentColorEntry =
      getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId) || {};
    const { colorDisplayId } = currentColorEntry || {};
    const isInWishList = this.checkInWishlist(
      wishList,
      colorDisplayId || pdpProductDetails?.currentProduct?.generalProductId
    );
    const addToBagError = this.getProductCartError(addToBagErrorId);
    let { sizeList, colorFitSizeDisplayNames } = this.props;
    colorFitSizeDisplayNames = {
      color: 'Color',
      fit: 'Fit',
      size: 'Size',
      ...colorFitSizeDisplayNames,
    };

    if (sizeList) {
      sizeList = fromJS(sizeList);
    }

    const { errorMessage } = plpLabels;
    return (
      <form className={className} noValidate>
        <Row className="edit-form-css">
          <Col colSize={{ small: 12, medium: 12, large: 12 }} className="edit-form-css-width">
            <div className="select-value-wrapper">
              <div className={`${isNewPDPEnabled ? 'product-summary-new-pdp' : ''}`}>
                {this.getProductSummary(keepAlive, true, isInWishList)}
              </div>
              {this.renderSizeList(sizeList, colorFitSizeDisplayNames, errorMessage, addToBagError)}
            </div>
            <RenderPerf.Measure name={CONTROLS_VISIBLE} />
          </Col>
        </Row>
        <div className="please-enter-an-error">
          {isATBErrorMessageDisplayed &&
            !isNewPDPEnabled &&
            ErrorComp(addToBagError, showAddToBagCTA)}
        </div>
        {showAddToBagCTA && !isNewQVEnabled && (
          <>
            {this.renderAddToBagSection(isBundleProduct, storeId, addToBagError)}
            {this.setCTAPerformanceTimer()}
          </>
        )}
      </form>
    );
  }
}

export default compose(
  connect((state, props) => {
    const formName = props.customFormName || PRODUCT_ADD_TO_BAG;
    return {
      form: `${formName}-${props.generalProductId}`,
      enableReinitialize: true,
    };
  }),
  reduxForm()
)(withStyles(ProductAddToBag, styles));

ProductAddToBag.propTypes = {
  fromBagPage: PropTypes.bool.isRequired,
  plpLabels: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  keepAlive: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  isFavoriteEdit: PropTypes.bool.isRequired,
  currentProduct: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  selectedColorProductId: PropTypes.number.isRequired,
  isOutfitPage: PropTypes.bool.isRequired,
  selectColor: PropTypes.string.isRequired,
  isGiftCard: PropTypes.bool.isRequired,
  showColorChips: PropTypes.bool.isRequired,
  quickViewColorSwatchesCss: PropTypes.string.isRequired,
  isPDP: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
  imagesByColor: PropTypes.shape({}).isRequired,
  relatedSwatchImages: PropTypes.shape({}).isRequired,
  isMultiItemQVModal: PropTypes.bool.isRequired,
  itemBrand: PropTypes.string.isRequired,
  selectFit: PropTypes.func.isRequired,
  selectedFit: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  hideAlternateSizes: PropTypes.shape([]).isRequired,
  onCloseClick: PropTypes.func.isRequired,
  isFromBagProductSfl: PropTypes.bool.isRequired,
  isPickup: PropTypes.bool.isRequired,
  sizeChartLinkVisibility: PropTypes.number.isRequired,
  isErrorMessageDisplayed: PropTypes.bool.isRequired,
  selectSize: PropTypes.func.isRequired,
  isDisableZeroInventoryEntries: PropTypes.bool.isRequired,
  sizeChartDetails: PropTypes.string.isRequired,
  isSKUValidated: PropTypes.bool.isRequired,
  onQuantityChange: PropTypes.func.isRequired,
  quantityList: PropTypes.shape([]).isRequired,
  fitChanged: PropTypes.bool.isRequired,
  currencySymbol: PropTypes.string.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  currencyAttributes: PropTypes.shape({}).isRequired,
  isCanada: PropTypes.bool.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  listPrice: PropTypes.string.isRequired,
  offerPrice: PropTypes.string.isRequired,
  displayErrorMessage: PropTypes.func.isRequired,
  handleFormSubmit: PropTypes.func.isRequired,
  setAnalyticsData: PropTypes.func.isRequired,
  showAddedToBagCta: PropTypes.bool.isRequired,
  revertAtbButton: PropTypes.func.isRequired,
  addedToBagLabel: PropTypes.string.isRequired,
  selectedSize: PropTypes.string.isRequired,
  unbxdId: PropTypes.string.isRequired,
  selectedQuantity: PropTypes.number.isRequired,
  hideStickyABTest: PropTypes.func.isRequired,
  isBundleProduct: PropTypes.bool.isRequired,
  errorOnHandleSubmit: PropTypes.func.isRequired,
  showAddToBagCTA: PropTypes.bool.isRequired,
  alternateSizes: PropTypes.shape([]).isRequired,
  isATBErrorMessageDisplayed: PropTypes.bool.isRequired,
  quickViewPickup: PropTypes.func.isRequired,
  outfitCarouselAddedToBagMsgDispatch: PropTypes.func.isRequired,
  storeId: PropTypes.string.isRequired,
  sizeList: PropTypes.shape([]).isRequired,
  fitList: PropTypes.shape([]).isRequired,
  colorList: PropTypes.shape([]).isRequired,
  colorFitSizeDisplayNames: PropTypes.shape([]).isRequired,
  addedTobagMsgDispatch: PropTypes.shape([]).isRequired,
  addBtnMsg: PropTypes.bool.isRequired,
  hasLoyaltyAbTestFlag: PropTypes.bool,
  showOutfitCarouselAddedCTA: PropTypes.bool,
  outfitCarouselState: PropTypes.bool,
  isOutfitCarousel: PropTypes.bool,
  isEnabledOutfitAddedCTA: PropTypes.bool,
  showAddtoBagDrawer: PropTypes.bool,
  revertCarouselAtbButton: PropTypes.func.isRequired,
  generalProductId: PropTypes.string,
  productDetailsToPath: PropTypes.string,
  closeModal: PropTypes.func.isRequired,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  TCPStyleType: PropTypes.string.isRequired,
  setSelectedMultipack: PropTypes.func,
  isStyleWith: PropTypes.bool,
  pdpLabels: PropTypes.instanceOf(Object).isRequired,
  getDetails: PropTypes.shape({}).isRequired,
  onQuickViewOpenClick: PropTypes.func.isRequired,
  fromPDP: PropTypes.bool.isRequired,
  selectedMultipack: PropTypes.string.isRequired,
  TCPStyleQTY: PropTypes.number.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  isPhysicalMpackEnabled: PropTypes.bool,
  imageSwatchesToDisplay: PropTypes.shape([]).isRequired,
  isImageSwatchDisabled: PropTypes.bool,
  isNewQVEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  isABTestLoaded: PropTypes.bool,
  setStickyFooterState: PropTypes.func.isRequired,
  checkForOOSForVariant: PropTypes.func.isRequired,
  isRecommendationAvailable: PropTypes.bool,
  closePickupModal: PropTypes.func.isRequired,
  otherAvailableSizeLabel: PropTypes.string,
  selectColorToHover: PropTypes.func,
  selectColorToHoverOut: PropTypes.func,
  TCPMultipackProductMapping: PropTypes.shape([]).isRequired,
  singlePageLoad: PropTypes.bool.isRequired,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  resetProductDetailsDynamicData: PropTypes.shape([]).isRequired,
  setInitialQuickViewLoad: PropTypes.bool.isRequired,
  getQickViewSingleLoad: PropTypes.func.isRequired,
  addToBagErrorId: PropTypes.string,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  newColorList: PropTypes.shape([]),
  initialFormValues: PropTypes.shape({}).isRequired,
  initialValues: PropTypes.shape({}).isRequired,
  formValues: PropTypes.shape({}),
  onPickUpOpenClick: PropTypes.func,
  hideMultipackPills: PropTypes.bool,
  disableSwappingMpackPills: PropTypes.bool,
  productInfoFromBag: PropTypes.shape({}),
  availableTCPmapNewStyleId: PropTypes.shape([]),
  isAfterPayDisplay: PropTypes.bool,
  pdpUrlFromBag: PropTypes.string,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  goToPDPPage: PropTypes.func,
  /** User's preferred store information */
  userDefaultStore: PropTypes.shape({
    basicInfo: PropTypes.shape({
      /** store id identifier */
      id: PropTypes.string.isRequired,
      /** store Name */
      storeName: PropTypes.string.isRequired,
    }).isRequired,
  }),
  showPickupInfo: PropTypes.bool,
  labels: PropTypes.shape({
    lbl_Product_pickup_BOPIS_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: PropTypes.string,
    lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: PropTypes.string,
    lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOSS_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_BOSS_ONLY_AVAILABLE: PropTypes.string,
    lbl_Product_pickup_FIND_STORE: PropTypes.string,
    lbl_Product_pickup_FREE_SHIPPING: PropTypes.string,
    lbl_Product_pickup_NO_MIN_PURCHASE: PropTypes.string,
    lbl_Product_pickup_PICKUP_IN_STORE: PropTypes.string,
    lbl_Product_pickup_PRODUCT_BOPIS: PropTypes.string,
    lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: PropTypes.string,
    lbl_Product_pickup_CHANGE_STORE: PropTypes.string,
  }),
  onPickupClickAddon: PropTypes.func,
  socialProofMessage: PropTypes.string,
  isFamilyOutfitEnabled: PropTypes.bool,
  pdpProductDetails: PropTypes.shape({}),
  onAddItemToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  wishList: PropTypes.func,
  resetCountUpdate: PropTypes.func,
  countUpdated: PropTypes.bool,
  isPerUnitPriceEnabled: PropTypes.bool,
  isBrierleyPromoEnabled: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  productDetailsPDP: PropTypes.shape({}),
  productInfo: PropTypes.shape({}),
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  hidePriceDisplay: PropTypes.bool,
  getGiftCardValue: PropTypes.string,
  isComplete: PropTypes.bool,
};

ProductAddToBag.defaultProps = {
  productDetailsToPath: '',
  generalProductId: '',
  isEnabledOutfitAddedCTA: false,
  isOutfitCarousel: false,
  outfitCarouselState: false,
  showAddtoBagDrawer: false,
  showOutfitCarouselAddedCTA: false,
  isPDP: false,
  hasLoyaltyAbTestFlag: false,
  setSelectedMultipack: () => {},
  isStyleWith: false,
  isPhysicalMpackEnabled: true,
  isImageSwatchDisabled: false,
  isNewQVEnabled: false,
  isNewPDPEnabled: false,
  isABTestLoaded: false,
  isRecommendationAvailable: false,
  otherAvailableSizeLabel: '',
  selectColorToHover: () => {},
  selectColorToHoverOut: () => {},
  addToBagErrorId: '',
  newColorList: [],
  formValues: {},
  onPickUpOpenClick: () => {},
  hideMultipackPills: false,
  disableSwappingMpackPills: false,
  productInfoFromBag: {},
  availableTCPmapNewStyleId: [],
  isAfterPayDisplay: false,
  pdpUrlFromBag: '',
  goToPDPPage: () => {},
  userDefaultStore: {
    basicInfo: {
      id: '',
      storeName: '',
    },
  },
  showPickupInfo: false,
  labels: {
    lbl_Product_pickup_BOPIS_AVAILABLE: 'Pick up TODAY!',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: 'husky',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: 'plus',
    lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: 'slim',
    lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: 'Item available for pickup TODAY',
    lbl_Product_pickup_BOSS_AVAILABLE: 'Or choose NO RUSH Pickup ',
    lbl_Product_pickup_BOSS_ONLY_AVAILABLE: 'Choose NO RUSH Pickup ',
    lbl_Product_pickup_FIND_STORE: 'Find In Store',
    lbl_Product_pickup_FREE_SHIPPING: 'FREE Shipping Every Day!',
    lbl_Product_pickup_NO_MIN_PURCHASE: 'No Minimum Purchase Required.',
    lbl_Product_pickup_PICKUP_IN_STORE: 'PICK UP IN STORE',
    lbl_Product_pickup_PRODUCT_BOPIS: 'Select Store',
    lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: 'Select Store',
    lbl_Product_pickup_CHANGE_STORE: '(Change Store)',
    lbl_Product_pickup_UNAVAILABLE_IN_STORES: 'UNAVAILABLE IN STORES',
  },
  onPickupClickAddon: () => {},
  socialProofMessage: '',
  isFamilyOutfitEnabled: false,
  pdpProductDetails: {},
  onAddItemToFavorites: () => {},
  isLoggedIn: false,
  wishList: () => {},
  resetCountUpdate: () => {},
  countUpdated: false,
  isPerUnitPriceEnabled: false,
  isBrierleyPromoEnabled: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  productDetailsPDP: {},
  productInfo: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  hidePriceDisplay: false,
  getGiftCardValue: '',
  isComplete: false,
};

export { ProductAddToBag as ProductAddToBagVanilla };
