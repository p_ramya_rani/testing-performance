import { css } from 'styled-components';

export default css`
  .offer-type__label {
    font-size: ${(props) => (props.theme.isGymboree ? '25px' : '30px')};
    padding-top: 20px;
    ::after {
      content: '';
      height: 2px;
      width: 100px;
      background: ${(props) =>
        props.theme.isGymboree
          ? props.theme.colorPalette.primary.main
          : props.theme.colors.TEXT.DARKERBLUE};
      margin: 12px auto 32px;
      display: block;
      @media ${(props) => props.theme.mediaQuery.medium} {
        margin: 16px auto 32px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      font-size: 15px;
    }
  }
  .off-text {
    font-size: 66.5px;
    margin-top: -30px;
    line-height: 1.4;
    display: inline-block;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      font-size: 44px;
      font-weight: 400;
    }
  }

  .super-text {
    > span {
      line-height: ${(props) => props.theme.typography.fontSizes.fs18};
      vertical-align: top;
      padding-top: 15px;
      display: inline-block;
    }
  }
  .get-text,
  .ten-text {
    font-size: 66.5px;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      font-size: 44px;
    }
  }
  .first-text {
    display: block;
    text-align: center;
    font-size: 44px;
    line-height: 54px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: 54px;
      line-height: 66px;
      margin-top: 32px;
    }
  }
  .know-text {
    display: inline-block;
    font-size: 56px;
    line-height: 68px;
    position: relative;
    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: 70px;
      line-height: 86px;
      margin-top: -15px;
    }
  }
  .flash-text {
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-right: 3px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-left: 0;
      margin-right: 0px;
    }
  }
  .dollar-text {
    vertical-align: top;
    line-height: 66.5px;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    padding: 0px 0 4px;
    .offer-type__label {
      ::after {
        margin-bottom: 0;
      }
    }
  }
  .sign-up__label {
    display: none;
  }
`;
