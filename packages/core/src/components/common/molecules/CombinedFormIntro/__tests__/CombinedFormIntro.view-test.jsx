import React from 'react';
import { shallow } from 'enzyme';
import { CombinedFormIntroVanilla } from '../views/CombinedFormIntro.view';

describe('CombinedConfirm Component', () => {
  it('should renders correctly when subscription is success', () => {
    const props = {
      className: 'classname',
      singleSignUpLabels: {
        signUpHeading: 'signUpHeading',
        signUpGetText: 'signUpGetText',
        signUpTenText: 'signUpTenTeText',
        signUpOffText: 'signUpOffText',
      },
    };
    const component = shallow(<CombinedFormIntroVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
