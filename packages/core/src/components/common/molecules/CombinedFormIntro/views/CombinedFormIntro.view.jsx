// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { isGymboree } from '@tcp/core/src/utils';
import { Col } from '@tcp/core/src/components/common/atoms';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Styles from '../styles/CombinedFormIntro.style';

const CombinedFormIntro = ({ className, singleSignUpLabels }) => (
  <Fragment>
    <Col
      colSize={{ small: 6, medium: 8, large: 12 }}
      ignoreGutter={{ large: true }}
      className={className}
    >
      <section id="combined-sign-up-modal-form-intro-view">
        <BodyCopy
          fontFamily="primary"
          fontWeight="black"
          textAlign="center"
          className="offer-type__label"
          color="gray.900"
        >
          {singleSignUpLabels.signUpHeading}
        </BodyCopy>
        <BodyCopy fontFamily="primary" className="flash-text" textAlign="center" component="div">
          <BodyCopy
            fontSize="fs48"
            component="span"
            className={!isGymboree() ? 'get-text' : 'first-text'}
            color="primary.main"
          >
            {singleSignUpLabels.signUpGetText}
          </BodyCopy>
          {!isGymboree() && (
            <>
              <BodyCopy
                fontSize="fs36"
                component="span"
                fontWeight="black"
                className="dollar-text"
                color="primary.main"
              >
                {singleSignUpLabels.signUpDollarText}
              </BodyCopy>
              <BodyCopy
                fontSize="fs48"
                component="span"
                fontWeight="black"
                className="ten-text"
                color="primary.main"
              >
                {singleSignUpLabels.signUpTenText}
              </BodyCopy>
            </>
          )}
          <BodyCopy
            fontSize="fs48"
            textAlign="center"
            fontWeight="black"
            className={!isGymboree() ? 'off-text' : 'know-text'}
            color="primary.main"
          >
            {singleSignUpLabels.signUpOffText}
          </BodyCopy>
        </BodyCopy>
      </section>
    </Col>
  </Fragment>
);

CombinedFormIntro.propTypes = {
  singleSignUpLabels: PropTypes.shape({}),
  className: PropTypes.string,
};

CombinedFormIntro.defaultProps = {
  singleSignUpLabels: {},
  className: '',
};

export { CombinedFormIntro as CombinedFormIntroVanilla };
export default withStyles(CombinedFormIntro, Styles);
