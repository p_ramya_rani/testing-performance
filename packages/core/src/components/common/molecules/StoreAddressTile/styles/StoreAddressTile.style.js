// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components';
import { listingHeader } from '../views/prop-types';

export const TileHeader = styled.div`
  .store-listing-header {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }
  .title-one {
    span {
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
      text-transform: capitalize;
    }
  }
  .title-two {
    flex: 1;
    span {
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
    }
  }
  .store-name {
    &--listing {
      text-transform: capitalize;
    }
    &--details {
      margin-top: 0;
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
      color: ${props => props.theme.colors.TEXT.DARK};
      text-transform: capitalize;
      &-btn {
        font-family: ${props => props.theme.fonts.secondaryFontBlackFamily};
        font-weight: ${props => props.theme.fonts.fontWeight.black};
        line-height: ${props => props.theme.fonts.lineHeight.normal};
        font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy4}px;
        padding: 0;
        border: 0;
        background: none;
        cursor: pointer;
        outline: inherit;
        text-transform: capitalize;
        text-align: left;
      }
    }
  }
`;

export const TileFooter = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-top: ${props =>
    props.variation === 'detail'
      ? props.theme.spacing.ELEM_SPACING.LRG
      : props.theme.spacing.ELEM_SPACING.XS};
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  margin-top: auto;
`;

export const TileBody = styled.div`
  .address-wrapper {
    @media ${props => props.theme.mediaQuery.large} {
      display: ${props => (props.variation === 'listing' ? 'flex' : 'block')};
      align-items: center;
    }
  }
  .store-type {
    text-transform: uppercase;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    &__marker {
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    }
  }

  .address-meta {
    display: ${props => (props.variation === 'detail' ? 'flex' : 'block')};
    align-items: center;

    &__left {
      flex: ${props => (props.variation === 'detail' ? 1 : 'none')};
    }
    &__right {
      flex: none;
    }
  }
  .address-meta__nodisplay {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }
`;

export const FavStore = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
  .favorite-store-icon {
    width: 30px;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }
`;

export default css`
  display: flex;
  flex-direction: column;
  box-sizing: bored-box;
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  border-bottom: 1px solid ${props => props.theme.colors.PRIMARY.LIGHTGRAY};

  &.address-tile__selectedStore {
    border: solid 1px ${props => props.theme.colorPalette.gray[500]};
    background-color: ${props => props.theme.colorPalette.gray[300]};
  }

  .brand-store {
    display: flex;
    align-items: center;
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    height: ${props => props.theme.spacing.ELEM_SPACING.MED};

    @media ${props => props.theme.mediaQuery.large} {
      ${props => (props.variation === listingHeader ? 'padding-top: 0' : '')};
      ${props => (props.variation === listingHeader ? 'height: auto' : '')};
    }

    &__image {
      height: 22px;
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
    }

    ${props =>
      (props.variation === listingHeader ||
        props.variation === 'detail' ||
        props.type === 'storelocations') &&
      props.store &&
      !props.store.isGym
        ? `display: none;`
        : ''}
  }

  .address-details {
    font-style: normal;
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    margin-right: ${props =>
      props.variation === 'listing' ? props.theme.spacing.ELEM_SPACING.XL : 0};
    text-transform: capitalize;
  }

  .listing-header {
    .title {
      @media ${props => props.theme.mediaQuery.smallOnly} {
        margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
      }
      @media ${props => props.theme.mediaQuery.largeOnly} {
        margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
      }
      span {
        margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
      }
      &__one {
        text-transform: capitalize;
      }
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      display: flex;
      .heading-left {
        flex: 1;
      }
      .heading-right {
        text-align: right;
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
      }
      .address-details {
        padding: 0;
      }
    }
    .address-wrapper {
      @media ${props => props.theme.mediaQuery.large} {
        ${props => (props.variation === listingHeader ? `display: flex;` : '')}
      }
    }
    .address-inline {
      display: none;
      @media ${props => props.theme.mediaQuery.mediumOnly} {
        display: block;
      }
    }
    .address-block {
      display: flex;
      flex-direction: column;
      @media ${props => props.theme.mediaQuery.mediumOnly} {
        display: none;
      }
    }
  }
  .address-details--listing-header {
    @media ${props => props.theme.mediaQuery.large} {
      margin-right: ${props =>
        props.variation === listingHeader ? '32px' : props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .brand-store--sm {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
    @media ${props => props.theme.mediaQuery.large} {
      display: flex;
      ${props => (props.variation === listingHeader ? 'margin-bottom: 0' : '')};
    }
  }

  .heading-right {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    }
    @media ${props => props.theme.mediaQuery.largeOnly} {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
`;
