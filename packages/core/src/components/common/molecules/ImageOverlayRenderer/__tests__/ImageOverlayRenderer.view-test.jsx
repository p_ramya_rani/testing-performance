// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';

import { ImageOverlayRendererVanilla } from '../views/ImageOverlayRenderer.view';

describe('ImageOverlayRenderer component', () => {
  it('should renders correctly', () => {
    const props = {
      previewWidth: 213,
      previewHeight: 265,
      previewPosLeft: 212,
      previewPosRight: 425,
      previewPosTop: 44,
      previewPosBottom: 85,
      imageWidth: 438,
      imageHeight: 544,
      active: true,
    };
    const component = shallow(<ImageOverlayRendererVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
