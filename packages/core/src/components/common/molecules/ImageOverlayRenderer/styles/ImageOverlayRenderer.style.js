// 9fbef606107a605d69c0edbcd8029e5d 
// Due to performance issue, styled component is not used.

const commonStyles = opacity => {
  return {
    backgroundColor: '#000',
    position: 'absolute',
    boxSizing: 'border-box',
    zIndex: '10',
    pointerEvents: 'none',
    transition: `opacity 0.4s ease`,
    transform: 'scale3d(1,1,1)',
    opacity,
  };
};

const OverlayCenterStyle = (width, height, left, top, opacity) => {
  return {
    position: 'absolute',
    width: `${width}px`,
    height: `${height}px`,
    left: 0,
    top: 0,
    boxSizing: 'border-box',
    transform: `translate(${left}px, ${top}px)`,
    border: '1px solid #fff',
    opacity,
    transition: `opacity 0.4s ease`,
    zIndex: '15',
    pointerEvents: 'none',
  };
};

const OverlayTopStyle = (width, height, opacity) => {
  return {
    top: 0,
    left: 0,
    width: `${width}px`,
    height: `${height}px`,
    ...commonStyles(opacity),
  };
};

const OverlayLeftStyle = (width, height, top, opacity) => {
  return {
    width: `${width}px`,
    top: `${top}px`,
    left: 0,
    height: `${height}px`,
    ...commonStyles(opacity),
  };
};

const OverlayRightStyle = (width, height, top, opacity) => {
  return {
    top: `${top}px`,
    right: 0,
    width: `${width}px`,
    height: `${height}px`,
    ...commonStyles(opacity),
  };
};

const OverlayBottomStyle = (width, height, top, opacity) => {
  return {
    top: `${top}px`,
    width: `${width}px`,
    height: `${height}px`,
    ...commonStyles(opacity),
  };
};

export default {
  OverlayCenterStyle,
  OverlayTopStyle,
  OverlayLeftStyle,
  OverlayRightStyle,
  OverlayBottomStyle,
};
