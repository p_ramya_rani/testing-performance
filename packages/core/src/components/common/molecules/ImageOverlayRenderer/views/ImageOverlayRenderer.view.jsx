// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import ImageOverlayStyle from '../styles/ImageOverlayRenderer.style';
import { BodyCopy } from '../../../atoms';

const ImageOverlayRenderer = (props) => {
  const {
    previewWidth,
    previewHeight,
    previewPosLeft,
    previewPosRight,
    previewPosTop,
    previewPosBottom,
    imageWidth,
    imageHeight,
    active,
  } = props;

  const opacity = active ? 0.6 : 0;
  const boxOpacity = active ? 0.8 : 0;

  // Due to dynamic change in className, styled component is causing slowness. So we are not using it here
  return (
    <React.Fragment>
      <BodyCopy
        component="div"
        style={ImageOverlayStyle.OverlayCenterStyle(
          previewWidth,
          previewHeight,
          previewPosLeft,
          previewPosTop,
          boxOpacity
        )}
      />
      <BodyCopy
        component="div"
        style={ImageOverlayStyle.OverlayTopStyle(imageWidth, previewPosTop, opacity)}
      />
      <BodyCopy
        component="div"
        style={ImageOverlayStyle.OverlayLeftStyle(
          previewPosLeft,
          previewHeight,
          previewPosTop,
          opacity
        )}
      />
      <BodyCopy
        component="div"
        style={ImageOverlayStyle.OverlayRightStyle(
          imageWidth - previewPosRight,
          previewHeight,
          previewPosTop,
          opacity
        )}
      />
      <BodyCopy
        component="div"
        style={ImageOverlayStyle.OverlayBottomStyle(
          imageWidth,
          imageHeight - previewPosBottom,
          previewPosBottom,
          opacity
        )}
      />
    </React.Fragment>
  );
};

ImageOverlayRenderer.propTypes = {
  previewWidth: PropTypes.number.isRequired,
  previewHeight: PropTypes.number.isRequired,
  previewPosLeft: PropTypes.number.isRequired,
  previewPosRight: PropTypes.number.isRequired,
  previewPosTop: PropTypes.number.isRequired,
  previewPosBottom: PropTypes.number.isRequired,
  imageWidth: PropTypes.number.isRequired,
  imageHeight: PropTypes.number.isRequired,
  active: PropTypes.bool.isRequired,
};

export default ImageOverlayRenderer;
export { ImageOverlayRenderer as ImageOverlayRendererVanilla };
