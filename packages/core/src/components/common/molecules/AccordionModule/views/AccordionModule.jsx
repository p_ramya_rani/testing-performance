// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import style from '../AccordionModule.style';
import { Row, Col } from '../../../atoms';
import AccordionList from '../../AccordionList';
import ModuleX from '../../ModuleX';
import { isClient, scrollPage } from '../../../../../utils';

/**
 * To render the Accordian Module and will be configured from CMS
 * @param {*} props
 */
// eslint-disable-next-line sonarjs/cognitive-complexity
const AccordionModule = (props) => {
  const { className, accordionWrapper } = props;
  const accordionItems = [];
  if (accordionWrapper && accordionWrapper.length > 0) {
    accordionWrapper.map(({ styled, text: { text = '' } = {} }) => {
      const isItemPresent = styled && styled.text;
      if (isItemPresent) {
        const titleKey = {
          header: {
            title: styled.text,
            id: text,
          },
        };
        accordionItems.push(titleKey);
      }
      return false;
    });
  }

  useEffect(() => {
    if (isClient()) {
      const { hash } = window.location;
      const id = hash && hash.replace('#', '');
      const element = document.getElementById(id);
      const HEADER_HEIGHT = 80;
      const scrollPosition = (element && element.getBoundingClientRect().top - HEADER_HEIGHT) || 0;
      // TODO: Can be improved with better solution
      // Currently Not working without setTimeout
      const scrollTimeout = setTimeout(() => {
        scrollPage(0, scrollPosition);
        clearTimeout(scrollTimeout);
      }, 1000);
    }
  }, []);

  return accordionWrapper ? (
    <Row className={className}>
      <Col
        colSize={{
          small: 6,
          medium: 8,
          large: 12,
        }}
      >
        <AccordionList accordionItems={accordionItems} className="module-accordion">
          {accordionWrapper.map(({ richText }) => {
            if (!(richText && richText.text)) {
              return null;
            }
            return <ModuleX richTextList={[richText]} />;
          })}
        </AccordionList>
      </Col>
    </Row>
  ) : null;
};

AccordionModule.propTypes = {
  className: PropTypes.string.isRequired,
  accordionWrapper: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.shape({}))).isRequired,
};

export default withStyles(AccordionModule, style);
export { AccordionModule as AccordionModuleVanilla };
