// 9fbef606107a605d69c0edbcd8029e5d
import usePushNotificationModal from '../hooks/usePushNotificationModal.native';

const PushNotificationModal = props => {
  usePushNotificationModal(props);
  return null;
};

export default PushNotificationModal;
export { PushNotificationModal as PushNotificationModalVanilla };
