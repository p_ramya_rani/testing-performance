// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';

import {
  updateCustomNavigationState,
  setAppLaunch,
} from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.actions';
import CUSTOM_NAVIGATION_CONSTANTS from '@tcp/core/src/components/features/content/CustomNavigation/CustomNavigation.constant';
import { updateAppTypeWithParams } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import { getIsAppLaunch } from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.selectors';
import { setClickAnalyticsData, trackPageView } from '@tcp/core/src/analytics/actions';

import PushNotificationModal from '../views';

const { PAGE_PLP, PAGE_PDP, PAGE_OUTFIT, CATEGORY } = CUSTOM_NAVIGATION_CONSTANTS;
const navigationPages = [PAGE_PLP, PAGE_PDP, PAGE_OUTFIT, CATEGORY];

export const mapDispatchToProps = (dispatch) => {
  return {
    updateCustomNavigationWithDeepLink: (payload) => {
      const { deepLink, notificationId } = payload;
      const indexOfQ = deepLink.indexOf('?');
      const page = deepLink.substring(6, indexOfQ).toLowerCase();

      const notification = {
        deepLink,
        requiredNavigationData: navigationPages.includes(page),
      };

      dispatch(
        updateCustomNavigationState({
          isNavigate: true,
          notification,
          notificationId,
        })
      );
    },
    useUpdateAppTypeDispatch: (payload) => {
      dispatch(updateAppTypeWithParams(payload));
    },
    clearAppLaunchFlag: (payload) => dispatch(setAppLaunch(payload)),
    trackAnalyticsLaunchApp: (eventData, payload) => {
      const loadtimer = setTimeout(() => {
        dispatch(setClickAnalyticsData(eventData));
        // Delayed setting of data when redirection on click event
        clearTimeout(loadtimer);
      }, 300);
      const timer = setTimeout(() => {
        dispatch(trackPageView(payload));
        clearTimeout(timer);
      }, 800);
    },
  };
};
const mapStateToProps = (state) => {
  return {
    isAppLaunch: getIsAppLaunch(state),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(PushNotificationModal);
