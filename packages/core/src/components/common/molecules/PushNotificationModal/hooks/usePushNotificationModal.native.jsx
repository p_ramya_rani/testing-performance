import { NativeModules, NativeEventEmitter, DeviceEventEmitter } from 'react-native';
import { useEffect } from 'react';
import { isAndroid, isIOS, getValueFromAsyncStorage } from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import AsyncStorage from '@react-native-community/async-storage';
import {
  APP_TYPE,
  DEEP_LINK_TYPE,
} from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import { getBrand } from '../../../../../utils';

const getPayload = (event) => {
  let payload = event;
  if (isAndroid()) {
    const { client_app_data: clientAppData, isAppOpenWithNotification } =
      JSON.parse(event.payload) || {};
    const { deep_link: deepLink, activity_uid: notificationId } = clientAppData;
    payload = { deepLink, notificationId, isAppOpenWithNotification };
  }
  return payload;
};

const vibesRegisterUser = async () => {
  try {
    const { VibesModule } = NativeModules;
    await VibesModule.registerPush();
  } catch (error) {
    logger.debug('Error: vibesRegisterUser', error);
  }
};

const usePushNotificationModal = (props) => {
  const currentAppBrand = getBrand();
  const {
    updateCustomNavigationWithDeepLink,
    clearAppLaunchFlag,
    trackAnalyticsLaunchApp,
    isAppLaunch,
    useUpdateAppTypeDispatch,
  } = props;

  // push notification action
  /*
    const deepLink = 'tcp://plp?category_id=toddler-girl-skinny-jeans&title=Skinny';
    const deepLink = 'tcp://pdp?department=Girls&category=title&product_id=2043572-10';
    const deepLink = 'tcp://outfit?department=Girls&seotoken=dressy-outfits&title=Title';
    const deepLink = 'tcp://plcc-card?page=plcc-reward-page';
    const deepLink = 'tcp://favorite?page=favorite_page';
    const deepLink = 'tcp://category?category_id=47511&title=girls&child_category_id=49007';
    this.NotificationHandle({}, deepLink, notificationResponse.notificationId);
    */
  const changeBrand = (brandType, deepLink, notificationId) => {
    const payload = {
      type: brandType,
    };
    useUpdateAppTypeDispatch(payload);
    updateCustomNavigationWithDeepLink({ deepLink, notificationId });
  };

  const deepLinkHandler = async (event) => {
    try {
      if (isAndroid()) {
        const { VibesModule } = NativeModules;
        await VibesModule.invokeApp();
      }
      const payload = getPayload(event);
      if (payload) {
        const { deepLink, notificationId, isAppOpenWithNotification } = payload;
        if (isAppOpenWithNotification) {
          await AsyncStorage.setItem('isAppOpenWithNotification', 'true');
          const eventData = { app_launch_v138: 'Push notification' };
          const payloadData = { currentScreen: 'app_launch' };
          trackAnalyticsLaunchApp(eventData, payloadData);
        }

        if (deepLink && notificationId) {
          if (deepLink.includes(APP_TYPE.TCP) && currentAppBrand !== APP_TYPE.TCP) {
            changeBrand(APP_TYPE.TCP, deepLink, notificationId);
          } else if (
            deepLink.includes(DEEP_LINK_TYPE.GYM) &&
            currentAppBrand !== APP_TYPE.GYMBOREE
          ) {
            changeBrand(APP_TYPE.GYMBOREE, deepLink, notificationId);
          } else {
            updateCustomNavigationWithDeepLink({ deepLink, notificationId });
          }
        }
      }
    } catch (e) {
      logger.error({
        error: `Exception in deep link handler: ${JSON.stringify(e)}`,
        errorTags: ['PushNotification', 'deepLinkHandler'],
        extraData: {
          event,
        },
      });
    }
  };

  useEffect(() => {
    const eventEmitter = isIOS()
      ? new NativeEventEmitter(NativeModules.PushEventEmitter)
      : DeviceEventEmitter;

    try {
      eventEmitter.addListener('pushOpened', deepLinkHandler);
      if (isAppLaunch) {
        if (isIOS()) {
          NativeModules.PushEventEmitter.isAppOpenWithNotification();
        } else {
          const { VibesModule } = NativeModules;
          VibesModule.isAppOpenWithNotification();
        }
        clearAppLaunchFlag(false);
      }
      getValueFromAsyncStorage('maybeLaterNotification').then((result) => {
        if (result === 'false') {
          vibesRegisterUser();
        }
      });
    } catch (e) {
      logger.error({
        error: `Exception in push notifications listener: ${JSON.stringify(e)}`,
        errorTags: ['PushNotification', 'Vibes'],
      });
    }
  }, []);
};

export default usePushNotificationModal;
