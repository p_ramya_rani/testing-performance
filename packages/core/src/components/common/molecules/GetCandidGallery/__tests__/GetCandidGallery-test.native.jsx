// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import GetCandidGallery from '../views/GetCandidGallery.native';

describe('GetCandidGallery component', () => {
  let component;
  beforeEach(() => {
    const props = {
      screenProps: {
        showBrands: false,
        network: {
          isConnected: true,
        },
        showPeekABooAnimation: true,
      },
      navigation: {
        state: {
          params: {
            activeIndex: 1,
            title: '#MYSTYLEPLACE',
          },
          routeName: 'GetCandidGallery',
          key: 'id-1597818683886-7',
        },
        actions: {},
      },
      candidData: {
        Views: [
          {
            Media: {
              Source: 'DirectUpload',
              Id: '3b527ee1-4606-48be-8932-44ae8ea40f3c',
              CreatedTime: 1596761031,
              ModifiedTime: 0,
              Type: 'image',
              Images: {
                Thumbnail: {
                  Width: 150,
                  Height: 150,
                  Url:
                    'https://api.getcandid.com/image/s/www.filepicker.io%2fapi%2ffile%2fF63ACzVdRsqe2DpfLYrq',
                },
                LowResolution: {
                  Width: 306,
                  Height: 306,
                  Url:
                    'https://api.getcandid.com/image/s/www.filepicker.io%2fapi%2ffile%2fFeaJduKKRremQt33pigm',
                },
                StandardResolution: {
                  Width: 4032,
                  Height: 3024,
                  Url:
                    'https://api.getcandid.com/image/s/www.filepicker.io%2fapi%2ffile%2flEbOYzYgQBeFiKkxVGpd',
                },
              },
              User: {
                Id: 'santina.rosario@aol.com',
                UserName: 'Noël norton',
                ProfilePicture:
                  'https://assets.imgix.net/~text?txt=NN&txtcolor=000000&txtsize=90&w=150&h=150&txtfont=Helvetica&bg=0FFF&txtalign=center%2Ccenter&txtpad=20',
                Ip: '69.206.40.153',
                Email: 'santina.rosario@aol.com',
              },
              Likes: 0,
              Comments: 0,
              Retweets: 0,
              ViewCount: 31,
              LikesDisplay: '0',
            },
            StreamEntry: {
              Id: '070167ca-8287-4d41-a9bb-6b3850cae9b1_',
              Source: 1,
              MediaType: 'image',
              StreamId: '070167ca-8287-4d41-a9bb-6b3850cae9b1',
              Tags: {
                Count: 4,
                ProductCount: 1,
                MissingCoordinates: 1,
                Items: [
                  {
                    TagId: 'approved',
                    TagType: 'Placement',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                  {
                    TagId: '00194936017666',
                    TagType: 'Product',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                  {
                    TagId: 'homepage',
                    TagType: 'Placement',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                  {
                    TagId: 'gallery',
                    TagType: 'Placement',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                ],
                ModifiedTime: 0,
              },
            },
            CreatedTime: 1596761031000,
            Age: '12d',
            CaptionHtml:
              ' <span class=\'candid-attr\'><i></i><a href="" target="_blank">@Noël norton</a><i></i><b>12d&nbsp;ago</b><i></i><b>DirectUpload</b></span>',
          },
          {
            Media: {
              Source: 'Instagram',
              Id: '17961946183332234',
              Caption: '🍦',
              CreatedTime: 1596475773,
              ModifiedTime: 0,
              Tags: ['surethingtcp'],
              OriginalLink: 'https://www.instagram.com/p/CDb4Hx9Joit/',
              Type: 'image',
              Images: {
                Thumbnail: {
                  Width: 0,
                  Height: 0,
                  Url:
                    'https://api.getcandid.com/image/s/candid.azureedge.net%2fstream-media%2f070167ca-8287-4d41-a9bb-6b3850cae9b1_17961946183332234_thumbnail.jpg',
                },
                LowResolution: {
                  Width: 0,
                  Height: 0,
                  Url:
                    'https://api.getcandid.com/image/s/candid.azureedge.net%2fstream-media%2f070167ca-8287-4d41-a9bb-6b3850cae9b1_17961946183332234_low.jpg',
                },
                StandardResolution: {
                  Width: 0,
                  Height: 0,
                  Url:
                    'https://api.getcandid.com/image/s/candid.azureedge.net%2fstream-media%2f070167ca-8287-4d41-a9bb-6b3850cae9b1_17961946183332234_standard.jpg',
                },
              },
              User: {
                Id: 'raising_ainsleigh',
                UserName: 'raising_ainsleigh',
                ProfilePicture:
                  'https://assets.imgix.net/~text?txt=R&txtcolor=000000&txtsize=90&w=150&h=150&txtfont=Helvetica&bg=0FFF&txtalign=center%2Ccenter&txtpad=20',
                ProfileUrl: 'https://www.instagram.com/raising_ainsleigh',
              },
              Likes: 495,
              Comments: 43,
              Retweets: 0,
              ShadowType: 2,
              ViewCount: 773,
              LikesDisplay: '495',
            },
            StreamEntry: {
              Id: '070167ca-8287-4d41-a9bb-6b3850cae9b1_17961946183332234',
              MediaId: '17961946183332234',
              UserId: 'raising_ainsleigh',
              Source: 1,
              MediaType: 'image',
              StreamId: '070167ca-8287-4d41-a9bb-6b3850cae9b1',
              Tags: {
                Count: 5,
                ProductCount: 2,
                MissingCoordinates: 2,
                Items: [
                  {
                    TagId: 'approved',
                    TagType: 'Placement',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                  {
                    TagId: '00193511902557',
                    TagType: 'Product',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                  {
                    TagId: '00193511910019',
                    TagType: 'Product',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                  {
                    TagId: 'homepage',
                    TagType: 'Placement',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                  {
                    TagId: 'gallery',
                    TagType: 'Placement',
                    Default: false,
                    Quantity: 0,
                    X: 0,
                    Y: 0,
                  },
                ],
                ModifiedTime: 0,
              },
              CreatedTime: 1596475773000,
            },
            CreatedTime: 1596475773000,
            Age: '15d',
            CaptionHtml:
              '🍦 <span class=\'candid-attr\'><i></i><a href="https://www.instagram.com/p/CDb4Hx9Joit/" target="_blank">@raising_ainsleigh</a><i></i><b>15d&nbsp;ago</b><i></i><b>Instagram</b></span>',
          },
        ],
      },
      labels: {
        referred: [],
        lbl_getCandid_title: '#MyStylePlace',
        lbl_getCandid_titleDescription:
          "Show us how you're celebrating every big & small occasion, in style!",
        lbl_getCandid_BtnGallery: 'VIEW GALLERY',
        lbl_getCandid_BtnPhoto: 'ADD PHOTO',
        lbl_getCandid_BtnShopNow: 'SHOP NOW',
        lbl_getCandid_BtnLoadMore: 'LOAD MORE',
        lbl_getCandid_btnSeeMore: 'See More',
      },
    };
    component = shallow(<GetCandidGallery {...props} />);
  });

  it('should renders correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
