// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FlatList, View } from 'react-native';
import { goToPlpTab } from '@tcp/core/src/utils';
import { Anchor, Button } from '../../../atoms';
import { getCandidData, getLabels } from '../../GetCandid/container/GetCandid.selectors';
import ClickTracker from '../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import names from '../../../../../constants/eventsName.constants';
import {
  Image,
  ItemWrapper,
  Title,
  ProfileImageWrapper,
  CaptionTextWrapper,
  ShopLookWrapper,
  ShopLookScroll,
  ShopLookItem,
  ShopLookItemCaption,
  Touchable,
  Wrapper,
  DescriptionWrapper,
  Divider,
  ButtonContainer,
  StyledCaption,
} from '../styles/GetCandidGallery.style.native';
import { getScreenWidth } from '../../../../../utils/index.native';
import { DEFAULT_PAGE_SIZE } from '../config';
import { trackPageView, setClickAnalyticsData } from '../../../../../analytics/actions';

/**
 * @class GetCandidGallery - display images shared by customers on the Get candid gallery page
 * promote popular products and encourage more customers to buy them
 *
 * @param {candidData} candidData the list of data to display images
 * @param {labels} labels label data for get candid module
 * @param {navigation} navigation object containing navigate method
 */

const TEXT_WIDTH = getScreenWidth() - 140;
const ITEM_WIDTH = getScreenWidth() / 3;

class GetCandidGallery extends React.PureComponent {
  /**
   * state is used to handle active image on click
   * for FlatList component.
   */
  state = {
    activeIndex: 0,
    batchSizeMultiplier: 1,
    isTrackPageLoad: false,
  };

  /**
   * @function componentDidMount function is used to
   * set the index of the activeImage clicked on the homepage
   * as its index is passed in the navigation's param with
   * @key activeIndex
   */
  componentDidMount() {
    const { navigation } = this.props;
    const {
      state: {
        params: { activeIndex },
      },
    } = navigation;
    this.candidItemClickHandler(activeIndex);
  }

  /**
   * @function componentDidUpdate function is used to
   * check if the prevState multiplier and currentState multiplier
   * values are different inorder to focus the very first image
   * in the next batch.
   */
  componentDidUpdate(prevProps, prevState) {
    const { batchSizeMultiplier: prevMultiplier } = prevState;
    const { batchSizeMultiplier: currentMultiplier } = this.state;
    if (prevMultiplier !== currentMultiplier) {
      this.candidItemClickHandler(prevMultiplier * 20);
    }
  }

  /**
   * @function calculateItemHeight function to calculate
   * height of the main image
   * 0.8 = image width/height ratio as per design
   */
  calculateItemHeight = () => this.getImageSize() / 0.8;

  /**
   * @function getItemLayout function to calculate
   * the offset of the flatList item
   * to display it on top using scrollToIndex function
   */
  getItemLayout = (data, index) => {
    const length = this.calculateItemHeight() + 8;
    return { length, offset: length * index, index };
  };

  /**
   * @function candidItemClickHandler function to update
   * the active clicked image
   * and render its respective section
   * containing description and shopThisLook section
   * @params index is required of the clicked image
   */
  candidItemClickHandler = (index) => {
    const { activeIndex } = this.state;
    if (activeIndex !== index && index >= 0) {
      this.setState({
        activeIndex: index,
      });
      if (this.flatListRef) {
        this.flatListRef.scrollToIndex({ animated: false, index });
      }
    }
  };

  /**
   * @function keyExtractor function to get unique key
   * for FlatList component.
   */
  keyExtractor = (_, index) => index.toString();

  /**
   * @function getImageSize function to calculate size of
   * image dynamically as per screen size.
   */
  getImageSize = () => parseInt(getScreenWidth(), 10);

  /**
   * @function renderShopThisLook function to render main image
   *  description and shopThisLook section.
   */
  renderShopThisLook = (tagItems) => {
    const { navigation } = this.props;
    return (
      <ShopLookScroll horizontal showsHorizontalScrollIndicator={false}>
        {tagItems.map(({ Id, ImageUrl, DisplayText, TagId }) => (
          <ShopLookItem key={Id} width={ITEM_WIDTH}>
            <Anchor
              onPress={() => {
                goToPlpTab(navigation);
                return navigation.navigate('ProductDetail', {
                  title: DisplayText,
                  pdpUrl: TagId,
                  selectedColorProductId: TagId,
                  reset: true,
                  source: 'Candid',
                });
              }}
            >
              <Image
                source={{ uri: ImageUrl }}
                alt={DisplayText}
                width={142}
                height={142}
                resizeMode="contain"
              />
              <ShopLookItemCaption
                fontFamily="secondary"
                fontSize="fs14"
                fontWeight="regular"
                color="gray.900"
                text={DisplayText}
                ellipsizeMode="tail"
                numberOfLines={2}
                textAlign="center"
              />
            </Anchor>
          </ShopLookItem>
        ))}
      </ShopLookScroll>
    );
  };

  /**
   * @function filterItemTags function to map tagIds
   * with Tags property in redux store
   */
  filterItemTags = (itemTag) => {
    const {
      candidData: { Tags },
    } = this.props;
    const tagsList = {};
    Tags.forEach((i) => {
      tagsList[i.TagId] = i;
    });
    return Tags.length
      ? itemTag.filter((item) => item.TagType === 'Product').map((item) => tagsList[item.TagId])
      : [];
  };

  /**
   * @function renderItem : Render method for Flatlist.
   * @desc This method is rendering GetCandid image items.
   *
   * @param {Object} item : Single object to render inside Flatlist.
   * @return {node} function returns Image element element.
   */
  renderItem = (item) => {
    const {
      item: {
        Media: {
          Images: { StandardResolution },
          User: { ProfilePicture, UserName },
          Caption,
        },
        StreamEntry: { Tags },
      },
      index,
    } = item;

    const image = StandardResolution;
    const { activeIndex } = this.state;
    const { labels, trackPageLoad } = this.props;
    let tagItems = [];

    const { isTrackPageLoad } = this.state;
    if (index === activeIndex) {
      tagItems = this.filterItemTags(Tags.Items);
      if (!isTrackPageLoad) {
        const customEvents = tagItems.length ? ['event152', 'event153'] : ['event152'];
        trackPageLoad({ currentScreen: names.screenNames.getCandid152, customEvents });
        this.setState({ isTrackPageLoad: true });
      }
    }

    return (
      <ItemWrapper key={index.toString()}>
        <Touchable accessibilityRole="image" onPress={() => this.candidItemClickHandler(index)}>
          <Image
            source={{ uri: image.Url }}
            alt="" // ignored - image detailed text is html content
            width={this.getImageSize()}
            height={this.calculateItemHeight()}
          />
        </Touchable>
        {index === activeIndex && (
          <View>
            <DescriptionWrapper>
              <ProfileImageWrapper>
                <Image source={{ uri: ProfilePicture }} alt={UserName} width={66} height={66} />
              </ProfileImageWrapper>
              <CaptionTextWrapper>
                <StyledCaption
                  width={TEXT_WIDTH}
                  fontFamily="secondary"
                  fontSize="fs14"
                  fontWeight="regular"
                  color="gray.900"
                  text={Caption}
                />
              </CaptionTextWrapper>
            </DescriptionWrapper>
            {tagItems.length ? (
              <ShopLookWrapper>
                <Divider />
                <Title
                  fontFamily="primary"
                  fontSize="fs20"
                  fontWeight="regular"
                  color="gray.900"
                  text={
                    labels &&
                    labels.lbl_getCandid_titleShopThisLook &&
                    labels.lbl_getCandid_titleShopThisLook.toUpperCase()
                  }
                  textAlign="center"
                  letterSpacing="ls222"
                />
                {this.renderShopThisLook(tagItems)}
              </ShopLookWrapper>
            ) : null}
            <Divider marginBottom={16} />
          </View>
        )}
      </ItemWrapper>
    );
  };

  /**
   * @function renderFlatListFooter default function of flatList
   * to render LoadMore button at the end of the list
   */
  renderFlatListFooter = () => {
    const { labels, candidData } = this.props;
    const candidDataLength = candidData.Views.length;
    const { batchSizeMultiplier } = this.state;
    const showLoadMore = candidDataLength > batchSizeMultiplier * DEFAULT_PAGE_SIZE;
    if (showLoadMore) {
      return (
        <ButtonContainer>
          <Button
            fontFamily="secondary"
            width="100%"
            text={labels.lbl_getCandid_btnLoadMore}
            fill="BLUE"
            color="white"
            onPress={this.loadMoreHandler}
          />
          <ClickTracker
            as={Anchor}
            name={names.screenNames.getCandid152}
            clickData={{ customEvents: ['event152'] }}
            fontSizeVariation="large"
            noLink
            dataLocator=""
            text={labels.lbl_getCandid_btnSeeMore}
            visible
          />
        </ButtonContainer>
      );
    }

    return null;
  };

  /**
   * @function loadMoreHandler function is used
   * to load data in batch of 20 by updating
   * batchSizeMultiplier in state
   */
  loadMoreHandler = () => {
    const { batchSizeMultiplier } = this.state;
    const newMultiplier = batchSizeMultiplier + 1;
    this.setState({
      batchSizeMultiplier: newMultiplier,
    });
  };

  render() {
    const { navigation, candidData } = this.props;
    const { batchSizeMultiplier } = this.state;
    const imagesCount = DEFAULT_PAGE_SIZE * batchSizeMultiplier;
    const data = candidData && candidData.Views.slice(0, imagesCount);
    const {
      state: {
        params: { activeIndex },
      },
    } = navigation;
    return (
      <Wrapper>
        {data && (
          <FlatList
            ref={(ref) => {
              this.flatListRef = ref;
            }}
            getItemLayout={this.getItemLayout}
            data={data}
            extraData={this.state}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
            initialScrollIndex={activeIndex}
            ListFooterComponent={this.renderFlatListFooter}
          />
        )}
      </Wrapper>
    );
  }
}

GetCandidGallery.propTypes = {
  navigation: PropTypes.shape({}),
  candidData: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  trackPageLoad: PropTypes.func.isRequired,
};

GetCandidGallery.defaultProps = {
  candidData: {
    Settings: {},
    Views: [],
    Tags: [],
  },
  labels: {},
  navigation: {},
};

const mapStateToProps = (state) => {
  return {
    candidData: getCandidData(state),
    labels: getLabels(state),
  };
};
function mapDispatchToProps(dispatch) {
  return {
    trackPageLoad: (payload) => {
      dispatch(setClickAnalyticsData(payload));
      dispatch(trackPageView(payload));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GetCandidGallery);
export { GetCandidGallery as GetCandidGalleryVanilla };
