// 9fbef606107a605d69c0edbcd8029e5d 
import { takeLeading } from 'redux-saga/effects';
import {
  RecommendationsSaga,
  fetchRecommendationsData,
} from '../container/Recommendations.saga.native';
import { FETCH_RECOMMENDATIONS_DATA_MONETATE } from '../container/Recommendations.constants';

describe('Recommendation saga', () => {
  it('should return correct takeLeading effect', () => {
    const generator = RecommendationsSaga();
    const takeLatestDescriptor = generator.next().value;
    const expected = takeLeading(FETCH_RECOMMENDATIONS_DATA_MONETATE, fetchRecommendationsData);
    expect(takeLatestDescriptor).toEqual(expected);
  });
});
