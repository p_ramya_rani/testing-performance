// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLeading, select, takeEvery } from 'redux-saga/effects';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { loadSocialProofData } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.actions';
import { RECOMMENDATIONS_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import RecommendationsAbstractor from '../../../../../services/abstractors/common/recommendations';
import { loadRecommendationsData, setRecommendationLoadingState } from './Recommendations.actions';
import {
  FETCH_RECOMMENDATIONS_DATA_MONETATE,
  FETCH_RECOMMENDATIONS_DATA_ADOBE,
  HIDDEN_MBOX_NAME,
  SEND_EVENTS_TO_MONETATE,
} from './Recommendations.constants';
import logger from '../../../../../utils/loggerInstance';
import { getPointsToNextReward } from '../../../../features/CnC/AddedToBag/container/AddedToBag.selectors';
import { getQuickAddThreshold } from '../../../../../reduxStore/selectors/session.selectors';

function getFavoritesData(state, key, resultParam, isCatch) {
  let result = resultParam;

  const previousRecommendationObj =
    state[RECOMMENDATIONS_REDUCER_KEY] &&
    state[RECOMMENDATIONS_REDUCER_KEY].get('favorites_global_products');
  let currentVal;
  if (isCatch) {
    currentVal = {
      [key]: { errors: key },
    };
  } else {
    currentVal = {
      [key]: result,
    };
  }

  if (!previousRecommendationObj) {
    result = currentVal;
  } else {
    Object.keys(previousRecommendationObj).forEach((keyVal) => {
      let newKeyVal = '';
      if (currentVal && Object.keys(currentVal)[0]) {
        newKeyVal = [Object.keys(currentVal)];
      }
      if (newKeyVal !== keyVal) {
        previousRecommendationObj[newKeyVal] = currentVal[newKeyVal];
        result = previousRecommendationObj;
      }
    });
  }
  return result;
}

function* fetchRecommendationsData(action) {
  const { payload } = action;
  const isLoggedIn = yield select(getUserLoggedInState) || false;
  const xappURLConfig = yield call(getUnbxdXappConfigs);
  try {
    yield put(setRecommendationLoadingState({ isLoading: true }));
    const { mboxName, itemPartNumber, reduxKey } = payload;
    const loadRecommendation = mboxName !== HIDDEN_MBOX_NAME;
    const pointsToNextReward = yield select(getPointsToNextReward);
    const quickAddThreshold = yield select(getQuickAddThreshold);
    const quickAdd = isLoggedIn && pointsToNextReward < quickAddThreshold;
    if (loadRecommendation) {
      let result = yield call(
        RecommendationsAbstractor.getData,
        { ...payload, isLoggedIn, quickAdd },
        xappURLConfig
      );
      if (reduxKey === 'favorites_global_products') {
        const state = yield select();
        const key = `${itemPartNumber}`;
        const isProductMissing = !!(result && result.products && result.products.length === 0);
        result = getFavoritesData(state, key, result, isProductMissing);
      }
      const socialProofData = {
        socialProofMsg: RecommendationsAbstractor.getSocialProofMsg(result.socialProofItems),
      };
      result = { products: result.products, mainTitle: result.mainTitle };
      yield put(
        loadRecommendationsData({
          reduxKey,
          result,
        })
      );
      if (
        socialProofData &&
        socialProofData.socialProofMsg &&
        socialProofData.socialProofMsg.priorityMsg &&
        socialProofData.socialProofMsg.variation
      ) {
        yield put(
          loadSocialProofData({
            reduxKey,
            socialProofData,
          })
        );
      }

      yield put(setRecommendationLoadingState({ isLoading: false }));
    }
  } catch (e) {
    if (payload.reduxKey === 'favorites_global_products') {
      const key = `${payload.itemPartNumber}`;
      const state = yield select();
      let result = {};
      result = getFavoritesData(state, key, result, true);
      yield put(
        loadRecommendationsData({
          reduxKey: payload.reduxKey,
          result,
        })
      );
    }
    yield put(setRecommendationLoadingState({ isLoading: false }));
    logger.error('Error has occurred: ', {
      error: e,
      extraData: {
        component: 'Recommendations Saga - fetchRecommendationsData',
        payloadRecieved: payload,
      },
    });
  }
}

function* sendEventsToMonetate(action) {
  const { payload } = action;
  const isLoggedIn = yield select(getUserLoggedInState) || false;
  try {
    yield call(RecommendationsAbstractor.sendEventsToMonetate, { ...payload, isLoggedIn });
  } catch (e) {
    logger.error('Error has occurred: ', {
      error: e,
      extraData: {
        component: 'Recommendations Saga - sendEventsToMonetate',
        payloadRecieved: payload,
      },
    });
  }
}

function* RecommendationsSaga() {
  yield takeLeading(FETCH_RECOMMENDATIONS_DATA_MONETATE, fetchRecommendationsData);
  yield takeEvery(FETCH_RECOMMENDATIONS_DATA_ADOBE, fetchRecommendationsData);
  yield takeLeading(SEND_EVENTS_TO_MONETATE, sendEventsToMonetate);
}

export default RecommendationsSaga;
