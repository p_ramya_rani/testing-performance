// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getABtestFromState, getAPIConfig } from '@tcp/core/src/utils';
import { RECOMMENDATIONS_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import { HIDDEN_MBOX_NAME, RECOMMENDATIONS_PAGES_MAPPING } from './Recommendations.constants';
import { parseBoolean } from '../../../../../services/abstractors/productListing/productParser';

export const getProducts = (state, reduxKey) => {
  const recommendation = state[RECOMMENDATIONS_REDUCER_KEY];
  return recommendation && recommendation.get(reduxKey) && recommendation.get(reduxKey).products;
};

export const getCompleteRecommendations = (state) => {
  return state[RECOMMENDATIONS_REDUCER_KEY];
};

export const getAppFirstSuggestedProduct = (state) => {
  const productsObj =
    state[RECOMMENDATIONS_REDUCER_KEY] &&
    state[RECOMMENDATIONS_REDUCER_KEY].get('favorites_global_products');
  return (
    (productsObj &&
      productsObj.products &&
      productsObj.products.length > 0 && [productsObj.products[0]]) ||
    null
  );
};

export const getFirstSuggestedProduct = (state, itemPartNumber) => {
  const productsObj =
    state[RECOMMENDATIONS_REDUCER_KEY] &&
    state[RECOMMENDATIONS_REDUCER_KEY].get('favorites_global_products');
  return (
    (productsObj &&
      itemPartNumber &&
      productsObj[itemPartNumber] &&
      productsObj[itemPartNumber].products &&
      productsObj[itemPartNumber].products.length > 0 && [
        productsObj[itemPartNumber].products[0],
      ]) ||
    null
  );
};

export const getSuggestedProductState = (state, itemPartNumber) => {
  const productsObj =
    state[RECOMMENDATIONS_REDUCER_KEY] &&
    state[RECOMMENDATIONS_REDUCER_KEY].get('favorites_global_products');

  return !!(
    productsObj &&
    itemPartNumber &&
    productsObj[itemPartNumber] &&
    productsObj[itemPartNumber].products &&
    productsObj[itemPartNumber].products.length > 0
  );
};

export const getSuggestedProductErrors = (state, itemPartNumber) => {
  const productsObj =
    state[RECOMMENDATIONS_REDUCER_KEY] &&
    state[RECOMMENDATIONS_REDUCER_KEY].get('favorites_global_products');

  return (
    (productsObj &&
      itemPartNumber &&
      productsObj[itemPartNumber] &&
      productsObj[itemPartNumber].errors) ||
    {}
  );
};

export const getLoadedProductsCount = createSelector(
  getProducts,
  (products) => products && products.length
);

export const getLabelsProductListing = (state) => {
  if (!state.Labels || !state.Labels.PLP)
    return {
      addToBag: {},
      readMore: {},
      readLess: {},
    };
  const {
    PLP: {
      plpTiles: {
        lbl_add_to_bag: addToBag,
        lbl_plp_in_grid_header: inGridHeader,
        lbl_plp_in_multi_grid_header: inMultiGridHeader,
        lbl_plp_in_grid_description: inGridDescription,
        lbl_plp_in_multi_grid_description: inMultiGridDescription,
      },
      seoText: { lbl_read_more: readMore, lbl_read_less: readLess },
    },
  } = state.Labels;

  return {
    addToBag,
    readMore,
    readLess,
    inGridHeader,
    inGridDescription,
    inMultiGridHeader,
    inMultiGridDescription,
  };
};

export const getRecommendationsAbTest = (state, isAddedToBag) => {
  let abTest = null;
  const getAbtests = getABtestFromState(state.AbTest);

  if (getAbtests) {
    if (isAddedToBag && getAbtests.addedToBagModal) {
      abTest = getAbtests.addedToBagModal.recommendations;
    } else if (getAbtests.recommendations) {
      abTest = getAbtests.recommendations;
    }
  }
  return abTest;
};

export const getPushNotificationInfo = (state) => {
  const getAbTests = getABtestFromState(state.AbTest);
  if (getAbTests) {
    return {
      enableBackgroundProcess:
        getAbTests.ENABLE_BACKGROUND_PROCESS && parseBoolean(getAbTests.ENABLE_BACKGROUND_PROCESS),
      pushNotificationBackgroundFetchTimer:
        getAbTests.PUSH_NOTIFICATION_BACKGROUND_FETCH_TIMER &&
        parseInt(getAbTests.PUSH_NOTIFICATION_BACKGROUND_FETCH_TIMER, 10),
      pushNotificationHoursAfterPurchase:
        getAbTests.PUSH_NOTIFICATION_HOURS_AFTER_PURCHASE &&
        parseInt(getAbTests.PUSH_NOTIFICATION_HOURS_AFTER_PURCHASE, 10),
      isPushNotificationTestingEnv:
        getAbTests.PUSH_NOTIFICATION_TESTING_ENVIRONMENT &&
        parseBoolean(getAbTests.PUSH_NOTIFICATION_TESTING_ENVIRONMENT),
      pushNotificationTime: {
        day: getAbTests.PUSH_NOTIFICATION_DAY && parseInt(getAbTests.PUSH_NOTIFICATION_DAY, 10),
        hour: getAbTests.PUSH_NOTIFICATION_HOUR && parseInt(getAbTests.PUSH_NOTIFICATION_HOUR, 10),
        minute:
          getAbTests.PUSH_NOTIFICATION_MINUTES &&
          parseInt(getAbTests.PUSH_NOTIFICATION_MINUTES, 10),
      },
    };
  }
  return {};
};

const getRecommendationLabel = (state, labelSuffix, getLabelValue) => {
  return getLabelValue(
    state.Labels,
    `lbl_carousel_${labelSuffix}`,
    RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
    'global'
  );
};

export const getRecommendationModulePLabel = (
  abTestConfig,
  state,
  labelSuffix,
  ownProps,
  getLabelValue,
  isOptimisticModalWithError = false
) => {
  if (isOptimisticModalWithError) {
    return getLabelValue(
      state.Labels,
      'lbl_error_optimistic_addtobag',
      RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
      'global'
    );
  }
  return abTestConfig
    ? getRecommendationLabel(state, labelSuffix, getLabelValue)
    : ownProps.headerLabel ||
        getLabelValue(
          state.Labels,
          'MODULE_P_HEADER_LABEL',
          RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
          'global'
        );
};

export const getRecommendationModuleOLabel = (
  abTestConfig,
  state,
  labelSuffix,
  ownProps,
  getLabelValue,
  isOptimisticModalWithError = false
) => {
  if (isOptimisticModalWithError) {
    return getLabelValue(
      state.Labels,
      'lbl_error_optimistic_addtobag',
      RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
      'global'
    );
  }
  return abTestConfig
    ? getRecommendationLabel(state, labelSuffix, getLabelValue)
    : ownProps.headerLabel ||
        getLabelValue(
          state.Labels,
          'MODULE_O_HEADER_LABEL',
          RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
          'global'
        );
};

export const getHomePageRecsModuleOLabel = (state, isSequenceTwo, getLabelValue) => {
  return isSequenceTwo
    ? getLabelValue(
        state.Labels,
        'lbl_home_moduleO_sequence_2',
        RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
        'global'
      )
    : getLabelValue(
        state.Labels,
        'lbl_home_moduleO_sequence_1',
        RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
        'global'
      );
};

export const getHomePageRecsModulePLabel = (state, isSequenceTwo, getLabelValue) => {
  return isSequenceTwo
    ? getLabelValue(
        state.Labels,
        'lbl_home_moduleP_sequence_2',
        RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
        'global'
      )
    : getLabelValue(
        state.Labels,
        'lbl_home_moduleP_sequence_1',
        RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS,
        'global'
      );
};

export const getABTestConfig = (abTestRecommendations, ownProps) => {
  const carouselSequence = ownProps.sequence || '1';
  const carousel = `carousel${carouselSequence}`;
  return abTestRecommendations[`${carousel}`];
};

export const getMboxName = (abTestConfig) => {
  return abTestConfig && abTestConfig.hide ? HIDDEN_MBOX_NAME : abTestConfig.mbox;
};

export const getLabelSuffix = (page, ownProps, abTestRecommendations) => {
  const segment = abTestRecommendations.segment || 'A';
  const carouselSequence = ownProps.sequence || '1';
  return `${carouselSequence}_${page}_segment_${segment}`;
};

export const getMonetateEnabledFlag = () => {
  const apiConfigObj = getAPIConfig();
  let { monetateEnabled } = apiConfigObj;
  try {
    monetateEnabled = JSON.parse(monetateEnabled);
  } catch (e) {
    monetateEnabled = true;
  }
  return monetateEnabled;
};

export const getUpdateFlag = (state) => {
  return state[RECOMMENDATIONS_REDUCER_KEY] && state[RECOMMENDATIONS_REDUCER_KEY].get('isUpdating');
};

export const getRecommendationOrderModalState = (state) => {
  const orderRecommendationModal =
    state[RECOMMENDATIONS_REDUCER_KEY] &&
    state[RECOMMENDATIONS_REDUCER_KEY].get('orderRecommendationModal');
  return (orderRecommendationModal && orderRecommendationModal.isOpen) || false;
};

export const getRecommendationOrderPartNumbers = (state) => {
  const orderRecommendationModal =
    state[RECOMMENDATIONS_REDUCER_KEY] &&
    state[RECOMMENDATIONS_REDUCER_KEY].get('orderRecommendationModal');
  return (orderRecommendationModal && orderRecommendationModal.partNumbers) || [];
};

export const getRecommendationLoadingState = (state) => {
  return state[RECOMMENDATIONS_REDUCER_KEY] && state[RECOMMENDATIONS_REDUCER_KEY].get('isLoading');
};

export const getPushNotificationTestingFlag = () => {
  const apiConfigObj = getAPIConfig();
  let { pushNotificationTesting } = apiConfigObj;
  try {
    pushNotificationTesting = JSON.parse(pushNotificationTesting);
  } catch (e) {
    pushNotificationTesting = false;
  }
  return pushNotificationTesting;
};
