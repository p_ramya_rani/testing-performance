// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLeading, takeEvery, select } from 'redux-saga/effects';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { loadSocialProofData } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.actions';
import { RECOMMENDATIONS_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import RecommendationsAbstractor from '../../../../../services/abstractors/common/recommendations';
import { loadRecommendationsData } from './Recommendations.actions';
import {
  FETCH_RECOMMENDATIONS_DATA_MONETATE,
  FETCH_RECOMMENDATIONS_DATA_ADOBE,
  SEND_EVENTS_TO_MONETATE,
} from './Recommendations.constants';
import logger from '../../../../../utils/loggerInstance';
import { getPointsToNextReward } from '../../../../features/CnC/AddedToBag/container/AddedToBag.selectors';
import { getQuickAddThreshold } from '../../../../../reduxStore/selectors/session.selectors';
import { getIsGuest } from '../../../../features/account/User/container/User.selectors';

function getFavoritesErrorResponse(resultParam, payload) {
  let result = resultParam;
  if (
    result &&
    result.products &&
    result.products.length === 0 &&
    payload.reduxKey !== 'favorites_global_products'
  ) {
    const errors = {};
    errors[payload.partNumber] = payload.partNumber;
    result = { ...result, errors };
  }

  return result;
}

function getFavoritesData(state, key, resultParam, isCatch) {
  let result = resultParam;

  const previousRecommendationObj =
    state[RECOMMENDATIONS_REDUCER_KEY] &&
    state[RECOMMENDATIONS_REDUCER_KEY].get('favorites_global_products');
  let currentVal;
  if (isCatch) {
    currentVal = {
      [key]: { errors: key },
    };
  } else {
    currentVal = {
      [key]: result,
    };
  }

  if (!previousRecommendationObj) {
    result = currentVal;
  } else {
    Object.keys(previousRecommendationObj).forEach((keyVal) => {
      let newKeyVal = '';
      if (currentVal && Object.keys(currentVal)[0]) {
        newKeyVal = [Object.keys(currentVal)];
      }
      if (newKeyVal !== keyVal) {
        previousRecommendationObj[newKeyVal] = currentVal[newKeyVal];
        result = previousRecommendationObj;
      }
    });
  }
  return result;
}

function* getSocialProofData(result) {
  const socialProofData = {
    socialProofMsg: RecommendationsAbstractor.getSocialProofMsg(result.socialProofItems),
  };
  if (
    socialProofData &&
    socialProofData.socialProofMsg &&
    socialProofData.socialProofMsg.priorityMsg &&
    socialProofData.socialProofMsg.variation
  ) {
    yield put(
      loadSocialProofData({
        reduxKey: 'PreviousPDP',
        socialProofData,
      })
    );
  }
}

export function* fetchRecommendationsData(action) {
  const { payload } = action;
  const xappURLConfig = yield call(getUnbxdXappConfigs);
  try {
    const { mbox } = payload;
    const loadRecommendation = mbox !== Constants.HIDDEN_MBOX_NAME;
    if (loadRecommendation) {
      let previousRecommendationObj = {};
      if (payload.reduxKey === 'pdp') {
        const state = yield select();
        previousRecommendationObj =
          state[RECOMMENDATIONS_REDUCER_KEY] && state[RECOMMENDATIONS_REDUCER_KEY].get('pdp');
      }

      yield put(
        loadRecommendationsData({
          reduxKey: payload.reduxKey,
          result: { errors: [] },
        })
      );
      const pointsToNextReward = yield select(getPointsToNextReward);
      const quickAddThreshold = yield select(getQuickAddThreshold);
      const isGuest = yield select(getIsGuest);
      const quickAdd = !isGuest && pointsToNextReward < quickAddThreshold;

      let result = yield call(
        RecommendationsAbstractor.getMobileData,
        { ...payload, quickAdd },
        xappURLConfig
      );
      result = getFavoritesErrorResponse(result, payload);
      if (payload.reduxKey === 'favorites_global_products') {
        const key = `${payload.partNumber}`;
        const state = yield select();
        const isProductMissing = !!(result && result.products && result.products.length === 0);
        result = getFavoritesData(state, key, result, isProductMissing);
      }

      if (payload.reduxKey === 'pdp') {
        // Storing double copy of pdp for loading data from cache
        if (previousRecommendationObj?.products) {
          yield put(
            loadRecommendationsData({
              reduxKey: 'PreviousPDP',
              result: previousRecommendationObj,
            })
          );
        } else {
          yield put(
            loadRecommendationsData({
              reduxKey: 'PreviousPDP',
              result,
            })
          );
        }
        yield call(getSocialProofData, result);
      }
      yield put(
        loadRecommendationsData({
          reduxKey: payload.reduxKey,
          result,
        })
      );
    }
  } catch (e) {
    logger.error({
      error: e,
      extraData: {
        component: 'Recommendation Saga',
        payloadRecieved: payload,
      },
    });
  }
}

function* sendEventsToMonetate(action) {
  const { payload } = action;
  const isUserLoggedIn = yield select(getUserLoggedInState) || false;
  try {
    yield call(RecommendationsAbstractor.sendMobileEventsToMonetate, {
      ...payload,
      isUserLoggedIn,
    });
  } catch (e) {
    logger.error('Error has occurred: ', {
      error: e,
      extraData: {
        component: 'Recommendations Saga - sendEventsToMonetate',
        payloadRecieved: payload,
      },
    });
  }
}

export function* RecommendationsSaga() {
  yield takeLeading(FETCH_RECOMMENDATIONS_DATA_MONETATE, fetchRecommendationsData);
  yield takeEvery(FETCH_RECOMMENDATIONS_DATA_ADOBE, fetchRecommendationsData);
  yield takeLeading(SEND_EVENTS_TO_MONETATE, sendEventsToMonetate);
}

export default RecommendationsSaga;
