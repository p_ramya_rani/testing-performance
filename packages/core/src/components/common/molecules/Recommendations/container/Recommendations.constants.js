// 9fbef606107a605d69c0edbcd8029e5d
import { RECOMMENDATIONS_ACTION_PATTERN } from '../../../../../constants/reducer.constants';

export const FETCH_RECOMMENDATIONS_DATA_MONETATE = `${RECOMMENDATIONS_ACTION_PATTERN}FETCH_RECOMMENDATIONS_DATA_MONETATE`;
export const SEND_EVENTS_TO_MONETATE = `${RECOMMENDATIONS_ACTION_PATTERN}SEND_EVENTS_TO_MONETATE`;
export const FETCH_RECOMMENDATIONS_DATA_ADOBE = `${RECOMMENDATIONS_ACTION_PATTERN}FETCH_RECOMMENDATIONS_DATA_ADOBE`;

export const FETCH_RECOMMENDATIONS_DATA = `${RECOMMENDATIONS_ACTION_PATTERN}FETCH_RECOMMENDATIONS_DATA`;
export const LOAD_RECOMMENDATIONS_DATA = `${RECOMMENDATIONS_ACTION_PATTERN}LOAD_RECOMMENDATIONS_DATA`;
export const UPDATE_RECOMMENDATIONS_DATA = `${RECOMMENDATIONS_ACTION_PATTERN}UPDATE_RECOMMENDATIONS_DATA`;
export const OPEN_RECOMMENDATION_ORDER_MODAL = `${RECOMMENDATIONS_ACTION_PATTERN}OPEN_RECOMMENDATION_ORDER_MODAL`;
export const CLOSE_RECOMMENDATION_ORDER_MODAL = `${RECOMMENDATIONS_ACTION_PATTERN}CLOSE_RECOMMENDATION_ORDER_MODAL`;
export const SET_RECOMMENDATION_LOADING_STATE = `${RECOMMENDATIONS_ACTION_PATTERN}SET_RECOMMENDATION_LOADING_STATE`;

export const RECOMMENDATIONS_PAGES_MAPPING = {
  PDP: 'pdp',
  BAG: 'cart',
  ADDED_TO_BAG: 'added_to_bag',
  HOMEPAGE: 'homepage',
  PLP: 'plp',
  FAVORITES: 'favorites',
  DEPARTMENT_LANDING: 'dlp',
  CHECKOUT: 'checkout',
  OUTFIT: 'outfit',
  NO_PAGE_FOUND: '404_page',
  SEARCH: 'search',
  NULL_SEARCH: 'null_search',
  COLLECTION: 'collection',
  NO_FAVORITES: 'no_favorites',
  NAVL2: 'NavMenuLevel2',
  RECOMMENDATIONS: 'recommendations',
};

export const RECOMMENDATIONS_MBOXNAMES = {
  RECENTLY_VIEWED: 'recently-viewed-products',
  CHECKOUT_SIMILAR_STYLES: 'checkout-similar-style',
  OUT_OF_STOCK_PDP: 'oos-pdp-products',
  PLP_PRODUCTS: 'plp-products',
  ADD_PRODUCT_TO_GET_PLACECASH: 'add-product-get-placecash',
  ADD_TO_BAG_MODAL_CAROUSAL: 'add-to-bag-modal-carousal',
  BAG_CART_REWARD_PLCC: 'bag-cart-reward-plcc',
  STYLE_WITH: 'styleWith',
  ORDER_NOTIFICATION: 'order-notification',
  NAVLEVEL2_Girls: 'navlevel2_girls',
  NAVLEVEL2_Boys: 'navlevel2_boys',
  NAVLEVEL2_Toddler_Girls: 'navlevel2_toddler_girls',
  NAVLEVEL2_Toddler_Boys: 'navlevel2_toddler_boys',
  NAVLEVEL2_Baby: 'navlevel2_baby',
  IN_GRID_PLP_PRODUCTS: 'in-grid-plp-products',
  IN_MULTI_GRID_PLP_PRODUCTS: 'in-multi-grid-plp-products',
  SOCIAL_PROOF_MESSAGE: 'social-proof-msg',
};

export const HIDDEN_MBOX_NAME = 'hidden';

const RECOMMENDATION = 'Recommendation';
const HOMEPAGE = 'homepage';

export const MAX_REC_ITEMS_PER_CAROUSEL = 30;

export default {
  FETCH_RECOMMENDATIONS_DATA,
  FETCH_RECOMMENDATIONS_DATA_MONETATE,
  FETCH_RECOMMENDATIONS_DATA_ADOBE,
  LOAD_RECOMMENDATIONS_DATA,
  RECOMMENDATIONS_PAGES_MAPPING,
  RECOMMENDATIONS_MBOXNAMES,
  SEND_EVENTS_TO_MONETATE,
  RECOMMENDATION,
  HOMEPAGE,
  HIDDEN_MBOX_NAME,
  UPDATE_RECOMMENDATIONS_DATA,
  OPEN_RECOMMENDATION_ORDER_MODAL,
  CLOSE_RECOMMENDATION_ORDER_MODAL,
  SET_RECOMMENDATION_LOADING_STATE,
};
