// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import {
  LOAD_RECOMMENDATIONS_DATA,
  UPDATE_RECOMMENDATIONS_DATA,
  OPEN_RECOMMENDATION_ORDER_MODAL,
  CLOSE_RECOMMENDATION_ORDER_MODAL,
  SET_RECOMMENDATION_LOADING_STATE,
} from './Recommendations.constants';

const initialState = fromJS({});

const getDefaultState = state => {
  // TODO: currently when initial state is hydrated on browser, List is getting converted to an JS Array
  if (state instanceof Object) {
    return fromJS(state);
  }
  return state;
};

const RecommendationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_RECOMMENDATIONS_DATA:
      return state.set(action.payload.reduxKey, action.payload.result);
    case UPDATE_RECOMMENDATIONS_DATA:
      return state.set('isUpdating', action.payload.isUpdating);
    case OPEN_RECOMMENDATION_ORDER_MODAL:
      return state.set('orderRecommendationModal', {
        isOpen: action.payload.isOpen,
        partNumbers: action.payload.partNumbers,
      });
    case CLOSE_RECOMMENDATION_ORDER_MODAL:
      return state.set('orderRecommendationModal', {
        isOpen: action.payload.isOpen,
      });
    case SET_RECOMMENDATION_LOADING_STATE:
      return state.set('isLoading', action.payload.isLoading);
    default:
      return getDefaultState(state);
  }
};

export default RecommendationsReducer;
