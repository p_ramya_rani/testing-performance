// 9fbef606107a605d69c0edbcd8029e5d
import {
  FETCH_RECOMMENDATIONS_DATA,
  FETCH_RECOMMENDATIONS_DATA_MONETATE,
  SEND_EVENTS_TO_MONETATE,
  FETCH_RECOMMENDATIONS_DATA_ADOBE,
  LOAD_RECOMMENDATIONS_DATA,
  UPDATE_RECOMMENDATIONS_DATA,
  OPEN_RECOMMENDATION_ORDER_MODAL,
  CLOSE_RECOMMENDATION_ORDER_MODAL,
  SET_RECOMMENDATION_LOADING_STATE,
} from './Recommendations.constants';

export const fetchRecommendationsData = payload => {
  return {
    type: FETCH_RECOMMENDATIONS_DATA,
    payload,
  };
};

export const fetchRecommendationsDataMonetate = payload => {
  return {
    type: FETCH_RECOMMENDATIONS_DATA_MONETATE,
    payload,
  };
};

export const sendEventsToMonetate = payload => {
  return {
    type: SEND_EVENTS_TO_MONETATE,
    payload,
  };
};

export const fetchRecommendationsDataAdobe = payload => {
  return {
    type: FETCH_RECOMMENDATIONS_DATA_ADOBE,
    payload,
  };
};

export const loadRecommendationsData = payload => {
  return {
    type: LOAD_RECOMMENDATIONS_DATA,
    payload,
  };
};

export const updateRecommendationsLoading = payload => {
  return {
    type: UPDATE_RECOMMENDATIONS_DATA,
    payload,
  };
};

export const openRecommendationOrderModal = payload => {
  return {
    type: OPEN_RECOMMENDATION_ORDER_MODAL,
    payload,
  };
};

export const closeRecommendationOrderModal = payload => {
  return {
    type: CLOSE_RECOMMENDATION_ORDER_MODAL,
    payload,
  };
};

export const setRecommendationLoadingState = payload => {
  return {
    type: SET_RECOMMENDATION_LOADING_STATE,
    payload,
  };
};

export default {
  fetchRecommendationsData,
  loadRecommendationsData,
};
