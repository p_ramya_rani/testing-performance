// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { Platform, SafeAreaView } from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const style = {
  flex: 1,
};

const withKeyboardAvoidingView = WrappedComponent => props => {
  const keyboardProps = {
    extraScrollHeight: Platform.OS === 'ios' ? 40 : 0,
    enableOnAndroid: true,
    keyboardShouldPersistTaps: 'handled',
  };
  return (
    <SafeAreaView style={style}>
      <KeyboardAwareScrollView
        {...keyboardProps}
        contentContainerStyle={Platform.OS !== 'ios' ? style : {}}
      >
        <WrappedComponent {...props} />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
export default withKeyboardAvoidingView;

export const WithKeyboardAvoidingViewHOC = ({ children, keyboardAwareScrollViewRef, ...props }) => {
  const { extraCustomScrollHeight } = props;
  const keyboardProps = {
    extraScrollHeight: Platform.OS === 'ios' ? extraCustomScrollHeight : 0,
    enableOnAndroid: true,
    keyboardShouldPersistTaps: 'handled',
  };

  return (
    <SafeAreaView style={style}>
      <KeyboardAwareScrollView
        {...props}
        {...keyboardProps}
        ref={keyboardAwareScrollViewRef}
        contentContainerStyle={Platform.OS !== 'ios' ? style : {}}
      >
        {children}
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

WithKeyboardAvoidingViewHOC.propTypes = {
  children: PropTypes.string,
  keyboardAwareScrollViewRef: PropTypes.shape({}),
  extraCustomScrollHeight: PropTypes.number,
};

WithKeyboardAvoidingViewHOC.defaultProps = {
  children: '',
  extraCustomScrollHeight: 40,
  keyboardAwareScrollViewRef: null,
};
