// 9fbef606107a605d69c0edbcd8029e5d 
export const DEFAULT_CLASS_NAME = 'safeComponent-error';
export const LIFECYCLE_METHODS = [
  'UNSAFE_componentWillMount',
  'componentDidMount',
  'UNSAFE_componentWillReceiveProps',
  'shouldComponentUpdate',
  'componentWillUpdate',
  'componentDidUpdate',
  'componentWillUnmount',
  'render',
];
