// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { isProduction } from '../../../../utils';

const FallbackErrorComponent = ({ errorMessage, componentName }) => {
  const isProd = isProduction();
  if (isProd) {
    return null;
  }
  return (
    <aside role="alert">
      Something went wrong in--
      <span>
        <strong>{componentName}</strong>
      </span>
      Component.
      <p>{errorMessage}</p>
    </aside>
  );
};

FallbackErrorComponent.propTypes = {
  errorMessage: PropTypes.string,
  componentName: PropTypes.string,
};

FallbackErrorComponent.defaultProps = {
  errorMessage: '',
  componentName: '',
};

export default FallbackErrorComponent;
