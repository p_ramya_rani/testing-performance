// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable no-param-reassign */
import React from 'react';
import logger from '@tcp/core/src/utils/loggerInstance';
import { generateSessionId } from '@tcp/core/src/utils/cookie.util';
import { isMobileApp } from '@tcp/core/src/utils';
import { API_CONFIG } from '@tcp/core/src/services/config';
import FallbackErrorComponent from './ErrorFallbackComponent';
import { DEFAULT_CLASS_NAME, LIFECYCLE_METHODS } from './config';

/**
 * Generate error component
 * @param {object} error
 * @param {string} componentName
 */
export const renderErrorComponent = (error, componentName) => {
  return React.createElement(
    'div',
    {
      className: DEFAULT_CLASS_NAME,
    },
    FallbackErrorComponent.call(this, { errorMessage: error.message, componentName })
  );
};

export const logError = ({ error, errorInfo }) => {
  const { componentName } = errorInfo || {};
  const { sessionCookieKey } = API_CONFIG;
  logger.error('Error in component - ', componentName, ' - ', {
    error,
    extraData: {
      errorInfo,
      [sessionCookieKey]: !isMobileApp() && generateSessionId(true),
    },
    errorTags: [componentName],
  });
  logger.debug('Error in component - ', componentName, ' - ', error, errorInfo);
  return true;
};

/**
 * Generate error safe client component
 * @param {function} renderComponent generate component render
 * @param {string} componentName
 */
export const renderClientSafeComponent = (renderComponent, componentName) => {
  return class ErrorBoundary extends React.Component {
    constructor(props) {
      super(props);
      this.state = { hasError: false, error: null };
      this.componentName = componentName;
    }

    static getDerivedStateFromError(error) {
      return { hasError: true, error };
    }

    componentDidCatch(error, errorInfo) {
      logError({ error, errorInfo });
    }

    render() {
      return renderComponent(this.props, this.state);
    }
  };
};

/**
 * Generate error safe functional component
 * @param {*} WrappedComponent
 */
export const functionalSafeComponent = WrappedComponent => {
  const renderComponent = (passedProps, passedState) => {
    try {
      const { hasError, error } = passedState;
      return hasError
        ? renderErrorComponent(error, WrappedComponent.name)
        : WrappedComponent(passedProps);
    } catch (err) {
      logError({
        error: err,
        extraData: {
          componentName: WrappedComponent.displayName || WrappedComponent.name,
          ...passedProps,
          ...passedState,
        },
      });
      return renderErrorComponent(err, WrappedComponent.name);
    }
  };
  return renderClientSafeComponent(renderComponent, WrappedComponent.name);
};

export const functionalSafeComponentStyled = WrappedComponent => {
  const renderComponent = (passedProps, passedState) => {
    try {
      const { hasError, error } = passedState;
      return hasError ? (
        renderErrorComponent(error, WrappedComponent.name)
      ) : (
        <WrappedComponent {...passedProps} {...passedState} />
      );
    } catch (err) {
      logError({
        error: err,
        extraData: {
          componentName: WrappedComponent.displayName || WrappedComponent.name,
          ...passedProps,
          ...passedState,
        },
      });
      return renderErrorComponent(err, WrappedComponent.name);
    }
  };
  return renderClientSafeComponent(renderComponent, WrappedComponent.name);
};

/**
 * Create error safe component methods
 * @param {string} methodName
 * @param {*} WrappedComponent
 */
export const wrapMethod = (methodName, WrappedComponent) => {
  const originalMethod = WrappedComponent.prototype[methodName];
  if (!originalMethod) {
    return;
  }

  // define default state
  WrappedComponent.prototype.state = {
    hasError: false,
    error: null,
    ...WrappedComponent.prototype.state,
  };

  WrappedComponent.prototype[methodName] = function _componentMethod() {
    try {
      if (methodName === 'render') {
        const { hasError, error } = this.state;
        return hasError
          ? renderErrorComponent(error, WrappedComponent.name)
          : // eslint-disable-next-line prefer-rest-params
            originalMethod.apply(this, arguments);
      }
      // eslint-disable-next-line prefer-rest-params
      return originalMethod.apply(this, arguments);
    } catch (err) {
      logError({ error: err, errorInfo: WrappedComponent.name });
      if (methodName === 'render') {
        return renderErrorComponent(err, WrappedComponent.name);
      }
      if (methodName === 'shouldComponentUpdate') {
        return false;
      }
      return false;
    }
  };
};

/**
 * Generate error safe non functional component
 * @param {*} WrappedComponent
 */
export const nonFunctionalSafeComponent = WrappedComponent => {
  LIFECYCLE_METHODS.forEach(method => {
    return WrappedComponent.target
      ? wrapMethod(method, WrappedComponent.target)
      : wrapMethod(method, WrappedComponent);
  });
  const renderComponent = passedProps => <WrappedComponent {...passedProps} />;
  return renderClientSafeComponent(renderComponent, WrappedComponent.name);
};

/**
 * Generate error safe component
 * @param {*} WrappedComponent
 */
const SafeComponent = WrappedComponent => {
  if (WrappedComponent.target) {
    return !(WrappedComponent.target.prototype && WrappedComponent.target.prototype.render)
      ? functionalSafeComponentStyled(WrappedComponent)
      : nonFunctionalSafeComponent(WrappedComponent);
  }

  return !(WrappedComponent.prototype && WrappedComponent.prototype.render)
    ? functionalSafeComponent(WrappedComponent)
    : nonFunctionalSafeComponent(WrappedComponent);
};

export default SafeComponent;
