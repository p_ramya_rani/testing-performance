// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React, { Component } from 'react';
import { isClient } from '@tcp/core/src/utils';
import hoistNonReactStatic from 'hoist-non-react-statics';
import { PropTypes } from 'prop-types';
import { withRouter } from 'next/router'; // eslint-disable-line
import { connect } from 'react-redux';

export default function ({ WrappedComponent, mapStateToProps, mapDispatchToProps }) {
  class IsomorphicRenderer extends Component {
    static async getInitialProps({ store, query, req, isServer, res, asPath }) {
      const props = { ...mapStateToProps(store.getState()), ...mapDispatchToProps(store.dispatch) };
      const deviceBot = req && req.device && req.device.type === 'bot';
      const ssrSearchPage = req && req.headers && req.headers['x-force-page-ssr'];
      const intialProps = {
        props,
        query,
        isServer,
        req,
        res,
        deviceBot,
        state: store.getState(),
        store,
        ssrSearchPage,
        asPath,
        apiConfig: res.locals.apiConfig || {},
      };
      let pageProps = {};
      if (isServer) {
        pageProps = await WrappedComponent.getInitialProps(intialProps);
      }
      return { ...WrappedComponent.pageInfo, ...pageProps, deviceBot };
    }

    componentDidMount() {
      const { deviceType, ...props } = this.props;
      // ComponentDidMount should work as expected in case of CSR with BOT
      if (deviceType !== 'bot' || (isClient() && deviceType === 'bot')) {
        WrappedComponent.getInitialProps({ props });
      }
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  IsomorphicRenderer.propTypes = {
    deviceType: PropTypes.string.isRequired,
  };

  const selfMapStateToProps = (state) => {
    const childProps = mapStateToProps(state);
    return {
      ...childProps,
      deviceType: state.DeviceInfo && state.DeviceInfo.deviceType,
      routerParam: state.routerParam || state.router,
    };
  };

  hoistNonReactStatic(IsomorphicRenderer, WrappedComponent, {
    getInitialProps: true,
  });

  const connectIsomorphicComponent = connect(
    selfMapStateToProps,
    mapDispatchToProps
  )(IsomorphicRenderer);

  const IsomorphicWithRouter = withRouter(connectIsomorphicComponent);

  hoistNonReactStatic(IsomorphicWithRouter, connectIsomorphicComponent, {
    getInitialProps: true,
  });

  return IsomorphicWithRouter;
}
