// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export default (WrappedComponent, styles) => styled(WrappedComponent)`
  ${styles};
`;
