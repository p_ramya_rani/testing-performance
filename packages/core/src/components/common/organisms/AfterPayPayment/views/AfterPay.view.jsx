// 9fbef606107a605d69c0edbcd8029e5d
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isClient, getAPIConfig } from '@tcp/core/src/utils';

class AfterPay extends Component {
  constructor(props) {
    super(props);
    this.afterpayButton = null;
    this.myRef = React.createRef();
  }

  componentDidUpdate() {
    if (isClient()) {
      this.initializeAfterPay();
    }
  }

  getOrderAddress = () => {
    const { shippingAddress = {}, isOrderHasPickup, pickupStoresInCart } = this.props;

    const address =
      pickupStoresInCart && pickupStoresInCart.size && pickupStoresInCart.getIn([0, 'address']);

    return isOrderHasPickup && address
      ? {
          addressLine1: address.get('addressLine1'),
          addressLine2: address.get('addressLine2'),
          city: address.get('city'),
          country: address.get('country'),
          firstName: address.get('firstName'),
          lastName: address.get('lastName'),
          state: address.get('state'),
          zipCode: address.get('zipCode'),
          nickName: address.get('addessKey'),
          addressId: address.get('addressId'),
        }
      : shippingAddress;
  };

  getCustomerInfo = () => {
    const { shippingPhoneAndEmail = {}, pickupInformation, isOrderHasPickup } = this.props;

    return pickupInformation && isOrderHasPickup ? pickupInformation : shippingPhoneAndEmail;
  };

  setStyles = () => {
    const shadowButton = this.afterpayButton?.shadowRoot?.querySelector('button');
    if (shadowButton) {
      shadowButton.setAttribute('style', 'cursor: pointer');
    }
  };

  getErrMsgAfterpay = (cartHaveGCItem, errorLabels) =>
    cartHaveGCItem ? errorLabels.AFTERPAY_CHECKOUT_GC_ERROR : errorLabels.AFTERPAY_CHECKOUT_ERROR;

  initializeAfterPay = () => {
    const {
      cartOrderItems,
      ledgerSummaryData = {},
      checkoutWithAfterPay,
      reviewFormValues,
      setErrorState,
      errorLabels,
      setLoader,
      currentOrderId,
      cartHaveGCItem,
    } = this.props;
    const { orderBalanceTotal } = ledgerSummaryData;

    const {
      addressLine1,
      addressLine2,
      city,
      country,
      firstName,
      lastName,
      state,
      zipCode,
      addressId,
    } = this.getOrderAddress();
    const {
      emailAddress,
      phoneNumber,
      firstName: firstNamePickup,
      lastName: lastNamePickup,
    } = this.getCustomerInfo();

    const itemsList =
      cartOrderItems &&
      cartOrderItems.size &&
      cartOrderItems.map(item => {
        const productInfo = item.get('productInfo');
        const itemInfo = item.get('itemInfo');
        return {
          name: productInfo.get('name'),
          sku: productInfo.get('skuId'),
          quantity: itemInfo.get('quantity'),
          pageUrl: productInfo.get('pdpUrl'),
          imageUrl: productInfo.get('imagePath'),
          price: {
            amount: itemInfo.get('offerPrice'),
            currency: 'USD',
          },
        };
      });

    const node = this.myRef.current;
    const [firstButton] = node.getElementsByTagName('afterpay-button');
    const errMsgAfterpay = this.getErrMsgAfterpay(cartHaveGCItem, errorLabels);

    this.afterpayButton = firstButton;
    this.setStyles();
    const onClickHandler = event => {
      if (cartHaveGCItem) {
        event.stopPropagation();
        setErrorState({
          errorMessage: errMsgAfterpay,
          component: 'PAGE',
        });
      } else {
        setLoader(true);
        const fName = firstName || firstNamePickup;
        const lName = lastName || lastNamePickup;
        this.afterpayButton.merchantReference = `${currentOrderId}`;

        this.afterpayButton.amount = (orderBalanceTotal || 0).toFixed(2).toString();

        this.afterpayButton.currency = 'USD';
        this.afterpayButton.items = JSON.stringify(itemsList);
        this.afterpayButton.consumerPhoneNumber = phoneNumber;
        this.afterpayButton.consumerGivenNames = fName;
        this.afterpayButton.consumerSurname = lName;
        this.afterpayButton.consumerEmail = emailAddress;

        this.afterpayButton.shippingName = `${fName} ${lName}`;
        this.afterpayButton.shippingLine1 = addressLine1;
        this.afterpayButton.shippingLine2 = addressLine2;
        this.afterpayButton.shippingArea1 = city;
        this.afterpayButton.shippingRegion = state;
        this.afterpayButton.shippingPostcode = zipCode;
        this.afterpayButton.shippingCountryCode = country;
        this.afterpayButton.shippingPhoneNumber = phoneNumber;

        this.afterpayButton.billingName = `${fName} ${lName}`;
        this.afterpayButton.billingLine1 = addressLine1;
        this.afterpayButton.billingLine2 = addressLine2;
        this.afterpayButton.billingArea1 = city;
        this.afterpayButton.billingRegion = state;
        this.afterpayButton.billingPostcode = zipCode;
        this.afterpayButton.billingCountryCode = country;
        this.afterpayButton.billingPhoneNumber = phoneNumber;
      }
    };

    this.afterpayButton.removeEventListener('click', onClickHandler, true);

    this.afterpayButton.addEventListener('click', onClickHandler, true);

    this.afterpayButton.onComplete = event => {
      const { status, virtualCard } = event.data;
      // The consumer confirmed the payment schedule.
      // The virtualCard details can be inserted into your checkout
      if (status === 'SUCCESS') {
        const { cardNumber, cvc: cvv, expiry, cardType } = virtualCard;
        const expiryValues = expiry ? expiry.split('-') : '';

        const yearExpire = expiryValues && expiryValues.length ? expiryValues[0] : '';
        const monthExpire = expiryValues && expiryValues.length > 1 ? expiryValues[1] : '';

        const ccInfo = {
          cardNumber,
          monthExpire,
          yearExpire,
          cvv,
          reviewFormValues,
          cardType,
          addressId,
        };

        checkoutWithAfterPay(ccInfo);
      } else {
        setLoader(false);
        setErrorState({
          errorMessage: errMsgAfterpay,
          component: 'PAGE',
        });
      }
    };

    this.afterpayButton.onError = () => {
      setLoader(false);
      setErrorState({
        errorMessage: errMsgAfterpay,
        component: 'PAGE',
      });
    };
  };

  render() {
    const { className } = this.props;
    const { afterpayButtonKey } = getAPIConfig();
    return (
      <div className={className} ref={this.myRef}>
        <afterpay-button merchantPublicKey={afterpayButtonKey} />
      </div>
    );
  }
}

AfterPay.propTypes = {
  className: PropTypes.string,
  checkoutWithAfterPay: PropTypes.func,
  shippingAddress: PropTypes.shape({}).isRequired,
  shippingPhoneAndEmail: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape({}).isRequired,
  ledgerSummaryData: PropTypes.shape({}).isRequired,
  currentOrderId: PropTypes.string.isRequired,
  reviewFormValues: PropTypes.string.isRequired,
  pickupStoresInCart: PropTypes.shape({}),
  pickupInformation: PropTypes.shape({}),
  isOrderHasPickup: PropTypes.bool,
  setErrorState: PropTypes.func,
  errorLabels: PropTypes.shape({}),
  setLoader: PropTypes.func,
  cartHaveGCItem: PropTypes.bool,
};

AfterPay.defaultProps = {
  className: '',
  checkoutWithAfterPay: () => {},
  pickupStoresInCart: {},
  pickupInformation: {},
  isOrderHasPickup: false,
  setErrorState: () => {},
  errorLabels: {},
  setLoader: () => {},
  cartHaveGCItem: false,
};

export default AfterPay;
