// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import AfterPay from '../AfterPay.view.native';

describe('AfterPay Button', () => {
  let component;
  beforeEach(() => {
    component = shallow(
      <AfterPay
        shippingAddress={{}}
        shippingPhoneAndEmail={{}}
        cartOrderItems={{}}
        ledgerSummaryData={{}}
        currentOrderId=""
        reviewFormValues=""
      />
    );
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render AfterPay Button', () => {
    expect(component).toMatchSnapshot();
  });
});
