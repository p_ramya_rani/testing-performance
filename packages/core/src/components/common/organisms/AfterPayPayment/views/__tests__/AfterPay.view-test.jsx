// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import AfterPay from '../AfterPay.view';

describe('AfterPay Button', () => {
  it('should render AfterPay Button', () => {
    const component = shallow(<AfterPay />);
    expect(component).toMatchSnapshot();
  });
});
