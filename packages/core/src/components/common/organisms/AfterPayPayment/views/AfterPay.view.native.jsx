/* eslint-disable react/prop-types */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { getAPIConfig } from '@tcp/core/src/utils';
import Button from '@tcp/core/src/components/common/atoms/Button';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import { STATUS, ENDPOINTS, REDIRECT_URL } from '../AfterPay.constants';
import { AfterPayCheckoutButton, AfterPayMessageWrapper } from '../styles/AfterPay.style.native';
import AfterPayCheckoutWebView from '../organisms/AfterPayCheckoutWebView';

const getCustomerInfo = ({ shippingPhoneAndEmail = {}, pickupInformation, isOrderHasPickup }) => {
  return pickupInformation && isOrderHasPickup ? pickupInformation : shippingPhoneAndEmail;
};

const getOrderAddress = ({ shippingAddress = {}, isOrderHasPickup, pickupStoresInCart }) => {
  const address =
    pickupStoresInCart && pickupStoresInCart.size && pickupStoresInCart.getIn([0, 'address']);

  return isOrderHasPickup && address
    ? {
        addressLine1: address.get('addressLine1'),
        addressLine2: address.get('addressLine2'),
        city: address.get('city'),
        country: address.get('country'),
        firstName: address.get('firstName'),
        lastName: address.get('lastName'),
        state: address.get('state'),
        zipCode: address.get('zipCode'),
        nickName: address.get('addessKey'),
        addressId: address.get('addressId'),
      }
    : shippingAddress;
};

const disableAfterPay = (orderBalanceTotal, afterPayMinOrderAmount, afterPayMaxOrderAmount) => {
  const totalAmount = orderBalanceTotal && orderBalanceTotal * 1;

  if (afterPayMinOrderAmount > totalAmount || afterPayMaxOrderAmount < totalAmount) {
    return true;
  }

  return totalAmount && totalAmount < 1;
};

const fireAnalyticsClick = (trackClickAfterPay, setAnalyticsTriggered) => {
  if (typeof setAnalyticsTriggered === 'function') {
    setAnalyticsTriggered();
  }

  if (typeof trackClickAfterPay === 'function') {
    trackClickAfterPay(
      {
        afterpay_metrics_v141: 1,
        afterpay_location_v131: 'Review',
        customEvents: ['event141'],
      },
      { name: 'afterpay_click', module: 'checkout' }
    );
  }
};

const AfterPay = ({
  status,
  updateAppStatus,
  updateAppTokens,
  tokens,
  updateAppCreditCardInfo,
  ...props
}) => {
  const { FETCH_CHECKOUT_TOKEN, REDIRECT_TO_PORTAL, ERROR } = STATUS;

  const { BUTTON } = ENDPOINTS;

  const {
    cartOrderItems,
    ledgerSummaryData = {},
    checkoutWithAfterPay,
    reviewFormValues,
    currentOrderId,
    setErrorState,
    errorLabels,
    setLoader,
    cartHaveGCItem,
    btnText,
    afterPayMinOrderAmount,
    afterPayMaxOrderAmount,
    labelAfterPayThresholdError,
    trackClickAfterPay,
    setAnalyticsTriggered,
  } = props;

  const errMsgAfterpay = cartHaveGCItem
    ? errorLabels.AFTERPAY_CHECKOUT_GC_ERROR
    : errorLabels.AFTERPAY_CHECKOUT_ERROR;

  const { orderBalanceTotal } = ledgerSummaryData;

  const {
    addressLine1,
    addressLine2,
    city,
    country,
    firstName,
    lastName,
    state,
    zipCode,
    addressId,
  } = getOrderAddress(props);
  const {
    emailAddress,
    phoneNumber,
    firstName: firstNamePickup,
    lastName: lastNamePickup,
  } = getCustomerInfo(props);

  // handler function for Afterpay Button click
  const handleAfterpayButtonClick = async () => {
    if (cartHaveGCItem) {
      setErrorState({
        errorMessage: errMsgAfterpay,
        component: 'PAGE',
      });
    } else {
      setLoader(true);
      const {
        afterpayButtonKey,
        afterpayApiBaseUrl: BASE_URL,
        afterpayShopDirectoryId,
      } = getAPIConfig();
      updateAppStatus(FETCH_CHECKOUT_TOKEN);
      // create the afterpay checkout

      const itemsList =
        cartOrderItems &&
        cartOrderItems.size &&
        cartOrderItems.map((item) => {
          const productInfo = item.get('productInfo');
          const itemInfo = item.get('itemInfo');
          return {
            name: productInfo.get('name'),
            sku: productInfo.get('skuId'),
            quantity: itemInfo.get('quantity'),
            pageUrl: productInfo.get('pdpUrl'),
            imageUrl: productInfo.get('imagePath'),
            price: {
              amount: itemInfo.get('offerPrice'),
              currency: 'USD',
            },
          };
        });

      const fName = firstName || firstNamePickup;
      const lName = lastName || lastNamePickup;

      const contactObj = {
        name: `${fName} ${lName}`,
        line1: addressLine1,
        line2: addressLine2,
        area1: city,
        region: state,
        postcode: zipCode,
        countryCode: country,
        phoneNumber,
      };

      const checkoutResource = await fetch(`${BASE_URL}${BUTTON}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          shopDirectoryId: afterpayShopDirectoryId,
          shopDirectoryMerchantId: afterpayButtonKey,
          amount: {
            amount: (orderBalanceTotal || 0).toFixed(2).toString(),
            currency: 'USD',
          },
          consumer: {
            phoneNumber,
            givenNames: fName,
            surname: lName,
            email: emailAddress,
          },
          items: itemsList,
          merchant: {
            redirectConfirmUrl: REDIRECT_URL,
            redirectCancelUrl: REDIRECT_URL,
          },
          shipping: contactObj,
          billing: contactObj,
          merchantReference: `${currentOrderId}`,
        }),
      });

      const checkoutResponse = await checkoutResource.json();
      setLoader(false);
      // if the response has the errorId set
      // change the status to ERROR
      if (checkoutResponse.errorId) updateAppStatus(ERROR);
      // if the singleUseCardToken is set
      else if (checkoutResponse.singleUseCardToken) {
        // change the status to REDIRECT_TO_PORTAL
        updateAppStatus(REDIRECT_TO_PORTAL);
        // update the tokens state with the response
        updateAppTokens(checkoutResponse);
      }
    }
    fireAnalyticsClick(trackClickAfterPay, setAnalyticsTriggered);
  };

  const shouldDisableAfterPay = disableAfterPay(
    orderBalanceTotal,
    afterPayMinOrderAmount,
    afterPayMaxOrderAmount
  );
  let thresholdErrorMessage = labelAfterPayThresholdError.replace(
    '[AfterPayMinOrderAmount]',
    afterPayMinOrderAmount
  );
  thresholdErrorMessage = thresholdErrorMessage.replace(
    '[AfterPayMaxOrderAmount]',
    afterPayMaxOrderAmount
  );

  return (
    <View>
      <AfterPayCheckoutButton
        onPress={handleAfterpayButtonClick}
        as={Button}
        color="white"
        fontWeight="extrabold"
        fontFamily="secondary"
        fontSize="fs13"
        text={btnText}
        disableButton={shouldDisableAfterPay}
      />
      {status === REDIRECT_TO_PORTAL && tokens !== {} && (
        <AfterPayCheckoutWebView
          tokens={tokens}
          status={status}
          updateAppStatus={updateAppStatus}
          updateAppTokens={updateAppTokens}
          updateAppCreditCardInfo={updateAppCreditCardInfo}
          addressId={addressId}
          checkoutWithAfterPay={checkoutWithAfterPay}
          reviewFormValues={reviewFormValues}
          setLoader={setLoader}
          setErrorState={setErrorState}
          errMessageTxt={errorLabels.AFTERPAY_CHECKOUT_ERROR}
        />
      )}
      {shouldDisableAfterPay && (
        <AfterPayMessageWrapper>
          <Notification
            status="warning"
            scrollIntoView
            fontWeight="bold"
            disableSpace
            message={thresholdErrorMessage}
          />
        </AfterPayMessageWrapper>
      )}
      {!shouldDisableAfterPay && (
        <AfterPayMessageWrapper>
          <AfterPayMessaging
            offerPrice={orderBalanceTotal}
            isOrderThresholdEnabled
            isPriceBold
            hideIntroText
          />
        </AfterPayMessageWrapper>
      )}
    </View>
  );
};

AfterPay.propTypes = {
  status: PropTypes.string.isRequired,
  updateAppStatus: PropTypes.string.isRequired,
  updateAppTokens: PropTypes.string.isRequired,
  tokens: PropTypes.string.isRequired,
  updateAppCreditCardInfo: PropTypes.func.isRequired,
  checkoutWithAfterPay: PropTypes.func,
  shippingAddress: PropTypes.shape({}).isRequired,
  shippingPhoneAndEmail: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape({}).isRequired,
  ledgerSummaryData: PropTypes.shape({}).isRequired,
  currentOrderId: PropTypes.string.isRequired,
  reviewFormValues: PropTypes.string.isRequired,
  pickupStoresInCart: PropTypes.shape({}),
  pickupInformation: PropTypes.shape({}),
  isOrderHasPickup: PropTypes.bool,
  setErrorState: PropTypes.func,
  errorLabels: PropTypes.shape({}),
  setLoader: PropTypes.func,
  cartHaveGCItem: PropTypes.bool,
  btnText: PropTypes.string,
  afterPayMinOrderAmount: PropTypes.number,
  afterPayMaxOrderAmount: PropTypes.number,
  labelAfterPayThresholdError: PropTypes.string,
  trackClickAfterPay: PropTypes.func,
  setAnalyticsTriggered: PropTypes.func,
};

AfterPay.defaultProps = {
  checkoutWithAfterPay: () => {},
  pickupStoresInCart: {},
  pickupInformation: {},
  isOrderHasPickup: false,
  setErrorState: () => {},
  errorLabels: {},
  setLoader: () => {},
  cartHaveGCItem: false,
  btnText: '',
  afterPayMinOrderAmount: 1,
  afterPayMaxOrderAmount: 1000,
  labelAfterPayThresholdError: '',
  trackClickAfterPay: () => {},
  setAnalyticsTriggered: () => {},
};

export default AfterPay;
