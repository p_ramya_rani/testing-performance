// 9fbef606107a605d69c0edbcd8029e5d
import { getAfterPayStatus, getAfterPayTokens } from '../container/AfterPay.selectors';

describe('#Afterpay selector', () => {
  it('#getAfterPayStatus should return getAfterPayStatus state', () => {
    const state = {
      AFTERPAY_ACTION_PATTERN: { status: 'test' },
    };

    expect(getAfterPayStatus(state)).toEqual('test');
  });

  it('#getAfterPayTokens should return getAfterPayTokens state', () => {
    const state = {
      AFTERPAY_ACTION_PATTERN: { tokens: 'test' },
    };

    expect(getAfterPayTokens(state)).toEqual('test');
  });
});
