// 9fbef606107a605d69c0edbcd8029e5d
import { UPDATE_STATUS, UPDATE_TOKENS, UPDATE_CREDIT_CARD_INFO } from '../AfterPay.constants';
import {
  updateAppStatusAction,
  updateAppTokensAction,
  updateAppCreditCardInfoAction,
} from '../container/AfterPay.actions';

describe('AfterPay Actions', () => {
  it('should dispatch UPDATE_STATUS ', () => {
    expect(updateAppStatusAction().type).toBe(UPDATE_STATUS);
  });

  it('should dispatch UPDATE_TOKENS ', () => {
    expect(updateAppTokensAction().type).toBe(UPDATE_TOKENS);
  });

  it('should dispatch UPDATE_CREDIT_CARD_INFO ', () => {
    expect(updateAppCreditCardInfoAction().type).toBe(UPDATE_CREDIT_CARD_INFO);
  });
});
