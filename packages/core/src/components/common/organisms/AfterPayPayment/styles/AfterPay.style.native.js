// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const AfterPayCheckoutButton = styled.TouchableOpacity`
  background: ${(props) => props.theme.colorPalette.blue.C900};
  display: flex;
  height: 42px;
  justify-content: center;
  align-items: center;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const AfterPayMessageWrapper = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export default AfterPayCheckoutButton;
