// 9fbef606107a605d69c0edbcd8029e5d
import { AFTERPAY_REDUCER_KEY } from '../../../../../constants/reducer.constants';

export const getAfterPayStatus = (state) => {
  return (state[AFTERPAY_REDUCER_KEY] && state[AFTERPAY_REDUCER_KEY].status) || false;
};

export const getAfterPayTokens = (state) => {
  return (state[AFTERPAY_REDUCER_KEY] && state[AFTERPAY_REDUCER_KEY].tokens) || '';
};
