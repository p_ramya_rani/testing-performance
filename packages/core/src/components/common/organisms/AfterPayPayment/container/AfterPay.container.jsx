// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import { getLedgerSummaryData } from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import selectors from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import CHECKOUT_ACTIONS, {
  checkoutWithAfterPay,
} from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.action';
import setLoaderState from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import {
  updateAppStatusAction,
  updateAppTokensAction,
  updateAppCreditCardInfoAction,
} from './AfterPay.actions';
import * as AfterPaySelectors from './AfterPay.selectors';

import AfterPay from '../views/AfterPay.view';

const AfterPayContainer = (props) => {
  return <AfterPay {...props} />;
};

const mapStateToProps = (state) => {
  return {
    cartOrderItems: BagPageSelector.getOrderItems(state),
    ledgerSummaryData: getLedgerSummaryData(state),
    shippingAddress: selectors.getShippingAddress(state),
    shippingPhoneAndEmail: selectors.getShippingPhoneAndEmail(state),
    currentOrderId: selectors.getCurrentOrderId(state),
    subOrderId: selectors.getSubOrderId(state),
    reviewFormValues: selectors.getReviewFormValues(state),
    pickupStoresInCart: BagPageSelector.getCartStores(state),
    isOrderHasPickup: selectors.getIsOrderHasPickup(state),
    pickupInformation: selectors.getPickupValues(state),
    errorLabels: BagPageSelector.getErrorMapping(state),
    cartHaveGCItem: selectors.getIsCartHaveGiftCards(state),
    status: AfterPaySelectors.getAfterPayStatus(state),
    tokens: AfterPaySelectors.getAfterPayTokens(state),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    checkoutWithAfterPay: (payload) => {
      const { navigation } = ownProps;
      const submitPayload = { ...payload, navigation };
      dispatch(checkoutWithAfterPay(submitPayload));
    },
    setErrorState: (payload) => {
      dispatch(CHECKOUT_ACTIONS.setServerErrorCheckout(payload));
    },
    setLoader: (payload) => dispatch(setLoaderState(payload)),
    updateAppStatus: (payload) => dispatch(updateAppStatusAction(payload)),
    updateAppTokens: (payload) => dispatch(updateAppTokensAction(payload)),
    updateAppCreditCardInfo: (payload) => dispatch(updateAppCreditCardInfoAction(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AfterPayContainer);
