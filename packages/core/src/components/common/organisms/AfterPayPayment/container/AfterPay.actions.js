// 9fbef606107a605d69c0edbcd8029e5d

import { UPDATE_STATUS, UPDATE_TOKENS, UPDATE_CREDIT_CARD_INFO } from '../AfterPay.constants';

export const updateAppStatusAction = (payload) => ({
  type: UPDATE_STATUS,
  payload,
});

export const updateAppTokensAction = (payload) => ({
  type: UPDATE_TOKENS,
  payload,
});

export const updateAppCreditCardInfoAction = (payload) => ({
  type: UPDATE_CREDIT_CARD_INFO,
  payload,
});
