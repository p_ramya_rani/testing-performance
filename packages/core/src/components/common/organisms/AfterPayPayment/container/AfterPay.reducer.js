// 9fbef606107a605d69c0edbcd8029e5d
import {
  STATUS,
  UPDATE_STATUS,
  UPDATE_TOKENS,
  UPDATE_CREDIT_CARD_INFO,
} from '../AfterPay.constants';

const { START } = STATUS;

const initialState = {
  status: START,
  tokens: {},
  creditCardInfo: {
    ccNo: '',
    cvc: '',
    expiryMonth: '',
    expiryYear: '',
  },
};

export default function (state = initialState, action) {
  let returnVal = state;
  const { type, payload } = action;

  switch (type) {
    case UPDATE_STATUS:
      returnVal = {
        ...state,
        status: payload,
      };
      break;
    case UPDATE_TOKENS:
      returnVal = {
        ...state,
        tokens: payload,
      };
      break;
    case UPDATE_CREDIT_CARD_INFO:
      returnVal = {
        ...state,
        creditCardInfo: payload,
      };
      break;
    default:
      break;
  }
  return returnVal;
}
