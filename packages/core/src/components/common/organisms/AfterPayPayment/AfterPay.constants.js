// 9fbef606107a605d69c0edbcd8029e5d

export const ENDPOINTS = {
  BUTTON: 'button',
  CONFIRM: 'button/confirm',
};
export const STATUS = {
  START: 'START',
  FETCH_CHECKOUT_TOKEN: 'FETCH_CHECKOUT_TOKEN',
  REDIRECT_TO_PORTAL: 'REDIRECT_TO_PORTAL',
  ERROR: 'ERROR',
  SUCCESS: 'SUCCESS',
  CANCELLED: 'CANCELLED',
};
export const REDIRECT_URL = 'https://www.afterpay.com';

export const UPDATE_STATUS = 'UPDATE_STATUS';
export const UPDATE_TOKENS = 'UPDATE_TOKENS';
export const UPDATE_CREDIT_CARD_INFO = 'UPDATE_CREDIT_CARD_INFO';
