// 9fbef606107a605d69c0edbcd8029e5d
import React, { Component } from 'react';
import { View, Dimensions, KeyboardAvoidingView } from 'react-native';
import PropTypes from 'prop-types';
import { WebView } from 'react-native-webview';
import ModalNative from '@tcp/core/src/components/common/molecules/Modal/view/Modal.native';
import { isIOS, getAPIConfig } from '@tcp/core/src/utils';
import { ENDPOINTS, STATUS, REDIRECT_URL } from '../../../AfterPay.constants';

/**
 * CheckoutWebView needs to be a class component for this.checkoutWebViewRef.stopLoading(); to work
 * The reason for this is still being investigated
 * Most of the examples of stopLoading using WebView ref make use of class components
 * Refer https://blog.logrocket.com/the-complete-guide-to-react-native-webview/
 */

class AfterPayCheckoutWebView extends Component {
  constructor(props) {
    super(props);
    this.handleNavigationStateChange = this.handleNavigationStateChange.bind(this);
  }

  getccInfo = ({ virtualCard }) => {
    const { addressId, reviewFormValues } = this.props;
    const { cardNumber, cvc: cvv, expiry, cardType } = virtualCard;
    const expiryValues = expiry ? expiry.split('-') : '';

    const yearExpire = expiryValues && expiryValues.length ? expiryValues[0] : '';
    const monthExpire = expiryValues && expiryValues.length > 1 ? expiryValues[1] : '';

    return {
      cardNumber,
      monthExpire,
      yearExpire,
      cvv,
      reviewFormValues,
      cardType,
      addressId,
    };
  };

  // handler function for navigation state change
  // this function invokes on the start and end the WebView loading
  // Refer https://blog.logrocket.com/the-complete-guide-to-react-native-webview/

  async handleNavigationStateChange(e) {
    const {
      tokens,
      updateAppStatus,
      updateAppTokens,
      updateAppCreditCardInfo,
      checkoutWithAfterPay,
      setLoader,
      setErrorState,
      errMessageTxt,
    } = this.props;

    const { afterpayApiBaseUrl: BASE_URL } = getAPIConfig();

    const { CONFIRM } = ENDPOINTS;
    const { SUCCESS, CANCELLED } = STATUS;

    // the handler parameter 'e', refers to the new navigation state
    // thus `e` holds the target url (where it is about to redirect) for the WebView
    // Refered https://snack.expo.io/@git/github.com/xmonkee/rn-sdk-example
    const { url } = e;

    // variable to hold an array of query string parameters
    let queryStringArr = [];

    // variable to hold the query string parameters in a key-value pair

    const queryString = {};

    // if the response contains the url parameter
    // and the url substring contains the domain specified in the redirectURL
    // only then fetch the query string parameters from the url
    // TODO: find a better (more robust) alternative for this
    if (url && url.indexOf(REDIRECT_URL) !== -1) {
      // stop loading the page (this needs to be done for Android as the redirectUrl loads briefly)
      this.checkoutWebViewRef.stopLoading();

      queryStringArr = url.split('?')[1].split('&');
      queryStringArr.forEach((param) => {
        if (param.length > 0) {
          const [key, value] = param.split('=');
          queryString[key] = value;
        }
      });

      const { orderToken, ppaConfirmToken, status: responseStatus } = queryString;
      const { singleUseCardToken } = tokens;
      if (responseStatus) {
        // if the response is successful
        if (responseStatus === SUCCESS) {
          setLoader(true);
          // update the life-cycle status state
          updateAppStatus('');
          // fetch the virtual card details from the confirm checkout endpoint
          const confirmationResource = await fetch(`${BASE_URL}${CONFIRM}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
            },
            body: JSON.stringify({
              token: orderToken,
              singleUseCardToken,
              ppaConfirmToken: decodeURIComponent(ppaConfirmToken),
            }),
          });

          const { paymentDetails } = await confirmationResource.json();
          if (paymentDetails) {
            checkoutWithAfterPay(this.getccInfo(paymentDetails));
          }
        }
        // if transaction is cancelled clear the credit card info state
        else if (responseStatus === CANCELLED) {
          updateAppCreditCardInfo({});
          setLoader(false);
          setErrorState({
            errorMessage: errMessageTxt,
            component: 'PAGE',
          });
        }
        // update the life-cycle status state
        updateAppStatus(responseStatus);
        // reset the tokens object state to be empty
        updateAppTokens({});
      }
    }
  }

  render() {
    const { tokens } = this.props;
    const dimensions = Dimensions.get('window');
    const webViewStyle = { marginTop: isIOS() ? 40 : 0 };
    const containerStyle = {
      flex: 1,
      height: dimensions.height,
      width: dimensions.width,
    };
    const KeyboardAvoidingViewStyle = { flex: 1 };
    return (
      <ModalNative isOpen showHeader="false" customTransparent>
        <View style={containerStyle}>
          <KeyboardAvoidingView
            behavior={isIOS() ? 'padding' : null}
            style={KeyboardAvoidingViewStyle}
          >
            <WebView
              androidHardwareAccelerationDisabled={true}
              ref={(ref) => {
                this.checkoutWebViewRef = ref;
              }}
              source={{ uri: tokens.redirectCheckoutUrl }}
              onNavigationStateChange={this.handleNavigationStateChange}
              style={webViewStyle}
              startInLoadingState
              useWebKit={isIOS()}
              domStorageEnabled
              javaScriptEnabled
              originWhitelist={['*']}
            />
          </KeyboardAvoidingView>
        </View>
      </ModalNative>
    );
  }
}

AfterPayCheckoutWebView.propTypes = {
  addressId: PropTypes.string.isRequired,
  updateAppStatus: PropTypes.string.isRequired,
  updateAppTokens: PropTypes.string.isRequired,
  tokens: PropTypes.string.isRequired,
  updateAppCreditCardInfo: PropTypes.func.isRequired,
  checkoutWithAfterPay: PropTypes.func,
  shippingAddress: PropTypes.shape({}).isRequired,
  shippingPhoneAndEmail: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape({}).isRequired,
  ledgerSummaryData: PropTypes.shape({}).isRequired,
  reviewFormValues: PropTypes.string.isRequired,
  pickupStoresInCart: PropTypes.shape({}),
  pickupInformation: PropTypes.shape({}),
  setErrorState: PropTypes.func,
  errorLabels: PropTypes.shape({}),
  setLoader: PropTypes.func,
  errMessageTxt: PropTypes.string.isRequired,
};

AfterPayCheckoutWebView.defaultProps = {
  checkoutWithAfterPay: () => {},
  pickupStoresInCart: {},
  pickupInformation: {},
  setErrorState: () => {},
  errorLabels: {},
  setLoader: () => {},
};

export default AfterPayCheckoutWebView;
