// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import {
  styles,
  WrapperStyle,
  PCContainer,
  TitleContainer,
  CardContainer,
  ShowMoreContainer,
  IconContainer,
  IconTextContainer,
} from '../styles/CouponListSection.style.native';
import BodyCopy from '../../../atoms/BodyCopy';
import Anchor from '../../../atoms/Anchor';
import CouponCard from '../../../molecules/CouponCard';
import CouponSkeleton from '../skeleton/CouponSkeleton.view.native';

export class CouponListSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMore: false,
    };
  }

  toggleShow = event => {
    event.preventDefault();
    this.setState(prevState => ({
      showMore: !prevState.showMore,
    }));
  };

  helpAnchorClick = event => {
    event.preventDefault();
    const { helpAnchorClick } = this.props;
    helpAnchorClick();
  };

  render() {
    const {
      labels,
      isFetching,
      couponList,
      heading,
      helpSubHeading,
      couponDetailClick,
      onApply,
      onRemove,
      dataLocator,
      handleErrorCoupon,
      showFullList,
      toastMessage,
      couponsOnHold,
    } = this.props;
    const { showMore } = this.state;
    const buttonText =
      showMore === true ? labels.LESS_MORE_BUTTON_TEXT : labels.SHOW_MORE_BUTTON_TEXT;
    const couponListFilter =
      showMore === true || showFullList ? couponList : couponList.slice(0, 5);
    return (
      <WrapperStyle>
        <TitleContainer>
          {couponList.size > 0 && (
            <BodyCopy
              fontSize="fs16"
              fontWeight="semibold"
              fontFamily="secondary"
              data-locator={dataLocator}
              text={`${heading} (${couponList.size})`}
            />
          )}
          {helpSubHeading && couponListFilter.size > 0 && (
            <PCContainer>
              <IconContainer>
                <IconTextContainer>?</IconTextContainer>
              </IconContainer>
              <Anchor
                fontFamily="secondary"
                fontSizeVariation="medium"
                underline
                anchorVariation="primary"
                onPress={this.helpAnchorClick}
                dataLocator=""
                text={labels.HELP_APPLYING}
              />
            </PCContainer>
          )}
        </TitleContainer>

        {!isFetching ? (
          <>
            <CardContainer>
              {couponListFilter.map(coupon => {
                return (
                  <CouponCard
                    key={coupon.id}
                    labels={labels}
                    isFetching={isFetching}
                    coupon={coupon}
                    couponDetailClick={couponDetailClick}
                    onApply={onApply}
                    onRemove={onRemove}
                    handleErrorCoupon={handleErrorCoupon}
                    toastMessage={toastMessage}
                    couponsOnHold={couponsOnHold}
                  />
                );
              })}
            </CardContainer>
            <ShowMoreContainer>
              {couponList.size > 5 && helpSubHeading !== undefined && (
                <Anchor
                  fontSizeVariation="small"
                  fontFamily="secondary"
                  underline
                  anchorVariation="primary"
                  onPress={this.toggleShow}
                  noLink
                  to="/#"
                  dataLocator=""
                  text={buttonText}
                />
              )}
            </ShowMoreContainer>
          </>
        ) : (
          <CouponSkeleton />
        )}
      </WrapperStyle>
    );
  }
}

CouponListSection.defaultProps = {
  couponsOnHold: false,
};

CouponListSection.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  isFetching: PropTypes.bool.isRequired,
  heading: PropTypes.string.isRequired,
  couponList: PropTypes.shape([]).isRequired,
  couponDetailClick: PropTypes.func.isRequired,
  onApply: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  handleErrorCoupon: PropTypes.func.isRequired,
  helpAnchorClick: PropTypes.func.isRequired,
  helpSubHeading: PropTypes.string.isRequired,
  dataLocator: PropTypes.string.isRequired,
  showFullList: PropTypes.bool.isRequired,
  toastMessage: PropTypes.func.isRequired,
  couponsOnHold: PropTypes.bool,
};

export default withStyles(CouponListSection, styles);
export { CouponListSection as CouponListSectionVanilla };
