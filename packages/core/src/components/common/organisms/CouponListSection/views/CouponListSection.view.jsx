// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { scrollToElement } from '@tcp/core/src/utils';
import withStyles from '../../../hoc/withStyles';
import CouponCard from '../../../molecules/CouponCard';
import Row from '../../../atoms/Row';
import Col from '../../../atoms/Col';
import BodyCopy from '../../../atoms/BodyCopy';
import Anchor from '../../../atoms/Anchor';

import styles from '../styles/CouponListSection.style';

class CouponListSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMore: false,
    };
  }

  toggleShow = (event) => {
    event.preventDefault();
    const { showMore } = this.state;
    this.setState(
      (prevState) => ({
        showMore: !prevState.showMore,
      }),
      () => {
        const couponDetailDOM = document.getElementsByClassName('cartDetailsLink')[4];
        if (couponDetailDOM) {
          couponDetailDOM.focus();
        }
        if (showMore) {
          const headingElem = document.querySelector('.couponList__title');
          const scrollTimer = setTimeout(() => {
            scrollToElement(headingElem);
            clearTimeout(scrollTimer);
          }, 100);
        }
      }
    );
  };

  helpAnchorClick = (event) => {
    event.preventDefault();
    const { helpAnchorClick } = this.props;
    helpAnchorClick();
  };

  render() {
    const {
      labels,
      isFetching,
      couponList,
      className,
      heading,
      helpSubHeading,
      couponDetailClick,
      onApply,
      onRemove,
      dataLocator,
      handleErrorCoupon,
      showFullList,
      couponsOnHold,
      newBag,
    } = this.props;
    const { showMore } = this.state;
    const buttonText =
      showMore === true ? labels.LESS_MORE_BUTTON_TEXT : labels.SHOW_MORE_BUTTON_TEXT;
    const couponListFilter =
      showMore === true || showFullList ? couponList : couponList.slice(0, 5);
    return (
      <div className={className}>
        <div className="couponList__title">
          {couponList.size > 0 && (
            <BodyCopy
              className="couponList__heading"
              fontWeight="semibold"
              component="h2"
              fontSize="fs16"
              data-locator={dataLocator}
              fontFamily="secondary"
            >
              {`${heading} (${couponList.size})`}
            </BodyCopy>
          )}
          {helpSubHeading && couponList.size > 0 && (
            <div className="couponList__iconContainer">
              <div className="couponList__helpIcon">?</div>
              <Anchor
                fontSizeVariation="medium"
                underline
                anchorVariation="primary"
                fontFamily="secondary"
                dataLocator="couponcard-helpApplyingPlaceCashlink"
                onClick={this.helpAnchorClick}
              >
                {labels.HELP_APPLYING}
              </Anchor>
            </div>
          )}
        </div>
        <Row fullBleed>
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: 12,
            }}
          >
            {couponListFilter.map((coupon) => {
              return (
                <CouponCard
                  key={coupon.id}
                  labels={labels}
                  isFetching={isFetching}
                  coupon={coupon}
                  couponDetailClick={couponDetailClick}
                  onApply={onApply}
                  onRemove={onRemove}
                  handleErrorCoupon={handleErrorCoupon}
                  ref={this.couponRef}
                  couponsOnHold={couponsOnHold}
                  newBag={newBag}
                />
              );
            })}
          </Col>
        </Row>
        <div className="cart-show-more-btn">
          {couponList.size > 5 && (
            <Anchor
              fontSizeVariation="small"
              underline
              anchorVariation="primary"
              dataLocator={showMore === true ? 'cartShowMoreButton' : 'cartShowLessButton'}
              onClick={this.toggleShow}
              fontFamily="secondary"
            >
              <BodyCopy component="span" fontSize="fs12">
                {buttonText}
              </BodyCopy>
            </Anchor>
          )}
        </div>
      </div>
    );
  }
}

CouponListSection.defaultProps = {
  couponsOnHold: false,
};

CouponListSection.propTypes = {
  labels: PropTypes.shape({}).isRequired,

  helpAnchorClick: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  couponList: PropTypes.shape([]).isRequired,
  className: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  helpSubHeading: PropTypes.string.isRequired,
  couponDetailClick: PropTypes.func.isRequired,
  onApply: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  dataLocator: PropTypes.string.isRequired,
  handleErrorCoupon: PropTypes.func.isRequired,
  showFullList: PropTypes.bool.isRequired,
  couponsOnHold: PropTypes.bool,
};

export default withStyles(CouponListSection, styles);
export { CouponListSection as CouponListSectionVanilla };
