// 9fbef606107a605d69c0edbcd8029e5d 
import EMAIL_SIGNUP_CONSTANTS from './EmailSignupForm.constants';

const EmailSignupReducer = (state = {}, action) => {
  switch (action.type) {
    case EMAIL_SIGNUP_CONSTANTS.EMAIL_SUBSCRIPTION_STATUS:
      return { ...state, ...action.payload };
    case EMAIL_SIGNUP_CONSTANTS.EMAIL_VALIDATION_STATUS:
      return { ...state, ...action.payload };
    case EMAIL_SIGNUP_CONSTANTS.CLEAR_SUBSCRIPTION_FORM:
      return { emailDisclaimer: state.emailDisclaimer };
    default:
      return state;
  }
};

export default EmailSignupReducer;
