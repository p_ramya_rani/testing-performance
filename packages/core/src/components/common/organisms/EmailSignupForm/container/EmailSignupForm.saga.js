// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import { subscribeEmailAddress } from '@tcp/core/src/components/features/CnC/Checkout/container/CheckoutExtended.saga.util';
import EMAIL_SIGNUP_CONSTANTS from './EmailSignupForm.constants';
import { setEmailValidationStatus } from './EmailSignupForm.actions';

export function* subscribeEmail(emailObj, status) {
  return yield call(subscribeEmailAddress, emailObj, status);
}

export function* verifyEmail({ payload }) {
  try {
    const emailValidationState = yield call(emailSignupAbstractor.verifyEmail, payload);
    yield put(setEmailValidationStatus({ validEmail: emailValidationState }));
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'EmailSignupFormSaga - verifyEmail',
        payloadRecieved: payload,
      },
    });
  }
}

function* EmailSignupSaga() {
  yield takeLatest(EMAIL_SIGNUP_CONSTANTS.EMAIL_SUBSCRIPTION_SUBMIT, subscribeEmail);
  yield takeLatest(EMAIL_SIGNUP_CONSTANTS.VALIDATE_EMAIL, verifyEmail);
}

export default EmailSignupSaga;
