// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent, Fragment } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { getLabelValue } from '@tcp/core/src/utils';
import PropTypes from 'prop-types';
import {
  Button,
  Col,
  Row,
  TextBox,
  DamImage,
  Anchor,
  RichText,
} from '@tcp/core/src/components/common/atoms';
import { Grid } from '@tcp/core/src/components/common/molecules';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { isGymboree, getIconPath } from '@tcp/core/src/utils/utils';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import Image from '@tcp/core/src/components/common/atoms/Image';
import SignupConfirm from '../../../molecules/SignupConfirm';
import SignupFormIntro from '../../../molecules/SignupFormIntro';

import styles from '../styles/EmailSignupForm.style';
import config from '../config';

class EmailSignupForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFieldEmpty: true,
    };
    this.fieldValue = '';
  }

  componentDidMount() {
    const { subscription, trackSubscriptionSuccess, defaultStore } = this.props;
    if (subscription.success) {
      trackSubscriptionSuccess(defaultStore);
    }
  }

  componentDidUpdate({ subscription: oldSubscription }) {
    const { subscription, trackSubscriptionSuccess, defaultStore } = this.props;
    if (subscription.success !== oldSubscription.success && subscription.success) {
      if (this.formSubmitPromise) {
        this.formSubmitPromise.resolve();
      }
      trackSubscriptionSuccess(defaultStore);
    } else if (subscription.error && this.formSubmitPromise) {
      this.formSubmitPromise.reject();
    }
  }

  submitForm = (formObject) => {
    const {
      submitEmailSubscription,
      validateSignupEmail,
      formViewConfig: { validationErrorLabel },
    } = this.props;
    const { emailSignupSecondBrand, signup } = formObject;

    // Checking the entered email is not same as previous try
    if (this.fieldValue === signup) {
      return false;
    }

    return validateSignupEmail(signup)
      .then((subscription) => {
        // Assigned value to fieldValue for next validation.
        this.fieldValue = signup;
        if (subscription.error) {
          return Promise.reject();
        }

        /* Faking this because redux-form `submitting` based on promise resolve
           and we will resolve formSubmitPromise only when the state has success flag on
           componentDidUpdate
         */
        return new Promise((resolve, reject) => {
          this.formSubmitPromise = { resolve, reject };
          submitEmailSubscription(
            {
              isEmailOptInSecondBrand: emailSignupSecondBrand,
              signup,
            },
            subscription.status
          );
        });
      })
      .catch(() => {
        const error = { signup: validationErrorLabel };
        throw new SubmissionError({ ...error, _error: error });
      });
  };

  fieldChange = (element) => {
    const isFieldEmpty = !element.currentTarget.value.trim();
    this.setState({ isFieldEmpty });
  };

  getImageData = () => {
    const { imageData, formViewConfig } = this.props;

    return imageData && Object.keys(imageData).length > 0
      ? imageData
      : {
          url: formViewConfig.lbl_SignUp_imageSrc,
          alt: formViewConfig.lbl_SignUp_imageAlt,
        };
  };

  // eslint-disable-next-line
  render() {
    const {
      formViewConfig,
      subscription,
      pristine,
      handleSubmit,
      submitting,
      className,
      noModal,
      closeModal,
      emailDisclaimer,
    } = this.props;
    const { left, right } = noModal ? config.pageColProps : config.modalColProps;
    const { IMG_DATA_MODAL, IMG_DATA_PAGE } = config;
    const isGym = isGymboree();
    const { isFieldEmpty } = this.state;

    return (
      <Fragment>
        {subscription.success ? (
          <Grid>
            <Row fullBleed className={`${className} ${noModal ? 'emailsubscribe__wrapper' : ''}`}>
              {!noModal && (
                <Col
                  isNotInlineBlock
                  colSize={left}
                  hideCol={{ small: true, medium: true }}
                  className="img-wrapper"
                >
                  <DamImage
                    imgConfigs={noModal ? IMG_DATA_PAGE : IMG_DATA_MODAL}
                    imgData={this.getImageData()}
                  />
                </Col>
              )}
              <Col
                colSize={right}
                ignoreGutter={{ large: true }}
                className="emailform__wrapper form-wrapper"
                aria-live="polite"
                aria-atomic="true"
              >
                <SignupConfirm
                  formViewConfig={formViewConfig}
                  tncHtml={emailDisclaimer}
                  susbscriptionType="email"
                />
                <Row className="button-wrapper" fullBleed>
                  <Col colSize={{ small: 4, medium: 4, large: 4 }} className="button-container">
                    <Anchor
                      to={getLabelValue(formViewConfig, 'lbl_SignUp_shopNowBtnUrl')}
                      asPath={getLabelValue(formViewConfig, 'lbl_SignUp_shopNowBtnUrl')}
                      target={getLabelValue(formViewConfig, 'lbl_SignUp_shopNowBtnUrlTarget')}
                      onClick={closeModal}
                    >
                      <Button
                        fullWidth
                        buttonVariation="fixed-width"
                        fill="BLUE"
                        type="submit"
                        className="shop-button"
                        dataLocator="shop_now_btn"
                      >
                        {getLabelValue(formViewConfig, 'lbl_SignUp_shopNowLabel')}
                      </Button>
                    </Anchor>
                  </Col>
                  {noModal && (
                    <Col
                      colSize={{ small: 4, medium: 4, large: 4 }}
                      className="button-container"
                      offsetLeft={{ large: 2, small: 0, medium: 0 }}
                    >
                      <Anchor
                        to={getLabelValue(formViewConfig, 'lbl_SignUp_storeLocatorUrl')}
                        asPath={getLabelValue(formViewConfig, 'lbl_SignUp_storeLocatorUrl')}
                      >
                        <Button
                          fullWidth
                          buttonVariation="fixed-width"
                          fill="BLUE"
                          type="submit"
                          className="shop-button"
                          dataLocator="store-locator_btn"
                        >
                          {getLabelValue(formViewConfig, 'lbl_SignUp_storeLocatorLabel')}
                        </Button>
                      </Anchor>
                    </Col>
                  )}
                </Row>
              </Col>
            </Row>
          </Grid>
        ) : (
          <form onSubmit={handleSubmit(this.submitForm)}>
            <Grid>
              <Row
                fullBleed={{ large: true }}
                className={`${className} wrapper form_container ${
                  noModal ? 'emailsubscribe__wrapper' : ''
                }`}
              >
                {!noModal && (
                  <Col
                    isNotInlineBlock
                    colSize={left}
                    hideCol={{ small: true, medium: true }}
                    className="img-wrapper"
                  >
                    <DamImage
                      imgConfigs={noModal ? IMG_DATA_PAGE : IMG_DATA_MODAL}
                      imgData={this.getImageData()}
                    />
                  </Col>
                )}
                <Col colSize={right} className="emailform__wrapper form-wrapper">
                  <div className="emailform__content">
                    <Row fullBleed>
                      {!noModal && (
                        <Col
                          colSize={{ small: 6, medium: 8, large: 10 }}
                          offsetLeft={{ small: 0, medium: 0, large: 1 }}
                          className="logo-wrapper"
                        >
                          <Image
                            className="logo"
                            aria-hidden="true"
                            src={
                              isGymboree()
                                ? getIconPath('gymboree-icon')
                                : getIconPath('header__brand-tab--tcp')
                            }
                          />
                        </Col>
                      )}
                    </Row>
                    <SignupFormIntro formViewConfig={formViewConfig} noModal={noModal} />
                    <Col
                      colSize={{ small: 6, medium: 6, large: noModal ? 8 : 12 }}
                      offsetLeft={{ small: 0, medium: 1, large: 0 }}
                      className="field-container"
                    >
                      <Field
                        placeholder={getLabelValue(formViewConfig, 'lbl_SignUp_placeholderText')}
                        name="signup"
                        id="modal_signup"
                        type="text"
                        component={TextBox}
                        maxLength={50}
                        dataLocator="email_address_field"
                        enableSuccessCheck={false}
                        onChange={this.fieldChange}
                      />

                      <Field
                        name="emailSignupSecondBrand"
                        id="emailSignupSecondBrand"
                        component={InputCheckbox}
                        dataLocator={isGym ? 'email_tcp_opt' : 'email_gym_opt'}
                        type="checkbox"
                        className="email-signup-second-brand"
                      >
                        {isGym
                          ? getLabelValue(formViewConfig, 'lbl_SignUp_tcpSignUpLabel')
                          : getLabelValue(formViewConfig, 'lbl_SignUp_gymSignUpLabel')}
                      </Field>

                      {!noModal && (
                        <div className="termscontainer">
                          {emailDisclaimer && emailDisclaimer.richText ? (
                            <RichText richTextHtml={emailDisclaimer.richText} />
                          ) : null}
                        </div>
                      )}
                    </Col>
                    <Row className="button-wrapper-form" fullBleed>
                      <Col
                        colSize={{ small: 4, medium: 4, large: 6 }}
                        className="button-wrapper-form__joinnowbtn"
                      >
                        <Button
                          fullWidth
                          disabled={isFieldEmpty || pristine || submitting}
                          buttonVariation="fixed-width"
                          fill="BLUE"
                          type="submit"
                          className="join-button"
                          dataLocator="join_now_btn"
                        >
                          {noModal
                            ? getLabelValue(formViewConfig, 'lbl_SignUp_signupBtn')
                            : getLabelValue(formViewConfig, 'lbl_SignUp_joinButtonLabel')}
                        </Button>
                      </Col>
                      <Row fullBleed className="no-thanks-row">
                        <Col
                          className="no-thanks-wrapper"
                          colSize={{ small: 4, medium: 4, large: 6 }}
                        >
                          <Anchor
                            handleLinkClick={(e) => {
                              e.preventDefault();
                              closeModal();
                            }}
                            className="no-thanks-email"
                            noLink
                          >
                            {getLabelValue(formViewConfig, 'lbl_SignUp_noThanks').toUpperCase()}
                          </Anchor>
                        </Col>
                      </Row>
                    </Row>
                    {noModal && (
                      <Row>
                        <Col
                          colSize={{ large: 10, medium: 8, small: 6 }}
                          className="termscontainer"
                        >
                          <div>
                            {emailDisclaimer && emailDisclaimer.richText ? (
                              <RichText richTextHtml={emailDisclaimer.richText} />
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                    )}
                  </div>
                </Col>
              </Row>
            </Grid>
          </form>
        )}
      </Fragment>
    );
  }
}

EmailSignupForm.propTypes = {
  buttonConfig: PropTypes.shape({}),
  submitEmailSubscription: PropTypes.func,
  className: PropTypes.string,
  formViewConfig: PropTypes.shape({}).isRequired,
  confirmationViewConfig: PropTypes.shape({}).isRequired,
  clearEmailSignupForm: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.func,
  validateSignupEmail: PropTypes.func,
  trackSubscriptionSuccess: PropTypes.func,
  subscription: PropTypes.shape({}),
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  colProps: PropTypes.shape({}),
  imageData: PropTypes.shape({}),
  noModal: PropTypes.bool,
  closeModal: PropTypes.func,
  defaultStore: PropTypes.shape({}),
  emailDisclaimer: PropTypes.shape({}),
};

EmailSignupForm.defaultProps = {
  buttonConfig: {},
  submitEmailSubscription: () => {},
  handleSubmit: () => {},
  trackSubscriptionSuccess: () => {},
  validateSignupEmail: () => Promise.resolve({}),
  className: '',
  subscription: {},
  pristine: false,
  submitting: false,
  colProps: {},
  imageData: {},
  noModal: false,
  closeModal: () => {},
  defaultStore: {},
  emailDisclaimer: {},
};
export default withStyles(
  reduxForm({
    form: 'EmailSignupForm',
    initialValues: {
      signup: '',
    },
  })(EmailSignupForm),
  styles
);

export { EmailSignupForm as EmailSignupFormVanilla };
