// 9fbef606107a605d69c0edbcd8029e5d 
const emailDisclaimerUS = {
  data: {
    moduleX: {
      contentId: '49975b99-e98e-4437-8307-fb8b06dc0365',
      name: 'moduleX',
      type: 'module',
      errorMessage: null,
      set: [
        {
          key: 'moduleClassName',
          val: '',
          __typename: 'KeyValPair',
        },
        {
          key: 'nativeRender',
          val: 'false',
          __typename: 'KeyValPair',
        },
      ],
      composites: {
        richTextList: [
          {
            text:
              '<style>\r\n    .d-copy {\r\n        font-family: "Nunito";\r\n        font-size: 10px;\r\n    }\r\n    .d-copy a {\r\n        font-family: "Nunito";\r\n        color: #000;\r\n    }\r\n</style>\r\n\r\n<p class="d-copy">\r\n*By signing up, you agree to provide certain personal information for use by The Children’s Place or Gymboree. You can withdraw your consent at any time. See <a href="/us/help-center/policies#privacypolicy">Privacy Policy</a>. <a href="/us/help-center/contact-us">Contact Us</a>: The Children\'s Place, 500 Plaza Drive, Secaucus, NJ 07094, <a href="/">www.childrensplace.com</a>.\r\n</p>',
            __typename: 'Text',
          },
        ],
        __typename: 'Composite',
      },
      __typename: 'ModuleContent',
    },
  },
};

const emailDisclaimerCA = {
  data: {
    moduleX: {
      contentId: '49975b99-e98e-4437-8307-fb8b06dc0365',
      name: 'moduleX',
      type: 'module',
      errorMessage: null,
      set: [
        {
          key: 'moduleClassName',
          val: '',
          __typename: 'KeyValPair',
        },
        {
          key: 'nativeRender',
          val: 'false',
          __typename: 'KeyValPair',
        },
      ],
      composites: {
        richTextList: [
          {
            text:
              '<style>\r\n    .d-copy {\r\n        font-family: "Nunito";\r\n        font-size: 10px;\r\n    }\r\n    .d-copy a {\r\n        font-family: "Nunito";\r\n        color: #000;\r\n    }\r\n</style>\r\n\r\n<p class="d-copy">\r\n*By signing up, you agree to provide certain personal information for use by The Children’s Place or Gymboree. You can withdraw your consent at any time. See <a href="/us/help-center/policies#privacypolicy">Privacy Policy</a>. <a href="/us/help-center/contact-us">Contact Us</a>: The Children\'s Place, 500 Plaza Drive, Secaucus, NJ 07094, <a href="/">www.childrensplace.com</a>.\r\n</p>',
            __typename: 'Text',
          },
        ],
        __typename: 'Composite',
      },
      __typename: 'ModuleContent',
    },
  },
};

export default {
  emailDisclaimerUS,
  emailDisclaimerCA,
};
