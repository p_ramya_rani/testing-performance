// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, change } from 'redux-form';
import { getAddressFromPlace, trimMaxCharacters } from '@tcp/core/src/utils';
import { AutoCompleteComponentGmaps } from '@tcp/core/src/components/common/atoms/AutoSuggest/AutoCompleteComponentGmaps';
import TextBox from '../../atoms/TextBox';
import SelectBox from '../../atoms/Select';
import InputCheckbox from '../../atoms/InputCheckbox';
import Row from '../../atoms/Row';
import Col from '../../atoms/Col';
import Button from '../../atoms/Button';
import createValidateMethod from '../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../utils/formValidation/validatorStandardConfig';
import { AutoCompleteComponent } from '../../atoms/AutoSuggest/AutoCompleteComponent';
import {
  countriesOptionsMap,
  CAcountriesStatesTable,
  UScountriesStatesTable,
} from './CountriesAndStates.constants';
import { formatPhoneNumber } from '../../../../utils/formValidation/phoneNumber';
import constants from '../AddEditAddress/container/AddEditAddress.constants';

export class AddressForm extends React.PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    dirty: PropTypes.bool.isRequired,
    className: PropTypes.string,
    backToAddressBookClick: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
    addressFormLabels: PropTypes.shape({}).isRequired,
    isEdit: PropTypes.bool,
    isMakeDefaultDisabled: PropTypes.bool.isRequired,
    initialValues: PropTypes.shape({
      country: PropTypes.string,
    }),
    mapboxAutocompleteTypesParam: PropTypes.string,
    mapboxSwitch: PropTypes.bool,
  };

  static defaultProps = {
    isEdit: false,
    initialValues: {},
    className: '',
    mapboxAutocompleteTypesParam: '',
    mapboxSwitch: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      country: props.initialValues.country || 'US',
    };
  }

  StateCountryChange = (e) => {
    this.setState({
      country: e.target.value ? e.target.value : '',
    });
  };

  handlePlaceSelected = (address) => {
    const { dispatch } = this.props;
    dispatch(change('AddressForm', 'city', address.city));
    dispatch(change('AddressForm', 'zipCode', address.zip));
    dispatch(change('AddressForm', 'state', address.state));
    dispatch(change('AddressForm', 'addressLine1', address.addressline));
  };

  handlePlaceSelectedGmaps = (place, inputValue) => {
    const { dispatch } = this.props;
    const address = getAddressFromPlace(place, inputValue);
    dispatch(change('AddressForm', 'city', address.city));
    dispatch(change('AddressForm', 'zipCode', address.zip));
    dispatch(change('AddressForm', 'state', address.state));
    dispatch(change('AddressForm', 'addressLine1', address.street));
  };

  disableStateVariation = (invalid, dirty, isEdit) => {
    return invalid || (isEdit && !dirty && !invalid);
  };

  render() {
    const {
      handleSubmit,
      invalid,
      className,
      backToAddressBookClick,
      addressFormLabels,
      isEdit,
      isMakeDefaultDisabled,
      dirty,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
    } = this.props;
    const { country } = this.state;
    const getBtnState = this.disableStateVariation(invalid, dirty, isEdit);
    return (
      <form className={className} onSubmit={handleSubmit} noValidate>
        <Row fullBleed>
          <Col ignoreGutter={{ small: true }} colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              placeholder={addressFormLabels.firstName}
              name="firstName"
              id="firstName"
              type="text"
              component={TextBox}
              dataLocator="addnewaddress-firstname"
            />
          </Col>
          <Col colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              placeholder={addressFormLabels.lastName}
              name="lastName"
              id="lastName"
              component={TextBox}
              dataLocator="addnewaddress-lastname"
            />
          </Col>
        </Row>
        <Row fullBleed>
          <Col ignoreGutter={{ small: true }} colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              id="addressLine1"
              placeholder={addressFormLabels.addressLine1}
              component={mapboxSwitch ? AutoCompleteComponent : AutoCompleteComponentGmaps}
              name="addressLine1"
              maxLength={30}
              normalize={trimMaxCharacters(30)}
              onPlaceSelected={
                mapboxSwitch ? this.handlePlaceSelected : this.handlePlaceSelectedGmaps
              }
              componentRestrictions={Object.assign({}, { country: [country] })}
              dataLocator="addnewaddress-addressl1"
              apiFields="address_components"
              types={['address']}
              mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
            />
          </Col>
          <Col colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              placeholder={addressFormLabels.addressLine2}
              name="addressLine2"
              id="addressLine2"
              maxLength={30}
              normalize={trimMaxCharacters(30)}
              component={TextBox}
              dataLocator="addnewaddress-addressl2"
            />
          </Col>
        </Row>
        <Row fullBleed>
          <Col ignoreGutter={{ small: true }} colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              id="city"
              placeholder={addressFormLabels.city}
              name="city"
              component={TextBox}
              dataLocator="addnewaddress-city"
            />
          </Col>
          <Col ignoreGutter={{ small: true }} colSize={{ small: 3, medium: 2, large: 3 }}>
            <Field
              id="state"
              placeholder={addressFormLabels.select}
              title={country === 'CA' ? addressFormLabels.province : addressFormLabels.stateLbl}
              name="state"
              component={SelectBox}
              options={country === 'CA' ? CAcountriesStatesTable : UScountriesStatesTable}
              dataLocator="addnewaddress-state"
            />
          </Col>
          <Col
            ignoreGutter={{ small: true }}
            colSize={{ small: 3, medium: 2, large: 3 }}
            className="zipField"
          >
            <Field
              placeholder={
                country === 'CA' ? addressFormLabels.postalCode : addressFormLabels.zipCode
              }
              id="zipCode"
              name="zipCode"
              maxLength={country === 'CA' ? 7 : 5}
              component={TextBox}
              dataLocator="addnewaddress-zipcode"
            />
          </Col>
        </Row>
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 4, large: 6 }} ignoreGutter={{ small: true }}>
            <Field
              id="country"
              placeholder={addressFormLabels.country}
              title={addressFormLabels.country}
              name="country"
              component={SelectBox}
              options={countriesOptionsMap}
              onChange={this.StateCountryChange}
              dataLocator="addnewaddress-country"
            />
          </Col>
          <Col colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              placeholder={addressFormLabels.phoneNumber}
              name="phoneNumber"
              id="phoneNumber"
              component={TextBox}
              dataLocator="addnewaddress-phnumber"
              type="tel"
              normalize={formatPhoneNumber}
            />
          </Col>
        </Row>
        <Row fullBleed className="elem-mb-XXL elem-mt-LRG">
          <Col
            colSize={{ small: 4, medium: 4, large: 6 }}
            offsetLeft={{ small: 1 }}
            className="dropdown-text"
          >
            <Field
              name="primary"
              component={InputCheckbox}
              dataLocator="addnewaddress-setdefaddress"
              disabled={isMakeDefaultDisabled}
              className="AddAddressForm__makeDefault"
            >
              {addressFormLabels.setDefaultMsg}
            </Field>
          </Col>
        </Row>
        <Row fullBleed className="AddAddressForm__ctaContainer">
          <Col
            className="AddAddressForm__cancel"
            colSize={{ small: 4, medium: 3, large: 3 }}
            offsetLeft={{ small: 1, medium: 1, large: 6 }}
          >
            <Button
              onClick={backToAddressBookClick}
              buttonVariation="fixed-width"
              type="button"
              data-locator="addnewaddress-cancel"
            >
              {addressFormLabels.cancel}
            </Button>
          </Col>
          <Col
            className="AddAddressForm__submit"
            colSize={{ small: 4, medium: 3, large: 3 }}
            offsetLeft={{ small: 1 }}
          >
            <Button
              fill="BLUE"
              disabled={getBtnState}
              type="submit"
              buttonVariation="fixed-width"
              data-locator="addnewaddress-addaddress"
            >
              {isEdit ? addressFormLabels.update : addressFormLabels.addAddress}
            </Button>
          </Col>
        </Row>
      </form>
    );
  }
}

const validateMethod = createValidateMethod(
  getStandardConfig([
    'firstName',
    'lastName',
    'addressLine1',
    'addressLine2',
    'city',
    'state',
    'zipCode',
    'country',
    'phoneNumber',
  ])
);

export default reduxForm({
  form: constants.ADDRESS_FORM, // a unique identifier for this form
  ...validateMethod,
})(AddressForm);
