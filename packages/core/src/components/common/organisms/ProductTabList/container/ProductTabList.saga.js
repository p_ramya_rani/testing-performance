// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeEvery } from 'redux-saga/effects';
import lightweightProductListing from '@tcp/core/src/services/abstractors/lightweightProductListing';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import constants from './ProductTabList.constants';
import { productTabListDataFail, productTabListDataSuccess } from './ProductTabList.actions';
import errorMessage from '../../../../../services/handler/stateful/errorResponseMapping/index.json';
import { toastMessageInfo } from '../../../atoms/Toast/container/Toast.actions';
import { isMobileApp } from '../../../../../utils';

export function* fetchProductTabListData({ payload }) {
  const { categoryId } = payload;
  const xappURLConfig = yield call(getUnbxdXappConfigs);
  try {
    const res = yield call(lightweightProductListing.getData, payload, xappURLConfig);
    if (res) {
      return yield put(
        productTabListDataSuccess({
          [categoryId]: res,
          errors: {
            [categoryId]: null,
          },
          completed: {
            [categoryId]: false,
          },
        })
      );
    }
    throw new Error();
  } catch (err) {
    if (isMobileApp())
      yield put(toastMessageInfo(errorMessage.ERROR_MESSAGES_BOPIS.storeSearchException));
    return yield put(
      productTabListDataFail({
        [categoryId]: [],
        errors: {
          [categoryId]: true,
        },
        completed: {
          [categoryId]: false,
        },
      })
    );
  }
}

function* ProductTabListSaga() {
  yield takeEvery(constants.PRODUCT_TAB_LIST_REQ, fetchProductTabListData);
}

export default ProductTabListSaga;
