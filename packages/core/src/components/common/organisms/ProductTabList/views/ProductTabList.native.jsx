// 9fbef606107a605d69c0edbcd8029e5d 
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';

import ButtonTabs from '../../../molecules/ButtonTabs';
import { Wrapper } from '../ProductTabList.style.native';

const ProductTabList = props => {
  const { selectedTabId, tabs, onTabChange, style, wrappedButtonTabs } = props;

  return (
    <Wrapper style={style}>
      <ButtonTabs
        selectedTabId={selectedTabId}
        onTabChange={onTabChange}
        tabs={tabs}
        wrappedButtonTabs={wrappedButtonTabs}
      />
    </Wrapper>
  );
};

ProductTabList.defaultProps = {
  onTabChange: () => {},
  tabs: [],
  selectedTabId: '',
  style: [],
  wrappedButtonTabs: false,
};

ProductTabList.propTypes = {
  onTabChange: PropTypes.func,
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.string,
    })
  ),
  selectedTabId: PropTypes.string,
  style: PropTypes.arrayOf(PropTypes.object),
  wrappedButtonTabs: PropTypes.bool,
};

export default ProductTabList;
