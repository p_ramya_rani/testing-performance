// 9fbef606107a605d69c0edbcd8029e5d 
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';

import ButtonTabs from '../../../molecules/ButtonTabs';

const ProductTabList = props => {
  const { selectedTabId, tabs, onTabChange, dataLocator, wrappedButtonTabs } = props;

  return (
    <ButtonTabs
      selectedTabId={selectedTabId}
      onTabChange={onTabChange}
      tabs={tabs}
      dataLocator={dataLocator}
      wrappedButtonTabs={wrappedButtonTabs}
    />
  );
};

ProductTabList.defaultProps = {
  onTabChange: () => {},
  tabs: [],
  selectedTabId: '',
  dataLocator: '',
  wrappedButtonTabs: false,
};

ProductTabList.propTypes = {
  onTabChange: PropTypes.func,
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.string,
    })
  ),
  selectedTabId: PropTypes.string,
  dataLocator: PropTypes.string,
  wrappedButtonTabs: PropTypes.bool,
};

export default ProductTabList;
