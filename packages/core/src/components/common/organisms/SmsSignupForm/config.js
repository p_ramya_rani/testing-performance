// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  IMG_DATA_MODAL: {
    imgConfig: ['t_smsSignUp_img_m', 't_smsSignUp_img_t', 't_smsSignUp_img_d'],
  },
  IMG_DATA_PAGE: {
    imgConfig: ['t_smsSignUpPage_img_m', 't_smsSignUpPage_img_t', 't_smsSignUpPage_img_d'],
  },
  modalColProps: {
    left: { small: 4, medium: 4, large: 4 },
    right: { small: 6, medium: 8, large: 8 },
  },
  pageColProps: {
    right: { small: 6, medium: 6, large: 6 },
  },
};
