// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { SmsSignupFormVanilla } from '../views/SmsSignupForm';

describe('SMSSignupModal component', () => {
  it('renders correctly', () => {
    const props = {
      buttonConfig: {},
      className: '',
      formViewConfig: {},
      subscription: { success: false },
      isEmailValid: false,
    };
    const component = shallow(<SmsSignupFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly when button is clicked', () => {
    const props = {
      buttonConfig: {},
      className: '',
      formViewConfig: {},
      subscription: { success: true },
      isEmailValid: true,
    };
    const component = shallow(<SmsSignupFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly when button is clicked', () => {
    const props = {
      buttonConfig: {},
      className: '',
      formViewConfig: {},
      subscription: { success: false },
      isEmailValid: true,
    };
    const component = shallow(<SmsSignupFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly when modal is true', () => {
    const props = {
      buttonConfig: {},
      className: '',
      formViewConfig: {},
      subscription: { success: false },
      isEmailValid: true,
      noModal: true,
    };
    const component = shallow(<SmsSignupFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly when modal is false', () => {
    const props = {
      buttonConfig: {},
      className: '',
      formViewConfig: {},
      subscription: { success: true },
      isEmailValid: true,
      noModal: false,
    };
    const component = shallow(<SmsSignupFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call when click onsubmit', () => {
    const props = {
      buttonConfig: {},
      className: '',
      formViewConfig: {},
      subscription: { success: false },
      isEmailValid: true,
    };
    const props1 = {
      signupPhoneNumber: '',
      optPhoneSignupSecondBrand: '',
    };
    const component = shallow(<SmsSignupFormVanilla {...props} />);
    component.instance().submitForm(props1);
  });

  it('calling componentDidMount method', () => {
    const props = {
      buttonConfig: {},
      className: '',
      formViewConfig: {},
      subscription: { success: false },
      isEmailValid: true,
    };
    const tree = shallow(<SmsSignupFormVanilla {...props} />);
    const componentInstance = tree.instance();
    jest.spyOn(componentInstance, 'componentDidMount');
    componentInstance.componentDidMount();
    expect(componentInstance.componentDidMount).toHaveBeenCalled();
  });

  it('calling renderTapToJoin method', () => {
    const props = {
      buttonConfig: {},
      className: '',
      formViewConfig: {},
      subscription: { success: false },
      isEmailValid: true,
      isMobile: true,
    };
    const tree = shallow(<SmsSignupFormVanilla {...props} />);
    const componentInstance = tree.instance();
    jest.spyOn(componentInstance, 'renderTapToJoin');
    componentInstance.renderTapToJoin();
    expect(componentInstance.renderTapToJoin).toHaveBeenCalled();
  });
});
