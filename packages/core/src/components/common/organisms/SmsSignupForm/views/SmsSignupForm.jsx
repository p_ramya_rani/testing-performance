/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';
import { getLabelValue, getViewportInfo } from '@tcp/core/src/utils';
import {
  Button,
  Col,
  Row,
  TextBox,
  DamImage,
  Anchor,
  RichText,
} from '@tcp/core/src/components/common/atoms';
import { formatPhoneNumber } from '@tcp/core/src/utils/formValidation/phoneNumber';
import SMSTapToJoin from '@tcp/core/src/components/common/molecules/SMSTapToJoin';
import { Grid } from '@tcp/core/src/components/common/molecules';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { isGymboree, getIconPath, isCanada } from '@tcp/core/src/utils/utils';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import Image from '@tcp/core/src/components/common/atoms/Image';
import SignupConfirm from '../../../molecules/SignupConfirm';
import SignupFormIntro from '../../../molecules/SignupFormIntro';
import smsSignupStyle from '../styles/SmsSignupForm.style';
import config from '../config';

class SmsSignupForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFieldEmpty: true,
    };
  }

  componentDidMount() {
    const { subscription, trackSubscriptionSuccess, defaultStore } = this.props;
    if (subscription.success) {
      trackSubscriptionSuccess(defaultStore);
    }
  }

  componentDidUpdate({ subscription: oldSubscription }) {
    const { subscription, trackSubscriptionSuccess, defaultStore } = this.props;
    if ((subscription.error || subscription.success) && this.formSubmitPromise) {
      if (subscription.error) {
        this.formSubmitPromise.reject();
      } else {
        this.formSubmitPromise.resolve();
      }
      this.formSubmitPromise = null;
    }

    if (subscription.success !== oldSubscription.success && subscription.success) {
      trackSubscriptionSuccess(defaultStore);
    }
  }

  submitForm = (formObj) => {
    const {
      submitSmsSubscription,
      clearSmsSignupForm,
      validateSignupSmsPhoneNumber,
      formViewConfig: { validationErrorLabel },
    } = this.props;
    const { signupPhoneNumber, optPhoneSignupSecondBrand } = formObj;
    return validateSignupSmsPhoneNumber(signupPhoneNumber)
      .then((subscription) => {
        if (subscription.error) {
          return Promise.reject();
        }
        /*
         Faking this because redux-form `submitting` based on promise resolve
       and we will resolve formSubmitPromise only when the state has success flag on
       componentDidUpdate.
       */
        return new Promise((resolve, reject) => {
          clearSmsSignupForm();
          this.formSubmitPromise = { resolve, reject };
          submitSmsSubscription({
            footerTopSmsSignup: signupPhoneNumber,
            isTextOptInSecondBrand: optPhoneSignupSecondBrand,
          });
        });
      })
      .catch(() => {
        const error = {
          signupPhoneNumber: validationErrorLabel,
        };
        throw new SubmissionError({
          ...error,
          _error: error,
        });
      });
  };

  fieldChange = (element) => {
    const isFieldEmpty = !element.currentTarget.value.trim();
    this.setState({ isFieldEmpty });
  };

  getImageData = () => {
    const { imageData, formViewConfig } = this.props;

    return imageData && Object.keys(imageData).length > 0
      ? imageData
      : {
          url: formViewConfig.lbl_SignUp_imageSrc,
          alt: formViewConfig.lbl_SignUp_imageAlt,
        };
  };

  renderTapToJoin = (isGym) => {
    const { className, formViewConfig, noModal, closeModal, smsDisclaimer, smsFromPage } =
      this.props;

    const { right } = noModal ? config.pageColProps : config.modalColProps;
    return (
      <Grid>
        <Row fullBleed={{ large: true }} className={`${className} wrapper form_container`}>
          <Col colSize={right} className="smsform__wrapper form-wrapper marginRightZero">
            <div className="smsform__content-join">
              <Row fullBleed>
                {!noModal && (
                  <Col
                    colSize={{ small: 6, medium: 6, large: 10 }}
                    offsetLeft={{ small: 0, medium: 1, large: 1 }}
                    className="logo-wrapper"
                  >
                    <Image
                      className="logo"
                      aria-hidden="true"
                      src={
                        isGymboree()
                          ? getIconPath('gymboree-icon')
                          : getIconPath('header__brand-tab--tcp')
                      }
                    />
                  </Col>
                )}
              </Row>
              <div className="button-wrapper-tap-join">
                <SMSTapToJoin isWhite={false} smsFromPage={smsFromPage} isGym={isGym} />
                {smsFromPage === 'overlay' && (
                  <Row>
                    <Col className="no-thanks-wrapper" colSize={{ small: 4, medium: 4, large: 6 }}>
                      <Anchor
                        className="no-thanks-sms"
                        handleLinkClick={(e) => {
                          e.preventDefault();
                          closeModal();
                        }}
                        noLink
                      >
                        {formViewConfig.noThanksLabel}
                      </Anchor>
                    </Col>
                  </Row>
                )}
              </div>
              <Row>
                <Col colSize={{ large: 10, medium: 8, small: 6 }} className="termscontainer">
                  {smsDisclaimer && smsDisclaimer.richText ? (
                    <RichText richTextHtml={smsDisclaimer.richText} />
                  ) : null}
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Grid>
    );
  };

  // eslint-disable-next-line
  render() {
    const {
      className,
      formViewConfig,
      subscription,
      submitting,
      pristine,
      handleSubmit,
      noModal,
      closeModal,
      smsDisclaimer,
    } = this.props;
    const { left, right } = noModal ? config.pageColProps : config.modalColProps;
    const { IMG_DATA_MODAL, IMG_DATA_PAGE } = config;
    const { isFieldEmpty } = this.state;
    const isGym = isGymboree();
    const isCanadaCheck = isCanada();
    const { isMobile } = getViewportInfo() || {};
    if (isMobile) {
      return this.renderTapToJoin(isGym);
    }
    return (
      <Fragment>
        {subscription.success ? (
          <Grid className="full-height">
            <Row
              fullBleed
              className={`${className} full-height ${noModal ? 'smssubscribe__wrapper' : ''}`}
            >
              {!noModal && (
                <Col
                  isNotInlineBlock
                  colSize={left}
                  hideCol={{ small: true, medium: true }}
                  className="img-wrapper"
                >
                  <DamImage
                    imgConfigs={noModal ? IMG_DATA_PAGE : IMG_DATA_MODAL}
                    imgData={this.getImageData()}
                  />
                </Col>
              )}
              <Col
                isNotInlineBlock
                colSize={right}
                ignoreGutter={{ large: true }}
                className="sms-signup-content smsform__wrapper form-wrapper"
                aria-live="polite"
                aria-atomic="true"
              >
                <SignupConfirm
                  formViewConfig={formViewConfig}
                  tncHtml={smsDisclaimer}
                  susbscriptionType="sms"
                />
                <Row className="button-wrapper" fullBleed>
                  <Col colSize={{ small: 4, medium: 4, large: 4 }} className="button-container">
                    <Anchor
                      to={formViewConfig.lbl_SignUp_shopNowBtnUrl}
                      asPath={formViewConfig.lbl_SignUp_shopNowBtnUrl}
                      target={formViewConfig.lbl_SignUp_shopNowBtnUrlTarget}
                      onClick={closeModal}
                    >
                      <Button
                        fullWidth
                        buttonVariation="fixed-width"
                        fill="BLUE"
                        type="submit"
                        className="shop-button"
                        dataLocator="shop_now_btn"
                      >
                        {formViewConfig.lbl_SignUp_shopNowLabel}
                      </Button>
                    </Anchor>
                  </Col>
                  {noModal && (
                    <Col
                      colSize={{ small: 4, medium: 4, large: 4 }}
                      className="button-container"
                      offsetLeft={{ large: 2, small: 0, medium: 0 }}
                    >
                      <Anchor
                        to={getLabelValue(formViewConfig, 'lbl_SignUp_storeLocatorUrl')}
                        asPath={getLabelValue(formViewConfig, 'lbl_SignUp_storeLocatorUrl')}
                      >
                        <Button
                          fullWidth
                          buttonVariation="fixed-width"
                          fill="BLUE"
                          type="submit"
                          className="shop-button"
                          dataLocator="store-locator_btn"
                        >
                          {getLabelValue(formViewConfig, 'lbl_SignUp_storeLocatorLabel')}
                        </Button>
                      </Anchor>
                    </Col>
                  )}
                </Row>
              </Col>
            </Row>
          </Grid>
        ) : (
          <form onSubmit={handleSubmit(this.submitForm)}>
            <Grid>
              <Row
                fullBleed={{ large: true }}
                className={`${className} wrapper form_container ${
                  noModal ? 'smssubscribe__wrapper' : ''
                }`}
              >
                {!noModal && (
                  <Col
                    isNotInlineBlock
                    colSize={left}
                    hideCol={{ small: true, medium: true }}
                    className="img-wrapper"
                  >
                    <DamImage
                      imgConfigs={noModal ? IMG_DATA_PAGE : IMG_DATA_MODAL}
                      imgData={this.getImageData()}
                    />
                  </Col>
                )}
                <Col colSize={right} className="smsform__wrapper form-wrapper marginRightZero">
                  <div className="smsform__content">
                    <Row fullBleed>
                      {!noModal && (
                        <Col
                          colSize={{ small: 6, medium: 6, large: 10 }}
                          offsetLeft={{ small: 0, medium: 1, large: 1 }}
                          className="logo-wrapper"
                        >
                          <Image
                            className="logo"
                            aria-hidden="true"
                            src={
                              isGymboree()
                                ? getIconPath('gymboree-icon')
                                : getIconPath('header__brand-tab--tcp')
                            }
                          />
                        </Col>
                      )}
                    </Row>
                    <SignupFormIntro formViewConfig={formViewConfig} noModal={noModal} />
                    <Col
                      colSize={{ small: 6, medium: 6, large: noModal ? 8 : 12 }}
                      offsetLeft={{ small: 0, medium: 1, large: 0 }}
                      className="field-container"
                    >
                      <Field
                        placeholder={formViewConfig.lbl_SignUp_placeholderText}
                        name="signupPhoneNumber"
                        id="signupPhoneNumber"
                        type="text"
                        component={TextBox}
                        maxLength={50}
                        dataLocator="sms_address_field"
                        normalize={formatPhoneNumber}
                        enableSuccessCheck={false}
                        onChange={this.fieldChange}
                      />

                      {!isCanadaCheck && (
                        <Field
                          name="optPhoneSignupSecondBrand"
                          id="optPhoneSignupSecondBrand"
                          className="phone-signup-second-brand"
                          component={InputCheckbox}
                          dataLocator={isGym ? 'sms_tcp_opt' : 'sms_gym_opt'}
                          type="checkbox"
                        >
                          {isGym
                            ? formViewConfig.lbl_SignUp_tcpSignUpLabel
                            : formViewConfig.lbl_SignUp_gymSignUpLabel}
                        </Field>
                      )}

                      {!noModal && (
                        <div className="termscontainer">
                          {smsDisclaimer && smsDisclaimer.richText ? (
                            <RichText richTextHtml={smsDisclaimer.richText} />
                          ) : null}
                        </div>
                      )}
                    </Col>
                    <Row className="button-wrapper-form" fullBleed>
                      <Col
                        colSize={{ small: 4, medium: 4, large: 6 }}
                        className="button-wrapper-form__joinnowbtn"
                      >
                        <Button
                          disabled={isFieldEmpty || pristine || submitting}
                          fullWidth
                          buttonVariation="fixed-width"
                          fill="BLUE"
                          type="submit"
                          className="join-button"
                          dataLocator="join_now_btn"
                        >
                          {noModal
                            ? formViewConfig.lbl_SignUp_signupBtn
                            : formViewConfig.lbl_SignUp_joinButtonLabel}
                        </Button>
                        {
                          // styled components issue while updating tests on this page
                        }
                      </Col>
                      <Row>
                        <Col
                          className="no-thanks-wrapper"
                          colSize={{ small: 4, medium: 4, large: 6 }}
                        >
                          <Anchor
                            className="no-thanks-sms"
                            handleLinkClick={(e) => {
                              e.preventDefault();
                              closeModal();
                            }}
                            noLink
                          >
                            {formViewConfig.noThanksLabel}
                          </Anchor>
                        </Col>
                      </Row>
                    </Row>
                    {noModal && (
                      <Row>
                        <Col
                          colSize={{ large: 10, medium: 8, small: 6 }}
                          className="termscontainer"
                        >
                          {smsDisclaimer && smsDisclaimer.richText ? (
                            <RichText richTextHtml={smsDisclaimer.richText} />
                          ) : null}
                        </Col>
                      </Row>
                    )}
                  </div>
                </Col>
              </Row>
            </Grid>
          </form>
        )}
      </Fragment>
    );
  }
}

SmsSignupForm.propTypes = {
  buttonConfig: PropTypes.shape({}),
  className: PropTypes.string,
  formViewConfig: PropTypes.shape({}).isRequired,
  confirmationViewConfig: PropTypes.shape({}).isRequired,
  clearSmsSignupForm: PropTypes.func,
  subscription: PropTypes.shape({}),
  submitSmsSubscription: PropTypes.func,
  validateSignupSmsPhoneNumber: PropTypes.func,
  trackSubscriptionSuccess: PropTypes.func,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func,
  colProps: PropTypes.shape({
    left: PropTypes.shape({}),
    right: PropTypes.shape({}),
  }),
  noModal: PropTypes.bool,
  imageData: PropTypes.shape({}),
  closeModal: PropTypes.func,
  defaultStore: PropTypes.shape({}),
  smsDisclaimer: PropTypes.shape({}),
  smsFromPage: PropTypes.string.isRequired,
};

SmsSignupForm.defaultProps = {
  buttonConfig: {},
  className: '',
  subscription: {},
  submitSmsSubscription: () => {},
  trackSubscriptionSuccess: () => {},
  validateSignupSmsPhoneNumber: () => Promise.resolve({}),
  clearSmsSignupForm: () => {},
  handleSubmit: () => {},
  colProps: {},
  noModal: false,
  imageData: {},
  closeModal: () => {},
  defaultStore: {},
  smsDisclaimer: {},
};

export default withStyles(
  reduxForm({
    form: 'SmsSignupForm',
    initialValues: {
      signupPhoneNumber: '',
    },
  })(SmsSignupForm),
  smsSignupStyle
);
export { SmsSignupForm as SmsSignupFormVanilla };
