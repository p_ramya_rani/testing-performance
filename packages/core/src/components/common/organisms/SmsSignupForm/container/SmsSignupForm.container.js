// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import get from 'lodash/get';
import { getLabelValue } from '@tcp/core/src/utils';
import { validatePhoneNumber } from '@tcp/core/src/utils/formValidation/phoneNumber';
import { trackPageView, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';

import { submitSmsSignup, clearSmsSignupForm } from './SmsSignupForm.actions';
import SmsSignupFormView from '../views/SmsSignupForm';

export const mapDispatchToProps = (dispatch) => {
  return {
    submitSmsSubscription: (payload) => {
      dispatch(submitSmsSignup(payload));
    },
    clearSmsSignupForm: () => {
      dispatch(clearSmsSignupForm());
    },
    validateSignupSmsPhoneNumber: (phoneNumber) => {
      return validatePhoneNumber(phoneNumber) ? Promise.resolve({}) : Promise.reject();
    },

    trackSubscriptionSuccess: () => {
      dispatch(
        setClickAnalyticsData({
          customEvents: ['event107', 'event80'],
          pageName: 'content:email confirmation',
          pageShortName: 'content:sms confirmation',
          pageSection: 'content',
          pageSubSection: 'content',
          pageType: 'content',
          pageTertiarySection: 'content',
        })
      );

      dispatch(trackPageView({}));
      const analyticsTimer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(analyticsTimer);
      }, 200);
    },
  };
};

const mapStateToProps = (state, props) => {
  const { SmsSignupFormReducer: { subscription } = {} } = state.SmsSignUp;
  const { mediaWrapper, set } = props;
  const richText = get(state, 'Footer.smsSignupBtn.richText.text', '');
  const smsDisclaimer = { richText };
  return {
    formViewConfig: {
      ...state.Labels.global.smsSignup,
      noThanksLabel: getLabelValue(state.Labels, 'lbl_SignUp_noThanks', 'emailSignup', 'global'),
    },
    subscription,
    smsDisclaimer,
    ...props,
    noModal: true,
    imageData: mediaWrapper && mediaWrapper.length > 0 ? mediaWrapper[0] : {},
    formDirectionVal: set && set.filter((item) => item.key === 'formPlacement')[0].val,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SmsSignupFormView);
