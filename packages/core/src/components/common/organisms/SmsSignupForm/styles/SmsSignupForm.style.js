// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const getFormAlignment = {
  left: 'flex-start',
  right: 'flex-end',
  center: 'center',
};

export default css`
  ${props =>
    props.noModal
      ? `
    @media ${props.theme.mediaQuery.large} {
      padding: 0 15px;
    }
  `
      : ``}
  .field-container {
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.XXXL};

    .phone-signup-second-brand {
      padding-top: 25px;
      ${props =>
        props.noModal
          ? ``
          : `
      p {
        padding-top: 2px;
        display: block;
      }
      `}
    }

    .CheckBox__text {
      margin-top: 6px;
    }
  }

  .form_container {
    position: relative;
    flex-direction: column;
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
  }
  .no-thanks-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
  .no-thanks-sms {
    font-size: ${props => props.theme.fonts.fontSize.anchor.medium}px;
    text-decoration: underline;
    font-family: ${props => props.theme.typography.fonts.secondary};
    text-transform: uppercase;
  }
  .sms-signup-content {
    position: relative;
  }
  .full-height {
    height: 100%;
  }
  .button-wrapper,
  .button-wrapper-form {
    background: ${props => props.theme.colors.BUTTON.WHITE.ALT_FOCUS};
    margin: 24px -15px 0 0;
    display: flex;
    justify-content: center;
    padding: 9px 0;
    @media ${props => props.theme.mediaQuery.mediumMax} {
      position: sticky;
      left: 0;
      right: 0;
      bottom: 0;
      ${props =>
        !props.noModal
          ? `padding: 21px 0 20px 0;
      background:  ${props.theme.colors.BUTTON.WHITE.HOVER};`
          : ''}
    }
  }
  .button-wrapper-form {
    margin: ${props => (props.noModal ? '32px 0 40px' : '24px -14px 0 -14px')};
    width: 100vw;
    @media ${props => props.theme.mediaQuery.medium} {
      margin: ${props => (props.noModal ? '32px 0 40px' : '24px -36px 0 -36px')};
    }
    @media ${props => props.theme.mediaQuery.large} {
      margin: ${props => (props.noModal ? '24px 0 24px' : '24px -36px 0 -36px')};
    }
  }
  .button-wrapper-tap-join {
    width: 100%;
    margin-top: 40px;
    margin-bottom: 20px;
    background: ${props => props.theme.colors.WHITE};
  }
  .button-container {
    width: 225px;
    box-sizing: border-box;
  }
  .async-error input {
    border-bottom: 1px solid ${props => props.theme.colors.NOTIFICATION.ERROR};
  }
  .async-success input {
    border-bottom: 1px solid ${props => props.theme.colors.TEXTBOX.SUCCESS_BORDER};
  }
  .terms-label {
    margin-top: 32px;
    padding-left: 47px;
    padding-right: 47px;
    @media ${props => props.theme.mediaQuery.medium} {
      padding-left: 0px;
      padding-right: 0px;
    }
  }
  .Modal-Header {
    .alignRight {
      right: 14px;
      @media ${props => props.theme.mediaQuery.medium} {
        right: 16px;
      }
    }
  }
  .alignTop {
    top: 14px;
    z-index: 1;
    @media ${props => props.theme.mediaQuery.medium} {
      top: 16px;
    }
  }

  .logo-wrapper {
    text-align: center;
    padding: 29px 0 28px 0;
    @media ${props => props.theme.mediaQuery.large} {
      padding: 31px 0 28px 0;
    }
  }

  .logo-wrapper img {
    height: 40px;
    @media ${props => props.theme.mediaQuery.medium} {
      height: 35px;
    }
  }

  .termscontainer {
    line-height: 1.4;
    font-size: ${props => props.theme.typography.fontSizes.fs10};
    margin-top: ${props => (props.noModal ? '0px' : '20px')};
  }

  .wrapper {
    width: 100%;
    margin-left: 0px;
    margin-right: 0px;
    padding-left: 14px;
    padding-right: 14px;
  }
  .marginRightZero {
    margin-right: 0px !important;
  }
  @media ${props => props.theme.mediaQuery.smallOnly} {
    .button-wrapper {
      position: sticky;
      width: 100%;
      bottom: 0;
    }
  }
  @media ${props => props.theme.mediaQuery.medium} {
    .button-wrapper,
    .button-wrapper-form {
      width: ${props => (props.noModal ? '100%' : '457px')};
    }
    .button-container {
      bottom: 24px;
      width: 222px;
      box-sizing: border-box;
    }
    .smsform__content {
      max-width: ${props => (props.noModal ? 'unset' : '381px')};
      margin: 0 auto;
    }
  }
  @media ${props => props.theme.mediaQuery.large} {
    .field-container {
      padding-top: 0;
    }
    .button-wrapper {
      bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
      position: absolute;
      padding-bottom: 25px;
    }
    .button-wrapper,
    .button-wrapper-form {
      ${props => (!props.noModal ? `margin: 24px auto 0;` : '')}
      padding: 0;
      padding-bottom: 24px;
      width: 100%;
      background: none;
    }
    .img-wrapper {
      display: block;
      ${props => (props.noModal ? `` : `width: 300px`)};
      margin-right: 0;
      height: 645px;

      img {
        height: 100%;
      }
    }
    ${props =>
      props.noModal
        ? ``
        : `
      .img-wrapper + div {
        width: calc(100% - 300px);
        position: absolute;
          top: 0;
          bottom: 0;
          overflow-y: auto;
          right: 0;
      }
    `}
    .shop-button {
      bottom: 0;
      width: 100%;
    }
  }

  ${props =>
    props.noModal
      ? `
    &.smssubscribe__wrapper {
      justify-content: center;
      .button-wrapper-form__joinnowbtn {
        width: 190px;
      }
      .button-wrapper-form, .button-wrapper {
        background: none;
        position: relative;
      }
      @media ${props.theme.mediaQuery.large} {
        background-image: url(${props.assetHost}/w_1440,f_auto,q_auto${props.imageData &&
          props.imageData.url});
        background-repeat: no-repeat;
        background-size: cover;
        margin: 0 auto;
        width: 97%;
        justify-content: ${getFormAlignment[props.formDirectionVal || 'right']};

        .smsform__wrapper {
          width: 570px;
          background-color: ${props.theme.colors.WHITE};
          margin: 120px;
        }
        .button-wrapper-form {
          margin-top: 24px;
          margin-bottom: 24px;
        }
      }

      @media ${props.theme.mediaQuery.mediumOnly} {
        .button-wrapper-form {
          margin-bottom: 40px;
        }
      }
      @media ${props.theme.mediaQuery.smallMax} {
        .button-wrapper-form {
          margin-bottom: 40px;
        }
      }
      .field-container {
        display: flex;
        flex-direction: column;
        margin: 0 auto;
      }
    }
    .termscontainer {
      margin: 0 auto 20px;
    }
    .terms-label {
      padding: 0;
      margin-top: 0;
    }
    @media ${props.theme.mediaQuery.smallMax} {
      .button-container:not(:last-child) {
        margin: 0 0 20px;
      }
    }
    @media ${props.theme.mediaQuery.large} {
      .button-container:last-child {
        margin-left: 5%;
      }
    }
  `
      : ''}
`;
