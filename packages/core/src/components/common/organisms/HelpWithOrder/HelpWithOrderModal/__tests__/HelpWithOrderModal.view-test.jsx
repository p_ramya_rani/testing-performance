// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { HelpWithOrderModalVanilla } from '../HelpWithOrderModal.view';

describe('HelpWithOrder component', () => {
  const returnImg =
    'https://test1.theplace.com/image/upload/v1595348498/ecom/assets/static/detail-menu/ic_keyboard_return_24px.svg';
  const props = {
    isCSHEnabled: true,
    setHelpWithOrderOpen: jest.fn(),
    setOrderResendModalOpen: jest.fn(),
    isHelpWithOrderOpen: false,
    className: 'something',
    helpWithOrderLabels: {
      receiptsCTA: 'Receipts',
      orderHelpCTA: 'Order Help',
      resendConfirmationText: 'Resend Order Confirmation',
      resendConfirmationImg:
        'https://test1.theplace.com/image/upload/v1595348482/ecom/assets/static/detail-menu/resend.svg',
      returnPolicyText: 'Return Policy',
      returnByMailText: 'Return By Mail',
      returnPolicyUrl: '/help-center/returns-exchanges#returnsandexchanges',
      returnByMailUrl: '/help-center/returns-exchanges#returning-exchange-bymail',
      returnPolicyImg: returnImg,
      returnByMailImg: returnImg,
      changePickUpText: 'Change Pick Up Person',
      changePickUpImg:
        'https://test1.theplace.com/image/upload/v1595348494/ecom/assets/static/detail-menu/person.svg',
      extendPickUpText: 'Extend Pick Time',
      extendPickUpImg:
        'https://test1.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/clock_3x.png',
      orderStatusHelpText: 'Contact Us About Your Order',
      orderStatusHelpUrl: '/content/order-status-help',
      orderStatusHelpImg:
        'https://test1.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/clock_3x.png',
      cshReturnItemOlderDays: 10,
    },
  };
  it('should render correctly', () => {
    const component = shallow(<HelpWithOrderModalVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly for canada sites', () => {
    const component = shallow(<HelpWithOrderModalVanilla {...props} isCanada />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly when isHelpWithOrderOpen is true', () => {
    const newProps = {
      ...props,
      isHelpWithOrderOpen: true,
    };
    const component = shallow(<HelpWithOrderModalVanilla {...newProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly for with killswitch isCSHEnabled enabled', () => {
    const component = shallow(<HelpWithOrderModalVanilla {...props} isCSHEnabled />);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly when orderId < snjMinOrderNo', () => {
    const newProps = {
      ...props,
      orderID: 1,
      snjMinOrderNo: 2,
    };
    const component = shallow(<HelpWithOrderModalVanilla {...newProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly for returneligible items', () => {
    const component = shallow(<HelpWithOrderModalVanilla {...props} returnEligible />);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly for older items', () => {
    const component = shallow(<HelpWithOrderModalVanilla {...props} isOldItem />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly with refundeligibleshipmentstatus', () => {
    const refundProps = {
      refundEligibleShipmentStatus: {
        deliveredItemShipment: 'item1',
        returnInitiated: 'item2',
        returnCompleted: 'item3',
      },
    };
    const component = shallow(<HelpWithOrderModalVanilla {...refundProps} {...props} />);
    expect(component).toMatchSnapshot();
  });
});
