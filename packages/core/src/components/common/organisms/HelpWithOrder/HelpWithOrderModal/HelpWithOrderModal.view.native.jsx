// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import { isOldItem } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import { BodyCopy, Anchor, Image } from '@tcp/core/src/components/common/atoms';
import {
  CUSTOMER_HELP_PAGE_TEMPLATE,
  CUSTOMER_HELP_GA,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import ResendOrderConfirmation from '../../ResendOrderConfirmation/container/ResendOrderConfirmation.container';
import {
  SafeAreaViewStyle,
  ModalOverlay,
  OrderHelpContent,
  ModalOutsideTouchable,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  LineWrapper,
  MoreHelpCtasWrapper,
  AnchorContainer,
  ImgStyle,
} from '../styles/HelpWithOrder.style.native';

const closeIcon = require('../../../../../../../mobileapp/src/assets/images/close.png');
const resendConfirmationImg = require('../../../../../../../mobileapp/src/assets/images/resend.png');
const returnPolicyImg = require('../../../../../../../mobileapp/src/assets/images/keyboard_return.png');
const orderStatusHelpImg = require('../../../../../../../mobileapp/src/assets/images/union.png');
const initiateReturnImg = require('../../../../../../../mobileapp/src/assets/images/initiate-return.png');

const MoreHelpCtas = (ctaProps) => {
  const { url, title, imgSource, navigation, handleLinkClick, onRequestClose } = ctaProps;
  const handleClick = handleLinkClick ? () => handleLinkClick() : null;
  return (
    <MoreHelpCtasWrapper>
      <Anchor
        url={url}
        navigation={navigation}
        onPress={handleClick}
        onReuqestClose={onRequestClose}
      >
        <AnchorContainer>
          <Image width={26} height={26} style={ImgStyle} source={imgSource} />
          <BodyCopy
            color="gray.900"
            fontWeight="bold"
            fontSize="fs14"
            fontFamily="secondary"
            textAlign="left"
            text={title}
            margin="0 0 0 20px"
          />
        </AnchorContainer>
      </Anchor>
    </MoreHelpCtasWrapper>
  );
};

const checkReturnEligiblefromOrderStatus = (refundEligibleShipmentStatus) => {
  const { returnInitiated, returnCompleted, deliveredItemShipment } =
    refundEligibleShipmentStatus || {};

  const isReturnInitiated = returnInitiated.length > 0;

  const isDeliveredItemShipment = deliveredItemShipment.length > 0;

  const isReturnCompleted = returnCompleted.length > 0;

  return isReturnInitiated || isDeliveredItemShipment || isReturnCompleted;
};

const HelpWithOrderModal = (props) => {
  const {
    className,
    helpWithOrderLabels,
    isHelpWithOrderOpen,
    setHelpWithOrderOpen,
    setOrderResendModalOpen,
    navigation,
    isCSHAppEnabled,
    orderId,
    trackAnalyticsClick,
    cshReturnItemOlderDays,
    orderDateWithoutFormatting,
    refundEligibleShipmentStatus,
    ...otherProps
  } = props;

  const {
    resendConfirmationText,
    returnPolicyUrl,
    returnPolicyText,
    orderHelpCTA,
    orderAssistanceHelpImg,
    initiateReturnText,
  } = helpWithOrderLabels || {};

  // const returnEligible = !isOldItem(new Date(orderDateWithoutFormatting), cshReturnItemOlderDays);
  let atLeastOneItemDelivered = false;
  if (refundEligibleShipmentStatus)
    atLeastOneItemDelivered = checkReturnEligiblefromOrderStatus(refundEligibleShipmentStatus);
  // Added this boolean variable to hide the Initiate Return CTA for mobile
  const isShowReturn = false;
  const returnEligible =
    !isOldItem(new Date(orderDateWithoutFormatting), cshReturnItemOlderDays) &&
    atLeastOneItemDelivered;

  return (
    <Modal
      isOpen={isHelpWithOrderOpen}
      onRequestClose={() => setHelpWithOrderOpen(false)}
      closeIconDataLocator="help-with-order-modal"
      customTransparent
      aria={{
        describedby: 'more-help-with-order',
      }}
    >
      <SafeAreaViewStyle>
        <ModalOutsideTouchable
          accessibilityRole="button"
          activeOpacity={1}
          onPressOut={() => setHelpWithOrderOpen(false)}
        >
          <ModalOverlay />
        </ModalOutsideTouchable>
        <OrderHelpContent>
          <LineWrapper borderWidth={2} borderColor="black" small />
          <ImageWrapper stickyCloseIcon>
            <StyledTouchableOpacity
              onPress={() => setHelpWithOrderOpen(false)}
              accessibilityRole="button"
              accessibilityLabel="close"
              isOverlay
            >
              <StyledCrossImage rightAlignCrossIcon source={closeIcon} />
            </StyledTouchableOpacity>
          </ImageWrapper>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            textAlign="center"
            color="gray.900"
            fontWeight="bold"
            text={orderHelpCTA}
          />
          <ResendOrderConfirmation {...otherProps} orderId={orderId} />
          {isShowReturn && returnEligible && (
            <MoreHelpCtas
              handleLinkClick={() => {
                setHelpWithOrderOpen(false);
                navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE, {
                  orderId,
                  fromPage: 'OrderDetailPage',
                  isInitiateReturn: 'true',
                });
              }}
              title={initiateReturnText}
              imgSource={initiateReturnImg}
              noLink
            />
          )}
          <MoreHelpCtas
            handleLinkClick={() => setOrderResendModalOpen(true)}
            title={resendConfirmationText}
            imgSource={resendConfirmationImg}
            noLink
          />

          <MoreHelpCtas
            url={returnPolicyUrl}
            title={returnPolicyText}
            imgSource={returnPolicyImg}
            navigation={navigation}
            onRequestClose={() => setHelpWithOrderOpen(false)}
          />
          {isCSHAppEnabled && (
            <MoreHelpCtas
              handleLinkClick={() => {
                setHelpWithOrderOpen(false);
                navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE, {
                  orderId,
                  fromPage: 'OrderDetailPage',
                });
                trackAnalyticsClick(
                  { orderHelpLocation: CUSTOMER_HELP_GA.HELP_DRAWER_TRACK.CD126 },
                  { name: 'orderHelpLocation_click', module: 'account' }
                );
              }}
              title={orderAssistanceHelpImg}
              imgSource={orderStatusHelpImg}
              navigation={navigation}
              onRequestClose={() => setHelpWithOrderOpen(false)}
            />
          )}
        </OrderHelpContent>
      </SafeAreaViewStyle>
    </Modal>
  );
};

HelpWithOrderModal.propTypes = {
  helpWithOrderLabels: PropTypes.shape({}),
  className: PropTypes.string,
  isHelpWithOrderOpen: PropTypes.bool.isRequired,
  setHelpWithOrderOpen: PropTypes.func.isRequired,
  setOrderResendModalOpen: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  selectOrderCustomerHelp: PropTypes.func,
  isCSHAppEnabled: PropTypes.bool,
  orderId: PropTypes.string,
  trackAnalyticsClick: PropTypes.func,
};

HelpWithOrderModal.defaultProps = {
  helpWithOrderLabels: {},
  className: '',
  selectOrderCustomerHelp: () => {},
  isCSHAppEnabled: false,
  orderId: '',
  trackAnalyticsClick: () => {},
};

export default HelpWithOrderModal;
export { HelpWithOrderModal as HelpWithOrderModalVanilla };
