// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@tcp/core/src/components/common/molecules/Modal/view/Modal.jsx';
import { routerPush, configureInternalNavigationFromCMSUrl, isCanada } from '@tcp/core/src/utils';
import { isOldItem } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor/views/Anchor.jsx';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy/views/BodyCopy.jsx';
import DamImage from '@tcp/core/src/components/common/atoms/DamImage/views/DamImage.jsx';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  CUSTOMER_HELP_GA,
  CUSTOMER_HELP_ROUTES,
  REASON_CODES,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { getAPIConfig } from '@tcp/core/src/utils/utils';
import ResendOrderConfirmation from '../../ResendOrderConfirmation/container/ResendOrderConfirmation.container';
import { helpWithOrderStyle, modalstyles } from '../styles/HelpWithOrder.style';

const MoreHelpCtas = (ctaProps) => {
  const { to, title, imgUrl, target, noLink, handleLinkClick, asPath, trackAnalyticsClick } =
    ctaProps;
  const handleClick = handleLinkClick
    ? (event) => {
        event.preventDefault();
        trackAnalyticsClick({
          orderHelpLocation: CUSTOMER_HELP_GA.HELP_DRAWER_TRACK.CD126,
        });
        handleLinkClick();
      }
    : null;
  return (
    <div className="more-help-inner-container">
      <Anchor
        to={to}
        title={title}
        asPath={asPath}
        noLink={noLink}
        target={target}
        className="more-help-with-order-ctas"
        handleLinkClick={handleClick}
      >
        <DamImage
          aria-hidden="true"
          className="more-help-with-order-image"
          imgConfigs={['w_100', 'w_100', 'w_100']}
          width="100%"
          imgData={{
            url: imgUrl,
            alt: title,
          }}
        />
        <BodyCopy
          fontSize="fs14"
          className="more-help-with-order-text"
          fontFamily="secondary"
          textAlign="left"
        >
          {title}
        </BodyCopy>
      </Anchor>
    </div>
  );
};

const checkReturnEligiblefromOrderStatus = (refundEligibleShipmentStatus) => {
  const { returnInitiated, returnCompleted, deliveredItemShipment } =
    refundEligibleShipmentStatus || {};
  const isReturnInitiated = returnInitiated.length > 0;
  const isDeliveredItemShipment = deliveredItemShipment.length > 0;
  const isReturnCompleted = returnCompleted.length > 0;
  if (isReturnCompleted && !isDeliveredItemShipment && !isReturnInitiated) return false;
  return isReturnInitiated || isDeliveredItemShipment || isReturnCompleted;
};

const HelpWithOrderModal = (props) => {
  const {
    className,
    helpWithOrderLabels,
    isHelpWithOrderOpen,
    setHelpWithOrderOpen,
    setOrderResendModalOpen,
    orderId,
    isCSHEnabled,
    trackAnalyticsClick,
    cshReturnItemOlderDays,
    orderDateWithoutFormatting,
    refundEligibleShipmentStatus,
    ...otherProps
  } = props;
  const {
    resendConfirmationText,
    returnPolicyUrl,
    returnPolicyText,
    returnPolicyImg,
    orderHelpCTA,
    resendConfirmationImg,
    orderStatusHelpText,
    orderStatusHelpUrl,
    orderStatusHelpImg,
    initiateReturnText,
    initiateReturnImg,
  } = helpWithOrderLabels || {};
  const { snjMinOrderNo } = getAPIConfig();
  let atLeastOneItemDelivered = false;
  if (refundEligibleShipmentStatus)
    atLeastOneItemDelivered = checkReturnEligiblefromOrderStatus(refundEligibleShipmentStatus);
  const returnEligible =
    atLeastOneItemDelivered &&
    !isOldItem(new Date(orderDateWithoutFormatting), cshReturnItemOlderDays);
  return (
    <Modal
      isOpen={isHelpWithOrderOpen}
      colSet={{ small: 6, medium: 6, large: 8 }}
      className={`${className} help-with-order-modal`}
      headingAlign="center"
      heading={orderHelpCTA}
      heightConfig={{ height: 'auto', maxHeight: '100%' }}
      overlayClassName="TCPModal__Overlay help-with-order-overlay"
      onRequestClose={() => setHelpWithOrderOpen(false)}
      closeIconDataLocator="help-with-order-modal"
      fixedWidth
      aria={{
        describedby: 'more-help-with-order',
      }}
      inheritedStyles={modalstyles}
    >
      {returnEligible && (
        <MoreHelpCtas
          title={initiateReturnText}
          imgUrl={initiateReturnImg}
          handleLinkClick={() =>
            routerPush(
              `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.to}&orderId=${orderId}&reasonCode=${REASON_CODES.INITIATE_RETURN}`,
              `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.asPath}/${orderId}/${REASON_CODES.INITIATE_RETURN}`
            )
          }
          noLink
          trackAnalyticsClick={trackAnalyticsClick}
        />
      )}

      {orderId < snjMinOrderNo && (
        <MoreHelpCtas
          handleLinkClick={() => setOrderResendModalOpen(true)}
          title={resendConfirmationText}
          imgUrl={resendConfirmationImg}
          noLink
          trackAnalyticsClick={trackAnalyticsClick}
        />
      )}

      <MoreHelpCtas
        to={returnPolicyUrl}
        title={returnPolicyText}
        imgUrl={returnPolicyImg}
        target="_blank"
        trackAnalyticsClick={trackAnalyticsClick}
      />

      {isCSHEnabled ? (
        <MoreHelpCtas
          title={orderStatusHelpText}
          imgUrl={orderStatusHelpImg}
          handleLinkClick={() =>
            routerPush(
              `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.to}&orderId=${orderId}`,
              `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.asPath}/${orderId}`
            )
          }
          noLink
          trackAnalyticsClick={trackAnalyticsClick}
        />
      ) : (
        !isCanada() && (
          <MoreHelpCtas
            to={configureInternalNavigationFromCMSUrl(orderStatusHelpUrl)}
            asPath={orderStatusHelpUrl}
            title={orderStatusHelpText}
            imgUrl={orderStatusHelpImg}
            trackAnalyticsClick={trackAnalyticsClick}
          />
        )
      )}

      <ResendOrderConfirmation {...otherProps} orderId={orderId} />
    </Modal>
  );
};

HelpWithOrderModal.propTypes = {
  helpWithOrderLabels: PropTypes.shape({}),
  className: PropTypes.string,
  isHelpWithOrderOpen: PropTypes.bool.isRequired,
  setHelpWithOrderOpen: PropTypes.func.isRequired,
  setOrderResendModalOpen: PropTypes.func.isRequired,
  orderAssistanceText: PropTypes.string,
  orderId: PropTypes.string.isRequired,
  isCSHEnabled: PropTypes.bool,
  trackAnalyticsClick: PropTypes.func,
};

HelpWithOrderModal.defaultProps = {
  helpWithOrderLabels: {},
  className: '',
  orderAssistanceText: '',
  isCSHEnabled: false,
  trackAnalyticsClick: () => {},
};

export default withStyles(HelpWithOrderModal, helpWithOrderStyle);
export { HelpWithOrderModal as HelpWithOrderModalVanilla };
