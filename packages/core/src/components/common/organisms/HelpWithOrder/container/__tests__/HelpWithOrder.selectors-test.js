// 9fbef606107a605d69c0edbcd8029e5d 
import { getHelpWithOrdersLabels } from '../HelpWithOrder.selectors';

const returnImg =
  'https://test1.theplace.com/image/upload/v1595348498/ecom/assets/static/detail-menu/ic_keyboard_return_24px.svg';
describe('HelpWithOrder Selectors', () => {
  const returnPolicy = 'Return Policy';
  it('getHelpWithOrdersLabels should return Labels', () => {
    const state = {
      Labels: {
        checkout: {
          orderConfirmation: {
            lbl_help_with_order_receipts: 'Receipts',
            lbl_help_with_order_modal_open: 'Order Help',
            'lbl_more_help_resend-confirm-text': 'Resend Order Confirmation',
            'lbl_more_help_resend-confirm-img':
              'https://test1.theplace.com/image/upload/v1595348482/ecom/assets/static/detail-menu/resend.svg',
            lbl_more_help_return_policy_text: returnPolicy,
            lbl_more_help_return_policy_url: '/help-center/returns-exchanges#returnsandexchanges',
            lbl_more_help_return_policy_img: returnImg,
            lbl_more_help_change_pick_up_text: 'Change Pick Up Person',
            lbl_more_help_change_pick_up_img:
              'https://test1.theplace.com/image/upload/v1595348494/ecom/assets/static/detail-menu/person.svg',
            lbl_more_help_extend_pick_up_text: 'Extend Pick Time',
            lbl_more_help_extend_pick_up_img:
              'https://test1.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/clock_3x.png',
          },
        },
        global: {
          helpCenter: {
            lbl_more_help_return_policy_text: returnPolicy,
          },
        },
      },
    };
    expect(getHelpWithOrdersLabels(state)).toMatchObject({
      receiptsCTA: 'Receipts',
      orderHelpCTA: 'Order Help',
      resendConfirmationText: 'Resend Order Confirmation',
      resendConfirmationImg:
        'https://test1.theplace.com/image/upload/v1595348482/ecom/assets/static/detail-menu/resend.svg',
      returnPolicyText: returnPolicy,
      returnPolicyUrl: '/help-center/returns-exchanges#returnsandexchanges',
      returnPolicyImg: returnImg,
      changePickUpText: 'Change Pick Up Person',
      changePickUpImg:
        'https://test1.theplace.com/image/upload/v1595348494/ecom/assets/static/detail-menu/person.svg',
      extendPickUpText: 'Extend Pick Time',
      extendPickUpImg:
        'https://test1.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/clock_3x.png',
    });
  });
});
