// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import {
  getIsCSHEnabled,
  getIsCSHAppEnabled,
  getCSHReturnItemOlderDays,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import { getHelpWithOrdersLabels } from './HelpWithOrder.selectors';
import HelpWithOrder from '../views/HelpWithOrder.view';
import { setOrderResendModalOpen } from '../../ResendOrderConfirmation/container/ResendOrderConfirmation.actions';

export const mapDispatchToProps = (dispatch) => {
  return {
    setOrderResendModalOpen: (payload) => {
      dispatch(setOrderResendModalOpen(payload));
    },
    trackAnalyticsClick: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 100);
    },
  };
};

const mapStateToProps = (state, props) => {
  return {
    helpWithOrderLabels: getHelpWithOrdersLabels(state),
    isCSHEnabled: getIsCSHEnabled(state),
    isCSHAppEnabled: getIsCSHAppEnabled(state),
    cshReturnItemOlderDays: getCSHReturnItemOlderDays(state),
    ...props,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HelpWithOrder);
