// 9fbef606107a605d69c0edbcd8029e5d 
import { mapDispatchToProps } from '../HelpWithOrder.container';

describe('HelpWithOrder component', () => {
  it('#fetchLabels should call on componentDidMount', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.setOrderResendModalOpen();
    expect(dispatch.mock.calls).toHaveLength(1);
  });
});
