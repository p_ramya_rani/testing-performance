// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';

const getLabels = (state) => {
  return state && state.Labels;
};

export const getHelpWithOrdersLabels = createSelector(getLabels, (labels) => {
  return {
    receiptsCTA: getLabelValue(
      labels,
      'lbl_help_with_order_receipts',
      'orderConfirmation',
      'checkout'
    ),
    orderHelpCTA: getLabelValue(
      labels,
      'lbl_help_with_order_modal_open',
      'orderConfirmation',
      'checkout'
    ),
    resendConfirmationText: getLabelValue(
      labels,
      'lbl_more_help_resend-confirm-text',
      'orderConfirmation',
      'checkout'
    ),
    resendConfirmationImg: getLabelValue(
      labels,
      'lbl_more_help_resend-confirm-img',
      'orderConfirmation',
      'checkout'
    ),
    returnPolicyText: getLabelValue(
      labels,
      'lbl_more_help_return_policy_text',
      'helpCenter',
      'global'
    ),
    returnPolicyUrl: getLabelValue(
      labels,
      'lbl_more_help_return_policy_url',
      'orderConfirmation',
      'checkout'
    ),
    returnPolicyImg: getLabelValue(
      labels,
      'lbl_more_help_return_policy_img',
      'orderConfirmation',
      'checkout'
    ),
    initiateReturnText: getLabelValue(labels, 'lbl_modal_initiate_return', 'helpCenter', 'global'),
    initiateReturnImg: getLabelValue(
      labels,
      'lbl_initiate_return_img',
      'orderConfirmation',
      'checkout'
    ),
    orderAssistanceHelpImg: getLabelValue(labels, 'lbl_order_help', 'helpCenter', 'global'),
    changePickUpText: getLabelValue(
      labels,
      'lbl_more_help_change_pick_up_text',
      'orderConfirmation',
      'checkout'
    ),
    changePickUpImg: getLabelValue(
      labels,
      'lbl_more_help_change_pick_up_img',
      'orderConfirmation',
      'checkout'
    ),
    extendPickUpText: getLabelValue(
      labels,
      'lbl_more_help_extend_pick_up_text',
      'orderConfirmation',
      'checkout'
    ),
    extendPickUpImg: getLabelValue(
      labels,
      'lbl_more_help_extend_pick_up_img',
      'orderConfirmation',
      'checkout'
    ),
    orderStatusHelpText: getLabelValue(
      labels,
      'lbl_order_status_help_text',
      'helpCenter',
      'global'
    ),
    orderStatusHelpUrl: getLabelValue(
      labels,
      'lbl_order_status_help_url',
      'orderConfirmation',
      'checkout'
    ),
    orderStatusHelpImg: getLabelValue(
      labels,
      'lbl_order_status_help_img',
      'orderConfirmation',
      'checkout'
    ),
    orderAssistanceText: getLabelValue(labels, 'lbl_order_help', 'helpCenter', 'global'),
  };
});

export default { getHelpWithOrdersLabels };
