// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { HelpWithOrderVanilla } from '../HelpWithOrder.view';

describe('HelpWithOrder component', () => {
  const props = {
    toggleReceiptsView: jest.fn(),
    helpWithOrderLabels: {
      receiptsCTA: 'receiptsCTA',
      orderHelpCTA: 'orderHelpCTA',
    },
    className: 'something',
  };
  it('should render correctly', () => {
    const component = shallow(<HelpWithOrderVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call the setstate on button click', () => {
    const setState = jest.fn();
    const useStateSpy = jest.spyOn(React, 'useState');
    useStateSpy.mockImplementation((init) => [init, setState]);

    const component = shallow(<HelpWithOrderVanilla {...props} />);
    component.find('.HelpWithOrder__more-button Styled(Button)').first().props().onClick();
    setTimeout(() => {
      expect(setState).toHaveBeenCalledWith(true);
    });
    expect(component).toMatchSnapshot();
  });
  it('should render with modal open isHelpWithOrderOpen', () => {
    const component = shallow(<HelpWithOrderVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
