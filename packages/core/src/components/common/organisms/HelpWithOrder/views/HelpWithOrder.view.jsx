// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { convertToISOString } from '@tcp/core/src/utils/utils';
import { getItemsWithTargetStatus } from '@tcp/core/src/components/features/account/Orders/molecules/OrderListWithDetails/views/OrderListWithDetails.utils';
import constants from '@tcp/core/src/components/features/account/Orders/Orders.constants';
import { Row, Button } from '@tcp/core/src/components/common/atoms';
import style from '../styles/HelpWithOrder.style';
import HelpWithOrderModal from '../HelpWithOrderModal/HelpWithOrderModal.view';

const HelpWithOrder = (props) => {
  const {
    helpWithOrderLabels,
    toggleReceiptsView,
    className,
    orderDetailsData,
    cshReturnItemOlderDays,
    ...otherProps
  } = props;
  const { STATUS_CONSTANTS = {} } = constants;
  const { DELIVERED, RETURN_INITIATED, RETURN_COMPLETED, REFUNDED } = STATUS_CONSTANTS;
  const { orderDate, purchasedItems } = orderDetailsData || {};
  let returnInitiated = [];
  let returnCompleted = [];
  let deliveredItemShipment = [];
  let refunded = [];
  if (purchasedItems) {
    deliveredItemShipment = getItemsWithTargetStatus(purchasedItems, DELIVERED);
    returnInitiated = getItemsWithTargetStatus(purchasedItems, RETURN_INITIATED);
    returnCompleted = getItemsWithTargetStatus(purchasedItems, RETURN_COMPLETED);
    refunded = getItemsWithTargetStatus(purchasedItems, REFUNDED);
    returnCompleted = [...returnCompleted, ...refunded];
  }
  const refundEligibleShipmentStatus = {
    deliveredItemShipment,
    returnInitiated,
    returnCompleted,
  };
  const [isHelpWithOrderOpen, setHelpWithOrderOpen] = useState(false);
  return (
    <Row className={className} fullBleed>
      <div className="HelpWithOrder__button-wrapper">
        <div className="HelpWithOrder__receipts-button">
          <Button
            buttonVariation="fixed-width"
            type="button"
            fill="WHITE"
            dataLocator="help-with-order"
            onClick={toggleReceiptsView}
          >
            {helpWithOrderLabels.receiptsCTA}
          </Button>
        </div>
        <div className="HelpWithOrder__more-button">
          <Button
            className="HelpWithOrder__order-help"
            buttonVariation="fixed-width"
            type="button"
            fill="BLUE"
            dataLocator="help-with-order"
            onClick={() => setHelpWithOrderOpen(true)}
          >
            {helpWithOrderLabels.orderHelpCTA}
          </Button>
        </div>
      </div>
      {isHelpWithOrderOpen && (
        <HelpWithOrderModal
          setHelpWithOrderOpen={setHelpWithOrderOpen}
          isHelpWithOrderOpen={isHelpWithOrderOpen}
          helpWithOrderLabels={helpWithOrderLabels}
          orderDateWithoutFormatting={convertToISOString(orderDate)}
          refundEligibleShipmentStatus={refundEligibleShipmentStatus}
          cshReturnItemOlderDays={cshReturnItemOlderDays}
          {...otherProps}
        />
      )}
    </Row>
  );
};

HelpWithOrder.propTypes = {
  helpWithOrderLabels: PropTypes.shape({}),
  className: PropTypes.string,
  toggleReceiptsView: PropTypes.func,
};

HelpWithOrder.defaultProps = {
  helpWithOrderLabels: {},
  className: '',
  toggleReceiptsView: () => {},
};

export default withStyles(HelpWithOrder, style);

export { HelpWithOrder as HelpWithOrderVanilla };
