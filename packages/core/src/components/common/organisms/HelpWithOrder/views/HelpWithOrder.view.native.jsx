// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@tcp/core/src/components/common/atoms';
import { convertToISOString } from '@tcp/core/src/utils/utils';

import { getItemsWithTargetStatus } from '@tcp/core/src/components/features/account/Orders/molecules/OrderListWithDetails/views/OrderListWithDetails.utils';

import constants from '@tcp/core/src/components/features/account/Orders/Orders.constants';
import {
  HelpWithOrderWrapper,
  ButtonWrapper,
  MoreHelpButton,
  CustomButtonStyle,
  CustomIconStyle,
} from '../styles/HelpWithOrder.style.native';
import HelpWithOrderModal from '../HelpWithOrderModal/HelpWithOrderModal.view.native';

const HelpWithOrder = (props) => {
  const { helpWithOrderLabels, toggleReceiptsView, className, orderDetailsData, ...otherProps } =
    props;
  const { STATUS_CONSTANTS = {} } = constants;

  const { DELIVERED, RETURN_INITIATED, RETURN_COMPLETED } = STATUS_CONSTANTS;

  const { orderDate, purchasedItems } = orderDetailsData || {};

  let returnInitiated = [];

  let returnCompleted = [];

  let deliveredItemShipment = [];

  if (purchasedItems) {
    deliveredItemShipment = getItemsWithTargetStatus(purchasedItems, DELIVERED);

    returnInitiated = getItemsWithTargetStatus(purchasedItems, RETURN_INITIATED);

    returnCompleted = getItemsWithTargetStatus(purchasedItems, RETURN_COMPLETED);
  }

  const refundEligibleShipmentStatus = {
    deliveredItemShipment,

    returnInitiated,

    returnCompleted,
  };
  const [isHelpWithOrderOpen, setHelpWithOrderOpen] = useState(false);

  return (
    <HelpWithOrderWrapper>
      <ButtonWrapper>
        <Button
          type="button"
          fill="WHITE"
          dataLocator="help-with-order"
          onPress={toggleReceiptsView}
          text={helpWithOrderLabels.receiptsCTA}
          width="48%"
          customTextStyle={CustomButtonStyle}
        />
        <MoreHelpButton>
          <Button
            type="button"
            fill="BLUE"
            dataLocator="help-with-order"
            onPress={() => setHelpWithOrderOpen(true)}
            text={helpWithOrderLabels.orderHelpCTA}
            showIcon
            customTextStyle={CustomButtonStyle}
            customIconStyle={CustomIconStyle}
            iconColor="white"
          />
        </MoreHelpButton>
      </ButtonWrapper>
      {isHelpWithOrderOpen && (
        <HelpWithOrderModal
          setHelpWithOrderOpen={setHelpWithOrderOpen}
          isHelpWithOrderOpen={isHelpWithOrderOpen}
          helpWithOrderLabels={helpWithOrderLabels}
          orderDateWithoutFormatting={convertToISOString(orderDate)}
          refundEligibleShipmentStatus={refundEligibleShipmentStatus}
          {...otherProps}
        />
      )}
    </HelpWithOrderWrapper>
  );
};

HelpWithOrder.propTypes = {
  helpWithOrderLabels: PropTypes.shape({}),
  className: PropTypes.string,
  toggleReceiptsView: PropTypes.func,
};

HelpWithOrder.defaultProps = {
  helpWithOrderLabels: {},
  className: '',
  toggleReceiptsView: () => {},
};

export default HelpWithOrder;

export { HelpWithOrder as HelpWithOrderVanilla };
