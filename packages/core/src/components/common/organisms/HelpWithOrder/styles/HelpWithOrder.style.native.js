// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import LineComp from '@tcp/core/src/components/common/atoms/Line';

const HelpWithOrderWrapper = styled.View`
  width: 100%;
  box-shadow: 0 0 ${props => props.theme.spacing.ELEM_SPACING.XXS} rgba(139, 139, 139, 0.5);
  background-color: ${props => props.theme.colors.WHITE};
  padding: ${props => props.theme.spacing.ELEM_SPACING.XS} 3%
    ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

const ButtonWrapper = styled.View`
  background-color: ${props => props.theme.colors.WHITE};
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

const MoreHelpButton = styled.View`
  width: 48%;
`;

const SafeAreaViewStyle = styled.SafeAreaView`
  flex: 1;
  background: rgba(0, 0, 0, 0.5);
`;

const ModalOutsideTouchable = styled.TouchableWithoutFeedback`
  flex: 1;
`;

const ModalOverlay = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.5);
`;

const OrderHelpContent = styled.View`
  background: ${props => props.theme.colorPalette.text.lightgray};
  width: 100%;
  padding: 0 ${props => props.theme.spacing.MODULE_SPACING.MED}
    ${props => props.theme.spacing.MODULE_SPACING.MED};
  position: absolute;
  bottom: 0;
  border-top-left-radius: 50;
  border-top-right-radius: 50;
  height: auto;
`;

const ImageWrapper = styled.View`
  position: absolute;
  right: 0;
`;

const StyledTouchableOpacity = styled.TouchableOpacity`
  padding: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS}
    ${props => props.theme.spacing.ELEM_SPACING.XXS} ${props => props.theme.spacing.ELEM_SPACING.SM}
    ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const StyledCrossImage = styled.Image`
  width: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const LineWrapper = styled(LineComp)`
  border-radius: 52px;
  width: 92px;
  margin-top: 15px;
`;

const MoreHelpCtasWrapper = styled.View`
  margin-bottom: 28px;
`;

const AnchorContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

const CustomButtonStyle = {
  textTransform: 'capitalize',
};

const CustomIconStyle = {
  top: 14,
  right: 30,
};

const ImgStyle = { resizeMode: 'contain' };

export {
  HelpWithOrderWrapper,
  ButtonWrapper,
  MoreHelpButton,
  SafeAreaViewStyle,
  ModalOutsideTouchable,
  ModalOverlay,
  OrderHelpContent,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  LineWrapper,
  CustomButtonStyle,
  CustomIconStyle,
  MoreHelpCtasWrapper,
  AnchorContainer,
  ImgStyle,
};
