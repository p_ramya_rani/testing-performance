// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const style = css`
  .HelpWithOrder__more-button {
    width: 100%;
    margin-left: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  }
  .HelpWithOrder__order-help {
    text-transform: capitalize;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      &:after {
        content: '';
        width: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
        height: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
        margin-left: 22px;
        display: inline-flex;
        background: url('${(props) => getIconPath('icon-carrot-white', props)}') no-repeat right
          center;
        transform: rotate(90deg);
      }
    }
  }
  .HelpWithOrder__button-wrapper {
    display: flex;
    bottom: 0;
    left: 0;
    position: fixed;
    width: 94%;
    z-index: 1000;
    background-color: ${(props) => props.theme.colorPalette.white};
    margin: 0px;
    box-shadow: 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 0 rgba(139, 139, 139, 0.5);
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS} 3%;
    justify-content: center;
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 100%;
      position: relative;
      padding: 0;
      box-shadow: none;
      z-index: unset;
    }
    .HelpWithOrder__receipts-button {
      width: 100%;
      button {
        text-transform: capitalize;
      }
    }
  }
`;

export const helpWithOrderStyle = css`
  padding: 0;
  .more-help-with-order-ctas {
    display: flex;
    align-items: center;
  }
  .more-help-inner-container {
    margin-bottom: 28px;
    margin-left: 22px;
  }
  .more-help-with-order-image {
    width: 26px;
    display: block;
  }
  .more-help-with-order-text {
    margin-left: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XS};
  }
  @media ${(props) => props.theme.mediaQuery.medium} {
    .more-help-inner-container {
      margin-bottom: 25px;
      margin-left: 27px;
    }
    .more-help-with-order-text {
      margin-left: 21px;
    }
  }
`;

export const modalstyles = css`
  div.TCPModal__InnerContent {
    height: auto;
    left: unset;
    top: unset;
    transform: none;
    bottom: 0;
    right: 0;
    border-radius: 50px 50px 0 0;
    width: 100%;
    padding: 15px 0;
    @media ${(props) => props.theme.mediaQuery.medium} {
      height: 100%;
      max-height: 100%;
      top: 0;
      width: 323px;
      border-radius: 0;
      padding: 0;
    }
    .close-modal {
      top: 14px;
    }
    .Modal_Heading {
      display: block;
      border: none;
      font-size: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      font-weight: bold;
      border-top: solid ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
        ${(props) => props.theme.colors.TEXT.DARKERGRAY};
      width: 82px;
      margin: 0 auto ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      padding-top: 28px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED}
          ${(props) => props.theme.spacing.ELEM_SPACING.MED}
          ${(props) => props.theme.spacing.ELEM_SPACING.MED} 27px;
        background-color: ${(props) => props.theme.colors.TEXT.PALEGRAY};
        height: unset;
        width: auto;
        border-top: none;
      }
    }
  }
`;

export default style;
