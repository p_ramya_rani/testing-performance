// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

// need to handle for direction props.

const socialStyle = css`
  .social-accounts__infoList {
    display: flex;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
  }
  .Facebook-icon--enable {
    width: 50px;
    height: 50px;
    background: transparent url('${(props) => getIconPath('circle-alert-fill', props)}') no-repeat 0
      0;
    background: url('${(props) => getIconPath('facebook-color-icon', props)}') no-repeat;
    background-size: cover;
  }
  .Instagram-icon--enable {
    width: 50px;
    height: 50px;
    background: url('${(props) => getIconPath('instagram-color-icon', props)}') no-repeat;
    background-size: cover;
  }
  .Instagram-icon--disable {
    width: 50px;
    height: 50px;
    background: url('${(props) => getIconPath('instagram-fade-color-icon', props)}') no-repeat;
    background-size: cover;
  }

  .Twitter-icon--enable {
    width: 50px;
    height: 50px;
    background: url('${(props) => getIconPath('twitter-color-icon', props)}') no-repeat;
    background-size: cover;
  }
  .Twitter-icon--disable {
    width: 50px;
    height: 50px;
    background: url('${(props) => getIconPath('twitter-fade-color-icon', props)}') no-repeat;
    background-size: cover;
  }

  .Facebook-icon--disable {
    width: 50px;
    height: 50px;
    background: url('${(props) => getIconPath('facebook-fade-color-icon', props)}') no-repeat;
    background-size: cover;
  }

  .social-accounts__align {
    display: flex;
    align-items: center;
    padding-right: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .social_accounts_cross_plus-icon {
    position: absolute;
    z-index: 1000;
    cursor: pointer;
    width: 230px;
    height: 55px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      right: 110px;
    }
  }
  .social-account-icon {
    position: absolute;
    right: 0;
  }
  .social-accounts-alignment {
    padding: 0 ${(props) => props.theme.spacing.LAYOUT_SPACING.LRG}
      ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
  }
  .button-style {
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.XXL} 0 0;
  }
  .points-theme {
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    color: ${(props) =>
      props.isPlcc
        ? props.theme.colorPalette.userTheme.plcc
        : props.theme.colorPalette.userTheme.mpr};
  }
`;

export default socialStyle;
