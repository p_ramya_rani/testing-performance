// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const Row = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const SocialMessage = styled.View`
  width: 220px;
`;

export { Row, SocialMessage };
