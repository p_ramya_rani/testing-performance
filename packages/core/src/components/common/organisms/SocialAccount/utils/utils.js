// 9fbef606107a605d69c0edbcd8029e5d 
import { getLabelValue } from '../../../../../utils';

/**
 * @function getLinkedSocialAccountLabel
 * @param {string} socialAccount - social account which is being linked.
 * @param {object} labels - label of linked social account
 * @returns {string} Label - Added social account reward points label
 */
const getLinkedSocialAccountLabel = (socialAccount, labels) => {
  switch (socialAccount) {
    case 'Gymboree_FacebookLink':
    case 'AddFacebookAcct':
      return getLabelValue(labels, 'lbl_prefrence_social_points_text_2');
    case 'AddInstagramAcct':
    case 'Gymboree_InstagramLink':
      return getLabelValue(labels, 'lbl_prefrence_social_points_text_2_instagram');
    case 'Gymboree_TwitterLink':
    case 'AddTwitterAcct':
      return getLabelValue(labels, 'lbl_prefrence_social_points_text_2_twitter');
    default:
      return getLabelValue(labels, 'lbl_prefrence_social_points_text_2');
  }
};

export default getLinkedSocialAccountLabel;
