// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton/views/SkeletonLine.view.native';
import { ScrollView, View } from 'react-native';
import { PropTypes } from 'prop-types';
import {
  SkeletonWrapper,
  StoreInfoSkeleton,
  StoreDetailSkeleton,
} from '../StoreListItemSkeleton.native.style';

const StoreListItemSkeletonView = () => {
  return (
    <SkeletonWrapper>
      <StoreInfoSkeleton>
        <LoaderSkelton width="100%" height="100%" />
      </StoreInfoSkeleton>
      <StoreDetailSkeleton>
        <LoaderSkelton width="100%" height="100%" />
      </StoreDetailSkeleton>
    </SkeletonWrapper>
  );
};

const StoreListItemSkeleton = ({ col }) => {
  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <View>
        {Array.from({ length: col }).map((cItem, index) => {
          return <StoreListItemSkeletonView key={index.toString()} />;
        })}
      </View>
    </ScrollView>
  );
};

StoreListItemSkeleton.propTypes = {
  col: PropTypes.number,
};

StoreListItemSkeleton.defaultProps = {
  col: 1,
};

export default StoreListItemSkeleton;
