// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import {
  PRODUCT_ADD_TO_BAG,
  PRODUCT_SKU_SELECTION_FORM,
  OUTFIT_LISTING_FORM,
} from '@tcp/core/src/constants/reducer.constants';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import { getAlternateBrandName } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import { updateMultipackSelection } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.actions';
import { getInitialMultipackMappingQv } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import { setDefaultStore } from '@tcp/core/src/components/features/account/User/container/User.actions';
import { isClient } from '@tcp/core/src/utils';
import {
  getIsNewQVEnabled,
  getIsNewPDPEnabled,
  getIsBopisOptionSelected,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import PickUpStoreModalView from '../views';
import * as PickupSelectors from './PickUpStoreModal.selectors';
import * as sessionSelectors from '../../../../../reduxStore/selectors/session.selectors';
import { maxAllowedStoresInCart, distancesMap } from '../PickUpStoreModal.config';
import {
  PICKUP_HEADING,
  PICKUP_HEADER_CLOSE_TEXT,
  CHANGE_STORE_HEADING,
} from '../PickUpStoreModal.constants';
import { isCanada, isMobileApp } from '../../../../../utils';
import {
  closePickupModal,
  getBopisStoresActn,
  setStoreSearchError,
  getUserCartStores,
  setPickupModal,
  pickupSelectedStore,
} from './PickUpStoreModal.actions';
import { addItemToCartBopis } from '../../../../features/CnC/AddedToBag/container/AddedToBag.actions';
import {
  getAccessibilityLabels,
  getPDPLabels,
  getSinglePageLoad,
  getInitialMultipackMapping,
  getSelectedMultipack,
} from '../../../../features/browse/ProductDetail/container/ProductDetail.selectors';
import {
  setFavoriteStoreActn,
  getFavoriteStoreActn,
} from '../../../../features/storeLocator/StoreLanding/container/StoreLanding.actions';

import { getAddedToPickupError } from '../../../../features/CnC/AddedToBag/container/AddedToBag.selectors';
import { updateCartItem } from '../../../../features/CnC/CartItemTile/container/CartItemTile.actions';
import BAG_ACTIONS from '../../../../features/CnC/BagPage/container/BagPage.actions';
import {
  closeQuickViewModal,
  updateAppTypeWithParams,
  setQuickViewProductDetailPageTitle,
  openQuickViewWithValues,
  quickViewSingleLoad,
  setDisableSelectedTab,
} from '../../QuickViewModal/container/QuickViewModal.actions';

import {
  getFromPage,
  getInitialQuickViewLoad,
  getDisableMultiPackTab,
} from '../../QuickViewModal/container/QuickViewModal.selectors';
import { setSelectedChangedStoreActiom } from '../../ProductPickup/container/ProductPickup.actions';

export const mapDispatchToProps = (dispatch) => {
  return {
    closeQuickViewModalAction: () => {
      dispatch(setQuickViewProductDetailPageTitle(''));
      dispatch(closeQuickViewModal({ isModalOpen: false }));
    },
    closePickupModal: (payload) => {
      dispatch(BAG_ACTIONS.setCartItemsSflError(null));
      dispatch(closePickupModal(payload));
    },
    onSearchAreaStoresSubmit: (payload) => {
      dispatch(getBopisStoresActn(payload));
    },
    onClearSearchFormError: () => {
      dispatch(setStoreSearchError({ errorMessage: '' }));
    },
    getUserCartStoresAndSearch: (payload) => {
      dispatch(getUserCartStores(payload));
    },
    addItemToCartInPickup: (payload) => {
      dispatch(addItemToCartBopis(payload));
    },
    updatePickUpCartItem: (payload) => {
      dispatch(updateCartItem(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    setFavoriteStore: (payload) => {
      dispatch(setFavoriteStoreActn(payload));
    },
    getFavoriteStore: (payload) => {
      dispatch(getFavoriteStoreActn(payload));
    },
    updateAppTypeHandler: (payload) => {
      dispatch(updateAppTypeWithParams(payload));
    },
    getDefaultStore: (payload) => {
      dispatch(setDefaultStore(payload));
    },
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    multipackSelectionAction: (payload) => {
      dispatch(updateMultipackSelection(payload));
    },
    getQickViewSingleLoad: (payload) => {
      dispatch(quickViewSingleLoad(payload));
    },
    getDisableSelectedTab: (payload) => {
      dispatch(setDisableSelectedTab(payload));
    },
    onPickUpOpenClick: (payload) => {
      dispatch(setPickupModal(payload));
    },

    onPickupStoreSelected: (payload) => {
      dispatch(pickupSelectedStore(payload));
      dispatch(setSelectedChangedStoreActiom(payload));
    },
  };
};

const getMultipackInitialValuesFromState = (state) => {
  return (
    PickupSelectors.getInitialMultipackMapping(state) ||
    getInitialMultipackMapping(state) ||
    getInitialMultipackMappingQv(state)
  );
};

const mapStateToProps = (state, ownProps) => {
  // creating new prop defaultStore which is a combination of
  //  favStore store or geo default store of user
  const favStore = PickupSelectors.getDefaultStore(state);
  const geoDefaultStore = PickupSelectors.getGeoDefaultStore(state);
  const defaultStore = favStore || geoDefaultStore || null;
  const { isShowAddItemSuccessNotification, onSubmit, onSubmitSuccess, navigation } = ownProps;

  let { reduxFormName, isNotProductAddToBag } = ownProps;
  const { router } = ownProps;
  const isShowDefaultSize = false; // TODO - Do we need this ? abTestingStoreView.getIsShowDefaultSize(state);
  const isOutfitPage = router && router.pathname === '/OutfitDetails';
  if (!isMobileApp() && isOutfitPage) {
    // This is the outfit detail page, hence modify the reduxFormName
    reduxFormName = OUTFIT_LISTING_FORM;
    isNotProductAddToBag = true;
  }
  const currentProduct = PickupSelectors.getCurrentProduct(state);
  const generalProductId = currentProduct && currentProduct.generalProductId;
  const atbProductFormId = !isNotProductAddToBag
    ? `${PRODUCT_ADD_TO_BAG}-${generalProductId}`
    : `${reduxFormName}-${generalProductId}`;
  const initialValueFromQuickView = PickupSelectors.getInitialValues(state, atbProductFormId);
  const itemValues = {
    showDefaultSizeMsg: false,
    formValues: initialValueFromQuickView,
  };
  const { colorFitSizeDisplayNames = null } = currentProduct;
  const isPickupModalOpen = PickupSelectors.getIsPickupModalOpen(state);
  const isBopisCtaEnabled = PickupSelectors.getIsBopisCtaEnabled(state);
  const isBossCtaEnabled = PickupSelectors.getIsBossCtaEnabled(state);
  const isPickUpWarningModal = PickupSelectors.getIsPickUpWarningModal(state);
  const openSkuSelectionForm = PickupSelectors.getOpenSkuSelectionForm(state);
  const storeSearchError = PickupSelectors.getStoreSearchError(state);
  const pickupSkuFormId = `${PRODUCT_SKU_SELECTION_FORM}-${generalProductId}`;
  const pickupBagFormId = `${PRODUCT_ADD_TO_BAG}-${generalProductId}`;
  const PickupSkuFormValues = {
    ...PickupSelectors.getInitialValues(state, pickupSkuFormId),
    pickupBagFormId,
  };
  const pickupAddToBagFormValues = {
    ...PickupSelectors.getInitialValues(state, pickupBagFormId),
  };
  const fromBagPage = PickupSelectors.getIsPickupModalOpenFromBagPage(state);
  const initialValuesFromBagPage = PickupSelectors.getInitialValuesFromBagPage(state);
  const updateCartItemStore = PickupSelectors.getUpdateCartItemStore(state);
  const isItemShipToHome = PickupSelectors.getIsItemShipToHome(state);
  const alwaysSearchForBOSS = PickupSelectors.getAlwaysSearchForBOSS(state);
  const openRestrictedModalForBopis = PickupSelectors.openRestrictedModalForBopis(state);
  const isGetUserStoresLoaded = PickupSelectors.getIsGetUserStoresLoaded(state);
  const isPdpPage = isClient() && window?.location?.pathname?.indexOf('/p/') !== -1;

  return {
    onAddItemToCartSuccess: isShowAddItemSuccessNotification,
    onSubmit,
    colorFitSizeDisplayNames,
    onSubmitSuccess,
    maxAllowedStoresInCart,
    cartBopisStoresList: PickupSelectors.getStoresOnCart(state),
    distancesMap,
    isShowExtendedSizesNotification: false,
    initialValues: fromBagPage ? initialValuesFromBagPage : itemValues.formValues,
    showDefaultSizeMsg: itemValues.showDefaultSizeMsg,
    isPickupStoreUpdating: false,
    requestorKey: '',
    defaultStore,
    isPickupModalOpen,
    isBopisCtaEnabled,
    isBossCtaEnabled,
    isPickUpWarningModal,
    openSkuSelectionForm,
    alwaysSearchForBOSS,
    openRestrictedModalForBopis,
    isCanada: isCanada(),
    addToBagError: getAddedToPickupError(state),
    isPlcc: PickupSelectors.getUserIsPlcc(state),
    currencySymbol: sessionSelectors.getCurrentCurrencySymbol(state),
    isInternationalShipping: sessionSelectors.getIsInternationalShipping(state),
    isBopisEnabled: sessionSelectors.getIsBopisEnabled(state),
    isBossEnabled: sessionSelectors.getIsBossEnabled(state),
    isRadialInventoryEnabled: sessionSelectors.getIsRadialInventoryEnabled(state),
    isShowDefaultSize,
    cartItemsCount: PickupSelectors.getItemsCount(state),
    pickupModalHeading: PICKUP_HEADING,
    pickupModalHeaderCloseText: PICKUP_HEADER_CLOSE_TEXT,
    storeSearchError,
    currentProduct,
    PickupSkuFormValues,
    currency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    navigation,
    updateCartItemStore,
    initialValuesFromBagPage,
    isItemShipToHome,
    accessibilityLabels: getAccessibilityLabels(state),
    isGetUserStoresLoaded,
    fromPage: getFromPage(state),
    pdpLabels: getPDPLabels(state),
    setInitialTCPStyleQty: PickupSelectors.getInitialTCPStyleQTY(state),
    singlePageLoad: getSinglePageLoad(state),
    initialMultipackMapping: getMultipackInitialValuesFromState(state),
    selectedMultipack: getSelectedMultipack(state),
    setInitialQuickViewLoad: getInitialQuickViewLoad(state),
    disableMultiPackTab: getDisableMultiPackTab(state),
    availableTCPmapNewStyleId: PickupSelectors.getAvailableTCPmapNewStyleId(state),
    isAfterPayEnabled: sessionSelectors.getIsAfterPayEnabled(state),
    newQVEnabled:
      !isOutfitPage &&
      (getIsNewQVEnabled(state) ||
        (getIsNewPDPEnabled(state) && isPdpPage && getIsNewQVEnabled(state))),
    productLoading: PickupSelectors.isProductLoading(state),
    fromBagPage,
    alternateBrand: PickupSelectors.getAlternateBrand(state) || getAlternateBrandName(state),
    changeStoreModalHeading: CHANGE_STORE_HEADING,
    isNewPDPEnabled: getIsNewPDPEnabled(state),
    suggestedStores: PickupSelectors.getSuggestedStores(state),
    isPickupStoreModalRedisgn: PickupSelectors.getPickupStoreModalRedsign(state),
    isBopisPickup: getIsBopisOptionSelected(state),
    pickupAddToBagFormValues,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PickUpStoreModalView);
