// 9fbef606107a605d69c0edbcd8029e5d
import { PICKUP_MODAL_ACTIONS_CONSTANTS } from '../PickUpStoreModal.constants';

export const togglePickupModal = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.PICKUP_MODAL_TOGGLE,
  };
};

export const setPickupModal = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_PICKUP_MODAL,
  };
};

export const setPickupModalProduct = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_PICKUP_MODAL_PRODUCT,
  };
};

export const openPickupModalWithValues = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.PICKUP_MODAL_OPEN,
  };
};

export const closePickupModal = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.PICKUP_MODAL_CLOSE,
  };
};

export const getBopisStoresActn = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.GET_BOPIS_STORES,
  };
};

export const setBopisStores = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_BOPIS_STORES,
  };
};

export const setStoreSearchError = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_STORE_SEARCH_ERROR,
  };
};

export const getUserCartStores = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.GET_USER_CART_STORES,
  };
};

export const setUserCartStores = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_USER_CART_STORES,
  };
};

export const setStoreSearchingState = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_STORE_SEARCH_STATE,
  };
};

export const setIsGetUserStoresLoaded = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_IS_USER_CART_STORES_LOADED,
  };
};

export const resetAlternateBrandInPickUp = () => {
  return {
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.RESET_ALTERNATE_BRAND,
  };
};

export const pickupSelectedStore = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_PICKUP_SELECTED_STORE,
  };
};
export const setBopisDefaultStores = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.SET_DEFAULT_STORE_LIST,
  };
};

export const setIsSkuEditFlag = (payload) => {
  return {
    payload,
    type: PICKUP_MODAL_ACTIONS_CONSTANTS.IS_SKU_EDIT,
  };
};

export default {
  togglePickupModal,
  closePickupModal,
  openPickupModalWithValues,
  getBopisStoresActn,
  setBopisStores,
  setStoreSearchError,
  setIsGetUserStoresLoaded,
  resetAlternateBrandInPickUp,
  pickupSelectedStore,
};
