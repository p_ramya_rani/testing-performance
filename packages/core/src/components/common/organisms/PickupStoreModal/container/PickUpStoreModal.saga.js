// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLatest, select, all } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { setDefaultStore } from '@tcp/core/src/components/features/account/User/container/User.actions';
import { getFavoriteStore } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import getBopisInventoryDetails from '@tcp/core/src/services/abstractors/common/bopisInventory/bopisInventory';
import { getSetGeoDefaultStoreActn } from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import {
  ANALYTICS_LABELS,
  BOPIS_ITEM_AVAILABILITY,
} from '@tcp/core/src/components/common/organisms/PickupStoreModal/PickUpStoreModal.constants';
import {
  setClickAnalyticsData,
  trackClick,
  trackClickWithData,
} from '@tcp/core/src/analytics/actions';
import { formatProductsData, isMobileApp } from '@tcp/core/src/utils/utils';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { getLocalStorage } from '../../../../../utils/localStorageManagement';
import names from '../../../../../constants/eventsName.constants';
import { PICKUP_MODAL_ACTIONS_CONSTANTS } from '../PickUpStoreModal.constants';
import { maxAllowedStoresInCart } from '../PickUpStoreModal.config';
import {
  setBopisStores,
  setStoreSearchError,
  setUserCartStores,
  setStoreSearchingState,
  setIsGetUserStoresLoaded,
  setPickupModalProduct,
  setBopisDefaultStores,
} from './PickUpStoreModal.actions';
import {
  submitGetBopisSearchByLatLng,
  getStoresPlusInventorybyLatLng,
  getCartStoresPlusInventory,
  getDefaultStoresPlusInventorybyLatLng,
} from '../../../../../services/abstractors/productListing/pickupStoreModal';
import {
  getCurrentProduct,
  getIsBopisCtaEnabled,
  getIsBossCtaEnabled,
} from './PickUpStoreModal.selectors';
import {
  getPageName,
  getStoreSearchDistance,
  getStoreSearchCriteria,
} from '../molecules/PickupStoreSelectionForm/container/PickupStoreSelectionForm.selectors';
import { getUnbxdXappConfigs } from '../../../../features/browse/ProductListing/container/ProductListing.saga';
import {
  getABTestPdpService,
  getIsPdpServiceEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import getProductInfoById from '../../../../../services/abstractors/productListing/productDetail';

let scName = '';

const storeBossBopisEligibility = (stores, isBopisCtaEnabled, isBossCtaEnabled) => {
  const bossBopisEnabled =
    stores.filter((store) => {
      return (
        store.storeBossInfo &&
        store.storeBossInfo.isBossEligible === '1' &&
        (store.pickupType.isStoreBossSelected || store.pickupType.isStoreBopisSelected)
      );
    }).length > 0;

  return bossBopisEnabled && (isBopisCtaEnabled || isBossCtaEnabled);
};

const getCurrentProductId = (currentProduct) => {
  return (
    currentProduct &&
    currentProduct.generalProductId &&
    currentProduct.generalProductId.split('_')[0]
  );
};

function* trackSearchEvent(isSuccess, stores, isFromSearchCta) {
  const currentProduct = yield select(getCurrentProduct);
  const pageNameProp = yield select(getPageName);
  const storeSearchCriteria = yield select(getStoreSearchCriteria);
  const storeSearchDistance = yield select(getStoreSearchDistance);
  const favStore = yield select(getFavoriteStore);
  const isBopisCtaEnabled = yield select(getIsBopisCtaEnabled);
  const isBossCtaEnabled = yield select(getIsBossCtaEnabled);
  const isBossBopisEnabledinStores = storeBossBopisEligibility(
    stores,
    isBopisCtaEnabled,
    isBossCtaEnabled
  );
  const productId = getCurrentProductId(currentProduct);
  const customEvents = ['event105'];
  scName = names.screenNames.pickup_store_search105;

  if (isSuccess && stores.length && favStore && isBossBopisEnabledinStores) {
    customEvents.push('event106');
    scName = names.screenNames.pickup_store_search106;
  }

  let pageName = '';
  if (productId) {
    pageName = `${pageNameProp}:${productId}:${
      currentProduct && currentProduct.name && currentProduct.name.toLowerCase()
    }`;
  }

  const clickData = {
    customEvents,
    storeSearchCriteria,
    storeSearchDistance,
    pageName,
  };

  if (isMobileApp()) {
    if (isFromSearchCta && isSuccess && stores.length > 0) {
      const name = ANALYTICS_LABELS.SEARCH_EVENT_NAME;
      const customEvent = ANALYTICS_LABELS.SEARCH_CUSTOM_EVENT;
      yield put(
        setClickAnalyticsData({
          customEvents: [customEvent],
          name,
          clickEvent: true,
        })
      );
      yield put(trackClick({ name: 'BOPIS_MODAL' }));
    }
    clickData.products = formatProductsData(currentProduct);
  }
  if (!isFromSearchCta) {
    yield put(
      trackClickWithData(clickData, {
        name: scName,
        module: 'browse',
      })
    );
  }
}

export function* getPickupStores(action) {
  const {
    payload,
    payload: {
      skuId,
      quantity,
      distance,
      country,
      variantId,
      isFromChangeStore,
      isFromSearchCta,
      cartItemsCount,
      isCallForCartStores = true,
    },
  } = action;
  // Reset the bopis store data
  yield put(setBopisDefaultStores({ stores: [] }));
  yield put(setBopisStores({ stores: [] }));
  // Reset error message
  yield put(setStoreSearchError(''));
  try {
    if (isFromChangeStore && isMobileApp()) {
      yield put(setLoaderState(true));
    }
    yield put(setStoreSearchingState(true));
    const locationRes = yield call(submitGetBopisSearchByLatLng, payload);
    const { errorMessage, location } = locationRes;
    if (errorMessage) {
      yield put(setStoreSearchError(errorMessage));
    } else {
      const reqObj = {
        skuId,
        quantity,
        distance,
        lat: location.lat,
        lng: location.lng,
        country,
        variantId,
      };
      if (isFromChangeStore && isMobileApp() && cartItemsCount > 0 && isCallForCartStores) {
        yield put(setUserCartStores({ stores: null }));
        const stores = yield call(getCartStoresPlusInventory, reqObj);
        yield put(setUserCartStores({ stores }));
      }
      const storesResponse = yield call(getStoresPlusInventorybyLatLng, reqObj);

      const { error, stores } = storesResponse;
      if (error) {
        yield put(setStoreSearchError(error));
        yield call(trackSearchEvent, false);
      } else {
        yield put(setBopisStores({ stores }));
        yield call(trackSearchEvent, true, stores, isFromSearchCta);
      }
    }
    yield put(setStoreSearchingState(false));
    if (isFromChangeStore) {
      yield put(setLoaderState(false));
    }
  } catch (err) {
    yield put(setLoaderState(false));
    logger.error({
      error: err,
      extraData: {
        component: 'PickUpStoreModalSaga - getPickupStores',
        payloadRecieved: payload,
      },
    });
    yield put(setStoreSearchingState(false));
  }
}
export function* getDefaultStores(action) {
  const { payload } = action;
  const { defaultStore } = payload;
  // Reset error message
  yield put(setStoreSearchError(''));
  try {
    yield put(setStoreSearchingState(true));
    const locationRes = yield call(submitGetBopisSearchByLatLng, payload);
    const { errorMessage, location } = locationRes;
    if (errorMessage) {
      yield put(setStoreSearchError(errorMessage));
    } else {
      const reqObj = {
        lat: location.lat,
        lng: location.lng,
        defaultStore,
      };

      const storesResponse = yield call(getDefaultStoresPlusInventorybyLatLng, reqObj);

      const { error, stores } = storesResponse;
      if (error) {
        yield put(setStoreSearchError(error));
      } else {
        yield put(setBopisDefaultStores({ stores }));
      }
    }
    yield put(setStoreSearchingState(false));
  } catch (err) {
    yield put(setLoaderState(false));
    logger.error({
      error: err,
      extraData: {
        component: 'PickUpStoreModalSaga - getPickupStores',
        payloadRecieved: payload,
      },
    });
    yield put(setStoreSearchingState(false));
  }
}
function getItemBopisAvailability(inventory, quantity) {
  if (inventory.quantity < quantity || inventory.status === 'Out of Stock') {
    return BOPIS_ITEM_AVAILABILITY.UNAVAILABLE;
  }
  if (inventory.status === 'In Stock') {
    return BOPIS_ITEM_AVAILABILITY.AVAILABLE;
  }
  return BOPIS_ITEM_AVAILABILITY.LIMITED;
}

function* getFavStoreDetails(stores, apiPayload) {
  // call favourite store api if the user's cart have not any store or
  // the selected cart store is not favorite
  const favoriteStore = {
    ...stores.find((store) => store.basicInfo.isDefault),
  };
  const { defaultStore: preferredStore, skuId, quantity, variantId, variantNo } = apiPayload;
  const defaultStore =
    JSON.parse(getLocalStorage('defaultStore')) || Object.assign({}, preferredStore);

  if (Object.keys(favoriteStore).length) {
    yield put(setDefaultStore(favoriteStore));
  } else if (defaultStore && defaultStore.basicInfo) {
    try {
      const itemInfo = [
        {
          storeId: defaultStore.basicInfo.id && defaultStore.basicInfo.id.substring(2),
          variantNo,
          itemPartNumber: variantId,
        },
      ];
      const inventoryArray = yield call(getBopisInventoryDetails, itemInfo);
      const inventory = inventoryArray[0];
      defaultStore.productAvailability = {
        quantity: inventory.quantity,
        skuId,
        status: getItemBopisAvailability(inventory, quantity),
      };
    } catch (err) {
      logger.error({
        error: err,
        extraData: {
          component: 'PickUpStoreModalSaga - getFavStoreDetails',
          payload: apiPayload,
        },
      });
      defaultStore.productAvailability = {
        quantity: 0,
        skuId,
        status: BOPIS_ITEM_AVAILABILITY.UNAVAILABLE,
      };
    }
    if (!defaultStore.basicInfo.isDefault) {
      // setting store as user's geo default store in state not fav store
      // TODO - To validate this once HOME Pod has worked on it
      yield put(getSetGeoDefaultStoreActn(defaultStore));
    } else {
      yield put(setDefaultStore(defaultStore));
    }
  }
}
// eslint-disable-next-line sonarjs/cognitive-complexity
export function* getUserCartStores(action) {
  const { payload } = action;
  try {
    const { alwaysSearchForBOSS, apiPayload } = payload;
    const { cartItemsCount, isFromChangeStore, skuId } = apiPayload;
    if (isFromChangeStore && isMobileApp()) {
      yield put(setLoaderState(true));
    }
    yield put(setBopisStores({ stores: [] }));
    yield put(setUserCartStores({ stores: null }));
    yield put(setStoreSearchError(''));
    yield put(setIsGetUserStoresLoaded(false));
    let stores = [];
    if (cartItemsCount > 0) {
      stores = yield call(getCartStoresPlusInventory, apiPayload);
    }
    const isSearchOnlyInCartStores = stores.length === maxAllowedStoresInCart;
    yield put(setUserCartStores({ stores }));
    const isValidZipCode = apiPayload && apiPayload.locationPromise;
    if (isFromChangeStore && isMobileApp()) {
      if (isValidZipCode && !skuId && (!isSearchOnlyInCartStores || alwaysSearchForBOSS)) {
        yield call(getDefaultStores, { payload: apiPayload });
      } else if (isValidZipCode && (!isSearchOnlyInCartStores || alwaysSearchForBOSS)) {
        yield call(getPickupStores, { payload: { ...apiPayload, isCallForCartStores: false } });
      }
    } else if (isValidZipCode && (!isSearchOnlyInCartStores || alwaysSearchForBOSS)) {
      yield call(getPickupStores, { payload: { ...apiPayload, isCallForCartStores: false } });
    }
    if (!isSearchOnlyInCartStores) {
      yield call(getFavStoreDetails, stores, apiPayload);
    }
    yield put(setIsGetUserStoresLoaded(true));
    if (isFromChangeStore) {
      yield put(setLoaderState(false));
    }
  } catch (err) {
    yield put(setLoaderState(false));
    logger.error({
      error: err,
      extraData: {
        component: 'PickUpStoreModalSaga - getUserCartStores',
        payloadRecieved: payload,
      },
    });
  }
}

const checkIfMultiPack = (payload) => {
  return payload && payload.isMultiPack ? payload.isMultiPack : null;
};

function* fetchProductData({ payload }) {
  const { setInitialTCPStyleQty, otherBrand } = payload;
  try {
    const state = yield select();
    const xappURLConfig = yield call(getUnbxdXappConfigs);
    const payloadArray = Array.isArray(payload) ? payload : [payload];
    const { orderInfo } = payloadArray[0];
    let itemBrand;
    if (orderInfo) {
      ({ itemBrand } = orderInfo);
    }
    const isPdpServiceEnabled = yield select(getIsPdpServiceEnabled);
    const enablePDPServiceABTest = yield select(getABTestPdpService);
    const fetchDetailArray = payloadArray.map((product) => {
      const { colorProductId, generalProductId } = product;
      return call(
        getProductInfoById,
        xappURLConfig,
        colorProductId,
        state,
        itemBrand,
        undefined,
        undefined,
        undefined,
        false,
        undefined,
        generalProductId,
        checkIfMultiPack(payload),
        isPdpServiceEnabled,
        enablePDPServiceABTest,
        null,
        otherBrand
      );
    });
    const productDetailArray = yield all(fetchDetailArray);
    yield put(
      setPickupModalProduct({
        product: productDetailArray && productDetailArray[0] && productDetailArray[0].product,
        setInitialTCPStyleQty,
      })
    );
  } catch (error) {
    logger.error({
      error,
      extraData: {
        component: 'Bopis modal',
        payloadRecieved: payload,
      },
    });
  }
}

function* PickUpStoreSaga() {
  yield takeLatest(PICKUP_MODAL_ACTIONS_CONSTANTS.SET_PICKUP_MODAL, fetchProductData);
  yield takeLatest(PICKUP_MODAL_ACTIONS_CONSTANTS.GET_BOPIS_STORES, getPickupStores);
  yield takeLatest(PICKUP_MODAL_ACTIONS_CONSTANTS.GET_USER_CART_STORES, getUserCartStores);
}

export default PickUpStoreSaga;
