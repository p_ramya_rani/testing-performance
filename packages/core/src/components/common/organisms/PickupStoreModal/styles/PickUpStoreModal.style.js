// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const styles = css`
  div.TCPModal__InnerContent {
    @media ${(props) => props.theme.mediaQuery.medium} {
      height: auto;
      max-height: 90%;
    }

    .close-modal {
      height: 14px;
      right: 0px;
      top: 12px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        top: 12px;
      }
      @media ${(props) => props.theme.mediaQuery.large} {
        top: 12px;
      }
    }
    font-family: Nunito;

    padding: 0 14px 17px;

    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 0px 24px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 0 17px;
    }

    .search-store {
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};

      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
        margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
      }

      @media ${(props) => props.theme.mediaQuery.large} {
        margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
        margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
      }
    }
  }

  .pickup-sku-selection {
    margin-bottom: ${(props) =>
      props.newQVEnabled
        ? props.theme.spacing.ELEM_SPACING.LRG
        : props.theme.spacing.ELEM_SPACING.XXXL};
    margin-right: 0;
    margin-left: 0;
    width: 100%;
    padding: 0;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: 61px;
      ${(props) =>
        props.newQVEnabled ? `margin-bottom: ${props.theme.spacing.ELEM_SPACING.XXXL};` : ''}
    }
    .product-view-details {
      display: inline-flex;
      ${(props) => (props.newQVEnabled ? 'display: block; white-space: pre-wrap;' : '')}
      .link-redirect {
        margin-left: ${(props) =>
          props.newQVEnabled ? '0px' : props.theme.spacing.ELEM_SPACING.XL};
      }

      .product-link {
        margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      }
      .product-cta-stick {
        position: absolute;
        margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
        margin-left: ${(props) => (props.newQVEnabled ? '0px' : '140px')};
      }
    }

    .product-name {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};

      @media ${(props) => props.theme.mediaQuery.medium} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs18};
        margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      }
    }

    .image-wrapper {
      width: 164px;
      display: flex;
      flex-direction: column;
      align-items: center;
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};

      @media ${(props) => props.theme.mediaQuery.medium} {
        width: 264px;
      }

      @media ${(props) => props.theme.mediaQuery.large} {
        width: 239px;
      }
    }

    .sku-items {
      flex: 1 1 0%;
    }

    .modal-header {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      text-align: center;
      color: ${(props) => props.theme.colorPalette.gray[900]};
      font-weight: ${(props) => props.theme.typography.fontWeights.black};
      font-size: ${(props) => props.theme.typography.fontSizes.fs22};
    }

    .price-container {
      display: block;
      @media ${(props) => props.theme.mediaQuery.medium} {
        display: flex;
      }
      .actual-price {
        color: ${(props) =>
          props.theme.isGymboree
            ? props.theme.colorPalette.gray[900]
            : props.theme.colorPalette.red};
        font-size: ${(props) => props.theme.typography.fontSizes.fs22};
      }

      .original-price {
        font-size: ${(props) => props.theme.typography.fontSizes.fs14};
        color: ${(props) => props.theme.colorPalette.gray[800]};
        margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
        line-height: 1.9;
      }

      .badge {
        display: none;
      }
    }

    .product-customize-form-container .product-price-container {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      .actual-price {
        color: ${(props) => (props.theme.isGymboree ? 'gray.900' : 'red.500')};
      }
    }

    .edit-form-css {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};

      .color-chips-selector-title,
      .size-and-fit-detail-title,
      .pdp-qty {
        text-transform: capitalize;
        font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      }

      .color-chips-selector-items-list {
        margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      }

      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      }

      @media ${(props) => props.theme.mediaQuery.large} {
        margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      }
    }

    .edit-form-css .qty-selector {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};

      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-top: 14px;
      }
    }
  }
`;

export const modalstyles = css`
  .Modal_Heading {
    border-bottom: 0px;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    margin-top: 0;
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: block;
    }
    font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
    font-size: ${(props) => props.theme.typography.fontSizes.fs22};
    text-align: center;
  }
  .Modal-Header {
    padding-top: 14px;
    z-index: 1;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-top: 24px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: 24px;
    }
  }
`;

export const modalstylesRedesign = css`
  div.TCPModal__InnerContent.pickup-store-innerContent {
    right: 0;
    left: auto;
    top: 0;
    bottom: 0;
    transform: none;
    box-shadow: 0 4px 8px 0 rgba(163, 162, 162, 0.5);
    padding: 7px 5px 0 14px;
    width: 350px;
    max-height: none;
    overflow-x: hidden;

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 480px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 480px;
      animation: slide-in 0.5s forwards;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      right: auto;
      left: 0;
      width: 100%;
      animation: slide-in-mobile 0.5s forwards;
      border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
        ${(props) => props.theme.spacing.ELEM_SPACING.XS} 0px 0px;
    }
    @keyframes slide-in {
      0% {
        transform: translateX(100%);
      }
      100% {
        transform: translateX(0%);
      }
    }
    @keyframes slide-in-mobile {
      0% {
        transform: translateY(100%);
      }
      100% {
        transform: translateY(5%);
      }
    }
    @keyframes slide-out {
      0% {
        transform: translateX(0%);
      }
      100% {
        transform: translateX(100%);
      }
    }

    .close-modal {
      background: url('${(props) => getIconPath('left-arrow-icon', props)}');
      background-size: 100% 100%;
      left: 0px;
      top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      @media ${(props) => props.theme.mediaQuery.medium} {
        top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        left: 10px;
      }
    }
    .Modal-Header {
      padding-top: 7px;
      z-index: ${(props) => props.theme.zindex.zLoader};
      background: none;
      @media ${(props) => props.theme.mediaQuery.medium} {
        padding-top: 0px;
      }
    }
    .Modal_Heading:before {
      content: '';
      position: absolute;
      height: 46px;
      margin: auto;
      z-index: -1;
      background-color: #f4f4f4;
      bottom: 39px;
      width: 100%;
      @media ${(props) => props.theme.mediaQuery.medium} {
        bottom: 38px;
        left: -13px;
        padding-right: 18px;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        left: -14px;
        padding-right: 19px;
      }
    }
    .Modal_Heading {
      border-bottom: none;
      padding-bottom: 26px;
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
      display: flex;
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      justify-content: center;
      margin-bottom: 0px;
      align-items: flex-start;
      height: 45px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        display: flex;
        justify-content: center;
        padding-top: 5px;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        padding-top: 0px;
      }
    }
  }
`;

export default styles;
