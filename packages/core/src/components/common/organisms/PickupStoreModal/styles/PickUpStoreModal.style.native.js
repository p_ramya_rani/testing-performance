// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';

const styles = css``;

export const modalstyles = css`
  padding-right: 14px;
  padding-left: 14px;
`;

export const ModalContent = styled.View``;
export const ComponentWrapper = styled.View`
  flex: 1;
  background: rgba(0, 0, 0, 0);
  height: 80%;
`;
export const TextWrapper = styled.View`
  height: 100px;
`;

export const FooterWrapper = styled.View`
  display: flex;
  width: 100%;
  flex-direction: column;
  flex: 1;
  margin-top: 8px;
  margin-bottom: 24px;
`;

export const StoreNamewrapper = styled.View`
  display: flex;
  flex-direction: row;
  width: 90%;
  margin-top: ${({ top }) => top};
  align-items: center;
  justify-content: center;
`;

export const OutofStockWrapper = styled.View`
  background-color: #f4cfd5;
  margin-right: 6px;
  border-radius: 25px;
  padding-horizontal: 10px;
  padding-vertical: 3px;
`;

export const BodyTextWrapper = styled.View`
  margin-left: ${({ left }) => left};
`;
export const ButtonWrapper = styled.View`
  padding-top: 10px;
  padding-horizontal: 18px;
  padding-bottom: 15px;
`;
export default styles;
