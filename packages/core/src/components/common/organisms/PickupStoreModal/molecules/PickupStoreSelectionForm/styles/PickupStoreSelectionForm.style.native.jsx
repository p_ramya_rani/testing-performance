// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { Platform } from 'react-native';

const styles = css`
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
  margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
`;

export const ModalTitleContainer = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
  margin-top: 20px;
`;

export const ModalTitle = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.typography.fontSizes.fs14};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  line-height: 16.8;
  font-weight: ${(props) => props.theme.typography.fontWeights.black};
`;

export const ModalCloseTouchable = styled.TouchableOpacity`
  position: absolute;
  right: 26;
`;

export const PickUpModalView = styled.View`
  flex: 1;
  margin: 0 14px 0;
`;

export const PickUpHeaderText = styled.Text`
  flex-direction: row;
  align-items: flex-start;
  font-size: ${(props) => props.theme.typography.fontSizes.fs16};
  font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
  text-transform: uppercase;
`;
export const PickUpHeaderTextRegularCase = styled.Text`
  flex-direction: row;
  align-items: flex-start;
  font-size: ${(props) => props.theme.typography.fontSizes.fs16};
  font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
`;
export const Row = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  margin-top: 10px;
`;

export const AddressCol = styled.View`
  width: 66%;
  margin-right: 4%;
  font-size: ${(props) => props.theme.typography.fontSizes.fs13};
  color: ${(props) => props.theme.colorPalette.gray[900]};
`;
export const DistanceCol = styled.View`
  width: 30%;
  margin-top: ${Platform.OS === 'ios' ? '5' : '3'};
  font-size: ${(props) => props.theme.typography.fontSizes.fs13};
  color: ${(props) => props.theme.colorPalette.gray[900]};
`;

export const ViewWrapper = styled.View`
  width: 100%;
  margin-top: 10px;
  margin-bottom: 8px;
`;

export const StyledStoreLocator = styled.View`
  width: 100%;
  flex-direction: row;
`;
export const StyledCurrentLocation = styled.View`
  margin-left: 6px;
`;

export const PickupStoreContainer = styled.View``;

export const dropDownStyle = {
  height: 23,
  border: 1,
};
export const dropDownStyleWithoutLine = {
  height: 20,
  border: 0,
};

export const itemStyle = {
  height: 23,
  paddingLeft: 6,
  color: 'black',
};

export const TextboxStyle = {
  height: 72,
  borderWidth: 5,
  borderColor: 'gray.700',
};

export const TextboxStyleFromStore = {
  borderColor: 'gray.2800',
};

export const ZIPContainerStyle = {
  height: 50,
};
export const AddressColIsfromChangeStore = styled.View`
  width: 38%;
  margin-right: 8px;
  font-size: ${(props) => props.theme.typography.fontSizes.fs13};
  color: ${(props) => props.theme.colorPalette.gray[900]};
`;

export const DistanceColIsFromChangeStaore = styled.View`
  width: 30%;
  margin-top: ${Platform.OS === 'ios' ? 0 : '3'};
  font-size: ${(props) => props.theme.typography.fontSizes.fs13};
  color: ${(props) => props.theme.colorPalette.gray[900]};
`;

export const ButtonWrapper = styled.TouchableOpacity`
  width: 26%;
  font-size: ${(props) => props.theme.typography.fontSizes.fs13};
  background-color: ${(props) =>
    !props.disabled ? props.theme.colorPalette.blue[800] : props.theme.colorPalette.gray['800']};
  justify-content: center;
  align-items: center;
  padding: 15px 21.5px 15px 22.5px;
  opacity: ${(props) => (!props.disabled ? '1' : '0.5')};
  margin-left: 8px;
  margin-top: 10px;
  height: 48px;
  border-radius: 16px;
`;

export default styles;
