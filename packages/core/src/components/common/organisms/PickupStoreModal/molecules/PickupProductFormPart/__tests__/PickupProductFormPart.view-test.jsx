// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { PickupProductFormPartVanilla } from '../views/PickupProductFormPart.view';
import PickupProductFormPartSkeleton from '../views/PickupProductFormPartSkeleton.view';

describe('PickupProductFormPart component', () => {
  const props = {
    colorFitSizeDisplayNames: [],
    colorFitsSizesMap: [],
    name: 'test',
    onEditSku: () => {},
    initialValues: { name: 'test' },
    currencySymbol: '$',
    imagePath: '/path/',
    listPrice: '22',
    offerPrice: '20',
    currencyAttributes: {},
    fromPage: 'bag',
    initialValuesFromBagPage: { name: 'dfsd' },
  };

  it('should render component correctly', () => {
    const component = shallow(<PickupProductFormPartVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render PickupProductFormPartSkeleton', () => {
    const newProps = { ...props, productLoading: true };
    const component = shallow(<PickupProductFormPartVanilla {...newProps} />);
    expect(component.find(PickupProductFormPartSkeleton)).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });
});
