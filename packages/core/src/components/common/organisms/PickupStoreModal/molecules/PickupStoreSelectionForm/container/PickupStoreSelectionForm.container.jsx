/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d

import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { propTypes as reduxFormPropTypes, change } from 'redux-form';
import { getQuickViewLabels } from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import { getAddressLocationInfoGmaps } from '@tcp/core/src/utils/addressLocationGmaps';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { setClickAnalyticsData, trackClick } from '../../../../../../../analytics/actions';
import PickupStoreSelectionForm from '../views';
import { getBrand, isMobileApp } from '../../../../../../../utils';
import { getAddressLocationInfo } from '../../../../../../../utils/addressLocation';
import {
  getPageName,
  getStoreSearchCriteria,
  getStoreSearchDistance,
  getUserStoreId,
} from './PickupStoreSelectionForm.selectors';
import {
  getSkuId,
  getVariantId,
  getMapSliceForSize,
  isProductOOS,
  isBOSSProductOOSQtyMismatched,
} from '../../../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import {
  PICKUP_LABELS,
  BOPIS_ITEM_AVAILABILITY,
  SKU_DETAILS,
} from '../../../PickUpStoreModal.constants';
import * as PickupSelectors from '../../../container/PickUpStoreModal.selectors';
import ORDER_ITEM_TYPE from '../../../../../../features/CnC/CartItemTile/CartItemTile.constants';
import { minStoreCount } from '../../../PickUpStoreModal.config';
import { getCartItemInfo } from '../../../../../../features/CnC/AddedToBag/util/utility';
import { getBreadCrumbs } from '../../../../../../features/browse/ProductDetail/container/ProductDetail.selectors';
import {
  getMapboxSwitch,
  getStoreSearchTypesParam,
  getIsBopisOptionSelected,
} from '../../../../../../../reduxStore/selectors/session.selectors';
import { setIsSkuEditFlag } from '../../../container/PickUpStoreModal.actions';

export const DISTANCES_MAP_PROP_TYPE = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
  })
);

const AKAMAI_ZIP_COOKIE = 'tcpZip';
const LATLONG_COOKIE = 'tcpGeoLoc';

class PickupStoreSelectionFormContainer extends React.Component {
  static propTypes = {
    /** flags if to search in current cart stores (if true)
     * or offer user to enter location to look for nearby stores (if false)
     */
    isSearchOnlyInCartStores: PropTypes.bool.isRequired,
    /** Callback for submitting this form. its parameters depend on the value of the prop
     * isSearchOnlyInCartStores as follows:
     * if true then it accepts skuId, quantity;
     * if false then it accepts locationPromise, skuId, quantity;
     * where locationPromise resolves to a location object with lat() and lng() methods that return the
     * latitude and longitude around which to search.
     */
    onSubmit: PropTypes.func.isRequired,
    /**
     * Callback for adding an item to cart for pickup isn the selected store.
     * Accepts: storeId, skuId (of the selected color/fit/size combination), quantity
     */
    onAddItemToCart: PropTypes.func.isRequired,

    /** We need to differentiate if Bopis Modal is open from cart or other place to change select item button's message (DT-27100) */
    isShoppingBag: PropTypes.bool.isRequired,

    // determines if variation is warning modal
    isPickUpWarningModal: PropTypes.bool.isRequired,

    /** Props passed by the redux-form Form HOC. */
    ...reduxFormPropTypes,

    /** Check to allow display of warning msg */
    /** global switches for BOSS and BOPIS */
    isBossEnabled: PropTypes.bool.isRequired,
    isBopisEnabled: PropTypes.bool.isRequired,
    isRadialInventoryEnabled: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);
    this.place = null;
    this.formData = null;
    this.state = {
      selectedStoreId: null,
      isBossSelected: null,
      isShowMessage: false,
      selectedValue: 25,
      tcpZip: null,
    };
    this.onSearch = this.onSearch.bind(this);
    this.untouch = this.untouch.bind(this);
    this.handleAddTobag = this.handleAddTobag.bind(this);
    this.handlePickupRadioBtn = this.handlePickupRadioBtn.bind(this);
    this.prePopulateZipCode = this.prePopulateZipCode.bind(this);
    this.getPreferredStoreData = this.getPreferredStoreData.bind(this);
    this.getIsBopisAvailable = this.getIsBopisAvailable.bind(this);
    this.preferredStore = null;
    this.isAutoSearchTriggered = false;
    this.isZipCodePrePopulated = false;
    this.initialValues = {
      addressLocation: '',
      distance: 25,
    };
  }

  componentWillMount() {
    this.getZip();
  }

  // get the akamai zip cookie when no default store selected
  getZip = () => {
    const tcpZipPromise = readCookie(AKAMAI_ZIP_COOKIE);

    if (tcpZipPromise) {
      let tcpZip;
      if (isMobileApp()) {
        tcpZipPromise.then((res) => {
          [tcpZip] = (res && res.split('+')) || [];
          this.setState({ tcpZip });
        });
      } else {
        [tcpZip] = tcpZipPromise.split('+');
        this.setState({ tcpZip });
      }
    }
  };

  getZipCode = (defaultStore) => {
    return (
      defaultStore &&
      defaultStore.basicInfo &&
      defaultStore.basicInfo.address &&
      defaultStore.basicInfo.address.zipCode
    );
  };

  prePopulateZipCodeAndSearch = (handleSubmit, changeDispatch) => {
    if (this.prePopulateZipCode()) {
      /*
       * If zipcode needs to be prepupulated then this method triggers
       * store's search automatically from user's default store zipcode
       */
      const { defaultStore, isSkuResolved } = this.props;
      const { tcpZip } = this.state;
      const zipCode = this.getZipCode(defaultStore);
      if (!this.isZipCodePrePopulated) {
        const zipcode = zipCode || tcpZip;
        const validSearchZipCode = zipcode ? zipcode.toString().split('-')[0] : '';
        changeDispatch('addressLocation', validSearchZipCode);
        this.isZipCodePrePopulated = true;
      }
      // submitting the search form forcefully in case of step-2
      const autoSearch = isSkuResolved;
      if (autoSearch) {
        // Adding setTimeout to handle case when fav store comes on the fly from API, not from redux
        this.isAutoSearchTriggered = true;
        const submitTimer = setTimeout(() => {
          handleSubmit(this.onSearch)();
          clearTimeout(submitTimer);
        }, 1);
      }
    }
  };

  getStoreList = ({ geometry, formObject }) => {
    const {
      colorFitsSizesMap,
      onSubmit,
      mapboxStoreSearchTypesParam,
      mapboxSwitch,
      isFromChangeStore,
      showLoaderDispatch,
    } = this.props;
    let latLongGeometry = null;
    let addressFormData = null;
    if (geometry) {
      latLongGeometry = geometry;
    }
    if (formObject) {
      addressFormData = formObject;
    }
    if (mapboxSwitch) {
      try {
        showLoaderDispatch(true); // We have only true here in try block because  for false  we are managing from saga
        const locationPromise = latLongGeometry
          ? Promise.resolve(latLongGeometry.location)
          : addressFormData &&
            getAddressLocationInfo(addressFormData.addressLocation, mapboxStoreSearchTypesParam);
        onSubmit(locationPromise, colorFitsSizesMap, addressFormData, isFromChangeStore);
        this.formData = { ...addressFormData };
      } catch (e) {
        showLoaderDispatch(false);
      }
    } else {
      const locationPromise = latLongGeometry
        ? Promise.resolve(latLongGeometry.location)
        : addressFormData && getAddressLocationInfoGmaps(addressFormData.addressLocation);
      onSubmit(locationPromise, colorFitsSizesMap, addressFormData, isFromChangeStore);
      this.formData = { ...addressFormData };
    }
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  onSearch = (formData, isfromSearchCta) => {
    const {
      isFromChangeStore,
      defaultStore,
      mapboxSwitch,
      mapboxStoreSearchTypesParam,
      colorFitsSizesMap,
      onSubmit,
    } = this.props;
    this.setState({
      selectedStoreId: null,
      isBossSelected: null,
    });
    if (isFromChangeStore && (formData?.isDefaultCase || formData?.isFromCurrentLocation)) {
      let zipCode = null;
      let latLong = null;
      if (formData?.isDefaultCase) {
        const { tcpZip } = this.state;
        zipCode = this.getZipCode(defaultStore) || tcpZip;
        if (!zipCode) {
          latLong = readCookie(LATLONG_COOKIE);
        }
      } else if (formData?.isFromCurrentLocation) {
        latLong = readCookie(LATLONG_COOKIE);
      }

      if (zipCode) {
        const formObject = {
          addressLocation: zipCode,
          distance: 25,
        };
        this.getStoreList({ formObject });
      } else if (latLong) {
        latLong.then((res) => {
          if (res) {
            const [lat, long] = res.split('|');
            const geometry = {
              location: {
                lat,
                lng: long,
              },
            };
            this.getStoreList({ geometry });
          }
        });
      }
    } else if (mapboxSwitch) {
      const locationPromise = this.place
        ? Promise.resolve(this.place.geometry.location)
        : formData && getAddressLocationInfo(formData.addressLocation, mapboxStoreSearchTypesParam);
      onSubmit(locationPromise, colorFitsSizesMap, formData, isFromChangeStore, isfromSearchCta);
      this.formData = { ...formData };
    } else {
      const locationPromise = this.place
        ? Promise.resolve(this.place.geometry.location)
        : formData && getAddressLocationInfoGmaps(formData.addressLocation);
      onSubmit(locationPromise, colorFitsSizesMap, formData, isFromChangeStore, isfromSearchCta);
      this.formData = { ...formData };
    }
  };

  getIsBopisAvailable = () => {
    const { defaultStore } = this.props;
    return (
      defaultStore &&
      defaultStore.productAvailability &&
      (defaultStore.productAvailability.status === BOPIS_ITEM_AVAILABILITY.AVAILABLE ||
        defaultStore.productAvailability.status === BOPIS_ITEM_AVAILABILITY.LIMITED)
    );
  };

  getPreferredStoreData = (defaultStore) => {
    this.preferredStore = defaultStore;
    return (
      this.preferredStore &&
      this.preferredStore.productAvailability &&
      this.preferredStore.productAvailability.status
    );
  };

  quantityChange = (selectedQuantity, form) => {
    this.setState({ selectedValue: selectedQuantity });
    const { distanceChange } = this.props;
    distanceChange({ form, selectedQuantity });
  };

  /**
   * @method renderVariationText
   * this method returns the variation text depending on the
   * stores selected by the user
   */
  renderVariationText = (storeLimitReached, sameStore) => {
    const { SELECT_STORE, ONE_STORE_SELECTED, SAME_STORE_BOPIS_BOPIS } = PICKUP_LABELS;
    const { cartBopisStoresList, allowBossStoreSearch, isBossCtaEnabled } = this.props;

    /**
     * @var allowBossMsgOnCart
     * The variable handles the condition to show boss warning msg on cart page.
     * @param allowBossStoreSearch carries the condition when boss item on cart is
     * selected for changing or toggling the store.
     */
    const showBossMsgOnCart = allowBossStoreSearch && cartBopisStoresList.length;

    /**
     * @var showBossMsgOnProductPages
     * The variable handles the condition to show boss warning msgs on product
     * pages - PDP, PLP, SLP when store selection Modal is opened.
     * @param isBossCtaEnabled - is added to show boss warning msg only on boss items.
     *
     */
    const showBossMsgOnProductPages =
      cartBopisStoresList.length === minStoreCount &&
      isBossCtaEnabled &&
      cartBopisStoresList[0].pickupType.isStoreBossSelected;

    const showBossWarningMsg = showBossMsgOnCart || showBossMsgOnProductPages;

    if (showBossWarningMsg) {
      // condition checks if there is only single BOSS selected store item in the cart, added by user
      return ONE_STORE_SELECTED;
    }
    if (sameStore || storeLimitReached) {
      // condition checks if the two stores in the cart are same for BOSS and BOPIS
      return SAME_STORE_BOPIS_BOPIS;
    }
    if (this.showStoreSearching) {
      return SELECT_STORE;
    }
    return '';
  };

  deriveSKUInfo = ({ colorFitsSizesMap } = this.props, { initialValues } = this.props) => {
    const { color, Fit, Size } = initialValues;
    const variantId = getVariantId(colorFitsSizesMap, color, Fit, Size);
    const currentSizeEntry = getMapSliceForSize(colorFitsSizesMap, color, Fit, Size);
    const variantNo =
      currentSizeEntry && currentSizeEntry.variantNo ? currentSizeEntry.variantNo : null;
    const skuId = getSkuId(colorFitsSizesMap, color, Fit, Size);
    return {
      skuId,
      variantId,
      variantNo,
    };
  };

  // Return the gender based on breadcrumb
  getGender = (breadcrumbs) => {
    const gender =
      breadcrumbs && breadcrumbs[0] && breadcrumbs[0].displayName
        ? breadcrumbs[0].displayName.toLowerCase()
        : '';
    return gender === 'home' ? '' : gender;
  };

  trackAddToBagClick = (params) => {
    let isBossSelected;
    let isBopisSelected;
    let sku;
    if (params) {
      const { isBossSelected: Boss } = params;
      const { isBopisSelected: Bopis } = params;
      const { skuId } = params;
      isBossSelected = Boss;
      isBopisSelected = Bopis;
      sku = skuId;
    }
    const {
      initialValues,
      storeSearchCriteria,
      storeSearchDistance,
      currentProduct,
      setClickAnalyticsDataDispatch,
      trackClickDispatch,
      favStoreId,
      breadCrumbs,
    } = this.props;
    const { color, Size: size, Quantity: quantity } = initialValues;
    const { listPrice, offerPrice, reviewsCount, ratings, generalProductId = '' } = currentProduct;

    const trackProduct = {
      colorId: generalProductId,
      color,
      id: generalProductId.split('_')[0],
      price: listPrice,
      extPrice: listPrice,
      listPrice,
      pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
      rating: ratings,
      reviews: reviewsCount,
      size,
      quantity,
      sku,
      storeId: favStoreId,
      gender: this.getGender(breadCrumbs),
    };

    // Checking BOSS BOPIS for analytics
    let customEventsVal = '';
    let eventName = '';
    if (isBopisSelected) {
      customEventsVal = 'event132';
      eventName = 'bopis cta click on pickup modal';
    }
    if (isBossSelected) {
      customEventsVal = 'event133';
      eventName = 'boss cta click on pickup modal';
    }

    // setting values and dispatching Click tracker based on the requirement on BOSS/BOPIS add to bag call
    setClickAnalyticsDataDispatch({
      customEvents: [customEventsVal],
      storeSearchCriteria,
      storeSearchDistance,
      eventName,
      products: [trackProduct],
      favStoreId,
      clickEvent: true,
      pageNavigationText: '',
    });
    trackClickDispatch({
      name: 'pickup_store_select',
      module: 'browse',
    });

    const analyticsTimer = setTimeout(() => {
      setClickAnalyticsDataDispatch({
        customEvents: ['scAdd,scOpen,event85,event61'],
        storeSearchCriteria,
        storeSearchDistance,
        eventName: 'cart add',
        products: [trackProduct],
        storeId: favStoreId,
        clickEvent: true,
        pageNavigationText: '',
      });
      clearTimeout(analyticsTimer);
    }, 100);
  };

  /**
   * @method handleAddTobag
   * @description this method adds item to bag
   */
  handleAddTobag = (storeLocId, isBoss, addToBagClickTrackingInfo, fromPage) => {
    const {
      initialValues,
      onAddItemToCart,
      onCloseClick,
      currentProduct,
      fromPage: pageName,
    } = this.props;
    const primaryBrand = currentProduct && currentProduct.primaryBrand;
    this.setState({
      isShowMessage: true,
    });
    const brand = primaryBrand || getBrand();
    /**
     * added initial values to send in AddItem params.
     * Previously it was only handled post user searching
     * If searching is blocked then the intitial values received
     * will be passed as the expected params
     */
    const { color, Fit: fit, Size: size, Quantity: quantity } = initialValues;
    const formIntialValues = {
      // This is required as different teams have used different 'Fit' or 'fit' labels
      color,
      fit,
      size,
    };
    const productFormData = {
      ...formIntialValues,
      wishlistItemId: false,
      quantity,
      isBoss,
      brand,
      storeLocId,
    };
    const productInfo = getCartItemInfo(currentProduct, productFormData);
    const payload = {
      productInfo,
      callback: onCloseClick,
      fromPage: fromPage || pageName,
    };
    this.trackAddToBagClick(addToBagClickTrackingInfo);
    return onAddItemToCart(payload);
  };

  /**
   *
   * @method calculateTargetOrderType
   * @description this method returns targetOrderType
   * @memberof PickupStoreSelectionFormContainer
   */
  calculateTargetOrderType = (isBopisCtaEnabled, isBossCtaEnabled) => {
    if (isBopisCtaEnabled) {
      return ORDER_ITEM_TYPE.BOPIS;
    }
    if (isBossCtaEnabled) {
      return ORDER_ITEM_TYPE.BOSS;
    }
    return ORDER_ITEM_TYPE.ECOM;
  };

  /**
   *
   * @method handleUpdatePickUpItem
   * @description this method returns targetOrderType
   * @memberof PickupStoreSelectionFormContainer
   */
  handleUpdatePickUpItem = (selectedStoreId, isBossSelected) => {
    const {
      onUpdatePickUpItem,
      initialValues,
      initialValuesFromBagPage,
      currentProduct,
      onCloseClick,
      isBopisCtaEnabled,
      isBossCtaEnabled,
      isItemShipToHome,
      fromBagPage,
    } = this.props;
    this.setState({
      isShowMessage: true,
    });
    const { itemBrand } = initialValuesFromBagPage;
    const { color, Fit: fit, Size: size, Quantity: quantity } = initialValues;
    const formIntialValues = {
      // This is required as different teams have used different 'Fit' or 'fit' labels
      color,
      fit,
      size,
    };
    const productFormData = {
      ...formIntialValues,
      wishlistItemId: false,
      quantity,
      isBoss: isBossSelected,
      brand: itemBrand,
      storeLocId: selectedStoreId,
    };
    const productInfo = getCartItemInfo(currentProduct, productFormData);
    const { orderId, orderItemId, orderItemType } = initialValuesFromBagPage;
    const {
      skuInfo: { skuId, variantNo, variantId },
      quantity: newQuantity,
    } = productInfo;
    const targetOrderType = this.calculateTargetOrderType(isBopisCtaEnabled, isBossCtaEnabled);

    const payload = {
      apiPayload: {
        orderId: orderId.toString(),
        orderItem: [
          {
            orderItemId,
            xitem_catEntryId: skuId,
            quantity: newQuantity.toString(),
            variantNo,
            itemPartNumber: variantId,
          },
        ],
        x_storeLocId: selectedStoreId,
        // replaced "ECOM" and "BOPIS" with a config variable
        x_orderitemtype: isItemShipToHome ? ORDER_ITEM_TYPE.ECOM : orderItemType, // source type of Item
        x_updatedItemType: targetOrderType,
      },
      callback: onCloseClick,
      updateActionType: 'UpdatePickUpItem',
      isCartPage: fromBagPage,
    };
    onUpdatePickUpItem(payload);
  };

  /**
   * @method handlePickupRadioBtn
   * @description this method sets the pickup mode for store
   */
  handlePickupRadioBtn = (selectedStoreId, isBossSelected, store) => {
    this.setState({
      isBossSelected,
      selectedStoreId,
      isShowMessage: false,
    });
    const { isFromChangeStore, updateSelectedStoreInfo } = this.props;
    if (isFromChangeStore) {
      updateSelectedStoreInfo(selectedStoreId, store);
    }
  };

  untouch = () => {
    const { untouch } = this.props;
    untouch('color');
    untouch('size');
    untouch('fit');
    untouch('quantity');
    untouch('addressLocation');
    untouch('distance');
  };

  prePopulateZipCode = () => {
    /** This method validates if zipcode needs to be propulated or not.
     * !this.formData.addressLocation - this check is to ensure that
     * auto search will not overwrite manual search
     * !this.isAutoSearchTriggered - this check is to ensure that
     * autosearch should be trigerred only once in componentDidUpdate
     * Since default store is already set till user reaches this modal,
     * prevProps and newProps are always same; thus using this check;
     * this.props.allowBossStoreSearch - this check is to allow search
     *  from change store link on my bag
     * !this.props.isSearchOnlyInCartStores - this is to check if there is no restriction modal
     * isLoading - This check is for restricted modal scenario to avoid API call till
     * getUserBopisStore API returns data
     */
    const { defaultStore } = this.props;
    const { tcpZip } = this.state;

    const showStoreSearchForm = this.showStoreSearching;
    const isValidZipCode = this.getZipCode(defaultStore) || tcpZip;
    return (
      isValidZipCode &&
      !this.isAutoSearchTriggered &&
      showStoreSearchForm &&
      (!this.formData || !this.formData.addressLocation)
    );
  };

  deriveBossCtaEnabled = () => {
    const { isRadialInventoryEnabled, isBossCtaEnabled, colorFitsSizesMap, initialValues } =
      this.props;
    let isRadialBossEnabled;
    if (isRadialInventoryEnabled) {
      isRadialBossEnabled =
        !isBOSSProductOOSQtyMismatched(colorFitsSizesMap, initialValues) && isBossCtaEnabled;
    } else {
      isRadialBossEnabled = !isProductOOS(colorFitsSizesMap, initialValues) && isBossCtaEnabled;
    }
    return isRadialBossEnabled;
  };

  deriveStoreSearchAttributes = () => {
    const {
      cartBopisStoresList,
      maxAllowedStoresInCart,
      isSearchOnlyInCartStores,
      allowBossStoreSearch,
      isSkuResolved,
      isGetUserStoresLoaded,
    } = this.props;

    const storeLimitReached = cartBopisStoresList.length === maxAllowedStoresInCart;
    const sameStore =
      storeLimitReached &&
      cartBopisStoresList[0].basicInfo.id === cartBopisStoresList[1].basicInfo.id;
    const showStoreSearching =
      !isSkuResolved ||
      allowBossStoreSearch ||
      (isGetUserStoresLoaded && !isSearchOnlyInCartStores);
    this.showStoreSearching = showStoreSearching;
    return {
      storeLimitReached,
      sameStore,
      showStoreSearching,
    };
  };

  render() {
    const {
      isPickUpWarningModal,
      updateCartItemStore,
      defaultStore,
      isSkuResolved,
      isShoppingBag,
      addToCartError,
      isBopisCtaEnabled,
      isBossEnabled,
      isBopisEnabled,
      isGiftCard,
      distancesMap,
      className,
      storeSearchError,
      PickupSkuFormValues,
      colorFitsSizesMap,
      isSearchOnlyInCartStores,
      onCloseClick,
      allowBossStoreSearch,
      bopisChangeStore,
      cartBopisStoresList,
      isGetUserStoresLoaded,
      error,
      currentProduct,
      pageNameProp,
      openRestrictedModalForBopis,
      storeSearchCriteria,
      storeSearchDistance,
      setClickAnalyticsDataDispatch,
      trackClickDispatch,
      setFavoriteStore,
      getDefaultStore,
      pageData,
      getSKUValidated,
      toastMessage,
      scrollRef,
      pickupStoreListRef,
      isStoreSearching,
      fromPage,
      newQVEnabled,
      isFromChangeStore,
      isBopisPickup,
      quickViewLabels,
      fromBagPage,
      isAnySkuSelected,
      setSkuEditFlag,
    } = this.props;
    const { selectedStoreId, isBossSelected, isShowMessage, selectedValue } = this.state;
    const sizeErrorMessage = SKU_DETAILS.errorMessage;
    return (
      <PickupStoreSelectionForm
        setFavoriteStore={setFavoriteStore}
        getDefaultStore={getDefaultStore}
        onSearch={this.onSearch}
        pageNameProp={pageNameProp}
        isPickUpWarningModal={isPickUpWarningModal}
        currentProduct={currentProduct}
        renderVariationText={this.renderVariationText}
        getPreferredStoreData={this.getPreferredStoreData}
        isGetUserStoresLoaded={isGetUserStoresLoaded}
        deriveStoreSearchAttributes={this.deriveStoreSearchAttributes}
        deriveBossCtaEnabled={this.deriveBossCtaEnabled}
        updateCartItemStore={updateCartItemStore}
        prePopulateZipCodeAndSearch={this.prePopulateZipCodeAndSearch}
        defaultStore={defaultStore}
        isSkuResolved={isSkuResolved}
        isShoppingBag={isShoppingBag}
        addToCartError={addToCartError}
        isBopisCtaEnabled={isBopisCtaEnabled}
        isBossEnabled={isBossEnabled}
        isBopisEnabled={isBopisEnabled}
        isGiftCard={isGiftCard}
        preferredStore={defaultStore}
        handleAddTobag={this.handleAddTobag}
        handlePickupRadioBtn={this.handlePickupRadioBtn}
        handleUpdatePickUpItem={this.handleUpdatePickUpItem}
        selectedStoreId={selectedStoreId}
        isBossSelected={isBossSelected}
        isShowMessage={isShowMessage}
        distancesMap={distancesMap}
        className={className}
        storeSearchError={storeSearchError}
        PickupSkuFormValues={PickupSkuFormValues}
        colorFitsSizesMap={colorFitsSizesMap}
        isSearchOnlyInCartStores={isSearchOnlyInCartStores}
        onCloseClick={onCloseClick}
        allowBossStoreSearch={allowBossStoreSearch}
        bopisChangeStore={bopisChangeStore}
        cartBopisStoresList={cartBopisStoresList}
        error={error}
        getIsBopisAvailable={this.getIsBopisAvailable}
        selectedValue={selectedValue}
        onQuantityChange={this.quantityChange}
        initialValues={this.initialValues}
        openRestrictedModalForBopis={openRestrictedModalForBopis}
        storeSearchCriteria={storeSearchCriteria}
        storeSearchDistance={storeSearchDistance}
        setClickAnalyticsData={setClickAnalyticsDataDispatch}
        trackClick={trackClickDispatch}
        pageData={pageData}
        getSKUValidated={getSKUValidated}
        toastMessage={toastMessage}
        sizeErrorMessage={sizeErrorMessage}
        scrollRef={scrollRef}
        pickupStoreListRef={pickupStoreListRef}
        isStoreSearching={isStoreSearching}
        fromPage={fromPage}
        newQVEnabled={newQVEnabled}
        isFromChangeStore={isFromChangeStore}
        isBopisPickup={isBopisPickup}
        quickViewLabels={quickViewLabels}
        fromBagPage={fromBagPage}
        isAnySkuSelected={isAnySkuSelected}
        setSkuEditFlag={setSkuEditFlag}
      />
    );
  }
}

const mapStateToProps = (state) => {
  const { pageData } = state;
  return {
    pageNameProp: getPageName(state),
    pageData,
    storeSearchCriteria: getStoreSearchCriteria(state),
    storeSearchDistance: getStoreSearchDistance(state),
    favStoreId: getUserStoreId(state),
    breadCrumbs: getBreadCrumbs(state),
    isStoreSearching: PickupSelectors.isStoreSearching(state),
    mapboxStoreSearchTypesParam: getStoreSearchTypesParam(state),
    mapboxSwitch: getMapboxSwitch(state),
    isBopisPickup: getIsBopisOptionSelected(state),
    quickViewLabels: getQuickViewLabels(state),
    isAnySkuSelected: PickupSelectors.getIsSkuEdit(state),
  };
};

const mapDispatchToProps = (dispatch) => ({
  setClickAnalyticsDataDispatch: (payload) => dispatch(setClickAnalyticsData(payload)),
  trackClickDispatch: (payload) => dispatch(trackClick(payload)),
  distanceChange: (payload) => {
    dispatch(change(payload.form, 'distance', payload.selectedQuantity));
  },
  setSkuEditFlag: (payload) => dispatch(setIsSkuEditFlag(payload)),
  showLoaderDispatch: (payload) => dispatch(setLoaderState(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PickupStoreSelectionFormContainer);
