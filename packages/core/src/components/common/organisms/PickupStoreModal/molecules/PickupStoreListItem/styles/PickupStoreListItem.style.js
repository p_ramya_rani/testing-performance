// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  ${(props) =>
    props.selectedStoreId === props.store.basicInfo.id
      ? `border: 2px solid ${props.theme.colorPalette.gray['800']};`
      : `border: 1px solid ${props.theme.colorPalette.gray['800']};`};

  padding: 0 15px 12px 10px;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  ${(props) => props.newQVEnabled && 'border-radius:6px;padding: 0px 14px 16px 14px;'}
  @media ${(props) => props.theme.mediaQuery.medium} {
    padding: 0px 30px 16px 30px;
    padding-left: ${(props) => (props.newQVEnabled ? '14px' : '')};
    ${(props) => props.newQVEnabled && 'padding: 0px 14px 16px 14px;'}
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    ${(props) => props.newQVEnabled && 'padding: 0px 14px 5px 14px;'}
  }
  .storeListItemWrapper {
    min-height: 138px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      min-height: 153px;
    }
    display: flex;
  }
  .newstoreListItemWrapper {
    min-height: 138px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      min-height: 153px;
    }
    display: flex;
    flex-direction: column;
  }
  .newstoreInfoWrapper {
    display: flex;
    flex-direction: column;
    width: 100%;
    border-radius: 6px;
    background-color: #f3f3f3;
    margin-top: 10px;
  }
  .storeInfoWrapper {
    display: flex;
    flex-direction: column;
    width: 44%;
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 40%;
    }
  }
  .pickupButtonsWrapper {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    justify-content: ${(props) => (props.newQVEnabled ? 'space-around' : 'flex-start')};
  }
  .newpickupButtonsWrapper {
    display: flex;
    flex-direction: row;
    flex-grow: 1;
    justify-content: space-between;
  }
  .no-rush-pickup {
    text-transform: capitalize;
  }
  .bossdetail {
    display: flex;
    flex-direction: column;
    padding: 10px 10px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: 18px 0px 0px 10px;
    }
  }
  .bopisdetail {
    display: flex;
    flex-direction: column;
    padding: 10px 10px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: 25px 0px 0px 10px;
    }
  }
  .bossdate {
    display: flex;
    flex-direction: row;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex-direction: column;
      .banner-wrapper {
        padding-left: 0px;
      }
    }
    .triangle-top {
      display: none;
    }
    .promo-wrapper {
      padding-left: 6px;
      border-radius: 6px;
      padding-bottom: 2px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        width: 55%;
      }
    }
    .richtextCss span {
      font-size: 10px;
      font-weight: 600;
    }
  }

  .detailcolor {
    color: ${(props) => props.theme.colors.NOTIFICATION.SUCCESS};
  }
  .pickupCTAWrapper {
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-left: 0px;
    }
  }
  .newpickupCTAWrapper {
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-left: 0px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: 0px;
      padding: 10px 0px;
    }
  }
  .addToCartError {
    padding-left: ${(props) => (props.newQVEnabled ? '0px' : props.theme.spacing.ELEM_SPACING.XXL)};
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => props.newQVEnabled && 'width:220px'}
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => props.newQVEnabled && 'width:110px'}
    }
  }
  .PickupRadioBtn {
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      .promotional-banner .banner-wrapper {
        margin-top: 5px;
        padding-left: 0;
      }

      .boss-extra-details {
        white-space: nowrap;
      }
    }

    @media ${(props) => props.theme.mediaQuery.medium} {
      .promotional-banner {
        display: flex;
      }
    }
  }
  .storeInfoWrapper div:nth-child(2) {
    justify-content: normal;
    margin-top: 0px;
  }
  .storeAddressWrapper {
    margin-top: ${(props) => (props.newQVEnabled ? '21px' : props.theme.spacing.ELEM_SPACING.SM)};
    display: flex;
    flex-grow: 1;
    flex-direction: column;
    justify-content: normal;
  }
  .newstoreAddressWrapper {
    padding: 0px 14px;
    margin-top: 10px;
    display: flex;
    flex-grow: 1;
    flex-direction: column;
    justify-content: normal;
  }
  .newstoredistancewrapper {
    display: flex;
    justify-content: space-between;
  }
  .newstoreDistance {
    display: flex;
    align-items: flex-end;
    padding-bottom: 8px;
  }
  .pickupBtnDivider {
    border-bottom: 1px solid ${(props) => props.theme.colorPalette.gray['800']};
  }
  .favStore {
    display: flex;
  }
  .newfavStore {
    position: absolute;
    right: 5px;
    margin-top: -4px;
  }
  .storeUnavailable {
    width: 170px;
    margin-top: ${(props) => (props.newQVEnabled ? '0px' : '40px')};
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: ${(props) => (props.newQVEnabled ? '100%' : '290px')};
      margin-top: ${(props) => (props.newQVEnabled ? '0px' : '60px')};
      ${(props) => props.newQVEnabled && 'padding: 2px 5px;'}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      width: ${(props) => (props.newQVEnabled ? '100%' : '350px')};
      margin-top: ${(props) => (props.newQVEnabled ? '0px' : '60px')};
    }
    ${(props) => props.newQVEnabled && 'width:100%; padding: 2px 5px;'}
  }
  .marker-icon {
    width: 17px;
    height: 17px;
  }
  .StoreDetailsAnchor {
    text-decoration: underline;
    cursor: pointer;
  }
  .tooltip-bubble {
    left: 0%;
    margin-left: 5%;
    transform: translateX(0);
    bottom: 60%;
    ::after {
      right: 75%;
    }
    ::before {
      right: 71%;
      bottom: -1px;
    }
  }

  .button-resize {
    min-height: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    height: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    padding: 0px;
    @media ${(props) => props.theme.mediaQuery.large} {
      min-height: 42px;
      height: 42px;
      padding: inherit;
    }
  }
  .button-resize.newpickupbtn {
    padding: 10px;
    background: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
    border-radius: 6px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding: 6px 10px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: 6px 10px;
      width: 150px;
      height: 42px;
    }
    .pick-btn {
      color: ${(props) => props.theme.colors.BUTTON.BLUE.TEXT};
    }
  }
`;
