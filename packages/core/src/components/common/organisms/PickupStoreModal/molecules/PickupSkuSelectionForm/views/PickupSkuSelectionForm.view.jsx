// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module PickupSkuSelectionForm
 * @desc Component to display step 1 of PickUp in Store Modal and fill sku details
 * doesn't open when sku is resolved
 */

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { Row, BodyCopy, Anchor, DamImage } from '@tcp/core/src/components/common/atoms';
import { PRODUCT_SKU_SELECTION_FORM } from '@tcp/core/src/constants/reducer.constants';
import { getSiteId, getAPIConfig, getBrand } from '../../../../../../../utils';
import withStyles from '../../../../../hoc/withStyles';
import styles, {
  customPriceStyles,
} from '../../../../QuickViewModal/molecules/ProductCustomizeFormPart/styles/ProductCustomizeFormPart.style';
import ProductPrice from '../../../../../../features/browse/ProductDetail/molecules/ProductPrice/ProductPrice';
import ProductAddToBagContainer from '../../../../../molecules/ProductAddToBag';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import { SKU_DETAILS, PICKUP_LABELS } from '../../../PickUpStoreModal.constants';
import { getProductListToPath } from '../../../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';

const getCurrentColorPdpUrl = (currentProduct, currentColorEntry) => {
  const { seoToken, pdpUrl } = currentProduct || {};
  const { pdpSeoUrl, pdpUrl: currentPdpUrl } = currentColorEntry || {};
  const currentColorPdpUrl = pdpSeoUrl || seoToken;
  return currentColorPdpUrl ? `/p/${currentColorPdpUrl}` : pdpUrl || currentPdpUrl;
};

const PickupSkuSelectionForm = (props) => {
  const {
    currentProduct,
    initialValues,
    currency,
    prices,
    isCanada,
    isInternationalShipping,
    isHasPlcc,
    className,
    onChangeColor,
    onChangeSize,
    currentColorEntry,
    imageUrl,
    generalProductId,
    navigateToPDP,
    currencyAttributes,
    isSKUValidated,
    getSKUValidated,
    onFitChange,
    pdpLabels,
    onQuickViewOpenClick,
    closePickupModal,
    setInitialTCPStyleQty,
    singlePageLoad,
    initialMultipackMapping,
    multipackSelectionAction,
    getQickViewSingleLoad,
    getDisableSelectedTab,
    selectedMultipack,
    disableMultiPackTab,
    TCPStyleQTY,
    onPickUpOpenClick,
    hideMultipackPills,
    isModalOpen,
    availableTCPmapNewStyleId,
    isAfterPayEnabled,
    newQVEnabled,
    primaryBrand,
    alternateBrand,
  } = props;
  const productPriceProps = {
    currencySymbol: currency,
    currencyAttributes,
    isItemPartNumberVisible: false,
    ...prices,
    isCanada,
    inheritedStyles: customPriceStyles,
    customFonts: { listPriceFont: 'fs14' },
    isPlcc: isHasPlcc,
    isInternationalShipping,
  };

  const currentColorPdpUrl = getCurrentColorPdpUrl(currentProduct, currentColorEntry);

  const apiConfigObj = getAPIConfig();
  const { crossDomain } = apiConfigObj;
  const currentSiteBrand = getBrand();
  let isProductBrandOfSameDomain = true;
  if (initialValues && initialValues.itemBrand) {
    isProductBrandOfSameDomain =
      currentSiteBrand.toUpperCase() === initialValues.itemBrand.toUpperCase();
  }
  const toPath = isProductBrandOfSameDomain
    ? `/${getSiteId()}${currentColorPdpUrl}`
    : `${crossDomain}/${getSiteId()}${currentColorPdpUrl}`;
  const pdpToPath = isProductBrandOfSameDomain
    ? getProductListToPath(currentColorPdpUrl)
    : `${crossDomain}/${getSiteId()}${currentColorPdpUrl}`;
  const itemBrand = !isEmpty(initialValues) ? initialValues.itemBrand : null;
  const getProductDetailContainer = () => {
    return (
      <Fragment>
        <ProductPrice isAfterPayDisplay={isAfterPayEnabled} {...productPriceProps} />

        <Anchor
          className={`link-redirect ${isAfterPayEnabled ? 'product-cta-stick' : ''}`}
          to={toPath}
          noLink
          onClick={(e) =>
            navigateToPDP(e, pdpToPath, isProductBrandOfSameDomain ? currentColorPdpUrl : pdpToPath)
          }
        >
          <BodyCopy className="product-link" fontSize="fs14" fontFamily="secondary">
            {PICKUP_LABELS.VIEW_DETAILS}
          </BodyCopy>
        </Anchor>
      </Fragment>
    );
  };

  const imgData = {
    alt: 'Error',
    url: imageUrl,
  };

  return (
    <Row className={className}>
      <div className="product-customize-form-container">
        <div className="image-title-wrapper">
          <div className="image-wrapper">
            <DamImage
              imgData={imgData}
              isProductImage
              itemBrand={itemBrand}
              primaryBrand={primaryBrand || alternateBrand}
            />
          </div>
          <div className="product-details-card-container-separate">
            <BodyCopy
              fontSize="fs18"
              fontWeight="extrabold"
              fontFamily="secondary"
              className="product-name"
            >
              {currentProduct.name}
            </BodyCopy>
            {getProductDetailContainer()}
          </div>
        </div>
        <div className="product-detail">
          <div className="product-details-card-container">
            <BodyCopy
              fontSize="fs18"
              fontWeight="extrabold"
              fontFamily="secondary"
              className="product-name"
            >
              {currentProduct.name}
            </BodyCopy>
            <div className="product-view-details">{getProductDetailContainer()}</div>
          </div>
          {!newQVEnabled && (
            <ProductAddToBagContainer
              onChangeColor={onChangeColor}
              onChangeSize={onChangeSize}
              isModalOpen={isModalOpen}
              plpLabels={SKU_DETAILS}
              currentProduct={currentProduct}
              customFormName={PRODUCT_SKU_SELECTION_FORM}
              selectedColorProductId={generalProductId}
              initialFormValues={initialValues}
              showAddToBagCTA={false}
              renderReceiveProps
              isDisableZeroInventoryEntries={false}
              isPickup
              isSKUValidated={isSKUValidated}
              getSKUValidated={getSKUValidated}
              itemBrand={itemBrand}
              onFitChange={onFitChange}
              pdpLabels={pdpLabels}
              onQuickViewOpenClick={onQuickViewOpenClick}
              closePickupModal={closePickupModal}
              setInitialTCPStyleQty={setInitialTCPStyleQty}
              singlePageLoad={singlePageLoad}
              initialMultipackMapping={initialMultipackMapping}
              multipackSelectionAction={multipackSelectionAction}
              getQickViewSingleLoad={getQickViewSingleLoad}
              getDisableSelectedTab={getDisableSelectedTab}
              selectedMultipack={selectedMultipack}
              disableMultiPackTab={disableMultiPackTab}
              TCPStyleQTY={TCPStyleQTY}
              onPickUpOpenClick={onPickUpOpenClick}
              hideMultipackPills={hideMultipackPills}
              availableTCPmapNewStyleId={availableTCPmapNewStyleId}
              alternateBrand={alternateBrand}
            />
          )}
        </div>
      </div>
      {newQVEnabled && (
        <ProductAddToBagContainer
          onChangeColor={onChangeColor}
          onChangeSize={onChangeSize}
          isModalOpen={isModalOpen}
          plpLabels={SKU_DETAILS}
          currentProduct={currentProduct}
          customFormName={PRODUCT_SKU_SELECTION_FORM}
          selectedColorProductId={generalProductId}
          initialFormValues={initialValues}
          showAddToBagCTA={false}
          renderReceiveProps
          isDisableZeroInventoryEntries={false}
          isPickup
          isSKUValidated={isSKUValidated}
          getSKUValidated={getSKUValidated}
          itemBrand={itemBrand}
          onFitChange={onFitChange}
          pdpLabels={pdpLabels}
          onQuickViewOpenClick={onQuickViewOpenClick}
          closePickupModal={closePickupModal}
          setInitialTCPStyleQty={setInitialTCPStyleQty}
          singlePageLoad={singlePageLoad}
          initialMultipackMapping={initialMultipackMapping}
          multipackSelectionAction={multipackSelectionAction}
          getQickViewSingleLoad={getQickViewSingleLoad}
          getDisableSelectedTab={getDisableSelectedTab}
          selectedMultipack={selectedMultipack}
          disableMultiPackTab={disableMultiPackTab}
          TCPStyleQTY={TCPStyleQTY}
          onPickUpOpenClick={onPickUpOpenClick}
          hideMultipackPills={hideMultipackPills}
          availableTCPmapNewStyleId={availableTCPmapNewStyleId}
          isNewQVEnabled={newQVEnabled}
          alternateBrand={alternateBrand}
        />
      )}
    </Row>
  );
};

PickupSkuSelectionForm.propTypes = {
  /** labels for selection fields */
  colorFitSizeDisplayNames: PropTypes.shape({
    /** label for color selection field */
    color: PropTypes.string,
    /** label for fit selection field */
    fit: PropTypes.string,
    /** label for size selection field */
    size: PropTypes.string,
  }),

  /** seed values for the form */
  initialValues: PropTypes.shape({
    /** user's preselected color id from parent instance */
    color: PropTypes.string,
    /** user's preselected fit id from parent instance */
    fit: PropTypes.string,
    /** user's preselected size id from parent instance */
    size: PropTypes.string,
    /** user's preselected quantity from parent instance */
    quantity: PropTypes.number,
  }).isRequired,

  isCanada: PropTypes.bool.isRequired,
  /* We are available to know if is an international shipping */
  isInternationalShipping: PropTypes.bool.isRequired,
  currentProduct: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,

  currency: PropTypes.string,
  currencyAttributes: PropTypes.shape({}).isRequired,

  prices: PropTypes.shape({
    listPrice: PropTypes.number.isRequired,
    offerPrice: PropTypes.number,
  }).isRequired,

  isHasPlcc: PropTypes.bool,

  className: PropTypes.string,

  generalProductId: PropTypes.string.isRequired,

  onChangeColor: PropTypes.func,
  onChangeSize: PropTypes.func,
  currentColorEntry: PropTypes.shape({}),
  imageUrl: PropTypes.string.isRequired,
  navigateToPDP: PropTypes.func.isRequired,
  isSKUValidated: PropTypes.bool,
  getSKUValidated: PropTypes.func,
  onFitChange: PropTypes.func,
  pdpLabels: PropTypes.shape({}).isRequired,
  onQuickViewOpenClick: PropTypes.func.isRequired,
  closePickupModal: PropTypes.func.isRequired,
  setInitialTCPStyleQty: PropTypes.bool,
  singlePageLoad: PropTypes.bool,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  multipackSelectionAction: PropTypes.func,
  getQickViewSingleLoad: PropTypes.func,
  getDisableSelectedTab: PropTypes.func,
  selectedMultipack: PropTypes.string,
  disableMultiPackTab: PropTypes.bool,
  TCPStyleQTY: PropTypes.string,
  onPickUpOpenClick: PropTypes.func,
  hideMultipackPills: PropTypes.bool,
  isModalOpen: PropTypes.bool,
  availableTCPmapNewStyleId: PropTypes.shape([]),
  isAfterPayEnabled: PropTypes.bool,
  newQVEnabled: PropTypes.bool,
};

PickupSkuSelectionForm.defaultProps = {
  colorFitSizeDisplayNames: null,
  currency: 'USD',
  isHasPlcc: false,
  className: '',
  onChangeColor: () => {},
  onChangeSize: () => {},
  currentColorEntry: {},
  isSKUValidated: false,
  getSKUValidated: () => {},
  onFitChange: () => {},
  setInitialTCPStyleQty: false,
  singlePageLoad: false,
  multipackSelectionAction: () => {},
  getQickViewSingleLoad: () => {},
  getDisableSelectedTab: () => {},
  selectedMultipack: '1',
  disableMultiPackTab: false,
  TCPStyleQTY: '',
  onPickUpOpenClick: () => {},
  hideMultipackPills: false,
  isModalOpen: true,
  availableTCPmapNewStyleId: [],
  isAfterPayEnabled: false,
  newQVEnabled: false,
};

export default withStyles(PickupSkuSelectionForm, styles);
