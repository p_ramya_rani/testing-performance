/* eslint-disable max-lines */
/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import DropDown from '@tcp/core/src/components/common/atoms/DropDown/views/DropDown.native';
import TextBox from '@tcp/core/src/components/common/atoms/TextBox';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { Anchor, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import { isAndroid } from '../../../../../../../utils/index.native';
import { Button } from '../../../../../atoms';
import withStyles from '../../../../../hoc/withStyles';
import PickupStoreListContainer from '../../PickupStoreList';
import PickupStoreListItem from '../../PickupStoreListItem';
import styles, {
  PickUpHeaderText,
  PickUpModalView,
  Row,
  AddressCol,
  DistanceCol,
  dropDownStyle,
  itemStyle,
  PickupStoreContainer,
  StyledStoreLocator,
  StyledCurrentLocation,
  AddressColIsfromChangeStore,
  DistanceColIsFromChangeStaore,
  PickUpHeaderTextRegularCase,
  ViewWrapper,
  TextboxStyleFromStore,
  ButtonWrapper,
  dropDownStyleWithoutLine,
  ZIPContainerStyle,
} from '../styles/PickupStoreSelectionForm.style.native';
import { ANALYTICS_LABELS, PICKUP_LABELS } from '../../../PickUpStoreModal.constants';

const MarkerIcon = require('../../../../../../../../../mobileapp/src/assets/images/store_locator.png');

class PickupStoreSelectionForm extends React.PureComponent {
  componentDidMount() {
    const {
      onSearch,
      openRestrictedModalForBopis,
      isSkuResolved,
      isFromChangeStore,
      defaultStore,
      handlePickupRadioBtn,
    } = this.props;

    if (!isFromChangeStore && (openRestrictedModalForBopis || isSkuResolved)) {
      onSearch();
    }
    if (isFromChangeStore) {
      onSearch({ isDefaultCase: true, isFromCurrentLocation: false });
    }
    if (defaultStore && defaultStore?.basicInfo) {
      handlePickupRadioBtn(defaultStore?.basicInfo?.id, false, defaultStore);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      prePopulateZipCodeAndSearch,
      handleSubmit,
      change,
      PickupSkuFormValues,
      isFromChangeStore,
      onSearch,
      isAnySkuSelected,
      setSkuEditFlag,
    } = this.props;
    if (
      isFromChangeStore &&
      PickupSkuFormValues &&
      PickupSkuFormValues.Size &&
      isAnySkuSelected &&
      prevProps.PickupSkuFormValues.Size !== PickupSkuFormValues.Size
    ) {
      onSearch({ isDefaultCase: true, isFromCurrentLocation: false });
      setSkuEditFlag({ isSkuSelected: false });
    }
    if (!isFromChangeStore) {
      prePopulateZipCodeAndSearch(handleSubmit, change);
    }
  }

  handleClick(skuResolved, formValues, isFromSearchCta) {
    const { onSearch, toastMessage, sizeErrorMessage, scrollRef } = this.props;
    if (!skuResolved) {
      onSearch(formValues, isFromSearchCta);
      scrollRef();
    } else {
      toastMessage(sizeErrorMessage);
    }
  }

  handleDistanceClick(itemValue) {
    const { onQuantityChange, form } = this.props;
    if (onQuantityChange) {
      onQuantityChange(itemValue, form);
    }
  }

  triggerAnalyticsEvents(customEvents, name) {
    const { setClickAnalyticsData, trackClick } = this.props;
    setClickAnalyticsData({
      customEvents: [customEvents],
      name,
      clickEvent: true,
    });
    trackClick({ name: 'BOPIS_MODAL' });
  }

  displayStoreListItems({ isBossCtaEnabled, buttonLabel, sameStore, isFromChangeStore }) {
    const {
      isShoppingBag,
      isSearchOnlyInCartStores,
      onCloseClick,
      addToCartError,
      isBopisCtaEnabled,
      updateCartItemStore,
      isBossEnabled,
      allowBossStoreSearch,
      defaultStore,
      bopisChangeStore,
      isBopisEnabled,
      isGiftCard,
      cartBopisStoresList,
      handleAddTobag,
      handlePickupRadioBtn,
      handleUpdatePickUpItem,
      selectedStoreId,
      isBossSelected,
      isShowMessage,
      setFavoriteStore,
      getDefaultStore,
      pickupStoreListRef,
      fromPage,
      PickupSkuFormValues,
    } = this.props;

    return (
      <PickupStoreContainer
        ref={(view) => {
          if (pickupStoreListRef && view) {
            pickupStoreListRef(view);
          }
        }}
        collapsable={false}
      >
        <PickupStoreListContainer
          isShoppingBag={isShoppingBag}
          onStoreSelect={handleAddTobag}
          onStoreUpdate={handleUpdatePickUpItem}
          onPickupRadioBtnToggle={handlePickupRadioBtn}
          isResultOfSearchingInCartStores={isSearchOnlyInCartStores}
          onCancel={onCloseClick}
          sameStore={sameStore}
          selectedStoreId={selectedStoreId}
          isBossSelected={isBossSelected}
          addToCartError={isShowMessage ? addToCartError : ''}
          isBopisCtaEnabled={isBopisCtaEnabled}
          isBossCtaEnabled={isBossCtaEnabled}
          isBossEnabled={isBossEnabled}
          isBopisEnabled={isBopisEnabled}
          allowBossStoreSearch={allowBossStoreSearch}
          bopisChangeStore={bopisChangeStore}
          updateCartItemStore={updateCartItemStore}
          buttonLabel={buttonLabel}
          isGiftCard={isGiftCard}
          defaultStore={defaultStore}
          cartBopisStoresList={cartBopisStoresList}
          setFavoriteStore={setFavoriteStore}
          getDefaultStore={getDefaultStore}
          fromPage={fromPage}
          isFromChangeStore={isFromChangeStore}
          isSizeSelected={PickupSkuFormValues?.Size}
        />
      </PickupStoreContainer>
    );
  }

  displayErrorCopy() {
    const { error, onCloseClick } = this.props;
    return error ? (
      <>
        <BodyCopyWithSpacing fontSize="fs14" fontWeight="semibold" color="red.500" text={error} />
        <Button onPress={onCloseClick} type="button" className="button-cancel">
          Cancel
        </Button>
      </>
    ) : null;
  }

  displayStoreSearchForm(showStoreSearching, isFromChangeStore) {
    const {
      pristine,
      submitting,
      storeSearchError,
      isSkuResolved,
      handleSubmit,
      PickupSkuFormValues,
    } = this.props;
    const skuResolved = Object.values(PickupSkuFormValues).includes('');
    const findStoreHeading = isFromChangeStore
      ? PICKUP_LABELS.FIND_STORE_FROM_CHANGE_STORE
      : PICKUP_LABELS.FIND_STORE;

    return showStoreSearching ? (
      <PickUpModalView>
        {!isFromChangeStore && <PickUpHeaderText>{findStoreHeading}</PickUpHeaderText>}
        {isFromChangeStore && (
          <PickUpHeaderTextRegularCase>{findStoreHeading}</PickUpHeaderTextRegularCase>
        )}
        {isFromChangeStore && (
          <>
            {this.renderCurrentlocationView()}
            {this.renderFromStoreRowView(skuResolved)}
          </>
        )}
        {!isFromChangeStore && this.renderRowView()}
        {!isFromChangeStore && (
          <Button
            margin={isAndroid() ? '16px 0 35px 0' : '16px 0 25px 0'}
            color="white"
            fill="BLUE"
            text="Search"
            fontSize="fs10"
            fontWeight="extrabold"
            fontFamily="secondary"
            onPress={handleSubmit((formValues) => {
              this.handleClick(skuResolved, formValues);
            })}
            locator="pdp_color_swatch"
            accessibilityLabel="Search"
            disabled={pristine || submitting}
          />
        )}
        {isSkuResolved && storeSearchError ? (
          <BodyCopyWithSpacing
            mobileFontFamily="secondary"
            fontSize="fs14"
            fontWeight="extrabold"
            textAlign="center"
            text={storeSearchError}
          />
        ) : null}
      </PickUpModalView>
    ) : null;
  }

  displayFavStore({
    storeLimitReached,
    prefStoreWithData,
    sameStore,
    buttonLabel,
    isBossCtaEnabled,
  }) {
    const {
      isShoppingBag,
      addToCartError,
      isBopisCtaEnabled,
      updateCartItemStore,
      isBossEnabled,
      isBopisEnabled,
      isGiftCard,
      preferredStore,
      handleAddTobag,
      handlePickupRadioBtn,
      selectedStoreId,
      isBossSelected,
      isShowMessage,
      getIsBopisAvailable,
      isGetUserStoresLoaded,
      handleUpdatePickUpItem,
      setFavoriteStore,
      getDefaultStore,
      setClickAnalyticsData,
      trackClick,
      pageData,
      fromPage,
    } = this.props;
    return (
      !storeLimitReached &&
      isGetUserStoresLoaded &&
      preferredStore &&
      prefStoreWithData && (
        <PickupStoreListItem
          sameStore={sameStore}
          isShoppingBag={isShoppingBag}
          store={preferredStore}
          onStoreSelect={handleAddTobag}
          onStoreUpdate={handleUpdatePickUpItem}
          onPickupRadioBtnToggle={handlePickupRadioBtn}
          isBopisSelected={preferredStore.basicInfo.id === selectedStoreId && !isBossSelected}
          isBossSelected={preferredStore.basicInfo.id === selectedStoreId && isBossSelected}
          selectedStoreId={selectedStoreId}
          isBopisAvailable={getIsBopisAvailable()}
          isBossAvailable={preferredStore.storeBossInfo.isBossEligible}
          storeBossInfo={preferredStore.storeBossInfo}
          addToCartError={isShowMessage ? addToCartError : ''}
          isBopisCtaEnabled={isBopisCtaEnabled && isBopisEnabled}
          isBossCtaEnabled={isBossCtaEnabled && isBossEnabled}
          updateCartItemStore={updateCartItemStore}
          buttonLabel={buttonLabel}
          isGiftCard={isGiftCard}
          setFavoriteStore={setFavoriteStore}
          getDefaultStore={getDefaultStore}
          setClickAnalyticsData={setClickAnalyticsData}
          trackClick={trackClick}
          pageData={pageData}
          fromPage={fromPage}
        />
      )
    );
  }

  displayStoreSearchComp() {
    const {
      getPreferredStoreData,
      deriveStoreSearchAttributes,
      deriveBossCtaEnabled,
      updateCartItemStore,
      defaultStore,
      isSkuResolved,
      renderVariationText,
      isFromChangeStore,
    } = this.props;
    const prefStoreWithData = getPreferredStoreData(defaultStore);
    const { storeLimitReached, sameStore, showStoreSearching } = deriveStoreSearchAttributes();
    const isBossCtaEnabled = deriveBossCtaEnabled();
    const buttonLabel = updateCartItemStore ? PICKUP_LABELS.UPDATE : PICKUP_LABELS.ADD_TO_BAG;
    if (isFromChangeStore) {
      return (
        <React.Fragment>
          {this.displayStoreSearchForm(showStoreSearching, isFromChangeStore)}
          <React.Fragment>
            {isSkuResolved &&
              this.displayStoreListItems({
                isBossCtaEnabled,
                buttonLabel,
                sameStore,
                isFromChangeStore,
              })}
            {isSkuResolved && this.displayErrorCopy()}
          </React.Fragment>
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        {isSkuResolved && (
          <BodyCopyWithSpacing
            margin="20px 14px"
            mobileFontFamily="secondary"
            fontSize="fs14"
            text={renderVariationText(storeLimitReached, sameStore)}
          />
        )}
        {isSkuResolved &&
          this.displayFavStore({
            storeLimitReached,
            prefStoreWithData,
            sameStore,
            buttonLabel,
            isBossCtaEnabled,
          })}
        {this.displayStoreSearchForm(showStoreSearching)}
        <React.Fragment>
          {isSkuResolved &&
            this.displayStoreListItems({
              isBossCtaEnabled,
              buttonLabel,
              sameStore,
            })}
          {isSkuResolved && this.displayErrorCopy()}
        </React.Fragment>
      </React.Fragment>
    );
  }

  renderFromStoreRowView(skuResolved) {
    const { distancesMap, handleSubmit, selectedValue, pristine, submitting } = this.props;
    return (
      <Row>
        <AddressColIsfromChangeStore>
          <Field
            name="addressLocation"
            id="addressLocation"
            component={TextBox}
            label={PICKUP_LABELS.ENTER_ZIP_CODE}
            type="text"
            showAnimatedBorder
            customStyle={TextboxStyleFromStore}
            style={ZIPContainerStyle}
          />
        </AddressColIsfromChangeStore>
        <DistanceColIsFromChangeStaore>
          <Field
            bounces={false}
            name="distance"
            component={DropDown}
            heading="Distance"
            data={distancesMap}
            label="Distance"
            className="distance-input"
            dropDownStyle={{ ...dropDownStyleWithoutLine }}
            itemStyle={{ ...itemStyle }}
            variation="secondary"
            showAnimatedBorder={true}
            selectedValue={selectedValue && selectedValue.toString()}
            onValueChange={(itemValue) => {
              this.handleDistanceClick(itemValue);
            }}
          />
        </DistanceColIsFromChangeStaore>
        <ButtonWrapper
          disabled={pristine || submitting}
          onPress={handleSubmit((formValues) => {
            this.handleClick(skuResolved, formValues, true);
          })}
        >
          <BodyCopy
            fontFamily="secondary"
            fontWeight="extrabold"
            fontSize="fs13"
            text="Search"
            color="white"
          />
        </ButtonWrapper>
      </Row>
    );
  }

  renderRowView() {
    const { distancesMap, selectedValue } = this.props;
    return (
      <Row>
        <AddressCol>
          <Field
            name="addressLocation"
            id="addressLocation"
            component={TextBox}
            label="Zip, City or State"
          />
        </AddressCol>
        <DistanceCol>
          <Field
            bounces={false}
            name="distance"
            component={DropDown}
            heading="Distance"
            data={distancesMap}
            label="Distance"
            className="distance-input"
            dropDownStyle={{ ...dropDownStyle }}
            itemStyle={{ ...itemStyle }}
            variation="secondary"
            selectedValue={selectedValue && selectedValue.toString()}
            onValueChange={(itemValue) => {
              this.handleDistanceClick(itemValue);
            }}
          />
        </DistanceCol>
      </Row>
    );
  }

  renderCurrentlocationView() {
    const { onSearch, scrollRef } = this.props;
    return (
      <ViewWrapper>
        <Anchor
          onPress={() => {
            const eventName = ANALYTICS_LABELS.USE_MY_CURRENT_LOCATION_EVENT_NAME;
            const customEvents = ANALYTICS_LABELS.USE_MY_CURRENT_LOCATION_CUSTOM_EVENT;
            this.triggerAnalyticsEvents(customEvents, eventName);
            onSearch({ isDefaultCase: false, isFromCurrentLocation: true });
            scrollRef();
          }}
        >
          <StyledStoreLocator>
            <Image source={MarkerIcon} height="20px" width="20px" />
            <StyledCurrentLocation>
              <BodyCopy
                fontFamily="secondary"
                fontWeight="semibold"
                fontSize="fs14"
                color="gray.900"
                text={PICKUP_LABELS.USE_MY_CURRENT_LOCATION}
                textDecoration="underline"
              />
            </StyledCurrentLocation>
          </StyledStoreLocator>
        </Anchor>
      </ViewWrapper>
    );
  }

  render() {
    const { isPickUpWarningModal, isFromChangeStore } = this.props;
    return (
      <>
        {isPickUpWarningModal && !isFromChangeStore && (
          <BodyCopyWithSpacing
            fontSize="fs14"
            fontWeight="semibold"
            color="gray.900"
            text={PICKUP_LABELS.ITEM_UNAVAILABLE}
          />
        )}
        {(!isPickUpWarningModal || isFromChangeStore) && this.displayStoreSearchComp()}
      </>
    );
  }
}

const defaultValidation = getStandardConfig(
  [{ addressLocation: 'addressLocation' }, { distance: 'distance' }],
  { stopOnFirstError: true }
);

const validateMethod = createValidateMethod(defaultValidation);

export default connect()(
  reduxForm({
    form: 'pickupSearchStoresForm',
    ...validateMethod,
    keepDirtyOnReinitialize: true, // [https://github.com/erikras/redux-form/issues/3690] redux-forms 7.2.0 causes bug that forms will reInit after mount setting changed values back to init values
    touchOnChange: true, // to show validation error messageas even if user did not touch the fields
    touchOnBlur: false, // to avoid hidding the search results on blur of any field without changes
  })(withStyles(PickupStoreSelectionForm, styles))
);

PickupStoreSelectionForm.propTypes = {
  onSearch: PropTypes.func,
  openRestrictedModalForBopis: PropTypes.func,
  isSkuResolved: PropTypes.bool,
  prePopulateZipCodeAndSearch: PropTypes.func,
  handleSubmit: PropTypes.func,
  change: PropTypes.func,
  isShoppingBag: PropTypes.bool,
  isSearchOnlyInCartStores: PropTypes.bool,
  onCloseClick: PropTypes.func,
  addToCartError: PropTypes.string,
  isBopisCtaEnabled: PropTypes.bool,
  updateCartItemStore: PropTypes.func,
  isBossEnabled: PropTypes.bool,
  allowBossStoreSearch: PropTypes.bool,
  defaultStore: PropTypes.bool,
  bopisChangeStore: PropTypes.func,
  isBopisEnabled: PropTypes.bool,
  isGiftCard: PropTypes.bool,
  cartBopisStoresList: PropTypes.shape({
    distance: PropTypes.string,
    productAvailability: PropTypes.shape({}),
    storeBossInfo: PropTypes.shape({}),
  }),
  handleAddTobag: PropTypes.func,
  handlePickupRadioBtn: PropTypes.func,
  handleUpdatePickUpItem: PropTypes.func,
  selectedStoreId: PropTypes.number,
  isBossSelected: PropTypes.bool,
  isShowMessage: PropTypes.bool,
  setFavoriteStore: PropTypes.func,
  getDefaultStore: PropTypes.func,
  pickupStoreListRef: PropTypes.func,
  error: PropTypes.string,
  distancesMap: PropTypes.shape({}),
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  storeSearchError: PropTypes.string,
  PickupSkuFormValues: PropTypes.shape({
    /** user's preselected color id from parent instance */
    color: PropTypes.string,
    /** user's preselected fit id from parent instance */
    fit: PropTypes.string,
    /** user's preselected size id from parent instance */
    size: PropTypes.string,
    /** user's preselected quantity from parent instance */
    quantity: PropTypes.number,
    Size: PropTypes.string,
  }),
  selectedValue: PropTypes.string,
  toastMessage: PropTypes.string,
  sizeErrorMessage: PropTypes.string,
  scrollRef: PropTypes.func,
  onQuantityChange: PropTypes.func,
  form: PropTypes.string,
  preferredStore: PropTypes.string,
  getIsBopisAvailable: PropTypes.bool,
  isGetUserStoresLoaded: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func,
  trackClick: PropTypes.func,
  pageData: PropTypes.shape({}),
  getPreferredStoreData: PropTypes.func,
  deriveStoreSearchAttributes: PropTypes.func,
  deriveBossCtaEnabled: PropTypes.bool,
  renderVariationText: PropTypes.func,
  isPickUpWarningModal: PropTypes.bool,
  fromPage: PropTypes.string,
  isFromChangeStore: PropTypes.bool,
  isAnySkuSelected: PropTypes.bool,
  setSkuEditFlag: PropTypes.func,
};
PickupStoreSelectionForm.defaultProps = {
  onSearch: () => {},
  openRestrictedModalForBopis: () => {},
  isSkuResolved: false,
  prePopulateZipCodeAndSearch: () => {},
  handleSubmit: () => {},
  change: () => {},
  isShoppingBag: false,
  isSearchOnlyInCartStores: false,
  onCloseClick: () => {},
  addToCartError: '',
  isBopisCtaEnabled: false,
  updateCartItemStore: () => {},
  isBossEnabled: false,
  allowBossStoreSearch: false,
  defaultStore: false,
  bopisChangeStore: () => {},
  isBopisEnabled: false,
  isGiftCard: false,
  cartBopisStoresList: {},
  handleAddTobag: () => {},
  handlePickupRadioBtn: () => {},
  handleUpdatePickUpItem: () => {},
  selectedStoreId: 0,
  isBossSelected: false,
  isShowMessage: false,
  setFavoriteStore: () => {},
  getDefaultStore: () => {},
  pickupStoreListRef: () => {},
  error: '',
  distancesMap: {},
  pristine: false,
  submitting: false,
  storeSearchError: '',
  PickupSkuFormValues: {},
  selectedValue: '',
  toastMessage: '',
  sizeErrorMessage: '',
  scrollRef: () => {},
  onQuantityChange: () => {},
  form: '',
  preferredStore: '',
  getIsBopisAvailable: false,
  isGetUserStoresLoaded: false,
  setClickAnalyticsData: () => {},
  trackClick: () => {},
  pageData: {},
  getPreferredStoreData: () => {},
  deriveStoreSearchAttributes: () => {},
  deriveBossCtaEnabled: false,
  renderVariationText: () => {},
  isPickUpWarningModal: false,
  fromPage: '',
  isFromChangeStore: false,
  isAnySkuSelected: false,
  setSkuEditFlag: () => {},
};

export { PickupStoreSelectionForm as PickupStoreSelectionFormVanilla };
