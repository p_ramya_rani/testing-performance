// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

export const FavStoreLabel = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const StoreListItemWrapper = styled.View`
  min-height: 138px;
  display: flex;
  flex-direction: row;
  border: 1px solid ${(props) => props.theme.colorPalette.gray['800']};
  padding: 0 15px 12px 10px;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED}
    ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const StoreListItemWrapperIsFromStore = styled.View`
  display: flex;
  flex: 1;
  min-height: 145px;
  background-color: ${(props) =>
    props.isDefaultStore ? props.theme.colorPalette.gray['300'] : '#fff'};
  border-radius: 16px;
  margin-top: 10px;
  margin-left: 16px;
  margin-right: 16px;
`;

export const StoreWrapperIsFromStore = styled.View`
  flex-direction: column;
  padding-left: 34px;
`;
export const AnchorWrapper = styled.View`
  margin-left: 24px;
  margin-bottom: 16px;
  margin-top: 14px;
`;

export const AvaialbilityWrapper = styled.View`
  padding-horizontal: 10px;
  padding-vertical: 3px;
  border-radius: 25px;
  margin-top: 5px;
  background-color: ${(props) =>
    props.isProductAvailable ? props.theme.colorPalette.green['200'] : '#f4cfd5'};
`;

export const FavStoreIcon = styled.View`
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
  width: 17px;
  height: 17px;
`;

export const StoreInfoWrapper = styled.View`
  display: flex;
  flex-direction: column;
  width: 44%;
`;
export const StoreInfoWrapperIsFromStore = styled.View`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding-horizontal: 10px;
  padding-top: 14px;
  padding-bottom: 4px;
`;
export const StoreWrapper = styled.View`
  display: flex;
  flex-direction: row;
`;
export const StoreWrapperHeader = styled.View`
  display: flex;
  flex-direction: row;
`;
export const PickupButtonsWrapper = styled.View`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: center;
  width: 56%;
`;

export const StoreUnavailable = styled.View`
  margin: 0 auto;
  width: 109px;
`;

export const PickupCTAWrapper = styled.View`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const PickupRadioBtnWrapper = styled.View`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  font-size: ${(props) => props.theme.typography.fontSizes.fs14};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
`;

export const PickupRadioBtnWrapperIsFromStore = styled.View`
  display: flex;
  flex: 0.65;
  padding-right: 15px;
`;
export const ViewWrapper = styled.View`
  display: flex;
  flex: 0.35;
  align-items: flex-end;
`;

export const StoreDetailsAnchorWrapper = css`
  text-decoration: underline;
`;

export const addToCartErrorStyle = css`
  padding-left: 35px;
`;

export const TooltipContentWrapper = styled.View`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const TooltipWrapper = styled.View`
  width: 73px;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;
export const TooltipWrapperFromStore = styled.View`
  margin-bottom: 16px;
  margin-top: 14px;
`;
export const Wrapper = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 8px;
`;

export default {
  FavStoreIcon,
  FavStoreLabel,
  StoreListItemWrapper,
  StoreInfoWrapper,
  PickupButtonsWrapper,
  StoreUnavailable,
  PickupCTAWrapper,
  PickupRadioBtnWrapper,
  StoreDetailsAnchorWrapper,
  TooltipWrapper,
  addToCartErrorStyle,
};
