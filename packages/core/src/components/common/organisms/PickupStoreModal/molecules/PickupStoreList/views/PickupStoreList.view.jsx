/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useRef } from 'react';
import { PropTypes } from 'prop-types';
import { BOPIS_ITEM_AVAILABILITY, BOPIS_FILTER_LABEL } from '../../../PickUpStoreModal.constants';
import PickupStoreListItem from '../../PickupStoreListItem';
import { STORE_SUMMARY_PROP_TYPES } from '../../../PickUpStoreModal.proptypes';
import StoreListItemSkeleton from '../../../atoms/StoreListItemSkeleton';
import { StyledInputCheckbox, InputBoxWrapper } from '../styles/PickupStoreList.style';
import { isMobileApp } from '../../../../../../../utils';

const renderCheckBoxComponent = (
  isOnlyShowAvailable,
  handleShowAvailableChange,
  isFromChangeStore
) => {
  return (
    <StyledInputCheckbox
      checkBoxLabel
      execOnChangeByDefault={false}
      input={{ value: isOnlyShowAvailable, onChange: handleShowAvailableChange }}
      isPickUpStoreView
      isFromChangeStore={isFromChangeStore}
    >
      {BOPIS_FILTER_LABEL}
    </StyledInputCheckbox>
  );
};

const renderCheckBox = (isOnlyShowAvailable, handleShowAvailableChange, isFromChangeStore) => {
  if (isMobileApp()) {
    return (
      <InputBoxWrapper>
        {renderCheckBoxComponent(isOnlyShowAvailable, handleShowAvailableChange, isFromChangeStore)}
      </InputBoxWrapper>
    );
  }
  return renderCheckBoxComponent(isOnlyShowAvailable, handleShowAvailableChange, isFromChangeStore);
};

const PickupStoreList = (props) => {
  const {
    sameStore,
    isShoppingBag,
    onStoreSelect,
    isShowFilterCheckbox,
    isResultOfSearchingInCartStores,
    isBossSelected,
    addToCartError,
    isBopisCtaEnabled,
    isBossCtaEnabled,
    allowBossStoreSearch,
    updateCartItemStore,
    buttonLabel,
    isBossEnabled,
    isBopisEnabled,
    defaultStoreName,
    isGiftCard,
    selectedStoreId,
    handleShowAvailableChange,
    isOnlyShowAvailable,
    derivedStoresList,
    addItemToCartInPickup,
    onPickupRadioBtnToggle,
    onStoreUpdate,
    isSearching,
    isUserCartStoreLoaded,
    currentProduct,
    setClickAnalyticsData,
    trackClick,
    setFavoriteStore,
    getDefaultStore,
    pageData,
    storeSearchCriteria,
    storeSearchDistance,
    fromPage,
    newQVEnabled,
    isFromChangeStore,
    quickViewLabels,
    isBopisPickup,
    isSizeSelected,
  } = props;

  const showBopisStoreFilterCheckbox = newQVEnabled ? isBopisPickup : true;

  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  };

  const prevList = usePrevious(derivedStoresList);
  const shouldUpdate = prevList !== derivedStoresList;

  useEffect(() => {
    if (isFromChangeStore && isMobileApp() && derivedStoresList && derivedStoresList[0]) {
      const filterDefaultStore = derivedStoresList.filter((item) => item.basicInfo.isDefault);
      if (filterDefaultStore && filterDefaultStore[0]) {
        onPickupRadioBtnToggle(filterDefaultStore[0].basicInfo.id, false, filterDefaultStore[0]);
      } else {
        onPickupRadioBtnToggle(derivedStoresList[0].basicInfo.id, false, derivedStoresList[0]);
      }
    }
  }, [shouldUpdate, isOnlyShowAvailable, isSearching]);

  const isBopisSelected = newQVEnabled ? isBopisPickup : !isBossSelected;
  return (
    <>
      {!allowBossStoreSearch &&
        showBopisStoreFilterCheckbox &&
        !isResultOfSearchingInCartStores &&
        isShowFilterCheckbox &&
        renderCheckBox(isOnlyShowAvailable, handleShowAvailableChange, isFromChangeStore)}

      {derivedStoresList.map((store) => (
        <PickupStoreListItem
          addItemToCartInPickup={addItemToCartInPickup}
          sameStore={sameStore}
          currentProduct={currentProduct}
          isShoppingBag={isShoppingBag}
          key={store.basicInfo.id}
          store={store}
          onStoreSelect={onStoreSelect}
          isBopisSelected={store.basicInfo.id === selectedStoreId && isBopisSelected}
          isBossSelected={store.basicInfo.id === selectedStoreId && !isBopisSelected}
          selectedStoreId={selectedStoreId}
          isBopisAvailable={
            store.productAvailability.status === BOPIS_ITEM_AVAILABILITY.AVAILABLE ||
            store.productAvailability.status === BOPIS_ITEM_AVAILABILITY.LIMITED
          }
          defaultStoreName={defaultStoreName}
          isBossAvailable={store.storeBossInfo.isBossEligible}
          storeBossInfo={store.storeBossInfo}
          addToCartError={addToCartError}
          isBopisCtaEnabled={isBopisCtaEnabled && isBopisEnabled}
          isBossCtaEnabled={isBossCtaEnabled && isBossEnabled}
          updateCartItemStore={updateCartItemStore}
          buttonLabel={buttonLabel}
          isGiftCard={isGiftCard}
          onPickupRadioBtnToggle={onPickupRadioBtnToggle}
          onStoreUpdate={onStoreUpdate}
          setClickAnalyticsData={setClickAnalyticsData}
          trackClick={trackClick}
          setFavoriteStore={setFavoriteStore}
          getDefaultStore={getDefaultStore}
          pageData={pageData}
          storeSearchCriteria={storeSearchCriteria}
          storeSearchDistance={storeSearchDistance}
          fromPage={fromPage}
          newQVEnabled={newQVEnabled}
          isFromChangeStore={isFromChangeStore}
          quickViewLabels={quickViewLabels}
          isBopisPickup={isBopisPickup}
          isSizeSelected={isSizeSelected}
        />
      ))}
      {isSearching || !isUserCartStoreLoaded ? <StoreListItemSkeleton col={20} /> : null}
    </>
  );
};

PickupStoreList.propTypes = {
  /** Error message when add to cart */
  addToCartError: PropTypes.string.isRequired,
  /** Array of stores to display */
  derivedStoresList: PropTypes.arrayOf(
    PropTypes.shape({
      ...STORE_SUMMARY_PROP_TYPES,
      /** the availability status of the searched for cart item in this store */
      basicInfo: PropTypes.shape({
        /** store id identifier */
        id: PropTypes.string,
      }),
      productAvailability: PropTypes.shape({
        status: PropTypes.oneOf(
          Object.keys(BOPIS_ITEM_AVAILABILITY).map((key) => BOPIS_ITEM_AVAILABILITY[key])
        ).isRequired,
      }).isRequired,
    })
  ).isRequired,

  /**
   * Function to call when a store is selected. The called function will
   * receive one parameter, the id of the clicked store.
   */
  onStoreSelect: PropTypes.func.isRequired,
  onStoreUpdate: PropTypes.func.isRequired,
  handleShowAvailableChange: PropTypes.func.isRequired,
  addItemToCartInPickup: PropTypes.func.isRequired,

  /** Flag to identify Boss/Bopis */
  isBossSelected: PropTypes.bool.isRequired,
  isOnlyShowAvailable: PropTypes.bool.isRequired,
  /** flags if to show the checkbox for showing only stores with avialability */
  isShowFilterCheckbox: PropTypes.bool.isRequired,
  /** flags if search was in stores already in cart */
  isResultOfSearchingInCartStores: PropTypes.bool.isRequired,

  /** We need to differentiate if Bopis Modal is open from cart or other place to change select item button's message (DT-27100) */
  isShoppingBag: PropTypes.bool.isRequired,

  /** Function for enabling and disabling Add to bag sticky button */
  // resetAddToBagForBopis: PropTypes.func.isRequired,

  /** array of added store in the account */
  cartBopisStoresList: PropTypes.shape({
    distance: PropTypes.string,
    productAvailability: PropTypes.shape({}),
    storeBossInfo: PropTypes.shape({}),
  }).isRequired,

  /** checks if the cart having both same store for BOPIS and BOSS */
  sameStore: PropTypes.bool.isRequired,
  pageData: PropTypes.shape({}).isRequired,
  isSearching: PropTypes.bool.isRequired,
  /** store id that was selected */
  selectedStoreId: PropTypes.number.isRequired,

  /** Global switches for BOSS and BOPIS */
  isBossEnabled: PropTypes.bool,
  isBopisEnabled: PropTypes.bool,

  isGiftCard: PropTypes.bool.isRequired,
  allowBossStoreSearch: PropTypes.bool.isRequired,
  onPickupRadioBtnToggle: PropTypes.func.isRequired,
  updateCartItemStore: PropTypes.bool.isRequired,
  buttonLabel: PropTypes.string.isRequired,
  isBopisCtaEnabled: PropTypes.bool.isRequired,
  isBossCtaEnabled: PropTypes.bool.isRequired,
  defaultStoreName: PropTypes.string,
  isUserCartStoreLoaded: PropTypes.bool.isRequired,
  setClickAnalyticsData: PropTypes.func.isRequired,
  trackClick: PropTypes.func.isRequired,
  currentProduct: PropTypes.string,
  setFavoriteStore: PropTypes.func,
  getDefaultStore: PropTypes.func,
  storeSearchCriteria: PropTypes.string,
  storeSearchDistance: PropTypes.string,
  fromPage: PropTypes.string,
  newQVEnabled: PropTypes.bool,
  isFromChangeStore: PropTypes.bool,
  quickViewLabels: PropTypes.bool,
  isBopisPickup: PropTypes.bool,
  isSizeSelected: PropTypes.bool,
};

PickupStoreList.defaultProps = {
  isBossEnabled: true,
  isBopisEnabled: true,
  defaultStoreName: '',
  currentProduct: '',
  setFavoriteStore: () => {},
  getDefaultStore: () => {},
  storeSearchCriteria: '',
  storeSearchDistance: '',
  fromPage: '',
  newQVEnabled: false,
  isFromChangeStore: false,
  quickViewLabels: false,
  isBopisPickup: false,
  isSizeSelected: false,
};

export default PickupStoreList;
