// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { BOPIS_PRODUCT_INFO_PROP_TYPES } from '@tcp/core/src/components/common/organisms/PickupStoreModal/PickUpStoreModal.proptypes';
import { getImageData } from '@tcp/core/src/utils';
import cssClassName from '@tcp/core/src/utils/cssClassName';
import {
  PRODUCT_VALUES,
  PICKUP_LABELS,
} from '@tcp/core/src/components/common/organisms/PickupStoreModal/PickUpStoreModal.constants';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { Row, Col, DamImage } from '@tcp/core/src/components/common/atoms';

import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ProductPrice from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductPrice/ProductPrice';
import PickupProductFormPartStyling from '../styles/PickupProductFormPart.style';
/**
 * @method ProductPricesBopisSection this method component to display
 *  offer price and list price
 * @param {props} props contain price values.
 */
export function ProductPricesBopisSection(props) {
  const { currencySymbol, listPrice, offerPrice } = props;
  const offerPriceClass = cssClassName('text-price ', 'offer-price ', {
    'offer-price-only': offerPrice === listPrice,
  });
  const listPriceClass = cssClassName('text-price ', 'list-price ');
  return (
    <span className="container-price-bopis">
      {offerPrice && offerPrice !== listPrice && (
        <BodyCopy
          className={listPriceClass}
          fontFamily="secondary"
          fontWeight="black"
          fontSize={['fs16']}
          component="span"
        >
          {currencySymbol + offerPrice.toFixed(2)}
        </BodyCopy>
      )}
      {offerPrice && (
        <BodyCopy
          className={offerPriceClass}
          component="span"
          fontSize={['fs12']}
          fontFamily="secondary"
          color="text.secondary"
        >
          {currencySymbol + listPrice.toFixed(2)}
        </BodyCopy>
      )}
    </span>
  );
}

ProductPricesBopisSection.propTypes = {
  /** This is used to display the correct currency symbol */
  currencySymbol: PropTypes.string.isRequired,

  listPrice: PropTypes.string.isRequired,

  offerPrice: PropTypes.string.isRequired,
};

export function DisplayProductSpecification(props) {
  const { productKey, productValue } = props;
  return (
    <BodyCopy className="product-key" fontSize="fs12" fontFamily="secondary" color="text.secondary">
      <BodyCopy fontSize="fs12" fontFamily="secondary" fontWeight="semibold" component="span">
        {productKey}
        {':'}
      </BodyCopy>
      <BodyCopy fontSize="fs12" fontFamily="secondary" color="text.secondary" component="span">
        {productValue}
      </BodyCopy>
    </BodyCopy>
  );
}

DisplayProductSpecification.propTypes = {
  /** This is used to display the correct product specification */
  productKey: PropTypes.string.isRequired,

  productValue: PropTypes.string.isRequired,
};

class PickupProductFormPartSkeleton extends React.Component {
  static propTypes = {
    /** the whole product detail to have it engaged on BOPIS form */
    ...BOPIS_PRODUCT_INFO_PROP_TYPES,
    /** This is used to display the correct currency symbol */
    currencySymbol: PropTypes.string.isRequired,
    isPickUpWarningModal: PropTypes.bool,
    isInternationalShipping: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    isPickUpWarningModal: false,
  };

  renderProductValues = (initialValues, size) => {
    const fitSize = `${initialValues.Size} ${initialValues.Fit || ''}`;
    return (
      <React.Fragment>
        <DisplayProductSpecification productKey={size} productValue={fitSize} />
        <DisplayProductSpecification
          productKey={PRODUCT_VALUES.quantity}
          productValue={initialValues.Quantity}
        />
      </React.Fragment>
    );
  };

  render() {
    const { initialValuesFromBagPage, className } = this.props;
    const {
      name,
      itemBrand,
      imagePath,
      currencySymbol,
      listPrice,
      offerPrice,
      color,
      currencyAttributes,
      Size,
      Fit,
      Quantity,
    } = initialValuesFromBagPage;
    const altImageText = `Image for product ${name}`;
    const imgData = {
      alt: altImageText,
      url: imagePath,
    };
    const configData = getImageData(imgData.url);
    return (
      <div className={`${className} item-product-container clearfix`}>
        <Row>
          <Col colSize={{ small: 3, medium: 4, large: 6 }} className="container-image">
            <DamImage
              imgData={imgData}
              isProductImage
              itemBrand={itemBrand}
              imgConfigs={configData.imgConfig}
              isOptimizedVideo
              primaryBrand={itemBrand}
            />
          </Col>
          <Col colSize={{ small: 3, medium: 4, large: 6 }} className="product-description">
            <BodyCopy
              className="product-title"
              fontSize={['fs14', 'fs14', 'fs18']}
              fontWeight={['semibold', 'extrabold', 'black']}
              fontFamily="secondary"
            >
              {name}
            </BodyCopy>
            <div>
              <BodyCopy className="product-color">
                <DisplayProductSpecification productKey="Color" productValue={color} />
              </BodyCopy>
              <BodyCopy className="product-values">
                {this.renderProductValues({ Size, Fit, Quantity }, 'Size')}
              </BodyCopy>
              <BodyCopy className="product-Price">
                <BodyCopy
                  fontWeight="semibold"
                  fontSize={['fs12']}
                  component="span"
                  fontFamily="secondary"
                >
                  {`${PICKUP_LABELS.PRICE_LABEL}:`}
                </BodyCopy>
                <div className="ProductPrice">
                  <ProductPrice
                    currencySymbol={currencySymbol}
                    listPrice={listPrice}
                    offerPrice={offerPrice}
                    currencyAttributes={currencyAttributes}
                    fromPickUp
                  />
                </div>
              </BodyCopy>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withStyles(PickupProductFormPartSkeleton, PickupProductFormPartStyling);
export { PickupProductFormPartSkeleton as PickupProductFormPartSkeletonVanilla };
