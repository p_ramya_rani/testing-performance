// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module PickupSkuSelectionForm
 * @desc Component to display step 1 of PickUp in Store Modal and fill sku details
 * doesn't open when sku is resolved
 */

import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { TouchableOpacity } from 'react-native';
import { BodyCopy, DamImage } from '@tcp/core/src/components/common/atoms';
import { PRODUCT_SKU_SELECTION_FORM } from '@tcp/core/src/constants/reducer.constants';
import { disableTcpAppProduct } from '@tcp/core/src/utils';
import { APP_TYPE } from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import { getBrand } from '../../../../../../../utils';
import withStyles from '../../../../../hoc/withStyles';
import styles from '../../../../QuickViewModal/molecules/ProductCustomizeFormPart/styles/ProductCustomizeFormPart.style';
import ProductAddToBagContainer from '../../../../../molecules/ProductAddToBag';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import { SKU_DETAILS, PICKUP_LABELS } from '../../../PickUpStoreModal.constants';
import { BodyCopyWithSpacing } from '../../../../../atoms/styledWrapper';
import { getAPIConfig } from '../../../../../../../utils/index.native';

import {
  PickUpSkUSectionContainer,
  ImageWrapper,
  ProductSummaryContainer,
  ProductDetailSummary,
  AfterPayWrapper,
  OfferPriceContainer,
  ProductContainer,
} from '../styles/PickupSkuSelectionForm.style.native';
import { getProductListToPathInMobileApp } from '../../../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';

const checkIfProductOfSameDomain = (initialValues) => {
  const currentAppBrand = getBrand();
  let isProductBrandOfSameDomain = true;
  if (initialValues && initialValues.itemBrand) {
    isProductBrandOfSameDomain =
      currentAppBrand.toUpperCase() === initialValues.itemBrand.toUpperCase();
  }
  return isProductBrandOfSameDomain;
};

const getItemBrand = (initialValues) => {
  return !isEmpty(initialValues) ? initialValues.itemBrand : null;
};

const getBrandColor = () => {
  const apiConfigObj = getAPIConfig();
  const { brandId } = apiConfigObj;
  return brandId === 'tcp' ? 'red.500' : 'black';
};

const switchBrands = ({
  currentAppBrand,
  updateAppTypeHandler,
  title,
  selectedColorProductId,
  pdpUrl,
}) => {
  return updateAppTypeHandler({
    type: currentAppBrand.toLowerCase() === APP_TYPE.TCP ? APP_TYPE.GYMBOREE : APP_TYPE.TCP,
    params: {
      title,
      pdpUrl: getProductListToPathInMobileApp(pdpUrl),
      selectedColorProductId,
      reset: true,
    },
  });
};

const handleNaNValue = (number) => {
  if (Number.isNaN(Number(number))) {
    return (0).toFixed(2);
  }
  return number;
};
const getAccessibilityText = (accessibilityLabels, currencyPrefix, listPrice) => {
  return `${
    accessibilityLabels && accessibilityLabels.previousPrice
  }: ${currencyPrefix}${listPrice}`;
};

const AfterPayMessagingWrapper = (offerPrice, isFromChangeStore) => {
  return offerPrice ? (
    <AfterPayWrapper>
      <AfterPayMessaging offerPrice={offerPrice} isFromChangeStore={isFromChangeStore} />
    </AfterPayWrapper>
  ) : null;
};

const getPrimaryBrand = (primaryBrand, alternateBrand) => {
  return primaryBrand || alternateBrand;
};

const PickupSkuSelectionForm = (props) => {
  const onGoToPDPPage = (pdpUrl, selectedColorProductId) => {
    const { navigation, onCloseClick, initialValues, updateAppTypeHandler } = props;
    const currentAppBrand = getBrand();
    const isProductBrandOfSameDomain = checkIfProductOfSameDomain(initialValues);
    const title = navigation && navigation.getParam('title');
    onCloseClick();

    if (!isProductBrandOfSameDomain) {
      const itemBrand = getItemBrand(initialValues);
      if (disableTcpAppProduct(itemBrand)) {
        return;
      }
      switchBrands({
        currentAppBrand,
        updateAppTypeHandler,
        title,
        selectedColorProductId,
        pdpUrl,
      });
    } else {
      navigation.navigate('ProductDetail', {
        title,
        pdpUrl: getProductListToPathInMobileApp(pdpUrl),
        selectedColorProductId,
        reset: true,
      });
    }
  };

  const renderBtn = (isFromChangeStore, currentProduct, currentColorEntry) => {
    const currentColorPdpUrl = currentColorEntry && currentColorEntry.pdpUrl;
    const colorProductId = currentColorEntry && currentColorEntry.colorProductId;
    return isFromChangeStore ? (
      <BodyCopy
        fontSize="fs12"
        fontWeight="regular"
        text={PICKUP_LABELS.EARN_DOUBLE_TRIPLE_POINTS}
        color="orange.800"
      />
    ) : (
      <TouchableOpacity
        onPress={() => onGoToPDPPage(currentColorPdpUrl, colorProductId)}
        accessible
        accessibilityRole="button"
        accessibilityLabel={`${currentProduct.name}`}
      >
        <BodyCopy
          fontSize="fs14"
          fontWeight="regular"
          fontFamily="secondary"
          textDecoration="underline"
          text={PICKUP_LABELS.VIEW_DETAILS}
        />
      </TouchableOpacity>
    );
  };

  const renderProductSummary = () => {
    const {
      initialValues,
      currentProduct,
      prices,
      currency,
      currentColorEntry,
      imageUrl,
      accessibilityLabels,
      primaryBrand,
      alternateBrand,
      isFromChangeStore,
    } = props;

    const badge2 = prices && prices.badge2;
    const currencyPrefix = currency === 'USD' ? '$' : currency;
    const primaryBrandValue = getPrimaryBrand(primaryBrand, alternateBrand);
    let listPrice = prices && (Math.round(prices.listPrice * 100) / 100).toFixed(2);
    let offerPrice = prices && (Math.round(prices.offerPrice * 100) / 100).toFixed(2);
    const listPriceStyle = { lineHeight: 20 };
    const itemBrand = getItemBrand(initialValues);
    offerPrice = handleNaNValue(offerPrice);
    listPrice = handleNaNValue(listPrice);

    return (
      <ProductSummaryContainer>
        <ImageWrapper>
          <DamImage
            resizeMode="contain"
            height="202px"
            width="164px"
            url={imageUrl}
            isProductImage
            itemBrand={itemBrand}
            primaryBrand={primaryBrandValue}
          />
        </ImageWrapper>
        <ProductDetailSummary>
          {isFromChangeStore ? (
            <BodyCopyWithSpacing
              width="100%"
              fontFamily="secondary"
              fontSize="fs12"
              fontWeight="bold"
              text={currentProduct.long_product_title || currentProduct.name}
              spacingStyles="margin-bottom-XS"
            />
          ) : (
            <BodyCopyWithSpacing
              width="100%"
              fontFamily="secondary"
              fontSize="fs14"
              fontWeight="extrabold"
              text={currentProduct.name}
              spacingStyles="margin-bottom-LRG"
            />
          )}
          <OfferPriceContainer isFromChangeStore={isFromChangeStore}>
            <BodyCopy
              dataLocator="pdp_current_product_price"
              fontFamily="secondary"
              fontSize={isFromChangeStore ? 'fs22' : 'fs16'}
              fontWeight="black"
              color={getBrandColor()}
              text={`${currencyPrefix}${offerPrice}`}
            />
            <ProductContainer>
              {listPrice !== offerPrice ? (
                <BodyCopy
                  dataLocator="pdp_discounted_product_price"
                  textDecoration="line-through"
                  fontFamily="secondary"
                  fontSize="fs12"
                  margin="0 0 0 10px"
                  fontWeight="regular"
                  color="gray.800"
                  text={`${currencyPrefix}${listPrice}`}
                  accessibilityText={getAccessibilityText(
                    accessibilityLabels,
                    currencyPrefix,
                    listPrice
                  )}
                  style={listPriceStyle}
                />
              ) : null}
              {badge2 ? (
                <BodyCopy
                  dataLocator="pdp_discounted_percentage"
                  margin="0 0 0 10px"
                  fontFamily="secondary"
                  fontSize="fs12"
                  fontWeight="regular"
                  color={getBrandColor()}
                  text={badge2}
                  style={listPriceStyle}
                />
              ) : null}
            </ProductContainer>
          </OfferPriceContainer>
          {AfterPayMessagingWrapper(offerPrice, isFromChangeStore)}
          {renderBtn(isFromChangeStore, currentProduct, currentColorEntry)}
        </ProductDetailSummary>
      </ProductSummaryContainer>
    );
  };

  const {
    initialValues,
    currentProduct,
    onChangeColor,
    onChangeSize,
    generalProductId,
    toastMessage,
    onFitChange,
    pdpLabels,
    onQuickViewOpenClick,
    closePickupModal,
    setInitialTCPStyleQty,
    singlePageLoad,
    initialMultipackMapping,
    multipackSelectionAction,
    getQickViewSingleLoad,
    getDisableSelectedTab,
    selectedMultipack,
    disableMultiPackTab,
    TCPStyleQTY,
    availableTCPmapNewStyleId,
    onPickUpOpenClick,
    hideMultipackPills,
    isFromChangeStore,
  } = props;

  return (
    <PickUpSkUSectionContainer>
      {renderProductSummary()}

      <ProductAddToBagContainer
        onChangeColor={onChangeColor}
        onChangeSize={onChangeSize}
        onFitChange={onFitChange}
        plpLabels={SKU_DETAILS}
        currentProduct={currentProduct}
        customFormName={PRODUCT_SKU_SELECTION_FORM}
        selectedColorProductId={generalProductId}
        initialFormValues={initialValues}
        formValues={initialValues}
        showAddToBagCTA={false}
        toastMessage={toastMessage}
        isDisableZeroInventoryEntries={true}
        isPickup
        pdpLabels={pdpLabels}
        onQuickViewOpenClick={onQuickViewOpenClick}
        closePickupModal={closePickupModal}
        setInitialTCPStyleQty={setInitialTCPStyleQty}
        singlePageLoad={singlePageLoad}
        initialMultipackMapping={initialMultipackMapping}
        multipackSelectionAction={multipackSelectionAction}
        getQickViewSingleLoad={getQickViewSingleLoad}
        getDisableSelectedTab={getDisableSelectedTab}
        selectedMultipack={selectedMultipack}
        disableMultiPackTab={disableMultiPackTab}
        TCPStyleQTY={TCPStyleQTY}
        availableTCPmapNewStyleId={availableTCPmapNewStyleId}
        onPickUpOpenClick={onPickUpOpenClick}
        hideMultipackPills={hideMultipackPills}
        isFromChangeStore={isFromChangeStore}
      />
    </PickUpSkUSectionContainer>
  );
};

PickupSkuSelectionForm.propTypes = {
  // colorFitsSizesMap: PropTypes.arrayOf(PropTypes.object).isRequired,

  /** seed values for the form */
  initialValues: PropTypes.shape({
    /** user's preselected color id from parent instance */
    color: PropTypes.string,
    /** user's preselected fit id from parent instance */
    fit: PropTypes.string,
    /** user's preselected size id from parent instance */
    size: PropTypes.string,
    /** user's preselected quantity from parent instance */
    quantity: PropTypes.number,
  }).isRequired,

  currentProduct: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,

  currency: PropTypes.string,
  generalProductId: PropTypes.string,

  prices: PropTypes.shape({
    listPrice: PropTypes.number.isRequired,
    offerPrice: PropTypes.number,
  }).isRequired,

  // isHasPlcc: PropTypes.bool,

  onCloseClick: PropTypes.func,
  navigation: PropTypes.func,
  onChangeColor: PropTypes.func,
  onChangeSize: PropTypes.func,
  currentColorEntry: PropTypes.shape({}),
  accessibilityLabels: PropTypes.shape({}),
  imageUrl: PropTypes.string.isRequired,
  toastMessage: PropTypes.func,
  updateAppTypeHandler: PropTypes.func.isRequired,
  onFitChange: PropTypes.func,
  pdpLabels: PropTypes.shape({}).isRequired,
  onQuickViewOpenClick: PropTypes.func.isRequired,
  closePickupModal: PropTypes.func.isRequired,
  setInitialTCPStyleQty: PropTypes.string.isRequired,
  singlePageLoad: PropTypes.string.isRequired,
  initialMultipackMapping: PropTypes.string.isRequired,
  multipackSelectionAction: PropTypes.func,
  getQickViewSingleLoad: PropTypes.func,
  getDisableSelectedTab: PropTypes.func,
  selectedMultipack: PropTypes.string,
  disableMultiPackTab: PropTypes.bool,
  TCPStyleQTY: PropTypes.string,
  availableTCPmapNewStyleId: PropTypes.shape([]),
  onPickUpOpenClick: PropTypes.func,
  hideMultipackPills: PropTypes.bool,
  isFromChangeStore: PropTypes.bool,
  primaryBrand: PropTypes.string,
  alternateBrand: PropTypes.string,
};

PickupSkuSelectionForm.defaultProps = {
  navigation: null,
  currency: 'USD',
  onCloseClick: () => {},
  onChangeColor: null,
  onChangeSize: () => {},
  currentColorEntry: {},
  accessibilityLabels: {},
  generalProductId: '',
  toastMessage: () => {},
  onFitChange: () => {},
  multipackSelectionAction: () => {},
  getQickViewSingleLoad: () => {},
  getDisableSelectedTab: () => {},
  selectedMultipack: '1',
  disableMultiPackTab: false,
  TCPStyleQTY: '',
  availableTCPmapNewStyleId: [],
  onPickUpOpenClick: () => {},
  hideMultipackPills: false,
  isFromChangeStore: false,
  primaryBrand: '',
  alternateBrand: '',
};

export default withStyles(PickupSkuSelectionForm, styles);
