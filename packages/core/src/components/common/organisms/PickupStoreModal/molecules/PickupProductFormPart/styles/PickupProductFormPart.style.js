// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  &.item-product-container + form {
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
  }

  .product-title {
    margin-bottom: 7px;
  }

  .container-price-bopis > span {
    margin-right: 5px;
  }

  .product-Price {
    position: relative;
  }

  .product-Price > span:first-child {
    margin-right: 14px;
  }

  .promotionMsgContainer {
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0
      ${(props) => props.theme.spacing.ELEM_SPACING.XL}
      ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0;
    }
    .loyalty-text-container {
      color: ${(props) => props.theme.colorPalette.black};
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      font-weight: ${(props) => props.theme.typography.fontWeights.regular};
      span {
        color: ${(props) =>
          props.isHasPlcc
            ? props.theme.colorPalette.userTheme.plcc
            : props.theme.colorPalette.userTheme.mpr};
        font-weight: ${(props) => props.theme.typography.fontWeights.black};
      }
    }
  }
  .product-color,
  .product-values,
  .product-key {
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (!props.newQVEnabled ? 'display: inline;' : '')}
    }
  }

  .product-Price > span:first-child,
  .product-color .product-key > span:first-child {
    margin-right: 14px;
  }

  .product-values .product-key:first-child > span {
    margin-right: ${(props) => (props.newQVEnabled ? '21px' : '23px')};

    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: ${(props) => (props.newQVEnabled ? '21px' : '5px')};
    }
  }

  .product-values .product-key:last-child > span {
    margin-right: ${(props) => (props.newQVEnabled ? '23px' : '25px')};

    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: ${(props) => (props.newQVEnabled ? '23px' : '2px')};
    }
  }
  .product-values .product-key {
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (!props.newQVEnabled ? 'padding: 0px 10px;' : 'padding: 4px 0px;')}
    }
  }

  .product-color .product-key {
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-right: 10px;
    }
  }

  .product-color .product-key,
  .product-key:not(:last-child) {
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) =>
        !props.newQVEnabled ? `border-right: 1px solid ${props.theme.colorPalette.gray[800]};` : ''}
    }
  }

  .edit-link {
    padding-top: 22px;
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
  .edit-link-afterpay {
    padding-top: 64px;
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }

  .list-price {
    color: ${(props) =>
      props.theme.isGymboree
        ? props.theme.colorPalette.gray[900]
        : props.theme.colorPalette.red[600]};
  }

  .ProductPrice {
    display: inline-flex;
  }

  .actual-price {
    color: #e02020;
    font-size: 16px;
  }

  .product-price-container {
    margin-top: 0px;

    .actual-price {
      color: ${(props) =>
        props.theme.isGymboree
          ? props.theme.colorPalette.gray[900]
          : props.theme.colorPalette.red[500]};
    }
  }

  .original-price {
    font-size: 12px;
    color: #595959;
    margin-left: 5px;
    line-height: 1.9;
  }

  .price-container {
    display: block;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: flex;
    }
    .badge {
      display: none;
    }
  }
`;
