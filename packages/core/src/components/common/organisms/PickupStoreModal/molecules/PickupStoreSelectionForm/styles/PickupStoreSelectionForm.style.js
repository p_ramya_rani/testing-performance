// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .TextBox__input,
  .select__input {
    padding-top: ${(props) => (!props.newQVEnabled ? '18px' : '26px')};
  }
  .find-store-label {
    margin-top: 12px;
    margin-bottom: 14px;
  }
  .distance-input {
    margin: 18px 0 12px;
  }
  margin: 0 0 24px 0;
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    ${(props) => (props.newQVEnabled ? 'margin: 0 0 50px 0;' : '')}
    .button-wrapper {
      ${(props) => (props.newQVEnabled ? 'margin-right: 10px;' : '')}
    }
  }
  .storeSearchError {
    margin: 10px;
  }
  .newfindstore {
    display: flex;
  }
  .newzipcode-field {
    flex-basis: 70%;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      flex-basis: 60%;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex-basis: 55%;
      padding: 25px 10px 0px 4px;
    }
  }
  .newdistance-input {
    flex-basis: 26%;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 3px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      flex-basis: 35%;
      padding: 3px 5px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex-basis: 34%;
      padding: 10px;
    }
  }
  .zipcode-field {
    margin-right: 3px;
  }
  .alignCenter {
    display: flex;
    justify-content: center;
  }
  .padding-top {
    padding-top: 24px;
  }
  @media ${(props) => props.theme.mediaQuery.medium} {
    .find-store-label {
      margin-top: 12px;
      margin-bottom: 20px;
    }
    .distance-input {
      margin: 0;
    }
    .storeSearchError {
      margin: 0;
    }
    .zipcode-field {
      margin-bottom: ${(props) => (props.newQVEnabled ? '15px' : '')};
      margin-right: ${(props) => (props.newQVEnabled ? '12px' : '0px')};
    }
    .button-search-bopis {
      ${(props) => (props.newQVEnabled ? 'margin-bottom: 15px;' : '')}
    }
    .button-wrapper {
      ${(props) => (props.newQVEnabled ? 'margin-right: 8px; width: 100%;' : '')}
    }
    margin: 0;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .find-store-label {
      margin-top: 12px;
      margin-bottom: 20px;
    }
    .distance-input {
      ${(props) => (!props.newQVEnabled ? 'margin: 0 10px' : 'margin-bottom: 7px;')}
    }
    .TextBox__input,
    .select__input {
      padding-top: 26px;
    }
    .storeSearchError {
      margin: 0;
    }
    .zipcode-field {
      margin-bottom: ${(props) => (props.newQVEnabled ? '15px' : '')};
      margin-right: ${(props) => (props.newQVEnabled ? '12px' : '0px')};
    }
    .button-search-bopis {
      ${(props) => (props.newQVEnabled ? 'margin-bottom: 15px;' : '')}
    }
    .button-wrapper {
      ${(props) => (props.newQVEnabled ? 'margin-right: 8px;' : '')}
    }
    margin: 0;
  }
`;
