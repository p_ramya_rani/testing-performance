// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';

const styles = css`
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
  margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
`;

export const Container = styled.View`
  margin-top: 45px;
`;

export const InnerContainer = styled.View`
  margin-left: 20px;

  flex: 1;
`;

export const OfferPriceAndBadge3Container = styled.View`
  flex-direction: row;
`;

export const OfferPriceAndBadge2Container = styled.View`
  flex-direction: row;
`;

export const PromotionalMsgContainer = styled.View`
  flex-direction: row;
  margin-left: ${(props) => props.theme.spacing.LAYOUT_SPACING.MED};
`;

export const ModalButton = styled.View`
  align-items: flex-start;
`;

export const AfterPayWrapper = styled.View`
  justify-content: center;
  align-items: flex-start;
  width: 100%;
  margin-bottom: 8px;
  flex-wrap: wrap;
`;

export default styles;
