// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module PickupSkuSelectionFormContainer
 * @desc Component to display step 1 of PickUp in Store Modal and fill sku details
 * doesn't open when sku is resolved
 */

import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import withStyles from '../../../../../hoc/withStyles';
import styles from '../../../../QuickViewModal/molecules/ProductCustomizeFormPart/styles/ProductCustomizeFormPart.style';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import PickupProductFormPart from '../../PickupProductFormPart';
import PickupSkuSelectionForm from '../views/PickupSkuSelectionForm.view';
import { routerPush } from '../../../../../../../utils';
import {
  getMapSliceForColorProductId,
  getMapSliceForColor,
  getPrices,
} from '../../../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';

class PickupSkuSelectionFormContainer extends React.Component {
  constructor(props) {
    super(props);
    const { currentProduct, initialValues } = this.props;
    this.formValues = initialValues;
    this.colorFitsSizesMap = currentProduct && currentProduct.colorFitsSizesMap;
    const generalProductId =
      currentProduct && getMapSliceForColor(this.colorFitsSizesMap, initialValues.color);
    this.generalProductId = generalProductId && generalProductId.colorDisplayId;
    this.state = {
      currentColorEntry: getMapSliceForColorProductId(
        this.colorFitsSizesMap,
        this.generalProductId
      ),
      selectedColor: initialValues.color,
      selectedSize: initialValues.Size,
      selectedFit: initialValues.Fit,
    };
  }

  componentDidUpdate() {
    const { currentProduct, productLoading } = this.props;
    const { selectedColor } = this.state;
    if (
      !productLoading &&
      !this.getSelectedImage(currentProduct, selectedColor) &&
      !isEmpty(currentProduct)
    ) {
      this.updateMultipackChange(currentProduct);
    }
  }

  getSelectedImage = (currentProduct, selectedColor) => {
    return selectedColor
      ? currentProduct &&
          currentProduct.imagesByColor &&
          currentProduct.imagesByColor[selectedColor] &&
          currentProduct.imagesByColor[selectedColor].basicImageUrl
      : null;
  };

  onChangeColor = (e, selectedSize, selectedFit, selectedQuantity) => {
    this.generalProductId =
      this.colorFitsSizesMap && getMapSliceForColor(this.colorFitsSizesMap, e);
    this.generalProductId = this.generalProductId && this.generalProductId.colorDisplayId;
    this.setState({
      currentColorEntry: getMapSliceForColor(this.colorFitsSizesMap, e),
      selectedColor: e,
    });

    this.formValues = {
      ...this.formValues,
      Fit: selectedFit,
      Size: selectedSize,
      color: e,
      Quantity: selectedQuantity,
    };
  };

  onChangeSize = (e, selectedSize) => {
    if (typeof selectedSize !== 'object') {
      this.formValues = {
        ...this.formValues,
        Size: selectedSize,
      };
      this.setState({
        selectedSize,
      });
    }
  };

  updateQuantityChange = (selectedQuantity) => {
    this.formValues = {
      ...this.formValues,
      Quantity: selectedQuantity,
    };
  };

  onFitChange = (fit) => {
    this.formValues = {
      ...this.formValues,
      Fit: fit,
    };
    this.setState({
      selectedFit: fit,
    });
  };

  navigateToPDP = (e, pdpToPath, currentColorPdpUrl) => {
    e.preventDefault();
    const { onCloseClick, alternateBrand } = this.props;
    const currentColorPdpUrlNew = alternateBrand
      ? `${currentColorPdpUrl}$brand=${alternateBrand}`
      : `${currentColorPdpUrl}`;
    const pdpToPathNew = alternateBrand ? `${pdpToPath}$brand=${alternateBrand}` : `${pdpToPath}`;
    routerPush(pdpToPathNew, currentColorPdpUrlNew);
    onCloseClick();
  };

  updateMultipackChange(currentProduct) {
    this.colorFitsSizesMap = currentProduct && currentProduct.colorFitsSizesMap;
    const initialProduct = Array.isArray(this.colorFitsSizesMap) && this.colorFitsSizesMap[0];
    const initialColor = initialProduct ? initialProduct.color.name : '';
    const initialFit = initialProduct && initialProduct.fits ? initialProduct.fits[0].fitName : '';
    const generalProductId =
      currentProduct && getMapSliceForColor(this.colorFitsSizesMap, initialColor);
    this.generalProductId = generalProductId && generalProductId.colorDisplayId;
    this.formValues = {
      ...this.formValues,
      Fit: initialFit,
      Quantity: 1,
      color: initialColor,
      Size: '',
    };
    this.setState({
      currentColorEntry: getMapSliceForColorProductId(
        this.colorFitsSizesMap,
        this.generalProductId
      ),
      selectedColor: initialColor,
      selectedSize: '',
      selectedFit: initialFit,
    });
  }

  render() {
    const {
      initialValues,
      colorFitSizeDisplayNames,
      isCanada,
      isPlcc,
      currencySymbol,
      isInternationalShipping,
      name,
      isShowExtendedSizesNotification,
      isSkuResolved,
      isPreferredStoreError,
      promotionalMessage,
      promotionalPLCCMessage,
      onEditSku,
      isPickUpWarningModal,
      currentProduct,
      currencyAttributes,
      toastMessage,
      updateAppTypeHandler,
      isSKUValidated,
      getSKUValidated,
      accessibilityLabels,
      fromPage,
      onPickUpOpenClick,
      hideMultipackPills,
      isPickupModalOpen,
      availableTCPmapNewStyleId,
      setInitialTCPStyleQty,
      isAfterPayEnabled,
      newQVEnabled,
      primaryBrand,
      productLoading,
      initialValuesFromBagPage,
      fromBagPage,
      alternateBrand,
      isFromChangeStore,
      productInfo,
    } = this.props;
    const { currentColorEntry, selectedColor, selectedSize, selectedFit } = this.state;
    const prices = getPrices(currentProduct, selectedColor, selectedFit, selectedSize);
    const imageUrl = this.getSelectedImage(currentProduct, selectedColor);

    if (fromBagPage) {
      const { itemBrand } = initialValuesFromBagPage;
      this.formValues = {
        ...this.formValues,
        itemBrand,
      };
    }

    const { listPrice, offerPrice } = prices || {};

    return isSkuResolved && !isFromChangeStore ? (
      <PickupProductFormPart
        colorFitSizeDisplayNames={colorFitSizeDisplayNames}
        colorFitsSizesMap={this.colorFitsSizesMap}
        name={name}
        isShowExtendedSizesNotification={isShowExtendedSizesNotification}
        isPreferredStoreError={isPreferredStoreError}
        onEditSku={onEditSku}
        promotionalMessage={promotionalMessage}
        initialValues={initialValues}
        promotionalPLCCMessage={promotionalPLCCMessage}
        isPickUpWarningModal={isPickUpWarningModal}
        isCanada={isCanada}
        isHasPlcc={isPlcc}
        currencySymbol={currencySymbol}
        isInternationalShipping={isInternationalShipping}
        imagePath={imageUrl}
        listPrice={listPrice}
        offerPrice={offerPrice}
        accessibilityLabels={accessibilityLabels}
        currencyAttributes={currencyAttributes}
        fromPage={fromPage}
        onPickUpOpenClick={onPickUpOpenClick}
        isAfterPayEnabled={isAfterPayEnabled}
        newQVEnabled={newQVEnabled}
        primaryBrand={primaryBrand}
        productLoading={productLoading}
        initialValuesFromBagPage={initialValuesFromBagPage}
        fromBagPage={fromBagPage}
        alternateBrand={alternateBrand}
      />
    ) : (
      <PickupSkuSelectionForm
        {...this.props}
        prices={prices}
        selectedColor={selectedColor}
        generalProductId={this.generalProductId}
        navigateToPDP={this.navigateToPDP}
        onFitChange={this.onFitChange}
        onChangeColor={this.onChangeColor}
        onChangeSize={this.onChangeSize}
        imageUrl={imageUrl}
        currentColorEntry={currentColorEntry}
        currencyAttributes={currencyAttributes}
        initialValues={this.formValues}
        toastMessage={toastMessage}
        updateAppTypeHandler={updateAppTypeHandler}
        isSKUValidated={isSKUValidated}
        accessibilityLabels={accessibilityLabels}
        getSKUValidated={getSKUValidated}
        fromPage={fromPage}
        onPickUpOpenClick={onPickUpOpenClick}
        hideMultipackPills={hideMultipackPills}
        isModalOpen={isPickupModalOpen}
        availableTCPmapNewStyleId={availableTCPmapNewStyleId}
        setInitialTCPStyleQty={setInitialTCPStyleQty}
        isAfterPayEnabled={isAfterPayEnabled}
        newQVEnabled={newQVEnabled}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
        isFromChangeStore={isFromChangeStore}
        fromBagPage={fromBagPage}
        productInfo={productInfo}
      />
    );
  }
}

PickupSkuSelectionFormContainer.propTypes = {
  /** labels for selection fields */
  colorFitSizeDisplayNames: PropTypes.shape({
    /** label for color selection field */
    color: PropTypes.string,
    /** label for fit selection field */
    fit: PropTypes.string,
    /** label for size selection field */
    size: PropTypes.string,
  }),

  colorFitsSizesMap: PropTypes.arrayOf(PropTypes.object).isRequired,

  /** seed values for the form */
  initialValues: PropTypes.shape({
    /** user's preselected color id from parent instance */
    color: PropTypes.string,
    /** user's preselected fit id from parent instance */
    fit: PropTypes.string,
    /** user's preselected size id from parent instance */
    size: PropTypes.string,
    /** user's preselected quantity from parent instance */
    quantity: PropTypes.number,
  }).isRequired,

  productInfo: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,
  isCanada: PropTypes.bool.isRequired,
  /** This is used to display the correct currency symbol */
  currencySymbol: PropTypes.string.isRequired,
  /* We are available to know if is an international shipping */
  isInternationalShipping: PropTypes.bool.isRequired,

  isPlcc: PropTypes.bool.isRequired,

  currentProduct: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,

  currency: PropTypes.string,
  currencyAttributes: PropTypes.shape({}).isRequired,

  prices: PropTypes.shape({
    listPrice: PropTypes.number.isRequired,
    offerPrice: PropTypes.number,
  }).isRequired,

  isHasPlcc: PropTypes.bool,

  className: PropTypes.string,

  name: PropTypes.string,

  isShowExtendedSizesNotification: PropTypes.bool,

  isSkuResolved: PropTypes.bool,

  isPreferredStoreError: PropTypes.bool,

  promotionalMessage: PropTypes.string,

  promotionalPLCCMessage: PropTypes.string,

  onEditSku: PropTypes.bool,

  isPickUpWarningModal: PropTypes.bool,

  onCloseClick: PropTypes.func,
  toastMessage: PropTypes.func,
  updateAppTypeHandler: PropTypes.func.isRequired,
  isSKUValidated: PropTypes.bool,
  accessibilityLabels: PropTypes.shape({}),
  getSKUValidated: PropTypes.func,
  fromPage: PropTypes.string,
  onPickUpOpenClick: PropTypes.func,
  hideMultipackPills: PropTypes.bool,
  isPickupModalOpen: PropTypes.bool,
  availableTCPmapNewStyleId: PropTypes.shape([]),
  setInitialTCPStyleQty: PropTypes.string,
  isAfterPayEnabled: PropTypes.bool,
  newQVEnabled: PropTypes.bool,
  fromBagPage: PropTypes.bool,
  isFromChangeStore: PropTypes.bool,
};

PickupSkuSelectionFormContainer.defaultProps = {
  colorFitSizeDisplayNames: null,
  currency: 'USD',
  isHasPlcc: false,
  className: '',
  name: '',
  isShowExtendedSizesNotification: false,
  isSkuResolved: false,
  isPreferredStoreError: false,
  promotionalMessage: '',
  promotionalPLCCMessage: '',
  onEditSku: false,
  isPickUpWarningModal: false,
  onCloseClick: () => {},
  toastMessage: () => {},
  isSKUValidated: false,
  accessibilityLabels: {},
  getSKUValidated: () => {},
  fromPage: '',
  onPickUpOpenClick: () => {},
  hideMultipackPills: false,
  isPickupModalOpen: true,
  availableTCPmapNewStyleId: [],
  setInitialTCPStyleQty: '',
  isAfterPayEnabled: false,
  newQVEnabled: false,
  fromBagPage: false,
  isFromChangeStore: false,
};

export default withStyles(PickupSkuSelectionFormContainer, styles);
