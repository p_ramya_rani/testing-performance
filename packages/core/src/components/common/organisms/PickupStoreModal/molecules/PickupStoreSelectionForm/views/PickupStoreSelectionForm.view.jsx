// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { reduxForm, Field, propTypes as reduxFormPropTypes } from 'redux-form';
import { isAndroidWeb } from '@tcp/core/src/utils';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import Spinner from '../../../atoms/Spinner';
import BodyCopy from '../../../../../atoms/BodyCopy';
import { PICKUP_LABELS } from '../../../PickUpStoreModal.constants';
import PickupStoreListContainer from '../../PickupStoreList';
import PickupStoreListItem from '../../PickupStoreListItem';
import withStyles from '../../../../../hoc/withStyles';
import PickupStoreSelectionFormStyle from '../styles/PickupStoreSelectionForm.style';
import { TextBox, SelectBox, Row, Col, Button } from '../../../../../atoms';

export const DISTANCES_MAP_PROP_TYPE = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
  })
);

class _PickupStoreSelectionForm extends React.Component {
  static propTypes = {
    /** The map of distances options to select the radius of search */
    distancesMap: DISTANCES_MAP_PROP_TYPE.isRequired,
    /** flags if to search in current cart stores (if true)
     * or offer user to enter location to look for nearby stores (if false)
     */
    isSearchOnlyInCartStores: PropTypes.bool.isRequired,

    /** We need to differentiate if Bopis Modal is open from cart or other place to change select item button's message (DT-27100) */
    isShoppingBag: PropTypes.bool.isRequired,

    // determines if variation is warning modal
    isPickUpWarningModal: PropTypes.bool.isRequired,

    /** Props passed by the redux-form Form HOC. */
    ...reduxFormPropTypes,

    /** Check to allow display of warning msg */
    /** global switches for BOSS and BOPIS */
    isBossEnabled: PropTypes.bool.isRequired,
    isBopisEnabled: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    const { onSearch, openRestrictedModalForBopis, isSkuResolved } = this.props;
    if (openRestrictedModalForBopis || isSkuResolved) {
      onSearch();
    }
  }

  componentDidUpdate() {
    const { prePopulateZipCodeAndSearch, handleSubmit, change, isStoreSearching } = this.props;
    prePopulateZipCodeAndSearch(handleSubmit, change);
    // FIX: fix specifically for android devices, where when we are searching for the stores, the list is scrolled to the bottom
    if (isStoreSearching && isAndroidWeb()) {
      // y co-ordinate is set to 200 to ensure that the screen is scrolled to a specific position
      document.querySelector('.TCPModal__InnerContent').scrollTo(0, 200);
    }
  }

  displayStoreListItems({ isBossCtaEnabled, buttonLabel, sameStore }) {
    const {
      isShoppingBag,
      submitting,
      isSearchOnlyInCartStores,
      onCloseClick,
      addToCartError,
      isBopisCtaEnabled,
      updateCartItemStore,
      isBossEnabled,
      allowBossStoreSearch,
      defaultStore,
      bopisChangeStore,
      isBopisEnabled,
      isGiftCard,
      cartBopisStoresList,
      handleAddTobag,
      handlePickupRadioBtn,
      handleUpdatePickUpItem,
      selectedStoreId,
      isBossSelected,
      isShowMessage,
      currentProduct,
      setFavoriteStore,
      getDefaultStore,
      pageData,
      storeSearchCriteria,
      storeSearchDistance,
      fromPage,
      newQVEnabled,
    } = this.props;
    return submitting ? (
      <Spinner />
    ) : (
      <PickupStoreListContainer
        setFavoriteStore={setFavoriteStore}
        getDefaultStore={getDefaultStore}
        isShoppingBag={isShoppingBag}
        currentProduct={currentProduct}
        onStoreSelect={handleAddTobag}
        onStoreUpdate={handleUpdatePickUpItem}
        onPickupRadioBtnToggle={handlePickupRadioBtn}
        isResultOfSearchingInCartStores={isSearchOnlyInCartStores}
        onCancel={onCloseClick}
        sameStore={sameStore}
        selectedStoreId={selectedStoreId}
        isBossSelected={isBossSelected}
        addToCartError={isShowMessage ? addToCartError : ''}
        isBopisCtaEnabled={isBopisCtaEnabled}
        isBossCtaEnabled={isBossCtaEnabled}
        isBossEnabled={isBossEnabled}
        isBopisEnabled={isBopisEnabled}
        allowBossStoreSearch={allowBossStoreSearch}
        bopisChangeStore={bopisChangeStore}
        updateCartItemStore={updateCartItemStore}
        buttonLabel={buttonLabel}
        isGiftCard={isGiftCard}
        defaultStore={defaultStore}
        cartBopisStoresList={cartBopisStoresList}
        pageData={pageData}
        storeSearchCriteria={storeSearchCriteria}
        storeSearchDistance={storeSearchDistance}
        fromPage={fromPage}
        newQVEnabled={newQVEnabled}
      />
    );
  }

  displayErrorCopy() {
    const { error, onCloseClick } = this.props;
    return error ? (
      <div className="error-box-bopis">
        <BodyCopy className="error-message">{error}</BodyCopy>
        <Button onClick={onCloseClick} type="button" className="button-cancel">
          Cancel
        </Button>
      </div>
    ) : null;
  }

  displayStoreSearchForm(showStoreSearching) {
    const {
      distancesMap,
      pristine,
      submitting,
      className,
      storeSearchError,
      isSkuResolved,
      newQVEnabled,
    } = this.props;
    const colSizeConfig = newQVEnabled
      ? { small: 4, medium: 6, large: 12 }
      : { small: 6, medium: 4, large: 6 };
    return showStoreSearching ? (
      <div className={className}>
        <BodyCopy
          className="find-store-label"
          fontFamily="secondary"
          fontSize="fs16"
          fontWeight="semibold"
        >
          {PICKUP_LABELS.FIND_STORE}
        </BodyCopy>
        <Row fullBleed>
          {!newQVEnabled && (
            <Col colSize={colSizeConfig}>
              <Field
                name="addressLocation"
                id="addressLocation"
                component={TextBox}
                className="zipcode-field"
                placeholder="Zip or City, State"
                enableSuccessCheck={false}
              />
            </Col>
          )}
          {!newQVEnabled && (
            <Col colSize={{ small: 2, medium: 2, large: 3 }}>
              <Field
                name="distance"
                component={SelectBox}
                title="Distance"
                options={distancesMap}
                className="distance-input"
              />
            </Col>
          )}
          {!newQVEnabled && (
            <Col colSize={{ small: 6, medium: 2, large: 3 }} className="button-wrapper">
              <Button
                buttonVariation="fixed-width"
                fill="BLUE"
                type="submit"
                title="search"
                className="button-search-bopis"
                disabled={pristine || submitting}
              >
                Search
              </Button>
            </Col>
          )}
        </Row>
        {newQVEnabled && (
          <div fullBleed className="newfindstore">
            <div colSize={colSizeConfig} className="newzipcode-field">
              <Field
                name="addressLocation"
                id="addressLocation"
                component={TextBox}
                className="zipcode-field"
                placeholder="Zip or City, State"
                enableSuccessCheck={false}
              />
            </div>
            <div colSize={{ small: 2, medium: 2, large: 3 }} className="newdistance-input">
              <Field
                name="distance"
                component={SelectBox}
                title="Distance"
                options={distancesMap}
                className="distance-input"
              />
            </div>
          </div>
        )}
        {newQVEnabled && (
          <Row fullBleed>
            <Col colSize={{ small: 6, medium: 4, large: 12 }} className="button-wrapper">
              <Button
                buttonVariation="fixed-width"
                fill="BLUE"
                type="submit"
                title="search"
                className="button-search-bopis"
                disabled={pristine || submitting}
              >
                Search
              </Button>
            </Col>
          </Row>
        )}
        {isSkuResolved && (
          <BodyCopy
            className="storeSearchError"
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="extrabold"
            textAlign="center"
          >
            {storeSearchError}
          </BodyCopy>
        )}
      </div>
    ) : null;
  }

  displayFavStore({
    storeLimitReached,
    prefStoreWithData,
    sameStore,
    buttonLabel,
    isBossCtaEnabled,
  }) {
    const {
      isShoppingBag,
      addToCartError,
      isBopisCtaEnabled,
      updateCartItemStore,
      isBossEnabled,
      isBopisEnabled,
      isGiftCard,
      preferredStore,
      handleAddTobag,
      handlePickupRadioBtn,
      handleUpdatePickUpItem,
      selectedStoreId,
      isBossSelected,
      isShowMessage,
      isGetUserStoresLoaded,
      getIsBopisAvailable,
      currentProduct,
      pageNameProp,
      storeSearchCriteria,
      storeSearchDistance,
      setClickAnalyticsData,
      trackClick,
      setFavoriteStore,
      getDefaultStore,
      pageData,
      fromPage,
      newQVEnabled,
      isBopisPickup,
      quickViewLabels,
    } = this.props;
    return (
      !storeLimitReached &&
      isGetUserStoresLoaded &&
      preferredStore &&
      prefStoreWithData && (
        <div className="favorite-store-box">
          <PickupStoreListItem
            sameStore={sameStore}
            pageNameProp={pageNameProp}
            isShoppingBag={isShoppingBag}
            store={preferredStore}
            currentProduct={currentProduct}
            onStoreSelect={handleAddTobag}
            onStoreUpdate={handleUpdatePickUpItem}
            onPickupRadioBtnToggle={handlePickupRadioBtn}
            isBopisSelected={
              newQVEnabled
                ? isBopisPickup
                : preferredStore.basicInfo.id === selectedStoreId && !isBossSelected
            }
            isBossSelected={
              newQVEnabled
                ? !isBopisPickup
                : preferredStore.basicInfo.id === selectedStoreId && isBossSelected
            }
            selectedStoreId={selectedStoreId}
            isBopisAvailable={getIsBopisAvailable()}
            isBossAvailable={preferredStore.storeBossInfo.isBossEligible}
            storeBossInfo={preferredStore.storeBossInfo}
            addToCartError={isShowMessage ? addToCartError : ''}
            isBopisCtaEnabled={isBopisCtaEnabled && isBopisEnabled}
            isBossCtaEnabled={isBossCtaEnabled && isBossEnabled}
            updateCartItemStore={updateCartItemStore}
            buttonLabel={buttonLabel}
            isGiftCard={isGiftCard}
            storeSearchCriteria={storeSearchCriteria}
            storeSearchDistance={storeSearchDistance}
            setClickAnalyticsData={setClickAnalyticsData}
            trackClick={trackClick}
            setFavoriteStore={setFavoriteStore}
            getDefaultStore={getDefaultStore}
            pageData={pageData}
            fromPage={fromPage}
            newQVEnabled={newQVEnabled}
            quickViewLabels={quickViewLabels}
          />
        </div>
      )
    );
  }

  displayStoreSearchComp() {
    const {
      deriveStoreSearchAttributes,
      deriveBossCtaEnabled,
      updateCartItemStore,
      isSkuResolved,
      renderVariationText,
    } = this.props;
    const { storeLimitReached, sameStore, showStoreSearching } = deriveStoreSearchAttributes();
    const isBossCtaEnabled = deriveBossCtaEnabled();
    const buttonLabel = updateCartItemStore ? PICKUP_LABELS.UPDATE : PICKUP_LABELS.ADD_TO_BAG;

    return (
      <React.Fragment>
        {isSkuResolved && (
          <BodyCopy fontFamily="secondary" fontSize="fs14">
            {renderVariationText(storeLimitReached, sameStore)}
          </BodyCopy>
        )}
        {this.displayStoreSearchForm(showStoreSearching)}
        {
          <React.Fragment>
            {isSkuResolved &&
              this.displayStoreListItems({
                isBossCtaEnabled,
                buttonLabel,
                sameStore,
              })}
            {isSkuResolved && this.displayErrorCopy()}
          </React.Fragment>
        }
      </React.Fragment>
    );
  }

  render() {
    const {
      className,
      handleSubmit,
      isPickUpWarningModal,
      onSearch,
      onCloseClick,
      getSKUValidated,
      PickupSkuFormValues,
    } = this.props;
    const skuResolved = Object.values(PickupSkuFormValues).includes('');
    return (
      <form
        onSubmit={handleSubmit((formValues) => {
          if (!skuResolved) {
            getSKUValidated(false);
            onSearch(formValues);
          } else {
            getSKUValidated(true);
          }
        })}
      >
        {isPickUpWarningModal && (
          <div className={className}>
            <div className="alignCenter padding-top">
              <BodyCopy
                className="item-unavailable"
                fontFamily="secondary"
                fontWeight="semibold"
                fontSize="fs14"
              >
                {PICKUP_LABELS.ITEM_UNAVAILABLE}
              </BodyCopy>
            </div>
            <div className="padding-top alignCenter">
              <Button
                onClick={onCloseClick}
                buttonVariation="variable-width"
                type="button"
                fill="BLUE"
                className="button-cancel"
              >
                Cancel
              </Button>
            </div>
          </div>
        )}
        {!isPickUpWarningModal && this.displayStoreSearchComp()}
      </form>
    );
  }
}

const defaultValidation = getStandardConfig(
  [{ addressLocation: 'addressLocation' }, { distance: 'distance' }],
  { stopOnFirstError: true }
);

const validateMethod = createValidateMethod(defaultValidation);

const PickupStoreSelectionForm = reduxForm({
  form: 'pickupSearchStoresForm',
  ...validateMethod,
  keepDirtyOnReinitialize: true, // [https://github.com/erikras/redux-form/issues/3690] redux-forms 7.2.0 causes bug that forms will reInit after mount setting changed values back to init values
  touchOnChange: true, // to show validation error messageas even if user did not touch the fields
  touchOnBlur: false, // to avoid hidding the search results on blur of any field without changes
})(_PickupStoreSelectionForm);
PickupStoreSelectionForm.displayName = 'PickupStoreSelection';

export default withStyles(PickupStoreSelectionForm, PickupStoreSelectionFormStyle);
