// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import InputCheckbox from '../../../../../atoms/InputCheckbox';

export const StyledInputCheckbox = styled(InputCheckbox)`
  p {
    margin: 0 3px;
    padding: 3px 0 0;
  }
`;

export const InputBoxWrapper = ``;

export default { StyledInputCheckbox, InputBoxWrapper };
