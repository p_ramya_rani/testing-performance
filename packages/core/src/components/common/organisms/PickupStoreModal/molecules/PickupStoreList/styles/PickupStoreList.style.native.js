// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import InputCheckbox from '../../../../../atoms/InputCheckbox';

export const StyledInputCheckbox = styled(InputCheckbox)`
  margin: 0 7px;
  padding: 4px 0 0;
`;

export const InputBoxWrapper = styled.View`
  margin: 0 0 0 8px;
`;

export default { StyledInputCheckbox, InputBoxWrapper };
