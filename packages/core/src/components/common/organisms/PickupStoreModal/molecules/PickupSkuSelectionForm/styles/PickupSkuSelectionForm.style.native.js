// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

const PickUpSkUSectionContainer = styled.View`
  margin: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XS} 14px;
`;

const ImageWrapper = styled.View`
  margin-right: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XS};
  width: 164px;
  background-color: #f7971f;
  overflow: hidden;
`;
const ProductSummaryContainer = styled.View`
  flex-direction: row;
`;
const OfferPriceAndBadge3Container = styled.View`
  flex: 0.4;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;
const ProductDetailSummary = styled.View`
  flex-wrap: wrap;
  flex: 1;
`;

const AfterPayWrapper = styled.View`
  justify-content: center;
  align-items: flex-start;
  width: 100%;
  flex-wrap: wrap;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.XS} 0;
`;
const OfferPriceContainer = styled.View`
  flex: ${(props) => (props.isFromChangeStore ? 0 : 0.4)};
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;
const ProductContainer = styled.View`
  flex-wrap: wrap;
  flex: 1;
  flex-direction: row;
`;
export {
  PickUpSkUSectionContainer,
  ImageWrapper,
  ProductSummaryContainer,
  ProductDetailSummary,
  OfferPriceAndBadge3Container,
  AfterPayWrapper,
  OfferPriceContainer,
  ProductContainer,
};
