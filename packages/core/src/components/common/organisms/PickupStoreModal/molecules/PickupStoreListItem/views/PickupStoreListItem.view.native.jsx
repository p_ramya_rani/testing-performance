/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import { PropTypes } from 'prop-types';
import { STORE_SUMMARY_PROP_TYPES } from '@tcp/core/src/components/common/organisms/PickupStoreModal/PickUpStoreModal.proptypes';
import { Anchor, BodyCopy, Button } from '@tcp/core/src/components/common/atoms';
import ReactTooltip from '@tcp/core/src/components/common/atoms/ReactToolTip';
import PickupRadioBtn from '@tcp/core/src/components/common/organisms/PickupStoreModal/atoms/PickupRadioButton';
import { parseDate } from '@tcp/core/src/utils/parseDate';
import { parseBoolean } from '@tcp/core/src/utils/badge.util';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { formatPhnNumber, mapHandler } from '@tcp/core/src/utils/index.native';
import {
  getProductStatusAndAvailability,
  getStoreHoursClosingTiming,
} from '@tcp/core/src/utils/utils';
import {
  STORE_DETAILS_LABELS,
  ITEM_AVAILABILITY_MESSAGES,
  BOPIS_ITEM_AVAILABILITY,
  PICKUP_CTA_LABELS,
  PICKUP_RADIO_BTN_NAME,
  PICKUP_LABELS,
} from '../../../PickUpStoreModal.constants';
import {
  toTimeString,
  capitalize,
  getTranslateDateInformation,
  getAPIConfig,
} from '../../../../../../../utils';
import {
  FavStoreLabel,
  FavStoreIcon,
  StoreInfoWrapper,
  StoreListItemWrapper,
  PickupButtonsWrapper,
  StoreUnavailable,
  PickupCTAWrapper,
  PickupRadioBtnWrapper,
  StoreDetailsAnchorWrapper,
  TooltipContentWrapper,
  TooltipWrapper,
  addToCartErrorStyle,
  StoreListItemWrapperIsFromStore,
  AvaialbilityWrapper,
  StoreInfoWrapperIsFromStore,
  PickupRadioBtnWrapperIsFromStore,
  StoreWrapperIsFromStore,
  AnchorWrapper,
  Wrapper,
  StoreWrapper,
  ViewWrapper,
  StoreWrapperHeader,
  TooltipWrapperFromStore,
} from '../styles/PickupStoreListItem.style.native';
import { BodyCopyWithSpacing } from '../../../../../atoms/styledWrapper';
import Image from '../../../../../atoms/Image';

const storeLocator = require('../../../../../../../../../mobileapp/src/assets/images/store_locator.png');
const favStoreImage = require('../../../../../../../../../mobileapp/src/assets/images/icon-fav-store.png');

const getTooltipContent = (basicInfo, address, storeClosingTimeToday, storeClosingTimeTomorrow) => {
  const storeName = capitalize(basicInfo.storeName);
  const addressLine1 = capitalize(address.addressLine1);
  const city = capitalize(address.city);
  const { CLOSED_TODAY, CLOSING_TODAY, CLOSING_TOMORROW, CLOSED_TOMORROW } = STORE_DETAILS_LABELS;
  const phoneNum = formatPhnNumber(basicInfo.phone);
  return (
    <TooltipContentWrapper>
      <BodyCopy
        fontFamily="secondary"
        color="text.primary"
        fontWeight="semibold"
        fontSize="fs16"
        text={storeName}
      />
      <BodyCopy
        fontFamily="secondary"
        color="text.secondary"
        fontSize="fs12"
        text={`${addressLine1} `}
      />
      <BodyCopy
        fontFamily="secondary"
        color="text.secondary"
        fontSize="fs12"
        text={`${city}, ${address.state} ${basicInfo.address.zipCode}`}
      />
      <BodyCopy
        fontFamily="secondary"
        color="text.secondary"
        fontSize="fs12"
        text={`${phoneNum} \n`}
      />
      {storeClosingTimeToday ? (
        <BodyCopy
          fontFamily="secondary"
          color="text.secondary"
          fontSize="fs12"
          text={`${CLOSING_TODAY} ${storeClosingTimeToday}`}
        />
      ) : (
        <BodyCopy
          fontFamily="secondary"
          color="text.secondary"
          fontSize="fs12"
          text={CLOSED_TODAY}
        />
      )}

      {storeClosingTimeTomorrow ? (
        <BodyCopy
          fontFamily="secondary"
          color="text.secondary"
          fontSize="fs12"
          text={`${CLOSING_TOMORROW} ${storeClosingTimeTomorrow}`}
        />
      ) : (
        <BodyCopy
          fontFamily="secondary"
          color="text.secondary"
          fontSize="fs12"
          text={CLOSED_TOMORROW}
        />
      )}
    </TooltipContentWrapper>
  );
};

const displayStoreDetailsAnchor = (
  basicInfo,
  address,
  storeClosingTimeToday,
  storeClosingTimeTomorrow
) => {
  const tooltipContent = getTooltipContent(
    basicInfo,
    address,
    storeClosingTimeToday,
    storeClosingTimeTomorrow
  );
  const StoreDetailsAnchorStyled = withStyles(BodyCopyWithSpacing, StoreDetailsAnchorWrapper);
  const StoreDetailsAnchor = (
    <StoreDetailsAnchorStyled
      fontFamily="secondary"
      color="text.primary"
      fontSize="fs12"
      text={STORE_DETAILS_LABELS.STORE_DETAILS}
    />
  );
  return (
    <TooltipWrapper>
      <ReactTooltip withOverlay={false} popover={tooltipContent} width={230} height={170}>
        {StoreDetailsAnchor}
      </ReactTooltip>
    </TooltipWrapper>
  );
};
const displayStoreDetailsAnchorFromStore = (
  basicInfo,
  address,
  storeClosingTimeToday,
  storeClosingTimeTomorrow
) => {
  const tooltipContent = getTooltipContent(
    basicInfo,
    address,
    storeClosingTimeToday,
    storeClosingTimeTomorrow
  );
  const StoreDetailsAnchor = (
    <BodyCopy
      mobilefontFamily="secondary"
      fontWeight="regular"
      fontSize="fs14"
      color="gray.900"
      text={STORE_DETAILS_LABELS.STORE_DETAILS}
      textDecoration="underline"
    />
  );
  return (
    <TooltipWrapperFromStore>
      <ReactTooltip withOverlay={false} popover={tooltipContent} width={230} height={170}>
        {StoreDetailsAnchor}
      </ReactTooltip>
    </TooltipWrapperFromStore>
  );
};
const displayFavoriteStore = (basicInfo, label) => {
  return basicInfo.isDefault ? (
    <FavStoreLabel>
      <FavStoreIcon>
        <Image source={storeLocator} height={14} width={15} dataLocator="pdp_store_marker_icon" />
      </FavStoreIcon>
      <BodyCopy
        fontFamily="secondary"
        color="text.secondary"
        fontWeight="extrabold"
        fontSize="fs12"
        text={`${label} `}
      />
    </FavStoreLabel>
  ) : null;
};

const displayFavoriteStoreIsFromChangeStore = (basicInfo, text) => {
  return basicInfo.isDefault ? (
    <Wrapper>
      <FavStoreIcon>
        <Image source={favStoreImage} height={18} width={18} dataLocator="pdp_store_marker_icon" />
      </FavStoreIcon>
      <BodyCopy
        fontFamily="secondary"
        color="gray.900"
        fontWeight="semibold"
        fontSize="fs12"
        text={text}
      />
    </Wrapper>
  ) : null;
};

const displayAddToCartError = (addToCartError) => {
  const AddToCartErrorStyled = withStyles(BodyCopyWithSpacing, addToCartErrorStyle);
  return (
    <AddToCartErrorStyled
      fontFamily="secondary"
      fontSize="fs10"
      text={addToCartError}
      color="red.500"
    />
  );
};
const displayStoreUnavailable = (showBopisCTA, showBossCTA) => {
  const { STORE_UNAVAILABLE } = STORE_DETAILS_LABELS;
  return !showBopisCTA && !showBossCTA ? (
    <StoreUnavailable>
      <BodyCopy
        fontFamily="secondary"
        color="text.primary"
        fontSize="fs14"
        text={STORE_UNAVAILABLE}
      />
    </StoreUnavailable>
  ) : null;
};

const displayStoreAddress = (address) => {
  return address && address.addressLine1 ? (
    <BodyCopy
      fontFamily="secondary"
      color="text.primary"
      fontSize="fs12"
      text={capitalize(address.addressLine1)}
    />
  ) : null;
};

const displayDistance = (distance) => {
  return distance ? (
    <BodyCopy
      fontFamily="secondary"
      color="text.primary"
      fontSize="fs12"
      text={`${distance} mi.`}
    />
  ) : null;
};

const displayStoreTitle = (basicInfo) => {
  const storeName = basicInfo ? capitalize(basicInfo.storeName) : '';
  return (
    <BodyCopyWithSpacing
      fontFamily="secondary"
      fontWeight="semibold"
      fontSize="fs16"
      text={storeName}
      spacingStyles="margin-bottom-XXS margin-top-XS"
    />
  );
};
const REGULAR_HOURS_PROP_TYPE = PropTypes.arrayOf(
  PropTypes.shape({
    dayName: PropTypes.string.isRequired,
    openIntervals: PropTypes.arrayOf(
      PropTypes.shape({
        fromHour: PropTypes.string.isRequired,
        toHour: PropTypes.string.isRequired,
      })
    ).isRequired,
    isClosed: PropTypes.bool.isRequired,
  })
);

class PickupStoreListItem extends React.Component {
  static propTypes = {
    /** Error message when add to cart */
    addToCartError: PropTypes.string.isRequired,
    /** The whole information of the store. */
    store: PropTypes.shape({
      ...STORE_SUMMARY_PROP_TYPES,
      /** the availability status of the searched for cart item in this store */
      basicInfo: PropTypes.shape({
        /** store id identifier */
        id: PropTypes.string,
        address: PropTypes.string,
        storeName: PropTypes.string,
      }),
      productAvailability: PropTypes.shape({
        status: PropTypes.oneOf(
          Object.keys(BOPIS_ITEM_AVAILABILITY).map((key) => BOPIS_ITEM_AVAILABILITY[key])
        ).isRequired,
      }).isRequired,
      hours: PropTypes.shape({
        /**
         * Array of opening and closing hours in which the store is open on convined (both regular and holidays)
         * days.
         */
        regularHours: REGULAR_HOURS_PROP_TYPE,
      }),
      distance: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      pickupType: PropTypes.shape({
        isStoreBopisSelected: PropTypes.bool,
        isStoreBossSelected: PropTypes.bool,
      }),
    }).isRequired,

    /** flag which describes wheter the item add to bag will be disabled or not */
    isBopisAvailable: PropTypes.bool.isRequired,
    /**
     * Function to call when a store is selected. The called function will
     * receive one parameter, the id of the clicked store.
     */
    onStoreSelect: PropTypes.func.isRequired,
    /** carries the boss information of the store */
    storeBossInfo: PropTypes.shape({
      isBossEligible: PropTypes.string,
      startDate: PropTypes.string,
      endDate: PropTypes.string,
    }).isRequired,
    /** boolean values of boss availability */
    isBossAvailable: PropTypes.bool.isRequired,

    sameStore: PropTypes.bool.isRequired,

    /** boolean values to check BOPIS selection */
    // eslint-disable-next-line react/no-unused-prop-types
    isBopisSelected: PropTypes.bool.isRequired,

    /** boolean values to check BOSS selection */
    // eslint-disable-next-line react/no-unused-prop-types
    isBossSelected: PropTypes.bool.isRequired,
    /** store id that was selected */
    selectedStoreId: PropTypes.number.isRequired,

    isBopisCtaEnabled: PropTypes.bool.isRequired,
    onPickupRadioBtnToggle: PropTypes.func.isRequired,
    isBossCtaEnabled: PropTypes.bool.isRequired,
    buttonLabel: PropTypes.string.isRequired,
    onStoreUpdate: PropTypes.func.isRequired,
    updateCartItemStore: PropTypes.bool.isRequired,
    setFavoriteStore: PropTypes.func,
    getDefaultStore: PropTypes.func,
    setClickAnalyticsData: PropTypes.func,
    trackClick: PropTypes.func,
    storeSearchCriteria: PropTypes.string,
    storeSearchDistance: PropTypes.string,
    pageData: PropTypes.shape({
      products: PropTypes.shape({}),
    }),
    fromPage: PropTypes.string,
    isFromChangeStore: PropTypes.bool,
    isSizeSelected: PropTypes.bool,
  };

  static defaultProps = {
    setFavoriteStore: () => {},
    getDefaultStore: () => {},
    setClickAnalyticsData: () => {},
    trackClick: () => {},
    storeSearchCriteria: '',
    storeSearchDistance: '',
    pageData: {},
    fromPage: '',
    isFromChangeStore: false,
    isSizeSelected: false,
  };

  constructor(props) {
    super(props);
    this.handleStoreSelect = this.handleStoreSelect.bind(this);
    this.handleStoreUpdate = this.handleStoreUpdate.bind(this);
    this.handlePickupRadioBtn = this.handlePickupRadioBtn.bind(this);
  }

  getStoreCloseTime() {
    const {
      store: { hours },
    } = this.props;
    const storeClosingTimeToday =
      hours &&
      hours.regularHours[0] &&
      !hours.regularHours[0].isClosed &&
      hours.regularHours[0].openIntervals[0]
        ? toTimeString(parseDate(hours.regularHours[0].openIntervals[0].toHour))
        : '';
    const storeClosingTimeTomorrow =
      hours &&
      hours.regularHours[1] &&
      !hours.regularHours[1].isClosed &&
      hours.regularHours[1].openIntervals[0]
        ? toTimeString(parseDate(hours.regularHours[1].openIntervals[0].toHour))
        : '';
    return {
      storeClosingTimeToday,
      storeClosingTimeTomorrow,
    };
  }

  getStoreCloseOpenTime() {
    const {
      store: { hours },
    } = this.props;

    const storeLocatorLabels = {
      lbl_storeldetails_closing: 'Closes in',
      lbl_storelanding_openInterval: 'Open until',
      lbl_storelanding_opensAt: 'Opens tomorrow at ',
      lbl_storeldetails_closed: 'Closed',
    };

    const currentDate = new Date();

    const { storeLabel, storeTimeLabel } =
      hours && Object.keys(hours).length > 0
        ? getStoreHoursClosingTiming(hours, storeLocatorLabels, currentDate)
        : null;
    return {
      storeLabel,
      storeTimeLabel,
    };
  }

  /**
   * @method handleStoreSelect
   * @description this method sets the selected store
   */
  handleStoreSelect() {
    // TODO - const {  isBoss = false } = e.target.something;
    const isBoss = this.isBossSelected;
    // Fetch isBoss from component instead of a new Arrow funct.
    const {
      onStoreSelect,
      store,
      isBossSelected,
      isBopisSelected,
      setFavoriteStore,
      getDefaultStore,
      fromPage,
    } = this.props;

    if (store && store.basicInfo) {
      setFavoriteStore(store);
      getDefaultStore(store);
    }

    const { skuId = '' } = (store && store.productAvailability) || {};

    const storeSelectTrackInfo = {
      isBopisSelected,
      isBossSelected,
      skuId,
    };

    return onStoreSelect(store.basicInfo.id, isBoss, storeSelectTrackInfo, fromPage);
  }

  /**
   *
   * @method handleStoreUpdate
   * @description this method handles store update
   * @memberof PickupStoreListItem
   */
  handleStoreUpdate() {
    const {
      onStoreUpdate,
      store,
      setClickAnalyticsData,
      trackClick,
      isBossSelected,
      isBopisSelected,
      storeSearchCriteria,
      storeSearchDistance,
      pageData,
    } = this.props;

    const isBoss = this.isBossSelected;
    const { products } = pageData;

    // Checking BOSS BOPIS for analytics
    let customEventsVal = '';
    let eventName = '';
    let scName = '';
    if (isBopisSelected) {
      customEventsVal = 'event132';
      eventName = 'bopis cta click on pickup modal';
      scName = 'Pick_Up_Later_e132';
    }
    if (isBossSelected) {
      customEventsVal = 'event133';
      eventName = 'boss cta click on pickup modal';
      scName = 'Pick_Up_Later_e133';
    }

    // setting values and dispatching Click tracker based on the requirement on BOSS/BOPIS add to bag call
    setClickAnalyticsData({
      customEvents: [customEventsVal],
      storeSearchCriteria,
      storeSearchDistance,
      eventName,
      products,
      clickEvent: true,
    });
    trackClick({
      name: scName,
      module: 'browse',
    });
    return onStoreUpdate(store.basicInfo.id, isBoss);
  }

  /**
   * @method handlePickupRadioBtn
   * @description this method sets the pickup mode for store
   */
  handlePickupRadioBtn(isBossSelected) {
    const { onPickupRadioBtnToggle, store } = this.props;
    this.isBossSelected = isBossSelected;
    return onPickupRadioBtnToggle(store.basicInfo.id, isBossSelected, store);
  }

  displayPickupCTA(showBopisCTA, showBossCTA, buttonLabel) {
    const { isBossSelected, isBopisSelected, updateCartItemStore } = this.props;
    return showBopisCTA || showBossCTA ? (
      <PickupCTAWrapper>
        <Button
          buttonVariation="fixed-width"
          onPress={updateCartItemStore ? this.handleStoreUpdate : this.handleStoreSelect}
          fill="BLUE"
          disableButton={!isBossSelected && !isBopisSelected}
          text={buttonLabel}
        />
      </PickupCTAWrapper>
    ) : null;
  }

  displayStoreDetails({
    basicInfo,
    address,
    distance,
    showBopisCTA,
    showBossCTA,
    isBopisSelected,
    isBossSelected,
    BossCtaProps,
    BopisCtaProps,
    buttonLabel,
    addToCartError,
    storeClosingTimeToday,
    storeClosingTimeTomorrow,
    status,
    isFromChangeStore,
    storeLabel,
    storeTimeLabel,
    isSizeSelected,
  }) {
    const { FAVORITE_STORE } = STORE_DETAILS_LABELS;
    const openLocationMap = () => {
      const store = {
        basicInfo: { address, coordinates: { lat: '', long: '' } },
      };
      mapHandler(store);
    };

    if (isFromChangeStore) {
      const { isAvaialble, productStatus } = getProductStatusAndAvailability(status);
      const fullAddress = address
        ? `${address.addressLine1}\n${address.city}, ${address.state} ${address.zipCode}`
        : '';
      return (
        <StoreListItemWrapperIsFromStore isDefaultStore={isBopisSelected}>
          <StoreInfoWrapperIsFromStore>
            <StoreWrapperHeader>
              <PickupRadioBtnWrapperIsFromStore>
                <PickupRadioBtn
                  radioGroupName={PICKUP_RADIO_BTN_NAME}
                  isSelected={isBopisSelected}
                  BopisCtaProps={BopisCtaProps}
                  handleClick={this.handlePickupRadioBtn}
                  isFromChangeStore={isFromChangeStore}
                />
              </PickupRadioBtnWrapperIsFromStore>
              {status && isSizeSelected ? (
                <ViewWrapper>
                  <AvaialbilityWrapper isProductAvailable={isAvaialble}>
                    <BodyCopy
                      color="gray.900"
                      fontFamily="secondary"
                      fontSize="fs14"
                      fontWeight="extrabold"
                      textAlign="center"
                      text={productStatus}
                    />
                  </AvaialbilityWrapper>
                </ViewWrapper>
              ) : null}
            </StoreWrapperHeader>
            <StoreWrapperIsFromStore>
              <StoreWrapper>
                <BodyCopy
                  fontWeight="semibold"
                  fontFamily="secondary"
                  color="gray.900"
                  fontSize="fs12"
                  text={storeLabel}
                />
                <BodyCopy
                  fontWeight="bold"
                  fontFamily="secondary"
                  color="green.700"
                  fontSize="fs12"
                  text={storeTimeLabel}
                />
              </StoreWrapper>
              <BodyCopy
                fontFamily="secondary"
                fontWeight="semibold"
                color="gray.900"
                fontSize="fs12"
                text={capitalize(fullAddress)}
              />
              {displayFavoriteStoreIsFromChangeStore(basicInfo, PICKUP_LABELS.YOUR_FAV_STORE)}
              <StoreWrapper>
                {displayStoreDetailsAnchorFromStore(
                  basicInfo,
                  address,
                  storeClosingTimeToday,
                  storeClosingTimeTomorrow
                )}

                <AnchorWrapper>
                  <Anchor onPress={() => openLocationMap()}>
                    <BodyCopy
                      mobilefontFamily="secondary"
                      fontWeight="regular"
                      fontSize="fs14"
                      color="gray.900"
                      text={PICKUP_LABELS.GET_DIRECTION}
                      textDecoration="underline"
                    />
                  </Anchor>
                </AnchorWrapper>
              </StoreWrapper>
            </StoreWrapperIsFromStore>
          </StoreInfoWrapperIsFromStore>
        </StoreListItemWrapperIsFromStore>
      );
    }
    return (
      <StoreListItemWrapper>
        <StoreInfoWrapper>
          {displayFavoriteStore(basicInfo, FAVORITE_STORE)}
          {displayStoreTitle(basicInfo)}
          {displayDistance(distance)}
          {displayStoreAddress(address)}
          {displayStoreDetailsAnchor(
            basicInfo,
            address,
            storeClosingTimeToday,
            storeClosingTimeTomorrow
          )}
        </StoreInfoWrapper>
        <PickupButtonsWrapper>
          {showBossCTA && (
            <View>
              <PickupRadioBtnWrapper>
                <PickupRadioBtn
                  radioGroupName={PICKUP_RADIO_BTN_NAME}
                  isSelected={isBossSelected}
                  isBossPickupButton
                  handleClick={this.handlePickupRadioBtn}
                  BossCtaProps={BossCtaProps}
                />
              </PickupRadioBtnWrapper>
              {!!addToCartError && isBossSelected && displayAddToCartError(addToCartError)}
            </View>
          )}
          {showBopisCTA && (
            <React.Fragment>
              <PickupRadioBtnWrapper>
                <PickupRadioBtn
                  radioGroupName={PICKUP_RADIO_BTN_NAME}
                  isSelected={isBopisSelected}
                  BopisCtaProps={BopisCtaProps}
                  handleClick={this.handlePickupRadioBtn}
                />
              </PickupRadioBtnWrapper>
              {!!addToCartError && isBopisSelected && displayAddToCartError(addToCartError)}
            </React.Fragment>
          )}

          {this.displayPickupCTA(showBopisCTA, showBossCTA, buttonLabel)}
          {displayStoreUnavailable(showBopisCTA, showBossCTA)}
        </PickupButtonsWrapper>
      </StoreListItemWrapper>
    );
  }

  render() {
    const {
      store: {
        basicInfo,
        distance,
        productAvailability: { status },
        pickupType,
      },
      isBopisAvailable,
      storeBossInfo,
      isBossAvailable,
      sameStore,
      addToCartError,
      selectedStoreId,
      isBopisCtaEnabled,
      isBossCtaEnabled,
      isBopisSelected,
      isBossSelected,
      buttonLabel,
      isFromChangeStore,
      isSizeSelected,
    } = this.props;
    const apiConfig = getAPIConfig();
    const radioLabelIsFromStore = `${capitalize(basicInfo.storeName)} ${
      distance ? `(${distance} mi)` : ``
    } `;

    const BopisCtaProps = {
      buttonLabel: isFromChangeStore ? radioLabelIsFromStore : PICKUP_CTA_LABELS.bopis,
      status:
        status === BOPIS_ITEM_AVAILABILITY.LIMITED ? ITEM_AVAILABILITY_MESSAGES.LIMITED : null,
      pickupDate: { ...getTranslateDateInformation('', apiConfig.language) },
    };
    const BossCtaProps = {
      buttonLabel: isFromChangeStore ? radioLabelIsFromStore : PICKUP_CTA_LABELS.boss,
      pickupLabel: ITEM_AVAILABILITY_MESSAGES.GET_IT_BY,
      startDate: { ...getTranslateDateInformation(storeBossInfo.startDate, apiConfig.language) },
      endDate: { ...getTranslateDateInformation(storeBossInfo.endDate, apiConfig.language) },
    };
    // checking if there is sameStore then both CTAs should be disabled
    const pickupTypeBOPIS = !sameStore ? pickupType.isStoreBopisSelected : true;
    const pickupTypeBOSS = !sameStore ? pickupType.isStoreBossSelected : true;

    const showBopisCTA = parseBoolean(isBopisAvailable) && pickupTypeBOPIS && isBopisCtaEnabled;
    const showBossCTA = parseBoolean(isBossAvailable) && pickupTypeBOSS && isBossCtaEnabled;
    const { storeClosingTimeToday, storeClosingTimeTomorrow } = this.getStoreCloseTime();
    const { storeLabel, storeTimeLabel } = this.getStoreCloseOpenTime();

    return this.displayStoreDetails({
      basicInfo,
      address: basicInfo.address,
      distance,
      storeClosingTimeToday,
      storeClosingTimeTomorrow,
      isBossAvailable,
      pickupTypeBOSS,
      isBopisSelected,
      isBossSelected,
      BossCtaProps,
      BopisCtaProps,
      showBopisCTA,
      showBossCTA,
      buttonLabel,
      addToCartError,
      selectedStoreId,
      status,
      isFromChangeStore,
      storeLabel,
      storeTimeLabel,
      isSizeSelected,
    });
  }
}

export default PickupStoreListItem;
export { PickupStoreListItem as PickupStoreListItemVanilla };
