/* eslint-disable max-lines */
/* eslint-disable no-underscore-dangle */
// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module PickUpStoreModal
 * @desc Component to display PickUp in Store  Modal
 */

import React from 'react';
import isEmpty from 'lodash/isEmpty';
import Modal from '../../../molecules/Modal';
import styles, { modalstyles, ModalContent } from '../styles/PickUpStoreModal.style.native';
import withStyles from '../../../hoc/withStyles';
import errorBoundary from '../../../hoc/withErrorBoundary/errorBoundary';
import withKeyboardAvoidingView from '../../../hoc/withKeyboardAvoidingView.native';

import PickUpStoreModalUtil from './PickUpStoreModal.utils';

const ModalContentWithKeyboardAvoidingView = withKeyboardAvoidingView(ModalContent);

// eslint-disable-next-line no-unused-vars
// const ERRORS_MAP = require('../../../../../services/handler/stateful/errorResponseMapping/index.json');
let modalInstance = null;
let storeListInstance = null;

class PickUpStoreModalView extends PickUpStoreModalUtil {
  componentDidMount() {
    const { closeQuickViewModalAction } = this.props;
    if (closeQuickViewModalAction) {
      closeQuickViewModalAction();
    }
  }

  shouldComponentUpdate(nextProps) {
    const { currentProduct } = nextProps;
    if (isEmpty(currentProduct)) return false;
    return true;
  }

  scrollPageToTarget = () => {
    setTimeout(() => {
      if (storeListInstance)
        storeListInstance.measure((fx, fy, width, height, px, py) => {
          if (modalInstance) modalInstance.scrollTo({ y: py - 100, animated: true });
        });
    }, 1000);
  };

  modalRef = (reference) => {
    modalInstance = reference;
  };

  pickupStoreListRef = (reference) => {
    storeListInstance = reference;
  };

  render() {
    const {
      pickupModalHeading,
      className,
      changeStoreModalHeading,
      isPickupModalOpen,
      isPickupStoreModalRedisgn,
    } = this.props;
    let isFromChangeStore = false;
    if (isPickupStoreModalRedisgn) {
      isFromChangeStore = isPickupModalOpen;
    }
    if (isFromChangeStore) {
      return (
        <Modal
          isOpen
          onRequestClose={this.onCloseClick}
          overlayClassName="TCPModal__Overlay"
          className={`${className} TCPModal__Content`}
          heading={changeStoreModalHeading}
          fixedWidth
          fullWidth
          stickyHeader
          inheritedStyles={modalstyles}
          widthConfig={{ small: '375px', medium: '600px', large: '704px' }}
          heightConfig={{ minHeight: '534px', height: '620', maxHeight: '650' }}
          headingAlign="center"
          horizontalBar={false}
          stickyCloseIcon
          setModalReference={this.modalRef}
          isFromChangeStore={true}
          stickyfooter={this.renderFooter()}
          roundedModal={true}
          overrideCSS={{
            modalCurvedContent: `
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            `,
          }}
        >
          <ModalContentWithKeyboardAvoidingView>
            {this.renderModal(this.scrollPageToTarget, this.pickupStoreListRef, isFromChangeStore)}
          </ModalContentWithKeyboardAvoidingView>
        </Modal>
      );
    }
    return (
      <Modal
        isOpen
        onRequestClose={this.onCloseClick}
        overlayClassName="TCPModal__Overlay"
        className={`${className} TCPModal__Content`}
        heading={pickupModalHeading}
        fixedWidth
        fullWidth
        stickyHeader
        inheritedStyles={modalstyles}
        widthConfig={{ small: '375px', medium: '600px', large: '704px' }}
        heightConfig={{ minHeight: '534px', height: '620', maxHeight: '650' }}
        headingAlign="center"
        horizontalBar={false}
        stickyCloseIcon
        setModalReference={this.modalRef}
      >
        <ModalContentWithKeyboardAvoidingView>
          {this.renderModal(this.scrollPageToTarget, this.pickupStoreListRef)}
        </ModalContentWithKeyboardAvoidingView>
      </Modal>
    );
  }
}

export default withStyles(errorBoundary(PickUpStoreModalView), styles);
export { PickUpStoreModalView as PickUpStoreModalViewVanilla };
