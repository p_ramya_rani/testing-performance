// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module PickUpStoreModal
 * @desc Component to display PickUp in Store  Modal
 */

import React from 'react';
import isEmpty from 'lodash/isEmpty';
import Modal from '../../../molecules/Modal';
import styles, { modalstyles, modalstylesRedesign } from '../styles/PickUpStoreModal.style';
import withStyles from '../../../hoc/withStyles';
import errorBoundary from '../../../hoc/withErrorBoundary/errorBoundary';

import PickUpStoreModalUtil from './PickUpStoreModal.utils';

class PickUpStoreModalView extends PickUpStoreModalUtil {
  componentDidMount() {
    const { closeQuickViewModalAction, newQVEnabled } = this.props;
    if (!newQVEnabled) {
      closeQuickViewModalAction();
    }
  }

  shouldComponentUpdate(nextProps) {
    const { currentProduct } = nextProps;
    if (isEmpty(currentProduct)) return false;
    return true;
  }

  render() {
    const { pickupModalHeading, className, newQVEnabled } = this.props;
    return newQVEnabled ? (
      <Modal
        isOpen
        onRequestClose={this.onCloseClick}
        onAfterOpen={this.onModalOpen}
        overlayClassName="TCPModal__Overlay"
        className={`${className} TCPModal__Content`}
        heading={pickupModalHeading}
        fixedWidth
        fullWidth
        stickyHeader
        inheritedStyles={modalstylesRedesign}
        headingAlign="center"
        horizontalBar={false}
        stickyCloseIcon
        innerContentClassName="pickup-store-innerContent"
      >
        {this.renderModal()}
      </Modal>
    ) : (
      <Modal
        isOpen
        onRequestClose={this.onCloseClick}
        onAfterOpen={this.onModalOpen}
        overlayClassName="TCPModal__Overlay"
        className={`${className} TCPModal__Content`}
        heading={pickupModalHeading}
        fixedWidth
        fullWidth
        stickyHeader
        inheritedStyles={modalstyles}
        widthConfig={{ small: '375px', medium: '600px', large: '704px' }}
        heightConfig={{ minHeight: '534px', height: '620', maxHeight: '650' }}
        headingAlign="center"
        horizontalBar={false}
        stickyCloseIcon
      >
        {this.renderModal()}
      </Modal>
    );
  }
}

export default errorBoundary(withStyles(PickUpStoreModalView, styles));
export { PickUpStoreModalView as PickUpStoreModalViewVanilla };
