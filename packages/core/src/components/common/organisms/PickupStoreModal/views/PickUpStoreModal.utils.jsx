/* eslint-disable max-lines */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/no-unused-prop-types */
// 9fbef606107a605d69c0edbcd8029e5d

/**
 * @module PickUpStoreModal
 * @desc Component to display PickUp in Store  Modal
 */

import React from 'react';
import PropTypes from 'prop-types';
import { getSiteId, isMobileApp, isAndroidWeb } from '@tcp/core/src/utils';
import { CART_ITEM_COUNTER, readCookie } from '@tcp/core/src/utils/cookie.util';

import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import {
  STORE_SUMMARY_PROP_TYPES,
  CART_BOPIS_STORE_LIST,
  COLOR_FIT_SIZE_DISPLAY_NAME,
} from '../PickUpStoreModal.proptypes';
import { clearAddToPickupErrorState } from '../../../../features/CnC/AddedToBag/container/AddedToBag.actions';
import {
  getSkuId,
  getVariantId,
  getVariantNo,
  getMapSliceForColor,
  getIconImageForColor,
  getMapSliceForSize,
} from '../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { SKU_DETAILS } from '../PickUpStoreModal.constants';
import PickupSkuSelectionForm from '../molecules/PickupSkuSelectionForm';
import PickupStoreSelectionFormContainer from '../molecules/PickupStoreSelectionForm';

const DISTANCES_MAP_PROP_TYPE = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
  })
);

// eslint-disable-next-line no-unused-vars
// const ERRORS_MAP = require('../../../../../services/handler/stateful/errorResponseMapping/index.json');

export default class PickUpStoreModalUtil extends React.Component {
  static propTypes = {
    /* the list of stores currently in the cart */
    cartBopisStoresList: CART_BOPIS_STORE_LIST.cartBopisStoresList.isRequired,
    /** labels for selection fields */
    colorFitSizeDisplayNames: COLOR_FIT_SIZE_DISPLAY_NAME.colorFitSizeDisplayNames,
    /** The map of distances options to select the radius of search */
    distancesMap: DISTANCES_MAP_PROP_TYPE.isRequired,
    /** seed values for the form */
    initialValues: PropTypes.shape({
      /** user's preselected color id from parent instance */
      color: PropTypes.string,
      /** user's preselected fit id from parent instance */
      fit: PropTypes.string,
      /** user's preselected size id from parent instance */
      size: PropTypes.string,
      /** user's preselected quantity from parent instance */
      quantity: PropTypes.number,
    }).isRequired,
    /**
     * indicates the modal is shown because of an error trying to add to the preferred store
     * (required only in PDP)
     */
    isPreferredStoreError: PropTypes.bool,
    /** We need to differentiate if Bopis Modal is open from cart or other place to change
     * select item button's message (DT27100)
     */
    isShoppingBag: PropTypes.bool,
    /** indicates the 'extended' sizes not available for bopis notification needs to show
     * (only when user attempted to select it)
     */
    isShowExtendedSizesNotification: PropTypes.bool.isRequired,

    // determines if variation is warning modal
    isPickUpWarningModal: PropTypes.bool,

    // determines if step one needs to be opened
    openSkuSelectionForm: PropTypes.bool,
    maxAllowedStoresInCart: PropTypes.number.isRequired,

    /**
     * Callback for adding an item to cart for pickup in the selected store.
     * Accepts: formData, itemId; where formData is an object with the keys:
     * storeId, skuId (of the selected color/fit/size combination), quantity
     * and itemId is an optional identifier of the item one wishes to add to the cart
     * (see the prop requestorKey).
     */
    addItemToCartInPickup: PropTypes.func.isRequired,
    navigation: PropTypes.shape({}),
    alwaysSearchForBOSS: PropTypes.bool.isRequired,
    openRestrictedModalForBopis: PropTypes.bool.isRequired,

    /**
     * Function to call when the item has been successfully added to, or updated
     * in, the cart.
     */
    // eslint-disable-next-line react/no-unused-prop-types
    onAddItemToCartSuccess: PropTypes.func,

    /** callback for closing this modal */
    closePickupModal: PropTypes.func.isRequired,
    onColorChange: PropTypes.func.isRequired,

    /**
     * Callback to run on component mount
     * (usually to populate redux store information consumened by this component).
     * Should return a promise
     * */
    getUserCartStoresAndSearch: PropTypes.func.isRequired,

    /** the maximum number of different stores used for BOPIS in a single shopping cart.
     * must be at least 1 */

    /** submit method for PickupStoreSelectionForm */
    // eslint-disable-next-line react/no-unused-prop-types
    onSearchAreaStoresSubmit: PropTypes.func.isRequired,

    /** submit method for BopisCartStoresInventoryForm */
    onSearchInCurrentCartStoresSubmit: PropTypes.func.isRequired,
    currentProduct: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,

    /** an optional identifier to be passed to addItemToCartInPickup */
    // eslint-disable-next-line react/no-unused-prop-types
    requestorKey: PropTypes.string,
    pickupModalHeading: PropTypes.string.isRequired,
    isCanada: PropTypes.bool.isRequired,
    isGetUserStoresLoaded: PropTypes.bool.isRequired,
    isPlcc: PropTypes.bool,
    /* The session currency symbol */
    currencySymbol: PropTypes.string,
    /* We are available to know if is an international shipping */
    isInternationalShipping: PropTypes.bool,

    /** Global switches for boss and bopis */
    isBossEnabled: PropTypes.bool,
    isBopisEnabled: PropTypes.bool,
    isBossCtaEnabled: PropTypes.bool,
    isBopisCtaEnabled: PropTypes.bool,
    updateCartItemStore: PropTypes.bool,
    isItemShipToHome: PropTypes.bool,
    autoSkipStep1: PropTypes.bool,
    showDefaultSizeMsg: PropTypes.bool,
    isRadialInventoryEnabled: PropTypes.number,
    cartItemsCount: PropTypes.number,
    defaultStore: STORE_SUMMARY_PROP_TYPES,
    storeSearchError: PropTypes.string,
    addToBagError: PropTypes.string,
    onClearSearchFormError: PropTypes.func.isRequired,
    PickupSkuFormValues: PropTypes.shape({
      /** user's preselected color id from parent instance */
      color: PropTypes.string,
      /** user's preselected fit id from parent instance */
      fit: PropTypes.string,
      /** user's preselected size id from parent instance */
      size: PropTypes.string,
      /** user's preselected quantity from parent instance */
      quantity: PropTypes.number,
    }).isRequired,
    className: PropTypes.string,
    currency: PropTypes.string,
    currencyAttributes: PropTypes.shape({}),
    accessibilityLabels: PropTypes.shape({}),
    updatePickUpCartItem: PropTypes.func.isRequired,
    initialValuesFromBagPage: PropTypes.shape({}).isRequired,
    toastMessage: PropTypes.func,
    setFavoriteStore: PropTypes.func,
    getDefaultStore: PropTypes.func,
    updateAppTypeHandler: PropTypes.func.isRequired,
    fromPage: PropTypes.string,
    pdpLabels: PropTypes.shape({}).isRequired,
    onQuickViewOpenClick: PropTypes.func.isRequired,
    setInitialTCPStyleQty: PropTypes.string.isRequired,
    singlePageLoad: PropTypes.string.isRequired,
    initialMultipackMapping: PropTypes.string.isRequired,
    multipackSelectionAction: PropTypes.func,
    getQickViewSingleLoad: PropTypes.func,
    getDisableSelectedTab: PropTypes.func,
    selectedMultipack: PropTypes.string,
    disableMultiPackTab: PropTypes.bool,
    onPickUpOpenClick: PropTypes.func,
    router: PropTypes.shape({}),
    isPickupModalOpen: PropTypes.bool,
    availableTCPmapNewStyleId: PropTypes.shape([]),
    isAfterPayEnabled: PropTypes.bool,
    newQVEnabled: PropTypes.bool,
    isNewPDPEnabled: PropTypes.bool,
    fromBagPage: PropTypes.bool,
  };

  static defaultProps = {
    colorFitSizeDisplayNames: {},
    updateCartItemStore: false,
    isPickUpWarningModal: false,
    isBossEnabled: false,
    isBopisEnabled: false,
    isBossCtaEnabled: false,
    isBopisCtaEnabled: false,
    openSkuSelectionForm: false,
    autoSkipStep1: false,
    onAddItemToCartSuccess: null,
    isRadialInventoryEnabled: false,
    showDefaultSizeMsg: false,
    isInternationalShipping: false,
    currencySymbol: '$',
    isPlcc: false,
    requestorKey: '',
    isPreferredStoreError: false,
    isShoppingBag: false,
    navigation: null,
    cartItemsCount: 0,
    defaultStore: {},
    storeSearchError: '',
    addToBagError: '',
    className: '',
    currency: 'USD',
    isItemShipToHome: false,
    currencyAttributes: {
      exchangevalue: 1,
    },
    accessibilityLabels: {},
    toastMessage: () => {},
    setFavoriteStore: () => {},
    getDefaultStore: () => {},
    fromPage: '',
    multipackSelectionAction: () => {},
    getQickViewSingleLoad: () => {},
    getDisableSelectedTab: () => {},
    selectedMultipack: '1',
    disableMultiPackTab: false,
    onPickUpOpenClick: () => {},
    router: {},
    isPickupModalOpen: true,
    availableTCPmapNewStyleId: [],
    isAfterPayEnabled: false,
    newQVEnabled: false,
    isNewPDPEnabled: false,
    fromBagPage: false,
  };

  constructor(props) {
    super(props);
    const {
      initialValues,
      openSkuSelectionForm,
      currentProduct,
      distancesMap,
      autoSkipStep1,
      showDefaultSizeMsg,
      productLoading,
    } = props;
    const isSkuResolved =
      productLoading ||
      (this.validateSkuDetails(initialValues, openSkuSelectionForm) &&
        (autoSkipStep1 || !showDefaultSizeMsg));
    const { itemBrand } = currentProduct;
    const SkuSelectedValues = {
      ...initialValues,
      distance: distancesMap[0].id,
      itemBrand,
    };

    this.state = {
      SkuSelectedValues, //  SkuSelectedValues has the initial and latest sku details to keep step 1 and step 2 in sync
      isSkuResolved,
      selectedColor: '',
      isSKUValidated: false,
    };
    this.rootElemId = '__next';
    this.skuId = null;
    this.quantity = null;
    this.callOnlyStoreSearchAPI = false;
    this.handleSearchAreaStoresSubmit = this.handleSearchAreaStoresSubmit.bind(this);
    this.handleSearchInCurrentCartStoresSubmit =
      this.handleSearchInCurrentCartStoresSubmit.bind(this);
    this.handleEditSkuDetails = this.handleEditSkuDetails.bind(this);
    this.onCloseClick = this.onCloseClick.bind(this);
    this.onModalOpen = this.onModalOpen.bind(this);
    this.getSKUValidated = this.getSKUValidated.bind(this);
  }

  onModalOpen() {
    if (!isMobileApp() && isAndroidWeb()) {
      document.querySelector(`#${this.rootElemId}`).style.display = 'none'; // In Android Chrome, whenever any input field got focus, field is getting out of viewport due to chrome bug with viewport calculation. Though It works correctly if modal is the only visible element
    }
  }

  onCloseClick() {
    const { closePickupModal } = this.props;
    clearAddToPickupErrorState();
    closePickupModal({
      isModalOpen: false,
    });
    if (!isMobileApp() && isAndroidWeb()) {
      document.querySelector(`#${this.rootElemId}`).style.display = '';
    }
  }

  getSKUValidated = (skuResolved) => {
    this.setState({
      isSKUValidated: skuResolved,
    });
  };

  getSkuIdForSearch(colorFitsSizesMap, formData) {
    const { PickupSkuFormValues } = this.props;

    return (
      getSkuId(
        colorFitsSizesMap,
        (PickupSkuFormValues && PickupSkuFormValues.color) || formData.color,
        (PickupSkuFormValues && PickupSkuFormValues.Fit) || formData.Fit,
        PickupSkuFormValues && PickupSkuFormValues.Size
      ) || formData.Size
    );
  }

  getVariantIdFormSearch(colorFitsSizesMap, formData) {
    const { PickupSkuFormValues } = this.props;

    return (
      getVariantId(
        colorFitsSizesMap,
        (PickupSkuFormValues && PickupSkuFormValues.color) || formData.color,
        (PickupSkuFormValues && PickupSkuFormValues.Fit) || formData.Fit,
        PickupSkuFormValues && PickupSkuFormValues.Size
      ) || formData.Size
    );
  }

  /** Validate SKU detils if SKU is resolved or not */
  validateSkuDetails(initialValues, openSkuSelectionForm) {
    const { currentProduct } = this.props;
    if (openSkuSelectionForm) {
      return false;
    }

    const invalidInitialValues = !initialValues || !Object.keys(initialValues).length;

    if (invalidInitialValues) {
      return false;
    }

    const colorFitsSizesMap = currentProduct && currentProduct.colorFitsSizesMap;
    const { color, Fit, Size } = initialValues;

    const sizeAvailable = getMapSliceForSize(colorFitsSizesMap, color, Fit, Size);
    const invalidInitialValuesState =
      sizeAvailable && sizeAvailable.maxAvailable > 0 ? !sizeAvailable : true;

    if (invalidInitialValuesState) {
      return false;
    }

    let isValidSKU = true;
    Object.keys(initialValues).forEach((key) => {
      if (
        !initialValues[key] &&
        (key !== SKU_DETAILS.fit || (key === SKU_DETAILS.fit && this.showFitsForProduct()))
      ) {
        isValidSKU = false;
      }
    });
    return isValidSKU;
  }

  /** Handle click of Edit button on Step 2 - which will switch to Step 1 */
  handleEditSkuDetails(e) {
    e.preventDefault();
    this.callOnlyStoreSearchAPI = false; // when changing from step 2 to 1, this is to ensure getUserBopisStore is called every time we click on search on step-1
    this.setState((oldState) => ({ isSkuResolved: !oldState.isSkuResolved }));
  }

  deriveSkuInfoAndSearch(locationPromise, colorFitsSizesMap, formData, cartItemsCount) {
    const { SkuSelectedValues } = this.state;
    const {
      getUserCartStoresAndSearch,
      alwaysSearchForBOSS,
      onSearchAreaStoresSubmit,
      defaultStore,
    } = this.props;
    const { color, Fit, Size, Quantity: quantity } = SkuSelectedValues;
    const country = getSiteId() && getSiteId().toUpperCase();
    const variantId = getVariantId(colorFitsSizesMap, color, Fit, Size);
    const skuId = getSkuId(colorFitsSizesMap, color, Fit, Size);
    const variantNo = getVariantNo(colorFitsSizesMap, color, Fit, Size);
    let distance;
    if (formData) {
      ({ distance } = formData);
    }
    this.skuId = skuId;
    this.quantity = quantity;
    let apiPayload = {
      skuId,
      quantity,
      distance,
      locationPromise,
      variantId,
      cartItemsCount,
      country,
      defaultStore,
      variantNo,
    };
    if (isMobileApp()) {
      const cartValuePromise = readCookie(CART_ITEM_COUNTER);
      cartValuePromise.then((res) => {
        apiPayload = { ...apiPayload, cartItemsCount: parseInt(res || 0, 10) };
        this.handleStoresAndSearchCall(
          apiPayload,
          alwaysSearchForBOSS,
          onSearchAreaStoresSubmit,
          getUserCartStoresAndSearch
        );
      });
    } else {
      this.handleStoresAndSearchCall(
        apiPayload,
        alwaysSearchForBOSS,
        onSearchAreaStoresSubmit,
        getUserCartStoresAndSearch
      );
    }
  }

  handleStoresAndSearchCall(
    apiPayload,
    alwaysSearchForBOSS,
    onSearchAreaStoresSubmit,
    getUserCartStoresAndSearch
  ) {
    if (this.callOnlyStoreSearchAPI) {
      onSearchAreaStoresSubmit(apiPayload);
    } else {
      this.callOnlyStoreSearchAPI = true;
      getUserCartStoresAndSearch({
        apiPayload,
        alwaysSearchForBOSS,
      });
    }
  }

  handleSearchAreaStoresSubmit(locationPromise, colorFitsSizesMap, formData) {
    const { isPickUpWarningModal, cartItemsCount } = this.props;
    const { isSkuResolved } = this.state;
    if (!isSkuResolved) {
      const { PickupSkuFormValues, distancesMap } = this.props;
      const SkuSelectedValues = {
        ...PickupSkuFormValues,
        distance: distancesMap[0].id,
      };
      this.setState(
        {
          isSkuResolved: true,
          SkuSelectedValues,
          selectedColor: PickupSkuFormValues.color,
        },
        () => {
          if (!isPickUpWarningModal)
            this.deriveSkuInfoAndSearch(
              locationPromise,
              colorFitsSizesMap,
              formData,
              cartItemsCount
            );
        }
      );
    } else if (!isPickUpWarningModal) {
      this.deriveSkuInfoAndSearch(locationPromise, colorFitsSizesMap, formData, cartItemsCount);
    }
  }

  handleSearchInCurrentCartStoresSubmit(skuId, quantity) {
    const { onSearchInCurrentCartStoresSubmit } = this.props;
    this.skuId = skuId;
    this.quantity = quantity;
    return onSearchInCurrentCartStoresSubmit(skuId, quantity);
  }

  /**
   * @method showFitsForProduct
   * @description this method returns the bool value, to show product
   * fits properties
   */
  showFitsForProduct() {
    const { currentProduct, initialValues } = this.props;
    const currentColorEntry = getMapSliceForColor(
      currentProduct.colorFitsSizesMap,
      initialValues.color
    );
    return currentColorEntry && currentColorEntry.hasFits;
  }

  renderModal(scrollRef, pickupStoreListRef) {
    const {
      isPreferredStoreError,
      isShowExtendedSizesNotification,
      isShoppingBag,
      cartBopisStoresList,
      maxAllowedStoresInCart,
      currentProduct,
      distancesMap,
      onColorChange,
      isCanada,
      isPlcc,
      isBopisCtaEnabled,
      isBossCtaEnabled,
      updateCartItemStore,
      isPickUpWarningModal,
      defaultStore,
      currencySymbol,
      isInternationalShipping,
      isBopisEnabled,
      isBossEnabled,
      showDefaultSizeMsg,
      isRadialInventoryEnabled,
      cartItemsCount,
      storeSearchError,
      onClearSearchFormError,
      addItemToCartInPickup,
      updatePickUpCartItem,
      currency,
      currencyAttributes,
      PickupSkuFormValues,
      addToBagError,
      navigation,
      initialValuesFromBagPage,
      isItemShipToHome,
      openRestrictedModalForBopis,
      isGetUserStoresLoaded,
      toastMessage,
      setFavoriteStore,
      updateAppTypeHandler,
      getDefaultStore,
      accessibilityLabels,
      fromPage,
      pdpLabels,
      onQuickViewOpenClick,
      closePickupModal,
      setInitialTCPStyleQty,
      singlePageLoad,
      initialMultipackMapping,
      multipackSelectionAction,
      getQickViewSingleLoad,
      getDisableSelectedTab,
      selectedMultipack,
      disableMultiPackTab,
      onPickUpOpenClick,
      router,
      isPickupModalOpen,
      availableTCPmapNewStyleId,
      isAfterPayEnabled,
      newQVEnabled,
      isNewPDPEnabled,
      productLoading,
      fromBagPage,
      alternateBrand,
    } = this.props;

    const { route = '' } = router;
    const isOutfit = route.toLowerCase().includes('outfit');
    let { colorFitSizeDisplayNames } = this.props;
    let { name } = currentProduct;
    const { colorFitsSizesMap, TCPStyleQTY } = currentProduct;

    name = currentProduct ? currentProduct.name : name;

    const {
      SkuSelectedValues = {},
      SkuSelectedValues: { color } = {},
      isSkuResolved,
      selectedColor,
      isSKUValidated,
    } = this.state;

    const isSearchOnlyInCartStores = maxAllowedStoresInCart <= cartBopisStoresList.length;

    /** allowBossStoreSearch flag allows searching in stores forcefully irrespective of
     *  store limit reached or if both the store available in account is bopis stores
     * */

    const allowBossStoreSearch = updateCartItemStore && !isBopisCtaEnabled && isBossCtaEnabled;

    /**
     * bopisChangeStore - checks if the product getting changed from bag is only bopis
     * product
     */
    const bopisChangeStore = updateCartItemStore && isBopisCtaEnabled && !isBossCtaEnabled;
    colorFitSizeDisplayNames = {
      color: 'Color',
      fit: 'Fit',
      size: 'Size',
      size_alt: 'Size',
      ...colorFitSizeDisplayNames,
    };

    return (
      <>
        <PickupSkuSelectionForm
          colorFitSizeDisplayNames={colorFitSizeDisplayNames}
          isShowExtendedSizesNotification={isShowExtendedSizesNotification}
          isPreferredStoreError={isPreferredStoreError}
          onEditSku={this.handleEditSkuDetails}
          promotionalMessage={currentProduct.promotionalMessage}
          promotionalPLCCMessage={currentProduct.promotionalPLCCMessage}
          isPickUpWarningModal={isPickUpWarningModal}
          onColorChange={onColorChange}
          productInfo={currentProduct}
          isCanada={isCanada}
          name={name}
          isPlcc={isPlcc}
          currencySymbol={currencySymbol}
          isInternationalShipping={isInternationalShipping}
          showDefaultSizeMsg={showDefaultSizeMsg}
          isSkuResolved={isSkuResolved}
          currentProduct={currentProduct}
          initialValues={SkuSelectedValues}
          selectedColor={selectedColor}
          currency={currency}
          currencyAttributes={currencyAttributes}
          className="pickup-sku-selection"
          onCloseClick={this.onCloseClick}
          navigation={navigation}
          toastMessage={toastMessage}
          setFavoriteStore={setFavoriteStore}
          getDefaultStore={getDefaultStore}
          updateAppTypeHandler={updateAppTypeHandler}
          isSKUValidated={isSKUValidated}
          getSKUValidated={this.getSKUValidated}
          accessibilityLabels={accessibilityLabels}
          fromPage={fromPage}
          pdpLabels={pdpLabels}
          onQuickViewOpenClick={onQuickViewOpenClick}
          closePickupModal={closePickupModal}
          setInitialTCPStyleQty={setInitialTCPStyleQty}
          singlePageLoad={singlePageLoad}
          initialMultipackMapping={initialMultipackMapping}
          multipackSelectionAction={multipackSelectionAction}
          getQickViewSingleLoad={getQickViewSingleLoad}
          getDisableSelectedTab={getDisableSelectedTab}
          selectedMultipack={selectedMultipack}
          disableMultiPackTab={disableMultiPackTab}
          TCPStyleQTY={TCPStyleQTY}
          onPickUpOpenClick={onPickUpOpenClick}
          hideMultipackPills={isOutfit}
          isPickupModalOpen={isPickupModalOpen}
          availableTCPmapNewStyleId={availableTCPmapNewStyleId}
          isAfterPayEnabled={isAfterPayEnabled}
          newQVEnabled={newQVEnabled}
          isNewPDPEnabled={isNewPDPEnabled}
          productLoading={productLoading}
          initialValuesFromBagPage={initialValuesFromBagPage}
          primaryBrand={currentProduct && currentProduct.primaryBrand}
          fromBagPage={fromBagPage}
          alternateBrand={alternateBrand}
        />
        <PickupStoreSelectionFormContainer
          setFavoriteStore={setFavoriteStore}
          getDefaultStore={getDefaultStore}
          isGetUserStoresLoaded={isGetUserStoresLoaded}
          colorFitSizeDisplayNames={colorFitSizeDisplayNames}
          maxAllowedStoresInCart={maxAllowedStoresInCart}
          colorFitsSizesMap={colorFitsSizesMap}
          cartBopisStoresList={cartBopisStoresList}
          distancesMap={distancesMap}
          imagePath={getIconImageForColor(currentProduct, color)}
          initialValues={SkuSelectedValues}
          isPreferredStoreError={isPreferredStoreError}
          isSearchOnlyInCartStores={isSearchOnlyInCartStores}
          isPickUpWarningModal={isPickUpWarningModal}
          isShoppingBag={isShoppingBag}
          isShowExtendedSizesNotification={isShowExtendedSizesNotification}
          name={name}
          onAddItemToCart={addItemToCartInPickup}
          onUpdatePickUpItem={updatePickUpCartItem}
          onCloseClick={this.onCloseClick}
          onSubmit={this.handleSearchAreaStoresSubmit}
          promotionalMessage={currentProduct.promotionalMessage}
          promotionalPLCCMessage={currentProduct.promotionalPLCCMessage}
          currentProduct={currentProduct}
          addToCartError={addToBagError}
          isBopisCtaEnabled={isBopisCtaEnabled}
          isBossCtaEnabled={isBossCtaEnabled}
          updateCartItemStore={updateCartItemStore}
          allowBossStoreSearch={allowBossStoreSearch}
          bopisChangeStore={bopisChangeStore}
          currencySymbol={currencySymbol}
          isBopisEnabled={isBopisEnabled}
          isBossEnabled={isBossEnabled}
          isGiftCard={currentProduct.isGiftCard}
          isRadialInventoryEnabled={isRadialInventoryEnabled}
          defaultStore={defaultStore}
          itemsCount={cartItemsCount}
          isCanada={isCanada}
          isPlcc={isPlcc}
          isInternationalShipping={isInternationalShipping}
          storeSearchError={storeSearchError}
          onClearSearchFormError={onClearSearchFormError}
          isSkuResolved={isSkuResolved}
          PickupSkuFormValues={PickupSkuFormValues}
          initialValuesFromBagPage={initialValuesFromBagPage}
          isItemShipToHome={isItemShipToHome}
          currencyAttributes={currencyAttributes}
          openRestrictedModalForBopis={openRestrictedModalForBopis}
          updateAppTypeHandler={updateAppTypeHandler}
          getSKUValidated={this.getSKUValidated}
          toastMessage={toastMessage}
          scrollRef={scrollRef}
          pickupStoreListRef={pickupStoreListRef}
          fromPage={fromPage}
          setInitialTCPStyleQty={setInitialTCPStyleQty}
          singlePageLoad={singlePageLoad}
          initialMultipackMapping={initialMultipackMapping}
          isAfterPayEnabled={isAfterPayEnabled}
          newQVEnabled={newQVEnabled}
          isNewPDPEnabled={isNewPDPEnabled}
          primaryBrand={currentProduct && currentProduct.primaryBrand}
          fromBagPage={fromBagPage}
          alternateBrand={alternateBrand}
        />
      </>
    );
  }
}
