// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const RichTextWrapper = styled.View`
  font-size: ${props => props.theme.typography.fontSizes.fs14};
  margin: ${props => props.theme.spacing.ELEM_SPACING.LRG}
    ${props => props.theme.spacing.ELEM_SPACING.MED} 0
    ${props => props.theme.spacing.ELEM_SPACING.MED};
`;
export const contentHeight = { minHeight: 600 };

export const BonusPointsLoaderWrapper = styled.View`
  height: 156px;
`;

export const SkeletonWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.MED};
  width: 200px;
`;
