// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { AddressBookVanilla } from '../AddEditAddress.view.native';

describe('AddEditAddress Native', () => {
  let component;
  beforeEach(() => {
    component = shallow(<AddressBookVanilla addressFormLabels={{}} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    component.setProps({ currentForm: 'AddAddress' });
    expect(component).toMatchSnapshot();
    expect(component.find(Notification)).toHaveLength(0);
  });

  it('should return the length of view as 1', () => {
    expect(component).toHaveLength(1);
    expect(component.find('View')).toHaveLength(1);
  });

  it('should test with verification modal', () => {
    component.setProps({ currentForm: 'VerificationModal' });
    expect(component).toMatchSnapshot();
  });

  it('should show submit error message', () => {
    component.setProps({ submitErrors: { address: { formGeneric: 'Some Error Occurred' } } });
    expect(component).toMatchSnapshot();
    expect(component.find(Notification)).toHaveLength(1);
  });
});
