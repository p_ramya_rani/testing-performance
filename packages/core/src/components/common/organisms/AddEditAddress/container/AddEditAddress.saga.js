// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest, select } from 'redux-saga/effects';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { trackFormError } from '@tcp/core/src/analytics/actions';
import logger from '@tcp/core/src/utils/loggerInstance';
import constants from './AddEditAddress.constants';
import { addAddressSuccess, addAddressFail } from './AddEditAddress.actions';
import {
  setAddressBookNotification,
  clearGetAddressListTTL,
} from '../../../../features/account/AddressBook/container/AddressBook.actions';
import { addAddress, updateAddress } from '../../../../../services/abstractors/account';
import { getUserEmail } from '../../../../features/account/User/container/User.selectors';
import { getUserInfo } from '../../../../features/account/User/container/User.actions';

function getTrackingList3VarValues(payload) {
  const {
    country,
    firstName,
    lastName,
    city,
    state,
    zipCode,
    phoneNumber,
    address1,
    address2,
    zip,
    emailAddress,
    primary,
    phone1Publish,
  } = payload;

  return {
    country,
    firstName,
    lastName,
    city,
    state,
    zipCode,
    phoneNumber,
    address1,
    address2,
    zip,
    emailAddress,
    primary,
    phone1Publish,
  };
}

export function* addAddressGet({ payload }, addToAddressBook = true) {
  const userEmail = yield select(getUserEmail);
  const updatedPayload = { ...payload, ...{ email: userEmail } };
  if (addToAddressBook) {
    yield put(setLoaderState(true));
  }
  try {
    const res = yield call(addAddress, updatedPayload);
    if (!addToAddressBook) {
      return res;
    }
    yield put(setLoaderState(false));
    if (res) {
      yield put(
        setAddressBookNotification({
          status: 'success',
        })
      );
      yield put(clearGetAddressListTTL());
      yield put(
        getUserInfo({
          ignoreCache: true,
        })
      );
      return yield put(addAddressSuccess(res.body));
    }
    return yield put(addAddressFail(res.body));
  } catch (err) {
    yield put(
      trackFormError({
        formName: constants.ADDRESS_FORM,
        formData: getTrackingList3VarValues(payload),
      })
    );

    if (!addToAddressBook) {
      yield put(setLoaderState(false));
      throw err;
    }
    yield put(setLoaderState(false));
    let error = {};
    /* istanbul ignore else */
    error = err;
    logger.error({
      error: err,
      extraData: {
        component: 'addAddressGet - AddEditAddress Saga',
        payload: updatedPayload,
      },
    });
    return yield put(addAddressFail(error.response.body.errors[0]));
  }
}

export function* updateAddressPut({ payload }, fromCheckout) {
  const userEmail = yield select(getUserEmail);
  const updatedPayload = { ...payload, ...{ email: userEmail } };
  if (!fromCheckout) {
    yield put(setLoaderState(true));
  }
  try {
    const res = yield call(
      updateAddress,
      updatedPayload,
      fromCheckout && fromCheckout.profileUpdate
    );
    if (!fromCheckout) {
      // in case of checkout, loading state will be handled by checkout page only
      yield put(setLoaderState(false));
    }
    if (res) {
      yield put(
        setAddressBookNotification({
          status: 'success',
        })
      );
      yield put(clearGetAddressListTTL());
      if (!fromCheckout) {
        yield put(
          getUserInfo({
            ignoreCache: true,
          })
        );
      }
      const putRes = yield put(addAddressSuccess(res.body));

      if (fromCheckout) {
        return res.body;
      }
      return putRes;
    }
    return yield res.body;
  } catch (err) {
    yield put(
      trackFormError({
        formName: constants.ADDRESS_FORM,
        formData: getTrackingList3VarValues(payload),
      })
    );
    let error = {};
    yield put(setLoaderState(false));
    if (err instanceof Error) {
      error = err.response.body;
    }
    logger.error({
      error: err,
      extraData: {
        component: 'updateAddressPut - AddEditAddress Saga',
        payload: updatedPayload,
      },
    });
    return yield put(addAddressFail(error));
  }
}

export function* AddEditAddressSaga() {
  yield takeLatest(constants.ADD_USER_ADDRESS_REQ, addAddressGet);
  yield takeLatest(constants.UPDATE_USER_ADDRESS_REQ, updateAddressPut);
}

export default AddEditAddressSaga;
