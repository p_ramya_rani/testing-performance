// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { ResendOrderWrapper } from '../styles/ResendOrderConfirmation.style.native';
import ResendOrderForm from '../ResendOrderForm/ResendOrderForm.view.native';

const ResendOrderConfirmation = props => {
  const {
    isOrderResendModalOpen,
    setOrderResendModalOpen,
    orderEmailAddress,
    ...otherProps
  } = props;
  return (
    <ResendOrderWrapper>
      {isOrderResendModalOpen ? (
        <ResendOrderForm
          setOrderResendModalOpen={setOrderResendModalOpen}
          isOrderResendModalOpen={isOrderResendModalOpen}
          orderEmailAddress={orderEmailAddress}
          initialValues={{
            emailAddress: orderEmailAddress || '',
          }}
          {...otherProps}
        />
      ) : null}
    </ResendOrderWrapper>
  );
};

ResendOrderConfirmation.propTypes = {
  setOrderResendModalOpen: PropTypes.func.isRequired,
  isOrderResendModalOpen: PropTypes.bool,
  orderId: PropTypes.string.isRequired,
  orderEmailAddress: PropTypes.string.isRequired,
};

ResendOrderConfirmation.defaultProps = {
  isOrderResendModalOpen: false,
};

export default ResendOrderConfirmation;

export { ResendOrderConfirmation as ResendOrderConfirmationVanilla };
