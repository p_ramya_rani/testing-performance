// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ResendOrderConfirmationVanilla } from '../ResendOrderConfirmation.view';

describe('ResendOrderConfirmation component', () => {
  it('should renders correctly when isOrderResendModalOpen is true', () => {
    const props = {
      isOrderResendModalOpen: true,
    };
    const component = shallow(<ResendOrderConfirmationVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
