// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { resendOrderStyle } from '../styles/ResendOrderConfirmation.style';
import ResendOrderForm from '../ResendOrderForm/ResendOrderForm.view';

const ResendOrderConfirmation = props => {
  const {
    isOrderResendModalOpen,
    setOrderResendModalOpen,
    orderEmailAddress,
    className,
    encryptedEmailAddress,
    ...otherProps
  } = props;
  return (
    <ResendOrderForm
      className={className}
      orderEmailAddress={orderEmailAddress}
      setOrderResendModalOpen={setOrderResendModalOpen}
      isOrderResendModalOpen={isOrderResendModalOpen}
      initialValues={{
        emailAddress: orderEmailAddress || '',
        encryptedEmail: encryptedEmailAddress || '',
      }}
      {...otherProps}
    />
  );
};

ResendOrderConfirmation.propTypes = {
  className: PropTypes.string,
  setOrderResendModalOpen: PropTypes.func.isRequired,
  isOrderResendModalOpen: PropTypes.bool,
  orderId: PropTypes.string.isRequired,
  orderEmailAddress: PropTypes.string,
  encryptedEmailAddress: PropTypes.string,
};

ResendOrderConfirmation.defaultProps = {
  className: '',
  isOrderResendModalOpen: false,
  orderEmailAddress: '',
  encryptedEmailAddress: '',
};

export default withStyles(ResendOrderConfirmation, resendOrderStyle);

export { ResendOrderConfirmation as ResendOrderConfirmationVanilla };
