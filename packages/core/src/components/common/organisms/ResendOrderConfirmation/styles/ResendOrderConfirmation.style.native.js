// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';
import { Button } from '@tcp/core/src/components/common/atoms';

export const ResendOrderWrapper = styled.View`
  width: 100%;
  margin-top: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
`;

const ResendOrderFormWrapper = styled.View`
  width: 100%;
`;

const StyledModalWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
  padding: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
`;

const ButtonWrapper = styled.View``;

const FormWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

const SendEmailButton = {
  marginBottom: 30,
};

const CustomButton = styled(Button)`
  border: 1px solid ${props => props.theme.colorPalette.white};
`;

const StyledErrorWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 90%;
`;

const StyledErrorIcon = styled.View`
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export {
  ResendOrderFormWrapper,
  ButtonWrapper,
  FormWrapper,
  StyledModalWrapper,
  SendEmailButton,
  CustomButton,
  StyledErrorWrapper,
  StyledErrorIcon,
};
