// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

export const resendOrderStyle = css`
  color: ${(props) => props.theme.colorPalette.BLACK};
  background: ${(props) => props.theme.colors.WHITE};
  border-color: ${(props) => props.theme.colorPalette.BLACK};
`;

export default css`
  div.TCPModal__InnerContent {
    width: 80%;
    padding-top: 45px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 50px;
      width: 432px;
    }
    height: auto;
  }
  .emailResend__inside-container {
    align-items: center;
    display: flex;
    flex-direction: column;
    .resend-email-description {
      margin: 22px 0;
      text-align: center;
    }
  }
  .resend-button-wrapper-form {
    width: 80%;
    margin: ${(props) => props.theme.spacing.MODULE_SPACING.MED} auto 0;
    .join-button {
      margin-bottom: 25px;
    }
    .cancel-button {
      color: ${(props) => props.theme.colorPalette.gray[400]};
    }
  }
  .ResendOrder__error {
    display: flex;
    flex-direction: row;
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    .warning-icon {
      background: transparent url('${(props) => getIconPath('circle-alert-fill', props)}') no-repeat
        0 0;
      background-size: contain;
      border: none;
      height: 14px;
      width: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      margin-right: 7px;
      flex-shrink: 0;
    }
  }
  .emailResend__content {
    justify-content: center;
    .input-fields-wrapper {
      width: 100%;
    }
  }
`;
