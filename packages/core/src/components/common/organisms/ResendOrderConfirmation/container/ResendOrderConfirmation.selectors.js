// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { ORDERDETAILS_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { getLabelValue } from '@tcp/core/src/utils';

const getState = state => state[ORDERDETAILS_REDUCER_KEY];

const getLabels = state => {
  return state && state.Labels;
};

export const getisOrderResendModalOpen = createSelector(
  getState,
  state => state && state.isOrderResendModalOpen
);

export const getResendEmailConfirmLabels = createSelector(
  getLabels,
  labels => {
    return {
      modalHeading: getLabelValue(labels, 'lbl_resend_email_modal_heading', 'trackOrder', 'global'),
      modalDescription: getLabelValue(
        labels,
        'lbl_resend_email_modal_description',
        'trackOrder',
        'global'
      ),
      noEmailDescription: getLabelValue(
        labels,
        'lbl_resend_noEmail_modal_description',
        'trackOrder',
        'global'
      ),
      emailPlaceHolder: getLabelValue(
        labels,
        'lbl_resend_email_placeHolder',
        'trackOrder',
        'global'
      ),
      confirmButton: getLabelValue(labels, 'lbl_resend_email_send_button', 'trackOrder', 'global'),
      cancelButton: getLabelValue(labels, 'lbl_resend_email_cancel_button', 'trackOrder', 'global'),
    };
  }
);

export default { getisOrderResendModalOpen };
