// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import sendOrderEmailAbstractor from '@tcp/core/src/services/abstractors/common/ResendOrderConfirmation/ResendOrderConfirmation';
import getErrorList from '@tcp/core/src/components/features/CnC/BagPage/container/Errors.selector';
import RESEND_ORDER_CONSTANTS from './ResendOrderConfirmation.constants';
import { setOrderResendModalOpen } from './ResendOrderConfirmation.actions';

const SUCCESS = 'success';

export function* sendOrderEmailSaga({ payload }) {
  const { emailAddress, encryptedEmail, orderId, fromPage, resolve, reject } = payload;
  try {
    const emailValidationState = emailAddress
      ? yield call(emailSignupAbstractor.verifyEmail, emailAddress)
      : {};
    const errorMapping = yield select(getErrorList);
    if (emailValidationState.error) {
      throw new SubmissionError({
        emailAddress: errorMapping.ERR_RESEND_ORDER_EMAIL,
      });
    } else {
      const emailld = emailAddress || encryptedEmail;
      const resendOrderConfirmation = yield call(sendOrderEmailAbstractor.sendOrderEmail, {
        emailld,
        orderId,
        fromPage,
      });
      if (resendOrderConfirmation === SUCCESS) {
        yield put(setOrderResendModalOpen(false));
      } else {
        throw new SubmissionError({
          _error: resendOrderConfirmation.errorMessage || errorMapping.DEFAULT,
        });
      }
    }
    resolve();
  } catch (err) {
    reject(err);
  }
}

function* ResendOrderConfirmationSaga() {
  yield takeLatest(RESEND_ORDER_CONSTANTS.RESEND_ORDER_EMAIL, sendOrderEmailSaga);
}

export default ResendOrderConfirmationSaga;
