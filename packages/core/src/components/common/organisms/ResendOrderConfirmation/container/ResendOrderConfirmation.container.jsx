// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import { submitResendOrderEmail, setOrderResendModalOpen } from './ResendOrderConfirmation.actions';
import ResendOrderConfirmation from '../views';
import {
  getUserEmail,
  getIsGuest,
} from '../../../../features/account/User/container/User.selectors';
import {
  getisOrderResendModalOpen,
  getResendEmailConfirmLabels,
} from './ResendOrderConfirmation.selectors';
import { getOrderEmailAddress } from '../../../../features/CnC/Confirmation/container/Confirmation.selectors';

export const mapDispatchToProps = dispatch => {
  return {
    submitResendOrderEmail: payload => {
      dispatch(submitResendOrderEmail(payload));
    },
    validateResendEmail: email => {
      return emailSignupAbstractor.verifyEmail(email);
    },
    setOrderResendModalOpen: payload => {
      dispatch(setOrderResendModalOpen(payload));
    },
  };
};

const mapStateToProps = (state, props) => {
  const { trackingEmail } = props;
  const isGuest = getIsGuest(state);
  const orderEmailAddress = isGuest ? getOrderEmailAddress(state) : getUserEmail(state);
  return {
    resendEmailLabels: getResendEmailConfirmLabels(state),
    isOrderResendModalOpen: getisOrderResendModalOpen(state),
    isGuest,
    orderEmailAddress: orderEmailAddress || trackingEmail,
    ...props,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResendOrderConfirmation);
