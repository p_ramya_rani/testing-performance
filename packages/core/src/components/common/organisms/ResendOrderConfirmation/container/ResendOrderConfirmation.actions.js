// 9fbef606107a605d69c0edbcd8029e5d 
import RESEND_ORDER_CONSTANTS from './ResendOrderConfirmation.constants';

export const validateEmail = payload => {
  return {
    payload,
    type: RESEND_ORDER_CONSTANTS.VALIDATE_EMAIL,
  };
};

export const submitResendOrderEmail = payload => {
  return {
    payload,
    type: RESEND_ORDER_CONSTANTS.RESEND_ORDER_EMAIL,
  };
};

export const setOrderResendModalOpen = payload => {
  return {
    payload,
    type: RESEND_ORDER_CONSTANTS.RESEND_ORDER_CONFIRMATION_MODAL,
  };
};
