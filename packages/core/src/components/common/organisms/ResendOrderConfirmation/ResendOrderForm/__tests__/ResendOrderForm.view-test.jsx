// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ResendOrderFormVanilla } from '../ResendOrderForm.view';

describe('ResendOrderForm component', () => {
  it('should renders correctly when isOrderResendModalOpen is true', () => {
    const props = {
      isOrderResendModalOpen: true,
      handleSubmit: jest.fn(),
    };
    const component = shallow(<ResendOrderFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with orderEmailAddress', () => {
    const props = {
      isOrderResendModalOpen: true,
      handleSubmit: jest.fn(),
      orderEmailAddress: 'abcd@yopmail.com',
    };
    const component = shallow(<ResendOrderFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when it has error message', () => {
    const props = {
      isOrderResendModalOpen: true,
      handleSubmit: jest.fn(),
      orderEmailAddress: 'abc@yopmail.com',
      error: 'Oops... an error occured',
      isGuest: true,
    };
    const component = shallow(<ResendOrderFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when addresses are present', () => {
    const props = {
      isOrderResendModalOpen: false,
      handleSubmit: jest.fn(),
    };
    const component = shallow(<ResendOrderFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call setOrderResendModalOpen when form submited success', () => {
    const props = {
      isOrderResendModalOpen: false,
      handleSubmit: jest.fn(),
      setOrderResendModalOpen: jest.fn(),
    };
    const component = shallow(<ResendOrderFormVanilla {...props} />);
    component.instance().setModalOpen();
    expect(props.setOrderResendModalOpen).toHaveBeenCalled();
  });

  it('should trigger setOrderResendModalOpen when triggering modal', () => {
    const props = {
      isOrderResendModalOpen: false,
      handleSubmit: jest.fn(),
      setOrderResendModalOpen: jest.fn(),
      reset: jest.fn(),
    };

    const component = shallow(<ResendOrderFormVanilla {...props} />);
    component.instance().setModalClose();
    expect(props.setOrderResendModalOpen).toHaveBeenCalled();
  });

  it('should trigger submitResendOrderEmail when submitting form', () => {
    const props = {
      isOrderResendModalOpen: false,
      handleSubmit: jest.fn(),
      setOrderResendModalOpen: jest.fn(),
      submitResendOrderEmail: jest.fn(),
    };
    const formValues = {
      emailAddress: 'abcd@yopmail.com',
    };
    const component = shallow(<ResendOrderFormVanilla {...props} />);
    component.instance().submitForm(formValues);
    expect(props.submitResendOrderEmail).toHaveBeenCalled();
  });
});
