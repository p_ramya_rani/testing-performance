// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components/native';
import { Button, TextBox, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import createValidateMethod from '../../../../../utils/formValidation/createValidateMethod';
import Modal from '../../../molecules/Modal';
import getStandardConfig from '../../../../../utils/formValidation/validatorStandardConfig';
import {
  ButtonWrapper,
  FormWrapper,
  ResendOrderFormWrapper,
  StyledModalWrapper,
  SendEmailButton,
  StyledErrorWrapper,
  StyledErrorIcon,
} from '../styles/ResendOrderConfirmation.style.native';
import withKeyboardAvoidingView from '../../../hoc/withKeyboardAvoidingView.native';

const errorIcon = require('../../../../../../../mobileapp/src/assets/images/alert-triangle.png');

class ResendOrderForm extends React.Component {
  setModalOpen = () => {
    const { setOrderResendModalOpen } = this.props;
    setOrderResendModalOpen(true);
  };

  setModalClose = () => {
    const { setOrderResendModalOpen, reset } = this.props;
    setOrderResendModalOpen(false);
    reset();
  };

  submitForm = formValues => {
    const { submitResendOrderEmail, orderId, fromPage } = this.props;
    const { emailAddress } = formValues;

    return new Promise((resolve, reject) => {
      submitResendOrderEmail({ emailAddress, orderId, fromPage, resolve, reject });
    });
  };

  render() {
    const {
      handleSubmit,
      resendEmailLabels,
      submitting,
      isOrderResendModalOpen,
      orderEmailAddress,
      isGuest,
      error,
    } = this.props;
    const {
      modalDescription,
      noEmailDescription,
      modalHeading,
      emailPlaceHolder,
      confirmButton,
      cancelButton,
    } = resendEmailLabels || {};
    const resendModalDescription = isGuest ? noEmailDescription : modalDescription;
    return (
      <ResendOrderFormWrapper>
        <Modal
          isOpen={isOrderResendModalOpen}
          heading={modalHeading}
          headingAlign="center"
          onRequestClose={this.setModalClose}
          closeIconDataLocator="email-resend-modal_close_btn"
          aria={{
            describedby: 'email-resend-modal-confirm-view',
          }}
          fontSize="fs16"
          fontFamily="secondary"
        >
          <StyledModalWrapper>
            <BodyCopy
              fontFamily="secondary"
              fontWeight="regular"
              fontSize="fs14"
              text={resendModalDescription}
              color="gray.900"
              textAlign="center"
            />
            <FormWrapper>
              {orderEmailAddress ? (
                <Field
                  label={emailPlaceHolder}
                  name="emailAddress"
                  id="emailAddress"
                  component={TextBox}
                  maxLength={50}
                  dataLocator="email_address_field"
                  editable={!isGuest}
                />
              ) : null}
              {error ? (
                <StyledErrorWrapper>
                  <StyledErrorIcon>
                    <Image source={errorIcon} alt="" width="16px" height="14px" />
                  </StyledErrorIcon>
                  <BodyCopy
                    fontFamily="secondary"
                    fontWeight="extrabold"
                    fontSize="fs12"
                    text={error}
                    color="error"
                  />
                </StyledErrorWrapper>
              ) : null}
              <ButtonWrapper>
                <Button
                  fullWidth
                  disableButton={submitting}
                  buttonVariation="fixed-width"
                  fill="BLUE"
                  text={confirmButton}
                  type="submit"
                  dataLocator="join-button"
                  onPress={handleSubmit(this.submitForm)}
                  style={SendEmailButton}
                />
                <Button
                  fullWidth
                  disableButton={submitting}
                  buttonVariation="fixed-width"
                  fill="WHITE"
                  text={cancelButton}
                  type="button"
                  dataLocator="cancel-button"
                  onPress={this.setModalClose}
                />
              </ButtonWrapper>
            </FormWrapper>
          </StyledModalWrapper>
        </Modal>
      </ResendOrderFormWrapper>
    );
  }
}

ResendOrderForm.propTypes = {
  setOrderResendModalOpen: PropTypes.func.isRequired,
  submitResendOrderEmail: PropTypes.func.isRequired,
  isOrderResendModalOpen: PropTypes.bool,
  isGuest: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  resendEmailLabels: PropTypes.shape({}),
  submitting: PropTypes.func.isRequired,
  orderId: PropTypes.string.isRequired,
  fromPage: PropTypes.string,
  reset: PropTypes.func.isRequired,
  orderEmailAddress: PropTypes.string,
  error: PropTypes.string,
};

ResendOrderForm.defaultProps = {
  isOrderResendModalOpen: false,
  isGuest: false,
  resendEmailLabels: {},
  fromPage: 'orderHistory',
  orderEmailAddress: '',
  error: '',
};

const validateMethod = createValidateMethod(getStandardConfig(['emailAddress']));

const onSubmitSuccess = (result, dispatch, { reset }) => {
  reset();
};

export default reduxForm({
  form: 'ResendOrderConfirmation',
  ...validateMethod,
  enableReinitialize: true,
  onSubmitSuccess,
})(withKeyboardAvoidingView(withTheme(ResendOrderForm)));

export { ResendOrderForm as ResendOrderFormVanilla };
