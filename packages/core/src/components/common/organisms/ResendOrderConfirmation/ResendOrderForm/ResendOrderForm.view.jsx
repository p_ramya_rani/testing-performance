// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Modal } from '@tcp/core/src/components/common/molecules';
import { Button, Col, Row, TextBox, BodyCopy } from '@tcp/core/src/components/common/atoms';
import createValidateMethod from '../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../utils/formValidation/validatorStandardConfig';
import { removeSpaces } from '../../../../../utils/formValidation/phoneNumber';
import styles from '../styles/ResendOrderConfirmation.style';

class ResendOrderForm extends React.Component {
  setModalOpen = () => {
    const { setOrderResendModalOpen } = this.props;
    setOrderResendModalOpen(true);
  };

  setModalClose = () => {
    const { setOrderResendModalOpen, reset } = this.props;
    setOrderResendModalOpen(false);
    reset();
  };

  submitForm = (formValues) => {
    const { submitResendOrderEmail, orderId, fromPage } = this.props;
    const { emailAddress, encryptedEmail } = formValues;

    return new Promise((resolve, reject) => {
      submitResendOrderEmail({ emailAddress, encryptedEmail, orderId, fromPage, resolve, reject });
    });
  };

  render() {
    const {
      className,
      handleSubmit,
      resendEmailLabels,
      submitting,
      isOrderResendModalOpen,
      orderEmailAddress,
      isGuest,
      error,
    } = this.props;
    const {
      modalDescription,
      noEmailDescription,
      modalHeading,
      emailPlaceHolder,
      confirmButton,
      cancelButton,
    } = resendEmailLabels || {};
    const resendModalDescription = isGuest ? noEmailDescription : modalDescription;
    return (
      <Fragment>
        <Modal
          isOpen={isOrderResendModalOpen}
          colSet={{ small: 6, medium: 6, large: 8 }}
          className={`${className} email-resend-modal`}
          headingAlign="center"
          heightConfig={{ minHeight: '400px', height: '429px', maxHeight: '436px' }}
          overlayClassName="TCPModal__Overlay order-email-resend-modal"
          onRequestClose={this.setModalClose}
          closeIconDataLocator="email-resend-modal_close_btn"
          aria={{
            describedby: 'email-resend-modal-confirm-view',
          }}
        >
          <form onSubmit={handleSubmit(this.submitForm)}>
            <Col
              colSize={{
                small: 6,
                medium: 8,
                large: 12,
              }}
              className="emailResend__inside-container"
            >
              <BodyCopy
                className="resend-email-heading"
                fontSize={['fs16', 'fs16', 'fs22']}
                component="div"
                color="text.primary"
                fontFamily="secondary"
                fontWeight="black"
              >
                {modalHeading}
              </BodyCopy>
              <BodyCopy
                className="resend-email-description"
                fontSize="fs14"
                component="div"
                color="text.primary"
                fontFamily="secondary"
                fontWeight="regular"
              >
                {resendModalDescription}
              </BodyCopy>
              {orderEmailAddress ? (
                <Row className="emailResend__content" fullBleed>
                  <Field
                    placeholder={emailPlaceHolder}
                    name="emailAddress"
                    id="order-resend-confirmation"
                    type="text"
                    component={TextBox}
                    maxLength={50}
                    dataLocator="email_address_field"
                    enableSuccessCheck={false}
                    disabled={isGuest}
                    normalize={removeSpaces}
                  />
                </Row>
              ) : (
                <Field
                  name="encryptedEmail"
                  id="order-resend-confirmation"
                  type="hidden"
                  component={TextBox}
                  dataLocator="email_address_field"
                  enableSuccessCheck={false}
                  disabled={isGuest}
                />
              )}
              {error && (
                <div className="ResendOrder__error">
                  <div className={error ? 'warning-icon' : ''} aria-disabled="true" />
                  <BodyCopy
                    color="error"
                    component="div"
                    fontSize="fs12"
                    fontFamily="secondary"
                    fontWeight="extrabold"
                    role="alert"
                    aria-live="assertive"
                    id="resend-order-email-error"
                  >
                    {error}
                  </BodyCopy>
                </div>
              )}
              <Row className="resend-button-wrapper-form" fullBleed>
                <Button
                  fullWidth
                  disabled={submitting}
                  buttonVariation="fixed-width"
                  fill="BLUE"
                  type="submit"
                  className="join-button"
                  dataLocator="join-button"
                >
                  {confirmButton}
                </Button>
                <Button
                  fullWidth
                  disabled={submitting}
                  buttonVariation="fixed-width"
                  fill="WHITE"
                  type="button"
                  className="cancel-button"
                  dataLocator="cancel-button"
                  onClick={this.setModalClose}
                  color
                >
                  {cancelButton}
                </Button>
              </Row>
            </Col>
          </form>
        </Modal>
      </Fragment>
    );
  }
}

ResendOrderForm.propTypes = {
  className: PropTypes.string,
  setOrderResendModalOpen: PropTypes.func.isRequired,
  submitResendOrderEmail: PropTypes.func.isRequired,
  isOrderResendModalOpen: PropTypes.bool,
  isGuest: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  resendEmailLabels: PropTypes.shape({}),
  submitting: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  orderId: PropTypes.string.isRequired,
  fromPage: PropTypes.string,
  orderEmailAddress: PropTypes.string,
  error: PropTypes.string,
};

ResendOrderForm.defaultProps = {
  className: '',
  isOrderResendModalOpen: false,
  isGuest: false,
  resendEmailLabels: {},
  fromPage: 'orderHistory',
  orderEmailAddress: '',
  error: '',
};

const validateMethod = createValidateMethod(getStandardConfig(['emailAddress']));

const onSubmitSuccess = (result, dispatch, { reset }) => {
  reset();
};

export default reduxForm({
  form: 'ResendOrderConfirmation',
  ...validateMethod,
  enableReinitialize: true,
  onSubmitSuccess,
})(withStyles(ResendOrderForm, styles));

export { ResendOrderForm as ResendOrderFormVanilla };
