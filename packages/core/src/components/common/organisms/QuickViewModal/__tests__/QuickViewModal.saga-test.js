// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable complexity */
import { takeLatest } from 'redux-saga/effects';
import QUICK_VIEW_CONSTANT from '../container/QuickViewModal.constants';
import QuickViewSaga, { fetchProductDetail } from '../container/QuickViewModal.saga';

describe('QuickViewSaga', () => {
  it('should return correct fetchProductDetail takeLatest effect', () => {
    const generator = QuickViewSaga();
    const takeLatestDescriptor = generator.next().value;
    const expected = takeLatest(QUICK_VIEW_CONSTANT.FETCH_QUICK_VIEW, fetchProductDetail);
    expect(takeLatestDescriptor).toEqual(expected);
  });
});
