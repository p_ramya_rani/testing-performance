// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  &.fulfillment-section-wrapper {
    padding: 0px 14px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 0px ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
  &.fulfillment-section-wrapper.quick-view-drawer-redesign {
    padding: 12px 14px 12px 0px;
    background: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: 12px 10px 12px 10px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 12px 14px 12px 20px;
      margin-left: -16px;
    }
  }
  .fulfillment-section-wrapper.quick-view-drawer-redesign {
    margin-top: 20px;
  }
  .fulfillment-section-wrapper-quick-view-drawer-redesign {
    background: ${(props) => props.theme.colors.WHITE};
    border-radius: 6px;
    ${(props) => !props.fromBagPage && `box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.12);`}
    margin-bottom: 12px;
    .delivery-section {
      padding: 16px;
    }
  }
  .product-link-fulfillment-quick-view-drawer-redesign {
    justify-content: center;
    display: flex;
    text-decoration: underline;
    align-items: center;
    color: ${(props) => props.theme.colors.TEXT.DARKBLUE};
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    ${(props) => props.fromBagPage && `font-weight: ${props.theme.fonts.fontWeight.extrabold};`}
  }
  .add-to-bag-button.quick-view-drawer-redesign {
    width: 100%;
    border-radius: 6px;
    background-color: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
    margin-top: 0px;
  }
  .pickup-header.quick-view-drawer-redesign {
    border: none;
  }
  .title-pickup-section-shipping,
  .title-pickup-section-location {
    display: flex;
  }
  .title-pickup-section-shipping {
    border-radius: 6px;
    border: solid 1px ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    cursor: pointer;
  }
  .title-pickup-section-shipping.selected {
    box-shadow: 0 5px 12px 0 rgba(0, 0, 0, 0.12);
    border: solid 3px ${(props) => props.theme.colors.BRAND.PRIMARY};
    outline: solid 3px transparent;
  }
  .pickup-section.quick-view-drawer-redesign {
    border-radius: 6px;
    border: solid 1px ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    cursor: pointer;
  }
  .pickup-section.selected {
    box-shadow: 0 5px 12px 0 rgba(0, 0, 0, 0.12);
    border: solid 3px ${(props) => props.theme.colors.BRAND.PRIMARY};
    outline: solid 3px transparent;
  }
  .added-to-bg-close {
    top: 21px;
  }
  .addedToBagWrapper {
    overflow-y: auto;
    overflow-x: hidden;
    height: calc(100% - 43px);
    padding-right: 15px;
    margin-left: -17px;
    padding-left: 17px;
  }
  .continue-shopping {
    width: 343px;
    height: 16px;
    margin: 0 0 30px 1px;
    font-family: Nunito;
    font-size: 12px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: ${(props) => props.theme.colors.BLACK};
  }
  .recommendationWrapper {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 0px;
    }
  }
  .loyaltyAddedToBagWrapper {
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .style-with-wrapper {
    position: relative;
    padding-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.LRG};
  }
  .modal-heading {
    ${(props) => props.fromBagPage && 'text-align: center;'}
  }
`;

export const customHeaderStyle = css`
  div.TCPModal__InnerContent {
    padding: 0;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 0px ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 0px ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
  .Modal_Heading {
    border-bottom: none;
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    font-size: ${(props) => props.theme.typography.fontSizes.fs22};
    display: flex;
    justify-content: center;
    height: auto;
    margin-bottom: 0px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: flex;
      justify-content: center;
      height: auto;
    }
  }
  .Modal-Header {
    z-index: ${(props) => props.theme.zindex.zIndexQuickView};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .close-modal {
    height: 14px;
    right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.medium} {
      right: 0px;
    }
  }
  .add-to-bag-button {
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-top: 0;
      width: 264px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      width: 243px;
    }
    margin: 0 auto;
  }
  .add-to-bag-button-wrapper {
    bottom: 0px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    position: sticky;
    z-index: ${(props) => props.theme.zindex.zModal + 2};
    background-color: ${(props) => props.theme.colors.WHITE};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM}
      ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0px;
    }
  }
`;

export const quickViewColorSwatchesCss = css`
  .color-chips-selector-item {
    /* Image color of item */
    .color-image {
      width: 23px;
      height: 23px;
    }

    .input-radio-icon-checked {
      width: 23px;
      height: 23px;
    }

    .input-radio-icon-checked + .input-radio-title .color-image {
      width: 19px;
      height: 19px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .color-chips-selector-item {
      .color-image {
        width: 20px;
        height: 20px;
      }
      .input-radio-icon-checked {
        width: 20px;
        height: 20px;
      }

      .input-radio-icon-checked + .input-radio-title .color-image {
        width: 16px;
        height: 16px;
      }
    }
  }
`;

export const customSpinnerStyle = css`
  height: 100%;
`;

export default styles;

export const productInfoStyles = css`
  margin-bottom: 10px;
  .product {
    margin: 0;
    width: 100%;
  }
`;

export const pointsInfoStyles = css`
  .row-padding {
    margin: 0;
    width: 100%;
  }
  .divided-line {
    margin-left: 0;
    margin-right: 0;
    width: 100%;
  }
`;

export const buttonActionStyles = css`
  .view-bag-container {
    margin: 0;
    width: 100%;
  }
  .checkout-button {
    margin: 0;
  }
`;

export const LoyaltyWrapperStyles = css`
  .loyalty-banner-wrapper {
    padding: 0;
    margin-bottom: ${(props) => (props.aboveCta ? `12px` : 0)};
  }
`;

export const recommendationStyles = css`
  .recommendations-header {
    width: auto;
    font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: ${(props) => props.theme.colors.BLACK};
    margin: 0px auto 0px 7px;
    padding-top: 10px;
  }
  .item-container-inner {
    box-sizing: border-box;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: 0 0 5px 5px;
      width: 104px;
    }

    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 0 0 5px 25px;
      width: 104px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 5px ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    }
  }

  .custom-ranking-container img {
    width: 71px;
    height: 14px;
  }

  .custom-ranking-container .reviews-count {
    margin-top: 0px;
    font-size: 10px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: 3px;
      font-size: 10px;
    }
  }

  .ranking-wrapper {
    white-space: pre;
    word-wrap: normal;
  }

  .product-list {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      max-width: 120px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .recommendations-container {
      margin-left: -5px;
    }
    .product-image-container {
      height: 150px;
    }
    .smooth-scroll-list-item {
      margin-right: 0px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .product-image-container {
      height: auto;
    }
    .smooth-scroll-list-item {
      margin-right: 0px;
    }
  }

  .container-price > p {
    font-size: ${(props) => props.theme.typography.fontSizes.fs15};
    width: 40px;
    height: 20px;
    margin: 0 2px -3px 0;
    font-family: Nunito;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
  }
  .container-price > span {
    font-size: ${(props) => props.theme.typography.fontSizes.fs10};
  }
  && .custom-ranking-container img {
    width: 60px;
    height: 12px;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 63px;
      height: 13px;
    }
  }
  && .product-image-container > a {
    min-height: 130px;
  }
  && .slick-list {
    margin-right: -20%;
    margin-left: -8%;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: -8%;
    }
  }
  && .recommendations-tile .slick-arrow {
    top: 35%;
  }
  && .slick-prev {
    top: 18%;
    margin-left: 15%;

    background-image: none;
    width: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.SM};
    height: 72px;
    opacity: 0.79;
    padding: 27px 5px 27px 7px;
    background-color: ${(props) => props.theme.colors.WHITE};
    border-top-right-radius: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    border-bottom-right-radius: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  }
  && .slick-prev::after {
    content: '';
    border: solid black;
    border-width: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
      ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 0;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    transform: rotate(135deg);
    margin: 4px 0;
  }
  && .slick-next {
    top: 18%;
    margin-right: 15%;

    background-image: none;
    width: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.SM};
    height: 72px;
    opacity: 0.79;
    padding: 27px 5px 27px 7px;
    background-color: ${(props) => props.theme.colors.WHITE};
    border-top-left-radius: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    border-bottom-left-radius: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  }
  && .slick-next::after {
    content: '';
    border: solid black;
    border-width: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
      ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 0;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    transform: rotate(-45deg);
    background: none;
    margin: 4px 0;
  }
  && .product-list {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin: 0 ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
`;
