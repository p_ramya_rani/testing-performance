// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export const modalStyles = css`
  div.TCPModal__InnerContent.atb-innerContent {
    right: 0;
    left: auto;
    top: 0;
    bottom: 0;
    transform: none;
    box-shadow: 0 4px 8px 0 rgba(163, 162, 162, 0.5);
    padding: 7px 0 0 17px;
    width: 350px;
    overflow-x: hidden;

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 375px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 481px;
      animation: slide-in 0.5s forwards;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
      animation: slide-in-mobile 0.5s forwards;
      border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
        ${(props) => props.theme.spacing.ELEM_SPACING.XS} 0px 0px;
      padding: 7px 0 20px 0px;
    }
    @keyframes slide-in {
      0% {
        transform: translateX(100%);
      }
      100% {
        transform: translateX(0%);
      }
    }
    @keyframes slide-in-mobile {
      0% {
        transform: translateY(100%);
      }
      100% {
        transform: translateY(5%);
      }
    }
    @keyframes slide-out {
      0% {
        transform: translateX(0%);
      }
      100% {
        transform: translateX(100%);
      }
    }

    .close-modal {
      right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      @media ${(props) => props.theme.mediaQuery.medium} {
        top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      }
    }
    .blurred-content {
      filter: blur(4px);
      -o-filter: blur(4px);
      -ms-filter: blur(4px);
      -moz-filter: blur(4px);
      -webkit-filter: blur(4px);
    }
    .Modal-Header {
      top: 0;
      position: sticky;
      z-index: 100;
      min-height: 53px;
    }
    .modal-heading {
      border-bottom: none;
      padding-bottom: 26px;
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      justify-content: center;
      height: auto;
      display: block;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
      margin: 0 30px 0 20px;
      white-space: nowrap;
      @media ${(props) => props.theme.mediaQuery.medium} {
        justify-content: center;
        height: auto;
        padding-top: 5px;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs14};
        padding-top: 9px;
      }
    }
    .modal-heading:before {
      content: '';
      position: absolute;
      width: 100%;
      height: 51px;
      margin: auto;
      z-index: -1;
      background-color: #f4f4f4;
      bottom: 10px;
      left: -17px;
      padding-right: 17px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        left: -17px;
        padding-right: 17px;
      }
    }
  }
`;

export const productInfoStyles = css`
  margin-bottom: 10px;
  .product {
    margin: 0;
    width: 100%;
  }
`;
