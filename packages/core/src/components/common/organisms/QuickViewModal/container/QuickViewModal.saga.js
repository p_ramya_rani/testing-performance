// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable complexity */
import { call, put, takeLatest, select, all } from 'redux-saga/effects';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import { getAlternateBrandName } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import logger from '@tcp/core/src/utils/loggerInstance';
import { resetAlternateBrandInPickUp } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import {
  getStringAfterSplit,
  getMultipackCount,
  filterMultiPack,
} from '../../../../../utils/utils';
import QUICK_VIEW_CONSTANT from './QuickViewModal.constants';
import {
  setQuickView,
  setModalState,
  setItemFromBagInfoForQuickView,
  setLoadingState,
  setLoadingError,
  setModuleX,
  setMultiPackDetails,
  singlePageLoadAction,
  quickViewSingleLoad,
  setDisableSelectedTab,
} from './QuickViewModal.actions';
import getProductInfoById from '../../../../../services/abstractors/productListing/productDetail';
import { getModuleX } from '../../../../../services/abstractors/common/moduleX';
import {
  getIsPdpServiceEnabled,
  getABTestPdpService,
} from '../../../../../reduxStore/selectors/session.selectors';
import { setLoaderForQuickView } from '../../../molecules/Loader/container/Loader.actions';
import { isMobileApp } from '../../../../../utils';

function* setQuickViewLoadingState(isLoading, isModalOpen) {
  if (!isMobileApp()) {
    yield put(setModalState({ isModalOpen }));
    yield put(setLoadingState({ isLoading }));
  } else {
    yield put(setLoaderForQuickView(isLoading));
  }
}

function fetchProductId(findUpdatedPartId) {
  let newPartId;
  let multiplePartId = '';
  if (findUpdatedPartId.includes(',')) {
    const arrayOfPartNum = getStringAfterSplit(findUpdatedPartId, ',');
    for (let i = 0; i < arrayOfPartNum.length; i += 1) {
      multiplePartId = multiplePartId.concat(getStringAfterSplit(arrayOfPartNum[i], '#')[0]);
      if (i !== arrayOfPartNum.length - 1) {
        multiplePartId = multiplePartId.concat(',');
      }
    }
  } else {
    newPartId = !!findUpdatedPartId && [getStringAfterSplit(findUpdatedPartId, '#')];
    multiplePartId += newPartId ? `${newPartId[0][0]},` : '';
  }
  return multiplePartId;
}

const getMultipackProduct = (newfetchDetailArray) => {
  const filterData =
    newfetchDetailArray &&
    newfetchDetailArray.product &&
    newfetchDetailArray.product.categoryIdList;
  return filterData && filterMultiPack(filterData);
};

const checkIfMultiPack = (payload) => {
  return payload && payload.isMultiPack ? payload.isMultiPack : null;
};

function* getQuickViewData(productDetailArray, payload) {
  if (!payload.getDetailonClick) {
    yield put(
      singlePageLoadAction({
        singlepageload: true,
        availableTCPMultipackProductMapping:
          productDetailArray[0].product && productDetailArray[0].product.TCPMultipackProductMapping,
        availableTCPMultipackProductMappingQuickView:
          productDetailArray[0].product && productDetailArray[0].product.TCPMultipackProductMapping,
        availableTCPmapNewStyleId:
          productDetailArray[0].product && productDetailArray[0].product.TCPmapNewStyleId,
        initialSelectedQty:
          productDetailArray[0].product && productDetailArray[0].product.TCPStyleQTY,
      })
    );
    yield put(quickViewSingleLoad({ initalQuickViewLoad: true }));
  } else yield put(quickViewSingleLoad({ initalQuickViewLoad: false }));
}

function* getProductDetails(productDetailArray) {
  const CheckifProductAvailable =
    productDetailArray &&
    productDetailArray[0] &&
    productDetailArray[0].product &&
    productDetailArray[0].product.colorFitsSizesMap.length &&
    productDetailArray[0].product.unbxdProdId;

  if (!CheckifProductAvailable) {
    yield put(setDisableSelectedTab({ disabledTab: true }));
  } else {
    yield put(setQuickView(productDetailArray));
  }
}

// eslint-disable-next-line sonarjs/cognitive-complexity
export function* fetchProductDetail({ payload }) {
  try {
    const state = yield select();
    const xappURLConfig = yield call(getUnbxdXappConfigs);
    yield put(resetAlternateBrandInPickUp());
    yield put(setLoadingError({ loadingError: false }));
    yield setQuickViewLoadingState(true, true);
    const payloadArray = Array.isArray(payload) ? payload : [payload];
    const { orderInfo } = payloadArray[0];
    let itemBrand;
    let newfetchDetailArray;
    let multiPackCount;
    let finalPartId = '';
    let partId;
    if (orderInfo) {
      ({ itemBrand } = orderInfo);
    }
    yield put(
      setItemFromBagInfoForQuickView({
        orderInfo,
        fromBagPage: !!payload.fromBagPage,
        isSflProduct: payload.isSflProduct,
        isFavoriteEdit: payload.isFavoriteEdit,
        uniqueId: payload.colorProductId,
        pdpUrl: payload.pdpUrl,
        fromPage: payload.fromPage,
        sbpNavigation: payload.sbpNavigation || {},
        isSBP: payload.isSBP || false,
      })
    );
    const isPdpServiceEnabled = yield select(getIsPdpServiceEnabled);
    const enablePDPServiceABTest = yield select(getABTestPdpService);
    const { fromPage } = payload;
    let alternateBrand;
    if (fromPage !== 'pdp') {
      alternateBrand = getAlternateBrandName(state);
    }
    const fetchDetailArray = payloadArray.map((product) => {
      const { colorProductId, generalProductId } = product;
      return call(
        getProductInfoById,
        xappURLConfig,
        colorProductId,
        state,
        itemBrand,
        undefined,
        undefined,
        undefined,
        false,
        undefined,
        generalProductId,
        checkIfMultiPack(payload),
        isPdpServiceEnabled,
        enablePDPServiceABTest,
        null,
        alternateBrand
      );
    });
    const productDetailArray = yield all(fetchDetailArray);
    yield getQuickViewData(productDetailArray, payload);
    if (
      !!productDetailArray[0] &&
      !!productDetailArray[0].product &&
      productDetailArray[0].product.multiPackUSStore
    ) {
      partId = productDetailArray[0].product.tcpMultiPackReferenceUSStore.join(',').split(',');
      const updatedPartId = new Set(partId);
      const findUpdatedPartId = [...updatedPartId];
      for (let k = 0; k < findUpdatedPartId.length; k += 1) {
        finalPartId += fetchProductId(findUpdatedPartId[k]);
      }

      const newMultiPackCount = findUpdatedPartId[0].split(',');

      multiPackCount = newMultiPackCount && getMultipackCount(newMultiPackCount);

      const multipackReferencesAPICall = true;

      newfetchDetailArray = yield call(
        getProductInfoById,
        xappURLConfig,
        finalPartId,
        state,
        itemBrand,
        undefined,
        undefined,
        undefined,
        multipackReferencesAPICall,
        partId,
        undefined,
        undefined,
        isPdpServiceEnabled,
        enablePDPServiceABTest,
        null,
        alternateBrand
      );
    }
    yield getProductDetails(productDetailArray);
    const multiPackData = newfetchDetailArray
      ? {
          newPartNumber: getMultipackProduct(newfetchDetailArray),
          multiPackCount,
          getMultiPackAllColor:
            productDetailArray && productDetailArray[0].product.getMultiPackAllColor,
          getNewMultiPackAllColor:
            newfetchDetailArray && newfetchDetailArray.product.getMultiPackAllColor,
          partIdDetail: newfetchDetailArray && newfetchDetailArray.product.partIdDetails,
        }
      : null;

    yield put(setMultiPackDetails({ multiPackData }));
    if (!isMobileApp()) {
      yield put(setLoadingState({ isLoading: false }));
    } else {
      yield put(setLoaderForQuickView(false));
      yield put(setModalState({ isModalOpen: true }));
    }
  } catch (err) {
    yield put(setLoadingError({ loadingError: true }));
    yield setQuickViewLoadingState(false, true);
    logger.error('fetchProductDetail: -', {
      error: err,
      extraData: {
        component: 'fetchProductDetail - Saga',
        payloadRecieved: payload,
      },
    });
  }
}

export function* fetchModuleX({ payload = '' }) {
  try {
    const result = yield call(getModuleX, payload);
    yield put(setModuleX(result));
  } catch (err) {
    yield null;
  }
}

function* QuickViewSaga() {
  yield takeLatest(QUICK_VIEW_CONSTANT.FETCH_QUICK_VIEW, fetchProductDetail);
  yield takeLatest(QUICK_VIEW_CONSTANT.FETCH_MODULEX_CONTENT, fetchModuleX);
}

export default QuickViewSaga;
