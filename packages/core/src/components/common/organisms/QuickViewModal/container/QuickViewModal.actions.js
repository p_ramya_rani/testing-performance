// 9fbef606107a605d69c0edbcd8029e5d
import QUICK_VIEW_CONSTANT from './QuickViewModal.constants';

export const setQuickView = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.SET_QUICK_VIEW,
    payload,
  };
};

export const setMultiPackDetails = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.SET_MULTIPRODUCT_DETAILS,
    payload,
  };
};

export const setDisableSelectedTab = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.SET_DISABLE_SELECTED_TAB,
    payload,
  };
};

export const setItemFromBagInfoForQuickView = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.SET_ITEM_FROM_BAG_INFO,
    payload,
  };
};

export const singlePageLoadAction = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.SINGLE_TAB_STORE,
    payload,
  };
};

export const quickViewSingleLoad = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.QUICK_SINGLE_TAB_STORE,
    payload,
  };
};

export const openQuickViewWithValues = (payload) => {
  return {
    payload,
    type: QUICK_VIEW_CONSTANT.FETCH_QUICK_VIEW,
  };
};

export const setModalState = (payload) => {
  return {
    payload,
    type: QUICK_VIEW_CONSTANT.OPEN_QUICK_VIEW_MODAL,
  };
};

export const updateMiniBagCloseStatusQV = (payload) => ({
  type: QUICK_VIEW_CONSTANT.UPDATE_MINIBAG_CLOSE_STATUS,
  payload,
});

export const setQuickViewProductDetailPageTitle = (payload) => {
  return {
    payload,
    type: QUICK_VIEW_CONSTANT.SET_QUICK_VIEW_PRODUCT_TITLE,
  };
};

export const setLoadingState = (payload) => {
  return {
    payload,
    type: QUICK_VIEW_CONSTANT.SET_LOADING_STATE,
  };
};

export const setLoadingError = (payload) => {
  return {
    payload,
    type: QUICK_VIEW_CONSTANT.SET_LOADING_ERROR,
  };
};

export const closeQuickViewModal = (payload) => {
  return {
    payload,
    type: QUICK_VIEW_CONSTANT.CLOSE_QUICK_VIEW_MODAL,
  };
};

export const updateAppTypeWithParams = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.UPDATE_APP_TYPE_AND_REDIRECT,
    payload,
  };
};

export const fetchModuleX = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.FETCH_MODULEX_CONTENT,
    payload,
  };
};

export const setModuleX = (payload) => {
  return {
    type: QUICK_VIEW_CONSTANT.SET_MODULEX_CONTENT,
    payload,
  };
};
