// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import getAddedToBagFormValues from '../../../../../reduxStore/selectors/form.selectors';
import { PRODUCT_ADD_TO_BAG } from '../../../../../constants/reducer.constants';

import { getLabelValue } from '../../../../../utils';

export const getProductInfo = (state) => {
  return state.QuickView.get('quickViewProducts');
};

export const getProductInfoFromBag = (state) => {
  return state.QuickView.get('quickViewProductFromBag');
};

export const getMultiPackProductInfo = (state) => {
  return state.QuickView.get('newPartNumber');
};

export const getDisableMultiPackTab = (state) => {
  return state && state.QuickView.get('disabledTab');
};

export const getInitialMultipackMappingQv = (state) => {
  return state && state.QuickView.get('availableTCPMultipackProductMapping');
};

export const getInitialAvailableTCPmapNewStyleId = (state) => {
  return state && state.QuickView.get('availableTCPmapNewStyleId');
};

export const getInitialMultipackMappingQvQuickView = (state) => {
  return state && state.QuickView.get('availableTCPMultipackProductMappingQuickView');
};
export const getSinglePageLoad = (state) => {
  return state && state.QuickView.get('singlepageload');
};

export const getInitialQuickViewLoad = (state) => {
  return state && state.QuickView.get('initalQuickViewLoad');
};

export const getInitialTCPStyleQTYQv = (state) => {
  return state && state.QuickView.get('initialSelectedQty');
};

export const getLoadingState = (state) => {
  return state.QuickView.get('isLoading');
};

export const getLoadingError = (state) => {
  return state.QuickView.get('isLoadingError');
};

export const getFromPage = (state) => {
  return state.QuickView.get('fromPage');
};

export const getFromBagPage = (state) => {
  return state.QuickView.get('fromBagPage');
};

export const getPdpUrlFromBagPage = (state) => {
  return state.QuickView.get('pdpUrl');
};

export const getIsFromBagProductSfl = (state) => {
  return state.QuickView.get('isSflProduct');
};

export const getEditFavorite = (state) => {
  return state.QuickView.get('isFavoriteEdit');
};

export const getQuickViewProductDetailPageTitle = (state) => {
  return state.QuickView.get('pageTitle');
};

export const getGeneralProductId = (state) => {
  return state.QuickView.getIn(['quickViewProducts', 'generalProductId']);
};

export const getModalState = (state) => {
  return state.QuickView.get('isModalOpen');
};

export const getUniqueId = (state) => {
  return state.QuickView.get('uniqueId');
};

export const getQuickViewFormValues = (state) => {
  const products = getProductInfo(state) || [];
  return products.map(({ product = {} } = {}) => {
    const { generalProductId } = product;
    return getAddedToBagFormValues(state, `${PRODUCT_ADD_TO_BAG}-${generalProductId}`);
  });
};

const getStateLabels = (state) => state.Labels;
export const getQuickViewLabels = createSelector(getStateLabels, (stateLabels) => {
  return {
    addToBag: getLabelValue(stateLabels, 'lbl_add_to_bag', 'QuickView', 'Browse'),
    editItem: getLabelValue(stateLabels, 'lbl_edit_item', 'QuickView', 'Browse'),
    editProduct: getLabelValue(stateLabels, 'lbl_edit_product', 'QuickView', 'Browse'),
    quickViewError: getLabelValue(stateLabels, 'lbl_quick_view_error', 'QuickView', 'Browse'),
    sorryErrorMsg: getLabelValue(
      stateLabels,
      'lbl_quick_view_sorry_message',
      'QuickView',
      'Browse'
    ),
    noProductSelected: getLabelValue(
      stateLabels,
      'plp_no_product_selected_error',
      'QuickView',
      'Browse'
    ),
    viewProductDetails: getLabelValue(
      stateLabels,
      'lbl_view_product_details',
      'QuickView',
      'Browse'
    ),
    availableInMoreColors: getLabelValue(
      stateLabels,
      'lbl_available_in_more_colors',
      'QuickView',
      'Browse'
    ),
    previousPrice: getLabelValue(stateLabels, 'lbl_previous_price', 'accessibility', 'global'),
    sizeGuide: getLabelValue(stateLabels, 'lbl_size_guide', 'CommonBrowseLabels', 'global'),
    shipIt: getLabelValue(stateLabels, 'lbl_ship_it', 'CommonBrowseLabels', 'global'),
    shipToHome: getLabelValue(stateLabels, 'lbl_ship_to_home', 'CommonBrowseLabels', 'global'),
    freeShipping: getLabelValue(stateLabels, 'lbl_free_shipping', 'CommonBrowseLabels', 'global'),
    noMinimumRequired: getLabelValue(stateLabels, 'lbl_no_min_req', 'CommonBrowseLabels', 'global'),
    pickItUp: getLabelValue(stateLabels, 'lbl_pick_it_up', 'CommonBrowseLabels', 'global'),
    pickUp: getLabelValue(stateLabels, 'lbl_pick_up', 'CommonBrowseLabels', 'global'),
    unavailable: getLabelValue(stateLabels, 'lbl_unavlbl', 'CommonBrowseLabels', 'global'),
    norushpickUp: getLabelValue(stateLabels, 'lbl_no_rush_pick_up', 'CommonBrowseLabels', 'global'),
    needItASAP: getLabelValue(stateLabels, 'lbl_need_it_asap', 'CommonBrowseLabels', 'global'),
    getItToday: getLabelValue(stateLabels, 'lbl_get_it_today', 'CommonBrowseLabels', 'global'),
    viewFullProductDetails: getLabelValue(
      stateLabels,
      'lbl_view_full_product_details',
      'CommonBrowseLabels',
      'global'
    ),
    deliveryMethod: getLabelValue(
      stateLabels,
      'lbl_delivery_method',
      'CommonBrowseLabels',
      'global'
    ),
    selectStore: getLabelValue(stateLabels, 'lbl_select_a_store', 'CommonBrowseLabels', 'global'),
    pickItUpInStore: getLabelValue(
      stateLabels,
      'lbl_pick_it_up_in_store',
      'CommonBrowseLabels',
      'global'
    ),
    extraPercentOff: getLabelValue(
      stateLabels,
      'lbl_extra_percent',
      'CommonBrowseLabels',
      'global'
    ),
  };
});
export const getQuickViewRichTextContentId = (state) => {
  let quickViewRichTextContentId;
  if (state.Labels.Browse.QuickView && Array.isArray(state.Labels.Browse.QuickView.referred)) {
    state.Labels.Browse.QuickView.referred.forEach((label) => {
      if (label.name === 'Quick View Promo') quickViewRichTextContentId = label.contentId;
    });
  }
  return quickViewRichTextContentId;
};

export const getQuickViewRichTextSelector = (state) => {
  return state.QuickView.get('quickViewPromo');
};

export const getSBPNavigationDetails = (state) => {
  return state.QuickView.get('sbpNavigation') || {};
};

export const getIsFromSBPPLP = (state) => {
  return state.QuickView.get('isSBP') || false;
};

export const getIsKeepMinibagCloseQV = (state) => {
  return state.QuickView.get('keepMinibagClose');
};
