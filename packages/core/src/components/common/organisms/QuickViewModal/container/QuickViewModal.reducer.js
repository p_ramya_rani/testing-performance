// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import QUICK_VIEW_CONSTANT from './QuickViewModal.constants';
import { DEFAULT_REDUCER_KEY } from '../../../../../utils/cache.util';

const initialState = fromJS({
  isLoading: false,
  [DEFAULT_REDUCER_KEY]: null,
});

function quickViewReducerBreak(state, action) {
  switch (action.type) {
    case QUICK_VIEW_CONSTANT.SINGLE_TAB_STORE:
      return state
        .set('singlepageload', action.payload.singlepageload)
        .set(
          'availableTCPMultipackProductMapping',
          action.payload.availableTCPMultipackProductMapping
        )
        .set('initialSelectedQty', action.payload.initialSelectedQty)
        .set(
          'availableTCPMultipackProductMappingQuickView',
          action.payload.availableTCPMultipackProductMapping
        )
        .set('availableTCPmapNewStyleId', action.payload.availableTCPmapNewStyleId);
    case QUICK_VIEW_CONSTANT.QUICK_SINGLE_TAB_STORE:
      return state.set('initalQuickViewLoad', action.payload.initalQuickViewLoad);

    case QUICK_VIEW_CONSTANT.SET_DISABLE_SELECTED_TAB:
      return state.set('disabledTab', action.payload.disabledTab);
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
}

const QuickViewReducer = (state = initialState, action) => {
  switch (action.type) {
    case QUICK_VIEW_CONSTANT.SET_QUICK_VIEW:
      return state.set('quickViewProducts', action.payload);

    case QUICK_VIEW_CONSTANT.SET_MULTIPRODUCT_DETAILS:
      return state.set('newPartNumber', action.payload);

    case QUICK_VIEW_CONSTANT.SET_ITEM_FROM_BAG_INFO:
      return state
        .set('fromBagPage', action.payload.fromBagPage)
        .set('isSflProduct', action.payload.isSflProduct)
        .set('quickViewProductFromBag', action.payload.orderInfo)
        .set('isFavoriteEdit', action.payload.isFavoriteEdit)
        .set('pdpUrl', action.payload.pdpUrl)
        .set('uniqueId', action.payload.uniqueId)
        .set('fromPage', action.payload.fromPage)
        .set('sbpNavigation', action.payload.sbpNavigation)
        .set('isSBP', action.payload.isSBP);
    case QUICK_VIEW_CONSTANT.SET_LOADING_STATE:
      return state.set('isLoading', action.payload.isLoading);
    case QUICK_VIEW_CONSTANT.SET_LOADING_ERROR:
      return state.set('isLoadingError', action.payload.loadingError);
    case QUICK_VIEW_CONSTANT.SET_MODULEX_CONTENT:
      return state.set('quickViewPromo', action.payload.richText);
    case QUICK_VIEW_CONSTANT.OPEN_QUICK_VIEW_MODAL:
      return state.set('isModalOpen', action.payload.isModalOpen).set('keepMinibagClose', true);
    case QUICK_VIEW_CONSTANT.UPDATE_MINIBAG_CLOSE_STATUS:
      return state.set('keepMinibagClose', action.payload);
    case QUICK_VIEW_CONSTANT.SET_QUICK_VIEW_PRODUCT_TITLE:
      return state.set('pageTitle', action.payload);
    case QUICK_VIEW_CONSTANT.CLOSE_QUICK_VIEW_MODAL:
      return state
        .set('fromBagPage', false)
        .set('isSflProduct', false)
        .set('isModalOpen', action.payload.isModalOpen)
        .set('quickViewProducts', null)
        .set('quickViewProductFromBag', null)
        .set('isFavoriteEdit', false)
        .set('fromPage', null);

    default:
      return quickViewReducerBreak(state, action);
  }
};

export default QuickViewReducer;
