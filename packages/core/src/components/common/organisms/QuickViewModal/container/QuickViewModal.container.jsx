/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import {
  getIsKeepAliveProduct,
  getIsShowPriceRange,
  getABTestIsShowPriceRange,
  getMultiPackThreshold,
  getIsDynamicBadgeEnabled,
  getIsNewReDesignEnabled,
  getIsNewQVEnabled,
  getIsNewBag,
  getIsCompleteTheLookTestEnabled,
  getABTestForPDPColorOrImageSwatchHover,
  getIsPDPSmoothScrollEnabled,
  getIsATBModalBackAbTestNewDesign,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import CheckoutConstants from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import checkoutSelector from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import { getAlternateBrandName } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import { getIsPickupModalOpen } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import {
  resetEddData,
  clearEddTTL,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.actions';
import {
  getDefaultFavItemInProgressFlag,
  wishListFromState,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.selectors';
import { updateMultipackSelection } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.actions';
import { setConfirmationDone } from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.actions';
import { getLabelsOutOfStock } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.selectors';
import { isClient, isMobileApp } from '@tcp/core/src/utils';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import { playHapticFeedback } from '@tcp/core/src/components/common/atoms/hapticFeedback/container/HapticFeedback.actions.native';
import QuickViewModal from '../views';
import {
  closeQuickViewModal,
  updateAppTypeWithParams,
  fetchModuleX,
  setQuickViewProductDetailPageTitle,
  openQuickViewWithValues,
  quickViewSingleLoad,
  setDisableSelectedTab,
} from './QuickViewModal.actions';
import {
  getAddedToBagError,
  getMultipleItemsAddedToBagError,
  getPDPlargeImageColorSelector,
} from '../../../../features/CnC/AddedToBag/container/AddedToBag.selectors';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import {
  getModalState,
  getProductInfo,
  getQuickViewLabels,
  getQuickViewFormValues,
  getProductInfoFromBag,
  getLoadingState,
  getFromBagPage,
  getFromPage,
  getIsFromBagProductSfl,
  getEditFavorite,
  getQuickViewRichTextContentId,
  getQuickViewRichTextSelector,
  getQuickViewProductDetailPageTitle,
  getUniqueId,
  getPdpUrlFromBagPage,
  getLoadingError,
  getMultiPackProductInfo,
  getInitialMultipackMappingQvQuickView,
  getSinglePageLoad,
  getInitialTCPStyleQTYQv,
  getInitialQuickViewLoad,
  getDisableMultiPackTab,
  getInitialAvailableTCPmapNewStyleId,
  getSBPNavigationDetails,
  getIsFromSBPPLP,
} from './QuickViewModal.selectors';
import {
  getPlpLabels,
  getProductInfoFromProductListingData,
  getAllMultiPackData,
  getPDPLabels,
  getSelectedMultipack,
  getInitialMultipackMapping,
  getAccessibilityLabels,
  getGeneralProductId,
  getStyliticsProductTabListSelector,
} from '../../../../features/browse/ProductDetail/container/ProductDetail.selectors';
import {
  addToCartEcom,
  addToCartEcomNewDesign,
  clearAddToBagErrorState,
  clearAddToCartMultipleItemErrorState,
} from '../../../../features/CnC/AddedToBag/container/AddedToBag.actions';
import { updateCartItem } from '../../../../features/CnC/CartItemTile/container/CartItemTile.actions';
import BAG_PAGE_ACTIONS from '../../../../features/CnC/BagPage/container/BagPage.actions';
import { getCartItemInfo } from '../../../../features/CnC/AddedToBag/util/utility';
import { updateWishListItemIdAction } from '../../../../features/browse/Favorites/container/Favorites.actions';
import CHECKOUT_ACTIONS from '../../../../features/CnC/Checkout/container/Checkout.action';

class QuickViewModalContainer extends React.Component {
  state = {
    retrievedProductInfo: null,
  };

  componentDidMount() {
    const { quickViewContentId, getRichText } = this.props;
    if (quickViewContentId) getRichText(quickViewContentId);
  }

  componentWillUnmount() {
    const { getDisableSelectedTab, getQickViewSingleLoad } = this.props;
    getQickViewSingleLoad({ initalQuickViewLoad: true });
    getDisableSelectedTab({ disabledTab: false });
  }

  static getDerivedStateFromProps = (props) => {
    const { productListingDetail, searchListingDetail } = props;

    const isSearchPage = isClient() && window.location.pathname.indexOf('/search/') !== -1;
    const dataToCarryForward = isSearchPage ? searchListingDetail : productListingDetail;

    const pid = props.uniqueId;
    if (typeof window !== 'undefined') {
      return {
        retrievedProductInfo: getProductInfoFromProductListingData({
          pid,
          details: dataToCarryForward && dataToCarryForward.loadedProductsPages,
        }),
      };
    }
    return null;
  };

  getOOSFlag = (productInfo) => {
    const { colorFitsSizesMap } = productInfo;
    const filteredColorFitsList =
      colorFitsSizesMap && colorFitsSizesMap.filter((item) => item.maxAvailable === 0);

    if (
      (colorFitsSizesMap && colorFitsSizesMap.length) ===
      (filteredColorFitsList && filteredColorFitsList.length)
    ) {
      return true;
    }
    return false;
  };

  handleUpdateItem = () => {
    const {
      updateCartItemAction,
      formValues,
      productInfo,
      closeQuickViewModalAction,
      isFromBagProductSfl,
      productInfoFromBag,
      updateCartSflItemAction,
      isFavoriteEdit,
      updateWishListItemFav,
      multipackProduct: { multiPackData },
    } = this.props;
    const [{ product }] = productInfo;
    const [formValue] = formValues;
    const cartItemInfo = getCartItemInfo(product, formValue);
    const {
      skuInfo: { skuId, variantNo, variantId, color, size },
    } = cartItemInfo;
    if (isFromBagProductSfl) {
      updateCartSflItemAction({
        oldSkuId: productInfoFromBag.skuId,
        newSkuId: skuId,
        callBack: closeQuickViewModalAction,
      });
    } else {
      const { skuInfo, quantity } = cartItemInfo;
      const { orderItemId } = productInfoFromBag;
      const payload = {
        skuId,
        itemId: orderItemId,
        quantity,
        variantNo,
        itemPartNumber: variantId,
        callBack: closeQuickViewModalAction,
        multipackProduct: null,
        multiPackCount: null,
        getAllMultiPack: null,
        partIdInfo: null,
        getAllNewMultiPack: null,
        size,
        skuInfo,
      };

      if (multiPackData) {
        payload.multipackProduct = multiPackData.newPartNumber;
        payload.multiPackCount = multiPackData.multiPackCount;
        payload.getAllMultiPack = multiPackData.getMultiPackAllColor;
        payload.partIdInfo = multiPackData.partIdDetail;
        payload.getAllNewMultiPack = multiPackData.getNewMultiPackAllColor;
      }

      if (isFavoriteEdit && updateWishListItemFav) {
        const formData = {
          itemId: orderItemId,
          quantity,
          color: color.name,
          product,
          callBack: closeQuickViewModalAction,
        };
        updateWishListItemFav(formData);
      } else {
        updateCartItemAction(payload);
      }
    }
  };

  /**
   *
   * @function getOriginPage
   * @description return OriginPage which is required to track GA analtycs
   */

  getOriginPage = (isSearchPage, fromPage) => {
    const originPage = isSearchPage ? 'slp' : 'plp';
    return isMobileApp() && fromPage ? fromPage.toLowerCase() : originPage;
  };

  isQualifiedRoute = (page) => {
    if (
      page === 'pdp' ||
      page === 'plp' ||
      page === 'productdetail' ||
      page === 'slp' ||
      page === 'sbppdp' ||
      page === 'cart' ||
      page === 'homepage' ||
      page === 'checkout'
    )
      return true;
    return false;
  };

  handleWhichAddToBagEcomWillBeCalled = (
    isNewReDesignQuickView,
    isATBModalBackAbTestNewDesign,
    fromPage,
    addToBagEcomNew,
    addToBagEcom,
    cartItemInfo
  ) => {
    if (
      isNewReDesignQuickView &&
      isATBModalBackAbTestNewDesign &&
      fromPage &&
      this.isQualifiedRoute(fromPage.toLowerCase())
    ) {
      const cartItemInfo1 = {
        ...cartItemInfo,
        isOpenNewATBModal: true,
      };
      addToBagEcomNew(cartItemInfo1);
    } else addToBagEcom(cartItemInfo);
  };

  handleAddToBag = () => {
    const {
      addToBagEcom,
      formValues,
      productInfo,
      closeQuickViewModalAction,
      fromPage,
      multipackProduct: { multiPackData },
      productInfoFromBag: { itemBrand: brand } = {},
      selectedMultipack,
      addToBagEcomNew,
      isNewReDesignQuickView,
      isATBModalBackAbTestNewDesign,
      currentCheckoutStage,
    } = this.props;
    const [{ product }] = productInfo;
    const [formValue] = formValues;
    const isSearchPage = isClient() && window.location.pathname.indexOf('/search/') !== -1;

    let cartItemInfo = getCartItemInfo(product, formValue);
    cartItemInfo = {
      ...cartItemInfo,
      callBack: closeQuickViewModalAction,
      fromPage,
      originPage: this.getOriginPage(isSearchPage, fromPage),
      multipackProduct: multiPackData ? multiPackData.newPartNumber : null,
      multiPackCount: multiPackData ? multiPackData.multiPackCount : null,
      getAllMultiPack: multiPackData ? multiPackData.getMultiPackAllColor : null,
      partIdInfo: multiPackData ? multiPackData.partIdDetail : null,
      getAllNewMultiPack: multiPackData ? multiPackData.getNewMultiPackAllColor : null,
      selectedMultipack,
      currentCheckoutStage,
    };
    if ((brand && !cartItemInfo.brand) || (product && product.primaryBrand)) {
      cartItemInfo.brand = brand || product.primaryBrand;
    }
    this.handleWhichAddToBagEcomWillBeCalled(
      isNewReDesignQuickView,
      isATBModalBackAbTestNewDesign,
      fromPage,
      addToBagEcomNew,
      addToBagEcom,
      cartItemInfo
    );
  };

  render() {
    const {
      isModalOpen,
      closeQuickViewModalAction,
      productInfo,
      plpLabels,
      currencyAttributes,
      toastMessage,
      quickViewRichText,
      pdpUrlFromBag,
      isLoadingError,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      multipackProduct,
      multiPackThreshold,
      multipackSelectionAction,
      initialMultipackMapping,
      singlePageLoad,
      setInitialTCPStyleQty,
      setInitialQuickViewLoad,
      getQickViewSingleLoad,
      disableMultiPackTab,
      getDisableSelectedTab,
      isDynamicBadgeEnabled,
      isNewReDesignQuickView,
      isNewQVEnabled,
      isNewBagEnabled,
      accessibilityLabels,
      isPDPSmoothScrollEnabled,
      defaultWishListFromState,
      deleteFavItemInProgressFlag,
      hasABTestForPDPColorOrImageSwatchHover,
      largeImageNameOnHover,
      isCompleteTheLookTestEnabled,
      itemPartNumber,
      completeLookSlotTile,
      fromBagPage,
      isPickupModal,
      alternateBrand,
      fromPage,
      fireHapticFeedback,
      sbpNavigation,
      isSBPPLP,
      currentCheckoutStage,
      ...otherProps
    } = this.props;
    const { retrievedProductInfo } = this.state;
    const checkForOOSForAllVariantsFlag = false;
    const props = {
      ...otherProps,
    };
    if (isMobileApp()) {
      props.fireHapticFeedback = fireHapticFeedback;
    }
    return (
      <QuickViewModal
        isNewReDesignQuickView={isNewReDesignQuickView}
        isModalOpen={isModalOpen}
        closeQuickViewModal={closeQuickViewModalAction}
        productInfo={productInfo}
        plpLabels={plpLabels}
        handleAddToBag={this.handleAddToBag}
        handleUpdateItem={this.handleUpdateItem}
        currencyAttributes={currencyAttributes}
        toastMessage={toastMessage}
        quickViewRichText={quickViewRichText}
        retrievedProductInfo={retrievedProductInfo}
        pdpUrlFromBag={pdpUrlFromBag}
        isLoadingError={isLoadingError}
        isShowPriceRangeKillSwitch={isShowPriceRangeKillSwitch}
        showPriceRangeForABTest={showPriceRangeForABTest}
        multiPackThreshold={multiPackThreshold}
        initialMultipackMapping={initialMultipackMapping}
        singlePageLoad={singlePageLoad}
        partIdInfo={
          multipackProduct &&
          multipackProduct.multiPackData &&
          multipackProduct.multiPackData.partIdDetail
        }
        getAllNewMultiPack={
          multipackProduct &&
          multipackProduct.multiPackData &&
          multipackProduct.multiPackData.getNewMultiPackAllColor
        }
        multipackProduct={
          multipackProduct &&
          multipackProduct.multiPackData &&
          multipackProduct.multiPackData.newPartNumber
        }
        multiPackCount={
          multipackProduct &&
          multipackProduct.multiPackData &&
          multipackProduct.multiPackData.multiPackCount
        }
        setSelectedMultipack={multipackSelectionAction}
        setInitialTCPStyleQty={setInitialTCPStyleQty}
        setInitialQuickViewLoad={setInitialQuickViewLoad}
        getQickViewSingleLoad={getQickViewSingleLoad}
        disableMultiPackTab={disableMultiPackTab}
        getDisableSelectedTab={getDisableSelectedTab}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        isNewQVEnabled={isNewQVEnabled}
        accessibilityLabels={accessibilityLabels}
        completeLookSlotTile={completeLookSlotTile}
        itemPartNumber={itemPartNumber}
        checkForOOSForVariant={checkForOOSForAllVariantsFlag}
        isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
        largeImageNameOnHover={largeImageNameOnHover}
        hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
        defaultWishListFromState={defaultWishListFromState}
        isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
        fromBagPage={fromBagPage}
        alternateBrand={alternateBrand}
        fromPage={fromPage}
        sbpNavigation={sbpNavigation}
        isSBPPLP={isSBPPLP}
        {...props}
      />
    );
  }
}

function mapStateToProps(state) {
  const productInfo = getProductInfo(state);
  const isMultiItemQVModal = productInfo && productInfo.length > 1;
  const { getCurrentCheckoutStage } = checkoutSelector;
  return {
    isModalOpen: getModalState(state),
    isLoading: getLoadingState(state),
    isLoadingError: getLoadingError(state),
    productInfo,
    isMultiItemQVModal,
    plpLabels: getPlpLabels(state),
    currency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    quickViewLabels: getQuickViewLabels(state),
    formValues: getQuickViewFormValues(state),
    addToBagError: getAddedToBagError(state) || '',
    addToBagMultipleItemError: getMultipleItemsAddedToBagError(state) || {},
    productInfoFromBag: getProductInfoFromBag(state),
    fromBagPage: getFromBagPage(state),
    fromPage: getFromPage(state),
    isFromBagProductSfl: getIsFromBagProductSfl(state),
    isFavoriteEdit: getEditFavorite(state),
    uniqueId: getUniqueId(state),
    quickViewContentId: getQuickViewRichTextContentId(state),
    quickViewRichText: getQuickViewRichTextSelector(state),
    quickViewProductDetailPageTitle: getQuickViewProductDetailPageTitle(state),
    productListingDetail: state && state.ProductListing.cacheUntil && state.ProductListing,
    searchListingDetail: state && state.SearchListingPage,
    isKeepAliveEnabled: getIsKeepAliveProduct(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    pdpUrlFromBag: getPdpUrlFromBagPage(state),
    isShowPriceRangeKillSwitch: getIsShowPriceRange(state),
    showPriceRangeForABTest: getABTestIsShowPriceRange(state),
    multipackProduct: getMultiPackProductInfo(state),
    multiPackThreshold: getMultiPackThreshold(state),
    getAllMultiPack: getAllMultiPackData(state),
    pdpLabels: getPDPLabels(state),
    selectedMultipack: getSelectedMultipack(state),
    initialMultipackMapping:
      getInitialMultipackMappingQvQuickView(state) || getInitialMultipackMapping(state),
    singlePageLoad: getSinglePageLoad(state),
    setInitialTCPStyleQty: getInitialTCPStyleQTYQv(state),
    setInitialQuickViewLoad: getInitialQuickViewLoad(state),
    disableMultiPackTab: getDisableMultiPackTab(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    availableTCPmapNewStyleId: getInitialAvailableTCPmapNewStyleId(state),
    isNewReDesignQuickView: getIsNewReDesignEnabled(state),
    isNewQVEnabled: getIsNewQVEnabled(state),
    isNewBagEnabled: getIsNewBag(state),
    accessibilityLabels: getAccessibilityLabels(state),
    completeLookSlotTile: getStyliticsProductTabListSelector(state),
    itemPartNumber: getGeneralProductId(state),
    isCompleteTheLookTestEnabled: getIsCompleteTheLookTestEnabled(state),
    largeImageNameOnHover: getPDPlargeImageColorSelector(state),
    hasABTestForPDPColorOrImageSwatchHover: getABTestForPDPColorOrImageSwatchHover(state),
    deleteFavItemInProgressFlag: getDefaultFavItemInProgressFlag(state),
    defaultWishListFromState: wishListFromState(state),
    isPDPSmoothScrollEnabled: getIsPDPSmoothScrollEnabled(state),
    isPickupModal: getIsPickupModalOpen(state),
    isATBModalBackAbTestNewDesign: getIsATBModalBackAbTestNewDesign(state),
    alternateBrand: getAlternateBrandName(state),
    currentCheckoutStage: getCurrentCheckoutStage(state),
    sbpNavigation: getSBPNavigationDetails(state),
    isSBPPLP: getIsFromSBPPLP(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeQuickViewModalAction: () => {
      dispatch(setQuickViewProductDetailPageTitle(''));
      dispatch(closeQuickViewModal({ isModalOpen: false }));
    },
    addToBagEcom: (payload) => {
      dispatch(addToCartEcom(payload));
      if (
        payload.currentCheckoutStage &&
        payload.currentCheckoutStage.toLowerCase() ===
          CheckoutConstants.CHECKOUT_STAGES.CONFIRMATION
      ) {
        dispatch(clearEddTTL());
        dispatch(resetEddData());
        dispatch(setConfirmationDone(true));
      }
    },
    addToBagEcomNew: (payload) => {
      dispatch(addToCartEcomNewDesign(payload));
    },
    clearAddToBagError: () => {
      dispatch(clearAddToBagErrorState());
    },
    clearMultipleItemsAddToBagError: () => {
      dispatch(clearAddToCartMultipleItemErrorState());
    },
    clearCheckoutServerError: (data) => dispatch(CHECKOUT_ACTIONS.setServerErrorCheckout(data)),
    updateCartItemAction: (payload) => {
      dispatch(updateCartItem(payload));
    },
    updateCartSflItemAction: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.updateSflItem(payload));
    },
    setCartItemsSflError: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.setCartItemsSflError(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    updateAppTypeHandler: (payload) => {
      dispatch(updateAppTypeWithParams(payload));
    },
    updateWishListItemFav: (payload) => {
      dispatch(updateWishListItemIdAction(payload));
    },
    getRichText: (cid) => {
      dispatch(fetchModuleX(cid));
    },
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    multipackSelectionAction: (payload) => {
      dispatch(updateMultipackSelection(payload));
    },
    getQickViewSingleLoad: (payload) => {
      dispatch(quickViewSingleLoad(payload));
    },
    getDisableSelectedTab: (payload) => {
      dispatch(setDisableSelectedTab(payload));
    },
    fireHapticFeedback: (payload) => {
      dispatch(playHapticFeedback(payload));
    },
  };
}

QuickViewModalContainer.propTypes = {
  plpLabels: PropTypes.shape({}),
  accessibilityLabels: PropTypes.shape({}),
  isModalOpen: PropTypes.bool.isRequired,
  formValues: PropTypes.shape([]).isRequired,
  closeQuickViewModalAction: PropTypes.func.isRequired,
  addToBagEcom: PropTypes.func.isRequired,
  updateCartItemAction: PropTypes.func.isRequired,
  productInfo: PropTypes.shape([PRODUCT_INFO_PROP_TYPE_SHAPE]).isRequired,
  productInfoFromBag: PropTypes.shape({}),
  currencyAttributes: PropTypes.shape({}),
  toastMessage: PropTypes.func,
  isFromBagProductSfl: PropTypes.bool,
  updateCartSflItemAction: PropTypes.func.isRequired,
  updateWishListItemFav: PropTypes.func,
  isFavoriteEdit: PropTypes.bool,
  quickViewContentId: PropTypes.string,
  quickViewRichText: PropTypes.string,
  getRichText: PropTypes.func,
  quickViewProductDetailPageTitle: PropTypes.string,
  pdpUrlFromBag: PropTypes.string,
  isLoadingError: PropTypes.bool,
  fromPage: PropTypes.string,
  isShowPriceRangeKillSwitch: PropTypes.bool.isRequired,
  showPriceRangeForABTest: PropTypes.bool.isRequired,
  multipackProduct: PropTypes.string.isRequired,
  multiPackThreshold: PropTypes.string.isRequired,
  getAllMultiPack: PropTypes.shape({}).isRequired,
  partIdInfo: PropTypes.string.isRequired,
  getAllNewMultiPack: PropTypes.shape({}).isRequired,
  multipackSelectionAction: PropTypes.shape({}).isRequired,
  selectedMultipack: PropTypes.string,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  singlePageLoad: PropTypes.bool.isRequired,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  setInitialQuickViewLoad: PropTypes.bool,
  getQickViewSingleLoad: PropTypes.func.isRequired,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isNewReDesignQuickView: PropTypes.bool,
  isNewQVEnabled: PropTypes.bool,
  itemPartNumber: PropTypes.string,
  isPDPSmoothScrollEnabled: PropTypes.bool,
  defaultWishListFromState: PropTypes.shape({}),
  deleteFavItemInProgressFlag: PropTypes.bool,
  hasABTestForPDPColorOrImageSwatchHover: PropTypes.bool,
  largeImageNameOnHover: PropTypes.string,
  isCompleteTheLookTestEnabled: PropTypes.bool,
  completeLookSlotTile: PropTypes.shape({}),
  fromBagPage: PropTypes.bool.isRequired,
  isPickupModal: PropTypes.bool.isRequired,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
  addToBagEcomNew: PropTypes.func,
  sbpNavigation: PropTypes.shape({}),
  isSBPPLP: PropTypes.bool,
};

QuickViewModalContainer.defaultProps = {
  productInfoFromBag: {},
  plpLabels: {},
  accessibilityLabels: {},
  currencyAttributes: {
    exchangevalue: 1,
  },
  toastMessage: () => {},
  isFromBagProductSfl: false,
  updateWishListItemFav: () => {},
  isFavoriteEdit: false,
  quickViewContentId: '',
  quickViewRichText: '',
  getRichText: () => {},
  quickViewProductDetailPageTitle: '',
  pdpUrlFromBag: '',
  isLoadingError: false,
  fromPage: '',
  selectedMultipack: '1',
  setInitialQuickViewLoad: true,
  isDynamicBadgeEnabled: false,
  isNewReDesignQuickView: false,
  isNewQVEnabled: false,
  itemPartNumber: '',
  isPDPSmoothScrollEnabled: false,
  defaultWishListFromState: {},
  deleteFavItemInProgressFlag: false,
  hasABTestForPDPColorOrImageSwatchHover: false,
  largeImageNameOnHover: '',
  isCompleteTheLookTestEnabled: false,
  completeLookSlotTile: {},
  isATBModalBackAbTestNewDesign: false,
  addToBagEcomNew: () => {},
  sbpNavigation: {},
  isSBPPLP: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(QuickViewModalContainer);

export { QuickViewModalContainer as QuickViewModalContainerVanilla };
