import PropTypes from 'prop-types';
import ModalBox from 'react-native-modalbox';
import React from 'react';
import { ScrollView, View } from 'react-native';
import {
  ModalBoxCloseImage,
  ProductDetailStyle,
  SizeDrawerUnderlay,
  HeaderView,
  PLPModalBoxBtn,
  TitleWrapper,
} from '../../../../../../features/browse/ProductDetail/styles/ProductDetail.style.native';
import { Anchor, BodyCopy } from '../../../../../atoms';
import Image from '../../../../../atoms/Image';
import {
  FooterWrapper,
  ProductDetailWrapper,
  ProductDetailWrapperIcon,
} from '../styles/QuickViewModalSheet.style';
import { getMapSliceForColorProductId } from '../../../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';

const closeImage = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/close_icon_drawer.png');
const arrowRight = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/icons-medium-left.png');

const QuickViewModalSheet = (props) => {
  const {
    isOpen,
    onClosed,
    children,
    quickViewLabels,
    product,
    navigation,
    newDesign,
    sbpNavigation,
    isSBPPLP,
  } = props;
  const hasMoreColors = false;
  const currentColorEntry =
    product && getMapSliceForColorProductId(product.colorFitsSizesMap, product.generalProductId);
  const goToPdp = () => {
    onClosed();
    if (isSBPPLP) {
      navigation.navigate('ProductDetailSBP', sbpNavigation);
    } else {
      navigation.navigate('ProductDetail', {
        title: product.name,
        pdpUrl: currentColorEntry.colorDisplayId,
        selectedColorProductId: currentColorEntry.colorProductId,
        reset: true,
      });
    }
  };

  if (!isOpen) return null;
  const drawerHeight = { maxHeight: '79%', padding: 0 };

  const paddingLeft = { paddingLeft: 0 };
  const paddingRight = { paddingRight: 0 };
  const CustomScrollStyle = { paddingBottom: 80, margin: 12, paddingLeft: 4, paddingRight: 4 };
  const customViewStyle = { flex: 0.2 };
  return (
    <ModalBox
      animationDuration={0}
      swipeThreshold={10}
      style={[ProductDetailStyle.modalBoxNew(), drawerHeight, paddingLeft, paddingRight]}
      position="bottom"
      swipeArea={400}
      isOpen={isOpen}
      entry="bottom"
      swipeToClose={false}
      coverScreen
      useNativeDriver
      backdropContent={<SizeDrawerUnderlay />}
      backdropOpacity={0.6}
      backdropPressToClose
      onClosed={() => onClosed()}
      onClosingState={() => onClosed()}
    >
      <HeaderView>
        <View style={customViewStyle} />
        <TitleWrapper>
          <BodyCopy
            text={product.name}
            color="gray.900"
            fontFamily="secondary"
            fontWeight="bold"
            textAlign="center"
            fontSize="fs12"
            numberOfLines={1}
            letterSpacing="ls05"
          />
        </TitleWrapper>
        <PLPModalBoxBtn
          accessibilityRole="button"
          hitSlop={{ top: 40, bottom: 40, left: 40, right: 40 }}
          onPress={onClosed}
        >
          <ModalBoxCloseImage source={closeImage} />
        </PLPModalBoxBtn>
      </HeaderView>

      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={CustomScrollStyle}>
        {children}
        <FooterWrapper newDesign={newDesign} hasMoreColors={hasMoreColors}>
          {hasMoreColors && (
            <BodyCopy
              text={quickViewLabels.availableInMoreColors}
              fontSize="fs14"
              fontFamily="secondary"
              fontWeight="bold"
              letterSpacing="ls05"
              color="gray.900"
            />
          )}
          <ProductDetailWrapper newDesign={newDesign}>
            <Anchor
              accessibilityRole="link"
              accessibilityLabel={quickViewLabels.viewProductDetails}
              text={quickViewLabels.viewProductDetails}
              fontWeight="regular"
              fontFamily="secondary"
              color="gray.900"
              fontSize="fs14"
              textAlign="right"
              underline
              onPress={() => goToPdp()}
              noLink
              to="/#"
              dataLocator=""
            />
            <ProductDetailWrapperIcon>
              <Image source={arrowRight} width={12} height={12} />
            </ProductDetailWrapperIcon>
          </ProductDetailWrapper>
        </FooterWrapper>
      </ScrollView>
    </ModalBox>
  );
};

QuickViewModalSheet.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClosed: PropTypes.bool.isRequired,
  children: PropTypes.shape({}).isRequired,
  quickViewLabels: PropTypes.shape({}).isRequired,
  product: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  newDesign: PropTypes.bool.isRequired,
  sbpNavigation: PropTypes.shape({}).isRequired,
  isSBPPLP: PropTypes.bool.isRequired,
};

export default QuickViewModalSheet;
