// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const FooterWrapper = styled.View`
  flex-direction: row;
  justify-content: ${(props) => (props.hasMoreColors ? 'space-between' : 'center')};
  margin: -10px 5px;
`;

export const ProductDetailWrapper = styled.View`
  flex-direction: row;
`;

export const ProductDetailWrapperIcon = styled.View`
  top: 5px;
`;
