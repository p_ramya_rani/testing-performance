// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';
import QuickViewModalSheet from '../QuickViewModalSheet.view.native';

describe('QuickViewModalSheet', () => {
  let component;

  beforeEach(() => {
    const props = {
      pdpLabels: {
        selectSizeLabel: '',
      },
      quickViewLabels: {
        viewProductDetails: '',
        availableInOtherColors: '',
      },
      product: {
        colorFitsSizesMap: [],
      },
    };
    component = shallow(<QuickViewModalSheet {...props} />);
  });

  it('QuickViewModalSheet should be defined', () => {
    expect(component).toBeDefined();
  });

  it('QuickViewModalSheet should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
