// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

export const QuickViewAddToBagButtonWrapper = styled.View`
  bottom: 0px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: ${props => props.theme.colors.WHITE};
  padding: ${props => props.theme.spacing.ELEM_SPACING.SM} 14px;
`;

export default {
  QuickViewAddToBagButtonWrapper,
};
