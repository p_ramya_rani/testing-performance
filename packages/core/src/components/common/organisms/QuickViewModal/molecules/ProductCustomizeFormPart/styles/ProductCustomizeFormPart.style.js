// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  flex-direction: column;
  display: flex;
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    padding: 0 8px;
  }
  .product-customize-form-container {
    padding-right: 0;
    flex-direction: column;
    display: flex;
    width: 100%;
  }
  .price-rating-section {
    display: flex;
    flex: 1;
    justify-content: space-between;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .rating-section-right {
    flex-shrink: 0;
    margin-right: 12px;
    .ratings-container {
      margin-right: 0px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-right: 14px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-right: 0px;
    }
  }
  /* stylelint-disable-next-line */
  _:-ms-fullscreen,
  .image-wrapper img {
    max-height: 320px;
  }
  .image-wrapper {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    width: ${(props) => (props.isMultiItemQVModal ? '112px' : '161px')};
  }
  .image-wrapper.quick-view-drawer-redesign {
    display: flex;
    flex: 1;
    align-items: center;
    width: 164px;
    height: 204px;
  }
  .image-wrapper.hide-item {
    display: none;
  }
  .product-link {
    text-decoration: underline;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};

    ${(props) =>
      props.isMultiItemQVModal
        ? `
        font-size:${props.theme.typography.fontSizes.fs10};

        @media ${props.theme.mediaQuery.mediumOnly} {
          font-size:${props.theme.typography.fontSizes.fs12};
        }
        @media ${props.theme.mediaQuery.large} {
          font-size:${props.theme.typography.fontSizes.fs14};
        }
        `
        : ``}
  }
  .link-redirect {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      align-self: flex-start;
    }
  }
  .product-detail {
    display: flex;
    flex: 1;
    flex-direction: row;
  }
  .product-name {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    padding-right: ${(props) =>
      props.isMultiItemQVModal ? props.theme.spacing.ELEM_SPACING.XXXL : 0};
  }
  .product-name.quick-view-drawer-redesign {
    display: none;
  }
  .product-details-card-container-separate {
    display: flex;
    flex: 1;
    flex-direction: column;
  }
  .product-details-card-container {
    display: none;
    .actual-price {
      color: ${(props) =>
        props.theme.isGymboree ? props.theme.colorPalette.gray[900] : props.theme.colorPalette.red};
    }
  }

  .image-title-wrapper {
    display: flex;
    flex-direction: row;
  }
  .image-title-wrapper.quick-view-drawer-redesign {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) =>
        props.fromBagPage
          ? props.theme.spacing.ELEM_SPACING.MED_1
          : props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .price-container {
    display: block;

    @media ${(props) => props.theme.mediaQuery.medium} {
      display: flex;
    }

    .actual-price {
      color: ${(props) =>
        props.theme.isGymboree ? props.theme.colorPalette.gray[900] : props.theme.colorPalette.red};
      font-size: ${(props) => props.theme.typography.fontSizes.fs22};
    }
    .original-price {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      color: ${(props) => props.theme.colorPalette.gray[800]};
      margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      line-height: 1.9;
    }
  }
  .multi-items-QV-product {
    .size-chart {
      display: ${(props) => (props.isMultiItemQVModal ? 'none' : 'block')};
    }

    ${(props) =>
      props.isMultiItemQVModal
        ? `
    display: flex;
    flex-direction: row;
    padding:${props.theme.spacing.ELEM_SPACING.LRG} ${props.theme.spacing.ELEM_SPACING.XXS} ${props.theme.spacing.ELEM_SPACING.SM} ${props.theme.spacing.ELEM_SPACING.XS};
    border-bottom: 1px solid ${props.theme.colors.BORDER.NORMAL};
    @media ${props.theme.mediaQuery.medium} {
      padding:${props.theme.spacing.ELEM_SPACING.SM} 0;
      border: 1px solid ${props.theme.colors.BORDER.NORMAL};
      margin-bottom: ${props.theme.spacing.ELEM_SPACING.XS};
      margin-top: ${props.theme.spacing.ELEM_SPACING.XS};
    }
    .select-value-wrapper > div:first-child {
      margin-top: 0;
    }
  `
        : `
    padding:0 6px;
    @media ${props.theme.mediaQuery.mediumOnly} {
      padding: 0px;
    }

  `}
    ${(props) =>
      props.formEnabled
        ? `
  opacity: 1;
  pointer-events: all;
`
        : `
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: ${props.theme.colorPalette.gray[500]};
    opacity: 0.25;
    pointer-events: none;
  `}
  }
  .price-container-bagPage {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG_2};
  }

  .inputCheckBox {
    position: relative;
    background-color: ${(props) => props.theme.colors.WHITE};
    width: 25px;
    margin-top: -25px;
    top: 48px;
    left: 88%;
    z-index: ${(props) => props.theme.zindex.zModal + 1};
    @media ${(props) => props.theme.mediaQuery.medium} {
      left: 93%;
      top: 39px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      left: 94%;
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .image-wrapper {
      width: ${(props) => (props.isMultiItemQVModal ? '260px' : '264px')};
    }
    .product-details-card-container-separate {
      display: none;
    }
    .product-details-card-container {
      display: block;
      padding-right: ${(props) =>
        props.isMultiItemQVModal ? props.theme.spacing.ELEM_SPACING.XXL : 0};
    }
    .product-detail {
      flex-direction: column;
    }
    .product-customize-form-container {
      flex-direction: ${(props) => (props.isNewQVEnabled ? 'column' : 'row')};
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .image-wrapper {
      width: 243px;
    }
    .product-details-card-container-separate {
      display: none;
    }
    .product-details-card-container {
      display: block;
      padding-right: ${(props) =>
        props.isMultiItemQVModal ? props.theme.spacing.ELEM_SPACING.XXL : 0};
    }
    .product-detail {
      flex-direction: column;
    }
    .product-customize-form-container {
      flex-direction: ${(props) => (props.isNewQVEnabled ? 'column' : 'row')};
    }
  }
`;

export const customPriceStyles = css`
  .list-badge-container {
    display: flex;
  }
`;

export const customSubmitButtonStyle = css`
  @media ${(props) => props.theme.mediaQuery.large} {
    .add-to-bag-button {
      width: 343px;
    }
  }
`;

export default styles;
