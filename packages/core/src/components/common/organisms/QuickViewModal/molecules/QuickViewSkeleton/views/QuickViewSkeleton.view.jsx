// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { fromJS } from 'immutable';
import PropTypes from 'prop-types';
import ProductPrice from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductPrice/ProductPrice';
import ProductColorChipsSelector from '@tcp/core/src/components/common/molecules/ProductColorChipSelector/';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { parseBoolean, getDynamicBadgeQty, getImageData } from '@tcp/core/src/utils';
import { getMapSliceForColorProductId } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import withStyles from '../../../../../hoc/withStyles';
import { Row, DamImage, BodyCopy } from '../../../../../atoms';
import styles from '../styles/QuickViewSkeleton.style';

const colorFitSizeDisplayNames = {
  color: 'Color',
  fit: 'Fit',
  size: 'Size',
};

const checkListPrice = (showPriceRange, listPrice, priceRange) => {
  return showPriceRange ? listPrice : priceRange.lowListPrice;
};

const checkOfferPrice = (showPriceRange, offerPrice, priceRange) => {
  return showPriceRange ? offerPrice : priceRange.lowOfferPrice;
};

const getImagePath = (retrievedProductInfo, colorFit, isImageAvailable) => {
  return (
    retrievedProductInfo &&
    colorFit &&
    colorFit[0] &&
    isImageAvailable &&
    `${isImageAvailable.split('_')[0]}/${isImageAvailable}`
  );
};

const getIsImageAvailable = (colorFit) =>
  colorFit &&
  colorFit.length > 0 &&
  (colorFit[0].productImage || `${colorFit[0].colorProductId}.jpg`);

const BaseStructureQVSkeleton = ({ className }) => {
  return (
    <div className={className}>
      <Row fullBleed className="product-detail-skeleton-wrapper">
        <div className="product-main-image">
          <LoaderSkelton width="100%" height="200px" />
        </div>
        <div className="product-overview-wrapper">
          <div className="product-title">
            <LoaderSkelton width="100%" height="50px" />
          </div>
          <div className="product-price">
            <LoaderSkelton width="70%" height="50px" />
          </div>
          <div className="product-color">
            <LoaderSkelton width="100%" height="70px" />
          </div>
          <div className="product-size">
            <LoaderSkelton />
          </div>
          <div className="product-add-to-bag">
            <LoaderSkelton />
          </div>
        </div>
      </Row>
    </div>
  );
};

BaseStructureQVSkeleton.propTypes = {
  className: PropTypes.string.isRequired,
};

const BagPageProductQVSkeleton = ({ className, productInfoFromBag, currencyAttributes }) => {
  const { name, imagePath, tcpStyleType, tcpStyleQty, isGiftCard, listPrice, offerPrice } =
    productInfoFromBag;
  const imgData = {
    alt: name,
    url: imagePath,
  };
  const configData = getImageData(imgData.url);
  const newQuickViewClass = 'quick-view-drawer-redesign';
  return (
    <div className={className}>
      <Row fullBleed className={`product-detail-skeleton-wrapper ${newQuickViewClass}`}>
        <div className={`product-main-image ${newQuickViewClass}`}>
          <DamImage
            className="full-size-desktop-image"
            imgData={imgData}
            itemProp="contentUrl"
            isProductImage
            lazyLoad={false}
            imgConfigs={configData.imgConfig}
            isOptimizedVideo
            dynamicBadgeOverlayQty={getDynamicBadgeQty(tcpStyleType, tcpStyleQty, false)}
            tcpStyleType={tcpStyleType}
            badgeConfig={configData.badgeConfig}
          />
        </div>
        <div className="product-overview-wrapper">
          <div className={`product-price ${newQuickViewClass}`}>
            <ProductPrice
              listPrice={listPrice}
              offerPrice={offerPrice}
              hideItemProp
              currencyAttributes={currencyAttributes}
              isGiftCard={isGiftCard}
            />
          </div>
          <div className="product-color">
            <LoaderSkelton />
          </div>
          <div className="product-size">
            <LoaderSkelton />
          </div>
          <div className="product-add-to-bag">
            <LoaderSkelton />
          </div>
        </div>
      </Row>
    </div>
  );
};

BagPageProductQVSkeleton.propTypes = {
  className: PropTypes.string.isRequired,
};

const QuickViewSkeleton = ({
  className,
  retrievedProductInfo,
  currencyAttributes,
  isShowPriceRangeKillSwitch,
  showPriceRangeForABTest,
  isDynamicBadgeEnabled,
  isNewQVEnabled,
  productInfoFromBag,
  fromBagPage,
  primaryBrand,
  alternateBrand,
}) => {
  if (productInfoFromBag && isNewQVEnabled && fromBagPage) {
    return (
      <BagPageProductQVSkeleton
        className={className}
        productInfoFromBag={productInfoFromBag}
        currencyAttributes={currencyAttributes}
      />
    );
  }
  if (!retrievedProductInfo) {
    return <BaseStructureQVSkeleton className={className} />;
  }
  const {
    name,
    colorFitsSizesMap,
    uniqueId,
    imagesByColor,
    listPrice,
    offerPrice,
    priceRange,
    isGiftCard,
    tcpStyleQty,
    tcpStyleType,
  } = retrievedProductInfo;
  const currentColorEntry = getMapSliceForColorProductId(colorFitsSizesMap, uniqueId) || {};
  const showPriceRange =
    parseBoolean(isShowPriceRangeKillSwitch) && showPriceRangeForABTest && priceRange;
  const getlistPriceLow = checkListPrice(showPriceRange, listPrice, priceRange);
  const getofferpriceLow = checkOfferPrice(showPriceRange, offerPrice, priceRange);
  const colorFit = retrievedProductInfo.colorFitsSizesMap;
  const isImageAvailable = getIsImageAvailable(colorFit);
  const imagePath = getImagePath(retrievedProductInfo, colorFit, isImageAvailable);
  const imgData = {
    alt: retrievedProductInfo.name,
    url: imagePath,
  };
  const configData = getImageData(imgData.url);
  const colorFitsSizesMapList = fromJS(colorFitsSizesMap);
  const newQuickViewClass = `${isNewQVEnabled ? 'quick-view-drawer-redesign' : ''}`;
  return (
    <div className={className}>
      <Row fullBleed className={`product-detail-skeleton-wrapper ${newQuickViewClass}`}>
        <div className={`product-main-image ${newQuickViewClass}`}>
          <DamImage
            className="full-size-desktop-image"
            imgData={imgData}
            itemProp="contentUrl"
            isProductImage
            lazyLoad={false}
            imgConfigs={configData.imgConfig} // TODO check for imageConfig in case of having cloudinary name form for bundle
            isOptimizedVideo
            dynamicBadgeOverlayQty={getDynamicBadgeQty(
              tcpStyleType,
              tcpStyleQty,
              isDynamicBadgeEnabled
            )}
            tcpStyleType={tcpStyleType}
            badgeConfig={configData.badgeConfig}
            primaryBrand={primaryBrand || alternateBrand}
          />
        </div>
        <div className="product-overview-wrapper">
          {!isNewQVEnabled ? (
            <div>
              <BodyCopy
                className="product-title hide-on-mobile"
                fontSize="fs18"
                component="h1"
                fontFamily="secondary"
                fontWeight="extrabold"
                itemprop="name"
              >
                {name}
              </BodyCopy>
            </div>
          ) : null}
          <div className={`product-price ${newQuickViewClass}`}>
            <ProductPrice
              listPrice={getlistPriceLow}
              offerPrice={getofferpriceLow}
              hideItemProp // prop is used to hide unwanted duplicate itemProp as we have duplicate ProductPrice component for web and mobile on PDP pages
              currencyAttributes={currencyAttributes}
              isGiftCard={isGiftCard}
              highOfferPrice={showPriceRange && priceRange.highOfferPrice}
              highListPrice={showPriceRange && priceRange.highListPrice}
            />
          </div>
          {!isNewQVEnabled ? (
            <div className="product-color">
              <ProductColorChipsSelector
                isGiftCard={isGiftCard}
                colorFitsSizesMap={colorFitsSizesMapList}
                title={`${colorFitSizeDisplayNames.color}:`}
                imagesByColor={imagesByColor}
                input={{ value: currentColorEntry && currentColorEntry.color }}
                primaryBrand={primaryBrand}
                alternateBrand={alternateBrand}
              />
            </div>
          ) : null}

          <div className="product-size">
            <LoaderSkelton />
          </div>
          <div className="product-add-to-bag">
            <LoaderSkelton />
          </div>
        </div>
      </Row>
    </div>
  );
};

QuickViewSkeleton.propTypes = {
  className: PropTypes.string.isRequired,
  retrievedProductInfo: PropTypes.string.isRequired,
  currencyAttributes: PropTypes.string.isRequired,
  isShowPriceRangeKillSwitch: PropTypes.bool.isRequired,
  showPriceRangeForABTest: PropTypes.bool.isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isNewQVEnabled: PropTypes.bool,
  fromBagPage: PropTypes.bool,
};
QuickViewSkeleton.defaultProps = {
  isDynamicBadgeEnabled: false,
  isNewQVEnabled: false,
  fromBagPage: false,
};
export default withStyles(QuickViewSkeleton, styles);
export { QuickViewSkeleton as QuickViewSkeletonVanilla };
