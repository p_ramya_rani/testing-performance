// 9fbef606107a605d69c0edbcd8029e5d 
const badgeTranformation = 'g_west,w_0.22,fl_relative';

export default {
  imgConfig: [`t_plp_img_m`, `t_plp_img_t`, `t_plp_img_d`],
  badgeConfig: [badgeTranformation, badgeTranformation, badgeTranformation],
};
