// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { disableTcpAppProduct, isMobileApp } from '@tcp/core/src/utils';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import { COLOR_FITS_SIZES_MAP_PROP_TYPE } from '../../../../PickupStoreModal/PickUpStoreModal.proptypes';
import ProductCustomizeFormPart from '../views/ProductCustomizeFormPart.view';
import { routerPush, getBrand } from '../../../../../../../utils';
import { APP_TYPE } from '../../../../../../features/CnC/Checkout/Checkout.constants';
import {
  getMapSliceForColorProductId,
  getMapSliceForColor,
} from '../../../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';

class ProductCustomizeFormPartContainer extends React.Component {
  constructor(props) {
    super(props);
    const { productInfo, colorFitsSizesMap } = this.props;
    this.onInputSelectionChange = this.onInputSelectionChange.bind(this);
    this.state = {
      formEnabled: true,
      currentColorEntry: getMapSliceForColorProductId(
        colorFitsSizesMap,
        productInfo.generalProductId
      ),
    };
  }

  goToRegularPDP = () => {
    const { navigation } = this.props;
    if (navigation?.state?.routeName === 'Home') return true;
    return (
      Object.keys(navigation?.state?.params).includes('navigationObj') ||
      navigation?.state?.params?.title
    );
  };

  onChangeColor = (e) => {
    const { colorFitsSizesMap } = this.props;
    this.setState({ currentColorEntry: getMapSliceForColor(colorFitsSizesMap, e) });
  };

  onInputSelectionChange = () => {
    this.setState((oldState) => ({ formEnabled: !oldState.formEnabled }));
    const { changeQuickViewState } = this.props;
    const { formEnabled } = this.state;
    if (!formEnabled) {
      changeQuickViewState(false);
    }
  };

  goToPDPPage = (e, pdpToPath, currentColorPdpUrl) => {
    e.preventDefault();
    const { onCloseClick } = this.props;
    routerPush(pdpToPath, currentColorPdpUrl);
    onCloseClick();
  };

  goToPDPPageMobile = (pdpUrl, selectedColorProductId, name) => {
    const {
      navigation,
      onCloseClick,
      updateAppTypeHandler,
      productInfoFromBag,
      fromBagPage,
      quickViewProductDetailPageTitle,
    } = this.props;

    const currentAppBrand = getBrand();
    let isProductBrandOfSameDomain = true;
    if (fromBagPage) {
      isProductBrandOfSameDomain =
        currentAppBrand.toUpperCase() ===
        (productInfoFromBag.itemBrand && productInfoFromBag.itemBrand.toUpperCase());
    }

    const title = quickViewProductDetailPageTitle === '' ? name : quickViewProductDetailPageTitle;
    const params = {
      title,
      pdpUrl,
      selectedColorProductId,
      reset: true,
    };
    onCloseClick();
    if (!isProductBrandOfSameDomain) {
      if (disableTcpAppProduct(productInfoFromBag.itemBrand)) {
        return;
      }
      updateAppTypeHandler({
        type: currentAppBrand.toLowerCase() === APP_TYPE.TCP ? APP_TYPE.GYMBOREE : APP_TYPE.TCP,
        params: {
          title,
          pdpUrl,
          selectedColorProductId,
          reset: true,
        },
      });
    } else if (this.goToRegularPDP()) {
      navigation.navigate('ProductDetail', params);
    } else {
      params.retrievedProductInfo = null;
      navigation.navigate('ProductDetailSBP', params);
    }
  };

  getFormName = (formValues) => {
    return formValues && formValues.color;
  };

  render() {
    const {
      productInfo,
      formRef,
      currencyAttributes,
      onCloseClick,
      isMultiItemQVModal,
      quickViewRichText,
      retrievedProductInfo,
      colorFitsSizesMap,
      formValues,
      isNewQVEnabled,
      accessibilityLabels,
      completeLookSlotTile,
      itemPartNumber,
      checkForOOSForAllVariantsFlag,
      isCompleteTheLookTestEnabled,
      largeImageNameOnHover,
      hasABTestForPDPColorOrImageSwatchHover,
      deleteFavItemInProgressFlag,
      defaultWishListFromState,
      isPDPSmoothScrollEnabled,
      onPickupClickAddon,
      isNewReDesignEnabled,
      isDynamicBadgeEnabled,
      primaryBrand,
    } = this.props;
    const { currentColorEntry, formEnabled } = this.state;
    const getFormName = formValues && formValues[0] && formValues[0].color;
    let updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, getFormName);
    if (isMobileApp()) {
      const colorNameAvailable = this.getFormName(formValues);
      updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, colorNameAvailable);
    }
    const newCurrentColorEntry = !updatedColorEntry ? currentColorEntry : updatedColorEntry;
    const imageUrl = newCurrentColorEntry
      ? productInfo.imagesByColor[newCurrentColorEntry.color.name] &&
        productInfo.imagesByColor[newCurrentColorEntry.color.name].basicImageUrl
      : null;
    return (
      <ProductCustomizeFormPart
        {...this.props}
        imageUrl={imageUrl}
        currentColorEntry={newCurrentColorEntry}
        onChangeColor={this.onChangeColor}
        goToPDPPage={this.goToPDPPage}
        goToPDPPageMobile={this.goToPDPPageMobile}
        onInputSelectionChange={this.onInputSelectionChange}
        formRef={formRef}
        formEnabled={formEnabled}
        currencyAttributes={currencyAttributes}
        onCloseClick={onCloseClick}
        isMultiItemQVModal={isMultiItemQVModal}
        quickViewRichText={quickViewRichText}
        retrievedProductInfo={retrievedProductInfo}
        isNewQVEnabled={isNewQVEnabled}
        accessibilityLabels={accessibilityLabels}
        completeLookSlotTile={completeLookSlotTile}
        itemPartNumber={itemPartNumber}
        checkForOOSForAllVariantsFlag={checkForOOSForAllVariantsFlag}
        isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
        largeImageNameOnHover={largeImageNameOnHover}
        hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
        defaultWishListFromState={defaultWishListFromState}
        isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
        onPickupClickAddon={onPickupClickAddon}
        isNewReDesignEnabled={isNewReDesignEnabled}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        primaryBrand={primaryBrand}
      />
    );
  }
}

ProductCustomizeFormPartContainer.propTypes = {
  plpLabels: PropTypes.shape({}).isRequired,
  accessibilityLabels: PropTypes.shape({}).isRequired,
  onCloseClick: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}),
  formRef: PropTypes.shape({}),
  handleAddToBag: PropTypes.func.isRequired,
  formValues: PropTypes.shape([]).isRequired,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  colorFitsSizesMap: COLOR_FITS_SIZES_MAP_PROP_TYPE.isRequired,
  productInfo: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,
  currency: PropTypes.string,
  addToBagError: PropTypes.string,
  currencyAttributes: PropTypes.shape({}),
  changeQuickViewState: PropTypes.bool.isRequired,
  isMultiItemQVModal: PropTypes.bool.isRequired,
  updateAppTypeHandler: PropTypes.func.isRequired,
  productInfoFromBag: PropTypes.shape({}).isRequired,
  fromBagPage: PropTypes.bool.isRequired,
  quickViewRichText: PropTypes.string,
  quickViewProductDetailPageTitle: PropTypes.string.isRequired,
  retrievedProductInfo: PropTypes.string.isRequired,
  isNewQVEnabled: PropTypes.bool,
  itemPartNumber: PropTypes.string,
  isPDPSmoothScrollEnabled: PropTypes.bool,
  defaultWishListFromState: PropTypes.shape({}),
  deleteFavItemInProgressFlag: PropTypes.bool,
  hasABTestForPDPColorOrImageSwatchHover: PropTypes.bool,
  largeImageNameOnHover: PropTypes.string,
  isCompleteTheLookTestEnabled: PropTypes.bool,
  completeLookSlotTile: PropTypes.shape({}),
  isDynamicBadgeEnabled: PropTypes.shape({}),
  checkForOOSForAllVariantsFlag: PropTypes.bool.isRequired,
  onPickupClickAddon: PropTypes.func,
  isNewReDesignEnabled: PropTypes.bool,
};

ProductCustomizeFormPartContainer.defaultProps = {
  currency: 'USD',
  addToBagError: '',
  navigation: {},
  formRef: {},
  currencyAttributes: {},
  quickViewRichText: '',
  isNewQVEnabled: false,
  itemPartNumber: '',
  isPDPSmoothScrollEnabled: false,
  defaultWishListFromState: {},
  deleteFavItemInProgressFlag: false,
  hasABTestForPDPColorOrImageSwatchHover: false,
  largeImageNameOnHover: '',
  isCompleteTheLookTestEnabled: false,
  completeLookSlotTile: {},
  isDynamicBadgeEnabled: {},
  onPickupClickAddon: () => {},
  isNewReDesignEnabled: false,
};

export default ProductCustomizeFormPartContainer;
export { ProductCustomizeFormPartContainer as ProductCustomizeFormPartContainerVanilla };
