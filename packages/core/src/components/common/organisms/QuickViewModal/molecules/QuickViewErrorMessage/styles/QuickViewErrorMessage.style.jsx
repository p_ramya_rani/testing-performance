// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .size-error {
    color: ${props => props.theme.colors.NOTIFICATION.ERROR};
    width: 100%;
    font-size: ${props => props.theme.fonts.fontSize.body.small.secondary}px;
    font-weight: 800;
  }
  .size-error-message {
    color: ${props => props.theme.colors.NOTIFICATION.ERROR};
    width: 100%;
    font-size: ${props => props.theme.fonts.fontSize.heading.small.h6}px;
    font-weight: 800;
    text-align: center;
    margin-top: 150px;
    margin-bottom: 200px;
  }
`;
