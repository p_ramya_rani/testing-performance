// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../../hoc/withStyles';
import { BodyCopy } from '../../../../../atoms';
import styles from '../styles/QuickViewErrorMessage.style';

const ErrorComp = (quickViewError) => {
  return quickViewError ? (
    <BodyCopy
      className="size-error"
      fontSize="fs12"
      component="div"
      fontFamily="secondary"
      fontWeight="regular"
      role="alert"
      aria-live="assertive"
    >
      <BodyCopy
        className="size-error-message"
        fontSize="fs12"
        component="div"
        fontFamily="secondary"
        fontWeight="regular"
      >
        {quickViewError}
      </BodyCopy>
    </BodyCopy>
  ) : null;
};

const QuickViewErrorMessage = ({ className, quickViewLabels }) => {
  const { quickViewError } = quickViewLabels;
  return (
    <BodyCopy className={className} component="div">
      <BodyCopy component="div">{ErrorComp(quickViewError)}</BodyCopy>
    </BodyCopy>
  );
};

QuickViewErrorMessage.propTypes = {
  className: PropTypes.string.isRequired,
  quickViewLabels: PropTypes.shape(),
};

QuickViewErrorMessage.defaultProps = {
  quickViewLabels: {
    quickViewError:
      'Your action could not be completed due to a system error. Please try again later.',
  },
};

export default withStyles(QuickViewErrorMessage, styles);
