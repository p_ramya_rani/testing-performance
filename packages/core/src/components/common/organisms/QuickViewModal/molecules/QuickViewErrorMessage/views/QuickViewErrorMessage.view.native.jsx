// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper/styledWrapper.native';

const QuickViewErrorMessage = ({ quickViewLabels }) => {
  const { quickViewError } = quickViewLabels;
  return (
    <View>
      {quickViewError && (
        <BodyCopyWithSpacing
          fontFamily="secondary"
          fontSize="fs16"
          fontWeight="bold"
          color="error"
          text={quickViewError}
          margin="200px 14px"
          textAlign="center"
        />
      )}
    </View>
  );
};

QuickViewErrorMessage.propTypes = {
  quickViewLabels: PropTypes.shape({}),
};

QuickViewErrorMessage.defaultProps = {
  quickViewLabels: {
    quickViewError:
      'Your action could not be completed due to a system error. Please try again later.',
  },
};

export default QuickViewErrorMessage;
