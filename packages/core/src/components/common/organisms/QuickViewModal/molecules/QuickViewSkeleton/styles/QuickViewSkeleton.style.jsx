// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  padding: 10px 15px 20px 15px;
  background: ${(props) => props.theme.colors.WHITE};
  margin: 15px 0;
  .product-detail-skeleton-wrapper {
    display: flex;
    flex-direction: row;
  }
  .product-detail-skeleton-wrapper.quick-view-drawer-redesign {
    display: flex;
    flex-direction: column;
  }
  .product-main-image {
    height: 325px;
    margin-right: 10px;
    width: 164px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 264px;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 239px;
    }
  }
  .product-main-image.quick-view-drawer-redesign {
    margin: 15px auto;
    justify-content: center;
    align-items: center;
    height: 204px;
    width: 164px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 282px;
      height: auto;
      margin: 0 auto;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 282px;
      height: auto;
      margin: 0 auto;
    }
  }
  .product-overview-wrapper {
    display: flex;
    flex: 1;
    flex-direction: column;
    margin-left: 0;
  }
  .product-title {
    margin-bottom: 20px;
  }
  .product-price {
    margin-bottom: 30px;
  }
  .product-color {
    margin-bottom: 20px;
  }
  .product-size {
    width: 100%;
    height: 70px;
    margin-bottom: 40px;
  }
  .product-add-to-bag {
    width: 70%;
    height: 50px;
  }
`;
