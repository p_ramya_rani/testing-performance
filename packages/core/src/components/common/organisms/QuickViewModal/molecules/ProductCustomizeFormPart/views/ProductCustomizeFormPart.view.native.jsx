/* eslint-disable max-lines */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import { WebView } from 'react-native-webview';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import colors from '@tcp/core/styles/themes/TCP/colors';
import {
  disableTcpAppProduct,
  isGymboree,
  getDynamicBadgeQty,
  getVideoUrl,
  isAndroid,
} from '@tcp/core/src/utils';
import PAGES from '@tcp/core/src/constants/pages.constants';
import ProductRating from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductRating/ProductRating.view.native';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import { BodyCopy, Anchor, DamImage } from '../../../../../atoms';
import {
  PickUpSkUSectionContainer,
  ImageWrapper,
  ProductSummaryContainer,
  ProductDetailSummary,
  OfferPriceAndBadge3Container,
  ListPriceAndBadgeContainer,
  MultiItemQVWrapper,
  InputCheckboxWrapper,
  AfterPayWrapper,
} from '../styles/ProductCustomizeFormPart.style.native';
import { BodyCopyWithSpacing } from '../../../../../atoms/styledWrapper';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import ProductAddToBagContainer from '../../../../../molecules/ProductAddToBag/container/ProductAddToBag.container';
import InputCheckbox from '../../../../../atoms/InputCheckbox';
import config from '../../../../../molecules/ProductDetailImage/config';
import { SNJ } from '../../../../../../../constants/brands.constants';

import {
  getPrices,
  getProductListToPathInMobileApp,
} from '../../../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';

const VID_DATA_PLP_WEBP_APP = 'e_loop,t_plp_webp_m,dpr_2.0';
const VID_DATA_PLP_GIF_APP = 't_plp_gif_app,dpr_2.0';

const handleFormSubmit = (fromBagPage, handleUpdateItem, handleAddToBag, isFavoriteEdit) => {
  const updateAddedItem = fromBagPage || isFavoriteEdit;
  return updateAddedItem ? handleUpdateItem : handleAddToBag;
};
const getPointerEvents = (formEnabled) => (formEnabled ? 'auto' : 'none');
const getCurrencySymbol = (currency) => (currency === 'USD' ? '$' : currency);

const getItemBrand = (productInfoFromBag) => {
  return !isEmpty(productInfoFromBag) ? productInfoFromBag.itemBrand : null;
};

const renderProductDetailLink = ({
  modifiedPdpUrl,
  colorProductId,
  name,
  quickViewLabels,
  goToPDPPageMobile,
  itemBrand,
}) => {
  let componentContainer = null;
  if (!disableTcpAppProduct(itemBrand)) {
    componentContainer = (
      <Anchor noLink onPress={() => goToPDPPageMobile(modifiedPdpUrl, colorProductId, name)}>
        <BodyCopy
          fontSize="fs14"
          fontWeight="regular"
          fontFamily="secondary"
          textDecoration="underline"
          text={quickViewLabels.viewProductDetails}
        />
      </Anchor>
    );
  }
  return <React.Fragment>{componentContainer}</React.Fragment>;
};

const getPriceColor = () => {
  return isGymboree() ? 'gray.900' : 'red.500';
};

const getGiftCardPrice = (fromBagPage, isGiftCard, productInfoFromBag) => {
  if (fromBagPage && isGiftCard) {
    return parseInt(productInfoFromBag.selectedSize, 10);
  }
  return null;
};

const getGiftCardOrOfferPrice = (giftCardPrice, offerPrice) => {
  return giftCardPrice || parseFloat(offerPrice);
};

const getAccessibilityText = (quickViewLabels, currencyPrefix, floatingListPrice) => {
  return `${
    quickViewLabels && quickViewLabels.previousPrice
  } ${currencyPrefix}${floatingListPrice.toFixed(2)}`;
};

const renderProductRatings = (productInfo) => {
  const ratings = (productInfo && productInfo.ratings) || 0;
  const reviewsCount = (productInfo && productInfo.reviewsCount) || 0;
  return !!ratings && <ProductRating ratings={ratings || 0} reviewsCount={reviewsCount} />;
};

const getInitialFitValue = (isGiftCard) => {
  return !isGiftCard ? 'regular' : '';
};

const isFromPagePLPSLP = (fromPage) => {
  const props = {};
  if (fromPage === PAGES.PLP || fromPage === PAGES.SLP) {
    props.isFastImage = true;
    [props.imgConfig] = config.IMG_DATA_CLIENT.imgConfig;
  }
  return props;
};

const renderPromoCopy = (productInfo) => {
  const { category } = productInfo;
  const htmlToRender =
    '<meta name="viewport" content="width=device-width, initial-scale=1" /><style>@font-face { font-family: "Nunito"; font-weight: 800; font-style: normal; src: local("Nunito ExtraBold"), local("Nunito-ExtraBold"), url("https://assets.theplace.com/rwd/fonts/Nunito-800.woff2") format("woff2"); } .gym-pdp-promo {  background-color: #463CA0;   color: #FFFFFF;   font-family: Nunito;   font-weight: 800;  padding-top: 2%;  padding-bottom: 2%;  text-align: center; font-size: 14; }</style><div class="gym-pdp-promo">PREORDER - ESTIMATED TO SHIP 2/11-2/18</div>';
  const validCategories = ['728006', '728007', '728008', '728009'];
  if (category && validCategories.indexOf(category) > -1) {
    return (
      <WebView
        androidHardwareAccelerationDisabled={true}
        source={{ html: htmlToRender }}
        // eslint-disable-next-line react-native/no-inline-styles
        style={{ height: 50 }}
        scalesPageToFit={false}
      />
    );
  }
  return <></>;
};

const renderAfterPayMessaging = (isGiftCard, floatingOfferPrice) => {
  return (
    !isGiftCard && (
      <AfterPayWrapper>
        <AfterPayMessaging offerPrice={floatingOfferPrice.toFixed(2)} />
      </AfterPayWrapper>
    )
  );
};

const getKillSwitchIsDynamicBadgeEnabled = (TCPStyleType, TCPStyleQTY, isDynamicBadgeEnabled) => {
  return getDynamicBadgeQty(TCPStyleType, TCPStyleQTY, isDynamicBadgeEnabled);
};
const getColorName = (productColorEntry) => {
  return productColorEntry && productColorEntry.color && productColorEntry.color.name;
};

const getInitialSizeVal = (formValues, size) =>
  (formValues && formValues[0] && formValues[0].size) || size;

const getInitialFitVal = (formValues, fit) =>
  (formValues && formValues[0] && formValues[0].fit) || fit;

const getInitialQty = (formValues, qty) =>
  (formValues && formValues[0] && formValues[0].quantity) || qty;

const getVideoTransformation = () => {
  return isAndroid() ? VID_DATA_PLP_WEBP_APP : VID_DATA_PLP_GIF_APP;
};
const getPrimaryBrand = (itemBrand, productInfo) => {
  return itemBrand || (productInfo && productInfo.primaryBrand);
};
const renderCheckbox = (isMultiItemQVModal, formEnabled, onInputSelectionChange) => {
  return (
    isMultiItemQVModal && (
      <InputCheckboxWrapper>
        <InputCheckbox
          execOnChangeByDefault={false}
          isChecked
          input={{ value: formEnabled, onChange: onInputSelectionChange }}
        />
      </InputCheckboxWrapper>
    )
  );
};

const ProductCustomizeFormPart = (props) => {
  const {
    productInfo,
    plpLabels,
    currency,
    quickViewLabels,
    handleAddToBag,
    addToBagError,
    currentColorEntry,
    imageUrl,
    isMultiItemQVModal,
    goToPDPPageMobile,
    onChangeColor,
    handleUpdateItem,
    formRef,
    formEnabled,
    onInputSelectionChange,
    toastMessage,
    onCloseClick,
    isFromBagProductSfl,
    multiPackThreshold,
    multiPackCount,
    multipackProduct,
    partIdInfo,
    getAllNewMultiPack,
    onQuickViewOpenClick,
    pdpLabels,
    fromPage,
    initialMultipackMapping,
    singlePageLoad,
    setInitialTCPStyleQty,
    getQickViewSingleLoad,
    disableMultiPackTab,
    getDisableSelectedTab,
    isQuickView,
    formValues,
    isDynamicBadgeEnabled,
    availableTCPmapNewStyleId,
    isModalOpen,
    isNewReDesignEnabled,
    retrievedProductInfo,
    alternateBrand,
    ...otherProps
  } = props;
  const { fromBagPage, productInfoFromBag, isFavoriteEdit, isGiftCard } = otherProps;
  const [fit, setFit] = useState(getInitialFitValue(isGiftCard));
  const [size, setSize] = useState('');
  const prices = productInfo && getPrices(productInfo, currentColorEntry.color.name, fit, size);
  const { name, TCPStyleType, TCPStyleQTY } = productInfo;
  const dynamicBadgeOverlayQty = getKillSwitchIsDynamicBadgeEnabled(
    TCPStyleType,
    TCPStyleQTY,
    isDynamicBadgeEnabled
  );
  const { badge2, listPrice, offerPrice } = prices;
  const currencyPrefix = getCurrencySymbol(currency);
  const currentColorPdpUrl =
    currentColorEntry && currentColorEntry.pdpUrl ? currentColorEntry.pdpUrl : productInfo.pdpUrl;
  const modifiedPdpUrl = getProductListToPathInMobileApp(currentColorPdpUrl) || '';
  const colorProductId = currentColorEntry && currentColorEntry.colorProductId;
  const itemBrand = getItemBrand(productInfoFromBag);
  const primaryBrand = getPrimaryBrand(itemBrand, productInfo);
  const priceColor = getPriceColor();
  const giftCardPrice = getGiftCardPrice(fromBagPage, isGiftCard, productInfoFromBag);
  const floatingListPrice = parseFloat(listPrice);
  const floatingOfferPrice = getGiftCardOrOfferPrice(giftCardPrice, offerPrice);
  const isVideoUrl = getVideoUrl(imageUrl);
  const videoTransformation = getVideoTransformation();

  const onChangeSizeHandle = (color, newSize) => {
    if (typeof newSize !== 'object' && !isGiftCard) {
      return setSize(newSize);
    }
    if (isGiftCard) {
      return setSize(newSize);
    }
    return null;
  };
  const initialFormValues = {
    Fit: getInitialFitVal(formValues, fit),
    Size: getInitialSizeVal(formValues, size),
    color: getColorName(currentColorEntry),
    Quantity: getInitialQty(formValues, 1),
  };

  const renderProductAddToBag = () => (
    <ProductAddToBagContainer
      currencySymbol={currencyPrefix}
      badge2={badge2}
      retrievedProductInfo={retrievedProductInfo}
      hideQuatity={isNewReDesignEnabled}
      isNewReDesignSizeFit={isNewReDesignEnabled}
      showAddToBagCTA={!isMultiItemQVModal}
      showColorChips={!isMultiItemQVModal && !isNewReDesignEnabled}
      onChangeColor={onChangeColor}
      plpLabels={plpLabels}
      currentProduct={productInfo}
      handleFormSubmit={handleFormSubmit(
        fromBagPage,
        handleUpdateItem,
        handleAddToBag,
        isFavoriteEdit
      )}
      errorOnHandleSubmit={addToBagError}
      fromBagPage={fromBagPage}
      productInfoFromBag={productInfoFromBag}
      formRef={formRef}
      formEnabled={formEnabled}
      hideMultipackPills={isNewReDesignEnabled}
      toastMessage={toastMessage}
      onCloseClick={onCloseClick}
      isMultiItemQVModal={isMultiItemQVModal}
      isFromBagProductSfl={isFromBagProductSfl}
      isFavoriteEdit={isFavoriteEdit}
      itemBrand={itemBrand}
      hideAlternateSizes
      onFitChange={setFit}
      onChangeSize={onChangeSizeHandle}
      multiPackThreshold={multiPackThreshold}
      multiPackCount={multiPackCount}
      multipackProduct={multipackProduct}
      partIdInfo={partIdInfo}
      getAllNewMultiPack={getAllNewMultiPack}
      onQuickViewOpenClick={onQuickViewOpenClick}
      pdpLabels={pdpLabels}
      initialMultipackMapping={initialMultipackMapping}
      singlePageLoad={singlePageLoad}
      setInitialTCPStyleQty={setInitialTCPStyleQty}
      getQickViewSingleLoad={getQickViewSingleLoad}
      disableMultiPackTab={disableMultiPackTab}
      getDisableSelectedTab={getDisableSelectedTab}
      isQuickView={isQuickView}
      formValues={initialFormValues}
      availableTCPmapNewStyleId={availableTCPmapNewStyleId}
      initialFormValues={initialFormValues}
      isDynamicBadgeEnabled={isDynamicBadgeEnabled}
      {...otherProps}
    />
  );
  if (isNewReDesignEnabled) return renderProductAddToBag();
  return (
    <PickUpSkUSectionContainer
      formEnabled={formEnabled}
      isMultiItemQVModal={isMultiItemQVModal}
      borderBottomColor={colors.BORDER.NORMAL}
      borderBottomWidth={isMultiItemQVModal ? 1 : 0}
      primaryBrand={primaryBrand}
      alternateBrand={alternateBrand}
    >
      {renderCheckbox(isMultiItemQVModal, formEnabled, onInputSelectionChange)}
      <MultiItemQVWrapper
        pointerEvents={getPointerEvents(formEnabled)}
        formEnabled={formEnabled}
        isMultiItemQVModal={isMultiItemQVModal}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
      >
        <ProductSummaryContainer isMultiItemQVModal={isMultiItemQVModal}>
          <ImageWrapper>
            <DamImage
              resizeMode="contain"
              url={imageUrl}
              isProductImage
              height={198}
              width={161}
              itemBrand={itemBrand}
              dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
              tcpStyleType={TCPStyleType}
              badgeConfig={config.IMG_DATA_CLIENT.badgeConfig}
              imgConfig="t_plp_img_m"
              isOptimizedVideo={!!isVideoUrl}
              videoTransformation={videoTransformation}
              primaryBrand={primaryBrand || alternateBrand}
              {...isFromPagePLPSLP(fromPage)}
            />
            {itemBrand !== SNJ &&
              renderProductDetailLink({
                modifiedPdpUrl,
                colorProductId,
                name,
                quickViewLabels,
                goToPDPPageMobile,
                itemBrand,
              })}
          </ImageWrapper>
          <ProductDetailSummary>
            <BodyCopyWithSpacing
              fontFamily="secondary"
              fontSize="fs18"
              fontWeight="extrabold"
              text={productInfo.name}
              spacingStyles="padding-bottom-SM"
            />
            {renderProductRatings(productInfo)}
            <OfferPriceAndBadge3Container>
              <BodyCopy
                dataLocator="pdp_current_product_price"
                fontFamily="secondary"
                fontSize="fs22"
                fontWeight="black"
                color={priceColor}
                text={`${currencyPrefix}${floatingOfferPrice.toFixed(2)}`}
              />
              <ListPriceAndBadgeContainer>
                {listPrice !== offerPrice ? (
                  <BodyCopy
                    dataLocator="pdp_discounted_product_price"
                    textDecoration="line-through"
                    fontFamily="secondary"
                    fontSize="fs14"
                    fontWeight="regular"
                    color="gray.800"
                    text={`${currencyPrefix}${floatingListPrice.toFixed(2)}`}
                    accessibilityText={getAccessibilityText(
                      quickViewLabels,
                      currencyPrefix,
                      floatingListPrice
                    )}
                  />
                ) : null}
                {badge2 ? (
                  <BodyCopy
                    dataLocator="pdp_discounted_percentage"
                    margin="0 0 0 4px"
                    fontFamily="secondary"
                    fontSize="fs14"
                    fontWeight="regular"
                    color="red.500"
                    text={badge2}
                  />
                ) : null}
              </ListPriceAndBadgeContainer>
            </OfferPriceAndBadge3Container>
            {renderAfterPayMessaging(isGiftCard, floatingOfferPrice)}
          </ProductDetailSummary>
        </ProductSummaryContainer>
        {renderPromoCopy(productInfo)}
        {renderProductAddToBag()}
      </MultiItemQVWrapper>
    </PickUpSkUSectionContainer>
  );
};

ProductCustomizeFormPart.propTypes = {
  plpLabels: PropTypes.shape({}).isRequired,
  imageUrl: PropTypes.string.isRequired,
  goToPDPPageMobile: PropTypes.func.isRequired,
  onChangeColor: PropTypes.func.isRequired,
  currentColorEntry: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}),
  handleAddToBag: PropTypes.func.isRequired,
  handleUpdateItem: PropTypes.func.isRequired,
  formValues: PropTypes.shape({}).isRequired,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  isMultiItemQVModal: PropTypes.bool.isRequired,
  productInfo: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,
  currency: PropTypes.string,
  addToBagError: PropTypes.string,
  formRef: PropTypes.shape({}).isRequired,
  formEnabled: PropTypes.bool.isRequired,
  onInputSelectionChange: PropTypes.func.isRequired,
  toastMessage: PropTypes.func,
  onCloseClick: PropTypes.func,
  isFavoriteEdit: PropTypes.bool,
  isFromBagProductSfl: PropTypes.bool,
  multipackProduct: PropTypes.string.isRequired,
  multiPackCount: PropTypes.string.isRequired,
  multiPackThreshold: PropTypes.string.isRequired,
  partIdInfo: PropTypes.string.isRequired,
  getAllNewMultiPack: PropTypes.shape([]).isRequired,
  onQuickViewOpenClick: PropTypes.shape([]).isRequired,
  pdpLabels: PropTypes.shape([]).isRequired,
  fromPage: PropTypes.string,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  singlePageLoad: PropTypes.bool.isRequired,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  getQickViewSingleLoad: PropTypes.func.isRequired,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  isQuickView: PropTypes.bool.isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  availableTCPmapNewStyleId: PropTypes.shape([]),
  isModalOpen: PropTypes.bool,
  isNewReDesignEnabled: PropTypes.bool.isRequired,
};

ProductCustomizeFormPart.defaultProps = {
  currency: 'USD',
  addToBagError: '',
  navigation: {},
  toastMessage: () => {},
  onCloseClick: () => {},
  isFavoriteEdit: false,
  isFromBagProductSfl: false,
  fromPage: '',
  isDynamicBadgeEnabled: {},
  availableTCPmapNewStyleId: [],
  isModalOpen: false,
};

renderProductDetailLink.propTypes = {
  colorProductId: PropTypes.string.isRequired,
  modifiedPdpUrl: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  quickViewLabels: PropTypes.shape({}).isRequired,
  goToPDPPageMobile: PropTypes.func.isRequired,
  itemBrand: PropTypes.string.isRequired,
};

export default ProductCustomizeFormPart;
export { ProductCustomizeFormPart as ProductCustomizeFormPartVanilla };
