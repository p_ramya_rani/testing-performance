// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { QuickViewSkeletonVanilla } from '../views/QuickViewSkeleton.view';

describe('QuickViewSkeletonVanilla', () => {
  const props = {};

  it('QuickViewSkeleton should render correctly', () => {
    const component = shallow(<QuickViewSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render BagPageProductQVSkeleton correctly', () => {
    const newQVProps = {
      ...props,
      isNewQVEnabled: true,
      productInfoFromBag: {
        name: 'test',
        imagePath: 'testpath',
        tcpStyleType: 'a',
        tcpStyleQty: '1',
        isGiftCard: false,
        listPrice: '23',
        offerPrice: '24',
      },
    };
    const component = shallow(<QuickViewSkeletonVanilla {...newQVProps} />);
    expect(component).toMatchSnapshot();
  });
});
