/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import ProductRating from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductRating/ProductRating';
import ProductImagesWrapper from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductImagesWrapper/views/ProductImagesWrapper.view';
import { BodyCopy, Anchor, DamImage } from '../../../../../atoms';
import { COLOR_FITS_SIZES_MAP_PROP_TYPE } from '../../../../PickupStoreModal/PickUpStoreModal.proptypes';
import withStyles from '../../../../../hoc/withStyles';
import styles, {
  customPriceStyles,
  customSubmitButtonStyle,
} from '../styles/ProductCustomizeFormPart.style';
import ProductPrice from '../../../../../../features/browse/ProductDetail/molecules/ProductPrice/ProductPrice';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import ProductAddToBagContainer from '../../../../../molecules/ProductAddToBag/container/ProductAddToBag.container';
import SIZE_CHART_LINK_POSITIONS from '../../../../../molecules/ProductAddToBag/container/ProductAddToBag.config';
import {
  getSiteId,
  getLocator,
  getAPIConfig,
  getBrand,
  getDynamicBadgeQty,
  getImageData,
} from '../../../../../../../utils';
import InputCheckbox from '../../../../../atoms/InputCheckbox';
import {
  getPrices,
  getProductListToPath,
  getImagesToDisplay,
  getMapSliceForColor,
} from '../../../../../../features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';

const handleFormSubmit = (fromBagPage, handleUpdateItem, handleAddToBag, isFavoriteEdit) => {
  const updateAddedItem = fromBagPage || isFavoriteEdit;
  return updateAddedItem ? handleUpdateItem : handleAddToBag;
};

const getColorName = (productColorEntry) => {
  return productColorEntry && productColorEntry.color && productColorEntry.color.name;
};

const getCurrentColorPdpUrl = (productInfo, currentColorEntry) => {
  const currentColorPdpUrl =
    currentColorEntry && currentColorEntry.pdpUrl ? currentColorEntry.pdpUrl : productInfo.pdpUrl;
  return productInfo && productInfo.seoToken ? `/p/${productInfo.seoToken}` : currentColorPdpUrl;
};

const getBagPageGiftCardUrl = ({ isProductBrandOfSameDomain, crossDomain, pdpUrlFromBag }) => {
  return isProductBrandOfSameDomain ? pdpUrlFromBag : `${crossDomain}${pdpUrlFromBag}`;
};

const getBagPageGiftCardToPath = ({ isProductBrandOfSameDomain, crossDomain, pdpUrlFromBag }) => {
  return isProductBrandOfSameDomain
    ? getProductListToPath(pdpUrlFromBag)
    : `${crossDomain}${pdpUrlFromBag}`;
};

const renderProductRatings = (productInfo, isNewQVEnabled) => {
  const ratings = (productInfo && productInfo.ratings) || 0;
  const reviewsCount = (productInfo && productInfo.reviewsCount) || 0;
  return (
    !!ratings && (
      <ProductRating
        ratings={ratings || 0}
        reviews={reviewsCount}
        isNewQVEnabled={isNewQVEnabled}
      />
    )
  );
};

const renderPriceAndRatingSection = (
  isNewQVEnabled,
  productInfo,
  productPriceProps,
  isGiftCard,
  productInfoFromBag,
  fromBagPage,
  isAfterPayDisplay
) => {
  if (!isNewQVEnabled) {
    return (
      <>
        {renderProductRatings(productInfo)}
        <ProductPrice
          {...productPriceProps}
          isGiftCard={isGiftCard}
          productInfoFromBag={productInfoFromBag}
          fromBagPage={fromBagPage}
          isAfterPayDisplay={isAfterPayDisplay}
        />
      </>
    );
  }
  return (
    <ProductPrice
      {...productPriceProps}
      isGiftCard={isGiftCard}
      productInfoFromBag={productInfoFromBag}
      fromBagPage={fromBagPage}
      isAfterPayDisplay={isAfterPayDisplay}
      isNewQVEnabled={isNewQVEnabled}
      renderProductRatings={renderProductRatings}
      productInfo={productInfo}
    />
  );
};

const getToPath = ({
  pdpToPath,
  isGiftCard,
  toPath,
  fromBagPage,
  isProductBrandOfSameDomain,
  currentColorPdpUrl,
}) => {
  if (isProductBrandOfSameDomain) {
    if (fromBagPage && isGiftCard) {
      return toPath;
    }
    return currentColorPdpUrl;
  }
  return pdpToPath;
};

const getKeepAlive = (currentColorEntry) =>
  currentColorEntry && currentColorEntry.miscInfo && currentColorEntry.miscInfo.keepAlive;

const getDynamicBadgeOverlayQty = (isDynamicBadgeEnabled, TCPStyleType, TCPStyleQTY) => {
  return getDynamicBadgeQty(TCPStyleType, TCPStyleQTY, isDynamicBadgeEnabled);
};

const getClassNameQVDrawer = (classNewQV, isNewQVEnabled) => {
  return isNewQVEnabled ? classNewQV : '';
};

const bagPageGiftCardCheck = (fromBagPage, isGiftCard) => {
  return fromBagPage && isGiftCard;
};

const getItemBrand = (productInfoFromBag) => {
  return !isEmpty(productInfoFromBag) ? productInfoFromBag.itemBrand : null;
};

const getIsProductOfSameDomain = (productInfoFromBag, currentSiteBrand) => {
  return !isEmpty(productInfoFromBag)
    ? currentSiteBrand.toUpperCase() === productInfoFromBag.itemBrand.toUpperCase()
    : true;
};
const getPrimaryBrand = (itemBrand, productInfo) => {
  return itemBrand || (productInfo && productInfo.primaryBrand);
};
const ProductCustomizeFormPart = (props) => {
  const {
    className,
    productInfo,
    isMultiItemQVModal,
    plpLabels,
    currency,
    priceCurrency,
    currencyAttributes,
    isCanada,
    isHasPlcc,
    isInternationalShipping,
    quickViewLabels,
    handleAddToBag,
    addToBagError,
    currentColorEntry,
    onChangeColor,
    goToPDPPage,
    imageUrl,
    formRef,
    formEnabled,
    onInputSelectionChange,
    handleUpdateItem,
    colorFitsSizesMap,
    fromBagPage,
    productInfoFromBag,
    quickViewColorSwatchesCss,
    onCloseClick,
    isGiftCard,
    isFavoriteEdit,
    quickViewRichText,
    pdpUrlFromBag,
    multiPackThreshold,
    multiPackCount,
    multipackProduct,
    partIdInfo,
    getAllNewMultiPack,
    initialMultipackMapping,
    singlePageLoad,
    setInitialTCPStyleQty,
    setInitialQuickViewLoad,
    getQickViewSingleLoad,
    disableMultiPackTab,
    getDisableSelectedTab,
    isQuickView,
    formValues,
    isDynamicBadgeEnabled,
    isAfterPayDisplay,
    isNewQVEnabled,
    pdpLabels,
    outOfStockLabels,
    accessibilityLabels,
    currentProduct,
    completeLookSlotTile,
    itemPartNumber,
    checkForOOSForAllVariantsFlag,
    isCompleteTheLookTestEnabled,
    largeImageNameOnHover,
    hasABTestForPDPColorOrImageSwatchHover,
    deleteFavItemInProgressFlag,
    defaultWishListFromState,
    isPDPSmoothScrollEnabled,
    onPickupClickAddon,
    alternateBrand,
    ...otherProps
  } = props;
  const [fit, setFit] = useState('');
  const [size, setSize] = useState('');
  const [Qty, setQty] = useState(1);
  const prices = productInfo && getPrices(productInfo, getColorName(currentColorEntry), fit, size);
  const currentColorPdpUrl = getCurrentColorPdpUrl(productInfo, currentColorEntry);
  const productPriceProps = {
    currencySymbol: currency,
    currencyAttributes,
    priceCurrency,
    isItemPartNumberVisible: false,
    ...prices,
    isCanada,
    inheritedStyles: customPriceStyles,
    customFonts: { listPriceFont: 'fs14', offerPriceFont: 'fs22' },
    isPlcc: isHasPlcc,
    isInternationalShipping,
  };
  const imgData = {
    alt: productInfo.name,
    url: imageUrl,
  };

  const sizeChartLinkVisibility = !isGiftCard ? SIZE_CHART_LINK_POSITIONS.AFTER_SIZE : null;

  const apiConfigObj = getAPIConfig();
  const { crossDomain } = apiConfigObj;
  const currentSiteBrand = getBrand();
  const isProductBrandOfSameDomain = getIsProductOfSameDomain(productInfoFromBag, currentSiteBrand);
  const itemBrand = getItemBrand(productInfoFromBag);
  let toPath = isProductBrandOfSameDomain
    ? `/${getSiteId()}${currentColorPdpUrl}`
    : `${crossDomain}/${getSiteId()}${currentColorPdpUrl}`;
  let pdpToPath = isProductBrandOfSameDomain
    ? getProductListToPath(currentColorPdpUrl)
    : `${crossDomain}/${getSiteId()}${currentColorPdpUrl}`;
  if (bagPageGiftCardCheck(fromBagPage, isGiftCard)) {
    const tempUrl = getBagPageGiftCardUrl({
      isProductBrandOfSameDomain,
      crossDomain,
      pdpUrlFromBag,
    });
    const pdpPath = tempUrl.split('/');
    pdpPath.shift();
    toPath = `/${pdpPath.join('/')}`;
    pdpToPath = getBagPageGiftCardToPath({
      isProductBrandOfSameDomain,
      crossDomain,
      pdpUrlFromBag,
    });
  }
  const newQuickViewClass = 'quick-view-drawer-redesign';
  const configData = getImageData(imgData.url);

  const keepAlive = getKeepAlive(currentColorEntry);

  const { TCPStyleType, TCPStyleQTY, imagesByColor, ratingsProductId, productFamily } = productInfo;

  const dynamicBadgeOverlayQty = getDynamicBadgeOverlayQty(
    isDynamicBadgeEnabled,
    TCPStyleType,
    TCPStyleQTY
  );
  const getFormName = formValues?.[0]?.color;
  let imagesToDisplay = [];
  const updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, getFormName);
  if (imagesByColor) {
    imagesToDisplay = getImagesToDisplay({
      imagesByColor,
      curentColorEntry: !updatedColorEntry ? currentColorEntry : updatedColorEntry,
      isAbTestActive: false,
      isFullSet: true,
    });
  }
  const colorNameAvailable = formValues?.color;
  const pdpDataInformation = {
    TCPStyleQTY,
    TCPStyleType,
    color: { name: colorNameAvailable },
    productFamily,
  };
  const primaryBrand = getPrimaryBrand(itemBrand, productInfo);
  return (
    <div className={className}>
      {isMultiItemQVModal && (
        <div className="inputCheckBox">
          <InputCheckbox
            execOnChangeByDefault={false}
            input={{ value: formEnabled, onChange: onInputSelectionChange }}
          />
        </div>
      )}
      <div className="multi-items-QV-product">
        <div className="product-customize-form-container">
          {isNewQVEnabled && (
            <ProductImagesWrapper
              productName={productInfo.name}
              isGiftCard={isGiftCard}
              isThumbnailListVisible
              images={imagesToDisplay}
              pdpLabels={pdpLabels}
              isZoomEnabled={false}
              currentProduct={currentProduct}
              onChangeColor={onChangeColor}
              currentColorEntry={currentColorEntry}
              initialValues={formValues}
              keepAlive={keepAlive}
              outOfStockLabels={outOfStockLabels}
              accessibilityLabels={accessibilityLabels}
              ratingsProductId={ratingsProductId}
              completeLookSlotTile={completeLookSlotTile}
              itemPartNumber={itemPartNumber}
              checkForOOSForVariant={checkForOOSForAllVariantsFlag}
              isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
              imagesByColor={productInfo.imagesByColor}
              largeImageNameOnHover={largeImageNameOnHover}
              hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
              pdpDataInformation={pdpDataInformation}
              deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
              TCPStyleQTY={TCPStyleQTY}
              TCPStyleType={TCPStyleType}
              isDynamicBadgeEnabled={isDynamicBadgeEnabled}
              fromPLPPage
              isNewQVEnabled={isNewQVEnabled}
              wishList={defaultWishListFromState}
              isPDPSmoothScrollEnabled={false}
              primaryBrand={primaryBrand || alternateBrand}
            />
          )}
          <div
            className={`image-title-wrapper ${getClassNameQVDrawer(
              newQuickViewClass,
              isNewQVEnabled
            )}`}
          >
            <div className={`image-wrapper ${getClassNameQVDrawer('hide-item', isNewQVEnabled)}`}>
              <DamImage
                data-locator={getLocator('quick_view_product_image')}
                imgData={imgData}
                imgConfigs={configData.imgConfig}
                lazyLoad={false}
                isProductImage
                itemBrand={itemBrand}
                isOptimizedVideo
                dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
                tcpStyleType={TCPStyleType}
                badgeConfig={configData.badgeConfig}
                primaryBrand={primaryBrand || alternateBrand}
              />
              <Anchor
                dataLocator={getLocator('quick_view_View_Product_details')}
                className="link-redirect"
                noLink
                to={toPath}
                onClick={(e) =>
                  goToPDPPage(
                    e,
                    pdpToPath,
                    getToPath({
                      pdpToPath,
                      isGiftCard,
                      toPath,
                      fromBagPage,
                      currentColorPdpUrl,
                      isProductBrandOfSameDomain,
                    })
                  )
                }
              >
                <BodyCopy className="product-link" fontSize="fs14" fontFamily="secondary">
                  {quickViewLabels.viewProductDetails}
                </BodyCopy>
              </Anchor>
              <div className="hide-on-mobile">
                <Espot richTextHtml={quickViewRichText} dataLocator="quickViewPromo" />
              </div>
            </div>
            <div className="product-details-card-container-separate">
              <BodyCopy
                fontSize="fs18"
                fontWeight="extrabold"
                fontFamily="secondary"
                className={`product-name ${getClassNameQVDrawer(
                  newQuickViewClass,
                  isNewQVEnabled
                )}`}
                dataLocator={getLocator('quick_view_product_name')}
              >
                {productInfo.name}
              </BodyCopy>
              {renderPriceAndRatingSection(
                isNewQVEnabled,
                productInfo,
                productPriceProps,
                isGiftCard,
                productInfoFromBag,
                fromBagPage,
                isAfterPayDisplay
              )}
            </div>
          </div>
          <div className="hide-on-mobile">
            <Espot richTextHtml={quickViewRichText} dataLocator="quickViewPromo" />
          </div>
          <div className="hide-on-desktop hide-on-tablet">
            <Espot richTextHtml={quickViewRichText} dataLocator="quickViewPromo" />
          </div>
          <div className="product-detail">
            <div className="product-details-card-container">
              <BodyCopy
                fontSize="fs18"
                fontWeight="extrabold"
                fontFamily="secondary"
                className={`product-name ${getClassNameQVDrawer(
                  newQuickViewClass,
                  isNewQVEnabled
                )}`}
                dataLocator={getLocator('quick_view_product_name')}
              >
                {productInfo.name}
              </BodyCopy>
              {renderPriceAndRatingSection(
                isNewQVEnabled,
                productInfo,
                productPriceProps,
                isGiftCard,
                productInfoFromBag,
                fromBagPage,
                isAfterPayDisplay
              )}
            </div>
            <ProductAddToBagContainer
              {...otherProps}
              onChangeColor={onChangeColor}
              showAddToBagCTA={!isMultiItemQVModal}
              showColorChips={!isMultiItemQVModal}
              plpLabels={plpLabels}
              pdpLabels={pdpLabels}
              currentProduct={productInfo}
              errorOnHandleSubmit={addToBagError}
              handleFormSubmit={handleFormSubmit(
                fromBagPage,
                handleUpdateItem,
                handleAddToBag,
                isFavoriteEdit
              )}
              fromBagPage={fromBagPage}
              productInfoFromBag={productInfoFromBag}
              customSubmitButtonStyle={customSubmitButtonStyle}
              colorFitsSizesMap={colorFitsSizesMap}
              formRef={formRef}
              formEnabled={formEnabled}
              quickViewColorSwatchesCss={quickViewColorSwatchesCss}
              onCloseClick={onCloseClick}
              sizeChartLinkVisibility={sizeChartLinkVisibility}
              isFavoriteEdit={isFavoriteEdit}
              isMultiItemQVModal={isMultiItemQVModal}
              itemBrand={itemBrand}
              keepAlive={keepAlive}
              hideAlternateSizes
              onFitChange={setFit}
              onChangeSize={(color, newSize) => setSize(newSize)}
              updateQuantityChange={setQty}
              multiPackThreshold={multiPackThreshold}
              multiPackCount={multiPackCount}
              multipackProduct={multipackProduct}
              partIdInfo={partIdInfo}
              getAllNewMultiPack={getAllNewMultiPack}
              initialMultipackMapping={initialMultipackMapping}
              singlePageLoad={singlePageLoad}
              setInitialTCPStyleQty={setInitialTCPStyleQty}
              setInitialQuickViewLoad={setInitialQuickViewLoad}
              getQickViewSingleLoad={getQickViewSingleLoad}
              disableMultiPackTab={disableMultiPackTab}
              getDisableSelectedTab={getDisableSelectedTab}
              isQuickView={isQuickView}
              formValues={formValues}
              isDynamicBadgeEnabled={isDynamicBadgeEnabled}
              initialFormValues={{
                Fit: fit,
                Size: size,
                color: getColorName(currentColorEntry),
                Quantity: Qty,
              }}
              isAfterPayDisplay={isAfterPayDisplay}
              pdpUrlFromBag={pdpUrlFromBag}
              quickViewLabels={quickViewLabels}
              onPickupClickAddon={onPickupClickAddon}
              isNewQVEnabled={isNewQVEnabled}
              primaryBrand={primaryBrand || alternateBrand}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

ProductCustomizeFormPart.propTypes = {
  plpLabels: PropTypes.shape({}).isRequired,
  pdpLabels: PropTypes.shape({}).isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  accessibilityLabels: PropTypes.shape({}).isRequired,
  onChangeColor: PropTypes.func.isRequired,
  handleAddToBag: PropTypes.func.isRequired,
  handleUpdateItem: PropTypes.func.isRequired,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  productInfo: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,
  currency: PropTypes.string,
  className: PropTypes.string,
  priceCurrency: PropTypes.string,
  currencyAttributes: PropTypes.shape({}).isRequired,
  isCanada: PropTypes.bool,
  isInternationalShipping: PropTypes.bool,
  isMultiItemQVModal: PropTypes.bool.isRequired,
  isHasPlcc: PropTypes.bool,
  addToBagError: PropTypes.string,
  imageUrl: PropTypes.string,
  currentColorEntry: PropTypes.shape({}),
  goToPDPPage: PropTypes.func,
  colorFitsSizesMap: COLOR_FITS_SIZES_MAP_PROP_TYPE.isRequired,
  formRef: PropTypes.shape({}).isRequired,
  formEnabled: PropTypes.bool.isRequired,
  onInputSelectionChange: PropTypes.func.isRequired,
  fromBagPage: PropTypes.bool.isRequired,
  quickViewColorSwatchesCss: PropTypes.string,
  productInfoFromBag: PropTypes.shape({}).isRequired,
  onCloseClick: PropTypes.func,
  isGiftCard: PropTypes.bool,
  isFavoriteEdit: PropTypes.bool,
  quickViewRichText: PropTypes.string,
  pdpUrlFromBag: PropTypes.string,
  multipackProduct: PropTypes.string.isRequired,
  multiPackCount: PropTypes.string.isRequired,
  multiPackThreshold: PropTypes.string.isRequired,
  partIdInfo: PropTypes.string.isRequired,
  getAllNewMultiPack: PropTypes.shape([]).isRequired,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  singlePageLoad: PropTypes.bool.isRequired,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  setInitialQuickViewLoad: PropTypes.bool.isRequired,
  getQickViewSingleLoad: PropTypes.func.isRequired,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  isQuickView: PropTypes.bool.isRequired,
  formValues: PropTypes.shape({}).isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isAfterPayDisplay: PropTypes.bool,
  isNewQVEnabled: PropTypes.bool,
  currentProduct: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  itemPartNumber: PropTypes.string,
  isPDPSmoothScrollEnabled: PropTypes.bool,
  defaultWishListFromState: PropTypes.shape({}),
  deleteFavItemInProgressFlag: PropTypes.bool,
  hasABTestForPDPColorOrImageSwatchHover: PropTypes.bool,
  largeImageNameOnHover: PropTypes.string,
  isCompleteTheLookTestEnabled: PropTypes.bool,
  completeLookSlotTile: PropTypes.shape({}),
  checkForOOSForAllVariantsFlag: PropTypes.bool.isRequired,
  onPickupClickAddon: PropTypes.func,
};

ProductCustomizeFormPart.defaultProps = {
  currency: 'USD',
  className: '',
  priceCurrency: '',
  isCanada: false,
  isHasPlcc: false,
  isInternationalShipping: false,
  currentColorEntry: {},
  goToPDPPage: () => {},
  addToBagError: '',
  imageUrl: '',
  quickViewColorSwatchesCss: '',
  onCloseClick: () => {},
  isGiftCard: false,
  isFavoriteEdit: false,
  quickViewRichText: '',
  pdpUrlFromBag: '',
  isDynamicBadgeEnabled: false,
  isAfterPayDisplay: false,
  isNewQVEnabled: false,
  itemPartNumber: '',
  isPDPSmoothScrollEnabled: false,
  defaultWishListFromState: {},
  deleteFavItemInProgressFlag: false,
  hasABTestForPDPColorOrImageSwatchHover: false,
  largeImageNameOnHover: '',
  isCompleteTheLookTestEnabled: false,
  completeLookSlotTile: {},
  onPickupClickAddon: () => {},
};

export default withStyles(ProductCustomizeFormPart, styles);
export { ProductCustomizeFormPart as ProductCustomizeFormPartVanilla };
