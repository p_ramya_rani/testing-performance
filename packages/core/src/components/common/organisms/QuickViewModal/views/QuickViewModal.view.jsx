/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  getMapSliceForColorProductId,
  setSelectedPack,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import AddedToBagContainer from '@tcp/core/src/components/features/CnC/AddedToBag';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import ProductPickupContainer from '../../ProductPickup';
import { BodyCopy } from '../../../atoms';
import withStyles from '../../../hoc/withStyles';
import styles, {
  customHeaderStyle,
  quickViewColorSwatchesCss,
} from '../styles/QuickViewModal.style';
import { getLocator, isMobileApp, isIOS } from '../../../../../utils';
import { modalStyles } from '../styles/QuickViewModalModalAnimation.style';
import Modal from '../../../molecules/Modal';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../features/browse/ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import ProductCustomizeFormPart from '../molecules/ProductCustomizeFormPart';
import QuickViewErrorMessage from '../molecules/QuickViewErrorMessage';
import QuickViewAddToBagButton from '../atoms/QuickViewAddToBagButton';
import { getCartItemInfo } from '../../../../features/CnC/AddedToBag/util/utility';
import QuickViewSkeleton from '../molecules/QuickViewSkeleton';
import QuickViewModalSheet from '../atoms/QuickViewModalSheet/index';

class QuickViewModal extends React.Component {
  constructor(props) {
    super(props);
    this.handleMultipleItemsAddToBagClick = this.handleMultipleItemsAddToBagClick.bind(this);
    this.state = {
      showAddProductValidation: false,
    };
  }

  componentWillMount() {
    const { setSelectedMultipack, productInfo } = this.props;
    if (productInfo && productInfo !== null) {
      const { TCPMultipackProductMapping, TCPStyleQTY } = productInfo[0]?.product;
      setSelectedPack(setSelectedMultipack, TCPStyleQTY, TCPMultipackProductMapping);
    }
  }

  changeQuickViewState = (state) => {
    this.setState({ showAddProductValidation: state });
  };

  onCloseClick = (fromPickup) => {
    const {
      closeQuickViewModal,
      clearAddToBagError,
      clearMultipleItemsAddToBagError,
      clearCheckoutServerError,
      setCartItemsSflError,
      productInfo,
      setSelectedMultipack,
    } = this.props;
    if (!fromPickup) {
      closeQuickViewModal();
    }
    clearAddToBagError();
    clearMultipleItemsAddToBagError();
    clearCheckoutServerError({});
    setCartItemsSflError(null);
    const { TCPMultipackProductMapping, TCPStyleType } = productInfo?.[0]?.product;
    setSelectedPack(setSelectedMultipack, TCPStyleType, TCPMultipackProductMapping);
  };

  getHeadingText = () => {
    const {
      quickViewLabels,
      fromBagPage,
      isLoading,
      isFavoriteEdit,
      isLoadingError,
      setInitialQuickViewLoad,
      productInfo,
      isNewQVEnabled,
      productInfoFromBag,
    } = this.props;
    if (isLoading && setInitialQuickViewLoad && !(productInfoFromBag && isNewQVEnabled)) {
      return ' ';
    }
    const { addToBag, editItem, editProduct, sorryErrorMsg } = quickViewLabels;
    let headerText = !isNewQVEnabled ? addToBag : productInfo?.[0]?.product?.name;
    if (isLoadingError) {
      return sorryErrorMsg;
    }
    if (fromBagPage) {
      headerText = isNewQVEnabled
        ? productInfoFromBag?.name || productInfo?.[0]?.product?.name
        : editItem;
    } else if (isFavoriteEdit) {
      headerText = editProduct;
    }
    return headerText;
  };

  /**
   * @function renderAddToBagButton
   * @returns Add To Bag Butyon
   *
   * @memberof ProductAddToBag
   */
  renderAddToBagButton = () => {
    const {
      plpLabels: { addToBag },
      quickViewLabels,
      toastMessage,
    } = this.props;
    const { showAddProductValidation } = this.state;
    return (
      <QuickViewAddToBagButton
        dataLocator="MULTI_QV_ATB"
        onClickActn={this.handleMultipleItemsAddToBagClick}
        toastMessage={toastMessage}
        buttonLabel={addToBag}
        quickViewLabels={quickViewLabels}
        showAddProductValidation={showAddProductValidation}
        changeQuickViewState={this.changeQuickViewState}
      />
    );
  };

  renderProductCustomizeFormPart = (newDrawerToShow) => {
    const {
      productInfo,
      isMultiItemQVModal,
      quickViewLabels,
      selectedColorProductId,
      plpLabels,
      addToBagError,
      addToBagMultipleItemError,
      currencyAttributes,
      toastMessage,
      quickViewRichText,
      retrievedProductInfo,
      pdpUrlFromBag,
      multiPackThreshold,
      multiPackCount,
      multipackProduct,
      partIdInfo,
      getAllNewMultiPack,
      initialMultipackMapping,
      singlePageLoad,
      setInitialTCPStyleQty,
      setInitialQuickViewLoad,
      getQickViewSingleLoad,
      disableMultiPackTab,
      getDisableSelectedTab,
      formValues,
      isDynamicBadgeEnabled,
      isNewReDesignQuickView,
      isNewQVEnabled,
      accessibilityLabels,
      completeLookSlotTile,
      itemPartNumber,
      checkForOOSForAllVariantsFlag,
      isCompleteTheLookTestEnabled,
      largeImageNameOnHover,
      hasABTestForPDPColorOrImageSwatchHover,
      deleteFavItemInProgressFlag,
      defaultWishListFromState,
      isPDPSmoothScrollEnabled,
      primaryBrand,
      alternateBrand,
      navigation,
      ...otherProps
    } = this.props;
    this.skuFormRefs = [];
    if (productInfo) {
      return productInfo.map(({ product = {} } = {}) => {
        const {
          colorFitsSizesMap = [],
          colorFitSizeDisplayNames,
          alternateSizes,
          isGiftCard,
        } = product;
        const formRef = React.createRef();
        this.skuFormRefs.push(formRef);
        const modifiedColorFitsSizesMap = selectedColorProductId
          ? colorFitsSizesMap.filter((item) => item.colorDisplayId === selectedColorProductId)
          : colorFitsSizesMap;
        const { errorProductId, errMsg } = addToBagMultipleItemError;
        const errorMessage = !isMultiItemQVModal
          ? addToBagError
          : (product.generalProductId === errorProductId && errMsg) || null;
        const fromPickup = !isMobileApp();
        return (
          <ProductCustomizeFormPart
            productInfo={product}
            addToBagError={errorMessage}
            colorFitsSizesMap={
              modifiedColorFitsSizesMap && modifiedColorFitsSizesMap.length
                ? modifiedColorFitsSizesMap
                : colorFitsSizesMap
            }
            currencyAttributes={currencyAttributes}
            plpLabels={plpLabels}
            colorFitSizeDisplayNames={colorFitSizeDisplayNames}
            quickViewLabels={quickViewLabels}
            onCloseClick={() => this.onCloseClick(fromPickup)}
            isMultiItemQVModal={isMultiItemQVModal}
            formRef={formRef}
            quickViewColorSwatchesCss={quickViewColorSwatchesCss}
            toastMessage={toastMessage}
            changeQuickViewState={this.changeQuickViewState}
            isQuickView
            marginTopNone
            alternateSizes={alternateSizes}
            isGiftCard={isGiftCard}
            quickViewRichText={quickViewRichText}
            retrievedProductInfo={retrievedProductInfo}
            pdpUrlFromBag={pdpUrlFromBag}
            multiPackThreshold={multiPackThreshold}
            multiPackCount={multiPackCount}
            multipackProduct={multipackProduct}
            partIdInfo={partIdInfo}
            getAllNewMultiPack={getAllNewMultiPack}
            initialMultipackMapping={initialMultipackMapping}
            singlePageLoad={singlePageLoad}
            setInitialTCPStyleQty={setInitialTCPStyleQty}
            setInitialQuickViewLoad={setInitialQuickViewLoad}
            getQickViewSingleLoad={getQickViewSingleLoad}
            disableMultiPackTab={disableMultiPackTab}
            getDisableSelectedTab={getDisableSelectedTab}
            formValues={formValues}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            isNewQVEnabled={isNewQVEnabled}
            onPickupClickAddon={() => this.onCloseClick(true)}
            accessibilityLabels={accessibilityLabels}
            completeLookSlotTile={completeLookSlotTile}
            itemPartNumber={itemPartNumber}
            checkForOOSForAllVariantsFlag={checkForOOSForAllVariantsFlag}
            isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
            largeImageNameOnHover={largeImageNameOnHover}
            hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
            deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
            defaultWishListFromState={defaultWishListFromState}
            isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
            isNewReDesignEnabled={isNewReDesignQuickView && newDrawerToShow}
            navigation={navigation}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
            {...otherProps}
            isAfterPayDisplay
          />
        );
      });
    }
    return null;
  };

  renderAppFulFillMentSection = (
    isMultiItemQVModal,
    fromBagPage,
    isFavoriteEdit,
    product,
    currentColorEntry,
    { itemBrand, availableTCPmapNewStyleId, initialMultipackMapping, setInitialTCPStyleQty }
  ) => {
    return (
      !isMultiItemQVModal &&
      !fromBagPage &&
      !isFavoriteEdit &&
      product &&
      currentColorEntry && (
        <ProductPickupContainer
          productInfo={product}
          formName={`ProductAddToBag-${product.generalProductId}`}
          miscInfo={currentColorEntry.miscInfo}
          onPickupClickAddon={() => this.onCloseClick(true)}
          itemBrand={itemBrand}
          availableTCPmapNewStyleId={availableTCPmapNewStyleId}
          initialMultipackMapping={initialMultipackMapping}
          initialSelectedQty={setInitialTCPStyleQty}
        />
      )
    );
  };

  renderFulFilmentSection = (
    isMultiItemQVModal,
    fromBagPage,
    product,
    currentColorEntry,
    isFavoriteEdit,
    className,
    { itemBrand, availableTCPmapNewStyleId, initialMultipackMapping, setInitialTCPStyleQty }
  ) => {
    if (isMobileApp()) {
      return this.renderAppFulFillMentSection(
        isMultiItemQVModal,
        fromBagPage,
        isFavoriteEdit,
        product,
        currentColorEntry,
        {
          itemBrand,
          availableTCPmapNewStyleId,
          initialMultipackMapping,
          setInitialTCPStyleQty,
        }
      );
    }
    const { isNewQVEnabled, productInfoFromBag, pdpUrlFromBag, quickViewLabels, alternateBrand } =
      this.props;
    const fromPickup = !isMobileApp();
    return (
      !isMultiItemQVModal &&
      !fromBagPage &&
      !isFavoriteEdit &&
      product &&
      currentColorEntry && (
        <BodyCopy
          component="div"
          className={`${className} fulfillment-section-wrapper ${
            isNewQVEnabled ? 'quick-view-drawer-redesign' : ''
          }`}
        >
          <ProductPickupContainer
            productInfo={product}
            itemBrand={itemBrand}
            formName={`ProductAddToBag-${product.generalProductId}`}
            miscInfo={currentColorEntry.miscInfo}
            onPickupClickAddon={() => this.onCloseClick(true)}
            availableTCPmapNewStyleId={availableTCPmapNewStyleId}
            initialMultipackMapping={initialMultipackMapping}
            setInitialTCPStyleQty={setInitialTCPStyleQty}
            isNewQVEnabled={isNewQVEnabled}
            currentColorEntry={currentColorEntry}
            productInfoFromBag={productInfoFromBag}
            fromBagPage={fromBagPage}
            pdpUrlFromBag={pdpUrlFromBag}
            onCloseClick={() => this.onCloseClick(fromPickup)}
            quickViewLabels={quickViewLabels}
            alternateBrand={alternateBrand}
          />
        </BodyCopy>
      )
    );
  };

  getQuickViewModalContent = ({
    isLoading,
    retrievedProductInfo,
    currencyAttributes,
    isMultiItemQVModal,
    fromBagPage,
    product,
    currentColorEntry,
    isFavoriteEdit,
    className,
    itemBrand,
    isShowPriceRangeKillSwitch,
    showPriceRangeForABTest,
    isMultiPackResponse,
    setInitialQuickViewLoad,
    isDynamicBadgeEnabled,
    availableTCPmapNewStyleId,
    initialMultipackMapping,
    setInitialTCPStyleQty,
    isNewQVEnabled,
    primaryBrand,
    productInfoFromBag,
    alternateBrand,
  }) => {
    return isLoading && setInitialQuickViewLoad && (!isMultiPackResponse || productInfoFromBag) ? (
      <QuickViewSkeleton
        retrievedProductInfo={retrievedProductInfo}
        currencyAttributes={currencyAttributes}
        isShowPriceRangeKillSwitch={isShowPriceRangeKillSwitch}
        showPriceRangeForABTest={showPriceRangeForABTest}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        isNewQVEnabled={isNewQVEnabled}
        primaryBrand={primaryBrand}
        productInfoFromBag={productInfoFromBag}
        fromBagPage={fromBagPage}
        alternateBrand={alternateBrand}
      />
    ) : (
      <React.Fragment>
        {this.renderProductCustomizeFormPart()}

        {!isNewQVEnabled &&
          this.renderFulFilmentSection(
            isMultiItemQVModal,
            fromBagPage,
            product,
            currentColorEntry,
            isFavoriteEdit,
            className,
            { itemBrand, availableTCPmapNewStyleId, initialMultipackMapping, setInitialTCPStyleQty }
          )}
        {isMultiItemQVModal && this.renderAddToBagButton()}
      </React.Fragment>
    );
  };

  /**
   * launches ReDesigned Quick View Drawer
   *
   */
  getNewDesignFlagValueAsPerPage = () => {
    const { fromPage } = this.props;
    const comingFromPage = (fromPage && fromPage.toLowerCase()) || '';
    return (
      comingFromPage === 'plp' ||
      comingFromPage === 'pdp' ||
      comingFromPage === 'slp' ||
      comingFromPage === 'homepage' ||
      comingFromPage === 'sbppdp' ||
      comingFromPage === 'cart' ||
      comingFromPage === 'checkout'
    );
  };

  handleMultipleItemsAddToBagClick(e) {
    e.preventDefault();
    const productItemsInfo = [];
    const { addMultipleItemsToBagEcom, formValues, productInfo, closeQuickViewModal } = this.props;
    const hasNoError = this.skuFormRefs.every((formRef, index) => {
      const {
        current: {
          props: { fitChanged, formEnabled, displayErrorMessage },
        },
      } = formRef;
      const addProductToBag = formEnabled; // Add product only when the form is enabled
      this.setState({ showAddProductValidation: !addProductToBag });
      const displayError = formEnabled && fitChanged; // Validate error only when the form is enabled and fit has changed
      if (displayError) {
        displayErrorMessage(fitChanged);
        if (!isMobileApp() && index === 0) {
          document.querySelector('.TCPModal__InnerContent').scrollTo(0, 0);
        }
        return !displayError;
      }
      if (addProductToBag) {
        const { product } = productInfo[index];
        const formValue = formValues[index];
        const cartItem = getCartItemInfo(product, formValue);
        productItemsInfo.push(cartItem);
      }
      return !displayError;
    });
    if (hasNoError && productItemsInfo.length) {
      const cartItemInfo = { productItemsInfo, callBack: closeQuickViewModal };
      addMultipleItemsToBagEcom(cartItemInfo);
    }
  }

  renderSizeDrawer = (newDrawerToShow) => {
    const {
      isModalOpen,
      pdpLabels,
      quickViewLabels,
      productInfo,
      navigation,
      isNewReDesignQuickView,
      sbpNavigation,
      isSBPPLP,
    } = this.props;
    if (!isModalOpen) return null;
    const product = productInfo && productInfo[0] && productInfo[0].product;
    return (
      <QuickViewModalSheet
        isOpen={isModalOpen}
        onClosed={() => this.onCloseClick()}
        pdpLabels={pdpLabels}
        quickViewLabels={quickViewLabels}
        product={product}
        navigation={navigation}
        newDesign={isNewReDesignQuickView}
        sbpNavigation={sbpNavigation}
        isSBPPLP={isSBPPLP}
      >
        {this.renderProductCustomizeFormPart(newDrawerToShow)}
        {!!isIOS() && <AddedToBagContainer navigation={navigation} />}
      </QuickViewModalSheet>
    );
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      isModalOpen,
      productInfo,
      isMultiItemQVModal,
      fromBagPage,
      isLoading,
      isFavoriteEdit,
      className,
      retrievedProductInfo,
      productInfoFromBag,
      currencyAttributes,
      isLoadingError,
      quickViewLabels,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      isMultiPackResponse,
      setInitialQuickViewLoad,
      isDynamicBadgeEnabled,
      availableTCPmapNewStyleId,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      isNewReDesignQuickView,
      isNewQVEnabled,
      primaryBrand,
      alternateBrand,
    } = this.props;

    const modalHeadingNewQV = {
      className: 'modal-heading',
    };
    const modalHeading = {
      className: 'Modal_Heading',
    };
    const headingProps = {
      headingStyle: isNewQVEnabled ? modalHeadingNewQV : modalHeading,
    };
    const newDrawerToShow = this.getNewDesignFlagValueAsPerPage();

    const product = productInfo && productInfo[0] && productInfo[0].product;
    const currentColorEntry =
      product && getMapSliceForColorProductId(product.colorFitsSizesMap, product.generalProductId);
    const { itemBrand } = productInfoFromBag;
    if (isNewReDesignQuickView && newDrawerToShow) return this.renderSizeDrawer(newDrawerToShow);
    return (
      isModalOpen && (
        <React.Fragment>
          {isNewQVEnabled ? (
            <Modal
              fixedWidth
              isOpen
              onRequestClose={() => {
                this.onCloseClick();
              }}
              heading={this.getHeadingText()}
              headingAlign="center"
              overlayClassName="TCPModal__Overlay"
              dataLocator={getLocator('quick_view_modal')}
              dataLocatorHeader={getLocator('quick_view_add_to_bag_header')}
              closeIconDataLocator={getLocator('quick_view_icon_btn')}
              className={`TCPModal__Content, ${className}`}
              inheritedStyles={modalStyles}
              innerContentClassName="atb-innerContent"
              headingFontWeight="bold"
              headingFontFamily="secondary"
              {...headingProps}
            >
              {!isLoadingError ? (
                this.getQuickViewModalContent({
                  isLoading,
                  retrievedProductInfo,
                  currencyAttributes,
                  isMultiItemQVModal,
                  fromBagPage,
                  product,
                  currentColorEntry,
                  isFavoriteEdit,
                  className,
                  itemBrand,
                  isShowPriceRangeKillSwitch,
                  showPriceRangeForABTest,
                  isMultiPackResponse,
                  setInitialQuickViewLoad,
                  isDynamicBadgeEnabled,
                  availableTCPmapNewStyleId,
                  initialMultipackMapping,
                  setInitialTCPStyleQty,
                  isNewQVEnabled,
                  productInfoFromBag,
                  primaryBrand,
                  alternateBrand,
                })
              ) : (
                <QuickViewErrorMessage quickViewLabels={quickViewLabels} />
              )}
            </Modal>
          ) : (
            <Modal
              isOpen={isModalOpen}
              onRequestClose={() => this.onCloseClick()}
              overlayClassName="TCPModal__Overlay"
              className="TCPModal__Content"
              dataLocator={getLocator('quick_view_modal')}
              dataLocatorHeader={getLocator('quick_view_add_to_bag_header')}
              closeIconDataLocator={getLocator('quick_view_icon_btn')}
              heading={this.getHeadingText()}
              widthConfig={{ small: '375px', medium: '600px', large: '704px' }}
              standardHeight
              fixedWidth
              inheritedStyles={customHeaderStyle}
              headingAlign="center"
              horizontalBar={false}
              stickyCloseIcon
              fullWidth
              stickyHeader
              headingFontWeight="bold"
              fontSize="fs22"
              headingFontFamily="secondary"
            >
              {!isLoadingError ? (
                this.getQuickViewModalContent({
                  isLoading,
                  retrievedProductInfo,
                  currencyAttributes,
                  isMultiItemQVModal,
                  fromBagPage,
                  product,
                  currentColorEntry,
                  isFavoriteEdit,
                  className,
                  itemBrand,
                  isShowPriceRangeKillSwitch,
                  showPriceRangeForABTest,
                  isMultiPackResponse,
                  setInitialQuickViewLoad,
                  isDynamicBadgeEnabled,
                  availableTCPmapNewStyleId,
                  initialMultipackMapping,
                  setInitialTCPStyleQty,
                  productInfoFromBag,
                  primaryBrand,
                  alternateBrand,
                })
              ) : (
                <QuickViewErrorMessage quickViewLabels={quickViewLabels} />
              )}
            </Modal>
          )}
        </React.Fragment>
      )
    );
  }
}

QuickViewModal.propTypes = {
  plpLabels: PropTypes.shape({}).isRequired,
  accessibilityLabels: PropTypes.shape({}).isRequired,
  addToBagMultipleItemError: PropTypes.shape({}).isRequired,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  closeQuickViewModal: PropTypes.func.isRequired,
  addMultipleItemsToBagEcom: PropTypes.func.isRequired,
  clearAddToBagError: PropTypes.func.isRequired,
  formValues: PropTypes.shape([]).isRequired,
  clearMultipleItemsAddToBagError: PropTypes.func.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  isMultiItemQVModal: PropTypes.bool.isRequired,
  addToBagError: PropTypes.string.isRequired,
  fromBagPage: PropTypes.bool.isRequired,
  productInfo: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,
  selectedColorProductId: PropTypes.string.isRequired,
  currencyAttributes: PropTypes.shape({}).isRequired,
  isLoading: PropTypes.string.isRequired,
  toastMessage: PropTypes.func,
  isFavoriteEdit: PropTypes.bool.isRequired,
  quickViewRichText: PropTypes.string,
  clearCheckoutServerError: PropTypes.bool.isRequired,
  setCartItemsSflError: PropTypes.bool.isRequired,
  className: PropTypes.string,
  retrievedProductInfo: PropTypes.string.isRequired,
  pdpUrlFromBag: PropTypes.string,
  productInfoFromBag: PropTypes.shape({}).isRequired,
  isLoadingError: PropTypes.bool,
  isShowPriceRangeKillSwitch: PropTypes.bool.isRequired,
  showPriceRangeForABTest: PropTypes.bool.isRequired,
  multipackProduct: PropTypes.string.isRequired,
  multiPackCount: PropTypes.string.isRequired,
  multiPackThreshold: PropTypes.string.isRequired,
  partIdInfo: PropTypes.string.isRequired,
  getAllNewMultiPack: PropTypes.shape([]).isRequired,
  isMultiPackResponse: PropTypes.bool.isRequired,
  selectedMultipack: PropTypes.string.isRequired,
  setSelectedMultipack: PropTypes.string.isRequired,
  TCPStyleQTY: PropTypes.number.isRequired,
  TCPMultipackProductMapping: PropTypes.shape([]).isRequired,
  isQuickView: PropTypes.bool.isRequired,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  singlePageLoad: PropTypes.bool.isRequired,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  setInitialQuickViewLoad: PropTypes.bool.isRequired,
  getQickViewSingleLoad: PropTypes.func.isRequired,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  availableTCPmapNewStyleId: PropTypes.shape([]),
  isNewReDesignQuickView: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  isNewQVEnabled: PropTypes.bool,
  itemPartNumber: PropTypes.string,
  isPDPSmoothScrollEnabled: PropTypes.bool,
  defaultWishListFromState: PropTypes.shape({}),
  deleteFavItemInProgressFlag: PropTypes.bool,
  hasABTestForPDPColorOrImageSwatchHover: PropTypes.bool,
  largeImageNameOnHover: PropTypes.string,
  isCompleteTheLookTestEnabled: PropTypes.bool,
  completeLookSlotTile: PropTypes.shape({}),
  checkForOOSForAllVariantsFlag: PropTypes.bool.isRequired,
  fromPage: PropTypes.string,
  sbpNavigation: PropTypes.shape({}).isRequired,
  isSBPPLP: PropTypes.bool.isRequired,
};

QuickViewModal.defaultProps = {
  toastMessage: () => {},
  quickViewRichText: '',
  className: null,
  pdpUrlFromBag: '',
  isLoadingError: false,
  isDynamicBadgeEnabled: false,
  availableTCPmapNewStyleId: [],
  pdpLabels: {},
  isNewReDesignQuickView: false,
  navigation: null,
  isNewQVEnabled: false,
  itemPartNumber: '',
  isPDPSmoothScrollEnabled: false,
  defaultWishListFromState: {},
  deleteFavItemInProgressFlag: false,
  hasABTestForPDPColorOrImageSwatchHover: false,
  largeImageNameOnHover: '',
  isCompleteTheLookTestEnabled: false,
  completeLookSlotTile: {},
  fromPage: '',
};

export default errorBoundary(withStyles(QuickViewModal, styles));
export { QuickViewModal as QuickViewModalVanilla };
