/* eslint-disable no-shadow */
// 9fbef606107a605d69c0edbcd8029e5d
import PropTypes from 'prop-types';
import React from 'react';
import withStyles from '../../../hoc/withStyles';
import ProductPickupOld from './ProductPickupOld.view.native';
import ProductPickupNew from './ProductPickupNew.view.native';

class ProductPickup extends React.PureComponent {
  render() {
    const { isPDPBossBopisEnabled } = this.props;

    if (isPDPBossBopisEnabled) {
      return <ProductPickupNew {...this.props} isPDPBossBopisEnabled={isPDPBossBopisEnabled} />;
    }
    return <ProductPickupOld {...this.props} isPDPBossBopisEnabled={isPDPBossBopisEnabled} />;
  }
}

ProductPickup.propTypes = {
  isPDPBossBopisEnabled: PropTypes.bool,
};

ProductPickup.defaultProps = {
  isPDPBossBopisEnabled: false,
};

export default withStyles(ProductPickup);

export { ProductPickup as ProductPickupVanilla };
