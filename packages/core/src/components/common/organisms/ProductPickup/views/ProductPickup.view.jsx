/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
/** @module ProductPickup
 * @summary Shows the BOPIS CTA in PDP
 *
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import PickupPromotionBanner from '@tcp/core/src/components/common/molecules/PickupPromotionBanner';
import { setLocalStorage, getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { getIconPath, getLabelValue } from '@tcp/core/src/utils';
import {
  COLOR_FITS_SIZES_MAP_PROP_TYPE,
  PRICING_PROP_TYPES,
} from '../../PickupStoreModal/PickUpStoreModal.proptypes';
import { handleGenericKeyDown } from '../util';
import withStyles from '../../../hoc/withStyles';

import { Button, Anchor, BodyCopy } from '../../../atoms';
import ProductPickupStyles from '../styles/ProductPickup.style';
import DeliveryMethods from '../../../molecules/DeliveryMethods/views';

import { KEY_CODES } from '../../../../../constants/keyboard.constants';

/**
 *  Describes a general product, not yet specialized by chosing a color, size, etc.
 *  For example, a product shown in reccomendations, or PDP.
 */
export const PRODUCT_INFO_PROP_TYPES = {
  /* this identifies a product for Bazaarvoice (reviews and ratings component) */
  ratingsProductId: PropTypes.string,

  /** This identifies the product regardless of color/fit/size (i.e., changing size/fit/color does not change this value) */
  generalProductId: PropTypes.string.isRequired,
  /** The name of the product to be displayed to the user */
  name: PropTypes.string.isRequired,
  /** Images for this product in different colors.
   * This is an object of key-vale pairs. the key is the color name, and the value has thew shape described below.
   */
  imagesByColor: PropTypes.oneOfType([
    PropTypes.objectOf(
      PropTypes.shape({
        extraImages: PropTypes.arrayOf(
          PropTypes.shape({
            iconSizeImageUrl: PropTypes.string.isRequired,
            regularSizeImageUrl: PropTypes.string.isRequired,
            bigSizeImageUrl: PropTypes.string.isRequired,
            superSizeImageUrl: PropTypes.string.isRequired,
          })
        ),
      })
    ),
    PropTypes.objectOf(
      PropTypes.shape({
        basicImageUrl: PropTypes.string.isRequired,
      })
    ),
  ]),
  /** optional displayNames of the color fit and size (e.g., for gift cards it is {color: 'Design, size: 'Value']) */
  colorFitSizeDisplayNames: PropTypes.shape({
    color: PropTypes.string,
    fit: PropTypes.string,
    size: PropTypes.string,
  }),
  /**
   * The available color fit and size options for this product
   * Organized in a three level nesting (similar to a site navigation) with L1 being the color,
   * L2 being the fit, and L3 being the size
   */
  colorFitsSizesMap: COLOR_FITS_SIZES_MAP_PROP_TYPE,

  /** SEO Friendly URL required to have the image and title linkable */
  pdpUrl: PropTypes.string.isRequired,

  /* Short description of the product. */
  shortDescription: PropTypes.string,

  /* Long description of the product that may include HTML. */
  longDescription: PropTypes.string,

  /** Flags if this SKU/product is a Gift-Card */
  isGiftCard: PropTypes.bool,

  /** Product price, which may be overriden by sku-level price */
  ...PRICING_PROP_TYPES,
};

export const PRODUCT_INFO_PROP_TYPE_SHAPE = PropTypes.shape(PRODUCT_INFO_PROP_TYPES);

const getBopisItemInventoryRes = (bopisItemInventory) => {
  return (
    bopisItemInventory &&
    bopisItemInventory.inventoryResponse &&
    bopisItemInventory.inventoryResponse[0]
  );
};

const getShouldShowFulfillmentSection = (fromBagPage, isNewPDPEnabled, isNewQVEnabled) => {
  return fromBagPage || (!isNewPDPEnabled && !isNewQVEnabled);
};

class ProductPickup extends React.PureComponent {
  static propTypes = {
    miscInfo: PropTypes.shape({}),
    className: PropTypes.string,

    /**
     * Information regarding the product at the swatch/color level.
     */
    productInfo: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,
    /** User's preferred store information */
    userDefaultStore: PropTypes.shape({
      basicInfo: PropTypes.shape({
        /** store id identifier */
        id: PropTypes.string.isRequired,
        /** store Name */
        storeName: PropTypes.string.isRequired,
      }).isRequired,
    }),
    /**
     * User's latitude and longitude coordinates if access is allowed.
     */
    userGeoCoordinates: PropTypes.shape({
      lat: PropTypes.number,
      long: PropTypes.number,
    }),

    /**
     * Item values selected for the product, to use in the BOPIS modal if it
     * gets opened.
     */
    itemValues: PropTypes.shape({
      color: PropTypes.string.isRequired,
      Fit: PropTypes.string,
      Size: PropTypes.string,
      Quantity: PropTypes.number,
    }),

    /**
     * method responsible for triggering the operator method for BopisQuickViewModal
     */
    onPickUpOpenClick: PropTypes.func,
    /**
     * carries the inventory information of the bopis item selected
     */
    bopisItemInventory: PropTypes.arrayOf(PropTypes.object),
    isBopisEligible: PropTypes.bool,
    isBossEligible: PropTypes.bool,
    isSkuResolved: PropTypes.bool,
    showChangeStore: PropTypes.bool,
    pickupTitleText: PropTypes.string,
    isBossEligBossInvAvail: PropTypes.bool,
    isStoreBopisEligible: PropTypes.bool,
    showPickupInfo: PropTypes.bool,
    itemBrand: PropTypes.string,
    isSubmitting: PropTypes.bool,
    labels: PropTypes.shape({
      lbl_Product_pickup_BOPIS_AVAILABLE: PropTypes.string,
      lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: PropTypes.string,
      lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: PropTypes.string,
      lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: PropTypes.string,
      lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: PropTypes.string,
      lbl_Product_pickup_BOSS_AVAILABLE: PropTypes.string,
      lbl_Product_pickup_BOSS_ONLY_AVAILABLE: PropTypes.string,
      lbl_Product_pickup_FIND_STORE: PropTypes.string,
      lbl_Product_pickup_FREE_SHIPPING: PropTypes.string,
      lbl_Product_pickup_NO_MIN_PURCHASE: PropTypes.string,
      lbl_Product_pickup_PICKUP_IN_STORE: PropTypes.string,
      lbl_Product_pickup_PRODUCT_BOPIS: PropTypes.string,
      lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: PropTypes.string,
    }),
    isAnchor: PropTypes.bool,
    sizeUnavailable: PropTypes.string,
    onPickupClickAddon: PropTypes.func,
    isOutfitVariant: PropTypes.bool,
    isStoreAndProductBossEligible: PropTypes.bool,
    keepAlive: PropTypes.bool,
    outOfStockLabels: PropTypes.shape({
      itemSoldOutMessage: PropTypes.string,
    }),
    isProductPickup: PropTypes.bool,
    availableTCPmapNewStyleId: PropTypes.shape([]),
    initialMultipackMapping: PropTypes.shape([]),
    setInitialTCPStyleQty: PropTypes.string,
    isNewQVEnabled: PropTypes.bool,
    currentColorEntry: PropTypes.shape({}),
    productInfoFromBag: PropTypes.shape({}).isRequired,
    quickViewLabels: PropTypes.shape({
      addToBag: PropTypes.string,
      viewProductDetails: PropTypes.string,
    }).isRequired,
    pickupSelectionUpdate: PropTypes.func,
    isPickUpSelected: PropTypes.string,
    isNewPDPEnabled: PropTypes.bool,
    fromBagPage: PropTypes.bool,
    checkForOOSForVariant: PropTypes.bool.isRequired,
    isAllSizeDisabled: PropTypes.bool,
    isBossEnabled: PropTypes.bool,
    isBopisEnabled: PropTypes.bool,
  };

  static defaultProps = {
    miscInfo: {},
    userDefaultStore: {
      basicInfo: {
        id: '',
        storeName: '',
      },
    },
    userGeoCoordinates: {
      lat: null,
      long: null,
    },
    itemValues: {
      color: '',
      Fit: '',
      Size: '',
      Quantity: null,
    },
    onPickUpOpenClick: null,
    bopisItemInventory: [],
    className: '',
    isBopisEligible: false,
    isBossEligible: false,
    isSkuResolved: false,
    showChangeStore: false,
    pickupTitleText: '',
    isBossEligBossInvAvail: false,
    isStoreBopisEligible: false,
    itemBrand: '',
    showPickupInfo: false,
    isSubmitting: false,
    labels: {
      lbl_Product_pickup_BOPIS_AVAILABLE: 'Pick up TODAY!',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: 'husky',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: 'plus',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: 'slim',
      lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: 'Item available for pickup TODAY',
      lbl_Product_pickup_BOSS_AVAILABLE: 'Or choose NO RUSH Pickup ',
      lbl_Product_pickup_BOSS_ONLY_AVAILABLE: 'Choose NO RUSH Pickup ',
      lbl_Product_pickup_FIND_STORE: 'Find In Store',
      lbl_Product_pickup_FREE_SHIPPING: 'FREE Shipping Every Day!',
      lbl_Product_pickup_NO_MIN_PURCHASE: 'No Minimum Purchase Required.',
      lbl_Product_pickup_PICKUP_IN_STORE: 'PICK UP IN STORE',
      lbl_Product_pickup_PRODUCT_BOPIS: 'Buy online - Pick up in store',
      lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: 'Select Store',
    },
    isAnchor: false,
    sizeUnavailable: 'Size unavailable online?',
    onPickupClickAddon: () => {},
    isOutfitVariant: false,
    isStoreAndProductBossEligible: false,
    keepAlive: false,
    outOfStockLabels: {
      itemSoldOutMessage: '',
    },
    isProductPickup: true,
    availableTCPmapNewStyleId: [],
    initialMultipackMapping: [],
    setInitialTCPStyleQty: '',
    isNewQVEnabled: false,
    currentColorEntry: {},
    pickupSelectionUpdate: () => {},
    isPickUpSelected: '',
    isNewPDPEnabled: false,
    fromBagPage: false,
    isAllSizeDisabled: false,
    isBossEnabled: false,
    isBopisEnabled: false,
  };

  constructor(props) {
    super(props);
    this.handlePickupModalClick = this.handlePickupModalClick.bind(this);
  }

  getPageName = (productInfo) => {
    let pageName = '';
    const productId = productInfo && productInfo.generalProductId.split('_')[0];
    const productName = productInfo && productInfo.name.toLowerCase();
    if (productId) {
      pageName = `product:${productId}:${productName}`;
    }
    return {
      pageName,
    };
  };

  /**
   * @method handlePickupModalClick -
   * method is responsible for invoking the method for open pickup modal
   */

  handlePickupModalClick = (e) => {
    const {
      onPickUpOpenClick,
      productInfo,
      onPickupClickAddon,
      isBopisEligible,
      isBossEligible,
      itemBrand,
      isProductPickup,
      availableTCPmapNewStyleId,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      alternateBrand,
      isPickUpSelected,
    } = this.props;
    e.preventDefault();
    onPickUpOpenClick({
      generalProductId: productInfo.generalProductId,
      isBopisCtaEnabled: isBopisEligible,
      isBossCtaEnabled: isBossEligible,
      currentProduct: { ...productInfo, itemBrand },
      colorProductId: productInfo.generalProductId,
      isProductPickup,
      availableTCPmapNewStyleId,
      availableTCPMultipackProductMapping: initialMultipackMapping,
      initialSelectedQty: setInitialTCPStyleQty,
      setInitialTCPStyleQty,
      alternateBrand,
      isBopisPickup: isPickUpSelected === 'bopis',
    });
    if (onPickupClickAddon) {
      onPickupClickAddon();
    }
  };

  getBopisInventoryStatus = () => {
    const { bopisItemInventory } = this.props;
    const bopisItemInventoryRes = getBopisItemInventoryRes(bopisItemInventory);
    return bopisItemInventoryRes && bopisItemInventoryRes.status;
  };

  oldPickupCheck = (showPickupInfo, isNewQVEnabled) => {
    return showPickupInfo && !isNewQVEnabled;
  };

  showFindInStore = () => {
    const { showPickupInfo, isSkuResolved } = this.props;
    let { userDefaultStore } = this.props;
    userDefaultStore = this.userStoreFromStorage(userDefaultStore);
    return showPickupInfo && userDefaultStore && isSkuResolved;
  };

  /**
   * @method handleChangeStoreOnKeyPress
   * handles the change store modal when Enter key is pressed post tabbing on the link
   */
  handleChangeStoreOnKeyPress = (event) =>
    handleGenericKeyDown(event, KEY_CODES.ENTER, this.handlePickupModalClick);

  /**
   * @method renderPickupTitle
   * @param {object} miscInfo carries the information of the product with
   * respective color product
   * @param {object} userDefaultStore carries the information of the store user's
   * default store
   */
  renderPickupTitle() {
    const {
      isSkuResolved,
      isBopisEligible,
      showChangeStore,
      pickupTitleText,
      isBossEligBossInvAvail,
      labels,
    } = this.props;
    let { userDefaultStore } = this.props;
    userDefaultStore = this.userStoreFromStorage(userDefaultStore);
    if (isSkuResolved) {
      if (userDefaultStore) {
        if (showChangeStore) {
          /**
           * if the sku is resolved and the user has added a store to the account
           * then it @returns - store name and the link to change the store
           * */
          return (
            <React.Fragment>
              <BodyCopy
                className="store-name capFirstLetter"
                fontSize="fs16"
                fontFamily="secondary"
                fontWeight="semibold"
                component="span"
              >
                {userDefaultStore.basicInfo.storeName}
              </BodyCopy>
              <Anchor
                fontSizeVariation="medium"
                className="change-store-link"
                role="button"
                tabIndex="0"
                onKeyDown={this.handleChangeStoreOnKeyPress}
                handleLinkClick={this.handlePickupModalClick}
                underline
                noLink
              >
                {labels.lbl_Product_pickup_CHANGE_STORE}
              </Anchor>
            </React.Fragment>
          );
        }
        return (
          <BodyCopy fontSize="fs16" fontWeight="semibold" fontFamily="secondary">
            {labels.lbl_Product_pickup_TITLE_DEFAULT_NOSTORE}
          </BodyCopy>
        );
      }
      if (isBopisEligible && !isBossEligBossInvAvail) {
        // bopis only
        /**
         * @returns if the product is only bopis eligible and the sku is resolved
         * then it @returns {labels.PRODUCT_BOPIS}
         */
        return (
          <BodyCopy fontSize="fs16" fontWeight="semibold" fontFamily="secondary">
            {pickupTitleText}
          </BodyCopy>
        );
      }
      /**
       * @returns if the product is only boss eligible and the sku is resolved
       * then it @returns {labels.TITLE_DEFAULT_NOSTORE}
       */
      return (
        <BodyCopy fontSize="fs16" fontWeight="semibold" fontFamily="secondary">
          {pickupTitleText}
        </BodyCopy>
      );
    }
    return (
      <BodyCopy fontSize="fs16" fontWeight="semibold" fontFamily="secondary">
        {pickupTitleText}
      </BodyCopy>
    );
  }

  /**
   * @method renderPickupTitleNewQV
   * @param {object} userDefaultStore carries the information of the store user's
   * default store
   */
  renderPickupTitleNewQV() {
    const { labels, isNewPDPEnabled, isNewQVEnabled } = this.props;
    let { userDefaultStore } = this.props;
    userDefaultStore = this.userStoreFromStorage(userDefaultStore);
    const pickupIconPath = `location-icon`;
    if (userDefaultStore) {
      /**
       * it @returns - store name and the link to change the store
       * */
      return (
        <React.Fragment>
          <img
            className="store-icon"
            alt={getLabelValue(labels, 'lbl_pickup_icon')}
            src={getIconPath('store-star')}
          />
          <div className="store-info-container">
            {isNewPDPEnabled || isNewQVEnabled ? (
              <img
                className="location-icon-store-name"
                alt="location icon"
                src={getIconPath(pickupIconPath)}
              />
            ) : null}
            <BodyCopy
              className="store-name quick-view-drawer-redesign capFirstLetter"
              fontFamily="secondary"
              fontWeight="extrabold"
              component="span"
            >
              {userDefaultStore?.basicInfo?.storeName}
            </BodyCopy>
          </div>

          {isNewPDPEnabled ? (
            <div className="text-right">
              <Anchor
                fontSizeVariation="medium"
                className="change-store-link quick-view-drawer-redesign"
                role="button"
                tabIndex="0"
                onKeyDown={this.handleChangeStoreOnKeyPress}
                handleLinkClick={this.handlePickupModalClick}
                underline
                noLink
              >
                {labels?.lbl_Product_pickup_CHANGE_STORE?.replace('(', '')?.replace(')', '')}
              </Anchor>
            </div>
          ) : (
            <Anchor
              fontSizeVariation="medium"
              className="change-store-link quick-view-drawer-redesign"
              role="button"
              tabIndex="0"
              onKeyDown={this.handleChangeStoreOnKeyPress}
              handleLinkClick={this.handlePickupModalClick}
              underline
              noLink
            >
              {labels?.lbl_Product_pickup_CHANGE_STORE?.replace('(', '')?.replace(')', '')}
            </Anchor>
          )}
        </React.Fragment>
      );
    }
    return null;
  }
  /**
   * @member renderPickupInfo
   * @description this method returns the information of pickup on the
   * respective store based on different scenarios
   * @param {object} miscInfo carries the product information
   * with respective color id
   * @param {object} userDefaultStore carries the information of User's default store
   * @return if the sku is resolved and the product is available in store
   * or both BOSS and BOPIS
   * then is @returns - item availability information along with the
   * pickup today label and the BOSS dates information with the boss pickup
   * label
   * @return if the sku is resolved and the product is only available for BOPIS
   * then it @returns - item availability information with the BOPIS only pickup
   * label
   * @return if the sku is resolved and the product is avialable only for BOSS
   * then it @returns - the boss dates information along with the BOSS only
   * pickup labels
   */

  renderPickupInfo() {
    const { isStoreBopisEligible, labels, isStoreAndProductBossEligible } = this.props;

    const bopisItemInventoryStatus = this.getBopisInventoryStatus();

    const bopisStatusAvailability =
      isStoreBopisEligible && isStoreAndProductBossEligible ? (
        <BodyCopy fontSize="fs12" fontFamily="secondary">
          <BodyCopy
            fontSize="fs12"
            className="availability"
            fontWeight="extrabold"
            fontFamily="secondary"
            color="success"
            component="span"
          >
            {`${bopisItemInventoryStatus}!`}
          </BodyCopy>
          {labels.lbl_Product_pickup_BOPIS_AVAILABLE}
        </BodyCopy>
      ) : null;

    if (isStoreBopisEligible && isStoreAndProductBossEligible) {
      return (
        <div className="pickup-info">
          {bopisStatusAvailability}
          <br />
          <span className="boss-message">
            {labels.lbl_Product_pickup_BOSS_AVAILABLE}
            <PickupPromotionBanner bossBanner />
          </span>
        </div>
      );
    }
    if (isStoreBopisEligible) {
      return <div className="pickup-info">{bopisStatusAvailability}</div>;
    }

    if (isStoreAndProductBossEligible) {
      return (
        <div className="pickup-info">
          <span className="boss-message">
            {labels.lbl_Product_pickup_BOSS_ONLY_AVAILABLE}
            <PickupPromotionBanner bossBanner />
          </span>
        </div>
      );
    }
    return null;
  }

  renderPickupInfoNewQV(labels, pickupIconPath) {
    const { isNewQVEnabled, quickViewLabels } = this.props;
    if (isNewQVEnabled) {
      return (
        <div className="pickup-info">
          <div className="pickup-icon-header">
            <img
              className="pickup-icon quick-view-drawer-redesign"
              alt={getLabelValue(labels, 'lbl_pickup_icon')}
              src={getIconPath(pickupIconPath)}
            />
            <BodyCopy
              fontSize="fs14"
              fontWeight="extrabold"
              fontFamily="secondary"
              component="span"
              className="header-pickup-line1"
            >
              {quickViewLabels?.pickItUp}
            </BodyCopy>
          </div>
          <BodyCopy
            fontSize="fs12"
            fontFamily="secondary"
            className="sub-header-pickup line2 quick-view-drawer-redesign"
          >
            {quickViewLabels?.needItASAP}
          </BodyCopy>
          <BodyCopy
            fontSize="fs12"
            fontFamily="secondary"
            className="sub-header-pickup line3 quick-view-drawer-redesign"
          >
            {quickViewLabels?.getItToday}
          </BodyCopy>
        </div>
      );
    }
    return null;
  }

  renderPickupInfoError() {
    const { labels } = this.props;
    return (
      <BodyCopy
        fontSize="fs10"
        fontWeight="regular"
        fontFamily="secondary"
        color="error"
        className="error-pickup-info"
      >
        {labels.lbl_Product_pickup_UNAVAILABLE_IN_STORES}
        UNAVAILABLE IN STORES.
      </BodyCopy>
    );
  }

  renderSoldOutError = () => {
    const { keepAlive, outOfStockLabels } = this.props;
    return keepAlive ? (
      <BodyCopy color="red.500" fontFamily="secondary" fontSize="fs10">
        {outOfStockLabels.itemSoldOutMessage}
      </BodyCopy>
    ) : null;
  };

  renderFulFillmentSection = (
    isPickUpSelected,
    shippingIconPath,
    showPickupInfo,
    pickupIconPath,
    bopisStatusAvailability,
    pageName
  ) => {
    const {
      isNewQVEnabled,
      pickupSelectionUpdate,
      labels,
      quickViewLabels,
      isSubmitting,
      isNewPDPEnabled,
      fromBagPage,
    } = this.props;
    return isNewQVEnabled && !fromBagPage ? (
      <div className="bottom-section quick-view-drawer-redesign">
        <BodyCopy
          className="fulfillment-section-heading quick-view-drawer-redesign"
          fontSize="fs14"
          fontFamily="secondary"
          fontWeight="extrabold"
        >
          {quickViewLabels?.deliveryMethod}
        </BodyCopy>
        <div className="pickup-sub-container quick-view-drawer-redesign">
          <div className="pickup-header quick-view-drawer-redesign">
            <div
              className={`title-pickup-section-shipping quick-view-drawer-redesign ${
                isPickUpSelected === 'home' ? 'selected' : ''
              }`}
              onClick={() => pickupSelectionUpdate('home')}
              onKeyDown={() => pickupSelectionUpdate('home')}
              role="button"
              tabIndex="0"
            >
              <div className="shipping-text-section quick-view-drawer-redesign">
                <div className="header-image-wrapper">
                  <img
                    className="shipping-icon quick-view-drawer-redesign"
                    alt={getLabelValue(labels, 'lbl_shipping_icon')}
                    src={getIconPath(shippingIconPath)}
                  />
                  <BodyCopy
                    fontSize="fs14"
                    fontWeight="extrabold"
                    fontFamily="secondary"
                    component="span"
                    className="header-shipit-line1"
                  >
                    {quickViewLabels?.shipIt}
                  </BodyCopy>
                </div>
                <BodyCopy
                  fontSize="fs12"
                  fontFamily="secondary"
                  className="sub-header-shipit line2 quick-view-drawer-redesign"
                >
                  {quickViewLabels?.freeShipping}
                </BodyCopy>
                <BodyCopy
                  fontSize="fs12"
                  fontFamily="secondary"
                  className="sub-header-shipit line3 quick-view-drawer-redesign"
                >
                  {quickViewLabels?.noMinimumRequired}
                </BodyCopy>
              </div>
            </div>
          </div>
          <div className="pickup-content quick-view-drawer-redesign">
            <div
              className={`pickup-section quick-view-drawer-redesign ${
                isPickUpSelected === 'pickup' ? 'selected' : ''
              }`}
              onClick={() => pickupSelectionUpdate('pickup')}
              onKeyDown={() => pickupSelectionUpdate('pickup')}
              role="button"
              tabIndex="0"
            >
              <div className="pickup-details quick-view-drawer-redesign">
                {this.oldPickupCheck(showPickupInfo, isNewQVEnabled) && this.renderPickupInfo()}
                {isNewQVEnabled && this.renderPickupInfoNewQV(labels, pickupIconPath)}
              </div>
            </div>
          </div>
        </div>
        {isPickUpSelected === 'pickup' ? (
          <div className="store-section-quick-view-drawer-redesign-container">
            <div className="store-section-quick-view-drawer-redesign">
              {this.renderPickupTitleNewQV()}
              {bopisStatusAvailability}
            </div>
          </div>
        ) : null}
      </div>
    ) : (
      !isNewPDPEnabled && !fromBagPage && (
        <div className="pickup-sub-container">
          <div className="pickup-header">
            <div className="title-pickup-section">
              <img
                className="shipping-icon"
                alt={getLabelValue(labels, 'lbl_shipping_icon')}
                src={getIconPath('fast-shipping')}
              />
              <div className="shipping-text-section">
                <BodyCopy
                  fontSize="fs16"
                  fontWeight="semibold"
                  fontFamily="secondary"
                  component="span"
                >
                  {labels.lbl_Product_pickup_FREE_SHIPPING}
                </BodyCopy>
                <BodyCopy fontSize="fs12" fontFamily="secondary" className="sub-header-pickup">
                  {labels.lbl_Product_pickup_NO_MIN_PURCHASE}
                </BodyCopy>
              </div>
            </div>
          </div>
          <div className="pickup-content">
            <div className="pickup-section">
              <div className="title-pickup-section">
                <img
                  className="pickup-icon"
                  alt={getLabelValue(labels, 'lbl_pickup_icon')}
                  src={getIconPath('marker-icon')}
                />
              </div>
              <div className="pickup-details">
                {this.renderPickupTitle()}
                {showPickupInfo && this.renderPickupInfo()}
              </div>
            </div>
            <ClickTracker
              clickData={{
                customEvents: ['event131'],
                pageName,
              }}
              className="btn-find-in-store-wrapper"
            >
              <Button
                className="button-find-in-store"
                buttonVariation="fixed-width"
                fill="BLACK"
                disabled={isSubmitting}
                onClick={this.handlePickupModalClick}
              >
                {this.showFindInStore()
                  ? labels.lbl_Product_pickup_PICKUP_IN_STORE
                  : labels.lbl_Product_pickup_FIND_STORE}
              </Button>
            </ClickTracker>
          </div>
        </div>
      )
    );
  };

  userStoreFromStorage = (userDefaultStore) => {
    let userStore = userDefaultStore;
    if (userStore) {
      setLocalStorage({ key: 'defaultStore', value: JSON.stringify(userStore) });
    } else {
      userStore = JSON.parse(getLocalStorage('defaultStore')) || '';
    }
    return userStore;
  };

  checkRenderPickupSection = (storeCityName, storeStateName, isPickUpSelected) => {
    return (
      storeCityName &&
      storeStateName &&
      (isPickUpSelected === 'bopis' || isPickUpSelected === 'boss')
    );
  };

  getOOSClassName = (bopisItemInventoryStatus) => {
    return bopisItemInventoryStatus === 'Out of Stock' ? ' out-of-stock' : '';
  };

  renderBopisStatus = (bopisItemInventoryStatus, isBopis, isBoss, shouldShowFulfillment) => {
    const {
      isBopisEligible,
      isStoreBopisEligible,
      isStoreAndProductBossEligible,
      isAllSizeDisabled,
    } = this.props;
    if (!shouldShowFulfillment) {
      return (
        (bopisItemInventoryStatus && isBopis && isBopisEligible) ||
        (isBoss && isStoreAndProductBossEligible && !isAllSizeDisabled)
      );
    }
    return isStoreBopisEligible && isStoreAndProductBossEligible;
  };

  getIsBopisProduct = (isBopisEnabled, isBopisEligible) => {
    return isBopisEnabled && isBopisEligible;
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  render() {
    const {
      className,
      showPickupInfo,
      isSubmitting,
      labels,
      isAnchor,
      sizeUnavailable,
      isOutfitVariant,
      productInfo,
      isPickUpSelected,
      pickupSelectionUpdate,
      quickViewLabels,
      isNewQVEnabled,
      checkForOOSForVariant,
      isNewPDPEnabled,
      miscInfo,
      isAllSizeDisabled,
      isBossEnabled,
      isBopisEnabled,
      isSkuResolved,
      isBopisEligible,
    } = this.props;
    let { userDefaultStore } = this.props;
    userDefaultStore = this.userStoreFromStorage(userDefaultStore);
    const { pageName } = this.getPageName(productInfo);
    const { isStoreBopisEligible, fromBagPage } = this.props;
    const isBopisProduct = this.getIsBopisProduct(isBopisEnabled, isBopisEligible);
    const pickupIconPath = `location-icon`;
    const shippingIconPath = `standard-shipping`;
    const storeCityName = userDefaultStore?.basicInfo?.address?.city || '';
    const storeStateName = userDefaultStore?.basicInfo?.address?.state || '';
    const isBopis = isPickUpSelected === 'bopis';
    const isBoss = isPickUpSelected === 'boss';
    const bopisItemInventoryStatus = isBoss ? 'In Stock' : this.getBopisInventoryStatus();

    const shouldShowFulfillment = getShouldShowFulfillmentSection(
      fromBagPage,
      isNewPDPEnabled,
      isNewQVEnabled
    );
    const bopisStatusAvailability = this.renderBopisStatus(
      bopisItemInventoryStatus,
      isBopis,
      isBoss,
      shouldShowFulfillment
    ) ? (
      <BodyCopy fontFamily="secondary" className="availability-wrapper quick-view-drawer-redesign">
        <BodyCopy
          className={`availability quick-view-drawer-redesign${this.getOOSClassName(
            bopisItemInventoryStatus
          )}`}
          fontWeight="extrabold"
          fontFamily="secondary"
          component="span"
        >
          {`${bopisItemInventoryStatus}`}
        </BodyCopy>
        <BodyCopy
          className="label-availability quick-view-drawer-redesign"
          fontWeight="normal"
          fontFamily="secondary"
          component="span"
        >
          at this store
        </BodyCopy>
      </BodyCopy>
    ) : null;

    return (
      <React.Fragment>
        {isOutfitVariant && (
          <ClickTracker
            clickData={{
              customEvents: ['event131'],
              pageName,
            }}
          >
            <Button
              className="button-find-in-store"
              buttonVariation="fixed-width"
              fill="BLACK"
              disabled={isSubmitting}
              onClick={this.handlePickupModalClick}
            >
              {showPickupInfo
                ? labels.lbl_Product_pickup_PICKUP_IN_STORE
                : labels.lbl_Product_pickup_FIND_STORE}
            </Button>
          </ClickTracker>
        )}
        {!isAnchor && !isOutfitVariant ? (
          <div className={className}>
            {shouldShowFulfillment ? (
              this.renderFulFillmentSection(
                isPickUpSelected,
                shippingIconPath,
                showPickupInfo,
                pickupIconPath,
                bopisStatusAvailability,
                pageName
              )
            ) : (
              <>
                <DeliveryMethods
                  className={className}
                  isPickUpSelected={isPickUpSelected}
                  isNewQVEnabled={isNewQVEnabled}
                  quickViewLabels={quickViewLabels}
                  pickupSelectionUpdate={pickupSelectionUpdate}
                  checkForOOSForVariant={checkForOOSForVariant}
                  isOnlineExclusive={miscInfo?.badge1?.defaultBadge}
                  isBossEnabled={isBossEnabled}
                  isBopisEnabled={isBopisProduct}
                  miscInfo={miscInfo}
                  isAllSizeDisabled={isAllSizeDisabled}
                  userDefaultStore={userDefaultStore}
                  isStoreBopisEligible={isStoreBopisEligible}
                />
                {this.checkRenderPickupSection(storeCityName, storeStateName, isPickUpSelected) ? (
                  <div className="store-section-quick-view-drawer-redesign-container">
                    <div className="store-section-quick-view-drawer-redesign">
                      {this.renderPickupTitleNewQV()}
                      {userDefaultStore?.basicInfo && storeCityName && storeStateName && (
                        <BodyCopy
                          className="city-state quick-view-drawer-redesign"
                          fontFamily="secondary"
                          fontWeight="semibold"
                          component="span"
                          fontSize="fs12"
                        >
                          {`${storeCityName}, ${storeStateName}`}
                        </BodyCopy>
                      )}
                      {isSkuResolved ? bopisStatusAvailability : null}
                    </div>
                  </div>
                ) : null}
              </>
            )}
          </div>
        ) : (
          !isOutfitVariant && (
            <p className="size-unavailable">
              <span className="unavailable-text">{sizeUnavailable}</span>
              <span className="size-find-in-store">
                <Anchor
                  noLink
                  handleLinkClick={this.handlePickupModalClick}
                  title={labels.lbl_Product_pickup_FIND_STORE}
                  className="size-unavailable-anchor"
                >
                  {labels.lbl_Product_pickup_FIND_STORE}
                </Anchor>
              </span>
            </p>
          )
        )}
        {/* {isBopisEligible && (
           <ContentSlot contentSlotName="pdp_bopis_promo" className="product-details-bopis-promo" />
         )} */}
      </React.Fragment>
    );
  }
}

export default withStyles(ProductPickup, ProductPickupStyles);
