/* eslint-disable max-lines */
/* eslint-disable no-shadow */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { TouchableOpacity } from 'react-native';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { BodyCopyWithTextTransform } from '@tcp/core/src/components/common/atoms/styledWrapper/styledWrapper.native';
import PickupPromotionBanner from '@tcp/core/src/components/common/molecules/PickupPromotionBanner';
import { formatProductsData, isGymboree } from '@tcp/core/src/utils/utils';
import names from '../../../../../constants/eventsName.constants';

import ClickTracker from '../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import {
  COLOR_FITS_SIZES_MAP_PROP_TYPE,
  PRICING_PROP_TYPES,
} from '../../PickupStoreModal/PickUpStoreModal.proptypes';
import { handleGenericKeyDown } from '../util';
import withStyles from '../../../hoc/withStyles';

import { Button, Anchor, BodyCopy } from '../../../atoms';
import LineComp from '../../../atoms/Line';
import {
  Container,
  styles,
  FastShippingContainer,
  FastShippingTextContainer,
  StoreContainer,
  RowContainer,
  ColumnContainer,
  UnavailableLink,
  ShippingContainer,
  ShipSelectionTypeContainer,
  ShippingCard,
  IconContainer,
  LabelContainer,
  ButtonContainerNewDesign,
  StoreContainerNewDesign,
  StoreTitleAndChangeContainer,
  PillContainerForInventoryStatus,
  TextImageContainer,
} from '../styles/ProductPickup.style.native';

import { KEY_CODES } from '../../../../../constants/keyboard.constants';

import Image from '../../../atoms/Image';

const shipping = require('../../../../../../../mobileapp/src/assets/images/shipping.png');
const shippingNew = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/shipping-not-blue.png');
const shippingSelected = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/shipping-selected.png');
const shippingSelectedGYM = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/shipping-selected-gym.png');
const storeLocator = require('../../../../../../../mobileapp/src/assets/images/store_locator.png');
const storeLocatorSelected = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/store-locator-blue.png');
const storeLocatorSelectedGYM = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/store-locator-gym.png');
const rightArrowImage = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icons-medium-left.png');
const warning = require('../../../../../../../mobileapp/src/assets/images/circle-warning-fill.png');

/**
 *  Describes a general product, not yet specialized by chosing a color, size, etc.
 *  For example, a product shown in reccomendations, or PDP.
 */
export const PRODUCT_INFO_PROP_TYPES = {
  /* this identifies a product for Bazaarvoice (reviews and ratings component) */
  ratingsProductId: PropTypes.string,

  /** This identifies the product regardless of color/fit/size (i.e., changing size/fit/color does not change this value) */
  generalProductId: PropTypes.string.isRequired,
  /** The name of the product to be displayed to the user */
  name: PropTypes.string.isRequired,
  /** Images for this product in different colors.
   * This is an object of key-vale pairs. the key is the color name, and the value has thew shape described below.
   */
  imagesByColor: PropTypes.oneOfType([
    PropTypes.objectOf(
      PropTypes.shape({
        extraImages: PropTypes.arrayOf(
          PropTypes.shape({
            iconSizeImageUrl: PropTypes.string.isRequired,
            regularSizeImageUrl: PropTypes.string.isRequired,
            bigSizeImageUrl: PropTypes.string.isRequired,
            superSizeImageUrl: PropTypes.string.isRequired,
          })
        ),
      })
    ),
    PropTypes.objectOf(
      PropTypes.shape({
        basicImageUrl: PropTypes.string.isRequired,
      })
    ),
  ]),
  /** optional displayNames of the color fit and size (e.g., for gift cards it is {color: 'Design, size: 'Value']) */
  colorFitSizeDisplayNames: PropTypes.shape({
    color: PropTypes.string,
    fit: PropTypes.string,
    size: PropTypes.string,
  }),
  /**
   * The available color fit and size options for this product
   * Organized in a three level nesting (similar to a site navigation) with L1 being the color,
   * L2 being the fit, and L3 being the size
   */
  colorFitsSizesMap: COLOR_FITS_SIZES_MAP_PROP_TYPE,

  /** SEO Friendly URL required to have the image and title linkable */
  pdpUrl: PropTypes.string.isRequired,

  /* Short description of the product. */
  shortDescription: PropTypes.string,

  /* Long description of the product that may include HTML. */
  longDescription: PropTypes.string,

  /** Flags if this SKU/product is a Gift-Card */
  isGiftCard: PropTypes.bool,

  /** Product price, which may be overriden by sku-level price */
  ...PRICING_PROP_TYPES,
};

export const PRODUCT_INFO_PROP_TYPE_SHAPE = PropTypes.shape(PRODUCT_INFO_PROP_TYPES);

class ProductPickupOld extends React.PureComponent {
  static propTypes = {
    miscInfo: PropTypes.shape({}),

    /**
     * Information regarding the product at the swatch/color level.
     */
    productInfo: PRODUCT_INFO_PROP_TYPE_SHAPE.isRequired,
    /** User's preferred store information */
    userDefaultStore: PropTypes.shape({
      basicInfo: PropTypes.shape({
        /** store id identifier */
        id: PropTypes.string.isRequired,
        /** store Name */
        storeName: PropTypes.string.isRequired,
      }).isRequired,
    }),
    /**
     * User's latitude and longitude coordinates if access is allowed.
     */
    userGeoCoordinates: PropTypes.shape({
      lat: PropTypes.number,
      long: PropTypes.number,
    }),

    /**
     * Item values selected for the product, to use in the BOPIS modal if it
     * gets opened.
     */
    itemValues: PropTypes.shape({
      color: PropTypes.string.isRequired,
      Fit: PropTypes.string,
      Size: PropTypes.string,
      Quantity: PropTypes.number,
    }),

    /**
     * method responsible for triggering the operator method for BopisQuickViewModal
     */
    onPickUpOpenClick: PropTypes.func,
    onPickupClickAddon: PropTypes.func,
    /**
     * carries the inventory information of the bopis item selected
     */
    bopisItemInventory: PropTypes.arrayOf(PropTypes.object),
    isBopisEligible: PropTypes.bool,
    isBossEligible: PropTypes.bool,
    isSkuResolved: PropTypes.bool,
    showChangeStore: PropTypes.bool,
    pickupTitleText: PropTypes.string,
    isBossEligBossInvAvail: PropTypes.bool,
    isStoreBopisEligible: PropTypes.bool,
    itemBrand: PropTypes.string,
    showPickupInfo: PropTypes.bool,
    isSubmitting: PropTypes.bool,
    labels: PropTypes.shape({
      lbl_Product_pickup_BOPIS_AVAILABLE: PropTypes.string,
      lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: PropTypes.string,
      lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: PropTypes.string,
      lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: PropTypes.string,
      lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: PropTypes.string,
      lbl_Product_pickup_BOSS_AVAILABLE: PropTypes.string,
      lbl_Product_pickup_BOSS_ONLY_AVAILABLE: PropTypes.string,
      lbl_Product_pickup_FIND_STORE: PropTypes.string,
      lbl_Product_pickup_FREE_SHIPPING: PropTypes.string,
      lbl_Product_pickup_NO_MIN_PURCHASE: PropTypes.string,
      lbl_Product_pickup_PICKUP_IN_STORE: PropTypes.string,
      lbl_Product_pickup_PRODUCT_BOPIS: PropTypes.string,
      lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: PropTypes.string,
      lbl_Product_pickup_OUT_OF_STOCK_button_redesign: PropTypes.string,
      lbl_Product_pickup_PICKUP_IN_STORE_redesign: PropTypes.string,
      lbl_Product_pickup_FIND_STORE_redesign: PropTypes.string,
      lbl_Product_pickup_CHANGE_STORE_redesign: PropTypes.string,
      lbl_Product_pickup_CHANGE_STORE: PropTypes.string,
      lbl_Product_pickup_BOPIS_IN_THE_STORE_redesign: PropTypes.string,
    }),
    simplifiedProductPickupView: PropTypes.bool,
    isAnchor: PropTypes.bool,
    sizeUnavailable: PropTypes.string,
    isStoreAndProductBossEligible: PropTypes.bool,
    availableTCPmapNewStyleId: PropTypes.shape([]),
    initialMultipackMapping: PropTypes.shape([]),
    setInitialTCPStyleQty: PropTypes.string,
    isNewReDesignFullfilmentSection: PropTypes.bool,
    isProductPickup: PropTypes.bool,
    noBossBopisInfo: PropTypes.bool,
    size: PropTypes.string,
    selectedPickUpType: PropTypes.func,
  };

  static defaultProps = {
    miscInfo: {},
    userDefaultStore: {
      basicInfo: {
        id: '',
        storeName: '',
      },
    },
    userGeoCoordinates: {
      lat: null,
      long: null,
    },
    itemValues: {
      color: '',
      Fit: '',
      Size: '',
      Quantity: null,
    },
    itemBrand: '',
    onPickUpOpenClick: null,
    onPickupClickAddon: () => {},
    bopisItemInventory: [],
    isBopisEligible: false,
    isBossEligible: false,
    isSkuResolved: false,
    showChangeStore: false,
    pickupTitleText: '',
    isBossEligBossInvAvail: false,
    isStoreBopisEligible: false,
    showPickupInfo: false,
    isSubmitting: false,
    labels: {
      lbl_Product_pickup_BOPIS_AVAILABLE: 'Pick up TODAY!',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: 'husky',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: 'plus',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: 'slim',
      lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: 'Item available for pickup TODAY',
      lbl_Product_pickup_BOSS_AVAILABLE: 'Or choose NO RUSH Pickup ',
      lbl_Product_pickup_BOSS_ONLY_AVAILABLE: 'Choose NO RUSH Pickup ',
      lbl_Product_pickup_FIND_STORE: 'FIND A STORE',
      lbl_Product_pickup_FREE_SHIPPING: 'FREE Shipping Every Day!',
      lbl_Product_pickup_NO_MIN_PURCHASE: 'No Minimum Purchase Required.',
      lbl_Product_pickup_PICKUP_IN_STORE: 'PICK UP IN STORE',
      lbl_Product_pickup_PRODUCT_BOPIS: 'Buy online - Pick up in store',
      lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: 'Select Store',
    },
    simplifiedProductPickupView: false,
    isAnchor: false,
    sizeUnavailable: 'Size unavailable online?',
    isStoreAndProductBossEligible: false,
    availableTCPmapNewStyleId: [],
    initialMultipackMapping: [],
    setInitialTCPStyleQty: '',
    isNewReDesignFullfilmentSection: false,
    isProductPickup: true,
    noBossBopisInfo: false,
    size: '',
    selectedPickUpType: () => {},
  };

  constructor(props) {
    super(props);
    this.handlePickupModalClick = this.handlePickupModalClick.bind(this);
    this.state = {
      currentSelectedPickUpType: 'shipIt',
    };
  }

  /**
   * @method handlePickupModalClick -
   * method is responsible for invoking the method for open pickup modal
   */

  handlePickupModalClick = () => {
    const {
      productInfo,
      onPickUpOpenClick,
      onPickupClickAddon,
      isBopisEligible,
      isBossEligible,
      itemBrand,
      availableTCPmapNewStyleId,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      isProductPickup,
    } = this.props;
    const { generalProductId } = productInfo;

    onPickUpOpenClick({
      generalProductId,
      colorProductId: generalProductId,
      isBopisCtaEnabled: isBopisEligible,
      isBossCtaEnabled: isBossEligible,
      currentProduct: { ...productInfo, itemBrand },
      availableTCPmapNewStyleId,
      availableTCPMultipackProductMapping: initialMultipackMapping,
      initialSelectedQty: setInitialTCPStyleQty,
      isProductPickup,
    });

    if (onPickupClickAddon) {
      onPickupClickAddon();
    }
  };

  handleChangeStoreOnKeyPress = (event) =>
    handleGenericKeyDown(event, KEY_CODES.ENTER, this.handlePickupModalClick);

  handleShippingSelection = (name) => {
    const { selectedPickUpType } = this.props;
    selectedPickUpType(name);
    this.setState({ currentSelectedPickUpType: name });
  };

  getGymStyling = () => {
    return isGymboree()
      ? { fontFamily: 'primary', fontWeight: 'semibold' }
      : { fontFamily: 'secondary', fontWeight: 'bold' };
  };

  getQuantityFromBOPIS = () => {
    const { bopisItemInventory } = this.props;

    const bopisItemInventoryRes =
      bopisItemInventory &&
      bopisItemInventory.inventoryResponse &&
      bopisItemInventory.inventoryResponse[0];

    return bopisItemInventoryRes && bopisItemInventoryRes.quantity;
  };

  getButtonTextNewDesign = () => {
    const { showPickupInfo, labels, size } = this.props;

    if (size && this.getQuantityFromBOPIS() === 0)
      return labels.lbl_Product_pickup_OUT_OF_STOCK_button_redesign;
    if (showPickupInfo) return labels.lbl_Product_pickup_PICKUP_IN_STORE_redesign;
    return labels.lbl_Product_pickup_FIND_STORE_redesign;
  };

  renderPickupTitle() {
    const {
      isSkuResolved,
      isBopisEligible,
      showChangeStore,
      pickupTitleText,
      userDefaultStore,
      isBossEligBossInvAvail,
      labels,
    } = this.props;
    if (isSkuResolved) {
      if (userDefaultStore) {
        if (showChangeStore) {
          /**
           * if the sku is resolved and the user has added a store to the account
           * then it @returns - store name and the link to change the store
           * */
          return this.renderShowChangeStore();
        }
        return (
          <BodyCopy
            margin="0 12px 0 0"
            dataLocator="pdp_store_name_value"
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="semibold"
            color="gray.900"
            text={labels.lbl_Product_pickup_TITLE_DEFAULT_NOSTORE}
          />
        );
      }
      if (isBopisEligible && !isBossEligBossInvAvail) {
        // bopis only
        /**
         * @returns if the product is only bopis eligible and the sku is resolved
         * then it @returns {labels.PRODUCT_BOPIS}
         */
        return (
          <BodyCopy
            margin="0 12px 0 0"
            dataLocator="pdp_store_name_value"
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="semibold"
            color="gray.900"
            text={pickupTitleText}
          />
        );
      }
      return (
        <BodyCopy
          margin="0 12px 0 0"
          dataLocator="pdp_store_name_value"
          fontFamily="secondary"
          fontSize="fs16"
          fontWeight="semibold"
          color="gray.900"
          text={pickupTitleText}
        />
      );
    }
    return (
      <BodyCopy
        margin="0 12px 0 0"
        dataLocator="pdp_store_name_value"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="semibold"
        color="gray.900"
        text={pickupTitleText}
      />
    );
  }

  renderShowChangeStore = () => {
    const { userDefaultStore, labels, isNewReDesignFullfilmentSection } = this.props;
    return (
      <React.Fragment>
        <BodyCopyWithTextTransform
          margin="0 12px 0 0"
          dataLocator="pdp_store_name_value"
          fontFamily="secondary"
          fontSize={isNewReDesignFullfilmentSection ? 'fs14' : 'fs16'}
          fontWeight={isNewReDesignFullfilmentSection ? 'black' : 'semibold'}
          color="gray.900"
          text={userDefaultStore.basicInfo.storeName}
        />
        <Anchor
          fontSizeVariation={isNewReDesignFullfilmentSection ? 'large' : 'medium'}
          anchorVariation={isNewReDesignFullfilmentSection ? 'primary' : 'custom'}
          colorName="gray.900"
          underline
          href="#"
          locator="pdp_change_store_label"
          className="details-link"
          onPress={this.handlePickupModalClick}
          text={
            isNewReDesignFullfilmentSection
              ? labels.lbl_Product_pickup_CHANGE_STORE_redesign
              : labels.lbl_Product_pickup_CHANGE_STORE
          }
        />
      </React.Fragment>
    );
  };

  renderBopisStatusAvialability = (bopisItemInventoryStatus) => {
    const { labels, isStoreBopisEligible } = this.props;
    return isStoreBopisEligible ? (
      <RowContainer margins="4px 0 0 0px">
        <PillContainerForInventoryStatus>
          <BodyCopy
            dataLocator="pdp_store_availability_label"
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="black"
            color="gray.900"
            text={`${bopisItemInventoryStatus}`}
          />
        </PillContainerForInventoryStatus>
        <BodyCopy
          margin="0 0 0 4px"
          dataLocator="pdp_store_availability_value"
          fontFamily="primary"
          fontSize="fs14"
          fontWeight="regular"
          color="gray.900"
          text={labels.lbl_Product_pickup_BOPIS_IN_THE_STORE_redesign}
        />
      </RowContainer>
    ) : null;
  };

  renderPickupInfoReDesign() {
    const { bopisItemInventory, isStoreBopisEligible, labels, isStoreAndProductBossEligible } =
      this.props;
    const bopisItemInventoryRes =
      bopisItemInventory &&
      bopisItemInventory.inventoryResponse &&
      bopisItemInventory.inventoryResponse[0];
    const bopisItemInventoryStatus = bopisItemInventoryRes && bopisItemInventoryRes.status;
    if (isStoreBopisEligible && isStoreAndProductBossEligible) {
      return (
        <React.Fragment>
          {this.renderBopisStatusAvialability(bopisItemInventoryStatus)}
          <RowContainer margins="4px 0 0 0px">
            <BodyCopy
              dataLocator="pdp_store_no_rush_label"
              fontFamily="secondary"
              fontSize="fs12"
              fontWeight="regular"
              color="gray.900"
              text={
                isStoreBopisEligible
                  ? labels.lbl_Product_pickup_BOSS_AVAILABLE
                  : labels.lbl_Product_pickup_BOSS_ONLY_AVAILABLE
              }
            />
            <PickupPromotionBanner bossBanner />
          </RowContainer>
        </React.Fragment>
      );
    }

    if (isStoreBopisEligible) {
      return this.renderBopisStatusAvialability(bopisItemInventoryStatus);
    }

    if (isStoreAndProductBossEligible) {
      return (
        <RowContainer margins="4px 0 0 0px">
          <BodyCopy
            dataLocator="pdp_store_no_rush_label"
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="regular"
            color="gray.900"
            text={labels.lbl_Product_pickup_BOSS_ONLY_AVAILABLE}
          />
          <PickupPromotionBanner bossBanner />
        </RowContainer>
      );
    }
    return null;
  }

  /**
   * @method renderPickupTitle
   * @param {object} miscInfo carries the information of the product with
   * respective color product
   * @param {object} userDefaultStore carries the information of the store user's
   * default store
   */

  /* eslint-disable */
  renderPickupInfo() {
    const { bopisItemInventory, isStoreBopisEligible, labels, isStoreAndProductBossEligible } =
      this.props;
    const bopisItemInventoryRes =
      bopisItemInventory &&
      bopisItemInventory.inventoryResponse &&
      bopisItemInventory.inventoryResponse[0];
    const bopisItemInventoryStatus = bopisItemInventoryRes && bopisItemInventoryRes.status;

    const bopisStatusAvailability = isStoreBopisEligible ? (
      <RowContainer margins="4px 0 0 0px">
        <BodyCopy
          dataLocator="pdp_store_availability_label"
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="semibold"
          color="green.500"
          text={`${bopisItemInventoryStatus}!`}
        />
        <BodyCopy
          margin="0 0 0 4px"
          dataLocator="pdp_store_availability_value"
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="regular"
          color="gray.900"
          text={labels.lbl_Product_pickup_BOPIS_AVAILABLE}
        />
      </RowContainer>
    ) : null;

    if (isStoreBopisEligible && isStoreAndProductBossEligible) {
      return (
        <React.Fragment>
          {bopisStatusAvailability}
          <RowContainer margins="4px 0 0 0px">
            <BodyCopy
              dataLocator="pdp_store_no_rush_label"
              fontFamily="secondary"
              fontSize="fs12"
              fontWeight="regular"
              color="gray.900"
              text={
                isStoreBopisEligible
                  ? labels.lbl_Product_pickup_BOSS_AVAILABLE
                  : labels.lbl_Product_pickup_BOSS_ONLY_AVAILABLE
              }
            />
            <PickupPromotionBanner bossBanner />
          </RowContainer>
        </React.Fragment>
      );
    }

    if (isStoreBopisEligible) {
      return bopisStatusAvailability;
    }

    if (isStoreAndProductBossEligible) {
      return (
        <RowContainer margins="4px 0 0 0px">
          <BodyCopy
            dataLocator="pdp_store_no_rush_label"
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="regular"
            color="gray.900"
            text={labels.lbl_Product_pickup_BOSS_ONLY_AVAILABLE}
          />
          <PickupPromotionBanner bossBanner />
        </RowContainer>
      );
    }
    return null;
  }

  renderPickupInfoError() {
    const { labels } = this.props;
    return (
      <BodyCopy
        dataLocator="pdp_pick_up_error_info"
        fontFamily="secondary"
        fontSize="fs10"
        fontWeight="black"
        color="red.500"
        text={labels.lbl_Product_pickup_UNAVAILABLE_IN_STORES}
      />
    );
  }

  renderPickUpInStoreAnchorButton() {
    const { labels, sizeUnavailable } = this.props;
    return (
      <UnavailableLink>
        <BodyCopyWithSpacing
          fontFamily="secondary"
          fontWeight="semibold"
          fontSize="fs12"
          color="black"
          text={sizeUnavailable}
          spacingStyles="padding-right-XS"
        />
        <Anchor
          noLink
          onPress={this.handlePickupModalClick}
          accessibilityRole="link"
          accessibilityLabel={labels.lbl_Product_pickup_FIND_STORE}
          text={labels.lbl_Product_pickup_FIND_STORE}
          anchorVariation="custom"
          colorName="gray.900"
          fontSizeVariation="medium"
          centered
          underline
        />
      </UnavailableLink>
    );
  }

  renderSelectSizeToPreviewAvailability = () => {
    const { itemValues, labels } = this.props;
    const { Size } = itemValues || {};
    const sizeNotAvailable = typeof Size === 'undefined' || typeof Size === 'object';
    return sizeNotAvailable ? (
      <RowContainer margins="10px 0 0 0px">
        <Image source={warning} height={14} width={16} dataLocator="pdp_fast_shipping_icon" />
        <BodyCopy
          margin="0 0 0 6px"
          dataLocator="pdp_store_availability_value"
          fontFamily="secondary"
          fontSize="fs14"
          fontWeight="light"
          color="gray.900"
          text={labels.lbl_Product_pickup_SELECT_SIZE_WARNING_TEXT_redesign}
        />
      </RowContainer>
    ) : null;
  };

  renderSoldOutErrorNewReDesign = () => {
    const { keepAlive, labels } = this.props;
    const soldOut = keepAlive || this.getQuantityFromBOPIS() === 0;
    return soldOut ? (
      <RowContainer margins="4px 0 0 0px">
        <PillContainerForInventoryStatus soldOut={soldOut}>
          <BodyCopy
            dataLocator="pdp_store_availability_label"
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="black"
            color="gray.900"
            text={labels.lbl_Product_pickup_OUT_OF_STOCK_TEXT_redesign}
          />
        </PillContainerForInventoryStatus>
        <BodyCopy
          margin="0 0 0 4px"
          dataLocator="pdp_store_availability_value"
          fontFamily="primary"
          fontSize="fs14"
          fontWeight="regular"
          color="gray.900"
          text={labels.lbl_Product_pickup_BOPIS_IN_THE_STORE_redesign}
        />
      </RowContainer>
    ) : null;
  };

  renderSoldOutError = () => {
    const { keepAlive, outOfStockLabels } = this.props;
    return keepAlive ? (
      <BodyCopy
        text={outOfStockLabels.itemSoldOutMessage}
        color="red.500"
        fontFamily="secondary"
        fontSize="fs10"
      />
    ) : null;
  };

  renderCard = (name, iconUrl, iconName, text1, text2, marLeft, keepAlive) => {
    const { currentSelectedPickUpType } = this.state;
    const { noBossBopisInfo } = this.props;
    if (name === 'shipIt' && currentSelectedPickUpType === 'shipIt' && !keepAlive) {
      iconUrl = isGymboree() ? shippingSelectedGYM : shippingSelected;
    } else if (name === 'pickItUp' && currentSelectedPickUpType === 'pickItUp' && !keepAlive) {
      iconUrl = isGymboree() ? storeLocatorSelectedGYM : storeLocatorSelected;
    }
    return (
      <ShippingCard
        name={name}
        currentSelected={currentSelectedPickUpType}
        marginL={marLeft}
        onPress={() => this.handleShippingSelection(name)}
        activeOpacity={1}
        disabled={(noBossBopisInfo && name === 'pickItUp') || keepAlive}
      >
        <IconContainer>
          <Image source={iconUrl} height={30} width={30} />
          <BodyCopy
            dataLocator="pdp_free_shipping_label"
            fontFamily="secondary"
            fontSize="fs14"
            margin="6px 0 10px 8px"
            fontWeight="extrabold"
            color="gray.900"
            text={iconName}
            textAlign="center"
            letterSpacing="ls05"
          />
        </IconContainer>
        <LabelContainer isGymboree={isGymboree()}>
          <BodyCopy
            dataLocator="pdp_free_shipping_label"
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="light"
            color="gray.900"
            text={text1}
            textAlign="center"
            letterSpacing="ls05"
          />
          <BodyCopy
            dataLocator="pdp_free_shipping_label"
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="light"
            color="gray.900"
            text={text2}
            textAlign="center"
          />
        </LabelContainer>
      </ShippingCard>
    );
  };

  render() {
    const {
      showPickupInfo,
      isSubmitting,
      labels,
      simplifiedProductPickupView,
      isAnchor,
      keepAlive,
      productInfo,
      isNewReDesignFullfilmentSection,
      size,
    } = this.props;
    const { currentSelectedPickUpType } = this.state;
    const formattedProducts = formatProductsData(productInfo);
    const margins = isNewReDesignFullfilmentSection ? '40px 0px 0px 0px' : '40px 14px 0px 14px';
    return (
      <>
        {!isAnchor ? (
          <Container
            margins={!simplifiedProductPickupView ? margins : '0'}
            borderWidth={simplifiedProductPickupView || isNewReDesignFullfilmentSection ? 0 : 1}
            isNewReDesignFullfilmentSection={isNewReDesignFullfilmentSection}
          >
            {!simplifiedProductPickupView && !isNewReDesignFullfilmentSection && (
              <>
                <FastShippingContainer>
                  <Image
                    source={shipping}
                    height={22}
                    width={20}
                    dataLocator="pdp_fast_shipping_icon"
                  />
                  <FastShippingTextContainer>
                    <BodyCopy
                      dataLocator="pdp_free_shipping_label"
                      fontFamily="secondary"
                      fontSize="fs16"
                      fontWeight="semibold"
                      color="gray.900"
                      text={labels.lbl_Product_pickup_FREE_SHIPPING}
                    />
                    <BodyCopy
                      dataLocator="pdp_free_shipping_label"
                      fontFamily="secondary"
                      fontSize="fs12"
                      fontWeight="regular"
                      color="gray.900"
                      text={labels.lbl_Product_pickup_NO_MIN_PURCHASE}
                    />
                  </FastShippingTextContainer>
                </FastShippingContainer>
                <LineComp marginTop={16} borderColor="gray.1600" />
                <StoreContainer>
                  <Image
                    source={storeLocator}
                    height={22}
                    width={20}
                    dataLocator="pdp_store_marker_icon"
                  />
                  <ColumnContainer margins="0 0 0 20px">
                    <RowContainer>{this.renderPickupTitle()}</RowContainer>
                    {showPickupInfo && this.renderPickupInfo()}
                    {this.renderSoldOutError()}
                  </ColumnContainer>
                </StoreContainer>
                <ClickTracker
                  as={Button}
                  clickData={{
                    customEvents: ['event131'],
                    products: formattedProducts,
                  }}
                  name={names.screenNames.pickup_store}
                  module="browse"
                  margin={!simplifiedProductPickupView ? '12px 12px 0 12px' : '0'}
                  color="white"
                  fill="BLACK"
                  text={
                    showPickupInfo
                      ? labels.lbl_Product_pickup_PICKUP_IN_STORE
                      : labels.lbl_Product_pickup_FIND_STORE
                  }
                  fontSize="fs12"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  onPress={this.handlePickupModalClick}
                  locator="pdp_pick_up_in_store_btn"
                  disableButton={isSubmitting}
                />
              </>
            )}
            {!simplifiedProductPickupView && isNewReDesignFullfilmentSection && (
              <>
                <ShippingContainer>
                  <BodyCopy
                    dataLocator="pdp_delivery_method_label"
                    fontFamily={this.getGymStyling().fontFamily}
                    fontSize="fs16"
                    fontWeight={this.getGymStyling().fontWeight}
                    color="gray.900"
                    text={labels.lbl_Product_pickup_SHIPPING_METHOD_TITLE}
                    letterSpacing="ls1"
                  />
                  <ShipSelectionTypeContainer>
                    {this.renderCard(
                      'shipIt',
                      shippingNew,
                      labels.lbl_Product_pickup_SHIP_IT_ICON_NAME,
                      labels.lbl_Product_pickup_FREE_SHIPPING_redesign,
                      labels.lbl_Product_pickup_NO_MIN_PURCHASE_redesign,
                      0,
                      keepAlive
                    )}
                    {this.renderCard(
                      'pickItUp',
                      storeLocator,
                      labels.lbl_Product_pickup_PICK_IT_UP_ICON_NAME,
                      labels.lbl_Product_pickup_PICK_IT_UP_FIRST_TEXT,
                      labels.lbl_Product_pickup_PICK_IT_UP_SECOND_TEXT,
                      19,
                      keepAlive
                    )}
                  </ShipSelectionTypeContainer>
                </ShippingContainer>
                {currentSelectedPickUpType !== 'shipIt' && (
                  <>
                    <StoreContainerNewDesign>
                      <StoreTitleAndChangeContainer>
                        {this.renderPickupTitle()}
                      </StoreTitleAndChangeContainer>
                      {showPickupInfo && size ? this.renderPickupInfoReDesign() : null}
                      {size ? this.renderSoldOutErrorNewReDesign() : null}
                      {this.renderSelectSizeToPreviewAvailability()}
                    </StoreContainerNewDesign>
                    <ButtonContainerNewDesign>
                      <ClickTracker
                        as={TouchableOpacity}
                        clickData={{
                          customEvents: ['event131'],
                          products: formattedProducts,
                        }}
                        name={names.screenNames.pickup_store}
                        module="browse"
                        onPress={this.handlePickupModalClick}
                        disabled={isSubmitting}
                      >
                        <TextImageContainer>
                          <BodyCopy
                            text={this.getButtonTextNewDesign()}
                            fontFamily="secondary"
                            fontSize="fs16"
                            fontWeight="extrabold"
                            color="blue.900"
                          />
                          <Image
                            source={rightArrowImage}
                            height={18}
                            width={10}
                            dataLocator="pdp_store_marker_icon"
                          />
                        </TextImageContainer>
                      </ClickTracker>
                    </ButtonContainerNewDesign>
                  </>
                )}
              </>
            )}
          </Container>
        ) : !keepAlive ? (
          this.renderPickUpInStoreAnchorButton()
        ) : null}
      </>
    );
  }
}

export default withStyles(ProductPickupOld, styles);
export { ProductPickupOld as ProductPickupVanilla };
