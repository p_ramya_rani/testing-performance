// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import PRODUCT_PICKUP_ACTIONS_CONSTANTS from '../ProductPickup.constants';

const initialState = {
  bopisInventoryDetails: {},
};

const ProductPickupReducer = (state = fromJS(initialState), action) => {
  switch (action.type) {
    case PRODUCT_PICKUP_ACTIONS_CONSTANTS.SET_BOPIS_INVENTORY:
      return state.set('bopisInventoryDetails', action.payload);
    case PRODUCT_PICKUP_ACTIONS_CONSTANTS.SET_CHANGED_SELECTED_STORE:
      return state.set('changedSelectedStore', action.payload.pickupSelectedStore);
    case PRODUCT_PICKUP_ACTIONS_CONSTANTS.RESET_CHANGED_SELECTED_STORE:
      return state.set('changedSelectedStore', {});
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default ProductPickupReducer;
