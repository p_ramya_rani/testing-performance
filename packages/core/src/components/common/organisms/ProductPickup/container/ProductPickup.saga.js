// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLatest } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { isMobileApp } from '@tcp/core/src/utils';
import PRODUCT_PICKUP_ACTIONS_CONSTANTS from '../ProductPickup.constants';
import { setBopisInventoryDetailsAction } from './ProductPickup.actions';
import getBopisInventoryDetails from '../../../../../services/abstractors/common/bopisInventory/bopisInventory';

export function* getBopisInventory({ payload }) {
  yield put(setBopisInventoryDetailsAction({}));
  try {
    const { itemInfo } = payload;
    const inventoryResponse = yield call(getBopisInventoryDetails, itemInfo);
    yield put(setBopisInventoryDetailsAction({ inventoryResponse }));
  } catch (err) {
    if (isMobileApp()) {
      yield put(setBopisInventoryDetailsAction({ error: err }));
    }
    logger.error({
      error: err,
      extraData: {
        component: 'ProductPickUp Saga - getBopisInventory',
        payloadRecieved: payload,
      },
    });
  }
}

function* ProductPickupStoreSaga() {
  yield takeLatest(PRODUCT_PICKUP_ACTIONS_CONSTANTS.GET_BOPIS_INVENTORY, getBopisInventory);
}

export default ProductPickupStoreSaga;
