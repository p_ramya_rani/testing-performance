// 9fbef606107a605d69c0edbcd8029e5d
import PRODUCT_PICKUP_ACTIONS_CONSTANTS from '../ProductPickup.constants';

export const getBopisInventoryDetailsAction = (payload) => {
  return {
    payload,
    type: PRODUCT_PICKUP_ACTIONS_CONSTANTS.GET_BOPIS_INVENTORY,
  };
};

export const setBopisInventoryDetailsAction = (payload) => {
  return {
    payload,
    type: PRODUCT_PICKUP_ACTIONS_CONSTANTS.SET_BOPIS_INVENTORY,
  };
};

export const setSelectedChangedStoreActiom = (payload) => {
  return {
    payload,
    type: PRODUCT_PICKUP_ACTIONS_CONSTANTS.SET_CHANGED_SELECTED_STORE,
  };
};
export const resetSelectedChangedStoreActiom = (payload) => {
  return {
    payload,
    type: PRODUCT_PICKUP_ACTIONS_CONSTANTS.RESET_CHANGED_SELECTED_STORE,
  };
};
