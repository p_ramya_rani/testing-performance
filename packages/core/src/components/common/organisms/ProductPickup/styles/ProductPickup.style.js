// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .shipping-text-section {
    display: inline-block;
    padding-left: 20px;
  }
  .shipping-text-section.quick-view-drawer-redesign {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding-left: 28px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-left: 40px;
    }
  }
  .fulfillment-section-heading.quick-view-drawer-redesign {
    text-transform: uppercase;
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => props.isNewPDPEnabled && 'display: none;'}
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled &&
        `display: block; font-size: 16px; font-weight: 800;
        margin-left: 7px;margin-top: 30px; padding-bottom: ${props.theme.spacing.ELEM_SPACING.MED};`}
    }
  }
  .shipping-icon {
    width: 26px;
    height: 23px;
  }
  .shipping-icon.quick-view-drawer-redesign {
    width: 31px;
    height: 30px;
    margin: 13px 0px 2px;
  }
  .header-shipit-line1 {
    justify-content: center;
    align-items: center;
    margin: 20px 8px 0px;
  }
  .sub-header-shipit.line2 {
    margin-left: 19px;
  }
  .sub-header-pickup.line2.quick-view-drawer-redesign {
    padding-left: 10px;
    margin-top: 2px;
  }
  .sub-header-pickup.line3.quick-view-drawer-redesign {
    padding-left: 17px;
  }
  .pickup-header {
    border: 1px solid ${(props) => props.theme.colorPalette.gray[1300]};
    padding: ${(props) => (props.isNewPDPEnabled ? '16px 14px 17px 2px' : '16px')};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => (props.isNewPDPEnabled ? '16px 14px 17px 11px;' : '16px')};
    }
  }
  .title-pickup-section {
    display: flex;
  }
  .pickup-section.quick-view-drawer-redesign {
    width: 189px;
    height: 77px;
    margin-left: -22px;
    padding-left: 0;
    border-radius: 6px;
    border: solid 1px ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    cursor: pointer;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 152px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 168px;
    }
  }
  .pickup-content {
    min-height: 112px;
    border: 1px solid ${(props) => props.theme.colorPalette.gray[1300]};
    border-top: none;
    padding: 16px 12px;
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    @media ${(props) => props.theme.mediaQuery.large} {
      min-height: 42px;
      flex-direction: row;
    }
  }
  .pickup-content.quick-view-drawer-redesign {
    border: none;
    min-height: 0px;
  }
  .title-pickup-section-shipping {
    width: 190px;
    height: 87px;
    border-radius: 6px;
    border: solid 1px ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    cursor: pointer;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 151px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 169px;
    }
  }
  .title-pickup-section-shipping,
  .title-pickup-section-location {
    display: flex;
  }
  .title-pickup-section-shipping.selected {
    box-shadow: 0 5px 12px 0 rgba(0, 0, 0, 0.12);
    border: solid 3px ${(props) => props.theme.colors.BRAND.PRIMARY};
    outline: solid 3px transparent;
  }
  .pickup-section.selected {
    box-shadow: 0 5px 12px 0 rgba(0, 0, 0, 0.12);
    border: solid 3px ${(props) => props.theme.colors.BRAND.PRIMARY};
    outline: solid 3px transparent;
  }
  .pickup-header.quick-view-drawer-redesign {
    border: none;
  }
  @media ${(props) => props.theme.mediaQuery.medium} {
    .button-find-in-store {
      flex-basis: 40%;
      align-self: flex-end;
      max-height: 42px;
      min-width: 198px;
      padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
    .richtextCss {
      width: 90px;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .btn-find-in-store-wrapper {
      display: flex;
    }
  }
  .pickup-section {
    display: flex;
    padding-top: 10px;
  }
  .pickup-details {
    padding-left: 18px;
  }
  .pickup-details.quick-view-drawer-redesign {
    padding-left: 21px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding-left: 33px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-left: 41px;
    }
  }
  .pickup-icon-header {
    display: flex;
    flex: 1;
    margin-top: 6px;
  }
  .store-name {
    padding-bottom: 3px;
    ${(props) => props.isNewPDPEnabled && 'padding-bottom: 3px;'}
  }
  .store-name.quick-view-drawer-redesign {
    line-height: 1.38;
    letter-spacing: 0.46px;
    color: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
    margin-left: 10px;
    ${(props) => props.isNewPDPEnabled && 'display: inline-block;'}
  }
  .city-state.quick-view-drawer-redesign {
    text-transform: capitalize;
    display: inline-flex;
    margin-left: 28px;
    top: -4px;
    position: relative;
  }
  .store-city-state-address {
    font-weight: 600;
    line-height: 1.17;
  }
  .store-icon {
    display: none;
    ${(props) => props.isNewPDPEnabled && 'float: right;display: block;'}
    position: absolute;
    right: 0px;
  }
  .availability {
    padding: 0 12px 7px 0;
  }
  .availability-wrapper.quick-view-drawer-redesign {
    margin-left: 18px;
    ${(props) => (props.isNewQVEnabled || props.isNewPDPEnabled) && 'margin-left: 28px;'}
  }
  .availability.quick-view-drawer-redesign {
    letter-spacing: 0.58px;
    color: ${(props) => props.theme.colors.PRIMARY.DARK};
    font-size: 14px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    border-radius: 11px;
    width: auto;
    height: auto;
    padding: 2px 8px;
    background-color: rgba(125, 194, 76, 0.3);
    &.out-of-stock {
      background-color: #f4cfd5;
    }
  }
  .label-availability.quick-view-drawer-redesign {
    font-size: 14px;
    letter-spacing: 0.58px;
    color: ${(props) => props.theme.colors.PRIMARY.DARK};
    margin-left: 3px;
  }
  .change-store-link.quick-view-drawer-redesign {
    float: right;
    margin-right: 12px;
    margin-top: 3px;
    line-height: 1;
    letter-spacing: 1px;
    ${(props) => props.isNewPDPEnabled && 'margin-top: 28px;'}
  }
  .pickup-info {
    padding-bottom: 14px;
  }
  .change-store-link {
    padding-left: 12px;
  }
  .pickup-sub-container {
    margin-bottom: 24px;
  }
  .header-image-wrapper {
    display: flex;
    flex: 1;
    margin-left: 11px;
  }
  .pickup-sub-container.quick-view-drawer-redesign {
    margin-bottom: 0px;
    display: flex;
    justify-content: ${(props) => (props.isNewPDPEnabled ? 'flex-start' : 'space-around')};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      justify-content: ${(props) => props.isNewPDPEnabled && 'space-around'};
    }
  }
  .pickup-icon {
    align-self: flex-start;
    width: 19px;
    height: 25px;
  }
  .pickup-icon.quick-view-drawer-redesign {
    width: 28px;
    height: 28px;
  }
  .header-pickup-line1 {
    margin: 4px 8px;
  }
  .error-pickup-info {
    padding-top: 8px;
  }
  .boss-message {
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    display: inline-flex;
    font-family: 'Nunito';

    .banner-wrapper {
      padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
  .store-section-quick-view-drawer-redesign-container {
    ${(props) => props.isNewQVEnabled && `padding: 0px 16px;`}
    ${(props) => props.isNewPDPEnabled && `padding: 0px;`}
  }
  .store-section-quick-view-drawer-redesign {
    background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
    padding: 10px 12px;
    border-radius: 6px;
    position: relative;
    height: ${(props) => (props.isNewQVEnabled ? '75px' : 'auto')};
    .store-info-container {
      display: flex;
      ${(props) => props.isNewPDPEnabled && `padding-top 10px;`}
    }
    img.location-icon-store-name {
      height: 20px;
    }
    ${(props) =>
      props.isNewPDPEnabled &&
      props.userDefaultStore &&
      `height: 80px;margin: 12px 10px 24px 0px;padding: 0 0 12px 8px;object-fit: contain;border-radius: 6px;background: ${props.theme.colors.PRIMARY.PALEGRAY};`}
    @media ${(props) => props.theme.mediaQuery.largeOnly} {
      ${(props) => props.isNewPDPEnabled && 'margin: 12px 0 24px 0px;max-width: 530px;'}
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) => props.isNewPDPEnabled && 'min-width: 350px; margin: 12px 5px 24px 0px;'}
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => props.isNewPDPEnabled && 'min-width: 320px;margin: 12px 10px 24px 10px;'}
    }
  }
`;
export default styles;
