// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

const getAdditionalStyle = (props) => {
  const { margins, fullWidth } = props;
  return {
    ...(margins && { margin: margins }),
    ...(fullWidth && { width: '100%' }),
  };
};

const getContainerStyle = (props) => {
  const { theme, isNewReDesignFullfilmentSection } = props;
  const { colorPalette, spacing, colors, shadow } = theme;
  if (isNewReDesignFullfilmentSection) {
    return `
      background-color: ${colors.WHITE};
      padding: ${spacing.ELEM_SPACING.MED_1} ${spacing.ELEM_SPACING.MED} ${spacing.ELEM_SPACING.LRG};
      margin-top: ${spacing.ELEM_SPACING.MED_1};
      border-radius: ${spacing.ELEM_SPACING.MED};
      box-shadow: ${shadow.NEW_DESIGN.BOX_SHADOW};
      elevation: 10;
    `;
  }
  return `
    border-color: ${colorPalette.gray[1600]};
    padding-top: ${spacing.ELEM_SPACING.MED};
    padding-bottom: ${spacing.ELEM_SPACING.MED};
  `;
};

const getFastShippingContainerStyle = (props) => {
  const { theme } = props;
  const { spacing } = theme;
  return `
    flex-direction: row;
    align-items: center;
    padding-left: ${spacing.ELEM_SPACING.MED};
    padding-right: ${spacing.ELEM_SPACING.MED};
  `;
};

const FastShippingContainer = styled.View`
  ${getFastShippingContainerStyle}
`;

const ShippingContainer = styled.View`
  flex-direction: column;
`;

const ShipSelectionTypeContainer = styled.View`
  flex-direction: row;
  width: 100%;
  height: auto;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
`;

const ShippingCard = styled.TouchableOpacity`
  ${(props) =>
    props.name === props.currentSelected && !props.disabled
      ? `
    box-shadow: 0 5px 11px ${props.theme.colors.NEWDESIGN_BOX_SHADOW_2};
    border: solid 3px ${
      props.theme.isGymboree
        ? props.theme.colors.DEFAULT_ACCENT_GYM
        : props.theme.colorPalette.blue[500]
    };
    background-color: ${props.theme.colors.WHITE};
    elevation: 10;
  `
      : `
    border: solid 1px ${props.theme.colors.BORDER.GRAY};
  `}
  flex: 1;
  ${(props) => `
    margin-left: ${props.marginL};
    border-radius: ${props.theme.spacing.ELEM_SPACING.MED};
  `}
  ${(props) => props.disabled && `opacity: 0.5;`}
   padding: 5px;
`;

const IconContainer = styled.View`
  ${(props) => `
    flex-direction: row;
    width: 100%;
    justify-content: center;
    padding: ${props.theme.spacing.ELEM_SPACING.SM} ${props.theme.spacing.ELEM_SPACING.LRG_2} 0 ${props.theme.spacing.ELEM_SPACING.LRG_2};
  `}
`;

const NewIconContainer = styled.View`
  ${() => `
    margin-top: 8px;
    justify-content: center;
    align-items: center;
  `}
`;

const LabelContainer = styled.View`
  ${(props) => `
    flex-direction: column;
    width: 100%;
    height: ${props.theme.spacing.ELEM_SPACING.LRG_2};
    justify-content: center;
  `}
  ${(props) => (props.isGymboree ? `padding: 0 3px` : ``)}
`;

const ButtonContainerNewDesign = styled.View`
  ${(props) => `
    margin: 23px 0 0;
    border-radius: ${
      props.theme.isGymboree
        ? props.theme.spacing.ELEM_SPACING.LRG
        : props.theme.spacing.ELEM_SPACING.MED
    };
    box-shadow: 0 5px ${props.theme.spacing.ELEM_SPACING.SM} ${
    props.theme.colors.NEWDESIGN_BOX_SHADOW_3
  };
    flex-direction: column;
    background-color: ${props.theme.colors.WHITE};
    elevation: 10;
  `}
`;

const TextImageContainer = styled.View`
  ${(props) => `
    padding: ${props.theme.spacing.ELEM_SPACING.REG} ${props.theme.spacing.ELEM_SPACING.REG} ${props.theme.spacing.ELEM_SPACING.REG} ${props.theme.spacing.LAYOUT_SPACING.XL};
  `}
  flex-direction: row;
  justify-content: space-between;
`;

const PillContainerForInventoryStatus = styled.View`
  background-color: ${(props) =>
    props.soldOut ? `rgba(200, 16, 46, 0.2)` : `rgba(125, 194, 76, 0.3)`};
  ${(props) => `
    padding: ${props.theme.spacing.ELEM_SPACING.XXS} ${props.theme.spacing.ELEM_SPACING.XS};
  `}
  border-radius: 11px;
`;

const FastShippingTextContainer = styled.View`
  flex-direction: column;
  margin-left: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XS};
`;

const getStoreContainerStyle = (props) => {
  const { theme } = props;
  const { spacing } = theme;
  return `
    flex-direction: row;
    margin-top: ${spacing.ELEM_SPACING.MED};
    padding-left: ${spacing.ELEM_SPACING.MED};
    padding-right: ${spacing.ELEM_SPACING.MED};
  `;
};

const StoreContainer = styled.View`
  ${getStoreContainerStyle}
`;

const StoreContainerNewDesign = styled.View`
  flex-direction: column;
`;

const StoreTitleAndChangeContainer = styled.View`
  flex-direction: row;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  justify-content: space-between;
`;

const RowContainer = styled.View`
  ${getAdditionalStyle};
  flex-direction: row;
  align-items: center;
  justify-content: ${(props) => (props.alignStart ? 'flex-start' : 'center')};
`;

const ColumnContainer = styled.View`
  ${getAdditionalStyle};
  flex-direction: column;
`;

const NewColumnContainer = styled.View`
  ${getAdditionalStyle};
  flex-direction: column;
  padding-left: 6px;
  width: 68%;
`;

const getPromotionESpotLeftArrow = (props) => {
  const { theme } = props;
  const { colorPalette } = theme;
  return `
    width: 0;
    height: 0;
    background: transparent;
    border-style: solid;
    border-top-width: 10;
    border-right-width: 20;
    border-bottom-width: 10;
    border-left-width: 0;
    border-top-color: transparent;
    border-right-color: ${colorPalette.yellow[500]};
    border-bottom-color: transparent;
    border-left-color: transparent;
  `;
};

const getPromotionESpot = (props) => {
  const { theme } = props;
  const { spacing } = theme;
  return `
    flex-direction: row;
    margin-left: ${spacing.ELEM_SPACING.XS};
  `;
};

const PromotionESpotLeftArrow = styled.View`
  ${getPromotionESpotLeftArrow}
`;

const PromotionESpot = styled.View`
  ${getPromotionESpot}
`;

const getPromotionESpotTextContainer = (props) => {
  const { theme } = props;
  const { colorPalette, spacing } = theme;
  return `
    background: ${colorPalette.yellow[500]};
    height: 20px;
    align-items: center;
    padding-right: ${spacing.ELEM_SPACING.XXS};
    justify-content: center;
  `;
};

const PromotionESpotTextContainer = styled.View`
  ${getPromotionESpotTextContainer};
`;

const Container = styled.View`
  ${getAdditionalStyle}
  ${getContainerStyle}
`;

const UnavailableLink = styled.View`
  flex-direction: row;
  border-top-width: 1px;
  border-bottom-width: 1px;
  padding-top: 13px;
  padding-bottom: 13px;
  border-color: ${(props) => props.theme.colorPalette.gray[500]};
`;

const SizeWarningContainer = styled.View`
  ${getAdditionalStyle};
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const PickupLocationContainer = styled.View`
  ${getAdditionalStyle};
  flex-direction: row;
`;

const PickupStoreContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  margin: 8px 0 0 0;
`;

const ContainerOfferNoRush = styled.View`
  background-color: ${(props) => (props.isNoRush ? props.theme.colorPalette.yellow[500] : 'white')};
  border-radius: 9px;
  margin: 7px 0 0;
  padding: ${(props) => (props.isNoRush ? `2px 4px 2px 4px` : 0)};
  align-self: center;
`;

const styles = css``;

export {
  styles,
  Container,
  ContainerOfferNoRush,
  FastShippingContainer,
  FastShippingTextContainer,
  StoreContainer,
  RowContainer,
  ColumnContainer,
  PromotionESpotLeftArrow,
  PromotionESpot,
  PromotionESpotTextContainer,
  UnavailableLink,
  ShippingContainer,
  ShipSelectionTypeContainer,
  ShippingCard,
  IconContainer,
  LabelContainer,
  ButtonContainerNewDesign,
  StoreContainerNewDesign,
  StoreTitleAndChangeContainer,
  PillContainerForInventoryStatus,
  TextImageContainer,
  NewIconContainer,
  SizeWarningContainer,
  PickupStoreContainer,
  PickupLocationContainer,
  NewColumnContainer,
};
