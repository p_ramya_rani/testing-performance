// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ProductPickupVanilla } from '../views/ProductPickupNew.view.native';

describe('ProductPickupVanilla', () => {
  let component;
  const props = {
    miscInfo: {},
    userDefaultStore: {
      basicInfo: {
        id: '',
        storeName: '',
      },
    },
    userGeoCoordinates: {
      lat: null,
      long: null,
    },
    itemValues: {
      color: '',
      Fit: '',
      Size: '',
      Quantity: null,
    },
    onPickUpOpenClick: null,
    bopisItemInventory: [],
    isBopisEligible: false,
    isBossEligible: false,
    isSkuResolved: false,
    showChangeStore: false,
    pickupTitleText: '',
    isBossEligBossInvAvail: false,
    isStoreBopisEligible: false,
    showPickupDetails: false,
    showPickupInfo: false,
    isSubmitting: false,
    isPDPBossBopisEnabled: true,
    labels: {
      lbl_Product_pickup_BOPIS_AVAILABLE: 'Pick up TODAY!',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_HUSKY: 'husky',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_PLUS: 'plus',
      lbl_Product_pickup_BOPIS_DISABLED_FITS_SLIM: 'slim',
      lbl_Product_pickup_BOPIS_ONLY_AVAILABLE: 'Item available for pickup TODAY',
      lbl_Product_pickup_BOSS_AVAILABLE: 'Or choose NO RUSH Pickup ',
      lbl_Product_pickup_BOSS_ONLY_AVAILABLE: 'Choose NO RUSH Pickup ',
      lbl_Product_pickup_FIND_STORE: 'FIND A STORE',
      lbl_Product_pickup_FREE_SHIPPING: 'FREE Shipping Every Day!',
      lbl_Product_pickup_NO_MIN_PURCHASE: 'No Minimum Purchase Required.',
      lbl_Product_pickup_PICKUP_IN_STORE: 'PICK UP IN STORE',
      lbl_Product_pickup_PRODUCT_BOPIS: 'Buy online - Pick up in store',
      lbl_Product_pickup_TITLE_DEFAULT_NOSTORE: 'Select Store',
    },
    productInfo: {
      ratingsProductId: '1082928',
      generalProductId: '1082928_01',
      categoryId: '47511>49007',
      category: '54187',
      name: 'Girls Microfiber Tights',
      colorFitsSizesMap: [
        {
          color: {
            name: 'BLACK',
            imagePath: '/wcsstore/GlobalSAS/images/tcp/products/swatches/1082928_01.jpg',
            family: 'BLACK',
          },
          pdpUrl: '/p/1082928_01',
          colorProductId: '31149',
          colorDisplayId: '1082928_01',
          categoryEntity: 'GIRL:School Uniforms:Accessories & Shoes',
          imageName: '1082928_01',
          favoritedCount: 99,
          maxAvailable: 304356,
          maxAvailableBoss: 303447,
          hasFits: false,
          miscInfo: {
            isBopisEligible: true,
            isBossEligible: true,
            badge1: {},
            keepAlive: false,
            isInDefaultWishlist: false,
          },
          listPrice: 4.5,
          offerPrice: 4.5,
        },
      ],
      isGiftCard: false,
      colorFitSizeDisplayNames: null,
      listPrice: 4.5,
      offerPrice: 4.5,
      highListPrice: 0,
      highOfferPrice: 0,
      lowListPrice: 4.5,
      lowOfferPrice: 0,
      ratings: 0,
      reviewsCount: 0,
      unbxdProdId: '1082928_IV',
      productId: '1082928_IV',
    },
  };

  beforeEach(() => {
    component = shallow(<ProductPickupVanilla {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should return styled view component value eleven', () => {
    expect(component.find('Styled(View)')).toHaveLength(10);
  });

  it('should return styled BodyCopy component value seven', () => {
    expect(component.find('Styled(BodyCopy)')).toHaveLength(4);
  });

  it('should return styled LineComp component value one', () => {
    expect(component.find('Styled(LineComp)')).toHaveLength(1);
  });
});
