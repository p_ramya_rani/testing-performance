// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { FloatingHelpMenuViewVanilla } from '../FloatingHelpMenu.view';

describe('FloatingHelpMenu component', () => {
  const props = {
    className: 'something',
    helpWithOrderLabels: {
      returnPolicyText: 'Return Policy',
      returnPolicyUrl: '/help-center/returns-exchanges#returnsandexchanges',
      orderStatusHelpText: 'Contact Us About Your Order',
      orderStatusHelpUrl: '/content/order-status-help',
    },
    helpMenuLabels: {
      helpCenterUrl: '/help-center/faq',
      helpCenterText: 'Help Center',
      orderStatusUrl: '/help-center/order-help#orderstatus',
      orderStatusText: 'Order Status',
      leaveFeedbackUrl: '/test',
      leaveFeedbackText: 'Leave Feedback',
      needHelpText: 'Need help?',
      needHelpImg:
        'https://test1.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/clock_3x.png',
    },
  };
  it('should render correctly', () => {
    const component = shallow(<FloatingHelpMenuViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call the setstate on floating menu click', () => {
    const setState = jest.fn();
    const useStateSpy = jest.spyOn(React, 'useState');
    useStateSpy.mockImplementation(init => [init, setState]);

    const component = shallow(<FloatingHelpMenuViewVanilla {...props} />);
    component
      .find('.floating-menu-collapsed')
      .props()
      .onClick();
    setTimeout(() => {
      expect(setState).toHaveBeenCalledWith(true);
    });
    expect(component).toMatchSnapshot();
  });
});
