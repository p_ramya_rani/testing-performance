// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { getIconPath, configureInternalNavigationFromCMSUrl, isCanada } from '@tcp/core/src/utils';
import { Image, BodyCopy, Anchor, DamImage } from '@tcp/core/src/components/common/atoms';
import styles from '../styles/FloatingHelpMenu.style';
import withStyles from '../../../hoc/withStyles';

const MenuCtas = (ctaProps) => {
  const { to, title, target, noLink, handleLinkClick, asPath } = ctaProps;

  const handleClick = handleLinkClick
    ? (event) => {
        event.preventDefault();
        handleLinkClick();
      }
    : null;

  return (
    <div className="menu-link">
      <Anchor
        to={to}
        title={title}
        asPath={asPath}
        noLink={noLink}
        target={target}
        handleLinkClick={handleClick}
      >
        <BodyCopy fontSize="fs14" fontFamily="secondary" fontWeight="semibold" textAlign="left">
          {title}
        </BodyCopy>
      </Anchor>
    </div>
  );
};

const FloatingHelpMenuView = ({ helpWithOrderLabels, helpMenuLabels, className }) => {
  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };
  const { returnPolicyUrl, returnPolicyText, orderStatusHelpText, orderStatusHelpUrl } =
    helpWithOrderLabels || {};
  const {
    helpCenterUrl,
    helpCenterText,
    orderStatusUrl,
    orderStatusText,
    leaveFeedbackUrl,
    leaveFeedbackText,
    needHelpText,
    needHelpImg: imgUrl,
  } = helpMenuLabels || {};
  return (
    <div className={className}>
      <nav className={`floating-menu ${menuOpen ? 'menu-show' : 'menu-hide'}`}>
        <Image
          alt="close-help-menu"
          role="button"
          tabIndex="0"
          className="close-icon-image"
          src={getIconPath('close-icon')}
          onClick={toggleMenu}
          onKeyDown={toggleMenu}
        />

        <MenuCtas
          to={configureInternalNavigationFromCMSUrl(helpCenterUrl)}
          asPath={helpCenterUrl}
          title={helpCenterText}
        />

        <MenuCtas
          to={configureInternalNavigationFromCMSUrl(returnPolicyUrl)}
          asPath={returnPolicyUrl}
          title={returnPolicyText}
        />

        <MenuCtas
          to={configureInternalNavigationFromCMSUrl(orderStatusUrl)}
          asPath={orderStatusUrl}
          title={orderStatusText}
        />

        {!isCanada() ? (
          <MenuCtas
            to={configureInternalNavigationFromCMSUrl(orderStatusHelpUrl)}
            asPath={orderStatusHelpUrl}
            title={orderStatusHelpText}
          />
        ) : null}

        <MenuCtas
          title={leaveFeedbackText}
          handleLinkClick={() => {
            const width = 720;
            const height = 625;
            const y = window.outerHeight / 2 + window.screenY - width / 2;
            const x = window.outerWidth / 2 + window.screenX - height / 2;
            return window.open(
              leaveFeedbackUrl,
              'Leave Feedback',
              `width=${width},height=${height},top=${y},left=${x}`
            );
          }}
          noLink
        />
      </nav>
      <BodyCopy
        onClick={toggleMenu}
        component="div"
        className={`floating-menu-collapsed ${menuOpen ? 'menu-hide' : 'menu-show'}`}
      >
        <DamImage
          aria-hidden="true"
          className="need-help-img"
          imgConfigs={['w_20', 'w_20', 'w_20']}
          width="100%"
          imgData={{
            url: imgUrl,
            alt: { needHelpText },
          }}
        />
        <BodyCopy
          component="span"
          fontSize="fs14"
          fontFamily="secondary"
          fontWeight="semibold"
          className="need-help-text"
        >
          {needHelpText}
        </BodyCopy>
      </BodyCopy>
    </div>
  );
};

FloatingHelpMenuView.propTypes = {
  helpWithOrderLabels: PropTypes.shape({}),
  className: PropTypes.string,
  helpMenuLabels: PropTypes.shape({}),
};

FloatingHelpMenuView.defaultProps = {
  helpWithOrderLabels: {},
  className: '',
  helpMenuLabels: {},
};

export default withStyles(FloatingHelpMenuView, styles);
export { FloatingHelpMenuView as FloatingHelpMenuViewVanilla };
