// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .floating-menu {
    padding: 20px;
    width: 202px;
    z-index: 500;
    position: fixed;
    top: 50vh;
    right: 0px;
    transform: translate(0, -50%);
    border-radius: 10px 0 0 10px;
    box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.2);
    background-color: ${props => props.theme.colors.WHITE};
  }

  .floating-menu-collapsed {
    padding: 10px;
    width: 18px;
    z-index: 100;
    position: fixed;
    top: 50vh;
    right: 0px;
    height: 100px;
    transform: translate(0, -50%);
    border-radius: 10px 0 0 10px;
    box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.2);
    background-color: ${props => props.theme.colors.WHITE};
    cursor: pointer;
  }

  .need-help-text {
    writing-mode: tb-rl;
    padding-top: 25px;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.29;
    color: ${props => props.theme.colorPalette.blue.C900};
  }

  .need-help-img {
    width: 20px;
    height: 20px;
    position: absolute;
  }

  .floating-menu p {
    display: block;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.29;
    letter-spacing: 0.7px;
    color: ${props => props.theme.colorPalette.blue.C900};
    text-decoration: none;
  }

  .floating-menu div:not(:last-child) p {
    padding-bottom: 14px;
  }

  .menu-link {
    width: fit-content;
  }

  .close-icon-image {
    position: absolute;
    cursor: pointer;
    right: 18px;
    top: 18px;
    height: 15px;
    width: 15px;
  }

  .menu-show {
    animation: slide-in 0.5s forwards;
  }

  .menu-hide {
    animation: slide-out 0.5s forwards;
  }

  @keyframes slide-in {
    0% {
      transform: translateX(100%);
    }
    100% {
      transform: translateX(0%);
    }
  }

  @keyframes slide-out {
    0% {
      transform: translateX(0%);
    }
    100% {
      transform: translateX(100%);
    }
  }
`;
export default styles;
