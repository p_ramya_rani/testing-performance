// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';

const getLabels = state => {
  return state && state.Labels;
};

export const getHelpMenuLabels = createSelector(
  getLabels,
  labels => {
    return {
      helpCenterUrl: getLabelValue(labels, 'lbl_help_center_url', 'orderConfirmation', 'checkout'),
      helpCenterText: getLabelValue(
        labels,
        'lbl_help_center_text',
        'orderConfirmation',
        'checkout'
      ),
      orderStatusUrl: getLabelValue(
        labels,
        'lbl_order_status_url',
        'orderConfirmation',
        'checkout'
      ),
      orderStatusText: getLabelValue(
        labels,
        'lbl_order_status_text',
        'orderConfirmation',
        'checkout'
      ),
      leaveFeedbackUrl: getLabelValue(
        labels,
        'lbl_leave_feedback_url',
        'orderConfirmation',
        'checkout'
      ),
      leaveFeedbackText: getLabelValue(
        labels,
        'lbl_leave_feedback_text',
        'orderConfirmation',
        'checkout'
      ),
      needHelpText: getLabelValue(labels, 'lbl_need_help_text', 'orderConfirmation', 'checkout'),
      needHelpImg: getLabelValue(labels, 'lbl_need_help_img', 'orderConfirmation', 'checkout'),
    };
  }
);

export default { getHelpMenuLabels };
