// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { getHelpWithOrdersLabels } from '../../HelpWithOrder/container/HelpWithOrder.selectors';
import { getHelpMenuLabels } from './FloatingHelpMenu.selectors';
import FloatingHelpMenuView from '../views/FloatingHelpMenu.view';

const mapStateToProps = (state, props) => {
  return {
    helpWithOrderLabels: getHelpWithOrdersLabels(state),
    helpMenuLabels: getHelpMenuLabels(state),
    ...props,
  };
};

export default connect(mapStateToProps)(FloatingHelpMenuView);
