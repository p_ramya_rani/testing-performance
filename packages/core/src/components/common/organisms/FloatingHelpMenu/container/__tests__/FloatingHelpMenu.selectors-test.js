// 9fbef606107a605d69c0edbcd8029e5d 
import { getHelpMenuLabels } from '../FloatingHelpMenu.selectors';

describe('HelpWithOrder Selectors', () => {
  it('getHelpMenuLabels should return Labels', () => {
    const state = {
      Labels: {
        checkout: {
          orderConfirmation: {
            lbl_help_center_url: '/help-center/faq',
            lbl_help_center_text: 'Help Center',
            lbl_order_status_url: '/help-center/order-help#orderstatus',
            lbl_order_status_text: 'Order Status',
            lbl_leave_feedback_url:
              'https://collection-preprod.iperceptions.com/?vm=1&pID=1&rn=128721&lID=1&hs1=102214&hs2=91787&hc=425312&res=1440x900&referrer=https%3A%2F%2Fiperworld.iperceptions.com%2FHome%2FTestPage%3FsurveyId%3D128721',
            lbl_leave_feedback_text: 'Leave Feedback',
            lbl_need_help_text: 'Need help?',
            lbl_need_help_img:
              'https://assets.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/icon-feedback_3x.png',
          },
        },
      },
    };
    expect(getHelpMenuLabels(state)).toMatchObject({
      helpCenterUrl: '/help-center/faq',
      helpCenterText: 'Help Center',
      orderStatusUrl: '/help-center/order-help#orderstatus',
      orderStatusText: 'Order Status',
      leaveFeedbackUrl:
        'https://collection-preprod.iperceptions.com/?vm=1&pID=1&rn=128721&lID=1&hs1=102214&hs2=91787&hc=425312&res=1440x900&referrer=https%3A%2F%2Fiperworld.iperceptions.com%2FHome%2FTestPage%3FsurveyId%3D128721',
      leaveFeedbackText: 'Leave Feedback',
      needHelpText: 'Need help?',
      needHelpImg:
        'https://assets.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/icon-feedback_3x.png',
    });
  });
});
