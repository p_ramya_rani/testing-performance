import React from 'react';
import { shallow } from 'enzyme';
import { EmailSmsSignUpFormVanilla } from '../views/EmailSmsSignUpForm.view';

describe('SingleSignup Form', () => {
  it('should renders correctly when subscription is success', () => {
    const props = {
      subscription: { success: true },
      className: 'classname',
      singleSignUpLabels: {},
      handleSubmit: () => {},
    };
    const component = shallow(<EmailSmsSignUpFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when subscription has error', () => {
    const props = {
      subscription: { error: 'invalid' },
      className: 'classname',
      singleSignUpLabels: {},
      handleSubmit: () => {},
    };
    const component = shallow(<EmailSmsSignUpFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('calling componentDidUpdate method', () => {
    const props = {
      subscription: {
        success: true,
      },

      singleSignUpLabels: {},

      validateSignupEmail: jest.fn(),
    };

    const component = shallow(<EmailSmsSignUpFormVanilla {...props} />);

    component.instance().componentDidUpdate(props);

    expect(component).toMatchSnapshot();
  });
  it('should call submit', () => {
    const props = {
      subscription: {
        success: true,
      },

      singleSignUpLabels: {},
      validateSignupEmail: jest.fn(),
    };

    const args = {
      emailSignupSecondBrand: '',

      signupPhoneNumber: '',
      signupEmailAddress: '',
    };

    const component = shallow(<EmailSmsSignUpFormVanilla {...props} />);

    component.instance().submitForm(args);
  });
  it('should call getImageData', () => {
    const props = {
      subscription: {
        success: true,
      },

      singleSignUpLabels: {},

      validateSignupEmail: jest.fn(),
    };

    const component = shallow(<EmailSmsSignUpFormVanilla {...props} />);

    component.instance().getImageData();
  });
});
