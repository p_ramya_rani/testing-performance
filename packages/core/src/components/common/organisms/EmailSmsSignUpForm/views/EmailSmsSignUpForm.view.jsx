// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';
import {
  Button,
  Col,
  Row,
  TextBox,
  DamImage,
  Anchor,
  RichText,
} from '@tcp/core/src/components/common/atoms';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import { Grid } from '@tcp/core/src/components/common/molecules';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { formatPhoneNumber } from '@tcp/core/src/utils/formValidation/phoneNumber';
import createValidateMethod from '@tcp/core/src/utils/formValidation/createValidateMethod';
import getStandardConfig from '@tcp/core/src/utils/formValidation/validatorStandardConfig';
import { isGymboree, getIconPath } from '@tcp/core/src/utils/utils';
import Image from '@tcp/core/src/components/common/atoms/Image';
import CombinedConfirm from '../../../molecules/CombinedConfirm/views/CombinedConfirm.view';
import CombinedFormIntro from '../../../molecules/CombinedFormIntro/views/CombinedFormIntro.view';

import styles from '../styles/EmailSmsSignupForm.style';
import config from '../../EmailSignupForm/config';

class EmailSmsSignUpForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isEmailFieldEmpty: true,
      isSmsFieldEmpty: true,
    };
    this.isGym = isGymboree();
  }

  componentDidUpdate({ subscription: oldSubscription }) {
    const { subscription } = this.props;
    if (subscription.success !== oldSubscription.success && subscription.success) {
      if (this.formSubmitPromise) {
        this.formSubmitPromise.resolve();
      }
    } else if (subscription.error && this.formSubmitPromise) {
      this.formSubmitPromise.reject();
    }
  }

  submitForm = (formObject) => {
    const {
      submitEmailSmsSubscription,
      validateSignupEmail,
      singleSignUpLabels: { emailVaidationError },
    } = this.props;
    const { signupEmailAddress, signupPhoneNumber, emailSignupSecondBrand } = formObject;

    if (!signupEmailAddress) {
      return new Promise((resolve, reject) => {
        this.formSubmitPromise = { resolve, reject };
        submitEmailSmsSubscription({
          signupEmailAddress,
          signupPhoneNumber,
          emailSignupSecondBrand,
        });
      });
    }

    return validateSignupEmail(signupEmailAddress)
      .then(({ error, status }) => {
        if (error) {
          return Promise.reject();
        }
        return new Promise((resolve, reject) => {
          this.formSubmitPromise = { resolve, reject };
          submitEmailSmsSubscription({
            signupEmailAddress,
            signupPhoneNumber,
            emailSignupSecondBrand,
            status,
          });
        });
      })
      .catch(() => {
        const error = { signupEmailAddress: emailVaidationError };
        throw new SubmissionError({ ...error, _error: error });
      });
  };

  emailFieldChange = (element) => {
    const isEmailFieldEmpty = !element.currentTarget.value.trim();
    this.setState({ isEmailFieldEmpty });
  };

  smsFieldChange = (element) => {
    const isSmsFieldEmpty = !element.currentTarget.value.trim();
    this.setState({ isSmsFieldEmpty });
  };

  getImageData = (isSuccess) => {
    const { singleSignUpLabels } = this.props;

    return {
      url: isSuccess ? singleSignUpLabels.successImageUrl : singleSignUpLabels.signUpImageUrl,
      alt: 'Combined signup image',
    };
  };

  render() {
    const {
      singleSignUpLabels,
      subscription,
      pristine,
      handleSubmit,
      submitting,
      className,
      closeModal,
    } = this.props;
    const { left, right } = config.modalColProps;
    const { IMG_DATA_MODAL } = config;
    const { isEmailFieldEmpty, isSmsFieldEmpty } = this.state;

    return (
      <div className={className}>
        {subscription.success ? (
          <Grid>
            <Row fullBleed className="combined-success-wrapper">
              <Col
                isNotInlineBlock
                colSize={left}
                hideCol={{ small: true, medium: true }}
                className="img-wrapper"
              >
                <DamImage imgConfigs={IMG_DATA_MODAL} imgData={this.getImageData(true)} />
              </Col>
              <Col
                colSize={right}
                ignoreGutter={{ large: true }}
                className="emailform__wrapper form-wrapper"
                aria-live="polite"
                aria-atomic="true"
              >
                <CombinedConfirm singleSignUpLabels={singleSignUpLabels} />
                <Row className="success-button-wrapper" fullBleed>
                  <Col colSize={{ small: 4, medium: 4, large: 4 }} className="button-container">
                    <Anchor
                      to={singleSignUpLabels.shopNowButtonUrl}
                      asPath={singleSignUpLabels.shopNowButtonUrl}
                      target="_self"
                      onClick={closeModal}
                    >
                      <Button
                        fullWidth
                        buttonVariation="fixed-width"
                        fill="BLUE"
                        type="submit"
                        className="shop-button"
                        dataLocator="shop_now_btn"
                      >
                        {singleSignUpLabels.shopNow}
                      </Button>
                    </Anchor>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        ) : (
          <form onSubmit={handleSubmit(this.submitForm)}>
            <Grid>
              <Row
                fullBleed={{ large: true }}
                className="wrapper form_container combined-signup-wrapper"
              >
                <Col
                  isNotInlineBlock
                  colSize={left}
                  hideCol={{ small: true, medium: true }}
                  className="img-wrapper"
                >
                  <DamImage imgConfigs={IMG_DATA_MODAL} imgData={this.getImageData()} />
                </Col>
                <Col colSize={right} className="emailform__wrapper form-wrapper">
                  <div className="emailform__content">
                    <Row fullBleed>
                      <Col
                        colSize={{ small: 6, medium: 8, large: 10 }}
                        offsetLeft={{ small: 0, medium: 0, large: 1 }}
                        className="logo-wrapper"
                      >
                        <Image
                          className="logo"
                          aria-hidden="true"
                          src={
                            isGymboree()
                              ? getIconPath('gymboree-icon')
                              : getIconPath('header__brand-tab--tcp')
                          }
                        />
                      </Col>
                    </Row>
                    <CombinedFormIntro singleSignUpLabels={singleSignUpLabels} />
                    <Col
                      colSize={{ small: 6, medium: 6, large: 12 }}
                      offsetLeft={{ small: 0, medium: 1, large: 0 }}
                      className="field-container"
                    >
                      <Field
                        placeholder={singleSignUpLabels.emailPlaceHolder}
                        name="signupEmailAddress"
                        id="signupEmailAddress"
                        type="text"
                        component={TextBox}
                        maxLength={50}
                        dataLocator="email_address_field"
                        enableSuccessCheck={false}
                        onChange={this.emailFieldChange}
                      />

                      <Field
                        placeholder={singleSignUpLabels.smsPlaceHolder}
                        name="signupPhoneNumber"
                        id="signupPhoneNumber"
                        type="text"
                        component={TextBox}
                        maxLength={50}
                        dataLocator="sms_address_field"
                        normalize={formatPhoneNumber}
                        enableSuccessCheck={false}
                        onChange={this.smsFieldChange}
                      />
                      <Field
                        name="emailSignupSecondBrand"
                        id="emailSignupSecondBrand"
                        component={InputCheckbox}
                        dataLocator="email_gym_opt"
                        type="checkbox"
                        className="combined-signup-second-brand"
                      >
                        {singleSignUpLabels.crossBrandEmail}
                      </Field>
                      <div className="termscontainer">
                        <RichText richTextHtml={singleSignUpLabels.legalText} />
                      </div>
                    </Col>
                    <Row className="combined-button-wrapper-form" fullBleed>
                      <Col
                        colSize={{ small: 4, medium: 4, large: 6 }}
                        className="combined-joinnow-btn"
                      >
                        <Button
                          fullWidth
                          disabled={
                            (isEmailFieldEmpty && isSmsFieldEmpty) || pristine || submitting
                          }
                          buttonVariation="fixed-width"
                          fill="BLUE"
                          type="submit"
                          className="combined-join-button"
                          dataLocator="join_now_btn"
                        >
                          {singleSignUpLabels.signUp}
                        </Button>
                      </Col>
                      <Row fullBleed className="combined-no-thanks-row">
                        <Col
                          className="no-thanks-wrapper"
                          colSize={{ small: 4, medium: 4, large: 6 }}
                        >
                          <Anchor
                            handleLinkClick={(e) => {
                              e.preventDefault();
                              closeModal();
                            }}
                            className="no-thanks-email"
                            noLink
                          >
                            {singleSignUpLabels.noThanks}
                          </Anchor>
                        </Col>
                      </Row>
                    </Row>
                  </div>
                </Col>
              </Row>
            </Grid>
          </form>
        )}
      </div>
    );
  }
}

EmailSmsSignUpForm.propTypes = {
  buttonConfig: PropTypes.shape({}),
  submitEmailSmsSubscription: PropTypes.func,
  className: PropTypes.string,
  singleSignUpLabels: PropTypes.shape({}).isRequired,
  confirmationViewConfig: PropTypes.shape({}).isRequired,
  clearEmailSignupForm: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.func,
  validateSignupEmail: PropTypes.func,
  subscription: PropTypes.shape({}),
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  colProps: PropTypes.shape({}),
  imageData: PropTypes.shape({}),
  closeModal: PropTypes.func,
  defaultStore: PropTypes.shape({}),
  emailDisclaimer: PropTypes.shape({}),
};

EmailSmsSignUpForm.defaultProps = {
  buttonConfig: {},
  submitEmailSmsSubscription: () => {},
  handleSubmit: () => {},
  validateSignupEmail: () => Promise.resolve({}),
  className: '',
  subscription: {},
  pristine: false,
  submitting: false,
  colProps: {},
  imageData: {},
  closeModal: () => {},
  defaultStore: {},
  emailDisclaimer: {},
};

const validateMethod = createValidateMethod(
  getStandardConfig(['signupEmailAddress', 'signupPhoneNumber'])
);

export default withStyles(
  reduxForm({
    form: 'EmailSignupForm',
    initialValues: {
      signupEmailAddress: '',
      signupPhoneNumber: '',
    },
    ...validateMethod,
  })(EmailSmsSignUpForm),
  styles
);

export { EmailSmsSignUpForm as EmailSmsSignUpFormVanilla };
