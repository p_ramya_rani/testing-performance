import { css } from 'styled-components';

export default css`
  .combined-signup-wrapper,
  .combined-success-wrapper {
    .img-wrapper {
      display: none;
    }
    .logo-wrapper {
      display: none;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      .emailform__content {
        max-width: 381px;
        margin: 0px auto;
      }
      .logo-wrapper {
        display: block;
        padding: 24px 0px 0px;
        text-align: center;
      }
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      .img-wrapper {
        display: block;
        width: 300px;
        margin-right: 0px;
        height: 645px;
        overflow: hidden;
      }
      .emailform__wrapper {
        width: calc(100% - 300px);
        position: absolute;
        top: 0px;
        bottom: 0px;
        overflow-y: auto;
        right: 0px;
      }
    }
    .combined-signup-second-brand {
      margin: 20px 0 17px 0;
    }
    .combined-button-wrapper-form {
      justify-content: center;
      padding-top: 13px;
      @media ${(props) => props.theme.mediaQuery.smallMax} {
        background-color: ${(props) => props.theme.colorPalette.gray[300]};
        margin: 5px -14px 0 -14px;
        width: calc(100% + 28px);
      }
    }
    .combined-joinnow-btn,
    .combined-no-thanks-row {
      width: 222px;
      justify-content: center;
      margin: 0;
    }
    .combined-joinnow-btn {
      font-size: 14px;
    }
    .combined-no-thanks-row {
      font-size: 12px;
      text-align: center;
      padding: 8px;
    }
    .no-thanks-email {
      font-size: ${(props) => props.theme.fonts.fontSize.anchor.medium}px;
      text-decoration: underline;
      font-family: ${(props) => props.theme.typography.fonts.secondary};
    }
  }

  .success-button-wrapper {
    justify-content: center;
    width: 100%;
    margin-top: 20px;
    margin-bottom: 22px;
  }

  .img-wrapper img {
    height: 100%;
  }
`;
