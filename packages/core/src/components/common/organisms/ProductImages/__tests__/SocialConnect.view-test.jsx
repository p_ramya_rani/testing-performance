// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { SocialConnectVanilla } from '../views/SocialConnect.view';

describe('SocialConnect component', () => {
  it('should renders correctly', () => {
    const props = {
      ratingsProductId: '',
    };
    const component = shallow(<SocialConnectVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when isNewPDPEnabled and newShareIcon are true', () => {
    const props = {
      ratingsProductId: '',
      isNewPDPEnabled: true,
      newShareIcon: true,
    };
    const component = shallow(<SocialConnectVanilla {...props} />);
    expect(component.type()).toEqual('ul');
  });

  it('should renders correctly when isFacebookEnabled is true', () => {
    const props = {
      ratingsProductId: '',
      isNewPDPEnabled: false,
      isFacebookEnabled: true,
    };
    const handleFacebookShare = jest.fn();
    window.open = jest.fn();
    const component = shallow(<SocialConnectVanilla {...props} />);
    component.find('[className="icon-facebook"]').simulate('click');
    handleFacebookShare.mockImplementation();
    expect(window.open).toBeCalled();
  });

  it('should renders correctly when isTwitterEnabled is true', () => {
    const props = {
      ratingsProductId: '',
      isNewPDPEnabled: false,
      isTwitterEnabled: true,
    };
    const handleTwitterShare = jest.fn();
    window.open = jest.fn();
    const component = shallow(<SocialConnectVanilla {...props} />);
    component.find('[className="icon-twitter"]').simulate('click');
    handleTwitterShare.mockImplementation();
    expect(window.open).toBeCalled();
  });

  it('should renders correctly when isTwitterEnabled is true', () => {
    const props = {
      ratingsProductId: '',
      isNewPDPEnabled: false,
      isPinterestEnabled: true,
    };
    const handlePinterestShare = jest.fn();
    window.open = jest.fn();
    const component = shallow(<SocialConnectVanilla {...props} />);
    component.find('[className="icon-pinterest"]').simulate('click');
    handlePinterestShare.mockImplementation();
    expect(window.open).toBeCalled();
  });

  it('should renders correctly when isNewPDPEnabled is true', () => {
    const props = {
      ratingsProductId: '',
      isNewPDPEnabled: true,
      newShareIcon: false,
    };
    const component = shallow(<SocialConnectVanilla {...props} />);
    expect(component.type()).toEqual(null);
  });
});
