// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';

import { ProductImagesVanilla } from '../views/ProductImages.view';
import { isUsOnly } from '../../../../../utils';

jest.mock('../../../../../utils', () => ({
  getViewportInfo: () => ({
    isMobile: true,
  }),
  isClient: () => true,
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isUsOnly: jest.fn(),
  parseBoolean: () => true,
  isCanada: () => false,
  getIconPath: jest.fn(),
  getLocator: jest.fn(),
  getVideoUrl: jest.fn(),
  getBrand: () => 'tcp',
  isMobileApp: jest.fn(),
}));

describe('ProductImages component', () => {
  const props = {
    isZoomEnabled: true,
    pdpLabels: {},
    images: [
      {
        isOnModalImage: false,
        iconSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/125/2082931_IV.jpg',
        listingSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/380/2082931_IV.jpg',
        regularSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2082931_IV.jpg',
        bigSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2082931_IV.jpg',
        superSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2082931_IV.jpg',
      },
      {
        isOnModalImage: false,
        iconSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/125/2082931_IV-1.jpg',
        listingSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/380/2082931_IV-1.jpg',
        regularSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/2082931_IV-1.jpg',
        bigSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2082931_IV-1.jpg',
        superSizeImageUrl:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/2082931_IV-1.jpg',
      },
    ],
    isThumbnailListVisible: true,
    productName: 'Girls Uniform Active Shorts',
    productMiscInfo: { favoritedCount: 4 },
    productInfo: { productId: 1281542 },
  };
  it('should renders correctly', () => {
    const component = shallow(<ProductImagesVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render rounded ui of complete the look in mobile for US', () => {
    isUsOnly.mockImplementation(() => true);
    props.isCompleteTheLookTestEnabled = true;
    props.itemPartNumber = '2084544_627';
    props.completeLookSlotTile = { '2084544_627': [{ largeImageUrl: 'dummy url' }] };
    props.isMobile = true;
    const component = shallow(<ProductImagesVanilla {...props} />);
    // eslint-disable-next-line sonarjs/no-duplicate-string
    expect(component.find('.complete-look-wrapper')).toHaveLength(0);
    // eslint-disable-next-line sonarjs/no-duplicate-string
    expect(component.find('.complete-look-round')).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });

  it('should render box ui of complete the look in mobile for US', () => {
    isUsOnly.mockImplementation(() => true);
    props.isCompleteTheLookTestEnabled = false;
    props.itemPartNumber = '2084544_627';
    props.completeLookSlotTile = { '2084544_627': [{ largeImageUrl: 'dummy url' }] };
    props.isMobile = true;
    const component = shallow(<ProductImagesVanilla {...props} />);
    expect(component.find('.complete-look-round')).toHaveLength(0);
    expect(component.find('.complete-look-wrapper')).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });

  it('should render box ui of complete the look in mobile for Not US', () => {
    isUsOnly.mockImplementation(() => false);
    props.isCompleteTheLookTestEnabled = false;
    props.itemPartNumber = '2084544_627';
    props.completeLookSlotTile = { '2084544_627': [{ largeImageUrl: 'dummy url' }] };
    props.isMobile = true;
    const component = shallow(<ProductImagesVanilla {...props} />);
    expect(component.find('.complete-look-round')).toHaveLength(0);
    expect(component.find('.complete-look-wrapper')).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });
  it('should not render hovered image ', () => {
    props.changePDPPrimaryImageOnHover = false;
    const component = shallow(<ProductImagesVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should  render hovered image ', () => {
    props.changePDPPrimaryImageOnHover = true;
    const component = shallow(<ProductImagesVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render image badge correctly when kill switch is enabled', () => {
    const props1 = {
      ...props,
      TCPStyleQTY: 10,
      TCPStyleType: '0002',
      isDynamicBadgeEnabled: true,
    };
    const component = shallow(<ProductImagesVanilla {...props1} />);
    expect(component).toMatchSnapshot();
  });
  it('should render image badge correctly when kill switch is disabled', () => {
    const props2 = {
      ...props,
      TCPStyleQTY: 0,
      TCPStyleType: '',
      isDynamicBadgeEnabled: false,
    };
    const component = shallow(<ProductImagesVanilla {...props2} />);
    expect(component).toMatchSnapshot();
  });
});
