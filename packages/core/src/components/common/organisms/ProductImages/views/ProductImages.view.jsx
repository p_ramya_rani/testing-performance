/* eslint-disable max-lines */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import SocialProofMessage from '@tcp/core/src/components/features/browse/ProductDetail/molecules/SocialProofMessage/container/SocialProofMessage.container';
import { Anchor, BodyCopy, Image, Button } from '../../../atoms';
import withStyles from '../../../hoc/withStyles';
import config, { defaultProps, propTypes } from '../config';
import ThumbnailsList from '../../../molecules/ThumbnailsList';
import FullSizeImageModal from '../../../../features/browse/ProductDetail/molecules/FullSizeImageModal/views/FullSizeImageModal.view';
import Carousel from '../../../molecules/Carousel';
import styles, { carousalStyle, carousalStyleNewQV } from '../styles/ProductImages.style';
import ProductDetailImage from '../../../molecules/ProductDetailImage';
import Share from '../../../molecules/Share';
import CompleteTheLook from '../../../molecules/CompleteTheLook';
import {
  getIconPath,
  getLocator,
  getVideoUrl,
  isUsOnly,
  isClient,
  getViewportInfo,
} from '../../../../../utils';
import SocialConnect from './SocialConnect.view';
import OutOfStockWaterMarkView from '../../../../features/browse/ProductDetail/molecules/OutOfStockWaterMark';
import { WishListIcon } from '../../../../features/browse/ProductListing/molecules/ProductList/views/ProductItemComponents';

// function to return Thumbnails list to show on PDP and full size page
const ThumbnailList = ({
  isThumbnailListVisible,
  thumbnailImagesPaths,
  currentImageIndex,
  onThumbnailClick,
  primaryBrand,
  alternateBrand,
  fromSkeleton,
}) => {
  return (
    isThumbnailListVisible &&
    !fromSkeleton && (
      <div className="preview-and-social-media-icons">
        <ThumbnailsList
          images={thumbnailImagesPaths}
          selectedImageIndex={currentImageIndex}
          onThumbnailClick={onThumbnailClick}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
        />
      </div>
    )
  );
};

const SocialConnectTemplate = ({
  isFullSizeVisible,
  isMobile,
  isCompleteTheLookTestEnabled,
  accessibilityLabels,
  ratingsProductId,
  isNewPDPEnabled,
}) => {
  return isFullSizeVisible
    ? (!isUsOnly() || (isUsOnly() && isMobile && !isCompleteTheLookTestEnabled)) && (
        // eslint-disable-next-line react/jsx-indent
        <SocialConnect
          isFacebookEnabled
          isPinterestEnabled
          isTwitterEnabled
          accessibilityLabels={accessibilityLabels}
          ratingsProductId={ratingsProductId}
          isNewPDPEnabled={isNewPDPEnabled}
        />
      )
    : null;
};

const getImageContainerClass = (fromSkeleton) => {
  return fromSkeleton ? 'carousel-container skeleton-img' : 'carousel-container';
};

class ProductImages extends React.Component {
  static propTypes = propTypes;

  constructor(props) {
    super(props);

    this.handleThumbnailClick = this.handleThumbnailClick.bind(this);
    this.scrollRef = React.createRef();
    this.state = {
      currentImageIndex: 0,
      clickedProdutId: '',
    };
  }

  static getDerivedStateFromProps(props, state) {
    const {
      isLoggedIn,
      onAddItemToFavorites,
      productInfo,
      productMiscInfo,
      pageName,
      skuId,
      formName,
    } = props;
    const productId = productInfo?.productId;
    const colorProductId = productMiscInfo?.colorProductId;
    const colorDisplayId = productMiscInfo?.colorDisplayId;
    const selectedColorId = colorDisplayId || productId;

    const { clickedProdutId, isLoggedIn: wasLoggedIn } = state;
    if (isLoggedIn && isLoggedIn !== wasLoggedIn && clickedProdutId === colorDisplayId) {
      onAddItemToFavorites({
        colorProductId: selectedColorId,
        productSkuId: (skuId && skuId.skuId) || null,
        pdpColorProductId: colorProductId,
        formName: formName || PRODUCT_ADD_TO_BAG,
        page: pageName || 'PDP',
      });
      return { clickedProdutId: '', isLoggedIn };
    }
    return null;
  }

  componentDidMount() {
    const { isPDPSmoothScrollEnabled } = this.props;
    const isDeskTop = isClient() && getViewportInfo().isDesktop;

    const elem = this.scrollRef.current;
    if (isPDPSmoothScrollEnabled && elem && !isDeskTop) {
      elem.addEventListener('touchend', this.setImageIndex);
    }
  }

  componentWillUnmount() {
    const { isPDPSmoothScrollEnabled } = this.props;
    const isDeskTop = isClient() && getViewportInfo().isDesktop;

    const elem = this.scrollRef.current;
    if (isPDPSmoothScrollEnabled && elem && !isDeskTop) {
      window.removeEventListener('touchend', this.setImageIndex);
    }
  }

  setImageIndex = () => {
    const { images } = this.props;
    const { currentImageIndex } = this.state;
    const elem = this.scrollRef.current;
    if (elem) {
      const timer = window.setTimeout(() => {
        const imagePos = elem.scrollLeft;
        const elemWidth = elem.clientWidth;
        const imagePosition = imagePos / elemWidth;
        const ceilPosition = Math.ceil(imagePosition);
        const floorPosition = Math.floor(imagePosition);
        if (
          imagePosition > currentImageIndex &&
          currentImageIndex !== ceilPosition &&
          ceilPosition <= images.length
        ) {
          this.handleThumbnailClick(ceilPosition, true);
        } else if (imagePosition < currentImageIndex && currentImageIndex !== floorPosition) {
          this.handleThumbnailClick(floorPosition, true);
        } else {
          this.handleThumbnailClick(currentImageIndex, true);
        }
        clearTimeout(timer);
      }, 500);
    }
  };

  handleThumbnailClick = (imageIndex, isScrollElem) => {
    const { isPDPSmoothScrollEnabled } = this.props;
    const { currentImageIndex } = this.state;
    const elem = this.scrollRef.current;
    if (elem && isPDPSmoothScrollEnabled && !isScrollElem) {
      elem.scrollTo({
        top: 0,
        left: elem.clientWidth * imageIndex,
        behavior: 'smooth',
      });
    }
    if (currentImageIndex !== imageIndex) this.setState({ currentImageIndex: imageIndex });
  };

  getImageWrapperCss = () => {
    const { isFullSizeForTab, isMobile, fromPLPPage, isNewQVEnabled } = this.props;
    const imageClass = !this.renderOnPlpQV(fromPLPPage, isNewQVEnabled)
      ? 'main-image-container-wrap'
      : 'main-image-container-wrap quick-view-drawer-redesign';
    return [
      imageClass,
      isFullSizeForTab && !isMobile ? 'main-image-container-wrap-full-size' : '',
    ].join(' ');
  };

  getUrlWithHttp = (url) => url.replace(/(^\/\/)/, 'https:$1');

  scrollToCompleteLookSection = (e) => {
    e.preventDefault();
    const completeLookSection = document.querySelector('.complete-look-section');
    const showCondensedHeader = document.querySelector('.show-condensed-header');
    const yPos =
      (completeLookSection && completeLookSection.getBoundingClientRect().top) +
      window.pageYOffset -
      (showCondensedHeader && showCondensedHeader.getBoundingClientRect().height);
    window.scrollTo({
      top: yPos,
      behavior: 'smooth',
    });
  };

  getCompleteLookImage = () => {
    const {
      completeLookSlotTile,
      pdpLabels = {},
      itemPartNumber,
      isCompleteTheLookTestEnabled,
      isNewPDPEnabled,
    } = this.props;
    const { completeTheLook } = pdpLabels;
    const dataSlot = completeLookSlotTile && completeLookSlotTile[itemPartNumber];
    if (dataSlot && dataSlot.length && dataSlot.length >= 1) {
      return dataSlot[0] && dataSlot[0].largeImageUrl ? (
        <>
          {isCompleteTheLookTestEnabled && (
            <Button className="complete-look-round" onClick={this.scrollToCompleteLookSection}>
              {completeTheLook}
            </Button>
          )}
          {!isCompleteTheLookTestEnabled && !isNewPDPEnabled && (
            <div className="complete-look-wrapper">
              <Anchor anchorVariation="secondary" onClick={this.scrollToCompleteLookSection}>
                <div className="complete-look-overlay">
                  <BodyCopy
                    component="span"
                    fontFamily="secondary"
                    fontSize="fs14"
                    className="complete-look-overlay-text"
                  >
                    {completeTheLook}
                  </BodyCopy>
                </div>
                <Image
                  src={this.getUrlWithHttp(dataSlot[0].largeImageUrl)}
                  height="103px"
                  width="103px"
                  className="complete-look-image"
                />
              </Anchor>
            </div>
          )}
        </>
      ) : (
        ''
      );
    }
    return null;
  };

  getProductImage = (imgItem) => {
    const {
      productName,
      onCloseClick,
      isFullSizeModalOpen,
      isMobile,
      fromSkeleton,
      imgConfigVal,
      isShowBigSizeImages,
      isStyleWith,
      isNewQVEnabled,
      primaryBrand,
      alternateBrand,
    } = this.props;
    const imageSizePropertyName = isShowBigSizeImages ? 'bigSizeImageUrl' : 'regularSizeImageUrl';
    return (
      isStyleWith && (
        <ProductDetailImage
          imageUrl={imgItem && imgItem[imageSizePropertyName]}
          zoomImageUrl=""
          imageName={productName}
          isZoomEnabled={false}
          onOpenSimpleFullSize={onCloseClick}
          isMobile={isMobile}
          isFullSizeModalOpen={isFullSizeModalOpen}
          fromSkeleton={fromSkeleton}
          imgConfigVal={imgConfigVal}
          isNewQVEnabled={isNewQVEnabled}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
          isStyleWith={isStyleWith}
        />
      )
    );
  };

  getImageSizePropertyname = (isShowBigSizeImages) => {
    return isShowBigSizeImages ? 'bigSizeImageUrl' : 'regularSizeImageUrl';
  };

  checkIsZoomEnabled = (isVideoUrl, isGiftCard, isZoomEnabled) => {
    return isVideoUrl ? false : !isGiftCard && isZoomEnabled;
  };

  ctlCheck = (isNewPDPEnabled, isMobile, dataSlot, isGiftCard) => {
    return isNewPDPEnabled && isMobile && dataSlot && dataSlot.length && !isGiftCard;
  };

  getIsDynamicBadgeEnabled = (index, isDynamicBadgeEnabled) => index === 0 && isDynamicBadgeEnabled;

  renderImageDotes = () => {
    const { images, isNewPDPEnabled } = this.props;
    const { currentImageIndex } = this.state;
    if (isNewPDPEnabled) {
      return (
        <div className="new-image-dots-wrapper">
          <ul>
            {images &&
              images.map((_, index) => {
                return (
                  <li className={`pdp-dots ${index === currentImageIndex ? 'image-active' : ''}`}>
                    <div
                      key={index.toString()}
                      className={`new-image-dots-pdp ${
                        index === currentImageIndex ? 'pdp-image-active' : null
                      }`}
                    />
                  </li>
                );
              })}
          </ul>
        </div>
      );
    }
    return (
      <div className="new-image-dots-wrapper">
        {images &&
          images.map((_, index) => {
            return (
              <div
                key={index.toString()}
                className={`new-image-dots-pdp ${
                  index === currentImageIndex ? 'pdp-image-active' : null
                }`}
              />
            );
          })}
      </div>
    );
  };

  renderLeftArrow = (showArrow, lightArrow, darkArrow) => {
    const { currentImageIndex } = this.state;
    return showArrow ? (
      <button
        type="button"
        disabled={!currentImageIndex}
        onClick={() => this.handleThumbnailClick(currentImageIndex - 1)}
        className="smooth-scroll-prev-button"
      >
        <Image
          src={!currentImageIndex ? lightArrow : darkArrow}
          alt="left-arrow"
          className="smooth-scroll-prev"
        />
      </button>
    ) : null;
  };

  renderRightArrow = (showArrow, isLastImage, lightArrow, darkArrow) => {
    const { currentImageIndex } = this.state;
    const arrowIcon = isLastImage ? lightArrow : darkArrow;
    return showArrow ? (
      <button
        type="button"
        disabled={isLastImage}
        onClick={() => this.handleThumbnailClick(currentImageIndex + 1)}
        className="smooth-scroll-next-button"
      >
        <Image src={arrowIcon} alt="left-arrow" className="smooth-scroll-next" />
      </button>
    ) : null;
  };

  stickyEnabledCheck = (isMobileDevice, isNewPDPEnabled) => {
    return isMobileDevice && isNewPDPEnabled;
  };

  stickyDesktopImage = (isActive) => {
    const { isMobile, isNewPDPEnabled } = this.props;
    if (!isMobile && isNewPDPEnabled) {
      const productImage = document.getElementById('productDetailsSection');
      if (productImage) {
        productImage.style.zIndex = isActive ? '-1' : '';
      }
    }
  };

  getIsSocialProofMobileTopEnabled = (isNewPDPEnabled, isPDP, isMobile, socialProofMessage) => {
    return isNewPDPEnabled && isPDP && isMobile && socialProofMessage?.variation === 'top';
  };

  getIsOOSMarkerView = (keepAlive, checkForOOSForVariant) => {
    return keepAlive || checkForOOSForVariant;
  };

  renderCarouselImage = () => {
    const {
      productName,
      images,
      isShowBigSizeImages,
      isZoomEnabled,
      onCloseClick,
      isFullSizeModalOpen,
      isMobile,
      isGiftCard,
      keepAlive,
      outOfStockLabels,
      fromSkeleton,
      imgConfigVal,
      checkForOOSForVariant,
      hasABTestForPDPColorOrImageSwatchHover,
      imagesByColor,
      largeImageNameOnHover,
      TCPStyleQTY,
      TCPStyleType,
      isDynamicBadgeEnabled,
      fromPLPPage,
      isNewQVEnabled,
      isNewPDPEnabled,
      primaryBrand,
      pdpLabels,
      ratingsProductId,
      productMiscInfo,
      deleteFavItemInProgressFlag,
      isInWishList,
      completeLookSlotTile,
      itemPartNumber,
      alternateBrand,
      isPDP,
      socialProofMessage,
      isPDPSmoothScrollEnabled,
    } = this.props;
    const { currentImageIndex } = this.state;
    let basicImageUrl;
    if (hasABTestForPDPColorOrImageSwatchHover) {
      basicImageUrl =
        imagesByColor &&
        imagesByColor[largeImageNameOnHover] &&
        imagesByColor[largeImageNameOnHover].basicImageUrl;
    }
    const imageSizePropertyName = this.getImageSizePropertyname(isShowBigSizeImages);
    const isDeskTop = isClient() && getViewportInfo().isDesktop;
    const imagesLength = Array.isArray(images) ? images.length : 0;
    const showArrow = isDeskTop && imagesLength > 1;
    const isLastImage = currentImageIndex === imagesLength - 1;
    const darkArrow = getIconPath('carousel-big-carrot');
    const lightArrow = getIconPath('right-disable-carousel-carrot');
    const favoritedCount = productMiscInfo?.favoritedCount;
    const isFavoriteView = false;
    const outFitFavt = isInWishList;
    const handleAddRemoveFromWishlistObj = {
      handleAddToWishlist: this.handleAddToWishlist,
      handleRemoveFromWishList: this.handleRemoveFromWishList,
      deleteFavItemInProgressFlag,
    };
    const dataSlot = completeLookSlotTile?.[itemPartNumber];
    const isOOSMarkerView = this.getIsOOSMarkerView(keepAlive, checkForOOSForVariant);
    const socialProofMobileTopNewPDP = this.getIsSocialProofMobileTopEnabled(
      isNewPDPEnabled,
      isPDP,
      isMobile,
      socialProofMessage
    );
    return (
      <div>
        <div className="main-image-scroll-container" ref={this.scrollRef}>
          {this.renderLeftArrow(showArrow, lightArrow, darkArrow)}
          {images &&
            images.map((image, index) => {
              const { superSizeImageUrl } = image;
              const isVideoUrl = getVideoUrl(superSizeImageUrl);
              const checkZoomEnabled = this.checkIsZoomEnabled(
                isVideoUrl,
                isGiftCard,
                isZoomEnabled
              );
              return (
                <div
                  className="product-image-wrapper product-wrapper-inline"
                  onMouseEnter={() => this.stickyDesktopImage(true)}
                  onMouseOut={() => this.stickyDesktopImage()}
                  onBlur={() => this.stickyDesktopImage()}
                >
                  <ProductDetailImage
                    imageUrl={
                      hasABTestForPDPColorOrImageSwatchHover && basicImageUrl
                        ? basicImageUrl
                        : image && image[imageSizePropertyName]
                    }
                    zoomImageUrl={superSizeImageUrl}
                    imageName={productName}
                    isZoomEnabled={checkZoomEnabled}
                    onOpenSimpleFullSize={onCloseClick}
                    isMobile={isMobile}
                    isFullSizeModalOpen={isFullSizeModalOpen}
                    fromSkeleton={fromSkeleton}
                    imgConfigVal={imgConfigVal}
                    TCPStyleQTY={TCPStyleQTY}
                    TCPStyleType={TCPStyleType}
                    isDynamicBadgeEnabled={this.getIsDynamicBadgeEnabled(
                      index,
                      isDynamicBadgeEnabled
                    )}
                    fromPLPPage={fromPLPPage}
                    isNewQVEnabled={isNewQVEnabled}
                    primaryBrand={primaryBrand}
                    alternateBrand={alternateBrand}
                  />
                  {isOOSMarkerView && (
                    <OutOfStockWaterMarkView
                      label={outOfStockLabels.outOfStockCaps}
                      fontSizes={['fs16', 'fs16', 'fs48']}
                    />
                  )}
                </div>
              );
            })}
          {this.renderRightArrow(showArrow, isLastImage, lightArrow, darkArrow)}
        </div>

        {isNewPDPEnabled && !fromSkeleton && (
          <div className="share-fav-icons">
            <div className="social-share-wrapper">
              <Share
                isFacebookEnabled
                isPinterestEnabled
                isTwitterEnabled
                accessibilityLabels={{}}
                ratingsProductId={ratingsProductId}
              />
            </div>
            <div className="wishlist-container-wrapper">
              {WishListIcon(
                isFavoriteView,
                outFitFavt,
                handleAddRemoveFromWishlistObj,
                false, // itemNotAvailable
                favoritedCount
              )}
            </div>
          </div>
        )}
        {this.ctlCheck(isNewPDPEnabled, isMobile, dataSlot, isGiftCard) ? (
          <div className="complete-the-look-wrapper-new-pdp">
            <CompleteTheLook
              scrollToCompleteLookSection={this.scrollToCompleteLookSection}
              pdpLabels={pdpLabels}
            />
          </div>
        ) : null}
        {socialProofMobileTopNewPDP ? (
          <div
            className={`social-proof-mobile-top ${
              isPDPSmoothScrollEnabled ? 'smooth-scroll-enabled' : ''
            }`}
          >
            <SocialProofMessage socialProofMessage={socialProofMessage} />
          </div>
        ) : null}
        {!isDeskTop && imagesLength > 1 && this.renderImageDotes()}
      </div>
    );
  };

  renderOnPlpQV = (fromPLPPage, isNewQVEnabled) => {
    return fromPLPPage && isNewQVEnabled;
  };

  renderOldCarouselImage = () => {
    const {
      productName,
      images,
      isShowBigSizeImages,
      isZoomEnabled,
      onCloseClick,
      isFullSizeModalOpen,
      isMobile,
      isGiftCard,
      keepAlive,
      outOfStockLabels,
      fromSkeleton,
      imgConfigVal,
      checkForOOSForVariant,
      hasABTestForPDPColorOrImageSwatchHover,
      imagesByColor,
      largeImageNameOnHover,
      TCPStyleQTY,
      TCPStyleType,
      isDynamicBadgeEnabled,
      fromPLPPage,
      isNewQVEnabled,
      isNewPDPEnabled,
      primaryBrand,
      pdpLabels,
      ratingsProductId,
      productMiscInfo,
      deleteFavItemInProgressFlag,
      isInWishList,
      completeLookSlotTile,
      itemPartNumber,
      alternateBrand,
    } = this.props;
    const favoritedCount = productMiscInfo?.favoritedCount;
    const isPlpQV = this.renderOnPlpQV(fromPLPPage, isNewQVEnabled);
    const { currentImageIndex } = this.state;
    let basicImageUrl;
    if (hasABTestForPDPColorOrImageSwatchHover) {
      basicImageUrl =
        imagesByColor &&
        imagesByColor[largeImageNameOnHover] &&
        imagesByColor[largeImageNameOnHover].basicImageUrl;
    }
    const imageSizePropertyName = this.getImageSizePropertyname(isShowBigSizeImages);
    const darkArrow = getIconPath('carousel-big-carrot');

    const { CAROUSEL_OPTIONS, CAROUSEL_OPTIONS_QV } = config;
    CAROUSEL_OPTIONS.beforeChange = (current, next) => {
      this.setState({ currentImageIndex: next });
    };
    CAROUSEL_OPTIONS_QV.beforeChange = (current, next) => {
      this.setState({ currentImageIndex: next });
    };
    const isFavoriteView = false;
    const outFitFavt = isInWishList;
    const handleAddRemoveFromWishlistObj = {
      handleAddToWishlist: this.handleAddToWishlist,
      handleRemoveFromWishList: this.handleRemoveFromWishList,
      deleteFavItemInProgressFlag,
    };
    const dataSlot = completeLookSlotTile?.[itemPartNumber];
    return (
      <div>
        <Carousel
          options={isPlpQV ? config.CAROUSEL_OPTIONS_QV : config.CAROUSEL_OPTIONS}
          inheritedStyles={isNewQVEnabled ? carousalStyleNewQV : carousalStyle}
          sliderImageIndex={currentImageIndex}
          carouselConfig={{
            autoplay: false,
            customArrowLeft: darkArrow,
            customArrowRight: darkArrow,
          }}
          className="product-img-slider"
          isPlpQV={isPlpQV}
        >
          {images &&
            images.map((image, index) => {
              const { superSizeImageUrl } = image;
              const isVideoUrl = getVideoUrl(superSizeImageUrl);
              const checkZoomEnabled = this.checkIsZoomEnabled(
                isVideoUrl,
                isGiftCard,
                isZoomEnabled
              );
              return (
                <div
                  className="product-image-wrapper"
                  onMouseEnter={() => this.stickyDesktopImage(true)}
                  onMouseOut={() => this.stickyDesktopImage()}
                  onBlur={() => this.stickyDesktopImage()}
                >
                  <ProductDetailImage
                    imageUrl={
                      hasABTestForPDPColorOrImageSwatchHover && basicImageUrl
                        ? basicImageUrl
                        : image && image[imageSizePropertyName]
                    }
                    zoomImageUrl={superSizeImageUrl}
                    imageName={productName}
                    isZoomEnabled={checkZoomEnabled}
                    onOpenSimpleFullSize={onCloseClick}
                    isMobile={isMobile}
                    isFullSizeModalOpen={isFullSizeModalOpen}
                    fromSkeleton={fromSkeleton}
                    imgConfigVal={imgConfigVal}
                    TCPStyleQTY={TCPStyleQTY}
                    TCPStyleType={TCPStyleType}
                    isDynamicBadgeEnabled={this.getIsDynamicBadgeEnabled(
                      index,
                      isDynamicBadgeEnabled
                    )}
                    fromPLPPage={fromPLPPage}
                    isNewQVEnabled={isNewQVEnabled}
                    primaryBrand={primaryBrand}
                    alternateBrand={alternateBrand}
                  />
                  {(keepAlive || checkForOOSForVariant) && (
                    <OutOfStockWaterMarkView
                      label={outOfStockLabels.outOfStockCaps}
                      fontSizes={['fs16', 'fs16', 'fs48']}
                    />
                  )}
                </div>
              );
            })}
        </Carousel>
        {isNewPDPEnabled && !fromSkeleton && (
          <div className="share-fav-icons">
            <div className="social-share-wrapper">
              <Share
                isFacebookEnabled
                isPinterestEnabled
                isTwitterEnabled
                accessibilityLabels={{}}
                ratingsProductId={ratingsProductId}
              />
            </div>
            <div className="wishlist-container-wrapper">
              {WishListIcon(
                isFavoriteView,
                outFitFavt,
                handleAddRemoveFromWishlistObj,
                false, // itemNotAvailable
                favoritedCount
              )}
            </div>
          </div>
        )}
        {isNewPDPEnabled && isMobile && dataSlot && dataSlot.length && !isGiftCard ? (
          <div className="complete-the-look-wrapper-new-pdp">
            <CompleteTheLook
              scrollToCompleteLookSection={this.scrollToCompleteLookSection}
              pdpLabels={pdpLabels}
            />
          </div>
        ) : null}
      </div>
    );
  };

  isFullSizeCheck = (isFullSizeVisible, isGiftCard, isStyleWith) => {
    return isFullSizeVisible && !isGiftCard && !isStyleWith;
  };

  handleAddToWishlist = () => {
    const {
      onAddItemToFavorites,
      productInfo: { productId },
      productMiscInfo: { colorProductId, colorDisplayId },
      pageName,
      skuId,
      formName,
      deleteFavItemInProgressFlag,
    } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const selectedColorId = colorDisplayId || productId;
    onAddItemToFavorites({
      colorProductId: selectedColorId,
      productSkuId: (skuId && skuId.skuId) || null,
      pdpColorProductId: colorProductId,
      formName: formName || PRODUCT_ADD_TO_BAG,
      page: pageName || 'PDP',
    });
    this.setState({
      clickedProdutId: selectedColorId,
    });
  };

  handleRemoveFromWishList = () => {
    const {
      onSetLastDeletedItemIdAction,
      productInfo: { productId },
      productMiscInfo: { colorDisplayId },
      checkForOOSForVariant,
      wishList,
      deleteFavItemInProgressFlag,
    } = this.props;
    if (checkForOOSForVariant || deleteFavItemInProgressFlag) {
      return;
    }
    const selectedWishList = wishList && wishList[colorDisplayId || productId];
    if (selectedWishList) {
      const { externalIdentifier, giftListItemID } = selectedWishList;
      onSetLastDeletedItemIdAction({
        externalId: externalIdentifier,
        itemId: giftListItemID,
        notFromFavoritePage: true,
        productId: colorDisplayId || productId,
      });
    }
  };

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps() {
    const { images } = this.props;
    const { currentImageIndex } = this.state;
    const imagesLength = images.length - 1;

    if (imagesLength < currentImageIndex) {
      this.setState({ currentImageIndex: imagesLength });
    }
  }

  render() {
    const {
      productName,
      images,
      isShowBigSizeImages,
      isFullSizeVisible,
      className,
      onCloseClick,
      isFullSizeModalOpen,
      isMobile,
      pdpLabels,
      isGiftCard,
      accessibilityLabels,
      ratingsProductId,
      fromSkeleton,
      isCompleteTheLookTestEnabled,
      isStyleWith,
      isPDPSmoothScrollEnabled,
      fromPLPPage,
      isNewQVEnabled,
      isThumbnailListVisible,
      isNewPDPEnabled,
      primaryBrand,
      pageName,
      alternateBrand,
      socialProofMessage,
      isPDP,
    } = this.props;
    const socialProofMobileTopNewPDP = this.getIsSocialProofMobileTopEnabled(
      isNewPDPEnabled,
      isPDP,
      isMobile,
      socialProofMessage
    );
    const isPlpQV = this.renderOnPlpQV(fromPLPPage, isNewQVEnabled);
    const { currentImageIndex } = this.state;

    const thumbnailImagesPaths = images.map((image) => ({
      imageUrl: image.iconSizeImageUrl,
      imageName: productName,
    }));
    const imageSizePropertyName = this.getImageSizePropertyname(isShowBigSizeImages);
    const imageContainerClass = getImageContainerClass(fromSkeleton);
    const imgItem = images[0];
    const isMobileDevice = isClient() && getViewportInfo().isMobile;
    return (
      <div
        className={`${className}${
          isNewPDPEnabled && pageName === 'PDP' ? ' product-image-social-icon-wrapper' : ''
        }`}
      >
        {!isStyleWith && (
          <ThumbnailList
            isThumbnailListVisible={isNewQVEnabled ? false : isThumbnailListVisible}
            thumbnailImagesPaths={thumbnailImagesPaths}
            currentImageIndex={currentImageIndex}
            onThumbnailClick={this.handleThumbnailClick}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
            fromSkeleton={fromSkeleton}
          />
        )}
        {this.getProductImage(imgItem)}
        {!isStyleWith && (
          <div className={this.getImageWrapperCss()}>
            <div className="main-image-container">
              <div className={imageContainerClass}>
                {isPDPSmoothScrollEnabled
                  ? this.renderCarouselImage(isMobileDevice)
                  : this.renderOldCarouselImage(isMobileDevice)}
              </div>
              {socialProofMobileTopNewPDP && !isPDPSmoothScrollEnabled ? (
                <div
                  className={`social-proof-mobile-top ${
                    isPDPSmoothScrollEnabled ? 'smooth-scroll-enabled' : ''
                  }`}
                >
                  <SocialProofMessage socialProofMessage={socialProofMessage} />
                </div>
              ) : null}
              <div className={`social-connect-wrapper ${isPlpQV ? 'hide-item' : ''}`}>
                {this.getCompleteLookImage()}
                {this.isFullSizeCheck(isFullSizeVisible, isGiftCard, isStyleWith) && (
                  <span className="fullSize-image-label">
                    <Anchor
                      className="resize-text"
                      asPath="/home"
                      aria-label="View full size image"
                      onClick={onCloseClick}
                      dataLocator={getLocator('pdp_full_size_btn')}
                    >
                      <Image
                        alt={pdpLabels.fullSize}
                        className="icon-expand"
                        src={getIconPath('icon-expand')}
                        height="25px"
                        width="25px"
                      />
                      <BodyCopy fontFamily="secondary" fontSize="fs10">
                        {pdpLabels.fullSize}
                      </BodyCopy>
                    </Anchor>
                  </span>
                )}
                <SocialConnectTemplate
                  isFullSizeVisible={isFullSizeVisible}
                  isMobile={isMobile}
                  isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
                  accessibilityLabels={accessibilityLabels}
                  ratingsProductId={ratingsProductId}
                  isNewPDPEnabled={isNewPDPEnabled}
                />
              </div>
            </div>
            <div className="enlarged-image-container">
              <div id="portal" className="enlarged-image" />
            </div>
          </div>
        )}
        {isFullSizeModalOpen &&
          (isMobile ? (
            <FullSizeImageModal
              name={productName}
              modalLabel={pdpLabels.fullSize}
              image={images[currentImageIndex] && images[currentImageIndex][imageSizePropertyName]}
              onCloseClick={onCloseClick}
              primaryBrand={primaryBrand}
              alternateBrand={alternateBrand}
            />
          ) : null)}
      </div>
    );
  }
}

ProductImages.defaultProps = defaultProps;

export default errorBoundary(withStyles(ProductImages, styles));
export { ProductImages as ProductImagesVanilla };
