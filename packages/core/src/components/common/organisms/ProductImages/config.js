// 9fbef606107a605d69c0edbcd8029e5d
import PropTypes from 'prop-types';
import { breakpoints } from '../../../../../styles/themes/TCP/mediaQuery';

export const defaultProps = {
  className: '',
  isShowBigSizeImages: false,
  isFullSizeForTab: false,
  isFullSizeVisible: true,
  isFullSizeModalOpen: false,
  isMobile: true,
  isGiftCard: false,
  outOfStockLabels: {
    outOfStockCaps: '',
  },
  keepAlive: false,
  accessibilityLabels: {},
  ratingsProductId: '',
  completeLookSlotTile: {},
  itemPartNumber: '',
  imgConfigVal: [],
  isCompleteTheLookTestEnabled: false,
  isStyleWith: false,
  checkForOOSForVariant: false,
  largeImageNameOnHover: '',
  imagesByColor: {},
  hasABTestForPDPColorOrImageSwatchHover: false,
  TCPStyleQTY: 0,
  TCPStyleType: '',
  isDynamicBadgeEnabled: false,
  fromPLPPage: false,
  isNewQVEnabled: false,
  isPDPSmoothScrollEnabled: false,
  isNewPDPEnabled: false,
};

export const propTypes = {
  /** Product's Name (global product, not by color, size, fit or some clasification) */
  productName: PropTypes.string.isRequired,

  /** list of images of the product */
  images: PropTypes.arrayOf(
    PropTypes.shape({
      iconSizeImageUrl: PropTypes.string.isRequired,
      regularSizeImageUrl: PropTypes.string.isRequired,
      bigSizeImageUrl: PropTypes.string.isRequired,
      superSizeImageUrl: PropTypes.string.isRequired,
    })
  ).isRequired,
  pdpLabels: PropTypes.shape({
    fullSize: PropTypes.string,
  }).isRequired,
  /**
   * Flags if we should show big size images, instead of regular size
   * images (default behavior)
   */
  isShowBigSizeImages: PropTypes.bool,
  isGiftCard: PropTypes.bool,

  /** Flags if the zoom should be enabled */
  isZoomEnabled: PropTypes.bool.isRequired,
  isThumbnailListVisible: PropTypes.bool.isRequired,
  className: PropTypes.string,
  isFullSizeVisible: PropTypes.bool,
  isFullSizeForTab: PropTypes.bool,
  onCloseClick: PropTypes.func.isRequired,
  isFullSizeModalOpen: PropTypes.bool,
  isMobile: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({
    outOfStockCaps: PropTypes.string,
  }),
  keepAlive: PropTypes.bool,
  accessibilityLabels: PropTypes.shape({}),
  ratingsProductId: PropTypes.string,
  completeLookSlotTile: PropTypes.shape({}),
  itemPartNumber: PropTypes.string,
  fromSkeleton: PropTypes.bool.isRequired,
  imgConfigVal: PropTypes.shape([]),
  isCompleteTheLookTestEnabled: PropTypes.bool,
  isStyleWith: PropTypes.bool,
  checkForOOSForVariant: PropTypes.bool,
  largeImageNameOnHover: PropTypes.string,
  imagesByColor: PropTypes.shape({}),
  hasABTestForPDPColorOrImageSwatchHover: PropTypes.bool,
  TCPStyleQTY: PropTypes.number,
  TCPStyleType: PropTypes.string,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  fromPLPPage: PropTypes.bool,
  isNewQVEnabled: PropTypes.bool,
  isPDPSmoothScrollEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
};

export default {
  CAROUSEL_OPTIONS: {
    autoplay: false,
    arrows: true,
    fade: false,
    speed: 500,
    dots: true,
    dotsClass: 'slick-dots',
    swipe: true,
    slidesToShow: 1,
    infinite: false,
    lazyLoad: 'progressive',
    responsive: [
      {
        breakpoint: breakpoints.values.lg - 1,
        settings: {
          arrows: false,
        },
      },
    ],
  },
  CAROUSEL_OPTIONS_QV: {
    autoplay: false,
    arrows: true,
    fade: false,
    speed: 500,
    dots: true,
    dotsClass: 'slick-dots-qv',
    swipe: true,
    slidesToShow: 1,
    infinite: false,
    lazyLoad: 'progressive',
    responsive: [
      {
        breakpoint: parseInt(breakpoints.medium, 10) - 1,
        settings: {
          arrows: false,
        },
      },
    ],
  },
};
