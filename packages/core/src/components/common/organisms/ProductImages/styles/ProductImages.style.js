/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '../../../../../utils';

export default css`
  display: flex;
  justify-content: space-between;
  .preview-and-social-media-icons {
    display: none;
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 100px;
      display: block;
    }
  }
  .resize-text {
    position: absolute;
    display: none;
  }

  .main-image-container-wrap {
    width: 100%;
    display: flex;
    flex-direction: row;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-bottom: 30px;
    }
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      ${(props) => (props.isNewPDPEnabled ? 'overflow:hidden;' : '')}
    }
  }
  .share-fav-icons {
    position: absolute;
    right: 0;
    top: 0;
  }
  .complete-the-look-wrapper-new-pdp {
    position: absolute;
    right: 5px;
    display: flex;
    animation: complete-the-look-position-slide-in 0.5s ease-in 7s forwards;
  }
  @keyframes complete-the-look-position-slide-in {
    0% {
      left: 50px;
    }
    100% {
      left: 185px;
    }
  }
  .social-proof-mobile-top {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: -36px;
      margin-bottom: -20px;
    }
    .social-proof-wrapper {
      justify-content: center;
    }
  }

  .social-proof-mobile-top.smooth-scroll-enabled {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: -72px;
      margin-bottom: 0;
    }
  }
  .smooth-scroll-enabled {
    .social-proof-wrapper {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        ${(props) => (props.isNewPDPEnabled ? 'bottom: -12px;' : '')}
      }
    }
  }

  .enlarged-image-container {
    position: relative;
    z-index: ${(props) => props.theme.zindex.zEnlargedImage};
  }
  .enlarged-image {
    position: absolute;
    z-index: ${(props) => props.theme.zindex.zEnlargedImage};
    height: 100%;
    margin-left: 90px;
    img {
      max-width: none;
    }
  }
  .social-share-wrapper {
    display: flex;
    position: relative;
    top: 18px;
    left: 25px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 190px;
      height: 44px;
      position: relative;
      padding: 0 1px 0 0;
      left: 32px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      display: flex;
      position: relative;
      left: 70px;
      top: 20px;
    }
  }
  .fav-icon-wrapper {
    display: grid;
  }
  .favorite-count {
    text-align: center;
    line-height: 1;
  }
  .wishlist-container-wrapper {
    height: 35px;
    width: 35px;
    top: 75px;
    left: 25px;
    position: relative;
    opacity: 0.9;
    border-radius: 10px;
    background-color: ${(props) => props.theme.colors.WHITE};
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-bottom: 8px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-right: 10px;
      left: 32px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: 2px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 35px;
      height: 41px;
      position: absolute;
      left: 155px;
      padding-left: 3px;
    }
  }
  .enlarged-image-wrapper {
    z-index: 100;
    border: 1px solid ${(props) => props.theme.colors.BORDER.NORMAL};
  }
  .main-image-container {
    width: 100%;
    position: relative;
  }
  .main-image-scroll-container {
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
    -ms-overflow-style: none;
    scrollbar-width: none;
    scroll-snap-type: x mandatory;
  }
  .main-image-scroll-container::-webkit-scrollbar {
    display: none;
  }

  .new-image-dots-wrapper {
    display: flex;
    justify-content: center;
    margin: 20px 0;
    align-items: center;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      ${(props) => (props.isNewPDPEnabled ? 'margin: 30px 0 20px 0; padding-top: 25px;' : '')}
    }
    ul {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
      display: flex;
      align-items: center;
      justify-content: center;
      `
          : ''}
    }
    li {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
      display: inline-block;
      position: relative;
      margin: 0 2px;
      width: 6px;
      height: 6px;
      padding-right: 36px;
      `
          : ''}
    }
    li div:before {
      ${(props) =>
        props.isNewPDPEnabled
          ? `content: '';
          box-sizing: border-box;
          position: absolute;
          top: 0;
          left: 0;
          width: ${props.theme.spacing.ELEM_SPACING.XXXL};
          height: ${props.theme.spacing.ELEM_SPACING.XXXS};
          border-radius: ${props.theme.spacing.ELEM_SPACING.XXS};
          background: ${props.theme.colors.PRIMARY.LIGHTGRAY};`
          : ''}
    }
    li.image-active div:before {
      ${(props) =>
        props.isNewPDPEnabled
          ? `width: 48px;height: 2px;background:${props.theme.colors.WHITE};border: 1px solid ${props.theme.colors.PRIMARY.DARK};`
          : ''}
    }
  }
  .new-image-dots-pdp {
    ${(props) =>
      !props.isNewPDPEnabled
        ? `
    width: ${props.theme.spacing.ELEM_SPACING.XS_6};
    height: ${props.theme.spacing.ELEM_SPACING.XS_6};
    background: rgb(87, 87, 87);
    margin-right: 3px;
    border-radius: 50%;
    display: inline-block;
    `
        : ''}
  }

  .pdp-image-active {
    ${(props) =>
      !props.isNewPDPEnabled
        ? `
    width: ${props.theme.spacing.ELEM_SPACING.XS};
    height: ${props.theme.spacing.ELEM_SPACING.XS};
    border: 1px solid rgb(87, 87, 87);
    background: ${props.theme.colors.WHITE};
    `
        : ''}
  }
  .carousel-container {
    position: relative;
    width: 100%;
  }

  .skeleton-img {
    img {
      min-width: 100%;
    }
  }

  .product-image-wrapper {
    position: relative;
  }
  .product-wrapper-inline {
    display: inline-block;
    width: 100%;
    vertical-align: middle;
    scroll-snap-align: center;
  }

  .facebook,
  .pinterest,
  .twitter {
    width: 35px;
    height: 35px;
    object-fit: contain;
  }

  .complete-look-round {
    height: 72px;
    width: 72px;
    position: absolute;
    bottom: -67px;
    border-radius: 50%;
    right: 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background: ${(props) => props.theme.colors.WHITE};
    align-items: center;
    color: ${(props) => props.theme.colors.PRIMARY.DARK};
    font-family: ${(props) => props.theme.fonts.secondaryFontSemilBoldFamily};
    font-size: ${(props) => props.theme.typography.fontSizes.fs10};
    text-align: center;
    box-sizing: border-box;
    text-transform: uppercase;
    border: 1px solid ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
    outline: none;
    letter-spacing: 0;
    &:after {
      content: '';
      width: 10px;
      height: 10px;
      bottom: 7px;
      left: 50%;
      transform: translateX(-50%);
      position: absolute;
      display: inline-flex;
      background: url('${(props) => getIconPath('down_arrow_icon', props)}') no-repeat right center;
    }
    &:active {
      background: ${(props) => props.theme.colors.WHITE};
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .social-connect-wrapper {
    padding-top: 20px;
    width: 100%;
    display: flex;
    flex-direction: inherit;
    justify-content: space-between;
    position: relative;
    margin: 18px 50px 0px 0px;
    ${(props) => (props.isPDPSmoothScrollEnabled ? '' : 'margin-top: 50px;')}
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          padding-top: 0px;
      `
          : 'padding-top: 20px;'}
    }
  }
  .social-connect-wrapper.hide-item {
    display: none;
  }
  .icon-twitter {
    margin-right: 14px;
  }
  .icon-facebook {
    margin-right: 14px;
  }
  .icon-expand {
    margin-right: 6px;
  }

  .smooth-scroll-prev-button,
  .smooth-scroll-next-button {
    cursor: pointer;
    background-color: transparent;
    border: none;
    position: absolute;
    top: 50%;
    z-index: 1;
  }

  .smooth-scroll-next-button {
    right: -30px;
  }
  .smooth-scroll-prev-button {
    left: -30px;
  }

  .smooth-scroll-prev {
    width: 19px;
    height: 39px;
    transform: rotate(180deg);
  }

  .smooth-scroll-next {
    width: 19px;
    height: 39px;
  }
  .slick-dots {
    position: absolute;
    bottom: -${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    z-index: 9;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          bottom: -15px;
      `
          : ` bottom: -${props.theme.spacing.ELEM_SPACING.XXXL};`}
    }
  }

  .product-img-slider .slick-dots-qv {
    position: absolute;
    top: 235px;
    height: 4px;
    z-index: 9;
  }
  .product-img-slider li.slick-active {
    ${(props) =>
      props.isNewQVEnabled && props.fromPLPPage
        ? 'margin: 0px;margin-top: 4px;width: 10px;height: 10px;padding-right: 35px;'
        : ''}
  }
  .product-img-slider .slick-dots-qv li {
    ${(props) =>
      props.isNewQVEnabled && props.fromPLPPage
        ? 'position: relative;display: inline-block;margin: 0 2px;width: 6px;height: 6px;padding-right: 36px;'
        : ''}
  }
  .product-img-slider .slick-dots-qv li button:before {
    ${(props) =>
      props.isNewQVEnabled && props.fromPLPPage
        ? `content:"";box-sizing: border-box;position: absolute;top: 0;left: 0;width: 48px;height: 2px;border-radius: 4px;background: ${props.theme.colors.PRIMARY.LIGHTGRAY};`
        : ''}
  }
  .product-img-slider li.slick-active button:before {
    ${(props) =>
      props.isNewQVEnabled && props.fromPLPPage
        ? `width: 48px;height: 2px;background:${props.theme.colors.WHITE};border: 1px solid ${props.theme.colors.PRIMARY.DARK};`
        : ''}
  }
  .social-connect-sub-wrapper {
    display: flex;
    justify-content: flex-end;
    flex: 1;
    align-items: center;
    @media ${(props) => props.theme.mediaQuery.medium} {
      justify-content: center;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      justify-content: flex-end;
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    &.product-image-social-icon-wrapper {
      position: sticky;
      top: 70px;
    }
    .main-image-container-wrap {
      width: 100%;
      margin-right: 30px;
    }
    .main-image-container-wrap.quick-view-drawer-redesign {
      width: calc(100% - 170px);
      margin: 0 auto;
    }
    .main-image-container-wrap-full-size {
      width: calc(100% - 146px);
      margin-left: 40px;
      margin-right: 16px;
    }
    .resize-text {
      display: inline-flex;
      flex-direction: row;
      align-items: center;
    }
    ${(props) =>
      !props.isNewPDPEnabled
        ? `
    .social-connect-wrapper {
      padding-top: 34px;
      width: 100%;
      display: inline-block;
    }

    .facebook,
    .pinterest,
    .twitter {
      width: 35px;
      height: 35px;
      object-fit: contain;
    }`
        : ''}
    .slick-dots {
      position: absolute;
      bottom: -${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    }
    .fullSize-image-label {
      display: inline-block;
    }
    .fullSize-image-label .resize-text {
      position: unset;
      bottom: initial;
    }
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .product-image-wrapper img {
      ${(props) =>
        props.isNewQVEnabled && props.fromPLPPage ? 'max-height: 204px;width: 164px;' : ''}
    }
  }
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    .social-connect-wrapper {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    }
    img {
      ${(props) =>
        props.isNewQVEnabled && props.fromPLPPage ? 'max-height: 388px;width: 310px;' : ''}
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .main-image-container-wrap {
      width: calc(100% - 280px);
      margin-left: 90px;
      margin-right: 90px;
    }
    img {
      ${(props) =>
        props.isNewQVEnabled && props.fromPLPPage ? 'max-height: 462px;width: 370px;' : ''}
    }
    .main-image-container-wrap-full-size {
      width: calc(100% - 195px);
      margin-left: 65px;
      margin-right: 40px;
    }
    ${(props) =>
      !props.isNewPDPEnabled
        ? `
    .social-connect-wrapper {
      padding-top: 11px;
      width: 100%;
      display: flex;
      flex-direction: inherit;
      justify-content: space-between;
    }
    .facebook,
    .pinterest,
    .twitter {
      width: 31px;
      height: 31px;
      object-fit: contain;
    }`
        : ''}
    .fullSize-image-label .resize-text {
      position: unset;
      bottom: initial;
    }
    /* stylelint-disable-next-line */
    _:-ms-fullscreen,
    .fullSize-image-label {
      margin-top: 95px;
    }
  }

  .complete-look-wrapper {
    position: relative;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .complete-look-wrapper a {
    display: inline-block;
  }
  .complete-look-overlay {
    position: absolute;
    height: 100%;
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    display: flex;
  }
  .complete-look-overlay-text {
    display: flex;
    align-items: center;
    text-transform: uppercase;
    text-align: center;
    z-index: 1;
  }
  .complete-look-image {
    border: 1px solid ${(props) => props.theme.colorPalette.gray['600']};
    opacity: 0.3;
  }
`;

export const carousalStyle = css`
  .slick-next {
    height: 39px;
    right: -20px;
    width: 19px;
  }
  .slick-prev {
    height: 38px;
    left: -20px;
    width: 19px;
  }
  .slick-disabled {
    pointer-events: none;
    background-image: url('${(props) => getIconPath('right-disable-carousel-carrot', props)}');
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .slick-dots {
      display: none;
    }
  }
`;
export const carousalStyleNewQV = css`
  .slick-next {
    height: 39px;
    right: -75px;
    width: 19px;
  }
  .slick-prev {
    height: 38px;
    left: -75px;
    width: 19px;
  }
  .slick-disabled {
    pointer-events: none;
    background-image: url('${(props) => getIconPath('right-disable-carousel-carrot', props)}');
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .slick-dots {
      display: none;
    }
    .slick-dots-qv {
      display: none;
    }
  }
`;
