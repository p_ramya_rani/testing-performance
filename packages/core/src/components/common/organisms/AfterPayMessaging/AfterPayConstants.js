// 9fbef606107a605d69c0edbcd8029e5d
const AFTERPAY_CONSTANTS = {
  DATA_MODAL_THEME: {
    WHITE: 'white',
    MINT: 'mint',
  },
  DATA_BADGE_THEME: {
    BLACK_ON_MINT: 'black-on-mint',
    MINT_ON_BLACK: 'mint-on-black',
    BLACK_ON_WHITE: 'black-on-white',
    WHITE_ON_BLACK: 'white-on-black',
  },
  LOGO_TYPE: {
    BADGE: 'badge',
    LOCK_UP: 'lockup',
    COMPACT_BADGE: 'compact-badge',
  },
};
export default AFTERPAY_CONSTANTS;
