// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AfterPayMessagingVanilla } from '../AfterPayMessaging.view';

describe('AfterPayMessaging view', () => {
  it('should render AfterPayMessaging', () => {
    const component = shallow(<AfterPayMessagingVanilla />);
    expect(component).toMatchSnapshot();
  });
});
