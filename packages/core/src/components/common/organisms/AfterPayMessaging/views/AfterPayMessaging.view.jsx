// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles from '../styles/AfterPayMessaging.style';
import BodyCopy from '../../../atoms/BodyCopy';
import { getIconPath } from '../../../../../utils';
import { Modal } from '../../../molecules';

const getDynamicLabel = (label = '', replaceText, hideIntroText, replaceText1 = '') => {
  let labelText = label.replace(/#placeholder/, replaceText);
  if (replaceText1) {
    labelText = labelText.replace(/#placeholder1/, replaceText1);
  }
  return hideIntroText
    ? labelText.replace(/^(or|Or|in|In|Pay| pay| Pay in|pay in)/g, '')
    : labelText;
};

const AfterPayMessaging = (props) => {
  const {
    className,
    isAfterPayEnabled,
    offerPrice,
    afterPayMinAmount,
    afterPayLabels,
    cartHaveGCItem,
    isOrderThresholdEnabled,
    afterPayMinOrderAmount,
    afterPayMaxOrderAmount,
    hideIntroText,
  } = props;
  const orderMinAmount = isOrderThresholdEnabled ? afterPayMinOrderAmount : afterPayMinAmount;
  const showUpperLimit =
    !cartHaveGCItem && isOrderThresholdEnabled && +offerPrice > afterPayMaxOrderAmount;
  const showLowerLimit = !cartHaveGCItem && +offerPrice > orderMinAmount && !showUpperLimit;
  const dataUpperLimit = showUpperLimit ? afterPayMaxOrderAmount : '';
  const [infoModalOpen, setInfoModalOpen] = useState(false);
  const interestFreeLabel = getDynamicLabel(
    afterPayLabels.interestFreeText,
    (offerPrice / 4).toFixed(2),
    hideIntroText
  );
  const availableOverLabel = cartHaveGCItem
    ? afterPayLabels.notAvailableText
    : getDynamicLabel(
        showUpperLimit ? afterPayLabels.availableRangeText : afterPayLabels.availableOverText,
        orderMinAmount,
        false,
        dataUpperLimit
      );
  return isAfterPayEnabled && offerPrice ? (
    <div className={className} key={`afterPay_${Math.random()}`}>
      <div className="afterPayMessagingWrapper">
        <BodyCopy
          className="messaging-text-wrapper"
          fontFamily="secondary"
          fontSize="fs11"
          component="p"
        >
          {showLowerLimit && (
            <BodyCopy
              className="messaging-text"
              fontFamily="secondary"
              fontSize="fs11"
              component="span"
            >
              {interestFreeLabel.slice(0, 31)}
              <span className="boldmessaging-text">{interestFreeLabel.slice(31, 38)}</span>
              {interestFreeLabel.slice(38)}
            </BodyCopy>
          )}
          <BodyCopy className="afterPayImage" component="span">
            <img src={getIconPath('Afterpay_Badge_BlackonMint')} alt="after-pay" />
          </BodyCopy>
          {showLowerLimit ? (
            ' '
          ) : (
            <BodyCopy
              className="messaging-text"
              fontFamily="secondary"
              fontSize="fs11"
              component="span"
            >
              {availableOverLabel}
            </BodyCopy>
          )}
          <BodyCopy
            accessibilityRole="button"
            component="span"
            tabIndex="0"
            onClick={() => setInfoModalOpen(true)}
          >
            <img
              className="styledInfoIcon"
              width="12px"
              height="12px"
              src={getIconPath('info-icon')}
              alt="info"
            />
          </BodyCopy>
        </BodyCopy>
      </div>
      <Modal
        isOpen={infoModalOpen}
        overlayClassName="TCPModal__Overlay"
        className="TCPModal__Content"
        fixedWidth
        maxWidth="100vw"
        minHeight="520px"
        heightConfig={{ maxHeight: '100%', height: '660px' }}
        widthConfig={{ medium: '774px', large: '774px' }}
        noPadding
        onRequestClose={() => setInfoModalOpen(false)}
        stickyCloseIcon
        horizontalBar={false}
      >
        <iframe
          src="https://static.afterpay.com/modal/en_US-theme-white.html"
          title="Afterpay modal"
          width="100%"
          height="100%"
          scrolling="yes"
          frameBorder="0"
        />
      </Modal>
    </div>
  ) : null;
};

AfterPayMessaging.propTypes = {
  offerPrice: PropTypes.number,
  isAfterPayEnabled: PropTypes.bool.isRequired,
  afterPayMinAmount: PropTypes.string.isRequired,
  afterPayLabels: PropTypes.shape({}),
  cartHaveGCItem: PropTypes.bool,
  isOrderThresholdEnabled: PropTypes.bool,
  afterPayMinOrderAmount: PropTypes.number,
  hideIntroText: PropTypes.bool,
  afterPayMaxOrderAmount: PropTypes.number,
};

AfterPayMessaging.defaultProps = {
  offerPrice: 0,
  afterPayLabels: {},
  cartHaveGCItem: false,
  isOrderThresholdEnabled: false,
  afterPayMinOrderAmount: 0,
  hideIntroText: false,
  afterPayMaxOrderAmount: 0,
};

export default withStyles(AfterPayMessaging, styles);
export { AfterPayMessaging as AfterPayMessagingVanilla };
