/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { WebView } from 'react-native-webview';
import { Text } from 'react-native';
import { isIOS } from '@tcp/core/src/utils';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import {
  AfterPayMessagingWrapper,
  AfterPayModalWrapper,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  StyledText,
  AfterPayImage,
  StyledInfoIcon,
} from '../styles/AfterPayMessaging.style.native';

const AfterPayBadgeBlackOnMint = require('../../../../../../../mobileapp/src/assets/images/afterpay-badge-blackonmint.png');
const infoIcon = require('../../../../../../../mobileapp/src/assets/images/info-icon.png');
const closeIcon = require('../../../../../../../mobileapp/src/assets/images/close.png');

const getDynamicLabel = (label, replaceText, hideIntroText, replaceText1 = '') => {
  let labelText = label.replace(/#placeholder/, replaceText);
  if (replaceText1) {
    labelText = labelText.replace(/#placeholder1/, replaceText1);
  }
  return hideIntroText
    ? labelText.replace(/^(or|Or|in|In|Pay| pay| Pay in|pay in)/g, '')
    : labelText;
};

const AfterPayMessaging = (props) => {
  const {
    isAfterPayEnabled,
    offerPrice,
    afterPayMinAmount,
    afterPayLabels,
    cartHaveGCItem,
    isOrderThresholdEnabled,
    afterPayMinOrderAmount,
    afterPayMaxOrderAmount,
    hideIntroText,
    isCTL,
    isFromBundlePage,
  } = props;

  const orderMinAmount = isOrderThresholdEnabled ? afterPayMinOrderAmount : afterPayMinAmount;
  const showUpperLimit =
    !cartHaveGCItem && isOrderThresholdEnabled && +offerPrice > afterPayMaxOrderAmount;
  const showLowerLimit = !cartHaveGCItem && +offerPrice > orderMinAmount && !showUpperLimit;
  const dataUpperLimit = showUpperLimit ? afterPayMaxOrderAmount : '';
  const [infoModalOpen, setInfoModalOpen] = useState(false);
  const interestFreeLabel = getDynamicLabel(
    afterPayLabels.interestFreeText,
    (offerPrice / 4).toFixed(2),
    hideIntroText
  );
  const availableOverLabel = cartHaveGCItem
    ? afterPayLabels.notAvailableText
    : getDynamicLabel(
        showUpperLimit ? afterPayLabels.availableRangeText : afterPayLabels.availableOverText,
        orderMinAmount,
        false,
        dataUpperLimit
      );

  const label = interestFreeLabel;

  // TODO need to check offerprice
  return isAfterPayEnabled && offerPrice ? (
    <AfterPayMessagingWrapper isCTL={isCTL} isFromBundlePage={isFromBundlePage}>
      <Text key={`afterPay_${Math.random()}`}>
        {showLowerLimit && (
          <StyledText fontFamily="secondary" fontSize="fs12" color="gray.900" text={label} />
        )}
        <AfterPayImage
          source={AfterPayBadgeBlackOnMint}
          width="74px"
          height="15px"
          resizeMode="contain"
        />
        {showLowerLimit ? (
          ' '
        ) : (
          <StyledText
            fontFamily="secondary"
            fontSize="fs12"
            color="gray.900"
            text={availableOverLabel}
          />
        )}
        <Text accessibilityRole="button" onPress={() => setInfoModalOpen(true)}>
          <StyledInfoIcon width="12px" height="12px" source={infoIcon} alt="info" />
        </Text>
      </Text>
      <Modal
        isOpen={infoModalOpen}
        onRequestClose={() => setInfoModalOpen(false)}
        closeIconDataLocator="help-with-order-modal"
        customTransparent
        aria={{
          describedby: 'more-help-with-order',
        }}
      >
        <AfterPayModalWrapper>
          <ImageWrapper stickyCloseIcon>
            <StyledTouchableOpacity
              onPress={() => setInfoModalOpen(false)}
              accessibilityRole="button"
              accessibilityLabel="close"
              isOverlay
            >
              <StyledCrossImage rightAlignCrossIcon source={closeIcon} />
            </StyledTouchableOpacity>
          </ImageWrapper>
          <WebView
            androidHardwareAccelerationDisabled={true}
            source={{ uri: 'https://static.sandbox.afterpay.com/modal/en_US.html' }}
            startInLoadingState
            javaScriptEnabled
            originWhitelist={['*']}
            useWebKit={isIOS()}
            domStorageEnabled
          />
        </AfterPayModalWrapper>
      </Modal>
    </AfterPayMessagingWrapper>
  ) : null;
};

AfterPayMessaging.propTypes = {
  offerPrice: PropTypes.number,
  isAfterPayEnabled: PropTypes.bool.isRequired,
  afterPayMinAmount: PropTypes.string.isRequired,
  afterPayLabels: PropTypes.shape({}),
  cartHaveGCItem: PropTypes.bool,
  isOrderThresholdEnabled: PropTypes.bool,
  afterPayMinOrderAmount: PropTypes.number,
  hideIntroText: PropTypes.bool,
  afterPayMaxOrderAmount: PropTypes.number,
};

AfterPayMessaging.defaultProps = {
  offerPrice: 0,
  afterPayLabels: {},
  cartHaveGCItem: false,
  isOrderThresholdEnabled: false,
  afterPayMinOrderAmount: 0,
  hideIntroText: false,
  afterPayMaxOrderAmount: 0,
};

export default AfterPayMessaging;
export { AfterPayMessaging as AfterPayMessagingVanilla };
