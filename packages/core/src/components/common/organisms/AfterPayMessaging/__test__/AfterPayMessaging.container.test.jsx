// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AfterPayMessagingContainer } from '../container/AfterPayMessaging.container';
import AfterPayMessagingView from '../views';

describe('AfterPayMessagingContainer', () => {
  it('should render AfterPayMessagingView', () => {
    const component = shallow(<AfterPayMessagingContainer />);
    expect(component.is(AfterPayMessagingView)).toBeTruthy();
  });
});
