import styled from 'styled-components/native';
import { Dimensions } from 'react-native';
import Image from '@tcp/core/src/components/common/atoms/Image';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';

const dimensions = Dimensions.get('window');

const AfterPayMessagingWrapper = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  min-height: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  width: ${(props) => (props.isCTL || props.isFromBundlePage ? '175px' : 'auto')};
`;

const AfterPayModalWrapper = styled.View`
  height: ${dimensions.height};
  width: ${dimensions.width};
  background-color: ${(props) => props.theme.colors.AFTERPAY.MINT};
  padding-top: 50px;
`;

const ImageWrapper = styled.View`
  position: absolute;
  right: 0;
  z-index: 1;
  align-items: flex-end;
  justify-content: center;
  margin-top: 40px;
`;

const StyledTouchableOpacity = styled.TouchableOpacity`
  padding: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS}
    ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
    ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const StyledCrossImage = styled.Image`
  width: 20px;
  height: 20px;
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const StyledText = styled(BodyCopy)`
  line-height: 16;
`;

const StyledInfoIcon = styled(Image)`
  margin-left: 3px;
`;

const AfterPayImage = styled(Image)`
  margin-top: -3px;
`;

export {
  AfterPayMessagingWrapper,
  AfterPayModalWrapper,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  StyledText,
  StyledInfoIcon,
  AfterPayImage,
};
