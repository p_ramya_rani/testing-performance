// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  min-height: 25px;
  font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  /* stylelint-disable-next-line*/
  afterpay-placement {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      --logo-badge-width: 74px;
    }
  }

  .afterPayMessagingWrapper {
    display: flex;
    justify-content: ${(props) => (props.centered ? 'center' : 'flex-start')};
    align-items: center;
    flex-wrap: wrap;
    min-height: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
    ${(props) => (props.centered ? 'justify-content: center;' : '')}
  }

  .messaging-text {
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs11};
    }
  }
  .boldmessaging-text {
    font-weight: 800;
  }

  .afterPayModalWrapper {
    height: 100%;
    width: 100%;
    background-color: ${(props) => props.theme.colors.AFTERPAY.MINT};
    padding-top: 50px;
  }

  .imageWrapper {
    position: absolute;
    right: 0;
    z-index: 1;
    align-items: flex-end;
    justify-content: center;
    margin-top: 40px;
  }

  .styledText {
    line-height: 16px;
  }

  .styledInfoIcon {
    margin-left: 3px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: 2px;
    }
    cursor: pointer;
  }

  .afterPayImage {
    width: 84px;
    display: inline-block;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 64px;
    }
    img {
      vertical-align: middle;
    }
  }
`;

export default styles;
