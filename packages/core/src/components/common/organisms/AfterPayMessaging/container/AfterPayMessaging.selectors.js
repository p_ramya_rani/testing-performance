import { getLabelValue } from '@tcp/core/src/utils';

const getAfterPayLabels = (state) => {
  return {
    interestFreeText: getLabelValue(
      state.Labels,
      'lbl_bnpla_interest_free_text',
      'addedToBagModal',
      'global'
    ),
    availableOverText: getLabelValue(
      state.Labels,
      'lbl_bnpla_available_over_text',
      'addedToBagModal',
      'global'
    ),
    notAvailableText: getLabelValue(
      state.Labels,
      'lbl_bnpla_not_available_text',
      'addedToBagModal',
      'global'
    ),
    availableRangeText: getLabelValue(
      state.Labels,
      'lbl_bnpla_available_range_text',
      'addedToBagModal',
      'global'
    ),
  };
};

export default getAfterPayLabels;
