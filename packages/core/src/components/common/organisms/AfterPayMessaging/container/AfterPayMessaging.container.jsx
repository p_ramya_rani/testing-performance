// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  getIsAfterPayEnabled,
  getAfterPayMinAmount,
  getAfterPayMinOrderAmount,
  getAfterPayMaxOrderAmount,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import selectors from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import getAfterPayLabels from './AfterPayMessaging.selectors';
import AfterPayMessaging from '../views';

export class AfterPayMessagingContainer extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { offerPrice } = this.props;
    return nextProps.offerPrice !== offerPrice;
  }

  render() {
    return <AfterPayMessaging {...this.props} />;
  }
}

const mapStateToProps = (state) => {
  return {
    isAfterPayEnabled: getIsAfterPayEnabled(state),
    afterPayMinAmount: getAfterPayMinAmount(state),
    afterPayMinOrderAmount: getAfterPayMinOrderAmount(state),
    afterPayMaxOrderAmount: getAfterPayMaxOrderAmount(state),
    cartHaveGCItem: selectors.getIsCartHaveGiftCards(state),
    afterPayLabels: getAfterPayLabels(state),
  };
};

AfterPayMessagingContainer.propTypes = {
  offerPrice: PropTypes.number.isRequired,
};

export default connect(mapStateToProps)(AfterPayMessagingContainer);
