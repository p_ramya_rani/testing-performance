/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';

const ESSENTIALS = 'essentials';

const userSpecificModuleX = (userType, isPlcc, isLoggedIn) => {
  if (
    (userType === 'plcc' && isPlcc) ||
    (userType === 'mpr' && !isPlcc && isLoggedIn) ||
    (userType === 'guest' && !isLoggedIn)
  ) {
    return true;
  }
  return false;
};

const PromoModules = ({
  asPath,
  plpTopPromos,
  isLoggedIn,
  isPlcc,
  viaModule,
  showEssentials,
  dynamicPromoModules: modules,
  ...otherProps
  // eslint-disable-next-line sonarjs/cognitive-complexity
}) => {
  return (
    plpTopPromos &&
    plpTopPromos.length &&
    plpTopPromos.map((promo) => {
      const { contentId, moduleName, userType, data: slotData, ...others } = promo;
      if (!showEssentials && moduleName === ESSENTIALS) return null;
      const Module = modules[moduleName];
      // This is user specific moduleX - eg. For loyalty Banner on PLP
      if (userType && moduleName === 'moduleX') {
        const isUserSpecificModuleX = userSpecificModuleX(userType, isPlcc, isLoggedIn);
        if (isUserSpecificModuleX) {
          // eslint-disable-next-line prefer-destructuring, dot-notation
          const Espot = modules['Espot'];
          return (
            <Espot
              richTextHtml={
                promo.richTextList && promo.richTextList[0] && promo.richTextList[0].text
              }
            />
          );
        }
        return null;
      }
      return (
        Module && (
          <Module
            fullBleed
            key={contentId}
            data={promo}
            asPath={asPath}
            accessibility={otherProps.accessibilityLabels}
            viaModule={viaModule}
            {...slotData}
            {...others}
          />
        )
      );
    })
  );
};

PromoModules.propTypes = {
  asPath: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  isPlcc: PropTypes.bool,
  accessibilityLabels: PropTypes.shape({}),
  plpTopPromos: PropTypes.arrayOf(
    PropTypes.shape({
      // Only including the most important property
      moduleName: PropTypes.string,
    })
  ),
  viaModule: PropTypes.string,
  showEssentials: PropTypes.bool,
};

PromoModules.defaultProps = {
  asPath: '',
  plpTopPromos: [],
  isLoggedIn: false,
  isPlcc: false,
  accessibilityLabels: {},
  viaModule: '',
  showEssentials: false,
};

export default PromoModules;
