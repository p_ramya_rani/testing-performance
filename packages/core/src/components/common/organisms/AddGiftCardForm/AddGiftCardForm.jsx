// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import TextBox from '../../atoms/TextBox';
import Row from '../../atoms/Row';
import Col from '../../atoms/Col';
import BodyCopy from '../../atoms/BodyCopy';
import Button from '../../atoms/Button';
import createValidateMethod from '../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../utils/formValidation/validatorStandardConfig';
import Recaptcha from '../../molecules/recaptcha/recaptcha';
import InputCheckbox from '../../atoms/InputCheckbox';
import constants from '../../../features/account/Payment/AddGiftCard/AddGiftCard.constants';

class AddGiftCardForm extends React.PureComponent {
  componentDidUpdate(prevProps) {
    const { addGiftCardError } = this.props;
    if (addGiftCardError !== prevProps.addGiftCardError) {
      this.resetReCaptcha();
    }
  }

  handleSubmit = ({ giftCardNumber, cardPin, recaptchaToken }) => {
    const data = { giftCardNumber, cardPin, recaptchaToken };
    const { onAddGiftCardClick } = this.props;
    onAddGiftCardClick(data);
    this.resetReCaptcha();
  };

  handleRecaptchaVerify = (token) => {
    const { change } = this.props;
    change('recaptchaToken', token);
  };

  handleRecaptchaExpired = () => {
    const { change } = this.props;
    change('recaptchaToken', '');
  };

  resetReCaptcha = () => {
    if (this.recaptcha) {
      this.recaptcha.reset();
    }
    this.handleRecaptchaExpired();
    const { untouch } = this.props;
    untouch('recaptchaToken');
  };

  onCancelClick = () => {
    const { goBackToPayment } = this.props;
    return goBackToPayment();
  };

  attachReCaptchaRef = (ref) => {
    this.recaptcha = ref;
  };

  renderReCaptcha = () => {
    const { isRecapchaEnabled } = this.props;
    return (
      isRecapchaEnabled && (
        <div>
          <Row fullBleed>
            <Col ignoreGutter={{ small: true }} colSize={{ small: 4, medium: 2, large: 2 }}>
              <Recaptcha
                theme="light"
                type="image"
                size="normal"
                tabindex="0"
                className="card__recaptcha"
                verifyCallback={this.handleRecaptchaVerify}
                expiredCallback={this.handleRecaptchaExpired}
                dataLocator="gift-card-addcardrecaptchacheckbox"
                ref={this.attachReCaptchaRef}
              />
            </Col>
          </Row>
          <div>
            <Field
              component={TextBox}
              type="hidden"
              name="recaptchaToken"
              className="card__hidden"
              onChange={this.handleChange}
            />
          </div>
        </div>
      )
    );
  };

  renderSaveToAccount = (labels) => {
    const { saveToAccountEnabled, isRow } = this.props;
    return (
      isRow && (
        <Row
          fullBleed
          className={`elem-mb-XL savetoaccount ${saveToAccountEnabled ? 'elem-mt-MED' : ''}`}
        >
          {saveToAccountEnabled && (
            <Field
              dataLocator="gift-card-checkbox-field"
              name="saveToAccount"
              component={InputCheckbox}
              className="save-to-account"
              onChange={this.handleChange}
            >
              <BodyCopy
                dataLocator="gift-card-sav-to-account-heading-lbl"
                fontSize="fs16"
                fontFamily="secondary"
                fontWeight="regular"
              >
                {getLabelValue(labels, 'lbl_payment_saveToAccount')}
              </BodyCopy>
            </Field>
          )}
        </Row>
      )
    );
  };

  getColSize = (isRow) => {
    return isRow ? { small: 3, medium: 3, large: 3 } : { small: 4, medium: 3, large: 2 };
  };

  renderButtons = (isRow, labels, isLoading) => {
    return (
      <>
        <Col
          ignoreGutter={{ small: true }}
          colSize={this.getColSize(isRow)}
          className="card__btn"
          offsetLeft={!isRow ? { small: 1, medium: 1, large: 2 } : null}
          offsetRight={!isRow ? { small: 1, medium: 1, large: 0 } : null}
        >
          <Button
            buttonVariation="fixed-width"
            type="button"
            data-locator="gift-card-cancelbtn"
            onClick={this.onCancelClick}
          >
            {getLabelValue(labels, 'lbl_payment_cancelCard')}
          </Button>
        </Col>
        <Col
          ignoreGutter={{ small: true }}
          colSize={this.getColSize(isRow)}
          className="card__btn--medium"
          offsetLeft={!isRow ? { small: 1, medium: 0, large: 0 } : null}
          offsetRight={{ small: 1, medium: 0, large: 0 }}
        >
          <Button
            buttonVariation="fixed-width"
            fill="BLUE"
            type="submit"
            data-locator="gift-card-addcardbtn"
            disabled={isLoading}
            onClick={this.handleSubmitData}
          >
            {getLabelValue(labels, 'lbl_payment_addCard')}
          </Button>
        </Col>
      </>
    );
  };

  handleChange = () => {
    const { onClearError, addGiftCardError } = this.props;
    if (addGiftCardError) {
      onClearError();
    }
  };

  getColProps = () => {
    const { isRow, isFromReview } = this.props;
    if (!isFromReview) {
      return {
        cardNoWithIsRow: { colSize: { small: 6, medium: isRow ? 10 : 4, large: isRow ? 6 : 4 } },
        cardNoWithoutRow: { colSize: { small: 6, medium: 2, large: 3 } },
        cardPin: { colSize: { small: 6, medium: 8, large: 6 } },
      };
    }
    return {
      cardNoWithIsRow: { colSize: { small: 6, medium: 8, large: 12 } },
      cardPin: { colSize: { small: 6, medium: 8, large: 12 } },
    };
  };

  handleSubmitData = (e) => {
    const { handleSubmit } = this.props;
    e.preventDefault();
    e.stopPropagation();
    handleSubmit(this.handleSubmit.bind(this))(e);
  };

  render() {
    const { labels, isLoading, isRow, isFromReview } = this.props;
    const colProps = this.getColProps();
    return (
      <form onSubmit={this.handleSubmitData}>
        <Row fullBleed className="elem-mb-MED">
          <Col
            ignoreGutter={{ small: true }}
            {...colProps.cardNoWithIsRow}
            className="giftCardNumberBox elem-mb-XL"
          >
            <Field
              placeholder={getLabelValue(labels, 'lbl_payment_giftCardNoPlaceholder')}
              name="giftCardNumber"
              type="tel"
              component={TextBox}
              maxLength={50}
              dataLocator="gift-card-cardnumberfield"
              className="giftCardNumber"
              onChange={this.handleChange}
              id="giftCardNumber"
              autoComplete="off"
            />
          </Col>
          {!isRow && (
            <Col
              ignoreGutter={{ small: true }}
              {...colProps.cardNoWithoutRow}
              className="cardPinBox"
            >
              <Field
                placeholder={getLabelValue(labels, 'lbl_payment_giftCardPinPlaceholder')}
                name="cardPin"
                type="tel"
                component={TextBox}
                dataLocator="gift-card-pinnumberfield"
                onChange={this.handleChange}
                id="cardPin"
                autoComplete="off"
              />
            </Col>
          )}
        </Row>
        {isRow && (
          <Row fullBleed className="elem-mb-XL">
            <Col ignoreGutter={{ small: true }} {...colProps.cardPin}>
              <Field
                placeholder={getLabelValue(labels, 'lbl_payment_giftCardPinPlaceholder')}
                name="cardPin"
                type="tel"
                component={TextBox}
                dataLocator="gift-card-pinnumberfield"
                className="cardPin"
                onChange={this.handleChange}
                id="cardPin"
                autoComplete="off"
              />
            </Col>
          </Row>
        )}

        {this.renderReCaptcha()}

        {this.renderSaveToAccount(labels)}

        <Row fullBleed className="card__row">
          {!isRow && (
            <Col ignoreGutter={{ small: true }} colSize={{ small: 6, medium: 8, large: 6 }}>
              <div className="card__msgWrapper">
                <BodyCopy
                  tag="p"
                  fontWeight="bold"
                  className="card__msg--bold elem-mb-XS"
                  dataLocator="git-card-headertext"
                  fontFamily="secondary"
                >
                  {getLabelValue(labels, 'lbl_payment_giftCardMessageHeading')}
                </BodyCopy>
                <BodyCopy
                  tag="p"
                  className="card__msg"
                  dataLocator="git-card-messagetext"
                  fontFamily="secondary"
                >
                  {getLabelValue(labels, 'lbl_payment_giftCardMessageDescription')}
                </BodyCopy>
              </div>
            </Col>
          )}

          {this.renderButtons(isRow, labels, isLoading, isFromReview)}
        </Row>
      </form>
    );
  }
}

const validateMethod = createValidateMethod(
  getStandardConfig(['giftCardNumber', 'cardPin', 'recaptchaToken'])
);

export default reduxForm({
  form: constants.ADD_GIFT_CARD_FORM, // a unique identifier for this form
  ...validateMethod,
  enableReinitialize: true,
})(AddGiftCardForm);

AddGiftCardForm.propTypes = {
  onAddGiftCardClick: PropTypes.func.isRequired,
  change: PropTypes.bool.isRequired,
  untouch: PropTypes.bool.isRequired,
  goBackToPayment: PropTypes.func.isRequired,
  isRecapchaEnabled: PropTypes.bool.isRequired,
  saveToAccountEnabled: PropTypes.func.isRequired,
  isRow: PropTypes.bool.isRequired,
  onClear: PropTypes.func.isRequired,
  addGiftCard: PropTypes.func.isRequired,
  isFromReview: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  isLoading: PropTypes.bool.isRequired,
  addGiftCardError: PropTypes.func.isRequired,
  onClearError: PropTypes.func.isRequired,
};

export { AddGiftCardForm as AddGiftCardFormVanilla };
