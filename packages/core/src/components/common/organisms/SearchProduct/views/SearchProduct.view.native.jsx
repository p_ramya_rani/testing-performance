// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Modal, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { BodyCopy, Button } from '../../../atoms';
import CustomIcon from '../../../atoms/Icon';
import QRScannerIcon from '../../../atoms/QRScannerIcon';
import Image from '../../../atoms/Image';
import { ICON_NAME, ICON_FONT_CLASS } from '../../../atoms/Icon/Icon.constants';
import SearchContainer, {
  HeaderContainer,
  ItemContainer,
  TextInput,
  SafeAreaView,
  CloseButton,
  SearchTextContainer,
  SectionListView,
  FooterView,
} from '../SearchProduct.style.native';
import { getSearchResult, getPopularSearch } from '../../../molecules/SearchBar/SearchBar.actions';
import { getLabelValue, isAndroid } from '../../../../../utils/index.native';
import { setRecentSearch, clearRecentSearch } from '../RecentSearch.actions';
import getRecentSearchesData from '../RecentSearches.selectors';

const searchIcon = require('../../../../../../../mobileapp/src/assets/images/search_icon_app.png');

class SearchProduct extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      showRecentSearches: true,
      searchText: '',
      extraDataForSearch: '',
      listData: [],
      popularData: [],
    };
    this.isAndroid = isAndroid();
  }

  componentDidMount() {
    const { popularSearch, labels } = this.props;

    popularSearch({
      slpLabels: labels,
    });
  }

  /* eslint-disable-next-line */
  UNSAFE_componentWillReceiveProps(nextProps) {
    const { searchResults, popularResults } = nextProps;
    const {
      searchResults: prevSearchResults,
      labels: slpLabels,
      popularResults: prevPopularResults,
    } = this.props;

    if (searchResults && searchResults !== prevSearchResults) {
      const { autosuggestList } = searchResults;
      const lookingForLabel = getLabelValue(slpLabels, 'lbl_search_looking_for');
      const lookingForData = autosuggestList.find(data => data.heading === lookingForLabel);
      const { suggestions } = lookingForData;

      const suggestionsCapitalize = suggestions.map(obj => {
        const newObj = { ...obj };
        newObj.text = obj.text.charAt(0).toUpperCase() + obj.text.slice(1).toLowerCase();
        return newObj;
      });

      const listData =
        suggestionsCapitalize.length > 0
          ? [
              {
                title: lookingForLabel,
                data: suggestionsCapitalize,
              },
            ]
          : [];

      // sets data with looking for results
      this.setState({
        listData,
      });
    } else if (popularResults && popularResults !== prevPopularResults) {
      const { autosuggestList } = popularResults;
      const lookingForLabel = getLabelValue(slpLabels, 'lbl_popular_search');
      const lookingForData = autosuggestList.find(data => data.heading === lookingForLabel);
      const { suggestions } = lookingForData;

      const popularData =
        suggestions.length > 0
          ? [
              {
                title: lookingForLabel,
                data: suggestions,
              },
            ]
          : [];

      // sets data with looking for results
      this.setState({
        popularData,
      });
    }
  }

  /**
   * @function closeModal
   * closes search modal
   *
   * @memberof SearchProduct
   */
  closeModal = () => {
    this.setState({ modalVisible: false }, () => {
      const { closeSearchModal } = this.props;
      if (closeSearchModal) closeSearchModal();
    });
  };

  /**
   * @function onChangeText
   * called when text changes on search bar
   *
   * @memberof SearchProduct
   */
  onChangeText = text => {
    const { startSearch, labels } = this.props;
    const { searchText } = this.state;

    // should start search only after 2 characters are entered
    const shouldSearch = text.length >= 2 || searchText.length > text.length;
    if (startSearch && shouldSearch)
      startSearch({
        searchText: text.trim(),
        slpLabels: labels,
      });

    this.setState({
      searchText: text,
      extraDataForSearch: shouldSearch ? text : '',
      showRecentSearches: text.length < 2,
    });
  };

  /**
   * @function searchProducts
   * navigates to search result page with search text in param
   *
   * @memberof SearchProduct
   */
  searchProducts = (name, typeahead) => {
    this.setState({ modalVisible: false }, () => {
      const { searchText } = this.state;
      const { goToSearchResultsPage, setRecentSearches } = this.props;
      const selectedSearchResult = name.trim() || searchText.trim();
      if (setRecentSearches) setRecentSearches(selectedSearchResult);
      if (goToSearchResultsPage) goToSearchResultsPage(selectedSearchResult, typeahead);
    });
  };

  /**
   * @function clearSearchText
   * clears search text
   *
   * @memberof SearchProduct
   */
  clearSearchText = () => {
    this.onChangeText('');
  };

  /**
   * @function clearRecentSearch
   * clears search text
   *
   * @memberof SearchProduct
   */
  clearRecentSearch = () => {
    const { clearRecentSearches } = this.props;
    clearRecentSearches();
  };

  /**
   * @function renderSearchContainer
   * renders search container with search bar
   *
   * @memberof SearchProduct
   */
  renderSearchContainer = () => {
    const { labels: slpLabels } = this.props;
    const cancelStyle = { borderWidth: 0, marginTop: 3 };
    const canceTextStyle = { textTransform: 'none', fontWeight: 'normal' };
    const { searchText } = this.state;
    const { navigation } = this.props;
    const lookingForLabel = getLabelValue(slpLabels, 'lbl_what_looking_for');
    const cancelLabel = getLabelValue(slpLabels, 'lbl_cancel_search');

    return (
      <SearchContainer isAndroid={this.isAndroid}>
        <SearchTextContainer>
          <Image source={searchIcon} height={14} width={14} />
          <TextInput
            placeholder={lookingForLabel}
            returnKeyType="search"
            onChangeText={this.onChangeText}
            autoFocus
            value={searchText}
            onSubmitEditing={() => {
              const { searchText: text } = this.state;
              this.searchProducts(text);
            }}
          />
          {searchText.length > 0 ? (
            <CloseButton onPress={this.clearSearchText}>
              <CustomIcon
                iconFontName={ICON_FONT_CLASS.Icomoon}
                name={ICON_NAME.large}
                size="fs10"
                color="gray.900"
                accessibilityLabel="close"
              />
            </CloseButton>
          ) : (
            <QRScannerIcon
              height={16}
              width={16}
              navigation={navigation}
              closeModal={this.closeModal}
              isDisabled={false}
            />
          )}
        </SearchTextContainer>
        <Button
          buttonVariation="fixed-width"
          text={cancelLabel}
          fontSize="fs13"
          fontWeight="regular"
          fontFamily="secondary"
          onPress={this.closeModal}
          customTextStyle={canceTextStyle}
          style={cancelStyle}
        />
      </SearchContainer>
    );
  };

  /**
   * @function renderSectionHeader
   * renders section header with different titles
   *
   * @memberof SearchProduct
   */
  renderSectionHeader = ({ section: { title } }) => {
    const { listData } = this.state;
    const cancelStyle = { borderWidth: 0, marginTop: 0 };
    const canceTextStyle = { textTransform: 'none', fontWeight: 'normal' };
    const { labels: slpLabels } = this.props;
    const clearAllLabel = getLabelValue(slpLabels, 'lbl_clear_recent_search');
    const recentSearchesData = this.recentSearchesListData();
    const isPadding = listData.length <= 0;

    return (
      <HeaderContainer isPadding={isPadding}>
        <BodyCopy
          fontWeight="bold"
          color="black"
          mobileFontFamily="primary"
          fontSize="fs12"
          text={title.toUpperCase()}
        />
        {recentSearchesData.length > 0 && (
          <Button
            buttonVariation="fixed-width"
            text={clearAllLabel}
            fontSize="fs10"
            fontWeight="regular"
            fontFamily="secondary"
            onPress={this.clearRecentSearch}
            customTextStyle={canceTextStyle}
            style={cancelStyle}
          />
        )}
      </HeaderContainer>
    );
  };

  /**
   * @function renderItem
   * renders an item in list
   *
   * @memberof SearchProduct
   */
  renderItem = ({ item }) => {
    const { listData } = this.state;
    const { text } = item;
    const { searchText } = this.state;
    const indexOfSearchText = text.toLowerCase().indexOf(searchText.toLowerCase());
    const searchTextAvailable = indexOfSearchText > -1 && searchText.length > 0;
    let prefix = '';
    let suffix = '';
    let current = text;
    let currentFontWeight = 'regular';
    const isPadding = listData.length <= 0;

    if (searchTextAvailable) {
      prefix = text.substring(0, indexOfSearchText);
      suffix = text.substring(indexOfSearchText + searchText.length);
      current = text.substring(indexOfSearchText, indexOfSearchText + searchText.length);
      currentFontWeight = 'semibold';
    }

    return (
      <ItemContainer isPadding={isPadding} onPress={() => this.searchProducts(text, 'typeahead')}>
        {prefix.length > 0 && (
          <BodyCopy
            fontWeight="regular"
            color="gray.900"
            mobileFontFamily="secondary"
            fontSize="fs13"
            text={prefix}
          />
        )}
        <BodyCopy
          fontWeight={currentFontWeight}
          color="gray.900"
          mobileFontFamily="secondary"
          fontSize="fs13"
          text={current}
        />
        {suffix.length > 0 && (
          <BodyCopy
            fontWeight="regular"
            color="gray.900"
            mobileFontFamily="secondary"
            fontSize="fs13"
            text={suffix}
          />
        )}
      </ItemContainer>
    );
  };

  /**
   * @function keyExtractor
   * returns key for list data
   *
   * @memberof SearchProduct
   */
  keyExtractor = (_, index) => index.toString();

  recentSearchesListData = () => {
    const { recentSearches, labels: slpLabels } = this.props;
    const recentSearchesLabel = getLabelValue(slpLabels, 'lbl_search_recent_search');
    return recentSearches && recentSearches.length > 0
      ? [{ title: recentSearchesLabel, data: recentSearches }]
      : [];
  };

  getData = () => {
    const { listData, popularData } = this.state;
    if (listData.length > 0) {
      return listData;
    }
    return popularData;
  };

  render() {
    const { modalVisible, extraDataForSearch, listData, showRecentSearches } = this.state;
    const recentSearchesData = this.recentSearchesListData();
    const data =
      showRecentSearches && recentSearchesData.length > 0 ? recentSearchesData : this.getData();
    const containerStyle = { flex: 1 };
    const isPadding = listData.length <= 0;

    return (
      <Modal
        animationType="fade"
        transparent={false}
        visible={modalVisible}
        onRequestClose={this.closeModal}
      >
        <SafeAreaView forceInset={{ top: 'always' }}>
          <KeyboardAvoidingView behavior={this.isAndroid ? null : 'padding'} style={containerStyle}>
            {this.renderSearchContainer()}
            {data.length > 0 && (
              <SectionListView
                sections={data}
                initialNumToRender={data.length}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}
                renderSectionHeader={this.renderSectionHeader}
                keyboardShouldPersistTaps="handled"
                stickySectionHeadersEnabled={false}
                extraData={extraDataForSearch}
                ListFooterComponent={<FooterView isPadding={isPadding} />}
                isPadding={isPadding}
                showsVerticalScrollIndicator={false}
              />
            )}
          </KeyboardAvoidingView>
        </SafeAreaView>
      </Modal>
    );
  }
}

/* PropTypes declaration */
SearchProduct.propTypes = {
  closeSearchModal: PropTypes.func,
  goToSearchResultsPage: PropTypes.func,
  startSearch: PropTypes.func,
  labels: PropTypes.instanceOf(Object),
  searchResults: PropTypes.instanceOf(Object),
  recentSearches: PropTypes.arrayOf(Object),
  setRecentSearches: PropTypes.func,
  clearRecentSearches: PropTypes.func,
  navigation: PropTypes.shape({}).isRequired,
  popularSearch: PropTypes.func,
  popularResults: PropTypes.instanceOf(Object),
};

SearchProduct.defaultProps = {
  closeSearchModal: null,
  goToSearchResultsPage: null,
  startSearch: null,
  labels: null,
  searchResults: {},
  recentSearches: [],
  setRecentSearches: null,
  clearRecentSearches: null,
  popularSearch: null,
  popularResults: {},
};

const mapStateToProps = state => {
  return {
    labels: state.Labels.global && state.Labels.global.Search,
    searchResults: state.Search && state.Search.searchResults,
    recentSearches: getRecentSearchesData(state),
    popularResults: state.Search && state.Search.popularResults,
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    startSearch: searchTerm => {
      dispatch(getSearchResult(searchTerm));
    },
    setRecentSearches: searchTerm => {
      dispatch(setRecentSearch({ searchTerm }));
    },
    clearRecentSearches: () => {
      dispatch(clearRecentSearch());
    },
    popularSearch: data => {
      dispatch(getPopularSearch(data));
    },
  };
};

/* Export */
export { SearchProduct as SearchProductVanilla };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchProduct);
