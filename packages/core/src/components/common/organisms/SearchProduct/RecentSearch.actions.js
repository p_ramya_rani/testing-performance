// 9fbef606107a605d69c0edbcd8029e5d 
import RECENT_SEARCH_CONSTANTS from './RecentSearch.constants';

const setRecentSearch = payload => ({
  type: RECENT_SEARCH_CONSTANTS.SET_RECENT_SEARCH,
  payload,
});

const clearRecentSearch = () => ({
  type: RECENT_SEARCH_CONSTANTS.CLEAR_RECENT_SEARCH,
});

const setRecentSearchData = payload => ({
  type: RECENT_SEARCH_CONSTANTS.SET_RECENT_SEARCH_DATA,
  payload,
});

export { setRecentSearchData, setRecentSearch, clearRecentSearch };
