// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

const SearchContainer = styled.View`
  flex-direction: row;
  height: ${props => (props.isAndroid ? '65px' : '50px')};
  padding-top: ${props => (props.isAndroid ? '15px' : 0)};
  padding-left: ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
  padding-right: ${props => props.theme.spacing.ELEM_SPACING.XXS};
`;

const SearchTextContainer = styled.View`
  flex: 1;
  flex-direction: row;
  height: 40px;
  background-color: ${props => props.theme.colorPalette.white};
  border-radius: 20px;
  align-items: center;
  padding-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
  padding-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
  box-shadow: 0 2px 6px ${props => props.theme.colors.BOX_SHADOW_LIGHT};
  justify-content: space-between;
`;

const HeaderContainer = styled.View`
  flex-direction: row;
  height: 40px;
  align-items: center;
  justify-content: space-between;
  padding-left: ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
  border-top-left-radius: ${props => (props.isPadding ? '11px' : 0)};
  border-top-right-radius: ${props => (props.isPadding ? '11px' : 0)};
  background-color: ${props => props.theme.colorPalette.white};
`;

const ItemContainer = styled.TouchableOpacity`
  height: 40px;
  padding-left: ${props =>
    props.isPadding
      ? props.theme.spacing.LAYOUT_SPACING.XXS
      : props.theme.spacing.LAYOUT_SPACING.MED};
  align-items: center;
  flex-direction: row;
  background-color: ${props => props.theme.colorPalette.white};
`;

const TextInput = styled.TextInput`
  padding-left: 6px;
  padding-right: 12px;
  font-size: ${props => props.theme.typography.fontSizes.fs12};
  font-family: ${props => props.theme.typography.fonts.secondary};
  font-weight: ${props => props.theme.typography.fontWeights.regular};
  color: ${props => props.theme.colorPalette.gray[900]};
  width: 85%;
`;

const SafeAreaView = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.colorPalette.gray[300]};
`;

const FooterView = styled.View`
  background-color: ${props => props.theme.colorPalette.white};
  border-bottom-left-radius: ${props => (props.isPadding ? '11px' : 0)};
  border-bottom-right-radius: ${props => (props.isPadding ? '11px' : 0)};
  height: 20px;
`;

const SectionListView = styled.SectionList`
  background-color: ${props => props.theme.colorPalette.gray[300]};
  margin-left: ${props => (props.isPadding ? props.theme.spacing.ELEM_SPACING.SM : 0)};
  margin-right: ${props => (props.isPadding ? props.theme.spacing.ELEM_SPACING.SM : 0)};
  box-shadow: 0 2px 6px ${props => props.theme.colors.BOX_SHADOW_LIGHT};
`;

const CloseButton = styled.TouchableOpacity`
  width: 25px;
  height: 25px;
  align-items: center;
  justify-content: center;
`;

export default SearchContainer;
export {
  HeaderContainer,
  ItemContainer,
  TextInput,
  SafeAreaView,
  CloseButton,
  SearchTextContainer,
  SectionListView,
  FooterView,
};
