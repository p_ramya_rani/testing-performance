// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { RECENT_SEARCH_REDUCER_KEY } from '../../../../constants/reducer.constants';

const getRecentSearchesState = state => {
  return state[RECENT_SEARCH_REDUCER_KEY];
};

const getRecentSearchesData = createSelector(
  getRecentSearchesState,
  recentSearches => recentSearches && recentSearches.searchTermList
);

export default getRecentSearchesData;
