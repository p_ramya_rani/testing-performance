// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { HelpMenuVanilla } from '../views/HelpMenu.view';

describe('Help Menu component', () => {
  const props = {
    className: 'help-menu',
    helpWithOrderLabels: {
      returnPolicyText: 'Return Policy',
      returnPolicyUrl: '/help-center/returns-exchanges#returnsandexchanges',
      orderStatusHelpText: 'Contact Us About Your Order',
      orderStatusHelpUrl: '/content/order-status-help',
    },
    helpMenuLabels: {
      helpCenterUrl: '/help-center/faq',
      helpCenterText: 'Help Center',
      orderStatusUrl: '/help-center/order-help#orderstatus',
      orderStatusText: 'Order Status',
      leaveFeedbackUrl: '/test',
      leaveFeedbackText: 'Leave Feedback',
      needHelpText: 'Need help?',
      needHelpImg:
        'https://test1.theplace.com/image/upload/v1595348478/ecom/assets/static/detail-menu/clock_3x.png',
    },
    isLoggedIn: false,
    openTrackOrderOverlay: jest.fn().mockImplementation(() => {}),
    router: {
      beforePopState: jest.fn(),
    },
  };

  it('Help Menu component renders correctly', () => {
    const component = shallow(<HelpMenuVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Help Menu component renders correctly', () => {
    const component = shallow(<HelpMenuVanilla {...props} />);
    component.setState({ flyoutOpen: true });
    expect(component.state('flyoutOpen')).toBe(true);
    const evt = document.createEvent('HTMLEvents');
    evt.initEvent('click', false, true);
    document.body.click();
    expect(component.state('flyoutOpen')).toBe(false);
  });
});
