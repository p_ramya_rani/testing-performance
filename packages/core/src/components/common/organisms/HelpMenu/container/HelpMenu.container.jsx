// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { setTrackOrderModalMountedState } from '@tcp/core/src/components/features/account/TrackOrder/container/TrackOrder.actions';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getStickyATBState } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import { getOrderDetailsDataState } from '@tcp/core/src/components/features/account/OrderDetails/container/OrderDetails.selectors';
import {
  getABTestForChatBot,
  getIsCSHEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { selectOrder } from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.actions';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import { getHelpWithOrdersLabels } from '../../HelpWithOrder/container/HelpWithOrder.selectors';
import { getHelpMenuLabels } from './HelpMenu.selectors';
import { mergeAccountReset } from '../../../../features/account/CustomerHelp/organisms/MergeAccounts/container/MergeAccounts.actions';
import { resetEmailConfirmationRequest } from '../../../../features/account/CustomerHelp/organisms/EmailConfirmation/container/EmailConfirmation.action';
import HelpMenuView from '../views/HelpMenu.view';

const mapStateToProps = (state) => {
  return {
    helpWithOrderLabels: getHelpWithOrdersLabels(state),
    helpMenuLabels: getHelpMenuLabels(state),
    isLoggedIn: getUserLoggedInState(state),
    stickyFooter: getStickyATBState(state),
    orderDetails: getOrderDetailsDataState(state),
    hasABTestForChatBot: getABTestForChatBot(state),
    isCSHEnabled: getIsCSHEnabled(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    openTrackOrderOverlay: (payload) => dispatch(setTrackOrderModalMountedState(payload)),
    selectOrderAction: () => dispatch(selectOrder({})),
    trackAnalyticsClick: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 100);
    },
    mergeAccountReset: () => {
      dispatch(mergeAccountReset());
    },
    resetEmailConfirmationRequest: () => {
      dispatch(resetEmailConfirmationRequest());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HelpMenuView);
