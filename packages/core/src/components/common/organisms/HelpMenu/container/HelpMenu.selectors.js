// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';

const getLabels = (state) => {
  return state && state.Labels;
};

export const getHelpMenuLabels = createSelector(getLabels, (labels) => {
  return {
    helpCenterUrl: getLabelValue(labels, 'lbl_help_center_url', 'orderConfirmation', 'checkout'),
    helpCenterText: getLabelValue(labels, 'lbl_help_center_text', 'helpCenter', 'global'),
    orderStatusUrl: getLabelValue(labels, 'lbl_order_status_url', 'orderConfirmation', 'checkout'),
    orderStatusText: getLabelValue(labels, 'lbl_order_status_text', 'helpCenter', 'global'),
    orderStatusContactUrl: getLabelValue(
      labels,
      'lbl_order_status_help_url',
      'orderConfirmation',
      'checkout'
    ),
    orderStatusContactText: getLabelValue(
      labels,
      'lbl_order_status_help_text',
      'helpCenter',
      'global'
    ),
    customerSupportText: getLabelValue(labels, 'lbl_customer_support', 'helpCenter', 'global'),
    leaveFeedbackUrl: getLabelValue(
      labels,
      'lbl_leave_feedback_url',
      'orderConfirmation',
      'checkout'
    ),
    orderAssistanceText: getLabelValue(labels, 'lbl_order_help', 'helpCenter', 'global'),
    leaveFeedbackText: getLabelValue(labels, 'lbl_leave_feedback_text', 'helpCenter', 'global'),
    helpText: getLabelValue(labels, 'lbl_overview_help', 'helpCenter', 'global'),
    closeText: getLabelValue(labels, 'closeIconButton', 'accessibility', 'global'),
  };
});

export default { getHelpMenuLabels };
