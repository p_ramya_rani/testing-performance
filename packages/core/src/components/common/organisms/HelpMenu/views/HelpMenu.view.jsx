// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'next/router';
import { isClient, getSessionStorage, parseBoolean } from '@tcp/core/src/utils';
import HelpChat from '@tcp/core/src/components/common/atoms/HelpChatButton';
import HelpNav from '@tcp/core/src/components/common/molecules/HelpNav';
import { readCookie } from '@tcp/core/src/utils/cookie.util';

class HelpMenu extends PureComponent {
  constructor(props) {
    super(props);

    const chatBotStarted = getSessionStorage('BotStarted') || '';
    const chatBotMinimized = getSessionStorage('botMinimized') || '';
    const isChatBotOpen = parseBoolean(chatBotStarted) && !parseBoolean(chatBotMinimized);
    const fromMobileAppCookie = readCookie('fromMobileApp') || '';
    this.state = {
      flyoutOpen: false,
      chatNowTriggered: isChatBotOpen,
      isMobileAppEnabled: fromMobileAppCookie,
      chatWindowMinimized: false,
      unreadMessage: 0,
      liveChatActive: false,
    };
  }

  toggleMenu = () => {
    const { flyoutOpen: currentFlyoutState, chatWindowMinimized, liveChatActive } = this.state;
    if (liveChatActive && chatWindowMinimized) {
      this.setState({
        flyoutOpen: false,
      });
      window.launchBot();
    } else {
      this.setState({
        flyoutOpen: !currentFlyoutState,
      });
    }
  };

  onBodyClick = (event) => {
    if (
      event &&
      event.target &&
      (event.target.id === 'launcherminimized' || event.target.id === 'launcher')
    ) {
      this.setState({
        chatNowTriggered: true,
      });
    }
    const { flyoutOpen: currentFlyoutState } = this.state;
    if (currentFlyoutState) {
      this.toggleMenu();
    }
  };

  getEventData = (event) => event && event.data && JSON.parse(event.data);

  getSessionStorageData = (text) => text && JSON.parse(window.sessionStorage.getItem(text));

  chatMinimize = () => {
    const botMinimized = this.getSessionStorageData('botMinimized');
    const BotStarted = this.getSessionStorageData('BotStarted');
    const unreadCount = this.getSessionStorageData('unreadCount');
    const chatWindowMinimizedValue = !!(botMinimized && BotStarted);
    this.setState({
      chatWindowMinimized: chatWindowMinimizedValue,
      unreadMessage: unreadCount,
    });
  };

  liveChat = (message) => {
    const messageCommand = message && message.command ? message.command : null;
    if (
      messageCommand === 'launchBot' ||
      messageCommand === 'CSRMessage' ||
      messageCommand === 'minimizeFromAdvisor'
    ) {
      setTimeout(() => {
        this.chatMinimize();
      }, 10);
    }
    if (messageCommand === 'minimizeFromAdvisor') {
      this.setState({
        chatNowTriggered: false,
      });
    }
    const liveChatActive = localStorage.getItem('chat-session-connected')
      ? JSON.parse(localStorage.getItem('chat-session-connected'))
      : false;
    this.setState({
      liveChatActive,
    });
  };

  componentDidMount = () => {
    const { router } = this.props;
    if (isClient()) {
      document.body.addEventListener('click', this.onBodyClick, { capture: true });
      if (window.addEventListener) {
        window.addEventListener(
          'message',
          (event) => {
            try {
              const message = this.getEventData(event);
              this.liveChat(message);
            } catch (e) {
              // do nothing - no state change required
            }
          },
          { capture: true }
        );
        const onlineHelpDiv = document.querySelector('#OnlineHelp');
        if (onlineHelpDiv) {
          onlineHelpDiv.addEventListener(
            'click',
            () => {
              this.setState({
                chatNowTriggered: false,
              });
            },
            { capture: true }
          );
        }
      }
      router.beforePopState(() => {
        this.setState({
          flyoutOpen: false,
        });
        return true;
      });
    }
  };

  componentWillUnmount = () => {
    document.body.removeEventListener('click', this.onBodyClick);
  };

  render() {
    const {
      className,
      helpMenuLabels,
      helpWithOrderLabels,
      openTrackOrderOverlay,
      isLoggedIn,
      stickyFooter,
      orderDetails,
      router,
      hideHelp,
      hasABTestForChatBot,
      isCSHEnabled,
      selectOrderAction,
      trackAnalyticsClick,
      mergeAccountReset,
      resetEmailConfirmationRequest,
    } = this.props;
    const {
      flyoutOpen,
      chatNowTriggered,
      isMobileAppEnabled,
      chatWindowMinimized,
      unreadMessage,
      liveChatActive,
    } = this.state;
    const orderDetailsSticky =
      router.asPath &&
      router.asPath.indexOf('/account/orders/order-details') !== -1 &&
      orderDetails;

    return (
      <div className={className}>
        <HelpNav
          flyoutOpen={flyoutOpen}
          helpMenuLabels={helpMenuLabels}
          helpWithOrderLabels={helpWithOrderLabels}
          openTrackOrderOverlay={openTrackOrderOverlay}
          isLoggedIn={isLoggedIn}
          stickyFooter={stickyFooter}
          orderDetailsSticky={orderDetailsSticky}
          hasABTestForChatBot={hasABTestForChatBot}
          isCSHEnabled={isCSHEnabled}
          selectOrderAction={selectOrderAction}
          trackAnalyticsClick={trackAnalyticsClick}
          mergeAccountReset={mergeAccountReset}
          resetEmailConfirmationRequest={resetEmailConfirmationRequest}
        />
        <HelpChat
          toggleMenu={this.toggleMenu}
          flyoutOpen={flyoutOpen}
          helpMenuLabels={helpMenuLabels}
          stickyFooter={stickyFooter}
          orderDetailsSticky={orderDetailsSticky}
          isChatBotOpen={chatNowTriggered}
          hideHelp={hideHelp || isMobileAppEnabled}
          hasABTestForChatBot={hasABTestForChatBot}
          chatWindowMinimized={chatWindowMinimized}
          unreadMessage={unreadMessage}
          liveChatActive={liveChatActive}
        />
      </div>
    );
  }
}

HelpMenu.propTypes = {
  helpWithOrderLabels: PropTypes.shape({}),
  className: PropTypes.string,
  helpMenuLabels: PropTypes.shape({}),
  openTrackOrderOverlay: PropTypes.func,
  isLoggedIn: PropTypes.bool.isRequired,
  stickyFooter: PropTypes.bool,
  orderDetails: PropTypes.shape({}),
  router: PropTypes.shape({}),
  hideHelp: PropTypes.bool,
  hasABTestForChatBot: PropTypes.bool,
  isCSHEnabled: PropTypes.bool,
  selectOrderAction: PropTypes.func.isRequired,
  trackAnalyticsClick: PropTypes.func,
  mergeAccountReset: PropTypes.func,
  resetEmailConfirmationRequest: PropTypes.func,
};

HelpMenu.defaultProps = {
  helpWithOrderLabels: {},
  className: '',
  helpMenuLabels: {},
  openTrackOrderOverlay: null,
  stickyFooter: false,
  orderDetails: {},
  router: {},
  hideHelp: false,
  hasABTestForChatBot: false,
  isCSHEnabled: false,
  trackAnalyticsClick: () => {},
  mergeAccountReset: () => {},
  resetEmailConfirmationRequest: () => {},
};

export default withRouter(HelpMenu);
export { HelpMenu as HelpMenuVanilla };
