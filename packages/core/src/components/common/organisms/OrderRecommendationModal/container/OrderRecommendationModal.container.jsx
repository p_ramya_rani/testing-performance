// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  getRecommendationOrderModalState,
  getRecommendationOrderPartNumbers,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import selectors from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.selectors';
import { closeRecommendationOrderModal } from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.actions';
import { trackClick, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import OrderRecommendationPrompt from '../views';

function OrderRecommendationPromptContainer({
  partNumbers,
  handleClose,
  isModalOpen,
  labels,
  navigation,
  ...others
}) {
  return (
    <OrderRecommendationPrompt
      partNumbers={partNumbers}
      handleClose={handleClose}
      isModalOpen={isModalOpen}
      labels={labels}
      navigation={navigation}
      {...others}
    />
  );
}

function mapStateToProps(state) {
  return {
    isModalOpen: getRecommendationOrderModalState(state),
    partNumbers: getRecommendationOrderPartNumbers(state),
    labels: selectors.getConfirmationLabels(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleClose: payload => {
      dispatch(closeRecommendationOrderModal(payload));
    },
    trackClickAction: data => {
      dispatch(trackClick(data));
    },
    setClickAnalyticsDataAction: data => {
      dispatch(setClickAnalyticsData(data));
    },
  };
}

OrderRecommendationPromptContainer.propTypes = {
  partNumbers: PropTypes.shape([]).isRequired,
  handleClose: PropTypes.func.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

OrderRecommendationPromptContainer.defaultProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderRecommendationPromptContainer);

export { OrderRecommendationPromptContainer as OrderRecommendationPromptContainerVanilla };
