// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import names from '@tcp/core/src/constants/eventsName.constants';
import { isAndroid } from '@tcp/core/src/utils';
import withStyles from '../../../hoc/withStyles';
import styles, { ModalContent } from '../styles/OrderRecommendationModal.style';
import Modal from '../../../molecules/Modal';
import withKeyboardAvoidingView from '../../../hoc/withKeyboardAvoidingView.native';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';

const ModalContentWithKeyboardAvoidingView = withKeyboardAvoidingView(ModalContent);

function handleModalClose(handleClose, trackClickAction, setClickAnalyticsDataAction) {
  const payload = {
    isOpen: false,
  };

  const name = names.screenNames.order_recommendation_drawer_close;
  const module = 'browse';
  const clickData = {
    customEvents: ['event126'],
  };
  setClickAnalyticsDataAction(clickData);
  trackClickAction({ name, module });
  handleClose(payload);
}

function OrderRecommendationModal({
  partNumbers,
  handleClose,
  isModalOpen,
  labels,
  navigation,
  trackClickAction,
  setClickAnalyticsDataAction,
}) {
  const recommendationAttributes = {
    variation: 'moduleO',
    navigation,
    page: Constants.RECOMMENDATIONS_PAGES_MAPPING.CHECKOUT,
    isHeaderAccordion: true,
    newRecommendationEnabled: true,
    disableViewMore: true,
    isFavoriteRecommendation: true,
    excludedIds: partNumbers.join(),
    handleClose,
  };
  if (isModalOpen) {
    const name = names.screenNames.order_recommendation_drawer_open;
    const module = 'browse';
    const clickData = {
      customEvents: ['event125'],
    };
    setTimeout(() => {
      setClickAnalyticsDataAction(clickData);
      trackClickAction({ name, module });
    }, 3000);
  }
  return (
    isModalOpen && (
      <Modal
        isOpen={isModalOpen}
        onRequestClose={() =>
          handleModalClose(handleClose, trackClickAction, setClickAnalyticsDataAction)
        }
        closeIconDataLocator="added-to-bg-close"
        animationType="none"
        headingAlign="left"
        heading={labels.orderNotificationTitle}
        headingFontFamily="primary"
        headingFontWeight={isAndroid() ? 'bold' : 'semibold'}
        horizontalBar={false}
        fontSize="fs16"
        aria={{
          labelledby: `${labels.orderNotificationTitle}`,
          describedby: `${labels.orderNotificationTitle}`,
        }}
      >
        <ModalContentWithKeyboardAvoidingView>
          <Recommendations
            portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.ORDER_NOTIFICATION}
            {...recommendationAttributes}
            sequence="2"
          />
        </ModalContentWithKeyboardAvoidingView>
      </Modal>
    )
  );
}

OrderRecommendationModal.propTypes = {
  partNumbers: PropTypes.shape([]).isRequired,
  handleClose: PropTypes.func.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

export default withStyles(errorBoundary(OrderRecommendationModal), styles);
export { OrderRecommendationModal as OrderRecommendationModalVanilla };
