// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { change } from 'redux-form';
import getAddressLocationInfo from '@tcp/core/src/utils/addressLocation';
import getAddressLocationInfoGmaps from '@tcp/core/src/utils/addressLocationGmaps';
import constants from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.constants';
import { getLocalStorage } from '@tcp/core/src/utils/utils.web';
import StoreLocatorModalView from '../views/StoreLocatorModal.view';
import {
  getStoresByCoordinates,
  setFavoriteStoreActn,
} from '../../../../features/storeLocator/StoreLanding/container/StoreLanding.actions';
import {
  getMapboxSwitch,
  getStoreLocatorMapboxApi,
  getStoreLocatorMapboxTileIDs,
  getStoreSearchTypesParam,
} from '../../../../../reduxStore/selectors/session.selectors';
import { getPageLabels } from '../../../../features/storeLocator/StoreLanding/container/StoreLanding.selectors';
import { getAccessibilityLabels } from '../../../../features/browse/Favorites/container/Favorites.selectors';

const { STORE_LOCATOR } = constants;
const DEFAULT_STORE_RADIUS = 25;
const INITIAL_STORE_LIMIT = 25;

const getDefaultStore = () => {
  let store;
  try {
    store = JSON.parse(getLocalStorage('defaultStore')) || '';
    return store;
  } catch (e) {
    return {};
  }
};

const StoreLocatorModal = (props) => {
  const {
    fetchStoresByCoordinates,
    storeLocatorMapboxApi,
    storeLocatorMapboxTileIDs,
    mapboxSwitch,
    mapboxStoreSearchTypesParam,
    formData,
    modalStoreSearchData,
    updateFormValue,
    storeSearchInProgress,
  } = props;

  const { storeAddress, storeDistance } = formData;
  const [searchedDistance, setSearchedDistsance] = useState(25);

  const loadStoresByCoordinates = (coordinatesPromise, maxItems, type, radius) => {
    coordinatesPromise.then((coordinates) => {
      fetchStoresByCoordinates({
        coordinates,
        maxItems,
        type,
        storeLocatorMapboxApi,
        storeLocatorMapboxTileIDs,
        mapboxSwitch,
        radius,
      });
    });
  };

  const setDefaultStoreSearchRadius = () => {
    updateFormValue('storeDistance', 25);
  };

  /**
   * @function getLocationStores function to fetch the
   * stores list based on the user location coordinates
   */

  const getLocationStores = () => {
    setDefaultStoreSearchRadius();
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((pos) => {
        loadStoresByCoordinates(
          Promise.resolve({ lat: pos.coords.latitude, lng: pos.coords.longitude }),
          INITIAL_STORE_LIMIT,
          'STORES'
        );
      });
    }
  };

  const fetchStores = (address, radius = DEFAULT_STORE_RADIUS) => {
    let res;
    try {
      if (mapboxSwitch) {
        res = getAddressLocationInfo(address, mapboxStoreSearchTypesParam);
      } else {
        res = getAddressLocationInfoGmaps(storeAddress);
      }
    } catch (err) {
      res = {};
    }
    return loadStoresByCoordinates(res, 25, 'STORES', radius);
  };

  const onSubmitSearchForm = () => {
    return fetchStores(storeAddress, storeDistance);
  };

  useEffect(() => {
    updateFormValue('storeDistance', 25);
    if (modalStoreSearchData) {
      const zipCode = modalStoreSearchData?.basicInfo?.address?.zipCode || '';
      if (zipCode) {
        updateFormValue('storeAddressLocator', zipCode);
        fetchStores(zipCode);
      }
    }
  }, [modalStoreSearchData]);

  useEffect(() => {
    if (!storeSearchInProgress) {
      setSearchedDistsance(storeDistance);
    }
  }, [storeSearchInProgress]);

  return (
    <StoreLocatorModalView
      {...props}
      setDefaultStoreSearchRadius={setDefaultStoreSearchRadius}
      onSubmitSearchForm={onSubmitSearchForm}
      getLocationStores={getLocationStores}
      storeRadius={searchedDistance}
      storeAddress={storeAddress}
      storeDistance={storeDistance}
    />
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStoresByCoordinates: (storeConfig) => dispatch(getStoresByCoordinates(storeConfig)),
    setFavoriteStore: (payload) =>
      dispatch(setFavoriteStoreActn({ ...payload, key: STORE_LOCATOR })),
    updateFormValue: (field, value) => {
      dispatch(change('FindBopisStore', field, value));
    },
  };
};
const mapStateToProps = (state) => {
  return {
    suggestedStoreList:
      state.StoreLocatorReducer && state.StoreLocatorReducer.get('suggestedStores'),
    mapboxStoreSearchTypesParam: getStoreSearchTypesParam(state),
    mapboxSwitch: getMapboxSwitch(state),
    storeLocatorMapboxApi: getStoreLocatorMapboxApi(state),
    storeLocatorMapboxTileIDs: getStoreLocatorMapboxTileIDs(state),
    favoriteStore: getDefaultStore(),
    searchDone: state.StoreLocatorReducer && state.StoreLocatorReducer.get('searchDone'),
    storeSearchInProgress:
      state.StoreLocatorReducer && state.StoreLocatorReducer.get('storeSearchInProgress'),
    formData: {
      storeAddress: state?.form?.FindBopisStore?.values?.storeAddressLocator,
      storeDistance: state?.form?.FindBopisStore?.values?.storeDistance,
    },
    labels: {
      ...getPageLabels(state),
      ...getAccessibilityLabels(state),
    },
  };
};
StoreLocatorModal.propTypes = {
  fetchStoresByCoordinates: PropTypes.func.isRequired,
  updateFormValue: PropTypes.func.isRequired,
  storeLocatorMapboxApi: PropTypes.bool.isRequired,
  storeLocatorMapboxTileIDs: PropTypes.string,
  mapboxSwitch: PropTypes.bool.isRequired,
  mapboxStoreSearchTypesParam: PropTypes.string,
  storeSearchInProgress: PropTypes.bool,
  formData: PropTypes.shape({
    storeAddress: PropTypes.string,
    storeDistance: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
  }).isRequired,
  modalStoreSearchData: PropTypes.shape({
    basicInfo: PropTypes.shape({
      address: PropTypes.shape({
        zipCode: PropTypes.string,
      }),
    }),
  }),
};
StoreLocatorModal.defaultProps = {
  mapboxStoreSearchTypesParam: '',
  storeLocatorMapboxTileIDs: '',
  modalStoreSearchData: {},
  storeSearchInProgress: false,
};
export { StoreLocatorModal as StoreLocatorModalVanilla };
export default connect(mapStateToProps, mapDispatchToProps)(StoreLocatorModal);
