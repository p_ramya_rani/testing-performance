// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { StoreLocatorModalVanilla as StoreLocatorModal } from '../StoreLocatorModal.container';
import StoreLocatorModalView from '../../views/StoreLocatorModal.view';

const props = {
  fetchStoresByCoordinates: jest.fn(),
  updateFormValue: jest.fn(),
  storeLocatorMapboxApi: false,
  storeLocatorMapboxTileIDs: '',
  mapboxSwitch: false,
  mapboxStoreSearchTypesParam: '',
  storeSearchInProgress: false,
  formData: {
    storeAddress: '10003',
    storeDistance: 25,
  },
  modalStoreSearchData: {
    basicInfo: {
      address: {
        zipCode: 10003,
      },
    },
  },
};

describe('Store Locator Modal View', () => {
  it('should render correctly', () => {
    const component = shallow(<StoreLocatorModal {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call instance methods correctly', () => {
    const component = shallow(<StoreLocatorModal {...props} />);
    const childNode = component.find(StoreLocatorModalView);
    const stopPropagation = jest.fn();
    const preventDefault = jest.fn();
    const mockGeolocation = {
      getCurrentPosition: jest.fn(),
      watchPosition: jest.fn(),
    };

    global.navigator.geolocation = mockGeolocation;
    childNode.props().onSubmitSearchForm({ preventDefault, stopPropagation });
    childNode.props().getLocationStores();
    expect(mockGeolocation.getCurrentPosition).toHaveBeenCalled();
  });
});
