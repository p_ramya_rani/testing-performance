import { css } from 'styled-components';

export default css`
  &.store-list-view {
    padding: 0 12px;
    .favorite-store-wraper {
      margin-bottom: 28px;
      .fav-store-label {
        text-transform: uppercase;
        margin-bottom: 12px;
      }
    }
    h2 {
      font-size: 18px;
      font-weight: bold;
      margin-bottom: 10px;
    }
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-bottom: 50px;
    }
  }
`;
