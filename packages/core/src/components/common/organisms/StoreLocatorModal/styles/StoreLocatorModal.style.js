import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

export default css``;

export const modalstylesRedesign = css`
  div.TCPModal__InnerContent.bopis-store-locator-innerContent {
    right: 0;
    left: auto;
    top: 0;
    bottom: 0;
    transform: none;
    box-shadow: 0 4px 8px 0 rgba(163, 162, 162, 0.5);
    padding: 0;
    width: 350px;
    max-height: none;
    overflow-x: hidden;

    .find-store-block .current-location-text {
      width: auto;
      margin-bottom: 13px;
      min-height: unset;
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 500px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 500px;
      animation: slide-in 0.5s forwards;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      right: auto;
      left: 0;
      width: 100%;
      animation: slide-in-mobile 0.5s forwards;
      border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
        ${(props) => props.theme.spacing.ELEM_SPACING.XS} 0px 0px;
    }
    @keyframes slide-in {
      0% {
        transform: translateX(100%);
      }
      100% {
        transform: translateX(0%);
      }
    }
    @keyframes slide-in-mobile {
      0% {
        transform: translateY(100%);
      }
      100% {
        transform: translateY(5%);
      }
    }
    @keyframes slide-out {
      0% {
        transform: translateX(0%);
      }
      100% {
        transform: translateX(100%);
      }
    }

    .close-modal {
      background: url('${(props) => getIconPath('modal-close', props)}');
      background-size: 100% 100%;
      right: 18px;
      top: 12px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        top: 16px;
      }
    }
    .Modal-Header {
      padding-top: 7px;
      margin-bottom: 24px;
      height: 51px;
      background-color: ${(props) => props.theme.colors.CULTURED};
      display: flex;
      align-items: center;
      justify-content: center;
      z-index: ${(props) => props.theme.zindex.zLoader};
      @media ${(props) => props.theme.mediaQuery.medium} {
        padding-top: 0px;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        margin-bottom: 10px;
      }
    }
    .Modal_Heading:before {
      content: '';
      position: absolute;
      margin: auto;
      z-index: -1;
      background-color: ${(props) => props.theme.colors.CULTURED};
      bottom: 39px;
      width: 100%;
      @media ${(props) => props.theme.mediaQuery.medium} {
        bottom: 38px;
        left: -13px;
        padding-right: 18px;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        left: -14px;
        padding-right: 19px;
      }
    }
    .Modal_Heading {
      border-bottom: none;
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
      display: flex;
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      justify-content: center;
      margin-bottom: 0px;
      align-items: flex-start;
      padding: 0;
    }
  }
`;
