import { css } from 'styled-components';

export default css`
  &.store-details-tile {
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    border: 1px solid ${(props) => props.theme.colors.BORDER.NORMAL};
    padding: 12px;
    margin-bottom: 10px;
    box-sizing: border-box;
    position: relative;
    border-radius: 6px;
    .favstore-image {
      position: absolute;
      right: 0;
      top: -1px;
    }
    .store-info {
      display: flex;
      flex-direction: column;
      text-align: left;
      color: #1a1a1a;
      padding: 10px;
      box-sizing: border-box;
      background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
      border-radius: 6px;
      font-family: ${(props) => props.theme.typography.fonts.secondary};
    }
    .store-name {
      font-size: 16px;
      font-weight: bold;
      line-height: 1.38;
      text-transform: capitalize;
    }
    .store-timing {
      font-size: 12px;
    }
    .store-time-lbl {
      font-family: Nunito;
    }
    .store-hour {
      text-transform: lowercase;
    }
    .store-address {
      font-size: 14px;
      margin-top: 10px;
      width: 182px;
      text-transform: capitalize;
      white-space: pre;
      @media ${(props) => props.theme.mediaQuery.large} {
        margin-top: 21px;
      }
    }
    .store-actions {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
      padding-top: 16px;
    }
    .select-store-btn {
      color: #fff;
      font-weight: bold;
      border-radius: 6px;
      width: 170px;
    }
    .get-direction-link {
      display: inline-flex;
      align-items: center;
    }
    .location-image {
      height: 18px;
      width: 18px;
      padding: 0 8px 0 10px;
    }
    .store-directions-link {
      font-size: 14px;
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      color: ${(props) => props.theme.colors.PROMO.BLUE};
    }
  }
`;
