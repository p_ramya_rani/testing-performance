import { css } from 'styled-components';
import { getImageFilePath, isiOSWeb } from '@tcp/core/src/utils';

const getDefaultFontSize = (props) => {
  return isiOSWeb() ? props.theme.typography.fontSizes.fs16 : props.theme.typography.fontSizes.fs14;
};

export default css`
  &.find-store-block {
    margin: 0px 12px 24px 12px;
    padding: 16px 8px 0 8px;
    border-radius: 6px;
    background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 16px 16px 0 16px;
    }
    .zipcodefield .TextBox__placeholder {
      top: 16px;
    }
    .TextBox__label {
      font-size: 14px;
    }
    .find-store-block-header {
      font-size: 18px;
      font-weight: bold;
      text-align: left;
      color: ${(props) => props.theme.colors.TEXT.DARK};
      font-family: Nunito;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: normal;
      margin-top: 0;
    }
    .currentLocationWrapper {
      @media ${(props) => props.theme.mediaQuery.smallMax} {
        margin-top: 2px;
      }
    }
    .location-image {
      height: 18px;
      width: 18px;
      padding: 0 4px 0 0px;
    }
    .current-location-text {
      display: inline-flex;
      width: 300px;
      align-items: center;
      font-family: Nunito;
      font-size: 14px;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.57;
      letter-spacing: normal;
      text-align: left;
      color: ${(props) => props.theme.colors.TEXT.DARK};
      text-transform: inherit;
      text-decoration: underline;
      padding-left: 0px;
    }
    .utility-no-nargin {
      margin-left: 0;
    }
    .storeadress {
      margin-top: 12px;
      display: block;
      @media ${(props) => props.theme.mediaQuery.smallMax} {
        padding-bottom: 12px;
      }
      @media ${(props) => props.theme.mediaQuery.medium} {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
      }
    }
    .storezipcode {
      float: left;
      width: calc(100% - 125px);
      @media ${(props) => props.theme.mediaQuery.medium} {
        display: flex;
        flex-direction: row;
        width: 45%;
      }
    }
    .zipcodefield {
      width: 83%;

      @media ${(props) => props.theme.mediaQuery.medium} {
        padding-right: 12px;
        margin-right: 22px;
      }
      .TextBox__input {
        font-family: Nunito;
        font-weight: ${(props) => props.theme.typography.fontWeights.bold};
        font-size: ${(props) => `${getDefaultFontSize(props)}`};
      }
      .TextBox__input:focus ~ .TextBox__label {
        top: -21px;
        left: 0;
        background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
        font-weight: normal;
        line-height: 1;
        letter-spacing: 0.2px;
        text-align: center;
        font-size: ${(props) => props.theme.typography.fontSizes.fs12};
        color: ${(props) => props.theme.colors.PRIMARY.DARK};
      }

      .TextBox__label.nonActive {
        font-size: ${(props) => props.theme.typography.fontSizes.fs12};
        color: ${(props) => props.theme.colors.PRIMARY.DARK};
        display: block;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
        top: -21px;
        left: 0;
        background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
        font-weight: normal;
        line-height: 1;
        letter-spacing: 0.2px;
        text-align: center;
      }
    }
    .button-search {
      height: 54px;
      border-radius: 6px;
      background-color: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
    }
    .distance-field-wrapper {
      float: right;
      @media ${(props) => props.theme.mediaQuery.medium} {
        float: none;
      }
    }
    .distancefield {
      width: 125px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        width: 110px;
      }
      .select__input {
        border: 1px solid ${(props) => props.theme.colors.PRIMARY.DARK};
        border-radius: 6px;
        padding: 8px 8px 8px 15px;
        font-family: Nunito;
        font-size: ${(props) => `${getDefaultFontSize(props)}`};
        font-weight: ${(props) => props.theme.typography.fontWeights.bold};
        height: 54px;
        background: url(${(props) => getImageFilePath(props)}/carrot-small-down.png) no-repeat right
          12px bottom 20px;
        background-color: white;
      }
      .select__label {
        font-size: ${(props) => props.theme.typography.fontSizes.fs12};
        width: 60px;
        padding: 2px 6px;
        color: ${(props) => props.theme.colors.PRIMARY.DARK};
        top: -21px;
        left: 0;
        background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
        font-weight: normal;
        line-height: 1;
        letter-spacing: 0.2px;
        text-align: center;
      }
    }
    .distanceborder {
      .select__input {
        border: 1px solid rgb(37, 79, 110);
      }
    }
  }
`;
