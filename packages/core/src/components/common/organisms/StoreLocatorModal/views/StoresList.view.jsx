/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { getLabelValue } from '@tcp/core/src/utils';
import withStyles from '../../../hoc/withStyles';
import StoreListStyle from '../styles/StoresList.style';
import StoreDetailsTileView from './StoreDetailsTile.view';
import { BodyCopy } from '../../../atoms';

const StoresListView = ({
  className,
  setFavoriteStore,
  favoriteStore,
  onClose,
  onStoreChange,
  suggestedStoreList,
  labels,
  storeRadius,
  searchDone,
}) => {
  const showSubmitError = false;
  if (
    showSubmitError ||
    (searchDone && !(suggestedStoreList && (suggestedStoreList.length || suggestedStoreList.size)))
  ) {
    return (
      <Notification
        status={showSubmitError ? 'error' : 'info'}
        message={getLabelValue(
          labels,
          showSubmitError ? 'lbl_storelanding_errorLabel' : 'lbl_storelanding_noStoresFound'
        )}
        className="storeview__error"
      />
    );
  }

  const storeNearYouText = getLabelValue(labels, 'lbl_storelanding_storeNearYou');
  const withinText = getLabelValue(labels, 'lbl_storelanding_within');
  const milesText = getLabelValue(labels, 'lbl_storelanding_miles');
  const favStoreId = favoriteStore?.basicInfo?.id || 0;
  const isSearchResultIncludesFavStoreOnly =
    suggestedStoreList &&
    suggestedStoreList.findIndex((store) => store?.basicInfo?.id === favStoreId);
  const isOnlyOneStoreAvailable =
    suggestedStoreList &&
    suggestedStoreList.length === 1 &&
    isSearchResultIncludesFavStoreOnly !== -1;
  const renderStoreList = () => {
    if ((suggestedStoreList && suggestedStoreList.length === 0) || !searchDone) {
      return null;
    }

    return (
      <>
        {!isOnlyOneStoreAvailable && (
          <h2>{`${storeNearYouText} (${withinText} ${storeRadius} ${milesText})`}</h2>
        )}
        {suggestedStoreList.map((store) => {
          const { basicInfo } = store;
          if (basicInfo.id === favStoreId) {
            return null;
          }
          return (
            <StoreDetailsTileView
              key={basicInfo.id}
              store={store}
              labels={labels}
              setFavoriteStore={setFavoriteStore}
              onCloseModal={onClose}
              onStoreChange={onStoreChange}
            />
          );
        })}
      </>
    );
  };

  return (
    <div className={`${className} store-list-view`}>
      {favoriteStore && (
        <div className="favorite-store-wraper">
          <BodyCopy
            className="fav-store-label"
            component="div"
            fontSize="fs16"
            fontWeight="extrabold"
            fontFamily="secondary"
          >
            {getLabelValue(labels, 'lbl_storelanding_favorite_store')}
          </BodyCopy>
          <StoreDetailsTileView
            isFavStore
            store={favoriteStore}
            setFavoriteStore={setFavoriteStore}
            onCloseModal={onClose}
            onStoreChange={onStoreChange}
            labels={labels}
          />
        </div>
      )}
      {renderStoreList()}
    </div>
  );
};
StoresListView.propTypes = {
  className: PropTypes.string.isRequired,
  setFavoriteStore: PropTypes.func.isRequired,
  favoriteStore: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
  onStoreChange: PropTypes.func.isRequired,
  suggestedStoreList: PropTypes.arrayOf({}).isRequired,
  labels: PropTypes.shape({
    lbl_storelanding_storeNearYou: PropTypes.string,
    lbl_storelanding_within: PropTypes.string,
    lbl_storelanding_miles: PropTypes.string,
    lbl_storelanding_favorite_store: PropTypes.string,
  }).isRequired,
  storeRadius: PropTypes.number.isRequired,
  searchDone: PropTypes.bool.isRequired,
};

export { StoresListView as StoresListViewVanilla };
export default withStyles(StoresListView, StoreListStyle);
