// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { FindStoreViewVanilla as FindStoreView } from '../FindStore.view';

const props = {
  className: 'test123slm',
  onSubmitSearchForm: jest.fn(),
  handleSubmit: jest.fn(),
  labels: {
    lbl_storelanding_currentLocation: 'Use Current Location',
    markerIconText: 'Marker Icon',
    lbl_storelanding_search: 'SEARCH',
    lbl_storelanding_storeSearchPlaceholder: 'ZIPCODE,CITY,STATE',
    lbl_storelanding_find_a_store_near_you: 'FIND A STORE NEAR YOU',
  },
  getLocationStores: jest.fn(),
  storeSearchInProgress: false,
};

describe('Store Locator Modal View', () => {
  it('should render correctly', () => {
    const component = shallow(<FindStoreView {...props} />);
    expect(component).toMatchSnapshot();
  });
});
