// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import { getLabelValue } from '@tcp/core/src/utils';
import withStyles from '../../../hoc/withStyles';
import StoreLocatorModalStyle, { modalstylesRedesign } from '../styles/StoreLocatorModal.style';
import FindStoreView from './FindStore.view';
import StoresListView from './StoresList.view';

const StoreLocatorModalView = (props) => {
  const { className, isOpen, onClose, labels } = props;
  return (
    <div className={`${className} bopis-store-locator-modal`}>
      <Modal
        isOpen={isOpen}
        onRequestClose={onClose}
        overlayClassName="TCPModal__Overlay"
        className={`${className} TCPModal__Content`}
        heading={getLabelValue(labels, 'lbl_storelanding_select_a_store')}
        fixedWidth
        stickyHeader
        headingAlign="center"
        horizontalBar={false}
        stickyCloseIcon
        innerContentClassName="bopis-store-locator-innerContent"
        inheritedStyles={modalstylesRedesign}
      >
        <FindStoreView {...props} />
        <StoresListView {...props} />
      </Modal>
    </div>
  );
};
StoreLocatorModalView.propTypes = {
  className: PropTypes.string.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  labels: PropTypes.shape({
    lbl_storelanding_select_a_store: PropTypes.string,
  }).isRequired,
};
export { StoreLocatorModalView as StoreLocatorModalViewVanilla };
export default withStyles(StoreLocatorModalView, StoreLocatorModalStyle);
