// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { StoresListViewVanilla as StoresListView } from '../StoresList.view';

const favStore = {
  distance: '4.33',
  basicInfo: {
    id: '110850',
    storeName: 'fulton street',
    isDefault: false,
    address: {
      addressLine1: '471-485 fulton street',
      city: 'brooklyn',
      state: 'NY',
      country: 'US',
      zipCode: 11201,
    },
    phone: '(718) 243-1150',
    coordinates: { lat: 40.69112, long: -73.98625 },
  },
  isGym: false,
  hours: {
    regularHours: [
      {
        dayName: 'WEDNESDAY',
        openIntervals: [{ fromHour: '2021-12-15 09:00:00', toHour: '2021-12-15 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'THURSDAY',
        openIntervals: [{ fromHour: '2021-12-16 09:00:00', toHour: '2021-12-16 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'FRIDAY',
        openIntervals: [{ fromHour: '2021-12-17 09:00:00', toHour: '2021-12-17 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'SATURDAY',
        openIntervals: [{ fromHour: '2021-12-18 09:00:00', toHour: '2021-12-18 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'SUNDAY',
        openIntervals: [{ fromHour: '2021-12-19 09:00:00', toHour: '2021-12-19 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'MONDAY',
        openIntervals: [{ fromHour: '2021-12-20 09:00:00', toHour: '2021-12-20 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'TUESDAY',
        openIntervals: [{ fromHour: '2021-12-21 09:00:00', toHour: '2021-12-21 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'WEDNESDAY',
        openIntervals: [{ fromHour: '2021-12-22 09:00:00', toHour: '2021-12-22 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'THURSDAY',
        openIntervals: [{ fromHour: '2021-12-23 09:00:00', toHour: '2021-12-23 21:00:00' }],
        isClosed: false,
      },
    ],
    holidayHours: [],
    regularAndHolidayHours: [],
  },
  features: { storeType: 'Retail Store' },
  key: 'STORE_LOCATOR',
};
const anotherStore = {
  distance: '4.33',
  basicInfo: {
    id: '110851',
    storeName: 'test street',
    isDefault: false,
    address: {
      addressLine1: '471-485 fulton street',
      city: 'brooklyn',
      state: 'NY',
      country: 'US',
      zipCode: 11201,
    },
    phone: '(718) 243-1150',
    coordinates: { lat: 40.69112, long: -73.98625 },
  },
  isGym: false,
  hours: {
    regularHours: [
      {
        dayName: 'WEDNESDAY',
        openIntervals: [{ fromHour: '2021-12-15 09:00:00', toHour: '2021-12-15 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'THURSDAY',
        openIntervals: [{ fromHour: '2021-12-16 09:00:00', toHour: '2021-12-16 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'FRIDAY',
        openIntervals: [{ fromHour: '2021-12-17 09:00:00', toHour: '2021-12-17 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'SATURDAY',
        openIntervals: [{ fromHour: '2021-12-18 09:00:00', toHour: '2021-12-18 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'SUNDAY',
        openIntervals: [{ fromHour: '2021-12-19 09:00:00', toHour: '2021-12-19 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'MONDAY',
        openIntervals: [{ fromHour: '2021-12-20 09:00:00', toHour: '2021-12-20 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'TUESDAY',
        openIntervals: [{ fromHour: '2021-12-21 09:00:00', toHour: '2021-12-21 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'WEDNESDAY',
        openIntervals: [{ fromHour: '2021-12-22 09:00:00', toHour: '2021-12-22 21:00:00' }],
        isClosed: false,
      },
      {
        dayName: 'THURSDAY',
        openIntervals: [{ fromHour: '2021-12-23 09:00:00', toHour: '2021-12-23 21:00:00' }],
        isClosed: false,
      },
    ],
    holidayHours: [],
    regularAndHolidayHours: [],
  },
  features: { storeType: 'Retail Store' },
  key: 'STORE_LOCATOR',
};
const props = {
  className: 'test123slv',
  setFavoriteStore: jest.fn(),
  favoriteStore: { ...favStore },
  onClose: jest.fn(),
  onStoreChange: jest.fn(),
  suggestedStoreList: [favStore, anotherStore],
  storeRadius: 25,
  searchDone: true,
  labels: {
    lbl_storelanding_storeNearYou: 'Store Near You',
    lbl_storelanding_within: 'WITHIN',
    lbl_storelanding_miles: 'MILES',
    lbl_storelanding_favorite_store: 'FAVORITE STORE',
  },
};

describe('Store List View', () => {
  it('should render correctly when search is not completed', () => {
    const component = shallow(<StoresListView {...props} searchDone={false} />);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly when search is completed', () => {
    const component = shallow(<StoresListView {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render error message correctly when search is completed and store not found', () => {
    const component = shallow(<StoresListView {...props} suggestedStoreList={[]} />);
    expect(component).toMatchSnapshot();
  });
});
