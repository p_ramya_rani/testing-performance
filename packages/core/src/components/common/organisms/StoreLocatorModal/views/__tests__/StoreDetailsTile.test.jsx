// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { StoreDetailsTileViewVanilla as StoreDetailsTileView } from '../StoreDetailsTile.view';

const props = {
  className: 'test123',
  labels: {
    lbl_storedetail_selectstore_btn: 'SELECT STORE',
    lbl_storelanding_getdirections_link: 'Get Directions',
    markerIconText: 'Marker Icon',
    lbl_storelanding_favStore: 'Favorite Store',
  },
  store: {
    distance: '4.33',
    basicInfo: {
      id: '110850',
      storeName: 'fulton street',
      isDefault: false,
      address: {
        addressLine1: '471-485 fulton street',
        city: 'brooklyn',
        state: 'NY',
        country: 'US',
        zipCode: 11201,
      },
      phone: '(718) 243-1150',
      coordinates: { lat: 40.69112, long: -73.98625 },
    },
    isGym: false,
    hours: {
      regularHours: [
        {
          dayName: 'WEDNESDAY',
          openIntervals: [{ fromHour: '2021-12-15 09:00:00', toHour: '2021-12-15 21:00:00' }],
          isClosed: false,
        },
        {
          dayName: 'THURSDAY',
          openIntervals: [{ fromHour: '2021-12-16 09:00:00', toHour: '2021-12-16 21:00:00' }],
          isClosed: false,
        },
        {
          dayName: 'FRIDAY',
          openIntervals: [{ fromHour: '2021-12-17 09:00:00', toHour: '2021-12-17 21:00:00' }],
          isClosed: false,
        },
        {
          dayName: 'SATURDAY',
          openIntervals: [{ fromHour: '2021-12-18 09:00:00', toHour: '2021-12-18 21:00:00' }],
          isClosed: false,
        },
        {
          dayName: 'SUNDAY',
          openIntervals: [{ fromHour: '2021-12-19 09:00:00', toHour: '2021-12-19 21:00:00' }],
          isClosed: false,
        },
        {
          dayName: 'MONDAY',
          openIntervals: [{ fromHour: '2021-12-20 09:00:00', toHour: '2021-12-20 21:00:00' }],
          isClosed: false,
        },
        {
          dayName: 'TUESDAY',
          openIntervals: [{ fromHour: '2021-12-21 09:00:00', toHour: '2021-12-21 21:00:00' }],
          isClosed: false,
        },
        {
          dayName: 'WEDNESDAY',
          openIntervals: [{ fromHour: '2021-12-22 09:00:00', toHour: '2021-12-22 21:00:00' }],
          isClosed: false,
        },
        {
          dayName: 'THURSDAY',
          openIntervals: [{ fromHour: '2021-12-23 09:00:00', toHour: '2021-12-23 21:00:00' }],
          isClosed: false,
        },
      ],
      holidayHours: [],
      regularAndHolidayHours: [],
    },
    features: { storeType: 'Retail Store' },
    key: 'STORE_LOCATOR',
  },
  setFavoriteStore: jest.fn(),
  onStoreChange: jest.fn(),
  onCloseModal: jest.fn(),
  isFavStore: true,
};

describe('Store Details Tile View', () => {
  it('should render correctly', () => {
    const component = shallow(<StoreDetailsTileView {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render handle View Bag Button Press', () => {
    const component = shallow(<StoreDetailsTileView {...props} />);
    component.find('.select-store-btn').simulate('click');
    expect(props.onCloseModal).toHaveBeenCalled();
  });
});
