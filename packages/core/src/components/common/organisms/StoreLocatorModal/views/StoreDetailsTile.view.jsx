// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue, getIconPath, getStoreHours } from '@tcp/core/src/utils';
import { Image, Button, Anchor, BodyCopy } from '@tcp/core/src/components/common/atoms';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import withStyles from '../../../hoc/withStyles';
import StoreDetailsTileStyle from '../styles/StoreDetailsTile.style';

function StoreDetailsTileView({
  className,
  labels,
  store,
  setFavoriteStore,
  onStoreChange,
  onCloseModal,
  isFavStore,
}) {
  const { protocol } = window ? window?.location : { protocol: 'https' };
  const { basicInfo, hours } = store;
  const { address, phone, storeName } = basicInfo;
  const { addressLine1, city, state, zipCode } = address;
  const storeGoggleMapLink = `${protocol}//maps.google.com/maps?daddr=${addressLine1},%20${city},%20${state},%20${zipCode}`;
  const currentDate = new Date();
  const { lblText, time } = getStoreHours(hours, labels, currentDate, 'object');
  const markerIcon = getIconPath('marker-icon');
  const favStoreIcon = getIconPath('favStore-icon');

  const onSelectStore = () => {
    setFavoriteStore(store);
    onCloseModal();
    onStoreChange(store);
  };

  return (
    <div className={`${className} store-details-tile`}>
      {isFavStore && (
        <Image
          alt={getLabelValue(labels, 'lbl_storelanding_favStore')}
          className="favstore-image icon-small"
          src={favStoreIcon}
          data-locator="favstore-icon"
        />
      )}
      <div className="store-info">
        <span className="store-name">{storeName}</span>
        <span className="store-timing">
          <BodyCopy className="store-time-lbl" component="span">
            {`${lblText} `}
          </BodyCopy>
          <BodyCopy className="store-hour" fontWeight="semibold" component="span">
            {time}
          </BodyCopy>
        </span>
        <span className="store-address">
          {`${addressLine1}\n${city} ${state} ${zipCode} \n${phone}`}
        </span>
      </div>
      <div className="store-actions">
        <div className="get-direction-link">
          <Image
            alt={getLabelValue(labels, 'markerIconText')}
            className="location-image icon-small"
            src={markerIcon}
            data-locator="marker-icon"
          />
          <ClickTracker
            as={Anchor}
            fontSizeVariation="medium"
            underline
            url={storeGoggleMapLink}
            anchorVariation="primary"
            target="_blank"
            className="store-directions-link"
            aria-label={`${storeName} ${getLabelValue(
              labels,
              'lbl_storelanding_getdirections_link'
            )}`}
            clickData={{ customEvents: ['event97'] }}
          >
            {getLabelValue(labels, 'lbl_storelanding_getdirections_link')}
          </ClickTracker>
        </div>
        <Button className="select-store-btn" fill="BLUE" type="submit" onClick={onSelectStore}>
          {getLabelValue(labels, 'lbl_storedetail_selectstore_btn')}
        </Button>
      </div>
    </div>
  );
}

StoreDetailsTileView.propTypes = {
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({
    lbl_storedetail_selectstore_btn: PropTypes.string.isRequired,
    lbl_storelanding_getdirections_link: PropTypes.string.isRequired,
    markerIconText: PropTypes.string.isRequired,
    lbl_storelanding_favStore: PropTypes.string.isRequired,
  }).isRequired,
  store: PropTypes.shape({
    basicInfo: PropTypes.shape({
      address: PropTypes.shape({
        addressLine1: PropTypes.string,
        city: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      phone: PropTypes.string,
      storeName: PropTypes.string,
    }),
    hours: PropTypes.arrayOf(PropTypes.shape({})),
  }).isRequired,
  setFavoriteStore: PropTypes.func.isRequired,
  onStoreChange: PropTypes.func.isRequired,
  onCloseModal: PropTypes.func.isRequired,
  isFavStore: PropTypes.bool,
};
StoreDetailsTileView.defaultProps = {
  isFavStore: false,
};
export { StoreDetailsTileView as StoreDetailsTileViewVanilla };
export default withStyles(StoreDetailsTileView, StoreDetailsTileStyle);
