// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { StoreLocatorModalViewVanilla as StoreLocatorModalView } from '../StoreLocatorModal.view';

const props = {
  className: 'test123slm',
  isOpen: true,
  onClose: jest.fn(),
  labels: {
    lbl_storelanding_select_a_store: 'Select a Store',
  },
};

describe('Store Locator Modal View', () => {
  it('should render correctly', () => {
    const component = shallow(<StoreLocatorModalView {...props} />);
    expect(component).toMatchSnapshot();
  });
});
