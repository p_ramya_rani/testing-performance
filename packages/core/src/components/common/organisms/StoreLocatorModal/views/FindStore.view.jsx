/* eslint-disable no-unused-expressions */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue, getIconPath } from '@tcp/core/src/utils';
import { Image, Button, Row, Col, TextBox, SelectBox } from '@tcp/core/src/components/common/atoms';
import getStandardConfig from '@tcp/core/src/utils/formValidation/validatorStandardConfig';
import createValidateMethod from '@tcp/core/src/utils/formValidation/createValidateMethod';
import { Field, reduxForm } from 'redux-form';
import { distancesMap } from '@tcp/core/src/components/common/organisms/PickupStoreModal/PickUpStoreModal.config';
import withStyles from '../../../hoc/withStyles';
import FindStoreStyle from '../styles/FindStore.style';

const DistanceOptionsMap = distancesMap;
const validateMethod = createValidateMethod(getStandardConfig(['storeAddressLocator']));
const FindStoreView = (props) => {
  const { onSubmitSearchForm, className, labels, getLocationStores, handleSubmit } = props;
  const markerIcon = getIconPath('marker-icon');

  const componentRef = React.createRef();
  const onSearchClick = () => {
    const storeAddressLocatorRef = document.querySelector('#storeAddressLocator');
    storeAddressLocatorRef?.focus();
    storeAddressLocatorRef?.blur();
  };

  const onKeyDown = (e) => {
    if (e.key === 'Enter') {
      validateMethod && validateMethod();
      e.target.blur();
    }
  };

  const renderZip = () => {
    return (
      <form name="findStoreForm" onSubmit={handleSubmit(onSubmitSearchForm)} noValidate>
        <div className="storeadress">
          <div className="storezipcode">
            <Field
              id="storeAddressLocator"
              onKeyDown={onKeyDown}
              placeholder={getLabelValue(labels, 'lbl_storelanding_storeSearchPlaceholder')}
              name="storeAddressLocator"
              dataLocator="storeAddressLocator"
              component={TextBox}
              className="zipcodefield"
              enableSuccessCheck={false}
              spc
              autocomplete="postal-code"
              title="storeAddress"
            />
          </div>
          <div className="distance-field-wrapper">
            <Field
              id="state"
              placeholder="Distance"
              title={getLabelValue(labels, 'lbl_storelanding_distancePlaceholder')}
              name="storeDistance"
              component={SelectBox}
              options={DistanceOptionsMap}
              dataLocator="distance-field"
              className="distancefield"
              enableSuccessCheck={false}
              spc
            />
          </div>
          <div>
            <Button
              buttonVariation="fixed-width"
              onClick={onSearchClick}
              fill="BLUE"
              type="submit"
              title="search"
              className="button-search"
              data-locator="search-icon"
            >
              {getLabelValue(labels, 'lbl_storelanding_search')}
            </Button>
          </div>
        </div>
      </form>
    );
  };

  return (
    <div className={`${className} find-store-block`} ref={componentRef}>
      <h1 className="find-store-block-header">
        {getLabelValue(labels, 'lbl_storelanding_find_a_store_near_you')}
      </h1>
      <Row className="utility-no-nargin">
        <Col colSize={{ large: 6, medium: 4, small: 6 }}>
          <div className="currentLocationWrapper">
            <Button
              className="current-location-text currentLocationWrapper__cta"
              onClick={getLocationStores}
            >
              <Image
                alt={getLabelValue(labels, 'markerIconText')}
                className="location-image icon-small"
                src={markerIcon}
                data-locator="marker-icon"
              />
              <span className="currentLocation">
                {getLabelValue(labels, 'lbl_storelanding_currentLocation')}
              </span>
            </Button>
          </div>
        </Col>
      </Row>
      {renderZip()}
    </div>
  );
};
FindStoreView.propTypes = {
  onSubmitSearchForm: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({
    lbl_storelanding_currentLocation: PropTypes.string,
    markerIconText: PropTypes.string,
    lbl_storelanding_search: PropTypes.string,
    lbl_storelanding_storeSearchPlaceholder: PropTypes.string,
    lbl_storelanding_find_a_store_near_you: PropTypes.string,
  }).isRequired,
  getLocationStores: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export { FindStoreView as FindStoreViewVanilla };
export default reduxForm({
  form: 'FindBopisStore', // a unique identifier for this form
  enableReinitialize: true,
  ...validateMethod,
})(withStyles(FindStoreView, FindStoreStyle));
