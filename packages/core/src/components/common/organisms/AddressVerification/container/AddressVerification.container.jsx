// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import constants from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import selectors, {
  getCouponRemovedFromOrderFlag,
} from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import {
  getUserAddress,
  getSuggestedAddress,
  getVerificationResult,
  getVerifyAddressLabels,
  getVerificationResultCodes,
} from './AddressVerification.selectors';
import AddressVerificationComponent from '../views/AddressVerification.view';
import { resetVerifyAddress, verifyAddress } from './AddressVerification.actions';

export const mapStateToProps = (state, ownProps) => {
  const getBillingValues = selectors.getBillingValues(state);
  const { billing } = getBillingValues || {};
  const { cardType } = billing || {};
  return {
    userAddress: getUserAddress(state) || ownProps.shippingAddress,
    suggestedAddress: getSuggestedAddress(state),
    verificationResult: getVerificationResult(state),
    verificationResultCodes: getVerificationResultCodes(state),
    labels: getVerifyAddressLabels(state),
    couponRemovedFromOrderFlag: getCouponRemovedFromOrderFlag(state),
    cardType,
  };
};

export const mapDispatchToProps = (dispatch) => ({
  resetVerifyAddressAction: () => {
    dispatch(resetVerifyAddress());
  },
  verifyAddressAction: (address) => dispatch(verifyAddress(address)),
  dispatchReviewReduxForm: () => dispatch(submit(constants.REVIEW_FORM_NAME)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressVerificationComponent);
