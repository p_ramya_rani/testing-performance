// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';
import { ADDRESS_VERIFICATION_REDUCER_KEY } from '../../../../../constants/reducer.constants';

export const getAddressVerificationState = state => state[ADDRESS_VERIFICATION_REDUCER_KEY];

export const getUserAddress = createSelector(
  getAddressVerificationState,
  state => state.get('userAddress')
);

export const getSuggestedAddress = createSelector(
  getAddressVerificationState,
  state => state.get('suggestedAddress')
);

export const getVerificationResult = createSelector(
  getAddressVerificationState,
  state => state.get('resultType')
);

export const getVerificationResultCodes = createSelector(
  getAddressVerificationState,
  state => state.get('resultCodes')
);

export const getVerifyAddressLabels = state => {
  return {
    verifyAddressLabels: {
      AE09: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_errorMessageAE09',
        'addEditAddress',
        'global'
      ),
      AE10: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_errorMessageAE10',
        'addEditAddress',
        'global'
      ),
      AE11: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_errorMessageAE11',
        'addEditAddress',
        'global'
      ),
      AE12: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_errorMessageAE12',
        'addEditAddress',
        'global'
      ),
      DEFAULT: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_errorMessageDefault',
        'addEditAddress',
        'global'
      ),
      youEntered: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_entered',
        'addEditAddress',
        'global'
      ),
      weSuggest: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_suggest',
        'addEditAddress',
        'global'
      ),
      verifyHeader: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyHeader',
        'addEditAddress',
        'global'
      ),
      continueCta: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_continueCta',
        'addEditAddress',
        'global'
      ),
      editAddress: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_editAddress',
        'addEditAddress',
        'global'
      ),
      addAddressHeading: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_addAddress',
        'addEditAddress',
        'global'
      ),
      addressLine2: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_addressLine2',
        'addEditAddress',
        'global'
      ),
      useUnverifiedBtn: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyUseUnverifiedBtn',
        'addEditAddress',
        'global'
      ),
      updateBtn: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_update',
        'addEditAddress',
        'global'
      ),
      modalAE02: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE02',
        'addEditAddress',
        'global'
      ),
      modalAE07: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE07',
        'addEditAddress',
        'global'
      ),
      modalAE10: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE10',
        'addEditAddress',
        'global'
      ),
      modalAE12: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE12',
        'addEditAddress',
        'global'
      ),
      modalAE13: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE13',
        'addEditAddress',
        'global'
      ),
      modalAE14: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE14',
        'addEditAddress',
        'global'
      ),
      modalAE01: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE01',
        'addEditAddress',
        'global'
      ),
      modalAE11: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE11',
        'addEditAddress',
        'global'
      ),
      modalAE08: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE08',
        'addEditAddress',
        'global'
      ),
      modalAE03: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyMappingAE03',
        'addEditAddress',
        'global'
      ),
      modalSubPremiseError: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyErrorsModalSubPremise',
        'addEditAddress',
        'global'
      ),
      formGenericError: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyErrorsFormGeneric',
        'addEditAddress',
        'global'
      ),
      addressLine2Error: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyErrorsAddressLine2',
        'addEditAddress',
        'global'
      ),
      addressLine2PlaceHolder: getLabelValue(
        state.Labels,
        'lbl_addEditAddress_verifyAddressLine2',
        'addEditAddress',
        'global'
      ),
    },
  };
};
