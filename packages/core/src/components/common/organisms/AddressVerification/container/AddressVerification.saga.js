// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest, delay, select } from 'redux-saga/effects';
import { clearSubmitErrors, stopSubmit } from 'redux-form';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getVerifyAddressLabels } from './AddressVerification.selectors';
import ADDRESS_VERIFICATION_CONSTANTS from '../AddressVerification.constants';
import {
  verifyAddressSuccess,
  verifyAddressError,
  resetVerifyAddress,
} from './AddressVerification.actions';
import { verifyAddressData } from '../../../../../services/abstractors/account';

export function* verifyAddress({ payload }) {
  try {
    yield delay(100);

    const { formName, doNotShowLoader = false } = payload;
    if (!doNotShowLoader) {
      yield put(setLoaderState(true));
    }
    if (formName) {
      yield put(clearSubmitErrors(formName));
    }
    const { suggestedAddress, resultType, resultCodes } = yield call(verifyAddressData, payload);
    const {
      verifyAddressLabels: { formGenericError },
    } = yield select(getVerifyAddressLabels);
    const {
      ADDRESS_FORM_ERROR_CODES: { GENERIC_ERROR_CODES },
    } = ADDRESS_VERIFICATION_CONSTANTS;
    const genericResultCode = GENERIC_ERROR_CODES.find(code => resultCodes.includes(code));
    const verifySubmitErrors = {
      address: {
        formGeneric: formGenericError,
      },
    };
    if (formName && genericResultCode) {
      yield put(stopSubmit(formName, verifySubmitErrors));
      yield put(resetVerifyAddress());
    } else {
      yield put(verifyAddressSuccess({ suggestedAddress, resultType, resultCodes }));
    }
    yield put(setLoaderState(false));
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'AddressVerification - verifyAddress',
        payloadRecieved: payload,
      },
    });
    yield put(setLoaderState(false));
    yield put(verifyAddressError({ resultType: 'ERROR' }));
  }
}

export function* verifyAddressSaga() {
  yield takeLatest(ADDRESS_VERIFICATION_CONSTANTS.VERIFY_ADDRESS, verifyAddress);
}

export default verifyAddressSaga;
