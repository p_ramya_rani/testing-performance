// 9fbef606107a605d69c0edbcd8029e5d 
import { put, takeLatest } from 'redux-saga/effects';
import { stopSubmit } from 'redux-form';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { verifyAddress, verifyAddressSaga } from '../AddressVerification.saga';
import { verifyAddressError, verifyAddressSuccess } from '../AddressVerification.actions';
import CONSTANTS from '../../AddressVerification.constants';

describe('AddressVerification saga', () => {
  describe('verifyAddress', () => {
    it('should dispatch verifyAddressSuccess action for success resposnse ', () => {
      const verifyAddressGen = verifyAddress({ payload: { firstName: 'test' } });
      verifyAddressGen.next();
      verifyAddressGen.next();
      const response = {
        suggestedAddress: {
          firstName: 'test',
          isCommercialAddress: false,
        },
        resultType: 'AS01',
        resultCodes: 'AS01, AC01',
      };
      verifyAddressGen.next();
      verifyAddressGen.next(response);
      const putDescriptor = verifyAddressGen.next({
        verifyAddressLabels: { formGenericError: '' },
      }).value;
      expect(putDescriptor).toEqual(
        put(
          verifyAddressSuccess({
            suggestedAddress: {
              firstName: 'test',
              isCommercialAddress: false,
            },
            resultType: 'AS01',
            resultCodes: 'AS01, AC01',
          })
        )
      );
    });

    it('should dispatch stopSubmit action from for redux from response ', () => {
      const verifyAddressGen = verifyAddress({
        payload: { firstName: 'test', formName: 'checkoutShipping' },
      });
      verifyAddressGen.next();
      verifyAddressGen.next();
      verifyAddressGen.next();
      const response = {
        suggestedAddress: {
          firstName: 'test',
        },
        resultType: 'AE04',
        resultCodes: 'AE04',
      };
      verifyAddressGen.next();
      verifyAddressGen.next(response);
      const putDescriptor = verifyAddressGen.next({
        verifyAddressLabels: { formGenericError: 'some error' },
      }).value;
      expect(putDescriptor).toEqual(
        put(stopSubmit('checkoutShipping', { address: { formGeneric: 'some error' } }))
      );
    });
  });

  it('should dispatch verifyAddressError action for error resposnse', () => {
    const verifyAddressGen = verifyAddress({
      payload: { firstName: 'test' },
    });
    verifyAddressGen.next();
    verifyAddressGen.next();
    const error = new Error();
    verifyAddressGen.throw(error);
    const putDescriptor = verifyAddressGen.next().value;
    expect(putDescriptor).toEqual(put(verifyAddressError({ resultType: 'ERROR' })));
  });
});

describe('verifyAddressSaga', () => {
  it('should return correct takeLatest effect', () => {
    const generator = verifyAddressSaga();
    const takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(takeLatest(CONSTANTS.VERIFY_ADDRESS, verifyAddress));
  });
});

describe('address saga loader', () => {
  it('should dispatch setLoaderState with true', () => {
    const verifyAddressGen = verifyAddress({
      payload: { firstName: 'test' },
    });
    verifyAddressGen.next();
    const putDescriptor = verifyAddressGen.next().value;
    expect(putDescriptor).toEqual(put(setLoaderState(true)));
  });

  it('should not dispatch setLoaderState ', () => {
    const verifyAddressGen = verifyAddress({
      payload: { firstName: 'test', doNotShowLoader: true },
    });
    verifyAddressGen.next();
    const putDescriptor = verifyAddressGen.next().value;
    expect(putDescriptor).not.toEqual(put(setLoaderState(true)));
  });
});
