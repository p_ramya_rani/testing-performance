/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { formatAddress } from '@tcp/core/src/utils';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import checkoutConstants from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import { convertHtml } from '@tcp/core/src/components/features/CnC/LoyaltyBanner/util/utility';
import Modal from '../../../molecules/Modal';
import Button from '../../../atoms/Button';
import Anchor from '../../../atoms/Anchor';
import TextBox from '../../../atoms/TextBox';
import BodyCopy from '../../../atoms/BodyCopy';
import AddressOption from '../../../molecules/AddressOption';
import Notification from '../../../molecules/Notification';
import styles from '../styles/AddressVerification.style';
import CONSTANTS from '../AddressVerification.constants';
import spacing from '../../../../../../styles/themes/TCP/spacing';

export class AddressVerification extends React.Component {
  static propTypes = {
    heading: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
    verificationResult: PropTypes.string.isRequired,
    userAddress: PropTypes.shape({}).isRequired,
    suggestedAddress: PropTypes.shape({}).isRequired,
    resetVerifyAddressAction: PropTypes.func.isRequired,
    onSuccess: PropTypes.func.isRequired,
    onError: PropTypes.func.isRequired,
    labels: PropTypes.shape({}).isRequired,
    verificationResultCodes: PropTypes.string.isRequired,
    verifyAddressAction: PropTypes.func.isRequired,
    onEditAddressClick: PropTypes.func,
    dispatchReviewReduxForm: PropTypes.func.isRequired,
    cardType: PropTypes.string,
    fromReviewPage: PropTypes.bool,
  };

  static defaultProps = {
    onEditAddressClick: () => {},
    cardType: '',
    fromReviewPage: false,
  };

  constructor(props) {
    super(props);
    const { verifyAddressLabels } = props.labels;
    this.state = {
      selectAddress: 'suggestedAddress',
      optionalAddressLine: '',
      optionalAddressLineError:
        verifyAddressLabels && verifyAddressLabels.addressLine2Error
          ? verifyAddressLabels.addressLine2Error
          : '',
    };

    this.isValidAddress = false;
    this.showInput = false;
    this.showVerifyModal = false;
    this.showOptionalAddressLine = false;
    this.isGenericError = false;
  }

  componentDidUpdate(prevProps, prevState) {
    const { fromReviewPage } = this.props;
    if (this.isValidAddress && (!fromReviewPage || prevState.optionalAddressLine)) {
      this.onConfirm();
    } else if (this.isError) {
      const { onError, userAddress } = this.props;
      onError(userAddress);
      this.onCloseModal();
    }
  }

  updateDisplayFlag = (verificationResult, userAddress, suggestedAddress) => {
    const { verificationResultCodes } = this.props;
    const { INVALID_CODES, GENERIC_CODES, OPTIONAL_ADDRESS_LINE, NO_ACTION_CODES } =
      CONSTANTS.VERIFY_ERROR_CODES_MAPPING;
    if (verificationResult) {
      const status = CONSTANTS.VERIFY_ADDRESS_STATUS_MAP[verificationResult];
      this.showOptionalAddressLine = verificationResultCodes.includes(OPTIONAL_ADDRESS_LINE);
      this.isValidAddress =
        NO_ACTION_CODES.find((code) => verificationResultCodes.includes(code)) ||
        !INVALID_CODES.find((code) => verificationResultCodes.includes(code));

      this.isGenericError = GENERIC_CODES.find((code) => verificationResultCodes.includes(code));
      this.showInput =
        (suggestedAddress &&
          !this.isValidAddress &&
          !this.showOptionalAddressLine &&
          !this.isGenericError) ||
        false;
      this.showVerifyModal = !this.isValidAddress || false;
      this.isError = status === CONSTANTS.VERIFY_ADDRESS_RESULT.ERROR;
    } else {
      this.showOptionalAddressLine = false;
      this.isValidAddress = false;
      this.showVerifyModal = false;
      this.showInput = false;
      this.isError = false;
    }
  };

  getMessage = (verificationResult) => {
    const {
      labels: { verifyAddressLabels },
    } = this.props;
    let message = verifyAddressLabels[verificationResult];
    let status = '';
    if (this.isGenericError) {
      message = verifyAddressLabels[`modal${this.isGenericError}`];
      status = 'error';
      message = message ? convertHtml(message) : '';
    } else if (this.showOptionalAddressLine) {
      message = verifyAddressLabels.modalSubPremiseError;
      status = 'error';
    }
    return this.isGenericError || this.showOptionalAddressLine ? (
      <Notification status={status} message={message} isErrorTriangleIcon isResetFontWeight />
    ) : (
      <div className="elem-mb-MED">
        <BodyCopy
          component="p"
          fontSize="fs14"
          textAlign="center"
          color={
            CONSTANTS.VERIFY_ADDRESS_STATUS_MAP[verificationResult] ===
            CONSTANTS.VERIFY_ADDRESS_RESULT.INVALID_ERROR
              ? 'error'
              : 'text.primary'
          }
          fontWeight="extrabold"
          fontFamily="secondary"
          data-locator="verifyaddress-msg"
        >
          {message}
        </BodyCopy>
      </div>
    );
  };

  onConfirm = () => {
    const { selectAddress, optionalAddressLine } = this.state;
    const { onSuccess, userAddress, suggestedAddress } = this.props;
    let addressPayload = {};

    if (!this.isGenericError && !this.showOptionalAddressLine && suggestedAddress) {
      const { validationCode } = suggestedAddress;
      addressPayload =
        selectAddress === 'userAddress' || optionalAddressLine
          ? Object.assign(addressPayload, { ...userAddress, validationCode })
          : Object.assign(addressPayload, suggestedAddress);
    } else {
      const { validationCode } = suggestedAddress || {};
      addressPayload = Object.assign(addressPayload, {
        ...userAddress,
        ...(validationCode && { validationCode }),
      });
    }
    if (optionalAddressLine) {
      addressPayload.address2 = optionalAddressLine;
    }
    onSuccess(addressPayload);
    this.onCloseModal();
  };

  onCloseModal = () => {
    const {
      resetVerifyAddressAction,
      dispatchReviewReduxForm,
      cardType,
      couponRemovedFromOrderFlag,
    } = this.props;
    const { optionalAddressLine } = this.state;
    if (optionalAddressLine) {
      this.setState({
        optionalAddressLine: '',
      });
    }
    if (cardType === checkoutConstants.APPLEPAY_LABEL && !couponRemovedFromOrderFlag) {
      dispatchReviewReduxForm();
    }
    resetVerifyAddressAction();
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      optionalAddressLineError: '',
    });
  };

  checkForAddressWarning = () => {
    const { AE01, AE08 } = CONSTANTS.ERROR_CODES;
    if (this.isGenericError) {
      switch (this.isGenericError) {
        case AE01:
          return 3;
        case AE08:
          return 2;
        default:
          return 1;
      }
    }
    return 0;
  };

  renderUserAddress = (verificationResult, userAddress) => {
    const {
      labels: { verifyAddressLabels },
    } = this.props;
    const { selectAddress } = this.state;
    const showWarning = this.checkForAddressWarning();
    return (
      <div
        className={`addressVerification__section layout-pr-LRG elem-mb-XL ${
          this.showInput ? 'layout-pl-LRG' : 'layout-pl-XL'
        }`}
      >
        <BodyCopy
          component="p"
          fontFamily="secondary"
          fontWeight="extrabold"
          fontSize="fs14"
          className="elem-mb-SM you-entered-lbl"
          data-locator="verifyaddress-youenteredlbl"
        >
          {verifyAddressLabels.youEntered}
        </BodyCopy>
        <div className="elem-mb-XL">
          <AddressOption
            className="addressVerification__input"
            address={formatAddress(userAddress)}
            name="selectAddress"
            value="userAddress"
            isSelected={selectAddress === 'userAddress'}
            onChange={this.handleChange}
            showInput={this.showInput}
            fontWeight={showWarning ? 'normal' : 'extrabold'}
            inputProps={{
              'data-locator': 'verifyaddress-enteredradio',
            }}
            showWarning={showWarning}
            showProminentWarning={showWarning}
          />
        </div>
      </div>
    );
  };

  renderSuggestedAddress = (verificationResult, suggestedAddress) => {
    const {
      labels: { verifyAddressLabels },
    } = this.props;
    if (this.showInput) {
      const { selectAddress } = this.state;
      return (
        <div className="addressVerification__section addressVerification__section--noBorder layout-pl-LRG layout-pr-LRG">
          <BodyCopy
            component="p"
            fontFamily="secondary"
            fontWeight="extrabold"
            fontSize="fs14"
            className="elem-mb-SM"
            data-locator="verifyaddress-wesuggestlbl"
          >
            {verifyAddressLabels.weSuggest}
          </BodyCopy>
          <div className="elem-mb-XL">
            <AddressOption
              className="addressVerification__input"
              address={formatAddress(suggestedAddress)}
              name="selectAddress"
              value="suggestedAddress"
              isSelected={selectAddress === 'suggestedAddress'}
              onChange={this.handleChange}
              fontWeight="extrabold"
              inputProps={{
                'data-locator': 'verifyaddress-suggestedradio',
              }}
              showInput
            />
          </div>
        </div>
      );
    }
    if (this.showOptionalAddressLine) {
      const { optionalAddressLine, optionalAddressLineError } = this.state;
      return (
        <div className="addressVerification__section addressVerification__section--noBorder elem-mb-XL layout-pl-XL">
          <TextBox
            input={{
              value: optionalAddressLine,
              onChange: this.handleChange,
              name: 'optionalAddressLine',
            }}
            enableSuccessCheck={false}
            placeholder={verifyAddressLabels.addressLine2PlaceHolder}
            id="optionalAddressLine"
            dataLocator="verifyaddress-addressLine2InPopUp"
            showExplicitError={optionalAddressLineError}
          />
        </div>
      );
    }

    return null;
  };

  renderContinueCTA = (verifyAddressLabels, showUnVerified) => {
    return showUnVerified ? (
      <Anchor
        fontSizeVariation="large"
        anchorVariation="primary"
        underline
        noLink
        target="_blank"
        handleLinkClick={(e) => {
          e.preventDefault();
          this.onConfirm();
        }}
      >
        {verifyAddressLabels.useUnverifiedBtn}
      </Anchor>
    ) : (
      <ClickTracker
        as={Button}
        className="addressVerification__cta"
        buttonVariation="variable-width"
        fill={showUnVerified ? 'RED' : 'BLUE'}
        onClick={this.onConfirm}
        data-locator="verifyaddress-continuebtn"
      >
        {verifyAddressLabels.continueCta}
      </ClickTracker>
    );
  };

  reVerifyAddress = () => {
    const { optionalAddressLine } = this.state;
    if (optionalAddressLine) {
      const { verifyAddressAction, suggestedAddress } = this.props;
      const newAddress = {
        ...suggestedAddress,
        address2: optionalAddressLine,
      };
      verifyAddressAction(newAddress);
    } else {
      const {
        labels: { verifyAddressLabels },
      } = this.props;
      this.setState({
        optionalAddressLineError:
          verifyAddressLabels && verifyAddressLabels.addressLine2Error
            ? verifyAddressLabels.addressLine2Error
            : '',
      });
    }
  };

  renderEditAddress = (verifyAddressLabels, showUnverified, onEditAddressClick) => {
    const { resetVerifyAddressAction } = this.props;
    const { optionalAddressLine } = this.state;
    const onCLickEvent = () => {
      if (this.showOptionalAddressLine && showUnverified) {
        return this.reVerifyAddress;
      }
      return () => {
        onEditAddressClick();
        if (optionalAddressLine) {
          this.setState({
            optionalAddressLine: '',
          });
        }
        resetVerifyAddressAction();
      };
    };
    return (
      <Button
        className="addressVerification__cta"
        buttonVariation="variable-width"
        onClick={onCLickEvent()}
        fill={showUnverified ? 'BLUE' : 'RED'}
        data-locator="verifyaddress-editaddressbtn"
      >
        {this.showOptionalAddressLine && showUnverified
          ? verifyAddressLabels.updateBtn
          : verifyAddressLabels.editAddress}
      </Button>
    );
  };

  render() {
    const {
      className,
      verificationResult,
      userAddress,
      suggestedAddress,
      labels: { verifyAddressLabels },
      heading,
      onEditAddressClick,
    } = this.props;
    this.updateDisplayFlag(verificationResult, userAddress, suggestedAddress);
    const showUnverified = this.isGenericError || this.showOptionalAddressLine;
    if (this.showVerifyModal) {
      return (
        <Modal
          colSet={{ large: 6, medium: 8, small: 6 }}
          isOpen
          onRequestClose={this.onCloseModal}
          overlayClassName="TCPModal__Overlay"
          className={`${className} TCPModal__Content`}
          heading={heading}
          fixedWidth
          contentHeader={verifyAddressLabels.verifyHeader}
          maxWidth={spacing.MODAL_WIDTH.SMALL}
          heightConfig={{ maxHeight: '100%', height: 'auto' }}
        >
          <div className="addressVerification">
            <BodyCopy
              component="p"
              fontSize="fs22"
              fontWeight="semibold"
              fontFamily="secondary"
              textAlign="center"
              className="elem-pt-LRG elem-mb-MED"
            >
              {verifyAddressLabels.verifyHeader}
            </BodyCopy>
            {this.getMessage(verificationResult, suggestedAddress)}
            {this.renderUserAddress(verificationResult, userAddress, suggestedAddress)}
            {this.renderSuggestedAddress(verificationResult, suggestedAddress)}
            <div className="addressVerification__ctaContainer">
              {showUnverified ? (
                <>
                  {this.renderEditAddress(verifyAddressLabels, showUnverified, onEditAddressClick)}
                  {this.renderContinueCTA(verifyAddressLabels, showUnverified)}
                </>
              ) : (
                <>
                  {this.renderContinueCTA(verifyAddressLabels)}
                  {this.renderEditAddress(verifyAddressLabels, null, onEditAddressClick)}
                </>
              )}
            </div>
          </div>
        </Modal>
      );
    }

    return null;
  }
}

export default withStyles(AddressVerification, styles);
