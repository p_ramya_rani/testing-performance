// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import TextBox from '@tcp/core/src/components/common/atoms/TextBox';
import Notification from '../../../../molecules/Notification';
import { AddressVerificationVanilla } from '../AddressVerification.view.native';
import AddressOption from '../../../../molecules/AddressOption';

const userAddress = {
  firstName: 'test',
  lastName: 'test',
  address1: 'test line 1',
  address2: '',
  city: 'test city',
  state: 'test state',
  zip: '11111',
};

const suggestedAddress = Object.assign({}, userAddress, {
  address1: 'suggested line 1',
  isCommercialAddress: false,
});

const heading = 'Add Address';

describe('AddressVerification component', () => {
  describe('for valid address', () => {
    let component;
    let onSuccessSpy;
    let resetVerifyAddressActionSpy;
    beforeEach(() => {
      onSuccessSpy = jest.fn();
      resetVerifyAddressActionSpy = jest.fn();
      const props = {
        heading,
        userAddress,
        suggestedAddress: userAddress,
        verificationResult: '',
        labels: { verifyAddressLabels: {} },
        onSuccess: onSuccessSpy,
        resetVerifyAddressAction: resetVerifyAddressActionSpy,
      };
      component = shallow(<AddressVerificationVanilla {...props} />);
      component.setProps({
        verificationResult: 'AS01',
        verificationResultCodes: 'AS01',
      });
    });

    it('should not render anything', () => {
      expect(component.isEmptyRender()).toBeTruthy();
    });

    it('should call onSuccess prop with user address', () => {
      expect(onSuccessSpy.mock.calls[0][0]).toEqual(userAddress);
    });
  });

  describe('for invalid address', () => {
    let component;
    let onSuccessSpy;
    let resetVerifyAddressActionSpy;

    beforeEach(() => {
      onSuccessSpy = jest.fn();
      resetVerifyAddressActionSpy = jest.fn();
      const props = {
        heading,
        userAddress,
        suggestedAddress,
        verificationResult: 'AE',
        labels: { verifyAddressLabels: {} },
        onSuccess: onSuccessSpy,
        resetVerifyAddressAction: resetVerifyAddressActionSpy,
      };
      component = shallow(<AddressVerificationVanilla {...props} />);
      component.setProps({
        verificationResult: 'AE08',
        verificationResultCodes: 'AE08',
      });
    });

    it('should show both user address and suggested address', () => {
      expect(component.find(AddressOption)).toHaveLength(1);
      expect(component.find(Notification)).toHaveLength(1);
    });

    it('should show userAddress and input box for AddressLine 2', () => {
      component.setProps({
        verificationResult: 'AE09',
        verificationResultCodes: 'AE09',
      });
      expect(component.find(AddressOption)).toHaveLength(1);
      expect(component.find(Notification)).toHaveLength(1);
      expect(component.find(TextBox)).toHaveLength(1);
    });
  });

  describe('for invalid address from review page', () => {
    let component;
    let onSuccessSpy;
    let resetVerifyAddressActionSpy;
    let props;

    beforeEach(() => {
      onSuccessSpy = jest.fn();
      resetVerifyAddressActionSpy = jest.fn();
      props = {
        heading,
        userAddress,
        suggestedAddress,
        verificationResult: 'AE',
        labels: { verifyAddressLabels: {} },
        onSuccess: onSuccessSpy,
        resetVerifyAddressAction: resetVerifyAddressActionSpy,
        fromReviewPage: true,
        closeAddAddressVerificationModal: jest.fn(),
      };
      component = shallow(<AddressVerificationVanilla {...props} />);
      component.setProps({
        verificationResult: 'AE08',
        verificationResultCodes: 'AE08',
      });
    });

    it('should show notification', () => {
      expect(component.find(Notification)).toHaveLength(1);
    });

    it('should show userAddress notification', () => {
      component.setProps({
        verificationResult: 'AE09',
        verificationResultCodes: 'AE09',
      });
      expect(component.find(Notification)).toHaveLength(1);
    });

    it('should test closeModal method from review page', () => {
      component.instance().closeModal({ target: '' });
      expect(props.closeAddAddressVerificationModal).toHaveBeenCalledWith({ target: '' });
    });

    it('should test closeModal method from review page for apple pay', () => {
      component.setProps({ cardType: 'APPLE PAY' });
      component.instance().closeModal({ target: '' });
      expect(props.closeAddAddressVerificationModal).toHaveBeenCalledWith(true);
    });

    it('should test onConfirm method from review page', () => {
      component.instance().onConfirm({ target: '' });
      expect(props.onSuccess).toHaveBeenCalledWith({
        ...userAddress,
      });
    });

    it('should test onConfirm method from review page for apple pay', () => {
      component.setProps({ cardType: 'APPLE PAY' });
      component.instance().onConfirm({ target: '' });
      expect(props.onSuccess).toHaveBeenCalledWith({
        ...userAddress,
        isApplePay: true,
      });
    });
  });

  describe('for invalid address with no suggestion', () => {
    let component;
    let onSuccessSpy;
    let resetVerifyAddressActionSpy;

    beforeEach(() => {
      onSuccessSpy = jest.fn();
      resetVerifyAddressActionSpy = jest.fn();
      const props = {
        heading,
        userAddress,
        verificationResult: 'AE',
        labels: { verifyAddressLabels: {} },
        onSuccess: onSuccessSpy,
        resetVerifyAddressAction: resetVerifyAddressActionSpy,
      };
      component = shallow(<AddressVerificationVanilla {...props} />);
      component.setProps({
        verificationResult: 'AC02',
        verificationResultCodes: 'AC02, AS01',
      });
    });

    it('should show only user address', () => {
      expect(component.find(AddressOption)).toHaveLength(2);
    });

    it('should show input radio', () => {
      expect(component.instance().showInput).toEqual(true);
    });
  });

  describe('#instances', () => {
    let component;
    let toggleAddressModalSpy;
    let resetVerifyAddressActionSpy;

    beforeEach(() => {
      toggleAddressModalSpy = jest.fn();
      resetVerifyAddressActionSpy = jest.fn();
      const props = {
        heading,
        userAddress,
        suggestedAddress: userAddress,
        verificationResult: 'AC02',
        labels: { verifyAddressLabels: {} },
        toggleAddressModal: toggleAddressModalSpy,
        resetVerifyAddressAction: resetVerifyAddressActionSpy,
      };
      component = shallow(<AddressVerificationVanilla {...props} />);
    });

    it('#onClose should call toggleAddressModal if prop is present', () => {
      component.instance().onClose();
      expect(toggleAddressModalSpy).toBeCalled();
    });

    it('#onClose should call resetVerifyAddressAction if toggleAddressModal prop is not present', () => {
      component.setProps({
        toggleAddressModal: false,
      });
      component.instance().onClose();
      expect(resetVerifyAddressActionSpy).toBeCalled();
    });

    it('should render correctly', () => {
      expect(component).toMatchSnapshot();
    });

    it('#handleUserAddress should set selectAddress to userAddress', () => {
      component.instance().handleUserAddress();
      expect(component.state('selectAddress')).toBe('userAddress');
    });

    it('#handleSuggestAddress should set selectAddress to suggestedAddress', () => {
      component.instance().handleSuggestAddress();
      expect(component.state('selectAddress')).toBe('suggestedAddress');
    });
  });
});
