/* eslint-disable sonarjs/no-duplicate-string, max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ScrollView, SafeAreaView } from 'react-native';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { PropTypes } from 'prop-types';
import CustomButton from '@tcp/core/src/components/common/atoms/Button';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import { formatAddress } from '@tcp/core/src/utils';
import TextBox from '@tcp/core/src/components/common/atoms/TextBox';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import checkoutConstants from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import RichText from '@tcp/core/src/components/common/atoms/RichText';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import {
  AddressVerificationContainer,
  ButtonWrapper,
  MessageWrapper,
  AddressOptionWrapper,
  EnteredWrapper,
  EnteredSectionWrapper,
  SuggestWrapper,
  SuggestSectionWrapper,
  VerifyAddressWrapper,
  OptionAddressLineWrapper,
  NotificationMessageWrapper,
} from '../styles/AddressVerification.style.native';
import CONSTANTS from '../AddressVerification.constants';
import AddressOption from '../../../molecules/AddressOption';
import ModalNative from '../../../molecules/Modal';
import Notification from '../../../molecules/Notification';

const colorPallete = createThemeColorPalette();

export default class AddressVerification extends React.PureComponent {
  constructor(props) {
    super(props);
    const { verifyAddressLabels } = props.labels;
    this.state = {
      selectAddress: 'suggestedAddress',
      optionalAddressLine: '',
      optionalAddressLineError:
        verifyAddressLabels && verifyAddressLabels.addressLine2Error
          ? verifyAddressLabels.addressLine2Error
          : '',
    };

    this.isValidAddress = false;
    this.showInput = false;
    this.showVerifyModal = false;
    this.showOptionalAddressLine = false;
    this.isGenericError = false;
  }

  componentDidMount() {
    const { fromReviewPage } = this.props;
    if (this.isValidAddress && !fromReviewPage) {
      this.onConfirm();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { verificationResult, userAddress, onError, fromReviewPage } = this.props;
    if (verificationResult !== prevProps.verificationResult) {
      if (this.isValidAddress && (!fromReviewPage || prevState.optionalAddressLine)) {
        this.onConfirm();
      } else if (this.isError) {
        onError(userAddress);
      }
    }
  }

  componentWillUnmount() {
    const { resetVerifyAddressAction } = this.props;
    const { optionalAddressLine } = this.state;
    if (optionalAddressLine) {
      this.setState({
        optionalAddressLine: '',
      });
    }

    resetVerifyAddressAction();
  }

  onConfirm = () => {
    const { selectAddress, optionalAddressLine } = this.state;
    const { onSuccess, userAddress, suggestedAddress, cardType } = this.props;
    let addressPayload = {};

    if (!this.isGenericError && !this.showOptionalAddressLine && suggestedAddress) {
      const { validationCode } = suggestedAddress;
      addressPayload =
        selectAddress === 'userAddress' || optionalAddressLine
          ? { ...addressPayload, ...userAddress, validationCode }
          : { ...addressPayload, ...suggestedAddress };
    } else {
      const { validationCode } = suggestedAddress || {};
      addressPayload = {
        ...addressPayload,
        ...userAddress,
        ...(validationCode && { validationCode }),
      };
    }
    if (optionalAddressLine) {
      addressPayload.address2 = optionalAddressLine;
    }

    if (cardType === checkoutConstants.APPLEPAY_LABEL) {
      addressPayload.isApplePay = true;
    }
    onSuccess(addressPayload);
  };

  closeModal = (event) => {
    const { closeAddAddressVerificationModal, cardType, fromReviewPage } = this.props;
    closeAddAddressVerificationModal(
      fromReviewPage && cardType === checkoutConstants.APPLEPAY_LABEL ? true : event
    );
  };

  onClose = () => {
    const {
      toggleAddressModal,
      resetVerifyAddressAction,
      plccOnClose,
      scrollViewRef,
      shippingFormRef,
    } = this.props;
    this.setState({
      optionalAddressLine: '',
    });

    if (toggleAddressModal) {
      toggleAddressModal(scrollViewRef, shippingFormRef);
    } else {
      resetVerifyAddressAction();
    }

    if (plccOnClose) {
      plccOnClose();
    }
  };

  getMessage = (verificationResult) => {
    const {
      labels: { verifyAddressLabels },
      fromCheckoutShipping,
    } = this.props;
    let message = verifyAddressLabels[verificationResult];
    let status = '';
    if (this.isGenericError) {
      message = verifyAddressLabels[`modal${this.isGenericError}`];
      status = 'error';
    } else if (this.showOptionalAddressLine) {
      message = verifyAddressLabels.modalSubPremiseError;
      status = 'error';
    }
    if (this.isGenericError || this.showOptionalAddressLine) {
      return (
        <NotificationMessageWrapper fromCheckoutShipping={fromCheckoutShipping}>
          <Notification status={status} fontWeight="regular" disableSpace isErrorTriangleIcon>
            <ViewWithSpacing spacingStyles="padding-left-MED padding-right-MED">
              <RichText source={`${message}`} isNativeView={true} />
            </ViewWithSpacing>
          </Notification>
        </NotificationMessageWrapper>
      );
    }
    if (message) {
      return (
        <MessageWrapper>
          <BodyCopy
            fontSize="fs14"
            textAlign="center"
            color={
              CONSTANTS.VERIFY_ADDRESS_STATUS_MAP[verificationResult] ===
              CONSTANTS.VERIFY_ADDRESS_RESULT.INVALID_ERROR
                ? 'error'
                : 'black'
            }
            mobilefontFamily={['secondary']}
            text={message}
          />
        </MessageWrapper>
      );
    }
    return <MessageWrapper />;
  };

  handleUserAddress = () => {
    this.setState({
      selectAddress: 'userAddress',
    });
  };

  handleSuggestAddress = () => {
    this.setState({
      selectAddress: 'suggestedAddress',
    });
  };

  updateDisplayFlag = (verificationResult, userAddress, suggestedAddress) => {
    const { verificationResultCodes } = this.props;
    const { INVALID_CODES, GENERIC_CODES, OPTIONAL_ADDRESS_LINE, NO_ACTION_CODES } =
      CONSTANTS.VERIFY_ERROR_CODES_MAPPING;
    if (verificationResult) {
      const status = CONSTANTS.VERIFY_ADDRESS_STATUS_MAP[verificationResult];
      this.showOptionalAddressLine = verificationResultCodes.includes(OPTIONAL_ADDRESS_LINE);
      this.isValidAddress =
        NO_ACTION_CODES.find((code) => verificationResultCodes.includes(code)) ||
        !INVALID_CODES.find((code) => verificationResultCodes.includes(code));

      this.isGenericError = GENERIC_CODES.find((code) => verificationResultCodes.includes(code));
      this.showInput =
        (suggestedAddress &&
          !this.isValidAddress &&
          !this.showOptionalAddressLine &&
          !this.isGenericError) ||
        false;
      this.showVerifyModal = !this.isValidAddress || false;
      this.isError = status === CONSTANTS.VERIFY_ADDRESS_RESULT.ERROR;
    } else {
      this.showOptionalAddressLine = false;
      this.isValidAddress = false;
      this.showVerifyModal = false;
      this.showInput = false;
      this.isError = false;
    }
  };

  checkForAddressWarning = () => {
    const { AE01, AE08 } = CONSTANTS.ERROR_CODES;
    if (this.isGenericError) {
      switch (this.isGenericError) {
        case AE01:
          return 3;
        case AE08:
          return 2;
        default:
          return 1;
      }
    }
    return 0;
  };

  renderUserAddress = (userAddress) => {
    const {
      labels: { verifyAddressLabels },
    } = this.props;
    const { selectAddress } = this.state;
    return (
      <EnteredSectionWrapper>
        <EnteredWrapper showInput={this.showInput}>
          <BodyCopy
            fontSize="fs14"
            color="black"
            mobilefontFamily={['secondary']}
            fontWeight="extrabold"
            data-locator="verifyaddress-youenteredlbl"
            text={verifyAddressLabels.youEntered}
          />
        </EnteredWrapper>

        <AddressOptionWrapper showInput={this.showInput}>
          <AddressOption
            name="selectAddress"
            address={formatAddress(userAddress || {})}
            value="userAddress"
            isSelected={selectAddress === 'userAddress'}
            onChange={this.handleUserAddress}
            showInput={this.showInput}
            inputProps={{
              'data-locator': 'verifyaddress-enteredradio',
            }}
            showWarning={this.checkForAddressWarning()}
            showProminentWarning={this.checkForAddressWarning()}
          />
        </AddressOptionWrapper>
      </EnteredSectionWrapper>
    );
  };

  renderSuggestedAddress = (verificationResult, suggestedAddress) => {
    const {
      labels: { verifyAddressLabels },
    } = this.props;
    if (this.showInput) {
      const { selectAddress } = this.state;
      return (
        <SuggestSectionWrapper>
          <SuggestWrapper showInput={this.showInput}>
            <BodyCopy
              fontSize="fs14"
              color="black"
              mobilefontFamily={['secondary']}
              fontWeight="extrabold"
              data-locator="verifyaddress-wesuggestlbl"
              text={verifyAddressLabels.weSuggest}
            />
          </SuggestWrapper>

          <AddressOptionWrapper showInput={this.showInput}>
            <AddressOption
              name="selectAddress"
              address={formatAddress(suggestedAddress)}
              value="suggestedAddress"
              isSelected={selectAddress === 'suggestedAddress'}
              onChange={this.handleSuggestAddress}
              showInput
              inputProps={{
                'data-locator': 'verifyaddress-suggestedradio',
              }}
            />
          </AddressOptionWrapper>
        </SuggestSectionWrapper>
      );
    }
    if (this.showOptionalAddressLine) {
      const { optionalAddressLine, optionalAddressLineError } = this.state;
      return (
        <SuggestSectionWrapper>
          <OptionAddressLineWrapper>
            <TextBox
              input={{
                value: optionalAddressLine,
                name: 'optionalAddressLine',
              }}
              label={verifyAddressLabels.addressLine2PlaceHolder}
              onChangeText={(text) =>
                this.setState({ optionalAddressLine: text, optionalAddressLineError: '' })
              }
              id="optionalAddressLine"
              dataLocator="verifyaddress-addressLine2InPopUp"
              showExplicitError={optionalAddressLineError}
            />
          </OptionAddressLineWrapper>
        </SuggestSectionWrapper>
      );
    }

    return null;
  };

  renderContinue = (verifyAddressLabels, showUnverified) => {
    return (
      <ButtonWrapper>
        {showUnverified ? (
          <Anchor
            fontSizeVariation="large"
            anchorVariation="custom"
            colorName="gray.900"
            underline
            onPress={(e) => {
              e.preventDefault();
              this.onConfirm();
            }}
            text={verifyAddressLabels.useUnverifiedBtn}
          />
        ) : (
          <CustomButton
            color="white"
            fill="BLUE"
            text={verifyAddressLabels.continueCta}
            onPress={this.onConfirm}
          />
        )}
      </ButtonWrapper>
    );
  };

  reVerifyAddress = () => {
    const { optionalAddressLine } = this.state;
    if (optionalAddressLine) {
      const { verifyAddressAction, userAddress } = this.props;
      const newAddress = {
        ...userAddress,
        address2: optionalAddressLine,
      };
      verifyAddressAction(newAddress);
    } else {
      const {
        labels: { verifyAddressLabels },
      } = this.props;
      this.setState({
        optionalAddressLineError:
          verifyAddressLabels && verifyAddressLabels.addressLine2Error
            ? verifyAddressLabels.addressLine2Error
            : '',
      });
    }
  };

  renderEditAddress = (verifyAddressLabels, showUnverified) => {
    const { onReviewEditClick, fromReviewPage } = this.props;
    return (
      <ButtonWrapper>
        <CustomButton
          color={showUnverified ? colorPallete.white : colorPallete.red[300]}
          fill={showUnverified ? 'BLUE' : 'WHITE'}
          text={
            this.showOptionalAddressLine && showUnverified
              ? verifyAddressLabels.updateBtn
              : verifyAddressLabels.editAddress
          }
          onPress={() => {
            if (fromReviewPage && onReviewEditClick && !this.showOptionalAddressLine) {
              return onReviewEditClick();
            }
            return this.showOptionalAddressLine && showUnverified
              ? this.reVerifyAddress()
              : this.onClose();
          }}
        />
      </ButtonWrapper>
    );
  };

  renderButtons = () => {
    const {
      labels: { verifyAddressLabels },
    } = this.props;
    const showUnverified = this.isGenericError || this.showOptionalAddressLine;
    return (
      <AddressVerificationContainer>
        {showUnverified ? (
          <>
            {this.renderEditAddress(verifyAddressLabels, showUnverified)}
            {this.renderContinue(verifyAddressLabels, showUnverified)}
          </>
        ) : (
          <>
            {this.renderContinue(verifyAddressLabels)}
            {this.renderEditAddress(verifyAddressLabels)}
          </>
        )}
      </AddressVerificationContainer>
    );
  };

  renderAddressVerificationContent() {
    const {
      verificationResult,
      userAddress,
      suggestedAddress,
      labels: { verifyAddressLabels },
    } = this.props;
    return (
      <>
        <ScrollView
          showsVerticalScrollIndicator={false}
          {...this.props}
          keyboardShouldPersistTaps="handled"
        >
          <VerifyAddressWrapper>
            <BodyCopy
              mobilefontFamily={['secondary']}
              fontSize="fs22"
              textAlign="center"
              color="gray.900"
              fontWeight="semibold"
              text={verifyAddressLabels.verifyHeader}
            />
            {this.getMessage(verificationResult)}
            {this.renderUserAddress(userAddress)}
            {this.renderSuggestedAddress(verificationResult, suggestedAddress)}
            {this.renderButtons()}
          </VerifyAddressWrapper>
        </ScrollView>
      </>
    );
  }

  render() {
    const {
      verificationResult,
      userAddress,
      suggestedAddress,
      setModalHeading,
      verifyModalRendered,
      fromCheckoutShipping,
      fromReviewPage,
      showAddressVerification,
      labelsFromShippingPage,
    } = this.props;
    this.updateDisplayFlag(verificationResult, userAddress, suggestedAddress);
    const showModal = fromCheckoutShipping || fromReviewPage;
    const modalHeading = fromReviewPage
      ? ''
      : getLabelValue(labelsFromShippingPage, 'lbl_shipping_addNewAddress', 'shipping', 'checkout');
    if (this.showVerifyModal) {
      setModalHeading();
      verifyModalRendered(true);
      return showModal ? (
        <ModalNative
          isOpen={showAddressVerification}
          onRequestClose={this.closeModal}
          heading={modalHeading}
          showOnlyCloseIcon={fromReviewPage}
          headingFontFamily="secondary"
        >
          <SafeAreaView>{this.renderAddressVerificationContent()}</SafeAreaView>
        </ModalNative>
      ) : (
        this.renderAddressVerificationContent()
      );
    }
    verifyModalRendered(false);
    return null;
  }
}

AddressVerification.propTypes = {
  labels: PropTypes.shape({
    verifyAddressLabels: PropTypes.shape({
      verifyHeader: PropTypes.string,
      continueCta: PropTypes.string,
      editAddress: PropTypes.string,
    }),
  }),
  verificationResult: PropTypes.string,
  suggestedAddress: PropTypes.shape({}),
  userAddress: PropTypes.shape({}),
  onError: PropTypes.func,
  onSuccess: PropTypes.func,
  resetVerifyAddressAction: PropTypes.func,
  toggleAddressModal: PropTypes.func,
  setModalHeading: PropTypes.func,
  verifyModalRendered: PropTypes.func,
  isValidAddress: PropTypes.bool,
  plccOnClose: PropTypes.func,
  fromCheckoutShipping: PropTypes.bool,
  showAddressVerification: PropTypes.bool,
  fromReviewPage: PropTypes.bool,
  closeAddAddressVerificationModal: PropTypes.func,
  onReviewEditClick: PropTypes.func,
  labelsFromShippingPage: PropTypes.shape(),
  scrollViewRef: PropTypes.node,
  shippingFormRef: PropTypes.node,
  verificationResultCodes: PropTypes.string,
  verifyAddressAction: PropTypes.func,
  cardType: PropTypes.string,
};

AddressVerification.defaultProps = {
  labels: {
    verifyAddressLabels: {
      verifyHeader: '',
      continueCta: '',
      editAddress: '',
    },
  },
  verificationResult: '',
  suggestedAddress: {},
  userAddress: {},
  onError: () => {},
  onSuccess: () => {},
  resetVerifyAddressAction: () => {},
  toggleAddressModal: () => {},
  setModalHeading: () => {},
  verifyModalRendered: () => {},
  isValidAddress: false,
  plccOnClose: () => {},
  fromCheckoutShipping: false,
  fromReviewPage: false,
  showAddressVerification: false,
  closeAddAddressVerificationModal: () => {},
  onReviewEditClick: () => {},
  labelsFromShippingPage: {},
  scrollViewRef: null,
  shippingFormRef: null,
  verificationResultCodes: '',
  verifyAddressAction: () => {},
  cardType: '',
};

export { AddressVerification as AddressVerificationVanilla };
