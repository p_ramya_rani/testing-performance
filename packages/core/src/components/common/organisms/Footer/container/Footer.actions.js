// 9fbef606107a605d69c0edbcd8029e5d 
import FOOTER_CONSTANTS from './Footer.constants';

export const loadFooterData = payload => {
  return {
    payload,
    type: FOOTER_CONSTANTS.LOAD_FOOTER_DATA,
  };
};

export default {
  loadFooterData,
};
