// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isMobileApp } from '@tcp/core/src/utils';
import selectors, {
  isGuest as isGuestUser,
} from '../../../../features/CnC/Checkout/container/Checkout.selector';
import {
  getVenmoClientToken,
  setVenmoData,
  setVenmoPaymentInProgress,
  setVenmoInstalled,
} from '../../../../features/CnC/Checkout/container/Checkout.action';
import { getCartOrderId } from '../../../../features/CnC/CartItemTile/container/CartItemTile.selectors';
import logger from '../../../../../utils/loggerInstance';
import VenmoPaymentButton from '../views';
import VenmoOverlay from '../VenmoButtonOverlay/VenmoButtonOverlay.view';
import { modes } from './VenmoPaymentButton.util';

export class VenmoPaymentButtonContainer extends React.PureComponent {
  /**
   * This method is used for set venmo data in the checkout redux store.
   * @param {object} data - venmo reducer data to store
   */
  setVenmoData = data => {
    const { setVenmoDataAction } = this.props;
    const payload = data;
    if (data && data.details && data.details.username) {
      payload.username = data.details.username;
    }
    setVenmoDataAction({
      ...payload,
    });
  };

  /**
   * This method is called once venmo token and authorization is successfull and user has to procced to next steps of checkout/review
   * @param {string} mode - guest or registered user mode
   */
  onVenmoPaymentButtonClick = mode => {
    const { onSuccess } = this.props;
    onSuccess();
    logger.info(mode);
  };

  /**
   * This method is called once we get error or user interupted the venmo authorization flow.
   */
  onVenmoPaymentButtonError = e => {
    const { setVenmoProgress, onError } = this.props;
    setVenmoProgress(false); // Cancelling venmo progress on error
    // onError Callback on error scenario
    if (onError && e && e.error && e.error.message) {
      onError(e.error.message);
    }
    logger.error(e);
  };

  render() {
    const { setVenmoProgress, isBagPage, isVenmoBlueButton, labels, ...otherProps } = this.props;
    return !isBagPage || isMobileApp() ? (
      <VenmoPaymentButton
        setVenmoData={this.setVenmoData}
        onVenmoPaymentButtonClick={this.onVenmoPaymentButtonClick}
        onVenmoPaymentButtonError={this.onVenmoPaymentButtonError}
        setVenmoPaymentInProgress={setVenmoProgress}
        isBagPage={isBagPage}
        isVenmoBlueButton={isVenmoBlueButton}
        labels={labels}
        {...otherProps}
      />
    ) : (
      <VenmoOverlay
        onVenmoPaymentButtonClick={this.onVenmoPaymentButtonClick}
        onVenmoPaymentButtonError={this.onVenmoPaymentButtonError}
        setVenmoPaymentInProgress={setVenmoProgress}
        isVenmoBlueButton={isVenmoBlueButton}
        setVenmoData={this.setVenmoData}
        isBagPage={isBagPage}
        labels={labels}
        {...otherProps}
      />
    );
  }
}

/* istanbul ignore next */
const mapStateToProps = state => {
  const venmoClientTokenData = selectors.getVenmoClientTokenData(state);
  const { venmoSecurityToken: authorizationKey, venmoPaymentTokenAvailable } =
    venmoClientTokenData || {};
  return {
    enabled: selectors.getIsVenmoEnabled(state),
    mode: venmoPaymentTokenAvailable === 'TRUE' ? modes.PAYMENT_TOKEN : modes.CLIENT_TOKEN,
    authorizationKey,
    isNonceNotExpired: selectors.isVenmoNonceNotExpired(state),
    venmoData: selectors.getVenmoData(state),
    venmoClientTokenData,
    allowNewBrowserTab: true,
    isGuest: isGuestUser(state),
    orderId: getCartOrderId(state),
    isRemoveOOSItems: false,
    labels: selectors.getAccessibilityLabels(state),
  };
};

export const mapDispatchToProps = dispatch => ({
  setVenmoProgress: data => dispatch(setVenmoPaymentInProgress(data)),
  getVenmoPaymentTokenAction: data => dispatch(getVenmoClientToken(data)),
  setVenmoDataAction: data => dispatch(setVenmoData(data)),
  setVenmoInstalledAction: data => dispatch(setVenmoInstalled(data)),
});

VenmoPaymentButtonContainer.propTypes = {
  setVenmoDataAction: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  setVenmoProgress: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  isBagPage: PropTypes.bool,
  isVenmoBlueButton: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
};
VenmoPaymentButtonContainer.defaultProps = {
  isBagPage: false,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VenmoPaymentButtonContainer);
