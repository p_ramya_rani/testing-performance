// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { NativeModules } from 'react-native';
import { Image } from '@tcp/core/src/components/common/atoms';
import VenmoPaymentButton from '../VenmoPaymentButton.view.native';
import { modes } from '../../container/VenmoPaymentButton.util';

describe('Venmo Payment Button', () => {
  const props = {
    setVenmoData: jest.fn(),
    onVenmoPaymentButtonClick: jest.fn(),
    onVenmoPaymentButtonError: jest.fn(),
    className: 'venmo-container sc-gqjmRU gvHLxz',
    enabled: true,
    isMobile: true,
    mode: modes.PAYMENT_TOKEN,
    authorizationKey: 'encrytptedauthorizationkey',
    isNonceNotExpired: false,
    isRemoveOOSItems: false,
    venmoData: {
      venmoClientTokenData: {
        userState: 'R',
        venmoCustomerIdAvailable: 'FALSE',
        venmoIsDefaultPaymentType: 'FALSE',
        venmoPaymentTokenAvailable: 'FALSE',
        venmoSecurityToken: 'encrytptedauthorizationkey',
      },
      deviceData: '762a73c4175ca24f7b1436a440da5bd0',
      supportedByBrowser: true,
      loading: false,
    },
    venmoClientTokenData: {
      userState: 'R',
      venmoCustomerIdAvailable: 'FALSE',
      venmoIsDefaultPaymentType: 'FALSE',
      venmoPaymentTokenAvailable: 'FALSE',
      venmoSecurityToken: 'encrytptedauthorizationkey',
    },
    allowNewBrowserTab: true,
    isGuest: false,
    orderId: '3000332630',
    setVenmoPaymentInProgress: jest.fn(),
    getVenmoPaymentTokenAction: jest.fn(),
    setVenmoDataAction: jest.fn(),
  };
  const tree = shallow(<VenmoPaymentButton {...props} />);
  const componentInstance = tree.instance();

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it.skip('should render Image correctly', () => {
    expect(tree.find(Image)).toHaveLength(1);
  });

  it.skip('calling handleVenmoSuccess method', () => {
    componentInstance.handleVenmoSuccess({ nonce: 'encryptedtext' });
    expect(props.setVenmoData).toBeCalled();
    expect(props.onVenmoPaymentButtonClick).toBeCalled();
  });

  it.skip('calling handleVenmoError method', () => {
    componentInstance.handleVenmoError({ code: 'error code 400' });
    expect(props.onVenmoPaymentButtonError).toBeCalled();
  });

  it.skip('calling canCallVenmoApi method', () => {
    expect(componentInstance.canCallVenmoApi()).toEqual(false);
  });

  it.skip('calling componentDidUpdate method', () => {
    const prevProps = {
      isGuest: true,
    };
    jest.spyOn(componentInstance, 'componentDidUpdate');
    componentInstance.componentDidUpdate(prevProps);
    expect(componentInstance.componentDidUpdate).toHaveBeenCalled();
  });

  it.skip('calling componentDidMount method', () => {
    jest.spyOn(componentInstance, 'componentDidMount');
    componentInstance.componentDidMount();
    expect(componentInstance.componentDidMount).toHaveBeenCalled();
  });

  it.skip('calling componentDidMount method with nonce', () => {
    const newProps = {
      ...props,
      venmoData: {
        venmoClientTokenData: {
          userState: 'R',
          venmoCustomerIdAvailable: 'FALSE',
          venmoIsDefaultPaymentType: 'FALSE',
          venmoPaymentTokenAvailable: 'FALSE',
          venmoSecurityToken: 'encrytptedauthorizationkey',
        },
        deviceData: '762a73c4175ca24f7b1436a440da5bd0',
        supportedByBrowser: true,
        loading: false,
        nonce: 'encrypted-nonce',
      },
      isNonceNotExpired: true,
    };
    const venmoComponent = shallow(<VenmoPaymentButton {...newProps} />);
    const venmoComponentInstance = venmoComponent.instance();
    jest.spyOn(venmoComponentInstance, 'componentDidMount');
    venmoComponentInstance.componentDidMount();
    expect(venmoComponentInstance.componentDidMount).toBeCalled();
  });

  it.skip('calling fetchVenmoClientToken method', () => {
    componentInstance.fetchVenmoClientToken();
    expect(props.getVenmoPaymentTokenAction).toHaveBeenCalled();
  });

  it.skip('calling fetchVenmoClientToken method with Guest User', () => {
    const newProps = {
      ...props,
      isGuest: true,
      enabled: true,
      isNonceNotExpired: false,
    };
    const venmoComponent = shallow(<VenmoPaymentButton {...newProps} />);
    const venmoComponentInstance = venmoComponent.instance();
    venmoComponentInstance.fetchVenmoClientToken();
    expect(props.getVenmoPaymentTokenAction).toHaveBeenCalled();
  });

  it.skip('calling displayVenmoButton method', () => {
    jest.spyOn(componentInstance, 'displayVenmoButton');
    NativeModules.VenmoPayment = {
      isVenmoInstalled: jest.fn().mockReturnValue(true),
    };
    componentInstance.setState({ showVenmoAppInstalled: false });
    expect(componentInstance.displayVenmoButton()).toEqual(true);
  });

  it.skip('calling displayVenmoButton method return true', () => {
    jest.spyOn(componentInstance, 'displayVenmoButton');
    jest.spyOn(componentInstance, 'isVenmoInstalled');
    componentInstance.setState({ showVenmoAppInstalled: true });
    expect(componentInstance.displayVenmoButton()).toEqual(true);
  });
});
