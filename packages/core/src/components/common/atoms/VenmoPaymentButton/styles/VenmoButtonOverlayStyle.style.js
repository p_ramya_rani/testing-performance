// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .venmo-overlay-button {
    background-color: ${(props) => props.theme.colorPalette.white};
    width: 100%;
    height: 42px;
    border-radius: ${(props) =>
      props.theme.isGymboree ? '21px' : props.theme.spacing.ELEM_SPACING.XXS};
    border: 1px solid ${(props) => props.theme.colorPalette.venmoBorder};
  }
  .venmo-overlay-image {
    height: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    width: ${(props) => props.theme.spacing.LAYOUT_SPACING.XL};
  }
`;

export const modalStyles = css`
  .close-modal {
    top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    right: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  div.TCPModal__InnerContent {
    height: 210px;
    width: 100%;
    top: 87.5%;
    .venmo-button {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
    .venmo-overlay-cancel {
      background-color: ${(props) => props.theme.colorPalette.white};
      width: 100%;
      height: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
      border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      border: 1px solid ${(props) => props.theme.colorPalette.venmoBorder};
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
      font-size: ${(props) => props.theme.typography.fontSizes.fs15};
    }
  }
`;

export default styles;
