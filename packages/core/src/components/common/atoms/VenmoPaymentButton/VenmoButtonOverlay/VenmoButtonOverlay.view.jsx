// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import VenmoButton from '../views/VenmoPaymentButton.view';
import Modal from '../../../molecules/Modal';
import Image from '../../Image/views/Image';
import { getIconPath } from '../../../../../utils/utils';
import withStyles from '../../../hoc/withStyles';
import VenmoButtonOverlayStyle, { modalStyles } from '../styles/VenmoButtonOverlayStyle.style';

export class VenmoButtonOverlay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasVenmoClicked: false,
    };
  }

  setVenmoClicked = (isClicked) => {
    this.setState({ hasVenmoClicked: isClicked });
  };

  render() {
    const { isVenmoBlueButton, labels, className, ...otherProps } = this.props;
    const { hasVenmoClicked } = this.state;
    const venmoIcon = isVenmoBlueButton ? getIconPath('venmo-button') : getIconPath('venmo-logo');
    return (
      <div className={className}>
        {hasVenmoClicked && (
          <Modal
            isOpen={hasVenmoClicked}
            overlayClassName="TCPModal__Overlay"
            className="TCPModal__Content"
            onRequestClose={() => this.setVenmoClicked(false)}
            stickyCloseIcon
            fixedWidth
            horizontalBar={false}
            inheritedStyles={modalStyles}
          >
            <VenmoButton isVenmoBlueButton={isVenmoBlueButton} labels={labels} {...otherProps} />
            <button
              onClick={() => this.setVenmoClicked(false)}
              className="venmo-overlay-cancel"
              aria-label="Venmo Payment Button"
            >
              {labels.venmoCancel}
            </button>
          </Modal>
        )}
        {!hasVenmoClicked && (
          <div>
            <button
              onClick={() => this.setVenmoClicked(true)}
              className="venmo-overlay-button"
              aria-label="Venmo Payment Button"
            >
              <Image src={venmoIcon} alt={labels.venmoIconText} className="venmo-overlay-image" />
            </button>
          </div>
        )}
      </div>
    );
  }
}

VenmoButtonOverlay.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}),
  isVenmoBlueButton: PropTypes.bool,
};

VenmoButtonOverlay.defaultProps = {
  className: '',
  labels: {},
  isVenmoBlueButton: false,
};

export default withStyles(VenmoButtonOverlay, VenmoButtonOverlayStyle);
export { VenmoButtonOverlay as VenmoButtonOverlayVanilla };
