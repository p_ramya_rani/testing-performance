// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { Image } from '@tcp/core/src/components/common/atoms';
import { VenmoButtonOverlayVanilla } from '../VenmoButtonOverlay.view';
import { modes } from '../../container/VenmoPaymentButton.util';

describe('Venmo Overlay Modal', () => {
  const props = {
    setVenmoData: jest.fn(),
    onVenmoPaymentButtonClick: jest.fn(),
    onVenmoPaymentButtonError: jest.fn(),
    className: 'sc-gqjmRU gvHLxz',
    enabled: true,
    isMobile: true,
    mode: modes.PAYMENT_TOKEN,
    authorizationKey: 'encrytptedauthorizationkey',
    isNonceNotExpired: false,
    isRemoveOOSItems: false,
    venmoData: {
      venmoClientTokenData: {
        userState: 'R',
        venmoCustomerIdAvailable: 'FALSE',
        venmoIsDefaultPaymentType: 'FALSE',
        venmoPaymentTokenAvailable: 'FALSE',
        venmoSecurityToken: 'encrytptedauthorizationkey',
      },
      deviceData: '762a73c4175ca24f7b1436a440da5bd0',
      supportedByBrowser: true,
      loading: false,
    },
    venmoClientTokenData: {
      userState: 'R',
      venmoCustomerIdAvailable: 'FALSE',
      venmoIsDefaultPaymentType: 'FALSE',
      venmoPaymentTokenAvailable: 'FALSE',
      venmoSecurityToken: 'encrytptedauthorizationkey',
    },
    allowNewBrowserTab: true,
    isGuest: false,
    orderId: 3000332630,
    setVenmoPaymentInProgress: jest.fn(),
    getVenmoPaymentTokenAction: jest.fn(),
    setVenmoDataAction: jest.fn(),
  };

  it('should render correctly', () => {
    const component = shallow(<VenmoButtonOverlayVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render blue venmo button correctly', () => {
    const newProps = {
      ...props,
      isVenmoBlueButton: true,
    };
    const component = shallow(<VenmoButtonOverlayVanilla {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render Venmo Button correctly', () => {
    const component = shallow(<VenmoButtonOverlayVanilla {...props} />);
    expect(component.find('.venmo-overlay-button')).toHaveLength(1);
  });

  it('should render correctly when hasVenmoClicked true', () => {
    const component = shallow(<VenmoButtonOverlayVanilla {...props} />);
    component.setState({ hasVenmoClicked: true });
    expect(component).toMatchSnapshot();
  });

  it('should render cancel button correctly', () => {
    const component = shallow(<VenmoButtonOverlayVanilla {...props} />);
    component.setState({ hasVenmoClicked: true });
    expect(component.find('.venmo-overlay-cancel')).toHaveLength(1);
  });

  it('should render Image correctly', () => {
    const component = shallow(<VenmoButtonOverlayVanilla {...props} />);
    expect(component.find(Image)).toHaveLength(1);
  });

  it('should render correctly when state update', () => {
    const component = shallow(<VenmoButtonOverlayVanilla {...props} />);
    expect(component.state().hasVenmoClicked).toBe(false);
    component.setState({ hasVenmoClicked: true });
    component.instance().setVenmoClicked(false);
    expect(component.state().hasVenmoClicked).toBe(false);
  });
});
