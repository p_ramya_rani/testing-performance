// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { SeparatorVanilla } from '../views/Separator';

describe('separator', () => {
  const props = {};
  it('should render correctly', () => {
    const component = shallow(<SeparatorVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
