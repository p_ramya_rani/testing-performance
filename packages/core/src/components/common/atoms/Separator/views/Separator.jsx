// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import PropTypes from 'prop-types';
import styles from '../styles/Separator.styles';

class Separator extends React.PureComponent {
  render() {
    const { className } = this.props;
    return <hr className={`${className} separator`} />;
  }
}
Separator.propTypes = {
  className: PropTypes.string,
};
Separator.defaultProps = {
  className: '',
};
export default withStyles(Separator, styles);
export { Separator as SeparatorVanilla };
