// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  &.separator {
    background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
    height: ${(props) => (props.height ? props.height : 20)}px;
    border-width: 0;
    position: relative;
    margin-left: -50vw;
    margin-right: calc(50% - 54vw);
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-left: -15px;
      margin-right: -15px;
      right: ${(props) => (props.right ? props.right : 0)}px;
    }
    @media ${(props) => props.theme.mediaQuery.largeOnly} {
      margin-right: calc(50% - 70vw);
    }
  }
`;
export default styles;
