// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';
import {
  typography as typographyStyleSystem,
  color as colorStyleSystem,
} from '@tcp/core/styles/rwdStyleSystem';

const BodyCopyStyles = css`
  ${typographyStyleSystem}
  ${colorStyleSystem}
`;

export default BodyCopyStyles;
