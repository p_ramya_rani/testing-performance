// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ImageCaching from '../view/FastImage.native';

describe('FastImage component', () => {
  let component;
  beforeEach(() => {
    const props = {
      imageWidth: 100,
      imageHeight: 120,
      uriInfo: 'http://childrensplace.com',
      cacheType: '',
      priority: '',
      resizeMode: '',
      onLoadStart: jest.fn(),
      onProgress: jest.fn(),
      onError: jest.fn(),
      isBackground: true,
      customStyle: null,
    };
    jest.mock('../../../../../utils/utils.app', () => ({
      validateExternalUrl: jest.fn().mockImplementation(() => true),
    }));
    component = shallow(<ImageCaching {...props} />);
  });

  it('should renders correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
