// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ImageBackground } from 'react-native';
import FastImage from '@stevenmasini/react-native-fast-image';
import PropTypes from 'prop-types';
import { validateExternalUrl } from '../../../../../utils/utils.app';
import AccessibilityRoles from '../../../../../constants/AccessibilityRoles.constant';

const placeHolderImg = require('../../../../../../../mobileapp/src/assets/images/img-placeholder.png');

/**
 * @manageCaching : In this return the FastImage component for using cache purpose .
 * @web : FastImage.cacheControl.web - Use headers and follow normal caching procedures.
 * @cacheOnly : FastImage.cacheControl.web - Use headers and follow normal caching procedures.
 *
 * @immutable : FastImage.cacheControl.immutable - (Default) - Only updates if url changes.
 *
 */

const manageCaching = cacheType => {
  let cacheValue = FastImage.cacheControl.immutable;
  if (cacheType === 'web') {
    cacheValue = FastImage.cacheControl.web;
  } else if (cacheType === 'cacheOnly') {
    cacheValue = FastImage.cacheControl.cacheOnly;
  }
  return cacheValue;
};

/**
 * @managePrority : In this return the FastImage component for using cache purpose .
 * @low : FastImage.priority.low - Low Priority.
 * @normal : FastImage.priority.normal (Default) - Normal Priority.
 * @high : FastImage.priority.high - High Priority.
 */

const managePrority = priority => {
  let priorityValue = FastImage.priority.normal;
  if (priority === 'low') {
    priorityValue = FastImage.priority.low;
  } else if (priority === 'high') {
    priorityValue = FastImage.priority.high;
  }
  return priorityValue;
};

/**
 * @manageResizeMode : In this return the FastImage component for using resizing purpose .
 *
 * @contains :  FastImage.resizeMode.contain - Scale the image uniformly (maintain the image's aspect ratio)
 *              so that both dimensions (width and height) of the image will be equal to or less than the corresponding
 *              dimension of the view (minus padding).
 *
 * @cover :     FastImage.resizeMode.cover (Default) - Scale the image uniformly (maintain the image's aspect ratio) so that
 *              both dimensions (width and height) of the image will be equal to or larger than the corresponding dimension
 *              of the view (minus padding).
 * @stretch :   FastImage.resizeMode.stretch - Scale width and height independently, This may change the aspect ratio of the src.
 * @center       :   FastImage.resizeMode.center - Do not scale the image, keep centered.
 */

const manageResizeMode = resizeMode => {
  let resizeModeValue = FastImage.resizeMode.contain;
  if (resizeMode === 'cover') {
    resizeModeValue = FastImage.resizeMode.cover;
  } else if (resizeMode === 'stretch') {
    resizeModeValue = FastImage.resizeMode.stretch;
  } else if (resizeMode === 'center') {
    resizeModeValue = FastImage.resizeMode.center;
  }
  return resizeModeValue;
};

/**
 * @renderFastImage : In this return the FastImage component for using cache purpose .
 * @imageWidth : used for image width .
 * @imageHeight : used for image height .
 * @uri : used as image source .
 * @onLoadStart :
 */

const ImageCaching = props => {
  const {
    imageWidth,
    imageHeight,
    cacheType,
    priority,
    onLoadStart,
    onProgress,
    onError,
    resizeMode,
    uriInfo: { uri },
    isBackground,
    customStyle,
    accessibilityLabel,
    overrideStyle,
  } = props;

  const imageStyle = customStyle
    ? { ...customStyle, ...overrideStyle }
    : {
        width: imageWidth,
        height: imageHeight,
        backgroundColor: '#ffffff',
        ...overrideStyle,
      };

  return (
    validateExternalUrl(uri) && (
      <ImageBackground style={imageStyle} source={isBackground === true ? placeHolderImg : null}>
        <FastImage
          style={imageStyle}
          source={{
            uri,
            priority: managePrority(priority),
            cache: manageCaching(cacheType),
            headers: {
              accept: '*/*',
            },
          }}
          resizeMode={manageResizeMode(resizeMode)}
          onLoadStart={onLoadStart}
          onProgress={onProgress}
          onError={onError}
          accessible
          accessibilityRole={AccessibilityRoles.Image}
          accessibilityLabel={accessibilityLabel}
        />
      </ImageBackground>
    )
  );
};

ImageCaching.propTypes = {
  imageWidth: PropTypes.number.isRequired,
  imageHeight: PropTypes.number.isRequired,
  cacheType: PropTypes.string,
  uriInfo: PropTypes.shape({}).isRequired,
  priority: PropTypes.string,
  onLoadStart: PropTypes.func,
  onProgress: PropTypes.func,
  onError: PropTypes.func,
  resizeMode: PropTypes.string,
  isBackground: PropTypes.bool,
  customStyle: PropTypes.shape({}),
};

ImageCaching.defaultProps = {
  cacheType: '',
  priority: '',
  resizeMode: '',
  onLoadStart: () => {},
  onProgress: () => {},
  onError: () => {},
  isBackground: true,
  customStyle: null,
};

export default ImageCaching;
