import { css } from 'styled-components';

const styles = css`
  display: ${(props) => (props.loading ? 'block' : 'none')};
  justify-content: center;
  align-items: center;
  position: absolute;
  transform: translate(-50%, -50%);
  top: 40%;
  left: 40%;
  z-index: 1400;

  &.spinner-container {
    ${(props) => (props.loaderLeft ? `left: 25%` : '')}
  }
  .spc-spinner {
    display: inline-block;
    position: relative;
    width: 5px;
    height: 5px;
  }
  .spc-spinner div {
    position: absolute;
    width: 8px;
    height: 8px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 14px;
      height: 14px;
    }
    border-radius: 100%;
    background: ${(props) => props.theme.colors.WHITE};
  }
  .spc-spinner div:nth-child(1) {
    left: 0px;
    animation: tcp-spc-spinner 1.2s infinite;
  }
  .spc-spinner div:nth-child(2) {
    animation: tcp-spc-spinner 1.2s infinite;
    animation-delay: 0.2s;
  }
  @keyframes tcp-spc-spinner {
    0% {
      transform: scale(0);
      opacity: 0;
    }
    10% {
      transform: scale(0);
      opacity: 0;
    }
    15% {
      transform: scale(0.3);
      opacity: 0.3;
    }
    20% {
      transform: scale(0.6);
      opacity: 0.6;
    }
    25% {
      transform: scale(0.9);
      opacity: 0.9;
    }
    30% {
      transform: scale(1);
      opacity: 1;
    }
    70% {
      transform: translateX(40px);
      opacity: 1;
    }
    75% {
      transform: translateX(40px) scale(0.9);
      opacity: 0.9;
    }
    80% {
      transform: translateX(40px) scale(0.6);
      opacity: 0.6;
    }
    85% {
      transform: translateX(40px) scale(0.3);
      opacity: 0.3;
    }
    90% {
      transform: translateX(40px) scale(0);
      opacity: 0;
    }
    100% {
      transform: translateX(40px);
      opacity: 0;
    }
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default styles;
