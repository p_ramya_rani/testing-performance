import React from 'react';
import { shallow } from 'enzyme';
import { ButtonSpinner } from '../views/ButtonSpinner';

describe('ButtonSpinner', () => {
  let component;

  beforeEach(() => {
    component = shallow(<ButtonSpinner />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
