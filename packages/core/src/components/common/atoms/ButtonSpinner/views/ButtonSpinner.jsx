import React, { memo } from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import styles from '../styles/ButtonSpinner.style';

export const ButtonSpinner = memo(({ className }) => {
  return (
    <div className={`${className} spinner-container`}>
      <div className="spc-spinner">
        <div />
        <div />
      </div>
    </div>
  );
});

ButtonSpinner.propTypes = {
  className: PropTypes.string,
};
ButtonSpinner.defaultProps = {
  className: '',
};

export default withStyles(ButtonSpinner, styles);
