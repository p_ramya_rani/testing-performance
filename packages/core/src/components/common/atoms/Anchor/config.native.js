// 9fbef606107a605d69c0edbcd8029e5d 
/**
 * @description - Global config values
 */
const config = {
  URL_PATTERN: {
    CATEGORY_LANDING: '/c',
    PRODUCT_LIST: '/p',
    OUTFIT_DETAILS: '/outfit/',
    SEARCH_DETAIL: '/search',
    BUNDLE_DETAIL: '/b',
    STORE_LOCATOR: '/store-locator',
    CUSTOMER_SELF_HELP: '/customer-help/',
  },
};

export default config;
