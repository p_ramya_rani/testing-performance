// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '../../../hoc/withStyles';
import styles from '../Badge.style';

const Badge = ({ children, className, showCheckmark, dataLocator }) => (
  <BodyCopy
    className={className}
    fontSize="fs10"
    component="div"
    color="text.primary"
    fontFamily="secondary"
    fontWeight="extrabold"
  >
    {showCheckmark && <span className="badge__checkmark" />}
    <span className="badge__content" data-locator={dataLocator}>
      {children}
    </span>
  </BodyCopy>
);

Badge.propTypes = {
  children: PropTypes.string,
  className: PropTypes.string,
  showCheckmark: PropTypes.bool,
  dataLocator: PropTypes.string,
};

Badge.defaultProps = {
  children: '',
  className: '',
  showCheckmark: false,
  dataLocator: '',
};
export default withStyles(Badge, styles);
export { Badge as BadgeVanilla };
