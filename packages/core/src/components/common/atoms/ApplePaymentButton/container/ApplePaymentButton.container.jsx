// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import bagPageActions from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import PropTypes from 'prop-types';
import {
  getLedgerSummaryData,
  getOrderLedgerLabels,
  getApplePayDisc,
} from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import { isMobileApp } from '@tcp/core/src/utils';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import BagPageSelectors from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {
  getOrderEddDates,
  getEddABState,
  getEddFeatureSwitch,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.selectors';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import ApplePaymentButton from '../views';
import selectors, {
  isGuest as isGuestUser,
  getIsApplePayEnabled,
} from '../../../../features/CnC/Checkout/container/Checkout.selector';
import { getCartOrderId } from '../../../../features/CnC/CartItemTile/container/CartItemTile.selectors';
import CHECKOUT_ACTIONS, {
  getAppleClientToken,
  setApplePaymentInProgress,
  submitBillingSection,
  setApplePayAppData,
  fetchShipmentMethods,
  getSetCheckoutStage,
  getAppleClientTokenSuccess,
  checkoutSetCartData,
} from '../../../../features/CnC/Checkout/container/Checkout.action';
import { APPLE_USER_STATES } from './ApplePaymentButton.util';

export const ApplePaymentButtonContainer = (props) => {
  const { authorizationKey, isGuest, orderId, getApplePaymentTokenAction } = props;
  const setApplePaymentProgress = (payloadParam) => {
    const { setAppleProgress } = props;
    setAppleProgress(payloadParam);
  };

  const submitBillingApple = (payloadParam) => {
    const { submitBilling } = props;
    submitBilling(payloadParam);
  };

  const appleClientToken = () => {
    const userState = isGuest ? APPLE_USER_STATES.GUEST : APPLE_USER_STATES.REGISTERED;
    getApplePaymentTokenAction({ userState, orderId });
  };

  useEffect(() => {
    if (!isMobileApp() && !authorizationKey) {
      appleClientToken();
    }
  }, []);

  const canRenderApplePayCta = () => (isMobileApp() ? true : authorizationKey);

  return canRenderApplePayCta() ? (
    <ApplePaymentButton
      setApplePaymentInProgressView={setApplePaymentProgress}
      submitAppleBillingSection={submitBillingApple}
      {...props}
    />
  ) : null;
};

ApplePaymentButtonContainer.propTypes = {
  setAppleProgress: PropTypes.func,
  submitBilling: PropTypes.func,
  labels: PropTypes.string.isRequired,
  getApplePaymentTokenAction: PropTypes.func.isRequired,
  isGuest: PropTypes.bool.isRequired,
  authorizationKey: PropTypes.string.isRequired,
  orderId: PropTypes.string.isRequired,
};

ApplePaymentButtonContainer.defaultProps = {
  setAppleProgress: () => {},
  submitBilling: () => {},
};

const { getAppleClientTokenData, getShipmentMethods, getApplePayEligibleShippingState } = selectors;
const {
  getShippingModeID,
  getErrorMapping,
  getShippingContactForApplePay,
  getOrderType,
  getCartStoresToJs,
} = BagPageSelectors;
const mapStateToProps = (state) => {
  const appleClientTokenData = getAppleClientTokenData(state);
  const shipModeId = getShippingModeID(state);
  const { clientToken: authorizationKey } = appleClientTokenData || {};
  const pageLedgerSummaryData = getLedgerSummaryData(state);
  const applePayDisc = getApplePayDisc(state) || 0;
  const { couponsTotal = 0, orderBalanceTotal = 0 } = pageLedgerSummaryData;
  const shipmentMethods = getShipmentMethods(state);
  const allowedApplePayStates = getApplePayEligibleShippingState(state);
  const errorLabels = getErrorMapping(state);
  const shippingContact = getShippingContactForApplePay(state);
  const orderId = getCartOrderId(state) || '';
  const applePayAppData = selectors.getApplePayAppDataApp(state);
  const { loading, isEligibleForApplePay } = applePayAppData;

  const stores = getCartStoresToJs(state);
  return {
    labels: getOrderLedgerLabels(state),
    isApplePayEnabled: getIsApplePayEnabled(state),
    isGuest: isGuestUser(state),
    orderId: orderId.toString(),
    authorizationKey,
    pageLedgerSummaryData: {
      ...pageLedgerSummaryData,
      couponsTotal: couponsTotal + applePayDisc,
      orderBalanceTotal: orderBalanceTotal - applePayDisc,
    },
    shipmentMethods,
    errorLabels,
    shippingContact,
    shipModeId,
    loading,
    isEligibleForApplePay,
    allowedApplePayStates,
    isEDD: getEddFeatureSwitch(state),
    orderEddDates: getOrderEddDates(state),
    eddLabels: BagPageSelectors.getBagPageLabels(state),
    stores,
    orderType: getOrderType(state),
    isEddABTest: getEddABState(state),
  };
};

export const mapDispatchToProps = (dispatch) => ({
  getApplePaymentTokenAction: (data) => dispatch(getAppleClientToken(data)),
  setApplePayAppDataAction: (data) => dispatch(setApplePayAppData(data)),
  setAppleProgress: (data) => dispatch(setApplePaymentInProgress(data)),
  submitBilling: (payload) => dispatch(submitBillingSection(payload)),
  setCartData: (payload) => dispatch(bagPageActions.getOrderDetailsComplete(payload)),
  setCheckoutCartData: (payload) => dispatch(checkoutSetCartData(payload)),
  loadShipmentMethods: (formName) => dispatch(fetchShipmentMethods(formName)),
  getSetCheckoutStage: (data) => dispatch(getSetCheckoutStage(data)),
  routeForBagCheckout: (payload) => dispatch(bagPageActions.routeForCheckout(payload)),
  getAppleClientTokenSuccess: (payload) => dispatch(getAppleClientTokenSuccess(payload)),
  openOOSModal: (payload) => dispatch(bagPageActions.openApayUnqualifiedItemsModal(payload)),
  showBagLoader: (data) => dispatch(setLoaderState(data)),
  displayServerErrorCheckout: (payload) =>
    dispatch(CHECKOUT_ACTIONS.setServerErrorCheckout(payload)),
  toastMessage: (payload) => {
    dispatch(toastMessageInfo(payload));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplePaymentButtonContainer);
