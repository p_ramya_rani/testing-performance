// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ApplePaymentButtonContainer, mapDispatchToProps } from '../ApplePaymentButton.container';
import ApplePaymentButton from '../../views';

describe('Apple Payment Button Container', () => {
  let props;
  beforeEach(() => {
    props = {
      isApplePayEnabled: true,
      isLoading: false,
      isEligibleForApplePay: true,
      authorizationKey: 'encrytptedauthorizationkey',
      applePayData: {
        deviceData: '762a73c4175ca24f7b1436a440da5bd0',
        email: 's@s.com',
        storeId: '10152',
        supportedByBrowser: true,
        loading: false,
        contact: {
          a1: '2203  Woodland Avenue',
          a2: '',
          city: 'Amite',
          state: 'LA',
          postal: '70422',
          ctry: 'United States',
        },
      },
      appleClientTokenData: {
        userState: 'R',
        clientToken: 'encrytptedauthorizationkey',
      },
      isGuest: false,
      orderId: 3000332630,
      setAppleProgress: jest.fn(),
      getApplePaymentTokenAction: jest.fn(),
      setApplePayDataAction: jest.fn(),
    };
  });

  it('should render correctly', () => {
    const tree = shallow(<ApplePaymentButtonContainer {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should render ApplePaymentButton correctly', () => {
    const tree = shallow(<ApplePaymentButtonContainer {...props} />);
    expect(tree).toMatchSnapshot();
    expect(tree.find(ApplePaymentButton)).toHaveLength(1);
  });
});

describe('#mapDispatchToProps', () => {
  it('should return an action setApplePaymentInProgress which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.setAppleProgress();
    expect(dispatch.mock.calls).toHaveLength(1);
  });
});

describe('Apple Button container', () => {
  const props = {
    isApplePayEnabled: true,
    authorizationKey: '83748*&^',
  };
  it('should render ApplePay Button', () => {
    const component = shallow(
      <ApplePaymentButtonContainer getApplePaymentTokenAction={() => {}} {...props} />
    );
    expect(component.is(ApplePaymentButton)).toBeTruthy();
  });

  it('should return an action getApplePaymentTokenAction which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.getApplePaymentTokenAction();
    expect(dispatch.mock.calls).toHaveLength(1);
  });
});
