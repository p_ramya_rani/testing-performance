/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { getUnqualifiedItems } from '@tcp/core/src/services/abstractors/CnC/CartItemTile';
import { getLabelValue, getEddSessionState, isMobileApp } from '@tcp/core/src/utils';
import { getDateRange } from '@tcp/core/src/utils/utils';
import { ORDER_TYPE } from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import { verifyAddressData } from '@tcp/core/src/services/abstractors/account';
import { sites } from '@tcp/web/src/constants/constants';
import {
  updateShippingMethod,
  addPickupPerson,
} from '@tcp/core/src/services/abstractors/CnC/Checkout';
import logger from '../../../../../utils/loggerInstance';

export const APPLE_USER_STATES = {
  GUEST: 'G',
  REGISTERED: 'R',
};

export const ErrorCodes = {
  INCORRECT_SHIPMODE_ID: 'INCORRECT_SHIPMODE_ID',
  ERR_US_ZIPCODE_INVALID: '_ERR_US_ZIPCODE_INVALID',
  ERR_INVALID_CITY: 'ERR_INVALID_CITY',
};

export const shouldDisplayEDD = ({ isEddABTest, isEDD }) => {
  let showEdd = false;
  if (isMobileApp()) {
    showEdd = isEDD && isEddABTest;
  } else {
    showEdd = getEddSessionState(isEddABTest) && isEDD;
  }
  return showEdd;
};

export const formatShippingMethods = (shipmentMethods, defaultShipModeId, otherProps) => {
  const {
    orderEddDates = {},
    isEDD = false,
    eddLabels = {},
    isEddABTest = false,
  } = otherProps || {};
  const shouldViewEDD = shouldDisplayEDD({ isEddABTest, isEDD });
  const items = shipmentMethods.map((item) => {
    const rangeDate = orderEddDates ? getDateRange(orderEddDates, item.code) : null;
    const detail =
      shouldViewEDD && rangeDate ? `${eddLabels.arrivesBy} ${rangeDate}` : item.shippingSpeed;

    return {
      label: item.displayName,
      detail,
      amount: (item.price || 0).toFixed(2).toString(),
      identifier: item.id,
    };
  });
  // move default shipping Method to top
  const sm = items.filter((item) => item.identifier !== defaultShipModeId);
  const defaultShipMethod = items.filter((item) => item.identifier === defaultShipModeId);
  if (defaultShipMethod && defaultShipMethod.length > 0) {
    sm.unshift(defaultShipMethod[0]);
  }
  return sm;
};

export const getStoreAddress = (addressObject) => {
  const {
    addressLine1 = '',
    city = '',
    state = '',
    zipCode = '',
    country = '',
  } = (addressObject && addressObject.address) || {};
  return {
    addressLine1,
    city,
    state,
    zipCode,
    country,
  };
};

export const formatShippingMethodsForBopisBoss = (stores, labels) => {
  const isValidStoreType = Array.isArray(stores);
  const firstStore = isValidStoreType && stores[0];
  const secondStore = isValidStoreType && stores[1];
  const storeCount = (isValidStoreType && stores.length) || 0;

  const { addressLine1, city, state, zipCode, country } = getStoreAddress(firstStore);
  const { stLocId = '', storeName = '' } = firstStore || {};
  const sameStore = stLocId === (secondStore && secondStore.stLocId); // max stores can only be 2 for any order
  const singleOrSameStore = storeCount === 1 || sameStore;
  return [
    {
      label: singleOrSameStore
        ? `${getLabelValue(labels, 'pickUpAt')} ${storeName}`
        : getLabelValue(labels, 'multipleLocations'),
      detail: singleOrSameStore ? `${addressLine1} ${city} ${state} ${zipCode} ${country}` : '',
      amount: '0',
      identifier: 'bopisOrBoss',
    },
  ];
};

export const getLineItems = (pageLedgerSummaryData, labels) => {
  const lineItems = [];
  let lineItem = {};
  const {
    subTotal,
    taxesTotal,
    shippingTotal,
    couponsTotal,
    giftCardsTotal,
    giftServiceTotal,
    savingsTotal,
  } = pageLedgerSummaryData;
  const totalDiscounts = couponsTotal + savingsTotal;
  lineItem = {
    type: 'final',
    label: labels.subtotal,
    amount: (subTotal || 0).toFixed(2).toString(),
  };
  lineItems.push(lineItem);
  if (giftServiceTotal && giftServiceTotal > 0) {
    lineItem = {
      type: 'final',
      label: labels.giftServices,
      amount: `${giftServiceTotal.toFixed(2).toString()}`,
    };
    lineItems.push(lineItem);
  }
  lineItem = {
    type: 'final',
    label: labels.estimatedTax,
    amount: (taxesTotal || 0).toFixed(2).toString(),
  };
  lineItems.push(lineItem);
  lineItem = {
    type: 'final',
    label: labels.shippingLabel,
    amount: (shippingTotal || 0).toFixed(2).toString(),
  };
  lineItems.push(lineItem);
  if (totalDiscounts && totalDiscounts > 0) {
    lineItem = {
      type: 'final',
      label: labels.discounts,
      amount: `-${totalDiscounts.toFixed(2).toString()}`,
    };
    lineItems.push(lineItem);
  }
  if (giftCardsTotal && giftCardsTotal > 0) {
    lineItem = {
      type: 'final',
      label: labels.giftCards,
      amount: `-${giftCardsTotal.toFixed(2).toString()}`,
    };
    lineItems.push(lineItem);
  }
  return lineItems;
};

export const getTotal = (orderBalanceTotal, labels, type) => {
  return {
    label: getLabelValue(labels, 'totalAmount'),
    type,
    amount: (orderBalanceTotal || 0).toFixed(2).toString(),
  };
};
export const getLedgerSummaryData = (cart) => {
  const {
    orderDetails: {
      subTotal,
      totalTax,
      applePayShippingTotal,
      couponsTotal,
      grandTotal,
      giftCardsTotal,
      giftWrappingTotal,
      savingsTotal,
      applePayDisc = 0,
    },
  } = cart;
  return {
    subTotal: subTotal || 0,
    taxesTotal: totalTax || 0,
    shippingTotal: applePayShippingTotal || 0,
    couponsTotal: (couponsTotal || 0) + (applePayDisc || 0),
    savingsTotal: savingsTotal || 0,
    giftServiceTotal: giftWrappingTotal || 0,
    giftCardsTotal: giftCardsTotal || 0,
    orderBalanceTotal: (grandTotal || 0) - (giftCardsTotal || 0) - (applePayDisc || 0),
  };
};

export const getUnqualifiedItemsCall = () => {
  return getUnqualifiedItems().then((res) => {
    const response = res || [];
    response.status = response.length > 0;
    return response;
  });
};

const getAddressLines = (addressLines) => {
  const addressLine1 = (addressLines && addressLines.length > 0 && addressLines[0]) || '';
  const addressLine2 = (addressLines && addressLines.length > 1 && addressLines[1]) || '';
  return [addressLine1, addressLine2];
};

export const getUpdateShippingPayload = (shippingContact, shipModeId, toggleShipMode) => {
  const {
    addressLines,
    postalCode,
    administrativeArea,
    locality,
    countryCode,
    familyName,
    givenName,
    emailAddress,
    phoneNumber,
    addressId = 0,
  } = shippingContact;
  return {
    shipModeId,
    toggleShipMode,
    contact: [
      {
        addressLine: [...getAddressLines(addressLines), ''],
        city: locality,
        country: countryCode && countryCode.toUpperCase(),
        email1: emailAddress || '',
        firstName: givenName,
        lastName: familyName,
        nickName: `${'sb_'}${new Date().getTime().toString()}`,
        phone1: phoneNumber || '',
        state: administrativeArea && administrativeArea.toUpperCase(),
        zipCode: postalCode,
      },
    ],
    fromPage: 'applePaySheet',
    addressId,
  };
};

export const getAddressPayload = (shippingContact) => {
  const { familyName, givenName, emailAddress, phoneNumber, addressId = 0 } = shippingContact;
  return {
    emailAddress: emailAddress || '',
    email2: '',
    firstName: givenName,
    lastName: familyName,
    phoneNumber: phoneNumber || '',
    addressId,
  };
};

export const getPaySheetError = (resultCodes, errorCode, field, message, errors = []) => {
  if (resultCodes.includes(errorCode)) {
    if (isMobileApp()) {
      errors.push({
        error: 'shippingContactInvalid',
        field,
        label: message,
        errorType: 'shipping',
      });
    } else {
      errors.push(new window.ApplePayError('shippingContactInvalid', field, message));
    }
  }
  return errors;
};

export const verifyMellisaAddress = (isMellisaValidationCompleted, shippingContact, labels) => {
  if (isMellisaValidationCompleted) {
    return new Promise((resolve) => {
      resolve({
        status: true,
        isMellisaValidationCompleted: true,
        errors: [],
      });
    });
  }

  const errors = [];
  const { addressLines, locality, postalCode, countryCode, administrativeArea } = shippingContact;
  const contact = {
    address1: addressLines[0],
    address2: addressLines[1],
    city: locality,
    zip: postalCode,
    country: countryCode.toUpperCase(),
    state: administrativeArea.toUpperCase(),
  };

  return verifyAddressData(contact).then((res) => {
    const { resultCodes } = res;
    const isMobile = isMobileApp();
    const fieldMapping = {
      streetField: isMobile ? 'street' : 'addressLines',
    };

    getPaySheetError(
      resultCodes,
      'AE02',
      fieldMapping.streetField,
      getLabelValue(labels, 'InvalidStreetName'),
      errors
    );
    getPaySheetError(
      resultCodes,
      'AE09',
      fieldMapping.streetField,
      getLabelValue(labels, 'InvalidHouseNumber'),
      errors
    );
    return {
      status: errors.length === 0,
      isMellisaValidationCompleted: true,
      errors,
    };
  });
};

const validateAppRequiredFields = (labels, errors, shippingContact) => {
  const { locality, administrativeArea, postalCode } = shippingContact;
  if (locality === '') {
    errors.push({
      error: 'shippingContactInvalid',
      field: 'city',
      label: getLabelValue(labels, 'requiredCity'),
    });
  } else if (administrativeArea === '') {
    errors.push({
      error: 'shippingContactInvalid',
      field: 'state',
      label: getLabelValue(labels, 'requiredState'),
    });
  } else if (postalCode === '') {
    errors.push({
      error: 'shippingContactInvalid',
      field: 'postalCode',
      label: getLabelValue(labels, 'requiredPostalCode'),
    });
  }
};

export const isValidShippingAddress = (labels, allowedStates, shippingContact) => {
  const { countryCode, administrativeArea } = shippingContact;

  const errors = [];
  if (
    allowedStates &&
    allowedStates.length > 0 &&
    allowedStates.toUpperCase().indexOf(administrativeArea.toUpperCase()) === -1
  ) {
    if (isMobileApp()) {
      errors.push({
        error: 'shippingContactInvalid',
        field: 'administrativeArea',
        label: getLabelValue(labels, 'nonEligibleStateErrorMessage'),
      });
    } else {
      errors.push(
        new window.ApplePayError(
          'shippingContactInvalid',
          'administrativeArea',
          getLabelValue(labels, 'nonEligibleStateErrorMessage')
        )
      );
    }
  } else if (countryCode.toUpperCase() !== sites.us.countryCode) {
    if (isMobileApp()) {
      errors.push({
        error: 'shippingContactInvalid',
        field: 'country',
        label: getLabelValue(labels, 'nonEligibleCountryErrorMessage'),
      });
    } else {
      errors.push(
        new window.ApplePayError(
          'shippingContactInvalid',
          'country',
          getLabelValue(labels, 'nonEligibleCountryErrorMessage')
        )
      );
    }
  } else if (isMobileApp()) {
    validateAppRequiredFields(labels, errors, shippingContact);
  }
  return {
    status: errors.length === 0,
    errors,
  };
};

export const errorType = (errors, typeCode, field, message, labels) => {
  if (isMobileApp()) {
    errors.push({
      error: typeCode,
      field,
      label: getLabelValue(labels, message),
      errorType: 'contact',
    });
  } else {
    errors.push(new window.ApplePayError(typeCode, field, getLabelValue(labels, message)));
  }
};

export const compareValues = (fieldValue) => {
  return fieldValue === '' || fieldValue === 'null' || !fieldValue;
};

export const isCompleteContactInPaysheet = (labels, shippingContact) => {
  const { familyName, givenName, phoneNumber, emailAddress } = shippingContact;
  const errors = [];
  if (compareValues(familyName) || compareValues(givenName)) {
    errorType(errors, 'shippingContactInvalid', 'name', 'noLastNameErrorMessage', labels);
  } else if (compareValues(phoneNumber)) {
    errorType(errors, 'shippingContactInvalid', 'phoneNumber', 'noPhoneNumberErrorMessage', labels);
  } else if (compareValues(emailAddress)) {
    errorType(
      errors,
      'shippingContactInvalid',
      'emailAddress',
      'noEmailAddressErrorMessage',
      labels
    );
  }
  return {
    statusErr: errors.length === 0,
    errors,
  };
};

export const updateShippingMethodSelection = (shippingContact, shipModeId) => {
  const payload = getUpdateShippingPayload(shippingContact, shipModeId);
  return updateShippingMethod(payload)
    .then((res) => {
      return { ...res, apiName: 'updateShipping' };
    })
    .catch((err) => {
      logger.error('Error: updateShipping', err);
      return { success: false, apiName: 'updateShipping' };
    });
};

export const updateAddress = (shippingContact) => {
  const payload = getAddressPayload(shippingContact);
  return addPickupPerson(payload)
    .then((res) => {
      const { addressId } = res;
      const response =
        addressId && addressId.length > 0
          ? { success: true, addressId: addressId[0].addressId }
          : { success: false, apiName: 'addAddress' };
      return { ...response, apiName: 'addAddress' };
    })
    .catch((err) => {
      logger.error('Error: addAddress', err);
      return { success: false, apiName: 'addAddress' };
    });
};

export const addAddressAndShippingMethod = (shippingContact, shipModeId, orderType) => {
  if (orderType === ORDER_TYPE.PURE_ECOM) {
    return updateShippingMethodSelection(shippingContact, shipModeId);
  }
  if (orderType === ORDER_TYPE.ECOM_MIX) {
    return updateAddress(shippingContact).then((res) => {
      if (res.success) {
        return updateShippingMethodSelection(shippingContact, shipModeId);
      }
      return res;
    });
  }
  return updateAddress(shippingContact);
};
