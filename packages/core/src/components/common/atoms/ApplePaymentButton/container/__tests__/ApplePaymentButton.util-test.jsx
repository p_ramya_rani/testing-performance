/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import {
  getLedgerSummaryData,
  getTotal,
  getLineItems,
  formatShippingMethods,
  getUpdateShippingPayload,
  isValidShippingAddress,
  isCompleteContactInPaysheet,
  getStoreAddress,
  formatShippingMethodsForBopisBoss,
  getAddressPayload,
  getPaySheetError,
  verifyMellisaAddress,
} from '../ApplePaymentButton.util';

import * as account from '../../../../../../services/abstractors/account';

jest.mock('../../../../../../services/abstractors/account');
account.verifyAddressData = jest.fn();

describe('ApplePaymentButtonUtil', () => {
  const errorLabels = {
    nonEligibleStateErrorMessage: 'nonEligibleStateErrorMessage',
    nonEligibleCountryErrorMessage: 'nonEligibleCountryErrorMessage',
  };
  const error = {
    code: 'shippingContactInvalid',
    contactField: 'postalCode',
    message: 'Invalid postal code',
  };
  window.ApplePayError = jest.fn(() => {
    return {
      error,
    };
  });
  const states = 'AL|AK|AS|CA|CO|CT|DE|DC|FM|FL|GA';
  it('test getLedgerSummaryData', () => {
    const cart = {
      orderDetails: {
        subTotal: 100,
        totalTax: 100,
        applePayShippingTotal: 100,
        couponsTotal: 100,
        grandTotal: 100,
        giftCardsTotal: 0,
        savingsTotal: 0,
        giftWrappingTotal: 0,
      },
    };
    const result = {
      subTotal: 100,
      taxesTotal: 100,
      shippingTotal: 100,
      couponsTotal: 100,
      orderBalanceTotal: 100,
      savingsTotal: 0,
      giftServiceTotal: 0,
      giftCardsTotal: 0,
    };

    expect(getLedgerSummaryData(cart)).toEqual(result);
  });
  it('test getLedgerSummaryData when orderDetails is empty', () => {
    const cart = {
      orderDetails: {},
    };
    const result = {
      subTotal: 0,
      taxesTotal: 0,
      shippingTotal: 0,
      couponsTotal: 0,
      orderBalanceTotal: 0,
      savingsTotal: 0,
      giftServiceTotal: 0,
      giftCardsTotal: 0,
    };

    expect(getLedgerSummaryData(cart)).toEqual(result);
  });
  it('test getTotal', () => {
    const labels = {
      totalAmount: 'Total Amount',
    };
    const total = {
      label: labels.totalAmount,
      type: 'final',
      amount: '100.00',
    };

    expect(getTotal(100, labels, 'final')).toEqual(total);
  });

  it('test getLineItems', () => {
    const pageLedgerSummaryData = {
      subTotal: 100,
      taxesTotal: 100,
      shippingTotal: 100,
      couponsTotal: 100,
      savingsTotal: 100,
    };
    const labels = {
      subtotal: 'Subtotal',
      estimatedTax: 'Estimated Tax',
      shippingLabel: 'Shipping',
      discounts: 'Discounts',
    };
    const result = [
      {
        type: 'final',
        label: labels.subtotal,
        amount: '100.00',
      },
      {
        type: 'final',
        label: labels.estimatedTax,
        amount: '100.00',
      },
      {
        type: 'final',
        label: labels.shippingLabel,
        amount: '100.00',
      },
      {
        type: 'final',
        label: labels.discounts,
        amount: '-200.00',
      },
    ];

    expect(getLineItems(pageLedgerSummaryData, labels)).toEqual(result);
  });

  const displayName = 'Super Express ';
  const shippingSpeed = '2 Days Delivery';
  it('test formatShippingMethods with EDD', () => {
    const shipmentMethods = [
      {
        displayName: 'Express ',
        shippingSpeed: 'Express',
        price: 30,
        id: '10092',
      },
      {
        displayName,
        shippingSpeed,
        price: 20,
        id: '10093',
      },
    ];
    const defaultShipModeId = '10093';

    const orderEddDates = {
      maxDates: {
        PMDC: undefined,
        U1AK: undefined,
        U1DS: '2020-10-23',
        U2AK: undefined,
        U2DY: '2020-10-24',
        UGNR: '2020-10-26',
      },
      minDates: {
        PMDC: undefined,
        U1AK: undefined,
        U1DS: '2020-10-23',
        U2AK: undefined,
        U2DY: '2020-10-24',
        UGNR: '2020-10-26',
      },
    };

    const eddLabels = {
      arrivesBy: 'Arrives by',
    };

    const result = [
      {
        label: displayName,
        detail: 'Arrives by Mon, Oct 26',
        amount: '20.00',
        identifier: '10093',
      },
      {
        label: 'Express ',
        detail: 'Arrives by Mon, Oct 26',
        amount: '30.00',
        identifier: '10092',
      },
    ];

    expect(
      formatShippingMethods(shipmentMethods, defaultShipModeId, {
        orderEddDates,
        isEDD: true,
        eddLabels,
        isEddABTest: true,
      })
    ).toEqual(result);
  });

  it('test formatShippingMethods without EDD', () => {
    const shipmentMethods = [
      {
        displayName: 'Express ',
        shippingSpeed: 'Express',
        price: 30,
        id: '10092',
      },
      {
        displayName,
        shippingSpeed,
        price: 20,
        id: '10093',
      },
    ];
    const defaultShipModeId = '10093';

    const orderEddDates = {};

    const eddLabels = {
      arrivesBy: 'Arrives by',
    };

    const result = [
      {
        label: displayName,
        detail: shippingSpeed,
        amount: '20.00',
        identifier: '10093',
      },
      {
        label: 'Express ',
        detail: 'Express',
        amount: '30.00',
        identifier: '10092',
      },
    ];

    expect(
      formatShippingMethods(shipmentMethods, defaultShipModeId, {
        orderEddDates,
        isEDD: false,
        eddLabels,
        isEddABTest: false,
      })
    ).toEqual(result);
  });

  it('test getUpdateShippingPayload', () => {
    const defaultShippingContact = {
      administrativeArea: 'AK',
      countryCode: 'US',
      familyName: 'test',
      givenName: 'test',
      locality: 'test',
      postalCode: '1001',
      emailAddress: '',
      phoneNumber: '',
      addressId: 1,
    };
    const shipModeId = '11111';
    const result = {
      shipModeId: '11111',
      toggleShipMode: 'true',
      contact: [
        {
          addressLine: ['', '', ''],
          city: 'test',
          country: 'US',
          email1: '',
          firstName: 'test',
          lastName: 'test',
          nickName: '',
          phone1: '',
          state: 'AK',
          zipCode: '1001',
        },
      ],
      fromPage: 'applePaySheet',
      addressId: 1,
    };
    const res = getUpdateShippingPayload(defaultShippingContact, shipModeId, 'true');
    res.contact[0].nickName = '';
    expect(res).toEqual(result);
  });

  it('test isValidShippingAddress true', () => {
    const shippingContact = {
      countryCode: 'US',
      administrativeArea: 'AK',
    };
    const result = {
      status: true,
      errors: [],
    };
    expect(isValidShippingAddress(errorLabels, states, shippingContact)).toEqual(result);
  });

  it('test isValidShippingAddress false', () => {
    const shippingContact = {
      countryCode: 'gb',
      administrativeArea: 'UK',
    };
    const result = {
      status: false,
      errors: [{ error: true }, { error: true }],
    };
    expect(isValidShippingAddress(errorLabels, states, shippingContact).status).toEqual(
      result.status
    );
  });

  it('test isCompleteContactInPaysheet true', () => {
    const shippingContactMethod = {
      familyName: 'tim',
      givenName: 'tom',
      phoneNumber: '7265273826',
      emailAddress: 'tim@tom.com',
    };
    const result = {
      statusErr: true,
      errors: [],
    };
    expect(isCompleteContactInPaysheet(errorLabels, shippingContactMethod)).toEqual(result);
  });
  it('test isCompleteContactInPaysheet false', () => {
    const shippingContactMethod = {
      familyName: '',
      givenName: 'test',
      phoneNumber: 'test',
      emailAddress: 'tim@tom.com',
    };
    const result = {
      statusErr: false,
      errors: [
        {
          error: {
            code: 'shippingContactInvalid',
            contactField: 'postalCode',
            message: 'Invalid postal code',
          },
        },
      ],
    };
    expect(isCompleteContactInPaysheet(errorLabels, shippingContactMethod)).toEqual(result);
  });
  it('getStoreAddress blank address', () => {
    const addressObject = {
      address: {},
    };

    const response = {
      addressLine1: '',
      city: '',
      state: '',
      zipCode: '',
      country: '',
    };
    expect(getStoreAddress(addressObject)).toEqual(response);
  });
  it('getStoreAddress with city', () => {
    const addressObject = {
      address: {
        city: 'US',
      },
    };

    const response = {
      addressLine1: '',
      city: 'US',
      state: '',
      zipCode: '',
      country: '',
    };
    expect(getStoreAddress(addressObject)).toEqual(response);
  });
  it('getStoreAddress blank address when object is undefined', () => {
    const addressObject = undefined;

    const response = {
      addressLine1: '',
      city: '',
      state: '',
      zipCode: '',
      country: '',
    };
    expect(getStoreAddress(addressObject)).toEqual(response);
  });

  it('getStoreAddress actual address', () => {
    const addressObject = {
      address: {
        addressLine1: 'addressLine1',
        city: 'city',
        state: 'state',
        zipCode: 'zipCode',
        country: 'country',
      },
    };

    const response = {
      addressLine1: 'addressLine1',
      city: 'city',
      state: 'state',
      zipCode: 'zipCode',
      country: 'country',
    };
    expect(getStoreAddress(addressObject)).toEqual(response);
  });

  it('formatShippingMethodsForBopisBoss invalid store', () => {
    const dataToFormat = {
      store: [],
      labels: {
        multipleLocations: 'multipleLocations',
        pickUpAt: 'pickUpAt',
      },
    };

    const response = [
      {
        label: 'multipleLocations',
        detail: '',
        amount: '0',
        identifier: 'bopisOrBoss',
      },
    ];

    expect(formatShippingMethodsForBopisBoss(dataToFormat.store, dataToFormat.labels)).toEqual(
      response
    );
  });

  it('formatShippingMethodsForBopisBoss invalid store', () => {
    const dataToFormat = {
      store: [{ stLocId: 1, storeName: 'Jest' }, { stLocId: 2 }],
      labels: {
        multipleLocations: 'multipleLocations',
        pickUpAt: 'pickUpAt',
      },
    };

    const response = [
      {
        label: 'multipleLocations',
        detail: '',
        amount: '0',
        identifier: 'bopisOrBoss',
      },
    ];

    expect(formatShippingMethodsForBopisBoss(dataToFormat.store, dataToFormat.labels)).toEqual(
      response
    );
  });

  it('formatShippingMethodsForBopisBoss valid store', () => {
    const dataToFormat = {
      store: [
        {
          stLocId: 1,
          storeName: 'Jest',
          address: {
            addressLine1: 'addressLine1',
            city: 'city',
            state: 'state',
            zipCode: 'zipCode',
            country: 'country',
          },
        },
        { stLocId: 1 },
      ],
      labels: {
        multipleLocations: 'multipleLocations',
        pickUpAt: 'pickUpAt',
      },
    };

    const response = [
      {
        label: 'pickUpAt Jest',
        detail: 'addressLine1 city state zipCode country',
        amount: '0',
        identifier: 'bopisOrBoss',
      },
    ];

    expect(formatShippingMethodsForBopisBoss(dataToFormat.store, dataToFormat.labels)).toEqual(
      response
    );
  });

  it('test getAddressPayload', () => {
    const defaultShippingContact = {
      familyName: 'test',
      givenName: 'test',
      emailAddress: 'test@gmail.com',
      phoneNumber: '99764445332',
      addressId: 1,
    };

    const result = {
      emailAddress: 'test@gmail.com',
      email2: '',
      firstName: 'test',
      lastName: 'test',
      phoneNumber: '99764445332',
      addressId: 1,
    };
    const res = getAddressPayload(defaultShippingContact);
    expect(res).toEqual(result);
  });

  it('test getPaySheetError', () => {
    const errors = [];

    const result = [
      {
        error,
      },
    ];

    const resonse = getPaySheetError('AE02', 'AE02', 'postalCode', 'Incorrect State', errors);
    expect(resonse).toEqual(result);
  });

  it('test verifyMellisaAddress', () => {
    account.verifyAddressData.mockImplementation(() => {
      return Promise.resolve({ resultCodes: 'AE02,AE09' });
    });

    const shippingContact = {
      addressLines: ['', ''],
      locality: 'test',
      postalCode: 'test',
      countryCode: 'test',
      administrativeArea: 'test',
    };

    const result = {
      status: false,
      errors: [error, error, error, error, error, error, error, error, error, error],
      isMellisaValidationCompleted: true,
    };

    verifyMellisaAddress(false, shippingContact, {}).then(response => {
      expect(result).toEqual(response);
    });
  });

  it('test verifyMellisaAddress with Prop isMellisaValidationCompleted: true', () => {
    const result = {
      status: true,
      errors: [],
      isMellisaValidationCompleted: true,
    };
    verifyMellisaAddress(true, {}, {}).then(response => {
      expect(result).toEqual(response);
    });
  });
});
