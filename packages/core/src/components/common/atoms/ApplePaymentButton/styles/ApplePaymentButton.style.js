// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  /* CSS */
  @supports (-webkit-appearance: -apple-pay-button) {
    .apple-pay-button {
      display: inline-block;
      -webkit-appearance: -apple-pay-button;
      width: 100%;
      height: ${props => (props.containerId ? '42px' : '48px')};
      cursor: pointer;
      @media ${props => props.theme.mediaQuery.smallMax} {
        height: 42px;
      }
    }
    .apple-pay-button-black {
      -apple-pay-button-style: black;
      -apple-pay-button-type: buy;
    }
    .apple-pay-button-white {
      -apple-pay-button-type: plain;
      -apple-pay-button-style: white-outline;
    }
    .apple-pay-button-white-with-line {
      -apple-pay-button-style: white-outline;
    }
  }

  @supports not (-webkit-appearance: -apple-pay-button) {
    .apple-pay-button {
      display: inline-block;
      background-size: 100% 60%;
      background-repeat: no-repeat;
      background-position: 50% 50%;
      border-radius: 5px;
      padding: 0px;
      box-sizing: border-box;
      min-width: 200px;
      min-height: 32px;
      max-height: 64px;
    }
    .apple-pay-button-black {
      background-image: -webkit-named-image(apple-pay-logo-white);
      background-color: black;
    }
    .apple-pay-button-white {
      background-image: -webkit-named-image(apple-pay-logo-black);
      background-color: white;
    }
    .apple-pay-button-white-with-line {
      background-image: -webkit-named-image(apple-pay-logo-black);
      background-color: white;
      border: 0.5px solid black;
    }
  }
`;

export default styles;
