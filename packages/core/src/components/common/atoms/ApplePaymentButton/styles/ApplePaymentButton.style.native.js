// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components/native';

const ApplePayButtonStyle = styled.View`
  align-items: center;
  justify-content: center;
  text-align: center;
  height: ${props => (props.isBillingPage ? '44px' : '42px')};
  border-radius: ${props =>
    props.theme.isGymboree
      ? props.theme.spacing.ELEM_SPACING.LRG
      : props.theme.spacing.ELEM_SPACING.XXS};
  ${props => (props.isBillingPage ? '' : `border: 1px solid ${props.theme.colorPalette.black}`)};
`;

const ApplePayStyles = css``;

export { ApplePayStyles, ApplePayButtonStyle };
