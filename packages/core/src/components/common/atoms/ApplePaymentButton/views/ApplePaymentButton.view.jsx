/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { Component } from 'react';
import { string, func, shape, bool } from 'prop-types';
import { client, applePay, dataCollector } from 'braintree-web';
import isEqual from 'lodash/isEqual';
import { getAPIConfig, getLabelValue } from '@tcp/core/src/utils';
import { isApplePayEligible } from '@tcp/core/src/utils/utils';
import { getCartData } from '@tcp/core/src/services/abstractors/CnC/CartItemTile';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import {
  getShippingMethods,
  updateShippingMethod,
} from '@tcp/core/src/services/abstractors/CnC/Checkout';
import CheckoutConstants, {
  CHECKOUT_ROUTES,
  ORDER_TYPE,
} from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import utility from '../../../../features/CnC/Checkout/util/utility';
import logger from '../../../../../utils/loggerInstance';
import {
  formatShippingMethods,
  formatShippingMethodsForBopisBoss,
  getLineItems,
  getTotal,
  getLedgerSummaryData,
  getUpdateShippingPayload,
  getUnqualifiedItemsCall,
  isValidShippingAddress,
  addAddressAndShippingMethod,
  verifyMellisaAddress,
  getPaySheetError,
  isCompleteContactInPaysheet,
  ErrorCodes,
} from '../container/ApplePaymentButton.util';
import withStyles from '../../../hoc/withStyles';
import styles from '../styles/ApplePaymentButton.style';
import BodyCopy from '../../BodyCopy';

class ApplePaymentButton extends Component {
  constructor(props) {
    super(props);
    this.shipModeId = '';
    this.shippingContact = {};
    this.deviceData = {};
    this.isMellisaValidationCompleted = false;
    this.isContactUpdated = false;
  }

  componentDidMount() {
    const { shipModeId, shipmentMethods } = this.props;
    this.shipModeId = shipModeId;
    if (!(shipmentMethods && shipmentMethods.length > 0 && shipModeId)) {
      this.fetchDefaultShippingMethods();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !(
      isEqual(nextState, this.state) &&
      isEqual(JSON.stringify(nextProps), JSON.stringify(this.props))
    );
  }

  // Get Shipping Method API Call
  fetchShippingMethods = async (zipCode, state) => {
    const { errorLabels } = this.props;
    const payload = {
      state,
      zipCode,
      fromPage: 'applePaySheet',
    };
    const eventType =
      zipCode || state
        ? CheckoutConstants.CHANGE_SHIPMMENT_METHODS
        : CheckoutConstants.LOAD_SHIPMMENT_METHODS;
    const shipmentMethods = await getShippingMethods(payload, errorLabels, eventType);
    return formatShippingMethods(shipmentMethods, this.shipModeId);
  };

  updateCartInRedux = orderDetails => {
    const { setCartData } = this.props;
    const { applePayShippingTotal } = orderDetails;
    setCartData({ ...orderDetails, shippingTotal: applePayShippingTotal });
  };

  fetchDefaultShippingMethods = () => {
    const { loadShipmentMethods } = this.props;
    const requestPayload = {
      payload: {
        pageEvent: CheckoutConstants.ERR_STATE_DOES_NOT_MATCH_SHIPMENT_OPTION,
      },
    };
    loadShipmentMethods(requestPayload);
  };

  createPaymentRequest = applePayInstance => {
    this.isMellisaValidationCompleted = false;
    const {
      shippingContact,
      pageLedgerSummaryData,
      shipmentMethods,
      labels,
      isEDD,
      orderEddDates,
      eddLabels,
      orderType,
      stores,
      isEddABTest,
    } = this.props;
    const { orderBalanceTotal } = pageLedgerSummaryData;
    const { postalCode, administrativeArea } = shippingContact;
    const contact = postalCode || administrativeArea ? shippingContact : {};
    this.shippingContact = shippingContact;

    let payloadSpecificToOrderType = {};

    if (orderType === ORDER_TYPE.PURE_ECOM || orderType === ORDER_TYPE.ECOM_MIX) {
      payloadSpecificToOrderType = {
        requiredShippingContactFields: ['name', 'phone', 'postalAddress', 'email'],
        shippingMethods: formatShippingMethods(shipmentMethods, this.shipModeId, {
          isEDD,
          orderEddDates,
          eddLabels,
          isEddABTest,
        }),
        shippingType: ['shipping'],
      };
    } else {
      payloadSpecificToOrderType = {
        requiredShippingContactFields: ['name', 'phone', 'email'],
        shippingMethods: formatShippingMethodsForBopisBoss(stores, labels),
        shippingType: ['storePickup'],
      };
    }

    return applePayInstance.createPaymentRequest({
      countryCode: 'US',
      currencyCode: 'USD',
      merchantCapabilities: ['supports3DS'],
      supportedNetworks: ['visa', 'masterCard', 'amex', 'discover'],
      total: getTotal(orderBalanceTotal, labels, 'final'),
      billingContact: contact,
      lineItems: getLineItems(pageLedgerSummaryData, labels),
      requiredBillingContactFields: ['phone', 'name'],
      shippingContact: contact,
      ...payloadSpecificToOrderType,
    });
  };

  createSession = paymentRequest => {
    let session;
    try {
      session = new window.ApplePaySession(3, paymentRequest);
      return session;
    } catch (err) {
      logger.error('__ Error Creating Payment Request', err);
      return null;
    }
  };

  submitPayment = (event, session, addressId, payload) => {
    const { orderId, submitAppleBillingSection, pageLedgerSummaryData, orderType } = this.props;
    const { orderBalanceTotal } = pageLedgerSummaryData;

    let contact = {};
    if (orderType === ORDER_TYPE.PURE_ECOM || orderType === ORDER_TYPE.ECOM_MIX) {
      contact = {
        address1: event.payment.shippingContact.addressLines[0],
        address2: event.payment.shippingContact.addressLines[1],
        city: event.payment.shippingContact.locality,
        zip: event.payment.shippingContact.postalCode,
        country: event.payment.shippingContact.countryCode.toUpperCase(),
        state: event.payment.shippingContact.administrativeArea.toUpperCase(),
      };
    }

    const requestData = {
      orderType,
      contact,
      paymentNonce: payload.nonce,
      storeId: getAPIConfig().storeId,
      device_data: this.deviceData,
      email: event.payment.shippingContact.emailAddress,
      amount: orderBalanceTotal,
      orderId,
      addressId,
    };

    submitAppleBillingSection(requestData);
    session.completePayment(window.ApplePaySession.STATUS_SUCCESS);
  };

  handlingMelissaAndBillingInformation = (event, session, addressId, applePayInstance) => {
    const { setApplePaymentInProgressView } = this.props;
    applePayInstance.tokenize(
      {
        token: event.payment.token,
      },
      (tokenizeErr, payload) => {
        if (tokenizeErr) {
          setApplePaymentInProgressView(false);
          session.completePayment(window.ApplePaySession.STATUS_FAILURE);
          return;
        }
        this.submitPayment(event, session, addressId, payload);
      }
    );
  };

  suppressMelissaForBopisAndBoss = orderType => {
    if (!(orderType === ORDER_TYPE.PURE_ECOM || orderType === ORDER_TYPE.ECOM_MIX)) {
      this.isMellisaValidationCompleted = true; // suppress melissa call in case of boss and bopis
    }
  };

  getShippingContactPayload = event => {
    const { shippingContact } = event.payment;
    const { shippingContact: shippingContactFromProps } = this.props;
    return {
      ...shippingContact,
      addressId: (!this.isContactUpdated && shippingContactFromProps.addressId) || 0,
    };
  };

  handleServiceErrors = (res, session) => {
    const { orderType, errorLabels, displayServerErrorCheckout } = this.props;
    if (res.errorCode === ErrorCodes.ERR_INVALID_CITY) {
      displayServerErrorCheckout({
        errorMessage: errorLabels.ERR_INVALID_CITY,
        component: 'Apple Pay',
      });
    } else if (res.errorCode === ErrorCodes.INCORRECT_SHIPMODE_ID) {
      displayServerErrorCheckout({
        errorMessage: res.errorMsg,
        component: 'Apple Pay',
      });
    }
    session.completePayment(window.ApplePaySession.STATUS_FAILURE);
    if (orderType === ORDER_TYPE.PURE_ECOM) {
      utility.routeToPage(CHECKOUT_ROUTES.shippingPage);
    } else {
      utility.routeToPage(CHECKOUT_ROUTES.pickupPage);
    }
  };

  addAddressAndPaymentAuthorization = (
    session,
    event,
    orderType,
    setCheckoutCartData,
    applePayInstance
  ) => {
    const { labels } = this.props;
    this.suppressMelissaForBopisAndBoss(orderType);
    verifyMellisaAddress(
      this.isMellisaValidationCompleted,
      event.payment.shippingContact,
      labels
    ).then(result => {
      const { status, errors, isMellisaValidationCompleted } = result;
      this.isMellisaValidationCompleted = isMellisaValidationCompleted;
      if (!status) {
        session.completePayment({
          status: window.ApplePaySession.STATUS_FAILURE,
          errors,
        });
      } else {
        const shippingContactPayload = this.getShippingContactPayload(event);
        addAddressAndShippingMethod(shippingContactPayload, this.shipModeId, orderType)
          .then(res => {
            if (
              res.success &&
              res.errorCode !== ErrorCodes.INCORRECT_SHIPMODE_ID &&
              res.errorCode !== ErrorCodes.ERR_INVALID_CITY
            ) {
              getCartData({}).then(cart => {
                this.updateCartInRedux(cart.orderDetails);
                setCheckoutCartData({ res: cart });
                this.handlingMelissaAndBillingInformation(
                  event,
                  session,
                  res.addressId,
                  applePayInstance
                );
              });
            } else {
              this.handleServiceErrors(res, session);
            }
          })
          .catch(err => {
            logger.error('Error: addAddressAndShippingMethod', err);
            utility.routeToPage(CHECKOUT_ROUTES.shippingPage);
          });
      }
    });
  };

  onShippingContactSelected = (event, session) => {
    const { labels, allowedApplePayStates } = this.props;
    const { postalCode, administrativeArea } = event.shippingContact;
    const { pageLedgerSummaryData, shipmentMethods } = this.props;
    this.isContactUpdated = true;
    const { status, errors } = isValidShippingAddress(
      labels,
      allowedApplePayStates,
      event.shippingContact,
      this.isMellisaValidationCompleted
    );
    if (!status) {
      session.completeShippingContactSelection({
        newShippingMethods: formatShippingMethods(shipmentMethods, this.shipModeId),
        newTotal: getTotal(pageLedgerSummaryData.orderBalanceTotal, labels, 'pending'),
        newLineItems: getLineItems(pageLedgerSummaryData, labels),
        errors,
      });
    } else {
      this.shippingContact = event.shippingContact;
      // ***** Get Shipping Methods Based on Selected Contact */
      this.fetchShippingMethods(postalCode, administrativeArea)
        .then(shippingMethods => {
          // ***** Update Selected Shipping Method */
          const payload = getUpdateShippingPayload(
            event.shippingContact,
            shippingMethods[0].identifier,
            'true'
          );
          updateShippingMethod(payload).then(res => {
            if (res.success) {
              this.shipModeId = shippingMethods[0].identifier;
              // ***** Update Cart ****/
              getCartData({}).then(cart => {
                this.updateCartInRedux(cart.orderDetails);
                const updatedPageLedgerSummaryData = getLedgerSummaryData(cart);
                session.completeShippingContactSelection(
                  window.ApplePaySession.STATUS_SUCCESS,
                  shippingMethods,
                  getTotal(updatedPageLedgerSummaryData.orderBalanceTotal, labels, 'final'),
                  getLineItems(updatedPageLedgerSummaryData, labels)
                );
              });
            } else if (res.errorCode === ErrorCodes.ERR_INVALID_CITY) {
              this.handleServiceErrors(res, session);
            }
          });
        })
        .catch(error => {
          if (error.errorCodes === ErrorCodes.ERR_US_ZIPCODE_INVALID) {
            session.completeShippingContactSelection({
              newShippingMethods: formatShippingMethods(shipmentMethods, this.shipModeId),
              newTotal: getTotal(pageLedgerSummaryData.orderBalanceTotal, labels, 'pending'),
              newLineItems: getLineItems(pageLedgerSummaryData, labels),
              errors: getPaySheetError(
                'AE01',
                'AE01',
                'postalCode',
                getLabelValue(labels, 'InvalidPostalCode')
              ),
            });
          }
        });
    }
  };

  onShippingMethodSelected = (event, session) => {
    const { labels } = this.props;
    // ***** Update Selected Shipping Method */

    const payload = getUpdateShippingPayload(
      this.shippingContact,
      event.shippingMethod.identifier,
      'true'
    );
    updateShippingMethod(payload).then(res => {
      if (res.success) {
        this.shipModeId = event.shippingMethod.identifier;
        // ***** Update Cart ****/
        getCartData({}).then(cart => {
          this.updateCartInRedux(cart.orderDetails);
          const pageLedgerSummaryData = getLedgerSummaryData(cart);
          session.completeShippingMethodSelection(
            window.ApplePaySession.STATUS_SUCCESS,
            getTotal(pageLedgerSummaryData.orderBalanceTotal, labels, 'final'),
            getLineItems(pageLedgerSummaryData, labels)
          );
        });
      } else if (res.errorCode === ErrorCodes.ERR_INVALID_CITY) {
        this.handleServiceErrors(res, session);
      }
    });
  };

  onPaymentAuthorized = (event, session, applePayInstance) => {
    const { labels } = this.props;
    const {
      setApplePaymentInProgressView,
      setCheckoutCartData,
      displayServerErrorCheckout,
      orderType,
    } = this.props;
    const { statusErr, errors } = isCompleteContactInPaysheet(
      labels,
      event.payment.shippingContact
    );

    if (!statusErr) {
      session.completePayment({
        status: 1,
        errors,
      });
    } else {
      setApplePaymentInProgressView(true);
      getUnqualifiedItemsCall().then(response => {
        if (response && response.status) {
          session.completePayment(window.ApplePaySession.STATUS_FAILURE);
          utility.routeToPage(CHECKOUT_ROUTES.bagPage);
        } else {
          this.addAddressAndPaymentAuthorization(
            session,
            event,
            orderType,
            setCheckoutCartData,
            applePayInstance,
            displayServerErrorCheckout
          );
        }
      });
    }
  };

  beginApplePay = applePayInstance => {
    const paymentRequest = this.createPaymentRequest(applePayInstance);
    const session = this.createSession(paymentRequest);

    session.onshippingcontactselected = event => {
      this.onShippingContactSelected(event, session);
    };

    session.onshippingmethodselected = event => {
      this.onShippingMethodSelected(event, session);
    };

    session.oncancel = () => {};

    session.onvalidatemerchant = event => {
      applePayInstance.performValidation(
        {
          validationURL: event.validationURL,
          displayName: "The Children's Place, Inc",
        },
        (err, merchantSession) => {
          if (err) {
            // You should show an error to the user, e.g. 'Apple Pay failed to load.'
            return;
          }
          session.completeMerchantValidation(merchantSession);
        }
      );
    };

    session.onpaymentauthorized = event => {
      this.onPaymentAuthorized(event, session, applePayInstance);
    };
    session.begin();
    logger.log('__ Apple Session Created');
  };

  handleAppleInstanceError = e => {
    logger.error('Apple', 'Promises Error', e);
  };

  getApplePayInstance = () => {
    const { authorizationKey } = this.props;
    client
      .create({ authorization: authorizationKey })
      .then(clientInstance => {
        return Promise.all([
          applePay.create({
            client: clientInstance,
          }),
          dataCollector.create({
            client: clientInstance,
            paypal: true,
          }),
        ]);
      })
      .then(([applePayInstanceRef, dataCollectorInstanceRef]) => {
        const applePayInstance = applePayInstanceRef;
        if (applePayInstance) {
          const { deviceData } = dataCollectorInstanceRef;
          const deviceDataValue = JSON.parse(deviceData);
          this.deviceData = deviceDataValue.correlation_id;
        }
        this.beginApplePay(applePayInstance);
      })
      .catch(err => this.handleAppleInstanceError(err));
  };

  onClickAppleButton = () => {
    this.getApplePayInstance();
  };

  renderAppleButton = () => {
    const { className, containerId, buttonClass } = this.props;
    return (
      isApplePayEligible() && (
        <div className={className}>
          <ClickTracker
            clickData={{
              customEvents: ['event148'],
              applePayButtonLocation: 'APAY Regular Bag',
            }}
          >
            <BodyCopy
              component="div"
              fontSize="fs15"
              fontWeight="semibold"
              fontFamily="secondary"
              className={`apple-pay-button ${buttonClass}`}
              onClick={this.onClickAppleButton}
              id={containerId}
            >
              <BodyCopy
                component="span"
                fontSize="fs15"
                fontWeight="semibold"
                fontFamily="secondary"
                className="logo"
              />
            </BodyCopy>
          </ClickTracker>
        </div>
      )
    );
  };

  render() {
    return <>{this.renderAppleButton()}</>;
  }
}

ApplePaymentButton.propTypes = {
  className: string,
  labels: shape({}),
  orderId: string.isRequired,
  authorizationKey: string,
  setApplePaymentInProgressView: func,
  submitAppleBillingSection: func,
  shippingContact: {},
  pageLedgerSummaryData: shape({}).isRequired,
  errorLabels: shape({}).isRequired,
  shipModeId: string.isRequired,
  setCartData: func.isRequired,
  setCheckoutCartData: func.isRequired,
  loadShipmentMethods: func.isRequired,
  containerId: string,
  shipmentMethods: shape([]).isRequired,
  buttonClass: string,
  allowedApplePayStates: string.isRequired,
  isEDD: bool,
  orderEddDates: shape({}),
  eddLabels: shape({}),
  orderType: string.isRequired,
  stores: shape({}).isRequired,
  isEddABTest: bool,
  displayServerErrorCheckout: func.isRequired,
};
ApplePaymentButton.defaultProps = {
  className: '',
  labels: {},
  authorizationKey: '',
  setApplePaymentInProgressView: () => {},
  submitAppleBillingSection: () => {},
  shippingContact: {},
  containerId: '',
  buttonClass: 'apple-pay-button-white',
  isEDD: false,
  orderEddDates: {},
  eddLabels: {},
  isEddABTest: false,
};

export default withStyles(ApplePaymentButton, styles);
export { ApplePaymentButton as ApplePaymentButtonVanilla };
