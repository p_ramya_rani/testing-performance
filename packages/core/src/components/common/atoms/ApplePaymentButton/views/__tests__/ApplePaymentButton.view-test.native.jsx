// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { NativeModules } from 'react-native';
import { ApplePaymentButtonVanilla } from '../ApplePaymentButton.view.native';

describe('Apple Payment Button', () => {
  const props = {
    setApplePayData: jest.fn(),
    verifyAppleAddressAction: jest.fn(),
    setApplePaymentInProgressView: jest.fn(),
    submitAppleBillingSection: jest.fn(),
    setApplePayAppDataAction: jest.fn(),
    loadShipmentMethods: jest.fn(),
    routeForBagCheckout: jest.fn(),
    openOOSModal: jest.fn(),
    getAppleClientTokenSuccess: jest.fn(),
    showBagLoader: jest.fn(),
    isGuest: false,
    orderId: '3000332630',
    shipModeId: '000000',
    isApplePayEnabled: true,
    isEligibleForApplePay: true,
    loading: true,
    onSuccess: true,
  };

  beforeEach(() => {
    NativeModules.ApplePayment = {
      initialize: jest.fn(),
      addListener: jest.fn(),
      removeListeners: jest.fn(),
    };
  });

  it('should render correctly', () => {
    const tree = shallow(<ApplePaymentButtonVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
