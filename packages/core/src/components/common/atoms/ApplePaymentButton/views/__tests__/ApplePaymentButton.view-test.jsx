// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import * as brainTree from 'braintree-web';
import * as utils from '@tcp/core/src/utils/utils';
import * as appleUtlis from '@tcp/core/src/components/common/atoms/ApplePaymentButton/container/ApplePaymentButton.util';
import * as checkoutAPIs from '@tcp/core/src/services/abstractors/CnC/Checkout';
import * as CartItemTile from '@tcp/core/src/services/abstractors/CnC/CartItemTile';
import { ApplePaymentButtonVanilla } from '../ApplePaymentButton.view';

jest.mock('braintree-web');
jest.mock('@tcp/core/src/utils/utils');
jest.mock('@tcp/core/src/services/abstractors/CnC/Checkout');
jest.mock('@tcp/core/src/services/abstractors/CnC/CartItemTile');
jest.mock(
  '@tcp/core/src/components/common/atoms/ApplePaymentButton/container/ApplePaymentButton.util'
);

utils.isApplePayEligible = jest.fn();
utils.isValidShippingAddress = jest.fn();
utils.getAPIConfig = jest.fn();
brainTree.client.create = jest.fn();
brainTree.applePay.create = jest.fn();
brainTree.dataCollector.create = jest.fn();
checkoutAPIs.getShippingMethods = jest.fn();
checkoutAPIs.updateShippingMethod = jest.fn();
CartItemTile.getCartData = jest.fn();
appleUtlis.getUnqualifiedItemsCall = jest.fn();
appleUtlis.verifyMellisaAddress = jest.fn();
appleUtlis.addAddressAndShippingMethod = jest.fn();
appleUtlis.isValidShippingAddress = jest.fn();
appleUtlis.getUpdateShippingPayload = jest.fn();
appleUtlis.getLedgerSummaryData = jest.fn();
appleUtlis.isCompleteContactInPaysheet = jest.fn();

describe('Apple Payment Button', () => {
  const props = {
    setApplePayData: jest.fn(),
    className: 'Apple-container sc-gqjmRU gvHLxz',
    isApplePayEnabled: true,
    authorizationKey: 'encrytptedauthorizationkey',
    applePayData: {
      deviceData: '762a73c4175ca24f7b1436a440da5bd0',
      supportedByBrowser: true,
      loading: false,
      contact: {
        a1: '2203  Woodland Avenue',
        a2: '',
        city: 'Amite',
        state: 'LA',
        postal: '70422',
        ctry: 'United States',
      },
    },
    appleClientTokenData: {
      userState: 'R',
      clientToken: 'encrytptedauthorizationkey',
    },
    allowNewBrowserTab: true,
    isGuest: false,
    orderId: '3000332630',
    setApplePaymentInProgressView: jest.fn(),
    getApplePaymentTokenAction: jest.fn(),
    setApplePayDataAction: jest.fn(),
    startApplePayCheckout: jest.fn(),
    getSetApplePayShippingValuesAction: jest.fn(),
    loadShipmentMethods: jest.fn(),
    setCheckoutCartData: jest.fn(),
    shipModeId: '10092',
    isEDD: false,
    shipmentMethods: [
      {
        displayName: 'Express ',
        shippingSpeed: 'Express',
        price: 30,
        id: '10092',
      },
      {
        displayName: 'Super Express ',
        shippingSpeed: '2 Days Delivery',
        price: 30,
        id: '10093',
      },
    ],
    orderType: 'PURE_ECOM',
    shippingContact: {
      addressLines: ['', ''],
      locality: 'test',
      postalCode: 'test',
      countryCode: 'test',
      administrativeArea: 'test',
    },
    pageLedgerSummaryData: {
      orderBalanceTotal: 1,
    },
    labels: {},
    setCartData: jest.fn(() => {}),
  };

  appleUtlis.formatShippingMethods = jest.fn();

  window.ApplePaySession = jest.fn(() => {
    return {
      begin: jest.fn(() => {}),
      completeShippingContactSelection: jest.fn(() => {}),
      completeShippingMethodSelection: jest.fn(() => {}),
      completePayment: jest.fn(() => {}),
    };
  });
  const error = {
    code: 'shippingContactInvalid',
    contactField: 'postalCode',
    message: 'Invalid postal code',
  };
  window.ApplePayError = jest.fn(() => {
    return {
      error,
    };
  });

  const session = window.ApplePaySession();
  utils.isApplePayEligible.mockImplementation(() => {
    return 'true';
  });

  appleUtlis.formatShippingMethods.mockImplementation(() => {
    return [
      {
        label: 'label',
        detail: 'detail',
        amount: '100.00',
        identifier: '1111',
      },
    ];
  });

  appleUtlis.getLedgerSummaryData.mockImplementation(() => {
    return {
      subTotal: 0,
      taxesTotal: 0,
      shippingTotal: 0,
      couponsTotal: 0,
      savingsTotal: 0,
      giftServiceTotal: 0,
      giftCardsTotal: 0,
      orderBalanceTotal: 0,
    };
  });

  const tree = shallow(<ApplePaymentButtonVanilla {...props} />);
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('button click', () => {
    brainTree.client.create.mockImplementation(() => {
      return Promise.resolve({ clientInstance: {} });
    });
    brainTree.applePay.create.mockImplementation(() => {
      const applePayInstance = {
        createPaymentRequest: () => {
          return {};
        },
      };
      return Promise.resolve(applePayInstance);
    });
    brainTree.dataCollector.create.mockImplementation(() => {
      return Promise.resolve({ deviceData: '{ "correlation_id":"234234"}' });
    });
    tree
      .find('div')
      .find('.apple-pay-button')
      .simulate('click');
  });

  it('test onShippingContactSelected with invalid Address', () => {
    const event = {
      shippingContact: {
        addressLines: ['', ''],
        locality: 'test',
        postalCode: 'test',
        countryCode: 'test',
        administrativeArea: 'test',
      },
    };

    // mock common methods
    appleUtlis.isValidShippingAddress.mockImplementation(() => {
      return { status: false };
    });

    appleUtlis.getUpdateShippingPayload.mockImplementation(() => {
      return {};
    });

    tree.instance().onShippingContactSelected(event, session);
  });

  it('test onShippingContactSelected with valid Address', () => {
    const event = {
      shippingContact: {
        addressLines: ['777 Brockton Avenue', ''],
        locality: 'Abington',
        postalCode: '02351',
        countryCode: 'US',
        administrativeArea: 'MA',
      },
    };
    appleUtlis.isValidShippingAddress.mockImplementation(() => {
      return { status: true };
    });
    checkoutAPIs.getShippingMethods.mockImplementation(() => {
      return Promise.resolve(props.shipmentMethods);
    });
    checkoutAPIs.updateShippingMethod.mockImplementation(() => {
      return Promise.resolve({ success: true });
    });

    CartItemTile.getCartData.mockImplementation(() => {
      return Promise.resolve({ orderDetails: { applePayShippingTotal: 100 } });
    });
    tree.instance().onShippingContactSelected(event, session);
  });

  it('test onShippingMethodSelected ', () => {
    checkoutAPIs.updateShippingMethod.mockImplementation(() => {
      return Promise.resolve({ success: true });
    });
    CartItemTile.getCartData.mockImplementation(() => {
      return Promise.resolve({ orderDetails: { applePayShippingTotal: 100 } });
    });
    const event = {
      shippingMethod: {
        identifier: '10092',
      },
    };
    tree.instance().onShippingMethodSelected(event, session);
  });

  it('test onPaymentAuthorized ', () => {
    const event = {
      payment: {
        shippingContact: {
          addressLines: ['777 Brockton Avenue', ''],
          locality: 'Abington',
          postalCode: '02351',
          countryCode: 'US',
          administrativeArea: 'MA',
          familyName: 'Rajeev',
          givenName: 'Jain',
          phoneNumber: '1111111',
          emailAddress: 'test@test.com',
        },
      },
    };

    appleUtlis.getUnqualifiedItemsCall.mockImplementation(() => {
      return Promise.resolve({ status: false });
    });

    appleUtlis.verifyMellisaAddress.mockImplementation(() => {
      return Promise.resolve({ status: true });
    });
    appleUtlis.addAddressAndShippingMethod.mockImplementation(() => {
      return Promise.resolve({ success: true });
    });

    appleUtlis.isCompleteContactInPaysheet.mockImplementation(() => {
      return {
        statusErr: true,
      };
    });

    CartItemTile.getCartData.mockImplementation(() => {
      return Promise.resolve({ orderDetails: { applePayShippingTotal: 100 } });
    });

    utils.getAPIConfig.mockImplementation(() => {
      return {
        storeId: '1111',
      };
    });

    const applePayInstance = {
      tokenize: jest.fn(),
    };
    tree.instance().onPaymentAuthorized(event, session, applePayInstance);
    tree.instance().submitPayment(event, session, '1234', {});
  });
});
