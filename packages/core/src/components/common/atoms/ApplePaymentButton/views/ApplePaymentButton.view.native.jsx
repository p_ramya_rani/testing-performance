/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { NativeModules, NativeEventEmitter, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
import Image from '@tcp/core/src/components/common/atoms/Image';
import { getCartData } from '@tcp/core/src/services/abstractors/CnC/CartItemTile';
import {
  getShippingMethods,
  updateShippingMethod,
} from '@tcp/core/src/services/abstractors/CnC/Checkout';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { getAppleToken } from '@tcp/core/src/services/abstractors/CnC/index';
import { getAPIConfig, getLabelValue } from '@tcp/core/src/utils';
import CheckoutConstants, {
  ORDER_TYPE,
} from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import withStyles from '../../../hoc/withStyles';
import logger from '../../../../../utils/loggerInstance';
import { ApplePayStyles, ApplePayButtonStyle } from '../styles/ApplePaymentButton.style.native';
import {
  APPLE_USER_STATES,
  formatShippingMethods,
  getLineItems,
  getTotal,
  getUpdateShippingPayload,
  getUnqualifiedItemsCall,
  getLedgerSummaryData,
  isValidShippingAddress,
  formatShippingMethodsForBopisBoss,
  addAddressAndShippingMethod,
  verifyMellisaAddress,
  isCompleteContactInPaysheet,
  ErrorCodes,
} from '../container/ApplePaymentButton.util';
import ClickTracker from '../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import names from '../../../../../constants/eventsName.constants';

const applePayIconWhite = require('../../../../../../../mobileapp/src/assets/images/apple-pay-icon-black.png');
const applePayIconBig = require('../../../../../../../mobileapp/src/assets/images/applepay-big.png');

class ApplePaymentButton extends PureComponent {
  constructor(props) {
    super(props);
    this.shipModeId = props.shipModeId;
    this.shippingContact = {};
    this.isContactUpdated = false;
    this.eventEmitter = new NativeEventEmitter(NativeModules.ApplePayment);

    this.paymentCancelListener = this.eventEmitter.addListener('cancelPayment', body => {
      console.log('Payment Cancel:-- ', body);
    });

    this.shippingMethodListener = this.eventEmitter.addListener('onShippingMethodChange', body => {
      this.onShippingMethodUpdated(body);
    });

    this.shippingContactListener = this.eventEmitter.addListener(
      'onShippingContactChange',
      body => {
        this.onShippingContactUpdated(body);
        this.isContactUpdated = true;
      }
    );

    this.shippingContactPaymentAuthorizeListener = this.eventEmitter.addListener(
      'shippingContactPaymentAuthorize',
      body => {
        console.log('Payment Authorize Shipping Contact:-- ', body);
        if (body && body.method) {
          this.validateShippingContact(body.method);
        }
      }
    );
  }

  componentDidMount() {
    const { isApplePayEnabled } = this.props;
    if (isApplePayEnabled) {
      this.initApplePay();
    } else {
      this.hideApplePayButton();
    }
  }

  componentWillUnmount() {
    this.paymentCancelListener.remove();
    this.shippingMethodListener.remove();
    this.shippingContactListener.remove();
    this.shippingContactPaymentAuthorizeListener.remove();
  }

  suppressMelissaForBopisAndBoss = orderType => {
    if (!(orderType === ORDER_TYPE.PURE_ECOM || orderType === ORDER_TYPE.ECOM_MIX)) {
      this.isMellisaValidationCompleted = true; // suppress melissa call in case of boss and bopis
    }
  };

  validateShippingContact = contactAddress => {
    const { labels, orderType } = this.props;
    // Check if any fields in contact are blank?
    const { statusErr, errors } = isCompleteContactInPaysheet(labels, contactAddress);

    if (!statusErr) {
      // show errors on paysheet
      const errorPayload = {
        errors,
      };
      NativeModules.ApplePayment.paymentValidation(errorPayload);
    } else {
      this.suppressMelissaForBopisAndBoss(orderType);
      // check shipping address for Melissa validation
      verifyMellisaAddress(this.isMellisaValidationCompleted, contactAddress, labels).then(
        result => {
          const { status, isMellisaValidationCompleted, errors: errorArr } = result;
          this.isMellisaValidationCompleted = isMellisaValidationCompleted;

          // show error on paysheet if Melisa reports issue
          if (!status) {
            const errorPayload = {
              errors: errorArr,
            };
            NativeModules.ApplePayment.paymentValidation(errorPayload);
          } else {
            this.shippingContact = contactAddress;
            // generate nonce if shipping address is fine
            NativeModules.ApplePayment.paymentValidation({});
          }
        }
      );
    }
  };

  showDefaultError = (logMessage = '', logError = {}) => {
    logger.error('Apple Pay', logMessage, logError);
    const { errorLabels, toastMessage } = this.props;

    const errorMessage = errorLabels.DEFAULT || 'Oops... an error occurred. Please try again !!';
    toastMessage(errorMessage);
  };

  onShippingMethodUpdated = body => {
    try {
      const { labels } = this.props;
      const payload = getUpdateShippingPayload(
        this.shippingContact,
        body.method.identifier,
        'true'
      );
      updateShippingMethod(payload).then(res => {
        if (res.success) {
          this.shipModeId = body.method.identifier;
          // ** Update Cart ***/
          getCartData({}).then(cart => {
            this.updateCartInRedux(cart.orderDetails);
            const pageLedgerSummaryDataUpdated = getLedgerSummaryData(cart);
            const requestPayload = {
              total: getTotal(pageLedgerSummaryDataUpdated.orderBalanceTotal, labels, 'final'),
              lineItems: getLineItems(pageLedgerSummaryDataUpdated, labels),
            };
            NativeModules.ApplePayment.shippingMethodValidation(requestPayload);
          });
        } else if (res.errorCode === ErrorCodes.ERR_INVALID_CITY) {
          this.handleServiceErrors(res);
        }
      });
    } catch (error) {
      this.showDefaultError('Error updating shipping method', error);
    }
  };

  onShippingContactUpdated = body => {
    try {
      const { postalCode, administrativeArea } = body.method;
      const {
        labels,
        allowedApplePayStates,
        pageLedgerSummaryData,
        shipmentMethods,
        isEDD,
        orderEddDates,
        eddLabels,
        isEddABTest,
      } = this.props;
      const { status, errors } = isValidShippingAddress(labels, allowedApplePayStates, body.method);
      if (!status) {
        const requestPayload = {
          shippingMethods: formatShippingMethods(shipmentMethods, this.shipModeId, {
            isEDD,
            orderEddDates,
            eddLabels,
            isEddABTest,
          }),
          total: getTotal(pageLedgerSummaryData.orderBalanceTotal, labels, 'final'),
          lineItems: getLineItems(pageLedgerSummaryData, labels),
          errors,
        };
        NativeModules.ApplePayment.shippingContactValidation(requestPayload);
      } else {
        this.shippingContact = body.method;
        // ***** Get Shipping Methods Based on Selected Contact */
        this.fetchShippingMethods(postalCode, administrativeArea)
          .then(shippingMethods => {
            // ***** Update Selected Shipping Method */
            const payload = getUpdateShippingPayload(
              this.shippingContact,
              shippingMethods[0].identifier,
              'true'
            );
            updateShippingMethod(payload)
              .then(res => {
                if (res.success) {
                  this.shipModeId = shippingMethods[0].identifier;
                  // ***** Update Cart ****/
                  getCartData({}).then(cart => {
                    this.updateCartInRedux(cart.orderDetails);
                    const updatedPageLedgerSummaryData = getLedgerSummaryData(cart);
                    const requestPayload = {
                      shippingMethods,
                      total: getTotal(
                        updatedPageLedgerSummaryData.orderBalanceTotal,
                        labels,
                        'final'
                      ),
                      lineItems: getLineItems(updatedPageLedgerSummaryData, labels),
                    };
                    NativeModules.ApplePayment.shippingContactValidation(requestPayload);
                  });
                } else if (res.errorCode === ErrorCodes.ERR_INVALID_CITY) {
                  this.handleServiceErrors(res);
                }
              })
              .catch(error => {
                logger.error('Error updating shipping method after adding new address', error);
                const errorPayload = {
                  errors: [
                    {
                      error: 'shippingContactInvalid',
                      field: 'addressLines',
                      label: '',
                      errorType: 'shipping',
                    },
                  ],
                };
                NativeModules.ApplePayment.shippingContactValidation(errorPayload);
              });
          })
          .catch(error => {
            if (error.errorCodes === ErrorCodes.ERR_US_ZIPCODE_INVALID) {
              const shippingMethods = formatShippingMethods(shipmentMethods, this.shipModeId, {
                isEDD,
                orderEddDates,
                eddLabels,
                isEddABTest,
              });
              const total = getTotal(pageLedgerSummaryData.orderBalanceTotal, labels, 'pending');
              const lineItems = getLineItems(pageLedgerSummaryData, labels);
              const errorArr = [
                {
                  error: 'shippingContactInvalid',
                  field: 'zipCode',
                  label: getLabelValue(labels, 'InvalidPostalCode'),
                },
              ];
              const requestPayload = {
                shippingMethods,
                total,
                lineItems,
                errors: errorArr,
              };
              NativeModules.ApplePayment.shippingContactValidation(requestPayload);
            }
          });
      }
    } catch (error) {
      this.showDefaultError('Error updating shipping contact', error);
    }
  };

  updateCartInRedux = orderDetails => {
    const { setCartData } = this.props;
    const { applePayShippingTotal } = orderDetails;
    setCartData({ ...orderDetails, shippingTotal: applePayShippingTotal });
  };

  fetchDefaultShippingMethods = () => {
    const { loadShipmentMethods } = this.props;
    const requestPayload = {
      payload: {
        pageEvent: CheckoutConstants.ERR_STATE_DOES_NOT_MATCH_SHIPMENT_OPTION,
      },
    };
    loadShipmentMethods(requestPayload);
  };

  hideApplePayButton = () => {
    const { setApplePayAppDataAction } = this.props;
    setApplePayAppDataAction({
      loading: false,
      isEligibleForApplePay: false,
    });
  };

  showApplePayButton = () => {
    const { setApplePayAppDataAction } = this.props;
    setApplePayAppDataAction({
      loading: false,
      isEligibleForApplePay: true,
    });
  };

  isSuccessResponse = data => {
    return data === 'true';
  };

  initializeSDK = token => {
    try {
      NativeModules.ApplePayment.initialize(token, data => {
        if (this.isSuccessResponse(data)) {
          const { shipmentMethods, shipModeId } = this.props;
          if (shipmentMethods && shipmentMethods.size === 0 && shipModeId)
            this.fetchDefaultShippingMethods();
          this.showApplePayButton();
        } else {
          this.hideApplePayButton();
        }
      });
    } catch (err) {
      logger.error('Apple Pay', 'Error invoking NativeModules.ApplePayment.initialize', err);
    }
  };

  initApplePay = () => {
    try {
      const { isGuest, orderId, applePayEnabledWithCard } = this.props;
      const userState = isGuest ? APPLE_USER_STATES.GUEST : APPLE_USER_STATES.REGISTERED;

      // validate if Apple pay pre configured card needs to checked
      const checkDeviceSupport = applePayEnabledWithCard
        ? 'isApplePayAvailable'
        : 'isDeviceSupportApplePay';

      // validate if APAY is supported
      NativeModules.ApplePayment[checkDeviceSupport](data => {
        if (this.isSuccessResponse(data)) {
          this.tokenizeAndInit(userState, orderId);
        } else {
          this.hideApplePayButton();
        }
      });
    } catch (err) {
      logger.error('Apple Pay', 'error while invoking initApplePay', err);
      this.hideApplePayButton();
    }
  };

  createPaymentRequest = () => {
    const {
      shippingContact,
      pageLedgerSummaryData,
      shipmentMethods,
      isEDD,
      orderEddDates,
      eddLabels,
      showBagLoader,
      orderType,
      stores,
      labels,
      setApplePaymentInProgressView,
      isGuest,
      isEddABTest,
    } = this.props;
    const { orderBalanceTotal } = pageLedgerSummaryData;
    const { postalCode, administrativeArea } = shippingContact;
    const contact = postalCode || administrativeArea ? shippingContact : {};
    this.shippingContact = shippingContact;

    let payloadSpecificToOrderType = {};

    if (orderType === ORDER_TYPE.PURE_ECOM || orderType === ORDER_TYPE.ECOM_MIX) {
      payloadSpecificToOrderType = {
        requiredShippingContactFields: ['name', 'phone', 'postalAddress', 'email'],
        shippingMethods: formatShippingMethods(shipmentMethods, this.shipModeId, {
          isEDD,
          orderEddDates,
          eddLabels,
          isEddABTest,
        }),
        shippingType: ['shipping'],
      };
    } else {
      payloadSpecificToOrderType = {
        requiredShippingContactFields: ['name', 'phone', 'email'],
        shippingMethods: formatShippingMethodsForBopisBoss(stores, labels),
        shippingType: ['storePickup'],
      };
    }

    const requestPayload = {
      countryCode: 'US',
      currencyCode: 'USD',
      merchantCapabilities: ['supports3DS'],
      supportedNetworks: ['visa', 'masterCard', 'amex', 'discover'],
      total: getTotal(orderBalanceTotal, labels, 'final'),
      billingContact: contact,
      lineItems: getLineItems(pageLedgerSummaryData, labels),
      requiredBillingContactFields: ['phone', 'name'],
      shippingContact: contact,
      isGuest,
      ...payloadSpecificToOrderType,
    };

    // invoked once the user clicks on authorize on the Paysheet
    NativeModules.ApplePayment.tappedApplePay(requestPayload, (error, nonce) => {
      if (error) {
        this.showDefaultError('tap pay button error', error);
      } else {
        showBagLoader(true);
        this.applePayNonce = nonce;

        const event = {
          payment: {
            shippingContact: {
              ...this.shippingContact,
              addressId: (!this.isContactUpdated && requestPayload.shippingContact.addressId) || 0,
            },
          },
        };
        setApplePaymentInProgressView(true);
        this.onPaymentAuthorized(event);
      }
    });
  };

  // Get Shipping Method API Call
  fetchShippingMethods = async (zipCode, state) => {
    const { errorLabels, isEDD, orderEddDates, eddLabels, isEddABTest } = this.props;
    const payload = {
      state,
      zipCode,
    };
    const eventType =
      zipCode || state
        ? CheckoutConstants.CHANGE_SHIPMMENT_METHODS
        : CheckoutConstants.LOAD_SHIPMMENT_METHODS;
    const shipmentMethods = await getShippingMethods(payload, errorLabels, eventType);
    return formatShippingMethods(shipmentMethods, this.shipModeId, {
      isEDD,
      orderEddDates,
      eddLabels,
      isEddABTest,
    });
  };

  onClickApplePayButton = () => {
    const { openOOSModal, showBagLoader, isBillingPage } = this.props;
    showBagLoader(true);
    // TODO APAY: test flow on back button on modal
    // modify modal as per req
    // add catch block to show error
    getUnqualifiedItemsCall()
      .then(res => {
        if (res && res.status) {
          openOOSModal({ items: res, fromApplePayBillingPage: isBillingPage });
        } else {
          this.createPaymentRequest();
        }
      })
      .catch(err => {
        this.showDefaultError('Unqualified API failure', err);
      })
      .finally(() => {
        showBagLoader(false);
      });
  };

  tokenizeAndInit = async (userState, orderId) => {
    const { getAppleClientTokenSuccess, authorizationKey } = this.props;
    let tokenResponse = '';
    if (authorizationKey) {
      tokenResponse = authorizationKey;
    } else {
      try {
        const token = await getAppleToken({ userState, orderId });
        tokenResponse =
          token && token.appleClientTokenData && token.appleClientTokenData.clientToken;
      } catch (err) {
        this.hideApplePayButton();
        logger.error('Apple Pay', 'error while invoking tokenizeAndInit ,Tokenize Error', err);
      }
    }

    if (tokenResponse) {
      getAppleClientTokenSuccess({
        appleClientTokenData: {
          clientToken: tokenResponse,
        },
      });
      this.initializeSDK(tokenResponse);
    } else {
      this.hideApplePayButton();
    }
  };

  handlingMelissaAndBillingInformation = (event, addressId) => {
    const {
      orderId,
      submitAppleBillingSection,
      pageLedgerSummaryData,
      navigation,
      showBagLoader,
      orderType,
    } = this.props;
    const { orderBalanceTotal } = pageLedgerSummaryData;

    const payload = {
      nonce: this.applePayNonce,
    };

    let contact = {};
    if (orderType === ORDER_TYPE.PURE_ECOM || orderType === ORDER_TYPE.ECOM_MIX) {
      contact = {
        address1: event.payment.shippingContact.addressLines[0],
        address2: event.payment.shippingContact.addressLines[1],
        city: event.payment.shippingContact.locality,
        zip: event.payment.shippingContact.postalCode,
        country: event.payment.shippingContact.countryCode.toUpperCase(),
        state: event.payment.shippingContact.administrativeArea.toUpperCase(),
      };
    }

    const requestData = {
      orderType,
      contact,
      paymentNonce: payload.nonce,
      storeId: getAPIConfig().storeId,
      email: event.payment.shippingContact.emailAddress,
      amount: orderBalanceTotal,
      orderId,
      addressId,
      navigation,
      navigationActions: NavigationActions,
    };

    showBagLoader(false);
    submitAppleBillingSection(requestData);
  };

  handleServiceErrors = res => {
    const {
      routeForBagCheckout,
      navigation,
      displayServerErrorCheckout,
      orderType,
      errorLabels,
    } = this.props;
    if (res.errorCode === ErrorCodes.ERR_INVALID_CITY) {
      displayServerErrorCheckout({
        errorMessage: errorLabels.ERR_INVALID_CITY,
        component: 'Apple Pay',
      });
    } else if (res.errorCode === ErrorCodes.INCORRECT_SHIPMODE_ID) {
      displayServerErrorCheckout({
        errorMessage: res.errorMsg,
        component: 'Apple Pay',
      });
    }
    const isEcomOnly = orderType === ORDER_TYPE.PURE_ECOM;
    routeForBagCheckout({
      navigation,
      navigationActions: NavigationActions,
      recalc: false,
      checkoutStage: isEcomOnly
        ? CheckoutConstants.SHIPPING_DEFAULT_PARAM
        : CheckoutConstants.PICKUP_DEFAULT_PARAM,
    });
  };

  addAddressAndPaymentAuthorization = (
    event,
    orderType,
    setCheckoutCartData,
    displayServerErrorCheckout,
    showBagLoader,
    routeForBagCheckout,
    navigation
  ) => {
    addAddressAndShippingMethod(event.payment.shippingContact, this.shipModeId, orderType)
      .then(res => {
        if (
          res.success &&
          res.errorCode !== ErrorCodes.INCORRECT_SHIPMODE_ID &&
          res.errorCode !== ErrorCodes.ERR_INVALID_CITY
        ) {
          getCartData({}).then(cart => {
            this.updateCartInRedux(cart.orderDetails);
            setCheckoutCartData({ res: cart });
            this.handlingMelissaAndBillingInformation(event, res.addressId);
          });
        } else {
          this.handleServiceErrors(res);
        }
      })
      .catch(err => {
        showBagLoader(false);
        routeForBagCheckout({
          navigation,
          navigationActions: NavigationActions,
          recalc: false,
          checkoutStage: CheckoutConstants.SHIPPING_DEFAULT_PARAM,
        });
        logger.error('Apple Pay', 'Error after invoking onPaymentAuthorized', err);
      });
  };

  onPaymentAuthorized = event => {
    const {
      routeForBagCheckout,
      navigation,
      showBagLoader,
      setCheckoutCartData,
      displayServerErrorCheckout,
      orderType,
    } = this.props;

    this.addAddressAndPaymentAuthorization(
      event,
      orderType,
      setCheckoutCartData,
      displayServerErrorCheckout,
      showBagLoader,
      routeForBagCheckout,
      navigation
    );
  };

  shouldRenderApplePay = () => {
    let showButton = false;
    const { isEligibleForApplePay } = this.props;

    if (isEligibleForApplePay) {
      showButton = true;
    }

    return showButton;
  };

  renderApplePayButton = () => {
    const { isBillingPage } = this.props;
    const imgSource = isBillingPage ? applePayIconBig : applePayIconWhite;
    const dimensions = {
      width: '100%',
      height: isBillingPage ? '44px' : '25px',
    };

    const analyticsClickLocation = isBillingPage ? 'APAY Regular Shipping' : 'APAY Regular Bag';

    return this.shouldRenderApplePay() ? (
      <ClickTracker
        as={TouchableOpacity}
        name={names.screenNames.applePay_e148}
        clickData={{
          customEvents: ['event148'],
          applePayButtonLocation: analyticsClickLocation,
        }}
        module="checkout"
        onPress={() => this.onClickApplePayButton()}
      >
        <ApplePayButtonStyle
          accessibilityLabel="Apple Pay Button"
          accessibilityRole="button"
          isBillingPage={isBillingPage}
        >
          <Image source={imgSource} {...dimensions} />
        </ApplePayButtonStyle>
      </ClickTracker>
    ) : null;
  };

  render() {
    const { isApplePayEnabled, loading, isBillingPage } = this.props;
    if (!isApplePayEnabled) {
      return null;
    }
    return loading ? (
      <LoaderSkelton height={isBillingPage ? '44px' : '42px'} />
    ) : (
      this.renderApplePayButton()
    );
  }
}

ApplePaymentButton.propTypes = {
  isGuest: PropTypes.bool.isRequired,
  orderId: PropTypes.string.isRequired,
  verifyAppleAddressAction: PropTypes.func,
  setApplePaymentInProgressView: PropTypes.func,
  submitAppleBillingSection: PropTypes.func,
  shippingContact: PropTypes.shape({}),
  pageLedgerSummaryData: PropTypes.shape({}).isRequired,
  errorLabels: PropTypes.shape({}).isRequired,
  shipModeId: PropTypes.string.isRequired,
  isApplePayEnabled: PropTypes.bool,
  setApplePayAppDataAction: PropTypes.func.isRequired,
  isEligibleForApplePay: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  loadShipmentMethods: PropTypes.func.isRequired,
  shipmentMethods: PropTypes.shape([]).isRequired,
  onSuccess: PropTypes.func.isRequired,
  getSetCheckoutStage: PropTypes.func,
  navigation: PropTypes.shape({}).isRequired,
  setCartData: PropTypes.isRequired,
  applePayEnabledWithCard: PropTypes.bool,
  routeForBagCheckout: PropTypes.func.isRequired,
  labels: PropTypes.shape({}),
  openOOSModal: PropTypes.func.isRequired,
  isBillingPage: PropTypes.bool,
  authorizationKey: PropTypes.string,
  getAppleClientTokenSuccess: PropTypes.func.isRequired,
  showBagLoader: PropTypes.func.isRequired,
  allowedApplePayStates: PropTypes.string.isRequired,
  setCheckoutCartData: PropTypes.func.isRequired,
  isEDD: PropTypes.bool,
  orderEddDates: PropTypes.shape({}),
  eddLabels: PropTypes.shape({}),
  orderType: PropTypes.string.isRequired,
  toastMessage: PropTypes.func.isRequired,
  stores: PropTypes.shape({}).isRequired,
  isEddABTest: PropTypes.bool,
  displayServerErrorCheckout: PropTypes.func.isRequired,
};
ApplePaymentButton.defaultProps = {
  verifyAppleAddressAction: () => {},
  setApplePaymentInProgressView: () => {},
  submitAppleBillingSection: () => {},
  shippingContact: {},
  isBillingPage: false,
  isApplePayEnabled: false,
  getSetCheckoutStage: () => {},
  applePayEnabledWithCard: false,
  labels: {},
  authorizationKey: '',
  isEDD: false,
  orderEddDates: {},
  eddLabels: {},
  isEddABTest: false,
};

export default withStyles(ApplePaymentButton, ApplePayStyles);
export { ApplePaymentButton as ApplePaymentButtonVanilla };
