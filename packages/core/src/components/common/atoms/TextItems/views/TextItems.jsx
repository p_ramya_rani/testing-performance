// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import Image from '../../Image';
import errorBoundary from '../../../hoc/withErrorBoundary';
import withStyles from '../../../hoc/withStyles';
import { getIconPath } from '../../../../../utils';
import textItemStyle, { StyledTextItem } from '../TextItems.style';

const getIcon = icon =>
  icon && <Image src={getIconPath(icon)} alt={icon} className="header-icon" />;

const finalText = (str, placement, icon) => {
  const text = str.split(' ');
  if (text.length > 1 && icon && placement && placement === 'middle') {
    return (
      <>
        {text[0]}
        {getIcon(icon)}
        {text.slice(1, text.length).join(' ')}
      </>
    );
  }
  return str;
};

const TextItems = ({ className, textItems, icon, titleStyle = {}, headerTextStyles = {} }) => {
  const { placement, icon: iconName } = icon || {};
  if (!textItems) {
    return null;
  }
  return textItems.map(({ style, text, styledObjWeb = '' }, index) => {
    const headerTextStyle = Array.isArray(headerTextStyles) ? headerTextStyles[index] : {};

    return (
      <span className={className}>
        {iconName && placement && placement === 'left' && getIcon(iconName)}
        <StyledTextItem
          key={index.toString()}
          className={style}
          style={{ ...titleStyle, ...headerTextStyle }}
          customStyle={styledObjWeb}
        >
          {index ? ` ${text}` : finalText(text, placement, iconName)}
        </StyledTextItem>
        {iconName && placement && placement === 'right' && getIcon(iconName)}
      </span>
    );
  });
};

export default errorBoundary(withStyles(TextItems, textItemStyle));
export { TextItems as TextItemsVanilla };
