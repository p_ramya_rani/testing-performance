// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { TextItemsVanilla } from '../views/TextItems';

describe('TextItems component', () => {
  it('renders correctly', () => {
    const props = {
      textItems: [
        {
          style: 'test',
          text: 'test',
        },
        {
          style: 'test1',
          text: 'test2',
        },
      ],
    };
    const component = shallow(<TextItemsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
