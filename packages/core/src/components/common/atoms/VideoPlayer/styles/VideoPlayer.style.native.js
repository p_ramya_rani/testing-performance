// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const VideoWrapper = styled.View`
  position: relative;
`;

const StyledImage = styled.Image`
  height: 56px;
  width: 56px;
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 1;
  transform: translateX(-28px) translateY(-28px);
`;

export { VideoWrapper, StyledImage };
