// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import { mediaQuery } from '@tcp/core/styles/themes/TCP/mediaQuery';

export default css`
  width: 100%;
  height: 100%;
`;

export const StyledVideoContainer = styled.div`
  .vjs-tech {
    ${props => props.overrideStyle || ''}
  }
`;

export const getVideoStyle = heightOverrides => {
  const { heightD, heightT, heightM } = heightOverrides || {};
  return `
    width: 100% !important;
    ${heightM ? `height: ${heightM}vw !important;` : null}
    @media ${mediaQuery.medium} {
      ${heightT ? `height: ${heightT}vw !important;` : null}

    }
    @media ${mediaQuery.large} {
      ${heightD ? `height: ${heightD}vw !important;` : null}
    }
  `;
};
