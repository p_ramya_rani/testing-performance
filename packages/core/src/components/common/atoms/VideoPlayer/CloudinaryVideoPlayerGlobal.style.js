/* eslint-disable max-lines */
/* stylelint-disable */
// 9fbef606107a605d69c0edbcd8029e5d
import { createGlobalStyle } from 'styled-components';

/*
  All content:'' property with spacial character has been escaped to support css in JS.
  For instance: content: '\2022' has been updated to content: '\\2012'
*/
export const CloudinaryVideoPlayerGlobalStyle = createGlobalStyle`
.video-js .vjs-big-play-button .vjs-icon-placeholder:before,
.vjs-button > .vjs-icon-placeholder:before,
.video-js .vjs-modal-dialog,
.vjs-modal-dialog .vjs-modal-dialog-content {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}

.video-js, video {
  background-color: #ffffff;
}

.video-js .vjs-big-play-button .vjs-icon-placeholder:before,
.vjs-button > .vjs-icon-placeholder:before {
  text-align: center;
}

@font-face {
  font-family: VideoJS;
  src: url(data:application/font-woff;charset=utf-8;base64,d09GRgABAAAAABBIAAsAAAAAGoQAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADsAAABUIIslek9TLzIAAAFEAAAAPgAAAFZRiV3RY21hcAAAAYQAAADQAAADIjn098ZnbHlmAAACVAAACv4AABEIAwnSw2hlYWQAAA1UAAAAKwAAADYV1OgpaGhlYQAADYAAAAAbAAAAJA4DByFobXR4AAANnAAAAA8AAACE4AAAAGxvY2EAAA2sAAAARAAAAEQ9NEHGbWF4cAAADfAAAAAfAAAAIAEyAIFuYW1lAAAOEAAAASUAAAIK1cf1oHBvc3QAAA84AAABDwAAAZ5AAl/0eJxjYGRgYOBiMGCwY2BycfMJYeDLSSzJY5BiYGGAAJA8MpsxJzM9kYEDxgPKsYBpDiBmg4gCACY7BUgAeJxjYGQ7xTiBgZWBgaWQ5RkDA8MvCM0cwxDOeI6BgYmBlZkBKwhIc01hcPjI+FGBHcRdyA4RZgQRAC4HCwEAAHic7dFprsIgAEXhg8U61XmeWcBb1FuQP4w7ZQXK5boMm3yclFDSANAHmuKviBBeBPQ8ymyo8w3jOh/5r2ui5nN6v8sYNJb3WMdeWRvLji0DhozKdxM6psyYs2DJijUbtuzYc+DIiTMXrty4k8oGLb+n0xCe37ekM7Z66j1DbUy3l6PpHnLfdLO5NdSBoQ4NdWSoY9ON54mhdqa/y1NDnRnq3FAXhro01JWhrg11Y6hbQ90Z6t5QD4Z6NNSToZ4N9WKoV0O9GerdUJORPqkhTd54nJ1YDXBU1RV+576/JBs2bPYPkrDZt5vsJrv53V/I5mclhGDCTwgGBQQSTEji4hCkYIAGd4TGIWFAhV0RQTpWmQp1xv6hA4OTOlNr2zFANbHUYbq2OtNCpViRqsk+e+7bTQAhzti8vPfuPffcc88959zznbcMMPjHD/KDDGEY0ABpYX384NhlomIYlo4JISGEY9mMh2FSidYiqkEUphtNYDSY/dXg9023l4DdxlqUl0chuZRhncJKrsCQHIwcGuwfnhMIzBnuH4Sym+1D2zaGjheXlhYfD238z80mKYMmvJ5XeOTzd8z9eujbMxJNhu4C9xPE/bCMiDuSNIWgkTQwBE55hLSAE7ZwhrHLnAHZOGV/kmBGTiNjZxzI77Hb7Hqjz68TjT6vh+5JT/cCIkqS0D6CqPf5jX4Qjdx5j6vlDfZM4aZFdbVXIxtOlJaP/WottMnH6CJQ3bTiue3PrY23HjnChtuamxwvvzFjxkPrNj3z0tG9T561HDYf6OgmRWvlY3JQHoQb8ltV2Yet7YfWctEjR1AtxS/cSX6U4alf6NJEBQ7YKg9wrXQKd0IeZCb2ux75Uhh1Un+Nz+9LTOE7PK777nN5xqdTneTBhCbx446mZrhnUkrCz2YhA9dSMxaG0SYmT8hi9ZPu1E94PJYQSH6LRmhxec7Q7ZeXntgQuVpbh+a4qWNsckVyTdn0P7o7DpgPW84+uRcq0BITflBikGdUjAZ9wYBVI3mtrNvr9kpg1UsaK6t3690aoorC1lg0GpMH2HAMtkZjsSi5Ig9ESVosOh7GQfLjKNLvKpMKkLSKNFAka710GdgSi8oDMSoNhqjkKBXTgn3swtaxyzGkUzIzae9RtLdWkSlZ1KDX6EzgllzV4NV4SoDFSOGD4+HCeQUF8wrZ5Hs8zIb5EaVxy8DYFTbMCJPnLIWZxugZE2NlivC0gc1qEQUR8jEKgZcAXeH18BiCgl5nlHh0CrjB4Hb5fX4gb0J7c9PuHVsfgkx2n/vTY/JV8kn8PGxf7faOZ8qX8JVByuIf4whk9sqXli2hvPJV9hrp0hY7l8r2x37ydaVsb4xvXv/47v2NjfCl8m5oRDJclFMoE1yk0Uh1Te4/m8lFXe9qBZD0EkheicebXvzI2PLCuoKCukLuhPIeKwaHPEouxw3kMqaIUXDQ1p0mip+MyCORSCQaoUsnY1VZ38nUTrG21WvVo4f1OsEJFhvSfAFwGfT8VHRMeAVUpwLOoLzjT/REIj3O3FhuURE+nERF+0pTId5Fyxv5sfwGyg4O+my4vZv0sZm7oeQlFZORiB+tG0MweVNraeitl7yxiPIHTk4/diVxs94o5lEYishB2iAtkchEnsActoEpx44Fo8XnsQMaA22BlqC20RmhBKzYojZyYaxg+JggMc4HHY2m+L9EkWSYljirOisrO7d3VorxzyZ6Vc4lJqITAu1b2wOBdrLElAP+bFc2eGaZFVbkmJktv5uT6Jlz5D/MnBFor6ig/JPnRViBsV3LNKGGqB1ChJ0tgQywlVLFJIuQgTFttwkiKxhyQdAZMdMYtSaoAewqfvXVYPAbDT6/1mez85YS8FSDywQ6NfAnef6FNEGMilnppyvn5rB6tTyq1pOceRWnp2WJEZFXHeX5oyoem1nTTgdqc4heDY7bOeKz63vnz+/dRx+s31Ht2JGanQ5seirfWJL9tjozU/12TnEjn5oux9OzU3ckGbBzBwNOyk69JykKH0n/0LM9A72tuwM3zQpIRu4AxiToseEpgPOmbROyFe9/X2yeUvoUsCyEvjcgs7fpWP3/aKlFN0+6HFUe6D9HFz/XPwBlN9tTqNyZjFJ8UO2RUT5/h4CptCctEyeisnOyXjALEp7dXKaQKf6O7IMnGjNNACRMLxqdYJX8eMLvmmd68D+ayBLyKKYZwYxDt/GNhzETDJ05Qxlyi3pi3/Z93ndYVSumgj0V/KkIFlO6+1K3fF2+3g0q+YtuSIf0bvmLqV09nnobI6hwcjIP8aPCKayjsF5JBY3LaKAeRLSyYB1h81oTwe9SlPMkXB7G0mfL9q71gaqqwPqu67QRKS1+ObTx+sbQy9QV2OQHEScGkdFBeT7v7qisqqrs6N52i78/R+6S0qQONVj26agOVoswCyQWIV5D86vH53bxNUeXV0K+XZaHv/nm/KsHhOvylwsWnJX/HE8l/4WCv5x+l5n08z6UU8bUMa3MBpSmM7F63AxntdC9eBCKEZW9Hr+ABNqtxgAQrSbMtmrW7lKQuoSgBhSrTazWVU2QAKWY8wiiuhqFmQgWJBgoXiuWIm42N7hqZbBsgXz52O5P5uSvaNgFGnOuvsRw8I8Laha91wMvDuxqWFheN7/8GVtTltdS83DQsXRmqc5ZtcJXEVrlV2doTWk5+Yunm71dG5f55m/qY0MjI93vv9/NfpxXV9sUXrxy2fbNy1or65cOlDRnOoKFeeXcbw42H/bNDT5Qs3flgs31gWC1lD1nfUV/X7NdCnSUdHY2e8afzfKsqZ5ZljfDqjLOmk3UebNXB+aHArPYDRs+/HDDxeT5DiP+sFg7OpRaVQMGBV89PpeBdj22hCE0Uub0UqwLrNWsG0cuyadgLXTeR5rbO4+3c/vl15cur2nRq+TXCQDcS3SO+s6ak+e5/eMS+1dw3btu3YG2tvFL8XdIZvdjdW6TO/4B7IdrZWVPmctm5/59AgsPItTSbCiIBr2OqIGzmu20SMKAS7yqwGBUfGfgjDYlLLDeF0SfcLB2LSx8flT+08/kzz6yOj96rft4rpTjdPQcmLd47uKibbDq7ZSz/XtbH2nN717Nd62rU+c8Icevvv7I09wA6WvjVcafb+FsbNG+ZQ80Rn6ZZsvrP7teP2dzTdoETvNhjCmsr8FID2sJ69VYvdUcxk4AzYRlKcaE38eXNRlfW9H1as9i6acLHp1XpuNB5K7DIvkX08y1ZYvh3KfWaiCzH+ztrSDmD7LuX73x/mJelB8Yj39t8nhNQJJ2CAthpoFGLsGgtSOCJooCGoaJAMTjSWHVZ08YAa1Fg9lPI5U6DOsGVjDasJeZZ+YyhfCwfOzCxlBA69M9XLXtza7H/rav+9Tjq5xNi0wpKQIRNO4Lrzz7yp5QVYM6Jd/oc1Uvn/mQhhuWh6ENXoS2YTZ8QT42bF5d/559zp5r0Uff2VnR2tdf2/WCOd2cO0Mw6qpWPnvxpV0nrt5fZd2yItc199GWe8vlNfNDq+CH/7yAAnB9hn7T4QO4c1g9ScxsZgmzntnE/IDGndtHMw69lFwoCnYsMGx+rBp8JSBqdLzBr9QRPq/PbhWMWFtQZp1xguy/haw3TEHm3TWAnxFWQQWgt7M5OV0lCz1VRYucpWliy7z6Zd4urwPIyeZQqli2Lgg7szJV09PysATbOQtYIrB2YzbkJYkGgJ0m4AjPUap1pvYu1K9qr97z0Yl3p332b2LYB78ncYIlRkau/8GObSsOlZancACE5d5ily+c2+7h5Yj4lqhVmXXB+iXLfvdqSgqfKtQvfHDV0OnvQR1qhw42XS/vkvsh/hXcrDFP0a+SJNIomEfD1nsrYGO+1bgTOJhM8Hv6ek+7vVglxuSRwoKn17S937bm6YJCeSSG0Op1n+7tE37tcZ/p7dsTv4EUrGpDbWueKigsLHhqTVsoEj+JU0kaSjnj9tz8/gryQWwJ9BcJXBC/7smO+I/IFURJetFPrdt5WcoL6DbEJaygI8CTHfQTjf40ofD+DwalTqIAAHicY2BkYGAA4gDud4bx/DZfGbjZGUDg+q1z05BpdkawOAcDE4gCAB45CXEAeJxjYGRgYGcAARD5/z87IwMjAypQBAAtgwI4AHicY2BgYGAfYAwAOkQA4QAAAAAAAA4AaAB+AMwA4AECAUIBbAGYAcICGAJYArQC4AMwA7AD3gQwBJYE3AUkBWYFigYgBmYGtAbqB1gIEghYCG4IhHicY2BkYGBQZChlYGcAASYg5gJCBob/YD4DABfTAbQAeJxdkE1qg0AYhl8Tk9AIoVDaVSmzahcF87PMARLIMoFAl0ZHY1BHdBJIT9AT9AQ9RQ9Qeqy+yteNMzDzfM+88w0K4BY/cNAMB6N2bUaPPBLukybCLvleeAAPj8JD+hfhMV7hC3u4wxs7OO4NzQSZcI/8Ltwnfwi75E/hAR7wJTyk/xYeY49fYQ/PztM+jbTZ7LY6OWdBJdX/pqs6NYWa+zMxa13oKrA6Uoerqi/JwtpYxZXJ1coUVmeZUWVlTjq0/tHacjmdxuL90OR8O0UEDYMNdtiSEpz5XQGqzlm30kzUdAYFFOb8R7NOZk0q2lwAyz1i7oAr1xoXvrOgtYhZx8wY5KRV269JZ5yGpmzPTjQhvY9je6vEElPOuJP3mWKnP5M3V+YAAAB4nG2PyXLCMBBE3YCNDWEL2ffk7o8S8oCnkCVHC5C/jzBQlUP6IHVPzYyekl5y0iL5X5/ooY8BUmQYIkeBEca4wgRTzDDHAtdY4ga3uMM9HvCIJzzjBa94wzs+8ImvZNAq8TM+HqVkKxWlrQiOxjujQkNlEzyNzl6Z/cU2XF06at7U83VQyklLpEvSnuzsb+HAPnPfQVgaupa1Jlu4sPLsFblcitaz0dHU0ZF1qatjZ1+aTXYCmp6u0gSvWNPyHLtFZ+ZeXWVSaEkqs3T8S74WklbGbNNNq4LL4+CWKtZDv2cfX8l8aFbKFhEnJnJ+IULFpqwoQnNHlHaVQtPBl+ypmbSWdmyC61KS/AKZC3Y+AA==)
    format('woff');
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-play,
.video-js .vjs-big-play-button .vjs-icon-placeholder:before,
.video-js .vjs-play-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-play:before,
.video-js .vjs-big-play-button .vjs-icon-placeholder:before,
.video-js .vjs-play-control .vjs-icon-placeholder:before {
  content: '\\F101';
}

.vjs-icon-play-circle {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-play-circle:before {
  content: '\\F102';
}

.vjs-icon-pause,
.video-js .vjs-play-control.vjs-playing .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-pause:before,
.video-js .vjs-play-control.vjs-playing .vjs-icon-placeholder:before {
  content: '\\F103';
}

.vjs-icon-volume-mute,
.video-js .vjs-mute-control.vjs-vol-0 .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-volume-mute:before,
.video-js .vjs-mute-control.vjs-vol-0 .vjs-icon-placeholder:before {
  content: '\\F104';
}

.vjs-icon-volume-low,
.video-js .vjs-mute-control.vjs-vol-1 .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-volume-low:before,
.video-js .vjs-mute-control.vjs-vol-1 .vjs-icon-placeholder:before {
  content: '\\F105';
}

.vjs-icon-volume-mid,
.video-js .vjs-mute-control.vjs-vol-2 .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-volume-mid:before,
.video-js .vjs-mute-control.vjs-vol-2 .vjs-icon-placeholder:before {
  content: '\\F106';
}

.vjs-icon-volume-high,
.video-js .vjs-mute-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-volume-high:before,
.video-js .vjs-mute-control .vjs-icon-placeholder:before {
  content: '\\F107';
}

.vjs-icon-fullscreen-enter,
.video-js .vjs-fullscreen-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-fullscreen-enter:before,
.video-js .vjs-fullscreen-control .vjs-icon-placeholder:before {
  content: '\\F108';
}

.vjs-icon-fullscreen-exit,
.video-js.vjs-fullscreen .vjs-fullscreen-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-fullscreen-exit:before,
.video-js.vjs-fullscreen .vjs-fullscreen-control .vjs-icon-placeholder:before {
  content: '\\F109';
}

.vjs-icon-square {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-square:before {
  content: '\\F10A';
}

.vjs-icon-spinner {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-spinner:before {
  content: '\\F10B';
}

.vjs-icon-subtitles,
.video-js .vjs-subtitles-button .vjs-icon-placeholder,
.video-js .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js.video-js:lang(en-GB) .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js.video-js:lang(en-IE) .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js.video-js:lang(en-AU) .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js.video-js:lang(en-NZ) .vjs-subs-caps-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-subtitles:before,
.video-js .vjs-subtitles-button .vjs-icon-placeholder:before,
.video-js .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js.video-js:lang(en-GB) .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js.video-js:lang(en-IE) .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js.video-js:lang(en-AU) .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js.video-js:lang(en-NZ) .vjs-subs-caps-button .vjs-icon-placeholder:before {
  content: '\\F10C';
}

.vjs-icon-captions,
.video-js .vjs-captions-button .vjs-icon-placeholder,
.video-js:lang(en) .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js:lang(fr-CA) .vjs-subs-caps-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-captions:before,
.video-js .vjs-captions-button .vjs-icon-placeholder:before,
.video-js:lang(en) .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js:lang(fr-CA) .vjs-subs-caps-button .vjs-icon-placeholder:before {
  content: '\\F10D';
}

.vjs-icon-chapters,
.video-js .vjs-chapters-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-chapters:before,
.video-js .vjs-chapters-button .vjs-icon-placeholder:before {
  content: '\\F10E';
}

.vjs-icon-share {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-share:before {
  content: '\\F10F';
}

.vjs-icon-cog {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-cog:before {
  content: '\\F110';
}

.vjs-icon-circle,
.video-js .vjs-play-progress,
.video-js .vjs-volume-level,
.vjs-seek-to-live-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-circle:before,
.video-js .vjs-play-progress:before,
.video-js .vjs-volume-level:before,
.vjs-seek-to-live-control .vjs-icon-placeholder:before {
  content: '\\F111';
}

.vjs-icon-circle-outline {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-circle-outline:before {
  content: '\\F112';
}

.vjs-icon-circle-inner-circle {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-circle-inner-circle:before {
  content: '\\F113';
}

.vjs-icon-hd {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-hd:before {
  content: '\\F114';
}

.vjs-icon-cancel,
.video-js .vjs-control.vjs-close-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-cancel:before,
.video-js .vjs-control.vjs-close-button .vjs-icon-placeholder:before {
  content: '\\F115';
}

.vjs-icon-replay,
.video-js .vjs-play-control.vjs-ended .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-replay:before,
.video-js .vjs-play-control.vjs-ended .vjs-icon-placeholder:before {
  content: '\\F116';
}

.vjs-icon-facebook {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-facebook:before {
  content: '\\F117';
}

.vjs-icon-gplus {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-gplus:before {
  content: '\\F118';
}

.vjs-icon-linkedin {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-linkedin:before {
  content: '\\F119';
}

.vjs-icon-twitter {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-twitter:before {
  content: '\\F11A';
}

.vjs-icon-tumblr {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-tumblr:before {
  content: '\\F11B';
}

.vjs-icon-pinterest {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-pinterest:before {
  content: '\\F11C';
}

.vjs-icon-audio-description,
.video-js .vjs-descriptions-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-audio-description:before,
.video-js .vjs-descriptions-button .vjs-icon-placeholder:before {
  content: '\\F11D';
}

.vjs-icon-audio,
.video-js .vjs-audio-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-audio:before,
.video-js .vjs-audio-button .vjs-icon-placeholder:before {
  content: '\\F11E';
}

.vjs-icon-next-item {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-next-item:before {
  content: '\\F11F';
}

.vjs-icon-previous-item {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}
.vjs-icon-previous-item:before {
  content: '\\F120';
}

.video-js {
  display: block;
  vertical-align: top;
  box-sizing: border-box;
  color: #fff;
  position: relative;
  padding: 0;
  font-size: 10px;
  line-height: 1;
  font-weight: normal;
  font-style: normal;
  font-family: Arial, Helvetica, sans-serif;
  word-break: initial;
}
.video-js:-moz-full-screen {
  position: absolute;
}
.video-js:-webkit-full-screen {
  width: 100% !important;
  height: 100% !important;
}

.video-js[tabindex='-1'] {
  outline: none;
}

.video-js *,
.video-js *:before,
.video-js *:after {
  box-sizing: inherit;
}

.video-js ul {
  font-family: inherit;
  font-size: inherit;
  line-height: inherit;
  list-style-position: outside;
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  margin-bottom: 0;
}

.video-js.vjs-fluid,
.video-js.vjs-16-9,
.video-js.vjs-4-3 {
  width: 100%;
  max-width: 100%;
  height: auto;
}

.video-js.vjs-16-9 {
  padding-top: 56.25%;
}

.video-js.vjs-4-3 {
  padding-top: 75%;
}

.video-js.vjs-fill {
  width: 100%;
  height: 100%;
}

.video-js .vjs-tech {
  top: 0;
  left: 0;
  width: 100%;
  height: auto;
}

body.vjs-full-window {
  padding: 0;
  margin: 0;
  height: 100%;
}

.vjs-full-window .video-js.vjs-fullscreen {
  position: fixed;
  overflow: hidden;
  z-index: 1000;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
}

.video-js.vjs-fullscreen {
  width: 100% !important;
  height: 100% !important;
  padding-top: 0 !important;
}

.video-js.vjs-fullscreen.vjs-user-inactive {
  cursor: none;
}

.vjs-hidden {
  display: none !important;
}

.vjs-disabled {
  opacity: 0.5;
  cursor: default;
}

.video-js .vjs-offscreen {
  height: 1px;
  left: -9999px;
  position: absolute;
  top: 0;
  width: 1px;
}

.vjs-lock-showing {
  display: block !important;
  opacity: 1;
  visibility: visible;
}

.vjs-no-js {
  padding: 20px;
  color: #fff;
  background-color: #000;
  font-size: 18px;
  font-family: Arial, Helvetica, sans-serif;
  text-align: center;
  margin: 0px auto;
}

.vjs-no-js a,
.vjs-no-js a:visited {
  color: #66a8cc;
}

.video-js .vjs-big-play-button {
  font-size: 3em;
  line-height: 1.5em;
  height: 1.5em;
  width: 3em;
  display: block;
  position: absolute;
  top: 10px;
  left: 10px;
  padding: 0;
  cursor: pointer;
  opacity: 1;
  border: 0.06666em solid #fff;
  background-color: #2b333f;
  background-color: rgba(43, 51, 63, 0.7);
  -webkit-border-radius: 0.3em;
  -moz-border-radius: 0.3em;
  border-radius: 0.3em;
  -webkit-transition: all 0.4s;
  -moz-transition: all 0.4s;
  -ms-transition: all 0.4s;
  -o-transition: all 0.4s;
  transition: all 0.4s;
}

.vjs-big-play-centered .vjs-big-play-button {
  top: 50%;
  left: 50%;
  margin-top: -0.75em;
  margin-left: -1.5em;
}

.video-js:hover .vjs-big-play-button,
.video-js .vjs-big-play-button:focus {
  border-color: #fff;
  background-color: #73859f;
  background-color: rgba(115, 133, 159, 0.5);
  -webkit-transition: all 0s;
  -moz-transition: all 0s;
  -ms-transition: all 0s;
  -o-transition: all 0s;
  transition: all 0s;
}

.vjs-controls-disabled .vjs-big-play-button,
.vjs-has-started .vjs-big-play-button,
.vjs-using-native-controls .vjs-big-play-button,
.vjs-error .vjs-big-play-button {
  display: none;
}

.vjs-has-started.vjs-paused.vjs-show-big-play-button-on-pause .vjs-big-play-button {
  display: block;
}

.video-js button {
  background: none;
  border: none;
  color: inherit;
  display: inline-block;
  font-size: inherit;
  line-height: inherit;
  text-transform: none;
  text-decoration: none;
  transition: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}

.vjs-control .vjs-button {
  width: 100%;
  height: 100%;
}

.video-js .vjs-control.vjs-close-button {
  cursor: pointer;
  height: 3em;
  position: absolute;
  right: 0;
  top: 0.5em;
  z-index: 2;
}

.video-js .vjs-modal-dialog {
  background: rgba(0, 0, 0, 0.8);
  background: -webkit-linear-gradient(-90deg, rgba(0, 0, 0, 0.8), rgba(255, 255, 255, 0));
  background: linear-gradient(180deg, rgba(0, 0, 0, 0.8), rgba(255, 255, 255, 0));
  overflow: auto;
}

.video-js .vjs-modal-dialog > * {
  box-sizing: border-box;
}

.vjs-modal-dialog .vjs-modal-dialog-content {
  font-size: 1.2em;
  line-height: 1.5;
  padding: 20px 24px;
  z-index: 1;
}

.vjs-menu-button {
  cursor: pointer;
}

.vjs-menu-button.vjs-disabled {
  cursor: default;
}

.vjs-workinghover .vjs-menu-button.vjs-disabled:hover .vjs-menu {
  display: none;
}

.vjs-menu .vjs-menu-content {
  display: block;
  padding: 0;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
  overflow: auto;
}

.vjs-menu .vjs-menu-content > * {
  box-sizing: border-box;
}

.vjs-scrubbing .vjs-control.vjs-menu-button:hover .vjs-menu {
  display: none;
}

.vjs-menu li {
  list-style: none;
  margin: 0;
  padding: 0.2em 0;
  line-height: 1.4em;
  font-size: 1.2em;
  text-align: center;
  text-transform: lowercase;
}

.vjs-menu li.vjs-menu-item:focus,
.vjs-menu li.vjs-menu-item:hover,
.js-focus-visible .vjs-menu li.vjs-menu-item:hover {
  background-color: #73859f;
  background-color: rgba(115, 133, 159, 0.5);
}

.vjs-menu li.vjs-selected,
.vjs-menu li.vjs-selected:focus,
.vjs-menu li.vjs-selected:hover,
.js-focus-visible .vjs-menu li.vjs-selected:hover {
  background-color: #fff;
  color: #2b333f;
}

.vjs-menu li.vjs-menu-title {
  text-align: center;
  text-transform: uppercase;
  font-size: 1em;
  line-height: 2em;
  padding: 0;
  margin: 0 0 0.3em 0;
  font-weight: bold;
  cursor: default;
}

.vjs-menu-button-popup .vjs-menu {
  display: none;
  position: absolute;
  bottom: 0;
  width: 10em;
  left: -3em;
  height: 0em;
  margin-bottom: 1.5em;
  border-top-color: rgba(43, 51, 63, 0.7);
}

.vjs-menu-button-popup .vjs-menu .vjs-menu-content {
  background-color: #2b333f;
  background-color: rgba(43, 51, 63, 0.7);
  position: absolute;
  width: 100%;
  bottom: 1.5em;
  max-height: 15em;
}

.vjs-workinghover .vjs-menu-button-popup:hover .vjs-menu,
.vjs-menu-button-popup .vjs-menu.vjs-lock-showing {
  display: block;
}

.video-js .vjs-menu-button-inline {
  -webkit-transition: all 0.4s;
  -moz-transition: all 0.4s;
  -ms-transition: all 0.4s;
  -o-transition: all 0.4s;
  transition: all 0.4s;
  overflow: hidden;
}

.video-js .vjs-menu-button-inline:before {
  width: 2.222222222em;
}

.video-js .vjs-menu-button-inline:hover,
.video-js .vjs-menu-button-inline:focus,
.video-js .vjs-menu-button-inline.vjs-slider-active,
.video-js.vjs-no-flex .vjs-menu-button-inline {
  width: 12em;
}

.vjs-menu-button-inline .vjs-menu {
  opacity: 0;
  height: 100%;
  width: auto;
  position: absolute;
  left: 4em;
  top: 0;
  padding: 0;
  margin: 0;
  -webkit-transition: all 0.4s;
  -moz-transition: all 0.4s;
  -ms-transition: all 0.4s;
  -o-transition: all 0.4s;
  transition: all 0.4s;
}

.vjs-menu-button-inline:hover .vjs-menu,
.vjs-menu-button-inline:focus .vjs-menu,
.vjs-menu-button-inline.vjs-slider-active .vjs-menu {
  display: block;
  opacity: 1;
}

.vjs-no-flex .vjs-menu-button-inline .vjs-menu {
  display: block;
  opacity: 1;
  position: relative;
  width: auto;
}

.vjs-no-flex .vjs-menu-button-inline:hover .vjs-menu,
.vjs-no-flex .vjs-menu-button-inline:focus .vjs-menu,
.vjs-no-flex .vjs-menu-button-inline.vjs-slider-active .vjs-menu {
  width: auto;
}

.vjs-menu-button-inline .vjs-menu-content {
  width: auto;
  height: 100%;
  margin: 0;
  overflow: hidden;
}

.video-js .vjs-control-bar {
  display: none;
  width: 100%;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  height: 3em;
  background-color: #2b333f;
  background-color: rgba(43, 51, 63, 0.7);
}

.vjs-has-started .vjs-control-bar {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  visibility: visible;
  opacity: 1;
  -webkit-transition: visibility 0.1s, opacity 0.1s;
  -moz-transition: visibility 0.1s, opacity 0.1s;
  -ms-transition: visibility 0.1s, opacity 0.1s;
  -o-transition: visibility 0.1s, opacity 0.1s;
  transition: visibility 0.1s, opacity 0.1s;
}

.vjs-has-started.vjs-user-inactive.vjs-playing .vjs-control-bar {
  visibility: visible;
  opacity: 0;
  -webkit-transition: visibility 1s, opacity 1s;
  -moz-transition: visibility 1s, opacity 1s;
  -ms-transition: visibility 1s, opacity 1s;
  -o-transition: visibility 1s, opacity 1s;
  transition: visibility 1s, opacity 1s;
}

.vjs-controls-disabled .vjs-control-bar,
.vjs-using-native-controls .vjs-control-bar,
.vjs-error .vjs-control-bar {
  display: none !important;
}

.vjs-audio.vjs-has-started.vjs-user-inactive.vjs-playing .vjs-control-bar {
  opacity: 1;
  visibility: visible;
}

.vjs-has-started.vjs-no-flex .vjs-control-bar {
  display: table;
}

.video-js .vjs-control {
  position: relative;
  text-align: center;
  margin: 0;
  padding: 0;
  height: 100%;
  width: 4em;
  -webkit-box-flex: none;
  -moz-box-flex: none;
  -webkit-flex: none;
  -ms-flex: none;
  flex: none;
}

.vjs-button > .vjs-icon-placeholder:before {
  font-size: 1.8em;
  line-height: 1.67;
}

.video-js .vjs-control:focus:before,
.video-js .vjs-control:hover:before,
.video-js .vjs-control:focus {
  text-shadow: 0em 0em 1em white;
}

.video-js .vjs-control-text {
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
}

.vjs-no-flex .vjs-control {
  display: table-cell;
  vertical-align: middle;
}

.video-js .vjs-custom-control-spacer {
  display: none;
}

.video-js .vjs-progress-control,
.video-js .cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker,
.cld-video-player .vjs-control-bar .video-js .vjs-progress-control-events-blocker {
  cursor: pointer;
  -webkit-box-flex: auto;
  -moz-box-flex: auto;
  -webkit-flex: auto;
  -ms-flex: auto;
  flex: auto;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -webkit-align-items: center;
  -ms-flex-align: center;
  align-items: center;
  min-width: 4em;
  touch-action: none;
}

.video-js .vjs-progress-control.disabled,
.video-js .cld-video-player .vjs-control-bar .disabled.vjs-progress-control-events-blocker,
.cld-video-player .vjs-control-bar .video-js .disabled.vjs-progress-control-events-blocker {
  cursor: default;
}

.vjs-live .vjs-progress-control,
.vjs-live .cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker,
.cld-video-player .vjs-control-bar .vjs-live .vjs-progress-control-events-blocker {
  display: none;
}

.vjs-liveui .vjs-progress-control,
.vjs-liveui .cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker,
.cld-video-player .vjs-control-bar .vjs-liveui .vjs-progress-control-events-blocker {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -webkit-align-items: center;
  -ms-flex-align: center;
  align-items: center;
}

.vjs-no-flex .vjs-progress-control,
.vjs-no-flex .cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker,
.cld-video-player .vjs-control-bar .vjs-no-flex .vjs-progress-control-events-blocker {
  width: auto;
}

.video-js .vjs-progress-holder {
  -webkit-box-flex: auto;
  -moz-box-flex: auto;
  -webkit-flex: auto;
  -ms-flex: auto;
  flex: auto;
  -webkit-transition: all 0.2s;
  -moz-transition: all 0.2s;
  -ms-transition: all 0.2s;
  -o-transition: all 0.2s;
  transition: all 0.2s;
  height: 0.3em;
}

.video-js .vjs-progress-control .vjs-progress-holder,
.video-js
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-progress-holder,
.cld-video-player
  .vjs-control-bar
  .video-js
  .vjs-progress-control-events-blocker
  .vjs-progress-holder {
  margin: 0 10px;
}

.video-js .vjs-progress-control:hover .vjs-progress-holder,
.video-js
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker:hover
  .vjs-progress-holder,
.cld-video-player
  .vjs-control-bar
  .video-js
  .vjs-progress-control-events-blocker:hover
  .vjs-progress-holder {
  font-size: 1.666666666666666666em;
}

.video-js .vjs-progress-control:hover .vjs-progress-holder.disabled,
.video-js
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker:hover
  .vjs-progress-holder.disabled,
.cld-video-player
  .vjs-control-bar
  .video-js
  .vjs-progress-control-events-blocker:hover
  .vjs-progress-holder.disabled {
  font-size: 1em;
}

.video-js .vjs-progress-holder .vjs-play-progress,
.video-js .vjs-progress-holder .vjs-load-progress,
.video-js .vjs-progress-holder .vjs-load-progress div {
  position: absolute;
  display: block;
  height: 100%;
  margin: 0;
  padding: 0;
  width: 0;
}

.video-js .vjs-play-progress {
  background-color: #fff;
}
.video-js .vjs-play-progress:before {
  font-size: 0.9em;
  position: absolute;
  right: -0.5em;
  top: -0.333333333333333em;
  z-index: 1;
}

.video-js .vjs-load-progress {
  background: rgba(115, 133, 159, 0.5);
}

.video-js .vjs-load-progress div {
  background: rgba(115, 133, 159, 0.75);
}

.video-js .vjs-time-tooltip {
  background-color: #fff;
  background-color: rgba(255, 255, 255, 0.8);
  -webkit-border-radius: 0.3em;
  -moz-border-radius: 0.3em;
  border-radius: 0.3em;
  color: #000;
  float: right;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 1em;
  padding: 6px 8px 8px 8px;
  pointer-events: none;
  position: absolute;
  top: -3.4em;
  visibility: hidden;
  z-index: 1;
}

.video-js .vjs-progress-holder:focus .vjs-time-tooltip {
  display: none;
}

.video-js .vjs-progress-control:hover .vjs-time-tooltip,
.video-js
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker:hover
  .vjs-time-tooltip,
.cld-video-player
  .vjs-control-bar
  .video-js
  .vjs-progress-control-events-blocker:hover
  .vjs-time-tooltip,
.video-js .vjs-progress-control:hover .vjs-progress-holder:focus .vjs-time-tooltip,
.video-js
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker:hover
  .vjs-progress-holder:focus
  .vjs-time-tooltip,
.cld-video-player
  .vjs-control-bar
  .video-js
  .vjs-progress-control-events-blocker:hover
  .vjs-progress-holder:focus
  .vjs-time-tooltip {
  display: block;
  font-size: 0.6em;
  visibility: visible;
}

.video-js .vjs-progress-control.disabled:hover .vjs-time-tooltip,
.video-js
  .cld-video-player
  .vjs-control-bar
  .disabled.vjs-progress-control-events-blocker:hover
  .vjs-time-tooltip,
.cld-video-player
  .vjs-control-bar
  .video-js
  .disabled.vjs-progress-control-events-blocker:hover
  .vjs-time-tooltip {
  font-size: 1em;
}

.video-js .vjs-progress-control .vjs-mouse-display,
.video-js
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-mouse-display,
.cld-video-player
  .vjs-control-bar
  .video-js
  .vjs-progress-control-events-blocker
  .vjs-mouse-display {
  display: none;
  position: absolute;
  width: 1px;
  height: 100%;
  background-color: #000;
  z-index: 1;
}

.vjs-no-flex .vjs-progress-control .vjs-mouse-display,
.vjs-no-flex
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-mouse-display,
.cld-video-player
  .vjs-control-bar
  .vjs-no-flex
  .vjs-progress-control-events-blocker
  .vjs-mouse-display {
  z-index: 0;
}

.video-js .vjs-progress-control:hover .vjs-mouse-display,
.video-js
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker:hover
  .vjs-mouse-display,
.cld-video-player
  .vjs-control-bar
  .video-js
  .vjs-progress-control-events-blocker:hover
  .vjs-mouse-display {
  display: block;
}

.video-js.vjs-user-inactive .vjs-progress-control .vjs-mouse-display,
.video-js.vjs-user-inactive
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-mouse-display,
.cld-video-player
  .vjs-control-bar
  .video-js.vjs-user-inactive
  .vjs-progress-control-events-blocker
  .vjs-mouse-display {
  visibility: hidden;
  opacity: 0;
  -webkit-transition: visibility 1s, opacity 1s;
  -moz-transition: visibility 1s, opacity 1s;
  -ms-transition: visibility 1s, opacity 1s;
  -o-transition: visibility 1s, opacity 1s;
  transition: visibility 1s, opacity 1s;
}

.video-js.vjs-user-inactive.vjs-no-flex .vjs-progress-control .vjs-mouse-display,
.video-js.vjs-user-inactive.vjs-no-flex
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-mouse-display,
.cld-video-player
  .vjs-control-bar
  .video-js.vjs-user-inactive.vjs-no-flex
  .vjs-progress-control-events-blocker
  .vjs-mouse-display {
  display: none;
}

.vjs-mouse-display .vjs-time-tooltip {
  color: #fff;
  background-color: #000;
  background-color: rgba(0, 0, 0, 0.8);
}

.video-js .vjs-slider {
  position: relative;
  cursor: pointer;
  padding: 0;
  margin: 0 0.45em 0 0.45em;
  /* iOS Safari */
  -webkit-touch-callout: none;
  /* Safari */
  -webkit-user-select: none;
  /* Konqueror HTML */
  -khtml-user-select: none;
  /* Firefox */
  -moz-user-select: none;
  /* Internet Explorer/Edge */
  -ms-user-select: none;
  /* Non-prefixed version, currently supported by Chrome and Opera */
  user-select: none;
  background-color: #73859f;
  background-color: rgba(115, 133, 159, 0.5);
}

.video-js .vjs-slider.disabled {
  cursor: default;
}

.video-js .vjs-slider:focus {
  text-shadow: 0em 0em 1em white;
  -webkit-box-shadow: 0 0 1em #fff;
  -moz-box-shadow: 0 0 1em #fff;
  box-shadow: 0 0 1em #fff;
}

.video-js .vjs-mute-control {
  cursor: pointer;
  -webkit-box-flex: none;
  -moz-box-flex: none;
  -webkit-flex: none;
  -ms-flex: none;
  flex: none;
}

.video-js .vjs-volume-control {
  cursor: pointer;
  margin-right: 1em;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
}

.video-js .vjs-volume-control.vjs-volume-horizontal {
  width: 5em;
}

.video-js .vjs-volume-panel .vjs-volume-control {
  visibility: visible;
  opacity: 0;
  width: 1px;
  height: 1px;
  margin-left: -1px;
}

.video-js .vjs-volume-panel {
  -webkit-transition: width 1s;
  -moz-transition: width 1s;
  -ms-transition: width 1s;
  -o-transition: width 1s;
  transition: width 1s;
}
.video-js .vjs-volume-panel:hover .vjs-volume-control,
.video-js .vjs-volume-panel:active .vjs-volume-control,
.video-js .vjs-volume-panel:focus .vjs-volume-control,
.video-js .vjs-volume-panel .vjs-volume-control:hover,
.video-js .vjs-volume-panel .vjs-volume-control:active,
.video-js .vjs-volume-panel .vjs-mute-control:hover ~ .vjs-volume-control,
.video-js .vjs-volume-panel .vjs-volume-control.vjs-slider-active {
  visibility: visible;
  opacity: 1;
  position: relative;
  -webkit-transition: visibility 0.1s, opacity 0.1s, height 0.1s, width 0.1s, left 0s, top 0s;
  -moz-transition: visibility 0.1s, opacity 0.1s, height 0.1s, width 0.1s, left 0s, top 0s;
  -ms-transition: visibility 0.1s, opacity 0.1s, height 0.1s, width 0.1s, left 0s, top 0s;
  -o-transition: visibility 0.1s, opacity 0.1s, height 0.1s, width 0.1s, left 0s, top 0s;
  transition: visibility 0.1s, opacity 0.1s, height 0.1s, width 0.1s, left 0s, top 0s;
}
.video-js .vjs-volume-panel:hover .vjs-volume-control.vjs-volume-horizontal,
.video-js .vjs-volume-panel:active .vjs-volume-control.vjs-volume-horizontal,
.video-js .vjs-volume-panel:focus .vjs-volume-control.vjs-volume-horizontal,
.video-js .vjs-volume-panel .vjs-volume-control:hover.vjs-volume-horizontal,
.video-js .vjs-volume-panel .vjs-volume-control:active.vjs-volume-horizontal,
.video-js .vjs-volume-panel .vjs-mute-control:hover ~ .vjs-volume-control.vjs-volume-horizontal,
.video-js .vjs-volume-panel .vjs-volume-control.vjs-slider-active.vjs-volume-horizontal {
  width: 5em;
  height: 3em;
}
.video-js .vjs-volume-panel:hover .vjs-volume-control.vjs-volume-vertical,
.video-js .vjs-volume-panel:active .vjs-volume-control.vjs-volume-vertical,
.video-js .vjs-volume-panel:focus .vjs-volume-control.vjs-volume-vertical,
.video-js .vjs-volume-panel .vjs-volume-control:hover.vjs-volume-vertical,
.video-js .vjs-volume-panel .vjs-volume-control:active.vjs-volume-vertical,
.video-js .vjs-volume-panel .vjs-mute-control:hover ~ .vjs-volume-control.vjs-volume-vertical,
.video-js .vjs-volume-panel .vjs-volume-control.vjs-slider-active.vjs-volume-vertical {
  left: -3.5em;
}
.video-js .vjs-volume-panel.vjs-volume-panel-horizontal:hover,
.video-js .vjs-volume-panel.vjs-volume-panel-horizontal:active,
.video-js .vjs-volume-panel.vjs-volume-panel-horizontal.vjs-slider-active {
  width: 9em;
  -webkit-transition: width 0.1s;
  -moz-transition: width 0.1s;
  -ms-transition: width 0.1s;
  -o-transition: width 0.1s;
  transition: width 0.1s;
}
.video-js .vjs-volume-panel.vjs-volume-panel-horizontal.vjs-mute-toggle-only {
  width: 4em;
}

.video-js .vjs-volume-panel .vjs-volume-control.vjs-volume-vertical {
  height: 8em;
  width: 3em;
  left: -3000em;
  -webkit-transition: visibility 1s, opacity 1s, height 1s 1s, width 1s 1s, left 1s 1s, top 1s 1s;
  -moz-transition: visibility 1s, opacity 1s, height 1s 1s, width 1s 1s, left 1s 1s, top 1s 1s;
  -ms-transition: visibility 1s, opacity 1s, height 1s 1s, width 1s 1s, left 1s 1s, top 1s 1s;
  -o-transition: visibility 1s, opacity 1s, height 1s 1s, width 1s 1s, left 1s 1s, top 1s 1s;
  transition: visibility 1s, opacity 1s, height 1s 1s, width 1s 1s, left 1s 1s, top 1s 1s;
}

.video-js .vjs-volume-panel .vjs-volume-control.vjs-volume-horizontal {
  -webkit-transition: visibility 1s, opacity 1s, height 1s 1s, width 1s, left 1s 1s, top 1s 1s;
  -moz-transition: visibility 1s, opacity 1s, height 1s 1s, width 1s, left 1s 1s, top 1s 1s;
  -ms-transition: visibility 1s, opacity 1s, height 1s 1s, width 1s, left 1s 1s, top 1s 1s;
  -o-transition: visibility 1s, opacity 1s, height 1s 1s, width 1s, left 1s 1s, top 1s 1s;
  transition: visibility 1s, opacity 1s, height 1s 1s, width 1s, left 1s 1s, top 1s 1s;
}

.video-js.vjs-no-flex .vjs-volume-panel .vjs-volume-control.vjs-volume-horizontal {
  width: 5em;
  height: 3em;
  visibility: visible;
  opacity: 1;
  position: relative;
  -webkit-transition: none;
  -moz-transition: none;
  -ms-transition: none;
  -o-transition: none;
  transition: none;
}

.video-js.vjs-no-flex .vjs-volume-control.vjs-volume-vertical,
.video-js.vjs-no-flex .vjs-volume-panel .vjs-volume-control.vjs-volume-vertical {
  position: absolute;
  bottom: 3em;
  left: 0.5em;
}

.video-js .vjs-volume-panel {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
}

.video-js .vjs-volume-bar {
  margin: 1.35em 0.45em;
}

.vjs-volume-bar.vjs-slider-horizontal {
  width: 5em;
  height: 0.3em;
}

.vjs-volume-bar.vjs-slider-vertical {
  width: 0.3em;
  height: 5em;
  margin: 1.35em auto;
}

.video-js .vjs-volume-level {
  position: absolute;
  bottom: 0;
  left: 0;
  background-color: #fff;
}
.video-js .vjs-volume-level:before {
  position: absolute;
  font-size: 0.9em;
}

.vjs-slider-vertical .vjs-volume-level {
  width: 0.3em;
}
.vjs-slider-vertical .vjs-volume-level:before {
  top: -0.5em;
  left: -0.3em;
}

.vjs-slider-horizontal .vjs-volume-level {
  height: 0.3em;
}
.vjs-slider-horizontal .vjs-volume-level:before {
  top: -0.3em;
  right: -0.5em;
}

.video-js .vjs-volume-panel.vjs-volume-panel-vertical {
  width: 4em;
}

.vjs-volume-bar.vjs-slider-vertical .vjs-volume-level {
  height: 100%;
}

.vjs-volume-bar.vjs-slider-horizontal .vjs-volume-level {
  width: 100%;
}

.video-js .vjs-volume-vertical {
  width: 3em;
  height: 8em;
  bottom: 8em;
  background-color: #2b333f;
  background-color: rgba(43, 51, 63, 0.7);
}

.video-js .vjs-volume-horizontal .vjs-menu {
  left: -2em;
}

.vjs-poster {
  display: inline-block;
  vertical-align: middle;
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: contain;
  background-color: #000000;
  cursor: pointer;
  margin: 0;
  padding: 0;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height: 100%;
}

.vjs-has-started .vjs-poster {
  display: none;
}

.vjs-audio.vjs-has-started .vjs-poster {
  display: block;
}

.vjs-using-native-controls .vjs-poster {
  display: none;
}

.video-js .vjs-live-control {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: flex-start;
  -webkit-align-items: flex-start;
  -ms-flex-align: flex-start;
  align-items: flex-start;
  -webkit-box-flex: auto;
  -moz-box-flex: auto;
  -webkit-flex: auto;
  -ms-flex: auto;
  flex: auto;
  font-size: 1em;
  line-height: 3em;
}

.vjs-no-flex .vjs-live-control {
  display: table-cell;
  width: auto;
  text-align: left;
}

.video-js:not(.vjs-live) .vjs-live-control,
.video-js.vjs-liveui .vjs-live-control {
  display: none;
}

.video-js .vjs-seek-to-live-control {
  cursor: pointer;
  -webkit-box-flex: none;
  -moz-box-flex: none;
  -webkit-flex: none;
  -ms-flex: none;
  flex: none;
  display: inline-flex;
  height: 100%;
  padding-left: 0.5em;
  padding-right: 0.5em;
  font-size: 1em;
  line-height: 3em;
  width: auto;
  min-width: 4em;
}

.vjs-no-flex .vjs-seek-to-live-control {
  display: table-cell;
  width: auto;
  text-align: left;
}

.video-js.vjs-live:not(.vjs-liveui) .vjs-seek-to-live-control,
.video-js:not(.vjs-live) .vjs-seek-to-live-control {
  display: none;
}

.vjs-seek-to-live-control.vjs-control.vjs-at-live-edge {
  cursor: auto;
}

.vjs-seek-to-live-control .vjs-icon-placeholder {
  margin-right: 0.5em;
  color: #888;
}

.vjs-seek-to-live-control.vjs-control.vjs-at-live-edge .vjs-icon-placeholder {
  color: red;
}

.video-js .vjs-time-control {
  -webkit-box-flex: none;
  -moz-box-flex: none;
  -webkit-flex: none;
  -ms-flex: none;
  flex: none;
  font-size: 1em;
  line-height: 3em;
  min-width: 2em;
  width: auto;
  padding-left: 1em;
  padding-right: 1em;
}

.vjs-live .vjs-time-control {
  display: none;
}

.video-js .vjs-current-time,
.vjs-no-flex .vjs-current-time {
  display: none;
}

.video-js .vjs-duration,
.vjs-no-flex .vjs-duration {
  display: none;
}

.vjs-time-divider {
  display: none;
  line-height: 3em;
}

.vjs-live .vjs-time-divider {
  display: none;
}

.video-js .vjs-play-control {
  cursor: pointer;
}

.video-js .vjs-play-control .vjs-icon-placeholder {
  -webkit-box-flex: none;
  -moz-box-flex: none;
  -webkit-flex: none;
  -ms-flex: none;
  flex: none;
}

.vjs-text-track-display {
  position: absolute;
  bottom: 3em;
  left: 0;
  right: 0;
  top: 0;
  pointer-events: none;
}

.video-js.vjs-user-inactive.vjs-playing .vjs-text-track-display {
  bottom: 1em;
}

.video-js .vjs-text-track {
  font-size: 1.4em;
  text-align: center;
  margin-bottom: 0.1em;
}

.vjs-subtitles {
  color: #fff;
}

.vjs-captions {
  color: #fc6;
}

.vjs-tt-cue {
  display: block;
}

video::-webkit-media-text-track-display {
  -moz-transform: translateY(-3em);
  -ms-transform: translateY(-3em);
  -o-transform: translateY(-3em);
  -webkit-transform: translateY(-3em);
  transform: translateY(-3em);
}

.video-js.vjs-user-inactive.vjs-playing video::-webkit-media-text-track-display {
  -moz-transform: translateY(-1.5em);
  -ms-transform: translateY(-1.5em);
  -o-transform: translateY(-1.5em);
  -webkit-transform: translateY(-1.5em);
  transform: translateY(-1.5em);
}

.video-js .vjs-fullscreen-control {
  cursor: pointer;
  -webkit-box-flex: none;
  -moz-box-flex: none;
  -webkit-flex: none;
  -ms-flex: none;
  flex: none;
}

.vjs-playback-rate > .vjs-menu-button,
.vjs-playback-rate .vjs-playback-rate-value {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}

.vjs-playback-rate .vjs-playback-rate-value {
  pointer-events: none;
  font-size: 1.5em;
  line-height: 2;
  text-align: center;
}

.vjs-playback-rate .vjs-menu {
  width: 4em;
  left: 0em;
}

.vjs-error .vjs-error-display .vjs-modal-dialog-content {
  font-size: 1.4em;
  text-align: center;
}

.vjs-error .vjs-error-display:before {
  color: #fff;
  content: 'X';
  font-family: Arial, Helvetica, sans-serif;
  font-size: 4em;
  left: 0;
  line-height: 1;
  margin-top: -0.5em;
  position: absolute;
  text-shadow: 0.05em 0.05em 0.1em #000;
  text-align: center;
  top: 50%;
  vertical-align: middle;
  width: 100%;
}

.vjs-loading-spinner {
  display: none;
  position: absolute;
  top: 50%;
  left: 50%;
  margin: -25px 0 0 -25px;
  opacity: 0.85;
  text-align: left;
  border: 6px solid rgba(43, 51, 63, 0.7);
  box-sizing: border-box;
  background-clip: padding-box;
  width: 50px;
  height: 50px;
  border-radius: 25px;
  visibility: hidden;
}

.vjs-seeking .vjs-loading-spinner,
.vjs-waiting .vjs-loading-spinner {
  display: block;
  animation: 0s linear 0.3s forwards vjs-spinner-show;
}

.vjs-loading-spinner:before,
.vjs-loading-spinner:after {
  content: '';
  position: absolute;
  margin: -6px;
  box-sizing: inherit;
  width: inherit;
  height: inherit;
  border-radius: inherit;
  opacity: 1;
  border: inherit;
  border-color: transparent;
  border-top-color: white;
}

.vjs-seeking .vjs-loading-spinner:before,
.vjs-seeking .vjs-loading-spinner:after,
.vjs-waiting .vjs-loading-spinner:before,
.vjs-waiting .vjs-loading-spinner:after {
  -webkit-animation: vjs-spinner-spin 1.1s cubic-bezier(0.6, 0.2, 0, 0.8) infinite,
    vjs-spinner-fade 1.1s linear infinite;
  animation: vjs-spinner-spin 1.1s cubic-bezier(0.6, 0.2, 0, 0.8) infinite,
    vjs-spinner-fade 1.1s linear infinite;
}

.vjs-seeking .vjs-loading-spinner:before,
.vjs-waiting .vjs-loading-spinner:before {
  border-top-color: white;
}

.vjs-seeking .vjs-loading-spinner:after,
.vjs-waiting .vjs-loading-spinner:after {
  border-top-color: white;
  -webkit-animation-delay: 0.44s;
  animation-delay: 0.44s;
}

@keyframes vjs-spinner-show {
  to {
    visibility: visible;
  }
}

@-webkit-keyframes vjs-spinner-show {
  to {
    visibility: visible;
  }
}

@keyframes vjs-spinner-spin {
  100% {
    transform: rotate(360deg);
  }
}

@-webkit-keyframes vjs-spinner-spin {
  100% {
    -webkit-transform: rotate(360deg);
  }
}

@keyframes vjs-spinner-fade {
  0% {
    border-top-color: #73859f;
  }
  20% {
    border-top-color: #73859f;
  }
  35% {
    border-top-color: white;
  }
  60% {
    border-top-color: #73859f;
  }
  100% {
    border-top-color: #73859f;
  }
}

@-webkit-keyframes vjs-spinner-fade {
  0% {
    border-top-color: #73859f;
  }
  20% {
    border-top-color: #73859f;
  }
  35% {
    border-top-color: white;
  }
  60% {
    border-top-color: #73859f;
  }
  100% {
    border-top-color: #73859f;
  }
}

.vjs-chapters-button .vjs-menu ul {
  width: 24em;
}

.video-js
  .vjs-subs-caps-button
  + .vjs-menu
  .vjs-captions-menu-item
  .vjs-menu-item-text
  .vjs-icon-placeholder {
  vertical-align: middle;
  display: inline-block;
  margin-bottom: -0.1em;
}

.video-js
  .vjs-subs-caps-button
  + .vjs-menu
  .vjs-captions-menu-item
  .vjs-menu-item-text
  .vjs-icon-placeholder:before {
  font-family: VideoJS;
  content: '\\F10D';
  font-size: 1.5em;
  line-height: inherit;
}

.video-js
  .vjs-audio-button
  + .vjs-menu
  .vjs-main-desc-menu-item
  .vjs-menu-item-text
  .vjs-icon-placeholder {
  vertical-align: middle;
  display: inline-block;
  margin-bottom: -0.1em;
}

.video-js
  .vjs-audio-button
  + .vjs-menu
  .vjs-main-desc-menu-item
  .vjs-menu-item-text
  .vjs-icon-placeholder:before {
  font-family: VideoJS;
  content: ' \F11D';
  font-size: 1.5em;
  line-height: inherit;
}

.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-custom-control-spacer {
  -webkit-box-flex: auto;
  -moz-box-flex: auto;
  -webkit-flex: auto;
  -ms-flex: auto;
  flex: auto;
  display: block;
}

.video-js.vjs-layout-tiny:not(.vjs-fullscreen).vjs-no-flex .vjs-custom-control-spacer {
  width: auto;
}

.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-current-time,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-time-divider,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-duration,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-remaining-time,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-playback-rate,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-progress-control,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen)
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker,
.cld-video-player
  .vjs-control-bar
  .video-js.vjs-layout-tiny:not(.vjs-fullscreen)
  .vjs-progress-control-events-blocker,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-mute-control,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-volume-control,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-volume-panel,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-chapters-button,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-descriptions-button,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-captions-button,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-subtitles-button,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-subs-caps-button,
.video-js.vjs-layout-tiny:not(.vjs-fullscreen) .vjs-audio-button {
  display: none;
}

.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-current-time,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-time-divider,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-duration,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-remaining-time,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-playback-rate,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-mute-control,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-volume-control,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-volume-panel,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-chapters-button,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-descriptions-button,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-captions-button,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-subtitles-button,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-subs-caps-button,
.video-js.vjs-layout-x-small:not(.vjs-fullscreen) .vjs-audio-button {
  display: none;
}

.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-current-time,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-time-divider,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-duration,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-remaining-time,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-playback-rate,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-mute-control,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-volume-control,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-volume-panel,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-chapters-button,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-descriptions-button,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-captions-button,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-subtitles-button,
.video-js.vjs-layout-small:not(.vjs-fullscreen) .vjs-audio-button {
  display: none;
}

.vjs-modal-dialog.vjs-text-track-settings {
  background-color: #2b333f;
  background-color: rgba(43, 51, 63, 0.75);
  color: #fff;
  height: 70%;
}

.vjs-text-track-settings .vjs-modal-dialog-content {
  display: table;
}

.vjs-text-track-settings .vjs-track-settings-colors,
.vjs-text-track-settings .vjs-track-settings-font,
.vjs-text-track-settings .vjs-track-settings-controls {
  display: table-cell;
}

.vjs-text-track-settings .vjs-track-settings-controls {
  text-align: right;
  vertical-align: bottom;
}

@supports (display: grid) {
  .vjs-text-track-settings .vjs-modal-dialog-content {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr;
    padding: 20px 24px 0px 24px;
  }
  .vjs-track-settings-controls .vjs-default-button {
    margin-bottom: 20px;
  }
  .vjs-text-track-settings .vjs-track-settings-controls {
    grid-column: 1 / -1;
  }
  .vjs-layout-small .vjs-text-track-settings .vjs-modal-dialog-content,
  .vjs-layout-x-small .vjs-text-track-settings .vjs-modal-dialog-content,
  .vjs-layout-tiny .vjs-text-track-settings .vjs-modal-dialog-content {
    grid-template-columns: 1fr;
  }
}

.vjs-track-setting > select {
  margin-right: 1em;
  margin-bottom: 0.5em;
}

.vjs-text-track-settings fieldset {
  margin: 5px;
  padding: 3px;
  border: none;
}

.vjs-text-track-settings fieldset span {
  display: inline-block;
}

.vjs-text-track-settings fieldset span > select {
  max-width: 7.3em;
}

.vjs-text-track-settings legend {
  color: #fff;
  margin: 0 0 5px 0;
}

.vjs-text-track-settings .vjs-label {
  position: absolute;
  clip: rect(1px 1px 1px 1px);
  clip: rect(1px, 1px, 1px, 1px);
  display: block;
  margin: 0 0 5px 0;
  padding: 0;
  border: 0;
  height: 1px;
  width: 1px;
  overflow: hidden;
}

.vjs-track-settings-controls button:focus,
.vjs-track-settings-controls button:active {
  outline-style: solid;
  outline-width: medium;
  background-image: linear-gradient(0deg, #fff 88%, #73859f 100%);
}

.vjs-track-settings-controls button:hover {
  color: rgba(43, 51, 63, 0.75);
}

.vjs-track-settings-controls button {
  background-color: #fff;
  background-image: linear-gradient(-180deg, #fff 88%, #73859f 100%);
  color: #2b333f;
  cursor: pointer;
  border-radius: 2px;
}

.vjs-track-settings-controls .vjs-default-button {
  margin-right: 1em;
}

@media print {
  .video-js > *:not(.vjs-tech):not(.vjs-poster) {
    visibility: hidden;
  }
}

.vjs-resize-manager {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: none;
  z-index: -1000;
}

.js-focus-visible .video-js *:focus:not(.focus-visible) {
  outline: none;
  background: none;
}

.video-js *:focus:not(:focus-visible),
.video-js .vjs-menu *:focus:not(:focus-visible) {
  outline: none;
  background: none;
}

.cld-video-player .vjs-time-tooltip,
.cld-video-player .vjs-mouse-display:after,
.cld-video-player .vjs-play-progress:after {
  font-weight: 300;
  padding: 0.4em 0.6em;
  top: -2.6em;
}

.cld-video-player .vjs-recommendations-overlay .vjs-recommendations-overlay-item:active:after,
.cld-video-player .vjs-recommendations-overlay .vjs-recommendations-overlay-item:hover:after {
  content: '';
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  background-color: rgba(255, 255, 255, 0.2);
}

.cld-video-player .vjs-context-menu-ui .vjs-menu-content .vjs-menu-item {
  text-align: left;
}

.cld-video-player.vjs-ad-playing .vjs-play-control.vjs-paused,
.cld-video-player.vjs-ad-playing .vjs-volume-level {
  background-color: transparent !important;
}

.cld-video-player.vjs-ad-playing .vjs-play-progress:before {
  display: none;
}

.cld-video-player.cld-video-player-skin-light .video-js button:hover,
.cld-video-player.cld-video-player-skin-light .vjs-volume-menu-button:hover {
  opacity: 0.9;
}

.cld-video-player.cld-video-player-skin-light
  .vjs-title-bar
  .vjs-title-bar-title:not(:empty)
  + .vjs-title-bar-subtitle:not(:empty):before {
  content: '\\2022';
  padding: 0 0.45em;
  font-size: 1.4em;
  font-weight: 500;
}

.cld-video-player.cld-video-player-skin-light .vjs-title-bar {
  flex-direction: row;
  justify-content: left;
  height: 3.6em;
}
.cld-video-player.cld-video-player-skin-light .vjs-title-bar > div {
  width: auto;
  padding: 0;
  margin: 0;
}

@font-face {
  font-family: VideoJS;
  src: url(data:application/font-woff;charset=utf-8;base64,d09GRgABAAAAABHEAAsAAAAAHiwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADsAAABUIIslek9TLzIAAAFEAAAAPgAAAFZRiV3YY21hcAAAAYQAAADeAAADTPOs5ipnbHlmAAACZAAADE4AABRQ4vNSwmhlYWQAAA60AAAAKgAAADYXvpunaGhlYQAADuAAAAAbAAAAJA4DByRobXR4AAAO/AAAAA8AAACQ9QAAAGxvY2EAAA8MAAAASgAAAEpQBErwbWF4cAAAD1gAAAAfAAAAIAE1AJBuYW1lAAAPeAAAASUAAAIK1cf1oHBvc3QAABCgAAABJAAAAcOMC/2oeJxjYGRgYOBiMGCwY2BycfMJYeDLSSzJY5BiYGGAAJA8MpsxJzM9kYEDxgPKsYBpDiBmg4gCACY7BUgAeJxjYGQ7xziBgZWBgaWQ5RkDA8MvCM0cwxDOeI6BgYmBlZkBKwhIc01hcPjI+FGZHcRdyA4RZgQRAC+MCwgAAHic7dHZbcQgAEXRy5jxbJ593xd+U1cKylcKSWNUMOHxUkYsHa6MMLIA6ANN8VFECN8E9HyV2VDnG8Z1PvJZ10TN5/R+lzFoLO+xjr2yNpYdWwYMGZXvJnRMmTFnwZIVazZs2bHnwJETZy5cuXHnwZMXqWzT8v90GsLP31vSSVs9+56hNqY7zNF0G7lvut/cGurAUIeGOjLUsene88RQO9Pf5amhzgx1bqgLQ10a6spQ14a6MdStoe4MdW+oB0M9GurJUM+GejHUq6HeDPVuqA9DfRrqy1CTkX4BD9lVXgAAeJy1WAtQlNcV/s/9XyyLC8u+VHDZB7CwwAL7VBYWIiIGfCAGjRpFDAiIGTVWA1rNjqZkBEeT6G6M0dik0TbazKQvk9ExQzPTNE0mqE0gtRmnmzaZabWxJjE2CeyfnvvvsmKUzNhpWfb/7z333HvP/b5zzz13GWDwj+/n+xnCMKAG0sR6+f6Ry0TJMCxtE7qELmzLYNwMoyAai6gCUUgzGMGgN/kC4POm2Rxgy2Ut8ssti5xys1ZWJVdgQKoKHezvHZzh988Y7O2Hkpvlgz3ru44XFRcXHe9a//nNIimBBvw8K+tI52/r+83Ad3vEigxdBa6nCtfDMiKuyKzOB7VZDQNQIA2RJiiAzZx+5DKnRzVOXp9ZMKGmgbExdtR323JtOoPXpxUNXo+brklH1wIijmRGfARR5/UZfCAauPNuZ9Nr7Jn8jfNqqq+G1p0oLh35dTO0SEfpJBBoWPrM1meao6sOH2aDLY0N9mOvTZ784OqNT714ZM/jZy2HTPvbOklhs3RUqpL64Yb0RkXGIWvrwWYufPgwmiXzwp3khxme8kKnJkqwwxapj1tFu3AnpH5mbL1rUC+JUcXtV3t93lgXvs3tvO8+p3s0jdok9ccsiR63NzTCPYlRYjybhFScS8VYGEYT6zw2FqtL0KkbYzwSG5D8DkFocrrP0OWXFp9YF7paXYNw3LQxkpiRXJMX/ffOtv2mQ5azj++BMkRijAfZB3lGyaiRCwasarPHyro8Lo8ZrDqz2srqXDqXmijDsCUSDkekPjYYgS3hSCRMrkh9YZIcCY8GsZH8OIzyO45JBzBr5NFAHlnjodPA5khY6ovQ0WCAjhymwzRhHauwZeRyBOVUzCTwHka8NfKYZosKdGqtEVxmZwA8arcDWPQUvmo0mD8rL29WPht/jwbZID8kF8Y1jFxhg4yQ2GdJzCRGxxgZK1OIuw1yrRZRECEHvRB4MyAVHjePLijotAYzj6SAC/Qup8/rA/I6tDY2PLZty4OQzu51fXpUuko+iZ6HrStcntF06RK+UklJ9GNsgfRu6dKiBVRXuspeIx2aooKFku2Rn3xTLtnqo5vWPPrYvvp6+Ep+19WjGC5KSVQJLlJvpLbG15/BZKGtd0QBzDozmD1mHr/0ww+NLM6vycuryedOyO+RIrBLw+RyVE8uY4gYBjst3Q5R9GRIGgqFQuEQnTruq/L8BUz1BHNbPVYdMqzTCgVgyUWZ1w9OvY6fSI4BL4/alMfp5Xd054ZQaENBViSrsBAfBURJ63JRFt7Byhs5kZw6qg52+qy7tRrn2MTdkOOSkkmN+Y/GhS4Y/1K01PSrM3siIfkPCjjdyJXYl/WEMY7CQEiqogXSFAqNxQmMYeuYUqxY0Fu8bhsgGIgFIkGx0RrAAVYsUYyc6CvoPkaItfNV9npj9J+iSFKNCwoq2svL27e2l4vRz8ZqFQULjEQr+Fu3tPr9rWSBMRN8Gc4McE8zyaqoMSVDejczVjNlSm9PmexvLSuj+on9IixF365mGtBCtA6PCBvrgFTILaaGmS1CKvq0LVcQWUGfBYLWgJHGoDFCJWBV5tVbic5v0Ht9Gm+ujbc4wB0ApxG0KuBP8vxzyYIYFqemnC6fmcnqVNKwSkcyZ5WdnjRVDIm88gjPH1HyWJw66bS/OpPoVGC/VSM6vbZ79uzuvfTB+uwB+zZFRgqwKQq+3pHxpio9XfVmZlE9r0iRoikZim1xBazcpoCdMhT3xIfCR5wfurcnI9va285Nk3xIhm47GOOHHhuc4OC8iW1sbJn9uz2bJxx9gmNZ6LrrA5m9xcbAf2OlBmlOUI4m9/Weo5Of6+2DkpvlCUxuj3spPqj1qCidv22AiawnTWM7orw9kS+YBDPu3Swmnyn6nuiDOxojjR/MGF7UWsFq9uEOv2Oc2YD/4ViUkIYxzAgmbLpFbzSIkWDgzBmqkFW4IfJd7rO/B1WNqACbAnwKPCwmpPtSp3Rdut4JSunLTkiBlE7py4mpHlXcoghK7ByPQ/ywcArzKMxXFKB2GvSUQTytLJhH5HqsMed3ysa5Y5QHMfXZvLVjjb+iwr+m4zothIqLjnWtv76+6xilAot8P54T/ahopzpfdLaVV1SUt3X2jOP7C9R2FCdyvyr0OoGe92or2oERMwQ82UaPZJqK4oHLMON0CdVVELPahapkW/RH5AqiTT/0yGbGYqycS2BSTNFETA2IKfD9a6VmqXktPC8/4Hms8mZ8yKWbDYkcJ4aPg1nM7LwLlKwe0eNz8bKWrFlMQydqV4Abz37548yiwRLH0tncsWYLzR5UrA67TQNtrNlJs4UA68Funhj4PWN4fk4Loe/ug3eq6nYmQYUoqJX6pBTRYhA0Ki6NFRMCvRgTlLMsYYmoSGFTOaLkOUI4lhBWSFZyqSwKeCDjiKTzIdmBADJKiUT2ih3HYjM7ikff/kLvLnndaJySpkzlAcnjdbxhktE4/XaRmKFUKgibxHGQxKt4NjlNMft20Xj885hiZgmzC/EfDzUCnyBBBtVF2YntKvj/EAAF49AOJTiQnZvm1jEHh28r63eJlIK0lATibBonimJagpNUFimoHI94CscDy8U4QUGcE9YzDuyenjEK5HkxA9uM0w4XO9Ao6af/ewri+6gSr2Va6v1WC+JiZjEFV9P8x+11OfnKI4vLIccmSYPffnv+5f3CdemrOXPOSn+KKsi/Ie/Pp99lEnF4L45TwtQwq5h1lEujDDRntdBd5EZCMGtGmAUU0GoAA7RoNWI2pGJtTjmTdhC0gObSRlbjDBAUACWOIG2UNSPBCwMGco8VrwouNqtq+bKqkjnS5aOPfTIjZ2ndLlCbsnQO/YF35lTOe28DvNC3q25uac3s0qdyG6Z6LJVrq+wLpxRrCyqWesu6lvtUqRpjcmbO/DSTp2P9Iu/sjdvZrqGhzvff72Q/zq6pbgjOX7Zo66ZFq8prF/Y5GtPtVfnZpdxvDzQe8s6sur9yz7I5m2r9VQFzxow1Zb3bG21mf5ujvb3RPfr0VPfKwJSS7MlWpWHadKLKnr7CP7vLP41dt+7DD9ddjJ+/QcwPWbzb2eW7pBr0cv7r9jr1tOrOjQGhNqenFWPebg2wLmy5JJ2CZmi/jzS2th9v5fZJry5cXNmkU0qvEgC4l2jtte2VJ89z+0bN7F/Aee/q1ftbWkYvRd8i6Z2P1LiMrugHsA+ulZQ8YSqZnvW3sVz1AIZbmq0Iol6nJSrgrCYb3RUY6mKvCtxFMnd6zpAruwXexwXRKxyoboa5zw5Lf/y59NlH1oKPXuk8nmXOLLBv2D9r/sz5hT2w/M2ks717Vj20KqdzBd+xukaVuVOKXn31oSe5PrK9hVcafrGZy2UL9y66vz70q+Tc7N6za3QzNlUmJ3wziD6F918w0G3vYD1qqyfAoe/4ESa8NqJP+Lx8SYPhlaUdL2+Yb/7ZnIdnlWh5ELnrME/65SRTdcl8OPepNQBk+gPd3WXE9MHUJSvWLyniRen+0eg3RrfHCCSOQ1AIMnXUcwk6rQ0z3FjSTt0w5oAYS2ja67XFQEC0qDP7qKdSwjCvZwVDLtbSs01cuhAclI5eWN/l13i1ayt6Xu945K97O089urygYZ4xKUkggtp14aWnX9rdVVGnSsoxeJ2BxVMeVHOD0iC0wAvQMsgGL0hHB00rat+zzdh9LfzwWzvKVm3vre54zpRiyposGLQVy56++OKuE1eXVFg3L81yzny46d5SaeXsruXww39cwAFwfob+5oIP4M7h7cbMTGcWMGuYjcwPqN+5vPSsox85qoqCjUZJH2b1XgeIai2v98l5vtfjtVkFQyyaGrSGMbFvnFinn0DMuyoBr/lWQQmgs7GZmR2Oue6KwnkFxcli06zaRZ4Ojx3IycYuhViyugp2TE1XpiVn4xVpxzRgicDaDBmQHRfqAXYYgSM8R6XWKZo7SL+uvnrPRyfenfTZv4h+L/yBRAleAVKzfA+09Sw9WFyaxAEQlnuDXTx3Zqubl0LiG6JGadJW1S5Y9PuXk5J4hVA794HlA6fvQjrQCm1sik7aJfVC9Gu4eQc8RX81iGeCck5K3dYzPqHGeKt2xfLUeAK2e3v3aZcHb3ERaSg/78mVLe+3rHwyL18aimDq63Gd7t4u/MbtOtO9fXf0Bkrw1tnVsvKJvPz8vCdWtnSFoiexK0nGUc643GjFfwBSXd19AAB4nGNgZGBgAGJDG4eIeH6brwzc7AwgcOP8ilBkmp0RLM7BwASiAPUaCBoAAHicY2BkYGBnAAEQ+f8/OyMDIwMqUAEALYYCOwB4nGNgYGBgH0QYAEV4APYAAAAAAAAOAGgAfgDMAOABAgFCAWwBmAHCAhgCWAK0AuADMAOwA94EMASWBNwFJAVmBXwFkgW0BnQHNgdaB/AINgiECLoJKAniCigAAHicY2BkYGBQYWhhYGcAASYg5gJCBob/YD4DABmbAcYAeJxdkE1qg0AYhl8Tk9AIoVDaVSmzahcF87PMARLIMoFAl0ZHY1BHdBJIT9AT9AQ9RQ9Qeqy+yteNMzDzfM+88w0K4BY/cNAMB6N2bUaPPBLukybCLvleeAAPj8JD+hfhMV7hC3u4wxs7OO4NzQSZcI/8Ltwnfwi75E/hAR7wJTyk/xYeY49fYQ/PztM+jbTZ7LY6OWdBJdX/pqs6NYWa+zMxa13oKrA6Uoerqi/JwtpYxZXJ1coUVmeZUWVlTjq0/tHacjmdxuL90OR8O0UEDYMNdtiSEpz5XQGqzlm30kzUdAYFFOb8R7NOZk0q2lwAyz1i7oAr1xoXvrOgtYhZx8wY5KRV269JZ5yGpmzPTjQhvY9je6vEElPOuJP3mWKnP5M3V+YAAAB4nG2PWW/CMBCEM0AC4ShQet8HfYzU/iTjLMTCsV0fQP99TQCpD92H3ZnVyp4vaSWHmib/1xwttNFBigxd9JCjjwGGGOEMY0wwxTlmuMAlrnCNG9ziDvd4wCOe8IwXvOIN75jjI+kYyX4G+1ZwYbmk1LDgaLDRMtRU1MFT/6il3p5kLcrTRSVW1WQZpHTcEqmClCc7/rvYCZ+578AsdZ0RSpHNXVh44SW5HmfGC62iqKIi61JXxcs216vsEOjsMAodvBSKZkfbPHTM3KrKjDPFSWaW9iyjBshY2ggdXN44RTufcqkjnFsLU3x9Rgw1PGkjg+stGaeF1ut01dj435pKobp+K3wMl/lQL6TNI0V05PyUhVLooqTIKhqQtNkkyS/jRoDc)
    format('woff');
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-play,
.video-js .vjs-big-play-button .vjs-icon-placeholder:before,
.video-js .vjs-play-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-play:before,
.video-js .vjs-big-play-button .vjs-icon-placeholder:before,
.video-js .vjs-play-control .vjs-icon-placeholder:before {
  content: '\\F101';
}

.vjs-icon-play-circle {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-play-circle:before {
  content: '\\F102';
}

.vjs-icon-pause,
.video-js .vjs-play-control.vjs-playing .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-pause:before,
.video-js .vjs-play-control.vjs-playing .vjs-icon-placeholder:before {
  content: '\\F103';
}

.vjs-icon-volume-mute,
.video-js .vjs-mute-control.vjs-vol-0 .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-volume-mute:before,
.video-js .vjs-mute-control.vjs-vol-0 .vjs-icon-placeholder:before {
  content: '\\F104';
}

.vjs-icon-volume-low,
.video-js .vjs-mute-control.vjs-vol-1 .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-volume-low:before,
.video-js .vjs-mute-control.vjs-vol-1 .vjs-icon-placeholder:before {
  content: '\\F105';
}

.vjs-icon-volume-mid,
.video-js .vjs-mute-control.vjs-vol-2 .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-volume-mid:before,
.video-js .vjs-mute-control.vjs-vol-2 .vjs-icon-placeholder:before {
  content: '\\F106';
}

.vjs-icon-volume-high,
.video-js .vjs-mute-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-volume-high:before,
.video-js .vjs-mute-control .vjs-icon-placeholder:before {
  content: '\\F107';
}

.vjs-icon-fullscreen-enter,
.video-js .vjs-fullscreen-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-fullscreen-enter:before,
.video-js .vjs-fullscreen-control .vjs-icon-placeholder:before {
  content: '\\F108';
}

.vjs-icon-fullscreen-exit,
.video-js.vjs-fullscreen .vjs-fullscreen-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-fullscreen-exit:before,
.video-js.vjs-fullscreen .vjs-fullscreen-control .vjs-icon-placeholder:before {
  content: '\\F109';
}

.vjs-icon-square {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-square:before {
  content: '\\F10A';
}

.vjs-icon-spinner {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-spinner:before {
  content: '\\F10B';
}

.vjs-icon-subtitles,
.video-js .vjs-subtitles-button .vjs-icon-placeholder,
.video-js .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js.video-js:lang(en-GB) .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js.video-js:lang(en-IE) .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js.video-js:lang(en-AU) .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js.video-js:lang(en-NZ) .vjs-subs-caps-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-subtitles:before,
.video-js .vjs-subtitles-button .vjs-icon-placeholder:before,
.video-js .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js.video-js:lang(en-GB) .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js.video-js:lang(en-IE) .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js.video-js:lang(en-AU) .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js.video-js:lang(en-NZ) .vjs-subs-caps-button .vjs-icon-placeholder:before {
  content: '\\F10C';
}

.vjs-icon-captions,
.video-js .vjs-captions-button .vjs-icon-placeholder,
.video-js:lang(en) .vjs-subs-caps-button .vjs-icon-placeholder,
.video-js:lang(fr-CA) .vjs-subs-caps-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-captions:before,
.video-js .vjs-captions-button .vjs-icon-placeholder:before,
.video-js:lang(en) .vjs-subs-caps-button .vjs-icon-placeholder:before,
.video-js:lang(fr-CA) .vjs-subs-caps-button .vjs-icon-placeholder:before {
  content: '\\F10D';
}

.vjs-icon-chapters,
.video-js .vjs-chapters-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-chapters:before,
.video-js .vjs-chapters-button .vjs-icon-placeholder:before {
  content: '\\F10E';
}

.vjs-icon-share {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-share:before {
  content: '\\F10F';
}

.vjs-icon-cog {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-cog:before {
  content: '\\F110';
}

.vjs-icon-circle,
.video-js .vjs-play-progress,
.video-js .vjs-volume-level,
.vjs-seek-to-live-control .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-circle:before,
.video-js .vjs-play-progress:before,
.video-js .vjs-volume-level:before,
.vjs-seek-to-live-control .vjs-icon-placeholder:before {
  content: '\\F111';
}

.vjs-icon-circle-outline {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-circle-outline:before {
  content: '\\F112';
}

.vjs-icon-circle-inner-circle {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-circle-inner-circle:before {
  content: '\\F113';
}

.vjs-icon-hd {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-hd:before {
  content: '\\F114';
}

.vjs-icon-cancel,
.video-js .vjs-control.vjs-close-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-cancel:before,
.video-js .vjs-control.vjs-close-button .vjs-icon-placeholder:before {
  content: '\\F115';
}

.vjs-icon-replay,
.video-js .vjs-play-control.vjs-ended .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-replay:before,
.video-js .vjs-play-control.vjs-ended .vjs-icon-placeholder:before {
  content: '\\F116';
}

.vjs-icon-play-previous {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-play-previous:before {
  content: '\\F117';
}

.vjs-icon-play-next {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-play-next:before {
  content: '\\F118';
}

.vjs-icon-close {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-close:before {
  content: '\\F119';
}

.vjs-icon-skip-10-min {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-skip-10-min:before {
  content: '\\F11A';
}

.vjs-icon-skip-10-plus {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-skip-10-plus:before {
  content: '\\F11B';
}

.vjs-icon-facebook {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-facebook:before {
  content: '\\F11C';
}

.vjs-icon-gplus {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-gplus:before {
  content: '\\F11D';
}

.vjs-icon-linkedin {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-linkedin:before {
  content: '\\F11E';
}

.vjs-icon-twitter {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-twitter:before {
  content: '\\F11F';
}

.vjs-icon-tumblr {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-tumblr:before {
  content: '\\F120';
}

.vjs-icon-pinterest {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-pinterest:before {
  content: '\\F121';
}

.vjs-icon-audio-description,
.video-js .vjs-descriptions-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-audio-description:before,
.video-js .vjs-descriptions-button .vjs-icon-placeholder:before {
  content: '\\F122';
}

.vjs-icon-audio,
.video-js .vjs-audio-button .vjs-icon-placeholder {
  font-family: VideoJS;
  font-weight: normal;
  font-style: normal;
}

.vjs-icon-audio:before,
.video-js .vjs-audio-button .vjs-icon-placeholder:before {
  content: '\\F123';
}

.vjs-label-hidden {
  display: none !important;
}

.cld-video-player .vjs-control-bar .vjs-control.vjs-ads-label {
  line-height: 3em;
  font-weight: normal;
  text-align: center;
  display: none;
  width: auto;
  padding: 0 1em;
}

.cld-video-player.vjs-ad-playing .vjs-control.vjs-ads-label {
  display: block;
}

.cld-video-player .ima-fullscreen-div,
.cld-video-player .ima-mute-div,
.cld-video-player .ima-play-pause-div,
.cld-video-player .ima-slider-div {
  line-height: 1.7;
}

.cld-video-player.vjs-ad-loading > video,
.cld-video-player.vjs-ad-loading > .vjs-poster {
  opacity: 0;
}

.ima-ad-container {
  top: 0;
  left: 0;
  position: absolute;
  display: block;
  width: 100%;
  height: 100%;
}
.ima-ad-container video {
  left: 0;
}

/* Move overlay if user fast-clicks play button. */
.video-js.vjs-playing .bumpable-ima-ad-container {
  margin-top: -50px;
}

/* Move overlay when controls are active. */
.video-js.vjs-user-inactive.vjs-playing .bumpable-ima-ad-container {
  margin-top: 0px;
}

.video-js.vjs-paused .bumpable-ima-ad-container,
.video-js.vjs-playing:hover .bumpable-ima-ad-container,
.video-js.vjs-user-active.vjs-playing .bumpable-ima-ad-container {
  margin-top: -50px;
}

.vjs-ima-non-linear .vjs-big-play-button,
.vjs-ima-non-linear .vjs-menu.vjs-settings-menu,
.vjs-ima-non-linear .vjs-info-overlay,
.vjs-ima-non-linear .vjs-modal-overlay,
.vjs-ima-non-linear .vjs-control-bar {
  z-index: 1112;
}

.ima-controls-div {
  bottom: 0px;
  height: 37px;
  position: absolute;
  overflow: hidden;
  display: none;
  opacity: 1;
  background-color: rgba(7, 20, 30, 0.7);
  background: -moz-linear-gradient(bottom, rgba(7, 20, 30, 0.7) 0%, rgba(7, 20, 30, 0) 100%);
  /* FF3.6+ */
  background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(0%, rgba(7, 20, 30, 0.7)),
    color-stop(100%, rgba(7, 20, 30, 0))
  );
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(bottom, rgba(7, 20, 30, 0.7) 0%, rgba(7, 20, 30, 0) 100%);
  /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(bottom, rgba(7, 20, 30, 0.7) 0%, rgba(7, 20, 30, 0) 100%);
  /* Opera 11.10+ */
  background: -ms-linear-gradient(bottom, rgba(7, 20, 30, 0.7) 0%, rgba(7, 20, 30, 0) 100%);
  /* IE10+ */
  background: linear-gradient(to top, rgba(7, 20, 30, 0.7) 0%, rgba(7, 20, 30, 0) 100%);
  /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0007141E', endColorstr='#07141E',GradientType=0 );
  /* IE6-9 */
}

.ima-countdown-div {
  height: 10px;
  color: #ffffff;
  text-shadow: 0 0 0.2em #000;
  cursor: default;
}

.ima-seek-bar-div {
  top: 12px;
  height: 3px;
  position: absolute;
  background: rgba(255, 255, 255, 0.4);
}

.ima-progress-div {
  width: 0px;
  height: 3px;
  background-color: #ecc546;
}

.ima-play-pause-div,
.ima-mute-div,
.ima-slider-div,
.ima-fullscreen-div {
  width: 35px;
  height: 20px;
  top: 11px;
  left: 0px;
  position: absolute;
  color: #cccccc;
  font-size: 1.5em;
  line-height: 2;
  text-align: center;
  font-family: VideoJS;
  cursor: pointer;
}

.ima-mute-div {
  left: auto;
  right: 85px;
}

.ima-slider-div {
  left: auto;
  right: 35px;
  width: 50px;
  height: 10px;
  top: 20px;
  background-color: #555555;
}

.ima-slider-level-div {
  width: 100%;
  height: 10px;
  background-color: #ecc546;
}

.ima-fullscreen-div {
  left: auto;
  right: 0px;
}

.ima-playing:before {
  content: '\\F103';
}

.ima-paused:before {
  content: '\\F101';
}

.ima-playing:hover:before,
.ima-paused:hover:before {
  text-shadow: 0 0 1em #fff;
}

.ima-non-muted:before {
  content: '\\F107';
}

.ima-muted:before {
  content: '\\F104';
}

.ima-non-muted:hover:before,
.ima-muted:hover:before {
  text-shadow: 0 0 1em #fff;
}

.ima-non-fullscreen:before {
  content: '\\F108';
}

.ima-fullscreen:before {
  content: '\\F109';
}

.ima-non-fullscreen:hover:before,
.ima-fullscreen:hover:before {
  text-shadow: 0 0 1em #fff;
}

.video-js.vjs-ad-playing .vjs-control-bar {
  z-index: 1112;
}

.video-js.vjs-ad-playing .vjs-control-bar .vjs-progress-control,
.video-js.vjs-ad-playing .cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker,
.cld-video-player .video-js.vjs-ad-playing .vjs-control-bar .vjs-progress-control-events-blocker {
  pointer-events: none;
}

.video-js.vjs-ad-playing
  .vjs-control-bar
  > :not(.vjs-play-control):not(.vjs-volume-menu-button):not(.vjs-time-control):not(.vjs-progress-control):not(.vjs-spacer):not(.vjs-fullscreen-control):not(.vjs-gradient):not(.ima-countdown-div),
.video-js.vjs-ad-playing .vjs-control-bar .vjs-mouse-display-tooltip,
.video-js.vjs-ad-playing .vjs-control-bar .vjs-progress-control:before,
.video-js.vjs-ad-playing
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker:before,
.cld-video-player
  .video-js.vjs-ad-playing
  .vjs-control-bar
  .vjs-progress-control-events-blocker:before,
.video-js.vjs-ad-playing .vjs-control-bar .vjs-play-progress:before,
.video-js.vjs-ad-playing .vjs-control-bar .vjs-progress-control .vjs-load-progress,
.video-js.vjs-ad-playing
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-load-progress,
.cld-video-player
  .video-js.vjs-ad-playing
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-load-progress,
.video-js.vjs-ad-playing .vjs-control-bar .vjs-progress-control .vjs-mouse-display,
.video-js.vjs-ad-playing
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-mouse-display,
.cld-video-player
  .video-js.vjs-ad-playing
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-mouse-display {
  display: none;
}

.video-js.vjs-ad-playing .vjs-control-bar {
  transition: height 0.1s ease;
}

.video-js.vjs-ad-playing.vjs-user-inactive:not(.vjs-ad-paused) .vjs-control-bar {
  opacity: 1;
  height: 0;
}

.video-js.vjs-ad-playing.vjs-user-inactive:not(.vjs-ad-paused)
  .vjs-control-bar
  .vjs-progress-holder {
  margin: 0;
}

.video-js.vjs-ad-playing .vjs-control-bar > :not(.vjs-progress-control):not(.ima-countdown-div) {
  transition: opacity 0.1s ease, visibility 0.1s ease;
}

.video-js.vjs-ad-playing
  .vjs-control-bar
  > :not(.vjs-progress-control):not(.ima-countdown-div)
  div {
  transition: line-height 0.1s ease;
}

.video-js.vjs-ad-playing.vjs-user-inactive:not(.vjs-ad-paused)
  .vjs-control-bar
  > :not(.vjs-progress-control):not(.ima-countdown-div) {
  opacity: 0;
  visibility: hidden;
  overflow: hidden;
}

.video-js.vjs-ad-playing.vjs-user-inactive:not(.vjs-ad-paused)
  .vjs-control-bar
  > :not(.vjs-progress-control):not(.ima-countdown-div)
  div {
  line-height: 0.01em;
}

.video-js.vjs-ad-playing .vjs-control-bar .vjs-progress-control .vjs-thumbnail-holder,
.video-js.vjs-ad-playing
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-thumbnail-holder,
.cld-video-player
  .video-js.vjs-ad-playing
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-thumbnail-holder,
.video-js.vjs-ad-playing .vjs-control-bar .vjs-progress-control .vjs-thumbnail,
.video-js.vjs-ad-playing
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-thumbnail,
.cld-video-player
  .video-js.vjs-ad-playing
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-thumbnail {
  display: none !important;
}

.video-js.vjs-ad-playing .vjs-control-bar .vjs-progress-control .vjs-progress-holder,
.video-js.vjs-ad-playing
  .cld-video-player
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-progress-holder,
.cld-video-player
  .video-js.vjs-ad-playing
  .vjs-control-bar
  .vjs-progress-control-events-blocker
  .vjs-progress-holder {
  cursor: auto;
  box-shadow: none;
  font-size: 100%;
  margin: 0 1em;
}

.video-js .vjs-control-bar .ima-countdown-div {
  display: none;
  z-index: 1;
}

.video-js.vjs-ad-playing .vjs-control-bar .ima-countdown-div {
  display: block;
  position: absolute;
  margin-top: -2em;
  left: 2em;
  font-size: 1.2em;
}

.video-js .vjs-control:before {
  font-size: 2em;
  line-height: 1.5em;
}

.video-js .vjs-control {
  width: 3em;
}

.video-js .vjs-control:focus,
.vjs-big-play-button:focus,
.vjs-icon-close:focus,
.vjs-volume-bar:focus {
  outline: none;
}

.video-js .vjs-control:hover,
.vjs-big-play-button:hover,
.vjs-icon-close:hover,
.vjs-volume-bar:hover {
  opacity: 0.95;
}

.vjs-cloudinary-button:focus {
  opacity: 0.8;
}

.cld-video-player.cld-video-player-skin-dark .vjs-big-play-button:focus {
  background-color: rgba(40, 40, 40, 0.8);
}

.cld-video-player.cld-video-player-skin-light .vjs-big-play-button:focus {
  background-color: rgba(223, 234, 252, 0.8);
}

.cld-video-player-skin-light .vjs-control:focus {
  text-shadow: 0em 0em 1em #0e2f5a;
}

.cld-ie11.video-js .vjs-control:before {
  font-size: 0.6em;
}

.cld-video-player {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  font-weight: 300;
}
.cld-video-player:focus {
  outline: none;
}
.cld-video-player.cld-fluid .vjs-poster {
  background-size: cover;
}
.cld-video-player .vjs-time-control {
  padding-left: 0;
  padding-right: 0;
}
.cld-video-player .vjs-time-divider {
  min-width: 1em;
  display: block;
}
.cld-video-player .vjs-current-time {
  display: block;
}
.cld-video-player .vjs-remaining-time {
  display: none;
}
.cld-video-player .vjs-duration {
  display: block;
}
.cld-video-player .vjs-big-play-button {
  font-size: 5em;
  line-height: 1.5em;
  height: 1.5em;
  width: 1.5em;
  border: 0;
  border-radius: 50%;
  left: 50%;
  top: 50%;
  margin-left: -0.75em;
  margin-top: -0.75em;
}
.cld-video-player.vjs-paused .vjs-big-play-button,
.cld-video-player.vjs-paused.vjs-has-started .vjs-big-play-button {
  opacity: 1;
  visibility: visible;
}
.cld-video-player.vjs-controls-disabled .vjs-big-play-button,
.cld-video-player.vjs-has-started .vjs-big-play-button,
.cld-video-player.vjs-using-native-controls .vjs-big-play-button,
.cld-video-player.vjs-error .vjs-big-play-button {
  transition: visibility 0.2s, opacity 0.2s;
  display: block;
  visibility: hidden;
  opacity: 0;
}
.cld-video-player .vjs-control-bar .vjs-volume-panel {
  margin-right: 1em;
}
.cld-video-player .vjs-control-bar .vjs-menu-button-inline:hover,
.cld-video-player .vjs-control-bar .vjs-menu-button-inline:focus,
.cld-video-player .vjs-control-bar .vjs-menu-button-inline.vjs-slider-active,
.cld-video-player .vjs-control-bar .vjs-no-flex .vjs-menu-button-inline {
  width: 10em;
}
.cld-video-player .vjs-control-bar .vjs-menu-button-inline:before {
  width: 1.6em;
}
.cld-video-player .vjs-control-bar .vjs-menu-button-inline .vjs-menu {
  left: 3em;
}
.cld-video-player .vjs-control-bar .vjs-progress-control,
.cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker {
  position: absolute;
  left: 0px;
  width: 100%;
  background-color: inherit;
  height: 1em;
  padding: 0.7em 0.2em 0 0.2em;
  bottom: 3em;
}
.cld-video-player .vjs-control-bar .vjs-progress-holder {
  height: 0.25em;
}
.cld-video-player .vjs-control-bar .vjs-progress-control:hover .vjs-progress-holder,
.cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker:hover .vjs-progress-holder {
  font-size: 1.2em;
  transition: none;
}
.cld-video-player .vjs-control-bar .vjs-progress-control:hover .vjs-time-tooltip,
.cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker:hover .vjs-time-tooltip {
  font-size: 0.8em;
}
.cld-video-player .vjs-control-bar .vjs-play-progress::before {
  top: -0.25em;
}
.cld-video-player .vjs-control-bar .vjs-progress-control-events-blocker {
  background-color: transparent;
}
.cld-video-player .vjs-spacer {
  flex: auto;
}

.video-js .vjs-title-bar {
  display: none;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  height: 7.2em;
  text-align: left;
  padding: 16px 21px;
  pointer-events: none;
}
.video-js .vjs-title-bar .vjs-title-bar-title {
  width: 100%;
  font-size: 1.2em;
  font-weight: 500;
  line-height: 1.2em;
  margin-bottom: 0.55em;
}
.video-js .vjs-title-bar .vjs-title-bar-subtitle {
  width: 100%;
  font-size: 1em;
  font-weight: 300;
  line-height: 1em;
}

.vjs-has-started .vjs-title-bar {
  display: flex;
  flex-direction: column;
  align-items: center;
  visibility: visible;
  opacity: 1;
  transition: visibility 0.1s, opacity 0.1s;
}

.vjs-has-started.vjs-user-inactive.vjs-playing .vjs-title-bar {
  visibility: visible;
  opacity: 0;
  transition: visibility 1s, opacity 1s;
}

.vjs-controls-disabled .vjs-title-bar,
.vjs-using-native-controls .vjs-title-bar,
.vjs-error .vjs-title-bar,
.vjs-ad-playing .vjs-title-bar {
  display: none !important;
}

.vjs-audio.vjs-has-started.vjs-user-inactive.vjs-playing .vjs-title-bar {
  opacity: 1;
  visibility: visible;
}

.vjs-has-started.vjs-no-flex .vjs-title-bar {
  display: table;
}

.vjs-recommendations-overlay {
  display: flex;
  align-items: center;
  justify-content: center;
  visibility: hidden;
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
}
.vjs-recommendations-overlay .vjs-recommendations-overlay-hide {
  font-size: 1em;
  cursor: pointer;
  position: absolute;
  display: inline-block;
  top: 3.75%;
  right: 1.72%;
}
.vjs-recommendations-overlay .vjs-recommendations-overlay-content {
  position: relative;
  width: 85%;
}
.vjs-recommendations-overlay .vjs-recommendations-overlay-content:before {
  display: block;
  content: '';
  width: 100%;
  padding-top: 56.25%;
}
.vjs-recommendations-overlay .vjs-recommendations-overlay-content > .aspect-ratio-content {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
}
.vjs-recommendations-overlay .vjs-recommendations-overlay-content .aspect-ratio-content {
  display: flex;
  flex-flow: column;
  height: auto;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item {
  border: 1px solid rgba(255, 255, 255, 0.5);
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-primary {
  flex: 1.82;
  display: flex;
  flex-flow: row;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-primary
  .vjs-recommendations-overlay-item-primary-image {
  flex: 1;
  background-size: cover;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-primary
  .vjs-recommendations-overlay-item-primary-content {
  flex: 0.5625;
  display: flex;
  flex-flow: column;
  background: rgba(0, 0, 0, 0.6);
  text-align: left;
  padding: 3%;
  min-width: 0;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-primary
  .vjs-recommendations-overlay-item-primary-content
  h2 {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0 0 0 0;
  font-size: 18px;
  margin: 0 0 1em 0;
  font-weight: 600;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-primary
  .vjs-recommendations-overlay-item-primary-content
  h3 {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0 0 0 0;
  font-size: 20px;
  margin: 0 0 1.3em 0;
  font-weight: 500;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-primary
  .vjs-recommendations-overlay-item-primary-content
  p {
  padding: 0 0 0 0;
  margin: 0 0 0 0;
  font-size: 14px;
  overflow: hidden;
  line-height: 1.4em;
  font-weight: 400;
}
@media only screen and (max-width: 1050px) {
  .vjs-recommendations-overlay
    .vjs-recommendations-overlay-content
    .aspect-ratio-content
    .vjs-recommendations-overlay-item-primary
    .vjs-recommendations-overlay-item-primary-content
    h2 {
    font-size: 16px;
  }
  .vjs-recommendations-overlay
    .vjs-recommendations-overlay-content
    .aspect-ratio-content
    .vjs-recommendations-overlay-item-primary
    .vjs-recommendations-overlay-item-primary-content
    h3 {
    font-size: 18px;
  }
  .vjs-recommendations-overlay
    .vjs-recommendations-overlay-content
    .aspect-ratio-content
    .vjs-recommendations-overlay-item-primary
    .vjs-recommendations-overlay-item-primary-content
    p {
    font-size: 12px;
  }
}
@media only screen and (max-width: 900px) {
  .vjs-recommendations-overlay
    .vjs-recommendations-overlay-content
    .aspect-ratio-content
    .vjs-recommendations-overlay-item-primary
    .vjs-recommendations-overlay-item-primary-content
    p {
    font-size: 10px;
  }
}
@media only screen and (max-width: 768px) {
  .vjs-recommendations-overlay
    .vjs-recommendations-overlay-content
    .aspect-ratio-content
    .vjs-recommendations-overlay-item-primary
    .vjs-recommendations-overlay-item-primary-content
    p {
    display: none;
  }
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container {
  flex: 1;
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container
  .vjs-recommendations-overlay-item-secondary {
  background-size: 100% 100%;
  flex: 1;
  max-width: 33%;
  font-size: 1.16em;
  position: relative;
  margin: 2% 2% 0 0;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container
  .vjs-recommendations-overlay-item-secondary:last-child {
  margin-right: 0;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container
  .vjs-recommendations-overlay-item-secondary
  div {
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  text-align: left;
  line-height: normal;
  position: absolute;
  width: 100%;
  bottom: 0;
  left: 0;
  padding: 20% 7% 4.5% 7%;
}
.cld-video-player-skin-light
  .vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container
  .vjs-recommendations-overlay-item-secondary
  div {
  padding: 5% 7%;
}
.cld-video-player-skin-dark
  .vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container
  .vjs-recommendations-overlay-item-secondary
  div.vjs-recommendations-overlay-item-info {
  text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.3);
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container
  .vjs-recommendations-overlay-item-secondary
  div
  span {
  display: block;
  min-width: 0;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container
  .vjs-recommendations-overlay-item-secondary
  div
  span.vjs-recommendations-overlay-item-secondary-title {
  flex: 2.3;
  font-size: 13px;
}
.vjs-recommendations-overlay
  .vjs-recommendations-overlay-content
  .aspect-ratio-content
  .vjs-recommendations-overlay-item-secondary-container
  .vjs-recommendations-overlay-item-secondary
  div
  span.vjs-recommendations-overlay-item-secondary-duration {
  text-align: right;
  margin-left: 10px;
}

.cld-plw-horizontal .cld-plw-col-list {
  padding-top: 14.0625%;
  position: relative;
}

.cld-plw-horizontal .cld-plw-panel {
  overflow: auto;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  display: flex;
  flex-direction: row;
}

.cld-plw-horizontal .cld-plw-panel-item {
  background-image: none;
  min-width: 25%;
  max-width: 25%;
}
.cld-plw-horizontal .cld-plw-panel-item:first-child {
  margin-left: 0;
}
.cld-plw-horizontal .cld-plw-panel-item:last-child {
  margin-right: 0;
}

.cld-plw-vertical {
  display: flex;
  flex-direction: row;
}
.cld-plw-vertical.cld-plw-layout > div:first-child {
  flex: 1;
}
.cld-plw-vertical .cld-plw-col-list {
  display: flex;
  flex-direction: column;
  min-width: 20%;
}
.cld-plw-vertical .cld-plw-panel {
  height: 100%;
  overflow: auto;
}
.cld-plw-vertical .cld-plw-panel-item {
  height: 25%;
}
.cld-plw-vertical .cld-plw-panel-item:last-child {
  margin-bottom: 0;
}

.cld-plw-item-title-next {
  font-weight: bold;
}

.cld-plw-item-info-wrap {
  position: absolute;
  bottom: 0;
  left: 0;
  padding: 5% 7%;
  width: 100%;
  transition: color 0.25s;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  box-sizing: border-box;
}
.cld-video-player-skin-dark .cld-plw-item-info-wrap {
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
}
@media only screen and (max-width: 768px) {
  .cld-video-player-skin-dark .cld-plw-item-info-wrap {
    background: #272727;
  }
}

.cld-plw-item-title {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.cld-plw-item-duration {
  margin-left: 10px;
}

/*  Default Playlist Layout */
.cld-plw-layout.cld-plw-layout-fluid {
  width: 100%;
}

.cld-plw-layout.cld-plw-vertical {
  flex-direction: row;
}

.cld-plw-layout.cld-plw-horizontal {
  flex-direction: column;
}

.cld-plw-custom {
  padding: 4px;
}
.cld-plw-custom .cld-plw-panel-item {
  background: none !important;
  margin: 4px 0;
}
.cld-plw-custom .cld-plw-panel-item:first-child {
  margin-top: 0;
}
.cld-plw-custom .cld-plw-panel-item:last-child {
  margin-bottom: 0;
}
.cld-plw-custom .cld-plw-panel-item img {
  display: block;
  width: 150px;
}
.cld-plw-custom .cld-plw-panel-item .cld-plw-item-info-wrap {
  left: 160px;
}
.cld-plw-custom .cld-plw-panel-item .cld-plw-item-title-next {
  display: block;
}

@media only screen and (max-width: 768px) {
  .cld-plw-layout.cld-plw-vertical,
  .cld-plw-layout.cld-plw-horizontal {
    flex-direction: column;
  }
  .cld-plw-layout.cld-plw-vertical .cld-plw-panel,
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel {
    flex-direction: column;
  }
  .cld-plw-layout.cld-plw-vertical .cld-plw-col-list,
  .cld-plw-layout.cld-plw-horizontal .cld-plw-col-list {
    width: inherit !important;
    height: inherit !important;
    max-height: 340px;
  }
  .cld-plw-layout.cld-plw-vertical .cld-plw-panel-item,
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel-item {
    background: none !important;
  }
  .cld-plw-layout.cld-plw-vertical .cld-plw-panel-item img,
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel-item img {
    display: block;
    width: 150px;
    height: 84px;
  }
  .cld-plw-layout.cld-plw-vertical .cld-plw-panel-item .cld-plw-item-info-wrap,
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel-item .cld-plw-item-info-wrap {
    left: 150px;
    padding: 15px 10px;
    top: 0;
    right: 0;
    width: auto;
  }
  .cld-plw-layout.cld-plw-vertical .cld-plw-panel-item .cld-plw-item-title-next,
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel-item .cld-plw-item-title-next {
    display: block;
  }
  .cld-plw-layout.cld-plw-vertical .cld-plw-panel-item .cld-plw-item-title-curr,
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel-item .cld-plw-item-title-curr {
    display: block;
  }
  .cld-plw-layout.cld-plw-vertical .cld-plw-panel-item .cld-plw-item-title,
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel-item .cld-plw-item-title {
    white-space: normal;
  }
  .cld-plw-layout.cld-plw-horizontal .cld-plw-col-list {
    padding-top: 0;
    overflow: auto;
  }
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel {
    position: initial;
  }
  .cld-plw-layout.cld-plw-horizontal .cld-plw-panel-item {
    max-width: none;
  }
}

.cld-thumbnail {
  position: relative;
  display: block;
  width: 100%;
  overflow: hidden;
  font-size: 1em;
  text-align: left;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
}
.cld-thumbnail .cld-thumbnail-img {
  display: none;
}
.cld-video-player-skin-light .cld-thumbnail .cld-plw-item-info-wrap {
  background: #fff;
}
.cld-video-player-skin-light .cld-thumbnail.cld-plw-panel-item-active {
  border: 1px solid #0078ff;
  box-sizing: border-box;
}
.cld-video-player-skin-light .cld-thumbnail.cld-plw-panel-item-active .cld-plw-item-info-wrap {
  background: rgba(0, 119, 255, 0.8);
}
.cld-video-player-skin-light .cld-thumbnail.cld-plw-panel-item-active .cld-plw-item-info-wrap {
  color: #fff;
}
.cld-video-player-skin-light
  .cld-thumbnail.cld-plw-panel-item-active:hover
  .cld-plw-item-info-wrap {
  color: #fff;
}
.cld-video-player-skin-dark .cld-thumbnail:before {
  content: '';
  position: absolute;
  top: 40%;
  max-height: 60%;
  right: 0;
  bottom: 0;
  left: 0;
  background: linear-gradient(to top, rgba(0, 0, 0, 0.7), transparent 80%);
  opacity: 1;
}
.cld-video-player-skin-dark .cld-thumbnail.cld-plw-panel-item-active {
  border: 1px solid #ff620c;
  box-sizing: border-box;
}
@media only screen and (min-width: 769px) {
  .cld-video-player-skin-dark .cld-thumbnail.cld-plw-panel-item-active:before {
    background: linear-gradient(to top, rgba(0, 0, 0, 0.9), transparent 90%);
  }
  .cld-video-player-skin-dark .cld-thumbnail.cld-plw-panel-item-active .cld-plw-item-info-wrap {
    font-weight: 500;
  }
}
.cld-video-player-skin-dark .cld-thumbnail.cld-plw-panel-item-active .cld-plw-item-info-wrap {
  color: #ff620c;
}

.cld-plw-panel-item:hover:after {
  content: '';
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  background-color: rgba(255, 255, 255, 0.2);
}

@media only screen and (max-width: 768px) {
  .cld-thumbnail:before {
    background: none;
  }
}

.video-js .vjs-volume-panel-horizontal .vjs-volume-control:before,
.video-js .vjs-volume-panel-horizontal .vjs-volume-level:before {
  content: '';
  font-size: 20px;
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 0;
  line-height: 0px;
  border-style: solid;
  border-width: 0 0 1.2em 6em;
  border-color: transparent transparent #0e2f5a transparent;
}

.video-js .vjs-volume-panel-horizontal .vjs-volume-control {
  overflow: hidden;
  height: 3em;
}

.video-js .vjs-volume-panel-horizontal .vjs-volume-bar {
  margin: 0;
  height: 100%;
  background-color: transparent;
}

.video-js .vjs-volume-panel-horizontal .vjs-volume-level {
  overflow: hidden;
  height: 100%;
  background: transparent;
  box-shadow: none;
}
.video-js .vjs-volume-panel-horizontal .vjs-volume-level:before {
  border-color: transparent transparent #e8e8e9 transparent;
}

.cld-video-player-floater {
  position: static;
  transition: all 0.5s ease-out;
  transition-property: bottom, right, left;
}
.cld-video-player-floater .cld-video-player-floater-close {
  display: none;
}
.cld-video-player-floater.cld-video-player-floating {
  position: fixed;
  z-index: 9;
  border: 4px solid #fff;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
}
.cld-video-player-floater.cld-video-player-floating .cld-video-player-floater-inner {
  height: 0;
  width: 100%;
}
.cld-video-player-floater.cld-video-player-floating .cld-video-player-floater-close {
  position: absolute;
  bottom: 100%;
  color: #999;
  background: #fff;
  border: 1px solid #e6e6e6;
  border-bottom: none;
  margin-bottom: 4px;
  padding: 4px 6px 2px;
  display: block;
  cursor: pointer;
}
.cld-video-player-floater.cld-video-player-floating .vjs-time-control,
.cld-video-player-floater.cld-video-player-floating .vjs-upcoming-video-title-display,
.cld-video-player-floater.cld-video-player-floating .vjs-recommendations-overlay {
  display: none;
}
.cld-video-player-floater.cld-video-player-floating .vjs-upcoming-video .vjs-upcoming-video-bar {
  background-color: transparent;
}
.cld-video-player-floater.cld-video-player-floating.cld-video-player-floater-bottom-right {
  bottom: 20px !important;
  right: 20px !important;
  top: auto !important;
  left: auto !important;
}
.cld-video-player-floater.cld-video-player-floating.cld-video-player-floater-bottom-right
  .cld-video-player-floater-close {
  right: 0;
}
.cld-video-player-floater.cld-video-player-floating.cld-video-player-floater-bottom-left {
  bottom: 20px !important;
  left: 20px !important;
  top: auto !important;
  right: auto !important;
}
.cld-video-player-floater.cld-video-player-floating.cld-video-player-floater-bottom-left
  .cld-video-player-floater-close {
  left: 0;
}

.vjs-context-menu-ui {
  position: absolute;
  z-index: 2;
}
.vjs-context-menu-ui .vjs-menu-content {
  background: rgba(0, 0, 0, 0.6);
  border-radius: 0.2em;
  padding: 0;
}
.vjs-context-menu-ui .vjs-menu-item {
  font-size: 1em;
  line-height: 1em;
  text-transform: none;
  cursor: pointer;
  margin: 0;
  padding: 0.8em 1.4em;
}
.vjs-context-menu-ui .vjs-menu-item:active,
.vjs-context-menu-ui .vjs-menu-item:hover {
  background-color: rgba(0, 0, 0, 0.5);
}

.cld-video-player.vjs-playlist .vjs-playlist-button {
  display: block;
}

.cld-video-player .vjs-playlist-button {
  display: none;
}

.vjs-upcoming-video {
  opacity: 0;
  transition: bottom 0.1s, visibility 0.4s, opacity 0.4s;
  visibility: hidden;
  position: absolute;
  bottom: 4.5em;
  right: 0.75em;
  width: 38.7%;
  max-width: 30em;
  border: 1px solid #e8e8e9;
}
.vjs-upcoming-video:before {
  display: block;
  content: '';
  width: 100%;
  padding-top: 56.25%;
}
.vjs-upcoming-video > .aspect-ratio-content {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
}
.vjs-upcoming-video .aspect-ratio-content {
  background-size: cover;
}
.vjs-upcoming-video .aspect-ratio-content .vjs-upcoming-video-bar {
  display: flex;
  flex: auto;
  justify-content: space-between;
  position: absolute;
  height: 3em;
  line-height: 3em;
  width: 100%;
  bottom: 0px;
}
.vjs-upcoming-video .aspect-ratio-content .vjs-upcoming-video-bar .vjs-upcoming-video-title {
  flex: auto;
  text-align: left;
  display: block;
  width: auto;
  max-width: 80%;
  padding-left: 1em;
  padding-right: 1em;
}
.vjs-upcoming-video
  .aspect-ratio-content
  .vjs-upcoming-video-bar
  .vjs-upcoming-video-title
  .vjs-upcoming-video-title-display {
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
}
.vjs-upcoming-video
  .aspect-ratio-content
  .vjs-upcoming-video-bar
  .vjs-upcoming-video-title
  .vjs-upcoming-video-title-display
  .vjs-upcoming-video-title-display-label {
  font-weight: 400;
}
.vjs-has-started.vjs-user-inactive.vjs-playing .vjs-upcoming-video {
  transition: bottom 1s, visibility 0.4s, opacity 0.4s;
  bottom: 0.7em;
}
.vjs-upcoming-video.vjs-upcoming-video-show {
  transition: visibility 0.4s, opacity 0.4s;
  opacity: 1;
  visibility: visible;
}
.vjs-upcoming-video.disable-transition {
  transition: visibility 0s;
}
@media only screen and (max-width: 768px) {
  .vjs-upcoming-video:before {
    display: none;
  }
  .cld-video-player.cld-video-player-skin-dark .vjs-upcoming-video,
  .cld-video-player.cld-video-player-skin-light .vjs-upcoming-video {
    border: none;
  }
  .vjs-upcoming-video .aspect-ratio-content {
    background-image: none !important;
  }
}

a.vjs-cloudinary-button {
  background: url(fonts/cloudinary_icon_for_black_bg.svg) no-repeat;
  background-size: 25px;
  background-position: center;
}
.cld-video-player-skin-light a.vjs-cloudinary-button {
  background-image: url(fonts/cloudinary_icon_for_white_bg.svg);
}
a.vjs-cloudinary-button:hover {
  cursor: pointer;
}

.vjs-cloudinary-tooltip {
  display: none;
  background: rgba(0, 0, 0, 0.9);
  padding: 20px;
  width: 320px;
  max-width: 100%;
  border-radius: 4px;
  position: absolute;
  bottom: 40px;
  right: 45px;
  z-index: 1;
  text-align: left;
}
.vjs-cloudinary-tooltip-show .vjs-cloudinary-tooltip {
  display: block;
}
.vjs-controls-disabled .vjs-cloudinary-tooltip {
  display: none;
}
.vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-close-button {
  display: none;
  background: url(data:image/svg+xml;base64,PCEtLSBHZW5lcmF0ZWQgYnkgSWNvTW9vbi5pbyAtLT4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIiB2aWV3Qm94PSIwIDAgMzIgMzIiPgo8dGl0bGU+YXJyb3ctbGVmdC13aGl0ZTwvdGl0bGU+CjxwYXRoIGZpbGw9IiNmZmYiIGQ9Ik0xMi41ODYgMjcuNDE0bC0xMC0xMGMtMC43ODEtMC43ODEtMC43ODEtMi4wNDcgMC0yLjgyOGwxMC0xMGMwLjc4MS0wLjc4MSAyLjA0Ny0wLjc4MSAyLjgyOCAwczAuNzgxIDIuMDQ3IDAgMi44MjhsLTYuNTg2IDYuNTg2aDE5LjE3MmMxLjEwNSAwIDIgMC44OTUgMiAycy0wLjg5NSAyLTIgMmgtMTkuMTcybDYuNTg2IDYuNTg2YzAuMzkgMC4zOSAwLjU4NiAwLjkwMiAwLjU4NiAxLjQxNHMtMC4xOTUgMS4wMjQtMC41ODYgMS40MTRjLTAuNzgxIDAuNzgxLTIuMDQ3IDAuNzgxLTIuODI4IDB6Ij48L3BhdGg+Cjwvc3ZnPgo=)
    no-repeat;
  background-size: 22px;
  height: 20px;
  width: 22px;
  margin-right: 10px;
}
.vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-close-button:hover {
  cursor: pointer;
}
.vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-header {
  display: inline-block;
  font-size: 14px;
  line-height: 25px;
  margin-bottom: 11px;
  text-decoration: none;
  color: inherit;
}
.vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-header .vjs-cloudinary-tooltip-logo {
  background: url(fonts/cloudinary_logo_for_dark_bg.svg) no-repeat;
  display: inline-block;
  height: 35px;
  width: 135px;
  vertical-align: bottom;
}
.vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-data-field {
  font-size: 12px;
  font-weight: 300;
  margin-bottom: 1em;
}
.vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-data-field .vjs-cloudinary-tooltip-label {
  display: inline-block;
  width: 44%;
  max-width: 120px;
}
.cld-video-player-skin-light .vjs-cloudinary-tooltip {
  background: rgba(255, 255, 255, 0.9);
}
.cld-video-player-skin-light .vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-close-button {
  background-image: url(data:image/svg+xml;base64,PCEtLSBHZW5lcmF0ZWQgYnkgSWNvTW9vbi5pbyAtLT4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIiB2aWV3Qm94PSIwIDAgMzIgMzIiPgo8dGl0bGU+YXJyb3ctbGVmdC1ibHVlPC90aXRsZT4KPHBhdGggZmlsbD0iIzBlMmY1YSIgZD0iTTEyLjU4NiAyNy40MTRsLTEwLTEwYy0wLjc4MS0wLjc4MS0wLjc4MS0yLjA0NyAwLTIuODI4bDEwLTEwYzAuNzgxLTAuNzgxIDIuMDQ3LTAuNzgxIDIuODI4IDBzMC43ODEgMi4wNDcgMCAyLjgyOGwtNi41ODYgNi41ODZoMTkuMTcyYzEuMTA1IDAgMiAwLjg5NSAyIDJzLTAuODk1IDItMiAyaC0xOS4xNzJsNi41ODYgNi41ODZjMC4zOSAwLjM5IDAuNTg2IDAuOTAyIDAuNTg2IDEuNDE0cy0wLjE5NSAxLjAyNC0wLjU4NiAxLjQxNGMtMC43ODEgMC43ODEtMi4wNDcgMC43ODEtMi44MjggMHoiPjwvcGF0aD4KPC9zdmc+Cg==);
}
.cld-video-player-skin-light .vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-logo {
  background-image: url(fonts/cloudinary_logo_for_white_bg.svg);
}
@media only screen and (max-width: 768px) {
  .vjs-cloudinary-tooltip {
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    border-radius: 0;
    width: auto;
  }
  .vjs-cloudinary-tooltip .vjs-cloudinary-tooltip-close-button {
    display: inline-block;
  }
}

.vjs-cloudinary-tooltip-show .vjs-control-bar {
  opacity: 1 !important;
}

.vjs-cloudinary-tooltip-show .vjs-upcoming-video {
  bottom: 4.5em !important;
}

.video-js .video-play-button {
  position: absolute;
  bottom: 16px;
  ${props =>
    props.isModule
      ? `
    left: 8px;
    @media only screen and (min-width: 768px) {
        left: 16px;
    }
    background-color: transparent;
    height: 40px;
    width: 40px;
  `
      : `
    left: 50%;
    transform: translate(-50%, 0);
    background: white !important;
    height: 28px;
    width: 28px;
    border-radius: 14px;
  `}
  padding: 0;
}

`;

export default CloudinaryVideoPlayerGlobalStyle;
