// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Image } from '@tcp/core/src/components/common/atoms';
import { getImageFilePath, getIconPath } from '@tcp/core/src/utils';
import styles from './VideoPlayer.style';
import { CloudinaryVideoPlayerGlobalStyle } from './CloudinaryVideoPlayerGlobal.style';

/**
 * To Render video in image tag only for safari
 * as safari is not palying videos on iOS 13.5.1
 */
class VideoPlayerInImg extends React.Component {
  constructor(props) {
    super(props);
    const { isModule } = this.props;
    this.playIcon = isModule
      ? `${getImageFilePath()}/play-accessible.svg`
      : getIconPath('icon-play');
    this.pauseIcon = isModule
      ? `${getImageFilePath()}/stop-accessible.svg`
      : getIconPath('icon-pause');
    this.playImgSrc = this.getImageSource(props.urlSansExtn, { extn: 'mp4' });
    this.pauseImgSrc = this.getImageSource(props.urlSansExtn, { extn: 'jpg' });
    this.imgEleRef = React.createRef();

    this.state = {
      uniqueId: props.uniqueId,
      iconPath: this.pauseIcon,
      isPlaying: true,
      classes: props.classes,
      componentHeight: '',
    };
    this.togglePlayPauseVideo = this.togglePlayPauseVideo.bind(this);
  }

  getComponentHeight = () => {
    const imgElement = this.imgEleRef;
    let height = '';
    if (imgElement) {
      height = imgElement.current.clientHeight;
    }
    return height;
  };

  togglePlayPauseVideo = (isPlaying) => {
    const { disableVideoClickHandler } = this.props;
    if (!disableVideoClickHandler) {
      if (isPlaying) {
        this.pauseVideo();
      } else {
        this.playVideo();
      }
    }
  };

  playVideo = () => {
    const { classes } = this.state;
    const newClasses = classes.filter((item) => item !== 'vjs-has-started');
    newClasses.push('vjs-has-started');
    this.setState({
      classes: newClasses,
      iconPath: this.pauseIcon,
      isPlaying: true,
    });
  };

  pauseVideo = () => {
    const { componentHeight } = this.state;
    const newState = {
      isPlaying: false,
      iconPath: this.playIcon,
    };
    // without this patch, when user pause the video
    // height of img and video can be different due to transfoms, ex. ModA
    if (!componentHeight) {
      newState.componentHeight = this.getComponentHeight();
    }
    this.setState(newState);
  };

  getImageSource = (urlSansExtn, source) => `${urlSansExtn}.${source.extn}`;

  render() {
    const { className, dataLocator } = this.props;
    const { uniqueId, iconPath, isPlaying, classes, componentHeight } = this.state;

    const imgSource = isPlaying ? this.playImgSrc : this.pauseImgSrc;

    return (
      <div className={classes.join(' ')}>
        <CloudinaryVideoPlayerGlobalStyle />
        <img
          className={`${className} vjs-tech`}
          id={uniqueId}
          data-locator={`${dataLocator}_video`}
          data-image
          data-playing={isPlaying}
          src={imgSource}
          onClick={() => this.togglePlayPauseVideo(isPlaying)}
          alt={`${dataLocator}_video`}
          style={{ height: componentHeight }}
          ref={this.imgEleRef}
          role="presentation"
        />
        <button
          className="video-play-button"
          type="button"
          title="Play Video"
          aria-disabled="false"
          onClick={() => this.togglePlayPauseVideo(isPlaying)}
          id={`button-${uniqueId}`}
        >
          <Image
            className="tcp_carousel__play_pause_button_icon"
            aria-hidden="true"
            src={iconPath}
          />
        </button>
      </div>
    );
  }
}

VideoPlayerInImg.propTypes = {
  className: PropTypes.string,
  dataLocator: PropTypes.string,
  disableVideoClickHandler: PropTypes.bool,
  uniqueId: PropTypes.string.isRequired,
  urlSansExtn: PropTypes.string.isRequired,
  classes: PropTypes.arrayOf(PropTypes.string),
  isModule: PropTypes.bool.isRequired,
};

VideoPlayerInImg.defaultProps = {
  dataLocator: '',
  className: '',
  disableVideoClickHandler: false,
  classes: [],
};

export default withStyles(VideoPlayerInImg, styles);
