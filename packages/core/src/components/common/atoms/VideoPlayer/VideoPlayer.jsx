// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Image } from '@tcp/core/src/components/common/atoms';
import {
  getImageFilePath,
  getViewportInfo,
  isSafariBrowser,
  getSafariVersion,
  getIconPath,
} from '@tcp/core/src/utils';
import VideoPlayerInImg from './VideoPlayerInImg';
import styles, { getVideoStyle, StyledVideoContainer } from './VideoPlayer.style';
import { CloudinaryVideoPlayerGlobalStyle } from './CloudinaryVideoPlayerGlobal.style';

/**
 * To Generate the unique ids based on the timestap for multiple video players
 */
const getUniqueID = () => {
  return `video_${Date.now() + (Math.random() * 100000).toFixed()}`;
};

/**
 * To Render the video element
 * cloudinary handles the url by default so when updateVideoUrl is passed as false then url should
 * not contain the base url and it should not have any version
 * example url when updateVideoUrl is false - ecom/assets/content/gym/video/Gymboree_CircleOpen_500_20191029
 * no need to pass the video extension as this is handled by cloudinary itself.
 */
class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    const { isModule } = this.props;
    this.playIcon = isModule
      ? `${getImageFilePath()}/play-accessible.svg`
      : getIconPath('icon-play');
    this.pauseIcon = isModule
      ? `${getImageFilePath()}/stop-accessible.svg`
      : getIconPath('icon-pause');
    this.state = {
      uniqueId: getUniqueID(),
      classes: ['video-js', 'vjs-fluid', 'cld-video-player', `cld-video-player_${getUniqueID()}`],
      iconPath: this.pauseIcon,
    };
    this.player = null;
    this.stopVideo = false;
    this.onPlay = this.onPlay.bind(this);
    this.onPause = this.onPause.bind(this);
    this.renderVideoInImage = this.renderVideoInImageTag();
  }

  componentDidMount() {
    const { uniqueId } = this.state;
    const { controls, muted } = this.props;

    // add muted attribute separately because React filters it out of the HTML
    document.getElementById(uniqueId).defaultMuted = !!muted;
    document.getElementById(uniqueId).setAttribute('webkit-video-playable-inline', '');

    // Assign controls to player here because it is an attribute without a value
    if (controls) {
      this.controls = controls;
    }
    const buttonEle = document.getElementById(`button-${uniqueId}`);
    if (buttonEle) {
      buttonEle.addEventListener('click', this.handleClick);
    }
  }

  componentWillUnmount() {
    const { uniqueId } = this.state;
    const buttonEle = document.getElementById(`button-${uniqueId}`);

    if (buttonEle) {
      buttonEle.removeEventListener('click', this.handleClick);
    }
  }

  togglePlayPauseVideo = (event) => {
    const { disableVideoClickHandler } = this.props;

    if (!disableVideoClickHandler) {
      const player = event.currentTarget;
      if (player.paused) {
        player.play();
      } else {
        player.pause();
      }
    }
  };

  onPause = () => {
    const { classes } = this.state;
    const newClasses = classes.filter((item) => item !== 'vjs-has-started');
    newClasses.push('vjs-paused');
    this.setState({
      classes: newClasses,
      iconPath: this.playIcon,
      isPlaying: false,
    });
  };

  onPlay = () => {
    const { classes } = this.state;
    const newClasses = classes.filter((item) => item !== 'vjs-paused');
    newClasses.push('vjs-has-started');
    this.setState({
      classes: newClasses,
      iconPath: this.pauseIcon,
      isPlaying: true,
    });
  };

  handleClick = (e) => {
    e.preventDefault();
    const { uniqueId, isPlaying } = this.state;
    const player = document.getElementById(uniqueId);
    if (player.paused || !isPlaying) {
      player.play();
      this.onPlay();
    } else {
      player.pause();
      this.onPause();
    }
  };

  checkIfPatchVersion = () => {
    let isPatchVersion = false;
    const version = `${getSafariVersion()}`;
    isPatchVersion = version && version.indexOf('13.5') === 0;
    return isPatchVersion;
  };

  renderVideoInImageTag = () => {
    let renderVideoInImage = false;
    if (getViewportInfo().isMobile && isSafariBrowser()) {
      renderVideoInImage = this.checkIfPatchVersion();
    }
    return renderVideoInImage;
  };

  render() {
    const {
      url,
      className,
      dataLocator,
      autoplay,
      muted,
      loop,
      disableVideoClickHandler,
      isModule,
      heightOverrides,
    } = this.props;
    const { uniqueId, classes, iconPath } = this.state;

    if (!url) {
      return null;
    }

    const urlSansExtn = url.replace(/\.(mp4|webm|mov|WEBM|MP4|MOV)$/, '');
    const posterImage = urlSansExtn.replace('##format##', '').concat('.jpg');

    return (
      <React.Fragment>
        {!this.renderVideoInImage ? (
          <StyledVideoContainer
            className={classes.join(' ')}
            overrideStyle={getVideoStyle(heightOverrides)}
          >
            <CloudinaryVideoPlayerGlobalStyle isModule={isModule} />
            <video
              autoPlay={autoplay}
              loop={loop}
              muted={muted}
              playsInline
              id={uniqueId}
              className={`${className} vjs-tech`}
              data-locator={`${dataLocator}_video`}
              onClick={(e) => {
                this.togglePlayPauseVideo(e);
              }}
              poster={posterImage}
              onPlay={(e) => {
                this.onPlay(e);
              }}
              onPause={(e) => {
                this.onPause(e);
              }}
              src={urlSansExtn.replace('##format##', 'f_mp4,vc_auto,q_auto').concat('.mp4')}
            >
              <track kind="captions" />
            </video>
            <button
              className="video-play-button"
              type="button"
              title="Play Video"
              aria-disabled="false"
              id={`button-${uniqueId}`}
            >
              <Image
                className="tcp_carousel__play_pause_button_icon"
                aria-hidden="true"
                src={iconPath}
              />
            </button>
          </StyledVideoContainer>
        ) : (
          <VideoPlayerInImg
            dataLocator={dataLocator}
            uniqueId={uniqueId}
            urlSansExtn={urlSansExtn}
            disableVideoClickHandler={disableVideoClickHandler}
            classes={classes}
            isModule={isModule}
          />
        )}
      </React.Fragment>
    );
  }
}

VideoPlayer.propTypes = {
  url: PropTypes.string.isRequired,
  loop: PropTypes.string,
  autoplay: PropTypes.string,
  muted: PropTypes.string,
  className: PropTypes.string,
  controls: PropTypes.string,
  dataLocator: PropTypes.string,
  disableVideoClickHandler: PropTypes.bool,
  isModule: PropTypes.bool,
  heightOverrides: PropTypes.shape({}),
};

VideoPlayer.defaultProps = {
  dataLocator: '',
  loop: '1',
  autoplay: '1',
  muted: '1',
  className: '',
  controls: '0',
  disableVideoClickHandler: false,
  isModule: false,
  heightOverrides: {},
};

export default withStyles(VideoPlayer, styles);
