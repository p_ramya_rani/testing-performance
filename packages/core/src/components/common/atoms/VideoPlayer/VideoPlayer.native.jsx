// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Video from 'react-native-video';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  getScreenWidth,
  convertNumToBool,
  cropVideoUrl,
  configureInternalNavigationFromCMSUrl,
} from '@tcp/core/src/utils';
import { navigateToPage } from '@tcp/core/src/utils/index.native';
import { VideoWrapper, StyledImage } from './styles/VideoPlayer.style.native';

const pauseImage = require('../../../../../../mobileapp/src/assets/images/play-icon.png');

class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    const autoPlayOption = props.autoplay ? convertNumToBool(props.autoplay) : false;
    this.state = {
      paused: !autoPlayOption,
    };
  }

  handleVideoClick = () => {
    const { isHomePage, link, navigation } = this.props;
    if (isHomePage && link && navigation) {
      const cmsValidatedUrl = configureInternalNavigationFromCMSUrl(link.url);
      navigateToPage(cmsValidatedUrl, navigation);
    } else {
      this.setState(prevState => ({
        paused: !prevState.paused,
      }));
    }
  };

  renderVideoPlayer = () => {
    const {
      videoHeight: height,
      videoWidth: width,
      url,
      poster,
      muted,
      loop,
      controls,
      disableVideoClickHandler,
      resizeMode,
      overrideStyle,
    } = this.props;
    const { paused } = this.state;
    const muteOption = muted ? convertNumToBool(muted) : false;
    const controlsOption = controls ? convertNumToBool(controls) : false;
    const loopOption = loop ? convertNumToBool(loop) : false;
    const croppedUrl = cropVideoUrl(url);
    const urlSansExtn = croppedUrl.replace(/\.(mp4|webm|WEBM|MP4)$/, '');
    const sources = [
      { mime: 'video/webm', format: 'f_webm,vc_vp9,q_auto' },
      { mime: 'video/mp4;code=hevc', format: 'f_mp4,vc_h265,q_auto' },
      { mime: 'video/mp4', format: 'f_mp4,vc_auto,q_auto' },
    ].map(source => {
      return urlSansExtn.replace('##format##', source.format);
    });
    return (
      <VideoWrapper>
        {paused && !disableVideoClickHandler && <StyledImage source={pauseImage} />}
        <Video
          source={{ uri: sources[2] }}
          ref={ref => {
            this.player = ref;
          }}
          onBuffer={this.onBuffer}
          onError={this.videoError}
          style={{
            height,
            width: width || '100%',
            ...overrideStyle,
          }}
          paused={paused}
          poster={poster}
          repeat={loopOption}
          resizeMode={resizeMode || 'cover'}
          posterResizeMode="contain"
          muted={muteOption}
          controls={controlsOption}
          data-setup='{"fluid": true}'
        />
      </VideoWrapper>
    );
  };

  render() {
    const { disableVideoClickHandler } = this.props;
    const VideoPlayerComponent = this.renderVideoPlayer();

    return disableVideoClickHandler ? (
      VideoPlayerComponent
    ) : (
      <TouchableOpacity accessibilityRole="link" onPress={this.handleVideoClick}>
        {VideoPlayerComponent}
      </TouchableOpacity>
    );
  }
}

VideoPlayer.propTypes = {
  videoWidth: PropTypes.number,
  videoHeight: PropTypes.number,
  url: PropTypes.string.isRequired,
  poster: PropTypes.string,
  muted: PropTypes.string,
  loop: PropTypes.string,
  autoplay: PropTypes.string,
  controls: PropTypes.string,
  disableVideoClickHandler: PropTypes.bool,
  isHomePage: PropTypes.bool,
  link: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  resizeMode: PropTypes.string.isRequired,
  overrideStyle: PropTypes.shape({}).isRequired,
};

VideoPlayer.defaultProps = {
  poster: '',
  videoWidth: getScreenWidth(),
  videoHeight: 400,
  muted: '1',
  loop: '1',
  autoplay: '1',
  controls: '0',
  disableVideoClickHandler: false,
  isHomePage: false,
  link: null,
  navigation: null,
};

export default withStyles(VideoPlayer);
export { VideoPlayer as VideoPlayerVanilla };
