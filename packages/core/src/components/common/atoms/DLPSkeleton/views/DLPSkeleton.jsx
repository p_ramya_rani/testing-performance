// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes } from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import style from '../DLPSkeleton.style';
import Row from '../../Row';
import Col from '../../Col';

const DLPSkeleton = ({ className, col, colSize }) => {
  return (
    <Row fullBleed className={`${className} skeleton-row`}>
      {Array.from({ length: col }).map((cItem, index) => {
        return (
          <Col key={index.toString()} colSize={colSize} className="product-tile">
            <div className="skeleton-col skeleton-img" />
            <div className="skeleton-col skeleton-badge" />
          </Col>
        );
      })}
    </Row>
  );
};

DLPSkeleton.propTypes = {
  className: PropTypes.string.isRequired,
  col: PropTypes.number,
  colSize: PropTypes.shape({}),
};

DLPSkeleton.defaultProps = {
  col: 1,
  colSize: { large: 12, medium: 8, small: 6 },
};

export default withStyles(DLPSkeleton, style);
export { DLPSkeleton as DLPSkeletonVanilla };
