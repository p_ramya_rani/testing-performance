// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { setTestId, getLocator } from '@tcp/core/src/utils';
import withStyles from '../../../hoc/withStyles.native';
import {
  PromotionalMessageContainer,
  PromotionalText,
  AppendedTextInMessageColor,
  PointsAndRewardsContainer,
  PointsAndRewardsTextOne,
  PointsAndRewardsTextTwo,
  PointsAndRewardsTextThree,
} from '../styles/PromotionalMessage.style.native';

export const getFormattedLoyaltyText = (text) => {
  return text
    .replace(/<[^>]*>/g, '')
    .replace(/\s+/g, ' ')
    .trim()
    .split('on');
};

const renderPointsAndReward = (labels) => {
  return (labels && labels.split('##')) || '';
};
const PromotionalMessage = ({
  text,
  isPlcc,
  marginTop,
  dataLocator,
  fontSize,
  isFullMessage,
  isBrierleyPromoEnabled,
  inGridPlpRecommendation,
  isNewReDesignProductSummary,
  isNewReDesignProductTile,
}) => {
  const pointsAndRewardText = renderPointsAndReward(text);
  return isBrierleyPromoEnabled ? (
    <PointsAndRewardsContainer
      marginTop={marginTop}
      {...setTestId(getLocator(dataLocator))}
      accessibilityRole="text"
      accessibilityLabel={text}
      inGridPlpRecommendation={inGridPlpRecommendation}
      isNewReDesignProductSummary={isNewReDesignProductSummary}
    >
      <PointsAndRewardsTextOne accessibilityRole="text">
        {pointsAndRewardText && pointsAndRewardText[0]}
      </PointsAndRewardsTextOne>
      <PointsAndRewardsTextTwo accessibilityRole="text">
        {pointsAndRewardText && pointsAndRewardText[1]}
      </PointsAndRewardsTextTwo>
      <PointsAndRewardsTextThree accessibilityRole="text">
        {pointsAndRewardText && pointsAndRewardText[2]}
      </PointsAndRewardsTextThree>
    </PointsAndRewardsContainer>
  ) : (
    <PromotionalMessageContainer
      marginTop={marginTop}
      {...setTestId(getLocator(dataLocator))}
      accessibilityRole="text"
      accessibilityLabel={text}
      inGridPlpRecommendation={inGridPlpRecommendation}
      isNewReDesignProductSummary={isNewReDesignProductSummary}
      isNewReDesignProductTile={isNewReDesignProductTile}
    >
      <PromotionalText
        isPlcc={isPlcc}
        accessibilityRole="text"
        numberOfLines={2}
        fontSize={fontSize}
        isNewReDesignProductTile={isNewReDesignProductTile}
      >
        {text && getFormattedLoyaltyText(text)[0]}
      </PromotionalText>
      {isFullMessage ? (
        <PromotionalText
          isPlcc={isPlcc}
          accessibilityRole="text"
          numberOfLines={2}
          fontSize={fontSize}
        >
          <AppendedTextInMessageColor>
            {text && getFormattedLoyaltyText(text)[1]}
          </AppendedTextInMessageColor>
        </PromotionalText>
      ) : null}
    </PromotionalMessageContainer>
  );
};

PromotionalMessage.propTypes = {
  text: PropTypes.string.isRequired,
  isPlcc: PropTypes.bool,
  height: PropTypes.string,
  marginTop: PropTypes.number,
  dataLocator: PropTypes.string,
  fontSize: PropTypes.string,
  isFullMessage: PropTypes.bool,
  isBrierleyPromoEnabled: PropTypes.bool,
  inGridPlpRecommendation: PropTypes.bool,
  isNewReDesignProductSummary: PropTypes.bool,
  isNewReDesignProductTile: PropTypes.bool,
};

PromotionalMessage.defaultProps = {
  isPlcc: false,
  height: null,
  marginTop: null,
  dataLocator: '',
  fontSize: null,
  isFullMessage: false,
  isBrierleyPromoEnabled: false,
  inGridPlpRecommendation: false,
  isNewReDesignProductSummary: false,
  isNewReDesignProductTile: false,
};

export default withStyles(PromotionalMessage);
export { PromotionalMessage as PromotionalMessageVanilla };
