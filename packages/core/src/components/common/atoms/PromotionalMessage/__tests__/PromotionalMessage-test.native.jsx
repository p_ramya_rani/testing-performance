// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { PromotionalMessageVanilla } from '../views/PromotionalMessage.view.native';

describe('PromotionalMessageVanilla', () => {
  let component;

  const props = {
    text: 'Test Data',
    isPlcc: false,
    height: null,
    marginTop: null,
    dataLocator: '',
    fontSize: null,
    isBrierleyPromoEnabled: true,
    inGridPlpRecommendation: true,
    isNewReDesignProductSummary: true,
    isNewReDesignProductTile: true,
    isFullMessage: true,
  };

  beforeEach(() => {
    component = shallow(<PromotionalMessageVanilla {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should render correctly isBrierleyPromoEnabled', () => {
    const component1 = shallow(
      <PromotionalMessageVanilla {...props} isBrierleyPromoEnabled={false} />
    );
    expect(component1).toMatchSnapshot();
  });
  it('should render correctly isFullMessage', () => {
    const component1 = shallow(<PromotionalMessageVanilla {...props} isFullMessage={false} />);
    expect(component1).toMatchSnapshot();
  });
});
