// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const getAdditionalStyle = (props) => {
  const { marginTop, height } = props;
  return {
    ...(marginTop && { 'margin-top': marginTop }),
    ...(height && { height }),
    'margin-left': '0px',
  };
};

const getPromotionalText = (props) => {
  const { fontSize, theme, isPlcc } = props;
  const { colorPalette, typography } = theme;
  const color = isPlcc ? colorPalette.userTheme.plcc : colorPalette.userTheme.mpr;
  return `
    color: ${color};
    font-family: ${typography.fonts.secondary};
    font-size: ${fontSize ? typography.fontSizes[fontSize] : typography.fontSizes.fs9};
    font-weight: ${props.theme.fonts.fontWeight.black}
    `;
};

const getAdditionalStylingPromotionalText = (props) => {
  const { isNewReDesignProductTile, theme } = props;
  if (isNewReDesignProductTile) {
    return `
      font-size: ${theme.typography.fontSizes.fs12}
      font-family: ${theme.fonts.nunitoBold}
    `;
  }
  return ``;
};

const PromotionalMessagePostfixText = (props) => {
  const { fontSize, theme } = props;
  const { colorPalette, typography } = theme;
  return `
    color: ${colorPalette.gray[900]};
    font-family: ${typography.fonts.secondary};
    font-size: ${fontSize ? typography.fontSizes[fontSize] : typography.fontSizes.fs9};
    `;
};

const additionalStylingPromotionalMessageContainer = (props) => {
  const { isNewReDesignProductTile, theme } = props;
  if (isNewReDesignProductTile) {
    return `
    margin-top: -5px
    margin-bottom: ${theme.spacing.ELEM_SPACING.XS}
    letter-spacing: 0.1
    `;
  }
  return ``;
};

const PromotionalMessageContainer = styled.View`
  ${getAdditionalStyle}
  ${(props) =>
    !props.isNewReDesignProductSummary &&
    `
  margin-bottom:${props.theme.spacing.ELEM_SPACING.SM};
  `}
  flex-direction:row;
  ${(props) => (props.inGridPlpRecommendation ? `padding-left: 10px` : ``)}
  ${additionalStylingPromotionalMessageContainer}
`;

// Color is hard code as not in the style guide
const PromotionalText = styled.Text`
  ${getPromotionalText}
  ${getAdditionalStylingPromotionalText}
`;
const PointsAndRewardsContainer = styled.View`
  ${getAdditionalStyle}
  ${(props) =>
    !props.isNewReDesignProductSummary
      ? `
  margin-bottom:${props.theme.spacing.ELEM_SPACING.SM};
  `
      : ''}
  flex-direction:row;
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: 12px;
  ${(props) => (props.inGridPlpRecommendation ? `padding-left: 10px` : ``)}
`;

const PointsAndRewardsTextOne = styled.Text`
  color: ${(props) => props.theme.colors.PROMO.ORANGE};
  font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
  margin-left: -3px;
`;
const PointsAndRewardsTextTwo = styled.Text`
  font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  color: ${(props) => props.theme.colors.BLACK};
  font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
  margin: 0px -1px;
`;
const PointsAndRewardsTextThree = styled.Text`
  font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  color: ${(props) => props.theme.colors.PROMO.BLUE};
  font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
`;
const AppendedTextInMessageColor = styled.Text`
  color: ${(props) => props.theme.colorPalette.gray[900]};
  font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
`;

const PromotionalMessagePostfix = styled.Text`
  ${PromotionalMessagePostfixText}
`;

export {
  PromotionalMessageContainer,
  PromotionalText,
  PromotionalMessagePostfix,
  AppendedTextInMessageColor,
  PointsAndRewardsContainer,
  PointsAndRewardsTextOne,
  PointsAndRewardsTextTwo,
  PointsAndRewardsTextThree,
};
