// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes } from 'prop-types';

import withStyles from '../../../hoc/withStyles';
import styles from '../Heading.style';

const headingVariants = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'nav', 'listMenu'];
const Heading = ({ children, className, component, variant, dataLocator, id }) => {
  let componentVariant = headingVariants.indexOf(variant) !== -1 ? variant : 'h1';
  componentVariant = componentVariant === 'listMenu' ? 'li' : componentVariant;
  const Component = component || componentVariant;
  return (
    <Component tabIndex="-1" className={className} data-locator={dataLocator} id={id}>
      {children}
    </Component>
  );
};

Heading.defaultProps = {
  component: null,
  className: null,
  children: null,
  variant: 'h1',
  dataLocator: '',
  id: '',
};

Heading.propTypes = {
  component: PropTypes.elementType,
  className: PropTypes.string,
  children: PropTypes.node,
  variant: PropTypes.oneOf(headingVariants),
  dataLocator: PropTypes.string,
  id: PropTypes.string,
};

const StyledHeading = withStyles(Heading, styles);
StyledHeading.defaultProps = Heading.defaultProps;

export { Heading as HeadingVanilla };
export default StyledHeading;
