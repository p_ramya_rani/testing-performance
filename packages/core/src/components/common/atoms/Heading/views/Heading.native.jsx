// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import HeadingStyles from '../Heading.style.native';
import withStyles from '../../../hoc/withStyles.native';
import { getLocator } from '../../../../../utils/index.native';

const Heading = props => {
  const { text, locator, ...otherProps } = props;
  return (
    <Text {...otherProps} testID={getLocator(locator)}>
      {text}
    </Text>
  );
};

Heading.propTypes = {
  text: PropTypes.string,
  locator: PropTypes.string,
};

Heading.defaultProps = {
  text: '',
  locator: '',
};

export default withStyles(Heading, HeadingStyles);
export { Heading as HeadingVanilla };
