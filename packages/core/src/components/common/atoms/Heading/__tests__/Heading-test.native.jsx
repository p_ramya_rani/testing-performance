// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import Heading from '../views/Heading';

describe('Heading', () => {
  let component;

  beforeEach(() => {
    const props = {
      text: 'Heading',
      locator: 'Locator',
    };
    component = shallow(<Heading {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should return abc component value one', () => {
    expect(component.find('Component')).toHaveLength(0);
  });
});
