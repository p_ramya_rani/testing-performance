// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const LoaderWrapper = styled.View`
  position: absolute;
  display: flex;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: ${props => props.theme.colors.GHOST_OVERLAY};
  z-index: 1400;
  align-items: center;
  justify-content: center;
`;

export const StyledMsg = styled.Text`
  font-size: ${props => props.theme.typography.fontSizes.fs16};
  color: ${props => props.theme.colorPalette.green[300]};
  font-family: ${props => props.theme.typography.fonts.secondary};
`;

export const RingView = styled.View`
  height: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
  width: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
  border-width: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
  border-color: ${props => props.theme.colorPalette.green[300]};
  border-radius: 50;
  justify-content: center;
  margin: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  padding-left: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export default { LoaderWrapper, StyledMsg, RingView };
