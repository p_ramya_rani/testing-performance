// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';
import Ring from './Ring.native';
import { LoaderWrapper, StyledMsg, RingView } from '../GhostOverlay.style.native';

const loadingSuccess = require('../../../../../assets/loader-success.png');

const GhostOverlay = ({ loadingMsg, isComplete }) => {
  return (
    <LoaderWrapper>
      {!isComplete && <Ring />}
      {isComplete && (
        <>
          <RingView>
            <Image width="15px" source={loadingSuccess} />
          </RingView>
          <StyledMsg>{loadingMsg}</StyledMsg>
        </>
      )}
    </LoaderWrapper>
  );
};

GhostOverlay.defaultProps = {
  loadingMsg: '',
  isComplete: false,
};

GhostOverlay.propTypes = {
  loadingMsg: PropTypes.string,
  isComplete: PropTypes.bool,
};

export default GhostOverlay;
