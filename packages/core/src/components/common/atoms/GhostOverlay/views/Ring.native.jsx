// 9fbef606107a605d69c0edbcd8029e5d 
import React, { Component } from 'react';
import { View, Animated } from 'react-native';

export default class Ring extends Component {
  state = {
    rotateValue: new Animated.Value(0),
  };

  componentDidMount() {
    this.start();
  }

  start = () => {
    const { rotateValue } = this.state;
    Animated.loop(
      Animated.timing(rotateValue, {
        toValue: 1,
        duration: 400,
        Infinite: true,
      })
    ).start();
  };

  getStylesObj = rotateValue => {
    return {
      transform: [
        {
          rotate: rotateValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '380deg'],
          }),
        },
      ],
      height: 48,
      width: 48,
      margin: 4,
      borderWidth: 2,
      borderColor: 'transparent',
      borderBottomColor: '#7dc24c',
      borderRadius: 50,
      justifyContent: 'center',
    };
  };

  render() {
    const { rotateValue } = this.state;
    return (
      <View>
        <Animated.View style={this.getStylesObj(rotateValue)} />
        <Animated.View />
      </View>
    );
  }
}
