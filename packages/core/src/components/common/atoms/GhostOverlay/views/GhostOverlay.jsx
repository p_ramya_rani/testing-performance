// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '../../../hoc/withStyles';
import styles from '../GhostOverlay.style';

const GhostOverlay = ({ loadingMsg, className, isComplete }) => {
  return (
    <div className={className}>
      <div id="default_spinner_overlay" className="spinner-overlay">
        <BodyCopy
          className="loader-msg"
          fontSize="fs16"
          component="p"
          fontFamily="secondary"
          fontWeight="bold"
        >
          {isComplete && loadingMsg}
        </BodyCopy>
        <div className="circle-loader-wrapper">
          <div className={`circle-loader ${isComplete ? 'load-complete' : ''}`}>
            <div className="checkmark draw"> </div>
          </div>
        </div>
      </div>
    </div>
  );
};

GhostOverlay.defaultProps = {
  className: '',
  loadingMsg: '',
  isComplete: false,
};

GhostOverlay.propTypes = {
  className: PropTypes.string,
  loadingMsg: PropTypes.string,
  isComplete: PropTypes.bool,
};

export default withStyles(GhostOverlay, styles);
export { GhostOverlay as GhostOverlayVanilla };
