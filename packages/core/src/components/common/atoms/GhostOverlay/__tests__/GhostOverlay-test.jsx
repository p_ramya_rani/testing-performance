// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { GhostOverlayVanilla } from '../views/GhostOverlay';

describe('GhostOverlay Componenet', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<GhostOverlayVanilla loadingMsg="loader" />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
