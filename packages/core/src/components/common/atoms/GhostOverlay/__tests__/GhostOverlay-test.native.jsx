// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import GhostOverlay from '../views/GhostOverlay.native';

describe('GhostOverlay Componenet', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<GhostOverlay loadingMsg="loader" />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
