// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .spinner-overlay {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(255, 255, 255, 0.9);
    z-index: 1400;
    cursor: pointer;
    align-items: center;
    justify-content: center;
    display: flex;
    flex-direction: column;
  }

  .loader-msg {
    color: ${props => props.theme.colorPalette.green[300]};
    text-transform: uppercase;
    text-align: center;
    width: 100%;
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  }

  .circle-loader-wrapper {
    text-align: center;
    width: 100%;
  }

  .circle-loader {
    border: 3px solid transparent;
    border-left-color: ${props => props.theme.colorPalette.green[500]};
    animation: loader-spin 1.2s infinite linear;
    position: relative;
    display: inline-block;
    vertical-align: top;
    border-radius: 50%;
    width: 3em;
    height: 3em;
  }

  .load-complete {
    -webkit-animation: none;
    animation: none;
    border-color: ${props => props.theme.colorPalette.green[500]};
    transition: border 500ms ease-out;
  }

  .checkmark {
    display: none;
  }

  .load-complete .checkmark {
    display: block;
  }

  .checkmark.draw:after {
    animation-duration: 800ms;
    animation-timing-function: ease;
    animation-name: checkmark;
    transform: scaleX(-1) rotate(135deg);
  }
  .checkmark:after {
    opacity: 1;
    height: 1.5em;
    width: 0.75em;
    transform-origin: left top;
    border-right: 3px solid ${props => props.theme.colorPalette.green[500]};
    border-top: 3px solid ${props => props.theme.colorPalette.green[500]};
    content: '';
    left: 0.6em;
    top: 1.5em;
    position: absolute;
  }

  @keyframes loader-spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
  @keyframes checkmark {
    0% {
      height: 0;
      width: 0;
      opacity: 1;
    }
    20% {
      height: 0;
      width: 0.75em;
      opacity: 1;
    }
    40% {
      height: 1.5em;
      width: 0.75em;
      opacity: 1;
    }
    100% {
      height: 1.5em;
      width: 0.75em;
      opacity: 1;
    }
  }
`;

export default styles;
