// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { getAPIConfig } from '@tcp/core/src/utils';
import { withTheme } from 'styled-components';
import BodyCopy from '../BodyCopy';
import Image from '../Image';
import { StyledErrorWrapper, ViewWithSpacing } from '../styledWrapper/styledWrapper.native';
import GooglePlacesAutocompleteGmaps from '../../../vendorLib/atoms/GooglePlaceInput/views/GooglePlacesAutocompleteGmaps.native';
import {
  StyledLabel,
  textInputContainer,
  description,
  listView,
  separator,
  poweredContainer,
  Container,
  item,
  container,
  textInputCustom,
} from './AutoCompleteComponent.native.style';

const errorIcon = require('../../../../../../mobileapp/src/assets/images/alert-triangle.png');

export class GooglePlacesInputGmaps extends PureComponent {
  constructor(props) {
    super(props);
    const apiConfigObj = getAPIConfig();
    const { googleApiKey } = apiConfigObj;
    this.googleApiKey = googleApiKey;
    this.state = {
      listViewDisplayed: false, // we need to handle listViewDisplayed by ourself as library used to set it true on every prop change
    };
  }

  onFocus = () => {
    this.setState({
      listViewDisplayed: 'auto',
    });
  };

  onBlur = () => {
    setTimeout(() => {
      // Need to add setTimeout as blur is calling first and then request to get detailed response
      // Value set to 1000ms to handle all auto complete scenarios
      this.setState({
        listViewDisplayed: false,
      });
    }, 1000);
  };

  onPress = (data, details = null) => {
    const { onValueChange } = this.props;
    this.setState(
      {
        listViewDisplayed: false,
      },
      () => {
        onValueChange(details, data.fullText);
      }
    );
  };

  render() {
    const {
      headerTitle,
      componentRestrictions,
      onSubmitEditing,
      onEndEditing,
      refs,
      onChangeText,
      input,
      initialValue,
      meta: { error, touched, active },
      clearButtonMode,
    } = this.props;
    const { listViewDisplayed } = this.state;

    const textInput = textInputCustom;

    return (
      <Container accessibilityLabel={headerTitle}>
        <StyledLabel isFocused={active || input.value}>{headerTitle}</StyledLabel>
        <GooglePlacesAutocompleteGmaps
          placeholder={null}
          suppressDefaultStyles
          minLength={3} // minimum length of text to search
          autoFocus={false}
          ref={instance => refs(instance)}
          returnKeyType="search"
          fetchDetails
          listViewDisplayed={listViewDisplayed}
          onPress={this.onPress}
          query={{
            key: this.googleApiKey,
            components: `country:${componentRestrictions.country[0]}`,
            types: 'address',
          }}
          getDefaultValue={() => initialValue}
          textInputProps={{
            name: input.name,
            onFocus: this.onFocus,
            onBlur: this.onBlur,
            clearButtonMode,
            onSubmitEditing: text => {
              onSubmitEditing(text.nativeEvent.text);
            },
            onEndEditing: text => {
              onEndEditing(text.nativeEvent.text);
            },
            onChangeText: text => {
              input.onChange(text);
              onChangeText(text);
            },
          }}
          styles={{
            textInputContainer,
            textInput,
            description,
            listView,
            separator,
            poweredContainer,
            row: item,
            container,
          }}
          nearbyPlacesAPI="GooglePlacesSearch"
          debounce={300} // debounce the requests in ms.
        />
        {touched && !active && error ? (
          <StyledErrorWrapper>
            <ViewWithSpacing spacingStyles="margin-right-XS">
              <Image source={errorIcon} alt="" width="16px" height="14px" />
            </ViewWithSpacing>
            <BodyCopy
              fontFamily="secondary"
              fontWeight="extrabold"
              fontSize="fs12"
              text={error}
              color="error"
            />
          </StyledErrorWrapper>
        ) : null}
      </Container>
    );
  }
}

GooglePlacesInputGmaps.propTypes = {
  headerTitle: PropTypes.string,
  componentRestrictions: PropTypes.shape({
    country: PropTypes.shape([]),
  }),
  onValueChange: PropTypes.func,
  onSubmitEditing: PropTypes.func,
  onEndEditing: PropTypes.func,
  refs: PropTypes.func,
  onChangeText: PropTypes.func,
  input: PropTypes.shape({}),
  meta: PropTypes.shape({
    touched: PropTypes.string,
    error: PropTypes.string,
  }),
  initialValue: PropTypes.string,
  clearButtonMode: PropTypes.string,
};

GooglePlacesInputGmaps.defaultProps = {
  headerTitle: 'Address Line',
  componentRestrictions: {
    country: [],
  },
  onValueChange: () => {},
  onSubmitEditing: () => {},
  onEndEditing: () => {},
  refs: () => {},
  onChangeText: () => {},
  input: {},
  meta: {},
  initialValue: '',
  clearButtonMode: 'while-editing',
};

export default withTheme(GooglePlacesInputGmaps);
