// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

export const AutoCompleteUL = styled.ul`
  background-color: white;
  padding: 0;
  list-style-type: none;
  border: 1px solid #d2d2d2;
  border-top: 0;
  box-sizing: border-box;
  position: absolute;
  z-index: 1;
  margin-top: -12px;
  ${props => `width:${props.inputElem.clientWidth}px`};
  ${props => {
    let display = `display:none`;
    if (props.plength && props.plength > 0 && props.inputElem.value) {
      display = `display:block`;
    }
    return display;
  }};

  &:after {
    content: '';
    padding: 1px 1px 1px 0;
    height: 18px;
    box-sizing: border-box;
    text-align: right;
    display: block;
    background-position: right;
    background-repeat: no-repeat;
    background-size: 120px 14px;
  }
`;

export const AutoCompleteLi = styled.li`
  padding: 5px 5px 5px 10px;
  height: 26px;
  line-height: 26px;
  border-top: 1px solid #d9d9d9;
  position: relative;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  font-size: 13px;
  font-family: ${props => (props.theme.brand === 'tcp' ? `Tofino` : `Nunito`)};

  &:hover {
    font-weight: bold;
  }
`;

export const SpanIcon = styled.span`
  display: inline-block;
  height: 18px;
  width: 15px;
  background-size: 31px;
  margin-right: 5px;
  background-image: url(https://maps.gstatic.com/mapfiles/api-3/images/autocomplete-icons_hdpi.png);
  background-position: -1px -161px;
`;
