// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import colors from '@tcp/core/styles/themes/TCP/colors';
import Fonts from '@tcp/core/styles/themes/TCP/fonts';
import Gray from '@tcp/core/styles/themes/colors/gray';

const StyledLabel = styled.Text`
  position: absolute;
  left: 0;
  top: ${props => (!props.isFocused ? props.theme.spacing.ELEM_SPACING.MED : '0')};
  font-size: ${props =>
    !props.isFocused
      ? props.theme.typography.fontSizes.fs14
      : props.theme.typography.fontSizes.fs10};
  color: ${props => props.theme.colors.PRIMARY.DARK};
  font-weight: ${props =>
    !props.isFocused
      ? props.theme.typography.fontWeights.regular
      : props.theme.typography.fontWeights.extrabold};
  margin-bottom: ${props => (props.isFocused ? props.theme.spacing.ELEM_SPACING.XXS : '0')};
  font-family: ${props => props.theme.typography.fonts.secondary};
`;

const Container = styled.TouchableOpacity`
  position: relative;
  width: 100%;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

const textInputNormal = {
  borderBottomWidth: 1,
  borderBottomColor: `${colors.FOOTER.DIVIDER}`,
  height: 40,
  paddingTop: 16,
  paddingBottom: 0,
  paddingLeft: 0,
  color: colors.BLACK,
};

const textInputCustom = {
  borderBottomWidth: 1,
  borderBottomColor: `${colors.FOOTER.DIVIDER}`,
  height: 40,
  paddingTop: 16,
  paddingBottom: 0,
  paddingLeft: 0,
  color: `${Gray[900]}`,
  fontWeight: `${Fonts.fontWeight.normal}`,
  fontFamily: 'Nunito',
  fontSize: 14,
};

const textInputContainer = {
  width: '100%',
};

const description = {
  fontWeight: `${Fonts.fontWeight.bold}`,
};

const listView = {
  backgroundColor: `${colors.WHITE}`,
};

const separator = {
  borderBottomWidth: 1,
  borderBottomColor: `${colors.FOOTER.DIVIDER}`,
};

const item = {
  paddingLeft: 6,
  paddingBottom: 6,
  paddingRight: 6,
  paddingTop: 8,
  height: 35,
};

const poweredContainer = {
  justifyContent: 'flex-end',
  flexDirection: 'row',
  alignItems: 'center',
};

const container = {
  flex: 1,
};

export {
  StyledLabel,
  Container,
  textInputNormal,
  textInputContainer,
  description,
  listView,
  separator,
  poweredContainer,
  item,
  container,
  textInputCustom,
};
