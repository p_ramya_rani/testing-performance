// 9fbef606107a605d69c0edbcd8029e5d 
/* Updated the component to use AutoPlaceService
  insead of the AutoComplete widget in order to have
  more control and reduce the number of calls being made
  and ultimately reducing the cose incurred.
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import { fetchData, formatPredictionValues } from '@tcp/core/src/utils/mapbox';
import { AutoCompleteUL, AutoCompleteLi, SpanIcon } from './AutoCompleteComponent.style';
import { getAPIConfig } from '../../../../utils';
import TextBox from '../TextBox';

export class AutoCompleteComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.refToInputElement = React.createRef();
    this.inputElementKey = '0';

    this.state = {
      gPredictions: [],
    };

    this.autoCompleteInit = this.autoCompleteInit.bind(this);
    this.handleOnPlaceSelected = this.handleOnPlaceSelected.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.sessionToken = null;
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside);
    const node = this.refToInputElement.current;
    this.autoCompleteInit(node);
  }

  componentWillUnmount() {
    if (document) document.removeEventListener('click', this.handleClickOutside);
  }

  // --------------- private methods --------------- //

  getAutoCompleteConfigObject() {
    let { types } = this.props;
    const { bounds, componentRestrictions, isStoresSearchForm } = this.props;
    if (isStoresSearchForm) {
      types = ['geocode'];
    }
    return componentRestrictions ? { types, bounds, componentRestrictions } : { types, bounds };
  }

  getResults = (
    placeName,
    placeAddressLine,
    placeCity,
    placeZip,
    placeState,
    placeCountry,
    placeGeometry
  ) => {
    const { onPlaceSelected } = this.props;
    if (placeName) {
      this.sessionToken = null;
      const address = {
        place: placeName,
        addressline: placeAddressLine,
        city: placeCity,
        zip: placeZip,
        state: placeState,
        country: placeCountry,
        geometry: placeGeometry,
      };

      onPlaceSelected(address, this.refToInputElement.value);
    }
  };

  handleClickOutside = e => {
    if (this.refToInputElement.current && !this.refToInputElement.current.contains(e.target)) {
      this.setState({ gPredictions: [] });
    }
  };

  autoCompleteInit(inputRef) {
    this.refToInputElement = inputRef;

    if (inputRef !== null) {
      inputRef.addEventListener(
        'input',
        debounce(async () => {
          const inputVal = inputRef.value;
          inputVal.replace('"', '\\"').replace(/^\s+|\s+$/g, '');
          this.sessionToken = getAPIConfig().mapBoxApiKey;
          if (inputVal !== '' && inputVal.length >= 3) {
            const data = await fetchData(inputVal, this.props);
            this.setState({ gPredictions: data });
          } else if (inputVal === '') {
            this.setState({ gPredictions: [] });
          }
        }, 300)
      );
    }
  }

  handleOnPlaceSelected(e) {
    const {
      dataset: {
        googlePlaceId = '',
        googlePlaceName = '',
        googlePlaceAddressLine = '',
        googlePlaceCity = '',
        googlePlaceZip = '',
        googlePlaceState = '',
        googlePlaceCountry = '',
        googlePlaceGeometry = {},
      },
    } = e.currentTarget;

    if (googlePlaceId) {
      this.getResults(
        googlePlaceName,
        googlePlaceAddressLine,
        googlePlaceCity,
        googlePlaceZip,
        googlePlaceState,
        googlePlaceCountry,
        googlePlaceGeometry
      );
    }
  }

  render() {
    const { ...otherProps } = this.props;
    const { gPredictions } = this.state;
    return (
      <>
        <TextBox {...otherProps} inputRef={this.refToInputElement} key={this.inputElementKey} />
        <AutoCompleteUL
          inputElem={this.refToInputElement}
          plength={gPredictions.length}
          className="autocomplete-results"
        >
          {gPredictions.length > 0 &&
            gPredictions.map(prediction => {
              const predictionData = formatPredictionValues(prediction);
              return (
                <AutoCompleteLi
                  key={predictionData.id}
                  onClick={this.handleOnPlaceSelected}
                  className="autocomplete-item"
                  data-type="place"
                  data-google-place-id={predictionData.id}
                  data-google-place-name={predictionData.place_name}
                  data-google-place-address-line={predictionData.addressline}
                  data-google-place-city={predictionData.city}
                  data-google-place-zip={predictionData.zip}
                  data-google-place-state={predictionData.state}
                  data-google-place-country={predictionData.country}
                  data-google-place-geometry={predictionData.coordinates}
                >
                  <SpanIcon />
                  <span className="autocomplete-text">{predictionData.place_name}</span>
                </AutoCompleteLi>
              );
            })}
        </AutoCompleteUL>
      </>
    );
  }

  // --------------- end of private methods --------------- //
}

AutoCompleteComponent.propTypes = {
  types: PropTypes.oneOf(['address']).isRequired,
  componentRestrictions: PropTypes.shape({}).isRequired,
  bounds: PropTypes.shape({}).isRequired,
  apiFields: PropTypes.shape({}).isRequired,
  input: PropTypes.shape({}).isRequired,
  onPlaceSelected: PropTypes.func.isRequired,
  isStoresSearchForm: PropTypes.bool.isRequired,
  mapboxAutocompleteTypesParam: PropTypes.string.isRequired,
};

export default AutoCompleteComponent;
