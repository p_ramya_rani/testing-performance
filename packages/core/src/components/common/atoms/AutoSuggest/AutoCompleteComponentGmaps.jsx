// 9fbef606107a605d69c0edbcd8029e5d 
/* Updated the component to use AutoPlaceService
  insead of the AutoComplete widget in order to have
  more control and reduce the number of calls being made
  and ultimately reducing the cose incurred.
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import { requireNamedOnlineModule } from '../../../../utils/resourceLoader';
import TextBox from '../TextBox';
import { AutoCompleteUL, AutoCompleteLi, SpanIcon } from './AutoCompleteComponent.style';
// this comment prevents linting errors

export class AutoCompleteComponentGmaps extends PureComponent {
  constructor(props) {
    super(props);
    this.googleAutocomplete = null;
    this.refToInputElement = React.createRef();
    this.inputElementKey = '0';
    this.googleAutocompleteServiceObject = null;

    this.state = {
      gPredictions: [],
    };

    this.autoCompleteInit = this.autoCompleteInit.bind(this);
    this.displaySuggestions = this.displaySuggestions.bind(this);
    this.handleOnPlaceSelected = this.handleOnPlaceSelected.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.autoCompleteInputDebaounce = debounce(this.autoCompleteDebounce, 600);
    this.sessionToken = null;
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside);
    const node = this.refToInputElement.current;
    this.autoCompleteInit(node);
  }

  componentWillUnmount() {
    const inputRef = this.refToInputElement.current;
    document.removeEventListener('click', this.handleClickOutside);
    if (inputRef) {
      inputRef.removeEventListener('input', this.autoCompleteInputDebaounce);
    }
  }

  // --------------- private methods --------------- //

  getAutoCompleteConfigObject() {
    let { types } = this.props;
    const { bounds, componentRestrictions, isStoresSearchForm } = this.props;
    if (isStoresSearchForm) {
      types = ['geocode'];
    }
    return componentRestrictions ? { types, bounds, componentRestrictions } : { types, bounds };
  }

  getResults = (place, status) => {
    const { onPlaceSelected } = this.props;
    if (status !== window.google.maps.places.PlacesServiceStatus.OK) {
      return;
    }
    if (place) {
      this.sessionToken = null;
      onPlaceSelected(place, this.refToInputElement.value);
    }
  };

  handleClickOutside = e => {
    if (this.refToInputElement.current && !this.refToInputElement.current.contains(e.target)) {
      this.setState({ gPredictions: [] });
    }
  };

  autoCompleteDebounce = () => {
    const inputRef = this.refToInputElement.current;
    const inputVal = inputRef.value;
    inputVal.replace('"', '\\"').replace(/^\s+|\s+$/g, '');
    const configObj = this.getAutoCompleteConfigObject();
    if (this.sessionToken === null)
      this.sessionToken = new window.google.maps.places.AutocompleteSessionToken();
    if (inputVal !== '' && inputVal.length >= 3) {
      this.googleAutocomplete.getPlacePredictions(
        {
          input: inputVal,
          ...configObj,
          sessionToken: this.sessionToken,
        },
        this.displaySuggestions
      );
    } else if (inputVal === '') {
      this.setState({ gPredictions: [] });
    }
  };

  displaySuggestions(predictions, status) {
    if (status !== window.google.maps.places.PlacesServiceStatus.OK) {
      return;
    }
    if (predictions && predictions.length > 1) {
      this.setState({ gPredictions: predictions });
    }
  }

  autoCompleteInit(inputRef) {
    this.refToInputElement = inputRef;

    if (inputRef !== null) {
      if (!this.googleAutocomplete) {
        // if the googleAutocomplete object was not created
        requireNamedOnlineModule('google.maps')
          .then(() => {
            this.googleAutocomplete = new window.google.maps.places.AutocompleteService();
            // we need a node to initialize the place service API
            this.googleAutocompleteServiceObject = new window.google.maps.places.PlacesService(
              document.createElement('div')
            );
            this.sessionToken = new window.google.maps.places.AutocompleteSessionToken();
          })
          .catch(() => null /* do nothing if unable to load googleAutocomplete */);
      } else {
        this.googleAutocomplete = new window.google.maps.places.AutocompleteService();
        this.googleAutocompleteServiceObject = new window.google.maps.places.PlacesService(
          document.createElement('div')
        );
        this.sessionToken = new window.google.maps.places.AutocompleteSessionToken();
      }

      inputRef.addEventListener('input', this.autoCompleteInputDebaounce);
    }
  }

  handleOnPlaceSelected(e) {
    const { apiFields } = this.props;
    const apiFieldsArray = apiFields && apiFields.split('|');
    const {
      dataset: { googlePlaceId = '' },
    } = e.currentTarget;
    if (this.googleAutocompleteServiceObject !== null && googlePlaceId) {
      this.googleAutocompleteServiceObject.getDetails(
        {
          fields: apiFieldsArray,
          placeId: googlePlaceId,
          sessionToken: this.sessionToken,
        },
        this.getResults
      );
    }
  }

  render() {
    const { ...otherProps } = this.props;
    const { gPredictions } = this.state;
    return (
      <>
        <TextBox {...otherProps} inputRef={this.refToInputElement} key={this.inputElementKey} />
        <AutoCompleteUL
          inputElem={this.refToInputElement}
          plength={gPredictions.length}
          className="autocomplete-results"
        >
          {gPredictions.length > 0 &&
            gPredictions.map(prediction => {
              return (
                <AutoCompleteLi
                  key={prediction.place_id}
                  onClick={this.handleOnPlaceSelected}
                  className="autocomplete-item"
                  data-type="place"
                  data-google-place-id={prediction.place_id}
                >
                  <SpanIcon />
                  <span className="autocomplete-text">{prediction.description}</span>
                </AutoCompleteLi>
              );
            })}
        </AutoCompleteUL>
      </>
    );
  }

  // --------------- end of private methods --------------- //
}

AutoCompleteComponentGmaps.propTypes = {
  types: PropTypes.oneOf(['address']).isRequired,
  componentRestrictions: PropTypes.shape({}).isRequired,
  bounds: PropTypes.shape({}).isRequired,
  apiFields: PropTypes.shape({}).isRequired,
  input: PropTypes.shape({}).isRequired,
  onPlaceSelected: PropTypes.func.isRequired,
  isStoresSearchForm: PropTypes.bool.isRequired,
};

export default AutoCompleteComponentGmaps;
