// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  margin-bottom: 12px;
  /* stylelint-disable-next-line*/
  _:-ms-fullscreen,
  .image-wrapper img {
    max-height: 100%;
  }
  .image-wrapper {
    display: inline-flex;
    height: 100%;
    width: 90px;
    border: 1px solid ${(props) => props.theme.colors.WHITE};
    &.selected-image {
      border: 1px solid ${(props) => props.theme.colors.BORDER.NORMAL};
    }
    img {
      display: inherit;
      height: 100%;
      width: 100%;
    }
  }
  .thumbnail {
    position: relative;
  }
  .thumbnail-play-icon {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    height: 35px;
  }
`;
