// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getVideoUrl, getImageFilePath } from '@tcp/core/src/utils';
import Image from '@tcp/core/src/components/common/atoms/Image';
import withStyles from '../../hoc/withStyles';
import styles from './Thumbnail.style';
import { getLocator } from '../../../../utils';
import Anchor from '../Anchor';
import DamImage from '../DamImage';
import IMG_DATA_PDP_THUMB from './config';

class Thumbnail extends React.Component {
  static propTypes = {
    /** thumbnail image data */
    image: PropTypes.shape({
      /** id of the image */
      id: PropTypes.number.isRequired,
      /** thumbnail's name */
      name: PropTypes.string.isRequired,
      /** url path of the thumbnail */
      thumbnailPath: PropTypes.string.isRequired,
    }).isRequired,
    /** flags if the thumbnail should be shown as selected */
    isSelected: PropTypes.bool,
    /**
     * Function to call when a thumbnail is clicked, which will receive the id
     * of the corresponding image as the only parameter.
     */
    onClick: PropTypes.func.isRequired,
    totalCount: PropTypes.number,
    index: PropTypes.number,
    className: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    const {
      onClick,
      image: { id },
    } = this.props;
    return onClick && onClick(id);
  }

  render() {
    const { image, isSelected, index, totalCount, className, primaryBrand, alternateBrand } =
      this.props;
    const imgData = {
      alt: image.name,
      url: image.thumbnailPath,
    };
    const isVideoUrl = imgData.url && getVideoUrl(imgData.url);

    return (
      <div isSelected={isSelected} className={className}>
        <Anchor
          aria-label={`Thumbnail image ${
            index + 1
          } of ${totalCount}. Click or Enter on this Icon to ${'display large view of the Image'}`}
          onClick={this.handleClick}
          className={[
            'image-wrapper',
            isSelected ? 'selected-image' : '',
            isVideoUrl ? 'thumbnail' : '',
          ].join(' ')}
          itemscope
          noLink
          itemtype="http://schema.org/ImageObject"
        >
          <DamImage
            data-locator={`${getLocator('pdp_alt_image')}_${index}`}
            imgData={imgData}
            itemProp="image"
            imgConfigs={IMG_DATA_PDP_THUMB.imgConfig}
            isProductImage
            disableVideoRender
            title={image.name}
            primaryBrand={primaryBrand || alternateBrand}
          />
          {isVideoUrl && (
            <Image
              className="thumbnail-play-icon"
              url={`${getImageFilePath()}/play-icon.svg`}
              alt="play icon"
              width="auto"
              primaryBrand={primaryBrand || alternateBrand}
            />
          )}
        </Anchor>
      </div>
    );
  }
}

Thumbnail.defaultProps = {
  totalCount: 1,
  isSelected: false,
  className: '',
  index: 0,
};

export default withStyles(Thumbnail, styles);
export { Thumbnail as ThumbnailVanilla };
