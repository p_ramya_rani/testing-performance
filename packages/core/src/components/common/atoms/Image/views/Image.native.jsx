// 9fbef606107a605d69c0edbcd8029e5d 
// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';
import FastImage from '../../FastImage';
// import { LazyloadImage } from 'react-native-lazyload-deux';
import withStyles from '../../../hoc/withStyles.native';
import style from '../Image.style';

/**
 * ImageComp returns two types of images
 * 1. Image from react-native
 * 2. LazyLoadImage - A image to be loaded only when it is visible on screen
 *                  - For an image to be lazy loaded, parent scrollview should be LazyLoadScrollView from react-native-lazyload-deux
 *                  - it needs "host" as props
 *                  - value of host prop should be same as parent LazyLoadScrollView
 */
const ImageComp = props => {
  const {
    url,
    source,
    host,
    alt,
    isFastImage,
    cacheType,
    priority,
    onLoadStart,
    onProgress,
    onError,
    resizeMode,
    width,
    height,
    ...otherProps
  } = props;
  const urlVal = url || '';
  const sourceVal = source || '';
  // const ImageComponent = host ? Image : Image;
  const ImageComponent = Image;
  if (sourceVal === '') {
    if (isFastImage) {
      return (
        <FastImage
          imageWidth={width}
          imageHeight={height}
          uriInfo={{ uri: urlVal }}
          cacheType={cacheType}
          priority={priority}
          onProgress={onProgress}
          onError={onError}
          resizeMode={resizeMode}
          accessibilityRole="image"
          accessibilityLabel={alt || ''}
        />
      );
    }
    return (
      <ImageComponent
        {...otherProps}
        host={host}
        accessibilityRole="image"
        accessibilityLabel={alt || ''}
        source={{ uri: urlVal }}
        resizeMode={resizeMode}
      />
    );
  }

  return (
    <ImageComponent
      {...otherProps}
      host={host}
      source={source}
      accessibilityRole="image"
      accessibilityLabel={alt || ''}
      resizeMode={resizeMode}
    />
  );
};

ImageComp.propTypes = {
  source: PropTypes.string,
  url: PropTypes.string,
  host: PropTypes.string,
  alt: PropTypes.string,
  priority: PropTypes.string,
  cacheType: PropTypes.string,
  onLoadStart: PropTypes.func,
  onProgress: PropTypes.func,
  onError: PropTypes.func,
  resizeMode: PropTypes.string,
  isFastImage: PropTypes.bool,
  width: PropTypes.string,
  height: PropTypes.string,
};

ImageComp.defaultProps = {
  source: '',
  url: '',
  host: '',
  alt: '',
  cacheType: 'immutable',
  priority: 'normal',
  resizeMode: 'contain',
  onLoadStart: () => {},
  onProgress: () => {},
  onError: () => {},
  isFastImage: false,
  width: '',
  height: '',
};

export default withStyles(ImageComp, style);
export { ImageComp as ImageCompVanilla };
