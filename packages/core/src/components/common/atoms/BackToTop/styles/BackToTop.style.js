// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const applyBrandSpecificBorder = props => {
  return props.theme.isGymboree
    ? props.theme.colorPalette.orange[800]
    : props.theme.colors.TEXT.DARKERBLUE;
};

export default css`
  width: 44px;
  height: 44px;
  border: none;
  background-color: ${props => props.theme.colorPalette.white};
  position: fixed;
  bottom: 298px;
  @media ${props => props.theme.mediaQuery.large} {
    bottom: 358px;
  }
  right: 0;
  z-index: 99;
  outline: none;
  cursor: pointer;
  transition-duration: 0.4s;
  border-radius: 10px 0 0 10px;
  box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.25);

  .scrollToTop__arrowBtn {
    width: 16.5px;
    height: 16.5px;
    border: solid ${props => applyBrandSpecificBorder(props)};
    border-width: 1px 0 0 1px;
    transform: rotate(45deg);
    margin: 10px auto 0;
    display: block;
  }

  &.scrollToTopBtn--show {
    display: block;
    animation: slide-in 0.5s forwards;

    @media print {
      display: none;
    }
  }

  &.scrollToTopBtn--hide {
    animation: slide-out 0.5s forwards;
  }

  @keyframes slide-in {
    0% {
      transform: translateX(100%);
    }
    100% {
      transform: translateX(0%);
    }
  }

  @keyframes slide-out {
    0% {
      transform: translateX(0%);
    }
    100% {
      transform: translateX(100%);
    }
  }
`;
