// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropType from 'prop-types';
import { TouchableOpacity, View } from 'react-native';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import Image from '../../Image';
import { isIOS } from '../../../../../utils/utils.app';
import CameraAccessPrompt from '../../../molecules/CameraAccess/container/Camera.container';

const qrScannerIconPath = require('../../../../../../../mobileapp/src/assets/images/qr_scanner.png');

class QRScannerIcon extends React.PureComponent {
  constructor() {
    super();
    this.state = { isOpen: false };
  }

  /**
   * @pageNaviagte : To manage to page navigate .
   */

  managePage = (navigation, closeModal, permission) => {
    request(permission).then(data => {
      if (data === 'granted' && navigation) {
        navigation.navigate('QRScanner');
      }
      if (closeModal) {
        closeModal();
      }
    });
  };

  /**
   * @managePermission : To manage the permission of Android and IOS.
   */

  managePermission = (permission, navigation, closeModal) => {
    check(permission)
      .then(result => {
        // eslint-disable-next-line default-case
        switch (result) {
          case RESULTS.UNAVAILABLE:
            break;
          case RESULTS.DENIED:
            this.managePage(navigation, closeModal, permission);
            break;
          case RESULTS.GRANTED:
            this.managePage(navigation, closeModal, permission);
            break;
          case RESULTS.BLOCKED:
            this.setState({ isOpen: true });
            break;
        }
      })
      .catch();
  };

  /**
   * @onQRIconFocus : To manage the permission according to OS .
   */
  onQRIconFocus = (navigation, closeModal) => {
    const permission = isIOS() ? PERMISSIONS.IOS.CAMERA : PERMISSIONS.ANDROID.CAMERA;
    this.managePermission(permission, navigation, closeModal);
  };

  /**
   * @toggleModal : To manage the modal state .
   */
  closeModal = () => {
    this.setState({
      isOpen: false,
    });
  };

  render() {
    const { navigation, closeModal, style, height, width } = this.props;
    const { isOpen } = this.state;
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.onQRIconFocus(navigation, closeModal)}
          accessibilityRole="button"
        >
          <Image
            height={height}
            width={width}
            source={qrScannerIconPath}
            accessibilityLabel="barcode"
            style={style}
            isButton
          />
        </TouchableOpacity>
        {isOpen && <CameraAccessPrompt closeModal={this.closeModal} />}
      </View>
    );
  }
}

QRScannerIcon.propTypes = {
  navigation: PropType.shape({}).isRequired,
  closeModal: PropType.func,
  style: PropType.shape({}),
  height: PropType.number,
  width: PropType.number,
};

QRScannerIcon.defaultProps = {
  closeModal: () => {},
  style: {},
  height: '',
  width: '',
};

export default QRScannerIcon;
