import React from 'react';
import { shallow } from 'enzyme';

import { ToggleSwitch } from '../../index';

describe('ToggleSwitch component', () => {
  it('should renders correctly', () => {
    const props = {
      name: 'test-filter',
      id: 'test-switch',
      checked: true,
      onChange: jest.fn(),
      theme: { colors: { REWARDS: { PLCC: '#00a7e0' } } },
    };
    const component = shallow(<ToggleSwitch {...props} />);
    expect(component).toMatchSnapshot();
  });
});
