import { css } from 'styled-components';

export default css`
  position: relative;
  width: 45px;
  display: inline-block;
  vertical-align: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  text-align: left;

  .toggle-switch-checkbox {
    display: none;
  }
  .toggle-switch-label {
    display: block;
    overflow: hidden;
    cursor: pointer;
    border-radius: 20px;
    margin: 0;
  }
  .toggle-switch-label:focus {
    outline: none;
  }
  .toggle-switch-label > span:focus {
    outline: none;
  }
  .toggle-switch-inner {
    display: block;
    width: 200%;
    margin-left: -100%;
    transition: margin 0.3s ease-in 0s;
  }
  .toggle-switch-inner:before,
  .toggle-switch-inner:after {
    display: block;
    float: left;
    width: 50%;
    height: 20px;
    padding: 0;
    line-height: 20px;
    font-size: 10px;
    font-weight: bold;
    box-sizing: border-box;
  }
  .toggle-switch-inner:before {
    content: attr(data-yes);
    text-transform: uppercase;
    padding-left: 5px;
    background-color: ${(props) =>
      props.switchColor ? props.switchColor : props.theme.colors.REWARDS.PLCC};
    color: #fff;
  }
  .toggle-switch-disabled {
    background-color: #ddd;
    cursor: not-allowed;
    opacity: 0.5;
  }
  .toggle-switch-disabled:before {
    background-color: #ddd;
    cursor: not-allowed;
  }
  .toggle-switch-inner:after {
    content: attr(data-no);
    text-transform: uppercase;
    padding-right: 3px;
    background-color: #fff;
    border: 1px solid #979797;
    border-radius: 20px;
    color: #000;
    text-align: right;
  }
  .toggle-switch-switch {
    display: block;
    width: 14px;
    height: 14px;
    margin: 3px;
    background: #000;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 25px;
    border: 0 solid #bbb;
    border-radius: 20px;
    transition: all 0.3s ease-in 0s;
  }
  .toggle-switch-checkbox:checked + .toggle-switch-label .toggle-switch-inner {
    margin-left: 0;
  }
  .toggle-switch-checkbox:checked + .toggle-switch-label .toggle-switch-switch {
    right: 0px;
    background: #fff;
  }
`;
