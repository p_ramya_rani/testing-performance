// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import HelpChat from '../views/HelpChat';

describe('ChatBotNative', () => {
  let component;

  beforeEach(() => {
    const props = {
      isUserLoggedIn: false,
      labels: {
        chatNow: 'CHAT NOW',
      },
      navigation: {},
    };
    component = shallow(<HelpChat {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
