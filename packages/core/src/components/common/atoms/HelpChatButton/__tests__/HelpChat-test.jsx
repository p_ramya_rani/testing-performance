// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { HelpChatVanilla } from '../views/HelpChat';

describe('Help Chat component', () => {
  const props = {
    className: '',
    toggleMenu: () => {},
    flyoutOpen: false,
  };
  it('Help Chat component renders correctly', () => {
    const component = shallow(<HelpChatVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});

describe('Help Chat component with closed flyout', () => {
  const props = {
    className: '',
    toggleMenu: () => {},
    flyoutOpen: true,
  };
  it('Help Chat component renders correctly', () => {
    const component = shallow(<HelpChatVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
