// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import Image from '@tcp/core/src/components/common/atoms/Image';

export const ChatButtonContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const ChatButton = styled.View`
  background-color: ${props => props.theme.colorPalette.blue.C900};
  width: 100%;
  height: 44px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const ChatImage = styled(Image)`
  margin-right: 8px;
`;

export default {
  ChatButtonContainer,
  ChatButton,
  ChatImage,
};
