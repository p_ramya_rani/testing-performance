// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  width: 70px;
  height: 70px;
  border-radius: 35px;
  box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.25);
  background-color: ${(props) => props.theme.colors.WHITE};
  position: fixed;
  bottom: ${(props) => (props.orderDetailsSticky ? '74px' : '8px')};
  @media ${(props) => props.theme.mediaQuery.medium} {
    bottom: 8px;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    bottom: 18px;
    right: 25px;
  }
  right: 25px;
  z-index: 99;
  cursor: pointer;
  border: none;
  outline: none;
  -webkit-animation: helpAnim 1s;
  animation: helpAnim 1s;
  -webkit-transform: scale(0);
  transform: scale(0);
  -webkit-animation-fill-mode: forwards;
  animation-fill-mode: forwards;
  transition-timing-function: cubic-bezier(0.25, 0.1, 0.46, 1.44);
  &.hide-button {
    -webkit-animation: hideHelpAnim 1s;
    animation: hideHelpAnim 1s;
  }
  &.help-btn-blue-color {
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
  }
  &.shift-up {
    bottom: 110px;
  }

  @-webkit-keyframes helpAnim {
    100% {
      -webkit-transform: scale(1);
    }
  }

  @keyframes helpAnim {
    100% {
      transform: scale(1);
    }
  }

  @-webkit-keyframes hideHelpAnim {
    100% {
      -webkit-transform: scale(0);
    }
  }

  @keyframes hideHelpAnim {
    100% {
      transform: scale(0);
    }
  }

  .help-image {
    width: 26px;
    height: 25px;
    object-fit: contain;
  }

  .dot {
    position: absolute;
    background-color: ${(props) => props.theme.colorPalette.red['500']};
    top: 0;
    right: 0;
    border-radius: 8px;
    margin: 1px 0px 0px -8px;
    padding: 2px 6px;
  }
`;
