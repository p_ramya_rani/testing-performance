// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import ModalNative from '@tcp/core/src/components/common/molecules/Modal/view/Modal.native';
import { getAPIConfig } from '@tcp/core/src/utils';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import ModalHeaderCurved from '@tcp/core/src/components/common/molecules/Modal/view/Modal.header.curved';
import InAppWebView from '../../../../../../../mobileapp/src/components/features/content/InAppWebView/views/InAppWebView.view';
import { ChatButtonContainer, ChatButton, ChatImage } from '../styles/HelpChat.style.native';
import names from '../../../../../constants/eventsName.constants';
import ClickTracker from '../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import { appendToken } from '../../../../../utils/utils';

/**
 * This component creates Mobile Chat button
 * test data
 *  const chatFormUrl = `http://local.childrensplace.com:8080/chatbot.html`;
 *  const chatBotWebViewUrl = 'http://local.childrensplace.com:8080/chatbot.html?version=1a';
 *
 */

const chatButtonIcon = require('../../../../../../../mobileapp/src/assets/images/btn-help.png');

const overrideCSS = {
  imageWrapper: `
    left: 92%;
    top: 22px;
    left: 91%;
    z-index: 1px;
    `,
  styledTouchableOpacity: `padding: 0;`,
  styledCrossImage: `
    margin: 0;
    width: 20px;
    height: 20px;
    `,
  topBar: `margin: 6px auto 2px auto;border-width:0`,
};

const HelpChat = props => {
  const { isUserLoggedIn, labels, navigation } = props;

  const [chatForm, setChatForm] = useState(false);
  const [sessionInfo, setSessionInfo] = useState('');

  const getSessionInfo = async () => {
    let sessionCookieData = '';
    if (isUserLoggedIn) {
      sessionCookieData = await readCookie('tcpSessionInfo');
    }
    return sessionCookieData;
  };

  const setSessionData = async () => {
    const sessionData = await getSessionInfo();
    setSessionInfo(sessionData);
  };

  useEffect(() => {
    setSessionData();
  }, [isUserLoggedIn]);

  const { chatBotVersion, chatBotEnv, brandId, chatBotPageUrl } = getAPIConfig();

  const openChatWindow = () => {
    setChatForm(true);
  };

  const closeChatWindow = () => {
    setChatForm(false);
  };

  const chatBotWebViewUrl = `${chatBotPageUrl}?${
    appendToken() ? 'token=tcprwd&' : ''
  }loggedIn=${isUserLoggedIn}&version=${chatBotVersion}&user=${sessionInfo}&env=${chatBotEnv}&brand=${brandId}`;

  const dimensions = {
    width: '26px',
    height: '25px',
  };

  return (
    <>
      <ChatButtonContainer>
        <ClickTracker
          as={TouchableOpacity}
          name={names.screenNames.chatBot_launch_e123}
          clickData={{
            customEvents: ['event123'],
          }}
          module="account"
          onPress={openChatWindow}
          accessibilityLabel={labels.chatNow}
          accessibilityRole="button"
        >
          <ChatButton>
            <ChatImage source={chatButtonIcon} {...dimensions} />
            <BodyCopy fontFamily="secondary" fontSize="fs14" text={labels.chatNow} color="white" />
          </ChatButton>
        </ClickTracker>
      </ChatButtonContainer>

      {chatForm ? (
        <ModalNative
          isOpen={chatForm}
          onRequestClose={closeChatWindow}
          headingFontFamily="secondary"
          roundedModal
          clickOutsideToClose
          fontSize="fs16"
          topMargin="100px"
          overrideCSS={{
            modalCurvedContent: `
            border-top-left-radius: 12;
            border-top-right-radius: 12;
            `,
          }}
        >
          <ModalHeaderCurved onRequestClose={closeChatWindow} overrideCSS={overrideCSS} />

          <InAppWebView pageUrl={chatBotWebViewUrl} navigation={navigation} />
        </ModalNative>
      ) : null}
    </>
  );
};

HelpChat.propTypes = {
  isUserLoggedIn: PropTypes.bool,
  labels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

HelpChat.defaultProps = {
  isUserLoggedIn: false,
};

export default HelpChat;
