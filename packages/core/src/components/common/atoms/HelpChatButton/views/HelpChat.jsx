// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath, isClient } from '@tcp/core/src/utils';
import { Image, BodyCopy } from '@tcp/core/src/components/common/atoms';
import style from '../styles/HelpChat.style';

const HelpChat = ({
  className,
  toggleMenu,
  flyoutOpen,
  helpMenuLabels = {},
  stickyFooter,
  isChatBotOpen,
  hideHelp,
  chatWindowMinimized,
  unreadMessage,
  liveChatActive,
}) => {
  const { helpText } = helpMenuLabels;
  const liveChatActiveValue = !!(liveChatActive && chatWindowMinimized);
  if (isClient()) {
    const helpImgPath = getIconPath('help-chat_sm');
    const helpWhiteImgPath = getIconPath('help-chat_white');
    // Since the help button should appear based on the pega chatbot status, this cannot be rendered on the server
    return (
      <button
        className={`${className} button-helpchat ${stickyFooter ? 'shift-up' : ''} ${
          hideHelp || isChatBotOpen || flyoutOpen ? 'hide-button' : ''
        } ${liveChatActiveValue ? 'help-btn-blue-color' : ''}`}
        aria-label={helpText}
        onClick={toggleMenu}
      >
        {!!unreadMessage && (
          <BodyCopy
            className="dot"
            fontSize="fs10"
            component="span"
            color="white"
            fontFamily="secondary"
            fontWeight="extrabold"
            textAlign="center"
          >
            {unreadMessage}
          </BodyCopy>
        )}
        <Image
          src={`${liveChatActiveValue ? helpWhiteImgPath : helpImgPath}`}
          alt={helpText}
          className="help-image"
        />
        <BodyCopy
          className="help-text"
          fontSize="fs14"
          component="p"
          color={`${liveChatActiveValue ? 'white' : 'blue.C900'}`}
          fontFamily="secondary"
          fontWeight="extrabold"
          textAlign="center"
        >
          {helpText}
        </BodyCopy>
      </button>
    );
  }
  return null;
};

HelpChat.propTypes = {
  toggleMenu: PropTypes.func,
  flyoutOpen: PropTypes.bool,
  className: PropTypes.string,
  helpMenuLabels: PropTypes.shape({}),
  stickyFooter: PropTypes.bool,
  isChatBotOpen: PropTypes.bool,
  hideHelp: PropTypes.bool,
  chatWindowMinimized: PropTypes.bool,
  unreadMessage: PropTypes.number,
  liveChatActive: PropTypes.bool,
};

HelpChat.defaultProps = {
  toggleMenu: null,
  flyoutOpen: false,
  className: '',
  helpMenuLabels: {},
  stickyFooter: false,
  isChatBotOpen: false,
  hideHelp: false,
  chatWindowMinimized: false,
  unreadMessage: 0,
  liveChatActive: false,
};

export default withStyles(HelpChat, style);
export { HelpChat as HelpChatVanilla };
