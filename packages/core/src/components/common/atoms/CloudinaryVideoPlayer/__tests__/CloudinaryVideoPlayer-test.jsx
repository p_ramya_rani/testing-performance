// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import * as utils from '@tcp/core/src/utils/utils';
import { CloudinaryVideoPlayerVanilla } from '../CloudinaryVideoPlayer';

const dummydomain = 'http://example.com';
const utilsLocation = '@tcp/core/src/utils/utils';

beforeEach(() => {
  jest.mock(utilsLocation);
  const apiConfig = {
    brandId: 'tcp',
    productAssetPathTCP: 'some/sample/path',
    assetHostTCP: dummydomain,
  };
  utils.getAPIConfig = jest.fn(() => {
    return apiConfig;
  });
  utils.getViewportInfo = jest.fn(() => {
    return {
      isDesktop: true,
      isMobile: false,
      isTablet: false,
    };
  });
});

const videoData = {
  poster: '',
  muted: '1',
  loop: '1',
  autoplay: '1',
  controls: '1',
  title: 'Video',
  poster_url: '/test/dummy.jpg',
  poster_url_m: '/test/dummy_mb.jpg',
  poster_url_t: '',
};
describe('testing block for CloudinaryVideoPlayer', () => {
  it('CloudinaryVideoPlayer should be rendered correctly ', () => {
    const component = shallow(<CloudinaryVideoPlayerVanilla videoData={videoData} isModule />);
    expect(component).toBeDefined();
  });
});
