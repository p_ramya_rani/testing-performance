// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState, useRef } from 'react';
import { PropTypes } from 'prop-types';
import { Cloudinary } from 'cloudinary-core';
import 'cloudinary-video-player/dist/cld-video-player.min';
import {
  parseBoolean,
  getIconPath,
  getImageFilePath,
  getViewportInfo,
  getAPIConfig,
} from '@tcp/core/src/utils';
import { getVideoUrls, findPosterImage, getPosterUrls } from '../DamImage/views/DamImage';
import { StyledVideo, getVideoStyle } from '../DamImage/DamImage.styles';
import { CloudinaryVideoPlayerGlobalStyle } from './CloudinaryVideoPlayer.style';

const getUniqueID = () => {
  return `video_${Date.now() + (Math.random() * 100000).toFixed()}`;
};

const getPlayIcon = (isModule) =>
  isModule ? `${getImageFilePath()}/play-accessible.svg` : getIconPath('icon-play');

const getPauseIcon = (isModule) =>
  isModule ? `${getImageFilePath()}/pause-accessible.svg` : getIconPath('icon-pause');

const getMuteIcon = (isModule) => isModule && `${getImageFilePath()}/mute.svg`;

const getUnmuteIcon = (isModule) => isModule && `${getImageFilePath()}/unmute.svg`;

const RenderMuteIcon = (playing, toggleMute, muteIconPath) => {
  return (
    playing && (
      <button
        className="video-mute-btn"
        type="button"
        title="Mute Video"
        aria-disabled="false"
        onClick={toggleMute}
      >
        <img className="mute_button_icon" alt="" aria-hidden="true" src={muteIconPath} />
      </button>
    )
  );
};

const RenderPauseButton = (playing, toggleVideo, iconPath) => {
  return (
    playing && (
      <button
        className="video-play-button"
        type="button"
        title="Pause Video"
        aria-disabled="false"
        onClick={toggleVideo}
      >
        <img
          className="tcp_carousel__play_pause_button_icon"
          alt=""
          aria-hidden="true"
          src={iconPath}
        />
      </button>
    )
  );
};

const RenderBigPlayButton = (playing, toggleVideo, playIcon) => {
  return (
    !playing && (
      <button
        className="big-video-play-btn"
        type="button"
        title="Play Video"
        aria-disabled="false"
        onClick={toggleVideo}
      >
        <img
          className="tcp_carousel__big_play_button_icon"
          alt=""
          aria-hidden="true"
          src={playIcon}
        />
      </button>
    )
  );
};

const callToggleMute = ({
  isMuted,
  vPlayer,
  setIsMuted,
  setMuteIconPath,
  muteIcon,
  unmuteIcon,
}) => {
  if (isMuted === true) {
    // eslint-disable-next-line no-param-reassign
    vPlayer.unmute();
    setIsMuted(false);
    setMuteIconPath(unmuteIcon);
  } else {
    // eslint-disable-next-line no-param-reassign
    vPlayer.mute();
    setIsMuted(true);
    setMuteIconPath(muteIcon);
  }
};

const getDamCloudName = (apiConfig) => {
  const { damCloudName } = apiConfig;
  return damCloudName;
};

const getSecureDistribution = (apiConfig) => {
  const { assetHostTCP } = apiConfig;
  return assetHostTCP?.split('/')[2];
};

// eslint-disable-next-line sonarjs/cognitive-complexity
function CloudinaryVideoPlayer({ videoData, isModule }) {
  const playIcon = getPlayIcon(isModule);
  const pauseIcon = getPauseIcon(isModule);
  const muteIcon = getMuteIcon(isModule);
  const unmuteIcon = getUnmuteIcon(isModule);
  const videoUrls = getVideoUrls(videoData);
  const posterUrls = getPosterUrls(videoData);
  const {
    autoplay = true,
    playsinline: playsInline = true,
    muted = true,
    controls = false,
    loop = true,
    title = 'Video',
  } = videoData;
  const defaultMuteIcon = parseBoolean(muted) ? muteIcon : unmuteIcon;
  const defaultAutoPlay = parseBoolean(autoplay) && parseBoolean(muted);
  const apiConfig = getAPIConfig();

  const [uniqueId] = useState(getUniqueID());
  const [source, setSource] = useState(videoUrls?.desktop);
  const [poster, setPoster] = useState(findPosterImage(posterUrls, videoUrls, 'desktop'));
  const [playing, setPlaying] = useState(defaultAutoPlay);
  const [isMuted, setIsMuted] = useState(parseBoolean(muted));
  const [muteIconPath, setMuteIconPath] = useState(defaultMuteIcon);
  const [iconPath] = useState(pauseIcon);
  const [vPlayer, setVplayer] = useState();

  const videoRef = useRef(null);

  const cloudinary = new Cloudinary({
    cloud_name: getDamCloudName(apiConfig),
    secure: true,
    private_cdn: true,
    secure_distribution: getSecureDistribution(apiConfig),
  });

  const videoPlayerInit = () => {
    const player = cloudinary.videoPlayer(videoRef?.current, {
      fluid: true,
      muted: parseBoolean(muted),
      autoplayMode: parseBoolean(autoplay) ? 'on-scroll' : 'never',
      loop: parseBoolean(loop),
      controls: parseBoolean(controls),
      preload: 'none',
      bigPlayButton: false,
      showLogo: false,
      posterOptions: { publicId: poster || '' },
    });

    player.source(source, {
      sourceTypes: ['hls'],
      transformation: { streaming_profile: 'full_hd' },
    });
    setVplayer(player);
  };

  const toggleVideo = () => {
    if (playing === true) {
      vPlayer.pause();
      setPlaying(false);
    } else {
      vPlayer.play();
      setPlaying(true);
    }
  };

  const toggleMute = () => {
    callToggleMute({ isMuted, vPlayer, setIsMuted, setMuteIconPath, muteIcon, unmuteIcon });
  };

  useEffect(() => {
    const { isDesktop, isTablet, isMobile } = getViewportInfo();
    if (isDesktop && videoUrls.desktop) {
      setSource(videoUrls.desktop);
      setPoster(findPosterImage(posterUrls, videoUrls, 'desktop'));
    }
    if (isTablet && videoUrls.tablet) {
      setSource(videoUrls.tablet);
      setPoster(findPosterImage(posterUrls, videoUrls, 'tablet'));
    }
    if (isMobile && videoUrls.mobile) {
      setSource(videoUrls.mobile);
      setPoster(findPosterImage(posterUrls, videoUrls, 'mobile'));
    }
  }, [videoData]);

  useEffect(() => {
    videoPlayerInit();
  }, [source]);

  return (
    <StyledVideo isModule={isModule} overrideStyle={getVideoStyle(videoData)}>
      <CloudinaryVideoPlayerGlobalStyle />
      <video
        className={uniqueId}
        playsInline={parseBoolean(playsInline)}
        title={title}
        ref={videoRef}
      >
        <track kind="captions" />
      </video>
      {RenderBigPlayButton(playing, toggleVideo, playIcon)}
      {RenderPauseButton(playing, toggleVideo, iconPath)}
      {RenderMuteIcon(playing, toggleMute, muteIconPath)}
    </StyledVideo>
  );
}

CloudinaryVideoPlayer.propTypes = {
  videoData: PropTypes.shape({
    autoplay: PropTypes.string,
    muted: PropTypes.string,
    controls: PropTypes.string,
    playsinline: PropTypes.string,
    loop: PropTypes.string,
    title: PropTypes.string,
  }),
  isModule: PropTypes.bool.isRequired,
};

CloudinaryVideoPlayer.defaultProps = {
  videoData: {
    autoplay: '1',
    muted: '1',
    controls: '1',
    loop: '1',
    title: 'Video',
    playsinline: '1',
  },
};

export default CloudinaryVideoPlayer;
export { CloudinaryVideoPlayer as CloudinaryVideoPlayerVanilla };
