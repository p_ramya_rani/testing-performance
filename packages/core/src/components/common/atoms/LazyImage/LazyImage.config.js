// 9fbef606107a605d69c0edbcd8029e5d 
import { ELEMENTS_CLASS } from './LazyImage.constants';

const lazyloadConfig = {
  lazyloadConfig: 200,
  elements_selector: `.${ELEMENTS_CLASS}`,
};

export default lazyloadConfig;
