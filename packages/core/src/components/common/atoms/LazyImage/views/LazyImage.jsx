// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import LazyLoad from 'vanilla-lazyload';
import { PropTypes } from 'prop-types';
import { isClient } from '../../../../../utils/index';
import lazyloadConfig from '../LazyImage.config';
import { ELEMENTS_CLASS } from '../LazyImage.constants';
import { StyledImage, getImageStyle } from './LazyImage.styles';

// Only initialize it one time on client side for the entire application
if (isClient() && !document.lazyLoadInstance) {
  document.lazyLoadInstance = new LazyLoad(lazyloadConfig);
}

export class LazyImage extends React.Component {
  constructor() {
    super();
    this.state = { isImgLoaded: false };
    this.setImgLoaded = this.setImgLoaded.bind(this);
  }

  // Update lazyLoad after first rendering of every image
  componentDidMount() {
    document.lazyLoadInstance.update();
  }

  // Update lazyLoad after rerendering of every image
  componentDidUpdate() {
    document.lazyLoadInstance.update();
  }

  setImgLoaded() {
    this.setState({ isImgLoaded: true });
  }

  getFilterLgSrcSet = (isImgLoaded, lgSrcSet) => {
    return isImgLoaded ? lgSrcSet.replace(/q_10/gi, 'q_auto') : lgSrcSet;
  };

  getFilterSmSrcSet = (isImgLoaded, smSrcSet) => {
    return isImgLoaded ? smSrcSet.replace(/q_10/gi, 'q_auto') : smSrcSet;
  };

  getFilterXsSrcSet = (isImgLoaded, xsSrcSet) => {
    return isImgLoaded ? xsSrcSet.replace(/q_10/gi, 'q_auto') : xsSrcSet;
  };

  getFilterSrcSet = (isImgLoaded, src) => {
    return isImgLoaded ? src.replace(/q_10/gi, 'q_auto') : src;
  };

  getOverrideStyle = imgData => {
    return imgData && getImageStyle(imgData);
  };

  // Just render the image with data-src
  render() {
    const { isImgLoaded } = this.state;
    const {
      alt,
      src,
      srcset,
      sizes,
      className,
      showPlaceHolder,
      forwardedRef,
      breakpoints,
      lgSrcSet,
      smSrcSet,
      xsSrcSet,
      imgProps,
      ...otherProps
    } = this.props;
    const { imgData } = imgProps;
    const filterlgSrcSet = this.getFilterLgSrcSet(isImgLoaded, lgSrcSet);
    const filtersmSrcSet = this.getFilterSmSrcSet(isImgLoaded, smSrcSet);
    const filterxsSrcSet = this.getFilterXsSrcSet(isImgLoaded, xsSrcSet);
    const filterSrc = this.getFilterSrcSet(isImgLoaded, src);

    return (
      <picture>
        <source
          media={`(min-width: ${breakpoints.values.lg}px)`}
          srcSet={showPlaceHolder ? filterlgSrcSet : ''}
          data-srcset={filterlgSrcSet}
        />
        <source
          media={`(min-width: ${breakpoints.values.sm}px)`}
          srcSet={showPlaceHolder ? filtersmSrcSet : ''}
          data-srcset={filtersmSrcSet}
        />
        <source
          media={`(min-width: ${breakpoints.values.xs}px)`}
          srcSet={showPlaceHolder ? filterxsSrcSet : ''}
          data-srcset={filterxsSrcSet}
        />
        <StyledImage
          alt={alt}
          className={`${ELEMENTS_CLASS} ${className} ${
            !isImgLoaded && showPlaceHolder ? 'img-placeholder' : 'image-new-placeholder'
          }`}
          src={showPlaceHolder ? filterSrc : ''}
          data-src={filterSrc}
          data-srcset={srcset}
          data-sizes={sizes}
          ref={forwardedRef}
          onLoad={this.setImgLoaded}
          overrideStyle={this.getOverrideStyle(imgData)}
          {...otherProps}
        />
      </picture>
    );
  }
}

LazyImage.propTypes = {
  src: PropTypes.string.isRequired,
  srcset: PropTypes.string,
  sizes: PropTypes.string,
  className: PropTypes.string,
  alt: PropTypes.string.isRequired,
  // We need this because React.forwardRef doesn't work with class components
  forwardedRef: PropTypes.shape({ current: PropTypes.any }),
  showPlaceHolder: PropTypes.bool.isRequired,
  breakpoints: PropTypes.shape({}).isRequired,
  imgProps: PropTypes.shape({}).isRequired,
  lgSrcSet: PropTypes.string.isRequired,
  smSrcSet: PropTypes.string.isRequired,
  xsSrcSet: PropTypes.string.isRequired,
};

LazyImage.defaultProps = {
  srcset: '',
  sizes: '',
  className: '',
  forwardedRef: null,
};

export default LazyImage;
