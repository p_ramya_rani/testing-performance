// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import { mediaQuery } from '@tcp/core/styles/themes/TCP/mediaQuery';

export const StyledImage = styled.img`
  ${props => props.overrideStyle || ''}
`;

export const getImageStyle = imgData => {
  return `
    ${imgData.width_m ? `width: ${imgData.width_m}px !important;` : ''}
    ${imgData.height_m ? `height: ${imgData.height_m}px !important;` : ''}
    @media ${mediaQuery && mediaQuery.medium} {
      ${imgData.width_t ? `width: ${imgData.width_t}px !important;` : ''}
      ${imgData.height_t ? `height: ${imgData.height_t}px !important;` : ''}

    }
    @media ${mediaQuery && mediaQuery.large} {
      ${imgData.width_d ? `width: ${imgData.width_d}px !important;` : ''}
      ${imgData.height_d ? `height: ${imgData.height_d}px !important;` : ''}
    }
  `;
};
