// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  &.blue-underline {
    border-bottom: 2px solid ${(props) => props.theme.colorPalette.blue[500]};
    width: 40px;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
`;
export default styles;
