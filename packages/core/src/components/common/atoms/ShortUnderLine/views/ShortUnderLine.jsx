// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import PropTypes from 'prop-types';
import styles from '../styles/ShortUnderLine.styles';

class ShortUnderLine extends React.PureComponent {
  render() {
    const { className } = this.props;
    return <div className={`${className} blue-underline`} />;
  }
}

ShortUnderLine.propTypes = {
  className: PropTypes.string,
};
ShortUnderLine.defaultProps = {
  className: '',
};
export default withStyles(ShortUnderLine, styles);
export { ShortUnderLine as ShortUnderLineVanilla };
