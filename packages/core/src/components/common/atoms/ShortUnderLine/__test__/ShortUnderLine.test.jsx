// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ShortUnderLineVanilla } from '../views/ShortUnderLine';

describe('blue-line', () => {
  const props = {};
  it('should render correctly', () => {
    const component = shallow(<ShortUnderLineVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
