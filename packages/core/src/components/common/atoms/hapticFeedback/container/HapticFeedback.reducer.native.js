import { HAPTICFEEDBACK_CONSTANTS } from '../HapticFeedback.constants';

const initialState = {
  eventType: null,
};

const HapticFeedbackReducer = (state = initialState, action) => {
  switch (action.type) {
    case HAPTICFEEDBACK_CONSTANTS.PLAY_HAPTIC_FEEDBACK:
      return {
        ...state,
        eventType: action.payload,
      };

    case HAPTICFEEDBACK_CONSTANTS.RESET_HAPTIC_FEEDBACK:
      return {
        ...state,
        eventType: null,
      };

    default:
      return {
        ...state,
      };
  }
};

export default HapticFeedbackReducer;
