import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { resetHapticFeedback } from './HapticFeedback.actions.native';
import { getEventType } from './HapticFeedback.selectors';
import { getIsHapticFeedbackEnabled } from '../../../../../reduxStore/selectors/session.selectors';

let triggerCount = 0;
export class HapticFeedback extends React.PureComponent {
  componentDidUpdate() {
    const { eventType, resetHapticEvent } = this.props;
    const isHapticFeedbackEnabled = getIsHapticFeedbackEnabled();

    if (isHapticFeedbackEnabled) {
      if (eventType != null && !triggerCount) {
        triggerCount += 1;
        const options = {
          enableVibrateFallback: true,
          ignoreAndroidSystemSettings: true,
        };
        ReactNativeHapticFeedback.trigger(eventType, options);
        resetHapticEvent(null);
      }
      if (eventType == null) {
        triggerCount = 0;
      }
    }
  }

  render() {
    return null;
  }
}

export const mapStateToProps = (state) => {
  return {
    eventType: getEventType(state),
  };
};

HapticFeedback.propTypes = {
  eventType: PropTypes.string,
  resetHapticEvent: PropTypes.func,
};

HapticFeedback.defaultProps = {
  eventType: null,
  resetHapticEvent: () => {},
};

export const mapDispatchToProps = (dispatch) => {
  return {
    resetHapticEvent: (payload) => {
      dispatch(resetHapticFeedback(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HapticFeedback);
