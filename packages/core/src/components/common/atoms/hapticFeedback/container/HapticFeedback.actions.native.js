import { HAPTICFEEDBACK_CONSTANTS } from '../HapticFeedback.constants';

export const playHapticFeedback = (payload) => ({
  type: HAPTICFEEDBACK_CONSTANTS.PLAY_HAPTIC_FEEDBACK,
  payload,
});

export const resetHapticFeedback = (payload) => ({
  type: HAPTICFEEDBACK_CONSTANTS.RESET_HAPTIC_FEEDBACK,
  payload,
});
