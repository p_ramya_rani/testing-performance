// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { WebView } from 'react-native-webview';
import { Dimensions, View } from 'react-native';
import { RenderTree, ComponentMap } from '@fabulas/astly';
import { PropTypes } from 'prop-types';
import { handleNavigationUrl, isIOS } from '@tcp/core/src/utils/utils.app';
import RNPrint from 'react-native-print';
import generateComponentMap from '../ComponentMap.native';
import { getSiteId } from '../../../../../utils';
import { getFontsURL } from '../../../../../utils/utils';

const onLoadEndScript = `(function() {
  const height = document.body.scrollHeight;
  window.ReactNativeWebView.postMessage(JSON.stringify({ height }));
})();`;

const webViewFontFaces = `
@font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 400;
    src: local('Montserrat Regular'), local('Montserrat-Regular'),
      url(${getFontsURL('fonts/Montserrat-regular.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-regular.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 500;
    src: local('Montserrat Medium'), local('Montserrat-Medium'),
      url(${getFontsURL('fonts/Montserrat-500.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-500.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 600;
    src: local('Montserrat SemiBold'), local('Montserrat-SemiBold'),
      url(${getFontsURL('fonts/Montserrat-600.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-600.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 700;
    src: local('Montserrat Bold'), local('Montserrat-Bold'),
      url(${getFontsURL('fonts/Montserrat-700.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-700.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 800;
    src: local('Montserrat ExtraBold'), local('Montserrat-ExtraBold'),
      url(${getFontsURL('fonts/Montserrat-800.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-800.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 900;
    src: local('Montserrat Black'), local('Montserrat-Black'),
      url(${getFontsURL('fonts/Montserrat-900.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Montserrat-900.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 400;
    src: local('Nunito Regular'), local('Nunito-Regular'),
      url(${getFontsURL('fonts/Nunito-regular.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-regular.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 600;
    src: local('Nunito SemiBold'), local('Nunito-SemiBold'),
      url(${getFontsURL('fonts/Nunito-600.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-600.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 700;
    src: local('Nunito Bold'), local('Nunito-Bold'),
      url(${getFontsURL('fonts/Nunito-700.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-700.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 800;
    src: local('Nunito ExtraBold'), local('Nunito-ExtraBold'),
      url(${getFontsURL('fonts/Nunito-800.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-800.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 900;
    src: local('Nunito Black'), local('Nunito-Black'),
      url(${getFontsURL('fonts/Nunito-900.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Nunito-900.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoCond';
    font-style: normal;
    font-weight: 400;
    src: local('TofinoCond Regular'), local('TofinoCond-Regular'),
      url(${getFontsURL('fonts/TofinoCond-Regular.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoCond-Regular.woff')}) format('woff');
    font-display: swap;
  }

  @font-face {
    font-family: 'TofinoCond';
    font-style: normal;
    font-weight: 500;
    src: local('TofinoCond Medium'), local('TofinoCond-Medium'),
      url(${getFontsURL('fonts/TofinoCond-Medium.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoCond-Medium.woff')}) format('woff');
    font-display: swap;
  }

  @font-face {
    font-family: 'TofinoCond';
    font-style: normal;
    font-weight: 600;
    src: local('TofinoCond Semibold'), local('TofinoCond-Semibold'),
      url(${getFontsURL('fonts/TofinoCond-Semibold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoCond-Semibold.woff')}) format('woff');
    font-display: swap;
  }

  @font-face {
    font-family: 'TofinoCond';
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: local('TofinoCond Bold'), local('TofinoCond-Bold'),
      url(${getFontsURL('fonts/TofinoCond-Bold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoCond-Bold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: local('TofinoWide Regular'), local('TofinoWide-Regular'),
      url(${getFontsURL('fonts/TofinoWide-Regular.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Regular.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 500;
    font-display: swap;
    src: local('TofinoWide Medium'), local('TofinoWide-Medium'),
      url(${getFontsURL('fonts/TofinoWide-Medium.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Medium.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 600;
    font-display: swap;
    src: local('TofinoWide Semibold'), local('TofinoWide-Semibold'),
      url(${getFontsURL('fonts/TofinoWide-Semibold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Semibold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: local('TofinoWide Bold'), local('TofinoWide-Bold'),
      url(${getFontsURL('fonts/TofinoWide-Bold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Bold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TofinoWide';
    font-style: normal;
    font-weight: 900;
    font-display: swap;
    src: local('TofinoWide Black'), local('TofinoWide-Black'),
      url(${getFontsURL('fonts/TofinoWide-Black.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TofinoWide-Black.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Tofino';
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: local('Tofino Regular'), local('Tofino-Regular'),
      url(${getFontsURL('fonts/Tofino-Regular.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Tofino-Regular.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Tofino';
    font-style: normal;
    font-weight: 500;
    font-display: swap;
    src: local('Tofino Medium'), local('Tofino-Medium'),
      url(${getFontsURL('fonts/Tofino-Medium.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Tofino-Medium.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Tofino';
    font-style: normal;
    font-weight: 600;
    font-display: swap;
    src: local('Tofino Semibold'), local('Tofino-Semibold'),
      url(${getFontsURL('fonts/Tofino-Semibold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Tofino-Semibold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Tofino';
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: local('Tofino Bold'), local('Tofino-Bold'),
      url(${getFontsURL('fonts/Tofino-Bold.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Tofino-Bold.woff')}) format('woff');
  }

  @font-face {
    font-family: 'Tofino';
    font-style: normal;
    font-weight: 900;
    font-display: swap;
    src: local('Tofino Black'), local('Tofino-Black'),
      url(${getFontsURL('fonts/Tofino-Black.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/Tofino-Black.woff')}) format('woff');
  }

  @font-face {
    font-family: 'TCPSansRegular-Normal';
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: local('TCPSansRegular-Normal'), local('TCPSansRegular-Normal'),
      url(${getFontsURL('fonts/TCPSansRegular-Normal.woff2')}) format('woff2'),
      url(${getFontsURL('fonts/TCPSansRegular-Normal.woff')}) format('woff');
  }
  `;

/**
 * @param {object} props : Props for RichText
 * @desc This is a RichText component.
 * 1. Loads static HTML or a URI (with optional headers) in the WebView.
 * Note that static HTML will require setting originWhitelist to ["*"]
 */

class RichText extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      webViewHeight: 0,
    };
    this.isPrintReceiptOpen = false;
    this.locationRef = null;
  }

  printHTML = async () => {
    await RNPrint.print({
      html: this.props.source,
    });
    this.isPrintReceiptOpen = false;
  };

  onMessage = (event) => {
    const { navigation, fromEspot, actionHandler } = this.props;
    const { height, href, target, dataTarget, text } = JSON.parse(event.nativeEvent.data);
    if (height) {
      this.setState({ webViewHeight: parseInt(height, 10) });
    }

    if ((href || dataTarget) && fromEspot && actionHandler) {
      return actionHandler(href, target, dataTarget);
    }

    if (href) {
      return handleNavigationUrl(navigation, href, target);
    }

    if (text && text === 'print a copy' && !this.isPrintReceiptOpen) {
      this.isPrintReceiptOpen = true;
      return this.printHTML();
    }
    return null;
  };

  renderWebView = () => {
    const {
      javaScriptEnabled,
      domStorageEnabled,
      thirdPartyCookiesEnabled,
      isApplyDeviceHeight,
      source,
      scrollEnabled,
      ...others
    } = this.props;
    const { webViewHeight } = this.state;
    const html = typeof source === 'string' ? source : source.html;
    const screenHeight = Math.round(Dimensions.get('window').height);
    /**
     * removed extra margin from left
     */
    const style = {
      backgroundColor: 'transparent',
      height: webViewHeight,
    };
    const styleWithHeight = { backgroundColor: 'transparent', height: screenHeight };
    const siteId = getSiteId();
    const webViewSource =
      source && source.uri
        ? {
            uri: source.uri,
          }
        : {
            html: `<html>
              <head>
                <meta charset="utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
                <link rel="stylesheet" type="text/css" href="https://www.gymboree.com/rwd/css/bootstrap-grid-custom-v3.css">
                <link rel="stylesheet" type="text/css" href="https://www.gymboree.com/rwd/css/modulex_app.css">

                <style>
                  ${webViewFontFaces}

                  html, body {
                    margin: 0;
                    padding: 0;
                  }

                  body {
                    font-family: Montserrat, Arial, Helvetica, sans-serif;
                    color: #1A1A1A;
                  }
                </style>
              </head>
                <body>
                  ${html}
                  <script>
                    const handleAnchorClicks = () => {
                      document.querySelectorAll('a').forEach(a => {
                        a.onclick = event => {
                          event.preventDefault();
                          const { target,text } = a;
                          let href = a.getAttribute('href');
                          const dataTarget = a.getAttribute('data-target');
                          const {isrelative} = event.target.dataset;
                          if(isrelative){
                            href = '/${siteId}'+href;
                          }
                          window.ReactNativeWebView.postMessage(JSON.stringify({ href, target, dataTarget,text }));
                        }
                      });
                    };

                    window.onload = function () {
                      handleAnchorClicks();
                      /* Because required font face is taking time to render and the height
                        is not being calculated correctly. We might need to find proper
                        solution later. */
                      setTimeout(() => {
                        setWebViewHeight();
                        }, 600);

                    }
                  </script>
                </body>
            </html>`,
          };
    return (
      <WebView
        androidHardwareAccelerationDisabled={true}
        ref={(instance) => {
          this.locationRef = instance;
        }}
        scalesPageToFit={false}
        originWhitelist={['*']}
        mixedContentMode="always"
        useWebKit={isIOS()}
        startInLoadingState
        style={isApplyDeviceHeight ? styleWithHeight : style}
        scrollEnabled={scrollEnabled}
        javaScriptEnabled
        domStorageEnabled
        thirdPartyCookiesEnabled
        allowsInlineMediaPlayback="true"
        onMessage={this.onMessage}
        source={webViewSource}
        onLoadEnd={() => {
          this.locationRef.injectJavaScript(onLoadEndScript);
        }}
        {...others}
      />
    );
  };

  handleNativeNavigation = (node) => {
    const { actionHandler } = this.props;
    if (node.properties && node.properties.dataTarget) {
      actionHandler(node.properties.dataTarget);
    }
  };

  renderNativeView = () => {
    const { source, navigation, actionHandler } = this.props;
    return (
      <View>
        <RenderTree
          tree={`<div>${source}</div>`}
          tools={{ navigate: this.handleNativeNavigation }}
          componentMap={{
            ...ComponentMap,
            ...generateComponentMap(navigation, actionHandler),
          }}
        />
      </View>
    );
  };

  render() {
    const { isNativeView } = this.props;
    return isNativeView ? this.renderNativeView() : this.renderWebView();
  }
}

RichText.propTypes = {
  isNativeView: PropTypes.bool,
  source: PropTypes.string,
  javaScriptEnabled: PropTypes.bool,
  domStorageEnabled: PropTypes.bool,
  thirdPartyCookiesEnabled: PropTypes.bool,
  isApplyDeviceHeight: PropTypes.bool,
  actionHandler: PropTypes.func,
  navigation: PropTypes.shape({}).isRequired,
  scrollEnabled: PropTypes.bool,
  fromEspot: PropTypes.bool,
};

RichText.defaultProps = {
  isNativeView: false,
  source: '',
  javaScriptEnabled: false,
  domStorageEnabled: false,
  thirdPartyCookiesEnabled: false,
  isApplyDeviceHeight: false,
  actionHandler: () => {},
  scrollEnabled: false,
  fromEspot: false,
};

export default RichText;
