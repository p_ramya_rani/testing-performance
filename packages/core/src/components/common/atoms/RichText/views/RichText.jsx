// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import { PropTypes } from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import styles from '../RichText.style';

const richTextRef = {};

const addScriptTagToDocument = (id, script) => {
  const tag = document.createElement('script');
  tag.id = id;
  if (script.src !== '') {
    tag.src = script.src;
  } else if (script.innerHTML || script.textContent) {
    tag.innerHTML = script.innerHTML || script.textContent;
  }
  document.getElementsByTagName('body')[0].appendChild(tag);
};

const getScriptIds = ref => {
  const scripts = [...ref.getElementsByTagName('script')];
  const url = window && window.location.pathname;
  return scripts.map((script, index) => {
    return {
      id: `script-${url.replace(/\//g, '-')}-${index}`,
      script,
    };
  });
};

const relocateScripts = ref => {
  getScriptIds(ref).forEach(item => {
    const { id, script } = item;

    const scriptTagExists = document.getElementById(id);
    if (!!scriptTagExists === false) {
      addScriptTagToDocument(id, script);
    }
  });
};

const unloadScripts = ref => {
  getScriptIds(ref).forEach(item => {
    const { id } = item;

    const scriptTag = document.getElementById(id);
    if (scriptTag) {
      scriptTag.remove();
    }
  });
};

const bindActions = onClickHandler => () => {
  const anchorElements =
    (richTextRef && richTextRef.current && richTextRef.current.getElementsByTagName('a')) || [];
  const imgElements =
    (richTextRef && richTextRef.current && richTextRef.current.getElementsByTagName('img')) || [];
  for (let img = 0; img < imgElements.length; img += 1) {
    imgElements[img].setAttribute('loading', 'lazy');
  }
  for (let i = 0; i < anchorElements.length; i += 1) {
    const imgLinks = anchorElements[i].getElementsByTagName('img');
    anchorElements[i].addEventListener('click', onClickHandler, false);
    for (let j = 0; j < imgLinks.length; j += 1) {
      imgLinks[j].addEventListener('click', onClickHandler, false);
    }
  }

  return () => {
    for (let i = 0; i < anchorElements.length; i += 1) {
      const imgLinks = anchorElements[i].getElementsByTagName('img');
      anchorElements[i].removeEventListener('click', onClickHandler, false);
      for (let j = 0; j < imgLinks.length; j += 1) {
        imgLinks[j].removeEventListener('click', onClickHandler, false);
      }
    }
  };
};

/**
 * This funcition Identifies if target is a link
 * @param {*} event
 */
const isLink = event => {
  const { target } = event;
  const { parentNode } = target;
  const superParent = event.currentTarget;
  const getAnchorTags = superParent.getElementsByTagName('a');
  return (
    target.tagName.toLowerCase() === 'a' ||
    parentNode.tagName.toLowerCase() === 'a' ||
    superParent.tagName.toLowerCase() === 'a' ||
    getAnchorTags.length
  );
};

/**
 * This function fetches attributes out from link in a event
 * @param {*} event
 * @param {*} attribute
 */
const getAttribute = (event, attribute) => {
  const { target } = event;
  const { parentNode } = target;
  const superParent = event.currentTarget;
  const getAnchorTags = superParent.getElementsByTagName('a');
  return (
    target.getAttribute(attribute) ||
    parentNode.getAttribute(attribute) ||
    superParent.getAttribute(attribute) ||
    (getAnchorTags[0] && getAnchorTags[0].getAttribute(attribute))
  );
};

// This accepts an HTML and inserts it wherever the component is added.
const RichText = ({
  className,
  richTextHtml,
  dataLocator,
  actionHandler,
  fromEspot,
  // eslint-disable-next-line sonarjs/cognitive-complexity
}) => {
  let unBindActions;
  const onClickHandler = event => {
    // Check if Event is fired from an anchor tag, also checks if Rich Text module is loaded from Espot module
    if (isLink(event) && fromEspot) {
      const href = getAttribute(event, 'href');
      const dataTarget = getAttribute(event, 'data-target');
      const target = getAttribute(event, 'target');
      const clickedText = (event.srcElement && event.srcElement.innerText) || '';

      // Check if a dataTarget is defined or href exists and does not starts with (#)
      if (dataTarget || (href && !href.startsWith('#'))) {
        event.preventDefault();
        event.stopPropagation();
        actionHandler(href, target, dataTarget, clickedText);
      }
    }
  };

  const richTextRefHandler = ref => {
    if (richTextRef.current) {
      unloadScripts(richTextRef.current);
    }

    if (ref) {
      richTextRef.current = ref;
      // TODO: Need to refactor this function later to handle binding and unbinding properly
      unBindActions = bindActions(onClickHandler)();
      relocateScripts(ref);
    } else {
      richTextRef.current = null;
    }
  };

  useEffect(() => {
    return () => {
      unBindActions();
    };
  });

  // The <div> element can have child anchor elements that allows keyboard interaction
  return (
    <div
      data-locator={dataLocator}
      className={className}
      dangerouslySetInnerHTML={{ __html: richTextHtml }}
      ref={richTextRefHandler}
    />
  );
};

RichText.propTypes = {
  className: PropTypes.string.isRequired,
  richTextHtml: PropTypes.string.isRequired,
  dataLocator: PropTypes.string,
  actionHandler: PropTypes.func,
  fromEspot: PropTypes.bool,
};

RichText.defaultProps = {
  dataLocator: '',
  actionHandler: () => {},
  fromEspot: false,
};

export default withStyles(RichText, styles);
export { RichText as RichTextVanilla };
