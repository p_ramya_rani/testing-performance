// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { Text, Image, View } from 'react-native';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor/views/Anchor.native';
import {
  getStyledViewComponent,
  getStyledTextComponent,
  getStyledImageComponent,
  ImageWrapperView,
} from './StyleGenerator.native';

const StyledComponents = {};
let navigationData = {};
let linkHandler = null;

const parseStyle = (styleString) => {
  const vacuumString = styleString.join(',').replace(/\s/g, '');
  const transformedString = vacuumString.slice(0, vacuumString.indexOf('@')).replace(/[.]/g, '');
  const splitStyleStrings = transformedString.split('}');
  splitStyleStrings.forEach((str) => {
    const styleConfig = str.split('{').filter((val) => val);
    // eslint-disable-next-line prefer-destructuring
    if (styleConfig.length) StyledComponents[styleConfig[0]] = styleConfig[1];
  });
  return StyledComponents;
};

export const RenderText = (props, className) => {
  const { style, children } = props;
  if (!children) {
    return null;
  }
  const StyledText = className ? getStyledTextComponent(StyledComponents, className) : Text;

  return <StyledText style={{ style }}>{children}</StyledText>;
};

export const RenderString = (text) => <Text>{text}</Text>;

RenderText.propTypes = {
  style: PropTypes.shape({}).isRequired,
  children: PropTypes.shape({}),
};

RenderText.defaultProps = {
  children: {},
};

export const RenderAnchor = (props, className) => {
  const { children, href, className: selfClass, target } = props;
  const classes = className ? `${className} ${selfClass}` : selfClass;
  const StyledText = className ? getStyledTextComponent(StyledComponents, classes) : Text;
  const anchorHandler = linkHandler
    ? {
        onPress: () => linkHandler(href, target, props['data-target']),
      }
    : {};
  return (
    <Anchor url={href} {...anchorHandler} navigation={navigationData}>
      <StyledText>{children[0].props.children}</StyledText>
    </Anchor>
  );
};

RenderAnchor.propTypes = {
  style: PropTypes.shape({}).isRequired,
  children: PropTypes.arrayOf({}).isRequired,
  href: PropTypes.string.isRequired,
  className: PropTypes.string,
  target: PropTypes.string,
  'data-target': PropTypes.string,
};

RenderAnchor.defaultProps = {
  className: '',
  target: '',
  'data-target': '',
};

export const RenderImage = (props, className) => {
  const { alt, source, className: selfClass, style } = props;
  const classes = className ? `${className} ${selfClass}` : selfClass;
  const StyledImage = classes ? getStyledImageComponent(StyledComponents, classes) : Image;

  return (
    <ImageWrapperView>
      <StyledImage
        style={{ ...style }}
        resizeMode="stretch"
        alt={alt}
        source={{ uri: source.uri }}
      />
    </ImageWrapperView>
  );
};

RenderImage.propTypes = {
  style: PropTypes.shape({}).isRequired,
  children: PropTypes.shape({}).isRequired,
  alt: PropTypes.string,
  source: PropTypes.string.isRequired,
  className: PropTypes.string,
};

RenderImage.defaultProps = {
  className: '',
  alt: '',
};

export const RenderView = (props) => {
  const { children, className } = props;
  const StyledView = className ? getStyledViewComponent(StyledComponents, className) : View;
  return children ? (
    <StyledView>
      {children.map((child) => {
        if (typeof child === 'string') {
          return RenderString(child);
        }
        switch (child.type.name) {
          case 'div':
          case 'p':
          case 'ul':
          case 'li':
            return RenderView(child.props);
          case 'img':
            return RenderImage(child.props, className);
          case 'a':
            return RenderAnchor(child.props, className);
          case 'style':
            parseStyle(child.props.children);
            return null;
          case 'Script':
            return null;
          default:
            return RenderText(child.props, className);
        }
      })}
    </StyledView>
  ) : null;
};

RenderView.propTypes = {
  children: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
};

RenderView.defaultProps = {
  className: '',
};

const generateComponentMap = (navigation, actionHandler) => {
  navigationData = navigation;
  linkHandler = actionHandler;
  return {
    br: () => <Text> </Text>,
    p: (props) => RenderView(props),
    b: (props) => RenderText(props),
    img: (props) => RenderImage(props),
    h3: (props) => RenderText(props),
    ul: (props) => RenderView(props),
    a: (props) => RenderAnchor(props),
    li: (props) => RenderView(props),
    div: (props) => RenderView(props),
  };
};

export default generateComponentMap;
