// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { PrintModulesVanilla } from '../views/PrintComponent.view';

describe('PrintComponent', () => {
  const props = {
    ComponentToPrint: {},
    printTriggerText: '',
  };
  it('should renders correctly', () => {
    const component = shallow(<PrintModulesVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
