// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .hide-content {
    display: none !important;
  }

  .print-text {
    color: ${props => props.theme.colorPalette.white};
    line-height: 0px;
    user-select: none;
  }
`;
