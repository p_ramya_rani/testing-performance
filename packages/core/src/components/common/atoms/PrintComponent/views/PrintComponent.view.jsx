// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useRef } from 'react';
import ReactToPrint from 'react-to-print';
import { PropTypes } from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import styles from '../styles/PrintComponent.style';

const PrintComponent = ({
  ComponentToPrint,
  printTriggerText,
  displayContentBeforePrint,
  className,
  printTextClassName,
  compProps,
  ...otherProps
}) => {
  const componentRef = useRef();
  const { renderTriggerButton } = otherProps;

  return (
    <div className={className}>
      <ReactToPrint
        trigger={() => {
          if (renderTriggerButton) {
            return renderTriggerButton();
          }
          return <div className={`print-text ${printTextClassName}`}>{printTriggerText}</div>;
        }}
        content={() => componentRef.current}
      />
      <div className={!displayContentBeforePrint ? 'hide-content' : ''}>
        <ComponentToPrint ref={componentRef} {...compProps} />
      </div>
    </div>
  );
};

PrintComponent.propTypes = {
  ComponentToPrint: PropTypes.shape({}).isRequired,
  printTriggerText: PropTypes.string.isRequired,
  displayContentBeforePrint: PropTypes.bool,
  printTextClassName: PropTypes.string,
  className: PropTypes.string,
  compProps: PropTypes.shape({}),
};

PrintComponent.defaultProps = {
  displayContentBeforePrint: true,
  printTextClassName: '',
  className: '',
  compProps: {},
};

export default withStyles(PrintComponent, styles);
export { PrintComponent as PrintModulesVanilla };
