// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class DynamicModule extends PureComponent {
  static propTypes = {
    importCallback: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      Component: null,
    };
  }

  componentDidMount() {
    const { importCallback } = this.props;
    importCallback().then(module => {
      this.setState({ Component: module.default });
    });
  }

  render() {
    const { importCallback, ...compProps } = this.props;
    const { Component } = this.state;
    return Component && <Component {...compProps} />;
  }
}

export default DynamicModule;
