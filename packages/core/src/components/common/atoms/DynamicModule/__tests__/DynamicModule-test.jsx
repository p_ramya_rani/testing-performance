// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import DynamicModule from '../DynamicModule';

describe('Dynamic Module component', () => {
  it('renders correctly', () => {
    const props = {
      importCallback: jest.fn().mockImplementation(
        () =>
          new Promise(resolve => {
            resolve({ default: () => {} });
          })
      ),
      extraProp: 'test',
    };
    const component = shallow(<DynamicModule {...props} />);
    expect(props.importCallback).toHaveBeenCalled();
    expect(component).toEqual({});
  });
});
