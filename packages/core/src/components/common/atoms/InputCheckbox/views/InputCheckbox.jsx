// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../hoc/withStyles';
import BodyCopy from '../../BodyCopy';
import styles from '../InputCheckbox.style';

const getWrapperStyle = styledClass =>
  styledClass ? `${styledClass}-wrapper` : 'checkBoxLabelAlign';
const getlabelStyle = styledClass => (styledClass ? `${styledClass}-lbl` : 'labelStyle');
const getTxtStyle = styledClass => (styledClass ? `${styledClass}-txt` : 'CheckBox__text');

const InputCheckbox = ({
  children,
  className,
  ariaLabel,
  input,
  disabled,
  dataLocator,
  meta,
  checked,
  styledClass,
}) => {
  const { touched, error } = meta;
  const errorMessagea11yLbl = `checkbox__error__${input.name}`;
  const checkboxClass = styledClass || 'CheckBox__input';
  const checkboxWrapperClass = getWrapperStyle(styledClass);
  const checkboxLblClass = getlabelStyle(styledClass);
  const checkboxTxtClass = getTxtStyle(styledClass);

  return (
    <React.Fragment>
      <div className={className}>
        <div className={checkboxWrapperClass}>
          <input
            {...input}
            id={input.name}
            aria-label={ariaLabel}
            className={checkboxClass}
            type="checkbox"
            data-locator={dataLocator}
            checked={input.value || checked}
            disabled={disabled}
            aria-describedby={errorMessagea11yLbl}
          />

          <label htmlFor={input.name} className={checkboxLblClass}>
            {children && (
              <BodyCopy
                fontFamily="secondary"
                className={`${checkboxTxtClass} ${disabled ? 'disabled' : ''}`}
              >
                {children}
              </BodyCopy>
            )}
          </label>
        </div>
        <div className="Checkbox__error" component="div">
          <span className={touched && error ? 'warning-icon' : ''} aria-disabled="true" />
          <BodyCopy
            color="error"
            component="div"
            fontSize="fs12"
            fontFamily="secondary"
            fontWeight="extrabold"
            role="alert"
            aria-live="assertive"
            data-locator="errorDataLocator"
            id={errorMessagea11yLbl}
          >
            {touched && error ? error : ''}
          </BodyCopy>
        </div>
      </div>
    </React.Fragment>
  );
};

InputCheckbox.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string.isRequired,
  ariaLabel: PropTypes.string,
  input: PropTypes.shape({}),
  disabled: PropTypes.bool,
  dataLocator: PropTypes.string,
  meta: PropTypes.shape({}),
  checked: PropTypes.bool,
  styledClass: PropTypes.string,
};
InputCheckbox.defaultProps = {
  ariaLabel: '',
  dataLocator: '',
  input: {},
  meta: {},
  checked: false,
  styledClass: '',
  children: null,
  disabled: null,
};

export default withStyles(InputCheckbox, styles);
export { InputCheckbox as InputCheckboxVanilla };
