// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-native';
import { RenderTree, ComponentMap } from '@fabulas/astly';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import BodyCopy from '../../BodyCopy';
import { BodyCopyWithSpacing } from '../../styledWrapper';
import {
  StyledCheckBox,
  StyledImage,
  StyledErrorIcon,
  BodyCopyCustom,
} from '../InputCheckbox.style.native';

import { StyledErrorWrapper } from '../../TextBox/TextBox.style.native';

import Image from '../../Image';

const uncheckedIcon = require('../../../../../../../mobileapp/src/assets/images/forms-check-box-inactive.png');
const checkedIcon = require('../../../../../../../mobileapp/src/assets/images/forms-check-box-active.png');
const errorIcon = require('../../../../../../../mobileapp/src/assets/images/alert-triangle.png');

class InputCheckBox extends React.Component {
  static propTypes = {
    rightText: PropTypes.string,
    isChecked: PropTypes.bool,
    onClick: PropTypes.func,
    id: PropTypes.string,
    input: PropTypes.shape({ value: PropTypes.bool }),
    hideCheckboxIcon: PropTypes.bool,
    meta: PropTypes.func,
    fontSize: PropTypes.string,
    disabled: PropTypes.bool,
    hideError: PropTypes.bool,
    checkBoxLabel: PropTypes.bool,
    execOnChangeByDefault: PropTypes.bool,
    children: PropTypes.string,
    inputVariation: PropTypes.string,
    textMargin: PropTypes.string,
    errorStyle: PropTypes.string,
    useEspot: PropTypes.bool,
    onRequestClose: PropTypes.func,
    accessibilityText: PropTypes.string,
    spacingStyles: PropTypes.string,
    isCurvedCheckBox: PropTypes.bool,
    overrideCSS: PropTypes.shape({}),
    isFromChangeStore: PropTypes.bool,
  };

  static defaultProps = {
    rightText: null,
    isChecked: false,
    execOnChangeByDefault: true,
    onClick: () => {},
    id: 'checkbox',
    input: { val: '', value: false },
    hideCheckboxIcon: false,
    meta: {},
    fontSize: 'fs12',
    disabled: false,
    hideError: false,
    inputVariation: 'inputVariation',
    checkBoxLabel: false,
    children: null,
    textMargin: null,
    errorStyle: null,
    useEspot: false,
    onRequestClose: () => {},
    accessibilityText: '',
    spacingStyles: '',
    isCurvedCheckBox: false,
    overrideCSS: { checkbox: {} },
    isFromChangeStore: false,
  };

  constructor(props) {
    super(props);
    const { isChecked, input, execOnChangeByDefault } = props;
    if (execOnChangeByDefault) input.onChange(isChecked);
    this.state = {
      isChecked,
    };
  }

  componentDidUpdate(prevProps) {
    const { isChecked: isCheckedProp } = this.props;
    const { isChecked: isCheckedState } = this.state;
    if (prevProps.isChecked !== isCheckedProp && isCheckedProp !== isCheckedState) {
      /* eslint-disable react/no-did-update-set-state */
      this.setState({
        isChecked: isCheckedProp,
      });
    }
  }

  onClick = () => {
    const { isChecked } = this.state;
    const { onClick, id, input } = this.props;
    const checkboxState = !isChecked;
    this.setState(
      {
        isChecked: checkboxState,
      },
      () => {
        input.onChange(checkboxState);
        onClick(checkboxState, id);
      }
    );
  };

  getSpacingStyles = (spacingStyles) => {
    return spacingStyles || 'margin-top-XXS';
  };

  genCheckedIcon(isCurvedCheckBox) {
    const { isChecked } = this.state;
    const source = isChecked ? checkedIcon : uncheckedIcon;
    let inlineStyle = isCurvedCheckBox ? { borderRadius: 2 } : {};
    inlineStyle =
      isCurvedCheckBox && !isChecked ? { ...inlineStyle, ...{ opacity: 0.3 } } : inlineStyle;

    return (
      <StyledImage>
        <Image style={inlineStyle} source={source} alt="" height="18px" width="18px" />
      </StyledImage>
    );
  }

  genCheckedIconFromChangeStore(isCurvedCheckBox) {
    const { input } = this.props;
    const { value } = input;
    const source = value ? checkedIcon : uncheckedIcon;
    let inlineStyle = isCurvedCheckBox ? { borderRadius: 2 } : {};
    inlineStyle =
      isCurvedCheckBox && !value ? { ...inlineStyle, ...{ opacity: 0.3 } } : inlineStyle;

    return (
      <StyledImage>
        <Image style={inlineStyle} source={source} alt="" height="18px" width="18px" />
      </StyledImage>
    );
  }

  renderRight({ ...otherProps }) {
    const { rightText, textMargin, fontSize, useEspot, onRequestClose } = this.props;
    if (useEspot) {
      return (
        <Espot richTextHtml={rightText} isNativeView={false} onRequestClose={onRequestClose} />
      );
    }
    const astlyBag = {
      navigate(node) {
        const { tagName, properties } = node;
        const { href } = properties;

        Alert.alert(
          `You just clicked on an ${tagName} tag for ${href}`,
          JSON.stringify(node, null, 2)
        );
      },
    };

    return (
      <RenderTree
        tree={`<span>${rightText}</span>`}
        tools={astlyBag}
        componentMap={{
          ...ComponentMap,
          span: (props) => (
            <BodyCopyCustom
              fontFamily="secondary"
              fontSize={fontSize || 'fs12'}
              text={props.children}
              {...props}
              {...otherProps}
            />
          ),
          a: (props) => {
            return (
              <BodyCopy
                margin={textMargin}
                fontFamily="secondary"
                fontSize={fontSize || 'fs12'}
                text={props.children}
                {...props}
              />
            );
          },
        }}
      />
    );
  }

  render() {
    const {
      input,
      hideCheckboxIcon,
      meta,
      disabled,
      rightText,
      inputVariation,
      checkBoxLabel,
      children,
      accessibilityText,
      fontSize,
      hideError,
      errorStyle,
      spacingStyles,
      isCurvedCheckBox,
      overrideCSS,
      isFromChangeStore,
      ...otherProps
    } = this.props;
    const { isChecked } = this.state;
    const { value } = input;
    const { touched, error } = meta;
    const isError = touched && error;

    return (
      <>
        <StyledCheckBox
          onStartShouldSetResponder={this.onClick}
          {...input}
          {...otherProps}
          value={value}
          accessible
          // eslint-disable-next-line react-native-a11y/has-valid-accessibility-role
          accessibilityRole="checkbox"
          accessibilityLabel={accessibilityText || children}
          pointerEvents={disabled ? 'none' : 'auto'}
          accessibilityState={{
            disabled,
            checked: isChecked,
          }}
          hideCheckboxIcon={hideCheckboxIcon}
          inputVariation={inputVariation}
          overrideCSS={overrideCSS.checkbox}
        >
          {!isFromChangeStore && !hideCheckboxIcon && this.genCheckedIcon(isCurvedCheckBox)}
          {isFromChangeStore &&
            !hideCheckboxIcon &&
            this.genCheckedIconFromChangeStore(isCurvedCheckBox)}
          {rightText && this.renderRight({ ...otherProps })}
          {checkBoxLabel && (
            <BodyCopyWithSpacing
              fontFamily="secondary"
              fontSize={fontSize}
              text={children}
              {...otherProps}
              spacingStyles={this.getSpacingStyles(spacingStyles)}
            />
          )}
        </StyledCheckBox>
        {!hideError && (
          <Fragment>
            {isError ? (
              <StyledErrorWrapper errorStyle={errorStyle}>
                <StyledErrorIcon>
                  <Image source={errorIcon} alt="" width="16px" height="14px" />
                </StyledErrorIcon>

                <BodyCopy
                  className="Checkbox__error"
                  fontWeight="extrabold"
                  color="error"
                  fontSize="fs12"
                  fontFamily="secondary"
                  text={isError ? error : null}
                />
              </StyledErrorWrapper>
            ) : null}
          </Fragment>
        )}
      </>
    );
  }
}

export default InputCheckBox;
