// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { androidFontStyles } from '../../../../../styles/globalStyles/StyledText.style';
import BodyCopy from '../BodyCopy';

const getAdditionalStyle = (props) => {
  const { margins } = props;
  return {
    ...(margins && { margin: margins }),
  };
};

const StyledCheckBox = styled.View`
  display: flex;
  flex-direction: row;
  align-items: ${(props) => (props.checkboxAlignTop ? 'flex-start' : 'center')};
  ${(props) => (props.hideCheckboxIcon ? 'padding:5px' : '')};
  ${getAdditionalStyle}
  ${(props) => (props.inputVariation === 'inputVariation-1' ? ' width:100%' : '')};
  ${(props) => (props.customWidth ? ` width: ${props.customWidth}` : '')};
  ${(props) => props.overrideCSS}
`;

const StyledImage = styled.View`
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const StyledText = styled.Text`
  ${androidFontStyles}
  ${(props) => (props.inputVariation === 'inputVariation-1' ? ' width:90%' : '')};
`;

const StyledErrorIcon = styled.View`
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  display: flex;
`;

const BodyCopyCustom = styled(BodyCopy)`
  margin-top: ${(props) => (props.marginTopZero ? '0' : '-4px')};
`;

export { StyledCheckBox, StyledImage, StyledText, StyledErrorIcon, BodyCopyCustom };
