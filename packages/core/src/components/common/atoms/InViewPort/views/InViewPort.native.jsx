// 9fbef606107a605d69c0edbcd8029e5d 
import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { withNavigationFocus } from 'react-navigation';
import PropTypes from 'prop-types';

class InViewPort extends Component {
  static propTypes = {
    disabled: PropTypes.bool,
    delay: PropTypes.number,
    onChange: PropTypes.func.isRequired,
    navigation: PropTypes.shape({}).isRequired,
    isFocused: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    disabled: false,
    delay: 100,
  };

  constructor(props) {
    super(props);
    this.state = { rectTop: 0, rectBottom: 0 };
  }

  componentDidMount() {
    const { disabled } = this.props;
    if (!disabled) {
      this.startWatching();
    }
  }

  /* eslint-disable-next-line */
  UNSAFE_componentWillReceiveProps(nextProps) {
    const { isFocused } = nextProps;
    if (!isFocused) {
      this.stopWatching();
    } else {
      this.lastValue = null;
      this.startWatching();
    }
  }

  componentWillUnmount() {
    this.stopWatching();
  }

  startWatching() {
    const { delay } = this.props;
    if (this.interval) {
      return;
    }
    this.interval = setInterval(() => {
      if (!this.myview) {
        return;
      }
      this.myview.measure((x, y, width, height, pageX, pageY) => {
        this.setState({
          rectTop: pageY,
          rectBottom: pageY + height,
          rectWidth: pageX + width,
        });
      });
      this.isInViewPort();
    }, delay || 100);
  }

  stopWatching() {
    this.interval = clearInterval(this.interval);
  }

  isInViewPort() {
    const { rectBottom, rectTop, rectWidth } = this.state;
    const { onChange } = this.props;
    const window = Dimensions.get('window');
    const isVisible =
      rectBottom !== 0 &&
      rectTop >= window.height * -0.25 &&
      rectBottom <= window.height * 1.25 &&
      rectWidth > 0 &&
      rectWidth <= window.width * 1.5;
    if (this.lastValue !== isVisible) {
      this.lastValue = isVisible;
      onChange(isVisible);
    }
  }

  render() {
    /* eslint-disable */
    const { children } = this.props;
    return (
      <View
        collapsable={false}
        ref={component => {
          this.myview = component;
        }}
        {...this.props}
      >
        {children}
      </View>
    );
  }
}

export default withNavigationFocus(InViewPort);
