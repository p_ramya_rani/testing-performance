// 9fbef606107a605d69c0edbcd8029e5d
export default {
  CAROUSEL_OPTIONS: {
    APP: {
      SMALL_SIZE: {
        PRODUCT_IMAGE_WIDTH_SMALL: '100%',
        PRODUCT_IMAGE_HEIGHT_SMALL: '100%',
        MODULE_HEIGHT_SMALL: 200,
        ITEM_WIDTH_SMALL: 200,
      },
      PRODUCT_IMAGE_WIDTH: '100%',
      PRODUCT_IMAGE_HEIGHT: '100%',
      MODULE_HEIGHT: 260,
      ITEM_WIDTH: 260,
    },
  },
  IMG_DATA: {
    productImgConfig: ['t_mod_Q_product_m', 't_mod_Q_img_product_t', 't_mod_Q_img_product_d'],
  },
  RECOMMENDATION: 'Stylistics_Recommendation',
};
