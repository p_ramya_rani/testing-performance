// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CarouselSlide } from '../views/CarouselSlide.native';

describe('Carousel Slide', () => {
  let component;
  const props = {
    productItem: {
      imageUrl: '/image.png',
      subItemsId: '12345',
      productItemIndex: '0',
      id: '567',
    },
    navigation: {
      state: {
        params: { isSBP: false, title: 'CTL', backgroundColor: 'white', profile: {} },
      },
      navigate: () => {},
    },
    completeTheLookMainTile: 'CTL',
    showSmallImage: false,
    selectedProductId: '12345',
    onPress: () => {},
    selected: true,
    firstItem: true,
    lastItem: false,
    ctlTitle: 'CTl',
  };

  beforeEach(() => {
    component = shallow(<CarouselSlide {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
