import styled from 'styled-components/native';
import Image from '../../Image';
import ProductTabList from '../../../organisms/StyliticsProductTabList';

const TILE_SHADOW = `
  shadow-opacity: 0.15;
  shadow-radius: 2px;
  shadow-color: ${(props) => props.theme.colorPalette.black};
  shadow-offset: 0px 4px;
  elevation: 2;
`;

export const ImageItemWrapper = styled.View`
  flex-direction: row;
  padding-bottom: ${(props) => (props.showData ? props.theme.spacing.ELEM_SPACING.LRG : '8px')};
`;

export const ImageSlideWrapper = styled.View`
  flex-direction: row;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  margin-left: ${(props) => (props.firstItem ? props.theme.spacing.ELEM_SPACING.LRG : 0)};
  padding-right: ${(props) => (props.lastItem ? props.theme.spacing.ELEM_SPACING.XXXL : 0)};
`;

export const StyledImage = styled(Image)`
  /* stylelint-disable-next-line */
  resize-mode: contain;
  ${(props) => (props.height ? `height: ${props.height};` : '')}
  ${(props) => (props.width ? `width: ${props.width};` : '')}
`;

export const OutfitMainImageWrapper = styled.View`
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  ${(props) => (props.height ? `height: ${props.height};` : '')}
  ${(props) => (props.width ? `width: ${props.width};` : '')}
`;

export const OutfitMainTileWrapper = styled.View`
  width: ${(props) => props.windowWidth - 130};
  height: ${(props) => props.windowWidth - 130};
  border-radius: 16px;
  padding: ${(props) =>
    `${props.theme.spacing.ELEM_SPACING.XS} ${props.theme.spacing.ELEM_SPACING.MED} ${props.theme.spacing.ELEM_SPACING.XS}`};
  background-color: ${(props) => props.theme.colorPalette.white};
  ${(props) => (props.isIOS && props.showShadow ? TILE_SHADOW : '')}
  ${(props) => (props.outfitSelected ? `border: solid 3px ${props.outfitSelectedColor};` : '')}
  margin-right:${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const StyledProductTabList = styled(ProductTabList)`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;
