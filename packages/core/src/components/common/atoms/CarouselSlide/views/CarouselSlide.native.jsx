import React from 'react';
import PropTypes from 'prop-types';
import { Dimensions } from 'react-native';
import { withTheme } from 'styled-components';
import { setSelectedOutfitColor } from '@tcp/core/src/utils/utils';
import { LAZYLOAD_HOST_NAME, getUrlWithHttp, getLocator } from '@tcp/core/src/utils';
import { isIOS } from '@tcp/core/src/utils/utils.app';
import Anchor from '../../Anchor';

import {
  ImageItemWrapper,
  ImageSlideWrapper,
  StyledImage,
  OutfitMainImageWrapper,
  OutfitMainTileWrapper,
} from '../styles/CarouselSlide.styles.native';
import constant from '../constant';

export const CarouselSlide = ({
  productItem,
  navigation,
  completeTheLookMainTile,
  showSmallImage,
  selectedProductId,
  onPress,
  selected,
  firstItem,
  lastItem,
  ctlTitle,
  showShadow,
  onRequestClose,
  fromPage,
  isCTL,
}) => {
  const { imageUrl, subItemsId, productItemIndex, id } = productItem;

  const isSBP = navigation?.state?.params?.isSBP;
  const title = navigation?.state?.params?.title;
  const backgroundColor = navigation?.state?.params?.backgroundColor;
  const profile = navigation?.state?.params?.profile;

  const { RECOMMENDATION, IMG_DATA, CAROUSEL_OPTIONS } = constant;
  const { PRODUCT_IMAGE_WIDTH, PRODUCT_IMAGE_HEIGHT } = CAROUSEL_OPTIONS.APP;
  const { PRODUCT_IMAGE_WIDTH_SMALL, PRODUCT_IMAGE_HEIGHT_SMALL } = CAROUSEL_OPTIONS.APP.SMALL_SIZE;
  const dimensions = Dimensions.get('window');

  const navigateToOutfitDetails = () => {
    navigation.navigate('OutfitDetail', {
      title: isSBP ? title : ctlTitle,
      isCTL,
      outfitId: id,
      vendorColorProductIdsList: subItemsId,
      viaModule: RECOMMENDATION,
      backgroundColor,
      isSBP,
      profile,
      fromPage,
      selectedProductId,
    });
  };

  return (
    <ImageSlideWrapper firstItem={firstItem} lastItem={lastItem}>
      <ImageItemWrapper>
        <Anchor
          accessibilityLabel={completeTheLookMainTile}
          navigation={navigation}
          testID={`${getLocator('completeLook_product_image')}${productItemIndex}`}
          onPress={() => {
            if (onPress) {
              onPress();
            } else {
              onRequestClose();
              navigateToOutfitDetails();
            }
          }}
        >
          <OutfitMainTileWrapper
            outfitSelected={selected}
            outfitSelectedColor={setSelectedOutfitColor()}
            windowWidth={dimensions.width}
            isIOS={isIOS()}
            showShadow={showShadow}
          >
            <OutfitMainImageWrapper>
              <StyledImage
                alt={completeTheLookMainTile}
                host={LAZYLOAD_HOST_NAME.HOME}
                url={getUrlWithHttp(imageUrl)}
                height={showSmallImage ? PRODUCT_IMAGE_HEIGHT_SMALL : PRODUCT_IMAGE_HEIGHT}
                width={showSmallImage ? PRODUCT_IMAGE_WIDTH_SMALL : PRODUCT_IMAGE_WIDTH}
                imgConfig={IMG_DATA.productImgConfig[0]}
              />
            </OutfitMainImageWrapper>
          </OutfitMainTileWrapper>
        </Anchor>
      </ImageItemWrapper>
    </ImageSlideWrapper>
  );
};

CarouselSlide.propTypes = {
  productItem: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  completeTheLookMainTile: PropTypes.string.isRequired,
  showSmallImage: PropTypes.bool.isRequired,
  selectedProductId: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
  firstItem: PropTypes.bool.isRequired,
  ctlTitle: PropTypes.string.isRequired,
  lastItem: PropTypes.bool.isRequired,
  showShadow: PropTypes.bool,
  onRequestClose: PropTypes.func,
  fromPage: PropTypes.string,
};

CarouselSlide.defaultProps = {
  selectedProductId: '',
  showShadow: true,
  onRequestClose: () => {},
  fromPage: '',
};

export default withTheme(CarouselSlide);
