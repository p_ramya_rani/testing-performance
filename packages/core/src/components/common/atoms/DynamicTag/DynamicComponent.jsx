// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const DynamicComponent = ({ tag = 'div', children, ...props }) => {
  const WithComponent = styled(tag)``.withComponent(tag);
  return <WithComponent {...props}>{children}</WithComponent>;
};

DynamicComponent.propTypes = {
  children: PropTypes.string,
  tag: PropTypes.string,
};
DynamicComponent.defaultProps = {
  children: '',
  tag: 'div',
};

export default DynamicComponent;
