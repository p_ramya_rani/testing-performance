// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { getScreenWidth } from '@tcp/core/src/utils/utils.app';
import CustomIcon from '@tcp/core/src/components/common/atoms/Icon';
import Image from '@tcp/core/src/components/common/atoms/Image';
import { ICON_NAME } from '@tcp/core/src/components/common/atoms/Icon/Icon.constants';
import colors from '../../../../../../styles/themes/TCP/colors';

import {
  UrlHandler,
  navigateToPage,
  validateExternalUrl,
  redirectToInAppView,
} from '../../../../../utils/utils.app';
import withStyles from '../../../hoc/withStyles.native';
import {
  style,
  CustomStyleText,
  TouchableOpacityComponent,
  IconContainer,
  ImageContainer,
  LoadingSpinnerWrapper,
} from '../ButtonRedesign.style.native';
import {
  getLocator,
  configureInternalNavigationFromCMSUrl,
  isWebViewPage,
  isGymboree,
} from '../../../../../utils';

export const calculateSizePillLength = (sizeCharacterLength) => {
  if (sizeCharacterLength > 5) {
    return getScreenWidth() / 4;
  }
  if (sizeCharacterLength >= 1 && sizeCharacterLength <= 5) {
    return getScreenWidth() / 5.5;
  }
  return false;
};

export const ImageComp = (
  buttonVariation,
  customImageStyle,
  imageLeft,
  imageRight,
  imageName,
  imageSize
) => {
  if (imageName) {
    return (
      <ImageContainer
        style={customImageStyle}
        buttonVariation={buttonVariation}
        imageLeft={imageLeft}
        imageRight={imageRight}
      >
        <Image source={imageName} width={imageSize} height={imageSize} />
      </ImageContainer>
    );
  }

  return null;
};

export const IconComp = (values) => {
  const {
    showIcon,
    iconName,
    selectedIcon,
    iconColor,
    iconSize,
    selected,
    buttonVariation,
    iconRight,
    customIconStyle,
  } = values;
  if (showIcon) {
    return (
      <IconContainer
        style={customIconStyle}
        buttonVariation={buttonVariation}
        iconRight={iconRight}
      >
        <CustomIcon name={selected ? selectedIcon : iconName} size={iconSize} color={iconColor} />
      </IconContainer>
    );
  }

  return null;
};

const LoadingSpinner = ({ textPadding }) => {
  return (
    <LoadingSpinnerWrapper textPadding={textPadding}>
      <ActivityIndicator size="small" color={colors.WHITE} />
    </LoadingSpinnerWrapper>
  );
};

const CustomButton = (props) => {
  const {
    locator,
    text,
    buttonVariation,
    fullWidth,
    customStyle,
    disableButton,
    color,
    fill,
    onPress,
    active,
    selected,
    customTextStyle,
    paddings,
    withNoLineHeight,
    iconFirst,
    fontSize,
    fontWeight,
    fontFamily,
    customBorderStyle,
    useCustomFontStyle,
    imageLeftShow,
    imageRightShow,
    customLeftImageStyle,
    customRightImageStyle,
    hitSlop,
    imageAlignLeft,
    imageAlignRight,
    imageLeftName,
    textPadding,
    imageLeftSize,
    imageRightName,
    imageRightSize,
    sizeCharacterLength,
    buttonSoftDisable,
    showLoadingSpinner,
    ...otherProps
  } = props;
  const textValue = text || '';
  const { url, navigation } = otherProps;
  const openUrl = () => {
    if (isWebViewPage(url) && navigation) {
      redirectToInAppView(url, navigation);
    } else if (validateExternalUrl(url)) {
      UrlHandler(url);
    } else {
      const cmsValidatedUrl = configureInternalNavigationFromCMSUrl(url);
      navigateToPage(cmsValidatedUrl, navigation);
    }
  };
  return (
    <TouchableOpacityComponent
      accessibilityRole="button"
      style={customStyle}
      disabled={disableButton}
      onPress={onPress || openUrl}
      testID={getLocator(locator)}
      hitSlop={hitSlop}
      sizePill={calculateSizePillLength(sizeCharacterLength)}
      buttonSoftDisable={buttonSoftDisable}
      {...props}
    >
      {imageLeftShow
        ? ImageComp(
            buttonVariation,
            customLeftImageStyle,
            imageAlignLeft,
            imageAlignRight,
            imageLeftName,
            imageLeftSize
          )
        : null}
      {iconFirst ? IconComp(props) : null}
      {showLoadingSpinner ? (
        <LoadingSpinner textPadding={textPadding} />
      ) : (
        <CustomStyleText
          fullWidth={fullWidth}
          buttonVariation={buttonVariation}
          color={color}
          fill={fill}
          disableButton={disableButton}
          customBorderStyle={customBorderStyle}
          active={active}
          selected={selected}
          style={customTextStyle}
          textPadding={textPadding}
          withNoLineHeight={withNoLineHeight}
          fontSize={fontSize}
          fontWeight={fontWeight}
          fontFamily={fontFamily}
          buttonSoftDisable={buttonSoftDisable}
          useCustomFontStyle={useCustomFontStyle}
        >
          {textValue}
        </CustomStyleText>
      )}
      {!iconFirst ? IconComp(props) : null}

      {imageRightShow
        ? ImageComp(
            buttonVariation,
            customLeftImageStyle,
            imageAlignLeft,
            imageAlignRight,
            imageRightName,
            imageRightSize
          )
        : null}
    </TouchableOpacityComponent>
  );
};

CustomButton.propTypes = {
  buttonVariation: PropTypes.string,
  fullWidth: PropTypes.string,
  customStyle: PropTypes.shape({}),
  text: PropTypes.string,
  url: PropTypes.string,
  disableButton: PropTypes.bool,
  locator: PropTypes.string,
  color: PropTypes.string,
  onPress: PropTypes.func,
  fill: PropTypes.string,
  active: PropTypes.bool,
  navigation: PropTypes.shape({}),
  selected: PropTypes.bool,
  theme: PropTypes.shape({}),
  showIcon: PropTypes.bool,
  selectedIcon: PropTypes.string,
  iconName: PropTypes.string,
  iconColor: PropTypes.string,
  iconSize: PropTypes.string,
  withNoLineHeight: PropTypes.bool,
  customTextStyle: PropTypes.shape({}),
  paddings: PropTypes.string,
  iconFirst: PropTypes.bool,
  customIconStyle: PropTypes.shape({}),
  fontSize: PropTypes.string,
  fontWeight: PropTypes.string,
  fontFamily: PropTypes.string,
  customBorderStyle: PropTypes.string,
  useCustomFontStyle: PropTypes.bool,
  imageLeftSize: PropTypes.number,
  imageLeftName: PropTypes.string,
  imageLeftShow: PropTypes.bool,
  imageAlignLeft: PropTypes.bool,
  imageRightSize: PropTypes.number,
  imageRightName: PropTypes.string,
  imageRightShow: PropTypes.bool,
  imageAlignRight: PropTypes.bool,
  hitSlop: PropTypes.string,
  showBorder: PropTypes.bool,
  showShadow: PropTypes.bool,
  wrapContent: PropTypes.bool,
  borderRadius: PropTypes.string,
  textPadding: PropTypes.string,
  buttonSoftDisable: PropTypes.bool,
  customLeftImageStyle: PropTypes.shape({}),
  customRightImageStyle: PropTypes.shape({}),
  sizeCharacterLength: PropTypes.number,
  showLoadingSpinner: PropTypes.bool,
};

CustomButton.defaultProps = {
  fullWidth: '',
  buttonVariation: 'fixed-width',
  customStyle: {},
  text: '',
  url: '',
  disableButton: false,
  hitSlop: { top: 0, bottom: 0, left: 0, right: 0 },
  locator: '',
  color: '',
  onPress: null,
  fill: 'BLANK',
  active: false,
  navigation: {},
  selected: false,
  theme: {},
  iconName: ICON_NAME.chevronDown,
  iconColor: 'gray.800',
  iconSize: 'fs12',
  showIcon: false,
  selectedIcon: ICON_NAME.chevronUp,
  customTextStyle: null,
  paddings: '0px',
  withNoLineHeight: false,
  iconFirst: false,
  customIconStyle: {},
  fontSize: '',
  fontWeight: '',
  fontFamily: '',
  customBorderStyle: '',
  useCustomFontStyle: false,
  imageLeftSize: 16,
  imageLeftName: '',
  imageLeftShow: false,
  imageAlignLeft: false,
  imageRightSize: 16,
  imageRightName: '',
  imageRightShow: false,
  imageAlignRight: false,
  showBorder: false,
  showShadow: false,
  wrapContent: false,
  borderRadius: isGymboree() ? '23px' : '16px',
  textPadding: '5px 10px',
  buttonSoftDisable: false,
  customLeftImageStyle: {},
  customRightImageStyle: {},
  sizeCharacterLength: 0,
  showLoadingSpinner: false,
};

export default withStyles(CustomButton, style);
export { CustomButton as CustomButtonVanilla };
