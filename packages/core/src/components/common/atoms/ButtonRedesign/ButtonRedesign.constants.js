// 9fbef606107a605d69c0edbcd8029e5d
const BTNREDESIGN_VARIATION = {
  mobileAppIsNewReDesignButton: 'mobileApp-isNewRedesignButton',
  variableWidth: 'variable-width',
  mobileAppIsNewReDesignPlanButtons: 'mobileApp-isNewRedesignPlanButton',
};

const BUTTON_FILL = {
  BLUE: 'BLUE',
  DARK: 'DARK',
  BLACK: 'BLACK',
  WHITE: 'WHITE',
};

export { BTNREDESIGN_VARIATION, BUTTON_FILL };
