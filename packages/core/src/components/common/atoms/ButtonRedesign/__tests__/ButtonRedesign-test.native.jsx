// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import {
  CustomButtonVanilla,
  calculateSizePillLength,
  ImageComp,
  IconComp,
} from '../views/ButtonRedesign.native';
import { TouchableOpacityComponent } from '../ButtonRedesign.style.native';

describe('CustomButton', () => {
  let component;

  beforeEach(() => {
    component = shallow(<CustomButtonVanilla />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should return styled TouchableOpacityComponent component value one', () => {
    expect(component.find(TouchableOpacityComponent)).toHaveLength(1);
  });

  it('should return a truthy value', () => {
    const sizeCharacterLength = 4;
    expect(calculateSizePillLength(sizeCharacterLength)).toBeTruthy();
  });

  it('should return a falsy value', () => {
    const sizeCharacterLength = 0;
    expect(calculateSizePillLength(sizeCharacterLength)).toBeFalsy();
  });

  it('ImageComp should render correctly', () => {
    const buttonVariation = 'fixed-width';
    const customImageStyle = {};
    const imageLeft = false;
    const imageRight = true;
    const imageName = 85;
    const imageSize = 16;

    expect(
      ImageComp(buttonVariation, customImageStyle, imageLeft, imageRight, imageName, imageSize)
    ).toMatchSnapshot();
  });

  it('IconComp should match snapshot', () => {
    const values = {
      showIcon: true,
      iconName: 'chevron-down',
      selectedIcon: 'chevron-down',
      iconColor: 'gray.800',
      iconSize: 'fs12',
      selected: false,
      buttonVariation: 'variable-width',
      iconRight: false,
      customIconStyle: {},
    };

    expect(IconComp(values)).toMatchSnapshot();
  });
});
