// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable no-shadow */
import styled, { css } from 'styled-components/native';
import { StyledText } from '../../../../../styles/globalStyles/StyledText.style';
import { BTNREDESIGN_VARIATION } from './ButtonRedesign.constants';

const getAdditionalStyle = (props) => {
  const { margin, paddings } = props;
  return {
    ...(margin && {
      margin,
    }),
    ...(paddings && {
      padding: paddings,
    }),
  };
};

const getAdditionalStylesForSizePills = (props) => {
  const { sizePill } = props;
  if (!sizePill) return ``;
  return `width: ${sizePill}px`;
};

const getBorderStyle = (props) => {
  const { disabled, selected, showBorder, theme, disableButton, buttonSoftDisable } = props;
  const { colors, isGymboree } = theme;
  if (!showBorder) {
    return `
    border: none;
  `;
  }

  if (disabled || disableButton || buttonSoftDisable) {
    return `
      border: 1px dashed ${colors.PRIMARY.LIGHTGRAY};
    `;
  }

  if (!selected) {
    return `border: 1px solid ${colors.PRIMARY.LIGHTERGRAY};`;
  }

  if (selected) {
    return `
    border: 3px solid ${isGymboree ? colors.DEFAULT_ACCENT_GYM : colors.BRAND.PRIMARY};
  `;
  }

  return `
    border: none;
  `;
};

const TouchableOpacityComponent = styled.TouchableOpacity`
  flex-direction: row;
  ${getAdditionalStyle}
  justify-content: center;
  text-align: center;
  align-items: center;
  ${getAdditionalStylesForSizePills}
  ${(props) => (props.borderRadius ? `border-radius: ${props.borderRadius}` : ``)}
`;

const ImageContainer = styled.View`
  justify-content: center;
  ${(props) =>
    props.imageLeft
      ? `
    position: absolute;
    left: 0px;
  `
      : ''}
  ${(props) =>
    props.imageRight
      ? `
    position: absolute;
    right: 0px;
  `
      : ''}
`;

const IconContainer = styled.View`
  justify-content: center;
  align-items: center;
`;

const style = css`
  justify-content: center;
  min-height: 32px;
  opacity: ${(props) =>
    props.disabled || props.disableButton ? props.theme.opacity.opacity.medium : '1'};

  ${(props) =>
    props.width
      ? `
      width: ${props.width};
   `
      : ''};

  ${(props) =>
    props.buttonVariation === 'variable-width'
      ? `
     width: ${props.width};
     height: ${props.height};
     `
      : ''};

  border-radius: ${(props) => props.borderRadius || (props.theme.isGymboree ? '23px' : '16px')};

  ${(props) => (props.wrapContent ? `  align-self: baseline;` : '')};
  ${(props) =>
    props.fill === 'BLUE'
      ? ` background: ${props.theme.colors.PRIMARY.DARKBLUE};
      border: 1px solid ${props.theme.colors.PRIMARY.DARKBLUE}; `
      : ''};

  ${(props) =>
    props.fill === 'transparent'
      ? `
      background-color: transparent;
    `
      : ''};

  ${(props) =>
    props.showShadow
      ? ` shadow-color: ${props.theme.colors.BLACK};
          shadow-opacity: 0.12;
          shadow-radius: 8;
          shadow-offset: 2px 2px;
          elevation: 5;
    `
      : ''};

  ${(props) => (props.fill === 'BLANK' ? ` background-color: white; ` : '')};
  ${(props) => (props.fill === 'WHITE' ? ` background-color: white; ` : '')};
  ${getBorderStyle};
`;

const getFontStyles = (props) => {
  const { theme, useCustomFontStyle } = props;
  if (!useCustomFontStyle) {
    return `
      font-family: ${theme.typography.fonts.secondary};
      font-weight: ${props.theme.typography.fontWeights.extrabold};
    `;
  }
  return ``;
};

const getMobileAppPlanButtonText = (props) => {
  const { theme, selected, buttonVariation } = props;
  const { typography, colors } = theme;
  const { fontSizes, fontWeights, fonts } = typography;
  let fontColor = colors.TEXT.DARKBLUE;
  let fontWeight = fontWeights.regular;
  const letterSpacing = '0.36px';

  if (selected) {
    fontColor = colors.TEXT.DARKBLUE;
    fontWeight = fontWeights.extrabold;
  }
  if (buttonVariation === BTNREDESIGN_VARIATION.mobileAppIsNewReDesignPlanButtons) {
    return `
      letter-spacing: ${letterSpacing};
      font-size: ${fontSizes.fs12};
      font-family: ${fonts.secondary};
      font-weight: ${fontWeight};
      color: ${fontColor};
    `;
  }
  return `
  null
  `;
};

const CustomStyleText = styled(StyledText)`
  text-align: center;
  justify-content: center;
  align-items: center;
  letter-spacing: 0.3px;
  opacity: ${(props) =>
    props.disabled || props.disableButton ? props.theme.opacity.opacity.medium : '1'};
  font-size: ${(props) =>
    props.theme.typography.fontSizes[props.fontSize] || props.theme.typography.fontSizes.fs13};
  ${getFontStyles};
  color: ${(props) => props.color || props.theme.colors.TEXT.DARK};
  padding: ${(props) => props.textPadding};
  ${(props) =>
    props.selected
      ? `color:${(props) => props.theme.colors.TEXT.DARKBLUE};font-weight: bold; `
      : ''};
  ${(props) => (props.fill === 'BLUE' ? ` color: ${props.theme.colorPalette.white}; ` : '')};
  ${(props) => (props.fill === 'DARK' ? ` color: ${props.theme.colorPalette.white}; ` : '')};
  ${(props) => (props.fill === 'BLACK' ? ` color: ${props.theme.colorPalette.white}; ` : '')};
  ${(props) => (props.fill === 'WHITE' ? ` color: ${props.theme.colors.TEXT.DARKBLUE}; ` : '')};

  ${getMobileAppPlanButtonText}
  ${(props) => (props.buttonSoftDisable ? ` color: ${props.theme.colors.PRIMARY.GRAY}; ` : '')};
`;

const LoadingSpinnerWrapper = styled.View`
  padding: ${(props) => props.textPadding};
`;

export {
  style,
  CustomStyleText,
  TouchableOpacityComponent,
  IconContainer,
  ImageContainer,
  LoadingSpinnerWrapper,
};
