// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const TILE_SHADOW = `
  shadow-opacity: 0.50;
  shadow-radius: 5;
  shadow-color: ${props => props.theme.colorPalette.black};
  shadow-offset: 0px 4px;
  elevation: 2;
`;

const OutfitMainTileWrapper = styled.View`
  padding: ${props =>
    `${props.theme.spacing.ELEM_SPACING.XS} ${props.theme.spacing.ELEM_SPACING.MED} ${
      props.theme.spacing.ELEM_SPACING.XS
    }`};
  background-color: ${props => props.theme.colorPalette.white};
  ${props =>
    props.tooltipStyle
      ? `width: ${props.tooltipStyle.width}px;
        height: ${props.tooltipStyle.height}px;`
      : ''}
  ${TILE_SHADOW};
`;

export default OutfitMainTileWrapper;
