// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

// need to handle for direction props.

const transformX100 = 'translateX(-100%)';
const transformX50 = 'translateX(-50%)';

const tooltipStyle = css`
  position: relative;
  cursor: default;

  .tooltip-bubble {
    width: fit-content;
    display: table;
    position: absolute;
    z-index: ${(props) => props.theme.zindex.zEnlargedImage};
    bottom: 100%;
    padding-bottom: 10px;
    left: ${(props) => (props.aligned === 'right' ? '26px' : '50%')};
    transform: ${(props) =>
      props.aligned === 'right' && props.direction !== 'right' ? transformX100 : transformX50};
    min-width: ${(props) => (props.minWidth ? `${props.minWidth}` : '320px')};
    ::after {
      border-left: 10px solid transparent;
      border-right: 10px solid transparent;
      border-top: 10px solid ${(props) => props.theme.colors.WHITE};
      bottom: 0;
      transform: ${(props) => (props.aligned === 'right' ? transformX100 : transformX50)};
      content: '';
      position: absolute;
      right: ${(props) => (props.direction === 'right' ? '90px' : '0')};
    }

    ::before {
      border-left: 13px solid transparent;
      border-right: 13px solid transparent;
      border-top: 11px solid rgba(163, 162, 162, 0.09);
      transform: ${(props) => (props.aligned === 'right' ? transformX100 : transformX50)};
      content: '';
      position: absolute;
      right: ${(props) => (props.direction === 'right' ? '82px' : '-8px')};
      bottom: -2px;
    }

    @media ${(props) => props.theme.mediaQuery.medium} {
      left: 36px;
    }
  }
  .tooltip-bubble.newqv {
    transform: translateX(-65%);
    ::after {
      transform: translateX(565%);
    }
    ::before {
      transform: translateX(400%);
    }
  }
  .tooltip-bubble.bottom-tooltip {
    transform: translate(-50%, 115%);
    ::after {
      top: 0;
      bottom: auto;
      transform: translate(-132%, -100%) rotate(180deg);
    }
    ::before {
      top: 0;
      bottom: auto;
      transform: translate(-122%, -113%) rotate(180deg);
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      ::after {
        transform: translate(-182%, -100%) rotate(180deg);
      }
      ::before {
        transform: translate(-160%, -113%) rotate(180deg);
      }
    }
  }
  .tooltip-message {
    background: ${(props) => props.theme.colors.WHITE};
    border-radius: 3px;
    line-height: normal;
    padding: 20px;
    text-align: left;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    box-shadow: 0 0 4px 3px rgba(163, 162, 162, 0.31);
  }
  .tooltip-message.center-for-mpack {
    padding: 10px;
    white-space: pre;
    border-radius: 0;
  }
  .tooltip-bubble.mpack {
    transform: translate(-48%, 0%);
    pointer-events: none;
    padding-bottom: 10px;
    ::after {
      top: 0;
      bottom: auto;
      transform: translate(-411%, 359%);
    }
    ::before {
      top: 0;
      bottom: auto;
      transform: translate(-335%, 340%);
    }
  }
  .showHideTooltip-btn {
    border: none;
    background: transparent;
    width: max-content;
  }
`;

export default tooltipStyle;
