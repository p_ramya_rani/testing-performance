// 9fbef606107a605d69c0edbcd8029e5d
import * as React from 'react';
import PropTypes from 'prop-types';
import BodyCopy from '../../BodyCopy';
import withStyles from '../../../hoc/withStyles';
import styles from '../styles/ReactToolTip.style';
import { isClient } from '../../../../../utils/index';

class ReactTooltip extends React.Component {
  constructor(props) {
    super(props);
    this.node = React.createRef();
  }

  state = {
    displayTooltip: false,
  };

  componentWillUnmount() {
    if (isClient()) {
      // remember to remove all events to avoid memory leaks
      document.body.removeEventListener('click', this.handleClick);
    }
  }

  onClickOutside = () => {
    this.setState({
      displayTooltip: false,
    });
  };

  handleClick = (event) => {
    const { current } = this.node; // get container that we'll wait to be clicked outside
    const { target } = event; // get direct click event target
    // if there is no proper callback - no point of checking
    // if (typeof onClickOutside !== 'function') {
    //   return;
    // }

    // if target is container - container was not clicked outside
    // if container contains clicked target - click was not outside of it
    if (target !== current && !current?.contains(target)) {
      this.onClickOutside(event); // clicked outside - fire callback
    }
  };

  /**
   * @function showTooltip - Shows the tooltip on the "onMouseOver" mouse event.
   */
  showHideTooltip = () => {
    this.setState((prevState) => ({
      displayTooltip: !prevState.displayTooltip,
    }));
  };

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount() {
    if (isClient()) {
      document.body.addEventListener('click', this.handleClick, { capture: true });
    }
  }

  /**
   * @function render - Function that renders and retunrs the tooltip component.
   */
  render() {
    const { id, children, direction, message, className, newQVEnabled } = this.props;
    const { displayTooltip } = this.state;
    let finalClass = '';
    if (direction === 'right') {
      finalClass = 'tooltip-bubble bottom-tooltip';
    } else if (direction === 'center-for-mpack') {
      finalClass = 'tooltip-bubble mpack';
    } else {
      finalClass = 'tooltip-bubble';
    }
    if (newQVEnabled) {
      finalClass = 'tooltip-bubble newqv';
    }
    return (
      <span className={className} ref={this.node} aria-selected>
        {displayTooltip && (
          <div className={finalClass} id={id} direction={direction} message={message}>
            <div className={`tooltip-message ${direction}`}>
              <BodyCopy fontFamily="secondary" fontSize="fs12">
                {message}
              </BodyCopy>
            </div>
          </div>
        )}
        {direction === 'center-for-mpack' ? (
          <span
            role="presentation"
            onMouseOver={this.showHideTooltip}
            onFocus={this.showHideTooltip}
            onMouseOut={this.showHideTooltip}
            onBlur={this.showHideTooltip}
          >
            {children}
          </span>
        ) : (
          <button
            className="showHideTooltip-btn"
            type="button"
            tabIndex="0"
            onClick={this.showHideTooltip}
            aria-expanded="false"
          >
            <span role="presentation">{children}</span>
          </button>
        )}
      </span>
    );
  }
}

/**
 * @exports Tooltip - Default export of the Tooltip component.
 */

ReactTooltip.propTypes = {
  id: PropTypes.string,
  children: PropTypes.node,
  direction: PropTypes.string,
  message: PropTypes.string,
  className: PropTypes.string,
  newQVEnabled: PropTypes.bool,
};

ReactTooltip.defaultProps = {
  id: '',
  children: null,
  direction: '',
  message: '',
  className: '',
  newQVEnabled: false,
};
export default withStyles(ReactTooltip, styles);
export { ReactTooltip as ReactTooltipVanilla };
