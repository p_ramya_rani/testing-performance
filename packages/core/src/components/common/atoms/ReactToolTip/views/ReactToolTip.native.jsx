// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import { TouchableOpacity, Modal, View, Platform, Dimensions, Image } from 'react-native';
import OutfitMainTileWrapper from '../styles/ReactToolTip.style.native';
import getTooltipCoordinate, { getElementVisibleWidth } from './getTooltipCoordinate';

const Screen = Dimensions.get('window');
const ScreenWidth = Screen.width;
const ScreenHeight = Screen.height;
const isIOS = Platform.OS === 'ios';
const colorPalette = createThemeColorPalette();
const upIcon = require('../../../../../../../mobileapp/src/assets/images/arrow-up.png');
const downIcon = require('../../../../../../../mobileapp/src/assets/images/arrow-down.png');

const styles = {
  container: (withOverlay, overlayColor) => ({
    backgroundColor: withOverlay ? overlayColor : 'transparent',
    flex: 1,
  }),
  Pointer: (pastMiddleLine, yOffset, elementHeight, xOffset, elementWidth) => ({
    position: 'absolute',
    top: pastMiddleLine ? yOffset - 10 : yOffset + elementHeight - 1,
    left: xOffset + getElementVisibleWidth(elementWidth, xOffset, ScreenWidth) / 2 - 7.5,
    zIndex: 9999,
  }),
  contentStyle: (yOffset, xOffset, highlightColor, elementWidth, elementHeight) => ({
    position: 'absolute',
    top: yOffset,
    left: xOffset,
    backgroundColor: highlightColor,
    overflow: 'visible',
    width: elementWidth,
    height: elementHeight,
  }),
  imageStyle: {
    width: 30,
    height: 12,
  },
};
class ReactTooltip extends React.PureComponent {
  state = {
    isVisible: false,
    yOffset: 0,
    xOffset: 0,
    elementWidth: 0,
    elementHeight: 0,
    dynamicHeight: 0,
  };

  renderedElement;

  componentDidMount() {
    // wait to compute onLayout values.
    setTimeout(this.getElementPosition, 1000);
  }

  toggleTooltip = () => {
    const { onClose } = this.props;
    this.getElementPosition();
    this.setState((prevState) => {
      if (prevState.isVisible && !isIOS) {
        onClose();
      }

      return { isVisible: !prevState.isVisible };
    });
  };

  wrapWithPress = (toggleOnPress, children) => {
    if (toggleOnPress) {
      return (
        <TouchableOpacity accessibilityRole="link" onPress={this.toggleTooltip} activeOpacity={1}>
          {children}
        </TouchableOpacity>
      );
    }

    return children;
  };

  getTooltipStyle = () => {
    const { yOffset, xOffset, elementHeight, elementWidth } = this.state;
    const { height, backgroundColor, width, withPointer } = this.props;

    const { x, y } = getTooltipCoordinate({
      x: xOffset,
      y: yOffset,
      elementWidth,
      elementHeight,
      ScreenWidth,
      ScreenHeight,
      tooltipWidth: width,
      tooltipHeight: height,
      withPointer,
    });

    return {
      position: 'absolute',
      left: x,
      top: y,
      width,
      height,
      backgroundColor,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      padding: 10,
    };
  };

  renderPointer = (tooltipY) => {
    const { yOffset, xOffset, elementHeight, elementWidth } = this.state;
    const pastMiddleLine = yOffset > tooltipY;
    const arrowIcon = pastMiddleLine ? downIcon : upIcon;
    return (
      <View style={styles.Pointer(pastMiddleLine, yOffset, elementHeight, xOffset, elementWidth)}>
        <Image source={arrowIcon} style={styles.imageStyle} />
      </View>
    );
  };

  renderContent = (withTooltip) => {
    const { popover, withPointer, toggleOnPress, children, autoHeight } = this.props;
    const { dynamicHeight } = this.state;
    if (!withTooltip) {
      return this.wrapWithPress(toggleOnPress, children);
    }

    const tooltipStyle = this.getTooltipStyle();
    const dynamicTooltipStyle = { ...tooltipStyle, height: dynamicHeight };
    return (
      <>
        {withPointer && this.renderPointer(tooltipStyle.top)}
        <View style={tooltipStyle} testID="tooltipPopoverContainer">
          {autoHeight && dynamicHeight === 0 ? (
            <OutfitMainTileWrapper
              onLayout={(event) => {
                const { height } = event.nativeEvent.layout;
                if (height) {
                  this.setState({
                    dynamicHeight: height,
                  });
                }
              }}
            >
              {popover}
            </OutfitMainTileWrapper>
          ) : (
            <OutfitMainTileWrapper tooltipStyle={autoHeight ? dynamicTooltipStyle : tooltipStyle}>
              {popover}
            </OutfitMainTileWrapper>
          )}
        </View>
      </>
    );
  };

  getElementPosition = () => {
    const { yOffset } = this.state;
    const tooltipStyle = this.getTooltipStyle();
    const pastMiddleLine = yOffset > tooltipStyle.top;

    if (this.renderedElement) {
      this.renderedElement.measure(
        (frameOffsetX, frameOffsetY, width, height, pageOffsetX, pageOffsetY) => {
          this.setState({
            xOffset: pageOffsetX,
            yOffset: isIOS ? pageOffsetY : pageOffsetY - (pastMiddleLine ? 0 : 5),
            elementWidth: width,
            elementHeight: height,
          });
        }
      );
    }
  };

  render() {
    const { isVisible } = this.state;
    const { onClose, withOverlay, overlayColor, onOpen } = this.props;

    return (
      <View
        collapsable={false}
        ref={(e) => {
          this.renderedElement = e;
        }}
      >
        {this.renderContent(false)}
        <Modal
          animationType="fade"
          visible={isVisible}
          transparent
          onDismiss={onClose}
          onShow={onOpen}
          onRequestClose={onClose}
        >
          <TouchableOpacity
            accessibilityRole="link"
            style={styles.container(withOverlay, overlayColor)}
            onPress={this.toggleTooltip}
            activeOpacity={1}
          >
            {this.renderContent(true)}
          </TouchableOpacity>
        </Modal>
      </View>
    );
  }
}

ReactTooltip.propTypes = {
  children: PropTypes.element.isRequired,
  withPointer: PropTypes.bool,
  popover: PropTypes.element.isRequired,
  toggleOnPress: PropTypes.bool,
  height: PropTypes.number,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onClose: PropTypes.func,
  onOpen: PropTypes.func,
  overlayColor: PropTypes.string,
  withOverlay: PropTypes.bool,
  backgroundColor: PropTypes.string,
  autoHeight: PropTypes.bool,
};

ReactTooltip.defaultProps = {
  withOverlay: true,
  overlayColor: 'rgba(250, 250, 250, 0.80)',
  withPointer: true,
  toggleOnPress: true,
  height: 96,
  width: 240,
  backgroundColor: colorPalette.white,
  onClose: () => {},
  onOpen: () => {},
  autoHeight: false,
};

export default ReactTooltip;
