// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { shallow } from 'enzyme';

import ReactTooltip from '../views/ReactToolTip.native';

describe('Tooltip component', () => {
  it('should render without issues', () => {
    const component = shallow(
      <ReactTooltip popover={<Text>Info here</Text>}>
        <Text>Press me</Text>
      </ReactTooltip>
    );

    expect(component).toMatchSnapshot();
  });

  it('should display ReactTooltip', () => {
    const Info = () => <Text>Info here</Text>;
    const component = shallow(
      <ReactTooltip height={100} width={200} popover={<Info />}>
        <Text>Press me</Text>
      </ReactTooltip>
    );

    component
      .find(TouchableOpacity)
      .at(0)
      .simulate('press');
    expect(component.find(Info)).toBeTruthy();

    expect(component).toMatchSnapshot();
  });

  it('does not render pointer', () => {
    const component = shallow(
      <ReactTooltip withPointer={false} height={100} width={200} popover={<Text>Info here</Text>}>
        <Text>Press me</Text>
      </ReactTooltip>
    );

    component
      .find(TouchableOpacity)
      .at(0)
      .simulate('press');

    expect(component.state('isVisible')).toBe(true);
  });
});
