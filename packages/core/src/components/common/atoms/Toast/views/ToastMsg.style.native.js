// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const ToastWrapper = styled.View`
  display: flex;
  flex-direction: row;
`;
const ToastText = styled.Text`
  width: 93%;
  color: ${props => props.theme.colors.WHITE};
  font-family: ${props => props.theme.typography.fonts.secondary};
  font-size: ${props => props.theme.typography.fontSizes.fs16};
`;

const ToastCrossWrapper = styled.TouchableOpacity`
  width: 25px;
  height: 25px;
  flex-direction: row;
  justify-content: flex-end;
`;

export { ToastWrapper, ToastText, ToastCrossWrapper };
