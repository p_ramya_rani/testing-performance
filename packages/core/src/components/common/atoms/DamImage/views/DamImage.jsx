/* eslint-disable camelcase, extra-rules/no-commented-out-code, max-lines, sonarjs/cognitive-complexity  */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect, useRef } from 'react';
import { PropTypes } from 'prop-types';
import dynamic from 'next/dynamic';
import {
  configureInternalNavigationFromCMSUrl,
  getAPIConfig,
  getViewportInfo,
  isIEBrowser,
  isSafariBrowser,
  badgeTranformationPrefix,
  getIconPath,
  getImageFilePath,
  removeExtension,
  parseBoolean,
  isAppleDevice,
  isServer,
} from '@tcp/core/src/utils';
import { isTier1Device } from '@tcp/web/src/utils/device-tiering';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import {
  StyledImage,
  StyledVideo,
  StyledFigureTag,
  StyledFigCaption,
  getImageStyle,
  getVideoStyle,
} from '../DamImage.styles';

const defaultImageCrop = 'w_auto,f_auto,q_auto';
export const isAbsoluteUrl = (url) => /^http/i.test(url);
export const isGif = (path) => /\.gif$/i.test(path);
export const isVideo = (path) => /\.(mp4|mov|webm)/i.test(path);

const CloudinaryVideoPlayer = dynamic(() =>
  import(
    /* webpackChunkName: "CloudinaryVideoPlayer" */ '@tcp/core/src/components/common/atoms/CloudinaryVideoPlayer'
  )
);

export const addOptimizedCrops = (input = '', isSecureImageFlagEnabled, imgData) => {
  let additional;
  if (isSecureImageFlagEnabled) {
    let additionalImgData = imgData.url.replace('/', ':');
    additionalImgData = `${additionalImgData}/t_plp_img_d,f_auto`;
    additional = [additionalImgData];
  } else {
    additional = ['f_auto', 'q_auto'];
  }
  const incoming = input.split(',');
  additional.forEach((crop) => {
    if (incoming.indexOf(crop) === -1) {
      incoming.push(crop);
    }
  });
  if (isSecureImageFlagEnabled) {
    return incoming.length ? incoming.join(':') : '';
  }
  return incoming.length ? incoming.join(',') : '';
};

export const getCropParams = (imgData, imgConfigs, badgeParams, isSecureImageFlagEnabled) => {
  const { crop_m: cropM, crop_t: cropT, crop_d: cropD } = imgData;
  const mobile = cropM || imgConfigs[0];
  const tablet = cropT || imgConfigs[1];
  const desktop = cropD || imgConfigs[2];
  if (badgeParams != null) {
    const { desktop: badgeD, tablet: badgeT, mobile: badgeM } = badgeParams;
    return {
      mobile: `${badgeM}/${mobile}`,
      tablet: `${badgeT}/${tablet}`,
      desktop: `${badgeD}/${desktop}`,
    };
  }
  return {
    mobile: addOptimizedCrops(mobile, isSecureImageFlagEnabled, imgData),
    tablet: addOptimizedCrops(tablet, isSecureImageFlagEnabled, imgData),
    desktop: addOptimizedCrops(desktop, isSecureImageFlagEnabled, imgData),
  };
};

export const getImageUrls = (
  imgData = {},
  itemBrand = null,
  isProductImage = false,
  primaryBrand,
  isSecureImageFlagEnabled
) => {
  const { url_m, url_t, url, url_m_es, url_t_es, url_img_es, url_m_fr, url_t_fr, url_img_fr } =
    imgData;
  let mobile = url_m;
  let tablet = url_t;
  let desktop = url;
  const apiConfig = getAPIConfig();
  const { language, brandId } = apiConfig;
  let brand = brandId;
  if (itemBrand) {
    brand = itemBrand;
  }
  if (primaryBrand) {
    brand = primaryBrand;
  }
  if (language === 'es') {
    mobile = url_m_es || mobile;
    tablet = url_t_es || tablet;
    desktop = url_img_es || desktop;
  }
  if (language === 'fr') {
    mobile = url_m_fr || mobile;
    tablet = url_t_fr || tablet;
    desktop = url_img_fr || desktop;
  }
  const xs = mobile || tablet || desktop;
  const sm = tablet || desktop;
  const lg = desktop;
  let assetPrefix;
  if (isProductImage) {
    if (isSecureImageFlagEnabled && brand === 'gym') {
      assetPrefix = 'v1/ecom/assets/products/gym_stylitics';
    } else {
      assetPrefix = apiConfig[`productAssetPath${brand.toUpperCase()}`];
    }
    return {
      mobile: `${assetPrefix}/${xs}`,
      tablet: `${assetPrefix}/${sm}`,
      desktop: `${assetPrefix}/${lg}`,
    };
  }
  return { mobile: xs, tablet: sm, desktop: lg };
};

export const cleanUpUrl = (url) => {
  let cleanUrl = url;
  if (isAbsoluteUrl(url) === true) {
    const start = url.indexOf('upload') + 'upload'.length;
    const end = url.length;
    cleanUrl = url.slice(start, end);
  }
  return cleanUrl ? cleanUrl.replace(/^\/,|^\//, '') : '';
};

export const getImageSourceString = (img, crop, render2x) => {
  const { assetHostTCP: assetHost } = getAPIConfig();
  const path = cleanUpUrl(img);
  return render2x
    ? [
        { density: '1x', url: `${assetHost}/${crop}/${path}` },
        { density: '2x', url: `${assetHost}/${crop},dpr_2.0/${path}` },
      ]
    : [
        { density: '1x', url: `${assetHost}/${crop}/${path}` },
        { density: '2x', url: `${assetHost}/${crop},dpr_1.5/${path}` },
        { density: '3x', url: `${assetHost}/${crop},dpr_3.0/${path}` },
      ];
};

export const ImageTag = (props) => {
  const { src, crop, altText, overrideStyle, className, tabIndex } = props;
  const url = cleanUpUrl(src);
  const { assetHostTCP: assetHost } = getAPIConfig();
  return (
    <StyledImage
      overrideStyle={overrideStyle}
      src={`${assetHost}/${crop}/${url}`}
      alt={altText}
      className={className}
      tabIndex={tabIndex}
    />
  );
};

ImageTag.propTypes = {
  src: PropTypes.string.isRequired,
  crop: PropTypes.string,
  altText: PropTypes.string,
  overrideStyle: PropTypes.string,
  className: PropTypes.string,
};

ImageTag.defaultProps = {
  crop: defaultImageCrop,
  altText: 'Image alt text',
  overrideStyle: '',
  className: '',
};

export const getImageSources = (imgPaths, cropParams, render2x) => {
  const desktop = getImageSourceString(imgPaths.desktop, cropParams.desktop, render2x);
  const tablet = getImageSourceString(imgPaths.tablet, cropParams.tablet, render2x);
  const mobile = getImageSourceString(imgPaths.mobile, cropParams.mobile, render2x);
  return {
    desktop,
    tablet,
    mobile,
  };
};

export const ImageSource = (props) => {
  const { srcSet = [], media } = props;
  const sourceStrings = srcSet.map(({ density, url }) => `${url} ${density}`).join(', ');
  return <source srcSet={sourceStrings} media={media} />;
};

ImageSource.propTypes = {
  srcSet: PropTypes.arrayOf(PropTypes.shape({ density: PropTypes.string, url: PropTypes.string }))
    .isRequired,
  media: PropTypes.string.isRequired,
};

export const getBadgedUrl = (dynamicBadgeOverlayQty, badgeConf, tcpStyleType) => {
  if (!dynamicBadgeOverlayQty || !badgeConf) return null;
  const badgeTranformation = badgeTranformationPrefix(tcpStyleType);
  const [desktop, tablet, mobile] = badgeConf;

  return {
    desktop: `${badgeTranformation}${dynamicBadgeOverlayQty},${desktop}`,
    tablet: `${badgeTranformation}${dynamicBadgeOverlayQty},${tablet}`,
    mobile: `${badgeTranformation}${dynamicBadgeOverlayQty},${mobile}`,
  };
};

export const RenderImage = (props) => {
  const {
    imgData,
    imgConfigs,
    alt,
    isProductImage,
    dynamicBadgeOverlayQty,
    badgeConfig,
    tcpStyleType,
    className,
    itemBrand,
    isHomePage,
    isSecureImageFlagEnabled,
    primaryBrand,
    tabIndex,
  } = props;
  const imgPaths = getImageUrls(imgData, itemBrand, isProductImage, primaryBrand);
  const badgeParams = getBadgedUrl(dynamicBadgeOverlayQty, badgeConfig, tcpStyleType);
  const cropParams = getCropParams(imgData, imgConfigs, badgeParams, isSecureImageFlagEnabled);
  const sources = getImageSources(imgPaths, cropParams, !isHomePage && !isProductImage);
  const imgSrc = { url: imgPaths.mobile, crop: cropParams.mobile, primaryBrand };
  const apiConfig = getAPIConfig();
  const { language } = apiConfig;
  const isTranslationTextEnabled = language !== 'en' && parseBoolean(imgData.useTranslationText);
  return isTranslationTextEnabled ? (
    <StyledFigureTag id="translation_container">
      <picture>
        <ImageSource srcSet={sources.desktop} media="(min-width: 1200px)" />
        <ImageSource srcSet={sources.tablet} media="(min-width: 768px)" />
        <ImageSource srcSet={sources.mobile} media="(min-width: 0px)" />
        <ImageTag
          src={imgSrc.url}
          crop={imgSrc.crop}
          altText={alt}
          overrideStyle={getImageStyle(imgData)}
          className={className}
          primaryBrand={primaryBrand}
          tabIndex={tabIndex}
        />
      </picture>
      <StyledFigCaption id="translation_text">{alt}</StyledFigCaption>
    </StyledFigureTag>
  ) : (
    <picture>
      <ImageSource srcSet={sources.desktop} media="(min-width: 1200px)" />
      <ImageSource srcSet={sources.tablet} media="(min-width: 768px)" />
      <ImageSource srcSet={sources.mobile} media="(min-width: 0px)" />
      <ImageTag
        src={imgSrc.url}
        crop={imgSrc.crop}
        altText={alt}
        overrideStyle={getImageStyle(imgData)}
        className={className}
        primaryBrand={primaryBrand}
        tabIndex={tabIndex}
      />
    </picture>
  );
};

RenderImage.propTypes = {
  imgData: PropTypes.shape({}),
  imgConfigs: PropTypes.arrayOf(PropTypes.string),
  alt: PropTypes.string,
  isProductImage: PropTypes.bool,
  noSourcePictureTag: PropTypes.bool,
  className: PropTypes.string,
  itemBrand: PropTypes.string,
};

RenderImage.defaultProps = {
  imgData: {},
  imgConfigs: [defaultImageCrop, defaultImageCrop, defaultImageCrop],
  alt: '',
  isProductImage: false,
  noSourcePictureTag: false,
  className: '',
  itemBrand: null,
};

export const getVideoSources = (url) => {
  const { assetHostTCP } = getAPIConfig();
  const assetHost = assetHostTCP.replace('/image/', '/video/');
  const renditions = [{ xform: 'f_mp4,vc_auto,q_auto', type: 'video/mp4', extn: 'mp4' }];
  return renditions.map((rendition) => {
    return {
      url: `${assetHost}/${rendition.xform}/${url}.${rendition.extn}`,
      type: rendition.type,
    };
  });
};

export const getPosterImage = (url) => {
  const { assetHostTCP } = getAPIConfig();
  const extn = 'jpg';
  const videoAssetPrefix = assetHostTCP.replace('/image/', '/video/');
  return `${videoAssetPrefix}/${url}.${extn}`;
};

export const getVideoUrls = (videoData, isProductVideo = false, primaryBrand) => {
  const {
    url = '',
    url_t = '',
    url_m = '',
    url_es,
    url_m_es,
    url_t_es,
    url_fr,
    url_m_fr,
    url_t_fr,
  } = videoData;
  let desktop = url;
  let tablet = url_t;
  let mobile = url_m;
  const apiConfig = getAPIConfig();
  const { language } = apiConfig;
  let { brandId } = apiConfig;
  if (primaryBrand) {
    brandId = primaryBrand;
  }

  if (language === 'es') {
    desktop = url_es || url;
    tablet = url_t_es || url_t;
    mobile = url_m_es || url_m;
  }

  if (language === 'fr') {
    desktop = url_fr || url;
    tablet = url_t_fr || url_t;
    mobile = url_m_fr || url_m;
  }

  if (isProductVideo) {
    const assetPrefix = apiConfig[`productAssetPath${brandId.toUpperCase()}`];
    desktop = `${assetPrefix}/${desktop}`;
    tablet = `${assetPrefix}/${tablet}`;
    mobile = `${assetPrefix}/${mobile}`;
  }

  const xs = mobile || tablet || desktop;
  const sm = tablet || desktop;
  const lg = desktop;

  return {
    desktop: removeExtension(cleanUpUrl(lg)),
    tablet: removeExtension(cleanUpUrl(sm)),
    mobile: removeExtension(cleanUpUrl(xs)),
  };
};
/* eslint-enable camelcase */

export const RenderHLSVideo = (props) => {
  const { videoData, isModule } = props;
  return <CloudinaryVideoPlayer videoData={videoData} isModule={isModule} />;
};

export const RenderVideo = (props) => {
  const { videoData, isModule } = props;
  const playIcon = isModule
    ? `${getImageFilePath()}/play-accessible.svg`
    : getIconPath('icon-play');
  const pauseIcon = isModule
    ? `${getImageFilePath()}/pause-accessible.svg`
    : getIconPath('icon-pause');

  const videoUrls = getVideoUrls(videoData);
  const {
    autoplay: autoPlay = true,
    playsinline: playsInline = true,
    muted = true,
    controls = false,
    loop = true,
    title = 'Video',
  } = videoData;
  const [iconPath, setIconPath] = useState(pauseIcon);
  const [sources, setSources] = useState([]);
  const [playing, setPlaying] = useState(true);
  const [poster, setPoster] = useState(getPosterImage(videoUrls.mobile));
  const videoRef = useRef(null);
  useEffect(() => {
    const { isDesktop, isTablet, isMobile } = getViewportInfo();
    if (isDesktop && videoUrls.desktop) {
      setSources(getVideoSources(videoUrls.desktop));
      setPoster(getPosterImage(videoUrls.desktop));
    }
    if (isTablet && videoUrls.tablet) {
      setSources(getVideoSources(videoUrls.tablet));
      setPoster(getPosterImage(videoUrls.tablet));
    }
    if (isMobile && videoUrls.mobile) {
      setSources(getVideoSources(videoUrls.mobile));
    }
  }, [videoData]);

  const toggleVideo = () => {
    if (playing === true) {
      setIconPath(playIcon);
      videoRef.current.pause();
      setPlaying(false);
    } else {
      setIconPath(pauseIcon);
      videoRef.current.play();
      setPlaying(true);
    }
  };

  return (
    <StyledVideo isModule={isModule} overrideStyle={getVideoStyle(videoData)}>
      <video
        key={poster}
        autoPlay={autoPlay}
        playsInline={playsInline}
        muted={muted}
        controls={controls === true ? true : undefined}
        loop={loop}
        poster={poster}
        ref={videoRef}
        title={title}
      >
        {sources?.length > 0 &&
          sources.map((rendition) => {
            return <source src={rendition.url} type={rendition.type} />;
          })}
        <track kind="captions" />
      </video>
      <button
        className="video-play-button"
        type="button"
        title="Play Video"
        aria-disabled="false"
        onClick={toggleVideo}
      >
        <img
          className="tcp_carousel__play_pause_button_icon"
          aria-hidden="true"
          src={iconPath}
          alt=""
        />
      </button>
    </StyledVideo>
  );
};

RenderVideo.propTypes = {
  videoData: PropTypes.shape({}),
};

RenderVideo.defaultProps = {
  videoData: {},
};

export const getPosterUrls = (videoData) => {
  const { poster_url: posterUrl, poster_url_t: posterUrlT, poster_url_m: posterUrlM } = videoData;
  const xs = posterUrlM || posterUrlT || posterUrl;
  const sm = posterUrlT || posterUrl;
  const lg = posterUrl;
  return {
    desktop: removeExtension(cleanUpUrl(lg)),
    tablet: removeExtension(cleanUpUrl(sm)),
    mobile: removeExtension(cleanUpUrl(xs)),
  };
};

export const getCustomPosterImage = (url) => {
  const { assetHostTCP } = getAPIConfig();
  const extn = 'jpg';
  return `${assetHostTCP}/${url}.${extn}`;
};

export const findPosterImage = (posterUrls, videoUrls, variation) => {
  return posterUrls && posterUrls[variation]
    ? getCustomPosterImage(posterUrls[variation])
    : getPosterImage(videoUrls[variation]);
};

export const RenderVideoImage = (props) => {
  const { videoData } = props;
  const videoUrls = getVideoUrls(videoData);
  const posterUrls = getPosterUrls(videoData);

  const [poster, setPoster] = useState(findPosterImage(posterUrls, videoUrls, 'mobile'));

  useEffect(() => {
    const { isDesktop, isTablet, isMobile } = getViewportInfo();
    if (isDesktop && videoUrls.desktop) {
      setPoster(findPosterImage(posterUrls, videoUrls, 'desktop'));
    }
    if (isTablet && videoUrls.tablet) {
      setPoster(findPosterImage(posterUrls, videoUrls, 'tablet'));
    }
    if (isMobile && videoUrls.mobile) {
      setPoster(findPosterImage(posterUrls, videoUrls, 'mobile'));
    }
  }, [videoData]);

  return <img src={poster} alt={videoData.title} />;
};

RenderVideoImage.propTypes = {
  videoData: PropTypes.shape({}),
};

RenderVideoImage.defaultProps = {
  videoData: {},
};

export const getVideoInImgSources = (url) => {
  const { assetHostTCP } = getAPIConfig();
  const assetHost = assetHostTCP.replace('/image/', '/video/');
  if (!isSafariBrowser()) {
    return {
      url: `${assetHost}/e_loop,fl_animated.awebp,q_auto:best/${url}.webp`,
    };
  }
  return {
    url: `${assetHost}/f_mp4,vc_auto,q_auto/${url}.mp4`,
  };
};

export const RenderVideoInImg = (props) => {
  const { videoData } = props;
  const videoUrls = getVideoUrls(videoData);
  const [source, setSource] = useState({});

  useEffect(() => {
    const { isDesktop, isTablet, isMobile } = getViewportInfo();
    if (isDesktop && videoUrls.desktop) {
      setSource(getVideoInImgSources(videoUrls.desktop));
    }
    if (isTablet && videoUrls.tablet) {
      setSource(getVideoInImgSources(videoUrls.tablet));
    }
    if (isMobile && videoUrls.mobile) {
      setSource(getVideoInImgSources(videoUrls.mobile));
    }
  }, [videoData]);

  return (
    <StyledImage src={source?.url} alt={videoData.title} overrideStyle={getVideoStyle(videoData)} />
  );
};

RenderVideoInImg.propTypes = {
  videoData: PropTypes.shape({}),
};

RenderVideoInImg.defaultProps = {
  videoData: {},
};

export const getProductAssetUrl = ({ url = '' }, primaryBrand) => {
  const path = removeExtension(url);
  const apiConfig = getAPIConfig();
  let { brandId } = apiConfig;
  if (primaryBrand) {
    brandId = primaryBrand;
  }
  const assetPrefix = apiConfig[`productAssetPath${brandId.toUpperCase()}`];
  return `${assetPrefix}/${path}`;
};

export const getProductVideoUrls = (url, crop) => {
  const [mobile, tablet, desktop] = crop;
  const { assetHostTCP } = getAPIConfig();
  const assetHost = assetHostTCP.replace('/image/', '/video/');
  return {
    mobile: `${assetHost}/${mobile}/${url}`,
    tablet: `${assetHost}/${tablet}/${url}`,
    desktop: `${assetHost}/${desktop}/${url}`,
  };
};

export const ProductVideo = (props) => {
  const { url, crops, altText, isOptimizedVideo, disableVideoRender, primaryBrand } = props;
  const path = getProductAssetUrl(url, primaryBrand);
  const urls = getProductVideoUrls(path, crops);
  const [source, setSources] = useState({
    video: urls.mobile.concat('.mp4'),
    poster: urls.mobile.concat('.jpg'),
  });
  useEffect(() => {
    const { isTablet, isDesktop } = getViewportInfo();
    const extn = (() => {
      if (disableVideoRender) return '.jpg';
      if (!isOptimizedVideo || !isSafariBrowser()) return '.webp';
      if (isIEBrowser()) return '.gif';
      return '.mp4';
    })();
    let newUrl = urls.mobile;
    if (isTablet) {
      newUrl = urls.tablet;
    } else if (isDesktop) {
      newUrl = urls.desktop;
    }
    setSources({ video: newUrl.concat(extn), poster: newUrl.concat('.jpg') });
  }, [url]);

  if (isOptimizedVideo) {
    return <img src={source.video} alt={altText} />;
  }

  if (disableVideoRender) {
    return <img src={source.video} alt={altText} />;
  }

  const videoData = {
    url: source.video,
    url_t: source.video,
    url_m: source.video,
  };

  return <RenderVideo videoData={videoData} isModule={false} />;
};

ProductVideo.propTypes = {
  url: PropTypes.shape({}),
  crops: PropTypes.arrayOf(PropTypes.string),
  altText: PropTypes.string,
  isOptimizedVideo: PropTypes.bool,
};

ProductVideo.defaultProps = {
  url: {},
  crops: [],
  altText: '',
  isOptimizedVideo: false,
};

export const DamMedia = (props) => {
  const {
    imgData,
    imgConfigs,
    alt,
    videoData: videoInfo,
    isProductImage,
    isOptimizedVideo,
    disableVideoRender,
    dynamicBadgeOverlayQty,
    badgeConfig,
    tcpStyleType,
    renderPictureTag,
    videoDataMobile,
    className,
    itemBrand,
    isHomePage,
    isSecureImageFlagEnabled,
    primaryBrand,
    tabIndex,
  } = props;

  const [videoData, setVideoData] = useState(videoInfo);
  const [mobileHasVideo, setMobileHasVideo] = useState(false);

  useEffect(() => {
    if (videoDataMobile && videoDataMobile.url) {
      const { isMobile } = getViewportInfo();
      if (isMobile) {
        setVideoData(videoDataMobile);
        setMobileHasVideo(true);
      }
    }
  }, []);
  if (!isTier1Device() && videoData && videoData.url) {
    return <RenderVideoImage videoData={videoData} />;
  }

  if (isAppleDevice() && videoData && videoData.url && !parseBoolean(videoData.hls)) {
    return <RenderVideoInImg videoData={videoData} />;
  }

  if (imgData && imgData.url && !mobileHasVideo) {
    if (isProductImage && isVideo(imgData.url)) {
      // there is a bunch of business logic surrounding product videos so keep it separate
      return (
        <ProductVideo
          url={imgData}
          crops={imgConfigs}
          altText={imgData?.alt}
          isOptimizedVideo={isOptimizedVideo}
          disableVideoRender={disableVideoRender}
          primaryBrand={primaryBrand}
        />
      );
    }
    return (
      <RenderImage
        imgData={imgData}
        imgConfigs={imgConfigs}
        alt={alt || imgData?.alt || imgData?.title}
        isProductImage={isProductImage}
        dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
        badgeConfig={badgeConfig}
        tcpStyleType={tcpStyleType}
        renderPictureTag={renderPictureTag}
        className={className}
        itemBrand={itemBrand}
        isHomePage={isHomePage}
        isSecureImageFlagEnabled={isSecureImageFlagEnabled}
        primaryBrand={primaryBrand}
        tabIndex={tabIndex}
      />
    );
  }
  if (videoData && parseBoolean(videoData.hls)) {
    const { isModule } = props;
    return isServer() ? (
      <RenderVideoImage videoData={videoData} />
    ) : (
      <RenderHLSVideo videoData={videoData} isModule={isModule} />
    );
  }

  if (videoData && videoData.url) {
    const { isModule } = props;
    return <RenderVideo videoData={videoData} isModule={isModule} />;
  }
  return <></>;
};

DamMedia.propTypes = {
  imgData: PropTypes.shape({}),
  videoData: PropTypes.shape({}),
  imgConfigs: PropTypes.arrayOf(PropTypes.string),
  alt: PropTypes.string,
  link: PropTypes.shape({}),
  isProductImage: PropTypes.bool,
  isOptimizedVideo: PropTypes.bool,
  noSourcePictureTag: PropTypes.bool,
  className: PropTypes.string,
};

DamMedia.defaultProps = {
  imgData: {},
  videoData: {},
  imgConfigs: [defaultImageCrop, defaultImageCrop, defaultImageCrop],
  alt: '',
  link: {},
  isProductImage: false,
  isOptimizedVideo: false,
  noSourcePictureTag: false,
  className: '',
};

const checkIfPlayPauseButton = (element) => {
  const eleType = element.nodeName;
  const { className } = element;
  const hasVjsClassName =
    className.includes('video-play-btn') || className.includes('vjs-icon-placeholder');
  const parentType = element.parentNode.nodeName;
  return (eleType === 'IMG' && parentType === 'BUTTON') || hasVjsClassName;
};

const DamImage = (props) => {
  const { link, dataLocator, showPlaceHolder } = props;
  if (link) {
    const { url: ctaUrl, target, title, actualUrl, className: ctaClassName } = link || {};
    let to = actualUrl;
    if (!actualUrl && ctaUrl) {
      to = configureInternalNavigationFromCMSUrl(ctaUrl);
    }
    const handleClick = (e) => {
      if (e.currentTarget !== e.target && checkIfPlayPauseButton(e.target)) {
        e.preventDefault(); // necessary to prevent default for video otherwise play pause button clicking navigates to anchor url
      }
    };
    return (
      <Anchor
        className={`${ctaClassName} ${showPlaceHolder ? 'full-width' : ''}`}
        to={to}
        asPath={ctaUrl}
        target={target}
        title={title}
        dataLocator={`${dataLocator}_image-link`}
        onClick={handleClick}
      >
        <DamMedia {...props} />
      </Anchor>
    );
  }
  return <DamMedia {...props} />;
};

export default DamImage;
export { DamImage as DamImageVanilla };
/* eslint-enable */
