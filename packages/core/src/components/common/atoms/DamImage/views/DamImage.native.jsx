// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
/* eslint-disable no-unused-vars */
import React from 'react';
import { Image, PixelRatio, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { MAX_IMAGE_RENDITION_MODULES_APP } from '@tcp/core/src/config/maxImageRendition.config';
import ImageZoom from 'react-native-image-pan-zoom';
import VideoPlayer from '../../VideoPlayer';
import withStyles from '../../../hoc/withStyles.native';
import style from '../DamImage.styles.native';
import FastImage from '../../FastImage';
import {
  cropImageUrl,
  getAPIConfig,
  getVideoUrl,
  isAndroid,
  badgeTranformationPrefix,
} from '../../../../../utils/index.native';
import AccessibilityRoles from '../../../../../constants/AccessibilityRoles.constant';
import { useInfoState } from '../../../../../../../mobileapp/src/context/info/info-provider';

const placeHolderImg = require('../../../../../../../mobileapp/src/assets/images/img-placeholder.png');

const window = Dimensions.get('window');
const screenWidth = window.width;
const screenHeight = window.height;

let dprValue = PixelRatio.get();
dprValue = dprValue ? `${dprValue}.0`.slice(0, 3) : '2.0';
const initialDprValue = dprValue;

/**
 * DamImage returns two types of images
 * 1. Image from react-native
 * 2. LazyLoadImage - A image to be  loaded only when it is visible on screen
 *                  - For an image to be  lazy loaded, parent scrollview should be LazyLoadScrollView from react-native-lazyload-deux
 *                  - it needs "host" as props
 *                  - value of host prop should be same as parent LazyLoadScrollView
 */

const brandNameCheck = (checkBrand, brandId) => {
  if (checkBrand) {
    return checkBrand.toUpperCase();
  }
  return brandId && brandId.toUpperCase();
};

const fetchURL = (imageData) => {
  let URL = imageData.url;

  if (imageData.url_m && imageData.url_m !== '') {
    URL = imageData.url_m;
    return URL;
  }

  if (imageData.url_t && imageData.url_t !== '') {
    URL = imageData.url_t;
    return URL;
  }
  return URL;
};

const setMaxRenditionDPRValue = (isHomePage) => {
  if (isHomePage && (dprValue === `2.0` || dprValue === `3.0`) && MAX_IMAGE_RENDITION_MODULES_APP) {
    dprValue = `1.5`;
  } else {
    dprValue = initialDprValue;
  }
};

const getIsVideoAndAssetHost = (isVideoUrl, assetHost, isBorderVideo) => {
  return (isVideoUrl && assetHost) || (isBorderVideo && assetHost);
};

const updateAssetPathForVideo = (configUrl, isVideoUrl, isOptimizedVideo) => {
  return configUrl && isVideoUrl && !isOptimizedVideo;
};

// only use maxMemory in conjunction with Android as iOS does not support it
const shouldRenderPlainImage = (deviceInfo, isHomePage) =>
  deviceInfo.platform.isAndroid && deviceInfo.device.memoryTotal < 6000000000 && isHomePage;

const getJpegUrl = (url, replace = false) => {
  let path = url;
  let assetHost;
  if (/^http/.test(url)) {
    const [first, second] = url.split('/upload/');
    assetHost = `${first}/upload`;
    path = second;
  } else {
    const { assetHostTCP } = getAPIConfig();
    assetHost = assetHostTCP.replace('/image/', '/video/');
  }
  if (replace) {
    path = path.slice(path.indexOf('ecom'), path.length);
  }
  path = path.replace(/\.(mp4|webm|mov|gif)$/i, '').replace(/^\//, '');
  const dpr = parseInt(PixelRatio.get(), 10);
  return `${assetHost}/f_webp,q_auto,dpr_${dpr}.0/${path}`;
};

const getConfigUrlWithOverlay = (
  dynamicBadgeOverlayQty,
  configUrl,
  badgeConfig,
  tcpStyleType,
  isVideoUrl
) => {
  const badgeTranformation = badgeTranformationPrefix(tcpStyleType);
  const badgeMobileConfig = badgeConfig && badgeConfig.length && badgeConfig[0];
  return dynamicBadgeOverlayQty && badgeMobileConfig && !isVideoUrl
    ? `${badgeTranformation}${dynamicBadgeOverlayQty},${badgeMobileConfig}/${configUrl}`
    : configUrl;
};

const getBrandName = (brandName, primaryBrand, alternateBrand) => {
  if (primaryBrand || alternateBrand) {
    return (
      (primaryBrand && primaryBrand.toUpperCase()) ||
      (alternateBrand && alternateBrand.toUpperCase())
    );
  }
  return brandName;
};

// eslint-disable-next-line complexity
const createURI = (properties) => {
  const {
    url,
    crop,
    imgConfig,
    isProductImage,
    itemBrand,
    checkBrand,
    swatchConfig,
    imgData,
    isOptimizedVideo,
    videoTransformation,
    isHomePage,
    isBorderVideo,
    dynamicBadgeOverlayQty,
    badgeConfig,
    tcpStyleType,
    primaryBrand,
    alternateBrand,
  } = properties;

  const transformations = /\.gif$/.test(url) === true ? 'q_auto' : 'f_auto,q_auto';
  let config = swatchConfig || imgConfig || 'w_768';
  setMaxRenditionDPRValue(isHomePage);
  config = `${config},${transformations},dpr_${dprValue}`;
  const cropVal = `${crop ? `${crop},` : ''}dpr_${dprValue}`;
  const urlVal = imgData ? fetchURL(imgData) : url;
  // const ImageComponent = host ? Image : Image;
  const namedTransformation = imgConfig || '';
  const apiConfigObj = getAPIConfig();

  let { brandId } = apiConfigObj;
  if (itemBrand) {
    brandId = itemBrand;
  }

  let brandName = brandNameCheck(checkBrand, brandId);
  brandName = getBrandName(brandName, primaryBrand, alternateBrand);

  let assetHost = apiConfigObj[`assetHost${brandName}`];
  const productAssetPath = apiConfigObj[`productAssetPath${brandName}`];

  const isVideoUrl = getVideoUrl(url);

  if (getIsVideoAndAssetHost(isVideoUrl, assetHost, isBorderVideo)) {
    assetHost = assetHost.replace('/image/', '/video/');
    config = isOptimizedVideo && videoTransformation ? videoTransformation : namedTransformation; // Image transformations not working as it is for Videos. New transformations need to be created and set.
  }

  const configUrl = config === '' ? '' : `${config}/`;

  // Updating the asset path with placeholder ##format## to update the format from VideoPlayer
  if (updateAssetPathForVideo(configUrl, isVideoUrl, isOptimizedVideo)) {
    assetHost = `${assetHost}/##format##,${configUrl}`;
  }

  if (isBorderVideo) {
    return {
      uri: `${assetHost}/${videoTransformation || ''}/${urlVal}`,
    };
  }

  const configUrlWithOverlay = getConfigUrlWithOverlay(
    dynamicBadgeOverlayQty,
    configUrl,
    badgeConfig,
    tcpStyleType,
    isVideoUrl
  );

  return {
    uri: isProductImage
      ? `${assetHost}/${configUrlWithOverlay}${productAssetPath}/${urlVal}`
      : cropImageUrl(urlVal, cropVal, namedTransformation, isVideoUrl),
  };
};

const renderVideo = (videoProps) => {
  const {
    video,
    image,
    disableVideoClickHandler,
    width,
    height,
    isOptimizedVideo,
    renderVideoAsImage,
    accessibilityLabel,
  } = videoProps;
  const { controls, uri: videoUri } = video;
  const androidTransformationSLP = renderVideoAsImage ? '.jpg' : '.webp';
  const transformationFormatPrefix = isAndroid() ? androidTransformationSLP : '.gif'; // webP is supported in android and have minimum size but unsupported in IOS, so using gif

  const options = {
    controls,
    url: isOptimizedVideo
      ? videoUri.replace(/\.(mp4|webm|WEBM|MP4)$/, transformationFormatPrefix)
      : videoUri,
    image,
  };
  const animatedImageStyle = {
    width,
    height,
    resizeMode: 'contain',
  };

  return isOptimizedVideo ? (
    <Image
      source={{ uri: options.url }}
      style={animatedImageStyle}
      accessible
      accessibilityRole={AccessibilityRoles.Image}
      accessibilityLabel={accessibilityLabel}
    />
  ) : (
    <VideoPlayer
      disableVideoClickHandler={disableVideoClickHandler}
      {...options}
      videoWidth={parseInt(width, 10)}
      videoHeight={parseInt(height, 10)}
    />
  );
};

const getImageWidth = (imgData, width) => {
  return width;
};

const getImageHeight = (imgData, width, height) => {
  let newHeight = height;
  if (imgData) {
    // eslint-disable-next-line camelcase
    const { width_m: widthM = width, height_m: heightM = height } = imgData;
    const aspectRatio = widthM / heightM;
    newHeight = width / aspectRatio;
    // eslint-disable-next-line no-restricted-globals
    if (isNaN(newHeight)) {
      newHeight = height;
    }
  }
  return newHeight;
};

// To zoom the images
const renderZoomImages = (src, props) => {
  const { width, height, cacheType, priority, onProgress, onError, imgData } = props;

  return (
    <ImageZoom
      cropWidth={screenWidth}
      cropHeight={screenHeight}
      imageWidth={getImageWidth(imgData, width)}
      imageHeight={getImageHeight(imgData, width, height)}
    >
      <FastImage
        imageWidth={getImageWidth(imgData, width)}
        imageHeight={getImageHeight(imgData, width, height)}
        uriInfo={src}
        cacheType={cacheType}
        priority={priority}
        onProgress={onProgress}
        onError={onError}
        accessibilityRole={AccessibilityRoles.Image}
      />
    </ImageZoom>
  );
};

// eslint-disable-next-line complexity, sonarjs/cognitive-complexity
const DamImage = (props) => {
  const {
    url,
    crop,
    source,
    host,
    imgConfig,
    alt,
    isProductImage,
    itemBrand,
    checkBrand,
    swatchConfig,
    videoData,
    width,
    height,
    isFastImage,
    cacheType,
    priority,
    onLoadStart,
    onProgress,
    onError,
    resizeMode,
    imgData,
    disableVideoClickHandler,
    isOptimizedVideo,
    isHomePage,
    isBorderVideo,
    link,
    navigation,
    dynamicBadgeOverlayQty,
    badgeConfig,
    customStyle,
    renderVideoAsImage,
    isBackground,
    overrideStyle,
    isNewReDesignEnabled,
    zoomImages,
    primaryBrand,
    alternateBrand,
    ...otherProps
  } = props;
  const deviceInfo = useInfoState();
  const renderPlainImage = shouldRenderPlainImage(deviceInfo, isHomePage);

  const ImageComponent = Image;
  if (videoData) {
    if (renderPlainImage) {
      const src = getJpegUrl(videoData.url);
      const accessibilityLbl = videoData?.title || alt;
      return (
        <Image
          source={{ uri: src }}
          style={{ width: parseInt(width, 10), height: parseInt(height, 10), ...overrideStyle }}
          accessible
          accessibilityRole={AccessibilityRoles.Image}
          accessibilityLabel={accessibilityLbl}
        />
      );
    }
    videoData.url = fetchURL(videoData);
    return (
      <VideoPlayer
        disableVideoClickHandler={disableVideoClickHandler}
        {...videoData}
        videoWidth={parseInt(width, 10)}
        videoHeight={parseInt(height, 10)}
        isHomePage={isHomePage}
        link={link}
        navigation={navigation}
        resizeMode={resizeMode}
        overrideStyle={overrideStyle}
      />
    );
  }

  let accessibilityLabel = '';
  if (imgData) {
    accessibilityLabel = imgData.alt || imgData.title;
  } else if (alt) {
    accessibilityLabel = alt;
  }

  const uriInfo = createURI(props);

  const uriParam = getVideoUrl(uriInfo.uri);

  if (uriParam && uriInfo) {
    const videoDataOptions = {
      video: uriInfo,
      width,
      height,
      disableVideoClickHandler,
      isOptimizedVideo,
      renderVideoAsImage,
      accessibilityLabel,
    };
    return renderVideo(videoDataOptions);
  }

  if (isFastImage && uriInfo) {
    let src = uriInfo;
    if (renderPlainImage) {
      src = { uri: getJpegUrl(uriInfo.uri, true) };
    }
    if (zoomImages) {
      return renderZoomImages(src, props);
    }
    return (
      <FastImage
        imageWidth={getImageWidth(imgData, width)}
        imageHeight={getImageHeight(imgData, width, height)}
        uriInfo={src}
        cacheType={cacheType}
        priority={priority}
        onProgress={onProgress}
        onError={onError}
        resizeMode={resizeMode}
        accessibilityRole={AccessibilityRoles.Image}
        accessibilityLabel={accessibilityLabel}
        customStyle={customStyle}
        overrideStyle={overrideStyle}
        isBackground={isBackground}
        primaryBrand={primaryBrand || alternateBrand}
      />
    );
  }

  return (
    <ImageComponent
      {...otherProps}
      host={host}
      accessible
      accessibilityRole={AccessibilityRoles.Image}
      accessibilityLabel={accessibilityLabel}
      source={uriInfo}
      defaultSource={placeHolderImg}
      resizeMode={isNewReDesignEnabled ? resizeMode : 'cover'}
      primaryBrand={primaryBrand || alternateBrand}
    />
  );
};

DamImage.propTypes = {
  source: PropTypes.string,
  crop: PropTypes.string,
  imgConfig: PropTypes.string,
  url: PropTypes.string,
  host: PropTypes.string,
  alt: PropTypes.string,
  itemBrand: PropTypes.string,
  swatchConfig: PropTypes.string,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  videoData: PropTypes.shape({}),
  isProductImage: PropTypes.bool,
  checkBrand: PropTypes.string,
  isFastImage: PropTypes.bool,
  priority: PropTypes.string,
  cacheType: PropTypes.string,
  onLoadStart: PropTypes.func,
  onProgress: PropTypes.func,
  onError: PropTypes.func,
  resizeMode: PropTypes.string,
  imgData: PropTypes.shape({}),
  disableVideoClickHandler: PropTypes.bool,
  isOptimizedVideo: PropTypes.bool,
  isHomePage: PropTypes.bool,
  isBorderVideo: PropTypes.bool,
  link: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  dynamicBadgeOverlayQty: PropTypes.number,
  badgeConfig: PropTypes.arrayOf(PropTypes.string),
  customStyle: PropTypes.shape({}),
  renderVideoAsImage: PropTypes.bool,
  isBackground: PropTypes.bool,
  overrideStyle: PropTypes.shape({}),
  isNewReDesignEnabled: PropTypes.bool,
};

DamImage.defaultProps = {
  source: '',
  crop: '',
  imgConfig: '',
  url: '',
  host: '',
  alt: '',
  itemBrand: '',
  swatchConfig: '',
  videoData: null,
  isProductImage: false,
  checkBrand: '',
  isFastImage: false,
  cacheType: 'immutable',
  priority: 'normal',
  resizeMode: 'contain',
  onLoadStart: () => {},
  onProgress: () => {},
  onError: () => {},
  imgData: null,
  disableVideoClickHandler: false,
  isOptimizedVideo: false,
  isHomePage: false,
  isBorderVideo: false,
  link: null,
  navigation: null,
  dynamicBadgeOverlayQty: 0,
  badgeConfig: [],
  customStyle: null,
  renderVideoAsImage: false,
  isBackground: true,
  overrideStyle: null,
  isNewReDesignEnabled: false,
};

export default withStyles(DamImage, style);
export { DamImage as DamImageCompVanilla };
