import styled from 'styled-components';
import { mediaQuery } from '@tcp/core/styles/themes/TCP/mediaQuery';
import colors from '@tcp/core/styles/themes/TCP/colors';

export const StyledImage = styled.img`
  ${(props) => props.overrideStyle || ''}
`;

export const StyledVideo = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  video {
    width: 100% !important;
    object-fit: fill;
    ${(props) => props.overrideStyle || ''}
  }
  .video-play-button {
    position: absolute;
    bottom: 16px;
    z-index: 10;
    ${(props) =>
      props.isModule
        ? `
      left: 8px;
      @media only screen and (min-width: 768px) {
          left: 16px;
      }
      background-color: transparent;
      height: 40px;
      width: 40px;
      border: 0;
    `
        : `
      left: 50%;
      transform: translate(-50%, 0);
      background: transparent;
      height: 28px;
      width: 28px;
      border: 0;
    `}
    padding: 0;
  }

  .video-mute-btn {
    position: absolute;
    bottom: 16px;
    z-index: 10;
    left: 42px;
    @media only screen and (min-width: 768px) {
      left: 52px;
    }
    background-color: transparent;
    height: 40px;
    width: 40px;
    border: 0;
  }

  .big-video-play-btn {
    position: absolute;
    left: 50%;
    bottom: 50%;
    transform: translate(-50%, 50%);
    background: ${colors.PRIMARY.PALEGRAY};
    height: 100px;
    width: 100px;
    border-radius: 50%;
    border: 0;
    padding: 0;
    padding-left: 10px;
    opacity: 50%;
  }
`;

export const getImageStyle = (imgData) => {
  const {
    width_d: widthD,
    width_t: widthT,
    width_m: widthM,
    height_d: heightD,
    height_t: heightT,
    height_m: heightM,
    borderBottomLeftRadius,
    borderBottomRightRadius,
    borderTopLeftRadius,
    borderTopRightRadius,
  } = imgData || {};
  return `
    ${widthM ? `width: ${widthM}px !important;` : ''}
    ${heightM ? `height: ${heightM}px !important;` : ''}
    ${borderBottomLeftRadius ? `border-bottom-left-radius: ${borderBottomLeftRadius}px;` : ''}
    ${borderBottomRightRadius ? `border-bottom-right-radius: ${borderBottomRightRadius}px;` : ''}
    ${borderTopLeftRadius ? `border-top-left-radius: ${borderTopLeftRadius}px;` : ''}
    ${borderTopRightRadius ? `border-top-right-radius: ${borderTopRightRadius}px;` : ''}

    @media ${mediaQuery && mediaQuery.medium} {
      ${widthT ? `width: ${widthT}px !important;` : ''}
      ${heightT ? `height: ${heightT}px !important;` : ''}
    }
    @media ${mediaQuery && mediaQuery.large} {
      ${widthD ? `width: ${widthD}px !important;` : ''}
      ${heightD ? `height: ${heightD}px !important;` : ''}
    }
  `;
};

export const getVideoStyle = (videoData) => {
  const {
    height_d: heightD,
    height_t: heightT,
    height_m: heightM,
    borderBottomLeftRadius,
    borderBottomRightRadius,
    borderTopLeftRadius,
    borderTopRightRadius,
  } = videoData || {};
  return `
    min-height: 100%;
    ${heightM ? `height: ${heightM}vw !important;` : ''}
    ${
      borderBottomLeftRadius
        ? `border-bottom-left-radius: ${borderBottomLeftRadius}px !important;`
        : ''
    }
    ${
      borderBottomRightRadius
        ? `border-bottom-right-radius: ${borderBottomRightRadius}px !important;`
        : ''
    }
    ${borderTopLeftRadius ? `border-top-left-radius: ${borderTopLeftRadius}px !important;` : ''}
    ${borderTopRightRadius ? `border-top-right-radius: ${borderTopRightRadius}px !important;` : ''}
    @media ${mediaQuery.medium} {
      ${heightT ? `height: ${heightT}vw !important;` : ''}

    }
    @media ${mediaQuery.large} {
      ${heightD ? `height: ${heightD}vw !important;` : ''}
    }
  `;
};

export const StyledFigureTag = styled.figure`
  position: relative;
  text-align: center;
  color: ${(props) => props.theme.colors.WHITE};
`;

export const StyledFigCaption = styled.figcaption`
  width: 100%;
  background: #afa8a8d9;
  padding: 6px 0;
  font-size: 20px;
  position: absolute;
  bottom: 20px;
  left: 50%;
  transform: translate(-50%, 50%);
  -webkit-transform: translate(-50%, 50%);
  -moz-transform: translate(-50%, 50%);
  -ms-transform: translate(-50%, 50%);
`;
