// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { act } from 'react-dom/test-utils';
import { shallow, mount } from 'enzyme';
import * as utils from '@tcp/core/src/utils/utils';
import DamImage, {
  RenderImage,
  RenderVideo,
  DamMedia,
  ProductVideo,
  isGif,
  isVideo,
  addOptimizedCrops,
  getCropParams,
} from '../views/DamImage';

const dummydomain = 'http://example.com';
const utilsLocation = '@tcp/core/src/utils/utils';
const imageData = {
  url: '/image/upload/imgD',
  url_t: '/image/upload/imgT',
  url_m: '/image/upload/imgM',
};
const videoData = { url: 'vidD.mp4', url_t: 'vidT.mp4', url_m: 'vidM.mp4' };

beforeEach(() => {
  jest.mock(utilsLocation);
  const apiConfig = {
    brandId: 'tcp',
    productAssetPathTCP: 'some/sample/path',
    assetHostTCP: dummydomain,
  };
  utils.getAPIConfig = jest.fn(() => {
    return apiConfig;
  });
  utils.getViewportInfo = jest.fn(() => {
    return {
      isDesktop: true,
      isMobile: false,
      isTablet: false,
    };
  });
});

describe('DamImage', () => {
  let component;
  it('should be defined', () => {
    component = shallow(<DamImage />);
    expect(component).toBeDefined();
  });
  it('should match snapshot', () => {
    component = shallow(<DamImage />);
    expect(component).toMatchSnapshot();
  });
  it('should render anchor around media tag', () => {
    component = shallow(<DamImage link="/c/toddler-boy-school-uniforms?viaModule=promoFilter" />);
    expect(component).toMatchSnapshot();
  });
});

describe('DamMedia', () => {
  it('renders image correctly', () => {
    const imagecomponent = shallow(<DamMedia imgData={imageData} isProductImage />);
    expect(imagecomponent).toMatchSnapshot();
  });
  it('renders video correctly', () => {
    const videocomponent = shallow(<DamMedia videoData={videoData} isModule />);
    expect(videocomponent).toMatchSnapshot();
  });
});

describe('RenderImage', () => {
  it('renders img tag correctly', () => {
    const component = shallow(<RenderImage imgData={imageData} isProductImage />);
    expect(component).toMatchSnapshot();
  });

  it('renders picture tag correctly', () => {
    const component = shallow(<RenderImage imgData={imageData} isProductImage renderPictureTag />);
    expect(component).toMatchSnapshot();
    utils.getAPIConfig = jest.fn(() => {
      return {
        brandId: 'tcp',
        productAssetPathTCP: dummydomain,
        assetHostTCP: dummydomain,
        language: 'es',
      };
    });
    const componentSpanish = shallow(
      <RenderImage
        imgData={{ url_img_es: 'imgD', url_t_es: 'imgT', url_m_es: 'imgM' }}
        isProductImage
        renderPictureTag
      />
    );
    expect(componentSpanish).toMatchSnapshot();
    utils.getAPIConfig = jest.fn(() => {
      return {
        brandId: 'tcp',
        productAssetPathTCP: dummydomain,
        language: 'fr',
      };
    });
    const componentFrench = shallow(
      <RenderImage
        imgData={{ url_img_fr: 'imgD', url_t_fr: 'imgT', url_m_fr: 'imgM' }}
        isProductImage
        renderPictureTag
      />
    );
    expect(componentFrench).toMatchSnapshot();
  });
});

describe('RenderVideo', () => {
  it('render video correctly', () => {
    const component = shallow(<RenderVideo videoData={videoData} />);
    expect(component).toMatchSnapshot();
    // eslint-disable-next-line sonarjs/no-identical-functions
    utils.getAPIConfig = jest.fn(() => {
      return {
        brandId: 'tcp',
        productAssetPathTCP: dummydomain,
        assetHostTCP: dummydomain,
        language: 'es',
      };
    });
    const componentSpanish = shallow(
      <RenderVideo
        videoData={{ url_es: 'vidD.mp4', url_t_es: 'vidT.mp4', url_m_es: 'vidM.mp4' }}
        isModule
      />
    );
    expect(componentSpanish).toMatchSnapshot();
    utils.getAPIConfig = jest.fn(() => {
      return {
        brandId: 'tcp',
        productAssetPathTCP: dummydomain,
        assetHostTCP: dummydomain,
        language: 'fr',
      };
    });
    const componentFrench = shallow(
      <RenderVideo
        videoData={{ url_fr: 'vidD.mp4', url_t_fr: 'vidT.mp4', url_m_fr: 'vidM.mp4' }}
        isModule
      />
    );
    expect(componentFrench).toMatchSnapshot();
  });
  it('renders appropriate url for viewport', () => {
    utils.getViewportInfo = jest.fn(() => {
      return {
        isDesktop: false,
        isMobile: true,
        isTablet: false,
      };
    });
    let component;
    act(() => {
      component = mount(
        <RenderVideo
          videoData={{ url: 'vidD.mp4', url_t: 'vidT.mp4', url_m: 'vidM.mp4' }}
          isModule
        />
      );
      component.find('button').simulate('click');
    });
    expect(component).toMatchSnapshot();
  });
});

describe('ProductVideo', () => {
  it('renders product video correctly', () => {
    const component = shallow(
      <ProductVideo url="video/upload/prodVideo.mp4" altText="" crop="plp_vid_m" />
    );
    expect(component).toMatchSnapshot();
  });
  it('renders optimized product video correctly', () => {
    const optimizedComponent = shallow(
      <ProductVideo url="video/upload/prodVideo.mp4" altText="" crop="plp_vid_m" isOptimizedVideo />
    );
    expect(optimizedComponent).toMatchSnapshot();
  });
  it('renders video in an image tag', () => {
    const optimizedComponent = shallow(
      <ProductVideo
        url="video/upload/prodVideo.mp4"
        altText=""
        crop="plp_vid_m"
        disableVideoRender
      />
    );
    expect(optimizedComponent).toMatchSnapshot();
  });
  it('re-renders component as per viewport', () => {
    // eslint-disable-next-line sonarjs/no-identical-functions
    utils.getViewportInfo = jest.fn(() => {
      return {
        isDesktop: true,
        isMobile: false,
        isTablet: false,
      };
    });
    let component;
    act(() => {
      component = mount(
        <ProductVideo
          url="video/upload/prodVideo.mp4"
          altText=""
          crop="plp_vid_m"
          disableVideoRender
        />
      );
    });
    expect(component).toMatchSnapshot();
    // eslint-disable-next-line sonarjs/no-identical-functions
    utils.getViewportInfo = jest.fn(() => {
      return {
        isDesktop: false,
        isMobile: true,
        isTablet: false,
      };
    });
    // eslint-disable-next-line sonarjs/no-identical-functions
    act(() => {
      component = mount(
        <ProductVideo
          url="video/upload/prodVideo.mp4"
          altText=""
          crop="plp_vid_m"
          disableVideoRender
        />
      );
    });
    expect(component).toMatchSnapshot();
    utils.getViewportInfo = jest.fn(() => {
      return {
        isDesktop: false,
        isMobile: false,
        isTablet: true,
      };
    });
    act(() => {
      component = mount(
        <ProductVideo url="video/upload/prodVideo.mp4" altText="" crop="plp_vid_m" />
      );
    });
  });
});

describe('DamImage utils', () => {
  it('detects gif image', () => {
    expect(isGif('abc.gif')).toBeTruthy();
    expect(isGif('abc.jpg')).toBeFalsy();
  });

  it('detects video urls', () => {
    expect(isVideo('abc.mp4')).toBeTruthy();
    expect(isVideo('abc.mov')).toBeTruthy();
    expect(isVideo('abc.jpg')).toBeFalsy();
  });

  it('adds crop params', () => {
    const crops = addOptimizedCrops('crop_a,crop_b');
    expect(crops).toEqual('crop_a,crop_b,f_auto,q_auto');
  });

  it('concatenates crop params and badge params', () => {
    const crops = getCropParams({ crop_d: 'crop_d', crop_t: 'crop_t', crop_m: 'crop_m' }, []);
    expect(crops).toEqual({
      mobile: 'crop_m,f_auto,q_auto',
      tablet: 'crop_t,f_auto,q_auto',
      desktop: 'crop_d,f_auto,q_auto',
    });
  });
});
