// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { DamImageCompVanilla } from '../views/DamImage.native';

describe('DamImageCompVanilla', () => {
  let component;

  beforeEach(() => {
    component = shallow(<DamImageCompVanilla />);
  });

  xit('should be defined', () => {
    expect(component).toBeDefined();
  });

  xit('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  xit('should return abc component value one', () => {
    expect(component.find('Styled(ImageComp)')).toHaveLength(0);
  });

  xit('should pass source value', () => {
    component.setProps({ source: 'foo' });
    expect(component).toMatchSnapshot();
  });

  xit('should render lazy load image component', () => {
    component.setProps({ host: 'lazyload-home' });
    expect(component.props().host).toEqual('lazyload-home');
  });
});
