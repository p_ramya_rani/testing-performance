// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { AppBannerVanilla as AppBanner } from '../views/AppBanner';

jest.mock('@tcp/core/src/utils', () => ({
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  getViewportInfo: () => ({
    isMobile: true,
  }),
  parseBoolean: () => true,
  isCanada: () => false,
  getIconPath: jest.fn(),
  getBrand: () => 'tcp',
}));

describe('AppBanner component', () => {
  const defaultProps = {
    isHomePage: true,
    smartBannerLabels: {
      title: "The Children's Place",
      desc: "Open in The Children's Place app",
      viewButton: 'Open',
    },
    isShowAppBannerKillSwitch: true,
    setClickAnalyticsData: jest.fn(),
  };

  it('renders correctly', () => {
    const AppBannerComp = shallow(<AppBanner />);
    expect(AppBannerComp).toMatchSnapshot();
  });
  it('compoent should render if kill switch is true', () => {
    const component = shallow(<AppBanner {...defaultProps} />);
    component.instance().state.showBanner = true;
    expect(component).toMatchSnapshot();
  });

  it('compoent should not render if kill switch is false', () => {
    const component = shallow(<AppBanner {...defaultProps} isShowAppBannerKillSwitch={false} />);
    expect(component).toMatchSnapshot();
  });

  it('canRenderBanner shoule be true', () => {
    const component = shallow(<AppBanner {...defaultProps} isShowAppBannerKillSwitch={false} />);
    expect(component.instance().state.showBanner).toBeFalsy();
  });
  it('showApp should be false', () => {
    const AppBannerComp = shallow(<AppBanner />);
    AppBannerComp.instance().close();
    expect(AppBannerComp.instance().state.showApp).toBeFalsy();
  });
});
