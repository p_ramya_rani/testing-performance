// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import AppBannerContainer, { mapStateToProps } from '../container/AppBanner';

describe('AppBannerContainer container', () => {
  const props = {
    getAppBanner: jest.fn(),
    Labels: {
      global: {
        smartBanner: {
          lbl_smart_banner_title: 'title',
          lbl_smart_banner_desc: 'desc',
          lbl_smart_banner_button: 'view',
        },
      },
    },
    session: {
      siteDetails: {
        IS_PLP_PDP_ANCHORING_ENABLED: true,
      },
    },
  };

  const ownProps = {
    isBrowseAnchoredPage: false,
  };
  it('should render AppBannerContainer', () => {
    const component = shallow(<AppBannerContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should show previously mapStateToProps', () => {
    expect(mapStateToProps(props, ownProps)).toEqual({
      title: 'title',
      desc: 'desc',
      viewButton: 'view',
      isBrowsePageAndAnchored: false,
      isShowAppBannerKillSwitch: false,
    });
  });
});
