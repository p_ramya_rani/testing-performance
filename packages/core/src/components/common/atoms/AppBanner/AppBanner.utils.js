// 9fbef606107a605d69c0edbcd8029e5d 
import { getBrand, getAPIConfig } from '@tcp/core/src/utils';
import appBannerConfig from './config';
/**
 * This will be used to redirect user to android play store link
 */
export const androidPlayStoreLink = () => {
  const androidDeviceType = 'android';
  const brandId = getBrand();
  const { APP_STORE_LINKS, APP_STORE_IDS } = appBannerConfig;
  return `${APP_STORE_LINKS[androidDeviceType]}${APP_STORE_IDS[brandId].appID[androidDeviceType]}`;
};

/**
 * This function will return the IOS app Playstore URl
 */
export const getIOSAppStoreDetail = () => {
  const brandId = getBrand();
  const iosDeviceType = 'ios';
  const { APP_STORE_LINKS, APP_STORE_IDS } = appBannerConfig;
  return `${APP_STORE_LINKS[iosDeviceType]}${APP_STORE_IDS[brandId].appID[iosDeviceType]}`;
};

export const getAppInstallPageUrl = () => {
  const brandId = getBrand();
  const { webDomainName } = getAPIConfig();
  return `${webDomainName}/app-install.html?brand=${brandId}`;
};
