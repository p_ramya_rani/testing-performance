// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components';

export const InfoWrapper = styled.div`
  display: inline-block;
  vertical-align: middle;
  white-space: normal;
  width: calc(99% - 120px);
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy1}px;
  line-height: 1.2em;
  font-weight: ${props => props.theme.fonts.fontWeight.medium};
  color: ${props => props.theme.colorPalette.white};
`;

export const Title = styled.div`
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy2}px;
  line-height: 18px;
  font-weight: ${props => props.theme.fonts.fontWeight.bold};
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  color: ${props => props.theme.colorPalette.gray[400]};
  text-shadow: 0 1px 0 hsla(0, 0%, 100%, 0.8);
`;
export const Description = styled.div`
  max-height: 40px;
  color: ${props => props.theme.colorPalette.gray[800]};
  overflow: hidden;
`;
export const ButtonWrapper = styled.div`
  max-width: 50px;
  display: inline-block;
  text-align: right;
  width: 100%;
`;

export const Container = styled.div`
  margin: 0 auto;
  padding: 7px 5px 0 5px;
`;

export const CloseButton = styled.button`
  vertical-align: middle;
  border-radius: 14px;
  padding: 0;
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy6}px;
  display: inline-block;
  border: none;
  max-width: 17px;
  width: 100%;
  margin-left: 5px;
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
  color: ${props => props.theme.colorPalette.gray[800]};
  background: none;
  text-shadow: 0 1px 0 ${props => props.theme.colorPalette.white};
  cursor: pointer;
`;

export const ViewButton = styled.button`
  margin: auto 0;
  height: 24px;
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy2}px;
  line-height: 24px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  border: none;
  min-width: 12%;
  color: #d1d1d1;
  font-weight: ${props => props.theme.fonts.fontWeight.bold};
  padding: 0;
  background: none;
  border-radius: 0;
  text-shadow: 0 1px 0 ${props => props.theme.colorPalette.white};
  -webkit-font-smoothing: none;
`;
export const ViewButtonText = styled.span`
  text-align: center;
  display: block;
  padding: 0 5px;
  text-transform: uppercase;
  text-shadow: none;
  box-shadow: none;
  color: ${props => (props.theme.isGymboree ? '#f7971f' : props.theme.colorPalette.blue['500'])};
`;
export const Wrapper = styled.div`
  left: 0;
  width: 100%;
  height: 44px;
  font-family: ${props => props.theme.typography.fonts.secondary};
  z-index: ${props => props.theme.zindex.zIndexQuickView};
  -webkit-font-smoothing: antialiased;
  overflow: hidden;
  -webkit-text-size-adjust: none;
  background: ${props => props.theme.colorPalette.gray[300]};
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
`;

export const Icon = styled.span`
  width: 30px;
  height: 30px;
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
  background-size: cover;
  border-radius: 5px;
  display: inline-block;
  vertical-align: middle;
`;

export default css`
  width: 100%;
  .app-banner-wrapper {
    margin-bottom: 1px;
    &.hide-banner {
      display: none;
    }
  }
`;
