// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import {
  getAppBanner,
  getIsPlpPdpAnchoringEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getLabelValue } from '@tcp/core/src/utils';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import AppBanner from '../views';

export const mapStateToProps = (state, ownProps) => {
  const { isBrowseAnchoredPage } = ownProps;
  return {
    title: getLabelValue(state.Labels, 'lbl_smart_banner_title', 'smartBanner', 'global'),
    desc: getLabelValue(state.Labels, 'lbl_smart_banner_desc', 'smartBanner', 'global'),
    viewButton: getLabelValue(state.Labels, 'lbl_smart_banner_button', 'smartBanner', 'global'),
    isShowAppBannerKillSwitch: getAppBanner(state),
    isBrowsePageAndAnchored: getIsPlpPdpAnchoringEnabled(state) && isBrowseAnchoredPage,
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    setClickAnalyticsData: payload => dispatch(setClickAnalyticsData(payload)),
    trackClick: () => dispatch(trackClick()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppBanner);
