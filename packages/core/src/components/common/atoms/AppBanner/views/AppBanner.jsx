// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes, string } from 'prop-types';
import { setSessionStorage, getSessionStorage } from '@tcp/core/src/utils/utils.web';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  getBrand,
  getIconPath,
  getViewportInfo,
  isCanada,
  isGymboree,
  parseBoolean,
} from '@tcp/core/src/utils';
import style, {
  InfoWrapper,
  Title,
  Description,
  ButtonWrapper,
  Container,
  CloseButton,
  ViewButton,
  ViewButtonText,
  Wrapper,
  Icon,
} from '../styles/AppBanner.style';
import appBannerConfig from '../config';
import { getAppInstallPageUrl } from '../AppBanner.utils';

const bannerSessionClose = appBannerConfig.SMART_BANNER_SESSION_CLOSE;
const tcpIcon = getIconPath('tcp-smart-banner-icon');
const GymIcon = getIconPath('gym-smart-banner-icon');

/**
 * @description
 * Display Rules:
 * The banner will auto display on the homepage or the first page the user lands on when they reach the website
 * If the first page the user sees is not the homepage then the banner should display on page load of that first page only and the remaining display rules apply
 * The banner will not display on Bag and Checkout pages
 * The banner will live on remaining pages of the website but will only display if the user scrolls up to the top of the screen (not on page load)
 * If the user actively closes the banner, it should no longer display at all in the users' current session
 * The banner should redisplay with a fresh session
 */
class AppBanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showBanner: false,
    };
  }

  componentDidMount() {
    this.canRenderBanner();
  }

  componentDidUpdate(prevProps) {
    const { asPath: prevAsPath } = prevProps;
    const { asPath } = this.props;
    if (prevAsPath !== asPath) {
      this.reInitializeAppBanner();
    }
  }

  /**
   * @method reInitializeAppBanner
   * @description hide banner from load and then decides based on display rules
   */
  reInitializeAppBanner = () => {
    this.setState({
      showBanner: false,
    });
    // Intermittently, banner was not hiding on subsequest page visits, removed set state callback and added timeout fixed the issue.
    const renderBannerTimeout = setTimeout(() => {
      this.canRenderBanner();
      clearTimeout(renderBannerTimeout);
    }, 0);
  };

  canRenderBanner = () => {
    if (!isCanada()) {
      const { isMobile } = getViewportInfo();
      const { SMART_BANNER_PAGELOAD_DONE } = appBannerConfig;
      const isBannerLoaded = parseBoolean(getSessionStorage(SMART_BANNER_PAGELOAD_DONE));
      const { isShowAppBannerKillSwitch } = this.props;
      if (isMobile && isShowAppBannerKillSwitch) {
        const isSessionClose = this.isBannerSessionClose(bannerSessionClose);
        if (isBannerLoaded) {
          this.showBannerOnScrollUp(this, isSessionClose);
        } else {
          this.setState({
            showBanner: !isSessionClose,
          });
          setSessionStorage({ key: SMART_BANNER_PAGELOAD_DONE, value: true });
        }
      }
    }
  };

  /**
   * @method showBannerOnScrollUp
   * @description show banner on scroll up from pages other than home pages
   */
  showBannerOnScrollUp = (component, isSessionClose) => {
    if (window) {
      let prevScrollpos = window.pageYOffset;
      // on PDP and other pages we have auto scroll, which is making windows scoll event to call. To avoid for banner
      // added bannerScroll flag
      let bannerScroll = true;
      const { isBrowsePageAndAnchored } = this.props;
      window.onscroll = () => {
        const currentScrollPos = window.pageYOffset;
        if (
          prevScrollpos > currentScrollPos &&
          bannerScroll &&
          (!isBrowsePageAndAnchored || (isBrowsePageAndAnchored && currentScrollPos < 200))
        ) {
          component.setState({
            showBanner: !isSessionClose,
          });
          bannerScroll = false;
        }
        prevScrollpos = currentScrollPos;
      };
    }
  };

  /**
   * This function will be used to indentify whether the cookie exist or not
   */
  isBannerSessionClose = name => parseBoolean(getSessionStorage(name));

  /**
   * This function will be used to close the smart banner
   */
  close = () => {
    this.setState(
      {
        showBanner: false,
      },
      () => setSessionStorage({ key: bannerSessionClose, value: true })
    );
  };

  /**
   * This will be used to redirect user to android play store
   */
  install = () => {
    const { setClickAnalyticsData, trackClick } = this.props;
    setClickAnalyticsData({
      customEvents: isGymboree() ? ['event171'] : ['event170'],
      eventName: 'download promo: header',
      appDownloadLocation: 'Header',
    });
    trackClick();
    const analyticsTimer = setTimeout(() => {
      setClickAnalyticsData({});
      window.location = getAppInstallPageUrl();
      clearTimeout(analyticsTimer);
    }, 100);
  };

  render() {
    const { showBanner } = this.state;
    if (!showBanner) {
      return null;
    }
    const showAppBanner =
      window && window.location && window.location.href.indexOf('hideHeaderFooter=true') > -1;
    const icon = getBrand() === 'tcp' ? tcpIcon : GymIcon;
    const { className, title, desc, viewButton } = this.props;
    const iconStyle = {
      backgroundImage: `url(${icon})`,
    };
    return (
      <div className={className}>
        <Wrapper className={`app-banner-wrapper ${showAppBanner ? 'hide-banner' : ''}`}>
          <Container>
            <CloseButton type="button" aria-label="close" onClick={this.close}>
              &times;
            </CloseButton>
            <Icon style={iconStyle} />
            <InfoWrapper>
              <Title>{title}</Title>
              <Description>{desc}</Description>
            </InfoWrapper>
            <ButtonWrapper>
              <ViewButton onClick={this.install}>
                <ViewButtonText>{viewButton}</ViewButtonText>
              </ViewButton>
            </ButtonWrapper>
          </Container>
        </Wrapper>
      </div>
    );
  }
}

AppBanner.defaultProps = {
  className: '',
  asPath: '',
  isShowAppBannerKillSwitch: false,
  setClickAnalyticsData: () => {},
  trackClick: () => {},
  isBrowsePageAndAnchored: false,
};

AppBanner.propTypes = {
  className: PropTypes.string,
  title: string.isRequired,
  desc: string.isRequired,
  viewButton: string.isRequired,
  asPath: PropTypes.string,
  isShowAppBannerKillSwitch: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func,
  trackClick: PropTypes.func,
  isBrowsePageAndAnchored: PropTypes.bool,
};

export default withStyles(AppBanner, style);
export { AppBanner as AppBannerVanilla };
