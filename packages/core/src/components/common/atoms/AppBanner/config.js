// 9fbef606107a605d69c0edbcd8029e5d 
const APP_STORE_LINKS = {
  android: 'https://play.google.com/store/apps/details?id=',
  ios: 'https://apps.apple.com/us/app/the-childrens-place/id',
};
const APP_STORE_IDS = {
  tcp: {
    appID: {
      ios: '1292363109',
      android: 'com.childrensplace.tcpmobileapp',
    },
    intentUrl: 'tcp://',
  },
  gym: {
    appID: {
      ios: '1495871937',
      android: 'com.childrensplace.gymmobileapp',
    },
    intentUrl: 'gym://',
  },
};

const SMART_BANNER_SESSION_CLOSE = 'smart-banner-session-close';
const SMART_BANNER_PAGELOAD_DONE = 'smart-banner-pageload-done';

export default {
  SMART_BANNER_SESSION_CLOSE,
  APP_STORE_LINKS,
  APP_STORE_IDS,
  SMART_BANNER_PAGELOAD_DONE,
};
