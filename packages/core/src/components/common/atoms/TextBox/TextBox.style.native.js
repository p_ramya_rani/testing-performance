// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

const StyledTextBoxWrapper = styled.View`
  margin-bottom: ${(props) => (props.marginBottom ? props.theme.spacing.ELEM_SPACING.LRG : 0)};
`;

const getInputBottomColor = (props) => {
  const { theme, meta } = props;
  const { colorPalette } = theme;
  const { dirty, error } = meta;
  const borderColor = dirty && error !== undefined ? colorPalette.error : colorPalette.gray[1500];

  return `
  border-bottom-color: ${borderColor};
  border-top-color: ${borderColor};
  border-left-color: ${borderColor};
  border-right-color: ${borderColor};
  `;
};

const StyledTextBoxReDesign = styled.TextInput`
  background-color: transparent;
  border-bottom-width: 1px;
  border-top-width: 1px;
  border-right-width: 1px;
  border-left-width: 1px;
  height: ${(props) => (props.isCVVField ? '28px' : '55px')};
  padding-bottom: 17px;
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding-top: 17px;
  top: 12px;
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  border-radius: 12px;
`;

const TextBoxStyle = css`
  ${getInputBottomColor};
`;

const StyledTextBox = styled.TextInput`
  border-bottom-width: 1px;
  height: ${(props) => (props.isCVVField ? '28px' : '40px')};
  padding-top: ${(props) => (props.isCVVField ? '0' : props.theme.spacing.ELEM_SPACING.MED)};
  padding-bottom: 0;
  padding-left: 0;
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-weight: ${(props) => props.theme.typography.fontWeights.regular};
  border-bottom-color: ${(props) =>
    props.meta.touched && props.meta.error
      ? props.theme.colorPalette.error
      : props.theme.colorPalette.gray[600]};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  ${(props) => (props.disabled ? `background-color: ${props.theme.colors.DISABLED_BG};` : '')};
`;

const StyledLabelReDesign = styled.Text`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  position: absolute;
  left: 0;
  top: ${(props) => (!props.isFocused ? props.theme.spacing.ELEM_SPACING.MED : '0')};
  background: ${(props) => (props.isFocused ? props.theme.colorPalette.white : 'transparent')};
  z-index: ${(props) => (props.isFocused ? 2 : 0)};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.typography.fontSizes.fs14};
  color: ${(props) =>
    props.isFocused ? props.theme.colorPalette.blue[800] : props.theme.colorPalette.gray[800]};
  font-weight: ${(props) =>
    !props.isFocused
      ? props.theme.typography.fontWeights.bold
      : props.theme.typography.fontWeights.extrabold};
  margin-top: ${(props) => (props.isFocused ? props.theme.spacing.ELEM_SPACING.XXS : 12)};
`;

const StyledLabel = styled.Text`
  position: absolute;
  left: 0;
  top: ${(props) => (!props.isFocused ? props.theme.spacing.ELEM_SPACING.MED : '0')};
  font-weight: ${(props) =>
    !props.isFocused
      ? props.theme.typography.fontWeights.regular
      : props.theme.typography.fontWeights.extrabold};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) =>
    !props.isFocused
      ? props.theme.typography.fontSizes.fs14
      : props.theme.typography.fontSizes.fs10};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  margin-bottom: ${(props) => (props.isFocused ? props.theme.spacing.ELEM_SPACING.XXS : '0')};
`;

const StyledErrorIcon = styled.View`
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const StyledSuccessCheck = styled.View`
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

const StyledErrorWrapper = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  display: flex;
  flex-direction: row;
  align-items: ${(props) => (props.isCVVFieldError ? 'stretch' : 'center')};
  width: 90%;
  ${(props) => (props.errorStyle ? props.errorStyle : '')}
`;

const StyledSuccessIcon = styled.View`
  position: absolute;
  right: 0;
  top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

const HiddenView = styled.View`
  display: none;
`;

export {
  TextBoxStyle,
  StyledTextBox,
  StyledLabel,
  StyledErrorIcon,
  StyledErrorWrapper,
  StyledTextBoxWrapper,
  StyledSuccessIcon,
  HiddenView,
  StyledSuccessCheck,
  StyledTextBoxReDesign,
  StyledLabelReDesign,
};
