// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const getDefaultFonTSize = (props) => {
  const { input, spc, theme } = props;
  let fontSize;
  if (input && input.value) {
    if (spc) {
      fontSize = theme.fonts.fontSize.body.bodytext.copy3;
    } else {
      fontSize = theme.fonts.fontSize.body.small.primary;
    }
  } else if (spc) {
    fontSize = theme.fonts.fontSize.textbox_input;
  } else {
    fontSize = theme.fonts.fontSize.textbox;
  }
  return fontSize;
};

const getLabelTopValue = (props) => {
  const { input, spc, theme } = props;
  let topValue = theme.spacing.ELEM_SPACING.LRG;
  if (input && input.value) {
    topValue = theme.spacing.ELEM_SPACING.XXS;
  } else if (spc) {
    topValue = theme.spacing.ELEM_SPACING.MED;
  }
  return topValue;
};

const getErrorBorder = (props) => {
  const { meta, theme, spc } = props;
  let borderValue;
  const errorVerify = meta && meta.touched && meta.error;
  if (errorVerify && spc) {
    borderValue = `border: 1px solid ${theme.colors.NOTIFICATION.ERROR};`;
  } else if (errorVerify) {
    borderValue = `border-bottom: 1px solid ${theme.colors.NOTIFICATION.ERROR};`;
  }
  return borderValue;
};

const textboxStyles = css`
  position: relative;
  display: ${(props) => (props.spc && props.variantWidth ? 'inline-block' : 'block')};
  width: ${(props) => (props.spc && props.variantWidth ? '100%' : '')};

  &.input-fields-wrapper {
    ${(props) =>
      props.spc &&
      `
      margin-right: 32px;
    `}
  }

  .TextBox__label {
    font-size: ${(props) => `${getDefaultFonTSize(props)}px;`};
    padding: 0;
    position: absolute;
    top: ${(props) => getLabelTopValue(props)};
    ${(props) =>
      props.input &&
      props.input.value &&
      `
      font-weight: ${props.theme.fonts.fontWeight.bold};
    `}
    left: ${(props) => (props.spc ? props.theme.spacing.ELEM_SPACING.MED : '')};
    &.nonActive {
      ${(props) => `
          font-size: ${props.theme.fonts.fontSize.body.bodytext.copy3}px;
          top: -${props.theme.spacing.ELEM_SPACING.SM};
          left: ${props.theme.spacing.ELEM_SPACING.SM};
          background: ${props.theme.colorPalette.white};
          color: ${props.theme.colorPalette.blue[800]};
          padding: ${props.theme.spacing.ELEM_SPACING.XXXS} ${
        props.theme.spacing.ELEM_SPACING.XS_6
      };
          color: ${
            props.spc && !((props.meta?.touched && props.meta?.error) || props.showExplicitError)
              ? props.theme.colorPalette.blue[800]
              : props.theme.colorPalette.red[500]
          };
        `};
    }
  }
  .TextBox__placeholder {
    color: ${(props) =>
      props.isAutoSuggest
        ? props.theme.colorPalette.blue[800]
        : props.theme.colorPalette.gray[800]};
  }

  .TextBox__view {
    font-size: 16px;
    position: absolute;
    top: 10px;
    padding: 5px;
    color: ${(props) => props.theme.colors.TEXTBOX.COLOR};
  }
  .TextBox__input {
    margin: 0;
    outline: 0;
    font-size: ${(props) => props.theme.fonts.fontSize.textbox_input}px;
    color: ${(props) => props.theme.colors.TEXTBOX.COLOR};
    width: ${(props) => (props.spc && props.variantWidth ? '90%' : '100%')};
    background-position: left top;
    background-repeat: no-repeat;
    background-size: contain;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};

    ${(props) =>
      props.spc &&
      `
      height: 20px;
    `}

    ${(props) =>
      props.spc
        ? `
        border: 1px solid ${
          !((props.touched && props.error) || props.showExplicitError)
            ? props.theme.colors.FOOTER.DIVIDER
            : props.theme.colorPalette.red[500]
        };
        padding: ${props.theme.spacing.ELEM_SPACING.MED} ${props.theme.spacing.ELEM_SPACING.MED};
        border-radius: ${props.theme.spacing.ELEM_SPACING.XS_6};
        background: ${props.theme.colorPalette.white};
      `
        : `border: 0 solid transparent;
        border-bottom: 1px solid ${props.theme.colors.FOOTER.DIVIDER};
        padding-bottom: ${props.theme.spacing.ELEM_SPACING.XS};
        padding-top: ${props.theme.spacing.ELEM_SPACING.LRG};
        `};

    ${(props) =>
      props.input &&
      props.input.value &&
      props.spc &&
      `
      border-color: ${props.theme.colorPalette.blue[800]};
    `}

    ${(props) => getErrorBorder(props)}

    ${(props) =>
      props.disabled
        ? `
      background-color: ${props.theme.fieldBackgroundDisabledColor};
      border-color: ${props.theme.fieldBorderDisabledColor};
    `
        : ''};

    &:focus ~ .TextBox__label {
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      ${(props) =>
        props.spc
          ? `
          font-size: ${props.theme.fonts.fontSize.body.bodytext.copy3}px;
          top: -${props.theme.spacing.ELEM_SPACING.SM};
          left: ${props.theme.spacing.ELEM_SPACING.SM};
          background: ${props.theme.colorPalette.white};
          color: ${
            !((props.meta?.touched && props.meta?.error) || props.showExplicitError)
              ? props.theme.colorPalette.blue[800]
              : props.theme.colorPalette.red[500]
          };
          padding: ${props.theme.spacing.ELEM_SPACING.XXXS} ${props.theme.spacing.ELEM_SPACING.XS_6}
      `
          : `top: ${props.theme.spacing.ELEM_SPACING.XXS};
          font-size: ${props.theme.fonts.fontSize.body.small.primary}px;`};
    }
    &:focus {
      border-color: ${(props) =>
        !((props.meta?.touched && props.meta?.error) || props.showExplicitError)
          ? props.theme.colorPalette.blue[800]
          : props.theme.colorPalette.red[500]};

      ${(props) =>
        props.spc &&
        props.isAutoSuggestOpen &&
        `
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
      border-bottom-color: ${props.theme.colors.WHITE};
    `}
    }
  }

  &.textbox_validation_success .checkmark-container {
    ${(props) =>
      props.spc
        ? `display: block;
    width: 24px;
    height: 24px;
    margin-left: 2px;
    transform-origin: left;
    position: absolute;
    right: 0;
    top: 15px;
    border: 1px solid ${props.theme.colors.TEXTBOX.SUCCESS_BORDER};
    border-radius: 50%;`
        : ``}
  }

  .success__checkmark {
    display: none;
  }
  &.textbox_validation_success .TextBox__input {
    ${(props) =>
      props.spc
        ? `border: 1px solid ${props.theme.colors.TEXTBOX.SUCCESS_BORDER};`
        : `border-bottom: 1px solid ${props.theme.colorPalette.success};`};
  }

  &.textbox_validation_success .success__checkmark {
    display: ${(props) => (props.input && props.type === 'hidden' ? 'none' : 'block')};
    width: 15px;
    height: 8px;
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    transform: rotate(-45deg);
    transform-origin: left;
    position: absolute;
    right: ${(props) => (props.checkMarkRightMargin ? props.checkMarkRightMargin : '2px')};
    top: ${(props) => (props.spc ? '12px' : '30px')};

    &:before {
      content: '';
      position: absolute;
      width: 3px;
      height: 100%;
      background-color: ${(props) => props.theme.colors.TEXTBOX.SUCCESS_BORDER};
    }

    &:after {
      content: '';
      position: absolute;
      width: 100%;
      height: 3px;
      background-color: ${(props) => props.theme.colors.TEXTBOX.SUCCESS_BORDER};
      bottom: 0;
    }

    &.success__checkmark--date {
      right: 25px;
      top: 35px;
    }
  }

  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};

  .TextBox__error {
    display: flex;
    flex-direction: row;
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
  .warning-icon {
    background: transparent url('${(props) => getIconPath('circle-alert-fill', props)}') no-repeat 0
      0;
    background-size: contain;
    border: none;
    height: 14px;
    width: 16px;
    margin-right: 7px;
    flex-shrink: 0;
  }

  .custom-icon {
    display: none;
    background-size: contain;
    border: none;
    height: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    width: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    margin-right: 7px;
    flex-shrink: 0;
    position: absolute;
    top: 22%;
    right: -${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }

  .custom-search-icon {
    display: block;
    background: transparent url('${(props) => getIconPath('search-icon', props)}') no-repeat 0 0;
  }

  .input-right-err-icon {
    background: transparent url('${(props) => getIconPath('icons-error-message', props)}') no-repeat
      0 0;
    background-size: contain;
    border: none;
    height: 16px;
    width: 16px;
    margin-right: 7px;
    flex-shrink: 0;
    position: absolute;
    top: 25%;
    right: -7%;
  }

  .hide-err {
    display: none;
  }

  .TextBox_view {
    border: 1px solid gray;
    border-radius: 4px;
    height: 140px;
    width: 100%;
    margin-bottom: 10px;
  }
  .TextBox_view_border {
    border: 1px solid red;
    border-radius: 4px;
    height: 140px;
    width: 100%;
    margin-bottom: 10px;
  }
  .TextBox__input[type='hidden'] ~ .checkmark-container {
    display: none;
  }
`;

export default textboxStyles;
