// 9fbef606107a605d69c0edbcd8029e5d
const BUTTON_VARIATION = {
  miniNav: 'mini-nav',
  mobileAppFilter: 'mobileApp-filter',
  mobileAppFilterIcon: 'mobileApp-filter-icon',
  mobileAppSelect: 'mobileApp-select',
  successButton: 'success-button',
  linkButton: 'link-button',
  borderless: 'border-less',
  mobileAppRoundedFilter: 'mobileApp-rounded-filter',
};

const BUTTON_FILL = {
  BLUE: 'BLUE',
  DARK: 'DARK',
  BLACK: 'BLACK',
};

export { BUTTON_VARIATION, BUTTON_FILL };
