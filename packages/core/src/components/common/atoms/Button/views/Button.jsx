// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import ButtonCTA from '../../../molecules/ButtonCTA';
import withStyles from '../../../hoc/withStyles';
import styles from '../Button.style';
import ButtonSpinner from '../../ButtonSpinner';

/**
 * @param {object} props : Props for button
 * @desc This is a button component. The two variations of buttons are:
 * 1. fixed-width: Takes the width of the column which it occupies.
 * It has the fixed padding as per the zeplin.

 * 2. variable-width: Takes the width of the text that is inside the button.
 * It has fixed padding as per the zeplin. This variation needs to be mentioned in buttonVariation property.
 * TODO - Not able to add these property here due to linting,
 * need to find a way of doing it. Might be resolved with flow types.

 * Additional button Prop:
 * fullWidth: Additional property to mention 100% width of the button.
 * disabled: to have disabled state of the button
 */
const Button = ({
  children,
  className,
  ariaLabel,
  disabled,
  fullWidth,
  dataLocator,
  type,
  customStyle,
  buttonVariation,
  cta,
  uniqueKey,
  noCurve,
  active,
  loading,
  borderRadius,
  loaderLeft,
  ...otherProps
}) => {
  if (!cta) {
    /* Using this label in CSS to fake the width of hover style category-link-button.
       It allows to stop flickering of the button on hover.
    */
    let categoryLinkLabel = '';
    if (buttonVariation === 'category-links-dark') {
      categoryLinkLabel = children;
    }
    return (
      <button
        disabled={disabled}
        aria-label={ariaLabel}
        className={className}
        type={type}
        fullWidth={fullWidth}
        data-locator={dataLocator}
        data-category-link-label={categoryLinkLabel}
        {...otherProps}
      >
        <ButtonSpinner loading={loading} loaderLeft={loaderLeft} />
        {children}
      </button>
    );
  }
  return (
    <ButtonCTA
      uniqueKey={uniqueKey}
      className={className}
      dataLocator={dataLocator}
      ariaLabel={ariaLabel}
      type={type}
      fullWidth={fullWidth}
      disabled={disabled}
      noCurve={noCurve}
      ctaInfo={{
        ctaVariation: buttonVariation,
        link: {
          ...cta,
        },
      }}
    />
  );
};

Button.propTypes = {
  children: PropTypes.element.isRequired,
  className: PropTypes.string.isRequired,
  ariaLabel: PropTypes.string.isRequired,
  disabled: PropTypes.string,
  fullWidth: PropTypes.string,
  dataLocator: PropTypes.string.isRequired,
  type: PropTypes.string,
  customStyle: PropTypes.string,
  buttonVariation: PropTypes.string.isRequired,
  cta: PropTypes.shape({
    url: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    target: PropTypes.string,
  }),
  uniqueKey: PropTypes.string,
  noCurve: PropTypes.bool,
  active: PropTypes.bool,
  loading: PropTypes.bool,
  borderRadius: PropTypes.number,
  loaderLeft: PropTypes.bool,
};

Button.defaultProps = {
  disabled: '',
  fullWidth: true,
  type: 'button',
  customStyle: '',
  uniqueKey: '',
  noCurve: false,
  cta: null,
  active: false,
  loading: false,
  borderRadius: 0,
  loaderLeft: false,
};

export default withStyles(Button, styles);
export { Button as ButtonVanilla };
