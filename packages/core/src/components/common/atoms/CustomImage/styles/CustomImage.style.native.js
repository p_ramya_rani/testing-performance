// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const getImageStyle = props => {
  const { resizeMode, styles = {} } = props;
  if (styles) {
    const { width, height } = styles;
    return `
      width: ${width};
      height: ${height};
      resize-mode: ${resizeMode};
    `;
  }
  return `
    resize-mode: ${resizeMode};
  `;
};

// border: 1px solid #00ff00;

const ImageContainer = styled.Image`
  ${getImageStyle}
`;

export default ImageContainer;
