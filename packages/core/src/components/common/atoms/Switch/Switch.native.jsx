import React, { Component } from 'react';
import { StyleSheet, Animated, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { SwitchActiveTextStyle, SwitchInActiveTextStyle } from './Switch.style.native';

const white = '#fff';
const black = '#000';
const styles = StyleSheet.create({
  animatedContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    width: 78,
  },
  circle: {
    backgroundColor: white,
    borderRadius: 15,
    height: 30,
    width: 30,
  },
  container: {
    backgroundColor: black,
    borderRadius: 30,
    height: 30,
    width: 71,
  },
});

export default class CustomSwitch extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      value: props.value,
      transformSwitch: new Animated.Value(
        props.value
          ? props.circleSize / props.switchLeftPx
          : -props.circleSize / props.switchRightPx
      ),
      backgroundColor: new Animated.Value(props.value ? 75 : -75),
      circleColor: new Animated.Value(props.value ? 75 : -75),
      circleBorderColor: new Animated.Value(props.value ? 75 : -75),
    };
  }

  componentDidUpdate(prevProps) {
    const { value, disabled } = this.props;
    if (prevProps.value === value) {
      return;
    }
    if (prevProps.disabled && disabled === prevProps.disabled) {
      return;
    }

    this.animateSwitch(value, () => this.setState({ value }));
  }

  handleSwitch = () => {
    const { value } = this.state;
    const { onValueChange, disabled, changeValueImmediately, value: propValue } = this.props;
    if (disabled) {
      return;
    }
    if (propValue === value) {
      onValueChange(!value);
      return;
    }

    if (changeValueImmediately) {
      this.animateSwitch(!propValue);
      onValueChange(!propValue);
    } else {
      this.animateSwitch(!value, () =>
        this.setState({ value: !value }, () => onValueChange(value))
      );
    }
  };

  animateSwitch = (value, cb = () => {}) => {
    const { transformSwitch, backgroundColor, circleColor, circleBorderColor } = this.state;
    const { circleSize, switchLeftPx, switchRightPx } = this.props;
    Animated.parallel([
      Animated.spring(transformSwitch, {
        toValue: value ? circleSize / switchLeftPx : -circleSize / switchRightPx,
        useNativeDriver: false,
      }),
      Animated.timing(backgroundColor, {
        toValue: value ? 75 : -75,
        duration: 200,
        useNativeDriver: false,
      }),
      Animated.timing(circleColor, {
        toValue: value ? 75 : -75,
        duration: 200,
        useNativeDriver: false,
      }),
      Animated.timing(circleBorderColor, {
        toValue: value ? 75 : -75,
        duration: 200,
        useNativeDriver: false,
      }),
    ]).start(cb);
  };

  render() {
    const { transformSwitch, backgroundColor, circleColor, circleBorderColor } = this.state;

    const {
      backgroundActive,
      backgroundInactive,
      circleActiveColor,
      circleInActiveColor,
      activeText,
      inActiveText,
      circleSize,
      containerStyle,
      activeTextStyle,
      inactiveTextStyle,
      barHeight,
      circleBorderInactiveColor,
      circleBorderActiveColor,
      circleBorderWidth,
      innerCircleStyle,
      outerCircleStyle,
      renderActiveText,
      renderInActiveText,
      renderInsideCircle,
      switchWidthMultiplier,
      switchBorderRadius,
      value,
      ...restProps
    } = this.props;

    const interpolatedColorAnimation = backgroundColor.interpolate({
      inputRange: [-75, 75],
      outputRange: [backgroundInactive, backgroundActive],
    });

    const interpolatedCircleColor = circleColor.interpolate({
      inputRange: [-75, 75],
      outputRange: [circleInActiveColor, circleActiveColor],
    });

    const interpolatedCircleBorderColor = circleBorderColor.interpolate({
      inputRange: [-75, 75],
      outputRange: [circleBorderInactiveColor, circleBorderActiveColor],
    });

    return (
      <TouchableWithoutFeedback
        accessibilityRole="switch"
        onPress={this.handleSwitch}
        {...restProps}
      >
        <Animated.View
          style={[
            styles.container,
            containerStyle,
            {
              backgroundColor: interpolatedColorAnimation,
              width: circleSize * switchWidthMultiplier,
              height: barHeight || circleSize,
              borderRadius: switchBorderRadius || circleSize,
            },
          ]}
        >
          <Animated.View
            style={[
              styles.animatedContainer,
              {
                left: transformSwitch,
                width: circleSize * switchWidthMultiplier,
              },
              outerCircleStyle,
            ]}
          >
            {value && renderActiveText && (
              <SwitchActiveTextStyle text={activeText} activeTextStyle={activeTextStyle} />
            )}

            <Animated.View
              style={[
                styles.circle,
                {
                  borderWidth: circleBorderWidth,
                  borderColor: interpolatedCircleBorderColor,
                  backgroundColor: interpolatedCircleColor,
                  width: circleSize,
                  height: circleSize,
                  borderRadius: circleSize / 2,
                },
                innerCircleStyle,
              ]}
            >
              {renderInsideCircle()}
            </Animated.View>
            {!value && renderInActiveText && (
              <SwitchInActiveTextStyle text={inActiveText} inactiveTextStyle={inactiveTextStyle} />
            )}
          </Animated.View>
        </Animated.View>
      </TouchableWithoutFeedback>
    );
  }
}

CustomSwitch.propTypes = {
  onValueChange: PropTypes.func,
  disabled: PropTypes.bool,
  activeText: PropTypes.string,
  inActiveText: PropTypes.string,
  backgroundActive: PropTypes.string,
  backgroundInactive: PropTypes.string,
  value: PropTypes.bool,
  circleActiveColor: PropTypes.string,
  circleInActiveColor: PropTypes.string,
  circleSize: PropTypes.number,
  circleBorderActiveColor: PropTypes.string,
  circleBorderInactiveColor: PropTypes.string,
  activeTextStyle: PropTypes.shape.isRequired,
  inactiveTextStyle: PropTypes.shape.isRequired,
  containerStyle: PropTypes.shape.isRequired,
  barHeight: PropTypes.number,
  circleBorderWidth: PropTypes.number,
  innerCircleStyle: PropTypes.shape,
  renderInsideCircle: PropTypes.func,
  changeValueImmediately: PropTypes.bool,
  outerCircleStyle: PropTypes.shape,
  renderActiveText: PropTypes.bool,
  renderInActiveText: PropTypes.bool,
  switchLeftPx: PropTypes.number,
  switchRightPx: PropTypes.number,
  switchWidthMultiplier: PropTypes.number,
  switchBorderRadius: PropTypes.number,
};

CustomSwitch.defaultProps = {
  value: false,
  onValueChange: () => null,
  renderInsideCircle: () => null,
  disabled: false,
  activeText: 'On',
  inActiveText: 'Off',
  backgroundActive: 'green',
  backgroundInactive: 'gray',
  circleActiveColor: 'white',
  circleInActiveColor: 'white',
  circleBorderActiveColor: 'rgb(100, 100, 100)',
  circleBorderInactiveColor: 'rgb(80, 80, 80)',
  circleSize: 30,
  barHeight: null,
  circleBorderWidth: 1,
  changeValueImmediately: true,
  innerCircleStyle: {},
  outerCircleStyle: {},
  renderActiveText: true,
  renderInActiveText: true,
  switchLeftPx: 2,
  switchRightPx: 2,
  switchWidthMultiplier: 2,
  switchBorderRadius: null,
};
