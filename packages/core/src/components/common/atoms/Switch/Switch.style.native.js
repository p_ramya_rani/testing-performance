// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import BodyCopy from '../BodyCopy';

const SwitchActiveTextStyle = styled(BodyCopy)`
  background-color: transparent;
  color: ${(props) => props.theme.colors.WHITE};
  ${(props) => (props.activeTextStyle ? { ...props.activeTextStyle } : '')};
`;

const SwitchInActiveTextStyle = styled(BodyCopy)`
  background-color: transparent;
  color: ${(props) => props.theme.colors.WHITE};
  ${(props) => (props.inactiveTextStyle ? { ...props.inactiveTextStyle } : '')};
`;

export { SwitchActiveTextStyle, SwitchInActiveTextStyle };
