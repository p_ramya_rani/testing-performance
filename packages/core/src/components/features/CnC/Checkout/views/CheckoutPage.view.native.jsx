// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import CookieManager from 'react-native-cookies';
import { isIOS, setValueInAsyncStorage, getValueFromAsyncStorage } from '@tcp/core/src/utils';
import { propTypes, defaultProps } from './CheckoutPage.utils.native';
import CheckoutConstants from '../Checkout.constants';
import PickupPage from '../organisms/PickupPage';
import ShippingPage from '../organisms/ShippingPage';
import BillingPage from '../organisms/BillingPage';
import ReviewPage from '../organisms/ReviewPage';
import { WithKeyboardAvoidingViewHOC } from '../../../../common/hoc/withKeyboardAvoidingView.native';
import Confirmation from '../../Confirmation';
import { getLabelValue } from '../../../../../utils';

export class CheckoutPage extends React.PureComponent {
  mergeCartMessageShown = false;

  componentDidMount() {
    const { navigation, toggleCheckoutFocus } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      toggleCheckoutFocus(true);
    });
  }

  componentDidUpdate(prevProps) {
    const { currentStage: prevCurrentStage, checkoutServerError: prevCheckoutServerError } =
      prevProps;
    const { setCheckoutStage, currentStage, checkoutServerError, toastMessage } = this.props;
    if (currentStage !== prevCurrentStage) {
      setCheckoutStage(currentStage);
    }

    if (checkoutServerError && checkoutServerError !== prevCheckoutServerError) {
      toastMessage(checkoutServerError.errorMessage);
    }
  }

  componentWillUnmount() {
    // Remove the event listener & remove checkout focuse flag to false
    const { toggleCheckoutFocus, setAfterPayProgress } = this.props;
    this.focusListener.remove();
    toggleCheckoutFocus(false);
    setAfterPayProgress(false);
  }

  submitShippingSection = (data) => {
    const { submitShippingSection, navigation } = this.props;
    submitShippingSection({ ...data, navigation });
  };

  scrollToTop = () => {
    if (this.keyBoardAvoidRef !== null && this.keyBoardAvoidRef) {
      this.keyBoardAvoidRef.scrollToPosition(0, 0, false);
    }
  };

  showMergeCartLimitError = () => {
    const {
      labels,
      showMaxItemError,
      toastMessage,
      currentStage,
      currentUserId,
      cartOrderItemsCount,
      showMaxItemErrorDisplayed,
      setMaxItemErrorAction,
      setMaxItemErrorDisplayedAction,
      isGuest,
    } = this.props;
    if (
      !isGuest &&
      showMaxItemError &&
      !showMaxItemErrorDisplayed &&
      !this.mergeCartMessageShown &&
      cartOrderItemsCount &&
      currentStage.toLowerCase() !== CheckoutConstants.CHECKOUT_STAGES.CONFIRMATION
    ) {
      const mergeCartLimitErrorMsg = getLabelValue(
        labels,
        'lbl_coupon_max_quantity_error',
        'bagPage',
        'checkout'
      );
      this.mergeCartMessageShown = true;
      if (isIOS()) {
        toastMessage(mergeCartLimitErrorMsg);
        CookieManager.clearByName(`MC_${currentUserId}`);
      } else {
        getValueFromAsyncStorage('mergeCartMessageShown').then((result) => {
          if (!result) {
            toastMessage(mergeCartLimitErrorMsg);
            setValueInAsyncStorage('mergeCartMessageShown', 'true'); // Don't show on app restart, as cookie was not deleting on android
          }
        });
      }
      setMaxItemErrorAction(false); // Don't show toast msg in other pages once shown
      setMaxItemErrorDisplayedAction(true);
    }
  };

  getCurrentPage = () => {
    const {
      isGuest,
      isMobile,
      isUsSite,
      onEditModeChange,
      isSmsUpdatesEnabled,
      currentPhoneNumber,
      navigation,
      shippingProps,
      billingProps,
      reviewProps,
      orderHasShipping,
      loadShipmentMethods,
      orderHasPickUp,
      isOrderUpdateChecked,
      isAlternateUpdateChecked,
      pickUpLabels,
      smsSignUpLabels,
      pickupInitialValues,
      bagLoading,
      availableStages,
      labels,
      submitBilling,
      updateShippingMethodSelection,
      updateShippingAddressData,
      addNewShippingAddressData,
      onPickupSubmit,
      formatPayload,
      verifyAddressAction,
      submitVerifiedShippingAddressData,
      submitReview,
      currentStage,
      setCheckoutStage,
      isVenmoPaymentInProgress,
      setVenmoPickupState,
      setVenmoShippingState,
      isVenmoPickupBannerDisplayed,
      isVenmoShippingBannerDisplayed,
      isExpressCheckout,
      pickUpContactAlternate,
      isHasPickUpAlternatePerson,
      pickUpAlternatePerson,
      cartOrderItemsCount,
      checkoutPageEmptyBagLabels,
      pickupDidMount,
      isPayPalWebViewEnable,
      pickUpContactPerson,
      isBonusPointsEnabled,
      initShippingPage,
      smsOrderUpdatesRichText,
      checkoutProgressBarLabels,
      formattedProductsData,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      persistEddNodeAction,
      itemsWithEdd,
      selectedShippingCode,
      isEDD,
      updateEdd,
      eddAllowedStatesArr,
      currentOrderId,
      cartOrderItems,
      clearCheckoutServerError,
      addItemToSflList,
      resetShippingEddData,
      checkoutServerError,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      setAfterPayProgress,
      giftCardTotal,
      orderBalanceTotal,
      trackClickAfterPay,
      setAnalyticsTriggered,
      setCouponRemovedFromOrderFlag,
      couponRemovedFromOrderFlag,
      toastMessage,
    } = this.props;
    const { PICKUP, SHIPPING, BILLING, REVIEW, CONFIRMATION } = CheckoutConstants.CHECKOUT_STAGES;
    const { venmoBannerText } = pickUpLabels;
    const { shipmentMethods, shippingAddress } = shippingProps;
    const couponRemovedErrorMsgLabel = getLabelValue(
      labels,
      'lbl_coupon_removed_error',
      'bagPage',
      'checkout'
    );
    switch (currentStage && currentStage.toLowerCase()) {
      case PICKUP:
        return (
          <PickupPage
            checkoutProgressBarLabels={checkoutProgressBarLabels}
            pickupDidMount={pickupDidMount}
            cartOrderItemsCount={cartOrderItemsCount}
            isGuest={isGuest}
            isMobile={isMobile}
            isUsSite={isUsSite}
            initialValues={pickupInitialValues}
            pickupInitialValues={pickupInitialValues}
            bagLoading={bagLoading}
            onEditModeChange={onEditModeChange}
            isSmsUpdatesEnabled={isSmsUpdatesEnabled}
            currentPhoneNumber={currentPhoneNumber}
            isOrderUpdateChecked={isOrderUpdateChecked}
            isAlternateUpdateChecked={isAlternateUpdateChecked}
            pickUpLabels={pickUpLabels}
            smsSignUpLabels={smsSignUpLabels}
            onPickupSubmit={onPickupSubmit}
            navigation={navigation}
            availableStages={availableStages}
            setCheckoutStage={setCheckoutStage}
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
            orderHasShipping={orderHasShipping}
            isVenmoPickupDisplayed={isVenmoPickupBannerDisplayed}
            isVenmoShippingDisplayed={isVenmoShippingBannerDisplayed}
            isBonusPointsEnabled={isBonusPointsEnabled}
            checkoutPageEmptyBagLabels={checkoutPageEmptyBagLabels}
            smsOrderUpdatesRichText={smsOrderUpdatesRichText}
            formattedProductsData={formattedProductsData}
            keyBoardAvoidRef={this.keyBoardAvoidRef}
            scrollToTop={this.scrollToTop}
          />
        );
      case SHIPPING:
        return (
          <ShippingPage
            {...shippingProps}
            checkoutProgressBarLabels={checkoutProgressBarLabels}
            cartOrderItemsCount={cartOrderItemsCount}
            bagLoading={bagLoading}
            checkoutPageEmptyBagLabels={checkoutPageEmptyBagLabels}
            loadShipmentMethods={loadShipmentMethods}
            navigation={navigation}
            isGuest={isGuest}
            isUsSite={isUsSite}
            submitVerifiedShippingAddressData={submitVerifiedShippingAddressData}
            verifyAddressAction={verifyAddressAction}
            formatPayload={formatPayload}
            orderHasPickUp={orderHasPickUp}
            handleSubmit={this.submitShippingSection}
            availableStages={availableStages}
            updateShippingMethodSelection={updateShippingMethodSelection}
            updateShippingAddressData={updateShippingAddressData}
            addNewShippingAddressData={addNewShippingAddressData}
            pickUpContactPerson={pickUpContactPerson}
            labels={labels}
            setCheckoutStage={setCheckoutStage}
            venmoBannerLabel={{ venmoBannerText }}
            setVenmoPickupState={setVenmoPickupState}
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
            isVenmoPickupDisplayed={isVenmoPickupBannerDisplayed}
            isBonusPointsEnabled={isBonusPointsEnabled}
            isVenmoShippingDisplayed={isVenmoShippingBannerDisplayed}
            initShippingPage={initShippingPage}
            smsOrderUpdatesRichText={smsOrderUpdatesRichText}
            keyBoardAvoidRef={this.keyBoardAvoidRef}
            scrollToTop={this.scrollToTop}
            zipCodeABTestEnabled={zipCodeABTestEnabled}
            getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
            zipCodeLoader={zipCodeLoader}
            isEDD={isEDD}
            updateEdd={updateEdd}
            cartOrderItems={cartOrderItems}
            eddAllowedStatesArr={eddAllowedStatesArr}
            checkoutServerError={checkoutServerError}
            clearCheckoutServerError={clearCheckoutServerError}
            addItemToSflList={addItemToSflList}
            currentOrderId={currentOrderId}
            resetShippingEddData={resetShippingEddData}
            mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
            mapboxSwitch={mapboxSwitch}
          />
        );
      case BILLING:
        return (
          <BillingPage
            {...billingProps}
            checkoutProgressBarLabels={checkoutProgressBarLabels}
            orderHasShipping={orderHasShipping}
            bagLoading={bagLoading}
            navigation={navigation}
            isGuest={isGuest}
            isUsSite={isUsSite}
            availableStages={availableStages}
            submitBilling={submitBilling}
            setCheckoutStage={setCheckoutStage}
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
            setVenmoPickupState={setVenmoPickupState}
            isBonusPointsEnabled={isBonusPointsEnabled}
            setVenmoShippingState={setVenmoShippingState}
            isPayPalWebViewEnable={isPayPalWebViewEnable}
            keyBoardAvoidRef={this.keyBoardAvoidRef}
            scrollToTop={this.scrollToTop}
            zipCodeABTestEnabled={zipCodeABTestEnabled}
            getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
            zipCodeLoader={zipCodeLoader}
            mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
            mapboxSwitch={mapboxSwitch}
            setAfterPayProgress={setAfterPayProgress}
            trackClickAfterPay={trackClickAfterPay}
          />
        );
      case REVIEW:
        return (
          <ReviewPage
            {...reviewProps}
            checkoutProgressBarLabels={checkoutProgressBarLabels}
            navigation={navigation}
            bagLoading={bagLoading}
            submitReview={submitReview}
            availableStages={availableStages}
            orderHasPickUp={orderHasPickUp}
            orderHasShipping={orderHasShipping}
            setCheckoutStage={setCheckoutStage}
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
            isExpressCheckout={isExpressCheckout}
            pickUpContactAlternate={pickUpContactAlternate}
            initialValues={{
              pickUpAlternateExpress: {
                hasAlternatePickup: isHasPickUpAlternatePerson,
                firstName_alt: pickUpAlternatePerson.firstName,
                lastName_alt: pickUpAlternatePerson.lastName,
                emailAddress_alt: pickUpAlternatePerson.emailAddress,
              },
              expressReviewShippingSection: {
                shippingMethodId: reviewProps.defaultShipmentId,
              },
              cardType: reviewProps.cardType,
            }}
            shipmentMethods={shipmentMethods}
            isBonusPointsEnabled={isBonusPointsEnabled}
            setVenmoShippingState={setVenmoShippingState}
            setVenmoPickupState={setVenmoPickupState}
            isGuest={isGuest}
            keyBoardAvoidRef={this.keyBoardAvoidRef}
            scrollToTop={this.scrollToTop}
            persistEddNodeAction={persistEddNodeAction}
            cartOrderItems={cartOrderItems}
            checkoutServerError={checkoutServerError}
            clearCheckoutServerError={clearCheckoutServerError}
            formatPayload={formatPayload}
            shippingAddress={shippingAddress}
            addItemToSflList={addItemToSflList}
            updateShippingMethodSelection={updateShippingMethodSelection}
            itemsWithEdd={itemsWithEdd}
            selectedShippingCode={selectedShippingCode}
            isEDD={isEDD}
            updateShippingAddressData={updateShippingAddressData}
            setAfterPayProgress={setAfterPayProgress}
            giftCardTotal={giftCardTotal}
            orderBalanceTotal={orderBalanceTotal}
            trackClickAfterPay={trackClickAfterPay}
            setAnalyticsTriggered={setAnalyticsTriggered}
            setCouponRemovedFromOrderFlag={setCouponRemovedFromOrderFlag}
            couponRemovedFromOrderFlag={couponRemovedFromOrderFlag}
            toastMessage={toastMessage}
            couponRemovedErrorMsgLabel={couponRemovedErrorMsgLabel}
          />
        );
      case CONFIRMATION:
        return (
          <Confirmation
            navigation={navigation}
            isBonusPointsEnabled={isBonusPointsEnabled}
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
          />
        );
      default:
        return null;
    }
  };

  render() {
    return (
      <WithKeyboardAvoidingViewHOC
        keyboardAwareScrollViewRef={(node) => {
          this.keyBoardAvoidRef = node;
        }}
      >
        {this.showMergeCartLimitError()}
        {this.getCurrentPage()}
      </WithKeyboardAvoidingViewHOC>
    );
  }
}

CheckoutPage.propTypes = propTypes;
CheckoutPage.defaultProps = defaultProps;

export default CheckoutPage;
