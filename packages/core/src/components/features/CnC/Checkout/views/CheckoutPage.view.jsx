/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {
  setLoaderState,
  setSectionLoaderState,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import AfterPayContainer from '@tcp/core/src/components/common/organisms/AfterPayPayment';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { CALL_TO_ACTION_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import { isClient } from '@tcp/core/src/utils';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import CnCTemplate from '../../common/organism/CnCTemplate';
import PickUpFormPart from '../organisms/PickupPage';
import ShippingPage from '../organisms/ShippingPage';
import BillingPage from '../organisms/BillingPage';
import ReviewPage from '../organisms/ReviewPage';
import CHECKOUT_STAGES from '../../../../../../../web/src/components/App.constants';
import VenmoBanner from '../../../../common/molecules/VenmoBanner';
import Confirmation from '../../Confirmation';
import {
  routerPush,
  scrollToParticularElement,
  getLabelValue,
  capitalize,
} from '../../../../../utils';
import ErrorMessage from '../../common/molecules/ErrorMessage';
import { Button, BodyCopy } from '../../../../common/atoms';
import CheckoutPageEmptyBag from '../molecules/CheckoutPageEmptyBag';
import checkoutUtil, {
  hasGiftServiceError,
  hasRushShippingError,
  hasApoFpoError,
} from '../util/utility';
import {
  getCurrentSection,
  updateAnalyticsData,
  getFormLoad,
  propsTypes,
} from './CheckoutPage.view.util';
import Espot from '../../../../common/molecules/Espot';

class CheckoutPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.pageServerError = null;
    this.pageServerErrorRef = this.pageServerErrorRef.bind(this);
    this.reviewFormRef = React.createRef();
    if (isClient()) {
      const { setCheckoutStage, isApplePaymentInProgress } = props;
      const currentSection = this.getCurrentCheckoutSection(props);
      setCheckoutStage(currentSection);
      if (
        currentSection.toLowerCase() === CHECKOUT_STAGES.CONFIRMATION &&
        !isApplePaymentInProgress
      ) {
        routerPush('/', '/home');
      }
    }
  }

  componentDidMount() {
    const { router } = this.props;
    setSectionLoaderState({ addedToBagLoaderState: false, section: 'addedtobag' });
    setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' });
    setLoaderState(false);
    router.events.on('routeChangeComplete', this.handleRouteComplete);
  }

  componentDidUpdate(prevProps) {
    const { checkoutServerError, setCheckoutStage, activeStage } = this.props;
    const currentSection = this.getCurrentCheckoutSection(this.props);
    if (currentSection !== activeStage) {
      setCheckoutStage(currentSection);
    }
    if (
      checkoutServerError &&
      this.pageServerError !== null &&
      checkoutServerError !== prevProps.checkoutServerError
    ) {
      scrollToParticularElement(this.pageServerError);
    }
    updateAnalyticsData(this.props, prevProps);
  }

  componentWillUnmount() {
    const { router, setAppleProgress, setAfterPayProgress } = this.props;
    router.events.off('routeChangeComplete', this.handleRouteComplete);
    setAppleProgress(false);
    setAfterPayProgress(false);
  }

  handleRouteComplete = () => {
    if (window) {
      window.scrollTo(0, 0);
    }
  };

  getCurrentCheckoutSection = (props) => {
    const { router } = props;
    const section = router.query.section || router.query.subSection;
    return section || CHECKOUT_STAGES.SHIPPING;
  };

  getIsRenderConfirmation = () => {
    const { router } = this.props;
    return router.query.fromReview || router.query.fromApplePay || false;
  };

  editShippingAddress = () => {
    const { router } = this.props;
    return router.query.editShippingAddress || false;
  };

  triggerAnalytics = () => {
    const { trackClickAfterPay } = this.props;
    const currentSection = this.getCurrentCheckoutSection(this.props);
    if (typeof trackClickAfterPay === 'function') {
      trackClickAfterPay({
        afterpay_metrics141: 1,
        afterpay_cd131: capitalize(currentSection.toLowerCase()),
        customEvents: ['event141'],
      });
    }
  };

  /**
   * This method will set venmo banner state once it is visible, so that it won't be visible
   * once user comes back
   */
  isVenmoPickupDisplayed = () => {
    const { isVenmoPickupBannerDisplayed } = this.props;
    const currentSection = getCurrentSection(this.props);
    return currentSection && currentSection.toLowerCase() === CHECKOUT_STAGES.PICKUP
      ? isVenmoPickupBannerDisplayed
      : false;
  };

  /**
   * This method will set venmo banner state once it is visible, so that it won't be visible
   * once user comes back
   */
  isVenmoShippingDisplayed = () => {
    const { isVenmoShippingBannerDisplayed } = this.props;
    const currentSection = getCurrentSection(this.props);
    return currentSection.toLowerCase() === CHECKOUT_STAGES.SHIPPING
      ? isVenmoShippingBannerDisplayed
      : false;
  };

  /**
   * This function is to validate if we need to show venmo banner or not.
   * Only if user comes on pickup or shipping page, but not on coming back from navigation
   * @params {string} currentSection - current checkout section name
   */
  isShowVenmoBanner = (currentSection) => {
    const { isUsSite, isVenmoPaymentInProgress } = this.props;
    return (
      isUsSite &&
      isVenmoPaymentInProgress &&
      ((currentSection.toLowerCase() === CHECKOUT_STAGES.PICKUP &&
        !this.isVenmoPickupDisplayed()) ||
        (currentSection.toLowerCase() === CHECKOUT_STAGES.SHIPPING &&
          !this.isVenmoShippingDisplayed()))
    );
  };

  renderPageErrors = () => {
    const { checkoutServerError } = this.props;
    return (
      <div className="checkout-page-error-container elem-mt-MED" ref={this.pageServerErrorRef}>
        {checkoutServerError && (
          <ErrorMessage error={checkoutServerError.errorMessage} className="checkout-page-error" />
        )}
      </div>
    );
  };

  expressCheckoutCallDone = () => {
    const { shippingProps: { initCheckoutFinished = true } = {}, isExpressCheckout = false } =
      this.props;
    return isExpressCheckout ? initCheckoutFinished : true;
  };

  showMergeCartLimitError = (currentSection) => {
    const { labels, showMaxItemError } = this.props;
    const mergeCartLimitErrorMsg = getLabelValue(
      labels,
      'lbl_coupon_max_quantity_error',
      'bagPage',
      'checkout'
    );
    if (showMaxItemError && currentSection.toLowerCase() !== CHECKOUT_STAGES.CONFIRMATION) {
      return (
        <Notification
          className="max-qty-reached elem-mt-MED"
          status="error"
          scrollIntoView
          isErrorTriangleIcon
          message={mergeCartLimitErrorMsg}
        />
      );
    }
    return null;
  };

  renderLeftSection = () => {
    const {
      isGuest,
      isMobile,
      isUsSite,
      onEditModeChange,
      isSmsUpdatesEnabled,
      currentPhoneNumber,
      shippingProps,
      navigation,
      orderHasPickUp,
      submitShippingSection,
      isOrderUpdateChecked,
      isGiftServicesChecked,
      isAlternateUpdateChecked,
      pickUpLabels,
      smsSignUpLabels,
      pickupInitialValues,
      loadShipmentMethods,
      onPickupSubmit,
      updateFromMSG,
      orderHasShipping,
      routeToPickupPage,
      updateShippingMethodSelection,
      updateShippingAddressData,
      addNewShippingAddressData,
      billingProps,
      labels,
      submitBilling,
      reviewProps,
      submitReview,
      isVenmoPaymentInProgress,
      isApplePaymentInProgress,
      setVenmoPickupState,
      setVenmoShippingState,
      verifyAddressAction,
      formatPayload,
      submitVerifiedShippingAddressData,
      isExpressCheckout,
      initShippingPage,
      shippingMethod,
      pickupDidMount,
      cartLoading,
      emailSignUpFlags,
      bagLoading,
      titleLabel,
      smsOrderUpdatesRichText,
      emailSignUpCaRichText,
      checkoutProgressBarLabels,
      formattedProductsData,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      persistEddNodeAction,
      itemsWithEdd,
      selectedShippingCode,
      isEDD,
      updateEdd,
      eddAllowedStatesArr,
      currentOrderId,
      resetShippingEddData,
      isEddABTest,
      pageName,
      appliedExpiredCoupon,
      addItemToSflList,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      setAfterPayProgress,
      orderBalanceTotal,
      trackClickAfterPay,
      setCouponRemovedFromOrderFlag,
      couponRemovedFromOrderFlag,
    } = this.props;
    const { isHasPickUpAlternatePerson, pickUpAlternatePerson, pickUpContactPerson } = this.props;
    const { pickUpContactAlternate, checkoutServerError, toggleCountrySelector } = this.props;
    const { clearCheckoutServerError, setClickAnalyticsDataCheckout, cartOrderItems } = this.props;
    const { cartOrderItemsCount, checkoutPageEmptyBagLabels } = this.props;
    const { isBagLoaded, isRegisteredUserCallDone, checkoutRoutingDone } = this.props;
    const currentSection = this.getCurrentCheckoutSection(this.props);
    const isFormLoad = getFormLoad(pickupInitialValues, isGuest);
    const { shipmentMethods } = shippingProps;
    const isRendorConfirmation = this.getIsRenderConfirmation();
    const editShippingAddress = this.editShippingAddress();
    const couponRemovedErrorMsg = getLabelValue(
      labels,
      'lbl_coupon_removed_error',
      'bagPage',
      'checkout'
    );

    return (
      <div>
        {this.isShowVenmoBanner(currentSection) && <VenmoBanner labels={pickUpLabels} />}
        {this.showMergeCartLimitError(currentSection)}
        {currentSection.toLowerCase() === CHECKOUT_STAGES.PICKUP && isFormLoad && (
          <PickUpFormPart
            emailSignUpFlags={emailSignUpFlags}
            checkoutRoutingDone={checkoutRoutingDone}
            pickupDidMount={pickupDidMount}
            isRegisteredUserCallDone={isRegisteredUserCallDone}
            isBagLoaded={isBagLoaded}
            isGuest={isGuest}
            isMobile={isMobile}
            isUsSite={isUsSite}
            initialValues={pickupInitialValues}
            pickupInitialValues={pickupInitialValues}
            onEditModeChange={onEditModeChange}
            isSmsUpdatesEnabled={isSmsUpdatesEnabled}
            currentPhoneNumber={currentPhoneNumber}
            isOrderUpdateChecked={isOrderUpdateChecked}
            isGiftServicesChecked={isGiftServicesChecked}
            isAlternateUpdateChecked={isAlternateUpdateChecked}
            pickUpLabels={pickUpLabels}
            smsSignUpLabels={smsSignUpLabels}
            orderHasShipping={orderHasShipping}
            onPickupSubmit={onPickupSubmit}
            checkoutProgressBarLabels={checkoutProgressBarLabels}
            updateFromMSG={updateFromMSG}
            navigation={navigation}
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
            /* To handle use cases for venmo banner and next CTA on pickup page. If true then normal checkout flow otherwise venmo scenarios  */
            isVenmoPickupDisplayed={this.isVenmoPickupDisplayed()}
            isApplePaymentInProgress={isApplePaymentInProgress}
            ServerErrors={this.renderPageErrors}
            checkoutServerError={checkoutServerError}
            pageCategory={currentSection.toLowerCase()}
            cartOrderItemsCount={cartOrderItemsCount}
            checkoutPageEmptyBagLabels={checkoutPageEmptyBagLabels}
            setClickAnalyticsDataCheckout={setClickAnalyticsDataCheckout}
            cartOrderItems={cartOrderItems}
            smsOrderUpdatesRichText={smsOrderUpdatesRichText}
            emailSignUpCaRichText={emailSignUpCaRichText}
            formattedProductsData={formattedProductsData}
            pageName={pageName}
            appliedExpiredCoupon={appliedExpiredCoupon}
          />
        )}
        {currentSection.toLowerCase() === CHECKOUT_STAGES.SHIPPING &&
          this.expressCheckoutCallDone() && (
            <ShippingPage
              emailSignUpFlags={emailSignUpFlags}
              checkoutRoutingDone={checkoutRoutingDone}
              isBagLoaded={isBagLoaded}
              cartOrderItemsCount={cartOrderItemsCount}
              checkoutPageEmptyBagLabels={checkoutPageEmptyBagLabels}
              checkoutProgressBarLabels={checkoutProgressBarLabels}
              {...shippingProps}
              toggleCountrySelector={toggleCountrySelector}
              initShippingPage={initShippingPage}
              isGuest={isGuest}
              isUsSite={isUsSite}
              formatPayload={formatPayload}
              orderHasPickUp={orderHasPickUp}
              handleSubmit={submitShippingSection}
              loadShipmentMethods={loadShipmentMethods}
              routeToPickupPage={routeToPickupPage}
              isMobile={isMobile}
              updateShippingMethodSelection={updateShippingMethodSelection}
              updateShippingAddressData={updateShippingAddressData}
              addNewShippingAddressData={addNewShippingAddressData}
              labels={labels}
              verifyAddressAction={verifyAddressAction}
              isVenmoPaymentInProgress={isVenmoPaymentInProgress}
              setVenmoPickupState={setVenmoPickupState}
              submitVerifiedShippingAddressData={submitVerifiedShippingAddressData}
              /* To handle use cases for venmo banner and next CTA on shipping page. If true, then normal checkout flow otherwise venmo scenarios  */
              isVenmoShippingDisplayed={this.isVenmoShippingDisplayed()}
              isApplePaymentInProgress={isApplePaymentInProgress}
              ServerErrors={this.renderPageErrors}
              checkoutServerError={checkoutServerError}
              clearCheckoutServerError={clearCheckoutServerError}
              pageCategory={currentSection.toLowerCase()}
              pickUpContactPerson={pickUpContactPerson}
              smsOrderUpdatesRichText={smsOrderUpdatesRichText}
              emailSignUpCaRichText={emailSignUpCaRichText}
              zipCodeABTestEnabled={zipCodeABTestEnabled}
              getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
              zipCodeLoader={zipCodeLoader}
              isEDD={isEDD}
              updateEdd={updateEdd}
              cartOrderItems={cartOrderItems}
              eddAllowedStatesArr={eddAllowedStatesArr}
              currentOrderId={currentOrderId}
              resetShippingEddData={resetShippingEddData}
              pageName={pageName}
              appliedExpiredCoupon={appliedExpiredCoupon}
              addItemToSflList={addItemToSflList}
              editShippingAddress={editShippingAddress}
              mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
              mapboxSwitch={mapboxSwitch}
            />
          )}
        {currentSection.toLowerCase() === CHECKOUT_STAGES.BILLING && (
          <BillingPage
            {...billingProps}
            checkoutRoutingDone={checkoutRoutingDone}
            orderHasShipping={orderHasShipping}
            isGuest={isGuest}
            checkoutProgressBarLabels={checkoutProgressBarLabels}
            submitBilling={submitBilling}
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
            ServerErrors={this.renderPageErrors}
            isApplePaymentInProgress={isApplePaymentInProgress}
            checkoutServerError={checkoutServerError}
            clearCheckoutServerError={clearCheckoutServerError}
            pageCategory={currentSection.toLowerCase()}
            zipCodeABTestEnabled={zipCodeABTestEnabled}
            getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
            zipCodeLoader={zipCodeLoader}
            pageName={pageName}
            appliedExpiredCoupon={appliedExpiredCoupon}
            mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
            mapboxSwitch={mapboxSwitch}
            setAfterPayProgress={setAfterPayProgress}
            trackClickAfterPay={trackClickAfterPay}
          />
        )}
        {currentSection.toLowerCase() === CHECKOUT_STAGES.REVIEW && (
          <ReviewPage
            {...reviewProps}
            submitReview={submitReview}
            checkoutRoutingDone={checkoutRoutingDone}
            navigation={navigation}
            orderHasPickUp={orderHasPickUp}
            orderHasShipping={orderHasShipping}
            setVenmoShippingState={setVenmoShippingState}
            checkoutProgressBarLabels={checkoutProgressBarLabels}
            setVenmoPickupState={setVenmoPickupState}
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
            isApplePaymentInProgress={isApplePaymentInProgress}
            isGuest={isGuest}
            isExpressCheckout={isExpressCheckout}
            onSubmit={this.reviewFormSubmit}
            shipmentMethods={shipmentMethods}
            reviewFormSubmit={this.reviewFormSubmit}
            pickUpContactPerson={pickUpContactPerson}
            pickUpContactAlternate={pickUpContactAlternate}
            ServerErrors={this.renderPageErrors}
            checkoutServerError={checkoutServerError}
            initialValues={{
              expressReviewShippingSection: {
                shippingMethodId: shippingMethod,
              },
              pickUpAlternateExpress: {
                hasAlternatePickup: isHasPickUpAlternatePerson,
                firstName_alt: pickUpAlternatePerson.firstName,
                lastName_alt: pickUpAlternatePerson.lastName,
                emailAddress_alt: pickUpAlternatePerson.emailAddress,
              },
              cardType: reviewProps.cardType,
            }}
            clearCheckoutServerError={clearCheckoutServerError}
            pageCategory={currentSection.toLowerCase()}
            cartLoading={cartLoading}
            bagLoading={bagLoading}
            titleLabel={titleLabel}
            persistEddNodeAction={persistEddNodeAction}
            itemsWithEdd={itemsWithEdd}
            selectedShippingCode={selectedShippingCode}
            isEddABTest={isEddABTest}
            isEDD={isEDD}
            pageName={pageName}
            appliedExpiredCoupon={appliedExpiredCoupon}
            cartOrderItems={cartOrderItems}
            addItemToSflList={addItemToSflList}
            shippingProps={shippingProps}
            updateShippingMethodSelection={updateShippingMethodSelection}
            updateShippingAddressData={updateShippingAddressData}
            setAfterPayProgress={setAfterPayProgress}
            orderBalanceTotal={orderBalanceTotal}
            trackClickAfterPay={trackClickAfterPay}
            setCouponRemovedFromOrderFlag={setCouponRemovedFromOrderFlag}
            couponRemovedFromOrderFlag={couponRemovedFromOrderFlag}
            couponRemovedErrorMsgLabel={couponRemovedErrorMsg}
          />
        )}
        {currentSection.toLowerCase() === CHECKOUT_STAGES.CONFIRMATION && isRendorConfirmation && (
          <Confirmation
            isVenmoPaymentInProgress={isVenmoPaymentInProgress}
            pageCategory={currentSection.toLowerCase()}
          />
        )}
      </div>
    );
  };

  handleDefaultLinkClick = (e) => {
    e.preventDefault();
  };

  disableAfterPay = () => {
    const { reviewProps, orderBalanceTotal } = this.props;
    const {
      afterPayInProgress,
      afterPayMaxOrderAmount = 1000,
      afterPayMinOrderAmount = 0,
    } = reviewProps;
    const totalAmount = orderBalanceTotal && orderBalanceTotal * 1;

    if (
      afterPayInProgress &&
      (afterPayMinOrderAmount > totalAmount || afterPayMaxOrderAmount < totalAmount)
    ) {
      return true;
    }

    return afterPayInProgress && totalAmount && totalAmount < 1;
  };

  shouldDisableCheckoutCta = () => {
    const { checkoutServerError } = this.props;
    if (
      checkoutServerError &&
      (hasGiftServiceError(checkoutServerError) ||
        hasRushShippingError(checkoutServerError) ||
        hasApoFpoError(checkoutServerError))
    ) {
      return true;
    }

    if (this.disableAfterPay()) {
      return true;
    }
    return false;
  };

  reviewFormSubmit = (data) => checkoutUtil.handleReviewFormSubmit(this, data);

  ctaText = () => {
    const { reviewProps } = this.props;
    const { afterPayInProgress } = reviewProps;
    const { nextSubmitText, labelAfterPaySubmit } = reviewProps.labels;
    return afterPayInProgress ? labelAfterPaySubmit : nextSubmitText;
  };

  pageServerErrorRef(ref) {
    this.pageServerError = ref;
  }

  renderAfterPayMessaging = () => {
    const { reviewProps, orderBalanceTotal } = this.props;
    const { afterPayInProgress, labels, afterPayMinOrderAmount, afterPayMaxOrderAmount } =
      reviewProps;
    const { labelAfterPayThresholdError } = labels;
    let thresholdErrorMessage = labelAfterPayThresholdError.replace(
      '[AfterPayMinOrderAmount]',
      afterPayMinOrderAmount
    );
    thresholdErrorMessage = thresholdErrorMessage.replace(
      '[AfterPayMaxOrderAmount]',
      afterPayMaxOrderAmount
    );
    if (afterPayInProgress && this.disableAfterPay()) {
      return (
        <Notification
          className="max-qty-reached elem-mt-MED"
          status="warning"
          scrollIntoView
          message={thresholdErrorMessage}
        />
      );
    }

    return afterPayInProgress && orderBalanceTotal ? (
      <BodyCopy
        component="p"
        fontSize="fs10"
        fontFamily="secondary"
        textAlign="center"
        className="elem-mt-XS"
      >
        <AfterPayMessaging offerPrice={orderBalanceTotal} centered dataIntroText="false" />
      </BodyCopy>
    ) : null;
  };

  renderAfterPayCTA = () => {
    const { reviewProps } = this.props;
    const { afterPayInProgress } = reviewProps;
    return afterPayInProgress && !this.disableAfterPay() && !this.shouldDisableCheckoutCta() ? (
      <AfterPayContainer className="after-pay-container" />
    ) : null;
  };

  render() {
    const {
      isGuest,
      dispatchReviewReduxForm,
      reviewProps,
      checkoutServerError,
      isBagLoaded,
      excludedIds,
      partNumber,
      labels,
    } = this.props;
    const { cartOrderItemsCount, checkoutPageEmptyBagLabels, bagLoading } = this.props;
    const { ariaLabelSubmitOrderButton } = reviewProps.labels;
    const { submitOrderRichText, afterPayInProgress } = reviewProps;
    const currentSection = this.getCurrentCheckoutSection(this.props);
    const isRendorConfirmation = this.getIsRenderConfirmation();

    return (
      <>
        {((!isBagLoaded || cartOrderItemsCount > 0) &&
          currentSection.toLowerCase() !== CHECKOUT_STAGES.CONFIRMATION) ||
        isRendorConfirmation ? (
          <CnCTemplate
            showLeftSection
            leftSection={this.renderLeftSection}
            marginTop={currentSection.toLowerCase() !== CHECKOUT_STAGES.CONFIRMATION}
            isGuest={isGuest}
            isCheckoutView
            orderLedgerAfterView={
              currentSection.toLowerCase() === CHECKOUT_STAGES.REVIEW && (
                <div className="review-submit-container">
                  <>
                    {submitOrderRichText && <Espot richTextHtml={submitOrderRichText} />}
                    <Button
                      aria-label={ariaLabelSubmitOrderButton}
                      type="button"
                      className="review-submit-button"
                      fontSize="fs13"
                      fontWeight="extrabold"
                      buttonVariation="variable-width"
                      fill="BLUE"
                      disabled={bagLoading || this.shouldDisableCheckoutCta()}
                      onClick={
                        !afterPayInProgress ? dispatchReviewReduxForm : this.triggerAnalytics
                      }
                    >
                      {this.ctaText()}
                      {this.renderAfterPayCTA()}
                    </Button>
                    {/* UX timer */}
                    {this.renderAfterPayMessaging()}
                    <RenderPerf.Measure name={CALL_TO_ACTION_VISIBLE} />
                  </>
                </div>
              )
            }
            isConfirmationPage={currentSection.toLowerCase() === CHECKOUT_STAGES.CONFIRMATION}
            pageCategory={currentSection.toLowerCase()}
            checkoutServerError={checkoutServerError}
            excludedIds={excludedIds}
            partNumber={partNumber}
            labels={labels}
          />
        ) : (
          <CheckoutPageEmptyBag labels={checkoutPageEmptyBagLabels} />
        )}
      </>
    );
  }
}

CheckoutPage.propTypes = propsTypes;

CheckoutPage.defaultProps = {
  isVenmoPaymentInProgress: false,
  isApplePaymentInProgress: false,
  setVenmoPickupState: () => {},
  setVenmoShippingState: () => {},
  isExpressCheckout: false,
  shippingMethod: {},
  isVenmoPickupBannerDisplayed: true,
  isVenmoShippingBannerDisplayed: true,
  pageData: {},
  excludedIds: '',
  addItemToSflList: () => {},
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  showMaxItemError: false,
};

export default CheckoutPage;
export { CheckoutPage as CheckoutPageVanilla };
