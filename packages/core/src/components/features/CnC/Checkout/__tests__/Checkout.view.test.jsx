// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CheckoutPageVanilla } from '../views/CheckoutPage.view';
import CHECKOUT_STAGES from '../../../../../../../web/src/components/App.constants';
import { getCurrentSection } from '../views/CheckoutPage.view.util';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileWeb: jest.fn(),
  getViewportInfo: () => ({
    isMobile: false,
  }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isMobileApp: jest.fn(),
  isClient: jest.fn().mockImplementation(() => true),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  readCookie: jest.fn(),
  setCookie: jest.fn(),
  getLabelValue: jest.fn(),
  getSiteId: jest.fn().mockImplementation(() => '102230'),
}));

describe('CheckoutPageVanilla component', () => {
  const initialProps = {
    className: 'className',
    backLinkText: 'backLinkText',
    nextButtonText: 'nextButtonText',
    disableNext: false,
    backLinkHandler: () => {},
    disableBackLink: false,
    router: {
      query: { subSection: CHECKOUT_STAGES.SHIPPING, section: CHECKOUT_STAGES.SHIPPING },
      events: { on: jest.fn() },
    },
    onPickupSubmit: () => {},
    isVenmoPaymentInProgress: true,
    isUsSite: true,
    reviewProps: { labels: {} },
    shippingProps: {
      shipmentMethods: {},
    },
    setCheckoutStage: jest.fn(),
  };

  const tree = shallow(<CheckoutPageVanilla {...initialProps} />);

  it('should renders correctly pickup', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { section: CHECKOUT_STAGES.PICKUP }, events: { on: jest.fn() } },
      onPickupSubmit: () => {},
      reviewProps: { labels: {} },
      shippingProps: {
        shipmentMethods: {},
      },
      setCheckoutStage: jest.fn(),
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    component.instance().renderLeftSection();
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with shipping', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { subSection: 'shipping' }, events: { on: jest.fn() } },
      onPickupSubmit: () => {},
      reviewProps: { labels: {} },
      shippingProps: {
        shipmentMethods: {},
      },
      setCheckoutStage: jest.fn(),
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    component.instance().renderLeftSection();
    expect(component).toMatchSnapshot();
  });

  it('calling getCurrentSection method', () => {
    const props = {
      router: {
        query: {
          section: 'shipping',
        },
      },
      events: { on: jest.fn() },
    };
    expect(getCurrentSection(props)).toEqual('shipping');
  });

  it('calling isVenmoPickupDisplayed method', () => {
    const componentInstance = tree.instance();
    expect(componentInstance.isVenmoPickupDisplayed()).toBeFalsy();
  });

  it('calling isShowVenmoBanner method', () => {
    const props = {
      ...initialProps,
      isVenmoPickupBannerDisplayed: false,
      router: {
        query: { subSection: CHECKOUT_STAGES.PICKUP, section: CHECKOUT_STAGES.PICKUP },
        events: { on: jest.fn() },
      },
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.isShowVenmoBanner(CHECKOUT_STAGES.PICKUP)).toBeTruthy();
  });

  it('calling isShowVenmoBanner method for Shipping', () => {
    const props = {
      ...initialProps,
      isVenmoShippingBannerDisplayed: false,
      router: {
        query: { subSection: CHECKOUT_STAGES.SHIPPING, section: CHECKOUT_STAGES.SHIPPING },
        events: { on: jest.fn() },
      },
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.isShowVenmoBanner(CHECKOUT_STAGES.SHIPPING)).toBeTruthy();
  });

  it('calling isVenmoPickupDisplayed method for pickup page', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { section: 'pickup', subSection: 'pickup' }, events: { on: jest.fn() } },
      onPickupSubmit: () => {},
      reviewProps: { labels: {} },
      isVenmoPickupBannerDisplayed: false,
      isVenmoPaymentInProgress: true,
      setCheckoutStage: jest.fn(),
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.isVenmoPickupDisplayed()).toBeFalsy();
  });

  it('calling isVenmoShippingDisplayed method', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { section: 'pickup', subSection: 'pickup' }, events: { on: jest.fn() } },
      onPickupSubmit: () => {},
      isVenmoShippingBannerDisplayed: false,
      reviewProps: {
        labels: {
          ariaLabelSubmitOrderButton: '',
          applyConditionPreText: '',
          applyConditionTermsText: '',
          nextSubmitText: '',
          applyConditionPolicyText: '',
          applyConditionAndText: '',
        },
      },
      setCheckoutStage: jest.fn(),
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.isVenmoShippingDisplayed()).toBeFalsy();
  });

  it('should be truthy when editShippingAddress url param is there', () => {
    const props = {
      ...initialProps,
      router: {
        query: {
          subSection: CHECKOUT_STAGES.SHIPPING,
          section: CHECKOUT_STAGES.SHIPPING,
          editShippingAddress: true,
        },
        events: { on: jest.fn() },
      },
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.editShippingAddress()).toBeTruthy();
  });

  it('should be falsy when editShippingAddress url param is not there', () => {
    const props = {
      ...initialProps,
      router: {
        query: { subSection: CHECKOUT_STAGES.SHIPPING, section: CHECKOUT_STAGES.SHIPPING },
        events: { on: jest.fn() },
      },
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.editShippingAddress()).toBeFalsy();
  });

  it('should return expressCheckoutCallDone false when initCheckoutFinished is false for expresscheckout', () => {
    const props = {
      ...initialProps,
      shippingProps: {
        shipmentMethods: {},
        initCheckoutFinished: false,
      },
      isExpressCheckout: true,
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.expressCheckoutCallDone()).toBeFalsy();
  });

  it('should return expressCheckoutCallDone true when initCheckoutFinished is true for expresscheckout', () => {
    const props = {
      ...initialProps,
      shippingProps: {
        shipmentMethods: {},
        initCheckoutFinished: true,
      },
      isExpressCheckout: true,
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.expressCheckoutCallDone()).toBeTruthy();
  });

  it('should return expressCheckoutCallDone true when isExpressCheckout is false', () => {
    const props = {
      ...initialProps,
      shippingProps: {
        shipmentMethods: {},
        initCheckoutFinished: true,
      },
      isExpressCheckout: false,
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    const componentInstance = component.instance();
    expect(componentInstance.expressCheckoutCallDone()).toBeTruthy();
  });

  it('should not render merge cart limit error', () => {
    const props = {
      ...initialProps,
      shippingProps: {
        shipmentMethods: {},
        initCheckoutFinished: true,
      },
      isExpressCheckout: false,
      router: {
        query: { subSection: CHECKOUT_STAGES.SHIPPING, section: CHECKOUT_STAGES.SHIPPING },
        events: { on: jest.fn() },
      },
      updateCheckoutPageData: jest.fn(),
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    expect(component.instance().showMergeCartLimitError()).toEqual(null);
  });

  it('should render merge cart limit error', () => {
    const props = {
      ...initialProps,
      shippingProps: {
        shipmentMethods: {},
        initCheckoutFinished: true,
      },
      isExpressCheckout: false,
      router: {
        query: { subSection: CHECKOUT_STAGES.SHIPPING, section: CHECKOUT_STAGES.SHIPPING },
        events: { on: jest.fn() },
      },
      updateCheckoutPageData: jest.fn(),
      showMaxItemError: true,
    };
    const component = shallow(<CheckoutPageVanilla {...props} />);
    expect(component.instance().showMergeCartLimitError(CHECKOUT_STAGES.SHIPPING)).toBeTruthy();
  });
});
