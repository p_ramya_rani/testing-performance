// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CheckoutContainer } from '../container/Checkout.container.native';
import CheckoutPage from '../views/CheckoutPage.view';

describe('CheckoutPage Container', () => {
  const router = {
    query: {
      section: '',
    },
  };
  const props = {
    router,
    initialValues: {},
    onEditModeChange: true,
    isSmsUpdatesEnabled: true,
    currentPhoneNumber: '',
    isGuest: true,
    isMobile: false,
    isExpressCheckout: false,
    activeStage: 'shipping',
    activeStep: 'shipping',
    isUsSite: false,
    initCheckout: jest.fn(),
    fetchNeedHelpContent: jest.fn(),
    isRegisteredUserCallDone: true,
    getUserInformation: jest.fn(),
    markBagPageRoutingDone: jest.fn(),
    isPayPalWebViewEnable: false,
    currentStage: 'shipping',
    resetAnalyticsData: jest.fn(),
    shippingMethodSelected: {},
    initCheckoutFinished: true,
    setMaxItemErrorAction: jest.fn(),
    setCouponMergeErrorAction: jest.fn(),
  };

  it('should render CheckoutPage view section', () => {
    const tree = shallow(<CheckoutContainer {...props} />);
    expect(tree.find(CheckoutPage)).toHaveLength(1);
  });

  it('should render CheckoutPage view section', () => {
    const component = shallow(<CheckoutContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
