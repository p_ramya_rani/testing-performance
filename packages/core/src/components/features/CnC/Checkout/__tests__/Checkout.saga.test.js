// 9fbef606107a605d69c0edbcd8029e5d 
import { put, call } from 'redux-saga/effects';
import { verifyAddressData } from '@tcp/core/src/services/abstractors/account/AddressVerification';
import { isCanada } from '@tcp/core/src/utils';
import {
  getEddData,
  resetEddData,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.actions';
import { verifyAddress } from '../../../../common/organisms/AddressVerification/container/AddressVerification.actions';
import CHECKOUT_ACTIONS, {
  toggleZipCodeLoader,
  submitVerifiedAddressData,
} from '../container/Checkout.action';

import {
  fetchAddressFromMelissa,
  callEddService,
  validateAdress,
} from '../container/Checkout.saga';
import { handleServerSideErrorAPI, updateShippingAddress } from '../container/Checkout.saga.util';
import {
  submitEmailSignup,
  validateAndSubmitEmailSignup,
} from '../container/CheckoutExtended.saga.util';

jest.mock('@tcp/core/src/utils', () => ({
  ...jest.requireActual('@tcp/core/src/utils'),
  isCanada: jest.fn(),
}));

describe('checkout saga utils', () => {
  it('updateShippingAddress', () => {
    const shippingAddress = {};
    const submitData = () => {};
    const navigation = '';
    const payload = {
      shipTo: {
        emailAddress: 'test@test.com',
        address: {},
      },
      shippingAddress,
      submitData,
      navigation,
    };
    const CheckoutSaga = updateShippingAddress({ payload });
    CheckoutSaga.next();

    CheckoutSaga.next();
    CheckoutSaga.next();
    CheckoutSaga.next();
    CheckoutSaga.next();
    CheckoutSaga.next();
    CheckoutSaga.next({});
    CheckoutSaga.next();
    CheckoutSaga.next();
    CheckoutSaga.next();
    CheckoutSaga.next();

    const putDescriptor = CheckoutSaga.next({}).value;
    expect(putDescriptor).toEqual(
      put(submitVerifiedAddressData({ shippingAddress, submitData, navigation }))
    );
  });
});

describe('CheckoutBilling saga', () => {
  it('handleServerSideErrorAPI', () => {
    const CheckoutSaga = handleServerSideErrorAPI({ response: { body: { errors: [] } } });
    CheckoutSaga.next();
    const putDescriptor = CheckoutSaga.next({}).value;
    expect(putDescriptor).toEqual(
      put(CHECKOUT_ACTIONS.setServerErrorCheckout({ errorMessage: undefined, component: 'PAGE' }))
    );
  });
});

describe('EmailSignup', () => {
  it('submitEmailSignup', () => {
    const emailAddress = 'test@123.com';
    const submitEmailSignupSaga = submitEmailSignup(emailAddress, {
      emailSignUpTCP: true,
      emailSignUpGYM: false,
    });
    submitEmailSignupSaga.next();
    submitEmailSignupSaga.next(true);
    expect(submitEmailSignupSaga.next(true).value).toEqual(
      validateAndSubmitEmailSignup(emailAddress, undefined, true, false)
    );
  });
});

describe('Checkout Saga:', () => {
  it('fetchAddressFromMelissa for Shipping', () => {
    const zipCodeValue = 12345;
    const fetchAddressFromMelissaSaga = fetchAddressFromMelissa({
      payload: {
        zipCodeValue,
        formName: 'checkoutShipping',
        formSection: 'shippingAddress',
        shouldLoadShipmentMethods: true,
      },
    });
    fetchAddressFromMelissaSaga.next();
    expect(fetchAddressFromMelissaSaga.next().value).toEqual(
      call(verifyAddressData, { zip: zipCodeValue })
    );
    fetchAddressFromMelissaSaga.next({ suggestedAddress: { city: 'Secaucus', state: 'NJ' } });
    fetchAddressFromMelissaSaga.next();
    fetchAddressFromMelissaSaga.next();
    fetchAddressFromMelissaSaga.next();
    expect(fetchAddressFromMelissaSaga.next().value).toEqual(put(toggleZipCodeLoader(false)));
  });

  it('fetchAddressFromMelissa for Shipping with State not present in response', () => {
    const zipCodeValue = 12345;
    const fetchAddressFromMelissaSaga = fetchAddressFromMelissa({
      payload: {
        zipCodeValue,
        formName: 'checkoutShipping',
        formSection: 'shippingAddress',
        shouldLoadShipmentMethods: true,
      },
    });
    fetchAddressFromMelissaSaga.next();
    expect(fetchAddressFromMelissaSaga.next().value).toEqual(
      call(verifyAddressData, { zip: zipCodeValue })
    );
    fetchAddressFromMelissaSaga.next({ suggestedAddress: { city: 'Secaucus', state: '' } });
    fetchAddressFromMelissaSaga.next();
    expect(fetchAddressFromMelissaSaga.next().value).toEqual(put(toggleZipCodeLoader(false)));
  });

  it('fetchAddressFromMelissa for Billing', () => {
    const zipCodeValue = 12345;
    const fetchAddressFromMelissaSaga = fetchAddressFromMelissa({
      payload: {
        zipCodeValue,
        formName: 'checkoutShipping',
        formSection: 'shippingAddress',
        shouldLoadShipmentMethods: false,
      },
    });
    fetchAddressFromMelissaSaga.next();
    expect(fetchAddressFromMelissaSaga.next().value).toEqual(
      call(verifyAddressData, { zip: zipCodeValue })
    );
    fetchAddressFromMelissaSaga.next({ suggestedAddress: { city: 'Secaucus', state: 'NJ' } });
    fetchAddressFromMelissaSaga.next();
    expect(fetchAddressFromMelissaSaga.next().value).toEqual(put(toggleZipCodeLoader(false)));
  });

  it('fetchAddressFromMelissa for Shipping in Canada', () => {
    const zipCodeValue = 12345;
    const fetchAddressFromMelissaSaga = fetchAddressFromMelissa({
      payload: {
        zipCodeValue,
        formName: 'checkoutShipping',
        formSection: 'shippingAddress',
        shouldLoadShipmentMethods: true,
      },
    });
    isCanada.mockImplementation(() => true);
    fetchAddressFromMelissaSaga.next();
    expect(fetchAddressFromMelissaSaga.next().value).toEqual(
      call(verifyAddressData, { zip: zipCodeValue })
    );
    fetchAddressFromMelissaSaga.next({ suggestedAddress: { city: 'Secaucus', state: 'NJ' } });
    fetchAddressFromMelissaSaga.next();
    expect(fetchAddressFromMelissaSaga.next().value).toEqual(put(toggleZipCodeLoader(false)));
  });

  it('callEddService for Shipping', () => {
    const payload = {
      zipCode: '10006',
      state: 'NY',
      orderId: '123356',
      skuList: [{ sku: '3232' }, { sku: '56778' }],
      isEDD: true,
      isEditing: false,
      eddAllowedStatesArr: ['NY', 'TX'],
    };
    const getEddCall = callEddService(payload);
    expect(getEddCall.next().value).toEqual(
      put(
        getEddData({
          zipCode: '10006',
          state: 'NY',
          orderId: '123356',
          skuList: [{ sku: '3232' }, { sku: '56778' }],
          ignoreCache: true,
        })
      )
    );
  });
  it('resetEDDdata for Shipping if allowed state is not matched', () => {
    const payload = {
      zipCode: '10006',
      state: 'NY',
      orderId: '123356',
      skuList: [{ sku: '3232' }, { sku: '56778' }],
      isEDD: true,
      isEditing: false,
      eddAllowedStatesArr: ['TX'],
    };
    const getEddCall = callEddService(payload);
    expect(getEddCall.next().value).toEqual(put(resetEddData(true)));
  });

  it('should call validateAddress only when returned from paypal or apple', () => {
    const shippingValues = {
      address: {
        addressLine1: 'address line1',
        addressLine2: 'address line2',
        zipCode: '111111',
        state: 'state',
        city: 'city',
        firstName: 'firstname',
        lastName: 'lastName',
      },
    };
    const expectedValue = {
      address1: shippingValues.address.addressLine1,
      address2: shippingValues.address.addressLine2,
      zip: shippingValues.address.zipCode,
      state: shippingValues.address.state,
      city: shippingValues.address.city,
      firstName: shippingValues.address.firstName,
      lastName: shippingValues.address.lastName,
      doNotShowLoader: true,
    };
    const validateAddressGen = validateAdress(true, shippingValues, false, 'review');
    validateAddressGen.next();
    expect(validateAddressGen.next().value).toEqual(put(verifyAddress({ ...expectedValue })));
  });

  it('should call verify Address with doNotShowLoader false', () => {
    const shippingValues = {
      address: {
        addressLine1: 'address line1',
        addressLine2: 'address line2',
        zipCode: '111111',
        state: 'state',
        city: 'city',
        firstName: 'firstname',
        lastName: 'lastName',
      },
    };
    const expectedValue = {
      address1: shippingValues.address.addressLine1,
      address2: shippingValues.address.addressLine2,
      zip: shippingValues.address.zipCode,
      state: shippingValues.address.state,
      city: shippingValues.address.city,
      firstName: shippingValues.address.firstName,
      lastName: shippingValues.address.lastName,
      doNotShowLoader: false,
    };
    const validateAddressGen = validateAdress(true, shippingValues, false, 'shipping');
    validateAddressGen.next();
    expect(validateAddressGen.next().value).toEqual(put(verifyAddress({ ...expectedValue })));
  });

  it('should call not call the verifyAddress action', () => {
    const validateAddressReturnValue = validateAdress(false, null, false, 'review');
    validateAddressReturnValue.next();
    expect(validateAddressReturnValue.next().value).toEqual(false);
  });
});
