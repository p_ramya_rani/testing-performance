// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, select } from 'redux-saga/effects';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
// eslint-disable-next-line import/no-unresolved
import submitBilling, {
  submitBillingData,
  submitVenmoBilling,
  updatePaymentInstruction,
  updateVenmoPaymentInstruction,
  getAddressData,
  addressIdToString,
  submitBillingWithSingleAPI,
  submitAppleBilling,
} from '../container/CheckoutBilling.saga';
import { isMobileApp } from '../../../../../utils';
import CONSTANTS from '../Checkout.constants';
import { getSetIsBillingVisitedActn } from '../container/Checkout.action';
// import utility from '../util/utility';

describe('CheckoutBilling saga', () => {
  it('CheckoutBilling', () => {
    const CheckoutReviewSaga = submitBilling({ payload: { address: {} } });
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next();

    CheckoutReviewSaga.next(false);
    CheckoutReviewSaga.next();
  });

  it('submitBillingData', () => {
    const CheckoutReviewSaga = submitBillingData({ address: { sameAsShipping: true } }, {});
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next({ onFileAddressKey: '234' });

    expect(CheckoutReviewSaga.next({ body: {} }).value).toEqual(
      call(
        updatePaymentInstruction,
        { address: { sameAsShipping: true } },
        undefined,
        undefined,
        {}
      )
    );
    expect(CheckoutReviewSaga.next(false).value).toEqual(put(getSetIsBillingVisitedActn(true)));
  });

  it('submitBillingDataWithSingleApi with onFileCardID', () => {
    const CheckoutReviewSaga = submitBillingData({ address: { sameAsShipping: true } }, {});
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next(true);
    const submitBillingWithSingleApi = submitBillingWithSingleAPI(
      { address: { sameAsShipping: false }, onFileCardId: '21331232' },
      { addressKey: '12345', addressId: '12345', billingAddressId: '54321' }
    );
    submitBillingWithSingleApi.next({ ERROR_MAP: 'ERROR_MAP' });
    submitBillingWithSingleApi.next();
    submitBillingWithSingleApi.next();
    submitBillingWithSingleApi.next({ addressId: '123' });
    submitBillingWithSingleApi.next({ addressId: '123', ccBrand: 'visa' });
    submitBillingWithSingleApi.next({
      addressId: '123',
      ccBrand: 'visa',
      expMonth: '8',
      expYear: '2020',
    });
  });

  it('submitBillingDataWithSingleApi with onFileAddressKey ', () => {
    const submitBillingWithSingleApi = submitBillingWithSingleAPI({
      address: { sameAsShipping: false, onFileAddressKey: '123445' },
      addressKey: '12345',
      addressId: '12345',
      billingAddressId: '54321',
      expMonth: '8',
      expYear: '2020',
    });
    submitBillingWithSingleApi.next({ ERROR_MAP: 'ERROR_MAP' });
    submitBillingWithSingleApi.next(false);
    submitBillingWithSingleApi.next('13456');
    submitBillingWithSingleApi.next({
      addressId: '123',
      ccBrand: 'visa',
      expMonth: '8',
      expYear: '2020',
    });
    submitBillingWithSingleApi.next({});
  });

  it('submitBillingDataWithSingleApi with same as shipping', () => {
    const submitBillingWithSingleApi = submitBillingWithSingleAPI({
      address: { sameAsShipping: true, onFileAddressKey: '123445' },
      addressKey: '12345',
      addressId: '12345',
      billingAddressId: '54321',
      expMonth: '8',
      expYear: '2020',
    });
    submitBillingWithSingleApi.next({ ERROR_MAP: 'ERROR_MAP' });
    submitBillingWithSingleApi.next(false);
    submitBillingWithSingleApi.next({ onFileAddressKey: '123445', onFileAddressId: '5678' });
    submitBillingWithSingleApi.next({
      addressId: '123',
      ccBrand: 'visa',
      expMonth: '8',
      expYear: '2020',
    });
    submitBillingWithSingleApi.next({});
  });

  it('submitBillingDataWithSingleApi without same as shipping', () => {
    const submitBillingWithSingleApi = submitBillingWithSingleAPI({
      address: { sameAsShipping: false, onFileAddressKey: '123445' },
      addressKey: '12345',
      addressId: '12345',
      billingAddressId: '54321',
      expMonth: '8',
      expYear: '2020',
    });
    submitBillingWithSingleApi.next({ ERROR_MAP: 'ERROR_MAP' });
    submitBillingWithSingleApi.next(false);
    submitBillingWithSingleApi.next(false);
    submitBillingWithSingleApi.next({
      addressId: '123',
      ccBrand: 'visa',
      expMonth: '8',
      expYear: '2020',
    });
    submitBillingWithSingleApi.next({});
  });

  it('submitBillingDataWithSingleApi without onFileAddressKey and guest user', () => {
    const submitBillingWithSingleApi = submitBillingWithSingleAPI({
      address: { sameAsShipping: false },
      addressKey: '12345',
      addressId: '12345',
      billingAddressId: '54321',
      expMonth: '8',
      expYear: '2020',
    });
    submitBillingWithSingleApi.next({ ERROR_MAP: 'ERROR_MAP' });
    submitBillingWithSingleApi.next(false);
    submitBillingWithSingleApi.next(true);
    submitBillingWithSingleApi.next({
      addressId: '123',
      ccBrand: 'visa',
      expMonth: '8',
      expYear: '2020',
    });
    submitBillingWithSingleApi.next({});
  });

  it('submitBillingData with CardId', () => {
    const CheckoutReviewSaga = submitBillingData(
      { address: { sameAsShipping: false }, onFileCardId: '21331232' },
      { addressKey: '12345', addressId: '12345', billingAddressId: '54321' }
    );
    CheckoutReviewSaga.next(); // select isGuest
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next({ onFileAddressKey: '234' });
    expect(CheckoutReviewSaga.next(false).value).not.toEqual(undefined);
  });

  it('submitBillingData with onFileAddressKey and not guest user', () => {
    const CheckoutReviewSaga = submitBillingData(
      { address: { sameAsShipping: false, onFileAddressKey: '54321' } },
      { addressKey: '12345', addressId: '12345', billingAddressId: '54321' }
    );
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next({ onFileAddressKey: '234' });
    expect(CheckoutReviewSaga.next(false).value).not.toEqual(undefined);
  });

  it('submitVenmoBilling Method', () => {
    const CheckoutReviewSaga = submitVenmoBilling({});
    expect(CheckoutReviewSaga.next().value).toEqual(call(updateVenmoPaymentInstruction));
    CheckoutReviewSaga.next();
    const navigate = jest.fn();
    const navigation = {
      navigate,
    };
    const routeToPage = jest.fn();
    const utility = { routeToPage };
    const spyUtility = jest.spyOn(utility, 'routeToPage');
    if (!isMobileApp()) {
      expect(spyUtility).not.toBeCalled();
    } else if (navigation) {
      expect(navigation.navigate(CONSTANTS.CHECKOUT_ROUTES_NAMES.CHECKOUT_REVIEW)).toBeCalled();
    }
  });
  it('submitAppleBilling Method-Error on submit billing', () => {
    const payload = {
      contact: {
        a1: '2203  Woodland Avenue',
        a2: '',
        city: 'Amite',
        state: 'LA',
        postal: '70422',
        ctry: 'United States',
      },
      orderId: 3000332630,
    };
    const CheckoutBillingSaga = submitAppleBilling({ payload });
    CheckoutBillingSaga.next();
    CheckoutBillingSaga.next();
    expect(CheckoutBillingSaga.next(false).value).toEqual(select(BagPageSelectors.getErrorMapping));
  });
  it('getAddressData Method', () => {
    const CheckoutReviewSaga = getAddressData({ address: { onFileAddressKey: '12345' } });
    CheckoutReviewSaga.next();
    const shippingDetails = CheckoutReviewSaga.next().value;
    expect(shippingDetails.onFileAddressId).toEqual(undefined);
  });

  it('addressIdToString Method', () => {
    const addressIdMethod = addressIdToString(12345);
    expect(addressIdMethod).toEqual('12345');
  });

  it('addressIdToString Method without address', () => {
    const addressIdMethod = addressIdToString();
    expect(addressIdMethod).toEqual(null);
  });

  it('updatePaymentInstruction with first variation', () => {
    const func = () => {};
    const CheckoutReviewSaga = updatePaymentInstruction(
      { address: { sameAsShipping: true }, cardNumber: '1234' },
      undefined,
      undefined,
      {},
      func
    );
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next({ billing: {}, paymentId: '123' });
    CheckoutReviewSaga.next();

    CheckoutReviewSaga.next({ body: {} });
    expect(CheckoutReviewSaga.next(false).value).toEqual(undefined);
  });

  it('updatePaymentInstruction with second variation', () => {
    const func = () => {};
    const CheckoutReviewSaga = updatePaymentInstruction(
      { address: { sameAsShipping: true }, cardNumber: '1234' },
      undefined,
      undefined,
      {},
      func
    );
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next({ billing: {} });
    CheckoutReviewSaga.next();

    expect(CheckoutReviewSaga.next(false).value).toEqual(undefined);
  });

  it('updatePaymentInstruction with third variation', () => {
    const func = () => {};
    const CheckoutReviewSaga = updatePaymentInstruction(
      {
        address: { sameAsShipping: true },
        cardNumber: '1234',
        onFileCardId: '243434',
        accountNo: '232322',
        cvv: 323,
        monthExpire: '',
        yearExpire: '',
        setAsDefault: false,
        saveToAccount: false,
      },
      { accountNo: '232322', billingAddressId: '43423423', ccBrand: 'VISA' },
      undefined,
      {},
      func
    );
    CheckoutReviewSaga.next();
    CheckoutReviewSaga.next({ billing: {}, paymentId: '123' });
    expect(CheckoutReviewSaga.next(false).value).not.toEqual(undefined);
  });

  it('updateVenmoPaymentInstruction Method', () => {
    const CheckoutReviewSaga = updateVenmoPaymentInstruction();
    const grandTotal = CheckoutReviewSaga.next(101).value; // select Grand Total
    CheckoutReviewSaga.next(); // select getShippingDestinationValues
    const isVenmoSaveSelected = CheckoutReviewSaga.next(false).value; // select isVenmoPaymentSaveSelected
    const venmoData = {
      nonce: 'fake-venmo-nonce-data',
      deviceData: 'venmoDeviceDataKey',
    };
    const userName = CheckoutReviewSaga.next('@test-user').value;
    CheckoutReviewSaga.next();
    const response = CheckoutReviewSaga.next(venmoData).value; // select getVenmoData
    const requestData = {
      billingAddressId: '12345',
      cardType: 'VENMO',
      cc_brand: 'VENMO',
      cardNumber: userName || 'test-user', // Venmo User Id, for all the scenario's it will have user information from the venmo, for dev, added test-user
      isDefault: 'false',
      orderGrandTotal: grandTotal,
      applyToOrder: true,
      monthExpire: '',
      yearExpire: '',
      setAsDefault: false,
      saveToAccount: false,
      venmoDetails: {
        userId: userName || 'test-user',
        saveVenmoTokenIntoProfile: isVenmoSaveSelected,
        nonce: response.nonce,
        venmoDeviceData: response.deviceData,
      },
    };
    CheckoutReviewSaga.next(requestData);
    expect(CheckoutReviewSaga.next().done).toBeTruthy();
  });
});
