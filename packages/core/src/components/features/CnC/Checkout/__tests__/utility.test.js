/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
// eslint-disable-next-line  import/no-unresolved
import Router from 'next/router';
import { fromJS } from 'immutable';
import Utility, {
  getSelectedCard,
  getCreditCardList,
  getExpirationRequiredFlag,
  getPayPalFlag,
  hasGiftServiceError,
  hasApoFpoError,
  hasRushShippingError,
  getSaveForLaterItemsCatEntryId,
  getSaveForLaterItemsOrderItemId,
  getSfsErroMessage,
  scrollToFirstError,
} from '../util/utility';
// import CardConstants from '../../../account/AddEditCreditCard/container/AddEditCreditCard.constants';
import {
  getSetCurrentOrderIdActn,
  getSetCartActn,
  getSetEstimatedAirMilesActn,
  getSetGiftWrappingTotalActn,
  getSetGiftCardTotalActn,
  setShippingTotal,
  getSetCartStoreActn,
  setTaxesTotal,
  getSetCouponsTotalActn,
  setSavingsTotal,
  getSetItemsTotalAction,
  setItemsCount,
  getSetSubTotal,
  getSetSubTotalWithDiscountsActn,
  getSetGrandTotal,
  getSetRewardsToBeEarnedActn,
  getSetIsPayPalEnabledActn,
  setCartTotalAfterPLCCDiscount,
  getSetPointsAndRewardsActn,
  getSetGiftCardValuesActn,
  getSetAirmilesPromoIdActn,
  getSetAirmilesAccountActn,
} from '../container/Checkout.action';

jest.mock('next/router', () => ({ push: jest.fn() }));
// const { CREDIT_CARDS_BIN_RANGES, ACCEPTED_CREDIT_CARDS } = CardConstants;

const {
  getOrderPointsRecalcFlag,
  updateCartInfo,
  isOrderHasPickup,
  routeToPage,
  getCreditCardType,
  isOrderHasShipping,
  getAvailableStages,
  hasPOBox,
} = Utility;

describe('utility', () => {
  const PLACE_CARD = 'PLACE CARD';
  it('getOrderPointsRecalcFlag', () => {
    expect(getOrderPointsRecalcFlag(false)).toBe(false);
  });
  it('getOrderPointsRecalcFlag when recalcRewards is true', () => {
    expect(getOrderPointsRecalcFlag(true)).toBe(true);
  });
  it('getOrderPointsRecalcFlag when recalcOrderPointsInterval is true and recalcRewards is true ', () => {
    expect(getOrderPointsRecalcFlag(true, true)).toBe(true);
  });
  it('getOrderPointsRecalcFlag when recalcOrderPointsInterval is false and recalcRewards is true ', () => {
    expect(getOrderPointsRecalcFlag(true, true)).toBe(true);
  });
  it('getOrderPointsRecalcFlag when recalcOrderPointsInterval is false and recalcRewards is false ', () => {
    expect(getOrderPointsRecalcFlag(false, false)).toBe(false);
  });
  it('getOrderPointsRecalcFlag when recalcOrderPointsInterval is true and recalcRewards is false ', () => {
    expect(getOrderPointsRecalcFlag(true, false)).toBe(true);
  });
  it('updateCartInfo', () => {
    const cartInfo = {
      estimatedRewards: 60,
      earnedReward: 20,
      pointsToNextReward: 20,
      orderId: '123434',
      estimatedAirMiles: 34,
      shippingTotal: 405,
      giftWrappingTotal: 210,
      giftCardsTotal: 540,
      totalTax: 560,
      savingsTotal: 120,
      couponsTotal: 340,
      orderTotalAfterDiscount: 450,
      totalItems: 5,
      subTotal: 560,
      subTotalWithDiscounts: 560,
      grandTotal: 450,
      appliedGiftCards: [],
      rewardsToBeEarned: 34,
      cartTotalAfterPLCCDiscount: 560,
      orderItems: [],
      stores: [],
      uiFlags: {
        isPaypalEnabled: true,
      },
      airmiles: {
        promoId: 1234,
        accountNumber: 45678,
      },
    };
    const getRewardPoints = {
      estimatedRewards: cartInfo.estimatedRewards,
      earnedReward: cartInfo.earnedReward,
      pointsToNextReward: cartInfo.pointsToNextReward,
    };
    const actions = [
      getSetCurrentOrderIdActn(cartInfo.orderId),
      getSetEstimatedAirMilesActn(cartInfo.estimatedAirMiles),
      setShippingTotal(cartInfo.shippingTotal),
      getSetGiftWrappingTotalActn(cartInfo.giftWrappingTotal),
      getSetGiftCardTotalActn(cartInfo.giftCardsTotal),
      setTaxesTotal(cartInfo.totalTax),
      setSavingsTotal(cartInfo.savingsTotal),
      getSetCouponsTotalActn(cartInfo.couponsTotal),
      getSetItemsTotalAction(cartInfo.orderTotalAfterDiscount),
      cartInfo.totalItems !== null && setItemsCount(cartInfo.totalItems),
      getSetSubTotal(cartInfo.subTotal),
      getSetSubTotalWithDiscountsActn(cartInfo.subTotalWithDiscounts),
      getSetGrandTotal(cartInfo.grandTotal),
      getSetGiftCardValuesActn(cartInfo.appliedGiftCards),
      getSetRewardsToBeEarnedActn(cartInfo.rewardsToBeEarned),
      getSetPointsAndRewardsActn(getRewardPoints),
      setCartTotalAfterPLCCDiscount(cartInfo.cartTotalAfterPLCCDiscount),
      getSetCartActn(cartInfo.orderItems),
      getSetCartStoreActn(cartInfo.stores),
      getSetIsPayPalEnabledActn(cartInfo.uiFlags.isPaypalEnabled),
      getSetAirmilesPromoIdActn(cartInfo.airmiles.promoId),
      getSetAirmilesAccountActn(cartInfo.airmiles.accountNumber),
    ];
    const isUpdateCartItems = true;
    expect(updateCartInfo(cartInfo, isUpdateCartItems)).toEqual(actions);
  });
  it('isOrderHasPickup', () => {
    const cartItems = fromJS({
      miscInfo: {
        store: [{}, {}],
      },
    });
    expect(isOrderHasPickup(cartItems)).toBe(0);
  });

  it('routeToPage', () => {
    expect(routeToPage({ to: 'test?', as: 'test' }, {}));
    expect(Router.push).toHaveBeenCalled();
  });
  it('routeToPage shipping', () => {
    expect(routeToPage('shipping'));
    expect(Router.push).toHaveBeenCalled();
  });
  it('isOrderHasShipping', () => {
    const cartItems = fromJS({
      miscInfo: {
        store: [{}, {}],
      },
    });
    expect(isOrderHasShipping(cartItems)).toBe(1);
  });

  it('getCreditCardType', () => {
    expect(getCreditCardType({ cardNumber: 'a2f', cardType: '' })).toBe(null);
  });
  it('getCreditCardType', () => {
    expect(getCreditCardType({ cardNumber: '*', cardType: '' })).toBe(null);
  });
  it('getCreditCardType', () => {
    const navigate = jest.fn();
    expect(getCreditCardType({ cardNumber: '', cardType: '' })).toBe(null);
    expect(navigate).toHaveBeenCalledTimes(0);
  });
  it('getSelectedCard ', () => {
    const obj = {
      creditCardList: [
        {
          accountNo: '************3721',
          ccBrand: PLACE_CARD,
          ccType: PLACE_CARD,
          creditCardId: 82596,
          defaultInd: false,
          onFileCardKey: 82596,
          addressDetails: { phone1: '1234567891' },
        },
        {
          accountNo: '************9847',
          ccBrand: PLACE_CARD,
          ccType: PLACE_CARD,
          creditCardId: 11111,
          defaultInd: false,
          onFileCardKey: 11111,
          addressDetails: { phone1: '1211111111' },
        },
      ],
      onFileCardKey: 82596,
    };
    const card = {
      accountNo: '************3721',
      ccBrand: PLACE_CARD,
      ccType: PLACE_CARD,
      creditCardId: 82596,
      defaultInd: false,
      onFileCardKey: 82596,
      addressDetails: { phone1: '1234567891' },
    };
    expect(getSelectedCard(obj, 82596)).toEqual(card);
  });
  it('getSelectedCard should return undefined', () => {
    const obj1 = {
      creditCardList: [],
      onFileCardKey: '',
    };

    expect(getSelectedCard(obj1, 1111)).toEqual(undefined);
  });
  it('getCreditCardList should return undefined ', () => {
    const obj = {
      cardList: [],
    };
    expect(getCreditCardList(obj)).toBeFalsy();
  });
  it('getExpirationRequiredFlag to be false', () => {
    const obj = {
      cardType: PLACE_CARD,
    };
    expect(getExpirationRequiredFlag(obj)).toBeFalsy();
  });
  it('getExpirationRequiredFlag to be true', () => {
    const obj = {
      cardType: 'APPLE PAY',
    };
    expect(getExpirationRequiredFlag(obj)).toBeTruthy();
  });
  it('getAvailableStages', () => {
    const cartItems = fromJS({});
    expect(
      getAvailableStages(cartItems, {
        billingLabel: '',
        reviewLabel: '',
      })
    ).toStrictEqual(['billing', 'review']);
  });

  it('getCreditCardList should return  list ', () => {
    const obj = {
      cardList: [
        {
          accountNo: '************3743',
          ccBrand: PLACE_CARD,
          ccType: PLACE_CARD,
          creditCardId: 82596,
          defaultInd: false,
          onFileCardKey: 82596,
          addressDetails: { phone1: '1234567891' },
        },
        {
          accountNo: '************9847',
          ccBrand: PLACE_CARD,
          ccType: PLACE_CARD,
          creditCardId: 11111,
          defaultInd: false,
          onFileCardKey: 11111,
          addressDetails: { phone1: '1211111111' },
        },
      ],
    };

    expect(getCreditCardList(obj)).toBeFalsy();
  });

  it('getAvailableStages including shipping', () => {
    const cartItems = fromJS({
      miscInfo: {
        store: [{}, {}],
      },
    });
    expect(
      getAvailableStages(cartItems, {
        billingLabel: '',
        reviewLabel: '',
      })
    ).toStrictEqual(['shipping', 'billing', 'review']);
  });
  it('getPayPalFlag  to be true', () => {
    const navigation = {
      state: {
        params: {
          isPayPalFlow: true,
        },
      },
    };
    expect(getPayPalFlag(navigation)).toBeTruthy();
  });
  it('getPayPalFlag to be false', () => {
    const navigation = {
      state: {
        params: {
          isPayPalFlow: false,
        },
      },
    };
    expect(getPayPalFlag(navigation)).toBeFalsy();
  });
  it('hasPOBox', () => {
    expect(hasPOBox('dsa', 'dasdas')).toBe(false);
  });
  it('hasPOBox to be true', () => {
    expect(hasPOBox('345 Airport Rd', 'PO Box 1242')).toBe(true);
  });
  it('hasPOBox to be false', () => {
    expect(hasPOBox('345 Airport Rd', 'postal office 1')).toBe(false);
  });
  it('scrollToFirstError should return undefined', () => {
    const errors = { name: 'first name' };
    expect(scrollToFirstError(errors)).toBeUndefined();
  });
  it('hasGiftServiceError to return true', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_GIFT_SERVICE'],
      },
    };
    expect(hasGiftServiceError(checkoutServerError)).toBeTruthy();
  });
  it('hasGiftServiceError to return false', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: [],
      },
    };
    expect(hasGiftServiceError(checkoutServerError)).toBeFalsy();
  });
  it('hasGiftServiceError to return null', () => {
    const checkoutServerError = {};
    expect(hasGiftServiceError(checkoutServerError)).toBeNull();
  });
  it('hasApoFpoError to return true', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_APO_FPO_ADDRESS'],
      },
    };
    expect(hasApoFpoError(checkoutServerError)).toBeTruthy();
  });
  it('hasRushShippingError to return false', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_APO_FPO_ADDRESS'],
      },
    };
    expect(hasRushShippingError(checkoutServerError)).toBeFalsy();
  });
  it('hasRushShippingError to return true', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_APO_FPO_ADDRESS', '_ERR_EXPRESS_RUSH_SHIPPING'],
      },
    };
    expect(hasRushShippingError(checkoutServerError)).toBeTruthy();
  });
  it('getSaveForLaterItemsCatEntryId should return undefined', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_APO_FPO_ADDRESS', '_ERR_EXPRESS_RUSH_SHIPPING'],
      },
    };
    expect(getSaveForLaterItemsCatEntryId(checkoutServerError)).toBeUndefined();
  });
  it('getSaveForLaterItemsCatEntryId should return id', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: [
          '_ERR_APO_FPO_ADDRESS',
          '_ERR_EXPRESS_RUSH_SHIPPING',
          'CATENTRY_ID=202002',
        ],
      },
    };
    expect(getSaveForLaterItemsCatEntryId(checkoutServerError)).toEqual(['202002']);
  });
  it('getSaveForLaterItemsOrderItemId should return id', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: [
          '_ERR_APO_FPO_ADDRESS',
          '_ERR_EXPRESS_RUSH_SHIPPING',
          'ORDER_ITEM_ID=100011',
        ],
      },
    };
    expect(getSaveForLaterItemsOrderItemId(checkoutServerError)).toEqual(['100011']);
  });
  it('getSaveForLaterItemsOrderItemId should return undefined', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_APO_FPO_ADDRESS', '_ERR_EXPRESS_RUSH_SHIPPING'],
      },
    };
    expect(getSaveForLaterItemsOrderItemId(checkoutServerError)).toBeUndefined();
  });
  it('getSfsErroMessage should return empty', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: [],
      },
    };
    expect(getSfsErroMessage(checkoutServerError)).toEqual('');
  });
  it('getSfsErroMessage should return lbl_sfs_error_rush_shipping', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_EXPRESS_RUSH_SHIPPING'],
      },
    };
    expect(getSfsErroMessage(checkoutServerError)).toEqual('lbl_sfs_error_rush_shipping');
  });
  it('getSfsErroMessage should return lbl_sfs_error_gift_service', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_GIFT_SERVICE'],
      },
    };
    expect(getSfsErroMessage(checkoutServerError)).toEqual('lbl_sfs_error_gift_service');
  });
  it('getSfsErroMessage should return lbl_sfs_error_shipping_address', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_APO_FPO_ADDRESS'],
      },
    };
    expect(getSfsErroMessage(checkoutServerError)).toEqual('lbl_sfs_error_shipping_address');
  });
  it('getSfsErroMessage should return lbl_sfs_error_address_rush', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_APO_FPO_ADDRESS', '_ERR_EXPRESS_RUSH_SHIPPING'],
      },
    };
    expect(getSfsErroMessage(checkoutServerError)).toEqual('lbl_sfs_error_address_rush');
  });

  it('getSfsErroMessage should return lbl_sfs_error_gift_rush', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: ['_ERR_APO_FPO_ADDRESS', '_ERR_GIFT_SERVICE'],
      },
    };
    expect(getSfsErroMessage(checkoutServerError)).toEqual('lbl_sfs_error_gift_address');
  });
  it('getSfsErroMessage should return lbl_sfs_error_gift_rush', () => {
    const checkoutServerError = {
      billingError: {
        errorParameters: [
          '_ERR_APO_FPO_ADDRESS',
          '_ERR_EXPRESS_RUSH_SHIPPING',
          '_ERR_APO_FPO_ADDRESS',
          '_ERR_GIFT_SERVICE',
        ],
      },
    };
    expect(getSfsErroMessage(checkoutServerError)).toEqual('lbl_sfs_error_gift_rush');
  });
});
