// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CheckoutPage } from '../views/CheckoutPage.view.native';

describe('CheckoutPageVanilla component', () => {
  it('should renders correctly pickup', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { section: 'pickup' } },
      onPickupSubmit: () => {},
      submitShippingSection: () => {},
      navigation: { addListener: () => {} },
      currentStage: 'PICKUP',
      pickUpLabels: { venmoBannerText: 'abc' },
      shippingProps: { shipmentMethods: [] },
    };
    const component = shallow(<CheckoutPage {...props} />);
    component.instance().scrollToTop();
    component.instance().submitShippingSection({});
    component.instance().getCurrentPage();
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with merge cart message', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { subSection: 'shipping' } },
      onPickupSubmit: () => {},
      setMaxItemErrorAction: () => {},
      showMaxItemErrorDisplayed: false,
      setMaxItemErrorDisplayedAction: () => {},
      pickUpLabels: { venmoBannerText: 'abc' },
      currentStage: 'SHIPPING',
      shippingProps: { shipmentMethods: [] },
      navigation: { addListener: () => {} },
    };
    const component = shallow(<CheckoutPage {...props} />);
    component.instance().getCurrentPage();
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with shipping', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { subSection: 'shipping' } },
      onPickupSubmit: () => {},
      pickUpLabels: { venmoBannerText: 'abc' },
      currentStage: 'SHIPPING',
      shippingProps: { shipmentMethods: [] },
      navigation: { addListener: () => {} },
    };
    const component = shallow(<CheckoutPage {...props} />);
    component.instance().getCurrentPage();
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with confirmation', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { subSection: 'CONFIRMATION' } },
      onPickupSubmit: () => {},
      pickUpLabels: { venmoBannerText: 'abc' },
      currentStage: 'CONFIRMATION',
      shippingProps: { shipmentMethods: [] },
      navigation: { addListener: () => {} },
    };
    const component = shallow(<CheckoutPage {...props} />);
    component.instance().getCurrentPage();
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with REVIEW', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { subSection: 'REVIEW' } },
      onPickupSubmit: () => {},
      pickUpLabels: { venmoBannerText: 'abc' },
      currentStage: 'REVIEW',
      shippingProps: { shipmentMethods: [] },
      navigation: { addListener: () => {} },
      pickUpAlternatePerson: { firstName: 'abc' },
      reviewProps: {},
    };
    const component = shallow(<CheckoutPage {...props} />);
    component.instance().getCurrentPage();
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with BILLING', () => {
    const props = {
      className: 'className',
      backLinkText: 'backLinkText',
      nextButtonText: 'nextButtonText',
      disableNext: false,
      backLinkHandler: () => {},
      disableBackLink: false,
      router: { query: { subSection: 'BILLING' } },
      onPickupSubmit: () => {},
      pickUpLabels: { venmoBannerText: 'abc' },
      currentStage: 'BILLING',
      shippingProps: { shipmentMethods: [] },
      navigation: { addListener: () => {} },
    };
    const component = shallow(<CheckoutPage {...props} />);
    component.instance().getCurrentPage();
    expect(component).toMatchSnapshot();
  });
});
