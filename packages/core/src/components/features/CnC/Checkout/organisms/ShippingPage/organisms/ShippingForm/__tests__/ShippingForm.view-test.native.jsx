// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { ShippingFormVanilla } from '../views/ShippingForm.view.native';

const mockedSetEditShippingFlag = jest.fn();
const mockedHandleSubmit = jest.fn();
describe('Shipping Form', () => {
  it('should render correctly', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      handleSubmit: jest.fn(),
      scrollToTop: jest.fn(),
      isGuest: true,
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with non us site', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      handleSubmit: jest.fn(),
      isGuest: true,
      isUsSite: false,
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with signed in', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      handleSubmit: jest.fn(),
      isGuest: false,
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with submit verification errors', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      handleSubmit: jest.fn(),
      isGuest: false,
      submitErrors: { address: { formGeneric: 'Some Error Occurred !!!' } },
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
    expect(tree.find(Notification)).toHaveLength(1);
  });

  it('should call setEditShippingFlag from toggleIsEditing', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      scrollToTop: jest.fn(),
      isGuest: true,
      setEditShippingFlag: mockedSetEditShippingFlag,
      handleSubmit: mockedHandleSubmit,
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    const componentInstance = tree.instance();
    componentInstance.setEditState();
    expect(mockedSetEditShippingFlag).toHaveBeenCalled();
  });
});
