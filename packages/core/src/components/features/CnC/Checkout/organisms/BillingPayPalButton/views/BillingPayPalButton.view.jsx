// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import styles from '../styles/BillingPayPalButton.style';
import withStyles from '../../../../../../common/hoc/withStyles';
import PayPalButton from '../../../../common/organism/PayPalButton';

export class BillingPayPalButton extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string.isRequired,
    labels: PropTypes.shape({}).isRequired,
    containerId: PropTypes.string.isRequired,
  };

  /**
   * @function render
   * @description render method to be called of component
   */
  render() {
    const { className, containerId } = this.props;

    return (
      <div className={className}>
        <div>
          <div>
            <PayPalButton
              className="billing-payPal-button"
              containerId={containerId}
              isBillingPage
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(BillingPayPalButton, styles);
export { BillingPayPalButton as BillingPayPalButtonVanilla };
