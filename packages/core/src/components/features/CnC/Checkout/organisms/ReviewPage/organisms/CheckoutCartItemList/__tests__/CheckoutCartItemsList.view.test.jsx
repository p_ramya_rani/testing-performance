// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { fromJS } from 'immutable';
import { shallow } from 'enzyme';
import { CheckoutCartItemsListVanilla } from '../views/CheckoutCartItemsList.view';

const props = {
  itemsCount: 0,
  items: fromJS([
    {
      productInfo: {
        skuId: '',
        name: '',
        imagePath: '',
        color: {
          name: 'Clay',
        },
        fit: 'slim',
        size: '5S',
      },
      itemInfo: {
        quantity: 1,
        listPrice: 12,
        offerPrice: 10,
      },

      miscInfo: {
        store: '',
        storeAddress: {},
        orderItemType: 'BOPIS',
        bossStartDate: {
          day: '',
          month: '',
          date: '',
        },
        bossEndDate: {},
      },
    },
  ]),
  currencySymbol: '$',
  labels: {},
  bagPageLabels: {},
  className: '',
  gettingSortedItemList: jest.fn(),
};
describe('testing block for CheckoutCartItemsListVanilla', () => {
  it('CheckoutCartItemsListVanilla should be rendered correclty ', () => {
    const component = shallow(<CheckoutCartItemsListVanilla {...props} />);
    expect(component).toBeDefined();
  });
});

const bossprops = {
  itemsCount: 0,
  items: fromJS([
    {
      miscInfo: {
        store: '',
        storeAddress: {},
        orderItemType: 'BOSS',
        bossStartDate: {
          day: '',
          month: '',
          date: '',
        },
        bossEndDate: {},
      },
    },
  ]),
  currencySymbol: '$',
  labels: {},
  bagPageLabels: {},
  className: '',
  gettingSortedItemList: jest.fn(),
};
describe('testing block for CheckoutCartItemsListVanilla', () => {
  it('CheckoutCartItemsListVanilla should be rendered correclty ', () => {
    const component = shallow(<CheckoutCartItemsListVanilla {...bossprops} />);
    expect(component).toBeDefined();
  });
});

describe('getPickupHeader', () => {
  it('should call getPickupHeader', () => {
    const args = {
      deliveryItem: {
        duration: '',
      },
      isShowHeader: true,
    };
    const component = shallow(<CheckoutCartItemsListVanilla {...bossprops} />);
    expect(component.instance().getPickupHeader(args)).toBeTruthy();
  });

  it('should call getShippingListItems', () => {
    const pickUpList = {
      list: [{}, {}],
    };

    const component = shallow(<CheckoutCartItemsListVanilla {...bossprops} />);
    expect(component.instance().getShippingListItems(pickUpList)).toBeTruthy();
  });

  it('should call getOrderItem', () => {
    const args = {
      labels: '',
      currencySymbol: '',
      disableProductRedirect: '',
    };
    const component = shallow(<CheckoutCartItemsListVanilla {...bossprops} />);
    expect(component.instance().getOrderItem(args)).toBeTruthy();
  });
  it('should call renderOrderItems', () => {
    const component = shallow(<CheckoutCartItemsListVanilla {...bossprops} />);
    expect(component.instance().renderOrderItems()).toBeTruthy();
  });

  it('should call renderItems', () => {
    const component = shallow(<CheckoutCartItemsListVanilla {...props} />);
    expect(component.instance().renderItems()).toBeTruthy();
  });
});
