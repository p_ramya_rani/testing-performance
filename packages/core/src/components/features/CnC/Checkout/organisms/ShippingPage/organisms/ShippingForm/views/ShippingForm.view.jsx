/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { FormSection, reduxForm, change, resetSection } from 'redux-form';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import BodyCopy from '../../../../../../../../common/atoms/BodyCopy';
import { Row, Col } from '../../../../../../../../common/atoms';
import AddressFields from '../../../../../../../../common/molecules/AddressFields';
import SMSFormFields from '../../../../../../../../common/molecules/SMSFormFields';
import createValidateMethod from '../../../../../../../../../utils/formValidation/createValidateMethod';
import CheckoutSectionTitleDisplay from '../../../../../../common/molecules/CheckoutSectionTitleDisplay';
import ShipmentMethods from '../../../../../../common/molecules/ShipmentMethods';
import CheckoutFooter from '../../../../../molecules/CheckoutFooter';
import EmailSignUpCheckBox from '../../../../../molecules/EmailSignUpCheckBox';
import getStandardConfig from '../../../../../../../../../utils/formValidation/validatorStandardConfig';
import withStyles from '../../../../../../../../common/hoc/withStyles';
import RegisteredShippingForm from '../../RegisteredShippingForm';
import CheckoutOrderInfo from '../../../../../molecules/CheckoutOrderInfoMobile';
import { getLabelValue, routerPush } from '../../../../../../../../../utils';
import { propTypes, defaultProps } from './ShippingForm.view.utils';
import {
  scrollToFirstError,
  hasGiftServiceError,
  hasRushShippingError,
  getSfsErroMessage,
  hasApoFpoError,
  getSaveForLaterItemsOrderItemId,
  getSaveForLaterItemsCatEntryId,
} from '../../../../../util/utility';
import GiftServices from '../../../molecules/GiftServices';

import styles from '../styles/ShippingForm.view.style';
import GenericSkeleton from '../../../../../../../../common/molecules/GenericSkeleton/GenericSkeleton.view';
import WarningPrompt from '../../../../WarningPrompt';

const formName = 'checkoutShipping';

class ShippingForm extends React.Component {
  static changeAddressFields(nextProps) {
    const { onFileAddressKey, dispatch, userAddresses, shippingAddress } = nextProps;
    let address = {};
    let isDefaultAddress = false;
    if (userAddresses && userAddresses.size > 0) {
      address = userAddresses.find((add) => add.addressId === onFileAddressKey);
      dispatch(change(formName, 'address.addressLine1', address.addressLine[0]));
      dispatch(change(formName, 'address.addressLine2', address.addressLine[1]));
      isDefaultAddress = address.primary === 'true';
      if (!isDefaultAddress && userAddresses.size === 1) isDefaultAddress = true;
    } else if (shippingAddress) {
      address = shippingAddress;
      dispatch(change(formName, 'address.addressLine1', address.addressLine1));
      dispatch(change(formName, 'address.addressLine2', address.addressLine2));
      isDefaultAddress = true;
    }
    dispatch(change(formName, 'address.firstName', address.firstName));
    dispatch(change(formName, 'address.lastName', address.lastName));
    dispatch(change(formName, 'address.city', address.city));
    dispatch(change(formName, 'address.zipCode', address.zipCode));
    dispatch(change(formName, 'address.state', address.state));
    dispatch(change(formName, 'address.phoneNumber', address.phone1));
    dispatch(change(formName, 'defaultShipping', isDefaultAddress));
    return { isEditingMode: true };
  }

  constructor(props) {
    super(props);
    this.state = {
      isEditing: false,
      isEditingMode: false,
      editShipmentDetailsError: '',
    };
    this.isAddressModalEmptied = false;
    this.addNewAddressEnabled = false;
    this.editShippingErrorRef = React.createRef();
  }

  componentDidMount() {
    const { editShippingAddress } = this.props;
    if (editShippingAddress) {
      this.toggleIsEditing();
    }
  }

  shouldComponentUpdate() {
    const { isSubmitting } = this.props;
    return !isSubmitting;
  }

  componentDidUpdate(prevProps) {
    this.checkPropsOnUpdation(prevProps);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isEditing, isEditingMode, shippingAddress } = prevState;
    const { onFileAddressKey, userAddresses } = nextProps;
    if (
      isEditing &&
      onFileAddressKey &&
      ((userAddresses && userAddresses.size > 0) || shippingAddress) &&
      !isEditingMode
    ) {
      return ShippingForm.changeAddressFields(nextProps);
    }
    return null;
  }

  checkPropsOnUpdation = (prevProps) => {
    const { dispatch, isAddNewAddress } = this.props;
    if (isAddNewAddress && !this.isAddressModalEmptied && !this.addNewAddressEnabled) {
      dispatch(resetSection(formName, 'address'));
      if (!this.isAddressModalEmptied) {
        this.isAddressModalEmptied = true;
      }
      if (!this.addNewAddressEnabled) {
        this.addNewAddressEnabled = true;
      }
    }
    this.toggleAddressState();

    this.checkPropsOnMoreUpdation(prevProps);
  };

  closeEditingMode = () => {
    this.setState({ isEditing: false });
  };

  checkPropsOnMoreUpdation = (prevProps) => {
    const { dispatch, defaultAddressId, onFileAddressKey, isAddNewAddress } = this.props;
    const { isEditingMode, isEditing } = this.state;
    const { defaultAddressId: prevDefaultAddressId } = prevProps;
    if (prevDefaultAddressId && defaultAddressId && defaultAddressId !== prevDefaultAddressId) {
      dispatch(change(formName, 'onFileAddressKey', defaultAddressId));
      this.closeEditingMode();
    } else if (defaultAddressId && !onFileAddressKey && !isAddNewAddress) {
      dispatch(change(formName, 'onFileAddressKey', defaultAddressId));
    }
    if (!isEditing && isEditingMode) {
      this.setState({ isEditingMode: false });
    }
  };

  toggleAddressState = () => {
    const { isAddNewAddress } = this.props;
    if (this.isAddressModalEmptied) {
      this.isAddressModalEmptied = false;
    }
    if (!isAddNewAddress && this.addNewAddressEnabled) {
      this.addNewAddressEnabled = false;
    }
  };

  toggleIsEditing = () => {
    const { isEditing } = this.state;
    const { setEditShippingFlag } = this.props;
    setEditShippingFlag(!isEditing);
    this.setState({ isEditing: !isEditing, editShipmentDetailsError: '' });
  };

  renderEmailSignUp = () => {
    const {
      orderHasPickUp,
      isGuest,
      isUsSite,
      emailSignUpLabels,
      emailSignUpFlags,
      emailSignUpCaRichText,
    } = this.props;
    return (
      !orderHasPickUp &&
      isGuest &&
      !isUsSite && (
        <EmailSignUpCheckBox
          labels={emailSignUpLabels}
          fieldName="emailSignUp"
          bottomSeparator
          emailSignUpFlags={emailSignUpFlags}
          emailSignUpCaRichText={emailSignUpCaRichText}
        />
      )
    );
  };

  handleSubmit = (e) => {
    const {
      handleSubmit,
      emailSignUpLabels: { lbl_shipping_addressEditError: shippingAddressEditError },
    } = this.props;
    const { isEditing } = this.state;
    if (isEditing && this.editShippingErrorRef && this.editShippingErrorRef.current) {
      e.preventDefault();
      this.setState({
        editShipmentDetailsError: shippingAddressEditError,
      });
      return this.editShippingErrorRef.current.scrollIntoView(false);
    }
    return handleSubmit(e);
  };

  renderGiftServices = () => {
    const { isGiftServicesChecked, dispatch } = this.props;
    return (
      <Col colSize={{ small: 6, medium: 8, large: 6 }}>
        <GiftServices
          showDefaultCheckbox={false}
          formName={formName}
          formSection="giftServices"
          variation="secondary"
          isGiftServicesChecked={isGiftServicesChecked}
          dispatch={dispatch}
          deselectCallback={this.deselctGiftService}
        />
      </Col>
    );
  };

  afterSflSuccessCallback = () => {
    const { cartOrderItems } = this.props;
    if (!cartOrderItems.size) {
      routerPush('/bag', '/bag');
    } else {
      this.handleSubmit();
    }
  };

  selectStandardShipping = () => {
    const { dispatch } = this.props;
    dispatch(change(formName, 'shipmentMethods.shippingMethodId', '901101'));
  };

  clearServerError = () => {
    const { checkoutServerError, clearCheckoutServerError } = this.props;
    if (checkoutServerError && clearCheckoutServerError) {
      clearCheckoutServerError({});
    }
  };

  deselctGiftService = () => {
    const { dispatch } = this.props;
    dispatch(change('GiftServices', 'hasGiftWrapping', false));
    dispatch(change('GiftServices', `message`, ''));
    dispatch(change('GiftServices', `optionId`, 'standard'));
    this.clearServerError();
  };

  renderShippingErrors = () => {
    const { ServerErrors, checkoutServerError, cartOrderItems, addItemToSflList, labels } =
      this.props;

    if (checkoutServerError && hasApoFpoError(checkoutServerError)) {
      const itemIds = getSaveForLaterItemsOrderItemId(checkoutServerError) || [];
      const catEntryId = getSaveForLaterItemsCatEntryId(checkoutServerError) || [];
      const afterSuccessCallback = () => {
        this.afterSflSuccessCallback();
      };
      return (
        <WarningPrompt
          heading={getLabelValue(labels, 'lbl_sfs_shipping_address_error', 'shipping', 'checkout')}
          body={getLabelValue(
            labels,
            getSfsErroMessage(checkoutServerError),
            'shipping',
            'checkout'
          )}
          type="shippingAddress"
          cartOrderItems={cartOrderItems}
          itemIds={itemIds}
          saveForLaterAction={() =>
            addItemToSflList({ catEntryId, itemId: itemIds, afterSuccessCallback })
          }
          labels={labels}
          scrollIntoView
        />
      );
    }

    if (!ServerErrors) {
      return null;
    }
    return <ServerErrors />;
  };

  renderShippingMethodErrors = () => {
    const { checkoutServerError, cartOrderItems, addItemToSflList, labels } = this.props;
    const giftServiceError = checkoutServerError && hasGiftServiceError(checkoutServerError);
    const rushShippingError = checkoutServerError && hasRushShippingError(checkoutServerError);
    if (giftServiceError || rushShippingError) {
      const itemIds = getSaveForLaterItemsOrderItemId(checkoutServerError) || [];
      const catEntryId = getSaveForLaterItemsCatEntryId(checkoutServerError) || [];
      const afterSuccessCallback = () => {
        this.afterSflSuccessCallback();
      };
      return (
        <WarningPrompt
          heading={getLabelValue(labels, 'lbl_sfs_order_error', 'shipping', 'checkout')}
          body={getLabelValue(
            labels,
            getSfsErroMessage(checkoutServerError),
            'shipping',
            'checkout'
          )}
          type={giftServiceError && !rushShippingError ? 'giftservices' : 'shipingMethod'}
          cartOrderItems={cartOrderItems}
          itemIds={itemIds}
          labels={labels}
          saveForLaterAction={() =>
            addItemToSflList({
              catEntryId,
              itemId: itemIds,
              afterSuccessCallback,
            })
          }
          secondaryCtaAction={() => {
            if (giftServiceError && !rushShippingError) {
              this.deselctGiftService();
            } else {
              this.selectStandardShipping();
            }
          }}
        />
      );
    }
    return null;
  };

  shouldDisableCheckoutCta = () => {
    const { checkoutServerError, isGuest } = this.props;
    const giftServiceError = checkoutServerError && hasGiftServiceError(checkoutServerError);
    const rushShippingError = checkoutServerError && hasRushShippingError(checkoutServerError);
    const hasShippingAddressError = checkoutServerError && hasApoFpoError(checkoutServerError);
    if (!isGuest && (giftServiceError || rushShippingError || hasShippingAddressError)) {
      return true;
    }
    return false;
  };

  renderVerifyAddressErrors = () => {
    const { submitErrors } = this.props;
    return (
      submitErrors &&
      submitErrors.address && (
        <Notification
          className="verify-response-error"
          status="error"
          scrollIntoView
          isErrorTriangleIcon
          message={submitErrors && submitErrors.address && submitErrors.address.formGeneric}
        />
      )
    );
  };

  /**
   * @function handleShipIntClick
   * function to open country selector popup on shipping page
   */
  handleShipIntClick = (e) => {
    const { toggleCountrySelector } = this.props;
    e.preventDefault();
    toggleCountrySelector({ isModalOpen: true });
  };

  getClassName = () => {
    const { userAddresses, shippingAddress } = this.props;
    return (userAddresses && userAddresses.size !== 0) || shippingAddress
      ? 'hide-on-desktop hide-on-tablet'
      : '';
  };

  render() {
    const {
      addressLabels: { addressFormLabels },
      className,
      dispatch,
      isOrderUpdateChecked,
      smsSignUpLabels,
      selectedShipmentId,
      addressPhoneNo,
      isGuest,
      orderHasPickUp,
      shipmentMethods,
      loadShipmentMethods,
      routeToPickupPage,
      isSaveToAddressBookChecked,
      isUsSite,
      shippingAddressId,
      toggleAddNewAddress,
      isAddNewAddress,
      updateShippingAddress,
      addNewShippingAddress,
      labels,
      shippingAddress,
      setDefaultAddressId,
      syncErrorsObject,
      isVenmoPaymentInProgress,
      isVenmoShippingDisplayed,
      showAccordian,
      isMobile,
      pageCategory,
      isLoadingShippingMethods,
      checkoutRoutingDone,
      bagLoading,
      isGiftOptionsEnabled,
      smsOrderUpdatesRichText,
      address,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      isEDD,
      updateEdd,
      cartOrderItems,
      eddAllowedStatesArr,
      currentOrderId,
      formMetaInfo,
      formSyncError,
      resetShippingEddData,
      isEddABTest,
      submitErrors,
      pageName,
      appliedExpiredCoupon,
      checkoutServerError,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
    } = this.props;
    const { isEditing, editShipmentDetailsError } = this.state;
    const nextButtonText =
      isVenmoPaymentInProgress && !isVenmoShippingDisplayed
        ? getLabelValue(labels, 'lbl_shipping_reviewText', 'shipping', 'checkout')
        : getLabelValue(labels, 'lbl_shipping_billingText', 'shipping', 'checkout');
    return (
      <div className={className}>
        <CheckoutSectionTitleDisplay
          title={getLabelValue(labels, 'lbl_shipping_header', 'shipping', 'checkout')}
        />
        {this.renderShippingErrors()}
        <BodyCopy
          fontFamily="primary"
          fontSize="fs26"
          fontWeight="regular"
          data-locator="shipping-details"
          className={`elem-mb-XS elem-mt-MED ${this.getClassName()}`}
        >
          {getLabelValue(labels, 'lbl_shipping_sectionHeader', 'shipping', 'checkout')}
        </BodyCopy>
        {this.renderVerifyAddressErrors()}
        <form
          name={formName}
          className={className}
          onSubmit={this.handleSubmit}
          id={formName}
          isEditing={isEditing}
        >
          {!bagLoading && checkoutRoutingDone ? (
            <>
              {!isGuest && (
                <RegisteredShippingForm
                  {...this.props}
                  isEditing={isEditing}
                  isMobile={isMobile}
                  toggleIsEditing={this.toggleIsEditing}
                  dispatch={dispatch}
                  isAddNewAddress={isAddNewAddress}
                  toggleAddNewAddress={toggleAddNewAddress}
                  isSaveToAddressBookChecked={isSaveToAddressBookChecked}
                  shippingAddressId={shippingAddressId}
                  updateShippingAddress={updateShippingAddress}
                  addNewShippingAddress={addNewShippingAddress}
                  shippingAddress={shippingAddress}
                  labels={labels}
                  afterAddressUpdate={this.closeEditingMode}
                  setDefaultAddressId={setDefaultAddressId}
                  syncErrorsObject={syncErrorsObject}
                  errorMessageRef={this.editShippingErrorRef}
                  editShipmentDetailsError={editShipmentDetailsError}
                  handleShipIntClick={this.handleShipIntClick}
                  address={address}
                  zipCodeABTestEnabled={zipCodeABTestEnabled}
                  getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                  zipCodeLoader={zipCodeLoader}
                  updateEdd={updateEdd}
                  isEDD={isEDD}
                  cartOrderItems={cartOrderItems}
                  eddAllowedStatesArr={eddAllowedStatesArr}
                  currentOrderId={currentOrderId}
                  formMetaInfo={formMetaInfo}
                  formSyncError={formSyncError}
                  submitErrors={submitErrors}
                  resetShippingEddData={resetShippingEddData}
                  checkoutServerError={checkoutServerError}
                  clearServerError={this.clearServerError}
                />
              )}
              {isGuest && (
                <div className="address-form">
                  <FormSection name="address">
                    <AddressFields
                      addressFormLabels={addressFormLabels}
                      showDefaultCheckbox={false}
                      formName={formName}
                      formSection="address"
                      variation="secondary"
                      dispatch={dispatch}
                      addressPhoneNo={addressPhoneNo}
                      loadShipmentMethods={loadShipmentMethods}
                      handleShipIntClick={this.handleShipIntClick}
                      address={address}
                      zipCodeABTestEnabled={zipCodeABTestEnabled}
                      getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                      zipCodeLoader={zipCodeLoader}
                      updateEdd={updateEdd}
                      cartOrderItems={cartOrderItems}
                      isEDD={isEDD}
                      eddAllowedStatesArr={eddAllowedStatesArr}
                      currentOrderId={currentOrderId}
                      formMetaInfo={formMetaInfo}
                      formSyncError={formSyncError}
                      submitErrors={submitErrors}
                      resetShippingEddData={resetShippingEddData}
                      mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
                      mapboxSwitch={mapboxSwitch}
                      zipCodeFirst
                    />
                  </FormSection>
                </div>
              )}
            </>
          ) : (
            <GenericSkeleton />
          )}
          {!orderHasPickUp && isUsSite && (
            <FormSection name="smsSignUp">
              <SMSFormFields
                labels={smsSignUpLabels}
                colSize={{ small: 6, medium: 8, large: 12 }}
                showDefaultCheckbox={false}
                formName={formName}
                formSection="smsSignUp"
                variation="secondary"
                isOrderUpdateChecked={isOrderUpdateChecked}
                dispatch={dispatch}
                borderBottom={!isEditing}
                addressPhoneNo={addressPhoneNo}
                smsOrderUpdatesRichText={smsOrderUpdatesRichText}
              />
            </FormSection>
          )}
          {this.renderEmailSignUp()}
          {this.renderShippingMethodErrors()}
          <Row fullBleed>
            <Col colSize={{ small: 6, medium: 8, large: 6 }}>
              <FormSection name="shipmentMethods">
                <div className="shipment-methods-form">
                  <ShipmentMethods
                    shipmentMethods={shipmentMethods}
                    formName={formName}
                    formSection="shipmentMethods"
                    selectedShipmentId={selectedShipmentId}
                    shipmentHeader={getLabelValue(
                      labels,
                      'lbl_shipping_shipmentHeader',
                      'shipping',
                      'checkout'
                    )}
                    isLoadingShippingMethods={isLoadingShippingMethods}
                    checkoutRoutingDone={checkoutRoutingDone}
                    isEDD={isEDD}
                    isEddABTest={isEddABTest}
                  />
                </div>
              </FormSection>
            </Col>
            {isGiftOptionsEnabled && this.renderGiftServices()}
          </Row>
          <CheckoutOrderInfo
            showAccordian={showAccordian}
            isGuest={isGuest}
            pageCategory={pageCategory}
            isCheckoutFlow
          />
          <CheckoutFooter
            hideBackLink={!!orderHasPickUp}
            backLinkHandler={routeToPickupPage}
            nextButtonText={nextButtonText}
            backLinkText={getLabelValue(
              labels,
              'lbl_shipping_backLinkText',
              'shipping',
              'checkout'
            )}
            isLoadingShippingMethods={isLoadingShippingMethods}
            bagLoading={bagLoading}
            pageName={pageName}
            appliedExpiredCoupon={appliedExpiredCoupon}
            disableNext={this.shouldDisableCheckoutCta()}
            submitForm={formName}
          />
        </form>
      </div>
    );
  }
}

ShippingForm.propTypes = propTypes;

ShippingForm.defaultProps = defaultProps;

const validateMethod = createValidateMethod({
  address: AddressFields.addressValidationConfig,
  smsSignUp: SMSFormFields.smsFormFieldsConfig,
  emailSignUp: getStandardConfig(['sendEmailSignup']),
});

export default reduxForm({
  form: formName, // a unique identifier for this form
  ...validateMethod,
  shouldValidate: () => true,
  onSubmitFail: (errors) => scrollToFirstError(errors),
})(withStyles(ShippingForm, styles));
export { ShippingForm as ShippingFormVanilla };
