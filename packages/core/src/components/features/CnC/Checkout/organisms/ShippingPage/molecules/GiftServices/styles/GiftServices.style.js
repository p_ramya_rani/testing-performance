// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  @media ${(props) => props.theme.mediaQuery.mediumMax} {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .brand-image {
    width: 65px;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      padding-top: 11px;
      width: 50px;
      height: 30px;
    }
  }

  .shortDesc {
    font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    padding-bottom: 0px;
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .longDescSection {
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .checkbox-header {
    width: 180px;
    display: flex;
  }
  .TextBox__input {
    border: 1px solid ${(props) => props.theme.colors.FOOTER.DIVIDER};
    height: 146px;
    padding: 0;
    width: 99%;
  }
  .addReceipt {
    padding-top: 10px;
    margin-left: 35px;
  }
  .giftServicesContainer {
    p {
      width: 100%;
    }
    margin-left: 35px;
  }
  .messageTextWrapper {
    margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.MED};
    padding-bottom: 6px;
  }
  .priceDropdown {
    margin-left: 120px;
  }
  .giftServicesField {
    display: inline-block;
    width: 142px;
    .CheckBox__text {
      padding-top: 6px;
    }
  }
  .dropdownliBottomBorder {
    cursor: pointer;
    margin-left: 0;
    margin-right: 0;
    padding: 0;
    padding-top: 2px;
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    border-bottom: 1px solid ${(props) => props.theme.colors.PRIMARY.DARK};
    > div {
      height: 100%;
    }
  }
  .dropdownUlBorder li:last-child {
    .dropdownliBottomBorder {
      border: 0;
    }
  }
  .price {
    float: right;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
  .longDesc {
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  }
  .giftServicesDetailsLink {
    text-decoration: underline;
    cursor: pointer;
    padding-top: 6px;
  }
  .phone-field-wrapper {
    width: auto;
    margin-left: -12px;
    padding-right: 0;
    display: flex;
    align-self: center;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  }
  .tcp-radio-button {
    .radio-button {
      top: 18px;
    }
    .radio-button-checked {
      top: 18px;
    }
  }
  .snj-radio-button,
  .gym-radio-button {
    .radio-button {
      top: 18px;
    }
    .radio-button-checked {
      top: 18px;
    }
  }
  .gym-radio-button {
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .normal-select-box {
    padding-top: 5px;
  }
  .custom-select {
    width: 100%;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    > span {
      display: none;
    }
  }
  .giftServicesModal {
    @media ${(props) => props.theme.mediaQuery.large} {
      height: 90%;
    }
  }
  .customSelectTitle {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
    .customSelectArrow {
      top: 52%;
      right: 0;
    }
    .price {
      margin-right: 13px;
    }
  }
  .dropdownDivOverFlow {
    overflow-y: visible;
    div {
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    }
    .dropdownUlBorder {
      width: auto;
      border: 1px solid ${(props) => props.theme.colors.PRIMARY.DARK};
    }
    .dropdownActiveClass {
      background-color: ${(props) => props.theme.colors.PRIMARY.DARK};
      div {
        color: ${(props) => props.theme.colors.WHITE};
        font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      }
      span {
        font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      }
      .dropdownActiveIcon {
        display: none;
      }
    }
  }
  .gymImage {
    @media ${(props) => props.theme.mediaQuery.small} {
      margin-top: 8px;
    }
  }
  .gift-message {
    width: 100%;
    height: 146px;
    resize: none;
    font-size: ${(props) => props.theme.fonts.fontSize.textbox}px;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    overflow-y: hidden;
    box-sizing: border-box;
  }
  .CheckBox__input {
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
`;

export const modalStyles = css`
  div.TCPModal__InnerContent {
    @media ${(props) => props.theme.mediaQuery.large} {
      max-width: 810px;
    }
  }
  .alignRight.alignRight {
    right: 25px;
  }

  .alignTop.alignTop {
    top: 38px;
  }
`;
