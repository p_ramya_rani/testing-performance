// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { isIOS } from '@tcp/core/src/utils/utils.app';
import Modal from '../../../../../../../../common/molecules/Modal';
import { RichText } from '../../../../../../../../common/atoms';
import {
  RichTextWrapper,
  ContentHeight,
  SkeletonWrapper,
} from '../styles/GiftServicesDetailsModal.style.native';
import GenericSkeleton from '../../../../../../../../common/molecules/GenericSkeleton/GenericSkeleton.view.native';

const isIOSDevice = isIOS();
class GiftServicesDetailsModal extends React.PureComponent {
  render() {
    const { openState, onRequestClose, labels, brand } = this.props;
    const { DETAILS_RICH_TEXT, DETAILS_RICH_TEXT_GYM } = labels;
    const modalContent = brand === 'TCP' ? DETAILS_RICH_TEXT : DETAILS_RICH_TEXT_GYM;
    const androidProps = isIOSDevice
      ? {}
      : {
          headerStyle: { head: { 'background-color': 'transparent' } },
          heading: ' ',
        };
    return (
      <Modal
        isOpen={openState}
        onRequestClose={onRequestClose}
        closeIconDataLocator="details-cross-icon"
        fontSize="fs16"
        horizontalBar={false}
        {...androidProps}
      >
        {!modalContent ? (
          <SkeletonWrapper>
            <GenericSkeleton />
          </SkeletonWrapper>
        ) : (
          <RichTextWrapper>
            <RichText
              source={{ html: modalContent }}
              dataLocator="bonus-points-details"
              scrollEnabled
              style={ContentHeight}
            />
          </RichTextWrapper>
        )}
      </Modal>
    );
  }
}
GiftServicesDetailsModal.propTypes = {
  openState: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  brand: PropTypes.string.isRequired,
};
export default GiftServicesDetailsModal;
