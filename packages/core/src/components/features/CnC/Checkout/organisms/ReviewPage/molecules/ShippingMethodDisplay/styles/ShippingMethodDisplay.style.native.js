// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const ShippingMethodHeading = styled.Text`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XL};
`;

const ShippingMethodName = styled.Text`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const EddWrapper = styled.View`
  width: 100%;
  flex-direction: row;
`;

export default { ShippingMethodHeading, ShippingMethodName, EddWrapper };
