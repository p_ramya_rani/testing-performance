/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import {
  getIsAfterPayEnabled,
  getAfterPayMinOrderAmount,
  getAfterPayMaxOrderAmount,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getGrandTotal,
  getGiftCardsTotal,
} from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import { getCardListState } from '../../../../../account/Payment/container/Payment.selectors';
import BillingPaymentForm from '../views';
import CreditCardSelector from './CreditCard.selectors';
import constants from './CreditCard.constants';
import CheckoutSelectors, {
  isUsSite,
  getIsApplePayEnabled,
} from '../../../container/Checkout.selector';
import BagPageSelectors from '../../../../BagPage/container/BagPage.selectors';
import CheckoutActions, {
  setVenmoPaymentInProgress,
  setApplePaymentInProgress,
  setFromDataIfNotEqual,
  setSaveCardChoice,
  setAfterPayInProgress,
} from '../../../container/Checkout.action';
import { toastMessageInfo } from '../../../../../../common/atoms/Toast/container/Toast.actions';
import { getSiteId, isMobileApp } from '../../../../../../../utils';

/**
 * @class GiftCardsContainer
 * @extends {Component}
 * @description container component to render signed in user form.
 */
export class GiftCardsContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.initialValues = null;
  }

  /**
   * @function getCreditCardDefault
   * @description returns the default credit card of the user
   */
  getCreditCardDefault = (cardList) => {
    let defaultCard;
    if (cardList && cardList.size > 0) {
      defaultCard = cardList.filter(
        (card) =>
          card.ccType !== constants.ACCEPTED_CREDIT_CARDS.GIFT_CARD &&
          card.ccType !== constants.ACCEPTED_CREDIT_CARDS.VENMO &&
          !card.isAfterpay &&
          card.defaultInd
      );
      if (defaultCard.size === 0) {
        defaultCard = cardList.filter(
          (card) =>
            card.ccType !== constants.ACCEPTED_CREDIT_CARDS.GIFT_CARD &&
            card.ccType !== constants.ACCEPTED_CREDIT_CARDS.VENMO &&
            !card.isAfterpay
        );
      }
    }

    return defaultCard && defaultCard.size > 1 ? defaultCard.slice(0, 1) : defaultCard;
  };

  /**
   * @function getOnFileAddressId
   * @description returns the on file address id
   */
  getOnFileAddressId = ({ billingOnFileAddressId, shippingOnFileAddressId }) => {
    const { userAddresses } = this.props;
    if (billingOnFileAddressId) {
      const selectedAddress =
        userAddresses &&
        userAddresses.size > 0 &&
        userAddresses.find((add) => add.addressId === billingOnFileAddressId);
      if (selectedAddress) return billingOnFileAddressId;
      return '';
    }
    return shippingOnFileAddressId;
  };

  /**
   * @function showAddressPrefilled
   * @description checks whether the address form needs to be prefilled or not
   */
  showAddressPrefilled = () => {
    const { userAddresses, billingData } = this.props;
    if (this.isBillingIfoPresent()) {
      const {
        address: { onFileAddressId },
      } = billingData;
      /* istanbul ignore else */
      if (userAddresses && userAddresses.size > 0) {
        const selectedAddress = userAddresses.find((add) => add.addressId === onFileAddressId);
        if (!selectedAddress) return true;
      } else return true;
    }
    return false;
  };

  /**
   * @function isBillingIfoPresent
   * @description checks whether cart item has billing info present or not
   */
  isBillingIfoPresent = () => {
    const { billingData } = this.props;
    return billingData.address && !isEmpty(billingData);
  };

  /**
   * @function getSelectedPaymentMethod
   * @description returns the initial payment method selected during billing page load.
   */
  getSelectedPaymentMethod = () => {
    const { billingData } = this.props;
    return billingData.paymentMethod === constants.ACCEPTED_CREDIT_CARDS.PAYPAL
      ? constants.PAYMENT_METHOD_PAY_PAL
      : constants.PAYMENT_METHOD_CREDIT_CARD;
  };

  /**
   * @function getPaymentMethodId
   * @description returns the initial payment method selected during billing page load.
   */
  getPaymentMethodId = () => {
    const { isVenmoPaymentInProgress, afterPayInProgress, billingData } = this.props;
    if (
      afterPayInProgress ||
      (billingData && billingData.billing && billingData.billing.isAfterpay)
    ) {
      return constants.PAYMENT_METHOD_AFTERPAY;
    }
    return isVenmoPaymentInProgress
      ? constants.PAYMENT_METHOD_VENMO
      : this.getSelectedPaymentMethod();
  };

  setBillingInitialValues = () => {
    const { billingData } = this.props;
    const cardType = billingData && billingData.billing && billingData.billing.cardType;
    return this.isBillingIfoPresent() && cardType !== constants.ACCEPTED_CREDIT_CARDS.PAYPAL;
  };

  getAddressValues = () => {
    return { country: getSiteId() && getSiteId().toUpperCase() };
  };

  getOnFileCardKey = ({ onFileCardId, cardList, paymentId }) => {
    if (!onFileCardId && paymentId) {
      return onFileCardId;
    }
    if (onFileCardId) {
      return onFileCardId;
    }
    return cardList.size > 0 && cardList.get(0) && cardList.get(0).creditCardId;
  };

  /**
   * @function getInitialValues
   * @description returns the initial values for the billing form
   */
  getInitialValues = (cardList) => {
    const {
      billingData,
      orderHasShipping,
      shippingOnFileAddressKey,
      shippingOnFileAddressId,
      isSaveCardDefaultChecked,
    } = this.props;
    let billingOnFileAddressKey;
    let cardNumber;
    let cvvCode;
    let expMonth;
    let expYear;
    let billingOnFileAddressId;
    let cardType;
    let onFileCardId;
    let firstName;
    let lastName;
    let addressLine1;
    let addressLine2;
    let city;
    let state;
    let zipCode;
    let country;
    let paymentId;
    let address = this.getAddressValues();
    if (this.setBillingInitialValues()) {
      ({
        address: {
          onFileAddressKey: billingOnFileAddressKey,
          onFileAddressId: billingOnFileAddressId,
          firstName,
          lastName,
          addressLine1,
          addressLine2,
          city,
          state,
          zipCode,
          country,
        },
        billing: { cardNumber, cvvCode, expMonth, expYear, cardType },
        onFileCardId,
        paymentId,
      } = billingData);
    }
    if (this.showAddressPrefilled()) {
      address = {
        firstName,
        lastName,
        addressLine1,
        addressLine2,
        city,
        state,
        zipCode,
        country,
      };
    }
    if (!cardList) {
      return {
        onFileCardKey: 0,
        paymentMethodId: this.getPaymentMethodId(),
        saveToAccount: isSaveCardDefaultChecked,
        sameAsShipping:
          orderHasShipping &&
          (isEmpty(billingData) || billingOnFileAddressKey === shippingOnFileAddressKey),
        cardNumber: this.getCCValues(cardNumber, billingData),
        cvvCode,
        expMonth: this.getCCValues(expMonth, billingData),
        expYear: this.getCCValues(expYear, billingData),
        cardType,
        onFileAddressId: this.getOnFileAddressId({
          billingOnFileAddressId,
          shippingOnFileAddressId,
        }),
        address,
      };
    }

    return {
      onFileCardKey: this.getOnFileCardKey({ onFileCardId, cardList, paymentId }),
      paymentMethodId: this.getPaymentMethodId(),
      saveToAccount: isSaveCardDefaultChecked,
      sameAsShipping: orderHasShipping && billingOnFileAddressKey === shippingOnFileAddressKey,
      cardNumber: this.getCCValues(cardNumber, billingData),
      cvvCode,
      expMonth: this.getCCValues(expMonth, billingData),
      expYear: this.getCCValues(expYear, billingData),
      cardType,
      onFileAddressId: this.getOnFileAddressId({
        billingOnFileAddressId,
        shippingOnFileAddressId,
      }),
      address,
    };
  };

  /**
   * @function getCCValues
   * @description returns the values of fields in credit card inital phase
   */
  getCCValues = (value, billingData) => {
    if (billingData && billingData.billing && billingData.billing.isAfterpay) {
      return '';
    }
    return billingData &&
      billingData.billing &&
      billingData.billing.cardType === constants.ACCEPTED_CREDIT_CARDS.APPLE
      ? ''
      : value;
  };

  /**
   * @function getSelectedCard
   * @description returns the selected card from the card list
   */
  getSelectedCard = (cardList, paymentId) => {
    return cardList && cardList.find((card) => card.creditCardId === paymentId);
  };

  getAddressLine2 = (addressLine2) => {
    return addressLine2 || '';
  };

  getNickName = (selectedAddress) =>
    selectedAddress && selectedAddress.get(0) && selectedAddress.get(0).nickName;

  getAppleStatus = (setAppleProgress, data) => {
    const { paymentMethodId } = data;
    if (paymentMethodId !== constants.PAYMENT_METHOD_APPLE) {
      setAppleProgress(false);
    }
  };

  setAfterPayStatus = (data) => {
    const { paymentMethodId } = data;
    const { setAfterPayProgress = () => {} } = this.props;
    if (paymentMethodId === constants.PAYMENT_METHOD_AFTERPAY) {
      setAfterPayProgress(true);
    } else {
      setAfterPayProgress(false);
    }
  };

  /**
   * @function submitBillingData
   * @description submits the billing data
   */
  submitBillingData = (data) => {
    const {
      cardList,
      handleSubmit,
      userAddresses,
      navigation,
      setVenmoProgress,
      setAppleProgress,
      setSaveCardChoiceAction,
    } = this.props;
    let onFileAddressKey;
    let addressLine1;
    let addressLine2;
    let city;
    let country;
    let firstName;
    let lastName;
    let state;
    let zipCode;
    let cardDetails = this.getSelectedCard(cardList, data.onFileCardKey);
    /* istanbul ignore else */
    if (!cardDetails) {
      /* istanbul ignore else */
      if (data.address) {
        ({ addressLine1, addressLine2, city, country, firstName, lastName, state, zipCode } =
          data.address);
      }
      cardDetails = {
        cardNumber: data.cardNumber,
        ccBrand: data.cardType,
        cvv: data.cvvCode,
        emailAddress: undefined,
        expMonth: data.expMonth,
        expYear: data.expYear,
        addressDetails: {
          addressLine1,
          addressLine2: this.getAddressLine2(addressLine2),
          city,
          country,
          firstName,
          lastName,
          state,
          zipCode,
        },
      };
    }
    /* istanbul ignore else */
    if (data.onFileAddressId) {
      const selectedAddress =
        userAddresses &&
        userAddresses.size > 0 &&
        userAddresses.filter((address) => address.addressId === data.onFileAddressId);
      onFileAddressKey = this.getNickName(selectedAddress);
    }
    const isCardTypeRequired = cardDetails.ccBrand !== constants.ACCEPTED_CREDIT_CARDS.PLACE_CARD;
    const { paymentMethodId, saveToAccount } = data;
    if (paymentMethodId !== constants.PAYMENT_METHOD_VENMO) {
      setVenmoProgress(false);
    }
    this.setAfterPayStatus(data);
    this.getAppleStatus(setAppleProgress, data);
    setSaveCardChoiceAction(saveToAccount);
    handleSubmit({
      address: {
        ...cardDetails.addressDetails,
        onFileAddressId: data.onFileAddressId,
        onFileAddressKey,
        sameAsShipping: data.sameAsShipping,
      },
      cardNumber: cardDetails.cardNumber || cardDetails.accountNo,
      cardType: cardDetails.ccBrand,
      cvv: data.cvvCode,
      emailAddress: undefined,
      expMonth: cardDetails.expMonth,
      expYear: cardDetails.expYear,
      isCVVRequired: isCardTypeRequired,
      isExpirationRequired: isCardTypeRequired,
      paymentId: cardDetails.creditCardId,
      paymentMethod: data.paymentMethodId,
      phoneNumber: cardDetails.addressDetails && cardDetails.addressDetails.phone1,
      saveToAccount,
      isDefault: data.defaultPayment || cardDetails.defaultInd,
      navigation,
      onFileCardId: data.onFileCardKey,
    });
  };

  /**
   * @function render
   * @description render method to be called of component
   */
  render() {
    const {
      cardList,
      labels,
      onFileCardKey,
      paymentMethodId,
      cvvCodeRichText,
      orderHasShipping,
      isGuest,
      backLinkPickup,
      backLinkShipping,
      nextSubmitText,
      formErrorMessage,
      isPaymentDisabled,
      shippingAddress,
      addressLabels,
      billingData,
      isSameAsShippingChecked,
      syncErrorsObj,
      cardType,
      isSaveToAccountChecked,
      userAddresses,
      selectedOnFileAddressId,
      isEditFormSameAsShippingChecked,
      editFormSelectedOnFileAddressId,
      navigation,
      creditFieldLabels,
      updateCardDetail,
      isPayPalEnabled,
      isVenmoEnabled,
      isApplePayEnabled,
      editFormCardType,
      defaultEditCardType,
      isPLCCEnabled,
      scrollView,
      toastMessage,
      setCheckoutStage,
      pageCategory,
      getPayPalSettings,
      isPayPalWebViewEnable,
      bagLoading,
      isVenmoAppInstalled,
      selectedCountry,
      isBonusPointsEnabled,
      isUSSite,
      keyBoardAvoidRef,
      dispatchFieldChangeIfNotThere,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      isApplePayEnabledOnBilling,
      isECOM,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      isAfterPayEnabled,
      afterPayInProgress,
      afterPayMinOrderAmount,
      afterPayMaxOrderAmount,
      orderTotal,
      trackClickAfterPay,
      cartHaveGCItem,
    } = this.props;

    this.initialValues = this.getInitialValues(this.getCreditCardDefault(cardList));

    return (
      <BillingPaymentForm
        cardList={cardList}
        labels={labels}
        onFileCardKey={onFileCardKey}
        initialValues={this.initialValues}
        paymentMethodId={paymentMethodId}
        cvvCodeRichText={cvvCodeRichText}
        onSubmit={this.submitBillingData}
        orderHasShipping={orderHasShipping}
        isGuest={isGuest}
        backLinkPickup={backLinkPickup}
        backLinkShipping={backLinkShipping}
        nextSubmitText={nextSubmitText}
        formErrorMessage={formErrorMessage}
        isPaymentDisabled={isPaymentDisabled}
        cardType={cardType}
        editFormCardType={editFormCardType}
        defaultEditCardType={defaultEditCardType}
        syncErrorsObj={syncErrorsObj}
        addressLabels={addressLabels}
        shippingAddress={shippingAddress}
        isSameAsShippingChecked={isSameAsShippingChecked}
        billingData={billingData}
        isSaveToAccountChecked={isSaveToAccountChecked}
        userAddresses={userAddresses}
        selectedOnFileAddressId={selectedOnFileAddressId}
        editFormSelectedOnFileAddressId={editFormSelectedOnFileAddressId}
        navigation={navigation}
        creditFieldLabels={creditFieldLabels}
        updateCardDetail={updateCardDetail}
        isEditFormSameAsShippingChecked={isEditFormSameAsShippingChecked}
        isPayPalEnabled={isPayPalEnabled}
        isVenmoEnabled={isVenmoEnabled}
        isApplePayEnabled={isApplePayEnabled}
        isECOM={isECOM}
        isApplePayEnabledOnBilling={isApplePayEnabledOnBilling}
        isPLCCEnabled={isPLCCEnabled}
        scrollView={scrollView}
        toastMessage={toastMessage}
        setCheckoutStage={setCheckoutStage}
        pageCategory={pageCategory}
        getPayPalSettings={getPayPalSettings}
        isPayPalWebViewEnable={isPayPalWebViewEnable}
        bagLoading={bagLoading}
        isVenmoAppInstalled={isVenmoAppInstalled}
        selectedCountry={selectedCountry}
        isBonusPointsEnabled={isBonusPointsEnabled}
        isUSSite={isUSSite}
        keyBoardAvoidRef={keyBoardAvoidRef}
        defaultSaveToAccount={this.initialValues.saveToAccount}
        dispatchFieldChangeIfNotThere={dispatchFieldChangeIfNotThere}
        zipCodeABTestEnabled={zipCodeABTestEnabled}
        getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
        zipCodeLoader={zipCodeLoader}
        mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
        mapboxSwitch={mapboxSwitch}
        isAfterPayEnabled={isAfterPayEnabled}
        afterPayInProgress={afterPayInProgress}
        afterPayMinOrderAmount={afterPayMinOrderAmount}
        afterPayMaxOrderAmount={afterPayMaxOrderAmount}
        orderTotal={orderTotal}
        trackClickAfterPay={trackClickAfterPay}
        cartHaveGCItem={cartHaveGCItem}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    cardList: getCardListState(state, true),
    onFileCardKey: CreditCardSelector.getOnFileCardKey(state, ownProps),
    paymentMethodId: CreditCardSelector.getPaymentMethodId(state, ownProps),
    formErrorMessage: CreditCardSelector.getFormValidationErrorMessages(state),
    isPaymentDisabled: CheckoutSelectors.getIsPaymentDisabled(state),
    syncErrorsObj: CreditCardSelector.getSyncError(state),
    cardType: CreditCardSelector.getCardType(state),
    editFormCardType: CreditCardSelector.getEditFormCardType(state),
    defaultEditCardType: CreditCardSelector.getCurrentEditCardType(state),
    isSameAsShippingChecked: CreditCardSelector.getSameAsShippingValue(state),
    isEditFormSameAsShippingChecked: CreditCardSelector.getEditFormSameAsShippingValue(state),
    isSaveToAccountChecked: CreditCardSelector.getSaveToAccountValue(state),
    shippingOnFileAddressKey: CreditCardSelector.getShippingOnFileAddressKey(state),
    selectedOnFileAddressId: CreditCardSelector.getSelectedOnFileAddressId(state),
    editFormSelectedOnFileAddressId: CreditCardSelector.getEditFormSelectedOnFileAddressId(state),
    shippingOnFileAddressId: CreditCardSelector.getShippingOnFileAddressId(state),
    isPayPalEnabled: BagPageSelectors.getIsPayPalEnabled(state),
    isPLCCEnabled: CreditCardSelector.getIsPLCCEnabled(state, true),
    isVenmoEnabled: CheckoutSelectors.getIsVenmoEnabled(state), // Venmo Kill Switch, if Venmo enabled then true, else false.
    isApplePayEnabled: getIsApplePayEnabled(state),
    isApplePayEnabledOnBilling: CheckoutSelectors.getIsApplePayEnabledOnBilling(state),
    isECOM: BagPageSelectors.getECOMStatus(state),
    getPayPalSettings: CheckoutSelectors.getPayPalSettings(state),
    isVenmoAppInstalled: CheckoutSelectors.isVenmoAppInstalled(state),
    selectedCountry: CreditCardSelector.getSelectedCountry(state),
    isUSSite: isUsSite(state),
    isSaveCardDefaultChecked: CheckoutSelectors.getIsSaveCardDefaultChecked(state),
    isAfterPayEnabled: getIsAfterPayEnabled(state),
    afterPayMinOrderAmount: getAfterPayMinOrderAmount(state),
    afterPayMaxOrderAmount: getAfterPayMaxOrderAmount(state),
    afterPayInProgress: CheckoutSelectors.getIsAfterPayInProgress(state),
    orderTotal: getGrandTotal(state) - getGiftCardsTotal(state),
    cartHaveGCItem: CheckoutSelectors.getIsCartHaveGiftCards(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCardDetail: (payload) => {
      dispatch(CheckoutActions.updateCardData(payload));
    },
    toastMessage: (palyoad) => {
      dispatch(toastMessageInfo(palyoad));
    },
    dispatchFieldChangeIfNotThere: (payload) => {
      dispatch(setFromDataIfNotEqual(payload));
    },
    setVenmoProgress: (payload) => {
      if (!isMobileApp()) dispatch(setVenmoPaymentInProgress(payload));
    },
    setAppleProgress: (payload) => {
      dispatch(setApplePaymentInProgress(payload));
    },
    setSaveCardChoiceAction: (payload) => {
      dispatch(setSaveCardChoice(payload));
    },
    setAfterPayProgress: (payload) => {
      dispatch(setAfterPayInProgress(payload));
    },
  };
};

GiftCardsContainer.propTypes = {
  userAddresses: PropTypes.shape([]).isRequired,
  billingData: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  isVenmoPaymentInProgress: PropTypes.bool.isRequired,
  orderHasShipping: PropTypes.bool.isRequired,
  shippingOnFileAddressKey: PropTypes.string.isRequired,
  shippingOnFileAddressId: PropTypes.string.isRequired,
  cardList: PropTypes.shape([]).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  setVenmoProgress: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  onFileCardKey: PropTypes.func.isRequired,
  paymentMethodId: PropTypes.string.isRequired,
  cvvCodeRichText: PropTypes.string.isRequired,
  isGuest: PropTypes.bool.isRequired,
  backLinkPickup: PropTypes.bool.isRequired,
  backLinkShipping: PropTypes.bool.isRequired,
  nextSubmitText: PropTypes.string.isRequired,
  formErrorMessage: PropTypes.shape({}).isRequired,
  isPaymentDisabled: PropTypes.bool.isRequired,
  shippingAddress: PropTypes.shape({}).isRequired,
  addressLabels: PropTypes.shape({}).isRequired,
  isSameAsShippingChecked: PropTypes.bool.isRequired,
  syncErrorsObj: PropTypes.shape({}).isRequired,
  cardType: PropTypes.string.isRequired,
  isSaveToAccountChecked: PropTypes.bool.isRequired,
  selectedOnFileAddressId: PropTypes.string.isRequired,
  isEditFormSameAsShippingChecked: PropTypes.bool.isRequired,
  editFormSelectedOnFileAddressId: PropTypes.func.isRequired,
  creditFieldLabels: PropTypes.shape({}).isRequired,
  updateCardDetail: PropTypes.func.isRequired,
  isPayPalEnabled: PropTypes.bool.isRequired,
  isVenmoEnabled: PropTypes.bool.isRequired,
  editFormCardType: PropTypes.string.isRequired,
  defaultEditCardType: PropTypes.string.isRequired,
  isPLCCEnabled: PropTypes.bool.isRequired,
  scrollView: PropTypes.bool.isRequired,
  toastMessage: PropTypes.string.isRequired,
  setCheckoutStage: PropTypes.func.isRequired,
  pageCategory: PropTypes.string.isRequired,
  getPayPalSettings: PropTypes.func.isRequired,
  isPayPalWebViewEnable: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isVenmoAppInstalled: PropTypes.bool.isRequired,
  selectedCountry: PropTypes.string.isRequired,
  isBonusPointsEnabled: PropTypes.bool.isRequired,
  isUSSite: PropTypes.bool.isRequired,
  keyBoardAvoidRef: PropTypes.shape({}).isRequired,
  dispatchFieldChangeIfNotThere: PropTypes.func.isRequired,
  zipCodeABTestEnabled: PropTypes.bool.isRequired,
  getZipCodeSuggestedAddress: PropTypes.func.isRequired,
  zipCodeLoader: PropTypes.bool.isRequired,
  setAppleProgress: PropTypes.func.isRequired,
  isApplePayEnabled: PropTypes.bool.isRequired,
  isECOM: PropTypes.bool.isRequired,
  isApplePayEnabledOnBilling: PropTypes.bool.isRequired,
  mapboxAutocompleteTypesParam: PropTypes.string.isRequired,
  mapboxSwitch: PropTypes.bool.isRequired,
  isSaveCardDefaultChecked: PropTypes.bool.isRequired,
  setSaveCardChoiceAction: PropTypes.func.isRequired,
  isAfterPayEnabled: PropTypes.bool.isRequired,
  afterPayInProgress: PropTypes.bool.isRequired,
  afterPayMinOrderAmount: PropTypes.string.isRequired,
  afterPayMaxOrderAmount: PropTypes.string.isRequired,
  orderTotal: PropTypes.string.isRequired,
  trackClickAfterPay: PropTypes.func.isRequired,
  setAfterPayProgress: PropTypes.func.isRequired,
  cartHaveGCItem: PropTypes.bool,
};

GiftCardsContainer.defaultProps = {
  cartHaveGCItem: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(GiftCardsContainer);
