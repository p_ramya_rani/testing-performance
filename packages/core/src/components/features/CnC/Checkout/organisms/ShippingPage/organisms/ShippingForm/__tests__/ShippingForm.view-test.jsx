// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { List } from 'immutable';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { ShippingFormVanilla } from '../views/ShippingForm.view';

const mockedSetEditShippingFlag = jest.fn();
describe('Shipping Form', () => {
  it('should render correctly', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      isGuest: false,
      ServerErrors: {},
      getEmailSignUpLabels: {},
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with submit errors', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      isGuest: false,
      ServerErrors: {},
      getEmailSignUpLabels: {},
      submitErrors: { address: { formGeneric: 'some error' } },
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree.find(Notification)).toHaveLength(1);
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with logged in user', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      emailSignUpLabels: {},
      isGuest: true,
      orderHasPickUp: false,
      isUsSite: false,
      getEmailSignUpLabels: {},
      setEditShippingFlag: mockedSetEditShippingFlag,
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with logged in user having addresses', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      emailSignUpLabels: {},
      isGuest: true,
      orderHasPickUp: false,
      isUsSite: false,
      userAddresses: new List([
        {
          addressId: '123',
          firstName: 'test',
          lastName: 'test',
          addressLine: ['addressline 1 hhhhh', 'addressline 2 mmmmmmmm'],
          city: 'test city',
          country: 'test country hhhhh',
          phone1: '1234567890',
          primary: 'true',
        },
      ]),
      dispatch: jest.fn(),
      onFileAddressKey: '123',
      shipmentMethods: [{}],
      defaultShipmentId: '90113',
      isMobile: true,
      getEmailSignUpLabels: {},
      setEditShippingFlag: mockedSetEditShippingFlag,
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    tree.setState({
      modalState: true,
      modalType: 'edit',
    });
    tree.setProps({ shipmentMethods: [{ id: 123 }] });
    tree.instance().isAddressModalEmptied = true;
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with logged in user having addresses with isAddressModalEmptied false', () => {
    const props = {
      addressLabels: { addressFormLabels: {} },
      shippingLabels: {},
      smsSignUpLabels: {},
      emailSignUpLabels: {},
      isGuest: true,
      orderHasPickUp: false,
      isUsSite: false,
      userAddresses: new List([
        {
          addressId: '123',
          firstName: 'test',
          lastName: 'test',
          addressLine: ['addressline 1 hhh', 'addressline 2 mmm'],
          city: 'test city',
          country: 'test country hhh',
          phone1: '1234567890',
          primary: 'true',
        },
      ]),
      dispatch: jest.fn(),
      onFileAddressKey: '123',
      shipmentMethods: [{}],
      defaultShipmentId: '90113',
      isSaveToAddressBookChecked: false,
      isAddNewAddress: false,
      defaultAddressId: null,
      getEmailSignUpLabels: {},
      setEditShippingFlag: mockedSetEditShippingFlag,
    };
    const tree = shallow(<ShippingFormVanilla {...props} />);
    tree.setState({
      modalState: false,
      modalType: 'add',
      isEditing: false,
    });
    tree.instance().isAddressModalEmptied = false;
    tree.setProps({
      shipmentMethods: [{ id: 123 }],
      isSaveToAddressBookChecked: true,
      defaultAddressId: '334',
    });
    tree.instance().toggleIsEditing();
    expect(tree.state('isEditing')).toBe(true);

    expect(tree).toMatchSnapshot();
  });

  const props = {
    addressLabels: { addressFormLabels: {} },
    shippingLabels: {},
    smsSignUpLabels: {},
    emailSignUpLabels: {},
    isGuest: true,
    orderHasPickUp: false,
    isUsSite: false,
    userAddresses: new List([
      {
        addressId: '123',
        firstName: 'test',
        lastName: 'test',
        addressLine: ['addressline 1 hhh', 'addressline 2 mmm'],
        city: 'test city',
        country: 'test country hhh',
        phone1: '1234567890',
        primary: 'true',
      },
    ]),
    dispatch: jest.fn(),
    onFileAddressKey: '123',
    shipmentMethods: [{}],
    defaultShipmentId: '90113',
    isSaveToAddressBookChecked: false,
    isAddNewAddress: false,
    defaultAddressId: '1234',
    getEmailSignUpLabels: {},
    setEditShippingFlag: mockedSetEditShippingFlag,
  };
  it('should render correctly with logged in user having addresses with modalType add and state true', () => {
    const tree = shallow(<ShippingFormVanilla {...props} />);
    tree.setState({
      modalState: true,
      modalType: 'add',
      isEditing: false,
    });

    expect(tree).toMatchSnapshot();
  });
  it('should render correctly correctly with bagLoading and checkoutRoutingDone', () => {
    props.bagLoading = false;
    props.checkoutRoutingDone = true;
    props.isGuest = false;
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly correctly with bagLoading and checkoutRoutingDone', () => {
    props.bagLoading = false;
    props.checkoutRoutingDone = true;
    props.isGuest = true;
    props.isVenmoShippingDisplayed = false;
    props.isVenmoPaymentInProgress = true;
    const tree = shallow(<ShippingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should call toggleIsEditing when editShippingAddress prop is true', () => {
    props.editShippingAddress = true;
    const component = shallow(<ShippingFormVanilla {...props} />);
    const componentInstance = component.instance();
    const spyToggleIsEditing = jest.spyOn(componentInstance, 'toggleIsEditing');
    componentInstance.componentDidMount();
    expect(spyToggleIsEditing).toHaveBeenCalled();
  });

  it('should call setEditShippingFlag from toggleIsEditing', () => {
    props.editShippingAddress = true;
    const component = shallow(<ShippingFormVanilla {...props} />);
    const componentInstance = component.instance();
    componentInstance.toggleIsEditing();
    expect(mockedSetEditShippingFlag).toHaveBeenCalled();
  });

  it('should not call toggleIsEditing when editShippingAddress prop is false', () => {
    props.editShippingAddress = false;
    const component = shallow(<ShippingFormVanilla {...props} />);
    const componentInstance = component.instance();
    const spyToggleIsEditing = jest.spyOn(componentInstance, 'toggleIsEditing');
    componentInstance.componentDidMount();
    expect(spyToggleIsEditing).toHaveBeenCalledTimes(0);
  });
});
