// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { View } from 'react-native';
import { string, bool, func, shape } from 'prop-types';
import ModalNative from '@tcp/core/src/components/common/molecules/Modal/view/Modal.native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { isAndroid } from '@tcp/core/src/utils';
import { getProductDetails } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { Button, DamImage } from '@tcp/core/src/components/common/atoms';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import CONSTANTS from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import {
  SaveLaterWrapper,
  SaveLaterButtonWrapper,
  ItemListWrapper,
  ItemWrapper,
  ItemImageWrapper,
  ItemTextWrapper,
} from '../styles/SaveForLaterModal.style.native';

const { SFS_ERROR_TYPES } = CONSTANTS;

class SaveForLaterModal extends React.PureComponent {
  displayItemInfo = (name, imagePath) => {
    return (
      <ItemWrapper>
        <ItemImageWrapper>
          <DamImage isProductImage alt={name} url={imagePath} height={60} width={60} />
        </ItemImageWrapper>
        <ItemTextWrapper>
          <BodyCopyWithSpacing
            textAlign="left"
            fontFamily="secondary"
            fontSize="fs16"
            text={name}
          />
        </ItemTextWrapper>
      </ItemWrapper>
    );
  };

  getSelectAction = () => {
    const {
      toggleSaveLaterModal,
      giftServicesAction,
      updateShippingMethodAction,
      type,
    } = this.props;

    let action =
      type === SFS_ERROR_TYPES.SHIPPING_ADDRESS ? toggleSaveLaterModal : updateShippingMethodAction;

    if (type === SFS_ERROR_TYPES.GIFT_SERVICES) {
      action = giftServicesAction;
    }
    return action;
  };

  render() {
    const {
      toggleSaveLaterModal,
      saveForLaterModalState,
      type,
      saveForLaterAction,
      cartOrderItems,
      itemIds,
      labels,
      heading,
      body,
    } = this.props;
    let ctaText =
      type === SFS_ERROR_TYPES.SHIPPING_ADDRESS
        ? getLabelValue(labels, 'lbl_sfs_address_cta', 'shipping', 'checkout')
        : getLabelValue(labels, 'lbl_sfs_shipping_cta', 'shipping', 'checkout');

    if (type === SFS_ERROR_TYPES.GIFT_SERVICES) {
      ctaText = getLabelValue(labels, 'lbl_sfs_giftservice_cta', 'shipping', 'checkout');
    }
    const selectAction = this.getSelectAction();
    return (
      <View>
        <ModalNative
          showOnlyCloseIcon
          onRequestClose={toggleSaveLaterModal}
          isOpen={saveForLaterModalState}
          stickyCloseIcon={!isAndroid()}
        >
          <SaveLaterWrapper>
            <BodyCopyWithSpacing
              textAlign="left"
              mobileFontFamily="secondary"
              fontSize="fs22"
              fontWeight="semibold"
              text={heading}
              spacingStyles="margin-right-XXS"
            />
            <BodyCopyWithSpacing
              textAlign="left"
              mobileFontFamily="secondary"
              fontSize="fs16"
              text={body}
              spacingStyles="margin-right-XXS margin-top-XS margin-bottom-SM"
            />
            <SaveLaterButtonWrapper>
              <Button
                fill="BLUE"
                color="white"
                onPress={selectAction}
                data-locator="orders-shop-now-btn"
                text={ctaText}
                width="340px"
              />
            </SaveLaterButtonWrapper>
            <SaveLaterButtonWrapper>
              <Button
                buttonVariation="fixed-width"
                onPress={saveForLaterAction}
                data-locator="orders-shop-now-btn"
                text={getLabelValue(labels, 'lbl_sfs_sfl_cta', 'shipping', 'checkout')}
                width="340px"
              />
            </SaveLaterButtonWrapper>
          </SaveLaterWrapper>
          <ItemListWrapper>
            {cartOrderItems &&
              cartOrderItems.size &&
              cartOrderItems.map(item => {
                const {
                  itemInfo: { name = '', imagePath, itemId },
                } = getProductDetails(item);
                return itemIds.indexOf(itemId) !== -1
                  ? this.displayItemInfo(name, imagePath)
                  : null;
              })}
          </ItemListWrapper>
        </ModalNative>
      </View>
    );
  }
}

SaveForLaterModal.propTypes = {
  toggleSaveLaterModal: string.isRequired,
  type: string.isRequired,
  heading: string.isRequired,
  body: string.isRequired,
  saveForLaterAction: func.isRequired,
  updateShippingMethodAction: func.isRequired,
  cartOrderItems: shape([]).isRequired,
  giftServicesAction: func.isRequired,
  itemIds: shape([]).isRequired,
  labels: shape({}).isRequired,
  saveForLaterModalState: bool.isRequired,
};

export default SaveForLaterModal;
