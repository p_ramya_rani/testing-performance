// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .payment-container {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXL};
    border-top: 1px solid ${props => props.theme.colors.TEXT.DARKGRAY};
  }
  .errorWrapper {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    font-family: ${props => props.theme.typography.fonts.secondary};
    font-size: ${props => props.theme.typography.fontSizes.fs12};
    color: ${props => props.theme.colorPalette.red[500]};
    font-weight: ${props => props.theme.typography.fontWeights.extrabold};
  }
  & .labelStyle {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }

  & .hide-show {
    right: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }

  & .show-hide-icons {
    top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }

  .afterPay_threshold {
    border: 2px solid ${props => props.theme.colorPalette.orange[800]};
    display: flex;
    flex-direction: row;
    padding: ${props => props.theme.spacing.ELEM_SPACING.SM};
    margin: 0 ${props => props.theme.spacing.ELEM_SPACING.SM};
  }
`;

export default styles;
