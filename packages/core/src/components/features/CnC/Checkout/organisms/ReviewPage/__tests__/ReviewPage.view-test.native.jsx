// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ReviewPageVanilla } from '../views/ReviewPage.view.native';

describe('ReviewPageVanilla component', () => {
  const defaultProps = {
    addressLabels: { addAddressHeading: 'Update Address' },
    formatPayload: jest.fn(),
  };
  it('should renders correctly props not present', () => {
    const props = {
      ...defaultProps,
      labels: {},
      handleSubmit: jest.fn(),
      reviewDidMount: () => {},
      ServerErrors: {},
    };
    const component = shallow(<ReviewPageVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  const props1 = {
    ...defaultProps,
    className: '',
    labels: {},
    submitReview: jest.fn(),
    handleSubmit: jest.fn(),
    reviewDidMount: () => {},
    isExpressCheckout: true,
    pickUpContactPerson: {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      emailAddress: '',
    },
    pickUpContactAlternate: {},
    orderHasPickUp: true,
    orderHasShipping: true,
    navigation: {},
  };
  const data = {
    pickUpAlternateExpress: {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      emailAddress: '',
      hasAlternatePickup: true,
    },
    pickUpAlternate: {
      hasAlternatePickup: true,
    },
    smsSignUp: {
      sendOrderUpdate: '',
    },
  };
  it('calling reviewFormSubmit method', () => {
    const tree = shallow(<ReviewPageVanilla {...props1} />);
    const componentInstance = tree.instance();
    jest.spyOn(componentInstance, 'reviewFormSubmit');
    componentInstance.reviewFormSubmit(data);
    expect(componentInstance.reviewFormSubmit).toHaveBeenCalled();
  });

  it('should render correctly without express checkout', () => {
    const props = {
      ...defaultProps,
      reviewDidMount: () => {},
      className: '',
      labels: {},
      submitReview: jest.fn(),
      handleSubmit: jest.fn(),
      isExpressCheckout: false,
      pickUpContactPerson: {
        firstName: '',
        lastName: '',
        phoneNumber: '',
        emailAddress: '',
      },
      pickUpContactAlternate: {},
    };
    const tree = shallow(<ReviewPageVanilla {...props} />);
    const componentInstance = tree.instance();
    jest.spyOn(componentInstance, 'reviewFormSubmit');
    componentInstance.reviewFormSubmit(data);
    expect(componentInstance.reviewFormSubmit).toHaveBeenCalled();
    expect(componentInstance).toMatchSnapshot();
  });
  it('should render correctly with express checkout', () => {
    const props = {
      ...defaultProps,
      reviewDidMount: () => {},
      className: '',
      labels: {},
      submitReview: jest.fn(),
      handleSubmit: jest.fn(),
      isExpressCheckout: true,
      pickUpContactPerson: {
        firstName: '',
        lastName: '',
        phoneNumber: '',
        emailAddress: '',
      },
      pickUpContactAlternate: {},
    };
    const tree = shallow(<ReviewPageVanilla {...props} />);
    const componentInstance = tree.instance();
    jest.spyOn(componentInstance, 'reviewFormSubmit');
    componentInstance.reviewFormSubmit(data);
    expect(componentInstance.reviewFormSubmit).toHaveBeenCalled();
    expect(componentInstance).toMatchSnapshot();
  });
  it('should render correctly with express checkout and SFS errors', () => {
    const props = {
      ...defaultProps,
      reviewDidMount: () => {},
      className: '',
      submitReview: jest.fn(),
      handleSubmit: jest.fn(),
      isExpressCheckout: true,
      pickUpContactPerson: {
        firstName: '',
        lastName: '',
        phoneNumber: '',
        emailAddress: '',
      },
      pickUpContactAlternate: {},
      checkoutServerError: {},
      clearCheckoutServerError: () => {},
      shipmentMethods: [
        {
          id: '901101',
          displayName: 'Standard - FREE',
          shippingSpeed: 'Up To 10 Business Days',
          price: 0,
          isDefault: false,
          code: 'UGNR',
        },
        {
          id: '901102',
          displayName: 'Express',
          shippingSpeed: 'Up To 5 Business Days',
          price: 15,
          isDefault: false,
          code: 'U2DY',
        },
        {
          id: '901103',
          displayName: 'Rush',
          shippingSpeed: 'Up To 3 Business Days',
          price: 20,
          isDefault: false,
          code: 'U1DS',
        },
        {
          id: '901107',
          displayName: 'NO RUSH Delivers in 3 weeks or less',
          shippingSpeed: '',
          price: 0,
          isDefault: true,
          code: 'HEXP',
        },
      ],
      formName: 'expressReviewPage',
      formSection: 'expressReviewShippingSection',
      bagLoading: false,
      cartOrderItems: [
        {
          productInfo: {
            imagePath: '2110252/2110252_IV.jpg',
            size: '10',
            generalProductId: '1118210',
            color: {
              name: 'TIDAL',
            },
            variantNo: '2110252024',
            upc: '00191755292700',
            orderType: 'ECOM',
            isGiftCard: false,
            name: 'Girls Uniform Ponte Knit Pull On Jeggings',
            productPartNumber: '2110252_IV',
            itemPartNumber: '00191755292700',
            fit: 'regular',
            pdpUrl: '/us/p/Girls-Uniform-Ponte-Knit-Pull-On-Jeggings-2110252-IV',
            itemBrand: 'TCP',
            colorFitSizeDisplayNames: {},
            skuId: '1119558',
          },
          itemInfo: {
            quantity: 1,
            itemId: '1244515033',
            itemPoints: null,
            listPrice: 9.98,
            offerPrice: 9.98,
            wasPrice: 24.95,
            salePrice: 9.98,
          },
          miscInfo: {
            store: null,
            vendorColorDisplayId: '2110252_IV',
            bossStartDate: null,
            badge: {},
            isOnlineOnly: false,
            isInventoryAvailBOSS: true,
            storeTomorrowOpenRange: null,
            isBossEligible: true,
            clearanceItem: false,
            storeItemsCount: 0,
            storeTodayOpenRange: null,
            storeAddress: null,
            storePhoneNumber: null,
            isBopisEligible: true,
            availability: 'OK',
            orderItemType: 'ECOM',
            isStoreBOSSEligible: false,
            storeId: null,
            bossEndDate: null,
          },
        },
        {
          productInfo: {
            imagePath: '3001569/3001569_160.jpg',
            size: 'M (7/8)',
            generalProductId: '1234001',
            color: {
              name: 'RENEW BLUE',
            },
            variantNo: '3001569009',
            upc: '00193511087827',
            orderType: 'ECOM',
            isGiftCard: false,
            name: 'Girls Uniform Ruffle Pique Polo',
            productPartNumber: '3001569_160',
            itemPartNumber: '00193511087827',
            pdpUrl: '/us/p/Girls-Uniform-Short-Sleeve-Ruffle-Pique-Polo-3001569-160',
            itemBrand: 'TCP',
            colorFitSizeDisplayNames: {},
            skuId: '1234480',
          },
          itemInfo: {
            quantity: 1,
            itemId: '1244515031',
            itemPoints: null,
            listPrice: 5.18,
            offerPrice: 5.18,
            wasPrice: 12.95,
            salePrice: 5.18,
          },
          miscInfo: {
            store: null,
            vendorColorDisplayId: '3001569_160',
            bossStartDate: null,
            badge: {},
            isOnlineOnly: false,
            isInventoryAvailBOSS: true,
            storeTomorrowOpenRange: null,
            isBossEligible: true,
            clearanceItem: false,
            storeItemsCount: 0,
            storeTodayOpenRange: null,
            storeAddress: null,
            storePhoneNumber: null,
            isBopisEligible: true,
            availability: 'OK',
            orderItemType: 'ECOM',
            isStoreBOSSEligible: false,
            storeId: null,
            bossEndDate: null,
          },
        },
        {
          productInfo: {
            imagePath: '3000935/3000935_627.jpg',
            size: 'L (10/12)',
            generalProductId: '1281593',
            color: {
              name: 'DAYBREAK',
            },
            variantNo: '3000935028',
            upc: '00193511074537',
            orderType: 'ECOM',
            isGiftCard: false,
            name: 'Girls Uniform Soft Jersey Polo',
            productPartNumber: '3000935_627',
            itemPartNumber: '00193511074537',
            fit: 'regular',
            pdpUrl: '/us/p/Girls-Uniform-Short-Sleeve-Soft-Jersey-Polo-3000935-627',
            itemBrand: 'TCP',
            colorFitSizeDisplayNames: {},
            skuId: '1281810',
          },
          itemInfo: {
            quantity: 1,
            itemId: '1244515029',
            itemPoints: null,
            listPrice: 5.18,
            offerPrice: 5.18,
            wasPrice: 12.95,
            salePrice: 5.18,
          },
          miscInfo: {
            store: null,
            vendorColorDisplayId: '3000935_627',
            bossStartDate: null,
            badge: {
              defaultBadge: 'ONLINE EXCLUSIVE',
            },
            isOnlineOnly: true,
            isInventoryAvailBOSS: true,
            storeTomorrowOpenRange: null,
            isBossEligible: true,
            clearanceItem: false,
            storeItemsCount: 0,
            storeTodayOpenRange: null,
            storeAddress: null,
            storePhoneNumber: null,
            isBopisEligible: true,
            availability: 'OK',
            orderItemType: 'ECOM',
            isStoreBOSSEligible: false,
            storeId: null,
            bossEndDate: null,
          },
        },
      ],
      globalLabels: {
        checkout: {
          shipping: {
            lbl_sfs_shipping_address_error: 'Important message for your shipping address.',
            lbl_sfs_cart_item_error: 'Important message for your cart',
            lbl_sfs_order_error: 'Important message for your order.',
            lbl_sfs_error_rush_shipping:
              'X item(s) in your cart are not eligible for Express or RUSH shipping.',
            lbl_sfs_error_gift_service: 'X item(s) in your cart not eligible for gift services.',
            lbl_sfs_error_shipping_address:
              'X item(s) in your cart not eligible.  APO/FPO address entered is not eligible.',
            lbl_sfs_error_address_rush:
              'X Item(s) in your cart are not eligible for shipment to APO/FPO address and Expedited/RUSH Shipping.',
            lbl_sfs_error_gift_address:
              'X item(s) in your cart not eligible for gift services and the APO/FPO address entered is not eligible.',
            lbl_sfs_error_gift_rush:
              'X Item(s) in your cart are not eligible for Rush/Expedited Shipping and Gift Services',
            lbl_sfs_sfl_cta: 'Save Items For Later & Continue',
            lbl_sfs_address_cta: 'Change Address',
            lbl_sfs_shipping_cta: 'Select Standard Shipping',
            lbl_sfs_giftservice_cta: 'Update Gift Services',
          },
        },
      },
      shippingAddress: {
        emailAddress: 'dev.device15@gmail.com',
        method: {
          shippingMethodId: '901101',
        },
        address: {
          firstName: 'Deepa',
          lastName: 'Krishnan',
          addressLine1: '1 Main St',
          addressLine2: '',
          city: 'San Jose',
          state: 'CA',
          zipCode: '95131',
          country: 'US',
        },
        onFileAddressKey: 'sb_2020-09-04 04:08:04.555',
        onFileAddressId: '91964881',
        phoneNumber: '8777522387',
        emailSignup: false,
      },
      shippingMethod: {
        id: '901101',
        displayName: 'Standard - FREE',
        shippingSpeed: 'Up To 10 Business Days',
        price: 0,
        isDefault: false,
        code: 'UGNR',
      },
      expressReviewShippingSectionId: {},
      labels: {
        lbl_review_shippingSectionTitle: 'Shipping',
        lbl_review_sectionAnchor: 'Edit',
        lbl_review_sectionShippingHeading: 'lbl_review_sectionShippingHeading',
        lbl_review_sectionShippingAddressTitle: 'Shipping Address',
        lbl_review_sectionShippingMethodTitle: 'Shipping Method',
        lbl_review_sectionShippingGiftServiceTitle: 'Gift Services',
        lbl_review_sectionShippingGiftServiceDefault: 'N/A',
      },
      isGiftOptionsEnabled: true,
      giftWrappingDisplayName: 'Gift Receipt / Gift Message Only',
      shippingMethodCode: 'UGNR',
      isEDD: false,
      checkoutRoutingDone: false,
    };
    const tree = shallow(<ReviewPageVanilla {...props} />);
    const componentInstance = tree.instance();
    jest.spyOn(componentInstance, 'reviewFormSubmit');
    componentInstance.reviewFormSubmit(data);
    expect(componentInstance.reviewFormSubmit).toHaveBeenCalled();
    expect(componentInstance).toMatchSnapshot();
  });

  it('should test close verification method functionality', () => {
    const tree = shallow(<ReviewPageVanilla {...props1} />);
    const componentInstance = tree.instance();
    componentInstance.closeAddAddressVerificationModal();
    expect(tree.state('showAddressVerification')).toBe(false);
  });

  it('should test close verification method functionality for Apple Pay', () => {
    const tree = shallow(<ReviewPageVanilla {...props1} />);
    const componentInstance = tree.instance();
    componentInstance.closeAddAddressVerificationModal(true);
    expect(props1.submitReview).toHaveBeenCalledWith({ navigation: props1.navigation });
  });
});
