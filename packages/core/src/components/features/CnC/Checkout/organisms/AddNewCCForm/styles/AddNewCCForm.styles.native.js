// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const SaveToAccWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

const DefaultPaymentWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

export { SaveToAccWrapper, DefaultPaymentWrapper };
