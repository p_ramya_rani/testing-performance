// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  align-items: flex-start;
  display: flex;
  border: 2px solid #ffab00;
  .notification__image {
    height: 24px;
  }
  .heading-container {
    flex: 1;
  }
  .right-arrow {
    border: solid #1a1a1a;
    border-width: 0 1px 1px 0;
    padding: 6px;
    transform: rotate(-45deg);
    margin-left: 30px;
    align-self: center;
    color: transparent;
    font-size: 0;
    cursor: pointer;
    outline: 0;
  }
`;

export default styles;
