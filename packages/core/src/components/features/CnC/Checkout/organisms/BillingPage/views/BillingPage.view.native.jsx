// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ScrollView } from 'react-native';
import { getScreenHeight, isIOS } from '@tcp/core/src/utils';
import PropTypes from 'prop-types';
import CheckoutSectionTitleDisplay from '../../../../../../common/molecules/CheckoutSectionTitleDisplay';
import CheckoutProgressIndicator from '../../../molecules/CheckoutProgressIndicator';
import GiftCardsContainer from '../../GiftCardsSection';
import style, { BillingPageContainer, PaymentWrapper } from '../styles/BillingPage.style.native';
import GuestBillingForm from '../../GuestBillingForm';
import BillingPaymentForm from '../../BillingPaymentForm';

const { Container } = style;

/**
 * @class BillingPage
 * @extends {PureComponent}
 * @description view component to render billing page .
 */
class BillingPage extends React.PureComponent {
  static propTypes = {
    addressLabels: PropTypes.shape({}).isRequired,
    shippingLabels: PropTypes.shape({}).isRequired,
    smsSignUpLabels: PropTypes.shape({}).isRequired,
    address: PropTypes.shape({}),
    emailSignUpLabels: PropTypes.shape({}).isRequired,
    navigation: PropTypes.shape({}).isRequired,
    submitBilling: PropTypes.func.isRequired,
    billingDidMount: PropTypes.func.isRequired,
    orderHasShipping: PropTypes.bool.isRequired,
    availableStages: PropTypes.shape([]).isRequired,
    checkoutProgressBarLabels: PropTypes.shape([]).isRequired,
    labels: PropTypes.shape({}).isRequired,
    isGuest: PropTypes.bool,
    shippingAddress: PropTypes.shape({}),
    cvvCodeRichText: PropTypes.shape({}),
    billingData: PropTypes.shape({}),
    userAddresses: PropTypes.shape({}),
    creditFieldLabels: PropTypes.shape({}),
    setCheckoutStage: PropTypes.func.isRequired,
    isVenmoPaymentInProgress: PropTypes.bool,
    isApplePayInProgress: PropTypes.bool,
    isVenmoEnabled: PropTypes.bool,
    isApplePayEnabled: PropTypes.bool.isRequired,
    isECOM: PropTypes.bool.isRequired,
    isApplePayEnabledOnBilling: PropTypes.bool.isRequired,
    isPayPalWebViewEnable: PropTypes.bool,
    isFetching: PropTypes.bool.isRequired,
    onVenmoError: PropTypes.shape({}),
    keyBoardAvoidRef: PropTypes.shape({}),
    isBonusPointsEnabled: PropTypes.bool.isRequired,
    scrollToTop: PropTypes.func.isRequired,
    zipCodeABTestEnabled: PropTypes.bool,
    getZipCodeSuggestedAddress: PropTypes.func,
    zipCodeLoader: PropTypes.bool,
    mapboxAutocompleteTypesParam: PropTypes.string,
    mapboxSwitch: PropTypes.bool,
    trackClickAfterPay: PropTypes.func,
  };

  static defaultProps = {
    address: null,
    isGuest: true,
    shippingAddress: null,
    cvvCodeRichText: null,
    billingData: null,
    userAddresses: null,
    creditFieldLabels: {},
    keyBoardAvoidRef: {},
    isVenmoPaymentInProgress: false,
    isApplePayInProgress: false,
    isVenmoEnabled: false,
    isPayPalWebViewEnable: false,
    onVenmoError: {},
    zipCodeABTestEnabled: false,
    getZipCodeSuggestedAddress: () => {},
    zipCodeLoader: false,
    mapboxAutocompleteTypesParam: '',
    mapboxSwitch: true,
    trackClickAfterPay: () => {},
  };

  componentDidMount() {
    const { billingDidMount, scrollToTop } = this.props;
    billingDidMount(true);
    setTimeout(() => {
      scrollToTop();
    }, 500);
  }

  /**
   * @function render
   * @description render method to be called of component
   */
  render() {
    const {
      navigation,
      availableStages,
      labels,
      submitBilling,
      orderHasShipping,
      isGuest,
      shippingAddress,
      cvvCodeRichText,
      addressLabels,
      billingData,
      userAddresses,
      creditFieldLabels,
      setCheckoutStage,
      isVenmoPaymentInProgress,
      isApplePayInProgress,
      isVenmoEnabled, // Venmo Kill Switch, if Venmo enabled then true, else false.
      isApplePayEnabled,
      isApplePayEnabledOnBilling,
      isECOM,
      isPayPalWebViewEnable,
      isFetching,
      isBonusPointsEnabled,
      onVenmoError,
      keyBoardAvoidRef,
      checkoutProgressBarLabels,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      trackClickAfterPay,
    } = this.props;

    const { header, backLinkShipping, backLinkPickup, nextSubmitText } = labels;
    // Below Style is Only for handling PayPal FullScreen View
    const iOS = isIOS();
    const screenHeight = getScreenHeight();
    const scrollStyle = {
      position: 'absolute',
      zIndex: 992,
      height: iOS ? screenHeight - 40 : screenHeight,
      width: '100%',
    };
    const defualtScrollStyle = { flexGrow: 1 };
    return (
      <BillingPageContainer isPayPalWebViewEnable={isPayPalWebViewEnable}>
        {!isPayPalWebViewEnable && (
          <CheckoutProgressIndicator
            activeStage="billing"
            navigation={navigation}
            availableStages={availableStages}
            setCheckoutStage={setCheckoutStage}
            checkoutProgressBarLabels={checkoutProgressBarLabels}
          />
        )}
        <ScrollView
          style={isPayPalWebViewEnable ? scrollStyle : defualtScrollStyle}
          ref={(scrollView) => {
            this.scrollView = scrollView;
          }}
          scrollEnabled={!isPayPalWebViewEnable}
          contentContainerStyle={isPayPalWebViewEnable ? scrollStyle : defualtScrollStyle}
          keyboardShouldPersistTaps="handled"
        >
          <Container isPayPalWebViewEnable={isPayPalWebViewEnable}>
            <PaymentWrapper>
              <CheckoutSectionTitleDisplay title={header} />
              <GiftCardsContainer isFetching={isFetching} />
            </PaymentWrapper>
            {isGuest ? (
              <GuestBillingForm
                shippingAddress={shippingAddress}
                cvvCodeRichText={cvvCodeRichText}
                labels={labels}
                isGuest={isGuest}
                addressLabels={addressLabels}
                backLinkPickup={backLinkPickup}
                backLinkShipping={backLinkShipping}
                nextSubmitText={nextSubmitText}
                orderHasShipping={orderHasShipping}
                billingData={billingData}
                navigation={navigation}
                btnText={nextSubmitText}
                creditFieldLabels={creditFieldLabels}
                setCheckoutStage={setCheckoutStage}
                isVenmoPaymentInProgress={isVenmoPaymentInProgress}
                isApplePayInProgress={isApplePayInProgress}
                isVenmoEnabled={isVenmoEnabled}
                isApplePayEnabled={isApplePayEnabled}
                isBonusPointsEnabled={isBonusPointsEnabled}
                isPayPalWebViewEnable={isPayPalWebViewEnable}
                keyBoardAvoidRef={keyBoardAvoidRef}
                onVenmoError={onVenmoError}
                zipCodeABTestEnabled={zipCodeABTestEnabled}
                getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                zipCodeLoader={zipCodeLoader}
                mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
                mapboxSwitch={mapboxSwitch}
                trackClickAfterPay={trackClickAfterPay}
              />
            ) : (
              <BillingPaymentForm
                handleSubmit={submitBilling}
                orderHasShipping={orderHasShipping}
                isGuest={isGuest}
                backLinkPickup={backLinkPickup}
                backLinkShipping={backLinkShipping}
                nextSubmitText={nextSubmitText}
                cvvCodeRichText={cvvCodeRichText}
                labels={labels}
                billingData={billingData}
                addressLabels={addressLabels}
                shippingAddress={shippingAddress}
                userAddresses={userAddresses}
                navigation={navigation}
                creditFieldLabels={creditFieldLabels}
                scrollView={this.scrollView}
                setCheckoutStage={setCheckoutStage}
                isVenmoPaymentInProgress={isVenmoPaymentInProgress}
                isApplePayInProgress={isApplePayInProgress}
                isVenmoEnabled={isVenmoEnabled}
                isApplePayEnabled={isApplePayEnabled}
                isECOM={isECOM}
                isApplePayEnabledOnBilling={isApplePayEnabledOnBilling}
                isBonusPointsEnabled={isBonusPointsEnabled}
                isPayPalWebViewEnable={isPayPalWebViewEnable}
                keyBoardAvoidRef={keyBoardAvoidRef}
                onVenmoError={onVenmoError}
                zipCodeABTestEnabled={zipCodeABTestEnabled}
                getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                zipCodeLoader={zipCodeLoader}
                mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
                mapboxSwitch={mapboxSwitch}
                trackClickAfterPay={trackClickAfterPay}
              />
            )}
          </Container>
        </ScrollView>
      </BillingPageContainer>
    );
  }
}

export default BillingPage;

export { BillingPage as BillingPageVanilla };
