// 9fbef606107a605d69c0edbcd8029e5d 
import { getShippingSubmitErrors } from '../ShippingPage.selectors';

describe('Shipping Page Selectors', () => {
  it('#getShippingSubmitErrors should return submit errors state', () => {
    const submitErrors = {
      address: {
        formGeneric: 'some error',
      },
    };
    const state = {
      form: {
        checkoutShipping: {
          submitErrors: {
            ...submitErrors,
          },
        },
      },
    };

    expect(getShippingSubmitErrors(state).address.formGeneric).toEqual('some error');
  });
});
