// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import SaveForLaterModal from '../views/SaveForLaterModal.view.native';

describe('Save for Later SFS Modal component', () => {
  const props = {
    type: 'shipping_address',
    toggleSaveLaterModal: jest.fn(),
    saveForLaterAction: jest.fn(),
    giftServicesAction: jest.fn(),
    saveForLaterModalState: true,
    heading: 'Important message for your shipping address.',
    body: '1 item(s) in your cart not eligible.  APO/FPO address entered is not eligible.',
    cartOrderItems: [
      {
        productInfo: {
          imagePath: '2045100/2045100_NN.jpg',
          size: 'L (10/12)',
          generalProductId: '586668',
          color: {
            name: 'NEW NAVY',
          },
          variantNo: '2045100009',
          upc: '00889705459463',
          orderType: 'ECOM',
          isGiftCard: false,
          name: 'Boys Uniform Zip Up Hoodie',
          productPartNumber: '2045100_NN',
          itemPartNumber: '00889705459463',
          pdpUrl: '/us/p/Boys-Uniform-Long-Sleeve-Zip-Up-Hoodie-2045100-NN',
          itemBrand: 'TCP',
          colorFitSizeDisplayNames: {},
          skuId: '871949',
        },
        itemInfo: {
          quantity: 1,
          itemId: '1244511191',
          itemPoints: null,
          listPrice: 11.98,
          offerPrice: 11.98,
          wasPrice: 29.95,
          salePrice: 11.98,
        },
        miscInfo: {
          store: null,
          vendorColorDisplayId: '2045100_NN',
          bossStartDate: null,
          badge: {},
          isOnlineOnly: false,
          isInventoryAvailBOSS: true,
          storeTomorrowOpenRange: null,
          isBossEligible: true,
          clearanceItem: false,
          storeItemsCount: 0,
          storeTodayOpenRange: null,
          storeAddress: null,
          storePhoneNumber: null,
          isBopisEligible: true,
          availability: 'OK',
          orderItemType: 'ECOM',
          isStoreBOSSEligible: false,
          storeId: null,
          bossEndDate: null,
        },
      },
    ],
    itemIds: ['1244511191'],
    catEntryId: ['871949'],
    labels: {
      checkout: {},
    },
  };

  it('renders correctly without props', () => {
    const component = shallow(<SaveForLaterModal {...props} />);
    expect(component).toMatchSnapshot();
  });
});
