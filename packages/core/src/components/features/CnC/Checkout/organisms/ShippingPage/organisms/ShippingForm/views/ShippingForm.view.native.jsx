// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ScrollView } from 'react-native';
import { FormSection, reduxForm, change } from 'redux-form';
import { withTheme } from 'styled-components/native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { scrollToFirstErrorApp } from '@tcp/core/src/components/features/CnC/Checkout/util/utility';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import AddressFields from '../../../../../../../../common/molecules/AddressFields';
import ShipmentMethods from '../../../../../../common/molecules/ShipmentMethods';
import SMSFormFields from '../../../../../../../../common/molecules/SMSFormFields';
import createValidateMethod from '../../../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../../../utils/formValidation/validatorStandardConfig';
import {
  ShippingFormWrapper,
  ShippingLoaderWrapper,
  NotificationWrapper,
} from '../styles/ShippingForm.view.style.native';
import GiftServices from '../../../molecules/GiftServices';
import CnCTemplate from '../../../../../../common/organism/CnCTemplate';
import RegisteredShippingFormView from '../../RegisteredShippingForm/views/RegisteredShippingForm.view.native';
import CONSTANTS from '../../../../../Checkout.constants';
import PickupPageSkeleton from '../../../../PickupPage/views/PickupPageSkeleton.native';
import {
  propTypes,
  defaultProps,
  nextCTAText,
  clearServerError,
  showSaveForLaterModal,
  renderShippingErrors,
  renderShippingMethodErrors,
  shouldDisableCheckoutCta,
} from './ShippingForm.view.utils.native';

const refObj = {};
let scrollViewRef = false;

const scrollToFirstErrorShippingForm = (errors) => {
  scrollToFirstErrorApp(errors, refObj, scrollViewRef);
};

const renderVerifyAddressErrors = (submitErrors) => {
  const errorMessage = submitErrors && submitErrors.address && submitErrors.address.formGeneric;
  if (errorMessage) {
    scrollToFirstErrorShippingForm({
      address: {
        addressLine1: '',
      },
    });
  }
  return (
    errorMessage && (
      <NotificationWrapper>
        <Notification
          status="error"
          scrollIntoView
          fontWeight="bold"
          message={errorMessage}
          isErrorTriangleIcon
        />
      </NotificationWrapper>
    )
  );
};

const createRefParent = (node, refName, keyBoardAvoidRef) => {
  if (node) {
    refObj[refName] = node;
  }
  if (keyBoardAvoidRef) {
    scrollViewRef = keyBoardAvoidRef;
  }
};

class ShippingForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editType: '',
      editShipmentDetailsError: '',
      saveForLaterModalState: false,
      sfsErrorType: CONSTANTS.SFS_ERROR_TYPES.SHIPPING_ADDRESS,
    };
  }

  setEditType = (val) => this.setState({ editType: val });

  setEditShipmentDetailsError = (val) => this.setState({ editShipmentDetailsError: val });

  setEditState = (state, type) => {
    const { setEditShippingFlag } = this.props;
    setEditShippingFlag(state);
    if (!state) {
      this.setEditShipmentDetailsError('');
    }
    this.setEditType(type);
  };

  handleShippingFormSubmit = (e) => {
    const { handleSubmit, submitShippingForm } = this.props;
    this.setEditShipmentDetailsError('');
    return handleSubmit(submitShippingForm)(e);
  };

  afterSflSuccessCallback = () => {
    const { cartOrderItems, navigation } = this.props;
    if (!cartOrderItems.size) {
      navigation.navigate('BagPage');
    } else {
      this.handleShippingFormSubmit({});
    }
  };

  /**
   * @method deselectGiftService
   * @description - set gift services form values as default and unchecked for SFS item error
   */
  deselectGiftService = () => {
    const { dispatch, setGiftWrapValue } = this.props;
    if (dispatch) {
      dispatch(change('GiftServices', 'hasGiftWrapping', false));
      dispatch(change('GiftServices', 'message', ''));
      dispatch(change('GiftServices', 'optionId', ''));
      clearServerError(this);
    }
    setGiftWrapValue({});
  };

  selectStandardShipping = () => {
    const { dispatch, updateShippingMethodSelection } = this.props;
    dispatch(change('checkoutShipping', 'shipmentMethods.shippingMethodId', '901101'));
    updateShippingMethodSelection({ id: '901101' });
  };

  render() {
    const {
      shipmentMethods,
      selectedShipmentId,
      dispatch,
      isGuest,
      isUsSite,
      orderHasPickUp,
      smsSignUpLabels,
      isOrderUpdateChecked,
      addressLabels: { addressFormLabels },
      addressPhoneNo,
      loadShipmentMethods,
      navigation,
      isGiftServicesChecked,
      labels,
      userAddresses,
      onFileAddressKey,
      isSaveToAddressBookChecked,
      updateShippingAddress,
      addNewShippingAddress,
      address,
      setAsDefaultShipping,
      defaultAddressId,
      syncErrorsObject,
      newUserPhoneNo,
      setCheckoutStage,
      isVenmoPaymentInProgress,
      isVenmoShippingDisplayed,
      bagLoading,
      isBonusPointsEnabled,
      initialValues,
      handleSMSChange,
      isGiftOptionsEnabled,
      shippingAddressId,
      smsOrderUpdatesRichText,
      keyBoardAvoidRef,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      isEDD,
      updateEdd,
      cartOrderItems,
      eddAllowedStatesArr,
      currentOrderId,
      addItemToSflList,
      updateShippingMethodSelection,
      isEditShipping,
      isSfsInvMessagingEnabled,
      resetShippingEddData,
      submitErrors,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
    } = this.props;

    const { editType, editShipmentDetailsError, saveForLaterModalState, sfsErrorType } = this.state;
    const disableCheckoutAction = shouldDisableCheckoutCta(this);
    return (
      <ScrollView keyboardShouldPersistTaps="handled">
        <ShippingFormWrapper>
          {bagLoading ? (
            <ShippingLoaderWrapper>
              <PickupPageSkeleton />
            </ShippingLoaderWrapper>
          ) : (
            <>
              {renderVerifyAddressErrors(submitErrors)}
              {renderShippingErrors(this)}
              {!isGuest && (
                <RegisteredShippingFormView
                  labels={labels}
                  userAddresses={userAddresses}
                  addressFormLabels={addressFormLabels}
                  formName="checkoutShipping"
                  dispatch={dispatch}
                  addressPhoneNo={addressPhoneNo}
                  loadShipmentMethods={loadShipmentMethods}
                  onFileAddressKey={onFileAddressKey}
                  isSaveToAddressBookChecked={isSaveToAddressBookChecked}
                  updateShippingAddress={updateShippingAddress}
                  addNewShippingAddress={addNewShippingAddress}
                  address={address}
                  isGuest={isGuest}
                  setAsDefaultShipping={setAsDefaultShipping}
                  defaultAddressId={defaultAddressId}
                  syncErrorsObject={syncErrorsObject}
                  newUserPhoneNo={newUserPhoneNo}
                  setEditState={this.setEditState}
                  editShipmentDetailsError={editShipmentDetailsError}
                  shippingAddressId={shippingAddressId}
                  setEditModalRef={() => {}}
                  createRefParent={(...params) => createRefParent(...params, keyBoardAvoidRef)}
                  isEditing={editType !== 'add'}
                  zipCodeABTestEnabled={zipCodeABTestEnabled}
                  getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                  zipCodeLoader={zipCodeLoader}
                  resetShippingEddData={resetShippingEddData}
                  isEDD={isEDD}
                  eddAllowedStatesArr={eddAllowedStatesArr}
                  cartOrderItems={cartOrderItems}
                  clearCheckoutServerError={() => clearServerError(this)}
                  isSfsInvMessagingEnabled={isSfsInvMessagingEnabled}
                  addItemToSflList={addItemToSflList}
                  isEditShipping={isEditShipping}
                  updateEdd={updateEdd}
                  currentOrderId={currentOrderId}
                  mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
                  mapboxSwitch={mapboxSwitch}
                />
              )}
              {isGuest && (
                <FormSection name="address">
                  <AddressFields
                    addressFormLabels={addressFormLabels}
                    showDefaultCheckbox={false}
                    formName="checkoutShipping"
                    formSection="address"
                    dispatch={dispatch}
                    addressPhoneNo={addressPhoneNo}
                    loadShipmentMethods={loadShipmentMethods}
                    disableCountry
                    initialValues={initialValues}
                    createRefParent={(...params) => createRefParent(...params, keyBoardAvoidRef)}
                    address={address}
                    zipCodeABTestEnabled={zipCodeABTestEnabled}
                    getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                    zipCodeLoader={zipCodeLoader}
                    updateEdd={updateEdd}
                    cartOrderItems={cartOrderItems}
                    isEDD={isEDD}
                    eddAllowedStatesArr={eddAllowedStatesArr}
                    currentOrderId={currentOrderId}
                    resetShippingEddData={resetShippingEddData}
                    mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
                    mapboxSwitch={mapboxSwitch}
                    zipCodeFirst
                  />
                </FormSection>
              )}
            </>
          )}
          {!orderHasPickUp && isUsSite && (
            <FormSection name="smsSignUp">
              <SMSFormFields
                labels={smsSignUpLabels}
                showDefaultCheckbox={false}
                formName="checkoutShipping"
                formSection="smsSignUp"
                isOrderUpdateChecked={isOrderUpdateChecked}
                dispatch={dispatch}
                addressPhoneNo={addressPhoneNo}
                handleSMSChange={handleSMSChange}
                smsOrderUpdatesRichText={smsOrderUpdatesRichText}
              />
            </FormSection>
          )}
          {renderShippingMethodErrors(this)}
          <FormSection name="shipmentMethods">
            <ShipmentMethods
              shipmentMethods={shipmentMethods}
              formName="checkoutShipping"
              formSection="shipmentMethods"
              shipmentHeader={getLabelValue(
                labels,
                'lbl_shipping_shipmentHeader',
                'shipping',
                'checkout'
              )}
              selectedShipmentId={selectedShipmentId}
              updateShippingMethodSelection={updateShippingMethodSelection}
              isSfsInvMessagingEnabled={isSfsInvMessagingEnabled}
              dispatch={dispatch}
              isEDD={isEDD}
            />
          </FormSection>
          {isGiftOptionsEnabled && (
            <FormSection name="giftServices">
              <GiftServices
                showDefaultCheckbox={false}
                formSection="giftServices"
                variation="secondary"
                isGiftServicesChecked={isGiftServicesChecked}
                dispatch={dispatch}
              />
            </FormSection>
          )}
        </ShippingFormWrapper>
        <CnCTemplate
          navigation={navigation}
          btnText={nextCTAText(labels, isVenmoPaymentInProgress, isVenmoShippingDisplayed)}
          routeToPage=""
          onPress={(e) => {
            if (!disableCheckoutAction) {
              this.handleShippingFormSubmit(e);
            }
          }}
          isGuest={isGuest}
          backLinkText={
            orderHasPickUp &&
            getLabelValue(labels, 'lbl_shipping_backLinkText', 'shipping', 'checkout')
          }
          onBackLinkPress={() => setCheckoutStage(CONSTANTS.PICKUP_DEFAULT_PARAM)}
          pageCategory="shippingPage"
          showAccordian
          bagLoading={bagLoading}
          isBonusPointsEnabled={isBonusPointsEnabled}
          pageName="checkout"
          pageSection="shipping"
          disableAction={disableCheckoutAction}
        />
        {isSfsInvMessagingEnabled &&
          showSaveForLaterModal(this, saveForLaterModalState, sfsErrorType)}
      </ScrollView>
    );
  }
}

ShippingForm.propTypes = propTypes;

ShippingForm.defaultProps = defaultProps;

const validateMethod = createValidateMethod({
  address: AddressFields.addressValidationConfig,
  smsSignUp: SMSFormFields.smsFormFieldsConfig,
  emailSignUp: getStandardConfig(['sendEmailSignup']),
});

export default reduxForm({
  form: 'checkoutShipping',
  ...validateMethod, // a unique identifier for this form
  destroyOnUnmount: false,
  shouldValidate: () => true,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  onSubmitFail: scrollToFirstErrorShippingForm,
})(withTheme(ShippingForm));

export { ShippingForm as ShippingFormVanilla };
