// 9fbef606107a605d69c0edbcd8029e5d
export default {
  FORM_NAME: 'checkoutBillingPayment',
  GUEST_FORM_NAME: 'checkoutBilling',
  EDIT_FORM_NAME: 'checkoutCardEditPayment',
  FETCH_MODULEX_CONTENT: 'FETCH_MODULEX_CONTENT_CREDIT',
  SET_MODULEX_CONTENT: 'SET_MODULEX_CONTENT_CREDIT',
  CREDIT_CARD_CVV_INFO_LABEL: 'cvv_info',
  PAYMENT_METHOD_CREDIT_CARD: 'creditCard',
  ACCEPTED_CREDIT_CARDS: {
    PLACE_CARD: 'PLACE CARD',
    VENMO: 'VENMO',
    GIFT_CARD: 'GiftCard',
    PAYPAL: 'paypal',
    APPLE: 'APPLE PAY',
    ApplePay: 'applepay',
  },
  PAYMENT_METHOD_PAY_PAL: 'payPal',
  PAYMENT_METHOD_VENMO: 'venmo',
  PAYMENT_METHOD_APPLE: 'apple',
  PAYMENT_METHOD_APPLE_PAY: 'applepay',
  PAYMENT_METHOD_AFTERPAY: 'afterpay',
};
