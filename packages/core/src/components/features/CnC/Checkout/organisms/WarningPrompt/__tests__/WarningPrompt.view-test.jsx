// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { WarningPromptViewVanilla } from '../views/WarningPrompt.view';
import SaveForLater from '../../SaveForLaterModal';

describe('ButtonList component', () => {
  const props = {
    className: '',
    itemIds: [],
    saveForLaterAction: () => {},
  };

  it('renders correctly', () => {
    const component = shallow(<WarningPromptViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders saveForLater modal', () => {
    const component = shallow(<WarningPromptViewVanilla {...props} />);
    component.find('.right-arrow').simulate('click');
    expect(component.find(SaveForLater)).toHaveLength(1);
  });
});
