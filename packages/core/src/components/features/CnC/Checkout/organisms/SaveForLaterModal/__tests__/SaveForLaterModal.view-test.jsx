// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { SaveForLaterModalVanilla } from '../views/SaveForLaterModal.view';

describe('ButtonList component', () => {
  const props = {
    openState: true,
    type: 'shippingAddress',
    toggleModal: () => {},
    cartOrderItems: [],
    itemIds: [],
    saveForLaterAction: () => {},
  };

  it('renders correctly without props', () => {
    const component = shallow(<SaveForLaterModalVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
