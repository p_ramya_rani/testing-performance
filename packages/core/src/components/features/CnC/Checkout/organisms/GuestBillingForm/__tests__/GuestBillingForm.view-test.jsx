// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { GuestBillingFormVanilla } from '../views/GuestBillingForm.view';

const afterpayThresholdClass = '.afterPay_threshold';
describe('GuestBillingFormVanilla', () => {
  const initialProps = {
    shippingAddress: null,
    cvvCodeRichText: '',
    cardType: null,
    syncErrorsObj: null,
    labels: { orderTotalThresholdMessage: 'test' },
    paymentMethodId: null,
  };
  it('should render correctly', () => {
    const props = {
      shippingAddress: null,
      cvvCodeRichText: '',
      cardType: null,
      syncErrorsObj: null,
      labels: { orderTotalThresholdMessage: 'test' },
      paymentMethodId: null,
    };
    const tree = shallow(<GuestBillingFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly when props change', () => {
    const mockedDispatch = jest.fn();
    const props = {
      shippingAddress: null,
      cvvCodeRichText: '',
      cardType: null,
      syncErrorsObj: { syncError: { cvvCode: 'error' } },
      labels: { orderTotalThresholdMessage: 'test' },
      paymentMethodId: 'creditCard',
      dispatch: mockedDispatch,
    };
    const tree = shallow(<GuestBillingFormVanilla {...props} />);
    tree.setProps({ cardType: 'VISA' });
    expect(mockedDispatch).toHaveBeenCalled();
    expect(tree).toMatchSnapshot();
  });
  it('calling showVenmoButton returning true', () => {
    initialProps.paymentMethodId = 'venmo';
    initialProps.isPaymentDisabled = false;
    const component = shallow(<GuestBillingFormVanilla {...initialProps} />);
    const instance = component.instance();
    jest.spyOn(instance, 'showVenmoButton');
    expect(instance.showVenmoButton(instance)).toBe(true);
  });
  it('calling showVenmoButton returning false', () => {
    initialProps.paymentMethodId = 'venmo';
    initialProps.isPaymentDisabled = true;
    const component = shallow(<GuestBillingFormVanilla {...initialProps} />);
    const instance = component.instance();
    jest.spyOn(instance, 'showVenmoButton');
    expect(instance.showVenmoButton(instance)).toBe(false);
  });
  it('calling showAppleButton returning true', () => {
    initialProps.paymentMethodId = 'apple';
    initialProps.isPaymentDisabled = true;
    const component = shallow(<GuestBillingFormVanilla {...initialProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should not render afterpay threshold message', () => {
    initialProps.paymentMethodId = 'afterpay';
    initialProps.isPaymentDisabled = false;
    initialProps.afterPayMinOrderAmount = '10';
    initialProps.orderTotal = 40;
    initialProps.afterPayMaxOrderAmount = '1000';
    const component = shallow(<GuestBillingFormVanilla {...initialProps} />);
    expect(component.find(afterpayThresholdClass)).toHaveLength(0);
  });

  it('should render afterpay threshold message', () => {
    initialProps.paymentMethodId = 'afterpay';
    initialProps.isPaymentDisabled = false;
    initialProps.afterPayMinOrderAmount = '10';
    initialProps.orderTotal = 2000;
    initialProps.afterPayMaxOrderAmount = '1000';
    const component = shallow(<GuestBillingFormVanilla {...initialProps} />);
    expect(component.find(afterpayThresholdClass)).toHaveLength(1);
  });

  it('should render afterpay threshold message', () => {
    initialProps.paymentMethodId = 'afterpay';
    initialProps.isPaymentDisabled = false;
    initialProps.afterPayMinOrderAmount = '10';
    initialProps.orderTotal = 2;
    initialProps.afterPayMaxOrderAmount = '1000';
    const component = shallow(<GuestBillingFormVanilla {...initialProps} />);
    expect(component.find(afterpayThresholdClass)).toHaveLength(1);
  });
});
