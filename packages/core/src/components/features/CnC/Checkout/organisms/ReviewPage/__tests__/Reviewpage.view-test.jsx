// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import { ReviewPageVanilla } from '../views/ReviewPage.view';

jest.mock('@tcp/core/src/utils', () => ({
  ...jest.requireActual('@tcp/core/src/utils'),
  isCanada: jest.fn(),
  routerPush: jest.fn(),
}));

describe('ReviewPageVanilla component', () => {
  const defaultProps = {
    checkoutRoutingDone: false,
    labels: {},
    reviewDidMount: () => {},
    handleSubmit: () => {},
  };
  const tree = shallow(<ReviewPageVanilla {...defaultProps} />);
  it('should renders correctly props not present', () => {
    const props = {
      labels: {},
      handleSubmit: jest.fn(),
      reviewDidMount: () => {},
      ServerErrors: {},
      checkoutRoutingDone: true,
    };
    const component = shallow(<ReviewPageVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when not routing done ', () => {
    const props = {
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasPickUp: true,
    };
    const component = shallow(<ReviewPageVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly when order has shipping ', () => {
    const props = {
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasShipping: true,
    };
    const component = shallow(<ReviewPageVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call handleDefaultLinkClick', () => {
    const event = {
      preventDefault: jest.fn(),
    };
    tree.instance().handleDefaultLinkClick(event);
    expect(event.preventDefault).toBeCalled();
  });

  it('should call updateShippingMethodSelection from afterSflSuccessCallback', () => {
    const updateShippingMethodSelection = jest.fn();
    const props = {
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasShipping: true,
      cartOrderItems: fromJS({ orderItems: ['1'] }),
      updateShippingMethodSelection,
    };

    const wrapper = shallow(<ReviewPageVanilla {...props} />);
    wrapper.instance().afterSflSuccessCallback();
    expect(props.updateShippingMethodSelection).toHaveBeenCalled();
  });

  it('should not call updateShippingMethodSelection from afterSflSuccessCallback', () => {
    const updateShippingMethodSelection = jest.fn();
    const props = {
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasShipping: true,
      cartOrderItems: fromJS({}),
      updateShippingMethodSelection,
    };

    const wrapper = shallow(<ReviewPageVanilla {...props} />);
    wrapper.instance().afterSflSuccessCallback();
    expect(props.updateShippingMethodSelection).not.toHaveBeenCalled();
  });
});

describe('ReviewPage componentDidUpdate', () => {
  it('should call persistEddNodeAction', () => {
    const persistEddNodeAction = jest.fn();
    const prevProps = {
      persistEddNodeAction,
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasShipping: true,
      selectedShippingCode: 'UGNR',
      itemsWithEdd: [{ orderItem: '1234567' }],
    };
    const newprops = {
      isEddABTest: true,
      persistEddNodeAction,
      selectedShippingCode: 'UIAD',
      itemsWithEdd: [{ orderItem: '1434567' }],
      isEDD: true,
    };
    const wrapper = shallow(<ReviewPageVanilla {...prevProps} />);
    wrapper.setProps(newprops);
    wrapper.instance().persistEddAction(prevProps);
    expect(wrapper.instance().props.persistEddNodeAction).toHaveBeenCalled();
  });

  it('should not call persistEddNodeAction if no eddData', () => {
    const persistEddNodeAction = jest.fn();
    const prevProps = {
      persistEddNodeAction,
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasShipping: true,
      selectedShippingCode: 'UGNR',
    };
    const newprops = {
      isEddABTest: true,
      persistEddNodeAction,
      selectedShippingCode: 'UIAD',
    };
    const wrapper = shallow(<ReviewPageVanilla {...prevProps} />);
    wrapper.setProps(newprops);
    expect(wrapper.instance().props.persistEddNodeAction).not.toHaveBeenCalled();
  });

  it('should not call persistEddNodeAction if eddABTest is off', () => {
    const persistEddNodeAction = jest.fn();
    const prevProps = {
      persistEddNodeAction,
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasShipping: true,
      selectedShippingCode: 'UGNR',
    };
    const newprops = {
      persistEddNodeAction,
      selectedShippingCode: 'UIAD',
      isEddABTest: false,
    };
    const wrapper = shallow(<ReviewPageVanilla {...prevProps} />);
    wrapper.setProps(newprops);
    expect(wrapper.instance().props.persistEddNodeAction).not.toHaveBeenCalled();
  });

  it('should call persistEddNodeAction if itemsWithEdd is blank', () => {
    const persistEddNodeAction = jest.fn();
    const prevProps = {
      persistEddNodeAction,
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasShipping: true,
      selectedShippingCode: 'UGNR',
    };
    const newprops = {
      isEddABTest: true,
      persistEddNodeAction,
      selectedShippingCode: 'UIAD',
      itemsWithEdd: [],
      isEDD: true,
    };
    const wrapper = shallow(<ReviewPageVanilla {...prevProps} />);
    wrapper.setProps(newprops);
    wrapper.instance().persistEddAction(prevProps);
    expect(wrapper.instance().props.persistEddNodeAction).toHaveBeenCalled();
  });

  it('should call not persistEddNodeAction if EDD is false', () => {
    const persistEddNodeAction = jest.fn();
    const prevProps = {
      persistEddNodeAction,
      checkoutRoutingDone: false,
      labels: {},
      reviewDidMount: () => {},
      handleSubmit: () => {},
      orderHasShipping: true,
      selectedShippingCode: 'UGNR',
      itemsWithEdd: [{ orderItem: '1234567' }],
    };
    const newprops = {
      isEddABTest: true,
      persistEddNodeAction,
      selectedShippingCode: 'UIAD',
      itemsWithEdd: [{ orderItem: '1434567' }],
      isEDD: false,
    };
    const wrapper = shallow(<ReviewPageVanilla {...prevProps} />);
    wrapper.setProps(newprops);
    wrapper.instance().persistEddAction(prevProps);
    expect(wrapper.instance().props.persistEddNodeAction).not.toHaveBeenCalled();
  });
});
