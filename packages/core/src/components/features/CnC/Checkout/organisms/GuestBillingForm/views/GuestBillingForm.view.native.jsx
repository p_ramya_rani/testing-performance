// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { FormSection, reduxForm, change } from 'redux-form';
import GenericSkeleton from '@tcp/core/src/components/common/molecules/GenericSkeleton/GenericSkeleton.view.native';
import ApplePaymentButton from '@tcp/core/src/components/common/atoms/ApplePaymentButton';
import { scrollToFirstErrorApp } from '@tcp/core/src/components/features/CnC/Checkout/util/utility';
import Button from '@tcp/core/src/components/common/atoms/Button';
import Image from '@tcp/core/src/components/common/atoms/Image';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import AddNewCCForm from '../../AddNewCCForm';
import cvvInfo from '../../../molecules/CVVInfo';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import CheckoutBillingAddress from '../../CheckoutBillingAddress';
import CREDIT_CARD_CONSTANTS from '../../BillingPaymentForm/container/CreditCard.constants';
import {
  PaymentMethodHeader,
  PayPalTextContainer,
  ApplePayTextContainer,
  SkeletonWrapper,
  PaymentMethodWrapper,
  AfterPayThresholdMsg,
  AfterPayThresholdMsgTxt,
  AfterPayContainer,
} from '../styles/GuestBillingForm.styles.native';
import CnCTemplate from '../../../../common/organism/CnCTemplate';
import CONSTANTS from '../../../Checkout.constants';
import PaymentMethods from '../../../../common/molecules/PaymentMethods';
import AddressFields from '../../../../../../common/molecules/AddressFields';
import { getExpirationRequiredFlag } from '../../../util/utility';
import {
  getPaymentMethods,
  renderAddressError,
} from '../../BillingPaymentForm/views/BillingPaymentForm.util';

const triangleWarning = require('../../../../../../../../../mobileapp/src/assets/images/triangle-warning.png');

const refObj = {};
let scrollViewRef = false;

const scrollToFirstErrorGuestBilling = (errors) => {
  scrollToFirstErrorApp(errors, refObj, scrollViewRef);
};

const createRefParent = (node, refName, keyBoardAvoidRef) => {
  if (node) {
    refObj[refName] = node;
  }
  if (keyBoardAvoidRef) {
    scrollViewRef = keyBoardAvoidRef;
  }
};

/**
 * @class GuestBillingForm
 * @extends {Component}
 * @description view component to render guest billing form.
 */
class GuestBillingForm extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    shippingAddress: PropTypes.shape({}),
    cvvCodeRichText: PropTypes.string,
    cardType: PropTypes.string,
    syncErrorsObj: PropTypes.shape({}),
    labels: PropTypes.shape({}),
    orderHasShipping: PropTypes.bool,
    isPaymentDisabled: PropTypes.bool.isRequired,
    addressLabels: PropTypes.shape({}).isRequired,
    isGuest: PropTypes.bool,
    isSameAsShippingChecked: PropTypes.bool,
    billingData: PropTypes.shape({}),
    nextSubmitText: PropTypes.string,
    backLinkShipping: PropTypes.string,
    backLinkPickup: PropTypes.string,
    navigation: PropTypes.shape({}).isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    creditFieldLabels: PropTypes.shape({}),
    setCheckoutStage: PropTypes.func.isRequired,
    getPayPalSettings: PropTypes.shape({}),
    paymentMethodId: PropTypes.string,
    isPayPalEnabled: PropTypes.bool,
    isPayPalWebViewEnable: PropTypes.func,
    bagLoading: PropTypes.bool.isRequired,
    isVenmoEnabled: PropTypes.bool,
    isApplePayEnabled: PropTypes.bool.isRequired,
    isApplePayEnabledOnBilling: PropTypes.bool.isRequired,
    onVenmoError: PropTypes.func,
    isBonusPointsEnabled: PropTypes.bool.isRequired,
    keyBoardAvoidRef: PropTypes.shape({}),
    isVenmoAppInstalled: PropTypes.bool,
    zipCodeABTestEnabled: PropTypes.bool,
    getZipCodeSuggestedAddress: PropTypes.func,
    zipCodeLoader: PropTypes.bool,
    mapboxAutocompleteTypesParam: PropTypes.string,
    afterPayMinOrderAmount: PropTypes.number,
    orderTotal: PropTypes.number,
    afterPayMaxOrderAmount: PropTypes.number,
    cartHaveGCItem: PropTypes.bool,
    isAfterPayEnabled: PropTypes.bool,
    trackClickAfterPay: PropTypes.func,
  };

  static defaultProps = {
    shippingAddress: null,
    cvvCodeRichText: '',
    cardType: null,
    syncErrorsObj: null,
    labels: {},
    orderHasShipping: true,
    isGuest: true,
    isSameAsShippingChecked: true,
    billingData: {},
    nextSubmitText: '',
    backLinkShipping: '',
    backLinkPickup: '',
    creditFieldLabels: {},
    getPayPalSettings: {},
    keyBoardAvoidRef: {},
    paymentMethodId: null,
    isPayPalEnabled: false,
    isPayPalWebViewEnable: false,
    isVenmoEnabled: false,
    onVenmoError: () => {},
    isVenmoAppInstalled: true,
    zipCodeABTestEnabled: false,
    getZipCodeSuggestedAddress: () => {},
    zipCodeLoader: false,
    mapboxAutocompleteTypesParam: '',
    afterPayMinOrderAmount: 1,
    orderTotal: 0,
    afterPayMaxOrderAmount: 2000,
    cartHaveGCItem: false,
    isAfterPayEnabled: false,
    trackClickAfterPay: () => {},
  };

  /**
   * @function componentDidUpdate
   * @memberof GuestBillingForm
   * @description method to be called on update of component
   */
  componentDidUpdate(prevProp) {
    const { cardType: prevCardType } = prevProp;
    const { cardType, dispatch } = this.props;
    /* istanbul ignore else */
    if (prevCardType !== cardType) {
      dispatch(change('checkoutBilling', 'cardType', cardType));
    }
  }

  /**
   * @description - renderVenmoText method renders text for venmo
   */
  renderVenmoText = () => {
    const { paymentMethodId, labels } = this.props;
    if (paymentMethodId === CONSTANTS.PAYMENT_METHOD_VENMO) {
      return (
        <PayPalTextContainer>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            spacingStyles="margin-bottom-MED"
            color="gray.900"
            dataLocator="venmoBillingText"
            text={labels.venmoLongText}
          />
        </PayPalTextContainer>
      );
    }
    return null;
  };

  renderPayPalTextContainer = (isPayPalEnabled, paymentMethodId, labels) => {
    return isPayPalEnabled && paymentMethodId === CREDIT_CARD_CONSTANTS.PAYMENT_METHOD_PAY_PAL ? (
      <PayPalTextContainer>
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs16"
          spacingStyles="margin-bottom-MED"
          color="gray.900"
          dataLocator="paymentMethodLbl"
          text={labels.payPalLongText}
        />
      </PayPalTextContainer>
    ) : null;
  };

  getAfterPaySubmitButton = ({
    paymentMethodId,
    labels,
    cartHaveGCItem,
    onSubmit,
    handleSubmit,
  }) => {
    const { afterPayMinOrderAmount, orderTotal, afterPayMaxOrderAmount = 1000 } = this.props;
    let orderTotalThresholdMessage =
      labels.orderTotalThresholdMessage &&
      labels.orderTotalThresholdMessage.replace('[AfterPayMinOrderAmount]', afterPayMinOrderAmount);
    orderTotalThresholdMessage =
      orderTotalThresholdMessage &&
      orderTotalThresholdMessage.replace('[AfterPayMaxOrderAmount]', afterPayMaxOrderAmount);
    return paymentMethodId === CONSTANTS.PAYMENT_METHOD_AFTERPAY ? (
      <AfterPayContainer>
        {afterPayMinOrderAmount <= orderTotal &&
        afterPayMaxOrderAmount >= orderTotal &&
        !cartHaveGCItem ? (
          <>
            <Button
              type="submit"
              fontSize="fs14"
              fontWeight="extrabold"
              buttonVariation="variable-width"
              fill="BLUE"
              value={CONSTANTS.PAYMENT_METHOD_AFTERPAY}
              fullWidth
              onPress={handleSubmit(onSubmit)}
              text={labels.afterpaySubmit}
            />
            <BodyCopyWithSpacing
              fontFamily="secondary"
              fontSize="fs16"
              fontWeight="regular"
              text={labels.afterPayDefaultMessage}
              spacingStyles="margin-top-MED"
            />
          </>
        ) : (
          <AfterPayThresholdMsg>
            <Image source={triangleWarning} height="25px" width="25px" />
            <AfterPayThresholdMsgTxt>
              <BodyCopyWithSpacing
                spacingStyles="margin-left-SM"
                fontFamily="secondary"
                fontSize="fs16"
                fontWeight="extrabold"
                text={cartHaveGCItem ? labels.cartHasGCMessage : orderTotalThresholdMessage}
              />
            </AfterPayThresholdMsgTxt>
          </AfterPayThresholdMsg>
        )}
      </AfterPayContainer>
    ) : null;
  };

  /**
   * @function render
   * @description render method to be called of component
   */
  // eslint-disable-next-line complexity
  render() {
    const {
      cvvCodeRichText,
      cardType,
      syncErrorsObj,
      labels,
      isGuest,
      orderHasShipping,
      addressLabels,
      dispatch,
      shippingAddress,
      isSameAsShippingChecked,
      billingData,
      backLinkShipping,
      backLinkPickup,
      navigation,
      nextSubmitText,
      handleSubmit,
      onSubmit,
      creditFieldLabels,
      isPaymentDisabled,
      setCheckoutStage,
      paymentMethodId,
      getPayPalSettings,
      isPayPalEnabled,
      isPayPalWebViewEnable,
      bagLoading,
      isVenmoEnabled,
      isBonusPointsEnabled,
      onVenmoError,
      keyBoardAvoidRef,
      isVenmoAppInstalled,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      mapboxAutocompleteTypesParam,
      isApplePayEnabled,
      isApplePayEnabledOnBilling,
      cartHaveGCItem,
      isAfterPayEnabled,
      trackClickAfterPay,
    } = this.props;
    let cvvError;
    if (syncErrorsObj) {
      cvvError = syncErrorsObj.syncError.cvvCode;
    }
    const isExpirationRequired = getExpirationRequiredFlag({ cardType });
    const paymentMethods = getPaymentMethods(
      labels,
      isVenmoEnabled,
      isVenmoAppInstalled,
      isPayPalEnabled,
      isApplePayEnabled,
      isAfterPayEnabled
    );
    return (
      <>
        {!isPayPalWebViewEnable && !isPaymentDisabled && (
          <PaymentMethodWrapper>
            <PaymentMethodHeader>
              <BodyCopy
                mobileFontFamily="primary"
                fontSize="fs26"
                fontWeight="regular"
                dataLocator="paymentMethodLbl"
                className="elem-mb-XS elem-mt-MED"
                text={labels.paymentMethod}
              />
            </PaymentMethodHeader>

            <FormSection name="shipmentMethods">
              <PaymentMethods
                paymentMethods={paymentMethods}
                formName={CREDIT_CARD_CONSTANTS.GUEST_FORM_NAME}
                selectedPaymentId={paymentMethodId}
                dispatch={dispatch}
                isApplePayEnabled={isApplePayEnabled}
                isApplePayEnabledOnBilling={isApplePayEnabledOnBilling}
                venmoId={CONSTANTS.PAYMENT_METHOD_VENMO}
                labels={labels}
                isAfterPayEnabled={isAfterPayEnabled}
                isVenmoEnabled={isVenmoEnabled}
                isPayPalEnabled={isPayPalEnabled}
              />
            </FormSection>
            {this.getAfterPaySubmitButton({
              paymentMethodId,
              labels,
              cartHaveGCItem,
              onSubmit,
              handleSubmit,
            })}
            {isApplePayEnabled && paymentMethodId === CONSTANTS.PAYMENT_METHOD_APPLE_PAY ? (
              <ApplePayTextContainer>
                <ApplePaymentButton navigation={navigation} isBillingPage />
              </ApplePayTextContainer>
            ) : null}

            {this.renderPayPalTextContainer(isPayPalEnabled, paymentMethodId, labels)}
            {isVenmoEnabled && this.renderVenmoText()}
            {paymentMethodId === CONSTANTS.PAYMENT_METHOD_CREDIT_CARD ? (
              <>
                <AddNewCCForm
                  cvvInfo={cvvInfo({ cvvCodeRichText })}
                  cardType={cardType}
                  cvvError={cvvError}
                  labels={labels}
                  formName="checkoutBilling"
                  isExpirationRequired={isExpirationRequired}
                  isGuest={isGuest}
                  dispatch={dispatch}
                  billingData={billingData}
                  creditFieldLabels={creditFieldLabels}
                  createRefParent={(...params) => createRefParent(...params, keyBoardAvoidRef)}
                />
                {!bagLoading ? (
                  <>
                    <CheckoutBillingAddress
                      isGuest={isGuest}
                      orderHasShipping={orderHasShipping}
                      addressLabels={addressLabels}
                      dispatch={dispatch}
                      shippingAddress={shippingAddress}
                      isSameAsShippingChecked={isSameAsShippingChecked}
                      labels={labels}
                      billingData={billingData}
                      formName="checkoutBilling"
                      createRefParent={(...params) => createRefParent(...params, keyBoardAvoidRef)}
                      zipCodeABTestEnabled={zipCodeABTestEnabled}
                      getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                      zipCodeLoader={zipCodeLoader}
                      mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
                    />
                    {renderAddressError(this)}
                  </>
                ) : (
                  <SkeletonWrapper>
                    <GenericSkeleton />
                  </SkeletonWrapper>
                )}
              </>
            ) : null}
          </PaymentMethodWrapper>
        )}
        <CnCTemplate
          navigation={navigation}
          btnText={nextSubmitText}
          routeToPage=""
          onPress={handleSubmit(onSubmit)}
          backLinkText={orderHasShipping ? backLinkShipping : backLinkPickup}
          onBackLinkPress={() =>
            orderHasShipping
              ? setCheckoutStage(CONSTANTS.SHIPPING_DEFAULT_PARAM)
              : setCheckoutStage(CONSTANTS.PICKUP_DEFAULT_PARAM)
          }
          pageCategory="guestBilling"
          showAccordian
          getPayPalSettings={getPayPalSettings}
          showPayPalButton={isPayPalEnabled && paymentMethodId === CONSTANTS.PAYMENT_METHOD_PAYPAL}
          isPayPalWebViewEnable={isPayPalWebViewEnable}
          showVenmoSubmit={paymentMethodId === CREDIT_CARD_CONSTANTS.PAYMENT_METHOD_VENMO}
          continueWithText={labels.continueWith}
          isBonusPointsEnabled={isBonusPointsEnabled}
          onVenmoSubmit={handleSubmit(onSubmit)}
          onVenmoError={onVenmoError}
          pageName="checkout"
          pageSection="billing"
          isGuest={isGuest}
          isAfterPaySelected={
            isAfterPayEnabled && paymentMethodId === CONSTANTS.PAYMENT_METHOD_AFTERPAY
          }
          trackClickAfterPay={trackClickAfterPay}
        />
      </>
    );
  }
}

const validateMethod = createValidateMethod({
  address: AddressFields.addressValidationConfig,
  ...getStandardConfig(['cardNumber', 'cvvCode', 'expYear', 'expMonth']),
});
export default reduxForm({
  form: 'checkoutBilling', // a unique identifier for this form
  enableReinitialize: true,
  shouldValidate: () => true,
  ...validateMethod,
  onSubmitFail: scrollToFirstErrorGuestBilling,
})(GuestBillingForm);
export { GuestBillingForm as GuestBillingFormVanilla };
