// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const PaymentMethodHeader = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

const PayPalTextContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

const ApplePayTextContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  min-height: 40px;
`;

const SkeletonWrapper = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  width: 200px;
`;

const PaymentMethodWrapper = styled.View`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const AfterPayThresholdMsg = styled.View`
  border: 2px solid ${(props) => props.theme.colorPalette.orange[800]};
  display: flex;
  flex-direction: row;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const AfterPayThresholdMsgTxt = styled.View`
  flex: 1;
`;

const AfterPayContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

export {
  PaymentMethodHeader,
  PayPalTextContainer,
  SkeletonWrapper,
  PaymentMethodWrapper,
  ApplePayTextContainer,
  AfterPayThresholdMsg,
  AfterPayThresholdMsgTxt,
  AfterPayContainer,
};
