/* eslint-disable max-lines */
/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { change } from 'redux-form';
import isEqual from 'lodash/isEqual';
import differenceWith from 'lodash/differenceWith';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { isMobileApp } from '@tcp/core/src/utils';
import {
  getEddFeatureSwitch,
  getEddABState,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.selectors';
import {
  isSfsInvMessagingEnabled,
  getEditShippingCheckoutStage,
} from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import {
  getSetGiftWrapValuesActn,
  setEditShippingFlag,
} from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.action.util';
import { getShippingSubmitErrors } from './ShippingPage.selectors';
import Shipping from '../views';
import checkoutUtil from '../../../util/utility';

const { hasPOBox } = checkoutUtil;

class ShippingContainer extends React.Component {
  static propTypes = {
    shippingDidMount: PropTypes.func.isRequired,
    isRegisteredUserCallDone: PropTypes.bool.isRequired,
    checkoutRoutingDone: PropTypes.bool.isRequired,
    shipmentMethods: PropTypes.shape({}).isRequired,
    address: PropTypes.shape({}).isRequired,
    selectedShipmentId: PropTypes.string.isRequired,
    addressLabels: PropTypes.shape({}).isRequired,
    checkoutServerError: PropTypes.shape({}).isRequired,
    isSubmitting: PropTypes.bool.isRequired,
    submitVerifiedShippingAddressData: PropTypes.func.isRequired,
    navigation: PropTypes.shape({}),
    updateShippingAddressData: PropTypes.func.isRequired,
    updateShippingMethodSelection: PropTypes.func.isRequired,
    shippingAddressId: PropTypes.string,
    loadShipmentMethods: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
    defaultShipmentId: PropTypes.string,
    onFileAddressKey: PropTypes.string,
    setAsDefaultShipping: PropTypes.bool,
    saveToAddressBook: PropTypes.bool,
    addNewShippingAddressData: PropTypes.func.isRequired,
    userAddresses: PropTypes.shape({}),
    scrollToTop: PropTypes.func.isRequired,
    mapStateToProps: PropTypes.shape({}).isRequired,
    formErrorMessage: PropTypes.shape({}).isRequired,
    updateReduxFormField: PropTypes.func.isRequired,
    shippingAddress: PropTypes.shape({}),
  };

  static defaultProps = {
    shippingAddressId: null,
    navigation: null,
    defaultShipmentId: null,
    onFileAddressKey: null,
    setAsDefaultShipping: false,
    saveToAddressBook: false,
    userAddresses: null,
    shippingAddress: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      showAddressVerification: false,
    };
  }

  componentDidMount() {
    const { shippingDidMount, scrollToTop } = this.props;
    shippingDidMount();
    const timer = setTimeout(() => {
      if (scrollToTop) {
        scrollToTop();
      }
      clearTimeout(timer);
    }, 500);
  }

  shouldComponentUpdate() {
    const { isSubmitting } = this.props;
    return !isSubmitting;
  }

  componentDidUpdate(prevProps) {
    const { onFileAddressKey } = this.props;
    const { onFileAddressKey: prevFileAddressKey } = prevProps;
    if (prevFileAddressKey && onFileAddressKey !== prevFileAddressKey) {
      this.getShipmentMethods(prevProps);
    }
  }

  closeAddAddressVerificationModal = (scrollViewRef, shippingFormRef) => {
    this.setState({ showAddressVerification: false });
    if (isMobileApp() && shippingFormRef) {
      shippingFormRef.measure((x, y, width, height) => {
        const scrollPosition = y - height;
        scrollViewRef.scrollTo({ x: 0, y: scrollPosition, animated: true });
      });
    }
  };

  /**
   * @description - Gets the first address from the selected address
   */
  getAddressField = (address) =>
    address && address.addressLine && address.addressLine.length > 0 ? address.addressLine[0] : '';

  /**
   * @description - get shipment methods with the updated address state
   */
  getShipmentMethods = () => {
    const { loadShipmentMethods, onFileAddressKey, userAddresses } = this.props;
    if (userAddresses && userAddresses.size > 0) {
      const address = userAddresses.find((add) => add.addressId === onFileAddressKey);
      if (address && address.state) {
        const requestPayload = {
          state: address.state,
          zipCode: address.zipCode,
          addressField1: this.getAddressField(address),
          formName: 'checkoutShipping',
        };
        loadShipmentMethods(requestPayload);
      }
    }
  };

  submitVerifiedShippingAddress = () => (shippingAddress) => {
    const {
      submitVerifiedShippingAddressData,
      navigation,
      updateShippingAddressData,
      updateReduxFormField,
    } = this.props;
    this.setState({ showAddressVerification: false }, () => {
      if (shippingAddress && shippingAddress.address2) {
        updateReduxFormField('checkoutShipping', 'address.addressLine2', shippingAddress.address2);
      }
    });
    if (this.isAddressUpdating) {
      this.isAddressUpdating = false;
      this.submitShippingAddressData.shipTo.address = {
        ...this.submitShippingAddressData.shipTo.address,
        ...shippingAddress,
        addressLine1: shippingAddress.address1,
        addressLine2: shippingAddress.address2,
        zipCode: shippingAddress.zip,
      };
      return updateShippingAddressData({
        ...this.submitShippingAddressData,
        shippingAddress,
        submitData: this.submitData,
        navigation,
      });
    }
    return submitVerifiedShippingAddressData({
      shippingAddress,
      submitData: this.submitData,
      navigation,
    });
  };

  callUpdateShippingMethod = ({
    prevSelectedShipmentId,
    selectedShipmentId,
    updateShippingMethodSelection,
    isAddressChange,
  }) => {
    if (
      prevSelectedShipmentId &&
      selectedShipmentId &&
      selectedShipmentId !== prevSelectedShipmentId
    ) {
      updateShippingMethodSelection({ id: selectedShipmentId, isAddressChange });
    }
  };

  getNextDefaultShipmentId = () => {
    const { shipmentMethods } = this.props;
    if (shipmentMethods.length > 0) {
      const defaultShipMode = shipmentMethods.find((method) => method.isDefault);
      return (defaultShipMode && defaultShipMode.id) || shipmentMethods[0].id;
    }
    return null;
  };

  shippingDidUpdate = (prevProps) => {
    const { address } = this.props;
    const { selectedShipmentId, updateShippingMethodSelection, shippingAddressId } = this.props;
    const {
      address: prevAddress,
      selectedShipmentId: prevSelectedShipmentId,
      onFileAddressKey,
    } = prevProps;
    if (address && prevAddress) {
      const {
        address: { addressLine1, addressLine2 },
        loadShipmentMethods,
      } = this.props;
      const {
        address: { addressLine1: prevAddressLine1, addressLine2: prevAddressLine2 },
      } = prevProps;
      if (
        (addressLine1 !== prevAddressLine1 || addressLine2 !== prevAddressLine2) &&
        hasPOBox(addressLine1, addressLine2)
      ) {
        loadShipmentMethods({ formName: 'checkoutShipping' });
      }
    }
    const isAddressChange = onFileAddressKey !== shippingAddressId;
    this.callUpdateShippingMethod({
      shippingAddressId,
      prevSelectedShipmentId,
      selectedShipmentId,
      updateShippingMethodSelection,
      isAddressChange,
    });
    const { shipmentMethods: prevShipmentMethods } = prevProps;
    const { shipmentMethods: nextShipmentMethods, dispatch } = this.props;
    if (
      nextShipmentMethods &&
      differenceWith(prevShipmentMethods, nextShipmentMethods, isEqual).length
    ) {
      dispatch(
        change(
          'checkoutShipping',
          'shipmentMethods.shippingMethodId',
          this.getNextDefaultShipmentId()
        )
      );
    }
  };

  addNewShippingAddress = () => {
    const {
      address,
      onFileAddressKey,
      setAsDefaultShipping,
      saveToAddressBook,
      addNewShippingAddressData,
    } = this.props;
    addNewShippingAddressData({
      shipTo: {
        address,
        addressId: address.addressId,
        emailAddress: address.emailAddress,
        emailSignup: true,
        onFileAddressKey,
        phoneNumber: address.phoneNumber,
        saveToAccount: saveToAddressBook,
        setAsDefault: setAsDefaultShipping,
      },
    });
  };

  updateShippingAddress = (scopeValue) => () => {
    const scope = scopeValue;
    const {
      address,
      onFileAddressKey,
      setAsDefaultShipping,
      saveToAddressBook,
      formatPayload,
      verifyAddressAction,
    } = scope.props;
    this.isAddressUpdating = true;
    this.submitShippingAddressData = {
      shipTo: {
        address,
        addressId: address.addressId,
        emailAddress: address.emailAddress,
        emailSignup: true,
        onFileAddressKey,
        phoneNumber: address.phoneNumber,
        saveToAccount: saveToAddressBook,
        setAsDefault: setAsDefaultShipping,
      },
    };
    const formattedPayload = { ...formatPayload(address), formName: 'checkoutShipping' };
    this.setState({ showAddressVerification: true });
    return verifyAddressAction(formattedPayload);
  };

  updateShippingAddressAndSubmit = (scope) => {
    const {
      address,
      onFileAddressKey,
      setAsDefaultShipping,
      saveToAddressBook,
      formatPayload,
      verifyAddressAction,
    } = scope.props;
    this.isAddressUpdating = true;
    this.submitShippingAddressData = {
      shipTo: {
        address,
        addressId: address.addressId,
        emailAddress: address.emailAddress,
        emailSignup: true,
        onFileAddressKey,
        phoneNumber: address.phoneNumber,
        saveToAccount: saveToAddressBook,
        setAsDefault: setAsDefaultShipping,
      },
    };
    const formattedPayload = { ...formatPayload(address), formName: 'checkoutShipping' };
    this.setState({ showAddressVerification: true });
    return verifyAddressAction(formattedPayload);
  };

  formatSubmitData = ({
    saveToAddressBook,
    onFileAddressKey,
    shipAddress = {},
    shipmentMethods = {},
    defaultShipping,
    smsSignUp = {},
    emailSignUp,
    hasSetGiftOptions,
  }) => {
    const { shippingMethodId = '' } = shipmentMethods;
    const { addressId = '', emailAddress = '', phoneNumber = '', primary = '' } = shipAddress;
    const { phoneNumber: smsUpdateNumber = '', sendOrderUpdate: wantsSmsOrderUpdates = '' } =
      smsSignUp;
    return {
      method: {
        shippingMethodId,
      },
      shipTo: {
        address: shipAddress,
        addressId,
        emailAddress,
        emailSignup: true,
        onFileAddressKey,
        phoneNumber,
        saveToAccount: saveToAddressBook,
        setAsDefault: defaultShipping || primary === 'true',
      },
      smsInfo: {
        smsUpdateNumber,
        wantsSmsOrderUpdates,
      },
      hasSetGiftOptions,
      emailSignUp,
    };
  };

  submitShippingForm = (scopeValue) => (data) => {
    const scope = scopeValue;
    const {
      address,
      shipmentMethods,
      onFileAddressKey,
      defaultShipping,
      saveToAddressBook,
      smsSignUp = {},
      emailSignUp,
    } = data;
    const {
      isGuest,
      userAddresses,
      formatPayload,
      setVenmoPickupState,
      hasSetGiftOptions,
      isEditShipping,
      handleSubmit,
      verifyAddressAction,
    } = scope.props;
    let shipAddress = address;
    if (
      !isGuest &&
      userAddresses &&
      userAddresses.size > 0 &&
      onFileAddressKey &&
      !isEditShipping
    ) {
      shipAddress = userAddresses.find((item) => item.addressId === onFileAddressKey);
      if (shipAddress) {
        const { addressLine } = shipAddress;
        const [addressLine1, addressLine2] = addressLine;
        shipAddress.addressLine1 = addressLine1;
        shipAddress.addressLine2 = addressLine2;
      }
    }
    const submitData = this.formatSubmitData({
      saveToAddressBook,
      onFileAddressKey,
      shipAddress,
      shipmentMethods,
      defaultShipping,
      smsSignUp,
      emailSignUp,
      hasSetGiftOptions,
    });

    if (isEditShipping) {
      this.submitData = submitData;
      return this.updateShippingAddressAndSubmit(scope);
    }

    if (isGuest || !onFileAddressKey || shipAddress.geographicalShippingCode) {
      const formattedPayload = { ...formatPayload(shipAddress), formName: 'checkoutShipping' };
      this.submitData = submitData;
      this.setState({ showAddressVerification: true });
      return verifyAddressAction(formattedPayload);
    }
    handleSubmit(submitData);
    return setVenmoPickupState(true);
  };

  render() {
    const { showAddressVerification } = this.state;
    const { formErrorMessage } = this.props;
    let { submitData } = this;
    if (this.isAddressUpdating) {
      submitData = this.submitShippingAddressData;
    }
    const shippingAddressData = (submitData && submitData.shipTo.address) || {};

    const { shippingAddressId, shippingAddress } = this.props;
    let { userAddresses } = this.props;
    const addressesContainsSelectedShipAddress =
      shippingAddress && userAddresses.find((address) => address.addressId === shippingAddressId);

    if (
      !addressesContainsSelectedShipAddress &&
      shippingAddressId &&
      shippingAddress &&
      shippingAddress.addressLine1
    ) {
      userAddresses = userAddresses.push({
        ...shippingAddress,
        addressLine: [shippingAddress.addressLine1, shippingAddress.addressLine2 || null],
        addressId: shippingAddressId,
      });
    }

    return (
      <>
        <Shipping
          {...this.props}
          userAddresses={userAddresses}
          updateShippingAddress={this.updateShippingAddress}
          submitShippingForm={this.submitShippingForm}
          shippingDidUpdate={this.shippingDidUpdate}
          submitVerifiedShippingAddressData={this.submitVerifiedShippingAddress}
          addNewShippingAddress={this.addNewShippingAddress}
          showAddressVerification={showAddressVerification}
          closeAddAddressVerificationModal={this.closeAddAddressVerificationModal}
          shippingAddressData={shippingAddressData}
          formErrorMessage={formErrorMessage}
        />
      </>
    );
  }
}

export const mapStateToProps = (state) => {
  return {
    formErrorMessage: state.Labels && state.Labels.global ? state.Labels.global.formValidation : {},
    isEDD: getEddFeatureSwitch(state),
    isEddABTest: getEddABState(state),
    submitErrors: getShippingSubmitErrors(state),
    isSfsInvMessagingEnabled: isSfsInvMessagingEnabled(state),
    isEditShipping: getEditShippingCheckoutStage(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateReduxFormField: (form, key, value) => {
      dispatch(change(form, key, value));
    },
    setGiftWrapValue: (payload) => {
      dispatch(getSetGiftWrapValuesActn(payload));
    },
    setEditShippingFlag: (payload) => {
      dispatch(setEditShippingFlag(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShippingContainer);
