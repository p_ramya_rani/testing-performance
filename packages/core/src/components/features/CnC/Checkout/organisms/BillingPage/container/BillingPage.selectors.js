// 9fbef606107a605d69c0edbcd8029e5d 
import CONSTANTS from '../../../Checkout.constants';

const getCVVCodeInfoContentInfo = state => {
  let cvvCodeCID;
  /* istanbul ignore else */
  if (state.Labels.checkout.billing && Array.isArray(state.Labels.checkout.billing.referred)) {
    cvvCodeCID =
      state.Labels.checkout.billing.referred.length &&
      state.Labels.checkout.billing.referred.find(
        label => label.name === CONSTANTS.CREDIT_CARD_CVV_INFO_LABEL
      );
  }
  return cvvCodeCID;
};

const getCVVCodeRichTextSelector = state => {
  const rContent = state.CartPageReducer.get('moduleXContent').find(
    moduleX => moduleX.name === CONSTANTS.CREDIT_CARD_CVV_INFO_LABEL
  );
  return rContent && rContent.richText;
};

export { getCVVCodeInfoContentInfo, getCVVCodeRichTextSelector };
