// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import EddComponent from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent';
import BodyCopy from '../../../../../../../../common/atoms/BodyCopy';
import style from '../styles/ShippingMethodDisplay.style.native';

const { ShippingMethodHeading, ShippingMethodName, EddWrapper } = style;

class ShippingMethodDisplay extends React.PureComponent {
  render() {
    const { displayName, labels, shippingCode, hideSubtitle } = this.props;
    const { lbl_review_sectionShippingMethodTitle: title } = labels;
    const eddDisplayName = hideSubtitle ? '' : displayName;
    return (
      <View>
        <ShippingMethodHeading>
          <BodyCopy
            fontSize="fs16"
            dataLocator=""
            mobileFontFamily="secondary"
            color="gray.900"
            fontWeight="extrabold"
            className="heading"
            text={title}
          />
        </ShippingMethodHeading>
        <ShippingMethodName>
          <BodyCopy
            fontSize="fs16"
            dataLocator=""
            mobileFontFamily="secondary"
            color="gray.900"
            fontWeight="regular"
            text={displayName}
          />
        </ShippingMethodName>
        <EddWrapper>
          <EddComponent
            selectedShipment={{ code: shippingCode, displayName: eddDisplayName }}
            isRangeDate
            textAlign="left"
            fontSize="fs14"
          />
        </EddWrapper>
      </View>
    );
  }
}

ShippingMethodDisplay.propTypes = {
  displayName: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
  shippingCode: PropTypes.string,
  hideSubtitle: PropTypes.bool,
};

ShippingMethodDisplay.defaultProps = {
  labels: {
    lbl_review_sectionShippingMethodTitle: 'Shipping Method',
  },
  shippingCode: '',
  hideSubtitle: false,
};

export default ShippingMethodDisplay;
export { ShippingMethodDisplay as ShippingMethodDisplayanilla };
