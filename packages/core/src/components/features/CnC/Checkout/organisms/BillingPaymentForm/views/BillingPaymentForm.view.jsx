/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { reduxForm, Field, change } from 'redux-form';
import GenericSkeleton from '@tcp/core/src/components/common/molecules/GenericSkeleton/GenericSkeleton.view';
import CardImage from '@tcp/core/src/components/common/molecules/CardImage';
import { getIconPath } from '@tcp/core/src/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/BillingPaymentForm.style';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';
import PaymentMethods from '../../../../common/molecules/PaymentMethods';
import CreditCardDropdown from './CreditCardDropdown.view';
import Card from '../../../../../../common/molecules/Card';
import {
  Row,
  Col,
  Heading,
  TextBox,
  BodyCopy,
  Anchor,
  Button,
  Image,
} from '../../../../../../common/atoms';
import constants from '../container/CreditCard.constants';
import CheckoutFooter from '../../../molecules/CheckoutFooter';
import utility, {
  getExpirationRequiredFlag,
  getCreditCardList,
  getSelectedCard,
  scrollToFirstError,
} from '../../../util/utility';
import { CHECKOUT_ROUTES } from '../../../Checkout.constants';
import DropdownList from './CreditCardDropdownList.view';
import getCvvInfo from '../../../molecules/CVVInfo';
import AddNewCCForm from '../../AddNewCCForm';
import CheckoutBillingAddress from '../../CheckoutBillingAddress';
import AddressFields from '../../../../../../common/molecules/AddressFields';
import {
  propTypes,
  defaultProps,
  getCardOptions,
  onCCDropUpdateChange,
  getFormName,
  renderBillingAddressHeading,
  renderAddressError,
  showVenmoButton,
} from './BillingPaymentForm.view.util';
import VenmoPaymentButton from '../../../../../../common/atoms/VenmoPaymentButton';
import ApplePaymentButton from '../../../../../../common/atoms/ApplePaymentButton';
import CheckoutOrderInfo from '../../../molecules/CheckoutOrderInfoMobile';
import CardEditFrom from './CardEditForm.view';
import BillingPayPalButton from '../../BillingPayPalButton';

import {
  onEditCardFocus as onCardEditFocus,
  setFormToEditState,
  unsetPaymentFormEditState,
  handleBillingFormSubmit,
  onAddNewCreditCardClick,
  onCCDropDownChange,
} from './BillingPaymentForm.util';
import ErrorMessage from '../../../../common/molecules/ErrorMessage';

/**
 * @class BillingPaymentForm
 * @extends {PureComponent}
 * @description view component to render signed in user form.
 */
export class BillingPaymentForm extends React.PureComponent {
  static propTypes = propTypes;

  static defaultProps = defaultProps;

  constructor(props) {
    super(props);
    this.state = { addNewCCState: false, editMode: false, editModeSubmissionError: '' };
    this.ediCardErrorRef = React.createRef();
  }

  /**
   * @function getCreditCardDropDown
   * @description returns the  credit card list
   */
  getCreditCardDropDown = (options, onClickHandler, activeValue) => {
    return (
      <DropdownList
        {...{ optionsMap: options, clickHandler: onClickHandler, activeValue }}
        className="custom-select-dropDownList"
      />
    );
  };

  /**
   * @function getCheckoutBillingAddress
   * @description returns the checkout billing address form
   */
  getCheckoutBillingAddress = ({ editMode } = {}) => {
    const {
      selectedOnFileAddressId,
      isSameAsShippingChecked,
      bagLoading,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
    } = this.props;
    const { isEditFormSameAsShippingChecked, editFormSelectedOnFileAddressId } = this.props;
    const { userAddresses, labels, cardList, isGuest, dispatch } = this.props;
    const { orderHasShipping, addressLabels, shippingAddress, billingData } = this.props;
    const { addNewCCState } = this.state;
    const creditCardList = getCreditCardList({ cardList });
    return !bagLoading ? (
      <CheckoutBillingAddress
        shippingAddress={shippingAddress}
        isSameAsShippingChecked={
          editMode ? isEditFormSameAsShippingChecked : isSameAsShippingChecked
        }
        billingData={billingData}
        userAddresses={userAddresses}
        {...{ labels, editMode, isGuest, orderHasShipping, dispatch, addressLabels }}
        selectedOnFileAddressId={
          editMode ? editFormSelectedOnFileAddressId : selectedOnFileAddressId
        }
        formName={getFormName(editMode)}
        addNewCCState={
          addNewCCState ||
          (!creditCardList && !orderHasShipping) ||
          (creditCardList && creditCardList.size === 0)
        }
        zipCodeABTestEnabled={zipCodeABTestEnabled}
        getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
        zipCodeLoader={zipCodeLoader}
        mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
        mapboxSwitch={mapboxSwitch}
      />
    ) : (
      <GenericSkeleton />
    );
  };

  /**
   * @function getAddNewCCForm
   * @description returns the add new credit card form
   */
  getAddNewCCForm = ({ onCardFocus, editMode } = {}) => {
    const { cardType, editFormCardType, labels, dispatch, syncErrorsObj, isGuest } = this.props;
    const { cvvCodeRichText, creditFieldLabels, isSaveToAccountChecked } = this.props;
    const { isPLCCEnabled } = this.props;
    const { infoIconText } = labels;
    let cvvError;
    /* istanbul ignore else */
    if (syncErrorsObj) {
      cvvError = syncErrorsObj.syncError.cvvCode;
    }
    const formCardType = editMode ? editFormCardType : cardType;
    const isExpirationRequired = getExpirationRequiredFlag({ cardType: formCardType });
    const formName = getFormName(editMode);
    dispatch(change(formName, 'cardType', cardType));
    dispatch(change(formName, 'isPLCCEnabled', isPLCCEnabled));
    return (
      <AddNewCCForm
        cvvInfo={getCvvInfo({ cvvCodeRichText, infoIconText })}
        {...{
          cardType: formCardType,
          cvvError,
          isGuest,
          onCardFocus,
          isSaveToAccountChecked,
        }}
        {...{
          dispatch,
          isExpirationRequired,
          creditFieldLabels,
          editMode,
          labels,
          isPLCCEnabled,
        }}
        formName={formName}
      />
    );
  };

  /**
   * @function addNewBillingInfoForm
   * @description returns the new billing info form
   */
  addNewBillingInfoForm = () => {
    const { onFileCardKey, labels, cardList, billingData } = this.props;
    const { paymentId } = billingData;
    const creditCardList = getCreditCardList({ cardList });
    const ccListPreset = creditCardList && creditCardList.size > 0;
    return (
      <>
        {!paymentId && renderBillingAddressHeading(labels)}
        {ccListPreset && this.getCCDropDown({ labels, creditCardList, onFileCardKey })}
        {this.getAddNewCCForm()}
        {this.getCheckoutBillingAddress()}
      </>
    );
  };

  /**
   * @function onEditCardFocus
   * @description on edit card number field focus event to clear the field first time only
   */
  onEditCardFocus = (scope) => {
    if (!this.cardNumberCleared) {
      this.cardNumberCleared = true;
      onCardEditFocus(scope);
    }
  };

  /**
   * @function getCCDropDown
   * @description returns the credit card drop down if user has credit cards
   */
  getCCDropDown = ({ labels, creditCardList, onFileCardKey, selectedCard, editMode }) => {
    const { addNewCCState } = this.state;
    const { dispatch } = this.props;
    const restCardParam = { addNewCC: () => onAddNewCreditCardClick(this), selectedCard };
    const cardParams = {
      creditCardList,
      labels,
      onFileCardKey,
      addNewCCState,
      ...restCardParam,
    };
    const colSize = { large: 6, small: 6, medium: 8 };
    if (onFileCardKey) {
      onCCDropUpdateChange(onFileCardKey, selectedCard, dispatch);
    }
    return (
      <Row fullBleed className="elem-mb-XL elem-mt-MED">
        <Col
          colSize={colSize}
          className={`creditCardForm__addressBook ${editMode ? 'disable-drop-down' : ''}`}
        >
          <Field
            selectListTitle=""
            name="onFileCardKey"
            id="onFileCardKey"
            component={CreditCardDropdown}
            dataLocator="selectCardDrpDown"
            options={getCardOptions(cardParams)}
            childrenComp={(options, onClickHandler, activeValue, onClose) =>
              this.getCreditCardDropDown(options, onClickHandler, activeValue, onClose)
            }
            onChange={() => onCCDropDownChange(this)}
            customTitle={labels.addCreditCard}
          />
        </Col>
      </Row>
    );
  };

  renderCardDetailsHeading = ({ hideAnchor } = {}) => {
    const { labels } = this.props;
    return (
      <BodyCopy component="div" fontFamily="secondary" className="billing-payment-details">
        <BodyCopy
          fontFamily="primary"
          fontSize="fs26"
          fontWeight="regular"
          dataLocator="cardDetailLbl"
          className="elem-mb-XS"
        >
          {labels.cardDetailsTitle}
        </BodyCopy>
        {!hideAnchor && (
          <Anchor
            fontSizeVariation="xlarge"
            underline
            noLink
            onClick={(e) => {
              this.cardNumberCleared = false;
              setFormToEditState(this, e);
            }}
            anchorVariation="primary"
            className="billing-payment-edit"
            dataLocator="billing-payment-edit"
          >
            {labels.edit}
          </Anchor>
        )}
      </BodyCopy>
    );
  };

  handleCVVNumber = (value, previousValue) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      return value;
    }
    return previousValue;
  };

  /**
   * @function getCreditListView
   * @description returns the credit card drop down along with selected card
   */
  getCreditListView = ({ labels, cvvCodeRichText, creditCardList, onFileCardKey }) => {
    const { defaultPayment, paymentMethod, creditCardEnd, cvvCode, infoIconText } = labels;
    const selectedCard = onFileCardKey ? getSelectedCard({ creditCardList, onFileCardKey }) : '';
    const { editMode, editModeSubmissionError } = this.state;
    const { dispatch, updateCardDetail, editFormCardType, isPLCCEnabled } = this.props;
    const billingPaymentFormWidthCol = { large: 3, small: 4, medium: 4 };
    const cvvCodeColWidth = { large: 3, small: 2, medium: 4 };
    const { renderCardDetailsHeading, getAddNewCCForm, unsetFormEditState, onEditCardFocus } = this;
    return (
      <>
        {renderBillingAddressHeading(labels)}
        {this.getCCDropDown({
          labels,
          creditCardList,
          onFileCardKey,
          selectedCard,
          editMode,
        })}
        {!editMode ? (
          <>
            {selectedCard ? (
              <>
                {this.renderCardDetailsHeading()}
                <Heading
                  component="h2"
                  variant="listMenu"
                  className="paymentMethodHeading"
                  dataLocator="paymentMethodLbl"
                >
                  {paymentMethod}
                </Heading>
                <Row fullBleed>
                  <Col colSize={billingPaymentFormWidthCol} className="billing-payment-card-info">
                    <CardImage
                      card={selectedCard}
                      cardNumber={`${creditCardEnd}${selectedCard.accountNo.slice(-4)}`}
                    />
                  </Col>
                  {selectedCard.ccType !== constants.ACCEPTED_CREDIT_CARDS.PLACE_CARD &&
                    selectedCard.ccType !== constants.ACCEPTED_CREDIT_CARDS.APPLE && (
                      <Col colSize={cvvCodeColWidth} className="position-relative cvvCode">
                        <Field
                          placeholder={cvvCode}
                          name="cvvCode"
                          id="cvvCode"
                          component={TextBox}
                          dataLocator="cvvTxtBox"
                          className="field"
                          showSuccessCheck={false}
                          enableSuccessCheck={false}
                          autoComplete="off"
                          type="tel"
                          maxLength="4"
                          normalize={this.handleCVVNumber}
                        />
                        <span className="hide-show show-hide-icons">
                          <span className="info-icon-img-wrapper">
                            {getCvvInfo({ cvvCodeRichText, infoIconText })}
                          </span>
                        </span>
                      </Col>
                    )}
                </Row>
                {!selectedCard.defaultInd && (
                  <Row fullBleed className="billing-payment-subHeading default-payment">
                    <Field
                      dataLocator="defaultPaymentChkBox"
                      name="defaultPaymentMethod"
                      component={InputCheckbox}
                      className="default-payment"
                    >
                      <BodyCopy
                        dataLocator="billing-payment-default-payment-heading-lbl"
                        fontSize="fs16"
                        fontFamily="secondary"
                        fontWeight="regular"
                      >
                        {defaultPayment}
                      </BodyCopy>
                    </Field>
                  </Row>
                )}
              </>
            ) : (
              this.getAddNewCCForm()
            )}
            {selectedCard && (
              <Row fullBleed className="billing-payment-subHeading">
                <Heading
                  component="h2"
                  variant="listMenu"
                  className="paymentMethodHeading"
                  dataLocator="billingAddressLbl"
                >
                  {labels.billingAddress}
                </Heading>
              </Row>
            )}

            {selectedCard ? (
              <Row fullBleed className="elem-mb-XL">
                {onFileCardKey && (
                  <Card
                    card={selectedCard}
                    className="CreditCardForm__address"
                    dataLocator="selectedCardDetail"
                    showAddress
                  />
                )}
              </Row>
            ) : (
              this.getCheckoutBillingAddress()
            )}
          </>
        ) : (
          <CardEditFrom
            {...{
              selectedCard,
              renderCardDetailsHeading,
              getAddNewCCForm,
              unsetFormEditState,
            }}
            {...{
              onEditCardFocus,
              dispatch,
              labels,
              updateCardDetail,
              editModeSubmissionError,
            }}
            key="cardEditForm"
            addressForm={this.getCheckoutBillingAddress}
            errorMessageRef={this.ediCardErrorRef}
            cardType={editFormCardType}
            isPLCCEnabled={isPLCCEnabled}
          />
        )}
      </>
    );
  };

  unsetFormEditState = (e) => {
    unsetPaymentFormEditState(this, e);
  };

  /**
   * @function getCreditCardWrapper
   * @description returns the credit card payment method view
   */
  getCreditCardWrapper = ({ labels, creditCardList, cvvCodeRichText, onFileCardKey }) => {
    const { addNewCCState } = this.state;
    return (
      <>
        {creditCardList && creditCardList.size > 0 && !addNewCCState
          ? this.getCreditListView({
              labels,
              cvvCodeRichText,
              creditCardList,
              onFileCardKey,
            })
          : this.addNewBillingInfoForm()}
        {<div className="errorWrapper">{renderAddressError(this, true)}</div>}
      </>
    );
  };

  /**
   * @description - Show Paypal if payment method is Paypal and Payments tab is enabled
   */
  showPaypalButton = () => {
    const { paymentMethodId, isPayPalEnabled } = this.props;
    return isPayPalEnabled && paymentMethodId === constants.PAYMENT_METHOD_PAY_PAL;
  };

  /**
   * @description - Show Paypal if payment method is Paypal and Payments tab is enabled
   */
  showApplePayButton = () => {
    const { paymentMethodId, isApplePayEnabled } = this.props;
    return isApplePayEnabled && paymentMethodId === constants.PAYMENT_METHOD_APPLE;
  };

  showAfterPayButton = () => {
    const { paymentMethodId, isAfterPayEnabled } = this.props;
    return isAfterPayEnabled && paymentMethodId === constants.PAYMENT_METHOD_AFTERPAY;
  };

  getAppleButton = ({ paymentMethodId, labels, isApplePayEnabled }) => {
    return isApplePayEnabled && paymentMethodId === constants.PAYMENT_METHOD_APPLE ? (
      <ApplePaymentButton
        className="apple-container"
        labels={labels}
        containerId="billing-page-apple-one"
        buttonClass="apple-pay-button-black"
      />
    ) : (
      ''
    );
  };

  getAfterPaySubmitButton = ({ paymentMethodId, labels, cartHaveGCItem }) => {
    const { afterPayMinOrderAmount, orderTotal, afterPayMaxOrderAmount = 1000 } = this.props;
    let orderTotalThresholdMessage = labels.orderTotalThresholdMessage.replace(
      '[AfterPayMinOrderAmount]',
      afterPayMinOrderAmount
    );
    orderTotalThresholdMessage = orderTotalThresholdMessage.replace(
      '[AfterPayMaxOrderAmount]',
      afterPayMaxOrderAmount
    );
    return paymentMethodId === constants.PAYMENT_METHOD_AFTERPAY ? (
      <Row className="elem-mb-XL" fullBleed>
        <Col colSize={{ small: 6, medium: 5, large: 5 }}>
          {afterPayMinOrderAmount <= orderTotal &&
          afterPayMaxOrderAmount >= orderTotal &&
          !cartHaveGCItem ? (
            <>
              <Button
                aria-label={constants.PAYMENT_METHOD_AFTERPAY}
                type="submit"
                fontSize="fs14"
                fontWeight="extrabold"
                buttonVariation="variable-width"
                fill="BLUE"
                dataLocator="reviewBtn"
                value={constants.PAYMENT_METHOD_AFTERPAY}
                fullWidth
              >
                {labels.afterpaySubmit}
              </Button>
              <BodyCopy
                fontFamily="secondary"
                fontSize="fs16"
                fontWeight="regular"
                className="elem-mt-MED"
              >
                {labels.afterPayDefaultMessage}
              </BodyCopy>
            </>
          ) : (
            <div className="afterPay_threshold">
              <Image
                alt="triangle-warning"
                src={getIconPath('triangle-warning')}
                height="25px"
                width="25px"
              />
              <BodyCopy
                className="elem-ml-SM"
                fontFamily="secondary"
                fontSize="fs16"
                fontWeight="extrabold"
              >
                {cartHaveGCItem ? labels.cartHasGCMessage : orderTotalThresholdMessage}
              </BodyCopy>
            </div>
          )}
        </Col>
      </Row>
    ) : null;
  };

  /**
   * @function render
   * @description render method to be called of component
   */
  render() {
    const { className, handleSubmit, cardList, isGuest, pageCategory, isUSSite } = this.props;
    const {
      onFileCardKey,
      labels,
      cvvCodeRichText,
      isVenmoEnabled,
      venmoError,
      isApplePayEnabled,
      isECOM,
      isApplePayEnabledOnBilling,
      isAfterPayEnabled,
      trackClickAfterPay,
      cartHaveGCItem,
    } = this.props;
    const { paymentMethodId, orderHasShipping, backLinkPickup, isPayPalEnabled } = this.props;
    const {
      backLinkShipping,
      nextSubmitText,
      isPaymentDisabled,
      showAccordian,
      pageName,
      appliedExpiredCoupon,
    } = this.props;
    const { bagLoading } = this.props;
    const creditCardList = getCreditCardList({ cardList });
    const afterPayInProgress = paymentMethodId === constants.PAYMENT_METHOD_AFTERPAY;
    return (
      <form
        name={constants.FORM_NAME}
        noValidate
        className={className}
        id={constants.FORM_NAME}
        onSubmit={(e) =>
          handleBillingFormSubmit(this, e, null, {
            trackClickAfterPay,
            afterPayInProgress,
            pageCategory,
          })
        }
      >
        {!isPaymentDisabled && (
          <div>
            <BodyCopy
              fontFamily="primary"
              fontSize="fs26"
              fontWeight="regular"
              dataLocator="paymentMethodLbl"
              className="elem-mb-LRG elem-mt-XL"
            >
              {labels.paymentMethod}
            </BodyCopy>
            <PaymentMethods
              labels={labels}
              className="elem-mb-LRG"
              isVenmoEnabled={isVenmoEnabled}
              isApplePayEnabled={isApplePayEnabled}
              isECOM={isECOM}
              isApplePayEnabledOnBilling={isApplePayEnabledOnBilling}
              isUSSite={isUSSite}
              isPayPalEnabled={isPayPalEnabled}
              isAfterPayEnabled={isAfterPayEnabled}
            />
            {paymentMethodId === constants.PAYMENT_METHOD_CREDIT_CARD &&
              this.getCreditCardWrapper({
                labels,
                creditCardList,
                cvvCodeRichText,
                onFileCardKey,
              })}
            {isPayPalEnabled && paymentMethodId === constants.PAYMENT_METHOD_PAY_PAL ? (
              <Row className="elem-mb-XL" fullBleed>
                <Col colSize={{ small: 6, medium: 5, large: 5 }}>
                  <BillingPayPalButton labels={labels} containerId="billing-page-paypal-one" />
                </Col>
              </Row>
            ) : null}
            {this.getAfterPaySubmitButton({ paymentMethodId, labels, cartHaveGCItem })}
            <Row className="elem-mb-XL" fullBleed>
              <Col colSize={{ small: 6, medium: 5, large: 5 }}>
                {this.getAppleButton({ paymentMethodId, labels, isApplePayEnabled })}
              </Col>
            </Row>
            {paymentMethodId === constants.PAYMENT_METHOD_VENMO && isVenmoEnabled && (
              <VenmoPaymentButton
                className="venmo-container"
                continueWithText={labels.continueWith}
                onSuccess={handleSubmit}
                isVenmoBlueButton
              />
            )}
            {venmoError && <ErrorMessage error={venmoError} className="checkout-page-error" />}
          </div>
        )}
        <CheckoutOrderInfo
          isGuest={isGuest}
          showAccordian={showAccordian}
          pageCategory={pageCategory}
          isCheckoutFlow
        />
        <CheckoutFooter
          hideBackLink
          backLinkHandler={() =>
            orderHasShipping
              ? utility.routeToPage(CHECKOUT_ROUTES.shippingPage)
              : utility.routeToPage(CHECKOUT_ROUTES.pickupPage)
          }
          nextButtonText={nextSubmitText}
          backLinkText={orderHasShipping ? backLinkShipping : backLinkPickup}
          showVenmoSubmit={showVenmoButton(this)}
          showPayPalButton={this.showPaypalButton()}
          showApplePayButton={this.showApplePayButton()}
          showAfterPayButton={this.showAfterPayButton()}
          continueWithText={labels.continueWith}
          onVenmoSubmit={handleSubmit}
          venmoError={venmoError}
          bagLoading={bagLoading}
          pageName={pageName}
          appliedExpiredCoupon={appliedExpiredCoupon}
          submitForm={constants.FORM_NAME}
        />
      </form>
    );
  }
}

const validateMethod = createValidateMethod({
  address: AddressFields.addressValidationConfig,
  ...getStandardConfig(['cardNumber', 'cvvCode', 'expYear', 'expMonth']),
});
export default reduxForm({
  form: constants.FORM_NAME, // a unique identifier for this form
  enableReinitialize: true,
  shouldValidate: () => true,
  ...validateMethod,
  onSubmitFail: (errors) => scrollToFirstError(errors),
})(withStyles(BillingPaymentForm, styles));
export { BillingPaymentForm as BillingPaymentFormVanilla };
