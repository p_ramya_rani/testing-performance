// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import {
  getIsAfterPayEnabled,
  getAfterPayMinOrderAmount,
  getAfterPayMaxOrderAmount,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getGrandTotal,
  getGiftCardsTotal,
} from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import GuestBillingPage from '../views';
import CONSTANTS from '../../../Checkout.constants';
import {
  getCardType,
  getSyncError,
  getPaymentMethodId,
  getSameAsShippingValue,
  getSelectedCountry,
} from './GuestBillingForm.selectors';
import {
  submitBillingSection,
  setVenmoPaymentInProgress,
  setApplePaymentInProgress,
  setAfterPayInProgress,
} from '../../../container/Checkout.action';
import CheckoutSelectors, {
  isUsSite,
  getIsApplePayEnabled,
} from '../../../container/Checkout.selector';
import CreditCardSelector from '../../BillingPaymentForm/container/CreditCard.selectors';
import { getSiteId, capitalize, isMobileApp } from '../../../../../../../utils';

/**
 * @class GuestBillingContainer
 * @extends {Component}
 * @description container component to render guest user form.
 */
class GuestBillingContainer extends React.Component {
  /**
   * @function getSelectedPaymentMethod
   * @description returns the initial payment method selected during billing page load.
   */
  getSelectedPaymentMethod = () => {
    const { billingData } = this.props;
    return billingData.paymentMethod === CONSTANTS.PAYPAL_LABEL
      ? CONSTANTS.PAYMENT_METHOD_PAYPAL
      : CONSTANTS.PAYMENT_METHOD_CREDIT_CARD;
  };

  selectOtherMethod = () => {
    const { isApplePaymentInProgress, afterPayInProgress, billingData } = this.props;
    if (
      afterPayInProgress ||
      (billingData && billingData.billing && billingData.billing.isAfterpay)
    ) {
      return CONSTANTS.PAYMENT_METHOD_AFTERPAY;
    }
    return isApplePaymentInProgress
      ? CONSTANTS.PAYMENT_METHOD_APPLE
      : this.getSelectedPaymentMethod();
  };

  isApplePay = (billingData) =>
    billingData && billingData.billing && billingData.billing.cardType === CONSTANTS.APPLEPAY_LABEL;

  setAfterPayStatus = (data, afterPayInProgress) => {
    const { setAfterPayProgress = () => {} } = this.props;
    if (afterPayInProgress) {
      setAfterPayProgress(true);
    } else {
      setAfterPayProgress(false);
    }
  };

  /**
   * @function submitBillingData
   * @description submits the billing data
   */
  submitBillingData = (data) => {
    const {
      submitBilling,
      navigation,
      setVenmoProgress,
      setAppleProgress,
      trackClickAfterPay,
      pageCategory,
    } = this.props;
    const { address, sameAsShipping, paymentMethodId } = data;
    const afterPayInProgress = paymentMethodId === CONSTANTS.PAYMENT_METHOD_AFTERPAY;
    let addressLine1;
    let addressLine2;
    let city;
    let country;
    let firstName;
    let lastName;
    let state;
    let zipCode;
    let onFileAddressKey;
    let onFileAddressId;
    let submitData = {};
    /* istanbul ignore else */
    if (address) {
      ({
        addressLine1,
        addressLine2,
        city,
        country,
        firstName,
        lastName,
        state,
        zipCode,
        onFileAddressKey,
        onFileAddressId,
      } = address);
    }
    if (paymentMethodId !== CONSTANTS.PAYMENT_METHOD_VENMO) {
      submitData = {
        cardNumber: data.cardNumber,
        cardType: data.cardType,
        cvv: data.cvvCode,
        emailAddress: undefined,
        expMonth: data.expMonth,
        expYear: data.expYear,
        address: {
          addressLine1,
          addressLine2: addressLine2 || '',
          city,
          country,
          firstName,
          lastName,
          state,
          zipCode,
          sameAsShipping,
          onFileAddressKey,
          onFileAddressId,
        },
        navigation,
      };
      setVenmoProgress(false); // Cancelling Venmo Progress for non venmo payment option
      setAppleProgress(false);
    } else {
      submitData = { paymentMethodId, navigation };
    }
    if (afterPayInProgress && typeof trackClickAfterPay === 'function') {
      if (isMobileApp()) {
        trackClickAfterPay(
          {
            afterpay_metrics_v141: 1,
            afterpay_location_v131: capitalize(pageCategory),
            customEvents: ['event141'],
          },
          { name: 'afterpay_click', module: 'checkout' }
        );
      } else {
        trackClickAfterPay({
          afterpay_metrics141: 1,
          afterpay_cd131: capitalize(pageCategory),
          customEvents: ['event141'],
        });
      }
    }
    this.setAfterPayStatus(data, afterPayInProgress);
    submitBilling(submitData);
  };

  /**
   * @function getAddressInitialValues
   * @description returns the initial values for address fields
   */
  getAddressInitialValues = () => {
    const { billingData } = this.props;
    if (billingData && billingData.address) {
      const { address } = billingData;
      const {
        addressLine1,
        addressLine2,
        city,
        country,
        firstName,
        lastName,
        state,
        zipCode,
        onFileAddressKey,
        onFileAddressId,
      } = address;
      return {
        addressLine1,
        addressLine2,
        city,
        country,
        firstName,
        lastName,
        state,
        zipCode,
        onFileAddressKey,
        onFileAddressId,
      };
    }

    return {
      country: getSiteId() && getSiteId().toUpperCase(),
    };
  };

  populateBillingInfo = (billingData) => {
    return (
      billingData &&
      billingData.billing &&
      billingData.billing.cardType !== CONSTANTS.PAYPAL_LABEL &&
      !billingData.billing.isAfterpay
    );
  };

  /**
   * @function render
   * @description render method to be called of component
   */
  render() {
    const {
      billingData,
      orderHasShipping,
      syncErrors,
      shippingOnFileAddressKey,
      isVenmoPaymentInProgress,
      setCheckoutStage,
      venmoError,
      isPayPalWebViewEnable,
      bagLoading,
      isUSSite,
      selectedCountry,
      isPLCCEnabled,
      mapboxAutocompleteTypesParam,
      trackClickAfterPay,
      pageCategory,
      cartHaveGCItem,
    } = this.props;
    let cardNumber;
    let cardType;
    let expMonth;
    let expYear;
    let billingOnFileAddressKey;
    if (this.populateBillingInfo(billingData)) {
      ({
        billing: { cardNumber, cardType, expMonth, expYear },
        address: { onFileAddressKey: billingOnFileAddressKey },
      } = billingData);
    }
    return (
      <GuestBillingPage
        {...this.props}
        initialValues={{
          paymentMethodId: isVenmoPaymentInProgress
            ? CONSTANTS.PAYMENT_METHOD_VENMO
            : this.selectOtherMethod(),
          sameAsShipping:
            orderHasShipping &&
            (isEmpty(billingData) || billingOnFileAddressKey === shippingOnFileAddressKey),
          address: this.getAddressInitialValues(),
          cardNumber: this.isApplePay(billingData) ? '' : cardNumber,
          cardType,
          expMonth: this.isApplePay(billingData) ? '' : expMonth,
          expYear: this.isApplePay(billingData) ? '' : expYear,
        }}
        onSubmit={this.submitBillingData}
        syncErrorsObj={syncErrors}
        setCheckoutStage={setCheckoutStage}
        venmoError={venmoError}
        isPayPalWebViewEnable={isPayPalWebViewEnable}
        bagLoading={bagLoading}
        isUSSite={isUSSite}
        selectedCountry={selectedCountry}
        isPLCCEnabled={isPLCCEnabled}
        mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
        trackClickAfterPay={trackClickAfterPay}
        pageCategory={pageCategory}
        cartHaveGCItem={cartHaveGCItem}
      />
    );
  }
}

export const mapStateToProps = (state) => {
  return {
    cardType: getCardType(state),
    syncErrors: getSyncError(state),
    isPaymentDisabled: CheckoutSelectors.getIsPaymentDisabled(state),
    isApplePayEnabled: getIsApplePayEnabled(state),
    isApplePayEnabledOnBilling: CheckoutSelectors.getIsApplePayEnabledOnBilling(state),
    isECOM: BagPageSelector.getECOMStatus(state),
    paymentMethodId: getPaymentMethodId(state),
    isPayPalEnabled: BagPageSelector.getIsPayPalEnabled(state),
    isSameAsShippingChecked: getSameAsShippingValue(state),
    shippingOnFileAddressKey: CreditCardSelector.getShippingOnFileAddressKey(state),
    venmoError: CheckoutSelectors.getVenmoError(state),
    getPayPalSettings: CheckoutSelectors.getPayPalSettings(state),
    bagLoading: BagPageSelector.isBagLoading(state),
    isVenmoAppInstalled: CheckoutSelectors.isVenmoAppInstalled(state),
    isUSSite: isUsSite(state),
    selectedCountry: getSelectedCountry(state),
    isPLCCEnabled: CreditCardSelector.getIsPLCCEnabled(state, true),
    isAfterPayEnabled: getIsAfterPayEnabled(state),
    afterPayInProgress: CheckoutSelectors.getIsAfterPayInProgress(state),
    afterPayMinOrderAmount: getAfterPayMinOrderAmount(state),
    afterPayMaxOrderAmount: getAfterPayMaxOrderAmount(state),
    orderTotal: getGrandTotal(state) - getGiftCardsTotal(state),
    cartHaveGCItem: CheckoutSelectors.getIsCartHaveGiftCards(state),
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    submitBilling: (payload) => {
      dispatch(submitBillingSection(payload));
    },
    setVenmoProgress: (payload) => {
      if (!isMobileApp()) dispatch(setVenmoPaymentInProgress(payload));
    },
    setAppleProgress: (payload) => {
      dispatch(setApplePaymentInProgress(payload));
    },
    setAfterPayProgress: (payload) => {
      dispatch(setAfterPayInProgress(payload));
    },
  };
};

GuestBillingContainer.propTypes = {
  cardType: PropTypes.string,
  syncErrors: PropTypes.shape({}),
  cvvCodeRichText: PropTypes.string,
  labels: PropTypes.shape({}),
  paymentMethodId: PropTypes.string,
  shippingAddress: PropTypes.shape({}),
  isSameAsShippingChecked: PropTypes.bool,
  billingData: PropTypes.shape({
    address: PropTypes.shape({
      addressLine1: PropTypes.string,
      addressLine2: PropTypes.string,
      city: PropTypes.string,
      country: PropTypes.string,
      firstName: PropTypes.string,
      lastName: PropTypes.string,
      state: PropTypes.string,
      zipCode: PropTypes.string,
      onFileAddressKey: PropTypes.string,
      onFileAddressId: PropTypes.string,
    }),
    billing: PropTypes.shape({}),
  }),
  orderHasShipping: PropTypes.bool,
  submitBilling: PropTypes.func.isRequired,
  shippingOnFileAddressKey: PropTypes.string,
  navigation: PropTypes.shape({}),
  isVenmoPaymentInProgress: PropTypes.bool,
  isApplePaymentInProgress: PropTypes.bool,
  setVenmoProgress: PropTypes.func.isRequired,
  setAppleProgress: PropTypes.func.isRequired,
  setCheckoutStage: PropTypes.func.isRequired,
  venmoError: PropTypes.string,
  isPayPalWebViewEnable: PropTypes.shape({}).isRequired,
  bagLoading: PropTypes.bool,
  isUSSite: PropTypes.bool,
  selectedCountry: PropTypes.string,
  isPLCCEnabled: PropTypes.bool.isRequired,
  mapboxAutocompleteTypesParam: PropTypes.string,
  trackClickAfterPay: PropTypes.func,
  pageCategory: PropTypes.string,
  afterPayInProgress: PropTypes.bool,
  setAfterPayProgress: PropTypes.func,
  cartHaveGCItem: PropTypes.bool,
};

GuestBillingContainer.defaultProps = {
  cardType: null,
  syncErrors: null,
  cvvCodeRichText: '',
  labels: {},
  paymentMethodId: null,
  shippingAddress: null,
  isSameAsShippingChecked: true,
  billingData: { billing: {} },
  orderHasShipping: true,
  shippingOnFileAddressKey: null,
  navigation: null,
  isVenmoPaymentInProgress: false,
  isApplePaymentInProgress: false,
  venmoError: '',
  bagLoading: false,
  isUSSite: true,
  selectedCountry: '',
  mapboxAutocompleteTypesParam: '',
  trackClickAfterPay: () => {},
  pageCategory: '',
  afterPayInProgress: false,
  setAfterPayProgress: () => {},
  cartHaveGCItem: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(GuestBillingContainer);
export { GuestBillingContainer as GuestBillingContainerVanilla };
