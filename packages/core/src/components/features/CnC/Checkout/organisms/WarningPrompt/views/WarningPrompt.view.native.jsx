// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { string } from 'prop-types';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import {
  WarningWrapper,
  WarningImage,
  WarningImageWrapper,
  ArrowImage,
  ArrowImageWrapper,
  MessageWrapper,
} from '../styles/WarningPrompt.style.native';

const warningImg = require('../../../../../../../../../mobileapp/src/assets/images/circle-warning-fill.png');
const rightArrowImg = require('../../../../../../../../../mobileapp/src/assets/images/caret-black-right.png');

const WarningPrompt = ({ heading, body }) => {
  return (
    <WarningWrapper>
      <WarningImageWrapper>
        <WarningImage height="24px" width="20px" source={warningImg} />
      </WarningImageWrapper>
      <MessageWrapper>
        <BodyCopyWithSpacing
          textAlign="left"
          fontFamily="secondary"
          fontSize="fs14"
          fontWeight="bold"
          text={heading}
          spacingStyles="margin-bottom-XS margin-top-SM"
        />
        <BodyCopyWithSpacing
          textAlign="left"
          fontFamily="secondary"
          fontSize="fs13"
          fontWeight="regular"
          text={body}
          spacingStyles="margin-bottom-SM"
        />
      </MessageWrapper>
      <ArrowImageWrapper>
        <ArrowImage height="22px" width="20px" source={rightArrowImg} />
      </ArrowImageWrapper>
    </WarningWrapper>
  );
};

WarningPrompt.propTypes = {
  heading: string.isRequired,
  body: string.isRequired,
};

WarningPrompt.defaultProps = {};

export default WarningPrompt;
