// 9fbef606107a605d69c0edbcd8029e5d
import {
  onEditCardFocus,
  setFormToEditState,
  unsetPaymentFormEditState,
  handleBillingFormSubmit,
  getPaymentMethods,
} from '../views/BillingPaymentForm.util';

describe('BillingPaymentForm Util', () => {
  const scope = {
    props: {
      dispatch: jest.fn(),
      handleSubmit: jest.fn(),
      labels: { cardEditUnSavedError: 'error' },
      scrollView: { scrollTo: jest.fn() },
    },
    setState: jest.fn(),
    ediCardErrorRef: { current: { scrollIntoView: jest.fn() } },
    state: { editMode: true },
  };
  const labels = {
    paypal: 'Paypal',
    venmo: 'Venmo',
    creditCard: 'Credit Card',
    applePay: 'Apple Pay',
    afterpay: 'afterpay',
  };
  it('should call onEditCardFocus', () => {
    onEditCardFocus(scope);
    expect(scope.props.dispatch).toBeCalled();
  });
  it('should call setFormToEditState', () => {
    setFormToEditState(scope, { preventDefault: jest.fn() });
    expect(scope.setState).toBeCalled();
  });
  it('#unsetPaymentFormEditState', () => {
    unsetPaymentFormEditState(scope, { preventDefault: jest.fn() });
    expect(scope.setState).toBeCalled();
  });
  it('#handleBillingFormSubmit with isMobile false and edit mode true', () => {
    handleBillingFormSubmit(scope, { preventDefault: jest.fn() }, false, {});
    expect(scope.ediCardErrorRef.current.scrollIntoView).toBeCalled();
  });
  it('#handleBillingFormSubmit with isMobile true', () => {
    handleBillingFormSubmit(scope, { preventDefault: jest.fn() }, true, {});
    expect(scope.ediCardErrorRef.current.scrollIntoView).toBeCalled();
  });
  it('#handleBillingFormSubmit with editMode false', () => {
    scope.state.editMode = false;
    handleBillingFormSubmit(scope, { preventDefault: jest.fn() }, false, {});
    expect(scope.props.handleSubmit).toBeCalled();
  });
  it('#onAddCreditCardClick called', () => {
    scope.state.state = true;
    expect(scope.state.state).toBeTruthy();
  });
  it('#getPaymentMethods called without Venmo', () => {
    const response = [
      { id: 'creditCard', displayName: labels.creditCard },
      { id: 'payPal', displayName: labels.paypal },
      { id: 'applepay', displayName: labels.applePay },
    ];
    expect(getPaymentMethods(labels, false)).toEqual(response);
  });
  it('#getPaymentMethods called without Venmo kill switch is disabled and venmo app is not installed', () => {
    const response = [
      { id: 'creditCard', displayName: labels.creditCard },
      { id: 'payPal', displayName: labels.paypal },
      { id: 'applepay', displayName: labels.applePay },
    ];
    expect(getPaymentMethods(labels, false, false)).toEqual(response);
  });
  it('#getPaymentMethods called with Venmo kill switch is enabled and venmo app is installed', () => {
    const response = [
      { id: 'creditCard', displayName: labels.creditCard },
      { id: 'payPal', displayName: labels.paypal },
      { id: 'applepay', displayName: labels.applePay },
      { id: 'venmo', displayName: labels.venmo },
    ];
    expect(getPaymentMethods(labels, true, true)).toEqual(response);
  });

  it('#getPaymentMethods called with Apple Pay Disabled', () => {
    const response = [
      { id: 'creditCard', displayName: labels.creditCard },
      { id: 'payPal', displayName: labels.paypal },
      { id: 'venmo', displayName: labels.venmo },
    ];
    expect(getPaymentMethods(labels, true, true, true, false)).toEqual(response);
  });

  it('#getPaymentMethods called with afterpay enabled', () => {
    const response = [
      { id: 'creditCard', displayName: labels.creditCard },
      { id: 'payPal', displayName: labels.paypal },
      { id: 'afterpay', displayName: labels.afterpay },
      { id: 'venmo', displayName: labels.venmo },
    ];
    expect(getPaymentMethods(labels, true, true, true, false, true)).toEqual(response);
  });
});
