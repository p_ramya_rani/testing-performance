// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ShippingMethodDisplayanilla } from '../views/ShippingMethodDisplay';

describe('ShippingMethodDisplay component', () => {
  const shippingMethod = 'Shipping Method';
  it('should renders correctly props not present', () => {
    const props = { labels: {}, displayName: '' };
    const component = shallow(<ShippingMethodDisplayanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly props are present', () => {
    const props = {
      labels: {
        lbl_review_sectionShippingMethodTitle: shippingMethod,
      },
      displayName: shippingMethod,
    };
    const component = shallow(<ShippingMethodDisplayanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly props are present', () => {
    const props = {
      labels: {
        lbl_review_sectionShippingMethodTitle: shippingMethod,
      },
      displayName: shippingMethod,
      hideSubtitle: false,
    };
    const component = shallow(<ShippingMethodDisplayanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly props are present and hideSubtitle is true', () => {
    const props = {
      labels: {
        lbl_review_sectionShippingMethodTitle: 'Shipping Method',
      },
      displayName: 'Standard - Free',
      hideSubtitle: true,
    };
    const component = shallow(<ShippingMethodDisplayanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
