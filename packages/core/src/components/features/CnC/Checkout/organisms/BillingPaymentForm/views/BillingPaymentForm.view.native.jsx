/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Keyboard } from 'react-native';
import { FormSection, reduxForm, Field } from 'redux-form';
import GenericSkeleton from '@tcp/core/src/components/common/molecules/GenericSkeleton/GenericSkeleton.view.native';

import ApplePaymentButton from '@tcp/core/src/components/common/atoms/ApplePaymentButton';
import Button from '@tcp/core/src/components/common/atoms/Button';
import Image from '@tcp/core/src/components/common/atoms/Image';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import constants from '../container/CreditCard.constants';
import getCvvInfo from '../../../molecules/CVVInfo';
import AddNewCCForm from '../../AddNewCCForm';

import AddressFields from '../../../../../../common/molecules/AddressFields';

import { propTypes, defaultProps, onCCDropUpdateChange } from './BillingPaymentForm.view.util';
import {
  getExpirationRequiredFlag,
  getCreditCardList,
  getSelectedCard,
} from '../../../util/utility';
import CnCTemplate from '../../../../common/organism/CnCTemplate';
import CONSTANTS from '../../../Checkout.constants';
import PaymentMethods from '../../../../common/molecules/PaymentMethods';
import CreditCardDropdown from './CreditCardDropDown.view.native';
import { CardImage } from '../../../../../../common/molecules/Card/views/CardImage.native';
import {
  CvvCode,
  CvvTextboxStyle,
  CVVInfo,
  BillingAddressWrapper,
  PaymentMethodWrapper,
  PaymentMethodHeader,
  SubHeader,
  CreditCardHeader,
  CreditCardWrapper,
  PayPalTextContainer,
  ApplePayTextContainer,
  PaymentMethodMainWrapper,
  PaymentMethodImage,
  SkeletonWrapper,
  ErrorWrapper,
  AfterPayThresholdMsg,
  AfterPayThresholdMsgTxt,
  AfterPayContainer,
} from '../styles/BillingPaymentForm.style.native';

import TextBox from '../../../../../../common/atoms/TextBox';
import {
  getCardDetailsMethod,
  getDefaultPayment,
  getBillingAddressWrapper,
  getCheckoutBillingAddress,
  scrollToFirstErrorBillingPayment,
  createRefParent,
} from './BillingPaymentForm.view.native.util';
import CardEditFrom from './CardEditForm.view.native';
import {
  onEditCardFocus,
  setFormToEditState,
  unsetPaymentFormEditState,
  handleBillingFormSubmit,
  getPaymentMethods,
  onAddCreditCardClick,
  onCCDropDownChange,
  renderAddressError,
  showVenmoButton,
} from './BillingPaymentForm.util';
import CCskeleton from './CCskeleton.native';

const triangleWarning = require('../../../../../../../../../mobileapp/src/assets/images/triangle-warning.png');

export class BillingPaymentForm extends React.PureComponent {
  static propTypes = propTypes;

  static defaultProps = defaultProps;

  constructor(props) {
    super(props);
    this.state = { addNewCCState: false, editMode: false, editModeSubmissionError: '' };
    this.ediCardErrorRef = React.createRef();
  }

  /**
   * @function getAddNewCCForm
   * @description returns the add new credit card form
   */
  getAddNewCCForm = ({ onCardFocus, editMode, change: formChange, setCardType } = {}) => {
    const { cardList, onFileCardKey, defaultSaveToAccount, keyBoardAvoidRef } = this.props;
    const { isEditFormSameAsShippingChecked, isSameAsShippingChecked, isPLCCEnabled } = this.props;
    const {
      cvvCodeRichText,
      cardType,
      labels,
      syncErrorsObj,
      isGuest,
      dispatch,
      dispatchFieldChangeIfNotThere,
    } = this.props;
    const { isSaveToAccountChecked, billingData, creditFieldLabels, defaultEditCardType } =
      this.props;
    const { infoIconText } = labels;
    let cvvError;
    /* istanbul ignore else */
    if (syncErrorsObj) {
      cvvError = syncErrorsObj.syncError.cvvCode;
    }
    const formCardType = editMode ? defaultEditCardType : cardType;
    const isExpirationRequired = getExpirationRequiredFlag({ cardType: formCardType });
    const { addNewCCState } = this.state;
    const formName = editMode ? constants.EDIT_FORM_NAME : constants.FORM_NAME;
    if (setCardType !== false) {
      dispatchFieldChangeIfNotThere({ formName, field: 'cardType', value: formCardType });
    }
    dispatchFieldChangeIfNotThere({
      formName,
      field: 'isPLCCEnabled',
      value: isPLCCEnabled,
    });
    dispatchFieldChangeIfNotThere({ formName, field: 'creditCardId', value: onFileCardKey });
    const creditCardList = getCreditCardList({ cardList });
    const selectedCard = onFileCardKey ? getSelectedCard({ creditCardList, onFileCardKey }) : '';
    return (
      <AddNewCCForm
        cvvInfo={getCvvInfo({ cvvCodeRichText, infoIconText })}
        cardType={formCardType}
        defaultSaveToAccount={defaultSaveToAccount}
        cvvError={cvvError}
        labels={labels}
        isGuest={isGuest}
        isSaveToAccountChecked={isSaveToAccountChecked}
        formName={formName}
        dispatch={dispatch}
        isExpirationRequired={isExpirationRequired}
        billingData={billingData}
        addNewCCState={addNewCCState}
        creditFieldLabels={creditFieldLabels}
        editMode={editMode}
        onCardFocus={onCardFocus}
        isSameAsShippingChecked={
          editMode ? isEditFormSameAsShippingChecked : isSameAsShippingChecked
        }
        selectedCard={selectedCard}
        createRefParent={(...params) => createRefParent(...params, keyBoardAvoidRef)}
        change={formChange}
      />
    );
  };

  getPaymentMethod = (labels, selectedCard, cvvCodeRichText) => {
    return (
      <>
        {labels.paymentMethod ? (
          <SubHeader>
            <BodyCopy
              mobileFontFamily="primary"
              fontSize="fs16"
              fontWeight="extrabold"
              dataLocator="paymentMethodLbl"
              text={labels.paymentMethod}
              color="gray.900"
            />
          </SubHeader>
        ) : null}
        {labels.creditCardEnd ? this.renderCVVField(labels, selectedCard, cvvCodeRichText) : null}
      </>
    );
  };

  renderCVVField = (labels, selectedCard, cvvCodeRichText) => {
    const { keyBoardAvoidRef } = this.props;
    return (
      <PaymentMethodWrapper>
        {labels.creditCardEnd ? (
          <PaymentMethodImage>
            <CardImage
              card={selectedCard}
              cardNumber={`${labels.creditCardEnd}${selectedCard.accountNo.slice(-4)}`}
            />
          </PaymentMethodImage>
        ) : null}
        {labels.cvvCode && selectedCard.ccType !== constants.ACCEPTED_CREDIT_CARDS.PLACE_CARD ? (
          <CvvCode>
            <Field
              label={labels.cvvCode}
              name="cvvCode"
              id="cvvCode"
              type="text"
              component={TextBox}
              keyboardType="numeric"
              dataLocator="cvvTxtBox"
              customStyle={CvvTextboxStyle}
              inputRef={(node) => createRefParent(node, 'cvvCode', keyBoardAvoidRef)}
              isCVVFieldError
            />
            {this.getCvvInfoIcon(cvvCodeRichText, labels.infoIconText)}
          </CvvCode>
        ) : null}
      </PaymentMethodWrapper>
    );
  };

  getCvvInfoIcon = (cvvCodeRichText, infoIconText) => {
    return <CVVInfo>{getCvvInfo({ cvvCodeRichText, infoIconText })}</CVVInfo>;
  };

  unsetFormEditState = (e) => {
    unsetPaymentFormEditState(this, e);
  };

  submitButtonClicked = (e, afterPayInProgress) => {
    const { trackClickAfterPay } = this.props;
    // Hide that keyboard!
    Keyboard.dismiss();
    handleBillingFormSubmit(this, e, true, {
      trackClickAfterPay,
      afterPayInProgress,
      pageCategory: 'billing',
    });
  };

  getCheckoutBillingAddressEdit = (edit) => {
    return getCheckoutBillingAddress(this, edit);
  };

  getCCDropDown = ({
    labels,
    creditCardList,
    onFileCardKey,
    selectedCard,
    dispatch,
    cvvCodeRichText,
  }) => {
    const { addNewCCState, editMode, editModeSubmissionError } = this.state;
    const isCardDetailEdit = selectedCard && !editMode;
    const { getAddNewCCForm, unsetFormEditState } = this;
    const { updateCardDetail, toastMessage, isPLCCEnabled, defaultEditCardType } = this.props;
    const isSpace = selectedCard && selectedCard.defaultInd ? selectedCard.defaultInd : false;

    return (
      <BillingAddressWrapper>
        <CreditCardWrapper pointerEvents={editMode ? 'none' : 'auto'}>
          <CreditCardHeader>
            <BodyCopy
              mobileFontFamily="primary"
              fontSize="fs10"
              fontWeight="extrabold"
              dataLocator="cardDropDownLbl"
              text={labels.selectFromCard}
            />
          </CreditCardHeader>
          <CreditCardDropdown
            creditCardList={creditCardList}
            labels={labels}
            onFileCardKey={onFileCardKey}
            addNewCC={() => onAddCreditCardClick(this)}
            selectedOnFileCardKey={selectedCard && selectedCard.creditCardId}
            dispatch={dispatch}
            formName={constants.FORM_NAME}
            addNewCCState={addNewCCState}
            onChange={() => onCCDropDownChange(this)}
          />
        </CreditCardWrapper>
        {selectedCard ? (
          getCardDetailsMethod(labels, setFormToEditState, editMode, this)
        ) : (
          <>
            {this.getAddNewCCForm()}
            {getCheckoutBillingAddress(this)}
          </>
        )}
        {isCardDetailEdit ? (
          <>
            {this.getPaymentMethod(labels, selectedCard, cvvCodeRichText)}
            {getDefaultPayment(selectedCard, labels, !isSpace)}
            {getBillingAddressWrapper(selectedCard, onFileCardKey, labels)}
          </>
        ) : null}

        {editMode ? (
          <CardEditFrom
            {...{ selectedCard, unsetFormEditState, getAddNewCCForm }}
            {...{
              onEditCardFocus,
              dispatch,
              labels,
              updateCardDetail,
              editModeSubmissionError,
            }}
            key="cardEditForm"
            setCardType={false}
            cardType={defaultEditCardType}
            addressForm={this.getCheckoutBillingAddressEdit}
            errorMessageRef={this.ediCardErrorRef}
            {...{ getDefaultPayment, toastMessage }}
            isPLCCEnabled={isPLCCEnabled}
          />
        ) : null}
      </BillingAddressWrapper>
    );
  };

  addNewBillingInfoForm = () => {
    const { cardList, labels, dispatch, onFileCardKey } = this.props;
    const creditCardList = getCreditCardList({ cardList });
    return (
      <>
        {creditCardList && creditCardList.size > 0 ? (
          this.getCCDropDown({ labels, creditCardList, onFileCardKey, dispatch })
        ) : (
          <>
            {this.getAddNewCCForm()}
            {getCheckoutBillingAddress(this)}
          </>
        )}
      </>
    );
  };

  getCreditListView = ({
    labels,
    creditCardList,
    onFileCardKey,
    selectedCard,
    dispatch,
    cvvCodeRichText,
  }) => {
    return (
      <>
        {creditCardList && creditCardList.size > 0
          ? this.getCCDropDown({
              labels,
              creditCardList,
              onFileCardKey,
              selectedCard,
              dispatch,
              cvvCodeRichText,
            })
          : this.getAddNewCCForm()}
      </>
    );
  };

  getCreditCardWrapper = ({ labels, creditCardList, cvvCodeRichText, onFileCardKey, dispatch }) => {
    const selectedCard = onFileCardKey ? getSelectedCard({ creditCardList, onFileCardKey }) : '';
    const { addNewCCState } = this.state;
    if (this.onFileCardKey !== onFileCardKey) {
      this.onFileCardKey = onFileCardKey;
      onCCDropUpdateChange(onFileCardKey, selectedCard, dispatch);
    }
    return (
      <>
        {creditCardList && creditCardList.size > 0 && !addNewCCState
          ? this.getCreditListView({
              labels,
              cvvCodeRichText,
              creditCardList,
              onFileCardKey,
              selectedCard,
              dispatch,
            })
          : this.addNewBillingInfoForm()}
        <ErrorWrapper>{renderAddressError(this)}</ErrorWrapper>
      </>
    );
  };

  getAfterPaySubmitButton = ({ paymentMethodId, labels, cartHaveGCItem }) => {
    const { afterPayMinOrderAmount, orderTotal, afterPayMaxOrderAmount = 1000 } = this.props;
    let orderTotalThresholdMessage =
      labels.orderTotalThresholdMessage &&
      labels.orderTotalThresholdMessage.replace('[AfterPayMinOrderAmount]', afterPayMinOrderAmount);
    orderTotalThresholdMessage =
      orderTotalThresholdMessage &&
      orderTotalThresholdMessage.replace('[AfterPayMaxOrderAmount]', afterPayMaxOrderAmount);
    return paymentMethodId === constants.PAYMENT_METHOD_AFTERPAY ? (
      <AfterPayContainer>
        {afterPayMinOrderAmount <= orderTotal &&
        afterPayMaxOrderAmount >= orderTotal &&
        !cartHaveGCItem ? (
          <>
            <Button
              type="submit"
              fontSize="fs14"
              fontWeight="extrabold"
              buttonVariation="variable-width"
              fill="BLUE"
              value={constants.PAYMENT_METHOD_AFTERPAY}
              fullWidth
              onPress={(e) => this.submitButtonClicked(e, true)}
              text={labels.afterpaySubmit}
            />
            <BodyCopyWithSpacing
              fontFamily="secondary"
              fontSize="fs16"
              fontWeight="regular"
              text={labels.afterPayDefaultMessage}
              spacingStyles="margin-top-MED"
            />
          </>
        ) : (
          <AfterPayThresholdMsg>
            <Image source={triangleWarning} height="25px" width="25px" />
            <AfterPayThresholdMsgTxt>
              <BodyCopyWithSpacing
                spacingStyles="margin-left-SM"
                fontFamily="secondary"
                fontSize="fs16"
                fontWeight="extrabold"
                text={cartHaveGCItem ? labels.cartHasGCMessage : orderTotalThresholdMessage}
              />
            </AfterPayThresholdMsgTxt>
          </AfterPayThresholdMsg>
        )}
      </AfterPayContainer>
    ) : null;
  };

  /*
    /**
    * @function render
    * @description render method to be called of component
    */
  // eslint-disable-next-line complexity
  render() {
    const {
      cardList,
      onFileCardKey,
      labels,
      cvvCodeRichText,
      paymentMethodId,
      orderHasShipping,
      backLinkPickup,
      backLinkShipping,
      nextSubmitText,
      navigation,
      dispatch,
      isPaymentDisabled,
      setCheckoutStage,
      getPayPalSettings,
      isPayPalWebViewEnable,
      isPayPalEnabled,
      bagLoading,
      isVenmoEnabled,
      isBonusPointsEnabled,
      onVenmoError,
      isVenmoAppInstalled,
      isApplePayEnabled,
      isApplePayEnabledOnBilling,
      isAfterPayEnabled,
      cartHaveGCItem,
    } = this.props;
    const paymentMethods = getPaymentMethods(
      labels,
      isVenmoEnabled,
      isVenmoAppInstalled,
      isPayPalEnabled,
      isApplePayEnabled,
      isAfterPayEnabled
    );
    const creditCardList = getCreditCardList({ cardList });

    return (
      <>
        {!isPaymentDisabled && (
          <PaymentMethodMainWrapper>
            <PaymentMethodHeader>
              <BodyCopy
                fontFamily="primary"
                fontSize="fs26"
                fontWeight="regular"
                spacingStyles="margin-bottom-MED"
                color="gray.900"
                dataLocator="paymentMethodLbl"
                text={labels.paymentMethod}
              />
            </PaymentMethodHeader>

            <FormSection name="shipmentMethods">
              <PaymentMethods
                paymentMethods={paymentMethods}
                formName={constants.FORM_NAME}
                selectedPaymentId={paymentMethodId}
                dispatch={dispatch}
                isApplePayEnabled={isApplePayEnabled}
                isApplePayEnabledOnBilling={isApplePayEnabledOnBilling}
                venmoId={constants.PAYMENT_METHOD_VENMO}
                labels={labels}
                isAfterPayEnabled={isAfterPayEnabled}
                isVenmoEnabled={isVenmoEnabled}
                isPayPalEnabled={isPayPalEnabled}
              />
            </FormSection>
            {this.getAfterPaySubmitButton({ paymentMethodId, labels, cartHaveGCItem })}
            {isApplePayEnabled && paymentMethodId === constants.PAYMENT_METHOD_APPLE_PAY ? (
              <ApplePayTextContainer>
                <ApplePaymentButton navigation={navigation} isBillingPage />
              </ApplePayTextContainer>
            ) : null}

            {isPayPalEnabled && paymentMethodId === constants.PAYMENT_METHOD_PAY_PAL ? (
              <PayPalTextContainer>
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs16"
                  spacingStyles="margin-bottom-MED"
                  color="gray.900"
                  dataLocator="paymentMethodLbl"
                  text={labels.payPalLongText}
                />
              </PayPalTextContainer>
            ) : null}
            {!bagLoading ? (
              <>
                {paymentMethodId === constants.PAYMENT_METHOD_VENMO && isVenmoEnabled && (
                  <PayPalTextContainer>
                    <BodyCopy
                      fontFamily="secondary"
                      fontSize="fs16"
                      spacingStyles="margin-bottom-MED"
                      color="gray.900"
                      dataLocator="venmoLabelText"
                      text={labels.venmoLongText}
                    />
                  </PayPalTextContainer>
                )}
                {paymentMethodId === constants.PAYMENT_METHOD_CREDIT_CARD ? (
                  this.getCreditCardWrapper({
                    labels,
                    creditCardList,
                    cvvCodeRichText,
                    onFileCardKey,
                    dispatch,
                  })
                ) : (
                  <SubHeader />
                )}
              </>
            ) : (
              <>
                <CCskeleton />
                <SkeletonWrapper>
                  <GenericSkeleton />
                </SkeletonWrapper>
              </>
            )}
          </PaymentMethodMainWrapper>
        )}
        <CnCTemplate
          navigation={navigation}
          btnText={nextSubmitText}
          routeToPage=""
          onPress={(e) => this.submitButtonClicked(e)}
          backLinkText={orderHasShipping ? backLinkShipping : backLinkPickup}
          onBackLinkPress={() =>
            orderHasShipping
              ? setCheckoutStage(CONSTANTS.SHIPPING_DEFAULT_PARAM)
              : setCheckoutStage(CONSTANTS.PICKUP_DEFAULT_PARAM)
          }
          pageCategory="billing"
          showAccordian
          getPayPalSettings={getPayPalSettings}
          showPayPalButton={isPayPalEnabled && paymentMethodId === constants.PAYMENT_METHOD_PAY_PAL}
          isPayPalWebViewEnable={isPayPalWebViewEnable}
          showVenmoSubmit={showVenmoButton(this)}
          continueWithText={labels.continueWith}
          onVenmoSubmit={(e) => this.submitButtonClicked(e)}
          onVenmoError={onVenmoError}
          isBonusPointsEnabled={isBonusPointsEnabled}
          bagLoading={bagLoading}
          pageName="checkout"
          pageSection="billing"
          isAfterPaySelected={
            isAfterPayEnabled && paymentMethodId === CONSTANTS.PAYMENT_METHOD_AFTERPAY
          }
        />
      </>
    );
  }
}

const validateMethod = createValidateMethod({
  address: AddressFields.addressValidationConfig,
  ...getStandardConfig(['cardNumber', 'cvvCode', 'expYear', 'expMonth']),
});

export default reduxForm({
  form: constants.FORM_NAME, // a unique identifier for this form
  enableReinitialize: true,
  shouldValidate: () => true,
  ...validateMethod,
  onSubmitFail: scrollToFirstErrorBillingPayment,
})(BillingPaymentForm);
export { BillingPaymentForm as BillingPaymentFormVanilla };
