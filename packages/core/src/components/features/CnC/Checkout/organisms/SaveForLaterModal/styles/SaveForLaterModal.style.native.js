// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const SaveLaterWrapper = styled.View`
  display: flex;
  flex-direction: column;
  margin: 20px;
  margin-top: 4px;
`;

const SaveLaterButtonWrapper = styled.View`
  align-items: center;
  margin: 6px 0;
`;

const SHADOW = `
  shadow-radius: 2px;
  shadow-color: ${props => props.theme.colorPalette.black};
  shadow-offset: 4px;
  elevation: 2;
`;

const ItemWrapper = styled.View`
  display: flex;
  flex-direction: row;
  flex: 1;
  margin-top: 10px;
  border: 1px solid gray;
  border-left-width: 0px;
  border-right-width: 0px;
  ${SHADOW}
`;

const ItemListWrapper = styled.View`
  display: flex;
  margin-bottom: 30px;
`;

const ItemImageWrapper = styled.View`
  flex: 0.3;
  align-items: center;
  padding: 10px;
  padding-left: 0px;
`;

const ItemTextWrapper = styled.View`
  flex: 0.6;
  align-items: flex-start;
  padding: 10px;
  padding-left: 0px;
  text-align: left;
  justify-content: center;
`;

export {
  SaveLaterWrapper,
  SaveLaterButtonWrapper,
  ItemWrapper,
  ItemImageWrapper,
  ItemTextWrapper,
  ItemListWrapper,
};
