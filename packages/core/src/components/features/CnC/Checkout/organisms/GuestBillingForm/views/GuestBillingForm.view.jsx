// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, change } from 'redux-form';
import { getIconPath } from '@tcp/core/src/utils';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import AddNewCCForm from '../../AddNewCCForm';
import cvvInfo from '../../../molecules/CVVInfo';
import PaymentMethods from '../../../../common/molecules/PaymentMethods';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import { Row, Col, Button, Image } from '../../../../../../common/atoms';
import CONSTANTS, { CHECKOUT_ROUTES } from '../../../Checkout.constants';
import CheckoutBillingAddress from '../../CheckoutBillingAddress';
import AddressFields from '../../../../../../common/molecules/AddressFields';
import CheckoutFooter from '../../../molecules/CheckoutFooter';
import utility, { scrollToFirstError, getExpirationRequiredFlag } from '../../../util/utility';
import VenmoPaymentButton from '../../../../../../common/atoms/VenmoPaymentButton';
import ApplePaymentButton from '../../../../../../common/atoms/ApplePaymentButton';
import CheckoutOrderInfo from '../../../molecules/CheckoutOrderInfoMobile';
import BillingPayPalButton from '../../BillingPayPalButton';
import ErrorMessage from '../../../../common/molecules/ErrorMessage';
import GenericSkeleton from '../../../../../../common/molecules/GenericSkeleton/GenericSkeleton.view';
import { renderAddressError } from '../../BillingPaymentForm/views/BillingPaymentForm.view.util';

class GuestBillingForm extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    shippingAddress: PropTypes.shape({}),
    className: PropTypes.string.isRequired,
    cvvCodeRichText: PropTypes.string,
    cardType: PropTypes.string,
    syncErrorsObj: PropTypes.shape({
      syncError: PropTypes.shape({
        cvvCode: PropTypes.string,
      }),
    }),
    labels: PropTypes.shape({
      paymentMethod: PropTypes.string,
      continueWith: PropTypes.string,
    }),
    paymentMethodId: PropTypes.string,
    orderHasShipping: PropTypes.bool,
    addressLabels: PropTypes.shape({}).isRequired,
    isGuest: PropTypes.bool,
    isSameAsShippingChecked: PropTypes.bool,
    nextSubmitText: PropTypes.string,
    backLinkShipping: PropTypes.string,
    backLinkPickup: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    billingData: PropTypes.shape({}),
    creditFieldLabels: PropTypes.shape({}),
    showAccordian: PropTypes.bool,
    isPayPalEnabled: PropTypes.bool,
    isVenmoEnabled: PropTypes.bool, // Venmo Kill Switch, if Venmo enabled then true, else false.
    isApplePayEnabled: PropTypes.bool.isRequired,
    isApplePayEnabledOnBilling: PropTypes.bool.isRequired,
    isPaymentDisabled: PropTypes.bool,
    pageCategory: PropTypes.string,
    venmoError: PropTypes.string,
    isPayPalWebViewEnable: PropTypes.bool,
    bagLoading: PropTypes.bool,
    isUSSite: PropTypes.bool,
    isPLCCEnabled: PropTypes.bool.isRequired,
    zipCodeABTestEnabled: PropTypes.bool,
    getZipCodeSuggestedAddress: PropTypes.func,
    zipCodeLoader: PropTypes.bool,
    mapboxAutocompleteTypesParam: PropTypes.string,
    isAfterPayEnabled: PropTypes.bool,
    afterPayMinOrderAmount: PropTypes.string,
    afterPayMaxOrderAmount: PropTypes.string,
    orderTotal: PropTypes.string,
    trackClickAfterPay: PropTypes.func,
    cartHaveGCItem: PropTypes.bool,
  };

  static defaultProps = {
    shippingAddress: null,
    cvvCodeRichText: '',
    cardType: null,
    syncErrorsObj: {
      syncError: {
        cvvCode: '',
      },
    },
    labels: {
      paymentMethod: '',
      continueWith: '',
    },
    paymentMethodId: null,
    orderHasShipping: true,
    isGuest: true,
    isSameAsShippingChecked: true,
    billingData: {},
    nextSubmitText: '',
    backLinkShipping: '',
    backLinkPickup: '',
    creditFieldLabels: {},
    isPayPalEnabled: false,
    showAccordian: true,
    isVenmoEnabled: false,
    isPaymentDisabled: false,
    pageCategory: '',
    venmoError: '',
    isPayPalWebViewEnable: false,
    bagLoading: false,
    isUSSite: true,
    zipCodeABTestEnabled: false,
    getZipCodeSuggestedAddress: () => {},
    zipCodeLoader: false,
    mapboxAutocompleteTypesParam: '',
    isAfterPayEnabled: false,
    afterPayMinOrderAmount: '0',
    afterPayMaxOrderAmount: '1000',
    orderTotal: '0',
    trackClickAfterPay: () => {},
    cartHaveGCItem: false,
  };

  componentDidUpdate(prevProp) {
    const { cardType: prevCardType } = prevProp;
    const { cardType, dispatch } = this.props;
    /* istanbul ignore else */
    if (prevCardType !== cardType) {
      dispatch(change('checkoutBilling', 'cardType', cardType));
    }
  }

  renderAppleButton = ({ paymentMethodId, isApplePayEnabled }) => {
    return (
      paymentMethodId === CONSTANTS.PAYMENT_METHOD_APPLE &&
      isApplePayEnabled && (
        <ApplePaymentButton className="apple-container" buttonClass="apple-pay-button-black" />
      )
    );
  };

  renderBillingPayPalButton = () => {
    const { isPayPalEnabled, paymentMethodId, labels } = this.props;
    return isPayPalEnabled && paymentMethodId === CONSTANTS.PAYMENT_METHOD_PAYPAL ? (
      <BillingPayPalButton labels={labels} containerId="billing-page-paypal-one" />
    ) : null;
  };

  /**
   * @description - Show Venmo if payment method is Venmo and Payments tab is enabled
   */
  showVenmoButton = () => {
    const { paymentMethodId, isPaymentDisabled } = this.props;
    return paymentMethodId === CONSTANTS.PAYMENT_METHOD_VENMO && !isPaymentDisabled;
  };

  getAfterPaySubmitButton = ({ paymentMethodId, labels, cartHaveGCItem }) => {
    const { afterPayMinOrderAmount, afterPayMaxOrderAmount, orderTotal } = this.props;
    let orderTotalThresholdMessage = labels.orderTotalThresholdMessage.replace(
      '[AfterPayMinOrderAmount]',
      afterPayMinOrderAmount
    );
    orderTotalThresholdMessage = orderTotalThresholdMessage.replace(
      '[AfterPayMaxOrderAmount]',
      afterPayMaxOrderAmount
    );
    return (
      paymentMethodId === CONSTANTS.PAYMENT_METHOD_AFTERPAY && (
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 5, large: 5 }}>
            {afterPayMinOrderAmount <= orderTotal &&
            afterPayMaxOrderAmount >= orderTotal &&
            !cartHaveGCItem ? (
              <>
                <Button
                  aria-label={CONSTANTS.PAYMENT_METHOD_AFTERPAY}
                  type="submit"
                  fontSize="fs14"
                  fontWeight="extrabold"
                  buttonVariation="variable-width"
                  fill="BLUE"
                  dataLocator="reviewBtn"
                  value={CONSTANTS.PAYMENT_METHOD_AFTERPAY}
                  fullWidth
                >
                  {labels.afterpaySubmit}
                </Button>
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs16"
                  fontWeight="regular"
                  className="elem-mt-MED"
                >
                  {labels.afterPayDefaultMessage}
                </BodyCopy>
              </>
            ) : (
              <div className="afterPay_threshold">
                <Image
                  alt="triangle-warning"
                  src={getIconPath('triangle-warning')}
                  height="25px"
                  width="25px"
                />
                <BodyCopy
                  className="elem-ml-SM"
                  fontFamily="secondary"
                  fontSize="fs16"
                  fontWeight="extrabold"
                >
                  {cartHaveGCItem ? labels.cartHasGCMessage : orderTotalThresholdMessage}
                </BodyCopy>
              </div>
            )}
          </Col>
        </Row>
      )
    );
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      cvvCodeRichText,
      cardType,
      syncErrorsObj,
      className,
      labels,
      paymentMethodId,
      isGuest,
      orderHasShipping,
      addressLabels,
      dispatch,
      shippingAddress,
      isSameAsShippingChecked,
      nextSubmitText,
      backLinkShipping,
      backLinkPickup,
      handleSubmit,
      billingData,
      creditFieldLabels,
      showAccordian,
      isPayPalEnabled,
      isVenmoEnabled,
      isApplePayEnabled,
      isApplePayEnabledOnBilling,
      isPaymentDisabled,
      venmoError,
      pageCategory,
      isPayPalWebViewEnable,
      bagLoading,
      isUSSite,
      isPLCCEnabled,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      mapboxAutocompleteTypesParam,
      isAfterPayEnabled,
      trackClickAfterPay,
      cartHaveGCItem,
    } = this.props;
    let cvvError;
    if (syncErrorsObj) {
      cvvError = syncErrorsObj.syncError.cvvCode;
    }
    const isExpirationRequired = getExpirationRequiredFlag({ cardType });
    return (
      <form
        className={className}
        name={CONSTANTS.CHECKOUT_BILLING}
        id={CONSTANTS.CHECKOUT_BILLING}
        onSubmit={handleSubmit}
      >
        {!isPaymentDisabled && (
          <>
            <BodyCopy
              fontFamily="primary"
              fontSize="fs28"
              fontWeight="regular"
              dataLocator="paymentMethodLbl"
              className="elem-mb-XS elem-mt-MED"
            >
              {labels.paymentMethod}
            </BodyCopy>
            <PaymentMethods
              labels={labels}
              isVenmoEnabled={isVenmoEnabled}
              isApplePayEnabled={isApplePayEnabled}
              isUSSite={isUSSite}
              isPayPalEnabled={isPayPalEnabled}
              isApplePayEnabledOnBilling={isApplePayEnabledOnBilling}
              isAfterPayEnabled={isAfterPayEnabled}
            />

            <div className="elem-mt-LRG elem-pb-XL">
              {paymentMethodId === CONSTANTS.PAYMENT_METHOD_CREDIT_CARD ? (
                <>
                  <AddNewCCForm
                    cvvInfo={cvvInfo({ cvvCodeRichText, infoIconText: labels.infoIconText })}
                    cardType={cardType}
                    cvvError={cvvError}
                    labels={labels}
                    formName="checkoutBilling"
                    isExpirationRequired={isExpirationRequired}
                    isGuest={isGuest}
                    creditFieldLabels={creditFieldLabels}
                    isPLCCEnabled={isPLCCEnabled}
                  />
                  {!bagLoading ? (
                    <>
                      <CheckoutBillingAddress
                        isGuest={isGuest}
                        orderHasShipping={orderHasShipping}
                        addressLabels={addressLabels}
                        dispatch={dispatch}
                        shippingAddress={shippingAddress}
                        isSameAsShippingChecked={isSameAsShippingChecked}
                        labels={labels}
                        billingData={billingData}
                        formName="checkoutBilling"
                        zipCodeABTestEnabled={zipCodeABTestEnabled}
                        getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                        zipCodeLoader={zipCodeLoader}
                        mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
                      />
                      <div className="errorWrapper">{renderAddressError(this, true)}</div>
                    </>
                  ) : (
                    <GenericSkeleton />
                  )}
                </>
              ) : null}
              <Row fullBleed>
                <Col colSize={{ small: 6, medium: 5, large: 5 }}>
                  {this.renderAppleButton({ paymentMethodId, isApplePayEnabled })}
                </Col>
              </Row>
              <Row fullBleed>
                <Col colSize={{ small: 6, medium: 5, large: 5 }}>
                  {this.renderBillingPayPalButton()}
                </Col>
              </Row>
              {paymentMethodId === CONSTANTS.PAYMENT_METHOD_VENMO && isVenmoEnabled && (
                <VenmoPaymentButton
                  className="venmo-container"
                  continueWithText={labels.continueWith}
                  onSuccess={handleSubmit}
                  isVenmoBlueButton
                />
              )}
              {this.getAfterPaySubmitButton({ paymentMethodId, labels, cartHaveGCItem })}
              {venmoError && <ErrorMessage error={venmoError} className="checkout-page-error" />}
            </div>
          </>
        )}
        <CheckoutOrderInfo
          isGuest={isGuest}
          showAccordian={showAccordian}
          pageCategory={pageCategory}
          isCheckoutFlow
        />
        <CheckoutFooter
          hideBackLink
          backLinkHandler={() =>
            orderHasShipping
              ? utility.routeToPage(CHECKOUT_ROUTES.shippingPage)
              : utility.routeToPage(CHECKOUT_ROUTES.pickupPage)
          }
          nextButtonText={nextSubmitText}
          backLinkText={orderHasShipping ? backLinkShipping : backLinkPickup}
          showVenmoSubmit={this.showVenmoButton()}
          showPayPalButton={isPayPalEnabled && paymentMethodId === CONSTANTS.PAYMENT_METHOD_PAYPAL}
          showAfterPayButton={
            isAfterPayEnabled && paymentMethodId === CONSTANTS.PAYMENT_METHOD_AFTERPAY
          }
          showApplePayButton={
            isApplePayEnabled && paymentMethodId === CONSTANTS.PAYMENT_METHOD_APPLE
          }
          continueWithText={labels.continueWith}
          onVenmoSubmit={handleSubmit}
          venmoError={venmoError}
          isPayPalWebViewEnable={isPayPalWebViewEnable}
          bagLoading={bagLoading}
          trackClickAfterPay={trackClickAfterPay}
          submitForm={CONSTANTS.CHECKOUT_BILLING}
        />
      </form>
    );
  }
}

const validateMethod = createValidateMethod({
  address: AddressFields.addressValidationConfig,
  ...getStandardConfig(['cardNumber', 'cvvCode', 'expYear', 'expMonth']),
});
export default reduxForm({
  form: CONSTANTS.CHECKOUT_BILLING, // a unique identifier for this form
  enableReinitialize: true,
  shouldValidate: () => true,
  ...validateMethod,
  onSubmitFail: (errors) => scrollToFirstError(errors),
})(GuestBillingForm);
export { GuestBillingForm as GuestBillingFormVanilla };
