// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';
import SaveForLaterModal from '@tcp/core/src/components/features/CnC/Checkout/organisms/SaveForLaterModal/views/SaveForLaterModal.view.native';
import WarningPrompt from '@tcp/core/src/components/features/CnC/Checkout/organisms/WarningPrompt/views/WarningPrompt.view.native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  getSfsErroMessage,
  hasApoFpoError,
  getSaveForLaterItemsOrderItemId,
  getSaveForLaterItemsCatEntryId,
  hasGiftServiceError,
  hasRushShippingError,
} from '@tcp/core/src/components/features/CnC/Checkout/util/utility';
import CONSTANTS from '../../../../../Checkout.constants';

const { SFS_ERROR_TYPES } = CONSTANTS;

const setSaveForLaterModalState = (instance, val) =>
  instance.setState({ saveForLaterModalState: val });

export const nextCTAText = (labels, isVenmoPaymentInProgress, isVenmoShippingDisplayed) => {
  return isVenmoPaymentInProgress && !isVenmoShippingDisplayed
    ? getLabelValue(labels, 'lbl_shipping_reviewText', 'shipping', 'checkout')
    : getLabelValue(labels, 'lbl_shipping_billingText', 'shipping', 'checkout');
};

export const toggleSaveLaterModal = (type = SFS_ERROR_TYPES.SHIPPING_ADDRESS, instance) => {
  const { saveForLaterModalState } = instance.state;
  instance.setState({ sfsErrorType: type });
  setSaveForLaterModalState(instance, !saveForLaterModalState);
};

export const getMessageBodyText = (labels, checkoutServerError, itemsCount) =>
  getLabelValue(labels, getSfsErroMessage(checkoutServerError), 'shipping', 'checkout').replace(
    'X',
    itemsCount
  );

export const clearServerError = instance => {
  const { checkoutServerError, clearCheckoutServerError } = instance.props;
  if (checkoutServerError && clearCheckoutServerError) {
    clearCheckoutServerError({});
  }
};

export const showSaveForLaterModal = (instance, saveForLaterModalState, sfsErrorType) => {
  const { cartOrderItems, checkoutServerError, addItemToSflList, labels } = instance.props;
  if (saveForLaterModalState && checkoutServerError && checkoutServerError.billingError) {
    const giftServiceError = hasGiftServiceError(checkoutServerError);
    const rushShippingError = hasRushShippingError(checkoutServerError);
    const catEntryId = getSaveForLaterItemsCatEntryId(checkoutServerError) || [];
    const itemIds = getSaveForLaterItemsOrderItemId(checkoutServerError) || [];
    const afterSuccessCallback = () => {
      instance.afterSflSuccessCallback();
    };
    const headerLabelKey =
      giftServiceError || rushShippingError
        ? 'lbl_sfs_order_error'
        : 'lbl_sfs_shipping_address_error';
    return (
      <SaveForLaterModal
        heading={getLabelValue(labels, headerLabelKey, 'shipping', 'checkout')}
        body={getMessageBodyText(labels, checkoutServerError, itemIds.length)}
        toggleSaveLaterModal={() => toggleSaveLaterModal(sfsErrorType, instance)}
        giftServicesAction={() => {
          instance.deselectGiftService();
          toggleSaveLaterModal(sfsErrorType, instance);
        }}
        updateShippingMethodAction={() => {
          instance.selectStandardShipping();
          toggleSaveLaterModal(sfsErrorType, instance);
        }}
        saveForLaterAction={() =>
          addItemToSflList({ catEntryId, itemId: itemIds, afterSuccessCallback })
        }
        saveForLaterModalState
        type={sfsErrorType}
        cartOrderItems={cartOrderItems}
        itemIds={itemIds}
        catEntryId={catEntryId}
        labels={labels}
      />
    );
  }
  return null;
};

export const renderShippingErrors = instance => {
  const { checkoutServerError, labels } = instance.props;
  if (checkoutServerError && hasApoFpoError(checkoutServerError)) {
    const itemIds = getSaveForLaterItemsOrderItemId(checkoutServerError) || [];
    return (
      <TouchableOpacity
        accessibilityRole="button"
        onPress={() => toggleSaveLaterModal(SFS_ERROR_TYPES.SHIPPING_ADDRESS, instance)}
      >
        <WarningPrompt
          heading={getLabelValue(labels, 'lbl_sfs_shipping_address_error', 'shipping', 'checkout')}
          body={getMessageBodyText(labels, checkoutServerError, itemIds.length)}
          type={SFS_ERROR_TYPES.SHIPPING_ADDRESS}
        />
      </TouchableOpacity>
    );
  }
  return null;
};

export const shouldDisableCheckoutCta = instance => {
  const { checkoutServerError, isGuest, isSfsInvMessagingEnabled } = instance.props;
  if (isSfsInvMessagingEnabled) {
    const giftServiceError = checkoutServerError && hasGiftServiceError(checkoutServerError);
    const rushShippingError = checkoutServerError && hasRushShippingError(checkoutServerError);
    const hasShippingAddressError = checkoutServerError && hasApoFpoError(checkoutServerError);
    if (!isGuest && (giftServiceError || rushShippingError || hasShippingAddressError)) {
      return true;
    }
  }
  return false;
};

export const renderShippingMethodErrors = instance => {
  const { checkoutServerError, labels } = instance.props;
  if (checkoutServerError) {
    const giftServiceError = hasGiftServiceError(checkoutServerError);
    const rushShippingError = hasRushShippingError(checkoutServerError);
    const messageType = rushShippingError
      ? SFS_ERROR_TYPES.SHIPPING_METHOD
      : SFS_ERROR_TYPES.GIFT_SERVICES;
    const itemIds = getSaveForLaterItemsOrderItemId(checkoutServerError) || [];
    if (giftServiceError || rushShippingError) {
      return (
        <TouchableOpacity
          accessibilityRole="button"
          onPress={() => toggleSaveLaterModal(messageType, instance)}
        >
          <WarningPrompt
            heading={getLabelValue(labels, 'lbl_sfs_order_error', 'shipping', 'checkout')}
            body={getMessageBodyText(labels, checkoutServerError, itemIds.length)}
            type={
              giftServiceError && !rushShippingError
                ? SFS_ERROR_TYPES.GIFT_SERVICES
                : SFS_ERROR_TYPES.SHIPPING_METHOD
            }
          />
        </TouchableOpacity>
      );
    }
  }
  return null;
};

const propTypes = {
  addressLabels: PropTypes.shape({}).isRequired,
  initialValues: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  isOrderUpdateChecked: PropTypes.bool,
  smsSignUpLabels: PropTypes.shape({}).isRequired,
  selectedShipmentId: PropTypes.string,
  addressPhoneNo: PropTypes.number,
  labels: PropTypes.shape({}).isRequired,
  isGuest: PropTypes.bool,
  isUsSite: PropTypes.bool,
  orderHasPickUp: PropTypes.bool,
  shipmentMethods: PropTypes.shape([]),
  loadShipmentMethods: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  submitShippingForm: PropTypes.func.isRequired,
  isGiftServicesChecked: PropTypes.bool.isRequired,
  userAddresses: PropTypes.shape([]),
  onFileAddressKey: PropTypes.string,
  isSaveToAddressBookChecked: PropTypes.bool,
  updateShippingAddress: PropTypes.func,
  addNewShippingAddress: PropTypes.func,
  address: PropTypes.shape({}),
  setAsDefaultShipping: PropTypes.func,
  defaultAddressId: PropTypes.string,
  syncErrorsObject: PropTypes.shape({}),
  newUserPhoneNo: PropTypes.string,
  setCheckoutStage: PropTypes.func.isRequired,
  isVenmoPaymentInProgress: PropTypes.bool,
  isVenmoShippingDisplayed: PropTypes.bool,
  emailSignUpLabels: PropTypes.shape({}),
  scrollView: PropTypes.shape({}).isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isBonusPointsEnabled: PropTypes.bool.isRequired,
  handleSMSChange: PropTypes.func.isRequired,
  isGiftOptionsEnabled: PropTypes.func.isRequired,
  shippingAddressId: PropTypes.string,
  keyBoardAvoidRef: PropTypes.shape({}),
  smsOrderUpdatesRichText: PropTypes.string,
  zipCodeABTestEnabled: PropTypes.bool,
  getZipCodeSuggestedAddress: PropTypes.func,
  zipCodeLoader: PropTypes.bool,
  isEDD: PropTypes.bool,
  isSfsInvMessagingEnabled: PropTypes.bool,
  updateEdd: PropTypes.func,
  cartOrderItems: PropTypes.shape([]),
  eddAllowedStatesArr: PropTypes.shape([]),
  currentOrderId: PropTypes.string,
  resetShippingEddData: PropTypes.func,
  addItemToSflList: PropTypes.func,
  clearCheckoutServerError: PropTypes.func,
  scrollToTop: PropTypes.func.isRequired,
  submitErrors: PropTypes.shape({}),
  checkoutServerError: PropTypes.shape({}),
};

const defaultProps = {
  isOrderUpdateChecked: false,
  selectedShipmentId: null,
  addressPhoneNo: null,
  isGuest: true,
  isUsSite: true,
  orderHasPickUp: false,
  shipmentMethods: null,
  userAddresses: null,
  onFileAddressKey: null,
  isSaveToAddressBookChecked: false,
  updateShippingAddress: () => {},
  addNewShippingAddress: () => {},
  emailSignUpLabels: { lbl_shipping_addressEditError: '' },
  address: null,
  setAsDefaultShipping: null,
  defaultAddressId: null,
  syncErrorsObject: {},
  keyBoardAvoidRef: {},
  newUserPhoneNo: null,
  isVenmoPaymentInProgress: false,
  isVenmoShippingDisplayed: true,
  shippingAddressId: null,
  smsOrderUpdatesRichText: null,
  zipCodeABTestEnabled: false,
  isSfsInvMessagingEnabled: false,
  getZipCodeSuggestedAddress: () => {},
  zipCodeLoader: false,
  isEDD: false,
  updateEdd: () => {},
  cartOrderItems: [],
  eddAllowedStatesArr: [],
  currentOrderId: '',
  resetShippingEddData: () => {},
  addItemToSflList: () => {},
  clearCheckoutServerError: () => {},
  checkoutServerError: () => {},
  submitErrors: {},
};

export { propTypes, defaultProps };
