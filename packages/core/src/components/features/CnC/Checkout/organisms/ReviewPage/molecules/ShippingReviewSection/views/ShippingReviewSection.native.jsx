// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { View } from 'react-native';
import { change } from 'redux-form';
import PropTypes from 'prop-types';
import { formatPhoneNumber } from '@tcp/core/src/utils/formValidation/phoneNumber';
import GenericSkeleton from '@tcp/core/src/components/common/molecules/GenericSkeleton/GenericSkeleton.view.native';
import CONSTANTS from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import {
  showSaveForLaterModal,
  renderShippingMethodErrors,
} from '@tcp/core/src/components/features/CnC/Checkout/organisms/ReviewPage/views/ReviewPage.view.utils.native';
import { routerPush } from '@tcp/core/src/utils';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import Address from '../../../../../../../../common/molecules/Address';
import BodyCopy from '../../../../../../../../common/atoms/BodyCopy';
import ShippingMethodDisplay from '../../ShippingMethodDisplay';
import GiftWrappingDisplay from '../../GiftWrappingDisplay';
import TitlePlusEditButton from '../../TitlePlusEditButton';
import style from '../styles/ShippingReviewSection.style.native';
import ShipmentMethods from '../../../../../../common/molecules/ShipmentMethods';

const {
  ShippingReviewContainer,
  AddressSection,
  AddressTitle,
  TitlePlusEditSection,
  SkeletonWrapper,
} = style;

export class ShippingReviewSection extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      saveForLaterModalState: false,
      sfsErrorType: CONSTANTS.SFS_ERROR_TYPES.SHIPPING_METHOD,
    };
  }

  afterSflSuccessCallback = () => {
    const {
      cartOrderItems,
      expressReviewShippingSectionId,
      updateShippingMethodSelection,
    } = this.props;
    const { shippingMethodId = '901101' } = expressReviewShippingSectionId || {};
    if (!cartOrderItems.size) {
      routerPush('/bag', '/bag');
    } else {
      updateShippingMethodSelection({ id: shippingMethodId });
    }
  };

  selectStandardShipping = () => {
    const { dispatch, formName } = this.props;
    dispatch(change(formName, 'expressReviewShippingSection.shippingMethodId', '901101'));
  };

  render() {
    const {
      shippingAddress,
      shippingMethod,
      isGiftOptionsEnabled,
      giftWrappingDisplayName,
      labels,
      onEdit,
      isExpressCheckout,
      shipmentMethods,
      formName,
      formSection,
      dispatch,
      expressReviewShippingSectionId,
      bagLoading,
      isEDD,
    } = this.props;
    const {
      lbl_review_shippingSectionTitle: title,
      lbl_review_sectionAnchor: edit,
      lbl_review_sectionShippingAddressTitle: addressTitle,
      lbl_review_sectionShippingMethodTitle: shippingMethodTitle,
    } = labels;
    const { saveForLaterModalState, sfsErrorType } = this.state;
    return (
      <>
        {!bagLoading ? (
          <>
            <TitlePlusEditSection>
              <TitlePlusEditButton
                title={title}
                editTitle={edit}
                onEdit={onEdit}
                dataLocator="shipping-section"
              />
            </TitlePlusEditSection>
            <ShippingReviewContainer>
              <View>
                <AddressTitle>
                  <BodyCopy
                    fontSize="fs16"
                    dataLocator=""
                    fontFamily="secondary"
                    color="gray.900"
                    fontWeight="extrabold"
                    text={addressTitle}
                  />
                </AddressTitle>
                <AddressSection>
                  {!!shippingAddress.address && (
                    <Address address={shippingAddress.address} regularName />
                  )}
                  <BodyCopy
                    fontSize="fs16"
                    dataLocator=""
                    fontFamily="secondary"
                    color="gray.900"
                    fontWeight="regular"
                    text={shippingAddress.emailAddress}
                  />
                  {!!shippingAddress.phoneNumber && (
                    <BodyCopy
                      fontSize="fs16"
                      dataLocator=""
                      fontFamily="secondary"
                      color="gray.900"
                      fontWeight="regular"
                      text={formatPhoneNumber(shippingAddress.phoneNumber)}
                    />
                  )}
                </AddressSection>
              </View>
              <View>
                <ViewWithSpacing spacingStyles="margin-top-SM">
                  {renderShippingMethodErrors(this)}
                </ViewWithSpacing>
                {!isExpressCheckout && shippingMethod && (
                  <ShippingMethodDisplay
                    labels={labels}
                    displayName={shippingMethod.displayName}
                    shippingCode={shippingMethod.code}
                    isEDD={isEDD}
                    hideSubtitle
                  />
                )}
                {isExpressCheckout && shippingMethod && (
                  <ShipmentMethods
                    shipmentMethods={shipmentMethods}
                    formName={formName}
                    formSection={formSection}
                    selectedShipmentId={
                      expressReviewShippingSectionId &&
                      expressReviewShippingSectionId.shippingMethodId
                    }
                    shipmentHeader={shippingMethodTitle}
                    dispatch={dispatch}
                    isEDD={isEDD}
                  />
                )}
                {isGiftOptionsEnabled && (
                  <GiftWrappingDisplay
                    labels={labels}
                    displayName={giftWrappingDisplayName}
                    onEdit={onEdit}
                    editTitle={edit}
                    isExpressCheckout={isExpressCheckout}
                  />
                )}
              </View>
            </ShippingReviewContainer>
            {showSaveForLaterModal(this, saveForLaterModalState, sfsErrorType)}
          </>
        ) : (
          <>
            <TitlePlusEditSection>
              <TitlePlusEditButton title={title} dataLocator="shipping-section" />
            </TitlePlusEditSection>
            <SkeletonWrapper>
              <GenericSkeleton />
            </SkeletonWrapper>
          </>
        )}
      </>
    );
  }
}

ShippingReviewSection.propTypes = {
  labels: PropTypes.shape({}),
  isGiftOptionsEnabled: PropTypes.bool,
  giftWrappingDisplayName: PropTypes.string,
  shippingAddress: PropTypes.shape({}),
  shippingMethod: PropTypes.shape({
    id: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    shippingSpeed: PropTypes.string.isRequired,
    isDefault: PropTypes.bool,
  }).isRequired,
  onEdit: PropTypes.func.isRequired,
  updateShippingMethodSelection: PropTypes.func.isRequired,
  isExpressCheckout: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  shipmentMethods: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape({}).isRequired,
  formName: PropTypes.string.isRequired,
  formSection: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  expressReviewShippingSectionId: PropTypes.func.isRequired,
  isEDD: PropTypes.bool,
};

ShippingReviewSection.defaultProps = {
  labels: {},
  shippingAddress: {},
  isGiftOptionsEnabled: false,
  giftWrappingDisplayName: 'N/A',
  isEDD: false,
};

export default ShippingReviewSection;
export { ShippingReviewSection as ShippingReviewSectionvanilla };
