// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import {
  getDetailsContent,
  getDetailsContentZymboorie,
  getGiftWrapOptions,
  getInitialGiftWrapOptions,
  getFormCatEntryId,
  getCartOrderDetails,
  getCartOrderId,
  getTcpBrandName,
  getGymBrandName,
  getFormGiftMsg,
} from '../GiftServices.selector';

describe('#GiftServices', () => {
  const GiftServicesState = fromJS({
    error: null,
    onAddAirmilesBanner: false,
    options: {
      giftWrapOptions: {
        text: {
          GiftMsg: 'GiftMsg',
        },
      },
    },
  });
  const CartState = fromJS({
    error: null,
    moduleXContent: [
      { name: 'GiftServicesDetailsTCPModal', richText: 'test' },
      { name: 'GiftServicesDetailsGYMModal', richText: 'test' },
    ],
    onAddAirmilesBanner: false,
    orderDetails: {
      orderId: 1,
      checkout: {
        giftWrap: 'giftWrap',
      },
    },
  });
  const state = {
    Checkout: GiftServicesState,
    CartPageReducer: CartState,
    Labels: {
      global: {
        accessibility: {
          lbl_brandlogo_tcpAltText: 'tcpAltText',
          lbl_brandlogo_gymAltText: 'gymAltText',
        },
      },
      checkout: {
        shipping: {
          referred: [
            { name: 'GiftServicesDetailsTCPModal' },
            { name: 'GiftServicesDetailsGYMModal' },
          ],
        },
      },
    },
  };

  it('#GiftServices should return GiftServices catEntryId', () => {
    expect(getFormCatEntryId(state)).toEqual(
      GiftServicesState.getIn(['options', 'giftWrapOptions', 'text', 'catEntryId']) || ''
    );
  });
  it('#GiftServices should return GiftServices  orderDetails', () => {
    expect(getCartOrderDetails(state)).toEqual(CartState.get('orderDetails'));
  });

  it('#GiftServices should return GiftServices  getCartOrderId', () => {
    expect(getCartOrderId(state)).toEqual(CartState.get('orderDetails').get('orderId'));
  });

  it('#GiftServices should return GiftServices getTcpBrandName', () => {
    expect(getTcpBrandName(state)).toEqual('tcpAltText');
  });

  it('#GiftServices should return GiftServices getGymBrandName', () => {
    expect(getGymBrandName(state)).toEqual('gymAltText');
  });

  it('#GiftServices should return GiftServices getFormGiftMsg', () => {
    expect(getFormGiftMsg(state)).toEqual('GiftMsg');
  });

  it('#GiftServices should return GiftServices getInitialGiftWrapOptions', () => {
    expect(getInitialGiftWrapOptions(state)).toEqual('giftWrap');
  });

  it('#GiftServices should return GiftServices getGiftWrapOptions', () => {
    expect(getGiftWrapOptions(state)).toEqual('');
  });

  it('#GiftServices should return GiftServices getDetailsContent', () => {
    expect(getDetailsContent(state)).toEqual(undefined);
  });

  it('#GiftServices should return GiftServices getDetailsContentZymboorie', () => {
    expect(getDetailsContentZymboorie(state)).toEqual(undefined);
  });
});
