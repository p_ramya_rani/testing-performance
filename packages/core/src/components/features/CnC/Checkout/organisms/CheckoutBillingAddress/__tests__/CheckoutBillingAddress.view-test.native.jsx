// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { List } from 'immutable';
import { shallow } from 'enzyme';
import { CheckoutAddressVanilla } from '../views/CheckoutBillingAddress.view';

describe('CheckoutAddress', () => {
  it('should render correctly', () => {
    const props = {
      dispatch: jest.fn(),
      orderHasShipping: true,
      addressLabels: { addressFormLabels: { selectFromAddress: '' } },
      isGuest: true,
      labels: {},
      shippingAddress: {},
      isSameAsShippingChecked: true,
    };
    const tree = shallow(<CheckoutAddressVanilla {...props} />);
    tree.setProps({ isSameAsShippingChecked: false });
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly if order doesnot have shipping items', () => {
    const props = {
      dispatch: jest.fn(),
      orderHasShipping: false,
      addressLabels: { addressFormLabels: { selectFromAddress: '' } },
      isGuest: true,
      labels: {},
      shippingAddress: {},
      isSameAsShippingChecked: false,
      userAddresses: new List([
        {
          addressId: '1234',
        },
      ]),
      selectedOnFileAddressId: '1234',
    };
    const tree = shallow(<CheckoutAddressVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly if isAddNewAddress is true', () => {
    const props = {
      dispatch: jest.fn(),
      orderHasShipping: false,
      addressLabels: { addressFormLabels: { selectFromAddress: '' } },
      isGuest: true,
      labels: {},
      shippingAddress: {},
      isSameAsShippingChecked: false,
      userAddresses: [
        {
          addressId: '1234',
          addressLine: [],
        },
        { addressId: '5678', addressLine: [] },
      ],
      selectedOnFileAddressId: '1234',

      onFileAddressId: '1234',
      editMode: true,
      change: jest.fn(),
      billingData: { address: {} },
    };
    const tree = shallow(<CheckoutAddressVanilla {...props} />);
    tree.setState({ isAddNewAddress: true });
    tree.instance().onAddressDropDownChange('5678');
    expect(tree.state('isAddNewAddress')).toBe(false);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly if selectedAddress is present', () => {
    const props = {
      dispatch: jest.fn(),
      orderHasShipping: true,
      addressLabels: { addressFormLabels: { selectFromAddress: '' } },
      isGuest: true,
      labels: {},
      isSameAsShippingChecked: false,
      userAddresses: new List([
        {
          addressId: '1234',
          addressLine: ['abcd'],
          state: 'AB',
        },
        { addressId: '5678' },
      ]),
      shippingAddress: {},
      onFileAddressId: '1234',
    };
    const tree = shallow(<CheckoutAddressVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should call onSameAsShippingChange', () => {
    const mockedDispatch = jest.fn();
    const props = {
      dispatch: mockedDispatch,
      orderHasShipping: false,
      addressLabels: {},
      isGuest: true,
      labels: {},
      shippingAddress: {},
      isSameAsShippingChecked: false,
      userAddresses: new List(),
    };

    const tree = shallow(<CheckoutAddressVanilla {...props} />);
    tree.setProps({ isSameAsShippingChecked: true });
    tree.instance().onSameAsShippingChange(true);
    tree.setState({ isAddNewAddress: false });
    tree.instance().toggleAddNewAddressMode();
    expect(tree.state('isAddNewAddress')).toBe(true);
    expect(mockedDispatch).toHaveBeenCalled();
    expect(tree).toMatchSnapshot();
  });
  it('should call onSameAsShippingChange with editMode', () => {
    const mockedDispatch = jest.fn();
    const props = {
      dispatch: mockedDispatch,
      orderHasShipping: false,
      addressLabels: { addressFormLabels: {} },
      isGuest: true,
      labels: {},
      shippingAddress: {},
      isSameAsShippingChecked: true,
      userAddresses: new List([
        {
          primary: 'true',
          addressId: '1234',
          firstName: 'a',
          lastName: 'b',
          addressLine1: '1',
          addressLine2: '2',
          state: 'AB',
          city: 'NY',
          zipCode: '10001',
          country: 'US',
        },
      ]),
      editMode: true,
    };

    const tree = shallow(<CheckoutAddressVanilla {...props} />);
    tree.setProps({ isSameAsShippingChecked: true });
    tree.instance().onSameAsShippingChange(true);
    tree.setState({ isAddNewAddress: false });
    tree.instance().toggleAddNewAddressMode();
    expect(tree.state('isAddNewAddress')).toBe(true);
    expect(mockedDispatch).toHaveBeenCalled();
    expect(tree).toMatchSnapshot();
  });
  it('should call onSameAsShippingChange with editMode without value', () => {
    const mockedDispatch = jest.fn();
    const props = {
      dispatch: mockedDispatch,
      orderHasShipping: false,
      addressLabels: { addressFormLabels: {} },
      isGuest: true,
      labels: {},
      shippingAddress: {},
      isSameAsShippingChecked: false,
      userAddresses: new List([
        {
          primary: 'true',
          addressId: '1234',
          firstName: 'a',
          lastName: 'b',
          addressLine1: '1',
          addressLine2: '2',
          state: 'AB',
          city: 'NY',
          zipCode: '10001',
          country: 'US',
        },
      ]),
      editMode: true,
    };

    const tree = shallow(<CheckoutAddressVanilla {...props} />);
    tree.setProps({ isSameAsShippingChecked: true });
    tree.instance().onSameAsShippingChange(false);
  });
});
