// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { ScrollView, View } from 'react-native';
import { change } from 'redux-form';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import ShippingForm from '../organisms/ShippingForm';
import CheckoutPageEmptyBag from '../../../molecules/CheckoutPageEmptyBag';
import { StyledHeader, HeaderContainer } from '../styles/ShippingPage.style.native';
import CheckoutSectionTitleDisplay from '../../../../../../common/molecules/CheckoutSectionTitleDisplay';
import CheckoutProgressIndicator from '../../../molecules/CheckoutProgressIndicator';
import AddressVerification from '../../../../../../common/organisms/AddressVerification/container/AddressVerification.container';
import VenmoBanner from '../../../../../../common/molecules/VenmoBanner';
import CONSTANTS from '../../../Checkout.constants';
import {
  getAddressInitialValues,
  isShowVenmoBanner,
  shippingPropsTypes,
  shippingDefaultProps,
  shippingPageGetDerivedStateFromProps,
  getPrimaryAddress,
} from './ShippingPage.view.utils';

export default class ShippingPage extends React.PureComponent {
  static propTypes = shippingPropsTypes;

  static defaultProps = shippingDefaultProps;

  constructor(props) {
    super(props);
    const { isOrderUpdateChecked } = props;
    this.state = {
      defaultAddressId: null,
      isSMSChecked: !!isOrderUpdateChecked,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const { shippingDidUpdate, defaultShipmentId, dispatch, initShippingPage } = this.props;
    const { isSMSChecked } = this.state;
    const { isSMSChecked: prevIsSMSChecked } = prevState;
    shippingDidUpdate(prevProps);
    const { defaultShipmentId: prevDefaultShipmentId } = prevProps;
    // to be called explicitly as initial values were not getting set in case of app
    if (prevDefaultShipmentId === false && prevDefaultShipmentId !== defaultShipmentId) {
      dispatch(change('checkoutShipping', 'shipmentMethods.shippingMethodId', defaultShipmentId));
    }
    if (
      initShippingPage &&
      initShippingPage.smsSignUp &&
      initShippingPage.smsSignUp.sendOrderUpdate &&
      !isSMSChecked &&
      isSMSChecked !== prevIsSMSChecked
    ) {
      dispatch(
        change(
          'checkoutShipping',
          'smsSignUp.sendOrderUpdate',
          initShippingPage.smsSignUp.sendOrderUpdate
        )
      );
      dispatch(
        change('checkoutShipping', 'smsSignUp.phoneNumber', initShippingPage.smsSignUp.phoneNumber)
      );
    }
  }

  static getDerivedStateFromProps = shippingPageGetDerivedStateFromProps;

  handleSMSChange = () => {
    const { isSMSChecked } = this.state;
    this.setState({
      isSMSChecked: !isSMSChecked,
    });
  };

  render() {
    const {
      shipmentMethods,
      defaultShipmentId,
      selectedShipmentId,
      isGuest,
      isUsSite,
      orderHasPickUp,
      smsSignUpLabels,
      isOrderUpdateChecked,
      addressPhoneNumber,
      addressLabels,
      emailSignUpLabels,
      loadShipmentMethods,
      navigation,
      availableStages,
      labels,
      isGiftServicesChecked,
      userAddresses,
      onFileAddressKey,
      isSaveToAddressBookChecked,
      address,
      setAsDefaultShipping,
      syncErrors,
      newUserPhoneNo,
      setCheckoutStage,
      formatPayload,
      venmoBannerLabel,
      isVenmoPaymentInProgress,
      isVenmoShippingDisplayed,
      cartOrderItemsCount,
      checkoutPageEmptyBagLabels,
      submitVerifiedShippingAddressData,
      bagLoading,
      addNewShippingAddress,
      updateShippingAddress,
      submitShippingForm,
      showAddressVerification,
      closeAddAddressVerificationModal,
      shippingAddressData,
      isBonusPointsEnabled,
      initShippingPage,
      isGiftOptionsEnabled,
      shippingAddressId,
      smsOrderUpdatesRichText,
      checkoutProgressBarLabels,
      keyBoardAvoidRef,
      formErrorMessage,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      isEDD,
      cartOrderItems,
      eddAllowedStatesArr,
      currentOrderId,
      updateEdd,
      checkoutServerError,
      addItemToSflList,
      scrollToTop,
      resetShippingEddData,
      isSfsInvMessagingEnabled,
      isEditShipping,
      setGiftWrapValue,
      updateShippingMethodSelection,
      submitErrors,
      clearCheckoutServerError,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      setEditShippingFlag,
    } = this.props;
    const { CHECKOUT_STAGES } = CONSTANTS;
    const { defaultAddressId } = this.state;
    const primaryAddressId = getPrimaryAddress(this);

    return (
      <>
        {cartOrderItemsCount > 0 ? (
          <>
            <AddressVerification
              onSuccess={submitVerifiedShippingAddressData(this)}
              heading={addressLabels.addAddressHeading}
              onError={submitVerifiedShippingAddressData(this)}
              shippingAddress={formatPayload(shippingAddressData)}
              toggleAddressModal={closeAddAddressVerificationModal}
              fromCheckoutShipping
              showAddressVerification={showAddressVerification}
              closeAddAddressVerificationModal={closeAddAddressVerificationModal}
              labelsFromShippingPage={labels}
              scrollViewRef={this.scrollView}
              shippingFormRef={this.shippingFormRef}
            />
            <>
              <CheckoutProgressIndicator
                activeStage="shipping"
                navigation={navigation}
                setCheckoutStage={setCheckoutStage}
                isVenmoPaymentInProgress={isVenmoPaymentInProgress}
                isVenmoShippingDisplayed={isVenmoShippingDisplayed}
                scrollView={this.scrollView}
                availableStages={availableStages}
                checkoutProgressBarLabels={checkoutProgressBarLabels}
              />
              <ScrollView
                keyboardShouldPersistTaps="handled"
                ref={scrollView => {
                  this.scrollView = scrollView;
                }}
              >
                {isShowVenmoBanner(CHECKOUT_STAGES.SHIPPING, this.props) && (
                  <VenmoBanner labels={venmoBannerLabel} />
                )}
                <HeaderContainer>
                  <CheckoutSectionTitleDisplay
                    title={getLabelValue(labels, 'lbl_shipping_header', 'shipping', 'checkout')}
                  />
                </HeaderContainer>
                <StyledHeader>
                  <BodyCopy
                    color="black"
                    fontWeight="regular"
                    fontFamily="primary"
                    fontSize="fs28"
                    text={getLabelValue(
                      labels,
                      'lbl_shipping_sectionHeader',
                      'shipping',
                      'checkout'
                    )}
                    textAlign="left"
                  />
                </StyledHeader>
                <View
                  ref={shippingFormRef => {
                    this.shippingFormRef = shippingFormRef;
                  }}
                >
                  <ShippingForm
                    shipmentMethods={shipmentMethods}
                    initialValues={{
                      address: getAddressInitialValues(this),
                      shipmentMethods: { shippingMethodId: defaultShipmentId },
                      onFileAddressKey: shippingAddressId || primaryAddressId,
                      smsSignUp: initShippingPage && initShippingPage.smsSignUp,
                    }}
                    selectedShipmentId={selectedShipmentId}
                    bagLoading={bagLoading}
                    scrollView={this.scrollView}
                    keyBoardAvoidRef={keyBoardAvoidRef}
                    isGuest={isGuest}
                    isUsSite={isUsSite}
                    orderHasPickUp={orderHasPickUp}
                    smsSignUpLabels={smsSignUpLabels}
                    isOrderUpdateChecked={isOrderUpdateChecked}
                    emailSignUpLabels={emailSignUpLabels}
                    addressPhoneNo={addressPhoneNumber}
                    addressLabels={addressLabels}
                    loadShipmentMethods={loadShipmentMethods}
                    navigation={navigation}
                    submitShippingForm={submitShippingForm(this)}
                    labels={labels}
                    isGiftServicesChecked={isGiftServicesChecked}
                    userAddresses={userAddresses}
                    onFileAddressKey={onFileAddressKey}
                    isSaveToAddressBookChecked={isSaveToAddressBookChecked}
                    updateShippingAddress={updateShippingAddress(this)}
                    addNewShippingAddress={addNewShippingAddress}
                    address={address}
                    setAsDefaultShipping={setAsDefaultShipping}
                    defaultAddressId={defaultAddressId}
                    syncErrorsObject={syncErrors}
                    newUserPhoneNo={newUserPhoneNo}
                    setCheckoutStage={setCheckoutStage}
                    isVenmoPaymentInProgress={isVenmoPaymentInProgress}
                    isBonusPointsEnabled={isBonusPointsEnabled}
                    isVenmoShippingDisplayed={isVenmoShippingDisplayed}
                    handleSMSChange={this.handleSMSChange}
                    isGiftOptionsEnabled={isGiftOptionsEnabled}
                    shippingAddressId={shippingAddressId}
                    smsOrderUpdatesRichText={smsOrderUpdatesRichText}
                    formErrorMessage={formErrorMessage}
                    zipCodeABTestEnabled={zipCodeABTestEnabled}
                    getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
                    zipCodeLoader={zipCodeLoader}
                    isEDD={isEDD}
                    updateEdd={updateEdd}
                    cartOrderItems={cartOrderItems}
                    eddAllowedStatesArr={eddAllowedStatesArr}
                    currentOrderId={currentOrderId}
                    checkoutServerError={checkoutServerError}
                    resetShippingEddData={resetShippingEddData}
                    addItemToSflList={addItemToSflList}
                    scrollToTop={scrollToTop}
                    setGiftWrapValue={setGiftWrapValue}
                    updateShippingMethodSelection={updateShippingMethodSelection}
                    isSfsInvMessagingEnabled={isSfsInvMessagingEnabled}
                    isEditShipping={isEditShipping}
                    submitErrors={submitErrors}
                    clearCheckoutServerError={clearCheckoutServerError}
                    mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
                    mapboxSwitch={mapboxSwitch}
                    setEditShippingFlag={setEditShippingFlag}
                  />
                </View>
              </ScrollView>
            </>
          </>
        ) : (
          <CheckoutPageEmptyBag labels={checkoutPageEmptyBagLabels} />
        )}
      </>
    );
  }
}
