// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import BillingPage from '../views';
import { getAddEditAddressLabels } from '../../../../../../common/organisms/AddEditAddress/container/AddEditAddress.selectors';
import { getAddressListState } from '../../../../../account/AddressBook/container/AddressBook.selectors';
import { getCardListFetchingState } from '../../../../../account/Payment/container/Payment.selectors';
import { getCVVCodeRichTextSelector } from './BillingPage.selectors';
import CheckoutSelectors, { getIsApplePayEnabled } from '../../../container/Checkout.selector';
import BagPageSelectors from '../../../../BagPage/container/BagPage.selectors';
import CheckoutActions from '../../../container/Checkout.action';
import { getBonusPointsSwitch } from '../../../../../../common/organisms/BonusPointsDays/container/BonusPointsDays.selectors';
import { toastMessageInfo } from '../../../../../../common/atoms/Toast/container/Toast.actions';

class BillingPageContainer extends React.Component {
  componentWillUnmount() {
    const {
      clearCheckoutServerError,
      checkoutServerError,
      isPayPalHidden,
      isApplePaymentInProgress,
    } = this.props;
    // for apple pay city special char error or any other error, need to show on next or shipping page
    if (checkoutServerError && !isPayPalHidden && !isApplePaymentInProgress) {
      clearCheckoutServerError({});
    }
  }

  /**
   * @description - Error notification for venmo authorization
   */
  onVenmoError = payload => {
    const { toastMessage } = this.props;
    if (payload && payload.venmoErrorMessage) {
      toastMessage(payload.venmoErrorMessage);
    }
  };

  render() {
    const { keyBoardAvoidRef } = this.props;
    return (
      <BillingPage
        onVenmoError={this.onVenmoError}
        {...this.props}
        keyBoardAvoidRef={keyBoardAvoidRef}
      />
    );
  }
}

export const mapDispatchToProps = dispatch => {
  return {
    updateCardDetail: payload => {
      dispatch(CheckoutActions.updateCardData(payload));
    },
    toastMessage: payload => {
      dispatch(toastMessageInfo(payload));
    },
  };
};

export const mapStateToProps = state => {
  const {
    getIsVenmoEnabled,
    getBillingLabels,
    getAccessibilityLabels,
    getIsApplePayEnabledOnBilling,
  } = CheckoutSelectors;
  return {
    cvvCodeRichText: getCVVCodeRichTextSelector(state),
    labels: {
      ...getBillingLabels(state),
      ...getAccessibilityLabels(state),
    },
    addressLabels: getAddEditAddressLabels(state),
    isVenmoEnabled: getIsVenmoEnabled(state), // Venmo Kill Switch, if Venmo enabled then true, else false.
    isApplePayEnabled: getIsApplePayEnabled(state),
    isApplePayEnabledOnBilling: getIsApplePayEnabledOnBilling(state),
    isECOM: BagPageSelectors.getECOMStatus(state),
    venmoError: CheckoutSelectors.getVenmoError(state),
    isPayPalHidden: BagPageSelectors.getIsPayPalHidden(state),
    shippingAddress: CheckoutSelectors.getShippingAddress(state),
    billingData: CheckoutSelectors.getBillingValues(state),
    userAddresses: getAddressListState(state),
    creditFieldLabels: CheckoutSelectors.getCreditFieldLabels(state),
    isFetching: getCardListFetchingState(state),
    isBonusPointsEnabled: getBonusPointsSwitch(state),
    bagLoading: BagPageSelectors.isBagLoading(state),
  };
};

BillingPageContainer.propTypes = {
  getCVVCodeInfo: PropTypes.func,
  clearCheckoutServerError: PropTypes.func.isRequired,
  checkoutServerError: PropTypes.shape({}).isRequired,
  isApplePaymentInProgress: PropTypes.bool.isRequired,
  isPayPalHidden: PropTypes.bool,
  keyBoardAvoidRef: PropTypes.shape({}),
  toastMessage: PropTypes.func,
};

BillingPageContainer.defaultProps = {
  getCVVCodeInfo: null,
  isPayPalHidden: false,
  keyBoardAvoidRef: {},
  toastMessage: () => {},
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BillingPageContainer);
export { BillingPageContainer as BillingPageContainerVanilla };
