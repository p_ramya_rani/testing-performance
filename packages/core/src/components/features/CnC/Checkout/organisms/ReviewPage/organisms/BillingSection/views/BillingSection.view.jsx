// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment, PureComponent } from 'react';
import GenericSkeleton from '@tcp/core/src/components/common/molecules/GenericSkeleton/GenericSkeleton.view';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Anchor, BodyCopy, Col, Row, TextBox } from '@tcp/core/src/components/common/atoms';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import { Grid } from '@tcp/core/src/components/common/molecules';
import Address from '@tcp/core/src/components/common/molecules/Address';
import CardImage from '@tcp/core/src/components/common/molecules/CardImage';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import GiftCardsContainer from '../../../../GiftCardsSection';
import { CHECKOUT_ROUTES } from '../../../../../Checkout.constants';
import getCvvInfo from '../../../../../molecules/CVVInfo';
import CREDIT_CONSTANTS from '../../../../BillingPaymentForm/container/CreditCard.constants';

import styles from '../styles/BillingSection.style';

/**
 * @function renderCardNumber
 * @param {Object} card
 * @param {Object} labels
 * @description This method renders the card number string with ending in prefixed
 */
const renderCardNumber = (card, labels) =>
  `${labels.lbl_review_paymentMethodEndingIn} ${card.cardNumber}`;

/**
 * @function BillingSection
 * @param {Object} props
 * @description Billing Section functional component
 */
export class BillingSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      saveVenmoPayment: false,
    };
  }

  /**
   * This method saves venmo checkbox state in the redux store
   */
  handleVenmoPaymentSaveChange = () => {
    const { saveVenmoPaymentOption } = this.props;
    this.setState(
      (prevState) => ({
        saveVenmoPayment: !prevState.saveVenmoPayment,
      }),
      () => {
        const { saveVenmoPayment } = this.state;
        saveVenmoPaymentOption(saveVenmoPayment);
      }
    );
  };

  getCvvField = () => {
    const {
      isExpressCheckout,
      labels,
      cvvCodeRichText,
      card,
      isBillingVisited,
      venmoPayment: { isVenmoPaymentSelected },
    } = this.props;
    const { infoIconText } = labels;
    return (
      isExpressCheckout &&
      card.ccType !== CREDIT_CONSTANTS.ACCEPTED_CREDIT_CARDS.PLACE_CARD &&
      card.ccType !== CREDIT_CONSTANTS.ACCEPTED_CREDIT_CARDS.PAYPAL &&
      !isBillingVisited &&
      !isVenmoPaymentSelected && (
        <Col colSize={{ small: 3, medium: 2, large: 2 }} className="cvvCode">
          <Field
            placeholder={labels.lbl_review_cvvCode}
            name="cvvCode"
            id="cvvCode"
            component={TextBox}
            dataLocator="cvvTxtBox"
            maxLength="4"
            enableSuccessCheck={false}
            autoComplete="off"
            type="tel"
          />
          <span className="cvv-icon">{getCvvInfo({ cvvCodeRichText, infoIconText })}</span>
        </Col>
      )
    );
  };

  getIsVendorPayment = () => {
    const { card } = this.props;
    return card && card.ccType && card.ccType === CREDIT_CONSTANTS.ACCEPTED_CREDIT_CARDS.APPLE;
  };

  renderAddress = () => {
    const { address, labels } = this.props;
    const isApplePayment = this.getIsVendorPayment();
    return (
      <>
        {address && !isApplePayment && (
          <>
            <BodyCopy
              fontSize="fs16"
              fontWeight="semibold"
              color="gray[900]"
              fontFamily="secondary"
              className="sub-heading"
            >
              {labels.lbl_review_billingAddress}
            </BodyCopy>
            <>
              {this.skeletonCondition() ? (
                <Address address={address} className="review-billing-address" />
              ) : (
                <>
                  <GenericSkeleton />
                </>
              )}
            </>
          </>
        )}
      </>
    );
  };

  getIsCCReq = () => {
    const { card, address, isPaymentDisabled, afterPayInProgress } = this.props;
    return (card || address) && !isPaymentDisabled && !afterPayInProgress;
  };

  getColProps = () => {
    const { isExpressCheckout, afterPayInProgress } = this.props;
    const isCreditCardReq = this.getIsCCReq();
    return {
      cardDetails: {
        colSize: { small: isExpressCheckout ? 3 : 6, medium: 4, large: isExpressCheckout ? 3 : 5 },
      },
      giftCards: {
        colSize: {
          small: 6,
          medium: isExpressCheckout || !isCreditCardReq ? 8 : 4,
          large: isCreditCardReq || afterPayInProgress ? 7 : 8,
        },
        offsetRight: { small: 0, medium: 0, large: isExpressCheckout ? 0 : 1 },
      },
      venmo: {
        colSize: { small: 6, medium: 4, large: 6 },
        offsetRight: { small: 0, medium: 0, large: 1 },
      },
    };
  };

  skeletonCondition = () => {
    const { bagLoading, checkoutRoutingDone } = this.props;
    return !bagLoading && checkoutRoutingDone;
  };

  /**
   * @description - Show Venmo Information if payment is Venmo and Payments tab is enabled
   */
  showVenmoInformation = (isVenmoPaymentSelected) => {
    const { isPaymentDisabled } = this.props;
    return isVenmoPaymentSelected && !isPaymentDisabled;
  };

  renderAfterPay = (colProps, bagLoading, checkoutRoutingDone, labels) => {
    return (
      <>
        <Col {...colProps.cardDetails}>
          <Fragment>
            <BodyCopy
              fontSize="fs16"
              fontWeight="semibold"
              color="gray[900]"
              fontFamily="secondary"
              className="sub-heading"
            >
              {labels.lbl_review_paymentMethod}
            </BodyCopy>
            <BodyCopy>
              {!bagLoading && checkoutRoutingDone ? (
                <CardImage card={{}} cardNumber="" afterPayInProgress />
              ) : (
                <div className="card-skeleton-wrapper">
                  <LoaderSkelton />
                </div>
              )}
            </BodyCopy>
          </Fragment>
        </Col>
      </>
    );
  };

  render() {
    const {
      className,
      card,
      labels,
      venmoPayment,
      venmoPayment: { isVenmoPaymentSelected, venmoSaveToAccountDisplayed, userName },
      bagLoading,
      checkoutRoutingDone,
      afterPayInProgress,
    } = this.props;
    const { saveVenmoPayment } = this.state;
    const colProps = this.getColProps();
    const isCCReq = this.getIsCCReq();

    return (
      <Grid className={`${className}`}>
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <BodyCopy component="span" fontSize="fs26" fontFamily="primary">
              {`${labels.lbl_review_billingSectionTitle} `}
            </BodyCopy>
            <Anchor
              fontSizeVariation="large"
              underline
              anchorVariation="primary"
              {...CHECKOUT_ROUTES.billingPage}
            >
              {labels.lbl_review_billingEdit}
            </Anchor>
          </Col>
        </Row>
        <Row fullBleed className="billing-items">
          {isCCReq && (
            <>
              <Col {...colProps.cardDetails}>
                {card && !isVenmoPaymentSelected && (
                  <Fragment>
                    <BodyCopy
                      fontSize="fs16"
                      fontWeight="semibold"
                      color="gray[900]"
                      fontFamily="secondary"
                      className="sub-heading"
                    >
                      {labels.lbl_review_paymentMethod}
                    </BodyCopy>
                    <BodyCopy>
                      {!bagLoading && checkoutRoutingDone ? (
                        <CardImage
                          card={card}
                          cardNumber={renderCardNumber(card, labels)}
                          afterPayInProgress={afterPayInProgress}
                        />
                      ) : (
                        <div className="card-skeleton-wrapper">
                          <LoaderSkelton />
                        </div>
                      )}
                    </BodyCopy>
                  </Fragment>
                )}

                {this.renderAddress()}
              </Col>
              {this.getCvvField()}
            </>
          )}
          {afterPayInProgress &&
            this.renderAfterPay(colProps, bagLoading, checkoutRoutingDone, labels)}
          <Col {...colProps.giftCards}>
            <GiftCardsContainer isFromReview />
          </Col>
          {this.showVenmoInformation(isVenmoPaymentSelected) && (
            <Col {...colProps.venmo}>
              <BodyCopy
                fontSize="fs16"
                fontWeight="semibold"
                color="gray[900]"
                fontFamily="secondary"
                className="sub-heading"
              >
                {labels.lbl_review_paymentMethod}
              </BodyCopy>
              <section className="venmo-payment-method-wrapper">
                <CardImage card={venmoPayment} cardNumber={userName} />
              </section>

              <div className="venmo-save-wrapper">
                {venmoSaveToAccountDisplayed && (
                  <InputCheckbox
                    component={InputCheckbox}
                    className="venmo-save-checkbox"
                    name="save-venmo-data"
                    input={{ value: saveVenmoPayment, onChange: this.handleVenmoPaymentSaveChange }}
                  >
                    <BodyCopy fontSize="fs14" fontFamily="secondary">
                      {labels.lbl_review_save_venmo}
                    </BodyCopy>
                  </InputCheckbox>
                )}
              </div>
            </Col>
          )}
        </Row>
      </Grid>
    );
  }
}

BillingSection.propTypes = {
  card: PropTypes.shape({}),
  address: PropTypes.shape({}),
  appliedGiftCards: PropTypes.shape([]),
  className: PropTypes.string,
  labels: PropTypes.shape({
    lbl_review_billingSectionTitle: PropTypes.string,
    lbl_review_paymentMethod: PropTypes.string,
    lbl_review_billingAddress: PropTypes.string,
    lbl_review_appliedGiftCards: PropTypes.string,
    lbl_review_paymentMethodEndingIn: PropTypes.string,
  }),
  venmoPayment: PropTypes.shape({}),
  saveVenmoPaymentOption: PropTypes.func,
  isExpressCheckout: PropTypes.bool,
  cvvCodeRichText: PropTypes.string,
  isBillingVisited: PropTypes.bool,
  isPaymentDisabled: PropTypes.bool,
  bagLoading: PropTypes.bool,
  checkoutRoutingDone: PropTypes.bool,
  afterPayInProgress: PropTypes.bool,
};

BillingSection.defaultProps = {
  isExpressCheckout: false,
  cvvCodeRichText: '',
  isBillingVisited: false,
  card: null,
  address: null,
  appliedGiftCards: [],
  isPaymentDisabled: false,
  className: '',
  labels: {
    lbl_review_billingSectionTitle: '',
    lbl_review_paymentMethod: '',
    lbl_review_billingAddress: '',
    lbl_review_appliedGiftCards: '',
    lbl_review_paymentMethodEndingIn: '',
  },
  venmoPayment: {
    userName: '',
  },
  saveVenmoPaymentOption: () => {},
  bagLoading: false,
  checkoutRoutingDone: false,
  afterPayInProgress: false,
};

export default withStyles(BillingSection, styles);
