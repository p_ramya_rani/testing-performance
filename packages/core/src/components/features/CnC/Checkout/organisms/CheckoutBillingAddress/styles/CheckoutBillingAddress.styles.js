// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .address-wrapper {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }
  .default-badge {
    height: 13px;
    z-index: 1;
  }
  .margin-right {
    margin-right: auto;
  }
  .address-field {
    height: 75px;
  }
  .dropdown-title {
    font-size: ${props => props.theme.typography.fontSizes.fs10};
    font-family: ${props => props.theme.typography.fonts.secondary};
    font-weight: ${props => props.theme.typography.fontWeights.extrabold};
  }
  .customSelectTitle {
    margin-top: 0;
  }
  .customSelectArrow {
    margin-top: 5px;
  }
  .billing-address-dropDown {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .paymentMethodHeading {
    font-size: ${props => props.theme.typography.fontSizes.fs16};
    font-weight: ${props => props.theme.typography.fontWeights.extrabold};
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
`;

export default styles;
