// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import { BodyCopy, Row, Col, Button } from '@tcp/core/src/components/common/atoms';
import DamImage from '@tcp/core/src/components/common/atoms/DamImage';
import { getLabelValue } from '@tcp/core/src/utils';
import { getProductDetails } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/SaveForLaterModal.view.style';

class SaveForLaterModal extends React.Component {
  getSecondaryButton = () => {
    const { type, toggleModal, saveForLaterAction, labels, secondaryCtaAction } = this.props;
    let ctaText =
      type === 'shippingAddress'
        ? getLabelValue(labels, 'lbl_sfs_address_cta', 'shipping', 'checkout')
        : getLabelValue(labels, 'lbl_sfs_shipping_cta', 'shipping', 'checkout');

    if (type === 'giftservices') {
      ctaText = getLabelValue(labels, 'lbl_sfs_giftservice_cta', 'shipping', 'checkout');
    }
    return (
      <BodyCopy component="div" textAlign="center" className="elem-mb-LRG cta-btn-container">
        <Button
          fill="BLUE"
          type="submit"
          buttonVariation="fixed-width"
          dataLocator="sfs-cta"
          className="elem-mb-XS sfs-cta"
          onClick={() => {
            toggleModal(false);
            secondaryCtaAction();
          }}
        >
          {ctaText}
        </Button>
        <Button
          fill="WHITE"
          type="button"
          buttonVariation="fixed-width"
          dataLocator="sfs-cta"
          className="elem-mb-XS elem-mt-SM sfs-cta"
          onClick={() => {
            toggleModal(false);
            saveForLaterAction();
          }}
        >
          {getLabelValue(labels, 'lbl_sfs_sfl_cta', 'shipping', 'checkout')}
        </Button>
      </BodyCopy>
    );
  };

  displayItemInfo = (productDetail) => {
    const { itemIds } = this.props;
    const {
      itemInfo: { name, imagePath, itemId },
    } = productDetail;
    if (itemIds.indexOf(itemId) === -1) {
      return null;
    }
    return (
      <BodyCopy
        component="div"
        className="elem-pr-LRG elem-pl-LRG elem-pt-MED elem-pb-MED item-tile elem-mb-MED"
      >
        <Row fullBleed>
          <Col colSize={{ large: 1, medium: 1, small: 1 }}>
            <BodyCopy component="div">
              <DamImage
                imgData={{
                  alt: name,
                  url: imagePath,
                }}
                imgConfigs={[
                  't_t_cart_item_tile_m',
                  't_t_cart_item_tile_t',
                  't_t_cart_item_tile_d',
                ]}
                isProductImage
                className="item-thumb"
              />
            </BodyCopy>
          </Col>
          <Col
            colSize={{ large: 9, medium: 5, small: 4 }}
            ignoreGutter={{ small: true, medium: true, large: true }}
            className="elem-mr-MED"
          >
            <BodyCopy component="div" className="item-preview-details">
              <Row fullBleed>
                <Col colSize={{ large: 7, medium: 8, small: 4 }}>
                  <BodyCopy
                    component="div"
                    fontSize="fs20"
                    fontWeight="extrabold"
                    fontFamily="secondary"
                    className="cart-item-name"
                  >
                    {name}
                  </BodyCopy>
                </Col>
                <Col colSize={{ large: 5, medium: 0, small: 4 }} />
              </Row>
            </BodyCopy>
          </Col>
        </Row>
      </BodyCopy>
    );
  };

  displaySaveForLaterItems = () => {
    const { cartOrderItems } = this.props;
    if (cartOrderItems) {
      return (
        <div className="items-container">
          {cartOrderItems.map((item) => {
            const productDetail = getProductDetails(item);
            return <>{this.displayItemInfo(productDetail)}</>;
          })}
        </div>
      );
    }
    return null;
  };

  onClose = () => {
    const { toggleModal } = this.props;
    toggleModal(false);
  };

  render() {
    const { className, openState, primaryHeading, secondaryHeading } = this.props;
    return (
      openState && (
        <Modal
          fixedWidth
          isOpen={openState}
          onRequestClose={this.onClose}
          heading=""
          overlayClassName="TCPModal__Overlay"
          className={`TCPModal__Content ${className}`}
          widthConfig={{ small: '100%', medium: '450px', large: '797px' }}
          standardHeight
        >
          <BodyCopy
            fontSize="fs24"
            fontWeight="black"
            fontFamily="secondary"
            textAlign="left"
            className="primary-heading"
          >
            {primaryHeading}
          </BodyCopy>
          <BodyCopy fontSize="fs14" fontFamily="secondary" textAlign="left" className="signuptext">
            {secondaryHeading}
          </BodyCopy>

          {this.getSecondaryButton()}
          {this.displaySaveForLaterItems()}
        </Modal>
      )
    );
  }
}

SaveForLaterModal.propTypes = {
  primaryHeading: PropTypes.string.isRequired,
  secondaryHeading: PropTypes.string.isRequired,
  openState: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  toggleModal: PropTypes.func.isRequired,
  cartOrderItems: PropTypes.shape({}).isRequired,
  itemIds: PropTypes.shape([]).isRequired,
  saveForLaterAction: PropTypes.func.isRequired,
  secondaryCtaAction: PropTypes.func,
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
};
SaveForLaterModal.defaultProps = {
  secondaryCtaAction: () => {},
  className: '',
};

export default withStyles(SaveForLaterModal, styles);
export { SaveForLaterModal as SaveForLaterModalVanilla };
