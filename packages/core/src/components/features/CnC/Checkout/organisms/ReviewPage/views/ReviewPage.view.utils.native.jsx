// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { TouchableOpacity } from 'react-native';
import SaveForLaterModal from '@tcp/core/src/components/features/CnC/Checkout/organisms/SaveForLaterModal/views/SaveForLaterModal.view.native';
import WarningPrompt from '@tcp/core/src/components/features/CnC/Checkout/organisms/WarningPrompt/views/WarningPrompt.view.native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  getSfsErroMessage,
  getSaveForLaterItemsOrderItemId,
  getSaveForLaterItemsCatEntryId,
  hasGiftServiceError,
  hasRushShippingError,
} from '@tcp/core/src/components/features/CnC/Checkout/util/utility';
import CONSTANTS from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';

const { SFS_ERROR_TYPES } = CONSTANTS;

const setSaveForLaterModalState = (instance, val) =>
  instance.setState({ saveForLaterModalState: val });

export const toggleSaveLaterModal = (type = SFS_ERROR_TYPES.SHIPPING_ADDRESS, instance) => {
  const { saveForLaterModalState } = instance.state;
  instance.setState({ sfsErrorType: type });
  setSaveForLaterModalState(instance, !saveForLaterModalState);
};

export const getMessageBodyText = (globalLabels, checkoutServerError, itemsCount) =>
  getLabelValue(
    globalLabels,
    getSfsErroMessage(checkoutServerError),
    'shipping',
    'checkout'
  ).replace('X', itemsCount);

export const clearServerError = instance => {
  const { checkoutServerError, clearCheckoutServerError } = instance.props;
  if (checkoutServerError && clearCheckoutServerError) {
    clearCheckoutServerError({});
  }
};

export const showSaveForLaterModal = (instance, saveForLaterModalState, sfsErrorType) => {
  const { cartOrderItems, checkoutServerError, addItemToSflList, globalLabels } = instance.props;
  if (saveForLaterModalState && checkoutServerError && checkoutServerError.billingError) {
    const giftServiceError = hasGiftServiceError(checkoutServerError);
    const rushShippingError = hasRushShippingError(checkoutServerError);
    const catEntryId = getSaveForLaterItemsCatEntryId(checkoutServerError) || [];
    const itemIds = getSaveForLaterItemsOrderItemId(checkoutServerError) || [];
    const afterSuccessCallback = () => {
      instance.afterSflSuccessCallback();
    };
    const headerLabelKey =
      giftServiceError || rushShippingError
        ? 'lbl_sfs_order_error'
        : 'lbl_sfs_shipping_address_error';
    return (
      <SaveForLaterModal
        heading={getLabelValue(globalLabels, headerLabelKey, 'shipping', 'checkout')}
        body={getMessageBodyText(globalLabels, checkoutServerError, itemIds.length)}
        toggleSaveLaterModal={() => toggleSaveLaterModal(sfsErrorType, instance)}
        saveForLaterAction={() => {
          addItemToSflList({ catEntryId, itemId: itemIds, afterSuccessCallback });
          toggleSaveLaterModal(sfsErrorType, instance);
        }}
        updateShippingMethodAction={() => {
          instance.selectStandardShipping();
          toggleSaveLaterModal(sfsErrorType, instance);
        }}
        saveForLaterModalState
        type={sfsErrorType}
        cartOrderItems={cartOrderItems}
        itemIds={itemIds}
        catEntryId={catEntryId}
        labels={globalLabels}
      />
    );
  }
  return null;
};

export const renderShippingMethodErrors = instance => {
  const { checkoutServerError, globalLabels } = instance.props;
  if (checkoutServerError) {
    const giftServiceError = hasGiftServiceError(checkoutServerError);
    const rushShippingError = hasRushShippingError(checkoutServerError);
    const messageType = rushShippingError
      ? SFS_ERROR_TYPES.SHIPPING_METHOD
      : SFS_ERROR_TYPES.GIFT_SERVICES;
    const itemIds = getSaveForLaterItemsOrderItemId(checkoutServerError) || [];
    if (giftServiceError || rushShippingError) {
      return (
        <TouchableOpacity
          accessibilityRole="button"
          onPress={() => toggleSaveLaterModal(messageType, instance)}
        >
          <WarningPrompt
            heading={getLabelValue(globalLabels, 'lbl_sfs_order_error', 'shipping', 'checkout')}
            body={getMessageBodyText(globalLabels, checkoutServerError, itemIds.length)}
            type={
              giftServiceError && !rushShippingError
                ? SFS_ERROR_TYPES.GIFT_SERVICES
                : SFS_ERROR_TYPES.SHIPPING_METHOD
            }
          />
        </TouchableOpacity>
      );
    }
  }
  return null;
};
