// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateShippingCode } from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.actions';
import {
  getEddFeatureSwitch,
  getSelectedShippingCode,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.selectors';
import ShippingReviewSection from '../../../molecules/ShippingReviewSection';
import CHECKOUT_SELECTOR from '../../../../../container/Checkout.selector';
import CONSTANTS from '../../../../../Checkout.constants';
import { updateShipmentMethodSelection } from '../../../../../container/Checkout.action';

export class ShippingReviewContainer extends React.PureComponent {
  static propTypes = {
    shippingAddress: PropTypes.shape({}).isRequired,
    shippingMethod: PropTypes.shape({}).isRequired,
    onEdit: PropTypes.func.isRequired,
    labels: PropTypes.shape({}),
    isGiftOptionsEnabled: PropTypes.bool.isRequired,
    giftWrappingDisplayName: PropTypes.string.isRequired,
    isExpressCheckout: PropTypes.bool,
    shipmentMethods: PropTypes.shape({}).isRequired,
    formName: PropTypes.string,
    formSection: PropTypes.string,
    updateShippingMethodSelection: PropTypes.func.isRequired,
    expressReviewShippingSectionId: PropTypes.shape({}),
    dispatch: PropTypes.func.isRequired,
    bagLoading: PropTypes.bool,
    checkoutRoutingDone: PropTypes.bool,
    shippingMethodCode: PropTypes.string,
    updateShippingCodeAction: PropTypes.func.isRequired,
    isEDD: PropTypes.bool,
  };

  static defaultProps = {
    labels: {},
    isExpressCheckout: false,
    formName: '',
    formSection: '',
    expressReviewShippingSectionId: {},
    bagLoading: false,
    checkoutRoutingDone: false,
    shippingMethodCode: '',
    isEDD: false,
  };

  componentDidMount() {
    const { updateShippingCodeAction, shippingMethodCode, isEDD } = this.props;
    if (isEDD) {
      updateShippingCodeAction(shippingMethodCode);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      updateShippingMethodSelection,
      expressReviewShippingSectionId,
      isExpressCheckout,
      updateShippingCodeAction,
      shippingMethodCode,
      isEDD,
    } = this.props;
    const {
      expressReviewShippingSectionId: prevexpressReviewShippingSectionId,
      shippingMethodCode: prevshippingMethodCode,
    } = prevProps;
    if (
      isExpressCheckout &&
      prevexpressReviewShippingSectionId.shippingMethodId &&
      typeof prevexpressReviewShippingSectionId.shippingMethodId !== 'object' &&
      expressReviewShippingSectionId.shippingMethodId !==
        prevexpressReviewShippingSectionId.shippingMethodId
    ) {
      updateShippingMethodSelection({ id: expressReviewShippingSectionId.shippingMethodId });
    }

    if (isExpressCheckout && shippingMethodCode !== prevshippingMethodCode && isEDD) {
      updateShippingCodeAction(shippingMethodCode);
    }
  }

  render() {
    const {
      shippingAddress,
      shippingMethod,
      onEdit,
      labels,
      isGiftOptionsEnabled,
      giftWrappingDisplayName,
      isExpressCheckout,
      shipmentMethods,
      formName,
      formSection,
      updateShippingMethodSelection,
      expressReviewShippingSectionId,
      dispatch,
      bagLoading,
      checkoutRoutingDone,
      isEDD,
    } = this.props;
    return (
      <ShippingReviewSection
        {...this.props}
        shippingAddress={shippingAddress}
        checkoutRoutingDone={checkoutRoutingDone}
        shippingMethod={shippingMethod}
        onEdit={onEdit}
        labels={labels}
        isGiftOptionsEnabled={isGiftOptionsEnabled}
        giftWrappingDisplayName={giftWrappingDisplayName}
        isExpressCheckout={isExpressCheckout}
        shipmentMethods={shipmentMethods}
        dispatch={dispatch}
        formName={formName}
        formSection={formSection}
        updateShippingMethodSelection={updateShippingMethodSelection}
        expressReviewShippingSectionId={expressReviewShippingSectionId}
        bagLoading={bagLoading}
        isEDD={isEDD}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    shippingAddress: CHECKOUT_SELECTOR.getShippingDestinationValues(state),
    shippingMethod: CHECKOUT_SELECTOR.getSelectedShippingMethodDetails(state),
    expressReviewShippingSectionId: CHECKOUT_SELECTOR.getExpressReviewShippingSectionId(state),
    labels: CHECKOUT_SELECTOR.getShippingSectionLabels(state),
    isGiftOptionsEnabled: !!CHECKOUT_SELECTOR.getSelectedGiftWrapDetails(state).name,
    giftWrappingDisplayName:
      CHECKOUT_SELECTOR.getSelectedGiftWrapDetails(state).name || CONSTANTS.NA,
    shippingMethodCode:
      CHECKOUT_SELECTOR.getSelectedShippingMethodCode(state) || getSelectedShippingCode(state),
    isEDD: getEddFeatureSwitch(state),
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    updateShippingMethodSelection: payload => {
      dispatch(updateShipmentMethodSelection(payload));
    },
    updateShippingCodeAction: payload => {
      dispatch(updateShippingCode(payload));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShippingReviewContainer);
