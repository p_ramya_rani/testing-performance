// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import Image from '@tcp/core/src/components/common/atoms/Image';

const WarningWrapper = styled.View`
  display: flex;
  border: 2px solid ${props => props.theme.colorPalette.yellow[600]};
  flex-direction: row;
`;

const MessageWrapper = styled.View`
  flex: 0.8;
  padding-right: 12px;
`;

const WarningImageWrapper = styled.View`
  flex: 0.1;
  align-items: center;
  margin-top: 14px;
`;

const WarningImage = styled(Image)`
  margin: 2px;
`;

const ArrowImageWrapper = styled.View`
  flex: 0.1;
  align-items: center;
  justify-content: center;
`;
const ArrowImage = styled(Image)`
  margin: 2px;
`;

export {
  WarningWrapper,
  WarningImage,
  WarningImageWrapper,
  ArrowImage,
  ArrowImageWrapper,
  MessageWrapper,
};
