// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const RichTextWrapper = styled.View`
  flex-basis: 80%;
  margin-left: 10px;
  margin-right: 10px;
`;

const SkeletonWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.MED};
  width: 200px;
`;

const ContentHeight = { minHeight: 1300 };

export { RichTextWrapper, ContentHeight, SkeletonWrapper };
