/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { FormSection, reduxForm, change } from 'redux-form';
import { requireNamedOnlineModule } from '@tcp/core/src/utils/resourceLoader';
import { setSessionStorage, getSessionStorage } from '@tcp/core/src/utils/utils.web';
import { parseBoolean, getLabelValue, routerPush, isClient } from '@tcp/core/src/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import CheckoutSectionTitleDisplay from '../../../../../../common/molecules/CheckoutSectionTitleDisplay';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import CheckoutFooter from '../../../molecules/CheckoutFooter';
import styles from '../styles/ReviewPage.style';
import { CHECKOUT_ROUTES } from '../../../Checkout.constants';
import utility, {
  scrollToFirstError,
  hasGiftServiceError,
  hasRushShippingError,
  getSfsErroMessage,
  getSaveForLaterItemsOrderItemId,
  getSaveForLaterItemsCatEntryId,
} from '../../../util/utility';
import { Anchor, BodyCopy, Col, Row } from '../../../../../../common/atoms';
import PickUpReviewSectionContainer from '../organisms/PickUpReviewSection';
import ShippingReviewSection from '../organisms/ShippingReviewSection';
import BillingSection from '../organisms/BillingSection';
import CheckoutCartItemList from '../organisms/CheckoutCartItemList';
import CheckoutOrderInfo from '../../../molecules/CheckoutOrderInfoMobile';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import ContactFormFields from '../../../molecules/ContactFormFields';
import GenericSkeleton from '../../../../../../common/molecules/GenericSkeleton/GenericSkeleton.view';
import WarningPrompt from '../../WarningPrompt';
import AddressVerification from '../../../../../../common/organisms/AddressVerification/container/AddressVerification.container';
import ErrorMessage from '../../../../common/molecules/ErrorMessage';

const formName = 'expressReviewPage';

class ReviewPage extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string.isRequired,
    labels: PropTypes.shape({}).isRequired,
    reviewDidMount: PropTypes.func.isRequired,
    reviewFormSubmit: PropTypes.func.isRequired,
    orderHasShipping: PropTypes.bool.isRequired,
    isRegisteredUserCallDone: PropTypes.bool.isRequired,
    orderHasPickUp: PropTypes.bool.isRequired,
    setVenmoShippingState: PropTypes.func,
    setVenmoPickupState: PropTypes.func,
    showAccordian: PropTypes.bool,
    isGuest: PropTypes.bool.isRequired,
    checkoutRoutingDone: PropTypes.bool.isRequired,
    isExpressCheckout: PropTypes.bool,
    shipmentMethods: PropTypes.shape({}).isRequired,
    handleSubmit: PropTypes.func.isRequired,
    pickUpContactPerson: PropTypes.shape({}).isRequired,
    pickUpContactAlternate: PropTypes.shape({}).isRequired,
    ServerErrors: PropTypes.node.isRequired,
    isPaymentDisabled: PropTypes.bool,
    dispatch: PropTypes.func.isRequired,
    pageCategory: PropTypes.string,
    checkoutServerError: PropTypes.shape({}).isRequired,
    clearCheckoutServerError: PropTypes.func.isRequired,
    bagLoading: PropTypes.bool,
    titleLabel: PropTypes.shape({}).isRequired,
    submitOrderRichText: PropTypes.string,
    itemsWithEdd: PropTypes.shape([]),
    selectedShippingCode: PropTypes.string,
    persistEddNodeAction: PropTypes.func,
    isEddABTest: PropTypes.func,
    isEDD: PropTypes.bool,
    pageName: PropTypes.string,
    appliedExpiredCoupon: PropTypes.shape([]),
    cartOrderItems: PropTypes.shape([]),
    addItemToSflList: PropTypes.func,
    globalLabels: PropTypes.shape({}),
    shippingProps: PropTypes.shape({}),
    expressReviewShippingSectionId: PropTypes.shape({}),
    updateShippingMethodSelection: PropTypes.func,
    updateShippingAddressData: PropTypes.func,
    setAfterPayProgress: PropTypes.func,
    afterPayInProgress: PropTypes.bool,
    orderBalanceTotal: PropTypes.number,
    trackClickAfterPay: PropTypes.func,
    afterPayMinOrderAmount: PropTypes.number,
    afterPayMaxOrderAmount: PropTypes.number,
  };

  static defaultProps = {
    setVenmoShippingState: () => {},
    setVenmoPickupState: () => {},
    showAccordian: true,
    isExpressCheckout: false,
    isPaymentDisabled: false,
    pageCategory: '',
    bagLoading: false,
    submitOrderRichText: null,
    itemsWithEdd: [],
    selectedShippingCode: '',
    persistEddNodeAction: () => {},
    isEddABTest: false,
    isEDD: false,
    pageName: '',
    appliedExpiredCoupon: [],
    cartOrderItems: [],
    addItemToSflList: () => {},
    globalLabels: {},
    shippingProps: {},
    expressReviewShippingSectionId: { shippingMethodId: '901101' },
    updateShippingMethodSelection: () => {},
    updateShippingAddressData: () => {},
    setAfterPayProgress: () => {},
    afterPayInProgress: false,
    orderBalanceTotal: 0,
    trackClickAfterPay: () => {},
    afterPayMinOrderAmount: 30,
    afterPayMaxOrderAmount: 1000,
  };

  componentDidMount() {
    const { setVenmoShippingState, setVenmoPickupState, reviewDidMount, afterPayInProgress } =
      this.props;
    setVenmoShippingState(true);
    setVenmoPickupState(true);
    reviewDidMount();
    this.eddApiCalled = false;
    if (isClient() && !window.isAfterPayLoaded && afterPayInProgress) {
      requireNamedOnlineModule('afterPay').then(() => {
        window.isAfterPayLoaded = true;
      });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      isPaymentDisabled: prevPaymentDisabled,
      isRegisteredUserCallDone: prevIsRegisteredUserCallDone,
    } = prevProps;
    const {
      isPaymentDisabled,
      dispatch,
      reviewDidMount,
      isRegisteredUserCallDone,
      bagLoading,
      setAfterPayProgress,
      afterPayInProgress,
      orderBalanceTotal,
    } = this.props;
    if (prevIsRegisteredUserCallDone !== isRegisteredUserCallDone && isRegisteredUserCallDone) {
      reviewDidMount();
    }
    if (!bagLoading && !orderBalanceTotal && afterPayInProgress) {
      setAfterPayProgress(false);
    }
    if (prevPaymentDisabled !== isPaymentDisabled) {
      dispatch(change(formName, 'cvvCode', null));
    }
    this.persistEddAction(prevProps);
  }

  componentWillUnmount() {
    const { clearCheckoutServerError, checkoutServerError, setCouponRemovedFromOrderFlag } =
      this.props;

    if (checkoutServerError) {
      clearCheckoutServerError({});
    }
    setCouponRemovedFromOrderFlag(false);
  }

  persistEddAction = (prevProps) => {
    const { selectedShippingCode: prevSelectedShippingCode, itemsWithEdd: prevItemsWithEdd } =
      prevProps;
    const { persistEddNodeAction, itemsWithEdd, selectedShippingCode, isEDD } = this.props;
    const isEddABStateFlag = parseBoolean(this.getEddSessionState());
    const shippingCode = this.getShipmentMethod(selectedShippingCode, itemsWithEdd);
    const prevShipmentMethod = this.getShipmentMethod(prevSelectedShippingCode, prevItemsWithEdd);
    if (
      shippingCode &&
      itemsWithEdd &&
      isEDD &&
      (!this.eddApiCalled ||
        prevShipmentMethod !== shippingCode ||
        prevItemsWithEdd !== itemsWithEdd)
    ) {
      persistEddNodeAction(isEddABStateFlag);
      this.eddApiCalled = true;
    }
  };

  getShipmentMethod = (selectedShippingCode, itemsWithEdd) => {
    const shippingDates = itemsWithEdd && itemsWithEdd[0] && itemsWithEdd[0].shippingDates;
    let selectedShipmentMethod = '';
    if (selectedShippingCode) {
      selectedShipmentMethod = selectedShippingCode;
    } else if (shippingDates && shippingDates.UGNR) {
      selectedShipmentMethod = 'UGNR';
    } else if (shippingDates && shippingDates.PMDC) {
      selectedShipmentMethod = 'PMDC';
    } else if (shippingDates && shippingDates.U2AK) {
      selectedShipmentMethod = 'U2AK';
    }
    return selectedShipmentMethod;
  };

  handleDefaultLinkClick = (e) => {
    e.preventDefault();
  };

  getEddSessionState = () => {
    const { isEddABTest } = this.props;
    const isEddSessionEnabled = parseBoolean(getSessionStorage('EDD_ENABLED'));
    if (!isEddSessionEnabled) {
      setSessionStorage({ key: 'EDD_ENABLED', value: isEddABTest });
    }
    return isEddABTest || isEddSessionEnabled;
  };

  shouldDisableCheckoutCta = () => {
    const { checkoutServerError } = this.props;
    if (
      checkoutServerError &&
      (hasGiftServiceError(checkoutServerError) || hasRushShippingError(checkoutServerError))
    ) {
      return true;
    }
    return false;
  };

  afterSflSuccessCallback = () => {
    const { cartOrderItems, expressReviewShippingSectionId, updateShippingMethodSelection } =
      this.props;
    const { shippingMethodId = '901101' } = expressReviewShippingSectionId || {};
    if (!cartOrderItems.size) {
      routerPush('/bag', '/bag');
    } else {
      updateShippingMethodSelection({ id: shippingMethodId });
    }
  };

  selectStandardShipping = () => {
    const { dispatch } = this.props;
    dispatch(change(formName, 'expressReviewShippingSection.shippingMethodId', '901101'));
  };

  renderShippingMethodErrors = () => {
    const { checkoutServerError, cartOrderItems, addItemToSflList, globalLabels } = this.props;

    if (
      checkoutServerError &&
      (hasGiftServiceError(checkoutServerError) || hasRushShippingError(checkoutServerError))
    ) {
      const itemIds = getSaveForLaterItemsOrderItemId(checkoutServerError) || [];
      const catEntryId = getSaveForLaterItemsCatEntryId(checkoutServerError) || [];
      const afterSuccessCallback = () => {
        this.afterSflSuccessCallback();
      };
      return (
        <WarningPrompt
          heading={getLabelValue(globalLabels, 'lbl_sfs_order_error', 'shipping', 'checkout')}
          body={getLabelValue(
            globalLabels,
            getSfsErroMessage(checkoutServerError),
            'shipping',
            'checkout'
          )}
          cartOrderItems={cartOrderItems}
          itemIds={itemIds}
          saveForLaterAction={() =>
            addItemToSflList({ catEntryId, itemId: itemIds, afterSuccessCallback })
          }
          secondaryCtaAction={() => {
            this.selectStandardShipping();
          }}
          labels={globalLabels}
        />
      );
    }
    return null;
  };

  redirectToShipping = () => {
    utility.routeToPage(CHECKOUT_ROUTES.shippingPage, { editShippingAddress: 'true' });
  };

  submitAddressForm = (payloadParam) => {
    const { updateShippingAddressData, shippingProps } = this.props;
    const { userAddresses, shippingAddressId } = shippingProps;
    const selectedAddress =
      (userAddresses && userAddresses.find((item) => item.addressId === shippingAddressId)) || {};
    const payload = {
      shipTo: {
        address: {
          ...selectedAddress,
          ...payloadParam,
          onFileAddressKey: shippingAddressId,
          addressLine1: payloadParam.address1,
          addressLine2: payloadParam.address2,
          zipCode: payloadParam.zip,
          setAsDefault: selectedAddress.primary,
        },
      },
      shouldUpdateCart: true,
    };
    updateShippingAddressData(payload);
  };

  render() {
    const {
      className,
      labels,
      orderHasPickUp,
      orderHasShipping,
      isGuest,
      showAccordian,
      isExpressCheckout,
      shipmentMethods,
      handleSubmit,
      ServerErrors,
      pageCategory,
      reviewFormSubmit,
      checkoutRoutingDone,
      bagLoading,
      titleLabel,
      submitOrderRichText,
      pageName,
      appliedExpiredCoupon,
      shippingProps,
      afterPayInProgress,
      orderBalanceTotal,
      trackClickAfterPay,
      afterPayMinOrderAmount,
      afterPayMaxOrderAmount,
      couponRemovedFromOrderFlag,
      couponRemovedErrorMsgLabel,
    } = this.props;
    const {
      header,
      backLinkBilling,
      nextSubmitText,
      applyConditionPreText,
      applyConditionTermsText,
      applyConditionAndText,
      applyConditionPolicyText,
      ariaLabelBackLink,
      ariaLabelSubmitOrderButton,
      labelAfterPaySubmit,
      labelAfterPayMessage,
      labelAfterPayThresholdError,
    } = labels;

    const { shippingAddress, addressLabels: { addAddressHeading } = {} } = shippingProps;

    const expressReviewShippingSection = 'expressReviewShippingSection';

    return (
      <>
        <form
          name={formName}
          className={className}
          id={formName}
          onSubmit={handleSubmit(reviewFormSubmit)}
        >
          <CheckoutSectionTitleDisplay title={header} dataLocator="review-title" />
          {ServerErrors && <ServerErrors />}
          {couponRemovedFromOrderFlag && (
            <ErrorMessage error={couponRemovedErrorMsgLabel} className="checkout-page-error" />
          )}
          {!!orderHasPickUp && (
            <div className="review-pickup">
              <PickUpReviewSectionContainer
                isExpressCheckout={isExpressCheckout}
                onEdit={() => {
                  utility.routeToPage(CHECKOUT_ROUTES.pickupPage);
                }}
                bagLoading={bagLoading}
                checkoutRoutingDone={checkoutRoutingDone}
              />
            </div>
          )}
          <FormSection name={expressReviewShippingSection}>
            {!!orderHasShipping && (
              <>
                {this.renderShippingMethodErrors()}
                <div className="review-shipping">
                  <ShippingReviewSection
                    isExpressCheckout={isExpressCheckout}
                    shipmentMethods={shipmentMethods}
                    formName={formName}
                    formSection={expressReviewShippingSection}
                    onEdit={() => {
                      utility.routeToPage(CHECKOUT_ROUTES.shippingPage);
                    }}
                    bagLoading={bagLoading}
                    checkoutRoutingDone={checkoutRoutingDone}
                  />
                </div>
              </>
            )}
          </FormSection>
          {!bagLoading ? (
            <BillingSection
              isExpressCheckout={isExpressCheckout}
              bagLoading={bagLoading}
              checkoutRoutingDone={checkoutRoutingDone}
              afterPayInProgress={afterPayInProgress}
            />
          ) : (
            <>
              <Row fullBleed className="review-billing-skeleton">
                <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                  <BodyCopy component="span" fontSize="fs26" fontFamily="primary">
                    {`${titleLabel.lbl_review_billingSectionTitle} `}
                  </BodyCopy>
                </Col>
              </Row>
              <GenericSkeleton />
            </>
          )}
          <CheckoutCartItemList
            disableProductRedirect
            bagLoading={bagLoading}
            checkoutRoutingDone={checkoutRoutingDone}
          />
          <CheckoutOrderInfo
            showAccordian={showAccordian}
            isGuest={isGuest}
            fullPageInfo
            pageCategory={pageCategory}
            isCheckoutFlow
          />
          <CheckoutFooter
            hideBackLink
            ariaLabelBackLink={ariaLabelBackLink || backLinkBilling}
            ariaLabelNextButton={ariaLabelSubmitOrderButton}
            backLinkHandler={() => utility.routeToPage(CHECKOUT_ROUTES.billingPage)}
            nextButtonText={afterPayInProgress ? labelAfterPaySubmit : nextSubmitText}
            backLinkText={backLinkBilling}
            submitOrderRichText={submitOrderRichText}
            footerBody={[
              applyConditionPreText,
              <Anchor
                underline
                to="/#"
                dataLocator="termAndConditionText"
                onClick={this.handleDefaultLinkClick}
              >
                {applyConditionTermsText}
              </Anchor>,
              applyConditionAndText,
              <Anchor
                underline
                to="/#"
                dataLocator="PrivacyText"
                onClick={this.handleDefaultLinkClick}
              >
                {applyConditionPolicyText}
              </Anchor>,
            ]}
            bagLoading={bagLoading}
            pageName={pageName}
            appliedExpiredCoupon={appliedExpiredCoupon}
            disableNext={this.shouldDisableCheckoutCta()}
            showCheckoutWithAfterPay={afterPayInProgress}
            orderBalanceTotal={orderBalanceTotal}
            trackClickAfterPay={trackClickAfterPay}
            pageCategory={pageCategory}
            labelAfterPayMessage={labelAfterPayMessage}
            labelAfterPayThresholdError={labelAfterPayThresholdError}
            afterPayMinOrderAmount={afterPayMinOrderAmount}
            afterPayMaxOrderAmount={afterPayMaxOrderAmount}
            submitForm={formName}
          />
        </form>
        <AddressVerification
          onSuccess={this.submitAddressForm}
          heading={addAddressHeading}
          onEditAddressClick={this.redirectToShipping}
          onError={() => {}}
          shippingAddress={shippingAddress}
          fromReviewPage
        />
      </>
    );
  }
}

const validateMethod = createValidateMethod({
  pickUpAlternateExpress: ContactFormFields.ContactValidationConfig,
  ...getStandardConfig(['cvvCode']),
});

export default reduxForm({
  form: formName, // a unique identifier for this form
  ...validateMethod,
  enableReinitialize: true,
  shouldValidate: () => true,
  onSubmitFail: (errors) => scrollToFirstError(errors),
})(withStyles(ReviewPage, styles));
export { ReviewPage as ReviewPageVanilla };
