// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../../../../../../../../common/molecules/Modal';
import { RichText } from '../../../../../../../../common/atoms';
import withStyles from '../../../../../../../../common/hoc/withStyles';
import styles, { modalStyles } from '../styles/GiftServices.style';
import GenericSkeleton from '../../../../../../../../common/molecules/GenericSkeleton/GenericSkeleton.view';

class GiftServicesDetailsModal extends React.PureComponent {
  render() {
    const { openState, onRequestClose, labels, brand } = this.props;
    const { DETAILS_RICH_TEXT, DETAILS_RICH_TEXT_GYM } = labels;
    const modalContent = brand === 'TCP' ? DETAILS_RICH_TEXT : DETAILS_RICH_TEXT_GYM;
    return (
      <Modal
        isOpen={openState}
        onRequestClose={onRequestClose}
        overlayClassName="TCPModal__Overlay"
        className="TCPModal__Content"
        maxWidth="500px"
        minHeight="90%"
        heightConfig={{ height: '90%' }}
        closeIconDataLocator="details-cross-icon"
        fixedWidth
        contentLabel={labels.giftServicesAriaLabel}
        inheritedStyles={modalStyles}
      >
        {!modalContent ? <GenericSkeleton /> : <RichText richTextHtml={modalContent} />}
      </Modal>
    );
  }
}

GiftServicesDetailsModal.propTypes = {
  openState: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  brand: PropTypes.string.isRequired,
};

export default withStyles(GiftServicesDetailsModal, styles);
export { GiftServicesDetailsModal as GiftServicesDetailsModalVanilla };
