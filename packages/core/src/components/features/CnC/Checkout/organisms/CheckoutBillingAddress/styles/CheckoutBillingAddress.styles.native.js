// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const BillingAddWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

const SameAsShippingWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

const CheckoutAddressWrapper = styled.View``;

const AddressDropdownWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
`;
export { BillingAddWrapper, SameAsShippingWrapper, CheckoutAddressWrapper, AddressDropdownWrapper };
