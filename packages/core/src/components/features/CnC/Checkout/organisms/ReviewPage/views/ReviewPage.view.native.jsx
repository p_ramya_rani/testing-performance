/* eslint-disable react/prop-types */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ScrollView } from 'react-native';
import { reduxForm, change, FormSection } from 'redux-form';
import PropTypes from 'prop-types';
import {
  scrollToFirstErrorApp,
  hasGiftServiceError,
  hasRushShippingError,
  hasApoFpoError,
} from '@tcp/core/src/components/features/CnC/Checkout/util/utility';
import AddressVerification from '../../../../../../common/organisms/AddressVerification/container/AddressVerification.container';
import CheckoutSectionTitleDisplay from '../../../../../../common/molecules/CheckoutSectionTitleDisplay';
import CheckoutProgressIndicator from '../../../molecules/CheckoutProgressIndicator';
import CnCTemplate from '../../../../common/organism/CnCTemplate';
import PickUpReviewSectionContainer from '../organisms/PickUpReviewSection';
import style from '../styles/ReviewPage.style.native';
import CONSTANTS from '../../../Checkout.constants';
import BillingSection from '../organisms/BillingSection';
import ShippingReviewSection from '../organisms/ShippingReviewSection';
import CheckoutCartItemList from '../organisms/CheckoutCartItemList';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import ContactFormFields from '../../../molecules/ContactFormFields';
import RichText from '../../../../../../common/atoms/RichText';

const { Container } = style;
const formName = 'expressReviewPage';

const refObj = {};
let scrollViewRef = false;

const scrollToFirstErrorReviewPage = (errors) => {
  scrollToFirstErrorApp(errors, refObj, scrollViewRef);
};

const createRefParent = (node, refName, keyBoardAvoidRef) => {
  if (node) {
    refObj[refName] = node;
  }
  if (keyBoardAvoidRef) {
    scrollViewRef = keyBoardAvoidRef;
  }
};

class ReviewPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showAddressVerification: true,
    };
  }

  componentDidMount() {
    const {
      reviewDidMount,
      setVenmoShippingState,
      setVenmoPickupState,
      scrollToTop,
      couponRemovedFromOrderFlag,
      toastMessage,
      couponRemovedErrorMsgLabel,
    } = this.props;
    setVenmoShippingState(true);
    setVenmoPickupState(true);
    reviewDidMount();
    setTimeout(() => {
      scrollToTop();
    }, 500);
    this.eddApiCalled = false;
    if (couponRemovedFromOrderFlag) {
      toastMessage(couponRemovedErrorMsgLabel);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      isPaymentDisabled: prevPaymentDisabled,
      selectedShippingCode: prevSelectedShippingCode,
      itemsWithEdd: prevItemsWithEdd,
    } = prevProps;
    const {
      isPaymentDisabled,
      dispatch,
      persistEddNodeAction,
      itemsWithEdd,
      selectedShippingCode,
      isEDD,
      bagLoading,
      setAfterPayProgress,
      afterPayInProgress,
      orderBalanceTotal,
    } = this.props;
    if (prevPaymentDisabled !== isPaymentDisabled) {
      dispatch(change(formName, 'cvvCode', null));
    }
    if (
      selectedShippingCode &&
      itemsWithEdd &&
      isEDD &&
      (!this.eddApiCalled ||
        prevSelectedShippingCode !== selectedShippingCode ||
        prevItemsWithEdd !== itemsWithEdd)
    ) {
      persistEddNodeAction();
      this.eddApiCalled = true;
    }
    if (!bagLoading && !orderBalanceTotal && afterPayInProgress) {
      setAfterPayProgress(false);
    }
  }

  componentWillUnmount() {
    const { setCouponRemovedFromOrderFlag } = this.props;
    setCouponRemovedFromOrderFlag(false);
  }

  /**
   * @function reviewFormSubmit
   * @description returns form submit data
   *
   */
  reviewFormSubmit = (data) => {
    const { submitReview, isExpressCheckout, navigation } = this.props;
    const { cvvCode } = data;
    const disableCheckoutAction = this.shouldDisableCheckoutCta();
    // Disable Submit for SFS checkout errors
    if (disableCheckoutAction) {
      return;
    }
    if (isExpressCheckout && cvvCode) {
      const formDataSubmission = {
        formData: {
          billing: {
            cvv: cvvCode,
          },
        },
        navigation,
      };
      submitReview(formDataSubmission);
    } else {
      submitReview({ navigation });
    }
  };

  renderFooter = () => {
    const { submitOrderRichText } = this.props;
    return <RichText source={{ html: submitOrderRichText !== null ? submitOrderRichText : '' }} />;
  };

  shouldDisableCheckoutCta = () => {
    const { checkoutServerError, isGuest, isSfsInvMessagingEnabled } = this.props;
    if (isSfsInvMessagingEnabled) {
      const giftServiceError = checkoutServerError && hasGiftServiceError(checkoutServerError);
      const rushShippingError = checkoutServerError && hasRushShippingError(checkoutServerError);
      const hasShippingAddressError = checkoutServerError && hasApoFpoError(checkoutServerError);
      if (!isGuest && (giftServiceError || rushShippingError || hasShippingAddressError)) {
        return true;
      }
    }
    return false;
  };

  closeAddAddressVerificationModal = () => {
    this.setState({ showAddressVerification: false });
  };

  submitAddressForm = (payloadParam) => {
    const { updateShippingAddressData, shippingProps } = this.props;
    const { userAddresses, shippingAddressId } = shippingProps;
    const selectedAddress =
      (userAddresses && userAddresses.find((item) => item.addressId === shippingAddressId)) || {};
    const payload = {
      shipTo: {
        address: {
          ...selectedAddress,
          ...payloadParam,
          onFileAddressKey: shippingAddressId,
          addressLine1: payloadParam.address1,
          addressLine2: payloadParam.address2,
          zipCode: payloadParam.zip,
          setAsDefault: selectedAddress.primary,
        },
      },
      shouldUpdateCart: true,
    };
    this.closeAddAddressVerificationModal();
    updateShippingAddressData(payload);
  };

  render() {
    const {
      navigation,
      labels,
      availableStages,
      orderHasShipping,
      orderHasPickUp,
      setCheckoutStage,
      handleSubmit,
      isExpressCheckout,
      shipmentMethods,
      dispatch,
      selectedShipmentId,
      isBonusPointsEnabled,
      bagLoading,
      checkoutProgressBarLabels,
      cartOrderItems,
      checkoutServerError,
      clearCheckoutServerError,
      globalLabels,
      addItemToSflList,
      addressLabels,
      formatPayload,
      updateShippingMethodSelection,
      shippingAddress,
      keyBoardAvoidRef,
      isGuest,
      afterPayInProgress,
      afterPayMinOrderAmount,
      afterPayMaxOrderAmount,
      trackClickAfterPay,
      setAnalyticsTriggered,
    } = this.props;
    const {
      header,
      backLinkBilling,
      nextSubmitText,
      labelAfterPaySubmit,
      labelAfterPayThresholdError,
    } = labels;
    const disableCheckoutAction = this.shouldDisableCheckoutCta();
    const { showAddressVerification } = this.state;
    return (
      <>
        <AddressVerification
          heading={addressLabels.addAddressHeading}
          shippingAddress={formatPayload(shippingAddress)}
          toggleAddressModal={this.closeAddAddressVerificationModal}
          fromReviewPage
          showAddressVerification={showAddressVerification}
          closeAddAddressVerificationModal={this.closeAddAddressVerificationModal}
          labelsFromShippingPage={labels}
          scrollViewRef={this.scrollViewRef}
          onReviewEditClick={() => {
            setCheckoutStage(CONSTANTS.SHIPPING_EDITING_DEFAULT_PARAM);
            this.closeAddAddressVerificationModal();
          }}
          onSuccess={this.submitAddressForm}
        />
        <CheckoutProgressIndicator
          activeStage="review"
          navigation={navigation}
          availableStages={availableStages}
          setCheckoutStage={setCheckoutStage}
          checkoutProgressBarLabels={checkoutProgressBarLabels}
        />
        <ScrollView keyboardShouldPersistTaps="handled">
          <Container>
            <CheckoutSectionTitleDisplay title={header} />
            {!!orderHasPickUp && (
              <PickUpReviewSectionContainer
                onEdit={() => {
                  setCheckoutStage(CONSTANTS.PICKUP_DEFAULT_PARAM);
                }}
                isExpressCheckout={isExpressCheckout}
                bagLoading={bagLoading}
              />
            )}

            {!!orderHasShipping && (
              <FormSection name="expressReviewShippingSection">
                <ShippingReviewSection
                  onEdit={() => {
                    setCheckoutStage(CONSTANTS.SHIPPING_DEFAULT_PARAM);
                  }}
                  isExpressCheckout={isExpressCheckout}
                  shipmentMethods={shipmentMethods}
                  dispatch={dispatch}
                  formName={formName}
                  formSection="expressReviewShippingSection"
                  selectedShipmentId={selectedShipmentId}
                  bagLoading={bagLoading}
                  createRefParent={(...params) => createRefParent(...params, keyBoardAvoidRef)}
                  cartOrderItems={cartOrderItems}
                  checkoutServerError={checkoutServerError}
                  clearCheckoutServerError={clearCheckoutServerError}
                  updateShippingMethodSelection={updateShippingMethodSelection}
                  addItemToSflList={addItemToSflList}
                  globalLabels={globalLabels}
                  handleSubmit={handleSubmit(this.reviewFormSubmit)}
                />
              </FormSection>
            )}

            <BillingSection
              onEdit={() => {
                setCheckoutStage(CONSTANTS.BILLING_DEFAULT_PARAM);
              }}
              isExpressCheckout={isExpressCheckout}
              bagLoading={bagLoading}
              createRefParent={(...params) => createRefParent(...params, keyBoardAvoidRef)}
            />
          </Container>
          <CheckoutCartItemList bagLoading={bagLoading} navigation={navigation} />
          <CnCTemplate
            isReviewPage
            navigation={navigation}
            btnText={afterPayInProgress ? labelAfterPaySubmit : nextSubmitText}
            routeToPage=""
            onPress={handleSubmit(this.reviewFormSubmit)}
            backLinkText={backLinkBilling}
            onBackLinkPress={() => setCheckoutStage(CONSTANTS.BILLING_DEFAULT_PARAM)}
            footerBody={this.renderFooter()}
            showAccordian
            pageCategory="review"
            bagLoading={bagLoading}
            isBonusPointsEnabled={isBonusPointsEnabled}
            pageName="checkout"
            pageSection="review"
            isGuest={isGuest}
            disableAction={disableCheckoutAction}
            afterPayInProgress={afterPayInProgress}
            afterPayMinOrderAmount={afterPayMinOrderAmount}
            afterPayMaxOrderAmount={afterPayMaxOrderAmount}
            labelAfterPayThresholdError={labelAfterPayThresholdError}
            trackClickAfterPay={trackClickAfterPay}
            setAnalyticsTriggered={setAnalyticsTriggered}
          />
        </ScrollView>
      </>
    );
  }
}

ReviewPage.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  checkoutProgressBarLabels: PropTypes.shape([]).isRequired,
  labels: PropTypes.shape({}).isRequired,
  orderHasShipping: PropTypes.bool.isRequired,
  orderHasPickUp: PropTypes.bool.isRequired,
  availableStages: PropTypes.func.isRequired,
  reviewDidMount: PropTypes.func.isRequired,
  submitReview: PropTypes.func.isRequired,
  setCheckoutStage: PropTypes.func.isRequired,
  isPaymentDisabled: PropTypes.bool,
  dispatch: PropTypes.func.isRequired,
  isExpressCheckout: PropTypes.bool,
  bagLoading: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  shipmentMethods: PropTypes.func.isRequired,
  selectedShipmentId: PropTypes.func.isRequired,
  setVenmoShippingState: PropTypes.func,
  isBonusPointsEnabled: PropTypes.bool.isRequired,
  setVenmoPickupState: PropTypes.func,
  submitOrderRichText: PropTypes.string,
  keyBoardAvoidRef: PropTypes.shape({}).isRequired,
  isGuest: PropTypes.bool,
  isSfsInvMessagingEnabled: PropTypes.bool,
  scrollToTop: PropTypes.func.isRequired,
  itemsWithEdd: PropTypes.shape([]),
  checkoutServerError: PropTypes.shape({}).isRequired,
  selectedShippingCode: PropTypes.string,
  persistEddNodeAction: PropTypes.func,
  clearCheckoutServerError: PropTypes.func.isRequired,
  globalLabels: PropTypes.shape({}).isRequired,
  formatPayload: PropTypes.func.isRequired,
  addressLabels: PropTypes.shape({}).isRequired,
  shippingAddress: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape([]),
  addItemToSflList: PropTypes.func,
  updateShippingMethodSelection: PropTypes.func.isRequired,
  isEDD: PropTypes.bool,
  updateShippingAddressData: PropTypes.func,
  shippingProps: PropTypes.shape({}),
  afterPayInProgress: PropTypes.bool,
  setAfterPayProgress: PropTypes.func,
  orderBalanceTotal: PropTypes.number,
  afterPayMinOrderAmount: 1,
  afterPayMaxOrderAmount: 1000,
  trackClickAfterPay: PropTypes.func,
  setAnalyticsTriggered: PropTypes.func,
};

ReviewPage.defaultProps = {
  setVenmoShippingState: () => {},
  setVenmoPickupState: () => {},
  isPaymentDisabled: false,
  isExpressCheckout: false,
  submitOrderRichText: null,
  isGuest: false,
  isSfsInvMessagingEnabled: false,
  itemsWithEdd: [],
  selectedShippingCode: '',
  cartOrderItems: [],
  addItemToSflList: () => {},
  persistEddNodeAction: () => {},
  isEDD: false,
  updateShippingAddressData: () => {},
  shippingProps: {},
  afterPayInProgress: false,
  setAfterPayProgress: () => {},
  orderBalanceTotal: 0,
  afterPayMinOrderAmount: PropTypes.number,
  afterPayMaxOrderAmount: PropTypes.number,
  trackClickAfterPay: () => {},
  setAnalyticsTriggered: () => {},
};

const validateMethod = createValidateMethod({
  pickUpAlternateExpress: ContactFormFields.ContactValidationConfig,
  ...getStandardConfig(['cvvCode']),
});

export default reduxForm({
  form: formName, // a unique identifier for this form
  ...validateMethod,
  enableReinitialize: true,
  shouldValidate: () => true,
  onSubmitFail: scrollToFirstErrorReviewPage,
})(ReviewPage);

export { ReviewPage as ReviewPageVanilla };
