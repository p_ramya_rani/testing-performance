// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { GiftServicesDetailsModalVanilla } from '../GiftServicesDetailsModal.view';

describe('GiftServices component', () => {
  it('renders correctly', () => {
    const props = {
      isOpen: true,
      labels: {
        DETAILS_RICH_TEXT: 'DETAILS_RICH_TEXT',
      },
    };
    const component = shallow(<GiftServicesDetailsModalVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
