// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CheckoutSelectors, {
  isGuest,
  isExpressCheckout,
  getMaxGiftCards,
} from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import { getIsRecapchaEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getCardList } from '../../../../../account/Payment/container/Payment.actions';
import {
  getGiftCards,
  checkbalanceValue,
  getCardListState,
} from '../../../../../account/Payment/container/Payment.selectors';
import GiftCard from '../views/GiftCards.view';
import GiftCardSelector from './GiftCards.selectors';
import GIFT_CARD_ACTIONS from './GiftCards.action';
import {
  setOrderBalanceTotal,
  setShowGiftCardForm,
  setHideGiftCardForm,
  resetAddGiftCard,
} from '../../../container/Checkout.action';
import { resetAddGiftCardSuccess } from '../../../container/Checkout.action.util';
import { toastMessageInfo } from '../../../../../../common/atoms/Toast/container/Toast.actions';
import { getFormValidationErrorMessages } from '../../../../../account/Account/container/Account.selectors';
import CHECKOUT_CONSTANTS from '../../../Checkout.constants';

const { getIsPaymentDisabled } = CheckoutSelectors;
export class GiftCardsContainer extends React.PureComponent {
  /* eslint-disable-next-line */
  UNSAFE_componentWillMount() {
    const { getCardListAction, isGuestUser, cardsList } = this.props;
    if (!isGuestUser && !cardsList) {
      getCardListAction({ ignoreCache: true });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      handleSetOrderBalanceTotal,
      itemOrderGrandTotal,
      itemsGiftCardTotal,
      addGiftCardResponse,
      hideAddGiftCard,
      getCardListAction,
      resetAddGiftCardAction,
    } = this.props;

    if (prevProps && prevProps.itemsGiftCardTotal !== itemsGiftCardTotal) {
      handleSetOrderBalanceTotal(itemOrderGrandTotal - itemsGiftCardTotal);
    }

    if (addGiftCardResponse === 'success') {
      hideAddGiftCard();
      getCardListAction();
      resetAddGiftCardAction();
    }
  }

  applyExistingGiftCardToOrder = giftCard => {
    const { itemOrderGrandTotal, handleApplyGiftCard } = this.props;
    const requestData = {
      creditCardId: giftCard.creditCardId,
      billingAddressId: giftCard.billingAddressId,
      orderGrandTotal: itemOrderGrandTotal,
      cardNumber: giftCard.accountNo,
      cardPin: giftCard.cardPin,
      balance: giftCard.balance,
    };

    handleApplyGiftCard(requestData);
  };

  submitGiftCardData = data => {
    const { handleSubmit, labels } = this.props;
    handleSubmit({
      giftcardAccountNumber: data.giftCardNumber,
      giftcardPin: data.cardPin,
      billingAddressId: data.billingAddressId,
      recaptchaToken: data.recaptchaToken,
      saveToAccount: data.saveToAccount,
      cardType: CHECKOUT_CONSTANTS.PAYMENT_CARD_TYPE.GC,
      labels,
    });
  };

  onClearError = () => {
    const { clearResetAddGiftCard } = this.props;
    clearResetAddGiftCard();
  };

  render() {
    const {
      giftCardList,
      appliedGiftCards,
      handleRemoveGiftCard,
      labels,
      giftCardErrors,
      itemOrderGrandTotal,
      itemsGiftCardTotal,
      toastMessage,
      showAddGiftCard,
      enableAddGiftCard,
      hideAddGiftCard,
      getAddGiftCardError,
      isRecapchaEnabled,
      isLoading,
      isExpressCheckoutUser,
      isFromReview,
      isPaymentDisabled,
      isGuestUser,
      isFetching,
      ServerErrors,
      maxGiftCards,
    } = this.props;

    let availableGiftCards = [];
    if (giftCardList && appliedGiftCards) {
      const appliedGiftCardsIds = appliedGiftCards.map(item => {
        return parseInt(item.get('onFileCardId'), 10);
      });

      availableGiftCards = giftCardList.filter(availableGiftCard => {
        return appliedGiftCardsIds.indexOf(availableGiftCard.creditCardId) === -1;
      });
    }

    const orderBalanceTotal = itemOrderGrandTotal - itemsGiftCardTotal;

    return (
      <GiftCard
        giftCardList={availableGiftCards}
        appliedGiftCards={appliedGiftCards}
        applyExistingGiftCardToOrder={this.applyExistingGiftCardToOrder}
        handleRemoveGiftCard={handleRemoveGiftCard}
        labels={labels}
        giftCardErrors={giftCardErrors}
        orderBalanceTotal={orderBalanceTotal}
        toastMessage={toastMessage}
        showAddGiftCard={showAddGiftCard}
        enableAddGiftCard={enableAddGiftCard}
        hideAddGiftCard={hideAddGiftCard}
        onAddGiftCardClick={this.submitGiftCardData}
        getAddGiftCardError={getAddGiftCardError}
        isGuestUser={isGuestUser}
        isRecapchaEnabled={isRecapchaEnabled}
        isLoading={isLoading}
        onClearError={this.onClearError}
        isExpressCheckout={isExpressCheckoutUser}
        isFromReview={isFromReview}
        isPaymentDisabled={isPaymentDisabled}
        isFetching={isFetching}
        ServerErrors={ServerErrors}
        maxGiftCards={maxGiftCards}
      />
    );
  }
}

export const mapDispatchToProps = dispatch => {
  return {
    getCardListAction: () => {
      dispatch(getCardList());
    },
    handleApplyGiftCard: giftCardData => {
      dispatch(GIFT_CARD_ACTIONS.applyGiftCard(giftCardData));
    },
    handleRemoveGiftCard: payload => {
      dispatch(GIFT_CARD_ACTIONS.removeGiftCard(payload));
    },
    handleSetOrderBalanceTotal: payload => {
      dispatch(setOrderBalanceTotal(payload));
    },
    toastMessage: palyoad => {
      dispatch(toastMessageInfo(palyoad));
    },
    showAddGiftCard: () => {
      dispatch(setShowGiftCardForm());
    },
    hideAddGiftCard: () => {
      dispatch(setHideGiftCardForm());
    },
    handleSubmit: giftCardData => {
      dispatch(GIFT_CARD_ACTIONS.addGiftCard(giftCardData));
    },
    resetAddGiftCardAction: () => {
      dispatch(resetAddGiftCardSuccess());
    },
    clearResetAddGiftCard: () => {
      dispatch(resetAddGiftCard());
    },
  };
};

const mapStateToProps = state => {
  const giftCardSectionLabels = GiftCardSelector.getGiftCardSectionLabels(state) || {};
  return {
    giftCardList: getGiftCards(state),
    cardsList: getCardListState(state),
    appliedGiftCards: GiftCardSelector.getAppliedGiftCards(state),
    itemOrderGrandTotal: GiftCardSelector.getGrandTotal(state),
    itemsGiftCardTotal: GiftCardSelector.getGiftCardsTotal(state),
    labels: {
      ...GiftCardSelector.getGiftSectionLabels(state),
      ...giftCardSectionLabels,
      giftCardAddUpToMsg: (giftCardSectionLabels.giftCardAddUpToMsg || '').replace(
        '#maxGiftCard#',
        getMaxGiftCards(state)
      ),
    },
    giftCardErrors: GiftCardSelector.getGiftCardErrors(state),
    enableAddGiftCard: GiftCardSelector.getShowAddGiftCard(state),
    formErrorMessage: getFormValidationErrorMessages(state),
    getAddGiftCardError: GiftCardSelector.getAddGiftCardErrors(state),
    giftCardBalance: checkbalanceValue(state),
    isRecapchaEnabled: getIsRecapchaEnabled(state),
    addGiftCardResponse: GiftCardSelector.getAddGiftCardResponse(state),
    isLoading: GiftCardSelector.getIsLoading(state),
    isExpressCheckoutUser: isExpressCheckout(state),
    isPaymentDisabled: getIsPaymentDisabled(state),
    isGuestUser: isGuest(state),
    maxGiftCards: getMaxGiftCards(state),
  };
};
GiftCardsContainer.propTypes = {
  getCardListAction: PropTypes.func,
  isGuestUser: PropTypes.bool,
  cardsList: PropTypes.shape({}),
  handleSetOrderBalanceTotal: PropTypes.func,
  itemOrderGrandTotal: PropTypes.number,
  itemsGiftCardTotal: PropTypes.number,
  addGiftCardResponse: PropTypes.string,
  hideAddGiftCard: PropTypes.func,
  resetAddGiftCardAction: PropTypes.func,
  handleApplyGiftCard: PropTypes.func,
  handleSubmit: PropTypes.func,
  labels: PropTypes.objectOf(PropTypes.shape({})),
  clearResetAddGiftCard: PropTypes.func,
  giftCardList: PropTypes.shape([]),
  appliedGiftCards: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  handleRemoveGiftCard: PropTypes.func,
  giftCardErrors: PropTypes.func,
  toastMessage: PropTypes.func,
  showAddGiftCard: PropTypes.func,
  enableAddGiftCard: PropTypes.bool,
  getAddGiftCardError: PropTypes.func,
  isRecapchaEnabled: PropTypes.bool,
  isLoading: PropTypes.bool,
  isExpressCheckoutUser: PropTypes.bool,
  isFromReview: PropTypes.bool,
  isPaymentDisabled: PropTypes.bool,
  isFetching: PropTypes.bool,
  ServerErrors: PropTypes.shape({}),
  maxGiftCards: PropTypes.number,
};
GiftCardsContainer.defaultProps = {
  getCardListAction: () => {},
  isGuestUser: false,
  cardsList: {},
  handleSetOrderBalanceTotal: () => {},
  itemOrderGrandTotal: 0,
  itemsGiftCardTotal: 0,
  addGiftCardResponse: PropTypes.string,
  hideAddGiftCard: () => {},
  resetAddGiftCardAction: () => {},
  handleApplyGiftCard: () => {},
  handleSubmit: () => {},
  labels: PropTypes.objectOf(PropTypes.shape({})),
  clearResetAddGiftCard: () => {},
  giftCardList: PropTypes.shape([]),
  handleRemoveGiftCard: () => {},
  giftCardErrors: () => {},
  toastMessage: () => {},
  showAddGiftCard: () => {},
  enableAddGiftCard: false,
  getAddGiftCardError: () => {},
  isRecapchaEnabled: false,
  isLoading: false,
  isExpressCheckoutUser: false,
  isFromReview: false,
  isPaymentDisabled: false,
  isFetching: false,
  ServerErrors: {},
  maxGiftCards: 0,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GiftCardsContainer);
