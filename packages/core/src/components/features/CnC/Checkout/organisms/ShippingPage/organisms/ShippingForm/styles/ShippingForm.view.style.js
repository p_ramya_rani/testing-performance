// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
  margin-top: ${props =>
    props.ServerErrors
      ? props.theme.spacing.ELEM_SPACING.MED
      : props.theme.spacing.ELEM_SPACING.LRG};
  .address-form {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
    @media ${props => props.theme.mediaQuery.medium} {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
    }
  }
  .shipment-methods-form {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .hide-on-desktop {
    @media ${props => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .select__input.select__input {
    font-size: ${props => props.theme.typography.fontSizes.fs13};
    font-family: ${props => props.theme.fonts.secondaryFontFamily};
    padding: ${props => props.theme.spacing.ELEM_SPACING.LRG} 1px
      ${props => props.theme.spacing.ELEM_SPACING.XS};
  }

  .select__label {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }

  @media ${props => props.theme.mediaQuery.mediumOnly} {
    .country-selector {
      height: 70px;
    }
  }

  .customSelectArrow {
    top: 70%;
    right: 0;
  }

  .verify-response-error {
    background: ${props => props.theme.colorPalette.gray[300]};
    align-items: flex-start;
  }
`;

export default styles;
