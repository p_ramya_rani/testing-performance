// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import ShippingForm from '../organisms/ShippingForm';
import AddressVerification from '../../../../../../common/organisms/AddressVerification/container/AddressVerification.container';
import {
  shippingPageGetDerivedStateFromProps,
  getAddressInitialValues,
  shippingPropsTypes,
  shippingDefaultProps,
  getPrimaryAddress,
} from './ShippingPage.view.utils';

export default class ShippingPage extends React.PureComponent {
  static propTypes = shippingPropsTypes;

  static defaultProps = shippingDefaultProps;

  constructor(props) {
    super(props);
    this.state = {
      isAddNewAddress: false,
      defaultAddressId: null,
    };
  }

  componentDidUpdate(prevProps) {
    const { shippingDidMount, isRegisteredUserCallDone, shippingDidUpdate } = this.props;
    const { isRegisteredUserCallDone: prevIsRegisteredUserCallDone } = prevProps;
    if (prevIsRegisteredUserCallDone !== isRegisteredUserCallDone && isRegisteredUserCallDone) {
      shippingDidMount();
    }
    shippingDidUpdate(prevProps);
  }

  componentWillUnmount() {
    const { clearCheckoutServerError, checkoutServerError } = this.props;
    if (checkoutServerError) {
      clearCheckoutServerError({});
    }
  }

  static getDerivedStateFromProps = shippingPageGetDerivedStateFromProps;

  setDefaultAddressId = id => {
    this.setState({ defaultAddressId: id });
  };

  toggleAddNewAddress = () => {
    const { isAddNewAddress } = this.state;
    this.setState({ isAddNewAddress: !isAddNewAddress });
  };

  render() {
    const {
      smsSignUpLabels,
      addressPhoneNumber,
      selectedShipmentId,
      emailSignUpLabels,
      isGuest,
      isUsSite,
      orderHasPickUp,
      shipmentMethods,
      defaultShipmentId,
      loadShipmentMethods,
      routeToPickupPage,
      isSaveToAddressBookChecked,
      userAddresses,
      onFileAddressKey,
      isLoadingShippingMethods,
      emailSignUpFlags,
      addNewShippingAddress,
      updateShippingAddress,
      initShippingPage,
      smsOrderUpdatesRichText,
      emailSignUpCaRichText,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      updateEdd,
      resetShippingEddData,
      isEddABTest,
    } = this.props;
    const {
      isMobile,
      newUserPhoneNo,
      shippingAddressId,
      submitShippingForm,
      clearCheckoutServerError,
      removeGiftWrapping,
    } = this.props;
    const { setAsDefaultShipping, labels, address, syncErrors } = this.props;
    const { isSubmitting, formatPayload, ServerErrors, checkoutServerError } = this.props;
    const { shippingAddress, isVenmoPaymentInProgress, isVenmoShippingDisplayed } = this.props;
    const {
      addressLabels,
      isOrderUpdateChecked,
      isGiftServicesChecked,
      isGiftOptionsEnabled,
    } = this.props;
    const {
      toggleCountrySelector,
      pageCategory,
      submitVerifiedShippingAddressData,
      checkoutRoutingDone,
      bagLoading,
      shippingAddressData,
      formErrorMessage,
      isEDD,
      cartOrderItems,
      eddAllowedStatesArr,
      currentOrderId,
      formMetaInfo,
      formSyncError,
      submitErrors,
      pageName,
      appliedExpiredCoupon,
      addItemToSflList,
      editShippingAddress,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      setEditShippingFlag,
    } = this.props;
    const primaryAddressId = getPrimaryAddress(this);
    const { isAddNewAddress, isEditing, defaultAddressId } = this.state;
    return (
      <>
        {((shipmentMethods && shipmentMethods.length > 0) || !checkoutRoutingDone) && (
          <>
            <ShippingForm
              checkoutRoutingDone={checkoutRoutingDone}
              bagLoading={bagLoading}
              toggleCountrySelector={toggleCountrySelector}
              emailSignUpFlags={emailSignUpFlags}
              checkoutServerError={checkoutServerError}
              isSubmitting={isSubmitting}
              routeToPickupPage={routeToPickupPage}
              addressLabels={addressLabels}
              isOrderUpdateChecked={isOrderUpdateChecked}
              isGiftServicesChecked={isGiftServicesChecked}
              smsSignUpLabels={smsSignUpLabels}
              initialValues={{
                address: getAddressInitialValues(this),
                shipmentMethods: { shippingMethodId: defaultShipmentId },
                saveToAddressBook: !isGuest,
                onFileAddressKey: '' || primaryAddressId,
                emailSignUp: {
                  emailSignUpTCP: emailSignUpFlags.emailSignUpTCP,
                  emailSignUpGYM: emailSignUpFlags.emailSignUpGYM,
                },
                smsSignUp: initShippingPage && initShippingPage.smsSignUp,
              }}
              selectedShipmentId={selectedShipmentId}
              addressPhoneNo={addressPhoneNumber}
              onSubmit={submitShippingForm(this)}
              emailSignUpLabels={emailSignUpLabels}
              isGuest={isGuest}
              isUsSite={isUsSite}
              orderHasPickUp={orderHasPickUp}
              shipmentMethods={shipmentMethods}
              loadShipmentMethods={loadShipmentMethods}
              defaultShipmentId={defaultShipmentId}
              isSaveToAddressBookChecked={isSaveToAddressBookChecked}
              userAddresses={userAddresses}
              onFileAddressKey={onFileAddressKey}
              isMobile={isMobile}
              newUserPhoneNo={newUserPhoneNo}
              defaultAddressId={defaultAddressId}
              shippingAddressId={shippingAddressId}
              isAddNewAddress={isAddNewAddress}
              isEditing={isEditing}
              toggleAddNewAddress={this.toggleAddNewAddress}
              updateShippingAddress={updateShippingAddress(this)}
              setAsDefaultShipping={setAsDefaultShipping}
              addNewShippingAddress={addNewShippingAddress}
              labels={labels}
              address={address}
              setDefaultAddressId={this.setDefaultAddressId}
              syncErrorsObject={syncErrors}
              shippingAddress={shippingAddress}
              isVenmoPaymentInProgress={isVenmoPaymentInProgress}
              isVenmoShippingDisplayed={isVenmoShippingDisplayed}
              ServerErrors={ServerErrors}
              pageCategory={pageCategory}
              isLoadingShippingMethods={isLoadingShippingMethods}
              isGiftOptionsEnabled={isGiftOptionsEnabled}
              smsOrderUpdatesRichText={smsOrderUpdatesRichText}
              emailSignUpCaRichText={emailSignUpCaRichText}
              formErrorMessage={formErrorMessage}
              zipCodeABTestEnabled={zipCodeABTestEnabled}
              getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
              zipCodeLoader={zipCodeLoader}
              isEDD={isEDD}
              updateEdd={updateEdd}
              cartOrderItems={cartOrderItems}
              eddAllowedStatesArr={eddAllowedStatesArr}
              currentOrderId={currentOrderId}
              formMetaInfo={formMetaInfo}
              formSyncError={formSyncError}
              resetShippingEddData={resetShippingEddData}
              isEddABTest={isEddABTest}
              submitErrors={submitErrors}
              pageName={pageName}
              appliedExpiredCoupon={appliedExpiredCoupon}
              addItemToSflList={addItemToSflList}
              clearCheckoutServerError={clearCheckoutServerError}
              removeGiftWrapping={removeGiftWrapping}
              editShippingAddress={editShippingAddress}
              mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
              mapboxSwitch={mapboxSwitch}
              setEditShippingFlag={setEditShippingFlag}
            />
            <AddressVerification
              onSuccess={submitVerifiedShippingAddressData(this)}
              heading={addressLabels.addAddressHeading}
              onError={submitVerifiedShippingAddressData(this)}
              shippingAddress={formatPayload(shippingAddressData)}
            />
          </>
        )}
      </>
    );
  }
}
