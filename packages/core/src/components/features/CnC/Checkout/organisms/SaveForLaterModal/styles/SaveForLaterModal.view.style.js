// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .primary-heading {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      padding-right: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
  .cta-btn-container {
    text-align: center;
  }
  .sfs-cta {
    margin: ${props => props.theme.spacing.ELEM_SPACING.LRG} 0
      ${props => props.theme.spacing.ELEM_SPACING.LRG}
      ${props => props.theme.spacing.ELEM_SPACING.LRG};
    max-width: 339px;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      margin-left: 0;
      margin-bottom: 0;
    }
  }
  .item-tile {
    box-shadow: 0 0 7px 0 rgba(0, 0, 0, 0.12);
  }
  .items-container {
    max-height: 178px;
    overflow-y: auto;
  }
  .item-thumb {
    width: 68px;
  }
  .cart-item-name {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      font-size: ${props => `${props.theme.fonts.fontSize.heading.small.h4}px`};
    }
  }
`;

export default styles;
