// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { getIconPath, getViewportInfo } from '@tcp/core/src/utils';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/WarningPrompt.view.style';
import SaveForLater from '../../SaveForLaterModal';

const WarningPromptView = ({
  className,
  heading,
  body,
  type,
  cartOrderItems,
  scrollIntoView,
  itemIds,
  saveForLaterAction,
  labels,
  secondaryCtaAction,
}) => {
  const [showSaveForLaterModal, toggleSaveForLaterModal] = useState(false);
  const secondaryHeading = body.replace('X', itemIds.length);
  const errorEl = useRef();

  const onClickCallback = e => {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    toggleSaveForLaterModal(true);
  };

  useEffect(() => {
    if (scrollIntoView) {
      const { isMobile } = getViewportInfo();
      if (errorEl && isMobile)
        errorEl.current.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }, []);

  return (
    <>
      <div
        className={`${className} elem-pt-SM elem-pr-LRG elem-pb-SM elem-pl-LRG elem-mb-LRG elem-mt-LRG`}
        ref={errorEl}
      >
        <img
          alt="warning"
          src={getIconPath('circle-warning-fill')}
          className="notification__image elem-mr-MED"
        />
        <div className="heading-container">
          <BodyCopy fontSize="fs14" fontWeight="extrabold" fontFamily="secondary">
            {heading}
          </BodyCopy>
          <BodyCopy fontSize="fs14" fontWeight="extrabold" fontFamily="secondary">
            {secondaryHeading}
          </BodyCopy>
        </div>
        <button onClick={onClickCallback} className="right-arrow">
          right arrow
        </button>
      </div>
      {showSaveForLaterModal && (
        <SaveForLater
          primaryHeading={heading}
          secondaryHeading={secondaryHeading}
          openState={showSaveForLaterModal}
          type={type}
          toggleModal={toggleSaveForLaterModal}
          cartOrderItems={cartOrderItems}
          itemIds={itemIds}
          saveForLaterAction={saveForLaterAction}
          labels={labels}
          secondaryCtaAction={secondaryCtaAction}
        />
      )}
    </>
  );
};

WarningPromptView.propTypes = {
  className: PropTypes.string.isRequired,
  heading: PropTypes.string,
  body: PropTypes.string,
  type: PropTypes.string,
  cartOrderItems: PropTypes.shape({}),
  itemIds: PropTypes.number.isRequired,
  saveForLaterAction: PropTypes.func,
  labels: PropTypes.shape({}).isRequired,
  secondaryCtaAction: PropTypes.func,
  scrollIntoView: PropTypes.bool,
};

WarningPromptView.defaultProps = {
  heading: '',
  body: '',
  type: '',
  cartOrderItems: [],
  saveForLaterAction: () => {},
  secondaryCtaAction: () => {},
  scrollIntoView: false,
};

export default withStyles(WarningPromptView, styles);
export { WarningPromptView as WarningPromptViewVanilla };
