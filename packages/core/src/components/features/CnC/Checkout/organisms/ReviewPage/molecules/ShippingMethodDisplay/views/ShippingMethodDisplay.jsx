// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import EddComponent from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent';
import BodyCopy from '../../../../../../../../common/atoms/BodyCopy';
import withStyles from '../../../../../../../../common/hoc/withStyles';
import styles from '../styles/ShippingMethodDisplay.style';

class ShippingMethodDisplay extends React.PureComponent {
  render() {
    const { className, displayName, labels, isEDD, shippingCode, hideSubtitle } = this.props;
    const { lbl_review_sectionShippingMethodTitle: title } = labels;
    const eddDisplayName = hideSubtitle ? '' : displayName;
    return (
      <div className={className}>
        <div className="shipping-method-container">
          <BodyCopy
            fontSize="fs16"
            dataLocator=""
            fontFamily="secondary"
            color="gray.900"
            fontWeight="extrabold"
            className="heading"
          >
            {title}
          </BodyCopy>
          <BodyCopy
            fontSize="fs16"
            dataLocator=""
            fontFamily="secondary"
            color="gray.900"
            fontWeight="regular"
          >
            {displayName}
          </BodyCopy>
          {isEDD && (
            <EddComponent
              selectedShipment={shippingCode}
              subtitle={eddDisplayName}
              isRangeDate
              textAlign="left"
              fontSize="fs16"
            />
          )}
        </div>
      </div>
    );
  }
}

ShippingMethodDisplay.propTypes = {
  className: PropTypes.string.isRequired,
  displayName: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
  shippingCode: PropTypes.string,
  isEDD: PropTypes.bool,
  hideSubtitle: PropTypes.bool,
};

ShippingMethodDisplay.defaultProps = {
  labels: {
    lbl_review_sectionShippingMethodTitle: 'Shipping Method',
  },
  shippingCode: '',
  isEDD: false,
  hideSubtitle: false,
};

export default withStyles(ShippingMethodDisplay, styles);
export { ShippingMethodDisplay as ShippingMethodDisplayanilla };
