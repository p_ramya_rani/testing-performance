// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { scrollToFirstErrorApp } from '@tcp/core/src/components/features/CnC/Checkout/util/utility';
import AddressFields from '../../../../../../common/molecules/AddressFields';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import constants from '../container/CreditCard.constants';

export const handleEditFromSubmit = updateCardDetail => (data, dispatch, props = {}) => {
  const { selectedCard = {} } = props;
  const { creditCardId } = selectedCard;
  const formData = data;
  formData.onFileAddressKey = formData.address.addressId || '';
  if (creditCardId) {
    formData.creditCardId = creditCardId;
  }
  return new Promise((resolve, reject) => updateCardDetail({ formData, resolve, reject }));
};

const refObj = {};
let scrollViewRef = false;

const createRefParent = (node, refName, keyBoardAvoidRef) => {
  if (node) {
    refObj[refName] = node;
  }
  if (keyBoardAvoidRef) {
    scrollViewRef = keyBoardAvoidRef;
  }
};

const scrollToFirstErrorCardEdit = errors => {
  scrollToFirstErrorApp(errors, refObj, scrollViewRef);
};

export const withCardEditReduxForm = Component => {
  const CardEditReduxForm = React.memo(props => {
    const {
      selectedCard,
      addressForm: AddressForm,
      updateCardDetail,
      renderCardDetailsHeading,
      getAddNewCCForm,
      unsetFormEditState,
      onEditCardFocus,
      labels,
      dispatch,
      editModeSubmissionError,
      errorMessageRef,
      getDefaultPayment,
      toastMessage,
      cardType,
      keyBoardAvoidRef,
      isPLCCEnabled,
      setCardType,
    } = props;
    const {
      accountNo,
      expMonth,
      expYear,
      addressDetails: address,
      creditCardId,
      defaultInd,
    } = selectedCard;
    const validateMethod = createValidateMethod({
      address: AddressFields.addressValidationConfig,
      ...getStandardConfig(['cardNumber', 'expYear', 'expMonth']),
    });
    const CartEditForm = reduxForm({
      form: constants.EDIT_FORM_NAME, // a unique identifier for this form
      enableReinitialize: true,
      keepDirtyOnReinitialize: true,
      destroyOnUnmount: false,
      ...validateMethod,
      onSubmitSuccess: () => {
        unsetFormEditState();
      },
      onSubmitFail: scrollToFirstErrorCardEdit,
    })(Component);

    return (
      <CartEditForm
        key="cardEditForm"
        onSubmit={handleEditFromSubmit(updateCardDetail)}
        initialValues={{
          cardNumber: accountNo,
          expMonth: expMonth && expMonth.trim(),
          expYear,
          address,
          creditCardId,
          isDefault: defaultInd,
          sameAsShipping: false,
        }}
        cardType={cardType}
        dispatch={dispatch}
        onEditCardFocus={onEditCardFocus}
        AddressForm={AddressForm}
        renderCardDetailsHeading={renderCardDetailsHeading}
        getAddNewCCForm={getAddNewCCForm}
        unsetFormEditState={unsetFormEditState}
        labels={labels}
        editModeSubmissionError={editModeSubmissionError}
        errorMessageRef={errorMessageRef}
        getDefaultPayment={getDefaultPayment}
        selectedCard={selectedCard}
        toastMessage={toastMessage}
        createRefParent={(...params) => createRefParent(...params, keyBoardAvoidRef)}
        isPLCCEnabled={isPLCCEnabled}
        setCardType={setCardType}
      />
    );
  });

  CardEditReduxForm.propTypes = {
    labels: PropTypes.shape({
      saveButtonText: PropTypes.string,
      cancelButtonText: PropTypes.string,
    }).isRequired,
    renderCardDetailsHeading: PropTypes.func.isRequired,
    setCardType: PropTypes.bool.isRequired,
    getAddNewCCForm: PropTypes.func.isRequired,
    unsetFormEditState: PropTypes.func.isRequired,
    addressForm: PropTypes.shape({}).isRequired,
    onEditCardFocus: PropTypes.func.isRequired,
    updateCardDetail: PropTypes.func.isRequired,
    editModeSubmissionError: PropTypes.string.isRequired,
    cardType: PropTypes.string.isRequired,
    errorMessageRef: PropTypes.shape({}).isRequired,
    dispatch: PropTypes.func.isRequired,
    selectedCard: PropTypes.shape({
      accountNo: PropTypes.string,
      expMonth: PropTypes.string,
      expYear: PropTypes.string,
      addressDetails: PropTypes.shape({}),
    }).isRequired,
    getDefaultPayment: PropTypes.func.isRequired,
    toastMessage: PropTypes.func.isRequired,
    keyBoardAvoidRef: PropTypes.shape({}).isRequired,
    isPLCCEnabled: PropTypes.bool.isRequired,
  };
  return CardEditReduxForm;
};
