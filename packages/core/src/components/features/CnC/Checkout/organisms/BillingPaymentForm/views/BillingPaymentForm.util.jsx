// 9fbef606107a605d69c0edbcd8029e5d
import { change, reset } from 'redux-form';
import { capitalize } from '@tcp/core/src/utils';
import constants from '../container/CreditCard.constants';
import { onAddNewCreditCardUpdate, renderAddressError } from './BillingPaymentForm.view.util';

const onEditCardFocus = (scope) => {
  const { dispatch } = scope.props;
  dispatch(change(constants.EDIT_FORM_NAME, 'cardNumber', ''));
};

const setFormToEditState = (scope, e) => {
  e.preventDefault();
  scope.setState({ editMode: true });
};

const unsetPaymentFormEditState = (scope, e) => {
  /* istanbul ignore else */
  if (e) {
    e.preventDefault();
  }
  const { dispatch } = scope.props;
  dispatch(reset(constants.EDIT_FORM_NAME));
  scope.setState({ editMode: false, editModeSubmissionError: '' });
};

const handleBillingFormSubmit = (scope, e, isMobile, otherParams = {}) => {
  const { handleSubmit, labels, scrollView } = scope.props;
  const { trackClickAfterPay, afterPayInProgress, pageCategory = '' } = otherParams;
  const { editMode } = scope.state;

  if (editMode) {
    /* istanbul ignore else */
    if (e && e.preventDefault) {
      e.preventDefault();
    }
    scope.setState({ editModeSubmissionError: labels.cardEditUnSavedError });
    return isMobile
      ? scrollView.scrollTo({ x: 0, y: 1300, animated: true })
      : scope.ediCardErrorRef.current.scrollIntoView(false);
  }
  if (afterPayInProgress && typeof trackClickAfterPay === 'function') {
    if (!isMobile) {
      trackClickAfterPay({
        afterpay_metrics141: 1,
        afterpay_cd131: capitalize(pageCategory),
        customEvents: ['event141'],
      });
    } else {
      trackClickAfterPay(
        {
          afterpay_metrics_v141: 1,
          afterpay_location_v131: capitalize(pageCategory),
          customEvents: ['event141'],
        },
        { name: 'afterpay_click', module: 'checkout' }
      );
    }
  }
  return handleSubmit(e);
};

/**
 * @function onAddNewCreditCardClick
 * @description sets the add new credit card state as true
 */
const onAddNewCreditCardClick = (scope) => {
  const { dispatch } = scope.props;
  scope.setState({ addNewCCState: true });
  onAddNewCreditCardUpdate(dispatch);
};

// TODO: APAY Saurabh -add labels from CMS for paypal and apple pay

const getPaymentMethods = (
  labels,
  isVenmoEnabled = true,
  isVenmoAppInstalled = true,
  isPayPalEnabled = true,
  isApplePayEnabled = true,
  isAfterPayEnabled = false
) => {
  const paymentTabs = [
    { id: constants.PAYMENT_METHOD_CREDIT_CARD, displayName: labels.creditCard },
  ];

  if (isPayPalEnabled) {
    paymentTabs.push({ id: constants.PAYMENT_METHOD_PAY_PAL, displayName: labels.paypal });
  }

  if (isAfterPayEnabled) {
    paymentTabs.push({ id: constants.PAYMENT_METHOD_AFTERPAY, displayName: labels.afterpay });
  }

  if (isApplePayEnabled) {
    paymentTabs.push({ id: constants.PAYMENT_METHOD_APPLE_PAY, displayName: labels.applePay });
  }

  if (isVenmoEnabled && isVenmoAppInstalled) {
    paymentTabs.push({ id: constants.PAYMENT_METHOD_VENMO, displayName: labels.venmo });
  }

  return paymentTabs;
};

/**
 * @function onAddCreditCardClick
 * @description sets the add new credit card state as true
 */
const onAddCreditCardClick = (scope) => {
  const { dispatch } = scope.props;
  scope.setState({ addNewCCState: true });
  dispatch(change(constants.FORM_NAME, 'cardNumber', ''));
  dispatch(change(constants.FORM_NAME, 'expMonth', ''));
  dispatch(change(constants.FORM_NAME, 'expYear', ''));
  dispatch(change(constants.FORM_NAME, 'cvvCode', ''));
};

/**
 * @function onCCDropDownChange
 * @description sets the add new credit card state to false if it is true
 */
const onCCDropDownChange = (scope) => {
  const { addNewCCState } = scope.state;
  if (addNewCCState) {
    scope.setState({ addNewCCState: false });
  }
  const { dispatch, setSaveCardChoiceAction = () => {} } = scope.props;
  dispatch(change(constants.FORM_NAME, 'cvvCode', ''));
  setSaveCardChoiceAction(null);
};

/**
 * @description - Show Venmo if payment method is Venmo and Payments tab is enabled
 */
const showVenmoButton = (scope) => {
  const { paymentMethodId, isPaymentDisabled } = scope.props;
  return paymentMethodId === constants.PAYMENT_METHOD_VENMO && !isPaymentDisabled;
};

export {
  onEditCardFocus,
  setFormToEditState,
  unsetPaymentFormEditState,
  handleBillingFormSubmit,
  onAddNewCreditCardClick,
  getPaymentMethods,
  onAddCreditCardClick,
  onCCDropDownChange,
  renderAddressError,
  showVenmoButton,
};
