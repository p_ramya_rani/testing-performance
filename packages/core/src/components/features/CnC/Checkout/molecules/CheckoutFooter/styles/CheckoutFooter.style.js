// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
  .left-arrow {
    border: solid ${props => props.theme.colors.ANCHOR.SECONDARY};
    border-width: 0 2px 2px 0;
    display: inline-block;
    padding: 5px;
    transform: rotate(135deg);
  }
  @media ${props => props.theme.mediaQuery.medium} {
    border-top: 1px solid ${props => props.theme.colors.PRIMARY.GRAY};
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }
  .footer-body-container {
    font-family: ${props => props.theme.typography.fonts.secondary};
    font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy1}px;
    text-align: right;
    color: ${props => props.theme.colorPalette.gray[800]};
    @media ${props => props.theme.mediaQuery.smallOnly} {
      text-align: center;
    }
  }
  .footer-buttons {
    flex-direction: column;
    display: flex;
    justify-content: space-between;
    box-sizing: border-box;
    align-items: center;

    --afterpay-button-width: 100%;
    --afterpay-button-height: 100%;

    .after-pay-container {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      opacity: 0;
      overflow: hidden;
      cursor: pointer;
    }

    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.XL};
      width: auto;
      flex-direction: row;
    }
    @media ${props => props.theme.mediaQuery.smallOnly} {
      ${props => (props.showPayPalButton ? 'flex-direction: column-reverse' : null)};
    }

    .back-space {
      margin-left: -7px;
    }
    .back-link {
      padding-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
      color: ${props => props.theme.colors.PRIMARY.BLUE};
      font-size: ${props => props.theme.typography.fontSizes.fs16};
      font-family: ${props => props.theme.typography.fonts.secondary};
      text-transform: none;
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
      outline: none;
      letter-spacing: normal;
      &:focus,
      &:active {
        background-color: transparent;
      }
      @media ${props => props.theme.mediaQuery.medium} {
        width: auto;
        margin-top: unset;
      }
      @media ${props => props.theme.mediaQuery.smallOnly} {
        display: none;
      }
    }

    .back-link-image {
      position: absolute;
      top: ${props => props.theme.spacing.APP_LAYOUT_SPACING.MED};
      left: 0;
      width: 9px;
      height: 18px;
    }
    .footer-button {
      background-color: ${props => props.theme.colorPalette.blue.C900};
      color: ${props => props.theme.colors.WHITE};
      font-size: ${props => props.theme.typography.fontSizes.fs14};
      font-weight: ${props => props.theme.typography.fontWeights.fontWeights};
      &:hover {
        background: ${props => props.theme.colorPalette.blue.C900};
      }
      position: relative;
    }
    .footer-button-web {
      display: none;
      @media ${props => props.theme.mediaQuery.medium} {
        display: flex;
        justify-content: center;
        white-space: nowrap;
      }
      @media ${props => props.theme.mediaQuery.mediumMax} {
        min-width: 192px;
        height: 42px;
      }
      @media ${props => props.theme.mediaQuery.large} {
        height: 51px;
        min-width: 210px;
      }
    }
    .footer-button-mob {
      height: 42px;
      width: 100%;
      @media ${props => props.theme.mediaQuery.medium} {
        display: none;
      }
    }
    .footer-button-mob-container {
      width: 100%;
      @media ${props => props.theme.mediaQuery.medium} {
        display: none;
      }
    }
    .afterpay-button-web {
      display: none;
      @media ${props => props.theme.mediaQuery.medium} {
        display: block;
      }
    }
    .afterpay-mobile-web {
      max-width: 100%;
      @media ${props => props.theme.mediaQuery.medium} {
        display: none;
      }
    }
    .footer-venmo-button {
      display: none;
      @media ${props => props.theme.mediaQuery.smallOnly} {
        display: block;
        width: 100%;
      }
    }

    .footer-paypal-button {
      position: relative;
      width: 450px;
      @media ${props => props.theme.mediaQuery.mediumOnly} {
        width: 192px;
      }
      @media ${props => props.theme.mediaQuery.smallOnly} {
        display: block;
        width: 100%;
      }
    }
    .footer-apple-button {
      position: relative;
      width: 450px;
      @media ${props => props.theme.mediaQuery.mediumOnly} {
        width: 192px;
      }
      @media ${props => props.theme.mediaQuery.smallOnly} {
        display: block;
        width: 100%;
      }
    }
  }
  .submit-disclaimer {
    font-family: ${props => props.theme.typography.fonts.secondary};
    font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy1}px;
    color: ${props => props.theme.colorPalette.gray[800]};
    text-align: center;
    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
      margin-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
    }
    @media ${props => props.theme.mediaQuery.large} {
      float: right;
    }
  }

  .submit-disclaimer-link {
    display: inline-block;
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
    margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
  }
  .after-pay-error {
    max-width: 270px;
    img {
      @media ${props => props.theme.mediaQuery.medium} {
        vertical-align: top;
      }
    }
    p {
      @media ${props => props.theme.mediaQuery.medium} {
        display: inline-block;
        width: calc(100% - 50px);
      }
    }
  }
`;

export default styles;
