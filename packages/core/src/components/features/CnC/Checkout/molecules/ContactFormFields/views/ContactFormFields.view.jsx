// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/ContactFormFields.style';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import TextBox from '../../../../../../common/atoms/TextBox';
import Row from '../../../../../../common/atoms/Row';
import Col from '../../../../../../common/atoms/Col';
import { formatPhoneNumber } from '../../../../../../../utils/formValidation/phoneNumber';

class ContactFormFields extends React.Component {
  static ContactValidationConfig = getStandardConfig([
    'firstName',
    'firstName_alt',
    'lastName',
    'lastName_alt',
    'emailAddress',
    'emailAddress_alt',
    'phoneNumber',
  ]);

  render() {
    const {
      className,
      showEmailAddress,
      showPhoneNumber,
      labels,
      isExpressCheckout,
      isAlternate,
    } = this.props;
    const colSizeLarge = isExpressCheckout ? 12 : 6;
    return (
      <div className={className}>
        <Row fullBleed>
          <Col
            className="pickupField fieldFirstName"
            colSize={{ small: 6, medium: 8, large: colSizeLarge }}
          >
            <Field
              placeholder={labels.firstName}
              name={isAlternate ? 'firstName_alt' : 'firstName'}
              id={isAlternate ? 'firstName_alt' : 'firstName'}
              component={TextBox}
              dataLocator="pickup-first-name"
              enableSuccessCheck={false}
            />
            <BodyCopy fontSize="fs12" fontFamily="secondary" fontWeight="regular">
              {labels.govIdText}
            </BodyCopy>
          </Col>
          <Col
            className="pickupField fieldLastName"
            colSize={{ small: 6, medium: 8, large: colSizeLarge }}
          >
            <Field
              placeholder={labels.lastName}
              name={isAlternate ? 'lastName_alt' : 'lastName'}
              id={isAlternate ? 'lastName_alt' : 'lastName'}
              component={TextBox}
              dataLocator="pickup-last-name"
              enableSuccessCheck={false}
            />
          </Col>

          {showEmailAddress && (
            <Col
              className="pickupField fieldEmail"
              colSize={{ small: 6, medium: 8, large: colSizeLarge }}
            >
              <Field
                id={isAlternate ? 'emailAddress_alt' : 'emailAddress'}
                placeholder={labels.email}
                name={isAlternate ? 'emailAddress_alt' : 'emailAddress'}
                component={TextBox}
                dataLocator="pickup-email"
                errorDataLocator="pickup-email-error"
                showSuccessCheck={false}
                enableSuccessCheck={false}
              />
            </Col>
          )}
          {showPhoneNumber && (
            <Col
              className="pickupField fieldNumber"
              colSize={{ small: 6, medium: 8, large: colSizeLarge }}
            >
              <Field
                placeholder={labels.mobile}
                name="phoneNumber"
                id="phoneNumber"
                type="tel"
                component={TextBox}
                maxLength={50}
                dataLocator="pickup-phone-number"
                enableSuccessCheck={false}
                normalize={formatPhoneNumber}
              />
            </Col>
          )}
        </Row>
      </div>
    );
  }
}

ContactFormFields.propTypes = {
  className: PropTypes.string,
  showEmailAddress: PropTypes.bool,
  showPhoneNumber: PropTypes.bool,
  labels: PropTypes.shape({}).isRequired,
  isExpressCheckout: PropTypes.bool,
  isAlternate: PropTypes.bool,
};

ContactFormFields.defaultProps = {
  className: '',
  showEmailAddress: false,
  showPhoneNumber: false,
  isExpressCheckout: false,
  isAlternate: false,
};

export default withStyles(ContactFormFields, styles);
export { ContactFormFields as ContactFormFieldsVanilla };
