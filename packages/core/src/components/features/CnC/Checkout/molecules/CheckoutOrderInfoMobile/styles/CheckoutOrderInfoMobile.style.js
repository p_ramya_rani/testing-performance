// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  @media ${props => props.theme.mediaQuery.medium} {
    display: none;
  }
  .order-summary {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  }

  @media ${props => props.theme.mediaQuery.smallMax} {
    .order-summary {
      margin-left: -14px;
      margin-right: -14px;
      padding: 0;
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .venmo-payment-method-wrapper {
    display: flex;
    flex-direction: row;
    text-align: left;
  }

  .cardImage-wrapper {
    display: flex;
  }

  .cardImage-img-wrapper {
    display: flex;
    margin-bottom: auto;
    border: 1px solid ${props => props.theme.colorPalette.gray[500]};
    border-radius: ${props => props.theme.spacing.ELEM_SPACING.XS};
    background-color: ${props => props.theme.colors.WHITE};

    .cardImage-img {
      width: 47px;
      height: 30px;
    }
  }

  .venmo-paid-text {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    margin-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }

  .cardImage-card-number {
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }
`;

export default styles;
