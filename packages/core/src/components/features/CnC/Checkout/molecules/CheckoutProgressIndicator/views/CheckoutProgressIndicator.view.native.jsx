// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes } from 'prop-types';
import { View } from 'react-native';
import { getLocator } from '@tcp/core/src/utils';
import Anchor from '../../../../../../common/atoms/Anchor';

import {
  StepIndicatorContainer,
  ProgressStep,
  ProgressDot,
  ProgressBar,
  ProgressDotIcon,
  StyledDisableLabels,
  CheckoutProgressBar,
  ProgressDotActive,
  StyledAnchor,
  StyledAnchorCompleted,
} from '../styles/CheckoutProgressIndicator.style.native';

const completedStage = require('../../../../../../../../../mobileapp/src/assets/images/circle-check-fill-black.png');

export class CheckoutProgressIndicator extends React.PureComponent {
  routeToStage = stage => {
    const { setCheckoutStage } = this.props;
    setCheckoutStage(stage);
  };

  getStageLabel = stage => {
    const { checkoutProgressBarLabels } = this.props;
    return checkoutProgressBarLabels[`${stage}Label`];
  };

  render() {
    const { activeStage, availableStages } = this.props;
    let hasSeenActive = false;
    return (
      <CheckoutProgressBar>
        <StepIndicatorContainer>
          {availableStages.map((stage, index) => {
            if (availableStages[index].toLowerCase() === activeStage.toLowerCase()) {
              hasSeenActive = true;
              return (
                <>
                  {index === availableStages.length - 1 ? (
                    <View>
                      <ProgressDotActive />
                      <StyledDisableLabels
                        accessible
                        accessibilityRole="link"
                        accessibilityLabel={`Active stage ${this.getStageLabel(stage)}`}
                      >
                        {this.getStageLabel(stage)}
                      </StyledDisableLabels>
                    </View>
                  ) : (
                    <ProgressStep>
                      <ProgressDotActive />
                      <ProgressBar />
                      <StyledAnchor index={index}>
                        <Anchor
                          fontSizeVariation="large"
                          fontFamily="secondary"
                          anchorVariation="primary"
                          fontWeightVariation="extrabold"
                          onPress={() => this.routeToStage(stage)}
                          dataLocator=""
                          text={this.getStageLabel(stage)}
                          accessible
                          accessibilityRole="link"
                          accessibilityLabel={`Active stage ${this.getStageLabel(stage)}`}
                        />
                      </StyledAnchor>
                    </ProgressStep>
                  )}
                </>
              );
            }
            if (hasSeenActive) {
              return (
                <>
                  {index === availableStages.length - 1 ? (
                    <View>
                      <ProgressDot />
                      <StyledDisableLabels
                        accessible
                        accessibilityRole="link"
                        accessibilityLabel={`Pending stage ${this.getStageLabel(stage)}`}
                      >
                        {this.getStageLabel(stage)}
                      </StyledDisableLabels>
                    </View>
                  ) : (
                    <ProgressStep>
                      <ProgressDot />
                      {index !== availableStages.length - 1 && <ProgressBar />}
                      <StyledDisableLabels
                        accessible
                        accessibilityRole="link"
                        accessibilityLabel={`Pending stage ${this.getStageLabel(stage)}`}
                      >
                        {this.getStageLabel(stage)}
                      </StyledDisableLabels>
                    </ProgressStep>
                  )}
                </>
              );
            }
            return (
              <ProgressStep>
                <ProgressDotIcon
                  source={completedStage}
                  data-locator={getLocator('global_headerpanelbagicon')}
                />
                <ProgressBar />
                <StyledAnchorCompleted index={index}>
                  <Anchor
                    fontSizeVariation="large"
                    fontFamily="secondary"
                    anchorVariation="primary"
                    fontWeightVariation="extrabold"
                    onPress={() => this.routeToStage(stage)}
                    dataLocator=""
                    text={this.getStageLabel(stage)}
                    accessible
                    accessibilityRole="link"
                    accessibilityLabel={`Completed stage ${this.getStageLabel(stage)}`}
                  />
                </StyledAnchorCompleted>
              </ProgressStep>
            );
          })}
        </StepIndicatorContainer>
      </CheckoutProgressBar>
    );
  }
}
CheckoutProgressIndicator.propTypes = {
  activeStage: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}),
  availableStages: PropTypes.shape([]).isRequired,
  checkoutProgressBarLabels: PropTypes.shape([]).isRequired,
  setCheckoutStage: PropTypes.func.isRequired,
};
CheckoutProgressIndicator.defaultProps = {
  navigation: null,
};

export default CheckoutProgressIndicator;
