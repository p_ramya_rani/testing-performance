// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CheckoutFooterVanilla } from '../views/CheckoutFooter.view';

const afterpayerrorclass = '.after-pay-error';

describe('CheckoutFooterVanilla component', () => {
  const props = {
    isGuest: false,
    isMobile: false,
    isUsSite: false,
    onEditModeChange: false,
    isSmsUpdatesEnabled: false,
    currentPhoneNumber: 3453453453,
    shippingProps: {},
    isOrderUpdateChecked: false,
    isAlternateUpdateChecked: false,
    pickupInitialValues: {},
    pickUpLabels: {},
    smsSignUpLabels: {},
    router: {},
    initialValues: {},
    orderHasPickUp: false,
    navigation: {},
    submitShippingSection: () => {},
    showVenmoSubmit: true,
  };

  it('should renders correctly', () => {
    const component = shallow(<CheckoutFooterVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with footer', () => {
    const updatedProps = {
      ...props,
      footerBody: 'Footer',
    };
    const component = shallow(<CheckoutFooterVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with venmo', () => {
    const updatedProps = {
      ...props,
      showVenmoSubmit: true,
    };
    const component = shallow(<CheckoutFooterVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with venmo and error', () => {
    const updatedProps = {
      ...props,
      showVenmoSubmit: true,
      venmoError: 'Cannot process your request',
    };
    const component = shallow(<CheckoutFooterVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders Next Button correctly', () => {
    const updatedProps = {
      ...props,
      showVenmoSubmit: false,
      showPayPalButton: false,
      bagLoading: false,
    };
    const component = shallow(<CheckoutFooterVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should not render after-pay-error', () => {
    const updatedProps = {
      ...props,
      showVenmoSubmit: false,
      showPayPalButton: false,
      bagLoading: false,
      showCheckoutWithAfterPay: true,
      orderBalanceTotal: 100,
      afterPayMinOrderAmount: 10,
      afterPayMaxOrderAmount: 1000,
      labelAfterPayThresholdError: 'test',
    };
    const component = shallow(<CheckoutFooterVanilla {...updatedProps} />);
    expect(component.find(afterpayerrorclass)).toHaveLength(0);
  });
  it('should renders after-pay-error', () => {
    const updatedProps = {
      ...props,
      showVenmoSubmit: false,
      showPayPalButton: false,
      bagLoading: false,
      showCheckoutWithAfterPay: true,
      orderBalanceTotal: 2,
      afterPayMinOrderAmount: 10,
      afterPayMaxOrderAmount: 1000,
      labelAfterPayThresholdError: 'test',
    };
    const component = shallow(<CheckoutFooterVanilla {...updatedProps} />);
    expect(component.find(afterpayerrorclass)).toHaveLength(2);
  });
  it('should renders after-pay-error', () => {
    const updatedProps = {
      ...props,
      showVenmoSubmit: false,
      showPayPalButton: false,
      bagLoading: false,
      showCheckoutWithAfterPay: true,
      orderBalanceTotal: 2000,
      afterPayMinOrderAmount: 10,
      afterPayMaxOrderAmount: 1000,
      labelAfterPayThresholdError: 'test',
    };
    const component = shallow(<CheckoutFooterVanilla {...updatedProps} />);
    expect(component.find(afterpayerrorclass)).toHaveLength(2);
  });
});
