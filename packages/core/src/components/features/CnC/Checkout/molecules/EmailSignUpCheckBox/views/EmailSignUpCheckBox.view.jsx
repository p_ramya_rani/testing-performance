// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { FormSection, Field } from 'redux-form';
import PropTypes from 'prop-types';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';
import withStyles from '../../../../../../common/hoc/withStyles';
import Espot from '../../../../../../common/molecules/Espot';

import styles from '../styles/EmailSignUpCheckBox.view.style';

class EmailSignUpCheckBox extends React.PureComponent {
  render() {
    const {
      labels: {
        lbl_shipping_emailSignUpHeader: emailSignUpHeader,
        lbl_shipping_emailSignUpSubHeader: emailSignUpSubHeader,
        lbl_shipping_childrenPlaceCheckoutTxt: childrenPlaceCheckoutTxt,
        lbl_shipping_gymboreePlaceCheckoutTxt: gymboreePlaceCheckoutTxt,
      },
      className,
      fieldName,
      emailSignUpFlags: { emailSignUpTCP, emailSignUpGYM } = {},
      emailSignUpCaRichText,
    } = this.props;
    return (
      <FormSection name="emailSignUp">
        <div className={className}>
          <BodyCopy
            dataLocator="email-signUp-heading"
            fontSize="fs16"
            fontFamily="secondary"
            fontWeight="bold"
          >
            {emailSignUpHeader}
          </BodyCopy>
          <BodyCopy
            dataLocator="email-signUp-subHeading"
            fontSize="fs14"
            fontFamily="secondary"
            className="email-signUp-subHeading"
            fontWeight="regular"
          >
            {emailSignUpSubHeader}
          </BodyCopy>
          <div className="email-checkbox-container">
            <div className="email-checkbox-list">
              <Field
                dataLocator="signUp-checkbox-field"
                name={`${fieldName}TCP`}
                component={InputCheckbox}
                className="email-signup-tcp"
                defaultChecked={emailSignUpTCP}
              >
                <BodyCopy
                  dataLocator="pickup-email-signUp-heading-lbl"
                  fontSize="fs16"
                  fontFamily="secondary"
                  fontWeight="regular"
                >
                  {childrenPlaceCheckoutTxt}
                </BodyCopy>
              </Field>
              <Field
                dataLocator="signUp-checkbox-field"
                name={`${fieldName}GYM`}
                component={InputCheckbox}
                className="email-signup-gym"
                defaultChecked={emailSignUpGYM}
              >
                <BodyCopy
                  dataLocator="pickup-email-signUp-heading-lbl"
                  fontSize="fs16"
                  fontFamily="secondary"
                  fontWeight="regular"
                >
                  {gymboreePlaceCheckoutTxt}
                </BodyCopy>
              </Field>
            </div>
            {emailSignUpCaRichText && (
              <div className="email-signup-text">
                <Espot richTextHtml={emailSignUpCaRichText} />
              </div>
            )}
          </div>
        </div>
      </FormSection>
    );
  }
}

EmailSignUpCheckBox.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  emailSignUpFlags: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  fieldName: PropTypes.string.isRequired,
  emailSignUpCaRichText: PropTypes.string,
};

EmailSignUpCheckBox.defaultProps = {
  emailSignUpCaRichText: null,
};

export default withStyles(EmailSignUpCheckBox, styles);
export { EmailSignUpCheckBox as EmailSignUpCheckBoxVanilla };
