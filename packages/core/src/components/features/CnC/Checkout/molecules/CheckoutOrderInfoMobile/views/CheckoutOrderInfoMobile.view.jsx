// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../../../common/hoc/withStyles';
import OrderLedgerContainer from '../../../../common/organism/OrderLedger';
import AirmilesBanner from '../../../../common/organism/AirmilesBanner';
import CouponAndPromos from '../../../../common/organism/CouponAndPromos';
import BonusPointsDays from '../../../../../../common/organisms/BonusPointsDays';
import { Row, Col } from '../../../../../../common/atoms';
import style from '../styles/CheckoutOrderInfoMobile.style';
import PersonalizedCoupons from '../../../../Confirmation/organisms/PersonalizedCoupons';

/** The hard coded values are just to show the template of confirmation page. these will be removed once the components are are in place */
class CheckoutOrderInfo extends React.PureComponent {
  render() {
    const {
      className,
      isGuest,
      showAccordian,
      isConfirmationPage,
      fullPageInfo,
      pageCategory,
      isCheckoutFlow,
    } = this.props;
    return (
      <div className={className}>
        {isConfirmationPage ? (
          <>
            <OrderLedgerContainer
              isConfirmationPage={isConfirmationPage}
              pageCategory={pageCategory}
            />
            <Row fullBleed>
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <PersonalizedCoupons />
              </Col>
            </Row>
          </>
        ) : (
          <>
            <CouponAndPromos
              showAccordian={showAccordian}
              additionalClassNameModal="coupon-modal-mob"
              idPrefix="mobile"
              fullPageInfo={fullPageInfo}
              isCheckoutFlow={isCheckoutFlow}
            />
            <OrderLedgerContainer showAccordian={showAccordian} pageCategory={pageCategory} />
            {!isGuest && (
              <BonusPointsDays
                showAccordian={showAccordian}
                enableApplyCta
                additionalClassNameModal="bonus-modal-mob"
              />
            )}
            <AirmilesBanner fromPage="checkoutOrderInfo" />
          </>
        )}
      </div>
    );
  }
}

CheckoutOrderInfo.propTypes = {
  className: PropTypes.string.isRequired,
  isGuest: PropTypes.bool.isRequired,
  showAccordian: PropTypes.bool.isRequired,
  isConfirmationPage: PropTypes.bool,
  fullPageInfo: PropTypes.bool,
  isCheckoutFlow: PropTypes.bool,
  pageCategory: PropTypes.string,
  labels: PropTypes.shape({
    paidWithVenmo: PropTypes.string,
  }),
};

CheckoutOrderInfo.defaultProps = {
  isConfirmationPage: false,
  fullPageInfo: false,
  pageCategory: '',
  isCheckoutFlow: false,
  labels: PropTypes.shape({
    paidWithVenmo: '',
  }),
};

export default withStyles(CheckoutOrderInfo, style);
export { CheckoutOrderInfo as CheckoutOrderInfoVanilla };
