// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import AfterPayContainer from '@tcp/core/src/components/common/organisms/AfterPayPayment';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import { capitalize } from '@tcp/core/src/utils';
import style from '../styles/CheckoutFooter.style';
import { Button, BodyCopy } from '../../../../../../common/atoms';
import VenmoPaymentButton from '../../../../../../common/atoms/VenmoPaymentButton';
import ErrorMessage from '../../../../common/molecules/ErrorMessage';
import Espot from '../../../../../../common/molecules/Espot';

class CheckoutFooter extends React.PureComponent {
  renderSkeleton = (showVenmoSubmit, showPayPalButton, showApplePayButton, showAfterPayButton) => {
    return (
      <>
        {!showVenmoSubmit && !showPayPalButton && !showApplePayButton && !showAfterPayButton && (
          <div className="footer-button footer-button-web">
            <LoaderSkelton />
          </div>
        )}
      </>
    );
  };

  disableAfterPay = () => {
    const {
      showCheckoutWithAfterPay,
      orderBalanceTotal,
      afterPayMinOrderAmount,
      afterPayMaxOrderAmount,
    } = this.props;
    const totalAmount = orderBalanceTotal && orderBalanceTotal * 1;

    if (
      showCheckoutWithAfterPay &&
      (afterPayMinOrderAmount > totalAmount || afterPayMaxOrderAmount < totalAmount)
    ) {
      return true;
    }

    return showCheckoutWithAfterPay && totalAmount && totalAmount < 1;
  };

  renderCheckoutWithAfterPay = () => {
    const { showCheckoutWithAfterPay, disableNext } = this.props;
    return (
      <>
        {showCheckoutWithAfterPay && !this.disableAfterPay() && !disableNext && (
          <AfterPayContainer className="after-pay-container" />
        )}
      </>
    );
  };

  shouldDisableCheckoutCta = () => {
    const { disableNext } = this.props;
    return disableNext || this.disableAfterPay();
  };

  renderAfterPayMessaging = (isMobileWeb) => {
    const {
      showCheckoutWithAfterPay,
      orderBalanceTotal,
      labelAfterPayThresholdError,
      afterPayMinOrderAmount,
      afterPayMaxOrderAmount,
    } = this.props;

    let thresholdErrorMessage = labelAfterPayThresholdError.replace(
      '[AfterPayMinOrderAmount]',
      afterPayMinOrderAmount
    );
    thresholdErrorMessage = thresholdErrorMessage.replace(
      '[AfterPayMaxOrderAmount]',
      afterPayMaxOrderAmount
    );

    if (showCheckoutWithAfterPay && this.disableAfterPay()) {
      return (
        <Notification
          className={`after-pay-error elem-mt-MED ${
            isMobileWeb ? 'afterpay-mobile-web' : 'afterpay-button-web'
          }`}
          status="warning"
          scrollIntoView
          message={thresholdErrorMessage}
        />
      );
    }

    return showCheckoutWithAfterPay && orderBalanceTotal ? (
      <BodyCopy
        component="p"
        fontSize="fs10"
        fontFamily="secondary"
        textAlign="center"
        className={`elem-mt-XS ${isMobileWeb ? 'afterpay-mobile-web' : 'afterpay-button-web'}`}
      >
        <AfterPayMessaging offerPrice={orderBalanceTotal} centered dataIntroText="false" />
      </BodyCopy>
    ) : null;
  };

  showButton = () => {
    const {
      showVenmoSubmit,
      showPayPalButton,
      showApplePayButton,
      showAfterPayButton,
      bagLoading,
    } = this.props;
    return (
      !showVenmoSubmit &&
      !showPayPalButton &&
      !showApplePayButton &&
      !showAfterPayButton &&
      !bagLoading
    );
  };

  triggerAnalytics = () => {
    const { trackClickAfterPay, pageCategory } = this.props;
    if (typeof trackClickAfterPay === 'function') {
      trackClickAfterPay({
        afterpay_metrics141: 1,
        afterpay_cd131: capitalize(pageCategory),
        customEvents: ['event141'],
      });
    }
  };

  renderNextButton = () => {
    const {
      showVenmoSubmit,
      showPayPalButton,
      showAfterPayButton,
      showApplePayButton,
      ariaLabelNextButton,
      nextHandler,
      nextButtonText,
      showCheckoutWithAfterPay,
    } = this.props;
    return this.showButton() ? (
      <div className="footer-button-mob-container">
        <Button
          disabled={this.shouldDisableCheckoutCta()}
          aria-label={ariaLabelNextButton}
          type={showCheckoutWithAfterPay ? 'button' : 'submit'}
          className="footer-button footer-button-mob"
          fontSize="fs14"
          fontWeight="extrabold"
          buttonVariation="variable-width"
          fill="BLUE"
          onClick={!showCheckoutWithAfterPay ? nextHandler : this.triggerAnalytics}
          dataLocator="reviewBtn"
        >
          {nextButtonText}
          {this.renderCheckoutWithAfterPay()}
        </Button>
        {this.renderAfterPayMessaging(true)}
      </div>
    ) : (
      <>
        {!showVenmoSubmit && !showPayPalButton && !showApplePayButton && !showAfterPayButton && (
          <div className="footer-button footer-button-mob">
            <LoaderSkelton />
          </div>
        )}
      </>
    );
  };

  renderNextButtonDesktop = () => {
    const {
      showVenmoSubmit,
      showPayPalButton,
      showApplePayButton,
      showAfterPayButton,
      ariaLabelNextButton,
      nextHandler,
      nextButtonText,
      disableDesktopOnlyNext,
      showCheckoutWithAfterPay,
      submitForm,
    } = this.props;
    return (
      <>
        {this.showButton() ? (
          <div>
            <Button
              disabled={disableDesktopOnlyNext || this.shouldDisableCheckoutCta()}
              aria-label={ariaLabelNextButton}
              type={showCheckoutWithAfterPay ? 'button' : 'submit'}
              className="footer-button footer-button-web"
              fontSize="fs14"
              fontWeight="extrabold"
              buttonVariation="variable-width"
              fill="BLUE"
              onClick={!showCheckoutWithAfterPay ? nextHandler : this.triggerAnalytics}
              dataLocator="reviewBtn"
              form={submitForm}
            >
              {nextButtonText}
              {this.renderCheckoutWithAfterPay()}
            </Button>
            {this.renderAfterPayMessaging()}
          </div>
        ) : (
          this.renderSkeleton(
            showVenmoSubmit,
            showPayPalButton,
            showApplePayButton,
            showAfterPayButton
          )
        )}
      </>
    );
  };

  render() {
    const {
      className,
      backLinkText,
      backLinkHandler,
      disableBackLink,
      hideBackLink,
      ariaLabelBackLink,
      showVenmoSubmit, // Show Venmo Submit on billing page on selecting venmo payment method
      continueWithText,
      onVenmoSubmit,
      venmoError,
      submitOrderRichText,
    } = this.props;

    return (
      <div className={className}>
        {submitOrderRichText && <Espot richTextHtml={submitOrderRichText} />}
        <div className="footer-buttons">
          {showVenmoSubmit ? (
            <>
              <VenmoPaymentButton
                className="footer-venmo-button"
                continueWithText={continueWithText}
                onSuccess={onVenmoSubmit}
                isVenmoBlueButton
              />
              {venmoError && <ErrorMessage error={venmoError} className="checkout-page-error" />}
            </>
          ) : (
            this.renderNextButton()
          )}
          <div className="back-space">
            {hideBackLink && (
              <Button
                aria-label={ariaLabelBackLink}
                disabled={disableBackLink}
                type="button"
                className="back-link"
                onClick={backLinkHandler}
                dataLocator="returnToLink"
              >
                <span className="left-arrow"> </span>
                {backLinkText}
              </Button>
            )}
          </div>
          {this.renderNextButtonDesktop()}
        </div>
      </div>
    );
  }
}

CheckoutFooter.propTypes = {
  className: PropTypes.string.isRequired,
  backLinkText: PropTypes.string.isRequired,
  nextButtonText: PropTypes.string.isRequired,
  ariaLabelBackLink: PropTypes.string.isRequired,
  ariaLabelNextButton: PropTypes.string.isRequired,
  disableNext: PropTypes.bool.isRequired,
  backLinkHandler: PropTypes.func.isRequired,
  nextHandler: PropTypes.oneOf([PropTypes.func, undefined]).isRequired,
  disableBackLink: PropTypes.bool.isRequired,
  hideBackLink: PropTypes.bool,
  footerBody: PropTypes.shape({}).isRequired,
  showVenmoSubmit: PropTypes.bool,
  showPayPalButton: PropTypes.bool,
  showApplePayButton: PropTypes.bool,
  disableDesktopOnlyNext: PropTypes.bool,
  continueWithText: PropTypes.string,
  onVenmoSubmit: PropTypes.func, // Venmo Submit for billing page, redirect to review page once already authorized or new authorization with the venmo app.
  venmoError: PropTypes.string,
  bagLoading: PropTypes.bool,
  submitOrderRichText: PropTypes.string,
  showCheckoutWithAfterPay: PropTypes.bool,
  orderBalanceTotal: PropTypes.number,
  showAfterPayButton: PropTypes.bool,
  trackClickAfterPay: PropTypes.func,
  pageCategory: PropTypes.string,
  labelAfterPayThresholdError: PropTypes.string,
  afterPayMinOrderAmount: PropTypes.number,
  afterPayMaxOrderAmount: PropTypes.number,
};

CheckoutFooter.defaultProps = {
  hideBackLink: false,
  showVenmoSubmit: false,
  showPayPalButton: false,
  showApplePayButton: false,
  disableDesktopOnlyNext: false,
  continueWithText: '',
  onVenmoSubmit: () => {},
  venmoError: '',
  bagLoading: false,
  submitOrderRichText: null,
  showCheckoutWithAfterPay: false,
  orderBalanceTotal: 0,
  showAfterPayButton: false,
  trackClickAfterPay: () => {},
  pageCategory: '',
  labelAfterPayThresholdError: '',
  afterPayMinOrderAmount: 30,
  afterPayMaxOrderAmount: 1000,
};

export default withStyles(CheckoutFooter, style);

export { CheckoutFooter as CheckoutFooterVanilla };
