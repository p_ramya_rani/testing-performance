// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import withStyles from '../../../../../../common/hoc/withStyles';
import { Style, GovernmentId } from '../styles/ContactFormFields.style.native';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import TextBox from '../../../../../../common/atoms/TextBox';
import { formatPhoneNumber } from '../../../../../../../utils/formValidation/phoneNumber';

class ContactFormFields extends React.Component {
  static ContactValidationConfig = getStandardConfig([
    'firstName',
    'lastName',
    'emailAddress',
    'phoneNumber',
    'firstName_alt',
    'lastName_alt',
    'emailAddress_alt',
  ]);

  render() {
    const { showEmailAddress, showPhoneNumber, labels, isAlternate, createRefParent } = this.props;

    return (
      <View>
        <View className="fieldFirstName">
          <Field
            label={labels.firstName}
            name={isAlternate ? 'firstName_alt' : 'firstName'}
            id={isAlternate ? 'firstName_alt' : 'firstName'}
            type="text"
            component={TextBox}
            dataLocator="pickup-first-name"
            inputRef={node => createRefParent(node, isAlternate ? 'firstName_alt' : 'firstName')}
          />
          <GovernmentId>
            <BodyCopy fontFamily="secondary" fontSize="fs12" text={labels.govIdText} />
          </GovernmentId>
        </View>
        <View className="fieldLastName">
          <Field
            label={labels.lastName}
            name={isAlternate ? 'lastName_alt' : 'lastName'}
            id={isAlternate ? 'lastName_alt' : 'lastName'}
            component={TextBox}
            dataLocator="pickup-last-name"
            enableSuccessCheck={false}
            inputRef={node => createRefParent(node, isAlternate ? 'lastName_alt' : 'lastName')}
          />
        </View>

        {showEmailAddress && (
          <View className="fieldEmail">
            <Field
              id={isAlternate ? 'emailAddress_alt' : 'emailAddress'}
              label={labels.email}
              name={isAlternate ? 'emailAddress_alt' : 'emailAddress'}
              component={TextBox}
              dataLocator="pickup-email"
              errorDataLocator="pickup-email-error"
              showSuccessCheck={false}
              enableSuccessCheck={false}
              inputRef={node =>
                createRefParent(node, isAlternate ? 'emailAddress_alt' : 'emailAddress')
              }
            />
          </View>
        )}
        {showPhoneNumber && (
          <View className="fieldNumber">
            <Field
              label={labels.mobile}
              name="phoneNumber"
              id="phoneNumber"
              keyboardType="phone-pad"
              component={TextBox}
              maxLength={50}
              dataLocator="pickup-phone-number"
              enableSuccessCheck={false}
              inputRef={node => createRefParent(node, 'phoneNumber')}
              normalize={formatPhoneNumber}
            />
          </View>
        )}
      </View>
    );
  }
}

ContactFormFields.propTypes = {
  showEmailAddress: PropTypes.bool,
  showPhoneNumber: PropTypes.bool,
  labels: PropTypes.shape({}).isRequired,
  createRefParent: PropTypes.func,
  isAlternate: PropTypes.bool,
};

ContactFormFields.defaultProps = {
  showEmailAddress: false,
  showPhoneNumber: false,
  isAlternate: false,
  createRefParent: () => {},
};

export default withStyles(ContactFormFields, Style);
export { ContactFormFields as ContactFormFieldsVanilla };
