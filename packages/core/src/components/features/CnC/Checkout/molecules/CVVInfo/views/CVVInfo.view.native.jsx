// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from '../../../../../../common/atoms/ReactToolTip';
import { Image, RichText } from '../../../../../../common/atoms';

const infoIcon = require('../../../../../../../../../mobileapp/src/assets/images/info-icon.png');

/**
 * @function getCvvInfo
 * @description renders the tooltip component
 */

const getCvvInfo = ({ cvvCodeRichText, infoIconText }) => {
  const tooltip = () => <RichText isNativeView source={cvvCodeRichText} />;
  return (
    <ReactTooltip withOverlay={false} popover={tooltip()} height={290} width={350}>
      <Image source={infoIcon} alt={infoIconText} height={15} width={15} marginTop={17} />
    </ReactTooltip>
  );
};

getCvvInfo.propTypes = {
  cvvCodeRichText: PropTypes.string,
  infoIconText: PropTypes.string,
};

getCvvInfo.defaultProps = {
  cvvCodeRichText: PropTypes.string,
  infoIconText: '',
};

export default getCvvInfo;
