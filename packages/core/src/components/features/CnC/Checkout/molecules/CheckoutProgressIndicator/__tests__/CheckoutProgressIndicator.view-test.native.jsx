// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CheckoutProgressIndicator } from '../views/CheckoutProgressIndicator.view.native';
import Anchor from '../../../../../../common/atoms/Anchor';

describe('CheckoutProgressIndicator component', () => {
  it('should renders correctly props not present', () => {
    const props = {
      activeStage: '',
      navigation: {
        navigate: jest.fn(),
      },
      availableStages: ['pickup', 'shipping'],
      checkoutProgressBarLabels: { pickupLabel: 'Pickup', shippingLabel: 'Shipping' },
    };
    const component = shallow(<CheckoutProgressIndicator {...props} />);
    expect(component.find(Anchor)).not.toHaveLength(5);
    expect(component).toMatchSnapshot();
  });
});
