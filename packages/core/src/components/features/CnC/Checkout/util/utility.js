// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable extra-rules/no-commented-out-code */
import queryString from 'query-string';
import isEmpty from 'lodash/isEmpty';
import { routerPush, isMobileApp, parseBoolean } from '@tcp/core/src/utils';
import {
  getSetCurrentOrderIdActn,
  getSetCartActn,
  getSetEstimatedAirMilesActn,
  getSetGiftWrappingTotalActn,
  getSetGiftCardTotalActn,
  setShippingTotal,
  getSetCartStoreActn,
  setTaxesTotal,
  getSetCouponsTotalActn,
  setSavingsTotal,
  getSetItemsTotalAction,
  setItemsCount,
  getSetSubTotal,
  getSetSubTotalWithDiscountsActn,
  getSetGrandTotal,
  getSetRewardsToBeEarnedActn,
  getSetIsPayPalEnabledActn,
  setCartTotalAfterPLCCDiscount,
  getSetPointsAndRewardsActn,
  getSetGiftCardValuesActn,
  getSetAirmilesPromoIdActn,
  getSetAirmilesAccountActn,
} from '../container/Checkout.action';
import CardConstants from '../../../account/AddEditCreditCard/container/AddEditCreditCard.constants';
import CreditCardConstants from '../organisms/BillingPaymentForm/container/CreditCard.constants';
import { getLocalStorage } from '../../../../../utils/localStorageManagement';
import CheckoutConstants from '../Checkout.constants';

const { CREDIT_CARDS_BIN_RANGES, ACCEPTED_CREDIT_CARDS } = CardConstants;

/**
 * getOrderPointsRecalcFlag
 * @param {boolean} recalcRewards - current recalculate rewards value for the request
 * @param {number} recalcOrderPointsInterval - XAPPConfig configuration value for timeout for recalc flag
 * the entire function will be dependent on this flag being set from backend
 * @description this method takes recalculate flag and the XappConfigValue configuration
 * in case recalcRewards is false and caching interval is configured, it changes it to true in these cases:
 * if time of last recalcRewards true request is not cached in localStorage
 * if the time elapsed since last recalcRewards true request is more than the set threshold
 * after recalcRewards flag is modified, if it is true, cache the time when the true request is sent
 * @returns {boolean} recalcVal to be passed in the getOrderDetails or cart API header
 */
const getOrderPointsRecalcFlag = (recalcRewards, recalcOrderPointsInterval, keyValue) => {
  let recalcVal = parseBoolean(recalcRewards);
  if (recalcOrderPointsInterval && !recalcRewards) {
    const orderPointsTimeStamp = getLocalStorage(keyValue) || null;
    const parsedTime = parseInt(orderPointsTimeStamp, 10);
    const currentTime = new Date().getTime();
    if (!parsedTime || currentTime - parsedTime > recalcOrderPointsInterval) {
      recalcVal = true;
    }
  }
  return recalcVal;
};

const updateCartInfo = (cartInfo, isUpdateCartItems) => {
  const getRewardPoints = {
    estimatedRewards: cartInfo.estimatedRewards,
    earnedReward: cartInfo.earnedReward,
    pointsToNextReward: cartInfo.pointsToNextReward,
  };
  const actions = [
    getSetCurrentOrderIdActn(cartInfo.orderId),
    getSetEstimatedAirMilesActn(cartInfo.estimatedAirMiles),
    setShippingTotal(cartInfo.shippingTotal),
    getSetGiftWrappingTotalActn(cartInfo.giftWrappingTotal),
    getSetGiftCardTotalActn(cartInfo.giftCardsTotal),
    setTaxesTotal(cartInfo.totalTax),
    setSavingsTotal(cartInfo.savingsTotal),
    getSetCouponsTotalActn(cartInfo.couponsTotal),
    getSetItemsTotalAction(cartInfo.orderTotalAfterDiscount),
    cartInfo.totalItems !== null && setItemsCount(cartInfo.totalItems),
    getSetSubTotal(cartInfo.subTotal),
    getSetSubTotalWithDiscountsActn(cartInfo.subTotalWithDiscounts),
    getSetGrandTotal(cartInfo.grandTotal),
    getSetGiftCardValuesActn(cartInfo.appliedGiftCards),
    getSetRewardsToBeEarnedActn(cartInfo.rewardsToBeEarned),
    getSetPointsAndRewardsActn(getRewardPoints),
    setCartTotalAfterPLCCDiscount(cartInfo.cartTotalAfterPLCCDiscount),
  ];
  /* istanbul ignore else */
  if (isUpdateCartItems) {
    actions.push(getSetCartActn(cartInfo.orderItems));
    actions.push(getSetCartStoreActn(cartInfo.stores));
  }
  /* istanbul ignore else */
  if (cartInfo.uiFlags) {
    actions.push(getSetIsPayPalEnabledActn(cartInfo.uiFlags.isPaypalEnabled));
  }
  /* istanbul ignore else */
  if (cartInfo.airmiles) {
    actions.push(getSetAirmilesPromoIdActn(cartInfo.airmiles.promoId));
    actions.push(getSetAirmilesAccountActn(cartInfo.airmiles.accountNumber));
  }

  /* Sometimes this function is used in another function to do bulk dispaches.
   * In this case we can just export all these actions so we can dispatch in one bulk.
   */
  return actions;
};

const hasPOBox = (addressLine1 = '', addressLine2 = '') => {
  // some delimiter that will not allow them to match only if concatenated
  const value = `${addressLine1}#${addressLine2}`;
  // REVIEW: got the regex from: https://gist.github.com/gregferrell/7494667
  // seems to cover most use cases; not in the mood to write it from scratch
  return (
    value.search(
      /\bbox(?:\b$|([\s|-]+)?[0-9]+)|(p[-.\s]*o[-.\s]*|(post office|post)\s)b(\.|ox|in)?\b|(^p[.]?(o|b)[.]?$)/gim
    ) >= 0
  );
};

const isOrderHasShipping = cartItems => {
  return cartItems && cartItems.filter(item => !item.getIn(['miscInfo', 'store'])).size;
};

const isOrderHasPickup = cartItems => {
  return cartItems && cartItems.filter(item => !!item.getIn(['miscInfo', 'store'])).size;
};

const getAvailableStages = cartItems => {
  const { PICKUP, SHIPPING, BILLING, REVIEW } = CheckoutConstants.CHECKOUT_STAGES;
  const stages = [BILLING, REVIEW];
  if (isOrderHasShipping(cartItems)) {
    stages.unshift(SHIPPING);
  }
  /* istanbul ignore else */
  if (isOrderHasPickup(cartItems)) {
    stages.unshift(PICKUP);
  }
  return stages;
};

const routeToPage = (dataObj, queryParams, ...others) => {
  const { asPath } = dataObj;
  let { to } = dataObj;
  if (queryParams) {
    if (to.indexOf('?') !== -1) {
      to += '&';
    }
    to += `${queryString.stringify(queryParams)}`;
  }
  if (!isMobileApp()) {
    routerPush(to, asPath, ...others);
  }
};

function getCreditCardType({ cardNumber = '', cardType } = {}) {
  if (cardNumber.length === 0) {
    return null;
  }
  const keys = Object.keys(CREDIT_CARDS_BIN_RANGES);
  for (let i = 0; i < keys.length; i += 1) {
    const type = keys[i];
    const cartRangeType = CREDIT_CARDS_BIN_RANGES[type];
    let currentRange = 0;
    const rangesCount = cartRangeType.length;
    for (; currentRange < rangesCount; currentRange += 1) {
      const { from, to } = cartRangeType[currentRange];
      const prefixLength = from.toString().length;
      const prefix = cardNumber.substr(0, prefixLength);

      if (prefix >= from && prefix <= to) {
        return ACCEPTED_CREDIT_CARDS[type];
      }
    }
  }
  if (cardType && cardNumber.substr(0, 1) === '*') {
    return cardType.toUpperCase();
  }
  return null;
}

export const getSelectedCard = ({ creditCardList, onFileCardKey }) => {
  return creditCardList.find(card => card.creditCardId === +onFileCardKey);
};

export const getCreditCardList = ({ cardList }) =>
  cardList &&
  cardList.size > 0 &&
  cardList.filter(
    card =>
      card.ccType !== CreditCardConstants.ACCEPTED_CREDIT_CARDS.GIFT_CARD &&
      card.ccType !== CreditCardConstants.ACCEPTED_CREDIT_CARDS.VENMO
  );

export const getExpirationRequiredFlag = ({ cardType }) => {
  return !cardType || cardType !== CreditCardConstants.ACCEPTED_CREDIT_CARDS.PLACE_CARD;
};

export const getPayPalFlag = navigation => {
  if (navigation && navigation.state) {
    const {
      state: { params },
    } = navigation;
    if (params) {
      const { isPayPalFlow } = params;
      return isPayPalFlow;
    }
  }
  return false;
};

const handleReviewFormSubmit = (scope, data) => {
  const {
    submitReview,
    pickUpContactPerson,
    pickUpContactAlternate,
    isExpressCheckout,
  } = scope.props;
  const {
    firstName_alt: firstName,
    lastName_alt: lastName,
    hasAlternatePickup,
    emailAddress_alt: emailAddress,
  } = data.pickUpAlternateExpress;
  const { cvvCode } = data;
  const pickupContactData =
    typeof pickUpContactPerson.firstName !== 'undefined'
      ? pickUpContactPerson
      : pickUpContactAlternate.pickUpContact;

  if (isExpressCheckout) {
    const formDataSubmission = {
      formData: {
        hasAlternatePickup,
        pickUpAlternate: {
          emailAddress,
          firstName,
          lastName,
        },
        pickUpContact: {
          firstName: pickupContactData.firstName,
          lastName: pickupContactData.lastName,
          phoneNumber: pickupContactData.phoneNumber,
          emailAddress: pickupContactData.emailAddress,
        },
        billing: {
          cvv: cvvCode,
        },
      },
    };
    submitReview(formDataSubmission);
  } else {
    submitReview({});
  }
};

const flattenObject = (obj, prefix = '') =>
  Object.keys(obj).reduce((acc, k) => {
    const pre = prefix.length ? `${prefix}.` : '';
    if (typeof obj[k] === 'object') Object.assign(acc, flattenObject(obj[k], pre + k));
    else acc[pre + k] = obj[k];
    return acc;
  }, {});

export const scrollToFirstError = errors => {
  const errorEl = document.querySelector(
    Object.keys(flattenObject(errors))
      .map(fieldName => `[name="${fieldName}"]`)
      .join(',')
  );
  if (errorEl && errorEl.focus) {
    errorEl.focus(); // this scrolls without visible scroll
  }
};

const scrollToFocussedField = (element, scrollViewRef) => {
  // eslint-disable-next-line no-underscore-dangle
  if (element && scrollViewRef && scrollViewRef._scrollToFocusedInputWithNodeHandle) {
    // eslint-disable-next-line no-underscore-dangle
    scrollViewRef._scrollToFocusedInputWithNodeHandle(element);
  }
};

const focusField = (refObj, focusErrorField, scrollViewRef) => {
  if (refObj && refObj[focusErrorField]) {
    const element = refObj[focusErrorField];
    if (element.focus) {
      element.focus();
      scrollToFocussedField(element, scrollViewRef);
    } else if (element.refs && element.refs.textInput && element.refs.textInput.focus) {
      const actualElem = element.refs.textInput;
      actualElem.focus();
      scrollToFocussedField(actualElem, scrollViewRef);
    }
  }
};

export const scrollToFirstErrorApp = (errors, refObj, scrollViewRef) => {
  if (isMobileApp()) {
    const errorFieldName = Object.keys(errors)[0];
    let focusErrorField = errorFieldName;
    const errorKeys = Object.keys(errors);
    if (typeof errors[focusErrorField] === 'object') {
      for (let i = 0; i < errorKeys.length; i += 1) {
        if (typeof errors[errorKeys[i]] === 'object' && !isEmpty(errors[errorKeys[i]])) {
          [focusErrorField] = Object.keys(errors[errorKeys[i]]);
          break;
        }
      }
    }
    focusField(refObj, focusErrorField, scrollViewRef);
  }
};

export const hasGiftServiceError = checkoutServerError => {
  const { billingError } = checkoutServerError;
  if (billingError) {
    const { errorParameters } = billingError;
    let giftServiceError = false;
    errorParameters.forEach(key => {
      if (key && key.includes('_ERR_GIFT_SERVICE')) {
        giftServiceError = true;
      }
    });
    return giftServiceError;
  }
  return null;
};

export const hasApoFpoError = checkoutServerError => {
  const { billingError } = checkoutServerError;
  if (billingError) {
    const { errorParameters } = billingError;
    let apoFpoError = false;
    errorParameters.forEach(key => {
      if (key && key.includes('_ERR_APO_FPO_ADDRESS')) {
        apoFpoError = true;
      }
    });
    return apoFpoError;
  }
  return null;
};

export const hasRushShippingError = checkoutServerError => {
  const { billingError } = checkoutServerError;
  if (billingError) {
    const { errorParameters } = billingError;
    let rushShippingError = false;
    errorParameters.forEach(key => {
      if (key && key.includes('_ERR_EXPRESS_RUSH_SHIPPING')) {
        rushShippingError = true;
      }
    });
    return rushShippingError;
  }
  return null;
};

export const getSaveForLaterItemsCatEntryId = checkoutServerError => {
  const { billingError } = checkoutServerError;
  if (billingError) {
    const { errorParameters } = billingError;
    let ids;
    errorParameters.forEach(key => {
      if (key && key.includes('CATENTRY_ID')) {
        ids = key.split('=')[1].split('|');
      }
    });
    return ids;
  }
  return null;
};

export const getSaveForLaterItemsOrderItemId = checkoutServerError => {
  const { billingError } = checkoutServerError;
  if (billingError) {
    const { errorParameters } = billingError;
    let ids;
    errorParameters.forEach(key => {
      if (key && key.includes('ORDER_ITEM_ID')) {
        ids = key.split('=')[1].split('|');
      }
    });
    return ids;
  }
  return null;
};

export const getSfsErroMessage = checkoutServerError => {
  const rushShipppingError = hasRushShippingError(checkoutServerError);
  const giftServiceError = hasGiftServiceError(checkoutServerError);
  const addressError = hasApoFpoError(checkoutServerError);
  let errorMessage = '';
  if (rushShipppingError) {
    errorMessage = 'lbl_sfs_error_rush_shipping';
  }
  if (giftServiceError) {
    errorMessage = 'lbl_sfs_error_gift_service';
  }
  if (addressError) {
    errorMessage = 'lbl_sfs_error_shipping_address';
  }
  if (addressError && rushShipppingError) {
    errorMessage = 'lbl_sfs_error_address_rush';
  }
  if (giftServiceError && addressError) {
    errorMessage = 'lbl_sfs_error_gift_address';
  }
  if (giftServiceError && rushShipppingError) {
    errorMessage = 'lbl_sfs_error_gift_rush';
  }

  return errorMessage;
};

export default {
  getOrderPointsRecalcFlag,
  updateCartInfo,
  hasPOBox,
  isOrderHasPickup,
  getAvailableStages,
  routeToPage,
  getCreditCardType,
  isOrderHasShipping,
  handleReviewFormSubmit,
};
