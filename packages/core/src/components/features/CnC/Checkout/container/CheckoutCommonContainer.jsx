/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import logger from '@tcp/core/src/utils/loggerInstance';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import {
  setSessionStorage,
  getDateRange,
  isMobileApp,
  fireEnhancedEcomm,
  addRecommendationsObj,
  getSessionStorage,
  isTCP,
  getValueFromAsyncStorage,
  getRecommendationsObj,
  clearRecommendationsObj,
} from '@tcp/core/src/utils';
import sbpConstant from '@tcp/core/src/components/features/browse/ShopByProfile/container/ShopByProfile.constants';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import { readCookie, removeCookie } from '@tcp/core/src/utils/cookie.util';
import { initActions } from './Checkout.action';
import CheckoutPage from '../views/CheckoutPage.view';
import checkoutUtil, { getPayPalFlag } from '../util/utility';
import constants from '../Checkout.constants';
import utils from '../../../../../utils';
import {
  intiSectionPage,
  formatPayload,
  mapDispatchToProps,
  mapStateToProps,
  callNeedHelpContent,
} from './CheckoutCommonContainer.util';

export class CheckoutContainer extends React.PureComponent {
  initialLoad = true;

  constructor(props) {
    super(props);
    this.analyticsTriggered = false;
    this.googleAnalyticsTriggered = false;
    this.firebaseAnalyticsTriggered = false;
    this.isMobileApp = isMobileApp();
    this.shouldTriggerExpress = false;
  }

  componentDidMount() {
    const {
      router,
      initCheckout,
      markBagPageRoutingDone,
      resetAnalyticsData,
      setMaxItemErrorAction,
      setCouponMergeErrorAction,
    } = this.props;
    resetAnalyticsData();
    markBagPageRoutingDone();
    const {
      isRegisteredUserCallDone,
      checkoutServerError,
      clearCheckoutServerError,
      navigation,
      isApplePaymentInProgress,
    } = this.props;
    /* istanbul ignore else */
    if (isRegisteredUserCallDone && !isApplePaymentInProgress) {
      initCheckout(router, getPayPalFlag(navigation), navigation);
    }
    callNeedHelpContent(this.props);
    if (checkoutServerError && checkoutServerError.component !== 'Apple Pay') {
      clearCheckoutServerError({});
    }
    setMaxItemErrorAction(false);
    setCouponMergeErrorAction(false);
  }

  componentDidUpdate(prevProps) {
    const {
      isRegisteredUserCallDone: prevIsRegisteredUserCallDone,
      cartOrderItems: prevCartOrderItems,
      currentStage: prevStage,
    } = prevProps;
    const {
      isRegisteredUserCallDone,
      router,
      initCheckout,
      navigation,
      isRTPSFlow,
      cartOrderItems,
      currentStage,
      shippingProps: { isGiftServicesChecked: { name = '' } = {} } = {},
      setConfirmationDoneActn,
      orderConfirmationStatus,
    } = this.props;

    /* istanbul ignore else */
    if (currentStage === 'review' && prevStage === 'confirmation' && orderConfirmationStatus) {
      this.shouldTriggerExpress = true;
    }

    if (
      (prevIsRegisteredUserCallDone !== isRegisteredUserCallDone ||
        (currentStage === 'review' && prevStage === 'confirmation' && orderConfirmationStatus)) &&
      isRegisteredUserCallDone &&
      !isRTPSFlow
    ) {
      initCheckout(router, getPayPalFlag(navigation), navigation);
      this.shouldTriggerExpress = false;
      setConfirmationDoneActn(false);
    }
    this.startCheckoutAnalytics(currentStage, cartOrderItems, prevCartOrderItems, name);
    this.checkForMergeCartErrors();
  }

  componentWillUnmount() {
    const {
      clearIsBillingVisitedState,
      resetAnalyticsData,
      setCheckoutStage,
      setMaxItemErrorAction,
      setCouponMergeErrorAction,
      resetShippingEddData,
      currentStage,
    } = this.props;
    const isConfirmationPage =
      currentStage && currentStage.toLowerCase() === constants.CHECKOUT_STAGES.CONFIRMATION;
    clearIsBillingVisitedState();
    resetAnalyticsData();
    setCheckoutStage(null);
    setMaxItemErrorAction(false);
    setCouponMergeErrorAction(false);
    if (isConfirmationPage) {
      resetShippingEddData();
    }
  }

  validateCartMergeMessaging = (isGuest, showMaxItemError, isAppMessageDisplayed) =>
    !isGuest && !showMaxItemError && !isAppMessageDisplayed;

  checkForMergeCartErrors = async () => {
    const {
      setCouponMergeErrorAction,
      setMaxItemErrorAction,
      currentUserId,
      showMaxItemError,
      showMaxItemErrorDisplayed,
      isGuest,
    } = this.props;
    const isAppMessageDisplayed = this.isMobileApp && showMaxItemErrorDisplayed;
    if (this.validateCartMergeMessaging(isGuest, showMaxItemError, isAppMessageDisplayed)) {
      const notificationFlag = this.isMobileApp
        ? await readCookie(`MC_${currentUserId}`)
        : readCookie(`MC_${currentUserId}`);
      if (notificationFlag) {
        if (notificationFlag.indexOf('ex_quantity') !== -1) {
          setMaxItemErrorAction(true);
        }
        if (notificationFlag.indexOf('ex_coupon') !== -1) {
          setCouponMergeErrorAction(true);
        }
        if (!this.isMobileApp) {
          removeCookie(`MC_${currentUserId}`, isTCP());
        }
      }
    }
  };

  getStepForAnalytics = (eventLabel) => {
    let step = 3;
    if (eventLabel.toLowerCase() === constants.CHECKOUT_STAGES.SHIPPING) step = 1;
    else if (eventLabel.toLowerCase() === constants.CHECKOUT_STAGES.BILLING) step = 2;
    return step;
  };

  shouldCMEventsFire = (currentStage) => {
    const analyticsObj = JSON.parse(getSessionStorage('AnalyticsObj')) || {};

    if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.SHIPPING) {
      const { metric9 = false } = analyticsObj;
      if (!metric9) {
        setSessionStorage({
          key: 'AnalyticsObj',
          value: JSON.stringify({ ...analyticsObj, metric9: true }),
        });
        return true;
      }
      return false;
    }
    if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.BILLING) {
      const { metric11 = false } = analyticsObj;
      if (!metric11) {
        setSessionStorage({
          key: 'AnalyticsObj',
          value: JSON.stringify({ ...analyticsObj, metric11: true }),
        });
        return true;
      }
      return false;
    }
    const { metric12 = false } = analyticsObj;
    if (!metric12) {
      setSessionStorage({
        key: 'AnalyticsObj',
        value: JSON.stringify({ ...analyticsObj, metric12: true }),
      });
      return true;
    }
    return false;
  };

  getStepMetrics = (currentStage) => {
    const shouldEventFire = this.shouldCMEventsFire(currentStage);
    if (shouldEventFire) {
      if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.SHIPPING)
        return { cm9: '1', cm11: '0', cm12: '0' };
      if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.BILLING)
        return { cm9: '0', cm11: '1', cm12: '0' };
      if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.REVIEW)
        return { cm9: '0', cm11: '0', cm12: '1' };
    }
    return null;
  };

  addMtagObj = (item) => {
    const { cartAnalyticsData } = this.props;
    let obj = {};
    const itemData = cartAnalyticsData.find((cartItem) => {
      return cartItem.skuId === item.sku && cartItem.name.toLowerCase() === item.name.toLowerCase();
    });
    if (itemData) {
      obj = {
        dimension124: itemData && itemData.dimension124,
        dimension125: itemData && itemData.dimension125,
        dimension199: itemData && itemData.dimension199,
      };
    }
    if (itemData && isMobileApp()) {
      obj = {
        mpackPageType: itemData && itemData.dimension124,
        mpackValue: itemData && itemData.dimension125,
        mPackStyleType: itemData && itemData.dimension199,
      };
    }
    return obj;
  };

  getShipBillReviewArgs = (products, dataLayer, currentStage) => {
    if (!products) return null;
    const { currency } = this.props;

    const args = {
      eventName: 'checkout',
      eventAction: 'Checkout',
      eventLabel: currentStage,
      eventType: 'checkout',
      ...this.getStepMetrics(currentStage),
      step: this.getStepForAnalytics(currentStage),
      productsObj: [],
    };
    args.productsObj = products.map((item) => {
      return {
        id: item.partNumber,
        price: item.price.toFixed(2),
        brand: item.prodBrand,
        category: '',
        variant: item.color,
        quantity: item.quantity,
        dimension35: item.prodBrand,
        dimension63: `${item.price.toFixed(2)} ${currency}: ${item.listPrice.toFixed(
          2
        )} ${currency}`,
        dimension77: item.color,
        dimension82: item.upc,
        dimension67: item.price < item.listPrice ? 'on sale' : 'full price',
        dimension88: item.size,
        dimension89: item.partNumber,
        dimension95: item.sku,
        dimension98: dataLayer && dataLayer.checkoutType,
        ...addRecommendationsObj(item.partNumber),
        ...this.addMtagObj(item),
      };
    });
    return args;
  };

  getConfirmationPageArgsNormal = (commonData, productsData, dataLayer, cartTypes) => {
    const {
      currentOrderId,
      paymentMethodId,
      currency,
      confirmationPageLedgerSummaryData,
      grandTotal,
      billingAddress,
      mixOrderDetails,
    } = this.props;
    let orderData = {};
    const { zipCode = '', country = '' } = billingAddress || {};
    let orderId = currentOrderId;
    if (
      mixOrderDetails &&
      mixOrderDetails.length &&
      mixOrderDetails[0] &&
      mixOrderDetails[0].subOrderId
    ) {
      orderId = mixOrderDetails[0].subOrderId;
    }
    orderData.actionField = {
      id: orderId,
      revenue: grandTotal,
      tax: confirmationPageLedgerSummaryData.taxesTotal,
      shipping: confirmationPageLedgerSummaryData.shippingTotal,
      coupon: dataLayer && dataLayer.couponCode,
      shippingAmount: confirmationPageLedgerSummaryData.shippingTotal,
      giftCardAmount: confirmationPageLedgerSummaryData.giftCardsTotal,
      taxAmount: confirmationPageLedgerSummaryData.taxesTotal,
      promotionAmount: confirmationPageLedgerSummaryData.savingsTotal,
      metric6: confirmationPageLedgerSummaryData.giftCardsTotal,
      metric7: `${confirmationPageLedgerSummaryData.couponsTotal} ${currency}`,
      metric22: confirmationPageLedgerSummaryData.savingsTotal,
      metric78: confirmationPageLedgerSummaryData.shippingTotal,
      metric79: confirmationPageLedgerSummaryData.taxesTotal,
      metric99: confirmationPageLedgerSummaryData.grandTotal,
      dimension4: paymentMethodId,
      dimension86: cartTypes,
      dimension73: confirmationPageLedgerSummaryData.grandTotal,
      dimension98: dataLayer && dataLayer.checkoutType,
      dimension68: zipCode,
      dimension9: country,
      dimension3: orderId,
      dimension6: dataLayer && dataLayer.couponCode,
      dimension79: confirmationPageLedgerSummaryData.grandTotal,
    };

    orderData.productsObj = productsData.map((item) => {
      const prodDataObj = {
        id: item.partNumber,
        variant: item.color,
        brand: item.prodBrand,
        quantity: item.quantity,
        price: item.price,
        dimension5: item.shippingMethod,
        dimension35: item.prodBrand,
        dimension53: item.storeId,
        dimension77: item.color,
        dimension88: item.size,
        dimension89: item.partNumber,
        dimension57: dataLayer && dataLayer.giftType,
        ...addRecommendationsObj(item.partNumber),
        ...this.addMtagObj(item),
      };
      delete prodDataObj.dimension87;
      return prodDataObj;
    });
    orderData = { ...commonData, ...orderData };
    return orderData;
  };

  getConfirmationPageArgsMix = (commonData, productsData, dataLayer, cartTypes) => {
    const { paymentMethodId, currency, billingAddress, mixOrderDetails } = this.props;

    const { zipCode = '', country = '' } = billingAddress || {};
    const orders = [];
    let bopisOrder = {};
    let normalOrder = {};
    mixOrderDetails.forEach((orderData) => {
      const order = {
        actionField: {
          id: orderData.subOrderId,
          revenue: orderData.subtotal,
          tax: orderData.totalTax,
          shipping: orderData.totalShipping,
          coupon: dataLayer && dataLayer.couponCode,
          shippingAmount: orderData.totalShipping,
          giftCardAmount: 0,
          taxAmount: orderData.totalTax,
          promotionAmount: 0,
          metric6: 0,
          metric7: 0,
          metric22: 0,
          metric78: orderData.totalShipping,
          metric79: orderData.totalTax,
          metric99: orderData.subtotal,
          dimension4: paymentMethodId,
          dimension86: cartTypes,
          dimension73: orderData.subtotal,
          dimension98: dataLayer && dataLayer.checkoutType,
          dimension68: zipCode,
          dimension9: country,
          dimension3: orderData.subOrderId,
          dimension6: dataLayer && dataLayer.couponCode,
          dimension79: orderData.subtotal,
        },
      };
      if (orderData.orderType === 'BOSS') {
        bopisOrder = order;
      } else {
        normalOrder = order;
      }
    });

    bopisOrder = { ...commonData, ...bopisOrder };
    normalOrder = { ...commonData, ...normalOrder };

    const bopisProducts = [];
    const normalProducts = [];

    productsData.forEach((item) => {
      const productData = {
        id: item.partNumber,
        variant: item.color,
        brand: item.prodBrand,
        quantity: item.quantity,
        price: item.price,
        dimension5: item.shippingMethod,
        dimension35: item.prodBrand,
        dimension53: item.storeId,
        dimension77: item.color,
        dimension88: item.size,
        dimension89: item.partNumber,
        dimension57: dataLayer && dataLayer.giftType,
        ...addRecommendationsObj(item.partNumber),
        ...this.addMtagObj(item),
      };
      delete productData.dimension87;

      if (item.storeId) {
        if (item.giftCardAmount) {
          bopisOrder.actionField.giftCardAmount += item.giftCardAmount;
          bopisOrder.actionField.metric6 = bopisOrder.actionField.giftCardAmount;
        }
        if (item.savingsAmount) {
          bopisOrder.actionField.promotionAmount += item.savingsAmount;
          bopisOrder.actionField.metric22 = bopisOrder.actionField.promotionAmount;
        }
        if (item.couponAmount) {
          bopisOrder.actionField.metric7 += item.couponAmount;
        }
        bopisProducts.push(productData);
      } else {
        if (item.giftCardAmount) {
          normalOrder.actionField.giftCardAmount += item.giftCardAmount;
          normalOrder.actionField.metric6 = normalOrder.actionField.giftCardAmount;
        }
        if (item.savingsAmount) {
          normalOrder.actionField.promotionAmount += item.savingsAmount;
          normalOrder.actionField.metric22 = normalOrder.actionField.promotionAmount;
        }
        if (item.couponAmount) {
          normalOrder.actionField.metric7 += item.couponAmount;
        }
        normalProducts.push(productData);
      }
    });
    bopisOrder.productsObj = bopisProducts;
    normalOrder.productsObj = normalProducts;
    bopisOrder.actionField.metric7 = `${bopisOrder.actionField.metric7} ${currency}`;
    normalOrder.actionField.metric7 = `${normalOrder.actionField.metric7} ${currency}`;
    orders.push(bopisOrder);
    orders.push(normalOrder);
    return orders;
  };

  getConfirmationPageArgs = (productsData, dataLayer) => {
    const { currency } = this.props;

    const args = {
      eventName: 'purchases',
      eventAction: 'Purchase',
      eventLabel: 'Purchase',
      currCode: currency,
      eventType: 'purchase',
    };
    let data = [];
    const uniqueCartTypes = new Set(
      productsData.map((el) => {
        if (!el.orderItemType || el.orderItemType === 'ECOM') return 'standard';
        return el.orderItemType.toLowerCase();
      })
    );
    const cartTypes =
      uniqueCartTypes && uniqueCartTypes.size && Array.from(uniqueCartTypes).join('|');
    if (uniqueCartTypes && uniqueCartTypes.size > 1) {
      data = this.getConfirmationPageArgsMix(args, productsData, dataLayer, cartTypes);
    } else {
      data = [this.getConfirmationPageArgsNormal(args, productsData, dataLayer, cartTypes)];
    }
    return data;
  };

  updateGoogleAnalyticsData = (currentStage) => {
    const isConfirmationPage =
      currentStage.toLowerCase() === constants.CHECKOUT_STAGES.CONFIRMATION;
    const productsData = this.formattedProductsData();
    let args = {};
    const { _dataLayer: dataLayer = {} } = window || {};
    try {
      if (isConfirmationPage && !this.googleAnalyticsTriggered) {
        this.googleAnalyticsTriggered = true;
        args = this.getConfirmationPageArgs(productsData, dataLayer);
      } else {
        args = this.getShipBillReviewArgs(productsData, dataLayer, currentStage);
      }
    } catch (ex) {
      logger.error({
        error: ex,
        extraData: {
          componentName: 'CheckoutCommonContainer',
          methodName: 'updateGoogleAnalyticsData',
          checkoutStage: currentStage,
        },
      });
    }
    if (Object.keys(args).length) fireEnhancedEcomm(args);
  };

  formattedProductsData = () => {
    const {
      cartOrderItems,
      confirmationPageLedgerSummaryData,
      currentStage,
      shippingMethodSelected,
      orderBalanceTotal,
      giftCardTotal,
      grandTotal,
    } = this.props;
    const { displayName, price } = shippingMethodSelected;
    const isConfirmationPage =
      currentStage && currentStage.toLowerCase() === constants.CHECKOUT_STAGES.CONFIRMATION;
    const shippingMethodTitle =
      price > 0 ? `${displayName.toLowerCase()} - $${price}` : displayName;

    const netRevenue = giftCardTotal > 0 ? grandTotal : orderBalanceTotal;

    return BagPageUtils.formatBagProductsData(
      cartOrderItems,
      {
        shippingMethod: shippingMethodTitle,
        shippingAmount: price,
        netRevenue,
      },
      confirmationPageLedgerSummaryData,
      isConfirmationPage
    );
  };

  sbpAnalytics = (productsData, isMobile) => {
    return new Promise(async (resolve) => {
      if (isMobile) {
        try {
          let sbpProducts = await getValueFromAsyncStorage(sbpConstant.PRODUCT_CACHE);
          sbpProducts = JSON.parse(sbpProducts);
          productsData.forEach((product) => {
            const prod = product;
            if (sbpProducts && sbpProducts.indexOf(prod.partNumber) !== -1) prod.sbp = true;
          });
        } catch (e) {
          logger.error(e);
        }
      }
      resolve();
    });
  };

  getConfirmationPageAdditionalData = () => {
    const { currentOrderId, paymentMethodId, billingAddress, orderSubTotal } = this.props;

    return {
      orderId: currentOrderId && currentOrderId.toString(),
      paymentMethod: paymentMethodId,
      billingZip: billingAddress && billingAddress.zipCode,
      billingCountry: billingAddress && billingAddress.country,
      orderSubtotal: orderSubTotal.toString(),
      confirmationPage: true,
    };
  };

  fireAnalyticsEvents = (currentStage) => {
    const { trackPageViewCheckout } = this.props;
    const { CHECKOUT_PAGE } = constants;
    if (!isMobileApp()) {
      this.updateGoogleAnalyticsData(currentStage);
      const timer = setTimeout(() => {
        trackPageViewCheckout({
          currentScreen: currentStage.toLowerCase(),
          pageData: {
            pageName: `${CHECKOUT_PAGE}:${currentStage.toLowerCase()}`,
            pageSection: CHECKOUT_PAGE,
            pageSubSection: CHECKOUT_PAGE,
            pageType: CHECKOUT_PAGE,
            pageShortName: `${CHECKOUT_PAGE}:${currentStage.toLowerCase()}`,
          },
        });
        clearTimeout(timer);
      }, 100);
    } else if (!this.firebaseAnalyticsTriggered) {
      this.firebaseAnalyticsTriggered = true;
      trackPageViewCheckout({
        currentScreen: currentStage.toLowerCase(),
        pageData: {
          pageName: `${CHECKOUT_PAGE}:${currentStage.toLowerCase()}`,
          pageSection: CHECKOUT_PAGE,
          pageSubSection: CHECKOUT_PAGE,
          pageType: CHECKOUT_PAGE,
          pageShortName: `${CHECKOUT_PAGE}:${currentStage.toLowerCase()}`,
        },
      });
    }
  };

  setAnalyticsTriggered = () => {
    this.firebaseAnalyticsTriggered = false;
  };

  confirmationPageDataClear = () => {
    const { resetCartCheckoutData } = this.props;
    if (this.isMobileApp) {
      clearRecommendationsObj();
    }
    resetCartCheckoutData();
  };
  /**
   * Below method for starting load analytics events
   * for all different checkout pages
   */

  startCheckoutAnalytics = async (
    currentStage,
    cartOrderItems,
    prevCartOrderItems,
    giftServiceName = ''
  ) => {
    const { setClickAnalyticsDataCheckout, isMobile } = this.props;
    const isConfirmationPage =
      currentStage.toLowerCase() === constants.CHECKOUT_STAGES.CONFIRMATION;

    if ((cartOrderItems !== prevCartOrderItems || isConfirmationPage) && !this.analyticsTriggered) {
      let additionalData;
      /** This additional data only on order confirmation load event */

      if (isConfirmationPage) {
        additionalData = this.getConfirmationPageAdditionalData();
      }

      const productsData = this.formattedProductsData();
      const events = this.getAnalyticsEvents();
      const isReviewPage = currentStage.toLowerCase() === constants.CHECKOUT_STAGES.REVIEW;

      if (isReviewPage) {
        setSessionStorage({ key: 'PREFERRED_GIFT_SERVICE', value: giftServiceName });
      }

      this.analyticsTriggered = true;
      await this.sbpAnalytics(productsData, isMobile);
      let recsObj = {};
      if (isMobileApp()) {
        recsObj = await getRecommendationsObj();
      }
      const productsDataObj = productsData.map((item) => {
        const recObj = (recsObj && recsObj[item.partNumber]) || {};
        return {
          ...item,
          ...this.addMtagObj(item),
          ...recObj,
        };
      });

      setClickAnalyticsDataCheckout({
        customEvents: events,
        products: productsDataObj,
        clickEvent: true,
        ...additionalData,
      });

      this.fireAnalyticsEvents(currentStage);

      // Clear the state after pageView event
      const analyticsTimer = setTimeout(() => {
        setClickAnalyticsDataCheckout({});
        clearTimeout(analyticsTimer);
      }, 800);

      if (isConfirmationPage) {
        this.confirmationPageDataClear();
      }
      if (!isConfirmationPage) this.analyticsTriggered = false;
    }
  };

  getAnalyticsEvents = () => {
    const { currentStage, isMobile } = this.props;
    const events = [];
    if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.PICKUP) {
      events.push('scCheckout', 'event69');
    } else if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.SHIPPING) {
      events.push('scCheckout', 'event9');
    } else if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.BILLING) {
      events.push('scCheckout', 'event11');
    } else if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.REVIEW) {
      events.push('scCheckout', 'event12');
    } else if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.CONFIRMATION) {
      events.push('purchase', 'event5');
    }
    if (!isMobile) {
      if (currentStage.toLowerCase() === constants.CHECKOUT_STAGES.CONFIRMATION) {
        events.push('event6', 'event7', 'event78', 'event79', 'event99', 'event2');
      } else {
        events.push('event86');
      }
    }
    return events;
  };

  shippingDidMount = () => {
    const { rememberedUserFlag, isMobile } = this.props;
    if (!rememberedUserFlag || isMobile) {
      this.firebaseAnalyticsTriggered = false;
      intiSectionPage(constants.CHECKOUT_STAGES.SHIPPING, this, {
        initialLoad: this.initialLoad,
      });
    }
  };

  billingDidMount = () => {
    const { rememberedUserFlag, isMobile } = this.props;
    if (!rememberedUserFlag || isMobile) {
      this.firebaseAnalyticsTriggered = false;
      intiSectionPage(constants.CHECKOUT_STAGES.BILLING, this);
    }
  };

  reviewDidMount = () => {
    const { rememberedUserFlag, isMobile } = this.props;
    if (!rememberedUserFlag || isMobile) {
      this.firebaseAnalyticsTriggered = false;
      intiSectionPage(constants.CHECKOUT_STAGES.REVIEW, this);
    }
  };

  pickupDidMount = () => {
    const { rememberedUserFlag, isMobile } = this.props;
    if (!rememberedUserFlag || isMobile) {
      this.firebaseAnalyticsTriggered = false;
      intiSectionPage(constants.CHECKOUT_STAGES.PICKUP, this);
    }
  };

  handleSubmitShippingSection = (payload) => {
    const { submitShipping } = this.props;
    const productsData = this.formattedProductsData();
    submitShipping({ ...payload, productsData });
  };

  handleSubmitBillingSection = (payload) => {
    const { submitBilling } = this.props;
    const productsData = this.formattedProductsData();
    submitBilling({ ...payload, productsData });
  };

  handleReviewSubmit = (payload) => {
    const { submitReview } = this.props;
    this.firebaseAnalyticsTriggered = false;
    submitReview(payload);
  };

  restructureEddData = (itemsWithEdd, selectedShippingCode) => {
    const eddData = [];
    itemsWithEdd.forEach((itm) => {
      eddData.push({
        edd: itm.shippingDates[selectedShippingCode],
        orderItemId: itm.orderItemId,
        quantity: itm.quantity,
        node: itm.node,
        nodeProcessingTime: itm.nodeProcessingTime[selectedShippingCode],
        transitTime: itm.transitTime[selectedShippingCode],
        carrier: itm.carrier && itm.carrier[selectedShippingCode],
      });
    });
    return eddData;
  };

  getShipmentMethod = (itemsWithEdd) => {
    const shippingDates = itemsWithEdd && itemsWithEdd[0] && itemsWithEdd[0].shippingDates;
    let selectedShipmentMethod = '';
    if (shippingDates) {
      if (shippingDates.UGNR) {
        selectedShipmentMethod = constants.SHIPPING_CODE.UGNR;
      } else if (shippingDates.PMDC) {
        selectedShipmentMethod = constants.SHIPPING_CODE.PMDC;
      } else if (shippingDates.U2AK) {
        selectedShipmentMethod = constants.SHIPPING_CODE.U2AK;
      }
    }
    return selectedShipmentMethod;
  };

  formatEddPersistParams = () => {
    const { itemsWithEdd, currentOrderId, orderEddDates, selectedShippingCode } = this.props;
    const shippingCode = !selectedShippingCode
      ? this.getShipmentMethod(itemsWithEdd)
      : selectedShippingCode;
    return {
      orderId: currentOrderId,
      orderItems: itemsWithEdd ? this.restructureEddData(itemsWithEdd, shippingCode) : null,
      orderEdd:
        (shippingCode && orderEddDates && getDateRange(orderEddDates, shippingCode, true)) || '',
    };
  };

  handlePersistEDD = (isShowEDD) => {
    const { persistEddNodeAction } = this.props;
    const payload = this.formatEddPersistParams();
    if (isShowEDD) {
      payload.showEDD = 'true';
    }
    if (payload && payload.orderId && payload.orderItems) {
      persistEddNodeAction(payload);
    }
  };

  render() {
    const {
      initialValues,
      pickupInitialValues,
      onEditModeChange,
      isSmsUpdatesEnabled,
      currentPhoneNumber,
      isMobile,
      activeStage,
      activeStep,
      isUsSite,
      shippingProps,
      navigation,
      orderHasPickUp,
      isOrderUpdateChecked,
      isGiftServicesChecked,
      isAlternateUpdateChecked,
      pickUpLabels,
      smsSignUpLabels,
      onPickupSubmit,
      updateFromMSG,
      loadShipmentMethods,
      isGuest,
      isExpressCheckoutPage,
      cartOrderItems,
      orderHasShipping,
      routeToPickupPage,
      setCheckoutStage,
      router,
      updateShippingMethodSelection,
      updateShippingAddressData,
      addNewShippingAddressData,
      labels,
      checkoutProgressBarLabels,
      reviewProps,
      isVenmoPaymentInProgress,
      isApplePaymentInProgress,
      setVenmoPickupState,
      verifyAddressAction,
      setVenmoShippingState,
      getPayPalSettings,
      checkoutServerError,
      currentStage,
      submitVerifiedShippingAddressData,
      shippingMethod,
      pickUpAlternatePerson,
      isHasPickUpAlternatePerson,
      isVenmoPickupBannerDisplayed,
      isVenmoShippingBannerDisplayed,
      isPayPalWebViewEnable,
      setClickAnalyticsDataCheckout,
      updateCheckoutPageData,
      dispatchReviewReduxForm,
      pageData,
      dispatch,
      titleLabel,
      initShippingPage,
      smsOrderUpdatesRichText,
      emailSignUpCaRichText,
      excludedIds,
      toggleCheckoutFocus,
      zipCodeABTestEnabled,
      getZipCodeSuggestedAddress,
      zipCodeLoader,
      itemsWithEdd,
      selectedShippingCode,
      updateEdd,
      currentOrderId,
      resetShippingEddData,
      appliedExpiredCoupon,
      pageName,
      addItemToSflList,
      setAppleProgress,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      setAfterPayProgress,
      orderBalanceTotal,
      trackClickAfterPay,
    } = this.props;

    const {
      pickUpContactPerson,
      pickUpContactAlternate,
      emailSignUpFlags,
      isEDD,
      eddAllowedStatesArr,
      isEddABTest,
      currency,
    } = this.props;
    const { isRegisteredUserCallDone, checkoutRoutingDone, isBonusPointsEnabled, currentUserId } =
      this.props;
    const { toggleCountrySelector, checkoutPageEmptyBagLabels, isBagLoaded } = this.props;
    const { toastMessage, clearCheckoutServerError, cartOrderItemsCount, bagLoading } = this.props;
    const {
      setCouponMergeErrorAction,
      setMaxItemErrorAction,
      showMaxItemError,
      showMaxItemErrorDisplayed,
      setMaxItemErrorDisplayedAction,
      couponRemovedFromOrderFlag,
      setCouponRemovedFromOrderFlag,
    } = this.props;
    const availableStages = checkoutUtil.getAvailableStages(cartOrderItems);
    return (
      <CheckoutPage
        pickupDidMount={this.pickupDidMount}
        emailSignUpFlags={emailSignUpFlags}
        isRegisteredUserCallDone={isRegisteredUserCallDone}
        isBagLoaded={isBagLoaded}
        initialValues={initialValues}
        onEditModeChange={onEditModeChange}
        isSmsUpdatesEnabled={isSmsUpdatesEnabled}
        currentPhoneNumber={currentPhoneNumber}
        isGuest={isGuest}
        billingProps={{
          billingDidMount: this.billingDidMount,
          isRegisteredUserCallDone,
        }}
        formattedProductsData={this.formattedProductsData()}
        dispatchReviewReduxForm={dispatchReviewReduxForm}
        isMobile={isMobile}
        isExpressCheckout={isExpressCheckoutPage}
        checkoutProgressBarLabels={checkoutProgressBarLabels}
        activeStage={activeStage}
        activeStep={activeStep}
        isUsSite={isUsSite}
        orderHasShipping={orderHasShipping}
        pickupInitialValues={pickupInitialValues}
        bagLoading={bagLoading}
        isOrderUpdateChecked={isOrderUpdateChecked}
        isGiftServicesChecked={isGiftServicesChecked}
        isAlternateUpdateChecked={isAlternateUpdateChecked}
        submitVerifiedShippingAddressData={submitVerifiedShippingAddressData}
        pickUpLabels={pickUpLabels}
        smsSignUpLabels={smsSignUpLabels}
        navigation={navigation}
        onPickupSubmit={onPickupSubmit}
        updateFromMSG={updateFromMSG}
        verifyAddressAction={verifyAddressAction}
        shippingProps={{
          ...shippingProps,
          shippingDidMount: this.shippingDidMount,
          isRegisteredUserCallDone,
          dispatch,
        }}
        orderHasPickUp={orderHasPickUp}
        checkoutRoutingDone={checkoutRoutingDone}
        submitShippingSection={this.handleSubmitShippingSection}
        loadShipmentMethods={loadShipmentMethods}
        cartOrderItems={cartOrderItems}
        routeToPickupPage={routeToPickupPage}
        setCheckoutStage={setCheckoutStage}
        availableStages={availableStages}
        router={router}
        updateShippingMethodSelection={updateShippingMethodSelection}
        updateShippingAddressData={updateShippingAddressData}
        addNewShippingAddressData={addNewShippingAddressData}
        labels={labels}
        submitBilling={this.handleSubmitBillingSection}
        submitReview={this.handleReviewSubmit}
        reviewProps={{
          ...reviewProps,
          reviewDidMount: this.reviewDidMount,
          isRegisteredUserCallDone,
        }}
        formatPayload={formatPayload}
        isVenmoPaymentInProgress={isVenmoPaymentInProgress}
        isApplePaymentInProgress={isApplePaymentInProgress}
        setVenmoPickupState={setVenmoPickupState}
        setVenmoShippingState={setVenmoShippingState}
        getPayPalSettings={getPayPalSettings}
        checkoutServerError={checkoutServerError}
        currentStage={currentStage}
        shippingMethod={shippingMethod}
        pickUpAlternatePerson={pickUpAlternatePerson}
        isHasPickUpAlternatePerson={isHasPickUpAlternatePerson}
        pickUpContactPerson={pickUpContactPerson}
        pickUpContactAlternate={pickUpContactAlternate}
        isVenmoPickupBannerDisplayed={isVenmoPickupBannerDisplayed}
        isVenmoShippingBannerDisplayed={isVenmoShippingBannerDisplayed}
        toastMessage={toastMessage}
        clearCheckoutServerError={clearCheckoutServerError}
        toggleCountrySelector={toggleCountrySelector}
        cartOrderItemsCount={cartOrderItemsCount}
        checkoutPageEmptyBagLabels={checkoutPageEmptyBagLabels}
        isPayPalWebViewEnable={isPayPalWebViewEnable}
        setClickAnalyticsDataCheckout={setClickAnalyticsDataCheckout}
        updateCheckoutPageData={updateCheckoutPageData}
        pageData={pageData}
        titleLabel={titleLabel}
        isBonusPointsEnabled={isBonusPointsEnabled}
        initShippingPage={initShippingPage}
        smsOrderUpdatesRichText={smsOrderUpdatesRichText}
        emailSignUpCaRichText={emailSignUpCaRichText}
        excludedIds={excludedIds}
        toggleCheckoutFocus={toggleCheckoutFocus}
        zipCodeABTestEnabled={zipCodeABTestEnabled}
        getZipCodeSuggestedAddress={getZipCodeSuggestedAddress}
        zipCodeLoader={zipCodeLoader}
        persistEddNodeAction={this.handlePersistEDD}
        itemsWithEdd={itemsWithEdd}
        selectedShippingCode={selectedShippingCode}
        isEDD={isEDD}
        updateEdd={updateEdd}
        eddAllowedStatesArr={eddAllowedStatesArr}
        currentOrderId={currentOrderId}
        resetShippingEddData={resetShippingEddData}
        isEddABTest={isEddABTest}
        appliedExpiredCoupon={appliedExpiredCoupon}
        pageName={pageName}
        currency={currency}
        addItemToSflList={addItemToSflList}
        setAppleProgress={setAppleProgress}
        currentUserId={currentUserId}
        mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
        setCouponMergeErrorAction={setCouponMergeErrorAction}
        setMaxItemErrorAction={setMaxItemErrorAction}
        setMaxItemErrorDisplayedAction={setMaxItemErrorDisplayedAction}
        mapboxSwitch={mapboxSwitch}
        showMaxItemError={showMaxItemError}
        showMaxItemErrorDisplayed={showMaxItemErrorDisplayed}
        setAfterPayProgress={setAfterPayProgress}
        orderBalanceTotal={orderBalanceTotal}
        trackClickAfterPay={trackClickAfterPay}
        setAnalyticsTriggered={this.setAnalyticsTriggered}
        couponRemovedFromOrderFlag={couponRemovedFromOrderFlag}
        setCouponRemovedFromOrderFlag={setCouponRemovedFromOrderFlag}
      />
    );
  }
}

CheckoutContainer.getInitActions = () => initActions;

CheckoutContainer.pageInfo = {
  pageId: 'Checkout',
};

CheckoutContainer.getInitialProps = (reduxProps, pageProps) => {
  const DEFAULT_ACTIVE_COMPONENT = 'shipping';
  const loadedComponent = utils.getObjectValue(
    reduxProps,
    DEFAULT_ACTIVE_COMPONENT,
    'query',
    'section'
  );
  return {
    ...pageProps,
    ...{
      pageData: {
        pageName: `checkout:${loadedComponent}`,
        pageSection: 'checkout',
        pageSubSection: 'checkout',
        pageType: 'checkout',
        pageShortName: `checkout:${loadedComponent}`,
        loadAnalyticsOnload: false,
      },
    },
  };
};

CheckoutContainer.propTypes = {
  initialValues: PropTypes.shape({}).isRequired,
  pickupInitialValues: PropTypes.shape({}).isRequired,
  onEditModeChange: PropTypes.func.isRequired,
  isSmsUpdatesEnabled: PropTypes.bool.isRequired,
  currentPhoneNumber: PropTypes.string.isRequired,
  isMobile: PropTypes.bool.isRequired,
  activeStage: PropTypes.string.isRequired,
  activeStep: PropTypes.string.isRequired,
  isUsSite: PropTypes.bool.isRequired,
  shippingProps: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  orderHasPickUp: PropTypes.bool.isRequired,
  isOrderUpdateChecked: PropTypes.bool.isRequired,
  isGiftServicesChecked: PropTypes.bool.isRequired,
  isAlternateUpdateChecked: PropTypes.bool.isRequired,
  pickUpLabels: PropTypes.shape({}).isRequired,
  smsSignUpLabels: PropTypes.shape({}).isRequired,
  onPickupSubmit: PropTypes.func.isRequired,
  updateFromMSG: PropTypes.func.isRequired,
  loadShipmentMethods: PropTypes.func.isRequired,
  isGuest: PropTypes.bool.isRequired,
  isExpressCheckoutPage: PropTypes.bool.isRequired,
  cartOrderItems: PropTypes.shape([]).isRequired,
  orderHasShipping: PropTypes.bool.isRequired,
  routeToPickupPage: PropTypes.func.isRequired,
  setCheckoutStage: PropTypes.func.isRequired,
  router: PropTypes.shape({}).isRequired,
  updateShippingMethodSelection: PropTypes.func.isRequired,
  updateShippingAddressData: PropTypes.func.isRequired,
  addNewShippingAddressData: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  checkoutProgressBarLabels: PropTypes.shape({}).isRequired,
  reviewProps: PropTypes.shape({}).isRequired,
  isVenmoPaymentInProgress: PropTypes.bool.isRequired,
  isApplePaymentInProgress: PropTypes.bool.isRequired,
  setVenmoPickupState: PropTypes.func.isRequired,
  verifyAddressAction: PropTypes.func.isRequired,
  setVenmoShippingState: PropTypes.func.isRequired,
  getPayPalSettings: PropTypes.func.isRequired,
  checkoutServerError: PropTypes.func.isRequired,
  currentStage: PropTypes.string.isRequired,
  submitVerifiedShippingAddressData: PropTypes.func.isRequired,
  shippingMethod: PropTypes.shape({}).isRequired,
  pickUpAlternatePerson: PropTypes.func.isRequired,
  isHasPickUpAlternatePerson: PropTypes.bool.isRequired,
  isVenmoPickupBannerDisplayed: PropTypes.bool.isRequired,
  isVenmoShippingBannerDisplayed: PropTypes.bool.isRequired,
  isPayPalWebViewEnable: PropTypes.bool.isRequired,
  setClickAnalyticsDataCheckout: PropTypes.func.isRequired,
  updateCheckoutPageData: PropTypes.func.isRequired,
  dispatchReviewReduxForm: PropTypes.func.isRequired,
  pageData: PropTypes.shape({}).isRequired,
  titleLabel: PropTypes.shape({}).isRequired,
  initShippingPage: PropTypes.func.isRequired,
  smsOrderUpdatesRichText: PropTypes.func.isRequired,
  emailSignUpCaRichText: PropTypes.func.isRequired,
  excludedIds: PropTypes.string.isRequired,
  toggleCheckoutFocus: PropTypes.func.isRequired,
  zipCodeABTestEnabled: PropTypes.bool.isRequired,
  getZipCodeSuggestedAddress: PropTypes.func.isRequired,
  zipCodeLoader: PropTypes.bool.isRequired,
  itemsWithEdd: PropTypes.shape([]).isRequired,
  selectedShippingCode: PropTypes.string.isRequired,
  updateEdd: PropTypes.func.isRequired,
  currentOrderId: PropTypes.string.isRequired,
  resetShippingEddData: PropTypes.func.isRequired,
  initCheckout: PropTypes.func.isRequired,
  markBagPageRoutingDone: PropTypes.func.isRequired,
  resetAnalyticsData: PropTypes.func.isRequired,
  isRegisteredUserCallDone: PropTypes.bool.isRequired,
  clearCheckoutServerError: PropTypes.func.isRequired,
  isRTPSFlow: PropTypes.bool.isRequired,
  clearIsBillingVisitedState: PropTypes.func.isRequired,
  trackPageViewCheckout: PropTypes.func.isRequired,
  paymentMethodId: PropTypes.string.isRequired,
  billingAddress: PropTypes.shape({ zipCode: PropTypes.string, country: PropTypes.string })
    .isRequired,
  orderSubTotal: PropTypes.string.isRequired,
  resetCartCheckoutData: PropTypes.func.isRequired,
  confirmationPageLedgerSummaryData: PropTypes.func.isRequired,
  shippingMethodSelected: PropTypes.shape({
    displayName: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
  }).isRequired,
  orderBalanceTotal: PropTypes.number.isRequired,
  giftCardTotal: PropTypes.number.isRequired,
  grandTotal: PropTypes.number.isRequired,
  rememberedUserFlag: PropTypes.bool.isRequired,
  submitShipping: PropTypes.func.isRequired,
  submitReview: PropTypes.func.isRequired,
  submitBilling: PropTypes.func.isRequired,
  persistEddNodeAction: PropTypes.func.isRequired,
  pickUpContactPerson: PropTypes.shape({}).isRequired,
  pickUpContactAlternate: PropTypes.shape({}).isRequired,
  emailSignUpFlags: PropTypes.shape({}).isRequired,
  isEDD: PropTypes.bool.isRequired,
  eddAllowedStatesArr: PropTypes.shape([]).isRequired,
  isEddABTest: PropTypes.bool.isRequired,
  checkoutRoutingDone: PropTypes.bool.isRequired,
  isBonusPointsEnabled: PropTypes.bool.isRequired,
  checkoutPageEmptyBagLabels: PropTypes.shape({}).isRequired,
  isBagLoaded: PropTypes.bool.isRequired,
  toastMessage: PropTypes.string.isRequired,
  cartOrderItemsCount: PropTypes.number.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  orderEddDates: PropTypes.shape({}).isRequired,
  toggleCountrySelector: PropTypes.shape({}).isRequired,
  dispatch: PropTypes.func.isRequired,
  appliedExpiredCoupon: PropTypes.shape({}).isRequired,
  pageName: PropTypes.string.isRequired,
  addItemToSflList: PropTypes.func,
  currency: PropTypes.string,
  setAppleProgress: PropTypes.func.isRequired,
  mapboxAutocompleteTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  showMaxItemError: PropTypes.bool,
  showMaxItemErrorDisplayed: PropTypes.bool.isRequired,
  currentUserId: PropTypes.string,
  setCouponMergeErrorAction: PropTypes.func,
  setMaxItemErrorDisplayedAction: PropTypes.func.isRequired,
  setMaxItemErrorAction: PropTypes.func,
  cartAnalyticsData: PropTypes.shape([]),
  mixOrderDetails: PropTypes.shape({}),
  setConfirmationDoneActn: PropTypes.func,
  orderConfirmationStatus: PropTypes.bool,
  deviceId: PropTypes.string,
  setAfterPayProgress: PropTypes.bool,
  trackClickAfterPay: PropTypes.func,
};

CheckoutContainer.defaultProps = {
  addItemToSflList: () => {},
  currency: 'USD',
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  showMaxItemError: false,
  currentUserId: '',
  setCouponMergeErrorAction: () => {},
  setMaxItemErrorAction: () => {},
  cartAnalyticsData: [],
  mixOrderDetails: {},
  setConfirmationDoneActn: () => {},
  orderConfirmationStatus: false,
  deviceId: '',
  setAfterPayProgress: () => {},
  trackClickAfterPay: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutContainer);
