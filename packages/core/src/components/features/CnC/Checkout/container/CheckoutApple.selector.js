// 9fbef606107a605d69c0edbcd8029e5d 
import { isAndroid } from '@tcp/core/src/utils';
import { CHECKOUT_REDUCER_KEY } from '../../../../../constants/reducer.constants';

export const getAppleClientTokenData = state =>
  state[CHECKOUT_REDUCER_KEY].getIn(['values', 'appleClientTokenData']);

export const isApplePaymentInProgress = state => {
  return state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'applePaymentInProgress']);
};

export function isAppleOrderPayment(state) {
  const orderConfirmation = state.Confirmation && state.Confirmation.get('orderConfirmation');
  const paymentLists = orderConfirmation && orderConfirmation.paymentsList;
  const applePayment =
    paymentLists && paymentLists.find(method => method.paymentMethod.toLowerCase() === 'apple');
  return applePayment || false;
}

export const getApplePayAppDataApp = state => {
  return {
    loading: state[CHECKOUT_REDUCER_KEY].getIn(['values', 'applePayAppData', 'loading']),
    isEligibleForApplePay: isAndroid()
      ? false
      : state[CHECKOUT_REDUCER_KEY].getIn(['values', 'applePayAppData', 'isEligibleForApplePay']),
  };
};

export const getApplePayEligibleShippingState = state => {
  return (
    state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.APPLEPAY_ELIGIBLE_SHIPPING_STATES
  );
};
