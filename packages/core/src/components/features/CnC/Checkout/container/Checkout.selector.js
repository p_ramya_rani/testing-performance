/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { formValueSelector } from 'redux-form';
import { createSelector } from 'reselect';
import { List } from 'immutable';
import {
  CHECKOUT_REDUCER_KEY,
  CARTPAGE_REDUCER_KEY,
  SESSIONCONFIG_REDUCER_KEY,
} from '@tcp/core/src/constants/reducer.constants';
import {
  getAPIConfig,
  isMobileApp,
  isSafariBrowser,
  getViewportInfo,
  getLabelValue,
  parseBoolean,
  getABtestFromState,
  isIOS,
  isCanada,
} from '@tcp/core/src/utils';
import {
  getIsSmsUpdatesEnabled,
  getIsInternationalShipping,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
/* eslint-disable extra-rules/no-commented-out-code */
import {
  getPersonalDataState,
  getUserName,
  getUserLastName,
  getUserPhoneNumber,
  getUserEmail,
  getplccCardNumber,
  isPlccUser,
} from '../../../account/User/container/User.selectors';
import constants from '../Checkout.constants';
import { getAddressListState } from '../../../account/AddressBook/container/AddressBook.selectors';
import {
  getPickUpContactFormLabels,
  getGiftServicesFormData,
  getSyncError,
  getPaypalPaymentSettings,
  getExpressReviewShippingSectionId,
  getDefaultShipmentMethods,
  getIsOptimseUpdateShipment,
  getIsAfterPayInProgress,
  getReviewFormValues,
} from './Checkout.selector.util';
import {
  getVenmoData,
  getVenmoClientTokenData,
  isVenmoPaymentInProgress,
  isVenmoPickupBannerDisplayed,
  isVenmoShippingBannerDisplayed,
  isVenmoPaymentSaveSelected,
  getVenmoError,
  isVenmoNonceNotExpired,
  isVenmoPaymentToken,
  isVenmoNonceActive,
  isVenmoPaymentAvailable,
  getVenmoUserName,
  getVenmoPayment,
  isVenmoOrderPayment,
  getVenmoOrderUserId,
  isVenmoAppInstalled,
} from './CheckoutVenmo.selector';
import {
  getAppleClientTokenData,
  isApplePaymentInProgress,
  getApplePayAppDataApp,
  getApplePayEligibleShippingState,
} from './CheckoutApple.selector';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';

// import { getAddressListState } from '../../../account/AddressBook/container/AddressBook.selectors';

export const getCheckoutState = (state) => {
  return state[CHECKOUT_REDUCER_KEY];
};

export const getCheckoutUiFlagState = (state) => {
  return state[CHECKOUT_REDUCER_KEY].get('uiFlags');
};

export const getCheckoutValuesState = createSelector(
  getCheckoutState,
  (state) => state && state.get('values')
);

const getIsOrderHasShipping = (state) =>
  !!state[CARTPAGE_REDUCER_KEY].getIn(['orderDetails', 'isShippingOrder']);

const getIsOrderHasPickup = (state) =>
  !!state[CARTPAGE_REDUCER_KEY].getIn(['orderDetails', 'isPickupOrder']);

const getIfCheckoutRoutingDone = (state) => {
  return state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'routingDone']);
};

const getIfRedirectFromExpress = (state) => {
  return state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'redirectFromExpress']);
};

const getIfInitCheckoutFinished = (state) => {
  return state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'initCheckoutFinished']);
};

const getCardType = (state) => {
  return state.Checkout.getIn(['values', 'billing', 'billing', 'cardType']);
};

export const isGuest = createSelector(getPersonalDataState, (state) =>
  state == null ? true : !!state.get('isGuest')
);

function getIsMobile() {
  if (isMobileApp()) return true;
  if (typeof window === 'undefined')
    return {
      width: 0,
      height: 0,
      isMobile: false,
      isTablet: false,
      isDesktop: false,
    };
  return getViewportInfo().isMobile;
}

export const isExpressCheckout = createSelector(
  getPersonalDataState,
  (state) => state && state.get('isExpressEligible')
);

export const getCheckoutStage = createSelector(
  getCheckoutUiFlagState,
  (state) => state && state.get('stage')
);

export const isRemembered = createSelector(
  getPersonalDataState,
  (state) => state && state.get('isRemembered')
);

// function getUserContactInfo(state) {
//   return state.User.getIn(['personalData', 'contactInfo']);
// }

export const getUserContactInfo = createSelector(
  getPersonalDataState,
  (state) => state && state.get('contactInfo')
);

function getShippingDestinationValues(state) {
  const { emailAddress, ...result } = JSON.parse(
    JSON.stringify(state.Checkout.getIn(['values', 'shipping']))
  );
  // For shipping address when user logged-in, override email address that of user.
  // When user is guest, keep the address he specified in shipping section.
  return {
    emailAddress: getUserEmail(state) || emailAddress,
    ...result,
  };
}

const getShippingAddressID = createSelector(
  getShippingDestinationValues,
  (shippingDestinationValues) => {
    const { onFileAddressId } = shippingDestinationValues;
    return onFileAddressId;
  }
);

const getShippingAddress = createSelector(
  getShippingDestinationValues,
  (shippingDestinationValues) => {
    const { address } = shippingDestinationValues;
    return address;
  }
);

export const getbillingToReviewLoaderState = (state) => {
  return state.PageLoader && state.PageLoader.billingToReviewLoader;
};

const getAddEditResponseAddressId = (state) =>
  state.Checkout.getIn(['values', 'addEditResponseAddressId']);

// function getAddressBook(state, country, noBillingAddresses) {
//   let addresses = [];

//   if (!country) {
//     // if (noBillingAddresses) {
//     //   addresses = state.addresses.addressBook.filter(entry => entry.type !== ADDREESS_TYPE.BILLING);
//     // } else {
//     addresses = getAddressListState(state);
//     // }
//   } // else {
//   // let filtered = state.addresses.addressBook.filter(
//   //   entry =>
//   //     entry.address.country === country &&
//   //     (!noBillingAddresses || entry.type !== ADDREESS_TYPE.BILLING)
//   // );
//   // let defaultAddress = filtered.find(addressEntry => addressEntry.isDefault);

//   // // REVIEW: if there's no default for the selected requested country (country filter might leave it out)
//   // // then flag the first one as default. Can't be on the abstractor,
//   // // unless we store different versions of the address book (per country)
//   // // but I'm not sure about location because storeviews trigger on everything and want to avoid unnecesary renders
//   // if (!defaultAddress) {
//   //   addresses = filtered.map((entry, index) => {
//   //     return {
//   //       ...entry,
//   //       isDefault: index === 0,
//   //     };
//   //   });
//   // } else {
//   //   addresses = filtered;
//   // }
//   // }

//   return addresses;
// }

function getDefaultAddress(/* state, country, noBillingAddresses */) {
  return false;
  // const countryFilteredAddresses = getAddressBook(state, country, noBillingAddresses);
  // const defaultAddress = countryFilteredAddresses.find(
  //   addressEntry => addressEntry.get && addressEntry.get('primary')
  // );

  // if (countryFilteredAddresses.length && !defaultAddress) {
  //   return countryFilteredAddresses.get('0');
  // } else {
  //   return defaultAddress;
  // }
}

export const getPickupValues = createSelector(
  getCheckoutValuesState,
  (state) => state && state.get('pickUpContact')
);

// function getPickupValues(state) {
//   return state.Checkout.getIn(['values', 'pickUpContact']);
// }

export const getPickupAltValues = createSelector(
  getCheckoutValuesState,
  (state) => state && state.get('pickUpAlternative')
);
// function getPickupAltValues(state) {
//   return state.Checkout.getIn(['values', 'pickUpAlternative']);
// }

function getGiftWrappingValues(state) {
  return state.Checkout.getIn(['values', 'giftWrap']) || '';
}

function getCurrentSiteId() {
  return getAPIConfig().siteId;
}

export function isUsSite() {
  return getCurrentSiteId() === constants.ROUTING_CONST.siteIds.us;
}

function isSmsUpdatesEnabled(state) {
  return isUsSite() && getIsSmsUpdatesEnabled(state);
}

// function getCurrentPickupFormNumber(state) {
//   let phoneNumber = '';

//   try {
//     phoneNumber = state.form.getIn(['checkoutPickup', 'values', 'pickUpContact', 'phoneNumber']);
//   } catch (error) {
//     // Gobble...Gobble.
//   }

//   return phoneNumber;
// }
const getShippingGiftServicesField = (state) => {
  const selector = formValueSelector('checkoutShipping');
  return selector(state, 'giftServices');
};

const getShippingSmsSignUpFields = (state) => {
  const selector = formValueSelector('checkoutShipping');
  return selector(state, 'smsSignUp');
};

const getShipmentMethodsFields = (state) => {
  const selector = formValueSelector('checkoutShipping');
  return selector(state, 'shipmentMethods');
};

const getShippingPickupFields = (state) => {
  const selector = formValueSelector('checkoutPickup');
  return selector(state, 'pickUpContact');
};

const getSelectedShipmentId = createSelector(
  getShipmentMethodsFields,
  (shipmentMethodsFields) => shipmentMethodsFields && shipmentMethodsFields.shippingMethodId
);

const getShippingSendOrderUpdate = createSelector(
  getShippingSmsSignUpFields,
  (smsSignUpFields) => smsSignUpFields && smsSignUpFields.sendOrderUpdate
);

export const getGiftServicesSend = createSelector(
  getShippingGiftServicesField,
  (giftServicesFields) => giftServicesFields && giftServicesFields.sendGiftServices
);

const getSaveToAddressBook = (state) => {
  const selector = formValueSelector('checkoutShipping');
  return selector(state, 'saveToAddressBook');
};

const getOnFileAddressKey = (state) => {
  const selector = formValueSelector('checkoutShipping');
  return selector(state, 'onFileAddressKey');
};

const getAddressFields = (state) => {
  const selector = formValueSelector('checkoutShipping');
  return selector(state, 'address');
};

const getBillingAddressFields = (state) => {
  return state.Checkout.getIn(['values', 'billing', 'address']);
};

const getAddressPhoneNo = createSelector(
  getAddressFields,
  (addressFields) => addressFields && addressFields.phoneNumber
);

const getDefaultShipping = (state) => {
  const selector = formValueSelector('checkoutShipping');
  return selector(state, 'defaultShipping');
};

const getShippingPhoneAndEmail = createSelector(
  getShippingDestinationValues,
  (shippingDestinationValues) => {
    const { phoneNumber, emailAddress } = shippingDestinationValues;
    return { phoneNumber, emailAddress };
  }
);

const getCurrentPickupFormNumber = createSelector(
  getShippingPickupFields,
  (pickUpContact) => pickUpContact && pickUpContact.phoneNumber
);

const getBillingLabelValue = (state) =>
  state.Labels && state.Labels.checkout && state.Labels.checkout.billing;

const getBillingLabels = createSelector(
  getBillingLabelValue,

  (billingLabel) => {
    const labels = {};
    const labelKeys = [
      'lbl_billing_title',
      'lbl_billing_backLinkPickup',
      'lbl_billing_backLinkShipping',
      'lbl_billing_nextSubmit',
      'lbl_billing_billingAddress',
      'lbl_billing_sameAsShipping',
      'lbl_billing_paymentMethodTitle',
      'lbl_billing_saveToAccount',
      'lbl_billing_defaultPayment',
      'lbl_billing_default_card',
      'lbl_billing_addNewAddress',
      'lbl_billing_selectFromCard',
      'lbl_billing_addCreditHeading',
      'lbl_billing_default',
      'lbl_billing_cardDetailsTitle',
      'lbl_billing_editBtn',
      'lbl_billing_creditCard',
      'lbl_billing_creditCardEnd',
      'lbl_billing_addCreditBtn',
      'lbl_billing_paypal',
      'lbl_billing_venmo',
      'lbl_billing_applepay',
      'lbl_billing_selectCardTitle',
      'lbl_billing_select',
      'lbl_billing_cardEditCancel',
      'lbl_billing_cardEditSave',
      'lbl_billing_cvvCode',
      'lbl_billing_continueWith',
      'lbl_billing_continueWithPayPal',
      'lbl_billing_payPalLongText',
      'lbl_billing_cardEditUnSavedError',
      'lbl_billing_addCC',
      'lbl_billing_venmoLongText',
      'lbl_billing_diffrentcountry_1',
      'lbl_billing_diffrentcountry_2',
      'lbl_billing_diffrentcountry_3',
      'lbl_billing_afterpaySubmit',
      'lbl_billing_otherPaymentOptions',
      'lbl_afterPay_default_msg',
      'lbl_orderTotal_threshold_msg',
      'lbl_afterPay_gc_in_cart_msg',
    ];
    labelKeys.forEach((key) => {
      labels[key] = getLabelValue(billingLabel, key);
    });
    const {
      lbl_billing_title: header,
      lbl_billing_backLinkPickup: backLinkPickup,
      lbl_billing_backLinkShipping: backLinkShipping,
      lbl_billing_nextSubmit: nextSubmitText,
      lbl_billing_billingAddress: billingAddress,
      lbl_billing_sameAsShipping: sameAsShipping,
      lbl_billing_default_card: defaultCard,
      lbl_billing_addNewAddress: addNewAddress,
      lbl_billing_paymentMethodTitle: paymentMethod,
      lbl_billing_saveToAccount: saveToAccount,
      lbl_billing_defaultPayment: defaultPayment,
      lbl_billing_creditCard: creditCard,
      lbl_billing_creditCardEnd: creditCardEnd,
      lbl_billing_selectFromCard: selectFromCard,
      lbl_billing_addCreditHeading: addCreditHeading,
      lbl_billing_default: defaultBadge,
      lbl_billing_cardDetailsTitle: cardDetailsTitle,
      lbl_billing_editBtn: edit,
      lbl_billing_addCreditBtn: addCreditBtn,
      lbl_billing_paypal: paypal,
      lbl_billing_venmo: venmo,
      lbl_billing_afterpay: afterpay,
      lbl_billing_applepay: applePay,
      lbl_billing_selectCardTitle: selectCardTitle,
      lbl_billing_select: select,
      lbl_billing_cvvCode: cvvCode,
      lbl_billing_cardEditCancel: cancelButtonText,
      lbl_billing_cardEditSave: saveButtonText,
      lbl_billing_continueWith: continueWith,
      lbl_billing_continueWithPayPal: continueWithPayPal,
      lbl_billing_payPalLongText: payPalLongText,
      lbl_billing_cardEditUnSavedError: cardEditUnSavedError,
      lbl_billing_addCC: addCreditCard,
      lbl_billing_venmoLongText: venmoLongText,
      lbl_billing_diffrentcountry_1: diffrentCountry1,
      lbl_billing_diffrentcountry_2: diffrentCountry2,
      lbl_billing_diffrentcountry_3: diffrentCountry3,
      lbl_billing_afterpaySubmit: afterpaySubmit,
      lbl_billing_otherPaymentOptions: otherPaymentOptions,
      lbl_afterPay_default_msg: afterPayDefaultMessage,
      lbl_orderTotal_threshold_msg: orderTotalThresholdMessage,
      lbl_afterPay_gc_in_cart_msg: cartHasGCMessage,
    } = labels;
    return {
      header,
      backLinkShipping,
      backLinkPickup,
      nextSubmitText,
      billingAddress,
      sameAsShipping,
      defaultCard,
      addNewAddress,
      paymentMethod,
      saveButtonText,
      cardEditUnSavedError,
      cancelButtonText,
      saveToAccount,
      defaultPayment,
      creditCard,
      creditCardEnd,
      selectFromCard,
      addCreditHeading,
      defaultBadge,
      cardDetailsTitle,
      edit,
      addCreditBtn,
      paypal,
      venmo,
      applePay,
      selectCardTitle,
      select,
      cvvCode,
      continueWith,
      continueWithPayPal,
      payPalLongText,
      addCreditCard,
      venmoLongText,
      diffrentCountry1,
      diffrentCountry2,
      diffrentCountry3,
      afterpaySubmit,
      otherPaymentOptions,
      afterPayDefaultMessage,
      orderTotalThresholdMessage,
      cartHasGCMessage,
      afterpay,
    };
  }
);

const getCreditFieldLabelsObj = (state) =>
  state.Labels && state.Labels.global && state.Labels.global.creditCardFields;

const getCreditFieldLabels = createSelector(getCreditFieldLabelsObj, (creditFieldLabels) => {
  const labels = {};
  const labelKeys = [
    'lbl_creditField_cardNumber',
    'lbl_creditField_expMonth',
    'lbl_creditField_expYear',
    'lbl_creditField_cvvCode',
    'lbl_creditField_cameraText',
  ];
  labelKeys.forEach((key) => {
    labels[key] = getLabelValue(creditFieldLabels, key);
  });
  const {
    lbl_creditField_cardNumber: cardNumber,
    lbl_creditField_expMonth: expMonth,
    lbl_creditField_expYear: expYear,
    lbl_creditField_cvvCode: cvvCode,
    lbl_creditField_cameraText: cameraText,
  } = labels;
  return {
    cardNumber,
    expMonth,
    expYear,
    cvvCode,
    cameraText,
  };
});

const getSmsSignUpLabels = (state) => {
  const {
    lbl_smsSignup_smsSignupText: smsSignupText,
    lbl_smsSignup_privacyPolicy: privacyPolicy,
    lbl_smsSignup_orderUpdates: orderUpdates,
  } = state.Labels.global && state.Labels.global.smsSignup;
  return {
    smsSignupText,
    privacyPolicy,
    orderUpdates,
  };
};

const getCheckoutProgressBarLabels = (state) => {
  return {
    pickupLabel: getLabelValue(
      state.Labels,
      'lbl_checkoutheader_pickup',
      'checkoutHeader',
      'checkout'
    ),
    shippingLabel: getLabelValue(
      state.Labels,
      'lbl_checkoutHeader_shipping',
      'checkoutHeader',
      'checkout'
    ),
    billingLabel: getLabelValue(
      state.Labels,
      'lbl_checkoutHeader_billing',
      'checkoutHeader',
      'checkout'
    ),
    reviewLabel: getLabelValue(
      state.Labels,
      'lbl_checkoutHeader_review',
      'checkoutHeader',
      'checkout'
    ),
  };
};

const getShipmentMethods = (state) => {
  return state.Checkout.getIn(['options', 'shippingMethods']);
};

const getShipmentLoadingStatus = (state) => {
  return state.Checkout.getIn(['values', 'isShippingFormLoading']);
};

const getDefaultShipmentID = createSelector(
  [getShipmentMethods, getShippingDestinationValues],
  (shipmentMethods, shippingDestinationValues) => {
    let defaultMethod;
    if (shippingDestinationValues && shippingDestinationValues.method) {
      const {
        method: { shippingMethodId },
      } = shippingDestinationValues;
      if (shippingMethodId) {
        const defaultShipment = shipmentMethods.find((method) => method.id === shippingMethodId);
        defaultMethod = defaultShipment && defaultShipment.id;
        if (defaultMethod) {
          return defaultMethod;
        }
      }
    }
    defaultMethod = shipmentMethods.find((method) => method.isDefault === true);
    return (
      (defaultMethod && defaultMethod.id) || (shipmentMethods.length > 0 && shipmentMethods[0].id)
    );
  }
);

const getSelectedShippingMethodDetails = createSelector(
  [getDefaultShipmentID, getShipmentMethods],
  (shippingID, method) => {
    const selectedMethod = method.filter((item) => item.id === shippingID);
    return selectedMethod.length > 0 && selectedMethod[0];
  }
);

const getSelectedShippingMethodCode = createSelector(
  [getDefaultShipmentID, getShipmentMethods],
  (shippingID, method) => {
    const selectedMethod = method.filter((item) => item.id === shippingID);
    return selectedMethod.length > 0 && selectedMethod[0].code;
  }
);

const getAlternateFormFields = (state) => {
  const selector = formValueSelector('checkoutPickup');
  return selector(state, 'pickUpAlternate');
};

export const getAlternateFormFieldsExpress = (state) => {
  const selector = formValueSelector('expressReviewPage');
  return selector(state, 'pickUpAlternateExpress');
};

export const isPickupAlt = createSelector(
  getPickupAltValues,
  (pickUpAlternate) => pickUpAlternate && !!pickUpAlternate.firstName
);

const getLabels = (state) => state.Labels;

export const getAlternateFormUpdate = createSelector(
  getAlternateFormFields,
  (smsSignUpFields) => smsSignUpFields && smsSignUpFields.hasAlternatePickup
);

const getSmsSignUpFields = (state) => {
  const selector = formValueSelector('checkoutPickup');
  return selector(state, 'smsSignUp');
};

export const getSendOrderUpdate = createSelector(
  getSmsSignUpFields,
  (smsSignUpFields) => smsSignUpFields && smsSignUpFields.sendOrderUpdate
);

const getSmsNumberForOrderUpdates = (state) =>
  state.Checkout.getIn(['values', 'smsInfo', 'numberForUpdates']) ||
  state.Checkout.getIn(['values', 'orderUpdateViaMsg']);

function getPickupInitialPickupSectionValues(state) {
  // let userContactInfo = userStoreView.getUserContactInfo(state);
  // values (if any) entered previously in the checkout process,
  // or reported as checkout defaults by backend
  const pickUpAltValues = getPickupAltValues(state);
  const pickupValues = getPickupValues(state);
  const alternativeData = {
    ...{ hasAlternatePickup: isPickupAlt(state) },
    lastName_alt: pickUpAltValues.lastName,
    firstName_alt: pickUpAltValues.firstName,
    emailAddress_alt: pickUpAltValues.emailAddress,
  };
  const { emailSignUpTCP: emailSignUp, emailSignUpGYM } =
    BagPageSelectors.getIfEmailSignUpDone(state);
  return {
    pickUpContact: {
      firstName: pickupValues.firstName || getUserName(state),
      lastName: pickupValues.lastName || getUserLastName(state),
      emailAddress: pickupValues.emailAddress || getUserEmail(state),
      phoneNumber: pickupValues.phoneNumber || getUserPhoneNumber(state),
    },
    smsSignUp: {
      sendOrderUpdate: !!getSmsNumberForOrderUpdates(state),
      phoneNumber: pickupValues.phoneNumber || getUserPhoneNumber(state),
    },
    hasAlternatePickup: isPickupAlt(state),
    pickUpAlternate: isPickupAlt(state) ? alternativeData : {},
    emailSignUp: {
      emailSignUp,
      emailSignUpGYM,
    },
  };
}
export const getSmsInfoValues = createSelector(
  getCheckoutValuesState,
  (state) => state && state.get('smsInfo')
);
function getShippingInitialSectionValues(state) {
  const shippingValues = getSmsInfoValues(state);
  return {
    smsSignUp: {
      sendOrderUpdate: !!getSmsNumberForOrderUpdates(state),
      phoneNumber: shippingValues.numberForUpdates || getUserPhoneNumber(state),
    },
  };
}

/**
 * Get if Pickup has values in the redux state
 * @param {object} state
 * @returns {boolean}
 */
const isPickupHasValues = (state) => {
  const pickupValues = getPickupInitialPickupSectionValues(state);
  return pickupValues && pickupValues.pickUpContact && pickupValues.pickUpContact.firstName;
};

function getIsPaymentDisabled(state) {
  const orderDetails = state.CartPageReducer.get('orderDetails');
  if (orderDetails) {
    return orderDetails.get('grandTotal') <= orderDetails.get('giftCardsTotal');
  }
  return false;
}

function getIsCartHaveGiftCards(state) {
  const orderDetails = state.CartPageReducer.get('orderDetails');
  const orderItems = orderDetails && orderDetails.get('orderItems');
  return orderItems && orderItems.size
    ? orderItems.findIndex((item) => item.getIn(['productInfo', 'isGiftCard'])) !== -1
    : false;
}

function getAddressByKey(state, onFileAddressKey) {
  const addressList = getAddressListState(state);
  if (addressList) {
    return addressList.find((address) => address.nickName === onFileAddressKey);
  }
  return false;
}

function getBillingValues(state) {
  return state.Checkout.getIn(['values', 'billing']);
}

function getDetailedCreditCardById(state, id) {
  return JSON.parse(JSON.stringify(state.PaymentReducer.get('cardList'))).find(
    ({ creditCardId }) => (creditCardId && creditCardId.toString()) === id.toString()
  );
}

function isCardNotUpdated(state, cardId) {
  return getBillingValues(state).onFileCardId === cardId;
}

const getReviewLabels = (state) => {
  const getReviewLabelValue = (label) => getLabelValue(state.Labels, label, 'review', 'checkout');
  return {
    header: getReviewLabelValue('lbl_review_title'),
    backLinkBilling: getReviewLabelValue('lbl_review_backLinkBilling'),
    nextSubmitText: getReviewLabelValue('lbl_review_nextSubmit'),
    applyConditionPreText: getReviewLabelValue('lbl_review_applyConditionPreText'),
    applyConditionTermsText: getReviewLabelValue('lbl_review_applyConditionTermsText'),
    applyConditionAndText: getReviewLabelValue('lbl_review_applyConditionAndText'),
    applyConditionPolicyText: getReviewLabelValue('lbl_review_applyConditionPolicyText'),
    pickupSectionTitle: getReviewLabelValue('lbl_review_pickupSectionTitle'),
    shippingSectionTitle: getReviewLabelValue('lbl_review_shippingSectionTitle'),
    billingSectionTitle: getReviewLabelValue('lbl_review_billingSectionTitle'),
    ariaLabelReviewPageTitle: getReviewLabelValue('lbl_review_ariaLabelReviewPageTitle'),
    ariaLabelBackLink: getReviewLabelValue('lbl_review_ariaLabelBackLink'),
    labelAfterPaySubmit: getReviewLabelValue('lbl_review_afterpaySubmit'),
    labelAfterPayMessage: getReviewLabelValue('lbl_review_afterpayMessage'),
    labelAfterPayThresholdError: getReviewLabelValue('lbl_review_afterpayThresholdError'),
  };
};

const getGlobalLabels = (state) => {
  return state.Labels;
};

const getCurrentOrderId = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'orderId']);
};

const getSubOrderId = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'subOrderId']);
};

const getSmsNumberForBillingOrderUpdates = (state) =>
  state.Checkout.getIn(['values', 'smsInfo', 'numberForUpdates']);

const getCurrentCheckoutStage = (state) => state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'stage']);

const getPrevCheckoutStage = (state) => state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'prevStage']);

export const getEditShippingCheckoutStage = (state) =>
  state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'editShipping']);

export const getCouponRemovedFromOrderFlag = (state) =>
  state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'couponRemovedFromOrder']);

const isGiftOptionsEnabled = (state) => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.GIFTOPTION_ENABLED === '1'
  );
};

const getCheckoutServerError = (state) => {
  return state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'checkoutServerError']);
};

/**
 * This method is used to decide if we need to show review page next based on order conditions.
 */
const hasVenmoReviewPageRedirect = (state) => {
  const isVenmoInProgress = isVenmoPaymentInProgress(state);
  const isVenmoShippingDisplayed = isVenmoShippingBannerDisplayed(state);
  const orderHasShipping = getIsOrderHasShipping(state);
  const orderHasPickup = getIsOrderHasPickup(state);
  const hasPickupValues = isPickupHasValues(state);
  const addressList = getAddressListState(state);
  const hasShippingAddress = addressList && addressList.size > 0;
  let reviewPageRedirect = false;
  if (!isVenmoInProgress || isVenmoShippingDisplayed) {
    return reviewPageRedirect;
  }
  if (orderHasShipping && orderHasPickup) {
    // Mix Cart
    reviewPageRedirect = hasShippingAddress && hasPickupValues;
  } else if (orderHasShipping) {
    // Ship to Home Item
    reviewPageRedirect = hasShippingAddress;
  } else if (orderHasPickup) {
    // Boss Bopis scenario
    reviewPageRedirect = hasPickupValues;
  }
  return reviewPageRedirect;
};

const getGiftWrapOptions = (state) => {
  return state.Checkout.getIn(['options', 'giftWrapOptions']);
};

const getSelectedGiftWrapDetails = (state) => {
  const orderDetails = state.CartPageReducer.get('orderDetails');
  const checkout = orderDetails.get('checkout');
  const giftWrapName = checkout && checkout.getIn(['giftWrap', 'name']);
  const optionId = checkout && checkout.getIn(['giftWrap', 'optionId']);
  const selectedOptionData = getGiftWrapOptions(state);
  if (selectedOptionData.body) {
    const selectedOption = selectedOptionData.body.giftOptions.filter(
      (option) => option.catEntryId === optionId
    );
    if (selectedOption.length === 1) return selectedOption[0];
  }

  return { name: giftWrapName };
};

/**
 * @function getInternationalCheckoutApiUrl
 * @description this selector gives borderFree url for iframe
 */
function getInternationalCheckoutApiUrl() {
  return getAPIConfig().borderFree;
}
/**
 * @function getInternationalCheckoutCommUrl
 * @description this selector gives borderFreeComm url for iframe
 */
function getInternationalCheckoutCommUrl() {
  return getAPIConfig().borderFreeComm;
}

/**
 *
 * @function getInternationalCheckoutUrl
 * @param {*} state
 * @description this selector gives international url from state.
 */
function getInternationalCheckoutUrl(state) {
  return state.Checkout.getIn(['options', 'internationalUrl']);
}

/**
 * @function getIsVenmoEnabled
 * @description - Venmo Kill Switch Selector
 * @param {object} state
 * @returns {bool}
 */
const getIsVenmoEnabled = (state) => {
  // Kill switch Handling for mobile app
  if (isMobileApp()) {
    // return (
    //   state &&
    //   state[SESSIONCONFIG_REDUCER_KEY] &&
    //   parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.VENMO_APP_ENABLED)
    // );

    return false;
  }
  // Kill switch for mobile web, also checking if on mobile web viewport
  return (
    getIsMobile() &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.VENMO_ENABLED)
  );
};

const getApplePayKillSwitchApp = (state) =>
  state &&
  state[SESSIONCONFIG_REDUCER_KEY] &&
  parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.APPLE_PAY_PAYMENT_APP_ENABLED);

/**
 * @function isApplePayEnabledInXapp
 * @description - Apple pay xapp config selector
 * @param {object} state
 * @returns {bool}
 */
export const isApplePayEnabledInXapp = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.APPLE_PAY_PAYMENT_ENABLED)
  );
};

/**
 * @function eligibilityForApplePay
 * @description - Apple pay rendering eligibility based on device and xapp config
 * @param {object} state
 * @returns {bool}
 */
export const eligibilityForApplePay = (state) =>
  isSafariBrowser() && isApplePayEnabledInXapp(state);

/**
 * @function getIsApplePayEnabled
 * @description - Apple Kill Switch Selector
 * @param {object} state
 * @returns {bool}
 */
export const getIsApplePayEnabled = (state) => {
  if (isMobileApp() && isIOS()) {
    return getApplePayKillSwitchApp(state);
  }

  if (isCanada() || getIsInternationalShipping(state)) return false;

  if (eligibilityForApplePay(state)) {
    return parseBoolean(getAPIConfig().enableApplePay);
  }

  return false;
};

const getApplePayEnabledWithCard = (state) =>
  state &&
  state[SESSIONCONFIG_REDUCER_KEY] &&
  parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.APPLE_PAY_WITH_CARD_ENABLED);

/**
 * @function getIsApplePayRememberedUser
 * @description - Apple Kill Switch for Remembered User Selector
 * @param {object} state
 * @returns {bool}
 */
const getIsApplePayRememberedUser = (state) => {
  // Kill switch Handling for apple pay remembered user
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.APPLE_PAY_ENABLED_REMEMBERED_USER)
  );
};

const getIsAppleSBPReDesignEnabled = (state) => {
  // Kill switch Handling for SBP Overlay Redesign
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.ENABLE_SBP_OVERLAY_REDESIGN)
  );
};
/**
 * @function getIsApplePayEnabledOnBilling
 * @description - Apple Kill Switch Selector
 * @param {object} state
 * @returns {bool}
 */
const getIsApplePayEnabledOnBilling = (state) => {
  if (isMobileApp() && isIOS()) {
    return (
      state &&
      state[SESSIONCONFIG_REDUCER_KEY] &&
      parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.APPLE_PAY_BILLPG_APP_ENABLED)
    );
  }

  if (eligibilityForApplePay(state)) {
    return parseBoolean(getAPIConfig().enableApplePayOnBilling);
  }

  return false;
};

const getCurrentLanguage = (state) => {
  return (
    (state[SESSIONCONFIG_REDUCER_KEY] &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
      state[SESSIONCONFIG_REDUCER_KEY].siteDetails.language) ||
    constants.DEFAULT_LANGUAGE
  );
};

const getReviewPageLabels = (state) =>
  state.Labels && state.Labels.checkout && state.Labels.checkout.review;

/**
 * @function getPickupSectionLabels
 * @param {Object} state
 * @description This selector provides the state of the review page labels.
 * @returns {Object}
 */
const getPickupSectionLabels = createSelector(getReviewPageLabels, (reviewLabels) => {
  const labels = {};
  const labelKeys = [
    'lbl_review_pickupSectionTitle',
    'lbl_review_sectionAnchor',
    'lbl_review_sectionPickupText',
    'lbl_review_sectionPickupItem',
    'lbl_review_sectionPickupItems',
    'lbl_review_sectionPickupToday',
    'lbl_review_sectionPickupAlternateHeading',
    'lbl_review_sectionPickupOrderTitle',
  ];
  labelKeys.forEach((key) => {
    labels[key] = getLabelValue(reviewLabels, key);
  });
  return labels;
});

const getEmailSignUpLabels = (state) => {
  const labels = {};
  const labelKeys = [
    'lbl_shipping_emailSignUpHeader',
    'lbl_shipping_emailSignUpSubHeader',
    'lbl_shipping_childrenPlaceCheckoutTxt',
    'lbl_shipping_emailSignUpDisclaimer',
    'lbl_shipping_gymboreePlaceCheckoutTxt',
    'lbl_shipping_addressEditError',
  ];
  labelKeys.forEach((key) => {
    labels[key] = getLabelValue(state.Labels, key, 'shipping', 'checkout');
  });
  return labels;
};

/**
 * @function getShippingSectionLabels
 * @param {Object} state
 * @description This selector provides the state of the review page labels.
 * @returns {Object}
 */
const getShippingSectionLabels = createSelector(getReviewPageLabels, (reviewLabels) => {
  const labels = {};
  const labelKeys = [
    'lbl_review_shippingSectionTitle',
    'lbl_review_sectionAnchor',
    'lbl_review_sectionShippingHeading',
    'lbl_review_sectionShippingAddressTitle',
    'lbl_review_sectionShippingMethodTitle',
    'lbl_review_sectionShippingGiftServiceTitle',
    'lbl_review_sectionShippingGiftServiceDefault',
  ];
  labelKeys.forEach((key) => {
    labels[key] = getLabelValue(reviewLabels, key);
  });
  return labels;
});

const getPayPalSettings = (state) => {
  return state.Checkout.getIn(['options', 'paypalPaymentSettings']) || null;
};

const getShippingAddressList = createSelector(
  [getAddressListState, getCurrentSiteId],
  (userAddresses, country) => {
    let addresses = List();
    const countryName = (country && country.toUpperCase()) || 'US';
    if (userAddresses && userAddresses.size > 0) {
      addresses = userAddresses.filter(
        (address) => address.country === countryName && address.xcont_isShippingAddress === 'true'
      );
      if (addresses.size) {
        return addresses;
      }
    }
    return addresses;
  }
);

const getShippingPhoneNo = (state) => {
  const shippingFormNumber = getAddressPhoneNo(state);
  const addressList = getShippingAddressList(state);
  const selectedAddressKey = getOnFileAddressKey(state);
  const selectedAddress = addressList.filter((address) => {
    return address.addressId === selectedAddressKey;
  });
  const selectedAddressPhoneNo =
    selectedAddress && selectedAddress.size > 0 && selectedAddress.getIn(['0', 'phone1']);
  const { phoneNumber } = getShippingPhoneAndEmail(state);
  return selectedAddressPhoneNo || shippingFormNumber || phoneNumber;
};

const getIsBillingVisited = createSelector(getCheckoutUiFlagState, (uiFlags) => {
  return uiFlags && uiFlags.get('isBillingVisited');
});

const isCheckoutFocused = createSelector(getCheckoutUiFlagState, (uiFlags) => {
  return uiFlags && uiFlags.get('checkoutPageFocus');
});

function getVenmoUserEmail(state) {
  const { emailAddress: shippingEmail } = getShippingPhoneAndEmail(state);
  const { emailAddress: pickupEmail } = getPickupValues(state);
  return getUserEmail(state) || shippingEmail || pickupEmail;
}

const getCheckoutPageEmptyBagLabels = createSelector(getReviewPageLabels, (reviewLabels) => {
  const labels = {};
  const labelKeys = [
    { keyLabel: 'emptyBagText', key: 'lbl_review_emptyBagText' },
    { keyLabel: 'emptyBagSubText', key: 'lbl_review_emptyBagSubText' },
  ];
  labelKeys.forEach(({ key, keyLabel }) => {
    labels[keyLabel] = getLabelValue(reviewLabels, key);
  });
  return labels;
});

const getIsRtpsFlow = createSelector(
  getCheckoutUiFlagState,
  (uiFlags) => uiFlags && uiFlags.get('isRTPSFlow')
);

const getIsRTPSEnabled = (state) =>
  state[SESSIONCONFIG_REDUCER_KEY] &&
  state[SESSIONCONFIG_REDUCER_KEY].siteDetails.ADS_OLPS_ENABLED === 'TRUE';

const hasPLCCOrRTPSEnabled = createSelector(
  [getplccCardNumber, getIsRTPSEnabled, isPlccUser],
  (plccCardNumber, isRTPSEnabled, isPLCCUser) => {
    const hasPLCC = isPLCCUser || plccCardNumber;
    return !hasPLCC && isRTPSEnabled;
  }
);

const getShowRTPSOnBilling = createSelector(
  [getIsOrderHasShipping, getGiftWrappingValues, hasPLCCOrRTPSEnabled],
  (isOrderHasShipping, giftWrappingValues, rtpsEnabled) => {
    const { hasGiftWrapping } = giftWrappingValues;
    return isOrderHasShipping && !hasGiftWrapping && rtpsEnabled;
  }
);

const getshowRTPSOnReview = createSelector(hasPLCCOrRTPSEnabled, (rtpsEnabled) => {
  return rtpsEnabled;
});

const getAccessibilityLabels = (state) => {
  return {
    infoIconText: getLabelValue(state.Labels, 'lbl_info_icon', 'accessibility', 'global'),
    venmoIconText: getLabelValue(state.Labels, 'lbl_venmo_icon', 'accessibility', 'global'),
    pickupIconText: getLabelValue(state.Labels, 'lbl_pickup_icon', 'accessibility', 'global'),
    venmoCancel: getLabelValue(state.Labels, 'lbl_bagPage_cancel', 'bagPage', 'checkout'),
    venmoProceed: getLabelValue(state.Labels, 'lbl_venmo_proceed_checkout', 'bagPage', 'checkout'),
  };
};

export const getPageData = (state) => {
  return state.pageData;
};

export const getMaxGiftCards = (state) => {
  return (
    state.session && state.session.siteDetails && (state.session.siteDetails.MAX_GIFTCARD || 5)
  );
};

export const isSfsInvMessagingEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.SFS_INVENTORY_CHECK_FOR_DELIVERY_ENABLED)
  );
};

export const getIsOptimiseBillingInfoEnabled = (state) =>
  state[SESSIONCONFIG_REDUCER_KEY] &&
  state[SESSIONCONFIG_REDUCER_KEY].siteDetails.OPTIMISE_BILLING_INFORMATION_ENABLED === '1';

/**
 * @description - validate if gift services option is selected or change, and based on that set recalculate flag.
 * @param {object} state
 */
const getRecalcValueFromGiftServices = (state) => {
  const giftServicesFormData = getGiftServicesFormData(state);
  const giftWrappingValues = getGiftWrappingValues(state);
  let recalc = false;
  const initialGiftWrappingVal = giftWrappingValues.hasGiftWrapping;
  const giftWrappingStoreOptionID = giftWrappingValues.optionId;
  // // If the giftwrapping option differs from the initial state
  // // Recalculate true needs to be sent as true
  if (
    initialGiftWrappingVal !== giftServicesFormData.hasGiftWrapping ||
    (giftWrappingStoreOptionID && giftServicesFormData.catEntryId !== giftWrappingStoreOptionID)
  ) {
    recalc = true;
  }
  return recalc;
};

const getSMSOrderUpdatesId = (state) => {
  let smsOrderUpdatesID;
  if (state.Labels.global.smsSignup && Array.isArray(state.Labels.global.smsSignup.referred)) {
    smsOrderUpdatesID =
      state.Labels.global.smsSignup.referred.length &&
      state.Labels.global.smsSignup.referred.find((label) => label.name === 'sms_text_terms');
  }
  return smsOrderUpdatesID;
};

const getSMSOrderUpdatesRichTextSelector = (state) => {
  const rContent = state.CartPageReducer.get('moduleXContent').find(
    (moduleX) => moduleX.name === (getSMSOrderUpdatesId(state) && getSMSOrderUpdatesId(state).name)
  );
  return rContent && rContent.richText;
};

const getSubmitOrderContentId = (state) => {
  let submitOrderContentID;

  if (state.Labels.checkout.review && Array.isArray(state.Labels.checkout.review.referred)) {
    submitOrderContentID =
      state.Labels.checkout.review.referred.length &&
      state.Labels.checkout.review.referred.find((label) => label.name === 'submit_order_policy');
  }
  return submitOrderContentID;
};

const getSubmitOrderRichTextSelector = (state) => {
  const rContent = state.CartPageReducer.get('moduleXContent').find(
    (moduleX) =>
      moduleX.name === (getSubmitOrderContentId(state) && getSubmitOrderContentId(state).name)
  );
  return rContent && rContent.richText;
};

const getEmailSignUpCaId = (state) => {
  let emailSignUpCaId;
  if (state.Labels.checkout.shipping && Array.isArray(state.Labels.checkout.shipping.referred)) {
    emailSignUpCaId = state.Labels.checkout.shipping.referred.find(
      (label) => label.name.toLowerCase() === 'email_sign_up_ca'
    );
  }
  return emailSignUpCaId;
};

const getEmailSignUpCaRichTextSelector = (state) => {
  const rContent = state.CartPageReducer.get('moduleXContent').find(
    (moduleX) => moduleX.name === (getEmailSignUpCaId(state) && getEmailSignUpCaId(state).name)
  );
  return rContent && rContent.richText;
};

export const getABTestForZipCode = ({ AbTest }) => {
  const AbTests = getABtestFromState(AbTest);
  return AbTests && AbTests.zipCodeABTestEnabled;
};

export const getZipCodeLoader = ({ Checkout }) => {
  return Checkout && Checkout.getIn(['shipping', 'zipCodeLoader']);
};

const getSaveCardDefaultChecked = (state) => {
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.SAVE_CARD_DEFAULT_CHECKED)
  );
};

const getSaveCardToAccountUserChoice = (state) => {
  return state[CHECKOUT_REDUCER_KEY].getIn(['uiFlags', 'saveCardToAccountUserChoice']);
};

const getIsSaveCardDefaultChecked = createSelector(
  [getSaveCardDefaultChecked, getSaveCardToAccountUserChoice],
  (saveCardDefaultCheckedValue, saveCardToAccountUserChoice) => {
    let shouldCardAddCheck = saveCardDefaultCheckedValue;
    if (saveCardToAccountUserChoice === constants.SAVE_CARD_CHOICE_VALUES.SAVE) {
      shouldCardAddCheck = true;
    } else if (saveCardToAccountUserChoice === constants.SAVE_CARD_CHOICE_VALUES.DO_NOT_SAVE) {
      shouldCardAddCheck = false;
    }
    return shouldCardAddCheck;
  }
);

export const getShippingLoaderState = (state) => {
  return state.PageLoader && state.PageLoader.shippingLoader;
};
export default {
  getIsOrderHasShipping,
  getShippingDestinationValues,
  getDefaultAddress,
  isGuest,
  getIsMobile,
  getPickupInitialPickupSectionValues,
  isSmsUpdatesEnabled,
  getCurrentPickupFormNumber,
  isUsSite,
  getUserContactInfo,
  getPickupAltValues,
  getCurrentSiteId,
  getIsSmsUpdatesEnabled,
  getSmsSignUpFields,
  getShipmentMethodsFields,
  getSelectedShipmentId,
  getSendOrderUpdate,
  getAddressFields,
  getBillingAddressFields,
  getAddressPhoneNo,
  getSmsSignUpLabels,
  getIsOrderHasPickup,
  getEmailSignUpLabels,
  getGiftWrappingValues,
  getSmsNumberForOrderUpdates,
  getShipmentMethods,
  getDefaultShipmentID,
  getShippingSendOrderUpdate,
  getAlternateFormUpdate,
  getPickUpContactFormLabels,
  getUserEmail,
  getSaveToAddressBook,
  getOnFileAddressKey,
  getShippingSmsSignUpFields,
  getShippingAddressID,
  getDefaultShipping,
  getAddEditResponseAddressId,
  getBillingLabels,
  getLabels,
  getShippingAddress,
  getIsPaymentDisabled,
  getShipmentLoadingStatus,
  getBillingValues,
  getAddressByKey,
  isCardNotUpdated,
  getDetailedCreditCardById,
  getCheckoutProgressBarLabels,
  getSyncError,
  getGiftServicesFormData,
  getGiftServicesSend,
  getReviewLabels,
  getPickupSectionLabels,
  getShippingSectionLabels,
  isGiftOptionsEnabled,
  getPaypalPaymentSettings,
  getSelectedGiftWrapDetails,
  getSelectedShippingMethodDetails,
  getCurrentOrderId,
  getSubOrderId,
  getSmsNumberForBillingOrderUpdates,
  getVenmoData,
  getVenmoClientTokenData,
  getAppleClientTokenData,
  getApplePayEligibleShippingState,
  isVenmoPaymentAvailable,
  isVenmoNonceActive,
  isVenmoNonceNotExpired,
  isVenmoPaymentInProgress,
  isApplePaymentInProgress,
  isVenmoPaymentToken,
  getInternationalCheckoutCommUrl,
  getInternationalCheckoutApiUrl,
  getInternationalCheckoutUrl,
  getIsVenmoEnabled,
  getIsApplePayEnabled,
  getIsApplePayEnabledOnBilling,
  getIsApplePayRememberedUser,
  getIsAppleSBPReDesignEnabled,
  getPayPalSettings,
  getCurrentLanguage,
  isVenmoShippingBannerDisplayed,
  isVenmoPickupBannerDisplayed,
  isVenmoPaymentSaveSelected,
  hasVenmoReviewPageRedirect,
  getShippingPhoneAndEmail,
  getCreditFieldLabels,
  isPickupHasValues,
  getVenmoUserName,
  getCheckoutServerError,
  getCurrentCheckoutStage,
  getExpressReviewShippingSectionId,
  getPrevCheckoutStage,
  getShippingAddressList,
  getIsBillingVisited,
  getIsRtpsFlow,
  getVenmoUserEmail,
  getVenmoError,
  getPickupValues,
  getCheckoutPageEmptyBagLabels,
  getCardType,
  getShippingPhoneNo,
  getIsRTPSEnabled,
  getIfCheckoutRoutingDone,
  getIfRedirectFromExpress,
  getIfInitCheckoutFinished,
  getShowRTPSOnBilling,
  getshowRTPSOnReview,
  getVenmoPayment,
  isVenmoOrderPayment,
  getVenmoOrderUserId,
  getShippingInitialSectionValues,
  isVenmoAppInstalled,
  getDefaultShipmentMethods,
  getAccessibilityLabels,
  getIsOptimseUpdateShipment,
  getIsAfterPayInProgress,
  getReviewFormValues,
  getIsOptimiseBillingInfoEnabled,
  getRecalcValueFromGiftServices,
  getSMSOrderUpdatesRichTextSelector,
  getSMSOrderUpdatesId,
  getSubmitOrderContentId,
  getSubmitOrderRichTextSelector,
  getEmailSignUpCaId,
  getEmailSignUpCaRichTextSelector,
  isCheckoutFocused,
  getSelectedShippingMethodCode,
  getApplePayAppDataApp,
  isSfsInvMessagingEnabled,
  getGlobalLabels,
  getIsSaveCardDefaultChecked,
  getApplePayEnabledWithCard,
  getSaveCardToAccountUserChoice,
  getIsCartHaveGiftCards,
};
