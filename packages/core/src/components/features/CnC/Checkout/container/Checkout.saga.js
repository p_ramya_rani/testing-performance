/* eslint-disable max-lines */
/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, all, select, takeEvery, takeLeading } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { formValueSelector, change } from 'redux-form';
import { verifyAddressData } from '@tcp/core/src/services/abstractors/account/AddressVerification';
import { trackFormError } from '@tcp/core/src/analytics/actions';
import setLoaderState, {
  setSectionLoaderState,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import {
  getEddData,
  resetEddData,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.actions';

import { verifyAddress } from '../../../../common/organisms/AddressVerification/container/AddressVerification.actions';
import CONSTANTS, { CHECKOUT_ROUTES } from '../Checkout.constants';
import {
  getShippingMethods,
  setShippingMethodAndAddressId,
  getInternationCheckoutSettings,
  startExpressCheckout,
  startEddPersist,
} from '../../../../../services/abstractors/CnC/index';
import selectors, { isGuest, isExpressCheckout } from './Checkout.selector';
import utility from '../util/utility';
import CHECKOUT_ACTIONS, {
  getSetPickupValuesActn,
  getSetPickupAltValuesActn,
  getSetShippingValuesActn,
  getSetBillingValuesActn,
  setIsLoadingShippingMethods,
  setShippingOptions,
  setAddressError,
  getSetCheckoutStage,
  toggleZipCodeLoader,
  fetchDefaultShipmentMethods,
  setInitCheckoutFinished,
} from './Checkout.action';
import { getCartDataSaga } from '../../BagPage/container/BagPage.saga';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import { getAddressList } from '../../../account/AddressBook/container/AddressBook.saga';
import { getCardList } from '../../../account/Payment/container/Payment.saga';
import { isMobileApp, isCanada } from '../../../../../utils';
import {
  updateShipmentMethodSelection,
  updateShippingAddress,
  addNewShippingAddress,
  addRegisteredUserAddress,
  routeToPickupPage,
  addAndSetGiftWrappingOptions,
  getVenmoClientTokenSaga,
  getAppleClientTokenSaga,
  saveLocalSmsInfo,
  addOrEditGuestUserAddress,
  callPickupSubmitMethod,
  callUpdateRTPS,
  handleServerSideErrorAPI,
  submitAcceptOrDeclinePlccData,
  handleCheckoutInitRouting,
  makeUpdateRTPSCall,
  shouldInvokeReviewCartCall,
  redirectFromExpress,
  getGiftWrapOptionsData,
  getShippingEventType,
} from './Checkout.saga.util';
import BAG_PAGE_ACTIONS from '../../BagPage/container/BagPage.actions';
import { submitEmailSignup, pickUpRouting } from './CheckoutExtended.saga.util';
import submitBilling, {
  updateCardDetails,
  submitVenmoBilling,
  submitAppleBilling,
  submitAfterPayOnBilling,
  checkoutWithAfterPay,
} from './CheckoutBilling.saga';
import submitOrderForProcessing from './CheckoutReview.saga';
import { submitVerifiedAddressData, submitShippingSectionData } from './CheckoutShipping.saga';
import { getIsInternationalShipping } from '../../../../../reduxStore/selectors/session.selectors';
import { getAddressListState } from '../../../account/AddressBook/container/AddressBook.selectors';
import { isRememberedUser } from '../../../account/User/container/User.selectors';
import { openOverlayModal } from '../../../account/OverlayModal/container/OverlayModal.actions';

const {
  getIsOrderHasShipping,
  getShippingDestinationValues,
  getGiftServicesFormData,
  getShipmentMethods,
  getCurrentCheckoutStage,
  getPrevCheckoutStage,
  getIfRedirectFromExpress,
  getShippingAddressList,
  getBillingValues,
} = selectors;
const { hasPOBox } = utility;
let oldHasPOB = {};

function* storeUpdatedCheckoutValues(res /* isCartNotRequired, updateSmsInfo = true */) {
  // setCartInfo(cartInfo, isSetCartItems, shouldExportActions)
  const resCheckoutValues = res.payload.res.orderDetails.checkout;
  const actions = [
    resCheckoutValues.pickUpContact && getSetPickupValuesActn(resCheckoutValues.pickUpContact),
    resCheckoutValues.pickUpAlternative &&
      getSetPickupAltValuesActn(resCheckoutValues.pickUpAlternative),
    resCheckoutValues.shipping && getSetShippingValuesActn(resCheckoutValues.shipping),
    // resCheckoutValues.smsInfo && updateSmsInfo &&
    //   setSmsNumberForUpdates(resCheckoutValues.smsInfo.numberForUpdates),
    resCheckoutValues.giftWrap &&
      CHECKOUT_ACTIONS.getSetGiftWrapValuesActn({
        hasGiftWrapping: !!resCheckoutValues.giftWrap.optionId,
        ...resCheckoutValues.giftWrap,
      }),
    resCheckoutValues.billing && getSetBillingValuesActn(resCheckoutValues.billing),
  ];
  yield all(actions.map((action) => put(action)));
  // if (checkoutStoreView.isExpressCheckout(this.store.getState()) && resCheckoutValues.billing && resCheckoutValues.billing.paymentMethod === 'venmo') {
  //   // We have
  //   this.store.dispatch(setVenmoPaymentInProgress(true));
  // }
}

function* submitPickupSection({ payload }) {
  try {
    const formData = { ...payload };
    const { navigation } = payload;
    const isGuestUser = yield select(isGuest);
    const getIsShippingRequired = yield select(getIsOrderHasShipping);
    if (formData.pickUpContact.smsInfo) yield saveLocalSmsInfo(formData.pickUpContact.smsInfo);
    yield put(setLoaderState(true));
    yield submitEmailSignup(formData.pickUpContact.emailAddress, formData);
    const result = yield call(callPickupSubmitMethod, formData);

    if (result.addressId) {
      if (!isGuestUser && !getIsShippingRequired) {
        yield call(getCardList, { ignoreCache: true });
      }
      if (!isMobileApp()) {
        const isVenmoInProgress = yield select(selectors.isVenmoPaymentInProgress);
        const isVenmoPickupDisplayed = yield select(selectors.isVenmoPickupBannerDisplayed);
        pickUpRouting({ getIsShippingRequired, isVenmoInProgress, isVenmoPickupDisplayed });
      } else if (navigation) {
        if (getIsShippingRequired) {
          yield put(getSetCheckoutStage(CONSTANTS.SHIPPING_DEFAULT_PARAM));
        } else yield put(getSetCheckoutStage(CONSTANTS.BILLING_DEFAULT_PARAM));
      }
    }
    /* In the future I imagine us sending the SMS to backend for them to
        store so it will be loaded in the below loadUpdatedCheckoutValues function.
        for now we are storing it only on browser so will lose this info on page re-load.
      */
    // eslint-disable-next-line no-unused-expressions
    // return getCheckoutOperator(this.store).loadUpdatedCheckoutValues(false, true, true, false, !wantsSmsOrderUpdates);
    // }).catch((err) => {
    //   throw getSubmissionError(this.store, 'submitPickupSection', err);
    // });
    yield put(setLoaderState(false));
  } catch (e) {
    const { pickUpContact } = payload;
    const { firstName, phoneNumber, emailAddress, lastName, smsInfo } = pickUpContact;
    const { smsUpdateNumber } = smsInfo;
    const requiredList3Val = { smsUpdateNumber, firstName, phoneNumber, emailAddress, lastName };
    yield put(trackFormError({ formName: 'checkoutPickup', formData: requiredList3Val }));

    yield put(setLoaderState(false));

    yield call(handleServerSideErrorAPI, e);
  }
}
// function setCartInfo(cartInfo, isSetCartItems) {
//   return updateCartInfo(cartInfo, isSetCartItems);
// }
function* loadShipmentMethods(miniAddress, loadPageEvent = false) {
  let address;
  let pageEvent = loadPageEvent;
  if (miniAddress.formName) {
    const addressSelector = formValueSelector(miniAddress.formName);
    const addressValues = yield select(addressSelector, 'address');
    address = {
      ...addressValues,
      zipCode: miniAddress.zipCode || addressValues.zipCode,
      addressField1: miniAddress.addressField1 || addressValues.addressField1,
      state: miniAddress.state || addressValues.state,
    };
  } else if (miniAddress.isCancelClicked) {
    const shippingAddress = yield select(getShippingDestinationValues);
    ({ address } = shippingAddress);
  } else {
    address = miniAddress;
  }

  if (miniAddress.payload && miniAddress.payload.pageEvent) {
    const { pageEvent: event } = miniAddress.payload;
    pageEvent = event;
  }
  try {
    const labels = yield select(BagPageSelectors.getErrorMapping);
    yield put(setIsLoadingShippingMethods(true));
    const isShipmentMethodCached = yield select(selectors.getDefaultShipmentMethods);
    const userAddresses = yield select(getAddressListState);
    const storedShippingAddress = yield select(getShippingDestinationValues);
    const shipmentAddress = address || storedShippingAddress.address;
    const eventType = getShippingEventType(
      pageEvent,
      isShipmentMethodCached,
      userAddresses,
      shipmentAddress
    );
    // Backend for Canada needs ZipCode and addressField1 in headers for change shipment.
    const res = yield getShippingMethods(address, labels, eventType);
    yield all([setShippingOptions(res), setIsLoadingShippingMethods(false)].map((val) => put(val)));
  } catch (err) {
    yield put(setIsLoadingShippingMethods(false));
  }
}

function* validDateAndLoadShipmentMethods(miniAddress, changhedFlags) {
  // Note: this convoluted logic is due to BE. If address lines do not contain a pobox
  // then in the US we should only respond to state changes, and in Canada only to
  // zipcode changes. And we also have to react to any change if switching from having
  // to not having a pobox, or a change in address lines if currently has a pobox
  const newHasPOB = hasPOBox(miniAddress.addressLine1, miniAddress.addressLine2);
  if (
    !(
      oldHasPOB !== newHasPOB ||
      (newHasPOB && (changhedFlags.addressLine1 || changhedFlags.addressLine2))
    ) &&
    // zipCode changed, but not state
    ((miniAddress.country === 'US' && !changhedFlags.state) ||
      // state changed, but not zipCode
      (miniAddress.country === 'CA' && !changhedFlags.zipCode))
  ) {
    // shipping methods are the same as we already have
    return yield;
  }
  oldHasPOB = newHasPOB;
  return yield loadShipmentMethods(miniAddress, true);
}

const makeShipmentMethodsCall = ({ shipmentMethods, makeCartApiCall }) =>
  makeCartApiCall || !shipmentMethods.length;

const getShipmentAddress = (shippingAddress = {}, defaultShipping = {}) => {
  const { addressLine1, addressLine2, state, zipCode, country } = shippingAddress;
  const {
    addressLine = [],
    country: defaultCountry = '',
    state: defaultState = '',
    zipCode: defaultZipCode = '',
  } = defaultShipping;
  return {
    addressField1: addressLine1 || addressLine[0],
    addressField2: addressLine2 || addressLine[1],
    country: country || defaultCountry,
    state: state || defaultState,
    zipCode: zipCode || defaultZipCode,
  };
};

function* initShippingData(pageName, makeCartApiCall) {
  const shipmentMethods = yield select(getShipmentMethods);
  if (pageName === CONSTANTS.CHECKOUT_STAGES.SHIPPING) {
    yield call(getGiftWrapOptionsData);
    if (makeShipmentMethodsCall({ shipmentMethods, makeCartApiCall })) {
      let shippingAddress = yield select(getShippingDestinationValues);
      shippingAddress = shippingAddress.address;

      const hasShipping =
        shippingAddress &&
        shippingAddress.country &&
        shippingAddress.state &&
        shippingAddress.zipCode;
      const isGuestUser = yield select(isGuest);
      if (isGuestUser || !hasShipping || !shipmentMethods.size) {
        const shippingAddressBook = yield select(getShippingAddressList);
        const defaultShipping = shippingAddressBook.find(
          (address) => address && address.primary === 'true'
        );
        const address = getShipmentAddress(shippingAddress, defaultShipping);
        yield call(validDateAndLoadShipmentMethods, address, { state: true, zipCode: true });
      }
    }
  }
}

function* triggerInternationalCheckoutIfRequired() {
  const isInternationalShipping = yield select(getIsInternationalShipping);
  if (isInternationalShipping && !isMobileApp()) {
    return utility.routeToPage(CHECKOUT_ROUTES.internationalCheckout);
  }
  return null;
}

function* getPaypayExpressDataIfRequired({ pageName, isPaypalPostBack, isNonExpressCheckout }) {
  const { REVIEW } = CONSTANTS.CHECKOUT_STAGES;
  if (isNonExpressCheckout && isPaypalPostBack && pageName === REVIEW) {
    const shippingValues = yield select(getShippingDestinationValues);
    const shippingAddress = (shippingValues && shippingValues.address) || {};
    yield validDateAndLoadShipmentMethods(
      {
        country: shippingAddress.country || '',
        state: shippingAddress.state || '',
        zipCode: shippingAddress.zipCode || '',
      },
      { state: true, zipCode: true }
    );
  }
}

const checkCartApiForMobileApp = (prevStage, currentStage) => {
  return isMobileApp() && prevStage && currentStage;
};

export function* validateAdress(
  isPaypalPostBack,
  shippingValues,
  isExpressCheckoutEnabled,
  pageName
) {
  const checkoutDetails = yield select(getBillingValues);
  const { billing } = checkoutDetails || {};
  const { cardType } = billing || {};
  const applePayOnReview =
    cardType === CONSTANTS.APPLEPAY_LABEL && pageName === CONSTANTS.CHECKOUT_STAGES.REVIEW;
  if (shippingValues && (isExpressCheckoutEnabled || isPaypalPostBack || applePayOnReview)) {
    const shippingAddress = shippingValues.address || {};
    const {
      addressLine1: address1,
      addressLine2: address2,
      zipCode: zip,
      state,
      city,
      country,
      firstName,
      lastName,
    } = shippingAddress;
    return yield put(
      verifyAddress({
        address1,
        address2,
        zip,
        state,
        city,
        country,
        firstName,
        lastName,
        doNotShowLoader: pageName === CONSTANTS.CHECKOUT_STAGES.REVIEW,
      })
    );
  }
  return false;
}

function* initCheckoutSectionData({ payload }) {
  const {
    recalc,
    pageName,
    isPaypalPostBack,
    appRouting,
    navigation,
    isServer,
    fromATB,
    isGetCoupon,
    plccUser = false,
  } = payload;
  yield call(triggerInternationalCheckoutIfRequired);
  const isExpressCheckoutEnabled = yield select(isExpressCheckout);
  const currentStage = yield select(getCurrentCheckoutStage);
  const prevStage = yield select(getPrevCheckoutStage);
  const ifRedirectFromExpress = yield select(getIfRedirectFromExpress);
  const cartItems = yield select(BagPageSelectors.getOrderItems);
  const availableStages = utility.getAvailableStages(cartItems);
  const { PICKUP, SHIPPING, BILLING } = CONSTANTS.CHECKOUT_STAGES;
  const pendingPromises = [];
  const isNonExpressCheckout = shouldInvokeReviewCartCall(isExpressCheckoutEnabled, payload);
  let makeCartApiCall = false;
  if (pageName === PICKUP || pageName === BILLING || pageName === SHIPPING) {
    if (checkCartApiForMobileApp(prevStage, currentStage)) {
      makeCartApiCall =
        availableStages.indexOf(prevStage.toLowerCase()) <=
        availableStages.indexOf(currentStage.toLowerCase());
    } else {
      makeCartApiCall =
        availableStages.indexOf(pageName.toLowerCase()) >=
        availableStages.indexOf(currentStage.toLowerCase());
    }

    if (ifRedirectFromExpress) makeCartApiCall = true;

    if (!appRouting && makeCartApiCall) {
      pendingPromises.push(
        call(getCartDataSaga, {
          payload: {
            isRecalculateTaxes: false,
            excludeCartItems: !isServer,
            recalcRewards: recalc,
            updateSmsInfo: false,
            translation: true,
            isCheckoutFlow: true,
            supressCoupons: !isGetCoupon && !fromATB,
          },
        })
      );
    }
  } else if (isNonExpressCheckout) {
    pendingPromises.push(
      call(getCartDataSaga, {
        payload: {
          isRecalculateTaxes: true,
          excludeCartItems: false,
          recalcRewards: recalc,
          updateSmsInfo: false,
          translation: true,
          isCheckoutFlow: true,
          supressCoupons: !plccUser,
        },
      })
    );
  }

  yield all(pendingPromises);
  yield call(getPaypayExpressDataIfRequired, {
    pageName,
    isPaypalPostBack,
    isExpressCheckoutEnabled,
    isNonExpressCheckout,
  });
  const requestedStage = yield call(handleCheckoutInitRouting, { pageName }, appRouting);
  yield call(initShippingData, requestedStage, makeCartApiCall);
  const isVenmoInProgress = yield select(selectors.isVenmoPaymentInProgress);
  if (makeUpdateRTPSCall(pageName, isPaypalPostBack, isExpressCheckoutEnabled, isVenmoInProgress)) {
    yield call(callUpdateRTPS, pageName, navigation, isPaypalPostBack, isVenmoInProgress);
  }
  const shippingValues = yield select(getShippingDestinationValues);
  yield call(validateAdress, isPaypalPostBack, shippingValues, false, pageName);
}

// function* displayPreScreenModal (res) {
//   return getPlccOperator(this.store).optionalPlccOfferModal(
//     res.plccEligible,
//     res.prescreenCode).then(() => {
//       return this.loadUpdatedCheckoutValues();
//     });
// };

function* triggerExpressCheckout(
  section,
  navigation,
  isPaypalPostBack,
  shouldPreScreenUser = false,
  source = null
) {
  let pageName = section;
  if (isMobileApp()) {
    pageName = yield select(getCurrentCheckoutStage);
    pageName = pageName.toLowerCase();
  }
  try {
    const initialShippingValues = yield select(getShippingDestinationValues);
    const checkoutStage = yield select(getCurrentCheckoutStage);
    const loaderCheck =
      !checkoutStage || checkoutStage === 'Shipping' || !initialShippingValues.address;
    if (isMobileApp()) {
      if (loaderCheck) {
        yield put(BAG_PAGE_ACTIONS.setBagPageLoading());
      }
    } else {
      yield put(BAG_PAGE_ACTIONS.setBagPageLoading());
    }
    yield put(BAG_PAGE_ACTIONS.setCartSupressOnBagUpdate(false));
    const isVenmoInProgress = yield select(selectors.isVenmoPaymentInProgress);
    const res = yield startExpressCheckout(shouldPreScreenUser, source);
    if (!res.orderId) {
      return yield redirectFromExpress();
    }
    yield call(getAddressList);
    yield call(getCartDataSaga, {
      payload: {
        isRecalculateTaxes: true,
        excludeCartItems: false,
        recalcRewards: true,
        isCheckoutFlow: true,
        translation: true,
      },
    });
    if (!isPaypalPostBack) {
      yield call(callUpdateRTPS, pageName, navigation, isPaypalPostBack, isVenmoInProgress);
    }
    const shippingValues = yield select(getShippingDestinationValues);
    const shippingAddress = (shippingValues && shippingValues.address) || {};
    const isExpressCheckoutEnabled = yield select(isExpressCheckout);
    yield call(validateAdress, false, shippingValues, isExpressCheckoutEnabled, pageName);
    return yield validDateAndLoadShipmentMethods(
      {
        country: shippingAddress.country || '',
        state: shippingAddress.state || '',
        zipCode: shippingAddress.zipCode || '',
      },
      { state: true, zipCode: true }
    );
  } catch (e) {
    return yield redirectFromExpress();
  }
}

function* loadExpressCheckout(section, navigation, isPaypalPostBack) {
  //    On shipping we taking into acocunt if this is a gift or not.
  //    On express checkout we pre-screen no matter what,
  //    even though the user may have a gift order
  // const isPreScreenEnabled = yield select(selectors.getIsPreScreenEnabled);
  // const isUserPlcc = yield select(isPlccUser);
  // const shouldPreScreenUser = false;
  // const shouldPreScreenUser = isPreScreenEnabled && !isUserPlcc;
  //     let source = null;
  //     if (checkoutStoreView.isVenmoPaymentInProgress(this.store.getState())) {
  //       source = 'venmo';
  //     }
  yield call(triggerExpressCheckout, section, navigation, isPaypalPostBack);
}

function* loadStartupData(isPaypalPostBack, isRecalcRewards, section, navigation /* isVenmo */) {
  const isExpressCheckoutEnabled = yield select(isExpressCheckout);
  const isAppleInProgress = yield select(selectors.isApplePaymentInProgress);
  const isGuestUser = yield select(isGuest);
  const isRemembered = yield select(isRememberedUser);
  // const isOrderHasPickup = yield select(selectors.getIsOrderHasPickup);
  // if (isVenmo) {
  //   const venmoData = getLocalStorage(VENMO_STORAGE_KEY);
  //   if (venmoData) {
  //    this.setVenmoData(JSON.parse(venmoData));
  //   }
  //   if (!(venmoData && venmoData.details && venmoData.details.username)) {
  //     const contextAttributes = userStoreView.getContextAttributes(storeState);
  //     let username = '';
  //     if (contextAttributes && contextAttributes.venmoUserId) {
  //       username = contextAttributes.venmoUserId;
  //       this.setVenmoData({details: { username }});
  //     }
  //   }
  //   this.store.dispatch(setVenmoPaymentInProgress(parseBoolean(isVenmo)));
  //   this.setVenmoData({ pageLoading: false, loading: false });
  // }
  // let checkoutSignalsOperator = getCheckoutSignalsOperator(this.store);
  // let generalOperator = getGeneralOperator(this.store);
  const pendingPromises = [];
  //   let loadCartAndCheckoutDetails = () => {
  //     return this.loadUpdatedCheckoutValues(null, null, isRecalcRewards)
  //     .then(loadSelectedOrDefaultShippingMethods);
  // };

  // let loadSelectedOrDefaultShippingMethods = () => {
  //    We need the shipping methods to load AFTER the cart details
  //    in case there are already prefilled shipping details
  //    (such in paypal postback)

  //   };

  // if (userStoreView.isRemembered(storeState)) {
  //   getRoutingOperator(this.store).gotoPage(HOME_PAGE_SECTIONS[DRAWER_IDS.LOGIN]);
  //   return Promise.reject(
  //     new Error('Remembered user must login/logout before checkout')
  //   );
  // }

  if (!isPaypalPostBack && isExpressCheckoutEnabled && !isAppleInProgress) {
    pendingPromises.push(call(loadExpressCheckout, section, navigation, isPaypalPostBack));
  } else if (!isGuestUser) {
    pendingPromises.push(call(getAddressList));
  }

  // DRP-974: If the user is a remembered user, we are redirecting him/her to the home page and open the login modal
  // else we are continuing.
  if (isRemembered && !isMobileApp()) {
    utility.routeToPage(CHECKOUT_ROUTES.home);
    yield put(
      openOverlayModal({
        component: 'login',
        variation: 'primary',
      })
    );
  } else {
    yield all(pendingPromises);
  }
}

function* initCheckout({ router, isPaypalFlow, navigation }) {
  let isPaypalPostBack;
  let recalc;
  let section;
  if (router && router.query) {
    const { query } = router;
    ({ isPaypalPostBack, recalc, section } = query);
  }
  if (isMobileApp() && isPaypalFlow) {
    isPaypalPostBack = isPaypalFlow;
  }
  try {
    yield call(loadStartupData, isPaypalPostBack, recalc, section, navigation);
    yield put(setInitCheckoutFinished(true));
  } catch (e) {
    yield put(setInitCheckoutFinished(true));
    logger.error(e);
  }
}

/**
 * initIntlCheckout component. This is responsible for initiating actions required for start of international checkout journey.
 */
function* initIntlCheckout() {
  try {
    const res = yield call(getInternationCheckoutSettings);
    yield put(CHECKOUT_ACTIONS.getSetIntlUrl(res.checkoutUrl));
  } catch (e) {
    logger.error(`initIntlCheckout:${e}`);
  }
}

function* triggerEddPersist({ payload }) {
  try {
    yield startEddPersist(payload);
  } catch (e) {
    logger.error(`triggerEddPersist:${e}`);
  }
}

function* submitShipping({
  isEmailSignUpAllowed,
  emailSignUp = {},
  emailAddress,
  isGuestUser,
  address,
  onFileAddressKey,
  setAsDefault,
  phoneNumber,
  saveToAccount,
  method,
  smsInfo,
  hasSetGiftOptions,
}) {
  const { emailSignUpTCP, emailSignUpGYM } = emailSignUp;
  const giftServicesFormData = yield select(getGiftServicesFormData);
  yield addAndSetGiftWrappingOptions(giftServicesFormData, hasSetGiftOptions);
  yield put(setAddressError(null));
  const pendingPromises = [
    // add the requested gift wrap options
    // giftWrap.hasGiftWrapping && call(addGiftWrappingOption, giftWrap.message, giftWrap.optionId),
    // remove old gift wrap option (if any)
    // !giftWrap.hasGiftWrapping && giftWrappingStoreOptionID && call(removeGiftWrappingOption),
    // sign up to receive mail newsletter
    isEmailSignUpAllowed &&
      (emailSignUpTCP || emailSignUpGYM) &&
      submitEmailSignup(emailAddress, { emailSignUpTCP, emailSignUpGYM }),
  ];
  let addOrEditAddressRes;
  if (isGuestUser) {
    const oldShippingDestination = yield select(getShippingDestinationValues);
    addOrEditAddressRes = yield addOrEditGuestUserAddress({
      oldShippingDestination,
      address,
      phoneNumber,
      emailAddress,
      saveToAccount,
      setAsDefault,
    });
  } else {
    addOrEditAddressRes = yield addRegisteredUserAddress({
      onFileAddressKey,
      address,
      phoneNumber,
      emailAddress,
      setAsDefault,
      saveToAccount,
    });
  }
  const {
    payload: { addressId },
  } = addOrEditAddressRes;
  // Retrieve phone number info for sms updates
  yield saveLocalSmsInfo(smsInfo);
  yield all(pendingPromises);
  const giftOptionValues = yield select(selectors.getGiftServicesFormData);
  const setShippingMethodResponse = yield call(
    setShippingMethodAndAddressId,
    method.shippingMethodId,
    addressId,
    false, // generalStoreView.getIsPrescreenFormEnabled(storeState) && !giftWrap.hasGiftWrapping && !userStoreView.getUserIsPlcc(storeState)
    smsInfo ? smsInfo.smsUpdateNumber : null,
    false,
    giftOptionValues
  );
  if (
    setShippingMethodResponse &&
    setShippingMethodResponse.ERR_STATE_DOES_NOT_MATCH_SHIPMENT_OPTION
  ) {
    yield put(
      fetchDefaultShipmentMethods({ pageEvent: CONSTANTS.ERR_STATE_DOES_NOT_MATCH_SHIPMENT_OPTION })
    );
    throw setShippingMethodResponse;
  }

  // return getPlccOperator(store)
  //   .optionalPlccOfferModal(res.plccEligible, res.prescreenCode)
  // REVIEW: the true indicates to load the reward data for user.
  // But how can the reward points change here?
  yield select(selectors.getSmsNumberForOrderUpdates);
}

export function* submitBillingSection(action) {
  const isVenmoInProgress = yield select(selectors.isVenmoPaymentInProgress);
  const isAppleInProgress = yield select(selectors.isApplePaymentInProgress);
  const isAfterPayInProgress = yield select(selectors.getIsAfterPayInProgress);
  if (isMobileApp) {
    yield put(setSectionLoaderState({ billingToReviewLoader: true, section: 'billingToReview' }));
  } else {
    yield put(setLoaderState(true));
  }
  if (isVenmoInProgress) {
    yield call(submitVenmoBilling, action);
  } else if (isAppleInProgress) {
    yield call(submitAppleBilling, action);
  } else if (isAfterPayInProgress) {
    yield call(submitAfterPayOnBilling, action);
  } else {
    yield call(submitBilling, action);
  }
  if (isMobileApp)
    yield put(setSectionLoaderState({ billingToReviewLoader: false, section: 'billingToReview' }));
  yield put(setLoaderState(false));
}

export function* submitShippingSection(action) {
  yield submitShippingSectionData(action, submitShipping);
}

export function* submitVerifiedAddress(action) {
  try {
    if (!isMobileApp) yield put(setLoaderState(true));
    yield submitVerifiedAddressData(action, submitShipping);
    yield put(setLoaderState(false));
  } catch (err) {
    yield put(setLoaderState(false));
  }
}

function* setFromDataIfNotEqual(action) {
  const {
    payload: { formName, field, value },
  } = action;
  const selector = formValueSelector(formName);
  const state = yield select();
  if (selector(state, field) !== value) {
    yield put(change(formName, field, value));
  }
}

export function* callEddService(payload) {
  const { zipCode, state, orderId, skuList, isEDD, isEditing, eddAllowedStatesArr } = payload;
  const isEDDAllowedState =
    eddAllowedStatesArr && eddAllowedStatesArr.length > 0
      ? eddAllowedStatesArr.indexOf(state) > -1
      : false;

  if (isEDD && skuList.length && !isEditing) {
    if (isEDDAllowedState) {
      yield put(
        getEddData({
          zipCode,
          state,
          orderId,
          skuList,
          ignoreCache: true,
        })
      );
    } else {
      yield put(resetEddData(true));
    }
  }
}

export function* fetchAddressFromMelissa({ payload }) {
  try {
    const {
      zipCodeValue,
      addressField1,
      formName,
      formSection,
      shouldLoadShipmentMethods,
      orderId,
      skuData,
      isEDD,
      isEditing,
      eddAllowedStatesArr,
    } = payload;
    yield put(toggleZipCodeLoader(true));
    const { suggestedAddress } = yield call(verifyAddressData, { zip: zipCodeValue });
    yield put(
      change(formName, `${formSection ? 'address.' : ''}city`, suggestedAddress.city.trim() || '')
    );
    yield put(
      change(
        formName,
        `${formSection ? 'address.' : ''}state`,
        suggestedAddress.state.trim() || null
      )
    );
    if (
      !isCanada() &&
      shouldLoadShipmentMethods &&
      suggestedAddress.state &&
      suggestedAddress.state.trim()
    ) {
      yield call(loadShipmentMethods, { state: suggestedAddress.state, addressField1 });
      yield call(callEddService, {
        zipCode: zipCodeValue,
        state: suggestedAddress.state,
        orderId,
        skuList: skuData,
        isEDD,
        isEditing,
        eddAllowedStatesArr,
      });
    }
    yield put(toggleZipCodeLoader(false));
  } catch (err) {
    yield put(toggleZipCodeLoader(false));
  }
}

export function* CheckoutSaga() {
  yield takeLatest(CONSTANTS.INIT_CHECKOUT, initCheckout);
  yield takeLatest(CONSTANTS.INIT_CHECKOUT_SECTION_PAGE, initCheckoutSectionData);
  // yield takeLatest(CONSTANTS.INIT_SHIPPING_PAGE, initShippingData);
  // yield takeLatest(CONSTANTS.INIT_BILLING_PAGE, initBillingData);
  // yield takeLatest(CONSTANTS.INIT_REVIEW_PAGE, initReviewData);
  yield takeLatest(CONSTANTS.CHECKOUT_SUBMIT_VERIFIED_SHIPPING_ADDRESS, submitVerifiedAddress);
  yield takeLatest('INIT_INTL_CHECKOUT', initIntlCheckout);
  yield takeLatest('CHECKOUT_SET_CART_DATA', storeUpdatedCheckoutValues);
  yield takeLatest(CONSTANTS.SUBMIT_SHIPPING_SECTION, submitShippingSection);
  yield takeLatest(CONSTANTS.SUBMIT_BILLING_SECTION, submitBillingSection);
  yield takeLatest('CHECKOUT_SUBMIT_PICKUP_DATA', submitPickupSection);
  yield takeLatest(CONSTANTS.CHECKOUT_LOAD_SHIPMENT_METHODS, loadShipmentMethods);
  yield takeLatest(CONSTANTS.ROUTE_TO_PICKUP_PAGE, routeToPickupPage);
  yield takeLatest(
    CONSTANTS.CHECKOUT_UPDATE_SHIPMENT_METHOD_SELECTION,
    updateShipmentMethodSelection
  );
  yield takeLatest(CONSTANTS.UPDATE_SHIPPING_ADDRESS, updateShippingAddress);
  yield takeLatest(CONSTANTS.ADD_NEW_SHIPPING_ADDRESS, addNewShippingAddress);
  yield takeLeading(CONSTANTS.SUBMIT_REVIEW_SECTION, submitOrderForProcessing);
  yield takeLatest(CONSTANTS.GET_VENMO_CLIENT_TOKEN, getVenmoClientTokenSaga);
  yield takeLatest(CONSTANTS.GET_APPLE_CLIENT_TOKEN, getAppleClientTokenSaga);
  yield takeLatest(CONSTANTS.UPDATE_CARD_DATA, updateCardDetails);
  yield takeLatest(CONSTANTS.SUBMIT_ACCEPT_DECLINE_PLCC_OFFER, submitAcceptOrDeclinePlccData);
  yield takeEvery(CONSTANTS.SET_FROM_DATA_IF_NOT_EQUAL, setFromDataIfNotEqual);
  yield takeLatest(CONSTANTS.FETCH_ZIP_CODE_SUGGESTED_ADDRESS, fetchAddressFromMelissa);
  yield takeLatest(CONSTANTS.CALL_PERSIST_EDD, triggerEddPersist);
  yield takeLatest(CONSTANTS.CHECKOUT_WITH_AFTERPAY, checkoutWithAfterPay);
}
export default CheckoutSaga;
