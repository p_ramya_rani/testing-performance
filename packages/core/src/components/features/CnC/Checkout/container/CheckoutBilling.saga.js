/* eslint-disable */
// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, select } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';
import { isCouponApplied } from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.selectors';
import {
  loadPersonalizedCoupons,
  fetchCoupons,
} from '@tcp/core/src/components/features/CnC/Checkout/container/CheckoutReview.saga';
import bagPageActions from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import { trackForterAction, ForterActionType } from '@tcp/core/src/utils/forter.util';
import {
  getSetOrderConfirmationActn,
  getSetOrderProductDetails,
} from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.actions';
import { getIsForterEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { trackFormError } from '@tcp/core/src/analytics/actions';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getForterDeviceID } from '@tcp/core/src/utils/forter.util';
import {
  updatePaymentOnOrder,
  addPaymentToOrder,
  getServerErrorMessage,
  CREDIT_CARDS_PAYMETHODID,
  saveOrUpdateBillingInfo,
  handleSubmitOrderResponse,
} from '../../../../../services/abstractors/CnC/index';

import { resetAirmilesReducer } from '../../common/organism/AirmilesBanner/container/AirmilesBanner.actions';
import { validateAndSubmitEmailSignup } from './CheckoutExtended.saga.util';
import { resetCouponReducer } from '../../common/organism/CouponAndPromos/container/Coupon.actions';
import { verifyAddressData, updateAddress } from '../../../../../services/abstractors/account';
import selectors, { isGuest } from './Checkout.selector';
import CHECKOUT_ACTIONS, {
  getSetIsBillingVisitedActn,
  getSetCheckoutStage,
  setAfterPayInProgress,
  submitReviewSection,
  setCouponRemovedFromOrder,
} from './Checkout.action';
import { getGrandTotal } from '../../common/organism/OrderLedger/container/orderLedger.selector';
import utility from '../util/utility';
import setLoaderState from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import {
  addAddressGet,
  updateAddressPut,
} from '../../../../common/organisms/AddEditAddress/container/AddEditAddress.saga';
import { updateCreditCardSaga } from '../../../account/AddEditCreditCard/container/AddEditCreditCard.saga';
import CONSTANTS, { CHECKOUT_ROUTES, ORDER_TYPE } from '../Checkout.constants';
import { isMobileApp, isTCP, isSNJ, isGymboree } from '../../../../../utils';
import { getAddressList } from '../../../account/AddressBook/container/AddressBook.saga';
import { getCardList } from '../../../account/Payment/container/Payment.saga';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import { getFormattedError } from '../../../../../utils/errorMessage.util';
import CreditCardSelector from '../organisms/BillingPaymentForm/container/CreditCard.selectors';
import ADDRESS_VERIFICATION_CONSTANTS from '../../../../common/organisms/AddressVerification/AddressVerification.constants';
import { isCanada } from '../../../../../utils/utils';
import { getCartDataSaga } from '../../BagPage/container/BagPage.saga';

const {
  getIsPaymentDisabled,
  getBillingValues,
  getAddressByKey,
  isCardNotUpdated,
  getShippingDestinationValues,
  getDetailedCreditCardById,
  getIsOptimiseBillingInfoEnabled,
} = selectors;
const { getCreditCardType } = utility;

export function* updatePaymentInstruction(formData, cardDetailsInfo, isGuestUser, res) {
  let cardDetails;
  const errorMappings = yield select(BagPageSelectors.getErrorMapping);
  if (formData.onFileCardId) {
    if (!cardDetailsInfo) {
      cardDetails = yield select(getDetailedCreditCardById, formData.onFileCardId);
    } else {
      cardDetails = cardDetailsInfo;
    }
    const grandTotal = yield select(getGrandTotal);
    const requestData = {
      onFileCardId: formData.onFileCardId,
      cardNumber: cardDetails.accountNo,
      billingAddressId: cardDetails.billingAddressId,
      cardType: cardDetails.ccBrand && cardDetails.ccBrand.toUpperCase(),
      cvv: formData.cvv,
      monthExpire: cardDetails.expMonth,
      yearExpire: cardDetails.expYear,
      orderGrandTotal: grandTotal,
      setAsDefault: formData.setAsDefault || cardDetails.defaultInd,
      saveToAccount: !isGuestUser, // it's already on the account? why is this needed?
      applyToOrder: true,
    };
    // FIXME: we need to store the details of the selected card and selected
    // address book entry, but like this it is pretty ugly. needs major cleanup
    yield call(addPaymentToOrder, requestData, errorMappings);
    yield select(isCardNotUpdated, requestData.onFileCardId);
  } else {
    const cardType = getCreditCardType(formData);
    const checkoutDetails = yield select(getBillingValues);
    const editingCardType =
      checkoutDetails.billing && checkoutDetails.billing.cardNumber
        ? getCreditCardType(checkoutDetails.billing)
        : '';
    const grandTotal = yield select(getGrandTotal);
    const requestData = {
      cardNumber: formData.cardNumber,
      billingAddressId: res.addressId,
      cardType: cardType || editingCardType,
      cvv: formData.cvv,
      monthExpire: formData.expMonth,
      yearExpire: formData.expYear,
      orderGrandTotal: grandTotal,
      setAsDefault: formData.setAsDefault,
      saveToAccount: !isGuestUser && formData.saveToAccount,
      applyToOrder: true,
    };
    let addOrEditPaymentToOrder = addPaymentToOrder;
    // if it's a new card (no '*' in it) then we still need to call the addPayment instead of updatePayment service
    if (
      checkoutDetails.paymentId &&
      formData.cardNumber &&
      formData.cardNumber.substr(0, 1) === '*'
    ) {
      requestData.paymentId = checkoutDetails.paymentId;
      addOrEditPaymentToOrder = updatePaymentOnOrder;
    }
    yield call(addOrEditPaymentToOrder, requestData, errorMappings);
  }
  // updatePaymentToActiveOnSubmitBilling(store);
  // getUserOperator(store).setRewardPointsData();
}

/**
 * @function updateVenmoPaymentInstruction
 * @description - Update payment instruction for venmo checkout
 * @param {object} venmoDetails
 */
export function* updateVenmoPaymentInstruction() {
  const { PAYMENT_METHOD_VENMO } = CONSTANTS;
  const grandTotal = yield select(getGrandTotal);
  const shippingDetails = yield select(getShippingDestinationValues);
  const isVenmoSaveSelected = yield select(selectors.isVenmoPaymentSaveSelected);
  const venmoData = yield select(selectors.getVenmoData);
  const { nonce: venmoNonce, deviceData: venmoDeviceData } = venmoData || {};
  const userName = yield select(selectors.getVenmoUserName);
  const billingAddressId = shippingDetails.onFileAddressId;
  const paymentMethod = PAYMENT_METHOD_VENMO && PAYMENT_METHOD_VENMO.toUpperCase();
  const requestData = {
    billingAddressId,
    cardType: paymentMethod,
    cc_brand: paymentMethod,
    cardNumber: userName || 'test-user', // Venmo User Id, for all the scenario's it will have user information from the venmo, for dev, added test-user
    isDefault: 'false',
    orderGrandTotal: grandTotal,
    applyToOrder: true,
    monthExpire: '',
    yearExpire: '',
    setAsDefault: false,
    saveToAccount: false,
    venmoDetails: {
      userId: userName || 'test-user',
      saveVenmoTokenIntoProfile: isVenmoSaveSelected,
      nonce: venmoNonce,
      venmoDeviceData,
    },
  };
  const errorMappings = yield select(BagPageSelectors.getErrorMapping);
  yield call(addPaymentToOrder, requestData, errorMappings);
}

/**
 * @function updateAppleBillingInstruction
 * @description - Update billing instruction for apple checkout
 * @param {object} appleOrderSubmit
 */
export function* updateAppleBillingInstruction(payload) {
  const { PAYMENT_TYPE_APPLE } = CONSTANTS;
  const {
    appleOrderSubmit,
    paymentNonce,
    storeId,
    device_data,
    email,
    amount,
    orderId,
    addressId,
  } = payload;
  let header = {};
  header.paymentType = PAYMENT_TYPE_APPLE;
  header.orderSubmit = appleOrderSubmit;

  const requestData = {
    paymentNonce,
    storeId,
    device_data,
    email,
    amount: amount.toFixed(2).toString(),
    orderId: orderId && orderId.toString(),
    addressId: addressId && addressId.toString(),
  };
  const errorMappings = yield select(BagPageSelectors.getErrorMapping);
  const forterEnabled = yield select(getIsForterEnabled);
  if (isMobileApp() && forterEnabled) {
    header.deviceID = getForterDeviceID();
  }
  const responseBilling = yield call(
    saveOrUpdateBillingInfo,
    { requestData, header },
    errorMappings
  );
  return responseBilling;
}

/**
 * @function checkoutWithAfterPay
 * @description - Update payment instruction for venmo checkout
 * @param {object} billingdetails
 */
export function* checkoutWithAfterPay({ payload }) {
  const grandTotal = yield select(getGrandTotal);
  const shippingDetails = yield select(getShippingDestinationValues);
  const {
    cardNumber,
    monthExpire,
    yearExpire,
    cvv,
    cardType,
    reviewFormValues,
    addressId,
    navigation,
  } = payload;
  const billingAddressId = shippingDetails.onFileAddressId || addressId;
  const requestData = {
    billingAddressId,
    cardType,
    cc_brand: cardType,
    cardNumber,
    monthExpire,
    yearExpire,
    cvv,
    isDefault: 'false',
    orderGrandTotal: grandTotal,
    applyToOrder: true,
    setAsDefault: false,
    saveToAccount: false,
    afterpay: true,
  };
  const errorMappings = yield select(BagPageSelectors.getErrorMapping);
  try {
    const response = yield call(addPaymentToOrder, requestData, errorMappings);
    if (response) {
      const submitReviewPayload = { formData: reviewFormValues };
      if (navigation) {
        submitReviewPayload.navigation = navigation;
      }
      yield put(submitReviewSection(submitReviewPayload));
    } else {
      yield put(setLoaderState(false));
    }
  } catch (e) {
    yield put(setLoaderState(false));
    const errorsMapping = yield select(BagPageSelectors.getErrorMapping);
    let billingError = getServerErrorMessage(e, errorsMapping);
    if (e && e.errorCodes === CONSTANTS.CUSTOM_OOS_ERROR_CODE) {
      billingError = e.errorMessages._error;
    }
    yield put(
      CHECKOUT_ACTIONS.setServerErrorCheckout({ errorMessage: billingError, component: 'PAGE' })
    );
  }
}

export function* getAddressData(formData) {
  const existingAddress = yield select(getAddressByKey, formData.address.onFileAddressKey);
  const shippingDetails = yield select(getShippingDestinationValues);
  return existingAddress ? existingAddress.addressId : shippingDetails.onFileAddressId;
}

export function addressIdToString(addressId) {
  if (addressId) {
    return addressId.toString();
  }
  return null;
}

export function* submitBillingWithSingleAPI(formData, navigation) {
  const {
    ADDRESS_STATUS: { ADD, NO_ACTION, UPDATE },
  } = CONSTANTS;
  let requestData = { contact: [], paymentInstruction: [] };
  let addressRequestData = {};
  let addressStatus = ADD;
  let header = {};
  let paymentInstruction = {};
  let fetchCreditCards = false;
  const errorMappings = yield select(BagPageSelectors.getErrorMapping);
  addressRequestData.fromPage = 'checkout';
  const isGuestUser = yield select(isGuest);
  if (formData.address.sameAsShipping) {
    const shippingDetails = yield select(getShippingDestinationValues);
    addressRequestData.addressId = shippingDetails.onFileAddressId;
    header.nickName = shippingDetails.onFileAddressKey;
    addressStatus = NO_ACTION;
  } else if (formData.onFileCardId) {
    const cardDetails = yield select(getDetailedCreditCardById, formData.onFileCardId);
    addressRequestData.addressId =
      addressIdToString(cardDetails.addressId) || addressIdToString(cardDetails.billingAddressId);
    addressStatus = NO_ACTION;
    header.nickName = cardDetails.addressKey;
  } else if (formData.address.onFileAddressKey && !isGuestUser) {
    const addressId = yield call(getAddressData, formData);
    addressRequestData.addressId = addressId;
    header.nickName = formData.address.onFileAddressKey;
    addressStatus = NO_ACTION;
  } else {
    const { address, phoneNumber } = formData;
    addressRequestData.addressLine = [address.addressLine1 || '', address.addressLine2 || '', ''];
    addressRequestData.attributes = [
      {
        key: 'addressField3',
        value: address.zipCode || '',
      },
    ];
    addressRequestData.addressType = 'ShippingAndBilling';
    addressRequestData.city = address.city;
    addressRequestData.country = address.country;
    addressRequestData.firstName = address.firstName;
    addressRequestData.lastName = address.lastName;
    addressRequestData.phone1 = phoneNumber;
    addressRequestData.email1 = (address.emailAddress || '').trim();
    addressRequestData.state = address.state;
    addressRequestData.zipCode = address.zipCode;
    addressRequestData.xcont_addressField2 = address.isCommercialAddress ? '2' : '1';
    addressRequestData.xcont_addressField3 = address.zipCode;

    if (formData.address.onFileAddressKey && isGuestUser) {
      let { addressKey } = formData;
      addressRequestData.phone1Publish = false;
      addressRequestData.isDefault = formData.isDefault;
      addressRequestData.applyToOrder = true;
      // DRP-847: When updating the billing address for guest user we need to pass the nickname as we pass when adding new address
      addressRequestData.nickName = addressKey || new Date().getTime().toString();
      addressStatus = UPDATE;
    } else {
      // add new Address
      let { addressKey } = formData;
      const saveToAccount = !isGuestUser && formData.saveToAccount;
      addressRequestData.phone1Publish = saveToAccount ? 'false' : 'true';
      addressRequestData.nickName = addressKey || new Date().getTime().toString();
      addressRequestData.primary = formData.isDefault ? 'true' : 'false';
    }
  }
  const grandTotal = yield select(getGrandTotal);
  const piAmount = grandTotal && grandTotal.toString();
  const cvv = formData.cvv;
  if (formData.onFileCardId) {
    const cardDetails = yield select(getDetailedCreditCardById, formData.onFileCardId);
    const ccBrand = cardDetails.ccBrand && cardDetails.ccBrand.toUpperCase();
    paymentInstruction = {
      billing_address_id: (
        formData.address.onFileAddressId ||
        cardDetails.billingAddressId ||
        ''
      ).toString(),
      piAmount,
      creditCardId: (formData.onFileCardId || '').toString(),
      cc_brand: ccBrand,
      payMethodId: CREDIT_CARDS_PAYMETHODID[ccBrand],
      cc_cvc: cvv,
      account: (cardDetails.accountNo || '').toString(),
      expire_month: (cardDetails && cardDetails.expMonth && cardDetails.expMonth.toString()) || '',
      expire_year: (cardDetails && cardDetails.expYear && cardDetails.expYear.toString()) || '',
      isDefault: formData.setAsDefault || cardDetails.isDefault,
      saveToAccount: !isGuestUser,
      applyToOrder: true,
    };
    header.saveToAccount = !isGuestUser;
    header.isAddPayment = true;
  } else {
    const cardType = getCreditCardType(formData);
    const checkoutDetails = yield select(getBillingValues);
    const editingCardType =
      checkoutDetails.billing && checkoutDetails.billing.cardNumber
        ? getCreditCardType(checkoutDetails.billing)
        : '';
    header.isAddPayment = true;
    paymentInstruction = {
      piAmount,
      cc_brand: cardType || editingCardType,
      payMethodId: CREDIT_CARDS_PAYMETHODID[cardType || editingCardType],
      cc_cvc: cvv,
      expire_month: (formData && formData.expMonth && formData.expMonth.toString()) || '',
      expire_year: (formData && formData.expYear && formData.expYear.toString()) || '',
      isDefault: (!!formData.setAsDefault).toString(),
      account: (formData.cardNumber || '').toString(),
    };
    if (
      checkoutDetails.paymentId &&
      formData.cardNumber &&
      formData.cardNumber.substr(0, 1) === '*'
    ) {
      paymentInstruction.piId = checkoutDetails.paymentId;
      header.isAddPayment = false;
    }
    header.saveToAccount = !isGuestUser && formData.saveToAccount;
    fetchCreditCards = true;
  }
  if (addressStatus === NO_ACTION) {
    requestData.addressId = addressRequestData.addressId;
    requestData.fromPage = addressRequestData.fromPage;
    delete requestData.contact;
  } else {
    requestData.contact.push(addressRequestData);
  }
  requestData.paymentInstruction.push(paymentInstruction);
  yield call(saveOrUpdateBillingInfo, { requestData, header, navigation }, errorMappings);

  if (!requestData.addressId && isMobileApp()) {
    trackForterAction(ForterActionType.TAP, 'BILLING_ADDRESS_UPDATE');
  }

  if (!isGuestUser) {
    if (addressStatus !== NO_ACTION) yield call(getAddressList);
    if (fetchCreditCards) yield call(getCardList, { ignoreCache: true });
  }
}

export function* submitBillingData(formData, address, navigation) {
  let res;
  let cardDetails;
  let fetchAddressBook = false;
  // const updatePaymentRequired = true;
  const isGuestUser = yield select(isGuest);
  const isOptimiseBillingEnabled = yield select(getIsOptimiseBillingInfoEnabled);
  try {
    if (isOptimiseBillingEnabled) {
      yield call(submitBillingWithSingleAPI, formData, navigation);
    } else {
      if (formData.address.sameAsShipping) {
        const shippingDetails = yield select(getShippingDestinationValues);
        res = yield call(updateAddress, {
          checkoutUpdateOnly: true,
          addressKey: shippingDetails.onFileAddressKey,
          addressId: shippingDetails.onFileAddressId,
        });
        res = res.body;
      } else if (formData.onFileCardId) {
        cardDetails = yield select(getDetailedCreditCardById, formData.onFileCardId);
        res = yield call(updateAddress, {
          checkoutUpdateOnly: true,
          addressKey: cardDetails.addressKey,
          addressId:
            addressIdToString(cardDetails.addressId) ||
            addressIdToString(cardDetails.billingAddressId),
        });
        res = res.body;
      } else if (formData.address.onFileAddressKey && !isGuestUser) {
        // return submitPaymentInformation({addressId: formData.address.onFileAddressKey});

        const addressId = yield call(getAddressData, formData);
        res = yield call(updateAddress, {
          checkoutUpdateOnly: true,
          addressKey: formData.address.onFileAddressKey,
          addressId,
        });
        res = res.body;
      } else if (formData.address.onFileAddressKey && isGuestUser) {
        // send update
        const addressId = yield call(getAddressData, formData);
        res = yield updateAddressPut(
          {
            payload: {
              ...address,
              phoneNumber: formData.phoneNumber,
              addressKey: formData.address.onFileAddressKey,
              nickName: formData.address.onFileAddressKey,
              addressId,
              isDefault: formData.isDefault,
              saveToAccount: false,
              applyToOrder: true,
            },
          },
          { profileUpdate: false }
        );
      } else {
        res = yield call(
          addAddressGet,
          {
            payload: {
              ...address,
              nickName: formData.address.onFileAddressKey,
              phoneNumber: formData.phoneNumber,
              isDefault: formData.isDefault,
              saveToAccount: !isGuestUser && formData.saveToAccount,
              applyToOrder: true,
            },
          },
          false
        );
        res = res.body;
        fetchAddressBook = true;
      }
      // if (updatePaymentRequired) {
      yield call(updatePaymentInstruction, formData, cardDetails, isGuestUser, res);
    }
    yield put(getSetIsBillingVisitedActn(true));
  } catch (err) {
    yield put(getSetIsBillingVisitedActn(false));
    throw err;
  }
  // }
  if (fetchAddressBook && !isGuestUser) {
    yield call(getAddressList);
  }
}

/**
 * @function submitVenmoBilling
 * @description - Redirect venmo payment from billing to review. This method is called from the Billing Page
 * @param {obejct} payload - venmo payload to submit billing and redirect to review page
 */
export function* submitVenmoBilling(payload = {}) {
  const { payload: { navigation } = {} } = payload;
  try {
    yield call(updateVenmoPaymentInstruction);
    yield put(getSetIsBillingVisitedActn(true)); // flag that billing section was visited by the user
  } catch (err) {
    yield put(getSetIsBillingVisitedActn(false)); // flag that billing section was visited by the user
    throw err;
  }
  if (!isMobileApp()) {
    utility.routeToPage(CHECKOUT_ROUTES.reviewPage);
  } else if (navigation) {
    yield put(getSetCheckoutStage(CONSTANTS.REVIEW_DEFAULT_PARAM));
  }
}

/**
 * @function submitAppleBilling
 * @description - Redirect apple payment from billing to review. This method is called from the Billing Page
 * @param {obejct} payload - apple payload to submit billing and redirect to review page
 */
export function* submitAppleBilling({ payload }) {
  let appleOrderSubmit = 'true';
  const { contact, orderId, orderType, navigation = null, navigationActions = null } = payload;
  let isECom = false;
  try {
    if (orderType === ORDER_TYPE.PURE_ECOM || orderType === ORDER_TYPE.ECOM_MIX) {
      isECom = true;
      const { resultCodes } = yield call(verifyAddressData, contact);
      const { GENERIC_CODES, OPTIONAL_ADDRESS_LINE } =
        ADDRESS_VERIFICATION_CONSTANTS.VERIFY_ERROR_CODES_MAPPING;
      const { GENERIC_ERROR_CODES } = ADDRESS_VERIFICATION_CONSTANTS.ADDRESS_FORM_ERROR_CODES;
      const isGenericError = GENERIC_CODES.some((code) => resultCodes.indexOf(code) !== -1);
      const showOptionalAddressLineError = resultCodes.includes(OPTIONAL_ADDRESS_LINE);
      const addressFormError = GENERIC_ERROR_CODES.some((code) => resultCodes.indexOf(code) !== -1);
      logger.info(
        'Malissa Results',
        isGenericError,
        showOptionalAddressLineError,
        addressFormError
      );
      if (isGenericError || showOptionalAddressLineError || addressFormError) {
        appleOrderSubmit = 'false';
      } else {
        appleOrderSubmit = 'true';
      }
    }
    try {
      const { tcpProducts, gymProducts, snjProducts } = yield select(
        BagPageSelectors.getProductsTypes
      );
      const result = yield call(updateAppleBillingInstruction, { ...payload, appleOrderSubmit });

      const res = result.addCheckoutResponse || result;
      yield put(getSetIsBillingVisitedActn(true)); // flag that billing section was visited by the user
      if (res && (res.paymentInstruction || res.orderSummaryJson)) {
        if (appleOrderSubmit === 'false' || res.couponRemovedFromOrder) {
          yield call(getAddressList);
          if (!isMobileApp()) {
            yield call(getCartDataSaga, {
              payload: {
                excludeCartItems: false,
                isCheckoutFlow: true,
                translation: false,
              },
            });
            utility.routeToPage(CHECKOUT_ROUTES.reviewPage);
          } else if (navigation) {
            yield put(
              bagPageActions.routeForCheckout({
                navigation,
                closeModal: null,
                recalc: true,
                navigationActions,
                checkoutStage: CONSTANTS.REVIEW_DEFAULT_PARAM,
              })
            );
          }
          if (res.couponRemovedFromOrder) {
            yield put(setCouponRemovedFromOrder(true));
          }
        } else {
          const response = { body: { ...res } };
          const formattedRes = handleSubmitOrderResponse(response);
          yield put(getSetOrderConfirmationActn(formattedRes));
          yield put(getSetCheckoutStage(CONSTANTS.CHECKOUT_STAGES.CONFIRMATION));
          if (!isMobileApp()) {
            utility.routeToPage(CHECKOUT_ROUTES.confirmationPage, { fromApplePay: 'true' });
          } else if (navigation) {
            // without this cart item count does not refresh
            yield call(getCartDataSaga, {
              payload: {
                isRecalculateTaxes: true,
                excludeCartItems: false,
                recalcRewards: true,
                isCheckoutFlow: true,
                translation: false,
              },
            });
            navigation.navigate(CONSTANTS.CHECKOUT_ROUTES_NAMES.CHECKOUT_CONFIRMATION);
          }
          yield call(loadPersonalizedCoupons, formattedRes, orderId);
          const cartItems = yield select(BagPageSelectors.getOrderItems);
          const email = formattedRes.userDetails
            ? formattedRes.userDetails.emailAddress
            : formattedRes.shipping.emailAddress;
          const isCaSite = yield call(isCanada);
          const isGuestUser = yield select(isGuest);
          const isCouponAppliedInOrder = yield select(isCouponApplied);
          yield put(getSetOrderProductDetails(cartItems));
          yield put(resetAirmilesReducer());
          yield put(resetCouponReducer());
          yield call(fetchCoupons, isCouponAppliedInOrder);
          if (isGuestUser && !isCaSite && email) {
            yield call(
              validateAndSubmitEmailSignup,
              email,
              'us_guest_checkout',
              tcpProducts.length > 0 || isTCP(),
              gymProducts.length > 0 || isGymboree(),
              snjProducts.length > 0 || isSNJ()
            );
          }
        }
      } else {
        if (!isMobileApp()) {
          utility.routeToPage(CHECKOUT_ROUTES.bagPage);
        } else if (navigation) {
          yield put(
            bagPageActions.routeForCheckout({
              navigation,
              closeModal: null,
              recalc: true,
              navigationActions,
              checkoutStage: CONSTANTS.BILLING_DEFAULT_PARAM,
            })
          );
        }
      }
    } catch (e) {
      logger.error('Error on submit billing', e);
      yield put(getSetIsBillingVisitedActn(false)); // flag that billing section was not visited by the user
      const errorsMapping = yield select(BagPageSelectors.getErrorMapping);
      let billingError = getServerErrorMessage(e, errorsMapping);
      if (e && e.errorCodes === CONSTANTS.CUSTOM_OOS_ERROR_CODE) {
        billingError = e.errorMessages._error;
      }
      yield put(
        CHECKOUT_ACTIONS.setServerErrorCheckout({ errorMessage: billingError, component: 'PAGE' })
      );
      if (!isMobileApp()) {
        utility.routeToPage(CHECKOUT_ROUTES.bagPage);
      } else if (navigation) {
        yield put(
          bagPageActions.routeForCheckout({
            navigation,
            closeModal: null,
            recalc: true,
            navigationActions,
            checkoutStage: CONSTANTS.BILLING_DEFAULT_PARAM,
          })
        );
      }
    }
  } catch (err) {
    logger.error('Error on mallisa call', err);
    if (!isMobileApp()) {
      utility.routeToPage(CHECKOUT_ROUTES.shippingPage);
    } else if (navigation) {
      yield put(
        bagPageActions.routeForCheckout({
          navigation,
          closeModal: null,
          recalc: true,
          navigationActions,
          checkoutStage: CONSTANTS.SHIPPING_DEFAULT_PARAM,
        })
      );
    }
  }
}

export default function* submitBilling(action = {}) {
  try {
    // TODO need to remove as it is temp fix to deliver review page for app
    const isGuestUser = yield select(isGuest);
    const { payload: { navigation, ...formData } = {} } = action;
    formData.phoneNumber = formData.phoneNumber || '';
    const { addressLine1: address1, addressLine2: address2, city, country } = formData.address;
    const { firstName, lastName } = formData.address;
    const { state, zipCode: zip } = formData.address;
    const address = { address1, address2, city, country, firstName, lastName, state, zip };
    yield put(getSetIsBillingVisitedActn(true)); // flag that billing section was visited by the user
    const isPaymentDisabled = yield select(getIsPaymentDisabled);
    if (!isPaymentDisabled) {
      yield call(submitBillingData, formData, address, navigation);
    }
    if (!isMobileApp()) {
      utility.routeToPage(CHECKOUT_ROUTES.reviewPage);
    } else if (navigation) {
      yield put(getSetCheckoutStage(CONSTANTS.REVIEW_DEFAULT_PARAM));
    }
  } catch (e) {
    logger.error('Error while submitting billing info', e);
    const { payload: { navigation, ...formData } = {} } = action;

    const { address, cardNumber, expYear, expMonth, cardType, cvvCode, paymentMethodId } = formData;
    const list3ValAddressFields = Object.keys(address).reduce((result, key) => {
      const resultItem = result;
      resultItem[`address.${key}`] = address[key];
      return resultItem;
    }, {});
    const requiredList3Val = {
      paymentMethodId,
      cardNumber,
      expYear,
      expMonth,
      cardType,
      cvvCode,
      ...list3ValAddressFields,
    };

    yield put(trackFormError({ formName: 'checkoutBillingPayment', formData: requiredList3Val }));
    // submitBillingError(store, e);
    const errorsMapping = yield select(BagPageSelectors.getErrorMapping);
    let billingError = getServerErrorMessage(e, errorsMapping);
    if (e && e.errorCodes === CONSTANTS.CUSTOM_OOS_ERROR_CODE) {
      billingError = e.errorMessages._error;
    }
    yield put(
      CHECKOUT_ACTIONS.setServerErrorCheckout({ errorMessage: billingError, component: 'PAGE' })
    );
  }
}

export function* updateCardDetails({ payload: { formData, resolve, reject } }) {
  try {
    const isGuestUser = yield select(isGuest);
    const cardType = yield select(CreditCardSelector.getEditFormCardType);
    yield updateCreditCardSaga({ payload: { ...formData, cardType } }, true);
    if (!isGuestUser) {
      yield call(getCardList, { ignoreCache: true });
    }
    resolve();
  } catch (err) {
    const errorsMapping = yield select(BagPageSelectors.getErrorMapping);
    const error = getServerErrorMessage(err, errorsMapping);
    reject(new SubmissionError({ _error: getFormattedError(new Error(error), errorsMapping) }));
  }
}

/**
 * @function submitAfterPayOnBilling
 * @description - Redirect venmo payment from billing to review. This method is called from the Billing Page
 * @param {obejct} payload - venmo payload to submit billing and redirect to review page
 */
export function* submitAfterPayOnBilling(action) {
  const { payload: { navigation } = {} } = action;
  yield put(setAfterPayInProgress(true));
  if (!isMobileApp()) {
    utility.routeToPage(CHECKOUT_ROUTES.reviewPage);
  } else if (navigation) {
    yield put(getSetCheckoutStage(CONSTANTS.REVIEW_DEFAULT_PARAM));
  }
}
