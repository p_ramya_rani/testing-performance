// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import { call, all, select, put } from 'redux-saga/effects';
import format from 'date-fns/format';
import differenceInMilliseconds from 'date-fns/differenceInMilliseconds';
// import { getUserEmail } from '../../../account/User/container/User.selectors';
import setLoaderState from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { getUserInfoSaga } from '@tcp/core/src/components/features/account/User/container/User.saga';
import { navigateXHRAction } from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.action';
import { getIsForterEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getSetWishlistsSummariesAction } from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import { FAVORITES_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { getForterDeviceID } from '@tcp/core/src/utils/forter.util';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  isCanada,
  sanitizeEntity,
  convertToISOString,
  isTCP,
  isGymboree,
  isSNJ,
} from '../../../../../utils/utils';
// import { addAddress } from '../../../../../services/abstractors/account/AddEditAddress';
import {
  submitOrder,
  requestPersonalizedCoupons,
  updatePaymentOnOrder,
  getServerErrorMessage,
} from '../../../../../services/abstractors/CnC/index';
import selectors, { isGuest, isExpressCheckout } from './Checkout.selector';
import utility from '../util/utility';
import constants, { CHECKOUT_ROUTES } from '../Checkout.constants';
import { callPickupSubmitMethod } from './Checkout.saga.util';
import { validateAndSubmitEmailSignup } from './CheckoutExtended.saga.util';
import {
  getAppliedCouponListState,
  isCouponApplied,
} from '../../common/organism/CouponAndPromos/container/Coupon.selectors';
import {
  isMobileApp,
  setValueInAsyncStorage,
  getValueFromAsyncStorage,
} from '../../../../../utils';
import {
  getSetCouponsValuesActn,
  getSetRewardPointsOrderConfActn,
  getSetOrderConfirmationActn,
  getSetOrderProductDetails,
  setCouponLoadingState,
} from '../../Confirmation/container/Confirmation.actions';
import ConfirmationSelectors from '../../Confirmation/container/Confirmation.selectors';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import CHECKOUT_ACTIONS, { getSetCheckoutStage, getSetBillingValuesActn } from './Checkout.action';
import { resetAirmilesReducer } from '../../common/organism/AirmilesBanner/container/AirmilesBanner.actions';
import {
  resetCouponReducer,
  getCouponList,
} from '../../common/organism/CouponAndPromos/container/Coupon.actions';

import { updateVenmoPaymentInstruction } from './CheckoutBilling.saga';
import { getGrandTotal } from '../../common/organism/OrderLedger/container/orderLedger.selector';

const {
  // isVenmoPaymentAvailable,
  getCurrentOrderId,
  getSmsNumberForBillingOrderUpdates,
  getIsPaymentDisabled,
  getCurrentLanguage,
  getBillingValues,
} = selectors;

function extractCouponInfo(personalizedOffersResponse) {
  try {
    return personalizedOffersResponse.coupon.map((coupon) => {
      let startDate;
      let endDate;
      let isPastStartDate;
      let categoryType;
      let description;
      const { couponCode, legalText, promotion } = coupon;
      /* istanbul ignore else */
      if (promotion) {
        const { categoryType: catType } = promotion;
        const proStartDate = new Date(convertToISOString(promotion.startDate));
        startDate = promotion.startDate && format(proStartDate, 'MMM do, yyyy');
        isPastStartDate =
          promotion.startDate && differenceInMilliseconds(new Date(), proStartDate) > 0; // check user browser date to server date
        endDate =
          promotion.endDate &&
          format(new Date(convertToISOString(promotion.endDate)), 'MMM do, yyyy');
        categoryType = catType;
        description = promotion.shortDescription;
      }

      return {
        code: couponCode,
        description,
        disclaimer: legalText && sanitizeEntity(legalText),
        endDate,
        isPastStartDate,
        startDate,
        categoryType,
      };
    });
  } catch (error) {
    return [];
  }
}

export function* loadPersonalizedCoupons(
  {
    isElectiveBonus,
    currencyCode,
    orderItems: cartItems,
    paymentsList: payments,
    userDetails: { emailAddress },
  },
  orderNumber
) {
  const isCaSite = yield call(isCanada);
  const isUsSite = !isCaSite;
  let couponList = yield select(getAppliedCouponListState);
  if (couponList && couponList.size > 0) {
    couponList = couponList.map((val) => ({ id: val.id }));
  }
  const { US_LOCATION_ID, CA_LOCATION_ID } = constants;
  const { personalizedOffersResponse, orderResponse } = yield call(requestPersonalizedCoupons, {
    orderNumber,
    emailAddress,
    locationId: isUsSite ? US_LOCATION_ID : CA_LOCATION_ID,
    couponList,
    isElectiveBonus,
    currencyCode,
    payments,
    cartItems,
  });
  let couponsInfo = [];
  /* istanbul ignore else */
  if (personalizedOffersResponse && personalizedOffersResponse.coupon) {
    couponsInfo = extractCouponInfo(personalizedOffersResponse);
    yield put(getSetCouponsValuesActn(couponsInfo)); // to be done at time of confirmation page as it require new reducer
  }
  const brierelyPointsInfo = {};
  /* istanbul ignore else */
  if (orderResponse) {
    // When brierley fails, backend returns -1 in these fields
    if (orderResponse.pointsToNextReward === -1) {
      brierelyPointsInfo.pointsToNextReward = 0;
    } else {
      brierelyPointsInfo.pointsToNextReward = orderResponse.pointsToNextReward;
    }
    if (orderResponse.userPoints === -1) {
      brierelyPointsInfo.estimatedRewards = null;
    } else {
      brierelyPointsInfo.estimatedRewards = orderResponse.userPoints;
    }
    /* istanbul ignore else */
    if (orderResponse.earnedReward) {
      brierelyPointsInfo.earnedReward = orderResponse.earnedReward;
    }
    // to be done at time of confirmation page as it require new reducer
    const summary = yield select(ConfirmationSelectors.getConfirmationSummary);
    const updatedSummary = { ...summary, ...brierelyPointsInfo };
    yield put(getSetRewardPointsOrderConfActn(updatedSummary));
  }
}

function* setDetailsInAsyncStorage(res) {
  const isGuestUser = yield select(isGuest);
  if (isGuestUser) {
    const { userDetails: { firstName, lastName, emailAddress, phoneNumber, zipCode } = {} } = res;
    const userDetail = {
      firstName,
      lastName,
      emailAddress,
      phoneNumber,
      zipCode,
    };
    const userDetailValue = JSON.stringify(userDetail);
    const userDetailString = yield call(getValueFromAsyncStorage, constants.USER_ACCOUNT_KEY);
    if (userDetailValue && userDetailValue !== userDetailString) {
      setValueInAsyncStorage(constants.USER_ACCOUNT_KEY, userDetailValue);
    }
  }
}

export function* submitOrderProcessing(orderId, smsOrderInfo, currentLanguage) {
  let venmoPayloadData = {};
  const isVenmoInProgress = yield select(selectors.isVenmoPaymentInProgress);
  const isVenmoSaveSelected = yield select(selectors.isVenmoPaymentSaveSelected);
  const venmoData = yield select(selectors.getVenmoData);
  const errorMappings = yield select(BagPageSelectors.getErrorMapping);
  const isPaymentDisabled = yield select(getIsPaymentDisabled);
  if (!isPaymentDisabled && isVenmoInProgress && venmoData) {
    yield call(updateVenmoPaymentInstruction);
    const { nonce: venmoNonce, deviceData: venmoDeviceData } = venmoData;
    const email = yield select(selectors.getVenmoUserEmail);
    venmoPayloadData = {
      venmoNonce,
      venmo_device_data: venmoDeviceData,
      email,
      isVenmoSaveSelected,
    };
  }
  const forterEnabled = yield select(getIsForterEnabled);
  const deviceID = getForterDeviceID();
  const header = isMobileApp() && forterEnabled ? { deviceID } : {};
  const orderItems = yield select(BagPageSelectors.getOrderItems);
  const hasBopisItems = orderItems.find(
    (item) => item.getIn(['productInfo', 'orderType']) === constants.ORDER_ITEM_TYPE.BOPIS
  );
  header.hasBopisItems = !!hasBopisItems;
  const res = yield call(
    submitOrder,
    orderId,
    smsOrderInfo,
    currentLanguage,
    venmoPayloadData,
    errorMappings,
    header
  );
  yield put(getSetOrderConfirmationActn(res));

  if (isMobileApp()) {
    const state = yield select();
    // The favorite page need to load wish list summary after submit order
    const activeWishlistObject =
      state[FAVORITES_REDUCER_KEY] && state[FAVORITES_REDUCER_KEY].get('activeWishList');
    if (activeWishlistObject && activeWishlistObject.id) {
      const activeWishlistId = activeWishlistObject.id;
      yield put(getSetWishlistsSummariesAction(activeWishlistId));
    }

    yield call(setDetailsInAsyncStorage, res);
  }
  return res;
}

// function* expressCheckoutSubmit(formData) {
export function* expressCheckoutSubmit(formData) {
  // if express checkout
  const { billing, hasAlternatePickup } = formData; // /* giftWrap, disabled */
  /* istanbul ignore else */
  if (hasAlternatePickup) {
    yield call(callPickupSubmitMethod, formData);
  }

  // const giftWrapPromise = Promise.resolve();

  /* NOTE: DT-19417: Gift Wrapping option no longer available for Express User on review page (backend does not support it)
   * I guess this needs to be re-enabled once backend fixes the services.
  if (giftWrap && giftWrap.hasGiftWrapping) {       // user specified gift wrapping
    // pendingPromises.push(this.checkoutServiceAbstractor.addGiftWrappingOption(giftWrap.message, giftWrap.optionId));
    giftWrapPromise = this.checkoutServiceAbstractor.addGiftWrappingOption(giftWrap.message, giftWrap.optionId);
  } else if (checkoutStoreView.getGiftWrappingValues(state).optionId) {
    // pendingPromises.push(this.checkoutServiceAbstractor.removeGiftWrappingOption()); // remove existing gift wrap (if any)
    giftWrapPromise = this.checkoutServiceAbstractor.removeGiftWrappingOption();
  }
  */
  /* istanbul ignore else */
  if (billing && billing.cvv) {
    // PLCC does not require CVV -> billing = null. User entered a cvv for a credit card
    // submit CVV
    const billingDetails = yield select(getBillingValues);
    const grandTotal = yield select(getGrandTotal);
    const requestData = {
      orderGrandTotal: grandTotal,
      monthExpire: billingDetails.billing.expMonth,
      yearExpire: billingDetails.billing.expYear,
      cardType: billingDetails.billing.cardType,
      cardNumber: billingDetails.billing.cardNumber,
      paymentId: billingDetails.paymentId,
      billingAddressId: billingDetails.address.onFileAddressId, // DT-34037 billing_address_id need to be send along with updatePayment.
      cvv: billing.cvv, // the cvv entered by the user
    };
    const result = yield call(updatePaymentOnOrder, requestData);
    if (result && result.paymentId) {
      billingDetails.paymentId = result.paymentId;
      yield put(getSetBillingValuesActn(billingDetails));
    }
  }
}

/**
 * @description calling this method on guest order submit, as BE needed to get the cookies and based on that associate
 * recent order
 * @param {bool} isGuestUser
 */
function* triggerNavigateXHRAPICall(isGuestUser) {
  if (isGuestUser) {
    yield put(navigateXHRAction());
  }
}

export function* fetchCoupons(isCouponAppliedInOrder) {
  if (isCouponAppliedInOrder) {
    yield put(getCouponList({ ignoreCache: true }));
  }
}

// method to handle submit of order in review page
function* submitOrderForProcessing({ payload: { navigation, formData } }) {
  try {
    const orderId = yield select(getCurrentOrderId);
    const smsOrderInfo = yield select(getSmsNumberForBillingOrderUpdates);
    const currentLanguage = yield select(getCurrentLanguage);
    const isExpressCheckoutEnabled = yield select(isExpressCheckout);
    const pendingPromises = [];
    yield put(setLoaderState(true));
    if (isExpressCheckoutEnabled && formData) {
      yield call(expressCheckoutSubmit, formData);
    }

    yield all(pendingPromises);
    const productCount = yield select(BagPageSelectors.getProductsTypes);
    const res = yield call(submitOrderProcessing, orderId, smsOrderInfo, currentLanguage);
    // setting up current checkout stage.
    yield put(getSetCheckoutStage(constants.CHECKOUT_STAGES.CONFIRMATION));
    if (!isMobileApp()) {
      utility.routeToPage(CHECKOUT_ROUTES.confirmationPage, { fromReview: 'true' });
    } else if (navigation) {
      navigation.navigate(constants.CHECKOUT_ROUTES_NAMES.CHECKOUT_CONFIRMATION);
    }
    yield call(loadPersonalizedCoupons, res, orderId);
    const cartItems = yield select(BagPageSelectors.getOrderItems);
    const email = res.userDetails ? res.userDetails.emailAddress : res.shipping.emailAddress;
    const isCaSite = yield call(isCanada);
    const isGuestUser = yield select(isGuest);
    /* istanbul ignore else */
    const isCouponAppliedInOrder = yield select(isCouponApplied);
    // const vendorId =
    //   cartItems.size > 0 && cartItems.getIn(['0', 'miscInfo', 'vendorColorDisplayId']);
    yield put(getSetOrderProductDetails(cartItems));
    yield put(resetAirmilesReducer());
    yield put(resetCouponReducer());
    yield call(fetchCoupons, isCouponAppliedInOrder);
    yield put(setLoaderState(false));
    yield put(setCouponLoadingState(false));
    if (!isGuestUser) {
      yield call(getUserInfoSaga, { payload: { reloadUserInfo: true } });
    }
    if (isGuestUser && !isCaSite && email) {
      const { tcpProducts, gymProducts, snjProducts } = productCount;
      yield call(
        validateAndSubmitEmailSignup,
        email,
        'us_guest_checkout',
        tcpProducts.length > 0 || isTCP(),
        gymProducts.length > 0 || isGymboree(),
        snjProducts.length > 0 || isSNJ()
      );
    }
    yield call(triggerNavigateXHRAPICall, isGuestUser);
  } catch (e) {
    logger.error(`Error in submitOrderForProcessing: ${e}`);
    yield put(setCouponLoadingState(false));
    const errorsMapping = yield select(BagPageSelectors.getErrorMapping);
    const billingError = getServerErrorMessage(e, errorsMapping);
    yield put(setLoaderState(false));
    yield put(
      CHECKOUT_ACTIONS.setServerErrorCheckout({ errorMessage: billingError, component: 'PAGE' })
    );
  }
}

export default submitOrderForProcessing;
