// 9fbef606107a605d69c0edbcd8029e5d 
import { withRouter } from 'next/router'; // eslint-disable-line
import hoistNonReactStatic from 'hoist-non-react-statics';
import CheckoutCommonContainer from './CheckoutCommonContainer';

const CheckoutWithRouterWrapper = withRouter(CheckoutCommonContainer);

hoistNonReactStatic(CheckoutWithRouterWrapper, CheckoutCommonContainer, {
  getInitialProps: true,
});

export default CheckoutWithRouterWrapper;
