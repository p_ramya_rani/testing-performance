/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { submit } from 'redux-form';
import {
  setClickAnalyticsData,
  trackClick,
  updatePageData,
  trackPageView,
  resetClickAnalyticsData,
} from '@tcp/core/src/analytics/actions';
import {
  getEddData,
  resetEddData,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.actions';
import {
  getEddFeatureSwitch,
  getEddAllowedStates,
  getItemsWithEDD,
  getOrderEddDates,
  getSelectedShippingCode,
  getEddABState,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.selectors';
import { getPageName } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { getBagAnalyticsData } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import {
  getAutocompleteTypesParam,
  getMapboxSwitch,
  getAfterPayMinOrderAmount,
  getAfterPayMaxOrderAmount,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';

import { setConfirmationDone } from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.actions';

import OrderSelectors from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.selectors';
import { getCurrentCurrency } from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import CHECKOUT_ACTIONS, {
  submitShippingSection,
  submitPickupSection,
  onEditModeChangeAction,
  fetchShipmentMethods,
  routeToPickupPage as routeToPickupPageActn,
  getSetCheckoutStage,
  updateShipmentMethodSelection,
  updateShippingAddress,
  addNewShippingAddress,
  submitBillingSection,
  submitReviewSection,
  setVenmoPickupMessageState,
  setVenmoShippingMessageState,
  submitVerifiedAddressData,
  getSetIsBillingVisitedActn,
  getZipCodeSuggestedAddress,
  callPersistEddNode,
  setApplePaymentInProgress,
  setAfterPayInProgress,
  setCouponRemovedFromOrder,
} from './Checkout.action';
import { setUpdateFromMSG, toggleCheckoutFocus } from './Checkout.action.util';
import selectors, {
  isGuest as isGuestUser,
  isExpressCheckout,
  getAlternateFormUpdate,
  getSendOrderUpdate,
  getCheckoutStage,
  getGiftServicesSend,
  isUsSite as isUsSiteUser,
  getPickupAltValues,
  isPickupAlt,
  getPickupValues,
  getPageData,
  getABTestForZipCode,
  getZipCodeLoader,
  isSfsInvMessagingEnabled,
  getCouponRemovedFromOrderFlag,
} from './Checkout.selector';
import confirmationSelector from '../../Confirmation/container/Confirmation.selectors';
import { getAddEditAddressLabels } from '../../../../common/organisms/AddEditAddress/container/AddEditAddress.selectors';
import BillingSectionSelectors from '../organisms/ReviewPage/organisms/BillingSection/container/BillingSection.selectors';
import BagPageSelector from '../../BagPage/container/BagPage.selectors';
import { getBonusPointsSwitch } from '../../../../common/organisms/BonusPointsDays/container/BonusPointsDays.selectors';
import {
  getUserPhoneNumber,
  getIsRegisteredUserCallDone,
  isRememberedUser,
  getUserId,
  isPlccUser,
} from '../../../account/User/container/User.selectors';
import {
  getAppliedExpiredCouponListState,
  getAllCoupons,
  isCouponApplied,
} from '../../common/organism/CouponAndPromos/container/Coupon.selectors';
import BAG_PAGE_ACTIONS from '../../BagPage/container/BagPage.actions';
import { toastMessageInfo } from '../../../../common/atoms/Toast/container/Toast.actions';
import { verifyAddress } from '../../../../common/organisms/AddressVerification/container/AddressVerification.actions';
import { getCVVCodeInfoContentInfo } from '../organisms/BillingPage/container/BillingPage.selectors';
import constants from '../Checkout.constants';
import { getPayPalFlag } from '../util/utility';
import { isMobileApp, isCanada } from '../../../../../utils';
import { getCartItemCount } from '../../../../../utils/cookie.util';
import GiftCardSelector from '../organisms/GiftCardsSection/container/GiftCards.selectors';
import { getCardListFetchingState } from '../../../account/Payment/container/Payment.selectors';
import SMSNotificationSelectors from '../../Confirmation/organisms/SMSNotifications/container/SMSNotifications.selectors';
import { getInitialGiftWrapOptions } from '../organisms/ShippingPage/molecules/GiftServices/container/GiftServices.selector';
import {
  getGrandTotal,
  getGiftCardsTotal,
} from '../../common/organism/OrderLedger/container/orderLedger.selector';
import PlaceCashSelector from '../../PlaceCashBanner/container/PlaceCashBanner.selectors';

const {
  getSmsSignUpLabels,
  getSelectedShipmentId,
  getAddressFields,
  getShippingPhoneNo,
  getIsOrderHasPickup,
  getIsOrderHasShipping,
  getEmailSignUpLabels,
  getShipmentMethods,
  getDefaultShipmentID,
  getShippingSendOrderUpdate,
  getSaveToAddressBook,
  getOnFileAddressKey,
  getShippingAddressID,
  getDefaultShipping,
  getAddEditResponseAddressId,
  getShippingAddress,
  getCheckoutProgressBarLabels,
  getSyncError,
  getGiftWrappingValues,
  getReviewLabels,
  getShippingPhoneAndEmail,
  getShipmentLoadingStatus,
  getCurrentCheckoutStage,
  getShippingAddressList,
  getIsPaymentDisabled,
  getCheckoutPageEmptyBagLabels,
  getSelectedShippingMethodDetails,
  getGlobalLabels,
} = selectors;

export const formatPayload = (payload) => {
  const { addressLine1, addressLine2, zipCode, ...otherPayload } = payload || {};
  return {
    ...otherPayload,
    ...{
      address1: addressLine1,
      address2: addressLine2,
      zip: zipCode,
    },
  };
};

// eslint-disable-next-line sonarjs/cognitive-complexity
export const intiSectionPage = (pageName, scope, extraProps = {}) => {
  const scopeValue = scope;
  const {
    initCheckoutSectionPage,
    router,
    isRegisteredUserCallDone,
    navigation,
    couponsAndPromos,
    plccUser,
    couponApplied,
  } = scope.props;
  let recalc;
  let isPaypalPostBack;
  let appRouting;
  let fromATB = false;
  if (router && router.query) {
    ({ recalc, isPaypalPostBack, appRouting, fromATB } = router.query);
    if (typeof fromATB === 'string') fromATB = fromATB === 'true';
  }

  let isServer = true;
  let isGetCoupon;
  if (isMobileApp()) {
    isPaypalPostBack = getPayPalFlag(navigation);
  } else {
    ({
      components: {
        '/Checkout': {
          props: { isServer = false },
        },
      },
    } = router);
    if (isServer) {
      isGetCoupon = getCartItemCount() !== 0;
    } else if (
      (router &&
        router.query &&
        router.query.section === 'shipping' &&
        !couponsAndPromos.size > 0) ||
      couponApplied
    ) {
      isGetCoupon = true;
    }
    if (!isServer && !couponApplied) isGetCoupon = false;
  }
  if (isRegisteredUserCallDone || isMobileApp()) {
    initCheckoutSectionPage({
      pageName,
      recalc,
      isPaypalPostBack,
      initialLoad: scopeValue.initialLoad,
      appRouting,
      navigation,
      isServer,
      fromATB,
      isGetCoupon,
      plccUser,
      ...extraProps,
    });
    scopeValue.initialLoad = false;
  }
};

/* istanbul ignore next */
export const mapDispatchToProps = (dispatch) => {
  return {
    initCheckout: (router, isPaypalFlow, navigation) => {
      dispatch(CHECKOUT_ACTIONS.initCheckoutAction(router, isPaypalFlow, navigation));
    },
    initCheckoutSectionPage: (payload) => {
      dispatch(CHECKOUT_ACTIONS.initCheckoutSectionPageAction(payload));
    },
    submitShipping: (payload) => {
      dispatch(submitShippingSection(payload));
    },
    onPickupSubmit: (data) => dispatch(submitPickupSection(data)),
    onEditModeChange: (data) => {
      dispatch(onEditModeChangeAction(data));
    },
    loadShipmentMethods: (formName) => {
      dispatch(fetchShipmentMethods(formName));
    },
    routeToPickupPage: () => {
      dispatch(routeToPickupPageActn());
    },
    setCheckoutStage: (payload) => {
      dispatch(getSetCheckoutStage(payload));
    },
    updateShippingMethodSelection: (payload) => {
      dispatch(updateShipmentMethodSelection(payload));
    },
    updateShippingAddressData: (payload, afterUpdateAddress) => {
      dispatch(updateShippingAddress(payload, afterUpdateAddress));
    },
    addNewShippingAddressData: (payload) => {
      dispatch(addNewShippingAddress(payload));
    },
    submitBilling: (payload) => dispatch(submitBillingSection(payload)),
    fetchNeedHelpContent: (contentIds) => dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds)),
    markBagPageRoutingDone: () => dispatch(BAG_PAGE_ACTIONS.setBagPageIsRouting(false)),
    submitReview: (payload) => dispatch(submitReviewSection(payload)),
    verifyAddressAction: (payload) => dispatch(verifyAddress(payload)),
    dispatchReviewReduxForm: () => dispatch(submit(constants.REVIEW_FORM_NAME)),
    submitVerifiedShippingAddressData: (payload) => {
      dispatch(submitVerifiedAddressData(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    setVenmoPickupState: (data) => {
      if (!isMobileApp()) dispatch(setVenmoPickupMessageState(data));
    },
    setVenmoShippingState: (data) => {
      if (!isMobileApp()) dispatch(setVenmoShippingMessageState(data));
    },
    clearCheckoutServerError: (data) => dispatch(CHECKOUT_ACTIONS.setServerErrorCheckout(data)),
    toggleCountrySelector: (payload) => {
      dispatch(CHECKOUT_ACTIONS.toggleCountrySelectorModal(payload));
    },
    setClickAnalyticsDataCheckout: (payload) => {
      dispatch(setClickAnalyticsData(payload));
    },
    trackClickAnalytics: (payload) => {
      dispatch(trackClick(payload));
    },
    trackClickAfterPay: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 250);
    },
    updateCheckoutPageData: (payload) => {
      dispatch(updatePageData(payload));
    },
    clearIsBillingVisitedState: () => {
      dispatch(getSetIsBillingVisitedActn(false));
    },
    updateFromMSG: (value) => {
      dispatch(setUpdateFromMSG(value));
    },
    cartLoading: () => {
      dispatch(BAG_PAGE_ACTIONS.setBagPageLoading());
    },
    resetAnalyticsData: () => {
      dispatch(resetClickAnalyticsData());
    },
    trackPageViewCheckout: (payload) => {
      dispatch(trackPageView(payload));
    },
    resetCartCheckoutData: () => {
      dispatch(CHECKOUT_ACTIONS.resetCheckoutReducer());
      dispatch(BAG_PAGE_ACTIONS.resetCartReducer());
    },
    setConfirmationDoneActn: (payload) => {
      dispatch(setConfirmationDone(payload));
    },
    toggleCheckoutFocus: (payload) => {
      dispatch(toggleCheckoutFocus(payload));
    },
    getZipCodeSuggestedAddress: (payload) => {
      dispatch(getZipCodeSuggestedAddress(payload));
    },
    updateEdd: (payload) => {
      dispatch(getEddData(payload));
    },
    persistEddNodeAction: (payload) => {
      dispatch(callPersistEddNode(payload));
    },
    resetShippingEddData: () => {
      dispatch(resetEddData());
    },
    addItemToSflList: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.addItemToSflList(payload, null));
    },
    setAppleProgress: (data) => dispatch(setApplePaymentInProgress(data)),
    setCouponMergeErrorAction: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.setCouponMergeError(payload));
    },
    setMaxItemErrorAction: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.setMaxItemError(payload));
    },
    setMaxItemErrorDisplayedAction: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.setMaxItemErrorDisplayed(payload));
    },
    setAfterPayProgress: (payload) => {
      dispatch(setAfterPayInProgress(payload));
    },
    setCouponRemovedFromOrderFlag: (payload) => {
      dispatch(setCouponRemovedFromOrder(payload));
    },
    dispatch,
  };
};

/* istanbul ignore next */
export const mapStateToProps = (state) => {
  const giftWrap = getInitialGiftWrapOptions(state);
  const hasSetGiftOptions = giftWrap && giftWrap.size;
  const shippingAddress = getShippingAddress(state);
  return {
    couponApplied: isCouponApplied(state),
    plccUser: isPlccUser(state),
    initialValues: selectors.getPickupInitialPickupSectionValues(state),
    checkoutRoutingDone: selectors.getIfCheckoutRoutingDone(state),
    pickupInitialValues: selectors.getPickupInitialPickupSectionValues(state),
    isSmsUpdatesEnabled: selectors.isSmsUpdatesEnabled(state),
    currentPhoneNumber: selectors.getCurrentPickupFormNumber(state),
    isGuest: isGuestUser(state),
    isMobile: selectors.getIsMobile(),
    isExpressCheckoutPage: isExpressCheckout(state),
    activeStage: getCheckoutStage(state),
    shippingMethod: getDefaultShipmentID(state),
    shippingMethodSelected: getSelectedShippingMethodDetails(state),
    checkoutPageEmptyBagLabels: getCheckoutPageEmptyBagLabels(state),
    emailSignUpFlags: BagPageSelector.getIfEmailSignUpDone(state),
    appliedExpiredCoupon: getAppliedExpiredCouponListState(state),
    couponsAndPromos: getAllCoupons(state),
    pageName: getPageName(state),
    shippingProps: {
      isSubmitting: getShipmentLoadingStatus(state),
      addressLabels: getAddEditAddressLabels(state),
      isOrderUpdateChecked: getShippingSendOrderUpdate(state),
      isGiftServicesChecked: getGiftWrappingValues(state),
      isGiftOptionsEnabled: selectors.isGiftOptionsEnabled(state),
      smsSignUpLabels: getSmsSignUpLabels(state),
      selectedShipmentId: getSelectedShipmentId(state), // selected shipment radio button
      address: getAddressFields(state), // address for fields data
      addressPhoneNumber: getShippingPhoneNo(state), // phone field inside address for section
      emailSignUpLabels: getEmailSignUpLabels(state),
      shipmentMethods: getShipmentMethods(state), // all the shipment methods from api
      defaultShipmentId: getDefaultShipmentID(state), // default shipment to be shown as selected
      isSaveToAddressBookChecked: getSaveToAddressBook(state),
      userAddresses: getShippingAddressList(state),
      onFileAddressKey: getOnFileAddressKey(state), // selected address Id in dropdown
      newUserPhoneNo: getUserPhoneNumber(state), // newly added user phone number to be shown as default in mobile number field in address form
      shippingAddressId: getShippingAddressID(state), // address user has selected should be shown as selected in dropdown, not the default address
      setAsDefaultShipping: getDefaultShipping(state),
      addEditResponseAddressId: getAddEditResponseAddressId(state),
      syncErrors: getSyncError(state),
      shippingPhoneAndEmail: getShippingPhoneAndEmail(state),
      isLoadingShippingMethods: GiftCardSelector.getIsLoading(state),
      isFetching: getCardListFetchingState(state),
      bagLoading: BagPageSelector.isBagLoading(state),
      initCheckoutFinished: selectors.getIfInitCheckoutFinished(state),
      hasSetGiftOptions,
      shippingAddress,
    },
    confirmationPageLedgerSummaryData: confirmationSelector.getLedgerSummaryDataConfirmation(state),
    activeStep: getCheckoutStage(state),
    //  isPlccOfferModalOpen: generalStoreView.getOpenModalId(state) === MODAL_IDS.plccPromoModalId,
    // isPlccFormModalOpen: generalStoreView.getOpenModalId(state) === MODAL_IDS.plccFormModalId,
    isUsSite: isUsSiteUser(),
    // shouldSkipBillingStep: storeOperators.checkoutOperator.shouldSkipBillingStep(),
    orderHasPickUp: getIsOrderHasPickup(state),
    orderHasShipping: getIsOrderHasShipping(state),
    pickUpLabels: {
      ...selectors.getPickUpContactFormLabels(state),
      ...getEmailSignUpLabels(state),
    },
    smsSignUpLabels: getSmsSignUpLabels(state),
    isBagLoaded: BagPageSelector.isBagLoaded(state),
    isOrderUpdateChecked: getSendOrderUpdate(state),
    isGiftServicesChecked: getGiftServicesSend(state),
    isAlternateUpdateChecked: getAlternateFormUpdate(state),
    cartOrderItems: BagPageSelector.getOrderItems(state),
    cartOrderItemsCount: BagPageSelector.getTotalItems(state) || getCartItemCount(),
    labels: selectors.getLabels(state),
    checkoutProgressBarLabels: getCheckoutProgressBarLabels(state),
    reviewProps: {
      labels: getReviewLabels(state),
      isPaymentDisabled: getIsPaymentDisabled(state),
      defaultShipmentId: getDefaultShipmentID(state),
      cardType: selectors.getCardType(state),
      isFetching: getCardListFetchingState(state),
      bagLoading: BagPageSelector.isBagLoading(state),
      submitOrderRichText: selectors.getSubmitOrderRichTextSelector(state),
      globalLabels: getGlobalLabels(state),
      isSfsInvMessagingEnabled: isSfsInvMessagingEnabled(state),
      addressLabels: getAddEditAddressLabels(state),
      shippingAddress,
      expressReviewShippingSectionId: selectors.getExpressReviewShippingSectionId(state),
      afterPayInProgress: selectors.getIsAfterPayInProgress(state),
      afterPayMinOrderAmount: getAfterPayMinOrderAmount(state),
      afterPayMaxOrderAmount: getAfterPayMaxOrderAmount(state),
    },
    isVenmoPaymentInProgress: selectors.isVenmoPaymentInProgress(state),
    isApplePaymentInProgress: selectors.isApplePaymentInProgress(state),
    getPayPalSettings: selectors.getPayPalSettings(state),
    checkoutServerError: selectors.getCheckoutServerError(state),
    isRegisteredUserCallDone: getIsRegisteredUserCallDone(state),
    rememberedUserFlag: isRememberedUser(state),
    currentUserId: getUserId(state),
    currentStage: getCurrentCheckoutStage(state) || '',
    pickUpAlternatePerson: getPickupAltValues(state),
    bagLoading: BagPageSelector.isBagLoading(state),
    isHasPickUpAlternatePerson: isPickupAlt(state),
    pickUpContactPerson: getPickupValues(state),
    pickUpContactAlternate: selectors.getPickupInitialPickupSectionValues(state),
    cvvCodeInfoContentInfo: getCVVCodeInfoContentInfo(state),
    isRTPSFlow: selectors.getIsRtpsFlow(state),
    isPayPalWebViewEnable: BagPageSelector.getPayPalWebViewStatus(state),
    pageData: getPageData(state),
    notificationMsgContentInfo: SMSNotificationSelectors.getNotificationMsgContentInfo(state),
    subscribeSuccessMsgContentInfo:
      SMSNotificationSelectors.getSubscribeSuccessMsgContentInfo(state),
    isVenmoPickupBannerDisplayed: selectors.isVenmoPickupBannerDisplayed(state),
    isVenmoShippingBannerDisplayed: selectors.isVenmoShippingBannerDisplayed(state),
    currentOrderId: selectors.getCurrentOrderId(state),
    paymentMethodId: BagPageSelector.getBillingPaymentMethod(state),
    orderSubTotal: BagPageSelector.getOrderSubTotal(state),
    billingAddress: selectors.getBillingAddressFields(state),
    isBonusPointsEnabled: getBonusPointsSwitch(state),
    titleLabel: BillingSectionSelectors.getReviewPageLabels(state),
    initShippingPage: selectors.getShippingInitialSectionValues(state),
    smsOrderUpdatesId: selectors.getSMSOrderUpdatesId(state),
    smsOrderUpdatesRichText: selectors.getSMSOrderUpdatesRichTextSelector(state),
    submitOrderContentId: selectors.getSubmitOrderContentId(state),
    emailSignUpCaContentId: selectors.getEmailSignUpCaId(state),
    emailSignUpCaRichText: selectors.getEmailSignUpCaRichTextSelector(state),
    orderBalanceTotal: getGrandTotal(state) - getGiftCardsTotal(state),
    excludedIds: confirmationSelector.getProductExcludeIds(state),
    partNumber: confirmationSelector.getExpensiveItem(state),
    grandTotal: getGrandTotal(state),
    giftCardTotal: getGiftCardsTotal(state),
    airmilesBannerContentId: PlaceCashSelector.getAirmilesBannerContentInfo(state),
    zipCodeABTestEnabled: getABTestForZipCode(state),
    zipCodeLoader: getZipCodeLoader(state),
    itemsWithEdd: getItemsWithEDD(state),
    orderEddDates: getOrderEddDates(state),
    selectedShippingCode: getSelectedShippingCode(state),
    isEDD: getEddFeatureSwitch(state),
    eddAllowedStatesArr: getEddAllowedStates(state),
    isEddABTest: getEddABState(state),
    currency: getCurrentCurrency(state),
    initCheckoutFinished: selectors.getIfInitCheckoutFinished(state),
    mapboxAutocompleteTypesParam: getAutocompleteTypesParam(state),
    mapboxSwitch: getMapboxSwitch(state),
    showMaxItemError: BagPageSelector.getShowMaxItemError(state),
    showMaxItemErrorDisplayed: BagPageSelector.getShowMaxItemErrorDisplayed(state),
    cartAnalyticsData: getBagAnalyticsData(state),
    mixOrderDetails: confirmationSelector.getMixOrderDetails(state),
    orderConfirmationStatus: OrderSelectors.getOrderConfimationStatus(state),
    deviceId: (state && state.APIConfig && state.APIConfig.deviceID) || '',
    couponRemovedFromOrderFlag: getCouponRemovedFromOrderFlag(state),
  };
};

export const callNeedHelpContent = (props) => {
  const {
    fetchNeedHelpContent,
    cvvCodeInfoContentInfo,
    notificationMsgContentInfo,
    subscribeSuccessMsgContentInfo,
    smsOrderUpdatesId,
    submitOrderContentId,
    emailSignUpCaContentId,
    airmilesBannerContentId,
  } = props;

  const needHelpContents = [
    cvvCodeInfoContentInfo,
    notificationMsgContentInfo,
    subscribeSuccessMsgContentInfo,
    smsOrderUpdatesId,
    submitOrderContentId,
    emailSignUpCaContentId,
  ];

  if (isCanada()) {
    needHelpContents.push(airmilesBannerContentId);
  }

  fetchNeedHelpContent(needHelpContents);
};
