// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import { call, put, select } from 'redux-saga/effects';
import {
  setLoaderState,
  setSectionLoaderState,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { trackFormError } from '@tcp/core/src/analytics/actions';
import CONSTANTS, { CHECKOUT_ROUTES } from '../Checkout.constants';
import selectors, { isGuest, isSfsInvMessagingEnabled } from './Checkout.selector';
import { getUserEmail } from '../../../account/User/container/User.selectors';
import utility from '../util/utility';
import CHECKOUT_ACTIONS, { setShippingLoadingState, getSetCheckoutStage } from './Checkout.action';
import { isCanada } from '../../../../../utils/utils';
import { redirectToBilling } from './Checkout.saga.util';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import { getServerErrorMessage } from '../../../../../services/abstractors/CnC/index';
import { isMobileApp } from '../../../../../utils';

function* handleBillingError(err) {
  const errorsMapping = yield select(BagPageSelectors.getErrorMapping);
  const billingError = getServerErrorMessage(err, errorsMapping);
  let setServerErrorPayload = { errorMessage: billingError, component: 'PAGE' };
  if (isSfsInvMessagingEnabled && billingError.errorMessage) {
    setServerErrorPayload.errorMessage = '';
    setServerErrorPayload = { ...setServerErrorPayload, billingError };
  }
  yield put(CHECKOUT_ACTIONS.setServerErrorCheckout(setServerErrorPayload));
}

export function* submitShippingSectionData({ payload: { navigation, ...formData } }, callback) {
  try {
    yield put(setShippingLoadingState(true));
    if (isMobileApp) {
      yield put(setSectionLoaderState({ shippingLoader: true, section: 'shippingLoader' }));
    } else {
      yield put(setLoaderState(true));
    }
    const { method, smsInfo, shipTo, emailSignUp, hasSetGiftOptions } = formData;
    let {
      shipTo: { emailAddress },
    } = formData;
    const isCanadaUser = yield select(isCanada);
    const isGuestUser = yield select(isGuest);
    const isEmailSignUpAllowed = isCanadaUser && isGuestUser;
    if (!emailAddress || !isGuestUser) {
      // on registered user entering a new address the email field is not visible -> emailAddress = null
      emailAddress = yield select(getUserEmail);
    }
    const { getRecalcValueFromGiftServices } = selectors;
    const isRecalculate = yield select(getRecalcValueFromGiftServices);
    if (callback) {
      yield callback({
        ...shipTo,
        method,
        smsInfo,
        isEmailSignUpAllowed,
        emailAddress,
        isGuestUser,
        emailSignUp,
        hasSetGiftOptions,
      });
    }
    const isVenmoInProgress = yield select(selectors.isVenmoPaymentInProgress);
    const isVenmoShippingDisplayed = yield select(selectors.isVenmoShippingBannerDisplayed);
    if (isVenmoInProgress && !isVenmoShippingDisplayed) {
      if (!isMobileApp()) {
        utility.routeToPage(CHECKOUT_ROUTES.reviewPage, { recalc: isRecalculate });
      } else if (navigation) {
        yield put(getSetCheckoutStage(CONSTANTS.REVIEW_DEFAULT_PARAM));
      }
    } else {
      yield call(redirectToBilling, isRecalculate);
    }
    yield put(setShippingLoadingState(false));
    yield put(setLoaderState(false));
    yield put(CHECKOUT_ACTIONS.setServerErrorCheckout({}));
    yield put(setSectionLoaderState({ shippingLoader: false, section: 'shippingLoader' }));
  } catch (err) {
    const { shipTo, method, smsInfo } = formData;
    const {
      addressLine1,
      zipCode,
      city,
      lastName,
      firstName,
      addressLine2,
      phoneNumber,
      emailAddress,
      country,
      state,
    } = shipTo.address;
    const { shippingMethodId } = method;
    const { smsUpdateNumber } = smsInfo;
    const requiredList3Val = {
      'address.firstName': firstName,
      'address.lastName': lastName,
      'address.addressLine1': addressLine1,
      'address.addressLine2': addressLine2,
      'address.city': city,
      'address.state': state,
      'address.zipCode': zipCode,
      'address.phoneNumber': phoneNumber,
      'address.country': country,
      'address.emailAddress': emailAddress,
      'smsSignUp.phoneNumber': smsUpdateNumber,
      'shipmentMethods.shippingMethodId': shippingMethodId,
    };

    yield put(trackFormError({ formName: 'checkoutShipping', formData: requiredList3Val }));

    yield put(setShippingLoadingState(false));
    yield put(setLoaderState(false));
    yield put(setSectionLoaderState({ shippingLoader: false, section: 'shippingLoader' }));
    // throw getSubmissionError(store, 'submitShippingSection', err);
    yield call(handleBillingError, err);
  } finally {
    yield put(setShippingLoadingState(false));
    yield put(setLoaderState(false));
  }
}

export function* submitVerifiedAddressData(
  { payload: { submitData, shippingAddress, navigation } },
  callback
) {
  const {
    address1: addressLine1,
    address2: addressLine2,
    zip: zipCode,
    ...restAddressData
  } = shippingAddress;
  const payloadData = submitData;
  const shipAddress = {
    ...payloadData.shipTo.address,
    addressLine1,
    addressLine2,
    zipCode,
    ...restAddressData,
  };
  payloadData.shipTo.address = shipAddress;
  payloadData.shipTo.phoneNumber = shipAddress.phoneNumber;
  yield call(submitShippingSectionData, { payload: { ...payloadData, navigation } }, callback);
}
