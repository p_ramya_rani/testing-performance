/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { put, takeLatest, call, select } from 'redux-saga/effects';
// import { validateReduxCache } from '../../../../../../utils/cache.util';
import {
  setLoaderState,
  setSectionLoaderState,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import { isMobileApp } from '@tcp/core/src/utils';
import { getFavoriteStoreActn } from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import { openPickupModalWithValues } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import CHECKOUT_ACTIONS from '../../Checkout/container/Checkout.action';
import {
  getIsPdpServiceEnabled,
  getABTestPdpService,
} from '../../../../../reduxStore/selectors/session.selectors';
import { getUserId } from '../../../account/User/container/User.selectors';
import { getProductSkuInfoByUnbxd } from '../../../../../services/abstractors/CnC';

import {
  CartPageSaga,
  confirmRemoveItem,
  confirmRemoveItemInChannel,
  removeCartItem,
  updateCartItemSaga,
  getProductSKUInfoSaga,
  afterRemovingCartItem,
  openPickupModalFromBag,
  makeCartApiCall,
  updateSagaErrorActions,
  setUpdateItemErrorMessages,
  getFavoriteStoreCall,
  fetchProductId,
} from '../container/CartItemTile.saga';
import { getUserInfoSaga } from '../../../account/User/container/User.saga';
import {
  removeCartItemComplete,
  setToggleCartItemError,
  setBossBopisToggleCartItemError,
  setProductMultiPackData,
  clearToggleCartItemError,
  getProductSKUInfoSuccess,
  setCartLoadingState,
} from '../container/CartItemTile.actions';
import CARTPAGE_CONSTANTS from '../CartItemTile.constants';
import BAG_PAGE_ACTIONS from '../../BagPage/container/BagPage.actions';
import {
  AddToPickupError,
  AddToCartError,
  clearAddToBagErrorState,
  clearAddToPickupErrorState,
} from '../../AddedToBag/container/AddedToBag.actions';
import CartItemTileSagaUtils from '../container/CartItemTile.saga.utils';

describe('Cart Item saga remove functionality', () => {
  it('should dispatch confirmRemoveItem action in success resposnse', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
      },
      isMiniBag: false,
    };
    const removeCartItemGen = confirmRemoveItem(payload);
    removeCartItemGen.next();
    removeCartItemGen.next();

    const res = {
      orderId: '3000284778',
      x_orderTotal: '49.35000',
    };
    const putDescriptor = removeCartItemGen.next(res).value;
    expect(putDescriptor).toEqual(put(removeCartItemComplete(res)));
  });
  it('should dispatch confirmRemoveItem action for success resposnse', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
      },
      isMiniBag: true,
    };
    const removeCartItemGen = confirmRemoveItem(payload);
    expect(removeCartItemGen.next().value).toEqual(
      put(setSectionLoaderState({ miniBagLoaderState: true, section: 'minibag' }))
    );
  });

  it('should dispatch confirmRemoveItem action for myBag', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
        pageView: 'myBag',
      },
      isMiniBag: false,
    };
    const removeCartItemGen = confirmRemoveItem(payload);
    expect(removeCartItemGen.next().value).toEqual(put(setLoaderState(true)));
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    expect(removeCartItemGen.next().value).toEqual(put(setLoaderState(false)));
    expect(removeCartItemGen.next().value).toEqual(
      put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }))
    );

    expect(removeCartItemGen.next().value).toEqual(
      put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isDeleting: false }))
    );
    expect(removeCartItemGen.next().value).toEqual(put(BAG_PAGE_ACTIONS.setCartItemsSFL(false)));
    expect(removeCartItemGen.next().done).toBeTruthy();
  });
  it('should dispatch makeRegisteredUserCall', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
        pageView: 'myBag',
      },
      isMiniBag: false,
    };
    const removeCartItemGen = confirmRemoveItem(payload);
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    expect(removeCartItemGen.next().done).toBeTruthy();
  });
});
describe('confirmRemoveItem', () => {
  try {
    it('should throw error', () => {
      const payload = {
        payload: {
          itemId: '',
        },
        isMiniBag: false,
        isMoveToSFL: false,
        userId: '',
      };
      const res = {
        orderId: '3000284778',
        x_orderTotal: '49.35000',
      };
      const removeCartItemGen = confirmRemoveItem(payload);
      removeCartItemGen.next();
      removeCartItemGen.next();
      expect(removeCartItemGen.next().value).toEqual(put(removeCartItemComplete(res)));
      expect(removeCartItemGen.next().value).toEqual(put(BAG_PAGE_ACTIONS.removeDeletedItem()));
      expect(removeCartItemGen.next().value).toEqual(select(getUserId));
      expect(removeCartItemGen.next().value).toEqual(call(getUserInfoSaga));
      removeCartItemGen.next();
      expect(removeCartItemGen.next().value).toEqual(put(setLoaderState(false)));
      expect(removeCartItemGen.next().value).toEqual(
        put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }))
      );
      removeCartItemGen.next();
      removeCartItemGen.next();
      removeCartItemGen.next();
      expect(removeCartItemGen.next().done).toBeTruthy();
    });
  } catch (err) {
    console.log('error in confirmRemoveItem', err);
  }
});

describe('Cart Item saga remove using confirmRemoveItemInChannel', () => {
  it('should dispatch confirmRemoveItem action for success', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
      },
    };
    const removeCartItemGen = confirmRemoveItemInChannel(payload);
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    const res = {
      orderId: '3000284778',
      orderItems: [payload],
    };
    const putDescriptor = removeCartItemGen.next(res).value;
    expect(putDescriptor).toEqual(put(removeCartItemComplete(res)));
  });
  it('should dispatch confirmRemoveItem action for success resposnse', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
      },
      isMiniBag: true,
    };
    const removeCartItemGen = confirmRemoveItemInChannel(payload);
    expect(removeCartItemGen.next().value).toEqual(put(setCartLoadingState(true)));
  });

  it('should dispatch confirmRemoveItem action for myBag', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
        pageView: 'myBag',
      },
      isMiniBag: false,
    };
    const removeCartItemGen = confirmRemoveItemInChannel(payload);
    expect(removeCartItemGen.next().value).toEqual(put(setCartLoadingState(true)));
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    expect(removeCartItemGen.next().value).toEqual(put(setCartLoadingState(false)));

    expect(removeCartItemGen.next().value).toEqual(
      put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isDeleting: false }))
    );
    expect(removeCartItemGen.next().value).toEqual(put(BAG_PAGE_ACTIONS.setCartItemsSFL(false)));
    expect(removeCartItemGen.next().done).toBeTruthy();
  });
  it('should dispatch makeRegisteredUserCall', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
        pageView: 'myBag',
      },
      isMiniBag: false,
    };
    const removeCartItemGen = confirmRemoveItemInChannel(payload);
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    expect(removeCartItemGen.next().done).toBeTruthy();
  });
});

describe('confirmRemoveItem', () => {
  try {
    it('should throw error', () => {
      const payload = {
        payload: {
          itemId: '',
        },
        isMiniBag: false,
        isMoveToSFL: false,
        userId: '',
      };
      const res = {
        orderId: '3000284778',
        orderItems: [payload],
      };
      const removeCartItemGen = confirmRemoveItemInChannel(payload);
      expect(removeCartItemGen.next().value).toEqual(put(setCartLoadingState(true)));
      removeCartItemGen.next();
      removeCartItemGen.next();
      expect(removeCartItemGen.next().value).toEqual(put(removeCartItemComplete(res)));
      removeCartItemGen.next();
      expect(removeCartItemGen.next().value).toEqual(select(getUserId));
      expect(removeCartItemGen.next().value).toEqual(call(getUserInfoSaga));
      removeCartItemGen.next();
      expect(removeCartItemGen.next().value).toEqual(put(setCartLoadingState(false)));
      removeCartItemGen.next();
      removeCartItemGen.next();
      removeCartItemGen.next();
      expect(removeCartItemGen.next().done).toBeTruthy();
    });
  } catch (err) {
    console.log('error in confirmRemoveItem', err);
  }
});

it('should dispatch afterRemovingCartItem action for success resposnse', () => {
  const removeCartItemGen = afterRemovingCartItem();

  const putDescriptor = removeCartItemGen.next().value;
  expect(putDescriptor).toEqual(put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isDeleting: true })));
});
it('should not  dispatch afterRemovingCartItem action', () => {
  const isMoveToSFL = false;
  const removeCartItemGen = afterRemovingCartItem(isMoveToSFL);
  expect(removeCartItemGen.next().done).toBeTruthy();
});
it('should  dispatch afterRemovingCartItem  action setCartItemsUpdating', () => {
  const isMoveToSFL = true;
  const removeCartItemGen = afterRemovingCartItem(isMoveToSFL);
  expect(removeCartItemGen.next().value).toEqual(
    put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isDeleting: true }))
  );
});
describe('makeCartApiCall ', () => {
  it('makeCartApiCall should return false', () => {
    const isMiniBag = false;
    const pageName = 'test';
    expect(makeCartApiCall(isMiniBag, pageName)).toEqual(false);
  });
  it('makeCartApiCall should return true', () => {
    const isMiniBag = true;
    const pageName = 'SHOPPING_BAG';
    expect(makeCartApiCall(isMiniBag, pageName)).toEqual(true);
  });
});
describe('setUpdateItemErrorMessages ', () => {
  it('should call setToggleCartItemError', () => {
    const payload = {
      fromToggling: true,
      apiPayload: {
        orderItem: [
          {
            orderItemId: '12345',
          },
        ],
      },
    };
    const errorMessage = '300Bad';
    const setUpdateItemErrorMessagesGen = setUpdateItemErrorMessages(payload, errorMessage);
    expect(setUpdateItemErrorMessagesGen.next().value).toEqual(
      put(
        setToggleCartItemError({
          errorMessage,
          itemId: payload.apiPayload.orderItem[0].orderItemId,
        })
      )
    );
  });
  it('should call setBossBopisToggleCartItemError', () => {
    const payload = {
      fromTogglingBossBopis: true,
      apiPayload: {
        orderItem: [{ orderItemId: '12345' }],
        x_updatedItemType: 'test',
      },
    };
    const errorMessage = '300Bad';
    const setUpdateItemErrorMessagesGen = setUpdateItemErrorMessages(payload, errorMessage);
    expect(setUpdateItemErrorMessagesGen.next().value).toEqual(
      put(
        setBossBopisToggleCartItemError({
          errorMessage,
          itemId: payload.apiPayload.orderItem[0].orderItemId,
          targetOrderType: payload.apiPayload.x_updatedItemType,
        })
      )
    );
  });
  it('should call AddToPickupError', () => {
    const payload = {};
    const errorMessage = '300Bad';
    const setUpdateItemErrorMessagesGen = setUpdateItemErrorMessages(payload, errorMessage);
    expect(setUpdateItemErrorMessagesGen.next().value).toEqual(put(AddToPickupError(errorMessage)));
  });
});
describe('Cart Item saga remove', () => {
  it('should dispatch removeCartItem action for success resposnse', () => {
    const payload = {
      payload: {
        itemId: '3001545548',
      },
      isMiniBag: true,
    };
    const removeCartItemGen = removeCartItem(payload);

    expect(removeCartItemGen.next().value).toEqual(
      call(confirmRemoveItem, {
        payload: '3001545548',
        isMiniBag: true,
        isCheckoutFlow: true,
        isMoveToSFL: undefined,
        price: undefined,
      })
    );
  });

  it('should dispatch removeCartItem action for bagPage', () => {
    const payloadValue = {
      itemId: '3001545548',
      pageView: 'myBag',
    };
    const payload = {
      payload: payloadValue,
    };
    const removeCartItemGen = removeCartItem(payload);
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next();
    removeCartItemGen.next(true);
    removeCartItemGen.next(true);

    expect(removeCartItemGen.next(false).value).toEqual(
      put(BAG_PAGE_ACTIONS.openItemDeleteConfirmationModal(payloadValue))
    );
  });
});
describe('updateSagaErrorActions', () => {
  it('should call AddToCartError', () => {
    const updateSagaErrorActionsGen = updateSagaErrorActions('', '404');
    expect(updateSagaErrorActionsGen.next().value).toEqual(put(AddToCartError('404')));
  });
  it('should call AddToPickupError', () => {
    const updateSagaErrorActionsGen = updateSagaErrorActions(true, '404');
    expect(updateSagaErrorActionsGen.next().value).toEqual(put(AddToPickupError('404')));
  });
});
describe('getFavoriteStoreCall', () => {
  it('should call getFavoriteStoreActn', () => {
    const getFavoriteStoreCallGen = getFavoriteStoreCall('add', '1022');
    getFavoriteStoreCallGen.next();
    expect(getFavoriteStoreCallGen.next().value).toEqual(
      put(getFavoriteStoreActn({ ignoreCache: true, isForceUpdate: true }))
    );
  });
  it('should not call getFavoriteStoreActn', () => {
    const getFavoriteStoreCallGen = getFavoriteStoreCall();
    getFavoriteStoreCallGen.next();
    expect(getFavoriteStoreCallGen.next().done).toEqual(true);
  });
});
describe('Cart Item saga update', () => {
  const payload = {
    itemPartNumber: '00193511095440',
    orderItemId: '3001545559',
    quantity: '1',
    variantNo: '3002156005',
    xitem_catEntryId: '1285036',
    callBack: jest.fn(),
    updateActionType: 'UpdatePickUpItem',
  };

  it('should dispatch updateCartItem action for success resposnse', () => {
    const updateCartItemSagaGen = updateCartItemSaga({ payload });
    expect(updateCartItemSagaGen.next().value).toEqual(put(setLoaderState(true)));
    updateCartItemSagaGen.next();
    expect(updateCartItemSagaGen.next().value).toEqual(
      put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isUpdating: true }))
    );
    const res = updateCartItemSagaGen.next();
    updateCartItemSagaGen.next(res);
    updateCartItemSagaGen.next({ isUpdating: true });
    updateCartItemSagaGen.next();
    updateCartItemSagaGen.next();
    updateCartItemSagaGen.next();
    updateCartItemSagaGen.next({ isUpdating: false });
  });

  it('should dispatch updateCartItem action for error resposnse', () => {
    try {
      const updateCartItemSagaGen = updateCartItemSaga({ payload });
      const err = {
        errorMessages: { _error: 'Error of update cart API' },
      };
      updateCartItemSagaGen.next();
      updateCartItemSagaGen.next();
      let putDescriptor = updateCartItemSagaGen.throw(err).value;
      putDescriptor = updateCartItemSagaGen.next().value;
      // eslint-disable-next-line no-underscore-dangle
      expect(putDescriptor).toEqual(put(AddToPickupError(err.errorMessages._error)));
    } catch (err) {
      console.log('testing errors for update item resposnse');
    }
  });

  it('should dispatch updateCartItem action for pick up error resposnse', () => {
    try {
      const updateCartItemSagaGen = updateCartItemSaga({ payload });
      const err = {
        errorMessages: { _error: 'Error in API of Update Cart' },
      };
      updateCartItemSagaGen.next();
      updateCartItemSagaGen.next();
      let putDescriptor = updateCartItemSagaGen.throw(err).value;
      putDescriptor = updateCartItemSagaGen.next().value;
      // eslint-disable-next-line no-underscore-dangle
      expect(putDescriptor).toEqual(put(AddToCartError(err.errorMessages._error)));
    } catch (err) {
      console.log('testing errors for update item resposnse');
    }
  });

  it('should dispatch updateCartItem action without error message from CMS', () => {
    try {
      const updateCartItemSagaGen = updateCartItemSaga({ payload });
      updateCartItemSagaGen.next();
      updateCartItemSagaGen.next();
      let putDescriptor = updateCartItemSagaGen.throw(null).value;
      putDescriptor = updateCartItemSagaGen.next({ DEFAULT: 'Default' }).value;
      // eslint-disable-next-line no-underscore-dangle
      expect(putDescriptor).toEqual(put(AddToPickupError('Default')));
    } catch (err) {
      console.log('testing errors without error message from CMS');
    }
  });

  it('should dispatch updateCartItem action without error message from CMS and without default', () => {
    try {
      const updateCartItemSagaGen = updateCartItemSaga({ payload });
      updateCartItemSagaGen.next();
      updateCartItemSagaGen.next();
      let putDescriptor = updateCartItemSagaGen.throw(null).value;
      putDescriptor = updateCartItemSagaGen.next().value;
      // eslint-disable-next-line no-underscore-dangle
      expect(putDescriptor).toEqual(put(AddToPickupError('ERROR')));
    } catch (err) {
      console.log('testing errors without error message from CMS');
    }
  });

  it('should dispatch updateCartItem action for error resposnse from toggling', () => {
    try {
      payload.fromToggling = true;
      const updateCartItemSagaGen = updateCartItemSaga({ payload });
      const err = {
        errorMessages: { _error: 'Error in API' },
      };
      updateCartItemSagaGen.next();
      updateCartItemSagaGen.next();
      let putDescriptor = updateCartItemSagaGen.throw(err).value;
      putDescriptor = updateCartItemSagaGen.next().value;
      // eslint-disable-next-line no-underscore-dangle
      expect(putDescriptor).toEqual(put(AddToPickupError(err.errorMessages._error)));
    } catch (err) {
      console.log('testing errors for resposnse from toggling');
    }
  });
  it(' updateCartItem action ', () => {
    const updateCartItemSagaGen = updateCartItemSaga({ payload });
    expect(updateCartItemSagaGen.next().value).toEqual(put(setLoaderState(true)));
    updateCartItemSagaGen.next();
    expect(updateCartItemSagaGen.next().value).toEqual(
      put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isUpdating: true }))
    );
    expect(updateCartItemSagaGen.next().value).toEqual(
      put(BAG_PAGE_ACTIONS.setCartItemData(payload))
    );
    expect(updateCartItemSagaGen.next().value).toEqual(
      put(CHECKOUT_ACTIONS.setServerErrorCheckout({}))
    );
    expect(updateCartItemSagaGen.next().value).toEqual(put(clearAddToBagErrorState()));
    expect(updateCartItemSagaGen.next().value).toEqual(put(clearAddToPickupErrorState()));
    expect(updateCartItemSagaGen.next().value).toEqual(put(clearToggleCartItemError()));
    updateCartItemSagaGen.next();
    updateCartItemSagaGen.next();
    updateCartItemSagaGen.next();
    updateCartItemSagaGen.next();
    expect(updateCartItemSagaGen.next().value).toEqual(
      put(
        setClickAnalyticsData({
          customEvents: isMobileApp() ? ['Cart_Edits_e68'] : ['event68'],
          products: [],
          eventName: 'cart update',
          clickEvent: true,
        })
      )
    );
    updateCartItemSagaGen.next();
    updateCartItemSagaGen.next();
    expect(updateCartItemSagaGen.next().value).toEqual(
      put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isUpdating: true }))
    );
    updateCartItemSagaGen.next();
    expect(updateCartItemSagaGen.next().value).toEqual(put(setLoaderState(false)));
    updateCartItemSagaGen.next();
    expect(updateCartItemSagaGen.next().value).toEqual(
      put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isUpdating: false }))
    );
  });

  it('should dispatch setSectionLoaderState from addToCartBopis', () => {
    payload.isAddedToBag = true;
    const addItemToCartBopisGen = updateCartItemSaga({ payload });
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    expect(addItemToCartBopisGen.next().value).toEqual(
      put(BAG_PAGE_ACTIONS.setCartItemData(payload))
    );
  });
});

describe('ForgotPasswordSaga', () => {
  it('should return correct takeLatest effect for forgot pwd', () => {
    const generator = CartPageSaga();
    let takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(CARTPAGE_CONSTANTS.REMOVE_CART_ITEM, removeCartItem)
    );

    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(CARTPAGE_CONSTANTS.UPDATE_CART_ITEM, updateCartItemSaga)
    );

    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(CARTPAGE_CONSTANTS.GET_PRODUCT_SKU_INFO, getProductSKUInfoSaga)
    );

    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(CARTPAGE_CONSTANTS.PICKUP_MODAL_OPEN_FROM_BAG, openPickupModalFromBag)
    );
  });
});

describe('getProductSKUInfoSaga', () => {
  it('should call getProductSKUInfoSafa', () => {
    const payload = {
      payload: {
        productId: '123456',
        itemBrand: 'test',
        xappURLConfig: '',
      },
    };
    const generator = getProductSKUInfoSaga(payload);
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    expect(generator.next().done).toBeTruthy();
  });
  it('should call setProductMultiPackData', () => {
    const payload = {
      payload: {
        productId: undefined,
        itemBrand: undefined,
        xappURLConfig: undefined,
      },
    };
    const generator = getProductSKUInfoSaga(payload);
    expect(generator.next().value).toEqual(put(setProductMultiPackData({})));
    expect(generator.next().value).toEqual(call(getUnbxdXappConfigs));
    expect(generator.next().value).toEqual(select(getIsPdpServiceEnabled));
    expect(generator.next().value).toEqual(select(getABTestPdpService));
    expect(generator.next().value).toEqual(
      call(
        getProductSkuInfoByUnbxd,
        payload.payload.productId,
        payload.payload.itemBrand,
        payload.payload.xappURLConfig,
        undefined,
        undefined
      )
    );
    expect(generator.next().value).toEqual(put(getProductSKUInfoSuccess(undefined)));
    expect(generator.next().done).toBeTruthy();
  });
});
describe('openPickupModalFromBag', () => {
  const payload = {
    payload: {
      colorProductId: '00193511095440',
      orderInfo: {},
      openSkuSelectionForm: false,
      isBopisCtaEnabled: true,
      isBossCtaEnabled: false,
      isItemShipToHome: false,
      alwaysSearchForBOSS: false,
      openRestrictedModalForBopis: false,
    },
  };
  const generator = openPickupModalFromBag(payload);
  it('should return correct takeLatest effect', () => {
    generator.next();
    generator.next();
    generator.next({ product: { generalProductId: '12345' } });
    generator.next();
  });
});
describe('openPickupModalFromBag with values', () => {
  const payload = {
    payload: {
      orderInfo: {},
      openSkuSelectionForm: false,
      isBopisCtaEnabled: true,
      isBossCtaEnabled: false,
      isItemShipToHome: false,
      alwaysSearchForBOSS: false,
      openRestrictedModalForBopis: false,
      finalPartId: '1000',
      itemBrand: 'test',
      xappURLConfig: '',
    },
  };

  it('should call openPickupModalWithValues', () => {
    const generator = openPickupModalFromBag(payload);
    expect(generator.next().value).toEqual(put(setLoaderState(true)));
    expect(generator.next().value).toEqual(call(getUnbxdXappConfigs));
    expect(generator.next().value).toEqual(select());
  });
});
describe('openPickupModalFromBag should set setLoaderState to false ', () => {
  const generator = openPickupModalFromBag({});
  it('should return correct takeLatest effect', () => {
    expect(generator.next().value).toEqual(put(setLoaderState(true)));
    expect(generator.next().value).toEqual(call(getUnbxdXappConfigs));
    expect(generator.next().value).toEqual(select());
    generator.next();
    expect(generator.next().value).toEqual(put(setLoaderState(false)));
  });
});

describe('openPickupModalFromBag should fire openPickupModalWithValues from newbag', () => {
  const payload = {
    payload: {
      orderInfo: {},
      openSkuSelectionForm: false,
      isBopisCtaEnabled: true,
      isBossCtaEnabled: false,
      isItemShipToHome: false,
      alwaysSearchForBOSS: false,
      openRestrictedModalForBopis: false,
      finalPartId: '1000',
      itemBrand: 'test',
      xappURLConfig: '',
      newBag: true,
      colorProductId: 'colorProductId',
    },
  };
  const generator = openPickupModalFromBag(payload);
  it('should call openPickupModalWithValues', () => {
    expect(generator.next().value).toEqual(call(getUnbxdXappConfigs));
    expect(generator.next().value).toEqual(select());
    expect(generator.next().value).toEqual(
      put(
        openPickupModalWithValues({
          productLoading: true,
          fromBagPage: true,
          initialValues: {},
          generalProductId: 'colorProductId',
        })
      )
    );
  });
});

describe('should call fetchProductId', () => {
  it('should call return concatenated partid', () => {
    expect(fetchProductId('10202,10203')).toEqual('10202,10203');
  });
});
describe('cartItemTile saga utils', () => {
  const response = {
    product: {
      colorFitsSizesMap: [
        {
          color: {
            name: 'WHITE',
            imagePath: '2047839/2047839_10_swatch.jpg',
            family: 'WHITE',
          },
          pdpUrl: '//p/2047839_10',
          colorProductId: '629413',
          colorDisplayId: '2047839_10',
          categoryEntity: '',
          imageName: '2047839_10',
          favoritedCount: 830,
          maxAvailable: 39999972,
          hasFits: false,
          miscInfo: {
            isBopisEligible: true,
            isBossEligible: false,
            badge1: {
              defaultBadge: 'ONLINE EXCLUSIVE',
            },
          },
          fits: [
            {
              fitName: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'XS (4)',
                  skuId: '842162',
                  listPrice: 15.5,
                  offerPrice: 15.5,
                  maxAvailable: 0,
                  v_qty: 0,
                  variantId: '00889705073645',
                  variantNo: '2047839001',
                  position: 0,
                },
                {
                  sizeName: 'S (5/6)',
                  skuId: '857592',
                  listPrice: 15.5,
                  offerPrice: 15.5,
                  maxAvailable: 9999990,
                  v_qty: 9999990,
                  variantId: '00889705073942',
                  variantNo: '2047839002',
                  position: 1,
                },
                {
                  sizeName: 'M (7/8)',
                  skuId: '831258',
                  listPrice: 15.5,
                  offerPrice: 15.5,
                  maxAvailable: 9999990,
                  v_qty: 9999990,
                  variantId: '00889705074178',
                  variantNo: '2047839003',
                  position: 2,
                },
                {
                  sizeName: 'L (10/12)',
                  skuId: '833083',
                  listPrice: 15.5,
                  offerPrice: 15.5,
                  maxAvailable: 12,
                  v_qty: 12,
                  variantId: '00889705073867',
                  variantNo: '2047839004',
                  position: 3,
                },
                {
                  sizeName: 'XL (14)',
                  skuId: '834779',
                  listPrice: 15.5,
                  offerPrice: 15.5,
                  maxAvailable: 9999990,
                  v_qty: 9999990,
                  variantId: '00889705074048',
                  variantNo: '2047839005',
                  position: 4,
                },
                {
                  sizeName: 'XXL (16)',
                  skuId: '873778',
                  listPrice: 15.5,
                  offerPrice: 15.5,
                  maxAvailable: 9999990,
                  v_qty: 9999990,
                  variantId: '00889705479997',
                  variantNo: '2047839006',
                  position: 5,
                },
              ],
            },
          ],
          listPrice: 15.5,
          offerPrice: 15.5,
          unbxdId: 'unbxdId',
        },
      ],
    },
  };
  it('should be defined with expected inputs', () => {
    expect(CartItemTileSagaUtils.applyMultiPackInfo(response, response, '2', '10')).toBeDefined();
  });

  it('should be defined without expected inputs', () => {
    expect(CartItemTileSagaUtils.applyMultiPackInfo(response, null, null, null)).toBeDefined();
  });
});
