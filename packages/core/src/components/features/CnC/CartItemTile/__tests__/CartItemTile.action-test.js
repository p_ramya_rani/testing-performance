// 9fbef606107a605d69c0edbcd8029e5d 
import {
  removeCartItem,
  removeCartItemComplete,
  openPickupModalWithValuesFromBag,
  getProductSKUInfo,
  getProductSKUInfoSuccess,
  setProductMultiPackData,
  updateCartItem,
  updateCartItemComplete,
  setToggleCartItemError,
  setBossBopisToggleCartItemError,
  clearToggleCartItemError,
  clearToggleBossBopisCartItemError,
} from '../container/CartItemTile.actions';
import CARTPAGE_CONSTANTS from '../CartItemTile.constants';

describe('cart tile Actions', () => {
  it('removeCartItem', () => {
    expect(removeCartItem({})).toEqual({
      payload: {},
      type: CARTPAGE_CONSTANTS.REMOVE_CART_ITEM,
    });
  });

  it('removeCartItemComplete', () => {
    expect(removeCartItemComplete({})).toEqual({
      type: CARTPAGE_CONSTANTS.REMOVE_CART_ITEM_COMPLETE,
    });
  });

  it('openPickupModalWithValuesFromBag', () => {
    expect(openPickupModalWithValuesFromBag({})).toEqual({
      payload: {},
      type: CARTPAGE_CONSTANTS.PICKUP_MODAL_OPEN_FROM_BAG,
    });
  });

  it('getProductSKUInfo', () => {
    expect(getProductSKUInfo({})).toEqual({
      payload: {},
      type: CARTPAGE_CONSTANTS.GET_PRODUCT_SKU_INFO,
    });
  });

  it('getProductSKUInfoSuccess', () => {
    expect(getProductSKUInfoSuccess({})).toEqual({
      payload: {},
      type: CARTPAGE_CONSTANTS.GET_PRODUCT_SKU_INFO_SUCCESS,
    });
  });

  it('setProductMultiPackData', () => {
    expect(setProductMultiPackData({})).toEqual({
      payload: {},
      type: CARTPAGE_CONSTANTS.SET_PRODUCT_MULTIPACK_DATA,
    });
  });

  it('updateCartItem', () => {
    expect(updateCartItem({})).toEqual({
      payload: {},
      type: CARTPAGE_CONSTANTS.UPDATE_CART_ITEM,
    });
  });

  it('updateCartItemComplete', () => {
    expect(updateCartItemComplete({})).toEqual({
      type: CARTPAGE_CONSTANTS.UPDATE_CART_ITEM_COMPLETE,
    });
  });

  it('setToggleCartItemError', () => {
    expect(setToggleCartItemError({})).toEqual({
      payload: {},
      type: CARTPAGE_CONSTANTS.SET_TOGGLE_CART_ITEM_ERROR,
    });
  });

  it('setBossBopisToggleCartItemError', () => {
    expect(setBossBopisToggleCartItemError({})).toEqual({
      payload: {},
      type: CARTPAGE_CONSTANTS.SET_TOGGLE_BOSS_BOPIS_CART_ITEM_ERROR,
    });
  });

  it('clearToggleCartItemError', () => {
    expect(clearToggleCartItemError({})).toEqual({
      type: CARTPAGE_CONSTANTS.CLEAR_TOGGLE_CART_ITEM_ERROR,
    });
  });

  it('clearToggleBossBopisCartItemError', () => {
    expect(clearToggleBossBopisCartItemError({})).toEqual({
      type: CARTPAGE_CONSTANTS.CLEAR_TOGGLE_BOSS_BOPIS_CART_ITEM_ERROR,
    });
  });
});
