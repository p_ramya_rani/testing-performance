// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import {
  getProductName,
  getProductColor,
  getProductBrand,
  getProductFit,
  getProductPoints,
  getProductQty,
  getProductOfferPrice,
  checkForGiftItem,
  getLabelsCartItemTile,
  getProductSize,
  getGeneralProdId,
  getIsCartItemsUpdating,
  getProductSkuId,
  getCartItemsSflError,
  getCartToggleError,
  getPdpUrl,
  getUpdateMiniBagFlag,
  getEcomSkuParams,
  getMultiPackItems,
  getBossState,
  getBossCity,
  getBopisState,
  getBopisCity,
} from '../container/CartItemTile.selectors';

describe('#CartItemTile selector', () => {
  it('#getProductName should return product name', () => {
    const productState = fromJS({
      productInfo: {
        name: '',
      },
    });
    expect(getProductName(productState)).toEqual(productState.getIn(['productInfo', 'name']));
  });

  it('#getProductColor should return product color', () => {
    const productState = fromJS({
      productInfo: {
        color: {
          name: 'red',
        },
      },
    });
    expect(getProductColor(productState)).toEqual(
      productState.getIn(['productInfo', 'color', 'name'])
    );
  });

  it('#getGeneralProdId should return product gen id', () => {
    const productState = fromJS({
      productInfo: {
        color: {
          name: 'red',
        },
        generalProductId: '1234',
      },
    });
    expect(getGeneralProdId(productState)).toEqual(
      productState.getIn(['productInfo', 'generalProductId'])
    );
  });

  it('#getProductSkuId should return product sku id', () => {
    const productState = fromJS({
      productInfo: {
        color: {
          name: 'red',
        },
        skuId: '1234',
      },
    });
    expect(getProductSkuId(productState)).toEqual(productState.getIn(['productInfo', 'skuId']));
  });

  it('#getPdpUrl should return product sku id', () => {
    const productState = fromJS({
      productInfo: {
        color: {
          name: 'red',
        },
        skuId: '1234',
        pdpUrl: '',
      },
    });
    expect(getPdpUrl(productState)).toEqual(productState.getIn(['productInfo', 'pdpUrl']));
  });

  it('#getProductFit should return product fit', () => {
    const productState = fromJS({
      productInfo: {
        fit: 'regular',
      },
    });
    expect(getProductFit(productState)).toEqual(productState.getIn(['productInfo', 'fit']));
  });
  it('#getProductSize should return product size', () => {
    const productState = fromJS({
      productInfo: {
        size: '4',
      },
    });
    expect(getProductSize(productState)).toEqual(productState.getIn(['productInfo', 'size']));
  });

  it('#getProductOfferPrice should return product price', () => {
    const productState = fromJS({
      itemInfo: {
        offerPrice: 20,
      },
    });
    expect(getProductOfferPrice(productState)).toEqual(
      productState.getIn(['itemInfo', 'offerPrice'])
    );
  });

  it('#getProductBrand should return product brand', () => {
    const productState = fromJS({
      productInfo: {
        itemBrand: 'TCP',
      },
    });
    expect(getProductBrand(productState)).toEqual(productState.getIn(['productInfo', 'itemBrand']));
  });

  it('#getProductPoints should return product points', () => {
    const productState = fromJS({
      itemInfo: {
        itemPoints: 120,
      },
    });
    expect(getProductPoints(productState)).toEqual(productState.getIn(['itemInfo', 'itemPoints']));
  });

  it('#getProductQty should return product qty', () => {
    const productState = fromJS({
      itemInfo: {
        quantity: 12,
      },
    });
    expect(getProductQty(productState)).toEqual(productState.getIn(['itemInfo', 'quantity']));
  });

  it('#checkForGiftItem should return boolean', () => {
    const productState = fromJS({
      productInfo: {
        isGiftCard: true,
      },
    });
    expect(checkForGiftItem(productState)).toEqual(
      productState.getIn(['productInfo', 'isGiftCard'])
    );
  });

  it('#getMultiPackItems should return multiPackItems', () => {
    const productState = fromJS({
      productInfo: {
        multiPackItems: '1',
      },
    });
    expect(getMultiPackItems(productState)).toEqual(
      productState.getIn(['productInfo', 'multiPackItems'])
    );
  });

  it('#getIsCartItemsUpdating', () => {
    const CartPageReducer = fromJS({
      uiFlags: {
        isCartItemsUpdating: true,
      },
    });
    expect(getIsCartItemsUpdating({ CartPageReducer })).toEqual(true);
  });

  it('#getUpdateMiniBagFlag', () => {
    const CartPageReducer = fromJS({
      uiFlags: {
        updateMiniBag: true,
      },
    });
    expect(getUpdateMiniBagFlag({ CartPageReducer })).toEqual(true);
  });

  it('#getUpdateMiniBagFlag for no update', () => {
    const CartPageReducer = fromJS({
      uiFlags: {
        updateMiniBag: false,
      },
    });
    expect(getUpdateMiniBagFlag({ CartPageReducer })).toEqual(false);
  });

  it('#getCartItemsSflError', () => {
    const CartPageReducer = fromJS({
      uiFlags: {
        cartItemSflError: 'error',
      },
    });
    expect(getCartItemsSflError({ CartPageReducer })).toEqual('error');
  });

  it('#getLabelsCartItemTile should return labels', () => {
    const addedToBagModal = {
      lbl_info_color: 'Color',
      lbl_info_size: 'Size',
      lbl_info_Qty: 'Qty',
      lbl_info_price: 'Price',
      lbl_info_giftDesign: 'Design',
      lbl_info_giftValue: 'Value',
    };

    const productState = {
      Labels: {
        global: {
          addedToBagModal,
          cartItemTile: {
            lbl_cartTile_fit: 'Fit',
            lbl_cartTile_points: 'Points',
            lbl_cartTile_cancel: 'Cancel',
            lbl_cartTile_edit: 'Edit',
            lbl_cartTile_saveForLater: 'Save For Later',
            lbl_cartTile_productBrandAlt: 'Brand',
            lbl_cartTile_productImageAlt: 'Product',
            lbl_cartTile_update: 'update',
            lbl_cartTile_remove: 'minibag',
            lbl_cartTile_bopis: 'bopis',
            lbl_cartTile_boss: 'boss',
            lbl_cartTile_noRushPickup: 'boss',
            lbl_cartTile_pickUpToday: 'pickup',
            lbl_cartTile_shipToHome: 'ecom',
            lbl_cartTile_extra: 'extra',
            lbl_cartTile_off: 'off',
            lbl_cartTile_delete: 'delete',
          },
          minibag: {
            lbl_miniBag_problemWithOrder: 'minibag',
            lbl_miniBag_error: 'minibag',
            lbl_miniBag_itemUnavailable: 'minibag',
            lbl_miniBag_itemSoldOut: 'minibag',
            lbl_miniBag_chooseDiff: 'minibag',
            lbl_miniBag_soldOut: 'minibag',
            lbl_minibag_errorSize: 'minibag',
            lbl_minibag_errorUpdateUnavailable: 'minibag',
            lbl_minibag_errorRemoveSoldoutHeader: 'minibag',
            lbl_minibag_errorRemove: 'remove',
          },
        },
        checkout: {
          bagPage: {
            lbl_sfl_actionLink: 'saveForLaterLink',
            lbl_sfl_maxLimitError: 'sflMaxLimitError',
            lbl_sfl_moveToBag: 'moveToBagLink',
            bl_sfl_actionSuccess: 'sflSuccess',
            lbl_sfl_itemDeleteSuccess: 'sflDeleteSuccess',
          },
        },
      },
    };
    expect(getLabelsCartItemTile(productState).bopisLabel).toEqual('bopis');
    expect(getLabelsCartItemTile(productState).problemWithOrder).toEqual('minibag');
    expect(getLabelsCartItemTile(productState).sflSuccess).toEqual('sflSuccess');
  });

  it('#getCartToggleError', () => {
    const CartItemTileReducer = fromJS({
      toggleError: {
        error: {
          errorMessage: 'ERROR',
        },
      },
    });

    expect(getCartToggleError({ CartItemTileReducer }).getIn(['error', 'errorMessage'])).toBe(
      'ERROR'
    );
  });
});

describe('getEcomSkuParams', () => {
  const cartOrderItems = [
    {
      productInfo: {
        generalProductId: '1234080',
        productPartNumber: '3001569_1301',
        skuId: '1234475',
        itemPartNumber: '00193511087773',
        variantNo: '3001569004',
      },
      itemInfo: {
        quantity: 3,
        itemId: '3001545564',
        itemPoints: 78,
        listPrice: 38.85,
      },
      miscInfo: {
        orderItemType: 'ECOM',
      },
    },
    {
      productInfo: {
        generalProductId: '1234080',
        productPartNumber: '3001569_13018',
        skuId: '12344751',
        itemPartNumber: '001935110877735',
        variantNo: '3001569004',
      },
      itemInfo: {
        quantity: 2,
        itemId: '3001545564',
        itemPoints: 78,
        listPrice: 38.85,
      },
      miscInfo: {
        orderItemType: 'BOPIS',
      },
    },
    {
      productInfo: {
        generalProductId: '1234080',
        productPartNumber: '3001569_13018',
        skuId: '12344751',
        itemPartNumber: '001935110877735',
        variantNo: '3001569004',
      },
      itemInfo: {
        quantity: 2,
        itemId: '3001545564',
        itemPoints: 78,
        listPrice: 38.85,
        isGiftItem: true,
      },
      miscInfo: {
        orderItemType: 'BOPIS',
      },
    },
  ];

  const formattedOrderItems = [
    {
      skuId: '00193511087773',
      skuQty: 3,
      orderItemId: '3001545564',
    },
  ];
  it('should return formatted orderItems for ECOM products', () => {
    const products = getEcomSkuParams(fromJS(cartOrderItems));
    expect(products).toEqual(formattedOrderItems);
  });
});

describe('get state and city', () => {
  const CartPageReducer = fromJS({
    orderDetails: {
      bossState: '',
      bossCity: '',
      bopisState: '',
      bopisCity: '',
    },
  });
  it('#getBossState should return state value', () => {
    expect(getBossState({ CartPageReducer })).toEqual('');
  });
  it('#getBossCity should return city value', () => {
    expect(getBossCity({ CartPageReducer })).toEqual('');
  });
  it('#getBopisState should return state value', () => {
    expect(getBopisState({ CartPageReducer })).toEqual('');
  });
  it('#getBopisCity should return city value', () => {
    expect(getBopisCity({ CartPageReducer })).toEqual('');
  });
});
