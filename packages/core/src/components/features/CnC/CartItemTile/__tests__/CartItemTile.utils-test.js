// 9fbef606107a605d69c0edbcd8029e5d
import { filterBopisProducts, currencyConversion, updateBopisInventory } from '../utils/utils';

describe('CartItemTile Utils', () => {
  const orderItems = [
    {
      productInfo: { orderType: 'BOPIS', variantNo: '123', itemPartNumber: '1234' },
      miscInfo: { store: {}, storeId: '123  ', inventoryAvailBOPIS: '123' },
    },
  ];
  it('should return Bopis products', () => {
    expect(filterBopisProducts(orderItems)).toEqual([
      { itemPartNumber: '1234', storeId: '3  ', variantNo: '123' },
    ]);
  });

  it('should currencyConversion', () => {
    expect(
      currencyConversion(50, { exchangevalue: 60, merchantMargin: 15, roundMethod: false })
    ).toEqual(45000);
  });

  it('should currencyConversion withRound', () => {
    expect(
      currencyConversion(50, { exchangevalue: 60, merchantMargin: 15, roundMethod: 2 })
    ).toEqual(45000);
  });

  it('should updateBopisInventory', () => {
    expect(updateBopisInventory(orderItems, [{ variantNo: '123', quantity: 1 }])).toEqual([
      {
        miscInfo: { availability: 'OK', store: {}, storeId: '123  ', inventoryAvailBOPIS: 1 },
        productInfo: { itemPartNumber: '1234', orderType: 'BOPIS', variantNo: '123' },
      },
    ]);
  });
});
