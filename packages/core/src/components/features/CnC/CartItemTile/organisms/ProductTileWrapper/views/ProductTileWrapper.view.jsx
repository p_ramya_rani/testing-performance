// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CartItemTile from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.container';
import {
  getProductName,
  getProductDetails,
} from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { FULLY_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { getIconPath, isMobileApp, routerPush } from '@tcp/core/src/utils';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import EmptyBag from '@tcp/core/src/components/features/CnC/EmptyBagPage/views/EmptyBagPage.view';
import ErrorMessage from '@tcp/core/src/components/features/CnC/common/molecules/ErrorMessage';

import MiniBagTileSkeleton from '@tcp/core/src/components/features/CnC/CartItemTile/molecules/CartItemTile/skelton/MiniBagTileSkeleton.view';
import RemoveSoldOut from '../../../../common/molecules/RemoveSoldOut';

import productTileCss, { miniBagCSS } from '../styles/ProductTileWrapper.style';
import CARTPAGE_CONSTANTS from '../../../CartItemTile.constants';
import CartItemTileSkelton from '../../../molecules/CartItemTile/skelton/CartItemTileSkelton.view';

/**
 * This just holds the logic for rendering a UX timer
 * to measure when the cart items appear. It reads the
 * number of cart items and renders the timer if that
 * number is greater than zero.
 */
function CartItemsUXTimer({ orderItems }) {
  if (isMobileApp()) {
    return null;
  }
  const [state, setState] = useState(false);
  useEffect(() => {
    if (orderItems.size > 0) setState(true);
  }, [orderItems.size]);
  return state ? <RenderPerf.Measure name={FULLY_VISIBLE} /> : null;
}

CartItemsUXTimer.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  orderItems: PropTypes.any.isRequired,
};

class ProductTileWrapper extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isEditAllowed: true,
      openedTile: 0,
      swipedElement: null,
      loginModal: false,
    };
  }

  toggleEditAllowance = () => {
    const { isEditAllowed } = this.state;
    const { onItemEdit } = this.props;
    if (onItemEdit) {
      onItemEdit(isEditAllowed);
    }
    this.setState({
      isEditAllowed: !isEditAllowed,
    });
  };

  toggleLoginModal = () => {
    const { loginModal } = this.state;
    this.setState({
      loginModal: !loginModal,
    });
  };

  setSwipedElement = (elem) => {
    this.setState({ swipedElement: elem });
  };

  setSelectedProductTile = ({ index }) => {
    this.setState({ openedTile: index });
  };

  isEditAllowed = (productDetail, pageView) => {
    const { isEditAllowed } = this.state;
    if (
      productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY_UNAVAILABLE ||
      productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY_SOLDOUT ||
      (pageView !== 'myBag' && productDetail.miscInfo.orderItemType === CARTPAGE_CONSTANTS.BOPIS)
    ) {
      return false;
    }
    return isEditAllowed;
  };

  onLinkClick = ({ e, componentId }) => {
    const { openOverlay, fullPageAuthEnabled } = this.props;
    const { pathname = 'referrer' } = (window && window.location) || {};
    e.preventDefault();
    if (fullPageAuthEnabled && componentId === 'login') {
      routerPush(`/home?target=login&successtarget=${pathname}`, '/home/login');
    } else {
      openOverlay({
        component: componentId,
        variation: 'primary',
      });
    }
  };

  getTickIcon = () => {
    return <Image alt="closeIcon" className="tick-icon" src={getIconPath('circle-check-fill')} />;
  };

  renderSflItemRemovedMessage = (
    isSflItemRemoved,
    isSflItemMovedToBag,
    sflDeleteSuccessMsg,
    sflMovedToBagSuccessMsg
  ) => {
    const { isBagPageSflSection } = this.props;
    return (
      isBagPageSflSection &&
      !isMobileApp() &&
      isSflItemRemoved && (
        <div className="delete-msg">
          {this.getTickIcon()}
          <BodyCopy
            component="span"
            fontSize="fs12"
            textAlign="center"
            fontFamily="secondary"
            fontWeight="extrabold"
          >
            {isSflItemMovedToBag ? sflMovedToBagSuccessMsg : sflDeleteSuccessMsg}
          </BodyCopy>
        </div>
      )
    );
  };

  renderItemSflSuccessMsg = (isBagPage, isCartItemSFL, itemSflSuccessMsg) => {
    const { isBagPageSflSection, cartSflMerged, labels } = this.props;
    const displayMessage = cartSflMerged ? labels.cartSflMergedSuccessMsg : itemSflSuccessMsg;
    const cartSflMergedSuccess =
      cartSflMerged &&
      displayMessage &&
      displayMessage.length > 0 &&
      displayMessage !== 'lbl_sfl_cartSflMergedSuccessMsg';

    return (
      !isBagPageSflSection &&
      !isMobileApp() &&
      isBagPage &&
      (isCartItemSFL || cartSflMergedSuccess) && (
        <div className="delete-msg">
          {this.getTickIcon()}
          <BodyCopy
            component="span"
            fontSize="fs12"
            textAlign="center"
            fontFamily="secondary"
            fontWeight="extrabold"
          >
            {displayMessage}
          </BodyCopy>
        </div>
      )
    );
  };

  renderEmptyBag = (
    cartData,
    bagLabels,
    isUserLoggedIn,
    isBagPageSflSection,
    showPlccApplyNow,
    isBagPage,
    openModalApplyNowModal
  ) => {
    const { isMiniBag, isBagLoading, newBag } = this.props;
    const { loginModal } = this.state;
    if (isBagLoading && isBagPage) {
      return (
        <>
          <CartItemTileSkelton />
          <CartItemTileSkelton />
        </>
      );
    }
    if (cartData.size === 0) {
      return (
        <EmptyBag
          bagLabels={bagLabels}
          isUserLoggedIn={isUserLoggedIn}
          isBagPageSflSection={isBagPageSflSection}
          showPlccApplyNow={showPlccApplyNow}
          onLinkClick={this.onLinkClick}
          openModalApplyNowModal={openModalApplyNowModal}
          toggleLoginModal={this.toggleLoginModal}
          loginModal={loginModal}
          newBag={newBag}
        />
      );
    }
    if (isMiniBag) {
      return (
        <>
          <MiniBagTileSkeleton />
          <MiniBagTileSkeleton />
        </>
      );
    }
    return null;
  };

  renderInformationHeaderError = (
    showError,
    isBagPageSflSection,
    isSoldOut,
    labels,
    confirmRemoveCartItem,
    getUnavailableOOSItems,
    isUnavailable
  ) => {
    return (
      <>
        {isMobileApp() && showError && <ErrorMessage error={labels.problemWithOrder} bagPage />}
        {!isBagPageSflSection && isSoldOut && isMobileApp() && (
          <RemoveSoldOut
            labels={labels}
            removeCartItem={confirmRemoveCartItem}
            getUnavailableOOSItems={getUnavailableOOSItems}
            isSoldOut={isSoldOut}
          />
        )}
        {!isBagPageSflSection && isUnavailable && isMobileApp() && (
          <RemoveSoldOut
            labels={labels}
            removeCartItem={confirmRemoveCartItem}
            getUnavailableOOSItems={getUnavailableOOSItems}
            isUnavailable={isUnavailable}
          />
        )}
      </>
    );
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity, complexity
  render() {
    const {
      orderItems,
      orderItemsLocal,
      bagLabels,
      labels,
      pageView,
      confirmRemoveCartItem,
      isUserLoggedIn,
      isPlcc,
      sflItemsCount,
      isBagPageSflSection,
      isCartItemsUpdating,
      sflItems,
      showPlccApplyNow,
      isCartItemSFL,
      isSflItemRemoved,
      isSflItemMovedToBag,
      setHeaderErrorState,
      navigation,
      openModalApplyNowModal,
      isMiniBag,
      closeMiniBag,
      isBagLoading,
      fullPageAuthEnabled,
      newBag,
      newMiniBag,
      isLowInventoryBannerEnabled,
      cartOrderItems,
      isLowInventoryBannerEnabledForOldDesign,
      isCartQuantityUpdating,
    } = this.props;
    const cartData = isBagPageSflSection ? sflItems : orderItems;

    let isUnavailable;
    let isSoldOut;
    const isBagPage = pageView === 'myBag';
    const inheritedStyles = isBagPage ? productTileCss : miniBagCSS;
    const getUnavailableOOSItems = [];
    const { openedTile, swipedElement } = this.state;
    const showError = orderItems.find((tile) => {
      const productDetail = getProductDetails(tile);
      return (
        productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY_SOLDOUT ||
        productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY_UNAVAILABLE
      );
    });

    if (
      (cartData && cartData.size > 0 && !isBagLoading) ||
      (orderItemsLocal && orderItemsLocal.size > 0 && isBagLoading)
    ) {
      const dataSource = isBagLoading && orderItemsLocal.size > 0 ? orderItemsLocal : cartData;

      const orderItemsView = dataSource.map((tile, index) => {
        const productDetail = getProductDetails(tile);
        if (productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY_SOLDOUT) {
          getUnavailableOOSItems.push(productDetail.itemInfo.itemId);
          isSoldOut = true;
        }
        if (productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY_UNAVAILABLE) {
          getUnavailableOOSItems.push(productDetail.itemInfo.itemId);
          isUnavailable = true;
        }
        return (
          <>
            <CartItemTile
              inheritedStyles={inheritedStyles}
              labels={labels}
              productDetail={productDetail}
              key={`${getProductName(tile)}`}
              pageView={pageView}
              toggleEditAllowance={this.toggleEditAllowance}
              isEditAllowed={this.isEditAllowed(productDetail, pageView)}
              isPlcc={isPlcc}
              itemIndex={index}
              openedTile={openedTile}
              setSelectedProductTile={this.setSelectedProductTile}
              setSwipedElement={this.setSwipedElement}
              swipedElement={swipedElement}
              sflItemsCount={sflItemsCount}
              isBagPageSflSection={isBagPageSflSection}
              navigation={navigation}
              closeMiniBag={closeMiniBag}
              isMiniBag={isMiniBag}
              isMultipackProduct={productDetail.productInfo.multipack}
              newBag={newBag}
              newMiniBag={newMiniBag}
              isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
              cartOrderItems={cartOrderItems}
              isLowInventoryBannerEnabledForOldDesign={isLowInventoryBannerEnabledForOldDesign}
              isCartQuantityUpdating={isCartQuantityUpdating}
            />
          </>
        );
      });
      return (
        <>
          {this.renderInformationHeaderError(
            showError,
            isBagPageSflSection,
            isSoldOut,
            labels,
            confirmRemoveCartItem,
            getUnavailableOOSItems,
            isUnavailable
          )}
          {!isMobileApp() &&
            setHeaderErrorState &&
            setHeaderErrorState(true, {
              labels,
              orderItems,
              pageView,
              isUnavailable,
              isSoldOut,
              getUnavailableOOSItems,
              confirmRemoveCartItem,
              isBagPageSflSection,
              isCartItemSFL,
              isCartItemsUpdating,
              isSflItemRemoved,
              isSflItemMovedToBag,
            })}
          {this.renderItemSflSuccessMsg(isBagPage, isCartItemSFL, labels.sflSuccess)}
          {this.renderSflItemRemovedMessage(
            isSflItemRemoved,
            isSflItemMovedToBag,
            labels.sflDeleteSuccess,
            labels.sflMovedToBagSuccess
          )}
          {orderItemsView}
          {/* UX timer */}
          {<CartItemsUXTimer orderItems={orderItems} />}
        </>
      );
    }
    return (
      <>
        {!isMobileApp() &&
          setHeaderErrorState &&
          setHeaderErrorState(true, {
            labels,
            orderItems,
            pageView,
            isUnavailable,
            isSoldOut,
            getUnavailableOOSItems,
            confirmRemoveCartItem,
            isBagPageSflSection,
            isCartItemSFL,
            isCartItemsUpdating,
            isSflItemRemoved,
            isSflItemMovedToBag,
          })}
        {this.renderItemSflSuccessMsg(isBagPage, isCartItemSFL, labels.sflSuccess)}
        {this.renderSflItemRemovedMessage(
          isSflItemRemoved,
          isSflItemMovedToBag,
          labels.sflDeleteSuccess,
          labels.sflMovedToBagSuccess
        )}
        {this.renderEmptyBag(
          cartData,
          bagLabels,
          isUserLoggedIn,
          isBagPageSflSection,
          showPlccApplyNow,
          isBagPage,
          openModalApplyNowModal,
          isMiniBag,
          fullPageAuthEnabled
        )}
      </>
    );
  }
}

ProductTileWrapper.defaultProps = {
  orderItemsLocal: [],
  pageView: '',
  bagLabels: {},
  isBagPageSflSection: false,
  cartSflMerged: false,
  fullPageAuthEnabled: false,
  newBag: false,
  isLowInventoryBannerEnabled: false,
  isLowInventoryBannerEnabledForOldDesign: false,
};

ProductTileWrapper.propTypes = {
  orderItems: PropTypes.shape([]).isRequired,
  orderItemsLocal: PropTypes.shape([]),
  sflItems: PropTypes.shape([]).isRequired,
  labels: PropTypes.shape({}).isRequired,
  confirmRemoveCartItem: PropTypes.func.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  pageView: PropTypes.string,
  bagLabels: PropTypes.shape(),
  sflItemsCount: PropTypes.number.isRequired,
  isBagPageSflSection: PropTypes.bool,
  showPlccApplyNow: PropTypes.bool.isRequired,
  isCartItemSFL: PropTypes.bool.isRequired,
  isSflItemRemoved: PropTypes.bool.isRequired,
  isMiniBag: PropTypes.bool.isRequired,
  onItemEdit: PropTypes.func.isRequired,
  openOverlay: PropTypes.func.isRequired,
  setHeaderErrorState: PropTypes.func.isRequired,
  openModalApplyNowModal: PropTypes.func.isRequired,
  closeMiniBag: PropTypes.func.isRequired,
  isBagLoading: PropTypes.bool.isRequired,
  isCartItemsUpdating: PropTypes.bool.isRequired,
  isSflItemMovedToBag: PropTypes.bool.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  cartSflMerged: PropTypes.bool,
  fullPageAuthEnabled: PropTypes.bool,
  newBag: PropTypes.bool,
  isLowInventoryBannerEnabled: PropTypes.bool,
  isLowInventoryBannerEnabledForOldDesign: PropTypes.bool,
};

export default ProductTileWrapper;
export { ProductTileWrapper as ProductTileWrapperVanilla };
