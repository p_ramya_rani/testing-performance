// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  getCartOrderList,
  getIsCartItemsUpdating,
  getIsCartItemsSFL,
  getIsSflItemRemoved,
  getIsSflItemMovedToBag,
} from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import { openOverlayModal } from '@tcp/core/src/components/features/account/OverlayModal/container/OverlayModal.actions';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import {
  getUserLoggedInState,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { updateCartItem, confirmRemoveCartItem } from '../../../container/CartItemTile.actions';
import ProductTileWrapper from '../views/ProductTileWrapper.view';
import BAG_SELECTORS from '../../../../BagPage/container/BagPage.selectors';

export const ProductTileWrapperContainer = (props) => {
  return (
    <>
      <ProductTileWrapper {...props} />
    </>
  );
};

const mapStateToProps = (state) => {
  const { getProductTileLabels, isBagLoading } = BAG_SELECTORS;
  return {
    orderItems: getCartOrderList(state),
    orderItemsLocal: BAG_SELECTORS.getOrderItemsLocal(state),
    labels: getProductTileLabels(state),
    isUserLoggedIn: getUserLoggedInState(state),
    isPlcc: isPlccUser(state),
    isCartItemsUpdating: getIsCartItemsUpdating(state),
    isCartItemSFL: getIsCartItemsSFL(state),
    isSflItemRemoved: getIsSflItemRemoved(state),
    isBagLoading: isBagLoading(state),
    isSflItemMovedToBag: getIsSflItemMovedToBag(state),
    fullPageAuthEnabled: getEnableFullPageAuth(state),
    isCartQuantityUpdating: BAG_SELECTORS.isBagLoading(state),
  };
};
export const mapDispatchToProps = (dispatch) => {
  return {
    confirmRemoveCartItem: (orderItemId) => {
      dispatch(confirmRemoveCartItem(orderItemId));
    },
    updateCartItem: (itemId, skuId, quantity, itemPartNumber, variantNo) => {
      dispatch(updateCartItem({ itemId, skuId, quantity, itemPartNumber, variantNo }));
    },
    openOverlay: (component) => dispatch(openOverlayModal(component)),
    openModalApplyNowModal: (payload) => dispatch(toggleApplyNowModal(payload)),
  };
};

ProductTileWrapperContainer.defaultProps = {
  orderItems: [],
  pageView: '',
  fullPageAuthEnabled: false,
};

ProductTileWrapperContainer.propTypes = {
  orderItems: PropTypes.shape([]),
  initialActions: PropTypes.func.isRequired,
  pageView: PropTypes.string,
  fullPageAuthEnabled: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductTileWrapperContainer);
