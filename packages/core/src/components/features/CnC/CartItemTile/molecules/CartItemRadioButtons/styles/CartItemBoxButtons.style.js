// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

// using these for disabled state
const yellowDisabledState = '#f7ec94';
const blackDisabledState = '#afafaf';

const styles = css`
  width: 100%;
  margin-top: 20px;
  &.main-container {
    &.cart-item-radio-buttons {
      margin-top: 16px;
      margin-inline-start: 0;
      margin-inline-end: 0;
      padding-block-start: 0;
      padding-inline-start: 0;
      padding-inline-end: 0;
      padding-block-end: 0;
      @media ${(props) => props.theme.mediaQuery.large} {
        width: 538px;
        margin-top: 24px;
        margin-bottom: 8px;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        margin-top: 4px;
      }
    }
  }

  .DTBoxContainer {
    display: flex;
    align-items: stretch;
    flex-wrap: wrap;
    gap: 0.5rem;
    justify-content: space-between;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-top: 10px;
    }
  }

  .DTBoxContainer > div {
    display: inline;
  }

  .store-details-container {
    display: flex;
    width: 100%;
    justify-content: space-between;
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 164px;
      flex-direction: column;
      margin-right: 16px;
      margin-top: 0;
    }
  }

  .text-ellipsis {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .stores-container {
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    margin-top: 10px;
    @media ${(props) => props.theme.mediaQuery.large} {
      position: relative;
      left: 184px;
      &.only-bopis {
        left: 269px;
      }
      &.only-boss {
        left: 269px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 100%;
    }
  }

  .show-store {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: flex;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      display: flex;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      opacity: 1;
    }
  }
  .hide-store {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      display: none;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      opacity: 0;
    }
  }

  .DTBoxItem {
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    border: solid 1px ${(props) => props.theme.colorPalette.gray[500]};
    background-color: ${(props) => props.theme.colorPalette.white};
    min-height: 60px;
    height: 100%;
    overflow: hidden;
    text-align: center;
    padding: 0;
    box-sizing: border-box;
    &.disabled {
      border: solid 1px rgb(216 216 216 / 50%);
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      min-height: 56px;
    }
  }

  .title-container p {
    text-transform: capitalize;
  }

  .subText {
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    color: ${(props) => props.theme.colorPalette.green[500]};
    padding-left: 0px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs11};
    }
    text-align: center;
  }

  .estimated-shipping-date {
    display: inline;
    white-space: normal;
  }

  .edd-component {
    > div {
      text-align: center;
      font-size: 12px;
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      margin-top: -1px;
    }
  }

  .boss-dates {
    width: 100%;
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs11};
    }
  }

  .changeStoreLink {
    padding-left: 0px;
    display: inline-block;
    color: ${(props) => props.theme.colorPalette.blue[800]};
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  }
  .subtitle-container {
    flex-direction: column;
    align-items: flex-start;
    display: flex;
    justify-content: space-between;
  }

  .clrMsg {
    padding-left: 0px;
  }
  .radio-button,
  .radio-button-checked {
    opacity: 0;
    width: 1px;
    height: 1px;
    overflow: hidden;
  }

  .DTBoxItem .main-container p.tileHeading {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    font-stretch: normal;
    font-style: normal;
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
  }

  .main-container {
    display: inline-flex;
    padding-top: 8px;
    flex-direction: column;

    &.disabled {
      opacity: 0.5;
    }
    &.boss-container {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    }
  }
  .title-container {
    width: 100%;
    display: inline-block;
    text-align: center;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
  }
  .boss-container {
    .title-container {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
  .bopis-container {
    .title-container {
      margin-top: 5px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        margin-top: 3px;
      }
    }
  }

  .DTBoxItem:not(:last-child) {
    margin-right: 10px;
  }
  .DTBoxItem:last-child {
    overflow: visible;
  }
  .icon-box {
    width: 21px;
    height: 21px;
    margin: 0 auto;
    display: inline-block;
    text-align: center;
    img {
      height: 100%;
    }
  }

  .DTBoxItemContainer .disabled .banner-wrapper .promo-wrapper span {
    color: ${blackDisabledState};
  }

  .PickupPromotionBannerContainer {
    .banner-wrapper .promo-wrapper {
      display: inline-block;
      width: 100%;
      background: none;
    }
    .triangle-left {
      display: none;
    }
    .banner-wrapper {
      display: initial;
      position: absolute;
      width: auto;
      bottom: -24px;
      left: 50%;
      transform: translate(-50%, -50%);
      padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        line-height: 13px;
        max-width: 102%;
        padding: 0;
        ${(props) => (props.isCanada ? 'bottom: -50%;' : '')}
      }
      ${(props) => (props.isCanada ? 'bottom:-60%;' : '')}
      white-space: nowrap;
    }
    .banner-wrapper .promo-wrapper > .richtextCss > div > div {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12} !important;
      margin-bottom: 3px;
      span {
        font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
        font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold} !important;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs10} !important;
      }
    }
  }

  .DTBoxItemContainer .banner-wrapper {
    background: ${(props) => props.theme.colorPalette.yellow[500]};
  }

  .selected-method {
    &.disabled {
      .radio-button-checked {
        display: none;
      }
    }
    .boss-container {
      .title-container {
        margin-top: 5px;
      }
    }
    .bopis-container {
      .title-container {
        margin-top: 2px;
      }
    }
    .banner-wrapper {
      bottom: -43%;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      .banner-wrapper {
        bottom: -40%;
      }
    }
  }

  .DTBoxItemContainer .disabled .banner-wrapper {
    background: ${yellowDisabledState};
  }

  .DTBoxItemContainer {
    flex: 1;
    .selected-method {
      box-shadow: 0 5px 12px 0 rgba(0, 0, 0, 0.12);
      border: solid 3px ${(props) => props.theme.colorPalette.blue[500]};
      box-sizing: border-box;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
  }

  .DTBoxItemContainerDisabled {
    .selected-method {
      border: solid 1px ${(props) => props.theme.colorPalette.red[500]};
    }
  }

  .padding-left-5 {
    padding-left: 5px;
  }
  .padding-horizontal-5 {
    padding: 0 5px;
  }
  .padding-top-10 {
    padding-top: 10px;
  }

  .disabled .radio-button {
    opacity: 0;
    width: 1px;
    height: 1px;
    overflow: hidden;
  }

  .disabled.common-select-box-css {
    pointer-events: none;
  }

  .disabled .subText {
    color: ${(props) => props.theme.colorPalette.gray[800]};
  }

  img.disabled-icon {
    height: 21px;
  }
  .store-address {
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    text-transform: capitalize;
  }
`;

export default styles;
