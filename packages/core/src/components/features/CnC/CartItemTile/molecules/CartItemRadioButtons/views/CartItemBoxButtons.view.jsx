// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  getLocator,
  getIconPath,
  getViewportInfo,
  getAPIConfig,
  getTranslateDateInformation,
} from '@tcp/core/src/utils';
import { BodyCopy, LabeledRadioButton, Image, Anchor } from '@tcp/core/src/components/common/atoms';
import PickupPromotionBanner from '@tcp/core/src/components/common/molecules/PickupPromotionBanner';
import EddComponent from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { fromJS } from 'immutable';
import withStyles from '../../../../../../common/hoc/withStyles';
import style from '../styles/CartItemBoxButtons.style';
import CARTPAGE_CONSTANTS from '../../../CartItemTile.constants';
import CONSTANTS from '../../../../Checkout/Checkout.constants';
import {
  handleShipToHome,
  handlePickupToggle,
  handleChangeStoreClick,
  hideChangeStore,
} from './CartItemRadioButtons.utils';

class CartItemBoxButtons extends React.Component {
  /**
   * @function showBoss Handles to show BOSS Item or not
   * @param isBossOrder Represents the current product is BOSS or not
   * @param isBossEnabled Represents the Country/State level kill switch for BOSS
   * @memberof CartItemRadioButtons
   */
  showBoss = (isBossOrder, isBossEnabled) => isBossOrder || isBossEnabled;

  /**
   * @function showBopis Handles to show BOPIS Item or not
   * @param isBopisOrder Represents the current product is BOPIS or not
   * @param isBopisEnabled Represents the Country/State level kill switch for BOPIS
   * @memberof CartItemRadioButtons
   */
  showBopis = (isBopisOrder, isBopisEnabled) => isBopisOrder || isBopisEnabled;

  /**
   * @function renderBossBanner
   * @param {bool} isBossItem Represents if the current item is Boss Item or not
   * @param {string} onlineClearanceMessage Represents the Online only message
   * @memberof CartItemRadioButtons
   */
  renderBossBanner = (isBossItem, bossDisabled) => {
    const {
      productDetail: {
        itemInfo: { itemBrand },
      },
    } = this.props;
    return isBossItem ? (
      <div className={`${bossDisabled ? 'disabled' : ''}`}>
        <div className="PickupPromotionBannerContainer">
          <PickupPromotionBanner bossBanner itemBrand={itemBrand} />
        </div>
      </div>
    ) : null;
  };

  /**
   * @function renderEDD
   * @param {bool} isECOMOrder Represents if the current item is is ECom
   * @memberof CartItemRadioButtons
   */
  renderEDD = (isECOMOrder, placeHolderText) => {
    const {
      productDetail: {
        productInfo: { itemPartNumber },
      },
      newBag,
      cartOrderItems,
    } = this.props;
    const { isDesktop } = getViewportInfo();
    return isECOMOrder && cartOrderItems && cartOrderItems.size > 0 ? (
      <EddComponent
        fontSize={isDesktop ? 'fs12' : 'fs11'}
        textAlign="center"
        itemIdprop={itemPartNumber}
        newBag={newBag}
        className="edd-component"
        placeHolderText={placeHolderText}
      />
    ) : null;
  };

  /**
   * @function renderBossDates Renders the boss dates values in the format as required.
   * @param {bool} isBossItem Represents if the current item is Boss Item or not
   * @returns {JSX} renders the boss dates unit
   * @memberof CartItemRadioButtons
   */
  renderBossDates = (placeholderMsg) => {
    let {
      productDetail: {
        miscInfo: { bossStartDate, bossEndDate },
      },
    } = this.props;

    const defaultStore = getLocalStorage('defaultStore');

    if (bossStartDate === null && defaultStore) {
      const defaultStoreObj = JSON.parse(defaultStore);
      const { storeBossInfo: { startDate, endDate } = {} } = defaultStoreObj;
      const { language } = getAPIConfig();
      bossStartDate = startDate && getTranslateDateInformation(startDate, language);
      bossEndDate = endDate && getTranslateDateInformation(endDate, language);
      bossStartDate = bossStartDate && fromJS(bossStartDate);
      bossEndDate = bossEndDate && fromJS(bossEndDate);
    }

    return bossStartDate && bossEndDate ? (
      <>
        <BodyCopy
          fontWeight="semibold"
          component="p"
          fontFamily="secondary"
          color="green.500"
          fontSize="fs11"
          textAlign="center"
          className="boss-dates"
        >
          {`${bossStartDate.get('month')} ${bossStartDate.get('date')} - ${bossEndDate.get(
            'month'
          )} ${bossEndDate.get('date')}`}
        </BodyCopy>
      </>
    ) : (
      <BodyCopy
        fontWeight="semibold"
        component="p"
        fontFamily="secondary"
        color="green.500"
        fontSize="fs11"
        textAlign="center"
        className="boss-dates"
      >
        {placeholderMsg}
      </BodyCopy>
    );
  };

  /**
   * @function renderChangeStore
   * @param {bool} disabled Represents with the current item is disabled or not
   * @param {bool} isBossItem Represents if the current item is Boss Item or not
   * @returns {JSX} Render Change store link.
   * @memberof CartItemRadioButtons
   */
  renderChangeStore = (disabled, isBossItem) => {
    const { labels } = this.props;
    return !disabled || hideChangeStore(isBossItem, this.props) ? (
      <Anchor
        className="elem-pl-LRG changeStoreLink"
        fontSizeVariation="small"
        underline
        anchorVariation="primary"
        fontSize="fs12"
        fontFamily="secondary"
        onClick={(e) => {
          e.preventDefault();
          handleChangeStoreClick(this.props);
        }}
      >
        {`${labels.changeStore}`}
      </Anchor>
    ) : null;
  };

  renderalertTriangle = () => {
    return (
      <Image
        alt="disabled"
        className="disabled-icon"
        src={getIconPath('alert-triangle')}
        width="21px"
        height="21px"
      />
    );
  };

  /**
   * @function renderRadioItem
   * @param {Object} Object Boss Bopis and STH settings object to draw different scenarios.
   * @return {JSX} Render each radio button line item with different variations.
   * @memberof CartItemRadioButtons
   */
  renderRadioItem = ({ disabled, radioText, isBossItem, bossEddPlaceHolder, isSelected }) => {
    return (
      <div className={`main-container boss-container ${disabled ? 'disabled' : ''}`}>
        <BodyCopy component="div" className="title-container">
          <BodyCopy
            dataLocator={getLocator(`cart_item_${isBossItem ? 'cartNoRush' : 'pickup_today'}`)}
            color={disabled ? 'gray.800' : 'gray.900'}
            fontSize="fs14"
            fontFamily="secondary"
            className="tileHeading"
          >
            {radioText}
          </BodyCopy>
          {disabled && isSelected ? (
            this.renderalertTriangle()
          ) : (
            <BodyCopy component="div" className="subtitle-container">
              {this.renderBossDates(bossEddPlaceHolder)}
            </BodyCopy>
          )}
        </BodyCopy>
      </div>
    );
  };

  renderBopisRadioItem = ({ disabled, radioText, isBossItem, needASAPCartTile, isSelected }) => {
    return (
      <div className={`main-container bopis-container ${disabled ? 'disabled' : ''}`}>
        <BodyCopy component="div" className="title-container">
          <BodyCopy
            dataLocator={getLocator(`cart_item_${isBossItem ? 'cartNoRush' : 'pickup_today'}`)}
            color={disabled ? 'gray.800' : 'gray.900'}
            fontSize="fs14"
            fontFamily="secondary"
            className="tileHeading"
          >
            {radioText}
          </BodyCopy>
          {disabled && isSelected ? (
            this.renderalertTriangle()
          ) : (
            <BodyCopy component="div" className="subtitle-container">
              <BodyCopy
                color={disabled ? 'gray.800' : 'gray.900'}
                fontSize="fs12"
                fontFamily="secondary"
                className="subText"
              >
                {needASAPCartTile}
              </BodyCopy>
            </BodyCopy>
          )}
        </BodyCopy>
      </div>
    );
  };

  renderShipToHomeRadioItem = ({
    onlineClearanceMessage,
    disabled,
    radioText,
    isBossItem,
    labels,
    isSelected,
    isEcomSoldOutorUnavailable,
  }) => {
    return (
      <div className={`main-container ${disabled ? 'disabled' : ''}`}>
        <BodyCopy component="div" className="title-container">
          <BodyCopy
            dataLocator={getLocator(`cart_item_${isBossItem ? 'cartNoRush' : 'pickup_today'}`)}
            color={disabled ? 'gray.800' : 'gray.900'}
            fontSize="fs14"
            fontFamily="secondary"
            className="tileHeading"
          >
            {radioText}
          </BodyCopy>

          {onlineClearanceMessage && (
            <BodyCopy
              className="elem-pl-MED"
              component="span"
              fontFamily="secondary"
              fontSize="fs12"
              color="gray[800]"
            >
              {onlineClearanceMessage}
            </BodyCopy>
          )}
          {isEcomSoldOutorUnavailable && isSelected ? (
            <BodyCopy component="div" className="icon-box">
              {this.renderalertTriangle()}
            </BodyCopy>
          ) : (
            this.renderEDD(true, labels.freeShippingCartTile)
          )}
        </BodyCopy>
      </div>
    );
  };

  getStoreAddressinfo = (city, state) => {
    return (
      <BodyCopy
        fontWeight="normal"
        component="div"
        fontFamily="secondary"
        fontSize="fs12"
        className="text-ellipsis store-address"
      >
        {`${city}, ${state}`}
      </BodyCopy>
    );
  };

  renderStoreDetails = (store, isBOSSOrder, isBOPISOrder, isBopisEnabled, isBossEnabled) => {
    const { bossState, bossCity, bopisState, bopisCity } = this.props;
    return (
      <>
        {isBOPISOrder || isBOSSOrder ? (
          <div
            className={`stores-container ${isBossEnabled && !isBopisEnabled ? 'only-boss' : ''} ${
              !isBossEnabled && isBopisEnabled ? 'only-bopis' : ''
            }`}
          >
            {isBopisEnabled && (
              <BodyCopy
                className={`${isBOPISOrder ? 'show-store' : 'hide-store'} store-details-container`}
                color="gray.800"
                fontFamily="secondary"
                fontSize="fs10"
              >
                <BodyCopy
                  fontWeight="bold"
                  component="span"
                  fontFamily="secondary"
                  fontSize="fs12"
                  className="text-ellipsis"
                >
                  {store}
                  {this.getStoreAddressinfo(bopisCity, bopisState)}
                </BodyCopy>
                {this.renderChangeStore(false, isBOSSOrder)}
              </BodyCopy>
            )}
            {isBossEnabled && (
              <BodyCopy
                className={`${isBOSSOrder ? 'show-store' : 'hide-store'} store-details-container`}
                color="gray.800"
                fontFamily="secondary"
                fontSize="fs10"
              >
                <BodyCopy
                  fontWeight="bold"
                  component="span"
                  fontFamily="secondary"
                  fontSize="fs12"
                  className="text-ellipsis"
                >
                  {store}
                  {this.getStoreAddressinfo(bossCity, bossState)}
                </BodyCopy>
                {this.renderChangeStore(false, isBOSSOrder)}
              </BodyCopy>
            )}
          </div>
        ) : null}
      </>
    );
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  render() {
    const {
      className,
      labels,
      productDetail,
      isBossEnabled,
      isBopisEnabled,
      isEcomSoldout,
      isECOMOrder,
      isBOPISOrder,
      isBOSSOrder,
      noBopisMessage,
      noBossMessage,
      bossDisabled,
      bopisDisabled,
      cartOrderItems,
    } = this.props;
    const { store, availability } = productDetail.miscInfo;
    const radioGroupName = `ship-it-${productDetail.itemInfo.itemId}`;
    const commonSelectBox = 'common-select-box-css';
    const selectedMethod = 'selected-method';
    const isECOMUnavailable = availability !== CARTPAGE_CONSTANTS.AVAILABILITY.OK;
    const isEcomSoldOutorUnavailable = isEcomSoldout || isECOMUnavailable;
    const noRushIconPath = `bag-no-rush${isBOSSOrder ? '-blue' : '-black'}`;
    const pickupIconPath = `bag-pick-up-today${isBOPISOrder ? '-blue' : '-black'}`;
    const shippingIconPath = `bag-ship-to-home${isECOMOrder ? '-blue' : '-black'}`;
    const selectedMethodContainer = `selectedMethod-container`;
    const isCartPage = true;
    const { isDesktop } = getViewportInfo();
    return (
      <div className={`${className} main-container DTBoxContainer`}>
        <div className={`${className} DTBoxContainer`}>
          <div
            className={`DTBoxItemContainer ${
              isEcomSoldOutorUnavailable ? 'DTBoxItemContainerDisabled' : ''
            }`}
          >
            <LabeledRadioButton
              className={`${[
                'normal-select-box',
                commonSelectBox,
                isECOMOrder && selectedMethod,
                isEcomSoldout && 'disabled',
              ].join(' ')} DTBoxItem ${selectedMethod ? selectedMethodContainer : ''}`}
              name={radioGroupName}
              checked={isECOMOrder}
              disabled={isEcomSoldout}
              onClick={() => {
                handleShipToHome(this.props, isCartPage, isECOMOrder, isEcomSoldout);
              }}
              data-locator={getLocator('cart_item_ship_to_home_radio_button')}
            >
              {this.renderShipToHomeRadioItem({
                isSelected: isECOMOrder,
                onlineClearanceMessage: false,
                store,
                disabled: isEcomSoldout,
                radioText: labels.ecomShipping,
                isBossItem: false,
                isEcomItem: true,
                labels,
                isECOMOrder,
                shippingIconPath,
                cartOrderItems,
                isEcomSoldOutorUnavailable,
              })}
            </LabeledRadioButton>
          </div>

          {this.showBopis(isBOPISOrder, isBopisEnabled) && (
            <ClickTracker
              clickData={{
                customEvents: ['event131', 'event69'],
                eventName: 'pickup modal open click',
              }}
              className={`DTBoxItemContainer ${bopisDisabled ? 'DTBoxItemContainerDisabled' : ''}`}
            >
              <LabeledRadioButton
                className={`${[
                  'normal-select-box',
                  commonSelectBox,
                  isBOPISOrder && selectedMethod,
                  bopisDisabled && 'disabled',
                ].join(' ')} DTBoxItem ${selectedMethod ? selectedMethodContainer : ''}`}
                name={radioGroupName}
                checked={isBOPISOrder}
                disabled={bopisDisabled}
                data-locator={getLocator('cart_item_pickup_radio_today_button')}
                onClick={() =>
                  !isBOPISOrder && !bopisDisabled
                    ? handlePickupToggle(this.props, CONSTANTS.ORDER_ITEM_TYPE.BOPIS)
                    : ''
                }
              >
                {this.renderBopisRadioItem({
                  isSelected: isBOPISOrder,
                  onlineClearanceMessage: noBopisMessage,
                  store,
                  disabled: bopisDisabled,
                  radioText: labels.bopisPickUpLabel,
                  isBossItem: false,
                  isEcomItem: false,
                  pickupIconPath,
                  needASAPCartTile: bopisDisabled ? labels.notAvailable : labels.getItToday,
                })}
              </LabeledRadioButton>
            </ClickTracker>
          )}
          {this.showBoss(isBOSSOrder, isBossEnabled) && (
            <ClickTracker
              clickData={{
                customEvents: ['event131', 'event69'],
                eventName: 'pickup modal open click',
              }}
              className={`DTBoxItemContainer ${bossDisabled ? 'DTBoxItemContainerDisabled' : ''}`}
            >
              <LabeledRadioButton
                className={`${[
                  'select-box-1',
                  commonSelectBox,
                  isBOSSOrder && selectedMethod,
                  bossDisabled && 'disabled',
                ].join(' ')} DTBoxItem ${selectedMethod ? selectedMethodContainer : ''}`}
                name={radioGroupName}
                checked={isBOSSOrder}
                disabled={bossDisabled}
                data-locator={getLocator('cart_item_no_rush_radio_button')}
                onClick={() =>
                  !isBOSSOrder && !bossDisabled
                    ? handlePickupToggle(this.props, CONSTANTS.ORDER_ITEM_TYPE.BOSS)
                    : ''
                }
              >
                {this.renderBossBanner(true, bossDisabled)}
                {this.renderRadioItem({
                  isSelected: isBOSSOrder,
                  onlineClearanceMessage: noBossMessage,
                  store,
                  disabled: bossDisabled,
                  radioText: labels.noRushPickUp,
                  bossEddPlaceHolder: labels.bossEddPlaceHolder,
                  isBossItem: true,
                  isEcomItem: false,
                  noRushIconPath,
                  isDesktop,
                })}
              </LabeledRadioButton>
            </ClickTracker>
          )}
        </div>
        {this.renderStoreDetails(store, isBOSSOrder, isBOPISOrder, isBopisEnabled, isBossEnabled)}
      </div>
    );
  }
}

CartItemBoxButtons.propTypes = {
  productDetail: PropTypes.shape({
    miscInfo: {},
  }).isRequired,
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  isBossEnabled: PropTypes.bool.isRequired,
  isBopisEnabled: PropTypes.bool.isRequired,
  isEcomSoldout: PropTypes.bool.isRequired,
  isECOMOrder: PropTypes.bool.isRequired,
  isBOPISOrder: PropTypes.bool.isRequired,
  isBOSSOrder: PropTypes.bool.isRequired,
  noBopisMessage: PropTypes.string.isRequired,
  noBossMessage: PropTypes.string.isRequired,
  bossDisabled: PropTypes.bool.isRequired,
  bopisDisabled: PropTypes.bool.isRequired,
  openPickUpModal: PropTypes.bool.isRequired,
  pickupStoresInCart: PropTypes.shape({}).isRequired,
  newBag: PropTypes.bool,
  bossState: PropTypes.string,
  bossCity: PropTypes.string,
  bopisState: PropTypes.string,
  bopisCity: PropTypes.string,
};

CartItemBoxButtons.defaultProps = {
  newBag: false,
  bossState: '',
  bossCity: '',
  bopisState: '',
  bopisCity: '',
};

export default withStyles(CartItemBoxButtons, style);
export { CartItemBoxButtons as CartItemBoxButtonsVanilla };
