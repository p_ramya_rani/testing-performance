// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const selectBoxCss = css`
  ${(props) => (props.customWidth ? `width: ${props.customWidth}` : `width: auto`)};
  .select__input {
    padding: 12px;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    border: solid 1px ${(props) => props.theme.colorPalette.gray[1600]};
    margin-bottom: 0;
  }
`;

export default selectBoxCss;
