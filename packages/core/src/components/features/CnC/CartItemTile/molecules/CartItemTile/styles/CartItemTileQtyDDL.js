// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .mob-qty-selector {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: -4px;
    }
  }

  .loader-style-wrapper {
    width: 77px;
    height: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    border: solid 1px ${(props) => props.theme.colorPalette.gray[1600]};
    align-items: center;
    display: flex;
  }

  .ddl-container {
    justify-content: center;
    flex-grow: 1;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      display: flex;
    }
  }

  .padding-left-6 {
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
  }
  .padding-left-13 {
    padding-left: 13px;
  }
  .product-details {
    margin-bottom: 5px;
  }
  .product-image {
    text-align: center;
  }
  .brand-image {
    text-align: center;
    width: 55px;
  }
  .edit-button {
    padding-left: 10px;
  }
  .padding-top-15 {
    padding-top: 15px;
  }
  .padding-top-30 {
    padding-top: 30px;
  }
  .padding-top-40 {
    padding-top: 26px;

    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding-top: 40px;
    }
  }

  .product-tile-wrapper {
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    flex-wrap: nowrap;
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      position: relative;
    }
    & .product-detail-row {
      margin: 0;
    }
    & .list-price {
      padding-left: 5px;
    }
    & .product-brand-img-wrapper {
      align-items: center;
      width: 103px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        padding: 0 12px 0 4px;
      }
      @media ${(props) => props.theme.mediaQuery.large} {
        width: 105px;
        padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      }
      & .brand-image {
        width: 60px;
      }
    }
    & .product-brand-img-wrapper-sfl {
      @media ${(props) => props.theme.mediaQuery.large} {
        height: auto;
      }
    }

    .color-size-fit-label {
      width: 49px;
    }

    .bag-product-detail-wrapper {
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      display: flex;
      flex-direction: column;
      position: relative;
      width: 100%;

      .label-responsive {
        width: 49px;
        margin-right: 0px;
        padding-right: 0px;
      }

      .value-responsive {
        padding-left: 3px;
        margin-right: 0px;
        text-align: right;
      }
      .label-responsive-wrapper {
        padding-top: 2px;
        display: flex;
        padding-right: 0px;
        /* stylelint-disable */
        span:not(.list-price) {
          font-size: ${(props) => props.theme.fonts.fontSize.listmenu.small}px;
        }
        span.was-price {
          font-size: ${(props) => props.theme.typography.fontSizes.fs14};
        }
        /* stylelint-enable */
      }

      .responsive-edit-css {
        text-decoration: underline;
        padding-top: 0px;
        cursor: pointer;
        justify-content: flex-start;
        display: inline;
      }
      .sflActions {
        float: right;
        color: ${(props) => props.theme.colorPalette.blue[800]};
      }
      .cta-links {
        flex-grow: 4;
        padding-right: 5px;
      }
      .item-action-cta {
        color: ${(props) => props.theme.colorPalette.blue[800]};
      }
    }

    .color-fit-size-separator {
      padding: 0 10px;
      color: ${(props) => props.theme.colorPalette.BUTTON.WHITE.FOCUS};
      display: inline-block;

      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        display: none;
      }
    }

    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      .save-for-later-label {
        margin: ${(props) => props.theme.spacing.ELEM_SPACING.XS} 0;
        width: 100%;
        left: 10px;
        margin-top: 0;
      }
      .color-map-size-fit {
        display: inline-grid;
      }
      .responsive-edit-css {
        text-decoration: underline;
        cursor: pointer;
      }
      .bag-product-detail-wrapper {
        position: static;
      }
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      .color-fit-size-desktop {
        width: auto;
      }
    }

    @media ${(props) => props.theme.mediaQuery.mobile} {
      .save-for-later-label {
        left: 3px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      .save-for-later-label {
        left: 3px;
      }
    }

    .price-label {
      color: ${(props) => props.theme.colorPalette.red[500]};
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
      text-align: right;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        ${(props) => (props.isBagPageSflSection ? 'top: -30px; position: absolute;' : '')};
      }
      @media ${(props) => props.theme.mediaQuery.large} {
        margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
        min-width: 75px;
      }
    }

    .price-label-bag {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        top: 103px;
        right: 0px;
      }
    }
    .Cta-section {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        width: 100%;
      }
    }

    .item-detail-section {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        float: right;
        margin-top: -36px;
      }
    }

    .original-price-label {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      display: block;
      height: 18px;
    }
  }
`;
