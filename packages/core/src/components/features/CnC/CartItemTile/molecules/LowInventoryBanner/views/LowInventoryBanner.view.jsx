// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import style from '../styles/LowInventoryBanner.style';

class LowInventoryBanner extends React.Component {
  toggleEditModal = () => {
    return true;
  };

  render() {
    const { className, lowInventoryMsg } = this.props;

    return (
      <div className={`${className} low-inventory-container`}>
        <BodyCopy className="low-inventory-banner" component="div">
          {lowInventoryMsg}
        </BodyCopy>
      </div>
    );
  }
}

LowInventoryBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  lowInventoryMsg: PropTypes.string.isRequired,
};

export default withStyles(LowInventoryBanner, style);
export { LowInventoryBanner as LowInventoryBannerVanilla };
