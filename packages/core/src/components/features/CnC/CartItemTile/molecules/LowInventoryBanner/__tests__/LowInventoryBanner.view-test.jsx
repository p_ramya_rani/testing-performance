// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { LowInventoryBannerVanilla } from '../views/LowInventoryBanner.view';

describe('LowInventoryBanner Component', () => {
  let component;
  let props;
  beforeEach(() => {
    props = {
      labels: {},
      className: '',
      lowInventoryMsg: '',
    };
  });

  it('LowInventoryBanner should be defined', () => {
    component = shallow(<LowInventoryBannerVanilla {...props} />);
    expect(component).toBeDefined();
  });

  it('LowInventoryBanner should render', () => {
    component = shallow(<LowInventoryBannerVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
