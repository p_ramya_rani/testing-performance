// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import style from './CartItemNewTileUtils';

export default css`
  border-bottom: 2px solid ${(props) => props.theme.colorPalette.gray[300]};
  position: relative;
  ${style}

  span.was-price {
    margin-left: 5px;
    color: ${(props) => props.theme.colorPalette.gray[800]};
    text-decoration: line-through;
  }

  .padding-bottom-20 {
    padding-bottom: 20px;
  }
  .align-product-img {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    @media ${(props) => props.theme.mediaQuery.large} {
      justify-content: space-between;
    }
  }

  .close-icon-image {
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      padding: 2px;
      position: absolute;
      right: 0;
      top: 0;
    }
  }

  .color-fit-size-separator-mobile {
    padding: 0 5px;
    color: ${(props) => props.theme.colorPalette.BUTTON.WHITE.FOCUS};
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      padding: 0 10px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: 0 3px;
    }
  }

  .color-size-fit-label {
    display: inline-block;
  }
  .responsive-edit-css {
    text-decoration: underline;
    padding-top: 2px;
    cursor: pointer;
    display: flex;
    justify-content: flex-start;
    padding-left: 15px;
    z-index: 1;
    outline: none;
  }
  .imageWrapper {
    position: relative;
    width: 116px;
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 82px;
    }

    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 90px;
    }
  }
  .logoWrapper {
    position: relative;
    margin-top: 8px;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    display: flex;
    justify-content: center;
  }
  .product-attributes {
    display: inline-flex;
    flex-wrap: wrap;
    width: 75%;
    margin: 0 13px;
  }
  .editLinkWrapper {
    display: inline-block;
  }

  .crossDeleteIconMiniBag {
    float: right;
    cursor: pointer;
    width: 10px;
    height: 10px;
    padding-top: 0px;
    position: absolute;
    top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  .crossDeleteIconBag {
    float: right;
    cursor: pointer;
    width: 36px;
    height: 36px;
    position: absolute;
    top: 16px;
    right: 20px;
    z-index: 2;
    img {
      display: block;
    }

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      top: 9px;
      right: 12px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      top: 8px;
      right: 11px;
    }
  }

  .product-detail {
    display: flex;
    flex-wrap: wrap;
  }

  .product-detail-row {
    width: 100%;

    p {
      width: 100%;
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: normal;
      color: ${(props) => props.theme.colorPalette.gray[900]};
    }
  }

  .unavailable-header {
    display: flex;
    justify-content: space-between;
  }

  .unavailable-error {
    padding-top: 0px;
    div {
      margin-right: 0px;
      margin-left: 0px;
      display: inline;
      width: 100%;
    }
  }

  .label-responsive {
    width: fit-content;
    padding-right: 22px;
  }

  .label-responsive-price {
    margin-right: 0px;
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .product-detail-section {
      display: ${(props) => (props.showOnReviewPage ? 'flex' : 'inherit')};
      align-items: center;
      flex-wrap: wrap;
      > span {
        padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      }
    }
    .value-clr {
      color: ${(props) => props.theme.colorPalette.gray[900]};
    }

    .product-detail {
      flex-wrap: nowrap;
    }
  }

  .sflActions {
    white-space: nowrap;
    text-decoration: underline;
    cursor: pointer;
  }

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .responsive-edit-css {
      padding-left: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    }
  }

  .productImgBrand {
    width: 100%;
    span {
      padding-right: 55px;
      display: block;
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      color: ${(props) => props.theme.colorPalette.gray[900]};
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      }
    }
  }

  .product-detail-bag {
    display: inline-block;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }

    @media ${(props) => props.theme.mediaQuery.largeOnly} {
      display: flex;
      flex-wrap: wrap;
      width: fit-content;
    }

    span {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};

      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      }

      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      }
    }
  }

  .soldOutLabel {
    width: 89px;
    height: 18px;
    background-color: ${(props) => props.theme.colorPalette.red[500]};
    color: ${(props) => props.theme.colorPalette.white};
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .updateOOSMiniBag {
    cursor: pointer;
    text-decoration: underline;
  }

  .parent- {
    padding-bottom: 0px;
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    .save-for-later-label {
      display: flex;
      justify-content: space-between;
    }
    .padding-left-13 {
      display: flex;
      padding: 0;
    }
  }

  .parent-myBag {
    .save-for-later-label {
      display: flex;
      align-items: center;
      flex-grow: 1;
      padding-top: 15px;

      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        justify-content: space-between;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        justify-content: space-between;
        padding-top: ${(props) => (!props.isBagPageSflSection ? '15px;' : '0px')};
        ${(props) => !props.isBagPageSflSection && 'position:relative'};
      }
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-bottom: 0px;
    }
  }

  .save-for-later-label-cart {
    justify-content: flex-end;
  }

  .save-for-later-label-sfl {
    justify-content: space-between;
    position: relative;
  }

  .cart-item-radio-buttons {
    margin-top: 0;
    padding: 0;
    border: 0;
    align-items: stretch;
    flex-wrap: wrap;
    justify-content: left;
    gap: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    display: flex;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      justify-content: space-around;
    }
  }

  .quantity-size-chart {
    display: none;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    justify-content: center;
    flex-grow: 1;
    @media ${(props) => props.theme.mediaQuery.large} {
      display: flex;
    }
  }
  .container {
    width: 110px;
    height: 35px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
    position: relative;
    border-radius: 4px;
  }
  .horizontalBar,
  .horizontalBarVertical {
    width: 13px;
    height: 1px;
    background-color: ${(props) => props.theme.colorPalette.black};
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .horizontalBarVertical {
    transform: translate(-50%, -50%) rotate(90deg);
  }

  .decreaseButton {
    width: 36px;
    height: 35px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
    &:hover {
      background: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    }
    &.disabled {
      pointer-events: none;
    }
    ${(props) => (props.isCartLoading ? `pointer-events:none;` : '')}
  }

  .selected_qty_value {
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    color: ${(props) => props.theme.colorPalette.black};
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
  }

  .cta-parent {
    display: flex;
  }

  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;
