// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .spinner-container {
    display: block;
    justify-content: center;
    align-items: center;
    z-index: 999;
    margin: 0 19px;
  }

  .spc-spinner {
    display: inline-block;
    position: relative;
    width: 5px;
    height: 5px;
  }
  .spc-spinner div {
    position: absolute;
    width: 10px;
    height: 10px;
    border-radius: 100%;
    background: ${(props) => props.theme.colors.PRIMARY.DARKBLUE};
    top: -3px;
  }
  .spc-spinner div:nth-child(1) {
    left: 0px;
    animation: quantity-spinner-animation 1.2s infinite;
  }
  .spc-spinner div:nth-child(2) {
    animation: quantity-spinner-animation 1.2s infinite;
    animation-delay: 0.2s;
  }
  @keyframes quantity-spinner-animation {
    0% {
      transform: scale(0);
      opacity: 0;
    }
    10% {
      transform: scale(0);
      opacity: 0;
    }
    15% {
      transform: scale(0.3);
      opacity: 0.3;
    }
    20% {
      transform: scale(0.6);
      opacity: 0.6;
    }
    25% {
      transform: scale(0.9);
      opacity: 0.9;
    }
    30% {
      transform: scale(1);
      opacity: 1;
    }
    70% {
      transform: translateX(20px);
      opacity: 1;
    }
    75% {
      transform: translateX(20px) scale(0.9);
      opacity: 0.9;
    }
    80% {
      transform: translateX(20px) scale(0.6);
      opacity: 0.6;
    }
    85% {
      transform: translateX(20px) scale(0.3);
      opacity: 0.3;
    }
    90% {
      transform: translateX(20px) scale(0);
      opacity: 0;
    }
    100% {
      transform: translateX(20px);
      opacity: 0;
    }
  }
`;
