// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  display: inline-block;

  .low-inventory-banner {
    color: ${(props) => props.theme.colorPalette.red[500]};
    background-color: ${(props) => props.theme.colorPalette.red[100]};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS}
      ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    font-size: ${(props) => props.theme.typography.fontSizes.fs10};
    width: fit-content;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  }
`;

export default styles;
