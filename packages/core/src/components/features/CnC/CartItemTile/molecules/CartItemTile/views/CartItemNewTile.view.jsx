/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ItemAvailability from '@tcp/core/src/components/features/CnC/common/molecules/ItemAvailability';
import ErrorMessage from '@tcp/core/src/components/features/CnC/common/molecules/ErrorMessage';
import { getDynamicBadgeQty } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  getLabelValue,
  getIconPath,
  getLocator,
  isCanada,
  getAPIConfig,
  getBrand,
  urlContainsQuery,
  fireEnhancedEcomm,
  removeRecFromStorage,
  getViewportInfo,
} from '@tcp/core/src/utils';
import { PriceCurrency, Modal } from '@tcp/core/src/components/common/molecules';
import { KEY_CODES } from '@tcp/core/src/constants/keyboard.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { CONTROLS_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import EddComponent from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent';
import MiniBagSelect from '@tcp/web/src/components/features/CnC/MiniBag/molecules/MiniBagSelectBox/MiniBagSelectBox';
import ProductEditForm from '../../../../../../common/molecules/ProductCustomizeForm';
import CartItemBoxButtons from '../../CartItemRadioButtons/views/CartItemBoxButtons.view';
import {
  Image,
  Row,
  BodyCopy,
  Col,
  Anchor,
  Button,
  SelectBox,
} from '../../../../../../common/atoms';
import getModifiedString from '../../../utils';
import styles from '../styles/CartItemNewTile.style';
import { modalStyles } from '../styles/CartItemNewTileUtils';
import CARTPAGE_CONSTANTS from '../../../CartItemTile.constants';
import config from '../../../../../../common/molecules/ProductDetailImage/config';
import DamImage from '../../../../../../common/atoms/DamImage';
import selectBoxStyle from '../styles/SelectBox.style';
import {
  getBossBopisFlags,
  isEcomOrder,
  isBopisOrder,
  isBossOrder,
  isSoldOut,
  noBossBopisMessage,
  checkBossBopisDisabled,
  showRadioButtons,
  hideEditBossBopis,
  getBOSSUnavailabilityMessage,
  getBOPISUnavailabilityMessage,
  getSTHUnavailabilityMessage,
  getPrices,
} from './CartItemTile.utils';
import { getProductListToPath } from '../../../../../browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import LowInventoryBanner from '../../LowInventoryBanner/index';
import UpdatingSpinner from './UpdatingSpinner.view';

/**
 * LinkWrapper component is to wrap anchor component to decide whether child needs anchor tag or not
 * @param {object} props
 */
const LinkWrapper = (props) => {
  LinkWrapper.propTypes = {
    pdpToPath: PropTypes.string.isRequired,
    pdpAsPathUrl: PropTypes.string.isRequired,
    disableLink: PropTypes.bool.isRequired,
    noWrap: PropTypes.bool.isRequired,
    IsSlugPathAdded: PropTypes.bool,
    children: PropTypes.node.isRequired,
  };

  LinkWrapper.defaultProps = {
    IsSlugPathAdded: false,
  };

  const { pdpToPath, pdpAsPathUrl, disableLink, children, noWrap, IsSlugPathAdded } = props;
  return noWrap ? (
    children
  ) : (
    <Anchor
      to={pdpToPath}
      asPath={pdpAsPathUrl}
      noLink={disableLink}
      IsSlugPathAdded={IsSlugPathAdded}
    >
      {children}
    </Anchor>
  );
};

class CartItemNewTile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
      quantityLabelValue: 'Qty: 1',
      openEditModal: false,
      quantityUpdating: false,
    };
  }

  componentDidUpdate(prevProps) {
    const {
      isBagPageSflSection,
      toggleBossBopisError,
      productDetail: {
        itemInfo: { itemId },
      },
      isCartLoading,
    } = this.props;
    if (
      !isBagPageSflSection &&
      toggleBossBopisError &&
      itemId === toggleBossBopisError.itemId &&
      (prevProps.toggleBossBopisError === null ||
        prevProps.toggleBossBopisError.errorMessage !== toggleBossBopisError.errorMessage)
    ) {
      const timer = setTimeout(() => {
        this.handleEditCartItemWithStore(toggleBossBopisError.targetOrderType);
        clearTimeout(timer);
      });
    }
    if (prevProps.isCartLoading !== isCartLoading && !isCartLoading) {
      this.setState({ quantityUpdating: false });
    }
  }

  componentWillUnmount() {
    this.clearToggleErrorState();
  }

  clearToggleErrorState = () => {
    const { pageView, clearToggleError } = this.props;
    if (pageView === 'myBag') {
      clearToggleError();
    }
  };

  toggleFormVisibility = () => {
    const { isEdit } = this.state;
    this.setState({ isEdit: !isEdit });
    this.toggleEditLinkVisibility();
  };

  toggleEditLinkVisibility = () => {
    const { toggleEditAllowance } = this.props;
    toggleEditAllowance();
  };

  handleEditCartItemMiniBag = (pageView, itemBrand, productNumber) => {
    const productNum = productNumber.slice(0, productNumber.indexOf('_'));
    this.toggleFormVisibility();
    const { getProductSKUInfo } = this.props;
    getProductSKUInfo({ productNum, itemBrand });
  };

  handleEditCartItem = (pageView, itemBrand, productNumber, isBagPageSflSection = false) => {
    if (pageView !== 'myBag') {
      this.handleEditCartItemMiniBag(pageView, itemBrand, productNumber);
    } else {
      const { onQuickViewOpenClick, productDetail } = this.props;
      const { itemId, qty, color, size, fit, isGiftItem, offerPrice, listPrice, name, imagePath } =
        productDetail.itemInfo;
      const {
        productInfo: { skuId, generalProductId, pdpUrl, isGiftCard, tcpStyleType, tcpStyleQty },
      } = productDetail;
      onQuickViewOpenClick({
        fromBagPage: pageView === 'myBag',
        colorProductId: productNumber,
        isSflProduct: isBagPageSflSection,
        pdpUrl,
        orderInfo: {
          orderItemId: itemId,
          selectedQty: qty,
          selectedColor: color,
          selectedSize: size,
          selectedFit: fit,
          itemBrand,
          skuId: isGiftItem ? generalProductId : skuId,
          name,
          imagePath,
          uniqueId: productNumber,
          listPrice,
          offerPrice,
          isGiftCard,
          tcpStyleQty,
          tcpStyleType,
        },
      });
    }
  };

  /**
   *
   * @method handleEditCartItemWithStore
   * @description this method handles edit for cart item for boss/bopis item
   * @memberof CartItemTile
   */
  handleEditCartItemWithStore = (
    changeStoreType,
    openSkuSelectionForm = false,
    openRestrictedModalForBopis = false,
    isPickUpWarningModal = false
  ) => {
    const { onPickUpOpenClick, productDetail, orderId } = this.props;
    const { itemId, qty, color, size, fit, itemBrand, offerPrice, listPrice, name, imagePath } =
      productDetail.itemInfo;
    const { store, orderItemType } = productDetail.miscInfo;
    const { productPartNumber, isGiftCard, tcpStyleType, tcpStyleQty } = productDetail.productInfo;
    const isItemShipToHome = !store;
    const isBopisCtaEnabled = changeStoreType === CARTPAGE_CONSTANTS.BOPIS;
    const isBossCtaEnabled = changeStoreType === CARTPAGE_CONSTANTS.BOSS;
    const alwaysSearchForBOSS = changeStoreType === CARTPAGE_CONSTANTS.BOSS;
    onPickUpOpenClick({
      colorProductId: productPartNumber,
      orderInfo: {
        orderItemId: itemId,
        Quantity: qty,
        color,
        Size: size,
        Fit: fit,
        orderId,
        orderItemType,
        itemBrand,
        name,
        imagePath,
        uniqueId: productPartNumber,
        listPrice,
        offerPrice,
        isGiftCard,
        tcpStyleQty,
        tcpStyleType,
      },
      openSkuSelectionForm,
      isBopisCtaEnabled,
      isBossCtaEnabled,
      isItemShipToHome,
      alwaysSearchForBOSS,
      openRestrictedModalForBopis,
      isPickUpWarningModal,
      newBag: true,
      isBopisPickup: isBopisCtaEnabled,
    });
  };

  callEditMethod = () => {
    const { productDetail, pageView, isBagPageSflSection } = this.props;
    const {
      miscInfo: { orderItemType },
    } = productDetail;
    if (orderItemType === CARTPAGE_CONSTANTS.ECOM || isBagPageSflSection) {
      this.handleEditCartItem(
        pageView,
        productDetail.itemInfo.itemBrand,
        productDetail.productInfo.productPartNumber,
        isBagPageSflSection
      );
    } else if (pageView === 'myBag') {
      const openSkuSelectionForm = true;
      this.handleEditCartItemWithStore(orderItemType, openSkuSelectionForm);
    } else {
      this.handleEditCartItemMiniBag(
        pageView,
        productDetail.itemInfo.itemBrand,
        productDetail.productInfo.productPartNumber
      );
    }
    this.setState({ openEditModal: false });
  };

  handleKeyDown = (event, callback) => {
    const { KEY_ENTER, KEY_SPACE } = KEY_CODES;
    const { which } = event;
    if (which === KEY_ENTER || which === KEY_SPACE) {
      callback();
    }
  };

  handleMoveItemtoSaveList = () => {
    const {
      productDetail,
      sflItemsCount,
      sflMaxCount,
      isCondense,
      isGenricGuest,
      addItemToSflList,
      setCartItemsSflError,
      labels,
      pageView,
      setClickAnalyticsData,
    } = this.props;
    const {
      itemInfo: { itemId, isGiftItem, color, name, offerPrice, size, listPrice, itemBrand },
      productInfo: { skuId, generalProductId, upc, productPartNumber, multiPackItems },
      miscInfo: { store },
    } = productDetail;
    const productCatEntryId = isGiftItem ? generalProductId : skuId;
    const userInfoRequired = isGenricGuest && isGenricGuest.get('userId') && isCondense; // Flag to check if getRegisteredUserInfo required after SflList
    const isMiniBag = pageView !== 'myBag';
    this.clearToggleErrorState();

    if (sflItemsCount >= sflMaxCount) {
      return setCartItemsSflError(labels.sflMaxLimitError);
    }
    let catEntryId = productCatEntryId;
    if (multiPackItems) {
      catEntryId = [
        {
          catEntryId: productCatEntryId,
          multiPackItems,
        },
      ];
    }

    const payloadData = { itemId, catEntryId, userInfoRequired, isMiniBag };
    const productsData = {
      color,
      id: itemId,
      name,
      price: offerPrice,
      extPrice: offerPrice,
      sflExtPrice: offerPrice,
      listPrice,
      partNumber: productPartNumber,
      size,
      upc,
      sku: skuId.toString(),
      pricingState: 'full price',
      colorId: generalProductId,
      storeId: store,
      prodBrand: itemBrand,
    };
    setClickAnalyticsData({
      customEvents: ['event138'],
      products: [productsData],
      eventName: 'Save for Later',
      event139: offerPrice,
    });
    return addItemToSflList({ ...payloadData });
  };

  removeSflItem = () => {
    const { productDetail, startSflItemDelete } = this.props;
    const {
      itemInfo: { isGiftItem },
      productInfo: { skuId, generalProductId, productPartNumber },
    } = productDetail;
    const catEntryId = isGiftItem ? generalProductId : skuId;
    removeRecFromStorage(productPartNumber);
    const payloadData = { catEntryId };
    this.clearToggleErrorState();
    return startSflItemDelete({ ...payloadData });
  };

  moveToBagSflItem = () => {
    const { productDetail, startSflDataMoveToBag, setClickAnalyticsData } = this.props;
    const {
      itemInfo: { itemId, isGiftItem, color, name, offerPrice, itemBrand },
      productInfo: { skuId, generalProductId, productPartNumber, multiPackItems },
      miscInfo: { store },
    } = productDetail;
    const catEntryId = isGiftItem ? generalProductId : skuId;

    const payloadData = { itemId, catEntryId, multiPackItems };
    this.clearToggleErrorState();
    const productsData = {
      color,
      id: itemId,
      name,
      price: offerPrice,
      extPrice: offerPrice,
      offerPrice,
      partNumber: productPartNumber,
      sku: skuId.toString(),
      pricingState: 'full price',
      colorId: generalProductId,
      storeId: store,
      prodBrand: itemBrand,
    };
    setClickAnalyticsData({
      customEvents: ['event140'],
      products: [productsData],
      eventName: 'Move to Bag',
      event142: offerPrice,
    });
    return startSflDataMoveToBag({ ...payloadData });
  };

  handleSubmit = (itemId, skuId, quantity, itemPartNumber, variantNo, selectedSize, formValue) => {
    const { updateCartItem, isMiniBagOpen, editableProductMultiPackInfo = {} } = this.props;
    const {
      multiPackInfo: {
        newPartNumber: multipackProduct,
        multiPackCount,
        getAllMultiPack,
        getAllNewMultiPack,
      } = {},
      tcpMultiPackReferenceUSStore: partIdInfo,
    } = editableProductMultiPackInfo;
    const skuInfo = { ...formValue, skuId };
    this.clearToggleErrorState();
    updateCartItem(
      itemId,
      skuId,
      quantity,
      itemPartNumber,
      variantNo,
      isMiniBagOpen,
      multipackProduct,
      multiPackCount,
      selectedSize,
      getAllNewMultiPack,
      getAllMultiPack,
      skuInfo,
      partIdInfo
    );
    this.toggleFormVisibility();
  };

  getBossBopisDetailsForMiniBag = (productDetail, labels) => {
    return (
      <Col className="padding-left-13" colSize={{ small: 4, medium: 6, large: 8 }}>
        {productDetail.miscInfo.store && (
          <BodyCopy
            fontFamily="secondary"
            color="gray.600"
            component="span"
            fontSize="fs10"
            fontWeight={['extrabold']}
          >
            {getModifiedString(
              labels,
              productDetail.miscInfo.store,
              productDetail.miscInfo.orderItemType,
              productDetail.miscInfo.bossStartDate,
              productDetail.miscInfo.bossEndDate
            )}
          </BodyCopy>
        )}
      </Col>
    );
  };

  getBadgeDetails = (productDetail) => {
    return (
      <Row className="product-detail-row">
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <BodyCopy
            fontFamily="secondary"
            className="item-tag"
            tag="span"
            fontSize="fs10"
            fontWeight={['extrabold']}
            dataLocator="addedtobag-productname"
          >
            {productDetail.miscInfo.badge}
          </BodyCopy>
        </Col>
      </Row>
    );
  };

  renderSflActionsLinks = (isCTA) => {
    const { productDetail, isShowSaveForLater, labels, isBagPageSflSection } = this.props;
    const { isEdit } = this.state;
    if (isEdit) return null;
    if (
      !isBagPageSflSection &&
      productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY.OK &&
      isShowSaveForLater
    ) {
      return (
        <>
          {isCTA ? (
            <ClickTracker name="Save_for_Later" as="span">
              <Anchor
                dataLocator="saveForLaterLink"
                onClick={(e) => {
                  e.preventDefault();
                  this.handleMoveItemtoSaveList();
                }}
                noLink
                className="sflActions"
                aria-label={`Save for later ${productDetail.itemInfo.name}`}
              >
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs12"
                  component="span"
                  fontWeight={['semibold']}
                  className="item-action-cta"
                >
                  {labels.saveForLaterLink}
                </BodyCopy>
              </Anchor>
            </ClickTracker>
          ) : (
            <ClickTracker name="Save_for_Later" className="cartTile-saveForLater-wrapper">
              <Button
                className="cartTile-btn cartTile-saveForLater"
                fullWidth
                buttonVariation="fixed-width"
                dataLocator="saveForLaterLink"
                onClick={(e) => {
                  e.preventDefault();
                  this.handleMoveItemtoSaveList();
                }}
                aria-label={`Save for later ${productDetail.itemInfo.name}`}
              >
                {labels.saveForLaterLink}
              </Button>
            </ClickTracker>
          )}
        </>
      );
    }
    if (
      isBagPageSflSection &&
      productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY.OK
    ) {
      return (
        <ClickTracker name="Move_to_Bag">
          <Button
            className="cartTile-btn cartTile-moveToBag"
            fullWidth
            buttonVariation="fixed-width"
            onClick={(e) => {
              e.preventDefault();
              this.moveToBagSflItem();
            }}
            aria-label={`Move to bag ${productDetail.itemInfo.name}`}
          >
            {labels.moveToBagLink}
          </Button>
        </ClickTracker>
      );
    }
    return null;
  };

  removeCartItem = () => {
    const {
      removeCartItem,
      pageView,
      productDetail,
      isGenricGuest,
      isCondense,
      isBagPageSflSection,
      setClickAnalyticsData,
      isMultipackProduct,
      currencyExchange,
    } = this.props;
    const {
      itemInfo: { itemId, isGiftItem, itemBrand },
      productInfo: { skuId, generalProductId, productPartNumber },
      miscInfo: { orderItemType, categoryName },
    } = productDetail;
    const { itemInfo, productInfo } = productDetail;
    const catEntryId = isGiftItem ? generalProductId : skuId;
    const userInfoRequired = isGenricGuest && isGenricGuest.get('userId') && isCondense; // Flag to check if getRegisteredUserInfo required after SflList

    this.clearToggleErrorState();
    setClickAnalyticsData({
      eventName: 'remove Cart',
      products: [{ ...itemInfo, ...productInfo }],
    });
    removeRecFromStorage(productPartNumber);
    removeCartItem({
      itemId,
      pageView,
      catEntryId,
      userInfoRequired,
      isBagPageSflSection,
      itemBrand,
      orderItemType,
      isMultipackProduct,
    });
    const removedProduct = {
      name: itemInfo.name,
      id: itemId,
      price: itemInfo.price,
      brand: itemBrand,
      category: categoryName || '',
      variant: itemInfo.color,
      quantity: itemInfo.qty,
    };
    fireEnhancedEcomm({
      eventName: 'removeFromCart',
      productsObj: [removedProduct],
      eventType: 'remove',
      pageName: 'shopping bag',
      currCode: currencyExchange && currencyExchange.length && currencyExchange[0].id,
    });
  };

  renderEditLink = (isCTA) => {
    const {
      labels,
      showOnReviewPage,
      isBagPageSflSection,
      isEditAllowed,
      productDetail,
      productDetail: {
        miscInfo: { orderItemType, availability },
        itemInfo: { itemBrand },
      },
    } = this.props;

    const { isBossEnabled, isBopisEnabled } = getBossBopisFlags(this.props, itemBrand);
    const isBOPISOrder = isBopisOrder(orderItemType);
    const isBOSSOrder = isBossOrder(orderItemType);
    const isEcomSoldout = isSoldOut(availability);

    const { bossDisabled, bopisDisabled } = checkBossBopisDisabled(
      this.props,
      isBossEnabled,
      isBopisEnabled,
      isEcomSoldout,
      isBOSSOrder,
      isBOPISOrder
    );
    const clickEventdata = {
      customEvents: ['event172'],
      eventName: 'Edit Product',
    };

    return (
      <>
        {showOnReviewPage &&
          !isBagPageSflSection &&
          isEditAllowed &&
          !hideEditBossBopis(isBOSSOrder, bossDisabled, isBOPISOrder, bopisDisabled) && (
            <>
              {isCTA ? (
                <ClickTracker clickData={clickEventdata} as="span">
                  <Anchor
                    role="button"
                    dataLocator={getLocator('cart_item_edit_link')}
                    className="padding-left-10 responsive-edit-css item-action-cta"
                    aria-label={`Edit ${productDetail.itemInfo.name}`}
                    onClick={(e) => {
                      e.preventDefault();
                      this.callEditMethod(e);
                    }}
                    onKeyDown={(e) => this.handleKeyDown(e, this.callEditMethod)}
                    noLink
                  >
                    <BodyCopy
                      fontFamily="secondary"
                      fontSize="fs12"
                      component="span"
                      fontWeight={['semibold']}
                      className="item-action-cta"
                    >
                      {labels.edit}
                    </BodyCopy>
                  </Anchor>
                </ClickTracker>
              ) : (
                <ClickTracker clickData={clickEventdata}>
                  <Button
                    role="button"
                    dataLocator={getLocator('cart_item_edit_link')}
                    className="cartTile-btn cartTile-edit cartTile-edit-responsive"
                    aria-label={`Edit ${productDetail.itemInfo.name}`}
                    onClick={(e) => {
                      e.preventDefault();
                      this.callEditMethod(e);
                    }}
                    onKeyDown={(e) => this.handleKeyDown(e, this.callEditMethod)}
                    fullWidth
                    buttonVariation="fixed-width"
                  >
                    {labels.edit}
                  </Button>
                </ClickTracker>
              )}
            </>
          )}
      </>
    );
  };

  // eslint-disable-next-line complexity
  getItemDetails = (productDetail, labels, pageView) => {
    const { isEdit, quantityLabelValue } = this.state;
    const { isBagPageSflSection, isHideMeatBallAbTest } = this.props;
    const isBagPage = pageView === 'myBag';
    return (
      <Row className={`parent-${pageView}`} fullBleed>
        {!isBagPage && this.getBossBopisDetailsForMiniBag(productDetail, labels)}
        <Col
          className={`save-for-later-label ${
            isBagPageSflSection ? 'save-for-later-label-sfl' : 'save-for-later-label-cart'
          }`}
          colSize={{ small: 1, medium: 4, large: 8 }}
        >
          <div className="cta-parent">
            {productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY.SOLDOUT && (
              <Button
                role="button"
                className="cartTile-btn cartTile-edit update-btn"
                dataLocator={getLocator('cart_item_soldOut_remove')}
                onClick={this.removeCartItem}
                fullWidth
                buttonVariation="fixed-width"
              >
                {labels.removeEdit}
              </Button>
            )}
            {productDetail.miscInfo.availability !== CARTPAGE_CONSTANTS.AVAILABILITY.OK &&
              productDetail.miscInfo.availability !== CARTPAGE_CONSTANTS.AVAILABILITY.SOLDOUT &&
              !isEdit && (
                <Button
                  role="button"
                  dataLocator={getLocator('cart_item_unavailable_update')}
                  className="cartTile-btn cartTile-edit update-btn"
                  onClick={this.callEditMethod}
                  fullWidth
                  buttonVariation="fixed-width"
                >
                  {labels.update}
                </Button>
              )}

            {isBagPage && this.renderEditLink()}
            {this.renderSflActionsLinks()}
          </div>

          {isBagPageSflSection && (
            <>
              <BodyCopy
                className="itemQuantity-desktop"
                fontFamily="secondary"
                component="span"
                fontSize="fs16"
              >
                {quantityLabelValue}
              </BodyCopy>
            </>
          )}
          {this.getQuantityDropDown(isBagPage)}
          {this.getPriceTag({ sectionName: `${this.getClassForPriceTag()}` })}
          {isHideMeatBallAbTest && this.getCTAForMobile()}
        </Col>
      </Row>
    );
  };

  getColorLabel = (productDetail, labels) => {
    return productDetail.itemInfo.isGiftItem === true ? `${labels.design}:` : `${labels.color}:`;
  };

  getSizeLabel = (productDetail, labels) => {
    return productDetail.itemInfo.isGiftItem === true ? `${labels.value}:` : `${labels.size}:`;
  };

  getPointsColor = () => {
    const { isPlcc } = this.props;
    if (isPlcc) {
      return 'blue.B100';
    }
    return 'orange.800';
  };

  getProductItemUpcNumber = (productDetail, isBagPage, showOnReviewPage) => {
    if (isBagPage && showOnReviewPage) {
      return (
        <Row className="product-detail-row">
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs10"
              color="gray.600"
              fontWeight="semiBold"
              dataLocator={getLocator('cart_item_upc')}
            >
              {`UPC: ${productDetail.productInfo.upc}`}
            </BodyCopy>
          </Col>
        </Row>
      );
    }
    return null;
  };

  getSalePrice = (showOnReviewPage, offerPrice, salePrice, isMiniBag) => {
    return !showOnReviewPage || isMiniBag ? offerPrice : salePrice;
  };

  // hide was price on review page
  getPriceCurrency = (showOnReviewPage, wasPrice, listPrice, price, isMiniBag) => {
    if (isMiniBag && listPrice === price) {
      return <></>;
    }
    const priceToShow = isMiniBag ? listPrice : wasPrice;
    return showOnReviewPage && <PriceCurrency price={Number(priceToShow)} />;
  };

  getProductPriceList = (productDetail, pageView, currencyExchange) => {
    const { isBagPageSflSection, showOnReviewPage, isMiniBag } = this.props;
    const { isGiftItem } = productDetail.itemInfo;
    const { salePrice, wasPrice, listPrice, price } = getPrices({
      productDetail,
      currencyExchange,
    });
    const getPriceCurrencyValue = this.getPriceCurrency(
      showOnReviewPage,
      wasPrice,
      listPrice,
      price,
      isMiniBag
    );
    if (isBagPageSflSection) {
      return (
        <>
          <div className="value-responsive">
            {!isGiftItem && listPrice !== price && (
              <BodyCopy
                fontFamily="secondary"
                component="span"
                fontSize="fs12"
                dataLocator={getLocator('cart_was_price')}
                fontWeight={['regular']}
                className="was-price"
              >
                <PriceCurrency price={Number(listPrice)} />
              </BodyCopy>
            )}
          </div>
        </>
      );
    }
    return (
      <>
        <div className="value-responsive">
          {!isGiftItem && wasPrice !== salePrice && (
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs12"
              dataLocator={getLocator('cart_was_price')}
              fontWeight={['regular']}
              className="was-price"
            >
              {getPriceCurrencyValue}
            </BodyCopy>
          )}
        </div>
      </>
    );
  };

  getProductPointsList = (productDetail, isBagPageSflSection, showOnReviewPage) => {
    const { isInternationalShipping, isBrierleyWorking, labels } = this.props;
    if (!isBrierleyWorking) return null;
    return (
      <>
        {!isCanada() &&
          !isInternationalShipping &&
          !isBagPageSflSection &&
          showOnReviewPage &&
          productDetail &&
          productDetail.itemInfo &&
          (productDetail.itemInfo.myPlacePoints || productDetail.itemInfo.myPlacePoints === 0) && (
            <>
              <BodyCopy
                className="color-fit-size-separator"
                fontFamily="secondary"
                component="span"
                fontSize="fs12"
              >
                |
              </BodyCopy>

              <BodyCopy
                fontFamily="secondary"
                component="span"
                fontSize="fs12"
                fontWeight={['extrabold']}
                color={this.getPointsColor()}
                className="productPoint"
                dataLocator={getLocator('cart_item_points')}
              >
                {productDetail.itemInfo.myPlacePoints || 0}
                <BodyCopy
                  className="points-label text-capitalize"
                  fontFamily="secondary"
                  component="span"
                  fontSize="fs12"
                  dataLocator={getLocator('cart_item_points')}
                  color="gray.900"
                >
                  {' '}
                  {labels.ptsCartTile}
                </BodyCopy>
              </BodyCopy>
            </>
          )}
      </>
    );
  };

  getProductFit = (productDetail) => {
    return !productDetail.itemInfo.fit ? ' ' : ` ${productDetail.itemInfo.fit}`;
  };

  getUnavailableHeaderClass = () => {
    const { productDetail } = this.props;
    if (productDetail.miscInfo.availability === 'UNAVAILABLE') {
      return 'unavailable-header';
    }
    return '';
  };

  renderItemQuantity = () => {
    const { isBagPageSflSection, productDetail } = this.props;
    if (isBagPageSflSection) return null;
    return (
      <div>
        <BodyCopy
          className="hideOnDesktop text-capitalize"
          fontFamily="secondary"
          component="span"
          fontSize="fs13"
          color="gray.900"
          dataLocator="addedtobag-productqty"
        >
          {`${productDetail.itemInfo.qty}`}
        </BodyCopy>
      </div>
    );
  };

  renderQtyForMobTab = (clsName) => {
    const { productDetail, isBagPageSflSection, labels } = this.props;
    const showSize = productDetail && productDetail.itemInfo && productDetail.itemInfo.size;
    return (
      <>
        {!isBagPageSflSection && (
          <>
            <div className={`qty-for-mob-tab text-capitalize ${clsName}`}>
              <BodyCopy
                className={`color-fit-size-separator-mobile${showSize ? '' : ' hide-on-mobile'}`}
                fontFamily="secondary"
                component="span"
                fontSize="fs13"
              >
                |
              </BodyCopy>
              <BodyCopy
                className=""
                fontFamily="secondary"
                component="span"
                fontSize="fs13"
                color="gray.900"
                dataLocator="addedtobag-productqty"
              >
                {`${productDetail.itemInfo.qty} ${' '} ${labels.qtyCartTile}`}
              </BodyCopy>
            </div>
          </>
        )}
      </>
    );
  };

  renderSizeAndFit = (sizeClass = '') => {
    const { productDetail } = this.props;
    const showSize = productDetail && productDetail.itemInfo && productDetail.itemInfo.size;
    return (
      <>
        {showSize && (
          <span className={`${sizeClass}`}>
            {this.getDetailSeparator('color-fit-size-separator')}
            <BodyCopy
              className="value-clr"
              fontFamily="secondary"
              component="span"
              fontSize="fs13"
              color="gray.900"
              dataLocator={getLocator('cart_item_size')}
            >
              {`${productDetail.itemInfo.size}`}
              {this.getProductFit(productDetail)}
            </BodyCopy>
          </span>
        )}
        {this.renderQtyForMobTab('hide-on-desktop hide-on-tablet hide-on-mobile')}
      </>
    );
  };

  getSizeAndSeparator = () => {
    const { isBagPageSflSection, showOnReviewPage } = this.props;
    return (
      <>
        {!isBagPageSflSection && (
          <span className="hide-on-desktop hide-on-tablet">
            {this.getDetailSeparator('color-fit-size-separator-mobile')}
            {showOnReviewPage && <>{this.renderSizeAndFit()}</>}
          </span>
        )}
      </>
    );
  };

  getDetailSeparator = (separatorCls = 'color-fit-size-separator') => {
    return (
      <BodyCopy className={separatorCls} fontFamily="secondary" component="span" fontSize="fs13">
        |
      </BodyCopy>
    );
  };

  renderHeartIcon = () => {
    const { isBagPageSflSection, labels, handleAddToWishlist, productDetail } = this.props;
    if (
      !isBagPageSflSection ||
      productDetail.miscInfo.availability !== CARTPAGE_CONSTANTS.AVAILABILITY.OK
    )
      return null;

    return (
      <div className="heartIcon">
        <Image
          alt={getLabelValue(labels, 'lbl_sfl_favIcon', 'bagPage', 'checkout')}
          className="sfl-fav-image"
          src={getIconPath('fav-icon')}
          onClick={handleAddToWishlist}
        />
      </div>
    );
  };

  getCrossIconImage = () => {
    const {
      isBagPageSflSection,
      productDetail: {
        itemInfo: { name, qty },
        productInfo: { variantNo, skuId },
      },
      labels: { removeEdit },
    } = this.props;
    return (
      <Image
        alt={`${removeEdit} ${name}`}
        role="button"
        tabIndex="0"
        className="close-icon-image"
        src={getIconPath('bag-close-icon')}
        onClick={isBagPageSflSection ? this.removeSflItem : this.removeCartItem}
        onKeyDown={(e) =>
          this.handleKeyDown(e, isBagPageSflSection ? this.removeSflItem : this.removeCartItem)
        }
        unbxdattr="RemoveFromCart"
        unbxdparam_sku={skuId}
        unbxdparam_variant={variantNo}
        unbxdparam_qty={qty}
      />
    );
  };

  getProductUnavailabilityStatus = ({
    isEcomSoldout,
    labels,
    isBOSSOrder,
    bossDisabled,
    noBossMessage,
    availability,
    isBOPISOrder,
    bopisDisabled,
    noBopisMessage,
  }) => {
    let unavailableMessage = '';
    if (isEcomSoldout) {
      unavailableMessage = labels.soldOutError;
    } else if (isBOSSOrder) {
      unavailableMessage = getBOSSUnavailabilityMessage(
        bossDisabled,
        noBossMessage,
        availability,
        labels
      );
    } else if (isBOPISOrder) {
      unavailableMessage = getBOPISUnavailabilityMessage(
        bopisDisabled,
        noBopisMessage,
        availability,
        labels
      );
    } else {
      unavailableMessage = getSTHUnavailabilityMessage(availability, labels);
    }
    return unavailableMessage;
  };

  /**
   * @function renderUnavailableErrorMessage
   * @param {Object} settings
   * @returns {JSX} Returns Item Unavailable component with respective variation of text via passed input
   * @memberof CartItemTile
   */
  renderUnavailableErrorMessage = ({
    isEcomSoldout,
    bossDisabled,
    isBOSSOrder,
    bopisDisabled,
    isBOPISOrder,
    noBossMessage,
    noBopisMessage,
    availability,
  }) => {
    const { labels } = this.props;
    const unavailableMessage = this.getProductUnavailabilityStatus({
      isEcomSoldout,
      labels,
      isBOSSOrder,
      bossDisabled,
      noBossMessage,
      availability,
      isBOPISOrder,
      bopisDisabled,
      noBopisMessage,
    });

    return unavailableMessage ? (
      <ItemAvailability
        className="unavailable-error"
        errorMsg={isEcomSoldout ? unavailableMessage : labels.itemUnavailable}
        chooseDiff={!isEcomSoldout ? unavailableMessage : null}
      />
    ) : null;
  };

  /**
   * @function headerAndAvailabilityErrorContainer
   * @param {Object} settings
   * @returns {JSX} Returns Error Message component
   * @memberof CartItemTile
   */
  headerAndAvailabilityErrorContainer = ({
    isEcomSoldout,
    bossDisabled,
    isBOSSOrder,
    bopisDisabled,
    isBOPISOrder,
    noBossMessage,
    noBopisMessage,
    availability,
  }) => {
    const { pageView, showOnReviewPage } = this.props;
    const { isEdit } = this.state;
    return (
      showOnReviewPage && (
        <div className={this.getUnavailableHeaderClass()}>
          {this.renderUnavailableErrorMessage({
            isEcomSoldout,
            bossDisabled,
            isBOSSOrder,
            bopisDisabled,
            isBOPISOrder,
            noBossMessage,
            noBopisMessage,
            availability,
          })}
          {!isEdit && (
            <div className={pageView === 'myBag' ? 'crossDeleteIconBag' : 'crossDeleteIconMiniBag'}>
              {this.getCrossIconImage()}
            </div>
          )}
        </div>
      )
    );
  };

  getProductDetailClass = () => {
    const { showOnReviewPage } = this.props;
    return showOnReviewPage ? 'product-detail' : 'product-detail product-detail-review-page';
  };

  renderReviewPageSection = () => {
    const { showOnReviewPage } = this.props;
    return (
      <>
        {!showOnReviewPage ? (
          <div className="size-and-item-container">
            {this.renderSizeAndFit()}
            {this.renderItemQuantity()}
          </div>
        ) : (
          <>
            {this.renderSizeAndFit()}
            {this.renderItemQuantity()}
          </>
        )}
      </>
    );
  };

  /**
   * @function renderTogglingError Render Toggling error
   * @returns {JSX} Error Component with toggling api error.
   * @memberof CartItemTile
   */
  renderTogglingError = () => {
    const {
      pageView,
      toggleError,
      productDetail: {
        itemInfo: { itemId },
      },
    } = this.props;
    return pageView === 'myBag' && toggleError && itemId === toggleError.itemId ? (
      <ErrorMessage
        className="toggle-error"
        fontSize="fs12"
        fontWeight="extrabold"
        error={toggleError.errorMessage}
      />
    ) : null;
  };

  getPdpToPath = (
    isProductBrandOfSameDomain,
    pdpUrl,
    crossDomain,
    brandSpecificCrossDomain,
    brand
  ) => {
    return isProductBrandOfSameDomain
      ? getProductListToPath(pdpUrl)
      : `${brandSpecificCrossDomain[brand.toUpperCase()]}${pdpUrl}`;
  };

  getPdpAsPathurl = (
    isProductBrandOfSameDomain,
    pdpUrl,
    crossDomain,
    cookieToken,
    brandSpecificCrossDomain,
    brand
  ) => {
    if (isProductBrandOfSameDomain) {
      return pdpUrl;
    }
    if (cookieToken)
      return `${brandSpecificCrossDomain[brand.toUpperCase()]}${pdpUrl}${
        urlContainsQuery(pdpUrl) ? '&' : '?'
      }ct=${cookieToken}`;
    return `${brandSpecificCrossDomain[brand.toUpperCase()]}${pdpUrl}`;
  };

  closeMiniBagMethod = () => {
    const { closeMiniBag } = this.props;
    closeMiniBag();
  };

  getItemBrand = (itemBrand) => itemBrand && itemBrand.toUpperCase();

  getImageConfigs = (showOnReviewPage) => {
    return showOnReviewPage
      ? ['t_t_cart_item_tile_m', 't_t_cart_item_tile_t', 't_t_cart_item_tile_d']
      : ['t_t_review_cart_item_m', 't_t_review_cart_item_t', 't_t_review_cart_item_d'];
  };

  /**
   * @function renderEddView
   * @returns {JSX}
   */
  renderEddView = (
    isEcomSoldout,
    isECOMOrder,
    isBossEnabled,
    isBopisEnabled,
    store,
    isReviewPage
  ) => {
    const {
      productDetail: {
        productInfo: { itemPartNumber },
      },
      newBag,
    } = this.props;
    const { isDesktop } = getViewportInfo();
    const newBagFontSize = isDesktop ? 'fs12' : 'fs11';
    return (
      !showRadioButtons({
        isEcomSoldout,
        isECOMOrder,
        isBossEnabled,
        isBopisEnabled,
        store,
      }) && (
        <BodyCopy className={isReviewPage ? 'edd-wrapper-review' : 'edd-wrapper'} component="div">
          <EddComponent
            itemIdprop={itemPartNumber}
            showIcon
            newBag={newBag}
            fontSize={newBag ? newBagFontSize : ''}
          />
        </BodyCopy>
      )
    );
  };

  showEDDonReview = (isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store) => {
    const { pageView } = this.props;
    if (pageView !== 'myBag') {
      return this.renderEddView(
        isEcomSoldout,
        isECOMOrder,
        isBossEnabled,
        isBopisEnabled,
        store,
        true
      );
    }
    return null;
  };

  showEDDonBag = (isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store) => {
    const { pageView, isBagPageSflSection } = this.props;
    if (pageView === 'myBag') {
      return !isBagPageSflSection
        ? this.renderEddView(isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store)
        : null;
    }
    return null;
  };

  getDynamicBadgeOverlayQty = (isDynamicBadgeEnabled, tcpStyleType, tcpStyleQty) =>
    getDynamicBadgeQty(tcpStyleType, tcpStyleQty, isDynamicBadgeEnabled);

  showVariantNo = (showOnReviewPage, productDetail, labels) => {
    return !showOnReviewPage ? (
      <div>
        <div className="color-size-fit-label">
          <BodyCopy
            fontFamily="secondary"
            component="span"
            fontSize="fs13"
            fontWeight={['extrabold']}
            textAlign="left"
          >
            {labels.itemNumberLabel}
          </BodyCopy>
        </div>
        <BodyCopy
          className="padding-left-10"
          fontFamily="secondary"
          component="span"
          fontSize="fs13"
          color="gray.900"
        >
          {`${productDetail.productInfo.variantNo}`}
        </BodyCopy>
      </div>
    ) : null;
  };

  getRoundedClass = (newBag) => (newBag ? 'rounded-container' : '');

  getNewProductImageHeightClass = (newBag) => (newBag ? 'dam-image-HW' : '');

  getBossBopisStoreIndexes = () => {
    const { pickupStoresInCart } = this.props;
    const bossStoreIndex = pickupStoresInCart.getIn([0, 'orderType']) === 'BOSS' ? 0 : 1;
    const bopisStoreIndex = pickupStoresInCart.getIn([0, 'orderType']) === 'BOPIS' ? 0 : 1;
    return {
      bossStoreIndex,
      bopisStoreIndex,
    };
  };

  handleQtyChange = (e) => {
    const itemQuantity = parseInt(e.target.value, 10);
    this.quantityChange(itemQuantity);
  };

  quantityChange = (quantity) => {
    const {
      productDetail: {
        itemInfo: { itemId, size: selectedSize, color, fit, size, isGiftItem },
        productInfo: {
          skuId: itemskuId,
          itemPartNumber,
          variantNo,
          generalProductId,
          multiPackItems,
        },
        miscInfo: { orderItemType, store },
      },
      orderId,
      pickupStoresInCart,
    } = this.props;
    const targetOrderType = orderItemType;
    const isECOMOrder = isEcomOrder(orderItemType);
    const isBOSSOrder = isBossOrder(orderItemType);

    const skuId = isGiftItem ? generalProductId : itemskuId;
    const formValue = { color, fit, quantity, size };
    const {
      updateCartItem,
      isMiniBagOpen,
      editableProductMultiPackInfo = {},
      autoSwitchPickupItemInCart,
    } = this.props;
    const {
      multiPackInfo: {
        newPartNumber: multipackProduct,
        multiPackCount,
        getAllMultiPack,
        getAllNewMultiPack,
      } = {},
      tcpMultiPackReferenceUSStore: partIdInfo,
    } = editableProductMultiPackInfo;

    const skuInfo = { ...formValue, skuId };

    const isCartTileQtyChange = true;
    this.setState({ quantityUpdating: true });
    const isCartPage = true;

    if (isECOMOrder) {
      updateCartItem(
        itemId,
        skuId,
        quantity,
        itemPartNumber,
        variantNo,
        isMiniBagOpen,
        multipackProduct,
        multiPackCount,
        selectedSize,
        getAllNewMultiPack,
        getAllMultiPack,
        skuInfo,
        partIdInfo,
        isCartTileQtyChange,
        isCartPage,
        multiPackItems
      );
    } else {
      const { bossStoreIndex, bopisStoreIndex } = this.getBossBopisStoreIndexes();
      const storeId = isBOSSOrder
        ? pickupStoresInCart.getIn([bossStoreIndex, 'stLocId'])
        : pickupStoresInCart.getIn([bopisStoreIndex, 'stLocId']);
      autoSwitchPickupItemInCart(
        {
          itemId,
          quantity,
          skuId,
          targetOrderType,
          orderId,
          store,
          storeId,
          itemPartNumber,
          variantNo,
          orderItemType,
          isCartTileQtyChange,
        },
        isCartPage
      );
    }
  };

  qtySelector = (isBagPage, isBagPageSflSection) => {
    const {
      productDetail: {
        miscInfo: { availability },
        itemInfo: { qty },
      },
    } = this.props;

    const { quantityUpdating } = this.state;
    const quantityArray = new Array(15).fill(1);
    const quantityList = quantityArray.map((val, index) => ({
      displayName: index + 1,
      id: index + 1,
    }));

    const isSoldOutItem = availability !== CARTPAGE_CONSTANTS.AVAILABILITY.OK;

    return (
      <>
        {isBagPage && !isBagPageSflSection ? (
          <MiniBagSelect
            width={32}
            id="quantity-new-qv"
            name="Quantity"
            options={quantityList}
            input={{
              name: '',
              arialabel: '',
              value: qty,
              onChange: this.quantityChange,
            }}
            showLoader={quantityUpdating}
            isSoldOutItem={isSoldOutItem}
            newQVDesign
          />
        ) : null}
      </>
    );
  };

  getSpinner = () => {
    const { isBagPageSflSection } = this.props;
    return (
      <div className="ddl-container">
        <div className="">
          <div className="loader-style-wrapper">{!isBagPageSflSection && <UpdatingSpinner />}</div>
        </div>
      </div>
    );
  };

  getQuantityDropDown = (isBagPage) => {
    const { isBagPageSflSection } = this.props;
    const { quantityUpdating } = this.state;
    return (
      <>
        {!quantityUpdating &&
          this.DropDownQuantitySelector(
            isBagPage,
            isBagPageSflSection,
            'mob-qty-selector hide-on-tablet ddl-container'
          )}
        {quantityUpdating && !isBagPageSflSection && this.getSpinner()}
      </>
    );
  };

  DropDownQuantitySelector = (isBagPage, isBagPageSflSection, cls) => {
    const {
      productDetail: {
        itemInfo: { qty },
        miscInfo: { availability },
      },
      labels,
      isCartQuantityUpdating,
    } = this.props;

    const quantityArray = new Array(15).fill(1);
    const quantityList = quantityArray.map((val, index) => ({
      displayName: index + 1,
      id: index + 1,
    }));

    const { quantityUpdating } = this.state;
    const isSoldOutItem = availability !== CARTPAGE_CONSTANTS.AVAILABILITY.OK;

    return (
      <div className={cls}>
        {isBagPage && !isBagPageSflSection && !quantityUpdating && !isSoldOutItem ? (
          <SelectBox
            customWidth="77px"
            customHeight="40px"
            downArrowBottom="17px"
            downArrowRight="12px"
            id="quantity"
            name="Quantity"
            component={MiniBagSelect}
            options={quantityList}
            onChange={this.handleQtyChange}
            input={{ value: qty }}
            meta
            inheritedStyles={selectBoxStyle}
            preText={`${labels.qtyLabel} `}
            className="quantity-ddl"
            disabled={isCartQuantityUpdating}
          />
        ) : null}
      </div>
    );
  };

  getPriceTag = ({ sectionName = '' }) => {
    const {
      isBagPageSflSection,
      productDetail,
      currencyExchange,
      showOnReviewPage,
      isMiniBag,
      pageView,
    } = this.props;
    const { offerPrice } = productDetail.itemInfo;
    const { salePrice, price } = getPrices({
      productDetail,
      currencyExchange,
    });
    const isBagPage = pageView === 'myBag';
    const getSaleValue = isBagPage
      ? offerPrice
      : this.getSalePrice(showOnReviewPage, offerPrice, salePrice, isMiniBag);

    const isMoveToBag = isBagPageSflSection ? ' price-label-sfl' : ' price-label-bag';
    return (
      <>
        {isBagPage && (
          <BodyCopy
            className={`price-label${isMoveToBag} ${sectionName}`}
            fontFamily="secondary"
            component="span"
            fontSize="fs16"
            fontWeight={['extrabold']}
            dataLocator={getLocator('cart_item_total_price')}
          >
            <PriceCurrency price={Number(isBagPageSflSection ? price : getSaleValue)} />
            <BodyCopy
              className="original-price-label"
              fontFamily="secondary"
              component="span"
              fontSize="fs16"
            >
              {this.getProductPriceList(productDetail, pageView, currencyExchange)}
            </BodyCopy>
          </BodyCopy>
        )}
      </>
    );
  };

  getClassForPriceTag = () => {
    const { isHideMeatBallAbTest, isBagPageSflSection } = this.props;
    let classForPriceTag = 'Cta-section';
    if (isBagPageSflSection) {
      classForPriceTag = '';
    } else if (isHideMeatBallAbTest && !isBagPageSflSection) {
      classForPriceTag = 'desktop-item hide-on-mobile';
    }
    return classForPriceTag;
  };

  getCTAForMobile = () => {
    const { isHideMeatBallAbTest, isShowEditCTAAbTest, isBagPageSflSection } = this.props;
    return (
      <>
        {!isBagPageSflSection && (
          <div className="cta-links hide-on-desktop hide-on-tablet">
            {isShowEditCTAAbTest && this.renderEditLink(true)}
            {isHideMeatBallAbTest && this.renderSflActionsLinks(true)}
          </div>
        )}
      </>
    );
  };

  getPriceTagForItemDetailSection = () => {
    const { isHideMeatBallAbTest, isBagPageSflSection } = this.props;
    return (
      isHideMeatBallAbTest &&
      !isBagPageSflSection &&
      this.getPriceTag({ sectionName: 'item-detail-section hide-on-desktop hide-on-tablet' })
    );
  };

  toggleEditModal = () => {
    const { openEditModal } = this.state;
    this.setState({ openEditModal: !openEditModal });
  };

  showEditModal = (isBagPage, isBagPageSflSection, isProductNotAvailable) => {
    const { openEditModal } = this.state;
    const { labels, productDetail, isHideMeatBallAbTest } = this.props;

    const headingProps = {
      headingStyle: {
        className: 'modal-heading',
      },
    };

    const clickEventdata = {
      customEvents: ['event172'],
      eventName: 'Edit Product',
    };

    return (
      <>
        {' '}
        {!isBagPageSflSection &&
        isBagPage &&
        !isHideMeatBallAbTest &&
        productDetail.miscInfo.availability !== CARTPAGE_CONSTANTS.AVAILABILITY.SOLDOUT ? (
          <BodyCopy
            className={`hamburger-icon${this.getProductNotAvailableClass(isProductNotAvailable)}`}
            onClick={this.toggleEditModal}
          >
            <Image src={getIconPath('hamburger-icon')} />
          </BodyCopy>
        ) : null}
        {openEditModal && (
          <Modal
            fixedWidth
            isOpen
            onRequestClose={() => {
              this.toggleEditModal();
            }}
            heading="Choose Options"
            headingAlign="center"
            overlayClassName="TCPModal__Overlay"
            className="TCPModal__Content"
            innerContentClassName="editModal-innerContent"
            headingFontWeight="bold"
            headingFontFamily="secondary"
            inheritedStyles={modalStyles}
            {...headingProps}
          >
            <ul className="edit-modal-options">
              <li>
                <div className="edit-icon">
                  <Image src={getIconPath('edit-icon')} />
                </div>
                <ClickTracker clickData={clickEventdata}>
                  <BodyCopy
                    fontFamily="secondary"
                    component="h2"
                    fontSize="fs16"
                    fontWeight={['semibold']}
                    onClick={() => {
                      this.callEditMethod();
                    }}
                  >
                    {labels.editItem}
                  </BodyCopy>
                </ClickTracker>
              </li>
              <li>
                <div className="save-for-later-icon">
                  <Image src={getIconPath('save-for-later-icon')} />
                </div>
                <BodyCopy
                  fontFamily="secondary"
                  component="h2"
                  fontSize="fs16"
                  fontWeight={['semibold']}
                  onClick={() => {
                    this.handleMoveItemtoSaveList();
                  }}
                >
                  {labels.saveForLaterLink}
                </BodyCopy>
              </li>
            </ul>
          </Modal>
        )}
      </>
    );
  };

  getBannerIsSFLOrFulfillmentDisabled = ({ isBagPage, classes, lowInvMsg, thresholdLimit }) => {
    const { showOnReviewPage, isBagPageSflSection, isLowInventoryBannerEnabled } = this.props;
    return (
      showOnReviewPage &&
      isBagPage && (
        <Row className={`low-inv-bag ${classes}`}>
          {this.renderLowInventoryBanner({
            isLowInventoryBannerEnabled,
            lowInventoryMsg: lowInvMsg,
            threshold: thresholdLimit.limit,
            selectedMethod: false,
            availableInventory: thresholdLimit.inventoryAvail,
            isBagPageSflSection,
          })}
        </Row>
      )
    );
  };

  renderLowInventoryBanner = ({
    isLowInventoryBannerEnabled,
    lowInventoryMsg,
    threshold,
    selectedMethod,
    availableInventory,
    isBagPageSflSection = false,
  }) => {
    const showInvMsg = 'show-low-inventory-banner-selected-method';
    const isSflSection = isBagPageSflSection ? showInvMsg : '';
    if (
      isLowInventoryBannerEnabled &&
      threshold > 0 &&
      threshold !== undefined &&
      threshold !== '' &&
      threshold > availableInventory &&
      availableInventory > 0
    ) {
      return (
        <BodyCopy
          component="span"
          className={`${
            selectedMethod ? showInvMsg : 'show-low-inventory-banner'
          } ${isSflSection} low-inv-banner-bag-do`}
        >
          <LowInventoryBanner
            isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
            lowInventoryMsg={lowInventoryMsg.replace('[val]', availableInventory)}
            threshold={threshold}
            availableInventory={availableInventory}
          />
        </BodyCopy>
      );
    }
    return null;
  };

  mobileShipmentMethods = ({
    isDesktop,
    showOnReviewPage,
    isBagPageSflSection,
    isBagPage,
    isEcomSoldout,
    isECOMOrder,
    isBossEnabled,
    isBopisEnabled,
    store,
    productDetail,
    labels,
    isBOSSOrder,
    isBOPISOrder,
    noBopisMessage,
    noBossMessage,
    bossDisabled,
    bopisDisabled,
    setShipToHome,
    pickupStoresInCart,
    autoSwitchPickupItemInCart,
    orderId,
    newBag,
    isLowInventoryBannerEnabled,
    cartOrderItems,
    isCanadaView,
    bossState,
    bossCity,
    bopisState,
    bopisCity,
  }) => {
    return (
      <>
        {!isDesktop &&
          showOnReviewPage &&
          !isBagPageSflSection &&
          isBagPage &&
          showRadioButtons({
            isEcomSoldout,
            isECOMOrder,
            isBossEnabled,
            isBopisEnabled,
            store,
          }) && (
            <Row fullBleed>
              <CartItemBoxButtons
                className="cart-item-radio-buttons"
                productDetail={productDetail}
                labels={labels}
                isEcomSoldout={isEcomSoldout}
                isECOMOrder={isECOMOrder}
                isBOSSOrder={isBOSSOrder}
                isBOPISOrder={isBOPISOrder}
                noBopisMessage={noBopisMessage}
                noBossMessage={noBossMessage}
                bossDisabled={bossDisabled}
                bopisDisabled={bopisDisabled}
                isBossEnabled={isBossEnabled}
                isBopisEnabled={isBopisEnabled}
                openPickUpModal={this.handleEditCartItemWithStore}
                setShipToHome={setShipToHome}
                pickupStoresInCart={pickupStoresInCart}
                autoSwitchPickupItemInCart={autoSwitchPickupItemInCart}
                orderId={orderId}
                newBag={newBag}
                isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
                renderLowInventoryBanner={this.renderLowInventoryBanner}
                cartOrderItems={cartOrderItems}
                isCanada={isCanadaView}
                bossState={bossState}
                bossCity={bossCity}
                bopisState={bopisState}
                bopisCity={bopisCity}
              />
              <RenderPerf.Measure name={CONTROLS_VISIBLE} />
            </Row>
          )}
      </>
    );
  };

  getLowInvBannerForMobTab = ({
    isBagPage,
    isEcomSoldout,
    isECOMOrder,
    isBossEnabled,
    isBopisEnabled,
    store,
    isBOPISOrder,
    isBOSSOrder,
    productNotAvailable,
  }) => {
    const { labels, isBagPageSflSection } = this.props;
    const hideOnMobTab = ' hide-on-mobile hide-on-tablet';
    const bannerStyle = isBagPageSflSection
      ? 'low-inv-banner-bag-wo-do-sfl'
      : 'low-inv-banner-bag-wo-do';
    return (
      <>
        {this.getBannerIsSFLOrFulfillmentDisabled({
          isBagPage,
          classes: `${bannerStyle} hide-on-desktop mobile-view${productNotAvailable}${
            showRadioButtons({
              isEcomSoldout,
              isECOMOrder,
              isBossEnabled,
              isBopisEnabled,
              store,
            })
              ? hideOnMobTab
              : ''
          }`,
          lowInvMsg: labels.lowInventoryCartTileTxt,
          thresholdLimit: this.getThresholdLimitByOrderType({
            isECOMOrder,
            isBOPISOrder,
            isBOSSOrder,
            labels,
          }),
        })}

        {isBagPageSflSection &&
          this.getBannerIsSFLOrFulfillmentDisabled({
            isBagPage,
            isEcomSoldout,
            isECOMOrder,
            isBossEnabled,
            isBopisEnabled,
            store,
            classes: `low-inv-banner-sfl hide-on-desktop mobile-view${productNotAvailable}${
              showRadioButtons({
                isEcomSoldout,
                isECOMOrder,
                isBossEnabled,
                isBopisEnabled,
                store,
              })
                ? ''
                : hideOnMobTab
            }`,
            lowInvMsg: labels.lowInventoryCartTileTxt,
            thresholdLimit: this.getThresholdLimitByOrderType({
              isECOMOrder,
              isBOPISOrder,
              isBOSSOrder,
              labels,
            }),
          })}
      </>
    );
  };

  getLowInvBannerForMobTabBagSection = ({
    isBagPage,
    isEcomSoldout,
    isECOMOrder,
    isBossEnabled,
    isBopisEnabled,
    store,
    isBOPISOrder,
    isBOSSOrder,
    productNotAvailable,
  }) => {
    const { labels, isBagPageSflSection } = this.props;
    const hideOnMobTab = ' hide-on-tablet hide-on-mobile';
    return (
      <>
        {!isBagPageSflSection &&
          this.getBannerIsSFLOrFulfillmentDisabled({
            isBagPage,
            classes: `hide-on-desktop mobile-view${productNotAvailable}${
              showRadioButtons({
                isEcomSoldout,
                isECOMOrder,
                isBossEnabled,
                isBopisEnabled,
                store,
              })
                ? ''
                : hideOnMobTab
            }`,
            lowInvMsg: labels.lowInventoryCartTileTxt,
            thresholdLimit: this.getThresholdLimitByOrderType({
              isECOMOrder,
              isBOPISOrder,
              isBOSSOrder,
              labels,
            }),
          })}
      </>
    );
  };

  getProductHeightClass = (isBossBopisEnabled) => (isBossBopisEnabled ? ' productFixedHeight' : '');

  brandImgWrapperSflClass = (isBagPageSflSection) =>
    isBagPageSflSection ? ' product-brand-img-wrapper-sfl' : '';

  getThresholdLimitByOrderType = ({ isECOMOrder, isBOPISOrder, isBOSSOrder, labels }) => {
    const { isBagPageSflSection, productDetail } = this.props;
    let limit = 0;
    let inventoryAvail = 0;
    if (isECOMOrder) {
      limit = labels.lblLowInvetoryThresholdShipToHome;
      inventoryAvail = productDetail && productDetail.miscInfo.inventoryAvailECOM;
    } else if (isBOPISOrder) {
      limit = labels.lblLowInvetoryThresholdBOPIS;
      inventoryAvail = productDetail && productDetail.miscInfo.inventoryAvailBOPIS;
    } else if (isBOSSOrder) {
      limit = labels.lblLowInvetoryThresholdBOSS;
      inventoryAvail = productDetail && productDetail.miscInfo.inventoryAvailBOSS;
    } else if (isBagPageSflSection) {
      limit = labels.lblLowInvetoryThresholdSFL;
      inventoryAvail = productDetail && productDetail.miscInfo.sflAvailInventory;
    }
    return { limit, inventoryAvail };
  };

  getProductNotAvailableClass = (isProductNotAvailable) => {
    return isProductNotAvailable ? ' hide-element' : '';
  };

  // eslint-disable-next-line complexity
  render() {
    const { isEdit, quantityLabelValue } = this.state;
    const {
      productDetail,
      productDetail: {
        miscInfo: { store, orderItemType, availability },
        itemInfo: { itemBrand },
        productInfo: { pdpUrl, tcpStyleQty, tcpStyleType },
      },
      labels,
      editableProductInfo,
      className,
      pageView,
      isBagPageSflSection,
      showOnReviewPage,
      setShipToHome,
      pickupStoresInCart,
      autoSwitchPickupItemInCart,
      orderId,
      disableProductRedirect,
      cookieToken,
      editableProductMultiPackInfo,
      isDynamicBadgeEnabled,
      newBag,
      isLowInventoryBannerEnabled,
      cartOrderItems,
      bossState,
      bossCity,
      bopisState,
      bopisCity,
      newMiniBag,
    } = this.props;
    const { isBossEnabled, isBopisEnabled } = getBossBopisFlags(this.props, itemBrand);
    const isECOMOrder = isEcomOrder(orderItemType);
    const isBOPISOrder = isBopisOrder(orderItemType);
    const isBOSSOrder = isBossOrder(orderItemType);
    const isEcomSoldout = isSoldOut(availability);
    const apiConfigObj = getAPIConfig();
    const { crossDomain, brandSpecificCrossDomain } = apiConfigObj;
    const currentSiteBrand = getBrand();
    const isProductBrandOfSameDomain =
      currentSiteBrand.toUpperCase() === this.getItemBrand(itemBrand);
    const { noBopisMessage, noBossMessage } = noBossBopisMessage(this.props);
    const { bossDisabled, bopisDisabled } = checkBossBopisDisabled(
      this.props,
      isBossEnabled,
      isBopisEnabled,
      isEcomSoldout,
      isBOSSOrder,
      isBOPISOrder
    );

    const initialValues = {
      color: { name: productDetail.itemInfo.color },
      Fit: productDetail.itemInfo.fit,
      Size: productDetail.itemInfo.size,
      Qty: productDetail.itemInfo.qty,
    };
    const productBrand = productDetail?.itemInfo?.itemBrand;
    const pdpToPath = this.getPdpToPath(
      isProductBrandOfSameDomain,
      pdpUrl,
      crossDomain,
      brandSpecificCrossDomain,
      productBrand
    );
    const pdpAsPathUrl = this.getPdpAsPathurl(
      isProductBrandOfSameDomain,
      pdpUrl,
      crossDomain,
      cookieToken,
      brandSpecificCrossDomain,
      productBrand
    );
    const imgConfig = this.getImageConfigs(showOnReviewPage);
    const isBagPage = pageView === 'myBag';
    const disableLink = disableProductRedirect;
    const dynamicBadgeOverlayQty = this.getDynamicBadgeOverlayQty(
      isDynamicBadgeEnabled,
      tcpStyleType,
      tcpStyleQty
    );
    const { badgeConfig } = config.IMG_DATA_CLIENT_BAGPAGE;
    const showSize = productDetail && productDetail.itemInfo && productDetail.itemInfo.size;
    const { isDesktop } = getViewportInfo();
    const isBossBopisEnabled = showRadioButtons({
      isEcomSoldout,
      isECOMOrder,
      isBossEnabled,
      isBopisEnabled,
      store,
    });
    const isCanadaView = isCanada();
    const isProductNotAvailable = this.getProductUnavailabilityStatus({
      isEcomSoldout,
      labels,
      isBOSSOrder,
      bossDisabled,
      noBossMessage,
      availability,
      isBOPISOrder,
      bopisDisabled,
      noBopisMessage,
    });
    const hideOnMobTab = ` hide-on-mobile hide-on-tablet${this.getProductNotAvailableClass(
      isProductNotAvailable
    )}`;
    const hideOnMobile = !isBagPageSflSection ? 'hide-on-mobile' : '';
    return (
      <div className={`${className} tile-header newCartTile ${this.getRoundedClass(newBag)} `}>
        {this.renderTogglingError()}
        {this.headerAndAvailabilityErrorContainer({
          isEcomSoldout,
          bossDisabled,
          isBOSSOrder,
          bopisDisabled,
          isBOPISOrder,
          noBossMessage,
          noBopisMessage,
          availability,
        })}
        <Row
          fullBleed
          className={['product', isBagPage ? 'product-tile-wrapper' : ''].join(' ')}
          aria-label={`${productDetail.itemInfo.name}. ${labels.price} ${productDetail.itemInfo.price}. ${labels.size} ${productDetail.itemInfo.size}. ${labels.qty} ${productDetail.itemInfo.qty}`}
        >
          <Col
            key="productDetails"
            className={`align-product-img product-brand-img-wrapper${this.brandImgWrapperSflClass(
              isBagPageSflSection
            )}${this.getProductHeightClass(isBossBopisEnabled)}`}
            colSize={{ small: 2, medium: 2, large: 3 }}
          >
            <div className="imageWrapper">
              <LinkWrapper
                pdpToPath={pdpToPath}
                pdpAsPathUrl={pdpAsPathUrl}
                disableLink={disableLink}
                noWrap={disableLink}
                IsSlugPathAdded
              >
                <DamImage
                  imgData={{
                    alt: `${labels.productImageAlt} ${productDetail.itemInfo.name}`,
                    url: productDetail.itemInfo.imagePath,
                  }}
                  imgConfigs={imgConfig}
                  itemBrand={this.getItemBrand(productDetail.itemInfo.itemBrand)}
                  isProductImage
                  onClick={this.closeMiniBagMethod}
                  className={`${
                    !showOnReviewPage
                      ? 'dam-image-review-page'
                      : this.getNewProductImageHeightClass(newBag)
                  }`}
                  dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
                  tcpStyleType={tcpStyleType}
                  badgeConfig={badgeConfig}
                />
              </LinkWrapper>
              {availability === CARTPAGE_CONSTANTS.AVAILABILITY.SOLDOUT && (
                <BodyCopy
                  className="soldOutLabel"
                  component="span"
                  fontFamily="secondary"
                  textAlign="center"
                  fontSize="fs12"
                >
                  {labels.soldOut}
                </BodyCopy>
              )}
            </div>
            {!productDetail.itemInfo.isGiftItem && (
              <div className="logoWrapper">
                <Image
                  alt={labels[`${itemBrand?.toLowerCase()}BrandAlt`]}
                  className="brand-image"
                  src={getIconPath(`header__brand-tab-${itemBrand?.toLowerCase()}`)}
                  data-locator={getLocator('cart_item_brand_logo')}
                />
              </div>
            )}
            {this.getBannerIsSFLOrFulfillmentDisabled({
              isBagPage,
              classes: `short-inv-msg${hideOnMobTab}`,
              lowInvMsg: labels.lowInventoryCartTileShortTxt,
              thresholdLimit: this.getThresholdLimitByOrderType({
                isECOMOrder,
                isBOPISOrder,
                isBOSSOrder,
                labels,
              }),
            })}
          </Col>
          <Col
            className="bag-product-detail-wrapper"
            key="productDetails"
            colSize={{ small: 4, medium: 6, large: 9 }}
          >
            {showOnReviewPage &&
              productDetail.miscInfo.badge &&
              this.getBadgeDetails(productDetail)}

            <Row className="product-detail-row">
              <Col className="productImgBrand" colSize={{ small: 4, medium: 6, large: 12 }}>
                <LinkWrapper
                  pdpToPath={pdpToPath}
                  pdpAsPathUrl={pdpAsPathUrl}
                  disableLink={disableLink}
                  noWrap={disableLink}
                  IsSlugPathAdded
                >
                  <BodyCopy
                    fontFamily="secondary"
                    component="span"
                    fontSize="fs14"
                    fontWeight={['extrabold']}
                    dataLocator={getLocator('cart_item_title')}
                    onClick={this.closeMiniBagMethod}
                  >
                    {productDetail.itemInfo.name}
                  </BodyCopy>
                </LinkWrapper>
              </Col>
            </Row>
            {this.getProductItemUpcNumber(productDetail, isBagPage, showOnReviewPage)}
            {!isEdit ? (
              <React.Fragment>
                <Row className="product-detail-row padding-top-10 color-map-size-fit">
                  <Col
                    className={!isBagPage ? this.getProductDetailClass() : 'product-detail-bag'}
                    colSize={{ medium: 8, large: 12 }}
                  >
                    <div className="product-detail-section">
                      {this.showVariantNo(showOnReviewPage, productDetail, labels)}
                      <BodyCopy
                        className="value-clr clr text-capitalize"
                        fontFamily="secondary"
                        component="span"
                        fontSize="fs13"
                        color="gray.900"
                        dataLocator={getLocator('cart_item_color')}
                      >
                        {`${productDetail.itemInfo?.color?.toLowerCase()}`}

                        {this.getSizeAndSeparator()}
                      </BodyCopy>

                      {showOnReviewPage && <>{this.renderSizeAndFit(hideOnMobile)}</>}
                      {this.getProductPointsList(
                        productDetail,
                        isBagPageSflSection,
                        showOnReviewPage
                      )}
                      {this.renderQtyForMobTab('hide-on-desktop hide-on-mobile')}
                      {isBagPageSflSection && (
                        <div className="hideOnDesktop">
                          {showSize && (
                            <BodyCopy
                              className="color-fit-size-separator-mobile"
                              fontFamily="secondary"
                              component="span"
                              fontSize="fs13"
                            >
                              |
                            </BodyCopy>
                          )}
                          <BodyCopy
                            className=""
                            fontFamily="secondary"
                            component="span"
                            fontSize="fs13"
                            color="gray.900"
                            dataLocator="addedtobag-productqty"
                          >
                            <BodyCopy
                              className="qty-for-mob"
                              fontFamily="secondary"
                              component="span"
                              fontSize="fs13"
                              color="gray.900"
                            >
                              {`1 ${labels.qtyCartTile}`}
                            </BodyCopy>
                            <BodyCopy
                              className="qty-for-tab"
                              fontFamily="secondary"
                              component="span"
                              fontSize="fs13"
                              color="gray.900"
                            >
                              {quantityLabelValue}
                            </BodyCopy>
                          </BodyCopy>
                        </div>
                      )}
                      {this.getPriceTagForItemDetailSection()}
                      {this.showEditModal(isBagPage, isBagPageSflSection, isProductNotAvailable)}
                    </div>
                  </Col>
                </Row>
              </React.Fragment>
            ) : (
              <ProductEditForm
                item={productDetail}
                colorFitsSizesMap={editableProductInfo}
                editableProductMultiPackInfo={editableProductMultiPackInfo}
                handleSubmit={this.handleSubmit}
                initialValues={initialValues}
                labels={labels}
                formVisiblity={this.toggleFormVisibility}
                itemBrand={this.getItemBrand(productDetail.itemInfo.itemBrand)}
                newMiniBag={newMiniBag}
              />
            )}

            {showOnReviewPage && this.getItemDetails(productDetail, labels, pageView)}

            {this.showEDDonReview(isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store)}
            {this.showEDDonBag(isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store)}
            {this.getLowInvBannerForMobTab({
              isBagPage,
              isEcomSoldout,
              isECOMOrder,
              isBossEnabled,
              isBopisEnabled,
              store,
              isBOPISOrder,
              isBOSSOrder,
              productNotAvailable: this.getProductNotAvailableClass(isProductNotAvailable),
            })}

            {isDesktop &&
              showOnReviewPage &&
              !isBagPageSflSection &&
              isBagPage &&
              showRadioButtons({
                isEcomSoldout,
                isECOMOrder,
                isBossEnabled,
                isBopisEnabled,
                store,
              }) && (
                <Row fullBleed>
                  <CartItemBoxButtons
                    className="cart-item-radio-buttons"
                    productDetail={productDetail}
                    labels={labels}
                    isEcomSoldout={isEcomSoldout}
                    isECOMOrder={isECOMOrder}
                    isBOSSOrder={isBOSSOrder}
                    isBOPISOrder={isBOPISOrder}
                    noBopisMessage={noBopisMessage}
                    noBossMessage={noBossMessage}
                    bossDisabled={bossDisabled}
                    bopisDisabled={bopisDisabled}
                    isBossEnabled={isBossEnabled}
                    isBopisEnabled={isBopisEnabled}
                    openPickUpModal={this.handleEditCartItemWithStore}
                    setShipToHome={setShipToHome}
                    pickupStoresInCart={pickupStoresInCart}
                    autoSwitchPickupItemInCart={autoSwitchPickupItemInCart}
                    orderId={orderId}
                    newBag={newBag}
                    isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
                    renderLowInventoryBanner={this.renderLowInventoryBanner}
                    cartOrderItems={cartOrderItems}
                    isCanada={isCanadaView}
                    bossState={bossState}
                    bossCity={bossCity}
                    bopisState={bopisState}
                    bopisCity={bopisCity}
                  />
                  <RenderPerf.Measure name={CONTROLS_VISIBLE} />
                </Row>
              )}
          </Col>
        </Row>
        {this.mobileShipmentMethods({
          isDesktop,
          showOnReviewPage,
          isBagPageSflSection,
          isBagPage,
          isEcomSoldout,
          isECOMOrder,
          isBossEnabled,
          isBopisEnabled,
          store,
          productDetail,
          labels,
          isBOSSOrder,
          isBOPISOrder,
          noBopisMessage,
          noBossMessage,
          bossDisabled,
          bopisDisabled,
          setShipToHome,
          pickupStoresInCart,
          autoSwitchPickupItemInCart,
          orderId,
          newBag,
          isLowInventoryBannerEnabled,
          cartOrderItems,
          isCanadaView,
          bossState,
          bossCity,
          bopisState,
          bopisCity,
        })}

        {this.getLowInvBannerForMobTabBagSection({
          isBagPage,
          isEcomSoldout,
          isECOMOrder,
          isBossEnabled,
          isBopisEnabled,
          store,
          isBOPISOrder,
          isBOSSOrder,
          productNotAvailable: this.getProductNotAvailableClass(isProductNotAvailable),
        })}
      </div>
    );
  }
}

CartItemNewTile.defaultProps = {
  pageView: '',
  isEditAllowed: true,
  isCondense: true,
  sflItemsCount: 0,
  isBagPageSflSection: false,
  showOnReviewPage: true,
  onQuickViewOpenClick: () => {},
  setShipToHome: () => {},
  toggleError: null,
  toggleBossBopisError: null,
  clearToggleError: () => {},
  currencyExchange: null,
  autoSwitchPickupItemInCart: () => {},
  disableProductRedirect: false,
  closeMiniBag: () => {},
  isBrierleyWorking: true,
  cookieToken: null,
  isMiniBag: false,
  isMultipackProduct: false,
  editableProductMultiPackInfo: {},
  isDynamicBadgeEnabled: false,
  newBag: PropTypes.bool,
  isCartLoading: PropTypes.bool,
  isLowInventoryBannerEnabled: PropTypes.bool,
  cartOrderItems: [],
  bossState: '',
  bossCity: '',
  bopisState: '',
  bopisCity: '',
};

CartItemNewTile.propTypes = {
  productDetail: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  getProductSKUInfo: PropTypes.func.isRequired,
  updateCartItem: PropTypes.func.isRequired,
  editableProductInfo: PropTypes.shape({}).isRequired,
  removeCartItem: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  isPlcc: PropTypes.string.isRequired,
  pageView: PropTypes.string,
  toggleEditAllowance: PropTypes.func.isRequired,
  isEditAllowed: PropTypes.bool,
  isShowSaveForLater: PropTypes.bool.isRequired,
  isCondense: PropTypes.bool,
  isGenricGuest: PropTypes.shape({}).isRequired,
  sflItemsCount: PropTypes.number,
  sflMaxCount: PropTypes.number.isRequired,
  addItemToSflList: PropTypes.func.isRequired,
  setCartItemsSflError: PropTypes.func.isRequired,
  isBagPageSflSection: PropTypes.bool,
  showOnReviewPage: PropTypes.bool,
  startSflItemDelete: PropTypes.func.isRequired,
  startSflDataMoveToBag: PropTypes.func.isRequired,
  onPickUpOpenClick: PropTypes.func.isRequired,
  onQuickViewOpenClick: PropTypes.func,
  orderId: PropTypes.number.isRequired,
  setShipToHome: PropTypes.func,
  toggleError: PropTypes.shape({}),
  toggleBossBopisError: PropTypes.shape({
    errorMessage: PropTypes.string,
  }),
  clearToggleError: PropTypes.func,
  currencyExchange: PropTypes.shape([]),
  pickupStoresInCart: PropTypes.shape({}).isRequired,
  autoSwitchPickupItemInCart: PropTypes.func,
  disableProductRedirect: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func.isRequired,
  closeMiniBag: PropTypes.func,
  isMiniBagOpen: PropTypes.bool.isRequired,
  handleAddToWishlist: PropTypes.func.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  isBrierleyWorking: PropTypes.bool,
  cookieToken: PropTypes.string,
  isMiniBag: PropTypes.bool,
  isMultipackProduct: PropTypes.bool,
  editableProductMultiPackInfo: PropTypes.shape({}),
  isDynamicBadgeEnabled: PropTypes.shape({}),
  newBag: false,
  isCartLoading: false,
  isLowInventoryBannerEnabled: false,
  cartOrderItems: PropTypes.shape([]),
  bossState: PropTypes.string,
  bossCity: PropTypes.string,
  bopisState: PropTypes.string,
  bopisCity: PropTypes.string,
};

export default withStyles(CartItemNewTile, styles);
export { CartItemNewTile as CartItemNewTileVanilla };
