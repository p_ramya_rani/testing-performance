// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { UpdatingSpinnerVanilla } from '../views/UpdatingSpinner.view';

describe('spinner Component', () => {
  let component;
  let props;
  beforeEach(() => {
    props = {
      className: '',
    };
  });

  it('spinner should be defined', () => {
    component = shallow(<UpdatingSpinnerVanilla {...props} />);
    expect(component).toBeDefined();
  });

  it('spinner should render', () => {
    component = shallow(<UpdatingSpinnerVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
