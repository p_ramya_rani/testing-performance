// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import qtyStyle from './CartItemTileQtyDDL.js';

export default css`
  .increaseButton {
    width: 36px;
    height: 35px;
    position: relative;
    cursor: pointer;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    &:hover {
      background: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    }
    &.disabled {
      pointer-events: none;
    }
    ${(props) => (props.isCartLoading ? `pointer-events:none;` : '')}
  }
  .decreaseButtonContent {
    height: 1px;
    width: 13px;
    background-color: ${(props) => props.theme.colorPalette.black};
  }
  .product-detail-review-page {
    display: inline-block;
  }

  .toggle-error {
    display: flex;
    align-items: center;
    background: ${(props) => props.theme.colorPalette.white};

    img {
      padding-top: 0px;
      padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      padding-left: 0;
      padding-bottom: 1px;
    }
  }
  .dam-image-review-page {
    width: 55px;
  }
  .sfl-fav-image {
    cursor: pointer;
  }

  .edd-wrapper {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: -20px;
    }
  }
  .edd-wrapper-review {
    margin: -20px 0 0 12px;
  }

  .dam-image-HW {
    max-height: 124px;
    width: 104px;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      max-height: 96px;
      width: 76px;
    }

    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      max-height: 96px;
      width: 89px;
    }
  }

  .cartTile-btn {
    width: fit-content;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    background-color: ${(props) => props.theme.colorPalette.white};
    min-height: 42px;
    border: solid 1px ${(props) => props.theme.colorPalette.gray[1600]};
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.typography.fontSizes.fs13};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.5px;
    color: ${(props) => props.theme.colorPalette.gray[900]};
    text-transform: capitalize;
    padding: 10px 24px 10px 24px;

    &:hover:not([disabled]) {
      background: transparent;
    }

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
      &.update-btn {
        margin-bottom: 10px;
        display: block;
      }
    }

    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      max-height: 36px;
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      padding: 10px;
    }
  }

  .qty-for-mob-tab {
    display: inline-block;
  }

  .hideOnDesktop {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: inline-block;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      display: block;
      .color-fit-size-separator-mobile {
        display: none;
      }
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      display: none;
    }

    .qty-for-tab {
      display: none;
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        display: block;
      }
    }
    .qty-for-mob {
      display: none;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        display: inline;
      }
    }
  }

  .cartTile-moveToBag {
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
    font-size: ${(props) => props.isBagPageSflSection && props.theme.typography.fontSizes.fs14};
    color: ${(props) => props.theme.colorPalette.white};
    border: none;
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    text-transform: uppercase;
    &:hover {
      .cartTile-btn:not([disabled]) {
        background: ${(props) => props.theme.colorPalette.blue.C900};
        color: ${(props) => props.theme.colorPalette.white};
      }
    }

    &:hover:not([disabled]) {
      background: ${(props) => props.theme.colorPalette.blue.C900};
      color: ${(props) => props.theme.colorPalette.white};
    }

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: block;
      font-size: ${(props) => props.isBagPageSflSection && props.theme.typography.fontSizes.fs13};
      max-width: 165px;
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG_2};
    }
  }
  .cartTile-edit {
    min-width: 78px;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  .points-label {
    color: ${(props) => props.theme.colorPalette.gray[900]};
  }

  .update-btn {
    border: 1px solid ${(props) => props.theme.colorPalette.red[500]};
    color: ${(props) => props.theme.colorPalette.red[500]};
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
  }

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .productPoint {
      display: block;
    }
    .clr {
      display: block;
    }
  }

  .text-capitalize {
    text-transform: capitalize;
  }

  .itemQuantity-desktop {
    right: 44%;
    position: absolute;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      display: none;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.isBagPageSflSection && props.theme.typography.fontSizes.fs14};
    }
  }

  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
  .hamburger-icon {
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      display: none;
    }
  }
  .product-detail-row {
    p.hamburger-icon {
      position: absolute;
      top: 52px;
      right: -6px;
      height: 36px;
      width: 36px;
    }
  }

  .item-tag {
    margin-bottom: 6px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-bottom: 4px;
    }
  }

  .product {
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    &:focus,
    &:active {
      outline: 0;
    }
  }
  .padding-left-10 {
    padding-left: 4px;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      width: ${(props) => (props.showOnReviewPage ? '150px' : 'auto')};
      display: inline-flex;
    }
  }

  .padding-top-5 {
    padding-top: 5px;
  }
  .low-inv-bag {
    margin-left: 0px;
    margin-right: 0px;
    justify-content: center;
    width: 100%;
  }
  .mobile-view {
    justify-content: flex-start;
  }

  .low-inv-banner-bag-do {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 0;
    }
  }
  .low-inv-banner-sfl {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
  .low-inv-banner-bag-wo-do {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    }
  }
  .low-inv-banner-bag-wo-do-sfl {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }

  .productFixedHeight {
    @media ${(props) => props.theme.mediaQuery.large} {
      height: 160px;
    }
  }
  .price-label-sfl {
    right: 0;
  }
  .mobile-view .low-inventory-banner {
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  }

  .short-inv-msg .low-inventory-banner {
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
  }

  .hide-element {
    display: none;
  }
  .unavailable-error {
    span {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs12};
      }
    }
  }

  ${qtyStyle}
`;

export const modalStyles = css`
  .edit-modal-options {
    padding: 4px 24px 0;
    li {
      padding: 15px 8px;
      border-bottom: 1px solid ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
      display: flex;
      h2 {
        padding-top: 6px;
      }
    }
    li:last-child {
      border-bottom: none;
    }
  }
  .edit-icon,
  .save-for-later-icon {
    margin-right: 16px;
    height: 35px;
    width: 36px;
    display: flex;
  }
  div.TCPModal__InnerContent.editModal-innerContent {
    right: 0;
    left: auto;
    top: 0;
    bottom: 0;
    transform: none;
    box-shadow: 0 4px 8px 0 rgba(163, 162, 162, 0.5);
    padding: 7px 0 0 17px;
    width: 350px;

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 375px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 481px;
      animation: slide-in 0.5s forwards;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      max-height: 227px;
      top: auto;
      width: 100%;
      animation: slide-in-mobile 0.5s forwards;
      border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
        ${(props) => props.theme.spacing.ELEM_SPACING.XS} 0px 0px;
      padding: 7px 0 20px 0px;
    }
    @keyframes slide-in {
      0% {
        transform: translateX(100%);
      }
      100% {
        transform: translateX(0%);
      }
    }
    @keyframes slide-in-mobile {
      0% {
        transform: translateY(100%);
      }
      100% {
        transform: translateY(5%);
      }
    }
    @keyframes slide-out {
      0% {
        transform: translateX(0%);
      }
      100% {
        transform: translateX(100%);
      }
    }

    .close-modal {
      right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      @media ${(props) => props.theme.mediaQuery.medium} {
        top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      }
      opacity: 0.6;
    }
    .blurred-content {
      filter: blur(4px);
      -o-filter: blur(4px);
      -ms-filter: blur(4px);
      -moz-filter: blur(4px);
      -webkit-filter: blur(4px);
    }
    .Modal-Header {
      top: 0;
      position: sticky;
      z-index: 100;
    }
    .modal-heading {
      border-bottom: none;
      padding-bottom: 26px;
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
      display: flex;
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      justify-content: center;
      height: auto;
      margin-bottom: 0px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        display: flex;
        justify-content: center;
        height: auto;
        padding-top: 5px;
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        padding-top: 9px;
      }
    }
    .modal-heading:before {
      content: '';
      position: absolute;
      width: 100%;
      height: 51px;
      margin: auto;
      z-index: -1;
      background-color: #f4f4f4;
      bottom: 10px;
      left: -17px;
      padding-right: 17px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        left: -17px;
        padding-right: 17px;
      }
    }
  }

  .quantity-selector-container {
    position: relative;
  }
`;
