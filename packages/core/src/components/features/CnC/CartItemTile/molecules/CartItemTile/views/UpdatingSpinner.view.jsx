// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import withStyles from '../../../../../../common/hoc/withStyles';
import style from '../styles/UpdatingSpinner.style';

class UpdatingSpinner extends React.Component {
  toggleEditModal = () => {
    return true;
  };

  render() {
    const { className } = this.props;
    return (
      <div className={`${className}`}>
        <div className="spinner-container">
          <div className="spc-spinner">
            <div />
            <div />
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(UpdatingSpinner, style);
export { UpdatingSpinner as UpdatingSpinnerVanilla };
