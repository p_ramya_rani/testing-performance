/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ItemAvailability from '@tcp/core/src/components/features/CnC/common/molecules/ItemAvailability';
import ErrorMessage from '@tcp/core/src/components/features/CnC/common/molecules/ErrorMessage';
import { getDynamicBadgeQty, capitalizeWords } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  getLabelValue,
  getIconPath,
  getLocator,
  isCanada,
  getAPIConfig,
  getBrand,
  urlContainsQuery,
  fireEnhancedEcomm,
  removeRecFromStorage,
} from '@tcp/core/src/utils';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import { KEY_CODES } from '@tcp/core/src/constants/keyboard.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { CONTROLS_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import EddComponent from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent';
import ProductEditForm from '../../../../../../common/molecules/ProductCustomizeForm';
import CartItemRadioButtons from '../../CartItemRadioButtons/views/CartItemRadioButtons.view';
import { Image, Row, BodyCopy, Col, Anchor } from '../../../../../../common/atoms';
import getModifiedString from '../../../utils';
import styles from '../styles/CartItemTile.style';
import CARTPAGE_CONSTANTS from '../../../CartItemTile.constants';
import config from '../../../../../../common/molecules/ProductDetailImage/config';
import DamImage from '../../../../../../common/atoms/DamImage';
import {
  getBossBopisFlags,
  isEcomOrder,
  isBopisOrder,
  isBossOrder,
  isSoldOut,
  noBossBopisMessage,
  checkBossBopisDisabled,
  showRadioButtons,
  hideEditBossBopis,
  getBOSSUnavailabilityMessage,
  getBOPISUnavailabilityMessage,
  getSTHUnavailabilityMessage,
  getPrices,
} from './CartItemTile.utils';
import { getProductListToPath } from '../../../../../browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import LowInventoryBanner from '../../LowInventoryBanner/index';

/**
 * LinkWrapper component is to wrap anchor component to decide whether child needs anchor tag or not
 * @param {object} props
 */
const LinkWrapper = (props) => {
  LinkWrapper.propTypes = {
    pdpToPath: PropTypes.string.isRequired,
    pdpAsPathUrl: PropTypes.string.isRequired,
    disableLink: PropTypes.bool.isRequired,
    noWrap: PropTypes.bool.isRequired,
    IsSlugPathAdded: PropTypes.bool,
    children: PropTypes.node.isRequired,
  };

  LinkWrapper.defaultProps = {
    IsSlugPathAdded: false,
  };

  const { pdpToPath, pdpAsPathUrl, disableLink, children, noWrap, IsSlugPathAdded } = props;
  return noWrap ? (
    children
  ) : (
    <Anchor
      to={pdpToPath}
      asPath={pdpAsPathUrl}
      noLink={disableLink}
      IsSlugPathAdded={IsSlugPathAdded}
    >
      {children}
    </Anchor>
  );
};

const getFontColor = (isMiniBag) => (isMiniBag ? 'gray.900' : 'gray.800');

class CartItemTile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
    };
  }

  componentDidUpdate(prevProps) {
    const {
      isBagPageSflSection,
      toggleBossBopisError,
      productDetail: {
        itemInfo: { itemId },
      },
    } = this.props;
    if (
      !isBagPageSflSection &&
      toggleBossBopisError &&
      itemId === toggleBossBopisError.itemId &&
      (prevProps.toggleBossBopisError === null ||
        prevProps.toggleBossBopisError.errorMessage !== toggleBossBopisError.errorMessage)
    ) {
      const timer = setTimeout(() => {
        this.handleEditCartItemWithStore(toggleBossBopisError.targetOrderType);
        clearTimeout(timer);
      });
    }
  }

  componentWillUnmount() {
    this.clearToggleErrorState();
  }

  clearToggleErrorState = () => {
    const { pageView, clearToggleError } = this.props;
    if (pageView === 'myBag') {
      clearToggleError();
    }
  };

  toggleFormVisibility = () => {
    const { isEdit } = this.state;
    this.setState({ isEdit: !isEdit });
    this.toggleEditLinkVisibility();
  };

  toggleEditLinkVisibility = () => {
    const { toggleEditAllowance } = this.props;
    toggleEditAllowance();
  };

  handleEditCartItemMiniBag = (pageView, itemBrand, productNumber) => {
    const productNum = productNumber.slice(0, productNumber.indexOf('_'));
    this.toggleFormVisibility();
    const { getProductSKUInfo } = this.props;
    getProductSKUInfo({ productNum, itemBrand });
  };

  handleEditCartItem = (pageView, itemBrand, productNumber, isBagPageSflSection = false) => {
    if (pageView !== 'myBag') {
      this.handleEditCartItemMiniBag(pageView, itemBrand, productNumber);
    } else {
      const { onQuickViewOpenClick, productDetail } = this.props;
      const { itemId, qty, color, size, fit, isGiftItem } = productDetail.itemInfo;
      const {
        productInfo: { skuId, generalProductId, pdpUrl },
      } = productDetail;
      onQuickViewOpenClick({
        fromBagPage: pageView === 'myBag',
        colorProductId: productNumber,
        isSflProduct: isBagPageSflSection,
        pdpUrl,
        orderInfo: {
          orderItemId: itemId,
          selectedQty: qty,
          selectedColor: color,
          selectedSize: size,
          selectedFit: fit,
          itemBrand,
          skuId: isGiftItem ? generalProductId : skuId,
        },
      });
    }
  };

  /**
   *
   * @method handleEditCartItemWithStore
   * @description this method handles edit for cart item for boss/bopis item
   * @memberof CartItemTile
   */
  handleEditCartItemWithStore = (
    changeStoreType,
    openSkuSelectionForm = false,
    openRestrictedModalForBopis = false,
    isPickUpWarningModal = false
  ) => {
    const { onPickUpOpenClick, productDetail, orderId } = this.props;
    const { itemId, qty, color, size, fit, itemBrand } = productDetail.itemInfo;
    const { store, orderItemType } = productDetail.miscInfo;
    const { productPartNumber, tcpStyleQty } = productDetail.productInfo;
    const isItemShipToHome = !store;
    const isBopisCtaEnabled = changeStoreType === CARTPAGE_CONSTANTS.BOPIS;
    const isBossCtaEnabled = changeStoreType === CARTPAGE_CONSTANTS.BOSS;
    const alwaysSearchForBOSS = changeStoreType === CARTPAGE_CONSTANTS.BOSS;
    onPickUpOpenClick({
      colorProductId: productPartNumber,
      orderInfo: {
        orderItemId: itemId,
        Quantity: qty,
        color,
        Size: size,
        Fit: fit,
        orderId,
        orderItemType,
        itemBrand,
      },
      openSkuSelectionForm,
      isBopisCtaEnabled,
      isBossCtaEnabled,
      isItemShipToHome,
      alwaysSearchForBOSS,
      openRestrictedModalForBopis,
      isPickUpWarningModal,
      initialSelectedQty: tcpStyleQty,
    });
  };

  callEditMethod = () => {
    const { productDetail, pageView, isBagPageSflSection } = this.props;
    const {
      miscInfo: { orderItemType },
    } = productDetail;
    if (orderItemType === CARTPAGE_CONSTANTS.ECOM || isBagPageSflSection) {
      this.handleEditCartItem(
        pageView,
        productDetail.itemInfo.itemBrand,
        productDetail.productInfo.productPartNumber,
        isBagPageSflSection
      );
    } else if (pageView === 'myBag') {
      const openSkuSelectionForm = true;
      this.handleEditCartItemWithStore(orderItemType, openSkuSelectionForm);
    } else {
      this.handleEditCartItemMiniBag(
        pageView,
        productDetail.itemInfo.itemBrand,
        productDetail.productInfo.productPartNumber
      );
    }
  };

  handleKeyDown = (event, callback) => {
    const { KEY_ENTER, KEY_SPACE } = KEY_CODES;
    const { which } = event;
    if (which === KEY_ENTER || which === KEY_SPACE) {
      callback();
    }
  };

  handleMoveItemtoSaveList = () => {
    const {
      productDetail,
      sflItemsCount,
      sflMaxCount,
      isCondense,
      isGenricGuest,
      addItemToSflList,
      setCartItemsSflError,
      labels,
      pageView,
      setClickAnalyticsData,
    } = this.props;
    const {
      itemInfo: { itemId, isGiftItem, color, name, offerPrice, size, listPrice, itemBrand },
      productInfo: { skuId, generalProductId, upc, productPartNumber, multiPackItems },
      miscInfo: { store },
    } = productDetail;
    const productCatEntryId = isGiftItem ? generalProductId : skuId;
    const userInfoRequired = isGenricGuest && isGenricGuest.get('userId') && isCondense; // Flag to check if getRegisteredUserInfo required after SflList
    const isMiniBag = pageView !== 'myBag';
    this.clearToggleErrorState();

    if (sflItemsCount >= sflMaxCount) {
      return setCartItemsSflError(labels.sflMaxLimitError);
    }
    let catEntryId = productCatEntryId;
    if (multiPackItems) {
      catEntryId = [
        {
          catEntryId: productCatEntryId,
          multiPackItems,
        },
      ];
    }

    const payloadData = { itemId, catEntryId, userInfoRequired, isMiniBag };
    const productsData = {
      color,
      id: itemId,
      name,
      price: offerPrice,
      extPrice: offerPrice,
      sflExtPrice: offerPrice,
      listPrice,
      partNumber: productPartNumber,
      size,
      upc,
      sku: skuId.toString(),
      pricingState: 'full price',
      colorId: generalProductId,
      storeId: store,
      prodBrand: itemBrand,
    };
    setClickAnalyticsData({
      customEvents: ['event138'],
      products: [productsData],
      eventName: 'Save for Later',
      event139: offerPrice,
    });
    return addItemToSflList({ ...payloadData });
  };

  removeSflItem = () => {
    const { productDetail, startSflItemDelete } = this.props;
    const {
      itemInfo: { isGiftItem },
      productInfo: { skuId, generalProductId, productPartNumber },
    } = productDetail;
    const catEntryId = isGiftItem ? generalProductId : skuId;
    removeRecFromStorage(productPartNumber);
    const payloadData = { catEntryId };
    this.clearToggleErrorState();
    return startSflItemDelete({ ...payloadData });
  };

  moveToBagSflItem = () => {
    const { productDetail, startSflDataMoveToBag, setClickAnalyticsData } = this.props;
    const {
      itemInfo: { itemId, isGiftItem, color, name, offerPrice, itemBrand },
      productInfo: { skuId, generalProductId, productPartNumber, multiPackItems },
      miscInfo: { store },
    } = productDetail;
    const catEntryId = isGiftItem ? generalProductId : skuId;

    const payloadData = { itemId, catEntryId, multiPackItems };
    this.clearToggleErrorState();
    const productsData = {
      color,
      id: itemId,
      name,
      price: offerPrice,
      extPrice: offerPrice,
      offerPrice,
      partNumber: productPartNumber,
      sku: skuId.toString(),
      pricingState: 'full price',
      colorId: generalProductId,
      storeId: store,
      prodBrand: itemBrand,
    };
    setClickAnalyticsData({
      customEvents: ['event140'],
      products: [productsData],
      eventName: 'Move to Bag',
      event142: offerPrice,
    });
    return startSflDataMoveToBag({ ...payloadData });
  };

  handleSubmit = (itemId, skuId, quantity, itemPartNumber, variantNo, selectedSize, formValue) => {
    const { updateCartItem, isMiniBagOpen, editableProductMultiPackInfo = {} } = this.props;
    const {
      multiPackInfo: {
        newPartNumber: multipackProduct,
        multiPackCount,
        getAllMultiPack,
        getAllNewMultiPack,
      } = {},
      tcpMultiPackReferenceUSStore: partIdInfo,
    } = editableProductMultiPackInfo;
    const skuInfo = { ...formValue, skuId };
    this.clearToggleErrorState();
    updateCartItem(
      itemId,
      skuId,
      quantity,
      itemPartNumber,
      variantNo,
      isMiniBagOpen,
      multipackProduct,
      multiPackCount,
      selectedSize,
      getAllNewMultiPack,
      getAllMultiPack,
      skuInfo,
      partIdInfo
    );
    this.toggleFormVisibility();
  };

  getBossBopisDetailsForMiniBag = (productDetail, labels) => {
    return (
      <Col className="padding-left-13" colSize={{ small: 4, medium: 6, large: 8 }}>
        {productDetail.miscInfo.store && (
          <BodyCopy
            fontFamily="secondary"
            color="gray.600"
            component="span"
            fontSize="fs10"
            fontWeight={['extrabold']}
          >
            {getModifiedString(
              labels,
              productDetail.miscInfo.store,
              productDetail.miscInfo.orderItemType,
              productDetail.miscInfo.bossStartDate,
              productDetail.miscInfo.bossEndDate
            )}
          </BodyCopy>
        )}
      </Col>
    );
  };

  getBadgeDetails = (productDetail) => {
    return (
      <Row className="product-detail-row">
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <BodyCopy
            fontFamily="secondary"
            tag="span"
            fontSize="fs10"
            fontWeight={['extrabold']}
            dataLocator="addedtobag-productname"
          >
            {productDetail.miscInfo.badge}
          </BodyCopy>
        </Col>
      </Row>
    );
  };

  renderSflActionsLinks = () => {
    const { productDetail, isShowSaveForLater, labels, isBagPageSflSection } = this.props;
    const { isEdit } = this.state;
    if (isEdit) return null;
    if (
      !isBagPageSflSection &&
      productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY.OK &&
      isShowSaveForLater
    ) {
      return (
        <ClickTracker name="Save_for_Later">
          <Anchor
            dataLocator="saveForLaterLink"
            onClick={(e) => {
              e.preventDefault();
              this.handleMoveItemtoSaveList();
            }}
            noLink
            className="sflActions"
            aria-label={`Save for later ${productDetail.itemInfo.name}`}
          >
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs12"
              component="span"
              fontWeight={['semibold']}
            >
              {labels.saveForLaterLink}
            </BodyCopy>
          </Anchor>
        </ClickTracker>
      );
    }
    if (
      isBagPageSflSection &&
      productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY.OK
    ) {
      return (
        <ClickTracker name="Move_to_Bag">
          <Anchor
            dataLocator="moveToBagLink"
            onClick={(e) => {
              e.preventDefault();
              this.moveToBagSflItem();
            }}
            noLink
            className="sflActions"
            aria-label={`Move to bag ${productDetail.itemInfo.name}`}
          >
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs12"
              component="span"
              fontWeight={['semibold']}
            >
              {labels.moveToBagLink}
            </BodyCopy>
          </Anchor>
        </ClickTracker>
      );
    }
    return null;
  };

  removeCartItem = () => {
    const {
      removeCartItem,
      pageView,
      productDetail,
      isGenricGuest,
      isCondense,
      isBagPageSflSection,
      setClickAnalyticsData,
      isMultipackProduct,
      currencyExchange,
    } = this.props;
    const {
      itemInfo: { itemId, isGiftItem, itemBrand },
      productInfo: { skuId, generalProductId, productPartNumber },
      miscInfo: { orderItemType, categoryName },
    } = productDetail;
    const { itemInfo, productInfo } = productDetail;
    const catEntryId = isGiftItem ? generalProductId : skuId;
    const userInfoRequired = isGenricGuest && isGenricGuest.get('userId') && isCondense; // Flag to check if getRegisteredUserInfo required after SflList

    this.clearToggleErrorState();
    setClickAnalyticsData({
      eventName: 'remove Cart',
      products: [{ ...itemInfo, ...productInfo }],
    });
    removeRecFromStorage(productPartNumber);
    removeCartItem({
      itemId,
      pageView,
      catEntryId,
      userInfoRequired,
      isBagPageSflSection,
      itemBrand,
      orderItemType,
      isMultipackProduct,
    });
    const removedProduct = {
      id: productPartNumber,
      price: itemInfo.price,
      brand: itemBrand,
      category: categoryName || '',
      variant: itemInfo.color,
      quantity: itemInfo.qty,
    };
    fireEnhancedEcomm({
      eventName: 'removeFromCart',
      productsObj: [removedProduct],
      eventType: 'remove',
      pageName: 'shopping bag',
      currCode: currencyExchange && currencyExchange.length && currencyExchange[0].id,
    });
  };

  renderEditLink = () => {
    const {
      labels,
      showOnReviewPage,
      isBagPageSflSection,
      isEditAllowed,
      productDetail,
      productDetail: {
        miscInfo: { orderItemType, availability },
        itemInfo: { itemBrand },
      },
    } = this.props;

    const { isBossEnabled, isBopisEnabled } = getBossBopisFlags(this.props, itemBrand);
    const isBOPISOrder = isBopisOrder(orderItemType);
    const isBOSSOrder = isBossOrder(orderItemType);
    const isEcomSoldout = isSoldOut(availability);

    const { bossDisabled, bopisDisabled } = checkBossBopisDisabled(
      this.props,
      isBossEnabled,
      isBopisEnabled,
      isEcomSoldout,
      isBOSSOrder,
      isBOPISOrder
    );
    return (
      <>
        {showOnReviewPage &&
          !isBagPageSflSection &&
          isEditAllowed &&
          !hideEditBossBopis(isBOSSOrder, bossDisabled, isBOPISOrder, bopisDisabled) && (
            <Anchor
              role="button"
              dataLocator={getLocator('cart_item_edit_link')}
              className="padding-left-10 responsive-edit-css"
              aria-label={`Edit ${productDetail.itemInfo.name}`}
              onClick={(e) => {
                e.preventDefault();
                this.callEditMethod(e);
              }}
              onKeyDown={(e) => this.handleKeyDown(e, this.callEditMethod)}
              noLink
            >
              <BodyCopy
                fontFamily="secondary"
                fontSize="fs12"
                component="span"
                fontWeight={['semibold']}
              >
                {labels.edit}
              </BodyCopy>
            </Anchor>
          )}
      </>
    );
  };

  // eslint-disable-next-line complexity
  getItemDetails = (productDetail, labels, pageView) => {
    const { isEdit } = this.state;
    const { isBagPageSflSection, currencyExchange, newMiniBag } = this.props;
    const { offerPrice } = productDetail.itemInfo;
    const { price } = getPrices({
      productDetail,
      currencyExchange,
    });
    // SFL prices
    const isBagPage = pageView === 'myBag';
    const topPaddingClass = isBagPageSflSection ? 'padding-top-40' : 'padding-top-15';
    return (
      <Row className={`${topPaddingClass} padding-bottom-20 parent-${pageView}`} fullBleed>
        {!isBagPage && this.getBossBopisDetailsForMiniBag(productDetail, labels)}
        <Col className="save-for-later-label" colSize={{ small: 1, medium: 1, large: 3 }}>
          {productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY.SOLDOUT && (
            <BodyCopy
              fontFamily="secondary"
              className={!isBagPage ? 'updateOOSMiniBag' : 'updateOOSBag'}
              color="error"
              fontSize="fs12"
              component="span"
              dataLocator={getLocator('cart_item_soldOut_remove')}
              onClick={this.removeCartItem}
            >
              {labels.removeEdit}
            </BodyCopy>
          )}
          {productDetail.miscInfo.availability !== CARTPAGE_CONSTANTS.AVAILABILITY.OK &&
            productDetail.miscInfo.availability !== CARTPAGE_CONSTANTS.AVAILABILITY.SOLDOUT &&
            !isEdit && (
              <BodyCopy
                fontFamily="secondary"
                className={!isBagPage ? 'updateOOSMiniBag' : 'updateOOSBag'}
                color="error"
                fontSize="fs12"
                component="span"
                dataLocator={getLocator('cart_item_unavailable_update')}
                onClick={this.callEditMethod}
              >
                {labels.update}
              </BodyCopy>
            )}
          {!newMiniBag && this.renderSflActionsLinks()}
          {!newMiniBag && isBagPage && this.renderEditLink()}
        </Col>
        {isBagPage && (
          <BodyCopy
            className="price-label"
            fontFamily="secondary"
            component="span"
            fontSize="fs16"
            fontWeight={['extrabold']}
            dataLocator={getLocator('cart_item_total_price')}
          >
            <PriceCurrency price={isBagPageSflSection ? price : offerPrice} />
          </BodyCopy>
        )}
      </Row>
    );
  };

  getColorLabel = (productDetail, labels) => {
    return productDetail.itemInfo.isGiftItem === true ? `${labels.design}:` : `${labels.color}:`;
  };

  getSizeLabel = (productDetail, labels) => {
    return productDetail.itemInfo.isGiftItem === true ? `${labels.value}:` : `${labels.size}:`;
  };

  getPointsColor = () => {
    const { isPlcc } = this.props;
    if (isPlcc) {
      return 'blue.B100';
    }
    return 'orange.800';
  };

  getProductItemUpcNumber = (productDetail, isBagPage) => {
    if (isBagPage) {
      return (
        <Row className="product-detail-row">
          <Col className="productImgBrand" colSize={{ small: 6, medium: 8, large: 12 }}>
            <BodyCopy
              fontFamily="secondary"
              tag="span"
              fontSize="fs10"
              dataLocator={getLocator('cart_item_upc')}
            >
              {`UPC: ${productDetail.productInfo.upc}`}
            </BodyCopy>
          </Col>
        </Row>
      );
    }
    return '';
  };

  getSalePrice = (showOnReviewPage, offerPrice, salePrice, isMiniBag) => {
    return !showOnReviewPage || isMiniBag ? offerPrice : salePrice;
  };

  // hide was price on review page
  getPriceCurrency = (showOnReviewPage, wasPrice, listPrice, price, isMiniBag) => {
    const { newMiniBag } = this.props;
    if (newMiniBag) {
      return showOnReviewPage && <PriceCurrency price={Number(wasPrice)} />;
    }
    if (isMiniBag && listPrice === price) {
      return <></>;
    }
    const priceToShow = isMiniBag ? listPrice : wasPrice;
    return showOnReviewPage && <PriceCurrency price={Number(priceToShow)} />;
  };

  getPriceClass = (showOnReviewPage, newMiniBag) => {
    if (newMiniBag) {
      return 'prominent-price';
    }
    return !showOnReviewPage && 'reviewPagePrice';
  };

  getProductPriceList = (productDetail, pageView, currencyExchange) => {
    const { isBagPageSflSection, showOnReviewPage, labels, isMiniBag, newMiniBag } = this.props;
    const { isGiftItem, offerPrice } = productDetail.itemInfo;

    const { salePrice, wasPrice, listPrice, price } = getPrices({
      productDetail,
      currencyExchange,
    });
    const getSaleValue = this.getSalePrice(showOnReviewPage, offerPrice, salePrice, isMiniBag);
    const getPriceCurrencyValue = this.getPriceCurrency(
      showOnReviewPage,
      wasPrice,
      listPrice,
      price,
      isMiniBag
    );
    if (isBagPageSflSection) {
      return (
        <>
          {showOnReviewPage && (
            <Col className="label-responsive" colSize={{ large: 3, medium: 3, small: 2 }}>
              <BodyCopy
                fontFamily="secondary"
                component="span"
                fontSize="fs12"
                fontWeight={['extrabold']}
              >
                {`${labels.price}: `}
              </BodyCopy>
            </Col>
          )}
          <Col className="value-responsive" colSize={{ small: 2, medium: 3, large: 8 }}>
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs13"
              dataLocator={getLocator('sfl_sale_price')}
              fontWeight={['extrabold']}
            >
              <PriceCurrency price={Number(price)} />
            </BodyCopy>
            {!isGiftItem && listPrice !== price && (
              <BodyCopy
                fontFamily="secondary"
                component="span"
                fontSize="fs12"
                dataLocator={getLocator('sfl_was_price')}
                fontWeight={['regular']}
                className="was-price"
              >
                <PriceCurrency price={Number(listPrice)} />
              </BodyCopy>
            )}
          </Col>
        </>
      );
    }
    return (
      <>
        {showOnReviewPage && !newMiniBag && (
          <Col className="label-responsive" colSize={{ large: 3, medium: 3, small: 2 }}>
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs12"
              fontWeight={['extrabold']}
            >
              {`${labels.price}: `}
            </BodyCopy>
          </Col>
        )}
        <Col
          className={`value-responsive ${newMiniBag ? 'positioned-price' : ''}`}
          colSize={{ small: 2, medium: 3, large: 8 }}
        >
          <BodyCopy
            fontFamily="secondary"
            component="span"
            fontSize={showOnReviewPage ? 'fs12' : 'fs16'}
            dataLocator={getLocator('cart_sale_price')}
            fontWeight={['extrabold']}
            className={this.getPriceClass(showOnReviewPage, newMiniBag)}
          >
            <PriceCurrency price={Number(getSaleValue)} />
          </BodyCopy>
          {!isGiftItem && wasPrice !== salePrice && (
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs12"
              dataLocator={getLocator('cart_was_price')}
              fontWeight={['regular']}
              className="was-price"
            >
              {getPriceCurrencyValue}
            </BodyCopy>
          )}
        </Col>
      </>
    );
  };

  getProductPointsList = (productDetail, isBagPageSflSection, showOnReviewPage) => {
    const { labels, isInternationalShipping, isBrierleyWorking, newMiniBag } = this.props;
    if (!isBrierleyWorking) return null;
    return (
      <>
        {!isCanada() && !isInternationalShipping && !isBagPageSflSection && showOnReviewPage && (
          <Row className="product-detail-row label-responsive-wrapper">
            {!newMiniBag && (
              <Col
                className="label-responsive label-responsive-price"
                colSize={{ large: 3, medium: 3, small: 2 }}
              >
                <BodyCopy
                  fontFamily="secondary"
                  component="span"
                  fontSize="fs12"
                  fontWeight={['extrabold']}
                >
                  {`${labels.points}:`}
                </BodyCopy>
              </Col>
            )}
            <Col className="value-responsive" colSize={{ small: 2, medium: 3, large: 3 }}>
              <BodyCopy
                fontFamily="secondary"
                component="span"
                fontSize="fs12"
                fontWeight={['extrabold']}
                color={this.getPointsColor()}
                dataLocator={getLocator('cart_item_points')}
              >
                {productDetail.itemInfo.myPlacePoints}
              </BodyCopy>
              {newMiniBag && (
                <BodyCopy
                  className="points-label"
                  fontFamily="secondary"
                  component="span"
                  fontSize="fs12"
                  dataLocator={getLocator('cart_item_points')}
                  color="gray.900"
                >
                  {' '}
                  {labels.ptsCartTile}
                </BodyCopy>
              )}
            </Col>
          </Row>
        )}
      </>
    );
  };

  getProductFit = (productDetail, newMiniBag) => {
    const fitValue = !productDetail.itemInfo.fit ? '' : ` ${productDetail.itemInfo.fit}`;
    if (fitValue && newMiniBag) {
      return capitalizeWords(fitValue);
    }
    return fitValue;
  };

  getUnavailableHeaderClass = () => {
    const { productDetail } = this.props;
    if (productDetail.miscInfo.availability === 'UNAVAILABLE') {
      return 'unavailable-header';
    }
    return '';
  };

  renderItemQuantity = () => {
    const { isBagPageSflSection, labels, productDetail, newMiniBag } = this.props;
    if (isBagPageSflSection) return null;
    return (
      <div>
        {!newMiniBag && (
          <div className="color-size-fit-label color-fit-size-desktop">
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs13"
              fontWeight={['extrabold']}
            >
              {`${labels.qty}:`}
            </BodyCopy>
          </div>
        )}
        <BodyCopy
          className="padding-left-10"
          fontFamily="secondary"
          component="span"
          fontSize="fs13"
          color={getFontColor(newMiniBag)}
          dataLocator="addedtobag-productqty"
        >
          {`${productDetail.itemInfo.qty} `}
          {newMiniBag && (
            <div className="color-size-fit-label color-fit-size-desktop">
              <BodyCopy
                fontFamily="secondary"
                component="span"
                fontSize="fs12"
                fontWeight={['regular']}
                color="gray.900"
              >
                {`  ${labels.qty}`}
              </BodyCopy>
            </div>
          )}
        </BodyCopy>
      </div>
    );
  };

  renderSizeAndFit = () => {
    const { labels, productDetail, isBagPageSflSection, newMiniBag } = this.props;
    return (
      <div>
        {!newMiniBag && (
          <div className="color-size-fit-label color-fit-size-desktop">
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs13"
              fontWeight={['extrabold']}
            >
              {this.getSizeLabel(productDetail, labels)}
            </BodyCopy>
          </div>
        )}
        <BodyCopy
          className={`${!newMiniBag ? 'padding-left-10' : ''}`}
          fontFamily="secondary"
          component="span"
          fontSize="fs13"
          color={getFontColor(newMiniBag)}
          dataLocator={getLocator('cart_item_size')}
        >
          {`${productDetail.itemInfo.size}`}
          {this.getProductFit(productDetail, newMiniBag)}
        </BodyCopy>
        {!isBagPageSflSection && (
          <BodyCopy
            className="color-fit-size-separator"
            fontFamily="secondary"
            component="span"
            fontSize="fs12"
            color="gray.600"
          >
            |
          </BodyCopy>
        )}
      </div>
    );
  };

  renderHeartIcon = () => {
    const { isBagPageSflSection, labels, handleAddToWishlist, productDetail } = this.props;
    if (
      !isBagPageSflSection ||
      productDetail.miscInfo.availability !== CARTPAGE_CONSTANTS.AVAILABILITY.OK
    )
      return null;

    return (
      <div className="heartIcon">
        <Image
          alt={getLabelValue(labels, 'lbl_sfl_favIcon', 'bagPage', 'checkout')}
          className="sfl-fav-image"
          src={getIconPath('fav-icon')}
          onClick={handleAddToWishlist}
        />
      </div>
    );
  };

  getCrossIconImage = () => {
    const {
      isBagPageSflSection,
      productDetail: {
        itemInfo: { name, qty },
        productInfo: { variantNo, skuId },
      },
      labels: { removeEdit },
      newMiniBag,
    } = this.props;
    return (
      <Image
        alt={`${removeEdit} ${name}`}
        role="button"
        tabIndex="0"
        className="close-icon-image"
        src={getIconPath(newMiniBag ? 'bag-close-icon' : 'close-icon')}
        onClick={isBagPageSflSection ? this.removeSflItem : this.removeCartItem}
        onKeyDown={(e) =>
          this.handleKeyDown(e, isBagPageSflSection ? this.removeSflItem : this.removeCartItem)
        }
        unbxdattr="RemoveFromCart"
        unbxdparam_sku={skuId}
        unbxdparam_variant={variantNo}
        unbxdparam_qty={qty}
      />
    );
  };

  /**
   * @function renderUnavailableErrorMessage
   * @param {Object} settings
   * @returns {JSX} Returns Item Unavailable component with respective variation of text via passed input
   * @memberof CartItemTile
   */
  renderUnavailableErrorMessage = ({
    isEcomSoldout,
    bossDisabled,
    isBOSSOrder,
    bopisDisabled,
    isBOPISOrder,
    noBossMessage,
    noBopisMessage,
    availability,
  }) => {
    const { labels } = this.props;
    let unavailableMessage = '';
    if (isEcomSoldout) {
      unavailableMessage = labels.soldOutError;
    } else if (isBOSSOrder) {
      unavailableMessage = getBOSSUnavailabilityMessage(
        bossDisabled,
        noBossMessage,
        availability,
        labels
      );
    } else if (isBOPISOrder) {
      unavailableMessage = getBOPISUnavailabilityMessage(
        bopisDisabled,
        noBopisMessage,
        availability,
        labels
      );
    } else {
      unavailableMessage = getSTHUnavailabilityMessage(availability, labels);
    }

    return unavailableMessage ? (
      <ItemAvailability
        className="unavailable-error"
        errorMsg={isEcomSoldout ? unavailableMessage : labels.itemUnavailable}
        chooseDiff={!isEcomSoldout ? unavailableMessage : null}
      />
    ) : null;
  };

  /**
   * @function headerAndAvailabilityErrorContainer
   * @param {Object} settings
   * @returns {JSX} Returns Error Message component
   * @memberof CartItemTile
   */
  headerAndAvailabilityErrorContainer = ({
    isEcomSoldout,
    bossDisabled,
    isBOSSOrder,
    bopisDisabled,
    isBOPISOrder,
    noBossMessage,
    noBopisMessage,
    availability,
  }) => {
    const { pageView, showOnReviewPage } = this.props;
    const { isEdit } = this.state;
    return (
      showOnReviewPage && (
        <div className={this.getUnavailableHeaderClass()}>
          {this.renderUnavailableErrorMessage({
            isEcomSoldout,
            bossDisabled,
            isBOSSOrder,
            bopisDisabled,
            isBOPISOrder,
            noBossMessage,
            noBopisMessage,
            availability,
          })}
          {!isEdit && (
            <div className={pageView === 'myBag' ? 'crossDeleteIconBag' : 'crossDeleteIconMiniBag'}>
              {this.getCrossIconImage()}
            </div>
          )}
        </div>
      )
    );
  };

  getProductDetailClass = () => {
    const { showOnReviewPage } = this.props;
    return showOnReviewPage ? 'product-detail' : 'product-detail product-detail-review-page';
  };

  renderReviewPageSection = () => {
    const { showOnReviewPage } = this.props;
    return (
      <>
        {!showOnReviewPage ? (
          <div className="size-and-item-container">
            {this.renderSizeAndFit()}
            {this.renderItemQuantity()}
          </div>
        ) : (
          <>
            {this.renderSizeAndFit()}
            {this.renderItemQuantity()}
          </>
        )}
      </>
    );
  };

  /**
   * @function renderTogglingError Render Toggling error
   * @returns {JSX} Error Component with toggling api error.
   * @memberof CartItemTile
   */
  renderTogglingError = () => {
    const {
      pageView,
      toggleError,
      productDetail: {
        itemInfo: { itemId },
      },
    } = this.props;
    return pageView === 'myBag' && toggleError && itemId === toggleError.itemId ? (
      <ErrorMessage
        className="toggle-error"
        fontSize="fs12"
        fontWeight="extrabold"
        error={toggleError.errorMessage}
      />
    ) : null;
  };

  getPdpToPath = (
    isProductBrandOfSameDomain,
    pdpUrl,
    crossDomain,
    brandSpecificCrossDomain,
    brand
  ) => {
    return isProductBrandOfSameDomain
      ? getProductListToPath(pdpUrl)
      : `${brandSpecificCrossDomain[brand.toUpperCase()]}${pdpUrl}`;
  };

  getPdpAsPathurl = (
    isProductBrandOfSameDomain,
    pdpUrl,
    crossDomain,
    cookieToken,
    brandSpecificCrossDomain,
    brand
  ) => {
    if (isProductBrandOfSameDomain) {
      return pdpUrl;
    }
    if (cookieToken)
      return `${brandSpecificCrossDomain[brand.toUpperCase()]}${pdpUrl}${
        urlContainsQuery(pdpUrl) ? '&' : '?'
      }ct=${cookieToken}`;
    return `${brandSpecificCrossDomain[brand.toUpperCase()]}${pdpUrl}`;
  };

  closeMiniBagMethod = () => {
    const { closeMiniBag } = this.props;
    closeMiniBag();
  };

  getItemBrand = (itemBrand) => itemBrand && itemBrand.toUpperCase();

  getImageConfigs = (showOnReviewPage) => {
    return showOnReviewPage
      ? ['t_t_cart_item_tile_m', 't_t_cart_item_tile_t', 't_t_cart_item_tile_d']
      : ['t_t_review_cart_item_m', 't_t_review_cart_item_t', 't_t_review_cart_item_d'];
  };

  /**
   * @function renderEddView
   * @returns {JSX}
   */
  renderEddView = (
    isEcomSoldout,
    isECOMOrder,
    isBossEnabled,
    isBopisEnabled,
    store,
    isReviewPage
  ) => {
    const {
      productDetail: {
        productInfo: { itemPartNumber },
      },
      newBag,
    } = this.props;
    return (
      !showRadioButtons({
        isEcomSoldout,
        isECOMOrder,
        isBossEnabled,
        isBopisEnabled,
        store,
      }) && (
        <BodyCopy className={isReviewPage ? 'edd-wrapper-review' : 'edd-wrapper'} component="div">
          <EddComponent itemIdprop={itemPartNumber} showIcon newBag={newBag} />
        </BodyCopy>
      )
    );
  };

  showEDDonReview = (isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store) => {
    const { pageView } = this.props;
    if (pageView !== 'myBag') {
      return this.renderEddView(
        isEcomSoldout,
        isECOMOrder,
        isBossEnabled,
        isBopisEnabled,
        store,
        true
      );
    }
    return null;
  };

  showEDDonBag = (isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store) => {
    const { pageView } = this.props;
    if (pageView === 'myBag') {
      return this.renderEddView(isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store);
    }
    return null;
  };

  getDynamicBadgeOverlayQty = (isDynamicBadgeEnabled, tcpStyleType, tcpStyleQty) =>
    getDynamicBadgeQty(tcpStyleType, tcpStyleQty, isDynamicBadgeEnabled);

  showVariantNo = (showOnReviewPage, productDetail, labels) => {
    const { newMiniBag } = this.props;
    return !showOnReviewPage ? (
      <div>
        {!newMiniBag && (
          <div className="color-size-fit-label">
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs13"
              fontWeight={['extrabold']}
              textAlign="left"
            >
              {labels.itemNumberLabel}
            </BodyCopy>
          </div>
        )}
        <BodyCopy
          className="padding-left-10"
          fontFamily="secondary"
          component="span"
          fontSize="fs13"
          color={getFontColor(newMiniBag)}
        >
          {`${productDetail.productInfo.variantNo}`}
        </BodyCopy>
      </div>
    ) : null;
  };

  getRoundedClass = (newBag) => (newBag ? 'rounded-container' : '');

  getEditLink = (isBagPage, newMiniBag) => !isBagPage && !newMiniBag && this.renderEditLink();

  getFontWeight = (newMiniBag) => (newMiniBag ? 'bold' : 'extrabold');

  // eslint-disable-next-line complexity
  renderSizeColor = (productDetail, labels) => {
    const { newMiniBag } = this.props;
    return (
      <div className={`${newMiniBag ? 'color-label-block' : ''}`}>
        {!newMiniBag && (
          <div className="color-size-fit-label">
            <BodyCopy
              fontFamily="secondary"
              component="span"
              fontSize="fs13"
              fontWeight={['extrabold']}
              textAlign="left"
            >
              {this.getColorLabel(productDetail, labels)}
            </BodyCopy>
          </div>
        )}
        <BodyCopy
          className={`${!newMiniBag ? 'padding-left-10' : ''}`}
          fontFamily="secondary"
          component="span"
          fontSize="fs13"
          color={getFontColor(newMiniBag)}
          dataLocator={getLocator('cart_item_color')}
        >
          {`${
            newMiniBag
              ? capitalizeWords(productDetail.itemInfo.color)
              : productDetail.itemInfo.color
          }`}
        </BodyCopy>
      </div>
    );
  };

  getBannerIsSFLOrFulfillmentDisabled = ({
    isBagPage,
    isEcomSoldout,
    isECOMOrder,
    isBossEnabled,
    isBopisEnabled,
    store,
  }) => {
    const {
      showOnReviewPage,
      isBagPageSflSection,
      isLowInventoryBannerEnabledForOldDesign,
      labels,
      productDetail,
    } = this.props;
    return (
      showOnReviewPage &&
      isBagPage &&
      (!showRadioButtons({
        isEcomSoldout,
        isECOMOrder,
        isBossEnabled,
        isBopisEnabled,
        store,
      }) ||
        isBagPageSflSection) && (
        <Row className="low-inv-sfl">
          <Col colSize={{ medium: 12, large: 12 }}>
            {this.renderLowInventoryBanner({
              isLowInventoryBannerEnabledForOldDesign,
              lowInventoryMsg: labels.lowInventoryCartTileTxt,
              threshold: isBagPageSflSection
                ? labels.lblLowInvetoryThresholdSFL
                : labels.lblLowInvetoryThresholdShipToHome,
              selectedMethod: false,
              availableInventory: isBagPageSflSection
                ? productDetail.miscInfo.sflAvailInventory
                : productDetail.miscInfo.inventoryAvailECOM,
              isBagPageSflSection,
            })}
          </Col>
        </Row>
      )
    );
  };

  renderLowInventoryBanner = ({
    isLowInventoryBannerEnabledForOldDesign,
    lowInventoryMsg,
    threshold,
    selectedMethod,
    availableInventory,
    isBagPageSflSection = false,
  }) => {
    const showInvMsg = 'show-low-inventory-banner-selected-method';
    const isSflSection = isBagPageSflSection ? showInvMsg : '';
    if (
      isLowInventoryBannerEnabledForOldDesign &&
      threshold > 0 &&
      threshold !== undefined &&
      threshold !== '' &&
      threshold > availableInventory &&
      availableInventory > 0
    ) {
      return (
        <BodyCopy
          component="span"
          className={`${selectedMethod ? showInvMsg : 'show-low-inventory-banner'} ${isSflSection}`}
        >
          <LowInventoryBanner
            isLowInventoryBannerEnabledForOldDesign={isLowInventoryBannerEnabledForOldDesign}
            lowInventoryMsg={lowInventoryMsg.replace('[val]', availableInventory)}
            threshold={threshold}
            availableInventory={availableInventory}
          />
        </BodyCopy>
      );
    }
    return null;
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  render() {
    const { isEdit } = this.state;
    const {
      productDetail,
      productDetail: {
        miscInfo: { store, orderItemType, availability },
        itemInfo: { itemBrand },
        productInfo: { pdpUrl, tcpStyleQty, tcpStyleType },
      },
      labels,
      editableProductInfo,
      className,
      pageView,
      isBagPageSflSection,
      showOnReviewPage,
      setShipToHome,
      currencyExchange,
      pickupStoresInCart,
      autoSwitchPickupItemInCart,
      orderId,
      disableProductRedirect,
      cookieToken,
      editableProductMultiPackInfo,
      isDynamicBadgeEnabled,
      newBag,
      newMiniBag,
      isLowInventoryBannerEnabledForOldDesign,
    } = this.props;
    const { isBossEnabled, isBopisEnabled } = getBossBopisFlags(this.props, itemBrand);
    const isECOMOrder = isEcomOrder(orderItemType);
    const isBOPISOrder = isBopisOrder(orderItemType);
    const isBOSSOrder = isBossOrder(orderItemType);
    const isEcomSoldout = isSoldOut(availability);
    const apiConfigObj = getAPIConfig();
    const { crossDomain, brandSpecificCrossDomain } = apiConfigObj;
    const currentSiteBrand = getBrand();
    const isProductBrandOfSameDomain =
      currentSiteBrand.toUpperCase() === this.getItemBrand(itemBrand);
    const { noBopisMessage, noBossMessage } = noBossBopisMessage(this.props);
    const { bossDisabled, bopisDisabled } = checkBossBopisDisabled(
      this.props,
      isBossEnabled,
      isBopisEnabled,
      isEcomSoldout,
      isBOSSOrder,
      isBOPISOrder
    );

    const initialValues = {
      color: { name: productDetail.itemInfo.color },
      Fit: productDetail.itemInfo.fit,
      Size: productDetail.itemInfo.size,
      Qty: productDetail.itemInfo.qty,
    };
    const productBrand = productDetail?.itemInfo?.itemBrand;
    const pdpToPath = this.getPdpToPath(
      isProductBrandOfSameDomain,
      pdpUrl,
      crossDomain,
      brandSpecificCrossDomain,
      productBrand
    );
    const pdpAsPathUrl = this.getPdpAsPathurl(
      isProductBrandOfSameDomain,
      pdpUrl,
      crossDomain,
      cookieToken,
      brandSpecificCrossDomain,
      productBrand
    );
    const imgConfig = this.getImageConfigs(showOnReviewPage);
    const isBagPage = pageView === 'myBag';
    const disableLink = disableProductRedirect;
    const dynamicBadgeOverlayQty = this.getDynamicBadgeOverlayQty(
      isDynamicBadgeEnabled,
      tcpStyleType,
      tcpStyleQty
    );
    const { badgeConfig } = config.IMG_DATA_CLIENT_BAGPAGE;
    return (
      <div className={`${className} tile-header ${this.getRoundedClass(newBag)}`}>
        {this.renderTogglingError()}
        {this.headerAndAvailabilityErrorContainer({
          isEcomSoldout,
          bossDisabled,
          isBOSSOrder,
          bopisDisabled,
          isBOPISOrder,
          noBossMessage,
          noBopisMessage,
          availability,
        })}
        <Row
          fullBleed
          className={['product', isBagPage ? 'product-tile-wrapper' : ''].join(' ')}
          tabIndex="0"
          aria-label={`${productDetail.itemInfo.name}. ${labels.price} ${productDetail.itemInfo.price}. ${labels.size} ${productDetail.itemInfo.size}. ${labels.qty} ${productDetail.itemInfo.qty}`}
        >
          <Col
            key="productDetails"
            className="align-product-img product-brand-img-wrapper"
            colSize={{ small: 2, medium: 2, large: 3 }}
          >
            <div className="imageWrapper">
              {/* <Image
                alt={labels.productImageAlt}
                className="product-image"
                src={endpoints.global.baseURI + productDetail.itemInfo.imagePath}
                data-locator={getLocator('cart_item_image')}
              /> */}
              <LinkWrapper
                pdpToPath={pdpToPath}
                pdpAsPathUrl={pdpAsPathUrl}
                disableLink={disableLink}
                noWrap={disableLink}
                IsSlugPathAdded
              >
                <DamImage
                  imgData={{
                    alt: `${labels.productImageAlt} ${productDetail.itemInfo.name}`,
                    url: productDetail.itemInfo.imagePath,
                  }}
                  imgConfigs={imgConfig}
                  itemBrand={this.getItemBrand(productBrand)}
                  isProductImage
                  onClick={this.closeMiniBagMethod}
                  className={`${!showOnReviewPage ? 'dam-image-review-page' : ''}`}
                  dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
                  tcpStyleType={tcpStyleType}
                  badgeConfig={badgeConfig}
                />
              </LinkWrapper>
              {availability === CARTPAGE_CONSTANTS.AVAILABILITY.SOLDOUT && (
                <BodyCopy
                  className="soldOutLabel"
                  component="span"
                  fontFamily="secondary"
                  textAlign="center"
                  fontSize="fs12"
                >
                  {labels.soldOut}
                </BodyCopy>
              )}
            </div>
            {!productDetail.itemInfo.isGiftItem && (
              <div className="logoWrapper">
                <Image
                  alt={labels[`${(productBrand || '').toLowerCase()}BrandAlt`]}
                  className="brand-image"
                  src={getIconPath(`header__brand-tab-${(productBrand || '').toLowerCase()}`)}
                  data-locator={getLocator('cart_item_brand_logo')}
                />
              </div>
            )}
          </Col>
          <Col
            className="bag-product-detail-wrapper"
            key="productDetails"
            colSize={{ small: 4, medium: 6, large: 9 }}
          >
            {showOnReviewPage &&
              productDetail.miscInfo.badge &&
              this.getBadgeDetails(productDetail)}
            <Row className="product-detail-row">
              <Col className="productImgBrand" colSize={{ small: 6, medium: 8, large: 12 }}>
                <LinkWrapper
                  pdpToPath={pdpToPath}
                  pdpAsPathUrl={pdpAsPathUrl}
                  disableLink={disableLink}
                  noWrap={disableLink}
                  IsSlugPathAdded
                >
                  <BodyCopy
                    fontFamily="secondary"
                    component="h2"
                    fontSize="fs14"
                    fontWeight={[`${this.getFontWeight(newMiniBag)}`]}
                    dataLocator={getLocator('cart_item_title')}
                    onClick={this.closeMiniBagMethod}
                  >
                    {productDetail.itemInfo.name}
                  </BodyCopy>
                </LinkWrapper>
              </Col>
            </Row>
            {showOnReviewPage && this.getProductItemUpcNumber(productDetail, isBagPage)}
            {!isEdit ? (
              <React.Fragment>
                <Row className="product-detail-row padding-top-10 color-map-size-fit">
                  <Col
                    className={!isBagPage ? this.getProductDetailClass() : 'product-detail-bag'}
                    colSize={{ medium: 8, large: 12 }}
                  >
                    <div className="product-detail-section">
                      {this.showVariantNo(showOnReviewPage, productDetail, labels)}
                      {this.renderSizeColor(productDetail, labels)}
                      {showOnReviewPage && !newMiniBag && (
                        <BodyCopy
                          className="color-fit-size-separator"
                          fontFamily="secondary"
                          component="span"
                          fontSize="fs13"
                          color="gray.600"
                        >
                          |
                        </BodyCopy>
                      )}
                      {this.renderReviewPageSection()}
                    </div>
                    {this.getEditLink(isBagPage, newMiniBag)}
                  </Col>
                </Row>
              </React.Fragment>
            ) : (
              <ProductEditForm
                item={productDetail}
                colorFitsSizesMap={editableProductInfo}
                editableProductMultiPackInfo={editableProductMultiPackInfo}
                handleSubmit={this.handleSubmit}
                initialValues={initialValues}
                labels={labels}
                formVisiblity={this.toggleFormVisibility}
                itemBrand={this.getItemBrand(productDetail.itemInfo.itemBrand)}
                newMiniBag={newMiniBag}
              />
            )}
            <Row className="product-detail-row label-responsive-wrapper padding-top-10">
              {this.getProductPriceList(productDetail, pageView, currencyExchange)}
            </Row>
            {this.getProductPointsList(productDetail, isBagPageSflSection, showOnReviewPage)}
            {this.getBannerIsSFLOrFulfillmentDisabled({
              isBagPage,
              isEcomSoldout,
              isECOMOrder,
              isBossEnabled,
              isBopisEnabled,
              store,
            })}
            {showOnReviewPage && this.getItemDetails(productDetail, labels, pageView)}
            {this.showEDDonReview(isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store)}
          </Col>
          {showOnReviewPage && this.renderHeartIcon()}
        </Row>

        {showOnReviewPage &&
          !isBagPageSflSection &&
          isBagPage &&
          showRadioButtons({
            isEcomSoldout,
            isECOMOrder,
            isBossEnabled,
            isBopisEnabled,
            store,
          }) && (
            <Row fullBleed>
              <CartItemRadioButtons
                className="cart-item-radio-buttons"
                productDetail={productDetail}
                labels={labels}
                isEcomSoldout={isEcomSoldout}
                isECOMOrder={isECOMOrder}
                isBOSSOrder={isBOSSOrder}
                isBOPISOrder={isBOPISOrder}
                noBopisMessage={noBopisMessage}
                noBossMessage={noBossMessage}
                bossDisabled={bossDisabled}
                bopisDisabled={bopisDisabled}
                isBossEnabled={isBossEnabled}
                isBopisEnabled={isBopisEnabled}
                openPickUpModal={this.handleEditCartItemWithStore}
                setShipToHome={setShipToHome}
                pickupStoresInCart={pickupStoresInCart}
                autoSwitchPickupItemInCart={autoSwitchPickupItemInCart}
                orderId={orderId}
                newBag={newBag}
                isLowInventoryBannerEnabledForOldDesign={isLowInventoryBannerEnabledForOldDesign}
                renderLowInventoryBanner={this.renderLowInventoryBanner}
              />
              <RenderPerf.Measure name={CONTROLS_VISIBLE} />
            </Row>
          )}
        {this.showEDDonBag(isEcomSoldout, isECOMOrder, isBossEnabled, isBopisEnabled, store)}
      </div>
    );
  }
}

CartItemTile.defaultProps = {
  pageView: '',
  isEditAllowed: true,
  isCondense: true,
  sflItemsCount: 0,
  isBagPageSflSection: false,
  showOnReviewPage: true,
  onQuickViewOpenClick: () => {},
  setShipToHome: () => {},
  toggleError: null,
  toggleBossBopisError: null,
  clearToggleError: () => {},
  currencyExchange: null,
  autoSwitchPickupItemInCart: () => {},
  disableProductRedirect: false,
  closeMiniBag: () => {},
  isBrierleyWorking: true,
  cookieToken: null,
  isMiniBag: false,
  isMultipackProduct: false,
  editableProductMultiPackInfo: {},
  isDynamicBadgeEnabled: false,
  newBag: PropTypes.bool,
  isLowInventoryBannerEnabledForOldDesign: PropTypes.bool,
};

CartItemTile.propTypes = {
  productDetail: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  getProductSKUInfo: PropTypes.func.isRequired,
  updateCartItem: PropTypes.func.isRequired,
  editableProductInfo: PropTypes.shape({}).isRequired,
  removeCartItem: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  isPlcc: PropTypes.string.isRequired,
  pageView: PropTypes.string,
  toggleEditAllowance: PropTypes.func.isRequired,
  isEditAllowed: PropTypes.bool,
  isShowSaveForLater: PropTypes.bool.isRequired,
  isCondense: PropTypes.bool,
  isGenricGuest: PropTypes.shape({}).isRequired,
  sflItemsCount: PropTypes.number,
  sflMaxCount: PropTypes.number.isRequired,
  addItemToSflList: PropTypes.func.isRequired,
  setCartItemsSflError: PropTypes.func.isRequired,
  isBagPageSflSection: PropTypes.bool,
  showOnReviewPage: PropTypes.bool,
  startSflItemDelete: PropTypes.func.isRequired,
  startSflDataMoveToBag: PropTypes.func.isRequired,
  onPickUpOpenClick: PropTypes.func.isRequired,
  onQuickViewOpenClick: PropTypes.func,
  orderId: PropTypes.number.isRequired,
  setShipToHome: PropTypes.func,
  toggleError: PropTypes.shape({}),
  toggleBossBopisError: PropTypes.shape({
    errorMessage: PropTypes.string,
  }),
  clearToggleError: PropTypes.func,
  currencyExchange: PropTypes.shape([]),
  pickupStoresInCart: PropTypes.shape({}).isRequired,
  autoSwitchPickupItemInCart: PropTypes.func,
  disableProductRedirect: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func.isRequired,
  closeMiniBag: PropTypes.func,
  isMiniBagOpen: PropTypes.bool.isRequired,
  handleAddToWishlist: PropTypes.func.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  isBrierleyWorking: PropTypes.bool,
  cookieToken: PropTypes.string,
  isMiniBag: PropTypes.bool,
  isMultipackProduct: PropTypes.bool,
  editableProductMultiPackInfo: PropTypes.shape({}),
  isDynamicBadgeEnabled: PropTypes.shape({}),
  newBag: false,
  isLowInventoryBannerEnabledForOldDesign: false,
};

export default withStyles(CartItemTile, styles);
export { CartItemTile as CartItemTileVanilla };
