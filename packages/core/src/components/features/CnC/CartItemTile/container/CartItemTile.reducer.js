// 9fbef606107a605d69c0edbcd8029e5d
/**
 * These are temporary changes for a dummy Bag page
 */
import { fromJS } from 'immutable';
import CARTPAGE_CONSTANTS from '../CartItemTile.constants';

const initialState = fromJS({
  editableItemData: {},
});

const CartPage = (state = initialState, action) => {
  switch (action.type) {
    case CARTPAGE_CONSTANTS.GET_PRODUCT_SKU_INFO_SUCCESS:
      return state.set('editableItemData', fromJS(action.payload.product));
    case CARTPAGE_CONSTANTS.SET_PRODUCT_MULTIPACK_DATA:
      return state.set('editableItemMultiPackData', action.payload.product);
    case CARTPAGE_CONSTANTS.SET_TOGGLE_CART_ITEM_ERROR:
      return state.set('toggleError', action.payload);
    case CARTPAGE_CONSTANTS.CLEAR_TOGGLE_CART_ITEM_ERROR:
      return state.set('toggleError', null);
    case CARTPAGE_CONSTANTS.SET_TOGGLE_BOSS_BOPIS_CART_ITEM_ERROR:
      return state.set('toggleBossBopisError', action.payload);
    case CARTPAGE_CONSTANTS.CLEAR_TOGGLE_BOSS_BOPIS_CART_ITEM_ERROR:
      return state.set('toggleBossBopisError', null);
    case CARTPAGE_CONSTANTS.CART_LAODINGSTATE:
      return state.set('cartLoadingState', action.payload);
    case CARTPAGE_CONSTANTS.UPDATE_CART_ITEM:
      return state.set('cartItemUpdating', true);
    case CARTPAGE_CONSTANTS.UPDATE_CART_ITEM_COMPLETE:
      return state.set('cartItemUpdating', false);
    default:
      /* istanbul ignore else */
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default CartPage;
