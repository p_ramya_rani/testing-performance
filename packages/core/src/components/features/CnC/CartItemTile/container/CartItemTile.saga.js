/* eslint-disable camelcase, max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
/**
 * These are temporary changes for a dummy login page
 */
// TODO: Need fix unused/proptypes eslint error

import { call, takeLatest, put, select, actionChannel, take } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { isMobileApp } from '@tcp/core/src/utils';
import { getSaveForLaterSwitch } from '@tcp/core/src/components/features/CnC/SaveForLater/container/SaveForLater.selectors';
import { trackForterAction, ForterActionType } from '@tcp/core/src/utils/forter.util';
import {
  setLoaderState,
  setSectionLoaderState,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import { getFavoriteStoreActn } from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import { isOptimizedDeleteEnabled } from '@tcp/core/src/utils/utils';
import CARTPAGE_CONSTANTS from '../CartItemTile.constants';
import { getUserId, getDefaultStoreData } from '../../../account/User/container/User.selectors';
import {
  getIsPdpServiceEnabled,
  getABTestPdpService,
  getIsNewBag,
} from '../../../../../reduxStore/selectors/session.selectors';
import { getUserInfoSaga } from '../../../account/User/container/User.saga';
import { getStringAfterSplit, getMultipackCount } from '../../../../../utils/utils';
import CartItemTileSagaUtils from './CartItemTile.saga.utils';

import {
  removeCartItemComplete,
  updateCartItemComplete,
  getProductSKUInfoSuccess,
  setProductMultiPackData,
  setToggleCartItemError,
  clearToggleCartItemError,
  setBossBopisToggleCartItemError,
  clearToggleBossBopisCartItemError,
  setCartLoadingState,
} from './CartItemTile.actions';
import {
  AddToPickupError,
  AddToCartError,
  clearAddToBagErrorState,
  clearAddToPickupErrorState,
} from '../../AddedToBag/container/AddedToBag.actions';
import { filterItems } from '../../AddedToBag/container/AddedToBag.saga';
import { getCartDataSaga } from '../../BagPage/container/BagPage.saga';
import BAG_PAGE_ACTIONS from '../../BagPage/container/BagPage.actions';
import {
  removeItem,
  updateItem,
  getProductSkuInfoByUnbxd,
} from '../../../../../services/abstractors/CnC';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import BAGPAGE_CONSTANTS from '../../BagPage/BagPage.constants';
import {
  isItemBossBopisInEligible,
  getIsDeleteConfirmationModalEnabled,
  getPageName,
  getMultiPackInventoryThreshold,
  getCartOrderId,
} from './CartItemTile.selectors';
import getProductInfoById from '../../../../../services/abstractors/productListing/productDetail';
import { openPickupModalWithValues } from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import { handleServerSideErrorAPI } from '../../Checkout/container/Checkout.saga.util';
import CHECKOUT_ACTIONS from '../../Checkout/container/Checkout.action';

const { checkoutIfItemIsUnqualified } = BagPageSelectors;
const isOptimizedDelete = isOptimizedDeleteEnabled();
export function* afterRemovingCartItem(isMoveToSFL = true) {
  if (isMoveToSFL) {
    yield put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isDeleting: true }));
  }
}

export function makeRegisteredUserCall(isMoveToSFL, userId) {
  return !isMoveToSFL && (!userId || userId === '-1002');
}

export function trackForterRemoveActionForApp(itemId) {
  if (isMobileApp()) {
    trackForterAction(ForterActionType.REMOVE_FROM_CART, itemId);
  }
}

export function makeCartApiCall(isMiniBag, pageName) {
  return isMiniBag && pageName !== BAGPAGE_CONSTANTS.SHOPPING_BAG;
}

/**
 *@function confirmRemoveItem to be invoked to delete item form cart
 *
 * @export
 * @param {*} { payload, afterHandler }
 */
export function* confirmRemoveItem({
  payload,
  afterHandler,
  isMiniBag,
  price,
  isMoveToSFL,
  isCheckoutFlow,
}) {
  try {
    if (isMiniBag || payload.isMiniBag) {
      yield put(setSectionLoaderState({ miniBagLoaderState: true, section: 'minibag' }));
    } else {
      yield put(setLoaderState(true));
    }
    const res = yield call(removeItem, payload);
    yield put(removeCartItemComplete(res));
    yield put(BAG_PAGE_ACTIONS.removeDeletedItem(res));
    if (afterHandler) {
      afterHandler();
    }
    const userId = yield select(getUserId);
    if (makeRegisteredUserCall(isMoveToSFL, userId)) {
      yield call(getUserInfoSaga);
      const pageName = yield select(getPageName);
      if (makeCartApiCall(isMiniBag, pageName)) {
        yield call(getCartDataSaga, {
          payload: {
            onCartRes: yield call(afterRemovingCartItem, isMoveToSFL),
            recalcRewards: true,
            isRecalculateTaxes: true,
            translation: false,
            excludeCartItems: false,
            ignoreCartLoader: true,
            isCheckoutFlow,
          },
        });
      }
    } else {
      yield call(getCartDataSaga, {
        payload: {
          onCartRes: yield call(afterRemovingCartItem, isMoveToSFL),
          recalcRewards: true,
          isRecalculateTaxes: true,
          translation: false,
          excludeCartItems: false,
          ignoreCartLoader: true,
          isCheckoutFlow,
        },
      });
    }
    yield put(setLoaderState(false));
    yield put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }));

    if (isMobileApp() && price) {
      trackForterRemoveActionForApp(payload);
    }
    yield put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isDeleting: false }));
    yield put(BAG_PAGE_ACTIONS.setCartItemsSFL(false));
  } catch (err) {
    const isMinibag = isMiniBag || payload.isMiniBag;
    yield put(setLoaderState(false));
    yield put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }));
    logger.error(err);
    yield call(handleServerSideErrorAPI, err, CARTPAGE_CONSTANTS.CART_ITEM_TILE, isMinibag);
  }
}

/**
 *@function confirmRemoveItemInChannel to be invoked to delete item form cart inside
 *
 * @export
 * @param {*} { payload, afterHandler }
 */
export function* confirmRemoveItemInChannel({
  payload,
  afterHandler,
  isMiniBag,
  price,
  isMoveToSFL,
  isCheckoutFlow,
}) {
  try {
    yield put(setCartLoadingState(true));
    if (afterHandler) {
      afterHandler();
    }
    const deletedProductInfo = {
      orderId: yield select(getCartOrderId),
      orderItems: [payload],
    };
    yield put(BAG_PAGE_ACTIONS.removeDeletedItem(deletedProductInfo));
    yield put(removeCartItemComplete(deletedProductInfo));
    try {
      yield call(removeItem, payload);
    } catch (error) {
      const errorMapping = yield select(BagPageSelectors.getErrorMapping);
      const errorMessage =
        // eslint-disable-next-line no-underscore-dangle
        (error && error.errorMessage && error.errorMessage._error) ||
        (errorMapping && errorMapping.DEFAULT) ||
        'ERROR';
      yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(errorMessage));
      yield put(BAG_PAGE_ACTIONS.addDeletedItemOnAPiFailure());
    }
    const userId = yield select(getUserId);
    if (makeRegisteredUserCall(isMoveToSFL, userId)) {
      yield call(getUserInfoSaga);
      const pageName = yield select(getPageName);
      if (makeCartApiCall(isMiniBag, pageName)) {
        yield call(getCartDataSaga, {
          payload: {
            onCartRes: yield call(afterRemovingCartItem, isMoveToSFL),
            recalcRewards: true,
            isRecalculateTaxes: true,
            translation: false,
            excludeCartItems: false,
            ignoreCartLoader: true,
            isCheckoutFlow,
          },
        });
      }
    } else {
      yield call(getCartDataSaga, {
        payload: {
          onCartRes: yield call(afterRemovingCartItem, isMoveToSFL),
          recalcRewards: true,
          isRecalculateTaxes: true,
          translation: false,
          excludeCartItems: false,
          ignoreCartLoader: true,
          isCheckoutFlow,
        },
      });
    }
    yield put(setCartLoadingState(false));

    if (isMobileApp() && price) {
      trackForterRemoveActionForApp(payload);
    }
    yield put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isDeleting: false }));
    yield put(BAG_PAGE_ACTIONS.setCartItemsSFL(false));
  } catch (err) {
    const isMinibag = isMiniBag || payload.isMiniBag;
    yield put(setCartLoadingState(false));
    logger.error(err);
    yield call(handleServerSideErrorAPI, err, CARTPAGE_CONSTANTS.CART_ITEM_TILE, isMinibag);
  }
}

export function* removeProductItems() {
  const deleteItem = yield actionChannel(CARTPAGE_CONSTANTS.CONFIRM_REMOVE_CART_ITEM);
  while (true) {
    try {
      const payload = yield take(deleteItem);
      yield call(confirmRemoveItemInChannel, payload);
    } catch (e) {
      logger.error(e);
      break;
    }
  }
}

/**
 *
 * @function removeCartItem to be called when item delete from cart is required it opens the confirmation modal if function is invoked from bag page and item is available
 * @export
 * @param {*} { payload }
 *
 */
export function* removeCartItem({ payload }) {
  const { itemId, pageView, price, isMoveToSFL } = payload;
  if (pageView === 'myBag') {
    const isUnqualifiedItem = yield select(checkoutIfItemIsUnqualified, itemId);
    const isItemInEligible = yield select(isItemBossBopisInEligible, payload);
    const isShowSaveForLaterSwitch = yield select(getSaveForLaterSwitch);
    const isDeleteConfirmationModalEnabled = yield select(getIsDeleteConfirmationModalEnabled);
    const isNewBag = yield select(getIsNewBag);
    if (
      isUnqualifiedItem ||
      isItemInEligible ||
      !isShowSaveForLaterSwitch ||
      !isDeleteConfirmationModalEnabled ||
      isNewBag
    ) {
      if (!isOptimizedDelete) {
        yield call(confirmRemoveItem, { payload: itemId, price, isMoveToSFL });
      } else {
        yield call(confirmRemoveItemInChannel, { payload: itemId, price, isMoveToSFL });
      }
      return;
    }
    yield put(BAG_PAGE_ACTIONS.openItemDeleteConfirmationModal(payload));
  } else if (!isOptimizedDelete) {
    yield call(confirmRemoveItem, {
      payload: itemId,
      isMiniBag: true,
      price,
      isMoveToSFL,
      isCheckoutFlow: true,
    });
  } else {
    yield call(confirmRemoveItemInChannel, {
      payload: itemId,
      isMiniBag: true,
      price,
      isMoveToSFL,
      isCheckoutFlow: true,
    });
  }
}

/**
 *
 * @function updateSagaErrorActions
 * @description decided error actions on basis of result of update item call
 * @param {*} updateActionType
 * @param {*} errorMessage
 */
export function* updateSagaErrorActions(updateActionType, errorMessage, isCartTileQtyChange) {
  if (updateActionType && !isCartTileQtyChange) {
    yield put(AddToPickupError(errorMessage));
  } else {
    yield put(AddToCartError(errorMessage));
  }
}

export function* setUpdateItemErrorMessages(payload, errorMessage) {
  if (payload.fromToggling) {
    yield put(
      setToggleCartItemError({
        errorMessage,
        itemId: payload.apiPayload.orderItem[0].orderItemId,
      })
    );
  } else if (payload.fromTogglingBossBopis && !payload.isCartTileQtyChange) {
    yield put(
      setBossBopisToggleCartItemError({
        errorMessage,
        itemId: payload.apiPayload.orderItem[0].orderItemId,
        targetOrderType: payload.apiPayload.x_updatedItemType,
      })
    );
  } else {
    yield put(AddToPickupError(errorMessage));
  }
}

export function* getFavoriteStoreCall(updateActionType, x_storeLocId) {
  const favStore = yield select(getDefaultStoreData);
  const favStoreId = favStore && favStore.basicInfo && favStore.basicInfo.id;
  if (updateActionType && x_storeLocId !== '' && x_storeLocId !== favStoreId) {
    yield put(
      getFavoriteStoreActn({
        ignoreCache: true,
        isForceUpdate: true,
      })
    );
  }
}

function* setBagPageLoading(isCartTileQtyChange, isMiniBagOpen) {
  if (isCartTileQtyChange || isMiniBagOpen) {
    yield put(setCartLoadingState(true));
    return yield put(BAG_PAGE_ACTIONS.setBagPageLoading());
  }
  return null;
}

// eslint-disable-next-line complexity
export function* updateCartItemSaga({ payload }) {
  const {
    updateActionType,
    isMiniBagOpen,
    fromTogglingBossBopis,
    apiPayload: { x_storeLocId } = {},
    getAllNewMultiPack,
    getAllMultiPack,
    skuInfo,
    partIdInfo,
    isAddedToBag,
    fromPickupBanner,
    isCartTileQtyChange,
    isCartPage,
    multiPackItems,
  } = payload;
  try {
    if (!isMiniBagOpen && !isAddedToBag && !isCartTileQtyChange) {
      yield put(setLoaderState(true));
    }
    yield call(setBagPageLoading, isCartTileQtyChange, isMiniBagOpen);
    yield put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isUpdating: true }));
    yield put(BAG_PAGE_ACTIONS.setCartItemData(payload));
    const multipackCatentryId = partIdInfo
      ? filterItems(getAllNewMultiPack, getAllMultiPack, skuInfo, partIdInfo)
      : multiPackItems || '';
    yield put(CHECKOUT_ACTIONS.setServerErrorCheckout({}));
    yield put(clearAddToBagErrorState());
    yield put(clearAddToPickupErrorState());
    yield put(clearToggleCartItemError());
    yield put(clearToggleBossBopisCartItemError());
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const res = yield call(updateItem, { ...payload, multipackCatentryId }, errorMapping);
    if (fromPickupBanner) {
      yield put(setLoaderState(false));
    }
    const cartOrderItems = yield select(BagPageSelectors.getOrderItems);
    const productsData = BagPageUtils.formatBagProductsData(cartOrderItems);
    yield put(
      setClickAnalyticsData({
        customEvents: isMobileApp() ? ['Cart_Edits_e68'] : ['event68'],
        products: productsData,
        eventName: 'cart update',
        clickEvent: true,
      })
    );
    yield put(
      trackClick({
        name: 'edit_cart',
        module: 'checkout',
      })
    );
    trackForterRemoveActionForApp(payload.itemId);
    const { callBack } = payload;
    yield put(updateCartItemComplete(res));
    yield put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isUpdating: true }));
    /* istanbul ignore else */
    if (callBack) {
      callBack();
    }
    yield call(getCartDataSaga, {
      payload: {
        recalcRewards: true,
        isRecalculateTaxes: true,
        translation: true,
        excludeCartItems: false,
        isCartPage,
      },
    });
    yield put(setLoaderState(false));
    yield call(getFavoriteStoreCall, updateActionType, x_storeLocId);
    yield put(BAG_PAGE_ACTIONS.setCartItemsUpdating({ isUpdating: false }));
    yield put(setCartLoadingState(false));
  } catch (err) {
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const errorMessage =
      // eslint-disable-next-line no-underscore-dangle
      (err && err.errorMessage && err.errorMessage._error) || errorMapping?.DEFAULT || 'ERROR';
    if (!fromTogglingBossBopis || isCartTileQtyChange) {
      yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(errorMessage));
    }
    yield call(updateSagaErrorActions, updateActionType, errorMessage, isCartTileQtyChange);
    yield setUpdateItemErrorMessages(payload, errorMessage);
    yield put(setLoaderState(false));
    yield put(BAG_PAGE_ACTIONS.resetBagLoadingState());
    yield put(setCartLoadingState(false));
  }
}

export function fetchProductId(partId) {
  let multipleProduct = false;
  let newPartId;
  let multiplePartId = '';
  if (partId.includes(',')) {
    multipleProduct = true;
    const arrayOfPartNum = getStringAfterSplit(partId, ',');
    for (let i = 0; i < arrayOfPartNum.length; i += 1) {
      multiplePartId = multiplePartId.concat(getStringAfterSplit(arrayOfPartNum[i], '#')[0]);
      if (i !== arrayOfPartNum.length - 1) {
        multiplePartId = multiplePartId.concat(',');
      }
    }
  } else {
    newPartId = !!partId && [getStringAfterSplit(partId, '#')];
  }
  return multipleProduct ? multiplePartId : newPartId[0][0];
}

export function* getProductSKUInfoSaga(payload) {
  try {
    yield put(setProductMultiPackData({}));
    const xappURLConfig = yield call(getUnbxdXappConfigs);
    const productId = payload.payload.productNum;
    const { itemBrand } = payload.payload;
    const isPdpServiceEnabled = yield select(getIsPdpServiceEnabled);
    const enablePDPServiceABTest = yield select(getABTestPdpService);
    let formattedResponse = yield call(
      getProductSkuInfoByUnbxd,
      productId,
      itemBrand,
      xappURLConfig,
      isPdpServiceEnabled,
      enablePDPServiceABTest
    );
    if (
      !!formattedResponse &&
      !!formattedResponse.product &&
      formattedResponse.product.multiPackUSStore
    ) {
      const partId = formattedResponse.product.tcpMultiPackReferenceUSStore;
      const finalPartId = fetchProductId(partId);
      const multiPackproductId = partId.split(',');
      const productIdCount = multiPackproductId[0].split(',');
      const multiPackCount = getMultipackCount(productIdCount);
      const state = yield select();
      const singleProduct = yield call(
        getProductInfoById,
        xappURLConfig,
        finalPartId,
        state,
        itemBrand,
        null,
        null,
        null,
        true,
        undefined,
        undefined,
        undefined,
        isPdpServiceEnabled,
        enablePDPServiceABTest
      );
      const multiPackInventoryThreshold = yield select(getMultiPackInventoryThreshold);
      formattedResponse = CartItemTileSagaUtils.applyMultiPackInfo(
        formattedResponse,
        singleProduct,
        multiPackCount,
        multiPackInventoryThreshold
      );
      yield put(setProductMultiPackData(formattedResponse));
    }
    yield put(getProductSKUInfoSuccess(formattedResponse));
  } catch (err) {
    logger.error(err);
  }
}

/**
 *
 * @method openPickupModalFromBag
 * @description this method handles opening of pickup modal on click of edit from bag for boss/bopis item
 * @export
 * @param {*} payload
 */
export function* openPickupModalFromBag(payload) {
  try {
    const {
      payload: {
        colorProductId,
        orderInfo,
        openSkuSelectionForm,
        isBopisCtaEnabled,
        isBossCtaEnabled,
        isItemShipToHome,
        alwaysSearchForBOSS,
        openRestrictedModalForBopis,
        isPickUpWarningModal,
        initialSelectedQty,
        newBag = false,
        isBopisPickup,
        isPickupStoreModalRedisgn,
      } = {},
    } = payload;
    if (!newBag) {
      yield put(setLoaderState(true));
    }

    const xappURLConfig = yield call(getUnbxdXappConfigs);
    const state = yield select();

    let itemBrand;
    if (orderInfo) {
      ({ itemBrand } = orderInfo);
    }
    if (newBag) {
      yield put(
        openPickupModalWithValues({
          productLoading: true,
          fromBagPage: true,
          initialValues: { ...orderInfo },
          generalProductId: colorProductId,
        })
      );
    }

    const productDetail = yield call(
      getProductInfoById,
      xappURLConfig,
      colorProductId,
      state,
      itemBrand
    );
    const { product } = productDetail;
    const currentProduct = product;
    const { generalProductId } = currentProduct;
    yield put(
      openPickupModalWithValues({
        generalProductId,
        colorProductId: generalProductId,
        isBopisCtaEnabled,
        isBossCtaEnabled,
        currentProduct,
        fromBagPage: true,
        openSkuSelectionForm,
        initialValues: { ...orderInfo },
        updateCartItemStore: true,
        isItemShipToHome,
        alwaysSearchForBOSS,
        openRestrictedModalForBopis,
        isPickUpWarningModal,
        initialSelectedQty,
        productLoading: false,
        isBopisPickup,
      })
    );
    if (!isPickupStoreModalRedisgn) {
      yield put(setLoaderState(false));
    }
  } catch (err) {
    yield put(setLoaderState(false));
  }
}

export function* CartPageSaga() {
  yield takeLatest(CARTPAGE_CONSTANTS.REMOVE_CART_ITEM, removeCartItem);
  yield takeLatest(CARTPAGE_CONSTANTS.UPDATE_CART_ITEM, updateCartItemSaga);
  yield takeLatest(CARTPAGE_CONSTANTS.GET_PRODUCT_SKU_INFO, getProductSKUInfoSaga);
  yield takeLatest(CARTPAGE_CONSTANTS.PICKUP_MODAL_OPEN_FROM_BAG, openPickupModalFromBag);
  if (isOptimizedDelete) {
    yield* removeProductItems();
  } else {
    yield takeLatest(CARTPAGE_CONSTANTS.CONFIRM_REMOVE_CART_ITEM, confirmRemoveItem);
  }
}

export default CartPageSaga;
