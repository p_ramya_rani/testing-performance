/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { processBreadCrumbs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import {
  getIsBossEnabled,
  getIsBopisEnabled,
  getIsBossClearanceProductEnabled,
  getIsBopisClearanceProductEnabled,
  getIsRadialInventoryEnabled,
  getIsInternationalShipping,
  getIsDynamicBadgeEnabled,
  getIsHideMeatBallMenuAbTest,
  getIsShowEditCTAAbTest,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getIsMiniBagOpen,
  getLabelsCartItemTile,
  getBossState,
  getBossCity,
  getBopisState,
  getBopisCity,
} from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { getOrdersLabels } from '@tcp/core/src/components/features/account/OrderDetails/container/OrderDetails.selectors';
import NavigateXHRSelector from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.selectors';
import { setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import { getCurrencyAttributes } from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import BAG_PAGE_ACTIONS from '../../BagPage/container/BagPage.actions';
import BAGPAGE_SELECTORS from '../../BagPage/container/BagPage.selectors';
import {
  removeCartItem,
  updateCartItem,
  getProductSKUInfo,
  openPickupModalWithValuesFromBag,
  clearToggleCartItemError,
  clearToggleBossBopisCartItemError,
} from './CartItemTile.actions';
import CartItemTile from '../molecules/CartItemTile/views/CartItemTile.view';
import CartItemNewTile from '../molecules/CartItemTile/views/CartItemNewTile.view';
import {
  getEditableProductInfo,
  getCartToggleError,
  getCartBossBopisToggleError,
  isBrierleyWorking,
  getEditableProductMultiPackInfo,
} from './CartItemTile.selectors';
import {
  getSaveForLaterSwitch,
  getSflMaxCount,
} from '../../SaveForLater/container/SaveForLater.selectors';
import {
  getPersonalDataState,
  getUserLoggedInState,
} from '../../../account/User/container/User.selectors';
import {
  openQuickViewWithValues,
  updateAppTypeWithParams,
} from '../../../../common/organisms/QuickViewModal/container/QuickViewModal.actions';
import CARTPAGE_CONSTANTS from '../CartItemTile.constants';
import CONSTANTS from '../../Checkout/Checkout.constants';
import { addItemsToWishlist } from '../../../browse/Favorites/container/Favorites.actions';
import { isMobileApp } from '../../../../../utils';
import { getPickupStoreModalRedsign } from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';

/* eslint-disable no-shadow */
export class CartItemTileContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      generalProductId: '',
      productDataforDelete: null,
      showLoginModal: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isLoggedIn, onAddItemToFavorites, startSflItemDelete } = nextProps;
    const { generalProductId, productDataforDelete } = prevState;

    if (isLoggedIn && generalProductId !== '') {
      onAddItemToFavorites({
        colorProductId: generalProductId,
      });
      startSflItemDelete({ ...productDataforDelete });
      return { generalProductId: '' };
    }
    return null;
  }

  handleAddToWishlist = () => {
    const { productDetail, onAddItemToFavorites, startSflItemDelete, isLoggedIn } = this.props;
    const {
      itemInfo: { isGiftItem },
      productInfo: { skuId, generalProductId },
    } = productDetail;
    const generalProductIdState = generalProductId;
    const catEntryId = isGiftItem ? generalProductId : skuId;
    const payloadData = { catEntryId };
    onAddItemToFavorites({ colorProductId: generalProductId });
    if (isLoggedIn) {
      startSflItemDelete({ ...payloadData });
    } else {
      this.setState({
        generalProductId: generalProductIdState,
        productDataforDelete: payloadData,
        showLoginModal: true,
      });
    }
  };

  toggleLoginModal = () => {
    this.setState((state) => ({
      showLoginModal: !state.showLoginModal,
    }));
  };

  render() {
    const {
      labels,
      productDetail,
      removeCartItem,
      updateCartItem,
      isMiniBagOpen,
      getProductSKUInfo,
      editableProductInfo,
      editableProductMultiPackInfo,
      pageView,
      className,
      isEditAllowed,
      toggleEditAllowance,
      inheritedStyles,
      isPlcc,
      itemIndex,
      openedTile,
      setSelectedProductTile,
      setSwipedElement,
      swipedElement,
      isShowSaveForLater,
      sflMaxCount,
      isGenricGuest,
      addItemToSflList,
      setCartItemsSflError,
      sflItemsCount,
      isBagPageSflSection,
      startSflItemDelete,
      startSflDataMoveToBag,
      onQuickViewOpenClick,
      isBossEnabledTCP,
      isBossEnabledGYM,
      isBopisEnabledTCP,
      isBopisEnabledGYM,
      isBossClearanceProductEnabled,
      isBopisClearanceProductEnabled,
      isRadialInventoryEnabled,
      onPickUpOpenClick,
      orderId,
      setShipToHome,
      toggleError,
      toggleBossBopisError,
      clearToggleError,
      pickupStoresInCart,
      autoSwitchPickupItemInCart,
      navigation,
      updateAppTypeHandler,
      disableProductRedirect,
      setClickAnalyticsData,
      closeMiniBag,
      isLoggedIn,
      cartOrderItems,
      isBrierleyWorking,
      isInternationalShipping,
      labelsCartItemTile,
      cookieToken,
      isMultipackProduct,
      isMiniBag,
      isDynamicBadgeEnabled,
      ordersLabels,
      newBag,
      isCartLoading,
      newMiniBag,
      isLowInventoryBannerEnabled,
      isLowInventoryBannerEnabledForOldDesign,
      bossState,
      bossCity,
      bopisState,
      bopisCity,
      isCartQuantityUpdating,
      isHideMeatBallAbTest,
      isShowEditCTAAbTest,
      isPickupStoreModalRedisgn,
    } = this.props;
    const { showLoginModal } = this.state;
    const isMobileAppCheck = isMobileApp();
    const { breadCrumbs } = isMobileAppCheck ? {} : this.props;
    const { currencyExchange } = isMobileAppCheck ? {} : this.props;
    return newBag ? (
      <CartItemNewTile
        labels={labels}
        productDetail={productDetail}
        removeCartItem={removeCartItem}
        updateCartItem={updateCartItem}
        isMiniBagOpen={isMiniBagOpen}
        getProductSKUInfo={getProductSKUInfo}
        editableProductInfo={editableProductInfo}
        editableProductMultiPackInfo={editableProductMultiPackInfo}
        pageView={pageView}
        className={className}
        toggleEditAllowance={toggleEditAllowance}
        isEditAllowed={isEditAllowed}
        inheritedStyles={inheritedStyles}
        isPlcc={isPlcc}
        itemIndex={itemIndex}
        openedTile={openedTile}
        setSelectedProductTile={setSelectedProductTile}
        setSwipedElement={setSwipedElement}
        swipedElement={swipedElement}
        isShowSaveForLater={isShowSaveForLater}
        sflMaxCount={sflMaxCount}
        isGenricGuest={isGenricGuest}
        addItemToSflList={addItemToSflList}
        setCartItemsSflError={setCartItemsSflError}
        sflItemsCount={sflItemsCount}
        isBagPageSflSection={isBagPageSflSection}
        startSflItemDelete={startSflItemDelete}
        startSflDataMoveToBag={startSflDataMoveToBag}
        onQuickViewOpenClick={onQuickViewOpenClick}
        isBossEnabledTCP={isBossEnabledTCP}
        isBossEnabledGYM={isBossEnabledGYM}
        isBopisEnabledTCP={isBopisEnabledTCP}
        isBopisEnabledGYM={isBopisEnabledGYM}
        isBossClearanceProductEnabled={isBossClearanceProductEnabled}
        isBopisClearanceProductEnabled={isBopisClearanceProductEnabled}
        isRadialInventoryEnabled={isRadialInventoryEnabled}
        onPickUpOpenClick={onPickUpOpenClick}
        orderId={orderId}
        setShipToHome={setShipToHome}
        toggleError={toggleError}
        toggleBossBopisError={toggleBossBopisError}
        clearToggleError={clearToggleError}
        currencyExchange={currencyExchange}
        pickupStoresInCart={pickupStoresInCart}
        autoSwitchPickupItemInCart={autoSwitchPickupItemInCart}
        navigation={navigation}
        updateAppTypeHandler={updateAppTypeHandler}
        disableProductRedirect={disableProductRedirect}
        setClickAnalyticsData={setClickAnalyticsData}
        closeMiniBag={closeMiniBag}
        handleAddToWishlist={this.handleAddToWishlist}
        isLoggedIn={isLoggedIn}
        showLoginModal={showLoginModal}
        toggleLoginModal={this.toggleLoginModal}
        cartOrderItems={cartOrderItems}
        isInternationalShipping={isInternationalShipping}
        labelsCartItemTile={labelsCartItemTile}
        cookieToken={cookieToken}
        isMiniBag={isMiniBag}
        isMultipackProduct={isMultipackProduct}
        isBrierleyWorking={isBrierleyWorking}
        ordersLabels={ordersLabels}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        breadCrumbs={breadCrumbs}
        newBag={newBag}
        isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
        isCartLoading={isCartLoading}
        bossState={bossState}
        bossCity={bossCity}
        bopisState={bopisState}
        bopisCity={bopisCity}
        isCartQuantityUpdating={isCartQuantityUpdating}
        isHideMeatBallAbTest={isHideMeatBallAbTest}
        isShowEditCTAAbTest={isShowEditCTAAbTest}
        isPickupStoreModalRedisgn={isPickupStoreModalRedisgn}
        newMiniBag={newMiniBag}
      />
    ) : (
      <CartItemTile
        labels={labels}
        productDetail={productDetail}
        removeCartItem={removeCartItem}
        updateCartItem={updateCartItem}
        isMiniBagOpen={isMiniBagOpen}
        getProductSKUInfo={getProductSKUInfo}
        editableProductInfo={editableProductInfo}
        editableProductMultiPackInfo={editableProductMultiPackInfo}
        pageView={pageView}
        className={className}
        toggleEditAllowance={toggleEditAllowance}
        isEditAllowed={isEditAllowed}
        inheritedStyles={inheritedStyles}
        isPlcc={isPlcc}
        itemIndex={itemIndex}
        openedTile={openedTile}
        setSelectedProductTile={setSelectedProductTile}
        setSwipedElement={setSwipedElement}
        swipedElement={swipedElement}
        isShowSaveForLater={isShowSaveForLater}
        sflMaxCount={sflMaxCount}
        isGenricGuest={isGenricGuest}
        addItemToSflList={addItemToSflList}
        setCartItemsSflError={setCartItemsSflError}
        sflItemsCount={sflItemsCount}
        isBagPageSflSection={isBagPageSflSection}
        startSflItemDelete={startSflItemDelete}
        startSflDataMoveToBag={startSflDataMoveToBag}
        onQuickViewOpenClick={onQuickViewOpenClick}
        isBossEnabledTCP={isBossEnabledTCP}
        isBossEnabledGYM={isBossEnabledGYM}
        isBopisEnabledTCP={isBopisEnabledTCP}
        isBopisEnabledGYM={isBopisEnabledGYM}
        isBossClearanceProductEnabled={isBossClearanceProductEnabled}
        isBopisClearanceProductEnabled={isBopisClearanceProductEnabled}
        isRadialInventoryEnabled={isRadialInventoryEnabled}
        onPickUpOpenClick={onPickUpOpenClick}
        orderId={orderId}
        setShipToHome={setShipToHome}
        toggleError={toggleError}
        toggleBossBopisError={toggleBossBopisError}
        clearToggleError={clearToggleError}
        currencyExchange={currencyExchange}
        pickupStoresInCart={pickupStoresInCart}
        autoSwitchPickupItemInCart={autoSwitchPickupItemInCart}
        navigation={navigation}
        updateAppTypeHandler={updateAppTypeHandler}
        disableProductRedirect={disableProductRedirect}
        setClickAnalyticsData={setClickAnalyticsData}
        closeMiniBag={closeMiniBag}
        handleAddToWishlist={this.handleAddToWishlist}
        isLoggedIn={isLoggedIn}
        showLoginModal={showLoginModal}
        toggleLoginModal={this.toggleLoginModal}
        cartOrderItems={cartOrderItems}
        isInternationalShipping={isInternationalShipping}
        labelsCartItemTile={labelsCartItemTile}
        cookieToken={cookieToken}
        isMiniBag={isMiniBag}
        isMultipackProduct={isMultipackProduct}
        isBrierleyWorking={isBrierleyWorking}
        ordersLabels={ordersLabels}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        breadCrumbs={breadCrumbs}
        newBag={newBag}
        newMiniBag={newMiniBag}
        isLowInventoryBannerEnabledForOldDesign={isLowInventoryBannerEnabledForOldDesign}
        isPickupStoreModalRedisgn={isPickupStoreModalRedisgn}
      />
    );
  }
}

const createSetShipToHomePayload = (orderItemId, orderItemType, isCartPage) => {
  return {
    apiPayload: {
      orderId: '.',
      orderItem: [
        {
          orderItemId,
        },
      ],
      x_storeLocId: '',
      x_orderitemtype: orderItemType,
      x_updatedItemType: CONSTANTS.ORDER_ITEM_TYPE.ECOM,
    },
    updateActionType: 'UpdatePickUpItem',
    fromToggling: true,
    isCartPage,
  };
};

const createBossBopisTogglePayload = (props, isCartPage) => {
  const { itemId, quantity, skuId, targetOrderType, orderId, store, storeId, isCartTileQtyChange } =
    props;
  const { itemPartNumber, variantNo, orderItemType } = props;
  return {
    apiPayload: {
      orderId: `${orderId}`,
      orderItem: [
        {
          orderItemId: itemId,
          xitem_catEntryId: skuId,
          quantity: `${quantity}`,
          variantNo,
          itemPartNumber,
        },
      ],
      x_storeLocId: storeId,
      x_orderitemtype: !store ? CONSTANTS.ORDER_ITEM_TYPE.ECOM : orderItemType, // source type of Item
      x_updatedItemType: targetOrderType, // target type of Item
    },
    updateActionType: 'UpdatePickUpItem',
    fromTogglingBossBopis: true,
    isCartTileQtyChange,
    isCartPage,
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    getOrderDetails: () => {
      dispatch(BAG_PAGE_ACTIONS.getOrderDetails());
    },
    removeCartItem: (orderItemId) => {
      dispatch(removeCartItem(orderItemId));
    },
    updateCartItem: (
      itemId,
      skuId,
      quantity,
      itemPartNumber,
      variantNo,
      isMiniBagOpen,
      ...args
    ) => {
      const [
        multipackProduct,
        multiPackCount,
        size,
        getAllNewMultiPack,
        getAllMultiPack,
        skuInfo,
        partIdInfo,
        isCartTileQtyChange,
        isCartPage,
        multiPackItems,
      ] = args;
      dispatch(
        updateCartItem({
          itemId,
          skuId,
          quantity,
          itemPartNumber,
          variantNo,
          isMiniBagOpen,
          multipackProduct,
          multiPackCount,
          size,
          getAllNewMultiPack,
          getAllMultiPack,
          skuInfo,
          partIdInfo,
          isCartTileQtyChange,
          multiPackItems,
          isCartPage,
        })
      );
    },
    getProductSKUInfo: (payload) => {
      dispatch(getProductSKUInfo(payload));
    },
    addItemToSflList: (payload, price) => {
      dispatch(BAG_PAGE_ACTIONS.addItemToSflList(payload, price));
    },
    setCartItemsSflError: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.setCartItemsSflError(payload));
    },
    startSflItemDelete: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.startSflItemDelete(payload));
    },
    startSflDataMoveToBag: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.startSflDataMoveToBag(payload));
    },
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    onPickUpOpenClick: (payload) => {
      dispatch(openPickupModalWithValuesFromBag(payload));
    },
    setShipToHome: (orderItemId, orderItemType, isCartPage) => {
      dispatch(updateCartItem(createSetShipToHomePayload(orderItemId, orderItemType, isCartPage)));
    },
    autoSwitchPickupItemInCart: (payload, isCartPage) => {
      dispatch(updateCartItem(createBossBopisTogglePayload(payload, isCartPage)));
    },
    clearToggleError: () => {
      dispatch(clearToggleCartItemError());
      dispatch(clearToggleBossBopisCartItemError());
    },
    updateAppTypeHandler: (payload) => {
      dispatch(updateAppTypeWithParams(payload));
    },
    setClickAnalyticsData: (payload) => {
      dispatch(setClickAnalyticsData(payload));
    },
    onAddItemToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
  };
};

export function mapStateToProps(state) {
  const commonProps = {
    editableProductInfo: getEditableProductInfo(state),
    editableProductMultiPackInfo: getEditableProductMultiPackInfo(state),
    isShowSaveForLater: getSaveForLaterSwitch(state),
    sflMaxCount: parseInt(getSflMaxCount(state), 10),
    isGenricGuest: getPersonalDataState(state),
    isBossEnabledTCP: getIsBossEnabled(state, CARTPAGE_CONSTANTS.BRANDS.TCP),
    isBossEnabledGYM: getIsBossEnabled(state, CARTPAGE_CONSTANTS.BRANDS.GYM),
    isBopisEnabledTCP: getIsBopisEnabled(state, CARTPAGE_CONSTANTS.BRANDS.TCP),
    isBopisEnabledGYM: getIsBopisEnabled(state, CARTPAGE_CONSTANTS.BRANDS.GYM),
    isBossClearanceProductEnabled: getIsBossClearanceProductEnabled(state),
    isBopisClearanceProductEnabled: getIsBopisClearanceProductEnabled(state),
    isRadialInventoryEnabled: getIsRadialInventoryEnabled(state),
    orderId: BAGPAGE_SELECTORS.getCurrentOrderId(state) || '',
    toggleError: getCartToggleError(state),
    toggleBossBopisError: getCartBossBopisToggleError(state),
    pickupStoresInCart: BAGPAGE_SELECTORS.getCartStores(state),
    isMiniBagOpen: getIsMiniBagOpen(state),
    isLoggedIn: getUserLoggedInState(state),
    cartOrderItems: BAGPAGE_SELECTORS.getOrderItems(state),
    isInternationalShipping: getIsInternationalShipping(state),
    labelsCartItemTile: getLabelsCartItemTile(state),
    cookieToken: NavigateXHRSelector.getCookieToken(state),
    isBrierleyWorking: isBrierleyWorking(state),
    ordersLabels: getOrdersLabels(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    isCartLoading: BAGPAGE_SELECTORS.isBagLoading(state),
    bossState: getBossState(state),
    bossCity: getBossCity(state),
    bopisState: getBopisState(state),
    bopisCity: getBopisCity(state),
    isHideMeatBallAbTest: getIsHideMeatBallMenuAbTest(state),
    isShowEditCTAAbTest: getIsShowEditCTAAbTest(state),
    isPickupStoreModalRedisgn: getPickupStoreModalRedsign(state),
  };
  const webProps = {
    breadCrumbs: processBreadCrumbs(state.ProductListing && state.ProductListing.breadCrumbTrail),
    currencyExchange: [getCurrencyAttributes(state)],
  };
  return isMobileApp() ? { ...commonProps } : { ...commonProps, ...webProps };
}

CartItemTileContainer.propTypes = {
  productDetail: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  getProductSKUInfo: PropTypes.func.isRequired,
  updateCartItem: PropTypes.func.isRequired,
  editableProductInfo: PropTypes.shape({}).isRequired,
  editableProductMultiPackInfo: PropTypes.shape({}),
  removeCartItem: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  isPlcc: PropTypes.string.isRequired,
  pageView: PropTypes.string,
  toggleEditAllowance: PropTypes.func.isRequired,
  isEditAllowed: PropTypes.bool,
  isShowSaveForLater: PropTypes.bool.isRequired,
  isGenricGuest: PropTypes.shape({}).isRequired,
  sflItemsCount: PropTypes.number,
  sflMaxCount: PropTypes.number.isRequired,
  addItemToSflList: PropTypes.func.isRequired,
  setCartItemsSflError: PropTypes.func.isRequired,
  isBagPageSflSection: PropTypes.bool,
  startSflItemDelete: PropTypes.func.isRequired,
  startSflDataMoveToBag: PropTypes.func.isRequired,
  onPickUpOpenClick: PropTypes.func.isRequired,
  orderId: PropTypes.number.isRequired,
  setShipToHome: PropTypes.func.isRequired,
  toggleError: PropTypes.shape({}),
  toggleBossBopisError: PropTypes.shape({
    errorMessage: PropTypes.string,
  }),
  clearToggleError: PropTypes.func.isRequired,
  currencyExchange: PropTypes.shape([]),
  pickupStoresInCart: PropTypes.shape({}).isRequired,
  autoSwitchPickupItemInCart: PropTypes.func.isRequired,
  disableProductRedirect: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func.isRequired,
  closeMiniBag: PropTypes.func,
  inheritedStyles: PropTypes.string,
  itemIndex: PropTypes.number,
  openedTile: PropTypes.number,
  setSelectedProductTile: PropTypes.func.isRequired,
  swipedElement: PropTypes.shape({}).isRequired,
  updateAppTypeHandler: PropTypes.func.isRequired,
  onQuickViewOpenClick: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}),
  setSwipedElement: PropTypes.func,
  isBopisClearanceProductEnabled: PropTypes.bool.isRequired,
  isBossClearanceProductEnabled: PropTypes.bool.isRequired,
  isBossEnabledTCP: PropTypes.bool.isRequired,
  isBossEnabledGYM: PropTypes.bool.isRequired,
  isBopisEnabledTCP: PropTypes.bool.isRequired,
  isBopisEnabledGYM: PropTypes.bool.isRequired,
  isRadialInventoryEnabled: PropTypes.bool.isRequired,
  isMiniBagOpen: PropTypes.bool.isRequired,
  onAddItemToFavorites: PropTypes.func.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  isBrierleyWorking: PropTypes.bool,
  cartOrderItems: PropTypes.shape([]).isRequired,
  labelsCartItemTile: PropTypes.shape({}),
  isMultipackProduct: PropTypes.bool,
  cookieToken: PropTypes.string,
  isMiniBag: PropTypes.bool,
  ordersLabels: PropTypes.shape({}),
  isDynamicBadgeEnabled: PropTypes.shape({}),
  breadCrumbs: PropTypes.shape([]),
  newBag: PropTypes.bool,
  isLowInventoryBannerEnabled: PropTypes.bool,
  isLowInventoryBannerEnabledForOldDesign: PropTypes.bool,
  bossState: PropTypes.string,
  bossCity: PropTypes.string,
  bopisState: PropTypes.string,
  bopisCity: PropTypes.string,
  isPickupStoreModalRedisgn: PropTypes.bool,
};

CartItemTileContainer.defaultProps = {
  inheritedStyles: '',
  itemIndex: 0,
  openedTile: 0,
  setSwipedElement: () => {},
  closeMiniBag: () => {},
  navigation: {},
  pageView: '',
  isEditAllowed: true,
  sflItemsCount: 0,
  isBagPageSflSection: false,
  toggleError: null,
  toggleBossBopisError: null,
  currencyExchange: null,
  disableProductRedirect: false,
  labelsCartItemTile: {},
  isBrierleyWorking: true,
  cookieToken: null,
  isMiniBag: false,
  isMultipackProduct: false,
  ordersLabels: {},
  editableProductMultiPackInfo: {},
  isDynamicBadgeEnabled: false,
  breadCrumbs: null,
  newBag: false,
  isLowInventoryBannerEnabled: false,
  isLowInventoryBannerEnabledForOldDesign: false,
  bossState: '',
  bossCity: '',
  bopisState: '',
  bopisCity: '',
  isPickupStoreModalRedisgn: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(CartItemTileContainer);
