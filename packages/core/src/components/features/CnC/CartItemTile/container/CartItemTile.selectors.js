/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { getLabelValue, isMobileApp, isUpdateRequired } from '@tcp/core/src/utils';
import { SESSIONCONFIG_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { List } from 'immutable';
import BAGPAGE_CONSTANTS from '@tcp/core/src/components/features/CnC/BagPage/BagPage.constants';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { createSelector } from 'reselect';
import CARTPAGE_CONSTANTS from '../CartItemTile.constants';

export const getCookieToken = (state) => state.NavigateXHR.getIn(['cookie_token', 'encodedcookie']);

export const getCartOrderList = (state) => {
  // needs to do it with get method.
  return state.CartPageReducer.getIn(['orderDetails', 'orderItems']);
};
export const getTotalItemCount = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'totalItems']);
};

export const getEditableProductInfo = (state) => {
  // needs to do it with get method.
  return state.CartItemTileReducer.getIn(['editableItemData', 'colorFitsSizesMap']);
};

export const getEditableProductMultiPackInfo = (state) => {
  return state.CartItemTileReducer.getIn(['editableItemMultiPackData']);
};

export const getCartOrderDetails = (state) => {
  return state.CartPageReducer.get('orderDetails');
};

export const getBossState = (state) => {
  return getCartOrderDetails(state).get('bossState') || '';
};

export const getBossCity = (state) => {
  return getCartOrderDetails(state).get('bossCity') || '';
};

export const getBopisState = (state) => {
  return getCartOrderDetails(state).get('bopisState') || '';
};

export const getBopisCity = (state) => {
  return getCartOrderDetails(state).get('bopisCity') || '';
};

export const isBrierleyWorking = (state) => {
  return getCartOrderDetails(state).get('isBrierleyWorking') || false;
};

export const getIsMiniBagOpen = (state) => {
  return state.Header.miniBag;
};

export const getCartOrderId = (state) => {
  return getCartOrderDetails(state).get('orderId');
};

export const getProductName = (product) => {
  return product.getIn(['productInfo', 'name']);
};

export const checkForGiftItem = (product) => {
  return product.getIn(['productInfo', 'isGiftCard']);
};

export const getProductFit = (product) => {
  return product.getIn(['productInfo', 'fit']);
};

export const getProductColor = (product) => {
  return product.getIn(['productInfo', 'color', 'name']) || product.getIn(['productInfo', 'color']);
};

export const getProductSize = (product) => {
  return product.getIn(['productInfo', 'size']);
};

export const getProductOfferPrice = (product) => {
  return product.getIn(['itemInfo', 'offerPrice']);
};

export const getProductQty = (product) => {
  return product.getIn(['itemInfo', 'quantity']);
};

export const getProductPoints = (product) => {
  return product.getIn(['itemInfo', 'itemPoints']);
};

export const getProductBrand = (product) => {
  return product.getIn(['productInfo', 'itemBrand']);
};

export const getProductImage = (product) => {
  return product.getIn(['productInfo', 'imagePath']);
};

export const getOrderItemId = (product) => {
  return product.getIn(['itemInfo', 'itemId']);
};

export const getProductPartNumber = (product) => {
  return product.getIn(['productInfo', 'productPartNumber']);
};

export const getProductItemPartNumber = (product) => {
  return product.getIn(['productInfo', 'itemPartNumber']);
};

export const getVariantNumber = (product) => {
  return product.getIn(['productInfo', 'variantNo']);
};

export const getProductBadge = (product) => {
  return product.getIn(['miscInfo', 'badge', 'defaultBadge']);
};

export const getProductStore = (product) => {
  return product.getIn(['miscInfo', 'store']);
};

export const getProductStoreAddress = (product) => {
  return product.getIn(['miscInfo', 'storeAddress']);
};

export const getProductOrderItemType = (product) => {
  return product.getIn(['miscInfo', 'orderItemType']);
};

export const getBossStartDate = (product) => {
  return product.getIn(['miscInfo', 'bossStartDate']);
};

export const getBossEndDate = (product) => {
  return product.getIn(['miscInfo', 'bossEndDate']);
};

export const getProductAvailability = (product) => {
  return product.getIn(['miscInfo', 'availability']);
};

export const getIsBossEligible = (product) => {
  return product.getIn(['miscInfo', 'isBossEligible']);
};

export const getIsBopisEligible = (product) => {
  return product.getIn(['miscInfo', 'isBopisEligible']);
};

export const getIsOnlineOnly = (product) => {
  return product.getIn(['miscInfo', 'isOnlineOnly']);
};

export const getClearanceItem = (product) => {
  return product.getIn(['miscInfo', 'clearanceItem']);
};

export const getIsInventoryAvailBOSS = (product) => {
  return product.getIn(['miscInfo', 'isInventoryAvailBOSS']);
};

export const getInventoryAvailBOSS = (product) => {
  return product.getIn(['miscInfo', 'inventoryAvailBOSS']);
};

export const getInventoryAvailBOPIS = (product) => {
  return product.getIn(['miscInfo', 'inventoryAvailBOPIS']);
};

export const getInventoryAvailSFL = (product) => {
  return product.getIn(['miscInfo', 'sflAvailInventory']);
};

export const getinventoryAvailECOM = (product) => {
  return product.getIn(['miscInfo', 'inventoryAvail']);
};

export const getIsStoreBOSSEligible = (product) => {
  return product.getIn(['miscInfo', 'isStoreBOSSEligible']);
};

export const getStoreTodayOpenRange = (product) => {
  return product.getIn(['miscInfo', 'storeTodayOpenRange']);
};

export const getIsMultiPack = (product) => product.getIn(['productInfo', 'multiPack']);

export const getMultiPackItems = (product) => product.getIn(['productInfo', 'multiPackItems']);

export const getMultiPackSKUs = (product) => product.getIn(['productInfo', 'multiPackSKUs']);

export const getTcpStyleQty = (product) => product.getIn(['productInfo', 'tcpStyleQty']);
export const getTcpStyleType = (product) => product.getIn(['productInfo', 'tcpStyleType']);

export const getStoreTomorrowOpenRange = (product) => {
  return product.getIn(['miscInfo', 'storeTomorrowOpenRange']);
};

export const getStorePhoneNumber = (product) => product.getIn(['miscInfo', 'storePhoneNumber']);

export const getVendorColorDisplayId = (product) => {
  return product.getIn(['miscInfo', 'vendorColorDisplayId']);
};

export const getProductItemUpcNumber = (product) => {
  return product.getIn(['productInfo', 'upc']);
};

export const getGeneralProdId = (product) => {
  return product.getIn(['productInfo', 'generalProductId']);
};

export const getProductSkuId = (product) => {
  return product.getIn(['productInfo', 'skuId']);
};

export const getPdpUrl = (product) => {
  return product.getIn(['productInfo', 'pdpUrl']);
};

export const getProductItemId = (product) => {
  return product.getIn(['itemInfo', 'itemId']);
};

export const getListPrice = (product) => {
  return product.getIn(['itemInfo', 'listPrice']);
};

export const getOfferPrice = (product) => {
  return product.getIn(['itemInfo', 'offerPrice']);
};

export const getWasPrice = (product) => {
  return product.getIn(['itemInfo', 'wasPrice']);
};

export const getSalePrice = (product) => {
  return product.getIn(['itemInfo', 'salePrice']);
};

export const getProductItemField = (product, objectName, fieldName) => {
  return product.getIn([objectName, fieldName]);
};

// export const getProductItemUnitOfferPrice = product => {
//   return product.getIn(['itemInfo', 'unitOfferPrice']);
// };

// export const getProductItemPrice = product => {
//   return product.getIn(['itemInfo', 'listPrice']);
// };

// export const getProductItemUnitPrice = product => {
//   return product.getIn(['itemInfo', 'listUnitPrice']);
// };

export const getIsCartItemsUpdating = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'isCartItemsUpdating']);
};

export const getIsCartItemsSFL = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'isItemMovedToSflList']);
};

export const getIsSflItemRemoved = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'isSflItemDeleted']);
};

export const getIsSflItemMovedToBag = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'isSflItemMovedToBag']);
};

export const getCartItemsSflError = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'cartItemSflError']);
};

export const getMprPointsMultiplier = (product) => {
  return product.getIn(['miscInfo', 'itemMprPointsMultiplier']);
};

export const getPlccPointsMultiplier = (product) => {
  return product.getIn(['miscInfo', 'itemPlccPointsMultiplier']);
};

export const getUpdateMiniBagFlag = (state) => {
  const isUpdateMiniBag = state.CartPageReducer.getIn(['uiFlags', 'updateMiniBag']);
  if (!isUpdateMiniBag && !isMobileApp()) {
    // Xapp config minibag getOrderDetails timeout in minutes
    const minibagCacheTimeout =
      (state.session &&
        state.session.siteDetails &&
        state.session.siteDetails.MINIBAG_UPDATE_TIMEOUT) ||
      10;
    const lastUpdatedTimestamp = getLocalStorage(BAGPAGE_CONSTANTS.UPDATE_MINIBAG_TIMESTAMP);
    return isUpdateRequired(lastUpdatedTimestamp, minibagCacheTimeout);
  }
  return isUpdateMiniBag;
};

export const getPageName = (state) => {
  const { pageData = {} } = state;
  return pageData.pageName || '';
};

const allLabels = (state) => state.Labels;

export const getLabelsCartItemTile = createSelector(allLabels, (labels) => {
  const saveForLaterLink = getLabelValue(labels, 'lbl_sfl_actionLink', 'bagPage', 'checkout');
  const moveToBagLink = getLabelValue(labels, 'lbl_sfl_moveToBag', 'bagPage', 'checkout');
  const sflMaxLimitError = getLabelValue(labels, 'lbl_sfl_maxLimitError', 'bagPage', 'checkout');
  const sflSuccess = getLabelValue(labels, 'bl_sfl_actionSuccess', 'bagPage', 'checkout');
  const freeShippingCartTile = getLabelValue(
    labels,
    'lbl_freeShipping_cartTile',
    'bagPage',
    'checkout'
  );
  const needASAPCartTile = getLabelValue(labels, 'lbl_NeedASAP_cartTile', 'bagPage', 'checkout');
  const getItToday = getLabelValue(labels, 'lbl_get_it_today', 'cartItemTile', 'global');
  const notAvailable = getLabelValue(labels, 'lbl_not_available', 'cartItemTile', 'global');
  const bossEddPlaceHolder = getLabelValue(
    labels,
    'lbl_edd_placeholdeer_boss',
    'cartItemTile',
    'global'
  );
  const ptsCartTile = getLabelValue(labels, 'lbl_Pts_cartTile', 'bagPage', 'checkout');
  const qtyCartTile = getLabelValue(labels, 'lbl_Qty_cartTile', 'bagPage', 'checkout');
  const lowInventoryCartTileTxt = getLabelValue(
    labels,
    'lbl_low_invetory_cart_tile',
    'bagPage',
    'checkout'
  );
  const lowInventoryCartTileShortTxt = getLabelValue(
    labels,
    'lbl_low_invetory_cart_tile_short',
    'bagPage',
    'checkout'
  );
  const lblLowInvetoryThresholdShipToHome = getLabelValue(
    labels,
    'lbl_low_invetory_threshold_ship_to_home',
    'bagPage',
    'checkout'
  );
  const lblLowInvetoryThresholdBOSS = getLabelValue(
    labels,
    'lbl_low_invetory_threshold_BOSS',
    'bagPage',
    'checkout'
  );
  const lblLowInvetoryThresholdBOPIS = getLabelValue(
    labels,
    'lbl_low_invetory_threshold_BOPIS',
    'bagPage',
    'checkout'
  );
  const lblLowInvetoryThresholdSFL = getLabelValue(
    labels,
    'lbl_low_invetory_threshold_SFL',
    'bagPage',
    'checkout'
  );
  const sflDeleteSuccess = getLabelValue(
    labels,
    'lbl_sfl_itemDeleteSuccess',
    'bagPage',
    'checkout'
  );
  const itemDeleted = getLabelValue(labels, 'lbl_msg_itemDeleteSuccess', 'bagPage', 'checkout');
  return {
    color: getLabelValue(labels, 'lbl_info_color', 'addedToBagModal', 'global'),
    size: getLabelValue(labels, 'lbl_info_size', 'addedToBagModal', 'global'),
    qty: getLabelValue(labels, 'lbl_info_Qty', 'addedToBagModal', 'global'),
    price: getLabelValue(labels, 'lbl_info_price', 'addedToBagModal', 'global'),
    design: getLabelValue(labels, 'lbl_info_giftDesign', 'addedToBagModal', 'global'),
    value: getLabelValue(labels, 'lbl_info_giftValue', 'addedToBagModal', 'global'),
    fit: getLabelValue(labels, 'lbl_cartTile_fit', 'cartItemTile', 'global'),
    points: getLabelValue(labels, 'lbl_cartTile_points', 'cartItemTile', 'global'),
    cancel: getLabelValue(labels, 'lbl_cartTile_cancel', 'cartItemTile', 'global'),
    edit: getLabelValue(labels, 'lbl_cartTile_edit', 'cartItemTile', 'global'),
    update: getLabelValue(labels, 'lbl_cartTile_update', 'cartItemTile', 'global'),
    removeEdit: getLabelValue(labels, 'lbl_cartTile_remove', 'cartItemTile', 'global'),
    saveForLater: getLabelValue(labels, 'lbl_cartTile_saveForLater', 'cartItemTile', 'global'),
    productBandAlt: getLabelValue(labels, 'lbl_cartTile_productBrandAlt', 'cartItemTile', 'global'),
    tcpBrandAlt: getLabelValue(labels, 'lbl_brandlogo_tcpAltText', 'accessibility', 'global'),
    gymBrandAlt: getLabelValue(labels, 'lbl_brandlogo_gymAltText', 'accessibility', 'global'),
    snjBrandAlt: getLabelValue(labels, 'lbl_brandlogo_snjAltText', 'accessibility', 'global'),
    productImageAlt: getLabelValue(
      labels,
      'lbl_cartTile_productImageAlt',
      'cartItemTile',
      'global'
    ),
    bopisLabel: getLabelValue(labels, 'lbl_cartTile_bopis', 'cartItemTile', 'global'),
    bossLabel: getLabelValue(labels, 'lbl_cartTile_boss', 'cartItemTile', 'global'),
    bossPickUp: getLabelValue(labels, 'lbl_cartTile_noRushPickup', 'cartItemTile', 'global'),
    noRushPickUp: getLabelValue(labels, 'lbl_cartTile_boss_pickup', 'cartItemTile', 'global'),
    bopisPickUp: getLabelValue(labels, 'lbl_cartTile_pickUpToday', 'cartItemTile', 'global'),
    bopisPickUpLabel: getLabelValue(labels, 'lbl_cartTile_bopis_pickUp', 'cartItemTile', 'global'),
    ecomShipping: getLabelValue(labels, 'lbl_cartTile_shipToHome', 'cartItemTile', 'global'),
    extra: getLabelValue(labels, 'lbl_cartTile_extra', 'cartItemTile', 'global'),
    off: getLabelValue(labels, 'lbl_cartTile_off', 'cartItemTile', 'global'),
    problemWithOrder: getLabelValue(labels, 'lbl_cartTile_remove', 'cartItemTile', 'global'),
    // pleaseText,
    // remove,
    removeSoldOut: getLabelValue(labels, 'lbl_miniBag_error', 'minibag', 'global'),
    itemUnavailable: getLabelValue(labels, 'lbl_miniBag_itemUnavailable', 'minibag', 'global'),
    itemSoldOut: getLabelValue(labels, 'lbl_miniBag_itemSoldOut', 'minibag', 'global'),
    chooseDiff: getLabelValue(labels, 'lbl_miniBag_chooseDiff', 'minibag', 'global'),
    soldOut: getLabelValue(labels, 'lbl_miniBag_soldOut', 'minibag', 'global'),
    errorSize: getLabelValue(labels, 'lbl_minibag_errorSize', 'minibag', 'global'),
    updateUnavailable: getLabelValue(
      labels,
      'lbl_minibag_errorUpdateUnavailable',
      'minibag',
      'global'
    ),
    removeSoldoutHeader: getLabelValue(
      labels,
      'lbl_minibag_errorRemoveSoldoutHeader',
      'minibag',
      'global'
    ),
    deleteItem: getLabelValue(labels, 'lbl_cartTile_delete', 'cartItemTile', 'global'),
    saveForLaterLink,
    sflMaxLimitError,
    moveToBagLink,
    itemDeleted,
    sflSuccess,
    freeShippingCartTile,
    needASAPCartTile,
    getItToday,
    notAvailable,
    bossEddPlaceHolder,
    ptsCartTile,
    qtyCartTile,
    lowInventoryCartTileTxt,
    lowInventoryCartTileShortTxt,
    lblLowInvetoryThresholdShipToHome,
    lblLowInvetoryThresholdBOSS,
    lblLowInvetoryThresholdBOPIS,
    lblLowInvetoryThresholdSFL,
    today: getLabelValue(labels, 'lbl_cartTile_today', 'cartItemTile', 'global'),
    tomorrow: getLabelValue(labels, 'lbl_cartTile_tomorrow', 'cartItemTile', 'global'),
    phone: getLabelValue(labels, 'lbl_cartTile_phone', 'cartItemTile', 'global'),
    pickup: getLabelValue(labels, 'lbl_cartTile_pickup', 'cartItemTile', 'global'),
    at: getLabelValue(labels, 'lbl_cartTile_pickup', 'cartItemTile', 'global'),
    by: getLabelValue(labels, 'lbl_cartTile_at', 'cartItemTile', 'global'),
    shipping: getLabelValue(labels, 'lbl_cartTile_shipping', 'cartItemTile', 'global'),
    sflDeleteSuccess,
    removeError: getLabelValue(labels, 'lbl_minibag_errorRemove', 'minibag', 'global'),
    itemUpdated: getLabelValue(labels, 'lbl_minibag_itemUpdated', 'minibag', 'global'),
    bossUnavailable: getLabelValue(
      labels,
      'lbl_cartTile_bossUnavailable',
      'cartItemTile',
      'global'
    ),
    bossReqQtyUnavailable: getLabelValue(
      labels,
      'lbl_cartTile_bossReqQtyUnavailable',
      'cartItemTile',
      'global'
    ),
    bopisUnavailable: getLabelValue(
      labels,
      'lbl_cartTile_bopisUnavailable',
      'cartItemTile',
      'global'
    ),
    ecomUnavailable: getLabelValue(
      labels,
      'lbl_cartTile_ecomUnavailable',
      'cartItemTile',
      'global'
    ),
    notAvailableOnlineOnly: getLabelValue(
      labels,
      'lbl_cartTile_notAvailableOnlineOnly',
      'cartItemTile',
      'global'
    ),
    notAvailableClearanceItem: getLabelValue(
      labels,
      'lbl_cartTile_notAvailableClearanceItem',
      'cartItemTile',
      'global'
    ),
    soldOutError: getLabelValue(labels, 'lbl_cartTile_soldOut', 'cartItemTile', 'global'),
    bossInEligible: getLabelValue(labels, 'lbl_cartTile_bossInEligible', 'cartItemTile', 'global'),
    changeStore: getLabelValue(labels, 'lbl_cartTile_changeStore', 'cartItemTile', 'global'),
    infoIconText: getLabelValue(labels, 'lbl_info_icon', 'accessibility', 'global'),
    heartIconText: getLabelValue(labels, 'favoriteIconButton', 'accessibility', 'global'),
    sflIconText: getLabelValue(labels, 'lbl_sfl_icon', 'accessibility', 'global'),
    previousPrice: getLabelValue(labels, 'lbl_previous_price', 'accessibility', 'global'),
    bagIconText: getLabelValue(labels, 'cartIconButton', 'accessibility', 'global'),
    itemNumberLabel: getLabelValue(labels, 'lbl_orderDetails_itemnumber', 'orders', 'account'),
    editItem: getLabelValue(labels, 'lbl_edit_item', 'QuickView', 'Browse'),
    qtyLabel: getLabelValue(labels, 'lbl_qty_text', 'bagPage', 'checkout'),
  };
});

export const getProductDetails = (tile) => {
  return {
    itemInfo: {
      name: getProductName(tile),
      isGiftItem: checkForGiftItem(tile),
      fit: getProductFit(tile),
      color: getProductColor(tile),
      size: getProductSize(tile),
      price: getProductOfferPrice(tile),
      qty: getProductQty(tile),
      myPlacePoints: getProductPoints(tile),
      itemBrand: getProductBrand(tile),
      imagePath: getProductImage(tile),
      itemId: getOrderItemId(tile),
      listPrice: getListPrice(tile),
      offerPrice: getOfferPrice(tile),
      wasPrice: getWasPrice(tile),
      salePrice: getSalePrice(tile),
      itemUnitDstPrice: getProductItemField(tile, 'itemInfo', 'itemUnitDstPrice'),
      itemUnitPrice: getProductItemField(tile, 'itemInfo', 'itemUnitPrice'),
      // itemPrice: getProductItemPrice(tile),
      // unitOfferPrice: getProductItemUnitOfferPrice(tile),
      // itemUnitPrice: getProductItemUnitPrice(tile),
    },
    productInfo: {
      productPartNumber: getProductPartNumber(tile),
      itemPartNumber: getProductItemPartNumber(tile),
      variantNo: getVariantNumber(tile),
      upc: getProductItemUpcNumber(tile),
      generalProductId: getGeneralProdId(tile),
      skuId: getProductSkuId(tile),
      pdpUrl: getPdpUrl(tile),
      multipack: getIsMultiPack(tile),
      multiPackItems: getMultiPackItems(tile),
      tcpStyleQty: getTcpStyleQty(tile),
      tcpStyleType: getTcpStyleType(tile),
    },
    miscInfo: {
      badge: getProductBadge(tile),
      store: getProductStore(tile),
      storeAddress: getProductStoreAddress(tile),
      orderItemType: getProductOrderItemType(tile),
      bossStartDate: getBossStartDate(tile),
      bossEndDate: getBossEndDate(tile),
      availability: getProductAvailability(tile),
      isBossEligible: getIsBossEligible(tile),
      isBopisEligible: getIsBopisEligible(tile),
      isOnlineOnly: getIsOnlineOnly(tile),
      clearanceItem: getClearanceItem(tile),
      isInventoryAvailBOSS: getIsInventoryAvailBOSS(tile),
      isStoreBOSSEligible: getIsStoreBOSSEligible(tile),
      storeTodayOpenRange: getStoreTodayOpenRange(tile),
      storeTomorrowOpenRange: getStoreTomorrowOpenRange(tile),
      storePhoneNumber: getStorePhoneNumber(tile),
      vendorColorDisplayId: getVendorColorDisplayId(tile),
      itemMprPointsMultiplier: getMprPointsMultiplier(tile),
      itemPlccPointsMultiplier: getPlccPointsMultiplier(tile),
      inventoryAvailECOM: getinventoryAvailECOM(tile),
      inventoryAvailBOSS: getInventoryAvailBOSS(tile),
      inventoryAvailBOPIS: getInventoryAvailBOPIS(tile),
      sflAvailInventory: getInventoryAvailSFL(tile),
    },
    multiPackSKUs: getMultiPackSKUs(tile),
  };
};

export const getBossBopisFlags = (state) => {
  return {
    isBOSSEnabled_TCP: state.session.siteDetails.isBOSSEnabled_TCP,
    isBOPISEnabled_TCP: state.session.siteDetails.isBOPISEnabled_TCP,
    isBOPISEnabled_GYM: state.session.siteDetails.isBOPISEnabled_GYM,
    isBOSSEnabled_GYM: state.session.siteDetails.isBOSSEnabled_GYM,
  };
};

export const isItemBossBopisInEligible = (state, { itemBrand, orderItemType } = {}) => {
  const bossBopisFlags = getBossBopisFlags(state);
  const flagName = `is${orderItemType}Enabled_${itemBrand}`;
  return (
    (orderItemType === CARTPAGE_CONSTANTS.BOSS || orderItemType === CARTPAGE_CONSTANTS.BOPIS) &&
    !bossBopisFlags[flagName]
  );
};

export const getCartToggleError = (state) => {
  return state.CartItemTileReducer.get('toggleError');
};

export const getCartBossBopisToggleError = (state) => {
  return state.CartItemTileReducer.get('toggleBossBopisError');
};

export const getIsDeleteConfirmationModalEnabled = (state) => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.IS_DELETE_CONFIRMATION_MODAL_ENABLED === 'TRUE'
  );
};

export const getMultiPackInventoryThreshold = (state) => {
  return state[SESSIONCONFIG_REDUCER_KEY] &&
    state[SESSIONCONFIG_REDUCER_KEY].siteDetails.MULTIPACK_INVENTORY_THRESHOLD
    ? state[SESSIONCONFIG_REDUCER_KEY].siteDetails.MULTIPACK_INVENTORY_THRESHOLD * 1
    : null;
};

const updateSkuList = (productsData, prodData, orderItemType, newBag) => {
  if (orderItemType === CARTPAGE_CONSTANTS.ECOM || newBag) {
    productsData.push(prodData);
  }
};

export const getEcomSkuParams = (cartOrderItems, newBag) => {
  const productsData = [];

  if (cartOrderItems) {
    cartOrderItems.forEach((tile) => {
      const productDetail = getProductDetails(tile);
      const {
        itemInfo: { qty, itemId },
        productInfo: { itemPartNumber },
        miscInfo: { orderItemType },
        multiPackSKUs,
      } = productDetail;

      if (List.isList(multiPackSKUs) && multiPackSKUs.size > 0) {
        multiPackSKUs.forEach((skuitem) => {
          const mpackPartNumber = skuitem.get('partnumber');
          const mpackQty = skuitem.get('quantity');
          const prodData = {
            skuId: mpackPartNumber,
            skuQty: qty * mpackQty,
            orderItemId: itemId,
            parentSkuId: itemPartNumber,
          };
          updateSkuList(productsData, prodData, orderItemType, newBag);
        });
      } else {
        const prodData = {
          skuId: itemPartNumber,
          skuQty: qty,
          orderItemId: itemId,
        };
        updateSkuList(productsData, prodData, orderItemType, newBag);
      }
    });
  }
  return productsData;
};

export const getCartLoadingState = (state) => {
  return state.CartItemTileReducer.get('cartLoadingState');
};

export const getItemUpdatingState = (state) => {
  return state.CartItemTileReducer.get('cartItemUpdating');
};
