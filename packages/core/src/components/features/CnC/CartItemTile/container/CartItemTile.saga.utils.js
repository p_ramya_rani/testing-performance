// 9fbef606107a605d69c0edbcd8029e5d 
const applyMultiPackInfo = (
  response,
  singleProduct,
  multiPackCount,
  multiPackInventoryThreshold
) => {
  const {
    product: {
      categoryIdList,
      colorFitsSizesMap: colorFitsSizesMapMultiPack,
      getMultiPackAllColor,
    } = {},
  } = singleProduct || {};
  const mappingObj = {};
  if (colorFitsSizesMapMultiPack && colorFitsSizesMapMultiPack.length > 0) {
    colorFitsSizesMapMultiPack.forEach(item => {
      const Color = item.color.name;
      mappingObj[Color] = {};
      const { fits, hasFits } = item;
      fits.forEach(fit => {
        const fitName = hasFits ? fit.name : 'noFit';
        mappingObj[Color][fitName] = {};
        fit.sizes.forEach(size => {
          const sizename = size.sizeName.replace(' ', '_');
          mappingObj[Color][fitName][sizename] = size.maxAvailable;
        });
      });
    });
  }
  response.product.colorFitsSizesMap = response.product.colorFitsSizesMap.map(sizeMap => {
    const sizeMapObj = sizeMap;
    const colorName = sizeMapObj.color.name;
    sizeMapObj.fits = sizeMapObj.fits.map(fit => {
      const fitObj = fit;
      const fitName = sizeMapObj.hasFits ? fitObj.fitName : 'noFit';
      fitObj.sizes = fit.sizes.map(size => {
        const sizeObj = size;
        const { sizeName } = sizeObj;
        sizeObj.v_qty =
          mappingObj[colorName] && mappingObj[colorName][fitName]
            ? mappingObj[colorName][fitName][sizeName.replace(' ', '_')]
            : sizeObj.v_qty;
        return sizeObj;
      });
      return fitObj;
    });
    return sizeMapObj;
  });
  // attach multipackInfo
  response.product.multiPackInfo = {
    newPartNumber: categoryIdList,
    multiPackCount,
    multiPackInventoryThreshold,
    getAllMultiPack: response.product.getMultiPackAllColor,
    getAllNewMultiPack: getMultiPackAllColor,
  };
  return response;
};

export default { applyMultiPackInfo };
