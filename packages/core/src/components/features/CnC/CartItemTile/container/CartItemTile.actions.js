// 9fbef606107a605d69c0edbcd8029e5d
/**
 * These are temporary changes for a dummy Bag page
 */

import CARTPAGE_CONSTANTS from '../CartItemTile.constants';

export const removeCartItem = (payload) => {
  return {
    type: CARTPAGE_CONSTANTS.REMOVE_CART_ITEM,
    payload,
  };
};

export const removeCartItemComplete = () => {
  return {
    type: CARTPAGE_CONSTANTS.REMOVE_CART_ITEM_COMPLETE,
  };
};

export const openPickupModalWithValuesFromBag = (payload) => {
  return {
    payload,
    type: CARTPAGE_CONSTANTS.PICKUP_MODAL_OPEN_FROM_BAG,
  };
};

export const confirmRemoveCartItem = (payload, afterHandler, price) => {
  return {
    type: CARTPAGE_CONSTANTS.CONFIRM_REMOVE_CART_ITEM,
    payload,
    afterHandler,
    price,
  };
};

export const getProductSKUInfo = (payload) => {
  return {
    type: CARTPAGE_CONSTANTS.GET_PRODUCT_SKU_INFO,
    payload,
  };
};

export const getProductSKUInfoSuccess = (payload) => {
  return {
    type: CARTPAGE_CONSTANTS.GET_PRODUCT_SKU_INFO_SUCCESS,
    payload,
  };
};

export const setProductMultiPackData = (payload) => {
  return {
    type: CARTPAGE_CONSTANTS.SET_PRODUCT_MULTIPACK_DATA,
    payload,
  };
};

export const updateCartItem = (payload) => {
  return {
    type: CARTPAGE_CONSTANTS.UPDATE_CART_ITEM,
    payload,
  };
};

export const updateCartItemComplete = () => {
  return {
    type: CARTPAGE_CONSTANTS.UPDATE_CART_ITEM_COMPLETE,
  };
};

export const setToggleCartItemError = (payload) => {
  return {
    type: CARTPAGE_CONSTANTS.SET_TOGGLE_CART_ITEM_ERROR,
    payload,
  };
};

export const setBossBopisToggleCartItemError = (payload) => {
  return {
    type: CARTPAGE_CONSTANTS.SET_TOGGLE_BOSS_BOPIS_CART_ITEM_ERROR,
    payload,
  };
};

export const clearToggleCartItemError = () => {
  return {
    type: CARTPAGE_CONSTANTS.CLEAR_TOGGLE_CART_ITEM_ERROR,
  };
};

export const clearToggleBossBopisCartItemError = () => {
  return {
    type: CARTPAGE_CONSTANTS.CLEAR_TOGGLE_BOSS_BOPIS_CART_ITEM_ERROR,
  };
};

export const setCartLoadingState = (payload) => {
  return {
    type: CARTPAGE_CONSTANTS.CART_LAODINGSTATE,
    payload,
  };
};
