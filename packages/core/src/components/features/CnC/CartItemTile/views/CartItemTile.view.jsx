// 9fbef606107a605d69c0edbcd8029e5d 
/**
 * These are temporary changes for a dummy Bag page
 */
import React from 'react';
import PropTypes from 'prop-types';
import Row from '@tcp/core/src/components/common/atoms/Row';
import Col from '@tcp/core/src/components/common/atoms/Col';
import CartItem from '../organisms/CartItem';

class CartItemTile extends React.Component {
  loadGetOrderDetails = () => {
    const { getOrderDetails } = this.props;
    getOrderDetails();
  };

  deleteCartItem = orderItemId => {
    const { removeCartItem } = this.props;
    removeCartItem({ orderItemId });
  };

  render() {
    const { cartItems, editableProductInfo, getProductSKUInfo, updateCartItem } = this.props;
    return (
      <div>
        <span>CART ITEM TILE</span>
        <button onClick={this.loadGetOrderDetails}>load data</button>
        <Row tagName="ul" className="cart-items-list">
          {cartItems &&
            cartItems.map(item => (
              <Col tagName="li" className="cart-item" colSize={{ small: 2, medium: 3, large: 12 }}>
                <CartItem
                  updateCartItem={updateCartItem}
                  item={item}
                  deleteCartItem={this.deleteCartItem}
                  getProductSKUInfo={getProductSKUInfo}
                  editableProductInfo={editableProductInfo}
                />
              </Col>
            ))}
        </Row>
      </div>
    );
  }
}

CartItemTile.propTypes = {
  getOrderDetails: PropTypes.func.isRequired,
  removeCartItem: PropTypes.shape({}).isRequired,
  cartItems: PropTypes.shape([]).isRequired,
  editableProductInfo: PropTypes.shape({}).isRequired,
  getProductSKUInfo: PropTypes.func.isRequired,
  updateCartItem: PropTypes.func.isRequired,
};
export default CartItemTile;
export { CartItemTile as CartItemTileVanilla };
