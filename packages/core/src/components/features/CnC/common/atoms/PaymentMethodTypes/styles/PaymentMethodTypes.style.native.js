// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const Wrapper = styled.TouchableOpacity`
  text-align: center;
  height: 66px;
  flex-direction: column;
  display: flex;
  justify-content: center;
  border: solid 1px
    ${({ theme: { colorPalette }, checked }) =>
      checked ? colorPalette.black : colorPalette.gray[500]};
  border-radius: 6px;
  flex: 1;
  margin: 4px;
  background-color: ${(props) =>
    props.id === 'afterpay' ? props.theme.colors.AFTERPAY.MINT : props.theme.colorPalette.white};
`;

const ImageWrapper = styled.View`
  justify-content: center;
  align-items: center;
`;

export { ImageWrapper, Wrapper };
