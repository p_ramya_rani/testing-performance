// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Image from '@tcp/core/src/components/common/atoms/Image';
import { getIconCard } from '@tcp/core/src/utils/index.native';
import CONSTANTS from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import { Wrapper, ImageWrapper } from '../styles/PaymentMethodTypes.style.native';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

const getIcon = (type, labels) => {
  const {
    PAYMENT_METHOD_VENMO,
    PAYMENT_METHOD_PAYPAL,
    PAYMENT_METHOD_APPLE_PAY,
    PAYMENT_METHOD_CREDIT_CARD,
    PAYMENT_METHOD_AFTERPAY,
  } = CONSTANTS;

  switch (type) {
    case PAYMENT_METHOD_PAYPAL:
      return {
        paymentTypeIcon: getIconCard('new-paypal-transparent'),
        iconHeight: '48px',
        iconCaption: '',
        iconWidth: '100',
      };
    case PAYMENT_METHOD_VENMO:
      return {
        paymentTypeIcon: getIconCard('venmo-transparent'),
        iconHeight: '16px',
        iconCaption: '',
        iconWidth: '70',
      };
    case PAYMENT_METHOD_APPLE_PAY:
      return {
        paymentTypeIcon: getIconCard('apple-pay-transparent'),
        iconHeight: '28px',
        iconCaption: '',
        iconWidth: '100',
      };
    case PAYMENT_METHOD_CREDIT_CARD:
      return {
        paymentTypeIcon: getIconCard('cc-transparent'),
        iconHeight: '40px',
        iconCaption: (labels && labels.creditCard) || '',
        iconWidth: '100',
      };
    case PAYMENT_METHOD_AFTERPAY:
      return {
        paymentTypeIcon: getIconCard('after-pay-icon'),
        iconHeight: '28px',
        iconCaption: '',
        iconWidth: '70',
      };

    default:
      return null;
  }
};
const PaymentMethodTypes = ({ title, onPress, id, selectedPaymentId, index, labels }) => {
  const isChecked = selectedPaymentId === id;
  const { paymentTypeIcon, iconHeight, iconCaption, iconWidth } = getIcon(id, labels);
  return (
    <Wrapper
      checked={isChecked}
      index={index}
      onPress={() => onPress()}
      accessible
      accessibilityRole="link"
      accessibilityLabel={
        isChecked
          ? `Active Payment Method ${title || id}`
          : `Inactive Payment Method ${title || id}`
      }
      id={id}
    >
      {paymentTypeIcon && (
        <ImageWrapper>
          <Image source={paymentTypeIcon} width={iconWidth} height={iconHeight} />
        </ImageWrapper>
      )}
      {iconCaption ? (
        <BodyCopy
          textAlign="center"
          mobileFontFamily="secondary"
          fontSize="fs10"
          fontWeight="extrabold"
          checked={isChecked}
          text={iconCaption}
        />
      ) : null}
    </Wrapper>
  );
};

PaymentMethodTypes.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
  id: PropTypes.string,
  selectedPaymentId: PropTypes.string,
  index: PropTypes.number,
  labels: PropTypes.shape({}).isRequired,
};

PaymentMethodTypes.defaultProps = {
  title: '',
  onPress: () => {},
  id: null,
  selectedPaymentId: null,
  index: null,
};

export default PaymentMethodTypes;
