// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  createAccount,
  resetCreateAccountErr,
} from '@tcp/core/src/components/features/account/CreateAccount/container/CreateAccount.actions';
import { resetCheckoutReducer } from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.action.util';
import { getErrorMessage } from '@tcp/core/src/components/features/account/CreateAccount/container/CreateAccount.selectors';
import ConfirmationAccountForm from '../views';
import confirmationSelectors from '../../../../Confirmation/container/Confirmation.selectors';
import confirmationAccountSelectors from './ConfirmationAccountForm.selectors';
import { toastMessageInfo } from '../../../../../../common/atoms/Toast/container/Toast.actions';

/**
 * @function ConfirmationAccountFormContainer
 * @param {Object} props
 * @return {JSX} Render Container
 */
export class ConfirmationAccountFormContainer extends React.Component {
  componentWillUnmount() {
    const { resetAccountErrorState, resetCheckoutReducerState } = this.props;
    resetCheckoutReducerState();
    resetAccountErrorState();
  }

  render() {
    return <ConfirmationAccountForm {...this.props} />;
  }
}

/* istanbul ignore next */
export const mapDispatchToProps = dispatch => {
  return {
    createAccountSubmit: payload => {
      dispatch(createAccount(payload));
    },
    resetAccountErrorState: () => {
      dispatch(resetCreateAccountErr());
    },
    resetCheckoutReducerState: () => {
      dispatch(resetCheckoutReducer());
    },
    toastMessage: palyoad => {
      dispatch(toastMessageInfo(palyoad));
    },
  };
};

ConfirmationAccountFormContainer.propTypes = {
  resetAccountErrorState: PropTypes.func.isRequired,
  resetCheckoutReducerState: PropTypes.func.isRequired,
};

/* istanbul ignore next */
const mapStateToProps = state => {
  const userInformation = confirmationSelectors.getInitialCreateAccountValues(state);
  return {
    className: 'confirmation-create-account',
    isPromptForUserDetails: !userInformation || !userInformation.firstName,
    emailAddress: userInformation && userInformation.emailAddress,
    initialValues: {
      ...userInformation,
    },
    userInformation,
    labels: {
      ...confirmationAccountSelectors.getCreateAccountLabels(state),
      ...confirmationAccountSelectors.getAccessibilityLabels(state),
    },
    passwordLabels: confirmationAccountSelectors.getPasswordLabels(state),
    createAccountSuccess: confirmationAccountSelectors.getCreateAccountSuccess(state),
    createAccountError: getErrorMessage(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmationAccountFormContainer);
