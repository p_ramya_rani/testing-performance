// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';
import { RichText } from '@tcp/core/src/components/common/atoms';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import { isAndroid } from '@tcp/core/src/utils/utils.app';
import { getLabelValue } from '@tcp/core/src/utils';
import RNPrint from 'react-native-print';
import ContentLink from '@tcp/core/src/components/features/account/common/molecule/ContentLink';
import ScreenViewShot from '../../../../../../common/atoms/ScreenViewShot';

import {
  StyledModalWrapper,
  Horizontal,
  PrivacyContent,
  FullHeaderStyle,
  StyledButton,
} from '../styles/CouponDetailModal.style.native';
import { COUPON_REDEMPTION_TYPE } from '../../../../../../../services/abstractors/CnC/CartItemTile';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import getMarkupForPrint from './CouponDetailPrintHTMLModal.native';

const COUPON_APPLIED = 'applied';

class CouponDetailModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.screenViewShotRef = null;

    this.setScreenViewShotRef = element => {
      this.screenViewShotRef = element;
    };
  }

  componentDidUpdate() {
    const { coupon, handleErrorCoupon } = this.props;
    if (coupon.error) {
      handleErrorCoupon(coupon);
    }
  }

  /**
   * This function is used for apply to bag coupon
   * can be passed in the component.
   */
  handleApplyToBag = () => {
    const { onApplyCouponToBagFromList, coupon, onRequestClose } = this.props;
    onApplyCouponToBagFromList(coupon);
    onRequestClose();
  };

  /**
   * This function is used to return coupon validity message
   * can be passed in the component.
   */
  showValidity = () => {
    const { coupon, labels } = this.props;
    const isPlaceCash = coupon.redemptionType === COUPON_REDEMPTION_TYPE.PLACECASH;
    const validityLbl = isPlaceCash
      ? getLabelValue(labels, 'COUPON_VALIDITY')
      : getLabelValue(labels, 'USE_BY_TEXT');
    const validityStr = isPlaceCash
      ? `${coupon.effectiveDate} - ${coupon.expirationDate}`
      : `${coupon.expirationDate}`;
    return `${validityLbl} ${validityStr}`;
  };

  /**
   * This function is to get cta label
   * can be passed in the component.
   * @param {obj} - labels
   * @param {boolean} - isStarted
   * @param {boolean} - isPlaceCash
   * @return {String} - cta label
   */
  getAddToBagCtaLabel = (labels, isStarted, isPlaceCash, isApplied) => {
    if (!isStarted && isPlaceCash) {
      return getLabelValue(labels, 'SEE_REDEEM_DATES');
    }
    return isApplied
      ? getLabelValue(labels, 'APPLIED_TO_BAG')
      : getLabelValue(labels, 'APPLY_TO_BAG');
  };

  getCouponCode = coupon => {
    return coupon && coupon.indexOf('-') > -1 ? coupon.split('-')[1] : coupon;
  };

  /**
   * This function is to print HTML
   * @param {obj} - labels
   * @param {obj} - coupon
   * @param {string} - addToBagCTALabel
   */
  async printHTML(coupon, labels) {
    const uri = await this.screenViewShotRef.capture();
    await RNPrint.print({
      html: getMarkupForPrint(coupon, labels, this.showValidity(), uri),
    });
  }

  render() {
    const { openState, onRequestClose, coupon, isDisabled, labels, isConfirmation } = this.props;
    const { isStarted } = coupon;
    const isApplied = coupon.status === COUPON_APPLIED;
    const couponId = this.getCouponCode(coupon.id);
    const isApplyButtonDisabled = isDisabled || !coupon.isStarted || isApplied;
    const isPlaceCash = coupon.redemptionType === COUPON_REDEMPTION_TYPE.PLACECASH;
    const addToBagCTALabel = this.getAddToBagCtaLabel(
      labels,
      coupon.isStarted,
      isPlaceCash,
      isApplied
    );

    return (
      <Modal
        fixedWidth
        isOpen={openState}
        onRequestClose={onRequestClose}
        overlayClassName="TCPModal__Overlay"
        closeIconDataLocator="added-to-bg-close"
        closeIconLeftAligned={false}
        horizontalBar={false}
        headerStyle={FullHeaderStyle}
        headingFontFamily="secondary"
        stickyCloseIcon={!isAndroid()}
      >
        <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
          <StyledModalWrapper>
            <BodyCopy
              data-locator={`couponDetailModal_${coupon.status}_NameLbl`}
              fontSize="fs40"
              fontFamily="primary"
              fontWeight="black"
              text={coupon.title}
              textAlign="center"
            />
            {!isConfirmation && (
              <ViewWithSpacing spacingStyles="margin-top-XS">
                <BodyCopy
                  data-locator={`couponDetailModal_${coupon.status}_ValidityDateLbl`}
                  fontSize="fs22"
                  fontFamily="secondary"
                  fontWeight="extrabold"
                  text={this.showValidity()}
                />
              </ViewWithSpacing>
            )}
            {couponId ? (
              <>
                <Horizontal />
                <View data-locator={`couponDetailModal_${coupon.status}_BarCode`}>
                  <ScreenViewShot
                    setScreenViewShotRef={this.setScreenViewShotRef}
                    options={{ format: 'png', quality: 0.9, result: 'base64' }}
                  >
                    <Barcode value={couponId} fontSize="fs12" />
                  </ScreenViewShot>
                </View>
                <Horizontal />
              </>
            ) : (
              <ScreenViewShot
                setScreenViewShotRef={this.setScreenViewShotRef}
                options={{ format: 'png', quality: 0.9, result: 'base64' }}
              >
                <BodyCopy
                  data-locator={`couponDetailModal_${coupon.status}_NameLbl`}
                  fontSize="fs42"
                  fontFamily="primary"
                  fontWeight="black"
                  text=" "
                />
              </ScreenViewShot>
            )}
            {!isConfirmation && isStarted && (
              <ViewWithSpacing spacingStyles="margin-bottom-LRG">
                <StyledButton
                  text={addToBagCTALabel}
                  buttonVariation="variable-width"
                  width={225}
                  disabled={isApplyButtonDisabled}
                  data-locator={`couponDetailModal_${coupon.status}_AddToBagBtn`}
                  fill="BLUE"
                  onPress={() => {
                    this.handleApplyToBag();
                  }}
                />
              </ViewWithSpacing>
            )}
            <Anchor
              fontSizeVariation="large"
              underline
              anchorVariation="primary"
              dataLocator={`couponDetailModal_${coupon.status}_printAch`}
              text={getLabelValue(labels, 'PRINT_ANCHOR_TEXT')}
              class="clickhere"
              onPress={() => this.printHTML(coupon, labels, this.showValidity())}
            />
            {!!coupon.legalText && (
              <PrivacyContent data-locator={`couponDetailModal_${coupon.status}_LongDesc`}>
                <RichText source={{ html: coupon.legalText }} />
              </PrivacyContent>
            )}
            {!isConfirmation && (
              <PrivacyContent>
                <BodyCopy
                  data-locator={`couponDetailModal_${coupon.status}_ShortDesc`}
                  fontSize="fs12"
                  fontFamily="secondary"
                  fontWeight="regular"
                  text={getLabelValue(labels, 'MODAL_SHORT_DESCRIPTION')}
                />
                <ContentLink
                  fontSizeVariation="medium"
                  fontFamily="primary"
                  anchorVariation="primary"
                  underline
                  urlKey="lbl_termcondition_link"
                  dataLocator={`couponDetailModal_${coupon.status}_tAndC`}
                  text={getLabelValue(labels, 'TERMS_AND_CONDITIONS')}
                />
                <BodyCopy
                  data-locator={`couponDetailModal_${coupon.status}_and`}
                  fontSize="fs12"
                  fontFamily="secondary"
                  fontWeight="regular"
                  text=" and "
                />
                <ContentLink
                  fontSizeVariation="medium"
                  underline
                  urlKey="lbl_privacyPolicy_link"
                  fontFamily="primary"
                  anchorVariation="primary"
                  dataLocator={`couponDetailModal_${coupon.status}_pp`}
                  text={getLabelValue(labels, 'PRIVACY_POLICY')}
                />
              </PrivacyContent>
            )}
          </StyledModalWrapper>
        </ScrollView>
      </Modal>
    );
  }
}
CouponDetailModal.propTypes = {
  coupon: PropTypes.shape({}).isRequired,
  handleErrorCoupon: PropTypes.func.isRequired,
  onApplyCouponToBagFromList: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  openState: PropTypes.bool.isRequired,
  isDisabled: PropTypes.bool.isRequired,
  isConfirmation: PropTypes.bool.isRequired,
};

export default CouponDetailModal;
export { CouponDetailModal as CouponDetailModalVanilla };
