// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { CARTPAGE_REDUCER_KEY } from '../../../../../../../constants/reducer.constants';
import { getLabelValue } from '../../../../../../../utils';

export const getItemsTotalCount = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'totalItems']) || 0;
};
export const getCouponsTotal = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'couponsTotal']) || 0;
};
export const getApplePayDisc = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'applePayDisc']) || 0;
};
export const getSavingsTotal = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'savingsTotal']) || 0;
};
export const getShippingTotal = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'shippingTotal']);
};
export const getTotalTax = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'totalTax']) || 0;
};
export const getGrandTotal = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'grandTotal']) || 0;
};
export const getGiftCardsTotal = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'giftCardsTotal']) || 0;
};
export const getTotalOrderSavings = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'totalOrderSavings']) || 0;
};
export const getSubTotal = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'subTotal']) || 0;
};
export const getCurrencySymbol = (state) => {
  const currency = state.session && state.session.siteDetails.currency;
  // eslint-disable-next-line no-nested-ternary
  return currency ? (currency === 'USD' || currency === 'CAD' ? '$' : currency) : '$';
};
export const getGiftServiceTotal = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'giftWrappingTotal']) || 0;
};

const getIsOrderHasShipping = (state) =>
  !!state[CARTPAGE_REDUCER_KEY].getIn(['orderDetails', 'isShippingOrder']);

export const getLedgerSummaryData = createSelector(
  [
    getItemsTotalCount,
    getSubTotal,
    getCouponsTotal,
    getSavingsTotal,
    getGiftServiceTotal,
    getShippingTotal,
    getTotalTax,
    getGrandTotal,
    getGiftCardsTotal,
    getTotalOrderSavings,
    getCurrencySymbol,
    getIsOrderHasShipping,
  ],
  (
    itemsCount,
    subTotal,
    couponsTotal,
    savingsTotal,
    giftServiceTotal,
    shippingTotal,
    taxesTotal,
    grandTotal,
    giftCardsTotal,
    totalOrderSavings,
    currencySymbol,
    isOrderHasShipping
    // eslint-disable-next-line max-params
  ) => {
    return {
      itemsCount,
      subTotal,
      couponsTotal,
      savingsTotal,
      giftServiceTotal,
      shippingTotal,
      taxesTotal,
      grandTotal,
      giftCardsTotal,
      orderBalanceTotal: grandTotal - giftCardsTotal,
      totalOrderSavings,
      currencySymbol,
      isOrderHasShipping,
    };
  }
);

const allLabels = (state) => state.Labels;

export const getOrderLedgerLabels = createSelector(allLabels, (labels) => {
  const {
    checkout: {
      bagPage: {
        lbl_orderledger_items: itemsLabel,
        lbl_orderledger_coupons: couponsLabel,
        lbl_orderledger_promotions: promotionsLabel,
        lbl_orderledger_giftServices: giftServiceLabel,
        lbl_orderledger_shipping: shippingLabel,
        lbl_orderledger_tax: taxLabel,
        lbl_orderledger_total: totalLabel,
        lbl_orderledger_giftcards: giftcardsLabel,
        lbl_orderledger_balance: balanceLabel,
        lbl_orderledger_totalsavings: totalSavingsLabel,
        lbl_orderledger_tooltiptext: tooltipText,
        lbl_orderledger_free: free,
        lbl_orderledger_title: orderLedgerTitle,
        lbl_confirmation_paid_with_venmo: paidWithVenmo,
        lbl_applepay_subTotal: subtotal,
        lbl_applepay_discounts: discounts,
        lbl_applepay_totalAmount: totalAmount,
        lbl_applepay_estimatedTax: estimatedTax,
        lbl_applepay_nonEligibleStateErrorMessage: nonEligibleStateErrorMessage,
        lbl_applepay_nonEligibleCountryErrorMessage: nonEligibleCountryErrorMessage,
        lbl_applepay_noLastNameErrorMessage: noLastNameErrorMessage,
        lbl_applepay_noPhoneNumberErrorMessage: noPhoneNumberErrorMessage,
        lbl_applepay_noEmailAddressErrorMessage: noEmailAddressErrorMessage,
        lbl_applepay_multipleLocations: multipleLocations,
        lbl_applepay_pickUpAt: pickUpAt,
        lbl_applepay_giftServices: giftServices,
        lbl_applepay_giftCards: giftCards,
        lbl_applepay_invalid_street_name: InvalidStreetName,
        lbl_applepay_invalid_house_number: InvalidHouseNumber,
        lbl_applepay_invalid_postal_code: InvalidPostalCode,

        lbl_applepay_required_city: requiredCity,
        lbl_applepay_required_state: requiredState,
        lbl_applepay_required_postal_code: requiredPostalCode,
      } = {},
    } = {},
  } = labels;

  return {
    itemsLabel,
    couponsLabel,
    promotionsLabel,
    giftServiceLabel,
    shippingLabel,
    taxLabel,
    totalLabel,
    giftcardsLabel,
    balanceLabel,
    totalSavingsLabel,
    tooltipText,
    free,
    orderLedgerTitle,
    totalLabelConfirmation: getLabelValue(
      labels,
      'lbl_orderledger_totalConfirmation',
      'bagPage',
      'checkout'
    ),
    paidWithVenmo,
    subtotal,
    discounts,
    totalAmount,
    estimatedTax,
    nonEligibleStateErrorMessage,
    nonEligibleCountryErrorMessage,
    noLastNameErrorMessage,
    noPhoneNumberErrorMessage,
    noEmailAddressErrorMessage,
    multipleLocations,
    pickUpAt,
    giftServices,
    giftCards,
    InvalidStreetName,
    InvalidHouseNumber,
    InvalidPostalCode,
    requiredCity,
    requiredState,
    requiredPostalCode,
  };
});
