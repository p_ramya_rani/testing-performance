// 9fbef606107a605d69c0edbcd8029e5d 
import {
  getLabelValue,
  getFormatedOrderDate,
  addDays,
  convertToISOString,
} from '@tcp/core/src/utils/utils';
import {
  outerSquareStyle,
  innerSquareStyle,
  brandLogoStyle,
  returnHeadingBoxStyle,
  barCodeDivStyle,
  barCodeStyle,
  returnByStyle,
  orderNumberHeadingStyle,
  orderNumberStyle,
  breakPage,
} from '../styles/GiftReceipt.style.native';

const logo =
  'https://assets.theplace.com/image/upload/w_185,h_54,f_auto,q_auto/v1593674792/TCP-GYM_Logos_PMS-Horizontal_BLK.jpg';

export default function getGiftReceiptMarkupForPrint({
  barcode,
  orderNumber,
  returnDays,
  giftReceiptLabels,
  orderDate,
}) {
  const giftReceiptHeading = getLabelValue(giftReceiptLabels, 'lbl_print_gift_receipt_heading');
  const giftReceiptReturnBy = getLabelValue(giftReceiptLabels, 'lbl_print_gift_receipt_return_by');
  const orderNumberHeading = getLabelValue(
    giftReceiptLabels,
    'lbl_print_gift_receipt_order_number'
  );
  const returnByUpdate = getLabelValue(giftReceiptLabels, 'lbl_print_gift_receipt_return_update');

  let finalReturnUpdate = '';
  let overrideCssObject = {};
  if (returnDays) {
    finalReturnUpdate = `${giftReceiptReturnBy}: ${getFormatedOrderDate(
      addDays(Date(convertToISOString(orderDate)), parseInt(returnDays, 10)),
      true
    )}`;

    overrideCssObject = {
      innerOverrideStyle: 'height: 250px;',
      overrideReturnByStyle: 'padding-left: 44px; font-size: 16px; font-weight: 800',
      outerOverrideStyle: '858px',
    };
  } else {
    /** if return days 0 then  display cms managed message */
    finalReturnUpdate = returnByUpdate;
    overrideCssObject = {
      innerOverrideStyle: 'height: 375px;',
      overrideReturnByStyle: 'padding: 0 28px 0 44px',
      outerOverrideStyle: 'height:825px',
    };
  }

  const { innerOverrideStyle, overrideReturnByStyle, outerOverrideStyle } = overrideCssObject;

  const htmlToPrint = `
    <div style="${innerSquareStyle} ${innerOverrideStyle}">
      <div style="${barCodeDivStyle}">
        <img style="${brandLogoStyle}" src="${logo}" alt="Brand Logo" />
      </div>
      <h1 style="${returnHeadingBoxStyle}">${giftReceiptHeading}</h1>
      <div style="${barCodeDivStyle}">
        <img style="${barCodeStyle}" src="data:image/png;base64, ${barcode}" alt="" />
      </div>
      <div style="${returnByStyle} ${overrideReturnByStyle}">${finalReturnUpdate}</div>
      <div>
        <div style="${orderNumberHeadingStyle}">${orderNumberHeading}:</div>
        <div style="${orderNumberStyle}"> ${orderNumber}</div>
      </div>
    </div>
  `;

  const htmlWithCMSUpdatedMessage = `
      <div style="${outerSquareStyle} ${outerOverrideStyle}">
        ${htmlToPrint}
        ${htmlToPrint}
      </div>
      <div style="${outerSquareStyle} ${outerOverrideStyle} ${breakPage}">
        ${htmlToPrint}
      </div>
  `;

  const htmlWithoutCMSUpdatedMessage = `
    <div style="${outerSquareStyle} ${outerOverrideStyle}">
      ${htmlToPrint}
      ${htmlToPrint}
      ${htmlToPrint}
    </div>
  `;

  return returnDays ? htmlWithoutCMSUpdatedMessage : htmlWithCMSUpdatedMessage;
}
