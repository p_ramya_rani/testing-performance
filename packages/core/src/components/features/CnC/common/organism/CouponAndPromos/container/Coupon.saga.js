/* eslint-disable no-underscore-dangle */
// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, delay, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  getProductDetails,
  getTotalItemCount,
} from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import setLoaderState from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { isMobileApp, setValueInAsyncStorage, isCouponsUpdateRequired } from '@tcp/core/src/utils';
import { getFavoriteStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { getCouponsFetchTimestamp } from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.selectors';
import { getCouponsTimeout } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  setCouponsFetchTimestampState,
  setOnHoldCoupon,
  setGetCouponDetailsLoader,
} from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import { getCartItemCount } from '@tcp/core/src/utils/cookie.util';
import COUPON_CONSTANTS from '../Coupon.constants';
import { validateReduxCache } from '../../../../../../../utils/cache.util';
import {
  hideLoader,
  showLoader,
  setStatus,
  setError,
  setCouponList,
  setCouponDetails,
  setCouponDetailsError,
  setApplyCouponCSHLoader,
  setApplyCouponCSHError,
} from './Coupon.actions';
import { getCartDataSaga } from '../../../../BagPage/container/BagPage.saga';
import BagPageSelectors from '../../../../BagPage/container/BagPage.selectors';
import {
  applyCouponToCart,
  removeCouponOrPromo,
  getAllCoupons as getAllCouponsAbstractor,
  getCouponDetails as getCouponDetailsAbstractor,
} from '../../../../../../../services/abstractors/CnC';
import {
  COUPON_STATUS,
  BUTTON_LABEL_STATUS,
} from '../../../../../../../services/abstractors/CnC/CartItemTile';
import COUPON_SELF_HELP_CONSTANT from '../../../../../account/CustomerHelp/organisms/TellUsMore/organisms/CouponSelfHelp/CouponSelfHelp.constants';

const pageTypeName = (formData) => {
  return formData.analyticsData && formData.analyticsData.pageType
    ? formData.analyticsData.pageType
    : 'shopping bag';
};

const getOrderFailMatricValue = (isFailed) => (isFailed ? '1' : '');
const getOrderSuccessMatricValue = (isFailed) => (isFailed ? '' : '1');

const getTrackingObj = (fromCSH, formData, productsData, coupon, storeId, isFailed) => {
  const pageSectionName = pageTypeName(formData);
  const event27 = isMobileApp() ? 'Coupon_Add_Unsuccessful_e27' : 'event27';
  const event28 = isMobileApp() ? 'Coupon_Add_Success_e28' : 'event28';
  let events;
  let cm152;
  let cm153;
  if (fromCSH) {
    cm153 = getOrderFailMatricValue(isFailed);
    cm152 = getOrderSuccessMatricValue(isFailed);
    events = isFailed ? 'event153' : 'event152';
  } else {
    events = isFailed ? event27 : event28;
  }
  const eventNameStr = isFailed ? 'invalid coupon code used' : 'coupon applied';

  return {
    customEvents: [events],
    products: productsData,
    eventName:
      formData.analyticsData && formData.analyticsData.eventName
        ? formData.analyticsData.eventName
        : eventNameStr,
    couponCode: coupon && coupon.id,
    pageNavigationText:
      formData.analyticsData && formData.analyticsData.pageNavigationText
        ? formData.analyticsData.pageNavigationText
        : null,
    storeId,
    pageSection: pageSectionName,
    pageType: pageSectionName,
    pageSubSection: pageSectionName,
    clickEvent: true,
    event152: cm152,
    event153: cm153,
  };
};

const getTrackingObjRemove = (formData, storeId) => {
  return {
    eventName:
      formData.analyticsCustomData && formData.analyticsCustomData.eventName
        ? formData.analyticsCustomData.eventName
        : 'coupon removed',
    pageNavigationText:
      formData.analyticsCustomData && formData.analyticsCustomData.pageNavigationText
        ? formData.analyticsCustomData.pageNavigationText
        : 'bag - remove coupon',
    storeId,
    clickEvent: true,
  };
};

export function* getCartCounts() {
  const cartItemsCookie = yield call(getCartItemCount, true);
  const cartItemsTotal = yield select(getTotalItemCount);
  return cartItemsTotal || parseInt(cartItemsCookie || 0, 10);
}

// eslint-disable-next-line
export function* applyCoupon({ payload }) {
  const {
    formData,
    formPromise: { resolve, reject },
    source,
    coupon,
    fullPageInfo,
    isCheckoutFlow,
    fromCSH,
  } = payload;

  const cartOrderItems = yield select(BagPageSelectors.getOrderItems);
  const storeId = yield select(getFavoriteStore);
  const productsData = [];
  if (cartOrderItems) {
    cartOrderItems.map((tile) => {
      const productDetail = getProductDetails(tile);
      const {
        itemInfo: { itemId, color, name, offerPrice, size, listPrice, qty },
        productInfo: { skuId, generalProductId, upc, productPartNumber },
      } = productDetail;

      const prodData = {
        color,
        id: itemId,
        name,
        price: offerPrice,
        extPrice: offerPrice,
        listPrice,
        partNumber: productPartNumber,
        size,
        upc,
        sku: skuId.toString(),
        quantity: qty,
        colorId: generalProductId,
      };
      productsData.push(prodData);
      return prodData;
    });
  }
  const cartItemsCount = yield call(getCartCounts);

  if (coupon) {
    let oldStatus = coupon.status;
    if (coupon.status === COUPON_STATUS.AVAILABLE) {
      oldStatus = BUTTON_LABEL_STATUS.APPLY;
    } else if (coupon.status === COUPON_STATUS.APPLIED) {
      oldStatus = BUTTON_LABEL_STATUS.REMOVE;
    }

    try {
      if (!fromCSH) yield put(setLoaderState(true));
      else yield put(setApplyCouponCSHLoader(true));
      yield put(showLoader());
      yield put(setStatus({ promoCode: coupon.id, status: COUPON_STATUS.APPLYING }));
      const labels = yield select(BagPageSelectors.getErrorMapping);
      const result = yield call(applyCouponToCart, formData, labels, cartItemsCount);
      yield put(hideLoader());
      if (result && result.isOnHold) {
        yield put(
          setOnHoldCoupon({
            promoCode: coupon.id,
            isOnHold: 'true',
            status: 'onHold',
            labelStatus: 'Remove',
          })
        );
      } else {
        yield put(
          setClickAnalyticsData(
            getTrackingObj(fromCSH, formData, productsData, coupon, storeId, false)
          )
        );
        yield put(
          trackClick({
            name: 'coupon_success',
            module: 'checkout',
          })
        );
        yield put(setStatus({ promoCode: coupon.id, status: COUPON_STATUS.APPLIED }));

        yield call(getCartDataSaga, {
          payload: {
            recalcRewards: true,
            isRecalculateTaxes: true,
            translation: false,
            excludeCartItems: !fullPageInfo,
            isCheckoutFlow,
          },
        });
      }
      if (!fromCSH) yield put(setLoaderState(false));
      else yield put(setApplyCouponCSHLoader(false));
      resolve();
    } catch (e) {
      logger.error('Error while applying coupon', e);
      yield put(
        setClickAnalyticsData(
          getTrackingObj(fromCSH, formData, productsData, coupon, storeId, true)
        )
      );
      yield put(
        trackClick({
          name: 'coupon_fails',
          module: 'checkout',
        })
      );
      yield put(setStatus({ promoCode: coupon.id, status: oldStatus }));
      if (!fromCSH) yield put(setLoaderState(false));
      else yield put(setApplyCouponCSHLoader(false));
      yield put(hideLoader());
      if (source !== 'form' && e.errors) {
        if (!fromCSH)
          yield put(setError({ msg: e.errors._error.msg, couponCode: formData.couponCode }));
        else {
          yield put(setApplyCouponCSHError({ msg: e.errors._error.msg }));
        }
      }
    }
  } else {
    try {
      yield put(setLoaderState(true));
      yield put(showLoader());
      const labels = yield select(BagPageSelectors.getErrorMapping);
      const result = yield call(applyCouponToCart, formData, labels, cartItemsCount);
      yield put(hideLoader());
      const { couponCode = '' } = formData || {};
      if (result && result.isOnHold) {
        yield put(
          setOnHoldCoupon({
            promoCode: couponCode,
            isOnHold: true,
            status: 'onHold',
            labelStatus: 'Remove',
          })
        );
      } else {
        yield put(
          setClickAnalyticsData(
            getTrackingObj(fromCSH, formData, productsData, null, storeId, false)
          )
        );
        yield put(
          trackClick({
            name: 'coupon_success',
            module: 'checkout',
          })
        );
        yield call(getCartDataSaga, {
          payload: {
            recalcRewards: true,
            isRecalculateTaxes: true,
            translation: false,
            excludeCartItems: !fullPageInfo,
            isCheckoutFlow,
          },
        });
      }
      yield put(setLoaderState(false));
      resolve();
    } catch (e) {
      logger.error('Error while applying coupon', e);
      yield put(setLoaderState(false));
      yield put(
        setClickAnalyticsData({
          customEvents: isMobileApp() ? ['Coupon_Add_Unsuccessful_e27'] : 'event27',
          products: productsData,
          eventName: 'invalid coupon code used',
        })
      );
      yield put(
        trackClick({
          name: 'coupon_fails',
          module: 'checkout',
        })
      );
      yield put(hideLoader());
      reject(e);
    }
  }
}

const couponAvailableStatus = (offertype) => (offertype === 'PLCB' ? 'available' : 'notavailable');

export function* removeCoupon({ payload }) {
  const {
    coupon,
    fullPageInfo,
    isCheckoutFlow,
    formPromise: { resolve, reject },
    fromCSH,
  } = payload;
  const formData = { couponCode: coupon.id };
  const storeId = yield select(getFavoriteStore);
  let oldStatus = coupon.status;
  if (coupon.status === COUPON_STATUS.AVAILABLE) {
    oldStatus = BUTTON_LABEL_STATUS.APPLY;
  } else if (coupon.status === COUPON_STATUS.APPLIED) {
    oldStatus = BUTTON_LABEL_STATUS.REMOVE;
  }
  try {
    if (!fromCSH) yield put(setLoaderState(true));
    else yield put(setApplyCouponCSHLoader(true));
    yield put(showLoader());
    if (!fromCSH) yield put(setStatus({ promoCode: coupon.id, status: COUPON_STATUS.REMOVING }));
    yield call(removeCouponOrPromo, formData);
    if (!coupon.isOnHold || coupon.isOnHold === 'false') {
      yield call(getCartDataSaga, {
        payload: {
          recalcRewards: true,
          isRecalculateTaxes: true,
          translation: false,
          excludeCartItems: !fullPageInfo,
          isCheckoutFlow,
        },
      });
    } else {
      yield put(
        setOnHoldCoupon({
          promoCode: coupon.id,
          isOnHold: false,
          status: couponAvailableStatus(coupon.originalOfferType),
          labelStatus: 'APPLY',
        })
      );
    }
    yield put(setStatus({ promoCode: coupon.id, status: BUTTON_LABEL_STATUS.APPLY }));

    yield put(hideLoader());
    if (!fromCSH) yield put(setLoaderState(false));
    else yield put(setApplyCouponCSHLoader(false));
    yield put(setClickAnalyticsData(getTrackingObjRemove(payload, storeId)));
    yield put(
      trackClick({
        name: 'coupon_removed',
        module: 'checkout',
      })
    );
    resolve();
  } catch (e) {
    yield put(setStatus({ promoCode: coupon.id, status: oldStatus }));
    if (!fromCSH) yield put(setLoaderState(false));
    else yield put(setApplyCouponCSHLoader(false));
    // eslint-disable-next-line
    yield put(setError({ msg: e.errors._error.msg, couponCode: formData.couponCode }));
    yield delay(5000);
    yield put(setError({ msg: null, couponCode: formData.couponCode }));
    reject(e);
  }
}

/**
 * @description - set current date and time to redux and async storage for last api call comparison
 * @param {boolean} couponsFetchRequired
 */
export function* updateCouponsTimeout(couponsFetchRequired) {
  if (couponsFetchRequired && isMobileApp()) {
    const { COUPONS_UPDATE_TIMEOUT } = COUPON_CONSTANTS;
    const currentDateTimeString = new Date().toString();
    setValueInAsyncStorage(COUPONS_UPDATE_TIMEOUT, currentDateTimeString);
    yield put(setCouponsFetchTimestampState(currentDateTimeString));
  }
}

/**
 * @description - condition to check if we need to pass source as login to fetch latest coupons
 * @param {boolean} isSourceLogin
 */
export function* isCouponsFetchRequired(isSourceLogin) {
  const couponsFetchTime = yield select(getCouponsFetchTimestamp);
  const couponsFetchTimeout = yield select(getCouponsTimeout);

  let couponsUpdateRequired = isSourceLogin;
  if (!isSourceLogin && isMobileApp()) {
    couponsUpdateRequired = yield call(
      isCouponsUpdateRequired,
      couponsFetchTime,
      couponsFetchTimeout
    );
  }
  return couponsUpdateRequired;
}

export function* getAllCoupons({ payload = {} }) {
  const { fromCSH } = payload;
  try {
    if (!fromCSH && !isMobileApp()) yield put(setLoaderState(true));
    yield put(showLoader());
    const { isSourceLogin = false } = payload;

    const couponsFetchRequired = yield call(isCouponsFetchRequired, isSourceLogin);

    const couponCachingTTL = yield select(BagPageSelectors.getCouponCachingTTL);
    const coupons = yield call(getAllCouponsAbstractor, couponsFetchRequired);
    yield put(setCouponList({ coupons, couponCachingTTL }));
    if (!fromCSH) yield put(setLoaderState(false));
    yield call(updateCouponsTimeout, couponsFetchRequired);
    yield put(hideLoader());
  } catch (e) {
    logger.error('getAllCoupons error', e);
    if (!fromCSH) yield put(setLoaderState(false));
    yield put(hideLoader());
  }
}

const getCouponDetailsTrackingObj = (formData, coupon, storeId, isFailed) => {
  const pageSectionName = pageTypeName(formData);
  const events = isFailed ? 'event151' : 'event150';
  const eventNameStr = isFailed ? 'invalid coupon code' : 'valid coupon code';

  return {
    customEvents: [events],
    eventName: eventNameStr,
    couponCode: coupon && coupon.id,
    storeId,
    pageSection: pageSectionName,
    pageType: pageSectionName,
    pageSubSection: pageSectionName,
    clickEvent: true,
    event151: isFailed ? '1' : '',
    event150: isFailed ? '' : '1',
  };
};

export function* getCouponDetails({ payload }) {
  const { formData } = payload;
  const storeId = yield select(getFavoriteStore);
  try {
    yield put(setGetCouponDetailsLoader(true));
    const coupon = yield call(getCouponDetailsAbstractor, formData);
    yield put(setCouponDetails({ coupon }));
    yield put(setGetCouponDetailsLoader(false));
    yield put(setClickAnalyticsData(getCouponDetailsTrackingObj(formData, coupon, storeId)));
    yield put(
      trackClick({
        name: 'can_not_apply_coupon_success',
        module: 'checkout',
      })
    );
  } catch (e) {
    if (e.message && !e.response) {
      yield put(setCouponDetailsError(e.message));
    } else if (
      e?.response?.body?.errorCode &&
      e?.response?.body?.errorCode !== COUPON_SELF_HELP_CONSTANT.EXPIRED_ERR_CODE
    ) {
      yield put(setCouponDetailsError(e.response.body.errorCode));
    } else if (e?.response?.body?.errorCode === COUPON_SELF_HELP_CONSTANT.EXPIRED_ERR_CODE) {
      const coupon = {
        title: e?.response?.body?.description,
        expirationDate: e?.response?.body?.endDate,
      };
      yield put(setCouponDetailsError(e.response.body.errorCode));
      yield put(setCouponDetails({ coupon }));
    } else {
      yield put(setCouponDetailsError('Some Unexpected Error Occured'));
    }
    logger.error('getCouponDetails error', e);
    yield put(setGetCouponDetailsLoader(false));
    yield put(setClickAnalyticsData(getCouponDetailsTrackingObj(formData, null, storeId, true)));

    yield put(
      trackClick({
        name: 'can_not_apply_coupon_fail',
        module: 'checkout',
      })
    );
  }
}

export function* CouponSaga() {
  const cachedAllCoupons = validateReduxCache(getAllCoupons);
  yield takeLatest(COUPON_CONSTANTS.GET_COUPON_LIST, cachedAllCoupons);
  yield takeLatest(COUPON_CONSTANTS.APPLY_COUPON, applyCoupon);
  yield takeLatest(COUPON_CONSTANTS.REMOVE_COUPON, removeCoupon);
  yield takeLatest(COUPON_CONSTANTS.GET_COUPON_DETAILS, getCouponDetails);
}

export default CouponSaga;
