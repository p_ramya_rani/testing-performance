// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import AirmilesToolTip from '../AirmilesToolTip.view';

describe('AirmilesBanner form component', () => {
  const props = {
    toolTipText: 'lables flyout',
  };

  it('should render component correctly', () => {
    const component = shallow(<AirmilesToolTip {...props} />);
    expect(component).toMatchSnapshot();
  });
});
