// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select } from 'redux-saga/effects';
import { getCartItemCount } from '@tcp/core/src/utils/cookie.util';

import { getTotalItemCount } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { trackClick } from '@tcp/core/src/analytics/actions';
import { getCouponsTimeout } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import setLoaderState from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { getCouponsFetchTimestamp } from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.selectors';
import { setStatus } from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import {
  applyCoupon,
  CouponSaga,
  removeCoupon,
  getAllCoupons,
  getCartCounts,
  updateCouponsTimeout,
  isCouponsFetchRequired,
} from '../container/Coupon.saga';
import { applyCouponToCart } from '../../../../../../../services/abstractors/CnC';
import COUPON_ACTION_PATTERN from '../Coupon.constants';
import { hideLoader, showLoader, setCouponList } from '../container/Coupon.actions';

const couponData = {
  id: 'Y00105579',
  status: 'available',
  isExpiring: false,
  title: '$10 off $50 Gymboree ONLY',
  detailsOpen: false,
  expirationDate: '12/31/99',
  effectiveDate: '7/31/19',
  details: null,
  legalText: '$10 off $50 Gymboree ONLY',
  isStarted: true,
  error: '',
  promotionType: 'public',
  expirationDateTimeStamp: '9999-12-31T18:29:5.999Z',
};
jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: () => true,
  setValueInAsyncStorage: jest.fn(),
  isCouponsUpdateRequired: jest.fn(),
}));
describe('Coupon saga', () => {
  it('should dispatch showLoader applyCoupon action', () => {
    const payload = { formPromise: {}, formData: {}, source: '' };
    const applyCouponSaga = applyCoupon({ payload });
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(put(showLoader()));
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(
      call(applyCouponToCart, payload.formData, undefined, undefined)
    );
    expect(applyCouponSaga.next().value).toEqual(put(hideLoader()));
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
  });

  it('should dispatch showLoader with coupon data applyCoupon ', () => {
    const payload = {
      formPromise: {},
      formData: {},
      coupon: couponData,
      source: '',
    };
    const applyCouponSaga = applyCoupon({ payload });
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(put(showLoader()));
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(
      call(applyCouponToCart, payload.formData, undefined, undefined)
    );
    expect(applyCouponSaga.next().value).toEqual(put(hideLoader()));
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(
      put(
        trackClick({
          name: 'coupon_success',
          module: 'checkout',
        })
      )
    );
    expect(applyCouponSaga.next().value).toEqual(
      put(setStatus({ promoCode: couponData.id, status: 'applied' }))
    );
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(put(setLoaderState(false)));
  });

  it('should dispatch showLoader with coupon with error ', () => {
    const payload = {
      formPromise: {},
      formData: {},
      coupon: couponData,
      source: 'data',
    };
    const applyCouponSaga = applyCoupon({ payload });
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(put(showLoader()));
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(
      call(applyCouponToCart, payload.formData, undefined, undefined)
    );
    expect(applyCouponSaga.next().value).toEqual(put(hideLoader()));
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(
      put(
        trackClick({
          name: 'coupon_success',
          module: 'checkout',
        })
      )
    );
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(put(setLoaderState(false)));
  });

  it('should dispatch showLoader with removeCouponOrPromo coupon', () => {
    const payload = {
      formPromise: {},
      formData: {},
      coupon: couponData,
    };
    const removeCouponSaga = removeCoupon({ payload });
    removeCouponSaga.next();
    removeCouponSaga.next();
    expect(removeCouponSaga.next().value).toEqual(put(showLoader()));
    removeCouponSaga.next();
    removeCouponSaga.next();
    removeCouponSaga.next();
    removeCouponSaga.next();
    expect(removeCouponSaga.next().value).toEqual(put(hideLoader()));
    expect(removeCouponSaga.next().value).toEqual(put(setLoaderState(false)));
    removeCouponSaga.next();
    expect(removeCouponSaga.next().value).toEqual(
      put(
        trackClick({
          name: 'coupon_removed',
          module: 'checkout',
        })
      )
    );
  });
  it('should execute with coupon details ', () => {
    const payload = {
      formPromise: {},
      formData: {},
      coupon: couponData,
    };
    const applyCouponSaga = applyCoupon({ payload });
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(put(setLoaderState(true)));
    expect(applyCouponSaga.next().value).toEqual(put(showLoader(true)));
    applyCouponSaga.next();
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(put(hideLoader()));
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(
      put(
        trackClick({
          name: 'coupon_success',
          module: 'checkout',
        })
      )
    );
    applyCouponSaga.next();
    applyCouponSaga.next();
    expect(applyCouponSaga.next().value).toEqual(put(setLoaderState(false)));
  });
  it('should return correct takeLatest applyCoupon effect', () => {
    const takeLatestDescriptor = CouponSaga();
    expect(takeLatestDescriptor.next().value.toString()).toMatch(
      takeLatest(COUPON_ACTION_PATTERN.APPLY_COUPON, applyCoupon).toString()
    );
  });

  it('should return correct takeLatest removeCoupon effect', () => {
    const takeLatestDescriptor = CouponSaga();
    takeLatestDescriptor.next();
    expect(takeLatestDescriptor.next().value.toString()).toMatch(
      takeLatest(COUPON_ACTION_PATTERN.REMOVE_COUPON, removeCoupon).toString()
    );
  });
  it('should return correct takelatest ', () => {
    const takeLatestDescriptor = CouponSaga();
    takeLatestDescriptor.next();
    takeLatestDescriptor.next();
    expect(takeLatestDescriptor.next().value.toString()).toMatch(
      takeLatest(COUPON_ACTION_PATTERN.REMOVE_COUPON, removeCoupon).toString()
    );
    takeLatestDescriptor.next();
    expect(takeLatestDescriptor.next().done).toBeTruthy();
  });

  describe('getAllCoupons', () => {
    let gen;

    beforeEach(() => {
      gen = getAllCoupons({
        isSourceLogin: true,
      });
      gen.next();
    });

    it('should dispatch setCouponList on success', () => {
      gen.next();
      gen.next();
      gen.next();
      const putDescriptor = gen.next({}).value;
      expect(putDescriptor).toEqual(
        put(setCouponList({ couponCachingTTL: undefined, coupons: {} }))
      );
      expect(gen.next().value).toEqual(put(setLoaderState(false)));
      expect(gen.next().value).toEqual(call(updateCouponsTimeout, undefined));
      expect(gen.next().value).toEqual(put(hideLoader()));
    });
  });
  describe('should return cart counts', () => {
    it('should return correct cart count', () => {
      const getCartCountSaga = getCartCounts();
      expect(getCartCountSaga.next().value).toEqual(call(getCartItemCount, true));
      expect(getCartCountSaga.next().value).toEqual(select(getTotalItemCount));
      expect(getCartCountSaga.next().done).toBeTruthy();
    });
  });
  describe('should call updateCouponsTimeout', () => {
    it('updateCouponsTimeout should be executed', () => {
      const updateCouponsTimeoutSaga = updateCouponsTimeout(true);
      expect(updateCouponsTimeoutSaga.next().done).toBeFalsy();
    });
    it('updateCouponsTimeout should not be executed', () => {
      const updateCouponsTimeoutSaga = updateCouponsTimeout(false);
      expect(updateCouponsTimeoutSaga.next().done).toBeTruthy();
    });
  });
  describe('should call isCouponsFetchRequired', () => {
    it('should return couponsUpdateRequired flag value', () => {
      const isCouponsFetchRequiredSaga = isCouponsFetchRequired(false);
      expect(isCouponsFetchRequiredSaga.next().value).toEqual(select(getCouponsFetchTimestamp));
      expect(isCouponsFetchRequiredSaga.next().value).toEqual(select(getCouponsTimeout));
      isCouponsFetchRequiredSaga.next();
      expect(isCouponsFetchRequiredSaga.next().done).toBeTruthy();
    });
  });
});
