// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getAPIConfig, isMobileApp, isGymboree } from '@tcp/core/src/utils/utils';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import PayPalButton from '../organism/PaypalButton';
import bagPageActions from '../../../../BagPage/container/BagPage.actions';
import { ServiceResponseError } from '../../../../../../../utils/errorMessage.util';
import CONSTANTS from '../../../../Checkout/Checkout.constants';
import { getSetIsPaypalPaymentSettings } from '../../../../Checkout/container/Checkout.action';

export class PayPalButtonContainer extends React.PureComponent {
  componentDidMount() {
    const {
      startPaypalNativeCheckoutAction,
      isBillingPage,
      getPayPalSettings,
      isGettingPaypalSettings,
    } = this.props;
    if (isMobileApp() && !getPayPalSettings && !isGettingPaypalSettings)
      startPaypalNativeCheckoutAction(isBillingPage);
  }

  componentWillUnmount() {
    const { payPalWebViewHandle } = this.props;
    if (isMobileApp()) payPalWebViewHandle(false);
  }

  initalizePayPalButton = data => {
    const {
      startPaypalCheckout,
      paypalAuthorizationHandle,
      clearPaypalSettingsWeb,
      isBillingPage,
    } = this.props;

    const { containerId, height, paypalEnv } = data;
    const options = {
      locale: CONSTANTS.PAYPAL_LOCATE,
      style: {
        size: 'responsive',
        color: isBillingPage ? CONSTANTS.PAYPAL_CTA_COLOR.GOLD : CONSTANTS.PAYPAL_CTA_COLOR.DEFAULT,
        shape: isGymboree() ? 'pill' : 'rect',
        label: isBillingPage ? CONSTANTS.CHECKOUT : CONSTANTS.PAYPAL_LABEL,
        tagline: false,
        height,
      },
      funding: {
        disallowed: [window.paypal && window.paypal.FUNDING.CREDIT],
      },
      env: paypalEnv,
      payment: () => {
        return new Promise((resolve, reject) =>
          startPaypalCheckout({ resolve, reject, isBillingPage })
        );
      },
      onAuthorize: paypalAuthorizationHandle,
      onCancel: clearPaypalSettingsWeb,
      onError: error => {
        throw new ServiceResponseError(error);
      },
    };
    window.paypal.Button.render(options, `#${containerId}`);
  };

  render() {
    const {
      isQualifedOrder,
      containerId,
      height,
      navigation,
      getPayPalSettings,
      payPalWebViewHandle,
      paypalAuthorizationHandle,
      clearPaypalSettings,
      setVenmoState,
      isBillingPage,
      closeModal,
      top,
      fullWidth,
      isRenderDone,
      isPaypalLibraryAction,
      isPaypalLibraryRendered,
      showCondensedHeader,
    } = this.props;
    const apiConfigObj = getAPIConfig();
    const { paypalEnv, paypalStaticUrl } = apiConfigObj;
    return (
      <PayPalButton
        isQualifedOrder={isQualifedOrder}
        height={height}
        initalizePayPalButton={this.initalizePayPalButton}
        containerId={containerId}
        navigation={navigation}
        getPayPalSettings={getPayPalSettings}
        payPalWebViewHandle={payPalWebViewHandle}
        paypalAuthorizationHandle={paypalAuthorizationHandle}
        clearPaypalSettings={clearPaypalSettings}
        paypalEnv={paypalEnv}
        paypalStaticUrl={paypalStaticUrl}
        setVenmoState={setVenmoState}
        closeModal={closeModal}
        top={top}
        isBillingPage={isBillingPage}
        fullWidth={fullWidth}
        isRenderDone={isRenderDone}
        isPaypalLibraryAction={isPaypalLibraryAction}
        isPaypalLibraryRendered={isPaypalLibraryRendered}
        showCondensedHeader={showCondensedHeader}
      />
    );
  }
}

// eslint-disable-next-line no-unused-vars
export const mapStateToProps = state => ({
  isQualifedOrder: false,
  bagLoading: BagPageSelector.isBagLoading(state),
  isPaypalLibraryRendered: BagPageSelector.getIsPayPalLibraryRendered(state),
  isGettingPaypalSettings: BagPageSelector.getIsGettingPaypalSettings(state),
});

export const mapDispatchToProps = dispatch => {
  return {
    startPaypalCheckout: payload => {
      dispatch(bagPageActions.startPaypalCheckout(payload));
    },
    paypalAuthorizationHandle: payload => {
      dispatch(bagPageActions.paypalAuthorization(payload));
    },
    clearPaypalSettingsWeb: () => {
      dispatch(getSetIsPaypalPaymentSettings(null));
    },
    clearPaypalSettings: isBillingPage => {
      dispatch(bagPageActions.startPaypalNativeCheckout({ isBillingPage }));
    },
    payPalWebViewHandle: payload => {
      dispatch(bagPageActions.getSetPayPalWebView(payload));
    },
    isRenderDone: payload => {
      dispatch(bagPageActions.isPaypalRenderDone(payload));
    },
    isPaypalLibraryAction: payload => {
      dispatch(bagPageActions.isPaypalLibraryAction(payload));
    },
    startPaypalNativeCheckoutAction: isBillingPage => {
      dispatch(bagPageActions.startPaypalNativeCheckout({ isBillingPage }));
    },
  };
};

PayPalButtonContainer.propTypes = {
  isBillingPage: PropTypes.bool.isRequired,
  getPayPalSettings: PropTypes.func.isRequired,
  payPalWebViewHandle: PropTypes.func.isRequired,
  startPaypalCheckout: PropTypes.func.isRequired,
  paypalAuthorizationHandle: PropTypes.func.isRequired,
  clearPaypalSettingsWeb: PropTypes.func.isRequired,
  isQualifedOrder: PropTypes.bool.isRequired,
  containerId: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  clearPaypalSettings: PropTypes.func.isRequired,
  setVenmoState: PropTypes.func.isRequired,
  closeModal: PropTypes.bool.isRequired,
  top: PropTypes.string.isRequired,
  fullWidth: PropTypes.string.isRequired,
  isRenderDone: PropTypes.bool.isRequired,
  isPaypalLibraryAction: PropTypes.bool.isRequired,
  isPaypalLibraryRendered: PropTypes.bool.isRequired,
  showCondensedHeader: PropTypes.func.isRequired,
  startPaypalNativeCheckoutAction: PropTypes.func.isRequired,
  isGettingPaypalSettings: PropTypes.bool.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PayPalButtonContainer);
