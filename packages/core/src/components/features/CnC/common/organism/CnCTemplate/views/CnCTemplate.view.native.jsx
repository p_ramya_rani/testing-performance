// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import Button from '@tcp/core/src/components/common/atoms/Button';
import AfterPayContainer from '@tcp/core/src/components/common/organisms/AfterPayPayment';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import ConfirmationAccountFormContainer from '@tcp/core/src/components/features/CnC/common/organism/ConfirmationAccountForm';
import { isTCP } from '@tcp/core/src/utils';
import Recommendations from '../../../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import OrderLedgerContainer from '../../OrderLedger';
import CouponAndPromos from '../../CouponAndPromos';
import BonusPointsDays from '../../../../../../common/organisms/BonusPointsDays';
import CardImage from '../../../../../../common/molecules/CardImage';
import {
  ButtonWrapper,
  CheckoutButton,
  BonusPointsWrapper,
  CouponAndPromosWrapper,
  PayPalButtonContainer,
  CnContainer,
  CnContent,
  VenmoPaidContainer,
  VenmoPaidTextContainer,
  ButtonsLoaderWrapper,
  CheckoutButtonDisabled,
} from '../styles/CnCTemplate.style.native';
import {
  LoadingSpinnerWrapper,
  TouchableOpacityComponent,
} from '../../../../../../common/atoms/ButtonRedesign/ButtonRedesign.style.native';
import colors from '../../../../../../../../styles/themes/TCP/colors';
import {
  getShippingLoaderState,
  getbillingToReviewLoaderState,
} from '../../../../Checkout/container/Checkout.selector';
import PersonalizedCoupons from '../../../../Confirmation/organisms/PersonalizedCoupons';
import PayPalButton from '../../PayPalButton';
import VenmoPaymentButton from '../../../../../../common/atoms/VenmoPaymentButton';
import { TIER1 } from '../../../../../../../constants/tiering.constants';
/** The hard coded values are just to show the confirmation template. these will be removed once the components are are in place */

const LoadingSpinner = () => {
  return (
    <LoadingSpinnerWrapper textPadding="10px 5px">
      <ActivityIndicator size="small" color={colors.BLUE} />
    </LoadingSpinnerWrapper>
  );
};

const getPaymentButton = (params) => {
  const {
    btnText,
    onPress,
    footerBody,
    navigation,
    getPayPalSettings,
    isPayPalWebViewEnable,
    showPayPalButton,
    showVenmoSubmit,
    onVenmoSubmit,
    onVenmoError,
    disableAction,
    afterPayInProgress,
    isAfterPaySelected,
    afterPayMinOrderAmount,
    afterPayMaxOrderAmount,
    labelAfterPayThresholdError,
    trackClickAfterPay,
    setAnalyticsTriggered,
    shippingLoaderState,
    billingLoaderState,
  } = params;
  const renderCTA = () => {
    const loaderButtonStyle = { backgroundColor: colors.PRIMARY.DARKBLUE, width: '100%' };
    const borderRadiusValue = isTCP() ? '0' : '19';
    if (shippingLoaderState || billingLoaderState) {
      return (
        <TouchableOpacityComponent
          accessibilityRole="button"
          style={loaderButtonStyle}
          borderRadius={borderRadiusValue}
        >
          <LoadingSpinner />
        </TouchableOpacityComponent>
      );
    }

    return (
      !showPayPalButton &&
      !showVenmoSubmit &&
      !disableAction &&
      !isAfterPaySelected &&
      !afterPayInProgress && (
        <CheckoutButton
          onPress={onPress}
          as={Button}
          color="white"
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize="fs13"
          text={btnText}
          dataLocator="reviewBtn"
          disableAction={disableAction}
        />
      )
    );
  };

  return (
    <ButtonWrapper
      showPayPalButton={showPayPalButton}
      isPayPalWebViewEnable={isPayPalWebViewEnable}
    >
      {showPayPalButton && (
        <PayPalButtonContainer isPayPalWebViewEnable={isPayPalWebViewEnable}>
          <PayPalButton
            getPayPalSettings={getPayPalSettings}
            navigation={navigation}
            isBillingPage
            fullWidth
            setVenmoState={() => {}}
            closeModal={() => {}}
          />
        </PayPalButtonContainer>
      )}
      {showVenmoSubmit && (
        <VenmoPaymentButton onSuccess={onVenmoSubmit} onError={onVenmoError} isVenmoBlueButton />
      )}
      {afterPayInProgress && (
        <AfterPayContainer
          navigation={navigation}
          btnText={btnText}
          afterPayMinOrderAmount={afterPayMinOrderAmount}
          afterPayMaxOrderAmount={afterPayMaxOrderAmount}
          labelAfterPayThresholdError={labelAfterPayThresholdError}
          trackClickAfterPay={trackClickAfterPay}
          setAnalyticsTriggered={setAnalyticsTriggered}
        />
      )}
      {renderCTA()}
      {disableAction && (
        <CheckoutButtonDisabled
          as={Button}
          color="white"
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize="fs13"
          text={btnText}
          dataLocator="reviewBtn"
        />
      )}
      {footerBody}
    </ButtonWrapper>
  );
};

const getVenmoPayment = (venmoPayment, userName) => {
  return (
    <VenmoPaidContainer>
      <VenmoPaidTextContainer>
        <BodyCopy fontWeight="regular" fontFamily="secondary" fontSize="fs16" text="Paid with " />
      </VenmoPaidTextContainer>
      <View>
        <CardImage card={venmoPayment} cardNumber={userName} />
      </View>
    </VenmoPaidContainer>
  );
};

const getBonusPointsSection = (
  isPayPalWebViewEnable,
  isGuest,
  isBonusPointsEnabled,
  bagLoading
) => {
  return (
    <>
      {!isPayPalWebViewEnable && !isGuest && isBonusPointsEnabled && (
        <BonusPointsWrapper>
          <BonusPointsDays isFetchingStatePageLevel={bagLoading} />
        </BonusPointsWrapper>
      )}
    </>
  );
};

export const CnCCommonTemplate = ({
  btnText,
  onPress,
  backLinkText,
  onBackLinkPress,
  footerBody,
  isGuest,
  showAccordian,
  isConfirmationPage,
  pageCategory,
  navigation,
  getPayPalSettings,
  isPayPalWebViewEnable,
  showPayPalButton,
  showVenmoSubmit,
  onVenmoSubmit,
  venmoPayment,
  onVenmoError,
  isBonusPointsEnabled,
  isVenmoPaymentInProgress,
  bagLoading,
  excludedIds,
  partNumber,
  disableAction,
  labels,
  afterPayInProgress,
  isAfterPaySelected,
  afterPayMinOrderAmount,
  afterPayMaxOrderAmount,
  labelAfterPayThresholdError,
  trackClickAfterPay,
  setAnalyticsTriggered,
  quickViewLoader,
  quickViewProductId,
  shippingLoaderState,
  billingLoaderState,
  fireHapticFeedback,
}) => {
  /* eslint-disable */
  const { deviceTier } = navigation.getScreenProps();
  const userName = venmoPayment ? venmoPayment.userName : '';
  const { recommendationheaderLabel = '', recommendationPageRecentlyViewedLabel = '' } = labels;
  /* eslint-enable */
  return (
    <CnContainer isPayPalWebViewEnable={isPayPalWebViewEnable}>
      {!isConfirmationPage ? (
        <CnContent isPayPalWebViewEnable={isPayPalWebViewEnable}>
          {!isPayPalWebViewEnable && (
            <>
              <CouponAndPromosWrapper>
                <CouponAndPromos
                  isCheckout
                  pageCategory={pageCategory}
                  bagLoading={bagLoading}
                  navigation={navigation}
                  isCheckoutFlow
                />
              </CouponAndPromosWrapper>
              <View>
                <OrderLedgerContainer
                  bagLoadingPageLevel={bagLoading}
                  showAccordian={showAccordian}
                  pageCategory={pageCategory}
                  navigation={navigation}
                />
              </View>
            </>
          )}
          {getBonusPointsSection(isPayPalWebViewEnable, isGuest, isBonusPointsEnabled, bagLoading)}
          {!bagLoading ? (
            getPaymentButton({
              btnText,
              onPress,
              backLinkText,
              onBackLinkPress,
              footerBody,
              getPayPalSettings,
              isPayPalWebViewEnable,
              showPayPalButton,
              showVenmoSubmit,
              onVenmoSubmit,
              onVenmoError,
              navigation,
              disableAction,
              afterPayInProgress,
              isAfterPaySelected,
              afterPayMinOrderAmount,
              afterPayMaxOrderAmount,
              labelAfterPayThresholdError,
              trackClickAfterPay,
              setAnalyticsTriggered,
              shippingLoaderState,
              billingLoaderState,
            })
          ) : (
            <ButtonsLoaderWrapper>
              <LoaderSkelton height={34} />
            </ButtonsLoaderWrapper>
          )}
        </CnContent>
      ) : (
        <View>
          <OrderLedgerContainer
            isConfirmationPage={isConfirmationPage}
            pageCategory={pageCategory}
            showAccordian={false}
            venmoPaymentInfo={
              isVenmoPaymentInProgress && venmoPayment && getVenmoPayment(venmoPayment, userName)
            }
          />
          {isGuest && <ConfirmationAccountFormContainer />}
          <PersonalizedCoupons />
          <Recommendations
            navigation={navigation}
            variation="moduleO"
            page={Constants.RECOMMENDATIONS_PAGES_MAPPING.CHECKOUT}
            excludedIds={excludedIds}
            partNumber={partNumber}
            headerLabel={recommendationheaderLabel}
            sequence="1"
            isCardTypeTiles
            productImageWidth={130}
            productImageHeight={161}
            paddings="0px"
            margins="5px 6px 18px"
            quickViewLoader={quickViewLoader}
            quickViewProductId={quickViewProductId}
            fireHapticFeedback={fireHapticFeedback}
          />
          {deviceTier === TIER1 && (
            <Recommendations
              page={Constants.RECOMMENDATIONS_PAGES_MAPPING.CHECKOUT}
              portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
              variations="moduleO"
              excludedIds={excludedIds}
              partNumber={partNumber}
              headerLabel={recommendationPageRecentlyViewedLabel}
              sequence="2"
              isCardTypeTiles
              productImageWidth={130}
              productImageHeight={161}
              paddings="0px"
              margins="5px 6px 18px"
              quickViewLoader={quickViewLoader}
              quickViewProductId={quickViewProductId}
              fireHapticFeedback={fireHapticFeedback}
            />
          )}
        </View>
      )}
    </CnContainer>
  );
};
CnCCommonTemplate.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  footerBody: PropTypes.shape({}).isRequired,
  btnText: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  backLinkText: PropTypes.string.isRequired,
  onBackLinkPress: PropTypes.func.isRequired,
  isGuest: PropTypes.func.isRequired,
  showAccordian: PropTypes.bool.isRequired,
  isConfirmationPage: PropTypes.bool,
  pageCategory: PropTypes.shape({}),
  getPayPalSettings: PropTypes.shape({}),
  isPayPalWebViewEnable: PropTypes.bool,
  showPayPalButton: PropTypes.bool,
  showVenmoSubmit: PropTypes.bool,
  onVenmoSubmit: PropTypes.func,
  venmoPayment: PropTypes.shape({}),
  onVenmoError: PropTypes.bool,
  isVenmoPaymentInProgress: PropTypes.bool,
  isBonusPointsEnabled: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  excludedIds: PropTypes.string,
  partNumber: PropTypes.string,
  disableAction: PropTypes.bool,
  labels: PropTypes.shape({}),
  afterPayInProgress: PropTypes.bool,
  isAfterPaySelected: PropTypes.bool,
  afterPayMinOrderAmount: PropTypes.number,
  afterPayMaxOrderAmount: PropTypes.number,
  labelAfterPayThresholdError: PropTypes.string,
  trackClickAfterPay: PropTypes.func,
  setAnalyticsTriggered: PropTypes.func,
  quickViewLoader: PropTypes.bool,
  quickViewProductId: PropTypes.string,
  shippingLoaderState: PropTypes.bool,
  billingLoaderState: PropTypes.bool,
};

CnCCommonTemplate.defaultProps = {
  isConfirmationPage: false,
  pageCategory: {},
  getPayPalSettings: {},
  isPayPalWebViewEnable: false,
  showPayPalButton: false,
  showVenmoSubmit: false,
  onVenmoSubmit: () => {},
  venmoPayment: null,
  onVenmoError: false,
  isVenmoPaymentInProgress: false,
  excludedIds: '',
  partNumber: '',
  disableAction: false,
  labels: {},
  afterPayInProgress: false,
  isAfterPaySelected: false,
  afterPayMinOrderAmount: 1,
  afterPayMaxOrderAmount: 1000,
  labelAfterPayThresholdError: '',
  trackClickAfterPay: () => {},
  setAnalyticsTriggered: () => {},
  quickViewLoader: false,
  quickViewProductId: '',
  shippingLoaderState: false,
  billingLoaderState: false,
};

const mapStateToProps = (state) => {
  return {
    shippingLoaderState: getShippingLoaderState(state),
    billingLoaderState: getbillingToReviewLoaderState(state),
  };
};
export default connect(mapStateToProps)(CnCCommonTemplate);
