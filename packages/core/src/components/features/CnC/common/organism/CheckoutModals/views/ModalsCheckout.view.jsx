// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLoginSuccessPayload } from '@tcp/core/src/constants/analytics';
import { setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import withStyles from '../../../../../../common/hoc/withStyles';
import OpenLoginModal from '../../../../../account/LoginPage/views/LoginModal';
import style from '../../../../AddedToBagActions/styles/AddedToBagActions.style';
import BagConfirmationModal from '../../../../BagPage/views/BagConfirmationModal.view';
import ItemDeleteConfirmationModal from '../../../../BagPage/views/ItemDeleteConfirmationModal.view';

class ModalsCheckout extends React.PureComponent {
  routeToCheckout = (e) => {
    const {
      routeForBagCheckout,
      closeCheckoutModalMountState,
      closeMiniBagDispatch,
      setBagPageIsRouting,
    } = this.props;

    /* istanbul ignore else */
    if (e) {
      e.preventDefault();
    }
    closeMiniBagDispatch(false, false);
    closeCheckoutModalMountState({ state: false });
    setBagPageIsRouting();
    setClickAnalyticsData(getLoginSuccessPayload('checkout-popup'));
    routeForBagCheckout();
  };

  closeModalAndHandleCheckout = () => {
    const { closeCheckoutConfirmationModal, handleCartCheckout } = this.props;
    closeCheckoutConfirmationModal();
    return handleCartCheckout();
  };

  closeOOSModalAndHandleCheckout = () => {
    const { closeCheckoutConfirmationModal, setPayPalModalActionOOS } = this.props;
    closeCheckoutConfirmationModal();
    setPayPalModalActionOOS(false);
  };

  render() {
    const {
      labels,
      checkoutModalMountedState,
      closeCheckoutModalMountState,
      modalInfo,
      removeUnqualifiedItemsAndCheckout,
      currentSelectItemInfo,
      closeItemDeleteModal,
      deleteConfirmationModalLabels,
      confirmRemoveCartItem,
      addItemToSflList,
      bagPageServerError,
      cartItemSflError,
      checkoutModalComponentType,
      isPayPalModalActionOOS,
    } = this.props;
    const { showModal, isEditingItem: modalEditingItem } = modalInfo;
    if (modalEditingItem) {
      labels.confirmationText = labels.editConfirmationText;
    }
    return (
      <>
        <BagConfirmationModal
          labels={labels}
          isOpen={showModal}
          closeCheckoutConfirmationModal={this.closeOOSModalAndHandleCheckout}
          removeUnqualifiedItemsAndCheckout={
            modalEditingItem ? this.closeModalAndHandleCheckout : removeUnqualifiedItemsAndCheckout
          }
          isPayPalModalActionOOS={isPayPalModalActionOOS}
        />
        {checkoutModalMountedState && (
          <OpenLoginModal
            variation="checkout"
            openState={checkoutModalMountedState}
            setLoginModalMountState={closeCheckoutModalMountState}
            handleContinueAsGuest={this.routeToCheckout}
            handleAfterLogin={this.routeToCheckout}
            componentType={checkoutModalComponentType}
          />
        )}
        <ItemDeleteConfirmationModal
          isOpen={currentSelectItemInfo.showModal}
          closeCheckoutConfirmationModal={closeItemDeleteModal}
          labels={deleteConfirmationModalLabels}
          moveToSfl={() => addItemToSflList(currentSelectItemInfo)}
          confirmRemoveCartItem={() => confirmRemoveCartItem(currentSelectItemInfo.itemId)}
          bagPageServerError={bagPageServerError}
          cartItemSflError={cartItemSflError}
        />
      </>
    );
  }
}

ModalsCheckout.propTypes = {
  labels: PropTypes.shape.isRequired,
  handleCartCheckout: PropTypes.func.isRequired,
  routeForBagCheckout: PropTypes.func.isRequired,
  bagPageServerError: PropTypes.shape({}),
  cartItemSflError: PropTypes.string,
  setPayPalModalActionOOS: PropTypes.func,
  isPayPalModalActionOOS: PropTypes.bool,
  navigation: PropTypes.shape({}).isRequired,
  closeCheckoutModalMountState: PropTypes.func.isRequired,
  closeCheckoutConfirmationModal: PropTypes.func.isRequired,
  checkoutModalMountedState: PropTypes.bool.isRequired,
  removeUnqualifiedItemsAndCheckout: PropTypes.func.isRequired,
  modalInfo: PropTypes.shape({}).isRequired,
  currentSelectItemInfo: PropTypes.shape({}).isRequired,
  closeItemDeleteModal: PropTypes.func.isRequired,
  deleteConfirmationModalLabels: PropTypes.shape({}).isRequired,
  addItemToSflList: PropTypes.func.isRequired,
  confirmRemoveCartItem: PropTypes.func.isRequired,
  checkoutModalComponentType: PropTypes.string.isRequired,
  closeMiniBagDispatch: PropTypes.func.isRequired,
  setBagPageIsRouting: PropTypes.bool.isRequired,
};

ModalsCheckout.defaultProps = {
  bagPageServerError: null,
  cartItemSflError: '',
  isPayPalModalActionOOS: false,
  setPayPalModalActionOOS: () => {},
};

export default withStyles(ModalsCheckout, style);
export { ModalsCheckout as ModalsCheckoutVanilla };
