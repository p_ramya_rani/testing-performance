// 9fbef606107a605d69c0edbcd8029e5d
import COUPON_CONSTANTS from '../Coupon.constants';

export const applyCoupon = (payload) => {
  return {
    type: COUPON_CONSTANTS.APPLY_COUPON,
    payload,
  };
};

export const showLoader = () => ({
  type: COUPON_CONSTANTS.SHOW_LOADER,
});

export const hideLoader = () => ({
  type: COUPON_CONSTANTS.HIDE_LOADER,
});

export const getCouponList = (payload) => {
  return {
    type: COUPON_CONSTANTS.GET_COUPON_LIST,
    payload,
  };
};

export const getCouponDetails = (payload) => {
  return {
    type: COUPON_CONSTANTS.GET_COUPON_DETAILS,
    payload,
  };
};

export const setCouponDetailsError = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_COUPON_DETAILS_ERROR,
    payload,
  };
};

export const setGetCouponDetailsLoader = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_COUPON_DETAILS_LOADER,
    payload,
  };
};

export const setApplyCouponCSHLoader = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_APPLY_COUPON_CSH_LOADER,
    payload,
  };
};

export const setApplyCouponCSHError = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_APPLY_COUPON_CSH_ERROR,
    payload,
  };
};

export const resetCouponDetails = () => {
  return {
    type: COUPON_CONSTANTS.RESET_COUPON_DETAILS,
  };
};

export const setCouponDetails = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_COUPON_DETAILS,
    payload,
  };
};

export const setCouponList = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_COUPON_LIST,
    payload,
  };
};

export const removeCoupon = (payload) => {
  return {
    type: COUPON_CONSTANTS.REMOVE_COUPON,
    payload,
  };
};

export const setStatus = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_STATUS_COUPON,
    payload,
  };
};

export const setError = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_ERROR,
    payload,
  };
};

export const toggleNeedHelpModalState = () => {
  return {
    type: COUPON_CONSTANTS.TOGGLE_NEED_HELP_MODAL_STATE,
  };
};

export const setNeedHelpModalState = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_NEED_HELP_MODAL_STATE,
    payload,
  };
};

export const setCouponsFetchTimestampState = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_COUPONS_FETCH_TIMESTAMP,
    payload,
  };
};

export const clearCouponTTL = () => {
  return {
    type: COUPON_CONSTANTS.CLEAR_COUPON_TTL,
  };
};

export const resetWalletAppState = () => {
  return {
    type: COUPON_CONSTANTS.RESET_COUPON_STATE,
  };
};

/**
 * @function resetCouponReducer
 * action creator for type: RESET_COUPON_REDUCER
 */
export const resetCouponReducer = () => {
  return {
    type: COUPON_CONSTANTS.RESET_COUPON_REDUCER,
  };
};

export const setOnHoldCoupon = (payload) => {
  return {
    type: COUPON_CONSTANTS.SET_ONHOLD_COUPON,
    payload,
  };
};
