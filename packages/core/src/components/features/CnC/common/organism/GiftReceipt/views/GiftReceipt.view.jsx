// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shape, string, number } from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import {
  getLabelValue,
  getFormatedOrderDate,
  addDays,
  getIconPath,
  convertToISOString,
} from '@tcp/core/src/utils/utils';
import { isMobileWeb } from '@tcp/core/src/utils';
import styles from '../styles/GiftReceipt.style';

class GiftReceipt extends React.PureComponent {
  getReceiptHTML = ({
    orderNumbers,
    returnDays,
    orderDate,
    giftReceiptHeading,
    giftReceiptReturnBy,
    orderNumberHeading,
    returnByUpdate,
  }) => {
    const orderNumber = orderNumbers[0];
    return (
      <BodyCopy component="div" className="inner-square">
        <BodyCopy component="div" className="brand-logo">
          <Image alt="TCP" className="brand-image" src={getIconPath(`tcp-gym-black-logo`)} />
        </BodyCopy>

        <BodyCopy
          component="h1"
          fontSize="fs16"
          className="return-heading-box"
          fontWeight="800"
          fontFamily="primary"
        >
          {giftReceiptHeading}
        </BodyCopy>

        <BodyCopy component="div" className="bar-code" aria-hidden>
          <Barcode
            value={orderNumber}
            barcodeId={orderNumber}
            displayValue={false}
            height="28"
            renderer="svg"
            width="3.5"
          />
        </BodyCopy>

        {returnDays ? (
          <BodyCopy
            component="div"
            fontSize="fs14"
            fontWeight="extrabold"
            fontFamily="secondary"
            className="return-by"
          >
            {` ${giftReceiptReturnBy}: ${getFormatedOrderDate(
              addDays(new Date(convertToISOString(orderDate)), parseInt(returnDays, 10)),
              true
            )}`}
          </BodyCopy>
        ) : (
          <BodyCopy
            component="div"
            fontSize="fs14"
            fontFamily="secondary"
            className="return-update"
          >
            {returnByUpdate}
          </BodyCopy>
        )}

        <div className="order-summary-section">
          <BodyCopy
            component="div"
            fontSize="fs14"
            fontWeight="extrabold"
            fontFamily="secondary"
            className="order-number-heading"
          >
            {`${orderNumberHeading}:`}
          </BodyCopy>
          <BodyCopy component="div" fontSize="fs14" fontFamily="secondary" className="order-number">
            {` ${orderNumber}`}
          </BodyCopy>
        </div>
      </BodyCopy>
    );
  };

  render() {
    const { orderNumbers, giftReceiptLabels, className, returnDays, orderDate } = this.props;
    const giftReceiptHeading = getLabelValue(giftReceiptLabels, 'lbl_print_gift_receipt_heading');
    const giftReceiptReturnBy = getLabelValue(
      giftReceiptLabels,
      'lbl_print_gift_receipt_return_by'
    );
    const orderNumberHeading = getLabelValue(
      giftReceiptLabels,
      'lbl_print_gift_receipt_order_number'
    );
    const returnByUpdate = getLabelValue(giftReceiptLabels, 'lbl_print_gift_receipt_return_update');

    const receiptHTML = this.getReceiptHTML({
      orderNumbers,
      returnDays,
      orderDate,
      giftReceiptHeading,
      giftReceiptReturnBy,
      orderNumberHeading,
      returnByUpdate,
    });
    const mobileWeb = isMobileWeb();
    return (
      <BodyCopy component="div" className={className}>
        {mobileWeb && !returnDays ? (
          <BodyCopy component="div" className="gift-receipt-split ">
            <BodyCopy component="div" className="outer-square">
              {receiptHTML}
              {receiptHTML}
            </BodyCopy>
            <BodyCopy component="div" className="outer-square outer-square-2">
              {receiptHTML}
            </BodyCopy>
          </BodyCopy>
        ) : (
          <BodyCopy component="div" className="outer-square">
            <BodyCopy component="div">{receiptHTML}</BodyCopy>
            <BodyCopy component="div">{receiptHTML}</BodyCopy>
            <BodyCopy component="div">{receiptHTML}</BodyCopy>
          </BodyCopy>
        )}
      </BodyCopy>
    );
  }
}

GiftReceipt.propTypes = {
  giftReceiptLabels: shape({}),
  orderNumbers: shape([]).isRequired,
  className: string,
  returnDays: number,
  orderDate: string,
};

GiftReceipt.defaultProps = {
  giftReceiptLabels: {},
  className: '',
  returnDays: 0,
  orderDate: '',
};

export default withStyles(GiftReceipt, styles);
export { GiftReceipt as GiftReceiptVanilla };
