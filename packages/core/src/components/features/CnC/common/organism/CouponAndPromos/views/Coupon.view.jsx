// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import ErrorMessage from '@tcp/core/src/components/features/CnC/common/molecules/ErrorMessage';
import { scrollToElement } from '@tcp/core/src/utils';
import { PROMOTION_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import CouponSkeleton from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/skeleton/CouponSkeleton.view';
import withStyles from '../../../../../../common/hoc/withStyles';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import Col from '../../../../../../common/atoms/Col';
import CouponListSection from '../../../../../../common/organisms/CouponListSection';
import CouponDetailModal from './CouponDetailModal.view';
import CouponForm from '../../../molecules/CouponForm';
import { styles } from '../styles/Coupon.style';
import CollapsibleContainer from '../../../../../../common/molecules/CollapsibleContainer';

class CouponView extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      detailStatus: false,
      placeCashStatus: false,
      selectedCoupon: {},
    };
    this.appliedCouponError = null;
    this.appliedCouponRef = this.appliedCouponRef.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { appliedExpiredCoupon, isFetching, bagLoading } = this.props;
    if (
      appliedExpiredCoupon.size > 0 &&
      (appliedExpiredCoupon.size !== prevProps.appliedExpiredCoupon.size ||
        isFetching !== prevProps.isFetching ||
        bagLoading !== prevProps.bagLoading)
    ) {
      scrollToElement(this.appliedCouponError);
    }
  }

  couponDetailClick = (coupon) => {
    this.setState({
      detailStatus: true,
      selectedCoupon: coupon,
    });
  };

  couponPlaceCashClick = () => {
    const { labels, fetchNeedHelpContent, placeCashContentId } = this.props;
    if (!labels.PLACE_CASH_CONTENT) {
      fetchNeedHelpContent([placeCashContentId]);
    }
    this.setState({
      placeCashStatus: true,
    });
  };

  getHeader = ({ labels }) => {
    return (
      <div className="elem-mb-SM rewards-header">
        <BodyCopy fontFamily="secondary" fontSize="fs16" fontWeight="semibold" component="span">
          {labels.couponCollapsibleHeader}
        </BodyCopy>
      </div>
    );
  };

  getAvailableCouponList = ({
    bagLoading,
    labels,
    availableCouponList,
    handleApplyCouponFromList,
    isFetching,
    handleErrorCoupon,
    additionalClassName,
    newBag,
  }) => {
    if (!bagLoading) {
      return (
        availableCouponList &&
        availableCouponList.size > 0 && (
          <CouponListSection
            labels={labels}
            isFetching={isFetching}
            couponList={availableCouponList}
            className="available_coupon"
            heading={labels.AVAILABLE_REWARDS_HEADING}
            helpSubHeading="true"
            couponDetailClick={this.couponDetailClick}
            helpAnchorClick={this.couponPlaceCashClick}
            onApply={handleApplyCouponFromList}
            dataLocator="coupon-cartAvaliableRewards"
            handleErrorCoupon={handleErrorCoupon}
            additionalClassNameModal={additionalClassName}
            newBag={newBag}
          />
        )
      );
    }

    return (
      <CouponSkeleton
        className
        heading={labels.AVAILABLE_REWARDS_HEADING}
        couponList={availableCouponList}
        labels={labels}
      />
    );
  };

  couponExpiredError = (appliedExpiredCoupon, labels) => {
    return (
      <>
        {appliedExpiredCoupon.map((coupon, i) => {
          if (i === 0) {
            const { id } = coupon;
            const errorMessage = labels.expiredCouponErrorMessage.replace('coupon_placeholder', id);
            return <ErrorMessage error={errorMessage} className="expired-coupon-error" />;
          }
          return null;
        })}
      </>
    );
  };

  showCouponConatiner = (availableCouponList, appliedCouponList, onHoldCouponList) =>
    (availableCouponList && availableCouponList.size > 0) ||
    (appliedCouponList && appliedCouponList.size > 0) ||
    (onHoldCouponList && onHoldCouponList.size > 0);

  getContent = ({
    isFetching,
    labels,
    handleApplyCoupon,
    handleApplyCouponFromList,
    appliedCouponList,
    availableCouponList,
    bagLoading,
    className,
    handleRemoveCoupon,
    handleErrorCoupon,
    detailStatus,
    selectedCoupon,
    additionalClassName,
    idPrefix,
    appliedExpiredCoupon,
    showCouponMergeError,
    onHoldCouponList,
    newBag,
  }) => {
    return (
      <div className={className} ref={this.appliedCouponRef}>
        <CouponForm
          onSubmit={handleApplyCoupon}
          isFetching={isFetching}
          source="form"
          labels={labels}
          additionalClassNameModal={additionalClassName}
          idPrefix={idPrefix}
          coupon={selectedCoupon}
          showCouponMergeError={showCouponMergeError}
          newBag={newBag}
        />
        <div
          className={`coupon_list ${
            newBag &&
            this.showCouponConatiner(availableCouponList, appliedCouponList, onHoldCouponList)
              ? 'rounded-container elem-pt-SM elem-pb-SM elem-pl-SM elem-pr-SM'
              : ''
          }`}
        >
          {appliedExpiredCoupon && appliedExpiredCoupon.size > 0 && (
            <div className="elem-mt-MED">
              {this.couponExpiredError(appliedExpiredCoupon, labels)}
            </div>
          )}
          {appliedCouponList && appliedCouponList.size > 0 && (
            <CouponListSection
              labels={labels}
              isFetching={isFetching}
              couponList={appliedCouponList}
              className="applied_coupon"
              heading={labels.APPLIED_REWARDS_HEADING}
              couponDetailClick={this.couponDetailClick}
              onRemove={handleRemoveCoupon}
              dataLocator="coupon-cartAppliedRewards"
              handleErrorCoupon={handleErrorCoupon}
              additionalClassNameModal={additionalClassName}
              showFullList
              newBag={newBag}
            />
          )}
          {onHoldCouponList && onHoldCouponList.size > 0 && (
            <CouponListSection
              labels={labels}
              isFetching={isFetching}
              couponList={onHoldCouponList}
              className="applied_coupon"
              heading={labels.onHoldHeading}
              couponDetailClick={this.couponDetailClick}
              onRemove={handleRemoveCoupon}
              dataLocator="coupon-cartAppliedRewards"
              handleErrorCoupon={handleErrorCoupon}
              additionalClassNameModal={additionalClassName}
              showFullList
              couponsOnHold
              newBag={newBag}
            />
          )}
          {this.getAvailableCouponList({
            bagLoading,
            labels,
            availableCouponList,
            handleApplyCouponFromList,
            isFetching,
            handleErrorCoupon,
            additionalClassName,
            newBag,
          })}
          {/* UX timer for coupons list visibility */}
          <RenderPerf.Measure name={PROMOTION_VISIBLE} />
          <CouponDetailModal
            labels={labels}
            openState={detailStatus}
            coupon={selectedCoupon}
            onRequestClose={() => {
              this.setState({
                detailStatus: false,
              });
            }}
            onApplyCouponToBagFromList={handleApplyCouponFromList}
            additionalClassNameModal={additionalClassName}
          />
        </div>
      </div>
    );
  };

  appliedCouponRef(ref) {
    this.appliedCouponError = ref;
  }

  render() {
    const {
      isFetching,
      labels,
      handleApplyCoupon,
      handleApplyCouponFromList,
      appliedCouponList,
      availableCouponList,
      className,
      handleRemoveCoupon,
      handleErrorCoupon,
      showAccordian,
      additionalClassNameModal,
      idPrefix,
      bagLoading,
      isNeedHelpModalOpen,
      appliedExpiredCoupon,
      showCouponMergeError,
      onHoldCouponList,
      newBag,
    } = this.props;
    const { detailStatus, selectedCoupon, placeCashStatus } = this.state;
    const header = this.getHeader({ labels });
    const body = (additionalClassName) =>
      this.getContent({
        isFetching,
        labels,
        handleApplyCoupon,
        handleApplyCouponFromList,
        appliedCouponList,
        availableCouponList,
        className,
        bagLoading,
        handleRemoveCoupon,
        handleErrorCoupon,
        detailStatus,
        placeCashStatus,
        selectedCoupon,
        additionalClassName,
        idPrefix,
        isNeedHelpModalOpen,
        appliedExpiredCoupon,
        showCouponMergeError,
        onHoldCouponList,
        newBag,
      });
    const defaultOpen = availableCouponList && availableCouponList.size > 0;
    return (
      <div className={className}>
        <Col
          colSize={{
            large: 12,
            medium: 8,
            small: 6,
          }}
          ignoreGutter={{ small: true }}
          className="coupon-sec"
        >
          <CollapsibleContainer
            className={`${className} ${showAccordian ? 'couponsWrapperAccordian' : ''}`}
            header={header}
            body={body(additionalClassNameModal)}
            iconLocator="arrowicon"
            defaultOpen={defaultOpen}
            isDefaultView={!showAccordian}
          />
        </Col>
      </div>
    );
  }
}

CouponView.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  isFetching: PropTypes.bool.isRequired,
  handleApplyCoupon: PropTypes.func.isRequired,
  handleRemoveCoupon: PropTypes.func.isRequired,
  appliedCouponList: PropTypes.shape([]).isRequired,
  availableCouponList: PropTypes.shape([]).isRequired,
  additionalClassNameModal: PropTypes.string.isRequired,
  handleErrorCoupon: PropTypes.func.isRequired,
  showAccordian: PropTypes.bool.isRequired,
  toggleNeedHelpModal: PropTypes.func.isRequired,
  isNeedHelpModalOpen: PropTypes.bool.isRequired,
  fetchNeedHelpContent: PropTypes.func.isRequired,
  placeCashContentId: PropTypes.shape({}).isRequired,
  needHelpContentId: PropTypes.shape({}).isRequired,
  handleApplyCouponFromList: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  idPrefix: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  appliedExpiredCoupon: PropTypes.shape({ size: PropTypes.number }),
  showCouponMergeError: PropTypes.bool,
  onHoldCouponList: PropTypes.shape([]),
  newBag: PropTypes.bool,
};

CouponView.defaultProps = {
  appliedExpiredCoupon: [],
  showCouponMergeError: false,
  onHoldCouponList: [],
  newBag: false,
};

export default withStyles(CouponView, styles);
export { CouponView as CouponViewVanilla };
