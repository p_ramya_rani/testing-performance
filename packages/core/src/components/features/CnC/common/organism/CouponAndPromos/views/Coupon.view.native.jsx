// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes, { bool, func, string } from 'prop-types';
import { View } from 'react-native';
import withStyles from '../../../../../../common/hoc/withStyles';
import {
  styles,
  WrapperStyle,
  CouponListContainer,
  StyledHeader,
} from '../styles/Coupon.style.native';
import CouponForm from '../../../molecules/CouponForm';
import CouponListSection from '../../../../../../common/organisms/CouponListSection';
import CouponHelpModal from './CouponHelpModal.view';
import CouponDetailModal from './CouponDetailModal.view';
import CollapsibleContainer from '../../../../../../common/molecules/CollapsibleContainer';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

class CouponView extends React.PureComponent {
  state = {
    detailStatus: false,
    placeCashStatus: false,
    selectedCoupon: {},
    couponOffset: 0,
    onApplyRemove: false,
  };

  /*
    this will trigger scroll after applying or removing any coupons
  */
  componentDidUpdate(prevProps, prevState) {
    const { onApplyRemove: onApplyRemovePrev } = prevState;
    const { onApplyRemove } = this.state;
    if (onApplyRemovePrev !== onApplyRemove && onApplyRemove === true) {
      this.measurePosition();
      this.onApplyRemove(false);
    }
  }

  couponDetailClick = coupon => {
    this.setState({
      detailStatus: true,
      selectedCoupon: coupon,
    });
  };

  couponPlaceCashClick = () => {
    const { labels, fetchNeedHelpContent, placeCashContentId } = this.props;
    if (!labels.PLACE_CASH_CONTENT) {
      fetchNeedHelpContent([placeCashContentId]);
    }
    this.setState({
      placeCashStatus: true,
    });
  };

  toggleCouponNeedHelpModal = ({ status } = {}) => {
    const { toggleNeedHelpModal, labels, fetchNeedHelpContent, needHelpContentId } = this.props;
    if (status === 'open' && !labels.NEED_HELP_RICH_TEXT) {
      fetchNeedHelpContent([needHelpContentId]);
    }
    toggleNeedHelpModal();
  };

  onApply = data => {
    const { handleApplyCouponFromList } = this.props;
    handleApplyCouponFromList(data);
    this.onApplyRemove(true);
  };

  onRemove = data => {
    const { handleRemoveCoupon } = this.props;
    handleRemoveCoupon(data);
    this.onApplyRemove(true);
  };

  /*
    will update the state after clicking on apply or remove button
  */
  onApplyRemove = status => {
    this.setState({
      onApplyRemove: status,
    });
  };

  /*
    call back function will trigger scrolling
  */
  measurePosition = () => {
    const { measurePositionHandler, appliedCouponList, availableCouponList } = this.props;
    const { couponOffset } = this.state;
    if (appliedCouponList.size > 0 || availableCouponList.size > 0) {
      setTimeout(() => {
        measurePositionHandler({ couponOffset });
      }, 1000);
    }
  };

  getHeader = ({ labels }) => {
    return (
      <StyledHeader>
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs16"
          fontWeight="semibold"
          component="span"
          text={labels.couponCollapsibleHeader}
        />
      </StyledHeader>
    );
  };

  getContent = ({
    isFetching,
    labels,
    handleApplyCoupon,
    handleApplyCouponFromList,
    appliedCouponList,
    availableCouponList,
    handleErrorCoupon,
    detailStatus,
    placeCashStatus,
    selectedCoupon,
    showAccordian,
    showCouponMergeError,
    currentUserId,
    setMaxItemErrorDisplayedAction,
    showMaxItemErrorDisplayed,
    isNeedHelpModalOpen,
    toastMessage,
    onHoldCouponList,
  }) => {
    return (
      <WrapperStyle>
        <CouponForm
          onSubmit={handleApplyCoupon}
          isFetching={isFetching}
          source="form"
          labels={labels}
          currentUserId={currentUserId}
          showMaxItemErrorDisplayed={showMaxItemErrorDisplayed}
          setMaxItemErrorDisplayedAction={setMaxItemErrorDisplayedAction}
          showCouponMergeError={showCouponMergeError}
          onNeedHelpTextClick={this.toggleCouponNeedHelpModal}
          showAccordian={showAccordian}
        />
        <CouponListContainer>
          {appliedCouponList && appliedCouponList.size > 0 && (
            <CouponListSection
              labels={labels}
              isFetching={isFetching}
              couponList={appliedCouponList}
              className="applied_coupon"
              heading={labels.APPLIED_REWARDS_HEADING}
              couponDetailClick={this.couponDetailClick}
              onRemove={this.onRemove}
              dataLocator="coupon-cartAppliedRewards"
              handleErrorCoupon={handleErrorCoupon}
              showFullList
            />
          )}
          {onHoldCouponList && onHoldCouponList.size > 0 && (
            <CouponListSection
              labels={labels}
              isFetching={isFetching}
              couponList={onHoldCouponList}
              className="available_coupon"
              heading={labels.onHoldHeading}
              couponDetailClick={this.couponDetailClick}
              onRemove={this.onRemove}
              dataLocator="coupon-cartAvaliableRewards"
              handleErrorCoupon={handleErrorCoupon}
              toastMessage={toastMessage}
              couponsOnHold
            />
          )}
          {availableCouponList && availableCouponList.size > 0 && (
            <CouponListSection
              labels={labels}
              isFetching={isFetching}
              couponList={availableCouponList}
              className="available_coupon"
              heading={labels.AVAILABLE_REWARDS_HEADING}
              helpSubHeading="true"
              couponDetailClick={this.couponDetailClick}
              helpAnchorClick={this.couponPlaceCashClick}
              onApply={this.onApply}
              dataLocator="coupon-cartAvaliableRewards"
              handleErrorCoupon={handleErrorCoupon}
              toastMessage={toastMessage}
            />
          )}
        </CouponListContainer>
        <CouponDetailModal
          labels={labels}
          openState={detailStatus}
          coupon={selectedCoupon}
          onRequestClose={() => {
            this.setState({
              detailStatus: false,
            });
          }}
          onApplyCouponToBagFromList={handleApplyCouponFromList}
        />
        {isNeedHelpModalOpen && (
          <CouponHelpModal
            labels={labels}
            openState={isNeedHelpModalOpen}
            horizontalBar={false}
            onRequestClose={this.toggleCouponNeedHelpModal}
          />
        )}
        {placeCashStatus && (
          <CouponHelpModal
            labels={labels}
            openState={placeCashStatus}
            isPlaceCashStatus={placeCashStatus}
            horizontalBar={false}
            onRequestClose={() => {
              this.setState({
                placeCashStatus: false,
              });
            }}
          />
        )}
      </WrapperStyle>
    );
  };

  /**
   * @description coupon expired toast message
   * @param {string} expiredMessage
   */
  couponsExpiredMessage = expiredMessage => {
    const { toastMessage } = this.props;
    toastMessage(expiredMessage);
  };

  couponExpiredError = (appliedExpiredCoupon, labels) => {
    return labels.expiredCouponErrorMessage
      ? labels.expiredCouponErrorMessage.replace(
          'coupon_placeholder',
          appliedExpiredCoupon.get(0).id
        )
      : '';
  };

  handleLayoutChange = () => {
    const {
      appliedCouponList,
      availableCouponList,
      labels,
      appliedExpiredCoupon,
      showExpiredMessage,
    } = this.props;
    const { couponOffset } = this.state;
    if ((appliedCouponList.size > 0 || availableCouponList.size > 0) && couponOffset === 0) {
      this.CouponListComponent.measure((fx, fy, width, height, px, py) => {
        this.setState({
          couponOffset: height + py,
        });
      });

      // Show expired coupon message to the user as toast
      if (showExpiredMessage && appliedExpiredCoupon && appliedExpiredCoupon.size) {
        const expiredMessage = this.couponExpiredError(appliedExpiredCoupon, labels);
        if (expiredMessage) {
          this.couponsExpiredMessage(expiredMessage);
        }
      }
    }
  };

  render() {
    const {
      isFetching,
      labels,
      handleApplyCoupon,
      handleApplyCouponFromList,
      appliedCouponList,
      availableCouponList,
      handleRemoveCoupon,
      handleErrorCoupon,
      isCheckout,
      showAccordian,
      showCouponMergeError,
      currentUserId,
      setMaxItemErrorDisplayedAction,
      isNeedHelpModalOpen,
      showMaxItemErrorDisplayed,
      toastMessage,
      onHoldCouponList,
    } = this.props;
    const { detailStatus, selectedCoupon, placeCashStatus } = this.state;

    const header = this.getHeader({ labels });
    const body = this.getContent({
      isFetching,
      labels,
      handleApplyCoupon,
      handleApplyCouponFromList,
      appliedCouponList,
      availableCouponList,
      handleRemoveCoupon,
      handleErrorCoupon,
      isCheckout,
      detailStatus,
      placeCashStatus,
      selectedCoupon,
      showAccordian,
      currentUserId,
      setMaxItemErrorDisplayedAction,
      showCouponMergeError,
      showMaxItemErrorDisplayed,
      isNeedHelpModalOpen,
      toastMessage,
      onHoldCouponList,
    });
    const defaultOpen = availableCouponList && availableCouponList.size > 0;
    return (
      <View
        onLayout={event => {
          this.handleLayoutChange(event);
        }}
        ref={view => {
          this.CouponListComponent = view;
        }}
      >
        {showAccordian ? (
          <CollapsibleContainer
            header={header}
            body={body}
            defaultOpen={defaultOpen}
            iconLocator="arrowicon"
          />
        ) : (
          <>{body}</>
        )}
      </View>
    );
  }
}

CouponView.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  isCheckout: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  handleApplyCoupon: PropTypes.func.isRequired,
  handleApplyCouponFromList: PropTypes.func.isRequired,
  handleRemoveCoupon: PropTypes.func.isRequired,
  appliedCouponList: PropTypes.shape([]).isRequired,
  availableCouponList: PropTypes.shape([]).isRequired,
  onHoldCouponList: PropTypes.shape([]),
  handleErrorCoupon: PropTypes.func.isRequired,
  showAccordian: PropTypes.bool,
  toggleNeedHelpModal: PropTypes.func.isRequired,
  isNeedHelpModalOpen: PropTypes.bool,
  measurePositionHandler: PropTypes.func,
  toastMessage: PropTypes.func,
  showExpiredMessage: PropTypes.bool,
  showCouponMergeError: bool,
  showMaxItemErrorDisplayed: bool,
  currentUserId: string,
  setMaxItemErrorDisplayedAction: func,
  appliedExpiredCoupon: PropTypes.shape([]).isRequired,
  fetchNeedHelpContent: PropTypes.func.isRequired,
  placeCashContentId: PropTypes.shape({}).isRequired,
  needHelpContentId: PropTypes.shape({}).isRequired,
};

CouponView.defaultProps = {
  showAccordian: true,
  isNeedHelpModalOpen: false,
  toastMessage: () => {},
  setMaxItemErrorDisplayedAction: () => {},
  currentUserId: '',
  showCouponMergeError: false,
  showMaxItemErrorDisplayed: false,
  showExpiredMessage: false,
  measurePositionHandler: () => {},
  onHoldCouponList: [],
};

export default withStyles(CouponView, styles);
export { CouponView as CouponViewVanilla };
