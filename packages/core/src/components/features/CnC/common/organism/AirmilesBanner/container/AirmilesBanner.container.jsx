// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';

import AirmileBanner from '../views/AirmilesBanner.view';
import {
  getAirmilesBannerData,
  getAirmilesBannerLabels,
  getSyncError,
  getFormAirmilesNumber,
  getFormAirmilesOfferCode,
  getAirmilesModuleXContent,
} from './AirmilesBanner.selector';
import { addAirmilesBannerRequest } from './AirmilesBanner.actions';
import { isCanada } from '../../../../../../../utils';

export const AirmilesBannerContainer = ({
  className,
  onAddAirmilesBanner,
  airmilesBannerData,
  labels,
  addAirmilesBanner,
  syncErrorObj,
  promoField,
  offerField,
  isMiniBag,
  airmilesModuleXContent,
  fromPage,
}) => {
  return (
    isCanada() && (
      <AirmileBanner
        className={className}
        onAddAirmilesBanner={onAddAirmilesBanner}
        airmilesBannerData={airmilesBannerData}
        labels={labels}
        initialValues={{
          promoId: airmilesBannerData && airmilesBannerData.collectorNumber,
          offerCode: airmilesBannerData && airmilesBannerData.offerCode,
        }}
        addAirmilesBanner={addAirmilesBanner}
        syncErrorObj={syncErrorObj}
        promoField={promoField}
        offerField={offerField}
        isMiniBag={isMiniBag}
        airmilesModuleXContent={airmilesModuleXContent}
        fromPage={fromPage}
      />
    )
  );
};

export const mapDispatchToProps = (dispatch, ownProps) => {
  const formName = ownProps.isMiniBag ? 'AirmilesBannerMiniBag' : 'AirmilesBanner';
  return {
    onAddAirmilesBanner: () => {
      dispatch(addAirmilesBannerRequest(formName));
    },
  };
};

function mapStateToProps(state, ownProps) {
  const formName = ownProps.isMiniBag ? 'AirmilesBannerMiniBag' : 'AirmilesBanner';
  return {
    className: 'airmile-banner',
    airmilesBannerData: getAirmilesBannerData(state),
    labels: getAirmilesBannerLabels(state),
    syncErrorObj: getSyncError(state),
    promoField: getFormAirmilesNumber(formName)(state),
    offerField: getFormAirmilesOfferCode(formName)(state),
    airmilesModuleXContent: getAirmilesModuleXContent(state),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AirmilesBannerContainer);
