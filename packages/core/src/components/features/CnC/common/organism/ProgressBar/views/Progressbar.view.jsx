// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Col, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getIconPath, getLocator } from '@tcp/core/src/utils';
import styles from '../styles/Progressbar.style';

// eslint-disable-next-line complexity
const Progressbar = ({
  labels,
  currency,
  className,
  cashToNextReward,
  nextRewardAmount,
  totalCashForNextReward,
  isLeftProgressBar,
  isPlaceReward,
  pointsToNextReward,
  totalPointsForNextReward,
  // eslint-disable-next-line sonarjs/cognitive-complexity
}) => {
  return (
    <div className={className}>
      <Row className="progress-bar">
        {isLeftProgressBar ? (
          <Col
            key="progressbarText"
            className="progress-bar__text-wrapper"
            colSize={{ small: 5, medium: 6, large: 12 }}
          >
            <BodyCopy
              component="span"
              textAlign="center"
              fontFamily="secondary"
              fontWeight="regular"
              className="progress-bar__title-text"
              fontSize="fs14"
            >
              {isPlaceReward ? labels.placeRewardTitle1 : labels.title1}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="secondary"
              textAlign="center"
              fontWeight="extrabold"
              className="progress-bar__amount-text"
              fontSize="fs14"
            >
              {!isPlaceReward && currency}
              {isPlaceReward ? pointsToNextReward : cashToNextReward}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="secondary"
              textAlign="center"
              fontWeight="regular"
              className="progress-bar__title-text"
              fontSize="fs14"
            >
              {isPlaceReward ? labels.placeRewardTitle2 : labels.title2}
            </BodyCopy>
            {isPlaceReward && (
              <Image
                alt="my place image"
                className="my-place-image"
                src={getIconPath('my_place_reward')}
                data-locator={getLocator('my_place_reward_icon_placereward')}
              />
            )}
            <BodyCopy
              component="span"
              fontFamily="secondary"
              textAlign="center"
              fontWeight="extrabold"
              className="progress-bar__amount-text"
              fontSize="fs14"
            >
              {!isPlaceReward && currency}
              {isPlaceReward ? labels.placeRewardValue : nextRewardAmount}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="secondary"
              textAlign="center"
              fontWeight="extrabold"
              className="progress-bar__amount-text"
              fontSize="fs14"
            >
              {isPlaceReward ? labels.placeRewardTitle3 : labels.title3}
            </BodyCopy>
            {!isPlaceReward && (
              <BodyCopy
                component="span"
                fontFamily="secondary"
                textAlign="center"
                fontWeight="extrabold"
                className="progress-bar__amount-text"
                fontSize="fs14"
              >
                {labels.title4}
              </BodyCopy>
            )}
          </Col>
        ) : (
          <Col
            key="progressbarText"
            className="progress-bar__text-wrapper"
            colSize={{ small: 5, medium: 6, large: 12 }}
          >
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="regular"
              className="progress-bar__title-text"
              fontSize="fs14"
            >
              {labels.title1}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar__amount-text"
              fontSize="fs14"
            >
              {!isPlaceReward && currency}
              {isPlaceReward ? pointsToNextReward : cashToNextReward}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="regular"
              className="progress-bar__title-text"
              fontSize="fs14"
            >
              {labels.title2}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar__amount-text"
              fontSize="fs14"
            >
              {!isPlaceReward && currency}
              {isPlaceReward ? '$5' : nextRewardAmount}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar__amount-text"
              fontSize="fs14"
            >
              {labels.title3}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar__title-text"
              fontSize="fs14"
            >
              {labels.title4}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar__title-text"
              fontSize="fs14"
            >
              {labels.title5}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar_redeem-dates"
              fontSize="fs14"
            >
              (
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar_redeem-dates"
              fontSize="fs14"
            >
              {labels.startDate}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar_redeem-dates"
              fontSize="fs14"
            >
              -
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar_redeem-dates"
              fontSize="fs14"
            >
              {labels.endDate}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontFamily="primary"
              textAlign="center"
              fontWeight="bold"
              className="progress-bar_redeem-dates"
              fontSize="fs14"
            >
              )
            </BodyCopy>
          </Col>
        )}

        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          {isLeftProgressBar ? (
            <Row className="progress-bar__outer-container__all">
              <Col
                className="progress-bar__outer-container__startvalue"
                colSize={{ small: 1, medium: 1, large: 1 }}
              >
                <BodyCopy
                  component="span"
                  fontFamily="primary"
                  textAlign="center"
                  fontWeight="bold"
                  className="progress-bar__amount-text"
                  fontSize="fs14"
                >
                  {!isPlaceReward && currency}
                  {'0'}
                </BodyCopy>
              </Col>
              <Col
                key="progressBar"
                className="progress-bar__outer-container"
                colSize={{ small: 4, medium: 5, large: 8 }}
              >
                <div className="progress-bar__status-bar" />
              </Col>
              <Col
                className="progress-bar__outer-container__endvalue"
                colSize={{ small: 1, medium: 1, large: 1 }}
              >
                <BodyCopy
                  component="span"
                  fontFamily="primary"
                  textAlign="center"
                  fontWeight="bold"
                  className="progress-bar__amount-text"
                  fontSize="fs14"
                >
                  {!isPlaceReward && currency}
                  {isPlaceReward ? totalPointsForNextReward : totalCashForNextReward}
                </BodyCopy>
              </Col>
            </Row>
          ) : (
            <Col
              key="progressBar"
              className="progress-bar__outer-container"
              colSize={{ small: 6, medium: 8, large: 12 }}
            >
              <div className="progress-bar__status-bar" />
            </Col>
          )}
        </Col>
      </Row>
    </div>
  );
};

Progressbar.propTypes = {
  labels: PropTypes.shape({
    title1: PropTypes.string.isRequired,
    title2: PropTypes.string.isRequired,
    title3: PropTypes.string.isRequired,
    title4: PropTypes.string.isRequired,
  }).isRequired,
  cashToNextReward: PropTypes.string.isRequired,
  nextRewardAmount: PropTypes.string.isRequired,
  totalCashForNextReward: PropTypes.string.isRequired,
  currency: PropTypes.string.isRequired,
  className: PropTypes.string,
  isLeftProgressBar: PropTypes.bool,
  isPlaceReward: PropTypes.bool,
  pointsToNextReward: PropTypes.number,
  totalPointsForNextReward: PropTypes.number,
};

Progressbar.defaultProps = {
  className: 'variable-width',
  isLeftProgressBar: false,
  isPlaceReward: false,
  pointsToNextReward: 0,
  totalPointsForNextReward: 0,
};

export default withStyles(Progressbar, styles);
export { Progressbar as ProgressbarVanilla };
