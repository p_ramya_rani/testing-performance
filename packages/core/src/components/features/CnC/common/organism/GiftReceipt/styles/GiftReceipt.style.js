// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  @media print {
    @page {
      margin: 6px 0 0;
    }
    .outer-square,
    .outer-square-2 {
      width: 413px;
      height: ${props => (props.returnDays ? '961' : '1106')}px;
      margin: ${props => (props.returnDays ? '75' : '0')}px auto;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      border: 1px solid ${props => props.theme.colorPalette.gray[1600]};
      ${props =>
        props.returnDays
          ? ''
          : `
        display: flex;
        flex-direction: column;
      `}
      page-break-inside: avoid;
    }

    .inner-square {
      width: 360px;
      height: ${props => (props.returnDays ? '284' : '350')}px;
      border: solid 1px ${props => props.theme.colorPalette.gray[1600]};
      margin: ${props => (props.returnDays ? '26px 27px 27px 26px' : '12px 0 0 26px')};
      page-break-inside: avoid;
    }

    .brand-logo {
      width: 185px;
      height: 84px;
      margin: 1px 87px 0 88px;
    }

    .return-heading-box {
      width: 190px;
      height: 19px;
      font-size: 16px;
      margin: ${props => (props.returnDays ? '0 87px 15px 83px' : '0 87px 8px  83px')};
    }

    .bar-code {
      margin: 0 30px 0 34px;
      height: 47px;
    }

    .return-by,
    .return-update {
      margin-top: ${props => (props.returnDays ? '25' : '15')}px;
      ${props =>
        props.returnDays
          ? 'padding-left:44px'
          : `
        padding: 0 28px 0 44px;
      `}
    }

    .order-number-heading {
      display: inline-block;
      padding-left: 44px;
      margin-top: ${props => (props.returnDays ? '16' : '10')}px;
    }

    .order-number {
      display: inline;
    }

    .order-summary-section {
      padding-bottom: 5px;
    }

    ${props =>
      props.returnDays
        ? ''
        : `
          .gift-receipt-split {
            .outer-square {
              margin-top: 100px;
              height: 825px;
            }
            .outer-square-2 {
              height: 413px;
            }
            .inner-square {
              height: 385px;
            }
          }
      `}
  }
`;
