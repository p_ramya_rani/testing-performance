// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import GiftReceipt from '../views';
import { getGiftReceiptLabels, getGiftReceiptReturnDays } from './GiftReceipt.selectors';

const mapStateToProps = state => {
  return {
    returnDays: getGiftReceiptReturnDays(state),
    giftReceiptLabels: getGiftReceiptLabels(state),
  };
};

export default connect(mapStateToProps)(GiftReceipt);
