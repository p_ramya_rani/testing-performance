// 9fbef606107a605d69c0edbcd8029e5d 
import get from 'lodash/get';

const DEFAULT_RETURN_DAYS = 45;

const getGiftReceiptReturnDays = state =>
  parseInt(get(state, 'session.siteDetails.ORDER_GIFT_RETURN_BY_DAYS', DEFAULT_RETURN_DAYS), 10);
const getGiftReceiptLabels = state => get(state, 'Labels.global.giftReceipt', {});

export { getGiftReceiptLabels, getGiftReceiptReturnDays };
