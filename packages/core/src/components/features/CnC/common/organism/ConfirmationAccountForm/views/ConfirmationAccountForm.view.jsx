// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { setLoyaltyLocation } from '@tcp/core/src/constants/analytics';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { Field, reduxForm } from 'redux-form';
import {
  TextBox,
  Row,
  Col,
  Button,
  BodyCopy,
  RichText,
  Anchor,
  Image,
} from '@tcp/core/src/components/common/atoms';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath, isCanada, isClient, scrollToElement } from '@tcp/core/src/utils';
import createValidateMethod from '@tcp/core/src/utils/formValidation/createValidateMethod';
import getStandardConfig from '@tcp/core/src/utils/formValidation/validatorStandardConfig';
import { removeSpaces } from '@tcp/core/src/utils/formValidation/phoneNumber';
import PasswordIndicator from '@tcp/core/src/components/features/account/CreateAccount/molecules/PasswordIndicator';
import styles from '../styles/ConfirmationAccountForm.style';
import { scrollToFirstError } from '../../../../Checkout/util/utility';

const UseFocus = () => {
  const htmlElRef = useRef(null);
  const setFocus = () => {
    const elem = htmlElRef && htmlElRef.current;
    // Set Focus at the end of the text instead of start
    const timer = setTimeout(() => {
      elem.focus();
      if (elem.setSelectionRange) {
        const { length } = elem.value;
        elem.setSelectionRange(length, length);
      }
      clearTimeout(timer);
    }, 0);
  };

  return [htmlElRef, setFocus];
};
/**
 * @function renderEmailAddress
 * @param {String} emailAddress
 * @param {Object} inputColGrid
 * @param {String} placeHolder
 * @returns {JSX}
 */
const renderEmailAddress = (emailAddress, inputColGrid, placeHolder, lblSubHeading) => {
  return emailAddress ? (
    <Col {...inputColGrid}>
      <BodyCopy component="div" className="email-address">
        <BodyCopy
          component="p"
          textAlign="center"
          fontFamily="secondary"
          fontSize="fs16"
          fontWeight="normal"
          color="gray[400]"
          lineHeight="1.71"
        >
          {placeHolder}
        </BodyCopy>
        <BodyCopy
          component="p"
          textAlign="center"
          fontFamily="secondary"
          fontSize="fs16"
          fontWeight="extrabold"
          color="gray[900]"
          lineHeight="1.71"
        >
          {emailAddress}
        </BodyCopy>
        <BodyCopy className="elem-pt-MED" fontSize="fs16" color="gray[400]" textAlign="center">
          {lblSubHeading}
        </BodyCopy>
      </BodyCopy>
    </Col>
  ) : (
    <Col {...inputColGrid} className="elem-mt-LRG">
      <Field
        placeholder="Email Address"
        name="emailAddress"
        id="emailAddress"
        component={TextBox}
        dataLocator="email-address-field"
        enableSuccessCheck={false}
        normalize={removeSpaces}
        spc
      />
    </Col>
  );
};

/**
 * @function renderNotification
 * @param {Boolean} success
 * @param {String} successMsg
 * @param {String} error
 * @returns {JSX} Notification component with error or success state as provided in the input params.
 */
const renderNotification = (success, successMsg, error, inputColGrid) => {
  return (
    (error || success) && (
      <Col {...inputColGrid}>
        <Notification status={error ? 'error' : 'success'} message={error || successMsg} />
      </Col>
    )
  );
};

const renderSuccessMessage = (className, lblSucccessMsgYay, lblSucccessMsg, lblSucccessMsgPara) => {
  return (
    <div className={`${className} elem-pt-MED success-message-container`}>
      <BodyCopy className="heading" fontSize="fs24" color="gray[900]" textAlign="center">
        {lblSucccessMsgYay}
      </BodyCopy>
      <BodyCopy className="heading" fontSize="fs24" color="gray[900]" textAlign="center">
        {lblSucccessMsg}
      </BodyCopy>
      {!isCanada() && (
        <BodyCopy className="heading" fontSize="fs16" color="gray[400]" textAlign="center">
          {lblSucccessMsgPara}
        </BodyCopy>
      )}
    </div>
  );
};

const getTermsAndConditionText = (termsConditions, lblTermsAndConditionsUS1) => {
  return isCanada() ? termsConditions : lblTermsAndConditionsUS1;
};

const getHeadingText = (lblHeading, lblHeadingUs) => {
  return isCanada() ? lblHeading : lblHeadingUs;
};

const scrollToSuccessMessage = () => {
  if (isClient()) {
    const scrollTimer = setInterval(() => {
      const successElem = document.querySelector('.success-message-container');
      if (successElem) {
        scrollToElement(successElem);
        clearInterval(scrollTimer);
      }
    }, 100);
  }
};

const handleMouseDown = (e) => {
  const passwordField = document.querySelector('#password');
  if (document.activeElement === passwordField && e?.target?.type === 'checkbox') {
    e.target.click();
  }
};

/**
 * @function ConfirmationCreateAccountForm
 * @param {Object} Props
 * @returns {JSX} Render method
 */
const ConfirmationCreateAccountForm = ({
  className,
  isPromptForUserDetails,
  emailAddress,
  userInformation,
  handleSubmit,
  createAccountSubmit,
  createAccountSuccess,
  createAccountError,
  resetAccountErrorState,
  labels: {
    lbl_createAccount_emailAddress: lblEmailAddress,
    lbl_createAccount_password: lblPassword,
    lbl_createAccount_firstName: lblFirstName,
    lbl_createAccount_lastName: lblLastName,
    lbl_createAccount_phoneNumber: lblPhoneNumber,
    lbl_createAccount_zipCode: lblZipCode,
    lbl_createAccount_createAccount: lblSubmitButton,
    lbl_createAccount_termsConditions: lblTermsAndConditions1,
    lbl_createAccount_termsConditions_1: lblTermsAndConditions2,
    lbl_createAccount_termsConditions_us: lblTermsAndConditionsUS1,
    lbl_createAccount_show: lblShow,
    lbl_createAccount_hide: lblHide,
    lbl_createAccount_heading: lblHeading,
    lbl_createAccount_heading1: lblHeadingUs,
    lbl_createAccount_sub_heading1: lblSubHeading,
    lbl_createAccount_succcess_yay: lblSucccessMsgYay,
    lbl_createAccount_succcessMsg1: lblSucccessMsg,
    lbl_createAccount_succcessMsgPara: lblSucccessMsgPara,
  },
  passwordLabels,
}) => {
  /* Added istanbul, as method is called via redux form */
  /* istanbul ignore next */
  const formSubmit = (formValues) => {
    resetAccountErrorState();
    createAccountSubmit({
      isOrderConfirmation: true,
      ...userInformation,
      ...formValues,
    });
  };

  const [showPwd, togglePwd] = useState(false);
  const [passwordFieldFocus, setPasswordFieldFocus] = useState(false);
  const [passwordValue, setPasswordValue] = useState(null);
  const [inputRef, setInputFocus] = UseFocus();
  const inputColGrid = {
    offsetLeft: { large: 3 },
    offsetRight: { large: 3 },
    ignoreGutter: { small: true },
    colSize: { small: 6, large: 6 },
  };

  useEffect(() => {
    document.removeEventListener('mousedown', handleMouseDown);
    document.addEventListener('mousedown', handleMouseDown);
    return () => {
      document.removeEventListener('mousedown', handleMouseDown);
    };
  }, []);

  useEffect(() => {
    scrollToSuccessMessage();
  }, [createAccountSuccess]);

  if (createAccountSuccess) {
    return (
      <>{renderSuccessMessage(className, lblSucccessMsgYay, lblSucccessMsg, lblSucccessMsgPara)}</>
    );
  }

  return (
    <div className={`${className} elem-pt-XL elem-pl-MED elem-pr-MED`}>
      {!isCanada() && (
        <BodyCopy component="div" className="my-rewards-img-wrapper">
          <Image
            className="tcp_carousel__play"
            src={getIconPath('my-place-rewards-single-line')}
            alt="My Place Rewards"
          />
        </BodyCopy>
      )}
      <BodyCopy
        className="heading"
        fontSize="fs16"
        color="gray[900]"
        textAlign="center"
        fontWeight="extrabold"
      >
        {getHeadingText(lblHeading, lblHeadingUs)}
      </BodyCopy>
      <form onSubmit={handleSubmit(formSubmit)}>
        <Row fullBleed className="row-form-wrapper">
          {renderEmailAddress(emailAddress, inputColGrid, lblEmailAddress, lblSubHeading)}
          {renderNotification(
            createAccountSuccess,
            lblSucccessMsg,
            createAccountError,
            inputColGrid
          )}
          {isPromptForUserDetails && (
            <Col {...inputColGrid} className="elem-mt-SM">
              <Field
                placeholder={lblFirstName}
                name="firstName"
                id="firstName"
                component={TextBox}
                dataLocator="first-name-field"
                enableSuccessCheck={false}
                spc
              />
            </Col>
          )}
          {isPromptForUserDetails && (
            <Col {...inputColGrid} className="elem-mt-SM">
              <Field
                placeholder={lblLastName}
                name="lastName"
                id="lastName"
                component={TextBox}
                dataLocator="last name-field"
                enableSuccessCheck={false}
                spc
              />
            </Col>
          )}
          <Col className="password-container elem-mt-SM" {...inputColGrid}>
            <Field
              placeholder={lblPassword}
              name="password"
              id="password"
              type={showPwd ? 'text' : 'password'}
              component={TextBox}
              dataLocator="password-field"
              enableSuccessCheck={false}
              inputRef={inputRef}
              onBlur={() => {
                setPasswordFieldFocus(false);
              }}
              onChange={(e, newValue) => {
                setPasswordValue(newValue);
              }}
              onFocus={() => {
                if (!passwordFieldFocus) {
                  setPasswordFieldFocus(true);
                }
              }}
              spc
            />
            <div className="hide-show">
              <Col ignoreGutter={{ small: true }} colSize={{ small: 6 }}>
                <Anchor
                  underline
                  noLink
                  handleLinkClick={(event) => {
                    event.preventDefault();
                    togglePwd(!showPwd);
                    setInputFocus();
                  }}
                  className="hide-show-checkbox"
                  fontSizeVariation="large"
                  anchorVariation="primary"
                  dataLocator="pwd-hide-show-checkbox"
                  spc
                >
                  {showPwd ? lblHide : lblShow}
                </Anchor>
              </Col>
            </div>
          </Col>
          {passwordFieldFocus ? (
            <Col className="password-container" {...inputColGrid}>
              <PasswordIndicator labels={passwordLabels} passwordValue={passwordValue} />
            </Col>
          ) : null}
          {isPromptForUserDetails && (
            <Col {...inputColGrid} className="elem-mt-SM">
              <Field
                placeholder={lblPhoneNumber}
                name="phoneNumber"
                id="phoneNumber"
                type="tel"
                component={TextBox}
                maxLength={50}
                dataLocator="phone-number-field"
                enableSuccessCheck={false}
                spc
              />
            </Col>
          )}
          {isPromptForUserDetails && (
            <Col {...inputColGrid} className="elem-mt-SM">
              <Field
                placeholder={lblZipCode}
                name="noCountryZip"
                id="noCountryZip"
                component={TextBox}
                dataLocator="zip-code-field"
                enableSuccessCheck={false}
                spc
              />
            </Col>
          )}
          {passwordValue && (
            <Col className="elem-pt-XXL elem-pb-XS i-agree-checkbox" {...inputColGrid}>
              <Field
                name="iAgree"
                component={InputCheckbox}
                dataLocator="i-agree-checkbox"
                alignCheckbox="top"
              >
                <BodyCopy fontFamily="secondary" fontSize="fs10">
                  <RichText
                    richTextHtml={`${getTermsAndConditionText(
                      lblTermsAndConditions1,
                      lblTermsAndConditionsUS1
                    )} ${lblTermsAndConditions2}`}
                  />
                </BodyCopy>
              </Field>
            </Col>
          )}
          <Col {...inputColGrid} className="elem-pb-XXL">
            <ClickTracker
              onClick={() => {
                setLoyaltyLocation('confirmation');
              }}
            >
              <Button
                buttonVariation="fixed-width"
                fill="BLUE"
                type="submit"
                data-locator="create-account-btn"
                onMouseDown={(e) => e.preventDefault()}
              >
                {lblSubmitButton}
              </Button>
            </ClickTracker>
          </Col>
        </Row>
      </form>
    </div>
  );
};

const validateMethod = createValidateMethod(
  getStandardConfig([
    'emailAddress',
    'firstName',
    'lastName',
    'phoneNumber',
    'noCountryZip',
    'password',
    'confirmPassword',
    'iAgree',
  ])
);

ConfirmationCreateAccountForm.propTypes = {
  className: PropTypes.string,
  isPromptForUserDetails: PropTypes.bool,
  emailAddress: PropTypes.string.isRequired,
  userInformation: PropTypes.shape({}),
  handleSubmit: PropTypes.func.isRequired,
  createAccountSubmit: PropTypes.func.isRequired,
  createAccountSuccess: PropTypes.bool,
  createAccountError: PropTypes.string,
  resetAccountErrorState: PropTypes.func.isRequired,
  labels: PropTypes.shape({
    lbl_createAccount_emailAddress: PropTypes.string,
    lbl_createAccount_password: PropTypes.string,
    lbl_createAccount_confirmPassword: PropTypes.string,
    lbl_createAccount_firstName: PropTypes.string,
    lbl_createAccount_lastName: PropTypes.string,
    lbl_createAccount_phoneNumber: PropTypes.string,
    lbl_createAccount_zipCode: PropTypes.string,
    lbl_createAccount_createAccount: PropTypes.string,
    lbl_createAccount_termsConditions: PropTypes.string,
    lbl_createAccount_termsConditions_1: PropTypes.string,
    lbl_createAccount_show: PropTypes.string,
    lbl_createAccount_hide: PropTypes.string,
    lbl_createAccount_heading: PropTypes.string,
    lbl_info_icon: PropTypes.string,
  }),
  passwordLabels: PropTypes.shape({}),
};

ConfirmationCreateAccountForm.defaultProps = {
  className: '',
  isPromptForUserDetails: false,
  userInformation: {},
  createAccountSuccess: null,
  createAccountError: '',
  labels: {
    lbl_createAccount_emailAddress: '',
    lbl_createAccount_password: '',
    lbl_createAccount_confirmPassword: '',
    lbl_createAccount_firstName: '',
    lbl_createAccount_lastName: '',
    lbl_createAccount_phoneNumber: '',
    lbl_createAccount_zipCode: '',
    lbl_createAccount_createAccount: '',
    lbl_createAccount_termsConditions: '',
    lbl_createAccount_termsConditions_1: '',
    lbl_createAccount_show: '',
    lbl_createAccount_hide: '',
    lbl_createAccount_heading: '',
    lbl_info_icon: '',
  },
  passwordLabels: {},
};

/**
 * Redux Form HOC
 */
const withReduxForm = reduxForm({
  form: 'ConfirmationCreateAccountForm', // a unique identifier for this form
  ...validateMethod,
  enableReinitialize: true,
  onSubmitFail: (errors) => scrollToFirstError(errors),
})(ConfirmationCreateAccountForm);

export default withStyles(withReduxForm, styles);
export { ConfirmationCreateAccountForm as ConfirmationCreateAccountFormVanilla };
