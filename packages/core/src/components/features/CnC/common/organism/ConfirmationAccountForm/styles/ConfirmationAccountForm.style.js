// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  background-color: ${props => props.theme.colorPalette.gray[300]};

  .heading {
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  }

  .email-address {
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};

    @media ${props => props.theme.mediaQuery.medium} {
      padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    }

    @media ${props => props.theme.mediaQuery.large} {
      padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }

  .i-agree-checkbox {
    .Checkbox__error {
      padding: ${props => props.theme.spacing.ELEM_SPACING.MED} 0 0;
    }
    .labelStyle {
      display: flex;
    }
  }

  .password-container {
    position: relative;

    .hide-show {
      position: absolute;
      right: 20px;
      top: 17px;
      text-align: right;

      .info-icon-img-wrapper {
        width: 10px;
        margin: auto;
      }

      &.confirm-pwd {
        top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
      }

      .hide-show-checkbox {
        font-size: ${props => props.theme.typography.fontSizes.fs12};
      }
    }
  }

  .TextBox__input {
    background: transparent;
  }
  .my-rewards-img-wrapper {
    text-align: center;
    height: 26px;
    img {
      height: 100%;
    }
  }
  button {
    border-radius: 6px;
  }
`;

export default styles;
