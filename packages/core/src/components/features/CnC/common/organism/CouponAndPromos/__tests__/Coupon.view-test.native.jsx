// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { List } from 'immutable';
import CouponView from '../views/Coupon.view.native';

describe('Coupon component', () => {
  it('should renders correctly when Coupon are not present', () => {
    const props = {
      isFetching: false,
      handleApplyCoupon: () => {},
      appliedCouponList: new List(),
      availableCouponList: new List(),
      labels: { APPLIED_REWARDS_HEADING: 'Applied', AVAILABLE_REWARDS_HEADING: 'Available' },
    };
    const component = shallow(<CouponView {...props} />);
    expect(component).toMatchSnapshot();
  });
});
