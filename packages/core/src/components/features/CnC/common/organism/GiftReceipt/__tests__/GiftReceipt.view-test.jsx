import React from 'react';
import { shallow } from 'enzyme';
import { GiftReceiptVanilla as GiftReceipt } from '../views/GiftReceipt.view';

describe('GiftReceipt component', () => {
  const props = {
    orderNumbers: ['10'],
  };
  it('should renders correctly', () => {
    const component = shallow(<GiftReceipt {...props} />);
    expect(component).toMatchSnapshot();
  });
});
