// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { ImageBackground, Text } from 'react-native';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { isAndroid } from '@tcp/core/src/utils/utils.app';
import {
  ProgressbarContainer,
  ProgressbarTextWrapper,
  ProgressbarStatusContainer,
  ProgressbarStatusPlaceRewardContainer,
  ProgressbarStatusPlaceReward,
  MyPlaceRewardImageView,
  MyPlaceRewardImageViewParent,
  PlaceCashImageBackground,
} from '../styles/Progressbar.style.native';

const progressBarImg = require('../../../../../../../../../mobileapp/src/assets/images/progress-bar-image.png');
const progressBarImgPlcc = require('../../../../../../../../../mobileapp/src/assets/images/progress-bar-image-plcc.png');
const progressBarImgNonPlcc = require('../../../../../../../../../mobileapp/src/assets/images/progress-bar-image-nonplcc.png');
const progressImg = require('../../../../../../../../../mobileapp/src/assets/images/progressimage.png');
const progressBarImgAndriod = require('../../../../../../../../../mobileapp/src/assets/images/progress-bar-image-android.png');
const progressBarImgPlccAndriod = require('../../../../../../../../../mobileapp/src/assets/images/progress-bar-image-plcc-android.png');
const progressBarImgNonPlccAndriod = require('../../../../../../../../../mobileapp/src/assets/images/progress-bar-image-nonplcc-android.png');
const progressImgAndriod = require('../../../../../../../../../mobileapp/src/assets/images/progressimage-android.png');

const myPlaceRewardIcon = require('../../../../../../../../../mobileapp/src/assets/images/myPlaceReward.png');

const ProgressbarTextWrapperRender = (
  isPlaceReward,
  labels,
  pointsToNextReward,
  currency,
  cashToNextReward,
  nextRewardAmount
) => {
  return (
    <ProgressbarTextWrapper>
      <Text>
        <BodyCopy
          text={isPlaceReward ? labels.placeRewardTitle1 : labels.title1}
          textAlign="center"
          fontSize="fs14"
          fontFamily="primary"
        />
        <BodyCopy
          text={isPlaceReward ? pointsToNextReward : `${currency}${cashToNextReward}`}
          textAlign="center"
          fontWeight="bold"
          fontSize="fs14"
          fontFamily="primary"
        />
        <BodyCopy
          text={isPlaceReward ? labels.placeRewardTitle2 : labels.title2}
          textAlign="center"
          fontSize="fs14"
          fontFamily="primary"
        />
      </Text>
      {isPlaceReward && (
        <MyPlaceRewardImageViewParent>
          <MyPlaceRewardImageView source={myPlaceRewardIcon} androidPlatform={isAndroid()} />
        </MyPlaceRewardImageViewParent>
      )}
      <Text>
        <BodyCopy
          text={isPlaceReward ? labels.placeRewardValue : `${currency}${nextRewardAmount}`}
          textAlign="center"
          fontWeight="bold"
          fontSize="fs14"
          fontFamily="primary"
        />
        <BodyCopy
          text={isPlaceReward ? labels.placeRewardTitle3 : labels.title3}
          textAlign="center"
          fontWeight="bold"
          fontSize="fs14"
          fontFamily="primary"
        />
        {!isPlaceReward && (
          <BodyCopy
            text={labels.title4}
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
            fontFamily="primary"
          />
        )}
      </Text>
    </ProgressbarTextWrapper>
  );
};

const progressBarRender = (androidBar, iosBar) => {
  return isAndroid() ? androidBar : iosBar;
};

const Progressbar = ({
  labels,
  currency,
  cashToNextReward,
  width,
  nextRewardAmount,
  isLeftProgressBar,
  isPlaceReward,
  totalPointsForNextReward,
  pointsToNextReward,
  totalCashForNextReward,
  lpWidth,
  isPlcc,
}) => {
  const style = { height: 28, borderRadius: 5 };
  const progressWidth = isPlaceReward ? lpWidth : width;
  const imageStyle = {
    resizeMode: 'repeat',
    width: isPlaceReward ? lpWidth : width,
  };
  return isLeftProgressBar ? (
    <ProgressbarContainer>
      {ProgressbarTextWrapperRender(
        isPlaceReward,
        labels,
        pointsToNextReward,
        currency,
        cashToNextReward,
        nextRewardAmount
      )}
      <ProgressbarStatusPlaceRewardContainer>
        <BodyCopy
          fontFamily="primary"
          text={isPlaceReward ? 0 : `${currency}0`}
          textAlign="left"
          fontWeight="bold"
          fontSize="fs14"
        />
        <ProgressbarStatusPlaceReward
          isLeftProgressBar={isLeftProgressBar}
          isPlaceReward={isPlaceReward}
          isPlcc={isPlcc}
        >
          {isPlaceReward ? (
            <PlaceCashImageBackground
              source={
                isPlcc
                  ? progressBarRender(progressBarImgPlccAndriod, progressBarImgPlcc)
                  : progressBarRender(progressBarImgNonPlccAndriod, progressBarImgNonPlcc)
              }
              progressWidth={progressWidth}
              resizeMode="repeat"
            />
          ) : (
            <PlaceCashImageBackground
              source={progressBarRender(progressImgAndriod, progressImg)}
              progressWidth={progressWidth}
              resizeMode="repeat"
            />
          )}
        </ProgressbarStatusPlaceReward>
        <BodyCopy
          fontFamily="primary"
          text={isPlaceReward ? totalPointsForNextReward : `${currency}${totalCashForNextReward}`}
          textAlign="left"
          fontWeight="bold"
          fontSize="fs14"
        />
      </ProgressbarStatusPlaceRewardContainer>
    </ProgressbarContainer>
  ) : (
    <ProgressbarContainer>
      <ProgressbarTextWrapper>
        <Text>
          <BodyCopy
            fontFamily="primary"
            text={labels.title1}
            textAlign="center"
            fontWeight="regular"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text={`${currency}${cashToNextReward}`}
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text={labels.title2}
            textAlign="center"
            fontWeight="regular"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text={`${currency}${nextRewardAmount}`}
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text={labels.title3}
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text={labels.title4}
            textAlign="center"
            fontWeight="regular"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text={labels.title5}
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text="("
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text={labels.startDate}
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text="-"
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text={labels.endDate}
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
          <BodyCopy
            fontFamily="primary"
            text=")"
            textAlign="center"
            fontWeight="bold"
            fontSize="fs14"
          />
        </Text>
      </ProgressbarTextWrapper>
      <ProgressbarStatusContainer>
        <ImageBackground
          style={style}
          source={progressBarRender(progressBarImgAndriod, progressBarImg)}
          imageStyle={imageStyle}
        />
      </ProgressbarStatusContainer>
    </ProgressbarContainer>
  );
};

Progressbar.propTypes = {
  labels: PropTypes.shape({
    title1: PropTypes.string.isRequired,
    title2: PropTypes.string.isRequired,
    title3: PropTypes.string.isRequired,
    title4: PropTypes.string.isRequired,
  }).isRequired,
  cashToNextReward: PropTypes.string.isRequired,
  nextRewardAmount: PropTypes.string.isRequired,
  currency: PropTypes.string.isRequired,
  width: PropTypes.string,
  lpWidth: PropTypes.string,
  isLeftProgressBar: PropTypes.bool,
  isPlaceReward: PropTypes.bool,
  totalPointsForNextReward: PropTypes.number,
  pointsToNextReward: PropTypes.number,
  totalCashForNextReward: PropTypes.number,
  isPlcc: PropTypes.bool,
};

Progressbar.defaultProps = {
  width: '0%',
  lpWidth: '0%',
  isLeftProgressBar: false,
  isPlaceReward: false,
  isPlcc: false,
  totalPointsForNextReward: 0,
  pointsToNextReward: 0,
  totalCashForNextReward: 0,
};

export default withStyles(Progressbar);
export { Progressbar as ProgressbarVanilla };
