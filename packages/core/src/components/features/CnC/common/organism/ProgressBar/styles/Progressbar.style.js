// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .progress-bar {
    width: 100%;
    margin: 0;
    &__text-wrapper {
      position: relative;
      margin: 0 auto;
      text-align: center;
    }
    &__outer-container {
      margin: ${(props) => (props.newBag ? '0' : '0 0 32px 0')};
      overflow: hidden;
      border: 1px solid #707070;
    }
    &__status-bar {
      width: ${(props) => (props.isPlaceReward ? props.lpWidth : props.width)};
      height: 29px;
      background-image: repeating-linear-gradient(-76deg, #7ee476 0 29px, #aaeb9e 29px 56px);
      background-size: 55px 55px;
    }
  }
`;
