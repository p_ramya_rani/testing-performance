// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { getCurrencySymbol } from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import { isPlccUser } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import Progressbar from '../views/Progressbar.view';
import ProgressBarSelector from './Progressbar.selectors';
import PlaceCashSelector from '../../../../PlaceCashBanner/container/PlaceCashBanner.selectors';

const mapStateToProps = (state, ownProps) => {
  const { className } = ownProps;
  const currency = getCurrencySymbol(state);
  const width = ProgressBarSelector.getProgressStatus(state);
  const lpWidth = ProgressBarSelector.getLpProgressStatus(state);
  const pointsToNextReward = ProgressBarSelector.getPointsToNextReward(state);
  const cashToNextReward = ProgressBarSelector.getAmountToNextReward(state);
  const nextRewardAmount = PlaceCashSelector.getCashAmountOff(state);
  const totalCashForNextReward = PlaceCashSelector.getPlaceCashOffer(state);
  const labels = ProgressBarSelector.getAllLabels(state);
  const totalPointsForNextReward = ProgressBarSelector.totalPointsForNextReward(state);
  const isPlcc = isPlccUser(state);
  return {
    labels,
    width,
    currency,
    className,
    cashToNextReward,
    pointsToNextReward,
    nextRewardAmount,
    totalPointsForNextReward,
    totalCashForNextReward,
    lpWidth,
    isPlcc,
  };
};

export default connect(mapStateToProps)(Progressbar);
