// 9fbef606107a605d69c0edbcd8029e5d 
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  getCurrentPointsState,
  getPointsToNextRewardState,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import PlaceCashSelector from '../../../../PlaceCashBanner/container/PlaceCashBanner.selectors';
import { getCartOrderDetails } from '../../../../CartItemTile/container/CartItemTile.selectors';

const getCartSubTotal = state => {
  return getCartOrderDetails(state).get('subTotalWithoutGiftCard') || 0;
};
const getAmountToNextReward = state => {
  const placeCashMin = PlaceCashSelector.getPlaceCashOffer(state);
  return (placeCashMin - (getCartSubTotal(state) % placeCashMin) || 0).toFixed(2);
};
const getProgressStatus = state => {
  const placeCashMin = PlaceCashSelector.getPlaceCashOffer(state);
  return `${((getCartSubTotal(state) / placeCashMin) % 1).toFixed(2) * 100 || 0}%`;
};

const getEstimatedCartRewards = state => {
  return getCartOrderDetails(state).get('estimatedRewards') || 0;
};

const userCurrentTotalPoints = state => {
  const currentUserPoints = getCurrentPointsState(state);
  const totalUserPoints = currentUserPoints + getEstimatedCartRewards(state);
  return totalUserPoints || 0;
};

const totalPointsForNextReward = state => {
  return getPointsToNextRewardState(state) + getCurrentPointsState(state) || 0;
};

const getPointsToNextReward = state => {
  const lpCurrent = userCurrentTotalPoints(state);
  const nextReward = totalPointsForNextReward(state);
  const nextPoints = nextReward - (lpCurrent % nextReward);
  return nextPoints || 0;
};

const getLpProgressStatus = state => {
  const lpCurrent = userCurrentTotalPoints(state);
  const nextReward = totalPointsForNextReward(state);
  return `${((lpCurrent / nextReward) % 1).toFixed(2) * 100 || 0}%`;
};

const getLabelsByName = (state, labelName) => {
  const labelKeyValue = getLabelValue(state.Labels, labelName, 'placeCashBanner', 'checkout');
  return labelKeyValue !== labelName ? labelKeyValue : labelName;
};
const getAllLabels = state => {
  const currentCountry = PlaceCashSelector.getCurrentCountry(state);

  return {
    title1: getLabelsByName(state, `lbl_placeCash_progress_${currentCountry}_before_cartAmount`),
    title2: getLabelsByName(state, `lbl_placeCash_progress_${currentCountry}_between_amount`),
    title3: getLabelsByName(state, `lbl_placeCash_progress_${currentCountry}_place_bucks`),
    title4: getLabelsByName(state, `lbl_placeCash_progress_${currentCountry}_after_bucks`),
    title5: getLabelsByName(state, `lbl_placeCash_to_use`),
    startDate: getLabelsByName(state, `lbl_placeCash_${currentCountry}_bag_startDate`),
    endDate: getLabelsByName(state, `lbl_placeCash_${currentCountry}_bag_endDate`),
    placeRewardTitle1: getLabelsByName(state, `lbl_placereward_us_progress_before_points`),
    placeRewardTitle2: getLabelsByName(state, `lbl_placereward_us_progress_title2`),
    placeRewardTitle3: getLabelsByName(state, `lbl_placereward_us_progress_after_points`),
    placeRewardValue: getLabelsByName(state, `lbl_placereward_us_progress_reward_value`),
  };
};

export default {
  getProgressStatus,
  getLpProgressStatus,
  getAmountToNextReward,
  getPointsToNextReward,
  totalPointsForNextReward,
  getAllLabels,
};
