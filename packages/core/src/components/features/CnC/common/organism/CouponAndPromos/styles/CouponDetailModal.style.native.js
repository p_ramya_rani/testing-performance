// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import CustomButton from '@tcp/core/src/components/common/atoms/Button';

const ModalHeaderStyle = { maxHeight: 50 };

const FullHeaderStyle = { width: '100%' };

const StyledModalWrapper = styled.View`
  align-items: center;
  margin: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

const Horizontal = styled.View`
  height: 1px;
  background-color: ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
  margin: ${props => props.theme.spacing.ELEM_SPACING.MED} 0;
  flex-direction: row;
  width: 100%;
`;

const PrivacyContent = styled.View`
  text-align: left;
  justify-content: flex-start;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XL};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const SkeletonWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.MED};
  width: 200px;
`;

const ContentHeight = { minHeight: 1600 };

const StyledButton = styled(CustomButton)`
  opacity: ${props => (props.disabled ? '0.6' : '1.0')};
`;

export {
  StyledModalWrapper,
  Horizontal,
  PrivacyContent,
  ModalHeaderStyle,
  FullHeaderStyle,
  ContentHeight,
  SkeletonWrapper,
  StyledButton,
};
