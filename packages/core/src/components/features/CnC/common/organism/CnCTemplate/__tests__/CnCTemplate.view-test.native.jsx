// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import AfterPayContainer from '@tcp/core/src/components/common/organisms/AfterPayPayment';
import { CnCCommonTemplate } from '../views/CnCTemplate.view.native';
import { CheckoutButton } from '../styles/CnCTemplate.style.native';

describe('CnCCommonTemplate Page', () => {
  it('should render correctly', () => {
    const props = {
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      btnText: 'Shipping',
      onPress: jest.fn(),
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly if on confirmation page', () => {
    const props = {
      isConfirmationPage: true,
      isGuest: true,
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render venmo payment correctly if on confirmation page', () => {
    const props = {
      isConfirmationPage: true,
      isGuest: true,
      isVenmoPaymentInProgress: true,
      venmoPayment: {
        ccBrand: 'VENMO',
        ccType: 'VENMO',
        defaultInd: true,
        userName: 'test-user',
      },
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render paypal button correctly if on confirmation page', () => {
    const props = {
      isConfirmationPage: true,
      isGuest: true,
      isVenmoPaymentInProgress: true,
      venmoPayment: {
        ccBrand: 'VENMO',
        ccType: 'VENMO',
        defaultInd: true,
        userName: 'test-user',
      },
      showPayPalButton: true,
      showVenmoSubmit: true,
      isPayPalWebViewEnable: true,
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render paypal button correctly if on confirmation page for registered user', () => {
    const props = {
      isConfirmationPage: true,
      isGuest: false,
      isVenmoPaymentInProgress: true,
      venmoPayment: {
        ccBrand: 'VENMO',
        ccType: 'VENMO',
        defaultInd: true,
        userName: 'test-user',
      },
      showPayPalButton: true,
      showVenmoSubmit: true,
      isPayPalWebViewEnable: true,
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render payment buttons correctly if on confirmation page for registered user', () => {
    const props = {
      isConfirmationPage: true,
      isGuest: false,
      isVenmoPaymentInProgress: true,
      venmoPayment: {
        ccBrand: 'VENMO',
        ccType: 'VENMO',
        defaultInd: true,
        userName: 'test-user',
      },
      showPayPalButton: true,
      showVenmoSubmit: true,
      isPayPalWebViewEnable: true,
      getPayPalSettings: jest.fn(),
      onVenmoSubmit: jest.fn(),
      onVenmoError: jest.fn(),
      btnText: 'Review',
      onPress: jest.fn(),
      backLinkText: 'Billing',
      onBackLinkPress: jest.fn(),
      footerBody: null,
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should render AfterPay Button', () => {
    const props = {
      isConfirmationPage: false,
      isGuest: false,
      afterPayInProgress: true,
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree.find(AfterPayContainer)).toHaveLength(1);
    expect(tree).toMatchSnapshot();
  });

  it('should not render AfterPay Button', () => {
    const props = {
      isConfirmationPage: false,
      isGuest: false,
      afterPayInProgress: false,
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree.find(AfterPayContainer)).toHaveLength(0);
    expect(tree).toMatchSnapshot();
  });

  it('should not render submit Button when afterpayinprogress', () => {
    const props = {
      isConfirmationPage: false,
      isGuest: false,
      afterPayInProgress: true,
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree.find(CheckoutButton)).toHaveLength(0);
    expect(tree).toMatchSnapshot();
  });

  it('should render submit Button when afterpayinprogress is false', () => {
    const props = {
      isConfirmationPage: false,
      isGuest: false,
      showPayPalButton: false,
      afterPayInProgress: false,
      showVenmoSubmit: false,
      disableAction: false,
      isAfterPaySelected: false,
      navigation: {
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const tree = shallow(<CnCCommonTemplate {...props} />);
    expect(tree.find(CheckoutButton)).toHaveLength(1);
    expect(tree).toMatchSnapshot();
  });
});
