import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import PlaceCashBanner from '@tcp/core/src/components/features/CnC/PlaceCashBanner';
import { isMobileWeb, getLabelValue } from '@tcp/core/src/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import Row from '../../../../../../common/atoms/Row';
import Col from '../../../../../../common/atoms/Col';
import OrderLedgerContainer from '../../OrderLedger';
import AirmilesBanner from '../../AirmilesBanner';
import CouponAndPromos from '../../CouponAndPromos';
import BonusPointsDays from '../../../../../../common/organisms/BonusPointsDays';
import SigninCtaBanner from '../../../molecules/SigninCtaBanner';

/** The hard coded values are just to show the confirmation template. these will be removed once the components are are in place */
import styles from '../styles/CnCTemplate.style';
import PersonalizedCoupons from '../../../../Confirmation/organisms/PersonalizedCoupons';
import BAG_CONSTANTS from '../../../../BagPage/BagPage.constants';

const getBonusPointsDaysSection = ({ isGuest, showAccordian, newBag }) => {
  return (
    !isGuest && (
      <BonusPointsDays
        showAccordian={showAccordian}
        enableApplyCta
        additionalClassNameModal="bonus-modal-web"
        newBag={newBag}
      />
    )
  );
};

const getRecommendationSection = (isConfirmationPage, excludedIds, partNumber, labels) => {
  const headerLabelCheckout = getLabelValue(labels, 'lbl_confirmation_page', 'bagPage', 'checkout');
  const headerLabelSimilarStyles = getLabelValue(
    labels,
    'lbl_confirmation_page_checkout_similar_styles',
    'bagPage',
    'checkout'
  );

  return (
    isConfirmationPage && (
      <div className="recommendationsWrapper">
        <Recommendations
          page={Constants.RECOMMENDATIONS_PAGES_MAPPING.CHECKOUT}
          variations="moduleO"
          excludedIds={excludedIds}
          partNumber={partNumber}
          headerLabel={headerLabelCheckout}
          sequence="1"
        />
        <Recommendations
          page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
          portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.CHECKOUT_SIMILAR_STYLES}
          variations="moduleO"
          excludedIds={excludedIds}
          partNumber={partNumber}
          headerLabel={headerLabelSimilarStyles}
          sequence="2"
        />
      </div>
    )
  );
};

// eslint-disable-next-line complexity
const CnCTemplate = ({
  leftSection: LeftSection,
  showLeftSection,
  className,
  header: Header,
  isGuest,
  isCheckoutView,
  showAccordian,
  isConfirmationPage,
  orderLedgerAfterView,
  pageCategory,
  excludedIds,
  partNumber,
  labels,
  isSignInBannerAbTestEnabled,
  isBagPage,
  isNoNEmptyBag,
  rememberedUser,
  newBag,
  isStickyCheckoutButtonEnabled,
}) => {
  const isSmallLeftSection = showLeftSection;
  const [showBannerDesktop, setIsShowBannerDesktop] = useState(false);
  const [showBannerMobileWeb, setIsShowBannerMobileWeb] = useState(false);
  useEffect(() => {
    const bannerDesktop = isSignInBannerAbTestEnabled && isBagPage && !isMobileWeb();
    const bannerMobileWeb = isSignInBannerAbTestEnabled && isBagPage && isMobileWeb();
    setIsShowBannerDesktop(bannerDesktop);
    setIsShowBannerMobileWeb(bannerMobileWeb);
  }, []);
  return (
    <section className={className}>
      {Header && <Header />}
      <Row>
        <Col
          colSize={{
            small: 6,
            medium: isSmallLeftSection ? 5 : 8,
            large: isSmallLeftSection ? 8 : 12,
          }}
          className="left-sec "
        >
          {showBannerMobileWeb && isNoNEmptyBag && !rememberedUser && isGuest && (
            <SigninCtaBanner labels={labels} />
          )}
          <LeftSection />
        </Col>
        {showLeftSection && (
          <Col
            colSize={{ small: 6, medium: 3, large: 4 }}
            className={`right-sec ${isCheckoutView ? 'hide-mobile' : ''}`}
          >
            <>
              {isConfirmationPage ? (
                <>
                  <OrderLedgerContainer
                    isConfirmationPage={isConfirmationPage}
                    pageCategory="confirmation"
                  />
                  <Row fullBleed>
                    <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                      <PersonalizedCoupons />
                    </Col>
                  </Row>
                </>
              ) : (
                <>
                  {showBannerDesktop && isNoNEmptyBag && !rememberedUser && isGuest && (
                    <SigninCtaBanner labels={labels} />
                  )}
                  <OrderLedgerContainer
                    orderLedgerAfterView={orderLedgerAfterView}
                    pageCategory={pageCategory}
                    newBag={newBag}
                    isStickyCheckoutButtonEnabled={isStickyCheckoutButtonEnabled}
                  />

                  {getBonusPointsDaysSection({ isGuest, showAccordian, newBag })}
                  {pageCategory === BAG_CONSTANTS.BAG_PAGE && <PlaceCashBanner newBag={newBag} />}
                  <AirmilesBanner fromPage="CnCTemplate" />
                  <CouponAndPromos
                    fullPageInfo={!isCheckoutView || orderLedgerAfterView}
                    showAccordian={showAccordian}
                    additionalClassNameModal="coupon-modal-web"
                    idPrefix="desktop"
                    isCheckoutFlow={isCheckoutView}
                    newBag={newBag}
                  />
                </>
              )}
            </>
          </Col>
        )}
      </Row>
      {getRecommendationSection(isConfirmationPage, excludedIds, partNumber, labels)}
    </section>
  );
};

CnCTemplate.propTypes = {
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
  header: PropTypes.oneOfType([PropTypes.bool, PropTypes.node]),
  orderLedgerAfterView: PropTypes.shape({}).isRequired,
  leftSection: PropTypes.node.isRequired,
  showLeftSection: PropTypes.bool,
  isGuest: PropTypes.bool.isRequired,
  showAccordian: PropTypes.bool,
  isCheckoutView: PropTypes.bool,
  isConfirmationPage: PropTypes.bool,
  pageCategory: PropTypes.string,
  excludedIds: PropTypes.string,
  partNumber: PropTypes.string,
  isSignInBannerAbTestEnabled: PropTypes.bool.isRequired,
  isBagPage: PropTypes.bool.isRequired,
  isNoNEmptyBag: PropTypes.bool.isRequired,
  rememberedUser: PropTypes.bool.isRequired,
  newBag: PropTypes.bool,
  isStickyCheckoutButtonEnabled: PropTypes.bool,
};

CnCTemplate.defaultProps = {
  header: false,
  showLeftSection: true,
  showAccordian: true,
  isCheckoutView: false,
  isConfirmationPage: false,
  pageCategory: '',
  excludedIds: '',
  partNumber: '',
  labels: {},
  newBag: false,
};

export default withStyles(CnCTemplate, styles);
export { CnCTemplate as CnCTemplateVanilla };
