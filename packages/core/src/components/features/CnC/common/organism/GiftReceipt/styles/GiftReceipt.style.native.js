// 9fbef606107a605d69c0edbcd8029e5d 
const outerSquareStyle = `
  width:413px;
  margin: 5px auto;
  border-width: 1px;
  border-style: solid;
  border-color: rgb(151, 151, 151)
`;

const innerSquareStyle = `
  width: 360px;
  border-width: 1px;
  border-style: solid;
  border-color: rgb(151, 151, 151);
  border-image: initial;
  margin: 26px 27px 27px 26px;
`;

const brandLogoStyle = `
  width: 185px;
  height: 54px;
  margin-top:1px;
`;

const returnHeadingBoxStyle = `
  display:block;
  width: 190px;
  height: 19px;
  font-size: 16px;
  margin: 0 87px 10px 83px;
`;

const barCodeDivStyle = `
  display:block;
  text-align:center;
`;

const barCodeStyle = 'height:47px';

const returnByStyle = `
  margin-top:25px;
`;

const orderNumberHeadingStyle = `
  display: inline-block;
  padding-left: 44px;
  margin-top: 16px;
`;

const orderNumberStyle = 'display:inline;';

const breakPage = 'page-break-before: always;';

export {
  outerSquareStyle,
  innerSquareStyle,
  brandLogoStyle,
  returnHeadingBoxStyle,
  barCodeDivStyle,
  barCodeStyle,
  returnByStyle,
  orderNumberHeadingStyle,
  orderNumberStyle,
  breakPage,
};
