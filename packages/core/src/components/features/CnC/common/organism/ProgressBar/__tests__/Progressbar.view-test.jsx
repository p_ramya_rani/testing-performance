// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ProgressbarVanilla } from '../views/Progressbar.view';

const props = {
  labels: {
    title1: 'Spend another ',
    title2: ' to get a ',
    title3: 'Place Bucks ',
    title4: 'coupon',
  },
  currency: '$',
  className: 'variable-width',
  placeCashReward: 10,
  amountToNextReward: '9.5',
};

describe('ProgressbarVanilla', () => {
  let shallowComponent;
  beforeEach(() => {
    shallowComponent = shallow(<ProgressbarVanilla {...props} />);
  });
  it('should render correctly', () => {
    expect(shallowComponent).toMatchSnapshot();
  });
  it('should contain status bar element', () => {
    expect(shallowComponent.find('.progress-bar__status-bar').length).toEqual(1);
  });
  it('should contain 4 title text', () => {
    expect(shallowComponent.find('.progress-bar__title-text').length).toEqual(4);
  });
  it('should contain 3 amount text', () => {
    expect(shallowComponent.find('.progress-bar__amount-text').length).toEqual(3);
  });
  it('should contain 5 amount text', () => {
    expect(shallowComponent.find('.progress-bar_redeem-dates').length).toEqual(5);
  });
});
