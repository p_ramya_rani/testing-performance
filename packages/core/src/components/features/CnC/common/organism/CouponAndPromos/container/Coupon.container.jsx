// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes, { func, string } from 'prop-types';
import { updatePageData } from '@tcp/core/src/analytics/actions';
import { connect } from 'react-redux';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import { isMobileApp } from '@tcp/core/src/utils';
import { getUserId } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { applyCoupon, removeCoupon, setError, toggleNeedHelpModalState } from './Coupon.actions';
import {
  getCouponFetchingState,
  getCouponsLabels,
  getAppliedCouponListState,
  getAvailableCouponListState,
  getNeedHelpContent,
  getPlaceCashContent,
  getAllCoupons,
  getNeedHelpModalState,
  getAppliedExpiredCouponListState,
  getonHoldCouponListState,
} from './Coupon.selectors';
import { getGlobalLabels } from '../../../../../account/Account/container/Account.selectors';
import Coupon from '../views/Coupon.view';
import MyOffersCoupons from '../../../../../account/common/organism/MyOffersCoupons/views/MyOffersCoupons.view';
import BagPageSelectors from '../../../../BagPage/container/BagPage.selectors';
import BAG_PAGE_ACTIONS from '../../../../BagPage/container/BagPage.actions';

const getUpdatedLabels = (labels, needHelpRichText, placeCashContent) => {
  return {
    ...labels,
    NEED_HELP_RICH_TEXT: needHelpRichText,
    PLACE_CASH_CONTENT: placeCashContent,
  };
};
export class CouponContainer extends React.PureComponent {
  componentDidMount() {
    if (isMobileApp()) {
      const { updateCouponPageData } = this.props;
      const pageData = this.getPageDataObject();
      updateCouponPageData(pageData);
    }
  }

  getPageDataObject = () => {
    const { pageName, pageSection } = this.props;
    return {
      pageName: `${pageName}:${pageSection}`,
      pageSection: pageName,
      pageSubSection: pageName,
      pageType: pageName,
      pageShortName: `${pageName}:${pageSection}`,
    };
  };

  render() {
    const {
      labels,
      commonLabels,
      handleApplyCoupon,
      handleApplyCouponFromList,
      handleRemoveCoupon,
      appliedCouponList,
      availableCouponList,
      allCouponList,
      needHelpRichText,
      placeCashContent,
      handleErrorCoupon,
      isCheckout,
      showAccordian,
      isCarouselView,
      closedOverlay,
      idPrefix,
      additionalClassNameModal,
      openApplyNowModal,
      navigation,
      bagLoading,
      isNeedHelpModalOpen,
      toggleNeedHelpModal,
      isFetchingCouponState,
      measurePositionHandler,
      needHelpContentId,
      fetchNeedHelpContent,
      toastMessage,
      placeCashContentId,
      currentUserId,
      setMaxItemErrorDisplayedAction,
      appliedExpiredCoupon,
      showMaxItemErrorDisplayed,
      showCouponMergeError,
      onHoldCouponList,
      couponSelfHelp,
      newBag,
    } = this.props;

    // isCarouselView flag is only passed when coupon component is included from account
    const isFetching = isFetchingCouponState || (!isCarouselView && bagLoading);
    // eslint-disable-next-line no-shadow
    const updateLabels = getUpdatedLabels(labels, needHelpRichText, placeCashContent);
    return (
      <>
        {!isCarouselView && (
          <Coupon
            labels={updateLabels}
            isCheckout={isCheckout}
            isFetching={isFetching}
            handleApplyCoupon={handleApplyCoupon}
            handleApplyCouponFromList={handleApplyCouponFromList}
            handleRemoveCoupon={handleRemoveCoupon}
            appliedCouponList={appliedCouponList}
            availableCouponList={availableCouponList}
            handleErrorCoupon={handleErrorCoupon}
            showAccordian={showAccordian}
            additionalClassNameModal={additionalClassNameModal}
            idPrefix={idPrefix}
            bagLoading={bagLoading}
            openApplyNowModal={openApplyNowModal}
            navigation={navigation}
            isNeedHelpModalOpen={isNeedHelpModalOpen}
            toggleNeedHelpModal={toggleNeedHelpModal}
            toastMessage={toastMessage}
            measurePositionHandler={measurePositionHandler}
            needHelpContentId={needHelpContentId}
            fetchNeedHelpContent={fetchNeedHelpContent}
            showExpiredMessage
            placeCashContentId={placeCashContentId}
            currentUserId={currentUserId}
            showMaxItemErrorDisplayed={showMaxItemErrorDisplayed}
            setMaxItemErrorDisplayedAction={setMaxItemErrorDisplayedAction}
            appliedExpiredCoupon={appliedExpiredCoupon}
            showCouponMergeError={showCouponMergeError}
            onHoldCouponList={onHoldCouponList}
            newBag={newBag}
          />
        )}

        {isCarouselView && (
          <MyOffersCoupons
            labels={updateLabels}
            commonLabels={commonLabels}
            isCheckout={isCheckout}
            isFetching={isFetching}
            handleApplyCoupon={handleApplyCoupon}
            handleApplyCouponFromList={handleApplyCouponFromList}
            handleRemoveCoupon={handleRemoveCoupon}
            allCouponList={allCouponList}
            handleErrorCoupon={handleErrorCoupon}
            showAccordian={showAccordian}
            sliceCount={10}
            additionalClassNameModal={additionalClassNameModal}
            isCarouselView={isCarouselView}
            closedOverlay={closedOverlay}
            toastMessage={toastMessage}
            isNeedHelpModalOpen={isNeedHelpModalOpen}
            toggleNeedHelpModal={toggleNeedHelpModal}
            couponSelfHelp={couponSelfHelp}
          />
        )}
      </>
    );
  }
}

CouponContainer.propTypes = {
  isFetchingCouponState: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isCheckout: PropTypes.bool.isRequired,
  labels: PropTypes.shape.isRequired,
  handleApplyCoupon: PropTypes.func.isRequired,
  handleApplyCouponFromList: PropTypes.func.isRequired,
  handleRemoveCoupon: PropTypes.func.isRequired,
  appliedCouponList: PropTypes.shape({}).isRequired,
  availableCouponList: PropTypes.shape({}).isRequired,
  showAccordian: PropTypes.bool.isRequired,
  additionalClassNameModal: PropTypes.string.isRequired,
  commonLabels: PropTypes.shape({}).isRequired,
  allCouponList: PropTypes.shape([]).isRequired,
  needHelpRichText: PropTypes.string.isRequired,
  placeCashContent: PropTypes.string.isRequired,
  handleErrorCoupon: PropTypes.func.isRequired,
  isCarouselView: PropTypes.bool,
  closedOverlay: PropTypes.func,
  idPrefix: PropTypes.string,
  openApplyNowModal: PropTypes.func,
  navigation: PropTypes.shape({}),
  pageName: PropTypes.string,
  pageSection: PropTypes.string,
  currentUserId: string.isRequired,
  isNeedHelpModalOpen: PropTypes.bool,
  toggleNeedHelpModal: PropTypes.func.isRequired,
  updateCouponPageData: PropTypes.func,
  toastMessage: PropTypes.func,
  measurePositionHandler: PropTypes.func,
  setMaxItemErrorDisplayedAction: func.isRequired,
  fetchNeedHelpContent: PropTypes.func.isRequired,
  needHelpContentId: PropTypes.shape({}).isRequired,
  placeCashContentId: PropTypes.shape({}).isRequired,
  appliedExpiredCoupon: PropTypes.shape({}),
  showMaxItemErrorDisplayed: PropTypes.bool.isRequired,
  showCouponMergeError: PropTypes.bool,
  onHoldCouponList: PropTypes.shape([]),
  couponSelfHelp: PropTypes.bool,
  newBag: PropTypes.bool,
};

CouponContainer.defaultProps = {
  closedOverlay: () => {},
  isCarouselView: false,
  idPrefix: '',
  navigation: null,
  openApplyNowModal: () => {},
  pageName: '',
  pageSection: '',
  isNeedHelpModalOpen: false,
  updateCouponPageData: () => {},
  measurePositionHandler: () => {},
  toastMessage: () => {},
  appliedExpiredCoupon: [],
  showCouponMergeError: false,
  onHoldCouponList: [],
  couponSelfHelp: false,
  newBag: false,
};

export const mapDispatchToProps = (dispatch, { fullPageInfo, isCheckoutFlow }) => ({
  fetchNeedHelpContent: (contentIds) => {
    dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
  },
  handleApplyCouponFromList: (coupon) => {
    return new Promise((resolve, reject) => {
      dispatch(
        applyCoupon({
          fullPageInfo,
          isCheckoutFlow,
          formData: { couponCode: coupon.id },
          formPromise: { resolve, reject },
          coupon,
        })
      );
    });
  },
  handleRemoveCoupon: (coupon) => {
    return new Promise((resolve, reject) => {
      dispatch(
        removeCoupon({
          coupon,
          fullPageInfo,
          isCheckoutFlow,
          formPromise: { resolve, reject },
          analyticsCustomData: {
            eventName: 'walletlinksclickevent',
            pageNavigationText: 'my account-my wallet-remove from bag',
          },
        })
      );
    });
  },
  handleApplyCoupon: (formData, _, props) =>
    new Promise((resolve, reject) => {
      dispatch(
        applyCoupon({
          formData,
          fullPageInfo,
          isCheckoutFlow,
          source: props && props.source,
          formPromise: { resolve, reject },
        })
      );
    }),
  handleErrorCoupon: (coupon) => {
    const timer = setTimeout(() => {
      dispatch(setError({ msg: null, couponCode: coupon.id }));
      clearTimeout(timer);
    }, 5000);
  },
  openApplyNowModal: (payload) => {
    dispatch(toggleApplyNowModal(payload));
  },
  toastMessage: (payload) => {
    dispatch(toastMessageInfo(payload));
  },
  toggleNeedHelpModal: () => {
    dispatch(toggleNeedHelpModalState());
  },
  setMaxItemErrorDisplayedAction: (payload) => {
    dispatch(BAG_PAGE_ACTIONS.setMaxItemErrorDisplayed(payload));
  },
  updateCouponPageData: (payload) => {
    dispatch(updatePageData(payload));
  },
});

export const mapStateToProps = (state) => ({
  isFetchingCouponState: getCouponFetchingState(state),
  labels: getCouponsLabels(state),
  appliedCouponList: getAppliedCouponListState(state),
  availableCouponList: getAvailableCouponListState(state),
  allCouponList: getAllCoupons(state),
  needHelpRichText: getNeedHelpContent(state),
  placeCashContent: getPlaceCashContent(state),
  commonLabels: getGlobalLabels(state),
  isNeedHelpModalOpen: getNeedHelpModalState(state),
  bagLoading: BagPageSelectors.isBagLoading(state),
  needHelpContentId: BagPageSelectors.getNeedHelpContent(state),
  placeCashContentId: BagPageSelectors.getPlaceCashContent(state),
  currentUserId: getUserId(state),
  showMaxItemErrorDisplayed: BagPageSelectors.getShowMaxItemErrorDisplayed(state),
  appliedExpiredCoupon: getAppliedExpiredCouponListState(state),
  showCouponMergeError: BagPageSelectors.getShowCouponMergeError(state),
  onHoldCouponList: getonHoldCouponListState(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(CouponContainer);
