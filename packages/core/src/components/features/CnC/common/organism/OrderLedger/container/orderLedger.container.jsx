// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  getIsNewLoyaltyBanner,
  getIsABLoaded,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import { getCartLoadingState } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { isPlccUser } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import OrderLedger from '../views/orderLedger.view';
import { getLedgerSummaryData, getOrderLedgerLabels } from './orderLedger.selector';
import confirmationSelectors from '../../../../Confirmation/container/Confirmation.selectors';
import checkoutSelectors from '../../../../Checkout/container/Checkout.selector';

const { getVenmoPayment, isVenmoOrderPayment } = checkoutSelectors;

const OrderLedgerContainer = (props) => {
  const {
    className,
    ledgerSummaryData,
    labels,
    showAccordian,
    confirmationPageLedgerSummaryData,
    isConfirmationPage,
    orderLedgerAfterView,
    pageCategory,
    navigation,
    bagLoadingSection,
    bagLoadingPageLevel,
    isVenmoPaymentInProgress,
    venmoPayment,
    venmoPaymentInfo,
    isCartLoading,
    newBag,
    isNewLoyaltyBanner,
    isPlcc,
    isStickyCheckoutButtonEnabled,
    isABTestLoaded,
  } = props;
  const bagLoading = bagLoadingSection || bagLoadingPageLevel;
  return (
    <OrderLedger
      className={className}
      ledgerSummaryData={ledgerSummaryData}
      labels={labels}
      showAccordian={showAccordian}
      orderLedgerAfterView={orderLedgerAfterView}
      confirmationPageLedgerSummaryData={confirmationPageLedgerSummaryData}
      isConfirmationPage={isConfirmationPage}
      pageCategory={pageCategory}
      navigation={navigation}
      bagLoading={bagLoading}
      isVenmoPaymentInProgress={isVenmoPaymentInProgress}
      venmoPayment={venmoPayment}
      venmoPaymentInfo={venmoPaymentInfo}
      isCartLoading={isCartLoading}
      newBag={newBag}
      isNewLoyaltyBanner={isNewLoyaltyBanner}
      isPlcc={isPlcc}
      isStickyCheckoutButtonEnabled={isStickyCheckoutButtonEnabled}
      isABTestLoaded={isABTestLoaded}
    />
  );
};

OrderLedgerContainer.propTypes = {
  className: PropTypes.string.isRequired,
  ledgerSummaryData: PropTypes.string.isRequired,
  labels: PropTypes.string.isRequired,
  showAccordian: PropTypes.string.isRequired,
  isConfirmationPage: PropTypes.string.isRequired,
  orderLedgerAfterView: PropTypes.string.isRequired,
  confirmationPageLedgerSummaryData: PropTypes.string.isRequired,
  pageCategory: PropTypes.string.isRequired,
  navigation: PropTypes.string.isRequired,
  bagLoadingSection: PropTypes.bool.isRequired,
  bagLoadingPageLevel: PropTypes.bool.isRequired,
  isVenmoPaymentInProgress: PropTypes.bool.isRequired,
  venmoPaymentInfo: PropTypes.bool.isRequired,
  venmoPayment: PropTypes.shape({}).isRequired,
  isCartLoading: PropTypes.bool.isRequired,
  newBag: PropTypes.bool,
  isNewLoyaltyBanner: PropTypes.bool,
  isPlcc: PropTypes.bool,
  isABTestLoaded: PropTypes.bool,
  isStickyCheckoutButtonEnabled: PropTypes.bool,
};

OrderLedgerContainer.defaultProps = {
  newBag: false,
  isNewLoyaltyBanner: false,
  isPlcc: false,
  isABTestLoaded: false,
  isStickyCheckoutButtonEnabled: false,
};

function mapStateToProps(state) {
  return {
    className: 'order-summary',
    ledgerSummaryData: getLedgerSummaryData(state),
    labels: getOrderLedgerLabels(state),
    bagLoadingSection: BagPageSelector.isBagLoading(state),
    confirmationPageLedgerSummaryData:
      confirmationSelectors.getLedgerSummaryDataConfirmation(state),
    venmoPayment: getVenmoPayment(state),
    isVenmoPaymentInProgress: isVenmoOrderPayment(state),
    isCartLoading: getCartLoadingState(state),
    isNewLoyaltyBanner: getIsNewLoyaltyBanner(state),
    isPlcc: isPlccUser(state),
    isABTestLoaded: getIsABLoaded(state),
  };
}

export default connect(mapStateToProps)(OrderLedgerContainer);
export const OrderLedgerContainerVanilla = OrderLedgerContainer;
