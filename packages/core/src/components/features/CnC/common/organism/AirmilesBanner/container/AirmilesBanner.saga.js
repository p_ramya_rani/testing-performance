// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest, select } from 'redux-saga/effects';
import AIRMILES_BANNER_CONSTANTS from '../AirmilesBanner.constants';
import { addAirmilesBannerFailure } from './AirmilesBanner.actions';
import BAG_PAGE_ACTIONS from '../../../../BagPage/container/BagPage.actions';
import addAirmilesBannerApi from '../../../../../../../services/abstractors/CnC/AirmilesBanner';
import {
  getFormAirmilesNumber,
  getFormAirmilesOfferCode,
  getCartOrderId,
} from './AirmilesBanner.selector';

export function* addAirmilesBanner({ payload: formName }) {
  try {
    const orderIdNum = yield select(getCartOrderId);
    const promoId = yield select(getFormAirmilesNumber(formName));
    const offerCode = yield select(getFormAirmilesOfferCode(formName));
    const orderId = orderIdNum.toString();
    yield call(addAirmilesBannerApi, { orderId, promoId, offerCode });
    yield put(BAG_PAGE_ACTIONS.updateAirmilesData({ promoId, offerCode }));
  } catch (err) {
    yield put(addAirmilesBannerFailure(err.message));
  }
}

export function* AddAirmilesBannerSaga() {
  yield takeLatest(AIRMILES_BANNER_CONSTANTS.ADD_AIRMILES_BANNER_REQUEST, addAirmilesBanner);
}

export default AddAirmilesBannerSaga;
