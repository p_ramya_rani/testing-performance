// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import Button from '@tcp/core/src/components/common/atoms/Button';
import { Anchor, RichText } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ContentLink from '@tcp/core/src/components/features/account/common/molecule/ContentLink';
import { BodyCopy } from '../../../../../../../../styles/themes/TCP/typotheme';
import styles from '../styles/CouponDetailModal.style';

const COUPON_APPLIED = 'applied';

class CouponDetailModal extends React.PureComponent {
  static defaultProps = {
    labels: {
      USE_BY_TEXT: '',
      APPLY_TO_BAG: 'Apply To Bag',
      PRINT_ANCHOR_TEXT: 'Print',
      MODAL_SHORT_DESCRIPTION: '',
      TERMS_AND_CONDITIONS: 'terms & conditions',
      PRIVACY_POLICY: 'privacy policy',
    },
    isConfirmation: false,
  };

  componentDidUpdate() {
    const { coupon, handleErrorCoupon } = this.props;
    if (coupon?.error && handleErrorCoupon) {
      handleErrorCoupon(coupon);
    }
  }

  printClick = (event) => {
    window.print();
    event.preventDefault();
  };

  /**
   * This function is used for apply to bag coupon
   * can be passed in the component.
   */
  handleApplyToBag = () => {
    const { onApplyCouponToBagFromList, coupon, onRequestClose } = this.props;
    onApplyCouponToBagFromList(coupon);
    onRequestClose();
  };

  renderModal = () => {
    const { labels, coupon, className, isConfirmation } = this.props;
    const { isStarted } = coupon;
    const isApplied = coupon.status === COUPON_APPLIED;
    const couponId =
      coupon && coupon.id && coupon.id.indexOf('-') > -1 ? coupon.id.split('-')[1] : coupon.id;
    return (
      <div className={className}>
        <BodyCopy
          fontWeight="black"
          className="couponModal_modalTitle text-break"
          data-locator={`couponDetailModal_${coupon.status}_NameLbl`}
        >
          {coupon.title}
        </BodyCopy>
        {!isConfirmation && (
          <BodyCopy
            fontWeight="bold"
            fontFamily="secondaryFontFamily"
            className="couponModal_modalSubTitle"
            data-locator={`couponDetailModal_${coupon.status}_ValidityDateLbl`}
          >
            {`${labels.USE_BY_TEXT} ${coupon.expirationDate}`}
          </BodyCopy>
        )}
        {couponId && (
          <BodyCopy
            component="div"
            data-locator={`couponDetailModal_${coupon.status}_BarCode`}
            className="couponModal_modalbarcode"
            aria-label={coupon.id}
            role="img"
          >
            <BodyCopy component="div" className="elem-mt-MED elem-mb-MED" aria-hidden>
              <Barcode value={couponId} barcodeId={couponId} fontSize={24} />
            </BodyCopy>
          </BodyCopy>
        )}
        {!isConfirmation && isStarted && (
          <div className="couponModal_btnWrapper">
            <Button
              buttonVariation="fixed-width"
              fill="BLUE"
              onClick={this.handleApplyToBag}
              disabled={isApplied}
              className="couponModal_applyToBag couponModal_btn"
              data-locator={`couponDetailModal_${coupon.status}_AddToBagBtn`}
            >
              {isApplied ? labels.APPLIED_TO_BAG : labels.APPLY_TO_BAG}
            </Button>
          </div>
        )}
        <div className="couponModal_print">
          <Anchor
            underline
            anchorVariation="primary"
            fontSize="fs14"
            dataLocator={`couponDetailModal_${coupon.status}_printAch`}
            to="/#"
            onClick={this.printClick}
          >
            {labels.PRINT_ANCHOR_TEXT}
          </Anchor>
        </div>
        <BodyCopy
          fontFamily="secondaryFontFamily"
          className="couponModal_modalLongDesc"
          data-locator={`couponDetailModal_${coupon.status}_LongDesc`}
        >
          <RichText richTextHtml={coupon.legalText} />
        </BodyCopy>
        {!isConfirmation && (
          <BodyCopy
            fontFamily="secondaryFontFamily"
            className="couponModal_modalShortDesc"
            data-locator={`couponDetailModal_${coupon.status}_ShortDesc`}
          >
            {`${labels.MODAL_SHORT_DESCRIPTION}`}
            <ContentLink
              underline
              anchorVariation="primary"
              fontSize="fs14"
              dataLocator={`couponDetailModal_${coupon.status}_tAndC`}
              urlKey="lbl_termcondition_link"
              className="couponModal_print_anchortext"
              target="_blank"
            >
              {`${labels.TERMS_AND_CONDITIONS}`}
            </ContentLink>

            {` and `}
            <ContentLink
              underline
              anchorVariation="primary"
              fontSize="fs14"
              dataLocator={`couponDetailModal_${coupon.status}_pp`}
              urlKey="lbl_privacyPolicy_link"
              className="couponModal_print_anchortext"
              target="_blank"
            >
              {`${labels.PRIVACY_POLICY}`}
            </ContentLink>
          </BodyCopy>
        )}
      </div>
    );
  };

  render() {
    const { openState, coupon, onRequestClose, additionalClassNameModal } = this.props;
    return (
      openState && (
        <Modal
          isOpen={openState}
          onRequestClose={onRequestClose}
          overlayClassName="TCPModal__Overlay"
          className="TCPModal__Content"
          heightConfig={{ height: '90%' }}
          contentLabel={coupon.title}
          closeIconDataLocator="coupondetailmodalcrossicon"
          customWrapperClassName={additionalClassNameModal}
        >
          {this.renderModal()}
        </Modal>
      )
    );
  }
}

CouponDetailModal.propTypes = {
  coupon: PropTypes.shape({}).isRequired,
  handleErrorCoupon: PropTypes.func.isRequired,
  onApplyCouponToBagFromList: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  openState: PropTypes.bool.isRequired,
  className: PropTypes.string.isRequired,
  additionalClassNameModal: PropTypes.string.isRequired,
  isConfirmation: PropTypes.bool,
  labels: PropTypes.shape({
    USE_BY_TEXT: PropTypes.string,
    APPLY_TO_BAG: PropTypes.string,
    PRINT_ANCHOR_TEXT: PropTypes.string,
    MODAL_SHORT_DESCRIPTION: PropTypes.string,
    TERMS_AND_CONDITIONS: PropTypes.string,
    PRIVACY_POLICY: PropTypes.string,
  }),
};

export default withStyles(CouponDetailModal, styles);
export { CouponDetailModal as CouponDetailModalVanilla };
