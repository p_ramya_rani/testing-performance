// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import ProgressBarSelector from '../container/Progressbar.selectors';

const { getProgressStatus, getAmountToNextReward, getAllLabels } = ProgressBarSelector;

const Confirmation = fromJS({
  orderConfirmation: {
    summary: {
      subTotal: 29,
    },
    orderDetails: {
      subTotal: 29,
    },
  },
});
const CartPageReducer = fromJS({
  orderDetails: {
    rewardsToBeEarned: 20,
    subTotalWithoutGiftCard: 29,
  },
});

const state = {
  Confirmation,
  CartPageReducer,
  session: {
    siteDetails: {
      PLACECASH_EARN_MIN_PURCHASE: 20,
    },
  },
  Labels: {
    checkout: {
      placeCashBanner: {
        lbl_placeCash_progress_US_before_cartAmount: 'title1',
        lbl_placeCash_progress_US_between_amount: 'title2',
        lbl_placeCash_progress_US_place_bucks: 'title3',
        lbl_placeCash_progress_US_after_bucks: 'title4',
        lbl_placeCash_to_use: 'title5',
        lbl_placeCash_US_bag_startDate: 'startDate',
        lbl_placeCash_US_bag_endDate: 'endDate',
        lbl_placereward_us_progress_before_points: 'placeRewardTitle1',
        lbl_placereward_us_progress_title2: 'placeRewardTitle2',
        lbl_placereward_us_progress_after_points: 'placeRewardTitle3',
        lbl_placereward_us_progress_reward_value: 'placeRewardValue',
      },
    },
  },
};
describe('Progress Bar Selectors', () => {
  it('#getProgressStatus', () => {
    expect(getProgressStatus(state)).toEqual('45%');
  });
  it('#getAmountToNextReward', () => {
    expect(getAmountToNextReward(state)).toEqual('11.00');
  });
  it('#getAllLabels', () => {
    const labels = {
      title1: 'title1',
      title2: 'title2',
      title3: 'title3',
      title4: 'title4',
      title5: 'title5',
      startDate: 'startDate',
      endDate: 'endDate',
      placeRewardTitle1: 'placeRewardTitle1',
      placeRewardTitle2: 'placeRewardTitle2',
      placeRewardTitle3: 'placeRewardTitle3',
      placeRewardValue: 'placeRewardValue',
    };
    expect(getAllLabels(state)).toEqual(labels);
  });
});
