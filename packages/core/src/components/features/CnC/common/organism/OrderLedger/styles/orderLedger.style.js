// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const LedgerStyle = css`
  background-color: ${(props) => props.theme.colors.WHITE};
  padding: 23px 1px 21px;
  padding-bottom: 0;
  .item-closed {
    .elem-mb-MED {
      margin-bottom: 0;
    }
  }

  .rowMargin {
    margin-bottom: 8px;
  }
  .estimated-total {
    padding-top: 10px;
  }
  .tax-total {
    padding-bottom: 7px;
    border-bottom: 1px solid ${(props) => props.theme.colors.PRIMARY.GRAY};
  }
  .after-pay-msg {
    border-bottom: 1px solid ${(props) => props.theme.colors.PRIMARY.GRAY};
    text-align: center;
    padding-bottom: 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
  .circle-info-image {
    width: 15px;
    height: 15px;
    position: relative;
    left: 5px;
    top: 2px;
  }
  .tooltip-bubble {
    left: 100%;
    ::after {
      right: 42%;
    }

    ::before {
      right: 39%;
    }
  }
  .tooltip-message p {
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  }
  .balance-total-columns {
    display: flex;
    justify-content: space-between;
    width: 100%;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy5}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    line-height: ${(props) => props.theme.fonts.lineHeight.normal};
  }

  .order-ledger-header {
    text-align: left;
  }

  @media ${(props) => props.theme.mediaQuery.smallMax} {
    background-color: ${(props) => props.theme.colors.WHITE};
  }

  .orderLedgerAccordian {
    .collapsible-header {
      background-color: ${(props) => props.theme.colorPalette.white};
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
    .collapsible-icon {
      top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
      right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
  }

  .review-submit-container {
    display: flex;
    flex-direction: column;
    margin: 35px ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    --afterpay-button-width: 100%;
    --afterpay-button-height: 100%;
    .review-submit-button {
      flex: 1;
      position: relative;
    }

    .after-pay-container {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      opacity: 0;
      overflow: hidden;
      cursor: pointer;
    }

    .submit-disclaimer {
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy1}px;
      color: ${(props) => props.theme.colorPalette.gray[800]};
      text-align: center;
      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
        margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
        margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      }
      @media ${(props) => props.theme.mediaQuery.large} {
        margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
        margin-left: 0;
        margin-right: 0;
      }
    }
    .submit-disclaimer-link {
      display: inline-block;
      margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    }
  }

  &.order-confirmation {
    .order-ledger-header {
      display: none;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        display: block;
        border-top: 1px solid ${(props) => props.theme.colors.PRIMARY.GRAY};
        border-bottom: 1px solid ${(props) => props.theme.colors.PRIMARY.GRAY};
        padding: 14px 14px;
      }
    }
  }

  .orderLedgerLoyalty {
    ${(props) =>
      props.newBag && props.isNewLoyaltyBanner && !props.isPlcc
        ? `padding: ${props.theme.spacing.ELEM_SPACING.SM}`
        : ''};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.newBag && props.isNewLoyaltyBanner && !props.isPlcc
          ? `padding: ${props.theme.spacing.ELEM_SPACING.SM} ${props.theme.spacing.ELEM_SPACING.XXS} ${props.theme.spacing.ELEM_SPACING.XXS}`
          : ''};
    }
    ${(props) =>
      props.pageCategory === 'confirmation'
        ? `
        @media ${props.theme.mediaQuery.medium} {
          display: none;
        }
        @media ${props.theme.mediaQuery.large} {
          display: none;
        }
        `
        : ''}
  }

  .venmo-payment-method-wrapper {
    display: flex;
    flex-direction: row;
    text-align: left;
    margin-left: 14px;
  }

  .cardImage-wrapper {
    display: flex;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }

  .cardImage-img-wrapper {
    display: flex;
    border: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    background-color: ${(props) => props.theme.colors.WHITE};

    .cardImage-img {
      width: 47px;
      height: 30px;
    }
  }

  .venmo-paid-text {
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
`;

export default LedgerStyle;
