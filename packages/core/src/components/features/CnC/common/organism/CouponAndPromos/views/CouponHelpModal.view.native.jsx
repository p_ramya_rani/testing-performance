// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { RichText } from '@tcp/core/src/components/common/atoms';
import Modal from '../../../../../../common/molecules/Modal';
import {
  ModalHeaderStyle,
  ContentHeight,
  SkeletonWrapper,
} from '../styles/CouponDetailModal.style.native';
import GenericSkeleton from '../../../../../../common/molecules/GenericSkeleton/GenericSkeleton.view.native';

class CouponHelpModal extends React.PureComponent {
  showContent = () => {
    const { isPlaceCashStatus, labels } = this.props;
    if (isPlaceCashStatus) {
      return !!labels.PLACE_CASH_CONTENT;
    }
    return !!labels.NEED_HELP_RICH_TEXT;
  };

  render() {
    const { openState, onRequestClose, labels, isPlaceCashStatus, horizontalBar } = this.props;
    return (
      <Modal
        fixedWidth
        isOpen={openState}
        onRequestClose={onRequestClose}
        overlayClassName="TCPModal__Overlay"
        closeIconDataLocator="added-to-bg-close"
        animationType="slide"
        headerStyle={{ ModalHeaderStyle, ...{ head: { 'background-color': 'transparent' } } }}
        horizontalBar={horizontalBar}
        heading=" "
      >
        {this.showContent() ? (
          <RichText
            source={{
              html: isPlaceCashStatus ? labels.PLACE_CASH_CONTENT : labels.NEED_HELP_RICH_TEXT,
            }}
            isApplyDeviceHeight
            style={ContentHeight}
          />
        ) : (
          <SkeletonWrapper>
            <GenericSkeleton />
          </SkeletonWrapper>
        )}
      </Modal>
    );
  }
}
CouponHelpModal.propTypes = {
  isPlaceCashStatus: PropTypes.bool,
  labels: PropTypes.objectOf(PropTypes.shape({})),
  openState: PropTypes.bool,
  onRequestClose: PropTypes.func,
  horizontalBar: PropTypes.bool,
};
CouponHelpModal.defaultProps = {
  isPlaceCashStatus: false,
  labels: {},
  openState: false,
  onRequestClose: () => {},
  horizontalBar: false,
};
export default CouponHelpModal;
