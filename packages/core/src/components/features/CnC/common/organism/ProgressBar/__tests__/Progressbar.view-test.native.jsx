// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ProgressbarVanilla } from '../views/Progressbar.view';

describe('ProgressbarVanilla', () => {
  const props = {
    labels: {
      title1: 'Spend another ',
      title2: ' to get a ',
      title3: 'Place Bucks ',
      title4: 'coupon',
      title5: 'to use',
    },
    currency: '$',
    className: 'variable-width',
    placeCashReward: 10,
    amountToNextReward: '9.5',
    width: '70%',
    isLeftProgressBar: false,
  };

  it('should render correctly', () => {
    const shallowComponent = shallow(<ProgressbarVanilla {...props} />);
    expect(shallowComponent).toMatchSnapshot();
  });

  it('should render correctly without left progress bar', () => {
    const shallowComponent = shallow(<ProgressbarVanilla {...props} isLeftProgressBar />);
    expect(shallowComponent).toMatchSnapshot();
  });
});
