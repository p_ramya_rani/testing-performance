// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const ProgressbarContainer = styled.View`
  margin: 5px;
`;
const ProgressbarTextWrapper = styled.View`
  display: flex;
  flex-direction: row;
  margin: 0px 0px 10px 0px;
  flex-wrap: wrap;
  justify-content: center;
`;
const ProgressbarStatusContainer = styled.View`
  width: 100%;
  border: 1px solid black;
`;

const checkProgressBarBorderColor = props => {
  const { theme } = props;
  if (props.isPlaceReward)
    return props.isPlcc ? theme.colors.REWARDS.PLCC : theme.colors.REWARDS.NON_PLCC;
  return theme.colors.REWARDS.PLACE_CASH;
};

const ProgressbarStatusPlaceRewardContainer = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 0px;
  padding: 0px 5px;
  justify-content: center;
`;

const MyPlaceRewardImageView = styled.Image`
  height: 13px;
  width: 160px;
  position: relative;
  top: ${props => (props.androidPlatform ? '6px' : '4px')};
  display: flex;
`;

const MyPlaceRewardImageViewParent = styled.View`
  width: 164px;
  height: 13px;
  padding: 0px 2px;
  display: flex;
  position: relative;
`;

const ProgressbarStatusPlaceReward = styled.View`
  width: 70%;
  border: 1px solid ${checkProgressBarBorderColor};
  height: 22px;
  border-radius: 14px;
  margin: 0px 15px 5px 15px;
  overflow: hidden;
`;

const PlaceCashImageBackground = styled.ImageBackground`
  height: 100%;
  width: ${props => props.progressWidth};
`;

export {
  ProgressbarContainer,
  ProgressbarTextWrapper,
  ProgressbarStatusContainer,
  ProgressbarStatusPlaceRewardContainer,
  ProgressbarStatusPlaceReward,
  MyPlaceRewardImageView,
  MyPlaceRewardImageViewParent,
  PlaceCashImageBackground,
};
