// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import Modal from '../../../../../../common/molecules/Modal';
import { RichText } from '../../../../../../common/atoms';

class CouponHelpModal extends React.PureComponent {
  showContent = () => {
    const { isPlaceCashStatus, labels } = this.props;
    if (isPlaceCashStatus) {
      return !!labels.PLACE_CASH_CONTENT;
    }
    return !!labels.NEED_HELP_RICH_TEXT;
  };

  render() {
    const {
      openState,
      onRequestClose,
      labels,
      additionalClassNameModal,
      inheritedStyles,
      isPlaceCashStatus,
    } = this.props;
    return (
      <Modal
        isOpen={openState}
        onRequestClose={onRequestClose}
        overlayClassName="TCPModal__Overlay"
        className="TCPModal__Content"
        maxWidth="500px"
        minHeight="90%"
        heightConfig={{ height: '90%' }}
        closeIconDataLocator="helpmodalcrossicon"
        fixedWidth
        contentLabel="Coupon Apply Help"
        customWrapperClassName={additionalClassNameModal}
        inheritedStyles={inheritedStyles}
      >
        {this.showContent() ? (
          <RichText
            richTextHtml={
              isPlaceCashStatus ? labels.PLACE_CASH_CONTENT : labels.NEED_HELP_RICH_TEXT
            }
          />
        ) : (
          <>
            <div className="elem-mt-LRG" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <div className="elem-mb-LRG" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
          </>
        )}
      </Modal>
    );
  }
}

CouponHelpModal.propTypes = {
  isPlaceCashStatus: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  openState: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  additionalClassNameModal: PropTypes.string.isRequired,
  inheritedStyles: PropTypes.shape({}).isRequired,
};
export default CouponHelpModal;
