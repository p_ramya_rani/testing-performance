// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field, change } from 'redux-form';
import ModuleX from '@tcp/core/src/components/common/molecules/ModuleX';

import withStyles from '../../../../../../common/hoc/withStyles';
import { TextBox, BodyCopy, Row, Col } from '../../../../../../common/atoms';

import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import { getLocator } from '../../../../../../../utils';
import AirmilesToolTip from './AirmilesToolTip.view';
import styles from '../styles/AirmilesBanner.style';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';

class AirmilesBanner extends React.PureComponent {
  constructor(props) {
    super(props);
    const { airmilesBannerData } = props;
    this.Input = React.createRef();
    this.state = {
      touched: false,
      expanded: true,
      isValidPromoField: !(airmilesBannerData && !!airmilesBannerData.collectorNumber),
    };
  }

  componentDidMount() {
    const { promoField, isMiniBag, airmilesBannerData } = this.props;
    if (promoField) {
      this.updateExpandedState(promoField);
    }
    if (isMiniBag && airmilesBannerData.collectorNumber) {
      this.updateExpandedState(airmilesBannerData.collectorNumber);
    }
  }

  componentDidUpdate(prevProps) {
    const { airmilesBannerData, dispatch } = this.props;
    const { airmilesBannerData: prevAirmilesBannerData } = prevProps;

    /* istanbul ignore else */
    if (
      (!prevAirmilesBannerData.collectorNumber &&
        airmilesBannerData.collectorNumber &&
        airmilesBannerData.collectorNumber !== prevAirmilesBannerData.collectorNumber) ||
      (!prevAirmilesBannerData.offerCode &&
        airmilesBannerData.offerCode &&
        airmilesBannerData.offerCode !== prevAirmilesBannerData.offerCode)
    ) {
      dispatch(change('AirmilesBanner', 'promoId', airmilesBannerData.collectorNumber));
      dispatch(change('AirmilesBanner', 'offerCode', airmilesBannerData.offerCode));
      this.updateState({ isValidPromoField: true, expanded: false });
    }
  }

  updateExpandedState = (promoField) => {
    const isPromoFieldEmptied = promoField === '';
    const isValidPromoField = this.checkIsValidPromoField(promoField);
    if (isValidPromoField || isPromoFieldEmptied) {
      this.updateState({ isValidPromoField, expanded: false });
    }
  };

  updateState = ({ isValidPromoField, expanded }, callbackMethod) => {
    this.setState(
      {
        isValidPromoField,
        expanded,
      },
      () => {
        if (callbackMethod) {
          callbackMethod();
        }
      }
    );
  };

  renderTextBox = ({ input, ...otherParams }) => {
    // eslint-disable-next-line
    input = { ...input, value: input.value.toUpperCase() };
    return <TextBox input={input} {...otherParams} />;
  };

  handleEditClick = () => this.setState({ expanded: true });

  toggleTouched = () => {
    const { touched } = this.state;
    this.setState({ touched: !touched });
  };

  checkIsValidPromoField = (promoField) => {
    return !!promoField && promoField.length > 10 && promoField.match(/^[0-9]+$/);
  };

  handleSubmit = () => {
    const { onAddAirmilesBanner, promoField, offerField } = this.props;
    const { ref: { current: { props: { initial } = {} } = {} } = {} } = this.Input.current || {};
    const { touched } = this.state;
    /* istanbul ignore else */
    if (touched) {
      this.toggleTouched();
    }
    if ((this.checkIsValidPromoField(promoField) || promoField === '') && initial !== offerField) {
      onAddAirmilesBanner();
    }
  };

  updatePromoCode = (e) => {
    const { onAddAirmilesBanner } = this.props;
    const promoField = e && e.target ? e.target.value : '';
    const isPromoFieldEmptied = promoField === '';
    const isValidPromoField = this.checkIsValidPromoField(promoField);
    if (isValidPromoField || isPromoFieldEmptied) {
      this.updateState({ isValidPromoField, expanded: false }, onAddAirmilesBanner);
    }
  };

  render() {
    const {
      className,
      airmilesBannerData: { collectorNumber },
      labels,
      promoField,
      airmilesModuleXContent,
      fromPage,
      isMiniBag,
    } = this.props;
    const richText =
      airmilesModuleXContent &&
      airmilesModuleXContent.size &&
      airmilesModuleXContent.getIn([0, 'richText']);
    const { expanded, isValidPromoField } = this.state;
    return (
      <div className={className}>
        <div className="coupon_form_container">
          {richText && <ModuleX richTextList={[{ text: richText }]} />}
          <BodyCopy
            bodySize="one"
            fontFamily="secondary"
            fontSize="fs10"
            textAlign="left"
            color="secondary"
          >
            {labels.headerText}
          </BodyCopy>
          <form className="coupon_submit_form">
            <Row
              className="airmilesRow"
              data-locator={getLocator('airnmileBannerinput_item_label')}
            >
              <Col className="airmilesBannerInput" colSize={{ large: 6, medium: 8, small: 3 }}>
                {!!isValidPromoField && !expanded ? (
                  <Row className="editButton">
                    <Col className="disabledText" colSize={{ large: 10, medium: 6, small: 4 }}>
                      {!promoField ? collectorNumber : promoField}
                    </Col>
                    <Col colSize={{ large: 2, medium: 2, small: 2 }}>
                      <BodyCopy
                        bodySize="one"
                        fontFamily="secondary"
                        fontSize="fs10"
                        textAlign="left"
                        color="secondary"
                        onClick={this.handleEditClick}
                        className="editCursor"
                      >
                        Edit
                      </BodyCopy>
                    </Col>
                  </Row>
                ) : (
                  <Field
                    id={`${fromPage}_promoId`}
                    placeholder={labels.collectorNumber}
                    name="promoId"
                    type="text"
                    component={TextBox}
                    maxLength={11}
                    dataLocator="airmile-banner-collectorNumber"
                    enableSuccessCheck={false}
                    className="upperCaseOffer"
                    autoComplete="off"
                    onChange={this.updatePromoCode}
                  />
                )}
                <AirmilesToolTip
                  toolTipText={labels.collectorFlyout}
                  altText={labels.infoIconText}
                  isMiniBag={isMiniBag}
                />
              </Col>
              <Col className="airmilesBannerInput" colSize={{ large: 6, medium: 8, small: 3 }}>
                <Field
                  id={`${fromPage}_offerCode`}
                  placeholder={labels.offerCode}
                  name="offerCode"
                  type="text"
                  component={TextBox}
                  maxLength={50}
                  dataLocator="airmile-banner-offerCode"
                  enableSuccessCheck={false}
                  onBlur={this.handleSubmit}
                  ref={this.Input}
                  autoComplete="off"
                />
                <AirmilesToolTip
                  toolTipText={labels.offerFlyout}
                  altText={labels.infoIconText}
                  isMiniBag={isMiniBag}
                />
              </Col>
            </Row>
          </form>
          <BodyCopy
            bodySize="one"
            fontFamily="secondary"
            fontSize="fs10"
            textAlign="center"
            color="secondary"
          >
            {labels.footerText}
          </BodyCopy>
        </div>
      </div>
    );
  }
}

AirmilesBanner.propTypes = {
  labels: PropTypes.shape({}),
  airmilesBannerData: PropTypes.shape({}),
  onAddAirmilesBanner: PropTypes.func,
  orderId: PropTypes.string,
  handleSubmit: PropTypes.func,
  promoField: PropTypes.shape({}).isRequired,
  isMiniBag: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  airmilesModuleXContent: PropTypes.shape({}).isRequired,
  fromPage: PropTypes.string.isRequired,
  offerField: PropTypes.string.isRequired,
};
AirmilesBanner.defaultProps = {
  airmilesBannerData: {},
  labels: {},
  onAddAirmilesBanner: () => {},
  orderId: ' ',
  handleSubmit: () => {},
};

const validateMethod = createValidateMethod(getStandardConfig(['promoId']));

export default compose(
  connect((state, props) => {
    const formName = props.isMiniBag ? 'AirmilesBannerMiniBag' : 'AirmilesBanner';
    return {
      form: formName,
      ...validateMethod,
      destroyOnUnmount: false,
      enableReinitialize: true,
    };
  }),
  reduxForm()
)(withStyles(AirmilesBanner, styles));

export { AirmilesBanner };
