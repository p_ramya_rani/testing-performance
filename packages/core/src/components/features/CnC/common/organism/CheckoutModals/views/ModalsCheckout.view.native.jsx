// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
import OpenLoginModal from '../../../../../account/LoginPage/container/LoginModal.container';
import CheckoutConstants from '../../../../Checkout/Checkout.constants';
import BagConfirmationModal from '../../../../BagPage/views/BagConfirmationModal.view';
import ItemDeleteConfirmationModal from '../../../../BagPage/views/ItemDeleteConfirmationModal.view';

class ModalsCheckout extends React.PureComponent {
  navigateToCheckout = stage => {
    const { setCheckoutStage, navigation, closeAddedToBagModal } = this.props;
    setTimeout(closeAddedToBagModal);
    setCheckoutStage(stage);
    navigation.navigate({ routeName: CheckoutConstants.CHECKOUT_ROOT });
  };

  routeToCheckout = e => {
    const {
      closeCheckoutModalMountState,
      routeForBagCheckout,
      closeModal,
      navigation,
      closeAddedToBagModal,
    } = this.props;
    if (e) {
      e.preventDefault();
    }
    routeForBagCheckout({ navigation, closeModal, navigationActions: NavigationActions });
    setTimeout(closeAddedToBagModal);
    closeCheckoutModalMountState({ state: false });
  };

  closeModalAndHandleCheckout = () => {
    const { handleCartCheckout, navigation, closeAddedToBagModal } = this.props;
    this.closeCheckoutConfirmationModal();
    return handleCartCheckout({
      navigation,
      closeModal: closeAddedToBagModal,
      navigationActions: NavigationActions,
    });
  };

  closeCheckoutConfirmationModal = () => {
    const {
      closeCheckoutConfirmationModal,
      closeModal,
      setPayPalModalActionOOS,
      setApplePayModalActionOOS,
      modalInfo,
      navigation,
    } = this.props;
    closeCheckoutConfirmationModal();
    setPayPalModalActionOOS(false);
    setApplePayModalActionOOS(false);
    const { fromApplePayBillingPage } = modalInfo;
    if (fromApplePayBillingPage) navigation.navigate('BagPage');
    if (closeModal) {
      setTimeout(() => {
        closeModal();
      });
    }
  };

  render() {
    const {
      labels,
      checkoutModalMountedState,
      closeCheckoutModalMountState,
      removeUnqualifiedItemsAndCheckout,
      modalInfo,
      currentSelectItemInfo,
      closeItemDeleteModal,
      deleteConfirmationModalLabels,
      addItemToSflList,
      confirmRemoveCartItem,
      isPayPalModalActionOOS,
      isApplePayModalActionOOS,
    } = this.props;
    const { showModal, isEditingItem: modalEditingItem } = modalInfo;
    if (modalEditingItem) {
      labels.confirmationText = labels.editConfirmationText;
    }
    return (
      <>
        <OpenLoginModal
          component="login"
          variation="checkout"
          openState={checkoutModalMountedState}
          setLoginModalMountState={closeCheckoutModalMountState}
          handleContinueAsGuest={this.routeToCheckout}
          handleAfterLogin={this.routeToCheckout}
        />
        <BagConfirmationModal
          labels={labels}
          isOpen={showModal}
          closeCheckoutConfirmationModal={this.closeCheckoutConfirmationModal}
          removeUnqualifiedItemsAndCheckout={
            modalEditingItem ? this.closeModalAndHandleCheckout : removeUnqualifiedItemsAndCheckout
          }
          isPayPalModalActionOOS={isPayPalModalActionOOS}
          isApplePayModalActionOOS={isApplePayModalActionOOS}
        />
        <ItemDeleteConfirmationModal
          isOpen={currentSelectItemInfo.showModal}
          closeCheckoutConfirmationModal={closeItemDeleteModal}
          labels={deleteConfirmationModalLabels}
          moveToSfl={() => addItemToSflList(currentSelectItemInfo)}
          confirmRemoveCartItem={() =>
            confirmRemoveCartItem(currentSelectItemInfo.itemId, currentSelectItemInfo.price)
          }
        />
      </>
    );
  }
}

ModalsCheckout.propTypes = {
  labels: PropTypes.shape.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  closeModal: PropTypes.func,
  setCheckoutStage: PropTypes.func.isRequired,
  closeAddedToBagModal: PropTypes.func.isRequired,
  closeCheckoutModalMountState: PropTypes.func.isRequired,
  handleCartCheckout: PropTypes.func.isRequired,
  closeCheckoutConfirmationModal: PropTypes.func.isRequired,
  checkoutModalMountedState: PropTypes.bool,
  removeUnqualifiedItemsAndCheckout: PropTypes.func.isRequired,
  modalInfo: PropTypes.shape({}).isRequired,
  currentSelectItemInfo: PropTypes.shape({}).isRequired,
  closeItemDeleteModal: PropTypes.func.isRequired,
  deleteConfirmationModalLabels: PropTypes.shape({}).isRequired,
  addItemToSflList: PropTypes.func.isRequired,
  confirmRemoveCartItem: PropTypes.func.isRequired,
  routeForBagCheckout: PropTypes.func,
  setPayPalModalActionOOS: PropTypes.func,
  setApplePayModalActionOOS: PropTypes.func,
  isPayPalModalActionOOS: PropTypes.bool,
  isApplePayModalActionOOS: PropTypes.bool,
};

ModalsCheckout.defaultProps = {
  closeModal: () => {},
  checkoutModalMountedState: false,
  routeForBagCheckout: () => {},
  setPayPalModalActionOOS: () => {},
  setApplePayModalActionOOS: () => {},
  isPayPalModalActionOOS: false,
  isApplePayModalActionOOS: false,
};

export default ModalsCheckout;
export { ModalsCheckout as ModalsCheckoutVanilla };
