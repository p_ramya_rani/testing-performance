// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { isMobileWeb } from '@tcp/core/src/utils';
import { CnCTemplateVanilla } from '../views/CnCTemplate.view';
import SigninCtaBanner from '../../../molecules/SigninCtaBanner';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileWeb: jest.fn(),
  getViewportInfo: () => ({ isDesktop: true, isTablet: false, width: 1440 }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isClient: jest.fn().mockImplementation(() => true),
  getIconPath: jest.fn(),
  isMobileApp: jest.fn(),
  getLabelValue: jest.fn(),
  getStaticFilePath: jest.fn,
}));

describe('CnC template', () => {
  it('should render correctly', () => {
    const props = {
      leftSection: {},
      bagActions: {},
      showLeftSection: true,
      className: 'Cnc',
      header: {},
      isCheckoutView: false,
      isGuest: false,
      isConfirmationPage: false,
      labels: {},
    };
    const tree = shallow(<CnCTemplateVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with guest', () => {
    const props = {
      leftSection: {},
      bagActions: {},
      showLeftSection: true,
      className: 'Cnc',
      header: {},
      isCheckoutView: true,
      isGuest: true,
      isConfirmationPage: true,
      labels: {},
    };
    const tree = shallow(<CnCTemplateVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should render SigninCtaBanner', () => {
    isMobileWeb.mockImplementation(() => true);
    const props = {
      leftSection: {},
      bagActions: {},
      showLeftSection: true,
      className: 'Cnc',
      header: {},
      isCheckoutView: true,
      isGuest: true,
      isConfirmationPage: true,
      isBagPage: true,
      isNoNEmptyBag: true,
      isSignInBannerAbTestEnabled: true,
      rememberedUser: false,
    };
    const tree = shallow(<CnCTemplateVanilla {...props} />);
    setTimeout(() => {
      expect(tree.find('.left-sec').find(SigninCtaBanner)).toHaveLength(1);
      expect(tree).toMatchSnapshot();
    });
  });

  it('should not render SigninCtaBanner', () => {
    isMobileWeb.mockImplementation(() => true);
    const props = {
      leftSection: {},
      bagActions: {},
      showLeftSection: true,
      className: 'Cnc',
      header: {},
      isCheckoutView: true,
      isGuest: true,
      isConfirmationPage: true,
      isBagPage: true,
      isNoNEmptyBag: true,
      isSignInBannerAbTestEnabled: false,
      rememberedUser: false,
    };
    const tree = shallow(<CnCTemplateVanilla {...props} />);
    setTimeout(() => {
      expect(tree.find('.left-sec').find(SigninCtaBanner)).toHaveLength(0);
      expect(tree.find('.right-sec').find(SigninCtaBanner)).toHaveLength(0);
      expect(tree).toMatchSnapshot();
    });
  });

  it('should not render SigninCtaBanner in right-sec', () => {
    isMobileWeb.mockImplementation(() => false);
    const props = {
      leftSection: {},
      bagActions: {},
      showLeftSection: true,
      className: 'Cnc',
      header: {},
      isCheckoutView: true,
      isGuest: true,
      isConfirmationPage: false,
      isBagPage: true,
      isNoNEmptyBag: true,
      isSignInBannerAbTestEnabled: true,
      rememberedUser: false,
    };
    const tree = shallow(<CnCTemplateVanilla {...props} />);
    setTimeout(() => {
      expect(tree.find('.right-sec').find(SigninCtaBanner)).toHaveLength(1);
      expect(tree).toMatchSnapshot();
    });
  });
  it('should not render SigninCtaBanner for empty bag', () => {
    isMobileWeb.mockImplementation(() => true);
    const props = {
      leftSection: {},
      bagActions: {},
      showLeftSection: true,
      className: 'Cnc',
      header: {},
      isCheckoutView: true,
      isGuest: true,
      isConfirmationPage: true,
      isBagPage: true,
      isNoNEmptyBag: false,
      isSignInBannerAbTestEnabled: true,
      rememberedUser: false,
    };
    const tree = shallow(<CnCTemplateVanilla {...props} />);
    setTimeout(() => {
      expect(tree.find('left-sec').find(SigninCtaBanner)).toHaveLength(0);
      expect(tree).toMatchSnapshot();
    });
  });
  it('should not render SigninCtaBanner for non bag page', () => {
    isMobileWeb.mockImplementation(() => true);
    const props = {
      leftSection: {},
      bagActions: {},
      showLeftSection: true,
      className: 'Cnc',
      header: {},
      isCheckoutView: true,
      isGuest: true,
      isConfirmationPage: true,
      isBagPage: false,
      isNoNEmptyBag: true,
      isSignInBannerAbTestEnabled: true,
      rememberedUser: false,
    };
    const tree = shallow(<CnCTemplateVanilla {...props} />);
    setTimeout(() => {
      expect(tree.find('left-sec').find(SigninCtaBanner)).toHaveLength(0);
      expect(tree).toMatchSnapshot();
    });
  });
  it('should not render SigninCtaBanner for remembered user', () => {
    isMobileWeb.mockImplementation(() => true);
    const props = {
      leftSection: {},
      bagActions: {},
      showLeftSection: true,
      className: 'Cnc',
      header: {},
      isCheckoutView: true,
      isGuest: true,
      isConfirmationPage: true,
      isBagPage: true,
      isNoNEmptyBag: true,
      isSignInBannerAbTestEnabled: true,
      rememberedUser: true,
    };
    const tree = shallow(<CnCTemplateVanilla {...props} />);
    setTimeout(() => {
      expect(tree.find('left-sec').find(SigninCtaBanner)).toHaveLength(0);
      expect(tree.find('right-sec').find(SigninCtaBanner)).toHaveLength(0);
      expect(tree).toMatchSnapshot();
    });
  });
});
