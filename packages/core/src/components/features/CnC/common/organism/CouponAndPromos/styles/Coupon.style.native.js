// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components';

const styles = css`
  .coupon_list {
    margin-top: 0px;
  }
`;

const WrapperStyle = styled.View``;

const CouponListContainer = styled.View`
  background-color: rgba(243, 243, 243, 1);
`;

const StyledHeader = styled.View`
  padding-left: 14px;
`;

export { styles, WrapperStyle, CouponListContainer, StyledHeader };
