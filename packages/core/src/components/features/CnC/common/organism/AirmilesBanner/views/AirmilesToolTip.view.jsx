// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getIconPath } from '@tcp/core/src/utils';
import ReactToolTip from '../../../../../../common/atoms/ReactToolTip';
import { Image } from '../../../../../../common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/AirmilesToolTip.style';

const AirmilesToolTip = ({ toolTipText, altText, isMiniBag }) => {
  return (
    <>
      <span className="airmileBannerTooltip">
        <span className="info-icon-img-wrapper">
          <ReactToolTip message={toolTipText} aligned="right">
            <Image
              src={getIconPath('info-icon')}
              alt={altText}
              className={`${isMiniBag ? 'info-icon-ca' : ''}`}
            />
          </ReactToolTip>
        </span>
      </span>
    </>
  );
};

AirmilesToolTip.propTypes = {
  toolTipText: PropTypes.string.isRequired,
  altText: PropTypes.string.isRequired,
};
export default withStyles(AirmilesToolTip, styles);
