// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getIconPath, isMobileApp } from '@tcp/core/src/utils';

import { getProductDetails } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import ErrorMessage from '@tcp/core/src/components/features/CnC/common/molecules/ErrorMessage';

import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import RemoveSoldOut from '../../RemoveSoldOut';
import CARTPAGE_CONSTANTS from '../../../../CartItemTile/CartItemTile.constants';

import style, { bagTileCSS, customStyles } from '../styles/InformationHeader.style';

class InformationHeader extends React.PureComponent {
  getTickIcon = () => {
    return <Image alt="closeIcon" className="tick-icon" src={getIconPath('circle-check-fill')} />;
  };

  renderBagpageServerSflErrors = (styles) => {
    const { bagPageServerError, cartItemSflError, isModalOpen, isPickupModalOpen } = this.props;
    let errorMsg = '';
    if (cartItemSflError) {
      errorMsg = cartItemSflError;
    } else if (
      bagPageServerError &&
      bagPageServerError.errorMessage &&
      !bagPageServerError.isMinibag
    ) {
      errorMsg = bagPageServerError.errorMessage;
    }

    return (
      !isModalOpen &&
      !isPickupModalOpen && <ErrorMessage bagPage customClass={styles} error={errorMsg} />
    );
  };

  /**
   * @method renderUpdatingBagItemSuccessfulMsg
   * @description render message once item get updated.
   * @memberof ProductTileWrapper
   */
  renderUpdatingBagItemSuccessfulMsg = (isUpdating) => {
    const { labels } = this.props;
    return (
      isUpdating &&
      !isMobileApp() && (
        <div className="delete-msg">
          {this.getTickIcon()}
          <BodyCopy
            component="span"
            fontSize="fs12"
            textAlign="center"
            fontFamily="secondary"
            fontWeight="extrabold"
          >
            {labels.itemUpdated}
          </BodyCopy>
        </div>
      )
    );
  };

  render() {
    const {
      confirmRemoveCartItem,
      isBagPageSflSection,
      labels,
      orderItems,
      pageView,
      isUnavailable,
      isSoldOut,
      getUnavailableOOSItems,
      className,
      isNewBag,
    } = this.props;

    const styles = pageView === 'myBag' ? bagTileCSS : customStyles;
    const showError =
      orderItems &&
      orderItems.find((tile) => {
        const productDetail = getProductDetails(tile);
        return (
          productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY_SOLDOUT ||
          productDetail.miscInfo.availability === CARTPAGE_CONSTANTS.AVAILABILITY_UNAVAILABLE
        );
      });

    return (
      <div className={`${className} information-header`}>
        {showError && !isNewBag && (
          <ErrorMessage bagPage isBagPage customClass={styles} error={labels.problemWithOrder} />
        )}
        {this.renderBagpageServerSflErrors(styles)}
        {!isBagPageSflSection && isSoldOut && (
          <RemoveSoldOut
            labels={labels}
            removeCartItem={confirmRemoveCartItem}
            getUnavailableOOSItems={getUnavailableOOSItems}
            isSoldOut={isSoldOut}
            pageView={pageView}
            isNewBag={isNewBag}
          />
        )}
        {!isBagPageSflSection && isUnavailable && (
          <RemoveSoldOut
            labels={labels}
            removeCartItem={confirmRemoveCartItem}
            getUnavailableOOSItems={getUnavailableOOSItems}
            isUnavailable={isUnavailable}
            pageView={pageView}
            isNewBag={isNewBag}
          />
        )}
      </div>
    );
  }
}

InformationHeader.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  styles: PropTypes.shape({}).isRequired,
  orderItems: PropTypes.shape([]).isRequired,
  pageView: PropTypes.string,
  isUnavailable: PropTypes.bool.isRequired,
  isSoldOut: PropTypes.bool.isRequired,
  getUnavailableOOSItems: PropTypes.shape([]),
  confirmRemoveCartItem: PropTypes.func.isRequired,
  isBagPageSflSection: PropTypes.bool,
  isCartItemSFL: PropTypes.bool.isRequired,
  isCartItemsUpdating: PropTypes.shape({}).isRequired,
  isSflItemRemoved: PropTypes.bool.isRequired,
  cartItemSflError: PropTypes.string,
  bagPageServerError: PropTypes.shape({}),
  isModalOpen: PropTypes.bool.isRequired,
  isPickupModalOpen: PropTypes.bool.isRequired,
  isSflItemMovedToBag: PropTypes.bool,
  isNewBag: PropTypes.bool,
};

InformationHeader.defaultProps = {
  className: '',
  isBagPageSflSection: false,
  pageView: '',
  getUnavailableOOSItems: [],
  cartItemSflError: '',
  bagPageServerError: null,
  isSflItemMovedToBag: false,
  isNewBag: false,
};

export default withStyles(InformationHeader, style);

export { InformationHeader };
