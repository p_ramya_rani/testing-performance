// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { Button } from '../../../../../../common/atoms';

const FlexRow = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

const FormContainer = styled.View`
  padding: 18px 16px 12px 14px;
`;

const Header = styled.View`
  display: flex;
  flex-direction: row;
  margin: 0px;
  margin-bottom: 18px;
  align-items: flex-end;
`;

const InputField = styled.View`
  display: flex;
  flex: 1;
  padding-right: 8px;
`;

const StyledButton = styled(Button)`
  height: 42px;
  width: 134px;
  opacity: ${props => (props.disabled ? '0.6' : '1.0')};
`;

const CouponFormContainer = styled.View`
  margin-top: 15;
`;

const NeedHelpText = { paddingLeft: 12, paddingBottom: 4 };

export {
  FlexRow,
  Header,
  InputField,
  StyledButton,
  FormContainer,
  CouponFormContainer,
  NeedHelpText,
};
