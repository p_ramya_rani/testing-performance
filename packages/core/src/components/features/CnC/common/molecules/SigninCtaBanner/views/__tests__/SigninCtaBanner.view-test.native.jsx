import React from 'react';
import { shallow } from 'enzyme';
import OpenLoginModal from '@tcp/core/src/components/features/account/LoginPage/container/LoginModal.container';
import SigninCtaBanner from '../SigninCtaBanner.view.native';
import ClickTracker from '../../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

describe('CheckoutSectionTitleDisplay', () => {
  it('should render correctly', () => {
    const tree = shallow(<SigninCtaBanner />);
    expect(tree).toMatchSnapshot();
  });

  it('should open the login modal', () => {
    const tree = shallow(<SigninCtaBanner />);
    tree
      .find(ClickTracker)
      .at(0)
      .simulate('press', { preventDefault: jest.fn() });
    expect(tree.find(OpenLoginModal)).toHaveLength(1);
  });
});
