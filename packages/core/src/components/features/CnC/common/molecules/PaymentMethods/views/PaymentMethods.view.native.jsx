// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import { Field, change } from 'redux-form';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Image from '@tcp/core/src/components/common/atoms/Image';
import { getIconCard } from '@tcp/core/src/utils/index.native';
import PaymentMethodTypes from '../../../atoms/PaymentMethodTypes';
import {
  FieldWrapper,
  PaymentMethodsWrapper,
  SecondRowFieldWrapper,
  SecondRowLabelWrapper,
  SecondRowLabelImageWrapper,
} from '../styles/PaymentMethods.style.native';

export const onPaymentMethodChange = ({ id, dispatch, formName }) => {
  dispatch(change(formName, 'paymentMethodId', id));
};

const getAppleEnabled = (isApplePayEnabled, isApplePayEnabledOnBilling) => {
  return isApplePayEnabled && isApplePayEnabledOnBilling;
};

const checkId = (isApplePayVisible, appleId, id, showSecondRow, title) => {
  if (id === appleId && !isApplePayVisible) {
    return false;
  }
  if (showSecondRow && title === 'Venmo') {
    return false;
  }

  return true;
};

export const SecondOptionsRow = ({
  labels,
  paymentMethods,
  selectedPaymentId,
  dispatch,
  formName,
}) => {
  const [showOptions, setShowOptions] = useState(false);
  const { otherPaymentOptions } = labels;
  return (
    <>
      {!showOptions && (
        <SecondRowLabelWrapper>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs13"
            onPress={() => setShowOptions(true)}
            text={otherPaymentOptions}
            textDecoration="underline"
          />
          <SecondRowLabelImageWrapper>
            <Image source={getIconCard('venmo-transparent')} width="45px" height="10px" />
          </SecondRowLabelImageWrapper>
        </SecondRowLabelWrapper>
      )}
      {showOptions && (
        <PaymentMethodsWrapper>
          <SecondRowFieldWrapper>
            {paymentMethods &&
              paymentMethods.length > 0 &&
              paymentMethods.map((option, index) => {
                const { id, displayName } = option;
                const title = displayName;
                const checkVisibility = title === 'Venmo';
                return (
                  checkVisibility && (
                    <Field
                      component={PaymentMethodTypes}
                      id={id}
                      selectedPaymentId={selectedPaymentId}
                      index={index}
                      name="paymentMethodId"
                      title={title}
                      onPress={() => onPaymentMethodChange({ id, dispatch, formName })}
                      labels={labels}
                    />
                  )
                );
              })}
          </SecondRowFieldWrapper>
        </PaymentMethodsWrapper>
      )}
    </>
  );
};

SecondOptionsRow.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  paymentMethods: PropTypes.shape([]).isRequired,
  selectedPaymentId: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  formName: PropTypes.string.isRequired,
};

const showPaymentMethodsSecondRow = (
  isAfterPayEnabled,
  isApplePayEnabled,
  isVenmoEnabled,
  isPayPalEnabled
) => {
  return isAfterPayEnabled && isApplePayEnabled && isVenmoEnabled && isPayPalEnabled;
};

const PaymentMethods = ({
  paymentMethods,
  selectedPaymentId,
  dispatch,
  formName,
  isApplePayEnabled,
  appleId,
  isApplePayEnabledOnBilling,
  labels,
  isAfterPayEnabled,
  isVenmoEnabled,
  isPayPalEnabled,
}) => {
  const isApplePayVisible = getAppleEnabled(isApplePayEnabled, isApplePayEnabledOnBilling);
  const showSecondRow = showPaymentMethodsSecondRow(
    isAfterPayEnabled,
    isApplePayEnabled,
    isVenmoEnabled,
    isPayPalEnabled
  );
  return (
    <>
      <PaymentMethodsWrapper>
        <FieldWrapper>
          {paymentMethods &&
            paymentMethods.length > 0 &&
            paymentMethods.map((option, index) => {
              const { id, displayName } = option;
              const title = displayName;
              const checkVisibility = checkId(isApplePayVisible, appleId, id, showSecondRow, title);
              return (
                checkVisibility && (
                  <Field
                    component={PaymentMethodTypes}
                    id={id}
                    selectedPaymentId={selectedPaymentId}
                    index={index}
                    name="paymentMethodId"
                    title={title}
                    onPress={() => onPaymentMethodChange({ id, dispatch, formName })}
                    labels={labels}
                  />
                )
              );
            })}
        </FieldWrapper>
      </PaymentMethodsWrapper>
      {showSecondRow && (
        <SecondOptionsRow
          labels={labels}
          paymentMethods={paymentMethods}
          selectedPaymentId={selectedPaymentId}
          dispatch={dispatch}
          formName={formName}
        />
      )}
    </>
  );
};

PaymentMethods.propTypes = {
  paymentMethods: PropTypes.shape([]),
  selectedPaymentId: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
  formName: PropTypes.string.isRequired,
  isApplePayEnabled: PropTypes.bool.isRequired,
  isApplePayEnabledOnBilling: PropTypes.bool.isRequired,
  appleId: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  isAfterPayEnabled: PropTypes.bool,
  isVenmoEnabled: PropTypes.bool,
  isPayPalEnabled: PropTypes.bool,
};
PaymentMethods.defaultProps = {
  paymentMethods: null,
  selectedPaymentId: null,
  appleId: 'applepay',
  isAfterPayEnabled: false,
  isVenmoEnabled: false,
  isPayPalEnabled: false,
};

export default PaymentMethods;
