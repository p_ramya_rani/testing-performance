// 9fbef606107a605d69c0edbcd8029e5d 
import { takeLeading, put } from 'redux-saga/effects';
import { EddSaga, getEddDetails } from '../container/EddComponent.saga';
import { setEddData, showLoader, hideLoader, setEddError } from '../container/EddComponent.actions';
import EDD_CONSTANTS from '../EddComponent.constants';

describe('Edd saga', () => {
  it('should return correct takeLeading effect', () => {
    const generator = EddSaga();
    const takeLeadingDescriptor = generator.next().value;
    const expected = takeLeading(EDD_CONSTANTS.GET_EDD_DATA, EddSaga);
    expect(takeLeadingDescriptor.toString()).toMatch(expected.toString());
  });
});
describe('getEddDetails Saga', () => {
  it('should dispatch showLoader getEddDetails action', () => {
    const payload = { formPromise: {}, formData: {}, source: '' };
    const getEddDetailsSaga = getEddDetails({ payload });
    expect(getEddDetailsSaga.next().value).toEqual(put(showLoader()));
  });
  it('should dispatch setEddData getEddDetails action', () => {
    const payload = {};
    const getEddDetailsSaga = getEddDetails({ payload });
    getEddDetailsSaga.next();
    getEddDetailsSaga.next();
    getEddDetailsSaga.next();
    expect(getEddDetailsSaga.next(payload).value).toEqual(put(setEddData(payload)));
  });
  it('should dispatch hideLoader getEddDetails action', () => {
    const payload = {};
    const getEddDetailsSaga = getEddDetails({ payload });
    getEddDetailsSaga.next();
    getEddDetailsSaga.next();
    getEddDetailsSaga.next();
    getEddDetailsSaga.next();
    expect(getEddDetailsSaga.next().value).toEqual(put(hideLoader()));
  });
  it('should dispatch setEddError getEddDetails action', () => {
    const payload = {};
    const getEddDetailsSaga = getEddDetails({ payload });
    getEddDetailsSaga.next();
    expect(getEddDetailsSaga.throw().value).toEqual(put(setEddError(true)));
  });
});
