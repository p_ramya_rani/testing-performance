// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const MainSection = styled.View`
  flex: 1;
  padding-bottom: 1px;
  margin-bottom: 3px;
`;
export const RowSectionStyle = styled.View`
  width: 100%;
  flex: 1;
  flex-direction: row;
`;
