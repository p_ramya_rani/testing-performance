// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .coupon_form_container {
    padding: 18px 16px 12px 14px;
    box-sizing: border-box;
    background-color: ${(props) => props.theme.colors.WHITE};

    ${(props) =>
      props.newBag
        ? `.checkmark-container {display: none}
        .textbox_validation_success .TextBox__input, .TextBox__input {border: 1px solid ${props.theme.colors.FOOTER.DIVIDER};}`
        : ''}

    .coupon_form_heading {
      margin: 0px;
      margin-bottom: 18px;
    }
    .coupon_need_help_link {
      padding-left: 12px;
      text-decoration: underline;
      cursor: pointer;
    }
    .coupon_submit_form {
      display: flex;
      flex: 1;
      ${(props) =>
        props.newBag
          ? `padding:8px 8px 5px;border: 0.8px dashed ${props.theme.colorPalette.gray[800]};`
          : ''}
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        flex-direction: ${(props) => (props.newBag ? 'row' : 'column')};
      }
    }

    .input-fields-wrapper {
      padding-right: 8px;
      flex: 1;
      .TextBox__label {
        ${(props) =>
          props.newBag
            ? `font-size: 12px; left: 15px; top:-4px; background-color: ${props.theme.colors.WHITE};`
            : ''}
      }
      ${(props) =>
        props.newBag
          ? `margin:0px;
          .TextBox__label.TextBox__placeholder{
        top:12px;
        font-size:${props.theme.typography.fontSizes.fs12};
        @media ${props.theme.mediaQuery.smallOnly}{
          top:14px;
        }
        @media (max-width: ${props.theme.breakpoints.small}){
          top:15px;
        }
      }
      .TextBox__label.nonActive{
        top:-8px;
        font-size:${props.theme.typography.fontSizes.fs10};
      }
      `
          : ''}

      .textbox_validation_success .checkmark-container {
        ${(props) => (props.newBag ? `top: 8px;` : '')}
      }
      .TextBox__label.nonActive {
        ${(props) =>
          props.newBag
            ? `top:-4px;
            font-size:${props.theme.typography.fontSizes.fs10};`
            : ''}
      }

      .TextBox__input:focus ~ .TextBox__label {
        ${(props) => (props.newBag ? `top:-4px;` : '')}
      }

      input {
        height: ${(props) => (props.newBag ? `40px;` : '16px')};
        margin-bottom: 0;
        margin-top: 1px;
      }
    }

    .coupon_submit_button {
      height: 42px;
      width: 102px;
      ${(props) => (props.newBag ? `margin-top: 0;` : `margin-top:8px;`)}
      cursor: default;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        font-size: ${(props) => props.theme.typography.fontSizes.fs10};
      }
      @media ${(props) => props.theme.mediaQuery.mediumOnly} {
        width: 162px;
        align-self: center;
        ${(props) => (props.newBag ? `margin-top: 0; width: 80px;` : `margin-top: 18px;`)}
      }
      @media ${(props) => props.theme.mediaQuery.large} {
        ${(props) =>
          props.newBag
            ? `height: 36px; width: 80px;`
            : `height: 51px;
        width: 120px;`}
        margin-top: 0;
      }
      ${(props) =>
        props.newBag
          ? `padding:0px;background-color:${props.theme.colors.BUTTON.BLACK.NORMAL};border-radius: 6px; height:36px;color:${props.theme.colors.WHITE};`
          : ''}
    }

    .success__checkmark {
      visibility: hidden;
    }

    .TextBox__input {
      border-bottom: 1px solid ${(props) => props.theme.colors.FOOTER.DIVIDER};
      ${(props) => (props.newBag ? `box-sizing: border-box; padding: 16px;` : '')}
    }
  }

  .coupon_error_message {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      padding-bottom: 0px;
      padding-top: 0px;
      background-color: ${(props) => props.theme.colors.WHITE};
    }
  }
`;
