// 9fbef606107a605d69c0edbcd8029e5d 
export const props = {
  className: 'className',
  subtitle: 'EDD',
  itemsWithEdd: [
    {
      orderItemId: '1',
      itemPartNumber: '1',
      quantity: 1,
      node: '001',
      nodeProcessingTime: {
        UGNR: '2020-10-16',
      },
      transitTime: {
        UGNR: '2020-10-17',
      },
      shippingDates: {
        UGNR: '2020-10-18',
      },
    },
  ],
  orderEddDates: {},
  itemIdprop: '1',
  isRangeDate: false,
  showIcon: false,
  selectedShipment: 'UGNR',
  isShipping: false,
  labels: {
    arrivesBy: 'Arrives By',
  },
  fetchEdd: jest.fn(),
  fetchEddUpdate: jest.fn(),
  selectedShippingMethod: 'UGNR',
  updateShipmentCode: jest.fn(),
};

export const expressSelected = {
  orderItemId: '1',
  itemPartNumber: '1',
  quantity: 1,
  node: '001',
  nodeProcessingTime: {
    UGNR: '2020-10-20',
  },
  transitTime: {
    UGNR: '2020-10-20',
  },
  shippingDates: {
    UGNR: '2020-10-21',
  },
};

export const standardSplitQty = {
  orderItemId: '1',
  itemPartNumber: '1',
  quantity: 2,
  node: '001',
  nodeProcessingTime: {
    U2DY: '2020-10-18',
  },
  transitTime: {
    U2DY: '2020-10-19',
  },
  shippingDates: {
    U2DY: '2020-10-21',
  },
};
