// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Field, change } from 'redux-form';
import PropTypes from 'prop-types';
import EddComponent from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import ShippingMethodButtons from '../../../atoms/ShippingMethodButtons';
import {
  ShippingMsgWrapper,
  FieldWrapper,
  italicStyle,
  ShipmentMethodsWrapper,
} from '../styles/ShipmentMethods.style.native';

export const onShipmentMethodChange = ({
  id,
  dispatch,
  formName,
  formSection,
  updateShippingMethodSelection,
  isSfsInvMessagingEnabled,
}) => {
  dispatch(change(formName, `${formSection}.shippingMethodId`, id));
  if (isSfsInvMessagingEnabled && updateShippingMethodSelection) {
    updateShippingMethodSelection({ id });
  }
};

const ShipmentMethods = ({
  shipmentHeader,
  shipmentMethods,
  selectedShipmentId,
  dispatch,
  formName,
  formSection,
  isEDD,
  updateShippingMethodSelection,
  isSfsInvMessagingEnabled,
}) => {
  const selectedShipment =
    shipmentMethods && shipmentMethods.find((method) => method.id === selectedShipmentId);
  return (
    <ShipmentMethodsWrapper>
      <BodyCopy
        color="gray.900"
        fontWeight="semibold"
        fontFamily="secondary"
        fontSize="fs16"
        text={shipmentHeader}
        textAlign="left"
      />
      <FieldWrapper>
        {shipmentMethods &&
          shipmentMethods.length > 0 &&
          shipmentMethods.map((option, index) => {
            const { id, displayName, price, code } = option;
            const title = price > 0 ? `${displayName} - $${price}` : displayName;
            return (
              <>
                <Field
                  component={ShippingMethodButtons}
                  id={id}
                  selectedShipmentId={selectedShipmentId}
                  index={index}
                  name="shippingMethodId"
                  title={title}
                  onPress={() =>
                    onShipmentMethodChange({
                      id,
                      dispatch,
                      formName,
                      formSection,
                      updateShippingMethodSelection,
                      isSfsInvMessagingEnabled,
                    })
                  }
                  code={code}
                />
              </>
            );
          })}
      </FieldWrapper>
      {selectedShipment && isEDD && (
        <ShippingMsgWrapper>
          <EddComponent selectedShipment={selectedShipment} isRangeDate fontSize="fs16" showIcon />
        </ShippingMsgWrapper>
      )}

      {selectedShipment && !isEDD && (
        <ShippingMsgWrapper>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="semibold"
            text={`${selectedShipment.displayName || ''} - `}
          />
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="semibold"
            style={italicStyle}
            text={` ${selectedShipment.shippingSpeed || ''}`}
          />
        </ShippingMsgWrapper>
      )}
    </ShipmentMethodsWrapper>
  );
};

ShipmentMethods.propTypes = {
  shipmentMethods: PropTypes.shape([]),
  selectedShipmentId: PropTypes.string,
  shipmentHeader: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
  updateShippingMethodSelection: PropTypes.func,
  formName: PropTypes.string.isRequired,
  formSection: PropTypes.string.isRequired,
  isSfsInvMessagingEnabled: PropTypes.bool,
  isEDD: PropTypes.bool,
};
ShipmentMethods.defaultProps = {
  shipmentMethods: null,
  selectedShipmentId: null,
  shipmentHeader: '',
  isEDD: false,
  isSfsInvMessagingEnabled: false,
  updateShippingMethodSelection: () => {},
};

export default ShipmentMethods;
