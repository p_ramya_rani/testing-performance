// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import { parseBoolean, isMobileApp, getABtestFromState } from '@tcp/core/src/utils';
import {
  getEddFeatureSwitch,
  getEddAllowedStates,
  getItemsWithEDD,
  getOrderEddDates,
  getSelectedShippingCode,
  getEddErrorState,
  getEddCachingInterval,
  getEddABState,
} from '../container/EddComponent.selectors';

jest.mock('@tcp/core/src/utils', () => ({
  parseBoolean: jest.fn(),
  isMobileApp: jest.fn(),
  getABtestFromState: jest.fn(),
}));

describe('#eddData selector', () => {
  it('#getItemsWithEDD should return itemsWithEDD state', () => {
    const EddDataState = fromJS({
      edd: {
        eddData: {},
      },
    });
    const state = {
      eddData: EddDataState,
    };
    expect(getItemsWithEDD(state)).toEqual(EddDataState.getIn(['eddData', 'itemsWithEdd']));
  });

  it('#getOrderEddDates should return eddDates state', () => {
    const EddDataState = fromJS({
      edd: {
        orderEddDates: {},
      },
    });
    const state = {
      eddData: EddDataState,
    };
    expect(getOrderEddDates(state)).toEqual(EddDataState.getIn(['eddData', 'orderEddDates']));
  });

  it('#getSelectedShippingCode should return shippingCode state', () => {
    const EddDataState = fromJS({
      edd: {
        orderEddDates: {},
      },
    });
    const state = {
      shippingCode: EddDataState,
    };
    expect(getSelectedShippingCode(state)).toEqual(EddDataState.get('shippingCode'));
  });

  it('#getEddFeatureSwitch should return featureSwitch state', () => {
    const state = {
      session: {
        siteDetails: {
          EDD_ENABLED: 1,
        },
      },
    };
    expect(getEddFeatureSwitch(state)).toEqual(parseBoolean(state.session.siteDetails.EDD_ENABLED));
  });

  it('#getEddFeatureSwitch should return featureSwitch state for app', () => {
    isMobileApp.mockImplementation(() => true);
    const state = {
      session: {
        siteDetails: {
          EDD_ENABLED: 1,
        },
      },
    };
    expect(getEddFeatureSwitch(state)).toEqual(parseBoolean(state.session.siteDetails.EDD_ENABLED));
  });

  it('#getEddAllowedStates should return allowedStatesList state', () => {
    const state = {
      session: {
        siteDetails: {
          EDD_ENABLED_STATES: 'NY|NJ',
        },
      },
    };
    expect(getEddAllowedStates(state)).toEqual(state.session.siteDetails.EDD_ENABLED_STATES);
  });

  it('#getEddErrorState should return eddError state', () => {
    const EddDataState = fromJS({
      edd: {
        orderEddDates: {},
        eddError: true,
      },
    });
    const state = {
      eddError: EddDataState,
    };
    expect(getEddErrorState(state)).toEqual(EddDataState.get('eddError'));
  });

  it('#getEddCachingInterval should return eddCachingInterval state', () => {
    const state = {
      session: {
        siteDetails: {
          EDD_CACHING_INTERVAL: '90000',
        },
      },
    };
    expect(getEddCachingInterval(state)).toEqual(
      parseInt(state.session.siteDetails.EDD_CACHING_INTERVAL, 10)
    );
  });

  it('#getEddABState should return disableEDDFunctionality state', () => {
    const state = {
      AbTest: {
        disableEDDFunctionality: false,
      },
    };
    getABtestFromState.mockImplementation(() => ({ ...state.AbTest }));
    expect(getEddABState(state)).not.toEqual(state.AbTest.disableEDDFunctionality);
  });
});
