// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { props, expressSelected, standardSplitQty } from './mockData';
import { EddComponentViewVanilla as EddComponent } from '../EddComponent.view';

describe('EddComponent Component', () => {
  it('EddComponent should render', () => {
    const component = shallow(<EddComponent {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('EddComponent should render with Date Range', () => {
    const component = shallow(<EddComponent {...props} isRangeDate />);
    expect(component).toMatchSnapshot();
  });

  it('EddComponent should render with Standard and express split scenario', () => {
    const newProps = { ...props, itemsWithEdd: [...props.itemsWithEdd, expressSelected] };
    const component = shallow(<EddComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  it('EddComponent should render with Standard and qty split scenario', () => {
    const newProps = { ...props, itemsWithEdd: [...props.itemsWithEdd, standardSplitQty] };
    const component = shallow(<EddComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });
});
