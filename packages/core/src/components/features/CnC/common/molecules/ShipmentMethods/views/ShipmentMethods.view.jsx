// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { parseBoolean } from '@tcp/core/src/utils';
import { setSessionStorage, getSessionStorage } from '@tcp/core/src/utils/utils.web';
import EddComponent from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent';
import { getSelectedShipmentMsg } from '@tcp/core/src/utils/utils';
import LabeledRadioButton from '../../../../../../common/atoms/LabeledRadioButton';
import Row from '../../../../../../common/atoms/Row';
import Col from '../../../../../../common/atoms/Col';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import styles from '../styles/ShipmentMethods.style';
import withStyles from '../../../../../../common/hoc/withStyles';
import GenericSkeleton from '../../../../../../common/molecules/GenericSkeleton/GenericSkeleton.view';

const setRadioMethodClass = code => {
  return code === 'HEXP' ? `no-rush-radio-method` : ``;
};

const getEddSessionState = isEddABTest => {
  const isEddSessionEnabled = parseBoolean(getSessionStorage('EDD_ENABLED'));
  if (!isEddSessionEnabled) {
    setSessionStorage({ key: 'EDD_ENABLED', value: isEddABTest });
  }
  return isEddABTest || isEddSessionEnabled;
};

const getSubtitle = (isEDD, isEddABStateFlag, subtitle) => {
  return isEDD && isEddABStateFlag ? '' : subtitle;
};

const renderSelectedMsgView = obj => {
  const { selectedShipment, selectedShipmentMsg, isEDD, isEddABStateFlag } = obj;
  return isEDD && isEddABStateFlag ? (
    <BodyCopy
      fontFamily="secondary"
      fontSize="fs12"
      fontWeight="semibold"
      className="elem-mt-LRG is-mobile-visible"
      textAlign="center"
    >
      <EddComponent
        selectedShipment={selectedShipment.code}
        subtitle={selectedShipmentMsg}
        isRangeDate
        fontSize="fs16"
        showIcon
      />
    </BodyCopy>
  ) : (
    <BodyCopy
      fontFamily="secondary"
      fontSize="fs12"
      fontWeight="semibold"
      className="estimated-shipping-rate elem-mt-LRG"
      textAlign="center"
    >
      {`${selectedShipment.displayName || ''} - `}
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs12"
        fontWeight="semibold"
        component="span"
        className="estimated-shipping-speed"
      >
        {` ${selectedShipment.shippingSpeed || ''}`}
      </BodyCopy>
    </BodyCopy>
  );
};

const ShipmentMethods = ({
  shipmentMethods,
  selectedShipmentId,
  className,
  shipmentHeader,
  isLoadingShippingMethods,
  checkoutRoutingDone,
  isEDD,
  isEddABTest,
}) => {
  const selectedShipment =
    shipmentMethods && shipmentMethods.find(method => method.id === selectedShipmentId);
  const selectedShipmentMsg = getSelectedShipmentMsg(selectedShipment, shipmentMethods);
  const isEddABStateFlag = parseBoolean(getEddSessionState(isEddABTest));
  return (
    <>
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="extrabold"
        className="elem-mb-XXS"
        dataLocator="shippingMethods"
      >
        {shipmentHeader}
      </BodyCopy>
      {!isLoadingShippingMethods && checkoutRoutingDone ? (
        <Row fullBleed className={className}>
          {shipmentMethods &&
            shipmentMethods.length > 0 &&
            shipmentMethods.map(option => {
              const { id, displayName, price, shippingSpeed, code } = option;
              const title = price > 0 ? `${displayName} - $${price}` : displayName;
              const subtitle = shippingSpeed ? `(${shippingSpeed})` : '';
              return (
                <Col
                  colSize={{ small: 2, medium: 8, large: 12 }}
                  className={`${setRadioMethodClass(code)} radio-method`}
                >
                  <Field
                    component={LabeledRadioButton}
                    key={id}
                    selectedValue={id}
                    title={title}
                    subtitle={getSubtitle(isEDD, isEddABStateFlag, subtitle)}
                    name="shippingMethodId"
                    hideSubtitleOnMobile
                    variation="secondary"
                    className="estimated-shipping-size"
                  />
                  <div className="hide-on-mobile">
                    {isEDD && (
                      <EddComponent
                        selectedShipment={code}
                        subtitle={subtitle}
                        isShipping
                        isRangeDate
                        textAlign="left"
                      />
                    )}
                  </div>
                </Col>
              );
            })}
          {selectedShipment && (
            <Col colSize={{ small: 6, medium: 0, large: 0 }}>
              {renderSelectedMsgView({
                selectedShipment,
                selectedShipmentMsg,
                isEDD,
                isEddABStateFlag,
              })}
            </Col>
          )}
        </Row>
      ) : (
        <GenericSkeleton />
      )}
    </>
  );
};

ShipmentMethods.propTypes = {
  shipmentMethods: PropTypes.shape([]),
  selectedShipmentId: PropTypes.string,
  className: PropTypes.string,
  shipmentHeader: PropTypes.string,
  isLoadingShippingMethods: PropTypes.bool,
  checkoutRoutingDone: PropTypes.bool,
  isEDD: PropTypes.bool,
  isEddABTest: PropTypes.bool,
};
ShipmentMethods.defaultProps = {
  shipmentMethods: null,
  selectedShipmentId: null,
  className: '',
  shipmentHeader: '',
  isLoadingShippingMethods: false,
  checkoutRoutingDone: false,
  isEDD: false,
  isEddABTest: false,
};

export default withStyles(ShipmentMethods, styles);
export { ShipmentMethods as ShipmentMethodsVanilla };
