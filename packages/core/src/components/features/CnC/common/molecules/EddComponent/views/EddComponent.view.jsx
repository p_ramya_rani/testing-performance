// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getIconPath, getLocator, getDateRange, getFormattedDate } from '@tcp/core/src/utils';
import { getEddSessionState } from '@tcp/core/src/utils/utils.web';
import { getItemEDD, filterEddItemsById } from './EddComponent.view.utils';
import Styles from '../styles/EddComponent.style';

class EddComponentView extends React.Component {
  componentDidMount() {
    const { fetchEdd } = this.props;
    fetchEdd();
  }

  componentDidUpdate(prevProps) {
    const { fetchEddUpdate, updateShipmentCode, selectedShipment } = this.props;
    const { selectedShipment: prevSelectedShipment } = prevProps;
    fetchEddUpdate();
    if (selectedShipment !== prevSelectedShipment) {
      updateShipmentCode(selectedShipment);
    }
  }

  renderRangeView = (selectedShipment, subtitle) => {
    const { className, isShipping, fontSize, textAlign, orderEddDates, labels, showIcon } =
      this.props;
    const rangeDate = orderEddDates ? getDateRange(orderEddDates, selectedShipment) : null;

    return rangeDate ? (
      <BodyCopy
        component="div"
        className={className}
        fontSize={fontSize || 'fs14'}
        textAlign={textAlign || 'center'}
      >
        {showIcon && (
          <Image
            alt={`${labels.estimatedToArriveBy} ${rangeDate}`}
            src={getIconPath('fast-shipping')}
            className="sth-icon hide-on-desktop hide-on-tablet"
            dataLocator={getLocator('sth-icon')}
          />
        )}
        <BodyCopy
          component="span"
          className={isShipping ? 'estimated-shipping-speed' : ''}
          fontWeight="semibold"
          fontFamily="secondary"
        >
          {labels.arrivesBy}
        </BodyCopy>
        <BodyCopy
          component="span"
          fontWeight="extrabold"
          className="estimated-shipping-date"
          text={rangeDate}
          fontFamily="secondary"
        >
          {rangeDate}
        </BodyCopy>
      </BodyCopy>
    ) : (
      <BodyCopy component="div" className={className}>
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="semibold"
          component="span"
          className="estimated-shipping-speed"
        >
          {subtitle}
        </BodyCopy>
      </BodyCopy>
    );
  };

  renderDateView = (eddItemsForId) => {
    const {
      showIcon,
      className,
      fontSize,
      labels,
      selectedShippingMethod,
      newBag,
      placeHolderText,
    } = this.props;
    const itemEdd = getItemEDD(eddItemsForId, selectedShippingMethod);

    return itemEdd ? (
      <BodyCopy component="div" className={className}>
        <BodyCopy component="div" className={showIcon ? 'elem-pt-MED' : ''}>
          {showIcon && (
            <Image
              alt={`${labels.estimatedToArriveBy} ${getFormattedDate(itemEdd)}`}
              src={`${
                newBag ? getIconPath('bag-ship-to-home-black') : getIconPath('fast-shipping')
              }`}
              className="sth-icon eddIcon"
              dataLocator={getLocator('sth-icon')}
            />
          )}
          <BodyCopy
            fontFamily="secondary"
            fontSize={fontSize || 'fs14'}
            fontWeight="semibold"
            component="span"
            color={`${newBag ? 'green.500' : 'text.primary'}`}
          >
            {labels.arrivesBy}
          </BodyCopy>
          <BodyCopy
            fontFamily="secondary"
            className="estimated-shipping-date"
            fontSize={fontSize || 'fs14'}
            fontWeight="extrabold"
            component="span"
            color={`${newBag ? 'green.500' : 'text.primary'}`}
          >
            {getFormattedDate(itemEdd, newBag)}
          </BodyCopy>
        </BodyCopy>
      </BodyCopy>
    ) : (
      <>
        {placeHolderText && (
          <BodyCopy
            fontFamily="secondary"
            fontSize={fontSize || 'fs14'}
            component="p"
            textAlign="center"
            color="green.500"
          >
            {placeHolderText}
          </BodyCopy>
        )}
      </>
    );
  };

  render() {
    const { itemsWithEdd, itemIdprop, isRangeDate, selectedShipment, subtitle, isEddABTest } =
      this.props;
    const eddItemsForId = filterEddItemsById(itemsWithEdd, itemIdprop);
    if (getEddSessionState(isEddABTest)) {
      return isRangeDate
        ? this.renderRangeView(selectedShipment, subtitle)
        : this.renderDateView(eddItemsForId);
    }
    return null;
  }
}

EddComponentView.propTypes = {
  className: PropTypes.string,
  subtitle: PropTypes.string,
  itemsWithEdd: PropTypes.shape({}),
  orderEddDates: PropTypes.shape({}),
  itemIdprop: PropTypes.string,
  isRangeDate: PropTypes.bool,
  showIcon: PropTypes.bool,
  selectedShipment: PropTypes.string,
  isShipping: PropTypes.bool,
  fontSize: PropTypes.string,
  textAlign: PropTypes.string,
  labels: PropTypes.shape({
    arrivesBy: PropTypes.string,
  }).isRequired,
  fetchEdd: PropTypes.func.isRequired,
  isEddABTest: PropTypes.bool,
  fetchEddUpdate: PropTypes.func.isRequired,
  selectedShippingMethod: PropTypes.string,
  updateShipmentCode: PropTypes.func,
  newBag: PropTypes.bool,
};

EddComponentView.defaultProps = {
  className: '',
  subtitle: '',
  itemsWithEdd: null,
  orderEddDates: {},
  itemIdprop: null,
  isRangeDate: false,
  showIcon: false,
  selectedShipment: '',
  isShipping: false,
  fontSize: '',
  textAlign: '',
  isEddABTest: false,
  selectedShippingMethod: null,
  updateShipmentCode: () => {},
  newBag: false,
};

export default withStyles(EddComponentView, Styles);
export { EddComponentView as EddComponentViewVanilla };
