// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { getDateRange, getFormattedDate } from '@tcp/core/src/utils';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getItemEDD, filterEddItemsById } from './EddComponent.view.utils';
import { BodyCopy, Image } from '../../../../../../common/atoms';
import { RowSectionStyle } from '../styles/EddComponent.style.native';
import fastShipping from '../../../../../../../../../mobileapp/src/assets/images/shipping.png';

class EddComponentView extends React.Component {
  componentDidMount() {
    const { fetchEdd } = this.props;
    fetchEdd();
  }

  componentDidUpdate(prevProps) {
    const { fetchEddUpdate, updateShipmentCode, selectedShipment } = this.props;
    const { selectedShipment: prevSelectedShipment } = prevProps;
    fetchEddUpdate();
    if (
      (selectedShipment && selectedShipment.code) !==
      (prevSelectedShipment && prevSelectedShipment.code)
    ) {
      updateShipmentCode(selectedShipment.code);
    }
  }

  renderRangeView = selectedShipment => {
    const { fontSize, orderEddDates, labels, showIcon } = this.props;
    const rangeDate = getDateRange(orderEddDates, selectedShipment.code);
    return rangeDate ? (
      <>
        {showIcon && (
          <ViewWithSpacing spacingStyles="margin-right-XXS">
            <Image source={fastShipping} alt="STH" height={22} width={26} />
          </ViewWithSpacing>
        )}
        <BodyCopy
          fontFamily="secondary"
          fontSize={fontSize || 'fs12'}
          color="gray[800]"
          text={labels.arrivesBy}
        />
        <ViewWithSpacing spacingStyles="margin-left-XXS">
          <BodyCopy
            fontWeight="extrabold"
            fontFamily="secondary"
            fontSize={fontSize || 'fs12'}
            color="gray[800]"
            spacingStyles="padding-left-XXS"
            text={rangeDate}
          />
        </ViewWithSpacing>
      </>
    ) : (
      <>
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="semibold"
          text={`${selectedShipment.displayName || ''} - `}
        />
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="semibold"
          /* style={italicStyle} */
          text={` ${selectedShipment.shippingSpeed || ''}`}
        />
      </>
    );
  };

  renderDateView = eddItemsForId => {
    const { showIcon, labels, fontSize, selectedShippingMethod } = this.props;
    const itemEdd = getItemEDD(eddItemsForId, selectedShippingMethod);
    return itemEdd ? (
      <RowSectionStyle>
        {showIcon && (
          <ViewWithSpacing spacingStyles="margin-right-XXS">
            <Image source={fastShipping} alt="STH" height={22} width={26} />
          </ViewWithSpacing>
        )}
        <BodyCopy
          fontFamily="secondary"
          fontWeight="semibold"
          fontSize={fontSize || 'fs12'}
          color="gray[800]"
          text={labels.arrivesBy}
        />
        <ViewWithSpacing spacingStyles="margin-left-XXS">
          <BodyCopy
            fontWeight="extrabold"
            fontFamily="secondary"
            fontSize={fontSize || 'fs12'}
            color="gray[800]"
            text={getFormattedDate(itemEdd)}
          />
        </ViewWithSpacing>
      </RowSectionStyle>
    ) : null;
  };

  render() {
    const { itemsWithEdd, itemIdprop, isRangeDate, selectedShipment } = this.props;
    const eddItemsForId = filterEddItemsById(itemsWithEdd, itemIdprop);

    return isRangeDate
      ? this.renderRangeView(selectedShipment)
      : this.renderDateView(eddItemsForId);
  }
}

EddComponentView.propTypes = {
  itemsWithEdd: PropTypes.shape({}),
  itemIdprop: PropTypes.string,
  isRangeDate: PropTypes.bool,
  showIcon: PropTypes.bool,
  selectedShipment: PropTypes.shape({}),
  orderEddDates: PropTypes.shape({}),
  fontSize: PropTypes.string,
  labels: PropTypes.shape({
    arrivesBy: PropTypes.string,
  }).isRequired,
  fetchEdd: PropTypes.func.isRequired,
  fetchEddUpdate: PropTypes.func.isRequired,
  selectedShippingMethod: PropTypes.string,
  updateShipmentCode: PropTypes.func,
};

EddComponentView.defaultProps = {
  itemsWithEdd: null,
  itemIdprop: null,
  isRangeDate: false,
  showIcon: false,
  selectedShipment: null,
  orderEddDates: {},
  fontSize: '',
  selectedShippingMethod: null,
  updateShipmentCode: () => {},
};

export default EddComponentView;
export { EddComponentView as EddComponentViewVanilla };
