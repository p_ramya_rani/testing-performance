// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getImageFilePath } from '@tcp/core/src/utils';

const styles = css`
  .payment-method-container {
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    background-color: ${(props) => props.theme.colors.WHITE};
    box-sizing: border-box;
    border-radius: 6px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      width: 100%;
      min-width: 0;
      &:last-child {
        margin-right: 0;
      }
    }
  }
  .container {
    display: flex;
    flex-direction: row;
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      justify-content: space-between;
    }
  }

  .payment-method-box {
    min-width: 120px;
    min-height: 66px;
    align-items: center;
    justify-content: center;
    display: flex;
    box-sizing: border-box;
    border: 1px solid ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    border-radius: 6px;
    flex-direction: column;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      min-width: 0;
    }
  }

  .payment-method {
    box-sizing: border-box;
    height: 53px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 40px;
    }
  }

  .radio-button-checked + div .payment-method-box {
    border: solid 2px ${(props) => props.theme.colors.BLACK};
    border-radius: 6px;
  }

  .creditCard {
    background: url('${(props) => getImageFilePath(props)}/pm_credit_card.svg') no-repeat 1px 1px;
    background-size: 70px 41px;
    width: 70px;
    height: 41px;
  }

  .creditCard-body {
    font-size: 12px;
    font-weight: bold;
  }

  .payPal {
    background: url('${(props) => getImageFilePath(props)}/pm_paypal.svg') no-repeat 1px 1px;
    background-size: 49px 48px;
    width: 49px;
    height: 48px;
  }

  .venmo {
    background: url('${(props) => getImageFilePath(props)}/pm_venmo.svg') no-repeat 1px 1px;
    background-size: 68px 13px;
    width: 68px;
    height: 13px;
  }

  .applePay {
    background: url('${(props) => getImageFilePath(props)}/pm_apple.svg') no-repeat 1px 1px;
    background-size: 64px 28px;
    width: 64px;
    height: 28px;
  }

  .afterpay-box {
    background-color: ${(props) => props.theme.colors.AFTERPAY.MINT};
  }

  .afterpay {
    background: url('${(props) => getImageFilePath(props)}/afterpay.svg') no-repeat 1px 1px;
    width: 85px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      background-size: 85px 100%;
    }
  }

  .radio-button-checked,
  .radio-button {
    display: none;
  }

  .payment-method-container > label {
    padding-top: 0px;
    padding-left: 0px;
    background-color: transparent;
    border: none;
    padding-bottom: 0;
  }
  .second-row {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    .payment-method-container {
      width: auto;
      min-width: 75px;
    }
  }
  .other-payment-options {
    text-decoration: underline;
  }
  .venmo-small {
    background: url('${(props) => getImageFilePath(props)}/pm_venmo.svg') no-repeat 1px 1px;
    background-size: 42px 10px;
    width: 45px;
    height: 10px;
    display: inline-block;
    margin-left: 8px;
    vertical-align: middle;
  }
`;

export default styles;
