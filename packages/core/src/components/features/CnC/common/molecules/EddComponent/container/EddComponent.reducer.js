// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import { DEFAULT_REDUCER_KEY, setCacheTTL } from '@tcp/core/src/utils/cache.util';
import constants from '../EddComponent.constants';

const initialState = fromJS({
  [DEFAULT_REDUCER_KEY]: null,
  eddData: null,
  isFetching: false,
  eddError: null,
});

const setEdddata = (state, action) => {
  const { payload } = action;
  return state
    .set('eddData', action.payload)
    .set(
      DEFAULT_REDUCER_KEY,
      setCacheTTL(
        typeof payload.eddCachingTTL !== 'undefined' ? payload.eddCachingTTL : constants.EDD_TTL
      )
    );
};

function eddReducerExtension(state, action) {
  switch (action.type) {
    case constants.RESET_EDD_ERROR:
      return state.set('eddError', null);
    default:
      // TODO: currently when initial state is hydrated on browser, List is getting converted to an JS Array
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
}

const eddReducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.SHOW_LOADER:
    case constants.GET_EDD_DATA:
      return state.set('isFetching', true);
    case constants.SET_EDD_DATA:
      return setEdddata(state, action);
    case constants.SET_EDD_ERROR:
      return state.set('eddError', action.payload);
    case constants.RESET_EDD_DATA:
      return state.set('eddData', null).set('eddError', null);
    case constants.CLEAR_EDD_TTL:
      return state.set(DEFAULT_REDUCER_KEY, null);
    case constants.UPDATE_SELECTED_SHIPPING_CODE:
      return state.set('shippingCode', action.payload);
    case constants.HIDE_LOADER:
      return state.set('isFetching', false);
    default:
      return eddReducerExtension(state, action);
  }
};

export default eddReducer;
