import { css } from 'styled-components';

const styles = css`
  border-left: solid 6px ${props => props.theme.colors.REWARDS.PLCC};
  border-top: solid 6px ${props => props.theme.colors.REWARDS.PLCC};
  border-right: solid 6px ${props => props.theme.colors.REWARDS.NON_PLCC};
  border-bottom: solid 6px ${props => props.theme.colors.REWARDS.NON_PLCC};
  text-align: center;
  padding: ${props => props.theme.spacing.ELEM_SPACING.SM};
  padding: ${props => props.theme.spacing.ELEM_SPACING.XS_6};
  background: #fff;
  border-radius: 10px;
  margin: ${props => props.theme.spacing.ELEM_SPACING.MED}
    ${props => props.theme.spacing.ELEM_SPACING.XS};
  .checkoutSectionTitle {
    display: flex;
    justify-content: center;
  }
  .cta-divider {
    margin: 0 14px;
  }
  .cta-link {
    text-decoration: underline;
    cursor: pointer;
  }
  @media ${props => props.theme.mediaQuery.smallOnly} {
    margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 0;
    border-left: solid 3px ${props => props.theme.colors.REWARDS.PLCC};
    border-top: solid 3px ${props => props.theme.colors.REWARDS.PLCC};
    border-right: solid 3px ${props => props.theme.colors.REWARDS.NON_PLCC};
    border-bottom: solid 3px ${props => props.theme.colors.REWARDS.NON_PLCC};
    min-height: 34px;
    .cta-link {
      font-size: ${props => props.theme.typography.fontSizes.fs14};
    }
    .cta-divider {
      margin: 0 ${props => props.theme.spacing.ELEM_SPACING.SM};
      font-size: ${props => props.theme.typography.fontSizes.fs14};
    }
  }
`;

export default styles;
