// 9fbef606107a605d69c0edbcd8029e5d 
import { parseBoolean, isMobileApp, getABtestFromState } from '@tcp/core/src/utils';

export const getEddFeatureSwitch = state => {
  const eddGlobalSwitch = parseBoolean(state.session.siteDetails.EDD_ENABLED);
  const eddAppSwitch = parseBoolean(state.session.siteDetails.EDD_APP_ENABLED);
  return isMobileApp() ? eddGlobalSwitch && eddAppSwitch : eddGlobalSwitch;
};

export const getEddAllowedStates = state => {
  return state.session.siteDetails.EDD_ENABLED_STATES;
};

export const getEddCachingInterval = state => {
  const cachingTTL = state.session.siteDetails.EDD_CACHING_INTERVAL;
  if (typeof cachingTTL !== 'undefined') {
    return parseInt(cachingTTL, 10);
  }
  return cachingTTL;
};

export const getItemsWithEDD = state => {
  return state && state.Edd && state.Edd.getIn(['eddData', 'itemsWithEdd']);
};

export const getOrderEddDates = state => {
  return state && state.Edd && state.Edd.getIn(['eddData', 'orderEddDates']);
};

export const getSelectedShippingCode = state => {
  return state && state.Edd && state.Edd.get('shippingCode');
};

export const getEddErrorState = state => {
  return state && state.Edd && state.Edd.get('eddError');
};

export const getFetchingState = state => {
  return state && state.Edd && state.Edd.get('isFetching');
};

export const getEddABState = state => {
  const disableEDDAB = state && state.AbTest && getABtestFromState(state.AbTest);
  return !(disableEDDAB && disableEDDAB.disableEDDFunctionality);
};
