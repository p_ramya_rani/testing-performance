// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  getLoginPayload,
  getCreateAccountPayload,
  setLoyaltyLocation,
} from '@tcp/core/src/constants/analytics';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { routerPush } from '@tcp/core/src/utils';
import LoyaltyBanner from '@tcp/core/src/components/features/CnC/LoyaltyBanner';
import withStyles from '../../../../../../common/hoc/withStyles';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import styles from '../styles/SigninCtaBanner.style';

class SigninCtaBanner extends React.PureComponent {
  onLinkClick = (e, linkType) => {
    const { pathname = 'referrer' } = (window && window.location) || {};
    e.preventDefault();
    if (linkType === 'signin') {
      routerPush(`/home?target=login&successtarget=${pathname}`, '/home/login');
    } else if (linkType === 'create-account') {
      routerPush(`/home?target=register&successtarget=${pathname}`, '/home/register');
    }
  };

  render() {
    const { className, labels = {} } = this.props;
    const {
      signinCtaBannerSignin = '',
      signinCtaBannerOr = '',
      signinBannerCreateAccount = '',
    } = labels;
    return (
      <div className={className}>
        <div className="checkoutSectionTitle">
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="bold"
            component="h2"
            data-locator="pickup-title"
            onClick={(e) => this.onLinkClick(e, 'signin')}
            className="cta-link"
          >
            <ClickTracker
              onClick={() => {
                setLoyaltyLocation('cart');
              }}
              clickData={getLoginPayload('cart')}
            >
              {signinCtaBannerSignin}
            </ClickTracker>
          </BodyCopy>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="bold"
            component="h2"
            className="cta-divider"
          >
            {signinCtaBannerOr}
          </BodyCopy>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="bold"
            component="h2"
            data-locator="pickup-title"
            onClick={(e) => this.onLinkClick(e, 'create-account')}
            className="cta-link"
          >
            <ClickTracker
              onClick={() => {
                setLoyaltyLocation('cart');
              }}
              clickData={getCreateAccountPayload('cart')}
            >
              {signinBannerCreateAccount}
            </ClickTracker>
          </BodyCopy>
        </div>
        <LoyaltyBanner pageCategory="signin-cta-banner" />
      </div>
    );
  }
}

SigninCtaBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
};

export default withStyles(SigninCtaBanner, styles);
export { SigninCtaBanner as SigninCtaBannerVanilla };
