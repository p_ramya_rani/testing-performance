// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import LoyaltyBanner from '@tcp/core/src/components/features/CnC/LoyaltyBanner';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import OpenLoginModal from '@tcp/core/src/components/features/account/LoginPage/container/LoginModal.container';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

import {
  SignInCtaBannerWrapper,
  CheckoutSectionTitle,
  CtaDivider,
  CtaLink,
} from '../styles/SigninCtaBanner.style.native';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import names from '../../../../../../../constants/eventsName.constants';

const SigninCtaBanner = ({ labels = {} }) => {
  const {
    signinCtaBannerSignin = '',
    signinCtaBannerOr = '',
    signinBannerCreateAccount = '',
  } = labels;

  const [loginModalState, setloginModalState] = useState({
    showLoginModal: false,
    loginType: null,
  });

  const setLoginModal = (params) => {
    setloginModalState({
      loginType: null,
      showLoginModal: params && params.state,
    });
  };

  const onLinkClick = (type) => {
    setloginModalState({ loginType: type, showLoginModal: true });
  };

  const { showLoginModal, loginType } = loginModalState;
  const pageName = 'shopping bag';

  return (
    <SignInCtaBannerWrapper>
      <CheckoutSectionTitle>
        <CtaLink>
          <ClickTracker
            as={Anchor}
            name={names.screenNames.loyalty_login_click}
            clickData={{
              customEvents: ['event126'],
              pageName,
              loyaltyLocation: pageName,
            }}
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="bold"
            data-locator="pickup-title"
            onPress={() => onLinkClick('login')}
            text={signinCtaBannerSignin}
            underline
          />
        </CtaLink>
        <CtaDivider>
          <BodyCopy fontFamily="secondary" fontSize="fs14" text={signinCtaBannerOr} />
        </CtaDivider>
        <CtaLink>
          <ClickTracker
            as={Anchor}
            name={names.screenNames.loyalty_join_click}
            clickData={{
              customEvents: ['event125'],
              pageName,
              loyaltyLocation: pageName,
            }}
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="bold"
            data-locator="pickup-title"
            onPress={() => onLinkClick('create-account')}
            text={signinBannerCreateAccount}
            underline
          />
        </CtaLink>
      </CheckoutSectionTitle>
      <LoyaltyBanner pageCategory="signin-cta-banner" />
      {showLoginModal && (
        <OpenLoginModal
          component={loginType}
          openState={showLoginModal}
          setLoginModalMountState={setLoginModal}
        />
      )}
    </SignInCtaBannerWrapper>
  );
};

SigninCtaBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
};

export default SigninCtaBanner;
