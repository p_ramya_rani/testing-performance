// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '../../../../../../common/atoms';
import { RemoveSoldOutView, RowSectionStyle } from '../styles/RemoveSoldOut.style.native';

class RemoveSoldOut extends React.PureComponent {
  getRemoveString = (labels, labelMsg, removeCartItem, getUnavailableOOSItems) => {
    const remove = labelMsg && labelMsg.split('#remove#');
    const newRemove = (
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs12"
        text={labels.removeError}
        textDecoration="underline"
        onPress={() => removeCartItem({ getUnavailableOOSItems })}
      />
    );
    if (remove) {
      remove.splice(1, 0, newRemove);
    }
    return remove;
  };

  render() {
    const { labels, removeCartItem, getUnavailableOOSItems, isSoldOut, isUnavailable } = this.props;
    const labelMsg =
      (isSoldOut && labels.removeSoldoutHeader) || (isUnavailable && labels.updateUnavailable);
    const labelForRemove = this.getRemoveString(
      labels,
      labelMsg,
      removeCartItem,
      getUnavailableOOSItems
    );
    return (
      <RemoveSoldOutView>
        {labels && (
          <RowSectionStyle>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs12"
              fontWeight="regular"
              text={labelForRemove}
            />
          </RowSectionStyle>
        )}
      </RemoveSoldOutView>
    );
  }
}

RemoveSoldOut.propTypes = {
  labels: PropTypes.string,
  removeCartItem: PropTypes.func.isRequired,
  getUnavailableOOSItems: PropTypes.shape([]),
  isSoldOut: PropTypes.bool,
  isUnavailable: PropTypes.bool,
};

RemoveSoldOut.defaultProps = {
  labels: '',
  getUnavailableOOSItems: [],
  isSoldOut: false,
  isUnavailable: false,
};

export default RemoveSoldOut;

export { RemoveSoldOut as RemoveSoldOutVanilla };
