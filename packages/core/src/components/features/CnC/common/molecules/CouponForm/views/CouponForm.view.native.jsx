// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes, { bool, string, func } from 'prop-types';
import CookieManager from 'react-native-cookies';
import { isIOS, setValueInAsyncStorage } from '@tcp/core/src/utils/utils.app';
import TextBox from '../../../../../../common/atoms/TextBox';
import { BodyCopy, Anchor } from '../../../../../../common/atoms';
import ErrorMessage from '../../ErrorMessage';
import {
  InputField,
  Header,
  FlexRow,
  FormContainer,
  CouponFormContainer,
  NeedHelpText,
  StyledButton,
} from '../styles/CouponForm.style.native';
import { couponFieldLength, getFormattedValue } from '../CouponForm.utils';

const formattedValue = value => {
  return value ? getFormattedValue(value) : '';
};

export class CouponForm extends React.PureComponent {
  renderTextBox = ({ input, ...otherParams }) => {
    return <TextBox input={input} {...otherParams} />;
  };

  mergeCouponError = () => {
    const {
      labels: { mergeCartCouponError = '' } = {},
      showAccordian,
      showCouponMergeError,
      showMaxItemErrorDisplayed,
      setMaxItemErrorDisplayedAction,
      currentUserId,
    } = this.props;
    if (showCouponMergeError) {
      if (!showMaxItemErrorDisplayed) {
        if (isIOS()) {
          CookieManager.clearByName(`MC_${currentUserId}`);
        }
        setMaxItemErrorDisplayedAction(true);
        setValueInAsyncStorage('mergeCartMessageShown', 'true'); // Don't show on app restart, as cookie was not deleting on android
      }
      return (
        <ErrorMessage
          showAccordian={showAccordian}
          error={mergeCartCouponError}
          isNativeView={false}
        />
      );
    }
    return null;
  };

  render() {
    const {
      handleSubmit,
      labels,
      fieldName,
      dataLocators,
      error,
      isFetching,
      onNeedHelpTextClick,
      showAccordian,
      pristine,
      submitting,
    } = this.props;
    return (
      <CouponFormContainer>
        <ErrorMessage
          showAccordian={showAccordian}
          error={error && error.msg}
          isEspot={error && error.isPlaceCashError}
          isNativeView={false}
        />
        {this.mergeCouponError()}
        <FormContainer>
          <Header>
            <BodyCopy
              fontSize="fs16"
              fontFamily="secondary"
              component="span"
              fontWeight="semibold"
              text={labels.couponCodeHeader}
            />
            <Anchor
              onPress={() => onNeedHelpTextClick({ status: 'open' })}
              style={NeedHelpText}
              fontSizeVariation="medium"
              fontFamily="secondary"
              anchorVariation="primary"
              component="span"
              fontWeight="regular"
              text={labels.couponNeedHelpText}
              underline
            />
          </Header>
          <FlexRow>
            <InputField>
              <Field
                name={fieldName}
                id={fieldName}
                label={labels.placeholderText}
                ariaLabel={labels.placeholderText}
                type="text"
                component={this.renderTextBox}
                dataLocator={dataLocators.inputField}
                maxLength={couponFieldLength}
                keyboardType={isIOS() ? 'default' : 'visible-password'}
                normalize={formattedValue}
              />
            </InputField>
            <StyledButton
              fill="WHITE"
              type="submit"
              external
              onPress={handleSubmit}
              text={labels.submitButtonLabel}
              disabled={isFetching || pristine || submitting}
              data-locator={dataLocators.submitButton}
            />
          </FlexRow>
        </FormContainer>
      </CouponFormContainer>
    );
  }
}

CouponForm.propTypes = {
  labels: PropTypes.shape({
    placeholderText: PropTypes.string.isRequired,
    submitButtonLabel: PropTypes.string.isRequired,
    couponCodeHeader: PropTypes.string.isRequired,
    couponNeedHelpText: PropTypes.string.isRequired,
    couponHelpText: PropTypes.string.isRequired,
  }),
  dataLocators: PropTypes.shape({
    submitButton: PropTypes.string,
    inputField: PropTypes.string,
  }),
  handleSubmit: PropTypes.func,
  fieldName: PropTypes.string,
  className: PropTypes.string.isRequired,
  error: PropTypes.string,
  showCouponMergeError: bool,
  currentUserId: string.isRequired,
  setMaxItemErrorDisplayedAction: func.isRequired,
  onNeedHelpTextClick: PropTypes.func,
  isFetching: PropTypes.isRequired,
  showMaxItemErrorDisplayed: bool.isRequired,
  showAccordian: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

CouponForm.defaultProps = {
  labels: {
    placeholderText: 'Enter Coupon Code',
    submitButtonLabel: 'Apply',
    couponCodeHeader: 'COUPON CODE',
    couponNeedHelpText: 'Need Help?',
  },
  dataLocators: {
    submitButton: 'coupon_submit_btn',
    inputField: 'coupon_code',
  },
  error: '',
  fieldName: 'couponCode',
  showCouponMergeError: false,
  handleSubmit: () => {},
  onNeedHelpTextClick: () => {},
};

const onSubmitSuccess = (result, dispatch, { reset }) => reset();
export default reduxForm({
  form: 'CouponForm', // a unique identifier for this form
  onSubmitSuccess,
})(CouponForm);

export { CouponForm as CouponFormVanilla };
