// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

export default css`
  display: flex;
  flex: 1;

  ${(props) =>
    props.isNewBag
      ? `
        font-size: ${props.theme.typography.fontSizes.fs14};
        background-color: ${props.theme.colorPalette.BUTTON.WHITE.NORMAL};
        padding: ${props.theme.spacing.LAYOUT_SPACING.XXS};
        border: 2px solid ${props.theme.colorPalette.red[500]};
        align-items: center;
        @media ${props.theme.mediaQuery.smallOnly} {
          align-items: flex-start;
        }
        @media ${props.theme.mediaQuery.mediumOnly} {
          align-items: flex-start;
        }`
      : `
        padding-top: 0px;
        padding-bottom: 13px;
        font-size: 10px;
        background-color: ${props.theme.colors.PRIMARY.PALEGRAY};
        `}

  img {
    height: 13px;
    width: 13px;
    padding: 3px 13px 0 0;
  }
  .removeItem {
    margin: -11px 0 0 32px;
  }
  .removeAnchor {
    margin: 0 2px;
    background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  }
  .pointer {
    cursor: pointer;
    margin: -11px 0 1px 26px;
  }
  .bagTileItem {
    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
      span {
        font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
      }
    }
    margin-left: ${(props) => (props.isNewBag ? props.theme.spacing.LAYOUT_SPACING.XXS : '25px')};
    font-weight: ${(props) => props.isNewBag && props.theme.fonts.fontWeight.extrabold};
  }
  .removeErrorMessage {
    text-decoration: underline;
    font-weight: ${(props) => props.isNewBag && props.theme.fonts.fontWeight.extrabold};
  }

  .warning-icon {
    background: transparent url('${(props) => getIconPath('circle-alert-fill', props)}') no-repeat 0
      0;
    background-size: contain;
    border: none;
    height: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
    width: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 84px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 45px;
    }
  }
`;
