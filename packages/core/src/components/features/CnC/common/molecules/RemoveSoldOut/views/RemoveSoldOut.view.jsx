// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';

import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import style from '../styles/RemoveSoldOut.style';

class RemoveSoldOut extends React.PureComponent {
  getRemoveString = (labels, labelMsg, removeCartItem, getUnavailableOOSItems) => {
    const { pageView } = this.props;
    const isMiniBag = pageView !== 'myBag';
    const remove = labelMsg && labelMsg.split('#remove#');
    const newRemove = (
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs12"
        component="span"
        className="removeErrorMessage"
        onClick={() => removeCartItem({ getUnavailableOOSItems, isMiniBag })}
      >
        {labels.removeError}
      </BodyCopy>
    );
    if (remove) {
      remove.splice(1, 0, newRemove);
    }
    return remove;
  };

  render() {
    const {
      labels,
      className,
      pageView,
      removeCartItem,
      getUnavailableOOSItems,
      isSoldOut,
      isUnavailable,
      isNewBag,
    } = this.props;
    const styleClass = pageView === 'myBag' ? 'bagTileItem' : 'removeItem';
    const labelMsg =
      (isSoldOut && labels.removeSoldoutHeader) || (isUnavailable && labels.updateUnavailable);
    const labelForRemove = this.getRemoveString(
      labels,
      labelMsg,
      removeCartItem,
      getUnavailableOOSItems
    );
    return (
      <>
        <div className={className}>
          {isNewBag && (
            <div
              className={`warning-icon  ${styleClass ? 'bag-style' : ''}`}
              aria-disabled="true"
            />
          )}

          {labels && (
            <BodyCopy
              className={styleClass}
              component="span"
              fontFamily="secondary"
              fontSize="fs12"
            >
              {labelForRemove}
            </BodyCopy>
          )}
        </div>
      </>
    );
  }
}

RemoveSoldOut.propTypes = {
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  className: PropTypes.string,
  pageView: PropTypes.string,
  removeCartItem: PropTypes.func.isRequired,
  getUnavailableOOSItems: PropTypes.shape([]),
  isSoldOut: PropTypes.bool,
  isUnavailable: PropTypes.bool,
  isNewBag: PropTypes.bool,
};

RemoveSoldOut.defaultProps = {
  labels: '',
  className: '',
  pageView: '',
  getUnavailableOOSItems: [],
  isSoldOut: false,
  isUnavailable: false,
  isNewBag: false,
};

export default withStyles(RemoveSoldOut, style);

export { RemoveSoldOut };
