// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import { isMobileApp, getSiteId } from '@tcp/core/src/utils';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import { API_CONFIG } from '@tcp/core/src/services/config';
import { getAddressListState } from '@tcp/core/src/components/features/account/AddressBook/container/AddressBook.selectors';
import { getUserZipCode } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import CheckoutSelectors from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import { getIsInternationalShipping } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getEcomSkuParams } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import EddComponentView from '../views';
import {
  getEddFeatureSwitch,
  getEddAllowedStates,
  getItemsWithEDD,
  getOrderEddDates,
  getFetchingState,
  getEddErrorState,
  getEddABState,
  getSelectedShippingCode,
} from './EddComponent.selectors';
import { getEddData, updateShippingCode, resetEddError } from './EddComponent.actions';

export class EddComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tcpState: null,
      tcpZip: null,
    };
  }

  componentWillMount() {
    this.getState();
    this.getZip();
  }

  componentWillUnmount() {
    const { resetEddErrorActn } = this.props;
    resetEddErrorActn();
  }

  getDefaultZipCode = () => {
    const { addressList, shippingZip, shippingCheckoutAddress } = this.props;
    const { tcpZip } = this.state;
    let zipcode = null;
    if (shippingCheckoutAddress && shippingCheckoutAddress.zipCode) {
      //  first pick address from checkout shipping address
      zipcode = shippingCheckoutAddress.zipCode;
    } else if (List.isList(addressList) && addressList.size) {
      // first pick the zipcode from shipping address from addressbook
      zipcode = this.getZipCodeFromList(addressList);
    } else if (shippingZip) {
      // then pick the zipcode from shipping adderess from orderDetails state
      zipcode = shippingZip;
    } else {
      zipcode = tcpZip;
    }
    return zipcode;
  };

  getDefaultAddressFromList = (addressList) => {
    return addressList.find((address) => address.primary === 'true');
  };

  getZipCodeFromList = (addressList) => {
    const defaultAddress =
      List.isList(addressList) && addressList.size && this.getDefaultAddressFromList(addressList);
    return defaultAddress && defaultAddress.zipCode;
  };

  getStateCodeFromList = (addressList) => {
    const defaultAddress =
      List.isList(addressList) && addressList.size && this.getDefaultAddressFromList(addressList);
    return defaultAddress && defaultAddress.state;
  };

  getState = () => {
    const tcpStatePromise = readCookie('tcpState');
    if (tcpStatePromise) {
      if (isMobileApp()) {
        tcpStatePromise.then((res) => {
          this.setState({ tcpState: res });
        });
      } else {
        this.setState({ tcpState: tcpStatePromise });
      }
    }
  };

  getReturnZip = () => {
    const tcpZipPromise = readCookie('tcpZip');
    let zipcode = null;
    if (tcpZipPromise) {
      if (isMobileApp()) {
        tcpZipPromise.then((res) => {
          zipcode = res;
        });
      } else {
        zipcode = tcpZipPromise;
      }
    }
    return zipcode;
  };

  getZip = () => {
    const tcpZipPromise = readCookie('tcpZip');
    if (tcpZipPromise) {
      if (isMobileApp()) {
        tcpZipPromise.then((res) => {
          this.setState({ tcpZip: res });
        });
      } else {
        this.setState({ tcpZip: tcpZipPromise });
      }
    }
  };

  getStateFromAddress = (addressList) => {
    const { tcpState } = this.state;
    const { shippingState, shippingCheckoutAddress } = this.props;
    let statecode = null;
    if (shippingCheckoutAddress && shippingCheckoutAddress.state) {
      //  first pick address from checkout shipping address
      statecode = shippingCheckoutAddress.state;
    } else if (List.isList(addressList) && addressList.size) {
      // second pick the state code from shipping address from addressbook
      statecode = this.getStateCodeFromList(addressList);
    } else if (shippingState) {
      // then pick the state code from shipping adderess from orderDetails state
      statecode = shippingState;
    } else {
      statecode = tcpState;
    }
    return statecode;
  };

  checkStateEligibility = () => {
    const { addressList, eddAllowedStates } = this.props;
    const stateAddress = this.getStateFromAddress(addressList);
    const eddAllowedStatesArr = eddAllowedStates.split('|');
    return eddAllowedStatesArr && eddAllowedStatesArr.length > 0
      ? eddAllowedStatesArr.indexOf(stateAddress) > -1
      : false;
  };

  fetchEddData = () => {
    const { cartOrderItems, getEdd, currentOrderId, addressList, newBag } = this.props;
    const zipcode = this.getDefaultZipCode();
    const stateAddress = this.getStateFromAddress(addressList);
    const payload = {};
    const skuData = (cartOrderItems && getEcomSkuParams(cartOrderItems, newBag)) || [];
    if (this.checkStateEligibility() && skuData.length) {
      payload.skuList = skuData;
      payload.orderId = currentOrderId && currentOrderId.toString();
      payload.state = stateAddress;
      if (zipcode) {
        payload.zipCode = zipcode;
        getEdd(payload);
      }
    }
  };

  isEddEligbleCountry = () => {
    const { isInternationalShipping } = this.props;
    const siteId = getSiteId();
    return isMobileApp() ? siteId === API_CONFIG.siteIds.us : !isInternationalShipping;
  };

  triggerEdd = () => {
    const { isEDD, eddError } = this.props;
    if (isEDD && !eddError && this.isEddEligbleCountry()) {
      this.fetchEddData();
    }
  };

  triggerEddOnUpdate = () => {
    const tcpZipPromise = this.getReturnZip();
    const zipCode = this.getDefaultZipCode();
    const isZipSame = tcpZipPromise !== zipCode;
    const { isEDD, itemsWithEdd, isFetchingState, eddError } = this.props;
    if (
      isEDD &&
      !eddError &&
      (!itemsWithEdd || isZipSame) &&
      !isFetchingState &&
      this.isEddEligbleCountry()
    ) {
      this.fetchEddData();
    }
  };

  render() {
    const { isEDD, placeHolderText, fontSize, ...otherProps } = this.props;
    return isEDD ? (
      <EddComponentView
        fetchEdd={this.triggerEdd}
        fetchEddUpdate={this.triggerEddOnUpdate}
        placeHolderText={placeHolderText}
        fontSize={fontSize}
        {...otherProps}
      />
    ) : (
      <>
        {placeHolderText && (
          <BodyCopy
            fontFamily="secondary"
            fontSize={fontSize || 'fs14'}
            component="p"
            textAlign="center"
            color="green.500"
          >
            {placeHolderText}
          </BodyCopy>
        )}
      </>
    );
  }
}

export const mapStateToProps = (state) => ({
  isEDD: getEddFeatureSwitch(state),
  eddAllowedStates: getEddAllowedStates(state),
  addressList: getAddressListState(state),
  profileZipCode: getUserZipCode(state),
  cartOrderItems: BagPageSelector.getOrderItems(state),
  isBossBopisOrder: BagPageSelector.getBossStatus(state) || BagPageSelector.getBopisStatus(state),
  isInternationalShipping: getIsInternationalShipping(state),
  itemsWithEdd: getItemsWithEDD(state),
  orderEddDates: getOrderEddDates(state),
  isFetchingState: getFetchingState(state),
  eddError: getEddErrorState(state),
  currentOrderId: BagPageSelector.getCurrentOrderId(state),
  labels: BagPageSelector.getBagPageLabels(state),
  isEddABTest: getEddABState(state),
  shippingState: BagPageSelector.getShippingAddressState(state),
  shippingZip: BagPageSelector.getShippingAddressZip(state),
  shippingCheckoutAddress: CheckoutSelectors.getShippingAddress(state),
  selectedShippingMethod: getSelectedShippingCode(state),
});

export const mapDispatchToProps = (dispatch) => ({
  getEdd: (payload) => {
    dispatch(getEddData(payload));
  },
  updateShipmentCode: (payload) => {
    dispatch(updateShippingCode(payload));
  },
  resetEddErrorActn: () => {
    dispatch(resetEddError());
  },
});

EddComponent.propTypes = {
  itemsWithEdd: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape({}),
  addressList: PropTypes.shape([]),
  eddAllowedStates: PropTypes.shape([]),
  isEDD: PropTypes.bool,
  getEdd: PropTypes.func.isRequired,
  isBossBopisOrder: PropTypes.bool,
  profileZipCode: PropTypes.string,
  isInternationalShipping: PropTypes.bool,
  isFetchingState: PropTypes.bool,
  currentOrderId: PropTypes.string,
  eddError: PropTypes.bool,
  shippingZip: PropTypes.string,
  shippingState: PropTypes.string,
  shippingCheckoutAddress: PropTypes.shape({}),
  resetEddErrorActn: PropTypes.func,
};

EddComponent.defaultProps = {
  isEDD: false,
  cartOrderItems: null,
  addressList: null,
  isBossBopisOrder: false,
  profileZipCode: null,
  eddAllowedStates: [],
  isInternationalShipping: false,
  isFetchingState: false,
  currentOrderId: '',
  eddError: false,
  shippingZip: '',
  shippingState: '',
  shippingCheckoutAddress: null,
  resetEddErrorActn: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(EddComponent);
