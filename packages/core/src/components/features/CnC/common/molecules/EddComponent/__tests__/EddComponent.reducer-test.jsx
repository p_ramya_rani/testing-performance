// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import EDD_CONSTANTS from '../EddComponent.constants';
import EddReducer from '../container/EddComponent.reducer';

describe('EDD reducer', () => {
  const payload = {
    itemsWithEDD: {
      edd: 'Mon, 5 May',
      orderItemId: '12345678',
      itemPartNumber: '1223333',
    },
  };

  it('should return empty Edd Data as default state', () => {
    expect(EddReducer(undefined, {}).get('eddData')).toBeNull();
  });

  it('should set EDD data', () => {
    const initialState = fromJS({
      eddData: null,
    });
    const updatedState = EddReducer(initialState, {
      type: EDD_CONSTANTS.SET_EDD_DATA,
      payload,
    });
    expect(updatedState.get('eddData')).toEqual(payload);
  });

  it('should reset EDD data', () => {
    const initialState = fromJS({
      eddData: null,
    });
    const updatedState = EddReducer(initialState, {
      type: EDD_CONSTANTS.RESET_EDD_DATA,
    });
    expect(updatedState.get('eddData')).toEqual(null);
  });

  it('should update shipping code', () => {
    const initialState = fromJS({
      shippingCode: null,
    });
    const updatedState = EddReducer(initialState, {
      type: EDD_CONSTANTS.UPDATE_SELECTED_SHIPPING_CODE,
      payload: 'UGNR',
    });
    expect(updatedState.get('shippingCode')).toEqual('UGNR');
  });
});
