// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CouponForm, onSubmitSuccess } from '../views/CouponForm.view';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileWeb: jest.fn(),
  getViewportInfo: () => ({
    isMobile: false,
  }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isMobileApp: jest.fn(),
  isClient: jest.fn().mockImplementation(() => true),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  readCookie: jest.fn(),
  setCookie: jest.fn(),
  getLabelValue: jest.fn(),
}));

describe.only('CouponForm Component', () => {
  let component;
  const props = {
    labels: {
      placeholderText: 'Enter Coupon Code',
      submitButtonLabel: 'Apply',
    },
    dataLocators: {
      submitButton: 'coupon_submit_btn',
      inputField: 'coupon_code',
    },
    fieldName: 'coupon_code',
    handleSubmit: () => {},
  };

  it('CouponForm should be defined', () => {
    component = shallow(<CouponForm {...props} />);
    expect(component).toBeDefined();
  });

  it('CouponForm should render correctly', () => {
    component = shallow(<CouponForm {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('CouponForm should render with error correctly', () => {
    const event = { stopPropagation: () => {} };
    props.error = { msg: '1234' };
    component = shallow(<CouponForm {...props} />);
    component.instance().renderTextBox({ input: { value: '123' } });
    component.instance().toggleTouched();
    component.instance().handleSubmit(event);
    expect(component).toMatchSnapshot();
  });

  it('on Submit Success', () => {
    const reset = jest.fn();
    onSubmitSuccess(undefined, undefined, { reset });
    expect(reset.mock.calls.length).toBe(1);
  });

  it('should not render merge cart limit error', () => {
    const component1 = shallow(<CouponForm {...props} />);
    expect(component1.instance().mergeCouponError()).toEqual(null);
  });

  it('should render merge cart limit error', () => {
    const props2 = { ...props, showCouponMergeError: true };
    const component2 = shallow(<CouponForm {...props2} />);
    expect(component2.instance().mergeCouponError()).toBeTruthy();
  });
});
