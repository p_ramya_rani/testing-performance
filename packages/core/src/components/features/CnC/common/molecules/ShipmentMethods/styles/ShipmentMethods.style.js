// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  justify-content: flex-start;
  .radio-method {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: 0;
    }
    @media ${props => props.theme.mediaQuery.smallOnly} {
      min-height: ${props => props.theme.spacing.ELEM_SPACING.XL};
      margin-right: 6px;
      flex: 1;
      display: flex;
      border-radius: 6px;
      border: solid 1px ${props => props.theme.colorPalette.gray[600]};
      background-color: ${props => props.theme.colorPalette.white};
    }
  }

  .radio-button-checked:focus-within {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      outline: none;
    }
  }

  .estimated-shipping-rate {
    @media ${props => props.theme.mediaQuery.medium} {
      display: none;
      justify-content: flex-start;
    }
    .estimated-shipping-speed {
      font-style: italic;
    }
  }
  .estimated-shipping-size {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      padding: 4px 0px;
      display: flex;
      align-items: center;
      justify-content: center;
      flex: 1;
      border: none;
      border-radius: 5px;
      span {
        text-align: center;
      }
    }
  }

  .is-mobile-visible {
    display: block;
    @media ${props => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
`;

export default styles;
