// 9fbef606107a605d69c0edbcd8029e5d
import { STANDARD_EDD_CODE } from '../EddComponent.constants';

export const selectMaxShippingDateNode = (eddItemsForId, selectedShippingMethod) => {
  let maxDate = '';
  let selectedIndex = -1;

  eddItemsForId.forEach((item, index) => {
    const { shippingDates } = item;
    const selectedMethodDate = shippingDates[selectedShippingMethod];
    if (selectedMethodDate && selectedMethodDate > maxDate) {
      maxDate = selectedMethodDate;
      selectedIndex = index;
    }
  });
  const selectedEddNode = eddItemsForId[selectedIndex];
  return selectedEddNode && selectedEddNode.shippingDates[selectedShippingMethod];
};

export const getItemEDD = (eddItemsForId, selectedShippingMethod) => {
  let itemEdd;
  if (eddItemsForId && eddItemsForId.length) {
    if (selectedShippingMethod) {
      itemEdd = selectMaxShippingDateNode(eddItemsForId, selectedShippingMethod);
    } else {
      const standardEdd = selectMaxShippingDateNode(eddItemsForId, STANDARD_EDD_CODE);
      const fallbackEddItem = eddItemsForId[0];
      if (standardEdd) {
        itemEdd = standardEdd;
      } else if (fallbackEddItem.shippingDates.PMDC) {
        itemEdd = fallbackEddItem.shippingDates.PMDC;
      } else if (fallbackEddItem.shippingDates.U2AK) {
        itemEdd = fallbackEddItem.shippingDates.U2AK;
      }
    }
  }
  return itemEdd;
};

export const filterEddItemsById = (cartItems, id) => {
  return (
    cartItems &&
    cartItems.filter((item) => {
      if (item.parentSkuId) {
        return String(item.parentSkuId) === id;
      }
      return String(item.itemPartNumber) === id;
    })
  );
};
