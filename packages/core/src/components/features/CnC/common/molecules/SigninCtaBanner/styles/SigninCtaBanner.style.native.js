import styled from 'styled-components/native';

const SignInCtaBannerWrapper = styled.View`
  text-align: center;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
  background: ${(props) => props.theme.colorPalette.white};
  border-radius: 10px;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  border-left-width: 3px;
  border-left-color: ${(props) => props.theme.colors.REWARDS.PLCC};
  border-top-width: 3px;
  border-top-color: ${(props) => props.theme.colors.REWARDS.PLCC};
  border-right-width: 3px;
  border-right-color: ${(props) => props.theme.colors.REWARDS.NON_PLCC};
  border-bottom-width: 3px;
  border-bottom-color: ${(props) => props.theme.colors.REWARDS.NON_PLCC};
  min-height: 34px;
`;

const CheckoutSectionTitle = styled.View`
  display: flex;
  justify-content: center;
  flex-direction: row;
`;

const CtaDivider = styled.View`
  margin: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  font-size: ${(props) => props.theme.typography.fontSizes.fs14};
`;

const CtaLink = styled.View`
  text-decoration: underline;
  font-size: ${(props) => props.theme.typography.fontSizes.fs14};
`;

export { SignInCtaBannerWrapper, CheckoutSectionTitle, CtaDivider, CtaLink };
