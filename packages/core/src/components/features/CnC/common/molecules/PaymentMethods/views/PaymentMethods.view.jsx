// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { isApplePayEligible } from '@tcp/core/src/utils/utils';
import { isMobileWeb } from '@tcp/core/src/utils';
import LabeledRadioButton from '../../../../../../common/atoms/LabeledRadioButton';
import Row from '../../../../../../common/atoms/Row';
import Col from '../../../../../../common/atoms/Col';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import styles from '../styles/PaymentMethods.style';
import withStyles from '../../../../../../common/hoc/withStyles';

const PaymentBox = ({ title, paymentMethodType }) => {
  return (
    <BodyCopy component="div" className={`payment-method-box ${paymentMethodType}-box`}>
      <BodyCopy component="div" className={`payment-method ${paymentMethodType}`} />
      <BodyCopy
        className={`elem-pt-XXXS ${paymentMethodType}-body`}
        fontFamily="primary"
        fontSize="fs14"
        textAlign="center"
      >
        {title}
      </BodyCopy>
    </BodyCopy>
  );
};

PaymentBox.propTypes = {
  title: PropTypes.string.isRequired,
  paymentMethodType: PropTypes.string.isRequired,
};

const getAppleEnabled = (isApplePayEnabled, isApplePayEnabledOnBilling) => {
  return isApplePayEnabled && isApplePayEnabledOnBilling && isApplePayEligible();
};

const getVenmoEnabled = (isVenmoEnabled, isUSSite) => {
  return isVenmoEnabled && isUSSite;
};

const showPaymentMethodsSecondRow = (
  isAfterPayEnabled,
  isApplePayVisible,
  isVenmoEnabled,
  isPayPalEnabled
) => {
  return (
    isAfterPayEnabled && isApplePayVisible && isVenmoEnabled && isPayPalEnabled && isMobileWeb()
  );
};

export const SecondOptionsRow = ({ labels }) => {
  const [showOptions, setShowOptions] = useState(false);
  const { otherPaymentOptions } = labels;
  return (
    <>
      {!showOptions && (
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs13"
          className="elem-mt-XL other-payment-options"
          onClick={() => setShowOptions(true)}
        >
          {otherPaymentOptions}
          <span className="venmo-small" />
        </BodyCopy>
      )}
      {showOptions && (
        <Col className="container second-row" colSize={{ small: 4, medium: 6, large: 6 }}>
          <BodyCopy component="div" className="payment-method-container">
            <Field
              component={LabeledRadioButton}
              key="Venmo"
              selectedValue="venmo"
              title=""
              subtitle=""
              name="paymentMethodId"
              variation="secondary"
              data-locator="venmoRadioBtn"
            >
              <PaymentBox paymentMethodType="venmo" />
            </Field>
          </BodyCopy>
        </Col>
      )}
    </>
  );
};

SecondOptionsRow.propTypes = {
  labels: PropTypes.shape({}).isRequired,
};

const PaymentMethods = ({
  className,
  paymentHeader,
  labels,
  isVenmoEnabled,
  isApplePayEnabled,
  isUSSite,
  isPayPalEnabled,
  isApplePayEnabledOnBilling,
  isAfterPayEnabled,
}) => {
  const { paymentMethod, paypal, creditCard } = labels;
  const payPalLabel = `${paymentMethod} ${paypal}`;
  const isApplePayVisible = getAppleEnabled(isApplePayEnabled, isApplePayEnabledOnBilling);
  const isVenmoBtnEnabled = getVenmoEnabled(isVenmoEnabled, isUSSite);
  const showSecondRow = showPaymentMethodsSecondRow(
    isAfterPayEnabled,
    isApplePayVisible,
    isVenmoEnabled,
    isPayPalEnabled
  );

  return (
    <>
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="extrabold"
        className="elem-mb-XXS"
      >
        {paymentHeader}
      </BodyCopy>
      <Row fullBleed className={className}>
        <Col className="container" colSize={{ small: 4, medium: 6, large: 6 }}>
          <BodyCopy component="div" className="payment-method-container">
            <Field
              component={LabeledRadioButton}
              key="Credit Card"
              selectedValue="creditCard"
              name="paymentMethodId"
              hideSubtitleOnMobile
              variation="secondary"
              data-locator="creditCardRadioBtn"
            >
              <PaymentBox title={creditCard} paymentMethodType="creditCard" />
            </Field>
          </BodyCopy>

          {isPayPalEnabled && (
            <BodyCopy component="div" className="payment-method-container">
              <Field
                component={LabeledRadioButton}
                key="PayPal"
                selectedValue="payPal"
                aria-label={payPalLabel}
                name="paymentMethodId"
                hideSubtitleOnMobile
                variation="secondary"
                data-locator="paypalRadioBtn"
              >
                <PaymentBox paymentMethodType="payPal" />
              </Field>
            </BodyCopy>
          )}

          {isAfterPayEnabled && (
            <div className="payment-method-container">
              <Field
                component={LabeledRadioButton}
                key="afterpay"
                selectedValue="afterpay"
                title=""
                subtitle=""
                name="paymentMethodId"
                variation="secondary"
                data-locator="afterpayRadioBtn"
              >
                <PaymentBox paymentMethodType="afterpay" />
              </Field>
            </div>
          )}

          {isApplePayVisible && (
            <BodyCopy component="div" className="payment-method-container">
              <Field
                component={LabeledRadioButton}
                key="Apple"
                selectedValue="apple"
                title=""
                subtitle=""
                name="paymentMethodId"
                variation="secondary"
                data-locator="appleRadioBtn"
              >
                <PaymentBox paymentMethodType="applePay" />
              </Field>
            </BodyCopy>
          )}

          {!showSecondRow && isVenmoBtnEnabled && (
            <BodyCopy component="div" className="payment-method-container">
              <Field
                component={LabeledRadioButton}
                key="Venmo"
                selectedValue="venmo"
                title=""
                subtitle=""
                name="paymentMethodId"
                variation="secondary"
                data-locator="venmoRadioBtn"
              >
                <PaymentBox paymentMethodType="venmo" />
              </Field>
            </BodyCopy>
          )}
        </Col>
        {showSecondRow && <SecondOptionsRow labels={labels} />}
      </Row>
    </>
  );
};

PaymentMethods.propTypes = {
  className: PropTypes.string,
  paymentHeader: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  isVenmoEnabled: PropTypes.bool, // Venmo Kill Switch
  isApplePayEnabled: PropTypes.bool.isRequired,
  isApplePayEnabledOnBilling: PropTypes.bool.isRequired,
  isUSSite: PropTypes.bool,
  isPayPalEnabled: PropTypes.bool,
  isAfterPayEnabled: PropTypes.bool,
};
PaymentMethods.defaultProps = {
  className: '',
  paymentHeader: '',
  isVenmoEnabled: false,
  isUSSite: true,
  isPayPalEnabled: true,
  isAfterPayEnabled: false,
};

export default withStyles(PaymentMethods, styles);
export { PaymentMethods as PaymentMethodsVanilla };
