// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const FieldWrapper = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;

const PaymentMethodsWrapper = styled.View`
  display: flex;
`;

const SecondRowLabelWrapper = styled.View`
  display: flex;
  flex-direction: row;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0 0;
`;

const SecondRowFieldWrapper = styled.View`
  display: flex;
  width: 92px;
  height: 78px;
`;

const SecondRowLabelImageWrapper = styled.View`
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

export {
  FieldWrapper,
  PaymentMethodsWrapper,
  SecondRowFieldWrapper,
  SecondRowLabelWrapper,
  SecondRowLabelImageWrapper,
};
