// 9fbef606107a605d69c0edbcd8029e5d 
import { call, takeLeading, put, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getEdd } from '@tcp/core/src/services/abstractors/CnC/';
import { validateReduxCache } from '@tcp/core/src/utils/cache.util';
import constants from '../EddComponent.constants';
import { setEddData, showLoader, hideLoader, setEddError } from './EddComponent.actions';
import { getEddCachingInterval } from './EddComponent.selectors';

/**
 * @function getEddDetails
 * @description This function will call getEddDetails Abstractor to get edd data
 */
export function* getEddDetails({ payload }) {
  try {
    yield put(showLoader());
    const eddCachingTTL = yield select(getEddCachingInterval);
    const data = yield call(getEdd, payload);
    data.eddCachingTTL = eddCachingTTL;
    yield put(setEddData(data));
  } catch (e) {
    logger.error('EDD error', {
      error: e,
      extraData: {
        component: 'EDD Saga - getEDDData',
        payloadRecieved: payload,
      },
    });
    yield put(setEddError(true));
  } finally {
    yield put(hideLoader());
  }
}

/**
 * @function EddSaga
 * @description watcher function for fetch orders.
 */
export function* EddSaga() {
  const cachedEddData = validateReduxCache(getEddDetails);
  yield takeLeading(constants.GET_EDD_DATA, cachedEddData);
}

export default EddSaga;
