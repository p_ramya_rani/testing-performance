// 9fbef606107a605d69c0edbcd8029e5d
import { EDD_ACTION_PATTERN } from '@tcp/core/src/constants/reducer.constants';

export const STANDARD_EDD_CODE = 'UGNR';

export default {
  GET_EDD_DATA: `${EDD_ACTION_PATTERN}GET_EDD_DATA`,
  SET_EDD_DATA: `${EDD_ACTION_PATTERN}SET_EDD_DATA`,
  RESET_EDD_DATA: `${EDD_ACTION_PATTERN}RESET_EDD_DATA`,
  SHOW_LOADER: `${EDD_ACTION_PATTERN}SHOW_LOADER`,
  HIDE_LOADER: `${EDD_ACTION_PATTERN}HIDE_LOADER`,
  UPDATE_SELECTED_SHIPPING_CODE: `${EDD_ACTION_PATTERN}UPDATE_SELECTED_SHIPPING_CODE`,
  SET_EDD_ERROR: `${EDD_ACTION_PATTERN}SET_EDD_ERROR`,
  EDD_TTL: 900000,
  CLEAR_EDD_TTL: `${EDD_ACTION_PATTERN}CLEAR_EDD_TTL`,
  RESET_EDD_ERROR: `${EDD_ACTION_PATTERN}RESET_EDD_ERROR`,
};
