import React from 'react';
import { shallow } from 'enzyme';
import Router from 'next/router';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { SigninCtaBannerVanilla } from '../SigninCtaBanner.view';
import { BodyCopy } from '../../../../../../../common/atoms';

jest.mock('next/router', () => ({ push: jest.fn() }));

describe('SigninCtaBanner component', () => {
  const props = {
    labels: {},
    className: '',
  };
  it('should render correctly', () => {
    const tree = shallow(<SigninCtaBannerVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should call redirects to login page', () => {
    const component = shallow(<SigninCtaBannerVanilla {...props} />);
    component.find(BodyCopy).at(0).simulate('click', { preventDefault: jest.fn() });
    expect(Router.push).toHaveBeenCalledWith(
      '/index?target=login&successtarget=/',
      '/undefined/home/login',
      { query: undefined }
    );
  });

  it('should call redirects to register page', () => {
    const component = shallow(<SigninCtaBannerVanilla {...props} />);
    component.find(BodyCopy).at(2).simulate('click', { preventDefault: jest.fn() });
    expect(Router.push).toHaveBeenCalledWith(
      '/index?target=register&successtarget=/',
      '/undefined/home/register',
      { query: undefined }
    );
  });

  it('should call setLoyaltyLocation function when click sign-in', () => {
    const setLoyaltyLocation = jest.fn();
    const component = shallow(<SigninCtaBannerVanilla {...props} />);
    component.find(ClickTracker).at(0).simulate('click', { preventDefault: jest.fn() });
    setLoyaltyLocation.mockImplementation();
    expect(window.loyaltyLocation).toBe('cart');
  });

  it('should call setLoyaltyLocation function when click create-account', () => {
    const setLoyaltyLocation = jest.fn();
    const component = shallow(<SigninCtaBannerVanilla {...props} />);
    component.find(ClickTracker).at(1).simulate('click', { preventDefault: jest.fn() });
    setLoyaltyLocation.mockImplementation();
    expect(window.loyaltyLocation).toBe('cart');
  });
});
