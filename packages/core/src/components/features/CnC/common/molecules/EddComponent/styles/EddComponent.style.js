// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .sth-icon {
    width: 26px;
    height: 22px;
    vertical-align: bottom;
    margin-right: 4px;
    ${(props) => (props.newBag ? 'vertical-align:middle;margin-right:7px;' : '')}
  }
  .estimated-shipping-speed {
    padding-left: 40px;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: 0;
    }
  }

  .estimated-shipping-date {
    padding-left: 5px;
    white-space: nowrap;
    display: inline-block;
  }
`;
