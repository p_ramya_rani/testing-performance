// 9fbef606107a605d69c0edbcd8029e5d 
export const couponFieldLength = 18;

export const getFormattedValue = couponValue => {
  return couponValue ? couponValue.replace(/[^0-9a-z]/gi, '').toUpperCase() : '';
};
