// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../EddComponent.constants';

/**
 * @function getOrdersList
 * action creator for type: GET_ORDERS_LIST
 */
export const getEddData = payload => ({
  type: constants.GET_EDD_DATA,
  payload,
});

/**
 * @function setOrdersList
 * action creator for type: SET_ORDERS_LIST
 */
export const setEddData = payload => {
  return {
    type: constants.SET_EDD_DATA,
    payload,
  };
};

export const showLoader = () => {
  return {
    type: constants.SHOW_LOADER,
  };
};

export const hideLoader = () => {
  return {
    type: constants.HIDE_LOADER,
  };
};

export const resetEddData = () => {
  return {
    type: constants.RESET_EDD_DATA,
  };
};

export const resetEddError = () => {
  return {
    type: constants.RESET_EDD_ERROR,
  };
};

export const clearEddTTL = () => {
  return {
    type: constants.CLEAR_EDD_TTL,
  };
};

export const updateShippingCode = payload => {
  return {
    type: constants.UPDATE_SELECTED_SHIPPING_CODE,
    payload,
  };
};

export const setEddError = payload => {
  return {
    type: constants.SET_EDD_ERROR,
    payload,
  };
};
