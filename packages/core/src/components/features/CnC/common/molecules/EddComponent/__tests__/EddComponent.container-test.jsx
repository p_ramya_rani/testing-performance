// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { List } from 'immutable';
import { shallow } from 'enzyme';
import { mapDispatchToProps, EddComponent } from '../container/EddComponent.container';

describe('EDD Container', () => {
  it('should return an action getEddData which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.getEdd();
    expect(dispatch.mock.calls).toHaveLength(1);
  });
});

describe('#instances getDefaultZipCode', () => {
  const props = {
    isEDD: true,
    addressList: List([
      {
        addressId: '12345',
        primary: 'true',
        zipCode: '10010',
        state: 'NJ',
      },
    ]),
    shippingZip: '10006',
    shippingState: 'TX',
    fetchEdd: jest.fn(),
  };

  const newprops = {
    isEDD: true,
    addressList: List([]),
    shippingZip: '10012',
    shippingState: 'FL',
    fetchEdd: jest.fn(),
    shippingCheckoutAddress: {},
  };
  const checkoutZipcodeProp = {
    isEDD: true,
    addressList: List([]),
    shippingState: 'FL',
    fetchEdd: jest.fn(),
    shippingCheckoutAddress: {
      zipCode: '10013',
      state: 'NJ',
    },
  };

  const component = shallow(<EddComponent {...props} />);

  it('should return zipcode if from addresslist if it is available', () => {
    expect(component.instance().getDefaultZipCode()).toBe('10010');
  });
  it('should return zipcode from checkout address state if it is available', () => {
    component.setProps(checkoutZipcodeProp);
    expect(component.instance().getDefaultZipCode()).toBe('10013');
  });
  it('should return zipcode from shipingZip if addresslist is not available', () => {
    component.setProps(newprops);
    expect(component.instance().getDefaultZipCode()).toBe('10012');
  });
});

describe('#instances getStateFromAddress', () => {
  const props = {
    isEDD: true,
    addressList: List([
      {
        addressId: '12345',
        primary: 'true',
        zipCode: '10010',
        state: 'NJ',
      },
    ]),
    shippingZip: '10006',
    shippingState: 'TX',
    fetchEdd: jest.fn(),
    shippingCheckoutAddress: {},
  };

  const newprops = {
    isEDD: true,
    addressList: List([]),
    shippingZip: '10012',
    shippingState: 'FL',
    fetchEdd: jest.fn(),
    shippingCheckoutAddress: {},
  };

  const checkoutStatecodeProp = {
    isEDD: true,
    addressList: List([]),
    shippingState: 'FL',
    fetchEdd: jest.fn(),
    shippingCheckoutAddress: {
      zipCode: '10013',
      state: 'NJ',
    },
  };
  const component = shallow(<EddComponent {...props} />);
  let addressList = List([
    {
      addressId: '12345',
      primary: 'true',
      zipCode: '10010',
      state: 'NJ',
    },
  ]);
  it('should return state from checkout address state if it is available', () => {
    component.setProps(checkoutStatecodeProp);
    expect(component.instance().getStateFromAddress(addressList)).toBe('NJ');
  });

  it('should return state if from addresslist if it is available', () => {
    expect(component.instance().getStateFromAddress(addressList)).toBe('NJ');
  });

  it('should return state from shippingState if addresslist is not available', () => {
    addressList = List([]);
    component.setProps(newprops);
    expect(component.instance().getStateFromAddress(addressList)).toBe('FL');
  });
});
