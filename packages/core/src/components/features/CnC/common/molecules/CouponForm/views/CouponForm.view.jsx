// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { KEY_CODES } from '@tcp/core/src/constants/keyboard.constants';
import { Button, TextBox, BodyCopy, Heading } from '@tcp/core/src/components/common/atoms';
import { routerPush } from '@tcp/core/src/utils';
import { CUSTOMER_HELP_DIRECT_ROUTES } from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { keyboard } from '../../../../../../../../../web/src/constants/constants';
import style from '../styles/CouponForm.style';
import ErrorMessage from '../../ErrorMessage';
import { couponFieldLength, getFormattedValue } from '../CouponForm.utils';

class CouponForm extends React.PureComponent {
  state = { touched: false };

  renderTextBox = ({ input, ...otherParams }) => {
    const formattedValue = input.value ? getFormattedValue(input.value) : '';
    // eslint-disable-next-line
    input = { ...input, value: formattedValue };
    return <TextBox input={input} {...otherParams} />;
  };

  toggleTouched = () => {
    const { touched } = this.state;
    this.setState({ touched: !touched });
  };

  handleKeyDown = (event, callback) => {
    const { KEY_ENTER, KEY_SPACE } = keyboard;
    const { which } = event;
    if (which === KEY_ENTER || which === KEY_SPACE) {
      callback({ status: 'open' });
    }
  };

  handleSubmit = (e) => {
    e.stopPropagation();
    const { handleSubmit } = this.props;
    const { touched } = this.state;

    if (touched) {
      this.toggleTouched();
    }
    return handleSubmit(e);
  };

  mergeCouponError = () => {
    const { labels: { mergeCartCouponError = '' } = {}, showCouponMergeError } = this.props;
    if (showCouponMergeError) {
      return (
        <ErrorMessage
          fontSize="fs12"
          fontWeight="extrabold"
          className="coupon_error_message"
          error={mergeCartCouponError}
        />
      );
    }
    return null;
  };

  render() {
    const {
      labels,
      dataLocators,
      fieldName,
      className,
      error,
      isFetching,
      idPrefix,
      pristine,
      submitting,
      newBag,
    } = this.props;
    const { touched } = this.state;
    return (
      <div className={className}>
        {!touched && (
          <ErrorMessage
            fontSize="fs12"
            fontWeight="extrabold"
            className="coupon_error_message"
            error={error && error.msg}
            isEspot={error && error.isPlaceCashError}
          />
        )}
        {this.mergeCouponError()}
        <div className={`coupon_form_container ${newBag ? 'rounded-container' : ''}`}>
          <Heading
            fontFamily="primaryFontFamily"
            component="h2"
            variant="h6"
            className="coupon_form_heading"
            color="black"
          >
            {labels.couponCodeHeader}
            <BodyCopy
              fontSize="fs12"
              fontFamily="secondary"
              className="coupon_need_help_link"
              component="span"
              fontWeight="semibold"
              tabIndex="0"
              onClick={() =>
                routerPush(
                  CUSTOMER_HELP_DIRECT_ROUTES.COUPON_ISSUE.to,
                  CUSTOMER_HELP_DIRECT_ROUTES.COUPON_ISSUE.asPath
                )
              }
              role="button"
              onKeyDown={(e) => {
                if (e.keyCode === KEY_CODES.KEY_ENTER) {
                  routerPush(
                    CUSTOMER_HELP_DIRECT_ROUTES.COUPON_ISSUE.to,
                    CUSTOMER_HELP_DIRECT_ROUTES.COUPON_ISSUE.asPath
                  );
                }
              }}
            >
              {labels.couponNeedHelpText}
            </BodyCopy>
          </Heading>
          <form onSubmit={this.handleSubmit} className="coupon_submit_form">
            <Field
              placeholder={labels.placeholderText}
              name={fieldName}
              id={`${fieldName}-id-${idPrefix}`}
              type="text"
              onChange={!touched && this.toggleTouched}
              component={this.renderTextBox}
              dataLocator={dataLocators.inputField}
              className="coupon_code_input"
              maxLength={couponFieldLength}
              newBag={newBag}
            />
            <Button
              disabled={isFetching || pristine || submitting}
              buttonVariation="fixed-width"
              type="submit"
              data-locator={dataLocators.submitButton}
              className="coupon_submit_button"
              newBag={newBag}
              onClick={this.handleSubmit}
            >
              {labels.submitButtonLabel}
            </Button>
          </form>
        </div>
      </div>
    );
  }
}

CouponForm.propTypes = {
  labels: PropTypes.shape({
    placeholderText: PropTypes.string,
    validationErrorLabel: PropTypes.string,
    termsTextLabel: PropTypes.string,
    submitButtonLabel: PropTypes.string,
  }),
  dataLocators: PropTypes.shape({
    submitButton: PropTypes.string,
    inputField: PropTypes.string,
  }),
  handleSubmit: PropTypes.func,
  fieldName: PropTypes.string,
  className: PropTypes.string.isRequired,
  error: PropTypes.string,
  idPrefix: PropTypes.string,
  onNeedHelpTextClick: PropTypes.func,
  isFetching: PropTypes.isRequired,
  coupon: PropTypes.string,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  showCouponMergeError: false,
  newBag: PropTypes.bool,
};

CouponForm.defaultProps = {
  labels: {
    placeholderText: 'Enter Coupon Code',
    submitButtonLabel: 'Apply',
    couponCodeHeader: 'COUPON CODE',
    couponNeedHelpText: 'Need Help?',
  },
  dataLocators: {
    submitButton: 'coupon_submit_btn',
    inputField: 'coupon_code',
  },
  error: '',
  fieldName: 'couponCode',
  handleSubmit: () => {},
  onNeedHelpTextClick: () => {},
  idPrefix: '',
  coupon: '',
  showCouponMergeError: false,
  newBag: false,
};

export const onSubmitSuccess = (result, dispatch, { reset }) => reset();

export default reduxForm({
  form: 'CouponForm',
  onSubmitSuccess,
})(withStyles(CouponForm, style));

export { CouponForm };
