// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { isMobileWeb } from '@tcp/core/src/utils';
import { isApplePayEligible } from '@tcp/core/src/utils/utils';
import { PaymentMethodsVanilla, SecondOptionsRow } from '../views/PaymentMethods.view';

jest.mock('@tcp/core/src/utils', () => {
  const originalModule = jest.requireActual('@tcp/core/src/utils');
  return {
    ...originalModule,
    getViewportInfo: jest.fn(),
    isMobileWeb: jest.fn(),
    isClient: jest.fn(),
    getIconPath: jest.fn(),
  };
});

jest.mock('@tcp/core/src/utils/utils', () => {
  const originalModule = jest.requireActual('@tcp/core/src/utils/utils');
  return {
    ...originalModule,
    isApplePayEligible: jest.fn(),
  };
});

describe('ButtonList component', () => {
  const props = {
    className: '',
    paymentHeader: '',
    labels: {},
  };

  it('renders correctly without props', () => {
    const component = shallow(<PaymentMethodsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should not render SecondOptionsRow when applepay is not available', () => {
    isMobileWeb.mockImplementation(() => true);
    isApplePayEligible.mockImplementation(() => true);
    const props1 = {
      ...props,
      isApplePayEnabled: false,
      isApplePayEnabledOnBilling: false,
      isVenmoEnabled: true,
      isPayPalEnabled: true,
      isUSSite: true,
    };
    const component = shallow(<PaymentMethodsVanilla {...props1} />);
    expect(component.find(SecondOptionsRow)).toHaveLength(0);
    expect(component).toMatchSnapshot();
  });

  it('should render SecondOptionsRow when applepay is available', () => {
    isMobileWeb.mockImplementation(() => true);
    isApplePayEligible.mockImplementation(() => true);
    const props1 = {
      ...props,
      isApplePayEnabled: true,
      isApplePayEnabledOnBilling: true,
      isVenmoEnabled: true,
      isPayPalEnabled: true,
      isUSSite: true,
      isAfterPayEnabled: true,
    };
    const component = shallow(<PaymentMethodsVanilla {...props1} />);
    expect(component.find(SecondOptionsRow)).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });
});
