/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useRef } from 'react';
import { TouchableOpacity, TouchableWithoutFeedback, Text } from 'react-native';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import PropTypes from 'prop-types';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { MULTI_PACK_TYPE } from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import Loader from '@tcp/core/src/components/common/molecules/Loader';
import GhostOverlay from '@tcp/core/src/components/common/atoms/GhostOverlay';
import { shouldRenderPoints } from '@tcp/core/src/components/features/CnC/AddedToBag/util/utility';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import Modal from '../../../../common/molecules/Modal';
import BodyCopy from '../../../../common/atoms/BodyCopy';
import {
  StyledWrapper,
  AddedToBagWrapper,
  ButtonsLoaderWrapper,
  RowWrapper,
  ModalHeading,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  StyledBodyWrapper,
  RecommendationWrapper,
  LoyaltyBannerWrapper,
  BannerLoaderWrapper,
} from '../styles/AddedToBag.style.native';
import ProductInformation from '../molecules/ProductInformation/views/ProductInformation.views.native';
import BossBanner from '../molecules/BossBanner/views/BossBanner.views.native';
import AddedToBagViewPoints from '../../AddedToBagViewPoints';
import AddedToBagActions from '../../AddedToBagActions';
import LoyaltyBanner from '../../LoyaltyBanner';
import VenmoPaymentButton from '../../../../common/atoms/VenmoPaymentButton';
import config, { defaultProps } from './config';
import PickupBannerView from '../molecules/PickupBanner/views/PickupBanner.view.native';
import { getPartNumber, getShowPickupBanner, isAddedFromSBP } from './AddedToBag.utils';
import AddedToBagNew from './AddedToBagNew.view.native';

const closeIcon = require('../../../../../../../mobileapp/src/assets/images/close.png');

const getContainerStyle = (navigation) => {
  if (!navigation.getParam('headerMode', false)) {
    return { width: 25, backgroundColor: 'rgba(0, 0, 0, 0.5)' };
  }
  return { width: 0, backgroundColor: 'rgb(0, 0, 0)' };
};

const getCloseIcon = (onRequestClose, labels) => {
  return (
    <ImageWrapper>
      <StyledTouchableOpacity
        accessibilityRole="button"
        accessibilityLabel={labels.close}
        onPress={onRequestClose}
      >
        <StyledCrossImage source={closeIcon} />
      </StyledTouchableOpacity>
    </ImageWrapper>
  );
};

const getRowWrapper = (labels, onRequestClose, navigation, bagLoading) => {
  if (!navigation.getParam('headerMode', false)) {
    return (
      <RowWrapper>
        <ModalHeading>
          <BodyCopy
            mobileFontFamily="secondary"
            fontWeight="semibold"
            fontSize="fs16"
            text={labels.addedToBag}
          />
        </ModalHeading>
        {!bagLoading && getCloseIcon(onRequestClose, labels)}
      </RowWrapper>
    );
  }
  return <RowWrapper />;
};

const isQualifiedRoute = (route) => {
  if (
    route === 'ProductListing' ||
    route === 'ProductDetail' ||
    route === 'SearchDetail' ||
    route === 'Home' ||
    route === 'PLP' ||
    route === 'PDP' ||
    route === 'SBPPDP' ||
    route === 'ProductDetailSBP'
  )
    return true;
  return false;
};

const newModalFlag = (isNewReDesignEnabled, isATBModalBackAbTestNewDesign, navigation) => {
  return isATBModalBackAbTestNewDesign && isQualifiedRoute(navigation?.state?.routeName);
};

const conditionTwo = (openState, addedToBagData) => {
  return openState.isOpenNewATBModal || addedToBagData.isOpenNewATBModal;
};

const getProductsWrapper = (
  addedToBagData,
  labels,
  quantity,
  pointsSummary,
  bagLoading,
  { isSuppressGetOrderDetails, isAvoidGetOrderDetails, isATBHidePoints },
  alternateBrand
) => {
  if (Array.isArray(addedToBagData)) {
    const hasDoubleItems = addedToBagData.length > 1;
    return addedToBagData.map((item) => {
      const prodDataATB = { ...item, imgConfig: 't_t_atb_m' };
      return (
        <ProductInformation
          data={prodDataATB}
          labels={labels}
          isDoubleAddedToBag
          hasDoubleItems={hasDoubleItems}
          isSuppressGetOrderDetails={isSuppressGetOrderDetails}
          bagLoading={bagLoading}
          isAvoidGetOrderDetails={isAvoidGetOrderDetails}
          isATBHidePoints={isATBHidePoints}
          alternateBrand={alternateBrand}
        />
      );
    });
  }
  const prodData = { ...addedToBagData, imgConfig: 't_t_atb_m' };
  return (
    <ProductInformation
      data={{ ...prodData, ...pointsSummary }}
      labels={labels}
      quantity={quantity}
      isAvoidGetOrderDetails={isAvoidGetOrderDetails}
      isATBHidePoints={isATBHidePoints}
      alternateBrand={alternateBrand}
    />
  );
};

const renderGhostOverlay = (addedToBagGhostLoaderState, addedToBagGhostLoaderCompleteState) => {
  return (
    addedToBagGhostLoaderState && (
      <GhostOverlay loadingMsg="ADDED TO BAG" isComplete={addedToBagGhostLoaderCompleteState} />
    )
  );
};

const renderLoyaltyBanner = (navigation) => {
  return (
    <LoyaltyBannerWrapper>
      <LoyaltyBanner pageCategory="isAddedToBagPage" navigation={navigation} />
    </LoyaltyBannerWrapper>
  );
};

export const getmultipackProductType = (
  addedToBagData,
  isABTestOffStyleWith,
  isPhysicalMpackEnabled
) => {
  const TCPStyleType =
    addedToBagData && addedToBagData.skuInfo && addedToBagData.skuInfo.TCPStyleType;
  return (
    isPhysicalMpackEnabled && TCPStyleType === MULTI_PACK_TYPE.MULTIPACK && !isABTestOffStyleWith
  );
};

const getPortalValue = (ismpackProductType) =>
  ismpackProductType
    ? Constants.RECOMMENDATIONS_MBOXNAMES.STYLE_WITH
    : Constants.RECOMMENDATIONS_MBOXNAMES.ADD_TO_BAG_MODAL_CAROUSAL;

const renderPickupBanner = (
  labels,
  addedToBagData,
  showPickupBanner,
  {
    showBOPISPickupInstead,
    getBopisInventoryDetails,
    bopisItemInventory,
    addItemToCartInPickup,
    addToPickupError,
    setAddToPickupError,
    userDefaultStore,
  }
) => {
  return showPickupBanner ? (
    <PickupBannerView
      labels={labels}
      addedToBagData={addedToBagData}
      showBOPISPickupInstead={showBOPISPickupInstead}
      getBopisInventoryDetails={getBopisInventoryDetails}
      bopisItemInventory={bopisItemInventory}
      addItemToCartInPickup={addItemToCartInPickup}
      addToPickupError={addToPickupError}
      setAddToPickupError={setAddToPickupError}
      userDefaultStore={userDefaultStore}
    />
  ) : null;
};

const renderLoader = (openState, addedToBagGhostLoaderState) => {
  return openState && !addedToBagGhostLoaderState && <Loader />;
};

const renderBagPageActions = (
  bagLoading,
  labels,
  navigation,
  onRequestClose,
  addedToBagData,
  isBossEnabled,
  isATBHidePoints
) => {
  return !bagLoading ? (
    <>
      <AddedToBagActions
        labels={labels}
        navigation={navigation}
        closeModal={onRequestClose}
        showAddTobag
        fromAddedToBagModal
        hideHeader={(hide) => {
          navigation.setParams({ headerMode: hide });
        }}
        addedFromSBP={isAddedFromSBP(addedToBagData)}
      />
      {isBossEnabled && <BossBanner labels={labels} />}
      {!isATBHidePoints && renderLoyaltyBanner(navigation)}
    </>
  ) : (
    <>
      <ButtonsLoaderWrapper>
        <LoaderSkelton height={40} />
      </ButtonsLoaderWrapper>
      <BannerLoaderWrapper>
        <LoaderSkelton height={40} />
      </BannerLoaderWrapper>
      <ButtonsLoaderWrapper>
        <LoaderSkelton height={20} />
      </ButtonsLoaderWrapper>
      <BannerLoaderWrapper>
        <LoaderSkelton height={60} />
      </BannerLoaderWrapper>
    </>
  );
};

const useEffectFirstIfFlag = (showPickupBanner, isMounted, openState, userDefaultStore) => {
  return showPickupBanner && !isMounted.current && openState && !userDefaultStore.basicInfo;
};

const AddedToBag = ({
  openState,
  onRequestClose,
  addedToBagData,
  addToBagModalError,
  labels,
  quantity,
  navigation,
  pointsSummary,
  bagLoading,
  hideHeader,
  isBossEnabled,
  isSuppressGetOrderDetails,
  isVenmoAppInstalled,
  isVenmoEnabled,
  isAvoidGetOrderDetails,
  isABTestOffStyleWith,
  isPhysicalMpackEnabled,
  addedToBagGhostLoaderState,
  addedToBagGhostLoaderCompleteState,
  showBOPISPickupInstead,
  getBopisInventoryDetails,
  bopisItemInventory,
  addItemToCartInPickup,
  addToPickupError,
  setAddToPickupError,
  userDefaultStore,
  isATBHidePoints,
  getFavoriteStore,
  isNewReDesignEnabled,
  handleContinueShopping,
  router,
  isInternationalShipping,
  enableKeepShoppingCTA,
  isPayPalButtonRendered,
  isPayPalEnabled,
  addedToBagLoaderState,
  addedToBagInterval,
  totalBagItems,
  multipackProductType,
  isPayPalWebViewEnable,
  isATBModalBackAbTestNewDesign,
  isMpackItem,
  pdpLabels,
  loyaltyAPICallStatus,
  aTbAPICallStatus,
  addToBagEcomNewAfterModalOpened,
  openStatePayloadData,
  isPlcc,
  alternateBrand,
  itemInfo,
}) => {
  const partNumber = getPartNumber(addedToBagData);
  const carouselProps = {
    ...config.CAROUSEL_OPTIONS,
    touchThreshold: 500,
  };

  const ismpackProductType = getmultipackProductType(
    addedToBagData,
    isABTestOffStyleWith,
    isPhysicalMpackEnabled
  );
  const { pickupType } = addedToBagData;
  const showPickupBanner = getShowPickupBanner(showBOPISPickupInstead, pickupType);
  const isMounted = useRef(null);

  useEffect(() => {
    if (useEffectFirstIfFlag(showPickupBanner, isMounted, openState, userDefaultStore)) {
      isMounted.current = true;
      const latLong = readCookie('tcpGeoLoc');
      if (latLong) {
        latLong.then((res) => {
          if (res) {
            const [lat, long] = res.split('|');
            getFavoriteStore({ geoLatLang: { lat, long }, isForceUpdate: true });
          }
        });
      }
    }
  }, [openState]);

  if (
    newModalFlag(isNewReDesignEnabled, isATBModalBackAbTestNewDesign, navigation) &&
    conditionTwo(openState, addedToBagData)
  ) {
    return (
      <AddedToBagNew
        openState={openState}
        openStatePayloadData={openStatePayloadData}
        closeModal={onRequestClose}
        router={router}
        onRequestClose={onRequestClose}
        showBOPISPickupInstead={showBOPISPickupInstead}
        addedToBagData={addedToBagData}
        addToBagModalError={addToBagModalError}
        getBopisInventoryDetails={getBopisInventoryDetails}
        bopisItemInventory={bopisItemInventory}
        addItemToCartInPickup={addItemToCartInPickup}
        addToPickupError={addToPickupError}
        setAddToPickupError={setAddToPickupError}
        userDefaultStore={userDefaultStore}
        isInternationalShipping={isInternationalShipping}
        labels={labels}
        quantity={quantity}
        handleContinueShopping={handleContinueShopping}
        navigation={navigation}
        isPayPalWebViewEnable={isPayPalWebViewEnable}
        isPayPalButtonRendered={isPayPalButtonRendered}
        isPayPalEnabled={isPayPalEnabled}
        hideHeader={hideHeader}
        addedToBagLoaderState={addedToBagLoaderState}
        addedToBagInterval={addedToBagInterval}
        totalBagItems={totalBagItems}
        pointsSummary={pointsSummary}
        bagLoading={bagLoading}
        isBossEnabled={isBossEnabled}
        isSuppressGetOrderDetails={isSuppressGetOrderDetails}
        isVenmoAppInstalled={isVenmoAppInstalled}
        isVenmoEnabled={isVenmoEnabled}
        isAvoidGetOrderDetails={isAvoidGetOrderDetails}
        enableKeepShoppingCTA={enableKeepShoppingCTA}
        multipackProductType={multipackProductType}
        isABTestOffStyleWith={isABTestOffStyleWith}
        isPhysicalMpackEnabled={isPhysicalMpackEnabled}
        addedToBagGhostLoaderState={addedToBagGhostLoaderState}
        addedToBagGhostLoaderCompleteState={addedToBagGhostLoaderCompleteState}
        isMpackItem={isMpackItem}
        isATBHidePoints={isATBHidePoints}
        getFavoriteStore={getFavoriteStore}
        isNewReDesignEnabled
        pdpLabels={pdpLabels}
        loyaltyAPICallStatus={loyaltyAPICallStatus}
        aTbAPICallStatus={aTbAPICallStatus}
        addToBagEcomNewAfterModalOpened={addToBagEcomNewAfterModalOpened}
        isPlcc={isPlcc}
        primaryBrand={addedToBagData.brand}
        alternateBrand={alternateBrand}
        itemInfo={itemInfo}
      />
    );
  }

  return (
    <Modal
      isOpen={openState}
      onRequestClose={onRequestClose}
      closeIconDataLocator="added-to-bg-close"
      animationType="none"
      headingAlign="left"
      heading={labels.addedToBag}
      headingFontFamily="secondary"
      headingFontWeight="semibold"
      horizontalBar={false}
      fontSize="fs16"
      aria={{
        labelledby: `${labels.addedToBag}`,
        describedby: `${labels.addedToBag}`,
      }}
      customTransparent
    >
      <StyledBodyWrapper>
        <TouchableOpacity
          accessibilityLabel={labels.overlayAriaText}
          accessibilityRole="none"
          onPress={onRequestClose}
          style={getContainerStyle(navigation)}
        >
          <TouchableWithoutFeedback accessibilityRole="none">
            <Text> </Text>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
        <StyledWrapper>
          {getRowWrapper(labels, onRequestClose, navigation, bagLoading)}
          {/* Below are place holders for   different data on added to Bag Modal. Replace <PlaceHolderView> with <View> and use your component within it. */}
          <AddedToBagWrapper payPalView={navigation.getParam('headerMode', false)}>
            {getProductsWrapper(
              addedToBagData,
              labels,
              quantity,
              pointsSummary,
              bagLoading,
              {
                isSuppressGetOrderDetails,
                isAvoidGetOrderDetails,
                isATBHidePoints,
              },
              alternateBrand
            )}
            {shouldRenderPoints(
              isSuppressGetOrderDetails,
              isAvoidGetOrderDetails,
              pointsSummary,
              isATBHidePoints
            ) && <AddedToBagViewPoints labels={labels} showPickupBanner={showPickupBanner} />}
            {renderPickupBanner(labels, addedToBagData, showPickupBanner, {
              labels,
              addedToBagData,
              showBOPISPickupInstead,
              getBopisInventoryDetails,
              bopisItemInventory,
              addItemToCartInPickup,
              addToPickupError,
              setAddToPickupError,
              userDefaultStore,
            })}
            {isATBHidePoints && renderLoyaltyBanner(navigation)}
            {renderBagPageActions(
              bagLoading,
              labels,
              navigation,
              onRequestClose,
              addedToBagData,
              isBossEnabled,
              isATBHidePoints
            )}
            <RecommendationWrapper>
              <Recommendations
                headerLabel={labels.youMayAlsoLike}
                navigation={navigation}
                priceOnly
                variation="moduleO"
                partNumber={partNumber}
                carouselConfigProps={carouselProps}
                page={Constants.RECOMMENDATIONS_PAGES_MAPPING.ADDED_TO_BAG}
                isAddedToBagOpen
                sequence="1"
                isAddedToBag
                showPeek={{ small: false, medium: false }}
                styleWithView={ismpackProductType}
                closeATBModal={onRequestClose}
                addedToBagData={addedToBagData}
                styleWithEligibility={ismpackProductType}
                portalValue={getPortalValue(ismpackProductType)}
                isSBP={isAddedFromSBP(addedToBagData)}
              />
            </RecommendationWrapper>
          </AddedToBagWrapper>
        </StyledWrapper>
      </StyledBodyWrapper>
      {renderLoader(openState, addedToBagGhostLoaderState)}
      {renderGhostOverlay(addedToBagGhostLoaderState, addedToBagGhostLoaderCompleteState)}
      {isVenmoEnabled && isVenmoAppInstalled && <VenmoPaymentButton hideVenmo />}
    </Modal>
  );
};

AddedToBag.propTypes = {
  openState: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  addedToBagData: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])).isRequired,
  addToBagModalError: PropTypes.string,
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])).isRequired,
  quantity: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}),
  pointsSummary: PropTypes.number.isRequired,
  bagLoading: PropTypes.bool,
  isBossEnabled: PropTypes.bool,
  isSuppressGetOrderDetails: PropTypes.bool,
  isVenmoAppInstalled: PropTypes.bool,
  isVenmoEnabled: PropTypes.bool,
  isAvoidGetOrderDetails: PropTypes.bool,
  isABTestOffStyleWith: PropTypes.bool,
  isPhysicalMpackEnabled: PropTypes.bool,
  addedToBagGhostLoaderState: PropTypes.bool,
  addedToBagGhostLoaderCompleteState: PropTypes.bool,
  getBopisInventoryDetails: PropTypes.func,
  userDefaultStore: PropTypes.shape({}),
  bopisItemInventory: PropTypes.shape({}),
  addItemToCartInPickup: PropTypes.func,
  addToPickupError: PropTypes.string,
  setAddToPickupError: PropTypes.func,
  showBOPISPickupInstead: PropTypes.bool,
  isATBHidePoints: PropTypes.bool,
  getFavoriteStore: PropTypes.func,
  hideHeader: PropTypes.bool,
  isNewReDesignEnabled: PropTypes.bool,
  handleContinueShopping: PropTypes.func,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
  router: PropTypes.shape({}),
  isInternationalShipping: PropTypes.bool,
  enableKeepShoppingCTA: PropTypes.bool,
  isPayPalButtonRendered: PropTypes.bool,
  isPayPalEnabled: PropTypes.bool,
  addedToBagLoaderState: PropTypes.bool,
  addedToBagInterval: PropTypes.func,
  totalBagItems: PropTypes.shape([]),
  multipackProductType: PropTypes.string,
  isPayPalWebViewEnable: PropTypes.bool,
  isMpackItem: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  loyaltyAPICallStatus: PropTypes.bool,
  aTbAPICallStatus: PropTypes.bool,
  addToBagEcomNewAfterModalOpened: PropTypes.func,
  openStatePayloadData: PropTypes.shape({}),
  isPlcc: PropTypes.bool,
};

AddedToBag.defaultProps = defaultProps;

export default gestureHandlerRootHOC(AddedToBag);
export { AddedToBag as AddedToBagVanilla };
