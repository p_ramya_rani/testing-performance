export const getPartNumber = (addedToBagData) => {
  let partNum;
  if (Array.isArray(addedToBagData)) {
    [partNum] = addedToBagData.map((item) => item && item.skuInfo && item.skuInfo.productId);
  } else {
    partNum = addedToBagData && addedToBagData.skuInfo && addedToBagData.skuInfo.productId;
  }
  return partNum;
};
export const getOfferPrice = (addedToBagData) => {
  let offerPrice;
  if (Array.isArray(addedToBagData)) {
    offerPrice += addedToBagData.map(
      (item) => item && item.skuInfo && parseFloat(item.skuInfo.offerPrice)
    );
  } else {
    offerPrice = addedToBagData && addedToBagData.skuInfo && addedToBagData.skuInfo.offerPrice;
  }
  return offerPrice;
};

export const getShowPickupBanner = (showBOPISPickupInstead, pickupType) =>
  showBOPISPickupInstead && !pickupType;

export const isAddedFromSBP = (addedToBagData) => addedToBagData && addedToBagData.addedFromSBP;

export const getMultiPackStyleType = (styleType) => {
  if (styleType === '0001') return 'Single';
  if (styleType === '0002') return 'Multipack';
  if (styleType === '0003') return 'Set';
  if (styleType === '0005') return 'Virtual Multipack';
  if (styleType === '0006') return 'Virtual Set';
  return '';
};
