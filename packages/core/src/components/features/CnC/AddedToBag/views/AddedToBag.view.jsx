// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import SpinnerOverlay from '@tcp/core/src/components/common/atoms/SpinnerOverlay';
import GhostOverlay from '@tcp/core/src/components/common/atoms/GhostOverlay';
import { MULTI_PACK_TYPE } from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import { capitalizeWords } from '@tcp/core/src/utils/utils';
import AddedToBagActions from '../../AddedToBagActions';
import AddedToBagViewPoints from '../../AddedToBagViewPoints';
import Modal from '../../../../common/molecules/Modal';
import withStyles from '../../../../common/hoc/withStyles';
import styles, {
  modalStyles,
  productInfoStyles,
  pointsInfoStyles,
  buttonActionStyles,
  LoyaltyWrapperStyles,
  recommendationStyles,
  customStyles,
} from '../styles/AddedToBag.style';
import ProductInformationView from '../molecules/ProductInformation/views/ProductInformation.views';
import BossBannerView from '../molecules/BossBanner/views/BossBanner.views';
import PickupBannerView from '../molecules/PickupBanner/views/PickupBanner.view';
import Anchor from '../../../../common/atoms/Anchor';
import LoyaltyBanner from '../../LoyaltyBanner';
import config from './config';
import { getPartNumber, getOfferPrice } from './AddedToBag.utils';
import ErrorMessage from '../../common/molecules/ErrorMessage';

const shouldRenderPointsView = (
  pointsSummary,
  isATBHidePoints,
  isOptimisticModalWithError,
  isOptmisticAddToBagModal
) => {
  if (isOptimisticModalWithError) {
    return false;
  }
  if (isOptmisticAddToBagModal) {
    return !isATBHidePoints;
  }
  return Object.keys(pointsSummary).length !== 0 && !isATBHidePoints;
};

class AddedToBag extends React.PureComponent {
  /* eslint-disable-next-line */
  UNSAFE_componentWillReceiveProps({ router: nextRouter }) {
    const { router, closeModal } = this.props;
    /* istanbul ignore else */
    if (router && nextRouter && router.asPath !== nextRouter.asPath) {
      closeModal();
    }
  }

  componentDidMount() {
    const { addedToBagData, userDefaultStore, getFavoriteStore, showBOPISPickupInstead } =
      this.props;
    const { pickupType } = addedToBagData || {};
    if (showBOPISPickupInstead && !pickupType && !userDefaultStore.basicInfo) {
      const latLong = readCookie('tcpGeoLoc');
      if (latLong) {
        const [lat, long] = latLong.split('|');
        getFavoriteStore({ geoLatLang: { lat, long }, isForceUpdate: true });
      }
    }
  }

  getmultipackProductType = () => {
    const { addedToBagData, isABTestOffStyleWith, isPhysicalMpackEnabled } = this.props;
    const TCPStyleType =
      addedToBagData && addedToBagData.skuInfo && addedToBagData.skuInfo.TCPStyleType;
    return (
      isPhysicalMpackEnabled && TCPStyleType === MULTI_PACK_TYPE.MULTIPACK && !isABTestOffStyleWith
    );
  };

  getVariations = () => {
    const ismultipackProductType = this.getmultipackProductType();
    return ismultipackProductType ? 'styleWith' : 'moduleO';
  };

  getHeaderLabel = () => {
    const { labels } = this.props;
    const ismultipackProductType = this.getmultipackProductType();
    return ismultipackProductType ? labels && labels.headingStyleWith : null;
  };

  getPortalValue = () => {
    const ismultipackProductType = this.getmultipackProductType();
    return ismultipackProductType ? Constants.RECOMMENDATIONS_MBOXNAMES.STYLE_WITH : '';
  };

  getStyleWrapperClass = () => {
    const ismultipackProductType = this.getmultipackProductType();
    return ismultipackProductType ? 'style-with-wrapper' : '';
  };

  getFbtWrapperClass = (isFbtCompleted) => {
    return isFbtCompleted ? 'fbt-complete' : '';
  };

  renderGhostOverlay = (
    addedToBagGhostLoaderState,
    addedToBagGhostLoaderCompleteState,
    isFBTDisplayed
  ) => {
    return (
      addedToBagGhostLoaderState &&
      !isFBTDisplayed && (
        <GhostOverlay loadingMsg="ADDED TO BAG" isComplete={addedToBagGhostLoaderCompleteState} />
      )
    );
  };

  renderSpinnerOverlay = (addedToBagLoaderState) => {
    return addedToBagLoaderState && <SpinnerOverlay inheritedStyles={customStyles} />;
  };

  getPartNumber = (addedToBagData) => {
    return addedToBagData && addedToBagData.skuInfo && addedToBagData.skuInfo.productId;
  };

  getModalHeading = (isOptimisticModalWithError) => {
    const { labels, isATBModalBackAbTestNewDesign } = this.props;

    let modalHeading = isOptimisticModalWithError ? '' : labels.addedToBagLabel;
    if (!isATBModalBackAbTestNewDesign) {
      modalHeading = capitalizeWords(modalHeading);
    }
    return modalHeading;
  };

  renderProductInfo() {
    const {
      addedToBagData,
      labels,
      quantity,
      pointsSummary,
      bagLoading,
      isSuppressGetOrderDetails,
      isInternationalShipping,
      isAvoidGetOrderDetails,
      isATBHidePoints,
      isATBModalBackAbTestNewDesign,
      primaryBrand,
      alternateBrand,
    } = this.props;
    if (Array.isArray(addedToBagData)) {
      const hasDoubleItems = addedToBagData.length > 1;
      return addedToBagData.map((item) => {
        const prodData = { ...item, imgConfig: ['t_t_atb_m', 't_t_atb_t', 't_t_atb_d'] };
        return (
          <div className="elem-mb-SM">
            <ProductInformationView
              data={prodData}
              labels={labels}
              inheritedStyles={productInfoStyles}
              isDoubleAddedToBag
              hasDoubleItems={hasDoubleItems}
              bagLoading={bagLoading}
              isSuppressGetOrderDetails={isSuppressGetOrderDetails}
              isInternationalShipping={isInternationalShipping}
              isAvoidGetOrderDetails={isAvoidGetOrderDetails}
              isATBHidePoints={isATBHidePoints}
              isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
              primaryBrand={primaryBrand}
              alternateBrand={alternateBrand}
            />
          </div>
        );
      });
    }
    const prodData = { ...addedToBagData, imgConfig: ['t_t_atb_m', 't_t_atb_t', 't_t_atb_d'] };
    return (
      <ProductInformationView
        data={{ ...prodData, ...pointsSummary }}
        labels={labels}
        quantity={quantity}
        inheritedStyles={productInfoStyles}
        isAvoidGetOrderDetails={isAvoidGetOrderDetails}
        isATBHidePoints={isATBHidePoints}
        isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
      />
    );
  }

  renderPickupBanner() {
    const {
      labels,
      addedToBagData,
      getBopisInventoryDetails,
      userDefaultStore,
      bopisItemInventory,
      addItemToCartInPickup,
      addToPickupError,
      setAddToPickupError,
      showBOPISPickupInstead,
      pageType,
      isATBModalBackAbTestNewDesign,
      alternateBrand,
      renderPickupBanner,
      isFBTDisplayed,
    } = this.props;
    const { pickupType, isOnlineExclusive } = addedToBagData;
    return showBOPISPickupInstead &&
      !pickupType &&
      !renderPickupBanner &&
      !isOnlineExclusive &&
      !isFBTDisplayed ? (
        <PickupBannerView
          labels={labels}
          addedToBagData={addedToBagData}
          getBopisInventoryDetails={getBopisInventoryDetails}
          pageType={pageType}
          userDefaultStore={userDefaultStore}
          bopisItemInventory={bopisItemInventory}
          addItemToCartInPickup={addItemToCartInPickup}
          addToPickupError={addToPickupError}
          setAddToPickupError={setAddToPickupError}
          isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
          alternateBrand={alternateBrand}
      />
    ) : null;
  }

  getFbtClass = (isFBTDisplayed) => (isFBTDisplayed ? 'fbt-wrapper' : '');

  // eslint-disable-next-line complexity
  render() {
    const {
      openState,
      onRequestClose,
      className,
      labels,
      handleContinueShopping,
      handleCartCheckout,
      isInternationalShipping,
      hideHeader,
      addedToBagLoaderState,
      isBossEnabled,
      addedToBagData,
      pointsSummary,
      enableKeepShoppingCTA,
      addedToBagGhostLoaderState,
      addedToBagGhostLoaderCompleteState,
      isATBHidePoints,
      addedToBagError,
      isOptmisticAddToBagModal,
      addToPickupError,
      isATBModalBackAbTestNewDesign,
      isFBTEnabled,
      isFBTDisplayed,
      isFbtCompleted,
    } = this.props;
    const partNumber = getPartNumber(addedToBagData);
    const carouselProps = {
      ...config.CAROUSEL_OPTIONS,
      touchThreshold: 500,
    };
    const ismultipackProductType = this.getmultipackProductType();
    const isOptimisticModalWithError =
      isOptmisticAddToBagModal && (addedToBagError || addToPickupError);
    const modalHeading = this.getModalHeading(isOptimisticModalWithError);
    const offerPrice = getOfferPrice(addedToBagData);

    return (
      openState && (
        <Modal
          fixedWidth
          isOpen={openState}
          onRequestClose={onRequestClose}
          heading={modalHeading}
          overlayClassName="TCPModal__Overlay"
          className={`TCPModal__Content, ${className}`}
          closeIconDataLocator="added-to-bg-close"
          aria={{
            labelledby: 'Added To Bag',
            describedby: 'Added To Bag Modal',
          }}
          data-locator="addedToBag-modal"
          inheritedStyles={modalStyles}
          innerContentClassName="atb-innerContent"
          isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
        >
          {isOptimisticModalWithError && (
            <div className="elem-mt-MED">
              <ErrorMessage
                className="error-notification"
                error={addedToBagError || addToPickupError}
                fontSize="fs20"
                fontWeight="extrabold"
                noBackground
              />
            </div>
          )}
          {this.renderSpinnerOverlay(addedToBagLoaderState)}
          {this.renderGhostOverlay(
            addedToBagGhostLoaderState,
            addedToBagGhostLoaderCompleteState,
            isFBTDisplayed
          )}
          <div
            className={`addedToBagWrapper ${
              addedToBagGhostLoaderState && !isFBTDisplayed ? 'blurred-content' : ''
            } ${isATBModalBackAbTestNewDesign ? '' : 'new-added-to-bag-modal'}`}
          >
            {this.renderProductInfo()}
            {shouldRenderPointsView(
              pointsSummary,
              isATBHidePoints,
              isOptimisticModalWithError,
              isOptmisticAddToBagModal
            ) && (
              <AddedToBagViewPoints
                labels={labels}
                className="added-to-bag-points"
                inheritedStyles={pointsInfoStyles}
                isOptmisticAddToBagModal={isOptmisticAddToBagModal}
                offerPrice={offerPrice}
                isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
              />
            )}
            {this.renderPickupBanner()}
            {!isOptimisticModalWithError &&
              isATBHidePoints &&
              isATBModalBackAbTestNewDesign &&
              !isFBTDisplayed && (
                <div className="loyaltyAddedToBagWrapper">
                  <LoyaltyBanner
                    pageCategory="isAddedToBagPage"
                    inheritedStyles={LoyaltyWrapperStyles}
                    aboveCta
                    isOptmisticAddToBagModal={isOptmisticAddToBagModal}
                  />
                </div>
              )}
            <AddedToBagActions
              labels={labels}
              onRequestClose={onRequestClose}
              handleCartCheckout={handleCartCheckout}
              showVenmo={false}
              containerId="paypal-button-container-added-to-bag"
              inheritedStyles={buttonActionStyles}
              hideHeader={hideHeader}
              handleContinueShopping={handleContinueShopping}
              enableKeepShoppingCTA={enableKeepShoppingCTA}
              isAddedToBag
              isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
            />

            {!isOptimisticModalWithError &&
              !isATBHidePoints &&
              isATBModalBackAbTestNewDesign &&
              !isFBTDisplayed && (
                <div className="loyaltyAddedToBagWrapper">
                  <LoyaltyBanner
                    pageCategory="isAddedToBagPage"
                    inheritedStyles={LoyaltyWrapperStyles}
                    isOptmisticAddToBagModal={isOptmisticAddToBagModal}
                  />
                </div>
              )}
            {!isInternationalShipping && isBossEnabled && !isFBTDisplayed && (
              <BossBannerView labels={labels} />
            )}
            {isFBTEnabled && (
              <div
                className={`recommendationWrapper ${this.getFbtClass(
                  isFBTDisplayed
                )}   ${this.getFbtWrapperClass(isFbtCompleted)}`}
              >
                <Recommendations
                  page={Constants.RECOMMENDATIONS_PAGES_MAPPING.ADDED_TO_BAG}
                  variations={this.getVariations()}
                  styleHeaderLabel={this.getHeaderLabel()}
                  headerLabel={labels.headingFbt}
                  priceOnly
                  inheritedStyles={recommendationStyles}
                  carouselConfigProps={carouselProps}
                  sequence="1"
                  isAddedToBag
                  starRatingSize={{ small: 63, large: 60 }}
                  partNumber={partNumber}
                  showPeek={{ small: false, medium: false }}
                  styleWithView={ismultipackProductType && !isFBTEnabled}
                  addedToBagData={addedToBagData}
                  styleWithEligibility={ismultipackProductType}
                  portalValue="fbt"
                  isFBTEnabled={isFBTEnabled}
                  isOptimisticModalWithError={isOptimisticModalWithError}
                  isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
                  addedToBagGhostLoaderState={addedToBagGhostLoaderState}
                  isComplete={addedToBagGhostLoaderCompleteState}
                  isFBTDisplayed={isFBTDisplayed}
                />
              </div>
            )}

            <div className="recommendationWrapper">
              <Recommendations
                page={Constants.RECOMMENDATIONS_PAGES_MAPPING.ADDED_TO_BAG}
                variations={this.getVariations()}
                priceOnly
                inheritedStyles={recommendationStyles}
                carouselConfigProps={carouselProps}
                sequence="1"
                isAddedToBag
                starRatingSize={{ small: 63, large: 60 }}
                partNumber={partNumber}
                showPeek={{ small: false, medium: false }}
                addedToBagData={addedToBagData}
                styleWithEligibility={ismultipackProductType}
                portalValue=""
                isFBTEnabled={isFBTEnabled}
                isOptimisticModalWithError={isOptimisticModalWithError}
              />
            </div>

            {!enableKeepShoppingCTA && isATBModalBackAbTestNewDesign && (
              <div className="continue-shopping">
                <Anchor
                  fontSizeVariation="medium"
                  underline
                  anchorVariation="primary"
                  handleLinkClick={handleContinueShopping}
                  noLink
                  to=""
                  dataLocator="addedToBag-continueShopping"
                >
                  {labels.continueShopping}
                </Anchor>
              </div>
            )}
          </div>
        </Modal>
      )
    );
  }
}

AddedToBag.propTypes = {
  openState: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  addedToBagData: PropTypes.shape({}).isRequired,
  router: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  quantity: PropTypes.number.isRequired,
  handleContinueShopping: PropTypes.func.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  hideHeader: PropTypes.bool.isRequired,
  handleCartCheckout: PropTypes.func.isRequired,
  addedToBagLoaderState: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  pointsSummary: PropTypes.shape({}).isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isBossEnabled: PropTypes.bool.isRequired,
  isSuppressGetOrderDetails: PropTypes.bool,
  isAvoidGetOrderDetails: PropTypes.bool,
  enableKeepShoppingCTA: PropTypes.bool,
  isABTestOffStyleWith: PropTypes.bool,
  isPhysicalMpackEnabled: PropTypes.bool,
  addedToBagGhostLoaderState: PropTypes.bool,
  addedToBagGhostLoaderCompleteState: PropTypes.bool,
  getBopisInventoryDetails: PropTypes.func,
  userDefaultStore: PropTypes.shape({}),
  bopisItemInventory: PropTypes.shape({}),
  pageType: PropTypes.shape({}),
  addItemToCartInPickup: PropTypes.func,
  addToPickupError: PropTypes.string,
  setAddToPickupError: PropTypes.func,
  showBOPISPickupInstead: PropTypes.bool,
  isATBHidePoints: PropTypes.bool,
  getFavoriteStore: PropTypes.func,
  addedToBagError: PropTypes.string,
  isOptmisticAddToBagModal: PropTypes.bool,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
};

AddedToBag.defaultProps = {
  isSuppressGetOrderDetails: false,
  isAvoidGetOrderDetails: false,
  enableKeepShoppingCTA: false,
  isABTestOffStyleWith: false,
  isPhysicalMpackEnabled: false,
  addedToBagGhostLoaderState: false,
  addedToBagGhostLoaderCompleteState: false,
  getBopisInventoryDetails: () => {},
  userDefaultStore: {},
  bopisItemInventory: {},
  pageType: {},
  addItemToCartInPickup: () => {},
  addToPickupError: '',
  setAddToPickupError: () => {},
  showBOPISPickupInstead: false,
  isATBHidePoints: false,
  getFavoriteStore: () => {},
  addedToBagError: '',
  isOptmisticAddToBagModal: true,
  isATBModalBackAbTestNewDesign: false,
};

export default withStyles(AddedToBag, styles);
export { AddedToBag as AddedToBagVanilla };
