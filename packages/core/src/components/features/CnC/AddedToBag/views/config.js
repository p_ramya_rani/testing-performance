// 9fbef606107a605d69c0edbcd8029e5d
import { breakpoints } from '@tcp/core/styles/themes/TCP/mediaQuery';

export const defaultProps = {
  navigation: null,
  isBossEnabled: true,
  bagLoading: true,
  isSuppressGetOrderDetails: false,
  isVenmoAppInstalled: true,
  isVenmoEnabled: false,
  isAvoidGetOrderDetails: false,
  isABTestOffStyleWith: false,
  isPhysicalMpackEnabled: false,
  addedToBagGhostLoaderState: false,
  addedToBagGhostLoaderCompleteState: false,
  getBopisInventoryDetails: () => {},
  userDefaultStore: {},
  bopisItemInventory: {},
  addItemToCartInPickup: () => {},
  addToPickupError: '',
  setAddToPickupError: () => {},
  showBOPISPickupInstead: false,
  isATBHidePoints: false,
  getFavoriteStore: () => {},
  isNewReDesignEnabled: false,
  isATBModalBackAbTestNewDesign: false,
  hideHeader: false,
  handleContinueShopping: () => {},
  router: {},
  isInternationalShipping: false,
  enableKeepShoppingCTA: false,
  isPayPalButtonRendered: false,
  isPayPalEnabled: false,
  addedToBagLoaderState: false,
  addedToBagInterval: () => {},
  totalBagItems: [],
  multipackProductType: '',
  isPayPalWebViewEnable: false,
  addToBagModalError: null,
  loyaltyAPICallStatus: false,
  aTbAPICallStatus: false,
  addToBagEcomNewAfterModalOpened: () => {},
  openStatePayloadData: null,
  isPlcc: false,
};

export default {
  CAROUSEL_OPTIONS: {
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: breakpoints.values.lg - 1,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
          arrows: false,
        },
      },
      {
        breakpoint: breakpoints.values.sm - 1,
        settings: {
          arrows: false,
          slidesToShow: 3,
          slidesToScroll: 2,
        },
      },
    ],
  },
};
