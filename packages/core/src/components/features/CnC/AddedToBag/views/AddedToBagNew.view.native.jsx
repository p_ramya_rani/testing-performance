/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { Component } from 'react';
import { View } from 'react-native';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import PropTypes from 'prop-types';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import { MULTI_PACK_TYPE } from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import ModalBox from 'react-native-modalbox';
import isEqual from 'lodash/isEqual';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import RelatedOutfits from '../../../browse/ProductDetail/molecules/RelatedOutfits/views/RelatedOutfits.view.native';
import BodyCopy from '../../../../common/atoms/BodyCopy';

import {
  StyledWrapper,
  AddedToBagWrapper,
  StyledBodyWrapper,
  RecommendationWrapper,
  StickyButtonContainer,
  CompleteTheLookWrapper,
} from '../styles/AddedToBag.style.native';
import ProductInformation from '../molecules/ProductInformation/views/ProductInformation.views.native';
import config, { defaultProps } from './config';
import { isAddedFromSBP } from './AddedToBag.utils';
import {
  ModalBoxBtn,
  ModalBoxCloseImage,
} from '../../../browse/ProductDetail/styles/ProductDetail.style.native';
import AddedToBagActions from '../../AddedToBagActions';
import { isAndroid } from '../../../../../utils/utils.app';

const closeImage = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/close_icon_drawer.png');

const getProductsWrapper = (arg) => {
  const {
    openStatePayloadData,
    labels,
    isNewReDesignEnabled,
    addedToBagData,
    addToBagModalError,
    loyaltyAPICallStatus,
    aTbAPICallStatus,
    isPlcc,
    primaryBrand,
    alternateBrand,
    pointsSummary,
    itemInfo,
  } = arg;
  if (Array.isArray(openStatePayloadData)) {
    const hasDoubleItems = openStatePayloadData.length > 1;
    return openStatePayloadData.map((item) => {
      const prodDataATB = { ...item, imgConfig: 't_t_atb_m' };
      return (
        <ProductInformation
          data={prodDataATB}
          labels={labels}
          isDoubleAddedToBag
          hasDoubleItems={hasDoubleItems}
          addToBagModalError={addToBagModalError}
          isPlcc={isPlcc}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
          itemInfo={itemInfo}
        />
      );
    });
  }
  const prodData = { ...openStatePayloadData, imgConfig: 't_t_atb_m' };
  return (
    <ProductInformation
      data={{ ...prodData, ...pointsSummary }}
      dataFromApi={addedToBagData}
      labels={labels}
      isNewReDesignEnabled={isNewReDesignEnabled}
      addToBagModalError={addToBagModalError}
      loyaltyAPICallStatus={loyaltyAPICallStatus}
      aTbAPICallStatus={aTbAPICallStatus}
      isPlcc={isPlcc}
      primaryBrand={primaryBrand}
      alternateBrand={alternateBrand}
      itemInfo={itemInfo}
    />
  );
};

export const getmultipackProductType = (
  addedToBagData,
  isABTestOffStyleWith,
  isPhysicalMpackEnabled
) => {
  const TCPStyleType =
    addedToBagData && addedToBagData.skuInfo && addedToBagData.skuInfo.TCPStyleType;
  return (
    isPhysicalMpackEnabled && TCPStyleType === MULTI_PACK_TYPE.MULTIPACK && !isABTestOffStyleWith
  );
};

const getPortalValue = (ismpackProductType) =>
  ismpackProductType
    ? Constants.RECOMMENDATIONS_MBOXNAMES.STYLE_WITH
    : Constants.RECOMMENDATIONS_MBOXNAMES.ADD_TO_BAG_MODAL_CAROUSAL;

const renderRelatedOutfits = (args) => {
  const { navigation, partNumber, pdpLabels, openState, onRequestClose, addToBagModalError } = args;
  if (addToBagModalError) return null;
  if (openState.isPdpPage) {
    return (
      <CompleteTheLookWrapper>
        <RelatedOutfits
          paddings="0 10px 0 10px"
          margins="8px 0 0 0"
          background="#ffffff"
          pdpLabels={pdpLabels}
          navigation={navigation}
          selectedColorProductId={partNumber}
          showSmallImage
          setShowCompleteTheLook={() => {}}
          isNewReDesignCTL
          fromPage={Constants.RECOMMENDATIONS_PAGES_MAPPING.ADDED_TO_BAG}
          showCompleteTheLookHeader
          onRequestClose={onRequestClose}
        />
      </CompleteTheLookWrapper>
    );
  }
  return null;
};

const renderBagPageActions = (args) => {
  const {
    labels,
    navigation,
    onRequestClose,
    addedToBagData,
    isNewReDesignEnabled,
    addToBagModalError,
  } = args;
  return (
    <>
      <AddedToBagActions
        labels={labels}
        navigation={navigation}
        closeModal={onRequestClose}
        showAddTobag
        fromAddedToBagModal
        hideHeader={(hide) => {
          navigation.setParams({ headerMode: hide });
        }}
        addedFromSBP={isAddedFromSBP(addedToBagData)}
        isNewRedesignedModal={isNewReDesignEnabled}
        addToBagModalError={addToBagModalError}
      />
    </>
  );
};

class AddedToBagNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      atbModalVisible: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (!isEqual(prevProps, this.props)) {
      this.showModalATB();
    }
  }

  showModalATB = () => {
    const { openState } = this.props;
    this.setState({ atbModalVisible: !!openState });
  };

  closeModalATB = (onRequestClose) => {
    this.setState({ atbModalVisible: false });
    onRequestClose();
  };

  renderHeader = (
    onRequestClose,
    addToBagModalError,
    labels,
    successCount,
    successQuantity,
    multiPill
  ) => {
    const viewStyle = {
      marginTop: 16,
      paddingTop: 16,
    };
    if (multiPill && successCount === 0) {
      return (
        <ModalBoxBtn
          accessibilityRole="button"
          onPress={() => this.closeModalATB(onRequestClose)}
          isPdpNewReDesign
        >
          <ModalBoxCloseImage source={closeImage} />
        </ModalBoxBtn>
      );
    }
    return (
      <>
        <ModalBoxBtn
          accessibilityRole="button"
          onPress={() => this.closeModalATB(onRequestClose)}
          isPdpNewReDesign
        >
          <ModalBoxCloseImage source={closeImage} />
        </ModalBoxBtn>
        <View style={viewStyle}>
          {addToBagModalError ? (
            <BodyCopy
              fontSize="fs20"
              fontWeight="bold"
              textAlign="center"
              text={`${labels.oopsItemUnavailable}`}
              fontFamily="secondary"
              color="red.500"
            />
          ) : (
            <BodyCopy
              fontSize="fs20"
              fontWeight="bold"
              textAlign="center"
              text={
                multiPill && successCount
                  ? `${successQuantity} ${
                      successQuantity > 1 ? labels.itemsAddedToBag : labels.itemAddedToBag
                    }`
                  : `${labels.itemAddedToBag}`
              }
              fontFamily="secondary"
              color="gray.900"
            />
          )}
        </View>
      </>
    );
  };

  render() {
    const {
      openState,
      onRequestClose,
      addedToBagData,
      labels,
      navigation,
      isNewReDesignEnabled,
      pdpLabels,
      isBossEnabled,
      bagLoading,
      isATBHidePoints,
      isABTestOffStyleWith,
      isPhysicalMpackEnabled,
      addToBagModalError,
      loyaltyAPICallStatus,
      aTbAPICallStatus,
      openStatePayloadData,
      isPlcc,
      primaryBrand,
      alternateBrand,
      pointsSummary,
      itemInfo,
    } = this.props;
    const modalStyle = {
      maxHeight: '88%',
      borderTopRightRadius: 16,
      borderTopLeftRadius: 16,
      backgroundColor: '#f3f3f3',
    };

    const partNumber =
      openStatePayloadData &&
      openStatePayloadData.skuInfo &&
      openStatePayloadData.skuInfo.unbxdProdId;

    const carouselProps = {
      ...config.CAROUSEL_OPTIONS,
      touchThreshold: 500,
    };

    const ismpackProductType = getmultipackProductType(
      addedToBagData,
      isABTestOffStyleWith,
      isPhysicalMpackEnabled
    );

    const label = addToBagModalError
      ? pdpLabels.weFoundTheseSimilarItems
      : pdpLabels.youMayAlsoLike;

    const { atbModalVisible } = this.state;

    const successProducts = Array.isArray(itemInfo) ? itemInfo.filter((item) => !item.error) : [];

    const successQuantity =
      Array.isArray(successProducts) && successProducts.length
        ? successProducts.reduce((accumulator, currentValue) => {
            return accumulator + currentValue.quantity;
          }, 0)
        : 0;
    const multiPill = addedToBagData && Array.isArray(addedToBagData) && addedToBagData.length;

    return (
      <ModalBox
        animationDuration={100}
        style={modalStyle}
        position="bottom"
        isOpen={atbModalVisible}
        entry="bottom"
        swipeToClose={false}
        onClosed={() => this.closeModalATB(onRequestClose)}
        coverScreen
        useNativeDriver
        swipeThreshold={10}
      >
        {this.renderHeader(
          onRequestClose,
          addToBagModalError,
          labels,
          successProducts.length,
          successQuantity,
          multiPill
        )}
        <StyledBodyWrapper>
          <StyledWrapper isNewReDesign={isNewReDesignEnabled}>
            {/* Below are place holders for   different data on added to Bag Modal. Replace <PlaceHolderView> with <View> and use your component within it. */}
            <AddedToBagWrapper
              payPalView={navigation.getParam('headerMode', false)}
              isNewReDesignEnabled={isNewReDesignEnabled}
            >
              {(openStatePayloadData || addedToBagData.size !== 0) &&
                getProductsWrapper({
                  openStatePayloadData,
                  labels,
                  isNewReDesignEnabled,
                  addedToBagData,
                  addToBagModalError,
                  loyaltyAPICallStatus,
                  aTbAPICallStatus,
                  isPlcc,
                  primaryBrand,
                  alternateBrand,
                  pointsSummary,
                  itemInfo,
                })}
              {renderRelatedOutfits({
                navigation,
                partNumber,
                pdpLabels,
                openState,
                onRequestClose,
                addToBagModalError,
              })}
              {partNumber && (
                <RecommendationWrapper isNewReDesignEnabled={isNewReDesignEnabled}>
                  <Recommendations
                    headerLabel={label}
                    navigation={navigation}
                    portalValue={getPortalValue(ismpackProductType)}
                    page={Constants.RECOMMENDATIONS_PAGES_MAPPING.ADDED_TO_BAG}
                    priceOnly
                    variation="moduleO"
                    partNumber={partNumber}
                    carouselConfigProps={carouselProps}
                    isAddedToBagOpen
                    sequence="1"
                    isAddedToBag
                    isCardTypeTiles
                    showPeek={{ small: false, medium: false }}
                    productImageWidth={130}
                    productImageHeight={161}
                    paddings="0px"
                    margins="5px 6px 18px"
                    styleWithView={ismpackProductType}
                    closeATBModal={onRequestClose}
                    addedToBagData={addedToBagData}
                    styleWithEligibility={ismpackProductType}
                    isSBP={isAddedFromSBP(addedToBagData)}
                  />
                </RecommendationWrapper>
              )}
            </AddedToBagWrapper>
          </StyledWrapper>
        </StyledBodyWrapper>
        <StickyButtonContainer isAndroid={isAndroid()}>
          {renderBagPageActions({
            bagLoading,
            labels,
            navigation,
            onRequestClose,
            addedToBagData,
            isBossEnabled,
            isATBHidePoints,
            isNewReDesignEnabled,
            addToBagModalError,
          })}
        </StickyButtonContainer>
      </ModalBox>
    );
  }
}

AddedToBagNew.propTypes = {
  openState: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  addedToBagData: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])).isRequired,
  addToBagModalError: PropTypes.string,
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])).isRequired,
  navigation: PropTypes.shape({}),
  isNewReDesignEnabled: PropTypes.bool,
  pdpLabels: PropTypes.shape({}).isRequired,
  isBossEnabled: PropTypes.bool,
  bagLoading: PropTypes.bool,
  isATBHidePoints: PropTypes.bool,
  isABTestOffStyleWith: PropTypes.bool,
  isPhysicalMpackEnabled: PropTypes.bool,
  loyaltyAPICallStatus: PropTypes.bool,
  aTbAPICallStatus: PropTypes.bool,
  openStatePayloadData: PropTypes.shape({}),
  isPlcc: PropTypes.bool,
};

AddedToBagNew.defaultProps = defaultProps;

export default gestureHandlerRootHOC(AddedToBagNew);
export { AddedToBagNew as AddedToBagVanilla };
