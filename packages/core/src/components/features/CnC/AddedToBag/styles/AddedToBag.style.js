// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export const modalStyles = css`
  div.TCPModal__InnerContent.atb-innerContent {
    right: 0;
    left: auto;
    top: 0;
    bottom: 0;
    transform: none;
    box-shadow: 0 4px 8px 0 rgba(163, 162, 162, 0.5);
    ${(props) =>
      props.isATBModalBackAbTestNewDesign
        ? `
    padding: 7px 0 20px 17px;
    width: 350px;`
        : `
    padding: 0;
    width: 100%;
    @media ${props.theme.mediaQuery.smallOnly} {
      animation: slide-in-mobile 0.5s forwards;
      border-radius: ${props.theme.spacing.ELEM_SPACING.XS}
        ${props.theme.spacing.ELEM_SPACING.XS} 0px 0px;
    }
    @keyframes slide-in-mobile {
      0% {
        transform: translateY(100%);
      }
      100% {
        transform: translateY(5%);
      }
    }
    `}
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 350px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 375px;
    }

    .Modal-Header {
      ${(props) =>
        props.isATBModalBackAbTestNewDesign
          ? ``
          : `
      background-color: ${props.theme.colorPalette.gray[300]};
      `}
    }

    .Modal_Heading {
      font-size: 16px;
      font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
      font-weight: ${(props) => props.theme.fonts.fontWeight.semibold};
      line-height: 43px;
      border: none;
      margin-bottom: 0;
      padding: 0;
      display: inline;
      ${(props) =>
        props.isATBModalBackAbTestNewDesign
          ? ``
          : `
      line-height: 46px;
      margin-left: 20px;
      @media ${props.theme.mediaQuery.medium} {
        margin-left: 40px;
      }
      `}
    }

    .close-modal {
      right: ${(props) =>
        props.isATBModalBackAbTestNewDesign ? props.theme.spacing.ELEM_SPACING.SM : '18px'};
      top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
    .blurred-content {
      filter: blur(4px);
      -o-filter: blur(4px);
      -ms-filter: blur(4px);
      -moz-filter: blur(4px);
      -webkit-filter: blur(4px);
    }

    .fbt-wrapper {
      overflow: hidden;
      opacity: 1;
    }

    .fbt-wrapper > div {
      margin-top: 0;
    }

    .fbt-complete {
      min-height: 0;
      height: 0;
      transition: height 1.2s ease;
      overflow: hidden;
      opacity: 0;
    }
  }
`;

export const productInfoStyles = css`
  margin-bottom: 10px;
  .product {
    margin: 0;
    width: 100%;
  }
`;

export const pointsInfoStyles = css`
  .row-padding {
    margin: 0;
    width: 100%;
  }
  .divided-line {
    margin-left: 0;
    margin-right: 0;
    width: 100%;
  }
`;

export const buttonActionStyles = css`
  .view-bag-container {
    margin: 0;
    width: 100%;
  }
  .checkout-button {
    margin: 0;
    width: 100%;
  }
`;

export const LoyaltyWrapperStyles = css`
  .loyalty-banner-wrapper {
    padding: 0;
    margin-bottom: ${(props) => (props.aboveCta ? `12px` : 0)};
  }
`;

export const recommendationStyles = css`
  .recommendations-header {
    width: auto;
    font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: ${(props) => (props.isATBModalBackAbTestNewDesign ? '600' : '800')};
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: ${(props) => props.theme.colors.BLACK};
    margin: 0px auto 0px 7px;
    padding-top: ${(props) => (props.isATBModalBackAbTestNewDesign ? '10px' : '0')};
  }
  .item-container-inner {
    box-sizing: border-box;
    width: ${(props) => (props.isATBModalBackAbTestNewDesign ? '104px' : '125px')};

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => (props.isATBModalBackAbTestNewDesign ? '0 0 5px 5px' : '0 8px 4px')};
    }

    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: ${(props) => (props.isATBModalBackAbTestNewDesign ? '0 0 5px 25px' : '0 8px 4px')};
    }

    @media ${(props) => props.theme.mediaQuery.large} {
      width: 104px;
      padding: ${(props) =>
        props.isATBModalBackAbTestNewDesign
          ? `5px ${props.theme.spacing.ELEM_SPACING.XS_6}`
          : '0 8px 4px'};
    }

    ${(props) =>
      props.isATBModalBackAbTestNewDesign
        ? ''
        : `
      border: solid 1px ${props.theme.colorPalette.gray[500]};
      padding: 0 8px 4px;
      margin-right: 8px;
      border-radius: 6px;
      height: 100%;
      overflow: hidden;
    `}
  }

  .custom-ranking-container img {
    width: 71px;
    height: 14px;
  }

  .custom-ranking-container .reviews-count {
    margin-top: 0px;
    font-size: 10px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: 3px;
      font-size: 10px;
    }
  }

  .ranking-wrapper {
    white-space: pre;
    word-wrap: normal;
  }

  .product-list {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      max-width: ${(props) => (props.isATBModalBackAbTestNewDesign ? '120px' : '133px')};
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .recommendations-container {
      margin-left: -5px;
    }
    .product-image-container {
      height: 150px !important;
    }
    .smooth-scroll-list-item {
      margin-right: 0px;
    }
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .product-image-container {
      height: auto;
    }
    .smooth-scroll-list-item {
      margin-right: 0px;
    }
  }

  .container-price > p {
    font-size: ${(props) => props.theme.typography.fontSizes.fs15};
    width: 40px;
    height: 20px;
    margin: 0 2px -3px 0;
    font-family: Nunito;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
  }
  .container-price > span {
    font-size: ${(props) => props.theme.typography.fontSizes.fs10};
  }
  && .custom-ranking-container img {
    width: 60px;
    height: 12px;
    min-width: 60px;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 63px;
      height: 13px;
      min-width: 63px;
    }
  }
  && .rating-star-grey {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: 2px;
    }
  }
  && .product-image-container > a {
    min-height: 130px;
  }
  && .slick-list {
    margin-right: -20%;
    margin-left: -8%;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: -8%;
    }
  }
  && .recommendations-tile .slick-arrow {
    top: 35%;
  }
  && .slick-prev {
    top: ${(props) => (props.isATBModalBackAbTestNewDesign ? '18%' : '28%')};
    margin-left: 15%;

    background-image: none;
    width: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.SM};
    height: 72px;
    opacity: 0.79;
    padding: 27px 5px 27px 7px;
    background-color: ${(props) => props.theme.colors.WHITE};
    border-top-right-radius: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    border-bottom-right-radius: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  }
  && .slick-prev::after {
    content: '';
    border: solid black;
    border-width: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
      ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 0;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    transform: rotate(135deg);
    margin: 4px 0;
  }
  && .slick-next {
    top: ${(props) => (props.isATBModalBackAbTestNewDesign ? '18%' : '28%')};
    margin-right: 15%;

    background-image: none;
    width: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.SM};
    height: 72px;
    opacity: 0.79;
    padding: 27px 5px 27px 7px;
    background-color: ${(props) => props.theme.colors.WHITE};
    border-top-left-radius: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    border-bottom-left-radius: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  }
  && .slick-next::after {
    content: '';
    border: solid black;
    border-width: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
      ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 0;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    transform: rotate(-45deg);
    background: none;
    margin: 4px 0;
  }
  && .product-list {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin: ${(props) =>
        props.isATBModalBackAbTestNewDesign ? `0 ${props.theme.spacing.ELEM_SPACING.SM}` : '0'};
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin: ${(props) =>
        props.isATBModalBackAbTestNewDesign ? `0 ${props.theme.spacing.ELEM_SPACING.SM}` : '0'};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
`;

const styles = css`
  .added-to-bg-close {
    top: 21px;
  }
  .addedToBagWrapper {
    overflow-y: auto;
    overflow-x: hidden;
    height: calc(100% - 43px);
    padding-right: 15px;
    margin-left: -17px;
    padding-left: 17px;
  }
  .continue-shopping {
    width: 343px;
    height: 16px;
    margin: 0 0 30px 1px;
    font-family: Nunito;
    font-size: 12px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: ${(props) => props.theme.colors.BLACK};
  }
  .recommendationWrapper {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 0px;
    }
  }
  .loyaltyAddedToBagWrapper {
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .style-with-wrapper {
    position: relative;
    padding-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.LRG};
  }
  .new-added-to-bag-modal {
    margin: 0;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    height: auto;
  }
`;

export const customStyles = css`
  .spinner-overlay {
    position: absolute;
  }
`;

export default styles;
