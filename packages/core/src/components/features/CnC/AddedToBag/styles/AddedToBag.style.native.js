// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export const StyledAnchorWrapper = styled.View`
  text-align: center;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  flex: 1;
`;

export const PlaceHolderView = styled.View`
  height: 150px;
  border: 1px solid black;
  margin: 10px;
  text-align: center;
`;

export const StyledText = styled.Text`
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const AddedToBagWrapper = styled.View`
  display: flex;
  flex: 1;
  ${(props) => {
    if (props.payPalView) {
      return `
      margin-top: 0;
      padding: 0;
      `;
    }
    return `margin-top: ${props.theme.spacing.ELEM_SPACING.LRG};
      padding: 0 ${props.theme.spacing.ELEM_SPACING.XS};`;
  }}
  ${(props) => (props.isNewReDesignEnabled ? `align-items: center` : ``)}
`;

export const ButtonsLoaderWrapper = styled.View`
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED}
    ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const BannerLoaderWrapper = styled.View`
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const StyledBodyWrapper = styled.View`
  display: flex;
  flex-direction: row;
  flex: 1;
`;

export const StyledWrapper = styled.ScrollView`
  height: 100%;
  ${(props) => (props.isNewReDesign ? '' : `background-color: ${props.theme.colors.WHITE}`)};
  flex: 1;
`;

export const RowWrapper = styled.SafeAreaView`
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED}
    ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0
    ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ImageWrapper = styled.View`
  margin-bottom: 12px;
`;

export const StyledHeader = styled.Text`
  padding: 0px 0px ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  justify-content: center;
  align-content: center;
`;

export const StyledTouchableOpacity = styled.TouchableOpacity`
  padding: 12px 12px 12px 12px;
`;

export const StyledCrossImage = styled.Image`
  width: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const ModalHeading = styled.Text`
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  width: 80%;
`;

export const RecommendationWrapper = styled.View`
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0 0 0;
  ${(props) =>
    props.isNewReDesignEnabled
      ? `margin-bottom: 60px; margin-left: ${props.theme.spacing.ELEM_SPACING.XS_6}`
      : ``}
`;

export const LoyaltyBannerWrapper = styled.View`
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const StickyButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  border-top-width: 1;
  width: 100%;
  border-radius: 0;
  z-index: 100;
  padding: 10px 18px;
  justify-content: center;
  background-color: ${(props) => props.theme.colors.WHITE};
  border-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
  ${(props) =>
    props.isAndroid
      ? `
  height: 70;
  padding-bottom: 15;
  `
      : `
  height: 80;
  padding-bottom: 25;
  `}
`;

export const CompleteTheLookWrapper = styled.View`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;
