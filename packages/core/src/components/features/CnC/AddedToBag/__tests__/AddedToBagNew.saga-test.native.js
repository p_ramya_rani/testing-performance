/* eslint-disable  max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { put, call, takeLatest, select } from 'redux-saga/effects';
import { Platform } from 'react-native';
import {
  setLoyaltyApiCallStatus,
  setATBApiCallStatus,
  setLoaderState,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { getIsGuest } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { navigateXHRAction } from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.action';
import { getPageData, getClickAnalyticsData } from '@tcp/core/src/analytics/Analytics.selectors';
import { MULTI_PACK_TYPE } from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import { getLoyaltyDetail } from '@tcp/core/src/components/features/CnC/LoyaltyBanner/container/Loyalty.actions';
import { trackClickWithData } from '@tcp/core/src/analytics/actions';
import {
  addToCartEcomNewLoadTheATB,
  addToCartEcomNewAfterAtbLoaded,
  AddedToBagNewSaga,
  filterMultiPackVariant,
  makeNavigateXHrCall,
  filterItems,
  isLoyaltyServiceEnabled,
  getAllCatId,
  getMpackTgas,
  setATBDataAndHapticFeedback,
  callGetLoyaltyPoints,
  triggerNavigateXHrCall,
  setMultipackData,
  clearATBErrors,
  addToCartUpdateLocalData,
  logAddToBagEvent,
  addSingleItemToCartECOM,
  addMultipleItemToCartECOM,
} from '../container/AddedToBagNew.saga.native';
import {
  openAddedToBagMinimal,
  addToBagModalError,
  SetAddedToBagData,
  setMultipackProductType,
  setIsMultipackItem,
  clearAddToBagModalError,
} from '../container/AddedToBag.actions';
import ADDEDTOBAG_CONSTANTS from '../AddedToBag.constants';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';

jest.mock('../util/utility', () => {
  return {
    makeBrandToggling: () => false,
    logCustomEvent: () => {},
  };
});

jest.mock('@tcp/core/src/utils', () => {
  const originalModule = jest.requireActual('@tcp/core/src/utils');
  return {
    ...originalModule,
    isMobileApp: jest.fn(),
  };
});

const payload = {
  skuInfo: {
    TCPMultipackProductMapping: ['1'],
    TCPStyleQTY: '1',
    TCPStyleType: '0001',
    color: {
      name: 'DRY INDIGO',
      imagePath: null,
      family: 'DENIM',
      swatchImage: '3015475_528_swatch.jpg',
    },
    fit: 'slim',
    imageUrl: '3015475/3015475_528.jpg',
    productFamily: undefined,
    productId: '3015475_528',
    size: '5S',
    skuId: '1644027',
    unbxdProdId: '3015475_528',
    variantId: '00194936074874',
    variantNo: '3015476002',
  },
  quantity: 1,
  wishlistItemId: '333',
  fromPage: 'cart',
  addedFromSBP: false,
};

const res = {
  cartTotalAfterPLCCDiscount: 97.65,
  earnedReward: undefined,
  estimatedRewards: -1,
  giftCardsTotal: 0,
  grandTotal: 138.5,
  isBopisOrder: true,
  isBossOrder: false,
  isBrierleyWorking: false,
  isECOM: true,
  isElectiveBonus: '0',
  isPickupOrder: true,
  loyaltyCallEnabled: undefined,
  loyaltyOrderItems: [
    {
      giftItem: false,
      itemBrand: 'TCP',
      itemDstPrice: 16.25,
      itemPoints: -1,
      itemPrice: 16.5,
      itemUnitDstPrice: 16.25,
      itemUnitPrice: 16.5,
      multiPack: false,
      orderItemId: '1517725845',
      orderItemType: 'BOPIS',
      productPartNumber: '2094333_A0',
      qty: 1,
      variantNo: '2094333007',
    },
  ],
  mprId: undefined,
  orderId: '343954233',
  orderItemId: '1517725851',
  orderItemTotal: '73.50',
  orderItems: [
    {
      itemInfo: {
        itemId: '1517725783',
        itemPoints: -1,
        listPrice: 16.5,
        offerPrice: 16.25,
        quantity: 1,
      },
    },
  ],
  orderSubTotal: 138.5,
  orderSubTotalBeforeDiscount: 139.5,
  orderSubTotalDiscount: 138.5,
  orderSubTotalWithoutGiftCard: 138.5,
  orderTotal: '139.50000',
  orderTotalAfterDiscount: 138.5,
  orderTotalListPrice: 139.5,
  orderTotalSaving: 1,
  pointsApi: 'brierley',
  pointsToNextReward: 0,
  totalItems: 7,
};

const productPayload = {
  brand: undefined,
  getAllMultiPack: null,
  getAllNewMultiPack: undefined,
  isBoss: undefined,
  isGiftCard: false,
  isOpenNewATBModal: true,
  isPdpPage: true,
  multiPackCount: null,
  multipackProduct: null,
  originPage: 'pdp',
  partIdInfo: undefined,
  productName: 'Boys Basic Straight Jeans',
  quantity: 1,
  skuInfo: {
    TCPMultipackProductMapping: ['1'],
    TCPStyleQTY: '1',
    TCPStyleType: '0001',
    color: {
      name: 'DRY INDIGO',
      imagePath: null,
      family: 'DENIM',
      swatchImage: '3015475_528_swatch.jpg',
    },
    fit: 'slim',
    imageUrl: '3015475/3015475_528.jpg',
    productFamily: undefined,
    productId: '3015475_528',
    size: '5S',
    skuId: '1644027',
    unbxdProdId: '3015475_528',
    variantId: '00194936074874',
    variantNo: '2094333007',
  },
  storeLocId: undefined,
  wishlistItemId: undefined,
  productInfo: {
    productName: 'Boys Basic Straight Jeans',
  },
  addedFromSBP: {},
};

describe('Added to bag new saga', () => {
  it('should return correct takeLatest effect', () => {
    const generator = AddedToBagNewSaga();
    const takeLatestDescriptor = generator.next().value;
    const expected = takeLatest(
      ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM_NEW,
      addToCartEcomNewLoadTheATB
    );
    expect(takeLatestDescriptor).toEqual(expected);

    const takeLatestDescriptor2 = generator.next().value;
    const expected2 = takeLatest(
      ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM_NEW_ATB_LOADED,
      addToCartEcomNewAfterAtbLoaded
    );
    expect(takeLatestDescriptor2).toEqual(expected2);
  });

  it('should dispatch openAddedToBagMinimal for addToCartEcom', () => {
    const addToCartEcomGen = addToCartEcomNewLoadTheATB({ payload });
    addToCartEcomGen.next();
    const putDescriptor = addToCartEcomGen.next().value;
    expect(putDescriptor).toEqual(put(openAddedToBagMinimal(payload)));
  });

  it('should dispatch setATBApiCallStatus & setLoyaltyApiCallStatus for addToCartEcom', () => {
    const addToCartEcomGen = addToCartEcomNewLoadTheATB({ payload });
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    let putDescriptor = addToCartEcomGen.next().value;
    expect(putDescriptor).toEqual(put(setATBApiCallStatus(true)));
    putDescriptor = addToCartEcomGen.next().value;
    expect(putDescriptor).toEqual(put(setLoyaltyApiCallStatus(true)));
  });

  it('should dispatch setATBApiCallStatus for addToCartEcom', () => {
    const addToCartEcomGen = addSingleItemToCartECOM(productPayload);
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    expect(addToCartEcomGen.next().value).toEqual(put(setATBApiCallStatus(false)));
  });

  it('should dispatch SetAddedToBagData on receiving ATB success', () => {
    const addToCartEcomGen = addSingleItemToCartECOM(productPayload);
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    expect(addToCartEcomGen.next().value).toEqual(put(SetAddedToBagData(productPayload)));
  });

  it('should dispatch addToBagModalError error resposnse', () => {
    const response = {
      orderId: '1234',
      orderItemId: '1111',
    };
    const err = {
      ...response,
      body: {
        error: 'error',
      },
    };

    const addToCartEcomGen = addSingleItemToCartECOM(productPayload);
    addToCartEcomGen.next();
    addToCartEcomGen.throw(err);
    expect(addToCartEcomGen.next().value).toEqual(select(getPageData));
    expect(addToCartEcomGen.next().value).toEqual(put(addToBagModalError('ERROR', '1234')));
  });

  it('should set loader state true for addMultipleItemToCartECOM', () => {
    const addToCartEcomGen = addMultipleItemToCartECOM(productPayload);
    expect(addToCartEcomGen.next().value).toEqual(put(setLoaderState(true)));
  });
});

it('should filter multipack product with skuinfo data', () => {
  const skuInfo = {
    color: {
      name: 'product name',
    },
  };

  const skuInfo2 = {
    color: {
      name: 'color 2',
    },
  };
  const getAllMultiPack = [
    {
      TCPMultiPackReferenceUSStore: 'test',
      name: 'product name',
    },
  ];
  const result = filterMultiPackVariant(getAllMultiPack, skuInfo);
  expect(result).toEqual(getAllMultiPack);

  const result2 = filterMultiPackVariant(getAllMultiPack, skuInfo2);
  expect(result2).toEqual([]);
});

describe('makeNavigateXHrCall', () => {
  it('should dispatch navigateXHRAction action', () => {
    const isGuestUser = true;
    const gen = makeNavigateXHrCall({});
    expect(gen.next().value).toEqual(select(getIsGuest));
    const next = gen.next(isGuestUser);
    expect(next.value).toEqual(put(navigateXHRAction()));
  });
});

describe('getAllCatId', () => {
  it('should return catIds', () => {
    const params = '123, 546, 567';
    expect(getAllCatId(params)).toEqual('123, 546');
  });
});

describe('getMpackTgas', () => {
  it('should return catIds', () => {
    expect(getMpackTgas('0002', 'pdp', '5', 'polo shirt', '1234')).toEqual({
      dimension124: 'Mpack Pdp|Mpack',
      dimension125: '5pack',
      dimension199: 'Multipack',
      name: 'polo shirt',
      skuId: '1234',
    });
  });
});

describe('filterItems', () => {
  it('should return filterItems', () => {
    const allNewMultiPack = [
      {
        TCPMultiPackUSStore: false,
        name: 'WHITE',
        variants: [
          {
            v_variant: '3000935001',
            v_listprice: 12.95,
            v_offerprice: 12.95,
            TCPWebOnlyFlagUSStore: '1',
            categoryPath2_catMap: [
              '47511>49007|GIRL>School Uniforms',
              '47511>49012|GIRL>Girls Tops 5thNov2',
              '484507>484508|Uniform Shop>Polos',
              '47511>454001|GIRL>Online Only',
              '47511>478019|GIRL>Uniform Extended Sizes',
            ],
            categoryPath3_catMap: [
              '47511>49007>479504|GIRL>School Uniforms>Uniform Extended Sizes',
              '47511>49007>478507|GIRL>School Uniforms>Polos',
              '47511>49012>464013|GIRL>Girls Tops 5thNov2>Polo Shirts',
            ],
            score: 0.5,
            vId: '3000935_10_00193511074261',
          },
        ],
        prodpartno: '3000935_10',
        TCPMultiPackReferenceUSStore: false,
      },
    ];
    const allMultiPack = [
      {
        TCPMultiPackUSStore: false,
        name: 'ROSE DUST',
        variants: [
          {
            v_variant: '3000935001',
            v_listprice: 12.95,
            v_offerprice: 12.95,
            categoryPath2_catMap: [
              '47511>49007|GIRL>School Uniforms',
              '47511>49012|GIRL>Girls Tops 5thNov2',
              '484507>484508|Uniform Shop>Polos',
            ],
            categoryPath3_catMap: [
              '47511>49007>478507|GIRL>School Uniforms>Polos',
              '47511>49012>464013|GIRL>Girls Tops 5thNov2>Polo Shirts',
            ],
            score: 0.34764153,
            vId: '3001569_1301_00193511087742',
          },
        ],
        prodpartno: '3001569_1301',
        TCPMultiPackReferenceUSStore: false,
      },
    ];

    const skuInfo = {
      skuId: '1234093',
      imageUrl: '3001569/3001569_32FJ.jpg',
      color: {
        name: 'REGAL VIOLET',
        imagePath: null,
        family: 'PURPLE',
        swatchImage: '3001569_32FJ_swatch.jpg',
      },
      variantId: '00193511087902',
      variantNo: '3001569017',
      unbxdProdId: '3001569_32FK',
      productId: '3001569_32FJ',
      fit: null,
      size: 'XL (14)',
    };

    const partIdInfo = ['3000935_10#5'];

    expect(filterItems(allNewMultiPack, allMultiPack, skuInfo, partIdInfo)).toEqual('');
  });
});

describe('isLoyaltyServiceEnabled', () => {
  it('should return isLoyaltyServiceEnabled', () => {
    const params = { isBrierleyWorking: false, pointsApi: 'loyalty' };
    expect(isLoyaltyServiceEnabled(params)).toEqual(true);
  });
});

it('should call setATBDataAndHapticFeedback fork', () => {
  Platform.OS = 'android';
  const addedFromSBP = false;

  const generator = setATBDataAndHapticFeedback({ productPayload, res, addedFromSBP });
  const next = generator.next();
  expect(next.value).toEqual(put(SetAddedToBagData({ productPayload, res, addedFromSBP })));
});

it('should call callGetLoyaltyPoints fork', () => {
  Platform.OS = 'android';
  const addedFromSBP = false;
  const dataPayload = {
    productPayload,
    res,
    source: 'AddedToBag',
    pointsApiCall: {},
    addedFromSBP,
  };

  const generator = callGetLoyaltyPoints(dataPayload);
  const next = generator.next();
  expect(next.value).toEqual(put(getLoyaltyDetail(dataPayload)));
  expect(generator.next().value).toEqual(put(setLoyaltyApiCallStatus(false)));
});

it('should call triggerNavigateXHrCall fork', () => {
  const generator = triggerNavigateXHrCall();
  expect(generator.next().value).toEqual(call(makeNavigateXHrCall, { prevCartCount: 0 }));
});

it('should call setMultipackData fork with type MULTIPACK', () => {
  const generator = setMultipackData(MULTI_PACK_TYPE.MULTIPACK);
  expect(generator.next().value).toEqual(put(setMultipackProductType(MULTI_PACK_TYPE.MULTIPACK)));
  expect(generator.next().value).toEqual(put(setIsMultipackItem(true)));
});

it('should call setMultipackData fork with type SINGLE', () => {
  const generator = setMultipackData(MULTI_PACK_TYPE.SINGLE);
  expect(generator.next().value).toEqual(put(setMultipackProductType(MULTI_PACK_TYPE.SINGLE)));
  expect(generator.next().value).toEqual(put(setIsMultipackItem(false)));
});

it('should dispatch clearATBErrors action', () => {
  const generator = clearATBErrors();
  expect(generator.next().value).toEqual(put(clearAddToBagModalError()));
});

it('should call addToCartUpdateLocalData fork', () => {
  const generator = addToCartUpdateLocalData(
    ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM,
    productPayload,
    res
  );
  expect(generator.next().value).toEqual(select(BagPageSelectors.getOrderItemsLocal));
});

describe('logAddToBagEvent', () => {
  it('should dispatch trackClickWithData action', () => {
    Platform.OS = 'android';
    const params = {
      quantity: 2,
      cartPrice: '120',
    };
    const data = {
      departmentList: [],
    };
    const generator = logAddToBagEvent({ params });
    expect(generator.next().value).toEqual(select(getClickAnalyticsData));
    const next = generator.next(data);
    generator.next();
    generator.next();
    expect(next.value).toEqual(
      put(
        trackClickWithData(data, {
          name: 'add_to_cart',
          module: 'browse',
        })
      )
    );
  });
});
