// 9fbef606107a605d69c0edbcd8029e5d 
import { setBagAnalytics } from '../container/AddedToBag.actions';
import ADDEDTOBAG_CONSTANTS from '../AddedToBag.constants';

describe('AddedtoBag Actions', () => {
  it('setBagAnalytics', () => {
    expect(setBagAnalytics([{ dimension124: 'mpack' }])).toEqual({
      payload: [{ dimension124: 'mpack' }],
      type: ADDEDTOBAG_CONSTANTS.SET_BAG_ANALYTICS,
    });
  });
});
