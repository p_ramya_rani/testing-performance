// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AddedToBagVanilla } from '../views/AddedToBag.view';
import PickupBannerView from '../molecules/PickupBanner/views/PickupBanner.view';

describe('Added to Bag View', () => {
  const props = {
    openState: true,
    onRequestClose: jest.fn(),
    addedToBagData: {},
    className: 'test',
    labels: {},
    quantity: 1,
    handleContinueShopping: jest.fn(),
    handleCartCheckout: jest.fn(),
    pointsSummary: {},
    isATBModalBackAbTestNewDesign: true,
  };
  const newprops = {
    ...props,
    isATBHidePoints: true,
  };
  it('should render Added to Bag view ', () => {
    const component = shallow(<AddedToBagVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render Added to Bag view with updated view', () => {
    const component = shallow(<AddedToBagVanilla {...newprops} />);
    expect(component).toMatchSnapshot();
  });

  it('should not render Added to Bag view with PickupBannerView ', () => {
    const props1 = {
      ...props,
      showBOPISPickupInstead: true,
      addedToBagData: { pickupType: 'bopis' },
    };
    const component = shallow(<AddedToBagVanilla {...props1} />);
    expect(component.find(PickupBannerView)).toHaveLength(0);
    expect(component).toMatchSnapshot();
  });

  it('should render Added to Bag view with PickupBannerView ', () => {
    const props1 = {
      ...props,
      showBOPISPickupInstead: true,
    };
    const component = shallow(<AddedToBagVanilla {...props1} />);
    expect(component.find(PickupBannerView)).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly for isATBModalBackAbTestNewDesign', () => {
    const props1 = {
      ...props,
      isATBModalBackAbTestNewDesign: false,
    };
    const component = shallow(<AddedToBagVanilla {...props1} />);
    expect(component).toMatchSnapshot();
  });
});
describe('Added to Bag View with Multiple Items', () => {
  const props = {
    openState: true,
    onRequestClose: jest.fn(),
    addedToBagData: [
      {
        branc: 'tcp',
      },
    ],
    className: 'test',
    labels: {},
    quantity: 1,
    handleContinueShopping: jest.fn(),
    handleCartCheckout: jest.fn(),
    pointsSummary: {},
  };
  it('should render Added to Bag view ', () => {
    const component = shallow(<AddedToBagVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render Added to Bag view without Continue Shopping link', () => {
    const component = shallow(<AddedToBagVanilla {...props} enableKeepShoppingCTA />);
    expect(component).toMatchSnapshot();
  });
});
