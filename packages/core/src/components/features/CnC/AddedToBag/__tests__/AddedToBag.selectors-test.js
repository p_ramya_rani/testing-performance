// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import * as utils from '@tcp/core/src/utils/utils';
import {
  getAddedToBagData,
  isOpenAddedToBag,
  getOrderItems,
  getQuantityValue,
  getPointsSummary,
  getAddedToPickupError,
  getMultipleItemsAddedToBagError,
  getAddedToBagInterval,
  getStickyATBState,
  getBagAnalyticsData,
  getEnableBOPISPickupInstead,
  getPageType,
} from '../container/AddedToBag.selectors';

utils.getBrand = jest.fn().mockReturnValue('tcp');

const earnedRewardStr = '$5 rewards';

describe('#Added to bag Selectors', () => {
  let AddedToBagState = fromJS({
    itemInfo: {
      quantity: '2',
    },
    error: false,
    pickupError: false,
    multipleItemsError: false,
    isOpenAddedToBag: false,
    stickyFooter: true,
  });

  AddedToBagState = AddedToBagState.set('itemInfo', {
    quantity: '2',
    orderItemId: 12345,
    orderItems: [
      {
        itemInfo: {
          itemId: 12345,
          quantity: '3',
          offerPrice: 2,
          itemPoints: 3,
        },
      },
    ],
    isBrierleyWorking: true,
    pointsToNextReward: 123,
    totalItems: 5,
    estimatedRewards: 200,
    grandTotal: 300,
    giftCardsTotal: 100,
    pointsApi: 'loyalty',
    cartTotalAfterPLCCDiscount: 20,
    earnedReward: earnedRewardStr,
    orderSubTotalDiscount: 10,
  });

  AddedToBagState = AddedToBagState.set('itemInfoArray', [
    {
      quantity: 1,
      orderItemId: 12345,
    },
  ]);
  const CartPageState = fromJS({
    orderDetails: {
      isBrierleyWorking: true,
      pointsToNextReward: 123,
      totalItems: 5,
      estimatedRewards: 200,
      grandTotal: 300,
      giftCardsTotal: 100,
      pointsApi: 'loyalty',
      cartTotalAfterPLCCDiscount: 20,
      earnedReward: earnedRewardStr,
      orderSubTotalDiscount: 10,
      orderItems: [
        {
          itemInfo: {
            itemId: 12345,
            quantity: '3',
            offerPrice: 2,
            itemPoints: 0,
          },
        },
      ],
    },
    uiFlags: {
      updateCartComplete: true,
    },
  });
  const state = {
    AddedToBagReducer: AddedToBagState,
    CartPageReducer: CartPageState,
  };

  it('#getAddedToBagData should return itemInfo', () => {
    expect(getAddedToBagData(state)).toEqual(AddedToBagState.get('itemInfo'));
  });
  it('#isOpenAddedToBag should return isOpenAddedToBag state', () => {
    expect(isOpenAddedToBag(state)).toEqual(AddedToBagState.get('isOpenAddedToBag'));
  });

  it('#getOrderItems should return state', () => {
    expect(getOrderItems(state)).toEqual(CartPageState.getIn(['orderDetails', 'orderItems']));
  });

  it('#getMultipleItemsAddedToBagError should return state', () => {
    expect(getMultipleItemsAddedToBagError(state)).toEqual(false);
  });

  it('#getAddedToPickupError should return state', () => {
    expect(getAddedToPickupError(state)).toEqual(false);
  });

  it('#getQuantityValue should return state', () => {
    expect(getQuantityValue(state)).toEqual('3');
  });

  it('#getBagAnalyticsData should return getBagAnalyticsData state', () => {
    expect(getBagAnalyticsData(state)).toEqual(AddedToBagState.get('cartAnalyticsData'));
  });

  it('#getPointsSummary should return state', () => {
    expect(getPointsSummary(state)).toEqual({
      bagSubTotal: 200,
      cartTotalAfterPLCCDiscount: 20,
      earnedReward: earnedRewardStr,
      isBrierleyWorking: true,
      itemPoints: 3,
      itemPrice: 2,
      orderSubTotalDiscount: 10,
      pointsApi: 'loyalty',
      pointsToNextReward: 123,
      totalItems: 5,
      userPoints: 200,
    });
  });

  it('#getPointsSummary should return state with multiple items', () => {
    expect(getPointsSummary(state)).toEqual({
      bagSubTotal: 200,
      earnedReward: earnedRewardStr,
      isBrierleyWorking: true,
      itemPoints: 3,
      itemPrice: 2,
      pointsToNextReward: 123,
      totalItems: 5,
      userPoints: 200,
      cartTotalAfterPLCCDiscount: 20,
      orderSubTotalDiscount: 10,
      pointsApi: 'loyalty',
    });
  });

  it('getStickyATBState should return state', () => {
    expect(getStickyATBState(state)).toEqual(true);
  });
});

describe('#Added to Bag Interval Selectors', () => {
  const sessionState = {
    siteDetails: {
      SFL_MAX_COUNT: '200',
      IS_SAVE_FOR_LATER_ENABLED: true,
    },
  };
  const state = {
    session: sessionState,
  };
  it('#getAddedToBagInterval should return Interval', () => {
    expect(getAddedToBagInterval(state)).toEqual(0);
  });
});

describe('#getPageType should return page type', () => {
  const state = {
    pageData: {
      pageType: 'outfit',
    },
  };
  it('#getPageType should return page type outfit', () => {
    expect(getPageType(state)).toEqual('outfit');
  });
});

describe('getEnableBOPISPickupInstead ABTest', () => {
  it('#getEnableBOPISPickupInstead should be true', () => {
    const state = {
      AbTest: {
        showBOPISPickupInstead: true,
      },
      session: {
        siteDetails: {
          isBOPISEnabled_TCP: true,
        },
      },
    };
    expect(getEnableBOPISPickupInstead(state)).toBeTruthy();
  });

  it('#getEnableBOPISPickupInstead should be false', () => {
    const state = {
      AbTest: {
        showBOPISPickupInstead: false,
      },
    };
    expect(getEnableBOPISPickupInstead(state)).toBeFalsy();
  });

  it('#getEnableBOPISPickupInstead should be false', () => {
    const state = {
      AbTest: {
        showBOPISPickupInstead: false,
      },
      session: {
        siteDetails: {
          isBOPISEnabled_TCP: false,
        },
      },
    };
    expect(getEnableBOPISPickupInstead(state)).toBeFalsy();
  });
});
