// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { AddedToBagSkeletonVanilla } from '../skeleton/AddedToBagSkeleton.view';

describe('AddedToBagSkelton common component', () => {
  it('renders correctly', () => {
    const props = {
      className: 'className',
    };
    const component = shallow(<AddedToBagSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
