// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Platform } from 'react-native';
import { shallow } from 'enzyme';
import { AddedToBagVanilla } from '../views/AddedToBagNew.view.native';
import PickupBannerView from '../molecules/PickupBanner/views/PickupBanner.view.native';

describe('Added to Bag View', () => {
  const props = {
    openState: true,
    onRequestClose: jest.fn(),
    className: 'test',
    labels: {},
    quantity: 1,
    handleContinueShopping: jest.fn(),
    handleCartCheckout: jest.fn(),
    pointsSummary: {},
    navigation: {
      getParam: jest.fn(),
    },
    isNewReDesignEnabled: true,
    isATBModalBackAbTestNewDesign: true,
    openStatePayloadData: {
      brand: undefined,
      fromPage: 'PLP',
      isGiftCard: false,
      isOpenNewATBModal: true,
      originPage: 'plp',
      productName: 'Boys Basic Straight Jeans',
      quantity: 1,
      selectedMultipack: '1',
      skuInfo: {
        TCPMultipackProductMapping: ['1'],
        TCPStyleQTY: '1',
        TCPStyleType: '0001',
        color: {
          name: 'INDIGO',
          imagePath: null,
          family: 'DENIM',
          swatchImage: '3015475_528_swatch1.jpg',
        },
        fit: 'slim',
        imageUrl: '3015475/3015475_5287.jpg',
        productFamily: undefined,
        productId: '3015475_528',
        size: '5S',
        skuId: '1644027',
        unbxdProdId: '3015475_528',
        variantId: '00194936074874',
        variantNo: '3015476002',
      },
    },
    addedToBagData: {
      addedFromSBP: undefined,
      brand: undefined,
      cartTotalAfterPLCCDiscount: 112.1,
      earnedReward: undefined,
      estimatedRewards: -1,
      fromPage: 'PLP',
      getAllMultiPack: null,
      getAllNewMultiPack: null,
      giftCardsTotal: 0,
      grandTotal: (144.14).toExponential,
      isBopisOrder: false,
      isBoss: undefined,
      isBossOrder: false,
      isBrierleyWorking: false,
      isECOM: true,
      isElectiveBonus: '0',
      isGiftCard: false,
      isOpenNewATBModal: true,
      isPickupOrder: false,
      loyaltyCallEnabled: undefined,
      loyaltyOrderItems: [
        {
          giftItem: false,
          itemBrand: 'TCP',
          itemDstPrice: 11.66,
          itemPoints: -1,
          itemPrice: 12.95,
          itemUnitDstPrice: 11.66,
          itemUnitPrice: 12.95,
          multiPack: false,
          orderItemId: '1517691991',
          orderItemType: 'ECOM',
          productPartNumber: '2043378_OM',
          qty: 1,
          variantNo: '2043378103',
        },
      ],
      mprId: undefined,
      multiPackCount: null,
      multipackProduct: null,
      orderId: '343918438',
      orderItemId: '1517692185',
      orderItemTotal: '73.50',
      orderItems: [
        {
          itemInfo: {
            itemId: '1517691991',
            itemPoints: -1,
            listPrice: 12.95,
            offerPrice: 11.66,
            quantity: 1,
          },
        },
      ],
      orderSubTotal: 144.14,
      orderSubTotalBeforeDiscount: 160.15,
      orderSubTotalDiscount: 144.14,
      orderSubTotalWithoutGiftCard: 144.14,
      orderTotal: '160.15000',
      orderTotalAfterDiscount: 144.14,
      orderTotalListPrice: 160.15,
      orderTotalSaving: 16.01,
      originPage: 'plp',
      partIdInfo: null,
      pointsApi: 'brierley',
      pointsToNextReward: 0,
      productName: 'Boys Basic Straight Jeans',
      quantity: 1,
      selectedMultipack: '1',
      skuInfo: {
        skuId: '1644027',
        imageUrl: '3015475/3015475_528.jpg',
        color: {
          name: 'DRY INDIGO',
          imagePath: null,
          family: 'DENIM',
          swatchImage: '3015475_528_swatch.jpg',
        },
        variantId: '00194936074874',
        variantNo: '3015476002',
      },
      storeLocId: undefined,
      totalItems: 10,
    },
    pdpLabels: {
      youMayAlsoLike: 'You May Also Like',
      weFoundTheseSimilarItems: 'We Found These Similar Items',
    },
  };

  const errorProps = {
    openState: true,
    onRequestClose: jest.fn(),
    className: 'test',
    labels: {},
    quantity: 1,
    handleContinueShopping: jest.fn(),
    handleCartCheckout: jest.fn(),
    pointsSummary: {},
    navigation: {
      getParam: jest.fn(),
    },
    isPhysicalMpackEnabled: true,
    isABTestOffStyleWith: false,
    isNewReDesignEnabled: true,
    isATBModalBackAbTestNewDesign: true,
    openStatePayloadData: {
      brand: undefined,
      fromPage: 'PLP',
      isGiftCard: false,
      isOpenNewATBModal: true,
      originPage: 'plp',
      productName: 'Basic Straight Jeans',
      quantity: 1,
      selectedMultipack: '1',
      skuInfo: {
        TCPMultipackProductMapping: ['1'],
        TCPStyleQTY: '1',
        TCPStyleType: '0002',
        color: {
          name: 'DRY INDIGO',
          imagePath: null,
          family: 'DENIM',
          swatchImage: '3015475_528_swatch.jpg',
        },
        fit: 'slim',
        imageUrl: '3015475/3015475_528.jpg',
        productFamily: undefined,
        productId: '3015475_528',
        size: '5S',
        skuId: '1644027',
        unbxdProdId: '3015475_528',
        variantId: '00194936074874',
        variantNo: '3015476002',
      },
    },
    addToBagModalError: 'Oops! Error',
    pdpLabels: {
      youMayAlsoLike: 'You May Also Like',
      weFoundTheseSimilarItems: 'We Found These Similar Items',
    },
  };

  it('should render Added to Bag view', () => {
    const component = shallow(<AddedToBagVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render Error Added to Bag view', () => {
    Platform.OS = 'android';
    const component = shallow(<AddedToBagVanilla {...errorProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should render Added to Bag view with new experience', () => {
    const component = shallow(<AddedToBagVanilla isATBHidePoints {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render Added to Bag view with new experience', () => {
    const component = shallow(<AddedToBagVanilla isATBHidePoints {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should not render Added to Bag view with PickupBannerView ', () => {
    const props1 = {
      ...props,
      showBOPISPickupInstead: true,
      addedToBagData: { pickupType: 'bopis' },
    };
    const component = shallow(<AddedToBagVanilla {...props1} />);
    expect(component.find(PickupBannerView)).toHaveLength(0);
    expect(component).toMatchSnapshot();
  });
});
