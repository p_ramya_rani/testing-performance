// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { AddedToBagVanilla } from '../views/AddedToBag.view.native';

describe('AddedToBag Component', () => {
  let component;
  let props = {
    openState: Function,
    onRequestClose: Function,
    className: '',
    addedToBag: {},
    addedToBagData: {},
    labels: {},
    quantity: '',
    handleContinueShopping: Function,
    navigation: {
      navigate: jest.fn(),
      param: { headerMode: false },
      getParam: Function,
    },
    isPayPalWebViewEnable: false,
    hideHeader: false,
    pointsSummary: {},
    isVenmoEnabled: true,
    isVenmoAppInstalled: true,
  };

  beforeEach(() => {
    component = shallow(<AddedToBagVanilla {...props} />);
  });

  it('AddedToBag should be defined', () => {
    expect(component).toBeDefined();
  });

  it('AddedToBag should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('AddedToBag should render correctly', () => {
    props = { ...props, bagLoading: true };
    component = shallow(<AddedToBagVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('AddedToBag should render correctly without continue shopping link', () => {
    props = { ...props, enableKeepShoppingCTA: true };
    component = shallow(<AddedToBagVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
