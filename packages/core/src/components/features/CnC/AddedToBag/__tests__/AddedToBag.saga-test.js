/* eslint-disable  max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { put, takeLatest, select, call } from 'redux-saga/effects';
import { trackClickWithData } from '@tcp/core/src/analytics/actions';
import { getClickAnalyticsData } from '@tcp/core/src/analytics/Analytics.selectors';
import { getIsGuest } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { navigateXHRAction } from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.action';
import {
  getIsSuppressGetOrderDetails,
  getIsAvoidGetOrderDetails,
  getABTestAddToBagDrawer,
  getIsSingleAddToBagAPIEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getPageName } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { getOrderDetailSaga } from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.saga';
import {
  getLoyaltyDetail,
  updateLoyaltyPoints,
} from '@tcp/core/src/components/features/CnC/LoyaltyBanner/container/Loyalty.actions';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { isMobileApp } from '@tcp/core/src/utils';
import { toggleCheckoutRouting } from '../../Checkout/container/Checkout.action';
import BAG_PAGE_ACTIONS from '../../BagPage/container/BagPage.actions';

// import { validateReduxCache } from '../../../../../../utils/cache.util';
import {
  addToCartEcom,
  addItemToCartBopis,
  AddedToBagSaga,
  addMultipleItemToCartECOM,
  filterMultiPackVariant,
  logAddToBagEvent,
  makeNavigateXHrCall,
  isSuppressGetOrderDetails,
  isAvoidGetOrderDetails,
  suppressGetOrderDetailsCheck,
  isgetABTestAddToBagDrawer,
  openAddedToBagModal,
  getAllCatId,
  filterItems,
  isLoyaltyServiceEnabled,
  checkLoyaltyNAFlag,
  getMpackTgas,
  multiAddToBagECOM,
  addMultipleItemToCartECOMSingleAPI,
  getParams,
  getErrorMessage,
} from '../container/AddedToBag.saga';
import {
  openAddedToBag,
  AddToPickupError,
  AddToCartMultipleItemError,
  setAddedToBagButton,
  setOutfitCarouselAddedButton,
  setLoyaltyABTestFlag,
  AddToCartError,
} from '../container/AddedToBag.actions';
import ADDEDTOBAG_CONSTANTS from '../AddedToBag.constants';

jest.mock('../util/utility', () => {
  return {
    makeBrandToggling: () => false,
    logCustomEvent: () => {},
  };
});

jest.mock('@tcp/core/src/utils', () => {
  const originalModule = jest.requireActual('@tcp/core/src/utils');
  return {
    ...originalModule,
    isMobileApp: jest.fn(),
    getAPIConfig: () => ({
      storeId: '11232',
      langId: 'en',
      catalogId: '111112',
    }),
  };
});

describe('Added to bag saga', () => {
  it('should dispatch addToCartEcomGen action for success resposnse', () => {
    const payload = {
      skuInfo: {
        skuId: 'fgfdgfdg',
      },
      quantity: 1,
      wishlistItemId: '333',
    };
    const addToCartEcomGen = addToCartEcom({ payload });
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();

    const response = {
      orderId: '1234',
      orderItemId: '1111',
    };
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    const err = {
      ...response,
      body: {
        error: 'error',
      },
    };
    const addToCartEcomGen1 = addToCartEcom({ payload });
    addToCartEcomGen1.next();
    addToCartEcomGen1.next(err);
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen1.next();
    addToCartEcomGen1.next();
    addToCartEcomGen1.throw(err);
    addToCartEcomGen1.next();
    expect(addToCartEcomGen1.next().value).toEqual(put(AddToCartError('ERROR')));
  });
  it('should dispatch addToCartEcomGen action for error resposnse', () => {
    const payload = {};
    const addToCartEcomGen1 = addToCartEcom({ payload });
    addToCartEcomGen1.next();
  });

  it('should dispatch getLoyaltyDetail action for addToCartEcom', () => {
    const payload = {
      skuInfo: {
        skuId: 'fgfdgfdg',
        unbxdProdId: '12345',
      },
      quantity: 1,
      wishlistItemId: '333',
      skipAddToBagModal: false,
    };
    const addToCartEcomGen = addToCartEcom({ payload });
    const response = {
      orderId: '1234',
      orderItemId: '1111',
      isBrierleyWorking: false,
      estimatedRewards: 123,
      pointsToNextReward: 312,
      earnedReward: null,
      pointsApi: 'loyalty',
      orderTotal: '540',
    };
    const requestPayload = {
      pointsApiCall: 'loyalty',
      pointsToNextReward: 312,
      quantity: 1,
      skipAddToBagModal: false,
      skuInfo: {
        skuId: 'fgfdgfdg',
        unbxdProdId: '12345',
      },
      source: 'AddedToBag',
      wishlistItemId: '333',
      earnedReward: null,
      estimatedRewards: 123,
      isBrierleyWorking: false,
      orderId: '1234',
      orderItemId: '1111',
      orderTotal: '540',
      pointsApi: 'loyalty',
      addedFromSBP: undefined,
    };
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next(response);
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    expect(addToCartEcomGen.next().value).toEqual(put(getLoyaltyDetail(requestPayload)));
  });

  it('should dispatch setAddedToBagButton action if skipAddToBagModal flag is true for addToCartEcom', () => {
    const payload = {
      skuInfo: {
        skuId: 'fgfdgfdg',
        unbxdProdId: '12345',
      },
      quantity: 1,
      wishlistItemId: '333',
      skipAddToBagModal: true,
    };
    const addToCartEcomGen = addToCartEcom({ payload });
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    const response = {
      orderId: '1234',
      orderItemId: '1111',
    };

    addToCartEcomGen.next();
    addToCartEcomGen.next();
    let putDescriptor = addToCartEcomGen.next(response).value;
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    putDescriptor = addToCartEcomGen.next().value;
    expect(putDescriptor).toEqual(put(setAddedToBagButton('12345')));
  });

  it('should call getOrderDetailSaga if it is from bag page for addToCartEcom', () => {
    const payload = {
      skuInfo: {
        skuId: 'fgfdgfdg',
        unbxdProdId: '12345',
      },
      quantity: 1,
      wishlistItemId: '333',
      fromPage: 'cart',
    };
    const addToCartEcomGen = addToCartEcom({ payload });
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();

    const response = {
      orderId: '1234',
      orderItemId: '1111',
    };
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    let putDescriptor = addToCartEcomGen.next(response).value;
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    putDescriptor = addToCartEcomGen.next().value;
    expect(putDescriptor).toEqual(
      call(getOrderDetailSaga, { ...payload, payload: { recalcRewards: true } })
    );
  });

  it('should dispatch setOutfitCarouselAddedButton for addToCartEcom', () => {
    const payload = {
      skuInfo: {
        skuId: 'fgfdgfdg',
        unbxdProdId: '12345',
      },
      quantity: 1,
      wishlistItemId: '333',
      fromPage: 'cart',
      fromOutfitCarouselDetails: true,
    };
    const addToCartEcomGen = addToCartEcom({ payload });
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();

    const response = {
      orderId: '1234',
      orderItemId: '1111',
    };
    addToCartEcomGen.next();

    let putDescriptor = addToCartEcomGen.next(response).value;
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    putDescriptor = addToCartEcomGen.next().value;
    addToCartEcomGen.next();
    expect(putDescriptor).toEqual(put(setOutfitCarouselAddedButton(true)));
  });

  it('should dispatch setLoyaltyABTestFlag  for addToCartEcom', () => {
    const payload = {
      skuInfo: {
        skuId: 'fgfdgfdg',
        unbxdProdId: '12345',
      },
      quantity: 1,
      wishlistItemId: '333',
      fromPage: 'cart',
      fromOutfitCarouselDetails: false,
      skipAddToBagModal: true,
    };

    const addToCartEcomGen = addToCartEcom({ payload });
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();

    const response = {
      orderId: '1234',
      orderItemId: '1111',
    };
    addToCartEcomGen.next();
    addToCartEcomGen.next();

    let putDescriptor = addToCartEcomGen.next(response).value;
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    addToCartEcomGen.next();
    putDescriptor = addToCartEcomGen.next().value;
    expect(putDescriptor).toEqual(put(setLoyaltyABTestFlag(true)));
  });

  it('should dispatch addToCartBopis', () => {
    const payload = {
      productInfo: {
        storeLocId: '345',
        isBoss: true,
        quantity: '1',
        skuInfo: { skuId: 'skuId', variantId: 'variantId', variantNo: 'variantNo' },
      },
    };
    const addItemToCartBopisGen = addItemToCartBopis({ payload });
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    const response = {
      orderItemId: '1111',
    };
    const res = {
      ...payload.productInfo,
      ...response,
    };
    addItemToCartBopisGen.next(response);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    const putDescriptor = addItemToCartBopisGen.next().value;
    expect(putDescriptor).toEqual(put(openAddedToBag(res)));
    const err = {
      ...response,
      body: {
        error: 'error',
      },
    };
    const addItemToCartBopisGen1 = addItemToCartBopis({ payload });
    addItemToCartBopisGen1.next();
    addItemToCartBopisGen1.next();
    addItemToCartBopisGen1.throw(err);
    expect(addItemToCartBopisGen1.next().value).toEqual(put(AddToPickupError('ERROR')));
  });

  it('should dispatch updateLoyaltyPoints for addToCartBopis', () => {
    const payload = {
      productInfo: {
        storeLocId: '345',
        isBoss: true,
        quantity: '1',
        skuInfo: { skuId: 'skuId', variantId: 'variantId', variantNo: 'variantNo' },
      },
    };
    const addItemToCartBopisGen = addItemToCartBopis({ payload });
    addItemToCartBopisGen.next(true);
    addItemToCartBopisGen.next();

    const response = {
      orderItemId: '1111',
      isBrierleyWorking: true,
      estimatedRewards: 123,
      pointsToNextReward: 312,
      earnedReward: null,
    };
    const res = {
      ...payload.productInfo,
      ...response,
    };
    const loyaltyPayload = {
      isBrierleyWorking: true,
      estimatedRewards: 123,
      pointsToNextReward: 312,
      earnedReward: null,
    };
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next(response);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next(res);
    addItemToCartBopisGen.next(true);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    const putDescriptor = addItemToCartBopisGen.next().value;
    expect(putDescriptor).toEqual(put(updateLoyaltyPoints(loyaltyPayload)));
  });

  it('should call getOrderDetailSaga for addToCartBopis', () => {
    const payload = {
      productInfo: {
        storeLocId: '345',
        isBoss: true,
        quantity: '1',
        skuInfo: { skuId: 'skuId', variantId: 'variantId', variantNo: 'variantNo' },
      },
      fromPage: 'cart',
    };
    const addItemToCartBopisGen = addItemToCartBopis({ payload });
    addItemToCartBopisGen.next(true);
    addItemToCartBopisGen.next();
    const response = {
      orderItemId: '1111',
      isBrierleyWorking: true,
      estimatedRewards: 123,
      pointsToNextReward: 312,
      earnedReward: null,
      pointsApi: 'loyalty',
      orderTotal: '540',
    };
    const res = {
      ...payload.productInfo,
      ...response,
    };
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next(response);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next(res);
    addItemToCartBopisGen.next(true);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    const putDescriptor = addItemToCartBopisGen.next().value;
    expect(putDescriptor).toEqual(put(setLoaderState(false)));
    addItemToCartBopisGen.next(true);
    expect(addItemToCartBopisGen.next().value).toEqual(
      call(getOrderDetailSaga, { ...payload, payload: { recalcRewards: true } })
    );
    addItemToCartBopisGen.next();
    expect(addItemToCartBopisGen.next().value).toEqual(
      call(makeNavigateXHrCall, { prevCartCount: 0 })
    );
    expect(addItemToCartBopisGen.next().value).toEqual(
      call(logAddToBagEvent, {
        cartPrice: '540',
        quantity: '1',
      })
    );
  });

  it('should call getOrderDetailSaga for addToCartBopis for mobile app', () => {
    isMobileApp.mockImplementation(() => true);
    const payload = {
      productInfo: {
        storeLocId: '345',
        isBoss: true,
        quantity: '1',
        skuInfo: { skuId: 'skuId', variantId: 'variantId', variantNo: 'variantNo' },
      },
      fromPage: 'cart',
    };
    const addItemToCartBopisGen = addItemToCartBopis({ payload });
    addItemToCartBopisGen.next(true);
    addItemToCartBopisGen.next();
    const response = {
      orderItemId: '1111',
      isBrierleyWorking: true,
      estimatedRewards: 123,
      pointsToNextReward: 312,
      earnedReward: null,
      pointsApi: 'loyalty',
      orderTotal: '540',
    };
    const res = {
      ...payload.productInfo,
      ...response,
    };
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next(response);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next(res);
    addItemToCartBopisGen.next(true);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    const putDescriptor = addItemToCartBopisGen.next().value;
    expect(putDescriptor).toEqual(put(setLoaderState(false)));
    addItemToCartBopisGen.next(true);
    expect(addItemToCartBopisGen.next().value).toEqual(
      call(getOrderDetailSaga, { ...payload, payload: { recalcRewards: true } })
    );
    expect(addItemToCartBopisGen.next().value).toEqual(
      call(makeNavigateXHrCall, { prevCartCount: 0 })
    );
    expect(addItemToCartBopisGen.next().value).toEqual(
      call(logAddToBagEvent, {
        cartPrice: '540',
        quantity: '1',
      })
    );
  });

  it('should call toggleCheckoutRouting for addToCartBopis', () => {
    const payload = {
      productInfo: {
        storeLocId: '345',
        isBoss: true,
        quantity: '1',
        skuInfo: { skuId: 'skuId', variantId: 'variantId', variantNo: 'variantNo' },
      },
      fromPage: 'pdp',
    };
    const addItemToCartBopisGen = addItemToCartBopis({ payload });
    addItemToCartBopisGen.next(true);
    addItemToCartBopisGen.next();

    const response = {
      orderItemId: '1111',
      isBrierleyWorking: true,
      estimatedRewards: 123,
      pointsToNextReward: 312,
      earnedReward: null,
      pointsApi: 'loyalty',
      orderTotal: '540',
    };
    const res = {
      ...payload.productInfo,
      ...response,
    };
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next(response);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next(res);
    addItemToCartBopisGen.next(true);
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    addItemToCartBopisGen.next();
    const putDescriptor = addItemToCartBopisGen.next().value;
    expect(putDescriptor).toEqual(put(setLoaderState(false)));
    addItemToCartBopisGen.next(true);
    expect(addItemToCartBopisGen.next().value).toEqual(put(toggleCheckoutRouting(true)));
  });

  it('should dispatch AddToCartMultipleItemError In Error', () => {
    const payload = {
      productItemsInfo: [
        {
          storeLocId: '345',
          isBoss: true,
          quantity: '1',
          skuInfo: { skuId: 'skuId', variantId: 'variantId', variantNo: 'variantNo' },
        },
      ],
    };
    const err = {
      error: { errorResponse: { errorMessage: 'Error' } },
      errorProductId: 1,
      atbSuccessProducts: [],
    }; // [] with zero length
    const atbError = { errMsg: 'Error', errorProductId: 1 };
    const addMultipleItemToCartECOMGenError = addMultipleItemToCartECOM(payload);
    addMultipleItemToCartECOMGenError.next();
    addMultipleItemToCartECOMGenError.next();
    addMultipleItemToCartECOMGenError.next();
    addMultipleItemToCartECOMGenError.next();
    addMultipleItemToCartECOMGenError.throw(err);
    expect(addMultipleItemToCartECOMGenError.next().value).toEqual(
      put(AddToCartMultipleItemError(atbError))
    );
    expect(addMultipleItemToCartECOMGenError.next().value).toEqual(put(setLoaderState(false)));
  });

  describe('CardListSaga', () => {
    it('should return correct takeLatest effect', () => {
      const generator = AddedToBagSaga();
      expect(generator.next().value.toString()).toMatch(
        takeLatest(ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM, addToCartEcom).toString()
      );
      expect(generator.next().value.toString()).toMatch(
        takeLatest(ADDEDTOBAG_CONSTANTS.ADD_TO_CART_BOPIS, addItemToCartBopis).toString()
      );
    });
  });

  it('should filter multipack product with skuinfo data', () => {
    const skuInfo = {
      color: {
        name: 'product name',
      },
    };
    const getAllMultiPack = [
      {
        TCPMultiPackReferenceUSStore: 'test',
        name: 'product name',
      },
    ];
    const result = filterMultiPackVariant(getAllMultiPack, skuInfo);
    expect(result).toEqual(getAllMultiPack);
  });

  describe('logAddToBagEvent', () => {
    it('should dispatch trackClickWithData action', () => {
      const params = {
        quantity: 2,
        cartPrice: '120',
      };
      const data = {
        departmentList: [],
      };
      const gen = logAddToBagEvent(params);
      expect(gen.next().value).toEqual(select(getClickAnalyticsData));
      const next = gen.next(data);
      expect(next.value).toEqual(
        put(
          trackClickWithData(data, {
            name: 'add_to_cart',
            module: 'browse',
          })
        )
      );
    });
  });

  describe('makeNavigateXHrCall', () => {
    it('should dispatch navigateXHRAction action', () => {
      const isGuestUser = true;
      const gen = makeNavigateXHrCall({});
      expect(gen.next().value).toEqual(select(getIsGuest));
      const next = gen.next(isGuestUser);
      expect(next.value).toEqual(put(navigateXHRAction()));
    });
  });

  describe('isSuppressGetOrderDetails', () => {
    it('should return suppressGetOrderDetails value', () => {
      const gen = isSuppressGetOrderDetails();
      expect(gen.next().value).toEqual(select(getIsSuppressGetOrderDetails));
    });
  });

  describe('suppressGetOrderDetailsCheck', () => {
    it('should return suppressGetOrderDetailsCheck value', () => {
      const gen = suppressGetOrderDetailsCheck();
      expect(gen.next().value).toEqual(call(isAvoidGetOrderDetails));
    });
  });

  describe('isAvoidGetOrderDetails', () => {
    it('should return avoidGetOrderDetails value', () => {
      const gen = isAvoidGetOrderDetails();
      expect(gen.next().value).toEqual(select(getIsAvoidGetOrderDetails));
    });
  });

  describe('isgetABTestAddToBagDrawer', () => {
    it('should return isgetABTestAddToBagDrawer value', () => {
      const gen = isgetABTestAddToBagDrawer();
      expect(gen.next().value).toEqual(select(getABTestAddToBagDrawer));
    });
  });

  describe('multiAddToBagECOM', () => {
    it('should invoke addMultipleItemToCartECOM when getIsSingleAddToBagAPIEnabled is false', () => {
      const singleAddedToBagAPIEnabled = false;
      const payload = [{}, {}];
      const gen = multiAddToBagECOM({ payload });
      expect(gen.next().value).toEqual(select(getIsSingleAddToBagAPIEnabled));
      const next = gen.next(singleAddedToBagAPIEnabled);
      expect(next.value).toEqual(call(addMultipleItemToCartECOM, payload));
    });
    it('should invoke addMultipleItemToCartECOMSingleAPI when getIsSingleAddToBagAPIEnabled is true', () => {
      const singleAddedToBagAPIEnabled = true;
      const payload = [{}, {}];
      const gen = multiAddToBagECOM({ payload });
      expect(gen.next().value).toEqual(select(getIsSingleAddToBagAPIEnabled));
      const next = gen.next(singleAddedToBagAPIEnabled);
      expect(next.value).toEqual(call(addMultipleItemToCartECOMSingleAPI, payload));
    });
  });

  describe('addMultipleItemToCartECOMSingleAPI', () => {
    it('handle error in addMultipleItemToCartECOMSingleAPI', () => {
      const payload = {
        productItemsInfo: [
          {
            storeLocId: '345',
            isBoss: true,
            quantity: '1',
            skuInfo: { skuId: 'skuId1', variantId: 'variantId', variantNo: 'variantNo' },
          },
          {
            storeLocId: '345',
            isBoss: true,
            quantity: '1',
            skuInfo: { skuId: 'skuId2', variantId: 'variantId', variantNo: 'variantNo' },
          },
        ],
      };
      const formattedPayload = {
        catalogId: '111112',
        storeId: '11232',
        langId: 'en',
        orderId: '.',
        field2: '0',
        requesttype: 'ajax',
        catEntryId: ['skuId1', 'skuId2'],
        quantity: ['skuId1:1', 'skuId2:1'],
        'calculationUsage[]': '-7',
        externalId: '',
        multipackCatentryId: null,
      };

      const addedToBagSaga = addMultipleItemToCartECOMSingleAPI(payload);
      expect(addedToBagSaga.next().value).toEqual(put(setLoaderState(true)));
      addedToBagSaga.next();
      const paramsArray = getParams(payload);
      expect(paramsArray).toEqual(formattedPayload);
      addedToBagSaga.next();
      addedToBagSaga.next();
      addedToBagSaga.next();

      const atbError = { errMsg: 'ERROR' };

      addedToBagSaga.throw(atbError);
      addedToBagSaga.next();
      expect(addedToBagSaga.next().value).toEqual(put(AddToCartMultipleItemError(atbError)));
    });
  });

  describe('openAddedToBagModal', () => {
    it('should call makeNavigateXHrCall', () => {
      const pageName = 'products';
      const gen = openAddedToBagModal([], 1);
      expect(gen.next().value).toEqual(select(getPageName));
      gen.next();
      gen.next();
      gen.next();
      gen.next();
      gen.next();
      const next = gen.next(pageName);
      expect(next.value).toEqual(
        call(makeNavigateXHrCall, {
          prevCartCount: 1,
        })
      );
    });
    it('should dispatch resetBagLoadingState', () => {
      const pageName = 'cart';
      const gen = openAddedToBagModal([], 1);
      expect(gen.next().value).toEqual(select(getPageName));
      gen.next();
      gen.next();
      gen.next();
      gen.next();
      const next = gen.next(pageName);
      expect(next.value).toEqual(put(BAG_PAGE_ACTIONS.resetBagLoadingState(false)));
    });
  });

  describe('getAllCatId', () => {
    it('should return catIds', () => {
      const params = '123, 546, 567';
      expect(getAllCatId(params)).toEqual('123, 546');
    });
  });

  describe('getErrorMessage', () => {
    it('should return error', () => {
      expect(
        getErrorMessage({
          errorCode: null,
          errorMessage:
            'The command is unable to determine an appropriate fulfillment center for an OrderItem.',
          errorMessageKey: '_API_CANT_RESOLVE_FFMCENTER',
          errorMessageParam: [
            '_API_CANT_RESOLVE_FFMCENTER',
            '',
            'partNumber:00889705034127',
            'invAvail=1',
          ],
          correctiveActionMessage: '',
          correlationIdentifier: '-7bc19355:17c130f6387:-7f6c',
          exceptionData: null,
          exceptionType: '0',
          originatingCommand: 'AjaxOrderChangeServiceItemAdd',
          systemMessage: '',
        })
      ).toEqual('ERROR');
    });
  });

  describe('getMpackTgas', () => {
    it('should return catIds', () => {
      expect(getMpackTgas('0002', 'pdp', '5', 'polo shirt', '1234')).toEqual({
        dimension124: 'Mpack Pdp-Mpack',
        dimension125: '5pack',
        dimension199: 'Multipack',
        name: 'polo shirt',
        skuId: '1234',
      });
    });
  });

  describe('filterItems', () => {
    it('should return filterItems', () => {
      const allNewMultiPack = [
        {
          TCPMultiPackUSStore: false,
          name: 'WHITE',
          variants: [
            {
              v_variant: '3000935001',
              v_listprice: 12.95,
              v_offerprice: 12.95,
              TCPWebOnlyFlagUSStore: '1',
              categoryPath2_catMap: [
                '47511>49007|GIRL>School Uniforms',
                '47511>49012|GIRL>Girls Tops 5thNov2',
                '484507>484508|Uniform Shop>Polos',
                '47511>454001|GIRL>Online Only',
                '47511>478019|GIRL>Uniform Extended Sizes',
              ],
              categoryPath3_catMap: [
                '47511>49007>479504|GIRL>School Uniforms>Uniform Extended Sizes',
                '47511>49007>478507|GIRL>School Uniforms>Polos',
                '47511>49012>464013|GIRL>Girls Tops 5thNov2>Polo Shirts',
              ],
              score: 0.5,
              vId: '3000935_10_00193511074261',
            },
          ],
          prodpartno: '3000935_10',
          TCPMultiPackReferenceUSStore: false,
        },
      ];
      const allMultiPack = [
        {
          TCPMultiPackUSStore: false,
          name: 'ROSE DUST',
          variants: [
            {
              v_variant: '3000935001',
              v_listprice: 12.95,
              v_offerprice: 12.95,
              categoryPath2_catMap: [
                '47511>49007|GIRL>School Uniforms',
                '47511>49012|GIRL>Girls Tops 5thNov2',
                '484507>484508|Uniform Shop>Polos',
              ],
              categoryPath3_catMap: [
                '47511>49007>478507|GIRL>School Uniforms>Polos',
                '47511>49012>464013|GIRL>Girls Tops 5thNov2>Polo Shirts',
              ],
              score: 0.34764153,
              vId: '3001569_1301_00193511087742',
            },
          ],
          prodpartno: '3001569_1301',
          TCPMultiPackReferenceUSStore: false,
        },
      ];

      const skuInfo = {
        skuId: '1234093',
        imageUrl: '3001569/3001569_32FJ.jpg',
        color: {
          name: 'REGAL VIOLET',
          imagePath: null,
          family: 'PURPLE',
          swatchImage: '3001569_32FJ_swatch.jpg',
        },
        variantId: '00193511087902',
        variantNo: '3001569017',
        unbxdProdId: '3001569_32FK',
        productId: '3001569_32FJ',
        fit: null,
        size: 'XL (14)',
      };

      const partIdInfo = ['3000935_10#5'];

      expect(filterItems(allNewMultiPack, allMultiPack, skuInfo, partIdInfo)).toEqual('');
    });
  });

  describe('isLoyaltyServiceEnabled', () => {
    it('should return isLoyaltyServiceEnabled', () => {
      const params = { isBrierleyWorking: false, pointsApi: 'loyalty' };
      expect(isLoyaltyServiceEnabled(params)).toEqual(true);
    });
  });

  describe('checkLoyaltyNAFlag', () => {
    it('should return checkLoyaltyNAFlag', () => {
      const params = { isBrierleyWorking: false, pointsApi: 'NA' };
      expect(checkLoyaltyNAFlag(params)).toEqual(true);
    });
  });
});
