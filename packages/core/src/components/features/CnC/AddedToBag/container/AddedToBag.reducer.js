// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import ADDEDTOBAG_CONSTANTS from '../AddedToBag.constants';

const initialState = fromJS({
  itemInfo: {},
  error: false,
  isOpenAddedToBag: false,
  pickupError: null,
  modalError: null,
  addedTobagMsg: false,
  multipleItemsError: null,
  showOutfitCarouselAddedCta: false,
  loyaltyAbTestFlag: false,
  pdpLargeImageUrlOnHover: '',
  payloadData: null,
  isFbtDisplayed: false,
  isFbtCompleted: false,
  fbtError: null,
  keepMinibagClose: false,
});

// eslint-disable-next-line complexity
const AddedToBagReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADDEDTOBAG_CONSTANTS.ADDED_TO_BAG_MSG:
      return state.set('addedTobagMsg', action.payload);
    case ADDEDTOBAG_CONSTANTS.SET_LOYALTY_AB_TEST:
      return state.set('loyaltyAbTestFlag', action.payload);
    case ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_BAG:
      return state.set('itemInfo', action.payload);
    case ADDEDTOBAG_CONSTANTS.OPEN_ADDED_TO_BAG:
      return state.set('isOpenAddedToBag', true).set('keepMinibagClose', true);
    case ADDEDTOBAG_CONSTANTS.UPDATE_MINIBAG_CLOSE_STATUS:
      return state.set('keepMinibagClose', action.payload);
    case ADDEDTOBAG_CONSTANTS.OPEN_ADDED_TO_BAG_MINIMAL:
      return state.set('isOpenAddedToBag', action.payload).set('payloadData', action.payload);
    case ADDEDTOBAG_CONSTANTS.CLOSE_ADDED_TO_BAG:
      return state.set('isOpenAddedToBag', false);
    case ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_BAG_ERROR:
      return state.set('error', action.payload).set('errorCatId', action.id);
    case ADDEDTOBAG_CONSTANTS.CLEAR_ADD_TO_BAG_ERROR_STATE:
      return state.set('error', null).set('errorCatId', null);
    case ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_PICKUP_ERROR:
      return state.set('pickupError', action.payload);
    case ADDEDTOBAG_CONSTANTS.CLEAR_ADD_TO_PICKUP_ERROR_STATE:
      return state.set('pickupError', null);
    case ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_FBT_ERROR:
      return state.set('fbtError', action.payload);
    case ADDEDTOBAG_CONSTANTS.CLEAR_ADD_TO_FBT_ERROR_STATE:
      return state.set('fbtError', null);
    case ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_BAG_MULTIPLE_ITEMS_ERROR:
      return state.set('multipleItemsError', action.payload);
    case ADDEDTOBAG_CONSTANTS.CLEAR_ADD_TO_BAG_MULTIPLE_ITEMS_ERROR_STATE:
      return state.set('multipleItemsError', null);
    case ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_BAG_BUTTON:
      return state.set('showAddedToBagCta', action.payload);
    case ADDEDTOBAG_CONSTANTS.SET_OUTFIT_CAROUSEL_BUTTON: {
      return state.set('showOutfitCarouselAddedCta', action.payload);
    }
    case ADDEDTOBAG_CONSTANTS.SET_MULTIPCAK_PRODUCT_TYPE: {
      return state.set('multipackProductType', action.payload);
    }
    case ADDEDTOBAG_CONSTANTS.SET_STICKY_FOOTER:
      return state.set('stickyFooter', action.payload);
    case ADDEDTOBAG_CONSTANTS.SET_PDP_LARGE_IMAGE_URL:
      return state.set('pdpLargeImageUrlOnHover', action.payload);
    case ADDEDTOBAG_CONSTANTS.SET_IS_MULTIPCAK_ITEM:
      return state.set('isMpackItem', action.payload);
    case ADDEDTOBAG_CONSTANTS.SET_BAG_ANALYTICS:
      return state.set('cartAnalyticsData', action.payload);
    case ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_CART_MODAL_ERROR:
      return state.set('modalError', action.payload).set('errorCatId', action.id);
    case ADDEDTOBAG_CONSTANTS.CLEAR_ADDED_TO_CART_MODAL_ERROR:
      return state.set('modalError', null).set('errorCatId', null);
    case ADDEDTOBAG_CONSTANTS.FBT_STATUS:
      return state.set('isFbtDisplayed', action.payload);
    case ADDEDTOBAG_CONSTANTS.IS_FBT_COMPLETE:
      return state.set('isFbtCompleted', action.payload);
    case ADDEDTOBAG_CONSTANTS.SET_FBT_FLAG:
      return state.set('isFbtEnabled', true);
    default:
      // TODO: currently when initial state is hydrated on browser, List is getting converted to an JS Array
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default AddedToBagReducer;
