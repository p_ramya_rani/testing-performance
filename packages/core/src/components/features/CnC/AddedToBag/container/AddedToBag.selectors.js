/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector, defaultMemoize } from 'reselect';
import { getLabelValue, getABtestFromState, parseBoolean } from '@tcp/core/src/utils';
import { fromJS } from 'immutable';
import { getIsBopisEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getCartOrderDetails } from '../../CartItemTile/container/CartItemTile.selectors';

export const getCheckoutLoaderState = (state) => {
  return state.PageLoader && state.PageLoader.checkoutLoader;
};

export const getAddedToBagLoaderState = (state) => {
  return state.PageLoader && state.PageLoader.addedToBagLoaderState;
};

export const getLoyaltyAPICallStatus = (state) => {
  return state.PageLoader && state.PageLoader.loyaltyAPICallStatus;
};

export const getATbAPICallStatus = (state) => {
  return state.PageLoader && state.PageLoader.aTbAPICallStatus;
};

export const getAddedToBagGhostLoaderState = (state) => {
  return state.PageLoader && state.PageLoader.addedToBagGhostLoaderState;
};

export const getAddedToBagGhostLoaderCompleteState = (state) => {
  return state.PageLoader && state.PageLoader.addedToBagGhostLoaderCompleteState;
};

export const addedToBagMsgSelector = (state) => {
  return state.AddedToBagReducer.get('addedTobagMsg');
};
export const loyaltyAbTestFlagSelector = (state) => {
  return state.AddedToBagReducer.get('loyaltyAbTestFlag');
};

export const getMultipackProductType = defaultMemoize((state) => {
  return state.AddedToBagReducer.get('multipackProductType');
});

export const getIsMpackItem = (state) => {
  return state.AddedToBagReducer.get('isMpackItem');
};

export const getBagAnalyticsData = (state) => {
  return state.AddedToBagReducer.get('cartAnalyticsData');
};

export const getEnableKeepShoppingCTA = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return parseBoolean(getAbtests.enableKeepShoppingCTA);
};

export const getShowBOPISPickupInstead = (state) => {
  const getAbtests = getABtestFromState(state.AbTest);
  return parseBoolean(getAbtests.showBOPISPickupInstead);
};

export const getEnableBOPISPickupInstead = createSelector(
  [getIsBopisEnabled, getShowBOPISPickupInstead],
  (isBopisEnabled, showBOPISPickupInstead) => isBopisEnabled && showBOPISPickupInstead
);

export const getPageType = (state) => {
  return state.pageData && state.pageData.pageType;
};

const itemInfoFunc = defaultMemoize((state) => state.AddedToBagReducer.get('itemInfo'));

const getOrderItemsCart = createSelector(getCartOrderDetails, (orderDetails) => {
  const items = [];
  if (orderDetails.size > 0) return orderDetails.get('orderItems');
  return items;
});

export const getAddedToBagData = createSelector(
  getOrderItemsCart,
  itemInfoFunc,
  (orderItems, itemInfo) => {
    let data = itemInfo.productItemsInfo ? itemInfo.productItemsInfo : itemInfo;
    if (itemInfo && data.length > 0) {
      data = data.map((item) => {
        const itemCopy = Object.assign({}, item);
        if (orderItems) {
          orderItems.forEach((orderItem) => {
            if (item.orderItemId === orderItem.getIn(['itemInfo', 'itemId'])) {
              itemCopy.listPrice = orderItem.getIn(['itemInfo', 'listPrice']);
              itemCopy.itemPoints = orderItem.getIn(['itemInfo', 'itemPoints']);
              itemCopy.itemPrice = orderItem.getIn(['itemInfo', 'listPrice']);
              itemCopy.quantity = orderItem.getIn(['itemInfo', 'quantity']);
              itemCopy.brand = orderItem.getIn(['productInfo', 'itemBrand']);
            }
          });
        }
        return itemCopy;
      });
    }
    if (orderItems) {
      orderItems.forEach((orderItem) => {
        if (data.orderItemId === orderItem.getIn(['itemInfo', 'itemId'], '')) {
          data.brand = orderItem.getIn(['productInfo', 'itemBrand'], '');
        }
      });
      return data;
    }
    return data;
  }
);

export const isOpenAddedToBag = (state) => {
  return state.AddedToBagReducer.get('isOpenAddedToBag');
};

export const isOpenAddedToBagMinimal = (state) => {
  return state.AddedToBagReducer.get('isOpenAddedToBag');
};

export const getPayloadData = (state) => {
  return state.AddedToBagReducer.get('payloadData');
};

export const getOrderItems = (state) => {
  return getCartOrderDetails(state) && getCartOrderDetails(state).get('orderItems');
};

export const getAddedToBagError = (state) => {
  return state.AddedToBagReducer.get('error');
};

export const getAddedToBagFbtError = (state) => {
  return state.AddedToBagReducer.get('fbtError');
};

export const getAddedToBagModalError = (state) => {
  return state.AddedToBagReducer.get('modalError');
};

export const getMultipleItemsAddedToBagError = (state) => {
  return state.AddedToBagReducer.get('multipleItemsError');
};

export const getAddedToPickupError = (state) => {
  return state.AddedToBagReducer.get('pickupError');
};

export const filterItemObject = (arr, searchedValue) => {
  const filteredValue = arr.filter((value) => {
    return (
      value.getIn(['itemInfo', 'itemId']) &&
      searchedValue.orderItemId &&
      value.getIn(['itemInfo', 'itemId']).toString() === searchedValue.orderItemId.toString()
    );
  });
  return filteredValue.get(0);
};

export const filterItemObjectFromArray = (arr, searchedValue) => {
  const filteredData = [];
  let lastItemsData;
  if (typeof arr !== 'undefined') {
    lastItemsData = searchedValue.map((lastItem) => {
      const filteredValue = arr.filter((value) => {
        return (
          value.getIn(['itemInfo', 'itemId']) &&
          lastItem.orderItemId &&
          value.getIn(['itemInfo', 'itemId']).toString() === lastItem.orderItemId.toString()
        );
      });
      const filteredValueParam = filteredValue.get(0);
      filteredData.push({
        itemPrice: filteredValueParam ? filteredValueParam.getIn(['itemInfo', 'offerPrice']) : 0,
        itemPoints: filteredValueParam ? filteredValueParam.getIn(['itemInfo', 'itemPoints']) : 0,
        quantity: filteredValueParam ? filteredValueParam.getIn(['itemInfo', 'quantity']) : 0,
      });
      return filteredData;
    });
    return {
      filteredPrice: lastItemsData[0].reduce((a, b) => a + (b.itemPrice || 0), 0),
      filteredProductItemPoints: lastItemsData[0].reduce((a, b) => a + (b.itemPoints || 0), 0),
      filteredProductQuantity: lastItemsData[0].reduce((a, b) => a + (b.quantity || 0), 0),
    };
  }
  return {};
};

export const getQuantityValue = (state) => {
  let quantity = '';
  const orderItems = getOrderItems(state);
  const lastAddedToBag = getAddedToBagData(state);
  if (orderItems && lastAddedToBag) {
    const lastAddedItem = filterItemObject(orderItems, lastAddedToBag);
    quantity = lastAddedItem && lastAddedItem.getIn(['itemInfo', 'quantity']);
  }
  return quantity;
};

export const getATBItemInfo = (state) => {
  return state.AddedToBagReducer.get('itemInfo');
};

export const getPrice = (state) => {
  let price = '';
  const orderItems = getOrderItems(state);
  const lastAddedToBag = getAddedToBagData(state);
  if (orderItems && lastAddedToBag) {
    const lastAddedItem = filterItemObject(orderItems, lastAddedToBag);
    price = lastAddedItem && lastAddedItem.getIn(['itemInfo', 'listPrice']);
  }
  return price;
};

export const getLabelsAddToActions = (state) => {
  return {
    otherPayOptions: getLabelValue(
      state.Labels,
      'lbl_other_pay_options_added_to_bag',
      'addedToBagModal',
      'global'
    ),
    lbl_or_added_to_bag: getLabelValue(
      state.Labels,
      'lbl_or_added_to_bag',
      'addedToBagModal',
      'global'
    ),
    viewBag: getLabelValue(state.Labels, 'lbl_cta_viewBag', 'addedToBagModal', 'global'),
    keepShopping: getLabelValue(state.Labels, 'lbl_cta_keepShopping', 'addedToBagModal', 'global'),
    keepShoppingRedesign: getLabelValue(
      state.Labels,
      'lbl_keep_shopping_redesign',
      'addedToBagModal',
      'global'
    ),
    weFoundTheseSimilarItems: getLabelValue(
      state.Labels,
      'lbl_we_found_these_similar_items',
      'addedToBagModal',
      'global'
    ),
    checkout: getLabelValue(state.Labels, 'lbl_cta_checkout', 'addedToBagModal', 'global'),
    continueCheckout: getLabelValue(
      state.Labels,
      'lbl_checkoutmodal_continueCheckout',
      'checkoutConfirmation',
      'global'
    ),
    confirmationText: getLabelValue(
      state.Labels,
      'lbl_checkoutmodal_confirmation',
      'checkoutConfirmation',
      'global'
    ),
    confirmationTextPaypal: getLabelValue(
      state.Labels,
      'lbl_checkoutmodal_paypalConfirmation',
      'checkoutConfirmation',
      'global'
    ),
    editConfirmationText: getLabelValue(
      state.Labels,
      'lbl_checkoutmodal_editConfirmation',
      'checkoutConfirmation',
      'global'
    ),
    backToBag: getLabelValue(
      state.Labels,
      'lbl_checkoutmodal_backToBag',
      'checkoutConfirmation',
      'global'
    ),
  };
};

const getOrderItemsDetails = (state) => {
  return state.get('orderItems');
};

const getOrderItemsData = (getOrderPointsSummary, lastAddedToBag) => {
  // Determine where to pick orderItems data AddedtoBag State or orderDetails State
  return getOrderItemsDetails(getOrderPointsSummary) &&
    ((lastAddedToBag.estimatedRewards === -1 &&
      lastAddedToBag.pointsApi &&
      lastAddedToBag.pointsApi !== 'NA') ||
      !lastAddedToBag.estimatedRewards)
    ? getOrderItemsDetails(getOrderPointsSummary)
    : fromJS(lastAddedToBag.orderItems);
};

const createPointSummaryObj = ({
  obj,
  productItemPoints,
  lastAddedItem,
  offerPrice,
  pointsApi,
}) => {
  let pointsSummary = {};
  if (lastAddedItem && offerPrice > 0) {
    pointsSummary = {
      itemPrice: offerPrice,
      itemPoints: productItemPoints,
      pointsToNextReward: obj.pointsToNextReward,
      userPoints: obj.estimatedRewards || 0,
      bagSubTotal: obj.grandTotal - obj.giftCardsTotal || 0,
      totalItems: obj.totalItems || 0,
      isBrierleyWorking: obj.isBrierleyWorking,
      cartTotalAfterPLCCDiscount: obj.cartTotalAfterPLCCDiscount,
      earnedReward: obj.earnedReward,
      orderSubTotalDiscount: obj.orderSubTotalDiscount,
      pointsApi,
    };
  }
  return pointsSummary;
};

export const getAddedToBagInterval = (state) => {
  return parseInt(state.session.siteDetails.ADDED_TO_BAG_MODAL_INTERVAL, 10) || 0;
};

const getUpdateCartFlag = (state) =>
  defaultMemoize(state.CartPageReducer.getIn(['uiFlags', 'updateCartComplete'], false));

export const getPointsSummary = createSelector(
  getCartOrderDetails,
  getAddedToBagData,
  getUpdateCartFlag,
  (getOrderPointsSummary, lastAddedToBag, updateCartFlag) => {
    const emptyObj = {};
    if (!updateCartFlag) return emptyObj;
    const ifmultipleAddedItems = Array.isArray(lastAddedToBag);
    let orderItems;
    if (ifmultipleAddedItems) {
      orderItems = getOrderItemsDetails(getOrderPointsSummary);
    } else {
      orderItems = getOrderItemsData(getOrderPointsSummary, lastAddedToBag);
    }
    let pointsSummary = {};
    let lastAddedItem;
    let offerPrice;
    let productItemPoints;

    if (orderItems) {
      if (ifmultipleAddedItems) {
        lastAddedItem = filterItemObjectFromArray(orderItems, lastAddedToBag);
        const { filteredPrice, filteredProductItemPoints } = lastAddedItem;
        offerPrice = filteredPrice;
        productItemPoints = filteredProductItemPoints;
      } else {
        lastAddedItem = filterItemObject(orderItems, lastAddedToBag);
        offerPrice = lastAddedItem && lastAddedItem.getIn(['itemInfo', 'offerPrice']);
        productItemPoints = lastAddedItem && lastAddedItem.getIn(['itemInfo', 'itemPoints']);
      }

      const obj = {
        totalItems: getOrderPointsSummary.get('totalItems'),
        grandTotal: getOrderPointsSummary.get('grandTotal'),
        giftCardsTotal: getOrderPointsSummary.get('giftCardsTotal'),
        isBrierleyWorking: getOrderPointsSummary.get('isBrierleyWorking'),
        cartTotalAfterPLCCDiscount: getOrderPointsSummary.get('cartTotalAfterPLCCDiscount'),
        earnedReward: getOrderPointsSummary.get('earnedReward'),
        orderSubTotalDiscount: getOrderPointsSummary.get('orderSubTotalDiscount'),
        pointsToNextReward: getOrderPointsSummary.get('pointsToNextReward'),
        estimatedRewards: getOrderPointsSummary.get('estimatedRewards'),
      };
      const { pointsApi } = lastAddedToBag;

      pointsSummary = createPointSummaryObj({
        obj,
        productItemPoints,
        lastAddedItem,
        offerPrice,
        pointsApi,
      });
    }
    return pointsSummary;
  }
);

export const getGrandTotal = (state) => {
  const orderItems = getCartOrderDetails(state);
  return orderItems && orderItems.get('grandTotal') ? orderItems.get('grandTotal').toFixed(2) : 0;
};

export const getStickyATBState = (state) => {
  return state.AddedToBagReducer && state.AddedToBagReducer.get('stickyFooter');
};
export const getPDPlargeImageColorSelector = (state) => {
  return state.AddedToBagReducer && state.AddedToBagReducer.get('pdpLargeImageUrlOnHover');
};

export const getPointsToNextReward = (state) => {
  return (
    (state.AddedToBagReducer &&
      state.AddedToBagReducer.getIn(['itemInfo', 'pointsToNextReward'])) ||
    0
  );
};

export const getLowInventoryMonetateExp = (state) => {
  return (
    (state.AddedToBagReducer &&
      state.AddedToBagReducer.getIn(['itemInfo', 'displayLowInvMonetateExp'])) ||
    false
  );
};

export const getFbtDisplayedState = (state) => {
  return state.AddedToBagReducer.get('isFbtDisplayed');
};

export const getIsFbtCompletedState = (state) => {
  return state.AddedToBagReducer.get('isFbtCompleted');
};

export const getIsFbtEnabledState = (state) => {
  return state.AddedToBagReducer.get('isFbtEnabled');
};

export const getIsKeepMinibagClose = (state) => {
  return state.AddedToBagReducer.get('keepMinibagClose');
};

const allLabels = (state) => state.Labels;

export const getAddedToBagLabels = createSelector(allLabels, (labels) => {
  let addToBagLabels = {};
  if (labels.global) {
    addToBagLabels = {
      colorLabel: getLabelValue(labels, 'lbl_info_color', 'addedToBagModal', 'global'),
      sizeLabel: getLabelValue(labels, 'lbl_info_size', 'addedToBagModal', 'global'),
      qtyLabel: getLabelValue(labels, 'lbl_info_Qty', 'addedToBagModal', 'global'),
      pickUpText: getLabelValue(
        labels,
        'lbl_bossBanner_headingDefault',
        'addedToBagModal',
        'global'
      ),
      simplyChooseText: getLabelValue(
        labels,
        'lbl_bossBanner_subHeadingDefault',
        'addedToBagModal',
        'global'
      ),
      noRushText: getLabelValue(labels, 'lbl_bossBanner_noRush', 'addedToBagModal', 'global'),
      price: getLabelValue(labels, 'lbl_info_price', 'addedToBagModal', 'global'),
      pointsYouCanEarn: getLabelValue(
        labels,
        'lbl_info_pointYouCanEarn',
        'addedToBagModal',
        'global'
      ),
      MPRPoints: getLabelValue(labels, 'lbl_info_mprPoint', 'addedToBagModal', 'global'),
      bagSubTotal: getLabelValue(labels, 'lbl_info_subTotal', 'addedToBagModal', 'global'),
      itemPoints: getLabelValue(labels, 'lbl_info_item_points', 'addedToBagModal', 'global'),
      totalRewardsInPoints: getLabelValue(
        labels,
        'lbl_info_totalRewardsInBag',
        'addedToBagModal',
        'global'
      ),
      addedToBagLabel: getLabelValue(labels, 'lbl_header_addedToBag', 'addedToBagModal', 'global'),
      totalNextRewards: getLabelValue(
        labels,
        'lbl_info_totalNextRewards',
        'addedToBagModal',
        'global'
      ),
      addedToBag: getLabelValue(labels, 'lbl_header_addedToBag', 'addedToBagModal', 'global'),
      giftDesign: getLabelValue(labels, 'lbl_info_giftDesign', 'addedToBagModal', 'global'),
      giftValue: getLabelValue(labels, 'lbl_info_giftValue', 'addedToBagModal', 'global'),
      continueShopping: getLabelValue(
        labels,
        'lbl_footer_continueShopping',
        'addedToBagModal',
        'global'
      ),
      viewBag: getLabelValue(labels, 'lbl_cta_viewBag', 'addedToBagModal', 'global'),
      keepShopping: getLabelValue(labels, 'lbl_cta_keepShopping', 'addedToBagModal', 'global'),
      checkout: getLabelValue(labels, 'lbl_cta_checkout', 'addedToBagModal', 'global'),
      close: getLabelValue(labels, 'lbl_aria_close', 'addedToBagModal', 'global'),
      overlayAriaText: getLabelValue(labels, 'lbl_aria_overlay_text', 'addedToBagModal', 'global'),
      points: getLabelValue(labels, 'lbl_info_points', 'addedToBagModal', 'global'),
      tcpBrandAlt: getLabelValue(labels, 'lbl_brandlogo_tcpAltText', 'accessibility', 'global'),
      gymBrandAlt: getLabelValue(labels, 'lbl_brandlogo_gymAltText', 'accessibility', 'global'),
      youMayAlsoLike: getLabelValue(labels, 'lbl_you_may_also_like', 'addedToBagModal', 'global'),
      headingStyleWith: getLabelValue(labels, 'lbl_heading_styleWith', 'addedToBagModal', 'global'),
      headingFbt: getLabelValue(labels, 'lbl_heading_fbt', 'addedToBagModal', 'global'),
      pickupInsteadHeading: getLabelValue(
        labels,
        'lbl_pickup_instead_heading',
        'addedToBagModal',
        'global'
      ),
      pickupInsteadHeadingSuccess: getLabelValue(
        labels,
        'lbl_pickup_instead_heading_success',
        'addedToBagModal',
        'global'
      ),
      pickupInsteadReady: getLabelValue(
        labels,
        'lbl_pickup_instead_ready',
        'addedToBagModal',
        'global'
      ),
      newPickupInsteadReady: getLabelValue(
        labels,
        'lbl_pickup_instead_ready_heading',
        'addedToBagModal',
        'global'
      ),
      pickupInsteadFreeOrder: getLabelValue(
        labels,
        'lbl_pickup_instead_free_order',
        'addedToBagModal',
        'global'
      ),
      pickupInsteadFromStore: getLabelValue(
        labels,
        'lbl_pickup_instead_from_store',
        'addedToBagModal',
        'global'
      ),
      pickupInsteadItemAdded: getLabelValue(
        labels,
        'lbl_pickup_instead_item_added',
        'addedToBagModal',
        'global'
      ),
      pickupInsteadBtnText: getLabelValue(
        labels,
        'lbl_pickup_instead_btn_text',
        'addedToBagModal',
        'global'
      ),
      pickupInsteadHeadingError: getLabelValue(
        labels,
        'lbl_pickup_instead_heading_error',
        'addedToBagModal',
        'global'
      ),
      pickupInsteadMsgError: getLabelValue(
        labels,
        'lbl_pickup_instead_msg_error',
        'addedToBagModal',
        'global'
      ),
      oopsItemUnavailable: getLabelValue(
        labels,
        'lbl_oops_item_unavailable',
        'addedToBagModal',
        'global'
      ),
      itemAddedToBag: getLabelValue(labels, 'lbl_item_added_to_bag', 'addedToBagModal', 'global'),
      itemsAddedToBag: getLabelValue(labels, 'lbl_items_added_to_bag', 'addedToBagModal', 'global'),
      itemsNotAddedToBag: getLabelValue(
        labels,
        'lbl_items_not_added_to_bag',
        'addedToBagModal',
        'global'
      ),
      itemNotAddedToBag: getLabelValue(
        labels,
        'lbl_item_not_added_to_bag',
        'addedToBagModal',
        'global'
      ),
      oopsAddedToBag: getLabelValue(labels, 'lbl_oops_added_to_bag', 'addedToBagModal', 'global'),
      bagSubTotalRedesign: getLabelValue(
        labels,
        'lbl_bag_sub_total_redesign',
        'addedToBagModal',
        'global'
      ),
      rewardLoyaltyPart1: getLabelValue(
        labels,
        'lbl_reward_loyalty_part1_redesign',
        'addedToBagModal',
        'global'
      ),
      rewardLoyaltyPart2: getLabelValue(
        labels,
        'lbl_reward_loyalty_part2_redesign',
        'addedToBagModal',
        'global'
      ),
      pointsLoyaltyPart1: getLabelValue(
        labels,
        'lbl_points_loyalty_part1_redesign',
        'addedToBagModal',
        'global'
      ),
      pointsLoyaltyPart2: getLabelValue(
        labels,
        'lbl_points_loyalty_part2_redesign',
        'addedToBagModal',
        'global'
      ),
    };
  } else {
    addToBagLabels = {
      colorLabel: '',
      sizeLabel: '',
      qtyLabel: '',
      pickUpText: '',
      simplyChooseText: '',
      noRushText: '',
      price: '',
      pointsYouCanEarn: '',
      MPRPoints: '',
      bagSubTotal: '',
      itemPoints: '',
      totalRewardsInPoints: '',
      totalNextRewards: '',
      addedToBag: '',
      giftDesign: '',
      giftValue: '',
      continueShopping: '',
      viewBag: '',
      checkout: '',
      close: getLabelValue(labels, 'lbl_aria_close', 'addedToBagModal', 'global'),
      overlayAriaText: getLabelValue(labels, 'lbl_aria_overlay_text', 'addedToBagModal', 'global'),
      points: '',
      youMayAlsoLike: '',
    };
  }
  return addToBagLabels;
});
