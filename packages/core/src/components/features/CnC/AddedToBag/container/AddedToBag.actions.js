// 9fbef606107a605d69c0edbcd8029e5d
import ADDEDTOBAG_CONSTANTS from '../AddedToBag.constants';
import BAGPAGE_CONSTANTS from '../../BagPage/BagPage.constants';

export const addToCartEcom = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM,
  payload,
});

export const addToCartEcomNewDesign = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM_NEW,
  payload,
});

export const addToCartEcomAtbLoadedNewDesign = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM_NEW_ATB_LOADED,
  payload,
});

export const addedTobagMsg = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.ADDED_TO_BAG_MSG,
  payload,
});
export const setLoyaltyABTestFlag = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_LOYALTY_AB_TEST,
  payload,
});
export const addMultipleItemsToCartEcom = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.ADD_MULTIPLE_ITEMS_CART_ECOM,
  payload,
});

export const addItemToCartBopis = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.ADD_TO_CART_BOPIS,
  payload,
});

export const AddToCartError = (payload, id) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_BAG_ERROR,
  payload,
  id,
});

export const AddToPickupError = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_PICKUP_ERROR,
  payload,
});

export const SetAddedToBagData = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_BAG,
  payload,
});

export const setAddedToBagDataLocal = (payload) => ({
  type: BAGPAGE_CONSTANTS.UPDATE_CART_LOCAL_DATA,
  payload,
});

export const openAddedToBag = () => ({
  type: ADDEDTOBAG_CONSTANTS.OPEN_ADDED_TO_BAG,
});

export const openAddedToBagMinimal = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.OPEN_ADDED_TO_BAG_MINIMAL,
  payload,
});

export const closeAddedToBag = () => ({
  type: ADDEDTOBAG_CONSTANTS.CLOSE_ADDED_TO_BAG,
});

export const clearAddToPickupErrorState = () => ({
  type: ADDEDTOBAG_CONSTANTS.CLEAR_ADD_TO_PICKUP_ERROR_STATE,
});

export const clearAddToBagErrorState = () => ({
  type: ADDEDTOBAG_CONSTANTS.CLEAR_ADD_TO_BAG_ERROR_STATE,
});

export const AddToCartMultipleItemError = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_BAG_MULTIPLE_ITEMS_ERROR,
  payload,
});
export const clearAddToCartMultipleItemErrorState = () => ({
  type: ADDEDTOBAG_CONSTANTS.CLEAR_ADD_TO_BAG_MULTIPLE_ITEMS_ERROR_STATE,
});

export const setAddedToBagButton = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_BAG_BUTTON,
  payload,
});

export const setOutfitCarouselAddedButton = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_OUTFIT_CAROUSEL_BUTTON,
  payload,
});

export const setMultipackProductType = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_MULTIPCAK_PRODUCT_TYPE,
  payload,
});

export const setStickyFooter = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_STICKY_FOOTER,
  payload,
});
export const setPDPLargeImageURL = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_PDP_LARGE_IMAGE_URL,
  payload,
});

export const setIsMultipackItem = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_IS_MULTIPCAK_ITEM,
  payload,
});
export const setBagAnalytics = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_BAG_ANALYTICS,
  payload,
});

export const addToBagModalError = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_CART_MODAL_ERROR,
  payload,
});

export const clearAddToBagModalError = () => ({
  type: ADDEDTOBAG_CONSTANTS.CLEAR_ADDED_TO_CART_MODAL_ERROR,
});

export const updateFbtViewStatus = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.FBT_STATUS,
  payload,
});

export const setFbtComplete = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.IS_FBT_COMPLETE,
  payload,
});
export const addMultipleItemsToCartEcomNew = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.ADD_MULTIPLE_ITEMS_CART_ECOM_NEW,
  payload,
});
export const setFBTFlag = (payload) => {
  return {
    payload,
    type: ADDEDTOBAG_CONSTANTS.SET_FBT_FLAG,
  };
};
export const setAddToCartFbtError = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.SET_ADDED_TO_FBT_ERROR,
  payload,
});
export const clearAddToCartFbtError = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.CLEAR_ADD_TO_FBT_ERROR_STATE,
  payload,
});
export const updateMiniBagCloseStatus = (payload) => ({
  type: ADDEDTOBAG_CONSTANTS.UPDATE_MINIBAG_CLOSE_STATUS,
  payload,
});
