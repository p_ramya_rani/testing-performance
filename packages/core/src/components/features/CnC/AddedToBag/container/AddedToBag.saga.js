/* eslint-disable complexity, sonarjs/cognitive-complexity, max-lines */
/* eslint-disable max-statements */
/* eslint-disable no-underscore-dangle */
// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select, delay } from 'redux-saga/effects';
import {
  isMobileApp,
  isCanada,
  fireEnhancedEcomm,
  getSessionStorage,
  setSessionStorage,
  isClient,
} from '@tcp/core/src/utils';
import { trackClickWithData } from '@tcp/core/src/analytics/actions';
import { setProp } from '@tcp/core/src/analytics/utils';
import { getClickAnalyticsData, getPageData } from '@tcp/core/src/analytics/Analytics.selectors';
import { fetchAbTestData } from '@tcp/core/src/components/common/molecules/abTest/abTest.actions';
import {
  setLoaderState,
  setGhostLoaderState,
  setGhostLoaderStateComplete,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import {
  closeQuickViewModal,
  setQuickViewProductDetailPageTitle,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import {
  getIsSuppressGetOrderDetails,
  getIsAvoidGetOrderDetails,
  getABTestAddToBagDrawer,
  getCartsSessionStatus,
  getOptimisticAddBagModal,
  getIsSingleAddToBagAPIEnabled,
  getIsNewQVEnabled,
  getIsPersistPointsApiEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getPageName } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import {
  getLoyaltyDetail,
  updateLoyaltyPoints,
} from '@tcp/core/src/components/features/CnC/LoyaltyBanner/container/Loyalty.actions';
import { MULTI_PACK_TYPE } from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import { getBagAnalyticsData } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import { setCartsSessionStatus } from '../../../../../reduxStore/actions';
import { toggleCheckoutRouting } from '../../Checkout/container/Checkout.action';
import ADDEDTOBAG_CONSTANTS from '../AddedToBag.constants';
import {
  addCartEcomItem,
  addCartBopisItem,
  addMultipleProductsInEcom,
} from '../../../../../services/abstractors/CnC/AddedToBag';
import {
  AddToCartError,
  AddToCartMultipleItemError,
  clearAddToCartMultipleItemErrorState,
  SetAddedToBagData,
  setAddedToBagDataLocal,
  openAddedToBag,
  clearAddToBagErrorState,
  clearAddToPickupErrorState,
  AddToPickupError,
  setAddedToBagButton,
  setOutfitCarouselAddedButton,
  addedTobagMsg,
  setLoyaltyABTestFlag,
  setMultipackProductType,
  setIsMultipackItem,
  setBagAnalytics,
  setFbtComplete,
  setAddToCartFbtError,
  clearAddToCartFbtError,
} from './AddedToBag.actions';
import BAG_PAGE_ACTIONS, { updateMiniBagFlag } from '../../BagPage/container/BagPage.actions';
// eslint-disable-next-line import/no-cycle
import { getOrderDetailSaga } from '../../BagPage/container/BagPage.saga';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import BAGPAGE_CONSTANTS from '../../BagPage/BagPage.constants';
import { getAPIConfig } from '../../../../../utils';
import { getIsGuest } from '../../../account/User/container/User.selectors';
import { navigateXHRAction } from '../../../account/NavigateXHR/container/NavigateXHR.action';
import { getCartItemCount } from '../../../../../utils/cookie.util';

import { getAddedToBagGhostLoaderCompleteState } from './AddedToBag.selectors';
import { getMultiPackStyleType } from '../views/AddedToBag.utils';

const {
  updateCartCount,
  setOrderId,
  updateCartPriceData,
  updateOrderTypeData,
  resetBagLoadingState,
  getOrderDetails,
  setCartUpdateComplete,
} = BAG_PAGE_ACTIONS;

const getDefaultError = (err, errorMapping) => {
  return (
    (err && err.errorCode && errorMapping[err.errorCode]) ||
    (err && err.errorMessage && err.errorMessage._error) ||
    (err && err.errorCodes && errorMapping[err.errorCodes]) ||
    (errorMapping && errorMapping.DEFAULT) ||
    'ERROR'
  );
};

export const getErrorMessage = (err, errorMapping, originPage) => {
  if (
    err &&
    err.errorCodes === ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER &&
    err &&
    err.errorMessageParam
  ) {
    let errorAvailString;
    const errorMessageParamArray = err && err.errorMessageParam;
    if (errorMessageParamArray) {
      errorAvailString =
        errorMessageParamArray &&
        errorMessageParamArray.filter((errorItem) => {
          return errorItem && errorItem.includes('=');
        });
    }

    const errorAvailStringValue = errorAvailString && errorAvailString[0];
    const inAvailQuantity = errorAvailStringValue && errorAvailStringValue.split('=')[1];
    const inAvailQuantityValue = parseInt(inAvailQuantity, 10);
    if (inAvailQuantityValue <= 0) {
      return errorMapping && errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER_OOS];
    }
    if (inAvailQuantityValue > 0) {
      return `${
        errorMapping &&
        errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER_PARTIAL_INSTOCK_MSG1]
      } ${inAvailQuantity} ${
        errorMapping &&
        errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER_PARTIAL_INSTOCK_MSG2]
      }`;
    }
  }

  if (err && err.errorCodes === ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER) {
    return originPage === 'shopping bag'
      ? errorMapping && errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER]
      : errorMapping && errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER_SHORT];
  }
  return getDefaultError(err, errorMapping);
};

const getCM2Event = () => {
  const analyticsObj = JSON.parse(getSessionStorage('AnalyticsObj')) || {};
  const { metric2 } = analyticsObj;
  if (!metric2) {
    setSessionStorage({
      key: 'AnalyticsObj',
      value: JSON.stringify({ ...analyticsObj, metric2: true }),
    });
    return { cm2: '1' };
  }
  return { cm2: '0' };
};

export const getMpackTgas = (
  multiPackProductType,
  originPage,
  selectedMultipack,
  productName,
  sku
) => {
  let mpackTags = {};
  let cd124 = '';
  const cd125 = `${selectedMultipack}${ADDEDTOBAG_CONSTANTS.GA_ANAYTICS.PACK}`;
  const cd199 = getMultiPackStyleType(multiPackProductType);
  const gaAnalyticsConfKey = isMobileApp() ? 'GA_ANAYTICS_APP' : 'GA_ANAYTICS';
  if (multiPackProductType) {
    switch (originPage) {
      case 'pdp':
        cd124 =
          multiPackProductType === MULTI_PACK_TYPE.MULTIPACK
            ? ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PDP_MULTIPACK
            : ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PDP_SINGLE;
        break;
      case 'spdp':
        cd124 = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].SEARCH_PDP_MPACK;
        break;
      case 'slp':
        cd124 = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].SLP_MPACK;
        break;
      case 'plp':
        cd124 = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PLP_MPACK;
        break;
      default:
        cd124 = '';
    }
    mpackTags = {
      dimension124: cd124,
      dimension125: cd125,
      dimension199: cd199,
      ...(productName && { name: productName }),
      ...(sku && { skuId: sku }),
    };
  }
  return mpackTags;
};

const getMpackPageType = (multiPackProductType, originPage) => {
  const gaAnalyticsConfKey = isMobileApp() ? 'GA_ANAYTICS_APP' : 'GA_ANAYTICS';
  let mpackPageType = '';
  if (multiPackProductType) {
    switch (originPage) {
      case 'pdp':
        mpackPageType =
          multiPackProductType === MULTI_PACK_TYPE.MULTIPACK
            ? ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PDP_MULTIPACK
            : ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PDP_SINGLE;
        break;
      case 'spdp':
        mpackPageType = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].SEARCH_PDP_MPACK;
        break;
      case 'slp':
        mpackPageType = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].SLP_MPACK;
        break;
      case 'plp':
        mpackPageType = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PLP_MPACK;
        break;
      default:
        mpackPageType = '';
    }
  }
  return mpackPageType;
};

export function* getDimension2Value(product) {
  const { dimension92 } = product || {};

  const path = isClient() ? window.location.href : null;
  if (path) {
    if (path.includes('/outfit/')) return 'outfit';
    if (path.includes('content')) return 'content page';
    if (path.includes('/b/')) return 'bundle';
  }
  if (dimension92) return Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS;

  const pageData = yield select(getPageData);
  return (pageData && pageData.pageType) || 'browse';
}

const getFormattedProducts = (products, multiPackProductType, originPage, selectedMultipack) => {
  if (!products) return null;
  const { isOutfitPage } = products;
  const outfitArr = isOutfitPage ? { metric77: '1' } : {};
  const { dimension20: cd20, dimension21: cd21, dimension92: cd92, dimension2: cd2 } = products;
  const mpackTags = getMpackTgas(multiPackProductType, originPage, selectedMultipack);

  return {
    id: products.colorId,
    price: products.price,
    brand: products.brand,
    variant: products.prodColor,
    category: products.category,
    quantity: products.quantity,
    metric61: products.cartPrice,
    metric85: products.price,
    dimension2: cd2 || getDimension2Value(products) || 'browse',
    dimension20: cd20 || '',
    dimension21: cd21 || '',
    dimension92: cd92 || '',
    dimension35: products.brand,
    dimension63: `${products.price}${products.CurrCurrency} ${products.listPrice}${products.CurrCurrency}`,
    dimension67: products.pricingState,
    dimension71: products.rating,
    dimension72: products.productFeatures,
    dimension77: products.prodColor,
    dimension80: products.category,
    dimension83: products.prodGender || products.gender,
    dimension88: products.size,
    dimension89: products.colorId,
    dimension95: products.sku,
    ...mpackTags,
    ...outfitArr,
  };
};

const fireAnalyticsCall = (prodArr, product) => {
  if (fireEnhancedEcomm) {
    fireEnhancedEcomm({
      eventName: 'Add To Cart',
      productsObj: [prodArr],
      eventType: 'add',
      pageName: product.fromPageName,
      currCode: product.CurrCurrency,
      ...getCM2Event(),
    });
  }
};

export function* logAddToBagEvent({
  quantity,
  cartPrice,
  multiPackProductType,
  originPage,
  selectedMultipack = 1,
  addedFromSBP,
}) {
  const clickAnalyticsData = yield select(getClickAnalyticsData);
  const { departmentList = '', categoryList = '', listingSubCategory = '' } = clickAnalyticsData;
  const breadcrumbs = [departmentList, categoryList, listingSubCategory];
  const product = clickAnalyticsData.products && clickAnalyticsData.products[0];
  const isMobile = isMobileApp();
  if (!isMobile && product) {
    product.dimension2 = yield call(getDimension2Value, product);
  }

  if (product) {
    product.quantity = parseInt(quantity, 10);
    product.cartPrice = parseFloat(cartPrice);
    product.mpackPageType = getMpackPageType(multiPackProductType, originPage);
    product.mpackValue = `${selectedMultipack}${ADDEDTOBAG_CONSTANTS.GA_ANAYTICS.PACK}`;
    if (isMobile && addedFromSBP) product.sbp = true;
  }
  const updatedClickAnalyticsData = { ...clickAnalyticsData };
  const breadcrumbString = breadcrumbs.reduce((bdString, current) => {
    return current ? `${bdString}${bdString ? ':' : ''}${current}` : bdString;
  }, '');
  const prodArr = getFormattedProducts(
    product,
    multiPackProductType,
    originPage,
    selectedMultipack
  );
  setProp('prop23', breadcrumbString);
  yield put(
    trackClickWithData(updatedClickAnalyticsData, {
      name: 'add_to_cart',
      module: 'browse',
    })
  );

  if (isMobile) {
    const cartsSessionStatus = yield select(getCartsSessionStatus);
    if (cartsSessionStatus) {
      yield put(setCartsSessionStatus(0));
    }
  }
  if (product) {
    fireAnalyticsCall(prodArr, product);
  }
}

export function* logMultiAddToBagEvent({ cartPrice, originPage, products }) {
  const { selectedCategoryId } = products[0];
  const productsNew = products.map((product, i) => {
    const { productId, offerPrice, size, skuId, TCPStyleQTY, TCPStyleType, color } =
      product.skuInfo;
    const {
      productName,
      fromPage,
      outfitId,
      brand,
      ratings,
      reviewsCount,
      addedFromSBP,
      quantity,
      listPrice,
      title,
      priceCurrency,
    } = product;
    return {
      isOutfitPage: title === 'COMPLETE THE LOOK',
      fromPageName: fromPage,
      CurrCurrency: priceCurrency,
      color: [color.name],
      colorId: productId,
      extPrice: listPrice,
      id: productId,
      listPrice,
      name: productName,
      outfitId,
      paidPrice: offerPrice,
      plpClick: false,
      position: i + 1,
      price: offerPrice,
      prodBrand: brand,
      rating: ratings,
      recsPageType: fromPage,
      recsProductId: productId,
      recsType: `${title === 'COMPLETE THE LOOK' ? 'CTL' : title}`,
      reviews: reviewsCount,
      searchClick: false,
      size,
      sku: skuId,
      quantity: parseInt(quantity, 10),
      cartPrice: parseFloat(cartPrice),
      mpackPageType: getMpackPageType(TCPStyleType, originPage),
      mpackValue: `${TCPStyleQTY || 1}${ADDEDTOBAG_CONSTANTS.GA_ANAYTICS.PACK}`,
      sbp: addedFromSBP || false,
      pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
    };
  });
  const eventsData = ['scAdd', 'event61'];
  const cartsSessionStatus = yield select(getCartsSessionStatus);
  if (cartsSessionStatus) {
    eventsData.push('scOpen');
    yield put(setCartsSessionStatus(0));
  }

  const clickAnalyticsData = {
    customEvents: eventsData,
    CartAddProductId: selectedCategoryId,
    linkName: 'cart add',
    pageName: 'outfit',
    pageSection: 'outfit',
    pageShortName: 'outfit',
    pageSubSection: 'outfit',
    pageType: 'outfit',
    products: productsNew,
  };

  const updatedClickAnalyticsData = { ...clickAnalyticsData };

  yield put(
    trackClickWithData(updatedClickAnalyticsData, {
      name: 'add_to_cart',
      module: 'browse',
    })
  );
}

const getPrevCartCount = (prevCartCount) => {
  return parseInt(prevCartCount || 0, 10);
};

export function* makeNavigateXHrCall(obj) {
  const { prevCartCount } = obj || {};
  const isGuestUser = yield select(getIsGuest);
  if (isGuestUser && !prevCartCount) {
    yield put(navigateXHRAction());
  }
}

export function* isSuppressGetOrderDetails() {
  return yield select(getIsSuppressGetOrderDetails);
}

export function* isAvoidGetOrderDetails() {
  return yield select(getIsAvoidGetOrderDetails);
}

export function* isgetABTestAddToBagDrawer() {
  return yield select(getABTestAddToBagDrawer);
}
export function* isOptimisticAddBagModal() {
  return yield select(getOptimisticAddBagModal);
}
export function* isNewQVEnabled() {
  return yield select(getIsNewQVEnabled);
}

const getExternalID = (itemId, wishlistItemId) => {
  return itemId || wishlistItemId || '';
};

export const isLoyaltyServiceEnabled = (res, isLoyaltyPointsEnabled) => {
  if (isLoyaltyPointsEnabled) return true;
  const { isBrierleyWorking, pointsApi } = res;
  return (
    res &&
    !isBrierleyWorking &&
    pointsApi &&
    (pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.BRIERLEY ||
      pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.LOYALTY)
  );
};

export const checkLoyaltyNAFlag = (res) => {
  return (
    res &&
    !res.isBrierleyWorking &&
    res.pointsApi &&
    res.pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.NA
  );
};

export const getAllCatId = (newCatId) => {
  const singlegetNewcatIdPop = newCatId && newCatId.split(',');
  const newSet = new Set(singlegetNewcatIdPop);
  const uniqueCatIdSet = [...newSet];
  uniqueCatIdSet.pop();
  return uniqueCatIdSet.join();
};

export const filterItems = (getAllNewMultiPack, getAllMultiPack, skuInfo, partIdInfo) => {
  let newCatId = '';
  let getpart;
  let isSingleProduct;
  const getAllMultiPackNumber = [];
  for (let index = 0; index < getAllMultiPack.length; index += 1) {
    if (
      (skuInfo.productPartNoOfCurrentColor
        ? getAllMultiPack[index].prodpartno === skuInfo.productPartNoOfCurrentColor
        : getAllMultiPack[index].name.toLowerCase() === skuInfo.color.name.toLowerCase()) &&
      getAllMultiPack[index].TCPMultiPackReferenceUSStore
    ) {
      getAllMultiPackNumber.push(getAllMultiPack[index].TCPMultiPackReferenceUSStore);
    }
  }

  for (let j = 0; j < getAllNewMultiPack.length; j += 1) {
    for (let k = 0; k < getAllNewMultiPack[j].variants.length; k += 1) {
      if (
        getAllMultiPackNumber.length > 0 &&
        skuInfo &&
        getAllNewMultiPack[j].variants[k].v_tcpsize &&
        (skuInfo.size === getAllNewMultiPack[j].variants[k].v_tcpsize.split('_')[1] ||
          skuInfo.size === getAllNewMultiPack[j].variants[k].v_tcpsize) &&
        getAllMultiPackNumber[0].indexOf(`${getAllNewMultiPack[j].prodpartno}#`) > -1
      ) {
        /* eslint-disable camelcase */
        const { v_item_catentry_id } = getAllNewMultiPack[j].variants[k];
        if (getAllMultiPackNumber[j] && partIdInfo[j].split(',').length > 1) {
          getpart = partIdInfo[j].split(',');
        }

        if (getAllMultiPackNumber[0].includes(',')) {
          getpart = getAllMultiPackNumber[0].split(',');
        } else if (getAllNewMultiPack[j].prodpartno === getAllMultiPackNumber[0].split('#')[0]) {
          getpart = getAllMultiPackNumber;
          isSingleProduct = true;
        }
        const results =
          getpart &&
          getpart.find((element) => element.indexOf(getAllNewMultiPack[j].prodpartno) === 0);
        const getNewCatIdVal = `${v_item_catentry_id}_${
          isSingleProduct ? getpart[0].split('#')[1] : results && results.split('#')[1]
        },`;
        if (isSingleProduct) {
          newCatId += getNewCatIdVal;
          return getAllCatId(newCatId);
        }

        newCatId += getNewCatIdVal;
      }
    }
  }
  return getAllCatId(newCatId);
};

export const filterMultiPackVariant = (getAllMultiPack, skuInfo) => {
  return getAllMultiPack.filter((product) => {
    const { TCPMultiPackReferenceUSStore } = product;
    return skuInfo.color.name.toLowerCase() === product.name.toLowerCase()
      ? TCPMultiPackReferenceUSStore
      : null;
  });
};

const getCatEntryIdForMultiAdd = (productItemsInfo) => {
  return productItemsInfo.map((product) => {
    const { skuId: catEntryId } = product.skuInfo;
    return catEntryId;
  });
};

const getQuantityForMultiAdd = (productItemsInfo) => {
  return productItemsInfo.map((product) => {
    const { skuId: catEntryId } = product.skuInfo;
    return `${catEntryId}:${product.quantity.toString()}`;
  });
};

export const getParams = (payload) => {
  const isMultipleAdd = payload.productItemsInfo && payload.productItemsInfo.length;
  const newPayload = isMultipleAdd ? payload.productItemsInfo[0] : payload;
  const {
    skuInfo,
    getAllMultiPack,
    getAllNewMultiPack,
    quantity,
    multipackProduct,
    multiPackItems,
    itemId,
    wishlistItemId,
    partIdInfo,
  } = newPayload;
  const isMultiPackVariant = !!getAllMultiPack && filterMultiPackVariant(getAllMultiPack, skuInfo);
  const { storeId, langId, catalogId } = getAPIConfig();
  const multipackCatentryId = multiPackItems || null;
  const multipackIds = multipackProduct || null;
  const apiConfigParams = {
    catalogId,
    storeId,
    langId,
  };
  const getCatId = !multipackIds
    ? null
    : filterItems(getAllNewMultiPack, getAllMultiPack, skuInfo, partIdInfo);

  const categoryEntryId = isMultipleAdd
    ? getCatEntryIdForMultiAdd(payload.productItemsInfo)
    : skuInfo.skuId;
  const productQuantity =
    isMultipleAdd > 1 ? getQuantityForMultiAdd(payload.productItemsInfo) : quantity.toString();
  return {
    ...apiConfigParams,
    orderId: '.',
    field2: '0',
    requesttype: 'ajax',
    catEntryId: categoryEntryId,
    quantity: productQuantity,
    'calculationUsage[]': '-7',
    externalId: getExternalID(itemId, wishlistItemId),
    multipackCatentryId:
      isMultiPackVariant &&
      isMultiPackVariant[0] &&
      isMultiPackVariant[0].TCPMultiPackUSStore &&
      getCatId
        ? getCatId
        : multipackCatentryId,
  };
};

function* updateBagActions(res, fromStyleWith) {
  const loyaltyPayload = {
    isBrierleyWorking: res && res.isBrierleyWorking,
    estimatedRewards: res && res.estimatedRewards,
    pointsToNextReward: res && res.pointsToNextReward,
    earnedReward: res && res.earnedReward,
  };
  yield put(updateCartCount(res));
  yield put(setOrderId(res));
  yield put(updateCartPriceData(res));
  yield put(updateOrderTypeData(res));
  yield put(updateLoyaltyPoints(loyaltyPayload));
  if (fromStyleWith) {
    yield put(setGhostLoaderStateComplete({ addedToBagLoaderCompleteState: true }));
  }
}

export function* addToCartUpdateLocalData(orderType, payload, apiResp) {
  const { productName, quantity, skuInfo, isGiftCard, storeLocId } =
    orderType === ADDEDTOBAG_CONSTANTS.ADD_TO_CART_BOPIS ? payload.productInfo : payload;
  const { loyaltyOrderItems } = apiResp;
  const respItemObj =
    loyaltyOrderItems && loyaltyOrderItems.find((item) => item.variantNo === skuInfo.variantNo);

  /*  This object contains just enough basic data stored locally to support quicker rendering of
      the bag page and items. Not all fields will be available so some fields are hardcoded.
      It is not a replacement for the cart api call (which will return later). Once api call is
      complete, contents in the bag will be updated  and a re-render will occur.
  */
  const cartAddItemData = {
    productInfo: {
      imagePath: skuInfo.imageUrl,
      size: skuInfo.size,
      generalProductId: skuInfo.productId,
      color: skuInfo.color,
      variantNo: skuInfo.variantNo,
      tcpStyleQty: skuInfo.TCPStyleQTY,
      upc: skuInfo.variantId,
      orderType: respItemObj.orderItemType,
      tcpStyleType: skuInfo.TCPStyleType,
      isGiftCard,
      name: productName,
      productPartNumber: skuInfo.productId,
      multiPackItems: null,
      itemPartNumber: skuInfo.variantId,
      fit: skuInfo.fit,
      multiPack: respItemObj.multiPack,
      pdpUrl: '', // unavailable
      itemBrand: respItemObj.itemBrand,
      colorFitSizeDisplayNames: skuInfo.color,
      skuId: skuInfo.skuId,
    },
    itemInfo: {
      wasPrice: respItemObj.itemPrice,
      salePrice: respItemObj.itemPrice,
      quantity,
      listPrice: respItemObj.itemPrice,
      offerPrice: respItemObj.itemPrice,
      itemId: respItemObj.orderItemId,
      itemUnitPrice: respItemObj.itemUnitPrice,
      itemPoints: respItemObj.itemPoints > 0 ? respItemObj.itemPoints : 0,
      itemUnitDstPrice: respItemObj.itemUnitDstPrice,
    },
    miscInfo: {
      itemMprPointsMultiplier: '1',
      store: '',
      vendorColorDisplayId: skuInfo.productId,
      bossStartDate: {
        day: '',
        month: '',
        date: '',
        year: '',
      },
      badge: null,
      isOnlineOnly: false,
      isInventoryAvailBOSS: apiResp.isBossOrder,
      storeTomorrowOpenRange: 'null - null',
      isBossEligible: apiResp.isBossOrder,
      clearanceItem: false,
      storeItemsCount: quantity,
      storeTodayOpenRange: 'null - null',
      itemPlccPointsMultiplier: '1',
      storeAddress: {
        addressLine1: '',
        addressLine2: '',
        city: '',
        state: '',
        zipCode: '',
      },
      storePhoneNumber: '',
      isBopisEligible: apiResp.isBopisOrder,
      availability: 'OK',
      orderItemType: respItemObj.orderItemType,
      isStoreBOSSEligible: respItemObj.orderItemType === 'BOSS',
      storeId: orderType === ADDEDTOBAG_CONSTANTS.ADD_TO_CART_BOPIS ? storeLocId : '',
      bossEndDate: {
        day: '',
        month: '',
        date: '',
        year: '',
      },
    },
  };

  // Check existing store for cartOrderItemsLocal, add new entry
  const cartItemsArray = yield select(BagPageSelectors.getOrderItemsLocal);
  const newCartItemsArray = Array.from(cartItemsArray);
  newCartItemsArray.splice(0, 0, cartAddItemData);
  // Add new cart object
  yield put(setAddedToBagDataLocal(newCartItemsArray));
}

export function* addToCartEcom({ payload }) {
  let res;
  const {
    skipAddToBagModal,
    skuInfo,
    fromCompleteTheLook,
    fromStyleWith,
    originPage,
    productName,
    addedFromSBP,
    fromPage,
    fromFbt,
  } = payload;
  try {
    if (
      !isMobileApp() &&
      !fromStyleWith &&
      fromPage !== Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG
    ) {
      yield put(
        fetchAbTestData({
          payload: {
            abTest: {},
            pathname: window?.location?.pathname,
            page: 'added_to_bag',
            setAbtestScript: () => {},
          },
        })
      );
    }
    const sku = skuInfo.skuId;
    const multiPackProductType = skuInfo.TCPStyleType;
    const multiPackQty = skuInfo.TCPStyleQTY;
    const { isPdpPage, fromOutfitCarouselDetails } = payload;
    const getaddToBagABTestDrawer = yield call(isgetABTestAddToBagDrawer);
    const getOptmisticBagModal = yield call(isOptimisticAddBagModal);
    const isOptmisticBagModal = !!getOptmisticBagModal && !isMobileApp();
    const prevCartCount = yield call(getCartItemCount, true);
    const { callBack, fromMoveToBag } = payload;
    yield put(setGhostLoaderStateComplete({ addedToBagLoaderCompleteState: false }));
    yield put(setFbtComplete(false));
    if (fromStyleWith) {
      yield put(setGhostLoaderState({ addedToBagLoaderState: true }));
    } else if (
      isOptmisticBagModal &&
      ((!fromMoveToBag && !isPdpPage) ||
        (!getaddToBagABTestDrawer && isPdpPage) ||
        (isPdpPage && isCanada()) ||
        !!fromCompleteTheLook)
    ) {
      yield put(setLoaderState(false));
      yield put(openAddedToBag());
      yield put(
        SetAddedToBagData({
          ...payload,
          addedFromSBP,
        })
      );
    } else {
      yield put(setLoaderState(true));
    }

    const params = getParams(payload);
    params.skipLoyaltyCall = (isPdpPage && !!getaddToBagABTestDrawer) || !!skipAddToBagModal;
    yield put(clearAddToBagErrorState());
    yield put(clearAddToCartFbtError());
    yield put(clearAddToCartMultipleItemErrorState());
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);

    res = yield call(addCartEcomItem, params, errorMapping);

    yield put(
      SetAddedToBagData({
        ...payload,
        ...res,
        addedFromSBP,
      })
    );
    yield updateBagActions(res, fromStyleWith);
    yield put(setCartUpdateComplete(true));
    const isLoyaltyPointsEnabled = yield select(getIsPersistPointsApiEnabled);

    // Call Loyalty Points API when Brierley service is down And Get flag 'loyaltyCallEnabled' from cart Api
    if (
      res &&
      fromPage !== Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG &&
      !params.skipLoyaltyCall &&
      res.pointsApi !== 'NA' &&
      isLoyaltyServiceEnabled(res, isLoyaltyPointsEnabled)
    ) {
      yield put(
        getLoyaltyDetail({
          ...payload,
          ...res,
          source: 'AddedToBag',
          pointsApiCall: res.pointsApi,
          addedFromSBP,
        })
      );
    }

    if (callBack) {
      callBack();
    }
    const hasLoyaltyABTestEnabled = isPdpPage && !isCanada() && checkLoyaltyNAFlag(res);
    yield put(setLoyaltyABTestFlag(false));
    yield put(setLoaderState(false));

    if (skipAddToBagModal || hasLoyaltyABTestEnabled) {
      yield put(resetBagLoadingState());
      yield put(setAddedToBagButton(skuInfo.unbxdProdId));
    } else if (
      (!fromMoveToBag && !isPdpPage) ||
      (!getaddToBagABTestDrawer && isPdpPage) ||
      (isPdpPage && isCanada()) ||
      !!fromCompleteTheLook
    ) {
      if (!isOptmisticBagModal) {
        yield put(openAddedToBag());
      }
      if (fromPage === Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG) {
        yield call(getOrderDetailSaga, { ...payload, payload: { recalcRewards: true } });
      } else {
        yield put(resetBagLoadingState());
        // set the flag that is being set from getOrderDetails unconditionally
        yield put(toggleCheckoutRouting(true));
      }
    }
    if (fromOutfitCarouselDetails) {
      yield put(setOutfitCarouselAddedButton(true));
    }
    if (
      (getaddToBagABTestDrawer && isPdpPage && !isCanada()) ||
      (skipAddToBagModal && !fromOutfitCarouselDetails) ||
      hasLoyaltyABTestEnabled
    ) {
      yield put(addedTobagMsg(true));
      yield put(setLoyaltyABTestFlag(true));
    } else {
      yield put(addedTobagMsg(false));
    }

    yield call(makeNavigateXHrCall, { prevCartCount: getPrevCartCount(prevCartCount) });
    const ghostLoaderCompleteState = yield select(getAddedToBagGhostLoaderCompleteState);
    let mPackData = yield select(getBagAnalyticsData);
    if (!mPackData) {
      mPackData = [];
    }
    const mpackTags = getMpackTgas(
      multiPackProductType,
      originPage,
      multiPackQty,
      productName,
      sku
    );
    mPackData.push(mpackTags);
    yield put(setBagAnalytics(mPackData));
    yield call(logAddToBagEvent, {
      quantity: payload.quantity,
      cartPrice: res.orderTotal,
      multiPackProductType,
      originPage,
      selectedMultipack: multiPackQty,
      addedFromSBP,
    });
    if (multiPackProductType === MULTI_PACK_TYPE.MULTIPACK) {
      if (multiPackProductType !== null)
        yield put(setMultipackProductType(MULTI_PACK_TYPE.MULTIPACK));
      yield put(setIsMultipackItem(true));
    } else {
      yield put(setMultipackProductType(multiPackProductType));
      yield put(setIsMultipackItem(false));
    }
    if (!isMobileApp()) {
      yield put(updateMiniBagFlag(true));
    }
    if (fromStyleWith && ghostLoaderCompleteState) {
      yield delay(ADDEDTOBAG_CONSTANTS.GHOST_LOADER_DELAY);
      yield put(setFbtComplete(true));
      yield put(setGhostLoaderState({ addedToBagLoaderState: false }));
      yield put(setGhostLoaderStateComplete({ addedToBagLoaderCompleteState: false }));
    }
    yield addToCartUpdateLocalData(ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM, payload, res);
  } catch (err) {
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const pageData = yield select(getPageData);
    const pageType = pageData && pageData.pageType;
    const errMsg = getErrorMessage(err, errorMapping, pageType);

    if (fromFbt) {
      yield put(setAddToCartFbtError(errMsg, payload.skuInfo.unbxdProdId));
    } else {
      yield put(AddToCartError(errMsg, payload.skuInfo.unbxdProdId));
    }
    yield put(setGhostLoaderState({ addedToBagLoaderState: false }));
    yield put(setGhostLoaderStateComplete({ addedToBagLoaderCompleteState: false }));
    yield put(setLoaderState(false));
  }
  if (res?.errorResponse && res?.errorMessage) {
    return res;
  }
  return null;
}

export function* suppressGetOrderDetailsCheck() {
  return !(yield call(isAvoidGetOrderDetails));
}

export function* addItemToCartBopis({ payload }) {
  try {
    const {
      productInfo,
      productInfo: {
        storeLocId,
        isBoss,
        quantity,
        skuInfo: { skuId, variantId, variantNo },
      },
      callback,
    } = payload;
    const PICKUP_TYPE = {
      boss: 'boss',
      bopis: 'bopis',
    };
    const params = {
      storeLocId: storeLocId.toString(),
      quantity: quantity.toString(),
      catEntryId: skuId,
      isRest: 'false',
      pickupType: isBoss ? PICKUP_TYPE.boss : PICKUP_TYPE.bopis,
      variantNo,
      itemPartNumber: variantId,
    };
    const getOptmisticBagModal = yield call(isOptimisticAddBagModal);
    const getQVredesignEnabled = yield call(isNewQVEnabled);
    const isOptmisticBagModal = !!getOptmisticBagModal && !isMobileApp();
    const isQVRedesignEnabled = !!getQVredesignEnabled && !isMobileApp();
    const prevCartCount = yield call(getCartItemCount, true);
    const { fromPage } = payload;
    yield put(clearAddToPickupErrorState());

    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    if (isOptmisticBagModal) {
      yield put(clearAddToBagErrorState());
      yield put(setLoaderState(false));
      yield put(openAddedToBag());
      yield put(
        SetAddedToBagData({
          ...productInfo,
          pickupType: isBoss ? PICKUP_TYPE.boss : PICKUP_TYPE.bopis,
        })
      );
    }
    const res = yield call(addCartBopisItem, params, errorMapping);
    if (res && res?.totalItems && isQVRedesignEnabled) {
      yield put(setQuickViewProductDetailPageTitle(''));
      yield put(closeQuickViewModal({ isModalOpen: false }));
    }
    const loyaltyPayload = {
      isBrierleyWorking: res && res.isBrierleyWorking,
      estimatedRewards: res && res.estimatedRewards,
      pointsToNextReward: res && res.pointsToNextReward,
      earnedReward: res && res.earnedReward,
    };
    if (yield call(isAvoidGetOrderDetails)) {
      yield put(BAG_PAGE_ACTIONS.resetBagLoadingState());
      yield put(BAG_PAGE_ACTIONS.updateCartCount(res));
      yield put(BAG_PAGE_ACTIONS.setOrderId(res));
      yield put(BAG_PAGE_ACTIONS.updateCartPriceData(res));
      yield put(BAG_PAGE_ACTIONS.updateOrderTypeData(res));
      yield put(updateLoyaltyPoints(loyaltyPayload));
    }
    if (callback) {
      callback();
    }
    yield put(
      SetAddedToBagData({
        ...productInfo,
        ...res,
        pickupType: isBoss ? PICKUP_TYPE.boss : PICKUP_TYPE.bopis,
      })
    );
    const isLoyaltyPointsEnabled = yield select(getIsPersistPointsApiEnabled);

    // Call Loyalty Points API when Brierley service is down And Get flag 'loyaltyCallEnabled' from cart Api
    if (
      (yield call(isAvoidGetOrderDetails)) &&
      isLoyaltyServiceEnabled(res, isLoyaltyPointsEnabled) &&
      fromPage !== Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG
    ) {
      yield put(
        getLoyaltyDetail({
          ...productInfo,
          ...res,
          source: 'AddedToBag',
          pointsApiCall: res.pointsApi,
          pickupType: isBoss ? PICKUP_TYPE.boss : PICKUP_TYPE.bopis,
        })
      );
    }
    if (!isOptmisticBagModal) {
      yield put(openAddedToBag());
    }
    yield put(setLoaderState(false));
    if (
      (yield call(suppressGetOrderDetailsCheck)) ||
      fromPage === Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG
    ) {
      yield call(getOrderDetailSaga, { ...payload, payload: { recalcRewards: true } });
    } else {
      // set the flag that is being set from getOrderDetails unconditionally
      yield put(toggleCheckoutRouting(true));
    }
    if (!isMobileApp()) {
      yield put(updateMiniBagFlag(true));
    }
    yield call(makeNavigateXHrCall, { prevCartCount: getPrevCartCount(prevCartCount) });
    yield call(logAddToBagEvent, {
      quantity,
      cartPrice: res.orderTotal,
    });
    yield addToCartUpdateLocalData(ADDEDTOBAG_CONSTANTS.ADD_TO_CART_BOPIS, payload, res);
    yield put(BAG_PAGE_ACTIONS.resetBagLoadingState());
  } catch (err) {
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const errorMessage = getErrorMessage(err, errorMapping);
    yield put(AddToPickupError(errorMessage));
    yield put(BAG_PAGE_ACTIONS.resetBagLoadingState());
  }
}

export function* openAddedToBagModal(products, prevCartCount, callBack) {
  const pageName = yield select(getPageName);
  yield put(setLoaderState(false));
  if (callBack) {
    callBack();
  }
  yield put(SetAddedToBagData([...products]));
  yield put(openAddedToBag());
  if (!(yield call(isSuppressGetOrderDetails)) || pageName === BAGPAGE_CONSTANTS.SHOPPING_BAG) {
    yield put(getOrderDetails({ recalcRewards: true }));
  } else yield put(resetBagLoadingState());
  yield call(makeNavigateXHrCall, {
    prevCartCount: getPrevCartCount(prevCartCount),
  });
}

const parseErrorMessage = (error, errorMapping) => {
  return (
    (error &&
      error.errorResponse &&
      errorMapping &&
      errorMapping[error.errorResponse.errorMessageKey]) ||
    (error && error.errorResponse && error.errorResponse.errorMessage) ||
    (errorMapping && errorMapping.DEFAULT)
  );
};

export function* addMultipleItemToCartECOM(payload) {
  const { callBack, productItemsInfo } = payload;
  yield put(setLoaderState(true));
  const prevCartCount = yield call(getCartItemCount, true);
  try {
    const paramsArray = productItemsInfo.map((product) => {
      const { productId, skuId: catEntryId } = product.skuInfo;
      const { wishlistItemId, quantity } = product;
      const { storeId, langId, catalogId } = getAPIConfig();

      const apiConfigParams = {
        catalogId,
        storeId,
        langId,
      };
      return {
        ...apiConfigParams,
        orderId: '.',
        field2: '0',
        requesttype: 'ajax',
        catEntryId,
        quantity: quantity.toString(),
        'calculationUsage[]': '-7',
        externalId: wishlistItemId || '',
        productId,
        product,
      };
    });
    yield put(clearAddToCartMultipleItemErrorState());
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const res = yield call(addMultipleProductsInEcom, paramsArray, errorMapping);
    if (isMobileApp()) {
      yield call(logMultiAddToBagEvent, {
        cartPrice: (res && res[res.length - 1].orderTotal) || '',
        originPage: 'outfit',
        products: res || [],
      });
    }

    yield openAddedToBagModal(res, prevCartCount, callBack);
  } catch (errorObj) {
    const { error, errorProductId, atbSuccessProducts = [] } = errorObj;
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const errMsg = parseErrorMessage(error, errorMapping);
    if (atbSuccessProducts.length > 0) {
      yield openAddedToBagModal(atbSuccessProducts, prevCartCount, callBack);
    } else {
      yield put(AddToCartMultipleItemError({ errMsg, errorProductId }));
      yield put(setLoaderState(false));
    }
  }
}

export function* addMultipleItemToCartECOMSingleAPI(payload) {
  const { callBack, productItemsInfo } = payload;
  yield put(setLoaderState(true));
  const prevCartCount = yield call(getCartItemCount, true);
  try {
    const paramsArray = getParams(payload);
    yield put(clearAddToCartMultipleItemErrorState());
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const res = yield call(addCartEcomItem, paramsArray, errorMapping);
    const addedToBagProduts = [];
    productItemsInfo.forEach((product) => {
      const { skuId: catEntryId } = product.skuInfo;
      const { loyaltyOrderItems } = res;
      let addedItem = {};
      loyaltyOrderItems.forEach((item) => {
        if (catEntryId === item.catentryId) {
          addedItem = {
            ...product,
            orderId: res.orderId,
            orderItemId: item.orderItemId,
          };
        }
      });
      addedToBagProduts.push(addedItem);
    });

    if (isMobileApp()) {
      yield put(
        SetAddedToBagData({
          ...payload,
          ...res,
          addedFromSBP: addedToBagProduts[0].addedFromSBP,
        })
      );
      yield call(logMultiAddToBagEvent, {
        cartPrice: (res && res.orderTotal) || '',
        originPage: 'outfit',
        products: addedToBagProduts || [],
      });
    }
    yield openAddedToBagModal(addedToBagProduts, prevCartCount, callBack);
  } catch (errorObj) {
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const pageData = yield select(getPageData);
    const pageType = pageData && pageData.pageType;
    const errMsg = getErrorMessage(errorObj, errorMapping, pageType);
    yield put(AddToCartMultipleItemError({ errMsg }));
    yield put(setLoaderState(false));
  }
}

export function* multiAddToBagECOM({ payload }) {
  const singleAddedToBagAPIEnabled = yield select(getIsSingleAddToBagAPIEnabled);
  if (singleAddedToBagAPIEnabled) {
    yield call(addMultipleItemToCartECOMSingleAPI, payload);
  } else {
    yield call(addMultipleItemToCartECOM, payload);
  }
}

export function* AddedToBagSaga() {
  yield takeLatest(ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM, addToCartEcom);
  yield takeLatest(ADDEDTOBAG_CONSTANTS.ADD_TO_CART_BOPIS, addItemToCartBopis);
  yield takeLatest(ADDEDTOBAG_CONSTANTS.ADD_MULTIPLE_ITEMS_CART_ECOM, multiAddToBagECOM);
}

export default AddedToBagSaga;
