/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEqual from 'lodash/isEqual';
import { isCanada } from '@tcp/core/src/utils';
import {
  getIsGuest,
  isRememberedUser,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getFavoriteStoreActn } from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import { getAlternateBrandName } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import { getAlternateBrand } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import {
  getIsBossEnabled,
  getIsSuppressGetOrderDetails,
  getIsAvoidGetOrderDetails,
  getABTestOffStyleWith,
  getIsPhysicalMpackEnabled,
  getIsATBHidePoints,
  getOptimisticAddBagModal,
  getIsNewReDesignEnabled,
  getIsATBModalBackAbTestNewDesign,
  getIsFBTAbTestEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getBopisInventoryDetailsAction } from '@tcp/core/src/components/common/organisms/ProductPickup/container/ProductPickup.actions';
import * as PickupSelectors from '@tcp/core/src/components/common/organisms/ProductPickup/container/ProductPickup.selectors';
import { updateCartItem } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.actions';
import {
  closeAddedToBag,
  AddToPickupError,
  clearAddToBagModalError,
  addToCartEcomAtbLoadedNewDesign,
} from './AddedToBag.actions';
import {
  getAddedToBagData,
  getAddedToBagModalError,
  getAddedToBagLabels,
  isOpenAddedToBag,
  isOpenAddedToBagMinimal,
  getQuantityValue,
  getAddedToBagLoaderState,
  getLoyaltyAPICallStatus,
  getATbAPICallStatus,
  getAddedToBagInterval,
  getPointsSummary,
  getEnableKeepShoppingCTA,
  getMultipackProductType,
  getAddedToBagGhostLoaderState,
  getAddedToBagGhostLoaderCompleteState,
  getIsMpackItem,
  getAddedToPickupError,
  getEnableBOPISPickupInstead,
  getAddedToBagError,
  getPayloadData,
  getFbtDisplayedState,
  getIsFbtCompletedState,
  getATBItemInfo,
  getIsFbtEnabledState,
} from './AddedToBag.selectors';
import AddedToBag from '../views/AddedToBag.view';
import { getIsInternationalShipping } from '../../../../../reduxStore/selectors/session.selectors';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import checkoutSelectors from '../../Checkout/container/Checkout.selector';
import { getPDPLabels } from '../../../browse/ProductDetail/container/ProductDetail.selectors';

export class AddedToBagContainer extends React.Component {
  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    if (isEqual(nextProps, this.props)) {
      return false;
    }
    return true;
  }

  componentWillUnmount() {
    const { isNewReDesignEnabled, isATBModalBackAbTestNewDesign } = this.props;
    if (!isNewReDesignEnabled || !isATBModalBackAbTestNewDesign) {
      this.handleCloseModal();
    }
  }

  handleCloseModal = () => {
    const { closeModal, clearModalError } = this.props;
    closeModal();
    clearModalError();
  };

  hideHeaderWhilePaypalView = (hide) => {
    const { navigation } = this.props;
    navigation.setParams({ headerMode: hide });
  };

  closeModal(event) {
    const { isOpenDialogMinimal } = this.props;
    if (event) event.preventDefault();
    if (isOpenDialogMinimal?.callBack) isOpenDialogMinimal.callBack();
    this.handleCloseModal();
  }

  render() {
    const {
      addedToBagData,
      addToBagModalError,
      isOpenDialog,
      isOpenDialogMinimal,
      labels,
      quantity,
      navigation,
      isInternationalShipping,
      isPayPalWebViewEnable,
      isPayPalButtonRendered,
      isPayPalEnabled,
      router,
      closeModal,
      addedToBagLoaderState,
      addedToBagInterval,
      totalBagItems,
      pointsSummary,
      bagLoading,
      isBossEnabled,
      isSuppressGetOrderDetails,
      isVenmoAppInstalled,
      isVenmoEnabled,
      isAvoidGetOrderDetails,
      enableKeepShoppingCTA,
      multipackProductType,
      isABTestOffStyleWith,
      isPhysicalMpackEnabled,
      addedToBagGhostLoaderState,
      addedToBagGhostLoaderCompleteState,
      isMpackItem,
      getBopisInventoryDetails,
      userDefaultStore,
      bopisItemInventory,
      addItemToCartInPickup,
      addToPickupError,
      setAddToPickupError,
      showBOPISPickupInstead,
      isABTestHidePoints,
      isUserGuest,
      isRememberedUserState,
      getFavoriteStore,
      addedToBagError,
      isOptmisticAddToBagModal,
      isNewReDesignEnabled,
      isATBModalBackAbTestNewDesign,
      pdpLabels,
      loyaltyAPICallStatus,
      aTbAPICallStatus,
      addToBagEcomNewAfterModalOpened,
      openStatePayloadData,
      isPlcc,
      alternateBrand,
      alternateBrandFromPickup,
      isFBTAbTestEnabled,
      isFBTDisplayed,
      isFbtCompleted,
      itemInfo,
      isFBTEnabled,
    } = this.props;
    const isATBHidePoints =
      isABTestHidePoints && isUserGuest && !isCanada() && !isRememberedUserState;
    const primaryBrand = addedToBagData && addedToBagData.primaryBrand;
    const alternateBrandFromUrl = addedToBagData && addedToBagData.alternateBrand;
    return (
      <AddedToBag
        openState={isNewReDesignEnabled ? isOpenDialogMinimal : isOpenDialog}
        openStatePayloadData={openStatePayloadData}
        closeModal={closeModal}
        router={router}
        onRequestClose={this.closeModal}
        showBOPISPickupInstead={showBOPISPickupInstead}
        addedToBagData={addedToBagData}
        addToBagModalError={addToBagModalError}
        getBopisInventoryDetails={getBopisInventoryDetails}
        bopisItemInventory={bopisItemInventory}
        addItemToCartInPickup={addItemToCartInPickup}
        addToPickupError={addToPickupError}
        setAddToPickupError={setAddToPickupError}
        userDefaultStore={userDefaultStore}
        isInternationalShipping={isInternationalShipping}
        labels={labels}
        quantity={quantity}
        handleContinueShopping={this.closeModal}
        navigation={navigation}
        isPayPalWebViewEnable={isPayPalWebViewEnable}
        isPayPalButtonRendered={isPayPalButtonRendered}
        isPayPalEnabled={isPayPalEnabled}
        hideHeader={this.hideHeaderWhilePaypalView}
        addedToBagLoaderState={addedToBagLoaderState}
        addedToBagInterval={addedToBagInterval}
        totalBagItems={totalBagItems}
        pointsSummary={pointsSummary}
        bagLoading={bagLoading}
        isBossEnabled={isBossEnabled}
        isSuppressGetOrderDetails={isSuppressGetOrderDetails}
        isVenmoAppInstalled={isVenmoAppInstalled}
        isVenmoEnabled={isVenmoEnabled}
        isAvoidGetOrderDetails={isAvoidGetOrderDetails}
        enableKeepShoppingCTA={enableKeepShoppingCTA}
        multipackProductType={multipackProductType}
        isABTestOffStyleWith={isABTestOffStyleWith}
        isPhysicalMpackEnabled={isPhysicalMpackEnabled}
        addedToBagGhostLoaderState={addedToBagGhostLoaderState}
        addedToBagGhostLoaderCompleteState={addedToBagGhostLoaderCompleteState}
        isMpackItem={isMpackItem}
        isATBHidePoints={isATBHidePoints}
        getFavoriteStore={getFavoriteStore}
        addedToBagError={addedToBagError}
        isOptmisticAddToBagModal={isOptmisticAddToBagModal}
        isNewReDesignEnabled={isNewReDesignEnabled}
        isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
        pdpLabels={pdpLabels}
        loyaltyAPICallStatus={loyaltyAPICallStatus}
        aTbAPICallStatus={aTbAPICallStatus}
        addToBagEcomNewAfterModalOpened={addToBagEcomNewAfterModalOpened}
        isPlcc={isPlcc}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand || alternateBrandFromUrl || alternateBrandFromPickup}
        isFBTEnabled={isFBTAbTestEnabled || isFBTEnabled}
        isFBTDisplayed={isFBTDisplayed}
        isFbtCompleted={isFbtCompleted}
        itemInfo={itemInfo}
      />
    );
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    closeModal: () => {
      dispatch(closeAddedToBag());
    },
    clearModalError: () => {
      dispatch(clearAddToBagModalError());
    },
    getBopisInventoryDetails: (payload) => {
      dispatch(getBopisInventoryDetailsAction(payload));
    },
    addItemToCartInPickup: (payload) => {
      dispatch(updateCartItem(payload));
    },
    setAddToPickupError: (payload) => {
      dispatch(AddToPickupError(payload));
    },
    getFavoriteStore: (payload) => {
      dispatch(getFavoriteStoreActn(payload));
    },
    addToBagEcomNewAfterModalOpened: (payload) => {
      dispatch(addToCartEcomAtbLoadedNewDesign(payload));
    },
  };
};

const mapStateToProps = (state) => {
  // ----------- commenting usage of labels as we are getting labels values from backend intermittently. ------------
  const { isVenmoAppInstalled, getIsVenmoEnabled } = checkoutSelectors;
  const defaultStoreObj = {};
  const { getUserDefaultStore } = PickupSelectors;
  const defaultStore = getUserDefaultStore(state) || defaultStoreObj;
  return {
    addedToBagData: getAddedToBagData(state),
    addToBagModalError: getAddedToBagModalError(state),
    pointsSummary: getPointsSummary(state),
    isOpenDialog: isOpenAddedToBag(state),
    isOpenDialogMinimal: isOpenAddedToBagMinimal(state),
    openStatePayloadData: getPayloadData(state),
    quantity: getQuantityValue(state),
    isInternationalShipping: getIsInternationalShipping(state),
    isPayPalWebViewEnable: BagPageSelectors.getPayPalWebViewStatus(state),
    isPayPalButtonRendered: BagPageSelectors.isPayPalButtonRenderDone(state),
    addedToBagLoaderState: getAddedToBagLoaderState(state),
    addedToBagInterval: getAddedToBagInterval(state),
    totalBagItems: BagPageSelectors.getTotalItems(state),
    isPayPalEnabled: BagPageSelectors.getIsPayPalEnabled(state),
    bagLoading: BagPageSelectors.isBagLoading(state),
    isBossEnabled: getIsBossEnabled(state),
    isSuppressGetOrderDetails: getIsSuppressGetOrderDetails(state),
    isVenmoAppInstalled: isVenmoAppInstalled(state),
    isVenmoEnabled: getIsVenmoEnabled(state),
    isAvoidGetOrderDetails: getIsAvoidGetOrderDetails(state),
    enableKeepShoppingCTA: getEnableKeepShoppingCTA(state),
    multipackProductType: getMultipackProductType(state),
    isABTestOffStyleWith: getABTestOffStyleWith(state),
    isPhysicalMpackEnabled: getIsPhysicalMpackEnabled(state),
    addedToBagGhostLoaderState: getAddedToBagGhostLoaderState(state),
    addedToBagGhostLoaderCompleteState: getAddedToBagGhostLoaderCompleteState(state),
    isMpackItem: getIsMpackItem(state),
    userDefaultStore: defaultStore,
    bopisItemInventory: PickupSelectors.getBopisItemInventory(state),
    addToPickupError: getAddedToPickupError(state),
    showBOPISPickupInstead: getEnableBOPISPickupInstead(state),
    isABTestHidePoints: getIsATBHidePoints(state),
    isOptmisticAddToBagModal: !!getOptimisticAddBagModal(state),
    isUserGuest: getIsGuest(state),
    isRememberedUserState: isRememberedUser(state),
    labels: getAddedToBagLabels(state),
    addedToBagError: getAddedToBagError(state),
    isNewReDesignEnabled: getIsNewReDesignEnabled(state),
    isATBModalBackAbTestNewDesign: getIsATBModalBackAbTestNewDesign(state),
    pdpLabels: getPDPLabels(state),
    loyaltyAPICallStatus: getLoyaltyAPICallStatus(state),
    aTbAPICallStatus: getATbAPICallStatus(state),
    isPlcc: isPlccUser(state),
    alternateBrand: getAlternateBrandName(state),
    alternateBrandFromPickup: getAlternateBrand(state),
    isFBTAbTestEnabled: getIsFBTAbTestEnabled(state),
    isFBTDisplayed: getFbtDisplayedState(state),
    isFbtCompleted: getIsFbtCompletedState(state),
    itemInfo: getATBItemInfo(state),
    isFBTEnabled: getIsFbtEnabledState(state),
  };

  // return newState;
};

AddedToBagContainer.propTypes = {
  addedToBagData: PropTypes.shape({}).isRequired,
  addToBagModalError: PropTypes.string,
  router: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  quantity: PropTypes.number.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  addedToBagLoaderState: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  clearModalError: PropTypes.func,
  pointsSummary: PropTypes.shape({}).isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isBossEnabled: PropTypes.bool.isRequired,
  isSuppressGetOrderDetails: PropTypes.bool.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  isOpenDialog: PropTypes.bool.isRequired,
  isOpenAddedToBagMinimal: PropTypes.shape({}),
  isPayPalWebViewEnable: PropTypes.bool.isRequired,
  isPayPalButtonRendered: PropTypes.bool.isRequired,
  isPayPalEnabled: PropTypes.bool.isRequired,
  addedToBagInterval: PropTypes.func.isRequired,
  totalBagItems: PropTypes.shape([]).isRequired,
  isVenmoAppInstalled: PropTypes.bool.isRequired,
  isVenmoEnabled: PropTypes.bool.isRequired,
  isAvoidGetOrderDetails: PropTypes.bool.isRequired,
  enableKeepShoppingCTA: PropTypes.bool.isRequired,
  multipackProductType: PropTypes.string,
  isABTestOffStyleWith: PropTypes.bool,
  isPhysicalMpackEnabled: PropTypes.bool,
  addedToBagGhostLoaderState: PropTypes.bool,
  isMpackItem: PropTypes.bool,
  addedToBagGhostLoaderCompleteState: PropTypes.bool,
  getBopisInventoryDetails: PropTypes.func,
  userDefaultStore: PropTypes.shape({}),
  bopisItemInventory: PropTypes.shape({}),
  addItemToCartInPickup: PropTypes.func,
  addToPickupError: PropTypes.string,
  setAddToPickupError: PropTypes.func,
  showBOPISPickupInstead: PropTypes.bool,
  isABTestHidePoints: PropTypes.bool,
  isUserGuest: PropTypes.bool,
  isRememberedUserState: PropTypes.bool,
  getFavoriteStore: PropTypes.func,
  addedToBagError: PropTypes.string,
  isOptmisticAddToBagModal: PropTypes.bool,
  isNewReDesignEnabled: PropTypes.bool,
  pdpLabels: PropTypes.shape({}).isRequired,
  isOpenDialogMinimal: PropTypes.shape({}),
  isATBModalBackAbTestNewDesign: PropTypes.bool,
  loyaltyAPICallStatus: PropTypes.bool,
  aTbAPICallStatus: PropTypes.bool,
  addToBagEcomNewAfterModalOpened: PropTypes.func,
  openStatePayloadData: PropTypes.shape({}),
  isPlcc: PropTypes.bool,
};

AddedToBagContainer.defaultProps = {
  multipackProductType: null,
  isABTestOffStyleWith: false,
  isPhysicalMpackEnabled: false,
  addedToBagGhostLoaderState: false,
  isMpackItem: false,
  addedToBagGhostLoaderCompleteState: false,
  getBopisInventoryDetails: () => {},
  userDefaultStore: {},
  bopisItemInventory: {},
  addItemToCartInPickup: () => {},
  addToPickupError: '',
  setAddToPickupError: () => {},
  showBOPISPickupInstead: false,
  isABTestHidePoints: false,
  isUserGuest: true,
  isRememberedUserState: false,
  getFavoriteStore: () => {},
  addedToBagError: '',
  isOptmisticAddToBagModal: false,
  isNewReDesignEnabled: false,
  isOpenAddedToBagMinimal: {},
  isOpenDialogMinimal: {},
  addToBagModalError: null,
  clearModalError: () => {},
  isATBModalBackAbTestNewDesign: false,
  loyaltyAPICallStatus: false,
  aTbAPICallStatus: false,
  addToBagEcomNewAfterModalOpened: () => {},
  openStatePayloadData: {},
  isPlcc: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddedToBagContainer);
