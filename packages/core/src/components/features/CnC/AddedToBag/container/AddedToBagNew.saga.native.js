/* eslint-disable complexity, sonarjs/cognitive-complexity, max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select, fork, delay } from 'redux-saga/effects';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { isMobileApp } from '@tcp/core/src/utils';
import { trackClickWithData } from '@tcp/core/src/analytics/actions';
import { setProp } from '@tcp/core/src/analytics/utils';
import { getClickAnalyticsData, getPageData } from '@tcp/core/src/analytics/Analytics.selectors';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import {
  getCartsSessionStatus,
  getIsHapticFeedbackEnabled,
  getIsSuppressGetOrderDetails,
  getIsNewReDesignEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  setLoyaltyApiCallStatus,
  setATBApiCallStatus,
  setLoaderState,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import {
  getLoyaltyDetail,
  updateLoyaltyPoints,
} from '@tcp/core/src/components/features/CnC/LoyaltyBanner/container/Loyalty.actions';
import { MULTI_PACK_TYPE } from '@tcp/core/src/components/common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import { getBagAnalyticsData } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import { HAPTICFEEDBACK_EVENTS } from '@tcp/core/src/components/common/atoms/hapticFeedback/HapticFeedback.constants';
import { getPageName } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { setCartsSessionStatus } from '../../../../../reduxStore/actions';
import ADDEDTOBAG_CONSTANTS from '../AddedToBag.constants';
import {
  addCartEcomItem,
  addMultipleProductsInEcom,
  addCartBopisItem,
} from '../../../../../services/abstractors/CnC/AddedToBag';
import {
  SetAddedToBagData,
  openAddedToBagMinimal,
  setAddedToBagDataLocal,
  addToBagModalError,
  clearAddToBagModalError,
  setBagAnalytics,
  setMultipackProductType,
  setIsMultipackItem,
  clearAddToCartMultipleItemErrorState,
  AddToCartMultipleItemError,
} from './AddedToBag.actions';
import { getAPIConfig } from '../../../../../utils';
import BAG_PAGE_ACTIONS from '../../BagPage/container/BagPage.actions';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import { getIsGuest } from '../../../account/User/container/User.selectors';
import { navigateXHRAction } from '../../../account/NavigateXHR/container/NavigateXHR.action';
import { getCartItemCount } from '../../../../../utils/cookie.util';
import { getRecommendationsObj } from '../../../../../utils/utils.app';
import { getMultiPackStyleType } from '../views/AddedToBag.utils';
import BAGPAGE_CONSTANTS from '../../BagPage/BagPage.constants';

const {
  updateCartCount,
  setOrderId,
  updateCartPriceData,
  updateOrderTypeData,
  setCartUpdateComplete,
  getOrderDetails,
  resetBagLoadingState,
} = BAG_PAGE_ACTIONS;

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: true,
};

const getErrorMessage = (err, errorMapping, originPage) => {
  if (
    err &&
    err.errorCodes === ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER &&
    err &&
    err.errorMessageParam
  ) {
    let errorAvailString;
    const errorMessageParamArray = err && err.errorMessageParam;
    if (errorMessageParamArray) {
      errorAvailString =
        errorMessageParamArray &&
        errorMessageParamArray.filter((errorItem) => {
          return errorItem && errorItem.includes('=');
        });
    }

    const errorAvailStringValue = errorAvailString && errorAvailString[0];
    const inAvailQuantity = errorAvailStringValue && errorAvailStringValue.split('=')[1];
    const inAvailQuantityValue = parseInt(inAvailQuantity, 10);
    if (inAvailQuantityValue <= 0) {
      return errorMapping && errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER_OOS];
    }
    if (inAvailQuantityValue > 0) {
      return `${
        errorMapping &&
        errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER_PARTIAL_INSTOCK_MSG1]
      } ${inAvailQuantity} ${
        errorMapping &&
        errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER_PARTIAL_INSTOCK_MSG2]
      }`;
    }
  }
  if (err && err.errorCodes === ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER) {
    return originPage === 'shopping bag'
      ? errorMapping && errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER]
      : errorMapping && errorMapping[ADDEDTOBAG_CONSTANTS.API_CANT_RESOLVE_FFMCENTER_SHORT];
  }
  return (
    // eslint-disable-next-line no-underscore-dangle
    (err && err.errorMessages && err.errorMessages._error) ||
    // eslint-disable-next-line no-underscore-dangle
    (err && err.errorMessage && err.errorMessage._error) ||
    (errorMapping && errorMapping.DEFAULT) ||
    'ERROR'
  );
};

export const isLoyaltyServiceEnabled = (res) => {
  const { isBrierleyWorking, pointsApi } = res;
  return (
    res &&
    !isBrierleyWorking &&
    pointsApi &&
    (pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.BRIERLEY ||
      pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.LOYALTY)
  );
};

export const getAllCatId = (newCatId) => {
  const singlegetNewcatIdPop = newCatId && newCatId.split(',');
  const newSet = new Set(singlegetNewcatIdPop);
  const uniqueCatIdSet = [...newSet];
  uniqueCatIdSet.pop();
  return uniqueCatIdSet.join();
};

export const filterItems = (getAllNewMultiPack, getAllMultiPack, skuInfo, partIdInfo) => {
  let newCatId = '';
  let getpart;
  let isSingleProduct;
  const getAllMultiPackNumber = [];
  for (let index = 0; index < getAllMultiPack.length; index += 1) {
    if (
      getAllMultiPack[index].name.toLowerCase() === skuInfo.color.name.toLowerCase() &&
      getAllMultiPack[index].TCPMultiPackReferenceUSStore
    ) {
      getAllMultiPackNumber.push(getAllMultiPack[index].TCPMultiPackReferenceUSStore);
    }
  }

  for (let j = 0; j < getAllNewMultiPack.length; j += 1) {
    for (let k = 0; k < getAllNewMultiPack[j].variants.length; k += 1) {
      if (
        getAllMultiPackNumber.length > 0 &&
        skuInfo &&
        getAllNewMultiPack[j].variants[k].v_tcpsize &&
        (skuInfo.size === getAllNewMultiPack[j].variants[k].v_tcpsize.split('_')[1] ||
          skuInfo.size === getAllNewMultiPack[j].variants[k].v_tcpsize) &&
        getAllMultiPackNumber[0].indexOf(getAllNewMultiPack[j].prodpartno) > -1
      ) {
        /* eslint-disable camelcase */
        const { v_item_catentry_id } = getAllNewMultiPack[j].variants[k];
        if (getAllMultiPackNumber[j] && partIdInfo[j].split(',').length > 1) {
          getpart = partIdInfo[j].split(',');
        }

        if (getAllMultiPackNumber[0].includes(',')) {
          getpart = getAllMultiPackNumber[0].split(',');
        } else if (getAllNewMultiPack[j].prodpartno === getAllMultiPackNumber[0].split('#')[0]) {
          getpart = getAllMultiPackNumber;
          isSingleProduct = true;
        }
        const results =
          getpart &&
          getpart.find((element) => element.indexOf(getAllNewMultiPack[j].prodpartno) === 0);
        const getNewCatIdVal = `${v_item_catentry_id}_${
          isSingleProduct ? getpart[0].split('#')[1] : results && results.split('#')[1]
        },`;
        if (isSingleProduct) {
          newCatId += getNewCatIdVal;
          return getAllCatId(newCatId);
        }

        newCatId += getNewCatIdVal;
      }
    }
  }
  return getAllCatId(newCatId);
};

export const filterMultiPackVariant = (getAllMultiPack, skuInfo) => {
  return getAllMultiPack.filter((product) => {
    const { TCPMultiPackReferenceUSStore } = product;
    return skuInfo.color.name.toLowerCase() === product.name.toLowerCase()
      ? TCPMultiPackReferenceUSStore
      : null;
  });
};

export const getParams = (payload) => {
  const { skuInfo, getAllMultiPack, getAllNewMultiPack } = payload;
  const isMultiPackVariant = !!getAllMultiPack && filterMultiPackVariant(getAllMultiPack, skuInfo);
  const { partIdInfo } = payload;
  const { storeId, langId, catalogId } = getAPIConfig();
  const multipackCatentryId = payload.multiPackItems ? payload.multiPackItems : null;
  const multipackIds = payload.multipackProduct ? payload.multipackProduct : null;
  const apiConfigParams = {
    catalogId,
    storeId,
    langId,
  };
  const sku = payload.skuInfo.skuId;
  const qty = payload.quantity;
  const getCatId = !multipackIds
    ? null
    : filterItems(getAllNewMultiPack, getAllMultiPack, skuInfo, partIdInfo);

  return {
    ...apiConfigParams,
    orderId: '.',
    field2: '0',
    requesttype: 'ajax',
    catEntryId: sku,
    quantity: qty.toString(),
    'calculationUsage[]': '-7',
    multipackCatentryId:
      isMultiPackVariant &&
      isMultiPackVariant[0] &&
      isMultiPackVariant[0].TCPMultiPackUSStore &&
      getCatId
        ? getCatId
        : multipackCatentryId,
  };
};

function* updateBagActions(res) {
  const loyaltyPayload = {
    isBrierleyWorking: res && res.isBrierleyWorking,
    estimatedRewards: res && res.estimatedRewards,
    pointsToNextReward: res && res.pointsToNextReward,
    earnedReward: res && res.earnedReward,
  };
  yield put(updateCartCount(res));
  yield put(setOrderId(res));
  yield put(updateCartPriceData(res));
  yield put(updateOrderTypeData(res));
  yield put(updateLoyaltyPoints(loyaltyPayload));
}

export const getMpackTgas = (
  multiPackProductType,
  originPage,
  selectedMultipack,
  productName,
  sku
) => {
  let mpackTags = {};
  let cd124 = '';
  const cd125 = `${selectedMultipack}${ADDEDTOBAG_CONSTANTS.GA_ANAYTICS.PACK}`;
  const cd199 = getMultiPackStyleType(multiPackProductType);
  const gaAnalyticsConfKey = isMobileApp() ? 'GA_ANAYTICS_APP' : 'GA_ANAYTICS';
  if (multiPackProductType) {
    switch (originPage) {
      case 'pdp':
        cd124 =
          multiPackProductType === MULTI_PACK_TYPE.MULTIPACK
            ? ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PDP_MULTIPACK
            : ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PDP_SINGLE;
        break;
      case 'spdp':
        cd124 = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].SEARCH_PDP_MPACK;
        break;
      case 'slp':
        cd124 = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].SLP_MPACK;
        break;
      case 'plp':
        cd124 = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PLP_MPACK;
        break;
      default:
        cd124 = '';
    }
    mpackTags = {
      dimension124: cd124,
      dimension125: cd125,
      dimension199: cd199,
      ...(productName && { name: productName }),
      ...(sku && { skuId: sku }),
    };
  }
  return mpackTags;
};

const getMpackPageType = (multiPackProductType, originPage) => {
  const gaAnalyticsConfKey = isMobileApp() ? 'GA_ANAYTICS_APP' : 'GA_ANAYTICS';
  let mpackPageType = '';
  if (multiPackProductType) {
    switch (originPage) {
      case 'pdp':
        mpackPageType =
          multiPackProductType === MULTI_PACK_TYPE.MULTIPACK
            ? ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PDP_MULTIPACK
            : ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PDP_SINGLE;
        break;
      case 'spdp':
        mpackPageType = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].SEARCH_PDP_MPACK;
        break;
      case 'slp':
        mpackPageType = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].SLP_MPACK;
        break;
      case 'plp':
        mpackPageType = ADDEDTOBAG_CONSTANTS[gaAnalyticsConfKey].PLP_MPACK;
        break;
      default:
        mpackPageType = '';
    }
  }
  return mpackPageType;
};

export function* logAddToBagEvent({
  quantity,
  cartPrice,
  multiPackProductType,
  originPage,
  selectedMultipack = 1,
  addedFromSBP,
}) {
  const clickAnalyticsData = yield select(getClickAnalyticsData);
  const { departmentList = '', categoryList = '', listingSubCategory = '' } = clickAnalyticsData;
  const breadcrumbs = [departmentList, categoryList, listingSubCategory];
  const product = clickAnalyticsData.products && clickAnalyticsData.products[0];

  if (product) {
    product.quantity = parseInt(quantity, 10);
    product.cartPrice = parseFloat(cartPrice);
    product.mpackPageType = getMpackPageType(multiPackProductType, originPage);
    product.mpackValue = `${selectedMultipack}${ADDEDTOBAG_CONSTANTS.GA_ANAYTICS.PACK}`;
  }
  if (addedFromSBP) product.sbp = true;
  let recsObj = {};
  if (product && product.colorId) {
    recsObj = yield call(getRecommendationsObj, product.colorId);
  }
  if (clickAnalyticsData && clickAnalyticsData.products && clickAnalyticsData.products.length) {
    clickAnalyticsData.products[0] = { ...product, ...recsObj };
  }
  const updatedClickAnalyticsData = { ...clickAnalyticsData };
  const breadcrumbString = breadcrumbs.reduce((bdString, current) => {
    return current ? `${bdString}${bdString ? ':' : ''}${current}` : bdString;
  }, '');
  setProp('prop23', breadcrumbString);
  yield put(
    trackClickWithData(updatedClickAnalyticsData, {
      name: 'add_to_cart',
      module: 'browse',
    })
  );

  if (isMobileApp()) {
    const cartsSessionStatus = yield select(getCartsSessionStatus);
    if (cartsSessionStatus) {
      yield put(setCartsSessionStatus(0));
    }
  }
}

const getPrevCartCount = (prevCartCount) => {
  return parseInt(prevCartCount || 0, 10);
};

export function* makeNavigateXHrCall(obj) {
  const { prevCartCount } = obj || {};
  const isGuestUser = yield select(getIsGuest);
  if (isGuestUser && !prevCartCount) {
    yield put(navigateXHRAction());
  }
}

export function* addToCartUpdateLocalData(orderType, payload, apiResp) {
  const { productName, quantity, skuInfo, isGiftCard, storeLocId } =
    orderType === ADDEDTOBAG_CONSTANTS.ADD_TO_CART_BOPIS ? payload.productInfo : payload;
  const { loyaltyOrderItems } = apiResp;
  const respItemObj =
    loyaltyOrderItems && loyaltyOrderItems.find((item) => item.variantNo === skuInfo.variantNo);

  /*  This object contains just enough basic data stored locally to support quicker rendering of
      the bag page and items. Not all fields will be available so some fields are hardcoded.
      It is not a replacement for the cart api call (which will return later). Once api call is
      complete, contents in the bag will be updated  and a re-render will occur.
  */
  const cartAddItemData = {
    productInfo: {
      imagePath: skuInfo.imageUrl,
      size: skuInfo.size,
      generalProductId: skuInfo.productId,
      color: skuInfo.color,
      variantNo: skuInfo.variantNo,
      tcpStyleQty: skuInfo.TCPStyleQTY,
      upc: skuInfo.variantId,
      orderType: respItemObj.orderItemType,
      tcpStyleType: skuInfo.TCPStyleType,
      isGiftCard,
      name: productName,
      productPartNumber: skuInfo.productId,
      multiPackItems: null,
      itemPartNumber: skuInfo.variantId,
      fit: skuInfo.fit,
      multiPack: respItemObj.multiPack,
      pdpUrl: '', // unavailable
      itemBrand: respItemObj.itemBrand,
      colorFitSizeDisplayNames: skuInfo.color,
      skuId: skuInfo.skuId,
    },
    itemInfo: {
      wasPrice: respItemObj.itemPrice,
      salePrice: respItemObj.itemPrice,
      quantity,
      listPrice: respItemObj.itemPrice,
      offerPrice: respItemObj.itemPrice,
      itemId: respItemObj.orderItemId,
      itemUnitPrice: respItemObj.itemUnitPrice,
      itemPoints: respItemObj.itemPoints > 0 ? respItemObj.itemPoints : 0,
      itemUnitDstPrice: respItemObj.itemUnitDstPrice,
    },
    miscInfo: {
      itemMprPointsMultiplier: '',
      store: '',
      vendorColorDisplayId: '',
      bossStartDate: null,
      badge: null,
      isOnlineOnly: false,
      isInventoryAvailBOSS: false,
      storeTomorrowOpenRange: null,
      isBossEligible: apiResp.isBossOrder,
      clearanceItem: false,
      storeItemsCount: 0,
      storeTodayOpenRange: null,
      itemPlccPointsMultiplier: '',
      storeAddress: '',
      storePhoneNumber: '',
      isBopisEligible: apiResp.isBopisOrder,
      availability: 'OK',
      orderItemType: respItemObj.orderItemType,
      isStoreBOSSEligible: false,
      storeId: orderType === ADDEDTOBAG_CONSTANTS.ADD_TO_CART_BOPIS ? storeLocId : '',
      bossEndDate: null,
    },
  };

  // Check existing store for cartOrderItemsLocal, add new entry
  const cartItemsArray = yield select(BagPageSelectors.getOrderItemsLocal);
  const newCartItemsArray = Array.from(cartItemsArray);
  newCartItemsArray.splice(0, 0, cartAddItemData);
  // Add new cart object
  yield put(setAddedToBagDataLocal(newCartItemsArray));
}

export function* clearATBErrors() {
  yield put(clearAddToBagModalError());
}

export function* setATBDataAndHapticFeedback(payload) {
  yield put(SetAddedToBagData(payload));
}

export function* callGetLoyaltyPoints(payload) {
  yield put(getLoyaltyDetail(payload));
  yield put(setLoyaltyApiCallStatus(false));
}

export function* triggerNavigateXHrCall(prevCartCount) {
  yield call(makeNavigateXHrCall, { prevCartCount: getPrevCartCount(prevCartCount) });
}

export function* setMultipackData(multiPackProductType) {
  if (multiPackProductType === MULTI_PACK_TYPE.MULTIPACK) {
    if (multiPackProductType !== null)
      yield put(setMultipackProductType(MULTI_PACK_TYPE.MULTIPACK));
    yield put(setIsMultipackItem(true));
  } else {
    yield put(setMultipackProductType(multiPackProductType));
    yield put(setIsMultipackItem(false));
  }
}

export function* isSuppressGetOrderDetails() {
  return yield select(getIsSuppressGetOrderDetails);
}

export function* logMultiAddToBagEvent({ cartPrice, originPage, products }) {
  const { selectedCategoryId } = products[0];
  const productsNew = products.map((product, i) => {
    const { productId, offerPrice, size, skuId, TCPStyleQTY, TCPStyleType, color } =
      product.skuInfo;
    const {
      productName,
      fromPage,
      outfitId,
      brand,
      ratings,
      reviewsCount,
      addedFromSBP,
      quantity,
      listPrice,
      title,
      priceCurrency,
    } = product;
    return {
      isOutfitPage: title === 'COMPLETE THE LOOK',
      fromPageName: fromPage,
      CurrCurrency: priceCurrency,
      color: [color.name],
      colorId: productId,
      extPrice: listPrice,
      id: productId,
      listPrice,
      name: productName,
      outfitId,
      paidPrice: offerPrice,
      plpClick: false,
      position: i + 1,
      price: offerPrice,
      prodBrand: brand,
      rating: ratings,
      recsPageType: fromPage,
      recsProductId: productId,
      recsType: `${title === 'COMPLETE THE LOOK' ? 'CTL' : title}`,
      reviews: reviewsCount,
      searchClick: false,
      size,
      sku: skuId,
      quantity: parseInt(quantity, 10),
      cartPrice: parseFloat(cartPrice),
      mpackPageType: getMpackPageType(TCPStyleType, originPage),
      mpackValue: `${TCPStyleQTY || 1}${ADDEDTOBAG_CONSTANTS.GA_ANAYTICS.PACK}`,
      sbp: addedFromSBP || false,
      pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
    };
  });
  const eventsData = ['scAdd', 'event61'];
  const cartsSessionStatus = yield select(getCartsSessionStatus);
  if (cartsSessionStatus) {
    eventsData.push('scOpen');
    yield put(setCartsSessionStatus(0));
  }

  const clickAnalyticsData = {
    customEvents: eventsData,
    CartAddProductId: selectedCategoryId,
    linkName: 'cart add',
    pageName: 'outfit',
    pageSection: 'outfit',
    pageShortName: 'outfit',
    pageSubSection: 'outfit',
    pageType: 'outfit',
    products: productsNew,
  };

  const updatedClickAnalyticsData = { ...clickAnalyticsData };

  yield put(
    trackClickWithData(updatedClickAnalyticsData, {
      name: 'add_to_cart',
      module: 'browse',
    })
  );
}

export function* openAddedToBagModal(products, prevCartCount, callBack) {
  const pageName = yield select(getPageName);
  yield put(setLoaderState(false));
  if (callBack) {
    callBack();
  }
  yield put(SetAddedToBagData([...products]));
  const maxValueProduct = products.reduce((prev, current) =>
    prev.listPrice > current.listPrice ? prev : current
  );
  const cartItemInfo = {
    ...maxValueProduct,
    isOpenNewATBModal: true,
  };
  yield put(openAddedToBagMinimal(cartItemInfo));
  if (!(yield call(isSuppressGetOrderDetails)) || pageName === BAGPAGE_CONSTANTS.SHOPPING_BAG) {
    yield put(getOrderDetails({ recalcRewards: true }));
  } else yield put(resetBagLoadingState());
  yield call(makeNavigateXHrCall, {
    prevCartCount: getPrevCartCount(prevCartCount),
  });
}

const parseErrorMessage = (error, errorMapping) => {
  return (
    (error &&
      error.errorResponse &&
      errorMapping &&
      errorMapping[error.errorResponse.errorMessageKey]) ||
    (error && error.errorResponse && error.errorResponse.errorMessage) ||
    (errorMapping && errorMapping.DEFAULT)
  );
};

export function* addMultipleItemToCartECOM(payload) {
  const { callBack, productItemsInfo } = payload;
  yield put(setLoaderState(true));
  const prevCartCount = yield call(getCartItemCount, true);
  try {
    const paramsArray = productItemsInfo.map((product) => {
      const { productId, skuId: catEntryId } = product.skuInfo;
      const { wishlistItemId, quantity } = product;
      const { storeId, langId, catalogId } = getAPIConfig();

      const apiConfigParams = {
        catalogId,
        storeId,
        langId,
      };
      return {
        ...apiConfigParams,
        orderId: '.',
        field2: '0',
        requesttype: 'ajax',
        catEntryId,
        quantity: quantity.toString(),
        'calculationUsage[]': '-7',
        externalId: wishlistItemId || '',
        productId,
        product,
      };
    });
    yield put(clearAddToCartMultipleItemErrorState());
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const isNewReDesign = yield select(getIsNewReDesignEnabled);
    const res = yield call(
      addMultipleProductsInEcom,
      paramsArray,
      errorMapping,
      getErrorMessage,
      isNewReDesign
    );
    if (isMobileApp()) {
      yield call(logMultiAddToBagEvent, {
        cartPrice: (res && res[res.length - 1].orderTotal) || '',
        originPage: 'outfit',
        products: res || [],
      });
    }
    yield put(setATBApiCallStatus(false));
    yield put(setLoyaltyApiCallStatus(false));
    const isHapticFeedbackEnabled = yield select(getIsHapticFeedbackEnabled);
    if (isHapticFeedbackEnabled) {
      ReactNativeHapticFeedback.trigger(HAPTICFEEDBACK_EVENTS.NOTIFICATION_WARINING, options);
    }
    yield openAddedToBagModal(res, prevCartCount, callBack);
  } catch (errorObj) {
    const { error, errorProductId, atbSuccessProducts = [] } = errorObj;
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const errMsg = parseErrorMessage(error, errorMapping);
    if (atbSuccessProducts.length > 0) {
      yield openAddedToBagModal(atbSuccessProducts, prevCartCount, callBack);
    } else {
      yield put(AddToCartMultipleItemError({ errMsg, errorProductId }));
      yield put(setLoaderState(false));
      const isHapticFeedbackEnabled = yield select(getIsHapticFeedbackEnabled);
      if (isHapticFeedbackEnabled) {
        ReactNativeHapticFeedback.trigger(HAPTICFEEDBACK_EVENTS.NOTIFICATION_ERROR, options);
      }
    }
    yield put(setATBApiCallStatus(false));
    yield put(setLoyaltyApiCallStatus(false));
  }
}

export function* addSingleItemToCartECOM(payload) {
  try {
    const {
      fromPage,
      storeLocId,
      quantity,
      isBoss,
      isBossBopisOrder,
      addedFromSBP,
      skuInfo,
      originPage,
      productName,
    } = payload;
    const { skuId, variantNo, variantId } = skuInfo;
    const params = getParams(payload);

    let res = {};
    const prevCartCount = yield call(getCartItemCount, true);
    yield fork(clearATBErrors);
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    if (isBossBopisOrder) {
      const PICKUP_TYPE = {
        boss: 'boss',
        bopis: 'bopis',
      };
      const params1 = {
        storeLocId: storeLocId.toString(),
        quantity: quantity.toString(),
        catEntryId: skuId,
        isRest: 'false',
        pickupType: isBoss ? PICKUP_TYPE.boss : PICKUP_TYPE.bopis,
        variantNo,
        itemPartNumber: variantId,
      };
      res = yield call(addCartBopisItem, params1, errorMapping);
    } else {
      res = yield call(addCartEcomItem, params, errorMapping);
    }

    yield put(
      SetAddedToBagData({
        ...payload,
        ...res,
        addedFromSBP,
      })
    );
    yield put(setATBApiCallStatus(false));
    const isHapticFeedbackEnabled = yield select(getIsHapticFeedbackEnabled);
    if (isHapticFeedbackEnabled) {
      ReactNativeHapticFeedback.trigger(HAPTICFEEDBACK_EVENTS.NOTIFICATION_WARINING, options);
    }

    yield delay(10);

    // Call Loyalty Points API when Brierley service is down And Get flag 'loyaltyCallEnabled' from cart Api
    if (
      res &&
      fromPage !== Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG &&
      res.pointsApi !== 'NA' &&
      !params.skipLoyaltyCall &&
      isLoyaltyServiceEnabled(res)
    ) {
      yield fork(callGetLoyaltyPoints, {
        ...payload,
        ...res,
        source: 'AddedToBag',
        pointsApiCall: res.pointsApi,
        addedFromSBP,
      });
    }

    yield delay(10);
    yield fork(updateBagActions, res);
    yield fork(setCartUpdateComplete, true);

    yield delay(10);
    yield fork(triggerNavigateXHrCall, prevCartCount);

    const sku = skuInfo.skuId;
    const multiPackProductType = skuInfo.TCPStyleType;
    const multiPackQty = skuInfo.TCPStyleQTY;

    yield fork(setMultipackData, multiPackProductType);
    addToCartUpdateLocalData(ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM, payload, res);

    let mPackData = yield select(getBagAnalyticsData);
    if (!mPackData) {
      mPackData = [];
    }
    const mpackTags = getMpackTgas(
      multiPackProductType,
      originPage,
      multiPackQty,
      productName,
      sku
    );
    mPackData.push(mpackTags);
    yield fork(setBagAnalytics, mPackData);
    yield fork(logAddToBagEvent, {
      quantity: payload.quantity,
      cartPrice: res.orderTotal,
      multiPackProductType,
      originPage,
      selectedMultipack: multiPackQty,
      addedFromSBP,
    });
  } catch (err) {
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const pageData = yield select(getPageData);
    const pageType = pageData && pageData.pageType;
    const errMsg = getErrorMessage(err, errorMapping, pageType);
    if (payload && payload.productItemsInfo) {
      yield put(addToBagModalError(errMsg));
    } else {
      yield put(addToBagModalError(errMsg, payload.skuInfo.unbxdProdId));
    }
    const isHapticFeedbackEnabled = yield select(getIsHapticFeedbackEnabled);
    if (isHapticFeedbackEnabled) {
      ReactNativeHapticFeedback.trigger(HAPTICFEEDBACK_EVENTS.NOTIFICATION_ERROR, options);
    }
    yield put(setATBApiCallStatus(false));
    yield put(setLoyaltyApiCallStatus(false));
  }
}

export function* addToCartEcomNewAfterAtbLoaded(payload) {
  const { productItemsInfo } = payload;
  if (payload && productItemsInfo) {
    yield addMultipleItemToCartECOM(payload);
  } else {
    yield addSingleItemToCartECOM(payload);
  }
}

export function* addToCartEcomNewLoadTheATB({ payload }) {
  try {
    let data = payload;
    if (payload && payload.productItemsInfo) {
      data = payload.productItemsInfo.reduce((prev, current) =>
        prev.listPrice > current.listPrice ? prev : current
      );
      yield put(openAddedToBagMinimal(data));
      //  Setting itemInfo upfront so that array of products are visible as soon as ATB modal opens
      yield put(SetAddedToBagData([...payload.productItemsInfo]));
    } else {
      //  When multiple products added previously, need to reset itemInfo to empty
      yield put(SetAddedToBagData({}));
      yield put(openAddedToBagMinimal(data));
    }
    yield put(setATBApiCallStatus(true));
    yield put(setLoyaltyApiCallStatus(true));
    yield delay(10);
    yield fork(addToCartEcomNewAfterAtbLoaded, payload);
  } catch (err) {
    const errorMapping = yield select(BagPageSelectors.getErrorMapping);
    const pageData = yield select(getPageData);
    const pageType = pageData && pageData.pageType;
    const errMsg = getErrorMessage(err, errorMapping, pageType);
    yield put(addToBagModalError(errMsg, payload.skuInfo.unbxdProdId));
  }
}

export function* AddedToBagNewSaga() {
  yield takeLatest(ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM_NEW, addToCartEcomNewLoadTheATB);
  yield takeLatest(
    ADDEDTOBAG_CONSTANTS.ADD_TO_CART_ECOM_NEW_ATB_LOADED,
    addToCartEcomNewAfterAtbLoaded
  );
  yield takeLatest(
    ADDEDTOBAG_CONSTANTS.ADD_MULTIPLE_ITEMS_CART_ECOM_NEW,
    addToCartEcomNewLoadTheATB
  );
}

export default AddedToBagNewSaga;
