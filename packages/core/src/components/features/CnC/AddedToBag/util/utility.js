// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable import/prefer-default-export */
import {
  getSkuId,
  getMapSliceForColor,
  getVariantId,
  getMapSliceForSize,
} from '../../../browse/ProductListingPage/util/utility';

const getImgUrl = (imagesByColor, color) =>
  (imagesByColor &&
    imagesByColor[color] &&
    (imagesByColor[color].basicImageUrl ||
      (imagesByColor[color].extraImages[0] || {}).iconSizeImageUrl)) ||
  '';

const getColor = (colorFitsSizesMap, color) => {
  const mapSliceForColor = getMapSliceForColor(colorFitsSizesMap, color);
  return mapSliceForColor && mapSliceForColor.color;
};

export const getCartItemInfo = (productInfoOrWishlistItem, customizationInfo, fromFBT) => {
  let obj = {};
  if (productInfoOrWishlistItem.productInfo) {
    // productInfoOrWishlistItem is a wishlistItem

    const {
      productInfo: { name, isGiftCard, TCPStyleType, TCPStyleQTY, TCPMultipackProductMapping },
      skuInfo,
      itemInfo,
    } = productInfoOrWishlistItem;
    obj = {
      isGiftCard,
      productName: name,
      skuInfo,
      quantity: 1,
      itemId: itemInfo.itemId || '',
      TCPStyleType,
      TCPStyleQTY,
      TCPMultipackProductMapping,
    };
  } else {
    // productInfoOrWishlistItem is a productInfo
    const { fit, size, quantity, isBoss, storeLocId, brand } = customizationInfo;
    let { color } = customizationInfo;
    let formPartNumber; // This is only defined in case of multiple colors with same name
    const { name, colorFitsSizesMap, isGiftCard, imagesByColor } = productInfoOrWishlistItem;
    if (!isGiftCard) {
      [color, formPartNumber] = color && color.split('-');
    }
    const currentSizeEntry = getMapSliceForSize(colorFitsSizesMap, color, fit, size, fromFBT);
    obj = {
      isGiftCard,
      productName: name,
      skuInfo: {
        skuId: getSkuId(colorFitsSizesMap, color, fit, size, fromFBT),
        imageUrl: getImgUrl(imagesByColor, color),
        color: getColor(colorFitsSizesMap, color),
        variantId: getVariantId(colorFitsSizesMap, color, fit, size),
        variantNo:
          currentSizeEntry && currentSizeEntry.variantNo ? currentSizeEntry.variantNo : null,
        unbxdProdId: productInfoOrWishlistItem.unbxdProdId,
        productId: productInfoOrWishlistItem.generalProductId,
        fit,
        size,
        TCPStyleType: productInfoOrWishlistItem.TCPStyleType,
        TCPStyleQTY: productInfoOrWishlistItem.TCPStyleQTY,
        productFamily: productInfoOrWishlistItem.productFamily,
        TCPMultipackProductMapping: productInfoOrWishlistItem.TCPMultipackProductMapping,
        productPartNoOfCurrentColor: formPartNumber,
        offerPrice: (currentSizeEntry && currentSizeEntry.offerPrice) || '',
      },
      quantity,
      isBoss,
      storeLocId,
      brand,
      wishlistItemId:
        customizationInfo.wishlistItemId ||
        (productInfoOrWishlistItem.itemInfo && productInfoOrWishlistItem.itemInfo.itemId),
      isOnlineExclusive: colorFitsSizesMap
        ? colorFitsSizesMap[0]?.miscInfo?.badge1?.defaultBadge
        : null,
    };
  }
  return obj;
};

export const shouldRenderPoints = (
  isSuppressGetOrderDetails,
  isAvoidGetOrderDetails,
  pointsSummary,
  isATBHidePoints
) =>
  (!isSuppressGetOrderDetails || isAvoidGetOrderDetails) &&
  Object.keys(pointsSummary).length !== 0 &&
  !isATBHidePoints;
