// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { capitalizeWords } from '@tcp/core/src/utils/utils';
import { getCurrentRouteName } from '@tcp/core/src/utils';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Button from '@tcp/core/src/components/common/atoms/Button';
import {
  BannerViewWrapper,
  ButtonWrapper,
  ContentWrapper,
  TextViewWrapper,
  ErrViewWrapper,
  StyledWarningImage,
} from '../styles/PickupBanner.style.native';
import { getIsStoreBopisEligible, errorOccurred, usePrevious } from './PickupBanner.utils';

const warningIcon = require('../../../../../../../../../mobileapp/src/assets/images/circle-warning-fill.png');

const renderErrorMsg = (errorOccured, pickupInsteadHeadingError, pickupInsteadMsgError) => {
  return errorOccured ? (
    <ContentWrapper fullWidth>
      <BodyCopy
        mobileFontFamily="secondary"
        fontSize="fs16"
        textAlign="left"
        fontWeight="extrabold"
        fontFamily="secondary"
        text={pickupInsteadHeadingError}
      />
      <ErrViewWrapper>
        <StyledWarningImage source={warningIcon} />
        <BodyCopy
          mobileFontFamily="secondary"
          fontWeight="bold"
          fontFamily="secondary"
          text={pickupInsteadMsgError}
        />
      </ErrViewWrapper>
    </ContentWrapper>
  ) : null;
};

const getUpdatedUserName = () => {
  let routeName = getCurrentRouteName() || '';
  routeName = routeName && routeName.toLowerCase().trim();
  return routeName;
};

const PickupBanner = ({
  labels,
  addedToBagData = {},
  getBopisInventoryDetails,
  userDefaultStore = {},
  bopisItemInventory = [],
  addItemToCartInPickup = () => {},
  addToPickupError = {},
  setAddToPickupError = () => {},
}) => {
  const {
    pickupInsteadHeading,
    pickupInsteadHeadingSuccess,
    pickupInsteadReady,
    pickupInsteadFreeOrder,
    pickupInsteadItemAdded,
    pickupInsteadBtnText,
    pickupInsteadHeadingError,
    pickupInsteadMsgError,
  } = labels;

  const { skuInfo = {}, quantity = '', orderItemId: itemId = '', orderId = '' } = addedToBagData;
  const { variantId: itemPartNumber = '', variantNo = '', skuId = '' } = skuInfo;
  const [itemAddedForPickup, updateItemAdded] = useState(false);

  const isStoreBopisEligible = getIsStoreBopisEligible(bopisItemInventory);

  const { basicInfo = {} } = userDefaultStore;
  const { storeName = '', id = '' } = basicInfo;
  const capitalizeStoreName = capitalizeWords(storeName);

  const getInventoryDetails = () => {
    const itemInfo = [
      {
        storeId: id.substring(2),
        variantNo,
        itemPartNumber,
      },
    ];
    getBopisInventoryDetails({ itemInfo });
  };

  const addedCallback = () => {
    updateItemAdded(true);
  };

  const prevDefaultStoreId = usePrevious(id) || '';
  const isMounted = useRef(null);

  useEffect(() => {
    const routeName = getUpdatedUserName();
    setAddToPickupError(null);
    if (routeName === 'outfitdetail' || routeName === 'bundledetail') {
      getInventoryDetails();
    }
  }, []);

  useEffect(() => {
    if (!isMounted.current && prevDefaultStoreId !== id) {
      getInventoryDetails();
      isMounted.current = true;
    }
  }, [userDefaultStore]);

  const errorOccured = errorOccurred(addToPickupError);

  const createBossBopisTogglePayload = () => {
    return {
      apiPayload: {
        orderId: `${orderId}`,
        orderItem: [
          {
            orderItemId: itemId,
            xitem_catEntryId: skuId,
            quantity: `${quantity}`,
            variantNo,
            itemPartNumber,
          },
        ],
        x_storeLocId: id,
        x_orderitemtype: 'ECOM', // source type of Item
        x_updatedItemType: 'BOPIS', // target type of Item
      },
      updateActionType: 'UpdatePickUpItem',
      fromTogglingBossBopis: true,
      callback: addedCallback,
      fromPickupBanner: true,
    };
  };

  const onPickupClick = () => {
    const payload = createBossBopisTogglePayload();
    addItemToCartInPickup(payload);
  };

  const heading = itemAddedForPickup ? pickupInsteadHeadingSuccess : pickupInsteadHeading;
  return (
    <>
      {isStoreBopisEligible && (
        <BannerViewWrapper>
          {renderErrorMsg(errorOccured, pickupInsteadHeadingError, pickupInsteadMsgError)}
          {!errorOccured && (
            <>
              <ContentWrapper fullWidth={itemAddedForPickup}>
                <BodyCopy
                  mobileFontFamily="secondary"
                  fontSize="fs16"
                  textAlign="left"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  color={itemAddedForPickup ? 'green.500' : ''}
                  text={heading}
                />
                <TextViewWrapper>
                  {!itemAddedForPickup && (
                    <>
                      <BodyCopy
                        mobileFontFamily="secondary"
                        fontWeight="semibold"
                        color="green.500"
                        text={pickupInsteadReady}
                      />
                      <BodyCopy
                        fontFamily="secondary"
                        mobileFontFamily="secondary"
                        text={pickupInsteadFreeOrder}
                      />
                    </>
                  )}
                  {itemAddedForPickup && (
                    <BodyCopy
                      fontFamily="secondary"
                      mobileFontFamily="secondary"
                      text={pickupInsteadItemAdded}
                    />
                  )}
                  <BodyCopy
                    fontFamily="secondary"
                    mobileFontFamily="secondary"
                    text={capitalizeStoreName}
                  />
                </TextViewWrapper>
              </ContentWrapper>

              {!itemAddedForPickup && (
                <ButtonWrapper>
                  <Button
                    fill="BLUE"
                    type="submit"
                    onPress={onPickupClick}
                    color="white"
                    text={pickupInsteadBtnText}
                  />
                </ButtonWrapper>
              )}
            </>
          )}
        </BannerViewWrapper>
      )}
    </>
  );
};

PickupBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  addedToBagData: PropTypes.shape({}).isRequired,
  userDefaultStore: PropTypes.shape({}).isRequired,
  bopisItemInventory: PropTypes.shape([]).isRequired,
  getBopisInventoryDetails: PropTypes.func.isRequired,
  addItemToCartInPickup: PropTypes.func.isRequired,
  addToPickupError: PropTypes.string.isRequired,
  setAddToPickupError: PropTypes.func.isRequired,
};

export default PickupBanner;
