import { useEffect, useRef } from 'react';

export const getIsStoreBopisEligible = bopisItemInventory => {
  return (
    bopisItemInventory &&
    bopisItemInventory.inventoryResponse &&
    bopisItemInventory.inventoryResponse.length > 0 &&
    bopisItemInventory.inventoryResponse[0].quantity > 0
  );
};

export const errorOccurred = addToPickupError => {
  return addToPickupError && addToPickupError.length;
};

export function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
}
