// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const setBackground = (props) => {
  if (props.theme.isGymboree) {
    return `
    background-color: ${props.theme.colorPalette.orange[900]};
    `;
  }
  return `
  background-color: ${props.theme.colorPalette.blue[800]};
  `;
};

const OuterContainer = styled.View`
  flex-direction: row;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;
const ProductName = styled.View``;
const ProductDesc = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;
const ImageWrapper = styled.View`
  width: 120px;
`;
const ProductDescription = styled.View`
  flex: 2;
  flex-wrap: wrap;
  flex-direction: column;
  margin-left: 5px;
`;
const ImgWrapper = styled.View`
  flex: 1;
  align-items: center;
`;
const ImageBrandStyle = styled.Image`
  margin-top: 10px;
  width: 62px;
  height: 22px;
`;

const ImageGymBrandStyle = styled.Image`
  margin-top: 10px;
  width: 62px;
  height: 15px;
`;
const ImageStyle = styled.Image`
  width: 100px;
  height: 100px;
`;
const LoaderWrapper = styled.View`
  flex: 1;
  max-width: 50px;
`;
const ProductSubDetails = styled.View`
  padding-top: 15px;
  flex: 1;
`;
const ProductSubDetailLabel = styled.View`
  min-width: 25%;
`;

const BagPillContainer = styled.View`
  flex-direction: row;
  ${(props) => {
    if (props.pillCount === 1) {
      return `width: 77%`;
    }
    if (props.pillCount === 2) {
      return `width: 82%`;
    }
    return `margin: 0px 10px 0px 10px;`;
  }}
  height: 97px;
  background-color: ${(props) => props.theme.colors.WHITE};
  border-radius: 16px;
  justify-content: space-evenly;
  align-items: center;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
  margin-top: -10px;
  ${(props) =>
    props.error
      ? `width: 90%; border-width: 1px; border-color: ${props.theme.colorPalette.red[500]}`
      : ``}
`;

const TextWrapper = styled.View`
  ${(props) => {
    if (props.pillCount <= 2) {
      return `width: 47%; padding-horizontal: 12px;`;
    }
    return `width: 42%; padding-horizontal: 12px;`;
  }}
  ${(props) => (props.error ? `width: 55%` : ``)}
`;

const BagPillImageWrapper = styled.View`
  flex-direction: row;
  margin-right: ${(props) => (props.multiPill ? '12px' : '0px')};
`;

const ScrollWrapper = styled.ScrollView`
  ${(props) => {
    if (props.pillCount > 2) {
      return `max-width: 195px;`;
    }
    if (props.pillCount === 1) {
      return `max-width: 105px;`;
    }
    return ``;
  }}
`;

const Separator = styled.View`
  border-left-width: 1px;
  border-left-color: ${(props) => props.theme.colors.PRIMARY.LIGHTERGRAY};
  height: 70px;
`;

const QuantityContainer = styled.View`
  ${setBackground}
  width: 18px;
  height: 19px;
  border-radius: 9px;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0;
  bottom: 0;
  border: 1px solid white;
`;

const ErrorIconContainer = styled.View`
  width: 16px;
  height: 16px;
  border-radius: 9px;
  justify-content: center;
  align-items: center;
  position: absolute;
  left: 0;
  top: 0;
`;

const ErrorIconImage = styled.Image`
  width: 100%;
  height: 100%;
`;

const SubTotalSkeletonWrapper = styled.View`
  padding-horizontal: 4px;
  margin-top: 9px;
`;

const ErrorBagPillContainer = styled.View`
  flex-direction: row;
  border-width: 1px;
  border-color: ${(props) => props.theme.colorPalette.red[500]};
  height: 97px;
  background-color: ${(props) => props.theme.colors.WHITE};
  border-radius: 16px;
  justify-content: center;
  align-items: center;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
  margin-top: 10px;
  max-width: 95%;
`;

const ErrorTextWrapper = styled.View`
  margin: 0px 20px 0px 20px;
  flex: 1;
`;

const ErrorScrollWrapper = styled.ScrollView`
  min-width: 100px;
  ${(props) => {
    if (props.pillCount === 1) {
      return `max-width: 100px;`;
    }
    return `max-width: 120px;`;
  }}
`;

export {
  OuterContainer,
  ProductName,
  ProductDesc,
  ImageWrapper,
  ProductDescription,
  ImgWrapper,
  ImageStyle,
  ImageBrandStyle,
  ImageGymBrandStyle,
  ProductSubDetails,
  ProductSubDetailLabel,
  LoaderWrapper,
  BagPillContainer,
  TextWrapper,
  BagPillImageWrapper,
  Separator,
  QuantityContainer,
  ErrorIconContainer,
  ErrorIconImage,
  SubTotalSkeletonWrapper,
  ScrollWrapper,
  ErrorBagPillContainer,
  ErrorTextWrapper,
  ErrorScrollWrapper,
};
