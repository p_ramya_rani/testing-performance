// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Row from '@tcp/core/src/components/common/atoms/Row';
import Col from '@tcp/core/src/components/common/atoms/Col';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import PickupPromotionBanner from '@tcp/core/src/components/common/molecules/PickupPromotionBanner';
import BOSSBannerStyle from '../styles/BossBanner.style';

const getModifiedString = (labels) => {
  const subHeading = `<span className="spanNoRush">${labels.simplyChooseText.replace(
    '#type',
    `<b>${labels.noRushText}</b>`
  )}</span>`;
  return (
    // eslint-disable-next-line react/no-danger
    <span dangerouslySetInnerHTML={{ __html: subHeading }} />
  );
};
const BOSSBanner = ({ labels, isATBModalBackAbTestNewDesign }) => {
  return (
    <React.Fragment>
      <BOSSBannerStyle isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}>
        <PickupPromotionBanner
          bossBanner
          fullBleed
          isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
        />
        <Row className="bossText">
          <Col className="pickUp" key="productDetails" colSize={{ small: 6, medium: 8, large: 12 }}>
            <BodyCopy
              tag="span"
              fontSize="fs11"
              textAlign="center"
              fontFamily="secondary"
              dataLocator="addedtobag-bossbannerdescription"
            >
              {getModifiedString(labels)}
            </BodyCopy>
          </Col>
        </Row>
      </BOSSBannerStyle>
    </React.Fragment>
  );
};

BOSSBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
};

export default BOSSBanner;
