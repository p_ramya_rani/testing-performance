/* eslint-disable no-nested-ternary */
// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable complexity , sonarjs/cognitive-complexity */
import React from 'react';
import PropTypes from 'prop-types';
import { DamImage } from '@tcp/core/src/components/common/atoms';
import PriceCurrency from '@tcp/core/src/components/common/molecules/PriceCurrency';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import {
  ProductName,
  ProductDesc,
  OuterContainer,
  ImgWrapper,
  ProductDescription,
  ImageBrandStyle,
  ImageGymBrandStyle,
  ProductSubDetails,
  ProductSubDetailLabel,
  LoaderWrapper,
  BagPillContainer,
  TextWrapper,
  BagPillImageWrapper,
  Separator,
  QuantityContainer,
  ErrorIconContainer,
  ErrorIconImage,
  SubTotalSkeletonWrapper,
  ScrollWrapper,
  ErrorBagPillContainer,
  ErrorTextWrapper,
  ErrorScrollWrapper,
} from '../styles/ProductInformation.style.native';
import { getLocator } from '../../../../../../../utils';

const gymboreeImage = require('../../../../../../../../../mobileapp/src/assets/images/gymboree-logo.png');
const tcpImage = require('../../../../../../../../../mobileapp/src/assets/images/tcp-logo.png');
const errorIcon = require('../../../../../../../../../mobileapp/src/assets/images/icons-error-message.png');

const showPriceAndPoints = (
  isDoubleAddedToBag,
  hasDoubleItems,
  isSuppressGetOrderDetails,
  isAvoidGetOrderDetails,
  isATBHidePoints
) => {
  return (
    isDoubleAddedToBag &&
    hasDoubleItems &&
    !isATBHidePoints &&
    (!isSuppressGetOrderDetails || isAvoidGetOrderDetails)
  );
};

const showItemBrand = (data) => !data.isGiftCard && data.brand;

const getImageURL = (data) => {
  let { imageUrl } = data.skuInfo;
  const { colorProductId } = data.skuInfo;
  if (imageUrl && imageUrl.includes('wcsstore')) {
    const dataImageObj = imageUrl.split('/');
    if (colorProductId.split('_') && colorProductId.split('_').length)
      imageUrl = `${colorProductId.split('_')[0]}/${imageUrl}`;
    imageUrl = dataImageObj.length ? dataImageObj[dataImageObj.length - 1] : imageUrl;
  }
  return imageUrl;
};

const getBagPill = (data, imageURL, imgConfig, brand, addToBagModalError, isMultiPill) => {
  const { productName, quantity } = data;
  return (
    <BagPillImageWrapper multiPill={isMultiPill}>
      <DamImage
        isProductImage
        width={55}
        height={69}
        isFastImage
        accessibilityLabel={productName}
        url={imageURL}
        imgConfig={imgConfig}
        primaryBrand={brand}
      />
      <QuantityContainer>
        <BodyCopy
          fontSize="fs10"
          fontWeight="extrabold"
          textAlign="center"
          text={quantity}
          color="white"
          fontFamily="secondary"
        />
      </QuantityContainer>
      {(addToBagModalError || !!data.error) && (
        <ErrorIconContainer>
          <ErrorIconImage source={errorIcon} />
        </ErrorIconContainer>
      )}
    </BagPillImageWrapper>
  );
};

const renderLoyaltyLabel = (loyaltyAPICallStatus, labelLoyalty, multiPill, isPlcc) => {
  if ((!loyaltyAPICallStatus && !!labelLoyalty) || (multiPill && !!labelLoyalty))
    return (
      <BodyCopy
        fontSize="fs12"
        fontWeight="extrabold"
        textAlign="center"
        text={labelLoyalty}
        color={isPlcc ? 'blue.B100' : 'orange.800'}
        fontFamily="secondary"
      />
    );
  return null;
};

const renderNewDesignPill = (args) => {
  const {
    data,
    imageUrl,
    dataFromApi,
    addToBagModalError,
    labels,
    loyaltyAPICallStatus,
    aTbAPICallStatus,
    isPlcc,
    primaryBrand,
    alternateBrand,
    itemInfo,
  } = args;
  const { rewardLoyaltyPart1, rewardLoyaltyPart2, pointsLoyaltyPart1, pointsLoyaltyPart2 } = labels;
  const { earnedReward, estimatedRewards } = dataFromApi;
  const multiPill = dataFromApi && Array.isArray(dataFromApi) && dataFromApi.length;
  let grandTotal = '';
  if (dataFromApi.grandTotal && dataFromApi.grandTotal > 0)
    grandTotal = `$${dataFromApi.grandTotal.toFixed(2)}`;
  let rewardAmount = earnedReward?.split(' ')[0];
  let labelLoyalty = '';
  let errorProducts = [];
  let successProducts = [];
  let errorProductsQuantity = 0;

  if (multiPill) {
    rewardAmount = data.earnedReward?.split(' ')[0];
    if (data.grandTotal && data.grandTotal > 0) grandTotal = `$${data.grandTotal.toFixed(2)}`;
    if (rewardAmount) labelLoyalty = `${rewardLoyaltyPart1} ${rewardAmount} ${rewardLoyaltyPart2}`;
    errorProducts = itemInfo.filter((item) => item.error);
    successProducts = itemInfo.filter((item) => !item.error);
    errorProductsQuantity =
      errorProducts.length &&
      errorProducts.reduce((accumulator, currentValue) => accumulator + currentValue.quantity, 0);
  } else if (rewardAmount) {
    labelLoyalty = `${rewardLoyaltyPart1} ${rewardAmount} ${rewardLoyaltyPart2}`;
  } else if (estimatedRewards && estimatedRewards > 0)
    labelLoyalty = `${pointsLoyaltyPart1} ${estimatedRewards} ${pointsLoyaltyPart2}`;

  const CustomScrollStyle = { paddingHorizontal: 24 };

  return (
    <>
      {((multiPill && successProducts.length > 0) || !multiPill) && (
        <BagPillContainer
          error={addToBagModalError}
          pillCount={multiPill ? successProducts.length : 1}
        >
          <TextWrapper
            error={addToBagModalError}
            pillCount={multiPill ? successProducts.length : 1}
          >
            {!addToBagModalError ? (
              <>
                <BodyCopy
                  fontSize="fs16"
                  fontWeight="bold"
                  textAlign="center"
                  text={`${labels.bagSubTotalRedesign}`}
                  fontFamily="secondary"
                  color="gray.900"
                />

                {!aTbAPICallStatus || multiPill ? (
                  <BodyCopy
                    fontSize="fs16"
                    fontWeight="regular"
                    textAlign="center"
                    text={grandTotal}
                    fontFamily="secondary"
                    color="gray.900"
                  />
                ) : (
                  <SubTotalSkeletonWrapper>
                    <LoaderSkelton height={12} />
                  </SubTotalSkeletonWrapper>
                )}
                {renderLoyaltyLabel(loyaltyAPICallStatus, labelLoyalty, multiPill, isPlcc)}
              </>
            ) : (
              <BodyCopy
                fontSize="fs12"
                fontWeight="semibold"
                textAlign="left"
                text={`${addToBagModalError}`}
                color="red.500"
                fontFamily="secondary"
              />
            )}
          </TextWrapper>
          <Separator />

          {multiPill && (
            <ScrollWrapper
              horizontal
              showsHorizontalScrollIndicator={false}
              pillCount={multiPill ? successProducts.length : 1}
              contentContainerStyle={CustomScrollStyle}
              scrollEnabled={successProducts.length > 1}
            >
              <BagPillImageWrapper>
                {successProducts.map((item) =>
                  getBagPill(
                    item,
                    getImageURL(item),
                    successProducts.imgConfig,
                    primaryBrand || alternateBrand,
                    addToBagModalError,
                    true
                  )
                )}
              </BagPillImageWrapper>
            </ScrollWrapper>
          )}

          {!multiPill && (
            <BagPillImageWrapper>
              {getBagPill(data, imageUrl, data.imgConfig, primaryBrand || alternateBrand)}
              {addToBagModalError && (
                <ErrorIconContainer>
                  <ErrorIconImage source={errorIcon} />
                </ErrorIconContainer>
              )}
            </BagPillImageWrapper>
          )}
        </BagPillContainer>
      )}

      {errorProducts.length > 0 && (
        <>
          <BodyCopy
            fontSize="fs20"
            fontWeight="bold"
            textAlign="center"
            text={`${labels.oopsAddedToBag} ${errorProductsQuantity} ${
              errorProductsQuantity > 1 ? labels.itemsNotAddedToBag : labels.itemNotAddedToBag
            }`}
            fontFamily="secondary"
            color="red.500"
          />
          <ErrorBagPillContainer
            error={addToBagModalError}
            pillCount={multiPill ? errorProducts.length : 1}
          >
            <ErrorTextWrapper>
              <BodyCopy
                fontSize="fs12"
                fontWeight="semibold"
                textAlign="left"
                text={errorProducts[0].errorMessage}
                color="red.500"
                fontFamily="secondary"
              />
            </ErrorTextWrapper>

            <Separator />

            <ErrorScrollWrapper
              horizontal
              showsHorizontalScrollIndicator={false}
              pillCount={multiPill ? errorProducts.length : 1}
              contentContainerStyle={CustomScrollStyle}
              scrollEnabled={errorProducts.length > 1}
            >
              <BagPillImageWrapper>
                {errorProducts.map((item) =>
                  getBagPill(
                    item,
                    getImageURL(item),
                    errorProducts.imgConfig,
                    primaryBrand || alternateBrand,
                    addToBagModalError,
                    true
                  )
                )}
              </BagPillImageWrapper>
            </ErrorScrollWrapper>
          </ErrorBagPillContainer>
        </>
      )}
    </>
  );
};

const ProductInformation = ({
  data,
  dataFromApi,
  labels,
  quantity,
  isDoubleAddedToBag,
  bagLoading,
  isSuppressGetOrderDetails,
  hasDoubleItems,
  isAvoidGetOrderDetails,
  isATBHidePoints,
  isNewReDesignEnabled,
  addToBagModalError,
  loyaltyAPICallStatus,
  aTbAPICallStatus,
  isPlcc,
  primaryBrand,
  alternateBrand,
  itemInfo,
}) => {
  let { imageUrl } = data.skuInfo;
  const { colorProductId } = data.skuInfo;
  if (imageUrl && imageUrl.includes('wcsstore')) {
    const dataImageObj = imageUrl.split('/');
    imageUrl = dataImageObj.length ? dataImageObj[dataImageObj.length - 1] : imageUrl;
    if (colorProductId.split('_') && colorProductId.split('_').length)
      imageUrl = `${colorProductId.split('_')[0]}/${imageUrl}`;
  }

  if (isNewReDesignEnabled)
    return renderNewDesignPill({
      data,
      imageUrl,
      dataFromApi,
      addToBagModalError,
      labels,
      loyaltyAPICallStatus,
      aTbAPICallStatus,
      isPlcc,
      primaryBrand,
      alternateBrand,
      itemInfo,
    });
  return (
    <OuterContainer>
      <ImgWrapper>
        {/* <ImageStyle source={{ uri: endpoints.global.baseURI + data.skuInfo.imageUrl }} /> */}
        <DamImage
          isProductImage
          width={80}
          height={100}
          isFastImage
          accessibilityLabel={data.productName}
          url={imageUrl}
          imgConfig={data.imgConfig}
          primaryBrand={primaryBrand || alternateBrand}
        />
        {showItemBrand(data) ? (
          data.brand === 'TCP' ? (
            <ImageBrandStyle
              data-locator={getLocator('cart_item_brand_logo')}
              source={tcpImage}
              accessibilityLabel={labels.tcpBrandAlt}
            />
          ) : (
            <ImageGymBrandStyle
              data-locator={getLocator('cart_item_brand_logo')}
              source={gymboreeImage}
              accessibilityLabel={labels.gymBrandAlt}
            />
          )
        ) : null}
      </ImgWrapper>
      <ProductDescription>
        <ProductName>
          <BodyCopy fontSize="fs12" fontWeight={['semibold']} text={data.productName} />
        </ProductName>
        <ProductSubDetails>
          <ProductDesc>
            <ProductSubDetailLabel>
              <BodyCopy
                fontSize="fs12"
                fontWeight={['semibold']}
                textAlign="left"
                text={
                  data.isGiftCard === true ? `${labels.giftDesign}: ` : `${labels.colorLabel}: `
                }
              />
            </ProductSubDetailLabel>

            <BodyCopy fontSize="fs12" text={data.skuInfo.color.name} />
          </ProductDesc>
          <ProductDesc>
            <ProductSubDetailLabel>
              <BodyCopy
                fontSize="fs12"
                fontWeight={['semibold']}
                textAlign="left"
                text={data.isGiftCard === true ? `${labels.giftValue}: ` : `${labels.sizeLabel}: `}
              />
            </ProductSubDetailLabel>

            <BodyCopy fontSize="fs12" text={`${data.skuInfo.size} `} />
            <BodyCopy fontSize="fs12" text={!data.skuInfo.fit ? ' ' : data.skuInfo.fit} />
          </ProductDesc>
          <ProductDesc>
            <ProductSubDetailLabel>
              <BodyCopy
                fontSize="fs12"
                fontWeight={['semibold']}
                textAlign="left"
                text={`${labels.qtyLabel}: `}
              />
            </ProductSubDetailLabel>

            <BodyCopy fontSize="fs12" text={quantity || data.quantity} />
          </ProductDesc>
          {isATBHidePoints && (
            <ProductDesc>
              <ProductSubDetailLabel>
                <BodyCopy
                  fontSize="fs12"
                  fontWeight={['semibold']}
                  textAlign="left"
                  text={`${labels.price}: `}
                />
              </ProductSubDetailLabel>

              <BodyCopy fontSize="fs12" text={<PriceCurrency price={Number(data.itemPrice)} />} />
            </ProductDesc>
          )}
          {showPriceAndPoints(
            isDoubleAddedToBag,
            hasDoubleItems,
            isSuppressGetOrderDetails,
            isAvoidGetOrderDetails,
            isATBHidePoints
          ) && (
            <>
              <ProductDesc>
                <ProductSubDetailLabel>
                  <BodyCopy
                    fontSize="fs12"
                    fontWeight={['semibold']}
                    textAlign="left"
                    text={`${labels.price}: `}
                  />
                </ProductSubDetailLabel>
                {!bagLoading ? (
                  <BodyCopy
                    fontSize="fs12"
                    fontWeight={['semibold']}
                    text={<PriceCurrency price={Number(data.listPrice)} />}
                  />
                ) : (
                  <LoaderWrapper>
                    <LoaderSkelton height={12} />
                  </LoaderWrapper>
                )}
              </ProductDesc>
              <ProductDesc>
                <ProductSubDetailLabel>
                  <BodyCopy
                    fontSize="fs12"
                    fontWeight={['semibold']}
                    textAlign="left"
                    text={`${labels.points}: `}
                  />
                </ProductSubDetailLabel>
                {!bagLoading ? (
                  <BodyCopy
                    fontSize="fs12"
                    fontWeight={['semibold']}
                    text={data.itemPoints}
                    color="orange.800"
                  />
                ) : (
                  <LoaderWrapper>
                    <LoaderSkelton height={12} />
                  </LoaderWrapper>
                )}
              </ProductDesc>
            </>
          )}
        </ProductSubDetails>
      </ProductDescription>
    </OuterContainer>
  );
};

ProductInformation.propTypes = {
  data: PropTypes.shape,
  labels: PropTypes.shape,
  quantity: PropTypes.string,
  isDoubleAddedToBag: PropTypes.bool,
  hasDoubleItems: PropTypes.bool,
  isSuppressGetOrderDetails: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isAvoidGetOrderDetails: PropTypes.bool,
  isATBHidePoints: PropTypes.bool,
  dataFromApi: PropTypes.shape({}),
  isNewReDesignEnabled: PropTypes.bool,
  addToBagModalError: PropTypes.string,
  loyaltyAPICallStatus: PropTypes.bool,
  aTbAPICallStatus: PropTypes.bool,
  isPlcc: PropTypes.bool,
};
ProductInformation.defaultProps = {
  data: {},
  labels: {},
  quantity: '',
  hasDoubleItems: false,
  isDoubleAddedToBag: false,
  isAvoidGetOrderDetails: false,
  isATBHidePoints: false,
  dataFromApi: {},
  isNewReDesignEnabled: false,
  addToBagModalError: null,
  loyaltyAPICallStatus: false,
  aTbAPICallStatus: false,
  isPlcc: false,
};
export default withStyles(ProductInformation);
export { ProductInformation as ProductInformationVanilla };
