// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { getIconPath, capitalizeWords } from '@tcp/core/src/utils/utils';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Button from '@tcp/core/src/components/common/atoms/Button';
import { isClient } from '@tcp/core/src/utils';
import PickupBannerStyle from '../styles/PickupBanner.style';
import { getIsStoreBopisEligible, errorOccurred, usePrevious } from './PickupBanner.utils';

const getHeading = (
  itemAddedForPickup,
  pickupInsteadHeadingSuccess,
  pickupInsteadHeading,
  newPickupInsteadReady,
  isATBModalBackAbTestNewDesign
) => {
  const newHeading = isATBModalBackAbTestNewDesign ? pickupInsteadHeading : newPickupInsteadReady;
  return itemAddedForPickup ? pickupInsteadHeadingSuccess : newHeading;
};

const getIsCallBoopis = () => {
  const { pathname = '' } = (window && window.location) || {};
  return isClient() && (pathname.indexOf('/outfit/') !== -1 || pathname.indexOf('/b/') !== -1);
};

const getStoreSize = (isATBModalBackAbTestNewDesign) => {
  return isATBModalBackAbTestNewDesign ? 'fs14' : 'fs12';
};

const getPickUpFromStoreText = (
  isATBModalBackAbTestNewDesign,
  pickupInsteadFreeOrder,
  pickupInsteadFromStore
) => {
  return isATBModalBackAbTestNewDesign ? pickupInsteadFreeOrder : pickupInsteadFromStore;
};

const PickupBanner = ({
  labels,
  addedToBagData = {},
  getBopisInventoryDetails,
  userDefaultStore = {},
  bopisItemInventory = [],
  addItemToCartInPickup = () => {},
  addToPickupError = {},
  setAddToPickupError = () => {},
  isATBModalBackAbTestNewDesign,
}) => {
  const {
    pickupInsteadHeading,
    pickupInsteadHeadingSuccess,
    pickupInsteadReady,
    pickupInsteadFreeOrder,
    pickupInsteadItemAdded,
    pickupInsteadBtnText,
    pickupInsteadHeadingError,
    pickupInsteadMsgError,
    newPickupInsteadReady,
    pickupInsteadFromStore,
  } = labels;
  const { skuInfo = {}, quantity = '', orderItemId: itemId = '', orderId = '' } = addedToBagData;
  const { variantId: itemPartNumber = '', variantNo = '', skuId = '' } = skuInfo;
  const [itemAddedForPickup, updateItemAdded] = useState(false);

  const isStoreBopisEligible = getIsStoreBopisEligible(bopisItemInventory);

  const { basicInfo = {} } = userDefaultStore;
  const { storeName = '', id = '' } = basicInfo;
  const capitalizeStoreName = capitalizeWords(storeName);

  const getInventoryDetails = () => {
    const itemInfo = [
      {
        storeId: id.substring(2),
        variantNo,
        itemPartNumber,
      },
    ];
    getBopisInventoryDetails({ itemInfo });
  };

  const prevDefaultStoreId = usePrevious(id) || '';
  const isMounted = useRef(null);

  useEffect(() => {
    setAddToPickupError('');
    if (getIsCallBoopis()) {
      getInventoryDetails();
    }
  }, []);

  useEffect(() => {
    if (!isMounted.current && prevDefaultStoreId !== id) {
      getInventoryDetails();
      isMounted.current = true;
    }
  }, [userDefaultStore]);

  const addedCallback = () => {
    updateItemAdded(true);
  };

  const createBossBopisTogglePayload = () => {
    return {
      apiPayload: {
        orderId: `${orderId}`,
        orderItem: [
          {
            orderItemId: itemId,
            xitem_catEntryId: skuId,
            quantity: `${quantity}`,
            variantNo,
            itemPartNumber,
          },
        ],
        x_storeLocId: id,
        x_orderitemtype: 'ECOM', // source type of Item
        x_updatedItemType: 'BOPIS', // target type of Item
      },
      updateActionType: 'UpdatePickUpItem',
      fromTogglingBossBopis: true,
      callback: addedCallback,
      isAddedToBag: true,
    };
  };

  const onPickupClick = () => {
    const payload = createBossBopisTogglePayload();
    addItemToCartInPickup(payload);
  };

  const heading = getHeading(
    itemAddedForPickup,
    pickupInsteadHeadingSuccess,
    pickupInsteadHeading,
    newPickupInsteadReady,
    isATBModalBackAbTestNewDesign
  );
  const errorOccured = errorOccurred(addToPickupError);
  return (
    <React.Fragment>
      {isStoreBopisEligible && (
        <PickupBannerStyle>
          {errorOccured && (
            <div className="pickup-banner-container">
              <div className="content-wrap">
                <BodyCopy
                  component="p"
                  fontSize="fs16"
                  textAlign="left"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                >
                  {pickupInsteadHeadingError}
                </BodyCopy>
                <BodyCopy
                  component="p"
                  fontSize="fs14"
                  textAlign="left"
                  className="elem-mt-SM error-message-para"
                >
                  <img
                    alt="warning icon"
                    src={getIconPath('circle-warning-fill')}
                    className="error-icon elem-mr-MED"
                  />
                  <BodyCopy component="span" fontWeight="bold" fontFamily="secondary">
                    {pickupInsteadMsgError}
                  </BodyCopy>
                </BodyCopy>
              </div>
            </div>
          )}
          {!errorOccured && (
            <div className="pickup-banner-container">
              <div className="content-wrap">
                <BodyCopy
                  component="p"
                  fontSize="fs16"
                  textAlign="left"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  color={itemAddedForPickup ? 'green.500' : ''}
                >
                  {heading}
                </BodyCopy>
                <BodyCopy
                  component="p"
                  className="pickup-banner-store-text"
                  fontSize={getStoreSize(isATBModalBackAbTestNewDesign)}
                  textAlign="left"
                >
                  {!itemAddedForPickup && (
                    <BodyCopy
                      component="span"
                      fontWeight="semibold"
                      color="green.500"
                      fontFamily="secondary"
                    >
                      {pickupInsteadReady}
                    </BodyCopy>
                  )}
                  {!itemAddedForPickup && (
                    <BodyCopy component="span" fontWeight="normal" fontFamily="secondary">
                      {getPickUpFromStoreText(
                        isATBModalBackAbTestNewDesign,
                        pickupInsteadFreeOrder,
                        pickupInsteadFromStore
                      )}
                    </BodyCopy>
                  )}
                  {itemAddedForPickup && (
                    <BodyCopy component="span" fontWeight="normal" fontFamily="secondary">
                      {pickupInsteadItemAdded}
                    </BodyCopy>
                  )}
                  <BodyCopy
                    component="span"
                    fontWeight="bold"
                    fontFamily="secondary"
                    className="store-name"
                  >
                    {capitalizeStoreName}
                  </BodyCopy>
                </BodyCopy>
              </div>
              {!itemAddedForPickup && (
                <Button className="pickup-cta" fill="BLUE" type="button" onClick={onPickupClick}>
                  <BodyCopy
                    component="span"
                    color="white"
                    fontWeight="extrabold"
                    fontFamily="secondary"
                    fontSize={isATBModalBackAbTestNewDesign ? 'fs14' : 'fs13'}
                  >
                    {pickupInsteadBtnText}
                  </BodyCopy>
                </Button>
              )}
            </div>
          )}
        </PickupBannerStyle>
      )}
    </React.Fragment>
  );
};

PickupBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  addedToBagData: PropTypes.shape({}).isRequired,
  userDefaultStore: PropTypes.shape({}).isRequired,
  bopisItemInventory: PropTypes.shape([]).isRequired,
  getBopisInventoryDetails: PropTypes.func.isRequired,
  addItemToCartInPickup: PropTypes.func.isRequired,
  addToPickupError: PropTypes.string.isRequired,
  setAddToPickupError: PropTypes.func.isRequired,
};

export default PickupBanner;
