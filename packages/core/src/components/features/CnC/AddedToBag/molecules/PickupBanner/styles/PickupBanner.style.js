// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export default styled.div`
  .pickup-banner-container {
    background: ${(props) => props.theme.colorPalette.gray[300]};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM}
      ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    display: flex;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED} -${(props) => props.theme.spacing.ELEM_SPACING.MED};
    ${(props) =>
      props.isATBModalBackAbTestNewDesign
        ? ''
        : `
    border-radius: 6px;
    margin: 7px 0 32px 0;`}
  }
  .pickup-banner-store-text {
    padding-bottom: 5px;
    margin-top: 16px;
  }
  .content-wrap {
    flex: 1;
  }
  .pickup-cta {
    background: ${(props) => props.theme.colorPalette.blue.C900};
    height: 42px;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    min-width: 103px;
    margin-left: 6px;
    align-self: ${(props) => (props.isATBModalBackAbTestNewDesign ? 'center' : 'flex-end')};
    border-radius: ${(props) => (props.isATBModalBackAbTestNewDesign ? '0px' : '6px')};
  }
  .error-icon {
    width: 24px;
    height: 24px;
  }
  .error-message-para {
    display: flex;
  }
  .store-name {
    text-transform: capitalize;
  }
`;
