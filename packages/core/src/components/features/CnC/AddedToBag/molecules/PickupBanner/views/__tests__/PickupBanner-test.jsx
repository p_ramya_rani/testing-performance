// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import PickupBannerView from '../PickupBanner.view';

describe('PickupBannerView component', () => {
  it('renders correctly', () => {
    const props = {
      className: 'className',
      labels: {
        pickUpText: 'PICK UP IN STORE AND SAVE AN EXTRA 5%',
        simplyChooseText: 'Simply choose #type in your bag before checking out.',
        noRushText: 'NO RUSH Pick Up',
      },
      bopisItemInventory: {
        inventoryResponse: [{ quantity: 100 }],
      },
      isATBModalBackAbTestNewDesign: true,
    };
    const component = shallow(<PickupBannerView {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly for isATBModalBackAbTestNewDesign', () => {
    const props = {
      className: 'className',
      labels: {
        pickUpText: 'PICK UP IN STORE AND SAVE AN EXTRA 5%',
        simplyChooseText: 'Simply choose #type in your bag before checking out.',
        noRushText: 'NO RUSH Pick Up',
      },
      bopisItemInventory: {
        inventoryResponse: [{ quantity: 100 }],
      },
      isATBModalBackAbTestNewDesign: false,
    };
    const component = shallow(<PickupBannerView {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('does not renders error message', () => {
    const props = {
      addToPickupError: null,
      bopisItemInventory: {
        inventoryResponse: [{ quantity: 100 }],
      },
      labels: {},
    };
    const component = shallow(<PickupBannerView {...props} />);
    expect(component.find('.error-message-para')).toHaveLength(0);
  });

  it('renders error message', () => {
    const props = {
      addToPickupError: 'Something went wrong',
      bopisItemInventory: {
        inventoryResponse: [{ quantity: 100 }],
      },
      labels: {},
    };
    const component = shallow(<PickupBannerView {...props} />);
    expect(component.find('.error-message-para')).toHaveLength(1);
  });
});
