// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import ProductInformationView from '../ProductInformation.views';

describe('BossBannerView component', () => {
  const props = {
    data: {
      productName: 'Boys Basic Skinny Jeans',
      skuInfo: {
        color: {
          family: 'RED',
        },
        fit: 'slim',
        size: 'M',
      },
      quantity: '1',
    },
    labels: {
      colorLabel: 'Color',
      sizeLabel: 'Size',
      qtyLabel: 'Qty',
    },
    isATBModalBackAbTestNewDesign: true,
  };
  it('renders correctly', () => {
    const component = shallow(<ProductInformationView {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly for isATBModalBackAbTestNewDesign', () => {
    const newProps = {
      ...props,
      isATBModalBackAbTestNewDesign: false,
    };
    const component = shallow(<ProductInformationView {...newProps} />);
    expect(component).toMatchSnapshot();
  });
});
