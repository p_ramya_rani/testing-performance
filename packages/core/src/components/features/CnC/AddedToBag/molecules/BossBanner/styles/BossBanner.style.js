// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export default styled.div`
  text-align: center;
  ${(props) => (props.isATBModalBackAbTestNewDesign ? '' : `margin-bottom: 32px;`)}
  .bossText {
    ${(props) =>
      props.isATBModalBackAbTestNewDesign
        ? 'margin-top: 10px;'
        : `
    margin: 10px 0 0 0;
    width: 100%;
    `}
  }
`;
