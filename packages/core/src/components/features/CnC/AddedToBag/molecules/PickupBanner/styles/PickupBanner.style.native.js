// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const BannerViewWrapper = styled.View`
  background: ${props => props.theme.colorPalette.gray[300]};
  display: flex;
  flex-direction: row;
  margin: 0 -10px;
  align-items: center;
  justify-content: space-between;
`;

export const TextViewWrapper = styled.View`
  display: flex;
  flex-direction: row;
  align-content: center;
  flex-wrap: wrap;
`;

export const ErrViewWrapper = styled.View`
  display: flex;
  flex-direction: row;
  align-content: center;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export const ContentWrapper = styled.View`
  margin: ${props => props.theme.spacing.ELEM_SPACING.SM};
  width: ${props => (props.fullWidth ? '80%' : '200px')};
`;

export const ButtonWrapper = styled.View`
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export const StyledWarningImage = styled.Image`
  margin: ${props => props.theme.spacing.ELEM_SPACING.XXS}
    ${props => props.theme.spacing.ELEM_SPACING.SM};
  width: 26px;
`;
