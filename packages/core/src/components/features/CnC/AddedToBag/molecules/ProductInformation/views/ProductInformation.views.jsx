// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable complexity , sonarjs/cognitive-complexity */
import React from 'react';
import PropTypes from 'prop-types';
import Row from '@tcp/core/src/components/common/atoms/Row';
import Col from '@tcp/core/src/components/common/atoms/Col';
import { Image } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { getIconPath, getLocator, isCanada, getBrand, getImageData } from '@tcp/core/src/utils';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import { capitalizeWords, getBrandNameFromHref } from '@tcp/core/src/utils/utils';
import DamImage from '../../../../../../common/atoms/DamImage';
import ProductInformationStyle from '../styles/ProductInformation.style';

const getColConfig = (isATBModalBackAbTestNewDesign) => {
  return isATBModalBackAbTestNewDesign
    ? { small: 4, medium: 6, large: 9 }
    : { small: 4.3, medium: 5.5, large: 8.5 };
};

const showPoints = (isInternationalShipping, isSuppressGetOrderDetails, isAvoidGetOrderDetails) => {
  return (
    !isCanada() &&
    !isInternationalShipping &&
    (!isSuppressGetOrderDetails || isAvoidGetOrderDetails)
  );
};

const showItemPoints = (bagLoading, data) => {
  return !bagLoading ? data.itemPoints : <LoaderSkelton width="50px" />;
};

const showPriceAndPoints = (
  isDoubleAddedToBag,
  hasDoubleItems,
  isSuppressGetOrderDetails,
  isAvoidGetOrderDetails
) => {
  return (
    isDoubleAddedToBag && hasDoubleItems && (!isSuppressGetOrderDetails || isAvoidGetOrderDetails)
  );
};

const showItemBrand = (data) => {
  return !data.isGiftCard && data.brand;
};

const setBrandIfNotPresent = (data, primaryBrand, alternateBrandParam) => {
  const alternateBrand = getBrandNameFromHref();
  const dataInfo = data;
  const brand = getBrand();
  dataInfo.brand = primaryBrand || alternateBrand || alternateBrandParam || dataInfo.brand || brand;
  dataInfo.brand = (dataInfo.brand || '').toUpperCase();
};

const getSizeAndQty = (data, isATBModalBackAbTestNewDesign, quantity, qtyLabel = '') => {
  let sizeSuffix = `${quantity || data.quantity} ${qtyLabel.toLowerCase()}`;
  if (isATBModalBackAbTestNewDesign) {
    sizeSuffix =
      data &&
      data.skuInfo &&
      ((data.skuInfo.fit && data.skuInfo.fit.name === '') || data.skuInfo.fit === null)
        ? ' '
        : data.skuInfo.fit;
  }
  return sizeSuffix;
};

const ProductInformation = ({
  data,
  labels,
  inheritedStyles,
  quantity,
  isDoubleAddedToBag,
  bagLoading,
  hasDoubleItems,
  isSuppressGetOrderDetails,
  isInternationalShipping,
  isAvoidGetOrderDetails,
  isATBHidePoints,
  isATBModalBackAbTestNewDesign,
  primaryBrand,
  alternateBrand,
}) => {
  setBrandIfNotPresent(data, primaryBrand, alternateBrand);
  let { imageUrl } = data.skuInfo;
  const { colorProductId } = data.skuInfo;
  if (imageUrl && imageUrl.includes('wcsstore')) {
    const dataImageObj = imageUrl.split('/');
    imageUrl = dataImageObj.length ? dataImageObj[dataImageObj.length - 1] : imageUrl;
    imageUrl =
      colorProductId.split('_') && colorProductId.split('_').length
        ? `${colorProductId.split('_')[0]}/${imageUrl}`
        : imageUrl;
  }
  const configData = getImageData(imageUrl);
  const { itemPrice, skuInfo: { offerPrice } = {} } = data;

  return (
    <ProductInformationStyle
      inheritedStyles={inheritedStyles}
      isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
    >
      <Row tagName="ul" className="product">
        <Col
          tagName="li"
          key="productDetails"
          className="product-logo"
          colSize={{ small: 2, medium: 2, large: 3 }}
        >
          <DamImage
            imgData={{
              alt: 'Product',
              url: imageUrl,
            }}
            imgConfigs={configData.imgConfig}
            className="product-image"
            data-locator="addedtobag-productimage"
            isProductImage
            itemBrand={data.brand}
            isOptimizedVideo
          />
          {showItemBrand(data) &&
            (data.brand === 'GYM' ? (
              <Image
                className="brand-image"
                src={getIconPath(`header__brand-tab-gymboree`)}
                alt={labels.gymBrandAlt}
                data-locator={getLocator('header__brand-tab--tcp')}
              />
            ) : (
              <Image
                className="brand-image"
                src={getIconPath(`header__brand-tab--tcp`)}
                alt={labels.tcpBrandAlt}
                data-locator={getLocator('header__brand-tab--tcp')}
              />
            ))}
        </Col>
        <Col
          tagName="li"
          key="productDetails"
          colSize={getColConfig(isATBModalBackAbTestNewDesign)}
          className="atb-product-detail-section"
        >
          <Row tagName="ul" className="product-details">
            <Col
              tagName="li"
              key="product-title"
              className="productImgBrand"
              colSize={{ small: 6, medium: 8, large: 12 }}
            >
              <BodyCopy
                tag="span"
                fontSize="fs14"
                fontWeight={isATBModalBackAbTestNewDesign ? ['semibold'] : ['extrabold']}
                textAlign="left"
                className="product-title"
                dataLocator="addedtobag-productname"
                fontFamily="secondary"
              >
                {data.productName}
              </BodyCopy>
            </Col>
          </Row>
          <Row tagName="ul" className="product-description">
            {isATBModalBackAbTestNewDesign && (
              <Col
                tagName="li"
                key="product-title"
                className="itemList"
                colSize={{ small: 2, medium: 3, large: 4 }}
              >
                <BodyCopy
                  tag="span"
                  fontSize="fs13"
                  fontWeight={['semibold']}
                  textAlign="left"
                  fontFamily="secondary"
                >
                  {data.isGiftCard === true ? `${labels.giftDesign}: ` : `${labels.colorLabel}: `}
                </BodyCopy>
              </Col>
            )}
            <Col
              tagName="li"
              key="product-title"
              className="itemList"
              colSize={{ small: 4, medium: 5, large: 8 }}
            >
              <BodyCopy
                tag="span"
                fontSize="fs13"
                textAlign="left"
                className="itemDesc"
                dataLocator="addedtobag-productcolor"
                fontFamily="secondary"
              >
                {isATBModalBackAbTestNewDesign
                  ? data.skuInfo.color.name
                  : capitalizeWords(data.skuInfo.color.name)}
              </BodyCopy>
            </Col>
          </Row>
          <Row tagName="ul" className="product-description">
            {isATBModalBackAbTestNewDesign && (
              <Col
                tagName="li"
                key="product-title"
                className="itemList"
                colSize={{ small: 2, medium: 3, large: 4 }}
              >
                <BodyCopy
                  tag="span"
                  fontSize="fs13"
                  fontWeight={['semibold']}
                  textAlign="left"
                  fontFamily="secondary"
                >
                  {data.isGiftCard === true ? `${labels.giftValue}: ` : `${labels.sizeLabel}: `}
                </BodyCopy>
              </Col>
            )}
            <Col tagName="li" key="product-title" colSize={{ small: 4, medium: 5, large: 8 }}>
              <BodyCopy
                tag="span"
                fontSize={isATBModalBackAbTestNewDesign ? 'fs13' : 'fs12'}
                textAlign="left"
                className="itemDesc"
                dataLocator="addedtobag-productsize"
                fontFamily="secondary"
              >
                {`${data.skuInfo.size} `}
                {!isATBModalBackAbTestNewDesign && (
                  <BodyCopy
                    component="span"
                    fontSize="fs13"
                    textAlign="left"
                    className="itemDesc"
                    color="gray.500"
                    dataLocator="addedtobag-productsize"
                    fontFamily="secondary"
                  >
                    {'| '}
                  </BodyCopy>
                )}
                {getSizeAndQty(data, isATBModalBackAbTestNewDesign, quantity, labels.qtyLabel)}
              </BodyCopy>
            </Col>
          </Row>
          {isATBModalBackAbTestNewDesign && (
            <Row tagName="ul" className="product-description">
              <Col
                tagName="li"
                key="product-title"
                className="itemList"
                colSize={{ small: 2, medium: 3, large: 4 }}
              >
                <BodyCopy
                  tag="span"
                  fontSize="fs13"
                  fontWeight={['semibold']}
                  textAlign="left"
                  fontFamily="secondary"
                >
                  {labels.qtyLabel}
                  {':'}
                </BodyCopy>
              </Col>
              <Col
                tagName="li"
                key="product-title"
                className="itemList"
                colSize={{ small: 4, medium: 5, large: 8 }}
              >
                <BodyCopy
                  tag="span"
                  fontSize="fs13"
                  textAlign="left"
                  className="itemDesc"
                  dataLocator="addedtobag-productqty"
                  fontFamily="secondary"
                >
                  {quantity || data.quantity}
                </BodyCopy>
              </Col>
            </Row>
          )}
          <Row className="addded-to-bag-portal-wrapper">
            <div id="added-to-bag-portal" className="added-to-bag-points-portal" />
          </Row>
          {isATBHidePoints && (
            <Row tagName="ul" className="product-description">
              <Col
                tagName="li"
                key="product-title"
                className="itemList"
                colSize={{ small: 2, medium: 3, large: 4 }}
              >
                <BodyCopy
                  tag="span"
                  fontSize="fs13"
                  fontWeight={['semibold']}
                  textAlign="left"
                  fontFamily="secondary"
                >
                  {labels.price}
                  {':'}
                </BodyCopy>
              </Col>
              <Col
                tagName="li"
                key="product-title"
                className="itemList"
                colSize={{ small: 4, medium: 5, large: 8 }}
              >
                <BodyCopy
                  tag="span"
                  fontSize="fs13"
                  textAlign="left"
                  className="itemDesc"
                  dataLocator="addedtobag-productqty"
                  fontFamily="secondary"
                >
                  {!bagLoading && (itemPrice || offerPrice) ? (
                    <PriceCurrency price={itemPrice || offerPrice} />
                  ) : (
                    '-'
                  )}
                </BodyCopy>
              </Col>
            </Row>
          )}
          {showPriceAndPoints(
            isDoubleAddedToBag,
            hasDoubleItems,
            isSuppressGetOrderDetails,
            isAvoidGetOrderDetails
          ) && (
            <>
              <Row tagName="ul" className="product-description">
                <Col
                  tagName="li"
                  key="product-title"
                  className="itemList"
                  colSize={{ small: 2, medium: 3, large: 4 }}
                >
                  <BodyCopy
                    tag="span"
                    fontSize="fs13"
                    fontWeight={['semibold']}
                    textAlign="left"
                    fontFamily="secondary"
                  >
                    {labels.price}
                    {':'}
                  </BodyCopy>
                </Col>
                <Col
                  tagName="li"
                  key="product-title"
                  className="itemList"
                  colSize={{ small: 4, medium: 5, large: 8 }}
                >
                  <BodyCopy
                    tag="span"
                    fontSize="fs13"
                    textAlign="left"
                    className="itemDesc"
                    dataLocator="addedtobag-productprice"
                    fontWeight={['semibold']}
                    fontFamily="secondary"
                  >
                    {!bagLoading && data.listPrice ? <PriceCurrency price={data.listPrice} /> : '-'}
                  </BodyCopy>
                </Col>
              </Row>
              {showPoints(
                isInternationalShipping,
                isSuppressGetOrderDetails,
                isAvoidGetOrderDetails
              ) && (
                <Row tagName="ul" className="product-description">
                  <Col
                    tagName="li"
                    key="product-title"
                    className="itemList"
                    colSize={{ small: 2, medium: 3, large: 4 }}
                  >
                    <BodyCopy
                      tag="span"
                      fontSize="fs13"
                      fontWeight={['semibold']}
                      textAlign="left"
                      fontFamily="secondary"
                    >
                      {labels.points}
                      {':'}
                    </BodyCopy>
                  </Col>
                  <Col
                    tagName="li"
                    key="product-title"
                    className="itemList"
                    colSize={{ small: 4, medium: 5, large: 8 }}
                  >
                    <BodyCopy
                      tag="span"
                      fontSize="fs13"
                      textAlign="left"
                      className="itemDesc"
                      dataLocator="addedtobag-productprice"
                      color="orange.800"
                      fontWeight={['semibold']}
                      fontFamily="secondary"
                    >
                      {showItemPoints(bagLoading, data)}
                    </BodyCopy>
                  </Col>
                </Row>
              )}
            </>
          )}
        </Col>
      </Row>
    </ProductInformationStyle>
  );
};

ProductInformation.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  data: PropTypes.shape({}).isRequired,
  inheritedStyles: PropTypes.shape({}).isRequired,
  quantity: PropTypes.number.isRequired,
  isDoubleAddedToBag: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  hasDoubleItems: PropTypes.shape([]).isRequired,
  isSuppressGetOrderDetails: PropTypes.bool.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  isAvoidGetOrderDetails: PropTypes.bool.isRequired,
  isATBHidePoints: PropTypes.bool,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
};

ProductInformation.defaultProps = {
  isATBHidePoints: false,
  isATBModalBackAbTestNewDesign: false,
};

export default errorBoundary(ProductInformation);
