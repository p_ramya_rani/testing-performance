// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export default styled.div`
  .product-details {
    float: left;
    width: 37%;
    margin-bottom: 5px;
  }
  .product-image {
    text-align: center;
    width: 100px;
  }
  .brand-image {
    text-align: center;
    padding-top: 8px;
    width: 55px;
    height: 20px;
  }
  .itemList {
    width: 36px;
    font-size: 13px;
  }
  .itemDesc {
    width: 120px;
    margin-left: ${(props) => (props.isATBModalBackAbTestNewDesign ? '17px' : '0')};
    font-size: 13px;
  }
  .product-title {
    width: 203px;
  }
  .product-description {
    width: 210px;
  }
  .product-logo {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
  ${(props) =>
    props.isATBModalBackAbTestNewDesign
      ? ''
      : `
  .product-logo {
    margin-right: 14px;
    width: 80px;
  }
  .brand-image {
    width: 41px;
    height: 15px;
    padding-top: 4px;
  }
  .product-title {
    width: 100%;
  }
  .product-details {
    width: 90%;
  }
  .added-to-bag-points-portal {
    width: 100%;
  }
  .atb-product-detail-section {
    @media (max-width: 360px) {
      max-width: 60%;
    }
  }
  `}
`;
