// 9fbef606107a605d69c0edbcd8029e5d
import PropTypes from 'prop-types';
import { COMPONENTS } from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';

export const renderCheckoutServerError = (checkoutServerError) => {
  // eslint-disable-next-line react/prop-types
  return checkoutServerError && checkoutServerError.component === COMPONENTS.CHECKOUT;
};

export const getClassForIsBagPageStickyHeader = (isBagPageStickyHeader) => {
  return isBagPageStickyHeader ? 'checkout-button-bagHeader' : '';
};

export const getIdForIsBagPageStickyHeader = (isBagPageStickyHeader) => {
  return !isBagPageStickyHeader ? 'checkout-button-section' : '';
};

export const getApplePayEnabled = (props) => {
  const { isApplePayVisible, isMiniBag, showAddTobag, orderBalanceTotal } = props;
  return orderBalanceTotal > 0 && isApplePayVisible && !isMiniBag && !showAddTobag;
};

export const getPropTypes = {
  className: PropTypes.string.isRequired,
  onClickViewBag: PropTypes.func.isRequired,
  labels: PropTypes.shape.isRequired,
  showAddTobag: PropTypes.bool,
  handleCartCheckout: PropTypes.func.isRequired,
  showVenmo: PropTypes.bool,
  isBagPageStickyHeader: PropTypes.bool,
  isUSSite: PropTypes.bool,
  checkoutServerError: PropTypes.shape({}).isRequired,
  venmoError: PropTypes.string,
  paypalButtonHeight: PropTypes.number,
  isAddedToBag: PropTypes.bool,
  isBagPage: PropTypes.bool,
  isMiniBag: PropTypes.bool,
  setClickAnalyticsDataCheckout: PropTypes.func.isRequired,
  cartOrderItems: PropTypes.shape([]).isRequired,
  isPayPalHidden: PropTypes.bool.isRequired,
  isPayPalEnabled: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isPayPalEnabledKillSwitch: PropTypes.bool.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  isEditingItem: PropTypes.bool.isRequired,
  isVenmoEnabled: PropTypes.bool.isRequired,
  clearCheckoutServerError: PropTypes.func.isRequired,
  setIsPaypalBtnHidden: PropTypes.func.isRequired,
  showCondensedHeader: PropTypes.func.isRequired,
  containerId: PropTypes.string.isRequired,
  cartValue: PropTypes.number,
  isApplePayVisible: PropTypes.bool.isRequired,
  handleContinueShopping: PropTypes.func,
  enableKeepShoppingCTA: PropTypes.bool,
  orderBalanceTotal: PropTypes.number.isRequired,
  isCartLoading: PropTypes.bool.isRequired,
  isOptmisticAddToBagModal: PropTypes.bool,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
};

export const getDefaultProps = {
  showAddTobag: true,
  showVenmo: true,
  isBagPageStickyHeader: false,
  isUSSite: true,
  venmoError: '',
  paypalButtonHeight: 48,
  isAddedToBag: false,
  isBagPage: false,
  isMiniBag: false,
  cartValue: 0,
  handleContinueShopping: () => {},
  enableKeepShoppingCTA: false,
  isOptmisticAddToBagModal: false,
  isATBModalBackAbTestNewDesign: false,
};
