/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import isEqual from 'lodash/isEqual';
import PropTypes from 'prop-types';
import { Platform, ActivityIndicator } from 'react-native';
import { NavigationActions } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import ApplePaymentButton from '@tcp/core/src/components/common/atoms/ApplePaymentButton';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import CONSTANTS from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import { isIOS, isTCP } from '@tcp/core/src/utils';
import names from '@tcp/core/src/constants/eventsName.constants';
import { connect } from 'react-redux';
import ClickTracker from '../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import {
  ButtonWrapperAddedToBag,
  ButtonWrapper,
  ActionsWrapper,
  ViewBagButton,
  CheckoutButton,
  PaymentsButtonWrapper,
  ButtonViewWrapper,
  PaypalPaymentsButtonWrapper,
  ButtonsLoaderWrapper,
  KeepShoppingButtonWrapper,
  BagWithShoppingButton,
  ShoppingButton,
  VenmoPaypalWrapper,
  AfterPayWrapper,
  AfterPayLoadingWrapper,
  ShopBagButtonWrapper,
} from '../styles/AddedToBagActions.style.native';
import ADDEDTOBAG_CONSTANTS from '../../AddedToBag/AddedToBag.constants';
import CheckoutModals from '../../common/organism/CheckoutModals';
import PayPalButton from '../../common/organism/PayPalButton';
import { Button } from '../../../../common/atoms';
import CustomButton from '../../../../common/atoms/ButtonRedesign';
import {
  LoadingSpinnerWrapper,
  TouchableOpacityComponent,
} from '../../../../common/atoms/ButtonRedesign/ButtonRedesign.style.native';
import colors from '../../../../../../styles/themes/TCP/colors';
import { getCheckoutLoaderState } from '../../AddedToBag/container/AddedToBag.selectors';

const LoadingSpinner = () => {
  return (
    <LoadingSpinnerWrapper textPadding="10px 5px">
      <ActivityIndicator size="small" color={colors.BLUE} />
    </LoadingSpinnerWrapper>
  );
};
export class AddedToBagActions extends React.Component {
  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      venmoEnable: false,
      applePayEnabled: true,
    };
    this.hasNotch = DeviceInfo.hasNotch();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  getVenmoButton = () => <></>;

  setVenmoState = (val) => {
    const { isPayPalWebViewEnable, hideHeader, fromAddedToBagModal, resetTimerStatus } = this.props;
    if (fromAddedToBagModal) {
      hideHeader(!isPayPalWebViewEnable);
      if (!val && resetTimerStatus) resetTimerStatus(true);
    }
  };

  getPaypalButton = (addWrapper) => {
    const {
      getPayPalSettings,
      payPalTop,
      navigation,
      orderId,
      isPayPalEnabled,
      isPayPalWebViewEnable,
      resetTimerStatus,
      isPayPalEnabledKillSwitch,
    } = this.props;
    if ((orderId && isPayPalEnabled) || isPayPalEnabledKillSwitch) {
      if (addWrapper) {
        return (
          <PaypalPaymentsButtonWrapper isPayPalWebViewEnable={isPayPalWebViewEnable}>
            <PayPalButton
              getPayPalSettings={getPayPalSettings}
              accessibilityRole="button"
              accessibilityLabel="Paypal"
              navigation={navigation}
              setVenmoState={this.setVenmoState}
              closeModal={this.closeModal}
              top={payPalTop}
              fullWidth
              resetTimerStatus={resetTimerStatus}
            />
          </PaypalPaymentsButtonWrapper>
        );
      }
      return (
        <PayPalButton
          getPayPalSettings={getPayPalSettings}
          navigation={navigation}
          setVenmoState={this.setVenmoState}
          closeModal={this.closeModal}
          top={payPalTop}
          fullWidth
          resetTimerStatus={resetTimerStatus}
        />
      );
    }
    return null;
  };

  /**
   * @description - render paypal and venmo CTAs
   */
  getVenmoPaypalPaymentButton(
    isVenmoFlag,
    showVenmoPayPalButton,
    isPayPalWebViewEnable,
    showKeepShoppingABTest
  ) {
    const { applePayAppData, showAddTobag, orderBalanceTotal } = this.props;
    const { isEligibleForApplePay } = applePayAppData;
    const canRenderApplePayButton = this.canRenderApplePay(
      isEligibleForApplePay,
      showAddTobag,
      orderBalanceTotal
    );
    return (
      isVenmoFlag &&
      showVenmoPayPalButton &&
      !showKeepShoppingABTest && (
        <VenmoPaypalWrapper isPayPalWebViewEnable={isPayPalWebViewEnable}>
          {this.getPaypalButton(true)}
          {
            <PaymentsButtonWrapper
              hasNotch={this.hasNotch}
              canRenderApplePayButton={canRenderApplePayButton}
            >
              {canRenderApplePayButton && this.getApplePayButton()}
            </PaymentsButtonWrapper>
          }
        </VenmoPaypalWrapper>
      )
    );
  }

  canRenderApplePay = (isEligibleForApplePay, showAddTobag, orderBalanceTotal) => {
    return isIOS() && isEligibleForApplePay && !showAddTobag && orderBalanceTotal > 0;
  };

  closeModal = (close) => {
    const { closeModal } = this.props;
    if (close) {
      closeModal();
    }
  };

  /**
   * @description - condition to show venmo and paypal CTA on next line
   */
  showVenmoPaypalButton = () => {
    const { venmoEnable } = this.state;
    const {
      isNoNEmptyBag,
      fromAddedToBagModal,
      isPayPalEnabled,
      isVenmoEnabled,
      isVenmoAppInstalled,
      applePayAppData: { isEligibleForApplePay },
    } = this.props;
    return (
      isNoNEmptyBag &&
      !fromAddedToBagModal &&
      isPayPalEnabled &&
      (isEligibleForApplePay || (venmoEnable && isVenmoEnabled && isVenmoAppInstalled))
    );
  };

  isBottomGap = (isVenmoFlag, showVenmoPayPalButton, showAddTobag) => {
    return !(isVenmoFlag && showVenmoPayPalButton) && !showAddTobag;
  };

  isAnyOneEnabled = (isVenmoFlag, isPayPalEnabled, fromAddedToBagModal) => {
    if (fromAddedToBagModal) {
      return isPayPalEnabled;
    }
    return isVenmoFlag || isPayPalEnabled;
  };

  isCheckoutButtonHalf = (
    isVenmoFlag,
    isPayPalEnabled,
    showVenmoPayPalButton,
    fromAddedToBagModal
  ) => {
    return (
      this.isAnyOneEnabled(isVenmoFlag, isPayPalEnabled, fromAddedToBagModal) &&
      !showVenmoPayPalButton
    );
  };

  getPageData = () => {
    const { navigation } = this.props;
    const { state } = navigation;
    const { SHOPPING_BAG, BROWSE } = CONSTANTS;
    const page = state.routeName === 'BagPage' ? SHOPPING_BAG : BROWSE;
    return {
      pageName: page,
      pageSection: page,
      pageSubSection: page,
      pageType: page,
      pageShortName: page,
      pageSubSubSection: page,
    };
  };

  getTopMargin = (isVenmoFlag, fromAddedToBagModal) => {
    if (isVenmoFlag) return '8px';
    if (fromAddedToBagModal) return '20px';
    return Platform.OS === 'ios' ? '0px' : '8px';
  };

  getApplePayButton = () => {
    const { navigation } = this.props;
    return <ApplePaymentButton navigation={navigation} />;
  };

  shouldShowVenmoButton = (applePayEnabled, showAddTobag, isVenmoAppInstalled) =>
    !applePayEnabled && !showAddTobag && isVenmoAppInstalled;

  checkforVenmoOrApplePayEnabled = () => {
    const { venmoEnable } = this.state;
    const {
      isVenmoEnabled,
      isVenmoAppInstalled,
      applePayAppData: { isEligibleForApplePay },
    } = this.props;
    return isEligibleForApplePay || (isVenmoEnabled && venmoEnable && isVenmoAppInstalled);
  };

  getRowOneButtons = () => {
    const {
      labels,
      showAddTobag,
      handleCartCheckout,
      isEditingItem,
      navigation,
      closeModal,
      isNoNEmptyBag,
      fromAddedToBagModal,
      isPayPalEnabled,
      resetTimerStatus,
      isPayPalWebViewEnable,
      cartOrderItems,
      applePayAppData,
      orderBalanceTotal,
      checkoutLoaderState,
    } = this.props;
    const { isEligibleForApplePay } = applePayAppData;
    const isVenmoOrApplePayFlag = this.checkforVenmoOrApplePayEnabled();
    const showVenmoPayPalButton = this.showVenmoPaypalButton();
    const pageData = this.getPageData();
    const productsData = BagPageUtils.formatBagProductsData(cartOrderItems);
    const loaderButtonStyle = { backgroundColor: colors.PRIMARY.DARKBLUE, width: '100%' };
    const borderRadiusValue = isTCP() ? '0' : '19';
    if (isNoNEmptyBag || fromAddedToBagModal) {
      return (
        <ButtonViewWrapper
          isBottomGap={this.isBottomGap(isVenmoOrApplePayFlag, showVenmoPayPalButton, showAddTobag)}
          isPayPalWebViewEnable={isPayPalWebViewEnable}
        >
          <ButtonWrapper
            isBothDisabled={this.isAnyOneEnabled(isVenmoOrApplePayFlag, isPayPalEnabled)}
            isPayPalWebViewEnable={isPayPalWebViewEnable}
            fromAddedToBagModal={fromAddedToBagModal}
            marginTop={this.getTopMargin(isVenmoOrApplePayFlag, fromAddedToBagModal)}
          >
            {!showVenmoPayPalButton && (
              <PaypalPaymentsButtonWrapper
                isAddedTobag={showAddTobag}
                isPayPalEnabled={isPayPalEnabled}
                isPayPalWebViewEnable={isPayPalWebViewEnable}
              >
                {this.getPaypalButton(false)}
                {this.canRenderApplePay(isEligibleForApplePay, showAddTobag, orderBalanceTotal) &&
                  this.getApplePayButton()}
              </PaypalPaymentsButtonWrapper>
            )}
            {!isPayPalWebViewEnable && (
              <CheckoutButton
                isHalf={this.isCheckoutButtonHalf(
                  isVenmoOrApplePayFlag,
                  isPayPalEnabled,
                  showVenmoPayPalButton,
                  fromAddedToBagModal
                )}
                isBothDisabled={this.isAnyOneEnabled(isVenmoOrApplePayFlag, isPayPalEnabled)}
                isAddedTobag={showAddTobag}
              >
                {!checkoutLoaderState ? (
                  <ClickTracker
                    name="checkout_button"
                    module="checkout"
                    clickData={{ customEvents: ['event8'], products: productsData }}
                    pageData={pageData}
                    as={Button}
                    color="white"
                    fill="BLUE"
                    width="100%"
                    fontWeight="extrabold"
                    fontFamily="secondary"
                    fontSize="fs13"
                    text={labels.checkout && labels.checkout.toUpperCase()}
                    isAddedTobag={showAddTobag}
                    onPress={() => {
                      if (resetTimerStatus) resetTimerStatus(true);
                      handleCartCheckout({
                        isEditingItem,
                        navigation,
                        closeModal,
                        navigationActions: NavigationActions,
                        isVenmoProgress: false,
                      });
                    }}
                  />
                ) : (
                  <TouchableOpacityComponent
                    accessibilityRole="button"
                    style={loaderButtonStyle}
                    borderRadius={borderRadiusValue}
                  >
                    <LoadingSpinner />
                  </TouchableOpacityComponent>
                )}
              </CheckoutButton>
            )}
          </ButtonWrapper>
        </ButtonViewWrapper>
      );
    }
    return null;
  };

  isSBP = () => {
    const { navigation } = this.props;
    if (!navigation.state || !navigation.state.params) return false;
    return Object.keys(navigation.state.params).includes('isSBP') && navigation.state.params.isSBP;
  };

  viewBagAction = (comingFromNewATBModal) => {
    const { navigation, closeModal, resetTimerStatus, addedFromSBP } = this.props;
    if (resetTimerStatus) resetTimerStatus(true);
    if (closeModal) {
      closeModal();
    }

    /*
      SBP-869:move the Bag to top of the stack when navigating to bag from PDP
    */
    if (addedFromSBP || this.isSBP()) {
      navigation.popToTop();
    }
    let isComingFromNewATBModal = false;
    if (comingFromNewATBModal) isComingFromNewATBModal = true;
    navigation.navigate(ADDEDTOBAG_CONSTANTS.BAG_PAGE, {
      comingFromNewATBModal: isComingFromNewATBModal,
    });
  };

  onClosePress = () => {
    const { closeModal } = this.props;
    if (closeModal) {
      closeModal();
    }
  };

  renderNewRedesignedModal = () => {
    const { labels, isNewRedesignedModal, addToBagModalError } = this.props;
    if (addToBagModalError) {
      return (
        <KeepShoppingButtonWrapper isNewRedesignedModal={isNewRedesignedModal}>
          <ShopBagButtonWrapper>
            <CustomButton
              fontSize="fs16"
              fontWeight="regular"
              fontFamily="secondary"
              textPadding="0px"
              margin="0px 10px 0px 0px"
              borderRadius="16px"
              showShadow
              buttonVariation="variable-width"
              height="46px"
              text={labels.keepShoppingRedesign}
              onPress={this.onClosePress}
              fill="BLUE"
            />
          </ShopBagButtonWrapper>
        </KeepShoppingButtonWrapper>
      );
    }
    return (
      <KeepShoppingButtonWrapper isNewRedesignedModal={isNewRedesignedModal}>
        <ShopBagButtonWrapper>
          <ClickTracker
            name={names.screenNames.atb_drawer_keep_shopping}
            module="browse"
            clickData={{ customEvents: ['event144'] }}
            as={CustomButton}
            fontSize="fs16"
            fontWeight="regular"
            fontFamily="secondary"
            textPadding="0px"
            margin="0px 10px 0px 0px"
            borderRadius="16px"
            showShadow
            buttonVariation="variable-width"
            height="46px"
            text={labels.keepShoppingRedesign}
            onPress={this.onClosePress}
            fill="WHITE"
          />
        </ShopBagButtonWrapper>
        <ShopBagButtonWrapper>
          <CustomButton
            fontSize="fs16"
            fontWeight="regular"
            fontFamily="secondary"
            textPadding="0px 5px"
            margin="0px 0px 0px 10px"
            borderRadius="16px"
            showShadow
            buttonVariation="variable-width"
            height="46px"
            fill="BLUE"
            text={labels.viewBag}
            onPress={() => this.viewBagAction(true)}
          />
        </ShopBagButtonWrapper>
      </KeepShoppingButtonWrapper>
    );
  };

  showKeepShoppingAndBag = (showKeepShoppingABTest) => {
    const { labels, isNewRedesignedModal } = this.props;
    if (!showKeepShoppingABTest) {
      return null;
    }
    return isNewRedesignedModal ? (
      this.renderNewRedesignedModal()
    ) : (
      <KeepShoppingButtonWrapper>
        <ShoppingButton
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize="fs13"
          text={labels.keepShopping && labels.keepShopping.toUpperCase()}
          as={Button}
          onPress={this.onClosePress}
        />
        <BagWithShoppingButton
          color="white"
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize="fs13"
          text={labels.viewBag && labels.viewBag.toUpperCase()}
          as={Button}
          onPress={this.viewBagAction}
        />
      </KeepShoppingButtonWrapper>
    );
  };

  render() {
    const {
      labels,
      showAddTobag,
      isPayPalWebViewEnable,
      navigation,
      fromAddedToBagModal,
      bagLoading,
      orderBalanceTotal,
      isNewRedesignedModal,
      isCartLoading,
    } = this.props;
    const isVenmoOrApplePayFlag = this.checkforVenmoOrApplePayEnabled();
    const showVenmoPayPalButton = this.showVenmoPaypalButton();
    const showKeepShoppingABTest = fromAddedToBagModal;
    const showViewBagCTA = showAddTobag && !isPayPalWebViewEnable && !showKeepShoppingABTest;
    const showStickyButtons = isNewRedesignedModal || !bagLoading;
    return (
      <>
        <ActionsWrapper
          isPayPalWebViewEnable={isPayPalWebViewEnable}
          showElement={showStickyButtons}
          isCartLoading={isCartLoading}
        >
          {this.showKeepShoppingAndBag(showKeepShoppingABTest)}
          {showViewBagCTA && (
            <ButtonWrapperAddedToBag
              isPayPalWebViewEnable={!isVenmoOrApplePayFlag && !showAddTobag}
            >
              <ViewBagButton
                textTransform="uppercase"
                color="white"
                fontWeight="extrabold"
                fontFamily="secondary"
                fontSize="fs13"
                text={labels.viewBag && labels.viewBag.toUpperCase()}
                as={Button}
                onPress={this.viewBagAction}
              />
            </ButtonWrapperAddedToBag>
          )}
          {!showKeepShoppingABTest && this.getRowOneButtons()}
          {this.getVenmoPaypalPaymentButton(
            isVenmoOrApplePayFlag,
            showVenmoPayPalButton,
            isPayPalWebViewEnable,
            showKeepShoppingABTest
          )}
          {!showAddTobag && !bagLoading && (
            <AfterPayWrapper>
              <AfterPayMessaging
                isOrderThresholdEnabled
                offerPrice={orderBalanceTotal}
                isPriceBold
              />
            </AfterPayWrapper>
          )}
          <CheckoutModals navigation={navigation} />
        </ActionsWrapper>
        {!showStickyButtons || isCartLoading ? (
          <ButtonsLoaderWrapper>
            <LoaderSkelton height={34} />
            <AfterPayLoadingWrapper>
              <LoaderSkelton height={12} />
            </AfterPayLoadingWrapper>
          </ButtonsLoaderWrapper>
        ) : null}
      </>
    );
  }
}

AddedToBagActions.propTypes = {
  labels: PropTypes.shape.isRequired,
  showAddTobag: PropTypes.shape,
  navigation: PropTypes.shape({}).isRequired,
  closeModal: PropTypes.func,
  isNoNEmptyBag: PropTypes.number.isRequired,
  fromAddedToBagModal: PropTypes.bool,
  payPalTop: PropTypes.number,
  hideHeader: PropTypes.func,
  cartOrderItems: PropTypes.shape([]).isRequired,
  isPayPalEnabled: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isPayPalEnabledKillSwitch: PropTypes.bool.isRequired,
  isEditingItem: PropTypes.bool.isRequired,
  isVenmoEnabled: PropTypes.bool.isRequired,
  resetTimerStatus: PropTypes.func.isRequired,
  isPayPalWebViewEnable: PropTypes.bool.isRequired,
  handleCartCheckout: PropTypes.func.isRequired,
  getPayPalSettings: PropTypes.func.isRequired,
  orderId: PropTypes.string.isRequired,
  isVenmoAppInstalled: PropTypes.bool.isRequired,
  applePayAppData: PropTypes.shape({}),
  orderBalanceTotal: PropTypes.number.isRequired,
  addedFromSBP: PropTypes.bool,
  isNewRedesignedModal: PropTypes.bool,
  addToBagModalError: PropTypes.string,
  isCartLoading: PropTypes.bool.isRequired,
  checkoutLoaderState: PropTypes.bool,
};

AddedToBagActions.defaultProps = {
  showAddTobag: true,
  closeModal: () => {},
  fromAddedToBagModal: false,
  payPalTop: 0,
  hideHeader: () => {},
  applePayAppData: {},
  addedFromSBP: false,
  isNewRedesignedModal: false,
  addToBagModalError: null,
  checkoutLoaderState: false,
};
const mapStateToProps = (state) => {
  return {
    checkoutLoaderState: getCheckoutLoaderState(state),
  };
};
export default connect(mapStateToProps)(AddedToBagActions);
