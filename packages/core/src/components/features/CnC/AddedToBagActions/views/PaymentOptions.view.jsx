import React from 'react';
import { getIconPath, isCanada } from '../../../../../utils';
import BodyCopy from '../../../../common/atoms/BodyCopy';

const PaymentOptionsComp = (props) => {
  const { handlePayment, labels, isApplePayEnabled } = props;
  const isCanadaCheck = isCanada();
  return (
    <>
      <div className="paymentHidden">
        <BodyCopy
          onClick={handlePayment}
          className="paymentLink"
          component="span"
          fontSize="fs14"
          fontWeight="semibold"
          fontFamily="secondary"
        >
          {labels.otherPayOptions}
        </BodyCopy>

        <BodyCopy onClick={handlePayment} className="payPalImage" component="span">
          <BodyCopy className="payPalIcon" component="span">
            <img src={getIconPath('paypal-logo')} alt="paypal-icon" />
          </BodyCopy>
        </BodyCopy>
        {!isCanadaCheck && (
          <>
            <BodyCopy onClick={handlePayment} className="otherPaymentsImage" component="span">
              <img src={getIconPath('Afterpay_Badge_BlackonMint')} alt="after-pay" />
            </BodyCopy>

            {isApplePayEnabled ? (
              <BodyCopy onClick={handlePayment} className="otherPaymentsImage" component="span">
                <img
                  src={getIconPath('apple-pay-logo')}
                  alt="apple-pay"
                  className="apple-pay-image"
                />
              </BodyCopy>
            ) : (
              <BodyCopy onClick={handlePayment} className="otherPaymentsImage" component="span">
                <img
                  src={getIconPath('venmo-logo')}
                  alt="venmo-logo-blue"
                  className="venmo-overlay-image"
                />
              </BodyCopy>
            )}
          </>
        )}
      </div>
    </>
  );
};

export default PaymentOptionsComp;
