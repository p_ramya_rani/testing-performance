/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import VenmoPaymentButton from '@tcp/core/src/components/common/atoms/VenmoPaymentButton';
import ApplePaymentButton from '@tcp/core/src/components/common/atoms/ApplePaymentButton';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { CALL_TO_ACTION_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import { setLoyaltyLocation } from '@tcp/core/src/constants/analytics';
import Button from '../../../../common/atoms/Button';
import withStyles from '../../../../common/hoc/withStyles';
import style from '../styles/AddedToBagActions.style';
import PayPalButton from '../../common/organism/PayPalButton';
import Row from '../../../../common/atoms/Row';
import Col from '../../../../common/atoms/Col';
import BodyCopy from '../../../../common/atoms/BodyCopy';
import { getLocator } from '../../../../../utils';
import ErrorMessage from '../../common/molecules/ErrorMessage';
import PaymentOptionsComp from './PaymentOptions.view';
import {
  renderCheckoutServerError,
  getClassForIsBagPageStickyHeader,
  getIdForIsBagPageStickyHeader,
  getApplePayEnabled,
  getPropTypes,
  getDefaultProps,
} from './AddedToBagActions.utils';

class AddedToBagActions extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      paymentDisabled: true,
    };
    this.onCheckoutScroll = this.onCheckoutScroll.bind(this);
    this.handlePayment = this.handlePayment.bind(this);
    this.myRef = React.createRef(null);
  }

  componentDidMount() {
    const {
      isPayPalHidden,
      showAddTobag,
      checkoutServerError,
      clearCheckoutServerError,
      isStickyCheckoutButtonEnabled,
      isMobile,
    } = this.props;
    if (isPayPalHidden && showAddTobag && checkoutServerError) {
      clearCheckoutServerError({});
    }
    if (isStickyCheckoutButtonEnabled && isMobile) {
      window.addEventListener('scroll', this.onCheckoutScroll);
    }
  }

  componentWillUnmount() {
    const { isStickyCheckoutButtonEnabled, isMobile } = this.props;
    if (isStickyCheckoutButtonEnabled && isMobile) {
      window.removeEventListener('scroll', this.onCheckoutScroll);
    }
  }

  onCheckoutScroll = () => {
    const { handleCheckoutHidden } = this.props;

    const val = parseInt(this.myRef.current.getBoundingClientRect().top, 10);
    if (-val > 0) {
      handleCheckoutHidden(true);
    } else {
      handleCheckoutHidden(false);
    }
  };

  getPaypalButton() {
    const {
      showAddTobag,
      containerId,
      isBagPageStickyHeader,
      isPayPalHidden,
      isPayPalEnabled,
      paypalButtonHeight,
      setIsPaypalBtnHidden,
      bagLoading,
      showCondensedHeader,
      isPayPalEnabledKillSwitch,
    } = this.props;
    let containerID = containerId;
    if (isBagPageStickyHeader) {
      containerID = 'paypal-button-container-bag-header';
    }
    if (showAddTobag && isPayPalHidden) {
      setIsPaypalBtnHidden(false);
    }
    return (
      ((!isPayPalHidden && !bagLoading && isPayPalEnabled) || isPayPalEnabledKillSwitch) && (
        <div className={`${showAddTobag ? 'paypal-wrapper-atb' : 'paypal-wrapper'}`}>
          <PayPalButton
            className="payPal-button"
            containerId={containerID}
            height={paypalButtonHeight}
            showCondensedHeader={showCondensedHeader}
          />
        </div>
      )
    );
  }

  getHeaderPaypalButton() {
    const { isBagPageStickyHeader, isInternationalShipping } = this.props;
    if (!isInternationalShipping && isBagPageStickyHeader) {
      return this.getPaypalButton();
    }
    return null;
  }

  getCheckoutButton(value) {
    const {
      labels,
      handleCartCheckout,
      isEditingItem,
      setClickAnalyticsDataCheckout,
      cartOrderItems,
      isAddedToBag,
      isBagPage,
      isMiniBag,
      bagLoading,
      cartValue,
      isOptmisticAddToBagModal,
      isCartLoading,
      isStickyCheckoutButtonEnabled,
      isMobile,
      isCheckoutBannerHidden,
    } = this.props;
    const productsData = BagPageUtils.formatBagProductsData(cartOrderItems);
    return (
      <div ref={value ? this.myRef : ''} className="checkoutBtn-wrapper">
        {!bagLoading || isOptmisticAddToBagModal ? (
          <>
            <ClickTracker
              name="Gift_Services"
              onClick={() => {
                setLoyaltyLocation('checkout-popup');
              }}
              className="checkoutBtnTracker"
            >
              <Button
                data-locator={getLocator('addedtobag_btncheckout')}
                className="checkout"
                disabled={isCartLoading}
                onClick={() => {
                  setClickAnalyticsDataCheckout({
                    products: productsData,
                    eventName: 'checkout login click',
                    pageNavigationText: '',
                    listingSortList: '',
                    listingCount: '',
                    cartAmount: cartValue,
                  });
                  handleCartCheckout({ isEditingItem, isAddedToBag, isBagPage, isMiniBag });
                }}
              >
                <BodyCopy
                  component="span"
                  color="white"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  fontSize="fs14"
                >
                  {labels.checkout}
                </BodyCopy>
              </Button>
            </ClickTracker>
            {isStickyCheckoutButtonEnabled && !isCheckoutBannerHidden && isMobile && (
              <BodyCopy
                component="div"
                color="black"
                fontWeight="extrabold"
                fontFamily="secondary"
                fontSize="fs14"
                className="checkout-divider"
              >
                {labels.lbl_or_added_to_bag}
              </BodyCopy>
            )}
          </>
        ) : (
          <div className="checkoutSkeleton">
            <LoaderSkelton />
          </div>
        )}
      </div>
    );
  }

  getApplePaymentButton() {
    const { isBagPageStickyHeader, bagLoading } = this.props;
    const containerID = isBagPageStickyHeader ? 'apple-button-container-bag-header' : '';
    return (
      getApplePayEnabled(this.props) && (
        <>
          {bagLoading ? (
            <div className="apple-wrapper">
              <LoaderSkelton />
            </div>
          ) : (
            <div className="apple-wrapper">
              <ApplePaymentButton className="apple-container" isBagPage containerId={containerID} />
            </div>
          )}
        </>
      )
    );
  }

  getVenmoEnabled() {
    const { isVenmoEnabled, isInternationalShipping, isUSSite, showVenmo } = this.props;
    return (
      isVenmoEnabled &&
      showVenmo &&
      !isInternationalShipping &&
      isUSSite &&
      !getApplePayEnabled(this.props)
    );
  }

  getVenmoPaymentButton() {
    const { handleCartCheckout, isEditingItem, bagLoading } = this.props;
    if (bagLoading) {
      return (
        <div className="venmo-wrapper">
          <LoaderSkelton />
        </div>
      );
    }

    if (this.getVenmoEnabled()) {
      return (
        <div className="venmo-wrapper">
          <VenmoPaymentButton
            className="venmo-container"
            onSuccess={() => handleCartCheckout(isEditingItem)}
            isBagPage
          />
        </div>
      );
    }
    return null;
  }

  getViewBagButton() {
    const {
      onClickViewBag,
      labels,
      showAddTobag,
      enableKeepShoppingCTA,
      handleContinueShopping,
      isATBModalBackAbTestNewDesign,
    } = this.props;

    if (!isATBModalBackAbTestNewDesign) {
      return (
        <>
          {showAddTobag && (
            <div className="view-bag-container redesigned-bag-container">
              <Button
                onClick={onClickViewBag}
                data-locator={getLocator('addedtobag_btnviewbag')}
                className="redesigned-view-bag"
              >
                <BodyCopy
                  component="span"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  fontSize="fs14"
                >
                  {labels.viewBag}
                </BodyCopy>
              </Button>
              <Button
                onClick={handleContinueShopping}
                buttonVariation="fixed-width"
                className="redesigned-keep-shopping"
              >
                <BodyCopy
                  component="span"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  fontSize="fs14"
                  color="white"
                >
                  {labels.keepShopping}
                </BodyCopy>
              </Button>
            </div>
          )}
        </>
      );
    }

    if (enableKeepShoppingCTA) {
      return (
        <>
          {showAddTobag && (
            <div className="view-bag-container">
              <Button
                onClick={handleContinueShopping}
                buttonVariation="fixed-width"
                className="keep-shopping"
              >
                <BodyCopy
                  component="span"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  fontSize="fs14"
                >
                  {labels.keepShopping}
                </BodyCopy>
              </Button>
              <Button
                onClick={onClickViewBag}
                data-locator={getLocator('addedtobag_btnviewbag')}
                className="view-bag"
              >
                <BodyCopy
                  component="span"
                  color="white"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  fontSize="fs14"
                >
                  {labels.viewBag}
                </BodyCopy>
              </Button>
            </div>
          )}
        </>
      );
    }
    return (
      <>
        {showAddTobag && (
          <Row className="view-bag-container">
            <Col colSize={{ medium: 8, large: 12, small: 6 }}>
              <Button
                onClick={onClickViewBag}
                data-locator={getLocator('addedtobag_btnviewbag')}
                className="view-bag"
              >
                <BodyCopy
                  component="span"
                  color="white"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                  fontSize="fs14"
                >
                  {labels.viewBag}
                </BodyCopy>
              </Button>
            </Col>
          </Row>
        )}
      </>
    );
  }

  renderPaymentButtons = () => {
    return (
      <>
        {this.getHeaderPaypalButton()}
        {this.getVenmoPaymentButton()}
        {this.getApplePaymentButton()}
      </>
    );
  };

  getPaymemtButtonsForStickyHeader = () => {
    const {
      isCheckoutBannerHidden,
      className,
      enableKeepShoppingCTA,
      isATBModalBackAbTestNewDesign,
      isBagPageStickyHeader,
      isCartLoading,
    } = this.props;
    return (
      <>
        {isCheckoutBannerHidden && (
          <div className={className}>
            {this.getViewBagButton()}
            {!enableKeepShoppingCTA && isATBModalBackAbTestNewDesign && isCheckoutBannerHidden && (
              <>
                <Row
                  className={`checkout-button ${
                    isCheckoutBannerHidden ? 'sticky-checkout-button-new' : ''
                  } ${isBagPageStickyHeader ? 'checkout-button-bagHeader' : ''}`}
                  id={`${!isBagPageStickyHeader ? 'checkout-button-section' : ''}`}
                >
                  {this.renderAfterPayMessaging()}
                  <div
                    className={`paypal-venmo ${
                      isBagPageStickyHeader ? 'checkout-sticky-header' : ''
                    }`}
                  >
                    {this.getPaypalButton()}
                    {this.renderPaymentButtons()}
                  </div>
                  {this.getCheckoutButton(false)}
                  {isCartLoading ? <div className="invisible-div" /> : null}
                  <RenderPerf.Measure name={CALL_TO_ACTION_VISIBLE} />
                </Row>
              </>
            )}
          </div>
        )}
      </>
    );
  };

  getPaypalButtonForStickyHeader = () => {
    const { isBagPageStickyHeader, isInternationalShipping } = this.props;
    return !isInternationalShipping && !isBagPageStickyHeader && this.getPaypalButton();
  };

  handlePayment() {
    this.setState({ paymentDisabled: false });
  }

  renderAfterPayMessaging() {
    const { isAddedToBag, orderBalanceTotal } = this.props;
    return (
      <>
        {!isAddedToBag && (
          <div className="afterpay-msg-section">
            <AfterPayMessaging
              isOrderThresholdEnabled
              offerPrice={orderBalanceTotal}
              isPriceBold
              disableForGC
              centered
            />
          </div>
        )}
      </>
    );
  }

  render() {
    const {
      className,
      isBagPageStickyHeader,
      checkoutServerError,
      venmoError,
      enableKeepShoppingCTA,
      isATBModalBackAbTestNewDesign,
      isCartLoading,
      isCheckoutBannerHidden,
      isStickyCheckoutButtonEnabled,
      isStickyCheckoutPaymentDisabled,
      isMobile,
      labels,
    } = this.props;
    const { paymentDisabled } = this.state;
    const isApplePayEnabled = getApplePayEnabled(this.props);

    return (
      <div className={className}>
        {this.getViewBagButton()}
        {!enableKeepShoppingCTA && isATBModalBackAbTestNewDesign && (
          <>
            <Row
              className={`checkout-button ${getClassForIsBagPageStickyHeader(
                isBagPageStickyHeader
              )}`}
              id={`${getIdForIsBagPageStickyHeader(isBagPageStickyHeader)}`}
            >
              {(isStickyCheckoutPaymentDisabled && paymentDisabled && isMobile) ||
              (isStickyCheckoutButtonEnabled && isCheckoutBannerHidden && isMobile) ? (
                <></>
              ) : (
                <div
                  className={`paypal-venmo ${
                    isBagPageStickyHeader ? 'checkout-sticky-header' : ''
                  }`}
                >
                  {this.getPaypalButtonForStickyHeader()}
                  {this.renderPaymentButtons()}
                </div>
              )}
              {this.getCheckoutButton(true)}
              {isCartLoading ? <div className="invisible-div" /> : null}
              <RenderPerf.Measure name={CALL_TO_ACTION_VISIBLE} />
            </Row>
            {((!isStickyCheckoutButtonEnabled && !isStickyCheckoutPaymentDisabled) ||
              !paymentDisabled) &&
              this.renderAfterPayMessaging()}
            {paymentDisabled && isStickyCheckoutPaymentDisabled && (
              <PaymentOptionsComp
                labels={labels}
                handlePayment={this.handlePayment}
                isApplePayEnabled={isApplePayEnabled}
              />
            )}
          </>
        )}
        {this.getPaymemtButtonsForStickyHeader()}
        {(renderCheckoutServerError(checkoutServerError) || venmoError) && (
          <Row className="elem-mt-MED">
            <ErrorMessage
              // eslint-disable-next-line react/prop-types
              error={venmoError || checkoutServerError.errorMessage}
              className="addBagActions-error"
            />
          </Row>
        )}
      </div>
    );
  }
}

AddedToBagActions.propTypes = getPropTypes;
AddedToBagActions.defaultProps = getDefaultProps;

export default withStyles(AddedToBagActions, style);
export { AddedToBagActions as AddedToBagActionsVanilla };
