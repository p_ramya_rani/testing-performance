// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEqual from 'lodash/isEqual';
import {
  setSessionStorage,
  getSessionStorage,
  isBagRoute,
  isMobileApp,
  isClient,
  getViewportInfo,
} from '@tcp/core/src/utils';
import { setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import AddedToBagActionsView from '../views/AddedToBagActions.view';
import {
  getLabelsAddToActions,
  getGrandTotal,
} from '../../AddedToBag/container/AddedToBag.selectors';
import { CHECKOUT_ROUTES } from '../../Checkout/Checkout.constants';
import utility from '../../Checkout/util/utility';
import bagPageActions from '../../BagPage/container/BagPage.actions';
import {
  getIsInternationalShipping,
  getIsPayPalEnabled,
  getOptimisticAddBagModal,
  getIsStickyPaymentAbTestEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import { getAppliedExpiredCouponListState } from '../../common/organism/CouponAndPromos/container/Coupon.selectors';
import checkoutSelectors, { isUsSite } from '../../Checkout/container/Checkout.selector';
import CHECKOUT_ACTIONS, {
  setVenmoPaymentInProgress,
  setApplePaymentInProgress,
} from '../../Checkout/container/Checkout.action';
import BagPageSelectors from '../../BagPage/container/BagPage.selectors';
import {
  getCartOrderId,
  getPageName,
  getCartLoadingState,
} from '../../CartItemTile/container/CartItemTile.selectors';
import { toastMessageInfo } from '../../../../common/atoms/Toast/container/Toast.actions';

export class AddedToBagContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isCheckoutBannerHidden: false,
    };
    this.handleCheckoutHidden = this.handleCheckoutHidden.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !(
      isEqual(nextState, this.state) &&
      isEqual(JSON.stringify(nextProps), JSON.stringify(this.props))
    );
  }

  handleCheckoutHidden = (value) => {
    if (value) {
      this.setState({ isCheckoutBannerHidden: true });
    } else {
      this.setState({ isCheckoutBannerHidden: false });
    }
  };

  onClickViewBag = () => {
    if (isBagRoute()) {
      const { onRequestClose } = this.props;
      onRequestClose();
    }
    utility.routeToPage(CHECKOUT_ROUTES.bagPage);
  };

  /**
   * @description - onCartCheckout method will check for selected checkout method
   * @param {object} payload - checkout payload for app and web
   */
  onCartCheckout = (payload) => {
    const { handleCartCheckout, setVenmoInProgress, toastMessage } = this.props;
    if (payload && !payload.isVenmoProgress) {
      setVenmoInProgress(false);
    }
    if (payload && payload.venmoErrorMessage) {
      toastMessage(payload.venmoErrorMessage);
    } else {
      handleCartCheckout(payload);
    }
  };

  isMobileDimension = () => {
    if (isMobileApp()) {
      return true;
    }
    if (window) {
      const { innerWidth: width } = window;
      if (width < 450) {
        return true;
      }
      return false;
    }
    return false;
  };

  render() {
    const {
      labels,
      showAddTobag,
      isEditingItem,
      isInternationalShipping,
      isVenmoEnabled,
      isApplePayVisible,
      isECOM,
      navigation,
      showVenmo,
      isNoNEmptyBag,
      fromAddedToBagModal,
      inheritedStyles,
      isBagPageStickyHeader,
      closeModal,
      isUSSite,
      getPayPalSettings,
      containerId,
      hideHeader,
      checkoutServerError,
      isPayPalWebViewEnable,
      venmoError,
      orderId,
      isPayPalHidden,
      payPalTop,
      paypalButtonHeight,
      isAddedToBag,
      isBagPage,
      isMiniBag,
      setClickAnalyticsDataCheckout,
      cartOrderItems,
      clearCheckoutServerError,
      isPayPalEnabled,
      setIsPaypalBtnHidden,
      bagLoading,
      resetTimerStatus,
      isVenmoAppInstalled,
      showCondensedHeader,
      isPayPalEnabledKillSwitch,
      appliedExpiredCoupon,
      pageName,
      applePayAppData,
      cartValue,
      enableKeepShoppingCTA,
      handleContinueShopping,
      orderBalanceTotal,
      addedFromSBP,
      isCartLoading,
      isNewRedesignedModal,
      addToBagModalError,
      newBag,
      isOptmisticAddToBagModal,
      newMiniBag,
      isATBModalBackAbTestNewDesign,
      isScrolling,
      isStickyCheckoutButtonEnabled,
      isStickyCheckoutPaymentDisabled,
    } = this.props;
    const isMobile = isClient() ? getViewportInfo().isMobile : null;
    return (
      <AddedToBagActionsView
        labels={labels}
        onClickViewBag={this.onClickViewBag}
        showAddTobag={showAddTobag}
        handleCartCheckout={this.onCartCheckout}
        isEditingItem={isEditingItem}
        isInternationalShipping={isInternationalShipping}
        isVenmoEnabled={isVenmoEnabled}
        isApplePayVisible={isApplePayVisible}
        isECOM={isECOM}
        navigation={navigation}
        showVenmo={showVenmo}
        isNoNEmptyBag={isNoNEmptyBag}
        fromAddedToBagModal={fromAddedToBagModal}
        isBagPageStickyHeader={isBagPageStickyHeader}
        closeModal={closeModal}
        isUSSite={isUSSite}
        inheritedStyles={inheritedStyles}
        getPayPalSettings={getPayPalSettings}
        containerId={containerId}
        hideHeader={hideHeader}
        checkoutServerError={checkoutServerError}
        isPayPalWebViewEnable={isPayPalWebViewEnable}
        venmoError={venmoError}
        orderId={orderId}
        isPayPalHidden={isPayPalHidden}
        payPalTop={payPalTop}
        paypalButtonHeight={paypalButtonHeight}
        isAddedToBag={isAddedToBag}
        isBagPage={isBagPage}
        isMiniBag={isMiniBag}
        setClickAnalyticsDataCheckout={setClickAnalyticsDataCheckout}
        cartOrderItems={cartOrderItems}
        clearCheckoutServerError={clearCheckoutServerError}
        setIsPaypalBtnHidden={setIsPaypalBtnHidden}
        bagLoading={bagLoading}
        isPayPalEnabled={isPayPalEnabled}
        resetTimerStatus={resetTimerStatus}
        isVenmoAppInstalled={isVenmoAppInstalled}
        showCondensedHeader={showCondensedHeader}
        isPayPalEnabledKillSwitch={isPayPalEnabledKillSwitch}
        appliedExpiredCoupon={appliedExpiredCoupon}
        pageName={pageName}
        applePayAppData={applePayAppData}
        cartValue={cartValue}
        enableKeepShoppingCTA={enableKeepShoppingCTA}
        handleContinueShopping={handleContinueShopping}
        orderBalanceTotal={orderBalanceTotal}
        addedFromSBP={addedFromSBP}
        isCartLoading={isCartLoading}
        isNewRedesignedModal={isNewRedesignedModal}
        addToBagModalError={addToBagModalError}
        newBag={newBag}
        isOptmisticAddToBagModal={isOptmisticAddToBagModal}
        newMiniBag={newMiniBag}
        isATBModalBackAbTestNewDesign={!isAddedToBag || isATBModalBackAbTestNewDesign}
        isScrolling={isScrolling}
        isStickyCheckoutButtonEnabled={isStickyCheckoutButtonEnabled}
        handleCheckoutHidden={this.handleCheckoutHidden}
        isCheckoutBannerHidden={this.state.isCheckoutBannerHidden}
        isStickyCheckoutPaymentDisabled={isStickyCheckoutPaymentDisabled}
        isMobile={isMobile}
      />
    );
  }
}

AddedToBagContainer.propTypes = {
  labels: PropTypes.shape.isRequired,
  handleCartCheckout: PropTypes.func.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  isNoNEmptyBag: PropTypes.number.isRequired,
  isBagPageStickyHeader: PropTypes.bool,
  containerId: PropTypes.string,
  setClickAnalyticsDataCheckout: PropTypes.func.isRequired,
  cartOrderItems: PropTypes.shape([]).isRequired,
  bagLoading: PropTypes.bool.isRequired,
  showAddTobag: PropTypes.shape.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  closeModal: PropTypes.func.isRequired,
  fromAddedToBagModal: PropTypes.bool.isRequired,
  payPalTop: PropTypes.number.isRequired,
  hideHeader: PropTypes.func.isRequired,
  isPayPalHidden: PropTypes.bool.isRequired,
  isPayPalEnabled: PropTypes.bool.isRequired,
  isPayPalEnabledKillSwitch: PropTypes.bool.isRequired,
  isEditingItem: PropTypes.bool.isRequired,
  isVenmoEnabled: PropTypes.bool.isRequired,
  clearCheckoutServerError: PropTypes.func.isRequired,
  setIsPaypalBtnHidden: PropTypes.func.isRequired,
  showCondensedHeader: PropTypes.func.isRequired,
  resetTimerStatus: PropTypes.func.isRequired,
  isPayPalWebViewEnable: PropTypes.bool.isRequired,
  getPayPalSettings: PropTypes.func.isRequired,
  orderId: PropTypes.string.isRequired,
  isVenmoAppInstalled: PropTypes.bool.isRequired,
  isUSSite: PropTypes.bool.isRequired,
  checkoutServerError: PropTypes.shape({}).isRequired,
  venmoError: PropTypes.string.isRequired,
  paypalButtonHeight: PropTypes.number.isRequired,
  isAddedToBag: PropTypes.bool.isRequired,
  isBagPage: PropTypes.bool.isRequired,
  isMiniBag: PropTypes.bool.isRequired,
  showVenmo: PropTypes.func.isRequired,
  setVenmoInProgress: PropTypes.func.isRequired,
  toastMessage: PropTypes.string.isRequired,
  inheritedStyles: PropTypes.shape({}).isRequired,
  onRequestClose: PropTypes.func.isRequired,
  appliedExpiredCoupon: PropTypes.shape({}),
  pageName: PropTypes.string.isRequired,
  cartValue: PropTypes.number,
  applePayAppData: PropTypes.shape({}),
  isApplePayVisible: PropTypes.bool.isRequired,
  enableKeepShoppingCTA: PropTypes.bool,
  handleContinueShopping: PropTypes.func,
  isECOM: PropTypes.bool,
  orderBalanceTotal: PropTypes.number.isRequired,
  addedFromSBP: PropTypes.bool,
  isCartLoading: PropTypes.bool.isRequired,
  isNewRedesignedModal: PropTypes.bool,
  addToBagModalError: PropTypes.string,
  isOptmisticAddToBagModal: PropTypes.bool,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
  isScrolling: PropTypes.bool,
};

AddedToBagContainer.defaultProps = {
  appliedExpiredCoupon: [],
  isBagPageStickyHeader: false,
  containerId: null,
  cartValue: 0,
  applePayAppData: {},
  isECOM: false,
  enableKeepShoppingCTA: false,
  handleContinueShopping: () => {},
  addedFromSBP: false,
  isNewRedesignedModal: false,
  addToBagModalError: null,
  isOptmisticAddToBagModal: false,
  isATBModalBackAbTestNewDesign: false,
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleCartCheckout: (payload) => {
      dispatch(bagPageActions.startCheckout(payload));
    },
    setVenmoInProgress: (data) => {
      if (!isMobileApp()) dispatch(setVenmoPaymentInProgress(data));
    },
    setAppleInProgress: (data) => dispatch(setApplePaymentInProgress(data)),
    setClickAnalyticsDataCheckout: (payload) => {
      const analyticsObj = JSON.parse(getSessionStorage('AnalyticsObj')) || {};
      const { checkoutClicked = false } = analyticsObj;
      if (!checkoutClicked)
        setSessionStorage({
          key: 'AnalyticsObj',
          value: JSON.stringify({ ...analyticsObj, checkoutClicked: true }),
        });
      dispatch(
        setClickAnalyticsData({
          ...payload,
          ...(!checkoutClicked && { metric86: payload.cartAmount, customEvents: ['event8'] }),
        })
      );
    },
    clearCheckoutServerError: (data) => dispatch(CHECKOUT_ACTIONS.setServerErrorCheckout(data)),
    setIsPaypalBtnHidden: (payload) => {
      dispatch(bagPageActions.setIsPaypalBtnHidden(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    labels: getLabelsAddToActions(state),
    isInternationalShipping: getIsInternationalShipping(state),
    isVenmoEnabled: checkoutSelectors.getIsVenmoEnabled(state),
    getPayPalSettings: checkoutSelectors.getPayPalSettings(state),
    isUSSite: isUsSite(state),
    checkoutServerError: checkoutSelectors.getCheckoutServerError(state),
    isPayPalWebViewEnable: BagPageSelectors.getPayPalWebViewStatus(state),
    orderId: getCartOrderId(state),
    venmoError: checkoutSelectors.getVenmoError(state),
    isPayPalHidden: BagPageSelectors.getIsPayPalHidden(state),
    cartOrderItems: BagPageSelectors.getOrderItems(state),
    bagLoading: BagPageSelectors.isBagLoading(state),
    isPayPalEnabled: BagPageSelectors.getIsPayPalEnabled(state),
    isVenmoAppInstalled: checkoutSelectors.isVenmoAppInstalled(state),
    isPayPalEnabledKillSwitch: getIsPayPalEnabled(state),
    appliedExpiredCoupon: getAppliedExpiredCouponListState(state),
    pageName: getPageName(state),
    applePayAppData: checkoutSelectors.getApplePayAppDataApp(state),
    cartValue: getGrandTotal(state),
    isECOM: BagPageSelectors.getECOMStatus(state),
    isCartLoading: getCartLoadingState(state),
    isOptmisticAddToBagModal: !!getOptimisticAddBagModal(state),
    isStickyCheckoutPaymentDisabled: getIsStickyPaymentAbTestEnabled(state),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddedToBagContainer);
