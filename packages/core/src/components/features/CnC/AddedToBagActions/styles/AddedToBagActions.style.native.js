// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const applyPositionClassStyle = `
  top: 0;
  position:absolute;
  height:100%;
  width:100%;
  z-index:997;
`;

const payPalDynamicStyle = (props) => {
  if (props.isPayPalWebViewEnable) {
    return `
    top: 0;
    bottom:0;
    left:0;
    right:0;
    position:absolute;
    height:100%;
    width:100%;
    z-index:997;
    margin:0px;
    padding:0px;
    margin-left:0px;
    margin-right:0px;
    margin-top: 0px;
    `;
  }
  return '';
};

export const ActionsWrapper = styled.View`
  display: flex;
  min-height: 40px;
  ${payPalDynamicStyle}
  ${(props) => (props.showElement ? `` : `display:none`)}
  ${(props) => (props.isCartLoading ? `display:none` : ``)}
`;

export const ButtonWrapperAddedToBag = styled.View`
  ${(props) =>
    props.isPayPalWebViewEnable
      ? applyPositionClassStyle
      : `
    margin: 0 10px;
    display: flex;
    margin-top: 20px;
    flex-direction: ${props.isBothDisabled ? 'row' : 'column'};
`}
`;

export const ButtonWrapper = styled.View`
  margin: 0 ${(props) => (props.fromAddedToBagModal ? '10px' : 0)};
  display: flex;
  margin-top: ${(props) => props.marginTop};
  flex-direction: ${(props) => (props.isBothDisabled ? 'row' : 'column')};
  ${payPalDynamicStyle}
`;

export const ButtonViewWrapper = styled.View`
  position: relative;
  ${payPalDynamicStyle}
`;

export const ViewBagButton = styled.TouchableOpacity`
  background: ${(props) => props.theme.colors.PRIMARY.DARK};
  height: 42px;
  justify-content: center;
  align-items: center;
  margin: -5px 0 -5px;
`;

export const CheckoutButton = styled.TouchableOpacity`
  display: flex;

  height: 42px;
  justify-content: center;
  align-items: center;
  ${(props) =>
    props.isAddedTobag
      ? `
      margin: 0 0 16px;
      ${props.isBothDisabled ? `flex:1` : ``};
      ${props.isHalf ? `flex: 0.5` : ``};
    `
      : `
      margin: 0 10px 8px;
      flex: ${props.isHalf ? `0.5` : `1`};
    `}
`;

export const PaymentsButtonWrapper = styled.View`
  margin: 0 ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  display: flex;
  flex: 0.5;
  ${(props) => (props.canRenderApplePayButton && !props.hasNotch ? `margin-bottom: 10px` : '')}
`;

export const KeepShoppingButtonWrapper = styled.View`
  flex: 1;
  flex-direction: row;
  ${(props) =>
    props.isNewRedesignedModal
      ? `
    margin: 0;
    height: auto;
    justify-content: center;
  `
      : `
    margin: 0 ${props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    height: ${props.theme.spacing.LAYOUT_SPACING.MED};
  `}
`;

export const ShopBagButtonWrapper = styled.View`
  flex: 1;
`;

export const BagWithShoppingButton = styled.TouchableOpacity`
  background: ${(props) => props.theme.colors.PRIMARY.DARK};
  height: 42px;
  justify-content: center;
  align-items: center;
  flex: 0.5;
  margin: 2px;
`;

export const ShoppingButton = styled.TouchableOpacity`
  border: 1px solid ${(props) => props.theme.colors.BLACK};
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
  height: 42px;
  justify-content: center;
  align-items: center;
  margin: 2px;
  flex: 0.5;
`;

export const PaypalPaymentsButtonWrapper = styled.View`
  display: flex;
  ${(props) => (props.isAddedTobag && props.isPayPalEnabled ? `flex: 0.5` : ``)};
  ${(props) => (!props.isAddedTobag ? `flex:0.5` : '')}
  margin-left: ${(props) => (props.isAddedTobag ? '0' : '10px')};
  margin-right: ${(props) => (props.isAddedTobag && props.isPayPalEnabled ? '10px' : '0')};
  ${payPalDynamicStyle}
`;

export const ButtonsLoaderWrapper = styled.View`
  padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: 20px;
`;

export const AfterPayLoadingWrapper = styled.View`
  margin-top: 5px;
`;

export const VenmoPaypalWrapper = styled.View`
  flex: 1;
  flex-direction: row;
  ${payPalDynamicStyle}
`;

export const AfterPayWrapper = styled.View`
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
`;

export default {
  ButtonWrapperAddedToBag,
  ButtonWrapper,
  ActionsWrapper,
  ViewBagButton,
  CheckoutButton,
  PaymentsButtonWrapper,
  KeepShoppingButtonWrapper,
  BagWithShoppingButton,
  ShoppingButton,
  AfterPayWrapper,
};
