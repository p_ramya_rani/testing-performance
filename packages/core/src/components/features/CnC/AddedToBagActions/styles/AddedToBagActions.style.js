// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const getSpacing = (props) =>
  props.isATBModalBackAbTestNewDesign ? props.theme.spacing.ELEM_SPACING.MED : '32px';

const ctaStyle = css`
  .paymentHidden {
    display: none;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
  }
  .checkout-divider {
    display: none;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isStickyCheckoutButtonEnabled &&
        `
      display: flex;
      justify-content: center;
      margin-bottom:10px;
      font-weight: ${props.theme.typography.fontWeights.regular};
      `};
    }
  }
  .checkout-divider:before,
  .checkout-divider:after {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
      content: '';
      flex: 1;
      border-bottom: groove 1px;
      margin: auto 10px auto 87px;
    }
  }
  .checkout-divider:after {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin: auto 87px auto 10px;
    }
  }

  display: block;
  margin-bottom: ${(props) => (props.isBagPageStickyHeader ? '0' : getSpacing(props))};
  .check-out-container {
    margin-top: 10px;
    display: flex;
    justify-content: space-between;
  }
  .view-bag {
    &:hover {
      background: ${(props) => props.theme.colors.PRIMARY.DARK};
    }
    width: inherit;
    background-color: ${(props) => props.theme.colors.PRIMARY.DARK};
    height: 48px;
  }

  .checkout-button {
    padding-top: 10px;
    position: relative;
  }

  .checkout {
    ${(props) => (props.newMiniBag ? `border-radius: 6px;` : '')};
    ${(props) => (props.newMiniBag && props.theme.isGymboree ? `border-radius: 24px;` : '')};
    &:hover {
      background: ${(props) => props.theme.colorPalette.blue.C900};
    }
    height: 48px;
    margin-left: ${(props) =>
      props.isInternationalShipping ? '0px' : props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    ${(props) => (!props.isVenmoEnabled && !props.isPayPalEnabled ? `margin-left: 0;` : '')};
    flex: 1;
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-left: 0;
    }
  }
  .payPal-button {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      display: none;
    }
  }

  .paypal-venmo {
    display: flex;
    flex-direction: row;
    margin-bottom: 8px;
    @media ${(props) => props.theme.mediaQuery.mediumOnlyPortrait} {
      flex-direction: column;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isStickyCheckoutButtonEnabled &&
        !props.isCheckoutBannerHidden &&
        `
      display: block;
      `}
    }
  }
  .paypal-wrapper {
    width: ${(props) => (props.isApplePayVisible && !props.isMiniBag ? '48%' : '100%')};
    z-index: 0;
    @media ${(props) => props.theme.mediaQuery.large} {
      min-width: ${(props) => (props.isApplePayVisible && !props.isMiniBag ? '' : '202px')};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnlyPortrait} {
      width: 100%;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isStickyCheckoutButtonEnabled &&
        !props.isCheckoutBannerHidden &&
        `
        width: 100%;
      `}
    }
  }
  .apple-wrapper {
    width: ${(props) => (props.isApplePayVisible && !props.isMiniBag ? '48%' : '0')};
    margin-left: ${(props) => (props.isApplePayVisible && !props.isMiniBag ? '4%' : '0')};
    @media ${(props) => props.theme.mediaQuery.mediumOnlyPortrait} {
      width: 100%;
      margin-left: 0;
      margin-top: 10px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isStickyCheckoutButtonEnabled &&
        !props.isCheckoutBannerHidden &&
        `
        width: 100%;
        margin-left: 0;
        margin-top: 10px;
      `}
    }
  }
  .paypal-wrapper-atb {
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-right: 10px;
    }
  }
  .venmo-wrapper {
    display: none;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: block;
      width: 100%;
      margin-left: 6px;
      ${(props) =>
        props.isStickyCheckoutButtonEnabled &&
        !props.isCheckoutBannerHidden &&
        `
      padding: 8px 0px 0px 0px;
      margin-left: 0;
      `}
    }
  }
  .checkoutBtn-wrapper {
    display: block;
    width: ${(props) => (props.newMiniBag ? '52%' : '100%')};
  }
  .addBagActions-error {
    background: transparent;

    span {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
  }
  .checkoutBtnTracker {
    display: inline-flex;
    flex: 1;
    width: 100%;
    ${(props) =>
      (props.isStickyCheckoutButtonEnabled || props.isStickyCheckoutPaymentDisabled) &&
      `
    width:100%;
    `}
  }

  .checkoutSkeleton {
    padding: 0px;
    &:hover {
      background: ${(props) => props.theme.colorPalette.blue.C900};
    }
    height: 48px;
    margin-left: ${(props) =>
      props.isInternationalShipping ? '0px' : props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    ${(props) => (!props.isVenmoEnabled && !props.isPayPalEnabled ? `margin-left: 0;` : '')};
    flex: 1;
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-left: 0;
    }
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
  ${(props) =>
    props.isCheckoutBannerHidden &&
    props.isStickyCheckoutButtonEnabled &&
    `
    .sticky-checkout-button-new {
      bottom: 0;
    left: 0;
    position: fixed;
    width: 100%;
    background: ${props.theme.colors.WHITE};
    z-index: ${props.theme.zindex.zLoader};
    margin: 0;
    border-top: ${props.theme.spacing.ELEM_SPACING.XXXS} solid
      ${props.theme.colors.PRIMARY.LIGHTGRAY};
    padding: ${props.theme.spacing.ELEM_SPACING.XS}
      ${props.theme.spacing.ELEM_SPACING.MED};
  }
  `}

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    ${(props) =>
      props.isScrolling
        ? `opacity: 0;animation: fadeOut ease 1.5s;`
        : `opacity: 1;animation: fadeIn ease 0.2s;`};
    @keyframes fadeOut {
      0% {
        opacity: 1;
      }
      20% {
        opacity: 0;
      }
    }
    @keyframes fadeIn {
      0% {
        opacity: 0;
      }
      20% {
        opacity: 1;
      }
    }
  }
  .view-bag-container {
    display: flex;
  }
  .keep-shopping {
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    height: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    padding: 0;
  }
  .afterpay-msg-section {
    text-align: center;
    width: 100%;
    clear: both;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .redesigned-bag-container {
    justify-content: space-between;
  }
  .redesigned-keep-shopping,
  .redesigned-view-bag {
    padding: 16px 0;
    border-radius: 6px;
    width: 49%;
    letter-spacing: 1.08px;
  }
  .redesigned-keep-shopping {
    &:hover {
      background-color: ${(props) => props.theme.colorPalette.blue.C900};
    }
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
  }
  .redesigned-view-bag {
    border: solid 1px ${(props) => props.theme.colorPalette.gray[600]};
  }
  .invisible-div {
    position: absolute;
    top: -10px;
    padding-bottom: 10px;
    width: 100%;
    height: 100%;
  }

  .paymentsImage {
    display: inline-block;
    width: 85px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 50px;
    }
    img {
      vertical-align: middle;
    }
  }
  .otherPaymentsImage {
    display: inline-block;
    width: 115px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 63px;
    }
    img {
      vertical-align: middle;
    }
  }
  .paypalPaymentsImage {
    display: inline-block;
    width: 75px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 40px;
    }
    img {
      vertical-align: middle;
    }
  }
  .payPalIcon {
    display: inline-block;
    width: 44px;
    img {
      vertical-align: middle;
      height: 26px;
    }
  }
  .payPalImage {
    display: flex;
    flex-direction: row;
    width: 44px;
  }
  .paymentLink {
    text-decoration: underline;
    cursor: pointer;
    margin-right: 5px;
  }

  img.venmo-overlay-image {
    width: 75px;
    height: 40px;
  }

  img.apple-pay-image {
    height: 12px;
  }
`;

export default ctaStyle;
