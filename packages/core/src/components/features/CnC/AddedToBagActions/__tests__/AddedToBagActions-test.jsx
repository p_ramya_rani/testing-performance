// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AddedToBagActionsVanilla } from '../views/AddedToBagActions.view';

const errorMessage = 'something here went wrong';
describe('AddedToBagActions component', () => {
  it('AddedToBagActions component renders correctly', () => {
    const props = {
      className: 'checkout',
      onClickViewBag: jest.fn(),
      labels: {},
      modalInfo: {},
      checkoutServerError: {
        errorMessage,
      },
    };
    const component = shallow(<AddedToBagActionsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('AddedToBagActions component with Venmo Error renders correctly', () => {
    const props = {
      className: 'checkout',
      onClickViewBag: jest.fn(),
      labels: {},
      modalInfo: {},
      checkoutServerError: {
        errorMessage,
      },
      venmoError: 'Venmo Authentication failed',
    };
    const component = shallow(<AddedToBagActionsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('AddedToBagActions component with Apple isApplePayEnabled renders correctly', () => {
    const initialprops = {
      className: 'checkout',
      onClickViewBag: jest.fn(),
      labels: {},
      modalInfo: {},
      checkoutServerError: {
        errorMessage,
      },
      isApplePayEnabled: true,
    };
    const component = shallow(<AddedToBagActionsVanilla {...initialprops} />);
    expect(component).toMatchSnapshot();
  });

  it('AddedToBagActions displays view bag CTA', () => {
    const initialprops = {
      className: 'checkout',
      onClickViewBag: jest.fn(),
      labels: {},
      modalInfo: {},
      checkoutServerError: {
        errorMessage,
      },
      isApplePayEnabled: true,
      showAddTobag: true,
    };
    const component = shallow(<AddedToBagActionsVanilla {...initialprops} />);
    expect(component.find('.view-bag-container')).toHaveLength(1);
  });

  it('AddedToBagActions Should not display view bag CTA', () => {
    const initialprops = {
      className: 'checkout',
      onClickViewBag: jest.fn(),
      labels: {},
      modalInfo: {},
      checkoutServerError: {
        errorMessage,
      },
      isApplePayEnabled: true,
      showAddTobag: false,
    };
    const component = shallow(<AddedToBagActionsVanilla {...initialprops} />);
    expect(component.find('.view-bag-container')).toHaveLength(0);
  });

  it('AddedToBagActions Should display keep shopping CTA', () => {
    const initialprops = {
      className: 'checkout',
      onClickViewBag: jest.fn(),
      labels: {},
      modalInfo: {},
      checkoutServerError: {
        errorMessage,
      },
      isApplePayEnabled: true,
      showAddTobag: true,
      enableKeepShoppingCTA: true,
      isATBModalBackAbTestNewDesign: true,
    };
    const component = shallow(<AddedToBagActionsVanilla {...initialprops} />);
    expect(component.find('.keep-shopping')).toHaveLength(1);
  });

  it('AddedToBagActions Should not display keep shopping CTA', () => {
    const initialprops = {
      className: 'checkout',
      onClickViewBag: jest.fn(),
      labels: {},
      modalInfo: {},
      checkoutServerError: {
        errorMessage,
      },
      isApplePayEnabled: true,
      showAddTobag: true,
      enableKeepShoppingCTA: false,
    };
    const component = shallow(<AddedToBagActionsVanilla {...initialprops} />);
    expect(component.find('.keep-shopping')).toHaveLength(0);
  });

  it('should render view-bag-container redesigned-bag-container button', () => {
    const initialprops = {
      className: 'checkout',
      onClickViewBag: jest.fn(),
      labels: {},
      modalInfo: {},
      checkoutServerError: {
        errorMessage,
      },
      isApplePayEnabled: true,
      showAddTobag: true,
    };
    const component = shallow(<AddedToBagActionsVanilla {...initialprops} />);
    const wrapper = component.find("[className='view-bag-container redesigned-bag-container']");
    expect(wrapper.length).toBe(1);
  });
});
