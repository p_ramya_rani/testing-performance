// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ConfirmationView from '../views/Confirmation.view.native';

const labels = {
  atingTitle: '',
  ratingTitle: '',
  ratingBody: '',
  ratingSureCTA: '',
  ratingMayBeCTA: '',
  ratingNoThanksCTA: '',
  URLAndroid: '',
  URLIos: '',
};

describe('ConfirmationView', () => {
  it('should render correctly', () => {
    const props = {
      isGuest: true,
      labels,
    };
    const tree = shallow(<ConfirmationView {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly', () => {
    const props1 = {
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [{ center: { shippingFullname: 'ship', orderType: 'BOSS' } }],
      },
      orderDetails: { date: '', orderNumber: 2345, trackingLink: '/' },
      orderShippingDetails: { address: {}, orderTotal: 23.45, itemsCount: 5 },
      labels,
    };
    const tree = shallow(<ConfirmationView {...props1} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly without order details', () => {
    const props1 = {
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [{ center: { shippingFullname: 'ship', orderType: 'BOSS' } }],
      },
      isVenmoPaymentInProgress: true,
      venmoPayment: {},
      labels,
    };
    const tree = shallow(<ConfirmationView {...props1} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with venmo Information', () => {
    const props1 = {
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [{ center: { shippingFullname: 'ship', orderType: 'BOSS' } }],
      },
      isVenmoPaymentInProgress: true,
      venmoUserName: 'test-user',
      venmoOrderConfirmationContent: {},
      labels,
    };
    const tree = shallow(<ConfirmationView {...props1} />);
    expect(tree).toMatchSnapshot();
  });
});
