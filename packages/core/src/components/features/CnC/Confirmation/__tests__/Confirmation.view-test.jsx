// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { isCanada } from '@tcp/core/src/utils';
import { ConfirmationViewVanilla } from '../views/Confirmation.view';
import ConfirmationAccountFormContainer from '../../common/organism/ConfirmationAccountForm';

jest.mock('@tcp/core/src/utils', () => ({
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  getViewportInfo: () => ({
    isMobile: true,
  }),
  parseBoolean: () => true,
  isCanada: jest.fn(),
  isUsOnly: jest.fn(),
  isTCP: jest.fn(),
  getIconPath: jest.fn(),
  getBrand: () => 'tcp',
  isMobileApp: () => false,
  getStaticFilePath: jest.fn,
}));

isCanada.mockImplementation(() => false);

let props = {};
describe('ConfirmationViewVanilla', () => {
  beforeEach(() => {
    props = {
      isGuest: true,
    };
  });

  it('should render correctly', () => {
    const props1 = {
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [{ shippingFullname: 'ship', orderType: 'BOSS' }],
      },
      orderDetails: { date: '', orderNumber: 2345, trackingLink: '/' },
      orderShippingDetails: { address: {}, orderTotal: 23.45, itemsCount: 5 },
    };
    const tree = shallow(<ConfirmationViewVanilla {...props1} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with venmo payment progress', () => {
    const props1 = {
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [{ shippingFullname: 'ship', orderType: 'BOSS' }],
      },
      orderDetails: { date: '', orderNumber: '', trackingLink: '/' },
      orderShippingDetails: { address: {}, orderTotal: 23.45, itemsCount: 5 },
      isVenmoPaymentInProgress: true,
      venmoPayment: {},
    };
    const tree = shallow(<ConfirmationViewVanilla {...props1} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly without order details', () => {
    const props1 = {
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [{ center: { shippingFullname: 'ship', orderType: 'BOSS' } }],
      },
      isVenmoPaymentInProgress: true,
      venmoPayment: {},
    };
    const tree = shallow(<ConfirmationViewVanilla {...props1} />);
    expect(tree).toMatchSnapshot();
  });

  it('should render confirmation account form', () => {
    const tree = shallow(<ConfirmationViewVanilla {...props} />);
    expect(tree.find(ConfirmationAccountFormContainer).length).toBe(1);
  });

  it('should show plcc border on confirmation account form', () => {
    const tree = shallow(<ConfirmationViewVanilla {...props} />);
    expect(tree.find('.plcc-border').length).toBe(1);
  });

  it('should not show plcc border on confirmation account form', () => {
    isCanada.mockImplementation(() => true);
    const tree = shallow(<ConfirmationViewVanilla {...props} />);
    expect(tree.find('.plcc-border').length).toBe(0);
    isCanada.mockImplementation(() => false);
  });

  it('should not render confirmation account form', () => {
    props.isGuest = false;
    const tree = shallow(<ConfirmationViewVanilla {...props} />);
    expect(tree.find(ConfirmationAccountFormContainer).length).toBe(0);
  });
  it('should render correctly without shipping', () => {
    const props1 = {
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [{ shippingFullname: '', orderType: 'BOSS' }],
      },
      orderDetails: { date: '', orderNumber: 2345, trackingLink: '/' },
      orderShippingDetails: { address: {}, orderTotal: 23.45, itemsCount: 5 },
    };
    const tree = shallow(<ConfirmationViewVanilla {...props1} />);
    expect(tree).toMatchSnapshot();
  });
});
