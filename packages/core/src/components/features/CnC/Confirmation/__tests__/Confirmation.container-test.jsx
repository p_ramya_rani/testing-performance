// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import {
  ConfirmationContainerVanilla,
  mapDispatchToProps,
} from '../container/Confirmation.container';

describe('ConfirmationContainer', () => {
  it('should render correctly', () => {
    const mockedFetchUpdateOrderDetails = jest.fn();
    const mockedFetchModuleXContent = jest.fn();
    const props = {
      fetchUpdateOrderDetails: mockedFetchUpdateOrderDetails,
      fetchModuleXContent: mockedFetchModuleXContent,
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [
          {
            orderType: 'BOSS',
          },
        ],
      },
      updateOrderDetailsBossId: 'boss-id',
      updateOrderDetailsBopisId: 'bopis-id',
      addressIds: {
        previousId: 0,
        currentId: 0,
      },
      updateAddressIds: jest.fn(),
      order: {
        orderItems: [
          {
            productId: '123',
            productInfo: {
              name: 'abc',
            },
            category: '',
            itemAtributes: {
              UPC: '12345',
              TCPSize: 'XS',
            },
          },
        ],
        currencyCode: 'USD',
        orderDetails: {},
        summary: {},
        paymentsList: [
          {
            paymentMethod: 'VISA',
          },
        ],
      },
    };
    const tree = shallow(<ConfirmationContainerVanilla {...props} />);
    expect(mockedFetchUpdateOrderDetails).toBeCalled();
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with Bopis item', () => {
    const mockedFetchUpdateOrderDetails = jest.fn();
    const mockedFetchModuleXContent = jest.fn();
    const props = {
      fetchUpdateOrderDetails: mockedFetchUpdateOrderDetails,
      fetchModuleXContent: mockedFetchModuleXContent,
      orderNumbersByFullfillmentCenter: {
        fullfillmentCenterMap: [
          {
            orderType: 'BOPIS',
          },
        ],
      },
      updateOrderDetailsBossId: 'boss-id',
      updateOrderDetailsBopisId: 'bopis-id',
      addressIds: {
        previousId: 0,
        currentId: 0,
      },
      updateAddressIds: jest.fn(),
      order: {
        orderItems: [
          {
            productId: '123',
            productInfo: {
              name: 'abc',
            },
            category: '',
            itemAtributes: {
              UPC: '12345',
              TCPSize: 'XS',
            },
          },
        ],
        currencyCode: 'USD',
        orderDetails: {},
        summary: {},
        paymentsList: [
          {
            paymentMethod: 'VISA',
          },
        ],
      },
    };
    const tree = shallow(<ConfirmationContainerVanilla {...props} />);
    expect(mockedFetchUpdateOrderDetails).toBeCalled();
    expect(tree).toMatchSnapshot();
  });
  it('should call mapDispatchToProps', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.fetchUpdateOrderDetails();
    expect(dispatch.mock.calls).toHaveLength(1);
  });
  it('should call fetchModuleXContent in mapDispatchToProps', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.fetchModuleXContent();
    expect(dispatch.mock.calls).toHaveLength(1);
  });
});
