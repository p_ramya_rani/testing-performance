// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .confirmation-wrapper {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    @media ${props => props.theme.mediaQuery.large} {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .loyalty-confirmation-wrapper {
    background: ${props => props.theme.colorPalette.white};
    padding: 20px 0;
    margin-bottom: 16px;
    text-align: center;
  }
  .loyalty-banner {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }
  .section-container {
    background: #fff;
    margin-bottom: 16px;
    border-radius: 12px;
    .confirmation-create-account,
    .notification-width,
    input {
      background: inherit;
    }
  }
  .plcc-border {
    padding: 3px;
    background: linear-gradient(158deg, rgb(0, 167, 224) 50%, rgb(247, 151, 31) 50%);
    border: none;
    > div {
      background: #fff;
      border: 5px solid transparent;
      border-radius: 12px;
    }
  }
  .smsNotification {
    .notification-width {
      padding-left: ${props => props.theme.spacing.ELEM_SPACING.MED};
      padding-right: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
`;

export default styles;
