// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';

const getConfirmationLabels = (state) =>
  state.Labels && state.Labels.checkout && state.Labels.checkout.orderConfirmation;

const getReferAFriendLabels = createSelector(getConfirmationLabels, (confirmationLabels) => {
  return {
    referAFriendHeadingForFriend: getLabelValue(
      confirmationLabels,
      'lbl_refer_a_friend_heading_for_friend'
    ),
    referAFriendHeadingForYou: getLabelValue(
      confirmationLabels,
      'lbl_refer_a_friend_heading_for_you'
    ),
    referAFriendBtnText: getLabelValue(confirmationLabels, 'lbl_refer_a_friend_btn_text'),
  };
});

export default {
  getReferAFriendLabels,
};
