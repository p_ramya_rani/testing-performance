// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';
import { formValueSelector } from 'redux-form';
import constants from '../SMSNotification.constants';
import { isCanada } from '../../../../../../../utils';
import ConfirmationSelectors from '../../../container/Confirmation.selectors';

const { getOrderConfirmation } = ConfirmationSelectors;

/**
 * getConfirmationLabels
 * @param {Object} state
 * @description This selector provides the state of the confirmation labels
 */
const getConfirmationLabels = (state) =>
  state.Labels && state.Labels.checkout && state.Labels.checkout.orderConfirmation;

/**
 * @function getConfirmationCouponLabels
 * @param {Object} state
 * @description This selector provides the state of the confirmation page notification labels.
 * @returns {Object}
 */
const getConfirmationNotificationLabels = createSelector(
  getConfirmationLabels,
  (confirmationLabels) => {
    return {
      textAlertHeading: getLabelValue(confirmationLabels, 'lbl_smsNotification_heading_1'),
      promosAndArrivalsHeading: getLabelValue(confirmationLabels, 'lbl_smsNotification_heading_2'),
      joinNow: getLabelValue(confirmationLabels, 'lbl_smsNotification_joinNow_btn'),
      childrenPlaceLabel: getLabelValue(
        confirmationLabels,
        'lbl_smsNotification_childrenPlace_lbl'
      ),
      gymboreeLabel: getLabelValue(confirmationLabels, 'lbl_smsNotification_gymboree_lbl'),
      snjLabel: getLabelValue(confirmationLabels, 'lbl_sugarandjade'),
      phoneNumber: getLabelValue(confirmationLabels, 'lbl_smsNotification_phone_lbl'),
      subscribeSuccess: getLabelValue(confirmationLabels, 'lbl_smsNotification_subscribeSuccess'),
    };
  }
);

const getChildrenPalaceKey = (state) => {
  const selector = formValueSelector(constants.FORM_NAME);
  return selector(state, 'brandTCP');
};

const getGymboreeKey = (state) => {
  const selector = formValueSelector(constants.FORM_NAME);
  return selector(state, 'brandGYM');
};

const getSNJKey = (state) => {
  const selector = formValueSelector(constants.FORM_NAME);
  return selector(state, 'brandSNJ');
};

const getNotificationMsgContentInfo = (state) => {
  let notificationContent;
  const notificationMsg = isCanada()
    ? constants.Notification_Carrier_Message_CA
    : constants.Notification_Carrier_Message_US;
  if (
    state.Labels.checkout.orderConfirmation &&
    Array.isArray(state.Labels.checkout.orderConfirmation.referred)
  ) {
    notificationContent =
      state.Labels.checkout.orderConfirmation.referred.length &&
      state.Labels.checkout.orderConfirmation.referred.find((label) => {
        return label.name === notificationMsg;
      });
  }
  return notificationContent;
};

const getNotificationRichTextSelector = (state) => {
  const rContent = state.CartPageReducer.get('moduleXContent').find(
    (moduleX) => moduleX.name === getNotificationMsgContentInfo(state).name
  );
  return rContent && rContent.richText;
};

const getSubscribeSuccessMsgContentInfo = (state) => {
  let notificationSuccessMsgID;
  if (
    state.Labels.checkout.orderConfirmation &&
    Array.isArray(state.Labels.checkout.orderConfirmation.referred)
  ) {
    notificationSuccessMsgID =
      state.Labels.checkout.orderConfirmation.referred.length &&
      state.Labels.checkout.orderConfirmation.referred.find((label) => {
        return label.name === constants.Notification_Success_Msg;
      });
  }
  return notificationSuccessMsgID;
};

const getSubscribeSuccessMsgRichTextSelector = (state) => {
  const rContent = state.CartPageReducer.get('moduleXContent').find(
    (moduleX) => moduleX.name === getSubscribeSuccessMsgContentInfo(state).name
  );
  return rContent && rContent.richText;
};

/**
 * re-select selector to provide create account success value
 */
const getSmsNotificationSuccess = createSelector(
  getOrderConfirmation,
  (orderConfirmation) => orderConfirmation && orderConfirmation.smsNotificationSuccess
);

const getSmsNotificationError = createSelector(
  getOrderConfirmation,
  (orderConfirmation) => orderConfirmation && orderConfirmation.error
);

export default {
  getConfirmationNotificationLabels,
  getChildrenPalaceKey,
  getGymboreeKey,
  getNotificationRichTextSelector,
  getNotificationMsgContentInfo,
  getSubscribeSuccessMsgRichTextSelector,
  getSubscribeSuccessMsgContentInfo,
  getSmsNotificationSuccess,
  getSmsNotificationError,
  getSNJKey,
};
