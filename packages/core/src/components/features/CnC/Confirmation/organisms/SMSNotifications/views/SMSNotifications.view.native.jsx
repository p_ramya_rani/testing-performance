// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import getStandardConfig from '@tcp/core/src/utils/formValidation/validatorStandardConfig';
import createValidateMethod from '@tcp/core/src/utils/formValidation/createValidateMethod';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import withStyles from '../../../../../../common/hoc/withStyles';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import RichText from '../../../../../../common/atoms/RichText';
import {
  Styles,
  BrandWrapper,
  CheckBoxFieldWrapper,
  RichTextContainer,
  GymboreeCheckBoxTextWrapper,
  SuccessTextWrapper,
  SuccessRichTextContainer,
  SNJBrandWrapper,
} from '../styles/SMSNotifications.style.native';
import TextBox from '../../../../../../common/atoms/TextBox';
import Button from '../../../../../../common/atoms/Button';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';
import { Image } from '../../../../../../common/atoms';
import SMSNOTIFICATION_CONSTANTS from '../SMSNotification.constants';
import ErrorMessage from '../../../../common/molecules/ErrorMessage';

const doneIcon = require('../../../../../../../../../mobileapp/src/assets/images/done.png');

const getEnableAction = (smsNotificationSuccess, isChecked) => {
  return !smsNotificationSuccess && isChecked;
};
const getBrand = (isGymboree, isChildrenPalace, isSugarNJade) => {
  return isGymboree || isChildrenPalace || isSugarNJade;
};
const renderBrand = ({ saved, labels, handleChange, isGymboreeCheckBoxShow, isSNJEnabled }) => {
  return (
    !saved && (
      <>
        <BrandWrapper>
          <CheckBoxFieldWrapper>
            <Field
              name="brandTCP"
              component={InputCheckbox}
              dataLocator="children-place-checkbox"
              enableSuccessCheck={false}
              className="children-place-checkbox"
              onChange={handleChange}
              fontSize="fs16"
              fontFamily="secondary"
              fontWeight="regular"
              marginTopZero
              rightText={labels.childrenPlaceLabel}
            />
          </CheckBoxFieldWrapper>
          {!isGymboreeCheckBoxShow && (
            <GymboreeCheckBoxTextWrapper>
              <Field
                name="brandGYM"
                component={InputCheckbox}
                dataLocator="gymboree-checkbox"
                enableSuccessCheck={false}
                className="gymboree-checkbox"
                onChange={handleChange}
                fontSize="fs16"
                fontFamily="secondary"
                fontWeight="regular"
                marginTopZero
                rightText={labels.gymboreeLabel}
              />
            </GymboreeCheckBoxTextWrapper>
          )}
        </BrandWrapper>
        {isSNJEnabled && (
          <SNJBrandWrapper>
            <Field
              name="brandSNJ"
              component={InputCheckbox}
              dataLocator="snj-checkbox"
              enableSuccessCheck={false}
              className="children-place-checkbox"
              onChange={handleChange}
              fontSize="fs16"
              fontFamily="secondary"
              fontWeight="regular"
              marginTopZero
              rightText={labels.snjLabel}
            />
          </SNJBrandWrapper>
        )}
      </>
    )
  );
};
/**
 * @function SMSNotifications
 * @description wrapper for sms notification.
 */
const SMSNotifications = ({
  labels,
  notificationCarrierMessage,
  isGymboree,
  isChildrenPalace,
  handleSubmit,
  smsNotificationSubmit,
  smsNotificationSuccess,
  smsNotificationError,
  resetNotificationErrorState,
  subscribeSuccessMsg,
  isCanada,
  isTCP,
  isSugarNJade,
  isSNJEnabled,
}) => {
  const formSubmit = (formValues) => {
    resetNotificationErrorState();
    smsNotificationSubmit({
      ...formValues,
    });
  };
  const handleChange = () => {
    if (smsNotificationError) {
      resetNotificationErrorState();
    }
  };

  const saved = smsNotificationSuccess;
  const isChecked = getBrand(isGymboree, isChildrenPalace, isSugarNJade);
  const enableActions = getEnableAction(smsNotificationSuccess, isChecked);
  const isGymboreeCheckBoxShow = isCanada && isTCP;

  return (
    <ViewWithSpacing spacingStyles="margin-top-SM margin-bottom-LRG">
      {smsNotificationError && <ErrorMessage showAccordian error={smsNotificationError} />}

      {!saved && (
        <>
          <BodyCopy
            fontSize="fs16"
            fontFamily="secondary"
            className=""
            dataLocator="join-our-text-alerts"
            textAlign="center"
            fontWeight="extrabold"
            text={labels.textAlertHeading}
          />
          <ViewWithSpacing spacingStyles="margin-top-MED margin-bottom-LRG">
            <BodyCopy
              fontSize="fs16"
              fontFamily="secondary"
              dataLocator="special-promos-new-arrivals"
              textAlign="center"
              text={labels.promosAndArrivalsHeading}
            />
          </ViewWithSpacing>
        </>
      )}
      {saved && (
        <BrandWrapper>
          <Image
            alt={labels.subscribeSuccess}
            source={doneIcon}
            width="25px"
            height="25px"
            title={labels.subscribeSuccess}
            data-locator="ChkmarkIcon"
          />
          <SuccessTextWrapper>
            <BodyCopy
              fontSize="fs16"
              fontFamily="secondary"
              dataLocator="join-our-text-alerts"
              textAlign="center"
              fontWeight="extrabold"
              text={labels.subscribeSuccess}
            />
          </SuccessTextWrapper>
        </BrandWrapper>
      )}
      {saved && (
        <SuccessRichTextContainer>
          <RichText source={{ html: subscribeSuccessMsg }} dataLocator="success-message" />
        </SuccessRichTextContainer>
      )}

      {renderBrand({ saved, labels, handleChange, isGymboreeCheckBoxShow, isSNJEnabled })}
      {enableActions && (
        <Field
          id="notification-phoneNumber"
          label={labels.phoneNumber}
          name="phoneNumber"
          keyboardType="phone-pad"
          component={TextBox}
          maxLength={50}
          dataLocator="phone-number-field"
          enableSuccessCheck={false}
          onChange={handleChange}
        />
      )}
      {enableActions && (
        <RichTextContainer>
          <RichText source={{ html: notificationCarrierMessage }} dataLocator="carrier-message" />
        </RichTextContainer>
      )}
      {enableActions && (
        <ViewWithSpacing spacingStyles="margin-bottom-LRG">
          <Button
            buttonVariation="fixed-width"
            fill="BLUE"
            type="submit"
            data-locator="join-now-btn"
            text={labels.joinNow}
            onPress={handleSubmit(formSubmit)}
          />
        </ViewWithSpacing>
      )}
    </ViewWithSpacing>
  );
};
SMSNotifications.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  notificationCarrierMessage: PropTypes.string,
  isChildrenPalace: PropTypes.bool,
  isGymboree: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  smsNotificationSubmit: PropTypes.func.isRequired,
  smsNotificationSuccess: PropTypes.bool,
  smsNotificationError: PropTypes.string,
  resetNotificationErrorState: PropTypes.func.isRequired,
  subscribeSuccessMsg: PropTypes.string,
  isTCP: PropTypes.bool,
  isCanada: PropTypes.bool,
  isSugarNJade: PropTypes.bool,
  isSNJEnabled: PropTypes.bool,
};
SMSNotifications.defaultProps = {
  notificationCarrierMessage: '',
  isChildrenPalace: false,
  isGymboree: false,
  smsNotificationSuccess: false,
  smsNotificationError: '',
  subscribeSuccessMsg: '',
  isTCP: false,
  isCanada: false,
  isSugarNJade: false,
  isSNJEnabled: false,
};

const validateMethod = createValidateMethod(getStandardConfig(['phoneNumber']));

export default reduxForm({
  form: SMSNOTIFICATION_CONSTANTS.FORM_NAME, // a unique identifier for this form
  enableReinitialize: true,
  ...validateMethod,
})(withStyles(SMSNotifications, Styles));

export { SMSNotifications as SMSNotificationsVanilla };
