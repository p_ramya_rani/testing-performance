// 9fbef606107a605d69c0edbcd8029e5d 
const SMSNOTIFICATION_CONSTANTS = {
  FORM_NAME: 'smsNotification',
  Notification_Carrier_Message_US: 'Notification_Carrier_Message_US',
  Notification_Carrier_Message_CA: 'Notification_Carrier_Message_CA',
  Notification_Success_Msg: 'Notification_Success_Msg',
};

export default SMSNOTIFICATION_CONSTANTS;
