// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import SMSNotificationForm from '../views';
import SMSNotificationSelectors from './SMSNotifications.selectors';
import { toastMessageInfo } from '../../../../../../common/atoms/Toast/container/Toast.actions';
import { smsNotification, resetNotificationErr } from '../../../container/Confirmation.actions';
import { isCanada, isTCP } from '../../../../../../../utils';
import BAG_PAGE_ACTIONS from '../../../../BagPage/container/BagPage.actions';

/**
 * @function SMSNotificationFormContainer
 * @param {Object} props
 * @return {JSX} Render Container
 */

export class SMSNotificationFormContainer extends React.Component {
  static propTypes = {
    labels: PropTypes.shape({}).isRequired,
    navigation: PropTypes.shape({}).isRequired,
    notificationCarrierMessage: PropTypes.shape({}),
    isChildrenPalace: PropTypes.bool,
    isGymboree: PropTypes.bool,
    resetNotificationErrorState: PropTypes.func.isRequired,
    smsNotificationSubmit: PropTypes.func.isRequired,
    subscribeSuccessMsg: PropTypes.string,
    smsNotificationSuccess: PropTypes.bool,
    smsNotificationError: PropTypes.string,
    fetchContent: PropTypes.func.isRequired,
    notificationMsgContentId: PropTypes.string,
    subscribeSuccessMsgContentInfo: PropTypes.string,
    isSugarNJade: PropTypes.bool,
    isSNJEnabled: PropTypes.bool,
  };

  static defaultProps = {
    notificationCarrierMessage: null,
    isChildrenPalace: false,
    isGymboree: false,
    subscribeSuccessMsg: null,
    smsNotificationSuccess: false,
    smsNotificationError: null,
    notificationMsgContentId: null,
    subscribeSuccessMsgContentInfo: null,
    isSugarNJade: false,
    isSNJEnabled: false,
  };

  /**
   * @function componentDidMount
   * called when component is mount and calls getInitialProps method of wrapped component
   * and adds didFocus listener to the view which is called every time view is displayed
   *
   */
  componentDidMount() {
    const { notificationMsgContentId, fetchContent, subscribeSuccessMsgContentInfo } = this.props;
    fetchContent([notificationMsgContentId, subscribeSuccessMsgContentInfo]);
  }

  /**
   * renders wrapped component
   *
   * @returns
   */
  render() {
    const { notificationCarrierMessage, labels, navigation, isChildrenPalace } = this.props;
    const {
      isGymboree,
      resetNotificationErrorState,
      smsNotificationSubmit,
      subscribeSuccessMsg,
      smsNotificationSuccess,
      smsNotificationError,
      isSugarNJade,
      isSNJEnabled,
    } = this.props;
    return (
      <SMSNotificationForm
        labels={labels}
        notificationCarrierMessage={notificationCarrierMessage}
        navigation={navigation}
        isChildrenPalace={isChildrenPalace}
        isGymboree={isGymboree}
        resetNotificationErrorState={resetNotificationErrorState}
        smsNotificationSubmit={smsNotificationSubmit}
        subscribeSuccessMsg={subscribeSuccessMsg}
        smsNotificationSuccess={smsNotificationSuccess}
        smsNotificationError={smsNotificationError}
        isTCP={isTCP()}
        isCanada={isCanada()}
        isSugarNJade={isSugarNJade}
        isSNJEnabled={isSNJEnabled}
      />
    );
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    smsNotificationSubmit: (payload) => {
      dispatch(smsNotification(payload));
    },
    resetNotificationErrorState: () => {
      dispatch(resetNotificationErr());
    },
    toastMessage: (palyoad) => {
      dispatch(toastMessageInfo(palyoad));
    },
    fetchContent: (contentIds) => {
      dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    initialValues: {},
    labels: SMSNotificationSelectors.getConfirmationNotificationLabels(state),
    smsNotificationSuccess: SMSNotificationSelectors.getSmsNotificationSuccess(state),
    smsNotificationError: SMSNotificationSelectors.getSmsNotificationError(state),
    notificationCarrierMessage: SMSNotificationSelectors.getNotificationRichTextSelector(state),
    isChildrenPalace: SMSNotificationSelectors.getChildrenPalaceKey(state),
    isGymboree: SMSNotificationSelectors.getGymboreeKey(state),
    isSugarNJade: SMSNotificationSelectors.getSNJKey(state),
    subscribeSuccessMsg: SMSNotificationSelectors.getSubscribeSuccessMsgRichTextSelector(state),
    notificationMsgContentInfo: SMSNotificationSelectors.getNotificationMsgContentInfo(state),
    subscribeSuccessMsgContentInfo:
      SMSNotificationSelectors.getSubscribeSuccessMsgContentInfo(state),
    isSNJEnabled: getIsSNJEnabled(state),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SMSNotificationFormContainer);
