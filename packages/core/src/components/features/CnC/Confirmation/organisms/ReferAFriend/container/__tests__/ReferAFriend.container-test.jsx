// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ReferAFriendContainer } from '../ReferAFriend.container';

describe('ReferAFriend Component', () => {
  let component;
  let props;
  beforeEach(() => {
    props = {
      labels: {},
      className: '',
      isReferAFriendTileEnabled: true,
    };
  });

  it('ReferAFriend should be defined', () => {
    component = shallow(<ReferAFriendContainer {...props} />);
    expect(component).toBeDefined();
  });

  it('ReferAFriend should render', () => {
    component = shallow(<ReferAFriendContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
