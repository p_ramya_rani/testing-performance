// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../../../common/hoc/withStyles';
import Row from '../../../../../../common/atoms/Row';
import styles from '../styles/ReferAFriend.style';
import Button from '../../../../../../common/atoms/Button';

const ReferAFriend = ({ labels, className }) => {
  return (
    <div className={`${className} refer-a-friend-confirmation`}>
      <Row fullBleed className="refer-a-friend-row">
        <div className="refer-a-friend-container">
          <div className="refer-a-friend-col refer-a-friend-heading">
            {labels.referAFriendHeadingForFriend}
            <span className="refer-a-friend-heading-for-you">
              {labels.referAFriendHeadingForYou}
            </span>
          </div>
          <div className="refer-a-friend-col">
            <Button
              id="extole_zone_confirmation"
              className="refer-a-friend-button-cta"
              fullWidth
              buttonVariation="fixed-width"
            >
              {labels.referAFriendBtnText}
            </Button>
          </div>
        </div>
      </Row>
    </div>
  );
};
ReferAFriend.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
};
ReferAFriend.defaultProps = {
  className: '',
};

export default withStyles(ReferAFriend, styles);

export { ReferAFriend as ReferAFriendVanilla };
