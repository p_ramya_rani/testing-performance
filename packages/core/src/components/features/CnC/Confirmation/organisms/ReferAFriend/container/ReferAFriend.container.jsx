// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ReferAFriendSelector from './ReferAFriend.selectors';
import ReferAFriend from '../views';

export class ReferAFriendContainer extends React.Component {
  getLabel = (labels) => {
    return labels;
  };

  render() {
    const { labels } = this.props;
    return <ReferAFriend labels={this.getLabel(labels)} />;
  }
}

const mapStateToProps = (state) => {
  return {
    labels: ReferAFriendSelector.getReferAFriendLabels(state),
  };
};

ReferAFriendContainer.propTypes = {
  labels: PropTypes.shape({}).isRequired,
};

export default connect(mapStateToProps)(ReferAFriendContainer);
