// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ReferAFriendVanilla } from '../ReferAFriend.view';

describe('ReferAFriendVanilla', () => {
  it('should render correctly', () => {
    const props = {
      labels: {},
      className: '',
    };
    const tree = shallow(<ReferAFriendVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
