// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import PlaceCashBanner from '@tcp/core/src/components/features/CnC/PlaceCashBanner';
import ConfirmationFulfillmentCenterItemDisplay from '../../ConfirmationFulfillmentCenterItemDisplay';
import styles from '../styles/ThankYouComponent.styles';
import withStyles from '../../../../../../common/hoc/withStyles';
import Row from '../../../../../../common/atoms/Row';
import Col from '../../../../../../common/atoms/Col';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import RichText from '../../../../../../common/atoms/RichText';

/**
 * @function ThankYouComponent
 * @description wrapper for thank you component.
 */
const ThankYouComponent = ({
  isBossInList,
  orderNumbersByFullfillmentCenter,
  updateOrderDetailsData,
  fullfillmentCenterData,
  isGuest,
  labels,
  className,
  selectedShippingCode,
}) => {
  return (
    <div className={className}>
      {/* Shipping only and mixed orders are handled from one component */}
      <Row className="confirmation-fullfillment-center variable-width " fullBleed>
        {fullfillmentCenterData &&
          fullfillmentCenterData.map((center, index) => (
            <Col
              colSize={{ large: 6, medium: 8, small: 6 }}
              className="confirmation-fullfillment-center-item"
            >
              <ConfirmationFulfillmentCenterItemDisplay
                key={center.orderNumber}
                center={center}
                index={index}
                isGuest={isGuest}
                labels={labels}
                selectedShippingCode={selectedShippingCode}
              />
            </Col>
          ))}
      </Row>
      {orderNumbersByFullfillmentCenter && (
        <Row fullBleed className="variable-width ">
          <Col
            colSize={{
              large: 12,
              medium: 8,
              small: 6,
            }}
          >
            <BodyCopy
              fontSize={['fs16', 'fs16', 'fs20']}
              fontFamily="primary"
              className="confirmation-next-update-heading"
            >
              {labels.nextHeading}
            </BodyCopy>
            <BodyCopy fontSize={['fs14', 'fs14', 'fs16']} fontFamily="secondary">
              {isBossInList ? labels.nextDetailsBoss : labels.nextDetails}
            </BodyCopy>
          </Col>
        </Row>
      )}
      <PlaceCashBanner isOrderConfirmation className="variable-width" />
      <Row fullBleed className="variable-width ">
        <Col
          colSize={{
            large: 12,
            medium: 8,
            small: 6,
          }}
          className="confirmation-update-details"
        >
          <BodyCopy
            fontSize={['fs16', 'fs16', 'fs20']}
            fontFamily="primary"
            className="confirmation-next-update-heading"
          >
            {labels.updateOrderHeading}
          </BodyCopy>
          <BodyCopy fontSize={['fs14', 'fs14', 'fs16']} fontFamily="secondary">
            <RichText richTextHtml={updateOrderDetailsData} dataLocator="update-order-details" />
          </BodyCopy>
        </Col>
      </Row>
    </div>
  );
};
ThankYouComponent.propTypes = {
  className: PropTypes.string,
  /** Flag indicates whether the user is a guest */
  isGuest: PropTypes.bool,

  /** shipped order only details */
  orderDetails: PropTypes.shape({
    date: PropTypes.instanceOf(Date).isRequired,
    orderNumber: PropTypes.string.isRequired,
    trackingLink: PropTypes.string.isRequired,
  }).isRequired,

  /** Bopis order details */
  orderNumbersByFullfillmentCenter: PropTypes.shape({
    holdDate: PropTypes.instanceOf(Date).isRequired,
    fullfillmentCenterMap: PropTypes.shape([{}]),
  }).isRequired,
  updateOrderDetailsData: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  orderShippingDetails: PropTypes.shape({}),
  isBossInList: PropTypes.bool,
  fullfillmentCenterData: PropTypes.shape([{}]),
  selectedShippingCode: PropTypes.string,
};
ThankYouComponent.defaultProps = {
  className: '',
  isGuest: true,
  updateOrderDetailsData: null,
  orderShippingDetails: null,
  isBossInList: false,
  fullfillmentCenterData: null,
  selectedShippingCode: '',
};

export default withStyles(ThankYouComponent, styles);
export { ThankYouComponent as ThankYouComponentVanilla };
