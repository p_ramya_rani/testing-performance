// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';

/**
 * getConfirmationLabels
 * @param {Object} state
 * @description This selector provides the state of the confirmation category labels
 */
const getConfirmationLabels = state =>
  state.Labels && state.Labels.checkout && state.Labels.checkout.orderConfirmation;

const getAccessibilityLabels = state =>
  state.Labels && state.Labels.global && state.Labels.global.accessibility;

/**
 * @function getConfirmationCouponLabels
 * @param {Object} state
 * @description This selector provides the state of the confirmation page coupons labels.
 * @returns {Object}
 */
const getConfirmationCouponLabels = createSelector(
  [getConfirmationLabels, getAccessibilityLabels],
  (confirmationLabels, accessibilityLabels) => {
    return {
      heading1: getLabelValue(confirmationLabels, 'lbl_odmCoupons_heading_1'),
      heading2: getLabelValue(confirmationLabels, 'lbl_odmCoupons_heading_2'),
      webCode: getLabelValue(confirmationLabels, 'lbl_odmCoupons_webCode'),
      validTill: getLabelValue(confirmationLabels, 'lbl_odmCoupons_validTill'),
      nowThrough: getLabelValue(confirmationLabels, 'lbl_odmCoupons_nowThrough'),
      detailsLink: getLabelValue(confirmationLabels, 'lbl_odmCoupons_detailsLink'),
      printIconAltText: getLabelValue(accessibilityLabels, 'lbl_print_icon'),
    };
  }
);

export default {
  getConfirmationCouponLabels,
};
