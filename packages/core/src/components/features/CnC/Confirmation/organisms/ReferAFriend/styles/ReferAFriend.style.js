// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  background-color: ${(props) => props.theme.colorPalette.white};
  width: 100%;
  min-height: 132px;
  padding: 0px;
  border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};

  .refer-a-friend-button-cta {
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
    color: ${(props) => props.theme.colorPalette.white};
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    padding: 13px;
    width: fit-content;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};

    a {
      color: ${(props) => props.theme.colorPalette.white};
      text-decoration: none;
    }
  }
  .extole_zone_global_footer a {
    color: ${(props) => props.theme.colorPalette.white};
  }
  .refer-a-friend-heading {
    font-size: ${(props) => props.theme.typography.fontSizes.fs18};
    font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  }
  .refer-a-friend-container {
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    width: 100%;
  }
  .refer-a-friend-col {
    text-align: center;
    margin: 0px;
  }
  .refer-a-friend-heading-for-you {
    color: ${(props) => props.theme.colorPalette.blue[500]};
    margin-left: 10px;
  }
`;

export default styles;
