// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { SMSNotificationsVanilla } from '../SMSNotifications.view';

describe('SMSNotificationsVanilla', () => {
  it('should render correctly', () => {
    const props = {
      labels: {},
      handleSubmit: jest.fn(),
      smsNotificationSubmit: jest.fn(),
      resetNotificationErrorState: jest.fn(),
    };
    const tree = shallow(<SMSNotificationsVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly with props', () => {
    const props = {
      labels: {},
      handleSubmit: jest.fn(),
      smsNotificationSubmit: jest.fn(),
      resetNotificationErrorState: jest.fn(),
      smsNotificationSuccess: true,
      isGymboree: true,
      isCanada: true,
      isTCP: true,
      smsNotificationError: 'test',
      isSugarNJade: true,
      isSNJEnabled: true,
    };
    const tree = shallow(<SMSNotificationsVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
