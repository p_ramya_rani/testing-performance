// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getDateInformation } from '@tcp/core/src/utils';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import EddComponent from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent';
import CONFIRMATION_CONSTANTS from '../../../Confirmation.constants';
import ConfirmationItemDisplay from '../../ConfirmationItemDisplay';
import Anchor from '../../../../../../common/atoms/Anchor';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import styles from '../styles/ConfirmationOrderNumberDisplay.styles';
import withStyles from '../../../../../../common/hoc/withStyles';
import internalEndpoints from '../../../../../account/common/internalEndpoints';

const { orderPage, trackOrder } = internalEndpoints;

const getBossDate = (isBoss, bossDate) => {
  return isBoss ? bossDate : '';
};
const renderDate = (orderType, bossDate, bopisDate) => {
  const isBoss = orderType === CONFIRMATION_CONSTANTS.ORDER_ITEM_TYPE.BOSS;
  const isBopis = orderType === CONFIRMATION_CONSTANTS.ORDER_ITEM_TYPE.BOPIS;
  const date = isBopis ? bopisDate : getBossDate(isBoss, bossDate);
  return (
    <BodyCopy
      fontSize="fs22"
      fontFamily="secondary"
      fontWeight="extrabold"
      className="confirmation-fullfillment-type"
      textAlign="center"
      isValid={date}
    >
      {date}
    </BodyCopy>
  );
};

/**
 * @function ConfirmationOrderNumberDisplay
 * @description renders the order number component.
 */
const ConfirmationOrderNumberDisplay = ({
  center,
  isGuest,
  labels,
  className,
  selectedShippingCode,
}) => {
  const {
    orderDate,
    orderNumber,
    orderTotal,
    bossMaxDate,
    bossMinDate,
    orderType,
    productsCount,
    encryptedEmailAddress,
    shippingFullname,
  } = center;

  const today = getDateInformation('', false);
  const bossStartDate = bossMinDate ? getDateInformation(bossMinDate) : '';
  const bossEndDate = bossMaxDate ? getDateInformation(bossMaxDate) : '';
  const bossDate =
    !!(bossStartDate && bossEndDate) &&
    `${bossStartDate.day}, ${bossStartDate.month}
   ${bossStartDate.date} - ${bossEndDate.day}, ${bossEndDate.month} ${bossEndDate.date}`;
  const bopisDate = `${labels.bopisDate} ${today.month} ${today.date}`;

  return (
    <div className={className}>
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="black"
        className="confirmation-item-count"
      >
        {`${productsCount} ${productsCount > 1 ? labels.items : labels.item}`}
      </BodyCopy>
      {renderDate(orderType, bossDate, bopisDate)}
      {shippingFullname && (
        <EddComponent
          selectedShipment={selectedShippingCode}
          isRangeDate
          fontSize="fs22"
          textAlign="center"
        />
      )}
      <div className="confirmation-order-details-wrapper">
        <ConfirmationItemDisplay title={labels.orderNumber}>
          {isGuest ? (
            <Anchor
              underline
              to={`${trackOrder.link}&orderId=${orderNumber}&emailAddress=${encryptedEmailAddress}`}
              asPath={`${trackOrder.path}/${orderNumber}/${encryptedEmailAddress}`}
            >
              {orderNumber}
            </Anchor>
          ) : (
            <Anchor
              underline
              to={`${orderPage.link}&orderId=${orderNumber}`}
              asPath={`${orderPage.path}/${orderNumber}`}
            >
              {orderNumber}
            </Anchor>
          )}
        </ConfirmationItemDisplay>
        <ConfirmationItemDisplay title={labels.orderDate}>
          {orderDate.toLocaleDateString('en-US', CONFIRMATION_CONSTANTS.DATE_OPTIONS)}
        </ConfirmationItemDisplay>
        {orderTotal && (
          <ConfirmationItemDisplay title={labels.orderTotal} boldFont>
            <PriceCurrency price={orderTotal} />
          </ConfirmationItemDisplay>
        )}
      </div>
    </div>
  );
};

ConfirmationOrderNumberDisplay.propTypes = {
  className: PropTypes.string,
  center: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  isGuest: PropTypes.bool,
  selectedShippingCode: PropTypes.string,
};
ConfirmationOrderNumberDisplay.defaultProps = {
  className: '',
  center: null,
  isGuest: true,
  selectedShippingCode: '',
};

export default withStyles(ConfirmationOrderNumberDisplay, styles);
export { ConfirmationOrderNumberDisplay as ConfirmationOrderNumberDisplayVanilla };
