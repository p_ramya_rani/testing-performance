// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const ItemContainer = styled.View`
  display: flex;
  flex-direction: column;
`;

export const ItemContainerWrapper = styled.View``;

export default { ItemContainer, ItemContainerWrapper };
