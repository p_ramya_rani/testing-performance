// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { labelsHashValuesReplace } from '@tcp/core/src/components/features/CnC/LoyaltyBanner/util/utility';
import { SESSIONCONFIG_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { getCartOrderDetails } from '../../CartItemTile/container/CartItemTile.selectors';
import ConfirmationSelector from '../../Confirmation/container/Confirmation.selectors';

const getCurrentCountry = state => {
  return (
    (state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails.country) ||
    'US'
  );
};

const replaceLabelUtil = ({
  earnedPlaceCashValue,
  cashAmountOff,
  placeCashOffer,
  currency,
  placeCashMultiplier,
}) => {
  return {
    title: [
      {
        key: '#earnedPlaceCashValue#',
        value: `${currency}${earnedPlaceCashValue}`,
      },
    ],
    subTitle: [
      {
        key: '#cashAmountOff#',
        value: `${currency}${cashAmountOff * placeCashMultiplier}`,
      },
      {
        key: '#placeCashOffer#',
        value: `${currency}${placeCashOffer}`,
      },
    ],
  };
};

/** The place cash amount to be rewarded to customer */
const getCashAmountOff = state => {
  return (
    (state.session &&
      state.session.siteDetails &&
      state.session.siteDetails.PLACECASH_EARN_AMOUNT_OFF) ||
    0
  );
};

/** Minimum worth of merchandise to be in cart */
const getPlaceCashOffer = state => {
  return (
    (state.session &&
      state.session.siteDetails &&
      state.session.siteDetails.PLACECASH_EARN_MIN_PURCHASE) ||
    0
  );
};

const getCurrencySymbol = state => {
  const currency = state.session && state.session.siteDetails && state.session.siteDetails.currency;
  // eslint-disable-next-line no-nested-ternary
  return currency ? (currency === 'USD' || currency === 'CAD' ? '$' : currency) : '$';
};

/** Place cash earned on transaction */
const getEarnedPlaceCashValue = state => {
  return (
    getCartOrderDetails(state).get('rewardsToBeEarned') ||
    ConfirmationSelector.getEarnedPlaceCashValue(state)
  );
};

const getAllLabels = state => state.Labels;

const getPlaceDetailsContentInfo = createSelector(
  getAllLabels,
  (arg, labelName) => labelName,
  (allLabels, labelName) => {
    const referred = getLabelValue(allLabels, 'referred', 'placeCashBanner', 'checkout', true);
    return referred && referred.length && referred.find(label => label.name === labelName);
  }
);

const getAirmilesBannerContentInfo = state => {
  const referred = getLabelValue(state.Labels, 'referred', 'airmilesBanner', 'global', true);
  return referred && referred.length && referred.find(label => label.name === 'air_miles_banner');
};

/** Flag indicated whether the place cash is enabled  */
const getIsNotInternationalShipping = currentCountry => {
  return currentCountry === 'US' || currentCountry === 'CA';
};

const getIsPlaceCashEnabled = state => {
  const currentCountry = getCurrentCountry(state);
  const earnedPlaceCashValue = getEarnedPlaceCashValue(state);
  return getIsNotInternationalShipping(currentCountry) && earnedPlaceCashValue > 0;
};

const getModuleX = state => state.CartPageReducer.get('moduleXContent');

const getCurrentPageName = (state, isOrderConfrimation) => {
  return isOrderConfrimation ? 'confirmation' : 'bag';
};

const getPlaceCashDetailBannerLabel = createSelector(
  getCurrentCountry,
  getCurrentPageName,
  (country, page) => {
    return `PlaceCash_Detail_${country}_${page}`;
  }
);

const getPlaceDetailsContent = createSelector(
  [getAllLabels, getPlaceCashDetailBannerLabel, getModuleX],
  (allLabels, labelName, moduleXContent) => {
    const referred = getLabelValue(allLabels, 'referred', 'placeCashBanner', 'checkout', true);
    const name = referred && referred.length && referred.find(label => label.name === labelName);
    const showDetailsContent = moduleXContent.find(moduleX => moduleX.name === name);
    return showDetailsContent && showDetailsContent.richText;
  }
);

const getPlaceCashMultiplier = state => {
  return (
    (state.session &&
      state.session.siteDetails &&
      !Number.isNaN(state.session.siteDetails.PLACECASH_BONUS_VOUCHER_MULTIPLIER) &&
      parseFloat(state.session.siteDetails.PLACECASH_BONUS_VOUCHER_MULTIPLIER)) ||
    1
  );
};

const getIsShowBanner = state => {
  return +(
    (state.session &&
      state.session.siteDetails &&
      state.session.siteDetails.PLACECASH_BONUS_VOUCHER_MULTIPLIER) ||
    0
  );
};

const getPlaceCashBannerLabels = createSelector(
  [
    getAllLabels,
    (state, isOrderConfirmation) => {
      return isOrderConfirmation;
    },
    getIsShowBanner,
    getCurrentCountry,
    getCurrencySymbol,
    getCashAmountOff,
    getEarnedPlaceCashValue,
    getPlaceCashOffer,
    getPlaceCashMultiplier,
    getPlaceDetailsContent,
  ],
  // eslint-disable-next-line max-params
  (
    allLabels,
    isOrderConfirmation,
    isEnabled,
    currentCountry,
    currency,
    cashAmountOff,
    earnedPlaceCashValue,
    placeCashOffer,
    placeCashMultiplier,
    placeCashDetailBannerLabel
    // eslint-disable-next-line max-params
  ) => {
    const labels = [
      'title',
      'subTitle',
      'tnc',
      'modalLink',
      'imgUrl',
      'detailModalTitle',
      'placeRewardLinkTitle',
      'placeRewardModelHeader',
      'placeRewardModelDesc',
      'SHOW_DETAILS_RICH_TEXT',
    ];
    const finalValue = {};

    if (isEnabled) {
      const currentPage = getCurrentPageName(isOrderConfirmation);
      const replaceLabelConfig = replaceLabelUtil({
        currency,
        cashAmountOff,
        earnedPlaceCashValue,
        placeCashOffer,
        placeCashMultiplier,
      });

      const labelKeys = [
        `lbl_placeCash_${currentCountry}_${currentPage}_title`,
        `lbl_placeCash_${currentCountry}_${currentPage}_subTitle`,
        `lbl_placeCash_${currentCountry}_${currentPage}_tnc`,
        `lbl_placeCash_${currentCountry}_${currentPage}_modalLink`,
        `lbl_placeCash_${currentCountry}_${currentPage}_imgUrl`,
        `lbl_placeCash_${currentCountry}_${currentPage}_detailModalTitle`,
        `lbl_placereward_link_title`,
        `lbl_placereward_model_header`,
        `lbl_placereward_model_description`,
      ];
      labelKeys.forEach((key, index) => {
        const labelKeyValue = getLabelValue(allLabels, key, 'placeCashBanner', 'checkout');
        const labelString = labelKeyValue === key ? '' : labelKeyValue;
        const labelConfig = replaceLabelConfig[labels[index]];

        finalValue[labels[index]] = labelConfig
          ? labelsHashValuesReplace(labelString, labelConfig)
          : labelString;
      });
      finalValue.SHOW_DETAILS_RICH_TEXT = placeCashDetailBannerLabel;
    } else {
      labels.forEach(key => {
        finalValue[key] = '';
      });
    }
    return finalValue;
  }
);

export default {
  getIsPlaceCashEnabled,
  getPlaceDetailsContentInfo,
  getAirmilesBannerContentInfo,
  getPlaceCashBannerLabels,
  getPlaceCashDetailBannerLabel,
  getCashAmountOff,
  getPlaceCashOffer,
  getCurrentCountry,
  getIsShowBanner,
};
