// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import PlaceCashBannerSelector from '../PlaceCashBanner.selectors';

describe('#PlaceCashBanner Selectors', () => {
  it('#getIsPlaceCashEnabled should return false if (country !US or !CA) and earnedReward <= 0 ', () => {
    const CartPageReducer = fromJS({
      orderDetails: {
        rewardsToBeEarned: 0,
      },
    });
    const state = {
      session: {
        siteDetails: {
          country: 'IN',
        },
      },
      CartPageReducer,
    };
    expect(PlaceCashBannerSelector.getIsPlaceCashEnabled(state)).toEqual(false);
  });

  it('#getIsPlaceCashEnabled should return true  if (country US or CA) and earnedReward > 0 ', () => {
    const CartPageReducer = fromJS({
      orderDetails: {
        rewardsToBeEarned: 30,
      },
    });
    const state = {
      session: {
        siteDetails: {
          country: 'US',
        },
      },
      CartPageReducer,
    };
    expect(PlaceCashBannerSelector.getIsPlaceCashEnabled(state)).toEqual(true);
  });

  it('#getPlaceDetailsContentId should return false', () => {
    const contentId = 0;
    const labelKey = 'PlaceCash_Detail_US_BAG';
    const state = {
      Labels: {
        checkout: {
          placeCashBanner: {
            referred: [
              {
                name: labelKey,
                contentId,
              },
            ],
          },
        },
      },
    };
    expect(PlaceCashBannerSelector.getPlaceDetailsContentInfo(state, labelKey)).toEqual({
      name: labelKey,
      contentId,
    });
  });

  it('#getPlaceCashBannerLabels should return default labels if isEnabled=false', () => {
    const finalValue = {
      title: '',
      subTitle: '',
      tnc: '',
      modalLink: '',
      imgUrl: '',
      detailModalTitle: '',
      placeRewardLinkTitle: '',
      placeRewardModelHeader: '',
      placeRewardModelDesc: '',
      SHOW_DETAILS_RICH_TEXT: '',
    };

    expect(
      PlaceCashBannerSelector.getPlaceCashBannerLabels.resultFunc(
        null,
        false,
        false,
        false,
        null,
        null,
        null,
        null,
        null,
        null
      )
    ).toEqual(finalValue);
  });

  it('#getPlaceCashBannerLabels should return default labels if isEnabled=true', () => {
    const finalValue = {
      detailModalTitle: 'Place Bucks Detail',
      imgUrl:
        'https://assets.theplace.com/image/upload/w_auto,f_auto,q_auto,dpr_2.0/ecom/assets/content/gym/us/place-bucks/place_bucks_new_green.jpg',
      modalLink: 'See Details',
      subTitle: 'Get $0  for every $0 you spend!',
      title: "YAY!, You'll Earn $30 in PLACE Bucks",
      tnc: 'Cannot be combined with any other offer. Terms & conditions apply.',
      placeRewardLinkTitle: 'Learn More',
      placeRewardModelHeader: 'Place reward',
      placeRewardModelDesc: 'Place reward desc',
      SHOW_DETAILS_RICH_TEXT: undefined,
    };

    const allLabels = {
      checkout: {
        placeCashBanner: {
          lbl_placeCash_US_bag_title: "YAY!, You'll Earn #earnedPlaceCashValue# in PLACE Bucks",
          lbl_placeCash_US_bag_subTitle:
            'Get #cashAmountOff#  for every #placeCashOffer# you spend!',
          lbl_placeCash_US_bag_tnc:
            'Cannot be combined with any other offer. Terms & conditions apply.',
          lbl_placeCash_US_bag_modalLink: 'See Details',
          lbl_placeCash_US_bag_imgUrl:
            'https://assets.theplace.com/image/upload/w_auto,f_auto,q_auto,dpr_2.0/ecom/assets/content/gym/us/place-bucks/place_bucks_new_green.jpg',
          lbl_placeCash_US_bag_detailModalTitle: 'Place Bucks Detail',
          lbl_placereward_link_title: 'Learn More',
          lbl_placereward_model_header: 'Place reward',
          lbl_placereward_model_description: 'Place reward desc',
        },
      },
    };

    expect(
      PlaceCashBannerSelector.getPlaceCashBannerLabels.resultFunc(
        allLabels,
        false,
        true,
        'US',
        '$',
        0,
        30,
        0,
        1,
        undefined
      )
    ).toEqual(finalValue);
  });
});
