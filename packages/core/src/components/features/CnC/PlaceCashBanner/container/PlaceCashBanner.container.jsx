// 9fbef606107a605d69c0edbcd8029e5d
import { connect } from 'react-redux';
import PlaceCashSelector from './PlaceCashBanner.selectors';
import PlaceCashBanner from '../views/PlaceCashBanner.view';
import BAG_PAGE_ACTIONS from '../../BagPage/container/BagPage.actions';

export const mapDispatchToProps = dispatch => {
  return {
    fetchNeedHelpContent: contentIds => {
      dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
    },
  };
};

export const mapStateToProps = (state, ownProps) => {
  const isEnabled = PlaceCashSelector.getIsPlaceCashEnabled(state);
  const isShowBanner = PlaceCashSelector.getIsShowBanner(state);
  const { isOrderConfirmation, className } = ownProps;
  const formattedPlaceCashLabels = PlaceCashSelector.getPlaceCashBannerLabels(
    state,
    isOrderConfirmation
  );
  const placeCashBagContentId = PlaceCashSelector.getPlaceDetailsContentInfo(
    state,
    PlaceCashSelector.getPlaceCashDetailBannerLabel(state)
  );

  return {
    labels: formattedPlaceCashLabels,
    isEnabled,
    className,
    isShowBanner,
    placeCashBagContentId,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlaceCashBanner);
