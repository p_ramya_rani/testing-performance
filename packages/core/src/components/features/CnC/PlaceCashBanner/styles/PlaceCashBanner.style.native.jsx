// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const PlaceCashContainer = styled.View`
  position: relative;
`;
const PlaceCashTextWrapper = styled.View`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
`;
const PlaceCashTncContainer = styled.View``;

const RichTextContainer = styled.View`
  width: 100%;
  min-height: 560px;
`;

const SkeletonWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.MED};
  width: 200px;
`;

const PlaceRewardRecommendation = styled.View`
  background-color: ${props =>
    props.children[0].props.isPlaceReward
      ? props.theme.colors.WHITE
      : props.theme.colors.REWARDS.PLACE_CASH_BACKGROUND};
  border-top-left-radius: 14px;
  border-top-right-radius: 14px;
  height: 145px;
  padding: 5px 30px 10px 30px;
`;

export {
  PlaceCashContainer,
  PlaceCashTextWrapper,
  PlaceCashTncContainer,
  RichTextContainer,
  SkeletonWrapper,
  PlaceRewardRecommendation,
};
