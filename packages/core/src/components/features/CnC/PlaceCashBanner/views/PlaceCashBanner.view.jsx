// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../common/hoc/withStyles';
import styles from '../styles/PlaceCashBanner.style';
import { BodyCopy, Row, Col, Anchor, DamImage } from '../../../../common/atoms';
import PlaceCashDetailsModal from './PlaceCashDetails.modal.view';
import ProgressBarContainer from '../../common/organism/ProgressBar/container/Progressbar.container';
import { propTypes } from '../../../../common/molecules/StoreAddressTile/views/prop-types';
/**
 * PlaceCashBanner Component
 * @description Display User's place cash value earned
 * @param {*} label
 * @param {Boolean} isEnabled
 * @returns {JSX}
 */

class PlaceCashBanner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isPlaceCasModalOpen: false,
    };
  }

  leftProgressBarRender = (labels, isLeftProgressBar, isPlaceReward, className, newBag) => {
    const { isPlaceCasModalOpen } = this.state;
    return (
      <div className={`${className} ${newBag ? 'rounded-container' : ''}`}>
        <div className="place-cash">
          <Row className="place-cash__container">
            <Col
              key="placeCashBanner"
              className="place-cash__row"
              colSize={{ small: 6, medium: 8, large: 12 }}
              textAlign="center"
            >
              <div className="place-cash__text-wrapper">
                <ProgressBarContainer
                  className="variable-width"
                  isLeftProgressBar
                  isPlaceReward={isPlaceReward}
                />
                <div className="place-cash__tnc-container">
                  {!isPlaceReward && (
                    <BodyCopy
                      component="span"
                      fontFamily="primary"
                      textAlign="center"
                      className="place-cash__tnc-text"
                      data-locator="place-cash_info_text"
                      fontSize="fs10"
                    >
                      {labels.tnc}
                    </BodyCopy>
                  )}
                  <Anchor
                    fontSizeVariation="medium"
                    underline
                    href="#"
                    anchorVariation="primary"
                    fontFamily="primary"
                    title={isPlaceReward ? labels.placeRewardLinkTitle : labels.modalLink}
                    dataLocator="detailslink"
                    className="place-cash__modalLink"
                    fontSize="fs10"
                    onClick={(e) => this.toggleShowDetailModal(e)}
                  >
                    {isPlaceReward ? labels.placeRewardLinkTitle : labels.modalLink}
                  </Anchor>
                </div>
              </div>
              <PlaceCashDetailsModal
                labels={labels}
                openState={isPlaceCasModalOpen}
                isPlaceReward={isPlaceReward}
                onRequestClose={() => {
                  this.setState({
                    isPlaceCasModalOpen: false,
                  });
                }}
                heading={isPlaceReward ? labels.placeRewardModelHeader : labels.detailModalTitle}
              />
            </Col>
          </Row>
        </div>
      </div>
    );
  };

  toggleShowDetailModal = (e) => {
    e.preventDefault();
    const { isPlaceCasModalOpen } = this.state;
    const { labels, placeCashBagContentId, fetchNeedHelpContent, isPlaceReward } = this.props;
    if (
      (!isPlaceReward && !labels.SHOW_DETAILS_RICH_TEXT) ||
      (isPlaceReward && !labels.placeRewardModelDesc)
    ) {
      fetchNeedHelpContent([placeCashBagContentId]);
    }
    this.setState({
      isPlaceCasModalOpen: !isPlaceCasModalOpen,
    });
  };

  render() {
    const {
      labels,
      className,
      isEnabled,
      isOrderConfirmation,
      isShowBanner,
      isLeftProgressBar,
      isPlaceReward,
      newBag,
    } = this.props;
    const { isPlaceCasModalOpen } = this.state;
    const titleFonts = isOrderConfirmation ? ['fs16', 'fs16', 'fs32'] : ['fs18', 'fs13', 'fs18'];
    const subTitleFonts = isOrderConfirmation ? ['fs14', 'fs14', 'fs26'] : ['fs14', 'fs10', 'fs16'];
    const showProgressBag = isShowBanner && !isOrderConfirmation;

    return isLeftProgressBar ? (
      this.leftProgressBarRender(labels, isLeftProgressBar, isPlaceReward, className, newBag)
    ) : (
      <div
        className={`${className} ${
          newBag
            ? 'rounded-container elem-pb-SM elem-pt-SM elem-pl-SM elem-pr-SM elem-mb-LRG elem-mt-LRG'
            : ''
        }`}
      >
        {showProgressBag || isEnabled ? (
          <>
            <div className="place-cash">
              <Row className="place-cash__container">
                <Col
                  key="placeCashBanner"
                  className="place-cash__row"
                  colSize={{ small: 6, medium: 8, large: 12 }}
                  textAlign="center"
                >
                  {/* Fix Me: with given zeplin assets transforms are not working properly
                  need correct zeplin assets : mod_placeBug_bag_d */}
                  <DamImage
                    aria-hidden="true"
                    className="place-cash__img"
                    width="100%"
                    imgData={{
                      url: labels.imgUrl,
                      alt: labels.imgUrl,
                    }}
                  />
                  <div className="place-cash__text-wrapper">
                    {isEnabled && (
                      <BodyCopy
                        component="h3"
                        fontSize={titleFonts}
                        fontFamily="primary"
                        fontWeight="bold"
                        textAlign="center"
                        className="place-cash__title"
                        data-locator="place-cash_info_text"
                      >
                        {labels.title}
                      </BodyCopy>
                    )}
                    <BodyCopy
                      component="h4"
                      fontSize={subTitleFonts}
                      fontFamily="primary"
                      textAlign="center"
                      className="place-cash__subTitle"
                      data-locator="place-cash_info_text"
                    >
                      {labels.subTitle}
                    </BodyCopy>
                    {!isOrderConfirmation && (
                      <div className="place-cash__tnc-container">
                        <BodyCopy
                          component="span"
                          fontFamily="primary"
                          textAlign="center"
                          className="place-cash__tnc-text"
                          data-locator="place-cash_info_text"
                          fontSize={['fs8', 'fs8', 'fs14']}
                        >
                          {labels.tnc}
                        </BodyCopy>
                        <Anchor
                          fontSizeVariation="medium"
                          underline
                          href="#"
                          anchorVariation="primary"
                          fontFamily="primary"
                          title={labels.label4}
                          dataLocator="detailslink"
                          className="place-cash__modalLink"
                          fontSize={['fs8', 'fs8', 'fs14']}
                          onClick={(e) => this.toggleShowDetailModal(e)}
                        >
                          {labels.modalLink}
                        </Anchor>
                      </div>
                    )}
                  </div>
                  <PlaceCashDetailsModal
                    labels={labels}
                    openState={isPlaceCasModalOpen}
                    // eslint-disable-next-line sonarjs/no-identical-functions
                    onRequestClose={() => {
                      this.setState({
                        isPlaceCasModalOpen: false,
                      });
                    }}
                    heading={labels.detailModalTitle}
                  />
                </Col>
              </Row>
            </div>
            {showProgressBag && <ProgressBarContainer className="variable-width" newBag={newBag} />}
          </>
        ) : null}
      </div>
    );
  }
}

PlaceCashBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  isEnabled: PropTypes.bool.isRequired,
  fetchNeedHelpContent: PropTypes.func.isRequired,
  placeCashBagContentId: PropTypes.shape({}).isRequired,
  isOrderConfirmation: PropTypes.bool.isRequired,
  isShowBanner: PropTypes.bool.isRequired,
  isLeftProgressBar: propTypes.bool,
  isPlaceReward: propTypes.bool,
  newBag: PropTypes.bool,
};

PlaceCashBanner.defaultProps = {
  className: '',
  isLeftProgressBar: false,
  isPlaceReward: false,
  newBag: false,
};

export default withStyles(PlaceCashBanner, styles);
export { PlaceCashBanner as PlaceCashBannerVanilla };
