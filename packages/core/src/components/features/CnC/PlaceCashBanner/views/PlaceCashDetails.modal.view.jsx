// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import Modal from '../../../../common/molecules/Modal';
import { RichText } from '../../../../common/atoms';

class PlaceCashDetailsModal extends React.PureComponent {
  render() {
    const {
      openState,
      onRequestClose,
      labels,
      heading,
      additionalClassNameModal,
      isPlaceReward,
    } = this.props;
    return (
      <Modal
        isOpen={openState}
        onRequestClose={onRequestClose}
        overlayClassName="TCPModal__Overlay"
        className="TCPModal__Content"
        maxWidth="500px"
        minHeight="90%"
        heightConfig={{ height: '90%' }}
        closeIconDataLocator="detailmodalcrossicon"
        heading={heading}
        fixedWidth
        customWrapperClassName={additionalClassNameModal}
      >
        {(!isPlaceReward && !labels.SHOW_DETAILS_RICH_TEXT) ||
        (isPlaceReward && !labels.placeRewardModelDesc) ? (
          <>
            <div className="elem-mt-LRG" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <div className="elem-mb-LRG" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
            <LoaderSkelton height="30px" width="80%" />
          </>
        ) : (
          <RichText
            richTextHtml={
              isPlaceReward ? labels.placeRewardModelDesc : labels.SHOW_DETAILS_RICH_TEXT
            }
          />
        )}
      </Modal>
    );
  }
}

PlaceCashDetailsModal.propTypes = {
  openState: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  heading: PropTypes.string.isRequired,
  isPlaceReward: PropTypes.bool,
  additionalClassNameModal: PropTypes.string.isRequired,
};

PlaceCashDetailsModal.defaultProps = {
  isPlaceReward: false,
};
export default PlaceCashDetailsModal;
