// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { ImageBackground } from 'react-native';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import withStyles from '../../../../common/hoc/withStyles';
import {
  PlaceCashContainer,
  PlaceCashTextWrapper,
  PlaceCashTncContainer,
  PlaceRewardRecommendation,
} from '../styles/PlaceCashBanner.style.native';
import PlaceCashDetailsModal from './PlaceCashDetails.modal.view.native';
import ProgressBarContainer from '../../common/organism/ProgressBar/container/Progressbar.container';

/**
 * PlaceCashBanner Component
 * @description Display User's place cash value earned
 * @param {*} label
 * @param {Boolean} isEnabled
 * @returns {JSX}
 */

class PlaceCashBanner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isPlaceCasModalOpen: false,
    };
  }

  toggleShowDetailModal = e => {
    e.preventDefault();
    const { isPlaceCasModalOpen } = this.state;
    const { labels, placeCashBagContentId, fetchNeedHelpContent, isPlaceReward } = this.props;
    if (
      (!isPlaceReward && !labels.SHOW_DETAILS_RICH_TEXT) ||
      (isPlaceReward && !labels.placeRewardModelDesc)
    ) {
      fetchNeedHelpContent([placeCashBagContentId]);
    }
    this.setState({
      isPlaceCasModalOpen: !isPlaceCasModalOpen,
    });
  };

  leftProgressBarRender = (labels, isLeftProgressBar, isPlaceReward) => {
    const { isPlaceCasModalOpen } = this.state;
    return (
      <PlaceRewardRecommendation>
        <ProgressBarContainer isLeftProgressBar isPlaceReward={isPlaceReward} />
        <PlaceCashTextWrapper>
          <PlaceCashTncContainer>
            {!isPlaceReward && <BodyCopy text={labels.tnc} fontSize="fs10" />}
            <Anchor
              fontSizeVariation="small"
              underline
              noLink
              to=""
              fontSize="fs10"
              anchorVariation="primary"
              text={isPlaceReward ? labels.placeRewardLinkTitle : labels.modalLink}
              dataLocator="detailslink"
              onPress={e => this.toggleShowDetailModal(e)}
            />
          </PlaceCashTncContainer>
        </PlaceCashTextWrapper>
        <PlaceCashDetailsModal
          labels={labels}
          openState={isPlaceCasModalOpen}
          isPlaceReward={isPlaceReward}
          onRequestClose={() => {
            this.setState({
              isPlaceCasModalOpen: false,
            });
          }}
          heading={isPlaceReward ? labels.placeRewardModelHeader : labels.detailModalTitle}
        />
      </PlaceRewardRecommendation>
    );
  };

  render() {
    const {
      labels,
      isEnabled,
      isOrderConfirmation,
      isShowBanner,
      isLeftProgressBar,
      isPlaceReward,
    } = this.props;
    const { isPlaceCasModalOpen } = this.state;
    const showProgressBag = isShowBanner && !isOrderConfirmation;

    const imgStyle = {
      height: 100,
      width: '100%',
      position: 'relative',
      top: 2,
      left: 2,
    };
    return isLeftProgressBar ? (
      this.leftProgressBarRender(labels, isLeftProgressBar, isPlaceReward)
    ) : (
      <PlaceCashContainer>
        <ImageBackground source={{ uri: labels.imgUrl }} style={imgStyle}>
          <PlaceCashTextWrapper>
            {isEnabled && (
              <BodyCopy
                fontSize={isOrderConfirmation ? 'fs16' : 'fs18'}
                text={labels.title}
                textAlign="center"
                fontWeight="bold"
              />
            )}
            <BodyCopy fontSize={isEnabled ? 'fs14' : 'fs16'} text={labels.subTitle} />
            {!isOrderConfirmation && (
              <PlaceCashTncContainer>
                <BodyCopy text={labels.tnc} fontSize="fs8" />
                <Anchor
                  fontSizeVariation="small"
                  underline
                  noLink
                  to=""
                  fontSize="fs8"
                  anchorVariation="primary"
                  fontFamily="primary"
                  text={labels.modalLink}
                  dataLocator="detailslink"
                  onPress={e => this.toggleShowDetailModal(e)}
                />
              </PlaceCashTncContainer>
            )}
          </PlaceCashTextWrapper>
          <PlaceCashDetailsModal
            labels={labels}
            openState={isPlaceCasModalOpen}
            // eslint-disable-next-line sonarjs/no-identical-functions
            onRequestClose={() => {
              this.setState({
                isPlaceCasModalOpen: false,
              });
            }}
            heading={labels.detailModalTitle}
          />
        </ImageBackground>
        {showProgressBag ? <ProgressBarContainer /> : null}
      </PlaceCashContainer>
    );
  }
}

PlaceCashBanner.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  isEnabled: PropTypes.isRequired,
  fetchNeedHelpContent: PropTypes.func.isRequired,
  placeCashBagContentId: PropTypes.shape({}).isRequired,
  isOrderConfirmation: PropTypes.bool.isRequired,
  isShowBanner: PropTypes.bool.isRequired,
  isLeftProgressBar: PropTypes.bool,
  isPlaceReward: PropTypes.bool,
};

PlaceCashBanner.defaultProps = {
  isLeftProgressBar: false,
  isPlaceReward: false,
};

export default withStyles(PlaceCashBanner);
export { PlaceCashBanner as PlaceCashBannerVanilla };
