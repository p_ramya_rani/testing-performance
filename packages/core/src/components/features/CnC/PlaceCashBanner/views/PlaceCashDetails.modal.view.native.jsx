// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import ModalNative from '@tcp/core/src/components/common/molecules/Modal';
import { RichText } from '../../../../common/atoms';
import { RichTextContainer, SkeletonWrapper } from '../styles/PlaceCashBanner.style.native';
import GenericSkeleton from '../../../../common/molecules/GenericSkeleton/GenericSkeleton.view.native';

class PlaceCashDetailsModal extends React.PureComponent {
  render() {
    const { openState, onRequestClose, labels, heading, isPlaceReward } = this.props;

    return (
      <ModalNative
        isOpen={openState}
        onRequestClose={onRequestClose}
        heading={heading}
        headingFontFamily="secondary"
        fontSize="fs16"
      >
        {(!isPlaceReward && !labels.SHOW_DETAILS_RICH_TEXT) ||
        (isPlaceReward && !labels.placeRewardModelDesc) ? (
          <SkeletonWrapper>
            <GenericSkeleton />
          </SkeletonWrapper>
        ) : (
          <RichTextContainer>
            <RichText
              source={{
                html: isPlaceReward ? labels.placeRewardModelDesc : labels.SHOW_DETAILS_RICH_TEXT,
              }}
            />
          </RichTextContainer>
        )}
      </ModalNative>
    );
  }
}

PlaceCashDetailsModal.propTypes = {
  openState: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  heading: PropTypes.string.isRequired,
  isPlaceReward: PropTypes.bool,
};

PlaceCashDetailsModal.defaultProps = {
  isPlaceReward: false,
};

export default PlaceCashDetailsModal;
