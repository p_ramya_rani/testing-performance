// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import { PropTypes } from 'prop-types';
import {
  LineStyle,
  LoyaltySectionWrapper,
  FooterSectionWrapper,
} from '../styles/LoyaltyBannerSection.style.native';
import {
  PointsValueText,
  PointsToNextReward,
  SectionSymbol,
} from '../../GuestMprPlccSection/styles/GuestMprPlccSection.style.native';
import mobileHashValues from '../../../util/utilityNative';
import { renderLoyaltyLabels, getPageCategory } from '../../../util/utilityCommon';
import GuestMprPlccSection from '../../GuestMprPlccSection';
import LoyaltyFooterSection from '../../LoyaltyFooterSection';

const shouldShowSubtotal = ({
  estimatedSubtotal,
  thresholdValue,
  isPlcc,
  isReviewPage,
  isConfirmationPage,
  isAddedToBagPage,
  isProductDetailView,
}) => {
  return (
    estimatedSubtotal > thresholdValue &&
    !isPlcc &&
    !isReviewPage &&
    !isConfirmationPage &&
    !isAddedToBagPage &&
    !isProductDetailView
  );
};
const LoyaltyBannerSection = props => {
  const {
    labels,
    currentSubtotal,
    estimatedSubtotal,
    thresholdValue,
    earnedReward,
    isGuest,
    isPlcc,
    estimatedRewardsVal,
    pointsToNextReward,
    getCurrencySymbol,
    pageCategory,
    footerLabels,
    openApplyNowModal,
    navigation,
    closeAddedToBagModal,
    isUserLoggedIn,
    resetTimerStatus,
  } = props;
  let showSubtotal = false;
  let headingLabel = '';
  let remainingPlcc = '';
  let subHeadingLabel = '';
  let descriptionLabel = '';
  const earnedRewardAvailable = !!earnedReward;

  const pageCategoryArr = getPageCategory(pageCategory);
  const {
    isReviewPage,
    isConfirmationPage,
    isAddedToBagPage,
    isProductDetailView,
  } = pageCategoryArr;

  const isSignInCtaBanner = pageCategory === 'signin-cta-banner';

  const pageChecksObj = {
    isGuest,
    isPlcc,
    pageCategoryArr,
    earnedRewardAvailable,
    isSignInCtaBanner,
  };

  /* istanbul ignore else */
  if (
    shouldShowSubtotal({
      estimatedSubtotal,
      thresholdValue,
      isPlcc,
      isReviewPage,
      isConfirmationPage,
      isAddedToBagPage,
      isProductDetailView,
    })
  ) {
    showSubtotal = true;
  }

  const LoyaltyLabels = renderLoyaltyLabels(
    labels,
    estimatedRewardsVal,
    earnedReward,
    isGuest,
    isPlcc,
    isReviewPage,
    isConfirmationPage,
    isAddedToBagPage,
    isProductDetailView
  );

  let rewardsVal = '';
  if (
    LoyaltyLabels.rewardPointsValueFn &&
    (LoyaltyLabels.rewardPointsValueFn >= 0 || LoyaltyLabels.rewardPointsValueFn.length > 0)
  ) {
    rewardsVal = earnedRewardAvailable
      ? LoyaltyLabels.rewardPointsValueFn.slice(0, -6)
      : LoyaltyLabels.rewardPointsValueFn;
  }
  const utilArrHeading = [
    {
      key: '#estimatedRewardsVal# ',
      value: <PointsValueText pageChecksObj={pageChecksObj}>{`${rewardsVal} `}</PointsValueText>,
    },
    {
      key: '#br# ',
      value: '\n',
    },
    {
      key: '#tagOpen# ',
      value: 'MyPlaceRewards',
    },
  ];
  headingLabel = LoyaltyLabels.headingLabelValFn
    ? mobileHashValues(LoyaltyLabels.headingLabelValFn, utilArrHeading)
    : false;

  const utilArrSubHeading = [
    {
      key: '#sectionSymbol# ',
      value: (
        <SectionSymbol pageChecksObj={pageChecksObj}>{`${labels.sectionSymbol} `}</SectionSymbol>
      ),
    },
  ];
  subHeadingLabel = LoyaltyLabels.subHeadingLabelFn
    ? mobileHashValues(LoyaltyLabels.subHeadingLabelFn, utilArrSubHeading)
    : false;
  const utilArrDescription = [
    {
      key: '#br# ',
      value: '\n',
    },
  ];
  descriptionLabel = LoyaltyLabels.descriptionLabelFn
    ? mobileHashValues(LoyaltyLabels.descriptionLabelFn, utilArrDescription)
    : false;
  const utilArrNextReward = [
    {
      key: '#pointsToNextReward# ',
      value: (
        <PointsToNextReward pageChecksObj={pageChecksObj}>
          {`${pointsToNextReward} `}
        </PointsToNextReward>
      ),
    },
  ];
  remainingPlcc = LoyaltyLabels.remainingPlccValFn
    ? mobileHashValues(LoyaltyLabels.remainingPlccValFn, utilArrNextReward)
    : false;

  return (
    <>
      <LineStyle isPlcc={isPlcc} pageCategory={pageCategory} />
      <LoyaltySectionWrapper pageCategory={pageCategory}>
        <GuestMprPlccSection
          labels={labels}
          headingLabel={headingLabel}
          subHeadingLabel={subHeadingLabel}
          descriptionLabel={descriptionLabel}
          remainingPlcc={remainingPlcc}
          showSubtotal={showSubtotal}
          getCurrencySymbol={getCurrencySymbol}
          currentSubtotal={currentSubtotal}
          estimatedSubtotal={estimatedSubtotal}
          pageChecksObj={pageChecksObj}
        />
        <FooterSectionWrapper pageCategory={pageCategory}>
          <View className="footer">
            <LoyaltyFooterSection
              isUserLoggedIn={isUserLoggedIn}
              footerLabels={footerLabels}
              openApplyNowModal={openApplyNowModal}
              navigation={navigation}
              closeAddedToBagModal={closeAddedToBagModal}
              resetTimerStatus={resetTimerStatus}
              pageCategory={pageCategory}
              isPlcc={isPlcc}
              isGuest={isGuest}
            />
          </View>
        </FooterSectionWrapper>
      </LoyaltySectionWrapper>
      <LineStyle isPlcc={isPlcc} pageCategory={pageCategory} />
    </>
  );
};

LoyaltyBannerSection.propTypes = {
  labels: PropTypes.shape.isRequired,
  footerLabels: PropTypes.shape.isRequired,
  currentSubtotal: PropTypes.number,
  estimatedSubtotal: PropTypes.number,
  thresholdValue: PropTypes.number,
  earnedReward: PropTypes.number,
  isPlcc: PropTypes.bool,
  isGuest: PropTypes.bool,
  getCurrencySymbol: PropTypes.string,
  estimatedRewardsVal: PropTypes.string,
  pointsToNextReward: PropTypes.number,
  pageCategory: PropTypes.string,
  openApplyNowModal: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}),
  closeAddedToBagModal: PropTypes.func.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  resetTimerStatus: PropTypes.func,
};

LoyaltyBannerSection.defaultProps = {
  currentSubtotal: 0,
  estimatedSubtotal: 0,
  thresholdValue: 0,
  earnedReward: 0,
  isPlcc: false,
  isGuest: false,
  getCurrencySymbol: '',
  estimatedRewardsVal: '',
  pointsToNextReward: 0,
  pageCategory: '',
  resetTimerStatus: () => {},
  navigation: null,
};

export default LoyaltyBannerSection;
