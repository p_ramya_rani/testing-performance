// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import { getIconPath } from '@tcp/core/src/utils/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import Styles from '../styles/GuestMprPlccSection.style';
import { BodyCopy, Row, Col, Image, Anchor } from '../../../../../../common/atoms';

const openModalApplyNowModal = (openApplyNowModal, pageCategory, step = 1) => {
  if (!openApplyNowModal) return null;
  if (step === 1) return openApplyNowModal({ isModalOpen: true, status: null, pageCategory });
  return openApplyNowModal({
    isModalOpen: false,
    isPLCCModalOpen: true,
    status: null,
    pageCategory,
  });
};

const renderSubtotalStripe = (props) => {
  const {
    closeAddedToBagModal,
    openApplyNowModal,
    labels,
    estimatedSubtotal,
    showSubtotal,
    isFancyBanner,
    pageCategory,
  } = props;
  return (
    showSubtotal &&
    isFancyBanner && (
      <Anchor
        className="subtotal-stripe"
        noLink
        handleLinkClick={(e) => {
          e.preventDefault();
          openModalApplyNowModal(openApplyNowModal, pageCategory, 2);
          closeAddedToBagModal();
        }}
      >
        <BodyCopy
          component="span"
          color="white"
          fontFamily="secondary"
          fontWeight="semibold"
          fontSize="fs12"
        >
          {labels.estimatedSubtotalshort}
        </BodyCopy>
        {estimatedSubtotal && (
          <BodyCopy
            component="span"
            color="white"
            fontFamily="secondary"
            fontWeight="extrabold"
            fontSize="fs12"
          >
            <PriceCurrency price={estimatedSubtotal} />
          </BodyCopy>
        )}
      </Anchor>
    )
  );
};

const GuestMprPlccSection = (props) => {
  const {
    className,
    headingLabel,
    subHeadingLabel,
    labels,
    showSubtotal,
    currentSubtotal,
    estimatedSubtotal,
    descriptionLabel,
    remainingPlcc,
    isFancyBanner,
  } = props;
  return (
    <div className={`${className} body`}>
      {isFancyBanner && <Image className="loyalty-banner-cc" src={getIconPath('cc-plcc-icon')} />}
      {headingLabel && (
        <BodyCopy
          className="heading-val"
          color="text.primary"
          fontFamily="secondary"
          fontWeight="extrabold"
        >
          {headingLabel}
        </BodyCopy>
      )}
      {subHeadingLabel && (
        <BodyCopy
          className="subheading-val"
          color="text.primary"
          fontFamily="secondary"
          fontWeight="extrabold"
        >
          {subHeadingLabel}
        </BodyCopy>
      )}
      {descriptionLabel && (
        <BodyCopy
          className="description-val"
          color="text.primary"
          fontFamily="secondary"
          fontWeight="extrabold"
        >
          {descriptionLabel}
        </BodyCopy>
      )}

      {remainingPlcc && (
        <BodyCopy
          className="remaining-val"
          color="text.primary"
          fontFamily="secondary"
          fontWeight="extrabold"
        >
          {remainingPlcc}
        </BodyCopy>
      )}
      {showSubtotal && !isFancyBanner && (
        <div className="subtotal-section">
          <Row fullBleed className="subtotal-row">
            <Col colSize={{ large: 7, medium: 5, small: 4 }} className="current-subtotal-text-col">
              <BodyCopy
                className="current-subtotal-text"
                component="span"
                color="text.primary"
                fontFamily="secondary"
                fontWeight="extrabold"
              >
                {labels.currentSubtotal}
              </BodyCopy>
            </Col>
            {currentSubtotal && (
              <Col colSize={{ large: 5, medium: 3, small: 2 }} className="current-subtotal-val-col">
                <BodyCopy
                  className="current-subtotal-val"
                  component="span"
                  color="text.primary"
                  fontFamily="secondary"
                  fontWeight="semibold"
                >
                  <PriceCurrency price={currentSubtotal} />
                </BodyCopy>
              </Col>
            )}
          </Row>
          <Row fullBleed className="estimated-subtotal-row elem-pt-SM elem-pb-SM">
            <Col
              colSize={{ large: `${props.newMiniBag ? 10 : 7}`, medium: 5, small: 4 }}
              className="estimated-subtotal-text-col"
            >
              <BodyCopy
                className="estimated-subtotal-text"
                component="span"
                color="text.primary"
                fontFamily="secondary"
                fontWeight="extrabold"
              >
                {labels.estimatedSubtotal}
              </BodyCopy>
            </Col>
            {estimatedSubtotal && (
              <Col
                colSize={{ large: `${props.newMiniBag ? 2 : 5}`, medium: 3, small: 2 }}
                className="estimated-subtotal-val-col"
              >
                <BodyCopy
                  className="estimated-subtotal-val"
                  component="span"
                  color="text.primary"
                  fontFamily="secondary"
                  fontWeight="extrabold"
                >
                  <PriceCurrency price={estimatedSubtotal} />
                </BodyCopy>
              </Col>
            )}
          </Row>
        </div>
      )}
      {renderSubtotalStripe(props)}
    </div>
  );
};

GuestMprPlccSection.propTypes = {
  estimatedSubtotal: PropTypes.number,
  currentSubtotal: PropTypes.number,
  showSubtotal: PropTypes.bool,
  labels: PropTypes.shape.isRequired,
  className: PropTypes.string,
  headingLabel: PropTypes.string,
  subHeadingLabel: PropTypes.string,
  descriptionLabel: PropTypes.string,
  remainingPlcc: PropTypes.number,
  isFancyBanner: PropTypes.bool,
};

GuestMprPlccSection.defaultProps = {
  className: '',
  estimatedSubtotal: 0,
  currentSubtotal: 0,
  showSubtotal: false,
  headingLabel: '',
  subHeadingLabel: '',
  descriptionLabel: '',
  remainingPlcc: 0,
  isFancyBanner: false,
};

export default withStyles(GuestMprPlccSection, Styles);
export { GuestMprPlccSection as GuestMprPlccSectionVanilla };
