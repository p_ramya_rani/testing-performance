// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import withStyles from '../../../../../../common/hoc/withStyles';
import {
  Styles,
  SubTotalLine,
  HeadingLabel,
  SubHeadingLabel,
  DescriptionLabel,
  RemainingPlccLabel,
  ShowSubTotalWrapper,
  CurrentTotalWrapper,
  SubTotalLabel,
  SubTotalValue,
  EstimatedSubTotalWrapper,
  EstimatedSubTotalLabel,
  GuestMprPlccSectionWrapper,
  EstimatedSubTotalValue,
  PLCCSectionWrapper,
} from '../styles/GuestMprPlccSection.style.native';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

const GuestMprPlccSection = props => {
  const {
    labels,
    headingLabel,
    subHeadingLabel,
    descriptionLabel,
    remainingPlcc,
    showSubtotal,
    getCurrencySymbol,
    currentSubtotal,
    estimatedSubtotal,
    pageChecksObj,
  } = props;

  const { isSignInCtaBanner } = pageChecksObj;
  return (
    <GuestMprPlccSectionWrapper>
      {headingLabel && (
        <HeadingLabel pageChecksObj={pageChecksObj}>
          <BodyCopy fontFamily={['secondary']} fontWeight="extrabold" text={headingLabel} />
        </HeadingLabel>
      )}
      {!isSignInCtaBanner && (
        <PLCCSectionWrapper>
          {subHeadingLabel && (
            <SubHeadingLabel pageChecksObj={pageChecksObj}>
              <BodyCopy fontFamily={['secondary']} fontWeight="extrabold" text={subHeadingLabel} />
            </SubHeadingLabel>
          )}
          {descriptionLabel && (
            <DescriptionLabel pageChecksObj={pageChecksObj}>
              <BodyCopy
                fontFamily={['secondary']}
                fontWeight="extrabold"
                color="black"
                text={descriptionLabel}
              />
            </DescriptionLabel>
          )}
          {remainingPlcc && (
            <RemainingPlccLabel pageChecksObj={pageChecksObj}>
              <BodyCopy
                fontFamily={['secondary']}
                fontWeight="extrabold"
                color="black"
                text={remainingPlcc}
              />
            </RemainingPlccLabel>
          )}
          {showSubtotal && (
            <ShowSubTotalWrapper>
              <SubTotalLine />
              <CurrentTotalWrapper>
                <SubTotalLabel>
                  <BodyCopy
                    fontFamily={['secondary']}
                    fontWeight="semibold"
                    fontSize="fs12"
                    text={labels.currentSubtotal}
                  />
                </SubTotalLabel>
                <SubTotalValue>
                  <BodyCopy
                    fontFamily={['secondary']}
                    fontWeight="semibold"
                    fontSize="fs14"
                    text={getCurrencySymbol}
                  />
                  <BodyCopy
                    fontFamily={['secondary']}
                    fontWeight="semibold"
                    fontSize="fs14"
                    text={currentSubtotal.toFixed(2)}
                  />
                </SubTotalValue>
              </CurrentTotalWrapper>
              <EstimatedSubTotalWrapper>
                <EstimatedSubTotalLabel>
                  <BodyCopy
                    fontFamily={['secondary']}
                    fontWeight="semibold"
                    fontSize="fs12"
                    text={labels.estimatedSubtotal}
                  />
                </EstimatedSubTotalLabel>
                <EstimatedSubTotalValue>
                  <BodyCopy
                    fontFamily={['secondary']}
                    fontWeight="extrabold"
                    fontSize="fs16"
                    text={getCurrencySymbol}
                  />
                  <BodyCopy
                    fontFamily={['secondary']}
                    fontWeight="extrabold"
                    fontSize="fs16"
                    text={estimatedSubtotal.toFixed(2)}
                  />
                </EstimatedSubTotalValue>
              </EstimatedSubTotalWrapper>
              <SubTotalLine />
            </ShowSubTotalWrapper>
          )}
        </PLCCSectionWrapper>
      )}
    </GuestMprPlccSectionWrapper>
  );
};

GuestMprPlccSection.propTypes = {
  headingLabel: PropTypes.string,
  labels: PropTypes.shape.isRequired,
  subHeadingLabel: PropTypes.string,
  descriptionLabel: PropTypes.string,
  remainingPlcc: PropTypes.string,
  showSubtotal: PropTypes.bool,
  pageChecksObj: PropTypes.shape({}),
  getCurrencySymbol: PropTypes.string,
  currentSubtotal: PropTypes.number,
  estimatedSubtotal: PropTypes.string,
};

GuestMprPlccSection.defaultProps = {
  headingLabel: '',
  subHeadingLabel: '',
  descriptionLabel: '',
  remainingPlcc: '',
  showSubtotal: false,
  pageChecksObj: {},
  getCurrencySymbol: '',
  currentSubtotal: 0,
  estimatedSubtotal: '',
};

export default withStyles(GuestMprPlccSection, Styles);
export { GuestMprPlccSection as GuestMprPlccSectionVanilla };
