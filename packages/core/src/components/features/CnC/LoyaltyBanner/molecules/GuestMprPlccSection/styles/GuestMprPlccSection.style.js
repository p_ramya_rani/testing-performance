// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import CHECKOUT_STAGES, {
  ADDED_TO_BAG_PAGE,
  PRODUCT_DETAIL_PAGE,
} from '../../../../../../../../../web/src/components/App.constants';

const plccMpr = (props) =>
  props.isPlcc ? props.theme.colorPalette.userTheme.plcc : props.theme.colorPalette.userTheme.mpr;

const alignCenter = () => `
    text-align: center;
`;
const paddingTopZero = () => `
    padding-top: 0;
`;
const paddingTopXxs = (props) => `
    padding-top: ${props.theme.spacing.ELEM_SPACING.XXS};
`;
const paddingTopXs = (props) => `
    padding-top: ${props.theme.spacing.ELEM_SPACING.XS};
`;
const paddingTopSm = (props) => `
    padding-top: ${props.theme.spacing.ELEM_SPACING.SM};
`;
const paddingTopMed = (props) => `
    padding-top: ${props.theme.spacing.ELEM_SPACING.MED};
`;
const fontSize10 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs10};
`;
const fontSize11 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs11};
`;
const fontSize12 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs12};
`;
const fontSize14 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs14};
`;
const fontSize16 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs16};
`;
const fontSize18 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs18};
`;
const fontSize20 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs20};
`;
const fontSize24 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs24};
`;
const fontSize30 = (props) => `
    font-size: ${props.theme.typography.fontSizes.fs30};
`;
const colorTheme = (props) => `
    color: ${plccMpr(props)};
`;

const Styles = css`
  .mpr-plcc-theme {
    ${(props) => colorTheme(props)}
    ${(props) =>
      props.isFancyBanner
        ? `color: ${props.theme.colors.WHITE}; font-size: 16px; font-weight: bold`
        : colorTheme(props)};
  }

  .heading-val {
    ${(props) => paddingTopSm(props)}
    ${alignCenter()}
    ${(props) => {
      if (props.isPlcc && !props.isFancyBanner) {
        return `${fontSize16(props)}${colorTheme(props)}`;
      }
      if (props.isFancyBanner) {
        return `${fontSize11(props)}color: ${
          props.theme.colors.WHITE
        };text-transform: uppercase; font-weight: normal`;
      }
      return fontSize12(props);
    }};

    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => (props.isPlcc ? `${fontSize14(props)}` : fontSize10(props))};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => paddingTopMed(props)}
      ${(props) => (props.isPlcc ? `${fontSize18(props)}` : fontSize16(props))};
      ${(props) => (props.isFancyBanner ? `${fontSize12(props)}` : '')};
    }
  }
  .subheading-val {
    ${alignCenter()}
    ${(props) => (props.isFancyBanner ? paddingTopZero() : paddingTopSm(props))}
    ${(props) => fontSize18(props)}
    ${(props) =>
      props.isFancyBanner
        ? `font-weight: bold; color:${
            props.theme.colors.WHITE
          }; font-family: TofinoWide;${fontSize30(props)}`
        : colorTheme(props)};

    .section-symbol {
      vertical-align: super;
      ${alignCenter}
      ${fontSize12}
    }

    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => fontSize14(props)}
      ${(props) => props.isFancyBanner && `${fontSize30(props)}`};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.isFancyBanner ? paddingTopZero() : paddingTopMed(props))}
      ${(props) => (props.isFancyBanner ? `${fontSize30(props)}` : fontSize20(props))};
    }
  }
  .description-val {
    ${alignCenter()}
    ${(props) =>
      props.isFancyBanner
        ? `${fontSize10(props)}color: ${
            props.theme.colors.WHITE
          }; font-weight:normal; text-transform: uppercase;${paddingTopZero()}`
        : `${paddingTopSm(props)}${fontSize12(props)}`}
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => fontSize10(props)}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.isFancyBanner ? paddingTopZero() : paddingTopMed(props))}
      ${(props) => (props.isFancyBanner ? fontSize10(props) : fontSize16(props))}
    }
  }
  .remaining-val {
    ${alignCenter()}
    ${(props) => paddingTopSm(props)}
    ${(props) => fontSize12(props)}
    ${(props) =>
      props.isFancyBanner
        ? `${fontSize10(props)}color: ${
            props.theme.colors.WHITE
          }; font-weight:normal; text-transform: uppercase;`
        : fontSize12(props)}
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => fontSize10(props)}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => paddingTopMed(props)}
      ${(props) => (props.isFancyBanner ? fontSize10(props) : fontSize16(props))}
    }
  }
  .subtotal-section {
    border-top: 1px solid ${(props) => props.theme.colorPalette.gray[300]};
    border-bottom: 1px solid ${(props) => props.theme.colorPalette.gray[300]};
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    ${(props) => paddingTopSm(props)}
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      ${(props) => paddingTopMed(props)}
    }
  }
  .current-subtotal-val-col,
  .estimated-subtotal-val-col {
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
  }
  .current-subtotal-text {
    color: ${(props) => props.theme.colorPalette.gray[800]};
    ${(props) => fontSize12(props)}
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => fontSize12(props)}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.newMiniBag ? `${fontSize12(props)};font-weight:600;` : fontSize14(props))}
    }
  }
  .current-subtotal-val {
    ${(props) => fontSize14(props)}
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => fontSize14(props)}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.newMiniBag ? fontSize14(props) : fontSize16(props))}
    }
  }
  .estimated-subtotal-text {
    color: ${(props) => props.theme.colorPalette.gray[800]};
    ${(props) => fontSize12(props)}
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => fontSize12(props)}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.newMiniBag ? `${fontSize12(props)};font-weight:600;` : fontSize14(props))}
    }
  }
  .estimated-subtotal-val {
    ${(props) => fontSize16(props)}
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => fontSize16(props)}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.newMiniBag ? fontSize14(props) : fontSize18(props))}
    }
  }

  .subtotal-stripe {
    background: ${(props) => props.theme.colors.PROMO.PINK};
    color: ${(props) => props.theme.colors.WHITE};
    border-radius: 50px;
    margin: 10px 5px 5px;
    padding: 4px;
    text-align: center;
    display: block;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 10px 25px 5px;
    }
  }

  .loyalty-banner-cc {
    position: absolute;
    width: 140px;
    left: 50%;
    margin-left: -75px;
    z-index: 999;
    top: -43px;
  }

  ${(props) =>
    props.pageCategory === CHECKOUT_STAGES.REVIEW
      ? `
        .heading-val {
          ${paddingTopSm(props)}
          ${fontSize16(props)}
          ${colorTheme(props)}
          @media ${props.theme.mediaQuery.medium} {
            ${fontSize14(props)}
          }
          @media ${props.theme.mediaQuery.large} {
            ${fontSize18(props)}
          }
        }
        .subheading-val {
          ${paddingTopSm(props)}
          ${fontSize12(props)}
          color: ${props.theme.colorPalette.gray[900]};
          @media ${props.theme.mediaQuery.medium} {
            ${fontSize10(props)}
          }
          @media ${props.theme.mediaQuery.large} {
            ${fontSize16(props)}
          }
        }
      `
      : ``};

  ${(props) =>
    props.pageCategory === CHECKOUT_STAGES.CONFIRMATION
      ? `
        .heading-val {
          ${paddingTopSm(props)}
          ${fontSize16(props)}
          ${colorTheme(props)}
          ${
            props.isGuest && !props.earnedRewardAvailable
              ? `color: ${props.theme.colorPalette.gray[900]};`
              : colorTheme(props)
          }
          @media ${props.theme.mediaQuery.medium} {
            ${fontSize14(props)}
          }
          @media ${props.theme.mediaQuery.large} {
            ${fontSize24(props)}
          }
        }
        .subheading-val {
          ${paddingTopSm(props)}
          ${fontSize12(props)}
          color: ${props.theme.colorPalette.gray[900]};
          @media ${props.theme.mediaQuery.medium} {
            ${fontSize12(props)}
          }
          @media ${props.theme.mediaQuery.large} {
            ${fontSize18(props)}
          }
        }
      `
      : ``};
  ${(props) =>
    props.pageCategory === ADDED_TO_BAG_PAGE
      ? `
        .heading-val {
          ${paddingTopSm(props)}
          ${fontSize16(props)}
          ${colorTheme(props)}
          @media ${props.theme.mediaQuery.medium} {
            ${fontSize16(props)}
          }
          @media ${props.theme.mediaQuery.large} {
            ${fontSize16(props)}
          }
        }
        .subheading-val {
          ${paddingTopSm(props)}
          ${fontSize12(props)}
          color: ${props.theme.colorPalette.gray[900]};
          ${
            !props.isGuest &&
            !props.isPlcc &&
            `${colorTheme(props)}
            ${fontSize16(props)}`
          }
          @media ${props.theme.mediaQuery.medium} {
            ${fontSize12(props)}
            ${
              !props.isGuest &&
              !props.isPlcc &&
              `${colorTheme(props)}
              ${fontSize16(props)}`
            }
          }
          @media ${props.theme.mediaQuery.large} {
            ${fontSize12(props)}
            ${
              !props.isGuest &&
              !props.isPlcc &&
              `${colorTheme(props)}
              ${fontSize16(props)}`
            }
          }
        }
        .description-val {
          ${paddingTopSm(props)}
          ${fontSize12(props)}
          @media ${props.theme.mediaQuery.medium} {
            ${fontSize12(props)}
          }
          @media ${props.theme.mediaQuery.large} {
            ${fontSize12(props)}
          }
        }
      `
      : ``};

  ${(props) =>
    props.pageCategory === CHECKOUT_STAGES.MINIBAG
      ? `
      .heading-val {
        ${paddingTopXs(props)}
        ${fontSize16(props)}
        @media ${props.theme.mediaQuery.medium} {
          ${fontSize12(props)}
        }
        @media ${props.theme.mediaQuery.large} {
          ${props.newMiniBag ? fontSize14(props) : fontSize18(props)}
        }
      }
      .subheading-val {
        ${paddingTopXxs(props)}
        ${fontSize12(props)}
        @media ${props.theme.mediaQuery.medium} {
          ${fontSize10(props)}
        }
        @media ${props.theme.mediaQuery.large} {
          ${fontSize14(props)}
        }
      }
      .description-val {
        ${paddingTopXxs(props)}
        ${fontSize12(props)}
        @media ${props.theme.mediaQuery.medium} {
          ${fontSize10(props)}
        }
        @media ${props.theme.mediaQuery.large} {
          ${props.newMiniBag ? fontSize12(props) : fontSize14(props)}
        }
      }
      .remaining-val {
        ${paddingTopXxs(props)}
        ${fontSize12(props)}
        @media ${props.theme.mediaQuery.medium} {
          ${fontSize10(props)}
        }
        @media ${props.theme.mediaQuery.large} {
          ${props.newMiniBag ? fontSize12(props) : fontSize14(props)}
        }
      }
      .subtotal-section {
        margin: 0;
        border: none;
        padding-top: 8px;
        ${paddingTopXs(props)}
      }
      .estimated-subtotal-row {
        padding: ${props.theme.spacing.ELEM_SPACING.XS} 0 0;
      }
    `
      : ``};

  ${(props) =>
    props.pageCategory === PRODUCT_DETAIL_PAGE
      ? `
      .heading-val {
        ${paddingTopSm(props)}
        ${fontSize20(props)}
        ${colorTheme(props)}
      }
      .subheading-val {
        ${paddingTopXxs(props)}
        ${fontSize12(props)}
        color: ${props.theme.colorPalette.gray[900]};
      }
      .description-val {
        ${paddingTopXxs(props)}
        ${fontSize12(props)}
      }
    `
      : ``};
`;

// ${props => (props.pageCategory ? `` : ``)};
export default Styles;
