// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { PropTypes } from 'prop-types';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import names from '../../../../../../../constants/eventsName.constants';
import userConstants from '../loyaltyFooterSection.constants';
import {
  routeToAnalyticsEventMap,
  getUserType,
} from '../../../../../browse/ApplyCardPage/utils/utility';
import withStyles from '../../../../../../common/hoc/withStyles';
import Styles from '../styles/LoyaltyFooterSection.style';
import { Anchor, BodyCopy } from '../../../../../../common/atoms';
import {
  FooterLinksSection,
  SizeBetweenWrapper,
} from '../styles/LoyaltyFooterSection.style.native';
import ModalNative from '../../../../../../common/molecules/Modal';
import LoginPageContainer from '../../../../../account/LoginPage';
import CreateAccount from '../../../../../account/CreateAccount';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

class LoyaltyFooterSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      getComponentId: {
        login: '',
        createAccount: '',
        favorites: '',
      },
      horizontalBar: true,
      modalHeaderLbl: ' ',
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.isUserLoggedIn && state.showModal) {
      return { showModal: false };
    }
    return null;
  }

  toggleModal = ({ getComponentId }) => {
    this.setState((state) => ({
      showModal: !state.showModal,
      getComponentId: getComponentId
        ? {
            login: getComponentId.login,
            createAccount: getComponentId.createAccount,
          }
        : '',
    }));
  };

  showLoginModal = () => {
    this.setState({
      showModal: true,
      getComponentId: {
        login: true,
        createAccount: false,
      },
    });
  };

  showCheckoutModal = () => {
    this.setState({
      showModal: true,
      getComponentId: {
        login: false,
        createAccount: true,
      },
    });
  };

  extractCheckout = (pageName, isRTPSFlow) => {
    if (pageName === 'shipping' || pageName === 'billing' || pageName === 'review')
      return isRTPSFlow ? `rtps-${pageName}` : pageName;

    if (pageName === 'bagpage' || pageName === 'signin-cta-banner') return 'cart';
    return pageName;
  };

  getCustomLoyaltyLocation = (pageCategory, isRTPSFlow) => {
    if (!pageCategory) return pageCategory;
    const pageName = pageCategory.toLowerCase();
    if (pageName === 'isproductdetailview') return 'pdp';
    if (pageName === 'isaddedtobagpage') return 'added-to-bag-modal';
    if (pageName === 'minibag') return 'minibag';
    return this.extractCheckout(pageName, isRTPSFlow);
  };

  handleClickData = (customEvts) => {
    const { cartOrderItems, isGuest, isPlcc, pageCategory } = this.props;
    const pageData = 'shopping bag';
    const productsData = BagPageUtils.formatBagProductsData(cartOrderItems);
    const userType = getUserType(isGuest, isPlcc);
    const loyaltyLocation = this.getCustomLoyaltyLocation(pageCategory);

    const analyticsData = {
      pageType: pageData,
      pageSection: pageData,
      pageSubSection: pageData,
      products: productsData,
      pageName: pageData,
      customEvents: customEvts,
      loyaltyLocation,
    };
    // let eventType = '';
    const pageValue = routeToAnalyticsEventMap[pageCategory.toLowerCase()];
    const trackingStr = `${userType}-${pageValue}`;
    if (userType === userConstants.PLCC_USER) {
      analyticsData.plccItem = trackingStr;
    } else if (userType === userConstants.MPR_USER) {
      analyticsData.mprItem = trackingStr;
    } else {
      analyticsData.guestItem = trackingStr;
    }
    return analyticsData;
  };

  renderComponent = ({ navigation, getComponentId, isUserLoggedIn }) => {
    let componentContainer = null;
    if (getComponentId.login || getComponentId.favorites) {
      componentContainer = (
        <LoginPageContainer
          onRequestClose={this.toggleModal}
          navigation={navigation}
          isUserLoggedIn={isUserLoggedIn}
          variation={getComponentId.favorites && 'favorites'}
          showLogin={this.showloginModal}
          showCheckoutModal={this.showCheckoutModal}
          updateHeader={this.updateHeader}
        />
      );
    }
    if (getComponentId.createAccount) {
      componentContainer = (
        <CreateAccount
          showCheckoutModal={this.showCheckoutModal}
          showLogin={this.showloginModal}
          navigation={navigation}
          onRequestClose={this.toggleModal}
        />
      );
    }
    return <React.Fragment>{componentContainer}</React.Fragment>;
  };

  renderApplyNowLink = (text, resetTimerStatus) => {
    const { openApplyNowModal, navigation, closeAddedToBagModal } = this.props;
    const analyticsData = this.handleClickData(['event129']);
    return (
      <ClickTracker
        as={Anchor}
        name={names.screenNames.loyalty_apply_now_click}
        clickData={analyticsData}
        className="applyNow"
        fontSizeVariation="medium"
        anchorVariation="primary"
        text={text}
        underline
        onPress={() => {
          if (resetTimerStatus) {
            resetTimerStatus(true);
          }
          if (navigation) {
            navigation.navigate('ApplyNow');
          }
          openApplyNowModal({ isModalOpen: false, isPLCCModalOpen: true });
          closeAddedToBagModal();
        }}
      />
    );
  };

  renderLearnMoreLink = (text, resetTimerStatus) => {
    const { openApplyNowModal, navigation, closeAddedToBagModal } = this.props;
    const analyticsData = this.handleClickData(['event130']);
    return (
      <ClickTracker
        as={Anchor}
        name={names.screenNames.loyalty_learn_more_click}
        clickData={analyticsData}
        className="learnMore"
        fontSizeVariation="medium"
        anchorVariation="primary"
        text={text}
        underline
        onPress={() => {
          if (resetTimerStatus) {
            resetTimerStatus(true);
          }
          if (navigation) {
            navigation.navigate('ApplyNow');
          }
          openApplyNowModal({ isModalOpen: true });
          closeAddedToBagModal();
        }}
      />
    );
  };

  renderCreateAccountLink = (text, resetTimerStatus) => {
    const analyticsData = this.handleClickData(['event125']);
    return (
      <ClickTracker
        as={Anchor}
        name={names.screenNames.loyalty_join_click}
        clickData={analyticsData}
        className="createAccount"
        fontSizeVariation="medium"
        anchorVariation="primary"
        text={text}
        underline
        onPress={(e) => {
          if (resetTimerStatus) {
            resetTimerStatus(true);
          }
          return this.toggleModal({
            e,
            getComponentId: {
              login: false,
              createAccount: true,
            },
          });
        }}
      />
    );
  };

  toggleLinks = (toggleLogin) => {
    toggleLogin();
  };

  renderLoginLink = (text, resetTimerStatus) => {
    const analyticsData = this.handleClickData(['event126']);
    return (
      <ClickTracker
        as={Anchor}
        name={names.screenNames.loyalty_login_click}
        clickData={analyticsData}
        className="logIn"
        fontSizeVariation="medium"
        anchorVariation="primary"
        text={text}
        underline
        onPress={(e) => {
          if (resetTimerStatus) {
            resetTimerStatus(true);
          }
          return this.toggleModal({
            e,
            getComponentId: {
              login: true,
              createAccount: false,
            },
          });
        }}
      />
    );
  };

  getLinkWithName = (props, action, text) => {
    let returnLink;
    const { resetTimerStatus } = props;
    switch (action) {
      case 'ApplyNowAction':
        returnLink = this.renderApplyNowLink(text, resetTimerStatus);
        break;
      case 'LearnMoreAction':
        returnLink = this.renderLearnMoreLink(text, resetTimerStatus);
        break;
      case 'CreateAccountAction':
        returnLink = this.renderCreateAccountLink(text, resetTimerStatus);
        break;
      case 'loginAction':
        returnLink = this.renderLoginLink(text, resetTimerStatus);
        break;
      default:
        break;
    }
    return returnLink;
  };

  render() {
    const { className, footerLabels, navigation, isUserLoggedIn } = this.props;
    const { showModal, getComponentId, modalHeaderLbl, horizontalBar } = this.state;
    return (
      <View className={`${className} footer-wrapper`}>
        <FooterLinksSection>
          {!!footerLabels.link1Prefix && (
            <BodyCopy
              fontWeight="regular"
              mobileFontFamily="secondary"
              fontSize="fs9"
              text={footerLabels.link1Prefix}
              color="text.primary"
            />
          )}
          {!!footerLabels.link1Action &&
            this.getLinkWithName(this.props, footerLabels.link1Action, footerLabels.link1Text)}
          {!!footerLabels.link1Action && !!footerLabels.link2Action && <SizeBetweenWrapper />}
          {!!footerLabels.link2Prefix && (
            <BodyCopy
              fontWeight="regular"
              mobileFontFamily="secondary"
              fontSize="fs9"
              text={footerLabels.link2Prefix}
              color="text.primary"
            />
          )}
          {!!footerLabels.link2Action &&
            this.getLinkWithName(this.props, footerLabels.link2Action, footerLabels.link2Text)}
          {showModal && (
            <ModalNative
              isOpen={showModal}
              onRequestClose={this.toggleModal}
              heading={modalHeaderLbl}
              headingFontFamily="secondary"
              fontSize="fs16"
              horizontalBar={horizontalBar}
            >
              <View>
                {this.renderComponent({
                  navigation,
                  getComponentId,
                  isUserLoggedIn,
                })}
              </View>
            </ModalNative>
          )}
        </FooterLinksSection>
      </View>
    );
  }
}

LoyaltyFooterSection.propTypes = {
  className: PropTypes.string,
  cartOrderItems: PropTypes.shape([]),
  isGuest: PropTypes.bool,
  isPlcc: PropTypes.bool,
  pageCategory: PropTypes.string,
  openApplyNowModal: PropTypes.func,
  navigation: PropTypes.shape({ navigate: PropTypes.func }),
  closeAddedToBagModal: PropTypes.func,
  footerLabels: PropTypes.objectOf(PropTypes.any).isRequired,
  isUserLoggedIn: PropTypes.bool,
};

LoyaltyFooterSection.defaultProps = {
  className: '',
  cartOrderItems: [],
  isGuest: false,
  isPlcc: false,
  pageCategory: '',
  openApplyNowModal: PropTypes.func,
  navigation: {},
  closeAddedToBagModal: PropTypes.func,
  isUserLoggedIn: false,
};

export default withStyles(LoyaltyFooterSection, Styles);
export { LoyaltyFooterSection as LoyaltyFooterSectionVanilla };
