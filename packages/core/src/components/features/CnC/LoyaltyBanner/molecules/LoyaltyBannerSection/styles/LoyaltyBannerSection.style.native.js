// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const plccMpr = (props) =>
  props.isPlcc ? props.theme.colorPalette.userTheme.plcc : props.theme.colorPalette.userTheme.mpr;

const isSigninCtaBanner = (props) =>
  props.pageCategory && props.pageCategory === 'signin-cta-banner';
const hide = 'display:none;';

const LineStyle = styled.View`
  border-width: 2px;
  border-color: ${plccMpr};
  ${(props) => (isSigninCtaBanner(props) ? `${hide}` : '')}
`;

const LoyaltySectionWrapper = styled.View`
  padding: 0 12px 12px;
  ${(props) => (isSigninCtaBanner(props) ? `padding:0;` : '')}
`;

const FooterSectionWrapper = styled.View`
  ${(props) => (isSigninCtaBanner(props) ? `${hide}` : '')}
`;

export { LineStyle, LoyaltySectionWrapper, FooterSectionWrapper };
