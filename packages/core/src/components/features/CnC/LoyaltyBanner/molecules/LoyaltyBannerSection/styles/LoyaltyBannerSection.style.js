// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { PRODUCT_DETAIL_PAGE } from '../../../../../../../../../web/src/components/App.constants';

const plccMpr = (props) =>
  props.isPlcc ? props.theme.colorPalette.userTheme.plcc : props.theme.colorPalette.userTheme.mpr;

const Styles = css`
  .loyalty-banner-wrapper {
    padding: 12px 14px;
    ${(props) =>
      props.newMiniBag
        ? ` background-color: ${props.theme.colors.WHITE}; border-radius: 0 0 10px 10px`
        : ''}
  }

  .loyalty-banner-section-wrapper {
    border-top: 5px solid ${plccMpr};
    border-bottom: 5px solid ${plccMpr};
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .loyalty-banner-wrapper-fancy {
    background: ${(props) => props.theme.colors.PROMO.PLCCBLUELIGHT};
    background: linear-gradient(145deg, rgba(68, 184, 231, 1) 50%, rgba(4, 167, 225, 1) 50%);
    border-top: 12px solid ${(props) => props.theme.colors.PROMO.PLCCBLUE};
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    position: relative;
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
  }

  .loyalty-banner-wrapper-fancy a {
    color: ${(props) => props.theme.colors.WHITE};
  }

  .rewards-discount {
    font-size: 54px;
    position: relative;
    width: 108px;
    display: inline-block;
    text-align: left;
    vertical-align: sub;
    line-height: 60px;
  }

  .rewards-percentage {
    font-size: 28px;
    position: absolute;
    top: 7px;
    line-height: 28px;
  }

  .rewards-off {
    font-size: 12px;
    display: inline;
    line-height: 12px;
  }

  .rewards-today {
    white-space: nowrap;
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .loyalty-banner-wrapper {
      padding: 12px 6px;
    }
    .loyalty-banner-section-wrapper {
      border-top: 3px solid ${plccMpr};
      border-bottom: 3px solid ${plccMpr};
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .loyalty-banner-wrapper {
      padding: 12px 12px;
    }

    .loyalty-banner-section-wrapper {
      border-top: 4px solid ${plccMpr};
      border-bottom: 4px solid ${plccMpr};
      padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      color: ${(props) => props.theme.colors.WHITE};
    }

    ${(props) =>
      props.pageCategory === PRODUCT_DETAIL_PAGE
        ? `
    .loyalty-banner-section-wrapper {
      padding-bottom: ${props.theme.spacing.ELEM_SPACING.SM};
    }`
        : ``};
  }

  ${(props) =>
    props.pageCategory && props.pageCategory === 'isProductDetailView'
      ? `
      .loyalty-banner-wrapper {
        padding: 0 0 14px;
      }
      .loyalty-banner-section-wrapper {
        padding-left: 0;
        padding-right: 0;
      }

      @media ${props.theme.mediaQuery.medium} {
        .loyalty-banner-wrapper {
          padding: 0 0  ${props.theme.spacing.ELEM_SPACING.SM};
        }`
      : ``};

  ${(props) =>
    props.pageCategory && props.pageCategory === 'signin-cta-banner'
      ? `
          .loyalty-banner-wrapper {
            padding:0;
          }

          .footer {
            display:none;
          }

          .subheading-val {
            display:none;
          }

          .mpr-plcc-theme {
            font-weight: ${props.theme.typography.fontWeights.extrabold};
          }

          .heading-val {
            padding:0;
            font-weight: ${props.theme.fonts.fontWeight.semiBold};
          }

          .subtotal-section {
            display:none;
          }

          .description-val {
            display:none;
          }

          .loyalty-banner-section-wrapper {
              border:none;padding:0;
          }

          @media ${props.theme.mediaQuery.large} {
            .loyalty-banner-section-wrapper {
                border:none;padding:0;
            }
            .subtotal-section {
              display:none;
            }
            .heading-val {
              padding:0;
            }
          }

          @media ${props.theme.mediaQuery.medium} {
            .loyalty-banner-section-wrapper {
              border:none;padding:0;
            }
            .subtotal-section {
              display:none;
            }
            .heading-val {
              padding:0;
            }
          }`
      : ``};

  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default Styles;
