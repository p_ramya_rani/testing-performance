// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const Styles = css`
  .links-wrapper {
    display: flex;
    justify-content: center;
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
    @media ${props => props.theme.mediaQuery.large} {
      padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  ${props =>
    props.pageCategory === 'miniBag'
      ? `
      .links-wrapper {
        padding-top: ${props.theme.spacing.ELEM_SPACING.XS};
      }
    `
      : ``};
  ${props =>
    props.isProductDetailView
      ? `
      .links-wrapper {
        padding-top: ${props.theme.spacing.ELEM_SPACING.XS};
      }
      .footer-wrapper {
        text-align: center;
      }`
      : ``};

  .space-between {
    padding-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
    @media ${props => props.theme.mediaQuery.large} {
      padding-left: ${props => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .links-container {
    display: flex;
  }
  ${props =>
    props.isFancyBanner
      ? `
      .symbolWrapper {
        color: ${props.theme.colors.WHITE};
      }`
      : ``};
  .reward-apply-cta {
    background: ${props => props.theme.colors.WHITE};
    padding: 10px 20px;
    text-transform: uppercase;
    text-decoration: none;
    color: ${props => props.theme.colors.PROMO.PLCCBLUE};
  }
  .reward-right-cta {
    position: absolute;
    right: 10px;
    display: flex;
    bottom: 10px;
    color: ${props => props.theme.colors.WHITE};
  }
  .reward-right-cta a {
    color: ${props => props.theme.colors.WHITE};
  }
  .links-wrapper a.reward-apply-cta {
    font-weight: 900;
    color: ${props => props.theme.colorPalette.userTheme.plcc};
  }
`;

export default Styles;
