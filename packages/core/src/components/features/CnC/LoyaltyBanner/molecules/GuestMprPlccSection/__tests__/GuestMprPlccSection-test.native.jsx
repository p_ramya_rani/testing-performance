// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { GuestMprPlccSectionVanilla } from '../views/GuestMprPlccSection';
import { SubHeadingLabel } from '../styles/GuestMprPlccSection.style.native';

describe('GuestMprPlccSection View Component', () => {
  let component;
  const Props = {
    className: '',
    labels: {},
    estimatedRewardsVal: 20,
    currentSubtotal: 21,
    estimatedSubtotal: 33,
    thresholdValue: 16.66,
    finalPointsLabelStr: '',
    showSubtotal: 0,
    fsPoints: '',
    isPlcc: true,
    pointsDescription: '',
    earnedReward: null,
    remainingPlcc: 0,
  };

  it('GuestMprPlccSection should render correctly', () => {
    component = shallow(<GuestMprPlccSectionVanilla {...Props} />);
    expect(component).toMatchSnapshot();
  });

  const Props2 = {
    className: '',
    labels: {},
    estimatedRewardsVal: 20,
    currentSubtotal: 21,
    estimatedSubtotal: 33,
    thresholdValue: 16.66,
    finalPointsLabelStr: '',
    showSubtotal: 1,
    fsPoints: '',
    isPlcc: false,
    pointsDescription: '',
    earnedReward: 0,
    remainingPlcc: 0,
    subHeadingLabel: 'subheading',
    pageChecksObj: {},
  };

  it('GuestMprPlccSection should render correctly', () => {
    component = shallow(<GuestMprPlccSectionVanilla {...Props2} />);
    expect(component).toMatchSnapshot();
  });

  it('should render SubHeadingLabel ', () => {
    component = shallow(<GuestMprPlccSectionVanilla {...Props2} />);
    expect(component.find(SubHeadingLabel)).toHaveLength(1);
  });

  it('should not render SubHeadingLabel ', () => {
    const Props3 = {
      ...Props2,
      isSignInCtaBanner: true,
      pageChecksObj: { isSignInCtaBanner: true },
    };
    component = shallow(<GuestMprPlccSectionVanilla {...Props3} />);
    expect(component.find(SubHeadingLabel)).toHaveLength(0);
  });
});
