// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import { PropTypes } from 'prop-types';
import LoyaltyBannerSkeleton from '../../../skeleton/LoyaltyBannerSkeleton.view';
import withStyles from '../../../../../../common/hoc/withStyles';
import Styles from '../styles/LoyaltyBannerSection.style';
import { BodyCopy } from '../../../../../../common/atoms';
import { labelsHashValuesReplace, convertHtml } from '../../../util/utility';
import GuestMprPlccSection from '../../GuestMprPlccSection';
import LoyaltyFooterSection from '../../LoyaltyFooterSection';
import { renderLoyaltyLabels, getPageCategory } from '../../../util/utilityCommon';

const getBRTag = (isFancyBanner) => {
  return isFancyBanner ? '' : '<br/>';
};

const getWrapperClass = (isFancyBanner) => (isFancyBanner ? '' : 'loyalty-banner-wrapper');

const getSectionClass = (isFancyBanner) =>
  isFancyBanner ? 'loyalty-banner-wrapper-fancy' : 'loyalty-banner-section-wrapper';

const utilArrayHeader = (LoyaltyLabels, className, earnedRewardAvailable, isFancyBanner) => {
  let rewardsVal = '';
  if (earnedRewardAvailable) {
    rewardsVal = LoyaltyLabels.rewardPointsValueFn.slice(0, -6);
  } else if (
    LoyaltyLabels &&
    LoyaltyLabels.rewardPointsValueFn !== null &&
    LoyaltyLabels.rewardPointsValueFn >= 0
  ) {
    rewardsVal = LoyaltyLabels.rewardPointsValueFn;
  }
  return [
    {
      key: '#estimatedRewardsVal#',
      value:
        rewardsVal || rewardsVal >= 0
          ? `<span class="${className} mpr-plcc-theme">${rewardsVal}</span>`
          : '',
    },
    {
      key: '#br#',
      value: getBRTag(isFancyBanner),
    },
    {
      key: '#tagOpen#',
      value: `<span class="${className} mpr-plcc-theme">`,
    },
    {
      key: '#tagClose#',
      value: `</span>`,
    },
  ];
};

const utilArrayNextReward = (pointsToNextReward, className) => {
  return [
    {
      key: '#pointsToNextReward#',
      value:
        pointsToNextReward && pointsToNextReward > 0
          ? `<span class="${className} mpr-plcc-theme">${pointsToNextReward}</span>`
          : '',
    },
  ];
};

const returnBoolForSubTotal = ({
  estimatedSubtotal,
  checkThresholdValue,
  thresholdValue,
  isPlcc,
  isReviewPage,
  isConfirmationPage,
  isAddedToBagPage,
  isProductDetailView,
}) => {
  let showSubtotal = false;
  /* istanbul ignore else */
  if (
    estimatedSubtotal &&
    checkThresholdValue > thresholdValue &&
    !isPlcc &&
    !isReviewPage &&
    !isConfirmationPage &&
    !isAddedToBagPage &&
    !isProductDetailView
  ) {
    showSubtotal = true;
  }
  return showSubtotal;
};

const LoyaltyBannerSection = (props) => {
  const {
    className,
    labels,
    currentSubtotal,
    estimatedSubtotal,
    checkThresholdValue,
    thresholdValue,
    isGuest,
    earnedReward,
    isPlcc,
    estimatedRewardsVal,
    pointsToNextReward,
    getCurrencySymbol,
    pageCategory,
    openOverlay,
    closeAddedToBagModal,
    openApplyNowModal,
    bagLoading,
    isCartEmpty,
    footerLabels,
    cartOrderItems,
    setClickAnalyticsData,
    showFullPageAuth,
    isRtpsFlow,
    isFancyBanner,
    isOptmisticAddToBagModal,
    newMiniBag,
  } = props;
  let showSubtotal = false;
  let headingLabel = '';
  let remainingPlcc = '';
  let subHeadingLabel = '';
  let descriptionLabel = '';
  const earnedRewardAvailable = !!earnedReward;
  const pageCategoryArr = getPageCategory(pageCategory);
  const { isReviewPage, isConfirmationPage, isAddedToBagPage, isProductDetailView } =
    pageCategoryArr;

  showSubtotal = returnBoolForSubTotal({
    estimatedSubtotal,
    checkThresholdValue,
    thresholdValue,
    isPlcc,
    isReviewPage,
    isConfirmationPage,
    isAddedToBagPage,
    isProductDetailView,
  });

  const LoyaltyLabels = renderLoyaltyLabels(
    labels,
    estimatedRewardsVal,
    earnedReward,
    isGuest,
    isPlcc,
    isReviewPage,
    isConfirmationPage,
    isAddedToBagPage,
    isProductDetailView,
    isFancyBanner
  );

  const utilArrHeader = utilArrayHeader(
    LoyaltyLabels,
    className,
    earnedRewardAvailable,
    isFancyBanner
  );
  const finalHeaderValue = labelsHashValuesReplace(LoyaltyLabels.headingLabelValFn, utilArrHeader);
  headingLabel = LoyaltyLabels.headingLabelValFn ? convertHtml(finalHeaderValue) : false;

  const utilArrSubHeader = [
    {
      key: '#sectionSymbol#',
      value: `<span class="${className} section-symbol">${labels.sectionSymbol}</span>${
        isFancyBanner ? '</span>' : ''
      }`,
    },
    {
      key: '#off#',
      value: `<span class="${className} rewards-discount">30<span class="${className} rewards-percentage">%</span><span class="${className} rewards-off">OFF</span></span>${
        isFancyBanner ? '<span class="rewards-today">' : ''
      }`,
    },
  ];
  const finalSubHeadingValue = labelsHashValuesReplace(
    LoyaltyLabels.subHeadingLabelFn,
    utilArrSubHeader
  );
  subHeadingLabel = LoyaltyLabels.subHeadingLabelFn ? convertHtml(finalSubHeadingValue) : false;
  const utilArrDescription = [
    {
      key: '#br#',
      value: getBRTag(isFancyBanner),
    },
  ];
  const finalDescriptionValue = labelsHashValuesReplace(
    LoyaltyLabels.descriptionLabelFn,
    utilArrDescription
  );
  descriptionLabel = LoyaltyLabels.descriptionLabelFn ? convertHtml(finalDescriptionValue) : false;

  const utilArrNextReward = utilArrayNextReward(pointsToNextReward, className);
  const finalStrRemainingValue = labelsHashValuesReplace(
    LoyaltyLabels.remainingPlccValFn,
    utilArrNextReward
  );

  remainingPlcc =
    LoyaltyLabels.remainingPlccValFn && pointsToNextReward
      ? convertHtml(finalStrRemainingValue)
      : false;

  return (
    <div className={`${className}`}>
      <div className={getWrapperClass(isFancyBanner)}>
        {(
          !isConfirmationPage
            ? isProductDetailView || !bagLoading || isCartEmpty || isOptmisticAddToBagModal
            : true
        ) ? (
          <>
            <BodyCopy
              className={getSectionClass(isFancyBanner)}
              component="div"
              fontFamily="secondary"
            >
              <GuestMprPlccSection
                labels={labels}
                headingLabel={headingLabel}
                subHeadingLabel={subHeadingLabel}
                descriptionLabel={descriptionLabel}
                remainingPlcc={remainingPlcc}
                showSubtotal={showSubtotal}
                getCurrencySymbol={getCurrencySymbol}
                currentSubtotal={currentSubtotal}
                estimatedSubtotal={estimatedSubtotal}
                isPlcc={isPlcc}
                isGuest={isGuest}
                pageCategory={pageCategory}
                isProductDetailView={isProductDetailView}
                earnedRewardAvailable={earnedRewardAvailable}
                isFancyBanner={isFancyBanner}
                closeAddedToBagModal={closeAddedToBagModal}
                openApplyNowModal={openApplyNowModal}
                newMiniBag={newMiniBag}
              />

              <div className="footer">
                <LoyaltyFooterSection
                  footerLabels={footerLabels}
                  className={className}
                  openOverlay={openOverlay}
                  closeAddedToBagModal={closeAddedToBagModal}
                  openApplyNowModal={openApplyNowModal}
                  pageCategory={pageCategory}
                  isProductDetailView={isProductDetailView}
                  cartOrderItems={cartOrderItems}
                  isPlcc={isPlcc}
                  isGuest={isGuest}
                  setClickAnalyticsData={setClickAnalyticsData}
                  showFullPageAuth={showFullPageAuth}
                  isRtpsFlow={isRtpsFlow}
                  isFancyBanner={isFancyBanner}
                />
              </div>
            </BodyCopy>
          </>
        ) : (
          <LoyaltyBannerSkeleton pageCategory={pageCategory} />
        )}
      </div>
    </div>
  );
};

LoyaltyBannerSection.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  footerLabels: PropTypes.shape({}).isRequired,
  estimatedRewardsVal: PropTypes.number,
  currentSubtotal: PropTypes.number,
  estimatedSubtotal: PropTypes.number,
  thresholdValue: PropTypes.number,
  checkThresholdValue: PropTypes.number,
  isGuest: PropTypes.bool,
  earnedReward: PropTypes.number,
  isPlcc: PropTypes.bool,
  pointsToNextReward: PropTypes.number,
  getCurrencySymbol: PropTypes.string,
  pageCategory: PropTypes.string,
  openOverlay: PropTypes.func.isRequired,
  closeAddedToBagModal: PropTypes.func.isRequired,
  openApplyNowModal: PropTypes.func.isRequired,
  bagLoading: PropTypes.bool,
  isCartEmpty: PropTypes.bool,
  cartOrderItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  setClickAnalyticsData: PropTypes.func.isRequired,
  showFullPageAuth: PropTypes.bool,
  isRtpsFlow: PropTypes.bool,
  isFancyBanner: PropTypes.bool,
  isOptmisticAddToBagModal: PropTypes.bool,
};

LoyaltyBannerSection.defaultProps = {
  className: '',
  estimatedRewardsVal: null,
  currentSubtotal: 0,
  estimatedSubtotal: 0,
  thresholdValue: 0,
  checkThresholdValue: 0,
  isGuest: false,
  earnedReward: 0,
  isPlcc: false,
  pageCategory: '',
  pointsToNextReward: 0,
  getCurrencySymbol: '',
  bagLoading: false,
  isCartEmpty: false,
  showFullPageAuth: false,
  isRtpsFlow: false,
  isFancyBanner: false,
  isOptmisticAddToBagModal: false,
};

export default withStyles(LoyaltyBannerSection, Styles);
export { LoyaltyBannerSection as LoyaltyBannerSectionVanilla };
