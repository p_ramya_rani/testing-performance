// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import {
  getCreateAccountPayload,
  getLoginPayload,
  getLearnMorePayload,
  getApplyNowPayload,
  getCustomLoyaltyLocation,
  setLoyaltyLocation,
} from '@tcp/core/src/constants/analytics';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import userConstants from '../loyaltyFooterSection.constants';
import { routeToAnalyticsEventMap } from '../../../../../browse/ApplyCardPage/utils/utility';
import withStyles from '../../../../../../common/hoc/withStyles';
import Styles from '../styles/LoyaltyFooterSection.style';
import { BodyCopy, Anchor } from '../../../../../../common/atoms';
import { routerPush } from '../../../../../../../utils';

const openModalApplyNowModal = (openApplyNowModal, pageCategory, step = 1) => {
  if (!openApplyNowModal) return null;
  if (step === 1) return openApplyNowModal({ isModalOpen: true, status: null, pageCategory });
  return openApplyNowModal({
    isModalOpen: false,
    isPLCCModalOpen: true,
    status: null,
    pageCategory,
  });
};

/**
 * @constant - handleClickData
 *
 * @param - userType --> User Type amongst [ 'MPR', 'PLCC', 'Guest' ]
 * @param - pageCategory - page value where link has been used
 *
 */

export const handleClickData = (userType = '', pageCategory = '') => {
  const pageValue = routeToAnalyticsEventMap[pageCategory.toLowerCase()];
  const trackingStr = `${userType}-${pageValue}`;
  const analyticsData = {};
  if (userType === userConstants.PLCC_USER) {
    analyticsData.plccItem = trackingStr;
  } else if (userType === userConstants.MPR_USER) {
    analyticsData.mprItem = trackingStr;
  } else {
    analyticsData.guestItem = trackingStr;
  }
  return analyticsData;
};

let loyaltyLocation = '';

const renderApplyNowLink = (
  text,
  closeAddedToBagModal,
  openApplyNowModal,
  pageCategory,
  isFancyBanner
) => {
  return (
    <Anchor
      fontSizeVariation="medium"
      anchorVariation="primary"
      noLink
      className={`${isFancyBanner ? 'reward-apply-cta' : ''}`}
      handleLinkClick={(e) => {
        e.preventDefault();
        openModalApplyNowModal(openApplyNowModal, pageCategory, 2);
        closeAddedToBagModal();
      }}
      underline
    >
      <ClickTracker
        onClick={() => {
          setLoyaltyLocation(loyaltyLocation);
        }}
        clickData={getApplyNowPayload(loyaltyLocation)}
      >
        {text}
      </ClickTracker>
    </Anchor>
  );
};

const renderLearnMoreLink = (text, closeAddedToBagModal, openApplyNowModal, pageCategory) => {
  return (
    <Anchor
      fontSizeVariation="medium"
      anchorVariation="primary"
      noLink
      underline
      handleLinkClick={(e) => {
        e.preventDefault();
        openModalApplyNowModal(openApplyNowModal, pageCategory, 1);
        closeAddedToBagModal();
      }}
    >
      <ClickTracker
        onClick={() => {
          setLoyaltyLocation(loyaltyLocation);
        }}
        clickData={getLearnMorePayload(loyaltyLocation)}
      >
        {text}
      </ClickTracker>
    </Anchor>
  );
};

const onLinkClick = ({ e, componentId, closeAddedToBagModal, openOverlay, showFullPageAuth }) => {
  e.preventDefault();
  const { pathname = 'referrer' } = (window && window.location) || {};
  if (showFullPageAuth && componentId === 'createAccount') {
    routerPush(`/home?target=register&successtarget=${pathname}`, '/home/register');
  } else if (showFullPageAuth && componentId === 'login') {
    routerPush(`/home?target=login&successtarget=${pathname}`, '/home/login');
  } else {
    closeAddedToBagModal();
    openOverlay({
      component: componentId,
      variation: 'primary',
    });
  }
};

const renderCreateAccountLink = (text, closeAddedToBagModal, openOverlay, userInfo = {}) => {
  const { showFullPageAuth = false } = userInfo;

  return (
    <Anchor
      fontSizeVariation="medium"
      anchorVariation="primary"
      noLink
      underline
      handleLinkClick={(e) => {
        e.preventDefault();
        onLinkClick({
          e,
          componentId: 'createAccount',
          closeAddedToBagModal,
          openOverlay,
          showFullPageAuth,
        });
      }}
    >
      <ClickTracker
        onClick={() => {
          setLoyaltyLocation(loyaltyLocation);
        }}
        clickData={getCreateAccountPayload(loyaltyLocation)}
      >
        {text}
      </ClickTracker>
    </Anchor>
  );
};

const renderLoginLink = (text, closeAddedToBagModal, openOverlay, userInfo = {}) => {
  const { showFullPageAuth = false } = userInfo;
  return (
    <Anchor
      fontSizeVariation="medium"
      anchorVariation="primary"
      noLink
      underline
      handleLinkClick={(e) => {
        e.preventDefault();
        onLinkClick({
          e,
          componentId: 'login',
          closeAddedToBagModal,
          openOverlay,
          showFullPageAuth,
        });
      }}
    >
      <ClickTracker
        onClick={() => {
          setLoyaltyLocation(loyaltyLocation);
        }}
        clickData={getLoginPayload(loyaltyLocation)}
      >
        {text}
      </ClickTracker>
    </Anchor>
  );
};

const renderSpacer = (footerLabels, isFancyBanner) => {
  return (
    footerLabels.link1Action &&
    footerLabels.link2Action &&
    !isFancyBanner && <span className="space-between" />
  );
};

const getLinkWithName = (props, action, text) => {
  let returnLink;
  const {
    closeAddedToBagModal,
    openOverlay,
    openApplyNowModal,
    isPlcc,
    isGuest,
    showFullPageAuth,
    pageCategory,
    isRtpsFlow,
    isFancyBanner,
  } = props;
  loyaltyLocation = getCustomLoyaltyLocation(pageCategory, isRtpsFlow);
  switch (action) {
    case 'ApplyNowAction':
      returnLink = renderApplyNowLink(
        text,
        closeAddedToBagModal,
        openApplyNowModal,
        pageCategory,
        isFancyBanner
      );
      break;
    case 'LearnMoreAction':
      returnLink = renderLearnMoreLink(text, closeAddedToBagModal, openApplyNowModal);
      break;
    case 'CreateAccountAction':
      returnLink = renderCreateAccountLink(text, closeAddedToBagModal, openOverlay, {
        isPlcc,
        isGuest,
        showFullPageAuth,
      });
      break;
    case 'loginAction':
      returnLink = renderLoginLink(text, closeAddedToBagModal, openOverlay, {
        isPlcc,
        isGuest,
        showFullPageAuth,
      });
      break;
    default:
      break;
  }
  return returnLink;
};

const renderLearnMore = (props, footerLabels) => {
  return (
    <>
      {footerLabels.link2Prefix && (
        <BodyCopy
          className="symbolWrapper"
          color="text.primary"
          component="span"
          fontFamily="secondary"
          fontWeight="regular"
          fontSize="fs9"
        >
          {footerLabels.link2Prefix}
        </BodyCopy>
      )}
      {footerLabels.link2Action &&
        getLinkWithName(props, footerLabels.link2Action, footerLabels.link2Text)}
    </>
  );
};

const renderLearnMoreFancy = (props, footerLabels) => {
  return (
    <span className="reward-right-cta">
      {footerLabels.linkPrefix && (
        <BodyCopy
          className="symbolWrapper"
          color="text.primary"
          component="span"
          fontFamily="secondary"
          fontWeight="regular"
          fontSize="fs9"
        >
          {footerLabels.linkPrefix}
        </BodyCopy>
      )}
      {footerLabels.link2Action &&
        getLinkWithName(props, footerLabels.link2Action, footerLabels.link2Text)}
    </span>
  );
};

const LoyaltyFooterSection = (props) => {
  const { className, footerLabels, pageCategory, isRtpsFlow, isFancyBanner } = props;
  loyaltyLocation = getCustomLoyaltyLocation(pageCategory, isRtpsFlow);

  return (
    <>
      {(footerLabels.link1Text || footerLabels.link2Text) && (
        <div className={`${className} footer-wrapper`}>
          <div className="links-wrapper">
            {footerLabels.link1Prefix && (
              <BodyCopy
                className="symbolWrapper"
                color="text.primary"
                component="span"
                fontFamily="secondary"
                fontWeight="regular"
                fontSize="fs9"
              >
                {footerLabels.link1Prefix}
              </BodyCopy>
            )}
            {footerLabels.link1Action &&
              getLinkWithName(props, footerLabels.link1Action, footerLabels.link1Text)}
            {renderSpacer(footerLabels, isFancyBanner)}
            {!isFancyBanner && renderLearnMore(props, footerLabels)}
            {isFancyBanner && renderLearnMoreFancy(props, footerLabels)}
          </div>
        </div>
      )}
    </>
  );
};

LoyaltyFooterSection.propTypes = {
  footerLabels: PropTypes.shape.isRequired,
  className: PropTypes.string,
  pageCategory: PropTypes.string,
  isRtpsFlow: PropTypes.bool,
  isFancyBanner: PropTypes.bool,
};

LoyaltyFooterSection.defaultProps = {
  className: '',
  pageCategory: '',
  isRtpsFlow: false,
  isFancyBanner: false,
};

export default withStyles(LoyaltyFooterSection, Styles);
export { LoyaltyFooterSection as LoyaltyFooterSectionVanilla };
