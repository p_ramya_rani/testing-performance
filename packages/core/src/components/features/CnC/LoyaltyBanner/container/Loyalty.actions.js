// 9fbef606107a605d69c0edbcd8029e5d 
import LoyaltyConstants from '../LoyaltyConstants';

export const getLoyaltyDetail = payload => {
  return {
    type: LoyaltyConstants.GET_LOYALTY_DETAIL,
    payload,
  };
};

// Update Loyalty Points in CART Redux
export const updatePointsInCart = payload => {
  return {
    type: LoyaltyConstants.UPDATE_POINTS_IN_CART,
    payload,
  };
};

// Update Loyalty Points in CART Redux
export const updateLoyaltyPoints = payload => {
  return {
    type: LoyaltyConstants.UPDATE_LOYALTY_POINTS,
    payload,
  };
};

export default {
  getLoyaltyDetail,
  updatePointsInCart,
};
