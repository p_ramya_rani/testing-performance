// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, select, put } from 'redux-saga/effects';
import {
  isPlccUser,
  getCurrentPointsState,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getComputedLoyaltyPoints } from '@tcp/core/src/services/abstractors/loyalty/Loyalty';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import { getProductDetails } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { SetAddedToBagData } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';
import logger from '@tcp/core/src/utils/loggerInstance';
import LoyaltyConstants from '../LoyaltyConstants';
import { updatePointsInCart, updateLoyaltyPoints } from './Loyalty.actions';
import {
  getIsPersistPointsApiEnabled,
  getLoyaltyPointsMultiplier,
} from '../../../../../reduxStore/selectors/session.selectors';

// Get Order Items from redux if Order Items are not received with API Response
function* getOrderItems(loyaltyOrderItems) {
  if (!(loyaltyOrderItems && loyaltyOrderItems.length > 0)) {
    const orderItems = yield select(BagPageSelector.getOrderItems);
    if (orderItems) {
      const cartItems = [];
      let cartItem = {};
      orderItems.map((item) => {
        const productDetail = getProductDetails(item);
        const {
          itemInfo: { itemId, qty, itemUnitDstPrice, itemUnitPrice, isGiftItem, itemBrand },
          productInfo: { variantNo },
          miscInfo: { itemPlccPointsMultiplier, itemMprPointsMultiplier },
        } = productDetail;
        cartItem = {
          variantNo,
          orderItemId: itemId,
          qty,
          itemUnitDstPrice,
          itemUnitPrice,
          itemBrand,
          giftItem: isGiftItem,
          fromRedux: true,
          itemPlccPointsMultiplier,
          itemMprPointsMultiplier,
        };
        cartItems.push(cartItem);
        return cartItem;
      });
      return cartItems;
    }
  }
  return loyaltyOrderItems;
}
function* getLoyaltyPayload(res) {
  const orderItems = yield getOrderItems(res.loyaltyOrderItems);
  return {
    orderId: res.orderId,
    orderItemId: res.orderItemId,
    orderTotal: res.orderTotal,
    orderItemTotal: res.orderItemTotal,
    orderItems,
    isElectiveBonus: res.isElectiveBonus,
    orderSubTotal: res.orderSubTotal,
    orderSubTotalBeforeDiscount: res.orderSubTotalBeforeDiscount,
    orderSubTotalDiscount: res.orderSubTotalDiscount,
    orderSubTotalWithoutGiftCard: res.orderSubTotalWithoutGiftCard,
    orderTotalAfterDiscount: res.orderTotalAfterDiscount,
    orderTotalListPrice: res.orderTotalListPrice,
    orderTotalSaving: res.orderTotalSaving,
    mprId: res.mprId,
    isPLCC: res.isPLCC,
  };
}

const getPoints = (loyaltyResponse, itemId) => {
  const items =
    loyaltyResponse &&
    loyaltyResponse.orderItemLoyalty &&
    loyaltyResponse.orderItemLoyalty.filter((obj) => {
      return String(obj.orderItemId) === String(itemId);
    });
  const item = items && items.length > 0 && items[0];
  return item.itemPoints || item.itemPoints === 0 ? parseInt(item.itemPoints, 10) : null;
};
const getPointsToNextReward = (loyaltyResponse) => {
  return (
    (loyaltyResponse &&
      loyaltyResponse.pointsToNextReward &&
      Math.round(loyaltyResponse.pointsToNextReward)) ||
    0
  );
};

function* calculateLoyaltyPoints(res, isPLCC) {
  try {
    const { orderItems } = res;
    const { rewardsDollarPoints, rewardsDollarAmount } = yield select(getLoyaltyPointsMultiplier);
    const userCurrentPoints = yield select(getCurrentPointsState);

    let loyaltyUserPoints = 0;
    const orderItemLoyalty = orderItems.map((item) => {
      const {
        orderItemId,
        variantNo,
        itemUnitDstPrice,
        qty,
        loyaltyMultiplier,
        loyaltyPLCCMultiplier,
        productInfo,
        giftItem,
      } = item;
      const mprPointsMultiplier = productInfo
        ? productInfo.loyaltyPromoMultiplierUSStore
        : loyaltyMultiplier;
      const plccPointsMultiplier = productInfo
        ? productInfo.loyaltyPromoPLCCMultiplierUSStore
        : loyaltyPLCCMultiplier;

      const pointsMultiplier = isPLCC
        ? parseInt(plccPointsMultiplier || 2, 10)
        : parseInt(mprPointsMultiplier || 1, 10);
      const itemPoints = giftItem ? 0 : Math.round(itemUnitDstPrice * qty * pointsMultiplier);
      loyaltyUserPoints += itemPoints;
      return {
        orderItemId,
        variantNo,
        itemPoints,
      };
    });
    const userTotalPoints =
      userCurrentPoints > 0 ? loyaltyUserPoints + userCurrentPoints : loyaltyUserPoints;
    const pointsToNextReward = rewardsDollarPoints - (userTotalPoints % rewardsDollarPoints);
    const rewardAmount = Math.floor(userTotalPoints / rewardsDollarPoints) * rewardsDollarAmount;
    const earnedReward = rewardAmount ? `$${rewardAmount} Reward` : null;
    return {
      loyaltyUserPoints,
      orderItemLoyalty,
      pointsToNextReward,
      earnedReward,
    };
  } catch (e) {
    logger.error('LoyaltySaga error', {
      error: e,
      extraData: {
        component: 'LoyaltySaga-calculateLoyaltyPoints',
        payloadRecieved: res,
      },
    });
    return {};
  }
}

export function* getLoyaltyDetailSaga({ payload }) {
  try {
    const isPLCC = yield select(isPlccUser);
    const isLoyaltyPointsEnabled = yield select(getIsPersistPointsApiEnabled);

    const { pointsApiCall } = payload;
    const updatedPayload = yield getLoyaltyPayload({ ...payload, isPLCC });
    const loyaltyResponse = !isLoyaltyPointsEnabled
      ? yield call(getComputedLoyaltyPoints, updatedPayload, pointsApiCall)
      : yield calculateLoyaltyPoints(updatedPayload, isPLCC);
    const pointsToNextReward = getPointsToNextReward(loyaltyResponse);
    if (payload.source === 'Cart' && loyaltyResponse) {
      const orderItems = yield select(BagPageSelector.getOrderItems);
      const loyaltyPayload = {
        isBrierleyWorking: loyaltyResponse.loyaltyUserPoints !== -1,
        estimatedRewards:
          loyaltyResponse.loyaltyUserPoints && Math.round(loyaltyResponse.loyaltyUserPoints),
        orderItems: orderItems.map((item) => {
          const itemId = item.getIn(['itemInfo', 'itemId']);
          const itemPoints = getPoints(loyaltyResponse, itemId);
          return item.setIn(['itemInfo', 'itemPoints'], itemPoints);
        }),
        pointsToNextReward,
        earnedReward: loyaltyResponse.earnedReward,
      };
      yield put(updatePointsInCart(loyaltyPayload));
    }
    if (payload.source === 'AddedToBag' && loyaltyResponse) {
      const isBrierleyWorking = loyaltyResponse.loyaltyUserPoints !== -1;
      const estimatedRewards =
        loyaltyResponse.loyaltyUserPoints && Math.round(loyaltyResponse.loyaltyUserPoints);

      const orderItems = payload.orderItems.map((item) => {
        return {
          itemInfo: {
            ...item.itemInfo,
            itemPoints: getPoints(loyaltyResponse, item.itemInfo.itemId),
          },
        };
      });

      const loyaltyPayload = {
        ...payload,
        isBrierleyWorking,
        estimatedRewards,
        orderItems,
        pointsToNextReward,
        earnedReward: loyaltyResponse.earnedReward,
      };
      yield put(
        SetAddedToBagData({
          ...payload,
          ...loyaltyPayload,
        })
      );
      yield put(updateLoyaltyPoints(loyaltyPayload));
    }
  } catch (e) {
    logger.error('LoyaltySaga error', {
      error: e,
      extraData: {
        component: 'LoyaltySaga-getLoyaltyDetailSaga',
        payloadRecieved: payload,
      },
    });
  }
}

export function* LoyaltyPageSaga() {
  yield takeLatest(LoyaltyConstants.GET_LOYALTY_DETAIL, getLoyaltyDetailSaga);
}
export default LoyaltyPageSaga;
