// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  getIsInternationalShipping,
  getIsNewLoyaltyBanner,
  getIsNewPDPEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getCurrencySymbol } from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import { openOverlayModal } from '@tcp/core/src/components/features/account/OverlayModal/container/OverlayModal.actions';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import { resetPLCCResponse } from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.actions';
import { closeAddedToBag } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import LoyaltyBannerView from '../views/LoyaltyBannerView';
import {
  getThresholdValue,
  cartOrderDetails,
  getLoyaltyBannerLabels,
  confirmationDetails,
  getFooterLabels,
} from './LoyaltyBanner.selectors';
import CheckoutSelectors, { isGuest } from '../../Checkout/container/Checkout.selector';
import { isPlccUser } from '../../../account/User/container/User.selectors';
import { setPageCategoryData } from '../../../../../analytics/actions';

export const LoyaltyBannerContainer = ({
  bagLoading,
  labels,
  orderDetails,
  isCartEmpty,
  thresholdValue,
  isGuestCheck,
  isPlcc,
  currencySymbol,
  pageCategory,
  isInternationalShipping,
  openOverlay,
  closeAddedToBagModal,
  inheritedStyles,
  openApplyNowModal,
  footerLabels,
  navigation,
  cartOrderItems,
  isUserLoggedIn,
  resetTimerStatus,
  clickAnalyticsData,
  isCompleteTheLookTestEnabled,
  showFullPageAuth,
  isFancyBanner,
  isNewLoyaltyBanner,
  isOptmisticAddToBagModal,
  isLoyalityCallCompleted,
  isNewPDPEnabled,
  newMiniBag,
}) => {
  const {
    estimatedRewards,
    subTotal,
    subTotalWithDiscounts,
    cartTotalAfterPLCCDiscount,
    earnedReward,
    pointsToNextReward,
  } = orderDetails;

  return (
    <LoyaltyBannerView
      isUserLoggedIn={isUserLoggedIn}
      labels={labels}
      estimatedRewardsVal={estimatedRewards}
      currentSubtotal={subTotal}
      estimatedSubtotal={cartTotalAfterPLCCDiscount}
      checkThresholdValue={subTotalWithDiscounts}
      thresholdValue={thresholdValue}
      isGuest={isGuestCheck}
      earnedReward={earnedReward}
      isPlcc={isPlcc}
      pointsToNextReward={pointsToNextReward}
      getCurrencySymbol={currencySymbol}
      pageCategory={pageCategory}
      isInternationalShipping={isInternationalShipping}
      openOverlay={openOverlay}
      closeAddedToBagModal={closeAddedToBagModal}
      inheritedStyles={inheritedStyles}
      openApplyNowModal={openApplyNowModal}
      footerLabels={footerLabels}
      navigation={navigation}
      bagLoading={bagLoading}
      isCartEmpty={isCartEmpty}
      cartOrderItems={cartOrderItems}
      resetTimerStatus={resetTimerStatus}
      setClickAnalyticsData={clickAnalyticsData}
      isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
      showFullPageAuth={showFullPageAuth}
      isFancyBanner={isNewLoyaltyBanner && isFancyBanner}
      isOptmisticAddToBagModal={isOptmisticAddToBagModal}
      isLoyalityCallCompleted={isLoyalityCallCompleted}
      isNewPDPEnabled={isNewPDPEnabled}
      newMiniBag={newMiniBag}
    />
  );
};

LoyaltyBannerContainer.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  orderDetails: PropTypes.shape({}).isRequired,
  closeAddedToBagModal: PropTypes.func.isRequired,
  openOverlay: PropTypes.func.isRequired,
  thresholdValue: PropTypes.number,
  isGuestCheck: PropTypes.bool,
  isPlcc: PropTypes.bool,
  currencySymbol: PropTypes.string,
  pageCategory: PropTypes.string,
  isInternationalShipping: PropTypes.bool,
  inheritedStyles: PropTypes.string,
  openApplyNowModal: PropTypes.func.isRequired,
  footerLabels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}),
  bagLoading: PropTypes.bool,
  isCartEmpty: PropTypes.bool,
  cartOrderItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  isUserLoggedIn: PropTypes.bool,
  resetTimerStatus: PropTypes.func,
  clickAnalyticsData: PropTypes.func.isRequired,
  isCompleteTheLookTestEnabled: PropTypes.bool,
  showFullPageAuth: PropTypes.bool,
  isFancyBanner: PropTypes.bool,
  isNewLoyaltyBanner: PropTypes.bool,
  isOptmisticAddToBagModal: PropTypes.bool,
  isLoyalityCallCompleted: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  newMiniBag: PropTypes.bool,
};

LoyaltyBannerContainer.defaultProps = {
  thresholdValue: null,
  isGuestCheck: false,
  isPlcc: false,
  currencySymbol: '',
  pageCategory: '',
  isInternationalShipping: false,
  inheritedStyles: '',
  navigation: null,
  bagLoading: false,
  isCartEmpty: false,
  isUserLoggedIn: false,
  resetTimerStatus: () => {},
  isCompleteTheLookTestEnabled: false,
  showFullPageAuth: false,
  isFancyBanner: false,
  isNewLoyaltyBanner: false,
  isOptmisticAddToBagModal: false,
  isLoyalityCallCompleted: true,
  isNewPDPEnabled: false,
  newMiniBag: false,
};

export const mapDispatchToProps = (dispatch) => ({
  openOverlay: (component) => dispatch(openOverlayModal(component)),
  closeAddedToBagModal: () => {
    dispatch(closeAddedToBag());
  },
  openApplyNowModal: (payload) => {
    dispatch(toggleApplyNowModal(payload));
    dispatch(resetPLCCResponse(payload));
  },
  clickAnalyticsData: (payload) => dispatch(setPageCategoryData(payload)),
});

/* istanbul ignore next */
export const mapStateToProps = (state, ownProps) => {
  return {
    labels: getLoyaltyBannerLabels(state),
    orderDetails:
      ownProps.pageCategory === 'confirmation'
        ? confirmationDetails(state)
        : cartOrderDetails(state),
    thresholdValue: getThresholdValue(state),
    isGuestCheck: isGuest(state),
    isPlcc: isPlccUser(state),
    currencySymbol: getCurrencySymbol(state),
    isInternationalShipping: getIsInternationalShipping(state),
    footerLabels: getFooterLabels(state, ownProps.pageCategory),
    bagLoading: BagPageSelector.isBagLoading(state),
    cartOrderItems: BagPageSelector.getOrderItems(state),
    isUserLoggedIn: getUserLoggedInState(state),
    showFullPageAuth: getEnableFullPageAuth(state),
    isRtpsFlow: CheckoutSelectors.getIsRtpsFlow(state),
    isNewLoyaltyBanner: getIsNewLoyaltyBanner(state),
    isNewPDPEnabled: getIsNewPDPEnabled(state),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoyaltyBannerContainer);
