// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { isUsOnly } from '@tcp/core/src/utils';
import { LoyaltyBannerVanilla } from '../LoyaltyBannerView/views/LoyaltyBanner.view';

jest.mock('@tcp/core/src/utils', () => ({
  getViewportInfo: () => ({
    isMobile: true,
  }),
  isClient: () => true,
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isUsOnly: jest.fn(),
  parseBoolean: () => true,
  isCanada: () => false,
  getIconPath: jest.fn(),
  getBrand: () => 'tcp',
  isMobileApp: () => false,
  getSiteId: jest.fn(),
  getStaticFilePath: jest.fn,
}));

describe('LoyaltyBanner View Component', () => {
  let component;
  const Props = {
    className: '',
    labels: {},
    estimatedRewardsVal: 20,
    currentSubtotal: 21,
    estimatedSubtotal: 33,
    thresholdValue: 16.66,
  };

  it('LoyaltyBanner should render correctly', () => {
    component = shallow(<LoyaltyBannerVanilla {...Props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render social connect wrapper for US On PDP', () => {
    isUsOnly.mockImplementation(() => true);
    component.setProps({ isCompleteTheLookTestEnabled: true });
    component.setProps({ pageCategory: 'isProductDetailView' });
    // eslint-disable-next-line sonarjs/no-duplicate-string
    expect(component.find('.social-connect-wrapper')).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });

  it('should not render social connect wrapper for US On PDP', () => {
    isUsOnly.mockImplementation(() => true);
    component.setProps({ isCompleteTheLookTestEnabled: false });
    component.setProps({ pageCategory: 'isProductDetailView' });
    expect(component.find('.social-connect-wrapper')).toHaveLength(0);
    expect(component).toMatchSnapshot();
  });

  it('should not render social connect wrapper for outside US On PDP', () => {
    isUsOnly.mockImplementation(() => false);
    component.setProps({ isCompleteTheLookTestEnabled: false });
    component.setProps({ pageCategory: 'isProductDetailView' });
    expect(component.find('.social-connect-wrapper')).toHaveLength(0);
    expect(component).toMatchSnapshot();
  });

  it('should render not social connect wrapper for pages other than PDP', () => {
    isUsOnly.mockImplementation(() => true);
    component.setProps({ isCompleteTheLookTestEnabled: true });
    component.setProps({ pageCategory: 'bag' });
    // eslint-disable-next-line sonarjs/no-duplicate-string
    expect(component.find('.social-connect-wrapper')).toHaveLength(0);
    expect(component).toMatchSnapshot();
  });
});
