// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const Styles = css`
  .social-connect-wrapper {
    display: flex;
    justify-content: flex-start;
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
`;

export default Styles;
