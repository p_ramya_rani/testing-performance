// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { isUsOnly, getViewportInfo, isClient } from '@tcp/core/src/utils';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import SocialConnect from '@tcp/core/src/components/common/organisms/ProductImages/views/SocialConnect.view';
import withStyles from '../../../../../../common/hoc/withStyles';
import LoyaltyBannerSection from '../../../molecules/LoyaltyBannerSection';
import Styles from '../styles/LoyaltyBanner.style';

class LoyaltyBanner extends React.PureComponent {
  static propTypes = {
    pageCategory: PropTypes.string,
    className: PropTypes.string,
    isInternationalShipping: PropTypes.string,
    accessibilityLabels: PropTypes.shape({}),
    ratingsProductId: PropTypes.string,
    isCompleteTheLookTestEnabled: PropTypes.bool,
    labels: PropTypes.shape({}),
    estimatedRewardsVal: PropTypes.number,
    currentSubtotal: PropTypes.number,
    estimatedSubtotal: PropTypes.number,
    thresholdValue: PropTypes.number,
    isGuest: PropTypes.bool,
    earnedReward: PropTypes.number,
    isPlcc: PropTypes.bool,
    pointsToNextReward: PropTypes.number,
    getCurrencySymbol: PropTypes.func,
    checkThresholdValue: PropTypes.number,
    openOverlay: PropTypes.func,
    closeAddedToBagModal: PropTypes.func,
    inheritedStyles: PropTypes.string,
    openApplyNowModal: PropTypes.func,
    footerLabels: PropTypes.shape({}),
    bagLoading: PropTypes.bool,
    isCartEmpty: PropTypes.bool,
    cartOrderItems: PropTypes.shape([]),
    setClickAnalyticsData: PropTypes.func,
    showFullPageAuth: PropTypes.bool,
    isRtpsFlow: PropTypes.bool,
    isFancyBanner: PropTypes.bool,
    isOptmisticAddToBagModal: PropTypes.bool,
    isLoyalityCallCompleted: PropTypes.bool,
    isNewPDPEnabled: PropTypes.bool,
    newMiniBag: PropTypes.bool,
  };

  static defaultProps = {
    pageCategory: '',
    className: '',
    isInternationalShipping: false,
    accessibilityLabels: {},
    ratingsProductId: '',
    isCompleteTheLookTestEnabled: false,
    labels: {},
    estimatedRewardsVal: 0,
    currentSubtotal: 0,
    estimatedSubtotal: 0,
    thresholdValue: 0,
    isGuest: false,
    earnedReward: 0,
    isPlcc: false,
    pointsToNextReward: 0,
    getCurrencySymbol: () => {},
    checkThresholdValue: 0,
    openOverlay: () => {},
    closeAddedToBagModal: () => {},
    inheritedStyles: '',
    openApplyNowModal: () => {},
    footerLabels: {},
    bagLoading: false,
    isCartEmpty: false,
    cartOrderItems: [],
    setClickAnalyticsData: () => {},
    showFullPageAuth: false,
    isRtpsFlow: false,
    isFancyBanner: false,
    isOptmisticAddToBagModal: false,
    isLoyalityCallCompleted: true,
    isNewPDPEnabled: false,
    newMiniBag: false,
  };

  render() {
    const {
      className,
      labels,
      estimatedRewardsVal,
      currentSubtotal,
      estimatedSubtotal,
      thresholdValue,
      isGuest,
      earnedReward,
      isPlcc,
      pointsToNextReward,
      getCurrencySymbol,
      pageCategory,
      checkThresholdValue,
      isInternationalShipping,
      openOverlay,
      closeAddedToBagModal,
      inheritedStyles,
      openApplyNowModal,
      footerLabels,
      bagLoading,
      isCartEmpty,
      cartOrderItems,
      setClickAnalyticsData,
      accessibilityLabels,
      ratingsProductId,
      isCompleteTheLookTestEnabled,
      showFullPageAuth,
      isRtpsFlow,
      isFancyBanner,
      isOptmisticAddToBagModal,
      isLoyalityCallCompleted,
      isNewPDPEnabled,
      newMiniBag,
    } = this.props;
    const isMobile = isClient() ? getViewportInfo().isMobile : null;
    const showSocialConnect =
      (!isMobile || isCompleteTheLookTestEnabled) && pageCategory === 'isProductDetailView';
    return (
      <>
        {!isInternationalShipping && isUsOnly() ? (
          <div className={className}>
            <LoyaltyBannerSection
              labels={labels}
              estimatedRewardsVal={estimatedRewardsVal}
              currentSubtotal={currentSubtotal}
              estimatedSubtotal={estimatedSubtotal}
              thresholdValue={thresholdValue}
              isGuest={isGuest}
              earnedReward={earnedReward}
              isPlcc={isPlcc}
              pointsToNextReward={pointsToNextReward}
              getCurrencySymbol={getCurrencySymbol}
              pageCategory={pageCategory}
              checkThresholdValue={checkThresholdValue}
              isInternationalShipping={isInternationalShipping}
              openOverlay={openOverlay}
              closeAddedToBagModal={closeAddedToBagModal}
              inheritedStyles={inheritedStyles}
              openApplyNowModal={openApplyNowModal}
              footerLabels={footerLabels}
              bagLoading={bagLoading}
              isCartEmpty={isCartEmpty}
              cartOrderItems={cartOrderItems}
              setClickAnalyticsData={setClickAnalyticsData}
              showFullPageAuth={showFullPageAuth}
              isRtpsFlow={isRtpsFlow}
              isFancyBanner={isFancyBanner && !isPlcc}
              isOptmisticAddToBagModal={isOptmisticAddToBagModal}
              isLoyalityCallCompleted={isLoyalityCallCompleted}
              newMiniBag={newMiniBag}
            />
          </div>
        ) : null}
        {showSocialConnect && !isNewPDPEnabled && isUsOnly() && (
          <div className={className}>
            <SocialConnect
              className="social-connect-wrapper"
              isFacebookEnabled
              isPinterestEnabled
              isTwitterEnabled
              accessibilityLabels={accessibilityLabels}
              ratingsProductId={ratingsProductId}
            />
          </div>
        )}
      </>
    );
  }
}

export default errorBoundary(withStyles(LoyaltyBanner, Styles));
export { LoyaltyBanner as LoyaltyBannerVanilla };
