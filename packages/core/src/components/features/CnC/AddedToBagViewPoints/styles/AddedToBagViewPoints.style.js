// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .text-value {
    text-align: right;
    font-size: ${(props) => (props.isATBModalBackAbTestNewDesign ? '13px' : '14px')};
  }
  .added-to-bag-subtotal {
    font-weight: 700;
  }
  .total-bag-points {
    letter-spacing: ${(props) => (props.isATBModalBackAbTestNewDesign ? 'normal' : '0.09px')};
  }
  .addedtobag-wrapper {
    min-height: 115px;
  }
  .row-padding {
    padding-bottom: 9px;
  }
  .bag-points {
    padding-bottom: 0;
  }
  .divided-line {
    height: 1px;
    background-color: ${(props) =>
      props.isATBModalBackAbTestNewDesign
        ? props.theme.colors.PRIMARY.DARK
        : props.theme.colorPalette.gray[1600]};
    margin-top: ${(props) => (props.isATBModalBackAbTestNewDesign ? '5px' : '0')};
    margin-bottom: ${(props) => (props.isATBModalBackAbTestNewDesign ? '5px' : '9px')};
  }
  .promo-color {
    color: ${(props) => props.theme.colors.PROMO.YELLOW};
  }
  &.added-to-bag-points-wrapper {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default styles;
