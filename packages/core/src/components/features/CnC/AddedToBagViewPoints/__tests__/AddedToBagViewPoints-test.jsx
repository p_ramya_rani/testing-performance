// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AddedToBagViewPointsVanilla } from '../views/AddedToBagViewPoints.view';

describe('AddedToBagViewPoints component', () => {
  const props = {
    className: 'abcd',
    pointsSummary: {
      itemPrice: 12,
      itemPoints: 23,
      subTotal: 34,
      userPoints: 60,
      pointsToNextReward: 12,
      totalItems: 3,
    },
    labels: { bagSubTotal: '' },
    isATBModalBackAbTestNewDesign: true,
  };
  it('renders correctly', () => {
    const component = shallow(<AddedToBagViewPointsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('renders correctly for isATBModalBackAbTestNewDesign', () => {
    const newProps = {
      ...props,
      isATBModalBackAbTestNewDesign: false,
    };
    const component = shallow(<AddedToBagViewPointsVanilla {...newProps} />);
    expect(component).toMatchSnapshot();
  });
});
