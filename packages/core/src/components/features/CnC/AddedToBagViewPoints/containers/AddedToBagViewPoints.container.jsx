// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  isPlccUser,
  getUserLoggedInState,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import AddedToBagViewPoints from '../views/AddedToBagViewPoints.view';
import { getPointsSummary } from '../../AddedToBag/container/AddedToBag.selectors';
import { getIsInternationalShipping } from '../../../../../reduxStore/selectors/session.selectors';

export class AddedToBagViewPointsContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pointsRendered: false,
    };
  }

  componentDidMount = () => {
    const { pointsSummary } = this.props;
    this.setState({
      pointsRendered:
        pointsSummary && Object.keys(pointsSummary).length > 0 && pointsSummary.userPoints > 0,
    });
  };

  componentWillUpdate = () => {
    const { pointsSummary } = this.props;
    const { pointsRendered } = this.state;
    const isPointsSummary = pointsSummary && Object.keys(pointsSummary).length > 0;
    if (!pointsRendered && isPointsSummary && pointsSummary.userPoints > 0) {
      this.setState({
        pointsRendered: true,
      });
    }
  };

  render() {
    const {
      pointsSummary,
      labels,
      isPlcc,
      isInternationalShipping,
      isUserLoggedIn,
      inheritedStyles,
      showPickupBanner,
      isOptmisticAddToBagModal,
      offerPrice,
      isATBModalBackAbTestNewDesign,
    } = this.props;

    // Added this state to determine if points are already rendered to avoid layout shift with update API call
    const { pointsRendered } = this.state;
    return (
      <AddedToBagViewPoints
        labels={labels}
        pointsSummary={pointsSummary}
        isPlcc={isPlcc}
        isUserLoggedIn={isUserLoggedIn}
        isInternationalShipping={isInternationalShipping}
        inheritedStyles={inheritedStyles}
        pointsRendered={pointsRendered}
        showPickupBanner={showPickupBanner}
        isOptmisticAddToBagModal={isOptmisticAddToBagModal}
        offerPrice={offerPrice}
        isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    pointsSummary: getPointsSummary(state),
    isPlcc: isPlccUser(state),
    isUserLoggedIn: getUserLoggedInState(state),
    isInternationalShipping: getIsInternationalShipping(state),
  };
}

AddedToBagViewPointsContainer.propTypes = {
  pointsSummary: PropTypes.shape,
  labels: PropTypes.shape,
  isPlcc: PropTypes.bool.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  inheritedStyles: PropTypes.string,
  isInternationalShipping: PropTypes.bool,
  showPickupBanner: PropTypes.bool,
  isOptmisticAddToBagModal: PropTypes.bool,
  offerPrice: PropTypes.number,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
};

AddedToBagViewPointsContainer.defaultProps = {
  pointsSummary: {},
  labels: {},
  isInternationalShipping: false,
  inheritedStyles: '',
  showPickupBanner: false,
  isOptmisticAddToBagModal: false,
  offerPrice: 0,
  isATBModalBackAbTestNewDesign: false,
};

export default connect(mapStateToProps)(AddedToBagViewPointsContainer);
