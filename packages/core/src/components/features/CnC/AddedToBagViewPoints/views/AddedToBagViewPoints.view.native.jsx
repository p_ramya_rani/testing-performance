// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import PriceCurrency from '@tcp/core/src/components/common/molecules/PriceCurrency';
import ADDEDTOBAG_CONSTANTS from '@tcp/core/src/components/features/CnC/AddedToBag/AddedToBag.constants';
import BodyCopy from '../../../../common/atoms/BodyCopy';
import {
  ViewPointsWrapper,
  DefaultView,
  Horizontal,
} from '../styles/AddedToBagViewPoints.style.native';

const getModifiedString = (labels, totalItems) => {
  return `${labels.bagSubTotal.replace('#items', `${totalItems}`)}`;
};
const showPoints = (userPoints, isBrierleyWorking, pointsApi, pointsRendered, showPickupBanner) => {
  const pointsApiNAFlag = pointsApi && pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.NA;
  return (
    (userPoints !== 0 && isBrierleyWorking && !pointsApiNAFlag) ||
    (pointsRendered && showPickupBanner)
  );
};

const showPointsToNextRewards = (
  isBrierleyWorking,
  pointsApi,
  pointsRendered,
  showPickupBanner
) => {
  const pointsApiNAFlag = pointsApi && pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.NA;
  return (isBrierleyWorking && !pointsApiNAFlag) || (pointsRendered && showPickupBanner);
};
const getPointsColor = isPlcc => {
  if (isPlcc) {
    return 'blue.B100';
  }
  return 'orange.800';
};

const AddedToBagViewPoints = ({
  pointsSummary,
  labels,
  isUserLoggedIn,
  isPlcc,
  pointsRendered,
  showPickupBanner,
}) => {
  const {
    itemPrice,
    itemPoints,
    bagSubTotal,
    userPoints,
    pointsToNextReward,
    isBrierleyWorking,
    totalItems,
    pointsApi,
  } = pointsSummary;
  return (
    <ViewPointsWrapper>
      <DefaultView>
        <BodyCopy fontFamily="secondary" fontSize="fs13" text={labels.price} />
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs13"
          text={<PriceCurrency price={itemPrice} />}
        />
      </DefaultView>
      {showPoints(userPoints, isBrierleyWorking, pointsApi, pointsRendered, showPickupBanner) && (
        <DefaultView>
          <BodyCopy
            fontWeight="extrabold"
            fontFamily="secondary"
            fontSize="fs13"
            text={isUserLoggedIn ? labels.MPRPoints : labels.pointsYouCanEarn}
          />
          <BodyCopy
            color={getPointsColor(isPlcc)}
            fontWeight="extrabold"
            fontFamily="secondary"
            fontSize="fs13"
            text={itemPoints || 0}
          />
        </DefaultView>
      )}
      <Horizontal />
      <DefaultView>
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs13"
          text={getModifiedString(labels, totalItems || 0)}
        />
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs13"
          text={<PriceCurrency price={bagSubTotal} />}
        />
      </DefaultView>
      {showPoints(userPoints, isBrierleyWorking, pointsApi, pointsRendered, showPickupBanner) && (
        <>
          <DefaultView>
            <BodyCopy
              fontWeight="extrabold"
              fontFamily="secondary"
              fontSize="fs13"
              text={labels.totalRewardsInPoints}
            />
            <BodyCopy
              color={getPointsColor(isPlcc)}
              fontWeight="extrabold"
              fontFamily="secondary"
              fontSize="fs13"
              text={userPoints || 0}
            />
          </DefaultView>
        </>
      )}
      {showPointsToNextRewards(isBrierleyWorking, pointsApi, pointsRendered, showPickupBanner) ? (
        <>
          <DefaultView>
            <BodyCopy
              fontWeight="extrabold"
              fontFamily="secondary"
              fontSize="fs13"
              text={labels.totalNextRewards}
            />
            <BodyCopy
              color={getPointsColor(isPlcc)}
              fontWeight="extrabold"
              fontFamily="secondary"
              fontSize="fs13"
              text={pointsToNextReward || 0}
            />
          </DefaultView>
        </>
      ) : null}
    </ViewPointsWrapper>
  );
};

AddedToBagViewPoints.propTypes = {
  pointsSummary: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  showPickupBanner: PropTypes.bool,
  pointsRendered: PropTypes.bool,
};
AddedToBagViewPoints.defaultProps = {
  showPickupBanner: false,
  pointsRendered: false,
};

export default AddedToBagViewPoints;
