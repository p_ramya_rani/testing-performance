// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { isCanada, isClient } from '@tcp/core/src/utils';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import ADDEDTOBAG_CONSTANTS from '@tcp/core/src/components/features/CnC/AddedToBag/AddedToBag.constants';
import Col from '../../../../common/atoms/Col';
import Row from '../../../../common/atoms/Row';
import BodyCopy from '../../../../common/atoms/BodyCopy';
import styles from '../styles/AddedToBagViewPoints.style';
import withStyles from '../../../../common/hoc/withStyles';

const getModifiedString = (labels, totalItems) => {
  const subHeading = `<span>${labels.bagSubTotal.replace(
    '#items',
    `${totalItems !== 0 ? totalItems : ''}`
  )}</span>`;
  return (
    // eslint-disable-next-line react/no-danger
    <span dangerouslySetInnerHTML={{ __html: subHeading }} />
  );
};

const showPoints = (userPoints, isInternationalShipping, pointsApi) => {
  const pointsApiNAFlag = pointsApi && pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.NA;
  return userPoints !== 0 && !isCanada() && !isInternationalShipping && !pointsApiNAFlag;
};

const showPointsToNextRewards = (isInternationalShipping, pointsApi) => {
  const pointsApiNAFlag = pointsApi && pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.NA;
  return !isCanada() && !isInternationalShipping && !pointsApiNAFlag;
};

const getPointsColor = (isPlcc) => {
  if (isPlcc) {
    return 'blue.B100';
  }
  return 'orange.800';
};

const getRewardsPointsLabel = (labels, isUserLoggedIn) => {
  return isUserLoggedIn ? labels.MPRPoints : labels.pointsYouCanEarn;
};

const isPointsAvailable = (pointsSummary, isInternationalShipping) => {
  const { isBrierleyWorking } = pointsSummary;
  const isPointSummary =
    pointsSummary && Object.keys(pointsSummary).length > 0 && pointsSummary.totalItems > 0;
  return !isCanada() && !isInternationalShipping ? isBrierleyWorking : isPointSummary;
};
const calculateItemPrice = (itemPrice, offerPrice, isOptmisticAddToBagModal) => {
  return isOptmisticAddToBagModal ? itemPrice || offerPrice : itemPrice;
};

const getPortalId = (isATBModalBackAbTestNewDesign) => {
  return (
    !isATBModalBackAbTestNewDesign && isClient() && document.getElementById('added-to-bag-portal')
  );
};

const getIsDataFetching = (isOptmisticAddToBagModal, pointsSummary, isInternationalShipping) => {
  return isOptmisticAddToBagModal
    ? isPointsAvailable(pointsSummary, isInternationalShipping)
    : true;
};

const PortalContent = ({
  isShowPoints,
  isATBModalBackAbTestNewDesign,
  isDataFetching,
  itemPoints,
  isPlcc,
  labels,
}) => {
  return isShowPoints ? (
    <Col colSize={{ large: 4, small: isATBModalBackAbTestNewDesign ? 1 : 4, medium: 2 }}>
      <BodyCopy
        fontSize={isATBModalBackAbTestNewDesign ? 'fs13' : 'fs12'}
        data-locator="addedtobag-pointsonitem"
        fontFamily="secondary"
        className="text-value"
        color={getPointsColor(isPlcc)}
        fontWeight="extrabold"
      >
        {isDataFetching ? itemPoints || 0 : '-'}
        {!isATBModalBackAbTestNewDesign && (
          <BodyCopy
            component="span"
            fontSize="fs12"
            fontFamily="secondary"
            className="text-value"
            color="gray.900"
            fontWeight="medium"
          >
            {labels.itemPoints}
          </BodyCopy>
        )}
      </BodyCopy>
    </Col>
  ) : null;
};

const AddedToBagViewPoints = ({
  className,
  pointsSummary,
  labels,
  isPlcc,
  isInternationalShipping,
  isUserLoggedIn,
  isOptmisticAddToBagModal,
  offerPrice,
  isATBModalBackAbTestNewDesign,
}) => {
  const {
    itemPrice,
    itemPoints,
    bagSubTotal,
    userPoints,
    pointsToNextReward,
    totalItems,
    pointsApi,
  } = pointsSummary;
  const portalId = getPortalId(isATBModalBackAbTestNewDesign);
  const isDataFetching = getIsDataFetching(
    isOptmisticAddToBagModal,
    pointsSummary,
    isInternationalShipping
  );
  const itemPriceToDisplay = calculateItemPrice(itemPrice, offerPrice, isOptmisticAddToBagModal);
  const isShowPoints = showPoints(userPoints, isInternationalShipping, pointsApi);

  return (
    <BodyCopy
      fontSize="fs13"
      color="black"
      fontFamily="secondary"
      component="div"
      className={`${className} ${
        isATBModalBackAbTestNewDesign ? '' : 'added-to-bag-points-wrapper'
      }`}
      tabIndex="0"
    >
      <div className={!isCanada() ? 'addedtobag-wrapper' : ''}>
        <Row className="row-padding" tabIndex="0">
          <Col colSize={{ large: 8, small: 4, medium: 6 }}>{labels.price}</Col>
          <Col
            data-locator="addedtobag-productprice"
            className="text-value"
            colSize={{ large: 4, small: 2, medium: 2 }}
          >
            {itemPriceToDisplay ? <PriceCurrency price={itemPriceToDisplay} /> : '-'}
          </Col>
        </Row>

        {portalId
          ? ReactDOM.createPortal(
              // eslint-disable-next-line react/jsx-indent
              <PortalContent
                isShowPoints={isShowPoints}
                isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
                isDataFetching={isDataFetching}
                itemPoints={itemPoints}
                isPlcc={isPlcc}
                labels={labels}
              />,
              portalId
            )
          : null}
        {isATBModalBackAbTestNewDesign &&
          showPoints(userPoints, isInternationalShipping, pointsApi) && (
            <Row className="row-padding bag-points">
              <Col colSize={{ large: 8, small: 5, medium: 6 }}>
                <BodyCopy fontSize="fs13" fontFamily="secondary" fontWeight="extrabold">
                  {getRewardsPointsLabel(labels, isUserLoggedIn)}
                </BodyCopy>
              </Col>
              <PortalContent
                isShowPoints={isShowPoints}
                isATBModalBackAbTestNewDesign={isATBModalBackAbTestNewDesign}
                isDataFetching={isDataFetching}
                itemPoints={itemPoints}
                isPlcc={isPlcc}
                labels={labels}
              />
            </Row>
          )}
        <Row className="divided-line" />
        <Row className="row-padding" tabIndex="0">
          <Col colSize={{ large: 8, small: 4, medium: 6 }}>
            {getModifiedString(labels, totalItems || 0)}
          </Col>
          <Col
            data-locator="addedtobag-bagsubtotal"
            className={`text-value ${isATBModalBackAbTestNewDesign ? '' : 'added-to-bag-subtotal'}`}
            colSize={{ large: 4, small: 2, medium: 2 }}
          >
            {bagSubTotal ? <PriceCurrency price={bagSubTotal} /> : '-'}
          </Col>
        </Row>
        {isShowPoints && (
          <Row className="row-padding" tabIndex="0">
            <Col colSize={{ large: 9, small: 5, medium: 6 }}>
              <BodyCopy
                fontSize="fs13"
                fontFamily="secondary"
                fontWeight="extrabold"
                className="total-bag-points"
              >
                {labels.totalRewardsInPoints}
              </BodyCopy>
            </Col>
            <Col colSize={{ large: 3, small: 1, medium: 2 }}>
              <BodyCopy
                fontSize="fs13"
                data-locator="addedtobag-totalrewardpoints"
                fontFamily="secondary"
                className="text-value total-bag-points-value"
                color={getPointsColor(isPlcc)}
                fontWeight="extrabold"
              >
                {isDataFetching ? userPoints || 0 : '-'}
              </BodyCopy>
            </Col>
          </Row>
        )}
        {showPointsToNextRewards(isInternationalShipping, pointsApi) ? (
          <Row className="row-padding" tabIndex="0">
            <Col colSize={{ large: 8, small: 5, medium: 6 }}>
              <BodyCopy
                fontSize="fs13"
                fontFamily="secondary"
                fontWeight="extrabold"
                className="total-bag-points"
              >
                {labels.totalNextRewards}
              </BodyCopy>
            </Col>
            <Col colSize={{ large: 4, small: 1, medium: 2 }}>
              <BodyCopy
                fontSize="fs13"
                data-locator="addedtobag-totalpointsnextreward"
                fontFamily="secondary"
                className="text-value"
                color={getPointsColor(isPlcc)}
                fontWeight="extrabold"
              >
                {isDataFetching ? pointsToNextReward || 0 : '-'}
              </BodyCopy>
            </Col>
          </Row>
        ) : null}
      </div>
    </BodyCopy>
  );
};

AddedToBagViewPoints.propTypes = {
  className: PropTypes.string.isRequired,
  pointsSummary: PropTypes.shape.isRequired,
  labels: PropTypes.shape.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  isOptmisticAddToBagModal: PropTypes.bool.isRequired,
  offerPrice: PropTypes.number,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
};

AddedToBagViewPoints.defaultProps = {
  isATBModalBackAbTestNewDesign: false,
};

export default withStyles(AddedToBagViewPoints, styles);
export { AddedToBagViewPoints as AddedToBagViewPointsVanilla };
