// 9fbef606107a605d69c0edbcd8029e5d 
import BAGPAGE_CONSTANTS from '../BagPage.constants';

export const setCouponMergeError = payload => {
  return {
    type: BAGPAGE_CONSTANTS.SET_COUPON_MERGE_ERROR,
    payload,
  };
};

export const setMaxItemError = payload => {
  return {
    type: BAGPAGE_CONSTANTS.SET_MAX_ITEM_ERROR,
    payload,
  };
};

export const setMaxItemErrorDisplayed = payload => {
  return {
    type: BAGPAGE_CONSTANTS.SET_MAX_ITEM_ERROR_DISPLAYED,
    payload,
  };
};
