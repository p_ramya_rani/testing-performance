// 9fbef606107a605d69c0edbcd8029e5d 
import { withRouter } from 'next/router'; // eslint-disable-line
import hoistNonReactStatic from 'hoist-non-react-statics';
import BagPageCommonContainer from './BagPageCommonContainer';

const BagPageWithRouter = withRouter(BagPageCommonContainer);

hoistNonReactStatic(BagPageWithRouter, BagPageCommonContainer, {
  getInitialProps: true,
});

export default BagPageWithRouter;
