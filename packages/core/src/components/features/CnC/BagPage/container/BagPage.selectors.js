/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector, defaultMemoize } from 'reselect';
import { List } from 'immutable';
import selectors from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import { getABtestFromState, isMobileApp, isIOS } from '@tcp/core/src/utils';
import Progressbar from '@tcp/core/src/components/features/CnC/common/organism/ProgressBar/container/Progressbar.selectors';
import { getLabelsCartItemTile } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { parseBoolean } from '../../../../../utils/badge.util';
import { getLabelValue } from '../../../../../utils';
import { AVAILABILITY } from '../../../../../services/abstractors/CnC/CartItemTile';
import getErrorList from './Errors.selector';
import BAGPAGE_CONSTANTS from '../BagPage.constants';
import COUPON_CONSTANTS from '../../common/organism/CouponAndPromos/Coupon.constants';

export const filterProductsBrand = (arr, searchedValue) => {
  const obj = [];
  const filterArray = arr.filter((value) => {
    return value.getIn(['productInfo', 'itemBrand']) === searchedValue;
  });
  filterArray.forEach((item) => {
    obj.push(item.getIn(['productInfo', 'productPartNumber']));
  });
  return obj;
};

const allLabels = (state) => state.Labels;

const getBagPageLabels = createSelector(allLabels, (labels) => {
  return {
    addedToBag: getLabelValue(labels, 'lbl_header_addedToBag', 'addedToBagModal', 'checkout'),
    checkout: getLabelValue(labels, 'lbl_cta_checkout', 'addedToBagModal', 'checkout'),
    bagHeading: getLabelValue(labels, 'lbl_header_bag', 'bagPage', 'checkout'),
    loggedInMsg: getLabelValue(labels, 'lbl_emptyBag_loggedInMsg', 'bagPage', 'checkout'),
    login: getLabelValue(labels, 'lbl_emptyBag_loginIn', 'bagPage', 'checkout'),
    shopNow: getLabelValue(labels, 'lbl_emptyBag_shopNow', 'bagPage', 'checkout'),
    tagLine: getLabelValue(labels, 'lbl_emptyBag_inspirationTagLine', 'bagPage', 'checkout'),
    guestUserMsg: getLabelValue(labels, 'lbl_emptyBag_notLoggedInMsg', 'bagPage', 'checkout'),
    helperMsg: getLabelValue(labels, 'lbl_emptyBag_helperMsg', 'bagPage', 'checkout'),
    savedForLaterText: getLabelValue(labels, 'lbl_sfl_savedForLater', 'bagPage', 'checkout'),
    myBagButton: getLabelValue(labels, 'lbl_sfl_myBagButton', 'bagPage', 'checkout'),
    addProductGetPlaceCash: getLabelValue(
      labels,
      'lbl_add_product_toget_placecash',
      'bagPage',
      'checkout'
    ),
    addProductGetPlaceReward: getLabelValue(
      labels,
      'lbl_add_product_toget_placereward',
      'bagPage',
      'checkout'
    ),
    savedLaterButton: getLabelValue(labels, 'lbl_sfl_savedLaterButton', 'bagPage', 'checkout'),
    emptySflMsg1: getLabelValue(labels, 'lbl_sfl_emptySflMsg_1', 'bagPage', 'checkout'),
    emptySflMsg2: getLabelValue(labels, 'lbl_sfl_emptySflMsg_2', 'bagPage', 'checkout'),
    sflSuccess: getLabelValue(labels, 'bl_sfl_actionSuccess', 'bagPage', 'checkout'),
    cartSflMergedSuccessMsg: getLabelValue(
      labels,
      'lbl_sfl_cartSflMergedSuccessMsg',
      'bagPage',
      'checkout'
    ),
    sflDeleteSuccess: getLabelValue(labels, 'lbl_sfl_itemDeleteSuccess', 'bagPage', 'checkout'),
    totalLabel: getLabelValue(labels, 'lbl_orderledger_total', 'bagPage', 'checkout'),
    recentlyViewed: getLabelValue(labels, 'lbl_recently_viewed', 'bagPage', 'checkout'),
    youMayAlsoLike: getLabelValue(labels, 'lbl_you_may_also_like', 'bagPage', 'checkout'),
    youMayAlsoLikeEmptyBagPage: getLabelValue(
      labels,
      'lbl_you_may_also_like_empty_bag_page',
      'bagPage',
      'checkout'
    ),
    recentlyViewedEmptyBagPage: getLabelValue(
      labels,
      'lbl_recently_viewed_empty_bag_page',
      'bagPage',
      'checkout'
    ),
    youMayAlsoLikeFullBagPage: getLabelValue(
      labels,
      'lbl_you_may_also_like_full_bag_page',
      'bagPage',
      'checkout'
    ),
    recentlyViewedFullBagPage: getLabelValue(
      labels,
      'lbl_recently_viewed_full_bag_page',
      'bagPage',
      'checkout'
    ),
    confirmationPage: getLabelValue(labels, 'lbl_confirmation_page', 'bagPage', 'checkout'),
    confirmationPageCheckoutSimilarStyles: getLabelValue(
      labels,
      'lbl_confirmation_page_checkout_similar_styles',
      'bagPage',
      'checkout'
    ),
    confirmationPageRecentlyViewed: getLabelValue(
      labels,
      'lbl_confirmation_page_recently_viewed',
      'bagPage',
      'checkout'
    ),
    confirmationPageOrderNotification: getLabelValue(
      labels,
      'lbl_confirmation_page_order_notification ',
      'bagPage',
      'checkout'
    ),
    applyNow: getLabelValue(labels, 'lbl_emptyBag_applyNow', 'bagPage', 'checkout'),
    sflMovedToBagSuccess: getLabelValue(
      labels,
      'lbl_sfl_itemMoveToBagSuccess',
      'bagPage',
      'checkout'
    ),
    arrivesBy: getLabelValue(labels, 'lbl_edd_arrivesby', 'bagPage', 'checkout'),
    estimatedToArriveBy: getLabelValue(labels, 'lbl_estimate_to_arrive', 'bagPage', 'checkout'),
    maxQuantityError: getLabelValue(labels, 'lbl_coupon_max_quantity_error', 'bagPage', 'checkout'),
    signinCtaBannerSignin: getLabelValue(
      labels,
      'lbl_signin_cta_banner_signin',
      'bagPage',
      'checkout'
    ),
    signinCtaBannerOr: getLabelValue(labels, 'lbl_signin_cta_banner_or', 'bagPage', 'checkout'),
    signinBannerCreateAccount: getLabelValue(
      labels,
      'lbl_signin_cta_banner_create_account',
      'bagPage',
      'checkout'
    ),
  };
});

const getTotalItemsCheck = defaultMemoize((state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'totalItems']) || -1;
});

const getTotalItems = defaultMemoize((state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'totalItems']) || 0;
});

const getOrderItems = defaultMemoize((state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'orderItems']) || 0;
});

const getOrderItemsLocal = (state) => {
  return state.CartPageReducer.get('localCartOrderItems') || [];
};

const getOrderSubTotal = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'subTotal']) || 0;
};

const getItemIds = (orderItems) => {
  const ids = [];
  orderItems.forEach((item) => {
    ids.push(item.getIn(['productInfo', 'productPartNumber']));
  });

  return ids.filter(Boolean).join(',');
};

const getCartProductIds = (state) => {
  const cartItems = getOrderItems(state);
  return cartItems ? getItemIds(cartItems) : '';
};

const getItemPrice = (item) => {
  return item.getIn(['itemInfo', 'offerPrice']);
};
const getItemPartNumber = (item) => {
  return item.getIn(['productInfo', 'productPartNumber']);
};

const getItemSku = (item) => {
  return item.getIn(['productInfo', 'skuId']);
};

const getItemUPC = (item) => {
  return item.getIn(['productInfo', 'upc'], '');
};

const getItemQuantity = (item) => {
  return item.getIn(['itemInfo', 'quantity']);
};

const getMostExpensiveItem = (cartItems) => {
  const expensiveItem = cartItems.maxBy((item) => getItemPrice(item));
  return expensiveItem ? expensiveItem.getIn(['productInfo', 'productPartNumber']) : '';
};

const getMostExpensiveItemId = (state) => {
  const cartItems = getOrderItems(state);
  return cartItems ? getMostExpensiveItem(cartItems) : '';
};

const getAfterPayId = (state) => {
  const paymentLists =
    state.Confirmation && state.Confirmation.get('orderConfirmation').paymentsList;
  return paymentLists && paymentLists.find((payment) => payment.afterpay);
};

const getBillingPaymentMethod = (state) => {
  const appliedGiftCards = state.CartPageReducer.getIn(['orderDetails', 'appliedGiftCards']);
  const isGiftCardUsed = appliedGiftCards && appliedGiftCards.size > 0;
  const paymentMethod = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'billing',
    'paymentMethod',
  ]);
  if (paymentMethod !== 'creditCard' && isGiftCardUsed) return 'gc';

  const paymentType = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'billing',
    'billing',
    'cardType',
  ]);

  const afterPayPayment = getAfterPayId(state);
  if (afterPayPayment && afterPayPayment.afterpay) {
    return isGiftCardUsed ? 'Afterpay, gc' : 'Afterpay';
  }

  return isGiftCardUsed ? `${paymentType}, gc` : paymentType;
};

const getIsPayPalEnabled = (state) => {
  if (isMobileApp() && isIOS()) {
    return false;
  }
  return parseBoolean(state.CartPageReducer.getIn(['uiFlags', 'isPayPalEnabled']));
};

const getIsPayPalLibraryRendered = (state) => {
  return parseBoolean(state.CartPageReducer.getIn(['uiFlags', 'isPayPalLibraryRenderDone']));
};

const getIsBagPageLoading = (state) => {
  return parseBoolean(state.CartPageReducer.getIn(['uiFlags', 'bagPageLoadingStatus'])) || false;
};

const getShowMaxItemError = (state) => {
  return parseBoolean(state.CartPageReducer.getIn(['uiFlags', 'maxItemError']));
};

const getShowMaxItemErrorDisplayed = (state) => {
  return parseBoolean(state.CartPageReducer.getIn(['uiFlags', 'maxItemErrorDisplayed']));
};

const getShowCouponMergeError = (state) => {
  return parseBoolean(state.CartPageReducer.getIn(['uiFlags', 'couponMergeError']));
};

const getAddressIds = (state) => {
  return {
    previousId: state.CartPageReducer.getIn(['uiFlags', 'addressIds', 'previousId']),
    currentId: state.CartPageReducer.getIn(['uiFlags', 'addressIds', 'currentId']),
  };
};

export const getConfirmationModalFlag = (state) => {
  return {
    showModal: state.CartPageReducer.get('showConfirmationModal'),
    isEditingItem: state.CartPageReducer.get('isEditingItem'),
    fromApplePayBillingPage: state.CartPageReducer.get('fromApplePayBillingPage'),
  };
};

const getErrorMapping = (state) => {
  return getErrorList(state);
};

const getProductsTypes = createSelector(getOrderItems, (orderItems) => {
  let tcpProducts = [];
  let gymProducts = [];
  let snjProducts = [];
  if (orderItems) {
    tcpProducts = filterProductsBrand(orderItems, 'TCP');
    gymProducts = filterProductsBrand(orderItems, 'GYM');
    snjProducts = filterProductsBrand(orderItems, 'SNJ');
  }
  return {
    tcpProducts,
    gymProducts,
    snjProducts,
  };
});

const getNeedHelpContent = (state) => {
  const referred = getLabelValue(state.Labels, 'referred', 'addedToBagModal', 'global', true);
  return referred && referred.length && referred.find((label) => label.name === 'NEED_HELP_DATA');
};

const getPlaceCashContent = (state) => {
  const referred = getLabelValue(state.Labels, 'referred', 'addedToBagModal', 'global', true);
  return referred && referred.length && referred.find((label) => label.name === 'PLACE_CASH_DATA');
};

const getDetailsContentTcpId = (state) => {
  const referred = getLabelValue(state.Labels, 'referred', 'shipping', 'checkout', true);
  const content =
    referred &&
    referred.length &&
    referred.find((label) => label.name === 'GiftServicesDetailsTCPModal');
  return content && content.contentId;
};

const getExitCheckoutAriaLabel = (state) =>
  getLabelValue(state.Labels, 'exit_checkout', 'accessibility', 'global');

const getDetailsContentGymId = (state) => {
  const referred = getLabelValue(state.Labels, 'referred', 'shipping', 'checkout', true);
  const content =
    referred &&
    referred.length &&
    referred.find((label) => label.name === 'GiftServicesDetailsGYMModal');
  return content && content.contentId;
};

const getGiftServicesContentTcpInfo = (state) => {
  const referred = getLabelValue(state.Labels, 'referred', 'shipping', 'checkout', true);
  return (
    referred &&
    referred.length &&
    referred.find((label) => label.name === 'GiftServicesDetailsTCPModal')
  );
};

const getGiftServicesContentGymInfo = (state) => {
  const referred = getLabelValue(state.Labels, 'referred', 'shipping', 'checkout', true);
  return (
    referred &&
    referred.length &&
    referred.find((label) => label.name === 'GiftServicesDetailsGYMModal')
  );
};

const getFilteredItems = (state, filter) => {
  const orderItems = getOrderItems(state);
  return (
    (orderItems &&
      orderItems.size &&
      orderItems.filter((item) => filter(item.getIn(['miscInfo', 'availability'])))) ||
    List()
  );
};

const getUnqualifiedItems = (state) => getFilteredItems(state, (type) => type !== AVAILABILITY.OK);

const getUnqualifiedCount = (state) => getUnqualifiedItems(state).size;
const getUnqualifiedItemsIds = (state) =>
  getUnqualifiedItems(state).map((item) => item.getIn(['itemInfo', 'itemId']));

const getUnavailableCount = (state) =>
  getFilteredItems(state, (type) => type === AVAILABILITY.UNAVAILABLE).size;

const getCurrentOrderId = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'orderId']) || 0;
};

const getOOSCount = (state) =>
  getFilteredItems(state, (type) => type === AVAILABILITY.SOLDOUT).size;

const returnCurrentCurrency = (currency) => {
  return currency === 'USD' || currency === 'CAD' ? '$' : currency;
};
const getCurrentCurrency = (state) => {
  const currency = state.session && state.session.siteDetails.currency;
  return currency ? returnCurrentCurrency(currency) : '$';
};

const getCartStores = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'stores']);
};

const getCartStoresToJs = createSelector(
  getCartStores,
  (store) => store && JSON.parse(JSON.stringify(store))
);

const getsflItemsList = (state) => {
  return state.CartPageReducer.get('sfl');
};

const getCartSflMerged = (state) => {
  return state.CartPageReducer.get('cartSflMerged');
};

/** @function checkoutIfItemIsUnqualified to check if item is Unavailable
 * @param {object} state
 * @param {string|number} itemId
 */
const checkoutIfItemIsUnqualified = (state, itemId) => {
  const items = getOrderItems(state);
  const indexValue = items.findIndex(
    (item) =>
      item.getIn(['itemInfo', 'itemId']) === itemId.toString() &&
      item.getIn(['miscInfo', 'availability']) !== AVAILABILITY.OK
  );
  return indexValue >= 0;
};

/** @function getCurrentDeleteSelectedItemInfo to get confirmation modal info
 * @param {object} state
 */
export const getCurrentDeleteSelectedItemInfo = (state) => {
  return state.CartPageReducer.get('openItemDeleteConfirmationModalInfo');
};

/** @function itemDeleteModalLabels to get item delete confirmation modal info
 * @param {object} state
 */
const itemDeleteModalLabels = (state) => {
  const getBagLabelByLabelName = (labelName) =>
    getLabelValue(state.Labels, labelName, 'bagPage', 'checkout');
  return {
    modalTitle: getBagLabelByLabelName('lbl_itemDelete_modalTitle'),
    modalHeading: getBagLabelByLabelName('lbl_itemDelete_modalHeading'),
    modalButtonSFL: getBagLabelByLabelName('lbl_itemDelete_modalButtonSFL'),
    modalButtonConfirmDelete: getBagLabelByLabelName('lbl_itemDelete_modalButtonConfirmDelete'),
  };
};

const getPayPalWebViewStatus = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'isPayPalWebViewEnable']) || false;
};

const isPayPalButtonRenderDone = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'isPayPalRenderDone']) || false;
};

const getIsPayPalModalActionOOS = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'isPayPalModalActionOOS']) || false;
};

const getIsApplePayModalActionOOS = (state) => {
  return state.CartPageReducer.getIn(['uiFlags', 'isApplePayModalActionOOS']) || false;
};

const isBagLoaded = (state) => {
  return state.CartPageReducer.getIn(['loaded']);
};
const getBagStickyHeaderInterval = (state) => {
  return (
    parseInt(state.session.siteDetails.BAG_CONDENSE_HEADER_INTERVAL, 10) ||
    BAGPAGE_CONSTANTS.BAG_PAGE_STICKY_HEADER_INTERVAL
  );
};

const getIsPayPalHidden = (state) => {
  return state.CartPageReducer.getIn(['paypalBtnHidden']);
};

const isBagLoading = (state) => {
  return state.CartPageReducer.getIn(['bagLoading']);
};

const isBagRouting = (state) => {
  return state.CartPageReducer.get('isRouting');
};

const getCartLoadedState = (state) => {
  return state.CartPageReducer.get('loaded');
};

const getIfEmailSignUpDone = (state) => {
  return {
    emailSignUpTCP: state.CartPageReducer.getIn(['orderDetails', 'emailSignUpTCP']),
    emailSignUpGYM: state.CartPageReducer.getIn(['orderDetails', 'emailSignUpGYM']),
  };
};

const getOptimiseAddProductOrderDetailsEnabled = (state) => {
  return parseInt(state.session.siteDetails.OPTIMISE_ADDPRODUCT_ORDERDETAILS_ENABLED, 10) || 0;
};

export const getCartModuleXList = (state) => {
  return state.CartPageReducer.get('moduleXContent');
};

const getCouponCachingTTL = (state) => {
  return +(
    (state.session && state.session.siteDetails && state.session.siteDetails.COUPON_CACHING_TTL) ||
    COUPON_CONSTANTS.GET_COUPON_LIST_TTL
  );
};

const getBossStatus = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'isBossOrder']);
};

const getBopisStatus = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'isBopisOrder']);
};

const getECOMStatus = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'isECOM']);
};

const getShippingAddressState = (state) => {
  return (
    state &&
    state.CartPageReducer.getIn(['orderDetails', 'checkout', 'shipping', 'address', 'state'])
  );
};

const getShippingAddressZip = (state) => {
  return (
    state &&
    state.CartPageReducer.getIn(['orderDetails', 'checkout', 'shipping', 'address', 'zipCode'])
  );
};
const getShippingAddressEmail = (state) => {
  return (
    state && state.CartPageReducer.getIn(['orderDetails', 'checkout', 'shipping', 'emailAddress'])
  );
};

const getAddressLines = (addressField1, addressField2) => {
  const addressLines = [];
  if (addressField1) addressLines.push(addressField1);
  if (addressField2) addressLines.push(addressField2);
  return addressLines;
};
const getAvoidFewStates = (state) => {
  return state === 'AK' || state === 'PR' || state === 'HI';
};

const getCompleteShippingAddress = (state) => {
  if (!state) return {};

  const administrativeArea = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'address',
    'state',
  ]);

  if (getAvoidFewStates(administrativeArea)) return {};

  const givenName = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'address',
    'firstName',
  ]);
  const familyName = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'address',
    'lastName',
  ]);
  const addressField1 = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'address',
    'addressLine1',
  ]);
  const addressField2 = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'address',
    'addressLine2',
  ]);
  const locality = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'address',
    'city',
  ]);
  const countryCode = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'address',
    'country',
  ]);

  const postalCode = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'address',
    'zipCode',
  ]);
  const phoneNumber = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'phoneNumber',
  ]);

  const addressId = state.CartPageReducer.getIn([
    'orderDetails',
    'checkout',
    'shipping',
    'onFileAddressId',
  ]);

  return {
    givenName,
    familyName,
    addressLines: getAddressLines(addressField1, addressField2),
    locality,
    countryCode,
    administrativeArea,
    postalCode,
    phoneNumber,
    addressId,
  };
};

const getApplePayShipmentAddress = (defaultAddress = {}) => {
  const {
    addressLine,
    state,
    zipCode,
    country,
    city,
    firstName,
    lastName,
    phone1,
    email1,
    addressId,
  } = defaultAddress;
  const addressLine1 = addressLine && addressLine.length > 0 && addressLine[0];
  const addressLine2 = addressLine && addressLine.length > 1 && addressLine[1];
  if (getAvoidFewStates(state)) return {};
  return {
    addressLines: getAddressLines(addressLine1, addressLine2),
    countryCode: country,
    administrativeArea: state,
    postalCode: zipCode,
    locality: city,
    familyName: lastName,
    givenName: firstName,
    phoneNumber: phone1,
    emailAddress: email1,
    addressId,
  };
};

const getShippingContactForApplePay = (state) => {
  const cartAddress = getCompleteShippingAddress(state);
  const emailAddress = getShippingAddressEmail(state);
  cartAddress.emailAddress = emailAddress;
  if (cartAddress && (cartAddress.postalCode || cartAddress.administrativeArea)) {
    return cartAddress;
  }
  const shippingAddressBook = selectors.getShippingAddressList(state);
  const defaultAddress =
    shippingAddressBook && shippingAddressBook.find((node) => node && node.primary === 'true');
  return getApplePayShipmentAddress(defaultAddress);
};
const getShippingModeID = (state) => {
  return state && state.CartPageReducer.getIn(['orderDetails', 'shipModeId']);
};

const getIsGettingPaypalSettings = (state) => {
  return parseBoolean(state.CartPageReducer.getIn(['uiFlags', 'isGettingPaypalSettings']));
};

const getLoyaltyPointsRequestPayload = (state) => {
  return state.CartPageReducer.getIn(['orderDetails', 'loyaltyPointsRequestPayload']);
};

export const getRawCartItems = (state) => {
  const lpPayload = getLoyaltyPointsRequestPayload(state);
  return (lpPayload && JSON.parse(JSON.stringify(lpPayload.get('loyaltyOrderItems')))) || [];
};

export const getCartLoyaltyPointsCount = (state) => {
  const lpPayload = getRawCartItems(state);
  let lpCount = 0;
  lpPayload.forEach((lp) => {
    lpCount += lp.itemPoints;
  });
  return lpCount;
};

const getOrderType = (state) => state.CartPageReducer.getIn(['orderDetails', 'orderType']);
export const getCartBackAbTest = ({ AbTest }) => {
  const AbTests = getABtestFromState(AbTest);
  return AbTests && AbTests.isCartBackAbTestEnabled;
};

export const getSignInBannerAbTest = ({ AbTest }) => {
  const AbTests = getABtestFromState(AbTest);
  return AbTests && AbTests.isSignInBannerAbTestEnabled;
};

const getCartSupressOnBagUpdate = (state) => {
  return state.CartPageReducer.get('isCartSupressOnBagUpdate');
};

const getIsPlaceCashEnabled = (state) => {
  const amt = Progressbar.getAmountToNextReward(state);
  let isPlaceCashEnabled = false;
  const abtest = getABtestFromState(state.AbTest).PlaceCash;
  if (abtest && abtest.isPlaceCashEnabled && amt <= abtest.NextPlaceCash) isPlaceCashEnabled = true;
  return isPlaceCashEnabled;
};

const getProductTileLabels = createSelector(
  getLabelsCartItemTile,
  getBagPageLabels,
  (cartTileLabels, bagPageLabels) => {
    return { ...cartTileLabels, ...bagPageLabels };
  }
);

const getIsPlaceRewardEnabled = (state) => {
  const amt = Progressbar.getPointsToNextReward(state);
  let isPlaceRewardEnabled = false;
  const abtest = getABtestFromState(state.AbTest).PlaceReward;
  if (abtest && abtest.isPlaceRewardEnabled && amt <= abtest.NextPlaceReward)
    isPlaceRewardEnabled = true;
  return isPlaceRewardEnabled;
};

export default {
  getBagPageLabels,
  getCartModuleXList,
  getTotalItems,
  getTotalItemsCheck,
  getOrderItems,
  getOrderItemsLocal,
  getProductsTypes,
  getNeedHelpContent,
  getPlaceCashContent,
  getUnqualifiedCount,
  getUnqualifiedItemsIds,
  getUnavailableCount,
  getOOSCount,
  getConfirmationModalFlag,
  getFilteredItems,
  getCurrentOrderId,
  getErrorMapping,
  getDetailsContentGymId,
  getDetailsContentTcpId,
  getGiftServicesContentTcpInfo,
  getGiftServicesContentGymInfo,
  getCurrentCurrency,
  getCartStores,
  isBagLoaded,
  getCartStoresToJs,
  getsflItemsList,
  getCartSflMerged,
  checkoutIfItemIsUnqualified,
  getCurrentDeleteSelectedItemInfo,
  itemDeleteModalLabels,
  getIsPayPalEnabled,
  getBagStickyHeaderInterval,
  getPayPalWebViewStatus,
  isPayPalButtonRenderDone,
  getIsPayPalHidden,
  getIsPayPalLibraryRendered,
  isBagLoading,
  getCartLoadedState,
  isBagRouting,
  getIfEmailSignUpDone,
  getExitCheckoutAriaLabel,
  getOrderSubTotal,
  getBillingPaymentMethod,
  getOptimiseAddProductOrderDetailsEnabled,
  getCartProductIds,
  getMostExpensiveItemId,
  getCouponCachingTTL,
  getBossStatus,
  getBopisStatus,
  getECOMStatus,
  getAddressIds,
  getIsPayPalModalActionOOS,
  getIsApplePayModalActionOOS,
  getShippingAddressState,
  getShippingAddressZip,
  getCompleteShippingAddress,
  getShippingAddressEmail,
  getShippingContactForApplePay,
  getShippingModeID,
  getIsGettingPaypalSettings,
  getItemPartNumber,
  getItemSku,
  getItemQuantity,
  getItemPrice,
  getItemUPC,
  getOrderType,
  getCartBackAbTest,
  getSignInBannerAbTest,
  getCartSupressOnBagUpdate,
  getIsPlaceCashEnabled,
  getShowMaxItemErrorDisplayed,
  getIsPlaceRewardEnabled,
  getProductTileLabels,
  getShowMaxItemError,
  getShowCouponMergeError,
  getIsBagPageLoading,
};
