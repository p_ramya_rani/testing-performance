/* eslint-disable max-lines */
/* eslint-disable max-statements */
/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d
import { List } from 'immutable';
import { call, takeLatest, put, all, select, delay, take, actionChannel } from 'redux-saga/effects';
import {
  setLoaderState,
  setSectionLoaderState,
} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import CheckoutSelectors from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import {
  getIsBagCarouselEnabled,
  getFlipSwitchState,
  getUnbxdDRUrlState,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { setBagCarouselModuleData } from '@tcp/core/src/components/common/molecules/BagCarouselModule/container/BagCarouselModule.actions';
import { getLoyaltyDetail } from '@tcp/core/src/components/features/CnC/LoyaltyBanner/container/Loyalty.actions';
import {
  isCouponsFetchRequired,
  updateCouponsTimeout,
} from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.saga';
import { updateRecommendationsLoading } from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.actions';
import { isOptimizedDeleteEnabled } from '@tcp/core/src/utils/utils';
import BAGPAGE_CONSTANTS from '../BagPage.constants';
import CONSTANTS, { CHECKOUT_ROUTES, RECALCULATE_CACHE } from '../../Checkout/Checkout.constants';
import utility from '../../Checkout/util/utility';
import {
  getOrderDetailsData,
  getCartData,
  getUnqualifiedItems,
  removeItem,
  getProductInfoForTranslationData,
  startPaypalCheckoutAPI,
  paypalAuthorizationAPI,
  getServerErrorMessage,
} from '../../../../../services/abstractors/CnC';

import BAG_PAGE_ACTIONS, { updateMiniBagFlag, setBagLoadingStatus } from './BagPage.actions';
import CHECKOUT_ACTIONS, {
  checkoutSetCartData,
  getSetIsPaypalPaymentSettings,
  getSetCheckoutStage,
  toggleCheckoutRouting,
  setVenmoPaymentInProgress,
  setApplePaymentInProgress,
  setAfterPayInProgress,
} from '../../Checkout/container/Checkout.action';
import BAG_SELECTORS, { getCartModuleXList, getRawCartItems } from './BagPage.selectors';
import { getModuleX } from '../../../../../services/abstractors/common/moduleXComposite';
import {
  getUserLoggedInState,
  getIsRegisteredUserCallDone,
} from '../../../account/User/container/User.selectors';
import { setCheckoutModalMountedState } from '../../../account/LoginPage/container/LoginPage.actions';
import checkoutSelectors, {
  isRemembered,
  isExpressCheckout,
} from '../../Checkout/container/Checkout.selector';
import { isMobileApp, isCanada, routerPush } from '../../../../../utils';

import {
  addItemToSflList,
  getSflItems,
  updateSflItem,
} from '../../../../../services/abstractors/CnC/SaveForLater';
import {
  removeCartItem,
  setCartLoadingState,
} from '../../CartItemTile/container/CartItemTile.actions';
import { imageGenerator } from '../../../../../services/abstractors/CnC/CartItemTile';
import { getCartOrderId } from '../../CartItemTile/container/CartItemTile.selectors';
import {
  getIsInternationalShipping,
  getIsRadialInventoryEnabled,
  getRecalcOrderPointsInterval,
  getCurrentSiteLanguage,
  getIsPersistPointsApiEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import {
  closeMiniBag,
  updateCartManually,
} from '../../../../common/organisms/Header/container/Header.actions';
import getBopisInventoryDetails from '../../../../../services/abstractors/common/bopisInventory/bopisInventory';
import { filterBopisProducts, updateBopisInventory } from '../../CartItemTile/utils/utils';
import { getUserInfoSaga } from '../../../account/User/container/User.saga';
// eslint-disable-next-line
import {
  handleServerSideErrorAPI,
  getRouteToCheckoutStage,
} from '../../Checkout/container/Checkout.saga.util';
import startSflItemDelete, { routeForAppCartCheckout } from './BagPage.saga.util';
// eslint-disable-next-line import/no-cycle
import { addToCartEcom } from '../../AddedToBag/container/AddedToBag.saga';
import { setAddedToBagDataLocal } from '../../AddedToBag/container/AddedToBag.actions';
import ADDEDTOBAG_CONSTANTS from '../../AddedToBag/AddedToBag.constants';

const isOptimizedDelete = isOptimizedDeleteEnabled();
const { getOrderPointsRecalcFlag } = utility;
const { getCurrentCheckoutStage } = CheckoutSelectors;

export const filterProductsBrand = (arr, searchedValue) => {
  const obj = [];
  const filterArray = arr.filter((value) => {
    return value.productInfo.itemBrand === searchedValue;
  });
  filterArray.forEach((item) => {
    obj.push(item.productInfo.productPartNumber);
  });
  return obj;
};

const getProductsTypes = (orderItems) => {
  let tcpProducts = [];
  let gymProducts = [];
  if (orderItems) {
    tcpProducts = filterProductsBrand(orderItems, 'TCP');
    gymProducts = filterProductsBrand(orderItems, 'GYM');
  }
  return { tcpProducts, gymProducts };
};

export function* getUnbxdXappConfigs() {
  const isFlipURL = yield select(getFlipSwitchState);
  const drURLpath = yield select(getUnbxdDRUrlState);
  return {
    isFlipURL,
    drURLpath,
  };
}

export function* getTranslatedProductInfo(cartInfo) {
  let tcpProductsResults;
  let gymProductsResults;
  const xappURLConfig = yield call(getUnbxdXappConfigs);
  try {
    const productypes = getProductsTypes(
      (cartInfo.orderDetails && cartInfo.orderDetails.orderItems) || cartInfo
    );
    const gymProdpartNumberList = productypes.gymProducts;
    const tcpProdpartNumberList = productypes.tcpProducts;
    if (tcpProdpartNumberList.length) {
      tcpProductsResults = yield call(
        getProductInfoForTranslationData,
        tcpProdpartNumberList.join(),
        'TCP',
        xappURLConfig
      );
    }
    if (gymProdpartNumberList.length) {
      gymProductsResults = yield call(
        getProductInfoForTranslationData,
        gymProdpartNumberList.join(),
        'GYM',
        xappURLConfig
      );
    }
    gymProductsResults = (gymProductsResults && gymProductsResults.body.response.products) || [];
    tcpProductsResults = (tcpProductsResults && tcpProductsResults.body.response.products) || [];

    return [...gymProductsResults, ...tcpProductsResults];
  } catch (err) {
    return [];
  }
}

function createMatchObject(res, translatedProductInfo) {
  res.forEach((orderItemInfo) => {
    const orderItem = orderItemInfo;
    translatedProductInfo.forEach((item) => {
      if (orderItem.productInfo.productPartNumber === item.prodpartno) {
        orderItem.productInfo.name = item.product_name;
        orderItem.productInfo.color.name = item.TCPColor;
      }
    });
  });
}

export function* shouldTranslate(translation) {
  const currentLanguage = yield select(getCurrentSiteLanguage);
  const allowLanguageTranslation = currentLanguage !== 'en';
  return translation && allowLanguageTranslation;
}

const isLoyaltyServiceEnabled = (res, isLoyaltyPointsEnabled) => {
  if (isLoyaltyPointsEnabled) return true;
  return (
    res &&
    res.orderDetails &&
    !res.orderDetails.isBrierleyWorking &&
    res.orderDetails.pointsApi &&
    (res.orderDetails.pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.LOYALTY ||
      res.orderDetails.pointsApi.toUpperCase() === ADDEDTOBAG_CONSTANTS.BRIERLEY)
  );
};

export function* getOrderDetailSaga(payload) {
  const { payload: { after, recalcRewards = false, isRecalc } = {} } = payload;
  try {
    yield put(updateCartManually(true));
    yield put(BAG_PAGE_ACTIONS.setBagPageLoading());
    const isOptimiseAddProductOrderDetailsEnabled = yield select(
      BAG_SELECTORS.getOptimiseAddProductOrderDetailsEnabled
    );
    const isECOM = yield select(BAG_SELECTORS.getECOMStatus);
    const isCalc = isECOM ? isRecalc : !isOptimiseAddProductOrderDetailsEnabled;
    const isRadialInvEnabled = yield select(getIsRadialInventoryEnabled);
    const recalcOrderPointsInterval = yield select(getRecalcOrderPointsInterval);
    const isRecalcRewards = getOrderPointsRecalcFlag(
      recalcRewards,
      recalcOrderPointsInterval,
      RECALCULATE_CACHE.ORDER_DETAILS
    );
    const res = yield call(getOrderDetailsData, isCalc, isRadialInvEnabled, isRecalcRewards);
    yield put(BAG_PAGE_ACTIONS.updatePayPalStatus(res));
    if (yield call(shouldTranslate, true)) {
      const translatedProductInfo = yield call(getTranslatedProductInfo, res);
      if (!translatedProductInfo.error) {
        createMatchObject(res.orderDetails.orderItems, translatedProductInfo);
      }
    }
    yield put(BAG_PAGE_ACTIONS.getOrderDetailsComplete(res.orderDetails));
    const isLoyaltyPointsEnabled = yield select(getIsPersistPointsApiEnabled);

    // Call Loyalty Points API when Brierley service is down And Get flag 'loyaltyCallEnabled' from cart Api
    if (isLoyaltyServiceEnabled(res, isLoyaltyPointsEnabled)) {
      yield put(
        getLoyaltyDetail({
          ...res.orderDetails.loyaltyPointsRequestPayload,
          source: 'Cart',
          pointsApiCall: res.orderDetails.pointsApi,
        })
      );
    }

    if (isMobileApp()) {
      const isBagCarouselEnabled = yield select(getIsBagCarouselEnabled);
      if (isBagCarouselEnabled) {
        yield put(
          setBagCarouselModuleData({
            ...res.orderDetails,
          })
        );
      }
    }
    yield put(toggleCheckoutRouting(true));
    if (after) {
      yield call(after);
    }
  } catch (err) {
    yield put(BAG_PAGE_ACTIONS.setBagPageError(err));
  }
}

export function* updateBopisItems(res, isCartPage) {
  const bopisItems = filterBopisProducts(res.orderDetails.orderItems);
  if (bopisItems.length && isCartPage) {
    const bopisInventoryResponse = yield call(getBopisInventoryDetails, bopisItems);
    res.orderDetails = {
      ...res.orderDetails,
      orderItems: updateBopisInventory(res.orderDetails.orderItems, bopisInventoryResponse),
    };
  }
}

export function* setBagPageLoading(fromMoveTobag, ignoreCartLoader) {
  if (!fromMoveTobag && !ignoreCartLoader) {
    yield put(BAG_PAGE_ACTIONS.setBagPageLoading());
  }
}

export function* handelAddressIds(orderDetails, addressIds) {
  if (orderDetails && orderDetails.checkout && orderDetails.checkout.shipping) {
    const onFileAddressId = Number(orderDetails.checkout.shipping.onFileAddressId) || 0;
    const updatedIds = { ...addressIds };
    if (addressIds.previousId === 0 && addressIds.currentId !== 0) {
      updatedIds.previousId = 0;
      updatedIds.currentId = onFileAddressId;
      yield put(BAG_PAGE_ACTIONS.updateAddressIds(updatedIds));
    } else if (addressIds.currentId !== onFileAddressId) {
      updatedIds.previousId = addressIds.currentId;
      updatedIds.currentId = onFileAddressId;
      yield put(BAG_PAGE_ACTIONS.updateAddressIds(updatedIds));
    }
  }
}

export function* setBagpageCarousel(res) {
  const isBagCarouselEnabled = yield select(getIsBagCarouselEnabled);
  if (isBagCarouselEnabled) {
    yield put(
      setBagCarouselModuleData({
        ...res.orderDetails,
      })
    );
  }
}

function getExcludeCartItems(excludeCartItems, cartTotalCount, rawCartItems = []) {
  const totalCount = Array.isArray(rawCartItems)
    ? rawCartItems.reduce((count, item) => {
        return item && item.qty ? item.qty + count : count;
      }, 0)
    : 0;
  return excludeCartItems && cartTotalCount === totalCount;
}

// eslint-disable-next-line sonarjs/cognitive-complexity
export function* getCartDataSaga(payload = {}) {
  try {
    yield put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }));
    const {
      payload: {
        isRecalculateTaxes,
        isCheckoutFlow,
        isCartPage,
        fromMoveTobag,
        ignoreCartLoader,
      } = {},
    } = payload;
    const { payload: { onCartRes, recalcRewards, translation = false, isLoggedIn } = {} } = payload;
    const { payload: { isCartNotRequired, updateSmsInfo, excludeCartItems, supressCoupons } = {} } =
      payload;
    yield put(setBagLoadingStatus(true));
    const recalcOrderPointsInterval = yield select(getRecalcOrderPointsInterval);
    const addressIds = yield select(BAG_SELECTORS.getAddressIds);
    const recalcOrderPoints = getOrderPointsRecalcFlag(
      recalcRewards,
      recalcOrderPointsInterval,
      RECALCULATE_CACHE.FULL_DETAILS
    );
    const rawCartItems = excludeCartItems ? yield select(getRawCartItems) : [];
    const isRadialInvEnabled = yield select(getIsRadialInventoryEnabled);
    const cartTotalCount = yield select(BAG_SELECTORS.getTotalItems);
    const cartProps = {
      excludeCartItems: getExcludeCartItems(excludeCartItems, cartTotalCount, rawCartItems),
      recalcRewards: recalcOrderPoints,
      isRadialInvEnabled,
      supressCoupons,
      rawCartItems,
    };
    const checkoutStage = yield select(getCurrentCheckoutStage);
    if (isMobileApp()) {
      if (!checkoutStage || checkoutStage === 'Shipping' || checkoutStage === 'Billing') {
        yield setBagPageLoading(fromMoveTobag, ignoreCartLoader);
      }
    } else {
      yield setBagPageLoading(fromMoveTobag, ignoreCartLoader);
    }

    // Get if we need to get updated coupons based on coupons xapp config timeout
    cartProps.isSourceLogin = yield call(isCouponsFetchRequired, isLoggedIn);
    const isPersistPointsApiEnabled = yield select(getIsPersistPointsApiEnabled);
    cartProps.recalcRewards = isPersistPointsApiEnabled ? false : recalcOrderPoints;
    const res = yield call(getCartData, {
      calcsEnabled: isRecalculateTaxes,
      ...cartProps,
    });
    // Update local cart order list, assumes cart api (res) has returned/completed

    yield put(setAddedToBagDataLocal(res.orderDetails.orderItems));
    yield put(BAG_PAGE_ACTIONS.updatePayPalStatus(res));

    if (yield call(shouldTranslate, translation)) {
      const translatedProductInfo = yield call(getTranslatedProductInfo, res);
      if (!translatedProductInfo.error) {
        createMatchObject(res.orderDetails.orderItems, translatedProductInfo);
      }
    }
    yield handelAddressIds(res.orderDetails, addressIds);
    yield updateBopisItems(res, isCartPage);

    // Update current time the redux and async storage for coupon fetch timeout
    yield call(updateCouponsTimeout, cartProps.isSourceLogin);

    if (isCheckoutFlow) {
      yield put(checkoutSetCartData({ res, isCartNotRequired, updateSmsInfo }));
    } else {
      yield put(toggleCheckoutRouting(true));
    }
    if (isMobileApp()) {
      yield setBagpageCarousel(res);
    }
    if (isMobileApp()) {
      if (!isCheckoutFlow) {
        yield put(updateRecommendationsLoading({ isUpdating: false }));
      }
    } else {
      yield put(updateRecommendationsLoading({ isUpdating: false }));
    }
    yield put(BAG_PAGE_ACTIONS.getOrderDetailsComplete(res.orderDetails, excludeCartItems));

    // Call Loyalty Points API when Brierley service is down And Get flag 'loyaltyCallEnabled' from cart Api
    if (isLoyaltyServiceEnabled(res, isPersistPointsApiEnabled)) {
      yield put(
        getLoyaltyDetail({
          ...res.orderDetails.loyaltyPointsRequestPayload,
          source: 'Cart',
          pointsApiCall: res.orderDetails.pointsApi,
        })
      );
    }
    if (!supressCoupons) {
      const couponCachingTTL = yield select(BAG_SELECTORS.getCouponCachingTTL);
      yield put(BAG_PAGE_ACTIONS.setCouponsData({ coupons: res.coupons, couponCachingTTL }));
    }
    if (onCartRes) {
      yield call(onCartRes, res);
    }
    yield put(updateMiniBagFlag(false));
    yield put(BAG_PAGE_ACTIONS.resetBagLoadingState());
    yield put(setBagLoadingStatus(false));
  } catch (err) {
    yield put(setBagLoadingStatus(false));
    yield put(BAG_PAGE_ACTIONS.setBagPageError(err));
  }
}

export const mergeTwoObjects = (obj1, obj2) => {
  const list1 = List(obj1);
  const list2 = List(obj2);
  const newList = [];
  list1.map((ele) => {
    if (list2.size > 0) {
      list2.map((ele2) => {
        if (ele && ele.name !== ele2.name) {
          if (newList.map((e) => e.name).indexOf(ele2.name) === -1) {
            return newList.push(ele2);
          }
          if (newList.map((e) => e.name).indexOf(ele.name) === -1) {
            return newList.push(ele);
          }
        }
        return newList;
      });
    }
    if (newList.map((e) => e.name).indexOf(ele.name) === -1) {
      return newList.push(ele);
    }
    return newList;
  });
  return newList;
};

export function* fetchModuleX({ payload: contentList = [] }) {
  try {
    const moduleXContentList = yield select(getCartModuleXList);
    const payload = contentList.filter((content) => {
      if (content) {
        const { name: contentName } = content;
        return !moduleXContentList.find(({ name }) => name === contentName);
      }
      return false;
    });
    let result = yield call(getModuleX, payload);
    result = Object.keys(result).map((name) => {
      const richText = result[name];
      return { richText, name };
    });
    const mergedModuleX = mergeTwoObjects(result, moduleXContentList);
    yield put(BAG_PAGE_ACTIONS.setModuleX(mergedModuleX));
  } catch (err) {
    yield null;
  }
}

function* navigateToCheckout(stage, navigation, navigationActions, isPayPalFlow = false) {
  yield put(getSetCheckoutStage(stage));
  const navigateAction = navigationActions.navigate({
    routeName: CONSTANTS.CHECKOUT_ROOT,
    params: {
      isPayPalFlow,
    },
  });
  navigation.dispatch(navigateAction);
}

/**
 * routeForCartCheckout component. This is responsible for routing our web to specific page of checkout journey.
 * @param {Boolean} recalc query parameter for recalculation of points
 * @param {Object} navigation for navigating in mobile app
 * @param {Boolean} closeModal for closing addedtoBag modal in app
 */
export function* routeForCartCheckout({
  navigation,
  closeModal,
  recalc,
  navigationActions,
  checkoutStage,
  isAddedToBag,
}) {
  yield put(setLoaderState(true));
  yield put(BAG_PAGE_ACTIONS.setBagPageIsRouting(true));
  const isRegisteredUserCallDone = yield select(getIsRegisteredUserCallDone);
  if (!isRegisteredUserCallDone) {
    yield call(getUserInfoSaga);
  }
  yield put(setLoaderState(false));
  const { hasVenmoReviewPageRedirect, getIsOrderHasPickup } = checkoutSelectors;
  const orderHasPickup = yield select(getIsOrderHasPickup);
  const IsInternationalShipping = yield select(getIsInternationalShipping);
  const isExpressCheckoutEnabled = yield select(isExpressCheckout);
  const hasVenmoReviewPage = yield select(hasVenmoReviewPageRedirect);
  if (isMobileApp()) {
    yield call(
      routeForAppCartCheckout,
      navigation,
      closeModal,
      navigationActions,
      orderHasPickup,
      checkoutStage
    );
  } else if (!IsInternationalShipping) {
    yield put(closeMiniBag());
    yield call(
      getRouteToCheckoutStage,
      { recalc, fromATB: isAddedToBag },
      hasVenmoReviewPage || isExpressCheckoutEnabled,
      true
    );
  } else {
    utility.routeToPage(CHECKOUT_ROUTES.internationalCheckout);
  }
}

export function* checkoutCart(recalc, navigation, closeModal, navigationActions, isAddedToBag) {
  const isVenmoPaymentInProgress = yield select(checkoutSelectors.isVenmoPaymentInProgress);
  const isLoggedIn = yield select(getUserLoggedInState);
  const isRememberedUser = yield select(isRemembered);
  // For Venmo, we need to show remember me user login modal.
  if (
    (!isLoggedIn && !isVenmoPaymentInProgress) ||
    (isVenmoPaymentInProgress && isRememberedUser)
  ) {
    yield put(BAG_PAGE_ACTIONS.setCartSupressOnBagUpdate(true));
    yield put(setSectionLoaderState({ addedToBagLoaderState: false, section: 'addedtobag' }));
    yield put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }));
    yield put(setLoaderState(false));
    return yield put(setCheckoutModalMountedState({ state: true }));
  }
  return yield call(routeForCartCheckout, {
    navigation,
    closeModal,
    recalc,
    navigationActions,
    isAddedToBag,
  });
}

function* confirmStartCheckout() {
  // this.store.dispatch(setVenmoData({ error: null })); // Clear Venmo error message
  // if (cartStoreView.getIsEditingSomeItem(state)) {
  //   // editing an item, display warning modal
  //   confirmationsResult = confirmationsResult.then(() =>
  //     generalOperator.openConfirmationModal(CONFIRM_MODAL_IDS.EDITING, null, null)
  //   );
  // }

  const [OOSCount, unavailableCount] = yield all(
    [BAG_SELECTORS.getOOSCount, BAG_SELECTORS.getUnavailableCount].map((val) => select(val))
  );
  if (OOSCount > 0 || unavailableCount > 0) {
    yield put(setSectionLoaderState({ addedToBagLoaderState: false, section: 'addedtobag' }));
    yield put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }));
    yield put(setLoaderState(false));
    yield put(BAG_PAGE_ACTIONS.openCheckoutConfirmationModal());
    return yield true;
  }
  return false;
}

function* getUnqualifiedItemsCall() {
  let res = yield call(getUnqualifiedItems);
  res = res || [];
  yield all(
    res.map(({ orderItemId, isOOS }) =>
      isOOS
        ? put(BAG_PAGE_ACTIONS.setItemOOS(orderItemId))
        : put(BAG_PAGE_ACTIONS.setItemUnavailable(orderItemId))
    )
  );
}

export function* renderMobileLoader() {
  if (isMobileApp()) {
    yield put(setSectionLoaderState({ checkoutLoader: true, section: 'checkout' }));
  }
}

export function* renderBagPageCheckoutLoader(isMiniBag, isAddedToBag) {
  if (!isAddedToBag && !isMiniBag && !isMobileApp) {
    yield put(setLoaderState(true));
  }
}
export function* startCartCheckout({
  payload: {
    isEditingItem,
    navigation,
    closeModal,
    navigationActions,
    isMiniBag,
    isAddedToBag,
  } = {},
} = {}) {
  try {
    yield call(renderMobileLoader);
    if (isEditingItem) {
      yield put(BAG_PAGE_ACTIONS.openCheckoutConfirmationModal({ isEditingItem }));
    } else {
      if (isMiniBag) {
        yield put(setSectionLoaderState({ miniBagLoaderState: true, section: 'minibag' }));
      }
      if (isAddedToBag) {
        yield put(setSectionLoaderState({ addedToBagLoaderState: true, section: 'addedtobag' }));
      }
      yield call(renderBagPageCheckoutLoader, isMiniBag, isAddedToBag);
      yield call(getUnqualifiedItemsCall);
      yield put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }));
      const oOSModalOpen = yield call(confirmStartCheckout);
      if (!oOSModalOpen) {
        yield call(checkoutCart, false, navigation, closeModal, navigationActions, isAddedToBag);
      }
    }
    yield put(setSectionLoaderState({ checkoutLoader: false, section: 'checkout' }));
    yield put(setLoaderState(false));
    yield put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }));
    yield put(setSectionLoaderState({ addedToBagLoaderState: false, section: 'addedtobag' }));
  } catch (e) {
    yield put(setSectionLoaderState({ checkoutLoader: false, section: 'checkout' }));
    yield put(setSectionLoaderState({ miniBagLoaderState: false, section: 'minibag' }));
    yield put(setSectionLoaderState({ addedToBagLoaderState: false, section: 'addedtobag' }));
    yield put(setLoaderState(false));
    yield call(handleServerSideErrorAPI, e, 'CHECKOUT');
  }
}

export function* startPaypalCheckout({ payload }) {
  const { resolve, reject, isBillingPage } = payload;
  try {
    let oOSModalOpen = false;
    if (!isBillingPage) {
      yield call(getUnqualifiedItemsCall);
      oOSModalOpen = yield call(confirmStartCheckout);
    }
    if (!oOSModalOpen) {
      const orderId = yield select(BAG_SELECTORS.getCurrentOrderId);
      const fromPage = isBillingPage ? 'OrderBillingView' : 'AjaxOrderItemDisplayView';
      const res = yield call(startPaypalCheckoutAPI, orderId, fromPage);
      if (res) {
        yield put(getSetIsPaypalPaymentSettings(res));
        resolve(res.paypalInContextToken);
      }
    } else {
      yield put(BAG_PAGE_ACTIONS.setPayPalModalActionOOS(true));
      throw new Error();
    }
  } catch (e) {
    reject(e);
  }
}

export function* startApplePayCheckout() {
  try {
    yield call(getUnqualifiedItemsCall);
    const oOSModalOpen = yield call(confirmStartCheckout);
    if (!oOSModalOpen) {
      return yield true;
    }
    return oOSModalOpen;
  } catch (e) {
    throw new Error();
  }
}

export function* openApayUnqualifiedItemModal({
  payload: { items, fromApplePayBillingPage } = {},
}) {
  if (items) {
    yield all(
      // eslint-disable-next-line sonarjs/no-identical-functions
      items.map(({ orderItemId, isOOS }) =>
        isOOS
          ? put(BAG_PAGE_ACTIONS.setItemOOS(orderItemId))
          : put(BAG_PAGE_ACTIONS.setItemUnavailable(orderItemId))
      )
    );
    yield put(BAG_PAGE_ACTIONS.openCheckoutConfirmationModal({ fromApplePayBillingPage }));
    yield put(BAG_PAGE_ACTIONS.setApplePayModalActionOOS(true));
  }
}

export function* startPaypalNativeCheckout({ payload }) {
  try {
    const { isBillingPage } = payload;
    yield put(getSetIsPaypalPaymentSettings(null));
    yield put(BAG_PAGE_ACTIONS.setIsGettingPaypalSettings(true));
    const orderId = yield select(BAG_SELECTORS.getCurrentOrderId);
    const fromPage = isBillingPage ? 'OrderBillingView' : 'AjaxOrderItemDisplayView';
    const res = yield call(startPaypalCheckoutAPI, orderId, fromPage);
    if (res) {
      yield put(getSetIsPaypalPaymentSettings(res));
      yield put(BAG_PAGE_ACTIONS.setIsGettingPaypalSettings(false));
    }
  } catch (e) {
    yield call(handleServerSideErrorAPI, e, 'CHECKOUT');
    yield put(BAG_PAGE_ACTIONS.setIsGettingPaypalSettings(false));
  }
}

export function* authorizePayPalPayment({ payload: { navigation, navigationActions } = {} } = {}) {
  try {
    const { tcpOrderId, centinelRequestPage, centinelPayload, centinelOrderId } = yield select(
      checkoutSelectors.getPaypalPaymentSettings
    );
    const params = [tcpOrderId, centinelRequestPage, centinelPayload, centinelOrderId];
    yield put(setLoaderState(true));
    const res = yield call(paypalAuthorizationAPI, ...params);
    if (!isMobileApp()) {
      yield put(setVenmoPaymentInProgress(false));
    }

    yield put(setApplePaymentInProgress(false));
    yield put(setAfterPayInProgress(false));
    if (res) {
      yield put(setLoaderState(false));
      yield put(getSetIsPaypalPaymentSettings(null));
      if (isMobileApp()) {
        yield call(
          navigateToCheckout,
          CONSTANTS.REVIEW_DEFAULT_PARAM,
          navigation,
          navigationActions,
          true
        );
      } else {
        yield put(closeMiniBag());
        utility.routeToPage(CHECKOUT_ROUTES.reviewPagePaypal);
      }
    }
  } catch (e) {
    yield put(setLoaderState(false));
    yield call(handleServerSideErrorAPI, e, 'CHECKOUT');
    if (!isMobileApp()) {
      yield put(closeMiniBag());
      yield put(BAG_PAGE_ACTIONS.setIsPaypalBtnHidden(true));
      routerPush('/bag', '/bag');
    }
  }
}
// export function* authorizePayPalPayment() {
//   try {
//     const { tcpOrderId, centinelRequestPage, centinelPayload, centinelOrderId } = yield select(
//       checkoutSelectors.getPaypalPaymentSettings
//     );
//     const params = [tcpOrderId, centinelRequestPage, centinelPayload, centinelOrderId];
//     const res = yield call(paypalAuthorizationAPI, ...params);
//     if (res) {
//       utility.routeToPage(CHECKOUT_ROUTES.reviewPagePaypal);
//     }
//   } catch (e) {
//     yield call(handleServerSideErrorAPI, e, 'CHECKOUT');
//   }
// }

export function* removeUnqualifiedItemsAndCheckout({
  navigation,
  closeModal,
  navigationActions,
} = {}) {
  yield put(setLoaderState(true));
  const unqualifiedItemsIds = yield select(BAG_SELECTORS.getUnqualifiedItemsIds);
  if (unqualifiedItemsIds && unqualifiedItemsIds.size > 0) {
    yield call(removeItem, unqualifiedItemsIds);
    yield call(getCartDataSaga, {
      payload: {
        recalcRewards: true,
        isRecalculateTaxes: true,
        translation: true,
        excludeCartItems: false,
      },
    });
  }
  yield put(BAG_PAGE_ACTIONS.closeCheckoutConfirmationModal());
  yield call(checkoutCart, true, navigation, closeModal, navigationActions);
  yield put(setLoaderState(false));
}

export function* addItemToSFL({
  payload: { itemId, catEntryId, afterHandler, isMiniBag, price, afterSuccessCallback } = {},
} = {}) {
  if (isMiniBag) {
    yield put(setSectionLoaderState({ miniBagLoaderState: true, section: 'minibag' }));
  } else {
    yield put(setLoaderState(true));
  }
  yield put(CHECKOUT_ACTIONS.setServerErrorCheckout({}));
  const isRememberedUser = yield select(isRemembered);
  const isRegistered = yield select(getUserLoggedInState);
  const countryCurrency = yield select(BAG_SELECTORS.getCurrentCurrency);
  const isCanadaSIte = isCanada();
  try {
    const res = yield call(
      addItemToSflList,
      catEntryId,
      isRememberedUser,
      isRegistered,
      imageGenerator,
      countryCurrency,
      isCanadaSIte
    );
    yield put(BAG_PAGE_ACTIONS.setSflData(res.sflItems));
    if (afterHandler) {
      afterHandler();
    }
    if (res.errorResponse && res.errorMessage) {
      const resErr = res.errorMessage[Object.keys(res.errorMessage)[0]];
      yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(resErr));
    } else {
      const isMoveToSFL = false;
      yield put(BAG_PAGE_ACTIONS.setCartItemsSFL(true));
      yield put(removeCartItem({ itemId, price, isMoveToSFL }));
      yield delay(BAGPAGE_CONSTANTS.ITEM_SFL_SUCCESS_MSG_TIMEOUT);
    }
    if (afterSuccessCallback) {
      afterSuccessCallback();
    }
  } catch (err) {
    yield put(setSectionLoaderState(false, 'minibag'));
    yield put(setLoaderState(false));
    const errorsMapping = yield select(BAG_SELECTORS.getErrorMapping);
    yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(getServerErrorMessage(err, errorsMapping)));
  }
}

export function* getSflDataSaga() {
  try {
    const currencyCode = yield select(BAG_SELECTORS.getCurrentCurrency);
    const isCanadaSite = isCanada();
    const res = yield call(getSflItems, imageGenerator, currencyCode, isCanadaSite);
    yield put(BAG_PAGE_ACTIONS.setSflData(res.sflItems));
    if (res.cartSflMerged) {
      yield put(BAG_PAGE_ACTIONS.setCartSflMerged(true));
      yield delay(BAGPAGE_CONSTANTS.ITEM_SFL_SUCCESS_MSG_TIMEOUT);
      yield put(BAG_PAGE_ACTIONS.setCartSflMerged(false));
    }
  } catch (err) {
    yield put(BAG_PAGE_ACTIONS.setBagPageError(err));
  }
}

export function* setModifiedSflData(data) {
  try {
    if (yield call(shouldTranslate, true)) {
      const translatedProductInfo = yield call(getTranslatedProductInfo, data.payload);
      if (!translatedProductInfo.error) {
        createMatchObject(data.payload, translatedProductInfo);
      }
    }
    yield put(BAG_PAGE_ACTIONS.setTranslatedSflData(data.payload));
  } catch (err) {
    yield put(BAG_PAGE_ACTIONS.setBagPageError(err));
  }
}
export function* setSflItemUpdate(payload) {
  try {
    const isRememberedUser = yield select(isRemembered);
    const isRegistered = yield select(getUserLoggedInState);
    const currencyCode = yield select(BAG_SELECTORS.getCurrentCurrency);
    const isCanadaSite = isCanada();
    const {
      payload: { oldSkuId, newSkuId, callBack },
    } = payload;
    const res = yield call(
      updateSflItem,
      oldSkuId,
      newSkuId,
      isRememberedUser,
      isRegistered,
      imageGenerator,
      currencyCode,
      isCanadaSite
    );
    if (callBack) {
      callBack();
    }
    yield put(BAG_PAGE_ACTIONS.setSflData(res.sflItems));
  } catch (err) {
    yield put(BAG_PAGE_ACTIONS.setBagPageError(err));
  }
}

export function* startSflItemMoveToBag({ payload }) {
  try {
    yield put(setLoaderState(true));
    const { itemId, multiPackItems = null } = payload;
    const addToCartData = {
      skuInfo: {
        skuId: itemId,
      },
      quantity: 1,
      fromMoveToBag: true,
      multiPackItems,
    };
    yield call(addToCartEcom, { payload: addToCartData });
    yield call(startSflItemDelete, { payload: { ...payload, fromMoveToBag: true } });
    yield call(getCartDataSaga, {
      payload: {
        isRecalculateTaxes: true,
        recalcRewards: true,
        translation: true,
        excludeCartItems: false,
      },
    });
    yield put(setLoaderState(false));
    yield put(BAG_PAGE_ACTIONS.setSflItemDeleted(true));
    yield put(BAG_PAGE_ACTIONS.setSflItemMovedToBag(true));
    yield delay(BAGPAGE_CONSTANTS.ITEM_SFL_SUCCESS_MSG_TIMEOUT);
    yield put(BAG_PAGE_ACTIONS.setSflItemDeleted(false));
  } catch (err) {
    yield put(setLoaderState(false));
    yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(err));
  }
}

export function* addItemToSFLInChannel({
  payload: { itemId, catEntryId, afterHandler, price, afterSuccessCallback } = {},
} = {}) {
  yield put(setCartLoadingState(true));
  yield put(CHECKOUT_ACTIONS.setServerErrorCheckout({}));
  if (afterHandler) {
    afterHandler();
  }
  const isRememberedUser = yield select(isRemembered);
  const isRegistered = yield select(getUserLoggedInState);
  const countryCurrency = yield select(BAG_SELECTORS.getCurrentCurrency);
  const isCanadaSIte = isCanada();
  const deletedProductInfo = {
    orderId: yield select(getCartOrderId),
    orderItems: [itemId],
  };
  yield put(BAG_PAGE_ACTIONS.removeDeletedItem(deletedProductInfo));
  try {
    const res = yield call(
      addItemToSflList,
      catEntryId,
      isRememberedUser,
      isRegistered,
      imageGenerator,
      countryCurrency,
      isCanadaSIte
    );
    yield put(BAG_PAGE_ACTIONS.setSflData(res.sflItems));
    if (res.errorResponse && res.errorMessage) {
      const resErr = res.errorMessage[Object.keys(res.errorMessage)[0]];
      yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(resErr));
      yield put(BAG_PAGE_ACTIONS.addDeletedItemOnAPiFailure());
    } else {
      const isMoveToSFL = false;
      yield put(BAG_PAGE_ACTIONS.setCartItemsSFL(true));
      yield put(removeCartItem({ itemId, price, isMoveToSFL }));
      yield delay(BAGPAGE_CONSTANTS.ITEM_SFL_SUCCESS_MSG_TIMEOUT);
    }
    if (afterSuccessCallback) {
      afterSuccessCallback();
    }
  } catch (err) {
    yield put(setCartLoadingState(false));
    const errorsMapping = yield select(BAG_SELECTORS.getErrorMapping);
    yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(getServerErrorMessage(err, errorsMapping)));
    yield put(BAG_PAGE_ACTIONS.addDeletedItemOnAPiFailure());
  }
}

// export function* startSflItemMoveToBagInChannel({ payload }) {
//   const getsflItemsList = yield select(BAG_SELECTORS.getsflItemsList);
//   try {
//     yield put(setLoaderState(true));
//     const { itemId, multiPackItems = null } = payload;
//     const addToCartData = {
//       skuInfo: {
//         skuId: itemId,
//       },
//       quantity: 1,
//       fromMoveToBag: true,
//       multiPackItems,
//     };
//     const res1 = yield call(addToCartEcom, { payload: addToCartData });
//     if (res1?.errorResponse && res1?.errorMessage) {
//       const resErr = res1.errorMessage[Object.keys(res1.errorMessage)[0]];
//       yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(resErr));
//       yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(res1.errorMessage));
//     } else {
//       yield call(startSflItemDelete, { payload: { ...payload, fromMoveToBag: true } });

//       yield call(getCartDataSaga, {
//         payload: {
//           isRecalculateTaxes: true,
//           recalcRewards: true,
//           translation: true,
//           excludeCartItems: false,
//         },
//       });
//       yield put(setLoaderState(false));
//       yield put(BAG_PAGE_ACTIONS.setSflItemDeleted(true));
//       yield put(BAG_PAGE_ACTIONS.setSflItemMovedToBag(true));
//       yield delay(BAGPAGE_CONSTANTS.ITEM_SFL_SUCCESS_MSG_TIMEOUT);
//       yield put(BAG_PAGE_ACTIONS.setSflItemDeleted(false));
//     }
//   } catch (err) {
//     yield put(setLoaderState(false));
//     yield put(BAG_PAGE_ACTIONS.setCartItemsSflError(err));
//     yield put(BAG_PAGE_ACTIONS.setSflData(getsflItemsList));
//   }
// }

// export function* addProductsToBag() {
//   const addProductToBag = yield actionChannel(BAGPAGE_CONSTANTS.SFL_ITEMS_MOVE_TO_BAG);
//   while (true) {
//     try {
//       yield take(addProductToBag);
//       yield* startSflItemMoveToBagInChannel();
//     } catch (e) {
//       console.log(e);
//       break;
//     }
//   }
// }

export function* addProductsToSFL() {
  const saveProductForLater = yield actionChannel(BAGPAGE_CONSTANTS.ADD_ITEM_SAVE_FOR_LATER);
  while (true) {
    try {
      const payload = yield take(saveProductForLater);
      yield call(addItemToSFLInChannel, payload);
      // yield* addProductsToBag();
    } catch (e) {
      console.log(e);
      break;
    }
  }
}

export function* BagPageSaga() {
  yield takeLatest(BAGPAGE_CONSTANTS.GET_ORDER_DETAILS, getOrderDetailSaga);
  yield takeLatest(BAGPAGE_CONSTANTS.GET_CART_DATA, getCartDataSaga);
  yield takeLatest(BAGPAGE_CONSTANTS.FETCH_MODULEX_CONTENT, fetchModuleX);
  yield takeLatest(
    BAGPAGE_CONSTANTS.REMOVE_UNQUALIFIED_AND_CHECKOUT,
    removeUnqualifiedItemsAndCheckout
  );
  yield takeLatest(BAGPAGE_CONSTANTS.ROUTE_FOR_CART_CHECKOUT, routeForCartCheckout);
  yield takeLatest(BAGPAGE_CONSTANTS.START_BAG_CHECKOUT, startCartCheckout);
  yield takeLatest(BAGPAGE_CONSTANTS.START_PAYPAL_CHECKOUT, startPaypalCheckout);
  yield takeLatest(BAGPAGE_CONSTANTS.START_APPLE_PAY_CHECKOUT, startApplePayCheckout);
  yield takeLatest(BAGPAGE_CONSTANTS.AUTHORIZATION_PAYPAL_CHECKOUT, authorizePayPalPayment);
  yield takeLatest(BAGPAGE_CONSTANTS.GET_SFL_DATA, getSflDataSaga);
  yield takeLatest(BAGPAGE_CONSTANTS.SFL_ITEMS_DELETE, startSflItemDelete);
  yield takeLatest(BAGPAGE_CONSTANTS.SFL_ITEMS_MOVE_TO_BAG, startSflItemMoveToBag);
  yield takeLatest(BAGPAGE_CONSTANTS.START_PAYPAL_NATIVE_CHECKOUT, startPaypalNativeCheckout);
  yield takeLatest(BAGPAGE_CONSTANTS.SET_SFL_DATA, setModifiedSflData);
  yield takeLatest(BAGPAGE_CONSTANTS.UPDATE_SFL_ITEM, setSflItemUpdate);
  yield takeLatest(
    BAGPAGE_CONSTANTS.OPEN_APAY_UNQUALIFIED_ITEMS_MODAL,
    openApayUnqualifiedItemModal
  );
  if (isOptimizedDelete) {
    yield* addProductsToSFL();
  } else {
    yield takeLatest(BAGPAGE_CONSTANTS.ADD_ITEM_SAVE_FOR_LATER, addItemToSFL);
  }
}

export default BagPageSaga;
