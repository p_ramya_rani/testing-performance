// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS, List } from 'immutable';
import LoyaltyConstants from '@tcp/core/src/components/features/CnC/LoyaltyBanner/LoyaltyConstants';
import { setLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import BAGPAGE_CONSTANTS from '../BagPage.constants';
import { AVAILABILITY } from '../../../../../services/abstractors/CnC/CartItemTile';

const initialState = fromJS({
  orderDetails: {},
  localCartOrderItems: [],
  sfl: [],
  cartSflMerged: false,
  errors: false,
  loaded: false, // flag to check if cart data is fetched for the first time.
  bagLoading: true, // flag to check if cart data is loading (set/unset every time cart API invokes)
  isRouting: false, // flag to check if cart page is routing to some other page.
  openItemDeleteConfirmationModalInfo: { showModal: false },
  currentItemId: null,
  moduleXContent: [],
  showConfirmationModal: false,
  isEditingItem: false,
  fromApplePayBillingPage: false,
  isCartSupressOnBagUpdate: false,

  uiFlags: {
    isPayPalEnabled: false,
    isPayPalWebViewEnable: false,
    isPayPalRenderDone: false,
    isPayPalLibraryRenderDone: false,
    lastItemUpdatedId: null,
    isTotalEstimated: true,
    isClosenessQualifier: false,
    recentlyRemovedItemsCount: 0,
    shouldRedirectBackTo: false,
    cartItemForRecommendations: null,
    largeProductImagesLoading: false,
    isCartItemsUpdating: {
      isItemDeleted: false,
      isItemUpdated: false,
    },
    cartItemSflError: null,
    isPickupStoreUpdating: false,
    isItemMovedToSflList: false,
    isSflItemDeleted: false,
    cartItemLargeImagesViewer: {
      opened: false,
      currentIndex: 0,
      itemId: '',
      productId: '',
    },
    isPayPalModalActionOOS: false,
    isApplePayModalActionOOS: false,
    isSflItemMovedToBag: false,
    addressIds: {
      previousId: 0,
      currentId: 0,
    },
    isGettingPaypalSettings: false,
    updateMiniBag: true,
    maxItemError: false,
    maxItemErrorDisplayed: false,
    couponMergeError: false,
    updateCartComplete: false,
    productListB4Delete: [],
  },
});

const resetState = initialState
  .set('bagLoading', false)
  .set('loaded', true)
  .set('orderDetails', fromJS({ orderItems: [] }));

function updateItem(state, itemId, status) {
  const indexValue = state
    .getIn(['orderDetails', 'orderItems'])
    .findIndex((item) => item.getIn(['itemInfo', 'itemId']) === itemId);
  if (indexValue >= 0) {
    return state.setIn(
      ['orderDetails', 'orderItems', indexValue, 'miscInfo', 'availability'],
      status
    );
  }
  return state;
}

function updateItemData(state, payload) {
  const indexValue = state
    .getIn(['localCartOrderItems'])
    .findIndex((item) => item.getIn(['itemInfo', 'itemId']) === payload.itemId);
  if (indexValue >= 0) {
    return state
      .setIn(['localCartOrderItems', indexValue, 'itemInfo', 'quantity'], payload.quantity)
      .setIn(['localCartOrderItems', indexValue, 'productInfo', 'size'], payload.size)
      .setIn(['localCartOrderItems', indexValue, 'productInfo', 'color'], payload.skuInfo.color);
  }
  return state;
}

function removeMultipleItem(state, { orderItems = [] }) {
  const filteredCartItems = state
    .getIn(['orderDetails', 'orderItems'])
    .filter((item) => !orderItems.includes(item.getIn(['itemInfo', 'itemId'])));
  const productListB4Delete = state.getIn(['orderDetails', 'orderItems']);
  return state
    .set('productListB4Delete', productListB4Delete)
    .setIn(['orderDetails', 'orderItems'], filteredCartItems);
}

function addDeletedItemBack(state) {
  const productListB4Delete = state.get('productListB4Delete');
  return state.setIn(['orderDetails', 'orderItems'], productListB4Delete);
}

function setCartItemsUpdating(state, isCartItemUpdating) {
  return state.setIn(['uiFlags', 'isCartItemsUpdating'], isCartItemUpdating);
}

function setConfirmationModalState(state, action) {
  let fromApplePayBillingPage = false;
  let isEditingItem = false;
  if (action.payload) {
    fromApplePayBillingPage = action.payload.fromApplePayBillingPage || false;
    isEditingItem = action.payload.isEditingItem || false;
  }
  return state
    .set('showConfirmationModal', true)
    .set('isEditingItem', isEditingItem)
    .set('fromApplePayBillingPage', fromApplePayBillingPage);
}

function updatePayPalStateValue(state, action) {
  const status = (action && action.orderDetails && action.orderDetails.isPaypalEnabled) || false;
  return state.setIn(['uiFlags', 'isPayPalEnabled'], status);
}

function setCartItemsSFL(state, isCartItemSFL) {
  return state.setIn(['uiFlags', 'isItemMovedToSflList'], isCartItemSFL);
}

function setSflItemDeleted(state, isCartItemSFL) {
  return state.setIn(['uiFlags', 'isSflItemDeleted'], isCartItemSFL);
}

function setCartItemsSflError(state, isCartItemSflError) {
  return state.setIn(['uiFlags', 'cartItemSflError'], isCartItemSflError);
}

function setUpdateMiniBagFlag(state, payload) {
  if (!payload) {
    setLocalStorage({
      key: BAGPAGE_CONSTANTS.UPDATE_MINIBAG_TIMESTAMP,
      value: new Date().toString(),
    });
  }
  return state.setIn(['uiFlags', 'updateMiniBag'], payload);
}

// eslint-disable-next-line complexity
const returnBagPageReducerExtension = (state = initialState, action = {}) => {
  switch (action.type) {
    case BAGPAGE_CONSTANTS.PAYPAL_BUTTON_HIDDEN:
      return state.set('paypalBtnHidden', action.payload);
    case BAGPAGE_CONSTANTS.FETCHING_CART_DATA:
      return state.set('bagLoading', true);
    case BAGPAGE_CONSTANTS.RESET_BAG_LOADED_STATE:
      return state.set('loaded', false);
    case BAGPAGE_CONSTANTS.SET_BAG_PAGE_ROUTING:
      return state.set('isRouting', action.payload);
    case BAGPAGE_CONSTANTS.UPDATE_PAYPAL_STATUS:
      return updatePayPalStateValue(state, action.payload);
    case BAGPAGE_CONSTANTS.IS_PAYPAL_BUTTON_RENDER_DONE:
      return state.setIn(['uiFlags', 'isPayPalRenderDone'], action.payload);
    case BAGPAGE_CONSTANTS.UPDATE_ADDRESS_IDS:
      return state
        .setIn(['uiFlags', 'addressIds', 'previousId'], action.payload.previousId)
        .setIn(['uiFlags', 'addressIds', 'currentId'], action.payload.currentId);
    case BAGPAGE_CONSTANTS.IS_PAYPAL_LIBRARY_RENDER_DONE:
      return state.setIn(['uiFlags', 'isPayPalLibraryRenderDone'], action.payload);
    case BAGPAGE_CONSTANTS.SET_BAG_PAGE_LOADING_STATUS:
      return state.setIn(['uiFlags', 'bagPageLoadingStatus'], action.payload);
    case BAGPAGE_CONSTANTS.UPDATE_MINIBAG_FLAG:
      return setUpdateMiniBagFlag(state, action.payload);
    case BAGPAGE_CONSTANTS.RESET_BAG_LOADING_STATE:
      return state.set('bagLoading', false);
    case BAGPAGE_CONSTANTS.UPDATE_AIRMILES_DATA:
      return state
        .setIn(['orderDetails', 'airmiles', 'promoId'], action.payload.offerCode)
        .setIn(['orderDetails', 'airmiles', 'accountNumber'], action.payload.promoId);
    case BAGPAGE_CONSTANTS.SET_PAYPAL_MODAL_ACTION_OOS:
      return state.setIn(['uiFlags', 'isPayPalModalActionOOS'], action.payload);
    case BAGPAGE_CONSTANTS.SET_APPLE_PAY_MODAL_ACTION_OOS:
      return state.setIn(['uiFlags', 'isApplePayModalActionOOS'], action.payload);
    case BAGPAGE_CONSTANTS.SET_COUPON_MERGE_ERROR:
      return state.setIn(['uiFlags', 'couponMergeError'], action.payload);
    case BAGPAGE_CONSTANTS.SET_MAX_ITEM_ERROR:
      return state.setIn(['uiFlags', 'maxItemError'], action.payload);
    case BAGPAGE_CONSTANTS.SET_MAX_ITEM_ERROR_DISPLAYED:
      return state.setIn(['uiFlags', 'maxItemErrorDisplayed'], action.payload);
    case BAGPAGE_CONSTANTS.UPDATE_CART_COUNT:
      return state.setIn(['orderDetails', 'totalItems'], action.payload.totalItems);
    case BAGPAGE_CONSTANTS.CART_SET_ORDER_ID:
      return state.setIn(['orderDetails', 'orderId'], action.payload.orderId);
    case BAGPAGE_CONSTANTS.REMOVE_DELETED_ITEM:
      return removeMultipleItem(state, action.payload);
    case BAGPAGE_CONSTANTS.ADD_DELETED_ITEM_ON_API_FAILURE:
      return addDeletedItemBack(state);
    case LoyaltyConstants.UPDATE_POINTS_IN_CART:
      return state
        .setIn(['orderDetails', 'estimatedRewards'], action.payload.estimatedRewards)
        .setIn(['orderDetails', 'isBrierleyWorking'], action.payload.isBrierleyWorking)
        .setIn(['orderDetails', 'orderItems'], action.payload.orderItems)
        .setIn(['orderDetails', 'pointsToNextReward'], action.payload.pointsToNextReward)
        .setIn(['orderDetails', 'earnedReward'], action.payload.earnedReward);
    case BAGPAGE_CONSTANTS.PAYPAL_SETTINGS_CALL_STATUS:
      return state.setIn(['uiFlags', 'isGettingPaypalSettings'], action.payload);
    case LoyaltyConstants.UPDATE_LOYALTY_POINTS:
      return state
        .setIn(['orderDetails', 'estimatedRewards'], action.payload.estimatedRewards)
        .setIn(['orderDetails', 'pointsToNextReward'], action.payload.pointsToNextReward)
        .setIn(['orderDetails', 'earnedReward'], action.payload.earnedReward)
        .setIn(['orderDetails', 'isBrierleyWorking'], action.payload.isBrierleyWorking);
    case BAGPAGE_CONSTANTS.SET_CART_SFL_MERGED:
      return state.set('cartSflMerged', action.payload);
    case BAGPAGE_CONSTANTS.UPDATE_CART_PRICE_DATA:
      return state
        .setIn(['orderDetails', 'totalItems'], action.payload.totalItems)
        .setIn(['orderDetails', 'grandTotal'], action.payload.grandTotal)
        .setIn(['orderDetails', 'giftCardsTotal'], action.payload.giftCardsTotal)
        .setIn(
          ['orderDetails', 'cartTotalAfterPLCCDiscount'],
          action.payload.cartTotalAfterPLCCDiscount
        )
        .setIn(['orderDetails', 'orderSubTotalDiscount'], action.payload.orderSubTotalDiscount);

    case BAGPAGE_CONSTANTS.UPDATE_ORDERTYPE_DATA:
      return state
        .setIn(['orderDetails', 'isBopisOrder'], action.payload.isBopisOrder)
        .setIn(['orderDetails', 'isBossOrder'], action.payload.isBossOrder)
        .setIn(['orderDetails', 'isECOM'], action.payload.isECOM)
        .setIn(['orderDetails', 'isPickupOrder'], action.payload.isPickupOrder);

    case BAGPAGE_CONSTANTS.SET_CART_SUPRESS_BAG_UPDATE:
      return state.set('isCartSupressOnBagUpdate', action.payload);

    default:
      // TODO: currently when initial state is hydrated on browser, List is getting converted to an JS Array
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

const returnBagPageReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case BAGPAGE_CONSTANTS.OPEN_CHECKOUT_CONFIRMATION_MODAL:
      return setConfirmationModalState(state, action);
    case BAGPAGE_CONSTANTS.CLOSE_CHECKOUT_CONFIRMATION_MODAL:
      return state
        .set('showConfirmationModal', false)
        .set('isEditingItem', false)
        .set('fromApplePayBillingPage', false);
    case BAGPAGE_CONSTANTS.CART_ITEMS_SET_UPDATING:
      return setCartItemsUpdating(state, action.payload);
    case BAGPAGE_CONSTANTS.CART_ITEMS_SET_SFL:
      return setCartItemsSFL(state, action.payload);
    case BAGPAGE_CONSTANTS.CART_ITEMS_SET_SFL_ERROR:
      return setCartItemsSflError(state, action.payload);
    case BAGPAGE_CONSTANTS.SET_TRANSLATED_SFL_DATA:
      return state.set('sfl', fromJS(action.payload));
    case BAGPAGE_CONSTANTS.CLOSE_ITEM_DELETE_CONFIRMATION_MODAL:
      return state.set('openItemDeleteConfirmationModalInfo', { showModal: false });
    case BAGPAGE_CONSTANTS.OPEN_ITEM_DELETE_CONFIRMATION_MODAL:
      return state.set('openItemDeleteConfirmationModalInfo', {
        ...action.payload,
        showModal: true,
      });
    case BAGPAGE_CONSTANTS.SFL_ITEMS_MOVED_TO_BAG:
      return state.setIn(['uiFlags', 'isSflItemMovedToBag'], action.payload);
    default:
      return returnBagPageReducerExtension(state, action);
  }
};

// eslint-disable-next-line complexity
const BagPageReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case BAGPAGE_CONSTANTS.GET_ORDER_DETAILS_COMPLETE:
      return state
        .set('loaded', true)
        .set('bagLoading', false)
        .set('orderDetails', fromJS(action.payload));
    case BAGPAGE_CONSTANTS.SET_BAG_PAGE_ERRORS:
      return state.set('errors', fromJS(action.payload)).set('bagLoading', false);
    case BAGPAGE_CONSTANTS.SET_MODULEX_CONTENT:
      return state.set('moduleXContent', List(action.payload));
    case 'CART_SUMMARY_SET_ORDER_ID':
      return state.setIn(['orderDetails', 'orderId'], action.orderId);
    case BAGPAGE_CONSTANTS.SET_ITEM_OOS:
      return updateItem(state, action.payload, AVAILABILITY.SOLDOUT);
    case BAGPAGE_CONSTANTS.PAYPAL_WEBVIEW_ENABLE:
      return state.setIn(['uiFlags', 'isPayPalWebViewEnable'], action.payload);

    case BAGPAGE_CONSTANTS.SET_ITEM_UNAVAILABLE:
      return updateItem(state, action.payload, AVAILABILITY.UNAVAILABLE);
    case BAGPAGE_CONSTANTS.SFL_ITEMS_SET_DELETED:
      return setSflItemDeleted(state, action.payload);
    case BAGPAGE_CONSTANTS.RESET_CART_DATA:
      return resetState;
    case BAGPAGE_CONSTANTS.UPDATE_CART_LOCAL_DATA:
      return state.set('localCartOrderItems', fromJS(action.payload));
    case BAGPAGE_CONSTANTS.UPDATE_CART_COMPLETE:
      return state.setIn(['uiFlags', 'updateCartComplete'], action.payload);
    case BAGPAGE_CONSTANTS.CART_ITEM_QUICK_UPDATE:
      return updateItemData(state, action.payload);
    default:
      return returnBagPageReducer(state, action);
  }
};

export default BagPageReducer;
