/* eslint-disable max-lines, import/no-named-as-default-member */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import logger from '@tcp/core/src/utils/loggerInstance';
import isEqual from 'lodash/isEqual';
import PropTypes, { func, bool } from 'prop-types';
import { connect } from 'react-redux';
import { fetchPageLayout, setCartsSessionStatus } from '@tcp/core/src/reduxStore/actions';
import { createSelector } from 'reselect';
import { isGuest as isGuestUser } from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import names from '@tcp/core/src/constants/eventsName.constants';
import { getQuickViewLoader } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.selector';
import {
  getUniqueId,
  getModalState,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import {
  getIsChangeRecalForEcom,
  getCartsSessionStatus,
  getIsDisableBagAnchoring,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { playHapticFeedback } from '@tcp/core/src/components/common/atoms/hapticFeedback/container/HapticFeedback.actions.native';
import {
  setClickAnalyticsData,
  trackClick,
  trackPageView,
  resetClickAnalyticsData,
  updatePageData,
} from '@tcp/core/src/analytics/actions';
import {
  isRememberedUser,
  getUserId,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { isApplePayEligible, isTCP } from '@tcp/core/src/utils/utils';
import {
  SFL_ITEM_COUNTER,
  readCookie,
  removeCookie,
  getCartItemCount,
} from '@tcp/core/src/utils/cookie.util';
import {
  resetEddData,
  clearEddTTL,
  hideLoader,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.actions';
import { updateRecommendationsLoading } from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.actions';
import sbpConstant from '@tcp/core/src/components/features/browse/ShopByProfile/container/ShopByProfile.constants';
import {
  getIsPayPalEnabled,
  getIsNewBag,
  getIsShowLowInventoryBanner,
  getIsShowLowInventoryBannerForOldDesign,
  getIsHideStickyCheckoutButton,
  getIsStickyCheckoutAbTestEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import BagPageSelector from './BagPage.selectors';
import BagPage from '../views/BagPage.view';
import BAG_PAGE_ACTIONS from './BagPage.actions';
import {
  getCartOrderList,
  getIsCartItemsUpdating,
  getLabelsCartItemTile,
  getIsCartItemsSFL,
  getIsSflItemRemoved,
  getCartItemsSflError,
  getPageName,
  getIsSflItemMovedToBag,
} from '../../CartItemTile/container/CartItemTile.selectors';
import {
  getUserLoggedInState,
  getIsRegisteredUserCallDone,
  isPlccUser,
} from '../../../account/User/container/User.selectors';
import { getBonusPointsSwitch } from '../../../../common/organisms/BonusPointsDays/container/BonusPointsDays.selectors';
import CHECKOUT_ACTIONS, {
  setVenmoPaymentInProgress,
  setVenmoPickupMessageState,
  setApplePaymentInProgress,
  setVenmoShippingMessageState,
} from '../../Checkout/container/Checkout.action';
import checkoutSelectors from '../../Checkout/container/Checkout.selector';
import {
  toastMessageInfo,
  toastMessagePosition,
} from '../../../../common/atoms/Toast/container/Toast.actions';
import utils, {
  isClient,
  scrollToElement,
  isMobileApp,
  getSiteId,
  isCanada,
  fireEnhancedEcomm,
  setSessionStorage,
  getSessionStorage,
  getValueFromAsyncStorage,
  getAPIConfig,
} from '../../../../../utils';
import { getSaveForLaterSwitch } from '../../SaveForLater/container/SaveForLater.selectors';
import {
  getGrandTotal,
  getGiftCardsTotal,
} from '../../common/organism/OrderLedger/container/orderLedger.selector';
import { getIsPickupModalOpen } from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { getAppliedExpiredCouponListState } from '../../common/organism/CouponAndPromos/container/Coupon.selectors';
import PlaceCashSelector from '../../PlaceCashBanner/container/PlaceCashBanner.selectors';
import BAGPAGE_CONSTANTS from '../BagPage.constants';
import BagPageUtils from '../views/Bagpage.utils';
import { getSflItemCount } from '../../../../../utils/cookie.util';
import ConfirmationSelector from '../../Confirmation/container/Confirmation.selectors';

const shouldEDDCall = (cartOrderItems, prevCartOrderItems) =>
  cartOrderItems !== prevCartOrderItems && cartOrderItems && cartOrderItems.size !== 0;
export class BagPageContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      triggerTracking: true,
    };
    this.isMobileApp = isMobileApp();
    this.fromMiniBag = false;
    this.isApplePayVisible = this.getApplePayVisible();
  }

  componentDidMount() {
    const {
      fetchNeedHelpContent,
      resetAnalyticsData,
      bagPageServerError,
      clearCheckoutServerError,
      venmoOrderConfirmationId,
      airmilesBannerContentId,
      setMaxItemErrorAction,
      setCouponMergeErrorAction,
      navigation,
      trackClickAction,
      setClickAnalyticsDataBag,
      screenProps,
      router,
    } = this.props;
    if (isMobileApp() && screenProps) {
      screenProps.checkFirst('BagStack');
    }
    resetAnalyticsData();
    this.fetchInitialActions();
    this.fetchSFLItems();

    const fromNewATBModal = (navigation && navigation.getParam('comingFromNewATBModal')) || false;
    if (fromNewATBModal) {
      const name = names.screenNames.atb_drawer_view_bag;
      const module = 'browse';
      const clickData = { customEvents: ['event143'] };
      setClickAnalyticsDataBag(clickData);
      trackClickAction({ name, module });
    }
    const needHelpContents = [venmoOrderConfirmationId];

    if (isCanada()) {
      needHelpContents.push(airmilesBannerContentId);
    }

    fetchNeedHelpContent(needHelpContents);
    const {
      setVenmoPickupState,
      setVenmoShippingState,
      setVenmoInProgress,
      setAppleInProgress,
      getHomePageLayout,
      layouts,
      isCartLoaded,
    } = this.props;
    if (!this.isMobileApp) {
      setVenmoPickupState(false);
      setVenmoShippingState(false);
      setVenmoInProgress(false);
    } // Setting venmo progress as false. User can select normal checkout on cart page.
    setAppleInProgress(false);

    if (bagPageServerError) {
      clearCheckoutServerError({});
    }
    setMaxItemErrorAction(false);
    setCouponMergeErrorAction(false);
    const apiConfigObj = getAPIConfig();
    const { loadHomePageDataEnabled, timeOutGap } = apiConfigObj;
    if (!this.isMobileApp && isCartLoaded && loadHomePageDataEnabled && !layouts.home) {
      const apiConfig = getAPIConfig();
      setTimeout(() => {
        getHomePageLayout(apiConfig);
      }, timeOutGap);
    }
    if (!this.isMobileApp) {
      this.fromMiniBag = utils.getObjectValue(router, false, 'query', 'fromMiniBag');
    }
  }

  shouldComponentUpdate(nextProps) {
    return !isEqual(nextProps, this.props);
  }

  componentDidUpdate(prevProps) {
    const { isRegisteredUserCallDone: prevIsRegisteredUserCallDone } = prevProps;
    const {
      router,
      isRegisteredUserCallDone,
      bagPageIsRouting,
      isCartLoaded,
      pageName,
      resetEDDCall,
      isCartSupressOnBagUpdate,
    } = this.props;
    if (
      prevIsRegisteredUserCallDone !== isRegisteredUserCallDone &&
      !bagPageIsRouting &&
      !isCartSupressOnBagUpdate
    ) {
      this.fetchInitialActions();
      this.fetchSFLItems();
    }
    if (isClient()) {
      const isSfl = utils.getObjectValue(router, undefined, 'query', 'isSfl');
      if (isSfl && isCartLoaded && pageName !== BAGPAGE_CONSTANTS.SHOPPING_BAG) {
        const headingElem = document.querySelector('.save-for-later-section-heading');
        const timer = setTimeout(() => {
          scrollToElement(headingElem);
          clearTimeout(timer);
        }, 700);
      }
    }
    const { cartOrderItems: prevCartOrderItems } = prevProps;
    const { cartOrderItems } = this.props;
    const { triggerTracking } = this.state;

    // Prevent pageload tracking after first pageload tracking
    if (triggerTracking) {
      this.startBagAnalytics(cartOrderItems, prevCartOrderItems);
    }

    if (shouldEDDCall(cartOrderItems, prevCartOrderItems)) {
      resetEDDCall();
    }
    this.checkForMergeCartErrors();
  }

  componentWillUnmount() {
    const { resetBagLoadedState, resetAnalyticsData, setCouponMergeErrorAction } = this.props;
    resetBagLoadedState();
    resetAnalyticsData();
    this.setState({ triggerTracking: true });
    setCouponMergeErrorAction(false);
  }

  validateCartMergeMessaging = (isGuest, showMaxItemError, isAppMessageDisplayed) =>
    !isGuest && !showMaxItemError && !isAppMessageDisplayed;

  checkForMergeCartErrors = async () => {
    const {
      setCouponMergeErrorAction,
      setMaxItemErrorAction,
      currentUserId,
      isGuest,
      showMaxItemErrorDisplayed,
      showMaxItemError,
    } = this.props;
    const isAppMessageDisplayed = this.isMobileApp && showMaxItemErrorDisplayed;
    if (this.validateCartMergeMessaging(isGuest, showMaxItemError, isAppMessageDisplayed)) {
      const notificationFlag = this.isMobileApp
        ? await readCookie(`MC_${currentUserId}`)
        : readCookie(`MC_${currentUserId}`);
      if (notificationFlag) {
        if (notificationFlag.indexOf('ex_quantity') !== -1) {
          setMaxItemErrorAction(true);
        }
        if (notificationFlag.indexOf('ex_coupon') !== -1) {
          setCouponMergeErrorAction(true);
        }
        if (!this.isMobileApp) {
          removeCookie(`MC_${currentUserId}`, isTCP());
        }
      }
    }
  };

  closeModal = () => {};

  getCustomEventsForAnalytics = () => {
    const analyticsObj = JSON.parse(getSessionStorage('AnalyticsObj')) || {};
    const { metric2 } = analyticsObj;
    if (!metric2) {
      setSessionStorage({
        key: 'AnalyticsObj',
        value: JSON.stringify({ ...analyticsObj, metric2: false }),
      });
      const obj = {
        event: 'BagPage_CM2',
        CM2: '1',
      };
      fireEnhancedEcomm({
        isCustomEvent: true,
        event: obj,
      });
    }
  };

  getApplePayVisible = () => {
    const { isApplePayEnabled, rememberedUser, isApplePayRememberedUser } = this.props;
    return (
      isApplePayEnabled && isApplePayEligible() && !(!isApplePayRememberedUser && rememberedUser)
    );
  };

  sbpAnalytics = (productsData) => {
    return new Promise(async (resolve) => {
      try {
        let sbpProducts = await getValueFromAsyncStorage(sbpConstant.PRODUCT_CACHE);
        sbpProducts = JSON.parse(sbpProducts);
        productsData.forEach((product) => {
          const prod = product;
          if (sbpProducts && sbpProducts.indexOf(prod.partNumber) !== -1) prod.sbp = true;
        });
      } catch (e) {
        logger.error(e);
      }
      resolve();
    });
  };

  addAdditionalMobileEvents = (events) => {
    const { navigation, cartsSessionStatus, resetCartsSessionStatus } = this.props;
    const viaModule = (navigation && navigation.getParam('viaModule')) || '';
    if (viaModule === 'bag_carousel_module') {
      events.push('event81');
    }
    if (cartsSessionStatus) {
      events.push('scOpen');
      resetCartsSessionStatus();
    }
    return events;
  };

  // eslint-disable-next-line complexity
  startBagAnalytics = async (cartOrderItems, prevCartOrderItems) => {
    const { setClickAnalyticsDataBag, trackPageViewBag, router, updateBagPageData } = this.props;
    let events = ['scView', 'event80'];
    let fromMiniBag = false;
    if (!this.isMobileApp) {
      events.push('scOpen');
      fromMiniBag = utils.getObjectValue(router, false, 'query', 'fromMiniBag');
      this.getCustomEventsForAnalytics();
    }
    if (cartOrderItems !== prevCartOrderItems && events.length > 0) {
      const productsData = BagPageUtils.formatBagProductsData(cartOrderItems);
      const { SHOPPING_BAG, HEADER_CART } = BAGPAGE_CONSTANTS;
      updateBagPageData({
        pageName: SHOPPING_BAG,
        pageSection: SHOPPING_BAG,
        pageSubSection: SHOPPING_BAG,
        pageType: SHOPPING_BAG,
        pageShortName: SHOPPING_BAG,
      });
      const customData = {
        customEvents: events,
        products: productsData,
        pageNavigationText: fromMiniBag ? HEADER_CART : '',
      };
      if (this.isMobileApp) {
        const { navigation } = this.props;
        const icidParam = (navigation && navigation.getParam('icidParam')) || '';
        customData.internalCampaignId = icidParam;
        events = this.addAdditionalMobileEvents(events);
        customData.customEvents = events;
        await this.sbpAnalytics(productsData);
      }
      setClickAnalyticsDataBag(customData);
      trackPageViewBag({
        currentScreen: 'bagPage',
        pageData: {
          pageName: BAGPAGE_CONSTANTS.SHOPPING_BAG,
          pageSection: BAGPAGE_CONSTANTS.SHOPPING_BAG,
          pageSubSection: BAGPAGE_CONSTANTS.SHOPPING_BAG,
          pageType: BAGPAGE_CONSTANTS.SHOPPING_BAG,
          pageShortName: BAGPAGE_CONSTANTS.SHOPPING_BAG,
        },
      });
      this.setState({ triggerTracking: false });
    }
  };

  fetchInitialActions() {
    const { isRegisteredUserCallDone, initialActions, isRecalculateTaxes, isECOM, isGuest } =
      this.props;
    const isRecalc = isECOM && isRecalculateTaxes;
    if (isRegisteredUserCallDone) {
      initialActions(isRecalc, isGuest);
    }
  }

  async fetchSFLItems() {
    const { isRegisteredUserCallDone, fetchSflData } = this.props;
    const country = getSiteId() && getSiteId().toUpperCase();
    if (isRegisteredUserCallDone) {
      const sflItemsCount = this.isMobileApp
        ? await readCookie(SFL_ITEM_COUNTER)
        : await getSflItemCount(country);
      if (sflItemsCount > 0) {
        fetchSflData();
      }
    }
  }

  render() {
    const {
      labels,
      totalCount,
      orderItemsCount,
      orderItemsCountLocal,
      navigation,
      isUserLoggedIn,
      isPlcc,
      handleCartCheckout,
      closeCheckoutConfirmationModal,
      removeUnqualifiedItemsAndCheckout,
      isGuest,
      sflItems,
      cartSflMerged,
      fetchLabels,
      setVenmoInProgress,
      setAppleInProgress,
      isCartItemsUpdating,
      toastMessage,
      isCartItemSFL,
      isSflItemRemoved,
      isShowSaveForLaterSwitch,
      orderBalanceTotal,
      bagStickyHeaderInterval,
      toastMessagePositionInfo,
      cartItemSflError,
      isPayPalWebViewEnable,
      isPickupModalOpen,
      isMobile,
      bagPageServerError,
      isBagPage,
      setClickAnalyticsDataBag,
      cartOrderItems,
      isVenmoEnabled,
      isPayPalEnabledKillSwitch,
      isPayPalEnabled,
      isCartLoaded,
      trackPageViewBag,
      isBonusPointsEnabled,
      isModalOpen,
      bagLoading,
      isSflItemMovedToBag,
      excludedIds,
      productId,
      setCartSflMergedAction,
      isCheckoutFocused,
      appliedExpiredCoupon,
      isCartBackAbTestEnabled = false,
      isSignInBannerAbTestEnabled = false,
      isPlaceCashEnabled = false,
      setMaxItemErrorAction,
      setMaxItemErrorDisplayedAction,
      showMaxItemErrorDisplayed,
      isPlaceRewardEnabled = false,
      currentUserId,
      showMaxItemError,
      rememberedUser,
      isDisableBagAnchoring,
      isNewBag,
      isLowInventoryBannerEnabled,
      isLowInventoryBannerEnabledForOldDesign,
      isHideStickyCheckoutButton,
      bagPageLoading,
      quickViewLoader,
      quickViewProductId,
      fireHapticFeedback,
      isStickyCheckoutButtonEnabled,
    } = this.props;
    return (
      <BagPage
        isMobile={isMobile}
        labels={labels}
        totalCount={totalCount}
        orderItemsCount={orderItemsCount}
        orderItemsCountLocal={orderItemsCountLocal}
        showAddTobag={false}
        navigation={navigation}
        isUserLoggedIn={isUserLoggedIn}
        isPlcc={isPlcc}
        isGuest={isGuest}
        closeCheckoutConfirmationModal={closeCheckoutConfirmationModal}
        removeUnqualifiedItemsAndCheckout={removeUnqualifiedItemsAndCheckout}
        handleCartCheckout={handleCartCheckout}
        sflItems={sflItems}
        cartSflMerged={cartSflMerged}
        fetchLabels={fetchLabels}
        setVenmoPaymentInProgress={setVenmoInProgress}
        setApplePaymentInProgress={setAppleInProgress}
        isCartItemsUpdating={isCartItemsUpdating}
        toastMessage={toastMessage}
        isCartItemSFL={isCartItemSFL}
        isSflItemRemoved={isSflItemRemoved}
        isShowSaveForLaterSwitch={isShowSaveForLaterSwitch}
        orderBalanceTotal={orderBalanceTotal}
        bagStickyHeaderInterval={bagStickyHeaderInterval}
        toastMessagePositionInfo={toastMessagePositionInfo}
        cartItemSflError={cartItemSflError}
        isPayPalWebViewEnable={isPayPalWebViewEnable}
        isPickupModalOpen={isPickupModalOpen}
        bagPageServerError={bagPageServerError}
        isBagPage={isBagPage}
        cartOrderItems={cartOrderItems}
        setClickAnalyticsDataBag={setClickAnalyticsDataBag}
        isCartLoaded={isCartLoaded}
        trackPageViewBag={trackPageViewBag}
        fromMiniBag={this.fromMiniBag}
        bagLoading={bagLoading}
        isBonusPointsEnabled={isBonusPointsEnabled}
        isVenmoEnabled={isVenmoEnabled}
        isPayPalEnabledKillSwitch={isPayPalEnabledKillSwitch}
        isModalOpen={isModalOpen}
        isPayPalEnabled={isPayPalEnabled}
        isSflItemMovedToBag={isSflItemMovedToBag}
        excludedIds={excludedIds}
        setCartSflMergedAction={setCartSflMergedAction}
        productId={productId}
        isCheckoutFocused={isCheckoutFocused}
        appliedExpiredCoupon={appliedExpiredCoupon}
        isApplePayVisible={this.isApplePayVisible}
        isCartBackAbTestEnabled={isCartBackAbTestEnabled}
        isSignInBannerAbTestEnabled={isSignInBannerAbTestEnabled}
        rememberedUser={rememberedUser}
        isPlaceCashEnabled={isPlaceCashEnabled}
        showMaxItemErrorDisplayed={showMaxItemErrorDisplayed}
        setMaxItemErrorAction={setMaxItemErrorAction}
        setMaxItemErrorDisplayedAction={setMaxItemErrorDisplayedAction}
        isPlaceRewardEnabled={isPlaceRewardEnabled}
        currentUserId={currentUserId}
        showMaxItemError={showMaxItemError}
        isDisableBagAnchoring={isDisableBagAnchoring}
        bagPageLoading={bagPageLoading}
        isNewBag={isNewBag}
        isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
        isLowInventoryBannerEnabledForOldDesign={isLowInventoryBannerEnabledForOldDesign}
        isHideStickyCheckoutButton={isHideStickyCheckoutButton}
        quickViewLoader={quickViewLoader}
        quickViewProductId={quickViewProductId}
        fireHapticFeedback={fireHapticFeedback}
        isStickyCheckoutButtonEnabled={isStickyCheckoutButtonEnabled}
      />
    );
  }
}

// BagPageContainer.getInitActions = () => BAG_PAGE_ACTIONS.initActions;

BagPageContainer.pageInfo = {
  pageId: 'Bag',
};

BagPageContainer.getInitialProps = (reduxProps, pageProps) => {
  const DEFAULT_ACTIVE_COMPONENT = 'shopping bag';
  const loadedComponent = utils.getObjectValue(reduxProps, DEFAULT_ACTIVE_COMPONENT, 'query', 'id');
  const { dispatch } = reduxProps.store;
  dispatch(updateRecommendationsLoading({ isUpdating: true }));
  return {
    ...pageProps,
    ...{
      pageData: {
        pageName: BAGPAGE_CONSTANTS.SHOPPING_BAG,
        pageSection: loadedComponent,
        pageSubSection: BAGPAGE_CONSTANTS.SHOPPING_BAG,
        pageType: BAGPAGE_CONSTANTS.SHOPPING_BAG,
        pageShortName: BAGPAGE_CONSTANTS.SHOPPING_BAG,
        loadAnalyticsOnload: false,
      },
    },
  };
};

export const mapDispatchToProps = (dispatch) => {
  const { setCartSflMerged } = BAG_PAGE_ACTIONS;
  return {
    initialActions: (isRecalc, isGuest) => {
      dispatch(
        BAG_PAGE_ACTIONS.getCartData({
          isCartPage: true,
          translation: true,
          excludeCartItems: false,
          isRecalculateTaxes: isRecalc,
          supressCoupons: isGuest && getCartItemCount() === 0,
        })
      );
    },
    fetchNeedHelpContent: (contentIds) => {
      dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
    },
    fetchSflData: () => {
      dispatch(BAG_PAGE_ACTIONS.getSflData());
    },
    fetchLabels: () => {
      dispatch(BAG_PAGE_ACTIONS.initActions[0]);
    },
    setVenmoInProgress: (data) => {
      if (!isMobileApp()) dispatch(setVenmoPaymentInProgress(data));
    },
    setAppleInProgress: (data) => dispatch(setApplePaymentInProgress(data)),
    setVenmoPickupState: (data) => {
      if (!isMobileApp()) dispatch(setVenmoPickupMessageState(data));
    },
    setVenmoShippingState: (data) => {
      if (!isMobileApp()) dispatch(setVenmoShippingMessageState(data));
    },
    toastMessage: (palyoad) => {
      dispatch(toastMessageInfo(palyoad));
    },
    toastMessagePositionInfo: (palyoad) => {
      dispatch(toastMessagePosition(palyoad));
    },
    setClickAnalyticsDataBag: (payload) => {
      dispatch(setClickAnalyticsData(payload));
    },
    trackClickAction: (data) => {
      dispatch(trackClick(data));
    },
    resetAnalyticsData: () => {
      dispatch(resetClickAnalyticsData());
    },
    trackPageViewBag: (payload) => {
      dispatch(trackPageView(payload));
    },
    updateBagPageData: (payload) => {
      dispatch(updatePageData(payload));
    },
    clearCheckoutServerError: (data) => dispatch(CHECKOUT_ACTIONS.setServerErrorCheckout(data)),
    resetBagLoadedState: () => {
      dispatch(BAG_PAGE_ACTIONS.resetBagLoadedState());
    },
    resetEDDCall: () => {
      dispatch(hideLoader());
      dispatch(clearEddTTL());
      dispatch(resetEddData());
    },
    setCartSflMergedAction: (payload) => {
      dispatch(setCartSflMerged(payload));
    },
    setCouponMergeErrorAction: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.setCouponMergeError(payload));
    },
    setMaxItemErrorAction: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.setMaxItemError(payload));
    },
    setMaxItemErrorDisplayedAction: (payload) => {
      dispatch(BAG_PAGE_ACTIONS.setMaxItemErrorDisplayed(payload));
    },
    getHomePageLayout: (apiConfig) => {
      dispatch(fetchPageLayout({ pageName: 'home', apiConfig }));
    },
    resetCartsSessionStatus: () => {
      dispatch(setCartsSessionStatus(0));
    },
    fireHapticFeedback: (payload) => {
      dispatch(playHapticFeedback(payload));
    },
  };
};

const getLabels = createSelector(
  [BagPageSelector.getBagPageLabels, getLabelsCartItemTile],
  (bagPageLabels, cartItemTileLabels) => {
    return { ...bagPageLabels, ...cartItemTileLabels };
  }
);

export const mapStateToProps = (state) => {
  const { size = false } = getCartOrderList(state) || {};
  return {
    isMobile: checkoutSelectors.getIsMobile(),
    labels: getLabels(state),
    totalCount: BagPageSelector.getTotalItems(state),
    productsTypes: BagPageSelector.getProductsTypes(state),
    orderItemsCount: size,
    orderItemsCountLocal: BagPageSelector.getOrderItemsLocal(state)
      ? BagPageSelector.getOrderItemsLocal(state).size
      : 0,
    airmilesBannerContentId: PlaceCashSelector.getAirmilesBannerContentInfo(state),
    isVenmoEnabled: checkoutSelectors.getIsVenmoEnabled(state),
    isApplePayEnabled: checkoutSelectors.getIsApplePayEnabled(state),
    isApplePayRememberedUser: checkoutSelectors.getIsApplePayRememberedUser(state),
    rememberedUser: isRememberedUser(state),
    isPayPalEnabledKillSwitch: getIsPayPalEnabled(state),
    isPayPalEnabled: BagPageSelector.getIsPayPalEnabled(state),
    isUserLoggedIn: getUserLoggedInState(state),
    isGuest: isGuestUser(state),
    isPlcc: isPlccUser(state),
    bagPageLoading: BagPageSelector.getIsBagPageLoading(state),
    sflItems: BagPageSelector.getsflItemsList(state),
    cartSflMerged: BagPageSelector.getCartSflMerged(state),
    isCartItemsUpdating: getIsCartItemsUpdating(state),
    isCartItemSFL: getIsCartItemsSFL(state),
    isSflItemRemoved: getIsSflItemRemoved(state),
    isSflItemMovedToBag: getIsSflItemMovedToBag(state),
    isShowSaveForLaterSwitch: getSaveForLaterSwitch(state),
    orderBalanceTotal: getGrandTotal(state) - getGiftCardsTotal(state),
    bagStickyHeaderInterval: BagPageSelector.getBagStickyHeaderInterval(state),
    cartItemSflError: getCartItemsSflError(state),
    isPayPalWebViewEnable: BagPageSelector.getPayPalWebViewStatus(state),
    currencySymbol: BagPageSelector.getCurrentCurrency(state) || '$',
    isRegisteredUserCallDone: getIsRegisteredUserCallDone(state),
    isPickupModalOpen: getIsPickupModalOpen(state),
    isModalOpen: getModalState(state),
    bagPageServerError: checkoutSelectors.getCheckoutServerError(state),
    cartOrderItems: BagPageSelector.getOrderItems(state),
    isCartLoaded: BagPageSelector.getCartLoadedState(state),
    bagPageIsRouting: BagPageSelector.isBagRouting(state),
    isCartSupressOnBagUpdate: BagPageSelector.getCartSupressOnBagUpdate(state),
    bagLoading: BagPageSelector.isBagLoading(state),
    isBonusPointsEnabled: getBonusPointsSwitch(state),
    excludedIds: BagPageSelector.getCartProductIds(state),
    productId: BagPageSelector.getMostExpensiveItemId(state),
    pageName: getPageName(state),
    venmoOrderConfirmationId: ConfirmationSelector.getVenmoOrderConfirmationContentId(
      state,
      'Venmo_Order_Confirmation'
    ),
    isCheckoutFocused: checkoutSelectors.isCheckoutFocused(state),
    appliedExpiredCoupon: getAppliedExpiredCouponListState(state),
    isRecalculateTaxes: getIsChangeRecalForEcom(state),
    isECOM: BagPageSelector.getECOMStatus(state),
    isCartBackAbTestEnabled: BagPageSelector.getCartBackAbTest(state),
    isSignInBannerAbTestEnabled: BagPageSelector.getSignInBannerAbTest(state),
    isPlaceCashEnabled: BagPageSelector.getIsPlaceCashEnabled(state),
    isPlaceRewardEnabled: BagPageSelector.getIsPlaceRewardEnabled(state),
    currentUserId: getUserId(state),
    showMaxItemErrorDisplayed: BagPageSelector.getShowMaxItemErrorDisplayed(state),
    showMaxItemError: BagPageSelector.getShowMaxItemError(state),
    layouts: state.Layouts,
    cartsSessionStatus: getCartsSessionStatus(state),
    isDisableBagAnchoring: getIsDisableBagAnchoring(state),
    isNewBag: getIsNewBag(state),
    isLowInventoryBannerEnabled: getIsShowLowInventoryBanner(state),
    isHideStickyCheckoutButton: getIsHideStickyCheckoutButton(state),
    isLowInventoryBannerEnabledForOldDesign: getIsShowLowInventoryBannerForOldDesign(state),
    quickViewLoader: getQuickViewLoader(state),
    quickViewProductId: getUniqueId(state),
    isStickyCheckoutButtonEnabled: getIsStickyCheckoutAbTestEnabled(state),
  };
};

BagPageContainer.propTypes = {
  fetchNeedHelpContent: PropTypes.func.isRequired,
  resetAnalyticsData: PropTypes.func.isRequired,
  venmoOrderConfirmationId: PropTypes.string.isRequired,
  airmilesBannerContentId: PropTypes.string.isRequired,
  setVenmoPickupState: PropTypes.func.isRequired,
  setVenmoShippingState: PropTypes.func.isRequired,
  setVenmoInProgress: PropTypes.func.isRequired,
  isRegisteredUserCallDone: PropTypes.bool.isRequired,
  router: PropTypes.shape({}).isRequired,
  bagPageIsRouting: PropTypes.bool.isRequired,
  isCartLoaded: PropTypes.bool.isRequired,
  pageName: PropTypes.string.isRequired,
  cartOrderItems: PropTypes.shape([]).isRequired,
  resetBagLoadedState: PropTypes.func.isRequired,
  setClickAnalyticsDataBag: PropTypes.func.isRequired,
  trackPageViewBag: PropTypes.func.isRequired,
  updateBagPageData: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  initialActions: PropTypes.func.isRequired,
  fetchSflData: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  totalCount: PropTypes.number.isRequired,
  orderItemsCount: PropTypes.number.isRequired,
  orderItemsCountLocal: PropTypes.number.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  handleCartCheckout: PropTypes.func.isRequired,
  closeCheckoutConfirmationModal: PropTypes.func.isRequired,
  removeUnqualifiedItemsAndCheckout: PropTypes.func.isRequired,
  isGuest: PropTypes.bool.isRequired,
  sflItems: PropTypes.shape([]).isRequired,
  cartSflMerged: PropTypes.bool.isRequired,
  fetchLabels: PropTypes.func.isRequired,
  isCartItemsUpdating: PropTypes.bool.isRequired,
  toastMessage: PropTypes.string.isRequired,
  isCartItemSFL: PropTypes.bool.isRequired,
  isSflItemRemoved: PropTypes.bool.isRequired,
  isShowSaveForLaterSwitch: PropTypes.bool.isRequired,
  orderBalanceTotal: PropTypes.number.isRequired,
  bagStickyHeaderInterval: PropTypes.number.isRequired,
  toastMessagePositionInfo: PropTypes.func.isRequired,
  isPayPalWebViewEnable: PropTypes.bool.isRequired,
  isPickupModalOpen: PropTypes.bool.isRequired,
  isMobile: PropTypes.bool.isRequired,
  isBagPage: PropTypes.bool.isRequired,
  isVenmoEnabled: PropTypes.bool.isRequired,
  isPayPalEnabled: PropTypes.bool.isRequired,
  isBonusPointsEnabled: PropTypes.bool.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  bagLoading: PropTypes.bool.isRequired,
  isSflItemMovedToBag: PropTypes.bool.isRequired,
  excludedIds: PropTypes.string.isRequired,
  productId: PropTypes.string.isRequired,
  isCheckoutFocused: PropTypes.bool.isRequired,
  cartItemSflError: PropTypes.string.isRequired,
  bagPageServerError: PropTypes.shape({}).isRequired,
  clearCheckoutServerError: PropTypes.func.isRequired,
  appliedExpiredCoupon: PropTypes.shape({}).isRequired,
  setCartSflMergedAction: func.isRequired,
  resetEDDCall: PropTypes.func.isRequired,
  setAppleInProgress: PropTypes.func.isRequired,
  isApplePayEnabled: PropTypes.bool.isRequired,
  isRecalculateTaxes: PropTypes.bool.isRequired,
  isECOM: PropTypes.bool.isRequired,
  rememberedUser: PropTypes.bool.isRequired,
  isApplePayRememberedUser: PropTypes.bool.isRequired,
  isPayPalEnabledKillSwitch: PropTypes.bool.isRequired,
  isCartBackAbTestEnabled: PropTypes.bool.isRequired,
  isSignInBannerAbTestEnabled: PropTypes.bool.isRequired,
  isPlaceCashEnabled: PropTypes.bool.isRequired,
  isPlaceRewardEnabled: PropTypes.bool.isRequired,
  isCartSupressOnBagUpdate: PropTypes.bool.isRequired,
  currentUserId: PropTypes.string.isRequired,
  showMaxItemError: PropTypes.bool.isRequired,
  showMaxItemErrorDisplayed: bool.isRequired,
  setCouponMergeErrorAction: PropTypes.func.isRequired,
  setMaxItemErrorDisplayedAction: PropTypes.func.isRequired,
  setMaxItemErrorAction: PropTypes.func.isRequired,
  layouts: PropTypes.shape({}).isRequired,
  getHomePageLayout: PropTypes.func.isRequired,
  cartsSessionStatus: PropTypes.number.isRequired,
  resetCartsSessionStatus: PropTypes.func.isRequired,
  isDisableBagAnchoring: PropTypes.bool,
  trackClickAction: PropTypes.func.isRequired,
  isNewBag: PropTypes.bool,
  isLowInventoryBannerEnabled: PropTypes.bool,
  isLowInventoryBannerEnabledForOldDesign: PropTypes.bool,
  bagPageLoading: PropTypes.bool,
  screenProps: PropTypes.shape({}),
  fireHapticFeedback: () => {},
};

BagPageContainer.defaultProps = {
  isDisableBagAnchoring: false,
  isNewBag: false,
  isLowInventoryBannerEnabled: false,
  isLowInventoryBannerEnabledForOldDesign: false,
  bagPageLoading: false,
  screenProps: {},
  fireHapticFeedback: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(BagPageContainer);
