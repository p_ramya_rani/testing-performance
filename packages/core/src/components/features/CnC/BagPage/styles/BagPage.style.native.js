// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const getAdditionalStyle = (props) => {
  const { margins, paddings } = props;
  return {
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
  };
};

export const Margin = styled.View`
  justify-content: center;
  ${getAdditionalStyle};
`;

export const WrapperStyle = styled.View`
  flex: 1;
`;

export const ScrollViewWrapper = styled.ScrollView`
  height: ${(props) => props.viewHeight};
`;

export const HeadingViewStyle = styled.TouchableOpacity`
  width: 50%;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
    ${(props) => props.theme.spacing.ELEM_SPACING.XS} 0;
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const SflHeadingViewStyle = styled.TouchableOpacity`
  width: 50%;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
    ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 0;
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const HeadingTextStyle = styled.Text`
  font-size: ${(props) => props.theme.typography.fontSizes.fs18};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  text-align: center;
  width: 100%;
`;

export const InActiveBagHeaderText = styled.Text`
  font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  color: ${(props) => props.theme.colors.TEXT.DARKGRAY};
`;

export const ActiveBagHeaderText = styled.Text`
  font-weight: ${(props) => props.theme.fonts.fontWeight.black};
`;

export const ActiveBagHeaderView = styled.View`
  border-bottom-color: ${(props) => props.theme.colorPalette.black};
  border-bottom-width: 2;
`;

export const InActiveBagHeaderView = styled.View``;

export const RowSectionStyle = styled.View`
  width: 100%;
  background: #fff;
`;

export const BonusPointsWrapper = styled.View`
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

export const NotificationContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin-top: ${(props) => props.marginTop};
  z-index: 1;
`;

export const MainSection = styled.View`
  flex: 1;
  background: #f3f3f3;
  margin-bottom: 3px;
`;

export const BagHeaderRow = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: 0 ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
`;
export const BagHeaderMain = styled.View`
  display: flex;
`;

export const SuccessTickImage = styled.Image`
  width: 23px;
  height: 23px;
  margin-right: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
`;

export const SuccessMessageContainer = styled.View`
  flex-direction: row;
  align-items: center;
  border-width: 2px;
  border-color: ${(props) => props.theme.colors.NOTIFICATION.SUCCESS};
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const HeadingTextStyleView = styled.View`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const ActiveBagHeaderTextNew = styled.Text`
  font-weight: ${(props) => props.theme.fonts.fontWeight.black};
  font-size: ${(props) => props.theme.typography.fontSizes.fs18};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  text-align: center;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
`;

export const InActiveBagHeaderTextView = styled.Text`
  font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  color: ${(props) => props.theme.colors.TEXT.DARKGRAY};
  font-size: ${(props) => props.theme.typography.fontSizes.fs18};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  text-align: center;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
`;

export const EstimateTextStyle = styled.Text`
  font-size: ${(props) => props.theme.typography.fontSizes.fs10};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  font-weight: ${(props) => props.theme.fonts.fontWeight.light};
  text-align: center;
  width: 100%;
`;
export const InActiveEstimateTextStyle = styled.Text`
  font-size: ${(props) => props.theme.typography.fontSizes.fs10};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  color: ${(props) => props.theme.colors.TEXT.DARKGRAY};
  text-align: center;
  width: 100%;
`;

const applyPositionClassStyle = (props) => {
  if (props.isPayPalWebViewEnable) {
    return `
    top: 0;
    height:100%;
    `;
  }
  return 'bottom: 0; min-height: 60px;';
};

export const FooterView = styled.View`
  width: 100%;
  min-height: 60px;
  position: absolute;
  background-color: ${(props) => props.theme.colors.WHITE};
  ${applyPositionClassStyle}
`;

export const ContainerMain = styled.View`
  flex: ${(props) => (props.selectedTab ? '0.9' : '1')};
`;

export const RecommendationView = styled.View`
  background-color: rgba(243, 243, 243, 1);
`;

export const RecommendationWrapper = styled.View`
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.XL} 0 60px 0;
`;

export const PlaceRewardRecommendationWrapper = styled.View`
  margin-top: 5px;
  height: ${(props) => (props.children.props.placeRewardRecoHeight ? 'auto' : '85px')};
  min-height: 80px;
  display: flex;
  overflow: hidden;
`;

export const PlaceCashRecommendationWrapper = styled.View`
  margin-top: 5px;
  height: ${(props) => (props.children.props.placeCashRecoHeight ? 'auto' : '95px')};
  min-height: 80px;
  display: flex;
  overflow: hidden;
`;

export const PlaceCashRecommendation = styled.View`
  border-radius: 14px;
  border: solid 4px ${(props) => props.theme.colors.REWARDS.PLACE_CASH};
  width: 100%;
  height: auto;
  background-color: #fff;
  margin: 10px 0px;
  display: ${(props) => (props.showPlaceCashRecommendation ? 'flex' : 'none')};
  overflow: hidden;
`;

export const PlaceRewardRecommendation = styled.View`
  border-radius: 14px;
  border-style: solid;
  border-width: 4px;
  border-left-color: ${(props) => props.theme.colors.REWARDS.PLCC};
  border-top-color: ${(props) => props.theme.colors.REWARDS.PLCC};
  border-right-color: ${(props) => props.theme.colors.REWARDS.NON_PLCC};
  border-bottom-color: ${(props) => props.theme.colors.REWARDS.NON_PLCC};
  display: ${(props) => (props.showPlaceRewardRecommendation ? 'flex' : 'none')};
  width: 100%;
  height: auto;
  background-color: #fff;
  margin: 10px 0px;
  overflow: hidden;
`;

export const PlaceCashRecoToggleButton = styled.TouchableOpacity`
  display: flex;
  width: 100px;
  justify-content: center;
  align-items: center;
  overflow: hidden;
`;

export const PlaceCashToggleButton = styled.Image`
  padding: 6px 10px;
  margin: 5px;
  width: 20px;
`;

export const ImageStyleWrapper = styled.View`
  justify-content: center;
  align-items: center;
  background-color: #fff;
  height: 25px;
  width: 98%;
  border-radius: 14px;
  overflow: hidden;
`;

export const HorizontalLine = styled.View`
  width: 90%;
  border-top-color: ${(props) => props.theme.colors.PRIMARY.GRAY};
  border-top-width: 1;
  align-self: center;
  padding-bottom: 10px;
`;

export default {
  HeadingViewStyle,
  MainSection,
  RowSectionStyle,
  HeadingTextStyle,
  ScrollViewWrapper,
  BonusPointsWrapper,
  BagHeaderRow,
  SflHeadingViewStyle,
  InActiveBagHeaderText,
  ActiveBagHeaderText,
  ActiveBagHeaderView,
  InActiveBagHeaderView,
  SuccessTickImage,
  SuccessMessageContainer,
  HeadingTextStyleView,
  EstimateTextStyle,
  ActiveBagHeaderTextNew,
  InActiveBagHeaderTextView,
  InActiveEstimateTextStyle,
  BagHeaderMain,
  FooterView,
  ContainerMain,
  RecommendationWrapper,
  RecommendationView,
  NotificationContainer,
  PlaceCashRecommendation,
  PlaceCashRecommendationWrapper,
  PlaceRewardRecommendation,
  PlaceRewardRecommendationWrapper,
  PlaceCashRecoToggleButton,
  PlaceCashToggleButton,
  ImageStyleWrapper,
  HorizontalLine,
};
