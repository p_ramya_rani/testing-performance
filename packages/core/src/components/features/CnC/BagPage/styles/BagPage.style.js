/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const radiusSix = (props) => `
    ${props.newBag && props.theme.brand !== 'gym' ? `border-radius: 6px` : ''};
`;

const styles = css`
  .ledgerForMobile {
    display: none;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: block;
    }
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .hideLoyaltyBanner {
      ${(props) =>
        props.isStickyCheckoutButtonEnabled &&
        `
      display: none;`}
    }
  }
  .row-ele {
    width: 100%;
    margin: 15px 0;
    background: ${(props) => props.theme.colors.WHITE};
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .many-item-recommendation-non-empty-bag {
      .product-image-container {
        height: 130px;
      }
    }
  }
  .order-summary {
    padding-bottom: 0;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin: 16px 0;
      .checkoutBtnTracker {
        flex: none;
      }
    }
    .item-closed {
      .elem-mb-MED {
        margin-bottom: 0;
      }
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .overwrite-recommendation-css {
      .smooth-scroll-list-item {
        min-width: 130px !important;
      }

      .smooth-scroll-list-item:last-child {
        margin-right: 100px;
      }
    }
  }

  .cartPageTitleHeader {
    display: flex;
    justify-content: center;
    @media ${(props) => props.theme.mediaQuery.medium} {
      justify-content: initial;
      pointer-events: none;
    }
  }

  .information-header {
    padding-left: 13px;
    padding-right: 13px;
    padding-top: 1px;
  }

  .with-condensed-header .information-header {
    flex: 1;
    background-color: ${(props) => props.theme.colors.WHITE};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .bag-header {
    margin: 0;
    padding: 20px 0 20px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      text-align: center;
      padding: 13px 0 0;
      margin: 0;
      font-size: ${(props) => props.theme.fonts.fontSize.anchor.xlarge}px;
      font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-left: 3px;
    }
  }

  .delete-msg {
    position: sticky;
    z-index: 1111;
    width: 100%;
    box-sizing: border-box;
    top: 143px;
    border: solid 2px ${(props) => props.theme.colors.NOTIFICATION.SUCCESS};
    text-align: left;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    background: ${(props) => props.theme.colors.WHITE};
    display: flex;
    align-items: center;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    .tick-icon {
      height: 23px;
      width: 23px;
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
  }

  .estimatedHeaderText {
    display: none;
  }

  .bag-condensed-header {
    display: none;
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .bag-condensed-header {
      display: block;
      box-sizing: border-box;
      position: fixed;
      left: 0;
      right: 0;
      z-index: ${(props) => props.theme.zindex.zLoader};
      background: ${(props) => props.theme.colors.WHITE};
      width: 100%;
      margin: 0;
      border-bottom: 1px solid ${(props) => props.theme.colorPalette.gray[300]};
    }
    .bagHeaderText {
      display: flex;
      align-items: center;
    }
  }

  .hidden-condensed-header {
    display: none;
  }

  .bag-section {
    & > .tile-header {
      border-bottom: none;
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .activeSection {
      display: block;
    }
    .inActiveSection {
      display: none;
    }
    .activeHeader {
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      font-size: ${(props) => props.theme.typography.fontSizes.fs18};
    }
    .estimatedHeaderText {
      display: block;
      text-align: center;
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
    .activeEstimatedHeader {
      border-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS} solid
        ${(props) => props.theme.colors.TEXT.DARKERGRAY};
      &.save-for-later {
        margin-top: 10px;
      }
    }
    .stickyBagHeader {
      margin-left: 0;
      margin-right: 0;
      position: sticky;
      z-index: ${(props) => props.theme.zindex.zLoader};
      background: ${(props) => props.theme.colors.WHITE};
    }
  }

  .save-for-later-section-heading {
    display: block;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }

  .bag-header-sfl {
    display: none;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: block;
      font-size: ${(props) => props.theme.typography.fontSizes.fs18};
      text-transform: uppercase;
    }
  }

  .recentlyViewed {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }
  .may-like-recommendation {
    margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
  }

  .cart-placecash {
    border-radius: 14px;
    border: solid 4px ${(props) => props.theme.colors.REWARDS.PLACE_CASH};
    margin: 10px 0 0 0;
    width: 100%;
    height: auto;
    background: ${(props) => props.theme.colors.WHITE};
    box-sizing: border-box;
    position: relative;
    overflow: hidden;
    @media ${(props) => props.theme.mediaQuery.large} {
      display: inline-flex;
      justify-content: center;
      align-items: stretch;
    }
  }

  .cart-placecash.hide-content {
    display: none;
  }

  .cart-placecash .cart-placecash-button {
    background-color: ${(props) => props.theme.colors.REWARDS.PLACE_CASH_BACKGROUND};
    width: 100%;
    text-align: center;
    position: relative;
    overflow: hidden;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      border-top-left-radius: 14px;
      border-top-right-radius: 14px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 37%;
    }
  }

  .cart-placecash .place-cash {
    padding: 16px 10px 5px;
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 0px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      width: 100%;
      box-sizing: border-box;
    }
  }

  .cart-placecash .place-cash__text-wrapper {
    position: static;
    -webkit-transform: none;
    -ms-transform: none;
    transform: none;
  }

  .cart-placecash .progress-bar__outer-container {
    margin: 0 8px;
    border: 1px solid ${(props) => props.theme.colors.REWARDS.PLACE_CASH};
    border-radius: 16.5px;
    background: ${(props) => props.theme.colors.WHITE};
  }

  .cart-placecash .progress-bar__outer-container__all {
    justify-content: center;
    align-items: center;
    margin: 16px auto;
  }

  .cart-placecash .progress-bar__outer-container__startvalue,
  .cart-placecash .progress-bar__outer-container__endvalue {
    margin: 0;
    display: inline-table;
    width: auto;
  }

  .cart-placecash .progress-bar__outer-container__startvalue span,
  .cart-placecash .progress-bar__outer-container__endvalue span {
    font-size: 12px;
  }

  .cart-placecash .progress-bar__title-text,
  .cart-placecash .progress-bar__amount-text {
    font-size: 14px;
  }
  .cart-placecash .progress-bar__text-wrapper {
    width: 90%;
  }
  .cart-placecash .progress-bar__title-text .progress-bar_redeem-dates {
    font-weight: ${(props) => props.theme.fonts.fontWeight.light};
  }

  .cart-placecash .progress-bar__status-bar {
    height: 20px;
  }

  .cart-placecash .place-cash__tnc-container {
    font-size: 10px;
  }

  .cart-placecash .place-cash__tnc-text {
    font-size: 10px;
  }

  .cart-placecash .place-cash__modalLink {
    font-size: 10px;
    text-transform: capitalize;
  }

  .cart-placecash .horizontal-line {
    border-top: solid 1px ${(props) => props.theme.colors.TEXT.LIGHTGRAY};
    width: 90%;
    text-align: center;
    display: none;
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      display: block;
    }
  }

  .cart-placecash.placereward {
    border-left: solid 4px ${(props) => props.theme.colors.REWARDS.PLCC};
    border-top: solid 4px ${(props) => props.theme.colors.REWARDS.PLCC};
    border-right: solid 4px ${(props) => props.theme.colors.REWARDS.NON_PLCC};
    border-bottom: solid 4px ${(props) => props.theme.colors.REWARDS.NON_PLCC};
  }

  .cart-placecash.plcc-user .cart-placecash-button {
    background: ${(props) => props.theme.colors.WHITE};
  }
  .cart-placecash.plcc-user .progress-bar__status-bar {
    background-image: repeating-linear-gradient(-76deg, #62c9ec 0 29px, #00a7e0 29px 56px);
  }

  .cart-placecash.plcc-user .progress-bar__outer-container {
    border: 1px solid ${(props) => props.theme.colors.REWARDS.PLCC};
  }

  .cart-placecash.no-plcc-user .cart-placecash-button {
    background: ${(props) => props.theme.colors.WHITE};
  }
  .cart-placecash.no-plcc-user .progress-bar__status-bar {
    background-image: repeating-linear-gradient(-76deg, #f9b662 0 29px, #f7971f 29px 56px);
  }

  .cart-placecash.no-plcc-user .progress-bar__outer-container {
    border: 1px solid ${(props) => props.theme.colors.REWARDS.NON_PLCC};
  }

  .cart-placecash .my-place-image {
    height: 14px;
    position: relative;
    top: 3px;
    @media ${(props) => props.theme.mediaQuery.large} {
      height: 14px;
      padding: 5px 0;
      top: 0;
      display: block;
      margin: 0 auto;
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .cart-placecash .cart-placecash-product {
      width: 63%;
      position: relative;
      display: inline-block;
    }
  }
`;

export const recommendationStylesPdp = css`
  .accordion-recommendation {
    font-family: Nunito;
    font-size: 14px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    letter-spacing: 0.2px;
    text-align: center;
    color: ${(props) => props.theme.colors.TEXT.DARK};
    margin: 14px auto 14px;
    pointer-events: none;
    border-top: 0;
    padding: 0;
    background: none;
    line-height: 1;
    text-transform: none;
    width: 85%;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 22px auto 4px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      margin: 14px auto 6px;
    }
  }

  .recommendations-container {
    padding: 0;
    overflow: hidden;
  }

  .recommendations-header {
    text-align: left;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.anchor.medium}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.semibold};
    margin: 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 10px;
    letter-spacing: normal;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin: 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 47px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.fonts.fontSize.anchor.large}px;
      font-weight: ${(props) => props.theme.fonts.fontWeight.black};
      margin: 0 0 18px 61px;
    }
  }
  && .smooth-scroll-list {
    padding-bottom: 10px;
    .smooth-scroll-list-item {
      margin: 2px 12px 2px 0px;
      position: relative;
      width: 38%;
      padding: 0px;
      border-radius: 11px;
      box-shadow: 0 2px 6px 0px rgb(0 0 0 / 10%);
      left: 10px;
      min-height: 257px;
      height: 257px;
      .product-list {
        margin: 0px;
      }
    }
  }

  .slick-slide {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin: 2px 10px 5px 0px;
      position: relative;
      padding: 0px;
      border-radius: 11px;
      box-shadow: 0 2px 6px 0px rgb(0 0 0 / 10%);
      height: auto;
    }
  }

  .slick-track {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      padding: 6px;
    }
  }

  .favorite-move-purchase-section {
    display: none !important;
  }

  && .item-container-inner {
    padding: 8px 7px 0 7px;
  }
  && .product-image-container {
    height: 131px;
    min-height: 131px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 100%;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
      height: 131px !important;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      height: 144px;
      min-height: 144px;
      margin: 4px;
      float: left;
      width: 48%;
      position: relative;
    }
  }
  && .container-price {
    p {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      margin-top: 0px;
      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: 20px;
        margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      }
    }
    span {
      font-size: ${(props) => props.theme.typography.fontSizes.fs10};
      @media ${(props) => props.theme.mediaQuery.large} {
        font-size: 14px;
        display: block;
      }
    }
  }
  && .custom-ranking-container {
    img {
      width: 60px;
      height: 12px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        width: 63px;
        height: 13px;
      }
    }
    .reviews-count {
      font-size: 10px;
      color: ${(props) => props.theme.colors.TEXT.DARK};
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        margin-top: 3px;
      }
      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-top: 0px;
      }
    }
  }
  && .added-to-bag {
    font-size: 10px;
    padding: 8px 0;
    min-height: 42px;
    letter-spacing: normal;
    margin-top: 0px;
    ${(props) => radiusSix(props)}
    @media ${(props) => props.theme.mediaQuery.large} {
      min-height: 33px;
      margin-top: 6px;
    }
  }

  .recommendations-section-row {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin: 10px 2px;
    }
    .no-carousel-container {
      ul {
        width: 143px;
      }
    }
  }

  .ranking-wrapper {
    min-height: 23px;
    @media ${(props) => props.theme.mediaQuery.large} {
      min-height: 22px;
    }
  }

  .fulfillment-section,
  .ranking-wrapper {
    margin-top: 0px !important;
  }

  .product-wishlist-container + .fulfillment-section {
    margin-top: 23px !important;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 22px !important;
    }
  }

  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    .no-carousel-container {
      ul {
        width: 115px;
      }
    }
    && .slick-prev {
      left: -8.5%;
    }
    && .slick-next {
      right: -8.5%;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .tcp_carousel_wrapper {
      height: 210px;
    }
    && .recommendation-height-toggle {
      display: none;
    }
    && .slick-slide {
      height: 156px;
      margin: 3px 11px;
      padding: 0px;
      border-radius: 11px;
      box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
    }
    && .item-container-inner {
      padding: 0px;
      display: block;
    }
    && .no-carousel-container {
      ul {
        width: 135px;
      }
    }
    && .product-wishlist-container > div:first-child {
      display: none;
      height: 0px;
    }
    && .product-wishlist-container {
      width: 47%;
      float: left;
      margin-top: auto;
      position: relative;
      min-height: 80px;
      height: 80px;
      padding-top: 0px;
    }
    && .fulfillment-section {
      width: 43%;
      float: left;
    }
    && .ranking-wrapper {
      width: 47%;
      float: left;
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    }

    && .slick-prev {
      left: -3.5%;
    }
    && .slick-next {
      right: -4.5%;
    }
  }
  && .slick-list {
    margin-right: 0;
    overflow: none;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      min-height: 280px;
    }
  }
  && .slick-dots {
    bottom: -25px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      bottom: 0px;
    }
  }
`;

export const addedToBagActionsStyles = css`
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    bottom: 0;
    left: 0;
    position: fixed;
    width: 92%;
    background: ${(props) => props.theme.colors.WHITE};
    z-index: ${(props) => props.theme.zindex.zLoader};
    margin: 0;
    border-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS} solid
      ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
      ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    ${(props) =>
      props.isStickyCheckoutButtonEnabled &&
      `
    position:relative;
    width:auto;
    border-top:none;

    `}
  }

  margin-bottom: 0px;

  .checkout-button.checkout-button-bagHeader {
    flex-direction: row-reverse;
    margin-top: 7px;
    float: right;
    padding: 0 3px 0 13px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      .checkoutBtnTracker {
        max-width: 140px;
        margin-right: 10px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      .checkoutBtnTracker {
        margin-right: 2px;
      }
    }
    .checkout {
      height: 42px;
      padding: 12px 0;
    }
    .paypal-venmo {
      flex: 2;
      display: inline-flex;
      margin-bottom: 7px;
      ${(props) => (!props.isVenmoEnabled && !props.isPayPalEnabled ? `flex: none;` : '')};
      .paypal-wrapper {
        min-width: auto;
      }
      .venmo-button {
        height: 42px;
      }
    }
    .checkoutBtn-wrapper {
      @media ${(props) => props.theme.mediaQuery.medium} {
        ${(props) => (!props.newMiniBag ? 'max-width: 140px; margin-right: 2px;' : '')};
      }
    }
  }

  .checkout-sticky-header {
    margin-right: ${(props) =>
      props.isApplePayVisible ? '' : props.theme.spacing.ELEM_SPACING.XS};
  }

  .checkout-button {
    display: flex;
    flex: 1;
    flex-direction: column-reverse;
    width: 100%;
    margin: 0;
    padding: 0 13px 0;
    box-sizing: border-box;
    margin-top: 35px;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-top: 0;
      padding: 0;
      ${(props) =>
        (!props.isVenmoEnabled && !(props.isApplePayVisible && !props.isMiniBag)) ||
        (!props.isPayPalEnabled && !props.isPayPalEnabledKillSwitch)
          ? `flex-direction: row;
          .checkoutBtnTracker {
            flex: 1;
            padding-left: 8px;
          }`
          : ''};
      ${(props) =>
        (!props.isVenmoEnabled && !props.isPayPalEnabled) ||
        (props.isApplePayVisible && !props.isMiniBag && props.isPayPalEnabled)
          ? `.checkoutBtnTracker {
              flex: 1;
              padding-left: 0;
            }`
          : ''};
      .checkout {
        height: 42px;
        padding: 12px 0;
      }
      .paypal-wrapper {
        min-width: 170px;
        height: 42px;
      }
      .venmo-button {
        min-width: 170px;
        height: 42px;
      }
    }
  }

  button.checkout {
    width: 100%;
    margin: 0 0 10px 0;
    padding: 16px 0;
    ${(props) => radiusSix(props)}
    ${(props) => props.theme.isGymboree && !props.isMiniBag && 'border-radius: 25.5px;'}
  }
`;

export const recommendationStyles = css`
  /* stylelint-disable no-descending-specificity */
  .recommendations-container {
    padding: 16px 0 24px 0;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 24px 0 32px 0;
    }
  }
  .smooth-scroll-list-item {
    width: 33%;
    margin-right: 0;
  }
  .custom-ranking-container {
    img {
      width: 68px;
      height: 13px;
    }
    .reviews-count {
      font-size: 10px;
      color: ${(props) => props.theme.colors.TEXT.DARK};
      margin-top: 1px;
    }
    line-height: 15px;
  }
  /* stylelint-enable no-descending-specificity */

  .recommendations-header {
    text-align: left;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.anchor.medium}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    margin: 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 10px;
    letter-spacing: normal;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin: 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 47px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.fonts.fontSize.anchor.large}px;
      font-weight: ${(props) => props.theme.fonts.fontWeight.black};
      margin: 0 0 18px 61px;
    }
  }
  && .item-container-inner {
    padding: 0 6.5px;
  }
  && .container-price {
    p {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      font-size: ${(props) => props.theme.fonts.fontSize.listmenu.small}px;
    }
    .list-price {
      font-size: 9px;
      color: #6d7278;
      font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
    }
    .merchant-tag {
      font-size: 9px;
      color: #c01f1f;
    }
  }
  && .added-to-bag {
    font-size: ${(props) => props.theme.fonts.fontSize.body.small.primary}px;
    padding: 8px 0;
    min-height: auto;
    letter-spacing: normal;
    ${(props) => radiusSix(props)}
  }
  .recommendations-section-row {
    .no-carousel-container {
      ul {
        width: 143px;
      }
    }
  }

  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    .recommendations-section-row {
      .recommendations-section-col {
        margin-left: 10%;
        margin-right: 10%;
        width: 80%;
      }
    }
    .no-carousel-container {
      ul {
        width: 119px;
      }
    }
    && .slick-prev {
      left: -8.5%;
    }
    && .slick-next {
      right: -8.5%;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    && .item-container-inner {
      padding: 0 12.5px;
    }
    && .no-carousel-container {
      ul {
        width: 261px;
      }
    }
    && .slick-prev {
      left: -5.5%;
    }
    && .slick-next {
      right: -5.5%;
    }
  }
  && .slick-list {
    margin-right: 0;
  }

  .product-wishlist-container + .fulfillment-section {
    margin-top: 23px;
  }
`;

export default styles;
