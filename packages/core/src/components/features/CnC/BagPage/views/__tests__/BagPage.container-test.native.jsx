// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { BagPageContainer } from '../../container/BagPage.container.native';
import BagPage from '../BagPage.view';

jest.mock('../../../../../../utils', () => ({
  __esModule: true,
  default: {
    getObjectValue: () => {
      return false;
    },
  },
}));

describe('Bag page Container', () => {
  const props = {
    labels: {},
    initialActions: jest.fn(),
    fetchNeedHelpContent: jest.fn(),
    fetchSflData: jest.fn(),
    setVenmoPickupState: jest.fn(),
    setVenmoShippingState: jest.fn(),
    getUserInformation: jest.fn(),
    isPickupModalOpen: jest.fn(),
    setVenmoInProgress: jest.fn(),
    setAppleInProgress: jest.fn(),
    resetAnalyticsData: jest.fn(),
    setMaxItemErrorAction: jest.fn(),
    setCouponMergeErrorAction: jest.fn(),
    screenProps: {
      checkFirst: jest.fn(),
    },
  };
  it('should render Added to Bag view section', () => {
    global.navigator = {
      product: 'ReactNative',
    };
    const tree = shallow(<BagPageContainer {...props} />);
    expect(tree.is(BagPage)).toBeTruthy();
  });
});
