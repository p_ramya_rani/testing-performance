/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View, Animated } from 'react-native';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import CookieManager from 'react-native-cookies';
import ToastContainer from '@tcp/core/src/components/common/atoms/Toast/container/Toast.container.native';
import { bool, func, number, shape, string } from 'prop-types';
import OrderLedgerContainer from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger';
import { WithKeyboardAvoidingViewHOC } from '@tcp/core/src/components/common/hoc/withKeyboardAvoidingView.native';
import {
  isCanada,
  isIOS,
  setValueInAsyncStorage,
  getValueFromAsyncStorage,
} from '@tcp/core/src/utils';
import { isAndroid } from '@tcp/core/src/utils/index.native';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import PlaceCashBanner from '@tcp/core/src/components/features/CnC/PlaceCashBanner';
import {
  trackForterNavigation,
  ForterNavigationType,
} from '@tcp/core/src/utils/forter.util.native';
import SigninCtaBanner from '@tcp/core/src/components/features/CnC/common/molecules/SigninCtaBanner';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import ProductTileWrapper from '../../CartItemTile/organisms/ProductTileWrapper/container/ProductTileWrapper.container';
import CouponAndPromos from '../../common/organism/CouponAndPromos';
import AirmilesBanner from '../../common/organism/AirmilesBanner';
import AddedToBagActions from '../../AddedToBagActions';

import {
  Margin,
  HeadingViewStyle,
  MainSection,
  RowSectionStyle,
  ScrollViewWrapper,
  BonusPointsWrapper,
  BagHeaderRow,
  SflHeadingViewStyle,
  ActiveBagHeaderView,
  InActiveBagHeaderView,
  HeadingTextStyleView,
  EstimateTextStyle,
  ActiveBagHeaderTextNew,
  InActiveBagHeaderTextView,
  InActiveEstimateTextStyle,
  BagHeaderMain,
  FooterView,
  ContainerMain,
  RecommendationWrapper,
  RecommendationView,
  PlaceCashRecommendation,
  PlaceCashRecommendationWrapper,
  PlaceRewardRecommendation,
  PlaceRewardRecommendationWrapper,
  PlaceCashToggleButton,
  PlaceCashRecoToggleButton,
  ImageStyleWrapper,
  HorizontalLine,
} from '../styles/BagPage.style.native';
import BonusPointsDays from '../../../../common/organisms/BonusPointsDays';
import InitialPropsHOC from '../../../../common/hoc/InitialPropsHOC/InitialPropsHOC.native';
import BAGPAGE_CONSTANTS from '../BagPage.constants';

import PickupStoreModal from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.container';
import { TIER1 } from '../../../../../constants/tiering.constants';

const downIcon = require('../../../../../../../mobileapp/src/assets/images/down-arrow-toggle.png');
const upIcon = require('../../../../../../../mobileapp/src/assets/images/up-arrow-toggle.png');

const AnimatedBagHeaderMain = Animated.createAnimatedComponent(BagHeaderMain);
export class BagPage extends React.Component {
  headerHeight = 68;

  headerEnable = true;

  mergeCartMessageShown = false;

  extraCustomScrollHeight = 120;

  constructor(props) {
    super(props);
    this.hideHeaderWhilePaypalView = this.hideHeaderWhilePaypalView.bind(this);
    const { orderItemsCount, sflItems, isShowSaveForLaterSwitch } = this.props;
    const activeSession =
      !orderItemsCount && sflItems.size && isShowSaveForLaterSwitch
        ? BAGPAGE_CONSTANTS.SFL_STATE
        : BAGPAGE_CONSTANTS.BAG_STATE;
    this.state = {
      activeSection: activeSession,
      showCondensedHeader: false,
      height: new Animated.Value(this.headerHeight),
      placeCashRecoHeight: false,
      placeRewardRecoHeight: false,
      showPlaceCashRecommendation: false,
      showPlaceRewardRecommendation: false,
      showRecommendation: false,
    };
    this.timer = null;
    this.isIOSApp = isIOS();
    trackForterNavigation('BagPage', ForterNavigationType.CART, {}, true);
  }

  async componentDidMount() {
    const { fetchLabels } = this.props;
    fetchLabels();
  }

  shouldComponentUpdate(nextProps) {
    const { sflItems } = nextProps;
    const { sflItems: preSFLItems } = this.props;
    if (sflItems && sflItems !== preSFLItems && sflItems.size === 0) {
      this.setState({
        activeSection: BAGPAGE_CONSTANTS.BAG_STATE,
      });
    }

    const { isCheckoutFocused, navigation } = nextProps;
    // On Click of Cart Icon from Checkout pages
    const fromAppCheckout = (navigation && navigation.getParam('fromAppCheckout')) || false;
    return !(isCheckoutFocused && !fromAppCheckout);
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  componentDidUpdate(prevProps) {
    const {
      cartItemSflError,
      bagPageServerError,
      cartSflMerged,
      labels,
      setCartSflMergedAction,
      isCartItemSFL,
      isSflItemRemoved,
      isCartItemsUpdating: { isUpdating },
      isSflItemMovedToBag,
      bagPageLoading,
    } = this.props;
    const {
      bagPageServerError: prevBagPageServerError,
      cartItemSflError: prevCartItemSflError,
      cartSflMerged: prevCartSflMerged,
      isCartItemSFL: prevIsCartItemSFL,
      bagPageLoading: prevBagPageLoading,
    } = prevProps;

    const { showRecommendation } = this.state;

    let message = null;
    const {
      sflSuccess,
      sflDeleteSuccess,
      itemUpdated,
      sflMovedToBagSuccess,
      cartSflMergedSuccessMsg,
    } = labels;
    if (cartItemSflError && cartItemSflError !== prevCartItemSflError) {
      this.showToastMessage(cartItemSflError);
    } else if (bagPageServerError && bagPageServerError !== prevBagPageServerError) {
      this.showToastMessage(bagPageServerError.errorMessage);
    } else if (cartSflMerged && cartSflMerged !== prevCartSflMerged) {
      // Show toast message if registered user items merged to SFL after guest login
      const { ITEM_SFL_SUCCESS_MSG_TIMEOUT } = BAGPAGE_CONSTANTS;
      this.showToastMessage(cartSflMergedSuccessMsg);
      setTimeout(() => {
        setCartSflMergedAction(false);
      }, ITEM_SFL_SUCCESS_MSG_TIMEOUT);
    } else if (isCartItemSFL && isCartItemSFL !== prevIsCartItemSFL) {
      message = sflSuccess;
      this.showToastMessage(message);
    } else if (isSflItemRemoved) {
      message = isSflItemMovedToBag ? sflMovedToBagSuccess : sflDeleteSuccess;
      this.showToastMessage(message);
    } else if (isUpdating) {
      message = itemUpdated;
      this.showToastMessage(message);
    }

    if (prevBagPageLoading && !bagPageLoading && !showRecommendation) {
      this.setState({ showRecommendation: true });
    } else if (showRecommendation && bagPageLoading) {
      this.setState({ showRecommendation: false });
    }
  }

  hideHeaderWhilePaypalView = (hide) => {
    const { navigation } = this.props;
    navigation.setParams({ headerMode: hide });
  };

  showToastMessage = (message) => {
    const { toastMessage, toastMessagePositionInfo } = this.props;
    toastMessage(message);
    toastMessagePositionInfo(0);
  };

  showMergeCartLimitError = () => {
    const {
      showMaxItemError,
      currentUserId,
      setMaxItemErrorAction,
      setMaxItemErrorDisplayedAction,
      showMaxItemErrorDisplayed,
    } = this.props;
    if (showMaxItemError && !this.mergeCartMessageShown && !showMaxItemErrorDisplayed) {
      const { labels: { maxQuantityError = '' } = {}, toastMessage } = this.props;
      this.mergeCartMessageShown = true;
      if (this.isIOSApp) {
        toastMessage(maxQuantityError);
        CookieManager.clearByName(`MC_${currentUserId}`);
      } else {
        getValueFromAsyncStorage('mergeCartMessageShown').then((result) => {
          if (!result) {
            toastMessage(maxQuantityError);
            setValueInAsyncStorage('mergeCartMessageShown', 'true'); // Don't show on app restart, as cookie was not deleting on android
          }
        });
      }
      setMaxItemErrorAction(false); // Don't show toast msg in other pages once shown
      setMaxItemErrorDisplayedAction(true);
    }
  };

  handleChangeActiveSection = (sectionName) => {
    const { isShowSaveForLaterSwitch } = this.props;
    if (isShowSaveForLaterSwitch) {
      this.setState({
        activeSection: sectionName,
      });
    }
  };

  setAnimation = (enable) => {
    this.headerEnable = enable;
    const { height } = this.state;
    Animated.timing(height, {
      duration: 200,
      toValue: enable ? this.headerHeight : 0,
    }).start();
  };

  hideHeader = () => {
    const { bagStickyHeaderInterval } = this.props;
    this.timer = setTimeout(() => {
      this.setAnimation(false);
      this.setState({ showCondensedHeader: true });
    }, bagStickyHeaderInterval);
  };

  handleScrollEnd = () => {
    this.hideHeader();
  };

  handleMomentumScrollEnd = (event) => {
    if (event.nativeEvent.contentOffset.y > 0) {
      this.hideHeader();
    }
  };

  handleScroll = (event) => {
    const contentOffset = event.nativeEvent.contentOffset.y;
    this.currentScrollValue = event.nativeEvent.contentOffset.y;
    if (contentOffset > 0) {
      this.setAnimation(true);
      this.setState({ showCondensedHeader: false });

      if (this.timer !== null) {
        clearTimeout(this.timer);
      }
    } else {
      if (this.timer !== null) {
        clearTimeout(this.timer);
      }
      this.setAnimation(true);
      this.setState({ showCondensedHeader: false });
    }
  };

  getScrollProps = () => {
    return isAndroid()
      ? {
          onScroll: this.handleScroll,
          onScrollEndDrag: this.handleScrollEnd,
          onMomentumScrollEnd: this.handleMomentumScrollEnd,
        }
      : {};
  };

  togglePlaceCashRecosHeight = () => {
    const { placeCashRecoHeight } = this.state;
    this.setState({
      placeCashRecoHeight: !placeCashRecoHeight,
    });
  };

  togglePlaceRewardRecosHeight = (status) => {
    this.setState({
      placeRewardRecoHeight: status,
    });
  };

  getPlaceCashRecommendationProductLength = (products) => {
    const { showPlaceCashRecommendation } = this.state;
    if (!showPlaceCashRecommendation && products && products.length > 0) {
      this.setState({ showPlaceCashRecommendation: true });
    } else if (showPlaceCashRecommendation && products && products.length < 1) {
      this.setState({ showPlaceCashRecommendation: false });
    }
  };

  getPlaceRewardRecommendationProductLength = (products) => {
    const { showPlaceRewardRecommendation } = this.state;
    if (!showPlaceRewardRecommendation && products && products.length > 0) {
      this.setState({ showPlaceRewardRecommendation: true });
    } else if (showPlaceRewardRecommendation && products && products.length < 1) {
      this.setState({ showPlaceRewardRecommendation: false });
    }
  };

  renderBagHeading() {
    const { activeSection } = this.state;
    const { labels, totalCount, orderBalanceTotal } = this.props;
    const { bagHeading } = labels;
    const bagHeadingTexts = `${bagHeading} (${totalCount})`;
    const estimateTotal = `${labels.totalLabel}: $${orderBalanceTotal.toFixed(2)}`;
    const selectedAccessibilityLabel = `Active ${bagHeadingTexts}`;
    const selectAccessibilityLabel = `Inactive ${bagHeadingTexts}`;
    return (
      <HeadingTextStyleView>
        {activeSection === BAGPAGE_CONSTANTS.SFL_STATE ? (
          <>
            <InActiveBagHeaderTextView
              accessible
              accessibilityLabel={selectAccessibilityLabel}
              accessibilityRole="link"
            >
              {bagHeadingTexts}
            </InActiveBagHeaderTextView>
            <InActiveEstimateTextStyle>
              {totalCount > 0 ? estimateTotal : ''}
            </InActiveEstimateTextStyle>
          </>
        ) : (
          <>
            <ActiveBagHeaderTextNew
              accessible
              accessibilityLabel={selectedAccessibilityLabel}
              accessibilityRole="link"
            >
              {bagHeadingTexts}
            </ActiveBagHeaderTextNew>
            <EstimateTextStyle>{totalCount > 0 ? estimateTotal : ''}</EstimateTextStyle>
          </>
        )}
      </HeadingTextStyleView>
    );
  }

  renderSflHeading() {
    const { activeSection } = this.state;
    const { labels, sflItems, isShowSaveForLaterSwitch } = this.props;
    if (!isShowSaveForLaterSwitch) return null;
    const { savedLaterButton } = labels;
    const headingTexts = `${savedLaterButton} (${sflItems.size})`;
    const selectedAccessibilityLabel = `Active ${headingTexts}`;
    const selectAccessibilityLabel = `Inactive ${headingTexts}`;
    return (
      <HeadingTextStyleView>
        {activeSection === BAGPAGE_CONSTANTS.BAG_STATE ? (
          <>
            <InActiveBagHeaderTextView
              accessible
              accessibilityLabel={selectAccessibilityLabel}
              accessibilityRole="link"
            >
              {headingTexts}
            </InActiveBagHeaderTextView>
            <EstimateTextStyle />
          </>
        ) : (
          <>
            <ActiveBagHeaderTextNew
              accessible
              accessibilityLabel={selectedAccessibilityLabel}
              accessibilityRole="link"
            >
              {headingTexts}
            </ActiveBagHeaderTextNew>
            <EstimateTextStyle />
          </>
        )}
      </HeadingTextStyleView>
    );
  }

  renderOrderLedgerContainer = (isNoNEmptyBag) => {
    const { navigation } = this.props;
    if (isNoNEmptyBag) {
      return (
        <RowSectionStyle>
          <OrderLedgerContainer pageCategory="bagPage" navigation={navigation} />
        </RowSectionStyle>
      );
    }
    return <></>;
  };

  renderBonusPoints = (isUserLoggedIn, isNoNEmptyBag) => {
    const { isBonusPointsEnabled } = this.props;
    if (isUserLoggedIn && isNoNEmptyBag && isBonusPointsEnabled) {
      return (
        <RowSectionStyle>
          <BonusPointsWrapper>
            <BonusPointsDays isBagPage showAccordian={false} />
          </BonusPointsWrapper>
        </RowSectionStyle>
      );
    }
    return <></>;
  };

  renderAirMiles = () => {
    if (isCanada()) {
      return (
        <RowSectionStyle>
          <AirmilesBanner />
        </RowSectionStyle>
      );
    }
    return <></>;
  };

  renderCouponPromos = (isNoNEmptyBag) => {
    if (isNoNEmptyBag) {
      return (
        <RowSectionStyle>
          <CouponAndPromos
            pageName="shopping bag"
            showAccordian={false}
            measurePositionHandler={(data) => {
              const { couponOffset } = data;
              const scrollPos = Math.abs(couponOffset);
              this.scrollView.scrollTo({ y: scrollPos, animated: false });
            }}
          />
        </RowSectionStyle>
      );
    }
    return <></>;
  };

  renderPlaceCashRecommendations = () => {
    const { navigation, labels, isPlaceCashEnabled } = this.props;
    const { placeCashRecoHeight, activeSection, showPlaceCashRecommendation } = this.state;
    if (activeSection === BAGPAGE_CONSTANTS.SFL_STATE) {
      return <></>;
    }
    const { showRecommendation } = this.state;

    // const hideRec = orderItemsCount === 0 && sflItems.size > 0;
    if (
      Constants.RECOMMENDATIONS_MBOXNAMES.ADD_PRODUCT_TO_GET_PLACECASH &&
      isPlaceCashEnabled &&
      showRecommendation
    ) {
      return (
        <PlaceCashRecommendation showPlaceCashRecommendation={showPlaceCashRecommendation}>
          {showPlaceCashRecommendation && <PlaceCashBanner isLeftProgressBar />}
          <PlaceCashRecommendationWrapper>
            <Recommendations
              navigation={navigation}
              page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
              variation="moduleO"
              headerLabel={labels.addProductGetPlaceCash}
              getRecommendationProducts={this.getPlaceCashRecommendationProductLength}
              portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.ADD_PRODUCT_TO_GET_PLACECASH}
              sequence="2"
              placeCashRecoHeight={placeCashRecoHeight}
              isPlaceCashReward
            />
          </PlaceCashRecommendationWrapper>

          {showPlaceCashRecommendation && (
            <ImageStyleWrapper>
              <PlaceCashRecoToggleButton onPress={this.togglePlaceCashRecosHeight}>
                <PlaceCashToggleButton source={placeCashRecoHeight ? upIcon : downIcon} />
              </PlaceCashRecoToggleButton>
            </ImageStyleWrapper>
          )}
        </PlaceCashRecommendation>
      );
    }
    return <></>;
  };

  renderPlaceRewardRecommendations = () => {
    const { navigation, labels, isPlaceRewardEnabled, isUserLoggedIn } = this.props;
    const {
      placeRewardRecoHeight,
      activeSection,
      showPlaceRewardRecommendation,
      showRecommendation,
    } = this.state;

    if (activeSection === BAGPAGE_CONSTANTS.SFL_STATE) {
      return <></>;
    }
    if (
      Constants.RECOMMENDATIONS_MBOXNAMES.BAG_CART_REWARD_PLCC &&
      isPlaceRewardEnabled &&
      isUserLoggedIn &&
      !isCanada() &&
      showRecommendation
    ) {
      return (
        <PlaceRewardRecommendation showPlaceRewardRecommendation={showPlaceRewardRecommendation}>
          {showPlaceRewardRecommendation && <PlaceCashBanner isLeftProgressBar isPlaceReward />}
          {showPlaceRewardRecommendation && <HorizontalLine />}
          <PlaceRewardRecommendationWrapper>
            <Recommendations
              navigation={navigation}
              page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
              portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.BAG_CART_REWARD_PLCC}
              variation="moduleO"
              headerLabel={labels.addProductGetPlaceReward}
              getRecommendationProducts={this.getPlaceRewardRecommendationProductLength}
              sequence="2"
              placeRewardRecoHeight={placeRewardRecoHeight}
              isPlaceCashReward
            />
          </PlaceRewardRecommendationWrapper>

          {showPlaceRewardRecommendation && (
            <ImageStyleWrapper>
              <PlaceCashRecoToggleButton
                onPress={() => {
                  this.togglePlaceRewardRecosHeight(!placeRewardRecoHeight);
                }}
              >
                <PlaceCashToggleButton source={placeRewardRecoHeight ? upIcon : downIcon} />
              </PlaceCashRecoToggleButton>
            </ImageStyleWrapper>
          )}
        </PlaceRewardRecommendation>
      );
    }
    return <></>;
  };

  renderRecommendations = (quickViewProductId, quickViewLoader) => {
    const {
      navigation,
      labels,
      sflItems,
      orderItemsCount,
      excludedIds,
      productId,
      fireHapticFeedback,
    } = this.props;
    const { deviceTier } = navigation.getScreenProps();
    const { showRecommendation } = this.state;
    const hideRec = orderItemsCount === 0 && sflItems.size > 0;
    const isNoNEmptyBag = orderItemsCount > 0;
    const headerText = isNoNEmptyBag
      ? labels.youMayAlsoLikeFullBagPage
      : labels.youMayAlsoLikeEmptyBagPage;
    const headerText2 = isNoNEmptyBag
      ? labels.recentlyViewedFullBagPage
      : labels.recentlyViewedEmptyBagPage;

    if (Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED && showRecommendation) {
      return (
        <RecommendationView>
          <RecommendationWrapper>
            {!hideRec && productId ? (
              <Margin margins="0px 0px 0px 14px">
                <Recommendations
                  navigation={navigation}
                  page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
                  variation="moduleO"
                  excludedIds={excludedIds}
                  partNumber={productId}
                  headerLabel={headerText}
                  sequence="1"
                  isCardTypeTiles
                  productImageWidth={130}
                  productImageHeight={161}
                  paddings="0px"
                  margins="5px 6px 18px"
                  quickViewLoader={quickViewLoader}
                  quickViewProductId={quickViewProductId}
                  fireHapticFeedback={fireHapticFeedback}
                />
              </Margin>
            ) : (
              <Margin margins="0px 0px 0px 14px">
                <Recommendations
                  navigation={navigation}
                  page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
                  variation="moduleO"
                  headerLabel={headerText}
                  sequence="1"
                  isCardTypeTiles
                  productImageWidth={130}
                  productImageHeight={161}
                  paddings="0px"
                  margins="5px 6px 18px"
                  quickViewLoader={quickViewLoader}
                  quickViewProductId={quickViewProductId}
                  fireHapticFeedback={fireHapticFeedback}
                />
              </Margin>
            )}
            {deviceTier === TIER1 && (
              <Margin margins="0px 0px 0px 14px">
                <Recommendations
                  navigation={navigation}
                  page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
                  variation="moduleO"
                  headerLabel={headerText2}
                  portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
                  sequence="2"
                  isCardTypeTiles
                  productImageWidth={130}
                  productImageHeight={161}
                  paddings="0px"
                  margins="5px 6px 18px"
                  quickViewLoader={quickViewLoader}
                  quickViewProductId={quickViewProductId}
                  fireHapticFeedback={fireHapticFeedback}
                />
              </Margin>
            )}
          </RecommendationWrapper>
        </RecommendationView>
      );
    }
    return <></>;
  };

  renderPickupModal = () => {
    const { isPickupModalOpen, navigation } = this.props;
    return <>{isPickupModalOpen ? <PickupStoreModal navigation={navigation} /> : null}</>;
  };

  renderBagTab = (viewHeight) => {
    const {
      labels,
      showAddTobag,
      navigation,
      orderItemsCount,
      isSignInBannerAbTestEnabled,
      rememberedUser,
      isUserLoggedIn,
      quickViewProductId,
      quickViewLoader,
    } = this.props;

    const isNoNEmptyBag = orderItemsCount > 0;
    const scrollProps = this.getScrollProps();

    return (
      <>
        {this.showMergeCartLimitError()}
        <WithKeyboardAvoidingViewHOC
          extraCustomScrollHeight={this.extraCustomScrollHeight}
          onScroll={this.handleScroll}
          onScrollEndDrag={this.handleScrollEnd}
          onMomentumScrollEnd={this.handleMomentumScrollEnd}
        >
          <ScrollViewWrapper
            viewHeight={showAddTobag ? '60%' : viewHeight}
            keyboardShouldPersistTaps="handled"
            ref={(scrollView) => {
              this.scrollView = scrollView;
            }}
            {...scrollProps}
          >
            <MainSection>
              {this.renderSignInCtaBanner(
                isUserLoggedIn,
                isSignInBannerAbTestEnabled,
                rememberedUser,
                labels,
                isNoNEmptyBag
              )}
              {this.renderPlaceRewardRecommendations()}
              {this.renderPlaceCashRecommendations()}

              <ProductTileWrapper bagLabels={labels} navigation={navigation} pageView="myBag" />
              {this.renderOrderLedgerContainer(isNoNEmptyBag)}
              {this.renderBonusPoints(isUserLoggedIn, isNoNEmptyBag)}
              {this.renderPlaceCashBanner(isNoNEmptyBag)}
              {this.renderAirMiles()}
              {this.renderCouponPromos(isNoNEmptyBag)}
              {this.renderRecommendations(quickViewProductId, quickViewLoader)}
            </MainSection>
            {this.renderPickupModal()}
          </ScrollViewWrapper>
        </WithKeyboardAvoidingViewHOC>
      </>
    );
  };

  renderSFLTab = (viewHeight) => {
    const {
      labels,
      showAddTobag,
      navigation,
      orderItemsCount,
      isSignInBannerAbTestEnabled,
      rememberedUser,
      isUserLoggedIn,
      sflItems,
    } = this.props;

    const isNoNEmptyBag = orderItemsCount > 0;
    const scrollProps = this.getScrollProps();

    return (
      <>
        {this.showMergeCartLimitError()}
        <WithKeyboardAvoidingViewHOC
          extraCustomScrollHeight={this.extraCustomScrollHeight}
          onScroll={this.handleScroll}
          onScrollEndDrag={this.handleScrollEnd}
          onMomentumScrollEnd={this.handleMomentumScrollEnd}
        >
          <ScrollViewWrapper
            viewHeight={showAddTobag ? '60%' : viewHeight}
            keyboardShouldPersistTaps="handled"
            ref={(scrollView) => {
              this.scrollView = scrollView;
            }}
            {...scrollProps}
          >
            <MainSection>
              {this.renderSignInCtaBanner(
                isUserLoggedIn,
                isSignInBannerAbTestEnabled,
                rememberedUser,
                labels,
                isNoNEmptyBag
              )}

              <ProductTileWrapper
                bagLabels={labels}
                sflItems={sflItems}
                isBagPageSflSection
                navigation={navigation}
              />
              {this.renderRecommendations()}
            </MainSection>
            {this.renderPickupModal()}
          </ScrollViewWrapper>
        </WithKeyboardAvoidingViewHOC>
      </>
    );
  };

  renderPlaceCashBanner = (isNoNEmptyBag) => {
    return isNoNEmptyBag ? <PlaceCashBanner /> : null;
  };

  renderSignInCtaBanner = (
    isUserLoggedIn,
    isSignInBannerAbTestEnabled,
    rememberedUser,
    labels,
    isNoNEmptyBag
  ) => {
    return isSignInBannerAbTestEnabled && !isUserLoggedIn && !rememberedUser && isNoNEmptyBag ? (
      <SigninCtaBanner labels={labels} />
    ) : null;
  };

  render() {
    const {
      labels,
      showAddTobag,
      navigation,
      orderItemsCount,
      isShowSaveForLaterSwitch,
      orderBalanceTotal,
      handleCartCheckout,
      isPayPalWebViewEnable,
    } = this.props;

    const isNoNEmptyBag = orderItemsCount > 0;
    const { activeSection, showCondensedHeader, height } = this.state;

    if (!labels.bagHeading) {
      return <View />;
    }
    const isBagStage = activeSection === BAGPAGE_CONSTANTS.BAG_STATE;
    const viewHeight = showCondensedHeader ? '74%' : '65%';

    return (
      <>
        <ToastContainer />

        <AnimatedBagHeaderMain style={{ height }}>
          <BagHeaderRow>
            <HeadingViewStyle
              onPress={() => {
                this.handleChangeActiveSection(BAGPAGE_CONSTANTS.BAG_STATE);
              }}
            >
              {isBagStage ? (
                <ActiveBagHeaderView>{this.renderBagHeading()}</ActiveBagHeaderView>
              ) : (
                <InActiveBagHeaderView>{this.renderBagHeading()}</InActiveBagHeaderView>
              )}
            </HeadingViewStyle>
            {isShowSaveForLaterSwitch && (
              <SflHeadingViewStyle
                onPress={() => {
                  this.handleChangeActiveSection(BAGPAGE_CONSTANTS.SFL_STATE);
                }}
              >
                {isBagStage ? (
                  <InActiveBagHeaderView>{this.renderSflHeading()}</InActiveBagHeaderView>
                ) : (
                  <ActiveBagHeaderView>{this.renderSflHeading()}</ActiveBagHeaderView>
                )}
              </SflHeadingViewStyle>
            )}
          </BagHeaderRow>
        </AnimatedBagHeaderMain>
        {this.showMergeCartLimitError()}
        <ContainerMain selectedTab={isBagStage}>
          {isBagStage ? this.renderBagTab(viewHeight) : this.renderSFLTab(viewHeight)}
        </ContainerMain>
        {isBagStage && (
          <FooterView isPayPalWebViewEnable={isPayPalWebViewEnable}>
            <AddedToBagActions
              handleCartCheckout={handleCartCheckout}
              labels={labels}
              showAddTobag={showAddTobag}
              navigation={navigation}
              isNoNEmptyBag={isNoNEmptyBag}
              hideHeader={this.hideHeaderWhilePaypalView}
              payPalTop={30}
              orderBalanceTotal={orderBalanceTotal}
            />
          </FooterView>
        )}
      </>
    );
  }
}

BagPage.propTypes = {
  bagPageServerError: shape({
    errorMessage: string,
  }),
  bagStickyHeaderInterval: number.isRequired,
  cartItemSflError: string.isRequired,
  cartSflMerged: bool,
  currentUserId: string,
  excludedIds: string,
  fetchLabels: func.isRequired,
  handleCartCheckout: func.isRequired,
  isBonusPointsEnabled: bool.isRequired,
  isCartItemSFL: bool.isRequired,
  isCartItemsUpdating: shape({
    isUpdating: bool,
    isDeleting: bool.isRequired,
  }).isRequired,
  isCheckoutFocused: bool,
  isPayPalWebViewEnable: bool.isRequired,
  isPickupModalOpen: bool,
  bagPageLoading: bool,
  isPlaceCashEnabled: bool.isRequired,
  isPlaceRewardEnabled: bool.isRequired,
  isSflItemMovedToBag: bool,
  isSflItemRemoved: bool.isRequired,
  isShowSaveForLaterSwitch: bool.isRequired,
  isSignInBannerAbTestEnabled: bool,
  isUserLoggedIn: bool.isRequired,
  labels: shape.isRequired,
  navigation: shape({
    getParam: func,
    getScreenProps: func,
  }).isRequired,
  orderBalanceTotal: number.isRequired,
  orderItemsCount: number.isRequired,
  productId: string,
  rememberedUser: bool,
  setCartSflMergedAction: func.isRequired,
  setMaxItemErrorAction: func.isRequired,
  setMaxItemErrorDisplayedAction: func.isRequired,
  sflItems: shape([]).isRequired,
  showAddTobag: bool.isRequired,
  showMaxItemError: bool,
  showMaxItemErrorDisplayed: bool.isRequired,
  toastMessage: func.isRequired,
  toastMessagePositionInfo: func.isRequired,
  totalCount: number.isRequired,
  quickViewLoader: bool,
  quickViewProductId: string,
};

BagPage.defaultProps = {
  bagPageServerError: null,
  cartSflMerged: false,
  currentUserId: '',
  excludedIds: '',
  isCheckoutFocused: false,
  isPickupModalOpen: false,
  isSflItemMovedToBag: false,
  isSignInBannerAbTestEnabled: false,
  productId: '',
  rememberedUser: false,
  showMaxItemError: false,
  bagPageLoading: false,
  quickViewLoader: false,
  quickViewProductId: '',
};

export default InitialPropsHOC(gestureHandlerRootHOC(BagPage));
