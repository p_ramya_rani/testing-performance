// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import SigninCtaBanner from '@tcp/core/src/components/features/CnC/common/molecules/SigninCtaBanner';
import { BagPage } from '../BagPage.view.native';

describe('AddedToBagActions native component', () => {
  const props1 = {
    labels: {
      viewBag: '',
      checkout: '',
      tagLine: 'tagline',
      bagHeading: 'test',
      savedLaterButton: 'savedLaterButton',
      itemUpdated: '',
    },
    activeSection: 'BAG',
    navigation: {
      getParam: jest.fn(),
      getScreenProps: () => {
        return { deviceTier: 'tier1' };
      },
    },
    sflItems: {},
    keyboardAwareScrollViewRef: {},
    totalCount: 0,
    orderBalanceTotal: 0,
    fetchLabels: jest.fn(),
    isCartItemsUpdating: { isDeleting: true, isUpdating: true },
    toastMessage: jest.fn(),
    toastMessagePositionInfo: jest.fn(),
    showAddTobag: true,
    orderItemsCount: 1,
    isUserLoggedIn: true,
    isNoNEmptyBag: true,
    isBagStage: true,
    bagStickyHeaderInterval: true,
    isShowSaveForLaterSwitch: true,
  };
  it('AddedToBagActions native component renders correctly', () => {
    const props = {
      labels: {
        viewBag: '',
        checkout: '',
        tagLine: 'tagline',
        bagHeading: 'test',
        savedLaterButton: 'savedLaterButton',
      },
      activeSection: 'BAG',
      navigation: {
        getParam: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      sflItems: {},
      totalCount: 10,
      orderBalanceTotal: 0,
      fetchLabels: jest.fn(),
      isCartItemsUpdating: { isDeleting: true },
      toastMessage: jest.fn(),
      toastMessagePositionInfo: jest.fn(),
    };
    const component = shallow(<BagPage {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('AddedToBagActions native component renders correctly with bag section', () => {
    const props = {
      labels: {
        viewBag: '',
        checkout: '',
        tagLine: 'tagline',
        bagHeading: 'test',
        savedLaterButton: 'savedLaterButton',
      },
      activeSection: 'BAG',
      navigation: {
        getParam: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      sflItems: {},
      totalCount: 0,
      orderBalanceTotal: 0,
      fetchLabels: jest.fn(),
      isCartItemsUpdating: { isDeleting: true },
      toastMessage: jest.fn(),
      toastMessagePositionInfo: jest.fn(),
      showAddTobag: true,
      orderItemsCount: 1,
      isUserLoggedIn: true,
      isNoNEmptyBag: true,
      isBagStage: true,
    };
    const component = shallow(<BagPage {...props} />);
    component.setState({ activeSection: 'BAG', showCondensedHeader: true });
    expect(component).toMatchSnapshot();
  });

  it('AddedToBagActions native component renders correctly with merge cart message', () => {
    const props = {
      labels: {
        viewBag: '',
        checkout: '',
        tagLine: 'tagline',
        bagHeading: 'test',
        savedLaterButton: 'savedLaterButton',
      },
      activeSection: 'BAG',
      navigation: {
        getParam: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      sflItems: {},
      totalCount: 10,
      orderBalanceTotal: 0,
      fetchLabels: jest.fn(),
      isCartItemsUpdating: { isDeleting: true },
      toastMessage: jest.fn(),
      toastMessagePositionInfo: jest.fn(),
      setMaxItemErrorDisplayedAction: jest.fn(),
      setMaxItemErrorAction: jest.fn(),
      showMaxItemErrorDisplayed: false,
    };
    const component = shallow(<BagPage {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('AddedToBagActions native component renders correctly with SFL section', () => {
    const props = {
      labels: {
        viewBag: '',
        checkout: '',
        tagLine: 'tagline',
        bagHeading: 'test',
        savedLaterButton: 'savedLaterButton',
      },
      activeSection: 'SFL',
      navigation: {
        getParam: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      sflItems: {},
      totalCount: 0,
      orderBalanceTotal: 0,
      fetchLabels: jest.fn(),
      isCartItemsUpdating: { isDeleting: true },
      toastMessage: jest.fn(),
      toastMessagePositionInfo: jest.fn(),
    };
    const component = shallow(<BagPage {...props} />);
    component.setState({ activeSection: 'SFL' });
    expect(component).toMatchSnapshot();
  });
  it('AddedToBagActions native component renders correctly with active SFL section', () => {
    const props = {
      labels: {
        viewBag: '',
        checkout: '',
        tagLine: 'tagline',
        bagHeading: 'test',
        savedLaterButton: 'savedLaterButton',
      },
      activeSection: 'BAG',
      navigation: {
        getParam: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      sflItems: {},
      totalCount: 0,
      orderBalanceTotal: 0,
      fetchLabels: jest.fn(),
      isCartItemsUpdating: { isDeleting: true },
      toastMessage: jest.fn(),
      toastMessagePositionInfo: jest.fn(),
      showAddTobag: true,
      orderItemsCount: 1,
    };
    const component = shallow(<BagPage {...props} />);
    component.setState({ activeSection: 'SFL' });
    expect(component).toMatchSnapshot();
  });

  it('AddedToBagActions native component renders correctly with active BAG section', () => {
    const props = {
      labels: {
        viewBag: '',
        checkout: '',
        tagLine: '',
        bagHeading: 'test',
        savedLaterButton: 'savedLaterButton',
      },
      activeSection: 'SFL',
      navigation: {
        getParam: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      sflItems: {},
      totalCount: 0,
      orderBalanceTotal: 0,
      fetchLabels: jest.fn(),
      isCartItemsUpdating: { isDeleting: true },
      toastMessage: jest.fn(),
      toastMessagePositionInfo: jest.fn(),
      showAddTobag: true,
      orderItemsCount: 1,
      isUserLoggedIn: true,
      isNoNEmptyBag: true,
      isBagStage: true,
      isPickupModalOpen: true,
    };
    const component = shallow(<BagPage {...props} />);
    component.setState({ activeSection: 'BAG', showCondensedHeader: true });
    expect(component).toMatchSnapshot();
  });
  it('AddedToBagActions native component renders correctly from checkout cart icon navigation', () => {
    const customProps = {
      navigation: {
        getParam: jest.fn().mockImplementation(() => true),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const component = shallow(<BagPage {...props1} {...customProps} />);
    component.setState({ activeSection: 'BAG', showCondensedHeader: true });
    expect(component).toMatchSnapshot();
  });
  it('AddedToBagActions native component renders correctly with bag section with method', () => {
    const component = shallow(<BagPage {...props1} />);
    component.setState({ activeSection: 'BAG', showCondensedHeader: true });
    const spyHandleScroll = jest.spyOn(component.instance(), 'handleScroll');
    const event = { nativeEvent: { contentOffset: { y: 4 } } };
    component.instance().handleScroll(event);
    expect(spyHandleScroll).toHaveBeenCalled();
  });
  it('AddedToBagActions native component renders correctly with bag section with method without y', () => {
    const component = shallow(<BagPage {...props1} />);
    component.setState({ activeSection: 'BAG', showCondensedHeader: true });
    const spyHandleScroll = jest.spyOn(component.instance(), 'handleScroll');
    const event = { nativeEvent: { contentOffset: { y: 0 } } };
    component.instance().handleScroll(event);
    expect(spyHandleScroll).toHaveBeenCalled();
  });
  it('AddedToBagActions native component renders correctly with bag section with method handleChangeActiveSection', () => {
    const component = shallow(<BagPage {...props1} />);
    component.setState({ activeSection: 'BAG', showCondensedHeader: true });
    const spyHandleScroll = jest.spyOn(component.instance(), 'handleChangeActiveSection');
    component.instance().handleChangeActiveSection('BagPage');
    expect(spyHandleScroll).toHaveBeenCalled();
  });
  it('AddedToBagActions native component renders correctly with bag section with method handleChangeActiveSection', () => {
    const component = shallow(<BagPage {...props1} />);
    const spyRenderModals = jest.spyOn(component.instance(), 'renderPickupModal');
    component.instance().renderPickupModal();
    expect(spyRenderModals).toHaveBeenCalled();
  });
  it('should call renderRecommendations', () => {
    props1.orderItemsCount = 0;
    props1.sflItems = { size: 2 };
    const component = shallow(<BagPage {...props1} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'renderRecommendations');
    component.instance().renderRecommendations();
    expect(spyOpenModal).toHaveBeenCalled();
  });

  it('should render SigninCtaBanner', () => {
    props1.isSignInBannerAbTestEnabled = true;
    props1.isUserLoggedIn = false;
    props1.orderItemsCount = 2;
    const component = shallow(<BagPage {...props1} />);
    expect(component.find(SigninCtaBanner)).toHaveLength(1);
  });

  it('should not render SigninCtaBanner when ab is off', () => {
    props1.isSignInBannerAbTestEnabled = false;
    props1.isUserLoggedIn = false;
    const component = shallow(<BagPage {...props1} />);
    expect(component.find(SigninCtaBanner)).toHaveLength(0);
  });

  it('should not render SigninCtaBanner for remembered user', () => {
    props1.isSignInBannerAbTestEnabled = true;
    props1.isUserLoggedIn = false;
    props1.rememberedUser = true;
    const component = shallow(<BagPage {...props1} />);
    expect(component.find(SigninCtaBanner)).toHaveLength(0);
  });

  it('should not render SigninCtaBanner for loggedin user', () => {
    props1.isSignInBannerAbTestEnabled = true;
    props1.isUserLoggedIn = true;
    props1.rememberedUser = false;
    const component = shallow(<BagPage {...props1} />);
    expect(component.find(SigninCtaBanner)).toHaveLength(0);
  });

  it('should not render SigninCtaBanner when bag is empty ', () => {
    props1.isSignInBannerAbTestEnabled = true;
    props1.isUserLoggedIn = false;
    props1.orderItemsCount = 0;
    const component = shallow(<BagPage {...props1} />);
    expect(component.find(SigninCtaBanner)).toHaveLength(0);
  });
});
