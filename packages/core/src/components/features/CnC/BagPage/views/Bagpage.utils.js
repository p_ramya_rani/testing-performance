// 9fbef606107a605d69c0edbcd8029e5d
import throttle from 'lodash/throttle';
import PropTypes from 'prop-types';
import { breakpoints } from '@tcp/core/styles/themes/TCP/mediaQuery';
import { getProductDetails } from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.selectors';
import { getBrand } from '@tcp/core/src/utils/utils';
import { isClient, scrollPage } from '../../../../../utils';

const getOffset = (elem) => {
  let x = 0;
  let y = 0;
  let el = elem;
  while (el && !Number.isNaN(el.offsetLeft) && !Number.isNaN(el.offsetTop)) {
    x += el.offsetLeft - el.scrollLeft;
    y += el.offsetTop - el.scrollTop;
    el = el.offsetParent;
  }
  return { top: y, left: x };
};

const getElementStickyPosition = (elem) => {
  return elem && getOffset(elem).top;
};

const bindScrollEvent = (callBack) => {
  const scrollThrottle = throttle(callBack, 100);
  if (isClient()) {
    window.addEventListener('scroll', scrollThrottle);
  }
  return () => window.removeEventListener('scroll', scrollThrottle);
};

const getPageLevelHeaderHeight = () => {
  return document.getElementsByClassName('condensed-header')[0]
    ? document.getElementsByClassName('condensed-header')[0].offsetHeight
    : 0;
};

const getProdBrand = (itemBrand) => {
  return itemBrand || getBrand();
};

const setFirstProductEvents = (index, summaryData, additionalInfoObj) => {
  if (index > 0) return {};
  return {
    giftCardAmount: summaryData && summaryData.giftCardsTotal,
    couponAmount: summaryData && summaryData.couponsTotal,
    shippingAmount: additionalInfoObj && additionalInfoObj.shippingAmount,
    savingsAmount: summaryData && summaryData.savingsTotal,
    taxAmount: summaryData && summaryData.taxesTotal,
    netRevenue: additionalInfoObj && additionalInfoObj.netRevenue,
  };
};

const formatBagProductsData = (
  cartOrderItems,
  additionalInfoObj,
  confirmationPageLedgerSummaryData,
  isConfirmationPage
) => {
  const productsData = [];

  if (cartOrderItems) {
    cartOrderItems.map((tile, index) => {
      const productDetail = getProductDetails(tile);
      const {
        itemInfo: { color, name, offerPrice, size, listPrice, qty, itemBrand },
        productInfo: { skuId, upc, productPartNumber, generalProductId },
        miscInfo: { store, orderItemType },
      } = productDetail;
      const summaryData = confirmationPageLedgerSummaryData || '';
      const prodData = {
        color,
        id: productPartNumber && productPartNumber.slice(0, productPartNumber.indexOf('_')),
        name,
        price: offerPrice,
        extPrice: offerPrice,
        paidPrice: offerPrice,
        listPrice,
        partNumber: productPartNumber,
        size,
        upc,
        sku: skuId.toString(),
        pricingState: 'full price',
        colorId: generalProductId,
        storeId: store,
        quantity: qty,
        prodBrand: getProdBrand(itemBrand),
        shippingMethod: isConfirmationPage
          ? additionalInfoObj && additionalInfoObj.shippingMethod
          : null,
        ...setFirstProductEvents(index, summaryData, additionalInfoObj),
        orderItemType,
      };
      productsData.push(prodData);
      return prodData;
    });
  }
  return productsData;
};

const formatBagCarousalData = (cartOrderItems) => {
  const productsData = [];

  if (cartOrderItems) {
    cartOrderItems.map((tile) => {
      const productDetail = getProductDetails(tile);
      const {
        itemInfo: { offerPrice, imagePath, itemBrand },
      } = productDetail;
      const prodData = {
        imagePath,
        price: offerPrice,
        itemBrand,
      };
      productsData.push(prodData);
      return prodData;
    });
  }
  return productsData;
};

const setBagPageAnalyticsData = (setClickAnalyticsDataBag, cartOrderItems, fromMiniBag) => {
  const productsData = formatBagProductsData(cartOrderItems);
  setClickAnalyticsDataBag({
    customEvents: ['scView', 'scOpen', 'event80'],
    products: productsData,
    pageNavigationText: fromMiniBag ? 'header-cart' : '',
  });
};

const getDefaultStateValues = () => {
  return {
    activeSection: null,
    showCondensedHeader: false,
    loadPaypalStickyHeader: false,
    showStickyHeaderMob: false,
    headerError: false,
    showPlaceCashRecommendation: false,
    showPlaceRewardRecommendation: false,
  };
};

const onPageUnload = () => {
  scrollPage();
};

const handleChangeActiveSection = (sectionName, scope) => {
  scope.setState({
    activeSection: sectionName,
  });
};

const BagPagePropTypes = {
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  orderItemsCount: PropTypes.number.isRequired,
  totalCount: PropTypes.number.isRequired,
  showAddTobag: PropTypes.bool.isRequired,
  isMobile: PropTypes.bool.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  isGuest: PropTypes.bool.isRequired,
  handleCartCheckout: PropTypes.func.isRequired,
  sflItems: PropTypes.shape([]).isRequired,
  setVenmoPaymentInProgress: PropTypes.func.isRequired,
  setApplePaymentInProgress: PropTypes.func.isRequired,
  isShowSaveForLaterSwitch: PropTypes.bool.isRequired,
  orderBalanceTotal: PropTypes.number.isRequired,
  bagStickyHeaderInterval: PropTypes.number.isRequired,
  currencySymbol: PropTypes.string.isRequired,
  isSflItemRemoved: PropTypes.bool.isRequired,
  isBagPage: PropTypes.bool.isRequired,
  cartItemSflError: PropTypes.string,
  bagPageServerError: PropTypes.shape({}),
  excludedIds: PropTypes.string,
  productId: PropTypes.string,
  isDisableBagAnchoring: PropTypes.bool,
};

const CarouselOptions = {
  CAROUSEL_OPTIONS: {
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 6,
    slide: true,
    touchMove: true,
    touchThreshold: 100,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: breakpoints.values.lg - 1,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: breakpoints.values.sm - 1,
        settings: {
          arrows: false,
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
    ],
  },
};

const PlacecashCarouselOptions = {
  CAROUSEL_OPTIONS: {
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    slide: true,
    dotsClass: 'slick-dots',
    dots: true,
    touchMove: true,
    touchThreshold: 100,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: breakpoints.values.lg - 1,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: false,
          dots: false,
        },
      },
      {
        breakpoint: breakpoints.values.sm - 1,
        settings: {
          arrows: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: false,
        },
      },
    ],
  },
};

export default {
  getElementStickyPosition,
  bindScrollEvent,
  getPageLevelHeaderHeight,
  setBagPageAnalyticsData,
  formatBagProductsData,
  getDefaultStateValues,
  onPageUnload,
  BagPagePropTypes,
  CarouselOptions,
  PlacecashCarouselOptions,
  handleChangeActiveSection,
  formatBagCarousalData,
};
