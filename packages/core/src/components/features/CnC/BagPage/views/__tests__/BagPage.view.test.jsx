// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import { BagPageViewVanilla } from '../BagPage.view';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileWeb: jest.fn(),
  getViewportInfo: () => ({
    isMobile: false,
  }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isMobileApp: jest.fn(),
  isClient: jest.fn().mockImplementation(() => true),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  readCookie: jest.fn(),
  setCookie: jest.fn(),
  getSiteId: jest.fn().mockImplementation(() => '102230'),
}));

describe('Bag page View', () => {
  const props = {
    labels: {},
    initialActions: jest.fn(),

    className: 'test',
    orderItemsCount: 10,
    orderItemsCountLocal: 10,
    totalCount: 10,
    handleCartCheckout: jest.fn(),
    showAddTobag: true,
    showConfirmationModal: true,
    closeCheckoutConfirmationModal: jest.fn(),
    removeUnqualifiedItemsAndCheckout: jest.fn(),
    setVenmoPaymentInProgress: jest.fn(),
    setApplePaymentInProgress: jest.fn(),
    sflItems: {},
    orderBalanceTotal: 50,
    router: {
      components: {
        '/Bag': {
          props: {
            isServer: true,
          },
        },
      },
    },
  };

  it('should render Added to Bag view section', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render SFL section', () => {
    const sflProps = {
      ...props,
      sflItems: fromJS([{}]),
    };
    const component = shallow(<BagPageViewVanilla {...sflProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should not render SFL section', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call componentWillUnmount', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'componentWillUnmount');
    component.instance().addScrollListener();
    component.instance().componentWillUnmount();
    expect(spyOpenModal).toHaveBeenCalled();
  });

  it('should call addScrollListener', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'addScrollListener');
    component.instance().addScrollListener();
    expect(spyOpenModal).toHaveBeenCalled();
  });

  it('should call getBagCondensedHeader', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'getBagCondensedHeader');
    component.instance().getBagCondensedHeader('<div></div>');
    expect(spyOpenModal).toHaveBeenCalled();
  });

  it('should call getBagPageHeaderRef', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'getBagPageHeaderRef');
    component.instance().getBagPageHeaderRef('<span></span>');
    expect(spyOpenModal).toHaveBeenCalled();
  });

  it('should call getBagActionsContainerRef', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'getBagActionsContainerRef');
    component.instance().getBagActionsContainerRef('<p></p>');
    expect(spyOpenModal).toHaveBeenCalled();
  });
  it('should render with showCondensedHeader: true,', () => {
    props.isShowSaveForLaterSwitch = true;
    const component = shallow(<BagPageViewVanilla {...props} />);
    component.setState({
      showCondensedHeader: true,
      showStickyHeaderMob: true,
      activeSection: 'SFL',
    });
    component.instance().renderActions();
    component.instance().renderLeftSection();
    expect(component).toMatchSnapshot();
  });
  it('should render with only SFL condition', () => {
    props.isShowSaveForLaterSwitch = true;
    props.totalCount = null;
    props.sflItems = { size: 2 };
    const component = shallow(<BagPageViewVanilla {...props} />);
    component.setState({
      showCondensedHeader: true,
      showStickyHeaderMob: true,
      activeSection: 'SFL',
    });
    component.setProps({ orderItemsCount: 20 });
    component.instance.bagActionsContainer = '<div></div>';
    expect(component).toMatchSnapshot();
  });
  it('should call componentWillUnmount', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    component.instance().addScrollListener();
    component.instance().componentWillUnmount();
  });
  it('should call renderRecommendations', () => {
    props.orderItemsCount = 0;
    props.sflItems = { size: 2 };
    const component = shallow(<BagPageViewVanilla {...props} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'renderRecommendations');
    component.instance().renderRecommendations();
    expect(spyOpenModal).toHaveBeenCalled();
  });
  it('should call renderHeaderError', () => {
    props.orderItemsCount = 0;
    props.sflItems = { size: 2 };
    props.labels = {};
    const component = shallow(<BagPageViewVanilla {...props} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'renderHeaderError');
    component.instance().renderHeaderError(true, [props]);
    expect(spyOpenModal).toHaveBeenCalled();
  });

  it('should render Added to Bag with orderbalanceTotal', () => {
    props.totalCount = 1;
    props.orderBalanceTotal = 0;
    props.isApplePayVisible = true;
    const component = shallow(<BagPageViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should not render merge cart limit error', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    expect(component.instance().showMergeCartLimitError()).toEqual(null);
  });

  it('should render merge cart limit error', () => {
    const props2 = { ...props, showMaxItemError: true };
    const component = shallow(<BagPageViewVanilla {...props2} />);
    expect(component.instance().showMergeCartLimitError()).toBeTruthy();
  });

  it('should call onScroll', () => {
    const component = shallow(<BagPageViewVanilla {...props} />);
    const spyOpenModal = jest.spyOn(component.instance(), 'onScroll');
    component.instance().onScroll();
    expect(spyOpenModal).toHaveBeenCalled();
  });
});
