/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { withRouter } from 'next/router';
import isEqual from 'lodash/isEqual';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { getSflItemCount, getCartItemCount } from '@tcp/core/src/utils/cookie.util';
import { getSiteId } from '@tcp/core/src/utils/utils.web';
import PlaceCashBanner from '@tcp/core/src/components/features/CnC/PlaceCashBanner';
import { isTier1Device } from '@tcp/web/src/utils/device-tiering';
import withRefWrapper from '../../../../common/hoc/withRefWrapper';
import withHotfix from '../../../../common/hoc/withHotfix';

import ProductTileWrapper from '../../CartItemTile/organisms/ProductTileWrapper/container/ProductTileWrapper.container';
import withStyles from '../../../../common/hoc/withStyles';
import Row from '../../../../common/atoms/Row';
import Heading from '../../../../common/atoms/Heading';
import Col from '../../../../common/atoms/Col';
import BodyCopy from '../../../../common/atoms/BodyCopy';
import AddedToBagActions from '../../AddedToBagActions';
import CnCTemplate from '../../common/organism/CnCTemplate';
import BAGPAGE_CONSTANTS from '../BagPage.constants';
import styles, {
  addedToBagActionsStyles,
  recommendationStyles,
  recommendationStylesPdp,
} from '../styles/BagPage.style';
import BagPageUtils from './Bagpage.utils';
import InformationHeader from '../../common/molecules/InformationHeader';
import { isClient, getViewportInfo, isCanada, scrollToBreadCrumb } from '../../../../../utils';

class BagPageView extends React.Component {
  constructor(props) {
    super(props);
    this.state = BagPageUtils.getDefaultStateValues();
    this.bagPageHeader = null;
    this.bagActionsContainer = null;
    this.bagCondensedHeader = null;
    this.timer = null;
    this.bagPageCondenseHeaderBind = false;
    this.bagPageAnchoring = false;
    this.getBagPageHeaderRef = this.getBagPageHeaderRef.bind(this);
    this.getBagActionsContainerRef = this.getBagActionsContainerRef.bind(this);
    this.getBagCondensedHeader = this.getBagCondensedHeader.bind(this);
    this.pageScrollEventUnbind = null;
    this.state = {
      isScrolling: false,
      scrollPos: -500,
    };
  }

  componentDidMount() {
    if (isClient()) {
      window.addEventListener('beforeunload', BagPageUtils.onPageUnload);
    }
    const {
      setVenmoPaymentInProgress,
      setApplePaymentInProgress,
      isShowSaveForLaterSwitch,
      isHideStickyCheckoutButton,
    } = this.props;

    setVenmoPaymentInProgress(false);
    setApplePaymentInProgress(false);

    const siteId = getSiteId() && getSiteId().toUpperCase();
    const cartTotalCount = getCartItemCount();
    const sflTotalCount = getSflItemCount(siteId);
    this.setState({
      activeSection:
        !cartTotalCount && sflTotalCount && isShowSaveForLaterSwitch
          ? BAGPAGE_CONSTANTS.SFL_STATE
          : BAGPAGE_CONSTANTS.BAG_STATE,

      orderLedgerAfterView: this.renderActions(),
    });
    if (isHideStickyCheckoutButton) {
      window.addEventListener('scroll', this.onScroll);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !(
      isEqual(nextState, this.state) &&
      isEqual(JSON.stringify(nextProps), JSON.stringify(this.props))
    );
  }

  componentDidUpdate(prevProps) {
    /* istanbul ignore else */
    const { isMobile, bagLoading, isDisableBagAnchoring, isHideStickyCheckoutButton } = this.props;
    const { isHideStickyCheckoutButton: prevIsHideStickyCheckoutButton } = prevProps;
    if (!this.bagPageCondenseHeaderBind) {
      const checkoutCta = this.bagActionsContainer;
      if (checkoutCta && !isMobile) {
        this.addScrollListener();
        this.bagPageCondenseHeaderBind = true;
      }
    }
    if (isMobile && !this.bagPageAnchoring && !bagLoading && !isDisableBagAnchoring) {
      scrollToBreadCrumb(true);
      this.bagPageAnchoring = true;
    }
    if (
      isHideStickyCheckoutButton &&
      prevIsHideStickyCheckoutButton !== isHideStickyCheckoutButton
    ) {
      window.removeEventListener('scroll', this.onScroll);
      window.addEventListener('scroll', this.onScroll);
    }
  }

  componentWillUnmount() {
    const { isHideStickyCheckoutButton } = this.props;
    if (isClient()) {
      window.removeEventListener('scroll', this.scrollEventLister);
      window.removeEventListener('beforeunload', BagPageUtils.onPageUnload);
      if (typeof this.pageScrollEventUnbind === 'function') this.pageScrollEventUnbind();
    }
    if (isHideStickyCheckoutButton) {
      window.removeEventListener('scroll', this.onScroll);
    }
  }

  onScroll = () => {
    const { isScrolling } = this.state;
    const { isStickyCheckoutButtonEnabled } = this.props;
    if (document.body.getBoundingClientRect().top < 12 && !isStickyCheckoutButtonEnabled) {
      this.setState((prev) => ({
        ...prev,
        isScrolling: document.body.getBoundingClientRect().top < prev.scrollPos,
        scrollPos: document.body.getBoundingClientRect().top,
      }));
    }
    if (isScrolling) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.setState({ isScrolling: false });
      }, 200);
    }
  };

  setHeaderErrorState = (state, ...params) => {
    this.setState({ headerError: true, params });
  };

  getBagPageHeaderRef(ref) {
    this.bagPageHeader = ref;
  }

  getBagActionsContainerRef(ref) {
    this.bagActionsContainer = ref;
  }

  getBagCondensedHeader(ref) {
    this.bagCondensedHeader = ref;
  }

  addScrollListener = () => {
    const checkoutCtaStickyPos = BagPageUtils.getElementStickyPosition(this.bagActionsContainer);
    this.scrollEventLister = this.handleBagHeaderScroll.bind(this, checkoutCtaStickyPos);
    this.pageScrollEventUnbind = BagPageUtils.bindScrollEvent(this.scrollEventLister);
  };

  getPlaceCashRecommendationProductLength = (products) => {
    const { showPlaceCashRecommendation } = this.state;
    if (!showPlaceCashRecommendation && products && products.length > 0) {
      this.setState({ showPlaceCashRecommendation: true });
    } else if (showPlaceCashRecommendation && products && products.length < 1) {
      this.setState({ showPlaceCashRecommendation: false });
    }
  };

  getPlaceRewardRecommendationProductLength = (products) => {
    const { showPlaceRewardRecommendation } = this.state;
    if (!showPlaceRewardRecommendation && products && products.length > 0) {
      this.setState({ showPlaceRewardRecommendation: true });
    } else if (showPlaceRewardRecommendation && products && products.length < 1) {
      this.setState({ showPlaceRewardRecommendation: false });
    }
  };

  handleBagHeaderScroll = (sticky) => {
    const condensedPageHeaderHeight = BagPageUtils.getPageLevelHeaderHeight();
    if (isClient() && window.pageYOffset > sticky + 30) {
      this.setState({ showCondensedHeader: true, loadPaypalStickyHeader: true }, () => {
        this.bagCondensedHeader.firstElementChild.style.top = `${condensedPageHeaderHeight.toString()}px`;
      });
    } else {
      this.setState({ showCondensedHeader: false });
    }
  };

  wrapSection = (Component, orderItemsCount) => {
    const isNoNEmptyBag = orderItemsCount > 0;
    const colSizes = { small: 6, medium: 5, large: 8 };
    if (!isNoNEmptyBag) {
      return <Col colSize={colSizes}>{Component}</Col>;
    }
    return Component;
  };

  renderRecommendations = () => {
    if (!isClient()) {
      return null;
    }
    this.willAlsoViewedCarouselDisplay = isTier1Device();
    const { labels, sflItems, orderItemsCount, excludedIds, productId, isNewBag } = this.props;
    const hideRec = orderItemsCount === 0 && sflItems.size > 0;
    const isNoNEmptyBag = orderItemsCount > 0;
    const headerText = isNoNEmptyBag
      ? labels.youMayAlsoLikeFullBagPage
      : labels.youMayAlsoLikeEmptyBagPage;
    let carouselOptions;
    if (isNoNEmptyBag) {
      carouselOptions = BagPageUtils.CarouselOptions.CAROUSEL_OPTIONS;
    }
    let customProductsLength = 3;
    if (isClient() && getViewportInfo().isDesktop) {
      customProductsLength = 6;
    }

    return (
      <>
        {!hideRec && productId ? (
          <div className="may-like-recommendation overwrite-recommendation-css">
            <Recommendations
              customProductsLength={customProductsLength}
              page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
              headerLabel={headerText}
              variations="moduleO"
              inheritedStyles={recommendationStyles}
              carouselConfigProps={carouselOptions}
              excludedIds={excludedIds}
              partNumber={productId}
              starRatingSize={{ small: 68, large: 68 }}
              sequence="1"
              showPeek={{ small: true, medium: false }}
              newBag={isNewBag}
              bagItemCount={orderItemsCount}
              isBagPage
            />
          </div>
        ) : (
          <div className="may-like-recommendation overwrite-recommendation-css">
            <Recommendations
              customProductsLength={customProductsLength}
              page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
              headerLabel={headerText}
              variations="moduleO"
              inheritedStyles={recommendationStyles}
              starRatingSize={{ small: 68, large: 68 }}
              carouselConfigProps={carouselOptions}
              sequence="1"
              showPeek={{ small: true, medium: false }}
              newBag={isNewBag}
              bagItemCount={orderItemsCount}
              isBagPage
            />
          </div>
        )}
        {this.willAlsoViewedCarouselDisplay && this.renderRecommendationsForRecent()}
      </>
    );
  };

  renderPlaceCashRecommendations = () => {
    const { labels, isNewBag } = this.props;
    const { activeSection, showPlaceCashRecommendation, orderItemsCount } = this.state;

    if (activeSection === BAGPAGE_CONSTANTS.SFL_STATE) {
      return null;
    }

    const recommendationAttributes = {
      variations: 'moduleO',
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG,
      showLoyaltyPromotionMessage: false,
      headerAlignment: 'center',
      accordionHeader: true,
      showHeightToggle: true,
    };
    const carouselOptions = BagPageUtils.PlacecashCarouselOptions.CAROUSEL_OPTIONS;

    const hideContent = showPlaceCashRecommendation ? '' : 'hide-content';
    let customProductsLength = 3;
    if (getViewportInfo().isDesktop) {
      customProductsLength = 2;
    }

    return (
      <>
        <div className={`cart-placecash ${hideContent}`}>
          <div className="cart-placecash-button">
            {showPlaceCashRecommendation && <PlaceCashBanner isLeftProgressBar />}
          </div>
          <div className="cart-placecash-product">
            <Recommendations
              customProductsLength={customProductsLength}
              headerLabel={labels.addProductGetPlaceCash}
              portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.ADD_PRODUCT_TO_GET_PLACECASH}
              sequence="0"
              starRatingSize={{ small: 63, large: 60 }}
              getRecommendationProducts={this.getPlaceCashRecommendationProductLength}
              inheritedStyles={recommendationStylesPdp}
              {...recommendationAttributes}
              carouselConfigProps={carouselOptions}
              isAbCartCash
              newBag={isNewBag}
              bagItemCount={orderItemsCount}
              isBagPage
            />
          </div>
        </div>
      </>
    );
  };

  renderPlaceRewardRecommendations = () => {
    const { labels, isPlcc, isNewBag, orderItemsCount } = this.props;
    const { activeSection, showPlaceRewardRecommendation } = this.state;

    if (activeSection === BAGPAGE_CONSTANTS.SFL_STATE) {
      return null;
    }

    const recommendationAttributes = {
      variations: 'moduleO',
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG,
      showLoyaltyPromotionMessage: false,
      headerAlignment: 'center',
      accordionHeader: true,
      showHeightToggle: true,
    };
    const classname = isPlcc ? 'plcc-user' : 'no-plcc-user';
    const carouselOptions = BagPageUtils.PlacecashCarouselOptions.CAROUSEL_OPTIONS;
    const hideContent = showPlaceRewardRecommendation ? '' : 'hide-content';
    let customProductsLength = 4;
    if (getViewportInfo().isDesktop) {
      customProductsLength = 2;
    }

    return (
      <>
        <div className={`cart-placecash placereward ${classname}  ${hideContent}`}>
          <div className="cart-placecash-button">
            {showPlaceRewardRecommendation && <PlaceCashBanner isLeftProgressBar isPlaceReward />}
          </div>
          {showPlaceRewardRecommendation && <hr className="horizontal-line" />}
          <div className="cart-placecash-product">
            <Recommendations
              customProductsLength={customProductsLength}
              headerLabel={labels.addProductGetPlaceReward}
              portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.BAG_CART_REWARD_PLCC}
              sequence="0"
              getRecommendationProducts={this.getPlaceRewardRecommendationProductLength}
              starRatingSize={{ small: 63, large: 60 }}
              {...recommendationAttributes}
              carouselConfigProps={carouselOptions}
              inheritedStyles={recommendationStylesPdp}
              isAbCartReward
              newBag={isNewBag}
              bagItemCount={orderItemsCount}
              isBagPage
            />
          </div>
        </div>
      </>
    );
  };

  renderRecommendationsForRecent = () => {
    const { labels, orderItemsCount, isNewBag } = this.props;
    const isNoNEmptyBag = orderItemsCount > 0;
    const headerText = isNoNEmptyBag
      ? labels.recentlyViewedFullBagPage
      : labels.recentlyViewedEmptyBagPage;
    let carouselOptions;
    if (isNoNEmptyBag) {
      carouselOptions = BagPageUtils.CarouselOptions.CAROUSEL_OPTIONS;
    }
    let customProductsLength = 3;
    if (getViewportInfo().isDesktop) {
      customProductsLength = 6;
    }
    return (
      <div className="recentlyViewed">
        <Recommendations
          customProductsLength={customProductsLength}
          page={Constants.RECOMMENDATIONS_PAGES_MAPPING.BAG}
          headerLabel={headerText}
          variations="moduleO"
          portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
          inheritedStyles={recommendationStyles}
          carouselConfigProps={carouselOptions}
          starRatingSize={{ small: 68, large: 68 }}
          sequence="2"
          showPeek={{ small: true, medium: false }}
          newBag={isNewBag}
          bagItemCount={orderItemsCount}
          isBagPage
        />
      </div>
    );
  };

  // eslint-disable-next-line complexity
  saveLaterAndRecommendation = () => {
    const { sflItems, orderItemsCount, bagLoading, labels, isNewBag } = this.props;
    const {
      isCartBackAbTestEnabled,
      isShowSaveForLaterSwitch,
      isSflItemRemoved,
      isLowInventoryBannerEnabled,
      isLowInventoryBannerEnabledForOldDesign,
    } = this.props;
    const viewPortInfo = getViewportInfo();
    let isMobileDevice = false;
    if (viewPortInfo) {
      const { isMobile } = viewPortInfo;
      isMobileDevice = isMobile;
    }
    const { activeSection } = this.state;
    const sflItemsCount = sflItems.size;
    const myBag = 'myBag';
    if (isCartBackAbTestEnabled || isMobileDevice) {
      return (
        <React.Fragment>
          {isShowSaveForLaterSwitch &&
            this.wrapSection(
              <div
                className={`save-for-later-section ${
                  activeSection === BAGPAGE_CONSTANTS.SFL_STATE
                    ? 'activeSection'
                    : 'inActiveSection'
                } ${
                  sflItems.size === 0 && !isSflItemRemoved ? 'hide-on-desktop hide-on-tablet' : ''
                }`}
              >
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs16"
                  fontWeight={['semibold']}
                  className="elem-mt-XXL elem-mb-XL save-for-later-section-heading"
                >
                  {`${labels.savedForLaterText} (${sflItems.size})`}
                </BodyCopy>
                <ProductTileWrapper
                  bagLabels={labels}
                  pageView={myBag}
                  sflItems={sflItems}
                  showPlccApplyNow={false}
                  isBagPageSflSection
                  sflItemsCount={sflItemsCount}
                  newBag={isNewBag}
                  isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
                  isLowInventoryBannerEnabledForOldDesign={isLowInventoryBannerEnabledForOldDesign}
                />
              </div>,
              orderItemsCount
            )}
          {!bagLoading && this.renderRecommendations()}
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        {!bagLoading && this.renderRecommendations()}
        {isShowSaveForLaterSwitch &&
          this.wrapSection(
            <div
              className={`save-for-later-section ${
                activeSection === BAGPAGE_CONSTANTS.SFL_STATE ? 'activeSection' : 'inActiveSection'
              } ${
                sflItems.size === 0 && !isSflItemRemoved ? 'hide-on-desktop hide-on-tablet' : ''
              }`}
            >
              <BodyCopy
                fontFamily="secondary"
                fontSize="fs16"
                fontWeight={['semibold']}
                className="elem-mt-XXL elem-mb-MED save-for-later-section-heading"
              >
                {`${labels.savedForLaterText} (${sflItems.size})`}
              </BodyCopy>
              <ProductTileWrapper
                bagLabels={labels}
                pageView={myBag}
                sflItems={sflItems}
                showPlccApplyNow={false}
                isBagPageSflSection
                sflItemsCount={sflItemsCount}
                newBag={isNewBag}
                isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
                isLowInventoryBannerEnabledForOldDesign={isLowInventoryBannerEnabledForOldDesign}
              />
            </div>,
            orderItemsCount
          )}
      </React.Fragment>
    );
  };

  renderLeftSection = () => {
    const {
      labels,
      sflItems,
      isPlaceCashEnabled,
      isUserLoggedIn,
      isPlaceRewardEnabled,
      cartSflMerged,
      isNewBag,
      isLowInventoryBannerEnabled,
      cartOrderItems,
      isLowInventoryBannerEnabledForOldDesign,
    } = this.props;
    const { activeSection } = this.state;
    const myBag = 'myBag';
    const sflItemsCount = sflItems.size;
    return (
      <React.Fragment>
        {isPlaceRewardEnabled &&
          isUserLoggedIn &&
          !isCanada() &&
          this.renderPlaceRewardRecommendations()}
        {isPlaceCashEnabled && this.renderPlaceCashRecommendations()}
        <div
          className={`bag-section ${
            activeSection === BAGPAGE_CONSTANTS.BAG_STATE ? 'activeSection' : 'inActiveSection'
          }`}
        >
          {this.showMergeCartLimitError()}
          <ProductTileWrapper
            bagLabels={labels}
            pageView={myBag}
            showPlccApplyNow
            setHeaderErrorState={this.setHeaderErrorState}
            sflItemsCount={sflItemsCount}
            cartSflMerged={cartSflMerged}
            newBag={isNewBag}
            isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
            cartOrderItems={cartOrderItems}
            isLowInventoryBannerEnabledForOldDesign={isLowInventoryBannerEnabledForOldDesign}
          />
        </div>
        {this.saveLaterAndRecommendation()}
      </React.Fragment>
    );
  };

  renderActions = () => {
    const {
      labels,
      showAddTobag,
      handleCartCheckout,
      isMobile,
      isBagPage,
      bagLoading,
      isApplePayVisible,
      orderBalanceTotal,
      isNewBag,
      isStickyCheckoutButtonEnabled,
    } = this.props;
    const { isScrolling } = this.state;
    return (
      <div ref={this.getBagActionsContainerRef}>
        <AddedToBagActions
          labels={labels}
          showAddTobag={showAddTobag}
          inheritedStyles={addedToBagActionsStyles}
          handleCartCheckout={handleCartCheckout}
          paypalButtonHeight={isMobile ? 42 : 48}
          containerId="paypal-button-container-bag"
          isBagPage={isBagPage}
          bagLoading={bagLoading}
          isApplePayVisible={isApplePayVisible}
          orderBalanceTotal={orderBalanceTotal}
          newBag={isNewBag}
          isScrolling={isScrolling}
          isStickyCheckoutButtonEnabled={isStickyCheckoutButtonEnabled}
        />
      </div>
    );
  };

  stickyBagCondensedHeader = (headerError, params) => {
    const {
      labels,
      showAddTobag,
      handleCartCheckout,
      isBagPage,
      bagLoading,
      isApplePayVisible,
      isNewBag,
      isStickyCheckoutButtonEnabled,
    } = this.props;
    const { orderBalanceTotal, totalCount, orderItemsCount } = this.props;
    const { showCondensedHeader, isScrolling } = this.state;
    // if (!showCondensedHeader) return null;
    return (
      <div
        ref={this.getBagCondensedHeader}
        className={`${
          orderItemsCount === 0 || orderItemsCount === false || !showCondensedHeader
            ? 'hidden-condensed-header'
            : ''
        }`}
      >
        <Row className="bag-condensed-header">
          <Row className="content-wrapper">
            <Col className="bagHeaderText" colSize={{ large: 8, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs16" fontWeight="semibold">
                {`${labels.bagHeading} (${totalCount})`}
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs13"
                  component="span"
                  className="elem-ml-SM"
                >
                  {`${labels.totalLabel}: `}
                  <PriceCurrency price={orderBalanceTotal} />
                </BodyCopy>
              </BodyCopy>
            </Col>
            <Col colSize={{ large: 4, medium: 4, small: 3 }}>
              <AddedToBagActions
                labels={labels}
                showAddTobag={showAddTobag}
                inheritedStyles={addedToBagActionsStyles}
                handleCartCheckout={handleCartCheckout}
                isBagPageStickyHeader
                paypalButtonHeight={42}
                containerId="paypal-button-container-bag-header"
                isBagPage={isBagPage}
                bagLoading={bagLoading}
                showCondensedHeader={showCondensedHeader}
                isApplePayVisible={isApplePayVisible}
                orderBalanceTotal={orderBalanceTotal}
                newBag={isNewBag}
                isScrolling={isScrolling}
                isStickyCheckoutButtonEnabled={isStickyCheckoutButtonEnabled}
              />
            </Col>
          </Row>
          <Row className="with-condensed-header">{this.renderHeaderError(headerError, params)}</Row>
        </Row>
      </div>
    );
  };

  getHeaderError = ({
    labels,
    orderItems,
    pageView,
    isUnavailable,
    isSoldOut,
    getUnavailableOOSItems,
    confirmRemoveCartItem,
    isBagPageSflSection,
    isCartItemSFL,
    isCartItemsUpdating,
    isSflItemRemoved,
    isSflItemMovedToBag,
  }) => {
    const { cartItemSflError, bagPageServerError, isModalOpen, isPickupModalOpen, isNewBag } =
      this.props;
    return (
      <InformationHeader
        labels={labels}
        orderItems={orderItems}
        pageView={pageView}
        isUnavailable={isUnavailable}
        isSoldOut={isSoldOut}
        getUnavailableOOSItems={getUnavailableOOSItems}
        confirmRemoveCartItem={confirmRemoveCartItem}
        isBagPageSflSection={isBagPageSflSection}
        isCartItemSFL={isCartItemSFL}
        isCartItemsUpdating={isCartItemsUpdating}
        isSflItemRemoved={isSflItemRemoved}
        cartItemSflError={cartItemSflError}
        bagPageServerError={bagPageServerError}
        isModalOpen={isModalOpen}
        isPickupModalOpen={isPickupModalOpen}
        isSflItemMovedToBag={isSflItemMovedToBag}
        isNewBag={isNewBag}
      />
    );
  };

  renderHeaderError = (headerError, params, showCondensedHeader) => {
    return headerError && !showCondensedHeader && this.getHeaderError(params[0]);
  };

  getIsShowRightSection = () => {
    const { isMobile, orderItemsCount, orderItemsCountLocal } = this.props;
    const { activeSection } = this.state;
    const isNoNEmptyBag = orderItemsCount > 0 || orderItemsCountLocal > 0;
    return isMobile ? activeSection === BAGPAGE_CONSTANTS.BAG_STATE : isNoNEmptyBag;
  };

  showMergeCartLimitError = () => {
    const { showMaxItemError } = this.props;
    const { labels: { maxQuantityError = '' } = {} } = this.props;
    if (showMaxItemError) {
      return (
        <Notification
          className="max-qty-reached elem-mt-MED"
          status="error"
          scrollIntoView
          isErrorTriangleIcon
          message={maxQuantityError}
        />
      );
    }
    return null;
  };

  getEstimatedHeaderCls = (activeSection) =>
    activeSection === BAGPAGE_CONSTANTS.BAG_STATE ? 'activeEstimatedHeader' : '';

  // eslint-disable-next-line sonarjs/cognitive-complexity, complexity
  render() {
    const {
      className,
      labels,
      totalCount,
      orderItemsCount,
      orderItemsCountLocal,
      isUserLoggedIn,
      isGuest,
      rememberedUser,
      isSignInBannerAbTestEnabled,
      bagLoading,
      isNewBag,
      isLowInventoryBannerEnabled,
      isStickyCheckoutButtonEnabled,
    } = this.props;
    const { sflItems, isShowSaveForLaterSwitch, orderBalanceTotal } = this.props;
    const {
      activeSection,
      showStickyHeaderMob,
      loadPaypalStickyHeader,
      headerError,
      showCondensedHeader,
      orderLedgerAfterView,
    } = this.state;
    const { params } = this.state;
    const isNoNEmptyBag = orderItemsCount > 0 || orderItemsCountLocal > 0;
    const isNonEmptySFL = sflItems.size > 0;
    const isNotLoaded = orderItemsCount === false;
    const showRightSection = this.getIsShowRightSection();

    return (
      <div className={className}>
        {loadPaypalStickyHeader && this.stickyBagCondensedHeader(headerError, params)}
        <div
          ref={this.getBagPageHeaderRef}
          className={`${showStickyHeaderMob ? 'stickyBagHeader' : ''}`}
        >
          <Row tagName="header" className="cartPageTitleHeader">
            <Col
              colSize={{ small: 2.5, medium: 4, large: 6 }}
              className="left-sec"
              onClick={() => {
                BagPageUtils.handleChangeActiveSection(BAGPAGE_CONSTANTS.BAG_STATE, this);
              }}
            >
              <Heading
                component="h1"
                variant="h6"
                fontSize="fs16"
                color="text.primary"
                className={`bag-header ${
                  activeSection === BAGPAGE_CONSTANTS.BAG_STATE ? 'activeHeader' : ''
                }`}
              >
                {`${labels.bagHeading} (${totalCount})`}
              </Heading>
              {totalCount > 0 && (
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs10"
                  className={`estimatedHeaderText ${this.getEstimatedHeaderCls(activeSection)}`}
                >
                  {`${labels.totalLabel}: `}
                  <PriceCurrency price={orderBalanceTotal} />
                </BodyCopy>
              )}
            </Col>
            {isShowSaveForLaterSwitch && (
              <Col
                colSize={{ small: 3.5, medium: 4, large: 6 }}
                className="left-sec"
                onClick={() => {
                  BagPageUtils.handleChangeActiveSection(BAGPAGE_CONSTANTS.SFL_STATE, this);
                }}
              >
                <Heading
                  variant="h6"
                  fontSize="fs16"
                  color="text.primary"
                  className={`bag-header bag-header-sfl ${
                    activeSection === BAGPAGE_CONSTANTS.SFL_STATE ? 'activeHeader' : ''
                  }`}
                >
                  {`${labels.savedLaterButton} (${sflItems.size})`}
                </Heading>
                {activeSection === BAGPAGE_CONSTANTS.SFL_STATE && (
                  <BodyCopy
                    fontFamily="secondary"
                    fontSize="fs10"
                    className="estimatedHeaderText activeEstimatedHeader save-for-later"
                  />
                )}
              </Col>
            )}
          </Row>

          {this.renderHeaderError(headerError, params, showCondensedHeader)}
        </div>

        <CnCTemplate
          leftSection={this.renderLeftSection}
          showLeftSection={(isNoNEmptyBag && showRightSection) || isNotLoaded}
          // isNotLoaded={!isNotLoaded}
          orderLedgerAfterView={orderBalanceTotal > 0 ? this.renderActions() : orderLedgerAfterView}
          isUserLoggedIn={isUserLoggedIn}
          isGuest={isGuest}
          showAccordian={false}
          isNonEmptySFL={isNonEmptySFL}
          pageCategory={BAGPAGE_CONSTANTS.BAG_PAGE}
          labels={labels}
          isSignInBannerAbTestEnabled={isSignInBannerAbTestEnabled}
          isBagPage
          isNoNEmptyBag={isNoNEmptyBag}
          rememberedUser={rememberedUser}
          bagLoading={bagLoading}
          newBag={isNewBag}
          isLowInventoryBannerEnabled={isLowInventoryBannerEnabled}
          isStickyCheckoutButtonEnabled={isStickyCheckoutButtonEnabled}
        />
      </div>
    );
  }
}

BagPageView.propTypes = BagPageUtils.BagPagePropTypes;
BagPageView.defaultProps = {
  cartItemSflError: '',
  bagPageServerError: null,
  excludedIds: '',
  isCartBackAbTestEnabled: false,
  isSignInBannerAbTestEnabled: false,
  isDisableBagAnchoring: false,
};
/**
 * Hotfix-Aware Component. The use of `withRefWrapper` and `withHotfix`
 * below are just for making the page hotfix-aware. The "BagPage"
 * argument defines the displayName of the hotfix-aware component.
 */
const BagPageViewWithRefWrapper = withRefWrapper(BagPageView, 'div', 'BagPage');
const BagPageViewWithHotfix = withHotfix(BagPageViewWithRefWrapper);
const BagPageWithRouter = withRouter(BagPageViewWithHotfix);

export default withStyles(BagPageWithRouter, styles);
export { BagPageView as BagPageViewVanilla };
