// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import LoyaltyConstants from '@tcp/core/src/components/features/CnC/LoyaltyBanner/LoyaltyConstants';
import BAGPAGE_CONSTANTS from '../BagPage.constants';
import BagPageReducer from '../container/BagPage.reducer';

describe('BagPage Reducer', () => {
  const initialState = {
    orderDetails: { orderItems: [], orderId: '1234' },
    sfl: fromJS([]),
    loaded: false,
    errors: false,
    uiFlags: {
      isItemMovedToSflList: false,
      isSflItemDeleted: false,
      cartItemSflError: null,
      isCartItemsUpdating: fromJS({}),
      isPayPalWebViewEnable: undefined,
      isSflItemMovedToBag: false,
      isGettingPaypalSettings: false,
      updateMiniBag: true,
    },
    openItemDeleteConfirmationModalInfo: { showModal: false },
  };
  const initialStateMutated = fromJS(initialState);

  const getOrderDetailAction = {
    type: BAGPAGE_CONSTANTS.GET_ORDER_DETAILS_COMPLETE,
    payload: {
      orderDetails: {
        orderItems: [1],
      },
    },
  };

  const loyaltyPayload = {
    type: LoyaltyConstants.UPDATE_POINTS_IN_CART,
    payload: {
      estimatedRewards: 1000000,
      isBrierleyWorking: true,
      orderItems: [],
    },
  };

  const setBagPageErrors = {
    type: BAGPAGE_CONSTANTS.SET_BAG_PAGE_ERRORS,
    payload: {
      error: {
        errorMessage: [1],
      },
    },
  };

  it('GET_ORDER_DETAILS_COMPLETE', () => {
    const newState = BagPageReducer(initialStateMutated, {
      ...loyaltyPayload,
    });

    expect(newState.getIn(['orderDetails', 'estimatedRewards'])).toEqual(
      loyaltyPayload.payload.estimatedRewards
    );
    expect(newState.getIn(['orderDetails', 'isBrierleyWorking'])).toEqual(
      loyaltyPayload.payload.isBrierleyWorking
    );
  });

  it('GET_ORDER_DETAILS_COMPLETE', () => {
    const newState = BagPageReducer(initialStateMutated, {
      ...getOrderDetailAction,
    });

    expect(newState.get('orderDetails').length).toEqual(getOrderDetailAction.payload.length);
  });

  it('GET_ORDER_DETAILS_COMPLETE', () => {
    const newState = BagPageReducer(initialStateMutated, {
      ...setBagPageErrors,
    });

    expect(newState.get('orderDetails').length).toEqual(setBagPageErrors.payload.length);
  });

  it('GET_ORDER_DETAILS_COMPLETE', () => {
    const newState = BagPageReducer(initialState, {});

    expect(newState).toEqual(initialStateMutated);
  });

  it('CLOSE_CHECKOUT_CONFIRMATION_MODAL', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.CLOSE_CHECKOUT_CONFIRMATION_MODAL,
    });

    expect(newState.get('showConfirmationModal')).toEqual(false);
  });

  it('RESET_BAG_LOADED_STATE', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.RESET_BAG_LOADED_STATE,
    });

    expect(newState.get('loaded')).toEqual(false);
  });

  it('OPEN_CHECKOUT_CONFIRMATION_MODAL', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.OPEN_CHECKOUT_CONFIRMATION_MODAL,
    });

    expect(newState.get('showConfirmationModal')).toEqual(true);
  });

  it('SET_MODULEX_CONTENT', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.SET_MODULEX_CONTENT,
      payload: [],
    });

    expect(newState.get('moduleXContent')).toEqual(fromJS([]));
  });

  it('SET_ITEM_OOS', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.SET_ITEM_OOS,
      payload: '123',
    });

    expect(newState).toEqual(initialStateMutated);
  });

  it('CART_ITEMS_SET_SFL', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.CART_ITEMS_SET_SFL,
      payload: false,
    });

    expect(newState).toEqual(initialStateMutated);
  });

  it('CART_ITEMS_SET_SFL_ERROR', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.CART_ITEMS_SET_SFL_ERROR,
      payload: null,
    });

    expect(newState).toEqual(initialStateMutated);
  });

  it('SET_SFL_DATA', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.SET_SFL_DATA,
      payload: fromJS([]),
    });

    expect(newState).toEqual(initialStateMutated);
  });

  it('CART_ITEMS_SET_UPDATING', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.CART_ITEMS_SET_UPDATING,
      payload: fromJS({}),
    });

    expect(newState).toEqual(initialStateMutated);
  });

  it('OPEN_ITEM_DELETE_CONFIRMATION_MODAL', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.OPEN_ITEM_DELETE_CONFIRMATION_MODAL,
      payload: { itemId: 123 },
    });

    expect(newState).toEqual(
      initialStateMutated.set('openItemDeleteConfirmationModalInfo', {
        showModal: true,
        itemId: 123,
      })
    );
  });

  it('CLOSE_ITEM_DELETE_CONFIRMATION_MODAL', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.CLOSE_ITEM_DELETE_CONFIRMATION_MODAL,
    });

    expect(newState).toEqual(
      initialStateMutated.set('openItemDeleteConfirmationModalInfo', {
        showModal: false,
      })
    );
  });

  it('CART_SUMMARY_SET_ORDER_ID', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: 'CART_SUMMARY_SET_ORDER_ID',
      orderId: '1234',
    });

    expect(newState).toEqual(initialStateMutated);
  });

  it('SFL_ITEMS_SET_DELETED', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.SFL_ITEMS_SET_DELETED,
      payload: false,
    });

    expect(newState).toEqual(initialStateMutated);
  });

  it('PAYPAL_WEBVIEW_ENABLE', () => {
    const newStatee = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.PAYPAL_WEBVIEW_ENABLE,
      isPayPalWebViewEnable: 'false',
    });

    expect(newStatee).toEqual(initialStateMutated);
  });

  it('SFL_ITEMS_MOVED_TO_BAG', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.SFL_ITEMS_MOVED_TO_BAG,
      payload: false,
    });

    expect(newState).toEqual(initialStateMutated);
  });

  it('PAYPAL_SETTINGS_CALL_STATUS', () => {
    const paypalSettingsStatusPayload = {
      type: BAGPAGE_CONSTANTS.PAYPAL_SETTINGS_CALL_STATUS,
      payload: true,
    };

    const newState = BagPageReducer(initialStateMutated, {
      ...paypalSettingsStatusPayload,
    });

    expect(newState.getIn(['uiFlags', 'isGettingPaypalSettings'])).toEqual(
      paypalSettingsStatusPayload.payload
    );
  });

  it('UPDATE_MINIBAG_FLAG', () => {
    const newState = BagPageReducer(initialStateMutated, {
      type: BAGPAGE_CONSTANTS.UPDATE_MINIBAG_FLAG,
      payload: true,
    });

    expect(newState).toEqual(initialStateMutated);
  });
});
