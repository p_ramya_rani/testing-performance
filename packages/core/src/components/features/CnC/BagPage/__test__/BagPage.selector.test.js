// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable import/no-named-as-default-member */
import { fromJS } from 'immutable';
import BAGPAGE_SELECTORS from '../container/BagPage.selectors';

describe('#Added to bag Selectors', () => {
  const BagPageState = {
    checkout: {
      bagPage: {
        lbl_header_bag: 'bagHeading',
        lbl_emptyBag_loggedInMsg: 'loggedInMsg',
        lbl_emptyBag_notLoggedInMsg: 'guestUserMsg',
        lbl_emptyBag_loginIn: 'login',
        lbl_emptyBag_shopNow: 'shopNow',
        lbl_emptyBag_inspirationTagLine: 'tagLine',
        lbl_emptyBag_helperMsg: 'helperMsg',
        lbl_sfl_myBagButton: 'myBagButton',
        lbl_add_product_toget_palcecash: 'addProductGetPlaceCash',
        lbl_add_product_toget_palcereward: 'addProductGetPlaceReward',
        lbl_sfl_savedForLater: 'savedForLaterText',
        lbl_sfl_savedLaterButton: 'savedLaterButton',
        lbl_sfl_emptySflMsg_1: 'emptySflMsg1',
        lbl_sfl_emptySflMsg_2: 'emptySflMsg2',
        bl_sfl_actionSuccess: 'sflSuccess',
        lbl_sfl_itemDeleteSuccess: 'sflDeleteSuccess',
        lbl_edd_arrivesby: 'arrivesBy',
      },
    },
    global: {
      addedToBagModal: {
        lbl_header_addedToBag: 'AddedToBag',
        lbl_add_product_toget_palcecash: 'addProductGetPlaceCash',
        lbl_add_product_toget_palcereward: 'addProductGetPlaceReward',
        lbl_cta_checkout: 'Checkout',
        referred: [],
      },
      checkoutConfirmation: {},
    },
  };
  const addressLine1 = 'Test';
  const addressLine2 = 'Test2';
  const givenName = 'Rajeev';
  const familyName = 'Jain';
  const countryCode = '+91';
  const locality = 'Noida';
  const postalCode = '10001';
  const phoneNumber = '999999999';
  const administrativeArea = 'TX';

  const completeShippingAddress = {
    givenName,
    familyName,
    addressLines: [addressLine1, addressLine2],
    locality,
    countryCode,
    administrativeArea,
    postalCode,
    phoneNumber,
  };
  const emailAddress = 'test@yopmail.com';
  const CartPageReducer = fromJS({
    orderDetails: {
      totalItems: 0,
      orderItems: [],
      subTotal: 10,
      checkout: {
        shipping: {
          address: {
            state: administrativeArea,
            zipCode: postalCode,
            firstName: givenName,
            lastName: familyName,
            city: locality,
            country: countryCode,
            addressLine1,
            addressLine2,
          },
          emailAddress,
          phoneNumber,
        },
      },
    },
    sfl: [],
    cartSflMerged: true,
    openItemDeleteConfirmationModalInfo: true,
    uiFlags: {
      isPayPalWebViewEnable: false,
    },
    isPayPalEnabled: false,
    loaded: false,
  });

  const sessionState = {
    siteDetails: {
      SFL_MAX_COUNT: '200',
      BAG_CONDENSE_HEADER_INTERVAL: 3000,
      OPTIMISE_ADDPRODUCT_ORDERDETAILS_ENABLED: '1',
    },
  };

  const state = {
    Labels: BagPageState,
    CartPageReducer,
    session: sessionState,
  };

  it('#getAddedToBagData should return itemInfo', () => {
    expect(BAGPAGE_SELECTORS.getBagPageLabels(state)).toEqual({
      addProductGetPlaceCash: 'lbl_add_product_toget_placecash',
      addProductGetPlaceReward: 'lbl_add_product_toget_placereward',
      addedToBag: 'lbl_header_addedToBag',
      applyNow: 'lbl_emptyBag_applyNow',
      recentlyViewed: 'lbl_recently_viewed',
      recentlyViewedEmptyBagPage: 'lbl_recently_viewed_empty_bag_page',
      recentlyViewedFullBagPage: 'lbl_recently_viewed_full_bag_page',
      youMayAlsoLike: 'lbl_you_may_also_like',
      youMayAlsoLikeEmptyBagPage: 'lbl_you_may_also_like_empty_bag_page',
      youMayAlsoLikeFullBagPage: 'lbl_you_may_also_like_full_bag_page',
      totalLabel: 'lbl_orderledger_total',
      bagHeading: 'bagHeading',
      cartSflMergedSuccessMsg: 'lbl_sfl_cartSflMergedSuccessMsg',
      checkout: 'lbl_cta_checkout',
      confirmationPage: 'lbl_confirmation_page',
      confirmationPageCheckoutSimilarStyles: 'lbl_confirmation_page_checkout_similar_styles',
      confirmationPageOrderNotification: 'lbl_confirmation_page_order_notification ',
      confirmationPageRecentlyViewed: 'lbl_confirmation_page_recently_viewed',
      guestUserMsg: 'guestUserMsg',
      helperMsg: 'helperMsg',
      loggedInMsg: 'loggedInMsg',
      login: 'login',
      maxQuantityError: 'lbl_coupon_max_quantity_error',
      shopNow: 'shopNow',
      signinBannerCreateAccount: 'lbl_signin_cta_banner_create_account',
      signinCtaBannerOr: 'lbl_signin_cta_banner_or',
      signinCtaBannerSignin: 'lbl_signin_cta_banner_signin',
      tagLine: 'tagLine',
      myBagButton: 'myBagButton',
      savedForLaterText: 'savedForLaterText',
      savedLaterButton: 'savedLaterButton',
      emptySflMsg1: 'emptySflMsg1',
      emptySflMsg2: 'emptySflMsg2',
      sflSuccess: 'sflSuccess',
      sflDeleteSuccess: 'sflDeleteSuccess',
      sflMovedToBagSuccess: 'lbl_sfl_itemMoveToBagSuccess',
      arrivesBy: 'arrivesBy',
      estimatedToArriveBy: 'lbl_estimate_to_arrive',
    });
  });

  it('#getTotalItems', () => {
    expect(BAGPAGE_SELECTORS.getTotalItems(state)).toEqual(0);
  });

  it('#getOrderItems', () => {
    expect(BAGPAGE_SELECTORS.getOrderItems(state)).toEqual(fromJS([]));
  });

  it('#getOrderSubTotal', () => {
    expect(BAGPAGE_SELECTORS.getOrderSubTotal(state)).toEqual(10);
  });

  it('#getUnqualifiedItemsIds', () => {
    expect(BAGPAGE_SELECTORS.getUnqualifiedItemsIds(state)).toEqual(fromJS([]));
  });
  it('#getUnavailableCount', () => {
    expect(BAGPAGE_SELECTORS.getUnavailableCount(state)).toEqual(0);
  });

  it('#getUnqualifiedCount', () => {
    expect(BAGPAGE_SELECTORS.getUnqualifiedCount(state)).toEqual(0);
  });

  it('#getNeedHelpContentId', () => {
    expect(BAGPAGE_SELECTORS.getNeedHelpContent(state)).toEqual(0);
  });

  it('#getConfirmationModalFlag', () => {
    expect(BAGPAGE_SELECTORS.getConfirmationModalFlag(state)).toEqual({
      isEditingItem: undefined,
      showModal: undefined,
    });
  });
  it('#getCurrentOrderId', () => {
    expect(BAGPAGE_SELECTORS.getCurrentOrderId(state)).toEqual(0);
  });
  it('#getsflItemsList', () => {
    expect(BAGPAGE_SELECTORS.getsflItemsList(state)).toEqual(fromJS([]));
  });

  it('#checkoutIfItemIsUnqualified', () => {
    expect(BAGPAGE_SELECTORS.checkoutIfItemIsUnqualified(state, 123)).toEqual(false);
  });

  it('#getIsPayPalEnabled', () => {
    expect(BAGPAGE_SELECTORS.getIsPayPalEnabled(state)).toEqual(false);
  });

  it('#getCurrentDeleteSelectedItemInfo', () => {
    expect(BAGPAGE_SELECTORS.getCurrentDeleteSelectedItemInfo(state)).toEqual(true);
  });
  it('#itemDeleteModalLabels', () => {
    expect(BAGPAGE_SELECTORS.itemDeleteModalLabels(state)).toEqual({
      modalButtonConfirmDelete: 'lbl_itemDelete_modalButtonConfirmDelete',
      modalButtonSFL: 'lbl_itemDelete_modalButtonSFL',
      modalHeading: 'lbl_itemDelete_modalHeading',
      modalTitle: 'lbl_itemDelete_modalTitle',
    });
  });
  it('#getBagStickyHeaderInterval should return interval', () => {
    expect(BAGPAGE_SELECTORS.getBagStickyHeaderInterval(state)).toEqual(
      sessionState.siteDetails.BAG_CONDENSE_HEADER_INTERVAL
    );
  });

  it('#getPayPalWebViewStatus should return paypalView enable status', () => {
    expect(BAGPAGE_SELECTORS.getPayPalWebViewStatus(state)).toEqual(false);
  });
  it('#getCartLoadedState', () => {
    expect(BAGPAGE_SELECTORS.getCartLoadedState(state)).toEqual(false);
  });
  it('#getOptimiseAddProductOrderDetailsEnabled should return value', () => {
    expect(BAGPAGE_SELECTORS.getOptimiseAddProductOrderDetailsEnabled(state)).toEqual(1);
  });
  it('#getShippingAddressState should return value', () => {
    expect(BAGPAGE_SELECTORS.getShippingAddressState(state)).toEqual('TX');
  });
  it('#getShippingAddressZip should return value', () => {
    expect(BAGPAGE_SELECTORS.getShippingAddressZip(state)).toEqual('10001');
  });
  it('#getShippingAddressEmail should return value', () => {
    expect(BAGPAGE_SELECTORS.getShippingAddressEmail(state)).toEqual(emailAddress);
  });
  it('#getCompleteShippingAddress should return value', () => {
    expect(BAGPAGE_SELECTORS.getCompleteShippingAddress(state)).toEqual(completeShippingAddress);
  });
  it('#getShippingContactForApplePay should return value if address found in cart', () => {
    const shippingAddress = { ...completeShippingAddress, emailAddress };
    expect(BAGPAGE_SELECTORS.getShippingContactForApplePay(state)).toEqual(shippingAddress);
  });

  it('#getShowMaxItemErrorDisplayed return false', () => {
    expect(BAGPAGE_SELECTORS.getShowMaxItemErrorDisplayed(state)).toEqual(false);
  });

  it('#getIsGettingPaypalSettings should return false', () => {
    expect(BAGPAGE_SELECTORS.getIsGettingPaypalSettings(state)).toEqual(false);
  });

  it('#getCartSflMergedshould return true', () => {
    expect(BAGPAGE_SELECTORS.getCartSflMerged(state)).toEqual(true);
  });
});
