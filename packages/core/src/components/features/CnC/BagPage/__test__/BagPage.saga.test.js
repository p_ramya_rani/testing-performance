/* eslint-disable max-lines */
/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d
import { put, takeLatest, select, call } from 'redux-saga/effects';
// import { validateReduxCache } from '../../../../../../utils/cache.util';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import {
  getOrderDetailSaga,
  BagPageSaga,
  getCartDataSaga,
  fetchModuleX,
  startCartCheckout,
  removeUnqualifiedItemsAndCheckout,
  checkoutCart,
  startPaypalCheckout,
  startApplePayCheckout,
  authorizePayPalPayment,
  routeForCartCheckout,
  addItemToSFL,
  getSflDataSaga,
  setSflItemUpdate,
  startSflItemMoveToBag,
  startPaypalNativeCheckout,
  setModifiedSflData,
  filterProductsBrand,
  getUnbxdXappConfigs,
  getTranslatedProductInfo,
  renderBagPageCheckoutLoader,
  renderMobileLoader,
  mergeTwoObjects,
  setBagPageLoading,
  handelAddressIds,
  setBagpageCarousel,
  shouldTranslate,
  updateBopisItems,
} from '../container/BagPage.saga';
import startSflItemDelete from '../container/BagPage.saga.util';
import BAG_PAGE_ACTIONS from '../container/BagPage.actions';
import BAGPAGE_CONSTANTS from '../BagPage.constants';
import BAG_SELECTORS from '../container/BagPage.selectors';
import { setCheckoutModalMountedState } from '../../../account/LoginPage/container/LoginPage.actions';
import { isMobileApp, isCanada } from '../../../../../utils';
import { getUserInfoSaga } from '../../../account/User/container/User.saga';

jest.mock('../../../../../utils', () => ({
  isMobileApp: jest.fn(),
  getViewportInfo: jest.fn(),
  isCanada: jest.fn(),
  parseBoolean: () => false,
  isClient: () => true,
  routerPush: jest.fn(),
}));

describe('Cart Item saga', () => {
  it('should filter products brand', () => {
    const arr = [
      {
        productInfo: {
          itemBrand: 'test',
          productPartNumber: '1',
        },
      },
    ];
    const searchedValue = 'test';
    const result = filterProductsBrand(arr, searchedValue);
    expect(result).toEqual(['1']);
  });

  it('should return getUnbxdXappConfigs', () => {
    const getUnbxdXappConfigsGen = getUnbxdXappConfigs();
    getUnbxdXappConfigsGen.next();
    getUnbxdXappConfigsGen.next(true);
    const result = getUnbxdXappConfigsGen.next('drURLpath').value;
    expect(result).toEqual({
      isFlipURL: true,
      drURLpath: 'drURLpath',
    });
  });

  it('should return getTranslatedProductInfo for TCP', () => {
    const cartInfo = {
      orderDetails: {
        orderItems: [
          {
            productInfo: {
              itemBrand: 'TCP',
              productPartNumber: '1',
            },
          },
        ],
      },
    };
    const getTranslatedProductInfoGen = getTranslatedProductInfo(cartInfo);
    getTranslatedProductInfoGen.next();

    getTranslatedProductInfoGen.next();
    const result = getTranslatedProductInfoGen.next({ body: { response: { products: [] } } }).value;
    expect(result).toEqual([]);
  });

  it('should return getTranslatedProductInfo for GYM', () => {
    const cartInfo = {
      orderDetails: {
        orderItems: [
          {
            productInfo: {
              itemBrand: 'GYM',
              productPartNumber: '1',
            },
          },
        ],
      },
    };
    const getTranslatedProductInfoGen = getTranslatedProductInfo(cartInfo);
    getTranslatedProductInfoGen.next();
    getTranslatedProductInfoGen.next();
    const result = getTranslatedProductInfoGen.next({ body: { response: { products: [] } } }).value;
    expect(result).toEqual([]);
  });

  it('should throw error', () => {
    const cartInfo = {
      orderDetails: {
        orderItems: [
          {
            productInfo: {
              itemBrand: 'GYM',
              productPartNumber: '1',
            },
          },
        ],
      },
    };
    const getTranslatedProductInfoGen = getTranslatedProductInfo(cartInfo);
    getTranslatedProductInfoGen.next();
    getTranslatedProductInfoGen.next();
    const result = getTranslatedProductInfoGen.next({ body: undefined }).value;
    expect(result).toEqual([]);
  });

  it('startPaypalCheckout for non billingpage', () => {
    const input = {
      payload: {
        isBillingPage: false,
        resolve: () => {},
        reject: () => {},
      },
    };
    const startPaypalCheckoutGen = startPaypalCheckout(input);
    startPaypalCheckoutGen.next();
    startPaypalCheckoutGen.next();
    startPaypalCheckoutGen.next();
    startPaypalCheckoutGen.next();
    startPaypalCheckoutGen.next('123');
    startPaypalCheckoutGen.next();
  });

  it('startPaypalCheckout for billingpage', () => {
    const input = {
      payload: {
        isBillingPage: true,
        resolve: () => {},
        reject: () => {},
      },
    };
    const startPaypalCheckoutGen = startPaypalCheckout(input);
    startPaypalCheckoutGen.next();
    startPaypalCheckoutGen.next();
    expect(startPaypalCheckoutGen.next().done).toBe(true);
    startPaypalCheckoutGen.next();
    startPaypalCheckoutGen.next('123');
    startPaypalCheckoutGen.next();
  });

  it('startPaypalCheckout for non billingpage throw error', () => {
    const input = {
      payload: {
        isBillingPage: false,
        resolve: () => {},
        reject: () => {},
      },
    };
    const startPaypalCheckoutGen = startPaypalCheckout(input);
    startPaypalCheckoutGen.next();

    startPaypalCheckoutGen.next();
    startPaypalCheckoutGen.next(true);
    startPaypalCheckoutGen.next();
    startPaypalCheckoutGen.next();
  });

  it('startApplePayCheckout', () => {
    const startApplePayCheckoutGen = startApplePayCheckout();
    startApplePayCheckoutGen.next();

    startApplePayCheckoutGen.next();
    expect(startApplePayCheckoutGen.next().done).toBe(false);
    startApplePayCheckoutGen.next();
  });

  it('startApplePayCheckout with confirmStartCheckout', () => {
    const startApplePayCheckoutGen = startApplePayCheckout();
    startApplePayCheckoutGen.next();

    startApplePayCheckoutGen.next();
    startApplePayCheckoutGen.next(true);
  });

  it('startPaypalNativeCheckout for billingPage', () => {
    const input = {
      payload: {
        isBillingPage: true,
      },
    };
    const startApplePayCheckoutGen = startPaypalNativeCheckout(input);
    startApplePayCheckoutGen.next();

    startApplePayCheckoutGen.next();
    startApplePayCheckoutGen.next();
    startApplePayCheckoutGen.next();
    startApplePayCheckoutGen.next('123');
    startApplePayCheckoutGen.next();
    startApplePayCheckoutGen.next();
  });

  it('startPaypalNativeCheckout for non billingPage', () => {
    const input = {
      payload: {
        isBillingPage: false,
      },
    };
    const startPaypalNativeCheckoutGen = startPaypalNativeCheckout(input);
    startPaypalNativeCheckoutGen.next();

    startPaypalNativeCheckoutGen.next();
    startPaypalNativeCheckoutGen.next();
    startPaypalNativeCheckoutGen.next();
    startPaypalNativeCheckoutGen.next('123');
    startPaypalNativeCheckoutGen.next();
    startPaypalNativeCheckoutGen.next();
  });

  it('authorizePayPalPayment', () => {
    const input = {
      payload: {
        navigation: '/',
        navigationActions: () => {},
      },
    };
    const tcpOrderId = 'tcpOrderId';
    const centinelRequestPage = 'centinelRequestPage';
    const centinelPayload = 'centinelPayload';
    const centinelOrderId = 'centinelOrderId';
    const authorizePayPalPaymentGen = authorizePayPalPayment(input);
    authorizePayPalPaymentGen.next();

    authorizePayPalPaymentGen.next({
      tcpOrderId,
      centinelRequestPage,
      centinelPayload,
      centinelOrderId,
    });
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next(true);
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
  });

  it('authorizePayPalPayment for mobile app', () => {
    const input = {
      payload: {
        navigation: '/',
        navigationActions: () => {},
      },
    };
    isMobileApp.mockImplementation(() => true);
    const tcpOrderId = 'tcpOrderId';
    const centinelRequestPage = 'centinelRequestPage';
    const centinelPayload = 'centinelPayload';
    const centinelOrderId = 'centinelOrderId';
    const authorizePayPalPaymentGen = authorizePayPalPayment(input);
    authorizePayPalPaymentGen.next();

    authorizePayPalPaymentGen.next({
      tcpOrderId,
      centinelRequestPage,
      centinelPayload,
      centinelOrderId,
    });
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next(true);
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    isMobileApp.mockImplementation(() => false);
  });

  it('should setModifiedSflData', () => {
    const setModifiedSflDataGen = setModifiedSflData({
      payload: [
        { a: '1', productInfo: { productPartNumber: 1, name: '1', color: { name: '1' } } },
        { b: '2', productInfo: { productPartNumber: 2, name: '1', color: { name: '1' } } },
      ],
    });
    setModifiedSflDataGen.next();

    setModifiedSflDataGen.next(true);
    setModifiedSflDataGen.next([{ prodpartno: 1, product_name: '1', TCPColor: '1' }]);
    setModifiedSflDataGen.next();
  });

  it('should throw exception setModifiedSflData', () => {
    const setModifiedSflDataGen = setModifiedSflData();
    setModifiedSflDataGen.next();

    setModifiedSflDataGen.next(true);
    setModifiedSflDataGen.next();
    setModifiedSflDataGen.next();
  });

  it('startSflItemMoveToBag', () => {
    const input = {
      payload: {
        itemId: 1,
        multiPackItems: [],
      },
    };
    const startSflItemMoveToBagGen = startSflItemMoveToBag(input);
    startSflItemMoveToBagGen.next();

    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
  });

  it('throw exception from startSflItemMoveToBag', () => {
    const input = {
      payload: {
        multiPackItems: [],
      },
    };
    const startSflItemMoveToBagGen = startSflItemMoveToBag(input);
    startSflItemMoveToBagGen.next();

    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
    startSflItemMoveToBagGen.next();
  });

  it('should throu exception from authorizePayPalPayment', () => {
    const input = {
      payload: {
        navigation: '/',
        navigationActions: () => {},
      },
    };
    const authorizePayPalPaymentGen = authorizePayPalPayment(input);
    authorizePayPalPaymentGen.next();

    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
  });

  it('should throu exception from authorizePayPalPayment for mobileApp', () => {
    isMobileApp.mockImplementation(() => true);
    const input = {
      payload: {
        navigation: '/',
        navigationActions: () => {},
      },
    };
    const authorizePayPalPaymentGen = authorizePayPalPayment(input);
    authorizePayPalPaymentGen.next();

    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
    authorizePayPalPaymentGen.next();
  });

  it('should dispatch getOrderDetailSaga action for success resposnse', () => {
    const afterFunc = () => {};
    isMobileApp.mockImplementation(() => false);
    const getOrderDetailSagaGen = getOrderDetailSaga({ payload: { after: afterFunc } });
    getOrderDetailSagaGen.next();
    getOrderDetailSagaGen.next();
    getOrderDetailSagaGen.next();
    getOrderDetailSagaGen.next();
    getOrderDetailSagaGen.next();
    const res = {
      orderDetails: {
        productInfo: {},
        itemInfo: {},
        miscInfo: {},
        orderItems: [],
      },
    };
    getOrderDetailSagaGen.next(res);
    getOrderDetailSagaGen.next(res);
    getOrderDetailSagaGen.next(res);
    getOrderDetailSagaGen.next(res);
    getOrderDetailSagaGen.next(res);
    expect(getOrderDetailSagaGen.next(res).value).toEqual(
      put(BAG_PAGE_ACTIONS.getOrderDetailsComplete(res.orderDetails))
    );
    getOrderDetailSagaGen.next(res);
    getOrderDetailSagaGen.next();
    expect(getOrderDetailSagaGen.next().value).toEqual(call(afterFunc));
  });

  it('should dispatch getCartDataSaga action for success resposnse', () => {
    const getCartDataSagaGen = getCartDataSaga({
      payload: { isCheckoutFlow: true, translation: true, onCartRes: true },
    });
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next(false);
    getCartDataSagaGen.next(true);

    const res = {
      orderDetails: {
        productInfo: {},
        itemInfo: {},
        miscInfo: {},
        orderItems: [],
        isUpdating: false,
      },
      coupons: {},
    };
    getCartDataSagaGen.next();
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next([{ prodpartno: '123' }]);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    expect(getCartDataSagaGen.next(res).value).not.toEqual(
      put(BAG_PAGE_ACTIONS.getOrderDetailsComplete(res.orderDetails))
    );
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
  });

  it('should dispatch getCartDataSaga action for loggedInUser', () => {
    const getCartDataSagaGen = getCartDataSaga({
      payload: { isCheckoutFlow: true, translation: true },
    });
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next();
    getCartDataSagaGen.next(true);

    const res = {
      orderDetails: {
        productInfo: {},
        itemInfo: {},
        miscInfo: {},
        orderItems: [],
      },
      coupons: {},
    };
    getCartDataSagaGen.next();
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next([{ prodpartno: '123' }]);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next(res);
    expect(getCartDataSagaGen.next(res).value).not.toEqual(
      put(BAG_PAGE_ACTIONS.getOrderDetailsComplete(res.orderDetails))
    );
    getCartDataSagaGen.next(res);
    getCartDataSagaGen.next();
  });
});

describe('Bag page Saga', () => {
  it('should return correct takeLatest', () => {
    const generator = BagPageSaga();
    let takeLatestDescriptor;

    function expectValue(action, value) {
      takeLatestDescriptor = generator.next().value;
      expect(takeLatestDescriptor).toEqual(takeLatest(action, value));
    }
    expectValue(BAGPAGE_CONSTANTS.GET_ORDER_DETAILS, getOrderDetailSaga);
    expectValue(BAGPAGE_CONSTANTS.GET_CART_DATA, getCartDataSaga);

    expectValue(BAGPAGE_CONSTANTS.FETCH_MODULEX_CONTENT, fetchModuleX);

    expectValue(
      BAGPAGE_CONSTANTS.REMOVE_UNQUALIFIED_AND_CHECKOUT,
      removeUnqualifiedItemsAndCheckout
    );
    expectValue(BAGPAGE_CONSTANTS.ROUTE_FOR_CART_CHECKOUT, routeForCartCheckout);
    expectValue(BAGPAGE_CONSTANTS.START_BAG_CHECKOUT, startCartCheckout);
    expectValue(BAGPAGE_CONSTANTS.START_PAYPAL_CHECKOUT, startPaypalCheckout);
    expectValue(BAGPAGE_CONSTANTS.START_APPLE_PAY_CHECKOUT, startApplePayCheckout);
    expectValue(BAGPAGE_CONSTANTS.AUTHORIZATION_PAYPAL_CHECKOUT, authorizePayPalPayment);
    expectValue(BAGPAGE_CONSTANTS.GET_SFL_DATA, getSflDataSaga);
    expectValue(BAGPAGE_CONSTANTS.SFL_ITEMS_DELETE, startSflItemDelete);
    expectValue(BAGPAGE_CONSTANTS.SFL_ITEMS_MOVE_TO_BAG, startSflItemMoveToBag);
    expectValue(BAGPAGE_CONSTANTS.START_PAYPAL_NATIVE_CHECKOUT, startPaypalNativeCheckout);
    expectValue(BAGPAGE_CONSTANTS.SET_SFL_DATA, setModifiedSflData);
    expectValue(BAGPAGE_CONSTANTS.UPDATE_SFL_ITEM, setSflItemUpdate);
  });
});

describe('Bag page Saga', () => {
  it('BagPageSaga', () => {
    const generator = BagPageSaga();

    let takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(BAGPAGE_CONSTANTS.GET_ORDER_DETAILS, getOrderDetailSaga)
    );

    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(BAGPAGE_CONSTANTS.GET_CART_DATA, getCartDataSaga)
    );
  });
});

describe('removeUnqualifiedItemsAndCheckout Saga', () => {
  it('removeUnqualifiedItemsAndCheckout effect', () => {
    const generator = removeUnqualifiedItemsAndCheckout();
    let takeLatestDescriptor = generator.next();
    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(select(BAG_SELECTORS.getUnqualifiedItemsIds));
    takeLatestDescriptor = generator.next();
    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(call(checkoutCart, true, undefined, undefined, undefined));
    generator.next();
  });

  it('removeUnqualifiedItemsAndCheckout effect with unqualifiedItemsIds', () => {
    const generator = removeUnqualifiedItemsAndCheckout();
    generator.next();

    generator.next();
    generator.next({ a: 1, b: 2, size: 3 });
    generator.next();
    generator.next();
  });
});

describe('startCartCheckout Saga with minibag', () => {
  it('startCartCheckout effect', () => {
    const input = {
      payload: {
        isMiniBag: true,
        isAddedToBag: true,
      },
    };
    const generator = startCartCheckout(input);
    let takeLatestDescriptor = generator.next().value;

    generator.next();
    generator.next();
    generator.next();
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next(false, {}).value;
    generator.next();
    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      call(checkoutCart, false, undefined, undefined, undefined, true)
    );
    generator.next();
    generator.next();
    generator.next();
  });

  it('startCartCheckout effect', () => {
    const generator = startCartCheckout({});
    let takeLatestDescriptor = generator.next().value;

    generator.next();
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next(false, {}).value;
    generator.next();
    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      call(checkoutCart, false, undefined, undefined, undefined, undefined)
    );
    generator.next();
    generator.next();
    generator.next();
  });

  it('startCartCheckout effect isEditingItem', () => {
    const input = {
      payload: {
        isEditingItem: true,
      },
    };
    const generator = startCartCheckout(input);
    generator.next();

    generator.next();
    generator.next();
    generator.next();
    generator.next();
  });
});

describe('fetchModuleX Saga', () => {
  it('check fetchModuleX', () => {
    const generator = fetchModuleX({ payload: ['123'] });

    let takeLatestDescriptor = generator.next([{}]).value;
    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(null);
  });
});

describe('renderBagPageCheckoutLoader', () => {
  it('check renderBagPageCheckoutLoader', () => {
    const generator = renderBagPageCheckoutLoader(false, false);
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(true);
  });

  it('check renderBagPageCheckoutLoader for minibag', () => {
    const generator = renderBagPageCheckoutLoader(false, true);
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(true);
  });
});

describe('renderMobileLoader', () => {
  it('check renderMobileLoader', () => {
    isMobileApp.mockImplementation(() => true);
    const generator = renderMobileLoader();
    generator.next();
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(true);
  });

  it('check renderMobileLoader for desktop', () => {
    isMobileApp.mockImplementation(() => false);
    const generator = renderMobileLoader();
    generator.next();
  });
});

describe('mergeTwoObjects', () => {
  it('check mergeTwoObjects', () => {
    const obj1 = [{ name: '1' }, { name: '2' }];
    const obj2 = [{ name: '2' }, { name: '3' }];
    const merger = mergeTwoObjects(obj1, obj2);
    expect(merger).toEqual([{ name: '2' }, { name: '3' }, { name: '1' }]);
  });
});

describe('setBagPageLoading', () => {
  it('check setBagPageLoading', () => {
    const generator = setBagPageLoading(false, false);
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(true);
  });

  it('check setBagPageLoading fromMoveTobag ', () => {
    const generator = setBagPageLoading(true, false);
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(true);
  });
});

describe('handelAddressIds', () => {
  it('check handelAddressIds with previousId zero', () => {
    const orderDetails = {
      checkout: {
        shipping: {
          onFileAddressId: 123,
        },
      },
    };
    const addressIds = {
      previousId: 0,
      currentId: 1,
    };
    const generator = handelAddressIds(orderDetails, addressIds);
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(true);
  });

  it('check handelAddressIds with previousId', () => {
    const orderDetails = {
      checkout: {
        shipping: {
          onFileAddressId: 123,
        },
      },
    };
    const addressIds = {
      previousId: 2,
      currentId: 3,
    };
    const generator = handelAddressIds(orderDetails, addressIds);
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(true);
  });

  it('check handelAddressIds with currentId equal to onFileAddressId', () => {
    const orderDetails = {
      checkout: {
        shipping: {
          onFileAddressId: 123,
        },
      },
    };
    const addressIds = {
      previousId: 2,
      currentId: 123,
    };
    const generator = handelAddressIds(orderDetails, addressIds);
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(true);
  });

  it('check handelAddressIds without shipping', () => {
    const orderDetails = {
      checkout: {},
    };
    const addressIds = {
      previousId: 2,
      currentId: 123,
    };
    const generator = handelAddressIds(orderDetails, addressIds);
    generator.next();
  });
});

describe('setBagpageCarousel', () => {
  it('check setBagpageCarousel', () => {
    const generator = setBagpageCarousel(true);
    generator.next();

    const takeLatestDescriptor = generator.next(true);
    expect(takeLatestDescriptor.done).toEqual(false);
  });

  it('check setBagpageCarousel when isBagCarouselEnabled is false', () => {
    const generator = setBagpageCarousel(true);
    generator.next();

    const takeLatestDescriptor = generator.next(false);
    expect(takeLatestDescriptor.done).toEqual(true);
  });
});

describe('shouldTranslate', () => {
  it('check shouldTranslate', () => {
    const generator = shouldTranslate(true);
    generator.next();

    const takeLatestDescriptor = generator.next('EU');
    expect(takeLatestDescriptor.done).toEqual(true);
  });
});

describe('updateBopisItems', () => {
  const res = {
    orderDetails: {
      orderItems: [
        {
          productInfo: { orderType: 'BOPIS', variantNo: 123, itemPartNumber: 123 },
          itemInfo: {},
          miscInfo: { store: {}, storeId: '123' },
        },
      ],
    },
  };
  it('check updateBopisItems for cartpage', () => {
    const generator = updateBopisItems(res, true);
    generator.next();

    generator.next();
  });

  it('check updateBopisItems for non cart page', () => {
    const generator = updateBopisItems(res, false);
    generator.next();

    generator.next();
  });
});

describe('checkoutCart Saga', () => {
  it('check checkoutCart', () => {
    const generator = checkoutCart();

    let takeLatestDescriptor = generator.next(true).value;
    takeLatestDescriptor = generator.next(false).value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(put(setCheckoutModalMountedState({ state: true })));
  });

  it('check checkoutCart with logged in user', () => {
    const generator = checkoutCart();
    let takeLatestDescriptor = generator.next(true).value;
    takeLatestDescriptor = generator.next(true).value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next(false).value;
    expect(takeLatestDescriptor).toEqual(
      call(routeForCartCheckout, {
        closeModal: undefined,
        navigation: undefined,
        navigationActions: undefined,
        recalc: undefined,
      })
    );
  });
});

describe('routeForCartCheckout Saga', () => {
  it('check routeForCartCheckout', () => {
    isMobileApp.mockImplementation(() => true);
    const generator = checkoutCart();
    generator.next(true);
    generator.next(false);
    generator.next(true);
    expect(isMobileApp()).toEqual(true);
  });

  it('should call getUserInfoSaga', () => {
    const generator = routeForCartCheckout({});
    generator.next();
    generator.next();
    generator.next(false);
    const nextValue = generator.next(false).value;
    expect(nextValue).toEqual(call(getUserInfoSaga));
  });

  it('should not call getUserInfoSaga', () => {
    const generator = routeForCartCheckout({});
    generator.next();
    generator.next();
    generator.next(true);
    const nextValue = generator.next(true).value;
    expect(nextValue).toEqual(put(setLoaderState(false)));
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next();
  });

  it('should call getUserInfoSaga for desktop', () => {
    isMobileApp.mockImplementation(() => false);
    const generator = routeForCartCheckout({});
    generator.next();
    generator.next();
    generator.next(false);
    const nextValue = generator.next(false).value;
    expect(nextValue).toEqual(call(getUserInfoSaga));
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next();
    isMobileApp.mockImplementation(() => true);
  });

  it('should call getUserInfoSaga for desktop and non InternationalShipping', () => {
    isMobileApp.mockImplementation(() => false);
    const generator = routeForCartCheckout({});
    generator.next();
    generator.next();
    generator.next(false);
    const nextValue = generator.next(false).value;
    expect(nextValue).toEqual(call(getUserInfoSaga));
    generator.next();
    generator.next(true);
    generator.next(true);
    generator.next(true);
    generator.next();
    generator.next();
    generator.next();
    isMobileApp.mockImplementation(() => true);
  });
});

describe('setSflItemUpdate Saga', () => {
  it('setSflItemUpdate', () => {
    const sflItemsData = {
      productInfo: {},
      itemInfo: {},
      miscInfo: {},
    };
    const res = {
      sflItems: sflItemsData,
    };
    const generator = setSflItemUpdate({
      payload: { oldSkuId: '', newSkuId: '', callBack: () => {} },
    });

    let takeLatestDescriptor = generator.next(true).value;
    takeLatestDescriptor = generator.next(true).value;
    takeLatestDescriptor = generator.next(false).value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next(res).value;
    expect(takeLatestDescriptor).toEqual(put(BAG_PAGE_ACTIONS.setSflData(sflItemsData)));
  });

  it('setSflItemUpdate without callback', () => {
    const generator = setSflItemUpdate({
      payload: { oldSkuId: '', newSkuId: '' },
    });

    generator.next(true);
    generator.next(true);
    generator.next(false);
    generator.next();
    generator.next();
    generator.next();
  });
});

describe('Bag SFL Saga', () => {
  it('add item to sfl', () => {
    const res = {
      errorResponse: null,
    };
    isCanada.mockImplementation(() => false);
    const generator = addItemToSFL({
      payload: {
        afterHandler: () => {},
        afterSuccessCallback: () => {},
      },
    });
    let takeLatestDescriptor = generator.next().value;

    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next(res).value;
    takeLatestDescriptor = generator.next(res).value;
    takeLatestDescriptor = generator.next(res).value;
    expect(takeLatestDescriptor).toEqual(put(BAG_PAGE_ACTIONS.setCartItemsSFL(true)));
    generator.next(res);
    generator.next(res);
    generator.next(res);
  });

  it('add item to sfl from minibag', () => {
    const res = {
      errorResponse: true,
      errorMessage: 'error',
    };
    isCanada.mockImplementation(() => false);
    const generator = addItemToSFL({
      payload: { isMiniBag: true, afterHandler: () => {}, afterSuccessCallback: () => {} },
    });
    generator.next();

    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
  });

  it('add item to sfl from minibag without callbacks', () => {
    const res = {
      errorResponse: true,
      errorMessage: 'error',
    };
    isCanada.mockImplementation(() => false);
    const generator = addItemToSFL({
      payload: { isMiniBag: true },
    });
    generator.next();

    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
  });

  it('should throw error from add item to sfl from minibag', () => {
    const res = {
      errorResponse: true,
      errorMessage: 'error',
    };
    isCanada.mockImplementation(() => false);
    const generator = addItemToSFL({
      payload: { isMiniBag: true, afterHandler: true, afterSuccessCallback: true },
    });
    generator.next();

    generator.next();
    generator.next();
    generator.next();
    generator.next();
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
    generator.next(res);
  });

  it('get all sfl data', () => {
    const sflItemsData = {
      productInfo: {},
      itemInfo: {},
      miscInfo: {},
    };
    const res = {
      sflItems: sflItemsData,
      cartSflMerged: true,
    };
    isCanada.mockImplementation(() => false);
    const generator = getSflDataSaga({});
    let takeLatestDescriptor = generator.next().value;

    takeLatestDescriptor = generator.next(res).value;
    takeLatestDescriptor = generator.next(res).value;
    expect(takeLatestDescriptor).toEqual(put(BAG_PAGE_ACTIONS.setSflData(sflItemsData)));
    generator.next();
    generator.next();
    generator.next();
  });

  it('get all sfl data when cartSflMerged is false', () => {
    const sflItemsData = {
      productInfo: {},
      itemInfo: {},
      miscInfo: {},
    };
    const res = {
      sflItems: sflItemsData,
    };
    isCanada.mockImplementation(() => false);
    const generator = getSflDataSaga({});
    let takeLatestDescriptor = generator.next().value;

    takeLatestDescriptor = generator.next(res).value;
    takeLatestDescriptor = generator.next(res).value;
    expect(takeLatestDescriptor).toEqual(put(BAG_PAGE_ACTIONS.setSflData(sflItemsData)));
    generator.next();
    generator.next();
    generator.next();
  });

  it('should throw error from getSflDataSaga', () => {
    const generator = getSflDataSaga({});
    generator.next();

    generator.next();
    generator.next();
    generator.next();
    generator.next();
  });

  it('DeleteSFL ITEM', () => {
    const sflItemsData = {
      productInfo: {},
      itemInfo: {},
      miscInfo: {},
    };
    const res = {
      sflItems: sflItemsData,
    };
    const generator = startSflItemDelete({});
    isCanada.mockImplementation(() => false);
    let takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next().value;
    takeLatestDescriptor = generator.next(res).value;
    takeLatestDescriptor = generator.next(res).value;
    takeLatestDescriptor = generator.next(res).value;
    expect(takeLatestDescriptor).toEqual(put(BAG_PAGE_ACTIONS.setSflData(sflItemsData)));
  });
});
