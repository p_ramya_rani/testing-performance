// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import PropTypes from 'prop-types';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf/RenderPerf';
import { PAGE_NAVIGATION_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import { Grid, Banner404 } from '@tcp/core/src/components/common/molecules';
import {
  Row,
  Col,
  DLPSkeleton,
  RichText,
  DynamicModule,
} from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import {
  getProductListingPageTrackData,
  getIcidValue,
  getCategoryName,
  internalCampaignProductAnalyticsList,
  fireEnhancedEcomm,
  isTCP,
  getAPIConfig,
} from '@tcp/core/src/utils';
import { productArr } from '@tcp/core/src/analytics/utils';
import { isTier1Device, isTier3Device } from '@tcp/web/src/utils/device-tiering';
import style, { customBreadCrumbStyle, seoTextStyle } from '../styles/CategoryListing.style';
import GlobalNavigationMenuDesktopL2 from '../../../ProductListing/molecules/GlobalNavigationMenuDesktopL2/views';
import FixedBreadCrumbs from '../../../ProductListing/molecules/FixedBreadCrumbs/views';
import ReadMore from '../../../ProductListing/molecules/ReadMore/views';
import SpotlightContainer from '../../../ProductListing/molecules/Spotlight/index.async';
import PromoModules from '../../../../../common/organisms/PromoModules';

class CategoryListing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageAnalyticsTriggered: false,
    };
    this.apiConfig = getAPIConfig();
  }

  componentDidUpdate(prevProps) {
    const { breadCrumbTrail, trackPageLoad, asPath, storeId, currency } = this.props;
    const { pageAnalyticsTriggered } = this.state;

    if (prevProps.breadCrumbTrail !== breadCrumbTrail || !pageAnalyticsTriggered) {
      const icid = getIcidValue(asPath);
      const products = internalCampaignProductAnalyticsList();
      const formattedProducts = products && productArr(products);
      fireEnhancedEcomm({
        eventName: 'internalcampaignimpression',
        productsObj: formattedProducts,
        eventType: 'impressions',
        pageName: 'homepage',
        currCode: currency,
      });
      trackPageLoad(getProductListingPageTrackData(breadCrumbTrail, {}, '', {}, icid, storeId));
      this.setPageLoadState();
    }
  }

  setPageLoadState = () => {
    this.setState({
      pageAnalyticsTriggered: true,
    });
  };

  show404Banner = (is404AbTest, mon404, labels) => {
    let pageName = 'categoryGYM';
    if (isTCP()) pageName = 'categoryTCP';
    if (is404AbTest && mon404) return <Banner404 pageName={pageName} labels={labels} />;
    return null;
  };

  getQueryVariable = (asPath, searchKey) => {
    const query = asPath && asPath.split('?') && asPath.split('?')[1];
    const vars = query && query.split('&');
    if (vars) {
      for (let i = 0; i < vars.length; i += 1) {
        const pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) === searchKey) {
          return decodeURIComponent(pair[1]);
        }
      }
    }
    return null;
  };

  render() {
    const {
      className,
      navTree,
      currentNavIds,
      breadCrumbs,
      categoryPromoModules,
      seoText,
      labels,
      categoryId,
      plpTopPromos,
      isLoggedIn,
      isPlcc,
      accessibilityLabels,
      isLoadingMore,
      is404AbTest,
      banner404Labels,
      asPath,
      seoHeaderCopyText,
      isSeoHeaderEnabled,
      dynamicPromoModules,
      dynamicModules: modules,
    } = this.props;
    const recommendationAttributes = {
      variations: 'moduleO',
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.DEPARTMENT_LANDING,
      showLoyaltyPromotionMessage: false,
      headerAlignment: 'left',
    };

    const mon404 = this.getQueryVariable(asPath, 'mon404');
    const categoryName = getCategoryName(breadCrumbs);
    const isBVSpotlightDisplay = !isTier3Device();
    const willAlsoViewedCarouselDisplay = isTier1Device();
    const { isCandidEnabled } = this.apiConfig;

    return (
      <div className={className}>
        <Grid className={className}>
          {isLoadingMore ? <DLPSkeleton col={4} /> : null}
          <Row>
            <Col colSize={{ large: 12, medium: 8, small: 6 }}>
              {this.show404Banner(is404AbTest, mon404, banner404Labels)}
            </Col>
          </Row>
          <Row fullBleed>
            <Col className="bread-crumb-container" colSize={{ large: 12, medium: 8, small: 6 }}>
              {breadCrumbs.length === 1 ? (
                <FixedBreadCrumbs inheritedStyles={customBreadCrumbStyle} crumbs={breadCrumbs} />
              ) : null}
            </Col>
            {isSeoHeaderEnabled && (
              <Col colSize={{ large: 12, medium: 8, small: 6 }}>
                <RichText className="seo-header-text-block" richTextHtml={seoHeaderCopyText} />
              </Col>
            )}

            <Col className="clp-promo-container" colSize={{ large: 12, medium: 8, small: 6 }}>
              {plpTopPromos.length > 0 && (
                <PromoModules
                  plpTopPromos={plpTopPromos}
                  isLoggedIn={isLoggedIn}
                  isPlcc={isPlcc}
                  accessibilityLabels={accessibilityLabels}
                  viaModule={categoryName}
                  dynamicPromoModules={dynamicPromoModules}
                />
              )}
            </Col>
          </Row>
          <Row>
            <Col
              colSize={{ large: 2, medium: 0, small: 0 }}
              hideCol={{ small: true, medium: true }}
            >
              <div className="sidebar">
                <GlobalNavigationMenuDesktopL2
                  navigationTree={navTree}
                  activeCategoryIds={currentNavIds}
                />
                {/* UX timer */}
                <RenderPerf.Measure name={PAGE_NAVIGATION_VISIBLE} />
              </div>
            </Col>
            <Col colSize={{ large: 10, medium: 8, small: 6 }}>
              <Row fullBleed>
                <Col
                  colSize={{ large: 12, medium: 8, small: 6 }}
                  ignoreGutter={{ small: true, medium: true, large: true }}
                >
                  {categoryPromoModules &&
                    categoryPromoModules.map((contentModule) => {
                      const { moduleName = 'categoryPromo' } = contentModule;
                      const Module = modules[moduleName];
                      if (moduleName === 'moduleX') {
                        return <Module richTextList={contentModule.richTextList} />;
                      }
                      if (moduleName === 'outfitCarousel') {
                        return <Module {...contentModule} />;
                      }
                      return <Module categoryPromoImages={contentModule} />;
                    })}
                </Col>

                <Col className="section-wrapper" colSize={{ small: 6, medium: 8, large: 12 }}>
                  <ReadMore
                    description={seoText}
                    labels={labels}
                    className={`${className} seo-text`}
                    inheritedStyles={seoTextStyle}
                  />
                </Col>
                <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                  <div className="product-detail-section">
                    <Recommendations
                      headerLabel={labels.recentlyViewed}
                      {...recommendationAttributes}
                      sequence="1"
                    />
                  </div>
                </Col>
                {willAlsoViewedCarouselDisplay && (
                  <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                    <div className="product-detail-section">
                      <Recommendations
                        headerLabel={labels.recentlyViewed}
                        portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
                        {...recommendationAttributes}
                        sequence="2"
                      />
                    </div>
                  </Col>
                )}
                {isCandidEnabled && (
                  <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                    <div className="get-candid-section">
                      <DynamicModule
                        key="get-candid"
                        importCallback={() =>
                          import(
                            /* webpackChunkName: "get-candid" */ '@tcp/core/src/components/common/molecules/GetCandid'
                          )
                        }
                      />
                    </div>
                  </Col>
                )}
                {isBVSpotlightDisplay && (
                  <Col
                    className="clp-spotlight-container"
                    colSize={{ small: 6, medium: 8, large: 12 }}
                  >
                    {categoryId ? <SpotlightContainer categoryId={categoryId} /> : null}
                  </Col>
                )}
              </Row>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

CategoryListing.propTypes = {
  className: PropTypes.string.isRequired,
  navTree: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  categoryPromoModules: PropTypes.shape({}),
  breadCrumbs: PropTypes.shape([]),
  breadCrumbTrail: PropTypes.shape([]),
  seoText: PropTypes.string.isRequired,
  currentNavIds: PropTypes.arrayOf(PropTypes.string),
  categoryId: PropTypes.string,
  trackPageLoad: PropTypes.func,
  plpTopPromos: PropTypes.shape({}),
  isLoggedIn: PropTypes.bool,
  isPlcc: PropTypes.bool,
  accessibilityLabels: PropTypes.shape({}),
  isLoadingMore: PropTypes.bool,
  asPath: PropTypes.string,
  storeId: PropTypes.string,
  currency: PropTypes.string,
  is404AbTest: PropTypes.bool,
  banner404Labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  seoHeaderCopyText: PropTypes.string,
  isSeoHeaderEnabled: PropTypes.bool,
  dynamicPromoModules: PropTypes.instanceOf(Object).isRequired,
};

CategoryListing.defaultProps = {
  navTree: {},
  categoryPromoModules: {},
  labels: {},
  currentNavIds: [],
  breadCrumbs: [],
  breadCrumbTrail: [],
  categoryId: '',
  trackPageLoad: () => {},
  plpTopPromos: {},
  isLoggedIn: false,
  isPlcc: false,
  accessibilityLabels: {},
  isLoadingMore: false,
  asPath: null,
  storeId: '',
  currency: 'USD',
  is404AbTest: false,
  banner404Labels: {},
  seoHeaderCopyText: '',
  isSeoHeaderEnabled: false,
};

export default withStyles(CategoryListing, style);
export { CategoryListing as CategoryListingVanilla };
