/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { fetchPageLayout, loadLayoutData } from '@tcp/core/src/reduxStore/actions';
import withIsomorphicRenderer from '@tcp/core/src/components/common/hoc/withIsomorphicRenderer';
import { getDefaultStore } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getFavoriteStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { getAPIConfig, getUrlParameter, routerPush } from '@tcp/core/src/utils';
import { getCurrentCurrency } from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import CategoryListing from './views/CategoryListing';
import { getCategoryIds, getImagesGrids, getCategoryName } from '../utils/utility';
import {
  getLabels,
  getBreadCrumbTrail,
  getIs404AbTest,
  get404Labels,
} from './CategoryListing.selectors';
import { trackPageView, setClickAnalyticsData } from '../../../../../analytics/actions';
import { openOverlayModal } from '../../../account/OverlayModal/container/OverlayModal.actions';

export class CategoryListingContainer extends PureComponent {
  static getInitialProps = async ({ props }) => {
    const { getLayout, router: { asPath } = {}, apiConfig } = props;
    await getLayout(getCategoryName(asPath), apiConfig);
  };

  componentDidMount() {
    const { router } = this.props;
    router.events.on('routeChangeComplete', this.handleRouteChange);
  }

  componentDidUpdate(prevProps) {
    const { getLayout, router: { asPath } = {}, clearPromoDataOnRouteChange, router } = this.props;
    const apiConfig = getAPIConfig();
    const { router: { asPath: prevAsPath } = {} } = prevProps;
    if (getCategoryName(prevAsPath) !== getCategoryName(asPath)) {
      getLayout(getCategoryName(asPath), apiConfig);
    }
    // Clears layout on route change to make sure promo data is not stale when jumping between different PLPs.
    if (prevProps.router.asPath !== '' && router.asPath !== prevProps.router.asPath) {
      clearPromoDataOnRouteChange();
    }
  }

  componentWillUnmount() {
    const { router } = this.props;
    router.events.off('routeChangeComplete', this.handleRouteChange);
  }

  handleRouteChange = () => {
    const { openOverlay, isLoggedIn } = this.props;
    const openRegister = getUrlParameter('openRegister');
    const redirectOnSuccess = getUrlParameter('redirectOnSuccess');

    if (!isLoggedIn && openRegister && openOverlay) {
      openOverlay({
        component: 'createAccount',
        componentProps: 'create-account',
      });
    } else if (isLoggedIn && redirectOnSuccess) {
      routerPush('/account?id=profile', redirectOnSuccess);
    }
  };

  render() {
    const { layouts, Modules, isLoadingMore, seoText, router: { asPath } = {} } = this.props;

    const categoryName =
      getCategoryName(asPath) &&
      getCategoryName(asPath).replace(/-([a-z])/g, (g) => {
        return g && g[1] && g[1].toUpperCase();
      });

    const categoryListingSlots = (layouts[categoryName] && layouts[categoryName].slots) || [];

    const categoryIds = getCategoryIds(categoryListingSlots);
    const categoryPromoModules = getImagesGrids(categoryIds, Modules);
    return (
      <Fragment>
        <CategoryListing
          {...this.props}
          seoText={seoText}
          categoryPromoModules={categoryPromoModules}
          isLoadingMore={isLoadingMore}
          asPath={asPath}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    deviceType: state.DeviceInfo && state.DeviceInfo.deviceType,
    layouts: state.Layouts || {},
    Modules: state.Modules || {},
    navigationData: (state.Navigation && state.Navigation.navigationData) || [],
    labels: getLabels(state),
    breadCrumbTrail: getBreadCrumbTrail(state),
    storeId: getDefaultStore(state) || getFavoriteStore(state),
    currency: getCurrentCurrency(state),
    is404AbTest: getIs404AbTest(state),
    banner404Labels: get404Labels(state),
  };
};

export const mapDispatchToProps = (dispatch) => ({
  openOverlay: (payload) => {
    dispatch(openOverlayModal(payload));
  },
  getLayout: (pageName, apiConfig) =>
    dispatch(fetchPageLayout({ pageName, layoutName: null, isClpPage: true, apiConfig })),
  trackPageLoad: (payload) => {
    const {
      products,
      customEvents,
      departmentList,
      categoryList,
      listingSubCategory,
      internalCampaignId,
      ...others
    } = payload;
    dispatch(
      setClickAnalyticsData({
        products,
        customEvents,
        departmentList,
        categoryList,
        listingSubCategory,
        internalCampaignId,
      })
    );
    const dispatchTimer = setTimeout(() => {
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...others,
                },
              },
            },
          },
        })
      );

      const analyticsTimer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(analyticsTimer);
      }, 200);
      clearTimeout(dispatchTimer);
    }, 100);
  },
  clearPromoDataOnRouteChange: () => {
    dispatch(loadLayoutData({}, 'productListingPage'));
  },
});

CategoryListingContainer.propTypes = {
  categoryListingSlots: PropTypes.shape([]).isRequired,
  Modules: PropTypes.shape({}).isRequired,
  router: PropTypes.shape({}).isRequired,
  layouts: PropTypes.shape({}).isRequired,
  navigationData: PropTypes.shape([]).isRequired,
  getLayout: PropTypes.func.isRequired,
  isLoadingMore: PropTypes.bool.isRequired,
  openOverlay: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  clearPromoDataOnRouteChange: PropTypes.func.isRequired,
  seoText: PropTypes.string,
};

CategoryListingContainer.defaultProps = {
  isLoggedIn: false,
};

export default withIsomorphicRenderer({
  WrappedComponent: CategoryListingContainer,
  mapStateToProps,
  mapDispatchToProps,
});
