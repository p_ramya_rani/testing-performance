// 9fbef606107a605d69c0edbcd8029e5d 
import { shallow } from 'enzyme';
import React from 'react';
import { CategoryListingVanilla } from '../views/CategoryListing';

describe('StoreSearch Container', () => {
  let wrapper;
  const props = {
    getLayout: jest.fn(),
    className: '',
    categoryPromoModules: [],
  };

  let tree = '';
  beforeEach(() => {
    tree = shallow(<CategoryListingVanilla {...props} />);
  });

  test('should render CLP Correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should render seoheadercopy text', () => {
    const seoHeaderProps = {
      ...props,
      isSeoHeaderEnabled: true,
    };
    wrapper = shallow(<CategoryListingVanilla {...seoHeaderProps} />);
    expect(wrapper).toMatchSnapshot();
  });
  it('should not render seoheadercopy text', () => {
    const seoHeaderProps = {
      ...props,
      isSeoHeaderEnabled: false,
    };
    wrapper = shallow(<CategoryListingVanilla {...seoHeaderProps} />);
    expect(wrapper).toMatchSnapshot();
  });
});
