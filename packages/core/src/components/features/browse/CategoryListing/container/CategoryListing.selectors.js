// 9fbef606107a605d69c0edbcd8029e5d 
import { getLabelValue, getABtestFromState, parseBoolean } from '../../../../../utils';

// eslint-disable-next-line import/prefer-default-export
export const getLabels = state => {
  return {
    readMore: getLabelValue(state.Labels, 'lbl_read_more', 'seoText', 'PLP'),
    readLess: getLabelValue(state.Labels, 'lbl_read_less', 'seoText', 'PLP'),
    youMayAlsoLike: getLabelValue(state.Labels, 'lbl_you_may_also_like', 'CLPs_Sub', 'CLPs'),
    recentlyViewed: getLabelValue(state.Labels, 'lbl_recently_viewed', 'CLPs_Sub', 'CLPs'),
  };
};

export const getBreadCrumbTrail = state => {
  const { ProductListing } = state;
  return ProductListing.breadCrumbTrail;
};

export const getIs404AbTest = state => {
  return (
    getABtestFromState(state.AbTest) &&
    getABtestFromState(state.AbTest).abTest404Experience &&
    parseBoolean(getABtestFromState(state.AbTest).abTest404Experience[0].is404AbTest)
  );
};

export const get404Labels = state => {
  return {
    header404GYM_CLP: getLabelValue(state.Labels, 'lbl_404_heading_gym', 'CLPs_Sub', 'CLPs'),
    subHeader404GYM_CLP: getLabelValue(state.Labels, 'lbl_404_sub_header_gym', 'CLPs_Sub', 'CLPs'),
    header404TCP_CLP: getLabelValue(state.Labels, 'lbl_404_heading_tcp', 'CLPs_Sub', 'CLPs'),
    subHeader404TCP_CLP: getLabelValue(state.Labels, 'lbl_404_sub_header_tcp', 'CLPs_Sub', 'CLPs'),
  };
};
