// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import useImageLoadedState from '@tcp/web/src/hooks/useImageLoadedState';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { HERO_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import { BodyCopy, Anchor, DamImage } from '../../../../../../common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/CategoryPromoImages.style';
import config from '../config';
import { configureInternalNavigationFromCMSUrl } from '../../../../../../../utils';

const GetDamImageComp = ({ video, imgRef, imgDataConfig, image, link }) => {
  if (video) {
    return (
      <DamImage
        videoData={video}
        forwardedRef={imgRef}
        imgConfigs={imgDataConfig}
        imgData={image}
        link={{
          ...link,
        }}
      />
    );
  }
  return (
    <DamImage
      videoData={video}
      forwardedRef={imgRef}
      imgConfigs={imgDataConfig}
      imgData={image}
      link={{
        ...link,
      }}
    />
  );
};

const ImageComponent = ({
  image,
  link,
  className,
  titleClass,
  linkClass,
  imgDataConfig,
  imgRef,
  video,
}) => {
  return (
    <div className={`${className} image-tile-desktop`}>
      {GetDamImageComp({ video, imgRef, imgDataConfig, image, link })}
      <div>
        <BodyCopy
          fontSize={['fs14', 'fs24', 'fs24']}
          fontWeight="semibold"
          fontFamily="secondary"
          color="gray.900"
          textAlign="center"
          className={titleClass}
        >
          {link.title}
        </BodyCopy>
        <Anchor
          to={configureInternalNavigationFromCMSUrl(link.url)}
          asPath={link.url}
          className={linkClass}
        >
          <BodyCopy
            fontSize={['fs12', 'fs22', 'fs22']}
            fontFamily="secondary"
            color="gray.900"
            textAlign="center"
          >
            {link.text ? `${link.text} > ` : ''}
          </BodyCopy>
        </Anchor>
      </div>
    </div>
  );
};

/**
 * Specialized variation to use for the category promo image that needs
 * a UX timer for performance tracking
 */
function HeroImageComponent(props) {
  const imgRef = useRef();
  const imageLoaded = useImageLoadedState(imgRef);
  return (
    <>
      <ImageComponent {...props} imgRef={imgRef} />
      {imageLoaded && <RenderPerf.Measure name={HERO_VISIBLE} />}
    </>
  );
}

// eslint-disable-next-line sonarjs/cognitive-complexity
const CategoryPromoImages = ({ className, categoryPromoImages }) => {
  const combinedCategoryImages = [];
  const tabMobileCombinedCategoryImages = [];
  if (Object.keys(categoryPromoImages).length) {
    Object.keys(categoryPromoImages).forEach(contentId => {
      tabMobileCombinedCategoryImages.push(
        categoryPromoImages[contentId].imageGrid.map(imageGrid => {
          const { image, link, video } = imageGrid;
          const isSingleImage = categoryPromoImages[contentId].images === '1';
          return (
            <ImageComponent
              video={video}
              titleClass={isSingleImage ? 'image-title-fullBleed' : 'image-title-2-up'}
              linkClass={isSingleImage ? 'image-link-fullBleed' : 'image-link-2-up'}
              className={isSingleImage ? 'image-tile-full-bleed' : 'image-tile-desktop-2-up'}
              image={image}
              key={image}
              link={link}
              imgDataConfig={
                isSingleImage ? config.IMG_DATA_1.imgConfig : config.IMG_DATA_2.imgConfig
              }
            />
          );
        })
      );

      if (categoryPromoImages[contentId].images === '4') {
        combinedCategoryImages.push(
          <div className="image-variant-wrapper">
            {categoryPromoImages[contentId].imageGrid.map(imageGrid => {
              const { image, link, video } = imageGrid;
              return (
                <ImageComponent
                  video={video}
                  key={`${link.text}`}
                  image={image}
                  link={link}
                  titleClass="image-title-4-up"
                  linkClass="image-link-4-up"
                  className="image-tile-desktop-4-up"
                  imgDataConfig={config.IMG_DATA_4.imgConfig}
                />
              );
            })}
          </div>
        );
      } else if (categoryPromoImages[contentId].images === '6') {
        combinedCategoryImages.push(
          <div className="image-variant-wrapper">
            {categoryPromoImages[contentId].imageGrid.map(imageGrid => {
              const { image, link, video } = imageGrid;
              return (
                <ImageComponent
                  video={video}
                  titleClass="image-title-3-up"
                  linkClass="image-link-3-up"
                  image={image}
                  link={link}
                  className="image-tile-desktop-3-up"
                  imgDataConfig={config.IMG_DATA_6.imgConfig}
                />
              );
            })}
          </div>
        );
      } else if (categoryPromoImages[contentId].images === '2') {
        combinedCategoryImages.push(
          <div className="image-variant-wrapper">
            {categoryPromoImages[contentId].imageGrid.map(imageGrid => {
              const { image, link, video } = imageGrid;
              return (
                <ImageComponent
                  video={video}
                  titleClass="image-title-2-up"
                  linkClass="image-link-2-up"
                  className="image-tile-desktop-2-up"
                  image={image}
                  link={link}
                  imgDataConfig={config.IMG_DATA_2.imgConfig}
                />
              );
            })}
          </div>
        );
      } else if (categoryPromoImages[contentId].images === '1') {
        combinedCategoryImages.push(
          <div className="image-variant-wrapper">
            {categoryPromoImages[contentId].imageGrid.map(imageGrid => {
              const { image, link, video } = imageGrid;
              return (
                <HeroImageComponent
                  video={video}
                  titleClass="image-title-fullBleed"
                  linkClass="image-link-fullBleed"
                  className="image-tile-full-bleed"
                  image={image}
                  link={link}
                  imgDataConfig={config.IMG_DATA_1.imgConfig}
                />
              );
            })}
          </div>
        );
      }
    });
  }

  return (
    <div className={className}>
      <div className="image-variant-wrapper clp-desktop-view">{combinedCategoryImages}</div>
      <div className="image-variant-wrapper clp-mobile-tab-view">
        {tabMobileCombinedCategoryImages}
      </div>
    </div>
  );
};

GetDamImageComp.propTypes = {
  image: PropTypes.shape({}).isRequired,
  link: PropTypes.shape({}).isRequired,
  imgDataConfig: PropTypes.shape([]).isRequired,
  imgRef: PropTypes.shape({ current: PropTypes.any }),
  video: PropTypes.shape({}).isRequired,
};

GetDamImageComp.defaultProps = {
  imgRef: null,
};

CategoryPromoImages.propTypes = {
  className: PropTypes.string.isRequired,
  categoryPromoImages: PropTypes.string.isRequired,
};

ImageComponent.propTypes = {
  className: PropTypes.string.isRequired,
  titleClass: PropTypes.string.isRequired,
  linkClass: PropTypes.string.isRequired,
  image: PropTypes.shape({}).isRequired,
  link: PropTypes.shape({}).isRequired,
  imgDataConfig: PropTypes.shape([]).isRequired,
  imgRef: PropTypes.shape({ current: PropTypes.any }),
  video: PropTypes.shape({}).isRequired,
};

ImageComponent.defaultProps = {
  imgRef: null,
};

export default withStyles(CategoryPromoImages, styles);

export { CategoryPromoImages as CategoryPromoImagesVanilla };
