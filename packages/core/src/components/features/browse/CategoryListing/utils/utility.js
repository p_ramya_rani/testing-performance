// 9fbef606107a605d69c0edbcd8029e5d
export const getCategoryIds = categoryListingSlots => {
  const categoryPromos =
    categoryListingSlots.filter(slot => {
      return (
        slot.moduleName === 'categoryPromo' ||
        slot.moduleName === 'moduleX' ||
        slot.moduleName === 'outfitCarousel'
      );
    }) || [];

  return categoryPromos.map(categoryPromo => {
    return categoryPromo.contentId;
  });
};

export const getImagesGrids = (categoryIds, modules) => {
  const categoryPromoModules = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const categoryId in modules) {
    if (
      Object.prototype.hasOwnProperty.call(modules, categoryId) &&
      categoryIds.indexOf(categoryId) >= 0
    ) {
      if (modules[categoryId].moduleName === 'categoryPromo') {
        const categoryPromoObj = {};
        categoryPromoObj[categoryId] = modules[categoryId];
        categoryPromoModules.push(categoryPromoObj);
      } else categoryPromoModules.push(modules[categoryId]);
    }
  }
  return categoryPromoModules;
};

export const getSeoText = (listOfL1, cid) => {
  let description = '';
  for (let i = 0; i < listOfL1.length; i += 1) {
    if (listOfL1[i] && listOfL1[i].url && listOfL1[i].url.split('/c?cid=')[1] === cid) {
      description = listOfL1[i].categoryContent && listOfL1[i].categoryContent.longDescription;
    }
  }
  return description;
};

export const getCategoryName = asPath => {
  const categoryNames = asPath && asPath.split('/');
  const categoryName = (categoryNames && categoryNames[categoryNames.length - 1]) || '';
  return categoryName.split('?')[0];
};
