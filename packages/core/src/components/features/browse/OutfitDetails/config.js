// 9fbef606107a605d69c0edbcd8029e5d 
import { breakpoints } from '../../../../../styles/themes/TCP/mediaQuery';

const badgeTranformation = 'g_west,w_0.22,fl_relative';

export default {
  CAROUSEL_OPTIONS: {
    autoplay: false,
    arrows: true,
    fade: false,
    speed: 0,
    dots: true,
    dotsClass: 'slick-dots',
    swipe: true,
    slidesToShow: 1,
    infinite: false,
    responsive: [
      {
        breakpoint: breakpoints.values.lg - 1,
        settings: {
          arrows: true,
        },
      },
    ],
  },
};

export const imageData = {
  imgConfig: [`t_outfit_img_m`, `t_outfit_img_t`, `t_outfit_img_d`],
  badgeConfig: [badgeTranformation, badgeTranformation, badgeTranformation],
};

export const videoData = {
  imgConfig: ['e_loop,t_plp_webp_m', 'e_loop,t_plp_webp_t', 'e_loop,t_plp_webp_d'],
};

export const videoDataClient = {
  imgConfig: ['t_plp_vid_m', 't_plp_vid_t', 't_plp_vid_d'],
};
