// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import ProductDetailWrapper, {
  ProductRowLayout,
  ProductDetails,
  ImageWrapper,
  ProductSize,
  SizeList,
  SizeWrapper,
} from '../styles/OutfitProductSkeleton.style.native';

const OutfitProductSkeleton = () => {
  return (
    <>
      <ProductDetailWrapper>
        <ProductRowLayout>
          <ImageWrapper>
            <LoaderSkelton width="100%" height="170px" borderRadius="16px" />
          </ImageWrapper>
          <ProductDetails>
            <ViewWithSpacing spacingStyles="margin-bottom-SM">
              <LoaderSkelton width="75%" height="14px" borderRadius="16px" />
            </ViewWithSpacing>
            <ViewWithSpacing spacingStyles="margin-bottom-XS">
              <LoaderSkelton width="20%" height="15px" borderRadius="16px" />
            </ViewWithSpacing>
            <ViewWithSpacing spacingStyles="margin-bottom-XXS">
              <LoaderSkelton width="35%" height="12px" borderRadius="16px" />
            </ViewWithSpacing>
            <ViewWithSpacing spacingStyles="margin-bottom-MED">
              <LoaderSkelton width="37%" height="15px" borderRadius="16px" />
            </ViewWithSpacing>
            <ViewWithSpacing>
              <LoaderSkelton width="72%" height="14px" borderRadius="16px" />
            </ViewWithSpacing>
          </ProductDetails>
        </ProductRowLayout>
        <ProductSize>
          <ViewWithSpacing spacingStyles="margin-bottom-MED margin-left-MED margin-top-MED">
            <LoaderSkelton width="36%" height="14px" borderRadius="16px" />
          </ViewWithSpacing>
          <SizeList>
            <SizeWrapper spacingStyles="margin-left-XS">
              <LoaderSkelton width="82%" height="42px" borderRadius="16px" />
            </SizeWrapper>
            <SizeWrapper spacingStyles="margin-left-XS">
              <LoaderSkelton width="82%" height="42px" borderRadius="16px" />
            </SizeWrapper>
            <SizeWrapper spacingStyles="margin-left-XS">
              <LoaderSkelton width="82%" height="42px" borderRadius="16px" />
            </SizeWrapper>
            <SizeWrapper spacingStyles="margin-left-XS">
              <LoaderSkelton width="82%" height="42px" borderRadius="16px" />
            </SizeWrapper>
          </SizeList>
        </ProductSize>
      </ProductDetailWrapper>
      <ProductDetailWrapper>
        <ProductRowLayout>
          <ImageWrapper>
            <LoaderSkelton width="100%" height="170px" borderRadius="16px" />
          </ImageWrapper>
          <ProductDetails>
            <ViewWithSpacing spacingStyles="margin-bottom-SM">
              <LoaderSkelton width="75%" height="14px" borderRadius="16px" />
            </ViewWithSpacing>
            <ViewWithSpacing spacingStyles="margin-bottom-XS">
              <LoaderSkelton width="20%" height="15px" borderRadius="16px" />
            </ViewWithSpacing>
            <ViewWithSpacing spacingStyles="margin-bottom-XXS">
              <LoaderSkelton width="35%" height="12px" borderRadius="16px" />
            </ViewWithSpacing>
            <ViewWithSpacing spacingStyles="margin-bottom-MED">
              <LoaderSkelton width="37%" height="15px" borderRadius="16px" />
            </ViewWithSpacing>
            <ViewWithSpacing>
              <LoaderSkelton width="72%" height="14px" borderRadius="16px" />
            </ViewWithSpacing>
          </ProductDetails>
        </ProductRowLayout>
        <ProductSize>
          <ViewWithSpacing spacingStyles="margin-bottom-MED margin-left-MED margin-top-MED">
            <LoaderSkelton width="36%" height="14px" borderRadius="16px" />
          </ViewWithSpacing>
          <SizeList>
            <SizeWrapper spacingStyles="margin-left-XS">
              <LoaderSkelton width="82%" height="42px" borderRadius="16px" />
            </SizeWrapper>
            <SizeWrapper spacingStyles="margin-left-XS">
              <LoaderSkelton width="82%" height="42px" borderRadius="16px" />
            </SizeWrapper>
            <SizeWrapper spacingStyles="margin-left-XS">
              <LoaderSkelton width="82%" height="42px" borderRadius="16px" />
            </SizeWrapper>
            <SizeWrapper spacingStyles="margin-left-XS">
              <LoaderSkelton width="82%" height="42px" borderRadius="16px" />
            </SizeWrapper>
          </SizeList>
        </ProductSize>
      </ProductDetailWrapper>
    </>
  );
};
export default OutfitProductSkeleton;
