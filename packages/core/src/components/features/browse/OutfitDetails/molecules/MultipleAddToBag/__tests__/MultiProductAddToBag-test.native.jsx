// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { MultiProductAddToBag } from '../views/MultiProductAddToBag.native';

describe('MultiProductAddToBag component', () => {
  let component;
  const props = {
    AddItemsToBagLabel: 'AddToBag',
    selectedItemsCount: '6',
    handleMultipleAddToBag: () => {},
    addMultipleToBagEcom: () => {},
    selectedOutfitProducts: {},
    currentState: {},
    isSBP: false,
    setSelectedOutfitProduct: () => {},
  };

  beforeEach(() => {
    component = shallow(<MultiProductAddToBag {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
