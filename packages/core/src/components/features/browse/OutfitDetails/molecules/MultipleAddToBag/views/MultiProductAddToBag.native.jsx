import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import AddToBag from '../styles/MultiProductAddToBag.native.styles';

export const MultiProductAddToBag = (props) => {
  const {
    AddItemsToBagLabel,
    selectedItemsCount,
    handleMultipleAddToBag,
    addMultipleToBagEcom,
    selectedOutfitProducts,
    currentState,
    isSBP,
    setSelectedOutfitProduct,
    navigation,
  } = props;

  MultiProductAddToBag.propTypes = {
    AddItemsToBagLabel: PropTypes.string.isRequired,
    selectedItemsCount: PropTypes.string.isRequired,
    handleMultipleAddToBag: PropTypes.func.isRequired,
    addMultipleToBagEcom: PropTypes.func.isRequired,
    selectedOutfitProducts: PropTypes.shape({}).isRequired,
    currentState: PropTypes.shape({}).isRequired,
    isSBP: PropTypes.bool.isRequired,
    setSelectedOutfitProduct: PropTypes.func.isRequired,
    navigation: PropTypes.shape({}).isRequired,
  };

  return (
    <AddToBag
      onPress={() => {
        handleMultipleAddToBag(
          addMultipleToBagEcom,
          selectedOutfitProducts,
          currentState,
          isSBP,
          navigation
        );
        setSelectedOutfitProduct({});
      }}
    >
      <BodyCopyWithSpacing
        spacingStyles="margin-top-SM margin-bottom-XS margin-right-XXL margin-left-XXL"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="extrabold"
        color="white"
        textAlign="center"
        text={`${AddItemsToBagLabel} (${selectedItemsCount})`}
      />
    </AddToBag>
  );
};

export default MultiProductAddToBag;
