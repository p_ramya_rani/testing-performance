import React from 'react';
import { Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { getDynamicBadgeQty } from '@tcp/core/src/utils';
import ImageCarousel from '@tcp/core/src/components/common/molecules/ImageCarousel';
import { OUTFIT_LISTING_FORM } from '@tcp/core/src/constants/reducer.constants';
import { ImageContainer, ImageWrapper } from '../styles/OutfitProduct.native.style';

const ProductImageContainer = ({
  navigation,
  outfitProduct,
  imageUrls,
  keepAlive,
  outOfStockLabels,
  isDynamicBadgeEnabled,
  isNewReDesignEnabled,
  navigateToPdp,
  alternateBrand,
  isFromBundlePage,
}) => {
  const { TCPStyleType, TCPStyleQTY } = outfitProduct;
  const dimensions = Dimensions.get('window');
  const imageHeight = dimensions.height * 0.2;
  const imageWidth = 0.35 * dimensions.width;

  const dynamicBadgeOverlayQty = getDynamicBadgeQty(
    TCPStyleType,
    TCPStyleQTY,
    isDynamicBadgeEnabled
  );
  const imageUrlsObj = imageUrls ? [...imageUrls] : [];
  return (
    <ImageContainer>
      <ImageWrapper>
        <ImageCarousel
          imageUrls={imageUrlsObj}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          onImageClick={() => navigateToPdp(navigation, outfitProduct, isFromBundlePage)}
          imageWidth={imageWidth}
          imageHeight={imageHeight}
          formName={OUTFIT_LISTING_FORM}
          dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          tcpStyleType={TCPStyleType}
          isFastImage
          isNewReDesignEnabled={isNewReDesignEnabled}
          alternateBrand={alternateBrand}
        />
      </ImageWrapper>
    </ImageContainer>
  );
};

ProductImageContainer.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  outfitProduct: PropTypes.shape({}).isRequired,
  imageUrls: PropTypes.shape([]).isRequired,
  keepAlive: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  navigateToPdp: PropTypes.func.isRequired,
  isNewReDesignEnabled: PropTypes.bool,
  isFromBundlePage: PropTypes.bool,
};
ProductImageContainer.defaultProps = {
  isDynamicBadgeEnabled: {},
  isNewReDesignEnabled: false,
  isFromBundlePage: false,
};

export default ProductImageContainer;
