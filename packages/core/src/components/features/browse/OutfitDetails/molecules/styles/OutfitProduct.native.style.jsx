// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const OutfitProductContainer = styled.View`
  border-top-color: ${(props) => props.theme.colorPalette.gray[500]};
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

export const DetailsContainer = styled.View`
  width: 51%;
  padding: 15px;
`;

export const ImageContainer = styled.View`
  width: 41%;
`;
export const ImageWrapper = styled.View`
  align-items: center;
`;
export const DiscountedPriceContainer = styled.View`
  flex-direction: row;
`;

export const FavoriteView = styled.View`
  width: ${(props) => (props.isBundleProduct ? '25%' : '13%')};
  align-self: flex-start;
  padding-right: 20px;
  margin-right: 20px;
`;

export const OutfitProductWrapper = styled.View`
  background: ${(props) => props.theme.colorPalette.white};
  border-radius: 16px;
  width: 100%;
  margin: 10px 0 10px;
  padding: 10px;
  box-shadow: ${(props) =>
    props.isFromBundlePage ? props.theme.shadow.NEW_DESIGN.BOX_SHADOW : 'none'};

  ${(props) => (props.outfitSelected ? `border: solid 3px ${props.outfitSelectedColor};` : '')}
`;

export const ImageTouchableOpacity = styled.TouchableOpacity`
  justify-content: center;
`;

export const TickTouchableOpacity = styled.TouchableOpacity`
  justify-content: center;
  position: absolute;
  z-index: 3;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

export const ProductDetailLabel = styled.TouchableOpacity`
  margin-top: 5px;
`;

export const FavouriteCount = styled.Text`
  color: ${(props) =>
    props.isFromBundlePage
      ? props.theme.colorPalette.gray[900]
      : props.theme.colorPalette.gray[600]};
  font-size: ${(props) => props.theme.typography.fontSizes.fs10};
  text-align: center;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 0 0 0;
  font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  width: 30;
`;

export const AfterPayWrapper = styled.View`
  justify-content: center;
  align-items: flex-start;
  margin-bottom: 8px;
`;
