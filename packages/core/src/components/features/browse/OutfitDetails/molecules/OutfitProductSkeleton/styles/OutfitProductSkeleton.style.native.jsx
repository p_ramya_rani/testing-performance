// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const ProductDetailWrapper = styled.ScrollView`
  flex-direction: column;
  margin: 25px 10px 18px 5px;
  background-color: white;
  width: 100%;
  height: 294px;
  border-radius: 16px;
  padding: 0 14px;
`;

export const ProductRowLayout = styled.View`
  flex-direction: row;
`;

export const ProductDetails = styled.View`
  flex-direction: column;
  width: 60%;
  margin: 25px 20px 10px 10px;
`;

export const ImageWrapper = styled.View`
  margin: 5px;
  border-radius: 16px;
  width: 37%;
`;

export const ProductSize = styled.View`
  flex-direction: column;
`;
export const SizeList = styled.View`
  flex-direction: row;
  width: 100%;
  margin: 5px 40px 20px 10px;
`;

export const SizeWrapper = styled.View`
  width: 24%;
`;

export default ProductDetailWrapper;
