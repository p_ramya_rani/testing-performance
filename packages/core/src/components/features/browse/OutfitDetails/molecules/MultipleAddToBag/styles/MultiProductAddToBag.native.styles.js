import styled from 'styled-components/native';

const AddToBag = styled.TouchableOpacity`
  width: 82%;
  height: 48px;
  margin: ${props => props.theme.spacing.ELEM_SPACING.LRG}
    ${props => props.theme.spacing.ELEM_SPACING.XL};
  border-radius: 16px;
  background-color: ${props => props.theme.colorPalette.blue['900']};
`;

export default AddToBag;
