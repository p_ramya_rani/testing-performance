/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { PRODUCT_ADD_TO_BAG, OUTFIT_LISTING_FORM } from '@tcp/core/src/constants/reducer.constants';
import { getVideoUrl, isSafariBrowser, getDynamicBadgeQty } from '@tcp/core/src/utils';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import OutfitProductPropTypes from './OutfitProductPropTypes';
import { Row, Col, BodyCopy, Anchor, DamImage } from '../../../../../common/atoms';
import ProductBasicInfo from '../../../ProductDetail/molecules/ProductBasicInfo/ProductBasicInfo';
import Carousel from '../../../../../common/molecules/Carousel';
import config, { imageData, videoData, videoDataClient } from '../../config';
import ProductPrice from '../../../ProductDetail/molecules/ProductPrice/ProductPrice';
import SIZE_CHART_LINK_POSITIONS from '../../../../../common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import {
  getPrices,
  getMapSliceForColorProductId,
  getProductListToPath,
  getImagesToDisplay,
} from '../../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import ProductAddToBagContainer from '../../../../../common/molecules/ProductAddToBag';
import withStyles from '../../../../../common/hoc/withStyles';
import OutfitProductStyle from '../styles/OutfitProduct.style';
import OutOfStockWaterMarkView from '../../../ProductDetail/molecules/OutOfStockWaterMark';
import { getIconPath, isClient } from '../../../../../../utils';

const getUpdatedPDPUrl = (pdpToPath, isBundleProduct) => {
  return pdpToPath && `${pdpToPath}&dataSource=${isBundleProduct ? 'bundle' : 'outfit'}`;
};
class OutfitDetailsView extends PureComponent {
  constructor(props) {
    super(props);
    this.formValues = null;
    this.state = {
      selectedSize: '',
      selectedFit: '',
      selectedColor: '',
    };
  }

  renderOutOfStock = (keepAlive, outOfStockLabels) => {
    return keepAlive ? <OutOfStockWaterMarkView label={outOfStockLabels.outOfStockCaps} /> : null;
  };

  carouselImageCollection = (pdpToPath, pdpUrl, name, imagesToDisplay, alternateBrand) => {
    return (
      <>
        <Carousel
          className="carousel-item"
          options={config.CAROUSEL_OPTIONS}
          carouselConfig={{
            autoplay: false,
            customArrowLeft: getIconPath('carousel-big-carrot'),
            customArrowRight: getIconPath('carousel-big-carrot'),
          }}
        >
          {imagesToDisplay &&
            imagesToDisplay.map((image) => {
              const imageUrl = image.regularSizeImageUrl;
              const isVideoUrl = imageUrl && getVideoUrl(imageUrl);
              let mainConfig = isVideoUrl ? videoData.imgConfig : imageData.imgConfig;
              mainConfig = isSafariBrowser() && isVideoUrl ? videoDataClient.imgConfig : mainConfig;

              return (
                <Anchor to={pdpToPath} asPath={pdpUrl}>
                  <DamImage
                    className="full-size-desktop-image"
                    imgData={{ alt: name, url: imageUrl }}
                    itemProp="contentUrl"
                    imgConfigs={mainConfig}
                    isProductImage
                    isOptimizedVideo={!!isVideoUrl}
                    primaryBrand={alternateBrand}
                  />
                </Anchor>
              );
            })}
        </Carousel>
      </>
    );
  };

  damImageOutfit = (
    images,
    pdpToPath,
    pdpUrl,
    name,
    imagesToDisplay,
    isOnModelAbTestEnabled,
    dynamicBadgeVal
  ) => {
    const { dynamicBadgeOverlayQty, TCPStyleType } = dynamicBadgeVal;
    const { outfitProduct, alternateBrand } = this.props;
    const { primaryBrand } = outfitProduct;
    let imageUrl = images.basicImageUrl;
    const isVideoUrl = imageUrl && getVideoUrl(imageUrl);
    let mainConfig = isVideoUrl ? videoData.imgConfig : imageData.imgConfig;
    mainConfig = isSafariBrowser() && isVideoUrl ? videoDataClient.imgConfig : mainConfig;
    if (isOnModelAbTestEnabled && imagesToDisplay && imagesToDisplay.length) {
      // for outfit since we do not show carousel for product images
      // we will show the first on model image in case AB test is ON
      imageUrl = imagesToDisplay[0].regularSizeImageUrl;
    }

    const shouldRenderDamImage = !isVideoUrl || isClient();

    if (shouldRenderDamImage) {
      return (
        <>
          <Anchor to={pdpToPath} asPath={pdpUrl}>
            <DamImage
              className="full-size-desktop-image"
              imgData={{ alt: name, url: imageUrl }}
              itemProp="contentUrl"
              imgConfigs={mainConfig}
              isProductImage
              isOptimizedVideo={!!isVideoUrl}
              dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
              tcpStyleType={TCPStyleType}
              badgeConfig={imageData.badgeConfig}
              primaryBrand={primaryBrand || alternateBrand}
            />
          </Anchor>
        </>
      );
    }
    return null;
  };

  imageDisplay = (
    pageName,
    images,
    pdpToPath,
    pdpUrl,
    name,
    imagesToDisplay,
    isOnModelAbTestEnabled,
    dynamicBadgeOverlayQty,
    TCPStyleType,
    alternateBrand
    // eslint-disable-next-line max-params
  ) => {
    return pageName === 'BUNDLE' && imagesToDisplay && imagesToDisplay.length > 1
      ? this.carouselImageCollection(pdpToPath, pdpUrl, name, imagesToDisplay, alternateBrand)
      : this.damImageOutfit(
          images,
          pdpToPath,
          pdpUrl,
          name,
          imagesToDisplay,
          isOnModelAbTestEnabled,
          { dynamicBadgeOverlayQty, TCPStyleType }
        );
  };

  isOnModelAbTestActive = (isBundleProduct, isOnModelAbTestBundlePdp, isOnModelAbTestOutfitPdp) => {
    return isBundleProduct ? isOnModelAbTestBundlePdp : isOnModelAbTestOutfitPdp;
  };

  getPdpAsPath = (outfitProduct, selectedPdpUrl, selectedPdpSeoUrl) => {
    const outfitProductPdpUrl = selectedPdpUrl || outfitProduct.pdpUrl;
    const outfitProductPdpSeoUrl = selectedPdpSeoUrl || outfitProduct.seoToken;
    return outfitProduct && outfitProduct.seoToken
      ? `/p/${outfitProductPdpSeoUrl}`
      : outfitProductPdpUrl;
  };

  getColorName = (colorProduct) => colorProduct && colorProduct.color && colorProduct.color.name;

  getPricesForProduct = (outfitProduct, colorProduct, selectedFit, selectedSize) =>
    outfitProduct && getPrices(outfitProduct, colorProduct.color.name, selectedFit, selectedSize);

  getProductName = (isBundleProduct, colorProduct) => (isBundleProduct ? colorProduct.name : '');

  getGroupBadgeLabel = (colorFitsSizesMap = []) =>
    colorFitsSizesMap[0] &&
    colorFitsSizesMap[0].miscInfo &&
    colorFitsSizesMap[0].miscInfo.groupBadgeLabel;

  getDefaultBadge = (badges) =>
    badges && badges.defaultBadge ? badges.defaultBadge : badges.matchBadge;

  getSelectedColorData = (colorFitsSizesMap, selectedColor = {}) => {
    return (
      colorFitsSizesMap &&
      colorFitsSizesMap.filter((colorItem) => {
        const {
          color: { name },
        } = colorItem;
        return (selectedColor.name || selectedColor) === name;
      })
    );
  };

  onChangeSize = (selectedColor, e) => {
    this.setState({
      selectedSize: e,
    });
  };

  onFitChange = (e) => {
    this.setState({
      selectedFit: e,
    });
  };

  onChangeColor = (e) => {
    this.setState({
      selectedColor: e,
    });
  };

  getDynamicBadgeOverlayQty = (isDynamicBadgeEnabled, TCPStyleType, TCPStyleQTY) => {
    return getDynamicBadgeQty(TCPStyleType, TCPStyleQTY, isDynamicBadgeEnabled);
  };

  render() {
    const {
      className,
      outfitProduct,
      productIndexText,
      plpLabels,
      labels,
      isCanada,
      isPlcc,
      isInternationalShipping,
      currencySymbol,
      currencyAttributes,
      handleAddToBag,
      addToBagError,
      isLoggedIn,
      addToFavorites,
      isBundleProduct,
      isKeepAliveEnabled,
      outOfStockLabels,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      pageName,
      isOnModelAbTestBundlePdp,
      isOnModelAbTestOutfitPdp,
      showAddedToBagCta,
      isDynamicBadgeEnabled,
      multiPackThreshold,
      getAllNewMultiPack,
      isInWishList,
      wishList,
      onSetLastDeletedItemIdAction,
      isOutfitPage,
      errorMessages,
      alternateBrand,
    } = this.props;
    const { selectedSize, selectedFit, selectedColor } = this.state;

    const { imagesByColor, colorFitsSizesMap, isGiftCard, name, TCPStyleQTY, TCPStyleType } =
      outfitProduct;

    const dynamicBadgeOverlayQty = this.getDynamicBadgeOverlayQty(
      isDynamicBadgeEnabled,
      TCPStyleType,
      TCPStyleQTY
    );
    const selectedColorObject = this.getSelectedColorData(colorFitsSizesMap, selectedColor);
    const selectedColorArray = selectedColorObject && selectedColorObject[0];

    const selectedColorId = !selectedColorArray ? '' : selectedColorArray.colorProductId;
    const selectedPdpUrl = !selectedColorArray ? '' : selectedColorArray.pdpUrl;
    const selectedPdpSeoUrl = !selectedColorArray ? '' : selectedColorArray.pdpSeoUrl;
    const groupBadgeLabel = this.getGroupBadgeLabel(colorFitsSizesMap);
    const colorProduct =
      outfitProduct && getMapSliceForColorProductId(colorFitsSizesMap, selectedColorId);

    const color = this.getColorName(colorProduct);
    const prices = this.getPricesForProduct(outfitProduct, colorProduct, selectedFit, selectedSize);
    const bundleProductName = this.getProductName(isBundleProduct, colorProduct);
    const badges = colorProduct.miscInfo.badge1;
    const badge1 = this.getDefaultBadge(badges);
    const isOnModelAbTestEnabled = this.isOnModelAbTestActive(
      isBundleProduct,
      isOnModelAbTestBundlePdp,
      isOnModelAbTestOutfitPdp
    );

    const imagesToDisplay = getImagesToDisplay({
      imagesByColor,
      curentColorEntry: colorProduct,
      isAbTestActive: isOnModelAbTestEnabled,
      isFullSet: true,
    });

    let pdpAsPath = this.getPdpAsPath(outfitProduct, selectedPdpUrl, selectedPdpSeoUrl);
    pdpAsPath = alternateBrand ? `${pdpAsPath}$brand=${alternateBrand}` : `${pdpAsPath}`;
    const pdpToPath = getProductListToPath(pdpAsPath);
    let newPdpToPath = getUpdatedPDPUrl(pdpToPath, isBundleProduct);
    newPdpToPath = alternateBrand ? `${newPdpToPath}$brand=${alternateBrand}` : `${newPdpToPath}`;

    const viewDetails = labels && labels.lbl_outfit_viewdetail;
    const images = imagesByColor[color];

    const sizeChartLinkVisibility = !isGiftCard ? SIZE_CHART_LINK_POSITIONS.AFTER_SIZE : null;
    const keepAlive = isKeepAliveEnabled && colorProduct.miscInfo.keepAlive;

    return (
      <>
        <BodyCopy fontSize="fs16" fontFamily="secondary" color="gray.900" fontWeight="extrabold">
          {groupBadgeLabel}
        </BodyCopy>
        <Row className={className}>
          <Col
            colSize={{ small: 6, medium: 3, large: 4 }}
            ignoreGutter={{ small: true }}
            hideCol={{ small: true, medium: true, large: false }}
            className="desktop-image-section"
          >
            <BodyCopy fontSize="fs10" fontFamily="secondary" className="image-section">
              {productIndexText}
            </BodyCopy>
            <BodyCopy component="div" className="image-wrapper">
              {this.imageDisplay(
                pageName,
                images,
                newPdpToPath,
                pdpAsPath,
                name,
                imagesToDisplay,
                isOnModelAbTestEnabled,
                dynamicBadgeOverlayQty,
                TCPStyleType,
                alternateBrand
              )}
              {this.renderOutOfStock(keepAlive, outOfStockLabels)}
            </BodyCopy>
            <BodyCopy className="view-detail-anchor">
              <Anchor underline fontSizeVariation="large" to={newPdpToPath} asPath={pdpAsPath}>
                {viewDetails}
              </Anchor>
            </BodyCopy>
          </Col>
          <Col
            colSize={{ small: 6, medium: 8, large: 8 }}
            ignoreGutter={{ small: true, medium: true, large: true }}
            className="tablet-product-info"
          >
            <div className="tablet-image-section">
              <BodyCopy fontSize="fs10" fontFamily="secondary" className="image-section">
                {productIndexText}
              </BodyCopy>

              <BodyCopy component="div" className="outfit-mobile-image">
                {this.imageDisplay(
                  pageName,
                  images,
                  newPdpToPath,
                  pdpAsPath,
                  name,
                  imagesToDisplay,
                  isOnModelAbTestEnabled,
                  dynamicBadgeOverlayQty,
                  TCPStyleType,
                  alternateBrand
                )}
                {this.renderOutOfStock(keepAlive, outOfStockLabels)}
              </BodyCopy>
              <BodyCopy className="view-detail-anchor">
                <Anchor underline fontSizeVariation="large" to={newPdpToPath} asPath={pdpAsPath}>
                  {viewDetails}
                </Anchor>
              </BodyCopy>
            </div>
            <div className="product-information">
              <ProductBasicInfo
                isOutfitPage={isOutfitPage}
                keepAlive={keepAlive}
                outOfStockLabels={outOfStockLabels}
                productInfo={outfitProduct}
                isCanada={isCanada}
                isPlcc={isPlcc}
                pdpUrl={newPdpToPath}
                asPath={pdpAsPath}
                isInternationalShipping={isInternationalShipping}
                onAddItemToFavorites={addToFavorites}
                isLoggedIn={isLoggedIn}
                badge={badge1}
                productMiscInfo={colorProduct}
                AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                pageName={pageName || 'OUTFIT'}
                formName={PRODUCT_ADD_TO_BAG}
                hideRating
                bundleProductName={bundleProductName}
                isShowFavoriteCount
                isInWishList={isInWishList}
                onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                wishList={wishList}
                errorMessages={errorMessages}
                primaryBrand={outfitProduct && outfitProduct.primaryBrand}
              />
              <ProductPrice
                isOutfitPage={isOutfitPage}
                currencySymbol={currencySymbol}
                currencyAttributes={currencyAttributes}
                {...prices}
                isCanada={isCanada}
                isPlcc={isPlcc}
                isInternationalShipping={isInternationalShipping}
                promotionalMessage={outfitProduct.promotionalMessage}
                isAfterPayDisplay
              />
            </div>
            <div className="outfit-sku">
              <ProductAddToBagContainer
                handleFormSubmit={handleAddToBag}
                currentProduct={outfitProduct}
                plpLabels={plpLabels}
                isOutfitPage={isOutfitPage}
                errorOnHandleSubmit={addToBagError}
                isBundleProduct={isBundleProduct}
                sizeChartLinkVisibility={sizeChartLinkVisibility}
                isKeepAliveEnabled={isKeepAliveEnabled}
                outOfStockLabels={outOfStockLabels}
                onChangeColor={this.onChangeColor}
                onChangeSize={this.onChangeSize}
                onFitChange={this.onFitChange}
                customFormName={OUTFIT_LISTING_FORM}
                showAddedToBagCta={showAddedToBagCta}
                multipackProduct={outfitProduct.newPartNumber}
                multiPackThreshold={multiPackThreshold}
                getAllNewMultiPack={getAllNewMultiPack}
                isInWishList={isInWishList}
                primaryBrand={outfitProduct && outfitProduct.primaryBrand}
              />
            </div>
          </Col>
        </Row>
      </>
    );
  }
}

OutfitDetailsView.propTypes = OutfitProductPropTypes;

OutfitDetailsView.defaultProps = {
  className: '',
  outfitProduct: {},
  selectedProductId: '',
  productIndexText: '',
  plpLabels: {},
  isCanada: false,
  isPlcc: false,
  currencySymbol: 'USD',
  labels: {},
  addToBagError: false,
  isLoggedIn: false,
  isBundleProduct: false,
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  pageName: '',
  isOnModelAbTestBundlePdp: false,
  isOnModelAbTestOutfitPdp: false,
  showAddedToBagCta: '',
  isDynamicBadgeEnabled: false,
  getAllNewMultiPack: {},
  onSetLastDeletedItemIdAction: () => {},
  wishList: {},
  isOutfitPage: false,
};

export default withStyles(errorBoundary(OutfitDetailsView), OutfitProductStyle);
