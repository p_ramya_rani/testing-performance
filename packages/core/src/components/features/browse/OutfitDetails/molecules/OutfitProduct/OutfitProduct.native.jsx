/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect, useRef } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { calculatePriceValue, isGymboree } from '@tcp/core/src/utils';
import { setSelectedOutfitColor, isTCP } from '@tcp/core/src/utils/utils';
import Notification from '@tcp/core/src/components/common/molecules/Notification/views/Notification.native';
import { OUTFIT_LISTING_FORM } from '@tcp/core/src/constants/reducer.constants';
import ProductRating from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductRating/ProductRating.view.native';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import PromotionalMessage from '../../../../../common/atoms/PromotionalMessage';
import {
  getPrices,
  getImagesToDisplay,
  getMapSliceForColorProductId,
  getMapSliceForSizeSkuID,
} from '../../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import ProductAddToBagContainer from '../../../../../common/molecules/ProductAddToBag';
import { BodyCopy } from '../../../../../common/atoms';
import { getPromotionalMessage } from '../../../ProductListing/molecules/ProductList/utils/utility';
import {
  OutfitProductContainer,
  DetailsContainer,
  DiscountedPriceContainer,
  FavoriteView,
  OutfitProductWrapper,
  ImageTouchableOpacity,
  ProductDetailLabel,
  TickTouchableOpacity,
  FavouriteCount,
  AfterPayWrapper,
} from '../styles/OutfitProduct.native.style';
import OpenLoginModal from '../../../../account/LoginPage/container/LoginModal.container';
import SIZE_CHART_LINK_POSITIONS from '../../../../../common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import ProductImageContainer from './ProductImageContainer.native';

const favorites = require('../../../../../../../../mobileapp/src/assets/images/favorites.png');
const favoriteSolid = require('../../../../../../../../mobileapp/src/assets/images/favorites_solid.png');
const tcpTick = require('../../../../../../../../mobileapp/src/assets/images/TCP-tick2x.png');
const gymTick = require('../../../../../../../../mobileapp/src/assets/images/GYM-tick2x.png');
const tcpTickBundle = require('../../../../../../../../mobileapp/src/assets/images/TCP-tick.png');
const gymTickBundle = require('../../../../../../../../mobileapp/src/assets/images/GYM-tick.png');

const getIsSBP = (navigation) => {
  if (navigation.state && navigation.state.params) {
    const { isSBP } = navigation.state.params;
    return isSBP;
  }
  return false;
};
const getSbpTitle = (navigation) => {
  const { title } = navigation.state.params;
  return title;
};

const navigateToPdp = (navigation, outfitProduct, isFromBundlePage) => {
  const isSBP = getIsSBP(navigation);
  const routeName = isSBP ? 'ProductDetailSBP' : 'ProductDetail';
  navigation.navigate(routeName, {
    title: isSBP ? getSbpTitle(navigation) : outfitProduct.name,
    pdpUrl: outfitProduct.pdpUrl && outfitProduct.pdpUrl.replace('/p/', ''),
    reset: true,
    viaModule: 'outfit',
    selectedColorProductId: outfitProduct.productId,
    retrievedProductInfo: null,
    isFromBundlePage,
  });
};

const onFavoriteRemove = (
  productId,
  defaultWishListFromState,
  onSetLastDeletedItemIdAction,
  deleteFavItemInProgressFlag,
  favoritedCount,
  setIsAddedToFav,
  setFavoritedCount
) => {
  if (deleteFavItemInProgressFlag) {
    return;
  }
  if (defaultWishListFromState && defaultWishListFromState[productId]) {
    const { externalIdentifier, giftListItemID } =
      defaultWishListFromState && defaultWishListFromState[productId];
    onSetLastDeletedItemIdAction({
      externalId: externalIdentifier,
      itemId: giftListItemID,
      notFromFavoritePage: true,
    });
    setIsAddedToFav(false);
    if (favoritedCount > 0) setFavoritedCount(favoritedCount - 1);
  }
};

const onFavoriteAdd = (handleAddToFavorites, skuId, setFavoritedCount, favoritedCount) => {
  handleAddToFavorites(skuId);
  setFavoritedCount(favoritedCount + 1);
};

const renderFavoriteSection = ({
  isAddedToFav,
  favoritedCount,
  handleAddToFavorites,
  skuId,
  plpLabels,
  isBundleProduct,
  defaultWishListFromState,
  onSetLastDeletedItemIdAction,
  deleteFavItemInProgressFlag,
  setIsAddedToFav,
  setFavoritedCount,
  productId,
  isFromBundlePage,
  // eslint-disable-next-line max-params
}) => {
  return (
    <FavoriteView isBundleProduct={isBundleProduct}>
      {isAddedToFav ? (
        <ImageTouchableOpacity
          accessibilityRole="button"
          accessibilityLabel={plpLabels.addToFavorites}
          onPress={() => {
            onFavoriteRemove(
              productId,
              defaultWishListFromState,
              onSetLastDeletedItemIdAction,
              deleteFavItemInProgressFlag,
              favoritedCount,
              setIsAddedToFav,
              setFavoritedCount
            );
          }}
        >
          <Image source={favoriteSolid} height={16} width={16} />
        </ImageTouchableOpacity>
      ) : (
        <ImageTouchableOpacity
          accessibilityRole="button"
          accessibilityLabel={plpLabels.addToFavorites}
          onPress={() => {
            onFavoriteAdd(handleAddToFavorites, skuId, setFavoritedCount, favoritedCount);
          }}
        >
          <Image source={favorites} height={16} width={16} />
        </ImageTouchableOpacity>
      )}
      <FavouriteCount isFromBundlePage={isFromBundlePage}>{favoritedCount}</FavouriteCount>
    </FavoriteView>
  );
};

renderFavoriteSection.propTypes = {
  isAddedToFav: PropTypes.bool.isRequired,
  favoritedCount: PropTypes.number.isRequired,
  handleAddToFavorites: PropTypes.func.isRequired,
  skuId: PropTypes.string.isRequired,
  plpLabels: PropTypes.shape({}).isRequired,
  isBundleProduct: PropTypes.bool.isRequired,
  productId: PropTypes.string.isRequired,
  defaultWishListFromState: PropTypes.shape({}).isRequired,
  onSetLastDeletedItemIdAction: PropTypes.func.isRequired,
  deleteFavItemInProgressFlag: PropTypes.bool.isRequired,
  setIsAddedToFav: PropTypes.func.isRequired,
  setFavoritedCount: PropTypes.func.isRequired,
};

const onChangeColor = (
  colorIndex,
  setCurrentColorIndex,
  colorindex,
  generalProductId,
  selectedFit,
  setSelectedFit
) => {
  if (setCurrentColorIndex) {
    setCurrentColorIndex(colorIndex);
  }
  if (colorindex && generalProductId) {
    colorindex(colorIndex, generalProductId);
  }
  setSelectedFit(selectedFit);
};

const checkIfProductAlreadySelectedForSameSize = (
  outfitProduct,
  selectedOutfitProducts,
  selectedSizeName
) => {
  let productAlreadySelected = false;
  if (selectedOutfitProducts?.length > 0) {
    selectedOutfitProducts.forEach((product) => {
      if (
        product.outfitProduct.generalProductId === outfitProduct.generalProductId &&
        product.selectedSize === selectedSizeName
      ) {
        productAlreadySelected = true;
      }
    });
  }
  return productAlreadySelected;
};

const setSizeChange = ({
  selectedSizeName,
  selectedFit,
  setSelectedFit,
  setSelectedSizeName,
  outfitProduct,
  setSelectedOutfitProduct,
  selectedOutfitProducts,
  isBundleProduct,
}) => {
  if (!isBundleProduct) {
    if (
      checkIfProductAlreadySelectedForSameSize(
        outfitProduct,
        selectedOutfitProducts,
        selectedSizeName
      )
    ) {
      setSelectedSizeName('');
      setSelectedOutfitProduct({ outfitProduct, selectedFit, selectedSize: '' });
    } else {
      setSelectedSizeName(selectedSizeName);
      setSelectedOutfitProduct({ outfitProduct, selectedFit, selectedSize: selectedSizeName });
    }
    setSelectedFit(selectedFit);
  } else {
    setSelectedFit(selectedFit);
    setSelectedSizeName(selectedSizeName);
  }
};

const setFitChange = ({
  selectedFitName: selectedFit,
  setSelectedFit,
  setSelectedOutfitProduct,
  outfitProduct,
  isBundleProduct,
}) => {
  setSelectedFit(selectedFit);
  if (!isBundleProduct) {
    setSelectedOutfitProduct({ outfitProduct, selectedFit, selectedSize: '' });
  }
};

const renderAddToBagContainer = (
  setCurrentColorIndex,
  setSelectedSizeName,
  handleAddToBag,
  outfitProduct,
  plpLabels,
  sizeChartLinkVisibility,
  addToBagError,
  isBundleProduct,
  toastMessage,
  isKeepAliveEnabled,
  outOfStockLabels,
  colorindex,
  showAddedToBagCta,
  setSelectedFit,
  multiPackThreshold,
  setSelectedOutfitProduct,
  selectedOutfitProducts,
  getAllNewMultiPack,
  isFromBundlePage,
  selectedSize,
  multipleAddToBagError,
  alternateBrand
  // eslint-disable-next-line max-params
) => {
  const { generalProductId } = outfitProduct;
  return (
    <ProductAddToBagContainer
      handleFormSubmit={handleAddToBag}
      currentProduct={outfitProduct}
      plpLabels={plpLabels}
      sizeChartLinkVisibility={sizeChartLinkVisibility}
      errorOnHandleSubmit={addToBagError}
      multiErrorOnSubmit={multipleAddToBagError}
      isOutfitPage={!isBundleProduct}
      isFromBundlePage={isFromBundlePage}
      simplifiedProductPickupView
      onChangeColor={(colorName, selectedSizeName, selectedFitName, selectedQuantity, colorIndex) =>
        onChangeColor(
          colorIndex,
          setCurrentColorIndex,
          colorindex,
          generalProductId,
          selectedFitName
        )
      }
      onChangeSize={(colorName, selectedSizeName, selectedFit) =>
        setSizeChange({
          selectedSizeName,
          selectedFit,
          setSelectedFit,
          setSelectedSizeName,
          outfitProduct,
          setSelectedOutfitProduct,
          selectedOutfitProducts,
          isBundleProduct,
        })
      }
      onFitChange={(selectedFitName) =>
        setFitChange({
          selectedFitName,
          setSelectedFit,
          setSelectedOutfitProduct,
          outfitProduct,
          isBundleProduct,
        })
      }
      isBundleProduct={isBundleProduct}
      toastMessage={toastMessage}
      isKeepAliveEnabled={isKeepAliveEnabled}
      outOfStockLabels={outOfStockLabels}
      customFormName={OUTFIT_LISTING_FORM}
      showAddedToBagCta={showAddedToBagCta}
      multiPackThreshold={multiPackThreshold}
      multipackProduct={outfitProduct.newPartNumber}
      getAllNewMultiPack={getAllNewMultiPack}
      selectedSizeFromOutfitProduct={selectedSize}
      isFromOutFit={true}
      primaryBrand={outfitProduct && outfitProduct.primaryBrand}
      alternateBrand={alternateBrand}
    />
  );
};

const getColorProductId = (colorProductId, colorFitsSizesMap, selectedColor) => {
  return (
    (colorProductId === '' &&
      colorFitsSizesMap &&
      colorFitsSizesMap[selectedColor].colorProductId) ||
    colorProductId
  );
};

const renderOutOfStockError = (keepAlive, outOfStockLabels) => {
  return keepAlive ? (
    <BodyCopy
      text={outOfStockLabels.itemSoldOutMessage}
      color="red.500"
      fontSize="fs10"
      fontFamily="secondary"
    />
  ) : null;
};

const checkKeepAlive = (isKeepAliveEnabled, keepAliveProduct) => {
  return isKeepAliveEnabled && keepAliveProduct;
};

const isOnModelAbTestActive = (
  isBundleProduct,
  isOnModelAbTestBundlePdp,
  isOnModelAbTestOutfitPdp
) => {
  return isBundleProduct ? isOnModelAbTestBundlePdp : isOnModelAbTestOutfitPdp;
};

// eslint-disable-next-line complexity
const OutfitDetailsView = ({
  outfitProduct,
  colorProductId,
  plpLabels,
  pdpLabels,
  isPlcc,
  currencySymbol,
  currencyExchange,
  favoriteCount,
  handleAddToBag,
  navigation,
  isLoggedIn,
  addToFavorites,
  isBundleProduct,
  addToBagError,
  toastMessage,
  isKeepAliveEnabled,
  outOfStockLabels,
  pageName,
  productMiscInfo,
  colorindex,
  isOnModelAbTestBundlePdp,
  isOnModelAbTestOutfitPdp,
  showAddedToBagCta,
  activeWishListId,
  isSBP,
  isDynamicBadgeEnabled,
  multiPackThreshold,
  setSelectedOutfitProduct,
  selectedOutfitProducts,
  getAllNewMultiPack,
  defaultWishListFromState,
  onSetLastDeletedItemIdAction,
  deleteFavItemInProgressFlag,
  errorMessages,
  isNewReDesignEnabled,
  isFromBundlePage,
  isCTL,
  multipleAddToBagError,
  alternateBrand,
  // eslint-disable-next-line sonarjs/cognitive-complexity
}) => {
  const [showModal, setShowModal] = useState(false);
  const [isAddedToFav, setIsAddedToFav] = useState(
    (defaultWishListFromState && defaultWishListFromState[outfitProduct.productId]) || false
  );
  const [currentColorIndex, setCurrentColorIndex] = useState(0);
  const [selectedSizeName, setSelectedSizeName] = useState('');
  const [selectedFit, setSelectedFit] = useState('');
  const [isFaviconClicked, setFaviconClicked] = useState(false);
  const [skuDetails, setSkuDetails] = useState(null);

  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  };

  const prevLoggedIn = usePrevious(isLoggedIn);

  useEffect(() => {
    if (prevLoggedIn !== isLoggedIn && isFaviconClicked) {
      setShowModal(false);
      setFaviconClicked(false);
      addToFavorites({
        colorProductId: outfitProduct.generalProductId,
        productSkuId: (skuDetails && skuDetails.skuId) || null,
        // eslint-disable-next-line no-use-before-define
        pdpColorProductId: colorProduct.colorProductId,
        page: pageName || 'OUTFIT',
        activeWishListId: isSBP ? activeWishListId : undefined,
        loading: isSBP,
      });
    }
    setIsAddedToFav(
      (defaultWishListFromState && defaultWishListFromState[outfitProduct.productId]) || false
    );
  }, [isLoggedIn, productMiscInfo]);

  const {
    colorFitsSizesMap,
    promotionalMessage,
    promotionalPLCCMessage,
    name,
    reviewsCount,
    ratings,
  } = outfitProduct;

  const colorProductIdValue = getColorProductId(
    colorProductId,
    colorFitsSizesMap,
    currentColorIndex
  );

  const colorProduct =
    outfitProduct && getMapSliceForColorProductId(colorFitsSizesMap, colorProductIdValue);
  const prices =
    outfitProduct &&
    getPrices(outfitProduct, colorProduct.color.name, selectedFit, selectedSizeName);
  let imageUrls = [];

  const [favoritedCount, setFavoritedCount] = useState(favoriteCount || 0);

  if (colorFitsSizesMap && outfitProduct?.imagesByColor) {
    const isOnModelAbTestEnabled = isOnModelAbTestActive(
      isBundleProduct,
      isOnModelAbTestBundlePdp,
      isOnModelAbTestOutfitPdp
    );
    imageUrls = getImagesToDisplay({
      imagesByColor: outfitProduct?.imagesByColor,
      curentColorEntry: colorProduct,
      isAbTestActive: isOnModelAbTestEnabled,
      isFullSet: true,
    });
  }

  let skuId = null;
  if (typeof selectedSizeName === 'string' && selectedSizeName) {
    skuId = getMapSliceForSizeSkuID(colorProduct, selectedSizeName);
  }

  const { miscInfo } = colorProduct;

  const { listPrice, offerPrice } = prices;
  // The PLP badge2 (EXTENDED SIZE etc) are not showing on the PDP as per the production behavior
  const { badge1, badge2, keepAlive: keepAliveProduct } = miscInfo;

  const keepAlive = checkKeepAlive(isKeepAliveEnabled, keepAliveProduct);
  // get default top badge data
  const badge1Value = badge1.matchBadge ? badge1.matchBadge : badge1.defaultBadge;

  // calculate default list price
  const listPriceForColor = calculatePriceValue(
    listPrice,
    currencySymbol,
    currencyExchange[0].exchangevalue,
    0
  );

  // calculate default offer price
  const offerPriceForColor = calculatePriceValue(
    offerPrice,
    currencySymbol,
    currencyExchange[0].exchangevalue,
    0
  );

  const handleAddToFavorites = (productSkuId) => {
    if (isLoggedIn) {
      addToFavorites({
        colorProductId: outfitProduct.generalProductId,
        productSkuId: (productSkuId && productSkuId.skuId) || null,
        pdpColorProductId: colorProduct.colorProductId,
        page: pageName || 'OUTFIT',
        activeWishListId: isSBP ? activeWishListId : undefined,
        loading: isSBP,
      });
      setSkuDetails(skuId);
    } else {
      setShowModal(true);
      setFaviconClicked(true);
    }
  };

  const loyaltyPromotionMessage = getPromotionalMessage(isPlcc, {
    promotionalMessage,
    promotionalPLCCMessage,
  });

  const sizeChartLinkVisibility = !outfitProduct.isGiftCard
    ? SIZE_CHART_LINK_POSITIONS.AFTER_SIZE
    : null;

  const selectedStatus = () => {
    let selected = false;
    if (selectedOutfitProducts?.length > 0) {
      selectedOutfitProducts.forEach((product) => {
        if (product.outfitProduct.generalProductId === outfitProduct.generalProductId) {
          selected = true;
        }
      });
    }
    return selected;
  };
  const tickImageTCP = isFromBundlePage ? tcpTickBundle : tcpTick;
  const tickImageGYM = isFromBundlePage ? gymTickBundle : gymTick;

  const favStatus =
    (defaultWishListFromState && defaultWishListFromState[outfitProduct.productId]) || false;
  if (favStatus !== isAddedToFav) setIsAddedToFav(favStatus);

  return (
    <OutfitProductWrapper
      isFromBundlePage={isFromBundlePage}
      outfitSelected={!isBundleProduct ? selectedStatus() : null}
      outfitSelectedColor={!isBundleProduct ? setSelectedOutfitColor() : null}
    >
      {!isBundleProduct && selectedStatus() ? (
        <TickTouchableOpacity
          accessibilityRole="button"
          accessibilityLabel="selected label"
          onPress={() => {
            setSelectedSizeName('');
            setSelectedOutfitProduct({ outfitProduct, selectedFit, selectedSize: '' });
          }}
        >
          <Image source={isTCP() ? tickImageTCP : tickImageGYM} height={16} width={16} />
        </TickTouchableOpacity>
      ) : null}
      {errorMessages &&
        errorMessages[outfitProduct.generalProductId] &&
        errorMessages[outfitProduct.generalProductId].itemId === outfitProduct.generalProductId && (
          <Notification
            status="error"
            message={`Error : ${errorMessages[outfitProduct.generalProductId].errorMessage}`}
          />
        )}

      <OutfitProductContainer>
        <ProductImageContainer
          navigation={navigation}
          outfitProduct={outfitProduct}
          imageUrls={imageUrls}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          navigateToPdp={navigateToPdp}
          isNewReDesignEnabled={isNewReDesignEnabled}
          primaryBrand={outfitProduct && outfitProduct.primaryBrand}
          alternateBrand={alternateBrand}
          isFromBundlePage={isFromBundlePage}
        />
        <DetailsContainer>
          {badge1Value !== '' && (
            <BodyCopy
              fontFamily="secondary"
              fontSize={!isBundleProduct ? 'fs12' : 'fs10'}
              fontWeight={!isBundleProduct ? 'extrabold' : 'regular'}
              color="gray.900"
              text={badge1Value}
              margin="0 0 4px 0"
            />
          )}
          {renderOutOfStockError(keepAlive, outOfStockLabels)}
          <TouchableOpacity
            onPress={() => navigateToPdp(navigation, outfitProduct, isFromBundlePage)}
            accessible
            accessibilityRole="button"
            accessibilityLabel={`${name}`}
          >
            <BodyCopy
              fontFamily="secondary"
              fontSize={!isBundleProduct ? 'fs12' : 'fs16'}
              fontWeight={!isBundleProduct ? 'regular' : 'extrabold'}
              color="gray.900"
              text={name}
              margin={isBundleProduct ? '0 0 8px 0' : '0'}
            />
          </TouchableOpacity>
          {isBundleProduct &&
            renderFavoriteSection({
              isAddedToFav,
              setShowModal,
              isLoggedIn,
              favoritedCount,
              handleAddToFavorites,
              skuId,
              plpLabels,
              isBundleProduct,
              generalProductId: outfitProduct.generalProductId,
              defaultWishListFromState,
              onSetLastDeletedItemIdAction,
              deleteFavItemInProgressFlag,
              setIsAddedToFav,
              setFavoritedCount,
              productId: outfitProduct.productId,
            })}
          <BodyCopy
            margin="8px 0 0 0"
            fontFamily="secondary"
            fontSize={!isBundleProduct ? 'fs15' : 'fs22'}
            fontWeight="black"
            color={isGymboree() ? 'gray.900' : 'red.500'}
            text={offerPriceForColor}
          />
          <DiscountedPriceContainer>
            {listPriceForColor !== offerPriceForColor && (
              <BodyCopy
                dataLocator="pdp_discounted_product_price"
                textDecoration="line-through"
                fontFamily="secondary"
                fontSize={!isBundleProduct ? 'fs10' : 'fs14'}
                fontWeight="regular"
                color="gray.800"
                text={listPriceForColor}
                accessibilityText={`${pdpLabels && pdpLabels.previousPrice}: ${listPriceForColor}`}
              />
            )}
            <BodyCopy
              dataLocator="pdp_discounted_percentage"
              margin="0 0 0 10px"
              fontFamily="secondary"
              fontSize={!isBundleProduct ? 'fs10' : 'fs14'}
              fontWeight="regular"
              color="red.500"
              text={badge2}
            />
          </DiscountedPriceContainer>
          <AfterPayWrapper>
            <AfterPayMessaging
              offerPrice={offerPrice}
              isCTL={isCTL}
              isFromBundlePage={isFromBundlePage}
            />
          </AfterPayWrapper>
          {loyaltyPromotionMessage && (
            <PromotionalMessage
              fontSize="fs12"
              text={loyaltyPromotionMessage}
              isPlcc={isPlcc}
              marginTop={8}
              dataLocator="pdp_loyalty_text"
            />
          )}
          {!isBundleProduct ? (
            <>
              <ProductRating ratings={ratings || 0} reviewsCount={reviewsCount} />
              <ProductDetailLabel
                onPress={() => navigateToPdp(navigation, outfitProduct, isFromBundlePage)}
                accessible
                accessibilityRole="button"
                accessibilityLabel={`${outfitProduct.name}`}
              >
                <BodyCopy
                  textAlign="left"
                  fontSize="fs14"
                  fontWeight="light"
                  textDecoration="underline"
                  text="View Product Details"
                />
              </ProductDetailLabel>
            </>
          ) : null}
        </DetailsContainer>
        {!isBundleProduct &&
          renderFavoriteSection({
            isAddedToFav,
            setShowModal,
            isLoggedIn,
            favoritedCount,
            handleAddToFavorites,
            skuId,
            plpLabels,
            generalProductId: outfitProduct.generalProductId,
            defaultWishListFromState,
            onSetLastDeletedItemIdAction,
            deleteFavItemInProgressFlag,
            setIsAddedToFav,
            setFavoritedCount,
            productId: outfitProduct.productId,
            isFromBundlePage,
          })}
      </OutfitProductContainer>
      {renderAddToBagContainer(
        setCurrentColorIndex,
        setSelectedSizeName,
        handleAddToBag,
        outfitProduct,
        plpLabels,
        sizeChartLinkVisibility,
        addToBagError,
        isBundleProduct,
        toastMessage,
        isKeepAliveEnabled,
        outOfStockLabels,
        colorindex,
        showAddedToBagCta,
        setSelectedFit,
        multiPackThreshold,
        setSelectedOutfitProduct,
        selectedOutfitProducts,
        getAllNewMultiPack,
        isFromBundlePage,
        selectedSizeName,
        multipleAddToBagError
      )}

      {showModal && (
        <OpenLoginModal
          component="login"
          openState={showModal}
          setLoginModalMountState={(params) => setShowModal(params.state)}
          isUserLoggedIn={isLoggedIn}
          variation="favorites"
        />
      )}
    </OutfitProductWrapper>
  );
};

OutfitDetailsView.propTypes = {
  labels: PropTypes.shape({}),
  outfitProduct: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  colorProductId: PropTypes.string,
  plpLabels: PropTypes.shape({}),
  pdpLabels: PropTypes.shape({}),
  isPlcc: PropTypes.bool,
  currencySymbol: PropTypes.string,
  currencyExchange: PropTypes.string,
  favoriteCount: PropTypes.string,
  handleAddToBag: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
  addToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  isBundleProduct: PropTypes.bool,
  addToBagError: PropTypes.string,
  toastMessage: PropTypes.func,
  isKeepAliveEnabled: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}),
  productMiscInfo: PropTypes.shape({}),
  colorindex: PropTypes.func,
  pageName: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  isOnModelAbTestBundlePdp: PropTypes.bool,
  isOnModelAbTestOutfitPdp: PropTypes.bool,
  showAddedToBagCta: PropTypes.bool,
  activeWishListId: PropTypes.string,
  isSBP: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  multiPackThreshold: PropTypes.string,
  setSelectedOutfitProduct: PropTypes.func.isRequired,
  selectedOutfitProducts: PropTypes.shape([]).isRequired,
  getAllNewMultiPack: PropTypes.shape({}),
  defaultWishListFromState: PropTypes.shape({}).isRequired,
  onSetLastDeletedItemIdAction: PropTypes.func.isRequired,
  deleteFavItemInProgressFlag: PropTypes.bool.isRequired,
  errorMessages: PropTypes.shape({}),
  multipleAddToBagError: PropTypes.string,
  isNewReDesignEnabled: PropTypes.bool,
  isFromBundlePage: PropTypes.bool,
};

OutfitDetailsView.defaultProps = {
  labels: {},
  outfitProduct: {},
  colorProductId: '',
  plpLabels: {},
  pdpLabels: {},
  isPlcc: false,
  currencySymbol: '$',
  currencyExchange: [{ exchangevalue: 1 }],
  favoriteCount: 0,
  navigation: {},
  isLoggedIn: false,
  addToFavorites: () => {},
  isBundleProduct: false,
  addToBagError: '',
  outOfStockLabels: {},
  toastMessage: () => {},
  productMiscInfo: {},
  colorindex: () => {},
  pageName: '',
  removeAddToFavoritesErrorMsg: () => {},
  isOnModelAbTestBundlePdp: false,
  isOnModelAbTestOutfitPdp: false,
  showAddedToBagCta: false,
  activeWishListId: '',
  isSBP: false,
  isDynamicBadgeEnabled: {},
  multiPackThreshold: '',
  getAllNewMultiPack: {},
  errorMessages: {},
  multipleAddToBagError: '',
  isNewReDesignEnabled: false,
  isFromBundlePage: false,
};
export default OutfitDetailsView;
export { OutfitDetailsView as OutfitDetailsViewVanilla };
