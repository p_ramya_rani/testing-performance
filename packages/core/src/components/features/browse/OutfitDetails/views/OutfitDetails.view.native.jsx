// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
// Disabling eslint for temporary fix
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { FlatList, View, Image, TouchableOpacity } from 'react-native';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { constructPageName, setRecommendationsObj } from '@tcp/core/src/utils/utils.app';
import { formatProductsData } from '@tcp/core/src/utils/utils';
import {
  ScrollViewContainer,
  RecommendationWrapper,
  OutfitListHeader,
} from '../styles/OutfitDetails.native.style';

import CustomImage from '../../../../common/atoms/CustomImage';
import MultiProductAddToBag from '../molecules/MultipleAddToBag';
import OutfitProduct from '../molecules/OutfitProduct/OutfitProduct.native';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import { OUTFIT_LISTING_FORM } from '../../../../../constants/reducer.constants';
import names from '../../../../../../../core/src/constants/eventsName.constants';
import OutfitCarousel from './OutfitCarousel.view.native';
import OutfitProductsList from './OutfitProductsList.native';
import OutfitProductSkeleton from '../molecules/OutfitProductSkeleton/views/OutfitProductSkeleton.view.native';
import { TIER1 } from '../../../../../constants/tiering.constants';
import { getTotalQuantityOfProducts } from '../util';

/**
 * @function renderItem populates the L1 menu item from the data passed to it
 * @param {object} item Details of the L1 menu item passed from the loop
 */

const OutfitDetailsView = (props) => {
  const { isSBP, fromPage, isCTL } = props.navigation.state.params;
  const {
    outfitImageUrl,
    outfitProducts,
    plpLabels,
    handleAddToBag,
    addToFavorites,
    addToBagEcom,
    currentState,
    labels,
    navigation,
    isLoggedIn,
    outfitId,
    pdpLabels,
    unavailableCount,
    toastMessage,
    AddToFavoriteErrorMsg,
    removeAddToFavoritesErrorMsg,
    isKeepAliveEnabled,
    outOfStockLabels,
    trackPageLoad,
    isOnModelAbTestOutfitPdp,
    isEnabledOutfitAddedCTA,
    showAddedToBagCta,
    setExternalCampaignFired,
    isExternalCampaignFired,
    activeWishListId,
    isDynamicBadgeEnabled,
    styliticsProductTabList,
    shopThisLookLabel,
    showSmallImage,
    getOutfit,
    outfitItemId,
    isLoading,
    multiPackThreshold,
    selectedOutfitProducts,
    setSelectedOutfitProduct,
    handleMultipleAddToBag,
    addMultipleToBagEcom,
    defaultWishListFromState,
    onSetLastDeletedItemIdAction,
    deleteFavItemInProgressFlag,
    errorMessages,
    completeFormValues,
    multipleAddToBagError,
  } = props;
  const { deviceTier } = navigation.getScreenProps();
  const recommendationAttributes = {
    variation: 'moduleO',
    navigation,
    page: Constants.RECOMMENDATIONS_PAGES_MAPPING.OUTFIT,
    partNumber: outfitId,
    isHeaderAccordion: true,
    priceOnly: true,
  };

  const selectedTotalQuantity = getTotalQuantityOfProducts(
    completeFormValues,
    selectedOutfitProducts
  );
  const [currentColorIndex, setCurrentColorIndex] = useState(0);
  const selectedProductId = navigation?.getParam('selectedProductId');
  const isScrolled = navigation.getParam('isScrolled');
  // Get the gender based on breadcrumb
  const getGender = (breadcrumbs) => {
    const gender =
      breadcrumbs && breadcrumbs[0] && breadcrumbs[0].displayName
        ? breadcrumbs[0].displayName.toLowerCase()
        : '';
    return gender === 'home' ? '' : gender;
  };

  useEffect(() => {
    if (outfitProducts?.length) {
      let productsFormatted = formatProductsData(outfitProducts, null, navigation, null, fromPage);
      const viaModule = (navigation && navigation.getParam('viaModule')) || '';
      const breadCrumbs = (navigation && navigation.getParam('breadCrumbs')) || null;
      const icidParam = navigation && navigation.getParam('internalCampaign');
      const externalCampaignId = navigation.getParam('externalCampaignId');
      const omMID = navigation.getParam('omMID');
      const omRID = navigation.getParam('omRID');

      productsFormatted = productsFormatted.map((product) => {
        const recObj = {
          recsProductId: product.recsProductId,
          recsPageType: product.recsPageType,
          recsType: product.recsType,
        };
        setRecommendationsObj(recObj);
        const productWithGender = product;
        productWithGender.gender = getGender(breadCrumbs);
        return productWithGender;
      });

      const recsType =
        viaModule && (viaModule === 'Stylistics_Recommendation' ? 'complete the look' : viaModule);
      let AnalyticsEvents = ['prodView', 'internalCampaignImpressions_e80', 'event93'];

      if (viaModule === 'Stylistics_Recommendation') {
        AnalyticsEvents = [
          'prodView',
          'Outfit_Page_Views_e62',
          'Stylitics_Product_View_e75',
          'internalCampaignImpressions_e80',
          names.eVarPropEvent.event50,
        ];
      }

      const analyticsData = {
        currentScreen: 'outfitDetail',
        events: AnalyticsEvents,
        pageData: {
          pageName: constructPageName(navigation),
          pageType: 'outfit',
          pageSection: 'outfit',
          pageSubSection: 'outfit',
          pageNavigationText: 'non-browse',
        },
        supressProps: ['eVar79'],
      };

      let customData = {};
      if (icidParam) {
        customData = {
          products: productsFormatted,
          internalCampaignId: icidParam,
          searchText: 'non-internal search',
          pageNavigationText: 'non-browse',
          originType: 'internal campaign',
          listingCount: 'non-internal search',
          searchType: 'non-internal search',
        };
      } else {
        customData = {
          products: productsFormatted,
          internalCampaignId: 'non-internal campaign',
          searchText: 'non-internal search',
          searchType: 'non-internal search',
          listingCount: 'non-internal search',
          customEvents: AnalyticsEvents,
          originType: `cross-sell:${recsType}`,
        };
      }
      const externalEvents = [];
      if (externalCampaignId) {
        externalEvents.push('event18');
        setExternalCampaignFired(true);
      }
      if (isExternalCampaignFired && !externalCampaignId) {
        externalEvents.push('event19');
        setExternalCampaignFired(false);
      }
      trackPageLoad(analyticsData, {
        ...customData,
        customEvents: [...(customData.customEvents || []), ...externalEvents],
        ...(externalCampaignId && {
          externalCampaignId,
        }),
        ...(omMID && { omMID }),
        ...(omRID && { omRID }),
      });
    }
    return () => {
      setSelectedOutfitProduct({});
    };
  }, [outfitProducts?.length]);

  const onScrollEndDrag = (event) => {
    const { navigation } = props;
    const isScrolled =
      event &&
      event.nativeEvent &&
      event.nativeEvent.contentOffset &&
      event.nativeEvent.contentOffset.y > 400;
    navigation.setParams({
      isScrolled,
    });
  };

  const onScroll = (event) => {
    const { navigation } = props;
    const { contentOffset } = event?.nativeEvent;
    if (!isScrolled) {
      if (contentOffset.y >= 400) {
        navigation.setParams({
          isScrolled: true,
        });
      }
    }
  };

  return (
    <>
      <ScrollViewContainer
        keyboardShouldPersistTaps="handled"
        onScrollEndDrag={(e) => onScrollEndDrag(e)}
        onScroll={(event) => onScroll(event)}
      >
        <OutfitCarousel
          selectedProductId={selectedProductId}
          navigation={navigation}
          shopThisLookLabel={shopThisLookLabel}
          showSmallImage={showSmallImage}
          styliticsProductTabList={styliticsProductTabList}
          getOutfit={getOutfit}
          isLoading={isLoading}
          setSelectedOutfitProduct={setSelectedOutfitProduct}
          ctlTitle={labels.lbl_ctl_title}
        />

        {!isLoading && outfitProducts ? (
          <>
            <OutfitListHeader>{labels.lbl_outfit_list_header}</OutfitListHeader>
            <OutfitProductsList
              plpLabels={plpLabels}
              pdpLabels={pdpLabels}
              outfitProducts={outfitProducts}
              handleAddToBag={handleAddToBag}
              addToFavorites={addToFavorites}
              addToBagEcom={addToBagEcom}
              currentState={currentState}
              labels={labels}
              navigation={navigation}
              isLoggedIn={isLoggedIn}
              toastMessage={toastMessage}
              currentColorIndex={currentColorIndex}
              setCurrentColorIndex={setCurrentColorIndex}
              AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
              removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
              isKeepAliveEnabled={isKeepAliveEnabled}
              outOfStockLabels={outOfStockLabels}
              isOnModelAbTestOutfitPdp={isOnModelAbTestOutfitPdp}
              isEnabledOutfitAddedCTA={isEnabledOutfitAddedCTA}
              showAddedToBagCta={showAddedToBagCta}
              activeWishListId={activeWishListId}
              isSBP={isSBP}
              isCTL={isCTL}
              isDynamicBadgeEnabled={isDynamicBadgeEnabled}
              multiPackThreshold={multiPackThreshold}
              setSelectedOutfitProduct={setSelectedOutfitProduct}
              selectedOutfitProducts={selectedOutfitProducts}
              outfitItemId={outfitItemId}
              defaultWishListFromState={defaultWishListFromState}
              onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
              deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
              errorMessages={errorMessages}
              multipleAddToBagError={multipleAddToBagError}
            />
            {!!unavailableCount && (
              <BodyCopyWithSpacing
                spacingStyles="margin-top-XS margin-bottom-MED"
                fontSize="fs16"
                fontFamily="secondary"
                text={`${unavailableCount} ${labels.lbl_outfit_unavailable}`}
              />
            )}
            <RecommendationWrapper>
              <Recommendations
                isSBP={isSBP}
                headerLabel={labels.lbl_you_may_also_like}
                {...recommendationAttributes}
                isCardTypeTiles
                productImageWidth={130}
                productImageHeight={161}
                sequence="1"
                paddings="0px"
                margins="5px 6px 18px"
              />
              {deviceTier === TIER1 && (
                <Recommendations
                  isCardTypeTiles
                  productImageWidth={130}
                  productImageHeight={161}
                  paddings="0px"
                  margins="5px 6px 18px"
                  isSBP={isSBP}
                  isRecentlyViewed
                  {...recommendationAttributes}
                  headerLabel={labels.lbl_recently_viewed}
                  portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
                  sequence="1"
                />
              )}
            </RecommendationWrapper>
          </>
        ) : (
          <OutfitProductSkeleton />
        )}
      </ScrollViewContainer>
      {selectedOutfitProducts?.length > 0 && (
        <MultiProductAddToBag
          AddItemsToBagLabel={labels.lbl_addtobag_text}
          selectedItemsCount={selectedTotalQuantity}
          handleMultipleAddToBag={handleMultipleAddToBag}
          addMultipleToBagEcom={addMultipleToBagEcom}
          selectedOutfitProducts={selectedOutfitProducts}
          currentState={currentState}
          isSBP={isSBP}
          setSelectedOutfitProduct={setSelectedOutfitProduct}
          navigation={navigation}
        />
      )}
    </>
  );
};

OutfitDetailsView.propTypes = {
  outfitImageUrl: PropTypes.string,
  outfitProducts: PropTypes.shape({}),
  unavailableCount: PropTypes.number,
  plpLabels: PropTypes.shape({}),
  item: PropTypes.shape({}),
  colorProductId: PropTypes.string,
  handleAddToBag: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  addToBagEcom: PropTypes.func.isRequired,
  currentState: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  isLoggedIn: PropTypes.bool,
  outfitId: PropTypes.string,
  pdpLabels: PropTypes.shape({}),
  toastMessage: PropTypes.func,
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
  isOnModelAbTestOutfitPdp: PropTypes.bool,
  isExternalCampaignFired: PropTypes.bool.isRequired,
  setExternalCampaignFired: PropTypes.func,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  getOutfit: PropTypes.func,
  outfitItemId: PropTypes.func,
  isLoading: PropTypes.bool,
  multiPackThreshold: PropTypes.string,
  setSelectedOutfitProduct: PropTypes.func.isRequired,
  selectedOutfitProducts: PropTypes.shape({}),
};

OutfitDetailsView.defaultProps = {
  outfitImageUrl: '',
  outfitProducts: null,
  unavailableCount: 0,
  plpLabels: {},
  item: {},
  colorProductId: '',
  labels: {},
  navigation: {},
  isLoggedIn: false,
  outfitId: '',
  pdpLabels: {},
  toastMessage: () => {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  addToBagError: '',
  addToBagErrorId: '',
  isOnModelAbTestOutfitPdp: false,
  setExternalCampaignFired: () => {},
  isDynamicBadgeEnabled: {},
  getOutfit: () => {},
  outfitItemId: '',
  isLoading: false,
  multiPackThreshold: '',
  selectedOutfitProducts: [],
};

export default OutfitDetailsView;
export { OutfitDetailsView as OutfitDetailsViewVanilla };
