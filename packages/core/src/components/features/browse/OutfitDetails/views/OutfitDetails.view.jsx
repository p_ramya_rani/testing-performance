/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import constant from '@tcp/core/src/components/common/molecules/ModuleQ/ModuleQ.constant';
import { getDimension2Value } from '@tcp/core/src/utils/utils.web';
import { isTier1Device } from '@tcp/web/src/utils/device-tiering';
import { Row, Col, Image, Anchor } from '../../../../common/atoms';
import withStyles from '../../../../common/hoc/withStyles';
import OutfitDetailsStyle, { recommendationStyles } from '../OutfitDetails.style';
import OutfitProduct from '../molecules/OutfitProduct/OutfitProduct';
import { routerPush, getIcidValue, isTCP, fireEnhancedEcomm } from '../../../../../utils';
import PromoPDPBanners from '../../../../common/organisms/PromoPDPBanners';
import names from '../../../../../constants/eventsName.constants';

const routesBack = (e) => {
  e.preventDefault();
  if (window.history.length > 2) window.history.back();
  else {
    routerPush('/', '/home');
  }
};

const PAGE_NAME = Constants.RECOMMENDATIONS_PAGES_MAPPING.OUTFIT;

const getRecsProductId = (tile, viaModule, viaPage, icidParam) => {
  if (
    ((icidParam === '' || icidParam == null) && viaModule === constant.RECOMMENDATION) ||
    viaPage === 'CLP'
  ) {
    return tile.generalProductId;
  }
  return '';
};

const checkInWishlist = (wishList, productId) => {
  return wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;
};

const ON_SALE_PRICING_STATE = 'on sale';
const FULL_PRICE_PRICING_STATE = 'full price';

const formatProductsData = (
  outfitProducts,
  viaPage,
  viaModule = '',
  pdpLabels = {},
  icidParam = ''
) => {
  return outfitProducts.map((tile) => {
    const colorName = tile.colorFitsSizesMap.map((productTile) => {
      return productTile.color.name || '';
    });
    const productId = tile.generalProductId && tile.generalProductId.split('_')[0];
    return {
      colorId: tile.generalProductId,
      color: (colorName && colorName[0]) || '',
      id: productId,
      outfitId: tile.categoryId,
      name: tile.name,
      price: tile.offerPrice,
      offerPrice: tile.offerPrice,
      listPrice: tile.listPrice,
      pricingState:
        tile.offerPrice < tile.listPrice ? ON_SALE_PRICING_STATE : FULL_PRICE_PRICING_STATE,
      rating: tile.ratings,
      reviews: tile.reviewsCount,
      recsProductId: getRecsProductId(tile, viaModule, viaPage, icidParam),
      features: 'alt images:full size image:size chart',
      dimension92:
        viaModule &&
        (viaModule === constant.RECOMMENDATION ? pdpLabels.completeTheLook : viaModule),
      recsPageType: viaPage,
    };
  });
};

const getRecs = (args) => {
  const { tile, viaModule, viaPage, icidParam = '', pdpLabels } = args;
  const cd92 =
    viaModule && (viaModule === constant.RECOMMENDATION ? pdpLabels.completeTheLook : viaModule);
  return {
    dimension20: getRecsProductId(tile, viaModule, viaPage, icidParam),
    dimension92: cd92,
    dimension21: viaPage,
    metric50: '1',
    dimension2: getDimension2Value(cd92) || 'outfit',
  };
};

const formatProductsDataEE = (
  outfitProducts,
  viaPage,
  viaModule = '',
  pdpLabels = {},
  icidParam = ''
) => {
  return outfitProducts.map((tile) => {
    const colorName = tile.colorFitsSizesMap.map((productTile) => {
      return productTile.color.name || '';
    });
    return {
      variant: (colorName && colorName[0]) || '',
      id: tile.generalProductId,
      price: tile.offerPrice,
      metric75: '1',
      dimension35: isTCP() ? 'TCP' : 'GYM',
      dimension60: tile.categoryId,
      dimension63: `${tile.offerPrice} ${tile.listPrice}`,
      dimension67:
        tile.offerPrice < tile.listPrice ? ON_SALE_PRICING_STATE : FULL_PRICE_PRICING_STATE,
      dimension71: tile.ratings,
      dimension72: 'alt images:full size image:size chart',
      dimension77: (colorName && colorName[0]) || '',
      dimension89: tile.generalProductId,
      ...getRecs({ tile, viaModule, viaPage, icidParam, pdpLabels }),
    };
  });
};

const OutfitDetailsView = ({
  className,
  outfitImageUrl,
  unavailableCount,
  outfitProducts,
  plpLabels,
  handleAddToBag,
  addToBagEcom,
  currentState,
  addToBagError,
  addToBagErrorId,
  isLoggedIn,
  addToFavorites,
  currencyAttributes,
  currencySymbol,
  labels,
  outfitId,
  AddToFavoriteErrorMsg,
  removeAddToFavoritesErrorMsg,
  asPathVal,
  topPromos,
  trackPageLoad,
  isKeepAliveEnabled,
  outOfStockLabels,
  isOnModelAbTestOutfitPdp,
  isInternationalShipping,
  viaModule,
  pdpLabels,
  storeId,
  viaPage,
  isEnabledOutfitAddedCTA,
  showAddedToBagCta,
  priceCurrency,
  isDynamicBadgeEnabled,
  multiPackThreshold,
  onSetLastDeletedItemIdAction,
  wishList,
  isOutfitPage,
}) => {
  const isCompleteTheLookProduct = useRef(false);
  const backLabel = labels && labels.lbl_outfit_back;
  const recommendationAttributes = {
    variations: 'moduleO',
    page: PAGE_NAME,
    partNumber: outfitId,
    showLoyaltyPromotionMessage: false,
    headerAlignment: 'left',
    priceOnly: true,
  };
  const willAlsoViewedCarouselDisplay = isTier1Device();

  useEffect(() => {
    const icidParam = getIcidValue(asPathVal);
    const productsFormatted = formatProductsData(
      outfitProducts,
      viaPage,
      viaModule,
      pdpLabels,
      icidParam
    );
    const productsFormattedEE = formatProductsDataEE(
      outfitProducts,
      viaPage,
      viaModule,
      pdpLabels,
      icidParam
    );
    const customEvents = ['prodView', 'event62', 'event75', 'event80'];
    let additionalData = {
      internalCampaignId: '',
      pageNavigationText: '',
    };

    if (icidParam) {
      additionalData = {
        internalCampaignId: icidParam,
        pageNavigationText: '',
      };
    } else if (viaModule === constant.RECOMMENDATION) {
      isCompleteTheLookProduct.current = true;
      customEvents.push(names.eVarPropEvent.event50);

      additionalData = {
        internalCampaignId: '',
        pageNavigationText: '',
      };
    }

    if (outfitProducts.length) {
      trackPageLoad({
        products: productsFormatted,
        pageType: 'outfit',
        pageName: productsFormatted.length
          ? `outfit:${productsFormatted[0].id}:${productsFormatted[0].name}`
          : 'outfit',
        pageSection: 'outfit',
        pageSubSection: 'outfit',
        customEvents,
        storeId,
        productId: productsFormatted[0].id,
        ...additionalData,
      });
      fireEnhancedEcomm({
        eventName: 'productDetail',
        productsObj: productsFormattedEE,
        eventType: 'detail',
        pageName: 'Outfit PDP',
        currCode: priceCurrency,
      });
    }
  }, [outfitProducts.length]);

  return (
    <div className={className}>
      <Row>
        <Col
          colSize={{
            small: 6,
            medium: 8,
            large: 12,
          }}
          ignoreGutter={{ small: true }}
          className="outfit-back-button"
        >
          <h1 className="outfit-title">Outfit Category</h1>
          <Anchor
            fontSizeVariation="xlarge"
            anchorVariation="secondary"
            handleLinkClick={routesBack}
            noLink
            className={`${className}__backlink`}
            title={backLabel}
          >
            <span className="left-arrow" />
            {backLabel}
          </Anchor>
        </Col>
      </Row>
      {topPromos && topPromos.length > 0 && (
        <Row>
          <Col className="promo-area-top" colSize={{ small: 6, medium: 8, large: 12 }}>
            <PromoPDPBanners promos={topPromos} asPath={asPathVal} />
          </Col>
        </Row>
      )}
      <Row>
        <Col
          colSize={{ small: 6, medium: 3, large: 6 }}
          ignoreGutter={{ small: true }}
          className="outfit-image"
        >
          <Image className="promo-area-0" src={outfitImageUrl} alt={pdpLabels.outfitImageLbl} />
        </Col>
        <hr className="outfit-line-break" />
        <Col
          colSize={{ small: 6, medium: 5, large: 6 }}
          ignoreGutter={{ small: true, medium: true, large: true }}
        >
          <ul className="outfiting-list-container">
            {outfitProducts &&
              outfitProducts.map((product, index) => (
                <li key={product.generalProductId}>
                  <OutfitProduct
                    plpLabels={plpLabels}
                    labels={labels}
                    outfitProduct={product}
                    productIndexText={`Product ${index + 1} of ${outfitProducts.length}`}
                    handleAddToBag={() => {
                      handleAddToBag(addToBagEcom, product, product.generalProductId, currentState);
                    }}
                    className="outfiting-list-details"
                    addToBagError={addToBagErrorId === product.generalProductId && addToBagError}
                    isLoggedIn={isLoggedIn}
                    addToFavorites={addToFavorites}
                    currencySymbol={currencySymbol}
                    currencyAttributes={currencyAttributes}
                    AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                    removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                    pageName="OUTFIT"
                    isKeepAliveEnabled={isKeepAliveEnabled}
                    outOfStockLabels={outOfStockLabels}
                    isOnModelAbTestOutfitPdp={isOnModelAbTestOutfitPdp}
                    isInternationalShipping={isInternationalShipping}
                    showAddedToBagCta={
                      isEnabledOutfitAddedCTA && showAddedToBagCta === product.generalProductId
                    }
                    isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                    multiPackThreshold={multiPackThreshold}
                    getAllNewMultiPack={product.getNewMultiPackAllColor}
                    isOutfitPage={isOutfitPage}
                    isInWishList={checkInWishlist(wishList, product.productId)}
                    onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                    wishList={wishList}
                  />
                </li>
              ))}

            {!!unavailableCount && (
              <BodyCopy
                textAlign="left"
                fontFamily="secondary"
                fontSize="fs16"
                className="elem-mt-MED elem-mb-MED"
              >
                {`${unavailableCount} ${labels.lbl_outfit_unavailable}`}
              </BodyCopy>
            )}
          </ul>
        </Col>
        <Col
          colSize={{ small: 6, medium: 8, large: 12 }}
          ignoreGutter={{ small: true, medium: true, large: true }}
        >
          <div className="placeholder promo-area-1">{labels.lbl_complete_the_look}</div>
        </Col>
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <Recommendations
            inheritedStyles={recommendationStyles}
            headerLabel={labels.lbl_you_may_also_like}
            page={PAGE_NAME}
            {...recommendationAttributes}
            sequence="1"
          />
        </Col>
        {willAlsoViewedCarouselDisplay && (
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <Recommendations
              inheritedStyles={recommendationStyles}
              headerLabel={labels.lbl_recently_viewed}
              portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
              page={PAGE_NAME}
              {...recommendationAttributes}
              sequence="2"
            />
          </Col>
        )}
      </Row>
    </div>
  );
};

OutfitDetailsView.propTypes = {
  className: PropTypes.string,
  outfitImageUrl: PropTypes.string,
  unavailableCount: PropTypes.number,
  outfitProducts: PropTypes.shape({}),
  plpLabels: PropTypes.shape({}),
  addToBagEcom: PropTypes.func.isRequired,
  handleAddToBag: PropTypes.func.isRequired,
  currentState: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}),
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
  addToFavorites: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  currencyAttributes: PropTypes.shape({}).isRequired,
  currencySymbol: PropTypes.string,
  outfitId: PropTypes.string,
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  asPathVal: PropTypes.string,
  topPromos: PropTypes.string,
  trackPageLoad: PropTypes.func,
  isOnModelAbTestOutfitPdp: PropTypes.bool,
  isInternationalShipping: PropTypes.bool.isRequired,
  pdpLabels: PropTypes.shape({}),
  viaModule: PropTypes.string,
  storeId: PropTypes.string,
  viaPage: PropTypes.string,
  isEnabledOutfitAddedCTA: PropTypes.bool,
  showAddedToBagCta: PropTypes.string,
  priceCurrency: PropTypes.string,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  multiPackThreshold: PropTypes.string,
  isInWishList: PropTypes.bool.isRequired,
  onSetLastDeletedItemIdAction: PropTypes.func,
  wishList: PropTypes.shape({}),
  isOutfitPage: PropTypes.bool,
  checkInWishlist: PropTypes.func,
};

OutfitDetailsView.defaultProps = {
  className: '',
  outfitImageUrl: '',
  outfitProducts: null,
  unavailableCount: 0,
  plpLabels: {},
  labels: {},
  addToBagError: '',
  addToBagErrorId: '',
  isLoggedIn: false,
  currencySymbol: 'USD',
  outfitId: '',
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  trackPageLoad: () => {},
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  asPathVal: '',
  topPromos: '',
  isOnModelAbTestOutfitPdp: false,
  pdpLabels: {},
  viaModule: '',
  storeId: '',
  viaPage: '',
  isEnabledOutfitAddedCTA: false,
  showAddedToBagCta: '',
  priceCurrency: 'USD',
  isDynamicBadgeEnabled: false,
  multiPackThreshold: '',
  onSetLastDeletedItemIdAction: () => {},
  wishList: {},
  isOutfitPage: true,
  checkInWishlist: () => {},
};

export default withStyles(OutfitDetailsView, OutfitDetailsStyle);
