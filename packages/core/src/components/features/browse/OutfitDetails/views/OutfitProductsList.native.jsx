import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import OutfitProduct from '../molecules/OutfitProduct/OutfitProduct.native';
import { getMapSliceForColorProductId } from '../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { OutfitList } from '../styles/OutfitDetails.native.style';

const keyExtractor1 = (_, index) => {
  return `outfit-details-${index}`;
};

const getColorindex = (colorIndex, setCurrentColorIndex) => {
  if (setCurrentColorIndex) {
    setCurrentColorIndex(colorIndex);
  }
};

export const OutfitProductsList = ({
  plpLabels,
  pdpLabels,
  outfitProducts,
  handleAddToBag,
  addToFavorites,
  addToBagEcom,
  currentState,
  labels,
  navigation,
  isLoggedIn,
  toastMessage,
  currentColorIndex,
  setCurrentColorIndex,
  AddToFavoriteErrorMsg,
  removeAddToFavoritesErrorMsg,
  isKeepAliveEnabled,
  outOfStockLabels,
  isOnModelAbTestOutfitPdp,
  isEnabledOutfitAddedCTA,
  showAddedToBagCta,
  activeWishListId,
  isSBP,
  isDynamicBadgeEnabled,
  multiPackThreshold,
  setSelectedOutfitProduct,
  selectedOutfitProducts,
  addToBagErrorId,
  addToBagError,
  colorProductId,
  outfitItemId,
  defaultWishListFromState,
  onSetLastDeletedItemIdAction,
  deleteFavItemInProgressFlag,
  errorMessages,
  isCTL,
  multipleAddToBagError,
}) => {
  const renderItem = ({ item, productsCount, index }) => {
    // eslint-disable-next-line no-shadow
    const getColorProductId = (colorProductId, colorFitsSizesMap, currentColorIndex) => {
      return (
        (colorProductId === '' &&
          colorFitsSizesMap &&
          colorFitsSizesMap[currentColorIndex].colorProductId) ||
        colorProductId
      );
    };

    const colorProductIdValue = getColorProductId(
      colorProductId,
      item.colorFitsSizesMap,
      currentColorIndex
    );

    const colorProduct =
      item && getMapSliceForColorProductId(item.colorFitsSizesMap, colorProductIdValue);
    return (
      <OutfitProduct
        plpLabels={plpLabels}
        pdpLabels={pdpLabels}
        outfitProduct={item}
        productIndexText={`Product ${index + 1} of ${productsCount}`}
        labels={labels}
        navigation={navigation}
        addToFavorites={addToFavorites}
        isLoggedIn={isLoggedIn}
        handleAddToBag={() => {
          handleAddToBag(addToBagEcom, item, item.generalProductId, currentState, isSBP);
        }}
        addToBagError={addToBagErrorId === item.generalProductId && addToBagError}
        toastMessage={toastMessage}
        AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
        removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
        productMiscInfo={colorProduct}
        favoriteCount={colorProduct.favoritedCount}
        colorindex={(colorIndex) => {
          getColorindex(colorIndex, setCurrentColorIndex);
        }}
        isKeepAliveEnabled={isKeepAliveEnabled}
        outOfStockLabels={outOfStockLabels}
        isOnModelAbTestOutfitPdp={isOnModelAbTestOutfitPdp}
        isSBP={isSBP}
        isCTL={isCTL}
        activeWishListId={activeWishListId}
        showAddedToBagCta={isEnabledOutfitAddedCTA && showAddedToBagCta === item.generalProductId}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        multiPackThreshold={multiPackThreshold}
        setSelectedOutfitProduct={setSelectedOutfitProduct}
        selectedOutfitProducts={selectedOutfitProducts}
        getAllNewMultiPack={item.getNewMultiPackAllColor}
        outfitItemId={outfitItemId}
        defaultWishListFromState={defaultWishListFromState}
        onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
        errorMessages={errorMessages}
        multipleAddToBagError={multipleAddToBagError}
      />
    );
  };

  renderItem.propTypes = {
    item: PropTypes.shape({}),
    productsCount: PropTypes.string,
    index: PropTypes.number,
  };

  renderItem.defaultProps = {
    item: {},
    productsCount: '0',
    index: 0,
  };

  return (
    <OutfitList>
      <FlatList
        data={outfitProducts}
        keyExtractor={keyExtractor1}
        listKey={(_, index) => `outfit-details-list-${index}`}
        renderItem={({ item, index }) =>
          renderItem({
            item,
            productsCount: outfitProducts?.length,
            index,
          })
        }
      />
    </OutfitList>
  );
};

OutfitProductsList.propTypes = {
  outfitProducts: PropTypes.shape([]),
  plpLabels: PropTypes.shape({}),
  handleAddToBag: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  addToBagEcom: PropTypes.func.isRequired,
  currentState: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  isLoggedIn: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  toastMessage: PropTypes.func,
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
  isOnModelAbTestOutfitPdp: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  multiPackThreshold: PropTypes.string,
  setSelectedOutfitProduct: PropTypes.func.isRequired,
  selectedOutfitProducts: PropTypes.shape([]),
  isKeepAliveEnabled: PropTypes.bool.isRequired,
  currentColorIndex: PropTypes.number.isRequired,
  setCurrentColorIndex: PropTypes.func.isRequired,
  activeWishListId: PropTypes.string.isRequired,
  isSBP: PropTypes.bool.isRequired,
  isCTL: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  isEnabledOutfitAddedCTA: PropTypes.bool.isRequired,
  showAddedToBagCta: PropTypes.bool.isRequired,
  colorProductId: PropTypes.string.isRequired,
  outfitItemId: PropTypes.string.isRequired,
  deleteFavItemInProgressFlag: PropTypes.bool,
  defaultWishListFromState: PropTypes.shape({}),
  onSetLastDeletedItemIdAction: PropTypes.func,
  errorMessages: PropTypes.shape({}),
  multipleAddToBagError: PropTypes.string,
};

OutfitProductsList.defaultProps = {
  outfitProducts: null,
  plpLabels: {},
  labels: {},
  navigation: {},
  isLoggedIn: false,
  pdpLabels: {},
  toastMessage: () => {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  addToBagError: '',
  addToBagErrorId: '',
  isOnModelAbTestOutfitPdp: false,
  isDynamicBadgeEnabled: {},
  multiPackThreshold: '',
  selectedOutfitProducts: [],
  deleteFavItemInProgressFlag: false,
  defaultWishListFromState: {},
  onSetLastDeletedItemIdAction: () => {},
  errorMessages: {},
  multipleAddToBagError: '',
};

export default OutfitProductsList;
