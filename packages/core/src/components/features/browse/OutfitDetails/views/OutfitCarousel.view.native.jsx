import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Platform, FlatList } from 'react-native';
import CarouselSlide from '../../../../common/atoms/CarouselSlide';
import { ImageSlidesWrapper, ScrollArea, ScrollBar } from '../styles/OutfitDetails.native.style';

const scrollToIndex = (ref, index = 0) => {
  if (ref && ref.current && index > -1) {
    ref.current.scrollToIndex({ index, viewPosition: 0.5 });
  }
};

const scrollToIndexFailed = (info, outfitCarouselRef) => {
  if (outfitCarouselRef !== null) {
    setTimeout(
      () =>
        outfitCarouselRef.current.scrollToIndex({
          index: info.index,
          animated: true,
          viewPosition: 0.5,
        }),
      50
    );
  }
};

export const OutfitCarousel = (props) => {
  const {
    navigation,
    shopThisLookLabel,
    showSmallImage,
    styliticsProductTabList,
    selectedProductId,
    getOutfit,
    isLoading,
    setSelectedOutfitProduct,
    ctlTitle,
  } = props;
  const outfitCarouselRef = useRef();
  const selectedOutfitId = navigation.getParam('outfitId');
  const selectedProductList = styliticsProductTabList[selectedProductId] || [];
  const [selectedOutfit, setSelectedOutfit] = useState(selectedOutfitId);
  const [nativeEvent, setNativeEvent] = useState();
  const [position, setPosition] = useState(0);
  const selectedProductCarouselList =
    selectedProductList.map((item, index) => {
      return { ...item, productItemIndex: index };
    }) || [];

  useEffect(() => {
    let selectedIndex = 0;
    selectedProductList.forEach((ele, index) => {
      if (ele.id === selectedOutfitId) {
        selectedIndex = index;
      }
    });
    setSelectedOutfit(selectedOutfitId);
    setTimeout(() => {
      scrollToIndex(outfitCarouselRef, selectedIndex);
    }, 50);
  }, [selectedOutfitId, outfitCarouselRef]);

  const onPressCTLItem = (item, index) => {
    if (index !== selectedOutfit) {
      const { id: outfitId, subItemsId: vendorColorProductIdsList } = item;
      setPosition(index);
      scrollToIndex(outfitCarouselRef, index);
      setSelectedOutfit(outfitId);
      setSelectedOutfitProduct({});
      getOutfit({
        outfitId,
        vendorColorProductIdsList,
        fromOutFitPage: true,
        fromOutfitCarousel: true,
      });
    }
  };

  const renderCarouselSlide = (slideProps) => {
    const { item, index } = slideProps;

    return (
      <CarouselSlide
        productItem={item}
        navigation={navigation}
        completeTheLookMainTile={shopThisLookLabel}
        showSmallImage={showSmallImage}
        onPress={() => onPressCTLItem(item, index)}
        selected={selectedOutfit === item.id}
        firstItem={index === 0}
        lastItem={index === selectedProductCarouselList.length - 1}
        ctlTitle={ctlTitle}
      />
    );
  };

  const left = nativeEvent
    ? (nativeEvent.contentOffset.x / nativeEvent.contentSize.width) *
      nativeEvent.layoutMeasurement.width *
      0.75
    : 0;

  return !isLoading ? (
    <ImageSlidesWrapper platform={Platform}>
      {selectedProductList?.length ? (
        <>
          <FlatList
            data={selectedProductCarouselList}
            renderItem={renderCarouselSlide}
            refreshing={false}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(_, index) => index.toString()}
            ref={outfitCarouselRef}
            onScroll={(event) => setNativeEvent(event.nativeEvent)}
            scrollEventThrottle={5}
            onScrollToIndexFailed={(info) => scrollToIndexFailed(info, outfitCarouselRef)}
          />

          <ScrollArea>
            <ScrollBar
              left={left}
              index={position}
              totalOutfits={selectedProductList?.length || 0}
            />
          </ScrollArea>
        </>
      ) : null}
    </ImageSlidesWrapper>
  ) : null;
};

const renderingCheck = (
  { selectedProductId: oldProductIdId },
  { selectedProductId: newProductIdId }
) => oldProductIdId === newProductIdId;

OutfitCarousel.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  shopThisLookLabel: PropTypes.string.isRequired,
  showSmallImage: PropTypes.bool.isRequired,
  styliticsProductTabList: PropTypes.shape({}).isRequired,
  selectedProductId: PropTypes.string.isRequired,
  getOutfit: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  setSelectedOutfitProduct: PropTypes.func.isRequired,
  ctlTitle: PropTypes.string.isRequired,
};

export default React.memo(OutfitCarousel, renderingCheck);
