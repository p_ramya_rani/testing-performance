// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export const DetailsContainer = styled.ScrollView``;

export const ScrollViewContainer = styled.ScrollView`
  border-radius: 6px;
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
`;

export const OutfitList = styled.View`
  padding: 0 ${(props) => props.theme.gridDimensions.gridOffsetObj.small}px;
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
`;

export const RecommendationWrapper = styled.View`
  margin-left: -${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding: 0 ${(props) => props.theme.gridDimensions.gridOffsetObj.small}px;
`;

/* eslint-disable */
export const ImageSlidesWrapper = styled.View`
  margin-top: ${(props) =>
    props.hideTabs || (props.divTabs && props.divTabs.length <= 1)
      ? '0'
      : props.platform.OS === 'android'
      ? props.theme.spacing.ELEM_SPACING.XXS
      : props.theme.spacing.ELEM_SPACING.SM};
  padding-top: ${(props) =>
    props.platform.OS === 'android'
      ? props.theme.spacing.ELEM_SPACING.XXS
      : props.theme.spacing.LAYOUT_SPACING.XL};
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
`;

export const OutfitListHeader = styled.Text`
  color: black;
  font-size: ${(props) => props.theme.typography.fontSizes.fs16};
  text-align: center;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
`;

export const ScrollArea = styled.View`
  height: 1;
  width: 64%;
  border-radius: 20;
  background-color: ${(props) => props.theme.colorPalette.gray[500]};
  margin-left: 45;
  margin-right: 40;
`;

export const ScrollBar = styled.View`
  left: ${(props) => props.left};
  height: 1;
  width: ${(props) => (props.totalOutfits ? `${Math.ceil(100 / props.totalOutfits)}%` : '100%')};
  border-radius: 20;
  background-color: ${(props) => props.theme.colorPalette.gray[900]};
`;
