// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .outfiting-list-details {
    margin-right: 0;
    margin-left: 0;
    width: 100%;
  }

  .outfiting-list-container {
    @media ${props => props.theme.mediaQuery.large} {
      position: relative;
    }
  }

  .outfit-back-button {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};

    @media ${props => props.theme.mediaQuery.large} {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXL};
    }
  }

  .outfit-title {
    visibility: hidden;
    height: 0;
    margin: 0;
  }

  .title-wrapper {
    display: block;
  }

  .product-title {
    margin: 0 0 8px;
  }

  .wishlist-container {
    width: 25px;
    margin: 0;
  }

  @media ${props => props.theme.mediaQuery.large} {
    .title-wrapper {
      min-width: max-content;
      display: flex;
    }
    .wishlist-container {
      position: absolute;
      right: 0;
    }
  }

  .outfit-image {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }

  .outfit-line-break {
    width: 100%;
    border-color: ${props => props.theme.colorPalette.gray[500]};
    border-top: 0;

    @media ${props => props.theme.mediaQuery.medium} {
      display: none;
    }
  }

  li {
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    border-bottom: 1px solid ${props => props.theme.colorPalette.gray[500]};
  }
`;

export const recommendationStyles = css`
  .recommendations-header {
    margin: 0;
  }

  .smooth-scroll-list-item {
    width: 40%;
    margin-right: 0;
  }
`;
