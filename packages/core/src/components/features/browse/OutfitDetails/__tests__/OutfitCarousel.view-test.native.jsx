// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OutfitCarousel } from '../views/OutfitCarousel.view.native';

describe('Outfit Carousel component', () => {
  let component;
  const props = {
    navigation: {
      getParam: () => {},
    },
    shopThisLookLabel: 'shopThisLookLabel',
    showSmallImage: false,
    styliticsProductTabList: {},
    selectedProductId: '1234',
    getOutfit: () => {},
    isLoading: false,
    setSelectedOutfitProduct: () => {},
    ctlTitle: 'CTl',
  };

  beforeEach(() => {
    component = shallow(<OutfitCarousel {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should renders correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
