// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OutfitProductsList } from '../views/OutfitProductsList.native';

describe('OutfitProductsList Component', () => {
  let component;
  const props = {
    plpLabels: {},
    pdpLabels: {},
    outfitProducts: [
      {
        getAllNewMultiPack: {},
        colorFitsSizesMap: {},
        generalProductId: '12344',
      },
    ],
    handleAddToBag: () => {},
    addToFavorites: () => {},
    addToBagEcom: () => {},
    currentState: {},
    labels: {},
    navigation: {},
    isLoggedIn: false,
    toastMessage: () => {},
    currentColorIndex: 0,
    setCurrentColorIndex: () => {},
    AddToFavoriteErrorMsg: 'addToFavouriteMsg',
    removeAddToFavoritesErrorMsg: () => {},
    isKeepAliveEnabled: false,
    outOfStockLabels: {},
    isOnModelAbTestOutfitPdp: false,
    isEnabledOutfitAddedCTA: true,
    showAddedToBagCta: false,
    activeWishListId: 'activeWishListId',
    isSBP: false,
    isDynamicBadgeEnabled: {},
    multiPackThreshold: '',
    setSelectedOutfitProduct: () => {},
    selectedOutfitProducts: [],
    addToBagErrorId: '123',
    addToBagError: 'abcd',
    colorProductId: '1234',
    outfitItemId: '40891',
  };

  beforeEach(() => {
    component = shallow(<OutfitProductsList {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should renders correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
