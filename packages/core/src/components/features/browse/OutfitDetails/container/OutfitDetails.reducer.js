// 9fbef606107a605d69c0edbcd8029e5d
import OUTFIT_DETAIL_CONSTANTS from './OutfitDetails.constants';
import { DEFAULT_REDUCER_KEY } from '../../../../../utils/cache.util';

const initialState = {
  [DEFAULT_REDUCER_KEY]: null,
  outfitCarouselModal: false,
  selectedOutfitProducts: [],
  outfitCacheData: {},
};

const removeSelectedProduct = (selectedOutfitProducts, outfitProduct) => {
  return selectedOutfitProducts.filter((product) => {
    if (product.outfitProduct.generalProductId === outfitProduct.generalProductId) {
      return false;
    }
    return true;
  });
};

const setSelectedOutfitProducts = (
  selectedOutfitProducts,
  outfitProduct,
  selectedFit,
  selectedSize,
  state
) => {
  if (selectedOutfitProducts.length > 0) {
    if (selectedSize === '') {
      return {
        ...state,
        selectedOutfitProducts: [...removeSelectedProduct(selectedOutfitProducts, outfitProduct)],
      };
    }
    let productAlreadySelected = false;
    const selectedProductList = selectedOutfitProducts.map((product) => {
      const newProduct = { ...product };
      if (product.outfitProduct.generalProductId === outfitProduct.generalProductId) {
        productAlreadySelected = true;
        newProduct.selectedSize = selectedSize;
        newProduct.selectedFit = selectedFit;
      }
      return newProduct;
    });
    if (!productAlreadySelected) {
      selectedProductList.push({ outfitProduct, selectedFit, selectedSize });
    }
    return { ...state, selectedOutfitProducts: [...selectedProductList] };
  }
  if (selectedSize === '') {
    return { ...state, selectedOutfitProducts: [] };
  }
  const selectedOutfitProduct = { outfitProduct, selectedFit, selectedSize };
  return { ...state, selectedOutfitProducts: [selectedOutfitProduct] };
};

const OutfitDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case OUTFIT_DETAIL_CONSTANTS.SET_OUTFIT_PRODUCTS:
      return { ...state, currentOutfit: action.currentOutfit };
    case OUTFIT_DETAIL_CONSTANTS.SET_LOADING_STATE:
      return { ...state, ...action.payload };
    case OUTFIT_DETAIL_CONSTANTS.RESET_OUTFIT_PRODUCTS:
      return { ...state, currentOutfit: null };
    case OUTFIT_DETAIL_CONSTANTS.CLEAR_OUTFIT_PRODUCTS:
      return { ...state, selectedOutfitProducts: [] };
    case OUTFIT_DETAIL_CONSTANTS.SET_OUTFIT_CAROUSEL_STATE: {
      return { ...state, ...action.payload };
    }
    case OUTFIT_DETAIL_CONSTANTS.SET_ADD_TO_FAVORITE:
      // eslint-disable-next-line no-case-declarations
      const outfitDetailsMap = state.currentOutfit;
      // eslint-disable-next-line consistent-return
      outfitDetailsMap.products = outfitDetailsMap.products.map((outfitDetails) => {
        // eslint-disable-next-line no-param-reassign
        outfitDetails.colorFitsSizesMap = outfitDetails.colorFitsSizesMap.map((item) => {
          if (item.colorProductId === action.payload.pdpColorProductId) {
            // eslint-disable-next-line no-param-reassign
            item = {
              ...item,
              isFavorite: true,
              favoritedCount: action.payload.res && action.payload.res.favoritedCount,
            };
          }
          return item;
        });
        return outfitDetails;
      });

      return { ...state, currentOutfit: outfitDetailsMap };
    case OUTFIT_DETAIL_CONSTANTS.REMOVE_OUTFIT_ITEM_FROM_FAVORITE: {
      const outfitDetailsMapList = state.currentOutfit;
      outfitDetailsMapList.products = outfitDetailsMapList.products.map((outfitDetails) => {
        const outfitDetailsElement = outfitDetails;
        const productIdFavItemToDelete =
          outfitDetailsElement && outfitDetailsElement.generalProductId;
        outfitDetailsElement.colorFitsSizesMap =
          outfitDetailsElement &&
          outfitDetailsElement.colorFitsSizesMap.map((item) => {
            if (productIdFavItemToDelete === action.payload.productId) {
              return {
                ...item,
                isFavorite: false,
              };
            }
            return item;
          });
        return outfitDetailsElement;
      });
      return { ...state, currentOutfit: outfitDetailsMapList };
    }

    case OUTFIT_DETAIL_CONSTANTS.SET_SELECTED_OUTFIT_PRODUCT_LIST:
      // eslint-disable-next-line no-case-declarations
      const { selectedOutfitProducts } = state;
      // eslint-disable-next-line no-case-declarations
      const { outfitProduct, selectedFit, selectedSize } = action.payload;
      if (outfitProduct) {
        return setSelectedOutfitProducts(
          selectedOutfitProducts,
          outfitProduct,
          selectedFit,
          selectedSize,
          state
        );
      }
      return { ...state, selectedOutfitProducts: [] };
    case OUTFIT_DETAIL_CONSTANTS.SET_OUTFIT_CACHE:
      // eslint-disable-next-line no-case-declarations
      const { outfitCacheData } = state;
      // eslint-disable-next-line no-case-declarations
      const { payload } = action;
      // eslint-disable-next-line no-case-declarations
      const { outfitData } = payload;
      // eslint-disable-next-line no-case-declarations
      const { outfitId } = outfitData;
      return { ...state, outfitCacheData: { ...outfitCacheData, [outfitId]: { ...payload } } };
    default:
      return { ...state };
  }
};

export default OutfitDetailReducer;
