// 9fbef606107a605d69c0edbcd8029e5d
import { OUTFIT_DETAILS_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import { getLabelValue, getABtestFromState } from '../../../../../utils';
import getAddedToBagFormValues from '../../../../../reduxStore/selectors/form.selectors';

export const getLabels = (state) => {
  return state.Labels.OutfitLabels && state.Labels.OutfitLabels.Outfit_Sub;
};

export const getPDPLabels = (state) => {
  return {
    completeTheLook: getLabelValue(
      state.Labels,
      'lbl_complete_the_look',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    youMayAlsoLike: getLabelValue(
      state.Labels,
      'lbl_you_may_also_like',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    recentlyViewed: getLabelValue(
      state.Labels,
      'lbl_recently_viewed',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    outfitImageLbl: getLabelValue(state.Labels, 'lbl_outfit_image', 'accessibility', 'global'),
    previousPrice: getLabelValue(state.Labels, 'lbl_previous_price', 'accessibility', 'global'),
  };
};

export const getOutfitImage = (state) => {
  return (
    state[OUTFIT_DETAILS_REDUCER_KEY] &&
    state[OUTFIT_DETAILS_REDUCER_KEY].currentOutfit &&
    state[OUTFIT_DETAILS_REDUCER_KEY].currentOutfit.outfitImageUrl
  );
};

export const getOutfitProducts = (state) => {
  return (
    state[OUTFIT_DETAILS_REDUCER_KEY] &&
    state[OUTFIT_DETAILS_REDUCER_KEY].currentOutfit &&
    state[OUTFIT_DETAILS_REDUCER_KEY].currentOutfit.products
  );
};

export const getOutfitId = (state) => {
  return (
    state[OUTFIT_DETAILS_REDUCER_KEY] &&
    state[OUTFIT_DETAILS_REDUCER_KEY].currentOutfit &&
    state[OUTFIT_DETAILS_REDUCER_KEY].currentOutfit.outfitId
  );
};

export const getOutfitCarouselState = (state) => {
  return state[OUTFIT_DETAILS_REDUCER_KEY] && state[OUTFIT_DETAILS_REDUCER_KEY].outfitCarouselModal;
};

export const getUnavailableCount = (state) => {
  return (
    state[OUTFIT_DETAILS_REDUCER_KEY] &&
    state[OUTFIT_DETAILS_REDUCER_KEY].currentOutfit &&
    state[OUTFIT_DETAILS_REDUCER_KEY].currentOutfit.unavailableCount
  );
};

export const getAddedToBagErrorCatId = (state) => {
  return state.AddedToBagReducer.get('errorCatId');
};

export const getGeneralProductId = (state) => {
  return state.OutfitDetails.currentOutfit &&
    state.OutfitDetails.currentOutfit.products &&
    state.OutfitDetails.currentOutfit.products.length > 0
    ? state.OutfitDetails.currentOutfit &&
        state.OutfitDetails.currentOutfit.products &&
        state.OutfitDetails.currentOutfit.products[0].generalProductId
    : false;
};

export const getOutfitDetailFormValues = (state) => {
  const generalProductId = getGeneralProductId(state);
  return generalProductId
    ? getAddedToBagFormValues(state, `ProductAddToBag-${generalProductId}`)
    : '';
};

export const getLoadingState = (state) => {
  return state[OUTFIT_DETAILS_REDUCER_KEY] && state[OUTFIT_DETAILS_REDUCER_KEY].isLoading;
};

export const getOnModelImageAbTestOutfitPdp = (state) => {
  return getABtestFromState(state.AbTest).enableOnModelImageOnOutfit;
};

export const getIsOutfitAddedCTAEnabled = (state) => {
  return getABtestFromState(state.AbTest).enableOutfitAddedCTA;
};

export const getShowAddedToBagCTA = (state) => {
  return state.AddedToBagReducer && state.AddedToBagReducer.get('showAddedToBagCta');
};

export const getOutfitCarouselAddedCTA = (state) => {
  return state.AddedToBagReducer && !!state.AddedToBagReducer.get('showOutfitCarouselAddedCta');
};

export const getSelectedProductList = (state) => {
  return (
    state[OUTFIT_DETAILS_REDUCER_KEY] && state[OUTFIT_DETAILS_REDUCER_KEY].selectedOutfitProducts
  );
};

export const getOutfitCache = (state) => {
  return state[OUTFIT_DETAILS_REDUCER_KEY] && state[OUTFIT_DETAILS_REDUCER_KEY].outfitCacheData;
};

export const getEntireFormValue = (state) => {
  return state?.form;
};
