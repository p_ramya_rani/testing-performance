// 9fbef606107a605d69c0edbcd8029e5d
import { loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import { SEO_DATA } from '@tcp/core/src/reduxStore/constants';
import { isMobileApp } from '../../../../../utils';
import OUTFIT_DETAILS_CONSTANTS from './OutfitDetails.constants';

export const getOutfitDetails = (payload) => {
  return {
    type: OUTFIT_DETAILS_CONSTANTS.FETCH_OUTFIT_PRODUCTS,
    payload,
  };
};

export const setCurrentOutfitAction = (currentOutfit) => {
  return {
    currentOutfit,
    type: OUTFIT_DETAILS_CONSTANTS.SET_OUTFIT_PRODUCTS,
  };
};

export const setAddToFavoriteOUTFIT = (payload) => {
  return {
    type: OUTFIT_DETAILS_CONSTANTS.SET_ADD_TO_FAVORITE,
    payload,
  };
};

export const removeOutfitFavoriteItemAction = (payload) => {
  return {
    type: OUTFIT_DETAILS_CONSTANTS.REMOVE_OUTFIT_ITEM_FROM_FAVORITE,
    payload,
  };
};
export const setLoadingState = (payload) => {
  return {
    type: OUTFIT_DETAILS_CONSTANTS.SET_LOADING_STATE,
    payload,
  };
};

export const resetOutfitDetails = () => ({
  type: OUTFIT_DETAILS_CONSTANTS.RESET_OUTFIT_PRODUCTS,
});

export const clearOutfitDetails = () => ({
  type: OUTFIT_DETAILS_CONSTANTS.CLEAR_OUTFIT_PRODUCTS,
});

export const setOutfitCarouselModalState = (payload) => ({
  type: OUTFIT_DETAILS_CONSTANTS.SET_OUTFIT_CAROUSEL_STATE,
  payload,
});

export const initActions = isMobileApp()
  ? [loadPageSEOData({ page: SEO_DATA.outfit })]
  : [(args) => loadPageSEOData({ page: SEO_DATA.outfit, ...args })];

export const setSelectedOutfitProductList = (payload) => {
  return {
    type: OUTFIT_DETAILS_CONSTANTS.SET_SELECTED_OUTFIT_PRODUCT_LIST,
    payload,
  };
};

export const setOutfitCacheData = (payload) => {
  return {
    type: OUTFIT_DETAILS_CONSTANTS.SET_OUTFIT_CACHE,
    payload,
  };
};
