/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import withIsomorphicRenderer from '@tcp/core/src/components/common/hoc/withIsomorphicRenderer';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import { PropTypes } from 'prop-types';
import { getDefaultStore } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getFavoriteStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { getCurrencyAttributes } from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import OutfitProductSkeleton from '../molecules/OutfitProductSkeleton';
import OutfitDetails from '../views/index';
import { trackPageView, setClickAnalyticsData } from '../../../../../analytics/actions';
import {
  getLabels,
  getOutfitImage,
  getOutfitProducts,
  getAddedToBagErrorCatId,
  getPDPLabels,
  getUnavailableCount,
  getLoadingState,
  getOnModelImageAbTestOutfitPdp,
  getIsOutfitAddedCTAEnabled,
  getShowAddedToBagCTA,
  getOutfitDetailFormValues,
  getOutfitId,
  getSelectedProductList,
  getOutfitCache,
  getEntireFormValue,
} from './OutfitDetails.selectors';
import {
  getOutfitDetails,
  resetOutfitDetails,
  initActions,
  setSelectedOutfitProductList,
} from './OutfitDetails.actions';
import {
  getPlpLabels,
  getPDPTopPromos,
} from '../../ProductDetail/container/ProductDetail.selectors';
import { isCanada, isMobileApp } from '../../../../../utils';
import {
  isPlccUser,
  getUserLoggedInState,
  isRememberedUser,
} from '../../../account/User/container/User.selectors';
import {
  getIsInternationalShipping,
  getCurrentCurrency,
  getIsKeepAliveProduct,
  getIsKeepAliveProductApp,
  getIsDynamicBadgeEnabled,
  getMultiPackThreshold,
  getIsNewReDesignEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import {
  getLabelsOutOfStock,
  getExternalCampaignFired,
} from '../../ProductListing/container/ProductListing.selectors';
import { getIsPickupModalOpen } from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { getCartItemInfo } from '../../../CnC/AddedToBag/util/utility';
import {
  addToCartEcomNewDesign,
  addToCartEcom,
  addMultipleItemsToCartEcom,
  addMultipleItemsToCartEcomNew,
  clearAddToBagErrorState,
} from '../../../CnC/AddedToBag/container/AddedToBag.actions';
import {
  getAddedToBagError,
  getMultipleItemsAddedToBagError,
} from '../../../CnC/AddedToBag/container/AddedToBag.selectors';
import getAddedToBagFormValues from '../../../../../reduxStore/selectors/form.selectors';
import { OUTFIT_LISTING_FORM } from '../../../../../constants/reducer.constants';
import {
  removeAddToFavoriteErrorState,
  addItemsToWishlist,
  setLastDeletedItemIdAction,
} from '../../Favorites/container/Favorites.actions';
import {
  fetchAddToFavoriteErrorMsg,
  selectActiveWishList,
  wishListFromState,
  getDefaultFavItemInProgressFlag,
  fetchErrorMessages,
} from '../../Favorites/container/Favorites.selectors';
import { setExternalCampaignState } from '../../../../common/molecules/Loader/container/Loader.actions';
import {
  getStyliticsProductTabListSelector,
  getLabel,
  getCTLImageABTest,
} from '../../../../common/molecules/ModuleQ/container/ModuleQ.selector';
import getLoadData from '../util';

/* eslint-disable max-len */
class OutfitDetailsContainer extends React.PureComponent {
  static pageProps = {
    pageData: {
      pageName: 'outfit',
    },
  };

  static getInitialProps = async ({ props, query, isServer, apiConfig }) => {
    const { getOutfit, navigation } = props;
    if (isMobileApp()) {
      const vendorColorProductIdsList = navigation.getParam('vendorColorProductIdsList');
      const outfitId = navigation.getParam('outfitId');
      await getOutfit({ outfitId, vendorColorProductIdsList, fromOutFitPage: true });
    } else {
      let vendorColorProductIdsList;
      let outfitId;
      if (isServer) {
        ({ vendorColorProductIdsList, outfitId } = query);
      } else {
        ({
          router: {
            query: { vendorColorProductIdsList, outfitId },
          },
        } = props);
      }
      await getOutfit({ outfitId, vendorColorProductIdsList, fromOutFitPage: true, apiConfig });
    }
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      router: {
        query: { outfitId: selectedOutfitId },
      },
    } = nextProps;
    return selectedOutfitId ? { ...prevState, outfitIdLocal: selectedOutfitId } : { ...prevState };
  }

  constructor(props) {
    super(props);
    this.state = {
      outfitIdLocal: '',
    };
  }

  componentWillUnmount() {
    const { resetOutFitProductState } = this.props;
    resetOutFitProductState();
  }

  getCartInfo = (addToBagEcom, productInfo, generalProductId, currentState, isSBP, navigation) => {
    const { isEnabledOutfitAddedCTA, priceCurrency } = this.props;
    const {
      newPartNumber: multipackProduct,
      partIdDetail: partIdInfo,
      multiPackCount,
      getMultiPackAllColor: getAllMultiPack,
      getNewMultiPackAllColor: getAllNewMultiPack,
      selectedMultipack,
      listPrice,
      ratings,
      reviewsCount,
      ratingsProductId,
    } = productInfo;
    const {
      fromPage = '',
      outfitId = '',
      selectedProductId = '',
      viaModule = '',
      title = '',
      brand = '',
      selectedCategoryId = '',
    } = navigation?.state?.params || {};
    // RWD-16438 Fix: Same form name is removed if user go from outfit to PDP and then press back button.
    const formName = `${OUTFIT_LISTING_FORM}-${generalProductId}`;
    const formValues = getAddedToBagFormValues(currentState, formName);
    let cartItemInfo = getCartItemInfo(productInfo, formValues);
    cartItemInfo = {
      ...cartItemInfo,
      skipAddToBagModal: isEnabledOutfitAddedCTA,
      multipackProduct,
      multiPackCount,
      getAllMultiPack,
      getAllNewMultiPack,
      selectedMultipack,
      partIdInfo,
      selectedCategoryId,
      listPrice,
      ratings,
      reviewsCount,
      ratingsProductId,
      fromPage,
      outfitId,
      selectedProductId,
      viaModule,
      brand,
      title,
      priceCurrency,
    };
    cartItemInfo.addedFromSBP = isSBP;
    return cartItemInfo;
  };

  handleAddToBag = (addToBagEcom, productInfo, generalProductId, currentState, isSBP = false) => {
    const { addToBagEcomNew, isNewReDesign } = this.props;
    const cartItemInfo = this.getCartInfo(
      isNewReDesign && isMobileApp() ? addToBagEcomNew : addToBagEcom,
      productInfo,
      generalProductId,
      currentState,
      isSBP
    );
    cartItemInfo.primaryBrand = productInfo && productInfo.primaryBrand;
    if (isMobileApp() && isNewReDesign) {
      const cartItemInfoUpdated = {
        ...cartItemInfo,
        isOpenNewATBModal: true,
      };
      addToBagEcomNew(cartItemInfoUpdated);
    } else {
      addToBagEcom(cartItemInfo);
    }
  };

  handleMultipleAddToBag = (
    addMultipleToBagEcom,
    productList,
    currentState,
    isSBP = false,
    navigation
  ) => {
    const { addMultipleToBagEcomNew, isNewReDesign } = this.props;
    if (productList.length === 1) {
      const { outfitProduct } = productList[0];
      const { generalProductId } = outfitProduct;
      const { addToBagEcom } = this.props;
      this.handleAddToBag(addToBagEcom, outfitProduct, generalProductId, currentState);
      return;
    }
    const productItemsInfo = productList.map((productInfo) => {
      const { outfitProduct } = productInfo;
      const { generalProductId } = outfitProduct;
      return this.getCartInfo(
        isNewReDesign && isMobileApp() ? addMultipleToBagEcomNew : addMultipleToBagEcom,
        outfitProduct,
        generalProductId,
        currentState,
        isSBP,
        navigation
      );
    });
    if (isMobileApp() && isNewReDesign) {
      addMultipleToBagEcomNew({ productItemsInfo });
    } else {
      addMultipleToBagEcom({ productItemsInfo });
    }
  };

  checkIfSBP = (selectActiveWishListFromState) => {
    const { navigation } = this.props;
    return navigation && selectActiveWishListFromState && navigation.getParam('isSBP');
  };

  checkInWishlist = (wishList, productId) =>
    wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;

  render() {
    const {
      labels,
      outfitImageUrl,
      unavailableCount,
      outfitProducts,
      plpLabels,
      isPlcc,
      isInternationalShipping,
      priceCurrency,
      currencyAttributes,
      addToBagEcom,
      addToFavorites,
      currentState,
      addToBagError,
      addToBagErrorId,
      multipleAddToBagError,
      isPickupModalOpen,
      isLoggedIn,
      navigation,
      pdpLabels,
      toastMessage,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      topPromos,
      router: { asPath: asPathVal } = {},
      isKeepAliveEnabled,
      outOfStockLabels,
      isLoading,
      trackPageLoad,
      isOnModelAbTestOutfitPdp,
      storeId,
      router: {
        query: { viaModule, viaPage },
      },
      isEnabledOutfitAddedCTA,
      showAddedToBagCta,
      isDynamicBadgeEnabled,
      multiPackThreshold,
      selectActiveWishListFromState,
      defaultWishListFromState,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      errorMessages,
      completeFormValues,
      ...otherProps
    } = this.props;

    const { outfitIdLocal } = this.state;
    const isApp = isMobileApp();
    const displayOutfitPDP = isApp ? true : !isLoading && outfitProducts;

    const isSBP = isApp ? this.checkIfSBP(selectActiveWishListFromState) : null;
    let wishList = defaultWishListFromState;
    // Checking if the current flow is SBP.
    if (isSBP) {
      const wishListSBP = {};
      /**
       * The defaultWishListFromState has a certain structure. When its the SBP flow then we need to
       * transform active wish list to the same structure so that the Image carousel component's
       *  actions perform well.
       */
      selectActiveWishListFromState.items.map((itm) => {
        wishListSBP[itm.skuInfo.color.imagePath] = {
          isInDefaultWishlist: selectActiveWishListFromState.isDefault,
          externalIdentifier: selectActiveWishListFromState.id,
          giftListItemID: itm.itemInfo.itemId,
        };
        return null;
      });
      wishList = wishListSBP;
    }
    const multiErrorMessage = multipleAddToBagError && multipleAddToBagError.errMsg;

    return (
      <React.Fragment>
        {displayOutfitPDP ? (
          <OutfitDetails
            labels={labels}
            outfitImageUrl={outfitImageUrl}
            unavailableCount={unavailableCount}
            outfitProducts={outfitProducts}
            plpLabels={plpLabels}
            isCanada={isCanada()}
            isPlcc={isPlcc}
            isInternationalShipping={isInternationalShipping}
            currencySymbol={priceCurrency}
            currencyAttributes={currencyAttributes}
            handleAddToBag={this.handleAddToBag}
            handleMultipleAddToBag={this.handleMultipleAddToBag}
            addToBagEcom={addToBagEcom}
            currentState={currentState}
            addToBagError={addToBagError}
            addToBagErrorId={addToBagErrorId}
            multipleAddToBagError={multiErrorMessage}
            isPickupModalOpen={isPickupModalOpen}
            addToFavorites={addToFavorites}
            isLoggedIn={isLoggedIn}
            navigation={navigation}
            outfitId={outfitIdLocal}
            pdpLabels={pdpLabels}
            toastMessage={toastMessage}
            AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
            removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
            asPathVal={asPathVal}
            topPromos={topPromos}
            isKeepAliveEnabled={isKeepAliveEnabled}
            outOfStockLabels={outOfStockLabels}
            trackPageLoad={trackPageLoad}
            isOnModelAbTestOutfitPdp={isOnModelAbTestOutfitPdp}
            storeId={storeId}
            viaModule={viaModule}
            viaPage={viaPage}
            isEnabledOutfitAddedCTA={isEnabledOutfitAddedCTA}
            showAddedToBagCta={showAddedToBagCta}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            isLoading={isLoading}
            {...otherProps}
            multiPackThreshold={multiPackThreshold}
            defaultWishListFromState={wishList}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            isOutfitPage
            checkInWishlist={this.checkInWishlist}
            wishList={defaultWishListFromState}
            deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
            errorMessages={errorMessages}
            completeFormValues={completeFormValues}
          />
        ) : null}
        {!isApp && isLoading ? <OutfitProductSkeleton /> : null}
      </React.Fragment>
    );
  }
}

OutfitDetailsContainer.pageInfo = {
  pageId: 'outfit',
  pageData: {
    pageName: 'outfit',
    pageSection: 'outfit',
    pageSubSection: 'outfit',
    loadAnalyticsOnload: false,
  },
};

OutfitDetailsContainer.getInitActions = () => initActions;

const mapStateToProps = (state) => {
  return {
    labels: getLabels(state),
    outfitImageUrl: getOutfitImage(state),
    isLoading: getLoadingState(state),
    unavailableCount: getUnavailableCount(state),
    outfitProducts: getOutfitProducts(state),
    outfitItemId: getOutfitId(state),
    plpLabels: getPlpLabels(state),
    isCanada: isCanada(),
    isPlcc: isPlccUser(state),
    isInternationalShipping: getIsInternationalShipping(state),
    priceCurrency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    addToBagError: getAddedToBagError(state),
    addToBagErrorId: getAddedToBagErrorCatId(state),
    multipleAddToBagError: getMultipleItemsAddedToBagError(state),
    currentState: state,
    isPickupModalOpen: getIsPickupModalOpen(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    pdpLabels: getPDPLabels(state),
    formValues: getOutfitDetailFormValues(state),
    AddToFavoriteErrorMsg: fetchAddToFavoriteErrorMsg(state),
    topPromos: getPDPTopPromos(state),
    isKeepAliveEnabled: isMobileApp()
      ? getIsKeepAliveProductApp(state)
      : getIsKeepAliveProduct(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    isOnModelAbTestOutfitPdp: getOnModelImageAbTestOutfitPdp(state),
    storeId: getDefaultStore(state) || getFavoriteStore(state),
    isEnabledOutfitAddedCTA: !isCanada() && getIsOutfitAddedCTAEnabled(state),
    showAddedToBagCta: getShowAddedToBagCTA(state),
    isExternalCampaignFired: getExternalCampaignFired(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    styliticsProductTabList: getStyliticsProductTabListSelector(state),
    shopThisLookLabel: getLabel(state),
    showSmallImage: getCTLImageABTest(state),
    multiPackThreshold: getMultiPackThreshold(state),
    selectedOutfitProducts: getSelectedProductList(state),
    outfitCache: getOutfitCache(state),
    defaultWishListFromState: wishListFromState(state),
    selectActiveWishListFromState: selectActiveWishList(state),
    deleteFavItemInProgressFlag: getDefaultFavItemInProgressFlag(state),
    errorMessages: fetchErrorMessages(state),
    completeFormValues: getEntireFormValue(state),
    isNewReDesign: getIsNewReDesignEnabled(state),
  };
};

function mapDispatchToProps(dispatch) {
  return {
    getOutfit: (payload) => {
      dispatch(getOutfitDetails(payload));
    },
    addToBagEcom: (payload) => {
      dispatch(addToCartEcom(payload));
    },
    addToBagEcomNew: (payload) => {
      dispatch(addToCartEcomNewDesign(payload));
    },
    clearAddToBagError: () => {
      dispatch(clearAddToBagErrorState());
    },
    addToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    removeAddToFavoritesErrorMsg: (payload) => {
      dispatch(removeAddToFavoriteErrorState(payload));
    },
    resetOutFitProductState: () => {
      dispatch(resetOutfitDetails());
    },
    setExternalCampaignFired: (payload) => {
      dispatch(setExternalCampaignState(payload));
    },
    trackPageLoad: (payload, customdata) => {
      const isApp = isMobileApp();
      const { eventSetData, loadData } = getLoadData(isApp, payload, customdata);
      dispatch(setClickAnalyticsData(eventSetData));
      const dispatchTimer = setTimeout(() => {
        dispatch(trackPageView(loadData));
        const analyticsTimer = setTimeout(() => {
          dispatch(setClickAnalyticsData({}));
          clearTimeout(analyticsTimer);
        }, 200);
        clearTimeout(dispatchTimer);
      }, 100);
    },
    trackViewAction: (data) => {
      dispatch(trackPageView(data));
    },
    setClickAnalyticsDataAction: (data) => {
      dispatch(setClickAnalyticsData(data));
    },
    setSelectedOutfitProduct: (payload) => {
      dispatch(setSelectedOutfitProductList(payload));
    },
    addMultipleToBagEcom: (payload) => {
      dispatch(addMultipleItemsToCartEcom(payload));
    },
    addMultipleToBagEcomNew: (payload) => {
      dispatch(addMultipleItemsToCartEcomNew(payload));
    },
    onSetLastDeletedItemIdAction: (payload) => {
      dispatch(setLastDeletedItemIdAction(payload));
    },
  };
}

OutfitDetailsContainer.propTypes = {
  labels: PropTypes.shape({}),
  outfitImageUrl: PropTypes.string,
  unavailableCount: PropTypes.number,
  outfitProducts: PropTypes.shape({}),
  router: PropTypes.shape({
    query: PropTypes.shape({}),
  }),
  plpLabels: PropTypes.shape({}),
  isPlcc: PropTypes.bool,
  isInternationalShipping: PropTypes.bool,
  priceCurrency: PropTypes.string,
  currencyAttributes: PropTypes.shape({}),
  addToBagEcom: PropTypes.func.isRequired,
  currentState: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}),
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
  isPickupModalOpen: PropTypes.bool,
  addToFavorites: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  toastMessage: PropTypes.func,
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  trackPageLoad: PropTypes.func,
  topPromos: PropTypes.string,
  isLoading: PropTypes.bool,
  isOnModelAbTestOutfitPdp: PropTypes.bool,
  resetOutFitProductState: PropTypes.func,
  storeId: PropTypes.string,
  isEnabledOutfitAddedCTA: PropTypes.bool,
  showAddedToBagCta: PropTypes.string,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  multiPackThreshold: PropTypes.string,
  selectActiveWishListFromState: PropTypes.shape([]),
  defaultWishListFromState: PropTypes.shape({}),
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  errorMessages: PropTypes.shape({}),
  completeFormValues: PropTypes.shape({}),
  multipleAddToBagError: PropTypes.string,
  isNewReDesign: PropTypes.bool,
  addToBagEcomNew: PropTypes.func.isRequired,
};

OutfitDetailsContainer.defaultProps = {
  labels: {},
  outfitImageUrl: '',
  unavailableCount: 0,
  outfitProducts: null,
  router: {
    query: {},
  },
  navigation: {},
  resetOutFitProductState: () => {},
  plpLabels: {},
  isPlcc: false,
  isInternationalShipping: false,
  priceCurrency: 'USD',
  currencyAttributes: { exchangevalue: 1 },
  addToBagError: '',
  addToBagErrorId: '',
  isPickupModalOpen: false,
  isLoggedIn: false,
  pdpLabels: {},
  trackPageLoad: () => {},
  toastMessage: () => {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  topPromos: '',
  isLoading: false,
  isOnModelAbTestOutfitPdp: false,
  storeId: '',
  isEnabledOutfitAddedCTA: false,
  showAddedToBagCta: '',
  isDynamicBadgeEnabled: false,
  multiPackThreshold: '',
  selectActiveWishListFromState: [],
  defaultWishListFromState: {},
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  errorMessages: {},
  completeFormValues: {},
  multipleAddToBagError: '',
  isNewReDesign: false,
};

export default withIsomorphicRenderer({
  WrappedComponent: OutfitDetailsContainer,
  mapStateToProps,
  mapDispatchToProps,
});
