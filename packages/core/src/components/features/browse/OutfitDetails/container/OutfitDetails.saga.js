// 9fbef606107a605d69c0edbcd8029e5d
import { loadLayoutData, loadModulesData } from '@tcp/core/src/reduxStore/actions';
import styliticsProductListing from '@tcp/core/src/services/abstractors/styliticsProductListing';
import { call, put, takeLatest, select } from 'redux-saga/effects';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import { getStringAfterSplit, getMultipackCount, filterMultiPack } from '@tcp/core/src/utils/utils';
import OUTFIT_DETAILS_CONSTANTS from './OutfitDetails.constants';
import {
  setCurrentOutfitAction,
  setLoadingState,
  setOutfitCacheData,
} from './OutfitDetails.actions';
import { getDefaultWishList } from '../../Favorites/container/Favorites.saga';
import getOutfitProdutsDetails from '../../../../../services/abstractors/productListing/outfitDetails';
import {
  getIsPdpServiceEnabled,
  getABTestPdpService,
} from '../../../../../reduxStore/selectors/session.selectors';
import { getOutfitCache } from './OutfitDetails.selectors';
import logger from '../../../../../utils/loggerInstance';
import getProductInfoById, {
  layoutResolver,
} from '../../../../../services/abstractors/productListing/productDetail';

const checkInCache = (outfitCache, outfitId) => {
  const keys = Object.keys(outfitCache);
  let outfitCachedData;
  keys.forEach(key => {
    if (key === outfitId.toString()) {
      outfitCachedData = outfitCache[key];
    }
  });
  const { products, outfitData } = outfitCachedData || {};
  return { outfitData, products };
};

function* loadOutfitProducts(products, apiConfig, pageName) {
  if (products && products.length > 0) {
    const category = products[0] && products[0].category;
    const { layout, modules } = yield call(layoutResolver, { category, pageName, apiConfig });
    yield put(loadLayoutData(layout, pageName));
    yield put(loadModulesData(modules));
  }
}

// eslint-disable-next-line
function* loadOutfitDetails({
  payload: { outfitId, vendorColorProductIdsList, fromOutFitPage, apiConfig, fromOutfitCarousel },
}) {
  try {
    const outfitCache = yield select(getOutfitCache);
    const pageName = 'pdp';
    const { outfitData, products: cacheProducts } = checkInCache(outfitCache, outfitId);
    if (!outfitData || !fromOutfitCarousel) {
      const state = yield select();
      const xappURLConfig = yield call(getUnbxdXappConfigs);
      let resurl;
      const navigationTree = state.Navigation.navigationData;
      yield put(setLoadingState({ isLoading: true }));
      if (fromOutFitPage) {
        resurl = yield call(styliticsProductListing.getOutfitData, outfitId);
      }
      const getUrl = fromOutFitPage ? resurl.largeImageUrl : '';
      const isPdpProductServiceEnabled = yield select(getIsPdpServiceEnabled);
      const enablePDPServiceABTest = yield select(getABTestPdpService);
      const res = yield call(
        getOutfitProdutsDetails,
        {
          outfitId,
          vendorColorProductIdsList,
          getImgPath: () => {},
          navigationTree,
          state,
          largeImgUrl: getUrl,
        },
        xappURLConfig,
        isPdpProductServiceEnabled,
        enablePDPServiceABTest
      );

      const { products } = res;

      let outfitProductItemPartId;
      let partId;
      const productsLength = products ? products.length : 0;

      for (let i = 0; i < productsLength; i += 1) {
        const outfitProductItem = products[i];
        if (!!outfitProductItem && outfitProductItem.multiPackUSStore) {
          let multiplePartId = '';
          partId = outfitProductItem.tcpMultiPackReferenceUSStore.join(',').split(',');
          const updatedPartId = new Set(partId);
          const findUpdatedPartId = [...updatedPartId];
          const findUpdatedPartIdLength = findUpdatedPartId ? findUpdatedPartId.length : 0;

          for (let id = 0; id < findUpdatedPartIdLength; id += 1) {
            if (findUpdatedPartId[id].includes(',')) {
              const arrayOfPartNum = getStringAfterSplit(findUpdatedPartId[id], ',');
              const arrayOfPartNumLength = arrayOfPartNum ? arrayOfPartNum.length : 0;

              for (let num = 0; num < arrayOfPartNumLength; num += 1) {
                multiplePartId = multiplePartId.concat(
                  getStringAfterSplit(arrayOfPartNum[num], '#')[0]
                );
                if (num !== arrayOfPartNumLength - 1) {
                  multiplePartId = multiplePartId.concat(',');
                }
              }
            } else {
              outfitProductItemPartId = [getStringAfterSplit(findUpdatedPartId[id], '#')[0]];
              multiplePartId += `${outfitProductItemPartId},`;
            }
            const newProductDetail = yield call(
              getProductInfoById,
              xappURLConfig,
              multiplePartId,
              state,
              undefined,
              undefined,
              false,
              '',
              true,
              partId,
              undefined,
              undefined,
              isPdpProductServiceEnabled,
              enablePDPServiceABTest
            );

            if (!!outfitProductItem && outfitProductItem.multiPackUSStore) {
              const newMultiPackCount =
                outfitProductItem &&
                outfitProductItem.multiPackUSStore &&
                outfitProductItem.tcpMultiPackReferenceUSStore &&
                outfitProductItem.tcpMultiPackReferenceUSStore[0].split(',');
              const multiPackCount = newMultiPackCount ? getMultipackCount(newMultiPackCount) : 0;

              if (newProductDetail) {
                const getFilterData = newProductDetail.product.categoryIdList;
                const getmultipackProduct = getFilterData && filterMultiPack(getFilterData);
                products[i].newPartNumber = getmultipackProduct;
                products[i].multiPackCount = multiPackCount;
                products[i].getMultiPackAllColor = outfitProductItem.getMultiPackAllColor;
                products[i].getNewMultiPackAllColor = newProductDetail.product.getMultiPackAllColor;
                products[i].partIdDetail = newProductDetail.product.partIdDetails;
              } else {
                products[i].newPartNumber = null;
                products[i].multiPackCount = null;
                products[i].getMultiPackAllColor = null;
              }
            }
          }
        }
      }

      yield put(setCurrentOutfitAction({ ...res }));
      yield call(getDefaultWishList);
      yield put(setOutfitCacheData({ outfitData: { ...res }, products }));
      yield put(setLoadingState({ isLoading: false }));
      loadOutfitProducts(products, apiConfig, pageName);
    } else {
      yield put(setCurrentOutfitAction({ ...outfitData }));
      loadOutfitProducts(cacheProducts, apiConfig, pageName);
    }
  } catch (err) {
    logger.error('error: ', err);
    yield put(setLoadingState({ isLoading: false }));
  }
}

function* OutfitDetailsSaga() {
  yield takeLatest(OUTFIT_DETAILS_CONSTANTS.FETCH_OUTFIT_PRODUCTS, loadOutfitDetails);
}

export default OutfitDetailsSaga;
