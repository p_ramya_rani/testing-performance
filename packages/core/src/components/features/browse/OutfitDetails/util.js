import { OUTFIT_LISTING_FORM } from '../../../../constants/reducer.constants';

const getLoadData = (isApp, payload, customdata) => {
  let eventSetData = {};
  let loadData = {};
  if (isApp) {
    loadData = payload;
    eventSetData = customdata;
  }
  if (!isApp) {
    const { products, internalCampaignId } = payload;
    eventSetData = { products, internalCampaignId };
    loadData = {
      props: {
        initialProps: {
          pageProps: {
            pageData: {
              ...payload,
            },
          },
        },
      },
    };
  }
  return {
    eventSetData,
    loadData,
  };
};

export const getTotalQuantityOfProducts = (completeFormValues, selectedOutfitProducts) => {
  if (selectedOutfitProducts.length === 0) return 0;
  const selectedProductInForm = [];
  selectedOutfitProducts.forEach(({ outfitProduct: { generalProductId } }) => {
    const formName = `${OUTFIT_LISTING_FORM}-${generalProductId}`;
    selectedProductInForm.push(completeFormValues[formName]);
  });

  return selectedProductInForm.reduce(
    (accumulator, item) => accumulator + item?.values?.Quantity,
    0
  );
};

export default getLoadData;
