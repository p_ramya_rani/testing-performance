/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import isEqual from 'lodash/isEqual';
import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import { PropTypes } from 'prop-types';
import * as labelsSelectors from '@tcp/core/src/reduxStore/selectors/labels.selectors';
import {
  getIsKeepAliveProductApp,
  getIsShowPriceRangeForApp,
  getABTestIsShowPriceRange,
  getChangeFilterViewABTest,
  getIsBrierleyPromoEnabled,
  getIsDynamicBadgeEnabled,
  getIsNewReDesignEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { TIER1 } from '@tcp/core/src/constants/tiering.constants';
import { trackPageView, setClickAnalyticsData, trackClick } from '../../../../../analytics/actions';
import SearchDetail from '../views/SearchDetail.view';
import {
  getSlpProducts,
  getMoreSlpProducts,
  resetSlpProducts,
  setSlpResultsAvailableState,
  setDeviceTier,
  setSlpLoadingState,
} from './SearchDetail.actions';
import getSortLabels from '../../ProductListing/molecules/SortSelector/views/Sort.selectors';
import {
  openQuickViewWithValues,
  setQuickViewProductDetailPageTitle,
} from '../../../../common/organisms/QuickViewModal/container/QuickViewModal.actions';
import {
  addItemsToWishlist,
  setLastDeletedItemIdAction,
} from '../../Favorites/container/Favorites.actions';
import {
  getUnbxdId,
  getCategoryId,
  getLabelsAccountOverView,
  getNavigationTreeApp,
  getLongDescription,
  getLastLoadedPageNumber,
  getIsHidePLPAddToBag,
  getIsHidePLPRatings,
  getSelectedFilter,
  getLabelsOutOfStock,
  getExternalCampaignFired,
  getAccessibilityLabels,
} from '../../ProductListing/container/ProductListing.selectors';
import {
  fetchErrorMessages,
  wishListFromState,
  getDefaultFavItemInProgressFlag,
} from '../../Favorites/container/Favorites.selectors';
import { setFilter } from '../../ProductListing/container/ProductListing.actions';
import {
  getLoadedProductsCount,
  getTotalProductsCount,
  getCurrentSearchForText,
  getLabels,
  getAppliedFilters,
  getAppliedSortId,
  getIsLoadingMore,
  checkIfSearchResultsAvailable,
  getAllProductsSelect,
  updateAppliedFiltersInState,
  getScrollToTopValue,
  getPDPLabels,
  getModalState,
  getPLPGridPromos,
  getPlpHorizontalPromo,
  getIsDataLoading,
  getRedirectObj,
  getSLPTopPromos,
  getLabelsProductListing,
  getAlternateBrandName,
} from './SearchDetail.selectors';

import NoResponseSearchDetail from '../views/NoResponseSearchDetail.view';
import {
  setRecentSearch,
  clearRecentSearch,
} from '../../../../common/organisms/SearchProduct/RecentSearch.actions';
import {
  getUserLoggedInState,
  isRememberedUser,
  isPlccUser,
} from '../../../account/User/container/User.selectors';
import { getProductsWithPromo } from '../../ProductListing/container/ProductListing.util';
import { PLPSkeleton } from '../../../../common/atoms';
import PurchaseGiftsCardContainer from '../../../account/PurchaseGiftsCard';
import { setExternalCampaignState } from '../../../../common/molecules/Loader/container/Loader.actions';

class SearchDetailContainer extends React.Component {
  constructor(props) {
    super(props);
    const { resetProducts, resetResults, navigation, setDeviceTierInState } = this.props;
    resetProducts();
    resetResults();
    let defaultDeviceTier = TIER1;
    this.deviceTier = defaultDeviceTier;
    if (navigation) {
      const { deviceTier } = navigation.getScreenProps();
      defaultDeviceTier = deviceTier;
    }
    this.deviceTier = defaultDeviceTier;
    setDeviceTierInState(this.deviceTier);
  }

  componentDidMount() {
    const { navigation, setSelectedFilter, setSlpLoadingStateForAlternateBrand } = this.props;
    setSlpLoadingStateForAlternateBrand({ isSLPPage: true });
    const title = navigation.getParam('title');
    setSelectedFilter({});
    this.makeApiCall(title);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentWillUpdate = (nextProps) => {
    const { navigation } = nextProps;
    const title = navigation.getParam('title');
    const isForceUpdate = navigation.getParam('isForceUpdate');
    if (isForceUpdate) {
      this.makeApiCall(title);
    }
  };

  componentWillUnmount() {
    const { resetProducts, resetResults } = this.props;
    resetProducts();
    resetResults();
  }

  makeApiCall = (searchQuery) => {
    const { getProducts, setRecentSearches, navigation, changeFilterViewABTest } = this.props;
    if (this.searchQuery !== searchQuery) {
      this.searchQuery = searchQuery;
      const splitAsPathBy = `/search/${this.searchQuery}?`;
      this.asPath = `/us/search/${this.searchQuery}`;
      const queryString = this.asPath.split(splitAsPathBy);
      const filterSortString = (queryString.length && queryString[1]) || '';
      const formValues = { sort: '' }; // TODO
      if (this.searchQuery.length > 0) {
        setRecentSearches(this.searchQuery);
      }
      getProducts({
        URI: 'search',
        asPath: filterSortString,
        searchQuery,
        ignoreCache: true,
        formValues,
        url: this.asPath,
        navigation,
        isFilterAbTestOn: changeFilterViewABTest,
      });
    }
  };

  onGoToPDPPage = (title, pdpUrl, selectedColorProductId, productInfo) => {
    const { navigation } = this.props;
    const { bundleProduct } = productInfo;
    const routeName = bundleProduct ? 'BundleDetail' : 'ProductDetail';
    navigation.navigate(routeName, {
      title,
      pdpUrl,
      selectedColorProductId,
      reset: true,
      clickOrigin: 'search',
    });
  };

  onLoadMoreProducts = () => {
    const { getMoreProducts } = this.props;
    getMoreProducts({ URI: 'search', url: this.asPath, ignoreCache: true });
  };

  onSubmitFilters = (formData, submit, getProducts) => {
    const { changeFilterViewABTest } = this.props;
    const data = {
      URI: 'search',
      ignoreCache: true,
      url: this.asPath,
      sortBySelected: true,
      formData,
      scrollToTop: true,
      isKeepModalOpen: true,
      isFilterAbTestOn: changeFilterViewABTest,
    };
    getProducts(data);
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1000;
  };

  onScrollHandler = ({ nativeEvent }) => {
    const { loadedProductCount, totalProductsCount, isLoadingMore } = this.props;
    if (
      loadedProductCount < totalProductsCount &&
      this.isCloseToBottom(nativeEvent) &&
      !isLoadingMore
    ) {
      this.onLoadMoreProducts();
    }
  };

  isRedirectSearch = (redirect) => {
    return redirect && redirect.value;
  };

  isShowSkeleton = (isDataLoading, isKeepModalOpen) => isDataLoading && !isKeepModalOpen;

  render() {
    const {
      formValues,
      products,
      currentNavIds,
      navTree,
      breadCrumbs,
      filters,
      totalProductsCount,
      filtersLength,
      initialValues,
      longDescription,
      labels,
      isLoadingMore,
      lastLoadedPageNumber,
      labelsFilter,
      categoryId,
      getProducts,
      onSubmit,
      onPickUpOpenClick,
      slpLabels,
      searchResultSuggestions,
      sortLabels,
      isSearchResultsAvailable,
      searchedText,
      onAddItemToFavorites,
      isLoggedIn,
      labelsLogin,
      navigation,
      pdpLabels,
      isKeepModalOpen,
      isDataLoading,
      redirect,
      defaultWishListFromState,
      searchTopPromos,
      isPlcc,
      accessibilityLabels,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      isNewReDesignEnabled,
      alternateBrand,
      ...otherProps
    } = this.props;
    if (this.isShowSkeleton(isDataLoading, isKeepModalOpen)) return <PLPSkeleton col={4} />;
    if (this.isRedirectSearch(redirect)) {
      return <PurchaseGiftsCardContainer navigation={navigation} />;
    }
    return (
      <React.Fragment>
        <View>
          {(isSearchResultsAvailable || isKeepModalOpen) &&
            ((this.searchQuery && products && products.length > 0) ||
            (filtersLength && Object.keys(filtersLength).length > 0) ? (
              <SearchDetail
                margins="0 12px 0 12px"
                filters={filters}
                formValues={formValues}
                filtersLength={filtersLength}
                getProducts={getProducts}
                isLoadingMore={isLoadingMore}
                initialValues={initialValues}
                onSubmit={this.onSubmitFilters}
                products={products}
                totalProductsCount={totalProductsCount}
                labels={labels}
                labelsFilter={labelsFilter}
                slpLabels={slpLabels}
                searchedText={searchedText}
                sortLabels={sortLabels}
                searchResultSuggestions={searchResultSuggestions}
                onGoToPDPPage={this.onGoToPDPPage}
                onLoadMoreProducts={this.onLoadMoreProducts}
                onAddItemToFavorites={onAddItemToFavorites}
                isLoggedIn={isLoggedIn}
                labelsLogin={labelsLogin}
                navigation={navigation}
                pdpLabels={pdpLabels}
                isKeepModalOpen={isKeepModalOpen}
                onScrollHandler={this.onScrollHandler}
                isDataLoading={isDataLoading}
                wishList={defaultWishListFromState}
                searchTopPromos={searchTopPromos}
                isPlcc={isPlcc}
                accessibilityLabels={accessibilityLabels}
                isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                isNewReDesignEnabled={isNewReDesignEnabled}
                alternateBrand={alternateBrand}
                {...otherProps}
              />
            ) : (
              <NoResponseSearchDetail
                totalProductsCount={totalProductsCount}
                labels={labels}
                slpLabels={slpLabels}
                searchedText={this.searchQuery}
                sortLabels={sortLabels}
                searchResultSuggestions={searchResultSuggestions}
                navigation={navigation}
                pdpLabels={pdpLabels}
                searchTopPromos={searchTopPromos}
                isPlcc={isPlcc}
                accessibilityLabels={accessibilityLabels}
                isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                alternateBrand={alternateBrand}
                {...otherProps}
              />
            ))}
        </View>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const appliedFilters = getAppliedFilters(state);
  const plpGridPromos = getPLPGridPromos(state);
  const plpHorizontalPromo = getPlpHorizontalPromo(state);
  const products = getAllProductsSelect(state);

  const filtersLength = {};
  let filterCount = 0;

  // eslint-disable-next-line
  for (let key in appliedFilters) {
    if (appliedFilters[key]) {
      filtersLength[`${key}Filters`] = appliedFilters[key].length;
      filterCount += appliedFilters[key].length;
    }
  }
  const productWithGrid = getProductsWithPromo(
    products,
    plpGridPromos,
    plpHorizontalPromo,
    filterCount
  );
  const filters = updateAppliedFiltersInState(state);
  return {
    products: productWithGrid,
    filters,
    categoryId: getCategoryId(state),
    loadedProductCount: getLoadedProductsCount(state),
    unbxdId: getUnbxdId(state),
    totalProductsCount: getTotalProductsCount(state),
    navTree: getNavigationTreeApp(state),
    searchedText: getCurrentSearchForText(state),
    filtersLength,
    initialValues: {
      ...getAppliedFilters(state),
      // TODO - change after site id comes for us or ca
      sort: getAppliedSortId(state) || '',
    },
    labelsFilter: state.Labels && state.Labels.PLP && state.Labels.PLP.PLP_sort_filter,
    longDescription: getLongDescription(state),
    labels: getLabelsProductListing(state),
    labelsLogin: getLabelsAccountOverView(state),
    isLoadingMore: getIsLoadingMore(state),
    isSearchResultsAvailable: checkIfSearchResultsAvailable(state),
    selectedFilterValue: getSelectedFilter(state),
    lastLoadedPageNumber: getLastLoadedPageNumber(state),
    formValues: getFormValues('filter-form')(state),
    currentNavIds: state.ProductListing && state.ProductListing.currentNavigationIds,
    slpLabels: getLabels(state),
    searchResultSuggestions:
      state.SearchListingPage && state.SearchListingPage.searchResultSuggestions,
    sortLabels: getSortLabels(state),
    scrollToTop: getScrollToTopValue(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    labelsPlpTiles: labelsSelectors.getPlpTilesLabels(state),
    pdpLabels: getPDPLabels(state),
    isKeepModalOpen: getModalState(state),
    isKeepAliveEnabled: getIsKeepAliveProductApp(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    errorMessages: fetchErrorMessages(state),
    isDataLoading: getIsDataLoading(state),
    isShowPriceRangeKillSwitch: getIsShowPriceRangeForApp(state),
    showPriceRangeForABTest: getABTestIsShowPriceRange(state),
    isHidePLPAddToBag: getIsHidePLPAddToBag(state),
    isHidePLPRatings: getIsHidePLPRatings(state),
    redirect: getRedirectObj(state),
    isPlcc: isPlccUser(state),
    defaultWishListFromState: wishListFromState(state),
    isExternalCampaignFired: getExternalCampaignFired(state),
    changeFilterViewABTest: getChangeFilterViewABTest(state),
    searchTopPromos: getSLPTopPromos(state),
    accessibilityLabels: getAccessibilityLabels(state),
    isBrierleyPromoEnabled: getIsBrierleyPromoEnabled(state),
    deleteFavItemInProgressFlag: getDefaultFavItemInProgressFlag(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    isNewReDesignEnabled: getIsNewReDesignEnabled(state),
    alternateBrand: getAlternateBrandName(state),
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    getProducts: (payload) => {
      dispatch(getSlpProducts(payload));
    },
    getMoreProducts: (payload) => {
      dispatch(getMoreSlpProducts(payload));
    },
    resetProducts: () => {
      dispatch(resetSlpProducts());
    },
    resetResults: () => {
      dispatch(setSlpResultsAvailableState());
    },
    onQuickViewOpenClick: (payload) => {
      const title = (ownProps.navigation && ownProps.navigation.getParam('title')) || '';
      dispatch(setQuickViewProductDetailPageTitle(title));
      dispatch(openQuickViewWithValues(payload));
    },
    onAddItemToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
    onSetLastDeletedItemIdAction: (payload) => {
      dispatch(setLastDeletedItemIdAction(payload));
    },
    setRecentSearches: (searchTerm) => {
      dispatch(setRecentSearch({ searchTerm }));
    },
    clearRecentSearchs: () => {
      dispatch(clearRecentSearch());
    },
    setSelectedFilter: (payload) => {
      dispatch(setFilter(payload));
    },
    setExternalCampaignFired: (payload) => {
      dispatch(setExternalCampaignState(payload));
    },
    setDeviceTierInState: (payload) => {
      dispatch(setDeviceTier(payload));
    },
    setSlpLoadingStateForAlternateBrand: (payload) => {
      dispatch(setSlpLoadingState(payload));
    },
    trackPageLoad: (payload, customData) => {
      dispatch(setClickAnalyticsData(customData));
      setTimeout(() => {
        dispatch(trackPageView(payload));
        setTimeout(() => {
          dispatch(setClickAnalyticsData({}));
        }, 200);
      }, 100);
    },
    trackClickFilter: ({ name, module, contextData }) => {
      dispatch(
        setClickAnalyticsData({
          customEvents: ['events161'],
        })
      );
      dispatch(trackClick({ name, module, contextData }));
    },
  };
}

SearchDetailContainer.propTypes = {
  router: PropTypes.shape({
    query: PropTypes.shape({
      searchQuery: PropTypes.string,
    }),
  }).isRequired,
  getProducts: PropTypes.func.isRequired,
  getMoreProducts: PropTypes.func.isRequired,
  navTree: PropTypes.shape({}),
  filters: PropTypes.shape({}),
  filtersLength: PropTypes.shape({}),
  initialValues: PropTypes.shape({}),
  formValues: PropTypes.shape({
    sort: PropTypes.string.isRequired,
  }).isRequired,
  onSubmit: PropTypes.func.isRequired,
  isLoadingMore: PropTypes.bool,
  isSearchResultsAvailable: PropTypes.bool,
  navigation: PropTypes.shape({}),
  products: PropTypes.arrayOf(PropTypes.shape({})),
  currentNavIds: PropTypes.arrayOf(PropTypes.shape({})),
  breadCrumbs: PropTypes.arrayOf(PropTypes.shape({})),
  totalProductsCount: PropTypes.number,
  longDescription: PropTypes.string,
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  lastLoadedPageNumber: PropTypes.number,
  labelsFilter: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  categoryId: PropTypes.string.isRequired,
  onPickUpOpenClick: PropTypes.func,
  searchedText: PropTypes.string,
  slpLabels: PropTypes.shape({}),
  searchResultSuggestions: PropTypes.shape({}),
  sortLabels: PropTypes.shape({}),
  resetProducts: PropTypes.func,
  setRecentSearches: PropTypes.func,
  clearRecentSearchs: PropTypes.func,
  onAddItemToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  labelsLogin: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  pdpLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  isKeepModalOpen: PropTypes.bool,
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  resetResults: PropTypes.func,
  loadedProductCount: PropTypes.bool.isRequired,
  isDataLoading: PropTypes.bool,
  redirect: PropTypes.shape({}),
  defaultWishListFromState: PropTypes.func.isRequired,
  setSelectedFilter: PropTypes.func,
  changeFilterViewABTest: PropTypes.bool,
  searchTopPromos: PropTypes.arrayOf(
    PropTypes.shape({
      // Only including the most important property
      moduleName: PropTypes.string,
    })
  ),
  accessibilityLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  isPlcc: PropTypes.bool,
  isBrierleyPromoEnabled: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isNewReDesignEnabled: PropTypes.bool,
  setDeviceTierInState: PropTypes.func,
};

SearchDetailContainer.defaultProps = {
  navTree: {},
  filters: {},
  filtersLength: {},
  initialValues: {},
  isLoadingMore: false,
  isSearchResultsAvailable: false,
  navigation: {},
  products: [],
  currentNavIds: [],
  breadCrumbs: [],
  totalProductsCount: 0,
  labels: {},
  longDescription: '',
  lastLoadedPageNumber: 0,
  labelsFilter: {},
  onPickUpOpenClick: () => {},
  searchedText: '',
  slpLabels: {},
  searchResultSuggestions: {},
  sortLabels: {},
  resetProducts: () => {},
  setRecentSearches: null,
  clearRecentSearchs: null,
  onAddItemToFavorites: null,
  isLoggedIn: false,
  labelsLogin: {},
  pdpLabels: {},
  isKeepModalOpen: false,
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  resetResults: () => {},
  isDataLoading: false,
  redirect: {},
  setSelectedFilter: () => {},
  changeFilterViewABTest: false,
  searchTopPromos: [],
  accessibilityLabels: {},
  isPlcc: false,
  isBrierleyPromoEnabled: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isDynamicBadgeEnabled: {},
  isNewReDesignEnabled: false,
  setDeviceTierInState: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchDetailContainer);
