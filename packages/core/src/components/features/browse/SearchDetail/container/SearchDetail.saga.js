/* eslint-disable sonarjs/cognitive-complexity */
/* eslint-disable max-statements */
// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, putResolve, takeLatest, select, fork } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { isClient, getSiteId, getAPIConfig, isMobileApp, getBrand } from '@tcp/core/src/utils';
import { API_CONFIG } from '@tcp/core/src/services/config';
import { getIsBopisFilterOn as getIsBopisFilterOnSlp } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import { makeSearch } from '@tcp/core/src/services/abstractors/common/searchBar';
import {
  getIsNonInvSessionFlag,
  getIsOtherBrandExpABTestEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import { loadLayoutData, loadModulesData } from '@tcp/core/src/reduxStore/actions';
import { isTier2Device, isTier3Device } from '@tcp/web/src/utils/device-tiering';
import { TIER2, TIER3 } from '@tcp/core/src/constants/tiering.constants';
import { resetAlternateBrandInPickUp } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import { SLP_PAGE_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import {
  setSlpProducts,
  setSlpLoadingState,
  setSlpSearchTerm,
  setListingFirstProductsPage,
  setNOSearchResultPageFlag,
} from './SearchDetail.actions';
import Abstractor from '../../../../../services/abstractors/productListing';
import ProductsOperator from '../../ProductListing/container/productsRequestFormatter';
import { setSearchResult } from '../../../../common/molecules/SearchBar/SearchBar.actions';
import { getLastLoadedPageNumber } from './SearchDetail.selectors';
import { getDefaultWishList } from '../../Favorites/container/Favorites.saga';
import SLP_CONSTANTS, {
  PRODUCTS_PER_LOAD,
  PRODUCTS_PER_LOAD_LOWER_END_DEVICES,
} from './SearchDetail.constants';
import { getCommonAnalyticsData } from '../../../../../analytics/Analytics.selectors';
import { setCommonAnalyticsData } from '../../../../../analytics/actions';

const instanceProductListing = new Abstractor();
const operatorInstance = new ProductsOperator();

const getUrl = (url) => {
  return url
    ? {
        pathname: url,
      }
    : window.location;
};
const isFormDataPresent = (formData, sortBySelected) => {
  return !formData && !sortBySelected;
};
const getSlpScrollFlag = (formDataPresent, isProcessedResponse, prefetchedResponse) => {
  return isClient() && formDataPresent && !isProcessedResponse && !prefetchedResponse;
};

const getIsScrollToTopFlag = (scrollToTop) => {
  return scrollToTop || false;
};

const getAlternateBrandToSearchFor = () => {
  const brand = getBrand();
  const { brands } = API_CONFIG;
  const otherBrand =
    brands &&
    brands.filter((brandName) => {
      return brandName !== brand;
    });
  return otherBrand && otherBrand[0] && otherBrand[0].toUpperCase();
};

const getIsTotalProductsCountMoreThanZero = (res, isOtherBrandExpABTestFlag) => {
  return (
    (res && res.totalProductsCount > 0) ||
    res?.body?.response?.numberOfProducts === 1 ||
    !isOtherBrandExpABTestFlag
  );
};

const getLocationValue = (location) => {
  return !location.search ? `${location.pathname}?` : `${location.pathname}?${location.search}`;
};

export function* setSearchTextFirstTouch(searchQuery) {
  let commonAnalyticsData = yield select(getCommonAnalyticsData) || {};
  const searchTextFirstTouch =
    (commonAnalyticsData && commonAnalyticsData.searchTextFirstTouch) || '';
  if (!commonAnalyticsData || !searchTextFirstTouch) {
    commonAnalyticsData = { ...commonAnalyticsData, searchTextFirstTouch: searchQuery };
    yield put(setCommonAnalyticsData({ ...commonAnalyticsData }));
  }
}

export function* fetchSlpProducts({ payload }) {
  try {
    const {
      prefetchedResponse,
      searchQuery,
      asPath,
      formData,
      url,
      scrollToTop,
      isKeepModalOpen,
      navigation = null,
      isFilterAbTestOn,
      sortBySelected,
      isProcessedResponse,
    } = payload;
    const formDataPresent = isFormDataPresent(formData, sortBySelected);
    yield put(loadLayoutData({}, 'searchListingPage'));

    const xappURLConfig = yield call(getUnbxdXappConfigs);
    const location = getUrl(url);
    const state = yield select();
    yield put(resetAlternateBrandInPickUp());
    yield put(
      setSlpLoadingState({
        isLoadingMore: true,
        isScrollToTop: getIsScrollToTopFlag(scrollToTop),
        isKeepModalOpen,
        isSearchResultsAvailable: false,
        isDataLoading: true,
        alternateBrandName: '',
      })
    );

    yield call(getDefaultWishList);
    yield fork(setSearchTextFirstTouch, searchQuery);
    yield put(setSlpSearchTerm({ searchTerm: searchQuery }));
    const isNonInvABTestEnabled = yield select(getIsNonInvSessionFlag);
    let res;
    if (prefetchedResponse) {
      const reqObj = {};
      res = yield call(
        instanceProductListing.getProducts,
        reqObj,
        state,
        prefetchedResponse,
        null,
        xappURLConfig,
        isNonInvABTestEnabled,
        false
      );
    } else {
      const reqObj = operatorInstance.getProductsListingFilters({
        state,
        formData,
        asPath,
        pageNumber: 1,
        location,
      });
      reqObj.navigation = navigation;
      reqObj.extraParams = {
        ...reqObj.extraParams,
        ...instanceProductListing.getExtraFilterParams(isFilterAbTestOn),
      };
      res = yield call(
        instanceProductListing.getProducts,
        reqObj,
        state,
        prefetchedResponse,
        null,
        xappURLConfig,
        isNonInvABTestEnabled,
        false
      );
    }
    let alternateBrandToSearch;
    const isOtherBrandExpABTestFlag = yield select(getIsOtherBrandExpABTestEnabled);
    const isBopisFilterOff = yield select(getIsBopisFilterOnSlp);

    const isBopisFilterRadioBtnOn = isBopisFilterOff === false && isBopisFilterOff !== undefined;
    if (!res) {
      yield put(
        setSlpLoadingState({
          loadedProductsPages: [],
          totalProductsCount: 0,
          isLoadingMore: false,
          isScrollToTop: false,
          isDataLoading: false,
          isSearchResultsAvailable: true,
          isBopisFilterOn: true,
        })
      );
    } else if (getIsTotalProductsCountMoreThanZero(res, isOtherBrandExpABTestFlag)) {
      const { layout, modules } = yield call(
        instanceProductListing.parsedModuleData,
        res.bannerInfo,
        getAPIConfig()
      );
      yield put(loadLayoutData(layout, 'searchListingPage'));
      yield put(loadModulesData(modules));

      let locationValue = '';

      if (res.updatedUrl) {
        const siteId = getSiteId();
        locationValue = `/${siteId}${res.updatedUrl}`;
      } else if (isClient()) {
        locationValue = !location.search
          ? `${location.pathname}?`
          : `${location.pathname}?${location.search}`;
      }

      yield putResolve(setListingFirstProductsPage({ ...res, pageUrl: locationValue }));
      yield put(
        setSlpLoadingState({
          isLoadingMore: false,
          isScrollToTop: false,
          isSearchResultsAvailable: true,
          isDataLoading: false,
        })
      );
      yield put(
        setSlpLoadingState({
          shouldSlpScroll: getSlpScrollFlag(
            formDataPresent,
            isProcessedResponse,
            prefetchedResponse
          ),
        })
      );
      // eslint-disable-next-line sonarjs/no-duplicated-branches
    } else if (isOtherBrandExpABTestFlag && isBopisFilterRadioBtnOn) {
      yield put(
        setSlpLoadingState({
          loadedProductsPages: [],
          totalProductsCount: 0,
          isLoadingMore: false,
          isScrollToTop: false,
          isDataLoading: false,
          isSearchResultsAvailable: true,
          isBopisFilterOn: true,
        })
      );
    } else if (isOtherBrandExpABTestFlag) {
      alternateBrandToSearch = getAlternateBrandToSearchFor();
      const reqObj = operatorInstance.getProductsListingFilters({
        state,
        formData,
        asPath,
        pageNumber: 1,
        location,
      });

      reqObj.navigation = navigation;
      reqObj.extraParams = {
        ...reqObj.extraParams,
        ...instanceProductListing.getExtraFilterParams(isFilterAbTestOn),
      };
      reqObj.brand = alternateBrandToSearch;
      res = yield call(
        instanceProductListing.getProducts,
        reqObj,
        state,
        prefetchedResponse,
        null,
        xappURLConfig,
        isNonInvABTestEnabled,
        false
      );

      const { layout, modules } = yield call(
        instanceProductListing.parsedModuleData,
        res.bannerInfo,
        getAPIConfig()
      );
      yield put(loadLayoutData(layout, 'searchListingPage'));
      yield put(loadModulesData(modules));

      let locationValue = '';

      if (res.updatedUrl) {
        const siteId = getSiteId();
        locationValue = `/${siteId}${res.updatedUrl}`;
      } else if (isClient()) {
        locationValue = getLocationValue(location);
      }
      yield putResolve(setListingFirstProductsPage({ ...res, pageUrl: locationValue }));
      yield put(
        setSlpLoadingState({
          isLoadingMore: false,
          isScrollToTop: false,
          isSearchResultsAvailable: true,
          isDataLoading: false,
          alternateBrandName: alternateBrandToSearch,
        })
      );
      yield put(
        setSlpLoadingState({
          shouldSlpScroll: getSlpScrollFlag(
            formDataPresent,
            isProcessedResponse,
            prefetchedResponse
          ),
        })
      );
    }
  } catch (err) {
    logger.error(err);
    yield put(
      setSlpLoadingState({
        isLoadingMore: false,
        isScrollToTop: false,
        isSearchResultsAvailable: true,
        isDataLoading: false,
      })
    );
  }
}

export function* fetchMoreProducts({ payload = {} }) {
  try {
    const { url, isFilterAbTestOn } = payload;
    const state = yield select();
    const location = getUrl(url);
    const xappURLConfig = yield call(getUnbxdXappConfigs);
    yield put(setSlpLoadingState({ isLoadingMore: true }));

    const { appliedFiltersIds } = state[SLP_PAGE_REDUCER_KEY];
    const sort = (state[SLP_PAGE_REDUCER_KEY] && state[SLP_PAGE_REDUCER_KEY].appliedSortId) || '';
    const alternateBrandName =
      state[SLP_PAGE_REDUCER_KEY] && state[SLP_PAGE_REDUCER_KEY].alternateBrandName;
    const appliedFiltersAndSort = { ...appliedFiltersIds, sort };

    const lastLoadedPageNumber = getLastLoadedPageNumber(state);
    const reqObj = operatorInstance.getProductsListingInfo({
      state,
      filtersAndSort: appliedFiltersAndSort,
      pageNumber: lastLoadedPageNumber + 1,
      location,
      isLazyLoading: true,
    });
    reqObj.brand = alternateBrandName;
    reqObj.extraParams = {
      ...reqObj.extraParams,
      ...instanceProductListing.getExtraFilterParams(isFilterAbTestOn),
    };
    let isLowerEndDevice = false;
    if (isMobileApp()) {
      isLowerEndDevice =
        state.ProductListing.deviceTier === TIER2 || state.ProductListing.deviceTier === TIER3;
    } else {
      isLowerEndDevice = isTier2Device() || isTier3Device();
    }
    const productsCount = isLowerEndDevice
      ? PRODUCTS_PER_LOAD_LOWER_END_DEVICES
      : PRODUCTS_PER_LOAD;
    const isNonInvABTestEnabled = yield select(getIsNonInvSessionFlag);
    const res = yield call(
      instanceProductListing.getProducts,
      reqObj,
      state,
      null,
      productsCount,
      xappURLConfig,
      isNonInvABTestEnabled,
      false
    );

    if (res) {
      yield put(setSlpProducts({ ...res }));
    }
    yield put(setSlpLoadingState({ isLoadingMore: false }));
  } catch (err) {
    logger.error(err);
    yield put(setSlpLoadingState({ isLoadingMore: false }));
  }
}

export function* fetchSlpSearchResults({ payload }) {
  const suggestionsCount = {
    category: 4,
    keywords: 4,
    promotedTopQueries: 4,
  };

  const isHideBundleProduct = false;
  const payloadData = {
    searchTerm: payload.searchText,
    suggestionsCount,
    isHideBundleProduct,
    slpLabels: payload.slpLabels,
  };

  const xappURLConfig = yield call(getUnbxdXappConfigs);

  try {
    const response = yield call(makeSearch, payloadData, xappURLConfig);

    if (
      response.autosuggestList &&
      response.autosuggestList[0] &&
      response.autosuggestList[0].suggestions.length < 1 &&
      response.autosuggestList[1] &&
      response.autosuggestList[1].suggestions.length < 1
    ) {
      yield put(setSearchResult(response));
      yield put(setNOSearchResultPageFlag(false));
    } else {
      yield put(setSearchResult(response));
      yield put(setNOSearchResultPageFlag(true));
    }
  } catch (err) {
    logger.error('Error: error in fetching Search bar results ');
  }
}

function* SearchPageSaga() {
  yield takeLatest(SLP_CONSTANTS.FETCH_SLP_PRODUCTS, fetchSlpProducts);
  yield takeLatest(SLP_CONSTANTS.GET_MORE_SLP_PRODUCTS, fetchMoreProducts);
  yield takeLatest(SLP_CONSTANTS.GET_SLP_SEARCH_RESULTS, fetchSlpSearchResults);
}

export default SearchPageSaga;
