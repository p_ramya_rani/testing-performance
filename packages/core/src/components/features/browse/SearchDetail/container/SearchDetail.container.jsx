/* eslint-disable */
// 9fbef606107a605d69c0edbcd8029e5d
// Disabling eslint for temporary fix
import React from 'react';
import Head from 'next/head';
import withIsomorphicRenderer from '@tcp/core/src/components/common/hoc/withIsomorphicRenderer';
import { getFormValues, change } from 'redux-form';
import { PropTypes } from 'prop-types';
import dynamic from 'next/dynamic';
import { loadLayoutData } from '@tcp/core/src/reduxStore/actions';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import utils from '@tcp/core/src/utils';
import {
  getIsKeepAliveProduct,
  getIsInternationalShipping,
  getIsShowPriceRange,
  getABTestIsShowPriceRange,
  getIsBrierleyPromoEnabled,
  getIsPlpPdpAnchoringEnabled,
  getIsDynamicBadgeEnabled,
  getABTestForStickyFilter,
  getIsNonInvSessionFlag,
  getIsShowBrandNameEnabled,
  getIsBopisFilterEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  resetStoresByCoordinates,
  getFavoriteStoreActn,
} from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import { setProp } from '@tcp/core/src/analytics/utils';
import { isClient, getStaticFilePath } from '@tcp/core/src/utils';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import SearchDetail from '../views/SearchDetail.view';
import { trackPageView, setClickAnalyticsData } from '../../../../../analytics/actions';
import {
  getSlpProducts,
  getMoreSlpProducts,
  initActions,
  resetSlpProducts,
  setSlpScrollState,
  setSlpLoadingState,
} from './SearchDetail.actions';
import { getAPIConfig } from '@tcp/core/src/utils/utils';
import {
  getProductsAndTitleBlocks,
  checkPromoCondition,
} from '../../ProductListing/container/ProductListing.util';
import {
  removeAddToFavoriteErrorState,
  addItemsToWishlist,
  getUserFavt,
  setLastDeletedItemIdAction,
} from '../../Favorites/container/Favorites.actions';
import {
  getPageName,
  getPageSection,
  getPageSubSection,
} from '../../../../common/organisms/PickupStoreModal/molecules/PickupStoreSelectionForm/container/PickupStoreSelectionForm.selectors';
import getSortLabels from '../../ProductListing/molecules/SortSelector/views/Sort.selectors';
import { openQuickViewWithValues } from '../../../../common/organisms/QuickViewModal/container/QuickViewModal.actions';
import {
  getUserLoggedInState,
  isRememberedUser,
  isPlccUser,
  getDefaultStoreData,
} from '../../../account/User/container/User.selectors';
import {
  getUnbxdId,
  getCategoryId,
  getNavigationTree,
  getLongDescription,
  getLastLoadedPageNumber,
  getIsHidePLPAddToBag,
  getLabelsOutOfStock,
  getOnModelImageAbTestPlp,
  getIsHidePLPRatings,
  getAccessibilityLabels,
  getLabelsProductListing as getPlpLabelsProductListing,
} from '../../ProductListing/container/ProductListing.selectors';
import { setBopisFilterState } from '../../ProductListing/container/ProductListing.actions';
import {
  getLoadedProductsCount,
  getLoadedProductsPages,
  getProductsFilters,
  getTotalProductsCount,
  getProductsSelect,
  getCurrentSearchForText,
  getLabels,
  getAppliedFilters,
  getAppliedSortId,
  getIsLoadingMore,
  checkIfSearchResultsAvailable,
  getPDPLabels,
  getPlpHorizontalPromo,
  getPLPGridPromos,
  getIsSearchPage,
  getSlpUrl,
  getSLPTopPromos,
  getLabelsProductListing,
  getShouldSlpScroll,
  getAlternateBrandName,
  getIsBopisFilterOn,
} from './SearchDetail.selectors';
import {
  fetchAddToFavoriteErrorMsg,
  fetchErrorMessages,
  wishListFromState,
  getDefaultFavItemInProgressFlag,
} from '../../Favorites/container/Favorites.selectors';

import submitProductListingFiltersForm from '../../ProductListing/container/productListingOnSubmitHandler';
import NoResponseSearchDetail from '../views/NoResponseSearchDetail.view';

const checkIfIsBackPage = ({ loadedProductCount, slpUrl }) => {
  const savedUrl = decodeURIComponent(slpUrl).replace(/\?/g, '');
  const newUrl =
    (isClient() &&
      decodeURIComponent(`${window.location.pathname}?${window.location.search}`).replace(
        /\?/g,
        ''
      )) ||
    '';

  return isClient() && savedUrl === newUrl && loadedProductCount > 0;
};

const shouldUnbxdRequestTrigger = (isBackSLPPage) => {
  return !isBackSLPPage && !window.isUnbxdRequestTriggered;
};

const dynamicPromoModules = {
  Espot: dynamic(() =>
    import(/* webpackChunkName: "espot" */ '@tcp/core/src/components/common/molecules/Espot')
  ),
  divisionTabs: dynamic(() =>
    import(
      /* webpackChunkName: "divisionTabs" */ '@tcp/core/src/components/common/molecules/DivisionTabModule'
    )
  ),
  outfitCarousel: dynamic(() =>
    import(
      /* webpackChunkName: "outfitCarousel" */ '@tcp/core/src/components/common/molecules/OutfitCarouselModule'
    )
  ),
  jeans: dynamic(() =>
    import(/* webpackChunkName: "jeans" */ '@tcp/core/src/components/common/molecules/JeansModule')
  ),
  moduleX: dynamic(() =>
    import(/* webpackChunkName: "moduleX" */ '@tcp/core/src/components/common/molecules/ModuleX')
  ),
  essentials: dynamic(() =>
    import(
      /* webpackChunkName: "essentials" */ '@tcp/core/src/components/features/browse/ProductListing/organisms/EssentialShop'
    )
  ),
};

class SearchDetailContainer extends React.PureComponent {
  static pageProps = {
    pageData: {
      pageName: 'search',
    },
  };

  static getInitialProps = async ({ props, query, req, isServer, ssrSearchPage, asPath }) => {
    const { getProducts, formValues, resetSlpScrollState } = props;
    let searchQuery;
    if (!isServer) {
      ({
        router: {
          query: { searchQuery },
          asPath,
        },
      } = props);
      const queryString = asPath.split('?');
      const filterSortString = (queryString.length && queryString[1]) || '';
      const isBackSLPPage = checkIfIsBackPage(props);

      if (shouldUnbxdRequestTrigger(isBackSLPPage)) {
        await getProducts({
          URI: 'search',
          asPath: filterSortString,
          searchQuery,
          ignoreCache: true,
          formValues,
          url: asPath,
        });
      } else if (isBackSLPPage) {
        resetSlpScrollState();
      }
    } else if (ssrSearchPage) {
      const queryString = asPath.split('?');
      const filterSortString = (queryString.length && queryString[1]) || '';
      ({ searchQuery = '' } = query);
      await getProducts({
        URI: 'search',
        asPath: filterSortString,
        searchQuery,
        ignoreCache: true,
        url: asPath,
      });
    }
  };

  constructor(props) {
    super(props);
    const { resetProducts, breadCrumbs } = this.props;
    const isBackSLPPage = checkIfIsBackPage(props);
    if (!isBackSLPPage) {
      resetProducts();
    }
    this.searchResponseCallback = this.searchResponseCallback.bind(this);
    if (isClient() && window.isUnbxdRequestTriggered) {
      if (window.elem && !window.SEARCH_RESPONSE_INITIAL) {
        window.elem.addEventListener('unbxdResRecieved', this.searchResponseCallback);
      } else if (window.SEARCH_RESPONSE_INITIAL) {
        this.searchResponseCallback(
          {
            detail: {
              unbxdResponse: window.SEARCH_RESPONSE_INITIAL.unbxdResponse,
            },
          },
          this
        );
      }
    }
  }

  searchResponseCallback = (e) => {
    const { getProducts, formValues, router } = this.props;
    const { unbxdResponse } = e.detail;
    const {
      query: { searchQuery },
      asPath,
    } = router;
    const queryString = asPath.split('?');
    const filterSortString = (queryString.length && queryString[1]) || '';
    const isBackSLPPage = checkIfIsBackPage(this.props);
    if (!isBackSLPPage) {
      getProducts({
        URI: 'search',
        asPath: filterSortString,
        searchQuery,
        ignoreCache: true,
        formValues,
        url: asPath,
        prefetchedResponse: unbxdResponse,
      });
      window.isUnbxdRequestTriggered = false;
    }
  };

  componentDidMount = () => {
    const { resetSlpLoadingState } = this.props;
    resetSlpLoadingState({ isSLPPage: true });
  };

  componentDidUpdate(prevProps) {
    const {
      router: { asPath },
      getProducts,
      formValues,
      isLoggedIn: currentlyLoggedIn,
      onFavtLoad,
      router,
      clearPromoDataOnRouteChange,
      resetSlpLoadingState,
      setBopisFilterStateActn,
    } = this.props;

    // Clears layout on route change to make sure promo data is not stale when jumping between different PLPs.
    if (prevProps.router.asPath !== '' && router.asPath !== prevProps.router.asPath) {
      clearPromoDataOnRouteChange();
    }

    const {
      router: { asPath: previousAsPath },
      isLoggedIn,
    } = prevProps;

    const searchQuery = asPath && asPath.split('?')[0];
    const previousSearchQuery = previousAsPath && previousAsPath.split('?')[0];

    if (searchQuery !== previousSearchQuery) {
      const queryString = asPath.split('?');
      const filterSortString = (queryString.length && queryString[1]) || '';
      setBopisFilterStateActn(false); // Reset Bopis filter for a new search term
      getProducts({
        URI: 'search',
        asPath: filterSortString,
        searchQuery,
        ignoreCache: true,
        formValues,
        url: asPath,
      });
    }

    this.setDefaultStore();

    if (currentlyLoggedIn && isLoggedIn !== currentlyLoggedIn) {
      onFavtLoad(true);
    }
  }

  componentWillUnmount() {
    const { resetSlpLoadingState, setBopisFilterStateActn, updateFormValue } = this.props;
    updateFormValue('bopisStoreId', []);

    resetSlpLoadingState({ isSLPPage: false });
    if (window.elem && !window.SEARCH_RESPONSE_INITIAL)
      window.elem.removeEventListener('unbxdResRecieved', this.searchResponseCallback);
  }

  getDefaultStore = () => {
    const { favStore } = this.props;
    try {
      if (favStore?.basicInfo?.id) return favStore;
      return JSON.parse(getLocalStorage('defaultStore')) || {};
    } catch (e) {
      return {};
    }
  };

  setDefaultStore = () => {
    const { getFavoriteStoreById, formValues } = this.props;
    const [bopisStoreId] = formValues?.bopisStoreId || [];

    const defaultStore = this.getDefaultStore();
    const localStoreId = defaultStore?.basicInfo?.id;
    const [storeId] = bopisStoreId?.match(/\d+/) || [];
    const fullStoreId = `11${storeId}`;
    if (
      !this.isFavStoreUpdated &&
      storeId &&
      bopisStoreId &&
      localStoreId &&
      fullStoreId !== localStoreId
    ) {
      getFavoriteStoreById({ storeId: fullStoreId });
      this.isFavStoreUpdated = true;
    }
  };

  renderNoResponseSearchDetail = () => {
    const {
      totalProductsCount,
      labels,
      slpLabels,
      searchedText,
      sortLabels,
      searchResultSuggestions,
      pdpLabels,
      trackPageLoad,
      router: {
        query: { searchType },
        asPath: asPathVal,
      },
      isSearchResultsAvailable,
      isLoadingMore,
      isBopisFilterOn,
      formValues,
      getProducts,
      onSubmit,
      updateFormValue,
      setBopisFilterStateActn,
      isBopisFilterEnabled,
      ...otherProps
    } = this.props;
    return (
      <React.Fragment>
        {isSearchResultsAvailable && (
          <NoResponseSearchDetail
            totalProductsCount={totalProductsCount}
            labels={labels}
            slpLabels={slpLabels}
            searchedText={searchedText}
            sortLabels={sortLabels}
            searchResultSuggestions={searchResultSuggestions}
            pdpLabels={pdpLabels}
            trackPageLoad={trackPageLoad}
            searchType={searchType}
            isSearchResultsAvailable={isSearchResultsAvailable}
            isLoadingMore={isLoadingMore}
            asPathVal={asPathVal}
            {...otherProps}
            isBopisFilterOn={isBopisFilterOn}
            formValues={formValues}
            getProducts={getProducts}
            onSubmit={onSubmit}
            updateFormValue={updateFormValue}
            setBopisFilterStateActn={setBopisFilterStateActn}
            isBopisFilterEnabled={isBopisFilterEnabled}
          />
        )}
      </React.Fragment>
    );
  };

  render() {
    const {
      formValues,
      productsBlock,
      products,
      currentNavIds,
      breadCrumbs,
      filters,
      totalProductsCount,
      filtersLength,
      initialValues,
      filterCount,
      longDescription,
      labels,
      isLoadingMore,
      lastLoadedPageNumber,
      labelsFilter,
      categoryId,
      getProducts,
      onSubmit,
      onPickUpOpenClick,
      searchedText,
      slpLabels,
      searchResultSuggestions,
      sortLabels,
      isSearchResultsAvailable,
      router: {
        query: { searchQuery, searchType },
        asPath: asPathVal,
      },
      currency,
      currencyAttributes,
      onAddItemToFavorites,
      isLoggedIn,
      pdpLabels,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      pageNameProp,
      pageSectionProp,
      pageSubSectionProp,
      trackPageLoad,
      isSearchPage,
      isOnModelAbTestPlp,
      apiConfig,
      defaultWishListFromState,
      searchTopPromos,
      isPlcc,
      accessibilityLabels,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isPlpPdpAnchoringEnabled,
      isDynamicBadgeEnabled,
      navigationData,
      isNonInvSessionFlag,
      alternateBrand,
      isShowBrandNameEnabled,
      isBopisFilterOn,
      ...otherProps
    } = this.props;

    const apiConfigObj = getAPIConfig();
    const { isNonInvStagEnabled } = apiConfigObj || {};
    const isSecureImageFlagEnabled = isNonInvSessionFlag && isNonInvStagEnabled;
    const brand = apiConfig.brandId.toUpperCase();
    const brandUnboxKey = apiConfig[`unboxKey${brand}`];
    const unbxdKey = apiConfig[`unbxd${brand}`];
    const requestUrl = `${unbxdKey}/${brandUnboxKey}/search`;

    const navTree = getNavigationTree(navigationData);
    const defaultStore = this.getDefaultStore();

    return (
      <React.Fragment>
        <>
          {!isClient() && (
            <Head>
              <link
                rel="preload"
                href={getStaticFilePath('scripts/prefetch-search-unbxd.min.js', 'noCookieLess')}
                as="script"
              />
              <script
                id="unbxdPrefetchMeta"
                key="rwd_unbxd_prefetch_meta"
                dangerouslySetInnerHTML={{
                  __html: `
                var unbxdUrl = '${requestUrl}';
              `,
                }}
              />
              <script
                key="rwd_unbxd_prefetch_search_script"
                type="text/javascript"
                src={getStaticFilePath('scripts/prefetch-search-unbxd.min.js', 'noCookieLess')}
              />
            </Head>
          )}
          <div id="elem" />
        </>
        {isSearchResultsAvailable && JSON.stringify(products) !== '{}' ? (
          <div>
            {products && products.length > 0 ? (
              <SearchDetail
                filters={filters}
                formValues={formValues}
                filtersLength={filtersLength}
                getProducts={getProducts}
                isLoadingMore={isLoadingMore}
                initialValues={initialValues}
                filterCount={filterCount}
                onSubmit={onSubmit}
                products={products}
                productsBlock={productsBlock}
                totalProductsCount={totalProductsCount}
                labels={labels}
                labelsFilter={labelsFilter}
                slpLabels={slpLabels}
                searchedText={searchedText}
                sortLabels={sortLabels}
                searchResultSuggestions={searchResultSuggestions}
                currencyAttributes={currencyAttributes}
                currency={currency}
                onAddItemToFavorites={onAddItemToFavorites}
                isLoggedIn={isLoggedIn}
                isSearchListing
                asPathVal={asPathVal}
                AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                pageNameProp={pageNameProp}
                pageSectionProp={pageSectionProp}
                pageSubSectionProp={pageSubSectionProp}
                trackPageLoad={trackPageLoad}
                isSearchPage={isSearchPage}
                searchType={searchType}
                isOnModelAbTestPlp={isOnModelAbTestPlp}
                wishList={defaultWishListFromState}
                isSearchResultsAvailable={isSearchResultsAvailable}
                searchTopPromos={searchTopPromos}
                isPlcc={isPlcc}
                accessibilityLabels={accessibilityLabels}
                isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                isPlpPdpAnchoringEnabled={isPlpPdpAnchoringEnabled}
                isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                breadCrumbs={breadCrumbs}
                dynamicPromoModules={dynamicPromoModules}
                isSecureImageFlagEnabled={isSecureImageFlagEnabled}
                alternateBrand={alternateBrand}
                isShowBrandNameEnabled={isShowBrandNameEnabled}
                isBopisFilterOn={isBopisFilterOn}
                defaultStore={defaultStore}
                {...otherProps}
              />
            ) : (
              this.renderNoResponseSearchDetail()
            )}
          </div>
        ) : (
          <div>
            <SearchDetail
              filters={filters}
              formValues={formValues}
              filtersLength={filtersLength}
              getProducts={getProducts}
              isLoadingMore={isLoadingMore}
              initialValues={initialValues}
              filterCount={filterCount}
              onSubmit={onSubmit}
              products={products}
              productsBlock={productsBlock}
              totalProductsCount={totalProductsCount}
              labels={labels}
              labelsFilter={labelsFilter}
              slpLabels={slpLabels}
              searchedText={searchedText}
              sortLabels={sortLabels}
              searchResultSuggestions={searchResultSuggestions}
              currency={currency}
              currencyAttributes={currencyAttributes}
              onAddItemToFavorites={onAddItemToFavorites}
              isLoggedIn={isLoggedIn}
              isSearchListing
              asPathVal={asPathVal}
              AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
              removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
              pageNameProp={pageNameProp}
              pageSectionProp={pageSectionProp}
              pageSubSectionProp={pageSubSectionProp}
              trackPageLoad={trackPageLoad}
              isSearchPage={isSearchPage}
              searchType={searchType}
              isOnModelAbTestPlp={isOnModelAbTestPlp}
              wishList={defaultWishListFromState}
              isSearchResultsAvailable={isSearchResultsAvailable}
              searchTopPromos={searchTopPromos}
              isPlcc={isPlcc}
              accessibilityLabels={accessibilityLabels}
              isBrierleyPromoEnabled={isBrierleyPromoEnabled}
              onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
              deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
              isPlpPdpAnchoringEnabled={isPlpPdpAnchoringEnabled}
              isDynamicBadgeEnabled={isDynamicBadgeEnabled}
              breadCrumbs={breadCrumbs}
              dynamicPromoModules={dynamicPromoModules}
              isSecureImageFlagEnabled={isSecureImageFlagEnabled}
              alternateBrand={alternateBrand}
              isShowBrandNameEnabled={isShowBrandNameEnabled}
              isBopisFilterOn={isBopisFilterOn}
              defaultStore={defaultStore}
              {...otherProps}
            />
          </div>
        )}
      </React.Fragment>
    );
  }
}

SearchDetailContainer.pageInfo = {
  pageId: 'search',
  pageData: {
    pageName: 'search',
    pageSection: 'search',
    pageSubSection: 'search',
    loadAnalyticsOnload: false,
  },
};

SearchDetailContainer.getInitActions = () => initActions;

function mapStateToProps(state) {
  const productBlocks = getLoadedProductsPages(state);
  const appliedFilters = getAppliedFilters(state);

  const filtersLength = {};
  let filterCount = 0;

  // eslint-disable-next-line
  for (let key in appliedFilters) {
    if (appliedFilters[key]) {
      filtersLength[`${key}Filters`] = appliedFilters[key].length;
      filterCount += appliedFilters[key].length;
    }
  }

  const plpHorizontalPromos = getPlpHorizontalPromo(state);
  const plpGridPromos = getPLPGridPromos(state);
  const { showInGridPromos } = utils.getABtestFromState(state.AbTest);
  const isPromoApplied = checkPromoCondition(
    filterCount,
    plpHorizontalPromos,
    plpGridPromos,
    {},
    {},
    showInGridPromos
  );
  return {
    apiConfig: state.APIConfig,
    productsBlock: isPromoApplied
      ? getProductsAndTitleBlocks(
          state,
          productBlocks,
          plpGridPromos,
          plpHorizontalPromos,
          5,
          filterCount
        )
      : productBlocks,
    filterCount,
    isBopisFilterEnabled: getIsBopisFilterEnabled(state),
    products: getProductsSelect(state),
    filters: getProductsFilters(state),
    categoryId: getCategoryId(state),
    loadedProductCount: getLoadedProductsCount(state),
    unbxdId: getUnbxdId(state),
    totalProductsCount: getTotalProductsCount(state),
    // navTree: getNavigationTree(state),
    navigationData: state.Navigation && state.Navigation.navigationData,
    searchedText: getCurrentSearchForText(state),
    filtersLength,
    initialValues: {
      ...getAppliedFilters(state),
      // TODO - change after site id comes for us or ca
      sort: getAppliedSortId(state) || '',
    },
    labelsFilter: state.Labels && state.Labels.PLP && state.Labels.PLP.PLP_sort_filter,
    longDescription: getLongDescription(state),
    labels: getLabelsProductListing(state),
    plpLabels: getPlpLabelsProductListing(state),
    isLoadingMore: getIsLoadingMore(state),
    isSearchResultsAvailable: checkIfSearchResultsAvailable(state),
    isBrierleyPromoEnabled: getIsBrierleyPromoEnabled(state),
    lastLoadedPageNumber: getLastLoadedPageNumber(state),
    formValues: getFormValues('filter-form')(state),
    onSubmit: submitProductListingFiltersForm,
    currentNavIds: state.ProductListing && state.ProductListing.currentNavigationIds,
    slpLabels: getLabels(state),
    searchResultSuggestions:
      state.SearchListingPage && state.SearchListingPage.searchResultSuggestions,
    sortLabels: getSortLabels(state),
    currency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    deviceType: state.DeviceInfo && state.DeviceInfo.deviceType,
    pdpLabels: getPDPLabels(state),
    isKeepAliveEnabled: getIsKeepAliveProduct(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    AddToFavoriteErrorMsg: fetchAddToFavoriteErrorMsg(state),
    pageNameProp: getPageName(state),
    pageSectionProp: getPageSection(state),
    pageSubSectionProp: getPageSubSection(state),
    isSearchPage: getIsSearchPage(state),
    errorMessages: fetchErrorMessages(state),
    isInternationalShipping: getIsInternationalShipping(state),
    isOnModelAbTestPlp: getOnModelImageAbTestPlp(state),
    slpUrl: getSlpUrl(state),
    defaultWishListFromState: wishListFromState(state),
    isShowPriceRangeKillSwitch: getIsShowPriceRange(state),
    isHidePLPAddToBag: getIsHidePLPAddToBag(state),
    isHidePLPRatings: getIsHidePLPRatings(state),
    showPriceRangeForABTest: getABTestIsShowPriceRange(state),
    isPlcc: isPlccUser(state),
    searchTopPromos: getSLPTopPromos(state),
    accessibilityLabels: getAccessibilityLabels(state),
    deleteFavItemInProgressFlag: getDefaultFavItemInProgressFlag(state),
    shouldSlpScrollState: getShouldSlpScroll(state),
    isPlpPdpAnchoringEnabled: getIsPlpPdpAnchoringEnabled(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    isABTestForStickyFilter: getABTestForStickyFilter(state),
    isNonInvSessionFlag: getIsNonInvSessionFlag(state),
    isShowBrandNameEnabled: getIsShowBrandNameEnabled(state),
    isPromoApplied,
    alternateBrand: getAlternateBrandName(state),
    isBopisFilterOn: getIsBopisFilterOn(state),
    favStore: getDefaultStoreData(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getProducts: (payload) => {
      dispatch(getSlpProducts(payload));
    },
    onFavtLoad: (payload) => {
      dispatch(getUserFavt(payload));
    },
    getMoreProducts: (payload) => {
      dispatch(getMoreSlpProducts(payload));
    },
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    onAddItemToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
    removeAddToFavoritesErrorMsg: (payload) => {
      dispatch(removeAddToFavoriteErrorState(payload));
    },
    resetProducts: () => {
      dispatch(resetSlpProducts());
    },
    onSetLastDeletedItemIdAction: (payload) => {
      dispatch(setLastDeletedItemIdAction(payload));
    },
    resetSlpScrollState: () => {
      dispatch(setSlpScrollState(false));
    },
    resetSlpLoadingState: (payload) => {
      dispatch(setSlpLoadingState(payload));
    },
    updateFormValue: (field, value) => {
      dispatch(change('filter-form', field, value));
    },
    setBopisFilterStateActn: (payload) => {
      dispatch(setBopisFilterState(payload));
    },
    trackPageLoad: (payload) => {
      dispatch(setClickAnalyticsData(payload));
      const timer = setTimeout(() => {
        dispatch(
          trackPageView({
            props: {
              initialProps: {
                pageProps: {
                  pageData: {
                    ...payload,
                  },
                },
              },
            },
          })
        );
        const analyticsTimer = setTimeout(() => {
          dispatch(setClickAnalyticsData({}));
          clearTimeout(analyticsTimer);
        }, 200);
        clearTimeout(timer);
      }, 100);
    },
    clearPromoDataOnRouteChange: () => {
      dispatch(loadLayoutData({}, 'productListingPage'));
    },
    resetSuggestedStores: (payload) => dispatch(resetStoresByCoordinates(payload)),
    getFavoriteStoreById: (payload) => dispatch(getFavoriteStoreActn(payload)),
  };
}

SearchDetailContainer.propTypes = {
  router: PropTypes.shape({
    query: PropTypes.shape({
      searchQuery: PropTypes.string,
    }),
  }).isRequired,
  getProducts: PropTypes.func.isRequired,
  getMoreProducts: PropTypes.func.isRequired,
  // navTree: PropTypes.shape({}),
  filters: PropTypes.shape({}),
  isBopisFilterEnabled: PropTypes.bool,
  filtersLength: PropTypes.shape({}),
  initialValues: PropTypes.shape({}),
  formValues: PropTypes.shape({
    sort: PropTypes.string.isRequired,
  }).isRequired,
  onSubmit: PropTypes.func.isRequired,
  isLoadingMore: PropTypes.bool,
  isSearchResultsAvailable: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  plpLabels: PropTypes.shape({}),
  isLoggedIn: PropTypes.bool,
  productsBlock: PropTypes.arrayOf(PropTypes.shape({})),
  products: PropTypes.arrayOf({}),
  currentNavIds: PropTypes.arrayOf(PropTypes.string),
  breadCrumbs: PropTypes.shape({}),
  isInternationalShipping: PropTypes.bool.isRequired,
  searchTopPromos: PropTypes.arrayOf(
    PropTypes.shape({
      // Only including the most important property
      moduleName: PropTypes.string,
    })
  ),
  isPlcc: PropTypes.bool,
  isBrierleyPromoEnabled: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  resetSlpScrollState: PropTypes.func.isRequired,
  isPlpPdpAnchoringEnabled: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isABTestForStickyFilter: PropTypes.bool,
  isPromoApplied: PropTypes.bool,
  clearPromoDataOnRouteChange: PropTypes.func.isRequired,
  isShowBrandNameEnabled: PropTypes.bool,
};

SearchDetailContainer.defaultProps = {
  // navTree: {},
  filters: {},
  filtersLength: {},
  initialValues: {},
  isLoadingMore: false,
  isSearchResultsAvailable: false,
  pdpLabels: {},
  productsBlock: [],
  isLoggedIn: false,
  products: [],
  currentNavIds: [],
  breadCrumbs: [],
  searchTopPromos: [],
  isPlcc: false,
  isBrierleyPromoEnabled: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isPlpPdpAnchoringEnabled: false,
  isDynamicBadgeEnabled: false,
  isABTestForStickyFilter: false,
  isPromoApplied: false,
  isShowBrandNameEnabled: false,
  isBopisFilterEnabled: false,
};

export default withIsomorphicRenderer({
  WrappedComponent: SearchDetailContainer,
  mapStateToProps,
  mapDispatchToProps,
});
