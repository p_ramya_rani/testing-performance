// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import isEmpty from 'lodash/isEmpty';
import toLower from 'lodash/toLower';
import { isTier2Device, isTier3Device } from '@tcp/web/src/utils/device-tiering';
import { TIER2, TIER3 } from '@tcp/core/src/constants/tiering.constants';
import { getIsGuest, isPlccUser } from '../../../account/User/container/User.selectors';
import { generateGroups } from '../../ProductListing/container/ProductListing.util';
import { getAPIConfig, flattenArray, getLabelValue, isMobileApp } from '../../../../../utils';
import { PRODUCTS_PER_LOAD, PRODUCTS_PER_LOAD_LOWER_END_DEVICES } from './SearchDetail.constants';
import { SLP_PAGE_REDUCER_KEY } from '../../../../../constants/reducer.constants';

const getReducer = (state) => state[SLP_PAGE_REDUCER_KEY];

const getSearchListingState = (state) => {
  return state[SLP_PAGE_REDUCER_KEY];
};

export const getSlpUrl = createSelector(getSearchListingState, (slpData) => slpData.pageUrl);

const getSlpProducts = (state) => getReducer(state).products;

export default getSlpProducts;

export const giftCardProducts = (state) => getReducer(state).giftCardProducts;

const getOrganizedHeaderNavigationTree = (state) => {
  const unorganizedTree = state.Navigation.navigationData;

  // Only in browser memory, will be cleaned on page re-fresh
  // if (cachedOrganizedNavTree) {
  //   return cachedOrganizedNavTree;
  // }

  const organizedNav =
    unorganizedTree &&
    unorganizedTree.map((L1) => {
      return {
        ...L1,
        menuGroupings: generateGroups(L1),
      };
    });

  // only on browser so we dont need to keep deriving this
  if (/* isClient() && */ organizedNav && organizedNav.length) {
    // TODO - fix this - cachedOrganizedNavTree = organizedNav;
  }

  return organizedNav;
};

export const getCurrentListingIds = createSelector(
  getSearchListingState,
  (products) => products && products.currentNavigationIds
);

export const getIsSearchPage = createSelector(
  getSearchListingState,
  (ProductListing) => ProductListing && ProductListing.isSearch
);

export const getNavigationTree = (state) => {
  // const currentListingIds = state.productListing.breadcrumbs.map(crumb => crumb.pathSuffix);
  const currentListingIds = getCurrentListingIds(state);
  const navTree = getOrganizedHeaderNavigationTree(state);
  return (
    currentListingIds &&
    currentListingIds[0] &&
    navTree &&
    navTree.find((L1) => L1.categoryId === currentListingIds[0])
  );
};

export const getBreadCrumbTrail = createSelector(
  getSearchListingState,
  (products) => products && products.breadCrumbTrail
);

export const getProductsSelect = createSelector(
  getSearchListingState,
  (products) => products && products.loadedProductsPages && products.loadedProductsPages[0]
);

export const getAllProductsSelect = createSelector(getSearchListingState, (products) => {
  const allProducts = products && products.loadedProductsPages;
  return allProducts && flattenArray(allProducts);
});

export const getLabels = (state) => {
  return state.Labels.SLPs && state.Labels.SLPs.SLPs_Sub;
};

export const getTotalProductsCount = createSelector(
  getSearchListingState,
  (products) => products && products.totalProductsCount
);

export const getCurrentSearchForText = createSelector(
  getSearchListingState,
  (products) => products && products.currentListingSearchForText
);
export const getSearchTerm = createSelector(
  getSearchListingState,
  (products) => products && products.searchTerm
);

export const getAppliedFilters = createSelector(
  getSearchListingState,
  (products) => products && products.appliedFiltersIds
);

export const getAppliedSortId = createSelector(
  getSearchListingState,
  (products) => products && products.appliedSortId
);

export const getLoadedProductsCount = createSelector(getSearchListingState, (products) => {
  const allProducts = products && products.loadedProductsPages;
  const totalProductCount =
    (allProducts && allProducts.reduce((sum, item) => item.length + sum, 0)) || 0;
  return totalProductCount || 0;
});

export const getLongDescription = createSelector(
  getSearchListingState,
  (ProductListing) => ProductListing && ProductListing.currentListingDescription
);

export const getUnbxdId = createSelector(
  getSearchListingState,
  (products) => products && products.unbxdId
);

export const getLoadedProductsPages = createSelector(
  getSearchListingState,
  (products) => products && products.loadedProductsPages
);

export const getProductsFilters = createSelector(
  getSearchListingState,
  (products) => products && products.filtersMaps
);
export const getIsBopisFilterOn = createSelector(
  getSearchListingState,
  (products) => products && products.isBopisFilterOn
);
export const getLabelsProductListing = (state) => {
  if (!state.Labels || !state.Labels.PLP || !state.Labels.global)
    return {
      addToBag: '',
      readMore: '',
      readLess: '',
      shopCollection: '',
      shopthelook: '',
      youMayAlsoLike: '',
      recentlyViewed: '',
      interimSoldout: '',
      shopnow: '',
      clickHere: '',
      pointsAndRewardsText: '',
    };
  const {
    PLP: {
      plpTiles: {
        lbl_add_to_bag: addToBag,
        lbl_plpTiles_shop_collection: shopCollection,
        lbl_plpTiles_shop_the_look: shopthelook,
        lbl_page_not_found: pageNotFound,
        lbl_you_may_also_like: youMayAlsoLike,
        lbl_recently_viewed: recentlyViewed,
      },
      seoText: { lbl_read_more: readMore, lbl_read_less: readLess },
    },
    global: {
      CommonBrowseLabels: {
        lbl_category_interimSoldout: interimSoldout = '',
        lbl_common_shopNow: shopnow = '',
        lbl_category_clickHere: clickHere = '',
      },
    },
  } = state.Labels;

  const pointsAndRewardsText =
    (state.Labels &&
      state.Labels.SLPs &&
      state.Labels.SLPs.SLPs_Sub &&
      state.Labels.SLPs.SLPs_Sub.lbl_brierley_promo_text_generic) ||
    '';
  return {
    addToBag,
    readMore,
    readLess,
    shopCollection,
    shopthelook,
    pageNotFound,
    youMayAlsoLike,
    recentlyViewed,
    interimSoldout,
    shopnow,
    clickHere,
    pointsAndRewardsText,
  };
};

export const getIsDataLoading = (state) => {
  return state[SLP_PAGE_REDUCER_KEY].isDataLoading;
};

export const getIsLoadingMore = (state) => {
  return state[SLP_PAGE_REDUCER_KEY].isLoadingMore;
};

export const getModalState = (state) => {
  return state[SLP_PAGE_REDUCER_KEY].isKeepModalOpen;
};

export const checkIfSearchResultsAvailable = (state) => {
  return state[SLP_PAGE_REDUCER_KEY].isSearchResultsAvailable;
};

export const getAlternateBrandName = (state) => {
  return (state[SLP_PAGE_REDUCER_KEY] && state[SLP_PAGE_REDUCER_KEY].isSLPPage) || isMobileApp()
    ? state[SLP_PAGE_REDUCER_KEY] && state[SLP_PAGE_REDUCER_KEY].alternateBrandName
    : '';
};
export const getSpotlightReviewsUrl = () => {
  return getAPIConfig().BAZAARVOICE_SPOTLIGHT;
};

export const getCategoryId = (state) => {
  const currentNavigationIds = state.ProductListing && state.ProductListing.currentNavigationIds;
  return currentNavigationIds && currentNavigationIds[currentNavigationIds.length - 1];
};

const getPageSize = (state) => {
  let isLowerEndDevice = false;
  if (isMobileApp()) {
    isLowerEndDevice =
      state.ProductListing.deviceTier === TIER2 || state.ProductListing.deviceTier === TIER3;
  } else {
    isLowerEndDevice = isTier2Device() || isTier3Device();
  }
  return isLowerEndDevice ? PRODUCTS_PER_LOAD_LOWER_END_DEVICES : PRODUCTS_PER_LOAD;
};

export const getRedirectObj = (state) => {
  return state[SLP_PAGE_REDUCER_KEY].redirect;
};

export const getLastLoadedPageNumber = (state) => {
  // note that we do not assume all pages have the same size, to protect against BE returning less products then requested.
  return Math.ceil(getLoadedProductsCount(state) / getPageSize(state));
};

export const getMaxPageNumber = (state) => {
  // We no longer need to divide by page size because UNBXD start parameter matches the direct number of results.
  return Math.ceil(state[SLP_PAGE_REDUCER_KEY].totalProductsCount / getPageSize());
};

/**
 * @function updateAppliedFiltersInState
 * matches filterMaps with appliedFilterIds
 * and updates isSelected state in filters
 *
 * @param {*} state
 * @returns
 */
export const updateAppliedFiltersInState = (state) => {
  const filters = getProductsFilters(state);
  const appliedFilters = getAppliedFilters(state);
  const filterEntries = (filters && Object.entries(filters)) || [];
  if (appliedFilters && Object.keys(appliedFilters).length) {
    filterEntries.map((filter) => {
      const key = filter[0];
      const values = filter[1];
      const appliedFilterValue = appliedFilters[key] || [];
      if (!(values instanceof Array)) return values;

      // for all arrays in filters - update isSelected as true if it is present in appliedFilterIds
      values.map((value) => {
        const isValueApplied = appliedFilterValue.filter((id) => id === value.id).length > 0;
        const updatedValue = value;
        updatedValue.isSelected = isValueApplied;
        return updatedValue;
      });
      return values;
    });
  }

  return filters;
};

export const getScrollToTopValue = (state) => {
  return getSearchListingState(state).isScrollToTop;
};

export const getPDPLabels = (state) => {
  return {
    completeTheLook: getLabelValue(
      state.Labels,
      'lbl_complete_the_look',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    youMayAlsoLike: getLabelValue(
      state.Labels,
      'lbl_you_may_also_like',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    recentlyViewed: getLabelValue(
      state.Labels,
      'lbl_recently_viewed',
      'PDP_Sub',
      'ProductDetailPage'
    ),
  };
};

const getModulesState = (state) => {
  return state.Modules;
};

const getTopPromosState = (state) => {
  const { searchListingPage: { top: topPromos } = {} } = state.Layouts;
  return topPromos;
};

const getLoyaltyPromosState = (state) => {
  const { searchListingPage: { loyaltyBanner: loyaltyPromo } = {} } = state.Layouts;
  return loyaltyPromo;
};

const flattenPromoSet = (promo) => {
  const promoSet = {};
  if (promo.set && Array.isArray(promo.set)) {
    promo.set.forEach((i) => {
      promoSet[i.key] = i.val;
    });
  }
  return promoSet;
};

const getPLPIngridPromoCondition = (promoItem, isGuest, isPlcc) => {
  const { guest, mpr, plcc } = flattenPromoSet(promoItem);

  return (guest && isGuest) || (plcc && isPlcc) || (!isGuest && !isPlcc && mpr);
};

const getPromoModuleInfo = ({ promoItem, modules, type }) => {
  let promoModuleInfo = (promoItem.contentId && modules[promoItem.contentId]) || {};
  if (type === 'grid' || type === 'horizontal') {
    promoModuleInfo = { ...promoModuleInfo, slot: promoItem && promoItem.slotNo };
  }
  return promoModuleInfo;
};

const getUserBasedPLPPromo = ({ promoItem, modules, isGuest, isPlcc, type }) => {
  let userBasedPLPPromo;
  const promoModuleInfo = getPromoModuleInfo({ promoItem, modules, type });
  switch (toLower(promoItem.name)) {
    case 'mpr':
      userBasedPLPPromo = !isGuest && !isPlcc && { ...promoModuleInfo };
      break;
    case 'plcc':
      userBasedPLPPromo = isPlcc && { ...promoModuleInfo };
      break;
    case 'guest':
      userBasedPLPPromo = isGuest && { ...promoModuleInfo };
      break;
    default:
      userBasedPLPPromo = {};
  }
  return userBasedPLPPromo;
};

export const getSLPTopPromos = createSelector(
  getTopPromosState,
  getLoyaltyPromosState,
  getModulesState,
  getIsGuest,
  isPlccUser,
  (topPromos, loyaltyPromo, modules, isGuest, isPlcc) => {
    const loyaltyPromos =
      (loyaltyPromo &&
        loyaltyPromo.map((loyalPromo) => {
          const loyalPromoModule = loyalPromo.contentId && modules[loyalPromo.contentId];
          if (loyalPromoModule) {
            loyalPromoModule.userType = loyalPromo.name;
          }
          return loyalPromoModule || {};
        })) ||
      [];

    const promos =
      (topPromos &&
        topPromos.map((promoItem) => {
          if (promoItem.slotNo) {
            return getUserBasedPLPPromo({ promoItem, modules, isGuest, isPlcc });
          }
          if (getPLPIngridPromoCondition(promoItem, isGuest, isPlcc)) {
            return {};
          }
          return (promoItem.contentId && modules[promoItem.contentId]) || {};
        })) ||
      [];

    return loyaltyPromos.concat(promos).filter((item) => !isEmpty(item));
  }
);

export const getPLPGridPromos = (state) => {
  const { bannerInfo: { val: { grid: gridPromo } = {} } = {} } = state.SearchListingPage;
  const isGuest = getIsGuest(state);
  const isPlcc = isPlccUser(state);
  const modules = state?.Modules;
  return (
    (gridPromo &&
      gridPromo.map((promoItem) => {
        const moduleInfo =
          (promoItem.val && promoItem.val.cid && state?.Modules[promoItem.val.cid]) || {};
        if (promoItem.slotNo) {
          return getUserBasedPLPPromo({ promoItem, modules, isGuest, isPlcc, type: 'grid' });
        }
        if (getPLPIngridPromoCondition(promoItem, state?.Modules, isPlcc)) {
          return {};
        }
        return { ...moduleInfo, slot: promoItem?.val?.length && promoItem && promoItem.sub };
      })) ||
    []
  ).filter((item) => !isEmpty(item));
};

export const getPlpHorizontalPromo = (state) => {
  const { bannerInfo: { val: { horizontal: horizontalPromo } = {} } = {} } =
    state.SearchListingPage;
  const isGuest = getIsGuest(state);
  const isPlcc = isPlccUser(state);
  const modules = state?.Modules;

  return (
    (horizontalPromo &&
      horizontalPromo.map((promoItem) => {
        const horizontalModuleInfo =
          (promoItem.val && promoItem.val.cid && state?.Modules[promoItem.val.cid]) || {};
        if (promoItem.slotNo) {
          return getUserBasedPLPPromo({
            promoItem,
            modules,
            isGuest,
            isPlcc,
            type: 'horizontal',
          });
        }
        if (getPLPIngridPromoCondition(promoItem, isGuest, isPlcc)) {
          return {};
        }
        return {
          ...horizontalModuleInfo,
          slot: promoItem?.val?.length && promoItem && promoItem.sub,
        };
      })) ||
    []
  ).filter((item) => !isEmpty(item));
};

export const getShouldSlpScroll = (state) => {
  return state && state[SLP_PAGE_REDUCER_KEY] && state[SLP_PAGE_REDUCER_KEY].shouldSlpScroll;
};
