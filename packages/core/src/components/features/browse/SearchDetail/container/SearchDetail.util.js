// 9fbef606107a605d69c0edbcd8029e5d
import PropTypes from 'prop-types';
import { updateLocalStorageData } from '../../../../common/molecules/SearchBar/userRecentStore';
import { routerPush } from '../../../../../utils/index';

export const isSearched = () => {
  return true;
};

export const getLatestSearchResultsExists = (latestSearchResults) => {
  return !!(latestSearchResults && latestSearchResults.length > 0);
};

export const setDataInLocalStorage = (searchText, url) => {
  updateLocalStorageData(searchText, url);
};

export const redirectToSuggestedUrl = (searchText, url) => {
  if (searchText) {
    setDataInLocalStorage(searchText, url);
    if (url) {
      routerPush(`/c?cid=${url.split('/c/')[1]}`, `${url}`, { shallow: false });
    } else {
      routerPush(`/search?searchQuery=${searchText}`, `/search/${searchText}`, { shallow: true });
    }
  }
};

export const SearchListingViewPropTypes = {
  className: PropTypes.string,
  filters: PropTypes.shape({}),
  filtersLength: PropTypes.shape({}),
  formValues: PropTypes.shape({
    sort: PropTypes.string.isRequired,
  }).isRequired,
  getProducts: PropTypes.func.isRequired,
  getMoreProducts: PropTypes.func.isRequired,
  initialValues: PropTypes.shape({}),
  labelsFilter: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  onSubmit: PropTypes.func.isRequired,
  productsBlock: PropTypes.arrayOf(PropTypes.shape({})),
  products: PropTypes.arrayOf(PropTypes.shape({})),
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  plpLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  totalProductsCount: PropTypes.number,
  searchedText: PropTypes.string,
  slpLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  sortLabels: PropTypes.arrayOf(PropTypes.shape({})),
  currencyAttributes: PropTypes.shape({}),
  currency: PropTypes.string,
  onAddItemToFavorites: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  isLoadingMore: PropTypes.bool,
  isSearchListing: PropTypes.bool,
  asPathVal: PropTypes.string,
  searchResultSuggestions: PropTypes.arrayOf(
    PropTypes.shape({
      suggestion: PropTypes.string.isRequired,
    })
  ),
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  trackPageLoad: PropTypes.func,
  pageNameProp: PropTypes.string,
  pageSectionProp: PropTypes.string,
  pageSubSectionProp: PropTypes.string,
  searchType: PropTypes.string,
  isHidePLPRatings: PropTypes.bool,
  wishList: PropTypes.func.isRequired,
  filterCount: PropTypes.number,
  searchTopPromos: PropTypes.arrayOf(
    PropTypes.shape({
      // Only including the most important property
      moduleName: PropTypes.string,
    })
  ),
  accessibilityLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  isPlcc: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  resetSlpScrollState: PropTypes.func.isRequired,
  shouldSlpScrollState: PropTypes.bool,
  isPlpPdpAnchoringEnabled: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isABTestForStickyFilter: PropTypes.bool,
  isPromoApplied: PropTypes.bool,
  isBopisFilterEnabled: PropTypes.bool,
};

export const SearchListingViewDefaultProps = {
  className: '',
  filters: {},
  filtersLength: {},
  initialValues: {},
  labelsFilter: {},
  products: [],
  productsBlock: [],
  labels: {},
  plpLabels: {},
  totalProductsCount: 0,
  searchedText: '',
  slpLabels: {},
  sortLabels: {},
  currencyAttributes: {
    exchangevalue: 1,
  },
  currency: 'USD',
  isLoggedIn: false,
  isLoadingMore: false,
  isSearchListing: true,
  asPathVal: '',
  searchResultSuggestions: [],
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  trackPageLoad: () => {},
  pageNameProp: '',
  pageSectionProp: '',
  pageSubSectionProp: '',
  isHidePLPRatings: false,
  searchType: 'keyword',
  filterCount: 0,
  searchTopPromos: [],
  accessibilityLabels: {},
  isPlcc: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  shouldSlpScrollState: false,
  isPlpPdpAnchoringEnabled: false,
  isDynamicBadgeEnabled: false,
  isABTestForStickyFilter: false,
  isPromoApplied: false,
  isBopisFilterEnabled: false,
};
