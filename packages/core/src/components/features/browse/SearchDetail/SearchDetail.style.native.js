// 9fbef606107a605d69c0edbcd8029e5d
import { Dimensions } from 'react-native';
import styled, { css } from 'styled-components';
import { isAndroid } from '../../../../utils/utils.app';

const Screen = Dimensions.get('window');
const ScreenHeight = Screen.height;

const getAdditionalStyle = (props) => {
  const { margins, paddings } = props;
  return {
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
  };
};

const getAdditionalStylingPageContainer = (props) => {
  const { isNewReDesignProductTile, theme } = props;
  let marginTop = theme.spacing.ELEM_SPACING.XXXS;
  let paddingTop = theme.spacing.ELEM_SPACING.XXXS;

  if (isNewReDesignProductTile && isAndroid()) {
    marginTop = 0;
    paddingTop = 0;
  }
  if (isNewReDesignProductTile) {
    return `
    background: ${theme.colors.PRIMARY.PALEGRAY};
    margin-top: ${marginTop};
    padding-top: ${paddingTop};
    height: ${ScreenHeight - 120};
    `;
  }
  return ``;
};

const PageContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  height: ${ScreenHeight - 180};
  ${getAdditionalStylingPageContainer}
`;

const Container = styled.View`
  ${(props) =>
    props.changeBackground
      ? `
  background-color: ${props.theme.colorPalette.white};
  padding: 0 12px`
      : getAdditionalStyle}
`;

const AnchorContainer = styled.View`
  flex-direction: row;
`;

const AnchorStyle = {
  flexDirection: 'row',
};

const DisplaySkeleton = styled.View`
  ${(props) => (!props.renderSkeleton ? `display:none` : ``)};
  ${(props) => (props.renderSLP ? `display:none` : '')};
`;

const styles = css``;

export { styles, PageContainer, Container, AnchorContainer, AnchorStyle, DisplaySkeleton };
