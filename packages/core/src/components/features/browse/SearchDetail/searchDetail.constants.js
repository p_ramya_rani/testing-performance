// 9fbef606107a605d69c0edbcd8029e5d 
const searchLabels = {
  SEARCHED_FOR: 'You searched for',
  FILTERS: 'FILTERS',
  SORT_BY: 'SORT BY',
  SHOW_X_RESULTS: 'Showing X Results',
  SLP: 'Search Listing Page',
};
export default searchLabels;
