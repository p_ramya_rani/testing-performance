// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import { navigateToNestedRoute } from '@tcp/core/src/utils';
import { formatProductsData, filterQuantityLabels } from '@tcp/core/src/utils/utils';
import { BodyCopy, Anchor } from '../../../../common/atoms';
import withStyles from '../../../../common/hoc/withStyles.native';
import {
  styles,
  PageContainer,
  Container,
  AnchorContainer,
  AnchorStyle,
} from '../SearchDetail.style.native';
import ProductListing from '../../ProductListing/views';

let analyticsData = {};

class SearchDetail extends React.PureComponent {
  listRef;

  componentDidMount() {
    this.fireAnalyticsPageView();
  }

  componentDidUpdate(prevProps) {
    const { products: oldProducts } = prevProps;
    const { products } = this.props;
    if (!isEqual(oldProducts, products)) {
      this.fireAnalyticsPageView();
    }
  }

  fireAnalyticsPageView = () => {
    const {
      products,
      trackPageLoad,
      searchedText,
      navigation,
      isExternalCampaignFired,
      setExternalCampaignFired,
    } = this.props;
    const productsFormatted = formatProductsData(products);
    const searchedType = navigation.getParam('searchedType');
    const externalCampaignId = navigation.getParam('externalCampaignId');
    const omMID = navigation.getParam('omMID');
    const omRID = navigation.getParam('omRID');
    analyticsData = {
      currentScreen: 'searchDetail_e20',
      pageData: {
        pageName: 'search:results',
        pageType: 'search',
        pageSection: 'search',
        pageSubSection: 'search',
      },
    };

    const customEvents = ['event91', 'event92', 'event80', 'event81', 'event20'];
    if (externalCampaignId) {
      customEvents.push('event18');
      setExternalCampaignFired(true);
    }
    if (isExternalCampaignFired && !externalCampaignId) {
      customEvents.push('event19');
      setExternalCampaignFired(false);
    }

    trackPageLoad(analyticsData, {
      currentScreen: 'searchDetail_e20',
      searchType: searchedType,
      searchText: searchedText,
      products: productsFormatted,
      customEvents,
      internalCampaignId: 'non-internal campaign',
      listingCount: products.length.toString(),
      pageNavigationText: 'non-browse',
      listingSortList: 'sort=recommended',
      originType: 'internal search',
      ...(externalCampaignId && {
        externalCampaignId,
      }),
      ...(omMID && { omMID }),
      ...(omRID && { omRID }),
    });
  };

  setListRef = (ref) => {
    this.listRef = ref;
  };

  pageHasPromos = () => {
    const { searchTopPromos } = this.props;
    const promos = searchTopPromos && searchTopPromos.filter((promo) => promo && promo.moduleName);
    return promos && promos.length > 0;
  };

  changeBackground = () => {
    const { isNewReDesignEnabled } = this.props;
    if (isNewReDesignEnabled) {
      return this.pageHasPromos();
    }
    return false;
  };

  renderSearchTopSection = () => {
    const { slpLabels, navigation } = this.props;
    let searchedText = navigation && navigation.getParam('title');
    if (searchedText !== undefined) {
      searchedText = decodeURIComponent(searchedText);
      return (
        <Container margins="0 12px 0 12px" changeBackground={this.changeBackground()}>
          <BodyCopy
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs14"
            text={slpLabels.lbl_searched_for}
          />
          <BodyCopy
            color="gray.900"
            fontWeight="extrabold"
            fontFamily="secondary"
            fontSize="fs16"
            text={`"${searchedText}"`}
          />
        </Container>
      );
    }
    return null;
  };

  didYouMeanText = (text, suggestion) => {
    return (
      <BodyCopy
        margin="12px 0 0 0"
        dataLocator="slp_store_name_value"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight={suggestion ? 'black' : 'semibold'}
        color={suggestion ? 'blue.800' : 'gray.900'}
        text={text}
      />
    );
  };

  goToSearchResultsPage = (searchText) => {
    const { navigation } = this.props;
    navigateToNestedRoute(navigation, 'HomeStack', 'SearchDetail', {
      title: searchText,
      isForceUpdate: true,
    });
  };

  renderDidYouMeanTextSection = (searchResultSuggestionsArg) => {
    const { slpLabels } = this.props;
    if (searchResultSuggestionsArg !== slpLabels.lbl_no_suggestion) {
      return (
        <Container margins="0 12px 0 12px">
          <AnchorContainer>
            {this.didYouMeanText(`${slpLabels.lbl_didYouMean} `, false)}
            <Anchor
              customStyle={AnchorStyle}
              onPress={() => this.goToSearchResultsPage(searchResultSuggestionsArg.toString())}
            >
              {this.didYouMeanText(`"${searchResultSuggestionsArg}"`, true)}
              {this.didYouMeanText('?', false)}
            </Anchor>
          </AnchorContainer>
        </Container>
      );
    }
    return null;
  };

  render() {
    const {
      searchedText,
      products,
      filters,
      totalProductsCount,
      filtersLength,
      labelsFilter,
      labels,
      isLoadingMore,
      lastLoadedPageNumber,
      submitProductListingFiltersForm,
      getProducts,
      navigation,
      sortLabels,
      onAddItemToFavorites,
      isLoggedIn,
      labelsLogin,
      searchResultSuggestions,
      slpLabels,
      isKeepModalOpen,
      wishList,
      searchTopPromos,
      isPlcc,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      isNewReDesignEnabled,
      alternateBrand,
      ...otherProps
    } = this.props;

    const filterMaps = filterQuantityLabels(filters, slpLabels);
    const searchResultSuggestionsArg =
      searchResultSuggestions && searchResultSuggestions.length
        ? searchResultSuggestions.map((searchSuggestion) => searchSuggestion.suggestion)
        : slpLabels.lbl_no_suggestion;

    return (
      <PageContainer isNewReDesignProductTile={isNewReDesignEnabled}>
        {searchResultSuggestionsArg !== slpLabels.lbl_no_suggestion
          ? this.renderDidYouMeanTextSection(searchResultSuggestionsArg)
          : this.renderSearchTopSection()}
        <ProductListing
          setListRef={this.setListRef}
          products={products}
          filters={filterMaps}
          totalProductsCount={totalProductsCount}
          filtersLength={filtersLength}
          labelsFilter={labelsFilter}
          labels={labels}
          isLoadingMore={isLoadingMore}
          lastLoadedPageNumber={lastLoadedPageNumber}
          onSubmit={submitProductListingFiltersForm}
          getProducts={getProducts}
          navigation={navigation}
          onGoToPDPPage={this.onGoToPDPPage}
          sortLabels={sortLabels}
          onLoadMoreProducts={this.onLoadMoreProducts}
          onAddItemToFavorites={onAddItemToFavorites}
          isLoggedIn={isLoggedIn}
          labelsLogin={labelsLogin}
          isSearchListing
          isKeepModalOpen={isKeepModalOpen}
          wishList={wishList}
          plpTopPromos={searchTopPromos}
          isPlcc={isPlcc}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          isNewReDesignProductTile={isNewReDesignEnabled}
          alternateBrand={alternateBrand}
          {...otherProps}
        />
      </PageContainer>
    );
  }
}

SearchDetail.propTypes = {
  searchedText: PropTypes.string,
  products: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  filters: PropTypes.shape({}).isRequired,
  totalProductsCount: PropTypes.number.isRequired,
  filtersLength: PropTypes.shape({}).isRequired,
  labelsFilter: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}),
  isLoadingMore: PropTypes.bool.isRequired,
  lastLoadedPageNumber: PropTypes.number.isRequired,
  submitProductListingFiltersForm: PropTypes.func.isRequired,
  getProducts: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  slpLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  sortLabels: PropTypes.arrayOf(PropTypes.shape({})),
  onAddItemToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  labelsLogin: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  searchResultSuggestions: PropTypes.arrayOf(PropTypes.shape({})),
  isKeepModalOpen: PropTypes.bool,
  trackPageLoad: PropTypes.func,
  wishList: PropTypes.shape({}).isRequired,
  isExternalCampaignFired: PropTypes.bool.isRequired,
  setExternalCampaignFired: PropTypes.func,
  searchTopPromos: PropTypes.arrayOf(
    PropTypes.shape({
      // Only including the most important property
      moduleName: PropTypes.string,
    })
  ),
  isPlcc: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isNewReDesignEnabled: PropTypes.bool,
};

SearchDetail.defaultProps = {
  searchedText: '',
  labels: {},
  slpLabels: {},
  sortLabels: {},
  onAddItemToFavorites: null,
  isLoggedIn: false,
  labelsLogin: {},
  searchResultSuggestions: [],
  isKeepModalOpen: false,
  trackPageLoad: () => {},
  setExternalCampaignFired: () => {},
  searchTopPromos: [],
  isPlcc: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isDynamicBadgeEnabled: {},
  isNewReDesignEnabled: false,
};

export default withStyles(SearchDetail, styles);
export { SearchDetail as SearchDetailVanilla };
