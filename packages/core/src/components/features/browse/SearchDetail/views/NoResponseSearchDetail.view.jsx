/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { getSiteId } from '@tcp/core/src/utils/utils';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { getSearchRedirectObj } from '@tcp/core/src/utils/utils.web';
import { getIcidValue, internalCampaignProductAnalyticsList } from '@tcp/core/src/utils';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import { isTier1Device } from '@tcp/web/src/utils/device-tiering';
import { setProp } from '@tcp/core/src/analytics/utils';
import withStyles from '../../../../common/hoc/withStyles';
import SearchListingStyle from '../SearchDetail.style';
import { Anchor, Row, Col, BodyCopy } from '../../../../common/atoms';
import {
  getSearchResult,
  setNOSearchResultPageFlag,
  manageHandleClickOutside,
  resetSlpProducts,
} from '../container/SearchDetail.actions';
import { updateLocalStorageData } from '../../../../common/molecules/SearchBar/userRecentStore';
import { routerPush } from '../../../../../utils/index';
import CONSTANTS from '../../../../common/molecules/SearchBar/SearchBar.constants';
import NoResponseInputContainer from './NoResponseInputContainer.view';
import BopisFilter from '../../ProductListing/molecules/BopisFilter/views/BopisFilter';
import BopisFilterEmptyResults from '../../ProductListing/molecules/BopisFilterEmptyResults';

class NoResponseSearchDetailView extends React.PureComponent {
  componentDidMount() {
    const { searchType, searchedText } = this.props;

    if (searchType && searchedText) {
      this.triggerAnalyticsCall();
    }
    this.willAlsoViewedCarouselDisplay = isTier1Device();
  }

  triggerAnalyticsCall = () => {
    const { trackPageLoad, searchType, searchedText, asPathVal } = this.props;
    const icid = getIcidValue(asPathVal);
    const products = internalCampaignProductAnalyticsList();
    const analyticsEvents = ['event91', 'event92', 'event80'];

    if (icid) {
      analyticsEvents.push('event81');
    }

    const searchRedirect = getSearchRedirectObj();
    if (searchRedirect && !searchRedirect.isDone) {
      analyticsEvents.push('event21');
    }
    setProp('eVar30', 'zero');
    if (!icid) {
      setProp('eVar94', '+1');
    }
    trackPageLoad({
      pageSearchType: icid ? '' : searchType,
      pageSearchText: icid ? '' : searchedText,
      pageType: 'search',
      pageName: 'search:results',
      pageSection: 'search',
      pageSubSection: 'search',
      customEvents: analyticsEvents,
      pageNavigationText: '',
      listingCount: 'zero',
      internalCampaignIdList: products,
      ...(icid && { internalCampaignId: icid }),
    });
  };

  setDataInLocalStorage = (searchText, url) => {
    updateLocalStorageData(searchText, url);
  };

  redirectToSuggestedUrl = (searchText, url) => {
    if (searchText) {
      this.setDataInLocalStorage(searchText, url);
      if (url) {
        routerPush(
          `/c?searchType=${CONSTANTS.ANALYTICS_TYPEAHEAD_CATEGORY}&cid=${url.split('/c/')[1]}`,
          `${url}`,
          { shallow: false }
        );
      } else {
        routerPush(
          `/search?searchQuery=${searchText}&searchType=typeahead:keyword`,
          `/search/${searchText}`,
          { shallow: true }
        );
      }
    }
  };

  render() {
    const {
      className,
      slpLabels,
      searchedText,
      searchResultSuggestions,
      searchResults,
      startSearch,
      labels,
      showProduct,
      showRecent,
      toggleSearchFlag,
      manageHandleClickOutsideFunc,
      resetProducts,
      isBopisFilterOn,
      formValues,
      getProducts,
      onSubmit,
      updateFormValue,
      setBopisFilterStateActn,
      plpLabels,
      isBopisFilterEnabled,
      resetSuggestedStores,
    } = this.props;
    const showNewBopisFilterEmptyMessage = isBopisFilterOn;

    const getEmptyLabelVal =
      searchedText.indexOf('?') > 0 ? searchedText.split('?')[0] : searchedText;
    const searchResultSuggestionsArg =
      searchResultSuggestions && searchResultSuggestions.length
        ? searchResultSuggestions.map((searchSuggestion) => searchSuggestion.suggestion)
        : slpLabels.lbl_no_suggestion;

    const recommendationAttributes = {
      variations: 'moduleO',
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.NULL_SEARCH,
      showLoyaltyPromotionMessage: false,
      headerAlignment: 'left',
    };
    return (
      <div className={className}>
        <Row className="search-by-keywords-container">
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <BodyCopy
              fontSize={['fs16', 'fs14', 'fs14']}
              component="div"
              fontFamily="secondary"
              fontWeight="regular"
            >
              {slpLabels.lbl_searched_for}
              <span className="empty-searched-label">{` "${getEmptyLabelVal}"`}</span>
            </BodyCopy>
          </Col>
          {isBopisFilterEnabled && (
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <BopisFilter
                isBopisFilterOn={isBopisFilterOn}
                labels={plpLabels}
                formValues={formValues}
                getProducts={getProducts}
                onSubmit={onSubmit}
                updateFormValue={updateFormValue}
                setBopisFilterStateActn={setBopisFilterStateActn}
                defaultStoreData={JSON.parse(getLocalStorage('defaultStore')) || {}}
                resetSuggestedStores={resetSuggestedStores}
              />
            </Col>
          )}
        </Row>
        {showNewBopisFilterEmptyMessage && (
          <Row>
            <BopisFilterEmptyResults labels={plpLabels} />
          </Row>
        )}
        {!showNewBopisFilterEmptyMessage && (
          <Row className="empty-search-result-title">
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <BodyCopy
                component="div"
                fontSize={['fs16', 'fs32', 'fs32']}
                fontFamily="secondary"
                fontWeight="semibold"
                textAlign="center"
              >
                {slpLabels.lbl_nothing_matched}
                <span className="empty-searched-label-title">{` "${getEmptyLabelVal}"`}</span>
              </BodyCopy>
            </Col>
          </Row>
        )}
        {!showNewBopisFilterEmptyMessage &&
          searchResultSuggestionsArg !== slpLabels.lbl_no_suggestion && (
            <Row className="empty-search-result-suggestion">
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <BodyCopy
                  fontSize={['fs16', 'fs32', 'fs32']}
                  component="div"
                  fontFamily="secondary"
                  fontWeight="semibold"
                  textAlign="center"
                >
                  {`${slpLabels.lbl_didYouMean} "`}
                  <Anchor
                    noLink
                    className="suggestion-label"
                    to={`/${getSiteId()}/search/${searchResultSuggestionsArg}`}
                    onClick={(e) => {
                      e.preventDefault();
                      resetProducts();
                      this.redirectToSuggestedUrl(`${searchResultSuggestionsArg}`);
                    }}
                  >
                    {` ${searchResultSuggestionsArg}`}
                  </Anchor>
                  &quot?
                </BodyCopy>
              </Col>
            </Row>
          )}
        <Row className="empty-search-inputBox-container-wrapper">
          <Col className="empty-search-inputBox-col" colSize={{ small: 6, medium: 8, large: 3 }}>
            <NoResponseInputContainer
              className={className}
              slpLabels={slpLabels}
              searchResults={searchResults}
              startSearch={startSearch}
              labels={labels}
              redirectToSuggestedUrl={this.redirectToSuggestedUrl}
              showProduct={showProduct}
              showRecent={showRecent}
              toggleSearchFlag={toggleSearchFlag}
              manageHandleClickOutsideFunc={manageHandleClickOutsideFunc}
            />
          </Col>
        </Row>

        <Row className="search-tips-message-container-wrapper">
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <BodyCopy fontSize="fs16" component="div" fontFamily="secondary" textAlign="center">
              <BodyCopy
                fontSize={['fs12', 'fs16', 'fs16']}
                component="div"
                textAlign="center"
                fontWeight="black"
                className="empty-search-tips-title"
                fontFamily="secondary"
              >
                {slpLabels.lbl_tips}
              </BodyCopy>
              <BodyCopy className="empty-search-tips-items">
                <BodyCopy
                  fontSize={['fs12', 'fs16', 'fs16']}
                  color="gray.1000"
                  fontFamily="secondary"
                  textAlign="center"
                >
                  {slpLabels.lbl_check_your_spelling}
                </BodyCopy>
                <BodyCopy
                  fontSize={['fs12', 'fs16', 'fs16']}
                  color="gray.1000"
                  fontFamily="secondary"
                  textAlign="center"
                >
                  {slpLabels.lbl_simplified_keywords}
                </BodyCopy>
                <BodyCopy
                  fontSize={['fs12', 'fs16', 'fs16']}
                  color="gray.1000"
                  fontFamily="secondary"
                  textAlign="center"
                >
                  {slpLabels.lbl_try_searching}
                </BodyCopy>
                <BodyCopy
                  fontSize={['fs12', 'fs16', 'fs16']}
                  color="gray.1000"
                  fontFamily="secondary"
                  textAlign="center"
                >
                  {slpLabels.lbl_narrow_searches}
                </BodyCopy>
              </BodyCopy>
            </BodyCopy>
          </Col>
        </Row>
        <Row>
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <div className={`${className} product-description-list`}>
              <Recommendations
                headerLabel={slpLabels.lbl_you_may_also_like}
                {...recommendationAttributes}
                sequence="1"
              />
            </div>
          </Col>
        </Row>
        {this.willAlsoViewedCarouselDisplay && (
          <Row>
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <div className="product-detail-section">
                <Recommendations
                  headerLabel={slpLabels.lbl_recently_viewed}
                  portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
                  sequence="2"
                  {...recommendationAttributes}
                />
              </div>
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

NoResponseSearchDetailView.propTypes = {
  className: PropTypes.string,
  slpLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  searchedText: PropTypes.string,
  searchResultSuggestions: PropTypes.arrayOf(
    PropTypes.shape({
      suggestion: PropTypes.string.isRequired,
    })
  ),
  startSearch: PropTypes.func.isRequired,
  searchResults: PropTypes.shape({
    trends: PropTypes.shape({}),
    categories: PropTypes.shape({}),
    products: PropTypes.shape({}),
  }),
  labels: PropTypes.shape({
    lbl_search_whats_trending: PropTypes.string,
    lbl_search_recent_search: PropTypes.string,
    lbl_search_looking_for: PropTypes.string,
    lbl_search_product_matches: PropTypes.string,
  }),
  trackPageLoad: PropTypes.func,
  searchType: PropTypes.string,
  showProduct: PropTypes.bool,
  showRecent: PropTypes.bool,
  toggleSearchFlag: PropTypes.func,
  manageHandleClickOutsideFunc: PropTypes.func,
  resetProducts: PropTypes.func,
  asPathVal: PropTypes.string,
};

NoResponseSearchDetailView.defaultProps = {
  className: '',
  slpLabels: {},
  searchedText: '',
  searchResultSuggestions: [],
  searchResults: {
    trends: {},
    categories: {},
    products: {},
  },
  labels: PropTypes.shape({
    lbl_search_whats_trending: '',
    lbl_search_recent_search: '',
    lbl_search_looking_for: '',
    lbl_search_product_matches: '',
  }),
  searchType: 'keyword',
  trackPageLoad: () => {},
  showProduct: false,
  showRecent: false,
  toggleSearchFlag: () => {},
  manageHandleClickOutsideFunc: () => {},
  resetProducts: () => {},
  asPathVal: '',
};

const mapStateToProps = (state) => {
  return {
    searchResults: state.Search.searchResults,
    labels: state.Labels.global && state.Labels.global.Search,
    showProduct: state.SearchListingPage.showProduct,
    showRecent: state.SearchListingPage.showRecent,
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    startSearch: (payload) => {
      dispatch(getSearchResult(payload));
    },
    toggleSearchFlag: (payload) => {
      dispatch(setNOSearchResultPageFlag(payload));
    },
    manageHandleClickOutsideFunc: (payload) => {
      dispatch(manageHandleClickOutside(payload));
    },
    resetProducts: (payload) => {
      dispatch(resetSlpProducts(payload));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(NoResponseSearchDetailView, SearchListingStyle));
