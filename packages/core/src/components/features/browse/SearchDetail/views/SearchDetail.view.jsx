/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { withRouter } from 'next/router';
import isEqual from 'lodash/isEqual';
import { getSiteId } from '@tcp/core/src/utils/utils';
import {
  getIcidValue,
  isTCP,
  fireEnhancedEcomm,
  filterQuantityLabels,
  scrollToBreadCrumb,
  isMobileWeb,
} from '@tcp/core/src/utils';
import { getSearchRedirectObj } from '@tcp/core/src/utils/utils.web';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf/RenderPerf';
import { CONTROLS_VISIBLE, RESULTS_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import { setProp } from '@tcp/core/src/analytics/utils';
import {
  SearchListingViewPropTypes,
  SearchListingViewDefaultProps,
  redirectToSuggestedUrl,
} from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.util';
import withStyles from '../../../../common/hoc/withStyles';
import SearchListingStyle from '../SearchDetail.style';
import ProductsGrid from '../../ProductListing/molecules/ProductsGrid/views';
import { Anchor, Row, Col, PLPSkeleton } from '../../../../common/atoms';
import LoadedProductsCount from '../../ProductListing/molecules/LoadedProductsCount/views';
import errorBoundary from '../../../../common/hoc/withErrorBoundary';
import BodyCopy from '../../../../common/atoms/BodyCopy';
import { isFiltersAvailable } from '../../ProductListing/container/ProductListing.selectors';
import ProductListingFiltersForm from '../../ProductListing/molecules/ProductListingFiltersForm';
import PromoModules from '../../../../common/organisms/PromoModules';
import BopisFilter from '../../ProductListing/molecules/BopisFilter/views/BopisFilter';

class SearchListingView extends React.Component {
  shouldComponentUpdate(nextProps) {
    if (isEqual(nextProps, this.props)) {
      return false;
    }
    return true;
  }

  componentDidUpdate(prevProps) {
    const { products, isPlpPdpAnchoringEnabled, shouldSlpScrollState, resetSlpScrollState } =
      this.props;
    const productsFormatted = this.formatProductsData(products);

    if (prevProps.products !== products && productsFormatted.length) {
      this.triggerAnalyticsCall();
    }
    if (isPlpPdpAnchoringEnabled && shouldSlpScrollState && isMobileWeb()) {
      scrollToBreadCrumb();
      resetSlpScrollState();
    }
  }

  getSearchRedirection = (searchedText) => {
    const searchRedirect = getSearchRedirectObj();
    if (searchRedirect && searchRedirect.isDone) return searchRedirect.searchTerm;
    return searchedText;
  };

  triggerAnalyticsCall = () => {
    const {
      products,
      trackPageLoad,
      searchType,
      searchedText,
      asPathVal,
      filterCount,
      initialValues,
      totalProductsCount,
      currency,
    } = this.props;

    const { sort } = initialValues;
    const productsFormatted = this.formatProductsData(products);
    const icid = getIcidValue(asPathVal);
    let analyticsEvents = ['event91', 'event92', 'event80', 'event81'];
    let internalCampaignId = '';
    const searchTerm = this.getSearchRedirection(searchedText);
    let pageSearchText = icid ? '' : searchTerm;
    let listingCount = products ? totalProductsCount : '';
    let pageSearchType = icid ? '' : searchType;
    if (!icid) {
      analyticsEvents = ['event91', 'event92', 'event80', 'event20'];
      setProp('eVar94', '+1');
    }

    if (icid) {
      internalCampaignId = icid;
    }

    // Ignore below variables if sort or filter applied
    if (sort || filterCount) {
      analyticsEvents = ['event91', 'event92', 'event80'];
      internalCampaignId = ''; // evar19
      pageSearchText = ''; // evar26
      listingCount = 0; // evar27
      pageSearchType = ''; // eVar59
    } else {
      setProp('eVar22', 'internal search');
    }

    trackPageLoad({
      products: productsFormatted,
      pageSearchType,
      pageSearchText,
      pageType: 'search',
      pageName: 'search:results',
      pageSection: 'search',
      pageSubSection: 'search',
      customEvents: analyticsEvents,
      listingCount,
      listingSortList: 'sort=recommended',
      internalCampaignId,
    });
    fireEnhancedEcomm({
      eventName: 'productimpression',
      productsObj: productsFormatted,
      eventType: 'impressions',
      pageName: 'Search',
      currCode: currency,
    });
  };

  formatProductsData = (products) => {
    const { currency } = this.props;
    return products && products.length
      ? products.map((tile, index) => {
          const {
            productInfo: { listPrice, offerPrice, generalProductId, priceRange },
            miscInfo: { categoryName },
          } = tile;
          return {
            id: generalProductId,
            colorId: generalProductId,
            price: offerPrice,
            dimension35: isTCP ? 'TCP' : 'GYM',
            listPrice,
            extPrice: priceRange.lowOfferPrice,
            dimension63: `${offerPrice}${currency} ${listPrice}${currency}`,
            dimension67: offerPrice < listPrice ? 'on sale' : 'full price',
            dimension80: categoryName,
            dimension89: generalProductId,
            dimension100: index + 1,
          };
        })
      : [];
  };

  getSearchString = (text) => {
    const { isLoadingMore, totalProductsCount } = this.props;
    if (!isLoadingMore && totalProductsCount > 0) {
      return text;
    }
    return '';
  };

  getSkeltonBlock = (products) => {
    const { isLoadingMore } = this.props;
    return isLoadingMore || JSON.stringify(products) === '{}' ? <PLPSkeleton col={20} /> : null;
  };

  getSearchResultSuggestionsArg = () => {
    const { searchResultSuggestions, slpLabels } = this.props;
    return searchResultSuggestions && searchResultSuggestions.length
      ? searchResultSuggestions.map((searchSuggestion) => searchSuggestion.suggestion)
      : slpLabels.lbl_no_suggestion;
  };

  renderLoadedProductCount = () => {
    const { totalProductsCount, slpLabels } = this.props;
    const { lbl_item: ItemLabel, lbl_items: ItemsLabel } = slpLabels || {};

    return (
      <BodyCopy
        className="new-product-count-text-search"
        component="span"
        fontSize="fs12"
        fontFamily="secondary"
      >
        {`(${totalProductsCount} ${totalProductsCount > 1 ? ItemsLabel : ItemLabel})`}
      </BodyCopy>
    );
  };

  renderOldLoadedProductsCount = () => {
    const { totalProductsCount, slpLabels, isABTestForStickyFilter } = this.props;
    return !isABTestForStickyFilter ? (
      <Row>
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <LoadedProductsCount
            className="show-items-count-section"
            totalProductsCount={totalProductsCount}
            showingItemsLabel={slpLabels}
          />
        </Col>
      </Row>
    ) : null;
  };

  renderNewSearchTitle = () => {
    const { searchedText, className, slpLabels, isABTestForStickyFilter } = this.props;

    return isABTestForStickyFilter ? (
      <BodyCopy component="span" fontFamily="secondary" fontSize="fs18" fontWeight="extrabold">
        {`${this.getSearchString(slpLabels.lbl_results_for)} ${this.getSearchString(
          `"${searchedText.split('?')[0]}"`
        )}`}
        {this.renderLoadedProductCount()}
      </BodyCopy>
    ) : (
      <BodyCopy
        className={`${className} searched-text-wrapper`}
        component="div"
        fontFamily="secondary"
        fontSize="fs14"
        fontWeight="regular"
      >
        {this.getSearchString(slpLabels.lbl_searched_for)}
        <BodyCopy
          fontFamily="secondary"
          className="searched-label"
          fontSize={['fs16', 'fs16', 'fs14']}
          fontWeight="extrabold"
        >
          {this.getSearchString(`"${searchedText.split('?')[0]}"`)}
        </BodyCopy>
      </BodyCopy>
    );
  };

  renderSearchKeyword = () => {
    const { searchedText, isABTestForStickyFilter } = this.props;
    return (
      <Row className={isABTestForStickyFilter ? 'new-searched-text-title' : null}>
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          {searchedText && this.renderNewSearchTitle()}
        </Col>
      </Row>
    );
  };

  render() {
    const {
      className,
      products,
      productsBlock,
      labels,
      plpLabels,
      totalProductsCount,
      searchedText,
      slpLabels,
      sortLabels,
      filters,
      filtersLength,
      formValues,
      getProducts,
      initialValues,
      labelsFilter,
      onSubmit,
      currency,
      currencyAttributes,
      onAddItemToFavorites,
      isLoggedIn,
      isLoadingMore,
      isSearchListing,
      searchResultSuggestions,
      asPathVal,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      wishList,
      isHidePLPRatings,
      searchTopPromos,
      isPlcc,
      accessibilityLabels,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      shouldSlpScrollState,
      resetSlpScrollState,
      isPlpPdpAnchoringEnabled,
      isDynamicBadgeEnabled,
      isABTestForStickyFilter,
      isPromoApplied,
      breadCrumbs,
      dynamicPromoModules,
      isSecureImageFlagEnabled,
      alternateBrand,
      isShowBrandNameEnabled,
      isBopisFilterOn,
      updateFormValue,
      setBopisFilterStateActn,
      isBopisFilterEnabled,
      defaultStore,
      ...otherProps
    } = this.props;

    const { resetSuggestedStores } = otherProps;

    const searchResultSuggestionsArg = this.getSearchResultSuggestionsArg();

    const filterMaps = filterQuantityLabels(filters, slpLabels);

    return (
      <div className={className}>
        {searchResultSuggestionsArg !== slpLabels.lbl_no_suggestion && (
          <Row className="empty-search-result-suggestion">
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <BodyCopy
                fontSize={['fs22', 'fs24', 'fs32']}
                component="div"
                fontFamily="secondary"
                fontWeight="semibold"
                textAlign="center"
              >
                {`${slpLabels.lbl_didYouMean} "`}
                <Anchor
                  noLink
                  className="suggestion-label"
                  to={`/${getSiteId()}/search/${searchResultSuggestionsArg}`}
                  onClick={(e) => {
                    e.preventDefault();
                    redirectToSuggestedUrl(`${searchResultSuggestionsArg}`);
                  }}
                >
                  {`${searchResultSuggestionsArg}`}
                </Anchor>
                &quot?
              </BodyCopy>
            </Col>
          </Row>
        )}
        {this.renderSearchKeyword()}
        <Row>
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            {searchTopPromos && searchTopPromos.length > 0 && (
              <PromoModules
                className="search-promo-container"
                plpTopPromos={searchTopPromos}
                asPath={asPathVal}
                isLoggedIn={isLoggedIn}
                isPlcc={isPlcc}
                accessibilityLabels={accessibilityLabels}
                dynamicPromoModules={dynamicPromoModules}
              />
            )}
          </Col>
        </Row>
        {isBopisFilterEnabled && (
          <Row>
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <BopisFilter
                isBopisFilterOn={isBopisFilterOn}
                labels={plpLabels}
                formValues={formValues}
                getProducts={getProducts}
                onSubmit={onSubmit}
                updateFormValue={updateFormValue}
                setBopisFilterStateActn={setBopisFilterStateActn}
                defaultStoreData={defaultStore}
                resetSuggestedStores={resetSuggestedStores}
              />
            </Col>
          </Row>
        )}
        {isABTestForStickyFilter ? (
          <Row>
            <Col colSize={{ small: 6, medium: 8, large: 2 }}>
              <div className="sidebar desktop-filter-sidebar">
                <ProductListingFiltersForm
                  isFilterBy={isFiltersAvailable(filters)}
                  filtersMaps={filterMaps}
                  totalProductsCount={totalProductsCount}
                  initialValues={initialValues}
                  filtersLength={filtersLength}
                  labels={labelsFilter}
                  onSubmit={onSubmit}
                  formValues={formValues}
                  sortLabels={sortLabels}
                  getProducts={getProducts}
                  slpLabels={slpLabels}
                  isLoadingMore={isLoadingMore}
                  isSearchListing={isSearchListing}
                  isABTestForStickyFilter={isABTestForStickyFilter}
                  isBopisFilterOn={isBopisFilterOn}
                />
              </div>

              {/* UX timer */}
              <RenderPerf.Measure name={CONTROLS_VISIBLE} />
            </Col>
            <Col colSize={{ small: 6, medium: 8, large: 10 }}>
              {productsBlock.length ? (
                <>
                  <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                    <ProductListingFiltersForm
                      isFilterBy={isFiltersAvailable(filters)}
                      filtersMaps={filterMaps}
                      totalProductsCount={totalProductsCount}
                      initialValues={initialValues}
                      filtersLength={filtersLength}
                      labels={labelsFilter}
                      onSubmit={onSubmit}
                      formValues={formValues}
                      sortLabels={sortLabels}
                      getProducts={getProducts}
                      slpLabels={slpLabels}
                      isLoadingMore={isLoadingMore}
                      isSearchListing={isSearchListing}
                      isABTestForStickyFilter={isABTestForStickyFilter}
                      isBopisFilterOn={isBopisFilterOn}
                    />
                  </Col>

                  <ProductsGrid
                    className={className}
                    productsBlock={productsBlock}
                    products={products}
                    labels={labels}
                    productTileVariation="search-product-tile"
                    currencyAttributes={currencyAttributes}
                    currency={currency}
                    onAddItemToFavorites={onAddItemToFavorites}
                    isLoggedIn={isLoggedIn}
                    isLoadingMore={isLoadingMore}
                    isSearchListing={isSearchListing}
                    getProducts={getProducts}
                    asPathVal={asPathVal}
                    AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                    removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                    totalProductsCount={totalProductsCount}
                    isHidePLPRatings={isHidePLPRatings}
                    wishList={wishList}
                    onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                    deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                    isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                    isABTestForStickyFilter={isABTestForStickyFilter}
                    isPlcc={isPlcc}
                    isPromoApplied={isPromoApplied}
                    breadCrumbs={breadCrumbs}
                    isSecureImageFlagEnabled={isSecureImageFlagEnabled}
                    alternateBrand={alternateBrand}
                    isShowBrandNameEnabled={isShowBrandNameEnabled}
                    isBopisFilterOn={isBopisFilterOn}
                    {...otherProps}
                  />
                  {/* UX timer */}
                  <RenderPerf.Measure name={RESULTS_VISIBLE} />
                </>
              ) : null}
              {this.getSkeltonBlock(products)}
            </Col>
          </Row>
        ) : (
          <>
            <Row>
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <ProductListingFiltersForm
                  isFilterBy={isFiltersAvailable(filters)}
                  filtersMaps={filterMaps}
                  totalProductsCount={totalProductsCount}
                  initialValues={initialValues}
                  filtersLength={filtersLength}
                  labels={labelsFilter}
                  onSubmit={onSubmit}
                  formValues={formValues}
                  sortLabels={sortLabels}
                  getProducts={getProducts}
                  slpLabels={slpLabels}
                  isLoadingMore={isLoadingMore}
                  isSearchListing={isSearchListing}
                  isBopisFilterOn={isBopisFilterOn}
                />
                {/* UX timer */}
                <RenderPerf.Measure name={CONTROLS_VISIBLE} />
              </Col>
            </Row>
            {this.renderOldLoadedProductsCount()}
            <Row>
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                {productsBlock.length ? (
                  <>
                    <ProductsGrid
                      className={className}
                      productsBlock={productsBlock}
                      products={products}
                      labels={labels}
                      productTileVariation="search-product-tile"
                      currencyAttributes={currencyAttributes}
                      currency={currency}
                      onAddItemToFavorites={onAddItemToFavorites}
                      isLoggedIn={isLoggedIn}
                      isLoadingMore={isLoadingMore}
                      isSearchListing={isSearchListing}
                      getProducts={getProducts}
                      asPathVal={asPathVal}
                      AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                      removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                      totalProductsCount={totalProductsCount}
                      isHidePLPRatings={isHidePLPRatings}
                      wishList={wishList}
                      onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                      deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                      isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                      isPlcc={isPlcc}
                      isPromoApplied={isPromoApplied}
                      isSecureImageFlagEnabled={isSecureImageFlagEnabled}
                      alternateBrand={alternateBrand}
                      isShowBrandNameEnabled={isShowBrandNameEnabled}
                      isBopisFilterOn={isBopisFilterOn}
                      {...otherProps}
                    />
                    {/* UX timer */}
                    <RenderPerf.Measure name={RESULTS_VISIBLE} />
                  </>
                ) : null}
                {isLoadingMore || JSON.stringify(products) === '{}' ? (
                  <PLPSkeleton col={20} />
                ) : null}
              </Col>
            </Row>
          </>
        )}
      </div>
    );
  }
}

SearchListingView.propTypes = SearchListingViewPropTypes;
SearchListingView.defaultProps = SearchListingViewDefaultProps;
const SearchListingViewWithRouter = withRouter(SearchListingView);

export default errorBoundary(withStyles(SearchListingViewWithRouter, SearchListingStyle));
export { SearchListingViewWithRouter as SearchListingViewVanilla };
