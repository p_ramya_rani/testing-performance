// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import enhanceWithClickOutside from 'react-click-outside';
import { getIconPath } from '@tcp/core/src/utils';
import { Image } from '@tcp/core/src/components/common/atoms';
import { breakpoints } from '@tcp/core/styles/themes/TCP/mediaQuery';
import { BodyCopy } from '../../../../common/atoms';
import withStyles from '../../../../common/hoc/withStyles';
import SearchListingStyle from '../SearchDetail.style';
import SuggestionBox from '../../../../common/molecules/SearchBar/views/SuggestionBox.view';
import CategoryMatches from './CategoryMatches.view';
import { getLatestSearchResultsExists } from '../container/SearchDetail.util';
import { getRecentStoreFromLocalStorage } from '../../../../common/molecules/SearchBar/userRecentStore';

class NoResponseInputContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isSearchOpen: false,
    };
    this.searchInput = React.createRef();
    this.changeSearchText = this.changeSearchText.bind(this);
    this.getSearchResults = this.getSearchResults.bind(this);
    this.openSearchBar = this.openSearchBar.bind(this);
  }

  changeSearchText = (e) => {
    e.preventDefault();
    const { startSearch, slpLabels, toggleSearchFlag } = this.props;

    let searchText = this.searchInput.current.value;
    searchText = searchText.replace(/ %|% |%/g, ' ').trim();
    const termLength = 1;
    if (searchText.length <= termLength) {
      toggleSearchFlag(false);
    } else {
      const payload = {
        slpLabels,
        searchText,
      };
      startSearch(payload);
    }
  };

  startInitiateSearch = () => {
    const { redirectToSuggestedUrl } = this.props;
    let searchText = this.searchInput.current.value;
    if (searchText) {
      searchText = searchText.toLowerCase();
      searchText = searchText.replace(/ %|% |%/g, ' ').trim();
      redirectToSuggestedUrl(searchText);
    }
  };

  initiateSearchBySubmit = (e) => {
    e.preventDefault();
    this.startInitiateSearch();
  };

  getSearchResults = (e) => {
    const { redirectToSuggestedUrl } = this.props;

    e.preventDefault();
    let searchText = this.searchInput.current.value;
    searchText = searchText.replace(/ %|% |%/g, ' ').trim();
    if (searchText) {
      redirectToSuggestedUrl(searchText);
    }
  };

  getRecentStoreInfo = () => {
    const getRecentStore = getRecentStoreFromLocalStorage();
    let latestSearchResults;

    if (getRecentStore) {
      latestSearchResults = JSON.parse(getRecentStore.split(','));
    } else {
      latestSearchResults = [];
    }

    return latestSearchResults;
  };

  openSearchBar = (e) => {
    e.preventDefault();
    const { toggleSearchFlag } = this.props;
    const { showProduct } = this.state;
    const latestSearchResults = this.getRecentStoreInfo();
    const isLatestSearchResultsExists = getLatestSearchResultsExists(latestSearchResults);
    if (isLatestSearchResultsExists && !showProduct) this.setState({ isSearchOpen: true });
    toggleSearchFlag(false);
  };

  getEmptySearchInputClassName = (isLatestSearchResultsExists) => {
    const { isSearchOpen } = this.state;
    const { showProduct } = this.props;

    return (isLatestSearchResultsExists && isSearchOpen) || showProduct
      ? 'empty-search-input-withRecent'
      : 'empty-search-input';
  };

  handleClickOutside() {
    const { isSearchOpen } = this.state;
    const { manageHandleClickOutsideFunc } = this.props;
    if (isSearchOpen && window.innerWidth > breakpoints.values.lg) {
      this.setState({ isSearchOpen: false });
      manageHandleClickOutsideFunc(false);
    }
  }

  render() {
    const { className, slpLabels, searchResults, redirectToSuggestedUrl, showProduct, showRecent } =
      this.props;

    const latestSearchResults = this.getRecentStoreInfo();
    const isLatestSearchResultsExists = getLatestSearchResultsExists(latestSearchResults);
    const emptySearchInputClassName = this.getEmptySearchInputClassName(
      isLatestSearchResultsExists
    );

    return (
      <BodyCopy
        fontSize={['fs16', 'fs32']}
        fontFamily="secondary"
        fontWeight="regular"
        className="empty-search-inputBox-container"
      >
        <form className={className} noValidate onSubmit={this.initiateSearchBySubmit}>
          <label htmlFor="emptySearchInput" className="visuallyhidden">
            <input
              id="emptySearchInput"
              className={`${emptySearchInputClassName}`}
              maxLength="150"
              placeholder={slpLabels.lbl_looking_for}
              onChange={this.changeSearchText}
              ref={this.searchInput}
              autoComplete="off"
              onClick={this.openSearchBar}
            />
            <p>{slpLabels.lbl_looking_for}</p>
          </label>
        </form>
        <Image
          alt="search"
          className="empty-search-image icon-small"
          src={getIconPath('search-icon')}
          data-locator="search-icon"
          height="25px"
          onClick={this.getSearchResults}
        />

        <div className="matchBox">
          <div className="matchLinkBox">
            {showRecent && (
              <SuggestionBox
                isLatestSearchResultsExists={isLatestSearchResultsExists}
                latestSearchResults={latestSearchResults}
                labels={slpLabels}
                redirectToSuggestedUrl={redirectToSuggestedUrl}
              />
            )}
            {showProduct && (
              <CategoryMatches
                searchResults={searchResults}
                labels={slpLabels}
                redirectToSuggestedUrl={redirectToSuggestedUrl}
              />
            )}
          </div>
        </div>
      </BodyCopy>
    );
  }
}

NoResponseInputContainer.propTypes = {
  className: PropTypes.string,
  slpLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  searchResults: PropTypes.shape({
    trends: PropTypes.shape({}),
    categories: PropTypes.shape({}),
    products: PropTypes.shape({}),
  }),
  startSearch: PropTypes.func.isRequired,
  redirectToSuggestedUrl: PropTypes.func.isRequired,
  labels: PropTypes.shape({
    lbl_search_whats_trending: '',
    lbl_search_recent_search: '',
    lbl_search_looking_for: '',
    lbl_search_product_matches: '',
  }),
  showProduct: PropTypes.bool,
  showRecent: PropTypes.bool,
  toggleSearchFlag: PropTypes.func,
  manageHandleClickOutsideFunc: PropTypes.func,
};

NoResponseInputContainer.defaultProps = {
  className: '',
  slpLabels: {},
  searchResults: {
    trends: {},
    categories: {},
    products: {},
  },
  labels: PropTypes.shape({
    lbl_search_whats_trending: PropTypes.string,
    lbl_search_recent_search: PropTypes.string,
    lbl_search_looking_for: PropTypes.string,
    lbl_search_product_matches: PropTypes.string,
  }),
  showProduct: false,
  showRecent: false,
  toggleSearchFlag: () => {},
  manageHandleClickOutsideFunc: () => {},
};

export default withStyles(enhanceWithClickOutside(NoResponseInputContainer), SearchListingStyle);
