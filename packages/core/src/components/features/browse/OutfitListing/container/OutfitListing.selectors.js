// 9fbef606107a605d69c0edbcd8029e5d 
import { getLabelValue } from '@tcp/core/src/utils';

const getLabels = state => {
  return state.Labels.OutfitLabels && state.Labels.OutfitLabels.Outfit_Sub;
};

const getAccessibilityLabels = state => {
  return {
    outfitImageLbl: getLabelValue(state.Labels, 'lbl_outfit_image', 'accessibility', 'global'),
  };
};

export default {
  getLabels,
  getAccessibilityLabels,
};
