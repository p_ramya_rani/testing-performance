// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
/* eslint-disable no-param-reassign */
/* eslint-disable no-lonely-if */
/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
import logger from '@tcp/core/src/utils/loggerInstance';
import { isBossProduct, isBopisProduct } from '../util/utility.js';
import {
  getProductAttributes,
  extractAttributeValue,
  extractPrioritizedBadge,
} from '../../../../../utils/badge.util';
import { getTCPMultiPackReferenceUSStore } from '../../../../../services/abstractors/productListing/processPdpResponse';

// https://tc39.github.io/ecma262/#sec-array.prototype.findindex
if (!Array.prototype.findIndex) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function (predicate) {
      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return k.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return k;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return -1.
      return -1;
    },
    configurable: true,
    writable: true,
  });
}

const sumBy = (arr, iteratee) => {
  const func = typeof iteratee === 'function' ? iteratee : (item) => item[iteratee];

  return arr.reduce((acc, item) => acc + func(item), 0);
};

// class ProductsDynamicAbstractor {
const apiHelper = {
  configOptions: { siteId: '' },
};

/**
 * @function getUnbxdId
 * @summary This will get the UNBXD id that we got from reponse headers in  UNBXD call.
 */

const getUnbxdId = () => 'unbxdId';
const isGiftCard = (product) =>
  !!(
    product &&
    ((product.style_partno && product.style_partno.toLowerCase() === 'giftcardbundle') ||
      product.giftcard === '1')
  );

const getSize = (sizeName) => {
  let size = sizeName && sizeName.split('_');
  return size && (size.length > 1 ? size[1] : sizeName);
};

const validateQuantityAvailable = (sizes) => {
  const index = sizes.findIndex(function (size) {
    return size.maxAvailable !== 0;
  });
  return index > -1 ? Number.MAX_VALUE : 0;
};

const getTotalQtyAvailable = (sizes) => sumBy(sizes, 'v_qty');

const getParticularCategory = (pathMap, breadCrumbs) => {
  let strBreadCrumbs;
  let categoryEntity;
  if (breadCrumbs) {
    strBreadCrumbs = breadCrumbs.map((obj) => obj.categoryId).join('>');
    categoryEntity =
      strBreadCrumbs.length && pathMap.find((category) => category.includes(strBreadCrumbs));
  }
  return categoryEntity;
};

const parseCategoryEntity = (pathMap, breadCrumbs) => {
  const categoryEntity = getParticularCategory(pathMap, breadCrumbs);
  const entities = categoryEntity && categoryEntity.split('|');
  const categoryName = entities && entities[1].split('>');

  return categoryName ? categoryName.slice(0, breadCrumbs.length).join(':') : '';
};

const multiPackUSStore = (productVariants) => {
  for (let j = 0; j < productVariants.length; j += 1) {
    if (productVariants[j].TCPMultiPackUSStore) {
      return productVariants[j].TCPMultiPackUSStore;
    }
  }

  return false;
};

const tcpMultiPackReferenceUSStore = (productVariants) => {
  for (let j = 0; j < productVariants.length; j += 1) {
    if (productVariants[j].TCPMultiPackReferenceUSStore) {
      return getTCPMultiPackReferenceUSStore(productVariants[j].TCPMultiPackReferenceUSStore);
    }
  }

  return false;
};

const getMultiPackAllColorInfo = (productVariants) => {
  return productVariants.map((product) => ({
    TCPMultiPackUSStore: product.TCPMultiPackUSStore ? product.TCPMultiPackUSStore : false,
    name: product.auxdescription,
    variants: product.variants,
    styleTypeUS: product.TCPStyleTypeUS,
    styleTypeCA: product.TCPStyleTypeCA,
    prodpartno: product.prodpartno,
    TCPMultiPackReferenceUSStore: getTCPMultiPackReferenceUSStore(
      product.TCPMultiPackReferenceUSStore
    ),
  }));
};

export const parseProductFromAPI = (
  product,
  colorIdOrSeoKeyword,
  dontFetchExtraImages,
  getImgPath,
  breadCrumbs,
  excludeBage
) => {
  const baseProduct = product[0] || product; // Getting multiple products as color variants
  const productVariants = (Array.isArray(product) ? product : Array.of(product)) || [];
  const isGiftCardBool = isGiftCard(baseProduct); // TBD: backend to confirm whether partNumber will always be giftCardBundle for gift cards.
  const productAttributes = getProductAttributes();
  let hasFit = false;
  let hasInventory = false;
  let alternateSizes;
  let defaultColorAlternateSizes;
  let otherColorAlternateSizes;

  let hasAdditionalStyles = false;
  let imagesByColor = {};
  //const imagesByColor = extractExtraImages(rawColors, baseProduct.alt_img, getImgPath);

  // This color map is used as an intermediary step to help consolidate all sizes under fits
  let colorsFitsMap = {};
  for (let colorVariant of productVariants) {
    let color = getProductColorName(isGiftCardBool, colorVariant);
    let currentColorFitsSizesMap = {};

    let fitName = '';
    for (let sizeVariant of colorVariant.variants) {
      fitName = (sizeVariant.v_tcpfit && sizeVariant.v_tcpfit.toLowerCase()) || '';
      if (!currentColorFitsSizesMap[fitName]) {
        currentColorFitsSizesMap[fitName] = [];
      }
      if (!hasInventory) {
        hasInventory = sizeVariant.v_qty && sizeVariant.v_qty !== 0 && sizeVariant.v_qty !== '';
      }
      currentColorFitsSizesMap[fitName].push({
        sizeName: getSize(sizeVariant.v_tcpsize) || getSize(sizeVariant.style_name),
        skuId: sizeVariant.v_item_catentry_id,
        listPrice: parseFloat(sizeVariant.v_listprice) || 0,
        offerPrice: parseFloat(sizeVariant.v_offerprice) || 0,
        maxAvailable: sizeVariant.v_qty,
        v_qty: sizeVariant.v_qty,
        variantId: sizeVariant.variantId,
        variantNo: sizeVariant.v_variant,
      });
    }

    if (fitName) {
      hasFit = true;
    }
    let hasDefaultFit = false;
    let sortOptions = {
      regular: 1,
      slim: 2,
      plus: 3,
      husky: 4,
      other: 5,
    };
    let sortedKeys = Object.keys(currentColorFitsSizesMap).sort(
      (a, b) =>
        (sortOptions[a.toLowerCase()] || sortOptions.other) -
        (sortOptions[b.toLowerCase()] || sortOptions.other)
    );

    colorsFitsMap[color] = sortedKeys.map((fitName) => {
      let isDefaultFit = fitName.toLowerCase() === 'regular';
      hasDefaultFit = hasDefaultFit || isDefaultFit;

      return {
        fitName: fitName,
        isDefault: isDefaultFit,
        maxAvailable: validateQuantityAvailable(currentColorFitsSizesMap[fitName]),
        sizes: convertMultipleSizeSkusToAlternatives(currentColorFitsSizesMap[fitName]),
      };
    });

    if (!hasDefaultFit && colorsFitsMap[color].length) {
      colorsFitsMap[color][0].isDefault = true;
    }

    if (colorVariant.uniqueId === colorIdOrSeoKeyword && colorVariant.additional_styles) {
      defaultColorAlternateSizes = colorVariant.additional_styles;
    }
    if (!hasAdditionalStyles && colorVariant.additional_styles) {
      otherColorAlternateSizes = colorVariant.additional_styles;
      hasAdditionalStyles = true;
    }
  }

  try {
    const defaultColorAltSizes =
      defaultColorAlternateSizes && JSON.parse(defaultColorAlternateSizes);
    const otherColorAltSizes = otherColorAlternateSizes && JSON.parse(otherColorAlternateSizes);
    alternateSizes = defaultColorAltSizes || otherColorAltSizes || '';
  } catch (err) {
    alternateSizes = {};
    logger.error('API response coming for additional_styles key JSON format is incorrect', err);
  }

  // Generate the colorFitsSizeMap needed for mapping colors to fits/sizes

  let colorFitsSizesMap = productVariants.map((itemColor) => {
    let { productImages, colorSwatch } = getImgPath(itemColor.imagename);
    const productID = itemColor.imagename && itemColor.imagename.split('_')[0];
    colorSwatch = `${productID}/${itemColor.imagename}_swatch.jpg`;
    let colorName = getProductColorName(isGiftCardBool, itemColor);
    let familyName = isGiftCardBool ? itemColor.product_name : itemColor.TCPColor;
    const categoryColorId =
      itemColor.categoryPath2_catMap && itemColor.categoryPath3_catMap
        ? [...itemColor.categoryPath3_catMap, ...itemColor.categoryPath2_catMap]
        : itemColor.categoryPath3_catMap || itemColor.categoryPath2_catMap;
    const categoryEntity = categoryColorId && parseCategoryEntity(categoryColorId, breadCrumbs);

    const bossDisabledFlags = {
      bossProductDisabled:
        extractAttributeValue(itemColor, getProductAttributes().bossProductDisabled) || 0,
      bossCategoryDisabled:
        extractAttributeValue(itemColor, getProductAttributes().bossCategoryDisabled) || 0,
    };
    imagesByColor = {
      ...extractExtraImages(
        `${itemColor.imagename}#${colorName}`,
        itemColor.alt_img,
        getImgPath,
        false,
        false,
        isGiftCard
      ),
      ...imagesByColor,
    };

    return {
      color: {
        name: colorName,
        imagePath: isGiftCardBool ? productImages[125] : colorSwatch,
        family: familyName,
        // Family name can be different from color name, quickViewStoreView using family name to find the initial value of Quick View Form
      },
      pdpUrl: `/${apiHelper.configOptions.siteId}/p/${itemColor.uniqueId}`,
      colorProductId: itemColor.productid,
      colorDisplayId: itemColor.uniqueId, // We need this to display on PDP as well as to send to api for recommendations
      categoryEntity: categoryEntity,
      imageName: itemColor.imagename,
      favoritedCount: itemColor.favoritedcount,
      maxAvailable: getTotalQtyAvailable(itemColor.variants) || 0, // No inventory message if it is zero
      hasFits: hasFit,
      miscInfo: {
        isBopisEligible:
          isBopisProduct(apiHelper.configOptions.isUSStore, itemColor) && !isGiftCard(itemColor),
        isBossEligible: isBossProduct(bossDisabledFlags) && !isGiftCard(itemColor),
        badge1: extractPrioritizedBadge(
          getFirstVariant(itemColor),
          productAttributes,
          '',
          excludeBage
        ),
        isClearance: extractAttributeValue(itemColor, getProductAttributes().clearance),
        hasOnModelAltImages: extractAttributeValue(
          itemColor,
          getProductAttributes().onModelAltImages
        ),
        videoUrl: extractAttributeValue(itemColor, productAttributes.videoUrl),
      },
      fits: colorsFitsMap[colorName],
      listPrice:
        parseFloat(getFirstVariant(itemColor).v_listprice) ||
        parseFloat(getFirstVariant(itemColor).v_offerprice) ||
        0,
      offerPrice: parseFloat(getFirstVariant(itemColor).v_offerprice) || 0,
      unbxdId: getUnbxdId(),
    };
  });

  const reviewsCount =
    (baseProduct.TCPBazaarVoiceReviewCount && parseInt(baseProduct.TCPBazaarVoiceReviewCount)) || 0;
  const categoryPathMap = baseProduct.categoryPath3_catMap || baseProduct.categoryPath2_catMap;
  const categoryPath =
    baseProduct.categoryPath2_catMap && baseProduct.categoryPath2_catMap[0].split('|')[0];
  const categoryId =
    breadCrumbs && breadCrumbs.length && breadCrumbs[0].categoryId
      ? parseCategoryId(baseProduct.categoryPath2_catMap, breadCrumbs)
      : categoryPath;

  return {
    breadCrumbTrail: [],
    rawBreadCrumb:
      categoryPathMap && categoryPathMap.length > 0 ? categoryPathMap[0].split('|')[0] : '',
    product: {
      // generalProductId = color with matching seo OR colorIdOrSeoKeyword is its a number OR default to first color's ID (To Support Outfits)
      ratingsProductId: baseProduct.style_partno,
      // generalProductId = color with matching seo OR colorIdOrSeoKeyword is its a number OR default to first color's ID (To Support Outfits)
      generalProductId:
        colorIdOrSeoKeyword ||
        (colorFitsSizesMap[0] && colorFitsSizesMap[0].productid) ||
        baseProduct.productid,
      categoryId: categoryId || '',
      name: isGiftCardBool ? 'Gift Card' : baseProduct.product_name,
      pdpUrl: `/${apiHelper.configOptions.siteId}/p/${colorIdOrSeoKeyword}`,
      shortDescription: baseProduct.product_short_description,
      longDescription: baseProduct.style_long_description,
      imagesByColor: imagesByColor,
      colorFitsSizesMap: colorFitsSizesMap,
      isGiftCard: isGiftCardBool,
      colorFitSizeDisplayNames: isGiftCardBool
        ? { color: 'Design', size: 'Value (USD)', size_alt: 'Value' }
        : null,
      listPrice:
        parseFloat(baseProduct.min_list_price) || parseFloat(baseProduct.min_offer_price) || 0,
      offerPrice: parseFloat(baseProduct.min_offer_price) || 0,
      highListPrice: parseFloat(baseProduct.high_list_price) || 0,
      highOfferPrice: parseFloat(baseProduct.high_offer_price) || 0,
      lowListPrice: parseFloat(baseProduct.low_list_price) || 0,
      lowOfferPrice: parseFloat(baseProduct.low_offer_price) || 0,
      ratings: isGiftCardBool ? 0 : baseProduct.TCPBazaarVoiceRating || 0,
      reviewsCount: isGiftCardBool ? 0 : reviewsCount,
      unbxdId: getUnbxdId(),
      unbxdProdId: baseProduct.uniqueId,
      alternateSizes,
      productId: baseProduct.uniqueId,
      promotionalMessage: baseProduct.TCPLoyaltyPromotionTextUSStore || '',
      promotionalPLCCMessage: baseProduct.TCPLoyaltyPLCCPromotionTextUSStore || '',
      multiPackUSStore: multiPackUSStore(productVariants),
      tcpMultiPackReferenceUSStore: tcpMultiPackReferenceUSStore(productVariants),
      getMultiPackAllColor: getMultiPackAllColorInfo(productVariants),
    },
  };
};

/** - - - - - - - - - - - - - - - PRIVATE METHODS - - - - - - - - - - - -  **/

// Due to the infinite wisdom of some of the TCP merchants, some products at the color-fit-size customization level
// (i.e., what is usually considered to be a sku) have received multiple UPC's and SKU id's, each with its own inventory count!
// Such duplicates share the same color and fit as well as size name.
// To address this "multiple SKU problem", this method removes duplicates from the given sizes array by storing (if needed), under the
// key alternativeSkuIds, a list of all the extra sku's for this size. This allows us, when receiving detailed inventory information, to
// identify and use the sku among all the alternatives for which the largest inventory exists, and use this as the primary identifier for this size.
function convertMultipleSizeSkusToAlternatives(sizes) {
  let uniqueSizesMap = Object.create(null);
  let result = [];

  if (sizes) {
    sizes.forEach(function (size, index) {
      let existingSizeForName = uniqueSizesMap[size.sizeName];
      let alternativeSkuIds;

      size.position = index;

      if (!existingSizeForName) {
        //Add current size to uniqueSizesMap if current size  not exist in uniqueSizesMap
        uniqueSizesMap[size.sizeName] = size;
        result.push(size);
      } else {
        if (size.maxAvailable > existingSizeForName.maxAvailable) {
          alternativeSkuIds = existingSizeForName.skuId;
          uniqueSizesMap[size.sizeName] = size;
          result[existingSizeForName.position] = size;
          existingSizeForName = uniqueSizesMap[size.sizeName];
        } else {
          alternativeSkuIds = size.skuId;
          if (!existingSizeForName.alternativeSkuIds) {
            //Check if alternativeSkuIds already exist or not, if not, then intialize alternativeSkuIds with empty array
            existingSizeForName.alternativeSkuIds = [];
          }
          // store the skuId of duplicate size with less quantity as an alternative
          existingSizeForName.alternativeSkuIds.push(alternativeSkuIds);
          uniqueSizesMap[size.sizeName] = existingSizeForName;
          result[existingSizeForName.position] = existingSizeForName;
        }
      }
    });
  }

  return result.filter((size) => size);
}

// /**
//  *
//  * @param {Boolean} isGiftCard - is this item a gift card
//  * @param {Object} product - this is the product object
//  */
function getProductColorName(isGiftCard, product) {
  return isGiftCard ? product.product_name : product.auxdescription || product.TCPColor;
}

/**
 *
 * @param {Object} product - this is the product object
 */
function getFirstVariant(product) {
  try {
    return product.variants[0] || {};
  } catch (ex) {
    return {};
  }
}

// We seem to be itterating over all colors and added alt images in this location
function extractExtraImages(rawColors, altImgs, getImgPath, uniqueId, defaultColor, isGiftCard) {
  let colorsImageMap = {};

  // backend send the colors in a very weird format
  try {
    if (rawColors && rawColors !== '') {
      // DTN-6314 Gift card pdp page broken
      // handle senario if gift card product_name contains '|' character in it.
      let colors = [];
      if (isGiftCard) {
        colors.push(rawColors);
      } else {
        colors = rawColors.split('|');
      }
      for (let color of colors) {
        let colorName = color.split('#')[1];
        let imageBasePath = color.split('#')[0];
        if (!colorName) {
          colorName = defaultColor;
          imageBasePath = uniqueId;
        }
        let { productImages } = getImgPath(imageBasePath);

        colorsImageMap[colorName] = {
          basicImageUrl: productImages[500],
          extraImages: _parseAltImagesForColor(imageBasePath),
        };
      }
    } else {
      let { productImages } = getImgPath(uniqueId);
      colorsImageMap[defaultColor] = {
        basicImageUrl: productImages[500],
        extraImages: _parseAltImagesForColor(uniqueId),
      };
    }
  } catch (error) {
    logger.error(error);
  }
  return colorsImageMap;

  // inner function
  function _parseAltImagesForColor(imageBasePath) {
    try {
      const altImages = altImageArray(imageBasePath, altImgs);

      return [imageBasePath, ...altImages].map((img) => {
        const hasExtension = img.indexOf('.jpg') !== -1; // we currently only support .jpg but we can make this a regex in the future if needed
        let { productImages } = getImgPath(img, hasExtension);

        // See DTN-155 for image suffex value definitions
        let isOnModalImage = parseInt(img.split('-')[1]) > 5; // this is assumming a structure of <alpahnumeric>-<numeric><other (optional)>

        return {
          isOnModalImage,
          iconSizeImageUrl: productImages[125],
          listingSizeImageUrl: productImages[380],
          regularSizeImageUrl: productImages[500],
          bigSizeImageUrl: productImages[900],
          superSizeImageUrl: productImages[900],
        };
      });
    } catch (error) {
      return [];
    }
  }
}
