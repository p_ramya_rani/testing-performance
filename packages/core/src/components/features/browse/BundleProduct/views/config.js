// 9fbef606107a605d69c0edbcd8029e5d 
import { breakpoints } from '../../../../../../styles/themes/TCP/mediaQuery';

const CAROUSEL_OPTIONS = {
  autoplay: false,
  arrows: false,
  fade: false,
  speed: 0,
  dots: true,
  dotsClass: 'slick-dots',
  swipe: true,
  slidesToShow: 1,
  infinite: false,
  responsive: [
    {
      breakpoint: breakpoints.values.lg - 1,
      settings: {
        arrows: false,
      },
    },
  ],
};

const IMG_DATA = {
  imgConfig: [`t_bundle_img_m`, `t_bundle_img_t`, `t_bundle_img_d`],
};

export { CAROUSEL_OPTIONS, IMG_DATA };
