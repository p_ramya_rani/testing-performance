// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ScrollView, Share, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Animated from 'react-native-reanimated';
import { TouchableOpacity as TouchableOpacityGH } from 'react-native-gesture-handler';
import FastImage from '@stevenmasini/react-native-fast-image';
import { PropTypes } from 'prop-types';
import { getLoading, scrollToViewBottom } from '@tcp/core/src/utils';
import { isAndroid } from '@tcp/core/src/utils/utils.app';
import { getAPIConfig } from '@tcp/core/src/utils/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import ImageCarousel from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ImageCarousel';
import ProductSummary from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductSummary';
import { getPDPLabels } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.selectors';
import ProductDetailDescription from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductDescription/views/ProductDescription.view.native';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import { OUTFIT_LISTING_FORM } from '@tcp/core/src/constants/reducer.constants';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import PLCCBanner from '@tcp//core/src/components/features/browse/ProductDetail/views/PLCCBanner.view.native';
import withStyles from '../../../../common/hoc/withStyles.native';
import { FullScreenImageCarousel } from '../../../../common/molecules/index.native';
import { ForterActionType, trackForterAction } from '../../../../../utils/forter.util';
import {
  PageContainer,
  RecommendationWrapper,
  PageInnerContainer,
  ShareContainer,
  Margin,
  AnimationContainer,
  BadgeTitleContainer,
} from '../styles/BundleProduct.style.native';
import {
  getImagesToDisplay,
  getMapSliceForColorProductId,
} from '../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import BundleProductItems from '../molecules/BundleProductItems';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import { TIER1 } from '../../../../../constants/tiering.constants';
import MultiProductAddToBag from '../../OutfitDetails/molecules/MultipleAddToBag';

const shareImage = require('../../../../../../../mobileapp/src/assets/images/icons_standard_share_3x.png');
const shareImageAndroid = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icons-standard-shareandroid.png');

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
class ProductBundle extends React.PureComponent {
  currentColorEntry;

  constructor(props) {
    super(props);
    this.state = {
      showCarousel: false,
      activeIndex: 0,
    };
    this.currentScrollValue = 0;
    this.topIconPos = isAndroid() ? 0 : 36;
    const stickyButtonHeight = isAndroid() ? 20 : 30;
    const drawerHeaderMaxHeight = 64;
    const maxHeight = isAndroid()
      ? screenHeight - this.topIconPos - stickyButtonHeight - drawerHeaderMaxHeight - 10 + 2
      : screenHeight - this.topIconPos - stickyButtonHeight - drawerHeaderMaxHeight + 2;
    const drawerHeader = 30;
    const priceTitleRating = 125;
    const swatches = 85;
    this.minHeight = drawerHeader + priceTitleRating + swatches + stickyButtonHeight + 15;
    const midHeight = this.minHeight + 90;
    this.snapPoints = [this.minHeight - 32, midHeight - 32, maxHeight];
    const midPoint =
      (this.snapPoints[2] - this.snapPoints[1]) / (this.snapPoints[2] - this.snapPoints[0]);
    const iconHeight = 36;
    this.shareIconStartTop = this.topIconPos + iconHeight + 14;
    this.iconStartLeft = screenWidth - iconHeight - 22;
    this.shareIconEndLeft = this.iconStartLeft - iconHeight - 5;
    this.shareIconPositionTop = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [this.topIconPos + 4, this.shareIconStartTop, this.shareIconStartTop],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
    this.shareIconPositionLeft = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [this.shareIconEndLeft - 20, this.iconStartLeft, this.iconStartLeft],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({
      isScrolled: false,
    });
  }

  onImageClick = () => {
    const { showCarousel } = this.state;
    this.setState({ showCarousel: !showCarousel });
  };

  scrollToAccordionBottom = (x, y, width, height, pageX, pageY) => {
    scrollToViewBottom({
      width,
      height,
      pageX,
      pageY,
      callBack: this.scrollView,
      currentScrollValue: this.currentScrollValue,
    });
  };

  handleScroll = (event) => {
    const { navigation } = this.props;
    this.currentScrollValue = event.nativeEvent.contentOffset.y;
    if (this.currentScrollValue > 5) {
      navigation.setParams({ isScrolled: true });
    } else {
      navigation.setParams({ isScrolled: false });
    }
  };

  checkInWishlist = (wishList, productId) =>
    wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;

  onShare = async () => {
    const { navigation } = this.props;
    const { webAppDomain } = getAPIConfig();
    const pdpUrl = (navigation && navigation.getParam('pdpUrl')) || '';
    const shareUrl = `${webAppDomain}/us/p/${pdpUrl}`;
    try {
      trackForterAction(ForterActionType.SHARE, 'PDP_ITEM_SHARE');
      await Share.share({ message: shareUrl });
    } catch (error) {
      logger.error(error);
    }
  };

  renderShare = () => {
    const AnimatedShareContainer = Animated.createAnimatedComponent(ShareContainer);
    const imageStyle = { height: 24, width: 24 };
    return (
      <AnimatedShareContainer
        defaultPositions={{ left: this.shareIconEndLeft + 40, top: this.topIconPos + 50 }}
      >
        <TouchableOpacityGH
          accessibilityRole="button"
          accessibilityLabel="favorite"
          accessibilityState={{ selected: true }}
          onPress={this.onShare}
        >
          <FastImage
            source={isAndroid() ? shareImageAndroid : shareImage}
            dataLocator="pdp_social_connect"
            style={imageStyle}
            resizeMode={FastImage?.resizeMode?.contain}
          />
        </TouchableOpacityGH>
      </AnimatedShareContainer>
    );
  };

  renderCarousel = (imageUrls, isSBP, isNewReDesignEnabled, colorFitsSizesMap) => {
    const { showCarousel, activeIndex } = this.state;
    if (!showCarousel) return null;
    return (
      <FullScreenImageCarousel
        zoomImages={true}
        imageUrls={imageUrls}
        parentActiveIndex={activeIndex}
        onCloseModal={this.onImageClick}
        isSBP={isSBP}
        isNewReDesignFullScreenImageCarousel={true}
        colorFitsSizesMap={colorFitsSizesMap}
        currentColor={this.currentColorEntry}
      />
    );
  };

  callbackIndex = (fullScreenImageIndex) => {
    this.setState({ activeIndex: fullScreenImageIndex });
  };

  renderTopTitle = (titleValue) => {
    if (titleValue !== '') {
      return (
        <BodyCopy
          dataLocator="pdp_product_titles"
          fontFamily="secondary"
          fontSize="fs14"
          fontWeight="extrabold"
          color="gray.900"
          text={`${titleValue} `}
          numberOfLines={2}
          textAlign="left"
        />
      );
    }
    return null;
  };

  renderBadgeAndTitleContainer = () => {
    const { currentProduct } = this.props;
    return <BadgeTitleContainer>{this.renderTopTitle(currentProduct?.name)}</BadgeTitleContainer>;
  };

  render() {
    const {
      currentProduct,
      currentBundle,
      selectedColorProductId,
      pdpLabels,
      shortDescription,
      itemPartNumber,
      longDescription,
      plpLabels,
      navigation,
      handleAddToBag,
      addToFavorites,
      addToBagEcom,
      currentState,
      isLoggedIn,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      addToBagErrorId,
      addToBagError,
      multipleAddToBagError,
      toastMessage,
      isKeepAliveEnabled,
      outOfStockLabels,
      collectionLabels,
      isOnModelAbTestBundlePdp,
      isEnabledOutfitAddedCTA,
      showAddedToBagCta,
      onSetLastDeletedItemIdAction,
      wishList,
      isOutfitPage,
      defaultWishListFromState,
      errorMessages,
      setSelectedOutfitProduct,
      selectedOutfitProducts,
      labels,
      handleMultipleAddToBag,
      addMultipleToBagEcom,
      isNewReDesignEnabled,
      isFromBundlePage,
      retrievedProductInfo,
      openApplyNowModal,
      alternateBrand,
    } = this.props;
    const isSBP = navigation.getParam('isSBP');
    const { deviceTier } = navigation.getScreenProps();
    if (currentProduct && JSON.stringify(currentProduct) !== '{}') {
      const { colorFitsSizesMap } = currentProduct;
      this.currentColorEntry = getMapSliceForColorProductId(
        colorFitsSizesMap,
        selectedColorProductId
      );
      let imageUrls = [];
      if (colorFitsSizesMap && currentProduct?.imagesByColor) {
        imageUrls = getImagesToDisplay({
          imagesByColor: currentProduct?.imagesByColor,
          curentColorEntry: this.currentColorEntry,
          isAbTestActive: isOnModelAbTestBundlePdp,
          isFullSet: true,
        });
      }
      const recommendationAttributes = {
        variation: 'moduleO',
        navigation,
        page: Constants.RECOMMENDATIONS_PAGES_MAPPING.COLLECTION,
        partNumber: itemPartNumber,
        isHeaderAccordion: true,
        priceOnly: true,
      };
      let totalItemQty = 0;
      selectedOutfitProducts.forEach((product) => {
        if (product.quantity > 0) {
          totalItemQty += product.quantity;
        }
      });
      return (
        <>
          <AnimationContainer>
            <Margin margins="12px 12px 0 12px" paddings="0 0 12px 0">
              {this.renderShare()}
            </Margin>
          </AnimationContainer>
          <ScrollView
            ref={(ref) => {
              this.scrollView = ref;
            }}
            onScroll={this.handleScroll}
            keyboardShouldPersistTaps="handled"
          >
            <PageContainer>
              <PageInnerContainer>
                <ImageCarousel
                  isGiftCard={currentProduct.isGiftCard}
                  imageUrls={imageUrls}
                  addToFavorites={addToFavorites}
                  isLoggedIn={isLoggedIn}
                  currentProduct={currentProduct}
                  onImageClick={this.onImageClick}
                  AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                  removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                  currentColorEntry={this.currentColorEntry}
                  isBundleProduct
                  formName={OUTFIT_LISTING_FORM}
                  onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                  wishList={wishList}
                  isInWishList={this.checkInWishlist(wishList, selectedColorProductId)}
                  isOutfitPage={isOutfitPage}
                  defaultWishListFromState={defaultWishListFromState}
                  isNewReDesignEnabled={isNewReDesignEnabled}
                  parentActiveIndex={this.callbackIndex}
                  alternateBrand={alternateBrand}
                />
                {this.renderCarousel(
                  imageUrls,
                  isSBP,
                  isNewReDesignEnabled,
                  currentProduct?.colorFitsSizesMap || retrievedProductInfo?.colorFitsSizesMap
                )}
                <ProductSummary
                  productData={currentProduct}
                  selectedColorProductId={selectedColorProductId}
                  pdpLabels={pdpLabels}
                  isBundleProduct={true}
                  onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                  wishList={wishList}
                  isInWishList={this.checkInWishlist(wishList, selectedColorProductId)}
                  isOutfitPage={isOutfitPage}
                  defaultWishListFromState={defaultWishListFromState}
                  isNewReDesignProductSummary={true}
                  isSBP={isSBP}
                  getProductNameAndTopbadge={this.renderBadgeAndTitleContainer}
                  isFromBundleProductView={true}
                  alternateBrand={alternateBrand}
                />
                <PLCCBanner
                  isFromCollection={true}
                  pdpLabels={pdpLabels}
                  openApplyNowModal={openApplyNowModal}
                  navigation={navigation}
                  backGroundColor
                />
                <ProductDetailDescription
                  margins="16px 0 0 0"
                  shortDescription={shortDescription}
                  itemPartNumber={itemPartNumber}
                  longDescription={longDescription}
                  isShowMore={false}
                  pdpLabels={pdpLabels}
                  backgroundColor="white"
                  onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                  wishList={wishList}
                  isInWishList={this.checkInWishlist(wishList, selectedColorProductId)}
                  isOutfitPage={isOutfitPage}
                  defaultWishListFromState={defaultWishListFromState}
                  isNewReDesignDescription={true}
                  isFromBundleProductView={true}
                  alternateBrand={alternateBrand}
                />
                <BundleProductItems
                  currentBundle={currentBundle}
                  plpLabels={plpLabels}
                  navigation={navigation}
                  handleAddToBag={handleAddToBag}
                  addToFavorites={addToFavorites}
                  addToBagEcom={addToBagEcom}
                  currentState={currentState}
                  isLoggedIn={isLoggedIn}
                  addToBagErrorId={addToBagErrorId}
                  addToBagError={addToBagError}
                  multipleAddToBagError={multipleAddToBagError}
                  toastMessage={toastMessage}
                  isKeepAliveEnabled={isKeepAliveEnabled}
                  outOfStockLabels={outOfStockLabels}
                  AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                  removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                  isOnModelAbTestBundlePdp={isOnModelAbTestBundlePdp}
                  showAddedToBagCta={showAddedToBagCta}
                  isEnabledOutfitAddedCTA={isEnabledOutfitAddedCTA}
                  onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                  wishList={wishList}
                  isInWishList={this.checkInWishlist(wishList, selectedColorProductId)}
                  isOutfitPage={isOutfitPage}
                  isFromBundlePage={isFromBundlePage}
                  defaultWishListFromState={defaultWishListFromState}
                  errorMessages={errorMessages}
                  isNewReDesignEnabled={isNewReDesignEnabled}
                  setSelectedOutfitProduct={setSelectedOutfitProduct}
                  selectedOutfitProducts={selectedOutfitProducts}
                  alternateBrand={alternateBrand}
                />
                {!isFromBundlePage && (
                  <RecommendationWrapper>
                    <Recommendations
                      headerLabel={collectionLabels.youMayAlsoLike}
                      {...recommendationAttributes}
                    />
                    {deviceTier === TIER1 && (
                      <Recommendations
                        isRecentlyViewed
                        {...recommendationAttributes}
                        headerLabel={collectionLabels.recentlyViewed}
                        portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
                      />
                    )}
                  </RecommendationWrapper>
                )}
              </PageInnerContainer>
            </PageContainer>
          </ScrollView>
          {selectedOutfitProducts?.length > 0 && (
            <MultiProductAddToBag
              AddItemsToBagLabel={labels.lbl_addtobag_text}
              selectedItemsCount={totalItemQty}
              handleMultipleAddToBag={handleMultipleAddToBag}
              addMultipleToBagEcom={addMultipleToBagEcom}
              selectedOutfitProducts={selectedOutfitProducts}
              currentState={currentState}
              isSBP={false}
              setSelectedOutfitProduct={setSelectedOutfitProduct}
            />
          )}
        </>
      );
    }
    return getLoading();
  }
}
const mapStateToProps = (state) => {
  return { pdpLabels: getPDPLabels(state) };
};
function mapDispatchToProps(dispatch) {
  return {
    openApplyNowModal: (payload) => {
      dispatch(toggleApplyNowModal(payload));
    },
  };
}
ProductBundle.propTypes = {
  currentProduct: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({
    getParam: PropTypes.func.isRequired,
    setParams: PropTypes.func.isRequired,
  }),
  selectedColorProductId: PropTypes.number.isRequired,
  plpLabels: PropTypes.shape({}),
  shortDescription: PropTypes.string,
  itemPartNumber: PropTypes.string,
  longDescription: PropTypes.string,
  pdpLabels: PropTypes.shape({}),
  currentBundle: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  handleAddToBag: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  addToBagEcom: PropTypes.func.isRequired,
  currentState: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool,
  isNewReDesignEnabled: PropTypes.bool,
  AddToFavoriteErrorMsg: PropTypes.string.isRequired,
  removeAddToFavoritesErrorMsg: PropTypes.func.isRequired,
  addToBagErrorId: PropTypes.string,
  addToBagError: PropTypes.string,
  multipleAddToBagError: PropTypes.string,
  toastMessage: PropTypes.func.isRequired,
  isKeepAliveEnabled: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}),
  collectionLabels: PropTypes.shape({}),
  isOnModelAbTestBundlePdp: PropTypes.bool,
  isEnabledOutfitAddedCTA: PropTypes.bool,
  showAddedToBagCta: PropTypes.string,
  wishList: PropTypes.func.isRequired,
  onSetLastDeletedItemIdAction: PropTypes.func,
  isOutfitPage: PropTypes.bool,
  isFromBundlePage: PropTypes.bool,
  defaultWishListFromState: PropTypes.shape({}),
  errorMessages: PropTypes.shape({}),
  setSelectedOutfitProduct: PropTypes.bool.isRequired,
  selectedOutfitProducts: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  handleMultipleAddToBag: PropTypes.func.isRequired,
  addMultipleToBagEcom: PropTypes.func,
};
ProductBundle.defaultProps = {
  labels: {},
  navigation: {},
  plpLabels: null,
  shortDescription: '',
  itemPartNumber: '',
  longDescription: '',
  pdpLabels: {},
  isLoggedIn: false,
  addToBagErrorId: '',
  addToBagError: '',
  multipleAddToBagError: '',
  outOfStockLabels: {},
  collectionLabels: {},
  isOnModelAbTestBundlePdp: false,
  isEnabledOutfitAddedCTA: false,
  isNewReDesignEnabled: false,
  showAddedToBagCta: '',
  onSetLastDeletedItemIdAction: () => {},
  addMultipleToBagEcom: () => {},
  isOutfitPage: false,
  isFromBundlePage: false,
  defaultWishListFromState: {},
  errorMessages: {},
  selectedOutfitProducts: [],
};
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(ProductBundle));
export { ProductBundle as ProductBundleVanilla };
