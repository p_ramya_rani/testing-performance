// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import get from 'lodash/get';

const getAdditionalStyle = (props) => {
  const { backgroundColor, theme, margins, paddings } = props;
  const { colorPalette } = theme;
  const bgColor = get(colorPalette, backgroundColor, props.theme.colors.PRIMARY.PALEGRAY);
  return {
    ...(backgroundColor && { background: bgColor }),
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
  };
};

export const PageContainer = styled.View`
  justify-content: center;
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
`;

export const AnimationContainer = styled.View`
  z-index: 101;
`;

export const PageInnerContainer = styled.View`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-top: 18px;
`;
export const RecommendationWrapper = styled.View`
  margin-left: -${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;
export const ShareContainer = styled.View`
  height: 36px;
  width: 36px;
  border-radius: 10px;
  position: absolute;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  background-color: ${(props) => props.theme.colors.TRANSLUCENT};
  top: ${(props) => props.defaultPositions.top};
  left: ${(props) => props.defaultPositions.left};
`;
export const Margin = styled.View`
  justify-content: center;
  ${getAdditionalStyle};
`;
export const BadgeTitleContainer = styled.View`
  justify-content: space-around;
  align-items: flex-start;
  width: 100%;
`;
