// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .placeholder-large {
    background: ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
    padding: 10px 0;
    text-align: center;
    width: calc(100% - 26px);
    margin: 0 13px ${props => props.theme.spacing.ELEM_SPACING.MED};

    @media ${props => props.theme.mediaQuery.medium} {
      margin: 0 13px ${props => props.theme.spacing.ELEM_SPACING.XXL};
    }
    @media ${props => props.theme.mediaQuery.large} {
      margin: 0 13px ${props => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .placeholder-small {
    background: ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
    padding: 20px 0;
    text-align: center;
    width: 100%;
    margin: ${props => props.theme.spacing.ELEM_SPACING.XL} 0;

    @media ${props => props.theme.mediaQuery.medium} {
      margin: ${props => props.theme.spacing.ELEM_SPACING.XXL} 0;
    }
    @media ${props => props.theme.mediaQuery.large} {
      margin: ${props => props.theme.spacing.ELEM_SPACING.XL} 0;
    }
  }

  .breadcrum-wrapper {
    margin: 0 0 ${props => props.theme.spacing.ELEM_SPACING.LRG};

    @media ${props => props.theme.mediaQuery.medium} {
      margin: 0 0 ${props => props.theme.spacing.ELEM_SPACING.LRG};
    }
    @media ${props => props.theme.mediaQuery.large} {
      margin: 0 0 ${props => props.theme.spacing.ELEM_SPACING.XXL};
    }
  }
  .title-wrapper {
    display: block;
    @media ${props => props.theme.mediaQuery.large} {
      display: flex;
    }
  }
  .wishlist-container {
    width: 25px;
  }
  .bundle-products-list {
    margin: 0;
    width: 100%;
  }
  .product-detail-section {
    flex: 1;
    margin: 0;
  }
  .product-image-wrapper {
    margin-right: 0;
  }
  .product-detail-image-wrapper {
    margin-bottom: 8px;
  }
  .product-summary-wrapper {
    display: flex;
    flex-direction: column;
    flex: 1;
  }
  .bundle-social-wrapper {
    justify-content: right;
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .product-summary-section {
    border-bottom: 1px solid ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .bundle-product-item {
    border-bottom: 1px solid ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    &:last-child {
      border-bottom: 0;
    }
  }
  .product-container {
    border-bottom: 1px solid ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  }

  .actual-price {
    font-size: ${props => props.theme.typography.fontSizes.fs22};
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
    @media ${props => props.theme.mediaQuery.medium} {
      font-size: ${props => props.theme.typography.fontSizes.fs24};
      margin-top: 0;
    }
  }
  .original-price {
    font-size: ${props => props.theme.typography.fontSizes.fs14};
    @media ${props => props.theme.mediaQuery.medium} {
      font-size: ${props => props.theme.typography.fontSizes.fs16};
    }
  }

  .outfiting-list-details {
    .actual-price {
      font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy7};
    }
    .original-price {
      font-size: ${props => props.theme.typography.fontSizes.fs14};
    }
  }

  @media ${props => props.theme.mediaQuery.smallMax} {
    .outfit-mobile-image img {
      height: 200px;
      width: 100%;
      object-fit: contain;
    }

    .outfit-carousal-image {
      height: 375px;
      object-fit: contain;
      width: 100%;
    }
  }

  @media ${props => props.theme.mediaQuery.medium} {
    .product-image-wrapper {
      margin-right: 30px;
    }
    .product-detail-image-wrapper {
      margin-bottom: 11px;
    }
    .product-summary-mobile-view {
      display: none;
    }
    .product-summary-desktop-view {
      display: flex;
    }
    .product-price-mobile-view {
      display: none;
    }
    .product-price-desktop-view {
      display: flex;
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
      border-bottom: 1px solid ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
      padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  @media ${props => props.theme.mediaQuery.large} {
    .product-image-wrapper {
      margin-right: 0;
    }
    .product-detail-image-wrapper {
      margin-bottom: 27px;
    }
    .product-summary-mobile-view {
      display: none;
    }
    .product-summary-desktop-view {
      display: flex;
    }
    .product-price-mobile-view {
      display: none;
    }
    .product-price-desktop-view {
      display: flex;
      border-bottom: 1px solid ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
      padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
`;
