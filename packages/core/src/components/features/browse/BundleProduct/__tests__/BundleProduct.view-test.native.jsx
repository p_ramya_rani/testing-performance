// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ProductBundleVanilla } from '../views/BundleProduct.view.native';

describe('BundleProductDetail', () => {
  let component;
  const props = {
    currentProduct: {},
    navigation: {
      getScreenProps: () => {
        return { deviceTier: 'tier1' };
      },
      getParam: jest.fn(),
      setParams: jest.fn(),
    },
    selectedColorProductId: 1,
    plpLabels: null,
    shortDescription: '',
    itemPartNumber: '',
    longDescription: '',
    pdpLabels: {},
  };

  beforeEach(() => {
    component = shallow(<ProductBundleVanilla {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });
  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
  it('should return styled bodycopy component value one', () => {
    expect(component.find('Styled(BodyCopy)')).toHaveLength(1);
  });
  it('should return styled LineComp component value zero', () => {
    expect(component.find('Styled(LineComp)')).toHaveLength(0);
  });

  it('should return styled PromotionalMessage component value zero', () => {
    expect(component.find('Styled(PromotionalMessage)')).toHaveLength(0);
  });
  it('should render expected number of components', () => {
    expect(component.find('View').length).toBe(0);
  });

  let updatedcomponent;

  const newProps = {
    ...props,
    isNewReDesignEnabled: true,
  };

  beforeEach(() => {
    updatedcomponent = shallow(<ProductBundleVanilla {...newProps} />);
  });

  it('should be defined', () => {
    expect(updatedcomponent).toBeDefined();
  });

  it('should renders correctly', () => {
    expect(updatedcomponent).toMatchSnapshot();
  });
  it('getParam should be called ', () => {
    expect(props.navigation.getParam).toBeCalled();
  });
  it('setParams should be called ', () => {
    expect(props.navigation.setParams).toBeCalled();
  });

  let wrapper;

  const newPropsWithOldDesign = {
    ...props,
    isNewReDesignEnabled: false,
  };

  beforeEach(() => {
    wrapper = shallow(<ProductBundleVanilla {...newPropsWithOldDesign} />);
  });

  it('wrapper component should be defined', () => {
    expect(wrapper).toBeDefined();
  });

  it('should renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('getParam should be called ', () => {
    expect(props.navigation.getParam).toBeCalled();
  });
  it('setParams should be called ', () => {
    expect(props.navigation.setParams).toBeCalled();
  });
});
