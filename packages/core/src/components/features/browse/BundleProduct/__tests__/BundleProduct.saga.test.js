import { put } from 'redux-saga/effects';
import { fetchBundleProductDetail } from '../container/BundleProduct.saga';
import { setLoadingState } from '../container/BundleProduct.actions';

describe('Bundle Product Saga', () => {
  const payload = {
    payload: '',
    url: '',
  };
  it('should fetch bundle Product details', () => {
    const getFetchBundleProductDetailGen = fetchBundleProductDetail({ payload });
    getFetchBundleProductDetailGen.next();
    getFetchBundleProductDetailGen.next();
    getFetchBundleProductDetailGen.next();
    expect(getFetchBundleProductDetailGen.next().value).toEqual(
      put(setLoadingState({ isLoading: true }))
    );
  });
});
