// 9fbef606107a605d69c0edbcd8029e5d 
import { getLabelValue, getABtestFromState } from '../../../../../utils';
import { processBreadCrumbs } from '../../ProductListing/container/ProductListing.util';

export const getNavTree = state => {
  return state.Navigation.navigationData;
};

export const prodDetails = state => {
  return state.BundleProduct;
};

export const getBreadCrumbs = state => {
  return processBreadCrumbs(state.BundleProduct && state.BundleProduct.breadCrumbs);
};

export const getAlternateSizes = state => {
  return state.BundleProduct.currentProduct && state.BundleProduct.currentProduct.alternateSizes;
};

export const getDescription = state => {
  return state.BundleProduct.currentProduct && state.BundleProduct.currentProduct.longDescription;
};

export const getRatingsProductId = state => {
  return state.BundleProduct.currentProduct && state.BundleProduct.currentProduct.ratingsProductId;
};

export const getGeneralProductId = state => {
  return state.BundleProduct.currentProduct && state.BundleProduct.currentProduct.generalProductId;
};

export const getShortDescription = state => {
  return state.BundleProduct.currentProduct && state.BundleProduct.currentProduct.shortDescription;
};

export const getPlpLabels = state => {
  if (!state.Labels || !state.Labels.PLP)
    return {
      addToBag: '',
      update: '',
      errorMessage: '',
      size: '',
      fit: '',
      color: '',
      quantity: '',
      sizeUnavalaible: '',
      sizeAvailable: '',
    };

  const {
    PLP: {
      plpTiles: {
        lbl_add_to_bag: addToBag,
        lbl_pdp_update: update,
        lbl_pdp_size_error: errorMessage,
        lbl_pdp_size: size,
        lbl_pdp_fit: fit,
        lbl_pdp_color: color,
        lbl_pdp_quantity: quantity,
        lbl_size_unavailable_online: sizeUnavalaible,
        lbl_other_sizes_available: sizeAvailable,
      },
    },
  } = state.Labels;

  return {
    addToBag,
    errorMessage,
    size,
    fit,
    color,
    quantity,
    update,
    sizeUnavalaible,
    sizeAvailable,
  };
};

// TODO - This is temporary - fix it by introducing the image carousel and zoom
export const getDefaultImage = state => {
  const firstColor =
    state.BundleProduct.currentProduct &&
    state.BundleProduct.currentProduct.colorFitsSizesMap &&
    state.BundleProduct.currentProduct.colorFitsSizesMap[0].color.name;
  return (
    firstColor &&
    state.BundleProduct.currentProduct &&
    state.BundleProduct.currentProduct.imagesByColor[firstColor].basicImageUrl
  );
};

export const getCurrentCurrency = state => {
  return state.session.siteDetails.currency;
};

export const getCurrencyAttributes = state => {
  return (
    (state.session.siteDetails && state.session.siteDetails.currencyAttributes) || {
      exchangevalue: 1,
      merchantMargin: 1,
      roundMethod: '',
    }
  );
};

export const getCurrentProduct = state => {
  return state.BundleProduct && state.BundleProduct.currentProduct;
};

export const getCurrentBundle = state => {
  return state.BundleProduct && state.BundleProduct.currentBundle;
};

export const getPDPLabels = state => {
  return {
    fullSize: getLabelValue(state.Labels, 'lbl_full_size', 'PDP_Sub', 'ProductDetailPage'),
    promoArea1: getLabelValue(state.Labels, 'lbl_promo_area_1', 'PDP_Sub', 'ProductDetailPage'),
    promoArea: getLabelValue(state.Labels, 'lbl_promo_area', 'PDP_Sub', 'ProductDetailPage'),
    promoArea3: getLabelValue(state.Labels, 'lbl_promo_area_3', 'PDP_Sub', 'ProductDetailPage'),
    completeTheLook: getLabelValue(
      state.Labels,
      'lbl_complete_the_look',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    myStylePlace: getLabelValue(state.Labels, 'lbl_my_style_place', 'PDP_Sub', 'ProductDetailPage'),
    ratingReview: getLabelValue(state.Labels, 'lbl_rating_review', 'PDP_Sub', 'ProductDetailPage'),
    ShowMore: getLabelValue(
      state.Labels,
      'lbl_product_description_show_more',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    ShowLess: getLabelValue(
      state.Labels,
      'lbl_product_description_show_less',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    writeAReview: getLabelValue(state.Labels, 'lbl_write_review', 'PDP_Sub', 'ProductDetailPage'),
    ProductDescription: getLabelValue(
      state.Labels,
      'lbl_product_description_label',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    ClaimMessage: getLabelValue(
      state.Labels,
      'lbl_product_description_claim_message',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    PartNumber: getLabelValue(
      state.Labels,
      'lbl_product_description_item_part_number',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    preferSendingViaEmail: getLabelValue(
      state.Labels,
      'lbl_prefer_sending_via_email',
      'PDP_Sub',
      'Browse'
    ),
    sendAnEmailCard: getLabelValue(
      state.Labels,
      'lbl_send_an_email_card',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    freeShippingEveryDay: getLabelValue(
      state.Labels,
      'lbl_free_shipping_every_day',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    back: getLabelValue(state.Labels, 'lbl_back', 'PDP_Sub', 'ProductDetailPage'),
    eGiftCardLink: getLabelValue(state.Labels, 'eGiftCardLink', 'PDP_Sub', 'ProductDetailPage'),
    chooseItemBtnLbl: getLabelValue(
      state.Labels,
      'lbl_bundleproduct_choosecta',
      'PDP_Sub',
      'ProductDetailPage'
    ),
  };
};

export const getOutfitLabels = state => {
  return state.Labels.OutfitLabels && state.Labels.OutfitLabels.Outfit_Sub;
};

export const getAddedToBagErrorCatId = state => {
  return state.AddedToBagReducer.get('errorCatId');
};

export const getLoadingState = state => {
  return state.BundleProduct && state.BundleProduct.isLoading;
};

export const getAccessibilityLabels = state => {
  return {
    lbl_social_twitter: getLabelValue(
      state.Labels,
      'lbl_social_twitter',
      'accessibility',
      'global'
    ),
    lbl_social_facebook: getLabelValue(
      state.Labels,
      'lbl_social_facebook',
      'accessibility',
      'global'
    ),
    lbl_social_pinterest: getLabelValue(
      state.Labels,
      'lbl_social_pinterest',
      'accessibility',
      'global'
    ),
  };
};

export const getOnModelImageAbTestBundlePdp = state => {
  return getABtestFromState(state.AbTest).enableOnModelImageOnBundlePDP;
};

export const getCollectionLabels = state => {
  return {
    youMayAlsoLike: getLabelValue(
      state.Labels,
      'lbl_you_may_also_like',
      'CollectionsLabels_Sub',
      'CollectionsLabels'
    ),
    recentlyViewed: getLabelValue(
      state.Labels,
      'lbl_recently_viewed',
      'CollectionsLabels_Sub',
      'CollectionsLabels'
    ),
  };
};

export const getPageUrl = state => {
  return state.BundleProduct.pageUrl;
};
