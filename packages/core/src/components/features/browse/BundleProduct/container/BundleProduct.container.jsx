/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
/*
  @Important
  If any changes specific to add to bag from collection or outfit container then make sure that the same changes should be done in the both outfitDetail.container and BundleProduct.container file. The Outfit and collection designs are same so we are using outfit view on the both container.
*/

import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { getAlternateBrandName } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import { getBrandNameFromHref, extractPID, isServer } from '@tcp/core/src/utils/utils';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import { isCanada, isMobileApp } from '@tcp/core/src/utils';
import { getProductDetails, clearBundleState } from './BundleProduct.actions';
import {
  getOutfitDetails,
  resetOutfitDetails,
  clearOutfitDetails,
  initActions,
  setSelectedOutfitProductList,
} from '../../OutfitDetails/container/OutfitDetails.actions';

import BundleProduct from '../views';
import {
  getCurrentProduct,
  getPlpLabels,
  getPDPLabels,
  getShortDescription,
  getGeneralProductId,
  getDescription,
  getCurrentCurrency,
  getCurrencyAttributes,
  getAlternateSizes,
  getCurrentBundle,
  getAddedToBagErrorCatId,
  getBreadCrumbs,
  prodDetails,
  getOutfitLabels,
  getLoadingState,
  getAccessibilityLabels,
  getOnModelImageAbTestBundlePdp,
  getCollectionLabels,
  getPageUrl,
} from './BundleProduct.selectors';
import {
  getIsOutfitAddedCTAEnabled,
  getShowAddedToBagCTA,
  getLabels,
  getSelectedProductList,
} from '../../OutfitDetails/container/OutfitDetails.selectors';
import getAddedToBagFormValues from '../../../../../reduxStore/selectors/form.selectors';
import { OUTFIT_LISTING_FORM } from '../../../../../constants/reducer.constants';
import { getCartItemInfo } from '../../../CnC/AddedToBag/util/utility';
import {
  addToCartEcom,
  addToCartEcomNewDesign,
  clearAddToBagErrorState,
  clearAddToCartMultipleItemErrorState,
  addMultipleItemsToCartEcom,
  addMultipleItemsToCartEcomNew,
} from '../../../CnC/AddedToBag/container/AddedToBag.actions';
import {
  getAddedToBagError,
  getMultipleItemsAddedToBagError,
} from '../../../CnC/AddedToBag/container/AddedToBag.selectors';
import { getIsPickupModalOpen } from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import {
  addItemsToWishlist,
  removeAddToFavoriteErrorState,
  getUserFavt,
  setLastDeletedItemIdAction,
} from '../../Favorites/container/Favorites.actions';
import {
  isPlccUser,
  getUserLoggedInState,
  isRememberedUser,
} from '../../../account/User/container/User.selectors';
import {
  fetchAddToFavoriteErrorMsg,
  wishListFromState,
  fetchErrorMessages,
} from '../../Favorites/container/Favorites.selectors';
import {
  getIsKeepAliveProduct,
  getIsKeepAliveProductApp,
  getIsInternationalShipping,
  getIsNewReDesignEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import { getLabelsOutOfStock } from '../../ProductListing/container/ProductListing.selectors';
import BundleProductItemsSkeleton from '../molecules/BundleProductItemsSkeleton';
import { getPDPTopPromos } from '../../ProductDetail/container/ProductDetail.selectors';
import BAGPAGE_SELECTORS from '../../../CnC/BagPage/container/BagPage.selectors';

export class ProductBundleContainer extends React.PureComponent {
  selectedColorProductId;

  pathname;

  static getInitialProps = async ({ props, asPath, req }) => {
    const { pageUrl, clearBundleDetails } = props;
    this.pathname = asPath || req.originalUrl;
    if (pageUrl && pageUrl !== this.pathname) {
      clearBundleDetails();
    }
  };

  constructor(props) {
    super(props);
    const { navigation } = props;
    this.selectedColorProductId =
      (navigation && navigation.getParam('selectedColorProductId')) || '';
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.makeApiCall();
    if (!navigation) window.scrollTo(0, 100);
  }

  componentDidUpdate(prevProps) {
    const { router } = this.props;
    const { router: prevRouter } = prevProps;
    const asPath = (router && router.asPath) || null;
    const prevAsPath = (prevRouter && prevRouter.asPath) || null;
    if (prevAsPath && asPath && prevAsPath !== asPath) {
      this.makeApiCall();
    }
  }

  componentWillUnmount() {
    const { resetOutFitProductState, clearOutFitProductState, clearBundleDetails } = this.props;
    resetOutFitProductState();
    clearOutFitProductState();
    clearBundleDetails();
  }

  makeApiCall = () => {
    const { getDetails, router, pageUrl, navigation, alternateBrand } = this.props;
    const asPath = (router && router.asPath) || null;
    const productId = extractPID(router, navigation);
    const { router: { asPath: asPathVal } = {} } = this.props;
    const brandName = getBrandNameFromHref(asPathVal) || alternateBrand;
    if (!isMobileApp()) {
      this.pathname =
        typeof window !== 'undefined' ? asPath || window.location.pathname : this.pathname;
      const [url] = this.pathname.split('?');
      if ((pageUrl && pageUrl !== url) || !pageUrl) {
        getDetails({ productId, url, ignoreCache: true, otherBrand: brandName });
      }
    } else {
      getDetails({ productId, ignoreCache: true, otherBrand: brandName });
    }
  };

  checkIfFromPLPorSLP = () => {
    const { router } = this.props;
    if (!isServer() && router?.components) {
      const isProductListing = Object.prototype.hasOwnProperty.call(
        router.components,
        '/ProductListing'
      );
      const isSearchPage = Object.prototype.hasOwnProperty.call(router.components, '/SearchDetail');
      return isProductListing || isSearchPage;
    }
    return false;
  };

  extractPID = () => {
    const { router, navigation } = this.props;
    let pid;
    if (navigation) {
      pid = navigation.getParam('pdpUrl');
    } else {
      const [prodId] = (router.query.bid || '').split('&');
      pid = prodId || '';
    }

    const id = pid && pid.split('-');
    return id && id.length > 1 ? id[id.length - 1] : pid;
  };

  handleAddToBag = (addToBagEcom, productInfo, generalProductId, currentState, primaryBrand) => {
    const { isEnabledOutfitAddedCTA, router, alternateBrand, addToBagEcomNew, isNewReDesign } =
      this.props;
    const asPath = (router && router.asPath) || null;
    const brandName = getBrandNameFromHref(asPath) || alternateBrand;
    const formName = `${OUTFIT_LISTING_FORM}-${generalProductId}`;
    const formValues = getAddedToBagFormValues(currentState, formName);
    let cartItemInfo = getCartItemInfo(productInfo, formValues);
    cartItemInfo = {
      ...cartItemInfo,
      skipAddToBagModal: isEnabledOutfitAddedCTA,
      primaryBrand,
      alternateBrand: brandName,
    };
    if (isMobileApp() && isNewReDesign) {
      cartItemInfo = {
        ...cartItemInfo,
        isOpenNewATBModal: true,
      };
      addToBagEcomNew(cartItemInfo);
    } else {
      addToBagEcom(cartItemInfo);
    }
  };

  handleMultipleAddToBag = (addMultipleToBagEcom, productList, currentState, isSBP = false) => {
    const { addMultipleToBagEcomNew, isNewReDesign } = this.props;
    if (productList.length === 1) {
      const { outfitProduct } = productList[0];
      const { generalProductId } = outfitProduct;
      const { addToBagEcom } = this.props;
      this.handleAddToBag(addToBagEcom, outfitProduct, generalProductId, currentState);
      return;
    }
    const productItemsInfo = productList.map((productInfo) => {
      const { outfitProduct } = productInfo;
      const { generalProductId } = outfitProduct;
      return this.getCartInfo(
        isNewReDesign && isMobileApp() ? addMultipleToBagEcomNew : addMultipleToBagEcom,
        outfitProduct,
        generalProductId,
        currentState,
        isSBP
      );
    });
    if (isNewReDesign && isMobileApp()) {
      addMultipleToBagEcomNew({ productItemsInfo });
    } else {
      addMultipleToBagEcom({ productItemsInfo });
    }
  };

  getCartInfo = (addToBagEcom, productInfo, generalProductId, currentState, isSBP) => {
    const { isEnabledOutfitAddedCTA } = this.props;
    const {
      newPartNumber: multipackProduct,
      partIdDetail: partIdInfo,
      multiPackCount,
      getMultiPackAllColor: getAllMultiPack,
      getNewMultiPackAllColor: getAllNewMultiPack,
      selectedMultipack,
    } = productInfo;
    // RWD-16438 Fix: Same form name is removed if user go from outfit to PDP and then press back button.
    const formName = `${OUTFIT_LISTING_FORM}-${generalProductId}`;
    const formValues = getAddedToBagFormValues(currentState, formName);
    let cartItemInfo = getCartItemInfo(productInfo, formValues);
    cartItemInfo = {
      ...cartItemInfo,
      skipAddToBagModal: isEnabledOutfitAddedCTA,
      multipackProduct,
      multiPackCount,
      getAllMultiPack,
      getAllNewMultiPack,
      selectedMultipack,
      partIdInfo,
    };
    cartItemInfo.addedFromSBP = isSBP;
    return cartItemInfo;
  };

  render() {
    const {
      currentProduct,
      plpLabels,
      pdpLabels,
      navigation,
      longDescription,
      shortDescription,
      itemPartNumber,
      currency,
      currencyAttributes,
      currentBundle,
      addToBagEcom,
      currentState,
      addToBagError,
      multipleAddToBagError,
      addToBagErrorId,
      isPickupModalOpen,
      addToFavorites,
      isLoggedIn,
      isPlcc,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      breadCrumbs,
      productDetails,
      outfitLabels,
      isKeepAliveEnabled,
      outOfStockLabels,
      toastMessage,
      isLoading,
      topPromos,
      accessibilityLabels,
      isOnModelAbTestBundlePdp,
      collectionLabels,
      isInternationalShipping,
      currencySign,
      isEnabledOutfitAddedCTA,
      showAddedToBagCta,
      getUserFavtList,
      onSetLastDeletedItemIdAction,
      defaultWishListFromState,
      errorMessages,
      labels,
      setSelectedOutfitProduct,
      addMultipleToBagEcom,
      selectedOutfitProducts,
      alternateBrand,
    } = this.props;
    const { router: { asPath: asPathVal } = {} } = this.props;
    const brandName = getBrandNameFromHref(asPathVal);
    const productItemsInfo = selectedOutfitProducts.map((productInfo) => {
      const { outfitProduct } = productInfo;
      const { generalProductId } = outfitProduct;
      const formName = `${OUTFIT_LISTING_FORM}-${generalProductId}`;
      const formValues = getAddedToBagFormValues(currentState, formName);
      return { ...productInfo, quantity: formValues.quantity };
    });
    const fromPLPPage = this.checkIfFromPLPorSLP();
    const multiErrorMessage = multipleAddToBagError && multipleAddToBagError.errMsg;
    return (
      <>
        {!isLoading ? (
          <BundleProduct
            labels={labels}
            currentProduct={currentProduct}
            selectedColorProductId={this.selectedColorProductId}
            plpLabels={plpLabels}
            pdpLabels={pdpLabels}
            outfitLabels={outfitLabels}
            navigation={navigation}
            shortDescription={shortDescription}
            itemPartNumber={itemPartNumber}
            longDescription={longDescription}
            currency={currency}
            currencyAttributes={currencyAttributes}
            currentBundle={currentBundle}
            handleAddToBag={this.handleAddToBag}
            addToBagEcom={addToBagEcom}
            currentState={currentState}
            addToBagError={addToBagError}
            addToBagErrorId={addToBagErrorId}
            multipleAddToBagError={multiErrorMessage}
            isPickupModalOpen={isPickupModalOpen}
            addToFavorites={addToFavorites}
            isLoggedIn={isLoggedIn}
            isPlcc={isPlcc}
            AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
            removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
            breadCrumbs={breadCrumbs}
            productDetails={productDetails}
            isKeepAliveEnabled={isKeepAliveEnabled}
            outOfStockLabels={outOfStockLabels}
            toastMessage={toastMessage}
            topPromos={topPromos}
            accessibilityLabels={accessibilityLabels}
            isMatchingFamily // TODO: Need to add kill switch for this
            isOnModelAbTestBundlePdp={isOnModelAbTestBundlePdp}
            collectionLabels={collectionLabels}
            isInternationalShipping={isInternationalShipping}
            currencySign={currencySign}
            isEnabledOutfitAddedCTA={isEnabledOutfitAddedCTA}
            showAddedToBagCta={showAddedToBagCta}
            getUserFavtList={getUserFavtList}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            wishList={defaultWishListFromState}
            defaultWishListFromState={defaultWishListFromState}
            isOutfitPage={false}
            isFromBundlePage={true}
            errorMessages={errorMessages}
            alternateBrand={brandName || alternateBrand}
            setSelectedOutfitProduct={setSelectedOutfitProduct}
            selectedOutfitProducts={productItemsInfo}
            handleMultipleAddToBag={this.handleMultipleAddToBag}
            addMultipleToBagEcom={addMultipleToBagEcom}
            isNewReDesignEnabled={true}
            fromPLPPage={fromPLPPage}
          />
        ) : (
          <BundleProductItemsSkeleton alternateBrand={brandName || alternateBrand} />
        )}
      </>
    );
  }
}

ProductBundleContainer.pageInfo = {
  pageId: 'b',
};

ProductBundleContainer.getInitActions = () => initActions;

function mapStateToProps(state) {
  return {
    labels: getLabels(state),
    currentProduct: getCurrentProduct(state),
    isLoading: getLoadingState(state),
    productDetails: prodDetails(state),
    plpLabels: getPlpLabels(state),
    pdpLabels: getPDPLabels(state),
    shortDescription: getShortDescription(state),
    longDescription: getDescription(state),
    itemPartNumber: getGeneralProductId(state),
    currency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    alternateSizes: getAlternateSizes(state),
    currentBundle: getCurrentBundle(state),
    currentState: state,
    addToBagError: getAddedToBagError(state),
    multipleAddToBagError: getMultipleItemsAddedToBagError(state),
    addToBagErrorId: getAddedToBagErrorCatId(state),
    isPickupModalOpen: getIsPickupModalOpen(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    isPlcc: isPlccUser(state),
    AddToFavoriteErrorMsg: fetchAddToFavoriteErrorMsg(state),
    breadCrumbs: getBreadCrumbs(state),
    outfitLabels: getOutfitLabels(state),
    isKeepAliveEnabled: isMobileApp()
      ? getIsKeepAliveProductApp(state)
      : getIsKeepAliveProduct(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    topPromos: getPDPTopPromos(state),
    accessibilityLabels: getAccessibilityLabels(state),
    isOnModelAbTestBundlePdp: getOnModelImageAbTestBundlePdp(state),
    collectionLabels: getCollectionLabels(state),
    isInternationalShipping: getIsInternationalShipping(state),
    currencySign: BAGPAGE_SELECTORS.getCurrentCurrency(state),
    pageUrl: getPageUrl(state),
    isEnabledOutfitAddedCTA: !isCanada() && getIsOutfitAddedCTAEnabled(state),
    showAddedToBagCta: getShowAddedToBagCTA(state),
    defaultWishListFromState: wishListFromState(state),
    errorMessages: fetchErrorMessages(state),
    selectedOutfitProducts: getSelectedProductList(state),
    alternateBrand: getAlternateBrandName(state),
    isNewReDesign: getIsNewReDesignEnabled(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getDetails: (payload) => {
      dispatch(getProductDetails(payload));
    },
    addToBagEcom: (payload) => {
      dispatch(addToCartEcom(payload));
    },
    addToBagEcomNew: (payload) => {
      dispatch(addToCartEcomNewDesign(payload));
    },
    clearAddToBagError: () => {
      dispatch(clearAddToBagErrorState());
    },
    clearMultipleAddToBagError: () => {
      dispatch(clearAddToCartMultipleItemErrorState());
    },
    clearBundleDetails: () => {
      dispatch(clearBundleState());
    },
    addToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
    removeAddToFavoritesErrorMsg: (payload) => {
      dispatch(removeAddToFavoriteErrorState(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    getUserFavtList: (payload) => {
      dispatch(getUserFavt(payload));
    },
    onSetLastDeletedItemIdAction: (payload) => {
      dispatch(setLastDeletedItemIdAction(payload));
    },
    setSelectedOutfitProduct: (payload) => {
      dispatch(setSelectedOutfitProductList(payload));
    },
    resetOutFitProductState: () => {
      dispatch(resetOutfitDetails());
    },
    clearOutFitProductState: () => {
      dispatch(clearOutfitDetails());
    },
    getOutfit: (payload) => {
      dispatch(getOutfitDetails(payload));
    },
    addMultipleToBagEcom: (payload) => {
      dispatch(addMultipleItemsToCartEcom(payload));
    },
    addMultipleToBagEcomNew: (payload) => {
      dispatch(addMultipleItemsToCartEcomNew(payload));
    },
  };
}

ProductBundleContainer.propTypes = {
  labels: PropTypes.shape({}),
  getDetails: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  currentProduct: PropTypes.shape({}).isRequired,
  currentBundle: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  plpLabels: PropTypes.shape({}),
  pdpLabels: PropTypes.shape({}),
  shortDescription: PropTypes.string,
  itemPartNumber: PropTypes.string,
  longDescription: PropTypes.string,
  currency: PropTypes.string,
  currencyAttributes: PropTypes.shape({}),
  addToBagEcom: PropTypes.func,
  setSelectedOutfitProduct: PropTypes.func,
  addMultipleToBagEcom: PropTypes.func,
  resetOutFitProductState: PropTypes.func,
  clearOutFitProductState: PropTypes.func,
  clearBundleDetails: PropTypes.func,
  currentState: PropTypes.shape({}),
  multipleAddToBagError: PropTypes.string,
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
  isPickupModalOpen: PropTypes.bool,
  addToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  isPlcc: PropTypes.bool,
  router: PropTypes.shape({}).isRequired,
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  breadCrumbs: PropTypes.shape({}),
  productDetails: PropTypes.arrayOf(PropTypes.shape({})),
  formValues: PropTypes.shape({}).isRequired,
  outfitLabels: PropTypes.shape({}),
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  toastMessage: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  topPromos: PropTypes.shape({}),
  accessibilityLabels: PropTypes.shape({}),
  isOnModelAbTestBundlePdp: PropTypes.bool,
  collectionLabels: PropTypes.shape({}),
  isInternationalShipping: PropTypes.bool.isRequired,
  currencySign: PropTypes.string,
  pageUrl: PropTypes.string.isRequired,
  isEnabledOutfitAddedCTA: PropTypes.bool,
  showAddedToBagCta: PropTypes.string,
  onSetLastDeletedItemIdAction: PropTypes.func,
  getUserFavtList: PropTypes.func,
  defaultWishListFromState: PropTypes.shape({}),
  errorMessages: PropTypes.shape({}),
  selectedOutfitProducts: PropTypes.shape({}),
  isNewReDesign: PropTypes.bool,
};

ProductBundleContainer.defaultProps = {
  labels: {},
  plpLabels: {},
  pdpLabels: {},
  outfitLabels: {},
  shortDescription: '',
  itemPartNumber: '',
  longDescription: '',
  currency: 'USD',
  currencyAttributes: {
    exchangevalue: 1,
  },
  addToBagEcom: () => {},
  setSelectedOutfitProduct: () => {},
  resetOutFitProductState: () => {},
  clearOutFitProductState: () => {},
  clearBundleDetails: () => {},
  addMultipleToBagEcom: () => {},
  currentState: {},
  multipleAddToBagError: '',
  addToBagError: '',
  addToBagErrorId: '',
  isPickupModalOpen: false,
  addToFavorites: () => {},
  isLoggedIn: false,
  isPlcc: false,
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  breadCrumbs: [],
  productDetails: [],
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  isLoading: false,
  topPromos: null,
  accessibilityLabels: {},
  isOnModelAbTestBundlePdp: false,
  collectionLabels: {},
  currencySign: '',
  isEnabledOutfitAddedCTA: false,
  showAddedToBagCta: '',
  onSetLastDeletedItemIdAction: () => {},
  getUserFavtList: () => {},
  defaultWishListFromState: {},
  errorMessages: {},
  selectedOutfitProducts: [],
  isNewReDesign: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductBundleContainer);
