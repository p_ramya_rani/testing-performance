// 9fbef606107a605d69c0edbcd8029e5d
import { loadLayoutData, loadModulesData } from '@tcp/core/src/reduxStore/actions';
import { call, put, takeLatest, select } from 'redux-saga/effects';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import BUNDLEPRODUCT_CONSTANTS from './BundleProduct.constants';
import {
  setProductDetails,
  setBundleDetails,
  setLoadingState,
  setPageUrl,
} from './BundleProduct.actions';
import {
  getIsPdpServiceEnabled,
  getABTestPdpService,
} from '../../../../../reduxStore/selectors/session.selectors';
import getProductInfoById, {
  layoutResolver,
} from '../../../../../services/abstractors/productListing/productDetail';
import { getDefaultWishList } from '../../Favorites/container/Favorites.saga';
import getBundleProductsDetails from '../../../../../services/abstractors/productListing/bundleProducts';
import logger from '../../../../../utils/loggerInstance';

export function* fetchBundleProductDetail({ payload: { productId, url, otherBrand } }) {
  try {
    const xappURLConfig = yield call(getUnbxdXappConfigs);
    const pageName = 'pdp';
    yield put(setProductDetails({ product: {} }));
    const state = yield select();
    yield put(setLoadingState({ isLoading: true }));
    const isPdpServiceEnabled = yield select(getIsPdpServiceEnabled);
    const enablePDPServiceABTest = yield select(getABTestPdpService);
    const productDetail = yield call(
      getProductInfoById,
      xappURLConfig,
      productId,
      state,
      null,
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      isPdpServiceEnabled,
      enablePDPServiceABTest,
      null,
      otherBrand
    );
    yield put(setProductDetails({ ...productDetail }));
    const bundledProducts = productDetail.product.bundleProducts;

    const bundleDetails = yield call(
      getBundleProductsDetails,
      xappURLConfig,
      { bundleProducts: bundledProducts },
      state,
      isPdpServiceEnabled,
      enablePDPServiceABTest,
      otherBrand
    );
    yield put(setBundleDetails([...bundleDetails]));
    yield call(getDefaultWishList);
    yield put(setLoadingState({ isLoading: false }));
    if (bundledProducts && bundledProducts.length > 0) {
      const category = bundledProducts[0] && bundledProducts[0].category;
      const { layout, modules } = yield call(layoutResolver, { category, pageName });
      yield put(loadLayoutData(layout, pageName));
      yield put(loadModulesData(modules));
      yield put(setPageUrl(url));
    }
  } catch (err) {
    logger.error('error: ', err);
    yield put(setLoadingState({ isLoading: false }));
  }
}

function* BundleProductSaga() {
  yield takeLatest(BUNDLEPRODUCT_CONSTANTS.FETCH_BUNDLE_DETAILS, fetchBundleProductDetail);
}

export default BundleProductSaga;
