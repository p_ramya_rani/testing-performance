// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { BundleProductItemsSkeletonVanilla } from '../views/BundleProductItemsSkeleton.view';

describe('BundleProductItemsSkeletonVanilla', () => {
  let component;
  const props = {};

  beforeEach(() => {
    component = shallow(<BundleProductItemsSkeletonVanilla {...props} />);
  });

  it('BundleProductItemsSkeletonVanilla should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
