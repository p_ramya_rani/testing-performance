// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import OutfitProduct from '@tcp/core/src/components/features/browse/OutfitDetails/molecules/OutfitProduct/OutfitProduct';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/BundleProductItems.style';

class BundleProductItems extends React.PureComponent {
  /**
   * @function renderItem populates the L1 menu item from the data passed to it
   * @param {object} item Details of the L1 menu item passed from the loop
   */
  checkInWishlist = (wishList, productId) => {
    return wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;
  };

  renderItems = () => {
    const {
      currentBundle,
      plpLabels,
      addToBagEcom,
      addToFavorites,
      currentState,
      addToBagError,
      addToBagErrorId,
      handleAddToBag,
      isLoggedIn,
      currencySymbol,
      currencyAttributes,
      className,
      outfitLabels,
      isKeepAliveEnabled,
      outOfStockLabels,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      isOnModelAbTestBundlePdp,
      isInternationalShipping,
      isEnabledOutfitAddedCTA,
      showAddedToBagCta,
      wishList,
      onSetLastDeletedItemIdAction,
      isOutfitPage,
      errorMessages,
      alternateBrand,
    } = this.props;

    return (
      <ul className="outfiting-list-container">
        {currentBundle &&
          currentBundle.map((product) => {
            const productItem = product && product.products;
            const primaryBrand = (productItem && productItem.primaryBrand) || alternateBrand;
            return (
              <li key={product.generalProductId} className="bundle-product-item">
                <OutfitProduct
                  plpLabels={plpLabels}
                  labels={outfitLabels}
                  outfitProduct={productItem}
                  productIndexText=""
                  handleAddToBag={() => {
                    handleAddToBag(
                      addToBagEcom,
                      productItem,
                      productItem.generalProductId,
                      currentState,
                      primaryBrand
                    );
                  }}
                  className={`${className} outfiting-list-details`}
                  addToBagError={addToBagErrorId === productItem.productId && addToBagError}
                  isLoggedIn={isLoggedIn}
                  addToFavorites={addToFavorites}
                  currencySymbol={currencySymbol}
                  currencyAttributes={currencyAttributes}
                  isBundleProduct
                  isKeepAliveEnabled={isKeepAliveEnabled}
                  outOfStockLabels={outOfStockLabels}
                  AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                  removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                  pageName="BUNDLE"
                  isOnModelAbTestBundlePdp={isOnModelAbTestBundlePdp}
                  isInternationalShipping={isInternationalShipping}
                  showAddedToBagCta={
                    isEnabledOutfitAddedCTA && showAddedToBagCta === productItem.productId
                  }
                  isInWishList={this.checkInWishlist(wishList, productItem.productId)}
                  onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                  wishList={wishList}
                  isOutfitPage={isOutfitPage}
                  errorMessages={errorMessages}
                  alternateBrand={alternateBrand}
                />
              </li>
            );
          })}
      </ul>
    );
  };

  render() {
    return <div className="bundle-productsList-container">{this.renderItems()}</div>;
  }
}

BundleProductItems.propTypes = {
  currentBundle: PropTypes.shape({}),
  plpLabels: PropTypes.shape({}),
  outfitLabels: PropTypes.shape({}),
  addToBagEcom: PropTypes.func.isRequired,
  handleAddToBag: PropTypes.func.isRequired,
  currentState: PropTypes.shape({}).isRequired,
  addToBagError: PropTypes.string,
  addToBagErrorId: PropTypes.string,
  addToFavorites: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  currencyAttributes: PropTypes.shape({}).isRequired,
  currencySymbol: PropTypes.string,
  pdpLabels: PropTypes.shape({}),
  className: PropTypes.string,
  isKeepAliveEnabled: PropTypes.bool.isRequired,
  outOfStockLabels: PropTypes.shape({}),
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  isOnModelAbTestBundlePdp: PropTypes.bool,
  isInternationalShipping: PropTypes.bool.isRequired,
  isEnabledOutfitAddedCTA: PropTypes.bool,
  showAddedToBagCta: PropTypes.string,
  wishList: PropTypes.func.isRequired,
  onSetLastDeletedItemIdAction: PropTypes.func,
  isOutfitPage: PropTypes.bool,
  errorMessages: PropTypes.shape({}),
};

BundleProductItems.defaultProps = {
  currentBundle: null,
  plpLabels: {},
  outfitLabels: {},
  addToBagError: '',
  addToBagErrorId: '',
  isLoggedIn: false,
  currencySymbol: 'USD',
  pdpLabels: {},
  className: '',
  outOfStockLabels: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  isOnModelAbTestBundlePdp: false,
  isEnabledOutfitAddedCTA: false,
  showAddedToBagCta: '',
  onSetLastDeletedItemIdAction: () => {},
  isOutfitPage: false,
  errorMessages: {},
};
export default withStyles(BundleProductItems, styles);
export { BundleProductItems as BundleProductItemsVanilla };
