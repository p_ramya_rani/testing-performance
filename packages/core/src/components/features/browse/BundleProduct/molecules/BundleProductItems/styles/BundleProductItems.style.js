// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  && .pdp-qty {
    padding-top: 4px;
  }
  && .select__input {
    top: 0;
  }
  .product-information {
    width: calc(50% - 10px);
    @media ${props => props.theme.mediaQuery.large} {
      width: calc(100% - 30px);
    }
    max-width: 100%;
  }
  && .button-wrapper {
    position: relative;
    min-width: 100%;
  }
  .outfit-sku .size-error-message {
    max-width: 90%;
  }
`;
