// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { BundleProductItemsVanilla } from '../views/BundleProductItems.view.native';

describe('BundleProductItemsVanilla', () => {
  let component;
  const props = {
    currentBundle: [],
    plpLabels: {},
    isFromBundlePage: true,
    handleAddToBag: () => {},
    addToFavorites: () => {},
    addToBagEcom: () => {},
    currentState: {},
    navigation: {},
    labels: {},
    isLoggedIn: false,
    isNewReDesignEnabled: true,
    selectedOutfitProducts: [],
    setSelectedOutfitProduct: () => {},
  };

  beforeEach(() => {
    component = shallow(<BundleProductItemsVanilla {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
