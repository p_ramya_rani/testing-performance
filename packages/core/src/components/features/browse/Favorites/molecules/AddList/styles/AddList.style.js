// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .default-check-row {
    p {
      padding-top: 5px;
    }
    img {
      width: 15px;
      height: 13px;
      padding-left: 7px;
      vertical-align: middle;
    }
  }
`;
