// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import Image from '@tcp/core/src/components/common/atoms/Image';

const getAdditionalStyle = (props) => {
  const { margins } = props;
  return {
    ...(margins && { margin: margins }),
  };
};

const PageContainer = styled.View`
  justify-content: center;
`;

const RowContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: wrap;
  justify-content: flex-start;
  ${getAdditionalStyle}
`;

const NoFavoriteContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
`;

const RecommendationWrapper = styled.View`
  margin-left: -${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;
const DropDownContainer = styled.View`
  margin-top: 12px;
`;

const ShareDropDownContainer = styled.View`
  align-self: flex-end;
`;

const ListHeaderContainer = styled.View`
  background-color: ${(props) => props.theme.colors.WHITE};
`;

const ListFooterContainer = styled.View`
  padding-left: 12px;
  padding-right: 12px;
  background-color: ${(props) => props.theme.colors.WHITE};
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

const DropDownWishlistItemContainer = styled.TouchableOpacity.attrs({
  underlayColor: (props) => props.theme.colors.BUTTON.WHITE.ALT_FOCUS,
  activeOpacity: 1,
})`
  background-color: ${(props) => props.theme.colors.WHITE};
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const getAdditionalListItemStyle = (props) => {
  const { width } = props;
  return {
    ...(width && { width }),
  };
};

const SelectedWishlistContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-left: 12px;
  ${getAdditionalListItemStyle}
`;

const ItemCountContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

const OOSWarningWrapper = styled.View`
  flex-direction: row;
  align-self: center;
  margin: 46px -14px 21px;
  padding: 13px 0 12px;
  background-color: ${(props) => props.theme.colors.WARNING_BACKGROUND};
  position: relative;
`;

const WarningMessage = styled.View`
  width: 90%;
  padding: 0 10px;
`;

const OOSMessage = styled.Text`
  font-size: 14px;
  font-family: ${(props) => props.theme.typography.fonts.secondary};
`;

const RemoveNow = styled.Text`
  font-size: 14px;
  text-decoration: underline;
`;

const CloseImage = styled(Image)`
  top: 10px;
  right: 8px;
`;

export {
  PageContainer,
  RowContainer,
  NoFavoriteContainer,
  RecommendationWrapper,
  DropDownContainer,
  ShareDropDownContainer,
  ListHeaderContainer,
  ListFooterContainer,
  DropDownWishlistItemContainer,
  SelectedWishlistContainer,
  ItemCountContainer,
  OOSWarningWrapper,
  WarningMessage,
  OOSMessage,
  RemoveNow,
  CloseImage,
};
