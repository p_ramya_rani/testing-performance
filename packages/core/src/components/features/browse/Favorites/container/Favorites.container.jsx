/* eslint-disable max-lines */
// eslint-disable-next-line max-lines
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { fetchRecommendationsData } from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.actions';
import {
  openQuickViewWithValues,
  updateAppTypeWithParams,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import { isMobileApp } from '@tcp/core/src/utils/utils';
import * as labelsSelectors from '@tcp/core/src/reduxStore/selectors/labels.selectors';
import NavigationXHRSelectors from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.selectors';
import {
  getSuggestedProductErrors,
  getSuggestedProductState,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import { getCurrencyAttributes } from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import Favorites from '../views';
import { trackPageView, setClickAnalyticsData } from '../../../../../analytics/actions';
import {
  getSetWishlistsSummariesAction,
  createNewWishListMoveItemAction,
  deleteWishListAction,
  getActiveWishlistAction,
  getActiveWishlistGuestAction,
  createNewWishListAction,
  setLastDeletedItemIdAction,
  updateWishListAction,
  sendWishListMailAction,
  setWishListShareSuccess,
  setReplaceWishlistItem,
  setOSSItemID,
} from './Favorites.actions';

import {
  selectWishlistsSummaries,
  selectActiveWishlistId,
  selectActiveWishList,
  selectActiveWishlistProducts,
  selectActiveDisplayName,
  selectTotalProductsCount,
  fetchCurrencySymbol,
  getLabelsFavorites,
  getSLPLabels,
  getIsDataLoading,
  selectDefaultWishlist,
  getAllProductsAvailability,
  getAllBrandsAvailability,
  selectWishListShareStatus,
  getFormErrorLabels,
  fetchErrorMessages,
  getAccessibilityLabels,
  getOOSItemIdWhoseSuggestionRequested,
  getFavOOSRemoveLabel,
} from './Favorites.selectors';
import {
  getUserEmail,
  getUserLoggedInState,
  isRememberedUser,
} from '../../../account/User/container/User.selectors';
import { getLabelsOutOfStock } from '../../ProductListing/container/ProductListing.selectors';
import {
  getIsKeepAliveProduct,
  getIsShowPriceRange,
  getABTestIsShowPriceRange,
  getIsNewReDesignEnabled,
  getIsSNJEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';
import { addToCartEcom } from '../../../CnC/AddedToBag/container/AddedToBag.actions';

class FavoritesContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.guestAccessKey = '';
    this.wishListId = '';
  }

  state = {
    selectedColorProductId: '',
    filteredId: 'ALL',
    appliedFilterLength: 0,
    sortId: '',
    gymSelected: false,
    tcpSelected: false,
    snjSelected: false,
    suggestedColorProductId: '',
  };

  componentDidMount() {
    const { loadWishList, getActiveWishlistGuest } = this.props;

    if (!isMobileApp()) {
      this.wishListId = this.getParameterByName('wishlistId');
      this.guestAccessKey = this.getParameterByName('guestAccessKey');
      if (this.wishListId !== '' && this.guestAccessKey !== '') {
        const { wishListId, guestAccessKey } = this;
        getActiveWishlistGuest({ wishListId, guestAccessKey });
      }
    }
    if (this.wishListId === '' && this.guestAccessKey === '') {
      loadWishList({ isDataLoading: true });
    }
  }

  getParameterByName = (name) => {
    const location = typeof window !== 'undefined' && window.location && window.location.search;
    const key = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]');
    const regex = new RegExp(`[\\?&]${key}=([^&#]*)`);
    const results = regex.exec(location);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };

  onFilterSelection = (filteredId) => {
    this.setState({
      filteredId,
      appliedFilterLength: 1,
    });
  };

  onSortSelection = (sortId) => {
    this.setState({
      sortId,
    });
  };

  selectBrandType = (event) => {
    const {
      target: { id, checked },
    } = event;
    this.setState({
      gymSelected: id === 'gymboreeOption' && checked,
      tcpSelected: id === 'tcpOption' && checked,
      snjSelected: id === 'snjOption' && checked,
    });
  };

  resetBrandFilters = () => {
    this.setState({
      gymSelected: false,
      tcpSelected: false,
      snjSelected: false,
    });
  };

  openQuickViewModal = (payload, allColors) => {
    const { onQuickViewOpenClick } = this.props;
    const selectedColorProductId = !allColors ? payload.colorProductId : '';
    this.setState(
      {
        selectedColorProductId,
      },
      () => {
        onQuickViewOpenClick(payload);
      }
    );
  };

  onGoToPDPPage = (title, pdpUrl, selectedColorProductId, productInfo) => {
    const { navigation } = this.props;
    const { bundleProduct } = productInfo;
    const routeName = bundleProduct ? 'BundleDetail' : 'ProductDetail';
    navigation.navigate(routeName, {
      title,
      pdpUrl,
      selectedColorProductId,
      reset: true,
    });
  };

  deleteWishListHandler = (products, deleteOOSItems) => {
    const { deleteWishList, activeWishListId, activeWishList } = this.props;
    const { isDefault } = activeWishList;
    if (deleteWishList && activeWishListId) {
      const payload = {
        wishListId: activeWishListId,
        isDefault,
        products,
        deleteOOSItems,
      };
      deleteWishList(payload);
    }
  };

  onLoadRecommendations = (payload) => {
    const { loadRecommendations } = this.props;
    if (loadRecommendations) {
      loadRecommendations(payload);
    }
  };

  onReplaceWishlistItem = (data) => {
    const { replaceWishlistItem, hasSuggestedProduct } = this.props;

    if (isMobileApp()) {
      if (replaceWishlistItem) {
        replaceWishlistItem(data);
      }
    } else if (replaceWishlistItem && hasSuggestedProduct) {
      replaceWishlistItem(data);
    }
  };

  render() {
    const {
      wishlistsSummaries,
      activeWishList,
      createNewWishListMoveItem,
      getActiveWishlist,
      createNewWishList,
      setLastDeletedItemId,
      activeWishListId,
      activeWishListProducts,
      activeDisplayName,
      currencySymbol,
      labels,
      slpLabels,
      navigation,
      onQuickViewOpenClick,
      totalProductsCount,
      isDataLoading,
      labelsPlpTiles,
      isKeepAliveEnabled,
      outOfStockLabels,
      defaultWishList,
      updateWishList,
      isBothTcpAndGymProductAreAvailable,
      availableBrands,
      userEmail,
      sendWishListEmail,
      wishlistShareStatus,
      setListShareSuccess,
      formErrorMessage,
      isLoggedIn,
      updateAppTypeHandler,
      addToBagEcom,
      errorMessages,
      currencyAttributes,
      trackPageLoad,
      cookieToken,
      suggestedProductErrors,
      hasSuggestedProduct,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      setSuggestedColorProductId,
      favOOSRemoveLabel,
      isNewReDesignProductTile,
      isSNJEnabled,
    } = this.props;
    const { selectedColorProductId, suggestedColorProductId } = this.state;
    return (
      <Favorites
        wishlistsSummaries={wishlistsSummaries}
        activeWishList={activeWishList}
        createNewWishListMoveItem={createNewWishListMoveItem}
        deleteWishList={this.deleteWishListHandler}
        getActiveWishlist={getActiveWishlist}
        createNewWishList={createNewWishList}
        setLastDeletedItemId={setLastDeletedItemId}
        activeWishListId={activeWishListId}
        activeWishListProducts={activeWishListProducts}
        activeDisplayName={activeDisplayName}
        currencySymbol={currencySymbol}
        labels={labels}
        slpLabels={slpLabels}
        onQuickViewOpenClick={isMobileApp() ? onQuickViewOpenClick : this.openQuickViewModal}
        selectedColorProductId={selectedColorProductId}
        navigation={navigation}
        onGoToPDPPage={this.onGoToPDPPage}
        onFilterSelection={this.onFilterSelection}
        onSortSelection={this.onSortSelection}
        selectBrandType={this.selectBrandType}
        totalProductsCount={totalProductsCount}
        isDataLoading={isDataLoading}
        labelsPlpTiles={labelsPlpTiles}
        isKeepAliveEnabled={isKeepAliveEnabled}
        outOfStockLabels={outOfStockLabels}
        defaultWishList={defaultWishList}
        updateWishList={updateWishList}
        isBothTcpAndGymProductAreAvailable={isBothTcpAndGymProductAreAvailable}
        availableBrands={availableBrands}
        userEmail={userEmail}
        sendWishListEmail={sendWishListEmail}
        wishlistShareStatus={wishlistShareStatus}
        setListShareSuccess={setListShareSuccess}
        resetBrandFilters={this.resetBrandFilters}
        guestAccessKey={this.guestAccessKey}
        formErrorMessage={formErrorMessage}
        isLoggedIn={isLoggedIn}
        addToBagEcom={addToBagEcom}
        onLoadRecommendations={this.onLoadRecommendations}
        onReplaceWishlistItem={this.onReplaceWishlistItem}
        updateAppTypeHandler={updateAppTypeHandler}
        errorMessages={errorMessages}
        currencyAttributes={currencyAttributes}
        trackPageLoad={trackPageLoad}
        cookieToken={cookieToken}
        suggestedProductErrors={suggestedProductErrors}
        hasSuggestedProduct={hasSuggestedProduct}
        isShowPriceRangeKillSwitch={isShowPriceRangeKillSwitch}
        showPriceRangeForABTest={showPriceRangeForABTest}
        suggestedColorProductId={suggestedColorProductId}
        setSuggestedColorProductId={setSuggestedColorProductId}
        favOOSRemoveLabel={favOOSRemoveLabel}
        isNewReDesignProductTile={isNewReDesignProductTile}
        isSNJEnabled={isSNJEnabled}
        {...this.state}
      />
    );
  }
}

FavoritesContainer.pageInfo = {
  pageData: {
    loadAnalyticsOnload: false,
  },
};

const mapStateToProps = (state) => {
  const ooSItemIdWhoseSuggestionRequested = getOOSItemIdWhoseSuggestionRequested(state);
  return {
    wishlistsSummaries: selectWishlistsSummaries(state),
    activeWishList: selectActiveWishList(state),
    activeWishListId: selectActiveWishlistId(state),
    defaultWishList: selectDefaultWishlist(state),
    activeWishListProducts: selectActiveWishlistProducts(state),
    activeDisplayName: selectActiveDisplayName(state),
    currencySymbol: fetchCurrencySymbol(state),
    currencyAttributes: getCurrencyAttributes(state),
    labels: {
      ...getLabelsFavorites(state),
      ...getAccessibilityLabels(state),
    },
    slpLabels: getSLPLabels(state),
    totalProductsCount: selectTotalProductsCount(state),
    isDataLoading: getIsDataLoading(state),
    labelsPlpTiles: labelsSelectors.getPlpTilesLabels(state),
    isKeepAliveEnabled: getIsKeepAliveProduct(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    isBothTcpAndGymProductAreAvailable: getAllProductsAvailability(state),
    availableBrands: getAllBrandsAvailability(state),
    userEmail: getUserEmail(state),
    wishlistShareStatus: selectWishListShareStatus(state),
    formErrorMessage: getFormErrorLabels(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    errorMessages: fetchErrorMessages(state),
    cookieToken: NavigationXHRSelectors.getCookieToken(state),
    suggestedProductErrors: getSuggestedProductErrors(state, ooSItemIdWhoseSuggestionRequested),
    hasSuggestedProduct: getSuggestedProductState(state, ooSItemIdWhoseSuggestionRequested),
    isShowPriceRangeKillSwitch: getIsShowPriceRange(state),
    showPriceRangeForABTest: getABTestIsShowPriceRange(state),
    favOOSRemoveLabel: getFavOOSRemoveLabel(state),
    isNewReDesignProductTile: getIsNewReDesignEnabled(state),
    isSNJEnabled: getIsSNJEnabled(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadWishList: (payload) => dispatch(getSetWishlistsSummariesAction(payload)),
    createNewWishListMoveItem: (formData) => {
      dispatch(createNewWishListMoveItemAction(formData));
    },
    deleteWishList: (payload) => {
      dispatch(deleteWishListAction(payload));
    },
    getActiveWishlistGuest: (payload) => dispatch(getActiveWishlistGuestAction(payload)),
    getActiveWishlist: (payload) => dispatch(getActiveWishlistAction(payload)),
    createNewWishList: (formData) => {
      dispatch(createNewWishListAction(formData));
    },
    setLastDeletedItemId: (itemId) => {
      dispatch(setLastDeletedItemIdAction(itemId));
    },
    sendWishListEmail: (payload) => dispatch(sendWishListMailAction(payload)),
    setListShareSuccess: (payload) => dispatch(setWishListShareSuccess(payload)),
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    updateWishList: (payload) => {
      dispatch(updateWishListAction(payload));
    },
    addToBagEcom: (payload) => {
      dispatch(addToCartEcom(payload));
    },
    loadRecommendations: (action) => dispatch(fetchRecommendationsData(action)),
    replaceWishlistItem: (payload) => dispatch(setReplaceWishlistItem(payload)),
    updateAppTypeHandler: (payload) => {
      dispatch(updateAppTypeWithParams(payload));
    },
    setSuggestedColorProductId: (payload) => {
      dispatch(setOSSItemID(payload));
    },
    trackPageLoad: (payload) => {
      const { products, customEvents, pageName, ...restPayload } = payload;

      dispatch(
        setClickAnalyticsData({
          products,
          customEvents,
          pageName,
        })
      );
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...restPayload,
                },
              },
            },
          },
        })
      );

      const analyticsTimer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(analyticsTimer);
      }, 300);
    },
  };
};

FavoritesContainer.propTypes = {
  loadWishList: PropTypes.func.isRequired,
  wishlistsSummaries: PropTypes.arrayOf({}),
  activeWishList: PropTypes.shape({}),
  createNewWishListMoveItem: PropTypes.func.isRequired,
  deleteWishList: PropTypes.func.isRequired,
  sendWishListEmail: PropTypes.func.isRequired,
  getActiveWishlist: PropTypes.func.isRequired,
  createNewWishList: PropTypes.func.isRequired,
  setLastDeletedItemId: PropTypes.func.isRequired,
  activeWishListId: PropTypes.string.isRequired,
  activeWishListProducts: PropTypes.shape({}).isRequired,
  activeDisplayName: PropTypes.string.isRequired,
  onQuickViewOpenClick: PropTypes.func.isRequired,
  currencySymbol: PropTypes.string,
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  slpLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}).isRequired,
  totalProductsCount: PropTypes.string,
  isDataLoading: PropTypes.bool,
  labelsPlpTiles: PropTypes.shape({}),
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  defaultWishList: PropTypes.shape({}),
  updateWishList: PropTypes.func.isRequired,
  isBothTcpAndGymProductAreAvailable: PropTypes.bool.isRequired,
  availableBrands: PropTypes.arrayOf('').isRequired,
  userEmail: PropTypes.string.isRequired,
  wishlistShareStatus: PropTypes.bool,
  setListShareSuccess: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  addToBagEcom: PropTypes.func.isRequired,
  loadRecommendations: PropTypes.func.isRequired,
  replaceWishlistItem: PropTypes.func.isRequired,
  getActiveWishlistGuest: PropTypes.func.isRequired,
  formErrorMessage: PropTypes.shape({}),
  errorMessages: PropTypes.string,
  updateAppTypeHandler: PropTypes.func.isRequired,
  currencyAttributes: PropTypes.shape({}),
  trackPageLoad: PropTypes.func,
  cookieToken: PropTypes.string,
  suggestedProductErrors: PropTypes.shape({}).isRequired,
  hasSuggestedProduct: PropTypes.bool.isRequired,
  isShowPriceRangeKillSwitch: PropTypes.bool.isRequired,
  showPriceRangeForABTest: PropTypes.bool.isRequired,
  setSuggestedColorProductId: PropTypes.func.isRequired,
  favOOSRemoveLabel: PropTypes.shape({}).isRequired,
  isNewReDesignProductTile: PropTypes.bool,
  isSNJEnabled: PropTypes.bool,
};

FavoritesContainer.defaultProps = {
  wishlistsSummaries: [],
  activeWishList: {},
  currencySymbol: '$',
  labels: {},
  slpLabels: {},
  totalProductsCount: '0',
  isDataLoading: false,
  labelsPlpTiles: {},
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  defaultWishList: {},
  wishlistShareStatus: false,
  isLoggedIn: false,
  errorMessages: '',
  formErrorMessage: {},
  currencyAttributes: {},
  trackPageLoad: () => {},
  cookieToken: null,
  isNewReDesignProductTile: false,
  isSNJEnabled: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesContainer);

export { FavoritesContainer as FavoritesContainerVanilla };
