// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';
import { FAVORITES_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import { TCP, SNJ, GYM } from '../../../../../constants/brands.constants';

const getFavoriteListState = (state) => {
  return state[FAVORITES_REDUCER_KEY];
};

export const selectWishlistsSummaries = createSelector(
  getFavoriteListState,
  (items) => items && items.get('wishlistsSummaries')
);

export const selectActiveWishList = createSelector(
  getFavoriteListState,
  (items) => items && items.get('activeWishList')
);

export const selectWishListShareStatus = (state) => {
  return state && state.Favorites && state.Favorites.get('isWishListShared');
};

export const wishListFromState = (state) => {
  return state && state.Favorites && state.Favorites.get('defaultWishList');
};

export const selectActiveWishlistProducts = (state) => {
  const activeWishList = selectActiveWishList(state);
  return activeWishList && activeWishList.items;
};

export const selectTotalProductsCount = (state) => {
  const activeWishList = selectActiveWishList(state);
  return activeWishList && activeWishList.items.length;
};

export const selectActiveDisplayName = createSelector(getFavoriteListState, (items) => {
  const activeWishList = items && items.get('activeWishList');
  return activeWishList && activeWishList.displayName;
});

export const selectActiveWishlistId = (state) => {
  const activeWishList = selectActiveWishList(state);
  return activeWishList && activeWishList.id;
};

export const selectDefaultWishlist = (state) => {
  const wishlistsSummaries = selectWishlistsSummaries(state);
  return (
    wishlistsSummaries &&
    wishlistsSummaries.length > 0 &&
    wishlistsSummaries.filter((wishlist) => {
      return wishlist.isDefault === true;
    })
  );
};

export const getAllProductsAvailability = (state) => {
  const activeWishList = selectActiveWishList(state);
  let isTcpProductAvailable = false;
  let isGymProductAvailable = false;
  let isSnjProductAvailable = false;
  let isTcpAndGymAndSnjProductsAvailable = false;
  const len = (activeWishList && activeWishList.items.length) || 0;
  for (let i = 0; i < len; i += 1) {
    const item = activeWishList.items[i];
    if (item.itemInfo.brand === TCP) {
      isTcpProductAvailable = true;
    } else if (item.itemInfo.brand === GYM) {
      isGymProductAvailable = true;
    } else if (item.itemInfo.brand === SNJ) {
      isSnjProductAvailable = true;
    }
  }
  isTcpAndGymAndSnjProductsAvailable =
    isTcpProductAvailable && isGymProductAvailable && isSnjProductAvailable;
  return isTcpAndGymAndSnjProductsAvailable;
};

export const getAllBrandsAvailability = (state) => {
  const activeWishList = selectActiveWishList(state);
  const availableBrands = ['', '', ''];
  const len = (activeWishList && activeWishList.items.length) || 0;
  for (let i = 0; i < len; i += 1) {
    const item = activeWishList.items[i];
    if (item.itemInfo.brand === TCP) {
      availableBrands[0] = TCP;
    } else if (item.itemInfo.brand === GYM) {
      availableBrands[1] = GYM;
    } else if (item.itemInfo.brand === SNJ) {
      availableBrands[2] = SNJ;
    }
  }
  return availableBrands;
};

export const fetchCurrencySymbol = (state) => {
  const currency = state.session && state.session.siteDetails.currency;
  if (currency) {
    return currency === 'USD' || currency === 'CA' ? '$' : currency;
  }
  return '$';
};

export const fetchAddToFavoriteErrorMsg = (state) => {
  return state.Favorites && state.Favorites.get('isAddToFavError');
};

export const getLabelsFavorites = (state) =>
  state.Labels.FavoritesLabels && state.Labels.FavoritesLabels.FavoritesLabels_Sub;

export const getSLPLabels = (state) => state.Labels.SLPs && state.Labels.SLPs.SLPs_Sub;

export default fetchCurrencySymbol;

export const getIsDataLoading = (state) => {
  return state[FAVORITES_REDUCER_KEY].get('isDataLoading');
};

export const getFormErrorLabels = (state) =>
  state.Labels.global && state.Labels.global.formValidation;

export const fetchErrorMessages = (state) => {
  return state.Favorites && state.Favorites.get('errorMessages');
};

export const getAccessibilityLabels = (state) => {
  return {
    upArrowAltText: getLabelValue(state.Labels, 'lbl_up_arrow_icon', 'accessibility', 'global'),
    downArrowAltText: getLabelValue(state.Labels, 'lbl_down_arrow_icon', 'accessibility', 'global'),
  };
};

export const getOOSItemIdWhoseSuggestionRequested = (state) => {
  return state.Favorites && state.Favorites.get('seeSuggestedItemForOOSItemId');
};

export const selectDefaultWishlistId = (state) => {
  return state && state.Favorites && state.Favorites.get('defaultWishListId');
};

export const getFavOOSRemoveLabel = (state) => {
  return {
    favOOSMessage: getLabelValue(
      state.Labels,
      'lbl_fav_oos_message',
      'FavoritesLabels_Sub',
      'FavoritesLabels'
    ),
    favOOSRemove: getLabelValue(
      state.Labels,
      'lbl_fav_oos_remove',
      'FavoritesLabels_Sub',
      'FavoritesLabels'
    ),
  };
};

export const getDefaultFavItemInProgressFlag = (state) => {
  return state && state.Favorites && state.Favorites.get('deleteFavItemInProgress');
};
