/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLatest, select, delay } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import processHelperUtil from '@tcp/core/src/services/abstractors/productListing/ProductDetail.util';
import { FAVORITES_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import getErrorList from '@tcp/core/src/components/features/CnC/BagPage/container/Errors.selector';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import { formatProductsData, getPageName } from '@tcp/core/src/utils/utils';
import { getFavoriteStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { trackForterAction, ForterActionType } from '@tcp/core/src/utils/forter.util';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import {
  setDefaultWishListIdActn,
  setDeleteFavItemProgress,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import FAVORITES_CONSTANTS from './Favorites.constants';
import { isCanada, isMobileApp } from '../../../../../utils';
import names from '../../../../../constants/eventsName.constants';

import {
  setWishlistState,
  setWishlistsSummariesAction,
  getSetIsWishlistReadOnlyAction,
  setActiveWishlistAction,
  getActiveWishlistAction,
  setDeletedItemAction,
  setLoadingState,
  setAddToFavoriteErrorState,
  setWishListShareSuccess,
  setMaximumProductAddedErrorState,
  resetMaximumProductAddedErrorState,
  getSetDefaultWishListActn,
} from './Favorites.actions';
import {
  wishListFromState as defaultWishListFromState,
  selectDefaultWishlistId,
} from './Favorites.selectors';
import addItemsToWishlistAbstractor, {
  getUserWishLists,
  getWishListbyId,
  getProductsPrices,
  createWishList,
  moveItemToNewWishList,
  deleteWishList,
  updateWishlistName,
  deleteWishListItem,
  shareWishlistByEmail,
} from '../../../../../services/abstractors/productListing/favorites';
import {
  getUserLoggedInState,
  getUserContactInfo,
  isRememberedUser,
} from '../../../account/User/container/User.selectors';
import { setLoginModalMountedState } from '../../../account/LoginPage/container/LoginPage.actions';
import { setAddToFavorite } from '../../ProductListing/container/ProductListing.actions';
import { setAddToFavoritePDP } from '../../ProductDetail/container/ProductDetail.actions';
import { setAddToFavoriteSLP } from '../../SearchDetail/container/SearchDetail.actions';
import {
  setAddToFavoriteOUTFIT,
  removeOutfitFavoriteItemAction,
} from '../../OutfitDetails/container/OutfitDetails.actions';
import { setAddToFavoriteBUNDLE } from '../../BundleProduct/container/BundleProduct.actions';
import getProductsUserCustomInfo from '../../../../../services/abstractors/productListing/defaultWishlist';
import { setLoaderState } from '../../../../common/molecules/Loader/container/Loader.actions';

export function* loadActiveWishlistByGuestKey({ payload }) {
  const { wishListId, guestAccessKey } = payload;
  try {
    const state = yield select();
    yield put(setLoadingState({ isDataLoading: true }));
    const userState = getUserContactInfo(state);
    const userName = userState && userState.get('firstName');
    const isCanadaCheck = isCanada();
    const wishlistItems = yield call(getWishListbyId, {
      wishListId,
      userName,
      guestAccessKey,
      isCanada: isCanadaCheck,
      imageGenerator: processHelperUtil.getImgPath,
    });
    yield put(getSetIsWishlistReadOnlyAction(true));
    yield put(setActiveWishlistAction(wishlistItems));
    yield put(setLoadingState({ isDataLoading: false }));
    return wishlistItems;
  } catch (err) {
    yield put(setLoadingState({ isDataLoading: false }));
    return [];
  }
}

export function* getDefaultWishList(makeApiCall = false) {
  try {
    const state = yield select();
    const isGuest = !getUserLoggedInState({ ...state });
    const isRemembered = isRememberedUser({ ...state });
    const defaultWishlistId = yield select(selectDefaultWishlistId);
    if (!isGuest && !isRemembered) {
      let defaultWishListItems = yield select(defaultWishListFromState);
      if ((defaultWishListItems === null || makeApiCall) && defaultWishlistId) {
        defaultWishListItems = yield call(getProductsUserCustomInfo, false, defaultWishlistId);
        yield put(getSetDefaultWishListActn({ ...defaultWishListItems }));
      }
    }
  } catch (err) {
    yield null;
  }
}

const getBreadCrumb = breadCrumbs => {
  if (breadCrumbs) {
    const breadCrumbsArray = breadCrumbs.map(breadCrumb => breadCrumb.displayName.toLowerCase());
    return breadCrumbsArray.join(':');
  }

  return null;
};

/* eslint-disable sonarjs/cognitive-complexity */
// eslint-disable-next-line complexity
export function* addItemsToWishlist({ payload }) {
  const {
    colorProductId,
    activeWishListId,
    productSkuId,
    pdpColorProductId,
    formName,
    page,
    products,
    customData,
    breadCrumbs,
  } = payload;
  const state = yield select();
  const storeId = getFavoriteStore(state);
  const isGuest = !getUserLoggedInState(state);
  const errorMapping = getErrorList(state);
  const productsFormatted = products ? formatProductsData(products, customData) : null;
  const pageShortName = products ? getPageName(products) : null;
  let skuIdOrProductId;
  let isProduct = true;
  const quantity =
    formName && state.form[`${formName}-${colorProductId}`]
      ? state.form[`${formName}-${colorProductId}`].values.Quantity
      : 1;

  if (productSkuId) {
    skuIdOrProductId = productSkuId;
    isProduct = false;
  } else if (pdpColorProductId) {
    skuIdOrProductId = pdpColorProductId;
  } else {
    skuIdOrProductId = colorProductId;
  }

  try {
    yield put(setAddToFavoriteErrorState({ errorMessage: '' }));
    if (isGuest) {
      yield put(setLoginModalMountedState({ state: true, isAccountCardVisible: true }));
    } else {
      yield put(resetMaximumProductAddedErrorState({}));
      yield put(setDeleteFavItemProgress(true));
      const res = yield call(addItemsToWishlistAbstractor, {
        wishListId: activeWishListId || '',
        skuIdOrProductId,
        quantity,
        isProduct,
        uniqueId: colorProductId,
        errorMapping,
      });

      if (res && res.defaultWishListId) {
        yield put(setDefaultWishListIdActn(res.defaultWishListId));
      }
      yield put(
        setClickAnalyticsData({
          customEvents: [names.eVarPropEvent.event52, names.eVarPropEvent.event94],
          pageName: pageShortName,
          products: productsFormatted,
          pageNavigationText: '',
          eventName: 'wishlist add item',
          linkName: 'wishlist add item',
          storeId,
          clickEvent: true,
          breadcrumb: getBreadCrumb(breadCrumbs),
        })
      );
      if (isMobileApp()) {
        trackForterAction(ForterActionType.TAP, 'ADD_TO_FAVORITES');
      }
      yield put(trackClick({ name: 'add_to_wishlist', module: 'browse' }));

      yield call(getDefaultWishList, true);

      if (res && res.errorMessage) {
        yield put(setAddToFavoriteErrorState(res));
        yield put(setDeleteFavItemProgress(false));
        const errorMessages = {
          errorMessage: res.errorMessage,
          itemId: colorProductId,
        };
        const data = {};
        data[colorProductId] = errorMessages;
        yield put(setMaximumProductAddedErrorState({ ...data }));
      }

      if (res && res.newItemId) {
        switch (page) {
          case 'PDP':
            yield put(setAddToFavoritePDP({ pdpColorProductId, res }));
            break;
          case 'PLP':
            yield put(setAddToFavorite({ colorProductId, res }));
            break;
          case 'SLP':
            yield put(setAddToFavoriteSLP({ colorProductId, res }));
            break;
          case 'OUTFIT':
            yield put(setAddToFavoriteOUTFIT({ pdpColorProductId, res }));
            break;
          case 'BUNDLE':
            yield put(setAddToFavoriteBUNDLE({ pdpColorProductId, res }));
            break;
          default:
            break;
        }
        // to update favorite page need to load wish list sumamry
        const activeWishlistObject =
          state[FAVORITES_REDUCER_KEY] && state[FAVORITES_REDUCER_KEY].get('activeWishList');
        const activeWishlistId = activeWishlistObject && activeWishlistObject.id;
        yield put(setWishlistState({ colorProductId, isInDefaultWishlist: true }));
        yield put(setDeleteFavItemProgress(false));
        // Disabling eslint for temporary fix
        // eslint-disable-next-line no-use-before-define
        yield* loadWishlistsSummaries(activeWishlistId);
      }
    }
  } catch (err) {
    yield put(setDeleteFavItemProgress(false));
    logger.error(err);
  }
}

export function* loadActiveWishlist({ wishListId, isSBP }) {
  try {
    const state = yield select();
    const xappURLConfig = yield call(getUnbxdXappConfigs);
    if (isMobileApp()) {
      yield put(setLoaderState(!isSBP));
    }
    const userName = getUserContactInfo(state).get('firstName');
    const wishlistById = yield call(getWishListbyId, {
      wishListId,
      userName,
      guestAccessKey: null,
      isCanada: isCanada(),
      imageGenerator: processHelperUtil.getImgPath,
    });
    const wishlistItems = wishlistById.items;
    const prices = yield call(
      getProductsPrices,
      wishlistItems.map(wishlist => wishlist.skuInfo.colorProductId),
      xappURLConfig
    );
    const WishlistWithUnbxdPrice = wishlistItems.map(wishlist => {
      return {
        ...wishlist,
        productInfo: {
          ...wishlist.productInfo,
          ...prices[wishlist.skuInfo.colorProductId],
        },
      };
    });
    const updatedWishList = { ...wishlistById, items: [...WishlistWithUnbxdPrice] };
    yield put(setActiveWishlistAction(updatedWishList));
    if (isMobileApp()) {
      yield put(setLoaderState(false));
    }
    yield put(setLoadingState({ isDataLoading: false }));
  } catch (err) {
    if (isMobileApp()) {
      yield put(setLoaderState(false));
    }
    yield null;
  }
}

export function* loadWishlistsSummaries(config) {
  try {
    const state = yield select();
    let wishListId;
    if (typeof config !== 'object') {
      wishListId = config;
    } else if (typeof config === 'object') {
      const { isDataLoading } = config.payload;
      yield put(setLoadingState({ isDataLoading: isDataLoading || false }));
    }
    const userName = getUserContactInfo(state).get('firstName');
    const wishlists = yield call(getUserWishLists, userName);
    yield put(setWishlistsSummariesAction(wishlists));
    const activeWishListId = wishListId || wishlists.find(list => list.isDefault).id;
    const deafultWishListId = wishlists && wishlists.find(list => list.isDefault).id;
    if (deafultWishListId) {
      yield put(setDefaultWishListIdActn(deafultWishListId));
    }
    yield put(getActiveWishlistAction(activeWishListId));
  } catch (err) {
    yield put(setLoadingState({ isDataLoading: false }));
    yield null;
  }
}

export function* createNewWishList({ payload: formData }) {
  try {
    const storeId = yield select(getFavoriteStore);
    const createdWishListResponse = yield call(
      createWishList,
      formData.wishListName,
      formData.isDefault
    );
    yield put(
      setClickAnalyticsData({
        customEvents: ['event64'],
        products: [],
        pageNavigationText: '',
        pageName: 'favorites',
        storeId,
        clickEvent: true,
      })
    );

    yield put(
      trackClick({
        name: isMobileApp() ? names.screenNames.create_wishlist : 'create_wishlist',
        module: 'browse',
      })
    );
    yield* loadWishlistsSummaries(createdWishListResponse.id);
    if (formData.isDefault) {
      yield call(getDefaultWishList, true);
    }
  } catch (err) {
    yield null;
  }
}

export function* createNewWishListMoveItem({ payload: formData }) {
  try {
    yield put(resetMaximumProductAddedErrorState({}));
    const state = yield select();
    const errorMapping = getErrorList(state);
    let createdWishListResponse;
    if (!formData.wisListId) {
      createdWishListResponse = yield call(
        createWishList,
        formData.wishListName,
        formData.isDefault
      );
    }
    const payload = {
      toWishListId: formData.wisListId || createdWishListResponse.id,
      itemId: formData.itemId,
    };
    const activeWishlistObject =
      state[FAVORITES_REDUCER_KEY] && state[FAVORITES_REDUCER_KEY].get('activeWishList');
    const activeWishlistId = activeWishlistObject.id;
    const activeWishlistItem = activeWishlistObject.items.find(
      item => item.itemInfo.itemId === formData.itemId
    );

    const itemMovedResponse = yield call(
      moveItemToNewWishList,
      payload,
      activeWishlistId,
      activeWishlistItem,
      errorMapping
    );
    if (itemMovedResponse && itemMovedResponse.errorMessage) {
      const errorMessages = {
        errorMessage: itemMovedResponse.errorMessage,
        itemId: formData.itemId,
      };
      const data = {};
      data[formData.itemId] = errorMessages;
      yield put(setMaximumProductAddedErrorState({ ...data }));
    } else {
      yield* loadWishlistsSummaries(activeWishlistId);
    }
    if (!itemMovedResponse.success) {
      throw itemMovedResponse;
    }
  } catch (err) {
    yield null;
  }
}

export function* deleteWishListById({ payload }) {
  const storeId = yield select(getFavoriteStore);
  try {
    const { wishListId, isDefault, products, deleteOOSItems } = payload;
    const deleteWishListResponse = yield call(deleteWishList, wishListId, deleteOOSItems);
    yield put(
      setClickAnalyticsData({
        customEvents: ['event54'],
        products,
        pageNavigationText: '',
        pageName: 'favorites',
        storeId,
        clickEvent: true,
      })
    );
    yield put(
      trackClick({
        name: isMobileApp() ? names.screenNames.delete_wishlist : 'delete_wishlist',
        module: 'browse',
      })
    );
    if (!deleteWishListResponse.success) {
      throw deleteWishListResponse;
    }
    yield* loadWishlistsSummaries();
    if (isDefault) {
      yield call(getDefaultWishList, true);
    }
  } catch (err) {
    yield null;
  }
}

export function* updateExistingWishList({ payload: formData }) {
  try {
    const updateWishListResponse = yield call(
      updateWishlistName,
      formData.wishlistId,
      formData.wishlistName,
      formData.setAsDefault
    );
    if (!updateWishListResponse.success) {
      throw updateWishListResponse;
    }
    yield* loadWishlistsSummaries(formData.wishlistId);
    if (formData.setAsDefault) {
      yield call(getDefaultWishList, true);
    }
  } catch (err) {
    yield null;
  }
}

// eslint-disable-next-line complexity
export function* deleteWishListItemById({ payload }, isByPallLoadSummary = false) {
  const storeId = yield select(getFavoriteStore);
  let deleteItemResponse = '';
  try {
    if (isMobileApp() && payload.isSBP) {
      yield put(setLoaderState(true));
    }
    const { products, toastMessage } = payload;
    const state = yield select();
    const activeWishlistObject =
      state[FAVORITES_REDUCER_KEY] && state[FAVORITES_REDUCER_KEY].get('activeWishList');
    const activeWishlistId = activeWishlistObject && activeWishlistObject.id;
    if (payload && payload.notFromFavoritePage) {
      yield put(setDeleteFavItemProgress(true));
      deleteItemResponse = yield call(deleteWishListItem, payload.externalId, payload.itemId);
    } else {
      deleteItemResponse = yield call(deleteWishListItem, activeWishlistId, payload.itemId);
    }
    const productsFormatted = products ? formatProductsData(products) : null;
    if (!deleteItemResponse.success) {
      if (isMobileApp() && payload.isSBP) {
        yield put(setLoaderState(false));
      }
      throw deleteItemResponse;
    }
    yield put(
      setClickAnalyticsData({
        customEvents: ['event53'],
        pageName: 'favorites',
        products: productsFormatted,
        pageNavigationText: '',
        storeId,
        clickEvent: true,
      })
    );
    yield put(
      trackClick({
        name: isMobileApp() ? names.screenNames.remove_from_wishlist : 'remove_from_wishlist',
        module: 'browse',
      })
    );

    yield call(getDefaultWishList, true);
    yield put(setDeletedItemAction(payload.itemId));
    yield put(removeOutfitFavoriteItemAction(payload));
    if (!isByPallLoadSummary) {
      yield* loadWishlistsSummaries(activeWishlistId);
    }
    yield put(setDeleteFavItemProgress(false));
    yield delay(4000);
    yield put(toastMessageInfo(toastMessage));
    yield put(setDeleteFavItemProgress(false));
  } catch (err) {
    if (isMobileApp() && payload.isSBP) {
      yield put(setLoaderState(false));
    }
    yield put(setDeleteFavItemProgress(false));
    yield null;
  }
}

export function* updateWishListItem({ payload }) {
  try {
    const { itemId, quantity, color, product, callBack } = payload;
    const state = yield select();
    const activeWishlistObject =
      state[FAVORITES_REDUCER_KEY] && state[FAVORITES_REDUCER_KEY].get('activeWishList');
    const activeWishlistId = activeWishlistObject.id;
    const variantSelected =
      product &&
      product.colorFitsSizesMap &&
      product.colorFitsSizesMap.find(entry => entry.color.name === color);
    const uniqueId = (variantSelected && variantSelected.colorDisplayId) || '';
    yield call(deleteWishListItem, activeWishlistId, itemId);
    yield call(addItemsToWishlistAbstractor, {
      wishListId: activeWishlistId,
      skuIdOrProductId: uniqueId,
      quantity,
      isProduct: true,
      uniqueId,
    });
    yield* loadWishlistsSummaries(activeWishlistId);
    if (callBack) {
      callBack();
    }
  } catch (err) {
    yield null;
  }
}

export function* sendWishListMail(formData) {
  try {
    const state = yield select();
    const activeWishlistObject =
      state[FAVORITES_REDUCER_KEY] && state[FAVORITES_REDUCER_KEY].get('activeWishList');
    const activeWishlistId = activeWishlistObject.id;
    const {
      shareToEmailAddresses,
      shareFromEmailAddresses,
      shareSubject,
      shareMessage,
    } = formData.payload;
    const emailSentResponse = yield call(
      shareWishlistByEmail,
      activeWishlistId,
      shareFromEmailAddresses,
      shareToEmailAddresses.split(','),
      shareSubject,
      shareMessage
    );

    if (emailSentResponse.successful) {
      yield put(setWishListShareSuccess(true));
    }
  } catch (err) {
    yield null;
  }
}

export function* replaceWishlistItem(payload) {
  try {
    yield* deleteWishListItemById(payload, true);
    yield* addItemsToWishlist(payload);
    const {
      payload: { activeWishListId },
    } = payload;
    yield* loadWishlistsSummaries(activeWishListId);
  } catch (err) {
    yield null;
  }
}

export function* createNewWishListAddItem({ payload }) {
  const payloadObj = payload;
  try {
    if (isMobileApp()) {
      yield put(setLoaderState(true));
    }
    yield put(resetMaximumProductAddedErrorState({}));
    /*
      On each click of favorites icon, a new wish list was getting created
      But we need single wishlist for a profile. This check will detect if the
      there is already a wish list active for this profile, then it would reuse
      that instead of creating a new one.
    */
    if (!payloadObj.activeWishListId) {
      const createdWishListResponse = yield call(
        createWishList,
        payload.wishListName,
        payload.isDefault
      );
      payloadObj.activeWishListId = createdWishListResponse.id;
    }
    yield* addItemsToWishlist({ payload: payloadObj });
    yield* loadWishlistsSummaries(payloadObj.activeWishListId);
  } catch (e) {
    if (isMobileApp()) {
      yield put(setLoaderState(false));
    }
    yield null;
  }
}

function* FavoriteSaga() {
  yield takeLatest(FAVORITES_CONSTANTS.SET_FAVORITES, addItemsToWishlist);
  yield takeLatest(FAVORITES_CONSTANTS.GET_USER_FAVORITES, getDefaultWishList);
  yield takeLatest(FAVORITES_CONSTANTS.GET_FAVORITES_WISHLIST, loadWishlistsSummaries);
  yield takeLatest(FAVORITES_CONSTANTS.LOAD_ACTIVE_FAVORITES_WISHLIST, loadActiveWishlist);
  yield takeLatest(
    FAVORITES_CONSTANTS.LOAD_ACTIVE_FAVORITES_WISHLIST_GUEST,
    loadActiveWishlistByGuestKey
  );
  yield takeLatest(FAVORITES_CONSTANTS.CREATE_NEW_WISHLIST, createNewWishList);
  yield takeLatest(FAVORITES_CONSTANTS.CREATE_NEW_WISHLIST_MOVE_ITEM, createNewWishListMoveItem);
  yield takeLatest(FAVORITES_CONSTANTS.DELETE_WISHLIST, deleteWishListById);
  yield takeLatest(FAVORITES_CONSTANTS.UPDATE_WISHLIST, updateExistingWishList);
  yield takeLatest(FAVORITES_CONSTANTS.DELETE_WISHLIST_ITEM, deleteWishListItemById);
  yield takeLatest(FAVORITES_CONSTANTS.UPDATE_WISHLIST_ITEM, updateWishListItem);
  yield takeLatest(FAVORITES_CONSTANTS.SEND_WISHLIST_EMAIL, sendWishListMail);
  yield takeLatest(FAVORITES_CONSTANTS.FAVORITES_REPLACE_WISHLIST_ITEM, replaceWishlistItem);
  yield takeLatest(FAVORITES_CONSTANTS.CREATE_NEW_WISHLIST_ADD_ITEM, createNewWishListAddItem);
}

export default FavoriteSaga;
