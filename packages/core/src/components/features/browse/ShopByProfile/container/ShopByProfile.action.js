// 9fbef606107a605d69c0edbcd8029e5d 
import SHOP_BY_PROFILE_CONSTANTS from './ShopByProfile.constants';

const setAddToFavoriteSBP = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.SET_ADD_TO_FAVORITE,
    payload,
  };
};

export default setAddToFavoriteSBP;
