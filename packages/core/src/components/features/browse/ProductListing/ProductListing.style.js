/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .seo-header-text-block {
    text-align: center;
    font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs26};
    }
  }
  .fixed-bread-crumb-height {
    min-height: 68px;
  }
  .breadcrum-item {
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
  }
  .breadcrum-item.showUnderline {
    text-decoration: underline;
  }
  .breadcrum-last-item {
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
    pointer-events: none;
    display: inline;
  }
  .breadcrum-separation {
    margin: 0 ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    vertical-align: middle;
  }
  .bread-crumb,
  .filter-area {
    padding-top: 20px;
    padding-bottom: 17px;
    margin: 5px 0 9px;
  }
  .bread-crumb,
  .product-list {
    display: flex;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .show-count-web {
      display: none;
    }
  }

  .sidebar {
    display: none;
  }
  .seo-text {
    text-align: center;
    padding-bottom: 20px;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .sidebar {
      display: flex;
    }
  }

  .render-desktop-view {
    display: none;

    @media ${(props) => props.theme.mediaQuery.large} {
      display: block;
    }
  }
  .show-count-section {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};

    @media ${(props) => props.theme.mediaQuery.large} {
      display: none;
    }
  }
  .new-count-section {
    align-items: center;
  }
  .render-mobile-view {
    position: relative;
    width: auto;
    padding: 0;

    @media ${(props) => props.theme.mediaQuery.large} {
      display: none;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      .filter-row {
        width: auto;
        padding-top: 0;
      }
    }
    .filter-row {
      margin: 0;
      padding-bottom: 0;
    }
  }
  .sticky-filter-enabled .filtered-by-section {
    display: none;
  }
  .desktop-filter-sidebar {
    position: sticky;
    top: 47px;
    height: 500px;
    overflow-y: auto;
    overflow-x: hidden;

    .sticky-filter-clear-all-variant {
      text-transform: capitalize;
      text-decoration: underline;
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      color: ${(props) => props.theme.colorPalette.gray[900]};
      padding-left: 30px;
    }
    .sticky-filter-clear-all-variant:focus {
      background: none;
    }
    .open-filter-button.close-filter-button {
      width: 50%;
    }
    .render-mobile-view {
      display: block;
      .filter-row > div {
        width: 100%;
      }
      .open-filter-button {
        background: none;
        background-color: ${(props) => props.theme.colorPalette.white};
        border: none;
        text-align: left;
      }
    }
    .filter-and-sort-form-container {
      width: 100%;
    }
    .render-desktop-view {
      .desktop-dropdown,
      .filtered-by-section .filtering-title {
        display: none;
      }
    }
    .applied-filters-sorting-container .applied-filter-list {
      width: 100%;
    }

    .applied-filters-sorting-container {
      .applied-filter-item {
        margin-right: 0;
      }
    }
    .items-count-content {
      .show-label {
        display: none;
      }
    }
    .hide-sort-sidebar > .open-filter-button {
      display: none;
    }
  }

  .filter-section {
    .render-mobile-view {
      padding-top: 0;
    }

    .available-filters-sorting-container {
      top: 44px;
    }
  }

  .count-section {
    text-align: right;
    position: relative;
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
  }

  .items-count-content-number {
    font-weight: ${(props) => props.theme.typography.fontWeights.black};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
  .recommendations-section-row {
    .product-list {
      display: list-item;
    }
  }
  .recommendation-wrapper {
    margin: 0 -14px;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 0;
    }
  }

  .banner-plp-recommendation-wrapper {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-left: -13px;
      margin-right: -13px;
    }
    min-height: 385px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      min-height: 345px;
    }
  }

  .hideBannerRecos {
    display: none;
  }
`;

export const recommendationStyles = css`
  .recommendations-header {
    overflow: hidden;
    text-align: center;
    font-family: Nunito;
    font-size: 20px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #1a1a1a;
    margin: 0 15px 0 14px;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-bottom: 10px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: 10px;
    }
  }

  && .product-list {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 95%;
      margin-top: 10px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: 90%;
      margin-top: 10px;
    }
  }

  .smooth-scroll-list {
    width: 100%;
  }

  .smooth-scroll-list-item {
    width: 40%;
    margin-left: 10px;
  }

  .recommendations-header:before,
  .recommendations-header:after {
    background-color: #000;
    content: '';
    display: inline-block;
    height: 1px;
    position: relative;
    vertical-align: middle;
    width: 50%;
  }

  .recommendations-header:before {
    right: 0.5em;
    margin-left: -50%;
  }

  .recommendations-header:after {
    left: 0.5em;
    margin-right: -50%;
  }

  .recommendations-section-row {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-bottom: 50px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: 50px;
    }
  }

  .recommendations-section-col {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 100%;
    }
  }

  .added-to-bag {
    border: solid 1px #9b9b9b;
    font-size: 11px;
  }

  && .slick-prev {
    margin-top: 5%;
  }

  && .slick-next {
    margin-top: 5%;
  }

  && .ranking-wrapper {
    width: 100%;
    margin-left: 10px;
  }

  && .item-container-inner {
    max-height: 320px;
    align-items: center;
    border-radius: 11px;
    box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.15);
  }

  && .product-image-container {
    margin: 0 0 10px;
    width: 85%;
    padding: 10px;
    @media ${(props) => props.theme.mediaQuery.large} {
      height: 165px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: 165px;
    }
  }

  && .container-price {
    margin-left: 5px;
    height: auto;
    p {
      font-family: Nunito;
      font-size: 14px;
      font-weight: 800;
      font-stretch: normal;
      font-style: normal;
      line-height: 1;
      letter-spacing: normal;
      color: #c8102e;
    }

    .list-price {
      font-family: Nunito;
      font-size: 12px;
      font-weight: 600;
      font-stretch: normal;
      font-style: normal;
      line-height: 1;
      letter-spacing: normal;
      color: #575757;
    }

    .merchant-tag {
      font-family: Nunito;
      font-size: 12px;
      font-weight: 600;
      font-stretch: normal;
      font-style: normal;
      line-height: 1;
      letter-spacing: normal;
      color: #c8102e;
    }
  }

  && .fulfillment-section {
    width: 95%;
  }

  && .slick-slide {
    margin: 10px;
  }
`;

export const bannerRecommendationStyles = css`
  .recommendations-header {
    display: none;
  }

  && .product-list {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 95%;
      margin-top: 10px;
      margin-left: 0;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: 96%;
      margin-top: 10px;
      margin-left: 0;
    }
  }

  .smooth-scroll-list {
    width: 100%;
    height: 310px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: 350px;
    }
  }

  .smooth-scroll-list-item {
    width: 41%;
    margin-left: 12px;
    margin-right: 0px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 23.2%;
      min-height: 290px;
    }
    &:first-child {
      margin-left: 0;
    }
    &:last-child {
      margin-right: 12px;
    }
  }

  .recommendations-section-col,
  .recommendations-section-col .hSDTpT {
    width: 100%;
    margin: 0;
  }

  .recommendations-tile {
    max-height: 390px;
  }

  .recommendations-container {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: -13px;
      margin-right: -13px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-left: -8px;
      margin-right: -29px;
    }
    margin-top: 10px;
    margin-bottom: 35px;
  }

  && .extended-sizes-text {
    display: none;
  }

  && .slick-prev {
    margin-top: 5%;
    margin-left: 4%;
  }

  && .slick-next {
    margin-top: 5%;
    margin-right: 4.3%;
  }

  && .ranking-wrapper {
    width: 100%;
    margin-left: 10px;
    margin-top: auto;
    min-height: 10px;
  }
  .custom-ranking-container {
    img {
      height: 14px;
      width: 82px;
    }
    .reviews-count {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      @media ${(props) => props.theme.mediaQuery.large} {
        margin-top: 1px;
      }
    }
  }

  && .item-container-inner {
    max-height: 360px;
    align-items: center;
    border-radius: 11px;
    box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.15);
    padding: 0 5px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 290px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: 330px;
    }
  }

  && .product-image-container {
    width: 100%;
    margin: 10px auto 5px;
    height: 180px;
    min-height: 180px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 161px !important;
      min-height: 161px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      height: 206px;
      min-height: 206px;
    }
  }

  && .container-price {
    margin-left: 5px;
    height: 40px;
    span {
      position: relative;
      top: -3px;
    }
  }

  && .fulfillment-section {
    width: 95%;
    position: relative;
    bottom: 6px;
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 92%;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      position: relative;
      bottom: 0px;
    }
  }

  && .slick-slide {
    margin: 12px;
    min-height: 360px;
    @media ${(props) => props.theme.mediaQuery.large} {
      display: grid;
    }
  }
  && .bannerStyles {
    padding: 0px;
    position: relative;
    text-align: center;
    line-height: 1.07;
    letter-spacing: 0.68px;
    height: 98%;
    box-sizing: border-box;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 14px 0;
      height: 94%;
    }
  }

  && .recommendation-banner {
    width: 18%;
    margin: 0;
    max-height: 390px;
  }

  && .recommendation-listview {
    margin: 0 auto;
    padding-left: 2%;
    width: 74.44%;
  }

  .recommendations-section-row {
    overflow: unset;
  }
  .focus-text {
    font-weight: bold;
    letter-spacing: normal;
    margin: 5px 0;
    line-height: 0.9;
    display: block;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      line-height: 0.92;
      letter-spacing: normal;
    }

    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      line-height: 1;
      letter-spacing: normal;
    }
  }

  .fulfillment-section .added-to-bag {
    min-height: 42px;
    padding: 2px;
    font-size: 14px;
    @media ${(props) => props.theme.mediaQuery.large} {
      min-height: 50px;
    }
  }

  .recommendations-message {
    font-weight: 500;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    padding: 0;
    width: 85%;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      line-height: 1.08;
      letter-spacing: 0.54px;
    }
  }
`;
