/* eslint-disable max-lines */
/* eslint-disable no-unused-vars */
// 9fbef606107a605d69c0edbcd8029e5d
/*
  @Important
  Container for Essentials Shops containing
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getFormValues, destroy, reset } from 'redux-form';
import { isMobileApp } from '@tcp/core/src/utils';
import { trackClick, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import { processBreadCrumbs } from '../../../container/ProductListing.util';
import EssentialShop from '../views';
import { getActiveStep, essentialShoppingLabels } from './EssentialShop.selectors';
import {
  setEssentialShopStep,
  getPlpProducts,
  setFilter,
} from '../../../container/ProductListing.actions';
import {
  getProductsFilters,
  getAppliedFilters,
  getIsLoadingMore,
  getIsESModuleLoading,
  getAppliedSortId,
  getLoadedProductsPages,
} from '../../../container/ProductListing.selectors';
import submitProductListingFiltersForm from '../../../container/productListingOnSubmitHandler';

import { getParsedGuidedSteps } from './EssentialShop.utils';

const FILTER_FORM = 'filter-form';
export class EssentialShopContainer extends React.PureComponent {
  onSubmit = (...params) => {
    const { onSubmit, setSelectedFilter } = this.props;
    if (isMobileApp()) {
      setSelectedFilter(params[0]);
    }
    onSubmit(...params);
  };

  render() {
    const { data, filtersMaps } = this.props;
    const { onSubmit, ...otherProps } = this.props;
    if (data && filtersMaps && Object.keys(filtersMaps).length) {
      const steps = getParsedGuidedSteps(data);
      const keys = steps.reduce((res, val) => {
        if (val.facetName) {
          res.push(val.facetName);
        }
        return res;
      }, []);
      let count = 0;
      keys.forEach(key => {
        count += (filtersMaps[key] || []).length;
      });
      return count ? (
        <EssentialShop
          essentialsCMSData={data}
          guidedSteps={steps}
          {...otherProps}
          onSubmit={this.onSubmit}
        />
      ) : null;
    }

    return null;
  }
}

EssentialShopContainer.propTypes = {
  filtersMaps: PropTypes.shape({}).isRequired,
  onSubmit: PropTypes.func.isRequired,
  setSelectedFilter: PropTypes.func.isRequired,
  data: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => {
  const productBlocks = getLoadedProductsPages(state);
  const initialBlock = (productBlocks && productBlocks[0]) || [];
  return {
    activeStep: getActiveStep(state),
    filterFormValues: getFormValues(FILTER_FORM)(state),
    filtersMaps: getProductsFilters(state),
    onSubmit: submitProductListingFiltersForm,
    initialValues: {
      ...getAppliedFilters(state),
      sort: getAppliedSortId(state) || '',
    },
    labels: essentialShoppingLabels(state),
    isLoadingMore: getIsLoadingMore(state) && !initialBlock.length,
    isESModuleLoading: getIsESModuleLoading(state),
    breadCrumbs: processBreadCrumbs(state.ProductListing && state.ProductListing.breadCrumbTrail),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    destroyEssentialFormState: () => {
      dispatch(destroy(FILTER_FORM));
    },
    resetForm: () => {
      dispatch(reset(FILTER_FORM));
    },
    setActiveStep: payload => {
      dispatch(setEssentialShopStep(payload));
    },
    getProducts: payload => {
      dispatch(getPlpProducts(payload));
    },
    setSelectedFilter: payload => {
      dispatch(setFilter(payload));
    },
    trackEssentialsAnalytics: payload => {
      dispatch(setClickAnalyticsData(payload));
      if (isMobileApp()) {
        dispatch(trackClick(payload));
      } else {
        dispatch(trackClick());
      }
      const timer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(timer);
      }, 200);
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EssentialShopContainer);
