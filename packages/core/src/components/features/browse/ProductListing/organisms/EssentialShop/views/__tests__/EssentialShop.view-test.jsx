// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { EssentialShopVanilla } from '../EssentialShop.view';

describe('Essential Shop View', () => {
  const props = {
    setActiveStep: jest.fn(),
    filtersMaps: {
      categoryPath2_uFilter: [
        {
          displayName: 'Bottoms',
          id: 'Bottoms',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Shorts Extended Sizes',
          id: 'Shorts Extended Sizes',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'School Uniforms',
          id: 'School Uniforms',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Uniform Extended Sizes',
          id: 'Uniform Extended Sizes',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Boy',
          id: 'Boy',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Denim Extended Sizes',
          id: 'Denim Extended Sizes',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Extended Sizes',
          id: 'Extended Sizes',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Jeans',
          id: 'Jeans',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Matchables',
          id: 'Matchables',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Essentials Shop',
          id: 'Essentials Shop',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Polos',
          id: 'Polos',
          facetName: 'categoryPath2_uFilter',
          style: '#c8102e',
        },
        {
          displayName: 'Big & Lil Brother',
          id: 'Big & Lil Brother',
          facetName: 'categoryPath2_uFilter',
        },
        {
          displayName: 'Tops',
          id: 'Tops',
          facetName: 'categoryPath2_uFilter',
          style: '#c25621',
        },
      ],
      TCPColor_uFilter: [
        {
          displayName: 'BLUE',
          id: 'BLUE',
          imagePath: '/wcsstore/GlobalSAS/images/tcp/category/color-swatches/BLUE.gif',
          facetName: 'TCPColor_uFilter',
        },
        {
          displayName: 'GRAY',
          id: 'GRAY',
          imagePath: '/wcsstore/GlobalSAS/images/tcp/category/color-swatches/GRAY.gif',
          facetName: 'TCPColor_uFilter',
        },
        {
          displayName: 'BLACK',
          id: 'BLACK',
          imagePath: '/wcsstore/GlobalSAS/images/tcp/category/color-swatches/BLACK.gif',
          facetName: 'TCPColor_uFilter',
        },
        {
          displayName: 'RED',
          id: 'RED',
          imagePath: '/wcsstore/GlobalSAS/images/tcp/category/color-swatches/RED.gif',
          facetName: 'TCPColor_uFilter',
        },
        {
          displayName: 'TAN',
          id: 'TAN',
          imagePath: '/wcsstore/GlobalSAS/images/tcp/category/color-swatches/TAN.gif',
          facetName: 'TCPColor_uFilter',
        },
        {
          displayName: 'GREEN',
          id: 'GREEN',
          imagePath: '/wcsstore/GlobalSAS/images/tcp/category/color-swatches/GREEN.gif',
          facetName: 'TCPColor_uFilter',
        },
        {
          displayName: 'DENIM',
          id: 'DENIM',
          imagePath: '/wcsstore/GlobalSAS/images/tcp/category/color-swatches/DENIM.gif',
          facetName: 'TCPColor_uFilter',
        },
        {
          displayName: 'MULTI',
          id: 'MULTI',
          imagePath: '/wcsstore/GlobalSAS/images/tcp/category/color-swatches/MULTI.gif',
          facetName: 'TCPColor_uFilter',
        },
      ],
      v_tcpsize_uFilter: [
        {
          displayName: 'XS',
          id: 'AF_XS',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'S',
          id: 'AT_S',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'M',
          id: 'BJ_M',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'L',
          id: 'BZ_L',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'L (10H-12H',
          id: 'CM_L (10H-12H',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'XL',
          id: 'CS_XL',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'XL(14H)',
          id: 'CY_XL(14H)',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: '18-24 M',
          id: 'EH_18-24 M',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'S (5/6)',
          id: 'FM_S (5/6)',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'M (7/8)',
          id: 'FN_M (7/8)',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'L (10/12)',
          id: 'FO_L (10/12)',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'XL (14)',
          id: 'FP_XL (14)',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: 'XXL (16)',
          id: 'FQ_XXL (16)',
          facetName: 'v_tcpsize_uFilter',
        },
        {
          displayName: '18H',
          id: 'HV_18H',
          facetName: 'v_tcpsize_uFilter',
        },
      ],
      unbxd_price_range_uFilter: [
        {
          displayName: '$10 and Under',
          id: '$10 and Under',
          facetName: 'unbxd_price_range_uFilter',
        },
        {
          displayName: '$10 - $19.99',
          id: '$10 - $19.99',
          facetName: 'unbxd_price_range_uFilter',
        },
        {
          displayName: '$20 - $29.99',
          id: '$20 - $29.99',
          facetName: 'unbxd_price_range_uFilter',
        },
      ],
      v_tcpfit_unbxd_uFilter: [
        {
          displayName: 'Regular',
          id: 'Regular',
          facetName: 'v_tcpfit_unbxd_uFilter',
        },
        {
          displayName: 'Husky',
          id: 'Husky',
          facetName: 'v_tcpfit_unbxd_uFilter',
        },
        {
          displayName: 'Slim',
          id: 'Slim',
          facetName: 'v_tcpfit_unbxd_uFilter',
        },
      ],
      gender_uFilter: [
        {
          displayName: 'Boy',
          id: 'Boy',
          facetName: 'gender_uFilter',
        },
      ],
      age_group_uFilter: [
        {
          displayName: 'Big Kid (4 - 18)',
          id: 'AB_Big Kid (4 - 18)',
          facetName: 'age_group_uFilter',
        },
      ],
      unbxdDisplayName: {
        categoryPath2_uFilter: 'Category',
        TCPColor_uFilter: 'Color',
        v_tcpsize_uFilter: 'Size',
        unbxd_price_range_uFilter: 'Price',
        v_tcpfit_unbxd_uFilter: 'Fit',
        gender_uFilter: 'Gender',
        age_group_uFilter: 'Size Range',
      },
      l1category: '',
    },
    department: {
      displayName: 'BOY',
      destination: 'c',
    },
    filterFormValues: {
      sort: '',
    },
    className: 'sc-fnwBNb iTMUwU',
  };

  it('should render correctly', () => {
    const tree = shallow(<EssentialShopVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
