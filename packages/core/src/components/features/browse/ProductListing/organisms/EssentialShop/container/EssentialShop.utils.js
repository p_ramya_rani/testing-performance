// 9fbef606107a605d69c0edbcd8029e5d 
import isEmpty from 'lodash/isEmpty';
import theme from '@tcp/core/styles/themes/TCP';

/**
 * @method getGuidedStepsFromCMS
 * @description get steps from the CMS
 */
const getGuidedStepsFromCMS = essentialsCMSData => {
  return (essentialsCMSData && essentialsCMSData.essentialsStepsWrapper) || [];
};

/**
 * @method - getGuidedStepHeader
 * @description - get header/title from headerText
 * @param {object} header - step headerText
 */
const getGuidedStepHeader = textItems => {
  const responseHeader = {
    title: '',
    style: '',
  };
  if (!isEmpty(textItems) && textItems[0].text) {
    const textItem = textItems[0];
    const { text, style } = textItem;
    responseHeader.title = text;
    responseHeader.style = style;
  }
  return responseHeader;
};

const getGuidedItem = (facet, styled, mediaList, styleColor) => {
  return {
    text: (styled && styled.text) || '',
    type: (facet && facet.facetId) || '',
    style: styleColor,
    defaultImage: (mediaList && mediaList[0]) || '',
    selectedImage: (mediaList && mediaList[1]) || '',
  };
};

/**
 * @method getGuidedOptions
 * @description Get facet option for each step
 * @param {*} facetOptions
 */
const getGuidedOptions = facetOptions => {
  const options = [];
  facetOptions.forEach(facetOption => {
    const { facet, styled, mediaList } = facetOption;
    const styleColor = (facetOption && facetOption.color && facetOption.color.color) || '';
    const item = getGuidedItem(facet, styled, mediaList, styleColor);
    options.push(item);
  });
  return options;
};

/**
 * @method getParsedGuidedSteps
 * @description Parsed json for UI
 */
const getParsedGuidedSteps = essentialsCMSData => {
  const guidedSteps = getGuidedStepsFromCMS(essentialsCMSData) || [];
  const steps = [];
  guidedSteps.forEach(val => {
    if (val) {
      const { textItems, essentialsFormWrapper } = val;
      const step = {
        header: getGuidedStepHeader(textItems),
        type: (val.essentialsStepType && val.essentialsStepType.itemType) || '',
        facetName: (val.facet && val.facet.facetId) || '',
        options: essentialsFormWrapper ? getGuidedOptions(essentialsFormWrapper) : [],
      };
      steps.push(step);
    }
  });
  return steps;
};

/**
 * @method getGuidedHeader
 * @description get Header component from CMS
 */
const getGuidedHeader = essentialsCMSData => {
  return (essentialsCMSData && essentialsCMSData.multicolorText) || [];
};

/**
 * @method getGuidedHeaderText
 * @description - gets the header text with color
 */
const getGuidedHeaderText = header => {
  const headerText = (header && header[0] && header[0].text && header[0].text.text) || '';
  return headerText.toUpperCase();
};

/**
 * @method parseColor
 * @description Returns the color from theme color palette
 */
const parseColor = style => {
  const arr = style.split('-');
  return arr && arr.length > 1 ? theme.colorPalette[arr[0]][arr[1]] : arr[0];
};

/**
 * @method getFacetList
 * @description Returns the unbxd filter list with the sequencing as per CMS
 */
const getFacetList = (options = [], filter = []) => {
  const facets = [...filter];
  const newFacets = [];

  options.forEach(item => {
    facets.forEach((category, index) => {
      if (category.id === item.text) {
        facets.splice(index, 1);
        newFacets.push(category);
        const ind = newFacets.length - 1;
        newFacets[ind].style = item.style ? parseColor(item.style) : '';
        newFacets[ind].defaultImage = item.defaultImage ? item.defaultImage : {};
        newFacets[ind].selectedImage = item.selectedImage ? item.selectedImage : {};
      }
    });
  });
  return newFacets.concat(facets);
};

/**
 * @method getHeaderColors
 * @description Get header text color for each alphabet
 */
const getHeaderColors = header => {
  const colors = (!isEmpty(header) && header[0].color) || {};
  return (colors.color && colors.color.split(',')) || '';
};

const getEssentialStepsId = essentialsCMSData => {
  const guidedSteps = getGuidedStepsFromCMS(essentialsCMSData) || [];
  return (
    guidedSteps.map(
      steps => steps && steps.essentialsStepType && steps.essentialsStepType.itemType
    ) || []
  );
};

export {
  getGuidedStepsFromCMS,
  getGuidedHeader,
  getGuidedHeaderText,
  getParsedGuidedSteps,
  getHeaderColors,
  getFacetList,
  parseColor,
  getEssentialStepsId,
};
