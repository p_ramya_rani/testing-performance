// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { EssentialShopContainer } from '../EssentialShop.container';

describe('EssentialShopContainer', () => {
  it('should render correctly', () => {
    const container = shallow(<EssentialShopContainer />);
    expect(container).toMatchSnapshot();
  });
});
