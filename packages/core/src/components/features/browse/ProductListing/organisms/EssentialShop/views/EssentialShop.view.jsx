// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment, createRef } from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Col, Button, BodyCopy } from '@tcp/core/src/components/common/atoms';
import styles from '../styles/EssentialShop.style';
import GuidedShoppingCategories from '../../../molecules/GuidedShoppingCategories';
import GuidedShoppingStyles from '../../../molecules/GuidedShoppingStyles';
import GuidedShoppingColors from '../../../molecules/GuidedShoppingColors';
import {
  getGuidedHeaderText,
  getHeaderColors,
  getGuidedHeader,
  getFacetList,
} from '../container/EssentialShop.utils';
import EssentialShopCommon from './EssentialShop.common';

class EssentialShop extends EssentialShopCommon {
  constructor(props) {
    super(props);
    this.ref = createRef();
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside);
    document.addEventListener('keydown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside);
    document.removeEventListener('keydown', this.handleClickOutside);
  }

  /**
   * @method - renderHeaderText
   * @@description - render Mix n Match Header from CMS data
   */
  renderHeaderText = () => {
    const { essentialsCMSData } = this.props;
    let index = -1;
    const header = getGuidedHeader(essentialsCMSData);
    const headerText = getGuidedHeaderText(header);
    const colorData = getHeaderColors(header);
    return headerText.split('').map((item) => {
      if (item === ' ') {
        return <BodyCopy component="span">{item}</BodyCopy>;
      }

      index += 1;
      index = !colorData[index] ? 0 : index;

      return (
        <BodyCopy
          component="span"
          fontSize="fs30"
          fontFamily="primary"
          customStyle={{ color: (colorData[index] || '').trim() }}
          fontWeight="bold"
        >
          {item}
        </BodyCopy>
      );
    });
  };

  renderStartStep = (startStep, { startText }) => {
    const { header: { title } = {} } = startStep;
    return (
      <Col colSize={{ small: 6, medium: 8, large: 12 }}>
        <BodyCopy fontSize="fs16" fontFamily="secondary" textAlign="center">
          {title}
        </BodyCopy>
        <Button
          fill="BLUE"
          onClick={() => this.handleRemoveAllFilters()}
          buttonVariation="variable-width"
          className="essentials-start-cta"
        >
          {startText}
        </Button>
      </Col>
    );
  };

  renderStepStyle = (stepInfo) => {
    const { activeDotCount } = this.state;
    const { filtersMaps = {}, labels, trackEssentialsAnalytics } = this.props;
    const { header: { title, style } = {}, facetName = '', options = [] } = stepInfo;
    let categoryList = (filtersMaps && filtersMaps[facetName]) || [];
    categoryList =
      options.length && categoryList.length ? getFacetList(options, categoryList) : categoryList;
    return (
      <Fragment>
        {title && (
          <BodyCopy
            className="headerText"
            fontSize={['fs12', 'fs12', 'fs14']}
            fontFamily="secondary"
            textAlign="center"
            customStyle={{ color: style }}
          >
            {title}
          </BodyCopy>
        )}
        <GuidedShoppingStyles
          setActiveStep={this.setActiveStep}
          stepInfo={stepInfo}
          categoryList={categoryList}
          labels={labels}
          activeDotCount={activeDotCount}
          trackAnalytics={trackEssentialsAnalytics}
        />
      </Fragment>
    );
  };

  renderStepColor = (stepInfo) => {
    const { activeDotCount } = this.state;
    const { filtersMaps, filterFormValues, labels, trackEssentialsAnalytics } = this.props;
    const { header: { title } = {}, facetName = '', options = [] } = stepInfo;
    let categoryList = (filtersMaps && filtersMaps[facetName]) || [];
    categoryList =
      options.length && categoryList.length ? getFacetList(options, categoryList) : categoryList;
    return (
      <Fragment>
        <BodyCopy
          component="div"
          className="elem-mt-SM elem-mb-SM"
          textAlign="center"
          fontSize="fs14"
          fontFamily="secondary"
        >
          {title}
        </BodyCopy>
        <GuidedShoppingColors
          categoryList={categoryList}
          setActiveStep={this.setActiveStep}
          stepInfo={stepInfo}
          filterFormValues={filterFormValues}
          labels={labels}
          clearColorHandler={this.clearColorHandler}
          activeDotCount={activeDotCount}
          trackAnalytics={trackEssentialsAnalytics}
        />
      </Fragment>
    );
  };

  renderDots = () => {
    const activeDotCount = this.getActiveDotCount() || 0;
    const dotsComp = [];

    for (let i = 0; i < this.steps.filter(Boolean).length; i += 1) {
      const activeDot = i === activeDotCount ? 'active-dot' : null;
      dotsComp.push(<BodyCopy component="span" className={`${activeDot} dots`} />);
    }
    return dotsComp;
  };

  renderNewSearch = (step, labels) => {
    const { header: { title } = {} } = step;
    return (
      <Col colSize={{ small: 6, medium: 8, large: 12 }}>
        <BodyCopy
          fontSize="fs16"
          className="essentials-new-search-container"
          fontFamily="secondary"
          textAlign="center"
        >
          {title}
        </BodyCopy>
        <Button
          className="essentials-search-cta"
          onClick={() => this.handleRemoveAllFilters()}
          type="button"
          buttonVariation="variable-width"
          nohover
        >
          {labels.newSearch}
        </Button>
      </Col>
    );
  };

  // TODO refactor for complexity - WIP
  // eslint-disable-next-line sonarjs/cognitive-complexity
  renderSteps = () => {
    const { department, filtersMaps, labels, guidedSteps, trackEssentialsAnalytics } = this.props;
    const filteredSteps = guidedSteps
      ? guidedSteps.map((step) => {
          switch (step.type) {
            case 'info':
              return this.renderStartStep(step, labels);
            case 'checkbox':
              return this.canGotoStep(step.facetName) ? (
                <GuidedShoppingCategories
                  content={step}
                  className="essential-shop-step-1"
                  filedName={step.facetName}
                  filters={filtersMaps[step.facetName]}
                  department={department}
                  labels={labels}
                  trackAnalytics={trackEssentialsAnalytics}
                />
              ) : null;
            case 'thumbnail':
              return this.canGotoStep(step.facetName) ? this.renderStepStyle(step) : null;
            case 'color':
              return this.canGotoStep(step.facetName) ? this.renderStepColor(step) : null;
            case 'search':
              return this.renderNewSearch(step, labels);
            default:
              return null;
          }
        })
      : [];
    this.setUpdatedSteps(filteredSteps);
    return filteredSteps;
  };

  handleClickOutside = (e) => {
    const timer = setTimeout(() => {
      const { resetForm } = this.props;
      const { moduleFocused } = this.state;
      if (this.ref.current && !this.ref.current.contains(e.target) && moduleFocused) {
        resetForm();
        this.setState({
          moduleFocused: false,
        });
      }
      clearTimeout(timer);
    });
  };

  handleFocusOnModule = () => {
    this.setState({
      moduleFocused: true,
    });
  };

  render() {
    const { className, isLoadingMore } = this.props;
    const { activeStep } = this.state;
    const steps = this.renderSteps();
    super.steps = steps;
    return (
      <React.Fragment>
        <div className={className} ref={this.ref}>
          {!isLoadingMore ? (
            <BodyCopy
              className="essential-wrapper"
              component="div"
              onClick={this.handleFocusOnModule}
              onKeyDown={this.handleFocusOnModule}
            >
              <div className="essential-content">
                {this.renderHeaderText()}
                <form onSubmit={(e) => this.handleOnSubmit(e)} className={className} noValidate>
                  {steps && steps[activeStep]}
                </form>
                <BodyCopy component="div" className="dots-container elem-mt-MED">
                  {this.renderDots()}
                </BodyCopy>
              </div>
            </BodyCopy>
          ) : (
            <LoaderSkelton width="100%" height="300px" />
          )}
        </div>
      </React.Fragment>
    );
  }
}

EssentialShop.propTypes = {
  department: PropTypes.shape({}).isRequired,
  filtersMaps: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
  getProducts: PropTypes.func.isRequired,
  filterFormValues: PropTypes.shape({}).isRequired,
  resetForm: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  trackEssentialsAnalytics: PropTypes.func,
};

export default withStyles(
  reduxForm({
    form: 'filter-form',
    enableReinitialize: true,
  })(EssentialShop),
  styles
);

export { EssentialShop as EssentialShopVanilla };
