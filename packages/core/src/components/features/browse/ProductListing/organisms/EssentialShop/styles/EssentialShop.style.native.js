// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';

const MixMatchWrapper = styled.View``;
const EssentialsText = styled.View`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 80%;
`;
const ButtonWrapper = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const StyledButton = styled(BodyCopy)`
  font-family: ${props => props.theme.typography.fonts.secondary};
  border: none;
  color: ${props => props.theme.colorPalette.gray[900]};
  font-weight: normal;
  text-decoration: underline;
  text-transform: none;
  letter-spacing: 0;
  background-color: ${props => props.theme.colors.WHITE};
`;

const buttonStyle = {
  button: {
    height: 32,
  },
  text: {
    fontSize: 12,
  },
};
const StepsWrapper = styled.View`
  margin: ${props => props.theme.spacing.ELEM_SPACING.XS};
  background-color: ${props => props.theme.colors.WHITE};
  padding: 18px;
  justify-content: center;
  align-items: center;
  border: 6px solid
    ${props =>
      props.theme.isGymboree
        ? props.theme.colorPalette.orange[800]
        : props.theme.colorPalette.blue[500]};
`;

const WordWrapper = styled.View`
  display: flex;
  flex-direction: row;
`;

export {
  MixMatchWrapper,
  EssentialsText,
  ButtonWrapper,
  StyledButton,
  buttonStyle,
  StepsWrapper,
  WordWrapper,
};
