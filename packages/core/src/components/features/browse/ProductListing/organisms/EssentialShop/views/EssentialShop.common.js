// 9fbef606107a605d69c0edbcd8029e5d 
import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isMobileApp } from '@tcp/core/src/utils';
import { getEssentialStepsId } from '../container/EssentialShop.utils';

class EssentialShopCommon extends PureComponent {
  steps = [];

  constructor(props) {
    super(props);
    const essentialSteps = getEssentialStepsId(props.essentialsCMSData);
    this.state = {
      activeStep: 0,
      essentialSteps,
      activeCategory: essentialSteps[0],
      currentType: null,
    };
  }

  componentDidUpdate(prevProps) {
    const { activeStep: activeStepProp } = this.props;
    const { activeStep: prevActiveStep } = prevProps;
    const { activeStep, essentialSteps } = this.state;
    if (activeStep !== activeStepProp && prevActiveStep !== activeStepProp) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        activeStep: activeStepProp || 0,
        activeCategory: essentialSteps[activeStepProp || 0],
      });
    }
  }

  canGotoStep = name => {
    const { filtersMaps } = this.props;
    const categoriesFacetsMap = filtersMaps && filtersMaps[name];
    return categoriesFacetsMap && categoriesFacetsMap.length;
  };

  handleAnalytics = (filterFormValues, breadCrumbs) => {
    const analyticsTimer = setTimeout(() => {
      const { trackEssentialsAnalytics, guidedSteps, activeStep } = this.props;
      const l1Cat = breadCrumbs && breadCrumbs[0] && breadCrumbs[0].displayName;
      let events = ['event174'];
      let essentialEventName = 'Essentials-Next';
      const filterValues =
        filterFormValues !== undefined ? Object.keys(filterFormValues) : ['sort'];
      const arr = [];

      filterValues.forEach(key => {
        let str = '';
        const facetValue = filterFormValues && filterFormValues[key];
        if (key !== 'sort') {
          if (facetValue.length > 1) {
            str += facetValue.join('|');
          } else if (facetValue.length !== 0) {
            str += facetValue[0];
          }
        }
        if (str !== '') arr.push(str);
      });

      const facetValue = `${l1Cat}-${arr.length > 1 ? arr.join('-') : arr[0]}`;

      if (
        guidedSteps &&
        guidedSteps[activeStep - 1] &&
        guidedSteps[activeStep - 1].type === 'color'
      ) {
        events = ['event176'];
        essentialEventName = 'Essentials-Find Your Styles';
      }

      trackEssentialsAnalytics({
        customEvents: events,
        eventName: essentialEventName,
        essentialsFormValue: facetValue,
        name: 'essentials',
      });
      clearTimeout(analyticsTimer);
    }, 100);
  };

  /**
   * @method handleOnSubmit
   * @description Handler to submit the selected facets and get the data from UNBXD
   */

  handleOnSubmit = e => {
    if (e && e.preventDefault) {
      e.preventDefault();
    }
    const { onSubmit, getProducts, filterFormValues, navigation, breadCrumbs } = this.props;
    if (isMobileApp()) {
      const url = navigation && navigation.getParam('url');
      onSubmit(filterFormValues, false, getProducts, url, true);
    } else {
      onSubmit(filterFormValues, false, getProducts);
    }
    this.handleAnalytics(filterFormValues, breadCrumbs);
    this.setActiveStep('inc');
  };

  setDefaultCategory = name => {
    const {
      department: { displayName = '' },
      filtersMaps,
    } = this.props;
    const categoriesFacetsMap = (filtersMaps && filtersMaps[name]) || [];
    const defaultCategoryIndex = categoriesFacetsMap.findIndex(
      facet => facet.displayName.toLowerCase() === displayName.toLowerCase()
    );
    if (defaultCategoryIndex > -1) {
      const departmentName = categoriesFacetsMap[defaultCategoryIndex].displayName;
      return {
        categoryPath2_uFilter: [departmentName],
      };
    }
    return {};
  };

  setActiveStep = source => {
    let { activeStep, activeCategory, currentType } = this.state;
    const { essentialSteps } = this.state;
    const { setActiveStep, resetForm } = this.props;
    const lastActiveStep = activeStep;
    if (source === 'inc') {
      activeCategory = essentialSteps[activeStep + 1];
      currentType = source;
      activeStep += 1;
      while (!this.steps[activeStep] && activeStep < (this.steps || []).length) {
        activeStep += 1;
      }
      if (!this.steps[activeStep]) {
        activeStep = this.steps.length - 1;
      }
    } else if (source === 'dec') {
      resetForm();
      activeCategory = essentialSteps[activeStep - 1];
      currentType = source;
      activeStep -= 1;
      while (!this.steps[activeStep] && activeStep !== 0) {
        activeStep -= 1;
      }
      if (!this.steps[activeStep]) {
        activeStep = lastActiveStep;
      }
    }
    setActiveStep(activeStep);
    this.setState({
      activeStep,
      currentType,
      activeCategory,
    });
  };

  getNextClick = (filteredSteps, stepCount) => {
    const { activeStep, currentType } = this.state;
    return currentType === 'inc' && (activeStep !== stepCount || !filteredSteps[activeStep]);
  };

  setUpdatedSteps = filteredSteps => {
    const { activeStep, essentialSteps, activeCategory } = this.state;
    const { setActiveStep } = this.props;
    let stepCount = essentialSteps.indexOf(activeCategory);

    if (
      this.getNextClick(filteredSteps, stepCount) ||
      (!activeStep && !filteredSteps[activeStep])
    ) {
      if (!filteredSteps[stepCount]) {
        while (!filteredSteps[stepCount] && stepCount < (filteredSteps || []).length) {
          stepCount += 1;
        }
      }
      if (!filteredSteps[stepCount]) {
        stepCount = filteredSteps.length - 1;
      }
      this.setState({
        activeStep: stepCount,
      });
      setActiveStep(stepCount);
    }
  };

  getActiveDotCount = () => {
    const { activeStep, activeDotCount } = this.state;
    const beforeSteps = this.steps.slice(0, activeStep);
    const activeCount = beforeSteps.filter(Boolean).length;
    const dotPosition = activeStep - (beforeSteps.length - activeCount);
    if (activeDotCount !== dotPosition) {
      this.setState({
        activeDotCount: dotPosition,
      });
    }
    return dotPosition;
  };

  clearColorHandler = facetName => {
    const { onSubmit, filterFormValues, getProducts, asPath } = this.props;
    if (asPath.includes(facetName)) {
      filterFormValues[facetName] = [];
      onSubmit(filterFormValues, false, getProducts);
    }
  };

  handleRemoveAllFilters() {
    const {
      filterFormValues,
      getProducts,
      onSubmit,
      destroyEssentialFormState,
      setActiveStep,
      navigation,
      trackEssentialsAnalytics,
    } = this.props;

    const { activeStep, essentialSteps } = this.state;
    destroyEssentialFormState();
    const obj = {};
    // eslint-disable-next-line no-restricted-syntax
    for (const key in filterFormValues) {
      if (key !== 'sort') {
        obj[key] = [];
      }
    }

    let customEvent = [];
    let clickEvent = '';
    if (activeStep === 0) {
      customEvent = ['event173'];
      clickEvent = 'Essentials-Start';
    } else if (activeStep === essentialSteps.length - 1) {
      customEvent = ['event175'];
      clickEvent = 'Essentials-New Search';
    }

    trackEssentialsAnalytics({
      customEvents: customEvent,
      eventName: clickEvent,
      name: 'essentials',
    });

    if (isMobileApp()) {
      const url = navigation && navigation.getParam('url');
      onSubmit(obj, false, getProducts, url, true);
    } else {
      onSubmit(obj, false, getProducts);
    }

    if (!activeStep) {
      this.setActiveStep('inc');
    } else {
      setActiveStep(0);
      this.setState({
        activeStep: 0,
        currentType: null,
        activeCategory: essentialSteps[0],
      });
    }
  }
}

EssentialShopCommon.defaultProps = {
  trackEssentialsAnalytics: () => null,
  breadCrumbs: [],
};

EssentialShopCommon.propTypes = {
  department: PropTypes.shape({}).isRequired,
  filtersMaps: PropTypes.shape({}).isRequired,
  getProducts: PropTypes.func.isRequired,
  filterFormValues: PropTypes.shape({}).isRequired,
  onSubmit: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  activeStep: PropTypes.number.isRequired,
  resetForm: PropTypes.func.isRequired,
  setActiveStep: PropTypes.func.isRequired,
  destroyEssentialFormState: PropTypes.func.isRequired,
  asPath: PropTypes.string.isRequired,
  essentialsCMSData: PropTypes.shape({}).isRequired,
  guidedSteps: PropTypes.shape({}).isRequired,
  trackEssentialsAnalytics: PropTypes.func,
  breadCrumbs: PropTypes.arrayOf(PropTypes.shape({})),
};

EssentialShopCommon.defaultProps = {
  navigation: {},
};

export default EssentialShopCommon;
