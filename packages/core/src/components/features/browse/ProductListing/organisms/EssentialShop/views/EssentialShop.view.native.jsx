// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { View, Text } from 'react-native';
import { Button } from '@tcp/core/src/components/common/atoms';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { Pagination } from 'react-native-snap-carousel';
import colors from '@tcp/core/styles/themes/TCP/colors';
import {
  MixMatchWrapper,
  EssentialsText,
  ButtonWrapper,
  StyledButton,
  StepsWrapper,
  WordWrapper,
  buttonStyle,
} from '../styles/EssentialShop.style.native';
import {
  getGuidedHeaderText,
  getHeaderColors,
  getGuidedHeader,
} from '../container/EssentialShop.utils';
import GuidedShoppingStyles from '../../../molecules/GuidedShoppingStyles';
import GuidedShoppingCategories from '../../../molecules/GuidedShoppingCategories/views';
import GuidedShoppingColors from '../../../molecules/GuidedShoppingColors';
import EssentialShopCommon from './EssentialShop.common';

class EssentialShop extends EssentialShopCommon {
  renderHeaderText = (text = '', header) => {
    let index = -1;
    const colorData = getHeaderColors(header);
    const wordArr = text.split(' ');
    return text.split(' ').map((word, i) => {
      return (
        <WordWrapper>
          {word.split('').map(item => {
            index += 1;
            if (!colorData[index]) {
              index = -1;
            }
            return (
              <BodyCopyWithSpacing
                fontFamily="primary"
                fontSize="fs26"
                fontWeight="bold"
                text={item}
                spacingStyles="margin-top-MED  margin-bottom-MED "
                textAlign="center"
                style={{ color: (colorData[index] || '').trim() }}
              />
            );
          })}
          {i - 1 !== wordArr.length ? <Text> </Text> : null}
        </WordWrapper>
      );
    });
  };

  renderStepStyle = stepInfo => {
    const { header = {} } = stepInfo;
    const { activeDotCount } = this.state;
    const { filtersMaps, labels } = this.props;
    return (
      <>
        <BodyCopyWithSpacing
          fontSize="fs14"
          fontFamily="secondary"
          textAlign="center"
          spacingStyles="margin-top-XXS"
          text={header && header.title}
          numberOfLines={1}
          style={{ color: header && header.style }}
        />
        <GuidedShoppingStyles
          onSubmit={this.handleOnSubmit}
          stepInfo={stepInfo}
          filters={filtersMaps}
          setActiveStep={this.setActiveStep}
          activeDotCount={activeDotCount}
          labels={labels}
        />
      </>
    );
  };

  renderStartStep = (startStep, labels) => {
    const {
      header: { title },
    } = startStep;
    return (
      <>
        <BodyCopyWithSpacing
          fontFamily="secondary"
          fontSize="fs16"
          text={title}
          spacingStyles="margin-top-MED margin-right-SM margin-bottom-MED margin-left-SM"
          fontWeight="regular"
          textAlign="center"
        />
        <ButtonWrapper>
          <Button
            width="225px"
            color="white"
            fill="BLUE"
            text={labels.startText}
            fontSize="fs10"
            onPress={() => this.handleRemoveAllFilters()}
          />
        </ButtonWrapper>
      </>
    );
  };

  renderStepColor = stepInfo => {
    const { header = {} } = stepInfo;
    const { activeDotCount } = this.state;
    const { filtersMaps, filterFormValues, labels } = this.props;

    return (
      <>
        <BodyCopyWithSpacing
          fontSize="fs14"
          fontFamily="secondary"
          textAlign="center"
          spacingStyles="margin-top-XXS"
          text={header && header.title}
          numberOfLines={1}
          style={{ color: header && header.style }}
        />
        <GuidedShoppingColors
          onSubmit={this.handleOnSubmit}
          stepInfo={stepInfo}
          filters={filtersMaps}
          setActiveStep={this.setActiveStep}
          activeDotCount={activeDotCount}
          filterFormValues={filterFormValues}
          labels={labels}
          clearColorHandler={this.clearColorHandler}
        />
      </>
    );
  };

  renderNewSearch = (step, labels) => {
    const {
      header: { title },
    } = step;
    return (
      <View>
        <BodyCopyWithSpacing
          fontFamily="secondary"
          fontSize="fs16"
          text={title}
          spacingStyles="margin-top-MED margin-right-SM margin-bottom-MED margin-left-SM"
          fontWeight="bold"
          textAlign="center"
        />
        <ButtonWrapper>
          <StyledButton
            fontFamily="secondary"
            text={labels.newSearch}
            onPress={() => this.handleRemoveAllFilters()}
            customTextStyle={buttonStyle.text}
            customStyle={buttonStyle.button}
          />
        </ButtonWrapper>
      </View>
    );
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  renderSteps = () => {
    const { department, filtersMaps, labels, guidedSteps } = this.props;
    const filteredSteps = guidedSteps
      ? guidedSteps.map(step => {
          switch (step.type) {
            case 'info':
              return this.renderStartStep(step, labels);
            case 'checkbox':
              return this.canGotoStep(step.facetName) ? (
                <GuidedShoppingCategories
                  content={step}
                  className="essential-shop-step-1"
                  onSubmit={this.handleOnSubmit}
                  filedName={step.facetName}
                  filters={filtersMaps[step.facetName]}
                  department={department}
                  labels={labels}
                />
              ) : null;
            case 'thumbnail':
              return this.canGotoStep(step.facetName) ? this.renderStepStyle(step) : null;
            case 'color':
              return this.canGotoStep(step.facetName) ? this.renderStepColor(step) : null;
            case 'search':
              return this.renderNewSearch(step, labels);
            default:
              return null;
          }
        })
      : [];
    this.setUpdatedSteps(filteredSteps);
    return filteredSteps;
  };

  getPagination() {
    const paginationProps = {
      containerStyle: {},
    };
    const {
      containerStyle: containerStyleOverride,
      dotContainerStyle: dotContainerStyleOverride,
      dotStyle: dotStyleOverride,
      inactiveDotStyle: inactiveDotStyleOverride,
    } = paginationProps;
    const activeDotCount = this.getActiveDotCount() || 0;

    /* eslint-disable react-native/no-inline-styles */
    return (
      <Pagination
        dotsLength={this.steps.filter(Boolean).length}
        activeDotIndex={activeDotCount}
        containerStyle={{
          paddingVertical: 22,
          paddingHorizontal: 10,
          ...containerStyleOverride,
        }}
        dotContainerStyle={{ marginHorizontal: 4, ...dotContainerStyleOverride }}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 0,
          padding: 0,
          borderColor: colors.BLACK,
          borderWidth: 1,
          backgroundColor: colors.WHITE,
          ...dotStyleOverride,
        }}
        inactiveDotStyle={{
          backgroundColor: colors.BLACK,
          width: 6,
          height: 6,
          ...inactiveDotStyleOverride,
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={1}
      />
    );
  }

  render() {
    const steps = this.renderSteps();
    super.steps = steps;
    const { activeStep } = this.state;
    const { essentialsCMSData, isESModuleLoading } = this.props;

    const header = getGuidedHeader(essentialsCMSData);
    const headerText = getGuidedHeaderText(header);
    return (
      <MixMatchWrapper>
        {!isESModuleLoading ? (
          <StepsWrapper>
            <EssentialsText>{this.renderHeaderText(headerText, header)}</EssentialsText>
            <View>{steps && steps[activeStep]}</View>
            {this.getPagination()}
          </StepsWrapper>
        ) : (
          <LoaderSkelton width="100%" height="200px" />
        )}
      </MixMatchWrapper>
    );
  }
}

EssentialShop.propTypes = {
  department: PropTypes.shape({}).isRequired,
  filtersMaps: PropTypes.shape({}).isRequired,
  getProducts: PropTypes.func.isRequired,
  filterFormValues: PropTypes.shape({}).isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'filter-form',
  enableReinitialize: true,
})(EssentialShop);
