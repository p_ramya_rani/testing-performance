// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .essential-wrapper {
    padding: 6px;
    text-align: center;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    background: linear-gradient(to right, #96e1e8, #1454d1, #c571de, #fabbd5, #f2418a, #f8af4b);
  }

  .dots-container {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  .headerText {
    margin-top: 5px;
  }

  .dots {
    height: 6px;
    width: 6px;
    background-color: ${props => props.theme.colorPalette.gray[700]};
    border-radius: 50%;
    margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }

  .active-dot {
    border: 1.5px solid ${props => props.theme.colorPalette.gray[700]};
    background-color: ${props => props.theme.colors.WHITE};
    height: ${props => props.theme.spacing.ELEM_SPACING.XS};
    width: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }

  .essential-content {
    background: ${props => props.theme.colors.WHITE};
    padding: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }

  .essentials-start-cta {
    margin: ${props => props.theme.spacing.ELEM_SPACING.LRG} 0px;
  }

  .essentials-search-cta {
    border: none;
    font-family: Nunito;
    padding: 0;
    color: ${props => props.theme.colorPalette.gray[900]};
    margin: ${props => props.theme.spacing.ELEM_SPACING.LRG} 0px 0px;
    text-decoration: underline;
    font-weight: normal;
    text-transform: none;
    min-height: 20px;
    line-height: 1.36;
    letter-spacing: normal;
    &:hover {
      background-color: #fff !important;
    }
  }

  .back-button {
    margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }
`;
