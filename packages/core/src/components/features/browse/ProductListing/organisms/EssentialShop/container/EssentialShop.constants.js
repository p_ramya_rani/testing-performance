// 9fbef606107a605d69c0edbcd8029e5d 
import theme from '@tcp/core/styles/themes/TCP';

const { breakpoints } = theme;

const CAROUSEL_OPTIONS = {
  autoplay: false,
  arrows: true,
  speed: 1000,
  infinite: false,
  initialSlide: 0,
  dots: false,
  focusOnSelect: false,
  slidesToShow: 8,
  slidesToScroll: 8,
  lazyLoad: false,
  responsive: [
    {
      breakpoint: breakpoints.values.lg - 1,
      settings: {
        slidesToShow: 6.5,
        slidesToScroll: 1,
        arrows: false,
        swipeToSlide: true,
      },
    },
    {
      breakpoint: breakpoints.values.sm - 1,
      settings: {
        slidesToShow: 4.25,
        slidesToScroll: 1,
        arrows: false,
        swipeToSlide: true,
      },
    },
  ],
};

export const assetRouteBasePath = {
  getOriginImgHostSetting: () => {
    return 'https://test-int3.childrensplace.com';
  },
};

const WHITE = 'WHITE';

export { CAROUSEL_OPTIONS, WHITE };
