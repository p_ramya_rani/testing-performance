// 9fbef606107a605d69c0edbcd8029e5d 
import { essentialShoppingLabels } from '../EssentialShop.selectors';

describe('EssentialShop selectors', () => {
  it('getEssentialShopSelectors should return correct labels if present', () => {
    const state = {
      Labels: {
        Browse: {
          browseCommon: {
            lbl_essentialShopping_startSearch: 'start search',
            lbl_essentialShopping_next: 'next',
            lbl_essentialShopping_back: 'back',
            lbl_essentialShopping_newSearch: 'new search',
          },
        },
      },
    };

    expect(essentialShoppingLabels(state).startText).toBe('start search');
  });

  it('getEssentialShopSelectors should return label key if state is present but specified labels are not present', () => {
    expect(
      essentialShoppingLabels({
        Labels: {},
      }).next
    ).toBe('lbl_essentialShopping_next');
  });
});
