// 9fbef606107a605d69c0edbcd8029e5d 
import { getLabelValue } from '@tcp/core/src/utils';

export const getActiveStep = state => {
  return state.ProductListing.essentialShopStep;
};

export const essentialShoppingLabels = state => {
  return {
    startText: getLabelValue(
      state.Labels,
      'lbl_essentialShopping_startSearch',
      'browseCommon',
      'Browse'
    ),
    next: getLabelValue(state.Labels, 'lbl_essentialShopping_next', 'browseCommon', 'Browse'),
    back: getLabelValue(state.Labels, 'lbl_essentialShopping_back', 'browseCommon', 'Browse'),
    newSearch: getLabelValue(
      state.Labels,
      'lbl_essentialShopping_newSearch',
      'browseCommon',
      'Browse'
    ),
    colorNext: getLabelValue(
      state.Labels,
      'lbl_essentialShopping_colorNext',
      'browseCommon',
      'Browse'
    ),
  };
};

export default { getActiveStep };
