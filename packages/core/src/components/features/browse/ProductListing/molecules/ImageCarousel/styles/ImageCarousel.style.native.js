// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const ImageTouchableOpacity = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
`;

export default ImageTouchableOpacity;
