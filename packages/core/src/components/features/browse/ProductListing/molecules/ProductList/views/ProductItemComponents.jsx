/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module ProductItemComponents
 * Container of smaller function that will be renderer as Component to create a ProductItem.
 *
 * @author Florencia <facosta@minutentag.com>
 */
import React from 'react';
import PropTypes from 'prop-types';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import get from 'lodash/get';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { routerPush, urlContainsQuery, getBrand } from '@tcp/core/src/utils';
import { setSessionStorage } from '@tcp/core/src/utils/utils.web';
// import { isClient, isTouchClient } from 'routing/routingHelper';
// import { isTouchClient } from '../../../../../../../utils';
import {
  isClient,
  getIconPath,
  getLocator,
  isCanada,
  isGymboree,
  getSiteId,
} from '../../../../../../../utils';
import { getFormattedLoyaltyText, getProductListToPath } from '../utils/productsCommonUtils';
// import { labels } from '../labels/labels';
import { Image, BodyCopy, Anchor, Button, Col, RichText } from '../../../../../../common/atoms';

import ServerToClientRenderPatch from './ServerToClientRenderPatch';
import BundlePriceSection from './BundlePriceSection.view';

export function productLink(
  loadedProductCount,
  { to, asPath },
  event,
  alternateBrand,
  isSearchListing
) {
  event.preventDefault();
  const toPath = alternateBrand && isSearchListing ? `${to}$brand=${alternateBrand}` : `${to}`;
  const asPathNew =
    alternateBrand && isSearchListing ? `${asPath}$brand=${alternateBrand}` : `${asPath}`;
  if (isClient()) {
    const skuClicked = event.target.closest('.item-container-inner').getAttribute('unbxdparam_sku');
    setSessionStorage('LAST_PAGE_PATH', window.location.pathname);
    setSessionStorage('LOADED_PRODUCT_COUNT', loadedProductCount);
    setSessionStorage({ key: 'SCROLL_POINT', value: window.pageYOffset });
    setSessionStorage({ key: 'UNBXDPARAM_SKU', value: skuClicked });
    setSessionStorage('LINK_HREF', window.scrollY);
    routerPush(toPath, asPathNew);
  }
}

const renderBrandTitle = (isShowBrandNameEnabled, tcpBrand, gymBrand, primaryBrand) => {
  const prodBrand = getBrand() && getBrand().toUpperCase();
  return isShowBrandNameEnabled ? (
    <BodyCopy
      fontSize="fs12"
      fontWeight="bold"
      fontFamily="secondary"
      className="product-brand-plp"
    >
      {(primaryBrand || prodBrand) === 'GYM' ? gymBrand : tcpBrand}
    </BodyCopy>
  ) : null;
};

export function ProductTitle(values) {
  const {
    name,
    pdpUrl,
    loadedProductCount,
    children,
    isSearchPage,
    viaModule,
    isProductBrandOfSameDomain,
    page,
    isSoldOut,
    cookieToken,
    bundleProduct,
    dataSource,
    googleAnalyticsData,
    alternateBrand,
    labels,
    isShowBrandNameEnabled,
    primaryBrand,
    isSearchListing,
  } = values;

  const siteId = getSiteId();
  const pdpToPath = isProductBrandOfSameDomain ? getProductListToPath(pdpUrl) : pdpUrl;
  const filteredPdpToPath = bundleProduct
    ? pdpToPath.replace('/b?bid=', `/${siteId}/b/`)
    : pdpToPath.replace('/p?pid=', `/${siteId}/p/`);

  const pdpPageUrl =
    pdpToPath &&
    `${pdpToPath}&isSearchPage=${isSearchPage}&viaModule=${viaModule}&viaPage=${page}&dataSource=${dataSource}`;
  let pdpAsUrl = pdpUrl;
  const { tcpBrand = "The Children's Place", gymBrand = 'Gymboree' } = labels;
  if (cookieToken && !isProductBrandOfSameDomain) {
    pdpAsUrl = `${pdpAsUrl}${urlContainsQuery(pdpAsUrl) ? '&' : '?'}ct=${cookieToken}`;
  }
  return isSoldOut ? (
    <div className="product-title-container">
      <span inheritedStyles="product-title-content">
        {renderBrandTitle(isShowBrandNameEnabled, tcpBrand, gymBrand, primaryBrand)}
        <BodyCopy
          fontSize={['fs12', 'fs13', 'fs14']}
          fontFamily="secondary"
          className="multiline-ellipsis"
        >
          {name}
        </BodyCopy>
      </span>
      {children}
    </div>
  ) : (
    <div className="product-title-container">
      <Anchor
        handleLinkClick={(e) => {
          googleAnalyticsData();
          productLink(
            loadedProductCount,
            { to: pdpPageUrl, asPath: pdpAsUrl },
            e,
            alternateBrand,
            isSearchListing
          );
        }}
        inheritedStyles="product-title-content"
        to={filteredPdpToPath}
        asPath={pdpAsUrl}
        noLink
      >
        {renderBrandTitle(isShowBrandNameEnabled, tcpBrand, gymBrand, primaryBrand)}
        <BodyCopy
          fontSize={['fs12', 'fs13', 'fs14']}
          fontFamily="secondary"
          className="multiline-ellipsis"
        >
          {name}
        </BodyCopy>
      </Anchor>
      {children}
    </div>
  );
}

const isBundleorABTestSwitchOn = (bundleProduct, showPriceRange) =>
  !bundleProduct && !showPriceRange;

/* NOTE: This issue (DT-28867) added isMobile condition. */
/* NOTE: As per DT-29548, isMobile condition is not valid. "Offer" price should be shown below "List" price (always) */
/* NOTE: DT-27216, if offerPrice and listPrice are the same, just offerPrice should be shown (and will be black) */
export function ProductPricesSection(props) {
  const { listPrice, offerPrice, merchantTag, bundleProduct, priceRange, showPriceRange } = props;

  const highListPrice = priceRange && priceRange.highListPrice;
  const highOfferPrice = priceRange && priceRange.highOfferPrice;
  const lowListPrice = priceRange && priceRange.lowListPrice;
  const lowOfferPrice = priceRange && priceRange.lowOfferPrice;
  return (
    <>
      {isBundleorABTestSwitchOn(bundleProduct, showPriceRange) ? (
        <div className="container-price">
          {offerPrice && (
            <>
              <BodyCopy
                dataLocator={getLocator('global_Price_text')}
                color={isGymboree() ? 'gray.900' : 'red.500'}
                fontWeight="extrabold"
                fontFamily="secondary"
                fontSize={['fs15', 'fs18', 'fs20']}
              >
                <PriceCurrency price={offerPrice} />
              </BodyCopy>
              {offerPrice !== listPrice && (
                <BodyCopy
                  component="span"
                  color="gray.700"
                  fontFamily="secondary"
                  fontWeight="semibold"
                  fontSize={['fs10', 'fs12', 'fs14']}
                  className="list-price"
                >
                  <PriceCurrency price={listPrice} />
                </BodyCopy>
              )}
            </>
          )}

          {merchantTag && (
            <BodyCopy
              component="span"
              color="red.500"
              fontFamily="secondary"
              fontWeight="semibold"
              className="merchant-tag elem-ml-XXXS"
              fontSize={['fs10', 'fs12', 'fs14']}
            >
              {merchantTag}
            </BodyCopy>
          )}
        </div>
      ) : (
        BundlePriceSection(highListPrice, highOfferPrice, lowListPrice, lowOfferPrice, merchantTag)
      )}
    </>
  );
}

/**
 * This Component Will be used to render inline search image svg in condensed header.
 * @param {*} classNamePrefix : this will be used as class name to change the color
 */
export const FavoritesIcons = ({ isSolidIcon }) => {
  if (isSolidIcon) {
    return (
      <span // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: `
         <svg class="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27 27">
         <defs>
            <style>
            .solid-favorite-1{stroke:#000;stroke-linecap:round;stroke-linejoin:round;}
          </style>
         </defs>
         <title>CHILDRENS PLACE-27X27</title>
           <path class="solid-favorite-1" d="M24.67,12.78A5.54,5.54,0,0,0,24.57,5h0a5.53,5.53,0,0,0-7.84,0L14.47,7.3a1.38,1.38,0,0,1-1.94,0L10.27,5A5.53,5.53,0,0,0,2.43,5h0a5.54,5.54,0,0,0-.1,7.74l10,10.33a1.59,1.59,0,0,0,2.28,0Z"/>
         </svg>`,
        }}
      />
    );
  }
  return (
    <span // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: `
        <svg class="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27 27">
        <defs>
          <style>.empty-favorite-1{fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;}
          </style>
        </defs>
        <title>CHILDRENS PLACE-27X27</title>
          <path class="empty-favorite-1" d="M24.67,12.78A5.54,5.54,0,0,0,24.57,5h0a5.53,5.53,0,0,0-7.84,0L14.47,7.3a1.38,1.38,0,0,1-1.94,0L10.27,5A5.53,5.53,0,0,0,2.43,5h0a5.54,5.54,0,0,0-.1,7.74l10,10.33a1.59,1.59,0,0,0,2.28,0Z"/>
          <path class="empty-favorite-1" d="M2.92,8.18A2.66,2.66,0,0,1,5.58,5.52"/>
        </svg>`,
      }}
    />
  );
};
FavoritesIcons.propTypes = {
  isSolidIcon: PropTypes.bool.isRequired,
};

export class ProductWishlistIcon extends ServerToClientRenderPatch {
  render() {
    const { onClick, isRemove, isDisabled, isMobile, className, activeButton, favoritedCount } =
      this.props;
    const removeTextHeader = isMobile ? 'Tap to Remove' : 'Click to Remove';
    const removeTxtDesc = isMobile
      ? 'Remove this item from your Favorites List by tapping the heart icon again.'
      : 'Remove this item from your Favorites List by clicking the heart icon again.';

    return (
      <BodyCopy
        component="div"
        role="button"
        className="fav-icon-wrapper"
        onClick={onClick}
        isDisabled={isDisabled}
      >
        {isRemove ? (
          <div className="information-remove">
            <p className="information-remove-message">
              <strong className="remove-title">{removeTextHeader}</strong>
              <br />
              {removeTxtDesc}
            </p>
          </div>
        ) : (
          <>
            <button className="clear-button">
              <span
                data-locator={getLocator('global_favorite_button')}
                title="addToFavorite"
                className={activeButton ? `${className} active` : className}
              >
                <FavoritesIcons isSolidIcon={activeButton} />
              </span>
            </button>
            <BodyCopy
              dataLocator="pdp_favorite_icon_count"
              className="favorite-count"
              fontSize="fs10"
              fontWeight="regular"
              color="gray.600"
            >
              {favoritedCount}
            </BodyCopy>
          </>
        )}
      </BodyCopy>
    );
  }
}

export function BadgeItem(props) {
  const { text, className, isShowBadges, customFontWeight, customFontSize } = props;

  return (
    <div className={className}>
      <BodyCopy
        dataLocator={getLocator('global_productbadge_txt')}
        fontFamily="secondary"
        fontWeight={customFontWeight || 'semibold'}
        fontSize={customFontSize}
      >
        {isShowBadges && text}
      </BodyCopy>
    </div>
  );
}

export function PromotionalMessage(props) {
  const { text, isInternationalShipping } = props;
  return text && !isInternationalShipping && !isCanada() ? (
    <BodyCopy
      fontSize={['fs9', 'fs12', 'fs14']}
      fontWeight="extrabold"
      fontFamily="secondary"
      data-locator={getLocator('global_loyalty_text')}
      className="loyalty-text-container"
      component="div"
    >
      {text && (
        <RichText
          className="rewards__benefits multiline-ellipsis"
          richTextHtml={getFormattedLoyaltyText(text)}
        />
      )}
    </BodyCopy>
  ) : null;
}

const renderWishListItem = (item, labels, activeWishListId) => (
  <div className="wish-list-item-section">
    <p className="wish-list-name">
      <span className={`${item.id === activeWishListId ? 'default-list' : ''}`}>
        {item.id === activeWishListId && (
          <Image
            src={getIconPath('selected-item-check-no-circle')}
            alt="check mark"
            className="wish-list-tick-mark icon-small"
            data-locator="wish-list-check-icon"
            height="20px"
          />
        )}
      </span>
      <span className={`${item.id === activeWishListId ? 'default-list-item' : 'other-list-item'}`}>
        {item.displayName}
      </span>
    </p>
    <p className="wish-list-count-section">
      <span
        className={`${
          item.id === activeWishListId ? 'default-list-count wish-list-count' : 'wish-list-count'
        }`}
      >
        {item.itemsCount}
      </span>
      <span>{` ${labels.lbl_fav_items}`}</span>
    </p>
  </div>
);

const moveToListErrorDisplay = (error) => {
  return error ? (
    <Notification status="error" colSize={{ large: 12, medium: 8, small: 6 }} message={error} />
  ) : null;
};

export const CreateWishList = (props) => {
  const {
    labels,
    wishlistsSummaries,
    // createNewWishList,
    createNewWishListMoveItem,
    itemId,
    getActiveWishlist,
    activeWishListId,
    openAddNewList,
    favoriteErrorMessages,
  } = props;
  const activateCreateButton = (wishlistsSummaries && wishlistsSummaries.length === 5) || false;
  const errorMsg = favoriteErrorMessages
    ? get(favoriteErrorMessages[itemId], 'errorMessage', null)
    : '';
  return (
    <div className="create-wish-list-section">
      <h4 className="create-wish-list-header" dataLocator={getLocator('fav_list_lbl')}>
        {labels.lbl_fav_myFavWishList}
      </h4>
      <ul>
        {wishlistsSummaries.map((item) => (
          <li className="wish-list-item">
            {createNewWishListMoveItem ? (
              <Button
                onClick={() => createNewWishListMoveItem({ wisListId: item.id, itemId })}
                className="wish-list-item__button"
              >
                {renderWishListItem(item, labels, activeWishListId)}
              </Button>
            ) : (
              <Button
                onClick={() => getActiveWishlist(item.id)}
                className="wish-list-change-item__button"
              >
                {renderWishListItem(item, labels, activeWishListId)}
              </Button>
            )}
          </li>
        ))}
      </ul>
      {moveToListErrorDisplay(errorMsg)}
      <Button
        onClick={() => openAddNewList(itemId)}
        buttonVariation="fixed-width"
        fill="BLACK"
        data-locator="create-new-wish-list"
        className="create-new__button"
        disabled={activateCreateButton}
      >
        {labels.lbl_fav_createNewList}
      </Button>
    </div>
  );
};

export const ProductSKUInfo = (props) => {
  const { size, fit } = props;

  if (!size && !fit) {
    return null;
  }

  return (
    <div className="product-sku-info-container">
      {size && <span className="size-container">{`Size ${size}`}</span>}
      {size && fit && <span className="separator-bar-icon"> | </span>}
      {fit && <span className="fit-container">{fit}</span>}
    </div>
  );
};

export const PurchaseSection = (quantity, labels, quantityPurchased) =>
  !!quantity && (
    <div className="purchase-section">
      <span>
        {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
        {quantityPurchased}/{quantity}
      </span>
      <span className="is-purchase-label">{labels.lbl_fav_purchased}</span>
    </div>
  );

export const WishListIcon = (
  isFavoriteView,
  isInDefaultWishlist,
  handleAddRemoveFromWishlistObj,
  itemNotAvailable,
  favoritedCount,
  isSuggestedItem,
  labelsPlpTiles
) => {
  if (itemNotAvailable && !isSuggestedItem) {
    return null;
  }

  return (
    <Col colSize={{ small: 2, medium: 2, large: 2 }}>
      <ProductWishlistIcon
        onClick={
          isInDefaultWishlist
            ? handleAddRemoveFromWishlistObj.handleRemoveFromWishList
            : handleAddRemoveFromWishlistObj.handleAddToWishlist
        }
        activeButton={isInDefaultWishlist || isFavoriteView}
        favoritedCount={favoritedCount}
        className="fav-icon"
        labelsPlpTiles={labelsPlpTiles}
      />
    </Col>
  );
};

export const EditButton = (props, selectedColorProductId, itemNotAvailable) => {
  if (itemNotAvailable) {
    return null;
  }
  const { isFavoriteView, labels, onQuickViewOpenClick, item } = props;
  if (isFavoriteView) {
    const {
      skuInfo: { skuId, size, fit, color },
    } = item;
    const { itemId, quantity, brand } = item.itemInfo;
    return (
      <Anchor
        className="edit-fav-item__button"
        handleLinkClick={(event) => {
          event.preventDefault();
          onQuickViewOpenClick({
            colorProductId: selectedColorProductId,
            orderInfo: {
              orderItemId: itemId,
              selectedQty: quantity,
              selectedColor: color.name,
              selectedSize: size || '',
              selectedFit: fit || '',
              skuId,
              itemBrand: (brand || '').toUpperCase(),
            },
            isFavoriteEdit: true,
          });
        }}
        noLink
      >
        {labels.lbl_fav_edit}
      </Anchor>
    );
  }
  return null;
};

EditButton.propTypes = {
  isFavoriteView: PropTypes.bool,
  labels: PropTypes.shape({}),
  onQuickViewOpenClick: PropTypes.func,
  item: PropTypes.shape({}),
};

EditButton.defaultProps = {
  isFavoriteView: false,
  labels: {},
  onQuickViewOpenClick: () => {},
  item: {},
};

ProductSKUInfo.propTypes = {
  size: PropTypes.string,
  fit: PropTypes.string,
};

ProductSKUInfo.defaultProps = {
  size: '',
  fit: '',
};

CreateWishList.propTypes = {
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])).isRequired,
  wishlistsSummaries: PropTypes.arrayOf({}).isRequired,
  // createNewWishList: PropTypes.func.isRequired,
  openAddNewList: PropTypes.func.isRequired,
  createNewWishListMoveItem: PropTypes.func.isRequired,
  itemId: PropTypes.string.isRequired,
  getActiveWishlist: PropTypes.func,
  activeWishListId: PropTypes.string.isRequired,
  favoriteErrorMessages: PropTypes.shape({}).isRequired,
};

CreateWishList.defaultProps = {
  getActiveWishlist: () => {},
};

PromotionalMessage.propTypes = {
  text: PropTypes.string.isRequired,
  isInternationalShipping: PropTypes.bool.isRequired,
};

BadgeItem.defaultProps = {
  text: '',
  className: '',
  customFontWeight: '',
  isShowBadges: true,
  customFontSize: ['fs10', 'fs12', 'fs14'],
};

BadgeItem.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  isShowBadges: PropTypes.bool,
  customFontWeight: PropTypes.string,
  customFontSize: PropTypes.arrayOf(PropTypes.string),
};

ProductPricesSection.defaultProps = {
  listPrice: 0,
  offerPrice: 0,
  merchantTag: '',
  bundleProduct: false,
  priceRange: {},
  showPriceRange: false,
};

ProductPricesSection.propTypes = {
  listPrice: PropTypes.number,
  offerPrice: PropTypes.number,
  merchantTag: PropTypes.string,
  bundleProduct: PropTypes.bool,
  priceRange: PropTypes.shape({}),
  showPriceRange: PropTypes.bool,
};
