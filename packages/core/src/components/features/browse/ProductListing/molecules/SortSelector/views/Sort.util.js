// 9fbef606107a605d69c0edbcd8029e5d 
const getSortOptions = sortLabels => {
  return [
    { displayName: sortLabels.lbl_recommended, id: '', isSelected: false }, // Default, no sort param required.
    {
      displayName: sortLabels.lbl_min_offer_price_desc,
      id: 'min_offer_price desc',
      isSelected: false,
    },
    {
      displayName: sortLabels.lbl_min_offer_price_asc,
      id: 'min_offer_price asc',
      isSelected: false,
    },
    { displayName: sortLabels.lbl_newest_score, id: 'newest_score desc', isSelected: false },
    { displayName: sortLabels.lbl_favoritedcount, id: 'favoritedcount desc', isSelected: false },
    {
      displayName: sortLabels.lbl_TCPBazaarVoiceRating,
      id: 'TCPBazaarVoiceRating desc',
      isSelected: false,
    },
  ];
};

export default getSortOptions;
