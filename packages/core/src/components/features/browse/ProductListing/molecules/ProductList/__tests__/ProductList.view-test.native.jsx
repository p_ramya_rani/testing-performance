// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ProductListVanilla } from '../views/ProductList.view.native';

describe('ProductList component', () => {
  it('should renders correctly', () => {
    const getScreenProps = jest
      .fn()
      .mockImplementation(() => Promise.resolve({ deviceTier: 'tier1' }));
    const navigation = {
      getScreenProps,
    };
    const props = {
      setListRef: jest.fn(),
      products: [{ productInfo: '' }],
      showQuickViewForProductId: '',
      currencySymbol: '',
      onAddItemToFavorites: jest.fn(),
      onQuickViewOpenClick: jest.fn(),
      onPickUpOpenClick: jest.fn(),
      onColorChange: jest.fn(),
      isBopisEnabled: false,
      unbxdId: '',
      onProductCardHover: jest.fn(),
      isBopisEnabledForClearance: false,
      onQuickBopisOpenClick: jest.fn(),
      currencyExchange: [],
      siblingProperties: '',
      loadedProductCount: 12,
      onGoToPDPPage: jest.fn(),
      title: '',
      onLoadMoreProducts: jest.fn(),
      onRenderHeader: jest.fn(),
      isFavorite: false,
      setLastDeletedItemId: jest.fn(),
      navigation,
    };
    const component = shallow(<ProductListVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders favorite icon and onFavoriteRemove', () => {
    const getScreenProps = jest
      .fn()
      .mockImplementation(() => Promise.resolve({ deviceTier: 'tier1' }));
    const navigation = {
      getScreenProps,
    };
    const props = {
      onFavoriteRemove: jest.fn(),
      currentProduct: {
        generalProductId: 1220912,
      },
      currentColorEntry: {
        colorProductId: 123456,
      },
      isLoggedIn: false,
      defaultWishListFromState: [
        {
          externalIdentifier: 122011,
          giftListItemID: 1111000,
        },
      ],
      navigation,
    };
    const component = shallow(<ProductListVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should pass dynamic badge kill switch', () => {
    const getScreenProps = jest
      .fn()
      .mockImplementation(() => Promise.resolve({ deviceTier: 'tier1' }));
    const navigation = {
      getScreenProps,
    };
    const props1 = {
      onFavoriteRemove: jest.fn(),
      currentProduct: {
        generalProductId: 1220912,
      },
      currentColorEntry: {
        colorProductId: 123456,
      },
      isLoggedIn: false,
      isDynamicBadgeEnabled: true,
      defaultWishListFromState: [
        {
          externalIdentifier: 122011,
          giftListItemID: 1111000,
        },
      ],
      navigation,
    };
    const component = shallow(<ProductListVanilla {...props1} />);
    expect(component).toMatchSnapshot();
  });
});
