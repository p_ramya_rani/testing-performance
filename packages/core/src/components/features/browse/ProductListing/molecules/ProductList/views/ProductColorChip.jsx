// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import Constants from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/views/ProductChip.constants';
import { getAPIConfig, getLocator, removeExtension } from '@tcp/core/src/utils';

export default class ProductColorChip extends React.Component {
  static propTypes = {
    /**
     * Callback for clicks on color chips. Accepts colorProductId, colorName.
     * Note that it is up to this callback to update the selectedColorId prop of this component.
     */
    /** the color name of the currently selected chip */
    isActive: PropTypes.bool.isRequired,
    onChipClick: PropTypes.func.isRequired,

    /** map of available colors to render chips for */
    colorEntry: PropTypes.shape().isRequired,
    imagesByColor: PropTypes.shape({}).isRequired,
    isFavoriteView: PropTypes.bool,
    swatchImage: PropTypes.string,
    isTier1: PropTypes.bool,
  };

  static defaultProps = {
    isFavoriteView: false,
    swatchImage: '',
    isTier1: true,
  };

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = () => {
    const {
      colorEntry: {
        colorProductId,
        color: { name },
        miscInfo,
      },
      onChipClick,
    } = this.props;
    return onChipClick(colorProductId, name, miscInfo);
  };

  getColorImageUrlByName = () => {
    const {
      colorEntry: {
        color: { imagePath },
      },
    } = this.props;

    return imagePath;
  };

  render() {
    const {
      colorEntry: {
        color: { name, imagePath: imageUrl },
      },
      isActive,
      isFavoriteView,
      isTier1,
      swatchImage: swatchImageUrl,
      primaryBrand,
      alternateBrand,
    } = this.props;
    const apiConfig = getAPIConfig();
    let { brandId } = apiConfig;
    const imagePath = removeExtension(imageUrl);
    let swatchImage = removeExtension(swatchImageUrl);
    if (primaryBrand) {
      brandId = primaryBrand;
    }
    if (alternateBrand) {
      brandId = alternateBrand;
    }
    const assetPrefix = apiConfig[`productAssetPath${brandId.toUpperCase()}`];
    const assetHost = apiConfig[`assetHost${brandId.toUpperCase()}`];
    if (isFavoriteView) {
      swatchImage = `${imagePath}_swatch`;
    }
    let formattedSwatchImage = swatchImage;
    if (swatchImage && swatchImage.indexOf('/') === -1) {
      formattedSwatchImage = `${swatchImage.split('_')[0]}/${swatchImage}`;
    }
    const imgUrl = formattedSwatchImage;
    const imgConfig = 't_swatch_img,w_50,h_50,c_thumb,g_auto:0,f_auto,q_auto';
    return (
      <button
        data-locator={getLocator('global_ColorSwatch_Swatch_link')}
        type="button"
        onClick={isTier1 && this.handleClick}
        title={name}
        className={[
          `${
            name === Constants.COLOR_CHIP.PRODUCTS_WHITE ||
            name === Constants.COLOR_CHIP.PRODUCTS_SIMPLYWHT
              ? 'white-chip-border'
              : ''
          } content-colors-button`,
          isActive ? 'active' : null,
        ].join(' ')}
      >
        <img
          className="product-color-chip-image"
          src={`${assetHost}/${imgConfig}/${assetPrefix}/${imgUrl}`}
          alt={name}
        />
      </button>
    );
  }
}

export { ProductColorChip as ProductColorChipVanilla };
