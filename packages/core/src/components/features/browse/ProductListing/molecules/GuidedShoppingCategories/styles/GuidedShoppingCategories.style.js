// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  fieldset {
    border: 0;
    margin: ${props => props.theme.spacing.ELEM_SPACING.MED} 0 0;
    padding: 0;

    & > div {
      @media ${props => props.theme.mediaQuery.medium} {
        justify-content: center;
      }
    }
  }
  .category-checkbox {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    font-size: ${props => props.theme.typography.fontSizes.fs22};
    font-weight: 600;
    @media ${props => props.theme.mediaQuery.small} {
      width: 50%;
    }
    @media ${props => props.theme.mediaQuery.smallOnly} {
      margin-bottom: 15px;
      input {
        height: 18px;
        width: 18px;
      }
      input::after {
        left: 7px !important;
        top: 2px !important;
      }
      label {
        margin-top: -4px;
      }
    }
    @media ${props => props.theme.mediaQuery.medium} {
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.XL};
      width: auto;
      &:last-child {
        margin-right: 0;
      }
    }
  }
  .text-category-size {
    font-style: italic;
  }
  button {
    width: 164px;
    height: ${props => props.theme.spacing.ELEM_SPACING.XL};
    @media ${props => props.theme.mediaQuery.medium} {
      width: 188px;
      height: 51px;
    }
  }
`;

export default styles;
