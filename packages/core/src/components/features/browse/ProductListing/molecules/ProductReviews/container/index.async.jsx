// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import dynamic from 'next/dynamic';
import React from 'react';

import withLazyLoad from '@tcp/core/src/components/common/hoc/withLazyLoad';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';

const ProductReviewsContainer = dynamic(
  () => import(/* webpackChunkName: "ProductReviews" */ './ProductReviews.container'),
  { loading: () => <LoaderSkelton height="100px" /> }
);
export default withLazyLoad(ProductReviewsContainer);
