// 9fbef606107a605d69c0edbcd8029e5d
const initialNumToRender = 7;
const maxToRenderPerBatch = 2;
const imgWidth = 14;
const isNewRedesignWidth = 13;
const seperatorMarginLeft = 8;
const initialNumOfColorOnPDP = 5;
const indexForPlus = 4;
const newDesignSeparatorMarginLeft = 1;
const indexForPlusRange = 1;
const initialNumOfColorOnPDPColorRange = 2;
export default {
  initialNumToRender,
  maxToRenderPerBatch,
  imgWidth,
  seperatorMarginLeft,
  initialNumOfColorOnPDP,
  indexForPlus,
  isNewRedesignWidth,
  newDesignSeparatorMarginLeft,
  indexForPlusRange,
  initialNumOfColorOnPDPColorRange,
};
