// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import throttle from 'lodash/throttle';
import isEqual from 'lodash/isEqual';
import ProductList from '../../ProductList/views';
import { isClient } from '../../../../../../../utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import ProductsGridStyles from '../ProductsGrid.style';

// hardcoded value to load products before the end of the products list (400 is about the height of 1 row)
const NEXT_PAGE_LOAD_OFFSET = 400;

function findElementPosition(objArg) {
  let obj = objArg;
  let curleft = 0;
  let curtop = 0;

  do {
    curleft += obj.offsetLeft;
    curtop += obj.offsetTop;
    obj = obj.offsetParent;
  } while (obj);

  return {
    left: curleft,
    top: curtop,
  };
}

class ProductsGrid extends React.Component {
  static propTypes = {
    isLoadingMore: PropTypes.bool,
    productsBlock: PropTypes.arrayOf(PropTypes.shape({})),
    getMoreProducts: PropTypes.func.isRequired,
    onPickUpOpenClick: PropTypes.func,
    onQuickViewOpenClick: PropTypes.func,
    isGridView: PropTypes.bool,
    className: PropTypes.string,
    labels: PropTypes.string,
    labelsPlpTiles: PropTypes.shape({}),
    navigation: PropTypes.shape({}).isRequired,
    router: PropTypes.shape({}).isRequired,
    routerParam: PropTypes.shape({}).isRequired,
    productTileVariation: PropTypes.string,
    currency: PropTypes.string,
    currencyAttributes: PropTypes.shape({}).isRequired,
    onAddItemToFavorites: PropTypes.func.isRequired,
    totalProductsCount: PropTypes.number.isRequired,
    loadedProductCount: PropTypes.number.isRequired,
    isLoggedIn: PropTypes.bool,
    isSearchListing: PropTypes.bool,
    plpGridPromos: PropTypes.shape({}),
    plpHorizontalPromos: PropTypes.shape({}),
    // showQuickViewForProductId: PropTypes.string,
    getProducts: PropTypes.func,
    asPathVal: PropTypes.string,
    AddToFavoriteErrorMsg: PropTypes.string,
    removeAddToFavoritesErrorMsg: PropTypes.func,
    openAddNewList: PropTypes.func,
    activeWishListId: PropTypes.number,
    addToBagEcom: PropTypes.func,
    isFavoriteView: PropTypes.bool,
    removeFavItem: PropTypes.func.isRequired,
    pageNameProp: PropTypes.string,
    pageSectionProp: PropTypes.string,
    pageSubSectionProp: PropTypes.string,
    onSeeSuggestedItems: PropTypes.func,
    isSearchPage: PropTypes.string,
    favoriteErrorMessages: PropTypes.shape({}),
    page: PropTypes.string,
    wishList: PropTypes.shape({}).isRequired,
    isHidePLPRatings: PropTypes.bool,
    suggestedProductErrors: PropTypes.shape({}).isRequired,
    hasSuggestedProduct: PropTypes.shape({}).isRequired,
    cookieToken: PropTypes.string,
    onSetLastDeletedItemIdAction: PropTypes.func,
    deleteFavItemInProgressFlag: PropTypes.bool,
    isDynamicBadgeEnabled: PropTypes.shape({}),
    isPlpGridEnabled: PropTypes.bool,
    isPromoApplied: PropTypes.bool,
    breadCrumbs: PropTypes.shape({}),
    isShowBrandNameEnabled: PropTypes.bool,
  };

  static defaultProps = {
    isLoadingMore: false,
    productsBlock: [],
    onPickUpOpenClick: null,
    onQuickViewOpenClick: null,
    isGridView: false,
    className: '',
    labels: '',
    labelsPlpTiles: {},
    productTileVariation: '',
    currency: 'USD',
    isLoggedIn: false,
    isSearchListing: false,
    plpGridPromos: {},
    plpHorizontalPromos: {},
    getProducts: () => {},
    asPathVal: '',
    AddToFavoriteErrorMsg: '',
    removeAddToFavoritesErrorMsg: () => {},
    openAddNewList: () => {},
    activeWishListId: '',
    addToBagEcom: () => {},
    isFavoriteView: false,
    pageNameProp: '',
    pageSectionProp: '',
    pageSubSectionProp: '',
    onSeeSuggestedItems: () => {},
    isSearchPage: 'false',
    favoriteErrorMessages: {},
    isHidePLPRatings: false,
    page: '',
    cookieToken: null,
    onSetLastDeletedItemIdAction: () => {},
    deleteFavItemInProgressFlag: false,
    isDynamicBadgeEnabled: false,
    isPlpGridEnabled: false,
    isPromoApplied: false,
    breadCrumbs: [],
    isShowBrandNameEnabled: false,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      // TODO - fix this - This would be used when integrating BOSS/ BOPIS
      // eslint-disable-next-line
      bopisAutoSkipStep1: true,
    };
    this.isLoadingMoreState = false;
    this.captureContainerDivRef = (ref) => {
      this.containerDivRef = ref;
    };
    this.handleLoadNextPage = this.handleLoadNextPage.bind(this);
    const throttleTime = 100;
    const throttleParam = { trailing: true, leading: true };
    this.loadNextPageThrottle = throttle(this.handleLoadNextPage, throttleTime, throttleParam);
  }

  componentDidMount() {
    this.addRemoveScrollListeners('scroll');
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate(prevProps) {
    const { isLoadingMore: isOldLoadingMore } = prevProps;
    const { isLoadingMore } = this.props;
    if (!isLoadingMore && isOldLoadingMore !== isLoadingMore) {
      const timer = setTimeout(() => {
        // This hack is required to let the newly feteched products get rendered and then enable the flag
        // else the call to fetch product is going in infinite loop - RWD-14751
        this.isLoadingMoreState = isLoadingMore;
        clearTimeout(timer);
      }, 10);
    }
  }

  componentWillUnmount() {
    if (isClient()) {
      this.addRemoveScrollListeners('scroll', false);
    }
    const { isFavoriteView, removeAddToFavoritesErrorMsg } = this.props;
    if (typeof removeAddToFavoritesErrorMsg === 'function') {
      removeAddToFavoritesErrorMsg('');
    }
    if (isFavoriteView && isClient()) {
      const headingElement = document.querySelector('.heading-wrapper');
      const condensedHeaderElement = document.querySelector('.show-condensed-header');

      if (headingElement && condensedHeaderElement) {
        const yPos =
          headingElement.getBoundingClientRect().top +
          window.pageYOffset -
          condensedHeaderElement.getBoundingClientRect().height;
        window.scrollTo({
          top: yPos,
          behavior: 'smooth',
        });
      }
    }
  }

  getOffsetY = () => {
    const offsetY =
      findElementPosition(this.containerDivRef).top + this.containerDivRef.offsetHeight;
    return offsetY > 2500 ? offsetY - 2500 : 0;
  };

  addRemoveScrollListeners(eventName, isAddEvent = true) {
    document[isAddEvent ? 'addEventListener' : 'removeEventListener'](
      eventName,
      this.loadNextPageThrottle
    );
  }

  handleLoadNextPage() {
    const {
      productsBlock,
      getMoreProducts,
      totalProductsCount,
      loadedProductCount,
      navigation = {},
      routerParam = {},
      router = {},
    } = this.props;
    if (!this.isLoadingMoreState && this.containerDivRef && productsBlock.length) {
      const offsetY = this.getOffsetY();
      if (
        window.pageYOffset + window.innerHeight + NEXT_PAGE_LOAD_OFFSET > offsetY &&
        getMoreProducts &&
        totalProductsCount >= loadedProductCount
      ) {
        this.isLoadingMoreState = true;
        const { asPath = '' } = routerParam || router;
        const url = (navigation.getParam && navigation.getParam('url')) || asPath;
        getMoreProducts({ url });
      }
    }
  }

  render() {
    const {
      isGridView,
      productsBlock,
      className,
      labels,
      labelsPlpTiles,
      isFavoriteView,
      isLoadingMore,
      onPickUpOpenClick,
      onQuickViewOpenClick,
      productTileVariation,
      currency,
      currencyAttributes,
      onAddItemToFavorites,
      isLoggedIn,
      isSearchListing,
      plpGridPromos,
      plpHorizontalPromos,
      // showQuickViewForProductId,
      getProducts,
      asPathVal,
      AddToFavoriteErrorMsg,
      removeFavItem,
      openAddNewList,
      activeWishListId,
      pageNameProp,
      pageSectionProp,
      pageSubSectionProp,
      onSeeSuggestedItems,
      isSearchPage,
      page,
      wishList,
      cookieToken,
      isHidePLPRatings,
      suggestedProductErrors,
      hasSuggestedProduct,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      isPlpGridEnabled,
      isPromoApplied,
      breadCrumbs,
      isSecureImageFlagEnabled,
      alternateBrand,
      isShowBrandNameEnabled,
      isBopisFilterOn,
      ...otherProps
    } = this.props;

    const classN = className + (isPlpGridEnabled ? ' plp-grid-recos-enabled' : '');
    const containerClassName = `${className} main-section-container `;
    let categoryHeading =
      productsBlock[0] &&
      productsBlock[0][0] &&
      productsBlock[0][0].miscInfo &&
      productsBlock[0][0].miscInfo.categoryName;
    let categoryHeadingChanged = false;
    /* eslint-disable react/no-array-index-key */
    return (
      <main className={containerClassName}>
        <section
          ref={this.captureContainerDivRef}
          className={`products-grid-container ${isPlpGridEnabled ? 'plp-grid-recos-enabled' : ''} ${
            isGridView ? 'product-grid-view-container' : ''
          }`}
        >
          <div className="product-grid-content">
            <div className="products-listing-grid">
              <div className="product-grid-block-container">
                {!!productsBlock.length &&
                  productsBlock.map((block, index) => {
                    const nextBlock =
                      index < productsBlock.length - 1 ? productsBlock[index + 1] : false;
                    const isPerfectBlock = nextBlock
                      ? block.length === 21 && typeof nextBlock[0] === 'string'
                      : false;
                    if (
                      block[0] &&
                      block[0].miscInfo &&
                      block[0].miscInfo.categoryName &&
                      categoryHeading !== block[0].miscInfo.categoryName
                    ) {
                      categoryHeading = block[0].miscInfo.categoryName;
                      categoryHeadingChanged = true;
                    } else {
                      categoryHeadingChanged = false;
                    }
                    return (
                      <ProductList
                        key={`block_${index}`}
                        currentBlockIndex={index}
                        categoryHeadingChanged={categoryHeadingChanged}
                        isPromoApplied={isPromoApplied}
                        isPerfectBlock={isPerfectBlock}
                        productsBlock={block}
                        onPickUpOpenClick={onPickUpOpenClick}
                        className={`${classN} product-list`}
                        labels={labels}
                        labelsPlpTiles={labelsPlpTiles}
                        isFavoriteView={isFavoriteView}
                        onQuickViewOpenClick={onQuickViewOpenClick}
                        productTileVariation={productTileVariation}
                        currency={currency}
                        currencyAttributes={currencyAttributes}
                        isLoggedIn={isLoggedIn}
                        onAddItemToFavorites={onAddItemToFavorites}
                        // showQuickViewForProductId={showQuickViewForProductId}
                        isSearchListing={isSearchListing}
                        plpGridPromos={plpGridPromos}
                        plpHorizontalPromos={plpHorizontalPromos}
                        getProducts={getProducts}
                        asPathVal={asPathVal}
                        AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                        removeFavItem={removeFavItem}
                        openAddNewList={openAddNewList}
                        activeWishListId={activeWishListId}
                        pageNameProp={pageNameProp}
                        pageSectionProp={pageNameProp}
                        pageSubSectionProp={pageNameProp}
                        onSeeSuggestedItems={onSeeSuggestedItems}
                        isSearchPage={isSearchPage}
                        page={page}
                        wishList={wishList}
                        cookieToken={cookieToken}
                        suggestedProductErrors={suggestedProductErrors}
                        hasSuggestedProduct={hasSuggestedProduct}
                        isHidePLPRatings={isHidePLPRatings}
                        onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                        breadCrumbs={breadCrumbs}
                        isSecureImageFlagEnabled={isSecureImageFlagEnabled}
                        alternateBrand={alternateBrand}
                        isShowBrandNameEnabled={isShowBrandNameEnabled}
                        isBopisFilterOn={isBopisFilterOn}
                        {...otherProps}
                      />
                    );
                  })}
              </div>
            </div>
          </div>
        </section>
      </main>
    );
  }
}

export default withStyles(ProductsGrid, ProductsGridStyles);
