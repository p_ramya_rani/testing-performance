// 9fbef606107a605d69c0edbcd8029e5d 
import React, { Component } from 'react';
import { View } from 'react-native';
import { Field } from 'redux-form';
import { PropTypes } from 'prop-types';

import Button from '../../../../../../common/atoms/Button';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';

import {
  StyledCheckboxContainer,
  StyledCheckbox,
  ButtonWrapper,
} from '../styles/GuidedShoppingCategories.style.native';

import BodyCopy from '../../../../../../common/atoms/BodyCopy';

class GuidedShoppingCategoriesForm extends Component {
  getOptions = () => {
    const { content, filters } = this.props;
    const { options = [] } = content;
    const cmsCategoriesFacets = options && [...options].map(a => a.type);
    const arr = [];
    filters.forEach(item => {
      const n = cmsCategoriesFacets.indexOf(item.displayName);
      if (arr[n]) {
        arr.splice(n, 0, { ...(item || {}), ...(options[n] || {}) });
      } else {
        arr.push({ ...(item || {}), ...(n ? options[n] : {}) });
      }
    });
    return arr;
  };

  multicheckbox = ({ input, options }) => {
    const { onChange, value: values = [] } = input;
    return options.map((option, index) => {
      const handleChange = (e, value) => {
        const arr = [...values];
        if (e && value && !arr.includes(value)) {
          arr.push(value);
        } else if (arr.includes(value)) {
          arr.splice(arr.indexOf(value), 1);
        }
        return onChange(arr);
      };
      const isChecked = values.includes(option.displayName);
      return (
        <StyledCheckbox>
          <InputCheckbox
            key={index.toString()}
            name={`categoryCheckbox_${index}`}
            id={`categoryCheckbox_${index}`}
            className="category-checkbox"
            isChecked={isChecked}
            execOnChangeByDefault={false}
            input={{
              onChange: e => handleChange(e, option.displayName),
              name: `categoryCheckbox_${index}`,
            }}
            checkBoxLabel
            fontSize="fs12"
            fontWeight="bold"
          >
            {option.text || option.displayName}
          </InputCheckbox>
        </StyledCheckbox>
      );
    });
  };

  render() {
    const { filedName, content, onSubmit, labels } = this.props;
    const { header } = content;
    const options = this.getOptions();
    return (
      <View>
        <BodyCopy
          fontFamily="secondary"
          tag="div"
          textAlign="center"
          fontSize={['fs14', 'fs16', 'fs16']}
          text={header && header.title}
        />
        <StyledCheckboxContainer>
          <Field
            name={filedName}
            id="essential_categories"
            type="checkbox"
            options={options}
            component={this.multicheckbox}
          />
        </StyledCheckboxContainer>
        <ButtonWrapper>
          <Button
            type="button"
            width="164px"
            text={labels.next}
            onPress={onSubmit}
            noCurve
            fill="BLUE"
          />
        </ButtonWrapper>
      </View>
    );
  }
}

GuidedShoppingCategoriesForm.defaultProps = {};

GuidedShoppingCategoriesForm.propTypes = {
  filedName: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  content: PropTypes.shape({}).isRequired,
  filters: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
};

GuidedShoppingCategoriesForm.defaultProps = {};

export default GuidedShoppingCategoriesForm;

export { GuidedShoppingCategoriesForm as GuidedShoppingCategoriesVanilla };
