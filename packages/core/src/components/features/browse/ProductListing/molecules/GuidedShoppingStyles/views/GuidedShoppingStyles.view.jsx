// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { Field } from 'redux-form';
import { PropTypes } from 'prop-types';
import { Carousel } from '@tcp/core/src/components/common/molecules';
import { getIconPath, isValidObj } from '@tcp/core/src/utils';
import { Col, Button, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { parseColor } from '../../../organisms/EssentialShop/container/EssentialShop.utils';
import styles from '../styles/GuidedShoppingStyles.style';
import { CAROUSEL_OPTIONS } from '../../../organisms/EssentialShop/container/EssentialShop.constants';

const fallBackColor = parseColor('blue-800');
const fallbackImg = getIconPath('essential-style-fallback');
const fallbackSelectedImg = getIconPath('essential-style-fallback-selected');

class GuidedShoppingStyles extends PureComponent {
  /**
   * @method getImageUrl
   * @description Get the selected/unslected image to render either from DAM or default one for the facet
   */
  getImageUrl = (isChecked, defaultImage, selectedImage, displayName) => {
    const imgUrl = isChecked ? fallbackSelectedImg : fallbackImg;
    const isDamImage = isChecked
      ? isValidObj(selectedImage) && selectedImage.url
      : isValidObj(defaultImage) && defaultImage.url;

    return <Image src={isDamImage || imgUrl} alt={displayName} className="facet-img" />;
  };

  /**
   * @method multiCheckbox
   * @description Function to render the carousel consisting of all styles
   */
  multiCheckbox = ({ input, options, childComponent: CHILDCOMPONENT }) => {
    const { onChange, value: values = [] } = input;
    const IconPath = getIconPath('carousel-big-carrot');
    return (
      <Carousel
        name="carousel"
        options={CAROUSEL_OPTIONS}
        carouselConfig={{
          autoplay: false,
          variation: 'big-arrows',
          customArrowLeft: IconPath,
          customArrowRight: IconPath,
        }}
        carouselTheme="dark"
      >
        {options &&
          options.map((option, index) => {
            const {
              id = '',
              displayName = '',
              style = fallBackColor,
              defaultImage = {},
              selectedImage = {},
            } = option;
            const handleChange = (e, value) => {
              const arr = [...values];
              const { checked } = e.target;
              if (checked && value) {
                arr.push(value);
              } else {
                arr.splice(arr.indexOf(value), 1);
              }
              return onChange(arr);
            };
            const isChecked = values.includes(id);
            const selectedColor = isChecked ? { color: style } : {};

            return (
              <CHILDCOMPONENT
                className="style-checkbox"
                checked={isChecked}
                input={{ onChange: (e) => handleChange(e, id), name: `styleCheckbox_${index}` }}
              >
                {this.getImageUrl(isChecked, defaultImage, selectedImage, displayName)}
                <BodyCopy
                  fontSize="fs14"
                  fontFamily="secondary"
                  className="style-name"
                  customStyle={selectedColor}
                >
                  {displayName}
                </BodyCopy>
              </CHILDCOMPONENT>
            );
          })}
      </Carousel>
    );
  };

  /**
   * @method render
   * @description Main function to render the component
   */
  render() {
    const {
      className,
      setActiveStep,
      stepInfo: { facetName = '' },
      categoryList,
      labels,
      activeDotCount,
    } = this.props;
    return (
      <div className={className}>
        <div className="guided-shopping-style">
          <Field
            id="essential_styles"
            name={facetName}
            type="checkbox"
            options={categoryList}
            component={this.multiCheckbox}
            childComponent={InputCheckbox}
          />
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            {activeDotCount ? (
              <Button
                fill="WHITE"
                type="button"
                buttonVariation="variable-width"
                className="back-button"
                onClick={() => setActiveStep('dec')}
              >
                {labels.back}
              </Button>
            ) : null}
            <Button fill="BLUE" type="submit" buttonVariation="variable-width">
              {labels.next}
            </Button>
          </Col>
        </div>
      </div>
    );
  }
}

GuidedShoppingStyles.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}),
  categoryList: PropTypes.shape([]),
  setActiveStep: PropTypes.func.isRequired,
  activeDotCount: PropTypes.number,
  stepInfo: PropTypes.shape({}),
};

GuidedShoppingStyles.defaultProps = {
  className: '',
  labels: {},
  categoryList: [],
  activeDotCount: 0,
  stepInfo: {},
};

export default withStyles(GuidedShoppingStyles, styles);
export { GuidedShoppingStyles as GuidedShoppingStylesVanilla };
