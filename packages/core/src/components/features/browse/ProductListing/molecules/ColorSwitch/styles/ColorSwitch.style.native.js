// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';
import { DamImage } from '../../../../../../common/atoms';
import constants from '../ColorSwitch.constant';

const getAdditionalImageStyle = (props) => {
  const { isNewReDesignProductTile } = props;
  if (isNewReDesignProductTile)
    return `
     margin-left: 3px;
  `;
  return ``;
};

const getImageStyle = (props) => {
  const { selected, theme, colorName, isNewReDesignProductTile } = props;
  const { colorPalette } = theme;
  const borderColornew = colorName === 'white' ? colorPalette.gray[500] : colorPalette.gray[300];
  const borderWidth = colorName === 'white' ? 1 : 0;
  const size = isNewReDesignProductTile ? constants.isNewRedesignWidth : constants.imgWidth;
  const radius = 7.5;
  const width = selected && !isNewReDesignProductTile ? size - 3 : size;
  const height = selected && !isNewReDesignProductTile ? size - 3 : size;
  const borderRadius = selected ? radius + 1 : radius;
  return `
    width: ${width};
    height: ${height};
    border-radius: ${borderRadius};
    resize-mode: cover;
    border-color: ${borderColornew};
    border-width: ${borderWidth};
   ${getAdditionalImageStyle}
  `;
};

const adjustColorSwatches = (props) => {
  const { colorsMap, isNewReDesignProductTile } = props;
  if (!isNewReDesignProductTile) return ``;
  let right = 0;
  if (colorsMap >= 9) right = -10;
  return `
  right: ${right}
  top: -2px
  `;
};

const getImageBorderStyle = (props) => {
  const { theme, selected, isNewReDesignProductTile } = props;
  const { colorPalette } = theme;
  const borderColor = selected ? colorPalette.gray[900] : colorPalette.gray[600];
  const size = constants.imgWidth;
  const radius = 7.5;
  const width = selected ? size + 1 : size;
  const height = selected ? size + 1 : size;
  const borderRadius = selected ? radius + 1 : radius;
  const borderWidth = isNewReDesignProductTile ? 0 : 1;

  return `
    width: ${width};
    height: ${height};
    border-radius: ${borderRadius};
    border-color: ${borderColor};
    border-width: ${borderWidth};
    align-items: center;
    justify-content: center;
  `;
};

const ColorSwitchesContainer = styled.View`
  height: 17;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  position: relative;
  ${adjustColorSwatches}
  ${(props) => (props.isNextArrowVisible ? `padding-right:15px` : '')};
`;

const ImageTouchableOpacity = styled.TouchableOpacity`
  ${getImageBorderStyle}
`;

const ItemSeparatorStyle = styled.View`
  margin-left: ${constants.seperatorMarginLeft}px;
`;

const NewDesignItemSeparator = styled.View`
  margin-left: ${constants.newDesignSeparatorMarginLeft}px;
`;

const ImageStyle = styled(DamImage)`
  ${getImageStyle}
`;

const NextArrow = styled.TouchableOpacity`
  position: absolute;
  right: 0px;
  top: 0px;
  width: 15px;
  height: 15px;
`;

const ArrowIcon = styled.Image`
  width: 6px;
  height: 10px;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  /* stylelint-disable-next-line */
  tint-color: ${(props) => props.theme.colorPalette.gray[600]};
`;
const styles = css``;

export {
  styles,
  ImageTouchableOpacity,
  ColorSwitchesContainer,
  ItemSeparatorStyle,
  NextArrow,
  ImageStyle,
  ArrowIcon,
  NewDesignItemSeparator,
};
