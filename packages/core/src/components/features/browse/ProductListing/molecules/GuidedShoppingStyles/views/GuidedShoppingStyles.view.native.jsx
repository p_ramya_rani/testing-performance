// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Field } from 'redux-form';
import { PropTypes } from 'prop-types';
import { Carousel } from '@tcp/core/src/components/common/molecules';
import { Button, Image } from '@tcp/core/src/components/common/atoms';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getScreenWidth } from '@tcp/core/src/utils/utils.app';
import { isValidObj, isAbsoluteUrl } from '@tcp/core/src/utils';
import {
  getFacetList,
  parseColor,
} from '../../../organisms/EssentialShop/container/EssentialShop.utils';
import {
  CarouselWrapper,
  ButtonWrapper,
  StyleImageWrapper,
} from '../styles/GuidedShoppingStyles.style.native';

/**
 * Module height and width.
 * Height is fixed for mobile
 * Width can vary as per device width.
 * Here, 5.5 is the no of item to render on the screen
 * DO NOT UPDATE THE VALUES
 */
const MODULE_HEIGHT = 220;
const MODULE_WIDTH = getScreenWidth() - 60;
const horizontalMargin = 15;
const slideWidth = MODULE_WIDTH / 5.5;
const itemWidth = slideWidth + horizontalMargin;
const fallBackColor = parseColor('blue-800');
const fallbackImg = require('../../../../../../../../src/assets/fallback.png');
const fallbackSelectedImg = require('../../../../../../../../src/assets/fallback-selected.png');

class GuidedShoppingStyles extends PureComponent {
  /**
   * @method getImageUrl
   * @description Get the selected/unslected image to render either from DAM or default one for the facet
   */
  getImageUrl = (isChecked, defaultImage, selectedImage, displayName) => {
    let imgUrl = isChecked ? fallbackSelectedImg : fallbackImg;
    const isDamImage = isChecked
      ? isValidObj(selectedImage) && selectedImage.url
      : isValidObj(defaultImage) && defaultImage.url;
    imgUrl = isDamImage || imgUrl;
    const isAbsUrl = isAbsoluteUrl(imgUrl);

    return isAbsUrl ? (
      <Image url={imgUrl} alt={displayName} width="63" height="59" />
    ) : (
      <Image source={imgUrl} alt={displayName} width="63" height="59" />
    );
  };

  /**
   * @method handleChange
   * @description Handles the facet seletion on the style step
   */
  handleChange = (e, value, values, onChange) => {
    const arr = [...values];
    if (arr.includes(value)) {
      arr.splice(arr.indexOf(value), 1);
    } else {
      arr.push(value);
    }
    return onChange(arr);
  };

  /**
   * @method itemRenderer
   * @description Renders all the style facets in the Carousel
   */
  itemRenderer = (item, input) => {
    const { item: option } = item;
    const {
      id = '',
      displayName = '',
      style = fallBackColor,
      defaultImage = {},
      selectedImage = {},
    } = option;
    const { onChange, value: values = [] } = input;
    const isChecked = values.includes(id);

    return (
      <View>
        <TouchableOpacity
          accessibilityRole="image"
          onPress={e => {
            this.handleChange(e, id, values, onChange);
          }}
        >
          <StyleImageWrapper>
            {this.getImageUrl(isChecked, defaultImage, selectedImage, displayName)}
          </StyleImageWrapper>
          <BodyCopyWithSpacing
            fontFamily="secondary"
            fontSize="fs12"
            textAlign="center"
            text={displayName}
            style={{ color: `${isChecked ? style : 'black'}` }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  /**
   * @method renderCarousel
   * @description Renders the Carousel consisting of all the facets
   */
  renderCarousel = ({ input, options }) => {
    return (
      <Carousel
        name="carousel"
        data={options}
        renderItem={item => this.itemRenderer(item, input)}
        height={MODULE_HEIGHT}
        sliderWidth={MODULE_WIDTH}
        itemWidth={itemWidth}
        activeSlideAlignment="start"
        inactiveSlideOpacity={1}
        autoplay={false}
        loop={false}
        carouselConfig={{
          autoplay: false,
        }}
        isUseScrollView
      />
    );
  };

  /**
   * @method render
   * @description Main function to render the component
   */
  render() {
    const { stepInfo, filters, activeDotCount, setActiveStep, onSubmit, labels } = this.props;
    const { options = [], facetName = '' } = stepInfo;
    let filteredList = (filters && filters[facetName]) || [];
    filteredList =
      options.length && filteredList.length ? getFacetList(options, filteredList) : filteredList;
    return (
      <View>
        <CarouselWrapper>
          <Field
            id="essential_styles"
            name={facetName}
            type="checkbox"
            options={filteredList}
            component={this.renderCarousel}
          />
        </CarouselWrapper>
        <ButtonWrapper>
          {activeDotCount > 0 && (
            <Button
              width="134px"
              color="black"
              fill="WHITE"
              text={labels.back}
              fontSize="fs12"
              fontFamily="secondary"
              accessibilityLabel={labels.back}
              accessibilityRole="button"
              onPress={() => setActiveStep('dec')}
            />
          )}
          <Button
            width="134px"
            color="white"
            fill="BLUE"
            fontSize="fs12"
            fontFamily="secondary"
            accessibilityRole="button"
            text={labels.next}
            accessibilityLabel={labels.next}
            onPress={onSubmit}
          />
        </ButtonWrapper>
      </View>
    );
  }
}

GuidedShoppingStyles.propTypes = {
  activeDotCount: PropTypes.number,
  stepInfo: PropTypes.shape([]),
  filters: PropTypes.shape([]),
  labels: PropTypes.shape({}),
  onSubmit: PropTypes.func.isRequired,
  setActiveStep: PropTypes.func.isRequired,
};

GuidedShoppingStyles.defaultProps = {
  activeDotCount: 0,
  stepInfo: [],
  labels: {},
  filters: [],
};

export default GuidedShoppingStyles;
