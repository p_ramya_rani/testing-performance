// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../../../common/hoc/withStyles';
import AppliedFiltersListStyle from '../styles/AppliedFiltersList.style';
import AppliedFilterChip from '../../../atoms/AppliedFilterChip';

class AppliedFiltersList extends React.PureComponent {
  render() {
    const {
      appliedFilters,
      onRemoveFilter,
      removeAllFilters,
      className,
      labels,
      id,
      isABTestForStickyFilter,
      isBopisFilterOn,
    } = this.props;
    let chipsCount = 0;
    let filtersPresent = false;
    if (appliedFilters) {
      appliedFilters.forEach((filter) => {
        if (filter?.length) {
          filtersPresent = true;
        }
      });
    }
    return (
      <div className={`${className} applied-filters-sorting-container`}>
        {(!isBopisFilterOn || filtersPresent) && (
          <span className="filtering-title" data-locator="plp_filter_filtering_by">
            {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
            {labels.lbl_filtering_by}:
          </span>
        )}

        <ul className="applied-filter-list">
          {appliedFilters.map(
            (filter) =>
              filter &&
              filter.map((data) => {
                const name = data.displayName;

                // eslint-disable-next-line no-plusplus
                chipsCount++;
                return (
                  <AppliedFilterChip
                    id={data.id}
                    key={data.id}
                    fieldName={data.facetName}
                    displayName={name}
                    onRemoveClick={onRemoveFilter}
                  />
                );
              })
          )}
          {removeAllFilters && chipsCount > 1 && !isABTestForStickyFilter && (
            <button
              type="button"
              className="applied-filter-clear-all"
              onClick={removeAllFilters}
              data-locator="plp_filter_applied_filter_clear_all"
              id={id}
            >
              <span className="applied-filter-remove-button"> Clear All </span>
              {labels.lbl_clear}
            </button>
          )}
        </ul>
      </div>
    );
  }
}

AppliedFiltersList.defaultProps = {
  isABTestForStickyFilter: false,
  isBopisFilterOn: false,
};
AppliedFiltersList.propTypes = {
  appliedFilters: PropTypes.shape([]).isRequired,
  onRemoveFilter: PropTypes.func.isRequired,
  removeAllFilters: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  id: PropTypes.string.isRequired,
  isABTestForStickyFilter: PropTypes.bool,
  isBopisFilterOn: PropTypes.bool,
};

export default withStyles(AppliedFiltersList, AppliedFiltersListStyle);
export { AppliedFiltersList as AppliedFiltersListVanilla };
