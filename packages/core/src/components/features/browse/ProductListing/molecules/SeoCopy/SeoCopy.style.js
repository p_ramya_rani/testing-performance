// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  outline: none;

  .title {
    text-align: center;
    margin-bottom: 14px;
    outline: none;
  }

  &.seo-text {
    padding-left: 14px;
    padding-right: 14px;
    margin-bottom: ${props => props.theme.spacing.MODULE_SPACING.MED};
    @media ${props => props.theme.mediaQuery.medium} {
      padding-left: 15px;
      padding-right: 19px;
    }
    @media ${props => props.theme.mediaQuery.large} {
      padding-left: 40px;
      padding-right: 50px;
    }
  }

  .body-copy {
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    text-align: left;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }

  .read-more-state {
    display: none;
  }

  .read-less {
    display: none;
  }
  .read-more-target {
    display: none;
  }

  &.read-more-expanded .read-more-target {
    display: block;
    font-size: ${props => props.theme.typography.fontSizes.fs14};
    font-weight: ${props => props.theme.typography.fontWeights.regular};
  }

  .read-more-trigger {
    font-size: ${props => props.theme.typography.fontSizes.fs18};
    font-weight: ${props => props.theme.typography.fontWeights.semibold};
    cursor: pointer;

    p {
      outline: none;
    }
  }

  .read-more-state:checked ~ .read-more {
    display: none;
  }

  .read-more-state:checked ~ .read-less {
    display: block;
  }
`;
