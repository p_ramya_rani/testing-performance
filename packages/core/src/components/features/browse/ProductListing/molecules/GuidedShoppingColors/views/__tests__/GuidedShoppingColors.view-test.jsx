// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { GuidedShoppingColorsVanilla } from '../GuidedShoppingColors.view';

describe('GuidedShoppingColors Shop', () => {
  const props = {
    array: {},
    anyTouched: true,
    asyncValidating: false,
    dirty: true,
    form: 'filter-form',
    initialized: true,
    invalid: false,
    pristine: false,
    submitting: false,
    submitAsSideEffect: false,
    submitFailed: false,
    submitSucceeded: false,
    labels: {},
    valid: true,
    _reduxForm: null,
    categoryList: [
      {
        displayName: 'BLUE',
        id: 'BLUE',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/BLUE.gif',
        facetName: 'TCPColor_uFilter',
      },
      {
        displayName: 'BLACK',
        id: 'BLACK',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/BLACK.gif',
        facetName: 'TCPColor_uFilter',
      },
      {
        displayName: 'RED',
        id: 'RED',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/RED.gif',
        facetName: 'TCPColor_uFilter',
      },
      {
        displayName: 'TAN',
        id: 'TAN',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/TAN.gif',
        facetName: 'TCPColor_uFilter',
      },
      {
        displayName: 'WHITE',
        id: 'WHITE',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/WHITE.gif',
        facetName: 'TCPColor_uFilter',
      },
      {
        displayName: 'GRAY',
        id: 'GRAY',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/GRAY.gif',
        facetName: 'TCPColor_uFilter',
      },
      {
        displayName: 'GREEN',
        id: 'GREEN',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/GREEN.gif',
        facetName: 'TCPColor_uFilter',
      },
      {
        displayName: 'YELLOW',
        id: 'YELLOW',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/YELLOW.gif',
        facetName: 'TCPColor_uFilter',
      },
      {
        displayName: 'ORANGE',
        id: 'ORANGE',
        imagePath:
          'https://test4.childrensplace.com/wcsstore/GlobalSAS/images/tcp/category/color-swatches/ORANGE.gif',
        facetName: 'TCPColor_uFilter',
      },
    ],
    stepInfo: {
      header: {
        title: 'Pick your color!',
        style: 'style1',
      },
      type: 'carousal',
      facetName: 'TCPColor_uFilter',
      button: {
        text: 'FIND YOUR BASICS',
        title: 'FIND YOUR BASICS',
        action: '<action_value>',
      },
      options: [
        {
          text: 'All',
          type: 'all',
        },
        {
          text: 'Yellow',
          type: 'yellow',
        },
        {
          text: 'Orange',
          type: 'orange',
        },
        {
          text: 'Red',
          type: 'red',
        },
        {
          text: 'Pink',
          type: 'pink',
        },
      ],
    },
    pure: true,
    className: 'sc-cugefK eXDlTQ',
  };

  it('should render correctly', () => {
    const tree = shallow(<GuidedShoppingColorsVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
