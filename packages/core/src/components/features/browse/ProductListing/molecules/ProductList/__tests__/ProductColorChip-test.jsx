// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import * as utils from '@tcp/core/src/utils/utils';
import { ProductColorChipVanilla } from '../views/ProductColorChip';

jest.mock('@tcp/core/src/utils/utils');
utils.getAPIConfig = jest.fn();

describe('ProductColorChipVanilla component', () => {
  it('should renders correctly', () => {
    const props = {
      colorEntry: {
        colorProductId: 123,
        color: { name: '', imagePath: '' },
        miscInfo: [{}],
      },
      isActive: true,
      onChipClick: jest.fn(),
    };

    utils.getAPIConfig.mockImplementation(() => {
      return {
        assetHostTCP: 'https://www.blahblah.com',
        brandId: 'tcp',
        productAssetPathTCP: 'some/sample/path',
      };
    });
    const component = shallow(<ProductColorChipVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
