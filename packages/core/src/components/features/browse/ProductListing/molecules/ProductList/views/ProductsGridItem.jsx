/* eslint-disable max-lines */
/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d

import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import {
  getIconPath,
  routerPush,
  getBrand,
  getAPIConfig,
  getSiteId,
  getDynamicBadgeQty,
} from '@tcp/core/src/utils';
import {
  DIMENSION92_TEXT,
  getGAPageName,
  getProductCategory,
  getSearchRedirectObj,
} from '@tcp/core/src/utils/utils.web';
import { currencyConversion } from '@tcp/core/src/components/features/CnC/CartItemTile/utils/utils';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import { KEY_CODES } from '@tcp/core/src/constants/keyboard.constants';
import ProductRating from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductRating/ProductRating';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import productGridItemPropTypes, {
  productGridDefaultProps,
} from '../propTypes/ProductGridItemPropTypes';
import { Button, BodyCopy, Col, Row, Image } from '../../../../../../common/atoms';
import { getLocator, isClient, fireEnhancedEcomm, isGymboree } from '../../../../../../../utils';
import {
  getImagesToDisplay,
  getMapSliceForColorProductId,
  getRearrangedImgUrls,
} from '../utils/productsCommonUtils';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/ProductsGridItem.style';
import { getPromotionalMessage } from '../utils/utility';
import {
  ProductTitle,
  ProductPricesSection,
  BadgeItem,
  PromotionalMessage,
  CreateWishList,
  PurchaseSection,
  WishListIcon,
  EditButton,
} from './ProductItemComponents';
import { getTopBadge } from './ProductGridItem.util';
import ProductColorChipWrapper from './ProductColorChipWrapper';
import ProductAltImages from './ProductAltImages';
import { AVAILABILITY } from '../../../../Favorites/container/Favorites.constants';
import { getCartItemInfo } from '../../../../../CnC/AddedToBag/util/utility';
import RecommendationsAbstractor from '../../../../../../../services/abstractors/common/recommendations';

class ProductsGridItem extends React.PureComponent {
  // eslint-disable-next-line react/forbid-prop-types
  static propTypes = { ...productGridItemPropTypes, forwardedRef: PropTypes.object };

  static defaultProps = { ...productGridDefaultProps };

  constructor(props) {
    super(props);
    const { colorsMap, skuInfo: { colorProductId: itemColorProductId } = {} } = props.item;
    if (colorsMap) {
      this.colorsExtraInfo = {
        [colorsMap[0].color.name]: colorsMap[0].miscInfo,
      };
    }
    const colorProductId = colorsMap ? colorsMap[0].colorProductId : itemColorProductId;
    const {
      item: {
        productInfo: { pdpUrl },
      },
    } = props;
    const currentSiteBrand = getBrand();
    const apiConfigObj = getAPIConfig();
    const { crossDomain } = apiConfigObj;
    const itemBrand = props.item?.itemInfo?.brand ? props.item.itemInfo.brand : currentSiteBrand;
    const isProductBrandOfSameSiteBrand =
      currentSiteBrand.toUpperCase() === itemBrand.toUpperCase();
    this.state = {
      isInDefaultWishlist: props.item.miscInfo.isInDefaultWishlist,
      selectedColorProductId: colorProductId,
      currentImageIndex: 0,
      pdpUrl: isProductBrandOfSameSiteBrand
        ? props.item.productInfo.pdpUrl
        : `${crossDomain}/${getSiteId()}${pdpUrl}`,
      isAltImgRequested: false,
      isMoveItemOpen: false,
      generalProductId: '',
      errorProductId: '',
    };
    const {
      onQuickViewOpenClick,
      item: {
        productInfo: { generalProductId },
      },
      page,
    } = this.props;

    const { selectedColorProductId } = this.state;
    this.handleOpenQuickViewClick = () =>
      onQuickViewOpenClick(generalProductId, selectedColorProductId, generalProductId, page);
    this.handleImageChange = (index) => this.setState({ currentImageIndex: index });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isLoggedIn, onAddItemToFavorites, isSearchListing } = nextProps;
    const { generalProductId } = prevState;

    if (isLoggedIn && generalProductId !== '') {
      onAddItemToFavorites({
        colorProductId: generalProductId,
        productSkuId: null,
        page: isSearchListing ? 'SLP' : 'PLP',
      });
      return { generalProductId: '' };
    }
    return null;
  }

  componentDidMount() {
    const { isFavoriteView } = this.props;
    if (isFavoriteView) {
      document.addEventListener('mousedown', this.handleClickOutside);
    }
  }

  // DT-32496
  // When using the back button isInDefaultWishlist gets set to undefined in constructor
  // Need to update the value when we receive the latest props
  /* eslint-disable-next-line */
  UNSAFE_componentWillReceiveProps(nextProps) {
    const {
      item: {
        miscInfo: { isInDefaultWishlist },
      },
    } = nextProps;
    const { isInDefaultWishlist: currentIsInDefaultWishlist } = this.state;
    if (currentIsInDefaultWishlist === undefined) {
      this.setState({ isInDefaultWishlist });
    }
    const {
      item: { colorsMap },
    } = this.props;
    if (colorsMap !== nextProps.item.colorsMap) {
      this.colorsExtraInfo = {
        [nextProps.item.colorsMap[0].color.name]: nextProps.item.colorsMap[0].miscInfo,
      };
    }
  }

  componentWillUnmount() {
    const { isFavoriteView } = this.props;
    if (isFavoriteView) {
      document.removeEventListener('mousedown', this.handleClickOutside);
    }
  }

  getQuickViewInitialValues() {
    const {
      item: { colorsMap },
    } = this.props;
    const { selectedColorProductId } = this.state;
    const colorEntry =
      colorsMap && colorsMap.find((entry) => entry.colorProductId === selectedColorProductId);
    return colorEntry && colorEntry.color && colorEntry.color.name
      ? { color: colorEntry.color.name }
      : undefined;
  }
  /**
   * This function returns array of suggested Items
   * @param {*} item
   */

  checkAndRenderSuggestedItem = (item) => {
    if (!item.skuInfo) {
      return false;
    }
    const { seeSuggestedDictionary, hasSuggestedProduct } = this.props;
    const {
      skuInfo: { colorProductId },
    } = item;
    const suggestedItem = {
      status: false,
      attributes: null,
      hasSuggestedProduct,
    };

    if (!colorProductId) {
      return null;
    }

    const outOfStockProduct =
      colorProductId && seeSuggestedDictionary && seeSuggestedDictionary[colorProductId];
    const outOfStockColorProductId = outOfStockProduct && outOfStockProduct.colorProductId;
    const suggestedAttributes = outOfStockProduct && outOfStockProduct.attributes;

    if (outOfStockColorProductId && outOfStockColorProductId === colorProductId) {
      suggestedItem.status = true;
      suggestedItem.attributes = suggestedAttributes;
    }
    return suggestedItem;
  };

  /**
   * This function returns array of images for carousal and also decides whether to show image carousal or not
   * @param {*} imageUrls
   */
  getImageCarouselOptions(imageUrls) {
    const { hideImageCarousel } = this.props;

    return hideImageCarousel ? imageUrls.slice(0, 1) : imageUrls;
  }

  /**
   * This function closes the move item container onclick
   * @param {object} event
   */
  handleClickOutside = (event) => {
    const { isMoveItemOpen } = this.state;
    const openItem = document.querySelector('.move-item-section');
    const isChildren =
      openItem &&
      openItem.contains &&
      typeof openItem.contains === 'function' &&
      openItem.contains(event.target);
    if (
      !isChildren &&
      event.target &&
      event.target.classList &&
      !event.target.classList.contains('move-item-button') &&
      isMoveItemOpen
    ) {
      this.setState({
        isMoveItemOpen: false,
      });
    }
  };

  crossIcon = () => {
    const { onDismissSuggestion, outOfStockColorProductId, isSuggestedItem } = this.props;

    return isSuggestedItem ? (
      <button
        aria-label="close"
        onClick={() => onDismissSuggestion(outOfStockColorProductId)}
        className="close-btn"
      >
        <svg className="close-btn-icon" viewBox="0 0 25 25" aria-hidden="true">
          <path
            fill="#a0a0a0"
            fillRule="nonzero"
            d="M14.107 12.5l10.56-10.56A1.136 1.136 0 1 0 23.06.333L12.5 10.893 1.94.333A1.136 1.136 0 1 0 .333 1.94l10.56 10.56L.333 23.06a1.136 1.136 0 1 0 1.607 1.607l10.56-10.56 10.56 10.56c.222.222.513.333.804.333a1.136 1.136 0 0 0 .803-1.94L14.107 12.5z"
          />
        </svg>
      </button>
    ) : null;
  };

  dismissBtn = () => {
    const { onDismissSuggestion, outOfStockColorProductId, labelsPlpTiles, isSuggestedItem } =
      this.props;

    return isSuggestedItem ? (
      <div>
        <div className="move-item-container">
          <Button
            className="move-item-button dismiss-btn"
            onClick={() => onDismissSuggestion(outOfStockColorProductId)}
          >
            <BodyCopy
              fontFamily="secondary"
              fontSize={['fs10', 'fs12', 'fs14']}
              color="gray.900"
              className="see-suggested-items"
            >
              {labelsPlpTiles.lbl_dismiss}
            </BodyCopy>
          </Button>
        </div>
      </div>
    ) : null;
  };

  handleAddToWishlist = () => {
    const { deleteFavItemInProgressFlag } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const {
      item: {
        productInfo: { generalProductId },
        itemInfo: { itemId } = {},
      },
      onAddItemToFavorites,
      isLoggedIn,
      removeFavItem,
      isSearchListing,
      isSuggestedItem,
      item,
    } = this.props;
    const { selectedColorProductId } = this.state;

    if (isSuggestedItem) {
      this.handleQuickViewOpenClick();
    } else if (removeFavItem) {
      removeFavItem({ itemId, products: item.productInfo });
    } else {
      onAddItemToFavorites({
        colorProductId: selectedColorProductId || generalProductId,
        page: isSearchListing ? 'SLP' : 'PLP',
        products: item.productInfo,
      });
      this.setState({ errorProductId: selectedColorProductId || generalProductId });
      if (isClient() && isLoggedIn) {
        this.setState({ isInDefaultWishlist: true });
      } else {
        this.setState({ generalProductId: selectedColorProductId || generalProductId });
      }
    }
  };

  handleRemoveFromWishList = () => {
    const { deleteFavItemInProgressFlag } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const {
      item: {
        productInfo: { generalProductId },
      },
      onSetLastDeletedItemIdAction,
      wishList,
    } = this.props;
    const { selectedColorProductId } = this.state;

    const { externalIdentifier, giftListItemID } =
      (wishList && wishList[selectedColorProductId || generalProductId]) || {};
    onSetLastDeletedItemIdAction({
      externalId: externalIdentifier,
      itemId: giftListItemID,
      notFromFavoritePage: true,
    });
  };

  renderQuickViewCardOrLink = () => {};

  handleOpenAltImages = () => {
    const { isAltImgRequested } = this.state;
    if (isAltImgRequested) {
      return;
    }
    this.setState({ isAltImgRequested: true });
  };

  renderCustomRatings = () => {
    const { item, isHidePLPRatings, starRatingSize, isBagPage } = this.props;
    const {
      productInfo: { ratings, reviewsCount },
    } = item;
    return (
      !isHidePLPRatings &&
      !!ratings && (
        <ProductRating
          ratings={ratings || 0}
          reviews={reviewsCount}
          starRatingSize={starRatingSize}
          isBagPage={isBagPage}
        />
      )
    );
  };

  renderSuggestedLabel = () => {
    const { labelsPlpTiles, isSuggestedItem } = this.props;

    return isSuggestedItem ? (
      <BodyCopy
        dataLocator="plp_offer_price"
        mobileFontFamily="secondary"
        fontSize="fs10"
        fontWeight="extrabold"
        color="white"
        className="suggested-label"
      >
        {labelsPlpTiles.lbl_suggested}
      </BodyCopy>
    ) : null;
  };

  /* get color chip component */
  getColorChipContainer = (curentColorEntry, itemBrand, pdpUrl, primaryBrand) => {
    const {
      isMobile,
      item: { colorsMap, skuInfo, relatedSwatchImages, imagesByColor },
      isPLPredesign,
      isFavoriteView,
      isSuggestedItem,
      compactView,
      inGridPlpRecommendation,
      alternateBrand,
    } = this.props;
    if (inGridPlpRecommendation) return null;
    const colorProductId = skuInfo && skuInfo.colorProductId;
    if (isSuggestedItem || compactView) {
      return false;
    }
    const ChipProps = {
      className: 'color-chips-container',
      isMobile: { isMobile },
      showColorEvenOne: true,
      isPLPredesign: { isPLPredesign },
    };
    if (colorProductId && !isSuggestedItem) {
      return (
        <ProductColorChipWrapper
          selectedColorId={colorProductId}
          skuInfo={skuInfo}
          isFavoriteView={isFavoriteView}
          imagesByColor={imagesByColor}
          itemBrand={itemBrand}
          pdpUrl={pdpUrl}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
          {...ChipProps}
        />
      );
    }
    return colorsMap && colorsMap.length >= 1 ? (
      <ProductColorChipWrapper
        onChipClick={this.handleChangeColor}
        maxVisibleItems={5}
        selectedColorId={curentColorEntry.color.name}
        colorsMap={colorsMap}
        imagesByColor={imagesByColor}
        relatedSwatchImages={relatedSwatchImages}
        itemBrand={itemBrand}
        pdpUrl={pdpUrl}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
        {...ChipProps}
      />
    ) : (
      <div className="empty-color-chips-container" />
    );
  };

  getSuggestedClassName = (isFavoriteView, isSuggestedItem) => {
    return isFavoriteView || isSuggestedItem ? 'favorite-edit-item-btn' : '';
  };

  getDisplayPriceRange = (showPriceRange, isFavoriteView) => {
    return showPriceRange && !isFavoriteView;
  };

  /* function to get product price section */

  getProductPriceSection = (listPriceForColor, offerPriceForColor, badge3, isShowBadges) => {
    const { item, currencyAttributes, showPriceRange } = this.props;
    const bundleProduct = item && item.productInfo && item.productInfo.bundleProduct;
    let priceRange = item && item.productInfo && item.productInfo.priceRange;

    if (priceRange && currencyAttributes && currencyAttributes.exchangevalue) {
      priceRange = {
        highListPrice: currencyConversion(priceRange.highListPrice, currencyAttributes),
        highOfferPrice: currencyConversion(priceRange.highOfferPrice, currencyAttributes),
        lowListPrice: currencyConversion(priceRange.lowListPrice, currencyAttributes),
        lowOfferPrice: currencyConversion(priceRange.lowOfferPrice, currencyAttributes),
      };
    }

    const badge3Text = listPriceForColor - offerPriceForColor !== 0 ? badge3 : '';
    const { isFavoriteView, isSuggestedItem } = this.props;
    const suggestedClassName = this.getSuggestedClassName(isFavoriteView, isSuggestedItem);
    const displayPriceRange = this.getDisplayPriceRange(showPriceRange, isFavoriteView);

    return (
      <>
        <div className={suggestedClassName} />
        <ProductPricesSection
          listPrice={listPriceForColor}
          offerPrice={offerPriceForColor}
          noMerchantBadge={badge3}
          merchantTag={isShowBadges ? badge3Text : null}
          hidePrefixListPrice
          bundleProduct={bundleProduct}
          priceRange={priceRange}
          showPriceRange={displayPriceRange}
        />
      </>
    );
  };

  /* function to return promotional message component */
  getPromotionalMessageComponent = (promotionalMessage, promotionalPLCCMessage) => {
    const { isPlcc, isInternationalShipping, compactView } = this.props;
    return !compactView ? (
      <PromotionalMessage
        isInternationalShipping={isInternationalShipping}
        text={getPromotionalMessage(isPlcc, {
          promotionalMessage,
          promotionalPLCCMessage,
        })}
      />
    ) : null;
  };

  openMoveItem = () => {
    this.setState((prevState) => ({
      isMoveItemOpen: !prevState.isMoveItemOpen,
    }));
  };

  handleChangeColor = (colorProductId) => {
    const { pdpUrl } = this.state;
    /**
     * Check if pdp url contains "_" then replace any "-" with "_" if not then vice versa.
     * So that the PDP URL changes correctly when color change in PLP page
     */
    const replaceObj =
      pdpUrl.indexOf('_') > 0
        ? { searchvalue: '-', newvalue: '_' }
        : { searchvalue: '_', newvalue: '-' };
    const { searchvalue, newvalue } = replaceObj;
    const { selectedColorProductId } = this.state;
    const color = selectedColorProductId.replace(searchvalue, newvalue);
    const selectedColor = colorProductId.replace(searchvalue, newvalue);
    this.onQuickViewOpenClick({
      colorProductId: selectedColor,
    });
    // eslint-disable-next-line react/destructuring-assignment
    this.state.pdpUrl = this.state.pdpUrl.replace(color, selectedColor);
  };

  callAnalyticsOnAddToBagclick = () => {
    const { item, page, portalValue, sequence = '1' } = this.props;
    if (!page) return;

    const recIdentifier = RecommendationsAbstractor.handleWhichRecommendationClicked(
      portalValue,
      page,
      sequence
    );
    const currentSiteBrand = getBrand();
    const args = {
      eventName: 'productDetail',
      eventType: 'detail',
      pageName: page,
      productsObj: [
        {
          id: item.productInfo.generalProductId,
          price: item.productInfo.offerPrice,
          name: item.productInfo.name,
          brand: currentSiteBrand.toUpperCase(),
          category: item.miscInfo.categoryName ? item.miscInfo.categoryName : '',
          dimension20: item.productInfo.generalProductId,
          dimension21: page,
          dimension35: currentSiteBrand.toUpperCase(),
          dimension80: item.miscInfo.categoryName ? item.miscInfo.categoryName : '',
          dimension89: item.productInfo.generalProductId,
          dimension92: recIdentifier ? `${DIMENSION92_TEXT}${recIdentifier}` : '',
          dimension2: recIdentifier
            ? Constants.RECOMMENDATIONS_PAGES_MAPPING.RECOMMENDATIONS
            : 'browse',
          metric50: '1',
        },
      ],
    };

    fireEnhancedEcomm(args);
  };

  sendEventsToMonetate = () => {
    const {
      categoryName,
      provider = 'Monetate',
      userEmail,
      styleWithEligibility,
      isPlcc,
      outOfStock,
      item,
      reduxKey,
      loadSendEventsToMonetate,
    } = this.props;
    const { selectedColorProductId } = this.state;
    let { page } = this.props;
    if (page === 'home page') page = Constants.HOMEPAGE;
    const { actionId = '' } = item || {};
    const payload = {
      page: page || '',
      ...(selectedColorProductId && { itemPartNumber: selectedColorProductId }),
      categoryName: categoryName || '',
      provider,
      userEmail,
      styleWithEligibility,
      isPlcc,
      outOfStock,
      actionId: actionId.toString(),
      reduxKey,
    };
    loadSendEventsToMonetate(payload);
  };

  onQuickViewOpenClick = (payload) => {
    const { viaModule, onQuickViewOpenClick } = this.props;
    if (viaModule === Constants.RECOMMENDATION) this.sendEventsToMonetate();
    onQuickViewOpenClick(payload);
  };

  handleQuickViewOpenClick = (e) => {
    const {
      isSuggestedItem,
      onReplaceWishlistItem,
      activeWishListId,
      suggestedOOSItemId,
      item,
      addToBagEcom,
      isFavoriteView,
      page,
    } = this.props;

    if (e) {
      e.preventDefault();
    }
    const {
      productInfo: { generalProductId },
      skuInfo: { skuId, size } = {},
      itemInfo: { brand } = {},
    } = item;

    const { selectedColorProductId } = this.state;
    if (isSuggestedItem && onReplaceWishlistItem) {
      const formData = {
        activeWishListId,
        itemId: suggestedOOSItemId,
        colorProductId: generalProductId,
      };
      onReplaceWishlistItem(formData);
    } else if (isFavoriteView) {
      const orderInfo = {
        itemBrand: brand,
      };
      if (skuId && size) {
        let cartItemInfo = getCartItemInfo(item, {});
        cartItemInfo = { ...cartItemInfo, brand };
        if (addToBagEcom) addToBagEcom(cartItemInfo);
      } else {
        this.onQuickViewOpenClick({
          colorProductId: selectedColorProductId,
          orderInfo,
          fromPage: page,
        });
      }
    } else {
      this.onQuickViewOpenClick({
        colorProductId: selectedColorProductId,
        fromPage: page,
      });
    }
    this.callAnalyticsOnAddToBagclick();
  };

  handleViewBundleClick = (e) => {
    const {
      isBundleProductABTest,
      item: {
        productInfo: { bundleProduct, pdpUrl },
      },
      alternateBrand,
    } = this.props;

    const newPdpUrl = alternateBrand ? `${pdpUrl}$brand=${alternateBrand}` : `${pdpUrl}`;
    const isBundleProduct = !isBundleProductABTest && bundleProduct;
    if (isBundleProduct && pdpUrl) {
      routerPush(pdpUrl.replace('b/', 'b?bid='), newPdpUrl);
    } else {
      this.handleQuickViewOpenClick(e);
    }
  };

  renderMoveItem = (itemId) => {
    const {
      wishlistsSummaries,
      labels,
      createNewWishList,
      createNewWishListMoveItem,
      gridIndex,
      openAddNewList,
      activeWishListId,
      favoriteErrorMessages,
      isLoggedIn,
    } = this.props;
    const { isMoveItemOpen } = this.state;
    const accordianIcon = isMoveItemOpen
      ? getIconPath('up_arrow_icon')
      : getIconPath('down_arrow_icon');
    return (
      isLoggedIn &&
      wishlistsSummaries && (
        <div className="move-item-container">
          <Button className="move-item-button" onClick={this.openMoveItem}>
            {labels.lbl_fav_moveToAnotherList}
            <Image
              alt="accordian button"
              className="accordian-item-arrow icon-small"
              src={accordianIcon}
              data-locator="accordian-icon"
              height="6px"
            />
          </Button>
          {isMoveItemOpen && (
            <div className={`item__${gridIndex % 2 ? 'even' : 'odd'} move-item-section`}>
              <CreateWishList
                labels={labels}
                wishlistsSummaries={wishlistsSummaries}
                createNewWishList={createNewWishList}
                createNewWishListMoveItem={createNewWishListMoveItem}
                itemId={itemId}
                gridIndex={gridIndex}
                openAddNewList={openAddNewList}
                activeWishListId={activeWishListId}
                favoriteErrorMessages={favoriteErrorMessages}
              />
            </div>
          )}
        </div>
      )
    );
  };

  onSeeSuggestedHandler = (itemId) => {
    const {
      item: { skuInfo },
      onSeeSuggestedItems,
    } = this.props;

    const { colorProductId } = skuInfo;

    if (colorProductId && onSeeSuggestedItems) {
      onSeeSuggestedItems(colorProductId, itemId);
    }
  };

  SeeSuggestedList = (itemId) => {
    const { labelsPlpTiles } = this.props;

    return (
      <div className="move-item-container">
        <Button className="move-item-button" onClick={() => this.onSeeSuggestedHandler(itemId)}>
          <BodyCopy
            fontFamily="secondary"
            fontSize={['fs10', 'fs12', 'fs14']}
            color="gray.900"
            className="see-suggested-items"
          >
            {labelsPlpTiles && labelsPlpTiles.lbl_see_suggested_items}
          </BodyCopy>
        </Button>
      </div>
    );
  };

  renderAddToBagLabel = (isBundleProduct, keepAlive) => {
    const {
      outOfStockLabels,
      labels: { shopCollection, addToBag, shopthelook },
      isSuggestedItem,
      labelsPlpTiles,
      labels,
      isBopisFilterOn,
    } = this.props;
    let addToBagLabel = '';
    if (isSuggestedItem) {
      addToBagLabel = labelsPlpTiles && labelsPlpTiles.lbl_add_to_favorites;
    } else if (isBundleProduct) {
      addToBagLabel = isGymboree() ? shopthelook : shopCollection;
    } else {
      addToBagLabel = addToBag;
    }
    if (isBopisFilterOn) {
      addToBagLabel = (labels && labels.pickupInStore) || 'PICK UP IN STORE';
    }
    return keepAlive ? outOfStockLabels.outOfStockCaps : addToBagLabel;
  };

  errorMsgDisplay = () => {
    const {
      errorMessages,
      AddToFavoriteErrorMsg,
      item: {
        productInfo: { generalProductId },
      },
    } = this.props;
    const errorMsg = errorMessages && errorMessages[generalProductId];

    return errorMsg && errorMsg.itemId === generalProductId && AddToFavoriteErrorMsg ? (
      <Notification
        status="error"
        colSize={{ large: 12, medium: 8, small: 6 }}
        message={AddToFavoriteErrorMsg}
      />
    ) : null;
  };

  renderSuggestedError = (
    hasSuggestedProduct,
    suggestedProductErrors,
    generalProductId,
    labels,
    item,
    isFavoriteView
  ) => {
    if (!isFavoriteView) {
      return false;
    }

    if (!item.skuInfo) {
      return false;
    }
    const {
      skuInfo: { colorProductId },
    } = item;

    if (!colorProductId) {
      return null;
    }
    const errorLabel = labels && labels.lbl_fav_see_suggested_exception;
    return !hasSuggestedProduct &&
      colorProductId &&
      suggestedProductErrors &&
      suggestedProductErrors === colorProductId ? (
      // eslint-disable-next-line react/jsx-indent
      <Notification
        status="error"
        colSize={{ large: 12, medium: 8, small: 6 }}
        message={errorLabel}
      />
    ) : null;
  };

  getColorMiscInfo = (curentColorEntry) => {
    return (
      (curentColorEntry &&
        (this.colorsExtraInfo[curentColorEntry.color.name] || curentColorEntry.miscInfo)) ||
      {}
    );
  };

  onkeyPress = (e) => {
    const {
      item: {
        productInfo: { bundleProduct },
      },
      isShowQuickView,
    } = this.props;
    const isBundleProduct = bundleProduct;
    const { KEY_ENTER, KEY_SPACE } = KEY_CODES;
    if (e.keyCode === KEY_ENTER || e.keyCode === KEY_SPACE) {
      e.preventDefault();
      if (isShowQuickView && !isBundleProduct) {
        this.handleQuickViewOpenClick();
      } else {
        this.handleViewBundleClick();
      }
    }
  };

  renderSubmitButton = (keepAlive, itemNotAvailable, isHidePLPAddToBag) => {
    // DRP-43: Kill Switch to hide Add to Bag CTA on PLP and SLP.
    // Added this condition here, as main render method is reaching higher complexity
    // Hide when response is false
    if (isHidePLPAddToBag) {
      return '';
    }
    const {
      labels,
      item: {
        itemInfo: { itemId } = {},
        productInfo: { bundleProduct, generalProductId, name },
      },
      removeFavItem,
      isFavoriteView,
      isShowQuickView,
      AddToFavoriteErrorMsg,
      item,
      unbxdId,
    } = this.props;
    const { errorProductId } = this.state;
    const fulfilmentSection =
      AddToFavoriteErrorMsg && errorProductId === generalProductId ? '' : 'fulfillment-section';
    const isBundleProduct = bundleProduct;

    return itemNotAvailable ? (
      <div className={`${fulfilmentSection} no-item-available-wrapper`}>
        <Button
          className="remove-favorite"
          fullWidth
          buttonVariation="fixed-width"
          dataLocator={getLocator('remove_favorite_Button')}
          onClick={() => removeFavItem({ itemId, products: item.productInfo })}
        >
          {labels.lbl_fav_removeFavorite}
        </Button>
      </div>
    ) : (
      <div className={fulfilmentSection}>
        <Button
          className="added-to-bag"
          fullWidth
          buttonVariation="fixed-width"
          dataLocator={getLocator('global_addtocart_Button')}
          onClick={
            isShowQuickView && !isBundleProduct
              ? this.handleQuickViewOpenClick
              : this.handleViewBundleClick
          }
          onKeyUp={this.onkeyPress}
          disabled={keepAlive}
          fill={isFavoriteView ? 'BLUE' : ''}
          ariaLabel={`${this.renderAddToBagLabel(isBundleProduct, keepAlive)} ${name}`}
          unbxdattr="AddToCart"
          unbxdparam_sku={generalProductId}
          unbxdparam_qty="1"
          unbxdparam_requestId={unbxdId}
        >
          {this.renderAddToBagLabel(isBundleProduct, keepAlive)}
        </Button>
      </div>
    );
  };

  getIsProductBrandSameDomain = () => {
    const { item } = this.props;
    const currentSiteBrand = getBrand();
    const itemBrand = item?.itemInfo?.brand ? item.itemInfo.brand : currentSiteBrand;
    return currentSiteBrand.toUpperCase() === itemBrand.toUpperCase();
  };

  renderFavouriteIcon = (
    bundleProduct,
    isFavoriteView,
    isInDefaultWishlist,
    itemNotAvailable,
    isSuggestedItem
  ) => {
    const { labelsPlpTiles, compactView } = this.props;
    const handleAddRemoveFromWishlistObj = {
      handleAddToWishlist: this.handleAddToWishlist,
      handleRemoveFromWishList: this.handleRemoveFromWishList,
    };
    return (
      !bundleProduct &&
      !compactView &&
      WishListIcon(
        isFavoriteView,
        isInDefaultWishlist,
        handleAddRemoveFromWishlistObj,
        itemNotAvailable,
        '',
        isSuggestedItem,
        labelsPlpTiles
      )
    );
  };

  renderProductTitle = ({
    name,
    pdpUrl,
    loadedProductCount,
    viaModule,
    analyticsData,
    isProductBrandOfSameDomain,
    isSearchPage,
    page,
    isSoldOut,
    cookieToken,
    bundleProduct,
    dataSource,
    googleAnalyticsData,
    primaryBrand,
  }) => {
    const {
      compactView,
      alternateBrand,
      labels,
      isShowBrandNameEnabled,
      isSearchListing,
      isRecommendation,
      isBopisFilterOn,
    } = this.props;
    return !compactView ? (
      <ProductTitle
        name={name}
        pdpUrl={pdpUrl}
        loadedProductCount={loadedProductCount}
        viaModule={viaModule}
        dataSource={dataSource}
        analyticsData={analyticsData}
        isProductBrandOfSameDomain={isProductBrandOfSameDomain}
        isSearchPage={isSearchPage}
        page={page}
        isSoldOut={isSoldOut}
        cookieToken={cookieToken}
        bundleProduct={bundleProduct}
        googleAnalyticsData={googleAnalyticsData}
        alternateBrand={alternateBrand}
        labels={labels}
        isShowBrandNameEnabled={isShowBrandNameEnabled}
        primaryBrand={primaryBrand || alternateBrand}
        isSearchListing={isSearchListing}
        isRecommendation={isRecommendation}
        isBopisFilterOn={isBopisFilterOn}
      />
    ) : null;
  };

  renderBadgeItem = (isShowBadges, topBadge) => {
    const { isSuggestedItem, isFavoriteView, compactView } = this.props;
    return !isSuggestedItem && !isFavoriteView && !compactView ? (
      <BadgeItem
        isShowBadges={isShowBadges}
        className="top-badge-container"
        text={topBadge}
        haveSpace
      />
    ) : null;
  };

  renderBadgeTwo = (badge2) => {
    const { isSuggestedItem, isFavoriteView, compactView } = this.props;
    return (
      <Col colSize={{ small: 12 }}>
        <BodyCopy
          dataLocator={getLocator('global_Extended_sizes_text')}
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize={['fs10', 'fs12', 'fs14']}
          className="extended-sizes-text elem-mb-SM"
        >
          {!isSuggestedItem && !isFavoriteView && !compactView && badge2 && badge2.toUpperCase()}
        </BodyCopy>
      </Col>
    );
  };

  checkBrand = (isTCP) => {
    if (typeof isTCP === 'boolean' && isTCP) {
      return 'TCP';
    }
    if (typeof isTCP === 'boolean' && !isTCP) {
      return 'GYM';
    }
    return false;
  };

  getPriceForProduct = (listPrice, offerPrice, currencyAttributes) => {
    let listPriceForColor = listPrice;
    let offerPriceForColor = offerPrice;
    if (currencyAttributes && currencyAttributes.exchangevalue) {
      listPriceForColor = currencyConversion(listPrice, currencyAttributes);
      offerPriceForColor = currencyConversion(offerPrice, currencyAttributes);
    }
    return {
      listPriceForColor,
      offerPriceForColor,
    };
  };

  getCategoryName = (breadCrumbs, isSearchPage) => {
    if (isSearchPage) {
      const searchRedirect = getSearchRedirectObj();
      return searchRedirect && searchRedirect.pc ? searchRedirect.pc : 'apparel';
    }
    return getProductCategory(breadCrumbs);
  };

  updateGoogleAnalyticsData = () => {
    const { item, currencyAttributes, position, currencySymbol, breadCrumbs } = this.props;
    let color = '';
    item.colorsMap.forEach((c) => {
      if (c.colorProductId === item.productInfo.generalProductId) color = c.color.name;
    });
    const currentSiteBrand = getBrand();
    const currency = (currencyAttributes && currencyAttributes.id) || currencySymbol || '';
    const isSearchPage = isClient() && window.location.href.includes('/search/');
    const category = this.getCategoryName(breadCrumbs, isSearchPage);
    const args = {
      eventName: 'productclick',
      eventAction: 'Product Click',
      eventType: 'click',
      pageName: getGAPageName(),
      currCode: currency,
      productsObj: [
        {
          name: item.productInfo.name,
          id: item.productInfo.generalProductId,
          price: item.productInfo.offerPrice,
          brand: currentSiteBrand.toUpperCase(),
          category,
          variant: color,
          position: isSearchPage ? position + 1 : position,
          dimension80: category,
          dimension67:
            item.productInfo.offerPrice < item.productInfo.listPrice ? 'on sale' : 'full price',
          dimension89: item.productInfo.generalProductId,
          dimension100: isSearchPage ? position + 1 : position,
          dimension63: `${item.productInfo.offerPrice} ${currency}: ${item.productInfo.listPrice} ${currency}`,
          dimension77: color,
          dimension35: currentSiteBrand.toUpperCase(),
        },
      ],
    };

    fireEnhancedEcomm(args);
  };

  renderPointsAndReward = (labels) => {
    const pointsAndRewardsLabel =
      (labels.pointsAndRewardsText && labels.pointsAndRewardsText.split('##')) || '';
    return pointsAndRewardsLabel ? (
      <p className="points-and-rewards">
        {pointsAndRewardsLabel[0] && (
          <span className="points-and-rewards-1">{pointsAndRewardsLabel[0]}</span>
        )}
        {pointsAndRewardsLabel[1] && (
          <span className="points-and-rewards-or">{pointsAndRewardsLabel[1]}</span>
        )}
        {pointsAndRewardsLabel[2] && (
          <span className="points-and-rewards-2">{pointsAndRewardsLabel[2]}</span>
        )}
      </p>
    ) : null;
  };

  getInGridRecommendation = (labels) => {
    const { portalValue } = this.props;
    const label =
      portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS
        ? labels.inGridDescription
        : labels.inMultiGridDescription;
    return (
      <div className="in-grid-plp-rectangle">
        <span>{label}</span>
      </div>
    );
  };

  renderFavoriteMovePurchaseSection = (
    itemNotAvailable,
    quantity,
    labels,
    quantityPurchased,
    itemId
  ) => {
    if (itemNotAvailable)
      return (
        <div className="favorite-move-purchase-section">
          {PurchaseSection(quantity, labels, quantityPurchased)}
          {this.SeeSuggestedList(itemId)}
        </div>
      );
    return (
      <div className="favorite-move-purchase-section">
        {PurchaseSection(quantity, labels, quantityPurchased)}
        {this.renderMoveItem(itemId)}
      </div>
    );
  };

  checkInWishlist = (wishList, productId) => {
    return wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;
  };

  isPDP = () => {
    const { page } = this.props;
    return `${page === 'pdp' ? 'pdp' : ''}`;
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      onQuickViewOpenClick,
      currencyAttributes,
      item,
      isOnModelAbTestPlp,
      isMatchingFamily,
      loadedProductCount,
      className,
      sqnNmbr,
      unbxdId,
      labels,
      isFavoriteView,
      forwardedRef,
      outOfStockLabels,
      isKeepAliveEnabled,
      isSuggestedItem,
      viaModule,
      dataSource,
      isSearchPage,
      page,
      wishList,
      isLoggedIn,
      cookieToken,
      isHidePLPAddToBag,
      hasSuggestedProduct,
      suggestedProductErrors,
      isBrierleyPromoEnabled,
      lowQualityEnabled,
      portalValue,
      isDynamicBadgeEnabled,
      inGridPlpRecommendation,
      widgetScrapeContentId,
      isSecureImageFlagEnabled,
      alternateBrand,
      isBopisFilterOn,
    } = this.props;
    const {
      productInfo: {
        bundleProduct,
        promotionalMessage,
        promotionalPLCCMessage,
        generalProductId,
        name,
        listPrice: itemListPrice,
        offerPrice: itemOfferPrice,
        long_product_title: longProductTitle,
        tcpStyleQty,
        tcpStyleType,
        primaryBrand,
      },
      itemInfo: { itemId, quantity, availability, brand } = {},
      quantityPurchased,
      colorsMap,
      imagesByColor,
      actionId,
    } = item;

    const itemNotAvailable = availability === AVAILABILITY.SOLDOUT;
    const prodNameAltImages = longProductTitle || name;
    const { selectedColorProductId, currentImageIndex, pdpUrl } = this.state;

    const suggestedItem = this.checkAndRenderSuggestedItem(item);
    const curentColorEntry = getMapSliceForColorProductId(colorsMap, selectedColorProductId);
    let imageUrls = getImagesToDisplay({
      imagesByColor,
      curentColorEntry,
      isAbTestActive: isOnModelAbTestPlp,
      isFavoriteView,
    });
    imageUrls = getRearrangedImgUrls(imageUrls, curentColorEntry);
    const imageUrlsToShow = this.getImageCarouselOptions(imageUrls);
    const currentColorMiscInfo = this.getColorMiscInfo(curentColorEntry);
    const isInWishList = this.checkInWishlist(wishList, selectedColorProductId);

    const {
      listPrice = itemListPrice,
      offerPrice = itemOfferPrice,
      badge1,
      badge2,
      badge3,
      keepAlive: keepAliveProduct,
    } = currentColorMiscInfo;
    const keepAlive = isKeepAliveEnabled && keepAliveProduct;
    const topBadge = getTopBadge(isMatchingFamily, badge1);
    const { listPriceForColor, offerPriceForColor } = this.getPriceForProduct(
      listPrice,
      offerPrice,
      currencyAttributes
    );
    const isShowBadges = currentImageIndex === 0;
    // NOTE: isClearance is a string 'Clearance', not a boolean we should clean this up globally in the abstractor
    const sqnNmb = sqnNmbr;
    const promotionalMessageModified = promotionalMessage || '';
    const promotionalPLCCMessageModified = promotionalPLCCMessage || '';
    const itemBrand = brand;

    const isProductBrandOfSameDomain = this.getIsProductBrandSameDomain();
    const googleAnalyticsData = this.updateGoogleAnalyticsData;
    let divProps;
    const isPdp = this.isPDP();
    if (widgetScrapeContentId) {
      divProps = {
        className: `item-container-inner ${isPdp}`,
        unbxdattr: 'product',
        unbxdparam_sku: selectedColorProductId,
        unbxdparam_prank: sqnNmb,
        unbxdparam_requestId: unbxdId,
        id: widgetScrapeContentId,
      };
    } else {
      divProps = {
        className: `item-container-inner ${isPdp}`,
        unbxdattr: 'product',
        unbxdparam_sku: selectedColorProductId,
        unbxdparam_prank: sqnNmb,
        unbxdparam_requestId: unbxdId,
      };
    }

    return (
      <li
        className={`${className}`}
        key={generalProductId}
        onMouseEnter={this.handleOpenAltImages}
        onMouseOut={this.handleCloseAltImages}
        onBlur={this.handleCloseAltImages}
        ref={forwardedRef}
      >
        {suggestedItem && suggestedItem.status && (
          <Recommendations
            {...suggestedItem.attributes}
            hasSuggestedProduct={suggestedItem.status}
            sequence="1"
          />
        )}
        <div {...divProps}>
          {!inGridPlpRecommendation && this.renderBadgeItem(isShowBadges, topBadge)}
          {this.crossIcon()}
          <ProductAltImages
            className="product-image-container"
            pdpUrl={pdpUrl}
            loadedProductCount={loadedProductCount}
            viaModule={viaModule}
            dataSource={dataSource}
            imageUrls={imageUrlsToShow}
            productName={prodNameAltImages}
            onImageChange={this.handleImageChange}
            colorsMap={colorsMap}
            analyticsData={{
              pId: generalProductId,
              prank: sqnNmb,
              requestId: unbxdId,
            }}
            keepAlive={keepAlive}
            isSoldOut={itemNotAvailable}
            soldOutLabel={outOfStockLabels.outOfStockCaps}
            itemBrand={itemBrand}
            isSearchPage={isSearchPage}
            page={page}
            isProductBrandOfSameDomain={isProductBrandOfSameDomain}
            isBundleProduct={bundleProduct}
            cookieToken={cookieToken}
            googleAnalyticsData={googleAnalyticsData}
            lowQualityEnabled={lowQualityEnabled}
            portalValue={portalValue}
            dynamicBadgeOverlayQty={getDynamicBadgeQty(
              tcpStyleType,
              tcpStyleQty,
              isDynamicBadgeEnabled
            )}
            tcpStyleType={tcpStyleType}
            lazyLoad
            actionId={actionId}
            isSecureImageFlagEnabled={isSecureImageFlagEnabled}
            primaryBrand={primaryBrand || alternateBrand}
            alternateBrand={alternateBrand}
            isBopisFilterOn={isBopisFilterOn}
          />
          {isLoggedIn &&
            EditButton(
              { onQuickViewOpenClick, isFavoriteView, labels, item },
              selectedColorProductId,
              itemNotAvailable
            )}
          {
            <Row fullBleed className="product-wishlist-container">
              {this.renderBadgeTwo(badge2)}
              <Col colSize={{ small: 4, medium: 6, large: 10 }}>
                {this.getProductPriceSection(
                  listPriceForColor,
                  offerPriceForColor,
                  badge3,
                  isShowBadges
                )}
              </Col>
              {this.renderFavouriteIcon(
                bundleProduct,
                isFavoriteView,
                isInWishList,
                itemNotAvailable,
                isSuggestedItem
              )}
            </Row>
          }
          {this.renderProductTitle({
            name,
            pdpUrl,
            loadedProductCount,
            viaModule,
            dataSource,
            analyticsData: {
              pId: generalProductId,
              prank: sqnNmb,
              requestId: unbxdId,
            },
            isProductBrandOfSameDomain,
            isSearchPage,
            page,
            isSoldOut: itemNotAvailable,
            cookieToken,
            bundleProduct,
            googleAnalyticsData,
            primaryBrand,
          })}
          {this.renderSuggestedLabel()}

          {inGridPlpRecommendation && this.getInGridRecommendation(labels)}
          {this.getColorChipContainer(curentColorEntry, itemBrand, pdpUrl, primaryBrand)}

          {isBrierleyPromoEnabled
            ? this.renderPointsAndReward(labels)
            : !isSuggestedItem &&
              this.getPromotionalMessageComponent(
                promotionalMessageModified,
                promotionalPLCCMessageModified
              )}
          {this.renderCustomRatings()}
          {this.renderSubmitButton(keepAlive, itemNotAvailable, isHidePLPAddToBag)}
          {!inGridPlpRecommendation &&
            isFavoriteView &&
            this.renderFavoriteMovePurchaseSection(
              itemNotAvailable,
              quantity,
              labels,
              quantityPurchased,
              itemId
            )}

          {this.dismissBtn()}

          {/* {error && <ErrorMessage error={error} />} */}
          {this.errorMsgDisplay()}
          {this.renderSuggestedError(
            hasSuggestedProduct,
            suggestedProductErrors,
            generalProductId,
            labels,
            item,
            isFavoriteView
          )}
        </div>
      </li>
    );
  }
}

const ProductsGridItemWithRef = forwardRef((props, ref) => {
  return <ProductsGridItem forwardedRef={ref} {...props} />;
});

export { ProductsGridItem as ProductsGridItemVanilla };

const ProductsGridItemStyled = withStyles(ProductsGridItemWithRef, styles);

// Display name is needed for hotfix mapping capability
ProductsGridItemStyled.displayName = 'ProductsGridItem';

export default ProductsGridItemStyled;
