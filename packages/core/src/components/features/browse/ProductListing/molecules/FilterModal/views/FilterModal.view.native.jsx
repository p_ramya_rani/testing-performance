/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Modal, Platform } from 'react-native';
import PropTypes from 'prop-types';
import ImageComp from '@tcp/core/src/components/common/atoms/Image';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import DropDown from '@tcp/core/src/components/common/atoms/DropDown/views/DropDown.native';
import ModalBox from 'react-native-modalbox';
import capitalized from 'lodash/capitalize';

import {
  styles,
  Container,
  SafeAreaViewStyle,
  ModalOverlay,
  ModalContent,
  SortContent,
  ModalOutsideTouchable,
  ModalTitle,
  ModalTitleContainer,
  ModalCloseTouchable,
  FilterBarContainer,
} from '../FilterModal.style.native';
import FilterButtons from '../../FilterButtons';
import Filters from '../../Filters';
import getSortOptions from '../../SortSelector/views/Sort.util';
import FILTER_CONFIG from '../../Filters/Filter.constants';
import StickyFilterButtons from '../../StickyFilterButtons';
import {
  filterArr,
  validateFiterValues,
  getFilterTotalSeletectedCount,
  triggerAnalytics,
} from './utils';

const closeIcon = require('../../../../../../../../../mobileapp/src/assets/images/close.png');

class FilterModal extends React.PureComponent {
  static propTypes = {
    filters: PropTypes.shape({}),
    theme: PropTypes.shape({}),
    labelsFilter: PropTypes.shape({}),
    onSubmit: PropTypes.func.isRequired,
    getProducts: PropTypes.func.isRequired,
    navigation: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
    sortLabels: PropTypes.arrayOf(PropTypes.shape({})),
    isFavorite: PropTypes.bool,
    onFilterSelection: PropTypes.func,
    onSortSelection: PropTypes.func,
    filteredId: PropTypes.string,
    selectedFilterValue: PropTypes.shape({}).isRequired,
    setSelectedFilter: PropTypes.func.isRequired,
    isKeepModalOpen: PropTypes.bool,
    isLoadingMore: PropTypes.bool,
    resetForm: PropTypes.func,
    appliedFiltersCount: PropTypes.number,
    displayAltFilterView: PropTypes.bool,
    renderItemCountView: PropTypes.func.isRequired,
    totalProductsCount: PropTypes.number,
  };

  static defaultProps = {
    filters: {},
    theme: {},
    labelsFilter: {},
    navigation: {},
    sortLabels: [],
    isFavorite: false,
    onFilterSelection: () => {},
    onSortSelection: () => {},
    filteredId: 'ALL',
    isKeepModalOpen: false,
    isLoadingMore: false,
    appliedFiltersCount: 0,
    resetForm: () => {},
    displayAltFilterView: false,
    totalProductsCount: 0,
  };

  constructor(props) {
    super(props);
    const { isKeepModalOpen, filters, appliedFiltersCount, labelsFilter } = props;
    const filter =
      labelsFilter &&
      this.getFilterSortLabel(appliedFiltersCount, capitalized(labelsFilter?.lbl_filter));
    const sort = labelsFilter && capitalized(labelsFilter?.lbl_sort);

    this.state = {
      showModal: isKeepModalOpen,
      language: '',
      showSortModal: false,
      stickyFilters: filterArr(filter, sort, labelsFilter),
      index: 0,
      isFromFilterPills: false,
      filterTitle: labelsFilter?.lbl_filter_sort,
    };
    this.sizeFitData = [];
    if (filters && filters[FILTER_CONFIG.facetId.groupedSize]) {
      this.sizeFitData = filters[FILTER_CONFIG.facetId.groupedSize];
    }
  }

  componentDidMount() {
    this.setFilterData();
  }

  componentDidUpdate(prevProps, prevState) {
    const { filters } = this.props;
    const { language } = this.state;
    if (prevProps.filters !== filters || prevState.language !== language) {
      this.setFilterData();
    }
  }

  setFilterData = () => {
    const { labelsFilter, filters, sortLabels, isFavorite } = this.props;
    const { language, stickyFilters } = this.state;
    const sortOptions = isFavorite ? sortLabels : getSortOptions(sortLabels);
    const updatedStickyFilters = stickyFilters;
    const updatedFilterData = this.combineFilterAndSort(filters, {
      sortLabel: labelsFilter.lbl_sort_ab,
      sortOptions,
      selectedOption: language,
    });

    const unbxdDisplayName =
      updatedFilterData &&
      Object.keys(updatedFilterData).length > 0 &&
      updatedFilterData?.unbxdDisplayName;

    if (unbxdDisplayName && Object.keys(unbxdDisplayName).length > 0) {
      const totalSelectedCount = getFilterTotalSeletectedCount(updatedFilterData);
      const filter =
        labelsFilter &&
        this.getFilterSortLabel(totalSelectedCount, capitalized(labelsFilter?.lbl_filter));
      stickyFilters[0].name = filter;
      stickyFilters[0].isSelected = totalSelectedCount > 0;
      this.setState({
        stickyFilters: validateFiterValues(
          unbxdDisplayName,
          updatedStickyFilters,
          language,
          updatedFilterData
        ),
      });
    }
  };

  onReset = () => {
    this.setState({ language: '' }, () => {
      this.applyFilterAndSort({});
    });
  };

  setModalVisibilityState = (flag) => {
    this.setState({
      showModal: flag,
      showSortModal: !flag,
    });
  };

  setSortModalVisibilityState = (flag) => {
    this.setState({
      showModal: flag,
      showSortModal: flag,
    });
  };

  resetAppliedFilters = () => {
    if (this.filterViewRef) this.filterViewRef.clearAllFilters();
  };

  onCloseModal = () => {
    const { isFavorite } = this.props;
    if (!isFavorite) {
      this.resetAppliedFilters();
    }
    this.setModalVisibilityState(false);
  };

  onPressOut = () => {
    this.resetAppliedFilters();
    this.setModalVisibilityState(false);
  };

  onPressFilter = () => {
    const { resetForm, labelsFilter } = this.props;
    this.setState({
      index: 0,
      isFromFilterPills: false,
      filterTitle:
        labelsFilter.lbl_filter_sort && labelsFilter.lbl_filter_sort.toString().toUpperCase(),
    });
    resetForm();
    this.setModalVisibilityState(true);
  };

  onPressSort = () => {
    const { resetForm } = this.props;
    resetForm();
    this.setSortModalVisibilityState(true);
  };

  onPressFilterPills = (index) => {
    const { stickyFilters } = this.state;
    const { resetForm } = this.props;
    this.setState({
      index,
      isFromFilterPills: true,
      filterTitle: capitalized(stickyFilters[index].name),
    });
    resetForm();
    this.setModalVisibilityState(true);
  };

  /**
   * @function applyFilterAndSort
   * This method applies filters and sort stored in current instance of FilterModal
   *
   * @memberof FilterModal
   */
  applyFilterAndSort = (filters) => {
    const {
      onSubmit,
      getProducts,
      navigation,
      selectedFilterValue,
      displayAltFilterView,
      trackClickFilter,
    } = this.props;
    const url = navigation && navigation.getParam('url');
    let filterData = {};
    const { language, index } = this.state;

    if (filters) {
      filterData = filters;
    } else if (selectedFilterValue) {
      filterData = selectedFilterValue;
    }

    if (language) {
      // restore sort if available
      filterData = { ...filterData, sort: language };
    }
    if (onSubmit) {
      const { name, module, contextData } = triggerAnalytics(filterData, language, index > 1);
      if (trackClickFilter) {
        trackClickFilter({ name, module, contextData });
      }
      onSubmit(filterData, false, getProducts, url, true, displayAltFilterView);
    }

    if (!filters) {
      // prevent modal from closing on sort apply if AB test view is ON
      const showModal = displayAltFilterView === true;
      this.setModalVisibilityState(showModal);
    }
  };

  closeModal = () => {
    this.setModalVisibilityState(false);
  };

  /**
   * @function handleClick
   * This method is called with selected sort value when sort is applied
   *
   * @memberof FilterModal
   */
  handleClick = (selectedValue, isFromSort) => {
    const { language } = this.state;
    const sortValue = Platform.OS === 'ios' ? language : selectedValue;
    this.sortValue = sortValue;
    const { isFavorite, onSortSelection } = this.props;
    if (isFavorite) {
      this.setModalVisibilityState(false);
      onSortSelection(sortValue);
    } else if (isFromSort && !isFavorite) {
      this.resetApplyFilter();
    } else {
      this.applyFilterAndSort();
    }
  };

  resetApplyFilter = () => {
    const { filters } = this.props;
    const { unbxdDisplayName } = filters;
    let filterNames = [];
    let selectedFilters = {};
    filterNames = (unbxdDisplayName && Object.entries(unbxdDisplayName)) || [];
    filterNames.forEach((name) => {
      const key = name[0];
      const filterData = filters[key] || [];
      // find all the selected items from filters list and map it with filter key present in filterNames
      const selectedFiltersData = filterData
        .filter((item) => item.isSelected)
        .map((item) => item.id);
      const result = {};
      result[key] = selectedFiltersData;
      selectedFilters = { ...selectedFilters, ...result };
    });

    this.applyFilterAndSort(selectedFilters);
  };

  /**
   * @function applyFilters
   * This method is called with selected filters when filter is applied
   *
   * @memberof FilterModal
   */
  applyFilters = (filters, isClearFilter) => {
    this.filters = filters;
    this.applyFilterAndSort(filters, isClearFilter);
  };

  renderFilters = (filterProps) => {
    const {
      labelsFilter,
      setSelectedFilter,
      filters,
      isFavorite,
      onFilterSelection,
      filteredId,
      isLoadingMore,
      displayAltFilterView,
      totalProductsCount,
    } = filterProps;
    const { language, index, isFromFilterPills, filterTitle } = this.state;
    return (
      <Filters
        name="filters"
        labelsFilter={labelsFilter}
        titleLabel={filterTitle}
        filters={filters}
        onSubmit={(filter) => {
          if (setSelectedFilter) {
            setSelectedFilter(filter);
          }
          this.applyFilters(filter);
        }}
        ref={(ref) => {
          this.filterViewRef = ref;
        }}
        isFavorite={isFavorite}
        onFilterSelection={onFilterSelection}
        filteredId={filteredId}
        onCloseModal={this.onCloseModal}
        closeModal={this.closeModal}
        isLoadingMore={isLoadingMore}
        displayAltFilterView={displayAltFilterView}
        totalProductsCount={totalProductsCount}
        selectedSortId={language}
        onSortSelection={(itemValue) => {
          this.setState({ language: itemValue }, () => this.handleClick(itemValue, true));
        }}
        sizeFitData={this.sizeFitData}
        filterPillsIndex={index}
        isFromFilterPills={isFromFilterPills}
      />
    );
  };

  combineFilterAndSort = (filters, sortData) => {
    const filterAndSortData = filters;
    if (filterAndSortData && filterAndSortData.unbxdDisplayName) {
      filterAndSortData[FILTER_CONFIG.facetId.sort] = sortData.sortOptions;
      filterAndSortData.unbxdDisplayName[FILTER_CONFIG.facetId.sort] = sortData.sortLabel;
    }
    return filterAndSortData;
  };

  getFilterSortLabel = (appliedFiltersCount, label = '') => {
    let filterSortLabel = label;
    if (appliedFiltersCount > 0) {
      filterSortLabel = `${label} (${appliedFiltersCount})`;
    }
    return filterSortLabel;
  };

  onStickyButtonPress = (index, flatListRef) => {
    switch (index) {
      case 0:
      case 1:
        this.onPressFilter();
        break;
      case 2:
        this.onPressFilterPills(index);
        break;
      case 3:
        this.onPressFilterPills(index);
        break;
      case 4:
        this.onPressFilterPills(index);
        break;
      case 5:
        this.onPressFilterPills(index);
        break;
      case 6:
        this.onReset();
        if (flatListRef) {
          flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
        }
        break;
      default:
        break;
    }
  };

  render() {
    const {
      labelsFilter,
      filters,
      sortLabels,
      isFavorite,
      onFilterSelection,
      filteredId,
      setSelectedFilter,
      isLoadingMore,
      appliedFiltersCount,
      displayAltFilterView,
      totalProductsCount,
    } = this.props;
    const { showModal, language, showSortModal, isFromFilterPills } = this.state;
    const sortOptions = isFavorite ? sortLabels : getSortOptions(sortLabels);

    const dropDownStyle = {
      height: Platform.OS === 'ios' ? 0 : 49,
      border: 1,
    };
    const highlightStyle = {
      backgroundColor: 'black',
      height: 49,
    };
    const itemStyle = {
      height: 49,
      color: 'gray.800',
    };

    const pluginStyle = [
      {
        height: '90%',
        borderRadius: 16,
      },
    ];

    const pluginPillsStyle = [
      {
        borderRadius: 16,
        height: '50%',
      },
    ];
    const modalStyle = isFromFilterPills ? pluginPillsStyle : pluginStyle; // isFromFilterPills
    return (
      <Container>
        {displayAltFilterView ? (
          <>
            <FilterBarContainer>
              <StickyFilterButtons
                showResetOption={appliedFiltersCount > 0 || language !== ''}
                stickyFilters={this.state.stickyFilters}
                onStickyButtonPress={(index, flatListRef) => {
                  this.onStickyButtonPress(index, flatListRef);
                }}
              />
            </FilterBarContainer>
            <ModalBox
              animationDuration={400}
              style={modalStyle}
              position="bottom"
              isOpen={showModal}
              entry="bottom"
              swipeToClose={false}
              onClosed={() => this.onCloseModal()}
              coverScreen
              useNativeDriver
              swipeThreshold={10}
            >
              {this.renderFilters({
                labelsFilter,
                setSelectedFilter,
                filters: this.combineFilterAndSort(filters, {
                  sortLabel: labelsFilter.lbl_sort_ab,
                  sortOptions,
                  selectedOption: language,
                }),
                isFavorite,
                onFilterSelection,
                filteredId,
                isLoadingMore,
                displayAltFilterView,
                totalProductsCount,
              })}
            </ModalBox>
          </>
        ) : (
          <>
            <FilterButtons
              labelsFilter={labelsFilter}
              onPressFilter={this.onPressFilter}
              onPressSort={this.onPressSort}
              selected={showModal}
              appliedFiltersCount={appliedFiltersCount}
            />

            <Modal visible={showModal} transparent animationType="slide">
              <SafeAreaViewStyle>
                <ModalOutsideTouchable
                  accessibilityRole="button"
                  activeOpacity={1}
                  onPressOut={this.onPressOut}
                >
                  <ModalOverlay />
                </ModalOutsideTouchable>
                {!showSortModal && (
                  <ModalContent>
                    <ModalTitleContainer>
                      <ModalTitle>{labelsFilter.lbl_filter_by}</ModalTitle>
                      <ModalCloseTouchable
                        isButton
                        onPress={this.onCloseModal}
                        accessibilityRole="button"
                        accessibilityLabel="Close button"
                      >
                        <ImageComp source={closeIcon} width={14} height={14} />
                      </ModalCloseTouchable>
                    </ModalTitleContainer>
                    {this.renderFilters({
                      labelsFilter,
                      setSelectedFilter,
                      filters,
                      isFavorite,
                      onFilterSelection,
                      filteredId,
                      isLoadingMore,
                    })}
                  </ModalContent>
                )}

                {showSortModal && (
                  <SortContent>
                    <DropDown
                      selectedValue={language}
                      data={sortOptions}
                      // eslint-disable-next-line sonarjs/no-identical-functions
                      onValueChange={(itemValue) => {
                        this.setState({ language: itemValue }, () => this.handleClick(itemValue));
                      }}
                      variation="primary"
                      dropDownStyle={{ ...dropDownStyle }}
                      itemStyle={{ ...itemStyle }}
                      highlightStyle={{ ...highlightStyle }}
                      bounces={false}
                      selectedItemFontWeight="extrabold"
                      dropDownItemFontWeight="regular"
                      onPressOut={this.onPressOut}
                      openDropdownOnLoad
                      isAnimateList={false}
                    />
                  </SortContent>
                )}
              </SafeAreaViewStyle>
            </Modal>
          </>
        )}
      </Container>
    );
  }
}

export default withStyles(FilterModal, styles);
export { FilterModal as FilterModalVanilla };
