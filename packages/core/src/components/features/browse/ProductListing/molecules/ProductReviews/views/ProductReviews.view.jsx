// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { getViewportInfo, getLocator } from '@tcp/core/src/utils';
import cssClassName from '../../../../../../../utils/cssClassName';
import withStyles from '../../../../../../common/hoc/withStyles';
import ProductReviewsStyle from '../ProductReviews.style';
import BodyCopy from '../../../../../../common/atoms/BodyCopy/views/BodyCopy';

const containerDivId = 'BVRRContainer-wrapper';
const accordionExpandedClass = 'accordion-expanded';
const ratingsAccordianClass = 'ratings-and-reviews-accordion';
class ProductReviews extends React.PureComponent {
  static onBVShowEvent() {
    const collapsedAccordion = document.querySelector(
      '.ratings-and-reviews-accordion:not(.accordion-expanded)'
    );
    if (collapsedAccordion) {
      document.querySelector(`.${ratingsAccordianClass}`).classList.add(accordionExpandedClass);
    }
  }

  constructor(props, context) {
    super(props, context);

    this.state = {
      isReviewsAvailable: false,
      fromStarRatingClick: false,
    };
    this.captureContainerRef = this.captureContainerRef.bind(this);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.bindWriteReviewClick = this.bindWriteReviewClick.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }

  componentDidMount = () => {
    window.bvCallback = (BV) => {
      BV.reviews.on('show', ProductReviews.onBVShowEvent);
    };
  };

  componentDidUpdate(prevProps) {
    const { isGuest: newLoggedInState } = this.props;
    const { isGuest: oldLoggedInState } = prevProps;

    if (oldLoggedInState !== newLoggedInState) {
      this.bindWriteReviewClick();
    }
  }

  componentWillUnmount() {
    const buttons = document.querySelectorAll(
      `button.bv-write-review, #WAR, #WAR-login1, #WAR-login2, button.bv-write-review-label`
    );
    [].forEach.call(buttons, (button) => {
      if (button.id === 'WAR') {
        if (document.querySelector('#WAR-login2')) {
          document.querySelector('#WAR-login2').removeEventListener('click', this.handleLoginClick);
        }
        if (document.querySelector('#WAR-login1')) {
          document.querySelector('#WAR-login1').removeEventListener('click', this.handleLoginClick);
        }
      } else {
        button.removeEventListener('click', this.handleLoginClick);
      }
    });

    if (document.getElementById(containerDivId)) {
      document
        .getElementById(containerDivId)
        .removeEventListener('DOMSubtreeModified', this.bindWriteReviewClick);
    }
  }

  guestUserEventBinding = () => {
    const buttons = document.querySelectorAll(
      `button.bv-write-review, #WAR, #WAR-login1, #WAR-login2, button.bv-write-review-label`
    );

    const topButton = document.querySelectorAll('.bv_button_buttonMinimalist');

    const bottomButton = document.querySelectorAll(
      'button.bv-write-review-label, button.bv-write-review'
    );

    document.querySelector('[name="bv:userToken"]').content = '';

    if (window.$BV) {
      window.$BV.configure('global', {
        userToken: '',
      });
    }

    if (topButton.length === 2 && bottomButton.length === 1) {
      document
        .getElementById(containerDivId)
        .removeEventListener('DOMSubtreeModified', this.bindWriteReviewClick);

      [].forEach.call(buttons, (button) => {
        if (button.id === 'WAR') {
          const WARCopy = document.querySelectorAll('#WAR-login1, #WAR-login2');
          const WARButtons = document.getElementsByClassName('bv_button_buttonMinimalist');
          if (!WARCopy.length) {
            const el1 = WARButtons[0];
            const elClone1 = el1.cloneNode(true);
            elClone1.id = 'WAR-login1';

            const el = WARButtons[1];
            const elClone = el.cloneNode(true);
            elClone.id = 'WAR-login2';
            el.parentNode.appendChild(elClone);
            elClone.addEventListener('click', this.handleLoginClick);

            el1.parentNode.appendChild(elClone1);
            elClone1.addEventListener('click', this.handleLoginClick);
          }
        } else {
          button.addEventListener('click', this.handleLoginClick);
        }
      });
    }
  };

  bindWriteReviewClick() {
    const getReviewLength = document.querySelectorAll(`.bv-section-summary-table`);
    this.setState({
      isReviewsAvailable: !!getReviewLength.length,
    });
    const containerElement = document.getElementById(containerDivId);
    if (containerElement) {
      containerElement.addEventListener('DOMSubtreeModified', this.bindWriteReviewClick);
    }
  }

  handleLoginClick(event) {
    const { isGuest, onLoginClick } = this.props;
    if (isGuest) {
      event.preventDefault();
      event.stopPropagation();
      const payload = { state: true, isAccountCardVisible: false };
      onLoginClick(payload);
    }
  }

  handleToggle() {
    const { handleRatingClick } = this.props;
    const expandedAccordion = document.querySelector(
      `.${ratingsAccordianClass}.${accordionExpandedClass}`
    );
    const accordionElement = document.querySelector('.ratings-and-reviews-accordion');
    if (!expandedAccordion) {
      accordionElement.classList.add(accordionExpandedClass);
    } else {
      handleRatingClick(false);
      accordionElement.classList.remove(accordionExpandedClass);
    }
  }

  captureContainerRef() {
    const { ratingsProductId } = this.props;
    document
      .getElementById(containerDivId)
      .addEventListener('DOMSubtreeModified', this.bindWriteReviewClick);

    const productIdsRatingSummary = document.querySelectorAll('[data-bv-product-id]');
    if (productIdsRatingSummary.length > 1) {
      productIdsRatingSummary[0].setAttribute('data-bv-product-id', ratingsProductId);
      productIdsRatingSummary[1].setAttribute('data-bv-product-id', ratingsProductId);
    }
  }

  render() {
    const { isReviewsAvailable } = this.state;
    const {
      className,
      isClient,
      ratingsProductId,
      ratingsAndReviewsLabel,
      bazaarVoice,
      starRatingClicked,
    } = this.props;
    if (!isClient) {
      return null;
    }
    const { isDesktop } = getViewportInfo() || {};
    if (starRatingClicked) {
      this.setState({
        fromStarRatingClick: starRatingClicked,
      });
    }
    const expandedAccordion = document.querySelector(
      `.ratings-and-reviews-accordion.${accordionExpandedClass}`
    );

    const { fromStarRatingClick } = this.state;

    if (!isDesktop && starRatingClicked && fromStarRatingClick && !expandedAccordion) {
      document.querySelector('.ratings-and-reviews-accordion').classList.add('accordion-expanded');
    }
    const accordionClassName = cssClassName('ratings-and-reviews-accordion');

    return (
      <div
        className={`${className} ${accordionClassName}`}
        data-locator={getLocator('pdp_rating_reviews')}
      >
        <BodyCopy
          className="accordion-button-toggle"
          component="div"
          fontSize="fs14"
          fontFamily="secondary"
          fontWeight="black"
          onClick={this.handleToggle}
          data-locator={getLocator('pdp_rating_reviews_title')}
        >
          {ratingsAndReviewsLabel.lbl_ratings_and_reviews}
          <span data-locator={getLocator('pdp_rating_reviews_count')}>
            {isReviewsAvailable && bazaarVoice && !!bazaarVoice.totalReviewCount
              ? `(${bazaarVoice.totalReviewCount})`
              : '(0)'}
          </span>
        </BodyCopy>
        <div
          data-bv-show="reviews"
          id="BVRRContainer-wrapper"
          className="ratings-and-reviews-container"
          ref={this.captureContainerRef}
          data-bv-product-id={ratingsProductId}
        />
      </div>
    );
  }
}

ProductReviews.propTypes = {
  ratingsProductId: PropTypes.string.isRequired,
  isClient: PropTypes.bool.isRequired,
  isGuest: PropTypes.bool.isRequired,
  className: PropTypes.string,
  bazaarVoice: PropTypes.shape({}).isRequired,
  onLoginClick: PropTypes.func,
  ratingsAndReviewsLabel: PropTypes.string,
  starRatingClicked: PropTypes.bool,
  handleRatingClick: PropTypes.func.isRequired,
};

ProductReviews.defaultProps = {
  className: '',
  ratingsAndReviewsLabel: '',
  onLoginClick: () => {},
  starRatingClicked: false,
};

export default withStyles(ProductReviews, ProductReviewsStyle);
export { ProductReviews as ProductReviewsVanilla };
