// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  position: relative;
  align-items: center;
  .new-product-category-text {
    margin-right: 6px;
    line-height: 1;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      font-size: 18px;
      line-height: 1.22;
    }
  }
  .new-product-count-text {
    line-height: 0.94;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      font-size: 12px;
      line-height: 1;
    }
  }
  @media ${props => props.theme.mediaQuery.mediumOnly} {
    margin-bottom: 16px;
  }
  @media ${props => props.theme.mediaQuery.large} {
    margin-bottom: 28px;
  }
`;
