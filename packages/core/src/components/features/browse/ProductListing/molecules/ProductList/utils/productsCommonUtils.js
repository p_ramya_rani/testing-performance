// 9fbef606107a605d69c0edbcd8029e5d
import isEmpty from 'lodash/isEmpty';
import groupBy from 'lodash/groupBy';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getVideoUrl } from '../views/ProductGridItem.util';

/**
 * @return the first element in the getMapSliceForColor(colorFitsSizesMap, colorName).fits array that corresponds to the given fit.
 * If there are no fits associated with the given color, then the first element in the array is returned.
 */
export function getMapSliceForFit(colorFitsSizesMap, colorName, fitName) {
  // eslint-disable-next-line
  const currentColorEntry = getMapSliceForColor(colorFitsSizesMap, colorName);
  if (!currentColorEntry) {
    return;
  }
  if (currentColorEntry.hasFits) {
    // eslint-disable-next-line
    return currentColorEntry.fits.find((entry) => {
      return entry.fitName === fitName || entry.fitNameVal === fitName;
    });
  }
  // eslint-disable-next-line
  return currentColorEntry.fits && currentColorEntry.fits.length > 0 && currentColorEntry.fits[0];
}

export function getMapSliceForSize(colorFitsSizesMap, colorName, fitName, sizeName) {
  const currentFitEntry = getMapSliceForFit(colorFitsSizesMap, colorName, fitName);
  if (!currentFitEntry) return;
  // eslint-disable-next-line
  return currentFitEntry.sizes.find((entry) => entry.sizeName === sizeName);
}

/**
 * @return the first element in the colorFitsSizesMap array that corresponds to the given colorName.
 */
export function getMapSliceForColor(colorFitsSizesMap, colorName) {
  return colorFitsSizesMap && colorFitsSizesMap.find((entry) => entry.color.name === colorName);
}

export function filterCurrentSelectedProduct(allProductId, getInitialFormVal) {
  return allProductId && allProductId.find((entry) => entry.auxdescription === getInitialFormVal);
}

export function getIconImageForColor(productInfo, colorId) {
  if (!productInfo.imagesByColor) return null;

  const imagesByColorEntry =
    productInfo.imagesByColor[colorId] ||
    productInfo.imagesByColor[Object.keys(productInfo.imagesByColor)[0]];
  return imagesByColorEntry.extraImages && imagesByColorEntry.extraImages[0]
    ? imagesByColorEntry.extraImages[0].iconSizeImageUrl
    : imagesByColorEntry.basicImageUrl;
}

export function getSkuId(colorFitsSizesMap, color, fit, size) {
  const currentSizeEntry = getMapSliceForSize(colorFitsSizesMap, color, fit, size);
  return currentSizeEntry && currentSizeEntry.skuId;
}

/**
 * @return the variant id selected by the user.
 */
export function getVariantId(colorFitsSizesMap, color, fit, size) {
  const currentSizeEntry = getMapSliceForSize(colorFitsSizesMap, color, fit, size);
  return currentSizeEntry && currentSizeEntry.variantId;
}

/**
 * @return the variant no selected by the user.
 */
export function getVariantNo(colorFitsSizesMap, color, fit, size) {
  const currentSizeEntry = getMapSliceForSize(colorFitsSizesMap, color, fit, size);
  return currentSizeEntry && currentSizeEntry.variantNo;
}

/**
 * Returns the list and offer prices corresponding to the sku with the given color, fit and size.
 */
export function getPrices(productInfo, color, fit, size) {
  const currentSizeEntry = getMapSliceForSize(productInfo.colorFitsSizesMap, color, fit, size);
  const currentColorEntry = getMapSliceForColor(productInfo.colorFitsSizesMap, color);
  if (currentSizeEntry && currentSizeEntry.listPrice) {
    return {
      listPrice: currentSizeEntry.listPrice,
      offerPrice: currentSizeEntry.offerPrice,
      badge2: currentColorEntry.miscInfo.badge2,
    };
  }

  if (currentColorEntry && currentColorEntry.listPrice) {
    return {
      listPrice: currentColorEntry.listPrice,
      offerPrice: currentColorEntry.offerPrice,
      badge2: currentColorEntry.miscInfo.badge2,
    };
  }

  return { listPrice: productInfo.listPrice, offerPrice: productInfo.offerPrice };
}

/**
 * Returns the list and offer prices corresponding to the sku with the given color, fit and size.
 */
export function getPricesWithRange(productInfo, color, fit, size, isSelectedSizeDisabled) {
  const currentSizeEntry = getMapSliceForSize(productInfo.colorFitsSizesMap, color, fit, size);
  if (currentSizeEntry && currentSizeEntry.listPrice && !isSelectedSizeDisabled) {
    return { listPrice: currentSizeEntry.listPrice, offerPrice: currentSizeEntry.offerPrice };
  }

  return {
    listPrice: productInfo.lowListPrice,
    offerPrice: productInfo.lowOfferPrice,
    highListPrice: productInfo.highListPrice || null,
    highOfferPrice: productInfo.highOfferPrice || null,
    onlyListPrice: productInfo.listPrice || null,
  };
}

/**
 * @return the first element in the colorFitsSizesMap array that corresponds to the given colorProductId.
 */
export function getMapSliceForColorProductId(colorFitsSizesMap, colorProductId) {
  const selectedProduct =
    colorFitsSizesMap &&
    colorFitsSizesMap.find(
      (entry) => entry.colorProductId === colorProductId || entry.colorDisplayId === colorProductId
    );
  return (
    selectedProduct ||
    (colorFitsSizesMap && colorFitsSizesMap.length > 0 ? colorFitsSizesMap[0] : null)
  );
}

/**
 * @return the first element in the colorFitsSizesMap array that corresponds to the given colorProductId.
 */
export function getMapSliceForSizeSkuID(colorProduct, size) {
  let skuId;
  if (colorProduct && colorProduct.fits && Array.isArray(colorProduct.fits)) {
    for (let i = 0; i < colorProduct.fits.length; i += 1) {
      const fitsMap = colorProduct.fits[i];
      for (let j = 0; j < fitsMap.sizes.length; j += 1) {
        const sizesMap = fitsMap.sizes[j];
        if (sizesMap.sizeName === size) {
          skuId = sizesMap;
          break;
        }
      }
    }
  }

  return skuId;
}

/**
 * @return the element flagged as default (or the first one) on the fits array
 */
export function getDefaultFitForColorSlice(colorFitsSizesMapEntry, ignoreQtyCheck = false) {
  return (
    colorFitsSizesMapEntry.fits.find(
      (fit) => !ignoreQtyCheck && fit.isDefault && fit.maxAvailable > 0
    ) ||
    colorFitsSizesMapEntry.fits.find((fit) => !ignoreQtyCheck && fit.maxAvailable > 0) ||
    colorFitsSizesMapEntry.fits[0]
  );
}

/**
 * @return if the product has a single fit with a single size common to all colors then return that common sizeName;
 * otherwise return the empty string.
 */
export function getDefaultSizeForProduct(colorFitsSizesMap) {
  const firstSizeName = colorFitsSizesMap[0].fits[0].sizes[0].sizeName;
  // eslint-disable-next-line
  for (let colorEnrtry of colorFitsSizesMap) {
    if (
      colorEnrtry.fits.length > 1 ||
      colorEnrtry.fits[0].sizes.length > 1 ||
      colorEnrtry.fits[0].sizes[0].sizeName !== firstSizeName
    ) {
      return '';
    }
  }
  return firstSizeName;
}

const getIsColorOnModelLegible = (curentColorEntry) =>
  curentColorEntry && curentColorEntry.miscInfo && curentColorEntry.miscInfo.hasOnModelAltImages;

/**
 * @summary This function will return an array of image paths to display
 * @param {Object} args
 * @param {Object} args.imagesByColor - the maping object holding all color's images
 * @param {Object} args.curentColorEntry - Object holding info of the currently selected Color
 * @param {Object} args.isAbTestActive - AB test flag to know if we should display
 * @param {Object} args.isFullSet - If true it will return all data from imagesByColor for given selection
 */
const getAltImages = (mainAndAltImages, excludeShortImage) => {
  return groupBy(mainAndAltImages, (img) => {
    if (!(excludeShortImage && img.isShortImage)) {
      return img.isOnModalImage ? 'onModelAltImages' : 'regularAltImages';
    }
    return 'shortImage';
  });
};

const getCurrentProductSelectedColor = (imagesByColor, curentColorEntry) => {
  const color = curentColorEntry?.color?.name || Object.keys(imagesByColor)[0];
  return imagesByColor[color].extraImages;
};
export const getImagesToDisplay = (args) => {
  const { imagesByColor, curentColorEntry, isAbTestActive, isFullSet, excludeShortImage } = args;
  let images = [];

  try {
    // See DTN-155 for image suffex value definitions
    const mainAndAltImages = isEmpty(imagesByColor)
      ? null
      : getCurrentProductSelectedColor(imagesByColor, curentColorEntry);
    const isColorOnModelLegible = getIsColorOnModelLegible(curentColorEntry);
    const altImages = getAltImages(mainAndAltImages, excludeShortImage);
    const { regularAltImages = null, onModelAltImages = null } = altImages;

    const imagesToDisplay =
      isAbTestActive && isColorOnModelLegible ? onModelAltImages : regularAltImages;
    // eslint-disable-next-line
    images = isFullSet
      ? imagesToDisplay
      : imagesToDisplay
      ? imagesToDisplay.map((imgData) => imgData.regularSizeImageUrl)
      : [];
  } catch (error) {
    logger.error(
      'ProductsGridItem: Backend sent us a bad color name so we dont know what image set to map to, see auxdescription in API call'
    );
  }
  return images;
};

export const getPrimaryImages = (args) => {
  const { imagesByColor, curentColorEntry, isAbTestActive, excludeShortImage } = args;
  let images = [];

  try {
    // See DTN-155 for image suffex value definitions
    const imageColorNames = Object.keys(imagesByColor);
    return (
      imageColorNames &&
      imageColorNames.map((colorName) => {
        const mainAndAltImages = isEmpty(imagesByColor)
          ? null
          : imagesByColor[colorName].extraImages;
        const isColorOnModelLegible = getIsColorOnModelLegible(curentColorEntry);
        const altImages = getAltImages(mainAndAltImages, excludeShortImage);
        const { regularAltImages = null, onModelAltImages = null } = altImages;
        const imagesToDisplay =
          isAbTestActive && isColorOnModelLegible ? onModelAltImages : regularAltImages;
        images = imagesToDisplay
          ? imagesToDisplay.map((imgData) => imgData.regularSizeImageUrl)
          : [];
        return images[0];
      })
    );
  } catch (error) {
    logger.error(
      'ProductsGridItem: Backend sent us a bad color name so we dont know what image set to map to, see auxdescription in API call'
    );
  }
  return images && images[0];
};

export const checkIsSelectedSizeDisabled = (productInfo, formData) => {
  if (formData.color && formData.size) {
    const currentFitInfo = getMapSliceForFit(
      productInfo.colorFitsSizesMap,
      formData.color,
      formData.fit
    );
    const sizesArray = currentFitInfo ? currentFitInfo.sizes : [];
    const currentSizeObj = sizesArray.filter((sizeObj) => sizeObj.sizeName === formData.size);
    return currentSizeObj.length ? currentSizeObj[0].maxAvailable <= 0 : true;
  }
  return false;
};

export const checkAndGetDefaultFitName = (fitName, colorName, colorFitsSizesMap) => {
  let defaultFitName = '';
  if (!fitName) {
    const currentColorInfo = getMapSliceForColor(colorFitsSizesMap, colorName);
    if (currentColorInfo && currentColorInfo.hasFits) {
      defaultFitName = getDefaultFitForColorSlice(currentColorInfo).fitName;
    }
  } else {
    return fitName;
  }
  return defaultFitName;
};

export const getFormattedLoyaltyText = (text) => {
  return text.replace(/\s+/g, ' ').trim();
};

export const getDefaultSizes = (formValues, productInfo, isShowDefaultSize) => {
  let showDefaultSizeMsg = false;
  const defaultSelection = {
    fit: null,
    size: null,
  };

  if (productInfo.categoryId && isShowDefaultSize) {
    // eslint-disable-next-line extra-rules/no-commented-out-code
    // defaultSelection = getCustomerSelection(productInfo.categoryId);
    showDefaultSizeMsg = !!(defaultSelection.size || defaultSelection.fit);
  }

  const formValuesWithDefaultSizes = {
    ...formValues,
    fit: defaultSelection.fit ? defaultSelection.fit : formValues.fit,
    size: defaultSelection.size ? defaultSelection.size : formValues.size,
  };
  const isSelectedSizeDisabled = checkIsSelectedSizeDisabled(
    productInfo,
    formValuesWithDefaultSizes
  );
  return isSelectedSizeDisabled ||
    (defaultSelection.size && formValues.size && defaultSelection.size !== formValues.size)
    ? { showDefaultSizeMsg: false, formValues }
    : { showDefaultSizeMsg, formValues: formValuesWithDefaultSizes };
};

/**
 * @method isProductOOS
 * @description checks if the selected size variant is having available
 * quantity
 */
export const isProductOOS = (colorFitsSizesMap, selectedSKu) => {
  const currentFitEntry = getMapSliceForFit(colorFitsSizesMap, selectedSKu.color, selectedSKu.Fit);
  if (currentFitEntry && currentFitEntry.sizes) {
    const selectedSKuProductInfo = currentFitEntry.sizes.find(
      (size) => size.sizeName === selectedSKu.Size
    );
    const maxAvailableProducts = selectedSKuProductInfo ? selectedSKuProductInfo.maxAvailable : 0;

    return maxAvailableProducts < 1;
  }
  return true;
};

/**
 * @method isBOSSProductOOSQtyMismatched
 * @description checks if the selected size variant is having available
 * quantity
 */
export const isBOSSProductOOSQtyMismatched = (colorFitsSizesMap, selectedSKu) => {
  const currentFitEntry = getMapSliceForFit(colorFitsSizesMap, selectedSKu.color, selectedSKu.fit);
  if (currentFitEntry && currentFitEntry.sizes) {
    const selectedSKuProductInfo = currentFitEntry.sizes.find(
      (size) => size.sizeName === selectedSKu.Size
    );
    const maxAvailableBossProducts = selectedSKuProductInfo
      ? selectedSKuProductInfo.maxAvailableBoss
      : 0;
    const qtyMismatch = selectedSKu.quantity > maxAvailableBossProducts;
    return maxAvailableBossProducts < 1 || qtyMismatch;
  }
  return true;
};

export const getProductListToPath = (str) => {
  if (str && str.indexOf('/b/') !== -1) {
    return `/b?bid=${str.split('/b/')[1]}`;
  }
  if (str && str.split('/p/') !== -1) {
    return `/p?pid=${str.split('/p/')[1]}`;
  }
  return str;
};

export const getProductListToPathInMobileApp = (str) => {
  let searchPath = str;
  if (str && str.indexOf('/p/') !== -1) {
    searchPath = `${str.split('/p/')[1]}`;
  } else if (str && str.indexOf('/b/') !== -1) {
    searchPath = `${str.split('/b/')[1]}`;
  }
  return searchPath;
};

export const getRearrangedImgUrls = (imageUrls, curentColorEntry) => {
  const videoUrl = getVideoUrl(curentColorEntry);
  if (videoUrl) {
    let completeVideoUrl = videoUrl;
    /* contruct complete URL, see if Unboxed can send the complete URL
    then this can be omitted */
    if (curentColorEntry.colorProductId) {
      const videoUrlPrefix = curentColorEntry.colorProductId.split('_')[0];
      completeVideoUrl = `${videoUrlPrefix}/${videoUrl}`;
    }
    // remove duplicate occurance of the video form imgUrls
    const indexOfVideo = imageUrls.indexOf(completeVideoUrl);
    if (indexOfVideo > -1) {
      imageUrls.splice(indexOfVideo, 1);
    }
    // push the video on first slot
    if (imageUrls && Array.isArray(imageUrls)) {
      imageUrls.unshift(completeVideoUrl);
    }
  }
  return imageUrls;
};

export const getSelectedColorData = (colorFitsSizesMap, colorId) => {
  return (
    colorFitsSizesMap &&
    colorFitsSizesMap.filter((colorItem) => {
      return colorItem.colorProductId === colorId;
    })
  );
};

export const getFeatures = (colorFitsSizesMap, colorId, isGiftCard) => {
  const selectedColorElement = getSelectedColorData(colorFitsSizesMap, colorId);

  const hasFits =
    selectedColorElement && selectedColorElement[0] && selectedColorElement[0].hasFits;

  let features = '';

  if (!isGiftCard) {
    features += 'alt images full size image';
  }
  if (hasFits) {
    features += `${features} size chart`;
  }
  return features;
};

export const setSelectedPack = (
  setSelectedMultipack,
  selectedMultipackNumber,
  TCPMultipackProductMapping
) => {
  let defaultSelectedPack = null;
  if (TCPMultipackProductMapping && TCPMultipackProductMapping.length > 0) {
    TCPMultipackProductMapping.forEach((ele) => {
      if (selectedMultipackNumber && Number(selectedMultipackNumber) === Number(ele)) {
        defaultSelectedPack = ele;
      }
    });
  }
  if (defaultSelectedPack != null) {
    setSelectedMultipack(defaultSelectedPack);
  }
};

export const getMapColorList = (TCPStyleColor, colorSlice) => {
  const getVal =
    TCPStyleColor &&
    TCPStyleColor.filter((x) => x.includes(colorSlice && colorSlice.colorDisplayId));
  return getVal && getVal[0] && getVal[0].split('#')[0] && getVal[0].split('#')[0].split('|');
};

export const closeQuickViewModalPopup = (fromPickup, props) => {
  const {
    closeQuickViewModal,
    clearAddToBagError,
    clearMultipleItemsAddToBagError,
    clearCheckoutServerError,
    setCartItemsSflError,
    productInfo,
    setSelectedMultipack,
  } = props;
  if (!fromPickup) {
    closeQuickViewModal();
  }
  clearAddToBagError();
  clearMultipleItemsAddToBagError();
  clearCheckoutServerError({});
  setCartItemsSflError(null);
  const { TCPMultipackProductMapping, TCPStyleType } = productInfo[0]?.product;
  setSelectedPack(setSelectedMultipack, TCPStyleType, TCPMultipackProductMapping);
};

export const getSoftClass = (softDisableMutiPack) => {
  const splitPack = softDisableMutiPack && softDisableMutiPack.split(';');
  return splitPack
    .map((packId) => {
      return packId && parseInt(packId.split('#')[1], 10);
    })
    .sort((a, b) => {
      return a - b;
    });
};

export const getFormValue = (initialFormValues, initialValues) => {
  return (
    (initialFormValues && initialFormValues.color) ||
    (initialValues && initialValues.color && initialValues.color.name)
  );
};

export const checkAvailablePill = (item, softClass, multipackToSelect, singleKey) => {
  return item === singleKey ||
    (item !== singleKey && softClass.includes(item) && item !== multipackToSelect) ||
    (item !== singleKey && !softClass.includes(item) && item === multipackToSelect)
    ? 'enable'
    : 'soft-disable';
};

export const getSoftDisableMutiPack = (getSelectedProductInformation) => {
  return getSelectedProductInformation && getSelectedProductInformation.multipack_mapping
    ? getSelectedProductInformation.multipack_mapping
    : null;
};

export const getIsDefaultProductBadge = (currentColorMiscInfo = {}) => {
  const { badge1 } = currentColorMiscInfo;
  let badge1Value;
  if (currentColorMiscInfo) {
    badge1Value = badge1?.matchBadge ? badge1?.matchBadge : badge1?.defaultBadge;
  }
  return badge1Value && badge1Value.includes('ONLINE');
};
