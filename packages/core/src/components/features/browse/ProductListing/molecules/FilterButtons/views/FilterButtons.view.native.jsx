// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import { styles, Container } from '../styles/FilterButtons.style.native';
import { Button } from '../../../../../../common/atoms';
import { BUTTON_VARIATION } from '../../../../../../common/atoms/Button';

/**
 * @param {Object} props : props for filter buttons
 * @desc This method generate filter buttons
 */
const FilterButtons = props => {
  const { labelsFilter, onPressFilter, onPressSort, selected, appliedFiltersCount } = props;
  const { mobileAppFilterIcon } = BUTTON_VARIATION;
  const filterText =
    appliedFiltersCount && appliedFiltersCount > 0
      ? `${labelsFilter.lbl_filter} (${appliedFiltersCount})`
      : labelsFilter.lbl_filter;
  return (
    <Container>
      <Button
        buttonVariation={mobileAppFilterIcon}
        type="button"
        data-locator="view_gallery_button"
        text={filterText}
        onPress={onPressFilter}
        showIcon
        width="48%"
        height={34}
        selected={selected}
      />
      <Button
        buttonVariation={mobileAppFilterIcon}
        type="button"
        data-locator="view_gallery_button"
        text={labelsFilter.lbl_sort}
        onPress={onPressSort}
        showIcon
        width="48%"
        height={34}
        selected={selected}
      />
    </Container>
  );
};

FilterButtons.propTypes = {
  props: PropTypes.shape({}),
  labelsFilter: PropTypes.shape({}),
  onPressFilter: PropTypes.func,
  onPressSort: PropTypes.func,
  selected: PropTypes.bool,
  appliedFiltersCount: PropTypes.number,
};

FilterButtons.defaultProps = {
  props: {},
  labelsFilter: {
    lbl_filter: 'FILTER',
    lbl_sort: 'SORT',
  },
  onPressFilter: () => {},
  onPressSort: () => {},
  selected: false,
  appliedFiltersCount: 0,
};

export default withStyles(FilterButtons, styles);
export { FilterButtons as FilterButtonsVanilla };
