// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { FastImage, Image, BodyCopy, Button } from '@tcp/core/src/components/common/atoms';

export const ColorImageWrapper = styled.View`
  display: flex;
  flex-direction: column;
  text-align: center;
  align-items: center;
  justify-content: center;
`;
export const ColorImage = styled(FastImage)``;

export const BorderImage = styled.View`
  border: ${props =>
    props.isChecked || props.isWhite
      ? `2px solid ${props.theme.colors.CHECKBOX.CHECKED_BORDER}`
      : 'none'};
  border-radius: ${props => (props.isWhite && !props.isChecked ? '30px' : '34px')};
  align-items: center;
  width: ${props => (props.isWhite && !props.isChecked ? '60px' : '68px')};
  height: ${props => (props.isWhite && !props.isChecked ? '60px' : '68px')};
`;

export const SelectedImage = styled(Image)`
  height: 20px;
  width: 20px;
  position: absolute;
  top: 35%;
  left: 35%;
`;

export const CarouselWrapper = styled.View`
  margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 0
    ${props => props.theme.spacing.ELEM_SPACING.MED} 0;
  height: 120px;
`;

export const ButtonWrapper = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export const CustomButton = styled(Button)`
  align-items: center;
`;

export const ClearColorAnchor = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export const BodyCopyText = styled(BodyCopy)`
  font-weight: ${props => (props.isChecked ? '900' : 'normal')};
  font-size: 12px;
  margin-top: ${props => (props.isWhite && !props.isChecked ? '20px' : '12px')};
  text-align: center;
`;

export const ImageStyle = {
  width: 56,
  height: 56,
  borderRadius: 56 / 2,
  marginTop: 2,
};
