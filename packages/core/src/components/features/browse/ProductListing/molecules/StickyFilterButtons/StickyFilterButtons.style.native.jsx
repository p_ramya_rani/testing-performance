// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';
import colors from '../../../../../../../styles/themes/TCP/colors';

const styles = css``;

export const RowViewContainer = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 2px;
  justify-content: center;
`;
export const ViewContainer = styled.View`
  padding-left: ${(props) => (props.showLeftImage ? '8px' : '3px')};
  padding-right: ${(props) => (props.showRightImage ? '8px' : '3px')};
`;
export const MainContainer = styled.TouchableOpacity`
  border-color: ${(props) =>
    props.showLeftImage ? colors.BUTTON.BLUE.NEW_FILTER_BTN : colors.PRIMARY.LIGHTERGRAY};
  flex-direction: row;
  justify-content: center;
  background-color: ${(props) =>
    props.isSelected ? colors.BUTTON.BLUE.NEW_FILTER_BTN : colors.WHITE};
  border-width: ${(props) => (props.showLeftImage ? '1.5px' : '1px')};
  border-radius: 25px;
  margin-left: 4px;
  margin-right: 4px;
  align-items: center;
  padding-left: 11px;
  padding-right: 11px;
  padding-top: 8px;
  padding-bottom: 8px;
  ${(props) => (props.disabled ? `opacity:0.3 ` : '')};
`;

export default styles;
