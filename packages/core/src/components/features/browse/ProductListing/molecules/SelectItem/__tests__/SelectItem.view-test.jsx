// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { SelectItemVanilla } from '../views/SelectItem.view';

describe('Select Item is shown', () => {
  const props = {
    className: 'Test',
    index: 0,
    content: {},
    highlighted: false,
    clickHandler: jest.fn(),
    highlightedRefCapturer: jest.fn(),
    docType: '',
    facetName: 'categoryPath2_uFilter',
    value: 'Sample Value',
    isAutosuggestAnalytics: 'PropTypes.string.isRequired',
    title: 'Sample Title',
    query: 'Sample Query',
  };
  it('should render select item ', () => {
    const component = shallow(<SelectItemVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
