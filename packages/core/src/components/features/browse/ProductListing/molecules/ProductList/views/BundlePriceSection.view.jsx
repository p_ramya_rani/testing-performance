// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import { BodyCopy } from '../../../../../../common/atoms';
import { getLocator, isGymboree } from '../../../../../../../utils';

const productPriceWrapper = (highOfferPrice, lowListPrice, lowOfferPrice) => {
  return (
    <React.Fragment>
      {!!highOfferPrice && !!lowOfferPrice && highOfferPrice !== lowOfferPrice ? (
        <BodyCopy
          dataLocator={getLocator('global_Price_text')}
          color={isGymboree() ? 'gray.900' : 'red.500'}
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize={['fs15', 'fs18', 'fs20']}
        >
          <PriceCurrency price={lowOfferPrice} />
          {' - '}
          <PriceCurrency price={highOfferPrice} />
        </BodyCopy>
      ) : (
        <BodyCopy
          dataLocator={getLocator('global_Price_text')}
          color={isGymboree() ? 'gray.900' : 'red.500'}
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize={['fs15', 'fs18', 'fs20']}
        >
          <PriceCurrency price={lowOfferPrice} />
        </BodyCopy>
      )}
    </React.Fragment>
  );
};

const showProductListPrices = (highListPrice, lowListPrice, lowOfferPrice) =>
  !!highListPrice &&
  !!lowListPrice &&
  highListPrice !== lowListPrice &&
  lowOfferPrice !== lowListPrice &&
  !!lowOfferPrice;

const productPriceWrapperForListPrice = (highListPrice, lowListPrice) => {
  return (
    <React.Fragment>
      {!!highListPrice && !!lowListPrice && highListPrice !== lowListPrice ? (
        <BodyCopy
          dataLocator={getLocator('global_Price_text')}
          color={isGymboree() ? 'gray.900' : 'red.500'}
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize={['fs15', 'fs18', 'fs20']}
        >
          <PriceCurrency price={lowListPrice} />
          {' - '}
          <PriceCurrency price={highListPrice} />
        </BodyCopy>
      ) : (
        <BodyCopy
          dataLocator={getLocator('global_Price_text')}
          color={isGymboree() ? 'gray.900' : 'red.500'}
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSize={['fs15', 'fs18', 'fs20']}
        >
          <PriceCurrency price={lowListPrice} />
        </BodyCopy>
      )}
    </React.Fragment>
  );
};

export default function (highListPrice, highOfferPrice, lowListPrice, lowOfferPrice, merchantTag) {
  return (
    <div className="container-price">
      {!lowOfferPrice
        ? productPriceWrapperForListPrice(highListPrice, lowListPrice)
        : productPriceWrapper(highOfferPrice, lowListPrice, lowOfferPrice)}

      {showProductListPrices(highListPrice, lowListPrice, lowOfferPrice) ? (
        <span className="list-price-container">
          <BodyCopy
            component="span"
            color="gray.700"
            fontFamily="secondary"
            fontWeight="semibold"
            fontSize={['fs10', 'fs12', 'fs14']}
            className="list-price"
          >
            <PriceCurrency price={lowListPrice} />
          </BodyCopy>
          <BodyCopy
            component="span"
            color="gray.700"
            fontFamily="secondary"
            fontWeight="semibold"
            fontSize={['fs10', 'fs12', 'fs14']}
          >
            {` - `}
          </BodyCopy>
          <BodyCopy
            component="span"
            color="gray.700"
            fontFamily="secondary"
            fontWeight="semibold"
            fontSize={['fs10', 'fs12', 'fs14']}
            className="list-price"
          >
            <PriceCurrency price={highListPrice} />
          </BodyCopy>
        </span>
      ) : (
        !!lowOfferPrice &&
        lowListPrice !== lowOfferPrice && (
          <BodyCopy
            component="span"
            color="gray.700"
            fontFamily="secondary"
            fontWeight="semibold"
            fontSize={['fs10', 'fs12', 'fs14']}
            className="list-price"
          >
            <PriceCurrency price={lowListPrice} />
          </BodyCopy>
        )
      )}
      {!!merchantTag && (
        <BodyCopy
          component="span"
          color="red.500"
          fontFamily="secondary"
          fontWeight="semibold"
          className="merchant-tag"
          fontSize={['fs10', 'fs12', 'fs14']}
        >
          {merchantTag}
        </BodyCopy>
      )}
    </div>
  );
}
