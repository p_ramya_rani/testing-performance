// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '../../../../../../utils/index';

const downArrowIcon = getIconPath('down_arrow_icon');
const upArrowIcon = getIconPath('up_arrow_icon');

export default css`
  border: ${(props) => props.theme.spacing.ELEM_SPACING.XS} solid
    ${(props) => props.theme.colorPalette.gray[300]};
  border-left: 0;
  border-right: 0;
  position: relative;
  background-color: ${(props) => props.theme.colors.WHITE};

  #BVRRContainer-wrapper {
    margin: 0 ${(props) => props.theme.gridDimensions.gridOffsetObj.small}px;
  }
  .ratings-and-reviews-container {
    display: none;
  }

  .accordion-button-toggle {
    background: url(${downArrowIcon}) no-repeat right 0 bottom 7px;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  &.accordion-expanded {
    .ratings-and-reviews-container {
      display: block;
    }
    .accordion-button-toggle {
      background: url(${upArrowIcon}) no-repeat right 0 bottom 7px;
      @media ${(props) => props.theme.mediaQuery.large} {
        background: none;
      }
    }
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    border: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
    border-left: 0;
    border-right: 0;

    .accordion-button-toggle {
      margin-bottom: 21px;
      background: none;
    }
    .ratings-and-reviews-container {
      display: block;
    }
  }

  #BVSpotlightsContainer {
    z-index: 1;
    position: relative;
  }

  .product-details-accordion {
    border-top: 7px solid ${(props) => props.theme.colors.ACCORDION.ACTIVE_HEADER};
  }
`;
