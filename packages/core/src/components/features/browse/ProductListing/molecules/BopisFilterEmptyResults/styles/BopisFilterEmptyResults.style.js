import { css } from 'styled-components';

export default css`
  &.bopis-filter-empty-results-view {
    width: 100%;
    background-color: ${(props) =>
      props.theme.isGymboree
        ? props.theme.colorPalette.orange[900]
        : props.theme.colorPalette.blue[1000]};
    height: auto;
    .msg-title {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy13}px;
      text-align: center;
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      color: ${(props) => props.theme.colors.WHITE};
      font-weight: ${(props) => props.theme.fonts.fontWeight.black};
      letter-spacing: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
      padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
      padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        padding: 24px 8px;
        font-size: 44px;
      }
    }
    .msg-description {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy9}px;
      text-align: center;
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      color: ${(props) => props.theme.colors.WHITE};
      font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
      line-height: 1.23;
      letter-spacing: ${(props) => props.theme.fonts.letterSpacing.normal};
      padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
      padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
`;
