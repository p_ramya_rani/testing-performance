/* eslint-disable max-lines */
/* eslint-disable sonarjs/no-extra-arguments */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { BodyCopy, Anchor } from '@tcp/core/src/components/common/atoms';
import PromotionalMessage from '@tcp/core/src/components/common/atoms/PromotionalMessage';
import { getColorMap } from '@tcp/core/src/utils/utils';
import { getLabelValue, getBrand, isGymboree, isAndroid } from '@tcp/core/src/utils';
import ErrorDisplay from '@tcp/core/src/components/common/atoms/ErrorDisplay';
import ProductRating from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductRating/ProductRating.view.native';
import PAGES from '@tcp/core/src/constants/pages.constants';
import { constructPageName, setRecommendationsObj } from '@tcp/core/src/utils/utils.app';
import names from '@tcp/core/src/constants/eventsName.constants';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import {
  styles,
  ListContainer,
  FavoriteIconContainer,
  Badge1Container,
  Badge1Text,
  Badge2Container,
  Badge2Text,
  PricesSection,
  OfferPriceAndBadge3Container,
  TitleContainer,
  TitleText,
  AddToBagContainer,
  AddToBagContainerRedesign,
  OfferPriceAndFavoriteIconContainer,
  ImageSectionContainer,
  RowContainer,
  OfferPriceAndBadge3View,
  CloseIconContainer,
  SuggestedContainer,
  SeeSuggestedContainer,
  RatingDefaultView,
  OfferPriceDefaultView,
  PlaceCashListContainer,
  PlaceCashDetailsContainer,
  PlaceCashPriceDetails,
  ListItemContainer,
  AddToBagViewContainer,
  BottomGridBanner,
  GridBannerText,
  InGridComponent,
  BannerBottom,
  TopCurveContainer,
  GridBannerTextBack,
  InGridEmptySpace,
  PriceAndColorContainer,
} from '../styles/ProductListItem.style.native';
import CustomButton from '../../../../../../common/atoms/Button';
import ButtonRedesign from '../../../../../../common/atoms/ButtonRedesign';

import ColorSwitch from '../../ColorSwitch';
import CustomIcon from '../../../../../../common/atoms/Icon';
import { ICON_FONT_CLASS, ICON_NAME } from '../../../../../../common/atoms/Icon/Icon.constants';
import ImageCarousel from '../../ImageCarousel';
import { getProductListToPathInMobileApp } from '../../ProductList/utils/productsCommonUtils';
import { AVAILABILITY } from '../../../../Favorites/container/Favorites.constants';
import { getCartItemInfo } from '../../../../../CnC/AddedToBag/util/utility';
import {
  IMG_DATA_PLP,
  VID_DATA_PLP_GIF_APP,
  VID_DATA_PLP_WEBP_APP,
} from '../../ProductList/config';
import Image from '../../../../../../common/atoms/Image';
import RecommendationsAbstractor from '../../../../../../../services/abstractors/common/recommendations';

const favorites = require('../../../../../../../../../mobileapp/src/assets/images/favorites.png');
const favoriteSolid = require('../../../../../../../../../mobileapp/src/assets/images/favorites_solid.png');

const fullWidthPer = '100';
const halfWidthPer = '48';

const TextProps = {
  text: PropTypes.string.isRequired,
  isNewReDesignProductTile: PropTypes.bool,
};

let renderVariation = false;
let priceRangeExist;
const handleFavoriteAddOrEdit = (
  colorProductId,
  item,
  addToBagEcom,
  onQuickViewOpenClick,
  isFavoriteEdit
) => {
  const {
    skuInfo: { skuId, size, fit, color },
  } = item;
  const { itemId, quantity, brand } = item.itemInfo;
  const itemBrand = brand;
  const orderInfo = {
    orderItemId: itemId,
    selectedQty: quantity,
    selectedColor: color,
    selectedSize: size,
    selectedFit: fit,
    skuId,
    itemBrand,
  };
  if (skuId && size) {
    if (isFavoriteEdit) {
      onQuickViewOpenClick({
        colorProductId,
        orderInfo,
        isFavoriteEdit: true,
      });
    } else if (addToBagEcom) {
      let cartItemInfo = getCartItemInfo(item, {});
      cartItemInfo = { ...cartItemInfo, originPage: 'slp' };
      addToBagEcom(cartItemInfo);
    }
  } else if (isFavoriteEdit) {
    onQuickViewOpenClick({
      colorProductId,
      orderInfo,
      isFavoriteEdit: true,
    });
  } else {
    onQuickViewOpenClick({
      colorProductId,
      orderInfo: {
        itemBrand,
      },
    });
  }
};

export const getProductId = (productItemData) => {
  return (
    (productItemData &&
      productItemData.generalProductId &&
      productItemData.generalProductId.split('_')[0]) ||
    null
  );
};

const getAnalyticData = (navigation) => {
  return {
    currentScreen: 'ProductDetail',
    pageData: {
      pageName: constructPageName(navigation),
      pageType: 'product',
      pageSection: 'product',
      pageSubSection: 'product',
    },
  };
};

// eslint-disable-next-line complexity, sonarjs/cognitive-complexity
const callAnalytics = async (
  viaModule,
  item,
  portalValue,
  trackAnalyticsOnAddToBagButtonClick,
  fromPage,
  navigation
) => {
  if (viaModule === 'Recommendation') {
    const recIdentifier = RecommendationsAbstractor.handleWhichRecommendationClicked(
      portalValue,
      fromPage
    );
    let recObj = {};
    if (item && item.productInfo && item.productInfo.generalProductId) {
      recObj = {
        recsPageType: fromPage,
        recsProductId: item.productInfo.generalProductId,
        recsType: recIdentifier ? `monetate-${recIdentifier}` : '',
      };
      await setRecommendationsObj(recObj);
    }
    const productItem =
      (item.productInfo &&
        item.productInfo.generalProductId && {
          color: getColorMap(item.productInfo),
          colorId: item.productInfo.generalProductId,
          extPrice: item.productInfo.offerPrice,
          features: 'alt images full size image',
          id: getProductId(item.productInfo),
          listPrice: item.productInfo.listPrice,
          name: item.productInfo.name,
          paidPrice: item.productInfo.offerPrice,
          plpClick: fromPage === 'browse' || false,
          price: item.productInfo.listPrice,
          pricingState: 'full price',
          rating: item.productInfo.ratings,
          reviews: item.productInfo.reviewsCount,
          searchClick: fromPage === 'search' || false,
          ...recObj,
        }) ||
      {};
    const customData = {
      products: [productItem],
      customEvents: [
        'prodView',
        'Product_Views_Custom_e1',
        'internalCampaignImpressions_e81',
        names.eVarPropEvent.event50,
      ],
      originType: `cross-sell:${viaModule}`,
    };
    const analyticsData = getAnalyticData(navigation);
    analyticsData.supressProps = ['eVar91'];
    trackAnalyticsOnAddToBagButtonClick(analyticsData, customData);
  }
};

const getEventsToMonetate = (props) => {
  const { categoryName, userEmail, styleWithEligibility, isPlcc, item, colorProductId } = props;
  let { page } = props;
  if (page === 'home page') page = Constants.HOMEPAGE;
  const { actionId = '' } = item || {};
  return {
    pageType: page || '',
    ...(colorProductId && { partNumber: colorProductId }),
    categoryName: categoryName || '',
    userEmail,
    styleWithEligibility,
    isPlcc,
    actionId: actionId.toString(),
  };
};

const handleOnQuickViewOpenClick = (
  payload,
  viaModule,
  onQuickViewOpenClick,
  loadSendEventsToMonetate,
  monetatePayload
) => {
  if (viaModule === Constants.RECOMMENDATION) loadSendEventsToMonetate(monetatePayload);
  onQuickViewOpenClick(payload);
};

const onCTAHandler = (props) => {
  const {
    item,
    selectedColorIndex,
    onGoToPDPPage,
    onQuickViewOpenClick,
    trackAnalyticsOnAddToBagButtonClick,
    isFavoriteOOS,
    setLastDeletedItemId,
    addToBagEcom,
    isFavoriteEdit,
    isFavorite,
    isSuggestedItem,
    onReplaceWishlistItem,
    fromPage,
    viaModule,
    navigation,
    portalValue,
    isSBP,
    setOpen = () => {},
    loadSendEventsToMonetate,
  } = props;
  setOpen(false);
  callAnalytics(
    viaModule,
    item,
    portalValue,
    trackAnalyticsOnAddToBagButtonClick,
    fromPage,
    navigation
  );
  const { productInfo, colorsMap } = item;
  const { pdpUrl, bundleProduct } = productInfo;
  const { colorProductId } = (colorsMap && colorsMap[selectedColorIndex || 0]) || item.skuInfo;
  const modifiedPdpUrl = getProductListToPathInMobileApp(pdpUrl) || '';
  const monetatePayload = getEventsToMonetate({ ...props, colorProductId });
  if (isSuggestedItem && onReplaceWishlistItem) {
    const { generalProductId } = productInfo;
    const { activeWishListId, suggestedOOSItemId } = props;
    const formData = {
      activeWishListId,
      itemId: suggestedOOSItemId,
      colorProductId: generalProductId,
      products: productInfo,
    };
    onReplaceWishlistItem(formData);
  } else if (isFavoriteOOS) {
    const {
      itemInfo: { itemId },
    } = item;
    setLastDeletedItemId({ itemId });
  } else if (bundleProduct) {
    onGoToPDPPage(modifiedPdpUrl, colorProductId, productInfo, item);
  } else if (isFavorite) {
    handleFavoriteAddOrEdit(
      colorProductId,
      item,
      addToBagEcom,
      onQuickViewOpenClick,
      isFavoriteEdit
    );
  } else {
    handleOnQuickViewOpenClick(
      {
        colorProductId,
        fromPage,
        isSBP,
      },
      viaModule,
      onQuickViewOpenClick,
      loadSendEventsToMonetate,
      monetatePayload
    );
  }
};

const getOOSButtonLabel = (isFavorite, outOfStockLabels, labelsPlpTiles) => {
  return isFavorite ? labelsPlpTiles.lbl_remove : outOfStockLabels.outOfStockCaps;
};

const getColorVariation = (LIGHTBLUE, ORANGE) => {
  return isGymboree() ? ORANGE : LIGHTBLUE;
};

const getBrandColor = (bundleProduct, { LIGHTBLUE, BLUE, ORANGE }) => {
  return bundleProduct ? getColorVariation(LIGHTBLUE, ORANGE) : BLUE;
};

const getButtonPadding = (placeReward) => {
  return placeReward ? '13px 4px 13px 4px' : '8px 4px 8px 4px';
};

export const getBorderRadius = (isNewReDesignProductTile) => {
  if (isNewReDesignProductTile && isGymboree()) return '23px';
  return isNewReDesignProductTile ? '16px' : '0px';
};

export const getViewStyling = (isNewReDesignProductTile) => {
  return isNewReDesignProductTile ? { flex: 1, marginHorizontal: 10 } : { flex: 1 };
};

const getText = (
  keepAlive,
  isNewReDesignProductTile,
  isFavoriteOOS,
  outOfStockLabels,
  labelsPlpTiles,
  buttonLabel
) => {
  return keepAlive && !isNewReDesignProductTile
    ? getOOSButtonLabel(isFavoriteOOS, outOfStockLabels, labelsPlpTiles)
    : buttonLabel;
};

// eslint-disable-next-line complexity
const renderAddToBagContainer = (
  props,
  keepAlive,
  selectedColorIndex,
  isNewReDesignProductTile,
  quickViewProductId,
  quickViewLoader
) => {
  const {
    item,
    renderPriceOnly,
    labelsPlpTiles,
    outOfStockLabels,
    isFavorite,
    isSuggestedItem,
    isPlaceCashReward,
    isCardTypeTiles,
  } = props;
  const { productInfo } = item;
  const { bundleProduct, uniqueId } = productInfo;
  if (renderVariation && renderPriceOnly) return null;
  let buttonLabel = '';
  if (isSuggestedItem) {
    buttonLabel = getLabelValue(labelsPlpTiles, 'lbl_add_to_favorites');
  } else if (bundleProduct) {
    buttonLabel = isGymboree()
      ? getLabelValue(labelsPlpTiles, 'lbl_plpTiles_shop_the_look')
      : getLabelValue(labelsPlpTiles, 'lbl_plpTiles_shop_collection');
  } else if (isNewReDesignProductTile) {
    buttonLabel = getLabelValue(labelsPlpTiles, 'lbl_add_to_bag_redesign');
  } else {
    buttonLabel = getLabelValue(labelsPlpTiles, 'lbl_add_to_bag');
  }

  const isFavoriteOOS = isFavorite && keepAlive;

  const ctaProps = {
    ...props,
    selectedColorIndex,
    isFavoriteOOS,
  };
  let buttonProps = null;
  if (isCardTypeTiles) {
    buttonProps = {
      buttonVariation: 'variable-width',
      height: '42px',
      textPadding: '0px 5px',
    };
    buttonLabel = getLabelValue(labelsPlpTiles, 'lbl_rcomm_atb_btn_redesign');
  }

  const showLoadingSpinner = uniqueId === quickViewProductId && quickViewLoader;

  return isNewReDesignProductTile || bundleProduct ? (
    <AddToBagContainerRedesign {...props}>
      <ButtonRedesign
        paddings="9px 5px"
        fill="BLUE"
        disableButton={keepAlive && !isFavorite}
        text={getText(
          keepAlive,
          isNewReDesignProductTile,
          isFavoriteOOS,
          outOfStockLabels,
          labelsPlpTiles,
          buttonLabel
        )}
        onPress={() => onCTAHandler(ctaProps)}
        accessibilityLabel={buttonLabel && buttonLabel.toLowerCase()}
        borderRadius={getBorderRadius(isNewReDesignProductTile)}
        fontSize={isCardTypeTiles ? 'fs12' : 'fs16'}
        fontFamily="secondary"
        fontWeight="regular"
        showLoadingSpinner={showLoadingSpinner}
        {...buttonProps}
      />
    </AddToBagContainerRedesign>
  ) : (
    <AddToBagContainer isPlaceCashReward={isPlaceCashReward}>
      <CustomButton
        paddings={getButtonPadding(isPlaceCashReward)}
        fill={getBrandColor(bundleProduct, {
          LIGHTBLUE: 'LIGHTBLUE',
          BLUE: 'BLUE',
          ORANGE: 'ORANGE',
        })}
        color="white"
        type="button"
        buttonVariation="variable-width"
        data-locator=""
        disableButton={keepAlive && !isFavorite}
        text={getText(
          keepAlive,
          isNewReDesignProductTile,
          isFavoriteOOS,
          outOfStockLabels,
          labelsPlpTiles,
          buttonLabel
        )}
        onPress={() => onCTAHandler(ctaProps)}
        accessibilityLabel={buttonLabel && buttonLabel.toLowerCase()}
        margin="0 0 0 0"
        padding="13px"
      />
    </AddToBagContainer>
  );
};

const onEditHandler = (props) => {
  const ctaProps = {
    ...props,
    isFavoriteEdit: true,
    isFavorite: true,
  };
  onCTAHandler(ctaProps);
};

const isItemOutOfStock = (isKeepAliveEnabled, keepAlive, itemInfo) => {
  return (
    (isKeepAliveEnabled && keepAlive) ||
    (itemInfo && itemInfo.availability === AVAILABILITY.SOLDOUT)
  );
};

const checkEditEnabled = (isFavorite, itemOutOfStock) => {
  return isFavorite && !itemOutOfStock;
};

const getKeepAlive = (isFavorite, itemInfo, miscInfoData) => {
  return isFavorite ? itemInfo.keepAlive : miscInfoData.keepAlive || false;
};

const checkInWishlist = (wishList, productId) =>
  wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;

const isHidePLPAddToBagButton = (isHidePLPAddToBag, isSuggestedItem) =>
  isHidePLPAddToBag && !isSuggestedItem;

// eslint-disable-next-line complexity
const ListItem = React.memo((props) => {
  const {
    item,
    badge1,
    badge2,
    loyaltyPromotionMessage,
    onFavorite,
    onFavoriteRemove,
    currencyExchange,
    currencySymbol,
    isPlcc,
    onGoToPDPPage,
    isFavorite,
    setLastDeletedItemId,
    fullWidth,
    renderPriceAndBagOnly,
    renderPriceOnly,
    productImageWidth,
    margins,
    paddings,
    isLoggedIn,
    labelsPlpTiles,
    isKeepAliveEnabled,
    outOfStockLabels,
    renderMoveToList,
    onSeeSuggestedItems,
    isSuggestedItem,
    outOfStockColorProductId,
    onDismissSuggestion,
    errorMessages,
    labelsFavorite,
    index,
    isOnModelAbTestPlp,
    hasSuggestedProduct,
    suggestedProductErrors,
    disableVideoClickHandler,
    isSwipeEnable,
    showPriceRange,
    noCarousel,
    accessibilityLabels,
    isHidePLPAddToBag,
    isHidePLPRatings,
    isOptimizedVideo,
    wishList,
    isBrierleyPromoEnabled,
    fromPage,
    isPlaceCashReward,
    isRecommendations,
    onSetLastDeletedItemIdAction,
    isNavL2,
    isCardTypeTiles,
    outOfStock,
    productImageHeight,
    isDynamicBadgeEnabled,
    tcpStyleType,
    portalValue,
    inGridPlpRecommendation,
    isNewReDesignProductTile,
    renderVideoAsImage,
    primaryBrand,
    onQuickViewOpenClick,
    quickViewProductId,
    quickViewLoader,
    alternateBrand,
  } = props;
  const [selectedColorIndex, setSelectedColorIndex] = useState(0);
  const { productInfo, colorsMap, itemInfo } = item;
  const { name, ratings, reviewsCount } = productInfo;
  const miscInfoData = colorsMap ? colorsMap[selectedColorIndex].miscInfo : productInfo;
  const colorMapData = colorsMap || [item.skuInfo];
  const keepAlive = getKeepAlive(isFavorite, itemInfo, miscInfoData);
  const itemOutOfStock = isItemOutOfStock(isKeepAliveEnabled, keepAlive, itemInfo);
  renderVariation = renderPriceAndBagOnly || renderPriceOnly;
  const { generalProductId, bundleProduct } = productInfo;
  const errorMsg =
    (errorMessages && get(errorMessages[generalProductId], 'errorMessage', null)) || null;
  const productImageWidthVal = 70;

  return isPlaceCashReward ? (
    <PlaceCashListContainer renderPriceAndBagOnly={renderVariation}>
      <PlaceCashDetailsContainer>
        <ImageSection
          item={item}
          selectedColorIndex={selectedColorIndex}
          onGoToPDPPage={onGoToPDPPage}
          productImageWidth={productImageWidthVal}
          isFavorite={isFavorite}
          keepAlive={itemOutOfStock}
          outOfStockLabels={outOfStockLabels}
          isBundleProduct={bundleProduct}
          isOnModelAbTestPlp={isOnModelAbTestPlp}
          noCarousel={noCarousel}
          disableVideoClickHandler={disableVideoClickHandler}
          isSwipeEnable={isSwipeEnable}
          isOptimizedVideo={isOptimizedVideo}
          isPlaceCashReward={isPlaceCashReward}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          tcpStyleType={tcpStyleType}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
        />
        <PlaceCashPriceDetails>
          <RenderPricesSection
            hideFavorite={renderPriceAndBagOnly}
            onFavorite={onFavorite}
            onFavoriteRemove={onFavoriteRemove}
            miscInfo={miscInfoData}
            currencyExchange={currencyExchange}
            currencySymbol={currencySymbol}
            setLastDeletedItemId={setLastDeletedItemId}
            isFavorite={checkInWishlist(wishList, item.productInfo.generalProductId) || isFavorite}
            itemInfo={isFavorite ? itemInfo : {}}
            productInfo={productInfo}
            item={item}
            isLoggedIn={isLoggedIn}
            accessibilityLabel="Price Section"
            isSuggestedItem={isSuggestedItem}
            itemOutOfStock={itemOutOfStock}
            labelsFavorite={labelsFavorite}
            index={index}
            labelsPlpTiles={labelsPlpTiles}
            showPriceRange={showPriceRange}
            accessibilityLabels={accessibilityLabels}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          />
          {!isHidePLPRatings && !!ratings && (
            <ProductRating
              ratings={ratings}
              reviewsCount={reviewsCount}
              isPlaceCashReward={isPlaceCashReward}
            />
          )}
          {!isHidePLPRatings && !ratings && <RatingDefaultView />}
        </PlaceCashPriceDetails>
      </PlaceCashDetailsContainer>
      {!isHidePLPAddToBagButton(isHidePLPAddToBag, isSuggestedItem) &&
        renderAddToBagContainer(props, itemOutOfStock, selectedColorIndex)}
    </PlaceCashListContainer>
  ) : (
    <ListContainer
      isRecommendations={isRecommendations}
      width={fullWidth ? fullWidthPer : halfWidthPer}
      renderPriceAndBagOnly={renderVariation}
      margins={margins}
      paddings={paddings}
      isNavL2={isNavL2}
      isCardTypeTiles={isCardTypeTiles}
      outOfStock={outOfStock}
      inGridPlpRecommendation={inGridPlpRecommendation}
      isNewReDesignProductTile={isNewReDesignProductTile}
      isBundleProduct={bundleProduct}
    >
      {(isNavL2 || isCardTypeTiles === true) && (
        <ListItemContainer outOfStock={outOfStock}>
          <RenderTopBadge1 text={badge1} isNewReDesignProductTile={isNewReDesignProductTile} />
          <RenderCloseIcon
            isSuggestedItem={isSuggestedItem}
            outOfStockColorProductId={outOfStockColorProductId}
            onDismissSuggestion={onDismissSuggestion}
          />
          <ImageSection
            item={item}
            selectedColorIndex={selectedColorIndex}
            onGoToPDPPage={onGoToPDPPage}
            productImageWidth={productImageWidth}
            isFavorite={isFavorite}
            keepAlive={itemOutOfStock}
            outOfStockLabels={outOfStockLabels}
            isBundleProduct={bundleProduct}
            isOnModelAbTestPlp={isOnModelAbTestPlp}
            noCarousel={noCarousel}
            disableVideoClickHandler={disableVideoClickHandler}
            isSwipeEnable={isSwipeEnable}
            isOptimizedVideo={isOptimizedVideo}
            isNavL2={isNavL2}
            isCardTypeTiles={isCardTypeTiles}
            productImageHeight={productImageHeight}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            tcpStyleType={tcpStyleType}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
          />
          <RenderBadge2 text={badge2} />
          {checkEditEnabled(isFavorite, itemOutOfStock) && (
            <Anchor
              fontSizeVariation="medium"
              fontFamily="secondary"
              underline
              anchorVariation="custom"
              onPress={() => onEditHandler(props)}
              dataLocator=""
              text={labelsPlpTiles.lbl_edit}
              colorName="gray.900"
            />
          )}
          <RenderPricesSection
            hideFavorite={renderPriceAndBagOnly}
            onFavorite={onFavorite}
            onFavoriteRemove={onFavoriteRemove}
            miscInfo={miscInfoData}
            currencyExchange={currencyExchange}
            currencySymbol={currencySymbol}
            setLastDeletedItemId={setLastDeletedItemId}
            isFavorite={checkInWishlist(wishList, item.productInfo.generalProductId) || isFavorite}
            itemInfo={isFavorite ? itemInfo : {}}
            productInfo={productInfo}
            item={item}
            isLoggedIn={isLoggedIn}
            accessibilityLabel="Price Section"
            isSuggestedItem={isSuggestedItem}
            itemOutOfStock={itemOutOfStock}
            labelsFavorite={labelsFavorite}
            index={index}
            labelsPlpTiles={labelsPlpTiles}
            showPriceRange={showPriceRange}
            accessibilityLabels={accessibilityLabels}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          />
          <RenderTitle
            text={name}
            onGoToPDPPage={onGoToPDPPage}
            selectedColorIndex={selectedColorIndex}
            item={item}
            productInfo={productInfo}
            colorsMap={colorMapData}
            alternateBrand={alternateBrand}
          />
          <RenderSuggestedLabel isSuggestedItem={isSuggestedItem} labelsPlpTiles={labelsPlpTiles} />
          <RenderColorSwitch
            colorsMap={colorMapData}
            item={item}
            setSelectedColorIndex={setSelectedColorIndex}
            isSuggestedItem={isSuggestedItem}
            isFavorite={isFavorite}
            widthPer={fullWidth ? fullWidthPer : halfWidthPer}
            onGoToPDPPage={onGoToPDPPage}
            productItem={item}
            productInfo={productInfo}
            fromPage={fromPage}
            primaryBrand={primaryBrand}
            onQuickViewOpenClick={onQuickViewOpenClick}
            alternateBrand={alternateBrand}
          />
          {isFavorite && <RenderSizeFit item={item} />}
          {loyaltyPromotionMessage ? (
            <PromotionalMessage
              isPlcc={isPlcc}
              text={loyaltyPromotionMessage}
              height="24px"
              marginTop={12}
              isBrierleyPromoEnabled={isBrierleyPromoEnabled}
            />
          ) : null}
          {!isHidePLPRatings && !!ratings && (
            <ProductRating ratings={ratings} reviewsCount={reviewsCount} />
          )}
          {!isHidePLPRatings && !ratings && <RatingDefaultView />}
          {!isHidePLPAddToBagButton(isHidePLPAddToBag, isSuggestedItem) &&
            renderAddToBagContainer(
              props,
              itemOutOfStock,
              selectedColorIndex,
              isCardTypeTiles,
              quickViewProductId,
              quickViewLoader
            )}
          {errorMsg && (
            <ErrorDisplay
              error={errorMsg}
              isBorder
              margins="12px 0 0 0"
              paddings="8px 8px 8px 8px"
            />
          )}
          <RenderDismissLink
            isSuggestedItem={isSuggestedItem}
            outOfStockColorProductId={outOfStockColorProductId}
            onDismissSuggestion={onDismissSuggestion}
            labelsPlpTiles={labelsPlpTiles}
          />
          {isFavorite && <RenderPurchasedQuantity item={item} />}
          {isFavorite && (
            <RenderMoveToListOrSeeSuggestedList
              item={item}
              index={index}
              labelsPlpTiles={labelsPlpTiles}
              renderMoveToList={renderMoveToList}
              onSeeSuggestedItems={onSeeSuggestedItems}
              hasSuggestedProduct={hasSuggestedProduct}
              suggestedProductErrors={suggestedProductErrors}
              labelsFavorite={labelsFavorite}
            />
          )}
        </ListItemContainer>
      )}
      {inGridPlpRecommendation && (
        <InGridComponent>
          <ImageSection
            item={item}
            selectedColorIndex={selectedColorIndex}
            onGoToPDPPage={onGoToPDPPage}
            productImageWidth={productImageWidth}
            productImageHeight={productImageHeight}
            isFavorite={isFavorite}
            keepAlive={itemOutOfStock}
            outOfStockLabels={outOfStockLabels}
            isBundleProduct={bundleProduct}
            isOnModelAbTestPlp={isOnModelAbTestPlp}
            noCarousel={noCarousel}
            disableVideoClickHandler={disableVideoClickHandler}
            isSwipeEnable={isSwipeEnable}
            isOptimizedVideo={isOptimizedVideo}
            isNavL2={isNavL2}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
          />
          <RenderBadge2 text={badge2} inGridPlpRecommendation />
          {checkEditEnabled(isFavorite, itemOutOfStock) && (
            <Anchor
              fontSizeVariation="medium"
              fontFamily="secondary"
              underline
              anchorVariation="custom"
              onPress={() => onEditHandler(props)}
              dataLocator=""
              text={labelsPlpTiles.lbl_edit}
              colorName="gray.900"
            />
          )}
          <RenderPricesSection
            hideFavorite={!renderPriceAndBagOnly}
            onFavorite={onFavorite}
            onFavoriteRemove={onFavoriteRemove}
            miscInfo={miscInfoData}
            currencyExchange={currencyExchange}
            currencySymbol={currencySymbol}
            setLastDeletedItemId={setLastDeletedItemId}
            isFavorite={checkInWishlist(wishList, item.productInfo.generalProductId) || isFavorite}
            itemInfo={isFavorite ? itemInfo : {}}
            productInfo={productInfo}
            item={item}
            isLoggedIn={isLoggedIn}
            accessibilityLabel="Price Section"
            isSuggestedItem={isSuggestedItem}
            itemOutOfStock={itemOutOfStock}
            labelsFavorite={labelsFavorite}
            index={index}
            labelsPlpTiles={labelsPlpTiles}
            showPriceRange={showPriceRange}
            accessibilityLabels={accessibilityLabels}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            inGridPlpRecommendation={inGridPlpRecommendation}
          />
          <RenderTitle
            text={name}
            onGoToPDPPage={onGoToPDPPage}
            selectedColorIndex={selectedColorIndex}
            item={item}
            productInfo={productInfo}
            colorsMap={colorMapData}
            inGridPlpRecommendation={inGridPlpRecommendation}
            alternateBrand={alternateBrand}
          />
          <RenderSuggestedLabel isSuggestedItem={isSuggestedItem} labelsPlpTiles={labelsPlpTiles} />
          <BannerBottom>
            <GridBannerTextBack>
              <GridBannerText>
                {portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS
                  ? labelsPlpTiles.lbl_plp_in_grid_description
                  : labelsPlpTiles.lbl_plp_in_multi_grid_description}
              </GridBannerText>
            </GridBannerTextBack>
            <BottomGridBanner />
          </BannerBottom>
          {isFavorite && <RenderSizeFit item={item} />}
          <InGridEmptySpace />
          {!isHidePLPRatings && !!ratings && (
            <ProductRating
              ratings={ratings}
              reviewsCount={reviewsCount}
              inGridPlpRecommendation={inGridPlpRecommendation}
            />
          )}
          {isRecommendations && !isHidePLPRatings && !ratings && <RatingDefaultView />}
          {!isHidePLPAddToBagButton(isHidePLPAddToBag, isSuggestedItem) && (
            <AddToBagViewContainer>
              {renderAddToBagContainer(props, itemOutOfStock, selectedColorIndex)}
            </AddToBagViewContainer>
          )}
          {errorMsg && (
            <ErrorDisplay
              error={errorMsg}
              isBorder
              margins="12px 0 0 0"
              paddings="8px 8px 8px 8px"
            />
          )}
          <RenderDismissLink
            isSuggestedItem={isSuggestedItem}
            outOfStockColorProductId={outOfStockColorProductId}
            onDismissSuggestion={onDismissSuggestion}
            labelsPlpTiles={labelsPlpTiles}
          />
          {isFavorite && <RenderPurchasedQuantity item={item} />}
          {isFavorite && (
            <RenderMoveToListOrSeeSuggestedList
              item={item}
              index={index}
              labelsPlpTiles={labelsPlpTiles}
              renderMoveToList={renderMoveToList}
              onSeeSuggestedItems={onSeeSuggestedItems}
              hasSuggestedProduct={hasSuggestedProduct}
              suggestedProductErrors={suggestedProductErrors}
              labelsFavorite={labelsFavorite}
            />
          )}
        </InGridComponent>
      )}
      {!inGridPlpRecommendation && !isNavL2 && !isCardTypeTiles && (
        <View style={getViewStyling(isNewReDesignProductTile)}>
          {!bundleProduct && (
            <RenderTopBadge1 text={badge1} isNewReDesignProductTile={isNewReDesignProductTile} />
          )}
          {isNewReDesignProductTile && !bundleProduct && (
            <RenderBadge2
              text={badge2}
              badgeOne={badge1}
              isNewReDesignProductTile={isNewReDesignProductTile}
            />
          )}
          {isNewReDesignProductTile && !bundleProduct && (
            <RenderFavoriteSection
              hideFavorite={renderPriceAndBagOnly}
              onFavorite={onFavorite}
              onFavoriteRemove={onFavoriteRemove}
              setLastDeletedItemId={setLastDeletedItemId}
              isFavorite={
                checkInWishlist(wishList, item.productInfo.generalProductId) || isFavorite
              }
              itemInfo={isFavorite ? itemInfo : {}}
              productInfo={productInfo}
              item={item}
              isLoggedIn={isLoggedIn}
              accessibilityLabel="Price Section"
              isSuggestedItem={isSuggestedItem}
              itemOutOfStock={itemOutOfStock}
              labelsFavorite={labelsFavorite}
              index={index}
              labelsPlpTiles={labelsPlpTiles}
              onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
              isNewReDesignProductTile={isNewReDesignProductTile}
            />
          )}
          <RenderCloseIcon
            isSuggestedItem={isSuggestedItem}
            outOfStockColorProductId={outOfStockColorProductId}
            onDismissSuggestion={onDismissSuggestion}
          />
          {!bundleProduct && <TopCurveContainer nonGrid />}
          <ImageSection
            item={item}
            selectedColorIndex={selectedColorIndex}
            onGoToPDPPage={onGoToPDPPage}
            productImageWidth={productImageWidth}
            productImageHeight={productImageHeight}
            isFavorite={isFavorite}
            keepAlive={itemOutOfStock}
            outOfStockLabels={outOfStockLabels}
            isBundleProduct={bundleProduct}
            isOnModelAbTestPlp={isOnModelAbTestPlp}
            noCarousel={noCarousel}
            disableVideoClickHandler={disableVideoClickHandler}
            isSwipeEnable={isSwipeEnable}
            isOptimizedVideo={isOptimizedVideo}
            isNavL2={isNavL2}
            isNewReDesignProductTile={isNewReDesignProductTile}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            tcpStyleType={tcpStyleType}
            renderVideoAsImage={renderVideoAsImage}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
          />
          {!isNewReDesignProductTile && <RenderBadge2 text={badge2} />}
          {checkEditEnabled(isFavorite, itemOutOfStock) && (
            <Anchor
              fontSizeVariation="medium"
              fontFamily="secondary"
              underline
              anchorVariation="custom"
              onPress={() => onEditHandler(props)}
              dataLocator=""
              text={labelsPlpTiles.lbl_edit}
              colorName="gray.900"
            />
          )}
          {isNewReDesignProductTile && (
            <PriceAndColorContainer>
              <RenderPricesSection
                hideFavorite={renderPriceAndBagOnly}
                onFavorite={onFavorite}
                onFavoriteRemove={onFavoriteRemove}
                miscInfo={miscInfoData}
                currencyExchange={currencyExchange}
                currencySymbol={currencySymbol}
                setLastDeletedItemId={setLastDeletedItemId}
                isFavorite={
                  checkInWishlist(wishList, item.productInfo.generalProductId) || isFavorite
                }
                itemInfo={isFavorite ? itemInfo : {}}
                productInfo={productInfo}
                item={item}
                isLoggedIn={isLoggedIn}
                accessibilityLabel="Price Section"
                isSuggestedItem={isSuggestedItem}
                itemOutOfStock={itemOutOfStock}
                labelsFavorite={labelsFavorite}
                index={index}
                labelsPlpTiles={labelsPlpTiles}
                showPriceRange={showPriceRange}
                accessibilityLabels={accessibilityLabels}
                onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                isNewReDesignProductTile={isNewReDesignProductTile}
              />
              {colorMapData && colorMapData.length > 1 && (
                <RenderColorSwitch
                  colorsMap={colorMapData}
                  item={item}
                  setSelectedColorIndex={setSelectedColorIndex}
                  isSuggestedItem={isSuggestedItem}
                  isFavorite={isFavorite}
                  widthPer={fullWidth ? fullWidthPer : halfWidthPer}
                  onGoToPDPPage={onGoToPDPPage}
                  productItem={item}
                  productInfo={productInfo}
                  fromPage={fromPage}
                  isNewReDesignProductTile={isNewReDesignProductTile}
                  primaryBrand={primaryBrand}
                  alternateBrand={alternateBrand}
                  showPriceRange={showPriceRange}
                  isBundleProduct={bundleProduct}
                  priceRangeExist={priceRangeExist}
                />
              )}
            </PriceAndColorContainer>
          )}
          {!isNewReDesignProductTile && (
            <RenderPricesSection
              hideFavorite={renderPriceAndBagOnly}
              onFavorite={onFavorite}
              onFavoriteRemove={onFavoriteRemove}
              miscInfo={miscInfoData}
              currencyExchange={currencyExchange}
              currencySymbol={currencySymbol}
              setLastDeletedItemId={setLastDeletedItemId}
              isFavorite={
                checkInWishlist(wishList, item.productInfo.generalProductId) || isFavorite
              }
              itemInfo={isFavorite ? itemInfo : {}}
              productInfo={productInfo}
              item={item}
              isLoggedIn={isLoggedIn}
              accessibilityLabel="Price Section"
              isSuggestedItem={isSuggestedItem}
              itemOutOfStock={itemOutOfStock}
              labelsFavorite={labelsFavorite}
              index={index}
              labelsPlpTiles={labelsPlpTiles}
              showPriceRange={showPriceRange}
              accessibilityLabels={accessibilityLabels}
              onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
              isNewReDesignProductTile={isNewReDesignProductTile}
            />
          )}
          <RenderTitle
            text={name}
            onGoToPDPPage={onGoToPDPPage}
            selectedColorIndex={selectedColorIndex}
            item={item}
            productInfo={productInfo}
            colorsMap={colorMapData}
            isNewReDesignProductTile={isNewReDesignProductTile}
            alternateBrand={alternateBrand}
          />
          <RenderSuggestedLabel isSuggestedItem={isSuggestedItem} labelsPlpTiles={labelsPlpTiles} />
          {!isNewReDesignProductTile ? (
            <RenderColorSwitch
              colorsMap={colorMapData}
              item={item}
              setSelectedColorIndex={setSelectedColorIndex}
              isSuggestedItem={isSuggestedItem}
              isFavorite={isFavorite}
              widthPer={fullWidth ? fullWidthPer : halfWidthPer}
              onGoToPDPPage={onGoToPDPPage}
              productItem={item}
              productInfo={productInfo}
              fromPage={fromPage}
              isNewReDesignProductTile={isNewReDesignProductTile}
              primaryBrand={primaryBrand}
              onQuickViewOpenClick={onQuickViewOpenClick}
              alternateBrand={alternateBrand}
            />
          ) : null}
          {isFavorite && <RenderSizeFit item={item} />}
          {loyaltyPromotionMessage && !isNewReDesignProductTile ? (
            <PromotionalMessage
              isPlcc={isPlcc}
              text={loyaltyPromotionMessage}
              height="24px"
              marginTop={12}
              isBrierleyPromoEnabled={isBrierleyPromoEnabled}
            />
          ) : null}
          {isNewReDesignProductTile && (
            <ProductRating
              ratings={ratings}
              reviewsCount={reviewsCount}
              isNewReDesignProductTile={isNewReDesignProductTile}
            />
          )}
          {loyaltyPromotionMessage && isNewReDesignProductTile ? (
            <PromotionalMessage
              isPlcc={isPlcc}
              text={loyaltyPromotionMessage}
              height="24px"
              marginTop={0}
              isBrierleyPromoEnabled={isBrierleyPromoEnabled}
              isNewReDesignProductTile={isNewReDesignProductTile}
            />
          ) : null}
          {!isHidePLPRatings && !!ratings && !isNewReDesignProductTile && (
            <ProductRating ratings={ratings} reviewsCount={reviewsCount} />
          )}
          {isRecommendations && !isHidePLPRatings && !ratings && <RatingDefaultView />}
          {!isHidePLPAddToBagButton(isHidePLPAddToBag, isSuggestedItem) && (
            <AddToBagViewContainer>
              {renderAddToBagContainer(
                props,
                itemOutOfStock,
                selectedColorIndex,
                isNewReDesignProductTile,
                quickViewProductId,
                quickViewLoader
              )}
            </AddToBagViewContainer>
          )}
          {errorMsg && (
            <ErrorDisplay
              error={errorMsg}
              isBorder
              margins="12px 0 0 0"
              paddings="8px 8px 8px 8px"
            />
          )}
          <RenderDismissLink
            isSuggestedItem={isSuggestedItem}
            outOfStockColorProductId={outOfStockColorProductId}
            onDismissSuggestion={onDismissSuggestion}
            labelsPlpTiles={labelsPlpTiles}
          />
          {isFavorite && !isNewReDesignProductTile && <RenderPurchasedQuantity item={item} />}
          {isFavorite && (
            <RenderMoveToListOrSeeSuggestedList
              item={item}
              index={index}
              labelsPlpTiles={labelsPlpTiles}
              renderMoveToList={renderMoveToList}
              onSeeSuggestedItems={onSeeSuggestedItems}
              hasSuggestedProduct={hasSuggestedProduct}
              suggestedProductErrors={suggestedProductErrors}
              labelsFavorite={labelsFavorite}
            />
          )}
        </View>
      )}
    </ListContainer>
  );
});

const RenderCloseIcon = ({ isSuggestedItem, outOfStockColorProductId, onDismissSuggestion }) => {
  if (isSuggestedItem) {
    return (
      <CloseIconContainer>
        <CustomIcon
          iconFontName={ICON_FONT_CLASS.Icomoon}
          name={ICON_NAME.large}
          size="fs10"
          color="gray.900"
          accessibilityLabel="close"
          isButton
          onPress={() => onDismissSuggestion(outOfStockColorProductId)}
        />
      </CloseIconContainer>
    );
  }
  return null;
};

const RenderSuggestedLabel = ({ isSuggestedItem, labelsPlpTiles }) => {
  if (isSuggestedItem) {
    return (
      <SuggestedContainer>
        <BodyCopy
          dataLocator="plp_offer_price"
          mobileFontFamily="secondary"
          fontSize="fs10"
          fontWeight="extrabold"
          color="white"
          text={getLabelValue(labelsPlpTiles, 'lbl_suggested')}
        />
      </SuggestedContainer>
    );
  }
  return null;
};

const RenderDismissLink = ({
  isSuggestedItem,
  outOfStockColorProductId,
  onDismissSuggestion,
  labelsPlpTiles,
}) => {
  if (isSuggestedItem) {
    return (
      <Anchor
        margins="12px 0 0 0"
        fontSizeVariation="large"
        fontFamily="secondary"
        underline
        anchorVariation="custom"
        onPress={() => onDismissSuggestion(outOfStockColorProductId)}
        dataLocator=""
        text={getLabelValue(labelsPlpTiles, 'lbl_dismiss')}
        colorName="gray.900"
        justifyContent="flex-start"
      />
    );
  }
  return null;
};

function getItemBrand(item) {
  const currentAppBrand = getBrand();
  return (
    item?.itemInfo?.brand?.toUpperCase() ||
    (currentAppBrand && currentAppBrand.toUpperCase()) ||
    'GYM'
  );
}

const RenderColorSwitch = (values) => {
  const {
    setSelectedColorIndex,
    colorsMap,
    isSuggestedItem,
    item,
    isFavorite,
    widthPer,
    onGoToPDPPage,
    productItem,
    productInfo,
    fromPage,
    isNewReDesignProductTile,
    primaryBrand,
    onQuickViewOpenClick,
    alternateBrand,
    showPriceRange,
    isBundleProduct,
  } = values;
  if (renderVariation || isSuggestedItem) return null;
  let itemBrand = getItemBrand(item);
  if (primaryBrand) {
    itemBrand = primaryBrand;
  }
  if (alternateBrand) {
    itemBrand = alternateBrand;
  }
  const isPLPPage = fromPage === PAGES.PLP || fromPage === PAGES.SLP;
  return (
    <ColorSwitch
      colorsMap={colorsMap}
      setSelectedColorIndex={setSelectedColorIndex}
      itemBrand={itemBrand}
      itemData={item}
      isFavorite={isFavorite}
      widthPer={widthPer}
      isPLPPage={isPLPPage}
      onGoToPDPPage={onGoToPDPPage}
      productItem={productItem}
      productInfo={productInfo}
      isNewReDesignProductTile={isNewReDesignProductTile}
      primaryBrand={primaryBrand}
      onQuickViewOpenClick={onQuickViewOpenClick}
      alternateBrand={alternateBrand}
      showPriceRange={showPriceRange}
      isBundleProduct={isBundleProduct}
      priceRangeExist={priceRangeExist}
    />
  );
};
const RenderTopBadge1 = ({ text, isNewReDesignProductTile }) => {
  if (renderVariation) return null;
  return (
    <Badge1Container isNewReDesignProductTile={isNewReDesignProductTile}>
      <Badge1Text
        accessible={text !== ''}
        accessibilityRole="text"
        accessibilityLabel={text}
        isNewReDesignProductTile={isNewReDesignProductTile}
      >
        {text}
      </Badge1Text>
    </Badge1Container>
  );
};

RenderTopBadge1.propTypes = TextProps;
RenderTopBadge1.defaultProps = {
  isNewReDesignProductTile: false,
};

const ImageSection = ({
  item,
  selectedColorIndex,
  onGoToPDPPage,
  productImageWidth,
  isFavorite,
  keepAlive,
  outOfStockLabels,
  isOnModelAbTestPlp,
  noCarousel,
  disableVideoClickHandler,
  isSwipeEnable,
  isOptimizedVideo,
  isPlaceCashReward,
  isNavL2,
  isCardTypeTiles,
  productImageHeight,
  isDynamicBadgeEnabled,
  tcpStyleType,
  inGridPlpRecommendation,
  renderVideoAsImage,
  isNewReDesignProductTile,
  primaryBrand,
  alternateBrand,
  isBundleProduct,
}) => {
  // TODO in case of having bundle product name key on cloudinary
  // const configData = isBundleProduct ? IMG_DATA_CLP : IMG_DATA_PLP;
  const configData = IMG_DATA_PLP;
  const videoTransformation = isAndroid() ? VID_DATA_PLP_WEBP_APP : VID_DATA_PLP_GIF_APP;
  let itemBrand = getItemBrand(item);
  if (primaryBrand) {
    itemBrand = primaryBrand;
  }
  if (alternateBrand) {
    itemBrand = alternateBrand;
  }

  return (
    <ImageSectionContainer
      isNavL2={isNavL2}
      isCardTypeTiles={isCardTypeTiles}
      inGridPlpRecommendation={inGridPlpRecommendation}
      isNewReDesignProductTile={isNewReDesignProductTile}
      isPlaceCashReward={isPlaceCashReward}
    >
      <ImageCarousel
        item={item}
        selectedColorIndex={selectedColorIndex}
        onGoToPDPPage={onGoToPDPPage}
        productImageWidth={productImageWidth}
        productImageHeight={isBundleProduct ? 220 : productImageHeight}
        isFavorite={isFavorite}
        keepAlive={keepAlive}
        imgConfig={configData.imgConfig[0]}
        videoTransformation={videoTransformation}
        outOfStockLabels={outOfStockLabels}
        itemBrand={itemBrand}
        isOnModelAbTestPlp={isOnModelAbTestPlp}
        noCarousel={noCarousel}
        disableVideoClickHandler={disableVideoClickHandler}
        isSwipeEnable={isSwipeEnable}
        isOptimizedVideo={isOptimizedVideo}
        isPlaceCashReward={isPlaceCashReward}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        tcpStyleType={tcpStyleType}
        inGridPlpRecommendation={inGridPlpRecommendation}
        renderVideoAsImage={renderVideoAsImage}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
      />
    </ImageSectionContainer>
  );
};

ImageSection.propTypes = {
  item: PropTypes.shape({}).isRequired,
  selectedColorIndex: PropTypes.number.isRequired,
  onGoToPDPPage: PropTypes.func.isRequired,
  productImageWidth: PropTypes.number,
  isFavorite: PropTypes.bool,
  keepAlive: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({
    outOfStockCaps: PropTypes.string,
  }),
  isOnModelAbTestPlp: PropTypes.bool,
  noCarousel: PropTypes.bool,
  disableVideoClickHandler: PropTypes.bool,
  isSwipeEnable: PropTypes.bool,
  isOptimizedVideo: PropTypes.bool,
  isPlaceCashReward: PropTypes.bool,
  isNavL2: PropTypes.bool,
  isCardTypeTiles: PropTypes.bool,
  productImageHeight: PropTypes.number,
  isBundleProduct: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  tcpStyleType: PropTypes.string,
  inGridPlpRecommendation: PropTypes.bool,
  renderVideoAsImage: PropTypes.bool,
  isNewReDesignProductTile: PropTypes.bool,
};

ImageSection.defaultProps = {
  productImageWidth: '',
  isFavorite: false,
  keepAlive: false,
  outOfStockLabels: {
    outOfStockCaps: '',
  },
  isOnModelAbTestPlp: false,
  noCarousel: false,
  disableVideoClickHandler: false,
  isBundleProduct: false,
  isSwipeEnable: true,
  isOptimizedVideo: false,
  isPlaceCashReward: false,
  isNavL2: false,
  isCardTypeTiles: false,
  productImageHeight: 0,
  isDynamicBadgeEnabled: {},
  tcpStyleType: '',
  inGridPlpRecommendation: false,
  renderVideoAsImage: false,
  isNewReDesignProductTile: false,
};

const RenderBadge2 = ({ text, inGridPlpRecommendation, badgeOne, isNewReDesignProductTile }) => {
  if (renderVariation) return null;
  return (
    <Badge2Container
      inGridPlpRecommendation={inGridPlpRecommendation}
      badgeOne={badgeOne}
      isNewReDesignProductTile={isNewReDesignProductTile}
    >
      <Badge2Text
        accessible={text !== ''}
        accessibilityRole="text"
        accessibilityLabel={text}
        isNewReDesignProductTile={isNewReDesignProductTile}
        badgeOne={badgeOne}
      >
        {text}
      </Badge2Text>
    </Badge2Container>
  );
};

RenderBadge2.propTypes = {
  text: PropTypes.string.isRequired,
  inGridPlpRecommendation: PropTypes.bool,
  badgeOne: PropTypes.string,
  isNewReDesignProductTile: PropTypes.bool,
};

RenderBadge2.defaultProps = {
  inGridPlpRecommendation: false,
  badgeOne: '',
  isNewReDesignProductTile: false,
};

/**
 * @description - This method calculate Price based on the given value
 */
const calculatePriceValue = (price, currencySymbol, currencyExchange, defaultReturn = 0) => {
  let priceValue = defaultReturn;
  if (price && price > 0) {
    priceValue = `${currencySymbol}${(price * currencyExchange[0].exchangevalue).toFixed(2)}`;
  }
  return priceValue;
};

export const getFontWeight = (isNewReDesignProductTile) => {
  let fontWeight = 'semibold';
  if (isAndroid()) fontWeight = 'bold';
  if (isNewReDesignProductTile) fontWeight = 'extrabold';
  return fontWeight;
};

const renderOfferPrice = (
  productInfo,
  currencySymbol,
  currencyExchange,
  bundleProduct,
  showPriceRange,
  miscInfo,
  isNewReDesignProductTile
) => {
  const lowOfferPrice = get(productInfo, 'priceRange.lowOfferPrice', productInfo.offerPrice || 0);
  const highOfferPrice = get(productInfo, 'priceRange.highOfferPrice', 0);
  const lowListPrice = get(productInfo, 'priceRange.lowListPrice', productInfo.listPrice || 0);
  const highListPrice = get(productInfo, 'priceRange.highListPrice', 0);
  const offerPriceValue = calculatePriceValue(
    miscInfo.offerPrice,
    currencySymbol,
    currencyExchange,
    null
  );
  let lowOfferPriceValue = calculatePriceValue(
    lowOfferPrice,
    currencySymbol,
    currencyExchange,
    null
  );
  let highOfferPriceValue = calculatePriceValue(
    highOfferPrice,
    currencySymbol,
    currencyExchange,
    null
  );

  if (!lowOfferPriceValue && !highOfferPriceValue) {
    lowOfferPriceValue = calculatePriceValue(lowListPrice, currencySymbol, currencyExchange, null);
    highOfferPriceValue = calculatePriceValue(
      highListPrice,
      currencySymbol,
      currencyExchange,
      null
    );
  }
  priceRangeExist = highOfferPriceValue;
  return (
    <BodyCopy
      margin="4px 0 0 0"
      dataLocator="plp_offer_price"
      mobileFontFamily="secondary"
      fontSize={isNewReDesignProductTile ? 'fs14' : 'fs15'}
      fontWeight={getFontWeight(isNewReDesignProductTile)}
      color={isGymboree() ? 'gray.900' : 'red.500'}
      text={
        (showPriceRange || bundleProduct) && highOfferPriceValue
          ? `${lowOfferPriceValue} - ${highOfferPriceValue}`
          : offerPriceValue
      }
    />
  );
};

const renderListPriceLabels = (value, accessibilityLabels) => {
  if (value) {
    return (
      <BodyCopy
        dataLocator="plp_filter_size_range"
        textDecoration="line-through"
        mobileFontFamily="secondary"
        fontSize="fs10"
        fontWeight="regular"
        color="gray.800"
        text={value}
        accessibilityText={`${accessibilityLabels && accessibilityLabels.previousPrice}: ${value}`}
      />
    );
  }
  return null;
};

const renderListPriceDash = (value) => {
  if (value) {
    return (
      <BodyCopy
        dataLocator="plp_filter_size_range"
        mobileFontFamily="secondary"
        fontSize="fs10"
        fontWeight="regular"
        color="gray.800"
        text=" - "
        accessibilityLabel="to"
      />
    );
  }
  return null;
};

const renderPricePercentageDiscountLabel = (value) => {
  if (value) {
    return (
      <BodyCopy
        margin="0 0 0 8px"
        dataLocator="plp_filter_size_range"
        mobileFontFamily="secondary"
        fontSize="fs10"
        fontWeight="regular"
        color="red.500"
        text={value}
        accessibilityLabel={`discount ${value}`}
      />
    );
  }
  return null;
};

const getListPrice = (showPriceRange, bundleProduct, lowListPriceValue, listPriceValue) =>
  showPriceRange || bundleProduct ? lowListPriceValue : listPriceValue;
const renderListPrice = (
  productInfo,
  currencySymbol,
  currencyExchange,
  badge3,
  bundleProduct,
  showPriceRange,
  accessibilityLabels
) => {
  const lowListPrice = get(productInfo, 'priceRange.lowListPrice', productInfo.listPrice || 0);
  const highListPrice = get(productInfo, 'priceRange.highListPrice', 0);
  const lowOfferPrice = get(productInfo, 'priceRange.lowOfferPrice', productInfo.offerPrice || 0);
  const listPriceValue = calculatePriceValue(
    productInfo.listPrice,
    currencySymbol,
    currencyExchange,
    null
  );
  const lowListPriceValue = calculatePriceValue(
    lowListPrice,
    currencySymbol,
    currencyExchange,
    null
  );
  const listPriceValues = getListPrice(
    showPriceRange,
    bundleProduct,
    lowListPriceValue,
    listPriceValue
  );
  const highListPriceValue = calculatePriceValue(
    highListPrice,
    currencySymbol,
    currencyExchange,
    null
  );
  if (lowOfferPrice && (lowListPrice > lowOfferPrice || highListPrice > lowListPrice)) {
    return (
      <OfferPriceAndBadge3Container>
        {renderListPriceLabels(listPriceValues, accessibilityLabels)}
        {(showPriceRange || bundleProduct) && renderListPriceDash(highListPriceValue)}
        {(showPriceRange || bundleProduct) && renderListPriceLabels(highListPriceValue)}
        {renderPricePercentageDiscountLabel(badge3)}
      </OfferPriceAndBadge3Container>
    );
  }
  return <OfferPriceAndBadge3View />;
};

const removeFavorite = (
  onFavoriteRemove,
  setLastDeletedItemId,
  generalProductId,
  itemId,
  productInfo,
  name,
  labelsFavorite
) => {
  onFavoriteRemove(generalProductId, itemId, productInfo);
  setLastDeletedItemId({
    itemId,
    products: productInfo,
    toastMessage: `${name} ${labelsFavorite.lbl_fav_removeFavorite}`,
  });
};

const renderFavoriteOnRemove = (args) => {
  const {
    onFavoriteRemove,
    setLastDeletedItemId,
    generalProductId,
    itemId,
    productInfo,
    name,
    labelsFavorite,
    height,
    width,
    labelsPlpTiles,
  } = args;
  return (
    <TouchableOpacity
      accessibilityRole="button"
      onPress={() =>
        removeFavorite(
          onFavoriteRemove,
          setLastDeletedItemId,
          generalProductId,
          itemId,
          productInfo,
          name,
          labelsFavorite
        )
      }
      accessibilityLabel={labelsPlpTiles.lbl_add_to_favorites}
    >
      <Image source={favoriteSolid} height={height} width={width} />
    </TouchableOpacity>
  );
};

const RenderFavoriteSection = (props) => {
  const {
    onFavorite,
    onFavoriteRemove,
    isFavorite,
    setLastDeletedItemId,
    itemInfo,
    hideFavorite,
    productInfo,
    isSuggestedItem,
    itemOutOfStock,
    labelsFavorite,
    index,
    labelsPlpTiles,
    isNewReDesignProductTile,
  } = props;
  const { itemId } = itemInfo;
  const { generalProductId, name } = productInfo || '';
  let width = 22;
  let height = 22;
  const bundleProduct = get(productInfo, 'bundleProduct', false);
  if (isNewReDesignProductTile) {
    width = 24;
    height = 24;
  }
  if (!hideFavorite && !bundleProduct && !isSuggestedItem && !itemOutOfStock) {
    return (
      <FavoriteIconContainer
        inGridPlpRecommendation
        isNewReDesignProductTile={isNewReDesignProductTile}
      >
        {isFavorite ? (
          renderFavoriteOnRemove({
            onFavoriteRemove,
            setLastDeletedItemId,
            generalProductId,
            itemId,
            productInfo,
            name,
            labelsFavorite,
            height,
            width,
            labelsPlpTiles,
          })
        ) : (
          <TouchableOpacity
            accessibilityRole="button"
            onPress={() => {
              onFavorite(generalProductId, itemId, productInfo, index);
            }}
            accessibilityLabel={labelsPlpTiles.lbl_add_to_favorites}
          >
            <Image source={favorites} height={height} width={width} />
          </TouchableOpacity>
        )}
      </FavoriteIconContainer>
    );
  }
  return null;
};

const RenderPriceOfferSection = (props) => {
  const {
    miscInfo,
    productInfo,
    currencySymbol,
    currencyExchange,
    badge3,
    bundleProduct,
    showPriceRange,
    accessibilityLabels,
  } = props;
  if (miscInfo && miscInfo.listPrice !== miscInfo.offerPrice) {
    return renderListPrice(
      productInfo,
      currencySymbol,
      currencyExchange,
      badge3,
      bundleProduct,
      showPriceRange,
      accessibilityLabels
    );
  }
  return <OfferPriceDefaultView />;
};

const RenderPricesSection = (values) => {
  const {
    miscInfo,
    currencyExchange,
    currencySymbol,
    onFavorite,
    onFavoriteRemove,
    isFavorite,
    setLastDeletedItemId,
    itemInfo,
    hideFavorite,
    productInfo,
    isSuggestedItem,
    itemOutOfStock,
    labelsFavorite,
    index,
    labelsPlpTiles,
    showPriceRange,
    accessibilityLabels,
    inGridPlpRecommendation,
    isNewReDesignProductTile,
  } = values;
  const { badge3 } = miscInfo;
  const { itemId } = itemInfo;
  const { generalProductId, name } = productInfo || '';
  const bundleProduct = get(productInfo, 'bundleProduct', false);
  const isFavoriteOOS = isFavorite && itemOutOfStock;
  const width = 16;
  const height = 16;
  return (
    <PricesSection
      isFavoriteOOS={isFavoriteOOS}
      inGridPlpRecommendation={inGridPlpRecommendation}
      isNewReDesignProductTile={isNewReDesignProductTile}
    >
      <OfferPriceAndFavoriteIconContainer>
        {renderOfferPrice(
          productInfo,
          currencySymbol,
          currencyExchange,
          bundleProduct,
          showPriceRange,
          miscInfo,
          isNewReDesignProductTile
        )}
        {!hideFavorite &&
          !bundleProduct &&
          !isSuggestedItem &&
          !itemOutOfStock &&
          !isNewReDesignProductTile && (
            <FavoriteIconContainer inGridPlpRecommendation>
              {isFavorite ? (
                renderFavoriteOnRemove({
                  onFavoriteRemove,
                  setLastDeletedItemId,
                  generalProductId,
                  itemId,
                  productInfo,
                  name,
                  labelsFavorite,
                  height,
                  width,
                  labelsPlpTiles,
                })
              ) : (
                <TouchableOpacity
                  accessibilityRole="button"
                  onPress={() => {
                    onFavorite(generalProductId, itemId, productInfo, index);
                  }}
                  accessibilityLabel={labelsPlpTiles.lbl_add_to_favorites}
                >
                  <Image source={favorites} height={16} width={16} />
                </TouchableOpacity>
              )}
            </FavoriteIconContainer>
          )}
      </OfferPriceAndFavoriteIconContainer>
      <RenderPriceOfferSection
        miscInfo={miscInfo}
        productInfo={productInfo}
        currencySymbol={currencySymbol}
        currencyExchange={currencyExchange}
        badge3={badge3}
        bundleProduct={bundleProduct}
        showPriceRange={showPriceRange}
        accessibilityLabels={accessibilityLabels}
      />
    </PricesSection>
  );
};

const RenderTitle = ({
  text,
  onGoToPDPPage,
  colorsMap,
  productInfo,
  selectedColorIndex,
  item,
  inGridPlpRecommendation,
  isNewReDesignProductTile,
}) => {
  const { pdpUrl } = productInfo;
  const modifiedPdpUrl = getProductListToPathInMobileApp(pdpUrl) || '';
  const { colorProductId } = (colorsMap && colorsMap[selectedColorIndex]) || item.skuInfo;
  if (renderVariation) return null;
  return (
    <TitleContainer
      onPress={() => onGoToPDPPage(modifiedPdpUrl, colorProductId, productInfo, item)}
      isNewReDesignProductTile={isNewReDesignProductTile}
    >
      <TitleText
        accessibilityRole="text"
        accessibilityLabel={text}
        numberOfLines={2}
        inGridPlpRecommendation={inGridPlpRecommendation}
      >
        {text}
      </TitleText>
    </TitleContainer>
  );
};

const RenderSizeFit = ({ item }) => {
  const { skuInfo } = item;
  const { fit, size } = skuInfo;
  if (fit || size) {
    return (
      <RowContainer margins="4px 0 12px 0">
        {size && (
          <BodyCopy
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="regular"
            text={size}
            textAlign="center"
          />
        )}
        {size && fit && (
          <BodyCopy
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="regular"
            text=" | "
            textAlign="center"
          />
        )}
        {fit && (
          <BodyCopy
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs12"
            fontWeight="regular"
            text={fit}
            textAlign="center"
          />
        )}
      </RowContainer>
    );
  }
  return <RowContainer margins="4px 0 12px 0" />;
};

RenderSizeFit.propTypes = {
  item: PropTypes.shape({
    skuInfo: PropTypes.shape({
      fit: PropTypes.string,
      size: PropTypes.string,
    }),
  }).isRequired,
};

const RenderPurchasedQuantity = ({ item }) => {
  const { quantityPurchased, itemInfo } = item;
  const { quantity } = itemInfo;
  return (
    <RowContainer margins="16px 0 0 0">
      <BodyCopy
        color="gray.900"
        fontFamily="secondary"
        fontSize="fs14"
        text={`${quantityPurchased}/${quantity} Purchased`}
        textAlign="center"
        fontWeight="regular"
      />
    </RowContainer>
  );
};

const onSeeSuggestedHandler = (item, onSeeSuggestedItems) => {
  const {
    skuInfo: { colorProductId },
    itemInfo: { itemId },
  } = item;
  if (colorProductId && onSeeSuggestedItems) {
    onSeeSuggestedItems(colorProductId, itemId);
  }
};

const RenderSuggestedError = (
  hasSuggestedProduct,
  suggestedProductErrors,
  colorProductId,
  labelsFavorite
) => {
  if (!hasSuggestedProduct && suggestedProductErrors && suggestedProductErrors === colorProductId) {
    return (
      <ErrorDisplay
        error={getLabelValue(labelsFavorite, 'lbl_fav_see_suggested_exception')}
        isBorder
        margins="8px 0 0 0"
        paddings="8px 8px 8px 8px"
      />
    );
  }
  return null;
};

const RenderMoveToListOrSeeSuggestedList = ({
  item,
  labelsPlpTiles,
  renderMoveToList,
  onSeeSuggestedItems,
  index,
  hasSuggestedProduct,
  suggestedProductErrors,
  labelsFavorite,
}) => {
  const {
    itemInfo: { availability, itemId },
    skuInfo: { colorProductId },
  } = item;
  if (availability && availability === 'SOLDOUT') {
    return (
      <SeeSuggestedContainer>
        <Anchor
          fontSizeVariation="large"
          fontFamily="secondary"
          underline
          anchorVariation="custom"
          onPress={() => onSeeSuggestedHandler(item, onSeeSuggestedItems)}
          dataLocator=""
          text={labelsPlpTiles.lbl_see_suggested_items}
          colorName="gray.900"
          justifyContent="flex-start"
        />
        {RenderSuggestedError(
          hasSuggestedProduct,
          suggestedProductErrors,
          colorProductId,
          labelsFavorite
        )}
      </SeeSuggestedContainer>
    );
  }

  return <RowContainer>{renderMoveToList && renderMoveToList(itemId, index)}</RowContainer>;
};

RenderMoveToListOrSeeSuggestedList.propTypes = {
  labelsPlpTiles: PropTypes.shape({}).isRequired,
  item: PropTypes.shape({
    quantityPurchased: PropTypes.string,
    itemInfo: PropTypes.shape({
      availability: PropTypes.string,
    }),
  }),
  renderMoveToList: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  onSeeSuggestedItems: PropTypes.func,
  hasSuggestedProduct: PropTypes.bool.isRequired,
  suggestedProductErrors: PropTypes.shape({}).isRequired,
  labelsFavorite: PropTypes.shape({}).isRequired,
};

RenderFavoriteSection.propTypes = {
  onFavorite: PropTypes.func.isRequired,
  onFavoriteRemove: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool.isRequired,
  setLastDeletedItemId: PropTypes.func.isRequired,
  itemInfo: PropTypes.shape({}),
  hideFavorite: PropTypes.bool.isRequired,
  productInfo: PropTypes.shape({}).isRequired,
  isSuggestedItem: PropTypes.bool.isRequired,
  itemOutOfStock: PropTypes.bool,
  labelsFavorite: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  labelsPlpTiles: PropTypes.shape.isRequired,
  isNewReDesignProductTile: PropTypes.bool,
};

RenderFavoriteSection.defaultProps = {
  itemInfo: {},
  itemOutOfStock: false,
  isNewReDesignProductTile: false,
};

RenderMoveToListOrSeeSuggestedList.defaultProps = {
  item: {
    itemInfo: {
      availability: 'OK',
    },
  },
  onSeeSuggestedItems: () => {},
};

RenderPurchasedQuantity.propTypes = {
  item: PropTypes.shape({
    quantityPurchased: PropTypes.string,
    itemInfo: PropTypes.shape({
      quantity: PropTypes.number,
    }),
  }).isRequired,
};

RenderTitle.propTypes = {
  text: PropTypes.string.isRequired,
  onGoToPDPPage: PropTypes.func.isRequired,
  colorsMap: PropTypes.shape({
    miscInfo: PropTypes.string,
  }),
  productInfo: PropTypes.shape({
    name: PropTypes.string,
    pdpUrl: PropTypes.string,
  }),
  selectedColorIndex: PropTypes.number,
  item: PropTypes.shape({
    skuInfo: PropTypes.string,
  }),
  inGridPlpRecommendation: PropTypes.bool,
  isNewReDesignProductTile: PropTypes.bool,
};

RenderPriceOfferSection.propTypes = {
  miscInfo: '',
  productInfo: PropTypes.shape({
    name: PropTypes.string,
    bundleProduct: PropTypes.shape({}),
  }),
  currencySymbol: PropTypes.string.isRequired,
  currencyExchange: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  badge3: PropTypes.string,
  bundleProduct: PropTypes.shape({}),
  showPriceRange: PropTypes.bool,
  accessibilityLabels: PropTypes.shape({}),
};

RenderPriceOfferSection.defaultProps = {
  miscInfo: PropTypes.string,
  productInfo: {
    name: '',
    pdpUrl: '',
  },
  badge3: '',
  bundleProduct: {},
  showPriceRange: false,
  accessibilityLabels: {},
};

RenderTitle.defaultProps = {
  colorsMap: {
    miscInfo: '',
  },
  productInfo: {
    name: '',
    pdpUrl: '',
  },
  selectedColorIndex: 0,
  item: {
    skuInfo: '',
  },
  inGridPlpRecommendation: false,
  isNewReDesignProductTile: false,
};

ListItem.propTypes = {
  theme: PropTypes.shape({}),
  item: PropTypes.shape({
    productInfo: PropTypes.shape({
      name: PropTypes.string,
      bundleProduct: PropTypes.shape({}),
    }),
    colorsMap: PropTypes.shape({}),
    itemInfo: PropTypes.shape({}),
    skuInfo: PropTypes.string,
  }),
  badge1: PropTypes.string,
  badge2: PropTypes.string,
  loyaltyPromotionMessage: PropTypes.string,
  onFavorite: PropTypes.func,
  onFavoriteRemove: PropTypes.func,
  isPlcc: PropTypes.bool,
  currencyExchange: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  currencySymbol: PropTypes.string.isRequired,
  onGoToPDPPage: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool,
  setLastDeletedItemId: PropTypes.func,
  fullWidth: PropTypes.bool,
  renderPriceAndBagOnly: PropTypes.bool,
  renderPriceOnly: PropTypes.bool,
  productImageWidth: PropTypes.number,
  margins: PropTypes.string,
  paddings: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  labelsPlpTiles: PropTypes.shape({}),
  accessibilityLabels: PropTypes.shape({}),
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  renderMoveToList: PropTypes.func,
  onSeeSuggestedItems: PropTypes.func,
  isSuggestedItem: PropTypes.bool,
  outOfStockColorProductId: PropTypes.string,
  onDismissSuggestion: PropTypes.func.isRequired,
  errorMessages: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  labelsFavorite: PropTypes.shape({}),
  isOnModelAbTestPlp: PropTypes.bool,
  hasSuggestedProduct: PropTypes.bool,
  suggestedProductErrors: PropTypes.shape({}),
  disableVideoClickHandler: PropTypes.bool,
  isSwipeEnable: PropTypes.bool,
  showPriceRange: PropTypes.bool,
  noCarousel: PropTypes.bool,
  isHidePLPAddToBag: PropTypes.bool,
  isHidePLPRatings: PropTypes.bool,
  isOptimizedVideo: PropTypes.bool,
  wishList: PropTypes.shape({}).isRequired,
  isBrierleyPromoEnabled: PropTypes.bool,
  isPlaceCashReward: PropTypes.bool,
  fromPage: PropTypes.string,
  isRecommendations: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  isNavL2: PropTypes.bool,
  isCardTypeTiles: PropTypes.bool,
  outOfStock: PropTypes.bool,
  productImageHeight: PropTypes.number,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  tcpStyleType: PropTypes.string,
  inGridPlpRecommendation: PropTypes.bool,
  portalValue: PropTypes.shape({}),
  isNewReDesignProductTile: PropTypes.bool,
  renderVideoAsImage: PropTypes.bool,
  quickViewLoader: PropTypes.bool,
  quickViewProductId: PropTypes.string,
};

ListItem.defaultProps = {
  theme: {},
  item: {
    productInfo: {
      name: '',
      bundleProduct: {},
    },
    colorsMap: {},
    itemInfo: {},
    skuInfo: '',
  },
  badge1: '',
  badge2: '',
  loyaltyPromotionMessage: '',
  onFavorite: () => {},
  isPlcc: false,
  isFavorite: false,
  setLastDeletedItemId: () => {},
  fullWidth: false,
  renderPriceAndBagOnly: false,
  renderPriceOnly: false,
  productImageWidth: '',
  margins: null,
  paddings: '12px 0 12px 0',
  isLoggedIn: false,
  labelsPlpTiles: {},
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  renderMoveToList: () => {},
  onSeeSuggestedItems: () => {},
  isSuggestedItem: false,
  isPlaceCashReward: false,
  outOfStockColorProductId: '',
  isCardTypeTiles: false,
  labelsFavorite: {
    lbl_fav_remove_item: 'was removed from your favorites.',
  },
  accessibilityLabels: {},
  isOnModelAbTestPlp: false,
  hasSuggestedProduct: false,
  suggestedProductErrors: {},
  disableVideoClickHandler: false,
  isSwipeEnable: true,
  showPriceRange: false,
  isHidePLPAddToBag: false,
  isHidePLPRatings: false,
  noCarousel: false,
  isOptimizedVideo: false,
  isBrierleyPromoEnabled: false,
  fromPage: '',
  isRecommendations: false,
  onFavoriteRemove: () => {},
  onSetLastDeletedItemIdAction: () => {},
  isNavL2: false,
  outOfStock: false,
  productImageHeight: 0,
  isDynamicBadgeEnabled: {},
  tcpStyleType: '',
  inGridPlpRecommendation: false,
  portalValue: {},
  isNewReDesignProductTile: false,
  renderVideoAsImage: false,
  quickViewLoader: false,
  quickViewProductId: '',
};

RenderDismissLink.propTypes = {
  isSuggestedItem: PropTypes.bool,
  outOfStockColorProductId: PropTypes.string,
  onDismissSuggestion: PropTypes.func.isRequired,
  labelsPlpTiles: PropTypes.shape({}),
};

RenderDismissLink.defaultProps = {
  isSuggestedItem: false,
  outOfStockColorProductId: '',
  labelsPlpTiles: {},
};

RenderSuggestedLabel.propTypes = {
  isSuggestedItem: PropTypes.bool,
  labelsPlpTiles: PropTypes.shape({}),
};

RenderSuggestedLabel.defaultProps = {
  isSuggestedItem: false,
  labelsPlpTiles: {},
};

renderAddToBagContainer.propTypes = {
  renderPriceOnly: PropTypes.bool,
  bundleProduct: PropTypes.shape({}),
  labelsPlpTiles: PropTypes.shape({}),
  outOfStockLabels: PropTypes.shape({}),
  isFavorite: PropTypes.bool,
  isSuggestedItem: PropTypes.bool,
  props: PropTypes.shape({}).isRequired,
  item: PropTypes.shape({}),
  isPlaceCashReward: PropTypes.bool,
  isCardTypeTiles: PropTypes.bool,
};

renderAddToBagContainer.defaultProps = {
  renderPriceOnly: false,
  bundleProduct: {},
  labelsPlpTiles: {},
  outOfStockLabels: {},
  isFavorite: false,
  isSuggestedItem: false,
  item: {},
  isPlaceCashReward: false,
  isCardTypeTiles: false,
};

RenderCloseIcon.propTypes = {
  isSuggestedItem: PropTypes.bool,
  outOfStockColorProductId: PropTypes.string,
  onDismissSuggestion: PropTypes.func.isRequired,
};

RenderCloseIcon.defaultProps = {
  isSuggestedItem: false,
  outOfStockColorProductId: '',
};

export default withStyles(ListItem, styles);
export { ListItem as ListItemVanilla };
