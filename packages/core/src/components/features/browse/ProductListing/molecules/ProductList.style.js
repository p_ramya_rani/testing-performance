// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  display: flex;
  flex-wrap: wrap;
  &.product-list.horizontal-promo {
    width: 100%;
    background: #eeeeee;
    @media ${props => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  &.product-tile,
  &.promo-div {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding: ${props => props.theme.spacing.ELEM_SPACING.SM} 10px;
    margin: 10px 0 6px 0;
    width: calc(50% - ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS});

    @media ${props => props.theme.mediaQuery.medium} {
      margin: 10px 0 ${props => props.theme.spacing.ELEM_SPACING.LRG} 0;
      padding: 12px ${props => props.theme.spacing.LAYOUT_SPACING.XS};
      width: calc(33.33% - ${props => props.theme.spacing.LAYOUT_SPACING.MED});
      align-content: center;
    }
    @media only screen and (min-width: 1350px) {
      margin: 10px 0 19px 0;
      padding: ${props => props.theme.spacing.ELEM_SPACING.SM} 21px;
      width: calc(25% - 42px);
    }
  }

  &.product-tile {
    padding-bottom: ${props => (props.isFavoriteView ? `18px` : '')};
  }

  ${props =>
    props.isFavoriteView
      ? `
    @media ${props.theme.mediaQuery.mediumOnly} {
    &.product-tile {
      width: calc(33% - 30px);
    }
    &.product-tile:nth-child(3n) {
      padding-right: 0;
    }
    &.product-tile:nth-child(3n + 1) {
      padding-left: 0;
    }
  }
  @media ${props.theme.mediaQuery.large} {
    &.product-tile {
      width: calc(25% - 32px);
    }
    &.product-tile:nth-child(4n) {
      padding-right: 0;
    }
    &.product-tile:nth-child(4n + 1) {
      padding-left: 0;
    }
  }
  `
      : ''};

  &.item-title {
    width: 100%;
    height: 22px;
    font-family: Nunito;
    font-size: ${props => props.theme.typography.fontSizes.fs16};
    font-weight: ${props => props.theme.typography.fontWeights.semibold};
    font-stretch: normal;
    font-style: normal;
    line-height: ${props => props.theme.fonts.lineHeight.normal};
    letter-spacing: ${props => props.theme.typography.letterSpacings.normal};
    color: ${props => props.theme.colorPalette.black};
  }

  .horizontal-bar {
    width: 100%;
    border-bottom-color: ${props => props.theme.colors.PRIMARY.LIGHTGRAY};
    border-bottom-width: 1px;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.L};
    margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
    padding-left: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
    padding-right: ${props => props.theme.spacing.ELEM_SPACING.XXS};

    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.L};
      border-bottom-width: 1px;
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
      margin-left: 0px;
    }

    @media ${props => props.theme.mediaQuery.large} {
      border-bottom-width: 1px;
      margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
      margin-left: 0px;
    }
  }
  .grid-item-wrapper {
    li {
      ${props => (props.isFavoriteView ? `margin: 0` : '')};
    }
  }

  &.multi-grid-view-4 {
    margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 21px;
    width: calc(100% - 42px);
    .multi-grid-product-tile {
      width: calc(25% - 31.5px);
      padding: 0px 21px;
    }
  }

  &.multi-grid-view-3 {
    margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 21px;
    width: calc(75% - 42px);
    .multi-grid-product-tile {
      width: calc(33.3% - 28px);
      padding: 0px 21px;
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      width: calc(100% - 48px);
      margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 24px;
    }
  }

  &.multi-grid-view-2 {
    margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 21px;
    width: calc(50% - 42px);
    .multi-grid-product-tile {
      width: calc(50% - 21px);
      padding: 0px 21px;
    }

    @media ${props => props.theme.mediaQuery.mediumOnly} {
      width: calc(66.6% - 48px);
      margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 24px;
    }

    @media ${props => props.theme.mediaQuery.smallOnly} {
      width: calc(100% - 20px);
      margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 10px;
      .multi-grid-product-tile {
        width: calc(50% - 10px);
        padding: 0px 10px;
      }
    }
  }

  &.multi-grid-view-1 {
    margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 21px;
    width: calc(25% - 42px);
    .multi-grid-product-tile {
      padding: 0px 21px;
      width: 100%;
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      width: calc(33.3% - 48px);
      margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 24px;
    }
    @media ${props => props.theme.mediaQuery.smallOnly} {
      width: calc(50% - 20px);
      margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 10px;
    }
  }

  .multi-grid-product-tile:first-child {
    padding-left: 0;
  }
  .multi-grid-product-tile:last-child {
    padding-right: 0;
  }

  &.multi-grid-recommendation {
    box-shadow: rgb(0 0 0 / 15%) 0px 2px 8px 0px;
    background-color: rgb(255, 255, 255);
    border-radius: 11px;
    display: inline-table !important;
    .recommendation-container {
      width: 100%;
      .item-container-inner {
        width: 100%;
      }
    }

    .in-grid-plp-rectangle {
      width: 90%;
      margin: 4px 14px 4px 0;
      min-height: 32px;
      background-color: ${props => props.theme.colors.PRIMARY.COLOR3};
      position: relative;
      display: table;
      border-top-right-radius: 8px;
      border-bottom-right-radius: 8px;
      @media ${props => props.theme.mediaQuery.smallOnly} {
        min-height: 23px;
        margin: 2px 14px 2px 0;
      }
      @media ${props => props.theme.mediaQuery.mediumOnly} {
        min-height: 27px;
        margin: 4px 14px 4px 0;
      }
    }
    .in-grid-plp-rectangle span {
      display: table-cell;
      vertical-align: middle;
      font-family: ${props => props.theme.typography.fonts.secondary};
      font-size: ${props => props.theme.fonts.fontSize.anchor.large}px;
      font-weight: ${props => props.theme.typography.fontWeights.black};
      font-stretch: normal;
      font-style: normal;
      line-height: 1;
      letter-spacing: 0.42px;
      padding-left: 10px;
      color: ${props => props.theme.colors.WHITE};
      @media ${props => props.theme.mediaQuery.mediumOnly} {
        font-size: ${props => props.theme.typography.fontSizes.fs12};
      }
      @media ${props => props.theme.mediaQuery.smallOnly} {
        font-size: ${props => props.theme.typography.fontSizes.fs10};
      }
      @media ${props => props.theme.mediaQuery.large} {
        padding-left: 14px;
      }
    }
    .in-grid-plp-rectangle:before,
    .in-grid-plp-rectangle:after {
      content: '';
      position: absolute;
    }

    .in-grid-plp-rectangle:after {
      border-left: 23px solid transparent;
      border-right: 23px solid white;
      border-top: 15px solid transparent;
      border-bottom: 18px solid transparent;
      right: -4px;
      top: 0;
      width: 0;
      height: 0;
      display: block;
      @media ${props => props.theme.mediaQuery.mediumOnly} {
        border-left: 20px solid transparent;
        border-right: 20px solid white;
        border-top: 14px solid transparent;
        border-bottom: 12px solid transparent;
        right: -2px;
      }
      @media ${props => props.theme.mediaQuery.smallOnly} {
        border-left: 18px solid transparent;
        border-right: 18px solid white;
        border-top: 11px solid transparent;
        border-bottom: 10px solid transparent;
        right: -1px;
      }
    }

    .fav-icon-wrapper {
      display: none;
    }

    .loyalty-text-container {
      span {
        display: none;
      }
    }

    .container-price,
    .product-title-container,
    .ranking-wrapper,
    .extended-sizes-text,
    .loyalty-text-container {
      padding-left: 10px;
      @media ${props => props.theme.mediaQuery.large} {
        padding-left: 14px;
      }
    }
  }
`;
