import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles from '../styles/BopisFilterEmptyResults.style';

const BopisFilterEmptyResultsView = ({ className, labels }) => {
  const interimSoldoutMessage = labels.interimSoldoutMessage
    ? labels.interimSoldoutMessage.split('#')
    : null;
  const title = interimSoldoutMessage[0] || '';
  const description = interimSoldoutMessage[1] || '';
  return (
    <div className={`${className} bopis-filter-empty-results-view`}>
      <div className="msg-title">{title.toLocaleUpperCase()}</div>
      <div className="msg-description">{description}</div>
    </div>
  );
};
BopisFilterEmptyResultsView.propTypes = {
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({
    interimSoldoutMessage: PropTypes.string,
  }).isRequired,
};
export { BopisFilterEmptyResultsView as BopisFilterEmptyResultsViewVanilla };
export default withStyles(BopisFilterEmptyResultsView, styles);
