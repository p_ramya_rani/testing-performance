// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, Dimensions } from 'react-native';
import { getDynamicBadgeQty } from '@tcp/core/src/utils';
import { IMG_DATA_PLP } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/config';
import {
  getImagesToDisplay,
  getMapSliceForColorProductId,
  getProductListToPathInMobileApp,
  getRearrangedImgUrls,
} from '../../ProductList/utils/productsCommonUtils';
import { DamImage } from '../../../../../../common/atoms';
import OutOfStockWaterMark from '../../../../ProductDetail/molecules/OutOfStockWaterMark';
import ImageTouchableOpacity from '../styles/ImageCarousel.style.native';

const win = Dimensions.get('window');
const numberOfColumn = 2;
const leftRightAndCenterMargin = 38;
const imageWidth = (win.width - leftRightAndCenterMargin) / numberOfColumn;
const imageHeight = 205;

class ImageCarousel extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { activeSlideIndex: 0, noCarousel: props.noCarousel };
  }

  setActiveSlideIndex = (index) => {
    const { activeSlideIndex } = this.state;
    if (index !== activeSlideIndex) {
      this.setState({
        activeSlideIndex: index,
      });
    }
  };

  onViewableItemsChanged = ({ changed }) => {
    const len = (changed && changed.length) || 0;
    for (let i = 0; i < len; i += 1) {
      const item = changed[i];
      const { isViewable, index } = item;
      if (isViewable) {
        this.setActiveSlideIndex(index);
        break;
      }
    }
  };

  renderOutOfStockOverlay = () => {
    const { keepAlive, outOfStockLabels } = this.props;
    return keepAlive ? <OutOfStockWaterMark label={outOfStockLabels.outOfStockCaps} /> : null;
  };

  renderItem = (imgSource, productInfo) => {
    const {
      productImageWidth,
      productImageHeight,
      imgConfig,
      itemBrand,
      disableVideoClickHandler,
      isSwipeEnable,
      isOptimizedVideo,
      videoTransformation,
      isPlaceCashReward,
      isDynamicBadgeEnabled,
      renderVideoAsImage,
      alternateBrand,
    } = this.props;
    const { tcpStyleType, tcpStyleQty } = productInfo;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      tcpStyleType,
      tcpStyleQty,
      isDynamicBadgeEnabled
    );

    const { index } = imgSource;
    const prodImageHeight = isPlaceCashReward ? 120 : productImageHeight;
    return (
      <DamImage
        alt={productInfo.name}
        key={index && index.toString()}
        url={isSwipeEnable ? imgSource.item : imgSource[0]}
        isProductImage
        imgConfig={imgConfig}
        height={prodImageHeight || imageHeight}
        width={productImageWidth || imageWidth}
        resizeMode="contain"
        itemBrand={itemBrand}
        isFastImage
        disableVideoClickHandler={disableVideoClickHandler}
        isOptimizedVideo={isOptimizedVideo}
        videoTransformation={videoTransformation}
        dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
        tcpStyleType={tcpStyleType}
        badgeConfig={IMG_DATA_PLP.badgeConfig}
        renderVideoAsImage={renderVideoAsImage}
        primaryBrand={alternateBrand}
      />
    );
  };

  manageRenderItem = (imgSource, modifiedPdpUrl, colorProductId, productInfo, activeSlideIndex) => {
    const { item, onGoToPDPPage, isPlaceCashReward, isStyleWith } = this.props;
    const { index } = imgSource;
    if (isStyleWith) {
      return (
        <ImageTouchableOpacity>{this.renderItem(imgSource, productInfo)}</ImageTouchableOpacity>
      );
    }
    return (
      <ImageTouchableOpacity
        onPress={() => onGoToPDPPage(modifiedPdpUrl, colorProductId, productInfo, item)}
        accessible={index && index === activeSlideIndex}
        accessibilityRole="image"
        isPlaceCashReward={isPlaceCashReward}
        accessibilityLabel={`${productInfo && productInfo.name} product image ${
          index && index + 1
        }`}
      >
        {this.renderItem(imgSource, productInfo)}
        {this.renderOutOfStockOverlay()}
      </ImageTouchableOpacity>
    );
  };

  onSwipeLeft = (len) => {
    this.setState({ noCarousel: false, activeSlideIndex: len > 1 ? 1 : 0 });
  };

  render() {
    const {
      item,
      selectedColorIndex,
      onGoToPDPPage,
      isFavorite,
      isOnModelAbTestPlp,
      isSwipeEnable,
    } = this.props;
    const { activeSlideIndex, noCarousel } = this.state;
    const { colorsMap, imagesByColor, productInfo } = item;
    const pdpUrl = productInfo ? productInfo.pdpUrl : item.pdpUrl;
    const modifiedPdpUrl = getProductListToPathInMobileApp(pdpUrl) || '';
    const { colorProductId } = (colorsMap && colorsMap[selectedColorIndex]) || item.skuInfo;
    const curentColorEntry = getMapSliceForColorProductId(colorsMap, colorProductId);
    let imageUrls = [];
    if (imagesByColor) {
      imageUrls = getImagesToDisplay({
        imagesByColor,
        curentColorEntry,
        isAbTestActive: isOnModelAbTestPlp,
        isFavoriteView: isFavorite,
      });
    }
    imageUrls = getRearrangedImgUrls(imageUrls, curentColorEntry);
    if (imageUrls && imageUrls.length === 0) {
      const imgSource = {
        index: colorProductId,
        item: 'no image',
      };
      return this.renderItem(imgSource, { name: 'No Image' });
    }
    if (noCarousel) {
      const imageSrc = { index: 0, item: imageUrls[0] };
      return this.manageRenderItem(
        imageSrc,
        modifiedPdpUrl,
        colorProductId,
        productInfo,
        item,
        onGoToPDPPage,
        0
      );
    }

    if (!isSwipeEnable) {
      return this.manageRenderItem(
        imageUrls,
        modifiedPdpUrl,
        colorProductId,
        productInfo,
        item,
        onGoToPDPPage,
        activeSlideIndex
      );
    }

    return (
      <FlatList
        onViewableItemsChanged={this.onViewableItemsChanged}
        viewabilityConfig={{
          itemVisiblePercentThreshold: 50,
        }}
        initialNumToRender={1}
        initialScrollIndex={activeSlideIndex}
        refreshing={false}
        data={imageUrls}
        pagingEnabled
        horizontal
        showsHorizontalScrollIndicator={false}
        listKey={(_, index) => index && index.toString()}
        renderItem={(imgSource) =>
          this.manageRenderItem(
            imgSource,
            modifiedPdpUrl,
            colorProductId,
            productInfo,
            item,
            onGoToPDPPage,
            activeSlideIndex
          )
        }
      />
    );
  }
}

ImageCarousel.propTypes = {
  item: PropTypes.shape({}),
  selectedColorIndex: PropTypes.number,
  imgConfig: PropTypes.string,
  onGoToPDPPage: PropTypes.func.isRequired,
  productImageWidth: PropTypes.number,
  productImageHeight: PropTypes.number,
  isFavorite: PropTypes.bool,
  keepAlive: PropTypes.bool,
  itemBrand: PropTypes.string.isRequired,
  outOfStockLabels: PropTypes.shape({
    outOfStockCaps: PropTypes.string,
  }),
  isOnModelAbTestPlp: PropTypes.bool,
  noCarousel: PropTypes.bool,
  disableVideoClickHandler: PropTypes.bool,
  isSwipeEnable: PropTypes.bool,
  isOptimizedVideo: PropTypes.bool,
  videoTransformation: PropTypes.string,
  isPlaceCashReward: PropTypes.bool,
  isStyleWith: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  renderVideoAsImage: PropTypes.bool,
};

ImageCarousel.defaultProps = {
  item: {},
  selectedColorIndex: 0,
  productImageWidth: null,
  productImageHeight: null,
  isFavorite: false,
  keepAlive: false,
  outOfStockLabels: {
    outOfStockCaps: '',
  },
  imgConfig: '',
  isOnModelAbTestPlp: false,
  noCarousel: false,
  disableVideoClickHandler: false,
  isSwipeEnable: true,
  isOptimizedVideo: false,
  videoTransformation: '',
  isPlaceCashReward: false,
  isStyleWith: false,
  isDynamicBadgeEnabled: {},
  renderVideoAsImage: false,
};

export default ImageCarousel;
export { ImageCarousel as ImageCarouselVanilla };
