// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf/RenderPerf';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { PAGE_NAVIGATION_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import { configureInternalNavigationFromCMSUrl } from '@tcp/core/src/utils';
import EmptyCategoryBannerStyle from '../EmptyCategoryBanner.style';
import withStyles from '../../../../../../common/hoc/withStyles';

import FixedBreadCrumbs from '../../FixedBreadCrumbs/views';
import { Row, Col, Anchor } from '../../../../../../common/atoms';
import GlobalNavigationMenuDesktopL2 from '../../GlobalNavigationMenuDesktopL2/views';
import ReadMore from '../../ReadMore/views';
import BopisFilter from '../../BopisFilter/views/BopisFilter';
import BopisFilterEmptyResults from '../../BopisFilterEmptyResults';

const returnAnchor = (navTree, labels) => {
  const departmentText = !!labels.labelForSplit[1] && labels.labelForSplit[1].split('#');

  return (
    <div className="department-msg">
      {!!departmentText[0] && departmentText[0]}
      {navTree && navTree.categoryContent && navTree.categoryContent.asPath && (
        <Anchor
          className="department-url"
          asPath={navTree.categoryContent.asPath}
          to={
            navTree.categoryContent.asPath &&
            configureInternalNavigationFromCMSUrl(navTree.categoryContent.asPath)
          }
        >
          {!!labels.clickHere && labels.clickHere}
        </Anchor>
      )}
      {!!departmentText[2] && `${departmentText[2]}!`}
    </div>
  );
};

const showCategoryAndDepartment = (
  interimSoldout,
  entityCategoryName,
  breadCrumbs,
  checkBreadCrumbAvailability,
  deptName,
  catName
) => {
  let newInterimSoldout = interimSoldout.replace('<Department Name>', deptName);
  newInterimSoldout = newInterimSoldout.replace('<Category Name>', catName);
  return newInterimSoldout;
};

const returnDeptName = (entityCategoryName, checkBreadCrumbAvailability, breadCrumbs) => {
  return entityCategoryName
    ? entityCategoryName.split(':')[0]
    : checkBreadCrumbAvailability && breadCrumbs[0].displayName;
};

const returnCategoryName = (entityCategoryName, checkBreadCrumbAvailability, breadCrumbs) => {
  const catName = entityCategoryName
    ? entityCategoryName.split(':')[entityCategoryName.split(':').length - 1]
    : checkBreadCrumbAvailability && breadCrumbs[breadCrumbs.length - 1].displayName;
  return catName || '';
};

const departmentNameCheck = (department) => {
  return department ? department.toUpperCase() : '';
};

const EmptyCategoryBanner = (props) => {
  const {
    className,
    currentNavIds,
    navTree,
    breadCrumbs,
    labels,
    entityCategoryName,
    plpSeoInfo,
    longDescription,
    isBopisFilterOn,
    formValues,
    getProducts,
    onSubmit,
    updateFormValue,
    setBopisFilterStateActn,
    isBopisFilterEnabled,
    resetSuggestedStores,
  } = props;

  const pluralMapping = {
    'TODDLER GIRL': 'Toddler Girls',
    'TODDLER BOY': 'Toddler Boys',
    'Family Shop': 'Family Shops',
    'Uniform Shop': 'Uniform Shops',
    GIRL: 'Girls',
    BOY: 'Boys',
  };

  const showNewBopisFilterEmptyMessage = isBopisFilterOn;
  const checkBreadCrumbAvailability = breadCrumbs && breadCrumbs[0] && breadCrumbs[0].displayName;
  if (plpSeoInfo && plpSeoInfo.length > 0) {
    labels.departmentName = plpSeoInfo[plpSeoInfo.length - 1].title;
    labels.categoryName = plpSeoInfo[0].title;
  } else {
    labels.departmentName = returnDeptName(
      entityCategoryName,
      checkBreadCrumbAvailability,
      breadCrumbs
    );
    labels.categoryName = returnCategoryName(
      entityCategoryName,
      checkBreadCrumbAvailability,
      breadCrumbs
    );
  }
  const dept = labels.departmentName;
  if (dept in pluralMapping) {
    labels.departmentName = pluralMapping[dept];
  }
  labels.departmentName = departmentNameCheck(labels.departmentName);
  labels.interimSoldout = labels.interimSoldout
    ? showCategoryAndDepartment(
        labels.interimSoldout,
        entityCategoryName,
        breadCrumbs,
        checkBreadCrumbAvailability,
        labels.departmentName,
        labels.categoryName
      )
    : '';
  labels.labelForSplit = labels.interimSoldout ? labels.interimSoldout.split('?') : '';
  /* eslint-disable react/no-array-index-key */
  return (
    <div className={className}>
      <Row>
        <Col className="fixed-bread-crumb-height" colSize={{ small: 6, medium: 8, large: 12 }}>
          <div className="bread-crumb">
            <FixedBreadCrumbs crumbs={breadCrumbs} separationChar=">" />
          </div>
        </Col>
      </Row>
      <Row>
        <Col colSize={{ small: 6, medium: 8, large: 2 }}>
          <div className="sidebar">
            {navTree && (
              <GlobalNavigationMenuDesktopL2
                navigationTree={navTree}
                activeCategoryIds={currentNavIds}
              />
            )}
            {/* UX timer */}
            <RenderPerf.Measure name={PAGE_NAVIGATION_VISIBLE} />
          </div>
        </Col>
        <Col colSize={{ small: 6, medium: 8, large: 10 }}>
          <div className="container-bopis-filter">
            {isBopisFilterEnabled && (
              <BopisFilter
                isBopisFilterOn={isBopisFilterOn}
                labels={labels}
                formValues={formValues}
                getProducts={getProducts}
                onSubmit={onSubmit}
                updateFormValue={updateFormValue}
                setBopisFilterStateActn={setBopisFilterStateActn}
                defaultStoreData={JSON.parse(getLocalStorage('defaultStore')) || {}}
                resetSuggestedStores={resetSuggestedStores}
              />
            )}
          </div>

          {showNewBopisFilterEmptyMessage ? (
            <BopisFilterEmptyResults labels={labels} />
          ) : (
            <div className="interim-sold-out">
              <div className="category-name">
                {`${!!labels.interimSoldout && labels.interimSoldout.split('?')[0].toUpperCase()}?`}
              </div>
              {returnAnchor(navTree, labels)}
            </div>
          )}

          <ReadMore
            description={longDescription}
            labels={labels}
            className={`${className} seo-text`}
          />
        </Col>
      </Row>
    </div>
  );
};

EmptyCategoryBanner.propTypes = {
  className: PropTypes.string,
  longDescription: PropTypes.string,
  labels: PropTypes.string,
  currentNavIds: PropTypes.arrayOf(PropTypes.shape({})),
  navTree: PropTypes.shape({}),
  breadCrumbs: PropTypes.arrayOf(PropTypes.shape({})),
  entityCategoryName: PropTypes.string,
  plpSeoInfo: PropTypes.arrayOf(PropTypes.shape({})),
};

EmptyCategoryBanner.defaultProps = {
  className: '',
  labels: '',
  longDescription: [],
  currentNavIds: [],
  navTree: {},
  breadCrumbs: [],
  entityCategoryName: '',
  plpSeoInfo: [],
};

export default withStyles(EmptyCategoryBanner, EmptyCategoryBannerStyle);
