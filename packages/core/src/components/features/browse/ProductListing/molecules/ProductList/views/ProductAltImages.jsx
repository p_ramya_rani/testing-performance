// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module ProductAltImages
 *
 * @author Agu
 */
import React from 'react';
import { PropTypes } from 'prop-types';
import {
  isClient,
  getLocator,
  getVideoUrl,
  routerPush,
  getSiteId,
  urlContainsQuery,
  isSafariBrowser,
} from '@tcp/core/src/utils';
import { setSessionStorage } from '@tcp/core/src/utils/utils.web';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { getProductListToPath } from '../utils/productsCommonUtils';
// import cssClassName from '../utils/cssClassName';
import styles, { imageAnchorInheritedStyles } from '../styles/ProductAltImages.style';
import { Anchor, DamImage } from '../../../../../../common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import OutOfStockWaterMark from '../../../../ProductDetail/molecules/OutOfStockWaterMark';
import { IMG_DATA_PLP, VID_DATA_PLP, VID_DATA_PLP_WEBP, IMG_DATA_PLP_SECURE } from '../config';
import RecommendationsAbstractor from '../../../../../../../services/abstractors/common/recommendations';

class ProductAltImages extends React.PureComponent {
  nodes = {};

  static propTypes = {
    /** callback for when the shown image changes. Accepts: image index */
    onImageChange: PropTypes.func,
    googleAnalyticsData: PropTypes.func,
    imageUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
    productName: PropTypes.string.isRequired,
    pdpUrl: PropTypes.string.isRequired,
    analyticsData: PropTypes.shape({
      pId: PropTypes.string.isRequired,
      prank: PropTypes.string.isRequired,
      requestId: PropTypes.string.isRequired,
    }),
    loadedProductCount: PropTypes.number.isRequired,
    className: PropTypes.string.isRequired,
    keepAlive: PropTypes.bool.isRequired,
    isSoldOut: PropTypes.bool,
    soldOutLabel: PropTypes.string,
    isSearchPage: PropTypes.string,
    viaModule: PropTypes.string,
    dataSource: PropTypes.string,
    isProductBrandOfSameDomain: PropTypes.bool,
    itemBrand: PropTypes.string.isRequired,
    page: PropTypes.string,
    cookieToken: PropTypes.string,
    isBundleProduct: PropTypes.bool,
    lowQualityEnabled: PropTypes.bool,
    portalValue: PropTypes.string,
    dynamicBadgeOverlayQty: PropTypes.number,
    tcpStyleType: PropTypes.string,
    actionId: PropTypes.string,
  };

  static defaultProps = {
    onImageChange: () => {},
    googleAnalyticsData: () => {},
    analyticsData: {},
    isSoldOut: false,
    soldOutLabel: '',
    isSearchPage: 'false',
    viaModule: '',
    dataSource: '',
    isProductBrandOfSameDomain: true,
    page: '',
    cookieToken: null,
    isBundleProduct: false,
    lowQualityEnabled: false,
    portalValue: '',
    dynamicBadgeOverlayQty: 0,
    tcpStyleType: '',
    actionId: '',
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      currentIndex: 0,
    };

    this.handledPrevImage = this.handledPrevImage.bind(this);
    this.handledNextImage = this.handledNextImage.bind(this);
    this.handlePrevImage = this.handlePrevImage.bind(this);
    this.handleNextImage = this.handleNextImage.bind(this);
  }

  // onVideoError = () => this.setState({ videoError: true });

  productLink = (
    loadedProductCount,
    { to, asPath },
    googleAnalyticsData,
    e,
    isProductBrandOfSameDomain,
    alternateBrand
  ) => {
    const toPath = alternateBrand ? `${to}$brand=${alternateBrand}` : `${to}`;
    const asPathNew = alternateBrand ? `${asPath}$brand=${alternateBrand}` : `${asPath}`;
    if (!isProductBrandOfSameDomain) {
      return false;
    }
    e.preventDefault();

    if (isClient()) {
      const skuClicked = e.target.closest('.item-container-inner').getAttribute('unbxdparam_sku');
      setSessionStorage({ key: 'LAST_PAGE_PATH', value: window.location.pathname });
      setSessionStorage({ key: 'LOADED_PRODUCT_COUNT', value: loadedProductCount });
      setSessionStorage({ key: 'SCROLL_POINT', value: window.pageYOffset });
      setSessionStorage({ key: 'UNBXDPARAM_SKU', value: skuClicked });
      setSessionStorage({ key: 'PDP1', value: getLocalStorage('pdp1') });
      setSessionStorage({ key: 'PDP2', value: getLocalStorage('pdp2') });
      routerPush(toPath, asPathNew);
    }
    googleAnalyticsData();
    return true;
  };

  getPDPAsPath = ({ pdpUrl, cookieToken, isProductBrandOfSameDomain }) => {
    let pdpAsPath = pdpUrl;
    if (cookieToken && !isProductBrandOfSameDomain) {
      pdpAsPath = `${pdpAsPath}${urlContainsQuery(pdpAsPath) ? '&' : '?'}ct=${cookieToken}`;
    }
    return pdpAsPath;
  };

  getVideoTransformation = () => (isSafariBrowser() ? VID_DATA_PLP : VID_DATA_PLP_WEBP);

  getPdpPageUrl = (
    pdpToPath,
    isSearchPage,
    viaModule,
    page,
    dataSource,
    recIdentifier,
    actionId
  ) => {
    return (
      pdpToPath &&
      `${pdpToPath}&isSearchPage=${isSearchPage}&viaModule=${viaModule}&viaPage=${page}&dataSource=${dataSource}${
        recIdentifier ? `&recommendationCarousel=${recIdentifier}` : ''
      }${actionId ? `&actionId=${actionId}` : ''}`
    );
  };

  getUnboxdData = (analyticsData) => {
    return analyticsData || {};
  };

  getFilteredPdpToPath = (isBundleProduct, pdpToPath, siteId) => {
    return !isBundleProduct
      ? pdpToPath.replace('/p?pid=', `/${siteId}/p/`)
      : pdpToPath.replace('/b?bid=', `/${siteId}/b/`);
  };

  getPdpToPath = (isProductBrandOfSameDomain, pdpUrl) => {
    return isProductBrandOfSameDomain ? getProductListToPath(pdpUrl) : pdpUrl;
  };

  handledNextImage() {
    const { currentIndex } = this.state;
    const { imageUrls, onImageChange } = this.props;
    let idx = currentIndex + 1;
    if (idx > imageUrls.length) {
      idx = 0;
    }
    this.setState({ currentIndex: idx });
    if (onImageChange) onImageChange(idx);
  }

  handledPrevImage() {
    const { currentIndex } = this.state;
    const { imageUrls, onImageChange } = this.props;
    let idx = currentIndex - 1;
    if (idx < 0) {
      idx += imageUrls.length + 1;
    }
    this.setState({ currentIndex: idx });
    if (onImageChange) onImageChange(idx);
  }

  handlePrevImage() {
    const { currentIndex } = this.state;
    const { imageUrls, onImageChange } = this.props;
    let idx = currentIndex - 1;
    if (idx < 0) {
      idx += imageUrls.length;
    }
    this.setState({ currentIndex: idx });
    if (onImageChange) onImageChange(idx);
  }

  handleNextImage() {
    const { currentIndex } = this.state;
    const { imageUrls, onImageChange } = this.props;
    const idx = (currentIndex + 1) % imageUrls.length;
    this.setState({ currentIndex: idx });
    if (onImageChange) onImageChange(idx);
  }

  renderSoldOutSection = () => {
    const { isSoldOut, keepAlive, soldOutLabel } = this.props;
    return isSoldOut || keepAlive ? <OutOfStockWaterMark label={soldOutLabel} /> : null;
  };

  renderDamImage(disableVideoClickHandler = false) {
    const {
      imageUrls,
      productName,
      itemBrand,
      lowQualityEnabled,
      dynamicBadgeOverlayQty,
      tcpStyleType,
      isSecureImageFlagEnabled,
      primaryBrand,
    } = this.props;
    const { currentIndex } = this.state;
    const imgData = {
      alt: productName,
      url: imageUrls[currentIndex],
    };
    let configData;
    // TODO in case of having bundle product name key on cloudinary
    // const configData = isBundleProduct ? IMG_DATA_CLP : IMG_DATA_PLP;

    const isVideoUrl = getVideoUrl(imgData.url);
    if (isSecureImageFlagEnabled) {
      configData = isVideoUrl ? this.getVideoTransformation() : IMG_DATA_PLP_SECURE;
    } else {
      configData = isVideoUrl ? this.getVideoTransformation() : IMG_DATA_PLP;
    }

    return (
      <>
        <DamImage
          className="loadImage"
          dataLocator={getLocator('global_productimg_imagelink')}
          imgData={imgData}
          imgConfigs={configData.imgConfig}
          isProductImage
          noSourcePictureTag
          itemBrand={itemBrand}
          disableVideoClickHandler={disableVideoClickHandler}
          isOptimizedVideo
          lowQualityEnabled={lowQualityEnabled}
          dynamicBadgeOverlayQty={currentIndex === 0 ? dynamicBadgeOverlayQty : 0}
          tcpStyleType={tcpStyleType}
          badgeConfig={IMG_DATA_PLP.badgeConfig}
          lazyLoad
          isSecureImageFlagEnabled={isSecureImageFlagEnabled}
          primaryBrand={primaryBrand}
          tabIndex="0"
        />
        {this.renderSoldOutSection()}
      </>
    );
  }

  renderDamWrapper() {
    const {
      pdpUrl,
      productName,
      loadedProductCount,
      analyticsData,
      isSearchPage,
      isProductBrandOfSameDomain,
      viaModule,
      dataSource,
      page,
      cookieToken,
      isBundleProduct,
      googleAnalyticsData,
      portalValue,
      actionId,
      alternateBrand,
    } = this.props;
    const unbxdData = this.getUnboxdData(analyticsData);
    const siteId = getSiteId();
    const pdpToPath = this.getPdpToPath(isProductBrandOfSameDomain, pdpUrl);

    const recIdentifier = RecommendationsAbstractor.handleWhichRecommendationClicked(
      portalValue,
      page
    );
    const pdpPageUrl = this.getPdpPageUrl(
      pdpToPath,
      isSearchPage,
      viaModule,
      page,
      dataSource,
      recIdentifier,
      actionId
    );
    const pdpAsPath = this.getPDPAsPath({ pdpUrl, cookieToken, isProductBrandOfSameDomain });
    const filteredPdpToPath = this.getFilteredPdpToPath(isBundleProduct, pdpToPath, siteId);

    return (
      <Anchor
        handleLinkClick={(e) =>
          this.productLink(
            loadedProductCount,
            { to: pdpPageUrl, asPath: pdpAsPath },
            googleAnalyticsData,
            e,
            isProductBrandOfSameDomain,
            alternateBrand
          )
        }
        title={productName}
        unbxdattr="product"
        unbxdparam_sku={unbxdData && unbxdData.pId}
        unbxdparam_prank={unbxdData && unbxdData.prank}
        inheritedStyles={imageAnchorInheritedStyles}
        to={filteredPdpToPath}
        asPath={pdpAsPath}
        tabIndex="-1"
        noLink
      >
        {this.renderDamImage()}
      </Anchor>
    );
  }

  renderImageContent() {
    const { imageUrls, productName, className } = this.props;
    return imageUrls.length < 2 ? (
      <figure
        className="product-image-container"
        itemScope
        itemType="http://schema.org/ImageObject"
      >
        {this.renderDamWrapper()}
      </figure>
    ) : (
      <figure
        // eslint-disable-next-line no-return-assign
        ref={(node) => (this.nodes[productName] = node)}
        className={className}
        itemScope
        itemType="http://schema.org/ImageObject"
      >
        <button
          data-locator={getLocator('global_imagecursors_arrows')}
          type="button"
          className="button-prev hide-on-mobile"
          onClick={this.handlePrevImage}
        >
          prev
        </button>
        {this.renderDamWrapper()}
        <button
          data-locator={getLocator('global_imagecursors_arrows')}
          type="button"
          className="button-next hide-on-mobile"
          onClick={this.handleNextImage}
        >
          next
        </button>
      </figure>
    );
  }

  // We are rendering only Images content
  render() {
    return this.renderImageContent();
  }
}

export default withStyles(ProductAltImages, styles);

export { ProductAltImages as ProductAltImagesVanilla };
