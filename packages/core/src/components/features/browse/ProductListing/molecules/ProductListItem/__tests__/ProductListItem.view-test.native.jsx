// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import {
  ListItemVanilla,
  getProductId,
  getBorderRadius,
  getViewStyling,
  getFontWeight,
} from '../views/ProductListItem.view.native';

describe('ProductListItem component', () => {
  let component;
  let redesignComp;
  const props = {
    isFavorite: true,
    selectedColorIndex: 0,
    item: {
      colorsMap: [
        {
          colorProductId: '3001084_IV',
          imageName: '3001084_IV',
          miscInfo: {},
          color: {
            name: 'TIDAL',
            imagePath: '',
          },
        },
      ],
      productInfo: {
        name: 'tcp',
        pdpUrl: '',
        keepAlive: true,
      },
      miscInfo: {
        isInDefaultWishlist: '',
        keepAlive: true,
      },
      quantityPurchased: 1,
      itemInfo: {
        availability: 'OK',
        quantity: 1,
      },
    },
    badge1: '',
    badge2: '',
    badge3: '',
    listPriceForColor: 10,
    offerPriceForColor: 12,
    loyaltyPromotionMessage: '',
    onAddToBag: jest.fn(),
    onFavorite: jest.fn(),
    onGoToPDPPage: jest.fn(),
    onQuickViewOpenClick: jest.fn(),
    setLastDeletedItemId: jest.fn(),
    fullWidth: false,
    renderPriceAndBagOnly: false,
    renderPriceOnly: false,
    productImageWidth: false,
    isDataLoading: false,
    keepAlive: false,
    isLoggedIn: false,
    labelsPlpTiles: {},
    isKeepAliveEnabled: false,
    outOfStockLabels: {},
    renderMoveToList: () => {},
    onSeeSuggestedItems: () => {},
  };
  beforeEach(() => {
    component = shallow(<ListItemVanilla {...props} />);
  });

  beforeEach(() => {
    redesignComp = shallow(<ListItemVanilla {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should return styled View component value four', () => {
    expect(component.find('Styled(View)')).toHaveLength(4);
  });

  it('should return RenderPricesSection component value one', () => {
    expect(component.find('RenderPricesSection')).toHaveLength(1);
  });

  it('should return RenderTopBadge1 component value one', () => {
    expect(component.find('RenderTopBadge1')).toHaveLength(1);
  });

  it('should return RenderBadge2 component value one', () => {
    expect(component.find('RenderBadge2')).toHaveLength(1);
  });

  it('should return ImageSection component value one', () => {
    expect(component.find('ImageSection')).toHaveLength(1);
  });

  it('should return RenderColorSwitch component value one', () => {
    expect(component.find('RenderColorSwitch')).toHaveLength(1);
  });

  it('should return RenderSizeFit component value one', () => {
    expect(component.find('RenderSizeFit')).toHaveLength(1);
  });

  it('should return RenderPurchasedQuantity component value one', () => {
    expect(component.find('RenderPurchasedQuantity')).toHaveLength(1);
  });

  it('should return RenderMoveToListOrSeeSuggestedList component value one', () => {
    expect(component.find('RenderMoveToListOrSeeSuggestedList')).toHaveLength(1);
  });

  it('should return Styled(Anchor) component value one', () => {
    expect(component.find('Styled(Anchor)')).toHaveLength(1);
  });

  it('should return Styled(CustomButton) component value one', () => {
    expect(component.find('Styled(CustomButton)')).toHaveLength(1);
  });

  it('should return RenderDismissLink component value one', () => {
    expect(component.find('RenderDismissLink')).toHaveLength(1);
  });

  it('should render correctly for new design', () => {
    redesignComp.setProps({
      isNewReDesignProductTile: true,
    });
    expect(redesignComp).toMatchSnapshot();
  });
});

describe('testing redesign functions', () => {
  it('should return the product id', () => {
    const productItemData = {
      generalProductId: '12345_AB',
    };
    const expectedResult = '12345';
    expect(getProductId(productItemData)).toEqual(expectedResult);
  });

  it('should return correct border radius for new design', () => {
    const result = '16px';
    expect(getBorderRadius(true)).toEqual(result);
  });

  it('should return correct styling for new design', () => {
    const result = { flex: 1, marginHorizontal: 10 };
    expect(getViewStyling(true)).toEqual(result);
  });

  it('should return correct font weight', () => {
    const result = 'extrabold';
    expect(getFontWeight(true)).toEqual(result);
  });
});
