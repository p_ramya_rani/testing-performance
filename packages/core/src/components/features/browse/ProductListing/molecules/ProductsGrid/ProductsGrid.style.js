// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .product-grid-block-container {
    display: flex;
    flex-wrap: wrap;
  }
  .product-grid-block-container .loadImage.error{
    height: 205px;

    @media ${props => props.theme.mediaQuery.medium} and ${props =>
  props.theme.mediaQuery.mediumMax} and (orientation: landscape) {
      height: 340px;
    }

    @media ${props => props.theme.mediaQuery.medium} {
      height: 267px;
    }

    @media ${props => props.theme.mediaQuery.large} {
      height: 325px;
    }
    background: ${props => props.theme.colorPalette.gray[500]};
    display: block;
  }
  .plp-grid-recos-enabled {
    .color-chips-container + .fulfillment-section {
      margin-top: 51px;
    }

    .color-chips-container + .ranking-wrapper {
      margin-top: 28px;
    }

    .points-and-rewards + .fulfillment-section {
      margin-top: 23px;
    }

    .in-grid-plp-rectangle + .fulfillment-section {
      margin-top: 51px;
    }

    .in-grid-plp-rectangle + .ranking-wrapper {
      margin-top: 28px;
    }

    .loyalty-text-container + .fulfillment-section {
      margin-top: 23px;
    }
  }
`;
