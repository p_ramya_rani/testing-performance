// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { requireUrlScript } from '@tcp/core/src/utils/resourceLoader';
import { createBVUrl } from '@tcp/core/src/utils';

export default class Spotlights extends React.PureComponent {
  constructor(props) {
    super(props);
    this.spotlightRef = React.createRef();
  }

  componentDidMount = () => {
    const { categoryId } = this.props;
    if (window.BV) {
      this.updateCategory(categoryId);
    } else {
      requireUrlScript(createBVUrl()).then(this.updateCategory(categoryId));
    }
  };

  componentWillReceiveProps = nextProps => {
    const { categoryId } = nextProps;
    const { categoryId: prevCategoryId } = this.props;
    if (this.spotlightRef.current && prevCategoryId !== categoryId) {
      this.spotlightRef.current.setAttribute('data-bv-subject-id', `SL-${categoryId}`);
    }
  };

  updateCategory(catId) {
    if (this.spotlightRef.current) {
      this.spotlightRef.current.setAttribute('data-bv-subject-id', `SL-${catId}`);
    }
  }

  render() {
    return <div ref={this.spotlightRef} data-bv-show="spotlights" data-bv-site-id="Spotlights" />;
  }
}

Spotlights.propTypes = {
  categoryId: PropTypes.string.isRequired,
};
