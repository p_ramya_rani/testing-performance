// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import DeviceInfo from 'react-native-device-info';
import { FiltersVanilla } from '../views/Filters.view.native';

describe('FiltersVanilla is shown', () => {
  let wrapper;

  beforeEach(() => {
    DeviceInfo.hasNotch = () => false;
    const props = {
      filters: {
        unbxdDisplayName: { TCPColor_uFilter: 'Color', Size: 'Size' },
      },
      theme: {},
      labelsFilter: { lbl_clear: 'Clear', lbl_apply: 'Apply' },
      displayAltFilterView: false,
    };
    wrapper = shallow(<FiltersVanilla {...props} />);
  });

  it('should render FiltersVanilla', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should render 2 Flatlists', () => {
    wrapper.setProps({
      filters: {
        TCPColor_uFilter: [{ id: 'red' }, { id: 'blue' }],
        Size: [{ id: 'XS' }, { id: 'S' }],
      },
    });

    const flatlist = wrapper.find('FlatList');
    expect(flatlist.length).toBe(2);
    expect(
      flatlist
        .first()
        .props()
        .listKey({ id: '1' })
    ).toEqual('1');
    expect(
      flatlist
        .first()
        .props()
        .keyExtractor({ id: '1' })
    ).toEqual('1');
  });

  it('should render items in flatlist', () => {
    wrapper.setProps({
      filters: {
        Size: [{ id: 'XS' }, { id: 'S' }],
      },
    });
    const item = { displayName: 'XS' };
    const listItem = shallow(
      wrapper
        .find('FlatList')
        .first()
        .props()
        .renderItem({ item })
    );
    expect(listItem.length).toBe(1);
    listItem.props().onPress();
    expect(item.isSelected).toBeTruthy();
    expect(wrapper.instance().state.selectedItems.length).toBe(1);
    listItem.props().onPress();
    expect(wrapper.instance().state.selectedItems.length).toBe(0);
  });

  it('should render color items', () => {
    wrapper.setProps({
      filters: {
        TCPColor_uFilter: [{ id: 'red' }, { id: 'blue' }],
      },
    });
    const item = { imagePath: '//' };
    const listItem = shallow(
      wrapper
        .find('FlatList')
        .first()
        .props()
        .renderItem({ item })
    );
    expect(listItem.length).toBe(1);
    listItem.props().onPress();
    expect(item.isSelected).toBeTruthy();
    expect(wrapper.instance().state.selectedItems.length).toBe(1);
    listItem.props().onPress();
    expect(wrapper.instance().state.selectedItems.length).toBe(0);
  });

  it('should clear all filters', () => {
    const onSubmit = jest.fn();
    wrapper.setProps({ onSubmit });
    wrapper.instance().setState({ selectedItems: [{ id: '1', isSelected: true }] });
    wrapper
      .find('[text="Clear"]')
      .props()
      .onPress();
    expect(onSubmit).toHaveBeenCalledWith({});
  });

  it('should return grouped filter', () => {
    const sizeFitList = [
      { id: 'XL_Regular' },
      { id: 'XXL_Regular' },
      { id: 'SM_SMALL' },
      { id: 'SM1_SMALL' },
    ];
    const formattedListSample = {
      XL: 'Regular',
      XXL: 'Regular',
      SM: 'SMALL',
      SM1: 'SMALL',
    };
    const formattedList = wrapper.instance().getSizeGroupedByFit(sizeFitList);

    expect(formattedList).toEqual(formattedListSample);
  });
});

describe('FiltersVanilla AB test is shown', () => {
  let wrapper;

  beforeEach(() => {
    DeviceInfo.hasNotch = () => true;
    const props = {
      filters: {
        unbxdDisplayName: { TCPColor_uFilter: 'Color', Size: 'Size' },
      },
      theme: {},
      labelsFilter: { lbl_clear: 'Clear', lbl_apply: 'Apply' },
      displayAltFilterView: true,
    };
    wrapper = shallow(<FiltersVanilla {...props} />);
  });

  it('should render FiltersVanilla', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
