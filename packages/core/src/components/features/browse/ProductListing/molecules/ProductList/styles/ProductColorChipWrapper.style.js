// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';
import { arrowWrapper } from './CommonStyle';

const styles = css`
  vertical-align: top;
  position: relative;
  opacity: 1;
  width: 100%;
  .color-swatches-desktop-view {
    display: none;
  }
  .color-swatches-tab-view {
    display: none;
  }
  .arrowRightWrapper {
    ${arrowWrapper}
    transform: rotate(180deg);
    right: 0;
    &:hover {
      cursor: pointer;
    }
  }

  .arrowImg {
    transform: rotate(180deg);
    height: 15px;
    width: 6px;
  }

  .content-colors {
    display: flex;
    padding: 0;
    margin: ${props => props.theme.spacing.ELEM_SPACING.XS} 0px
      ${props => props.theme.spacing.ELEM_SPACING.SM};
  }

  .color-swatches-container {
    margin-left: 19px;
  }

  .product-color-chip-image {
    height: 100%;
    width: 100%;
  }

  .content-colors-button {
    margin-right: 6px;
    font-size: 0;
    border-radius: 50%;
    width: 15px;
    height: 15px;
    display: inline;
    overflow: hidden;
    background: transparent;
    padding: 0;
    border: 0;

    &.active {
      border: 1px solid ${props => props.theme.colors.DARK} !important;
      padding: 1px;
      .product-color-chip-image {
        border-radius: 50%;
      }
      box-shadow: inset 0 0 0 1px ${props => props.theme.colors.WHITE};
    }
    &:focus {
      outline: 0;
    }
    &:hover {
      cursor: pointer;
    }
  }
  .mobile-content-colors {
    padding-right: 10px;
    margin: ${props => props.theme.spacing.ELEM_SPACING.XXS} 0px
      ${props => props.theme.spacing.ELEM_SPACING.XXS};
    .container-carousel {
      overflow-x: scroll;
      overflow-y: hidden;
      white-space: nowrap;
      width: 100%;
      -ms-overflow-style: none;
      scroll-behavior: smooth;
      text-align: left;
    }
    .container-carousel::-webkit-scrollbar {
      display: none;
    }
    .content-colors-button {
      vertical-align: top;
    }
  }
  @media ${props => props.theme.mediaQuery.medium} {
    .mobile-content-colors {
      display: none;
    }
    .color-swatches-mobile-view {
      display: none;
    }
    .color-swatches-tab-view {
      display: flex;
    }
  }
  @media ${props => props.theme.mediaQuery.large} {
    .content-colors-button {
      margin-right: 14px;
      width: 20px;
      height: 20px;
    }
    .color-swatches-tab-view {
      display: none;
    }

    .color-swatches-desktop-view {
      display: flex;
    }
  }
`;

export default styles;
