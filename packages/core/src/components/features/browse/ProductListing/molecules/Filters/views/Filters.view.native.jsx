/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { FlatList, ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components/native';
import DeviceInfo from 'react-native-device-info';
import theme from '@tcp/core/styles/themes/TCP';
import { Button, BodyCopy, LabeledRadioButton } from '@tcp/core/src/components/common/atoms';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import { BUTTON_VARIATION } from '@tcp/core/src/components/common/atoms/Button';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ModalHeaderCurved from '@tcp/core/src/components/common/molecules/Modal/view/Modal.header.curved';
import capitalized from 'lodash/capitalize';
import { isIOS, isAndroid } from '@tcp/core/src/utils/utils.app';
import { isGymboree } from '@tcp/core/src/utils/utils';
import CustomButton from '../../../../../../common/atoms/ButtonRedesign';
import SpinnerIcon from '../../../../../../../../../mobileapp/src/components/common/molecules/SpinnerIcon';
import styles, {
  PageContainer,
  Container,
  getFlatListContainerStyle,
  ItemSeparatorStyle,
  TitleText,
  ApplyAndClearButtonContainer,
  ShowResultButtonContainer,
  InputCheckboxWrapper,
  LineWrapper,
  StickyContainer,
  FilterPillsStyle,
  AtbButton,
  BottomnView,
  ClearBtnWrapper,
} from '../Filters.style.native';
import {
  TabListVertical,
  TabListHeaderContainer,
  TabListHeader,
  TabListBodyContainer,
  TabListHeaderContent,
  TabListBodyContent,
  FilterPillsListBodyContainer,
} from '../../../../../../../../../mobileapp/src/components/common/molecules/TabListVertical/TabListVertical.style.native';
import LinkImageIcon from '../../../atoms/LinkImageIcon';
import { FILTER_COLOR_SWATCH_URL } from '../../../container/ProductListing.constants';
import FILTER_CONFIG from '../Filter.constants';

const VirtualizedView = (props) => {
  return (
    <FlatList
      data={[]}
      ListEmptyComponent={null}
      keyExtractor={() => 'key'}
      renderItem={null}
      listKey={(key) => key.id}
      ListHeaderComponent={() => props.children}
    />
  );
};

VirtualizedView.propTypes = {
  children: PropTypes.shape({}).isRequired,
};

class Filters extends React.PureComponent {
  static propTypes = {
    filters: PropTypes.shape({}),
    theme: PropTypes.shape({}),
    labelsFilter: PropTypes.shape({}),
    onSubmit: PropTypes.func,
    isFavorite: PropTypes.bool,
    onFilterSelection: PropTypes.func,
    filteredId: PropTypes.string,
    onCloseModal: PropTypes.func,
    closeModal: PropTypes.func,
    isLoadingMore: PropTypes.bool,
    displayAltFilterView: PropTypes.bool,
    totalProductsCount: PropTypes.number,
    onSortSelection: PropTypes.func,
    selectedSortId: PropTypes.string,
    sizeFitData: PropTypes.arrayOf(PropTypes.shape({})),
    filterPillsIndex: PropTypes.number,
    isFromFilterPills: PropTypes.bool,
    titleLabel: PropTypes.string,
  };

  static defaultProps = {
    filters: {},
    theme: {},
    labelsFilter: {},
    onSubmit: null,
    isFavorite: false,
    onFilterSelection: () => {},
    filteredId: 'ALL',
    onCloseModal: () => {},
    closeModal: () => {},
    isLoadingMore: false,
    displayAltFilterView: false,
    totalProductsCount: 0,
    onSortSelection: () => {},
    selectedSortId: '',
    sizeFitData: [],
    filterPillsIndex: 0,
    isFromFilterPills: false,
    titleLabel: '',
  };

  filterNames = [];

  constructor(props) {
    super(props);
    const { filters, filteredId, displayAltFilterView, sizeFitData } = this.props;
    const { unbxdDisplayName } = filters;

    const selectedIndex1 = this.getSelectedItemIndexById(filters, filteredId);
    this.filterNames = (unbxdDisplayName && Object.entries(unbxdDisplayName)) || [];
    this.state = {
      selectedItems: [],
      selectedIndex: selectedIndex1,
      activeTab: 0,
    };

    if (displayAltFilterView) {
      this.sizeByFitList = this.getSizeGroupedByFit(sizeFitData);
      this.isIOS = isIOS();

      this.titleStyles = {
        fontFamily: 'secondary',
        fontWeight: 'black',
        color: theme.colorPalette.gray[900],
      };

      this.clearAllCtaStyle = {
        textTransform: 'none',
        letterSpacing: 0.6,
        color: theme.colorPalette.gray[900],
        fontWeight: this.isIOS ? '400' : 'regular',
      };

      this.hasNotch = DeviceInfo.hasNotch();

      this.selectedFilterFontStyle = this.getSelectedItemFontStyle(true);
      this.filterFontStyle = this.getSelectedItemFontStyle(false);
    }
  }

  getSizeGroupedByFit = (sizeFitData) => {
    const sizeFitList = {};
    if (sizeFitData.length > 0) {
      sizeFitData.forEach((sizeFit) => {
        const sizeFitId = sizeFit.id;
        const index = sizeFitId.lastIndexOf('_');
        const size = sizeFitId.substring(0, index);
        const fit = sizeFitId.substring(index + 1, sizeFitId.length);
        sizeFitList[size] = fit;
      });
    }

    return sizeFitList;
  };

  /**
   * @desc This is method iterate passed array based on the passed id and return selected index
   */
  getSelectedItemIndexById = (filters, selectedId) => {
    const len = (filters && filters.length) || 0;
    let matchedSelectedId = null;
    for (let i = 0; i < len; i += 1) {
      const item = filters[i];
      if (item.id === selectedId) {
        matchedSelectedId = i;
        break;
      }
    }
    return matchedSelectedId;
  };

  /**
   * @desc This is seperator method which used for making gap between color switches
   */
  renderSeparator = () => {
    return <ItemSeparatorStyle />;
  };

  onItemPress = (isFavorite, item, index) => {
    if (isFavorite) {
      this.onSingleSelectFilter(item, index);
    } else {
      this.onSelectFilter(item);
    }
    this.onApplyFilter();
  };

  onColorItemPress = (isLoadingMore, item) => {
    if (!isLoadingMore) {
      this.onSelectFilter(item);
      this.onApplyFilter();
    }
  };

  /**
   * @function onSelectFilter
   * This method is called on tap of an item in filter list
   * toggles isSelected property of selected item and saves or removes it from selectedItems in state
   *
   * @memberof Filters
   */
  onSelectFilter = (item) => {
    const updatedItem = item;
    updatedItem.isSelected = !updatedItem.isSelected;
    const { selectedItems } = this.state;
    const selectedItemsList = updatedItem.isSelected
      ? [...selectedItems, updatedItem]
      : selectedItems.filter((i) => i !== updatedItem);
    this.setState({ selectedItems: selectedItemsList });
  };

  /**
   * @function onSingleSelectFilter
   * This method is called on tap of an item in filter list
   * set selected state of the filter
   */
  onSingleSelectFilter = (item, index) => {
    const { onFilterSelection, onCloseModal } = this.props;
    const { id } = item;
    if (onFilterSelection) {
      onFilterSelection(id);
    }
    this.setState({ selectedIndex: index }, () => {
      onCloseModal();
    });
  };

  renderListItem = ({ item, index }) => {
    const { isFavorite, isLoadingMore } = this.props;
    const { selectedIndex } = this.state;
    const { displayName, isSelected } = item;
    const selectedState = isFavorite ? selectedIndex === index : isSelected;
    const isDisabled = item.disabled || isLoadingMore;
    return (
      <Button
        buttonVariation={BUTTON_VARIATION.mobileAppFilter}
        text={displayName}
        onPress={() => {
          this.onItemPress(isFavorite, item, index);
        }}
        accessibilityState={{ selected: selectedIndex === index || isSelected }}
        selected={selectedState}
        data-locator=""
        accessibilityLabel={displayName}
        disableButton={isDisabled}
      />
    );
  };

  sizeFilterTitle = (titleText) => {
    const { isFromFilterPills } = this.props;
    const updatedHeader = isFromFilterPills ? capitalized(titleText) : titleText.toUpperCase();

    if (titleText === 'UNDEFINED') return null;
    return (
      <>
        <BodyCopy
          fontFamily={isGymboree() ? 'primary' : 'secondary'}
          fontSize="fs16"
          text={updatedHeader}
          fontWeight={isFromFilterPills ? 'bold' : 'regular'}
          color="gray.900"
        />
        {!isFromFilterPills ? (
          <LineWrapper marginTop="5" marginBottom="18" borderColor="gray.500" />
        ) : (
          <BottomnView />
        )}
      </>
    );
  };

  renderSizeFilterView = ({ filterData, componentRenderer, selectedItems }) => {
    const sizeFitGroups = [];
    filterData.forEach((filterItem) => {
      const fitName = this.sizeByFitList[filterItem.id];
      if (sizeFitGroups[fitName]) {
        sizeFitGroups[fitName].data.push(filterItem);
      } else {
        sizeFitGroups[fitName] = {};
        sizeFitGroups[fitName].data = [];
        sizeFitGroups[fitName].data.push(filterItem);
      }
    });

    const groupedSizeWithFit = Object.entries(sizeFitGroups);

    return (
      <>
        {groupedSizeWithFit.map((sizeFitGroup) => (
          <FlatList
            listKey={(key) => key.id}
            data={sizeFitGroup[1].data}
            renderItem={componentRenderer}
            keyExtractor={(key) => key.id}
            extraData={selectedItems}
            ListHeaderComponent={() => this.sizeFilterTitle(sizeFitGroup[0] || '')}
          />
        ))}
      </>
    );
  };

  renderCheckBoxFilterView = ({ item, index }) => {
    const { isFavorite, isLoadingMore, isFromFilterPills } = this.props;
    const { selectedIndex } = this.state;
    const { displayName, isSelected } = item;
    const selectedState = isFavorite ? selectedIndex === index : isSelected;
    const isDisabled = item.disabled || isLoadingMore;
    return (
      <InputCheckboxWrapper isFromFilterPills={isFromFilterPills}>
        <InputCheckbox
          spacingStyles=" "
          dataLocator="data-locator"
          execOnChangeByDefault={false}
          isChecked={selectedState}
          fontSize="fs14"
          checkBoxLabel
          disabled={isDisabled}
          accessibilityText={displayName}
          isCurvedCheckBox
          input={{
            name: displayName,
            onChange: () => this.onItemPress(isFavorite, item, index),
          }}
          overrideCSS={{ checkbox: { 'align-self': 'flex-start' } }}
        >
          <BodyCopy
            fontFamily={!isFromFilterPills ? 'secondary' : 'primary'}
            fontSize="fs14"
            letterSpacing="ls02"
            text={displayName}
            fontWeight={selectedState && !isFromFilterPills ? 'extrabold' : 'regular'}
            color={isFromFilterPills ? 'gray.900' : 'gray.2100'}
          />
        </InputCheckbox>
      </InputCheckboxWrapper>
    );
  };

  renderSortFilterView = ({ item }) => {
    const { isLoadingMore, onSortSelection, selectedSortId } = this.props;
    const { displayName, id } = item;
    const selectedState = selectedSortId === item.id;
    const isDisabled = item.disabled || isLoadingMore;
    const radioButtonStyle = {};
    const radioButtonLabelStyle = {
      fontSize: 14,
      fontFamily: selectedState ? this.selectedFilterFontStyle : this.filterFontStyle,
      marginBottom: 32,
      paddingLeft: 6,
      color: theme.colorPalette.gray[2100],
      fontWeight: selectedState ? '800' : '400',
    };

    return (
      <LabeledRadioButton
        name={displayName}
        onPress={() => onSortSelection(item.id)}
        checked={selectedState}
        disabled={isDisabled}
        buttonSize={14}
        buttonOuterSize={20}
        obj={{
          label: displayName,
          value: id,
        }}
        buttonStyle={radioButtonStyle}
        labelStyle={radioButtonLabelStyle}
        buttonOuterColor={theme.colorPalette.gray[2200]}
      />
    );
  };

  setStyle = (displayAltFilterView, isSelected) => {
    const { isFromFilterPills } = this.props;
    const isSeletedFilter = displayAltFilterView && isSelected;
    let widthHeightIOS = isSeletedFilter ? 18 : 20;
    let widthHeight = 20;
    let borderRadiusValue = 12;
    if (isFromFilterPills) {
      widthHeightIOS = isSeletedFilter ? 42 : 48;
      widthHeight = isSeletedFilter ? 42 : 48;
      borderRadiusValue = isSeletedFilter ? 21 : 25;
    }
    return { widthHeightIOS, widthHeight, borderRadiusValue, isSeletedFilter, isFromFilterPills };
  };

  getSizePropertiesForLabelSize = (displayAltFilterView, isSelected) => {
    const { widthHeightIOS, widthHeight, borderRadiusValue, isSeletedFilter, isFromFilterPills } =
      this.setStyle(displayAltFilterView, isSelected);
    let dimensions = {};
    if (this.isIOS) {
      dimensions = {
        width: widthHeightIOS,
        height: widthHeightIOS,
        borderWidth: isSeletedFilter ? 0.3 : 1,
        borderRadius: borderRadiusValue,
      };
    } else {
      const borderRad = isFromFilterPills ? borderRadiusValue : 12;
      dimensions = {
        width: widthHeight,
        height: widthHeight,
        borderWidth: isSeletedFilter ? 1.3 : 1,
        borderRadius: borderRad,
      };
    }

    return dimensions;
  };

  renderColorSwitchItem = ({ item }) => {
    const { isSelected, displayName, id } = item;
    const { isLoadingMore, displayAltFilterView, isFromFilterPills } = this.props;
    const name = displayName || '';

    const imageUrl = `${FILTER_COLOR_SWATCH_URL}${id}.gif?id=${Math.random() * 1000}`;
    const textStyle = displayAltFilterView
      ? {
          color: theme.colorPalette.gray[900],
          marginTop: isFromFilterPills ? 6 : 0,
          fontWeight: isFromFilterPills && isSelected ? '800' : 'normal',
          textAlign: 'center',
        }
      : {};

    const fontProps = displayAltFilterView
      ? {
          fontFamily: 'secondary',
          fontWeight: isSelected ? 'extrabold' : 'regular',
          color: 'gray.900',
        }
      : {};

    const sizeProps = this.getSizePropertiesForLabelSize(displayAltFilterView, isSelected);

    return (
      <LinkImageIcon
        isFromFilterPills={isFromFilterPills}
        uri={imageUrl}
        selected={isSelected}
        onPress={() => {
          this.onColorItemPress(isLoadingMore, item);
        }}
        name={isFromFilterPills ? capitalized(name) : name}
        isProductImage={false}
        {...sizeProps}
        overflow="hidden"
        imageWithText={displayAltFilterView}
        textStyles={textStyle}
        {...fontProps}
      />
    );
  };

  /**
   * @function onClearAll
   * This method clears selected items list and sets isSelected as false for all selected items
   *
   * @memberof Filters
   */
  onClearAll = () => {
    this.clearAllFilters();

    // Call onSubmit with empty filter list
    const { onSubmit } = this.props;
    if (onSubmit) onSubmit({});
  };

  onPressClear = () => {
    this.onApplyFilter(true);
  };

  clearAllFilters = () => {
    const { selectedItems } = this.state;
    selectedItems.forEach((item) => {
      const updatedItem = item;
      updatedItem.isSelected = false;
    });
  };

  /**
   * @function onApplyFilter
   * This method is called on tap of APPLY button in filters modal
   *
   * @memberof Filters
   */
  onApplyFilter = (isClearFilter) => {
    const { onSubmit } = this.props;
    const { filters } = this.props;
    let selectedFilters = {};

    this.filterNames.forEach((name) => {
      const key = name[0];
      const filterData = filters[key] || [];

      // find all the selected items from filters list and map it with filter key present in filterNames
      const selectedFiltersData = filterData
        .filter((item) => item.isSelected)
        .map((item) => item.id);
      const result = {};
      result[key] = selectedFiltersData;
      selectedFilters = { ...selectedFilters, ...result };
    });

    if (isClearFilter) {
      selectedFilters = this.validateSelectedValues(isClearFilter, selectedFilters);
    }

    if (onSubmit) onSubmit(selectedFilters);
  };

  renderList = (key, title, filterData) => {
    if (filterData && filterData.length > 0) {
      const { selectedItems } = this.state;
      const itemRenderer =
        key === 'TCPColor_uFilter' ? this.renderColorSwitchItem : this.renderListItem;
      return (
        <Container>
          <TitleText>{title}</TitleText>
          <FlatList
            contentContainerStyle={getFlatListContainerStyle()}
            listKey={(item) => item.id}
            data={filterData}
            renderItem={itemRenderer}
            keyExtractor={(item) => item.id}
            initialNumToRender={8}
            maxToRenderPerBatch={2}
            horizontal
            ItemSeparatorComponent={this.renderSeparator}
            showsHorizontalScrollIndicator={false}
            extraData={selectedItems}
          />
        </Container>
      );
    }
    return null;
  };

  renderFavoriteFilters = (filterData) => {
    if (filterData && filterData.length > 0) {
      const { selectedItems } = this.state;
      return (
        <FlatList
          contentContainerStyle={getFlatListContainerStyle()}
          listKey={(item) => item.id}
          data={filterData}
          renderItem={this.renderListItem}
          keyExtractor={(item) => item.id}
          initialNumToRender={8}
          maxToRenderPerBatch={2}
          horizontal
          ItemSeparatorComponent={this.renderSeparator}
          showsHorizontalScrollIndicator={false}
          extraData={selectedItems}
        />
      );
    }
    return null;
  };

  onTabPress = (activeTab) => {
    this.setState({ activeTab });
  };

  renderTabListHeaders = (filterNames, filterDataObject, activeTab) => {
    return filterNames.map((item, tabIndex) => {
      const { isActive, filterName, shouldRender, appliedFilterCount, facetType } =
        this.getFilterConfig({
          item,
          filterDataObject,
          tabIndex,
          activeTab,
        });
      const fontWeight = isActive ? 'extrabold' : 'regular';

      return shouldRender ? (
        <TabListHeader
          activeHeader={isActive}
          onPress={() => {
            this.onTabPress(tabIndex);
          }}
        >
          <TabListHeaderContent activeHeader={isActive}>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs14"
              text={filterName}
              fontWeight={fontWeight}
              color="gray.900"
            />

            {appliedFilterCount && facetType !== FILTER_CONFIG.facetType.sort ? (
              <BodyCopy
                fontFamily="secondary"
                fontSize="fs14"
                fontWeight={fontWeight}
                text={appliedFilterCount}
                color="gray.900"
              />
            ) : null}
          </TabListHeaderContent>
        </TabListHeader>
      ) : null;
    });
  };

  getFacetType = (facetId) => {
    let facetType = '';
    if (facetId === FILTER_CONFIG.facetId.sort) {
      facetType = FILTER_CONFIG.facetType.sort;
    } else if (facetId === FILTER_CONFIG.facetId.color) {
      facetType = FILTER_CONFIG.facetType.color;
    } else if (facetId === FILTER_CONFIG.facetId.size) {
      facetType = FILTER_CONFIG.facetType.size;
    } else {
      facetType = FILTER_CONFIG.facetType.checkBox;
    }
    return facetType;
  };

  getAppliedFilterItemsCount = (filterData) => {
    return filterData.filter((item) => {
      return item.isSelected;
    }).length;
  };

  getFilterConfig = ({ item, filterDataObject, tabIndex, activeTab }) => {
    const filterConfig = {
      isActive: false,
      filterData: [],
      filterName: '',
      shouldRender: false,
    };
    [filterConfig.filterId, filterConfig.filterName] = [item[0], item[1]];
    filterConfig.isActive = tabIndex === activeTab;
    filterConfig.filterData = filterDataObject[filterConfig.filterId] || [];
    filterConfig.appliedFilterCount = this.getAppliedFilterItemsCount(filterConfig.filterData);
    filterConfig.shouldRender =
      filterConfig.filterData.length > 0 &&
      filterConfig.filterId !== FILTER_CONFIG.facetId.groupedSize;
    filterConfig.facetType = this.getFacetType(filterConfig.filterId);

    return filterConfig;
  };

  getRenderMethodBasedOnType = (facetType) => {
    let facetView = null;
    if (facetType === FILTER_CONFIG.facetType.sort) {
      facetView = this.renderSortFilterView;
    } else if (facetType === FILTER_CONFIG.facetType.color) {
      facetView = this.renderColorSwitchItem;
    } else {
      facetView = this.renderCheckBoxFilterView;
    }
    return facetView;
  };

  renderFilterView = ({ filterData, componentRenderer, selectedItems, facetType }) => {
    const { isFromFilterPills } = this.props;
    const showNewColorFilter = isFromFilterPills && facetType === FILTER_CONFIG.facetType.color;

    return facetType === FILTER_CONFIG.facetType.size ? (
      this.renderSizeFilterView({ filterData, componentRenderer, selectedItems })
    ) : (
      <FlatList
        listKey={(key) => key.id}
        data={filterData}
        renderItem={componentRenderer}
        keyExtractor={(key) => key.id}
        extraData={selectedItems}
        numColumns={showNewColorFilter && isFromFilterPills ? 5 : 1}
      />
    );
  };

  getActiveIndexValue = (selectedIndexValue, filterPillsIndex, filterNames) => {
    let index = filterPillsIndex;
    if (filterNames && filterNames.length > 0) {
      filterNames.map((name, indexValue) => {
        if (name[0] === selectedIndexValue) {
          index = indexValue;
        }
        return index;
      });
    }
    return index;
  };

  validateSelectedFilterIndex = (filterPillsIndex, filterNames) => {
    let index = filterPillsIndex;
    switch (filterPillsIndex) {
      case 2:
        index = this.getActiveIndexValue('gender_uFilter', index, filterNames);
        break;
      case 3:
        index = this.getActiveIndexValue('TCPColor_uFilter', index, filterNames);
        break;
      case 4:
        index = this.getActiveIndexValue('v_tcpsize_uFilter', index, filterNames);
        break;
      case 5:
        index = this.getActiveIndexValue('unbxd_price_range_uFilter', index, filterNames);
        break;
      default:
        break;
    }
    return index;
  };

  renderTabListBody = (filterNames, filterDataObject, activeTab) => {
    const { selectedItems } = this.state;
    return filterNames.map((item, tabIndex) => {
      const { isActive, filterData, shouldRender, facetType } = this.getFilterConfig({
        item,
        filterDataObject,
        tabIndex,
        activeTab,
      });

      const componentRenderer = this.getRenderMethodBasedOnType(facetType);
      return shouldRender ? (
        <TabListBodyContent showTab={isActive}>
          <VirtualizedView>
            {this.renderFilterView({ filterData, componentRenderer, selectedItems, facetType })}
          </VirtualizedView>
        </TabListBodyContent>
      ) : null;
    });
  };

  renderAbTestFilterView = (filterNames, filters) => {
    const { activeTab } = this.state;

    return (
      <TabListVertical>
        <TabListHeaderContainer>
          <ScrollView>{this.renderTabListHeaders(filterNames, filters, activeTab)}</ScrollView>
        </TabListHeaderContainer>

        <TabListBodyContainer>
          {this.renderTabListBody(filterNames, filters, activeTab)}
        </TabListBodyContainer>
      </TabListVertical>
    );
  };

  renderSelectedFilterList = () => {
    const { filters, filterPillsIndex } = this.props;
    return (
      <TabListVertical>
        <FilterPillsListBodyContainer>
          {this.renderTabListBody(
            this.filterNames,
            filters,
            this.validateSelectedFilterIndex(filterPillsIndex, this.filterNames)
          )}
        </FilterPillsListBodyContainer>
      </TabListVertical>
    );
  };

  renderFilters = () => {
    const { filters, isFavorite, displayAltFilterView } = this.props;
    if (isFavorite) return this.renderFavoriteFilters(filters);
    return displayAltFilterView
      ? this.renderAbTestFilterView(this.filterNames, filters)
      : this.filterNames.map((item) => {
          const key = item[0];
          const value = item[1];
          const filterData = filters[key] || [];
          return this.renderList(key, value, filterData);
        });
  };

  onScrollEndDrag = (event) => {
    const { onCloseModal } = this.props;

    const isScrolled =
      event &&
      event.nativeEvent &&
      event.nativeEvent.contentOffset &&
      event.nativeEvent.contentOffset.y < 0;

    if (isScrolled) {
      onCloseModal();
    }
  };

  // need this patch for styling radio button text on Android
  getSelectedItemFontStyle = (selectedState) => {
    let fontFamily = theme.typography.fonts.secondary;
    if (!this.isIOS && selectedState) {
      fontFamily = `${theme.typography.fonts.secondary.toLowerCase()}_extrabold`;
    }
    return fontFamily;
  };

  stickyButtonContainerStyling = () => {
    const { showSize } = this.state;
    if (showSize)
      return { flexDirection: 'column', height: 'auto', borderWidth: 0, borderRadius: 30 };
    return {
      flexDirection: 'row',
      height: isAndroid() ? 80 : 90,
      borderWidth: 1,
      borderRadius: 0,
      zIndex: 100,
      paddingBottom: isAndroid() ? 20 : 30,
    };
  };

  validateSelectedValues = (isClearFilter, selectedFilters) => {
    const { filters, filterPillsIndex } = this.props;
    let disableNow = false;
    const selectedFiltersData = selectedFilters;
    switch (filterPillsIndex) {
      case 2:
        if (isClearFilter) {
          selectedFiltersData.gender_uFilter = [];
        } else {
          disableNow =
            filters.gender_uFilter.filter((res) => {
              return res.isSelected;
            }).length === 0;
        }
        break;
      case 3:
        if (isClearFilter) {
          selectedFiltersData.TCPColor_uFilter = [];
        } else {
          disableNow =
            filters.TCPColor_uFilter.filter((res) => {
              return res.isSelected;
            }).length === 0;
        }
        break;
      case 4:
        if (isClearFilter) {
          selectedFiltersData.v_tcpsize_uFilter = [];
        } else {
          disableNow =
            filters.v_tcpsize_uFilter.filter((res) => {
              return res.isSelected;
            }).length === 0;
        }
        break;
      case 5:
        if (isClearFilter) {
          selectedFiltersData.unbxd_price_range_uFilter = [];
        } else {
          disableNow =
            filters.unbxd_price_range_uFilter.filter((res) => {
              return res.isSelected;
            }).length === 0;
        }
        break;
      default:
        break;
    }
    return isClearFilter ? selectedFiltersData : disableNow;
  };

  renderStickyButtons = () => {
    const { showSize } = this.state;
    const { isLoadingMore, displayAltFilterView, labelsFilter } = this.props;
    const stickyButtonContainer = this.stickyButtonContainerStyling();
    const addToBagGymStyling = isGymboree() ? { borderRadius: 23 } : {};
    const startSpinning = displayAltFilterView && isLoadingMore;
    const disable = this.validateSelectedValues();
    const disableButton = startSpinning || disable ? { opacity: 0.5 } : {};

    return (
      <View style={[FilterPillsStyle.view2, stickyButtonContainer]}>
        {!showSize && (
          <ClearBtnWrapper
            accessibilityRole="button"
            onPress={this.onPressClear}
            disabled={startSpinning || disable}
            style={[FilterPillsStyle.touchableOpacity5, disableButton, addToBagGymStyling]}
          >
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs16"
              fontWeight="bold"
              color="blue.900"
              letterSpacing="ls02"
              height="46px"
              text={labelsFilter?.lbl_clearTxt}
              textAlign="center"
            />
          </ClearBtnWrapper>
        )}
        {this.renderAddToBagButton()}
      </View>
    );
  };

  renderAddToBagButton = () => {
    const { isLoadingMore, totalProductsCount, closeModal, displayAltFilterView, labelsFilter } =
      this.props;
    const startSpinning = displayAltFilterView && isLoadingMore;
    return (
      <AtbButton>
        <CustomButton
          fontSize="fs16"
          fontWeight="regular"
          fontFamily="secondary"
          paddings="10px"
          textPadding="0px 5px"
          margin="18px 0px 0px 0px"
          borderRadius={this.getButtonBorderRadius}
          showShadow
          fill="BLUE"
          disableButton={isLoadingMore}
          buttonVariation="variable-width"
          height="46px"
          wrapContent={false}
          text={
            isLoadingMore
              ? ''
              : `${labelsFilter?.lbl_show_results_txt || ''} (${totalProductsCount})`
          }
          onPress={closeModal}
        />
        {isLoadingMore ? <SpinnerIcon startSpinning={startSpinning} /> : null}
      </AtbButton>
    );
  };

  getButtonBorderRadius = () => {
    return isGymboree() ? '23px' : '16px';
  };

  render() {
    const {
      labelsFilter,
      isFavorite,
      closeModal,
      onCloseModal,
      displayAltFilterView,
      totalProductsCount,
      isLoadingMore,
      isFromFilterPills,
      titleLabel,
    } = this.props;

    const startSpinning = displayAltFilterView && isLoadingMore;
    return displayAltFilterView ? (
      <>
        <ModalHeaderCurved
          labelTitle={titleLabel}
          onRequestClose={onCloseModal}
          labelAction={labelsFilter.lbl_clear}
          onActionClick={this.onClearAll}
          titleStyles={this.titleStyles}
          actionStyles={this.clearAllCtaStyle}
          overrideCSS={{ styledCrossImage: 'margin: 0', styledTouchableOpacity: 'padding: 0' }}
          hideTopBar
          isFromFilterPills={isFromFilterPills}
        />
        {isFromFilterPills ? (
          <>
            {this.renderSelectedFilterList()}
            <StickyContainer>{this.renderStickyButtons()}</StickyContainer>
          </>
        ) : (
          <>
            {this.renderFilters()}
            <ShowResultButtonContainer hasNotch={this.hasNotch}>
              <>
                <Button
                  name="show_results"
                  color="white"
                  fill="BLUE"
                  width="100%"
                  fontFamily="secondary"
                  fontWeight="extrabold"
                  fontSize="fs14"
                  text={
                    isLoadingMore
                      ? ''
                      : `${labelsFilter.lbl_show_results || ''} (${totalProductsCount})`
                  }
                  buttonVariation="variable-width"
                  paddings="18px"
                  onPress={closeModal}
                  disableButton={isLoadingMore}
                  useCustomFontStyle
                />
                {isLoadingMore ? <SpinnerIcon startSpinning={startSpinning} /> : null}
              </>
            </ShowResultButtonContainer>
          </>
        )}
      </>
    ) : (
      <PageContainer>
        {this.renderFilters()}
        {!isFavorite && (
          <ApplyAndClearButtonContainer>
            <Button
              fill="WHITE"
              type="submit"
              data-locator=""
              text={labelsFilter.lbl_clear}
              onPress={this.onClearAll}
              accessibilityLabel={labelsFilter.lbl_clear}
              width="48%"
              fontSize="fs14"
            />
            <Button
              fill="BLACK"
              type="submit"
              data-locator=""
              text={labelsFilter.lbl_apply}
              onPress={closeModal}
              accessibilityLabel={labelsFilter.lbl_apply}
              width="48%"
              fontSize="fs14"
            />
          </ApplyAndClearButtonContainer>
        )}
      </PageContainer>
    );
  }
}

export default withStyles(withTheme(Filters), styles);
export { Filters as FiltersVanilla };
