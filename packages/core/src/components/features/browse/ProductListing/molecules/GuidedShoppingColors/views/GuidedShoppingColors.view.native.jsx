// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Field } from 'redux-form';
import { PropTypes } from 'prop-types';

import { getScreenWidth } from '@tcp/core/src/utils/utils.app';
import { Carousel } from '@tcp/core/src/components/common/molecules';
import { Button, Anchor } from '@tcp/core/src/components/common/atoms';
import { WHITE } from '../../../organisms/EssentialShop/container/EssentialShop.constants';
import { getFacetList } from '../../../organisms/EssentialShop/container/EssentialShop.utils';
import {
  CarouselWrapper,
  ButtonWrapper,
  ClearColorAnchor,
  ColorImageWrapper,
  ColorImage,
  BorderImage,
  SelectedImage,
  BodyCopyText,
  CustomButton,
} from '../styles/GuidedShoppingColors.style.native';

const whiteCrossIcon = require('../../../../../../../../src/assets/white-cross-icon.png');

/**
 * Module height and width.
 * Height is fixed for mobile
 * Width can vary as per device width.
 */
const MODULE_HEIGHT = 220;
const MODULE_WIDTH = getScreenWidth() - 60;
const horizontalMargin = 15;
const slideWidth = MODULE_WIDTH / 5.5;
const itemWidth = slideWidth + horizontalMargin;

class GuidedShoppingColors extends PureComponent {
  /**
   * @method handleChange
   * @description Handles the color seletion
   */
  handleChange = (e, value, values, onChange) => {
    const arr = [...values];
    if (arr.includes(value)) {
      arr.splice(arr.indexOf(value), 1);
    } else {
      arr.push(value);
    }
    return onChange(arr);
  };

  /**
   * @description For White color option, need to have default border
   */
  getColorOptionStyle = isWhiteColor => {
    return {
      width: isWhiteColor ? 52 : 56,
      height: isWhiteColor ? 52 : 56,
      borderRadius: isWhiteColor ? 52 / 2 : 56 / 2,
      marginTop: 2,
    };
  };

  itemRenderer = (item, input) => {
    const { item: option } = item;
    const { id = '', displayName = '', imagePath } = option;
    const { onChange, value: values = [] } = input;
    const isChecked = values.includes(id);
    const isWhiteColor = (displayName || '').toUpperCase() === WHITE;
    return (
      <View>
        <TouchableOpacity
          accessibilityRole="image"
          onPress={e => {
            this.handleChange(e, id, values, onChange);
          }}
        >
          <ColorImageWrapper>
            <BorderImage isChecked={isChecked} isWhite={isWhiteColor}>
              <ColorImage
                uriInfo={{ uri: imagePath }}
                alt={displayName}
                isFastImage
                isBackground={false}
                customStyle={this.getColorOptionStyle(isWhiteColor)}
              />
              {isChecked && <SelectedImage source={whiteCrossIcon} alt={displayName} />}
            </BorderImage>
            <BodyCopyText
              fontFamily="secondary"
              text={displayName}
              isChecked={isChecked}
              isWhite={isWhiteColor}
            />
          </ColorImageWrapper>
        </TouchableOpacity>
      </View>
    );
  };

  /**
   * @method renderCarousel
   * @description Renders the Carousel consisting of all the colors
   */
  renderCarousel = ({ input, options }) => {
    const { onChange } = input;
    const { filterFormValues, clearColorHandler, stepInfo } = this.props;
    const { facetName = '' } = stepInfo;
    return (
      <>
        <Carousel
          name="carousel"
          data={options}
          renderItem={item => this.itemRenderer(item, input)}
          height={MODULE_HEIGHT}
          sliderWidth={MODULE_WIDTH}
          itemWidth={itemWidth}
          activeSlideAlignment="start"
          inactiveSlideOpacity={1}
          autoplay={false}
          loop={false}
          carouselConfig={{
            autoplay: false,
          }}
          isUseScrollView
        />
        {filterFormValues[facetName] && !!filterFormValues[facetName].length && (
          <ClearColorAnchor>
            <Anchor
              fontFamily="secondary"
              underline
              anchorVariation="primary"
              onPress={e => {
                e.preventDefault();
                onChange([]);
                clearColorHandler(facetName);
              }}
              to="/#"
              dataLocator=""
              text="Clear All Colors"
            />
          </ClearColorAnchor>
        )}
      </>
    );
  };

  /**
   * @method renderCarousel
   * @description Main function to render the component
   */
  render() {
    const { stepInfo, filters, setActiveStep, onSubmit, labels, activeDotCount } = this.props;
    const { options = [], button = {}, facetName = '' } = stepInfo;
    const { text = '' } = button;

    const filteredList = filters[facetName] && getFacetList(options, filters[facetName]);
    return (
      <View>
        <CarouselWrapper>
          <Field
            id="essential_colors"
            name={facetName}
            type="checkbox"
            options={filteredList}
            component={this.renderCarousel}
          />
        </CarouselWrapper>
        <ButtonWrapper>
          {activeDotCount > 0 && (
            <CustomButton
              color="black"
              fill="WHITE"
              text={labels.back}
              fontSize="fs12"
              fontFamily="secondary"
              accessibilityLabel="< BACK"
              accessibilityRole="button"
              width="48%"
              onPress={() => setActiveStep('dec')}
            />
          )}
          <Button
            color="white"
            fill="BLUE"
            fontSize="fs12"
            fontFamily="secondary"
            accessibilityRole="button"
            text={labels.colorNext}
            accessibilityLabel={text}
            onPress={onSubmit}
            width="48%"
          />
        </ButtonWrapper>
      </View>
    );
  }
}

GuidedShoppingColors.propTypes = {
  activeDotCount: PropTypes.number,
  stepInfo: PropTypes.shape({}),
  filters: PropTypes.shape([]),
  onSubmit: PropTypes.func.isRequired,
  setActiveStep: PropTypes.func.isRequired,
  filterFormValues: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  clearColorHandler: PropTypes.func.isRequired,
};

GuidedShoppingColors.defaultProps = {
  activeDotCount: 0,
  stepInfo: {},
  filters: [],
  filterFormValues: {},
};

export default GuidedShoppingColors;
