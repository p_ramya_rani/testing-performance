/* eslint-disable max-lines */
/* eslint-disable complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import isEqual from 'lodash/isEqual';
import { connect } from 'react-redux';
import { FlatList, Dimensions } from 'react-native';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { getBrand, disableTcpAppProduct, parseBoolean } from '@tcp/core/src/utils';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import AddedToBagContainer from '@tcp/core/src/components/features/CnC/AddedToBag';
import QuickViewModal from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.container';
import {
  getModalState,
  getUniqueId,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import PAGES from '@tcp/core/src/constants/pages.constants';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import { TIER1 } from '@tcp/core/src/constants/tiering.constants';
import ListItem from '../../ProductListItem';
import { getMapSliceForColorProductId } from '../utils/productsCommonUtils';
import { getPromotionalMessage } from '../utils/utility';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import {
  styles,
  PageContainer,
  HeaderContainer,
  RecommendationContainer,
  GridPromoContainer,
  InGridRecContainer,
  InMultiGridRecContainer,
  BlankProductForMultiGrid,
} from '../styles/ProductList.style.native';
import OpenLoginModal from '../../../../../account/LoginPage/container/LoginModal.container';
import Recommendations from '../../../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import GridPromo from '../../../../../../common/molecules/GridPromo';
import { APP_TYPE } from '../../../../../../../../../mobileapp/src/components/common/hoc/ThemeWrapper.constants';
import { getQuickViewLoader } from '../../../../../../common/molecules/Loader/container/Loader.selector';

class ProductList extends React.Component {
  flatListRef = null;

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      favorites: true,
      colorProductId: '',
    };

    const { navigation } = props;

    let defaultDeviceTier = TIER1;
    this.deviceTier = defaultDeviceTier;

    if (navigation) {
      const { deviceTier } = navigation.getScreenProps();
      defaultDeviceTier = deviceTier;
    }
    this.deviceTier = defaultDeviceTier;
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate(prevProps) {
    const isScrollToTopValue = get(this.props, 'scrollToTop');
    const isScrollToTopPrevPropValue = get(prevProps, 'scrollToTop');
    if (isScrollToTopValue && isScrollToTopValue !== isScrollToTopPrevPropValue) {
      this.scrollToTop();
    }
  }

  componentWillUnmount() {
    const { removeAddToFavoritesErrorMsg } = this.props;
    if (typeof removeAddToFavoritesErrorMsg === 'function') {
      removeAddToFavoritesErrorMsg('');
    }
  }

  static getDerivedStateFromProps(props, state) {
    const { onAddItemToFavorites, getProducts, navigation, isSearchListing } = props;
    const { colorProductId } = state;
    if (props.isLoggedIn && state.showModal) {
      this.categoryUrl = navigation && navigation.getParam('url');
      getProducts({ URI: 'category', url: this.categoryUrl, ignoreCache: true });
      if (colorProductId !== '') {
        onAddItemToFavorites({ colorProductId, page: isSearchListing ? 'SLP' : 'PLP' });
      }
      return { showModal: false, colorProductId: '' };
    }
    return null;
  }

  // eslint-disable-next-line
  onAddToBag = (data) => {};

  // eslint-disable-next-line
  onFavorite = (generalProductId, itemId, productInfo, index) => {
    const { deleteFavItemInProgressFlag } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const {
      isLoggedIn,
      onAddItemToFavorites,
      isSearchListing,
      isFavorite,
      setLastDeletedItemId,
      breadCrumbs,
    } = this.props;
    if (!isLoggedIn) {
      this.setState({ colorProductId: generalProductId });
      this.setState({ showModal: true });
    } else if (isFavorite) {
      if (setLastDeletedItemId) setLastDeletedItemId({ itemId, products: productInfo });
    } else {
      onAddItemToFavorites({
        colorProductId: generalProductId,
        page: isSearchListing ? 'SLP' : 'PLP',
        products: productInfo,
        index,
        breadCrumbs,
      });
    }
  };

  onFavoriteRemove = (
    generalProductId,
    itemId,
    productInfo,
    setLastDeletedItemId,
    name,
    labelsFavorite
  ) => {
    const { deleteFavItemInProgressFlag } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const { onSetLastDeletedItemIdAction, wishList } = this.props;

    if (wishList && wishList[generalProductId]) {
      const { externalIdentifier, giftListItemID } = wishList && wishList[generalProductId];
      onSetLastDeletedItemIdAction({
        externalId: externalIdentifier,
        itemId: giftListItemID,
        notFromFavoritePage: true,
      });
    }
    if (setLastDeletedItemId) {
      setLastDeletedItemId({
        itemId,
        products: productInfo,
        toastMessage: `${name} ${labelsFavorite.lbl_fav_removeFavorite}`,
      });
    }
  };

  toggleModal = () => {
    this.setState((state) => ({
      showModal: !state.showModal,
      favorites: false,
    }));
  };

  onOpenPDPPageHandler = (pdpUrl, selectedColorIndex, productInfo, item) => {
    const { title, onGoToPDPPage, isFavorite, updateAppTypeHandler } = this.props;
    const { name } = productInfo;
    const currentAppBrand = getBrand();
    const isTCP = item.itemInfo ? item.itemInfo.isTCP : currentAppBrand.toUpperCase() === 'TCP';
    const itemBrand = isTCP ? 'TCP' : 'GYM';
    const isProductBrandOfSameSiteBrand = currentAppBrand.toUpperCase() === itemBrand.toUpperCase();
    const productTitle = isFavorite ? name : title;
    if (!isProductBrandOfSameSiteBrand) {
      if (disableTcpAppProduct(itemBrand)) {
        return;
      }
      updateAppTypeHandler({
        type: currentAppBrand.toLowerCase() === APP_TYPE.TCP ? APP_TYPE.GYMBOREE : APP_TYPE.TCP,
        params: {
          title,
          pdpUrl,
          selectedColorProductId: selectedColorIndex,
          reset: true,
        },
      });
    } else if (onGoToPDPPage) {
      onGoToPDPPage(productTitle, pdpUrl, selectedColorIndex, productInfo);
    }
  };

  getLoyaltyPromotionMessage = (productInfo, colorsMap) => {
    const { isPlcc } = this.props;
    const { promotionalMessage, promotionalPLCCMessage } = productInfo;
    return (
      colorsMap &&
      getPromotionalMessage(isPlcc, {
        promotionalMessage,
        promotionalPLCCMessage,
      })
    );
  };

  renderComponent = ({ showModal, isLoggedIn, favorites }) => {
    let componentContainer = null;
    if (!isLoggedIn) {
      componentContainer = (
        <OpenLoginModal
          component="login"
          openState={showModal}
          setLoginModalMountState={this.toggleModal}
          isUserLoggedIn={isLoggedIn}
          variation={favorites && 'favorites'}
        />
      );
    }
    return <React.Fragment>{componentContainer}</React.Fragment>;
  };

  checkAndRenderSuggestedItem = (item) => {
    const { seeSuggestedDictionary } = this.props;
    const suggestedItem = {
      status: false,
      attributes: null,
    };
    const skuInfoColorProductId = get(item, 'skuInfo.colorProductId', null);
    const outOfStockProduct =
      skuInfoColorProductId &&
      seeSuggestedDictionary &&
      seeSuggestedDictionary[skuInfoColorProductId];
    const outOfStockColorProductId = outOfStockProduct && outOfStockProduct.colorProductId;
    const suggestedAttributes = outOfStockProduct && outOfStockProduct.attributes;

    if (outOfStockColorProductId && outOfStockColorProductId === skuInfoColorProductId) {
      suggestedItem.status = true;
      suggestedItem.attributes = suggestedAttributes;
    }
    return suggestedItem;
  };

  /**
   * @param {Object} itemData : product list item
   * @desc This is renderer method of the product tile list
   */
  // eslint-disable-next-line sonarjs/cognitive-complexity
  renderItemList = (itemData) => {
    const {
      isMatchingFamily,
      currencyExchange,
      currencySymbol,
      isPlcc,
      onQuickViewOpenClick,
      isFavorite,
      setLastDeletedItemId,
      isLoggedIn,
      labelsPlpTiles,
      isKeepAliveEnabled,
      outOfStockLabels,
      renderMoveToList,
      addToBagEcom,
      onSeeSuggestedItems,
      errorMessages,
      labelsFavorite,
      isOnModelAbTestPlp,
      suggestedProductErrors,
      hasSuggestedProduct,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      accessibilityLabels,
      isHidePLPAddToBag,
      isHidePLPRatings,
      noCarousel,
      wishList,
      isBrierleyPromoEnabled,
      labels,
      isSearchListing,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      isNewReDesignProductTile,
      navigation,
      quickViewProductId,
      quickViewLoader,
      alternateBrand,
    } = this.props;
    const { item, index } = itemData;
    const suggestedItem = this.checkAndRenderSuggestedItem(item);
    if (suggestedItem.status) {
      return (
        <RecommendationContainer>
          <Recommendations
            {...suggestedItem.attributes}
            hasSuggestedProduct={suggestedItem.status}
            sequence="1"
          />
        </RecommendationContainer>
      );
    }

    if (item.itemType === 'gridPromo') {
      const variation = item.gridStyle;
      return (
        <GridPromoContainer fullWidth={variation === 'horizontal'}>
          <GridPromo promoObj={item.itemVal} variation={item.gridStyle} />
        </GridPromoContainer>
      );
    }

    if (item.itemType === 'inGridPlpRecommendation') {
      return (
        <InGridRecContainer>
          <Recommendations
            portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS}
            variation="moduleP"
            page={Constants.RECOMMENDATIONS_PAGES_MAPPING.PLP}
            slots={item.inGridSlots}
            index={item.slotIndex}
            navigation={navigation}
            onFavorite={this.onFavorite}
            onFavoriteRemove={this.onFavoriteRemove}
            inGridPlpRecommendation
          />
        </InGridRecContainer>
      );
    }

    if (item.itemType === 'inMultiGridPlpRecommendation') {
      return (
        <InMultiGridRecContainer>
          <Recommendations
            portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.IN_MULTI_GRID_PLP_PRODUCTS}
            variation="moduleP"
            page={Constants.RECOMMENDATIONS_PAGES_MAPPING.PLP}
            slots={item.inGridSlots}
            index={item.slotIndex}
            flatSlots={item.flatSlots}
            navigation={navigation}
            onFavorite={this.onFavorite}
            onFavoriteRemove={this.onFavoriteRemove}
            inGridPlpRecommendation
          />
        </InMultiGridRecContainer>
      );
    }

    if (item.itemType === 'blankProductForMultiGrid') {
      return <BlankProductForMultiGrid />;
    }

    const { colorsMap, productInfo } = item;
    const colorProductId = colorsMap && colorsMap[0].colorProductId;

    // get default zero index color entry
    const curentColorEntry = colorsMap && getMapSliceForColorProductId(colorsMap, colorProductId);
    // get product color and price info of default zero index item
    const currentColorMiscInfo = (colorsMap && curentColorEntry.miscInfo) || {};
    const { badge1, badge2 } = currentColorMiscInfo;

    // get default top badge data
    let topBadge;
    if (colorsMap) {
      topBadge = isMatchingFamily && badge1.matchBadge ? badge1.matchBadge : badge1.defaultBadge;
    }

    const getPriceRange = isShowPriceRangeKillSwitch
      ? parseBoolean(isShowPriceRangeKillSwitch)
      : false;
    const getABTestPriceRange = showPriceRangeForABTest;
    const showPriceRange = getPriceRange && getABTestPriceRange;

    // get default Loyalty message
    const loyaltyPromotionMessage = isBrierleyPromoEnabled
      ? labels.pointsAndRewardsText
      : this.getLoyaltyPromotionMessage(productInfo, colorsMap);

    return (
      <ListItem
        item={item}
        isMatchingFamily={isMatchingFamily}
        badge1={topBadge}
        badge2={badge2}
        isPlcc={isPlcc}
        loyaltyPromotionMessage={loyaltyPromotionMessage}
        onAddToBag={this.onAddToBag}
        onFavorite={this.onFavorite}
        onFavoriteRemove={this.onFavoriteRemove}
        currencyExchange={currencyExchange}
        currencySymbol={currencySymbol}
        onGoToPDPPage={this.onOpenPDPPageHandler}
        onQuickViewOpenClick={onQuickViewOpenClick}
        isFavorite={isFavorite}
        setLastDeletedItemId={setLastDeletedItemId}
        isLoggedIn={isLoggedIn}
        labelsPlpTiles={labelsPlpTiles}
        isKeepAliveEnabled={isKeepAliveEnabled}
        outOfStockLabels={outOfStockLabels}
        renderMoveToList={renderMoveToList}
        addToBagEcom={addToBagEcom}
        onSeeSuggestedItems={onSeeSuggestedItems}
        errorMessages={errorMessages}
        index={index}
        labelsFavorite={labelsFavorite}
        isOnModelAbTestPlp={isOnModelAbTestPlp}
        suggestedProductErrors={suggestedProductErrors}
        hasSuggestedProduct={hasSuggestedProduct}
        disableVideoClickHandler
        showPriceRange={showPriceRange}
        noCarousel={noCarousel}
        accessibilityLabels={accessibilityLabels}
        isHidePLPAddToBag={isHidePLPAddToBag}
        isHidePLPRatings={isHidePLPRatings}
        isOptimizedVideo
        wishList={wishList}
        isBrierleyPromoEnabled={isBrierleyPromoEnabled}
        fromPage={isSearchListing ? PAGES.SLP : PAGES.PLP}
        onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        isNewReDesignProductTile={isNewReDesignProductTile}
        renderVideoAsImage={this.deviceTier !== TIER1}
        productImageHeight={isNewReDesignProductTile ? 190 : 0}
        primaryBrand={productInfo && productInfo.primaryBrand}
        quickViewProductId={quickViewProductId}
        quickViewLoader={quickViewLoader}
        alternateBrand={alternateBrand}
      />
    );
  };

  onLoadMoreProductsHandler = () => {
    const { onLoadMoreProducts } = this.props;
    if (onLoadMoreProducts) {
      onLoadMoreProducts(this.deviceTier);
    }
  };

  /**
   * @desc This is render product list load more footer
   */
  renderHeader = () => {
    const { onRenderHeader } = this.props;
    if (onRenderHeader) {
      return <HeaderContainer>{onRenderHeader()}</HeaderContainer>;
    }
    return null;
  };

  setListRef = (ref) => {
    const { setListRef } = this.props;
    this.flatListRef = ref;
    if (setListRef) {
      setListRef(ref);
    }
  };

  getColumnWrapperStyle = () => {
    return {
      justifyContent: 'space-between',
    };
  };

  /**
   * @desc This is render product list
   */
  renderList = () => {
    const { isLoggedIn, AddToFavoriteErrorMsg, products, navigation, isQVModalOpen } = this.props;
    const { showModal, favorites } = this.state;
    return (
      <>
        {AddToFavoriteErrorMsg !== '' && (
          <Notification status="error" message={`Error : ${AddToFavoriteErrorMsg}`} />
        )}
        {products && products.length > 0 && (
          <FlatList
            ref={(ref) => this.setListRef(ref)}
            data={products}
            renderItem={this.renderItemList}
            keyExtractor={(item, index) =>
              (item.productInfo && item.productInfo.generalProductId) ||
              (item.itemVal && item.itemVal.slot) ||
              index
            }
            windowSize={21}
            numColumns={2}
            initialNumToRender={10}
            maxToRenderPerBatch={10}
            extraData={this.props}
            ListHeaderComponent={this.renderHeader}
            stickyHeaderIndices={[0]}
            columnWrapperStyle={this.getColumnWrapperStyle()}
          />
        )}
        {showModal &&
          this.renderComponent({
            showModal,
            isLoggedIn,
            favorites,
          })}
        {isQVModalOpen && <QuickViewModal navigation={navigation} />}
        <AddedToBagContainer navigation={navigation} />
      </>
    );
  };

  scrollToTop = () => {
    if (this.flatListRef) {
      this.flatListRef.scrollToOffset({ animated: true, offset: 0 });
    }
  };

  render() {
    const { width } = Dimensions.get('window');
    const { isNewReDesignProductTile, isSearchListing } = this.props;
    return (
      <PageContainer
        width={width}
        isNewReDesignProductTile={isNewReDesignProductTile}
        isSearchListing={isSearchListing}
      >
        {this.renderList()}
      </PageContainer>
    );
  }
}

ProductList.propTypes = {
  // TODO: Disable eslint for the proptypes as some of the values are not being used in the list. This will be cover in kill swithc story.
  /* eslint-disable */
  products: PropTypes.arrayOf(PropTypes.shape({})),
  /** the generalProductId of the product (if any) requesting quickView to show */
  showQuickViewForProductId: PropTypes.string,
  /** Price related currency symbol to be rendered */
  currencySymbol: PropTypes.string,
  currencyExchange: PropTypes.arrayOf(PropTypes.shape({})),
  /** callback for clicks on wishlist CTAs. Accepts: colorProductId. */
  onAddItemToFavorites: PropTypes.func,
  /** callback for clicks on quickView CTAs. Accepts a generalProductId, colorProductId */
  onQuickViewOpenClick: PropTypes.func,
  onPickUpOpenClick: PropTypes.func,
  /** callback to trigger when the user chooses to display a different color (used to retrieve prices) */
  onColorChange: PropTypes.func,
  /** When flase, flags that BOPIS is globaly disabled */
  isBopisEnabled: PropTypes.bool,
  /* This unbxd request ID will be passed to UNXD product click anlytics as request ID */
  unbxdId: PropTypes.string,
  onProductCardHover: PropTypes.func,
  isBopisEnabledForClearance: PropTypes.bool,
  onQuickBopisOpenClick: PropTypes.func,
  siblingProperties: PropTypes.shape({
    colorMap: PropTypes.arrayOf(PropTypes.shape({})),
    promotionalMessage: PropTypes.string,
    promotionalPLCCMessage: PropTypes.string,
  }),
  loadedProductCount: PropTypes.number.isRequired,
  isMatchingFamily: PropTypes.bool,
  isPlcc: PropTypes.bool,
  /* eslint-enable */
  onGoToPDPPage: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  onLoadMoreProducts: PropTypes.func.isRequired,
  onRenderHeader: PropTypes.func.isRequired,
  setListRef: PropTypes.func,
  isFavorite: PropTypes.bool,
  setLastDeletedItemId: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  labelsLogin: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  accessibilityLabels: PropTypes.shape({}),
  labelsPlpTiles: PropTypes.shape({}),
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  isSearchListing: PropTypes.bool,
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  renderMoveToList: PropTypes.func,
  addToBagEcom: PropTypes.func,
  onSeeSuggestedItems: PropTypes.func,
  seeSuggestedDictionary: PropTypes.shape({}),
  isSuggestedItem: PropTypes.bool,
  errorMessages: PropTypes.shape({}),
  updateAppTypeHandler: PropTypes.func.isRequired,
  labelsFavorite: PropTypes.shape({}),
  isOnModelAbTestPlp: PropTypes.bool,
  suggestedProductErrors: PropTypes.shape({}),
  hasSuggestedProduct: PropTypes.bool,
  isShowPriceRangeKillSwitch: PropTypes.bool,
  showPriceRangeForABTest: PropTypes.bool,
  noCarousel: PropTypes.bool,
  navigation: PropTypes.shape({}),
  isQVModalOpen: PropTypes.bool.isRequired,
  isHidePLPAddToBag: PropTypes.bool,
  isHidePLPRatings: PropTypes.bool,
  breadCrumbs: PropTypes.shape([]),
  wishList: PropTypes.shape({}).isRequired,
  isBrierleyPromoEnabled: PropTypes.bool,
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isNewReDesignProductTile: PropTypes.bool,
  quickViewProductId: PropTypes.string,
  quickViewLoader: PropTypes.bool,
};

ProductList.defaultProps = {
  setListRef: () => {},
  products: [],
  showQuickViewForProductId: '',
  onAddItemToFavorites: () => {},
  onQuickViewOpenClick: () => {},
  onPickUpOpenClick: () => {},
  onColorChange: () => {},
  isBopisEnabled: false,
  unbxdId: 'fc0d2287-4a11-4739-98b4-1e2fd91016c4',
  onProductCardHover: () => {},
  isBopisEnabledForClearance: false,
  onQuickBopisOpenClick: () => {},
  currencyExchange: [{ exchangevalue: 1 }],
  siblingProperties: {
    colorMap: [],
    promotionalMessage: '',
    promotionalPLCCMessage: '',
  },
  isMatchingFamily: true,
  isPlcc: false,
  currencySymbol: '$',
  isFavorite: false,
  isLoggedIn: false,
  labelsLogin: {
    logIn: '',
  },
  labelsPlpTiles: {},
  accessibilityLabels: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  isSearchListing: false,
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  renderMoveToList: () => {},
  addToBagEcom: () => {},
  onSeeSuggestedItems: () => {},
  seeSuggestedDictionary: null,
  isSuggestedItem: false,
  errorMessages: null,
  labelsFavorite: {
    lbl_fav_remove_item: 'was removed from your favorites.',
  },
  isOnModelAbTestPlp: false,
  suggestedProductErrors: {},
  hasSuggestedProduct: false,
  isShowPriceRangeKillSwitch: false,
  showPriceRangeForABTest: false,
  isHidePLPAddToBag: false,
  isHidePLPRatings: false,
  noCarousel: false,
  navigation: {},
  breadCrumbs: [],
  isBrierleyPromoEnabled: false,
  labels: {},
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isDynamicBadgeEnabled: {},
  isNewReDesignProductTile: false,
  quickViewProductId: '',
  quickViewLoader: false,
};

const mapStateToProps = (state) => {
  return {
    isQVModalOpen: getModalState(state),
    quickViewProductId: getUniqueId(state),
    quickViewLoader: getQuickViewLoader(state),
  };
};

export default withStyles(connect(mapStateToProps)(ProductList), styles);
export { ProductList as ProductListVanilla };
