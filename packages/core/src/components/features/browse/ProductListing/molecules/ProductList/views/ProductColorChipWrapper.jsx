// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module ProductColorChipWrapper
 *
 * @author Agu
 */
import React from 'react';
import { PropTypes } from 'prop-types';
import { Swipeable } from 'react-swipeable';
import isEqual from 'lodash/isEqual';
import {
  isClient,
  getViewportInfo,
  configureInternalNavigationFromCMSUrl,
} from '@tcp/core/src/utils';
import { isTier1Device } from '@tcp/web/src/utils/device-tiering';
import { getIconPath } from '../../../../../../../utils';
import { breakpoints } from '../../../../../../../../styles/themes/TCP/mediaQuery';
import { COLOR_PROP_TYPE } from '../propTypes/productsAndItemsPropTypes';
import ProductColorChip from './ProductColorChip';
import withStyles from '../../../../../../common/hoc/withStyles';
import { BodyCopy, Anchor } from '../../../../../../common/atoms';
import styles from '../styles/ProductColorChipWrapper.style';
import { ProductSKUInfo } from './ProductItemComponents';

class ProductColorChipWrapper extends React.Component {
  static propTypes = {
    /**
     * Callback for clicks on color chips. Accepts colorProductId, colorName.
     * Note that it is up to this callback to update the selectedColorId prop of this component.
     */
    onChipClick: PropTypes.func,
    /** the color name of the currently selected chip */
    selectedColorId: PropTypes.string.isRequired,
    isPLPredesign: PropTypes.bool.isRequired,
    showColorEvenOne: PropTypes.bool.isRequired,
    className: PropTypes.string.isRequired,
    skuInfo: PropTypes.shape(PropTypes.string).isRequired,
    /** map of available colors to render chips for */
    colorsMap: PropTypes.arrayOf(
      PropTypes.shape({
        color: COLOR_PROP_TYPE.isRequired,
        colorProductId: PropTypes.string.isRequired,
      })
    ),
    relatedSwatchImages: PropTypes.arrayOf(PropTypes.string),
    imagesByColor: PropTypes.shape({}),
    isFavoriteView: PropTypes.bool.isRequired,
    itemBrand: PropTypes.string.isRequired,
    pdpUrl: PropTypes.string,
  };

  static defaultProps = {
    onChipClick: () => {},
    colorsMap: [],
    relatedSwatchImages: [],
    imagesByColor: {},
    pdpUrl: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      firstItemIndex: 0,
      // activeItem: 0,
      isArrEnd: false,
      maxVisibleItems: 5,
      isScrollEndReached: false,
    };

    this.captureContainerRef = this.captureContainerRef.bind(this);

    this.handleNextClick = this.handleNextClick.bind(this);
    this.swipeConfig = {
      delta: 10, // min distance(px) before a swipe starts
      preventDefaultTouchmoveEvent: false, // preventDefault on touchmove, *See Details*
      trackTouch: true, // track touch input
      trackMouse: false, // track mouse input
      rotationAngle: 0,
    };
  }

  componentDidMount = () => {
    const availableNextColorArrowWidth = 19;
    const colorSwatchWidthForDesktop = 34;
    const colorSwatchWidthForTabMobile = 21;
    const defaultMaxVisibleItems = 5;
    const divWidth =
      this.containerRef && this.containerRef.clientWidth - availableNextColorArrowWidth;
    const colorSwatchWidth =
      window.screen.width >= breakpoints.values.lg
        ? colorSwatchWidthForDesktop
        : colorSwatchWidthForTabMobile;
    this.setState({
      maxVisibleItems: Math.round(divWidth / colorSwatchWidth) || defaultMaxVisibleItems,
    });
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  captureContainerRef = (ref) => {
    this.containerRef = ref;
  };

  handleNextClick = () => {
    const { isPLPredesign, colorsMap } = this.props;
    const { maxVisibleItems, firstItemIndex } = this.state;
    const stepSize = maxVisibleItems;
    const nextStartIndex = firstItemIndex + 1;
    const maxViewableIndex = isPLPredesign
      ? colorsMap.length - stepSize + 1
      : colorsMap.length - stepSize;
    const isEndBounded = nextStartIndex >= maxViewableIndex;
    let firstItemIndexVar = nextStartIndex;
    if (isEndBounded) {
      if (isPLPredesign) {
        firstItemIndexVar = 0;
      } else {
        firstItemIndexVar = maxViewableIndex;
      }
    }
    this.setState({
      firstItemIndex: firstItemIndexVar,
      isArrEnd: isEndBounded,
    });
  };

  handleNextEndClick = () => {
    if (this.colorSwatchRefMobile) {
      const { isScrollEndReached } = this.state;
      if (!isScrollEndReached) {
        this.colorSwatchRefMobile.scrollTo(this.colorSwatchRefMobile.scrollWidth, 0);
      } else {
        this.colorSwatchRefMobile.scrollTo(0, 0);
      }
      this.setState({
        isScrollEndReached: !isScrollEndReached,
      });
    }
  };

  getColors = () => {
    const { colorsMap, isPLPredesign } = this.props;
    const { maxVisibleItems } = this.state;
    const stepSize = maxVisibleItems;
    const { isArrEnd, firstItemIndex } = this.state;
    const { isMobile } = getViewportInfo() || {};
    if (isMobile) {
      return colorsMap;
    }
    if (isArrEnd && isPLPredesign) {
      return colorsMap.slice(0, stepSize);
    }
    if (firstItemIndex + stepSize < colorsMap.length) {
      return colorsMap.slice(firstItemIndex, firstItemIndex + stepSize);
    }
    const sliceIni = colorsMap.length - stepSize;
    return colorsMap.slice(sliceIni > 0 ? sliceIni : 0, colorsMap.length);
  };

  getColorSwatches = () => {
    const {
      onChipClick,
      selectedColorId,
      skuInfo,
      isFavoriteView,
      imagesByColor,
      relatedSwatchImages,
      itemBrand,
      pdpUrl,
      primaryBrand,
      alternateBrand,
    } = this.props;
    const { firstItemIndex } = this.state;
    const isTier1 = isTier1Device();
    let colorSwatchesComponent = '';
    if (isFavoriteView) {
      colorSwatchesComponent = (
        <ProductColorChip
          key={selectedColorId}
          colorEntry={skuInfo}
          isActive
          onChipClick={onChipClick}
          skuInfo={skuInfo}
          imagesByColor={imagesByColor}
          itemBrand={itemBrand}
          isFavoriteView
          isTier1={isTier1}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
        />
      );
    }
    colorSwatchesComponent = this.getColors().map((colorEntry, index) => {
      let {
        color: { swatchImage },
      } = colorEntry;
      const { imageName } = colorEntry;

      if ((index !== 0 || firstItemIndex !== 0) && relatedSwatchImages) {
        swatchImage = relatedSwatchImages.find(
          (swatchImageUrl) => swatchImageUrl === `${colorEntry.colorProductId}_swatch.jpg`
        );
      }

      return (
        <ProductColorChip
          key={colorEntry.colorProductId}
          swatchImage={swatchImage || imageName}
          colorEntry={colorEntry}
          isActive={selectedColorId === colorEntry.color.name}
          onChipClick={onChipClick}
          imagesByColor={imagesByColor}
          isTier1={isTier1}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
        />
      );
    });
    if (isTier1) {
      return colorSwatchesComponent;
    }
    return (
      <>
        <Anchor to={configureInternalNavigationFromCMSUrl(pdpUrl)} asPath={pdpUrl}>
          {colorSwatchesComponent}
        </Anchor>
      </>
    );
  };

  render() {
    const { colorsMap, showColorEvenOne, className, isFavoriteView, skuInfo } = this.props;
    const { maxVisibleItems } = this.state;
    const isDisplayNext = colorsMap.length > maxVisibleItems;

    if (!isFavoriteView && (showColorEvenOne ? colorsMap.length <= 0 : colorsMap.length <= 1)) {
      return null;
    }

    const arrowLeft = getIconPath('icon-carrot-black-small');
    const isMobileView = isClient() && getViewportInfo().isMobile;
    return (
      <div ref={this.captureContainerRef} className={className}>
        <div className="mobile-content-colors">
          <div
            className="color-swatches-mobile-view container-carousel"
            ref={(ref) => {
              this.colorSwatchRefMobile = ref;
            }}
          >
            {this.getColorSwatches()}
          </div>
        </div>
        <ol className="content-colors">
          <Swipeable
            {...this.swipeConfig}
            className="color-swatches-tab-view"
            onSwipedLeft={this.handleNextClick}
          >
            {this.getColorSwatches()}
          </Swipeable>
          <div className="color-swatches-desktop-view">{this.getColorSwatches()}</div>
        </ol>
        {isDisplayNext && (
          <BodyCopy
            component="div"
            title="Next"
            role="button"
            onClick={isMobileView ? this.handleNextEndClick : this.handleNextClick}
            className="arrowRightWrapper"
          >
            <img
              src={arrowLeft}
              data-locator="color_swatch_arrow"
              alt="right-arrow"
              className="arrowImg"
            />
          </BodyCopy>
        )}
        {skuInfo && <ProductSKUInfo {...skuInfo} />}
      </div>
    );
  }
}

export default withStyles(ProductColorChipWrapper, styles);
export { ProductColorChipWrapper as ProductColorChipWrapperVanilla };
