// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { BopisFilterEmptyResultsViewVanilla as BopisFilterEmptyResultsView } from '../BopisFilterEmptyResults.view';

const props = {
  className: 'test123slm',
  labels: {
    interimSoldoutMessage:
      'THESE STYLES ARE CURRENTLY UNAVILABLE AT YOUR LOCAL STORE.#Still interested? Check a different location or order online with free shipping everyday!',
  },
};

describe('Bopis Filter Empty Results View', () => {
  it('should render correctly', () => {
    const component = shallow(<BopisFilterEmptyResultsView {...props} />);
    expect(component).toMatchSnapshot();
  });
});
