// 9fbef606107a605d69c0edbcd8029e5d
const badgeTranformation = 'g_west,w_0.22,fl_relative';
const secureImageConfig = 'd_ecom:assets:products:gym';
const IMG_DATA_PLP = {
  imgConfig: ['t_plp_img_m,f_auto', 't_plp_img_t,f_auto', 't_plp_img_d,f_auto'],
  badgeConfig: [badgeTranformation, badgeTranformation, badgeTranformation],
};
const IMG_DATA_PLP_SECURE = {
  imgConfig: [secureImageConfig, secureImageConfig, secureImageConfig],
  badgeConfig: [badgeTranformation, badgeTranformation, badgeTranformation],
};

const IMG_DATA_PLP_CAT_FILTERS = {
  imgConfig: ['t_plp_img_cat_d', 't_plp_img_cat_d', 't_plp_img_cat_d'],
};

const IMG_DATA_CLP = {
  imgConfig: ['t_clp_img_m', 't_clp_img_t', 't_clp_img_d'],
};

const VID_DATA_PLP = {
  imgConfig: ['t_plp_vid_m', 't_plp_vid_t', 't_plp_vid_d'],
};

const VID_DATA_PLP_WEBP = {
  imgConfig: ['e_loop,t_plp_webp_m', 'e_loop,t_plp_webp_t', 'e_loop,t_plp_webp_d'],
};

const VID_DATA_PLP_WEBP_APP = 'e_loop,t_plp_webp_m,dpr_2.0';

const VID_DATA_PLP_GIF_APP = 't_plp_gif_app,dpr_2.0';

export {
  IMG_DATA_PLP,
  IMG_DATA_CLP,
  VID_DATA_PLP,
  VID_DATA_PLP_WEBP,
  VID_DATA_PLP_GIF_APP,
  VID_DATA_PLP_WEBP_APP,
  IMG_DATA_PLP_CAT_FILTERS,
  IMG_DATA_PLP_SECURE,
};
