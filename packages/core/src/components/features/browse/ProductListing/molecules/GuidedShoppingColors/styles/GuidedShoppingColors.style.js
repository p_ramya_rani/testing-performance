// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const whiteCrossIcon = getIconPath('white-cross-icon');

const styles = css`
  .guided-shopping-color {
    padding-top: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};

    @media ${props => props.theme.mediaQuery.large} {
      margin: 20px 40px 0 40px;
    }

    .button-row button {
      width: 148px;
      padding: ${props => props.theme.spacing.ELEM_SPACING.XS} 0;
      font-size: ${props => props.theme.typography.fontSizes.fs12};
      ${props => props.theme.mediaQuery.medium} {
        padding: ${props => props.theme.spacing.ELEM_SPACING.MED} 0;
        width: 188px;
        font-size: ${props => props.theme.typography.fontSizes.fs14};
      }
    }

    .tcp_carousel_wrapper {
      padding-bottom: 20px;

      .slick-prev {
        left: 0;
      }

      .slick-next {
        right: 0;
      }

      button.slick-arrow.slick-disabled {
        cursor: not-allowed;
        opacity: 0.2;
      }
    }

    .slick-slide > div {
      display: inline-block;
    }

    .CheckBox__input {
      display: none;
    }

    .labelStyle {
      margin-top: 0px;
    }

    .CheckBox__text {
      width: 56px;

      img {
        width: 56px;
        height: 56px;
        border-radius: 50%;
        position: relative;
        border: 1px solid transparent;
      }

      .img-wrapper {
        position: relative;
        width: 58px;
        display: block;
        padding: 5px;
      }
      .black-border {
        border: 1px solid ${props => props.theme.colors.BLACK};
      }
      .checked-img {
        padding: 5px;
      }
      .checked-img:after {
        content: '';
        position: absolute;
        background: url(${whiteCrossIcon}) no-repeat center;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        border-radius: 50%;
        border: 2px solid ${props => props.theme.colors.CHECKBOX.CHECKED_BORDER};
        padding: 3px;
      }

      .style-name {
        padding-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
        text-align: center;
        letter-spacing: -0.22px;
        color: ${props => props.theme.colors.BLACK};
      }
    }

    .back-button {
      margin-right: 14px;
    }

    .essential-style-img {
      display: none;
    }
  }
`;

export default styles;
