/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { getViewportInfo, isClient } from '@tcp/core/src/utils';
import DamImage from '../../../../../../common/atoms/DamImage';
import withStyles from '../../../../../../common/hoc/withStyles';
import ProductListingFiltersFormStyle from '../ProductListingFiltersForm.style';
import CustomSelect from '../../CustomSelect/views';

import { BodyCopy, Col, Row } from '../../../../../../common/atoms';
import cssClassName from '../../utils/cssClassName';
import ProductListingMobileFiltersForm from '../../ProductListingMobileFiltersForm';
import { getLocator } from '../../../../../../../utils';
import AppliedFiltersList from '../../AppliedFiltersList';
import { FACETS_FIELD_KEY } from '../../../../../../../services/abstractors/productListing/productListing.utils';
import SortSelector from '../../SortSelector';
import {
  DESCRIPTION_FILTER,
  FILTER_COLOR_SWATCH_URL,
} from '../../../container/ProductListing.constants';
import getSortOptions from '../../SortSelector/views/Sort.util';
import LoadedProductsCount from '../../LoadedProductsCount/views';

/**
 * @function getColorFilterOptionsMap This handles to render the desktop filter fields of color
 * @summary  This is to set the color filters
 * @param {Array} colorOptionsMap - list of color options
 * @param {String} filterName - filter names "color_group_uFilter etc"
 * @param {Boolean} isMobile - check for mobile view
 */
function getColorFilterOptionsMap(colorOptionsMap, filterName, isMobile) {
  return colorOptionsMap.map((color) => {
    const imgUrl = `${FILTER_COLOR_SWATCH_URL}${color.id}.gif`;
    const imgData = { alt: color.displayName, url: imgUrl };
    const imgConfig = `w_50,h_50,c_thumb,g_auto:0`;
    const imgDataConfig = [`${imgConfig}`, `${imgConfig}`, `${imgConfig}`];
    const whiteColorClass = color.displayName.toLowerCase() === 'white';

    return {
      value: color.id,
      title: color.displayName,
      content: (
        <div className="color-title" role="option" aria-selected="false">
          <DamImage
            className={`color-chip ${whiteColorClass ? 'white-color-class' : ''}`}
            imgData={imgData}
            imgConfigs={imgDataConfig}
            data-colorname={color.displayName.toLowerCase()}
          />
          {!isMobile && (
            <BodyCopy
              component="span"
              role="option"
              textAlign="center"
              tabIndex={-1}
              fontSize="fs14"
              fontFamily="secondary"
              color="gray.900"
              className="color-name"
              outline="none"
            >
              {color.displayName}
            </BodyCopy>
          )}
        </div>
      ),
    };
  });
}

/**
 * @function getFilterOptionsMap This handles to render the desktop filter fields of non-color
 * @summary  This is to set the non-color filters
 * @param {Array} colorOptionsMap - list of non-color options
 * @param {String} filterName - filter names "categoryPath2_uFilter, age_group_uFilter etc"
 * @param {Boolean} isMobile - check for mobile view
 */
function getFilterOptionsMap(optionsMap) {
  return optionsMap.map((option) => ({
    value: option.id,
    title: option.displayName,
    disabled: option.disabled,
    content: (
      <BodyCopy
        component="span"
        role="button"
        textAlign="center"
        tabIndex={-1}
        fontSize="fs14"
        fontFamily="secondary"
        color="gray.900"
        className="size-title"
        outline="none"
      >
        {option.displayName}
      </BodyCopy>
    ),
  }));
}

function getSortCustomOptionsMap(sortOptionsMap) {
  return sortOptionsMap.map((sortOption) => ({
    value: sortOption.id,
    title: (
      <BodyCopy
        component="span"
        className="sort-item-selected"
        fontSize="fs13"
        fontFamily="secondary"
        fontWeight="extrabold"
      >
        {sortOption.displayName}
      </BodyCopy>
    ),
    content: (
      <BodyCopy component="span" className="sort-title" fontSize="fs14" fontFamily="secondary">
        {sortOption.displayName}
      </BodyCopy>
    ),
  }));
}

class ProductListingFiltersForm extends React.Component {
  /**
   * @constructor for this class
   * @param {object} props props to the constructor
   */
  constructor(props) {
    super(props);
    this.filterRef = [];

    this.handleSubmitOnChange = this.handleSubmitOnChange.bind(this);
    this.handleRemoveFilter = this.handleRemoveFilter.bind(this);
    this.handleRemoveAllFilters = this.handleRemoveAllFilters.bind(this);
    this.handleImmediateSubmit = this.handleImmediateSubmit.bind(this);
  }

  /**
   * @function getAppliedFiltersCount This handles to get applied filter count.
   */
  getAppliedFiltersCount() {
    const { initialValues } = this.props;
    let count = 0;

    // returns count for the applied filters.
    // eslint-disable-next-line
    for (let key in initialValues) {
      count +=
        key.toLowerCase() !== FACETS_FIELD_KEY.sort && key !== 'bopisStoreId'
          ? initialValues[key].length
          : 0;
    }
    return count;
  }

  handleFilterFieldBlur = () => {
    const { reset } = this.props;
    setTimeout(() => reset());
  };

  captureFilterRef = (ref) => {
    if (!ref) return;
    const typeRef = ref.getRenderedComponent();
    typeRef.filterRefType = ref.props.name;
    this.filterRef.push(typeRef);
  };

  /**
   * @function handleSubmitOnChange This handles to submit remove filters call.
   */
  handleSubmitOnChange() {
    const { submitting, onSubmit, getProducts } = this.props;
    if (submitting) return;

    this.filterRef.forEach((filter) => {
      if (filter.filterRefType !== DESCRIPTION_FILTER) filter.closeMenu();
    });

    // Observe that since submission can occur by capturing the change events in the CustomSelects of the form
    // we need to wait for the next event loop for the value in the redux-store to reflect the ones in the fields
    setTimeout(() => {
      // DT-31958
      // Need to get form values from props / redux-store and compare to the previous values
      // eslint-disable-next-line react/prop-types
      const { formValues } = this.props;
      return onSubmit(formValues, false, getProducts);
    });
  }

  /**
   * @function handleRemoveFilter remove single filter
   * @param {String} fieldName - field name to be removed
   * @param {number} filterId - id to be removed
   */
  handleRemoveFilter(fieldName, filterId) {
    const { change, initialValues } = this.props;
    const changeParam = initialValues[fieldName]
      ? initialValues[fieldName].filter((entryId) => entryId !== filterId)
      : [];
    change(fieldName, changeParam);
    this.handleSubmitOnChange();
  }

  /**
   * @function handleRemoveAllFilters remove all filters
   * @param void
   */
  handleRemoveAllFilters() {
    const { change, initialValues, formValues } = this.props;
    // eslint-disable-next-line no-restricted-syntax
    for (const key in initialValues) {
      if (key !== 'sort') {
        change(key, []);
      }
    }

    // eslint-disable-next-line no-restricted-syntax
    for (const key in formValues) {
      if (key !== 'sort') {
        change(key, []);
      }
    }
    this.handleSubmitOnChange();
    return true;
  }

  /**
   * @function handleImmediateSubmit submit filter call
   * @param {Object} formValues - form value for filter to be applied
   */
  handleImmediateSubmit() {
    const { submitting, onSubmit, getProducts, formValues } = this.props;
    if (submitting) return;
    if (document.activeElement) {
      document.activeElement.blur();
    }
    this.filterRef.forEach((filter) => {
      if (filter.filterRefType !== DESCRIPTION_FILTER) filter.closeMenu();
    });

    // eslint-disable-next-line consistent-return
    return onSubmit(formValues, false, getProducts);
  }

  /**
   * @function renderFilterField
   * @summary This handles to render the color filter fields
   * @param {Object} appliedFilterVal - object of selected filters
   * @param {Object} selectedFilters - object of selected filters
   * @param {String} filterName - filter names "categoryPath2_uFilter, age_group_uFilter etc"
   * @param {String} facetName - filter names "category, color etc"
   */

  renderFilterField(appliedFilterVal, selectedFilters, filterName, facetName) {
    const {
      filtersMaps,
      labels,
      isFavoriteView,
      onFilterSelection,
      filteredId,
      isABTestForStickyFilter,
    } = this.props;
    return (
      <Field
        name={facetName}
        appliedFilterVal={!isFavoriteView && appliedFilterVal}
        facetName={facetName}
        component={CustomSelect}
        optionsMap={getFilterOptionsMap(filtersMaps[facetName])}
        title=""
        placeholder={filterName}
        allowMultipleSelections={!isFavoriteView}
        className="size-detail-chips"
        expanded={false}
        disableExpandStateChanges={false}
        ref={this.captureFilterRef}
        withRef
        forwardRef
        labels={labels}
        onChange={onFilterSelection}
        onBlur={this.handleFilterFieldBlur}
        isFavoriteView={isFavoriteView}
        filteredId={filteredId}
        isABTestForStickyFilter={isABTestForStickyFilter}
      />
    );
  }

  /**
   * @function renderColorFilterField This handles to render the color filter fields
   * @param {appliedFilterVal} appliedFilterVal
   * @param {selectedFilters} selectedFilters
   * @param {filterName} filterName - filter names "categoryPath2_uFilter, age_group_uFilter etc"
   * @param {facetName} facetName - filter names "category, color etc"
   */
  renderColorFilterField(appliedFilterVal, selectedFilters, filterName, facetName) {
    const isMobile = false;
    const { filtersMaps, labels, isFavoriteView, isABTestForStickyFilter } = this.props;

    const className = cssClassName(
      isMobile ? 'color-detail-chips' : 'color-filter-chip size-detail'
    );
    return (
      <Field
        name={facetName}
        appliedFilterVal={!isFavoriteView && appliedFilterVal}
        facetName={facetName}
        component={CustomSelect}
        optionsMap={getColorFilterOptionsMap(filtersMaps[facetName], filterName, isMobile)}
        title={isMobile ? filterName : ''}
        placeholder={filterName}
        allowMultipleSelections
        className={className}
        expanded={isMobile}
        disableExpandStateChanges={isMobile}
        ref={this.captureFilterRef}
        withRef
        forwardRef
        labels={labels}
        onBlur={this.handleFilterFieldBlur}
        isFavoriteView={isFavoriteView}
        isABTestForStickyFilter={isABTestForStickyFilter}
      />
    );
  }

  renderFilterByLabel = (labels, isFilterBy) => {
    return (
      isFilterBy && (
        <BodyCopy
          component="span"
          role="option"
          textAlign="center"
          tabIndex={0}
          fontSize="fs14"
          fontFamily="secondary"
          color="gray.900"
          outline="none"
          data-locator={getLocator('plp_filter_label_filterby')}
        >
          {`${labels.lbl_filter_by}`}
        </BodyCopy>
      )
    );
  };

  renderDisplayFilters(totalProductsCount, isFavoriteView) {
    return totalProductsCount > 1 || this.getAppliedFiltersCount() > 0 || isFavoriteView;
  }

  /**
   * @function renderDskFilterView renders the filter view for desktop
   * @param {Object} appliedFilters - filters if already applied
   */

  renderDskFilterView = (appliedFilters, isABTestForStickyFilter) => {
    const {
      filtersMaps,
      totalProductsCount,
      handleSubmit,
      colorSeqMap,
      labels,
      className,
      slpLabels,
      isFilterBy,
      onSortSelection,
      defaultPlaceholder,
      isFavoriteView,
      favoriteSortingParams,
      sortLabels,
      isBopisFilterOn,
    } = this.props;
    const filterKeys = Object.keys(filtersMaps);

    const sortOptions = favoriteSortingParams || getSortOptions(sortLabels);
    return (
      <form className="render-desktop-view" onSubmit={handleSubmit(this.handleImmediateSubmit)}>
        <Field component="input" name="bopisStoreId" value="" type="hidden" id="bopisStoreId" />
        {this.renderDisplayFilters(totalProductsCount, isFavoriteView) && (
          <div
            className={`${className} desktop-dropdown ${
              isABTestForStickyFilter ? 'desktop-dropdown-justify-content-end' : ''
            }`}
          >
            {!isABTestForStickyFilter && (
              <div className="filters-only-container">
                {this.renderFilterByLabel(labels, isFilterBy)}

                {filtersMaps && this.renderDesktopFilters(filterKeys, appliedFilters)}
              </div>
            )}

            <div className="sort-selector-wrapper">
              <SortSelector
                isMobile={false}
                defaultPlaceholder={defaultPlaceholder}
                sortSelectOptions={getSortCustomOptionsMap(sortOptions)}
                labels={labels}
                onChange={
                  !isFavoriteView
                    ? handleSubmit(this.handleSubmitOnChange)
                    : (selectedOption) => onSortSelection(selectedOption)
                }
                isFavoriteView={isFavoriteView}
              />
            </div>
          </div>
        )}
        <Row
          fullBleed
          className={`filtered-by-section ${
            isABTestForStickyFilter ? 'filtered-by-hide-utility' : ''
          }`}
        >
          <Col colSize={{ small: 0, medium: 0, large: 10 }}>
            {this.getAppliedFiltersCount() > 0 && (
              <AppliedFiltersList
                auxColorMap={colorSeqMap}
                onRemoveFilter={this.handleRemoveFilter}
                appliedFilters={appliedFilters}
                removeAllFilters={this.handleRemoveAllFilters}
                className={className}
                labels={labels}
                totalProductsCount={totalProductsCount}
                isABTestForStickyFilter={isABTestForStickyFilter}
                isBopisFilterOn={isBopisFilterOn}
              />
            )}
          </Col>
          {!isABTestForStickyFilter && (
            <Col colSize={{ small: 6, medium: 8, large: 2 }}>
              <LoadedProductsCount
                totalProductsCount={totalProductsCount}
                showingItemsLabel={slpLabels}
                isFavoriteView={isFavoriteView}
              />
            </Col>
          )}
        </Row>
      </form>
    );
  };

  /**
   * @function renderMobileFilterView renders the filter view for mobile
   */

  renderMobileFilterView = () => {
    const {
      filtersMaps,
      totalProductsCount,
      handleSubmit,
      labels,
      className,
      initialValues,
      onSubmit,
      sortLabels,
      favoriteSortingParams,
      onSortSelection,
      isFavoriteView,
      change,
      isLoadingMore,
      onFilterSelection,
      isProductListing,
      isSearchListing,
      isABTestForStickyFilter,
      isBopisFilterOn,
    } = this.props;
    return (
      <div className="render-mobile-view">
        {this.renderDisplayFilters(totalProductsCount, isFavoriteView) && (
          <ProductListingMobileFiltersForm
            totalProductsCount={totalProductsCount}
            initialValues={initialValues}
            filtersMaps={filtersMaps}
            className={className}
            labels={labels}
            onSubmit={onSubmit}
            handleSubmit={handleSubmit}
            handleImmediateSubmit={this.handleImmediateSubmit}
            removeAllFilters={this.handleRemoveAllFilters}
            handleSubmitOnChange={this.handleSubmitOnChange}
            sortLabels={sortLabels}
            isFavoriteView={isFavoriteView}
            favoriteSortingParams={favoriteSortingParams}
            onSortSelection={onSortSelection}
            onChange={change}
            isLoadingMore={isLoadingMore}
            onFilterSelection={onFilterSelection}
            isProductListing={isProductListing}
            isSearchListing={isSearchListing}
            isABTestForStickyFilter={isABTestForStickyFilter}
            isBopisFilterOn={isBopisFilterOn}
          />
        )}
      </div>
    );
  };

  /**
   * @function renderFilters renders the filter view for desktop
   * or mobile depending on the client.
   * @param {Object} appliedFilters - filters if already applied
   */
  renderFilters(appliedFilters) {
    const { isABTestForStickyFilter } = this.props;
    const isDesktopFlag = isClient() && getViewportInfo().isDesktop;
    return (
      <div className="filter-and-sort-form-container">
        {!isDesktopFlag
          ? this.renderMobileFilterView()
          : this.renderDskFilterView(appliedFilters, isABTestForStickyFilter)}
        {isABTestForStickyFilter && isDesktopFlag ? this.renderMobileFilterView() : null}
      </div>
    );
  }

  /**
   * @function renderDesktopFilters
   * @summary This handles to render the desktop filter fields
   * @param none
   */
  renderDesktopFilters(filterKeys, appliedFilters) {
    const { filtersMaps, filtersLength, isFavoriteView } = this.props;
    const unbxdKeyMapping = filtersMaps.unbxdDisplayName;
    const appliedFilterAvailable = this.getAppliedFiltersCount();
    let filterList = {};
    return filterKeys.map((key) => {
      if (
        key.toLowerCase() !== FACETS_FIELD_KEY.unbxdDisplayName &&
        key.toLowerCase() !== FACETS_FIELD_KEY.l1category &&
        filtersMaps[key]
      ) {
        if (key.toLowerCase() === FACETS_FIELD_KEY.color) {
          filterList =
            filtersMaps[key].length > 0 &&
            this.renderColorFilterField(
              appliedFilterAvailable && appliedFilters[key],
              filtersLength[`${key}Filters`],
              unbxdKeyMapping[key],
              key
            );
        } else {
          filterList =
            filtersMaps[key].length > 0 &&
            this.renderFilterField(
              appliedFilterAvailable && appliedFilters[key],
              !isFavoriteView ? filtersLength[`${key}Filters`] : 'Display',
              unbxdKeyMapping[key],
              key
            );
        }
        return filterList;
      }
      return false;
    });
  }

  render() {
    const { initialValues, filtersMaps } = this.props;

    const appliedFilters = [];
    const keys = Object.keys(initialValues).sort();

    // eslint-disable-next-line
    for (let index = 0; index < keys.length; index++) {
      const selectedFacet = filtersMaps[keys[index]]
        ? initialValues[keys[index]].map((filterId) =>
            filtersMaps[keys[index]].find((filter) => filterId === filter.id)
          )
        : [];
      appliedFilters.push(selectedFacet);
    }

    return <Fragment>{this.renderFilters(appliedFilters)}</Fragment>;
  }
}

ProductListingFiltersForm.propTypes = {
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  className: PropTypes.string,
  filtersMaps: PropTypes.shape({}),
  totalProductsCount: PropTypes.string,
  initialValues: PropTypes.shape({}),
  filtersLength: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number])),
  handleSubmit: PropTypes.func.isRequired,
  colorSeqMap: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  submitting: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  getProducts: PropTypes.func.isRequired,
  change: PropTypes.func,
  sortLabels: PropTypes.arrayOf(PropTypes.shape({})),
  slpLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  isFilterBy: PropTypes.bool,
  isFavoriteView: PropTypes.bool,
  onFilterSelection: PropTypes.func,
  favoriteSortingParams: PropTypes.shape({}),
  onSortSelection: PropTypes.func,
  defaultPlaceholder: PropTypes.string,
  formValues: PropTypes.shape({}),
  isLoadingMore: PropTypes.bool,
  reset: PropTypes.func,
  filteredId: PropTypes.string,
  isProductListing: PropTypes.bool,
  isSearchListing: PropTypes.bool,
  isABTestForStickyFilter: PropTypes.bool,
  isBopisFilterOn: PropTypes.bool,
};

ProductListingFiltersForm.defaultProps = {
  filtersMaps: {},
  labels: {},
  className: '',
  totalProductsCount: '0',
  initialValues: {},
  filtersLength: {},
  colorSeqMap: {},
  submitting: false,
  change: () => null,
  sortLabels: [],
  slpLabels: {},
  isFilterBy: true,
  isFavoriteView: false,
  onFilterSelection: () => null,
  defaultPlaceholder: '',
  onSortSelection: () => null,
  favoriteSortingParams: null,
  formValues: {},
  isLoadingMore: false,
  reset: () => {},
  filteredId: '',
  isProductListing: false,
  isSearchListing: false,
  isABTestForStickyFilter: false,
  isBopisFilterOn: false,
};
export default reduxForm({
  form: 'filter-form', // a unique identifier for this form
  enableReinitialize: true,
})(withStyles(ProductListingFiltersForm, ProductListingFiltersFormStyle));

export { ProductListingFiltersForm as ProductListingFiltersFormVanilla };
