// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { WebView } from 'react-native-webview';
import { getAPIConfig } from '@tcp/core/src/utils/index.native';
import { Platform } from 'react-native';

export default class Spotlights extends React.PureComponent {
  constructor(props) {
    super(props);
    this.apiConfig = getAPIConfig();
    this.state = {
      webViewHeight: 0,
    };
  }

  getSpotlightFormattedUrl = (categoryId) => {
    return `${this.apiConfig.BV_WEB_VIEW_URL}?bvaction=bv_spotlight&categoryId=${categoryId}&env=${this.apiConfig.BV_SPOTLIGHT_ENVIRONMENT}&instance=${this.apiConfig.BV_SPOTLIGHT_INSTANCE}`;
  };

  handleWebViewEvents = (event) => {
    let data = get(event, 'nativeEvent.data', null);
    if (data) {
      data = JSON.parse(data);
      if (typeof data === 'object') {
        const { webViewHeight } = data;
        if (webViewHeight) {
          this.setState({ webViewHeight });
        }
      }
    }
  };

  render() {
    const { categoryId } = this.props;
    const { webViewHeight } = this.state;
    const bvSpotLightUrl = this.getSpotlightFormattedUrl(categoryId);
    const webViewStyle = { backgroundColor: 'transparent', height: webViewHeight };
    return (
      <WebView
        androidHardwareAccelerationDisabled={true}
        originWhitelist={['*']}
        source={{
          uri: bvSpotLightUrl,
        }}
        mixedContentMode="always"
        useWebKit={Platform.OS === 'ios'}
        scrollEnabled={false}
        domStorageEnabled
        thirdPartyCookiesEnabled
        startInLoadingState
        allowUniversalAccessFromFileURLs
        javaScriptEnabled
        onMessage={this.handleWebViewEvents}
        automaticallyAdjustContentInsets
        style={webViewStyle}
      />
    );
  }
}

Spotlights.propTypes = {
  categoryId: PropTypes.string.isRequired,
};
