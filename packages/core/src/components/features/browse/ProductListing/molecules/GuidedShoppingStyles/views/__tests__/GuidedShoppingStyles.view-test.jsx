// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { GuidedShoppingStylesVanilla } from '../GuidedShoppingStyles.view';

describe('GuidedShoppingStyles View', () => {
  const imageSchemaDesktop = 'c_scale,h_71,q_100,w_75';
  const imageSchemaMobile = 'c_scale,h_59,q_100,w_63';
  const props = {
    stepInfo: {
      header: {
        title: 'Pick your styles!',
        style: 'style1',
      },
      type: 'carousal',
      facetName: 'categoryPath2_uFilter',
      button: {
        text: 'NEXT >',
        title: 'Next',
        action: 'next',
      },
      options: [
        {
          text: 'All',
          type: 'all',
          style: 'select-all',
          defaultImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_68_3x.png',
              alt: 'all',
              title: 'all',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
          selectedImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_90_3x.png',
              alt: 'all',
              title: 'all',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
        },
        {
          text: 'Outfits',
          type: 'outfits',
          style: 'red-300',
          defaultImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_54_3x.png',
              alt: 'outfits',
              title: 'outfits',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
          selectedImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_64_3x.png',
              alt: 'outfits',
              title: 'outfits',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
        },
        {
          text: 'Multi-Packs',
          type: 'multi-packs',
          style: 'blue-600',
          defaultImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_53_3x.png',
              alt: '',
              title: '',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
          selectedImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_63_3x.png',
              alt: '',
              title: '',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
        },
        {
          text: 'Tees',
          type: 'tees',
          style: 'orange-900',
          defaultImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_52_3x.png',
              alt: 'tees',
              title: 'tees',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
          selectedImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_62_3x.png',
              alt: 'tees',
              title: 'tees',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
        },
        {
          text: 'Accessories',
          type: 'accessories',
          style: 'red-500',
          defaultImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_48_3x.png',
              alt: 'accessories',
              title: 'accessories',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
          selectedImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_57_3x.png',
              alt: 'accessories',
              title: 'accessories',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
        },
        {
          text: 'Baby One-Piece',
          type: 'baby one-piece',
          style: 'red-500',
          defaultImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_47_3x.png',
              alt: '',
              title: '',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
          selectedImage: {
            typ: 'media',
            sub: 'image',
            val: {
              url:
                'https://test1.theplace.com/image/upload/v1583271351/ecom/assets/content/tcp/us/essentials/Mask_Group_56_3x.png',
              alt: '',
              title: '',
              crop_d: imageSchemaDesktop,
              crop_t: imageSchemaDesktop,
              crop_m: imageSchemaMobile,
            },
          },
        },
      ],
    },
    categoryList: [
      {
        displayName: 'Bottoms',
        id: 'Bottoms',
        facetName: 'categoryPath2_uFilter',
      },
      {
        displayName: 'School Uniforms',
        id: 'School Uniforms',
        facetName: 'categoryPath2_uFilter',
      },
      {
        displayName: 'Skorts & Shorts',
        id: 'Skorts & Shorts',
        facetName: 'categoryPath2_uFilter',
      },
      {
        displayName: 'Uniform Extended Sizes',
        id: 'Uniform Extended Sizes',
        facetName: 'categoryPath2_uFilter',
      },
      {
        displayName: 'Jeggings & Chinos',
        id: 'Jeggings & Chinos',
        facetName: 'categoryPath2_uFilter',
      },
      {
        displayName: 'Shorts',
        id: 'Shorts',
        facetName: 'categoryPath2_uFilter',
      },
      {
        displayName: 'Activewear',
        id: 'Activewear',
        facetName: 'categoryPath2_uFilter',
      },
    ],
    className: 'sc-jRuhRL fyqWjg',
  };

  it('should render correctly', () => {
    const tree = shallow(<GuidedShoppingStylesVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
