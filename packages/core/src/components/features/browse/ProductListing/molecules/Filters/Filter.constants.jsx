// 9fbef606107a605d69c0edbcd8029e5d 
const FILTER_CONFIG = {
  facetType: {
    sort: 'radioType',
    color: 'colorType',
    checkBox: 'checkBoxType',
    size: 'sizeType',
  },
  facetId: {
    sort: '1', // sudo id for ordering to 1st position in Object.Entries in Filter.native constructor
    color: 'TCPColor_uFilter',
    groupedSize: 'v_tcp_groupedsize_uFilter',
    size: 'v_tcpsize_uFilter',
  },
};

export default FILTER_CONFIG;
