// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';
import LineComp from '@tcp/core/src/components/common/atoms/Line';
import { StyleSheet } from 'react-native';
import {
  colorWhite,
  colorBlack,
  colorGainsboro,
} from '../../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.styles';

const PageContainer = styled.View`
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const Container = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const TitleText = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.typography.fontSizes.fs16};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  line-height: 19.2;
  font-weight: ${(props) => props.theme.typography.fontWeights.black};
`;

const getFlatListContainerStyle = () => ({
  marginTop: 8,
});

const ItemSeparatorStyle = styled.View`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const ApplyAndClearButtonContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  flex-direction: row;
  justify-content: space-between;
`;

// new styled for AB test modal

const LineWrapper = styled(LineComp)`
  width: 80%;
`;

const StickyContainer = styled.View`
  position: absolute;
  bottom: 0;
  width: 100%;
`;

// filter styles
const InputCheckboxWrapper = styled.View`
  margin-bottom: ${(props) => (props.isFromFilterPills ? '20px' : '27px')};
  margin-left: ${(props) => (props.isFromFilterPills ? '12px' : '0px')};
`;

const ShowResultButtonContainer = styled.View`
  width: 100%;
  margin-bottom: ${(props) => (props.hasNotch ? 30 : 0)};
`;

export const FilterPillsStyle = StyleSheet.create({
  touchableOpacity5: {
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: colorWhite,
    borderRadius: 16,
    elevation: 22,
    height: 46,
    justifyContent: 'center',
    marginLeft: 18,
    marginRight: 19,
    marginTop: 5,
    shadowColor: colorBlack,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 15,
    width: 99,
  },
  view2: {
    backgroundColor: colorWhite,
    borderColor: colorGainsboro,
    bottom: 0,
    justifyContent: 'center',
    padding: 16,
    position: 'absolute',
    width: '100%',
    zIndex: 10,
  },
});

const AtbButton = styled.View`
  align-self: center;
  flex: 1;
  justify-content: center;
  margin-left: 8px;
`;

const BottomnView = styled.View`
  margin-bottom: 18px;
`;

const ClearBtnWrapper = styled.TouchableOpacity``;

const styles = css``;

export {
  styles,
  PageContainer,
  Container,
  getFlatListContainerStyle,
  ItemSeparatorStyle,
  TitleText,
  ApplyAndClearButtonContainer,
  LineWrapper,
  ShowResultButtonContainer,
  InputCheckboxWrapper,
  StickyContainer,
  AtbButton,
  BottomnView,
  ClearBtnWrapper,
};
