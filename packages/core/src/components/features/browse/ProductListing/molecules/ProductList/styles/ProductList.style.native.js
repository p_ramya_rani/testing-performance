// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';

const getAdditionalStylesPageContainer = (props) => {
  const { isNewReDesignProductTile, width, isSearchListing } = props;
  let marginLeft = '-5px';
  let marginBottom = '0px';
  if (isSearchListing) {
    marginLeft = '-11px';
    marginBottom = '160px';
  }
  if (isNewReDesignProductTile) {
    return `
      padding: 0px 2px;
      width: ${width};
      margin-bottom: ${marginBottom};
      margin-left: ${marginLeft}`;
  }
  return `
    width: 100%;
  `;
};

const PageContainer = styled.View`
  margin-top: ${(props) =>
    props.isNewReDesignProductTile ? '0px' : props.theme.spacing.ELEM_SPACING.MED};
  ${getAdditionalStylesPageContainer}
`;

const RecommendationContainer = styled.View`
  width: 48%;
`;

const HeaderContainer = styled.View``;

const GridPromoContainer = styled.View`
  display: flex;
  width: ${(props) => (props.fullWidth ? '100%' : '48%')};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const InGridRecContainer = styled.View``;

const InMultiGridRecContainer = styled.View`
  width: 100%;
  overflow: hidden;
  margin: 4px 0 4px 0;
`;

const BlankProductForMultiGrid = styled.View`
  width: 0;
`;
const styles = css``;

export {
  styles,
  PageContainer,
  HeaderContainer,
  GridPromoContainer,
  RecommendationContainer,
  InGridRecContainer,
  InMultiGridRecContainer,
  BlankProductForMultiGrid,
};
