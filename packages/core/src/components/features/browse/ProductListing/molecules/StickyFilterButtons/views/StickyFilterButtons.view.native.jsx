import React, { useRef } from 'react';
import { FlatList, Image } from 'react-native';
import { withTheme } from 'styled-components';
import { BodyCopy } from '../../../../../../common/atoms';
import withStylesNative from '../../../../../../common/hoc/withStyles.native';
import styles, {
  RowViewContainer,
  ViewContainer,
  MainContainer,
} from '../StickyFilterButtons.style.native';

function StickyFilterButtons({ stickyFilters, onStickyButtonPress, showResetOption }) {
  const flatListRef = useRef();
  const renderStickyFilterImages = (source, showRight) => {
    const size = showRight ? 14 : 18;
    const imageStyle = {
      width: size,
      height: size,
      alignSelf: 'center',
    };
    return <Image source={source} style={imageStyle} resizeMode="contain" />;
  };

  const listKeyFunction = (_, index) => index.toString();

  const renderStickyFilterItemData = (item, index) => {
    let textColor = 'gray.900';
    let icon = item?.image;
    const showRightImage = index === 0 || index === 1;
    const showLeftImage = index === 6;
    const isSelected = item?.isSelected;
    const disabled = item?.isDisabled;
    if (isSelected) {
      icon = item?.selectedImage;
      textColor = 'white';
    }
    const fontWeight = showLeftImage || isSelected ? 'extrabold' : 'semibold';
    const color = showLeftImage ? 'blue.800' : textColor;

    return {
      isSelected,
      fontWeight,
      color,
      icon,
      showLeftImage,
      showRightImage,
      disabled,
    };
  };

  const renderStickyFilterItem = ({ item, index }) => {
    const { fontWeight, color, icon, showLeftImage, showRightImage, isSelected, disabled } =
      renderStickyFilterItemData(item, index);
    if (!showResetOption && index === 6) {
      return null;
    }
    return (
      <MainContainer
        onPress={() => onStickyButtonPress(index, flatListRef)}
        showLeftImage={showLeftImage}
        isSelected={isSelected}
        disabled={disabled}
      >
        <RowViewContainer>
          {showLeftImage && renderStickyFilterImages(icon, showRightImage)}
          <ViewContainer showLeftImage showRightImage>
            <BodyCopy
              fontWeight={fontWeight}
              color={color}
              fontFamily="secondary"
              fontSize="fs12"
              text={item?.name}
            />
          </ViewContainer>
          {showRightImage && renderStickyFilterImages(icon)}
        </RowViewContainer>
      </MainContainer>
    );
  };

  return (
    <FlatList
      ref={flatListRef}
      testID="StickyFilterButtons"
      accessibilityLabel="StickyFilterButtons"
      data={stickyFilters}
      horizontal
      showsHorizontalScrollIndicator={false}
      listKey={(_, index) => listKeyFunction(_, index)}
      renderItem={renderStickyFilterItem}
    />
  );
}

export default withStylesNative(withTheme(StickyFilterButtons), styles);
export { StickyFilterButtons as StickyFilterButtonsVanilla };
