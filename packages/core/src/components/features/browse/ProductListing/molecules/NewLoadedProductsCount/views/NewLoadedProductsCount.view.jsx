// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { PropTypes } from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import style from '../styles/NewLoadedProductsCount.style';

const NewLoadedProductsCount = props => {
  const { className, totalProductsCount, showingItemsLabel, breadCrumbs } = props;
  const categories = Array.isArray(breadCrumbs) ? breadCrumbs.splice(1) : null;
  const categoryName =
    categories && categories.length ? categories.map(cat => cat.displayName).join(': ') : null;
  if (!categoryName || !totalProductsCount) return null;
  const { lbl_item: ItemLabel, lbl_items: ItemsLabel } = showingItemsLabel;
  return (
    <div className={className}>
      <BodyCopy
        className="new-product-category-text"
        component="span"
        fontSize="fs22"
        fontFamily="secondary"
        fontWeight="extrabold"
      >
        {categoryName}
      </BodyCopy>
      <BodyCopy
        className="new-product-count-text"
        component="span"
        fontSize="fs16"
        fontFamily="secondary"
        fontWeight="medium"
      >
        {`(${totalProductsCount} ${totalProductsCount > 1 ? ItemsLabel : ItemLabel})`}
      </BodyCopy>
    </div>
  );
};

NewLoadedProductsCount.propTypes = {
  className: PropTypes.string,
  totalProductsCount: PropTypes.number,
  showingItemsLabel: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  breadCrumbs: PropTypes.shape([]).isRequired,
};

NewLoadedProductsCount.defaultProps = {
  className: '',
  totalProductsCount: 0,
  showingItemsLabel: {},
};

export default withStyles(NewLoadedProductsCount, style);
export { NewLoadedProductsCount as NewLoadedProductsCountVanilla };
