const closeIcon = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/filter-Icons/clear/clear.png');
const filterIconWhite = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/filter-Icons/filter_white.png');
const filterIcon = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/filter-Icons/filter.png');
const sortIcon = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/filter-Icons/sort.png');
const sortIconWhite = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/filter-Icons/sort_white.png');

const filterArr = (filter, sort, labelsFilter) => {
  return [
    {
      name: filter,
      imageRightShow: true,
      image: filterIcon,
      selectedImage: filterIconWhite,
      isSelected: false,
      isDisabled: false,
    },
    {
      name: sort,
      imageRightShow: true,
      image: sortIcon,
      selectedImage: sortIconWhite,
      isSelected: false,
      isDisabled: false,
    },
    {
      name: labelsFilter?.lbl_gender,
      isSelected: false,
      isDisabled: false,
    },
    {
      name: labelsFilter?.lbl_color,
      isSelected: false,
      isDisabled: false,
    },
    {
      name: labelsFilter?.lbl_size,
      isSelected: false,
      isDisabled: false,
    },
    {
      name: labelsFilter?.lbl_price,
      isSelected: false,
      isDisabled: false,
    },
    {
      name: labelsFilter?.lbl_reset,
      image: closeIcon,
      imageLeftShow: true,
      isSelected: false,
      isDisabled: false,
    },
  ];
};

const checkFilterItemSelected = (updatedFilter) => {
  return (
    updatedFilter &&
    updatedFilter.length > 0 &&
    updatedFilter.filter((data) => {
      return data.isSelected;
    }).length > 0
  );
};

const setSelectionFilterData = (filterValue, index, filterData) => {
  const data = filterData;
  if (filterValue && filterValue.length === 0) {
    data[index].isSelected = false;
    data[index].isDisabled = true;
  } else {
    data[index].isSelected = checkFilterItemSelected(filterValue);
    data[index].isDisabled = false;
  }
  return JSON.parse(JSON.stringify(data));
};

const getFilterTotalSeletectedCount = (updatedFilterData) => {
  const updatedFilterDataValue = updatedFilterData;
  let count = 0;
  Object.keys(updatedFilterData).map((filter) => {
    if (filter && filter !== '1') {
      count =
        updatedFilterDataValue[filter] &&
        Array.isArray(updatedFilterDataValue[filter]) &&
        updatedFilterDataValue[filter].length > 0 &&
        checkFilterItemSelected(updatedFilterDataValue[filter])
          ? count + 1
          : count;
    }
    return count;
  });
  return count > 0 && count;
};

const validateFiterValues = (
  unbxdDisplayName,
  updatedStickyFilters,
  language,
  updatedFilterData
) => {
  let filtersData = JSON.parse(JSON.stringify(updatedStickyFilters));
  Object.keys(updatedFilterData).map((filtersList) => {
    switch (filtersList) {
      case '1':
        filtersData[1].name = unbxdDisplayName['1'];
        filtersData[1].isSelected = language && language !== '';
        break;
      case 'gender_uFilter':
        filtersData[2].name = unbxdDisplayName.gender_uFilter;
        filtersData = setSelectionFilterData(updatedFilterData.gender_uFilter, 2, filtersData);
        break;

      case 'TCPColor_uFilter':
        filtersData[3].name = unbxdDisplayName.TCPColor_uFilter;
        filtersData = setSelectionFilterData(updatedFilterData.TCPColor_uFilter, 3, filtersData);
        break;

      case 'v_tcpsize_uFilter':
        filtersData[4].name = unbxdDisplayName.v_tcpsize_uFilter;
        filtersData = setSelectionFilterData(updatedFilterData.v_tcpsize_uFilter, 4, filtersData);
        break;

      case 'unbxd_price_range_uFilter':
        filtersData[5].name = unbxdDisplayName.unbxd_price_range_uFilter;
        filtersData = setSelectionFilterData(
          updatedFilterData.unbxd_price_range_uFilter,
          5,
          filtersData
        );
        break;

      default:
        break;
    }
    return null;
  });
  return JSON.parse(JSON.stringify(filtersData));
};

const sortfilterTypeSeparator = '|';
const multiSortFilterSeparator = ',';

const generateSortFilterKeys = (ParamObj, isFromTopPill) => {
  let sortFilterKey = '';
  Object.keys(ParamObj).forEach((key) => {
    if (ParamObj[key] && ParamObj[key].length) {
      if (sortFilterKey) sortFilterKey += sortfilterTypeSeparator;
      sortFilterKey += `${key}=${ParamObj[key].join(multiSortFilterSeparator)}`;
    }
  });
  return isFromTopPill && sortFilterKey !== '' ? `top-${sortFilterKey}` : sortFilterKey;
};

const getFilterParams = (filterValues, isFromTopPill) => {
  const {
    categoryPath2_uFilter: category = [],
    v_tcpsize_uFilter: size = [],
    TCPColor_uFilter: color = [],
    v_tcpfit_unbxd_uFilter: fit = [],
    age_group_uFilter: age = [],
    gender_uFilter: gender = [],
    unbxd_price_range_uFilter: price = [],
  } = filterValues;
  return generateSortFilterKeys({ category, color, size, fit, age, gender, price }, isFromTopPill);
};

const sortMapper = {
  'min_offer_price desc': 'price: high to low',
  'min_offer_price asc': 'price: low to high',
  'newest_score desc': 'new arrivals',
  'favoritedcount desc': 'most favorited',
  'TCPBazaarVoiceRating desc': 'top rated',
};

const getSortParams = (sortValues, isFromTopPill) => {
  const key = isFromTopPill ? 'sort-top=' : 'sort=';

  return sortValues ? `${key}${sortMapper[sortValues]}` : '';
};

const triggerAnalytics = (filterData, sortValue, isFromTopPill) => {
  const filterValues = getFilterParams(filterData, isFromTopPill);
  const sortValues = getSortParams(sortValue, isFromTopPill);
  const name = 'Sort_filter_clicks_e161';
  const module = 'browse';

  const contextData = {
    adobe: '',
    google: {},
  };

  if (filterValues && filterValues.length > 0) {
    contextData.google.Refinement_Used_v50 = filterValues;
  }
  if (sortValues && sortValues.length > 0) {
    contextData.google.Sort_Options_v51 = sortValues;
  }

  return { name, module, contextData };
};

export { filterArr, validateFiterValues, getFilterTotalSeletectedCount, triggerAnalytics };
