// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { Platform, Image, SafeAreaView, ScrollView } from 'react-native';
import get from 'lodash/get';
import {
  getBrand,
  getAPIConfig,
  getScreenHeight,
  getPixelRatio,
} from '@tcp/core/src/utils/index.native';
import { WebView } from 'react-native-webview';
import { trackForterAction, ForterActionType } from '@tcp/core/src/utils/forter.util';

import { BodyCopy, Anchor } from '../../../../../../common/atoms';
import {
  AccordionHeader,
  ImageStyleWrapper,
  ProductRatingsContainer,
  SubmissionFormWrapper,
} from '../styles/ProductReviews.style.native';
import ModalNative from '../../../../../../common/molecules/Modal/index';
import OpenLoginModal from '../../../../../account/LoginPage/container/LoginModal.container';
import AccessibilityRoles from '../../../../../../../constants/AccessibilityRoles.constant';
import { appendToken } from '../../../../../../../utils/utils';

const downIcon = require('../../../../../../../../../mobileapp/src/assets/images/carrot-small-down.png');
const upIcon = require('../../../../../../../../../mobileapp/src/assets/images/carrot-small-up.png');

class ProductReviews extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isAccordionOpen: props.expanded,
      showLoginModal: false,
      openSubmissionForm: false,
      webViewHeight: 300,
    };
    this.handleWebViewEvents = this.handleWebViewEvents.bind(this);
    this.apiConfig = getAPIConfig();
    this.brand = getBrand();
  }

  componentDidUpdate(prevProps) {
    const { expanded: isRatingExpanded } = this.props;
    const { isAccordionOpen: isExpandedState } = this.state;
    if (prevProps.expanded !== isRatingExpanded && isRatingExpanded !== isExpandedState) {
      /* eslint-disable react/no-did-update-set-state */
      this.setState({
        isAccordionOpen: isRatingExpanded,
      });
    }
  }

  handleAccordionToggle = () => {
    const { isAccordionOpen } = this.state;
    this.setState({ isAccordionOpen: !isAccordionOpen });
  };

  getWriteReviewFormattedUrl = (ratingsProductId) => {
    return `${this.apiConfig.BV_WEB_VIEW_URL}?bvproductId=${ratingsProductId}&env=${
      this.apiConfig.BV_ENVIRONMENT
    }&instance=${this.apiConfig.BV_INSTANCE}${appendToken() ? '&token=tcprwd' : ''}`;
  };

  getSubmissionFormUrl = (userId, mprId, productId) => {
    const { getSecurityToken } = this.props;
    const securityToken = getSecurityToken(userId, mprId);

    return this.apiConfig.BV_SUBMISSION_URL.replace('#INSTANCE#', this.apiConfig.BV_INSTANCE)
      .replace('#PRODUCTID#', productId)
      .replace('#TOKEN#', securityToken)
      .replace('#DEPLOYMENT_ZONE#', this.apiConfig.BV_INSTANCE)
      .replace('#ENV#', this.apiConfig.BV_ENVIRONMENT)
      .concat(`${appendToken() ? '&token=tcprwd' : ''}`);
  };

  handleWebViewEvents = (event) => {
    let data = get(event, 'nativeEvent.data', null);
    if (data === 'writeReview') {
      this.handleWriteReviewClick();
    } else if (data === 'closeRating') {
      this.handleAccordionToggle();
    } else if (data) {
      data = JSON.parse(data);
      if (typeof data === 'object') {
        const { webViewHeight } = data;
        if (webViewHeight) {
          this.setState({ webViewHeight });
        }
      }
    }
  };

  handleWriteReviewClick = () => {
    this.toggleSubmissionForm();
  };

  toggleLoginModal = () => {
    this.setState((state) => ({
      showLoginModal: !state.showLoginModal,
    }));
  };

  toggleSubmissionForm = () => {
    const { openSubmissionForm: isOpen } = this.state;
    const { isNewReDesignEnabled, closeRatingDrawer } = this.props;
    if (!isOpen) {
      trackForterAction(ForterActionType.RATE, 'ProductReview');
    }
    if (isNewReDesignEnabled) {
      if (isOpen) closeRatingDrawer();
      this.setState((state) => ({
        openSubmissionForm: !state.openSubmissionForm,
      }));
    } else {
      this.setState((state) => ({
        openSubmissionForm: !state.openSubmissionForm,
        isAccordionOpen: false,
      }));
    }
  };

  renderComponent = ({ isGuest, showLoginModal }) => {
    let componentContainer = null;
    if (isGuest) {
      componentContainer = (
        <OpenLoginModal
          component="login"
          openState={showLoginModal}
          setLoginModalMountState={this.toggleLoginModal}
          isUserLoggedIn={!isGuest}
          handleAfterLogin={this.toggleLoginModal}
        />
      );
    }
    return <React.Fragment>{componentContainer}</React.Fragment>;
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      ratingsAndReviewsLabel,
      ratingsProductId,
      isGuest,
      userId,
      mprId,
      reviewsCount,
      isNewReDesignEnabled,
    } = this.props;
    const { isAccordionOpen, margins, showLoginModal, openSubmissionForm, webViewHeight } =
      this.state;

    const bvReviewHtmlUrl = isAccordionOpen && this.getWriteReviewFormattedUrl(ratingsProductId);

    const bvSubmissionFormHtmlUrl =
      openSubmissionForm && this.getSubmissionFormUrl(userId, mprId, ratingsProductId);

    const webViewStyle = { backgroundColor: 'transparent', height: webViewHeight };

    return (
      <ProductRatingsContainer margins={margins}>
        {isNewReDesignEnabled ? (
          <ScrollView>
            <WebView
              originWhitelist={['*']}
              source={{
                uri: bvReviewHtmlUrl,
              }}
              mixedContentMode="always"
              useWebKit={Platform.OS === 'ios'}
              scrollEnabled={false}
              domStorageEnabled
              thirdPartyCookiesEnabled
              startInLoadingState
              allowUniversalAccessFromFileURLs
              javaScriptEnabled
              onMessage={this.handleWebViewEvents}
              automaticallyAdjustContentInsets={false}
              style={webViewStyle}
            />
          </ScrollView>
        ) : (
          <AccordionHeader
            onPress={this.handleAccordionToggle}
            accessible
            accessibilityRole={AccessibilityRoles.Button}
            accessibilityLabel={`${ratingsAndReviewsLabel.lbl_ratings_and_reviews} (${reviewsCount})`}
            accessibilityState={{ expanded: isAccordionOpen }}
          >
            <BodyCopy
              fontFamily="secondary"
              fontWeight="black"
              fontSize="fs14"
              isAccordionOpen={isAccordionOpen}
              text={`${ratingsAndReviewsLabel.lbl_ratings_and_reviews} (${reviewsCount})`}
              textAlign="center"
            />

            <ImageStyleWrapper>
              <Anchor onPress={this.handleAccordionToggle}>
                <Image source={isAccordionOpen ? upIcon : downIcon} />
              </Anchor>
            </ImageStyleWrapper>
          </AccordionHeader>
        )}
        {isAccordionOpen && !isNewReDesignEnabled && (
          <WebView
            androidHardwareAccelerationDisabled={true}
            originWhitelist={['*']}
            source={{
              uri: bvReviewHtmlUrl,
            }}
            mixedContentMode="always"
            useWebKit={Platform.OS === 'ios'}
            scrollEnabled={false}
            domStorageEnabled
            thirdPartyCookiesEnabled
            startInLoadingState
            allowUniversalAccessFromFileURLs
            javaScriptEnabled
            onMessage={this.handleWebViewEvents}
            automaticallyAdjustContentInsets={false}
            style={webViewStyle}
          />
        )}
        {showLoginModal && this.renderComponent({ showLoginModal, isGuest })}
        {openSubmissionForm ? (
          <ModalNative
            isOpen={openSubmissionForm}
            onRequestClose={this.toggleSubmissionForm}
            heading={ratingsAndReviewsLabel.lbl_rating_form_title}
            headingFontFamily="secondary"
            fontSize="fs16"
          >
            <SafeAreaView>
              <SubmissionFormWrapper
                height={
                  getPixelRatio() === 'xhdpi' || getPixelRatio() === 'mdpi'
                    ? getScreenHeight()
                    : getScreenHeight() - 150
                }
              >
                <WebView
                  androidHardwareAccelerationDisabled={true}
                  originWhitelist={['*']}
                  source={{
                    uri: bvSubmissionFormHtmlUrl,
                  }}
                  mixedContentMode="always"
                  useWebKit={Platform.OS === 'ios'}
                  scrollEnabled
                  domStorageEnabled
                  thirdPartyCookiesEnabled
                  startInLoadingState
                  allowUniversalAccessFromFileURLs
                  javaScriptEnabled
                  onMessage={this.handleWebViewEvents}
                  automaticallyAdjustContentInsets={false}
                />
              </SubmissionFormWrapper>
            </SafeAreaView>
          </ModalNative>
        ) : null}
      </ProductRatingsContainer>
    );
  }
}

ProductReviews.propTypes = {
  ratingsProductId: PropTypes.string.isRequired,
  isGuest: PropTypes.bool.isRequired,
  expanded: PropTypes.bool,
  userId: PropTypes.string,
  mprId: PropTypes.string,
  reviewsCount: PropTypes.number,
  ratingsAndReviewsLabel: PropTypes.shape({}).isRequired,
  getSecurityToken: PropTypes.func.isRequired,
  isNewReDesignEnabled: PropTypes.bool,
  closeRatingDrawer: PropTypes.func,
};

ProductReviews.defaultProps = {
  userId: '',
  mprId: '',
  reviewsCount: 0,
  expanded: false,
  isNewReDesignEnabled: false,
  closeRatingDrawer: () => {},
};

export default ProductReviews;
export { ProductReviews as ProductReviewsVanilla };
