// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .fixed-bread-crumb-height {
    min-height: 68px;
  }
  .interim-sold-out {
    background-color: ${(props) =>
      props.theme.isGymboree
        ? props.theme.colorPalette.orange[900]
        : props.theme.colorPalette.blue[1000]};
    height: auto;
  }
  .category-name {
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy13}px;
    text-align: center;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    color: ${(props) => props.theme.colors.WHITE};
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    letter-spacing: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
  }
  .department-msg {
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy9}px;
    text-align: center;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    color: ${(props) => props.theme.colors.WHITE};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    line-height: 1.23;
    letter-spacing: ${(props) => props.theme.fonts.letterSpacing.normal};
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .department-url {
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy9}px;
    text-align: center;
    text-decoration: underline;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    color: ${(props) => props.theme.colors.WHITE};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    line-height: 1.23;
    letter-spacing: ${(props) => props.theme.fonts.letterSpacing.normal};
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
  }
  .bread-crumb {
    padding-top: 20px;
    padding-bottom: 17px;
    margin: 5px 0 9px;
  }
  .bread-crumb,
  .product-list {
    display: flex;
  }
  .container-bopis-filter {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  }
  .sidebar {
    display: none;
  }
`;
