// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel';
import { getIconPath } from '@tcp/core/src/utils';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import { Col, Button, BodyCopy, Anchor, Image } from '@tcp/core/src/components/common/atoms';
import styles from '../styles/GuidedShoppingColors.style';
import {
  assetRouteBasePath,
  CAROUSEL_OPTIONS,
  WHITE,
} from '../../../organisms/EssentialShop/container/EssentialShop.constants';

class GuidedShoppingColors extends PureComponent {
  handleSubmitForm = (e) => {
    e.preventDefault();
  };

  multicheckbox = ({ input, options, childComponent: CHILDCOMPONENT }) => {
    const { filterFormValues, clearColorHandler, stepInfo } = this.props;
    const { facetName = '' } = stepInfo;
    const { onChange, value: values = [] } = input;
    const IconPath = getIconPath('carousel-big-carrot');
    const imgHostDomain = assetRouteBasePath.getOriginImgHostSetting();
    const isCompleteHTTPUrl = (url) => /^(http|https):\/\//.test(url);
    return (
      <>
        <Carousel
          name="carousel"
          options={CAROUSEL_OPTIONS}
          carouselConfig={{
            autoplay: false,
            customArrowLeft: IconPath,
            customArrowRight: IconPath,
            variation: 'big-arrows',
          }}
          carouselTheme="dark"
        >
          {options &&
            options.map((option, index) => {
              const { id, displayName } = option;
              const handleChange = (e, value) => {
                const arr = [...values];
                const { checked } = e.target;
                if (checked && value) {
                  arr.push(value);
                } else {
                  arr.splice(arr.indexOf(value), 1);
                }
                return onChange(arr);
              };
              const isChecked = values.includes(id);
              const imageUrl = isCompleteHTTPUrl(option.imagePath)
                ? option.imagePath
                : `${imgHostDomain}${option.imagePath}`;
              return (
                <CHILDCOMPONENT
                  className="colorSelector"
                  checked={isChecked}
                  input={{ onChange: (e) => handleChange(e, id), name: `styleCheckbox_${index}` }}
                >
                  <BodyCopy className={`img-wrapper ${isChecked ? 'checked-img' : ''}`}>
                    <Image
                      url={imageUrl}
                      alt={displayName}
                      className={displayName === WHITE && !isChecked ? 'black-border' : null}
                    />
                  </BodyCopy>

                  <BodyCopy fontSize="fs14" fontFamily="secondary" className="style-name">
                    {displayName}
                  </BodyCopy>
                </CHILDCOMPONENT>
              );
            })}
        </Carousel>
        <BodyCopy
          component="div"
          className="elem-mt-SM elem-mb-SM"
          textAlign="center"
          fontSize="fs14"
          fontFamily="secondary"
        >
          {filterFormValues[facetName] && !!filterFormValues[facetName].length && (
            <Anchor
              onClick={(e) => {
                e.preventDefault();
                onChange([]);
                clearColorHandler(facetName);
              }}
              noLink
            >
              Clear All Colors
            </Anchor>
          )}
        </BodyCopy>
      </>
    );
  };

  render() {
    const { stepInfo, className, setActiveStep, categoryList, labels, activeDotCount } = this.props;
    const { facetName = '' } = stepInfo;
    return (
      <div className={className}>
        <div className="guided-shopping-color">
          <Field
            id="essential_color"
            name={facetName}
            type="checkbox"
            options={categoryList}
            component={this.multicheckbox}
            childComponent={InputCheckbox}
          />

          <Col colSize={{ small: 6, medium: 8, large: 12 }} className="button-row">
            {activeDotCount ? (
              <Button
                fill="WHITE"
                type="button"
                buttonVariation="variable-width"
                className="back-button"
                onClick={() => setActiveStep('dec')}
              >
                {labels.back}
              </Button>
            ) : null}
            <Button fill="BLUE" type="submit" buttonVariation="variable-width">
              {labels.colorNext}
            </Button>
          </Col>
        </div>
      </div>
    );
  }
}

GuidedShoppingColors.propTypes = {
  categoryList: PropTypes.shape([]),
  setActiveStep: PropTypes.func.isRequired,
  className: PropTypes.string,
  filterFormValues: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  clearColorHandler: PropTypes.func.isRequired,
  activeDotCount: PropTypes.number,
  stepInfo: PropTypes.shape({}),
};

GuidedShoppingColors.defaultProps = {
  categoryList: [],
  className: '',
  filterFormValues: {},
  activeDotCount: 0,
  stepInfo: {},
};

export default withStyles(GuidedShoppingColors, styles);
export { GuidedShoppingColors as GuidedShoppingColorsVanilla };
