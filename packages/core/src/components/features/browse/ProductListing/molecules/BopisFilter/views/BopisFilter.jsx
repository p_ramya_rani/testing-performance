/* eslint-disable sonarjs/cognitive-complexity */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import { getLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { BodyCopy, ToggleSwitch } from '../../../../../../common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/BopisFilter.style';
import StoreLocatorModal from '../../../../../../common/organisms/StoreLocatorModal';

const BopisFilter = ({
  isBopisFilterOn,
  className,
  onStoreChangeLinkClick,
  onSubmit,
  formValues = {},
  getProducts,
  updateFormValue,
  setBopisFilterStateActn,
  labels,
  defaultStoreData,
  resetSuggestedStores,
}) => {
  const [storeData, setStoreData] = useState({});
  const [modalStoreSearchData, setModalStoreSearchData] = useState(null);
  const [bopisStoreSelectorModalOpen, setBopisStoreSelectorModalOpen] = useState(false);
  if (defaultStoreData && !isEqual(storeData, defaultStoreData)) {
    setStoreData(defaultStoreData);
  }
  useEffect(() => {
    if (!defaultStoreData) {
      setStoreData(defaultStoreData);
    }
  }, [defaultStoreData]);
  const handleChangeStoreOnKeyPress = (e) => {
    if (e.keyCode !== 32) return;
    e.preventDefault();
    onStoreChangeLinkClick();
  };

  const handleChangeStoreLinkClick = () => {
    if (onStoreChangeLinkClick) {
      onStoreChangeLinkClick();
    }
    resetSuggestedStores([]);
    setBopisStoreSelectorModalOpen(true);
    setModalStoreSearchData(storeData);
  };

  const handleBopisStoreModalClose = () => {
    setBopisStoreSelectorModalOpen(false);
    setModalStoreSearchData(null);
  };

  const isStoreAvailable = storeData?.basicInfo?.id;
  let storeChangeText = labels?.plpSelectStore;
  let storeSelectionMessage = labels?.plpSelectNearbyStore;
  if (isStoreAvailable) {
    storeChangeText = labels?.plpChangeStore;
    storeSelectionMessage = labels?.pickupAt;
  }

  const toggleBopisFilter = () => {
    const storeFilterId = [`v_Quantity_${storeData?.basicInfo?.id?.slice(-4)}:[1 TO *]`];
    let updatedFormValues = {};
    if (isBopisFilterOn) {
      updateFormValue('bopisStoreId', []);
      updatedFormValues = { ...formValues, bopisStoreId: [] };
    } else {
      updateFormValue('bopisStoreId', storeFilterId);
      updatedFormValues = { ...formValues, bopisStoreId: storeFilterId };
    }
    setBopisFilterStateActn(!isBopisFilterOn);
    onSubmit(updatedFormValues, false, getProducts);
  };

  const onStoreChange = (store) => {
    const storeFilterId = [`v_Quantity_${store?.basicInfo?.id?.slice(-4)}:[1 TO *]`];
    let updatedFormValues = {};
    if (isBopisFilterOn) {
      updateFormValue('bopisStoreId', storeFilterId);
      updatedFormValues = { ...formValues, bopisStoreId: storeFilterId };
      onSubmit(updatedFormValues, false, getProducts);
    }
  };

  const isStorePresentForToggling = () => {
    const store = JSON.parse(getLocalStorage('defaultStore'));
    if (store) {
      toggleBopisFilter();
    }
  };

  const renderStoreChangeLink = () => {
    return (
      <div className="store-change-wrapper">
        <div
          className={`change-store-link ${
            storeChangeText === labels?.plpSelectStore && 'select-store'
          }`}
          role="button"
          tabIndex="0"
          onKeyDown={handleChangeStoreOnKeyPress}
          onClick={handleChangeStoreLinkClick}
          underline
          noLink={false}
        >
          {storeChangeText}
        </div>
      </div>
    );
  };

  const renderSwitchFilter = () => {
    return (
      <div className="switch-filter">
        <ToggleSwitch
          name="bopis-filter"
          id="bopis-switch"
          checked={isBopisFilterOn}
          onChange={isStorePresentForToggling}
          disabled={!isStoreAvailable && !isEqual(isStoreAvailable, undefined)}
        />
      </div>
    );
  };

  const renderStoreSelection = () => {
    return (
      <span>
        {`${storeSelectionMessage} `}
        <BodyCopy
          fontWeight="extrabold"
          fontFamily="secondary"
          component="span"
          className="bopis-storename"
          textAlign="center"
        >
          {storeData?.basicInfo?.storeName}
        </BodyCopy>
      </span>
    );
  };
  return (
    <div className={`${className} bopis-filter-wrapper`}>
      {renderSwitchFilter()}
      <div className="store-info">
        {renderStoreSelection()}
        {renderStoreChangeLink()}
      </div>
      {}
      <StoreLocatorModal
        isOpen={bopisStoreSelectorModalOpen}
        onClose={handleBopisStoreModalClose}
        onStoreChange={onStoreChange}
        modalStoreSearchData={modalStoreSearchData}
      />
    </div>
  );
};
BopisFilter.defaultProps = {
  onStoreChangeLinkClick: () => {},
  onSubmit: () => {},
  formValues: {},
  getProducts: () => {},
  updateFormValue: () => {},
  setBopisFilterStateActn: () => {},
  labels: {
    plpSelectStore: '',
    pickupAt: '',
    plpChangeStore: '',
    plpSelectNearbyStore: '',
  },
  defaultStoreData: {},
};
BopisFilter.propTypes = {
  isBopisFilterOn: PropTypes.bool.isRequired,
  className: PropTypes.string.isRequired,
  onStoreChangeLinkClick: PropTypes.func,

  onSubmit: PropTypes.func,
  formValues: PropTypes.shape({}),
  labels: PropTypes.shape({
    plpSelectStore: PropTypes.string,
    pickupAt: PropTypes.string,
    plpChangeStore: PropTypes.string,
    plpSelectNearbyStore: PropTypes.string,
  }),
  getProducts: PropTypes.func,
  updateFormValue: PropTypes.func,
  setBopisFilterStateActn: PropTypes.func,
  resetSuggestedStores: PropTypes.func.isRequired,
  defaultStoreData: PropTypes.shape({}),
};
export default withStyles(BopisFilter, styles);
