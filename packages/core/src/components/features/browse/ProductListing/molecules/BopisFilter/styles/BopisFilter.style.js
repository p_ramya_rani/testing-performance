import { css } from 'styled-components';

export default css`
  &.bopis-filter-wrapper {
    height: 46px;
    width: auto;
    box-sizing: border-box;
    padding: 12px 16px;
    border-radius: 6px;
    background-color: #edf5fb;
    display: inline-flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    font-size: 16px;

    .store-info {
      display: inline-flex;
      flex-direction: row;
      font-family: Nunito;
    }
    .change-store-link {
      font-size: 14px;
      display: inline-block;
      cursor: pointer;
      text-decoration: underline;
      margin-top: 2px;
    }
    .bopis-storename {
      text-transform: capitalize;
    }
    .switch-filter {
      margin-right: 10px;
    }
    .store-change-wrapper {
      margin-left: 10px;
      display: flex;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      .change-store-link.select-store {
        font-size: 14px;
        margin-top: 2px;
      }
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      font-size: 16px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
      height: 56px;
      font-size: 13px;
      margin-bottom: 16px;
      .store-info {
        flex-direction: column;
        font-family: Nunito;
      }
      .store-change-wrapper {
        margin-left: 0px;
        text-transform: capitalize;
        font-weight: bold;
      }
      .change-store-link {
        font-size: 12px;
      }
    }
  }
  .header-pickup-line {
    text-transform: capitalize;
  }
`;
