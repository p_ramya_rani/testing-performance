// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import withHotfix from '@tcp/core/src/components/common/hoc/withHotfix';
import { checkItemIsProduct } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import { Heading } from '@tcp/core/styles/themes/TCP/typotheme';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import withStyles from '../../../../../../common/hoc/withStyles';
import ProductListStyle from '../../ProductList.style';
import { isMobileApp, parseBoolean } from '../../../../../../../utils';
import ProductsGridItemBase from './ProductsGridItem';
import GridPromo from '../../../../../../common/molecules/GridPromo';

/**
 * Hotfix-Aware Component. The use `withHotfix` below is just for
 * making the cart page hotfix-aware.
 */
const ProductsGridItem = withHotfix(ProductsGridItemBase);

const isGridItem = (item) => {
  let flag = true;
  if (
    typeof item === 'string' ||
    (item &&
      // eslint-disable-next-line
      item.hasOwnProperty('type') &&
      (item.type === 'marketing' || item.type === 'marketing_contained'))
  ) {
    flag = false;
  }
  return flag;
};

const getGridIndex = (item, gridIndex) => {
  let index = gridIndex;
  if (typeof item === 'string') {
    index = 0;
  } else if (isGridItem(item)) {
    index += 1;
  }
  return index;
};

const getItemWidgetID = (currentBlockIndex, item, index) => {
  let widgetId;
  if (index <= 3 && currentBlockIndex === 0) {
    widgetId = `widget_PLP_Scrape_Content_${index + 1}`;
  }
  return widgetId;
};

class ProductList extends React.Component {
  shouldComponentUpdate(nextProps) {
    if (isEqual(nextProps, this.props)) {
      return false;
    }
    return true;
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  render() {
    const {
      className,
      productsBlock,
      showQuickViewForProductId,
      currency,
      onAddItemToFavorites,
      onQuickViewOpenClick,
      onPickUpOpenClick,
      onColorChange,
      isBopisEnabled,
      unbxdId,
      onProductCardHover,
      isBopisEnabledForClearance,
      onQuickBopisOpenClick,
      currencyAttributes,
      siblingProperties,
      loadedProductCount,
      labels,
      labelsPlpTiles,
      isPlcc,
      productTileVariation,
      isLoggedIn,
      wishlistsSummaries,
      isFavoriteView,
      removeFavItem,
      createNewWishListMoveItem,
      outOfStockLabels,
      isKeepAliveEnabled,
      isSearchListing,
      getProducts,
      asPathVal,
      AddToFavoriteErrorMsg,
      openAddNewList,
      activeWishListId,
      onSeeSuggestedItems,
      onCloseSuggestedModal,
      seeSuggestedDictionary,
      addToBagEcom,
      isSearchPage,
      errorMessages,
      favoriteErrorMessages,
      isOnModelAbTestPlp,
      isInternationalShipping,
      page,
      wishList,
      cookieToken,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      isHidePLPAddToBag,
      suggestedProductErrors,
      isHidePLPRatings,
      hasSuggestedProduct,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      products,
      currentBlockIndex,
      isPromoApplied,
      categoryHeadingChanged,
      breadCrumbs,
      isSecureImageFlagEnabled,
      alternateBrand,
      isShowBrandNameEnabled,
      isBopisFilterOn,
    } = this.props;
    let gridIndex = 0;
    const productTileClass = isSearchListing
      ? ' product-tile search-product-tile'
      : ' product-tile';
    const getPriceRange = isShowPriceRangeKillSwitch
      ? parseBoolean(isShowPriceRangeKillSwitch)
      : false;
    const getABTestPriceRange = showPriceRangeForABTest;
    const showPriceRange = getPriceRange && getABTestPriceRange;
    return (
      <Fragment>
        {
          // eslint-disable-next-line complexity
          productsBlock.map((item, index) => {
            const isEvenElement = gridIndex % 2;
            const itemIndex = item.productInfo && `${item.productInfo.generalProductId}_${index}`;
            const itemGenProductID = item.productInfo ? item.productInfo.generalProductId : '';
            const ifItemIsProduct = checkItemIsProduct(item);
            const itemWidgetID = getItemWidgetID(currentBlockIndex, item, index);
            const productsGridItemHolder = ifItemIsProduct ? (
              <div className={`${className} product-tile ${productTileVariation}`} key={itemIndex}>
                <ProductsGridItem
                  position={index}
                  isMobile={isMobileApp()}
                  loadedProductCount={loadedProductCount}
                  item={item}
                  isGridView
                  isShowQuickView={showQuickViewForProductId === itemGenProductID}
                  currencySymbol={currency}
                  currencyAttributes={currencyAttributes}
                  onAddItemToFavorites={onAddItemToFavorites}
                  onQuickViewOpenClick={onQuickViewOpenClick}
                  onPickUpOpenClick={onPickUpOpenClick}
                  onColorChange={onColorChange}
                  isBopisEnabled={isBopisEnabled}
                  sqnNmbr={index + 1}
                  unbxdId={unbxdId}
                  onProductCardHover={onProductCardHover}
                  isBopisEnabledForClearance={isBopisEnabledForClearance}
                  isCanada={false}
                  isPlcc={isPlcc}
                  isPLPShowPickupCTA={false}
                  isOnModelAbTestPlp={isOnModelAbTestPlp}
                  isBossEnabled
                  isBossClearanceProductEnabled
                  isInternationalShipping={isInternationalShipping}
                  isShowVideoOnPlp={false}
                  onQuickBopisOpenClick={onQuickBopisOpenClick}
                  isProductsGridCTAView
                  isMatchingFamily // TODO: Need to add kill switch for this
                  siblingProperties={siblingProperties}
                  isEvenElement={isEvenElement}
                  gridIndex={gridIndex}
                  isPLPredesign // isPLPredesign should always be true, because this code is taken from existing project(MRT) and this filed has many condition to run the new code correctly and this and if we remove this line we need to change the many existing files.
                  isKeepAliveEnabled={isKeepAliveEnabled}
                  labels={labels}
                  labelsPlpTiles={labelsPlpTiles}
                  isLoggedIn={isLoggedIn}
                  wishlistsSummaries={wishlistsSummaries}
                  isFavoriteView={isFavoriteView}
                  removeFavItem={removeFavItem}
                  createNewWishListMoveItem={createNewWishListMoveItem}
                  outOfStockLabels={outOfStockLabels}
                  isSearchListing={isSearchListing}
                  getProducts={getProducts}
                  asPathVal={asPathVal}
                  AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                  openAddNewList={openAddNewList}
                  activeWishListId={activeWishListId}
                  onSeeSuggestedItems={onSeeSuggestedItems}
                  onCloseSuggestedModal={onCloseSuggestedModal}
                  seeSuggestedDictionary={seeSuggestedDictionary}
                  addToBagEcom={addToBagEcom}
                  isSearchPage={isSearchPage}
                  page={page}
                  errorMessages={errorMessages}
                  favoriteErrorMessages={favoriteErrorMessages}
                  cookieToken={cookieToken}
                  isHidePLPAddToBag={isHidePLPAddToBag}
                  showPriceRange={showPriceRange}
                  isHidePLPRatings={isHidePLPRatings}
                  suggestedProductErrors={suggestedProductErrors}
                  hasSuggestedProduct={hasSuggestedProduct}
                  isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                  lowQualityEnabled
                  onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                  wishList={wishList}
                  deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                  isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                  products={products}
                  widgetScrapeContentId={itemWidgetID}
                  breadCrumbs={breadCrumbs}
                  isSecureImageFlagEnabled={isSecureImageFlagEnabled}
                  alternateBrand={alternateBrand}
                  isShowBrandNameEnabled={isShowBrandNameEnabled}
                  isBopisFilterOn={isBopisFilterOn}
                />
              </div>
            ) : null;
            gridIndex = getGridIndex(item, gridIndex);
            if (isPromoApplied) {
              if (item && item.itemType === 'gridPromo') {
                return (
                  <div
                    className={
                      item.gridStyle === 'horizontal'
                        ? `${className} horizontal-promo`
                        : `${className} vertical-promo ${productTileClass}`
                    }
                  >
                    <GridPromo promoObj={item.itemVal} variation={item.gridStyle} />
                  </div>
                );
              }

              if (item && item.itemType === 'inMultiGridPlpRecommendation') {
                const portalValue =
                  item.totalSlots === 1
                    ? Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS
                    : Constants.RECOMMENDATIONS_MBOXNAMES.IN_MULTI_GRID_PLP_PRODUCTS;
                return (
                  <div
                    className={`${className} multi-grid-recommendation ${productTileVariation} multi-grid-view-${item.totalSlots}`}
                    key={itemIndex}
                  >
                    <Recommendations
                      portalValue={portalValue}
                      variations="moduleP"
                      page={Constants.RECOMMENDATIONS_PAGES_MAPPING.PLP}
                      slots={item.inGridSlots}
                      slotsList={item.inGridSlotsFlat}
                      index={item.slotIndex}
                      inGridPlpRecommendation
                    />
                  </div>
                );
              }
              return typeof item === 'string' ? (
                <Heading
                  key={item}
                  className={`${className} item-title`}
                  fontFamily="secondaryFontFamily"
                >
                  {item}
                  <hr className="horizontal-bar" />
                </Heading>
              ) : (
                <>{productsGridItemHolder}</>
              );
            }
            return (currentBlockIndex === 0 &&
              item.miscInfo &&
              item.miscInfo.categoryName &&
              gridIndex === 1) ||
              (categoryHeadingChanged &&
                item.miscInfo &&
                item.miscInfo.categoryName &&
                gridIndex === 1) ? (
              // eslint-disable-next-line react/jsx-indent
              <>
                <Heading
                  key={item.miscInfo.categoryName}
                  className={`${className} item-title`}
                  fontFamily="secondaryFontFamily"
                >
                  {item.miscInfo.categoryName}
                  <hr className="horizontal-bar" />
                </Heading>
                {productsGridItemHolder}
              </>
            ) : (
              <>{productsGridItemHolder}</>
            );
          })
        }
      </Fragment>
    );
  }
}

ProductList.propTypes = {
  className: PropTypes.string,
  productsBlock: PropTypes.arrayOf(PropTypes.shape({})),
  /** the generalProductId of the product (if any) requesting quickView to show */
  showQuickViewForProductId: PropTypes.string,
  /** Price related currency symbol to be rendered */
  currency: PropTypes.string,
  currencyAttributes: PropTypes.shape({}).isRequired,
  /** callback for clicks on wishlist CTAs. Accepts: colorProductId. */
  onAddItemToFavorites: PropTypes.func,
  /** callback for clicks on quickView CTAs. Accepts a generalProductId, colorProductId */
  onQuickViewOpenClick: PropTypes.func,
  /** callback to trigger when the user chooses to display a different color (used to retrieve prices) */
  onColorChange: PropTypes.func,
  /** When flase, flags that BOPIS is globaly disabled */
  isBopisEnabled: PropTypes.bool,
  /* This unbxd request ID will be passed to UNXD product click anlytics as request ID */
  unbxdId: PropTypes.string,
  onProductCardHover: PropTypes.func,
  onPickUpOpenClick: PropTypes.func,
  isBopisEnabledForClearance: PropTypes.bool,
  onQuickBopisOpenClick: PropTypes.func,
  siblingProperties: PropTypes.shape({
    colorMap: PropTypes.arrayOf(PropTypes.shape({})),
    promotionalMessage: PropTypes.string,
    promotionalPLCCMessage: PropTypes.string,
  }),
  loadedProductCount: PropTypes.number.isRequired,
  labels: PropTypes.shape().isRequired,
  isPlcc: PropTypes.bool,
  productTileVariation: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  wishlistsSummaries: PropTypes.shape(),
  isFavoriteView: PropTypes.bool,
  removeFavItem: PropTypes.func,
  createNewWishListMoveItem: PropTypes.func,
  outOfStockLabels: PropTypes.shape({}),
  isKeepAliveEnabled: PropTypes.bool,
  isSearchListing: PropTypes.bool,
  plpGridPromos: PropTypes.shape({}),
  plpHorizontalPromos: PropTypes.shape({}),
  getProducts: PropTypes.func,
  asPathVal: PropTypes.string,
  AddToFavoriteErrorMsg: PropTypes.string,
  openAddNewList: PropTypes.func,
  activeWishListId: PropTypes.number,
  labelsPlpTiles: PropTypes.shape({}),
  onSeeSuggestedItems: PropTypes.func,
  onCloseSuggestedModal: PropTypes.func,
  seeSuggestedDictionary: PropTypes.shape({}),
  addToBagEcom: PropTypes.func,
  isSearchPage: PropTypes.string,
  errorMessages: PropTypes.shape({}),
  favoriteErrorMessages: PropTypes.shape({}),
  isOnModelAbTestPlp: PropTypes.bool,
  isInternationalShipping: PropTypes.bool.isRequired,
  page: PropTypes.string,
  wishList: PropTypes.shape({}).isRequired,
  cookieToken: PropTypes.string,
  isShowPriceRangeKillSwitch: PropTypes.bool,
  showPriceRangeForABTest: PropTypes.bool,
  isHidePLPAddToBag: PropTypes.bool,
  isHidePLPRatings: PropTypes.bool,
  suggestedProductErrors: PropTypes.shape({}).isRequired,
  hasSuggestedProduct: PropTypes.shape({}).isRequired,
  isBrierleyPromoEnabled: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  products: PropTypes.arrayOf(PropTypes.shape({})),
  currentBlockIndex: PropTypes.number,
  isPromoApplied: PropTypes.bool,
  categoryHeadingChanged: PropTypes.bool,
  breadCrumbs: PropTypes.shape({}),
  isShowBrandNameEnabled: PropTypes.bool,
};

ProductList.defaultProps = {
  className: '',
  productsBlock: [],
  showQuickViewForProductId: '',
  currency: '',
  onAddItemToFavorites: () => {},
  onQuickViewOpenClick: () => {},
  onPickUpOpenClick: () => {},
  onColorChange: () => {},
  isBopisEnabled: false,
  unbxdId: 'fc0d2287-4a11-4739-98b4-1e2fd91016c4',
  onProductCardHover: () => {},
  isBopisEnabledForClearance: false,
  onQuickBopisOpenClick: () => {},
  siblingProperties: {
    colorMap: [],
    promotionalMessage: '',
    promotionalPLCCMessage: '',
  },
  isPlcc: false,
  productTileVariation: '',
  isLoggedIn: false,
  wishlistsSummaries: null,
  isFavoriteView: false,
  removeFavItem: null,
  createNewWishListMoveItem: null,
  outOfStockLabels: {},
  isKeepAliveEnabled: false,
  isSearchListing: false,
  plpGridPromos: {},
  plpHorizontalPromos: {},
  getProducts: () => {},
  asPathVal: '',
  AddToFavoriteErrorMsg: '',
  openAddNewList: () => {},
  activeWishListId: '',
  labelsPlpTiles: {},
  onSeeSuggestedItems: () => {},
  onCloseSuggestedModal: () => {},
  seeSuggestedDictionary: {},
  addToBagEcom: () => {},
  isSearchPage: 'false',
  errorMessages: {},
  favoriteErrorMessages: {},
  cookieToken: null,
  isOnModelAbTestPlp: false,
  page: '',
  isHidePLPAddToBag: false,
  isHidePLPRatings: false,
  isShowPriceRangeKillSwitch: false,
  showPriceRangeForABTest: false,
  isBrierleyPromoEnabled: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isDynamicBadgeEnabled: false,
  products: [],
  currentBlockIndex: 0,
  isPromoApplied: false,
  categoryHeadingChanged: false,
  breadCrumbs: [],
  isShowBrandNameEnabled: false,
};

export default withStyles(ProductList, ProductListStyle);
export { ProductList as ProductListVanilla };
