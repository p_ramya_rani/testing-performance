// 9fbef606107a605d69c0edbcd8029e5d
import { Dimensions } from 'react-native';
import styled, { css } from 'styled-components/native';

const getAdditionalStyle = (props) => {
  const { margins, paddings } = props;
  return {
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
  };
};

const getMinimumHeight = (props) => {
  const {
    renderPriceAndBagOnly,
    isRecommendations,
    outOfStock,
    isCardTypeTiles,
    isNavL2,
    isBundleProduct,
  } = props;

  if (isBundleProduct) {
    return `min-height: 320px;`;
  }
  if (!renderPriceAndBagOnly) {
    return `min-height: 380px;`;
  }
  if (outOfStock || isCardTypeTiles || isNavL2) {
    return `min-height: 250px;`;
  }
  if (isRecommendations) {
    return `min-height: 310px;`;
  }
  return ``;
};

const getAdditionalRedesignedStyles = (props) => {
  const { isNewReDesignProductTile, theme } = props;
  if (isNewReDesignProductTile) {
    return `
        shadow-color: ${theme.colors.BLACK};
        shadow-opacity: 0.2
        shadow-radius: 4.65;
        shadow-offset: 0px 3px;
        elevation: 10;
        border-radius: 16px;
        margin: 8px 4px
        width: 47%
    `;
  }
  return ``;
};

const getAdditionalFavoriteStyles = (props) => {
  const { isNewReDesignProductTile } = props;
  if (isNewReDesignProductTile) {
    return `
  position: absolute;
  right: -4;
  top: -3
  `;
  }
  return ``;
};

const getAdditionalBadge1TextStyles = (props) => {
  const { isNewReDesignProductTile, theme } = props;
  if (isNewReDesignProductTile) {
    return `
    font-family: ${theme.typography.fonts.secondary}
    color: ${theme.colors.PRIMARY.DARK}
    font-size: ${theme.typography.fontSizes.fs10}
    font-weight: ${theme.typography.fontWeights.extrabold};
    letter-spacing: .5
    `;
  }
  return ``;
};

const RowContainer = styled.View`
  flex-direction: row;
  align-items: center;
  ${getAdditionalStyle}
`;

const ListContainer = styled.View`
  width: ${(props) => (props.isCardTypeTiles || props.isNavL2 ? '146px' : `${props.width}%`)};
  ${getMinimumHeight};
  background: ${(props) => props.theme.colors.WHITE};
  ${getAdditionalStyle};
  ${(props) =>
    props.isNavL2 || props.isCardTypeTiles
      ? `
      box-shadow:  ${props.theme.shadow.NEW_DESIGN.BOX_SHADOW};
      border-radius: 16px;
      elevation: 10;`
      : ``};
  ${getAdditionalRedesignedStyles}
`;

const ListItemContainer = styled.View`
  width: 146px;
  height: ${(props) => (props.outOfStock ? `240px` : `auto`)};
  background: ${(props) => props.theme.colorPalette.white};
  border-radius: 11px;
  padding: 8px;
`;

const PlaceCashListContainer = styled.View`
  background: ${(props) => props.theme.colors.WHITE};
  border: 1px solid #fff;
  width: ${Dimensions.get('window').width / 2 - 20};
  margin: 10px 8px;
  border-radius: 15px;
  padding: 3px;
  shadow-color: ${(props) => props.theme.colors.BLACK};
  shadow-opacity: 0.25;
  shadow-radius: 3;
  elevation: 5;
`;

const PlaceCashDetailsContainer = styled.View`
  flex-direction: row;
`;

const PlaceCashPriceDetails = styled.View`
  flex-direction: column;
  overflow: hidden;
  padding-top: 10px;
  margin: 0px;
`;

const getImageSectionContainerMaxHeight = (props) => {
  const { isPlaceCashReward, isNavL2, isCardTypeTiles, isNewReDesignProductTile } = props;
  let maxHeight = 205;
  if (isNewReDesignProductTile) {
    maxHeight = 190;
  } else if (isNavL2 || isCardTypeTiles) {
    maxHeight = 161;
  } else if (isPlaceCashReward) {
    maxHeight = 85;
  }
  return maxHeight;
};

const ImageSectionContainer = styled.View`
  max-height: ${getImageSectionContainerMaxHeight};
  margin-top: 0;
  margin-bottom: ${(props) => (props.children.props.isPlaceCashReward ? 9 : 0)};
  margin-right: ${(props) => (props.children.props.isPlaceCashReward ? 10 : 0)};
  padding-top: 0;
  overflow: hidden;
`;

const BottomGridBanner = styled.View`
  width: 25px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-top-width: 19;
  border-top-color: ${(props) => props.theme.colors.PRIMARY.COLOR3};
  border-right-color: transparent;
  border-right-width: 19;
  border-bottom-color: ${(props) => props.theme.colors.PRIMARY.COLOR3};
  border-bottom-width: 19;
`;

const GridBannerText = styled.Text`
  width: 128px;
  letter-spacing: ${(props) => props.theme.typography.letterSpacings.ls038};
  font-size: ${(props) => props.theme.typography.fontSizes.fs10};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  color: ${(props) => props.theme.colorPalette.white};
  font-weight: ${(props) => props.theme.typography.fontWeights.black};
`;

const GridBannerTextBack = styled.View`
  width: 128px;
  height: 38px;
  padding-left: 10px;
  background-color: ${(props) => props.theme.colors.PRIMARY.COLOR3};
  justify-content: center;
`;

const BannerBottom = styled.View`
  flex-direction: row;
  align-items: center;
`;

const FavoriteIconContainer = styled.View`
  ${(props) => (props.inGridPlpRecommendation ? `padding-right: 5px` : ``)}
  ${getAdditionalFavoriteStyles}
`;

const OfferPriceAndFavoriteIconContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const Badge1Container = styled.View`
  height: 19;
`;

const TopCurveContainer = styled.View`
  ${(props) =>
    props.nonGrid
      ? `height: 10`
      : `
  background-color: white;
  height: 10;
  border-top-left-radius: 15;
  border-top-right-radius: 15;
  top: -5;`}
`;

const InGridEmptySpace = styled.View`
  height: 30px;
`;

const InGridComponent = styled.View`
  min-height: 435px;
  width: ${Dimensions.get('window').width / 2 - 20};
`;

const Badge1Text = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  color: ${(props) =>
    props.inGridPlpRecommendation
      ? props.theme.colorPalette.white
      : props.theme.colorPalette.gray[900]};
  line-height: 15;
  ${(props) =>
    props.inGridPlpRecommendation
      ? `text-align: center; letter-spacing: ${props.theme.typography.letterSpacings.ls038};`
      : ``}
  font-weight: ${(props) =>
    props.inGridPlpRecommendation
      ? props.theme.typography.fontWeights.extrabold
      : props.theme.typography.fontWeights.semibold};
  ${getAdditionalBadge1TextStyles}
`;

const additionalStyleBadge2Container = (props) => {
  if (props.badgeOne && props.isNewReDesignProductTile) return `margin-top: -5px`;
  if (!props.badgeOne && props.isNewReDesignProductTile) return `margin-top: -13px`;
  return `margin-top: 0px`;
};

const Badge2Container = styled.View`
  height: 14;
  ${(props) => (props.inGridPlpRecommendation ? `padding-left: 10px` : ``)};
  margin-bottom: ${(props) => (props.badgeOne && props.isNewReDesignProductTile ? '-4px' : '3px')};
  ${additionalStyleBadge2Container}
`;

const getAdditionalStyleBadge2Text = (props) => {
  const { isNewReDesignProductTile, badgeOne } = props;
  if (!badgeOne && isNewReDesignProductTile) {
    return `
      margin-top: -5px;
    `;
  }
  return ``;
};

const Badge2Text = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) =>
    props.isNewReDesignProductTile
      ? props.theme.typography.fontSizes.fs10
      : props.theme.typography.fontSizes.fs12};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  line-height: 12;
  text-transform: ${(props) => (props.isNewReDesignProductTile ? `capitalize` : `uppercase`)};
  ${getAdditionalStyleBadge2Text}
`;

const PricesSection = styled.View`
  margin-top: ${(props) =>
    !props.isFavoriteOOS
      ? props.theme.spacing.ELEM_SPACING.XXS
      : props.theme.spacing.APP_LAYOUT_SPACING.XS};
  ${(props) => (props.inGridPlpRecommendation ? `padding-left: 10px` : ``)}
`;

const PriceAndColorContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const OfferPriceAndBadge3Container = styled.View`
  flex-direction: row;
`;

const OfferPriceAndBadge3View = styled.View`
  height: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const TitleContainer = styled.TouchableOpacity`
  height: 32;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

const TitleText = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  line-height: 14.4;
  ${(props) => (props.inGridPlpRecommendation ? `padding-left: 10px` : ``)}
`;

const AddToBagContainer = styled.View`
  margin: ${(props) => (props.isPlaceCashReward ? '0px 8px 8px 8px' : '3px 0 3px 0')};
  width: ${(props) => (props.isPlaceCashReward ? '92%' : '100%')};
`;

const AddToBagContainerRedesign = styled.View`
  width: 100%;
`;

const CloseIconContainer = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const SuggestedContainer = styled.View`
  background: ${(props) => props.theme.colorPalette.gray[800]};
  border-bottom-right-radius: 8px;
  border-top-right-radius: 8px;
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  width: 100px;
  margin-top: 6px;
`;

const SeeSuggestedContainer = styled.View`
  min-height: 36;
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: 13;
`;

const PromotionalMessageDefaultView = styled.View`
  min-height: 36;
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const RatingDefaultView = styled.View`
  min-height: 27px;
  background: ${(props) => props.theme.colorPalette.white};
`;

const OfferPriceDefaultView = styled.View`
  min-height: 12px;
  background: ${(props) => props.theme.colorPalette.white};
`;

const AddToBagViewContainer = styled.View`
  flex: 1;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: -2px;
`;

const styles = css``;

export {
  styles,
  ListContainer,
  FavoriteIconContainer,
  Badge1Container,
  Badge1Text,
  Badge2Container,
  Badge2Text,
  PricesSection,
  OfferPriceAndBadge3Container,
  TitleContainer,
  TitleText,
  AddToBagContainer,
  AddToBagContainerRedesign,
  OfferPriceAndFavoriteIconContainer,
  ImageSectionContainer,
  RowContainer,
  OfferPriceAndBadge3View,
  CloseIconContainer,
  SuggestedContainer,
  SeeSuggestedContainer,
  RatingDefaultView,
  OfferPriceDefaultView,
  PlaceCashListContainer,
  PlaceCashDetailsContainer,
  PlaceCashPriceDetails,
  ListItemContainer,
  PromotionalMessageDefaultView,
  AddToBagViewContainer,
  BottomGridBanner,
  GridBannerText,
  InGridComponent,
  BannerBottom,
  TopCurveContainer,
  GridBannerTextBack,
  InGridEmptySpace,
  PriceAndColorContainer,
};
