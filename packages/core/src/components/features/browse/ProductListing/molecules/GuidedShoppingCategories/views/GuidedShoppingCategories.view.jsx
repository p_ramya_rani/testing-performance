// 9fbef606107a605d69c0edbcd8029e5d 
import React, { Component } from 'react';
import { Field } from 'redux-form';
import { PropTypes } from 'prop-types';
import styles from '../styles/GuidedShoppingCategories.style';
import { Row, Col, BodyCopy, Button } from '../../../../../../common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';

class GuidedShoppingCategoriesForm extends Component {
  getOptions = () => {
    const { content, filters } = this.props;
    const { options = [] } = content;
    const cmsCategoriesFacets = options && [...options].map(a => a.type);
    const arr = [];
    filters.forEach(item => {
      const n = cmsCategoriesFacets.indexOf(item.displayName);
      const style = options[n] ? options[n].style : '';
      if (arr[n]) {
        arr.splice(n, 0, { ...(item || {}), ...(options[n] || {}), style });
      } else {
        arr.push({ ...(item || {}), ...(options[n] || {}), style });
      }
    });
    return arr;
  };

  multicheckbox = ({ input, options, childComponent: CHILDCOMPONENT }) => {
    const { onChange, value: values = [] } = input;
    return options.map((option, index) => {
      const handleChange = (e, value) => {
        const arr = [...values];
        const { checked } = e.target;
        if (checked && value) {
          arr.push(value);
        } else {
          arr.splice(arr.indexOf(value), 1);
        }
        return onChange(arr);
      };
      const isChecked = values.includes(option.displayName);
      const selectedColor = isChecked ? { color: option.style } : {};
      return (
        <CHILDCOMPONENT
          name={`categoryCheckbox_${index}`}
          className="category-checkbox"
          checked={isChecked}
          input={{
            onChange: e => handleChange(e, option.displayName),
            name: `categoryCheckbox_${index}`,
          }}
        >
          <BodyCopy
            fontSize={['fs14', 'fs16', 'fs16']}
            fontFamily="secondary"
            fontWeight="bold"
            component="span"
            customStyle={selectedColor}
          >
            {option.text || option.displayName}
          </BodyCopy>
          {option.sizeText && (
            <BodyCopy
              className="text-category-size"
              fontSize={['fs14', 'fs16', 'fs16']}
              fontFamily="secondary"
              fontWeight="normal"
              component="span"
            >
              {` ${option.sizeText}`}
            </BodyCopy>
          )}
        </CHILDCOMPONENT>
      );
    });
  };

  render() {
    const { className, filedName, content, labels } = this.props;
    const { header } = content;
    const options = this.getOptions();
    return (
      <div className={className}>
        <BodyCopy
          fontFamily="secondary"
          tag="div"
          textAlign="center"
          fontSize={['fs14', 'fs16', 'fs16']}
        >
          {header && header.title}
        </BodyCopy>
        <fieldset>
          <Row fullBleed>
            <Field
              name={filedName}
              id="essential_categories"
              type="checkbox"
              options={options}
              component={this.multicheckbox}
              childComponent={InputCheckbox}
            />
          </Row>
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <Button fill="BLUE" type="submit" buttonVariation="variable-width">
              {labels.next}
            </Button>
          </Col>
        </fieldset>
      </div>
    );
  }
}

GuidedShoppingCategoriesForm.propTypes = {
  className: PropTypes.string.isRequired,
  filedName: PropTypes.string.isRequired,
  content: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  filters: PropTypes.arrayOf({}).isRequired,
};

GuidedShoppingCategoriesForm.defaultProps = {};

export default withStyles(GuidedShoppingCategoriesForm, styles);

export { GuidedShoppingCategoriesForm as GuidedShoppingCategoriesVanilla };
