// 9fbef606107a605d69c0edbcd8029e5d 
const getSortLabels = state => {
  return state.Labels && state.Labels.global && state.Labels.global.Sorting;
};

export default getSortLabels;
