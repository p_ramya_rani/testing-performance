// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

export const StyledCheckboxContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 22px 0;
`;

export const StyledCheckbox = styled.TouchableOpacity`
  flex-direction: row;
  height: 25px;
  width: 50%;
  margin-bottom: 18px;
`;

export const BodyCopyCustom = styled(BodyCopy)`
  margin-top: 4px;
`;

export const BodyCopyCustom2 = styled(BodyCopy)`
  font-style: italic;
  margin-top: 4px;
`;

export const ButtonWrapper = styled.View`
  align-items: center;
  display: flex;
`;
