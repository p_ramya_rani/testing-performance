// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @module BreadCrumbs
 * @summary A React component rendering a list of hyperlinks as bread crumbs.
 /**
 * Component that displays a list of section links.
 *
 * @example
 * <code>
 *   <Breadcrum className={string} name={string} steps={array[object]} />
 * </code>
 *
 * Component Props description/enumeration:
 *  @param {string} actualSection: the text to display as title
 *  @param {string} className: the additional pickup person details
 *  @param {array} steps: array of links to show
 *
 * Style (ClassName) Elements description/enumeration
 *  .breadcrum-container
 *  .breadcrum-item
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import isEqual from 'lodash/isEqual';
import { Anchor, BodyCopy } from '../../../../../../common/atoms';
import errorBoundary from '../../../../../../common/hoc/withErrorBoundary';
import withStyles from '../../../../../../common/hoc/withStyles';
import { getLocator } from '../../../../../../../utils/utils';
import FixedBreadCrumbsStyles from '../styles/FixedBreadCrumbs.styles';
import { getAPIConfig as apiConfig } from '../../../../../../../utils';

const asPathConstructor = (url) => {
  return url && url.replace('?cid=', '/');
};

const arePropsEqual = (prevProps, nextProps) => {
  return isEqual(prevProps.crumbs, nextProps.crumbs);
};
const constructSchemaList = (crumbs) => {
  const api = apiConfig();
  const schemaList = {
    '@context': 'https://schema.org/',
    '@type': 'BreadcrumbList',
    itemListElement: [],
  };
  const { protocol } = window?.location || '';
  const breadCrumbUrl = `${protocol}//${api.hostname}/${api.siteId}/c`;
  Object.entries(crumbs).forEach(([key, value]) => {
    const item = {};
    item['@type'] = 'ListItem';
    item.position = parseInt(key, 10) + 1;
    item.name = value.displayName;
    item.item = `${breadCrumbUrl}/${value.pathSuffix.replace('c?cid=', '')}`;
    schemaList.itemListElement.push(item);
  });
  return schemaList;
};

const renderAnchor = (itemClassName, pathSuffix, index, otherHyperLinkProps, displayName) => {
  return itemClassName !== 'breadcrum-last-item' ? (
    <Anchor
      className={`${itemClassName} showUnderline`}
      to={pathSuffix === 'home' ? `/${asPathConstructor(pathSuffix)}` : `/${pathSuffix}`}
      asPath={`/${asPathConstructor(pathSuffix)}`}
      data-locator={`${getLocator(`breadCrumb_L${index + 1}_Category`)}`}
      {...otherHyperLinkProps}
    >
      {displayName}
    </Anchor>
  ) : (
    <BodyCopy className={itemClassName} component="h1" fontFamily="secondary">
      {displayName}
    </BodyCopy>
  );
};
const FixedBreadCrumbs = ({ crumbs, separationChar, className, isPDPPage }) => {
  return (
    <div className={`${className} breadcrum-container`}>
      {crumbs &&
        crumbs.map((crumb, index) => {
          const { displayName, destinationUrl, destination, pathSuffix, ...otherHyperLinkProps } =
            crumb;
          // for PLP breadcrumb new field categoryKey is added So, if categoryKey exist then  pathSuffix will be updated by SeoUrl
          if (
            otherHyperLinkProps &&
            otherHyperLinkProps.pathSuffix &&
            otherHyperLinkProps.linkUrl
          ) {
            otherHyperLinkProps.pathSuffix = otherHyperLinkProps.categoryKey;
          }
          const itemClassName =
            index === crumbs.length - 1 && !isPDPPage ? 'breadcrum-last-item' : 'breadcrum-item';
          return (
            <span key={`${displayName}`} className="breadcrum-item-container">
              {destinationUrl ? (
                <a className={`${itemClassName} showUnderline`} href={destinationUrl}>
                  {displayName}
                </a>
              ) : (
                renderAnchor(itemClassName, pathSuffix, index, otherHyperLinkProps, displayName)
              )}
              {index !== crumbs.length - 1 && (
                <span className="breadcrum-separation">{separationChar}</span>
              )}
            </span>
          );
        })}
      <script type="application/ld+json">{JSON.stringify(constructSchemaList(crumbs))}</script>
    </div>
  );
};

FixedBreadCrumbs.propTypes = {
  crumbs: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        /** A description of a (non-legacy) page, or section within a page, to link to */
        destination: PropTypes.string,
        /** optional suffix for the url's path (used for example to concatenate a skuId, etc.) */
        pathSuffix: PropTypes.string,
        /** Optional simple object of key-value pairs that will be appended to the query string of the url */
        queryValues: PropTypes.string,
        /** optional hash to be added to url */
        hash: PropTypes.string,
        /** optional state part for the url (see window.location docs).
         * Observe that this is ignored if the page is not the same as the current page (as this is not preseved
         * when going back to the server).
         */
        state: PropTypes.shape({}),
      }),
      PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        /** A url to link to */
        destinationUrl: PropTypes.string.isRequired,
      }),
    ])
  ).isRequired,
  separationChar: PropTypes.string,
  className: PropTypes.string,
  isPDPPage: PropTypes.bool,
};

FixedBreadCrumbs.defaultProps = {
  separationChar: '/',
  className: '',
  isPDPPage: false,
};

export default React.memo(
  withStyles(errorBoundary(FixedBreadCrumbs), FixedBreadCrumbsStyles),
  arePropsEqual
);
