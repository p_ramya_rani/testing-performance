// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';
import { Platform } from 'react-native';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';

const Container = styled.View`
  width: 100%;
`;

const SafeAreaViewStyle = styled.SafeAreaView`
  flex: 1;
  background: rgba(0, 0, 0, 0.5);
`;

const ModalOutsideTouchable = styled.TouchableWithoutFeedback`
  flex: 1;
`;

const ModalCloseTouchable = styled.TouchableOpacity`
  position: absolute;
  right: 20;
  padding: 8px 8px 8px 8px;
`;

const ModalOverlay = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.5);
`;

const ModalContent = styled.View`
  background: ${(props) => props.theme.colorPalette.white};
  width: 100%;
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

const SortContent = styled.View`
  background: ${(props) => props.theme.colorPalette.text.lightgray};
  width: 100%;
  padding-bottom: ${Platform.OS === 'ios' ? (props) => props.theme.spacing.ELEM_SPACING.LRG : '0'};
  position: absolute;
  bottom: ${Platform.OS === 'ios' ? '0' : '50%'};
`;

const ModalTitleContainer = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
  margin-top: 20;
`;

const ModalTitle = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.typography.fontSizes.fs14};
  color: ${(props) => props.theme.colorPalette.gray[900]};
  line-height: 16.8;
  font-weight: ${(props) => props.theme.typography.fontWeights.black};
`;

// star AB test filter CSS

const FilterBarContainer = styled.View`
  background: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
`;

const FilterBarRow = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  margin: 5px 14px 0 4px;
`;

const ProductCount = styled.View`
  width: 20%;
`;

const FilterCtaContainer = styled.TouchableOpacity`
  flex-direction: row;
  width: 80%;
  justify-content: flex-end;
`;

const FilterSortText = styled(BodyCopy)`
  margin-left: 6px;
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
`;

const styles = css``;

export {
  styles,
  Container,
  ModalTitle,
  ModalTitleContainer,
  SafeAreaViewStyle,
  ModalOutsideTouchable,
  ModalOverlay,
  ModalContent,
  ModalCloseTouchable,
  SortContent,
  FilterBarContainer,
  FilterBarRow,
  ProductCount,
  FilterCtaContainer,
  FilterSortText,
};
