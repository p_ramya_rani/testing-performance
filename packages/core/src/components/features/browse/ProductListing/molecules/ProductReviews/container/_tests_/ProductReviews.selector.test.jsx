// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import { getUserState, getPersonalDataState, getLabels } from '../ProductReviews.selectors';

describe('#ProductReviews Selectors', () => {
  it('#getUserState  should return user state', () => {
    const userData = {
      userId: '324324',
    };
    const state = {
      User: userData,
    };
    expect(getUserState(state)).toEqual(userData);
  });

  it('#getPersonalDataState  should return getPersonalDataState', () => {
    const data = fromJS({
      userId: '324324',
    });
    const state = {
      User: fromJS({ personalData: data }),
    };
    expect(getPersonalDataState(state)).toEqual(data);
  });

  it('#getLabels  should return getLabels', () => {
    const data = {
      lbl_login_modal_title: 'lbl_login_modal_title',
      lbl_rating_form_title: 'lbl_rating_form_title',
      lbl_ratings_and_reviews: 'lbl_ratings_and_reviews',
    };

    const state = {
      Labels: {
        Browse: {
          PDP: data,
        },
      },
    };
    expect(getLabels(state)).toEqual(data);
  });
});
