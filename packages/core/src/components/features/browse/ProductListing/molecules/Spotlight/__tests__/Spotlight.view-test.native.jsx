// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';

import Spotlight from '../views/Spotlight.native';

describe('Spotlight component', () => {
  const props = {
    categoryId: '12323',
  };
  it('should renders correctly', () => {
    const component = shallow(<Spotlight {...props} />);
    expect(component).toMatchSnapshot();
  });
});
