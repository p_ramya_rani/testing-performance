import React from 'react';
import { shallow } from 'enzyme';
import StickyFilterButtons from '../views/StickyFilterButtons.view.native';

describe('StickyFilterButtons Component', () => {
  it('should render correctly', () => {
    const container = shallow(<StickyFilterButtons />);
    expect(container).toMatchSnapshot();
  });
});
