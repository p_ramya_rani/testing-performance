// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const CarouselWrapper = styled.View`
  margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 0
    ${props => props.theme.spacing.ELEM_SPACING.MED} 0;
  height: 120px;
`;

export const ButtonWrapper = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export const StyleImageWrapper = styled.View`
  width: auto;
  margin-left: auto;
  margin-right: auto;
`;
