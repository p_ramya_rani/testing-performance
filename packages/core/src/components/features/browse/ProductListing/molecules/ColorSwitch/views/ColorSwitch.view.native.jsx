// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { FlatList, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { getScreenWidth } from '@tcp/core/src/utils/index.native';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import { getProductListToPathInMobileApp } from '../../ProductList/utils/productsCommonUtils';
import {
  styles,
  ColorSwitchesContainer,
  ItemSeparatorStyle,
  ImageStyle,
  ImageTouchableOpacity,
  NextArrow,
  ArrowIcon,
  NewDesignItemSeparator,
} from '../styles/ColorSwitch.style.native';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import constants from '../ColorSwitch.constant';

const screenWidth = getScreenWidth();

const Icon = require('../../../../../../../../../mobileapp/src/assets/images/smallright.png');

/**
 * @param {String} selectedId : Selected color id
 * @param {Number} index : selected index id
 * @param {Function} setSelectedColorId : Method to set selected color
 * @param {Function} setSelectedColorIndex : Method to set image index
 * @desc This method call when select any color switch
 */
const onSelectHandler = (
  selectedId,
  index,
  setSelectedColorId,
  setSelectedColorIndex,
  onQuickViewOpenClick,
  colorsMapLength
) => {
  if (colorsMapLength > 1) onQuickViewOpenClick({ colorProductId: selectedId });
};

const getIsSelected = (selectedColorId, index, colorProductId) => {
  return (selectedColorId === 'none' && index === 0) || selectedColorId === colorProductId;
};

/**
 * @param {String} imageUrl : Image source
 * @desc This method paint color image with border
 */
const getImageIcon = (
  imageUrl,
  selected,
  itemBrand,
  imgConfig,
  colorName,
  isNewReDesignProductTile
) => {
  return (
    <ImageStyle
      url={imageUrl}
      swatchConfig={imgConfig}
      isProductImage
      selected={selected}
      itemBrand={itemBrand}
      colorName={colorName}
      isNewReDesignProductTile={isNewReDesignProductTile}
    />
  );
};

/**
 * @desc This is seperator method which used for making gap between color switches
 */
const RenderSeparator = () => {
  return <ItemSeparatorStyle />;
};

const NewDesignSeparator = () => {
  return <NewDesignItemSeparator />;
};

const getColorName = (name) => {
  const colorName = Array.isArray(name) && name.length ? name[0] : name;
  return colorName ? String(colorName).toLowerCase() : '';
};

const plpReDesignStyling = (plpReDesign) =>
  plpReDesign
    ? { marginLeft: constants.newDesignSeparatorMarginLeft, marginTop: 0, marginRight: 3 }
    : {};

/**
 * @param {Object} itemObj : colorsMap item
 * @param {Number} index : colorsMap item index
 * @param {String} selectedColorId : Selected Color Id
 * @param {Function} setSelectedColorId : Method to set selected color
 * @param {Function} setSelectedColorIndex : Method to set image index
 * @desc This renderer method of the list which draw color switches
 */

const RenderItem = (data) => {
  const {
    setSelectedColorId,
    setSelectedColorIndex,
    itemBrand,
    isShowingPlusCount,
    remainingColorCount,
    onGoToPDPPage,
    productItem,
    productInfo,
    colorName,
    accState,
    imgConfig,
    imageUrl,
    index,
    selected,
    colorProductId,
    selectedColorId,
    isNewReDesignProductTile,
    onQuickViewOpenClick,
    colorsMapLength,
    showPriceRangeCondition,
  } = data;
  const { indexForPlus, indexForPlusRange } = constants;
  if (
    isShowingPlusCount &&
    index === (showPriceRangeCondition ? indexForPlusRange : indexForPlus)
  ) {
    const pdpUrl = productInfo ? productInfo.pdpUrl : productItem.pdpUrl;
    const modifiedPdpUrl = getProductListToPathInMobileApp(pdpUrl) || '';
    return (
      <TouchableOpacity
        onPress={() => onGoToPDPPage(modifiedPdpUrl, selectedColorId, productInfo, productItem)}
        accessibilityRole="button"
        disabled={isNewReDesignProductTile}
        style={plpReDesignStyling(isNewReDesignProductTile)}
      >
        <BodyCopy
          fontFamily="secondary"
          fontWeight="regular"
          fontSize={isNewReDesignProductTile ? 'fs11' : 'fs12'}
          text={'+'.concat('', remainingColorCount)}
          color="black"
        />
      </TouchableOpacity>
    );
  }

  return (
    <ImageTouchableOpacity
      // eslint-disable-next-line
      accessibilityStates={[accState]}
      disabled={isNewReDesignProductTile}
      accessibilityHint="color switches"
      selected={isNewReDesignProductTile ? false : selected}
      accessibilityRole="button"
      accessibilityLabel={colorName}
      isNewReDesignProductTile={isNewReDesignProductTile}
      onPress={() =>
        onSelectHandler(
          colorProductId,
          index,
          setSelectedColorId,
          setSelectedColorIndex,
          onQuickViewOpenClick,
          colorsMapLength
        )
      }
    >
      {getImageIcon(imageUrl, selected, itemBrand, imgConfig, colorName, isNewReDesignProductTile)}
    </ImageTouchableOpacity>
  );
};

const RenderColorItem = (values) => {
  const {
    itemObj,
    selectedColorId,
    setSelectedColorId,
    setSelectedColorIndex,
    itemBrand,
    itemData,
    isFavorite,
    isShowingPlusCount,
    remainingColorCount,
    onGoToPDPPage,
    productItem,
    productInfo,
    isNewReDesignProductTile,
    colorsMap,
    onQuickViewOpenClick,
    colorsMapLength,
    showPriceRangeCondition,
  } = values;
  const { item, index } = itemObj;
  const { color, colorProductId } = item;
  const { relatedSwatchImages } = itemData;
  const { name } = color;
  let { swatchImage } = color;
  const colorName = getColorName(name);
  const selected = getIsSelected(selectedColorId, index, colorProductId);
  const accState = selected ? 'selected' : '';

  if (index !== 0 && relatedSwatchImages) {
    swatchImage = relatedSwatchImages.find(
      (swatchImageUrl) => swatchImageUrl === `${colorProductId}_swatch.jpg`
    );
  }
  let imgConfig = 't_swatch_img';
  if (isFavorite) {
    const prodId = colorProductId.split('_')[0];
    swatchImage = colorProductId ? `${prodId}/${colorProductId}_swatch.jpg` : '';
    imgConfig = 't_swatch_img,w_50,h_50,c_thumb,g_auto:0';
  }

  let formattedSwatchImage = swatchImage;
  if (!swatchImage) {
    formattedSwatchImage = `${colorProductId.split('_')[0]}/${colorProductId}.jpg`;
    imgConfig = 't_swatch_img,w_50,h_50,c_thumb,g_auto:0';
  }
  if (formattedSwatchImage && formattedSwatchImage.indexOf('/') === -1) {
    formattedSwatchImage = `${formattedSwatchImage.split('_')[0]}/${formattedSwatchImage}`;
  }

  const imageUrl = formattedSwatchImage || '';

  return RenderItem({
    setSelectedColorId,
    setSelectedColorIndex,
    itemBrand,
    isShowingPlusCount,
    remainingColorCount,
    onGoToPDPPage,
    productItem,
    productInfo,
    colorName,
    accState,
    imgConfig,
    imageUrl,
    index,
    selected,
    colorProductId,
    selectedColorId,
    isNewReDesignProductTile,
    colorsMap,
    onQuickViewOpenClick,
    colorsMapLength,
    showPriceRangeCondition,
  });
};

/**
 * @param {Object} props : props for colorsMap
 * @desc This method generate color switches
 */

class ColorSwitch extends React.Component {
  constructor() {
    super();
    this.state = { selectedColorId: 'none', isScrollEnd: false };
    this.scrollToIndex = constants.maxToRenderPerBatch;
  }

  setSelectedColorId = (selectedColorId) => {
    this.setState({ selectedColorId });
  };

  /**
   * This function will calculate whether we need to show the arrow for color swatch or not
   * this will be calculated dynamically based on product grid width, swatch width, margin etc
   */

  showNextCarousel = (colorsMap) => {
    if (!colorsMap) {
      return false;
    }
    const { widthPer } = this.props;
    const componentWidth = (screenWidth - 24) * (widthPer / 100);
    const oneImageSize = constants.imgWidth + constants.seperatorMarginLeft;
    return colorsMap.length > componentWidth / oneImageSize;
  };

  /**
   * to toggle the next arrow click of flat list.
   */
  handleNextClick = () => {
    const { isScrollEnd } = this.state;
    if (!isScrollEnd) {
      this.flatlist.scrollToEnd();
    } else {
      this.flatlist.scrollToIndex({ index: 0 });
    }
    this.setState({
      isScrollEnd: !isScrollEnd,
    });
  };

  render() {
    const {
      colorsMap,
      setSelectedColorIndex,
      itemBrand,
      itemData,
      isFavorite,
      isPLPPage,
      onGoToPDPPage,
      productItem,
      productInfo,
      isNewReDesignProductTile,
      onQuickViewOpenClick,
      showPriceRange,
      isBundleProduct,
      priceRangeExist,
    } = this.props;
    const colorsMapLength = colorsMap && colorsMap.length;
    const { selectedColorId } = this.state;
    const {
      initialNumOfColorOnPDP,
      indexForPlus,
      indexForPlusRange,
      initialNumOfColorOnPDPColorRange,
    } = constants;
    // const showNextArrow = colorsMap && colorsMap.length > initialNumToRender;
    const showNextArrow = this.showNextCarousel(colorsMap);
    const showPriceRangeCondition = (showPriceRange || isBundleProduct) && priceRangeExist;
    const isShowingPlusCount =
      isPLPPage &&
      colorsMap &&
      colorsMap.length > (showPriceRangeCondition ? indexForPlusRange : indexForPlus);
    const showPlusData =
      isShowingPlusCount &&
      colorsMap.slice(
        0,
        showPriceRangeCondition ? initialNumOfColorOnPDPColorRange : initialNumOfColorOnPDP
      );
    const remainingColorCount =
      isShowingPlusCount &&
      colorsMap.length - (showPriceRangeCondition ? indexForPlusRange : indexForPlus);

    return (
      <ColorSwitchesContainer
        isNextArrowVisible={showNextArrow}
        colorsMap={colorsMap.length}
        isNewReDesignProductTile={isNewReDesignProductTile}
      >
        <FlatList
          ref={(ref) => {
            this.flatlist = ref;
          }}
          listKey={(item) => item.colorProductId}
          data={isShowingPlusCount ? showPlusData : colorsMap}
          renderItem={(item) => {
            const values = {
              itemObj: item,
              selectedColorId,
              setSelectedColorId: this.setSelectedColorId,
              setSelectedColorIndex,
              itemBrand,
              itemData,
              isFavorite,
              isShowingPlusCount,
              remainingColorCount,
              onGoToPDPPage,
              productItem,
              productInfo,
              isNewReDesignProductTile,
              colorsMap,
              onQuickViewOpenClick,
              colorsMapLength,
              showPriceRangeCondition,
            };

            return RenderColorItem(values);
          }}
          keyExtractor={(item) => item.colorProductId}
          initialNumToRender={constants.initialNumToRender}
          maxToRenderPerBatch={constants.maxToRenderPerBatch}
          horizontal
          ItemSeparatorComponent={isNewReDesignProductTile ? NewDesignSeparator : RenderSeparator}
          showsHorizontalScrollIndicator={false}
        />
        {!isShowingPlusCount && showNextArrow && (
          <NextArrow onPress={this.handleNextClick}>
            <ArrowIcon alt="alt" source={Icon} />
          </NextArrow>
        )}
      </ColorSwitchesContainer>
    );
  }
}

ColorSwitch.propTypes = {
  props: PropTypes.shape({}),
  colorsMap: PropTypes.arrayOf(PropTypes.shape({})),
  setSelectedColorIndex: PropTypes.func,
  itemBrand: PropTypes.string.isRequired,
  itemData: PropTypes.shape({}),
  isFavorite: PropTypes.bool,
  widthPer: PropTypes.number,
  isPLPPage: PropTypes.bool,
  onGoToPDPPage: PropTypes.func,
  productItem: PropTypes.shape({}),
  productInfo: PropTypes.shape({}),
  isNewReDesignProductTile: PropTypes.bool,
  showPriceRange: PropTypes.bool.isRequired,
  isBundleProduct: PropTypes.bool.isRequired,
  priceRangeExist: PropTypes.func.isRequired,
};

ColorSwitch.defaultProps = {
  props: {},
  colorsMap: [],
  setSelectedColorIndex: () => {},
  itemData: {},
  isFavorite: false,
  widthPer: 0,
  isPLPPage: false,
  onGoToPDPPage: {},
  productItem: {},
  productInfo: {},
  isNewReDesignProductTile: false,
};

export default withStyles(ColorSwitch, styles);
export { ColorSwitch as ColorSwitchVanilla };
