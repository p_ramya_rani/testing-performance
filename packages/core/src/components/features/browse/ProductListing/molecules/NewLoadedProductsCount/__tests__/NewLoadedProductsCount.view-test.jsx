// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { NewLoadedProductsCountVanilla } from '../views/NewLoadedProductsCount.view';

describe('NewLoadedProductsCount component', () => {
  const props = {
    className: 'sfd89ywv wsdv080',
    totalProductsCount: 878,
    showingItemsLabel: {
      lbl_item: 'Item',
      lbl_items: 'Items',
    },
    breadCrumbs: [
      { displayName: 'Boy' },
      { displayName: 'School Uniform' },
      { displayName: 'Polos' },
    ],
  };
  it('should renders NewLoadedProductsCount correctly', () => {
    const component = shallow(<NewLoadedProductsCountVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders NewLoadedProductsCount correctly for empty breadcrumb', () => {
    const newProps = {
      ...props,
      breadCrumbs: [],
    };
    const component = shallow(<NewLoadedProductsCountVanilla {...newProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders NewLoadedProductsCount correctly for zero product', () => {
    const newProps = {
      ...props,
      totalProductsCount: 0,
    };
    const component = shallow(<NewLoadedProductsCountVanilla {...newProps} />);
    expect(component).toMatchSnapshot();
  });
});
