// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';
import { GuidedShoppingCategoriesVanilla } from '../views/GuidedShoppingCategories.view.native';

describe('GuidedShoppingCategories Component', () => {
  const props = {
    content: {
      header: {},
      button: {},
      options: [],
    },
    labels: {},
    filters: [],
    onSubmit: jest.fn(),
  };

  it('GuidedShoppingCategories should be defined', () => {
    const component = shallow(<GuidedShoppingCategoriesVanilla {...props} />);
    expect(component).toMatchSnapshot();
    expect(component).toBeDefined();
  });

  it('GuidedShoppingCategories should be render checkbox', () => {
    props.content.options = [
      {
        text: 'TODDLER GIRL',
        type: 'girl',
        categoryId: 'UnbxdCatId',
        style: 'green-600',
      },
    ];
    props.filedName = 'categoryPath1_uFilter';
    props.filters = [
      {
        displayName: 'GIRL',
      },
      {
        displayName: 'TODDLER GIRL',
      },
    ];
    const component = shallow(<GuidedShoppingCategoriesVanilla {...props} />);
    const options = component.instance().getOptions();
    const checkboxes = component.instance().multicheckbox({
      input: { value: [], onChange: jest.fn() },
      options,
      childComponent: InputCheckbox,
    });
    expect(component).toMatchSnapshot();
    expect(options.length).toBe(props.filters.length);
    expect(checkboxes.length).toBe(props.filters.length);
  });
});
