// 9fbef606107a605d69c0edbcd8029e5d 
import { USER_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { getAPIConfig, getLabelValue } from '@tcp/core/src/utils';
import { bin2hex, md5 } from '../encoding';

export const getUserState = state => {
  return state[USER_REDUCER_KEY];
};

export const getPersonalDataState = state => {
  return state[USER_REDUCER_KEY].get('personalData');
};

export const getLabels = state => {
  return {
    lbl_ratings_and_reviews: getLabelValue(
      state.Labels,
      'lbl_ratings_and_reviews',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    lbl_login_modal_title: getLabelValue(
      state.Labels,
      'lbl_login_modal_title',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    lbl_rating_form_title: getLabelValue(
      state.Labels,
      'lbl_rating_form_title',
      'PDP_Sub',
      'ProductDetailPage'
    ),
  };
};

export const getUserToken = (userId, mprId) => {
  const apiConfig = getAPIConfig();
  // as per BV guideline prod key should be used to POST review on local env also
  const sharedKey = apiConfig.BV_SHARED_KEY;

  // obtain current date in the format of yyyyMMdd
  const rightNow = new Date();
  const res = rightNow
    .toISOString()
    .slice(0, 10)
    .replace(/-/g, '');
  const queryString = `date=${res.toString()}&userid=${userId}&MprId=${mprId}`;
  // const queryString = `date=${res.toString()}&userid=300124031&MprId=B10000012060062`;
  // define unhashed security key
  const unhashed = sharedKey.toString().concat(queryString.toString());

  // obtain HEX representation of queryString
  const hexQueryString = bin2hex(queryString);

  // obtain MD5 hash of the unhashed security key
  const hashed = md5(unhashed);

  return hashed.toString() + hexQueryString.toString();
};
