// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import withStyles from '../../../../../../common/hoc/withStyles';
import SortSelectorStyle from '../SortSelector.style';
import CustomSelect from '../../CustomSelect/views';
import { getLocator } from '../../../../../../../utils';

export const FACETS_FIELD_KEY = {
  sort: 'sort',
};

class SortSelector extends React.PureComponent {
  render() {
    const {
      className,
      onChange,
      sortSelectOptions,
      selectTextOverride,
      onExpandCallback,
      expanded,
      hideTitle,
      isSortOpenModal,
      defaultPlaceholder,
      isFavoriteView,
    } = this.props;

    return (
      <Field
        name={FACETS_FIELD_KEY.sort}
        component={CustomSelect}
        optionsMap={sortSelectOptions}
        title={hideTitle ? '' : 'Sort By: '}
        placeholder={hideTitle ? '' : defaultPlaceholder || 'Sort'}
        allowMultipleSelections={false}
        className={className}
        onChange={onChange}
        onExpandCallback={onExpandCallback}
        expanded={expanded}
        selectTextOverride={selectTextOverride}
        type={FACETS_FIELD_KEY.sort}
        isSortOpenModal={isSortOpenModal}
        data-locator={getLocator('plp_sort_by_text')}
        isFavoriteView={isFavoriteView}
      />
    );
  }
}

SortSelector.propTypes = {
  className: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  sortSelectOptions: PropTypes.shape([]).isRequired,
  selectTextOverride: PropTypes.string.isRequired,
  onExpandCallback: PropTypes.func.isRequired,
  expanded: PropTypes.bool.isRequired,
  hideTitle: PropTypes.string.isRequired,
  isSortOpenModal: PropTypes.bool.isRequired,
  defaultPlaceholder: PropTypes.string.isRequired,
  isFavoriteView: PropTypes.bool.isRequired,
};

export default withStyles(SortSelector, SortSelectorStyle);
export { SortSelector as SortSelectorVanilla };
