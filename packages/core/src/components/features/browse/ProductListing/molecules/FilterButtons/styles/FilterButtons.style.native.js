// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components/native';

const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  max-width: 100%;
`;

const styles = css``;

export { styles, Container };
