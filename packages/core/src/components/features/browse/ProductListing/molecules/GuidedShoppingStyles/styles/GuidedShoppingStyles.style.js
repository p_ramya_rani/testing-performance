// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .guided-shopping-style {
    margin-top: 8px;

    @media ${props => props.theme.mediaQuery.large} {
      margin: 20px 40px 0 40px;
    }

    .tcp_carousel_wrapper {
      padding-bottom: 20px;

      .slick-prev {
        left: 0;
      }

      .slick-next {
        right: 0;
      }

      button.slick-arrow.slick-disabled {
        cursor: not-allowed;
        opacity: 0.2;
      }
    }

    .slick-list {
      @media ${props => props.theme.mediaQuery.large} {
        margin: 0 25px 0;
      }
    }

    .slick-arrow {
      @media ${props => props.theme.mediaQuery.medium} {
        display: none;
      }
    }

    .slick-slide > div {
      display: inline-block;
    }

    .CheckBox__input {
      display: none;
    }

    .labelStyle {
      margin-top: 0px;
    }

    .CheckBox__text {
      width: 75px;
      @media ${props => props.theme.mediaQuery.large} {
        width: 66px;
      }

      .style-name {
        padding-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
        text-align: center;
        letter-spacing: -0.22px;
        color: ${props => props.theme.colors.BLACK};
      }
    }

    .facet-img {
      width: 63px;
      height: 59px;
      margin: 0 auto;

      @media ${props => props.theme.mediaQuery.large} {
        width: 75px;
        height: 71px;
      }
    }

    .back-button {
      margin-right: 14px;
    }
  }
`;

export default styles;
