// 9fbef606107a605d69c0edbcd8029e5d 
import { getSeoHeaderCopyFromBreadCrumb } from '../container/ProductListing.util';

describe('isOrderHasPickup', () => {
  const breadcrumb = [
    {
      categoryId: '47502',
      displayName: 'Toddler Girl',
      urlPathSuffix: '/c?cid=toddler-girl-clothes',
      seoHeaderCopy: 'test1',
    },
    {
      categoryId: '419528',
      displayName: 'Graphic Tees',
      urlPathSuffix: '/c?cid=toddler-girls-tees',
      seoHeaderCopy: 'test2',
    },
    {
      categoryId: '419553',
      displayName: 'Long Sleeve',
      urlPathSuffix: '/c?cid=toddler-girl-long-sleeve-t-shirts',
      seoHeaderCopy: 'test3',
    },
  ];
  it('should return seoheader', () => {
    expect(getSeoHeaderCopyFromBreadCrumb(breadcrumb)).toEqual('test3');
  });
});
