// 9fbef606107a605d69c0edbcd8029e5d
import logger from '@tcp/core/src/utils/loggerInstance';

const getIndex = (data) => {
  return data && data.some((category) => !!(category && category.url)) ? data.length : 0;
};

export const extractCategory = (category = '') => {
  // Extracting category id or path from the URL
  try {
    let categoryId;
    if (Number.isInteger(category)) {
      categoryId = category;
    } else if (category && category.lastIndexOf('/') === category.length - 1) {
      categoryId = category && category.split('/');
      categoryId = categoryId.length > 1 ? categoryId[categoryId.length - 2] : categoryId[0];
    } else {
      categoryId = category && category.split('/').pop();
    }
    return categoryId;
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'ProductListing Util - extractCategory',
        categoryRecieved: category,
      },
    });
  }
  return category;
};

const extractCategoryFilters = (navArray) => {
  const categoryFilterArray = navArray.map((arrayItem) => {
    const overrideCategoryName = arrayItem.categoryContent?.mainCategory?.set?.filter(
      (setEntry) => setEntry.key === 'categoryName' && setEntry.val
    )[0]?.val;
    if (!arrayItem.categoryContent?.displayToCustomer) {
      return false;
    }
    if (
      arrayItem.categoryContent &&
      arrayItem.categoryContent.mainCategory &&
      arrayItem.categoryContent.mainCategory.categoryFilterImage &&
      arrayItem.categoryContent.mainCategory.categoryFilterImage[0]
    ) {
      return {
        ...arrayItem.categoryContent.mainCategory.categoryFilterImage[0],
        linkUrl: arrayItem.url,
        linkAspath: arrayItem.asPath,
        filterTitle: overrideCategoryName || arrayItem.categoryContent.name,
      };
    }
    return {
      linkUrl: arrayItem.url,
      linkAspath: arrayItem.asPath,
      filterTitle: overrideCategoryName || arrayItem.categoryContent.name,
    };
  });

  return categoryFilterArray.filter((catItem) => !!catItem);
};

const hasSubCategories = (navObj, subCategoryArr) => {
  return navObj && navObj.subCategories && !navObj.subCategories.length && subCategoryArr.length;
};

const getNewCatArr = (navObj, subCategoryArr) => {
  let newCatArr = [];
  if (hasSubCategories(navObj, subCategoryArr)) {
    for (let groupCount = 0; groupCount < subCategoryArr.length; groupCount += 1) {
      newCatArr = newCatArr.concat(navObj.subCategories[subCategoryArr[groupCount]].items);
    }
  } else if (navObj.subCategories) {
    newCatArr = newCatArr.concat(navObj.subCategories);
  }
  return newCatArr;
};

const getSubcatArrInitial = (navObj) =>
  (navObj.subCategories && Object.keys(navObj.subCategories)) || [];
const getNavUrl = (navObj) => extractCategory(navObj.url && navObj.url.replace('/c?cid=', ''));

const isUrlMatching = (navObj, navUrl, categoryId) => {
  return (
    (navObj.categoryContent && navObj.categoryContent.categoryId === categoryId) ||
    (navUrl && navUrl.toLowerCase()) === (categoryId && categoryId.toLowerCase())
  );
};

const compareSetVal = (navObj, setKey, expectedVal) => {
  return (
    navObj?.categoryContent?.mainCategory?.set?.filter(
      (setEntry) => setEntry.key === setKey && setEntry.val
    )[0]?.val === expectedVal
  );
};

const extractSetVal = (navObj, setKey) => {
  const setItem =
    navObj?.categoryContent?.mainCategory?.set?.filter((item) => item.key === setKey) || [];
  return setItem && setItem[0] && setItem[0].val;
};

const getShopAllImageDetails = (navObj) =>
  (navObj.categoryContent?.mainCategory?.categoryFilterImage &&
    navObj.categoryContent.mainCategory.categoryFilterImage.length &&
    navObj.categoryContent.mainCategory.categoryFilterImage[0]) ||
  {};

const getShopAllArray = (
  categoryFilters,
  carouselShowShopAll,
  navObj,
  shopAllLabel,
  shopAllDisplayLocation
) => {
  if (categoryFilters && carouselShowShopAll) {
    const imageDetails = getShopAllImageDetails(navObj);
    const shopAllArray = [
      {
        linkAspath: navObj.asPath,
        linkUrl: navObj.url,
        filterTitle: shopAllLabel || '',
        ...imageDetails,
      },
    ];
    return shopAllDisplayLocation === 'last'
      ? [...categoryFilters, ...shopAllArray]
      : [...shopAllArray, ...categoryFilters];
  }
  return categoryFilters;
};

const checkIfL2AndShouldShowSiblings = (categoryFilters, shouldShowSiblings) =>
  categoryFilters && shouldShowSiblings;

const isNewCatArrHasItem = (newCatArr) => newCatArr && newCatArr.length;

// eslint-disable-next-line sonarjs/cognitive-complexity
export const findCategoryFilter = (data, category, shopAllLabel) => {
  const index = getIndex(data);
  let iterator = 0;
  let navTreeStructureArray = [];
  const categoryId = extractCategory(category);

  while (iterator < index) {
    const navObj = data[iterator];
    const subCategoryArr = getSubcatArrInitial(navObj);
    const newCatArr = getNewCatArr(navObj, subCategoryArr);

    const navUrl = getNavUrl(navObj);

    if (isUrlMatching(navObj, navUrl, categoryId)) {
      // If the leaf node has categoryFilters object, it means it is an L2
      // In case of L2, we need to consider the configuration to show siblings or children
      // set.showSibling = "true" in case we need to show siblings
      let carouselData;
      const categoryFilters = navObj?.categoryFilters;
      const shouldShowSiblings = compareSetVal(navObj, 'showSibling', 'true');
      const shouldHideCarousel = compareSetVal(navObj, 'hideCarousel', 'true');
      const carouselTitle = extractSetVal(navObj, 'shopByCategory');
      if (shouldHideCarousel) {
        carouselData = [];
      }
      // If L2(=categoryFilters is defined) and should show siblings
      else if (checkIfL2AndShouldShowSiblings(categoryFilters, shouldShowSiblings)) {
        carouselData = extractCategoryFilters(data);
      }
      // if L2(=categoryFilters is defined) and should not show siblings, ie, show children
      // if L3, we do not have carouselData info at this level. Look for a level up = carouselData would be undefined.
      else {
        carouselData = categoryFilters;
      }
      navTreeStructureArray.push({
        set: navObj?.categoryContent?.mainCategory?.set,
        carouselData,
        carouselTitle,
      });
    } else if (isNewCatArrHasItem(newCatArr)) {
      navTreeStructureArray = findCategoryFilter(newCatArr, category, shopAllLabel);
      if (navTreeStructureArray.length) {
        const categoryFilters = navObj?.categoryFilters;
        const carouselTitle = extractSetVal(navObj, 'shopByCategory');

        const carouselShowShopAll = compareSetVal(navObj, 'shopAll', 'true');
        const shopAllDisplayLocation = extractSetVal(navObj, 'displayLocation');
        const shouldHideCarousel = compareSetVal(navObj, 'hideCarousel', 'true');
        // This is not the leaf node.
        // It is precisely the L2 if categoryFilters is defined.
        // If show children, then use the categoryFilters object as is.
        // if show siblings, still we need to use categoryFilters as this is for L3.
        // Also, check the ShopAll configuration and append the Shop ALL in the category Filter list.
        let carouselData = getShopAllArray(
          categoryFilters,
          carouselShowShopAll,
          navObj,
          shopAllLabel,
          shopAllDisplayLocation
        );
        if (shouldHideCarousel) {
          carouselData = [];
        }
        navTreeStructureArray.push({
          set: navObj?.categoryContent?.mainCategory?.set,
          carouselData,
          carouselTitle,
        });
      }
    }
    if (navTreeStructureArray.length) {
      break;
    } else {
      iterator += 1;
    }
  }
  return navTreeStructureArray;
};
