// 9fbef606107a605d69c0edbcd8029e5d 
const submitProductListingFiltersForm = (
  formData,
  submit,
  getProducts,
  url,
  isKeepModalOpen,
  isFilterAbTestOn
) => {
  const data = {
    URI: 'category',
    ignoreCache: true,
    url,
    sortBySelected: true,
    formData,
    scrollToTop: true,
    isKeepModalOpen,
    filterApplied: true,
    isFilterAbTestOn,
  };
  getProducts(data);
};

export default submitProductListingFiltersForm;
