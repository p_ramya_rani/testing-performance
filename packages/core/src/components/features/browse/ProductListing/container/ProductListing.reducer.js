// 9fbef606107a605d69c0edbcd8029e5d
import logger from '@tcp/core/src/utils/loggerInstance';
import PRODUCTLISTINGPAGE_CONSTANTS from './ProductListing.constants';
import { DEFAULT_REDUCER_KEY, setCacheTTL } from '../../../../../utils/cache.util';

const initialState = {
  [DEFAULT_REDUCER_KEY]: null,
  essentialShopStep: 0,
};

// eslint-disable-next-line complexity
const ProductListingReducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_PRODUCTS:
      return {
        ...state,
        loadedProductsPages: [...state.loadedProductsPages, action.payload.loadedProductsPages[0]],
        isFirstTimeLoad: false,
      };
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_FIRST_PRODUCTS_PAGE:
      return {
        ...state,
        ...action.productsPage,
        [DEFAULT_REDUCER_KEY]: setCacheTTL(),
        isFirstTimeLoad: true,
        ssr: true,
      };
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_PLP_LOADING_STATE:
      return { ...state, ...action.payload };
    case PRODUCTLISTINGPAGE_CONSTANTS.UNSET_PLP_BREADCRUMB:
      return { ...state, ...action.payload };
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_WISHLIST_ITEMS:
      return { ...state };
    case PRODUCTLISTINGPAGE_CONSTANTS.RESET_PRODUCTS:
      return { ...state };
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_FILTER:
      return { ...state, ...{ selectedFilter: action.payload } };
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_ADD_TO_FAVORITE: {
      if (state.loadedProductsPages) {
        state.loadedProductsPages.forEach((products) => {
          products.forEach((item) => {
            if (
              item.miscInfo &&
              item.productInfo &&
              item.productInfo.generalProductId === action.payload.colorProductId
            ) {
              // eslint-disable-next-line no-param-reassign
              item.miscInfo = {
                ...item.miscInfo,
                isInDefaultWishlist: true,
                favoriteCounter: action.payload.res && action.payload.res.favoritedCount,
              };
            }
          });
        });
      }
      return { ...state };
    }
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_FIRST_PRODUCTS_PAGE_SSR:
      return {
        ...state,
        ...action.payload,
        [DEFAULT_REDUCER_KEY]: setCacheTTL(),
        isFirstTimeLoad: true,
        ssr: true,
      };
    case PRODUCTLISTINGPAGE_CONSTANTS.CATEGORY_NAVIGATION_INFO:
      return {
        ...state,
        categoryNavigationInfo: action.payload,
      };
    case PRODUCTLISTINGPAGE_CONSTANTS.ESSENTIAL_SHOP_STEP:
      return {
        ...state,
        essentialShopStep: action.payload,
      };
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_CATEGORY_PAGE_SEO_DATA: {
      try {
        const {
          payload: {
            response: {
              categorySeoData: {
                seoTitle = '',
                seoMetaDesc = '',
                canonicalUrl = '',
                longDescription: currentListingDescription = '',
              } = {},
            } = {},
          } = {},
        } = action;
        return {
          ...state,
          seoTitle,
          seoMetaDesc,
          canonicalUrl,
          currentListingDescription,
        };
      } catch (e) {
        logger.error({
          error: `ProductListing.reducer.js | SET_CATEGORY_PAGE_SEO_DATA | ${e}`,
          extradata: {
            methodName: 'PRODUCTLISTINGPAGE_CONSTANTS.SET_CATEGORY_PAGE_SEO_DATA',
          },
        });
        return state;
      }
    }
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_PLP_SCROLL_STATE: {
      return {
        ...state,
        shouldPlpScroll: action.payload,
      };
    }
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_DEVICE_TIER: {
      return {
        ...state,
        deviceTier: action.payload,
      };
    }
    case PRODUCTLISTINGPAGE_CONSTANTS.SET_BOPIS_FILTER_STATE: {
      return {
        ...state,
        isBopisFilterOn: action.payload,
      };
    }
    default: {
      return { ...state };
    }
  }
};

export default ProductListingReducer;
