/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import isEqual from 'lodash/isEqual';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { reset } from 'redux-form';
import * as labelsSelectors from '@tcp/core/src/reduxStore/selectors/labels.selectors';
import {
  getIsKeepAliveProductApp,
  getIsShowPriceRangeForApp,
  getABTestIsShowPriceRange,
  getChangeFilterViewABTest,
  getABTestAppCategoryCarouselL3,
  getIsBrierleyPromoEnabled,
  getIsDynamicBadgeEnabled,
  getIsPLPCarouselEnabled,
  getIsNewReDesignEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import get from 'lodash/get';
import { getFavoriteStore } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import { TIER1 } from '@tcp/core/src/constants/tiering.constants';
import ProductListing from '../views';
import {
  getPlpProducts,
  getMorePlpProducts,
  resetPlpProducts,
  setFilter,
  setEssentialShopStep,
  setDeviceTier,
} from './ProductListing.actions';
import names from '../../../../../constants/eventsName.constants';
import { processBreadCrumbs, getProductsWithPromo } from './ProductListing.util';
import {
  addItemsToWishlist,
  setLastDeletedItemIdAction,
} from '../../Favorites/container/Favorites.actions';
import {
  openQuickViewWithValues,
  setQuickViewProductDetailPageTitle,
} from '../../../../common/organisms/QuickViewModal/container/QuickViewModal.actions';
import {
  getNavigationTreeApp,
  getLoadedProductsCount,
  getUnbxdId,
  getCategoryId,
  getLabelsProductListing,
  getAccessibilityLabels,
  getLabelsAccountOverView,
  getLongDescription,
  getIsLoadingMore,
  getLastLoadedPageNumber,
  getAppliedFilters,
  updateAppliedFiltersInState,
  getAllProductsSelect,
  getScrollToTopValue,
  getModalState,
  getTotalProductsCount,
  getIsDataLoading,
  getSelectedFilter,
  getPLPTopPromos,
  getLabelsOutOfStock,
  getIsHidePLPAddToBag,
  getPLPGridPromos,
  getIsHidePLPRatings,
  getPlpHorizontalPromo,
  getOnModelImageAbTestPlp,
  getAppliedSortId,
  getExternalCampaignFired,
  getEntityCategory,
  getLabels,
  getAbPlpPersonalized,
  getAbPlpPersonalizedCategories,
  getPlpInGridRecommendation,
  getPlpInMultiGridRecommendation,
  getGenericCarousel,
  getShowCarouselFlag,
  getCategoryFilterArray,
} from './ProductListing.selectors';
import { getIsPickupModalOpen } from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import {
  isPlccUser,
  getUserLoggedInState,
  isRememberedUser,
} from '../../../account/User/container/User.selectors';
import submitProductListingFiltersForm from './productListingOnSubmitHandler';
import getSortLabels from '../molecules/SortSelector/views/Sort.selectors';
import {
  fetchErrorMessages,
  wishListFromState,
  getDefaultFavItemInProgressFlag,
} from '../../Favorites/container/Favorites.selectors';
import {
  trackPageView,
  setClickAnalyticsData,
  setCommonAnalyticsData,
  trackClick,
} from '../../../../../analytics/actions';
import { setExternalCampaignState } from '../../../../common/molecules/Loader/container/Loader.actions';
import { getCommonAnalyticsData } from '../../../../../analytics/Analytics.selectors';

class ProductListingContainer extends React.Component {
  categoryUrl;

  constructor(props) {
    super(props);
    const { resetProducts, navigation, setDeviceTierInState } = this.props;
    this.state = {
      showCustomLoader: (navigation && navigation.getParam('showCustomLoader')) || false,
    };
    this.analyticsFired = false;
    this.analyticsShouldFired = false;
    this.currentScreen = names.screenNames.product_listing;
    resetProducts();

    let defaultDeviceTier = TIER1;
    this.deviceTier = defaultDeviceTier;

    if (navigation) {
      const { deviceTier } = navigation.getScreenProps();
      defaultDeviceTier = deviceTier;
    }
    this.deviceTier = defaultDeviceTier;
    setDeviceTierInState(this.deviceTier);
  }

  componentDidMount() {
    const { setSelectedFilter } = this.props;
    setSelectedFilter({});
    this.makeApiCall();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate({
    navigation: oldNavigation,
    sort: oldSort,
    selectedFilterValue: OldselectedFilterValue,
  }) {
    const {
      getProducts,
      navigation,
      products,
      breadCrumbs,
      sort,
      selectedFilterValue,
      setEssentialStep,
      changeFilterViewABTest,
    } = this.props;
    const oldNavigationUrl = oldNavigation.getParam('url');
    const newNavigationUrl = navigation.getParam('url');
    if (navigation && oldNavigationUrl !== newNavigationUrl) {
      this.categoryUrl = newNavigationUrl;
      setEssentialStep(0);
      getProducts({
        URI: 'category',
        url: newNavigationUrl,
        ignoreCache: true,
        isFilterAbTestOn: changeFilterViewABTest,
      });
    }

    this.resetCustomLoader();

    if (oldSort !== sort || OldselectedFilterValue !== selectedFilterValue) {
      this.analyticsShouldFired = true;
      if (this.analyticsFired) {
        this.currentScreen = 'product_listing_filters';
      }
    }

    if ((!this.analyticsFired || this.analyticsShouldFired) && products.length) {
      this.triggerAnalyticsTracking(products, breadCrumbs, this.currentScreen);
      this.analyticsFired = true;
      this.analyticsShouldFired = false;
    }
  }

  triggerAnalyticsTracking = (products, breadCrumbs, currentScreen) => {
    const {
      trackPageLoad,
      navigation,
      userFavStoreId,
      isExternalCampaignFired,
      setExternalCampaignFired,
      firstTouchData,
      setCommonAnalyticsDataFirstTouch,
    } = this.props;
    const icid = navigation.getParam('icid');
    const externalCampaignId = navigation.getParam('externalCampaignId');
    const omMID = navigation.getParam('omMID');
    const omRID = navigation.getParam('omRID');
    const productsFormatted = this.formatProductsData(products, breadCrumbs);
    const breadCrumbNames = breadCrumbs.map((breadCrumb) => breadCrumb.displayName.toLowerCase());
    const pageName = `browse:${breadCrumbNames.join(':')}`;
    const [departmentList, categoryList, subCategoryList] = breadCrumbNames;
    const nonInternalStr = 'non-internal search';

    let pageNavigationText = breadCrumbNames.join('-');
    let icidValue = 'non-internal campaign';
    let origin = 'browse';
    let eventsData = ['event91', 'event82'];
    let supressItems = [];

    if (icid) {
      icidValue = icid;
      pageNavigationText = 'non-browse';
      origin = 'internal campaign';
      eventsData = ['event91', 'event92', 'event81'];
      supressItems = [names.eVarPropEvent.prop21];
    }

    if (externalCampaignId) {
      eventsData.push('event18');
      setExternalCampaignFired(true);
    }

    if (isExternalCampaignFired && !externalCampaignId) {
      eventsData.push('event19');
      setExternalCampaignFired(false);
    }

    let commonAnalyticsData = firstTouchData;
    if (
      !commonAnalyticsData ||
      (commonAnalyticsData && !commonAnalyticsData.navigationFirstTouch)
    ) {
      commonAnalyticsData = { ...commonAnalyticsData, navigationFirstTouch: pageNavigationText };
      setCommonAnalyticsDataFirstTouch({ ...commonAnalyticsData });
    }

    const pageData = {
      pageName,
      pageType: 'browse',
      pageSection: `browse:${breadCrumbNames[0]}`,
      pageSubSection: pageName,
      customEvents: eventsData,
      pageNavigationText,
      products: productsFormatted,
      searchType: nonInternalStr,
      searchText: nonInternalStr,
      departmentList,
      categoryList,
      subCategoryList,
      breadcrumb: breadCrumbNames.join(':'),
      internalCampaignId: icidValue,
      listingCount: nonInternalStr,
      originType: origin,
      supressProps: supressItems,
      productsCount: products.length,
      storeId: userFavStoreId,
      ...(externalCampaignId && {
        externalCampaignId,
      }),
      ...(omMID && { omMID }),
      ...(omRID && { omRID }),
    };

    trackPageLoad(pageData, currentScreen);
  };

  getAddType = (breadCrumbs) => {
    let type = '';
    const formattedCategory =
      breadCrumbs &&
      breadCrumbs.map((listing) => listing.displayName && listing.displayName.toLowerCase());

    if (formattedCategory.length && formattedCategory.includes('accessories')) {
      type = 'accessories';
    } else if (formattedCategory.includes('shoes')) {
      type = 'shoes';
    } else {
      type = 'apparel';
    }
    return type;
  };

  formatProductsData = (products, breadCrumbs) => {
    const type = this.getAddType(breadCrumbs);
    return products.map((tile, index) => {
      const {
        productInfo: { listPrice, offerPrice, name, generalProductId, priceRange } = {},
        miscInfo: { categoryName } = {},
      } = tile;
      const lowOfferPrice = priceRange && priceRange.lowOfferPrice ? priceRange.lowOfferPrice : 0;
      const productId = generalProductId && generalProductId.split('_')[0];
      const productName = name;
      return {
        id: productId,
        colorId: generalProductId,
        name: productName,
        price: offerPrice,
        listPrice,
        extPrice: lowOfferPrice,
        position: index + 1,
        type: categoryName || type,
        pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
      };
    });
  };

  resetCustomLoader = () => {
    const { showCustomLoader } = this.state;
    const { isDataLoading } = this.props;
    if (showCustomLoader && !isDataLoading) {
      this.setState({ showCustomLoader: false });
    }
  };

  makeApiCall = () => {
    const { getProducts, navigation, changeFilterViewABTest } = this.props;
    this.categoryUrl = navigation && navigation.getParam('url');
    getProducts({
      URI: 'category',
      url: this.categoryUrl,
      ignoreCache: true,
      isFilterAbTestOn: changeFilterViewABTest,
    });
  };

  onGoToPDPPage = (title, pdpUrl, selectedColorProductId, productInfo) => {
    const { navigation } = this.props;
    const { bundleProduct } = productInfo;
    const routeName = bundleProduct ? 'BundleDetail' : 'ProductDetail';
    navigation.navigate(routeName, {
      title,
      pdpUrl,
      selectedColorProductId,
      clickOrigin: 'browse',
      reset: true,
    });
  };

  onLoadMoreProducts = () => {
    const { getMoreProducts, changeFilterViewABTest } = this.props;
    getMoreProducts({
      URI: 'category',
      url: this.categoryUrl,
      ignoreCache: true,
      isFilterAbTestOn: changeFilterViewABTest,
    });
  };

  onAddItemToFavorites = (payload) => {
    const { onAddToFavorites, breadCrumbs } = this.props;
    const type = this.getAddType(breadCrumbs);
    const data = payload;
    data.customData = { addType: type, index: payload.index };
    onAddToFavorites(data);
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1800;
  };

  onScrollHandler = ({ nativeEvent }) => {
    const { loadedProductCount, totalProductsCount, isLoadingMore } = this.props;
    if (
      loadedProductCount < totalProductsCount &&
      this.isCloseToBottom(nativeEvent) &&
      !isLoadingMore
    ) {
      this.onLoadMoreProducts();
    }
  };

  render() {
    const {
      products,
      currentNavIds,
      navTree,
      breadCrumbs,
      filters,
      totalProductsCount,
      filtersLength,
      initialValues,
      longDescription,
      labels,
      accessibilityLabels,
      isLoadingMore,
      lastLoadedPageNumber,
      labelsFilter,
      categoryId,
      getProducts,
      navigation,
      sortLabels,
      onAddItemToFavorites,
      isLoggedIn,
      isPlcc,
      labelsLogin,
      plpTopPromos,
      isSearchListing,
      isKeepModalOpen,
      QRAnimationURL,
      trackPageLoad,
      loadedProductCount,
      defaultWishListFromState,
      entityCategoryName,
      isAbPlpPersonalized,
      abPlpPersonalizedCategories,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      categoryFilterArray,
      isNewReDesignProductTile,
      productInfo,
      ...otherProps
    } = this.props;
    const { showCustomLoader } = this.state;
    const newMargins = isNewReDesignProductTile ? '0px' : '0 12px 0 12px';
    const newPaddings = isNewReDesignProductTile ? '5px' : '0px';
    return (
      <ProductListing
        margins={newMargins}
        paddings={newPaddings}
        products={products}
        filters={filters}
        currentNavIds={currentNavIds}
        categoryId={categoryId}
        navTree={navTree}
        breadCrumbs={breadCrumbs}
        totalProductsCount={totalProductsCount}
        initialValues={initialValues}
        filtersLength={filtersLength}
        longDescription={longDescription}
        labelsFilter={labelsFilter}
        labels={labels}
        accessibilityLabels={accessibilityLabels}
        labelsLogin={labelsLogin}
        isLoadingMore={isLoadingMore}
        lastLoadedPageNumber={lastLoadedPageNumber}
        onSubmit={submitProductListingFiltersForm}
        getProducts={getProducts}
        navigation={navigation}
        onGoToPDPPage={this.onGoToPDPPage}
        sortLabels={sortLabels}
        onLoadMoreProducts={this.onLoadMoreProducts}
        onAddItemToFavorites={this.onAddItemToFavorites}
        isLoggedIn={isLoggedIn}
        isPlcc={isPlcc}
        plpTopPromos={plpTopPromos}
        isSearchListing={isSearchListing}
        isKeepModalOpen={isKeepModalOpen}
        showCustomLoader={showCustomLoader}
        QRAnimationURL={QRAnimationURL}
        resetCustomLoader={this.resetCustomLoader}
        trackPageLoad={trackPageLoad}
        loadedProductCount={loadedProductCount}
        onScrollHandler={this.onScrollHandler}
        wishList={defaultWishListFromState}
        entityCategoryName={entityCategoryName}
        isAbPlpPersonalized={isAbPlpPersonalized}
        abPlpPersonalizedCategories={abPlpPersonalizedCategories}
        isBrierleyPromoEnabled={isBrierleyPromoEnabled}
        onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        categoryFilters={categoryFilterArray}
        isNewReDesignProductTile={isNewReDesignProductTile}
        {...otherProps}
      />
    );
  }
}

function mapStateToProps(state) {
  const appliedFilters = getAppliedFilters(state);
  const plpGridPromos = getPLPGridPromos(state);

  const loadedProductCount = getLoadedProductsCount(state);
  const totalProductsCount = getTotalProductsCount(state);
  const plpHorizontalPromo = getPlpHorizontalPromo(state);
  const products = getAllProductsSelect(state);
  // eslint-disable-next-line
  let filtersLength = {};
  let filterCount = 0;

  // eslint-disable-next-line
  for (let key in appliedFilters) {
    if (appliedFilters[key]) {
      filtersLength[`${key}Filters`] = appliedFilters[key].length;
      filterCount += appliedFilters[key].length;
    }
  }
  const inGridPlp = getPlpInGridRecommendation(state);
  const inMultiGrid = getPlpInMultiGridRecommendation(state);
  let productWithGrid = getProductsWithPromo(
    products,
    plpGridPromos,
    plpHorizontalPromo,
    filterCount,
    state,
    {},
    {
      loadedProductCount,
      totalProductsCount,
    }
  );

  if (inGridPlp.isSingleIngridEnabled === true) {
    productWithGrid = getProductsWithPromo(
      productWithGrid,
      plpGridPromos,
      plpHorizontalPromo,
      filterCount,
      state,
      inGridPlp
    );
  } else if (inMultiGrid.isMultiIngridEnabled === true) {
    productWithGrid = getProductsWithPromo(
      productWithGrid,
      plpGridPromos,
      plpHorizontalPromo,
      filterCount,
      state,
      inMultiGrid
    );
  }

  const filters = updateAppliedFiltersInState(state);

  return {
    products: productWithGrid,
    filters,
    currentNavIds: state.ProductListing && state.ProductListing.currentNavigationIds,
    categoryId: getCategoryId(state),
    navTree: getNavigationTreeApp(state),
    breadCrumbs: processBreadCrumbs(state.ProductListing && state.ProductListing.breadCrumbTrail),
    loadedProductCount,
    unbxdId: getUnbxdId(state),
    filtersLength,
    initialValues: {
      ...state.ProductListing.appliedFiltersIds,
    },
    labelsFilter: state.Labels && state.Labels.PLP && state.Labels.PLP.PLP_sort_filter,
    longDescription: getLongDescription(state),
    entityCategoryName: getEntityCategory(state),
    labels: getLabelsProductListing(state),
    labelsLogin: getLabelsAccountOverView(state),
    accessibilityLabels: getAccessibilityLabels(state),
    isLoadingMore: getIsLoadingMore(state),
    lastLoadedPageNumber: getLastLoadedPageNumber(state),
    isPlcc: isPlccUser(state),
    sortLabels: getSortLabels(state),
    scrollToTop: getScrollToTopValue(state),
    isKeepModalOpen: getModalState(state),
    isPickupModalOpen: getIsPickupModalOpen(state),
    totalProductsCount,
    isDataLoading: getIsDataLoading(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    labelsPlpTiles: labelsSelectors.getPlpTilesLabels(state),
    selectedFilterValue: getSelectedFilter(state),
    plpTopPromos: getPLPTopPromos(state),
    isKeepAliveEnabled: getIsKeepAliveProductApp(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    QRAnimationURL: get(state, 'Labels.global.qrScanner.lbl_animation_plp', ''),
    errorMessages: fetchErrorMessages(state),
    isOnModelAbTestPlp: getOnModelImageAbTestPlp(state),
    sort: getAppliedSortId(state),
    isShowPriceRangeKillSwitch: getIsShowPriceRangeForApp(state),
    showPriceRangeForABTest: getABTestIsShowPriceRange(state),
    isHidePLPAddToBag: getIsHidePLPAddToBag(state),
    isHidePLPRatings: getIsHidePLPRatings(state),
    userFavStoreId: getFavoriteStore(state),
    defaultWishListFromState: wishListFromState(state),
    isExternalCampaignFired: getExternalCampaignFired(state),
    changeFilterViewABTest: getChangeFilterViewABTest(state),
    slpLabels: getLabels(state),
    appCategoryCarouselL3: getABTestAppCategoryCarouselL3(state),
    isAbPlpPersonalized: getAbPlpPersonalized(state),
    abPlpPersonalizedCategories: getAbPlpPersonalizedCategories(state),
    isBrierleyPromoEnabled: getIsBrierleyPromoEnabled(state),
    deleteFavItemInProgressFlag: getDefaultFavItemInProgressFlag(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    isPLPCarouselEnabled: getIsPLPCarouselEnabled(state),
    genericCarousel: getGenericCarousel(state),
    showCarouselInspiteOfPromo: getShowCarouselFlag(state),
    categoryFilterArray: getCategoryFilterArray(state),
    isNewReDesignProductTile: getIsNewReDesignEnabled(state),
    firstTouchData: getCommonAnalyticsData(state),
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    getProducts: (payload) => {
      dispatch(getPlpProducts(payload));
    },

    setSelectedFilter: (payload) => {
      dispatch(setFilter(payload));
    },
    getMoreProducts: (payload) => {
      dispatch(getMorePlpProducts(payload));
    },
    addToCartEcom: () => {},
    addItemToCartBopis: () => {},
    resetProducts: () => {
      dispatch(resetPlpProducts());
    },
    onAddToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
    resetForm: () => {
      dispatch(reset('filter-form'));
    },
    setEssentialStep: (payload) => {
      dispatch(setEssentialShopStep(payload));
    },
    onQuickViewOpenClick: (payload) => {
      const title = (ownProps.navigation && ownProps.navigation.getParam('title')) || '';
      dispatch(setQuickViewProductDetailPageTitle(title));
      dispatch(openQuickViewWithValues(payload));
    },
    setExternalCampaignFired: (payload) => {
      dispatch(setExternalCampaignState(payload));
    },
    onSetLastDeletedItemIdAction: (payload) => {
      dispatch(setLastDeletedItemIdAction(payload));
    },
    setDeviceTierInState: (payload) => {
      dispatch(setDeviceTier(payload));
    },

    setCommonAnalyticsDataFirstTouch: (payload) => {
      dispatch(setCommonAnalyticsData(payload));
    },
    trackPageLoad: (payload, currentScreen) => {
      const {
        products,
        customEvents,
        searchType,
        departmentList,
        categoryList,
        listingCount,
        breadcrumb,
        subCategoryList,
        internalCampaignId,
        searchText,
        originType,
        supressProps,
        productsCount,
        storeId,
        externalCampaignId,
        omMID,
        omRID,
      } = payload;

      dispatch(
        setClickAnalyticsData({
          products,
          customEvents,
          searchType,
          departmentList,
          categoryList,
          listingCount,
          breadcrumb,
          subCategoryList,
          internalCampaignId,
          searchText,
          originType,
          supressProps,
          productsCount,
          storeId,
          ...(externalCampaignId && { externalCampaignId }),
          ...(omMID && { omMID }),
          ...(omRID && { omRID }),
        })
      );
      setTimeout(() => {
        dispatch(
          trackPageView({
            currentScreen,
            pageData: {
              ...payload,
            },
            props: {
              initialProps: {
                pageProps: {
                  pageData: {
                    ...payload,
                  },
                },
              },
            },
            supressProps,
          })
        );
      }, 100);
    },
    trackClickFilter: ({ name, module, contextData }) => {
      dispatch(
        setClickAnalyticsData({
          customEvents: ['events161'],
        })
      );
      dispatch(trackClick({ name, module, contextData }));
    },
  };
}

ProductListingContainer.propTypes = {
  getProducts: PropTypes.func.isRequired,
  getMoreProducts: PropTypes.func.isRequired,
  categoryId: PropTypes.string.isRequired,
  products: PropTypes.arrayOf(PropTypes.shape({})),
  currentNavIds: PropTypes.arrayOf(PropTypes.shape({})),
  navTree: PropTypes.shape({}),
  breadCrumbs: PropTypes.arrayOf(PropTypes.shape({})),
  filters: PropTypes.shape({}),
  totalProductsCount: PropTypes.string,
  filtersLength: PropTypes.shape({}),
  initialValues: PropTypes.shape({}),
  longDescription: PropTypes.string,
  navigation: PropTypes.shape({}).isRequired,
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  accessibilityLabels: PropTypes.shape({}),
  labelsFilter: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  isLoadingMore: PropTypes.bool,
  lastLoadedPageNumber: PropTypes.number,
  router: PropTypes.shape({}).isRequired,
  sortLabels: PropTypes.arrayOf(PropTypes.shape({})),
  resetProducts: PropTypes.func,
  onAddItemToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  labelsLogin: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  plpTopPromos: PropTypes.arrayOf(PropTypes.shape({})),
  isSearchListing: PropTypes.bool,
  isKeepModalOpen: PropTypes.bool,
  QRAnimationURL: PropTypes.string,
  isPlcc: PropTypes.bool,
  isDataLoading: PropTypes.bool,
  trackPageLoad: PropTypes.func,
  loadedProductCount: PropTypes.bool.isRequired,
  sort: PropTypes.string,
  selectedFilterValue: PropTypes.shape({}),
  onAddToFavorites: PropTypes.func,
  setEssentialStep: PropTypes.func.isRequired,
  userFavStoreId: PropTypes.string,
  defaultWishListFromState: PropTypes.func.isRequired,
  setSelectedFilter: PropTypes.func,
  isExternalCampaignFired: PropTypes.bool.isRequired,
  setExternalCampaignFired: PropTypes.func,
  changeFilterViewABTest: PropTypes.bool.isRequired,
  entityCategoryName: PropTypes.string.isRequired,
  isAbPlpPersonalized: PropTypes.bool,
  abPlpPersonalizedCategories: PropTypes.shape([]),
  isBrierleyPromoEnabled: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  categoryFilterArray: PropTypes.shape({}),
  isNewReDesignProductTile: PropTypes.bool,
  setDeviceTierInState: PropTypes.func,
};

ProductListingContainer.defaultProps = {
  products: [],
  currentNavIds: [],
  navTree: {},
  breadCrumbs: [],
  filters: {},
  totalProductsCount: '0',
  filtersLength: {},
  initialValues: {},
  longDescription: '',
  labels: {},
  accessibilityLabels: {},
  labelsFilter: {},
  isLoadingMore: false,
  lastLoadedPageNumber: 0,
  sortLabels: [],
  resetProducts: () => {},
  onAddItemToFavorites: null,
  isLoggedIn: false,
  labelsLogin: {},
  plpTopPromos: [],
  isSearchListing: false,
  isKeepModalOpen: false,
  QRAnimationURL: '',
  isPlcc: false,
  isDataLoading: false,
  trackPageLoad: () => {},
  sort: '',
  selectedFilterValue: {},
  onAddToFavorites: () => {},
  userFavStoreId: '',
  setSelectedFilter: () => {},
  setExternalCampaignFired: () => {},
  isAbPlpPersonalized: false,
  abPlpPersonalizedCategories: [],
  isBrierleyPromoEnabled: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isDynamicBadgeEnabled: {},
  categoryFilterArray: [],
  isNewReDesignProductTile: false,
  setDeviceTierInState: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductListingContainer);
export { ProductListingContainer as ProductListingContainerVanilla };
