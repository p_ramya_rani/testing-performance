// 9fbef606107a605d69c0edbcd8029e5d
import PRODUCTLISTINGPAGE_CONSTANTS from './ProductListing.constants';

export const setPlpProducts = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_PRODUCTS,
    payload,
  };
};

export const getPlpProducts = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.FETCH_PRODUCTS,
    payload,
  };
};

export const resetPlpProducts = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.RESET_PRODUCTS,
    payload,
  };
};

export const setFilter = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_FILTER,
    payload,
  };
};

export const getMorePlpProducts = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.GET_MORE_PRODUCTS,
    payload,
  };
};

export function setListingFirstProductsPage(productsPage) {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_FIRST_PRODUCTS_PAGE,
    productsPage,
  };
}

export const setPlpLoadingState = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_PLP_LOADING_STATE,
    payload,
  };
};

export const setPlpProductsDataOnServer = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_FIRST_PRODUCTS_PAGE_SSR,
    payload,
  };
};

export const setAddToFavorite = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_ADD_TO_FAVORITE,
    payload,
  };
};

export const setEssentialShopStep = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.ESSENTIAL_SHOP_STEP,
    payload,
  };
};

export const unsetPLPBreadCrumb = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.UNSET_PLP_BREADCRUMB,
    payload,
  };
};

export const setPlpScrollState = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_PLP_SCROLL_STATE,
    payload,
  };
};

export const loadCategoryPageSeoData = (payload) => {
  return {
    payload,
    type: PRODUCTLISTINGPAGE_CONSTANTS.LOAD_CATEGORY_PAGE_SEO_DATA,
  };
};

export const setCategoryPageSeoData = (payload) => {
  return {
    payload,
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_CATEGORY_PAGE_SEO_DATA,
  };
};

export const setDeviceTier = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_DEVICE_TIER,
    payload,
  };
};

export const setBopisFilterState = (payload) => {
  return {
    type: PRODUCTLISTINGPAGE_CONSTANTS.SET_BOPIS_FILTER_STATE,
    payload,
  };
};
