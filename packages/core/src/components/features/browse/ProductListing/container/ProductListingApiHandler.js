// 9fbef606107a605d69c0edbcd8029e5d
import layoutAbstractor from '@tcp/core/src/services/abstractors/bootstrap/layout/index';
import logger from '@tcp/core/src/utils/loggerInstance';
import { defaultBrand, defaultChannel, defaultCountry } from '@tcp/core/src/services/api.constants';
import ProductsOperator from './productsRequestFormatter';
import ProductAbstractor from '../../../../../services/abstractors/productListing';
import {
  getUserLoggedInState,
  isRememberedUser,
} from '../../../account/User/container/User.selectors';
import { getDefaultWishList } from '../../Favorites/container/Favorites.saga';
import { getUrlPageNumber } from './ProductListing.util';

const instanceProductListing = new ProductAbstractor();
const operatorInstance = new ProductsOperator();

const getListingPageType = (navigationData, pathname) =>
  navigationData &&
  navigationData.find(item => {
    if (item.categoryContent) {
      const urlParts = pathname.split('/');
      const path = urlParts[urlParts.length - 1];
      return path === item.categoryContent.asPath;
    }
    return false;
  });

const isCustomInfo = state => {
  const isGuest = !getUserLoggedInState(state);
  const isRemembered = isRememberedUser(state);
  return !isGuest && !isRemembered;
};

/* eslint-disable sonarjs/cognitive-complexity */
// eslint-disable-next-line complexity
export const fetchPlpProductsInfo = async ({
  navigationData,
  location: locationObj,
  formData,
  sortBySelected,
  state,
  productCount,
  apiConfig,
}) => {
  let reqObj = {};
  let layout;
  let modules;
  let res = {};
  let pageName;
  let isBucketing = false;

  const locationInfo = getUrlPageNumber(locationObj.pathname);
  const location = { ...locationObj, ...locationInfo };

  if (!getListingPageType(navigationData, location.pathname)) {
    reqObj = operatorInstance.getProductListingBucketedData(
      { ...state, Navigation: { navigationData } },
      location,
      sortBySelected,
      formData,
      1,
      null,
      productCount,
      0
    );

    res = {};

    let plpCurrentNavigationIds;
    pageName = 'productListingPage';
    if (reqObj.isFetchFiltersAndCountReq) {
      res = await instanceProductListing.getProducts(
        { ...reqObj, location },
        { ...state, Navigation: { navigationData } }
      );
      plpCurrentNavigationIds = res.currentNavigationIds;
      ({ layout, modules } = await instanceProductListing.parsedModuleData(
        res.bannerInfo,
        apiConfig
      ));
      reqObj = operatorInstance.processProductFilterAndCountData(
        res,
        { ...state, Navigation: { navigationData } },
        reqObj
      );
      isBucketing = true;
    }
    if (reqObj.categoryId) {
      if (isCustomInfo({ ...state, Navigation: { navigationData } })) {
        await getDefaultWishList();
      }
      const plpProducts = await instanceProductListing.getProducts(
        { ...reqObj, location, filterMaps: res.filterMaps || res.filtersMaps },
        { ...state, Navigation: { navigationData } }
      );
      if (plpProducts) {
        if (Object.keys(plpProducts.bannerInfo).length === 0) {
          plpProducts.bannerInfo = res && res.bannerInfo;
        }
        if (!isBucketing) {
          ({ layout, modules } = await instanceProductListing.parsedModuleData(
            plpProducts.bannerInfo,
            apiConfig
          ));
        } else {
          plpProducts.currentNavigationIds = plpCurrentNavigationIds;
        }
        const updatedBucketingConfig = operatorInstance.updateBucketingConfig(plpProducts);
        delete updatedBucketingConfig.navigationTree;
        return {
          apiRes: {
            processedPlpProducts: plpProducts,
            reqObj,
            bucketingConfig: updatedBucketingConfig,
          },
          reqObj,
          res: plpProducts,
          layout,
          modules,
          pageName,
        };
      }
    }
  } else {
    try {
      pageName = 'categoryListingPage';
      const { language } = apiConfig;
      const layoutParams = {
        page: 'boy',
        layoutName: pageName,
        brand: (apiConfig && apiConfig.brandIdCMS) || defaultBrand,
        channel: defaultChannel,
        country: (apiConfig && apiConfig.siteIdCMS) || defaultCountry,
      };
      const layoutData = await layoutAbstractor.getLayoutData(layoutParams);
      const [layoutItem] = layoutData.items;
      ({ layout } = layoutItem);
      modules = await layoutAbstractor.getModulesFromLayout(layoutData, language);
    } catch (e) {
      logger.error(e);
    }
  }
  return new Promise(resolve =>
    resolve({
      apiRes: {
        processedPlpProducts: res,
      },
      reqObj,
      res,
      layout,
      modules,
      pageName,
    })
  );
};
/* eslint-disable sonarjs/cognitive-complexity */

export default fetchPlpProductsInfo;
