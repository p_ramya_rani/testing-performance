// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable extra-rules/no-commented-out-code */
// import { routingStoreView } from 'reduxStore/storeViews/routing/routingStoreView.js';
// import { generalStoreView } from 'reduxStore/storeViews/generalStoreView';
import { findCategoryIdandName, matchPath } from './ProductListing.util';
// import { getSessionStorage } from 'util/sessionStorageManagement';
import { PRODUCTS_PER_LOAD } from './ProductListing.constants';
// import PAGES from '../../../../../constants/pages.constants';
import { isMobileApp } from '../../../../../utils';

class BucketingBL {
  /**
   * @function getUpdatedL3 We get the avaialble L3 in the L2 which has been clicked in the L2 call response. If the user has selected the filters then
   *           then the available L3 changes on the basis of th filters applied. We need to replce our cached L3 with the updated ones.
   * @param {Array} l3ReturnedByL2 The L3 which have been returned by L2 call.
   */
  getUpdatedL3 = (l3ReturnedByL2, availableL3) => {
    const updatedAvailableL3 = [];
    // Looping over the cached L3
    // eslint-disable-next-line
    if (l3ReturnedByL2 && l3ReturnedByL2.length) {
      availableL3.forEach(item => {
        const itm = { ...item };
        // We need to check if the L3 in loop matches with any L3 in the list got in L2 response. If it does then we will push it in a tempArray.
        // and then replcae it with cached L3 left and available L3. We are doing this beacuse, yes it is true that we need to make calls for all those
        // L3 which we recevied in L2 response but we need to make in the sequence which is there in Taxanomy.
        for (let idx = 0; idx < l3ReturnedByL2.length; idx += 1) {
          if (itm.categoryId === l3ReturnedByL2[idx].id && itm.displayToCustomer) {
            itm.count = l3ReturnedByL2[idx].count;
            updatedAvailableL3.push(itm);
          }
          if (
            itm.categoryContent &&
            itm.categoryContent.id === l3ReturnedByL2[idx].id &&
            itm.categoryContent.displayToCustomer
          ) {
            itm.count = l3ReturnedByL2[idx].count;
            updatedAvailableL3.push(itm);
          }
        }
      });
    }
    return updatedAvailableL3;
  };

  /**
   * @funtion updateBucketingParamters This function updates the start and the products to be fetched , after an L3 call is successfull.
   * @param {Object} res The response object of L3 Call.
   */
  updateBucketingParamters = (res, bucketingConfig) => {
    const temp = { ...bucketingConfig };
    const { productsToFetchPerLoad, productsFetchedInPreviousCall } = temp;
    const { start } = temp;
    const productsFecthedTillNow =
      start + (productsFetchedInPreviousCall || productsToFetchPerLoad);
    const productsLeft = res.productsInCurrCategory - productsFecthedTillNow;
    // The start for the next call will be the products which have been fetched till now.
    temp.start = productsFecthedTillNow;
    // If the products be fetched are less than what are left , then we will fetch the remaining products in the next L3 call.
    if (productsToFetchPerLoad > productsLeft) {
      temp.productsToFetchPerLoad = productsLeft;
    }
    // If no products are left in an L3 means all the products have been fetched, then we need to reset the start to 0 again and we need to remove
    // the exhausted category from the L3left variable so that next time the next category with start 0 is called.
    if (productsLeft <= 0) {
      temp.start = 0;
      temp.productsToFetchPerLoad = PRODUCTS_PER_LOAD;
      temp.L3Left.splice(0, 1);
    }
    return temp;
  };

  /**
   * @function doBucketingLogic This function does the logic work needed for bucketing
   */

  getMatchPath = (isSearchPage, location) => {
    const categoryParam = isMobileApp() ? '/c?cid=' : '/c/';
    const params = isSearchPage ? '/search/' : categoryParam;
    return matchPath(location.pathname, params);
  };

  // TODO: need to check why some of the lines are commented in the existing code
  // eslint-disable-next-line complexity
  doBucketingLogic = (
    location = '',
    state,
    bucketingConfig,
    sortBySelected,
    filterAndSortParam,
    callback,
    productCount
  ) => {
    const temp = {};
    const bucketingConfigTemp = { ...bucketingConfig };
    temp.isSearchPage = false;
    const match = this.getMatchPath(temp.isSearchPage, location);
    temp.categoryKey = temp.isSearchPage ? match.searchTerm : match.listingKey;
    temp.navigationTree = state.Navigation.navigationData;
    // categoryNameList is an array of the categories. Eg if the click has happened over L2 which is boys -> Denim. Then categoryNameList will
    // be [{category information of boys}, {category information of denim}].
    temp.categoryNameList = findCategoryIdandName(temp.navigationTree, temp.categoryKey).reverse();

    if (
      !temp.categoryNameList.length &&
      typeof window !== 'undefined' &&
      window.PLP_CATEGORY_NAVIGATION_INFO
    ) {
      try {
        temp.categoryNameList = JSON.parse(window.PLP_CATEGORY_NAVIGATION_INFO);
      } catch (e) {
        console.log(e);
      }
    }
    // eslint-disable-next-line
    temp.clickedL2 = temp.categoryNameList[1];
    bucketingConfigTemp.currL2NameList = [...temp.categoryNameList];
    bucketingConfigTemp.bucketingSeqScenario = false;
    // Checking if the user has clicked on sort by options.
    if (sortBySelected) {
      temp.sortingAvailable = filterAndSortParam.sort;
      // setting the start to 0. We send this in UNBXD call to tell them from which index do we need to fetch the products.
      bucketingConfigTemp.start = 0;
      bucketingConfigTemp.productsToFetchPerLoad = productCount || PRODUCTS_PER_LOAD;
      temp.filtersAndSort = filterAndSortParam;
    } else {
      temp.filtersAndSort = (location && callback(location.search)) || {};
      temp.sortingAvailable = temp.filtersAndSort.sort;
    }
    const isSearchPage = false;
    const isSearch = match.searchTerm || match.listingKey;
    const searchTerm = decodeURIComponent(isSearch);
    temp.isOutfitPage = !isSearchPage && searchTerm && searchTerm.indexOf('-outfit') > -1;
    return { ...temp, ...bucketingConfigTemp };
  };

  /**
   * @function fetchPendingCallStack This function make a stack of all the pending promises which needs to be completed to fetch the products
   *                                 according to the cached count. There is one limitation of UNBXD that we cannot order more than 100 products
   *                                 over a single call. Now suppose one L3 is having 106 products to be fetched. We will make two calls for this L3.
   *                                 One for 100 products and other for 6 products. We are making all the calls in parallel.
   */

  fetchPendingCallStack = (
    totalCount,
    MaxProducts,
    pendingPromisesStack,
    filtersAndSort,
    start,
    categoryPathMap,
    callback,
    location,
    catNameL3
    // eslint-disable-next-line
  ) => {
    const countToFetch = totalCount < MaxProducts ? totalCount : MaxProducts;
    pendingPromisesStack.push(
      callback(filtersAndSort, '', location, start, countToFetch, categoryPathMap, catNameL3)
    );
    const countLeft = totalCount - countToFetch;
    // Checking of there is still some products left of the L3 to be fetched. if yes then recursively call this function to fetch all the products.
    if (countLeft > 0) {
      return this.fetchPendingCallStack(
        countLeft,
        MaxProducts,
        pendingPromisesStack,
        filtersAndSort,
        countToFetch,
        categoryPathMap,
        callback,
        location,
        catNameL3
      );
    }
    return pendingPromisesStack;
  };
}

export default BucketingBL;
