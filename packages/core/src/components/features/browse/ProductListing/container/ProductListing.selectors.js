/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import isEmpty from 'lodash/isEmpty';
import toLower from 'lodash/toLower';
import { isMobileApp } from '@tcp/core/src/utils';
import {
  getCategoryFilters,
  findCategoryIdandName,
} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import { isTier2Device, isTier3Device } from '@tcp/web/src/utils/device-tiering';
import { findCategoryFilter } from '@tcp/core/src/components/features/browse/ProductListing/container/PlpCarousel.utils';
import { TIER2, TIER3 } from '@tcp/core/src/constants/tiering.constants';
import { generateGroups, getUrl } from './ProductListing.util';
import {
  getAPIConfig,
  flattenArray,
  getLabelValue,
  getABtestFromState,
  parseBoolean,
} from '../../../../../utils';
import { getIsGuest, isPlccUser } from '../../../account/User/container/User.selectors';
import { PRODUCT_LISTING_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import { PRODUCTS_PER_LOAD, PRODUCTS_PER_LOAD_LOWER_END_DEVICES } from './ProductListing.constants';
import { FACETS_FIELD_KEY } from '../../../../../services/abstractors/productListing/productListing.utils';

const getProductListingState = (state) => {
  return state[PRODUCT_LISTING_REDUCER_KEY];
};

const getNavigationState = (state) => {
  return state && state.Navigation && state.Navigation.navigationData;
};

const getOrganizedHeaderNavigationTree = (unorganizedTree) => {
  // Only in browser memory, will be cleaned on page re-fresh
  // if (cachedOrganizedNavTree) {
  //   return cachedOrganizedNavTree;
  // }
  const organizedNav =
    unorganizedTree &&
    unorganizedTree.map((L1) => {
      return {
        ...L1,
        menuGroupings: generateGroups(L1),
      };
    });
  // only on browser so we dont need to keep deriving this
  if (/* isClient() && */ organizedNav && organizedNav.length) {
    // TODO - fix this - cachedOrganizedNavTree = organizedNav;
  }
  return organizedNav;
};

const getStateLabels = (state) => state.Labels;

export const getCurrentListingIds = createSelector(
  getProductListingState,
  (products) => products && products.currentNavigationIds
);

const findInL2 = (currentCategory, subCategories) => {
  const subGroups = subCategories ? Object.keys(subCategories) : [];
  return subGroups.find((group) =>
    subCategories[group].items.find((L2) => L2 && L2.asPath === currentCategory)
  );
};

const findInL3 = (currentCategory, subCategories) => {
  const subGroups = subCategories ? Object.keys(subCategories) : [];
  return subGroups.find((group) =>
    subCategories[group].items.find(
      (L2) =>
        L2.subCategories && L2.subCategories.find((item) => item && item.asPath === currentCategory)
    )
  );
};

export const getNavigationTree = (navigationData, url) => {
  const { pathname } = getUrl(url);
  if (!pathname) {
    return null;
  }
  const currentCategory = `/${pathname.split('/').slice(2).join('/')}`;
  const navTree = getOrganizedHeaderNavigationTree(navigationData);
  if (!navTree) {
    return navTree;
  }

  return (
    navTree.find(
      (l1) => l1 && l1.categoryContent && l1.categoryContent.asPath === currentCategory
    ) ||
    navTree.find((l1) => findInL2(currentCategory, l1.subCategories)) ||
    navTree.find((l1) => findInL3(currentCategory, l1.subCategories))
  );
};

export const getNavigationTreeApp = (state) => {
  const currentListingIds = getCurrentListingIds(state);
  const navigationData = getNavigationState(state);
  const navTree = getOrganizedHeaderNavigationTree(navigationData);

  return (
    currentListingIds &&
    currentListingIds[0] &&
    navTree &&
    navTree.find((L1) => L1.categoryId === currentListingIds[0])
  );
};

export const getBreadCrumbTrail = createSelector(
  getProductListingState,
  (products) => products && products.breadCrumbTrail
);

export const getIsSearchPage = createSelector(
  getProductListingState,
  (ProductListing) => ProductListing && ProductListing.isSearch
);
export const getSelectedFilter = createSelector(
  getProductListingState,
  (products) => products && products.selectedFilter
);
export const getProductsSelect = createSelector(
  getProductListingState,
  (products) => products && products.loadedProductsPages && products.loadedProductsPages[0]
);
export const getAllProductsSelect = createSelector(getProductListingState, (products) => {
  const allProducts = products && products.loadedProductsPages;
  return allProducts && flattenArray(allProducts);
});
export const getLabels = (state) => {
  return state.Labels.SLPs && state.Labels.SLPs.SLPs_Sub;
};
export const getAccessibilityLabels = createSelector(getStateLabels, (stateLabels) => {
  return {
    lbl_tick_icon: getLabelValue(stateLabels, 'lbl_tick_icon', 'accessibility', 'global'),
    lbl_plus_icon: getLabelValue(stateLabels, 'lbl_plus_icon', 'accessibility', 'global'),
    previousPrice: getLabelValue(stateLabels, 'lbl_previous_price', 'accessibility', 'global'),
    lbl_right_arrow: getLabelValue(stateLabels, 'lbl_right_arrow', 'accessibility', 'global'),
  };
});
export const get404Labels = createSelector(getStateLabels, (stateLabels) => {
  return {
    header404TCP_PLP: getLabelValue(stateLabels, 'lbl_404_heading_tcp', 'SLPs_Sub', 'SLPs'),
    subHeader404TCP_PLP: getLabelValue(stateLabels, 'lbl_404_sub_header_tcp', 'SLPs_Sub', 'SLPs'),
    header404GYM_PLP: getLabelValue(stateLabels, 'lbl_404_heading_gym', 'SLPs_Sub', 'SLPs'),
    subHeader404GYM_PLP: getLabelValue(stateLabels, 'lbl_404_sub_header_gym', 'SLPs_Sub', 'SLPs'),
  };
});
export const getTotalProductsCount = createSelector(
  getProductListingState,
  (products) => products && products.totalProductsCount
);

export const getAppliedFilters = createSelector(
  getProductListingState,
  (products) => products && products.appliedFiltersIds
);

export const getIsBopisFilterOn = createSelector(
  getProductListingState,
  (products) => products && products.isBopisFilterOn
);

export const getAppliedSortId = createSelector(
  getProductListingState,
  (products) => products && products.appliedSortId
);

export const getLoadedProductsCount = createSelector(getProductListingState, (products) => {
  const allProducts = products && products.loadedProductsPages;
  const totalProductCount =
    (allProducts && allProducts.reduce((sum, item) => item && item.length + sum, 0)) || 0;
  return totalProductCount || 0;
});

export const getLongDescription = createSelector(
  getProductListingState,
  (ProductListing) => ProductListing && ProductListing.currentListingDescription
);

export const getSeoHeaderCopytext = createSelector(
  getProductListingState,
  (ProductListing) => ProductListing && ProductListing.seoHeaderCopy
);

export const getSeoTitle = createSelector(
  getProductListingState,
  (ProductListing) => ProductListing && ProductListing.seoTitle
);

export const getSeoMetaDesc = createSelector(
  getProductListingState,
  (ProductListing) => ProductListing && ProductListing.seoMetaDesc
);

export const getCanonicalUrl = createSelector(
  getProductListingState,
  (ProductListing) => ProductListing && ProductListing.canonicalUrl
);

export const getUnbxdId = createSelector(
  getProductListingState,
  (products) => products && products.unbxdId
);

export const getLoadedProductsPages = createSelector(
  getProductListingState,
  (products) => products && products.loadedProductsPages
);

export const getProductsFilters = createSelector(
  getProductListingState,
  (products) => products && (products.filtersMaps || {})
);
export const getLabelsProductListing = createSelector(getStateLabels, (stateLabels) => {
  if (!stateLabels || !stateLabels.PLP || !stateLabels.global)
    return {
      addToBag: '',
      readMore: '',
      readLess: '',
      shopCollection: '',
      shopthelook: '',
      youMayAlsoLike: '',
      recentlyViewed: '',
      interimSoldout: '',
      shopnow: '',
      clickHere: '',
      pointsAndRewardsText: '',
      plpPersonalized: '',
      inGridHeader: '',
      inGridDescription: '',
      inMultiGridHeader: '',
      inMultiGridDescription: '',
      plpSelectStore: '',
      pickupAt: '',
      plpChangeStore: '',
      plpSelectNearbyStore: '',
      pickupInStore: '',
    };
  const {
    PLP: {
      plpTiles: {
        lbl_add_to_bag: addToBag,
        lbl_plpTiles_shop_collection: shopCollection,
        lbl_plpTiles_shop_the_look: shopthelook,
        lbl_page_not_found: pageNotFound,
        lbl_you_may_also_like: youMayAlsoLike,
        lbl_recently_viewed: recentlyViewed,
        lbl_plp_personalized: plpPersonalized,
        lbl_plp_in_grid_header: inGridHeader,
        lbl_plp_in_grid_description: inGridDescription,
        lbl_plp_in_multi_grid_header: inMultiGridHeader,
        lbl_plp_in_multi_grid_description: inMultiGridDescription,
        lbl_plp_select_store: plpSelectStore,
        lbl_plp_select_at: pickupAt,
        lbl_plp_change_store: plpChangeStore,
        lbl_plp_select_nearby_store: plpSelectNearbyStore,
        lbl_plp_pickup_in_store: pickupInStore,
      },
      seoText: { lbl_read_more: readMore, lbl_read_less: readLess },
    },
    global: {
      CommonBrowseLabels: {
        lbl_category_interimSoldout: interimSoldout,
        lbl_bopis_interimSoldoutMessage: interimSoldoutMessage,
        lbl_common_shopNow: shopnow,
        lbl_category_clickHere: clickHere,
        lbl_brierley_promo_text_generic: pointsAndRewardsText,
        lbl_plp_carousel_default_title: plpGenericCarouselDefaultTitle,
        lbl_plp_shop_all: shopAllLabel,
        lbl_tcp: tcpBrand,
        lbl_gym: gymBrand,
      },
    },
  } = stateLabels;

  return {
    addToBag,
    readMore,
    readLess,
    shopCollection,
    shopthelook,
    pageNotFound,
    youMayAlsoLike,
    recentlyViewed,
    interimSoldout,
    interimSoldoutMessage,
    shopnow,
    clickHere,
    pointsAndRewardsText,
    plpPersonalized,
    inGridHeader,
    inGridDescription,
    inMultiGridHeader,
    inMultiGridDescription,
    plpGenericCarouselDefaultTitle,
    shopAllLabel,
    tcpBrand,
    gymBrand,
    plpSelectStore,
    pickupAt,
    plpChangeStore,
    plpSelectNearbyStore,
    pickupInStore,
  };
});

const getShopAllLabel = (state) => {
  const labels = getLabelsProductListing(state);
  return labels && labels.shopAllLabel;
};

export const getLabelsOutOfStock = createSelector(getStateLabels, (stateLabels) => {
  return {
    outOfStockCaps: getLabelValue(
      stateLabels,
      'lbl_common_outOfStockCaps',
      'CommonBrowseLabels',
      'global'
    ),
    itemSoldOutMessage: getLabelValue(
      stateLabels,
      'lbl_common_itemSoldOut',
      'CommonBrowseLabels',
      'global'
    ),
  };
});

export const getLabelsAccountOverView = (state) => {
  if (!state.Labels || !state.Labels.account) return { logIn: {} };
  const {
    account: {
      accountOverview: { lbl_overview_login_text: logIn },
    },
  } = state.Labels;

  return {
    logIn,
  };
};

export const getIsLoadingMore = (state) => {
  return state.ProductListing.isLoadingMore;
};

export const getShouldPlpScroll = (state) => {
  return state.ProductListing && state.ProductListing.shouldPlpScroll;
};

export const getEntityCategory = (state) => {
  return state.ProductListing.entityCategory;
};

export const getScrollToTopValue = (state) => {
  return state.ProductListing.isScrollToTop;
};

export const getModalState = (state) => {
  return state.ProductListing.isKeepModalOpen;
};

export const getProductsInCurrCategory = (state) => {
  return state.ProductListing.productsInCurrCategory;
};

export const getSpotlightReviewsUrl = () => {
  return getAPIConfig().BAZAARVOICE_SPOTLIGHT;
};

export const getBazaarvoiceApiUrl = () => {
  return getAPIConfig().BAZAARVOICE_REVIEWS;
};

export const getCategoryId = (state) => {
  const currentNavigationIds = state.ProductListing && state.ProductListing.currentNavigationIds;
  return currentNavigationIds && currentNavigationIds[currentNavigationIds.length - 1];
};

const getPageSize = (state) => {
  let isLowerEndDevice = false;
  if (isMobileApp()) {
    isLowerEndDevice =
      state.ProductListing.deviceTier === TIER2 || state.ProductListing.deviceTier === TIER3;
  } else {
    isLowerEndDevice = isTier2Device() || isTier3Device();
  }
  return isLowerEndDevice ? PRODUCTS_PER_LOAD_LOWER_END_DEVICES : PRODUCTS_PER_LOAD;
};

export const getLastLoadedPageNumber = (state) => {
  // note that we do not assume all pages have the same size, to protect against BE returning less products then requested.
  return Math.ceil(getLoadedProductsCount(state) / getPageSize(state));
};

/**
 * @function updateAppliedFiltersInState
 * matches filterMaps with appliedFilterIds
 * and updates isSelected state in filters
 *
 * @param {*} state
 * @returns
 */
export const updateAppliedFiltersInState = (state) => {
  const filters = getProductsFilters(state);
  const appliedFilters = getAppliedFilters(state);
  const filterEntries = (filters && Object.entries(filters)) || [];
  if (appliedFilters && Object.keys(appliedFilters).length) {
    filterEntries.map((filter) => {
      const key = filter[0];
      const values = filter[1];
      const appliedFilterValue = appliedFilters[key] || [];
      if (!(values instanceof Array)) return values;

      // for all arrays in filters - update isSelected as true if it is present in appliedFilterIds
      values.map((value) => {
        const isValueApplied = appliedFilterValue.filter((id) => id === value.id).length > 0;
        const updatedValue = value;
        updatedValue.isSelected = isValueApplied;
        return updatedValue;
      });
      return values;
    });
  }
  return filters;
};

export const isFiltersAvailable = (filterMaps) => {
  const filterKeys =
    filterMaps && filterMaps.unbxdDisplayName && Object.keys(filterMaps.unbxdDisplayName);
  return (
    filterKeys &&
    filterKeys
      .filter((filter) => {
        return filter !== FACETS_FIELD_KEY.aux_color_unbxd;
      })
      .some((facets) => {
        return filterMaps[facets].length > 0;
      })
  );
};

export const getIsFilterBy = (state) => {
  const filterMaps = state.ProductListing.filtersMaps;
  return isFiltersAvailable(filterMaps);
};

// to get redux page url
export const getPageUrl = (state) => {
  return state.ProductListing.pageUrl;
};

export const getIsDataLoading = (state) => {
  return state.ProductListing.isDataLoading;
};

const getTopPromosState = (state) => {
  const { productListingPage: { top: topPromos } = {} } = state.Layouts;
  return topPromos;
};

const getLoyaltyPromosState = (state) => {
  const { productListingPage: { loyaltyBanner: loyaltyPromo } = {} } = state.Layouts;
  return loyaltyPromo;
};

const getGridPromoState = (state) => {
  const { productListingPage: { grid: gridPromo } = {} } = state.Layouts;
  return gridPromo;
};

const getHorizontalPromoState = (state) => {
  const { productListingPage: { horizontal: horizontalPromo } = {} } = state.Layouts;
  return horizontalPromo;
};

export const getModulesState = (state) => {
  return state.Modules;
};

const flattenPromoSet = (promo) => {
  const promoSet = {};
  if (promo.set && Array.isArray(promo.set)) {
    promo.set.forEach((i) => {
      promoSet[i.key] = i.val;
    });
  }
  return promoSet;
};

const getPLPIngridPromoCondition = (promoItem, isGuest, isPlcc) => {
  const { guest, mpr, plcc } = flattenPromoSet(promoItem);

  return (guest && isGuest) || (plcc && isPlcc) || (!isGuest && !isPlcc && mpr);
};

const getPromoModuleInfo = ({ promoItem, modules, type }) => {
  let promoModuleInfo = (promoItem.contentId && modules[promoItem.contentId]) || {};
  if (type === 'grid' || type === 'horizontal') {
    promoModuleInfo = { ...promoModuleInfo, slot: promoItem && promoItem.slotNo };
  }
  return promoModuleInfo;
};

const getUserBasedPLPPromo = ({ promoItem, modules, isGuest, isPlcc, type, userTypeObj }) => {
  let userBasedPLPPromo;
  const promoModuleInfo = getPromoModuleInfo({ promoItem, modules, type });
  switch (toLower(promoItem.name)) {
    case 'mpr':
      userBasedPLPPromo = !isGuest && !isPlcc && { ...promoModuleInfo };
      break;
    case 'plcc':
      userBasedPLPPromo = isPlcc && { ...promoModuleInfo };
      break;
    case 'guest':
      userBasedPLPPromo = ((!userTypeObj?.hasPlcc && isPlcc) ||
        (!userTypeObj?.hasMpr && !isGuest && !isPlcc) ||
        isGuest) && { ...promoModuleInfo };
      break;
    default:
      userBasedPLPPromo = {};
  }
  return userBasedPLPPromo;
};

const findIfHasPlccOrMpr = (promos) => {
  const userTypeObj = {
    hasPlcc: false,
    hasMpr: false,
  };
  // eslint-disable-next-line no-unused-expressions
  promos?.forEach((promo) => {
    if (promo?.slotNo) {
      if (promo?.name === 'mpr') {
        userTypeObj.hasMpr = true;
      } else if (promo.name === 'plcc') {
        userTypeObj.hasPlcc = true;
      }
    }
  });
  return userTypeObj;
};

export const getPLPTopPromos = createSelector(
  getTopPromosState,
  getLoyaltyPromosState,
  getModulesState,
  getIsGuest,
  isPlccUser,
  (topPromos, loyaltyPromo, modules, isGuest, isPlcc) => {
    const loyaltyPromos =
      (loyaltyPromo &&
        loyaltyPromo.map((loyalPromo) => {
          const loyalPromoModule = loyalPromo.contentId && modules[loyalPromo.contentId];
          if (loyalPromoModule) {
            loyalPromoModule.userType = loyalPromo.name;
          }
          return loyalPromoModule || {};
        })) ||
      [];

    const userTypeObj = findIfHasPlccOrMpr(topPromos);
    const promos =
      (topPromos &&
        topPromos.map((promoItem) => {
          if (promoItem.slotNo) {
            return getUserBasedPLPPromo({ promoItem, modules, isGuest, isPlcc, userTypeObj });
          }
          if (getPLPIngridPromoCondition(promoItem, isGuest, isPlcc)) {
            return {};
          }
          return (promoItem.contentId && modules[promoItem.contentId]) || {};
        })) ||
      [];
    return loyaltyPromos.concat(promos).filter((item) => !isEmpty(item));
  }
);

export const getPLPGridPromos = createSelector(
  getGridPromoState,
  getModulesState,
  getIsGuest,
  isPlccUser,
  (gridPromo, modules, isGuest, isPlcc) => {
    const userTypeObj = findIfHasPlccOrMpr(gridPromo);
    return (
      (gridPromo &&
        gridPromo.map((promoItem) => {
          const moduleInfo = (promoItem.contentId && modules[promoItem.contentId]) || {};
          if (promoItem.slotNo) {
            return getUserBasedPLPPromo({
              promoItem,
              modules,
              isGuest,
              isPlcc,
              type: 'grid',
              userTypeObj,
            });
          }
          if (getPLPIngridPromoCondition(promoItem, isGuest, isPlcc)) {
            return {};
          }
          return { ...moduleInfo, slot: promoItem && promoItem.name };
        })) ||
      []
    ).filter((item) => !isEmpty(item));
  }
);

export const getPlpHorizontalPromo = createSelector(
  getHorizontalPromoState,
  getModulesState,
  getIsGuest,
  isPlccUser,
  (horizontalPromo, modules, isGuest, isPlcc) => {
    const userTypeObj = findIfHasPlccOrMpr(horizontalPromo);
    return (
      (horizontalPromo &&
        horizontalPromo.map((promoItem) => {
          const horizontalModuleInfo = (promoItem.contentId && modules[promoItem.contentId]) || {};
          if (promoItem.slotNo) {
            return getUserBasedPLPPromo({
              promoItem,
              modules,
              isGuest,
              isPlcc,
              type: 'horizontal',
              userTypeObj,
            });
          }
          if (getPLPIngridPromoCondition(promoItem, isGuest, isPlcc)) {
            return {};
          }
          return { ...horizontalModuleInfo, slot: promoItem && promoItem.name };
        })) ||
      []
    ).filter((item) => !isEmpty(item));
  }
);

export const getIsESModuleLoading = (state) => {
  return state.ProductListing.isESModuleLoading;
};

export const getIsHidePLPRatings = (state) => {
  return (
    getABtestFromState(state.AbTest) &&
    parseBoolean(getABtestFromState(state.AbTest).isHidePLPRatings)
  );
};

export const getIs404AbTest = (state) => {
  return (
    getABtestFromState(state.AbTest) &&
    getABtestFromState(state.AbTest).abTest404Experience &&
    parseBoolean(getABtestFromState(state.AbTest).abTest404Experience[0].is404AbTest)
  );
};

export const get404RedirectExperience = (state) => {
  return (
    getABtestFromState(state.AbTest) &&
    getABtestFromState(state.AbTest).abTest404Experience &&
    getABtestFromState(state.AbTest).abTest404Experience[1]
  );
};

export const getAbPlpPersonalized = (state) => {
  return (
    getABtestFromState(state.AbTest) &&
    parseBoolean(getABtestFromState(state.AbTest).PLP_PERSONALIZED)
  );
};

export const getAbInGridPlp = (state) => {
  return (
    getABtestFromState(state.AbTest) &&
    getABtestFromState(state.AbTest).IN_GRID_PLP &&
    parseBoolean(getABtestFromState(state.AbTest).IN_GRID_PLP.isEnabled)
  );
};

export const getNewPLPExperienceAbTest = (state) => {
  return getABtestFromState(state.AbTest) && getABtestFromState(state.AbTest).newPLPExperience;
};

export const getAbPlpPersonalizedCategories = (state) => {
  return typeof getABtestFromState(state.AbTest).PLP_PERSONALIZED_CATEGORIES === 'object'
    ? getABtestFromState(state.AbTest).PLP_PERSONALIZED_CATEGORIES
    : [];
};

export const getAbInGridPlpCategories = (state) => {
  const AbTest = getABtestFromState(state.AbTest);
  if (AbTest && AbTest.IN_GRID_PLP && typeof AbTest.IN_GRID_PLP.categories === 'object')
    return AbTest.IN_GRID_PLP.categories;
  return [];
};

export const getOnModelImageAbTestPlp = (state) => {
  return getABtestFromState(state.AbTest).enableOnModelImageOnPLP;
};

export const getIsHidePLPAddToBag = (state) =>
  getABtestFromState(state.AbTest) &&
  parseBoolean(getABtestFromState(state.AbTest).isHidePLPAddToBag);

export const getCategoryNavigationInfo = (state) => {
  return state.Navigation.categoryNavigationInfo;
};

export const getIsShowProductsOnL1 = (state) => {
  return getABtestFromState(state.AbTest).isShowProductsOnL1;
};

export const getExternalCampaignFired = (state) => {
  const { PageLoader } = state;
  return PageLoader && PageLoader.isExternalCampaignFired;
};

const getCurrentListingSeoKey = (state) => {
  return state && state.ProductListing && state.ProductListing.currentListingSeoKey;
};

export const getCategoryFilterArray = createSelector(
  getCurrentListingSeoKey,
  getNavigationState,
  (categoryKey, navigationState) => {
    if (categoryKey) {
      const categoryNameList = findCategoryIdandName(navigationState, categoryKey).reverse();
      return getCategoryFilters(categoryNameList);
    }
    return [];
  }
);

export const getGenericCarousel = createSelector(
  getCurrentListingSeoKey,
  getNavigationState,
  getShopAllLabel,
  (categoryKey, navigationState, shopAllLabel) => {
    const categoryCarouselList = categoryKey
      ? findCategoryFilter(navigationState, categoryKey, shopAllLabel).reverse()
      : [];

    let carouselObj = null;
    if (categoryCarouselList.length > 2) {
      carouselObj = categoryCarouselList[2].carouselData;
    }
    if (categoryCarouselList.length > 1 && !carouselObj) {
      carouselObj = categoryCarouselList[1].carouselData;
    }
    const carouselTitle =
      categoryCarouselList.length > 1 ? categoryCarouselList[1].carouselTitle || '' : '';
    return { carouselData: carouselObj, carouselTitle };
  }
);

export const getFilterAppliedState = createSelector(
  getProductListingState,
  (products) => products && products.filterApplied
);

export const isInGridPlpRecommendationEnabled = (
  entityCategoryName,
  abPlpPersonalizedCategories,
  isAbPlpPersonalized
) => {
  if (!entityCategoryName) return false;
  const isL2OrL3Category =
    entityCategoryName.split(':').length === 2 || entityCategoryName.split(':').length === 3;
  const isAbCategoryEnabled = abPlpPersonalizedCategories.indexOf(entityCategoryName) !== -1;
  return isL2OrL3Category && isAbPlpPersonalized && isAbCategoryEnabled;
};

export const inGridPlpAbTest = (state) => {
  const AbTest = getABtestFromState(state.AbTest);
  if (AbTest && AbTest.IN_GRID_PLP && typeof AbTest.IN_GRID_PLP.slots === 'object')
    return AbTest.IN_GRID_PLP.slots;
  return {};
};

export const getPlpInGridRecommendation = (state) => {
  const categoryName = getEntityCategory(state);
  const isInGridPLPEnabled = getAbInGridPlp(state);
  const categories = getAbInGridPlpCategories(state);
  const isEnabled = isInGridPlpRecommendationEnabled(categoryName, categories, isInGridPLPEnabled);
  if (isEnabled) return inGridPlpAbTest(state);
  return {};
};

export const getAbInMultiGridPlp = (state) => {
  return (
    getABtestFromState(state.AbTest) &&
    getABtestFromState(state.AbTest).IN_MULTI_GRID_PLP &&
    parseBoolean(getABtestFromState(state.AbTest).IN_MULTI_GRID_PLP.isEnabled)
  );
};

export const getAbInMultiGridPlpCategories = (state) => {
  const AbTest = getABtestFromState(state.AbTest);
  if (AbTest && AbTest.IN_MULTI_GRID_PLP && typeof AbTest.IN_MULTI_GRID_PLP.categories === 'object')
    return AbTest.IN_MULTI_GRID_PLP.categories;
  return [];
};

export const inMultiGridPlpAbTest = (state) => {
  const AbTest = getABtestFromState(state.AbTest);
  if (AbTest && AbTest.IN_MULTI_GRID_PLP && typeof AbTest.IN_MULTI_GRID_PLP.slots === 'object')
    return AbTest.IN_MULTI_GRID_PLP.slots;
  return {};
};

export const getPlpInMultiGridRecommendation = (state) => {
  const categoryName = getEntityCategory(state);
  const isInGridPLPEnabled = getAbInMultiGridPlp(state);
  const categories = getAbInMultiGridPlpCategories(state);
  const isEnabled = isInGridPlpRecommendationEnabled(categoryName, categories, isInGridPLPEnabled);
  if (isEnabled) return inMultiGridPlpAbTest(state);
  return {};
};

export const checkInGridRecommendationEnabled = (state) => {
  const multiGrid = getPlpInMultiGridRecommendation(state);
  const singelGrid = getPlpInGridRecommendation(state);
  return (
    (singelGrid && singelGrid.isSingleIngridEnabled === true) ||
    (multiGrid && multiGrid.isMultiIngridEnabled === true)
  );
};

export const getShowCarouselFlag = createSelector(
  getProductListingState,
  (ProductListing) => ProductListing && ProductListing.showCarouselFlag
);

export const getInitialValues = createSelector(
  getAppliedFilters,
  getAppliedSortId,
  (appliedFilters, appliedSortId) => {
    return {
      ...appliedFilters,
      sort: appliedSortId || '',
    };
  }
);
