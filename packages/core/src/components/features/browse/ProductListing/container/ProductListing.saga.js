/* eslint-disable max-statements */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, putResolve, takeLatest, select, fork } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import { isClient, getSiteId, getAPIConfig, isMobileApp } from '@tcp/core/src/utils';
import { loadLayoutData, loadModulesData } from '@tcp/core/src/reduxStore/actions';
import { resetAlternateBrandInPickUp } from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import {
  getFlipSwitchState,
  getUnbxdDRUrlState,
  getIsNonInvSessionFlag,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { isTier2Device, isTier3Device } from '@tcp/web/src/utils/device-tiering';
import { TIER2, TIER3 } from '@tcp/core/src/constants/tiering.constants';
import PRODUCTLISTING_CONSTANTS, {
  PRODUCTS_PER_LOAD,
  PRODUCTS_PER_LOAD_LOWER_END_DEVICES,
} from './ProductListing.constants';
import {
  setPlpProducts,
  setListingFirstProductsPage,
  setPlpLoadingState,
} from './ProductListing.actions';
import ProductAbstractor from '../../../../../services/abstractors/productListing';
import ProductsOperator from './productsRequestFormatter';
import { getDefaultWishList } from '../../Favorites/container/Favorites.saga';
import { getUrl } from './ProductListing.util';
import { getModulesState } from './ProductListing.selectors';

const instanceProductListing = new ProductAbstractor();
const operatorInstance = new ProductsOperator();

const isBucketingExtraCallRequired = (isFetchFiltersAndCountReq, prefetchedResponse) => {
  return isFetchFiltersAndCountReq && !prefetchedResponse;
};

const isReqCategoryIdDefined = (reqObj, isProcessedResponse) =>
  (reqObj && reqObj.categoryId) || isProcessedResponse;

const getLocationValue = (updatedUrl, location) => {
  let locationValue = '';

  if (updatedUrl) {
    const siteId = getSiteId();
    locationValue = `/${siteId}${updatedUrl}`;
  } else if (isClient()) {
    locationValue = !location.search
      ? `${location.pathname}?`
      : `${location.pathname}?${location.search}`;
  }
  return locationValue;
};

const initialBucketingValue = (prefetchedResponse) =>
  prefetchedResponse &&
  prefetchedResponse.bucketingConfig &&
  prefetchedResponse.bucketingConfig.bucketingSeqScenario;

const getBucketingConfig = (prefetchedResponse, productCount) => {
  const bucketingConfig = (prefetchedResponse && prefetchedResponse.bucketingConfig) || {};
  bucketingConfig.productsFetchedInPreviousCall = productCount;
  bucketingConfig.productsToFetchPerLoad = PRODUCTS_PER_LOAD;
  return bucketingConfig;
};

const getInitialProductsLoaded = (productCount, isProcessedResponse, prefetchedResponse) => {
  return !isProcessedResponse && prefetchedResponse ? productCount : PRODUCTS_PER_LOAD;
};

export function* getUnbxdXappConfigs() {
  const isFlipURL = yield select(getFlipSwitchState);
  const drURLpath = yield select(getUnbxdDRUrlState);
  return {
    isFlipURL,
    drURLpath,
  };
}

const getShowCarouselsSetVal = (showCarouselsObj) =>
  showCarouselsObj && showCarouselsObj[0] && showCarouselsObj[0].val;

const getCarouselObjInfo = (bannerInfo) => {
  return (
    bannerInfo &&
    bannerInfo.set &&
    bannerInfo.set.length &&
    bannerInfo.set.filter((item) => item.key === 'showCarousels')
  );
};
const getModuleData = (moduleDataInState) => {
  const moduleData = isClient() ? moduleDataInState : {};
  return Object.keys(moduleData) || [];
};

export function* fetchModulesAndLayout(
  res,
  apiConfig,
  fromPrefetch,
  isProcessedResponse,
  navDataObj
) {
  try {
    if (isProcessedResponse && navDataObj) {
      yield put(loadLayoutData(navDataObj.layout, 'productListingPage'));
      yield put(loadModulesData(navDataObj.modules));
    } else {
      const moduleDataInState = yield select(getModulesState);
      const moduleData = getModuleData(moduleDataInState);
      const bannerCheck =
        fromPrefetch ||
        (!fromPrefetch &&
          res.reqObj &&
          res?.reqObj?.breadCrumbs?.length === res?.reqObj?.categoryNameList.length);
      const bannerInfo =
        res &&
        (res.bannerInfo ||
          (res.processedPlpProducts && res.processedPlpProducts.bannerInfo) ||
          (bannerCheck &&
            res.body &&
            res.body.banner &&
            res.body.banner.banners &&
            res.body.banner.banners[0] &&
            res.body.banner.banners[0].bannerHtml &&
            JSON.parse(res.body.banner.banners[0].bannerHtml)));
      const { layout, modules } = yield call(
        instanceProductListing.parsedModuleData,
        bannerInfo,
        apiConfig,
        moduleData
      );
      const showCarouselsObj = getCarouselObjInfo();
      const showCarouselFlag = getShowCarouselsSetVal(showCarouselsObj);

      yield put(loadLayoutData(layout, 'productListingPage'));
      yield put(loadModulesData(modules));
      yield put(setPlpLoadingState({ showCarouselFlag }));
    }
  } catch (e) {
    logger.error({
      error: e,
      extraData: {
        component: 'ProductListing Saga - fetchModulesAndLayout',
        payloadRecieved: res,
      },
    });
  }
}

const isFormDataPresent = (formData, sortBySelected) => {
  return !formData && !sortBySelected;
};

const getPlpScrollFlag = (
  formDataPresent,
  isProcessedResponse,
  prefetchedResponse,
  filterApplied
) => {
  return (
    isClient() && formDataPresent && !isProcessedResponse && !prefetchedResponse && !filterApplied
  );
};

export function* fetchPlpProducts({ payload }) {
  const xappURLConfig = yield call(getUnbxdXappConfigs);
  yield put(resetAlternateBrandInPickUp());
  try {
    const {
      url,
      formData,
      sortBySelected,
      scrollToTop,
      isKeepModalOpen,
      prefetchedResponse,
      isProcessedResponse,
      passedNavigationData,
      responseFromPrefetchScript,
      productCount,
      isFilterAbTestOn,
      filterApplied,
      navDataObj,
      apiConfig = getAPIConfig(),
    } = payload;
    const formDataPresent = isFormDataPresent(formData, sortBySelected);
    let isBucketing = initialBucketingValue(prefetchedResponse);
    const location = getUrl(url);
    yield put(
      setPlpLoadingState({
        isLoadingMore: true,
        isScrollToTop: scrollToTop,
        isDataLoading: true,
        isKeepModalOpen,
        isESModuleLoading: true,
        initialProductsLoaded: 0,
        filterApplied,
      })
    );
    yield put(loadLayoutData({}, 'productListingPage'));
    if (!responseFromPrefetchScript) {
      yield put(
        setPlpLoadingState({
          loadedProductsPages: [],
        })
      );
    }
    let state = yield select();
    yield call(getDefaultWishList);
    let reqObj = operatorInstance.getProductListingBucketedData(
      { ...state },
      location,
      sortBySelected,
      formData,
      1,
      passedNavigationData,
      productCount,
      0
    );
    reqObj.extraParams = {
      ...reqObj.extraParams,
      ...instanceProductListing.getExtraFilterParams(isFilterAbTestOn),
    };
    if (isBucketing) {
      yield fork(
        fetchModulesAndLayout,
        prefetchedResponse,
        apiConfig,
        true,
        isProcessedResponse,
        navDataObj
      );
    }
    let res;

    const isNonInvABTestEnabled = yield select(getIsNonInvSessionFlag);
    if (isBucketingExtraCallRequired(reqObj.isFetchFiltersAndCountReq, prefetchedResponse)) {
      res = yield call(
        instanceProductListing.getProducts,
        { ...reqObj, location },
        { ...state },
        null,
        null,
        xappURLConfig,
        isNonInvABTestEnabled,
        true
      );

      if (isNonInvABTestEnabled && !res) {
        res = yield call(
          instanceProductListing.getProducts,
          { ...reqObj, location },
          { ...state },
          null,
          null,
          xappURLConfig,
          isNonInvABTestEnabled,
          false
        );
      }
      yield fork(fetchModulesAndLayout, res, apiConfig);
      const locationValue = getLocationValue(res.updatedUrl, location);
      yield put(setListingFirstProductsPage({ ...res, pageUrl: locationValue }));
      state = yield select();
      reqObj = operatorInstance.processProductFilterAndCountData(res, { ...state }, reqObj);
      isBucketing = true;
    }
    let initialProductsLoaded = getInitialProductsLoaded(
      productCount,
      isProcessedResponse,
      prefetchedResponse
    );
    let plpProducts;
    if (isReqCategoryIdDefined(reqObj, isProcessedResponse)) {
      if (isProcessedResponse) {
        plpProducts = prefetchedResponse.processedPlpProducts;
        // This is the SSR data, this is not considered for enabling the lazy loading
        initialProductsLoaded = 0;
      } else {
        plpProducts = yield call(
          instanceProductListing.getProducts,
          { ...reqObj, location },
          { ...state },
          prefetchedResponse,
          null,
          xappURLConfig,
          isNonInvABTestEnabled,
          true
        );
      }

      if (isNonInvABTestEnabled && !plpProducts) {
        plpProducts = yield call(
          instanceProductListing.getProducts,
          { ...reqObj, location },
          { ...state },
          prefetchedResponse,
          null,
          xappURLConfig,
          isNonInvABTestEnabled,
          false
        );
      }
      if (plpProducts) {
        if (!isBucketing) {
          yield fork(fetchModulesAndLayout, plpProducts, apiConfig);
        }
        operatorInstance.updateBucketingConfig(
          plpProducts,
          getBucketingConfig(prefetchedResponse, productCount),
          true,
          isProcessedResponse
        );

        const locationValue = getLocationValue(plpProducts.updatedUrl, location);
        yield put(
          setListingFirstProductsPage({
            ...plpProducts,
            pageUrl: locationValue,
          })
        );
      }
    }
    if (!plpProducts) {
      operatorInstance.composeBreadCrumbOnSSr({
        state: { ...state },
        location,
      });
      yield put(
        setPlpLoadingState({
          isLoadingMore: false,
          isScrollToTop: false,
          isDataLoading: false,
          isESModuleLoading: false,
          initialProductsLoaded: 0,
          totalProductsCount: 0,
        })
      );
    } else {
      yield put(
        setPlpLoadingState({
          isLoadingMore: false,
          isScrollToTop: false,
          isDataLoading: false,
          isESModuleLoading: false,
          initialProductsLoaded,
        })
      );
      yield put(
        setPlpLoadingState({
          shouldPlpScroll: getPlpScrollFlag(
            formDataPresent,
            isProcessedResponse,
            prefetchedResponse,
            filterApplied
          ),
        })
      );
    }
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'ProductListing Saga - fetchPlpProducts',
        payloadRecieved: payload,
      },
    });
    yield put(
      setPlpLoadingState({
        isLoadingMore: false,
        isScrollToTop: false,
        isDataLoading: false,
        isESModuleLoading: false,
        initialProductsLoaded: 0,
        totalProductsCount: 0,
      })
    );
  }
}

const isIntialProductLoaded = (productListingState) => {
  return productListingState && productListingState.initialProductsLoaded;
};

const getLastLoadedProductsNumber = (plpProducts) => {
  return (
    plpProducts &&
    plpProducts.loadedProductsPages &&
    plpProducts.loadedProductsPages[0] &&
    plpProducts.loadedProductsPages[0].length
  );
};

export function* fetchMoreProducts({ payload = {} }) {
  try {
    const { url, isFilterAbTestOn } = payload;
    const location = getUrl(url);
    let state = yield select();
    const initialProductsLoaded = isIntialProductLoaded(state.ProductListing);
    const xappURLConfig = yield call(getUnbxdXappConfigs);
    let isLowerEndDevice = false;
    if (isMobileApp()) {
      isLowerEndDevice =
        state.ProductListing.deviceTier === TIER2 || state.ProductListing.deviceTier === TIER3;
    } else {
      isLowerEndDevice = isTier2Device() || isTier3Device();
    }
    const productsToBeLoaded = isLowerEndDevice
      ? PRODUCTS_PER_LOAD_LOWER_END_DEVICES
      : PRODUCTS_PER_LOAD;

    let plpProducts;
    if (initialProductsLoaded) {
      yield put(setPlpLoadingState({ isLoadingMore: true, isScrollToTop: false }));
      const reqObj = operatorInstance.getMoreBucketedProducts(
        state,
        location,
        initialProductsLoaded,
        productsToBeLoaded,
        isLowerEndDevice
      );
      if (reqObj && reqObj.categoryId) {
        reqObj.extraParams = {
          ...reqObj.extraParams,
          ...instanceProductListing.getExtraFilterParams(isFilterAbTestOn),
        };
        state = yield select();
        const isNonInvABTestEnabled = yield select(getIsNonInvSessionFlag);
        plpProducts = yield call(
          instanceProductListing.getProducts,
          reqObj,
          state,
          null,
          productsToBeLoaded,
          xappURLConfig,
          isNonInvABTestEnabled,
          true
        );

        if (isNonInvABTestEnabled && !plpProducts) {
          plpProducts = yield call(
            instanceProductListing.getProducts,
            reqObj,
            state,
            null,
            productsToBeLoaded,
            xappURLConfig,
            isNonInvABTestEnabled,
            false
          );
        }
        if (plpProducts) {
          const productsLoadedInLastCall = getLastLoadedProductsNumber(plpProducts);
          operatorInstance.updateBucketingConfig(plpProducts, {
            productsFetchedInPreviousCall: productsLoadedInLastCall,
            productsToFetchPerLoad: productsToBeLoaded,
          });
        }
        if (getLastLoadedProductsNumber(plpProducts)) {
          yield putResolve(setPlpProducts({ ...plpProducts }));
          yield put(
            setPlpLoadingState({
              initialProductsLoaded: initialProductsLoaded + productsToBeLoaded,
            })
          );
        }
      }
      yield put(setPlpLoadingState({ isLoadingMore: false }));
    }
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'ProductListing Saga - fetchMoreProducts',
        payloadRecieved: payload,
      },
    });
  }
}

function* ProductListingSaga() {
  yield takeLatest(PRODUCTLISTING_CONSTANTS.FETCH_PRODUCTS, fetchPlpProducts);
  yield takeLatest(PRODUCTLISTING_CONSTANTS.GET_MORE_PRODUCTS, fetchMoreProducts);
}

export default ProductListingSaga;
