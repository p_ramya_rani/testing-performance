/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import queryString from 'query-string';
import logger from '@tcp/core/src/utils/loggerInstance';
import { breakpoints } from '@tcp/core/styles/themes/TCP/mediaQuery';
import {
  isMobileApp,
  isServer,
  getABtestFromState,
  getViewportInfo,
  isClient,
} from '../../../../../utils';
import { FACETS_FIELD_KEY } from '../../../../../services/abstractors/productListing/productListing.utils';

const shouldAddExtraPromo = (showInGridPromos, loadedProductCount, totalProductsCount, slots) =>
  showInGridPromos && loadedProductCount === totalProductsCount && slots;

const getIndex = (data) => {
  return data && data.some((category) => !!(category && category.url)) ? data.length : 0;
};

const minifiedData = (data, excludeLongDescription, subCats) => {
  return {
    categoryId: data.categoryId,
    title: data.title,
    seoTitle: excludeLongDescription ? '' : data.seoTitle,
    seoMetaDesc: excludeLongDescription ? '' : data.seoMetaDesc,
    longDescription: excludeLongDescription ? '' : data.longDescription,
    subCategories: subCats,
    url: data.url,
    canonicalUrl: excludeLongDescription ? '' : data.canonicalUrl,
    categoryFilters: data.categoryFilters,
  };
};

const getSeoHeaderCopy = (excludeLongDescription, data) => {
  const setValue =
    data &&
    data.categoryContent &&
    data.categoryContent.mainCategory &&
    data.categoryContent.mainCategory.set &&
    data.categoryContent.mainCategory.set.filter((item) => item.key === 'seoHeaderCopy');
  return excludeLongDescription ? '' : setValue && setValue[0] && setValue[0].val;
};

// TODO - add the required information from the commented lines
export const getRequiredCategoryData = (data, excludeLongDescription) => {
  const subCats = [];
  if (data.subCategories && data.subCategories.length) {
    data.subCategories.forEach((subCategory) => {
      subCats.push({
        categoryId: subCategory.categoryContent.id,
        title: subCategory.categoryContent.name,
        displayToCustomer: subCategory.categoryContent.displayToCustomer,
        isShortImage: subCategory.categoryContent.isShortImage,
        productCount: subCategory.productCount,
      });
    });
  }
  if (!data.categoryContent) {
    return minifiedData(data, excludeLongDescription, subCats);
  }
  return {
    categoryId: data.categoryId || data.categoryContent.id,
    title: data.title || data.categoryContent.name,
    seoTitle: excludeLongDescription ? '' : data.categoryContent.seoTitle,
    seoMetaDesc: excludeLongDescription ? '' : data.categoryContent.seoMetaDesc,
    longDescription: excludeLongDescription ? '' : data.categoryContent.longDescription,
    subCategories: subCats,
    url: data.url,
    canonicalUrl: excludeLongDescription ? '' : data.categoryContent.canonicalUrl,
    categoryFilters: data.categoryFilters,
    isShortImage: data.categoryContent.isShortImage,
    seoHeaderCopy: getSeoHeaderCopy(excludeLongDescription, data),
  };
};

export const extractCategory = (category) => {
  // Extracting category id or path from the URL
  try {
    let categoryId;
    // if (category && category.indexOf('/') === -1) {
    //   return category;
    // }
    if (Number.isInteger(category)) {
      categoryId = category;
    } else if (category && category.lastIndexOf('/') === category.length - 1) {
      categoryId = category && category.split('/');
      categoryId = categoryId.length > 1 ? categoryId[categoryId.length - 2] : categoryId[0];
    } else {
      categoryId = category && category.split('/').pop();
    }
    return categoryId;
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'ProductListing Util - extractCategory',
        categoryRecieved: category,
      },
    });
  }
  return category;
};

// eslint-disable-next-line
export const findCategoryIdandName = (data, category, excludeLongDescription, onlySubCats) => {
  const index = getIndex(data);
  let iterator = 0;
  let categoryFound = [];
  const categoryId = (category && extractCategory(category)) || '';

  while (iterator < index) {
    const subCategoryArr =
      (data[iterator].subCategories && Object.keys(data[iterator].subCategories)) || [];
    let newCatArr = [];
    if (
      data[iterator] &&
      data[iterator].subCategories &&
      !data[iterator].subCategories.length &&
      subCategoryArr.length
    ) {
      for (let groupCount = 0; groupCount < subCategoryArr.length; groupCount += 1) {
        newCatArr = newCatArr.concat(
          data[iterator].subCategories[subCategoryArr[groupCount]].items
        );
      }
    } else if (data[iterator].subCategories) {
      newCatArr = newCatArr.concat(data[iterator].subCategories);
    }

    const navUrl = extractCategory(data[iterator].url && data[iterator].url.replace('/c?cid=', ''));

    if (
      (data[iterator].categoryContent &&
        data[iterator].categoryContent.categoryId === categoryId) ||
      (navUrl && navUrl.toLowerCase()) === (categoryId && categoryId.toLowerCase())
    ) {
      if (onlySubCats) {
        categoryFound.push(data[iterator]);
        break;
      }
      categoryFound.push(getRequiredCategoryData(data[iterator], excludeLongDescription));
    } else if (newCatArr && newCatArr.length) {
      categoryFound = findCategoryIdandName(
        newCatArr,
        category,
        excludeLongDescription,
        onlySubCats
      );
      if (categoryFound.length) {
        categoryFound.push(getRequiredCategoryData(data[iterator], excludeLongDescription));
      }
    }
    if (categoryFound.length) {
      break;
    } else {
      iterator += 1;
    }
  }
  return categoryFound;
};

// TODO - refactor this function - this is random and dummy
export const matchPath = (url, param) => {
  if (param === '/search' && url.indexOf(param) !== -1) {
    return {
      searchTerm: url,
    };
  }
  if (
    (param === '/c?cid=' && url.indexOf(param) !== -1) ||
    (param === '/c/' && url.indexOf(param) !== -1)
  ) {
    const urlWithCat = url.split(param)[1];
    return {
      listingKey: urlWithCat,
    };
  }
  return url;
};

export const processBreadCrumbs = (breadCrumbTrail) => {
  if (breadCrumbTrail && breadCrumbTrail.length) {
    return breadCrumbTrail.map((crumb) => ({
      displayName: crumb.displayName,
      destination: 'c',
      pathSuffix: extractCategory(crumb.urlPathSuffix),
    }));
  }
  return [];
};

// Organized Navigation Tree
export const generateGroups = (level1) => {
  try {
    let level2Groups = [];
    const groupings = {};

    const listOfGroups = Object.keys(level1.subCategories);
    // for each L2 parse and place in proper group
    listOfGroups.forEach((grp) => {
      if (level1.subCategories[grp] && level1.subCategories[grp].items) {
        level1.subCategories[grp].items.forEach((L2) => {
          const groupName = grp;
          const groupOrder = level1.subCategories[grp].order;

          // if new grouping initalize array
          if (!groupings[groupName]) {
            groupings[groupName] = {
              order: groupOrder,
              menuItems: [],
            };
          }

          // Push L2 in this bucket
          groupings[groupName].menuItems.push(L2);
        });
      }
    });

    // Now get all groups and generate array of object, this is not to bad as there are at most 3-4 groups
    level2Groups = Object.keys(groupings).map((group) => ({
      groupName: group,
      order: groupings[group].order,
      menuItems: groupings[group].menuItems,
    }));

    return level2Groups.sort((prevGroup, curGroup) => {
      return prevGroup.order - curGroup.order;
    });
  } catch (error) {
    logger.error('getHeaderNavigationTree:generateGroups', error);
    return [];
  }
};

export const isSearch = () => {
  return false;
};

export const matchValue = (isSearchPage, location) => {
  const categoryParam = isMobileApp() ? '/c?cid=' : '/c/';
  const params = isSearchPage ? '/search/' : categoryParam;
  return matchPath(location, params);
};

export const getCategoryKey = (isSearchPage, match) => {
  return isSearchPage ? match.searchTerm : match.listingKey;
};

export const getCurrentCatId = (breadCrumb) => {
  return breadCrumb.length ? breadCrumb[breadCrumb.length - 1].categoryId : '';
};

export const getBreadcrumbCatId = (breadCrumb) => {
  return breadCrumb.length ? breadCrumb.map((category) => category.categoryId) : [];
};

export const getSeoHeaderCopyFromBreadCrumb = (breadCrumb) => {
  return breadCrumb && breadCrumb.length ? breadCrumb[breadCrumb.length - 1].seoHeaderCopy : '';
};

export const getCatById = (navTree, breadCrumbId) => {
  return navTree && navTree.length
    ? navTree.find((category) => {
        return category && category.categoryContent && category.categoryContent.id === breadCrumbId;
      })
    : {};
};

export const getCatId = (categoryNameList) => {
  return categoryNameList ? categoryNameList.map((item) => item.categoryId).join('>') : '';
};

export const getDesiredL3 = (catNameL3, bucketingConfig) => {
  return !catNameL3 && bucketingConfig.L3Left.length
    ? (bucketingConfig.L3Left[0] &&
        bucketingConfig.L3Left[0].categoryContent &&
        bucketingConfig.L3Left[0].categoryContent.name) ||
        (bucketingConfig.L3Left[0] && bucketingConfig.L3Left[0].title)
    : catNameL3;
};

export const getCatIdUbxd = (categoryPathMap, categoryNameList) => {
  return (
    categoryPathMap ||
    (categoryNameList ? categoryNameList.map((item) => item.categoryId).join('>') : '')
  );
};

export const isRequiredChildrenExists = (requiredChildren) => {
  return requiredChildren && requiredChildren.length;
};

export const isCatIdBucketingSeq = (categoryNameList, clickedL2) => {
  return categoryNameList && categoryNameList.length ? clickedL2.categoryId : null;
};

export const getSeoKeywordOrCategoryIdOrSearchTerm = (match) => {
  return match.searchTerm || match.listingKey;
};

export const getDesiredNav = (clickedNav) => {
  return clickedNav ? clickedNav.title : '';
};

export const isRequiredL2L1 = (isUnbxdSequencing, shouldApplyUnbxdLogic) => {
  return !isUnbxdSequencing || shouldApplyUnbxdLogic;
};

export const getBreadCrumb = (categoryNameList) => {
  return categoryNameList
    ? categoryNameList.map((crumb) => ({
        categoryId: crumb.categoryId,
        displayName: crumb.title,
        urlPathSuffix: extractCategory(crumb.url),
        longDescription: crumb.longDescription,
        seoHeaderCopy: crumb.seoHeaderCopy,
      }))
    : [];
};

// Inline function to get sum of object array element
const sumValues = (obj) => Object.values(obj).reduce((a, b) => a + b);

export const getIsShowCategoryGrouping = (state) => {
  const isL2Category = state.ProductListing.breadCrumbTrail.length === 2;
  // const isNotAppliedSort = !state.productListing.appliedSortId;
  const isNotAppliedSort = !null;
  const appliedFilters = state.ProductListing.appliedFiltersIds;
  const isNotAppliedFilter =
    (appliedFilters && appliedFilters.length > 0 && !sumValues(appliedFilters)) || true;

  return isL2Category && isNotAppliedSort && isNotAppliedFilter;
};

export const getUrlPageNumber = (pathUrl) => {
  const pathUrls = pathUrl.split('/');
  if (pathUrls.length === 5 && pathUrls[4]) {
    let pageNumber = Number(pathUrls[4]);
    if (Number.isNaN(pageNumber)) {
      pageNumber = 1;
    }
    pathUrls.pop();
    return { pathname: pathUrls.join('/'), pageNumber };
  }
  return { pathname: pathUrl, pageNumber: 1 };
};

export const getUrl = (url) => {
  if (url) {
    const [pageUrl, search] = url.split('?');
    const { pathname, pageNumber } = getUrlPageNumber(pageUrl);
    return {
      pathname: isMobileApp() ? url : pathname,
      search,
      pageNumber,
    };
  }
  return !isServer() && window.location;
};

export const addInGridPlp = (
  productsAndTitleBlock,
  inGridSlots,
  currentSlot,
  promoAddedInCurrentBlock
) => {
  let totalPromo = promoAddedInCurrentBlock;
  const slotIndex = inGridSlots.indexOf(currentSlot + 1);
  if (slotIndex !== -1) {
    productsAndTitleBlock.push({
      slotIndex: currentSlot + 1,
      inGridSlots,
      totalSlots: 1,
      itemType: 'inMultiGridPlpRecommendation',
    });
    totalPromo += 1;
  }
  return totalPromo;
};

export const convertMultiGridSlotsRangeToList = (slots) => {
  const itemSlots = [];
  slots.forEach((slot) => {
    if (slot[0] && slot[1])
      for (let i = slot[0]; i <= slot[1]; i += 1) {
        itemSlots.push(i);
      }
  });
  return itemSlots;
};

export const addInMultiGridPlp = (
  productsAndTitleBlock,
  inGridSlots,
  inMultiGridSlotsList,
  currentSlot,
  promoAddedInCurrentBlock
) => {
  let totalPromo = promoAddedInCurrentBlock;
  const slotIndex = inMultiGridSlotsList.indexOf(currentSlot + 1);

  if (slotIndex !== -1) {
    let totalSlots = 0;
    inGridSlots.forEach((slot) => {
      if (currentSlot + 1 === slot[0]) {
        totalSlots = slot[1] - slot[0] + 1;
        totalPromo += totalSlots;
      }
    });
    if (totalSlots)
      productsAndTitleBlock.push({
        slotIndex: currentSlot + 1,
        inGridSlots,
        totalSlots,
        inGridSlotsFlat: inMultiGridSlotsList,
        itemType: 'inMultiGridPlpRecommendation',
      });
  }
  return totalPromo;
};

export const addInMultiGridPlpMobile = (productsAndPromos, inGridSlots, flatSlots, currentSlot) => {
  if (!Array.isArray(flatSlots)) return currentSlot;
  if ((currentSlot + 1) % 2 === 0) return currentSlot; // Always check multi grid from odd position
  let total = currentSlot;
  const slotIndex = flatSlots.indexOf(currentSlot + 1);

  if (slotIndex !== -1) {
    productsAndPromos.push({
      slotIndex: currentSlot + 1,
      inGridSlots,
      flatSlots,
      itemType: 'inMultiGridPlpRecommendation',
    });
    productsAndPromos.push({
      itemType: 'blankProductForMultiGrid',
      slotIndex: currentSlot + 2,
    });
    total += 2;
  }
  return total;
};

export const addInGridPlpMobile = (productsAndPromos, inGridSlots, currentSlot) => {
  if (!Array.isArray(inGridSlots)) return currentSlot;
  let total = currentSlot;
  const slotIndex = inGridSlots.indexOf(currentSlot + 1);
  if (slotIndex !== -1) {
    productsAndPromos.push({
      slotIndex: currentSlot + 1,
      inGridSlots,
      itemType: 'inGridPlpRecommendation',
    });
    total += 1;
  }
  return total;
};

const getInGridSlots = (inGridPlp) => {
  let inGridSlots;
  if (isClient() && inGridPlp.isSingleIngridEnabled === true) {
    const { isMobile, isTablet, isDesktop } = getViewportInfo();
    if (isMobile) inGridSlots = inGridPlp.mobileSlots;
    else if (isTablet) inGridSlots = inGridPlp.tabletSlots;
    else if (isDesktop) inGridSlots = inGridPlp.desktopSlots;
  }
  return inGridSlots;
};

const getInMultiGridSlots = (inMultiGridPlp) => {
  let inMultiGridSlots;
  let inMultiGridSlotsList;
  if (isClient() && inMultiGridPlp.isMultiIngridEnabled === true) {
    const { isMobile, isTablet, isDesktop } = getViewportInfo();
    if (isMobile) inMultiGridSlots = inMultiGridPlp.mobileSlots;
    else if (isTablet) inMultiGridSlots = inMultiGridPlp.tabletSlots;
    else if (isDesktop) inMultiGridSlots = inMultiGridPlp.desktopSlots;
    inMultiGridSlotsList = convertMultiGridSlotsRangeToList(inMultiGridSlots);
  }
  return [inMultiGridSlots, inMultiGridSlotsList];
};

const getSlots = (gridPromo) => {
  const slots = [];
  gridPromo.forEach((promoItem) => {
    const slotNumber = (promoItem.slot && promoItem.slot.split('slot_')[1]) || '';
    slots.push(parseInt(slotNumber, 10));
  });
  return slots;
};

const getHorizontalSlots = (horizontalPromo, slots) => {
  const horizontalSlots = [];
  horizontalPromo.forEach((promoItem) => {
    const slotNumber = (promoItem.slot && promoItem.slot.split('slot_')[1]) || '';
    // If the horizontal slot is same as the grid slot we add horizontal slot at +2
    if (slots.indexOf(parseInt(slotNumber, 10)) !== -1) {
      horizontalSlots.push(parseInt(slotNumber, 10) + 2);
    } else {
      horizontalSlots.push(parseInt(slotNumber, 10));
    }
  });
  return horizontalSlots;
};

const getPromoAddedInCurrentBlock = (
  productsAndTitleBlock,
  inGridSlots,
  inMultiGridSlots,
  inMultiGridSlotsList,
  currentSlot,
  promoAddedInCurrentBlock
) => {
  let promoAddedInCurrentIteration = promoAddedInCurrentBlock;

  if (Array.isArray(inGridSlots)) {
    promoAddedInCurrentIteration = addInGridPlp(
      productsAndTitleBlock,
      inGridSlots,
      currentSlot,
      promoAddedInCurrentBlock
    );
  }
  if (Array.isArray(inMultiGridSlots)) {
    promoAddedInCurrentIteration = addInMultiGridPlp(
      productsAndTitleBlock,
      inMultiGridSlots,
      inMultiGridSlotsList,
      currentSlot,
      promoAddedInCurrentBlock
    );
  }
  return promoAddedInCurrentIteration;
};

const handleInGridPromo = (
  currentSlot,
  horizontalSlots,
  productsAndTitleBlock,
  horizontalPromo,
  slots,
  gridPromo
) => {
  // First add the horizontal/mobile promos as they don't add up to the count
  // For horizontal / mobile only promo
  // Also, if slot_6 needs to be added, the slot number has to be 6 since it gets added after 6th product
  const productsAndTitleBlockTemp = [...productsAndTitleBlock];
  let promoAdded = 0;
  const horizontalSlotIndex = horizontalSlots.indexOf(currentSlot);
  if (horizontalSlotIndex !== -1) {
    productsAndTitleBlockTemp.push({
      itemType: 'gridPromo',
      gridStyle: 'horizontal',
      itemVal: horizontalPromo[horizontalSlotIndex],
    });
  }
  // If Vertical promo slot_8 needs to be added, the slot number has to be 7
  // since it occupies space of the 8th product, which is actually slot 7
  let slotCounter = 1;
  let slotIndex = slots.indexOf(currentSlot + slotCounter);
  while (slotIndex !== -1) {
    productsAndTitleBlockTemp.push({
      itemType: 'gridPromo',
      gridStyle: 'vertical',
      itemVal: gridPromo[slotIndex],
    });
    slotCounter += 1;
    slotIndex = slots.indexOf(currentSlot + slotCounter);
    promoAdded += 1;
  }
  return [productsAndTitleBlockTemp, promoAdded];
};

// eslint-disable-next-line
export function getProductsAndTitleBlocks(
  state,
  productBlocks = [],
  gridPromo,
  horizontalPromo,
  rowSize,
  filterCount,
  inGridPlp = {},
  inMultiGridPlp = {},
  loadedProductCount,
  totalProductsCount
) {
  const productsAndTitleBlocks = [];
  let lastCategoryName = null;
  let slots = [];
  let horizontalSlots = [];
  const { showInGridPromos } = state && state.AbTest ? getABtestFromState(state.AbTest) : {};
  // If filters are applied, do not consider the promos even if they are configured
  let inGridSlots;
  let inMultiGridSlots;
  let inMultiGridSlotsList;

  if (filterCount === 0) {
    slots = getSlots(gridPromo);
    horizontalSlots = getHorizontalSlots(horizontalPromo, slots);
    inGridSlots = getInGridSlots(inGridPlp);
    [inMultiGridSlots, inMultiGridSlotsList] = getInMultiGridSlots(inMultiGridPlp);
  }

  let totalItemsAdded = 0;

  let isL2WithBucket = false;
  let numberOfItemsInCurrentL2 = 0;
  const totalPagesToBeShown = productBlocks.length;
  for (let i = 0; i < totalPagesToBeShown; i += 1) {
    if (productBlocks[i]) {
      let productsAndTitleBlock = [];
      let promoAddedInCurrentBlock = 0;
      const productsInCurrentArr = productBlocks[i].length;
      for (let j = 0; j < productsInCurrentArr; j += 1) {
        const { categoryName } = productBlocks[i][j].miscInfo;
        const product1 = productBlocks[i][j];

        // indexOfProduct is all the items added already in the previous iteration
        // plus the promos added in the current iteration
        // plus index which would give the current iteration products added
        const currentSlot = totalItemsAdded + promoAddedInCurrentBlock + j;
        if (showInGridPromos) {
          const inGridPromoHandler = handleInGridPromo(
            currentSlot,
            horizontalSlots,
            productsAndTitleBlock,
            horizontalPromo,
            slots,
            gridPromo
          );
          [productsAndTitleBlock] = inGridPromoHandler;
          promoAddedInCurrentBlock += inGridPromoHandler[1];
        }
        promoAddedInCurrentBlock = getPromoAddedInCurrentBlock(
          productsAndTitleBlock,
          inGridSlots,
          inMultiGridSlots,
          inMultiGridSlotsList,
          currentSlot,
          promoAddedInCurrentBlock
        );

        // push: If we should group and we hit a new category name push on array
        const shouldGroup =
          state.ProductListing.breadCrumbTrail && getIsShowCategoryGrouping(state);
        if (shouldGroup && categoryName && categoryName !== lastCategoryName) {
          isL2WithBucket = true;
          numberOfItemsInCurrentL2 = 0;
          productsAndTitleBlock.push(categoryName);
          lastCategoryName = categoryName;
        }

        // push: product onto block
        productsAndTitleBlock.push(product1);
      }

      totalItemsAdded += productsInCurrentArr + promoAddedInCurrentBlock;
      numberOfItemsInCurrentL2 += productsInCurrentArr + promoAddedInCurrentBlock;
      // Check if the number of products and the promos count sum
      // to understand the number of slots blank at the end of the block
      const numberOfItemsInLastRow = numberOfItemsInCurrentL2 % rowSize;

      // For L2 with buckets only
      // If there is some empty space in the last row of the block
      // and the next block starts with a new L3 category,
      // or if this is the last block of the entire productsBlock that is loaded yet
      // ie. this is the last row to appear, irrespective of the fact if it is followed by a new L3 or not
      // check if some slots were supposed to be added in those empty space
      if (
        isL2WithBucket &&
        ((numberOfItemsInLastRow !== 0 &&
          productBlocks[i + 1] &&
          productBlocks[i + 1][0].miscInfo.categoryName !== lastCategoryName) || // TODO - add null check
          i + 1 === productBlocks.length)
      ) {
        let emptySpaces = rowSize - numberOfItemsInLastRow;
        if (emptySpaces < rowSize) {
          while (emptySpaces) {
            totalItemsAdded += 1;
            const indexOfEmptySlot = slots.indexOf(totalItemsAdded);
            if (indexOfEmptySlot !== -1 && showInGridPromos) {
              // See if the empty spaces are omitting any promo
              productsAndTitleBlock.push({
                itemType: 'gridPromo',
                gridStyle: 'vertical',
                itemVal: gridPromo[indexOfEmptySlot],
              });
            }
            emptySpaces -= 1;
          }
        }
        // If this is the last block
      }
      // push: product block onto matrix
      productsAndTitleBlocks.push(productsAndTitleBlock);
    }
  }
  if (showInGridPromos && loadedProductCount === totalProductsCount && slots) {
    const extraSlotPos = slots.filter((slot, index) => totalProductsCount + index < slot);
    if (extraSlotPos[0]) {
      const slotIndex = slots.indexOf(extraSlotPos[0]);
      const productsAndTitleBlocksLength = productsAndTitleBlocks.length;
      productsAndTitleBlocks[productsAndTitleBlocksLength - 1].push({
        itemType: 'gridPromo',
        gridStyle: 'vertical',
        itemVal: gridPromo[slotIndex],
      });
    }
  }
  return productsAndTitleBlocks;
}

export const getPlpCutomizersFromUrlQueryString = (urlQueryString) => {
  const queryParams = queryString.parse(urlQueryString);
  Object.keys(queryParams).forEach((key) => {
    const value = decodeURIComponent(queryParams[key]);
    queryParams[key] =
      key && (key.toLowerCase() === FACETS_FIELD_KEY.sort ? value : value.split(','));
  }); // Fetching Facets and sort key from the URL query string
  return queryParams;
};

// This function is used for mobile app In-grid promo implementation
export const getProductsWithPromo = (
  products,
  gridPromo,
  horizontalPromo,
  filterCount,
  state,
  inGridPlp = {},
  { loadedProductCount, totalProductsCount } = {}
  // eslint-disable-next-line sonarjs/cognitive-complexity
) => {
  const slots = [];
  const horizontalSlots = [];
  const { showInGridPromos } = state && state.AbTest ? getABtestFromState(state.AbTest) : {};

  let inGridSlots;
  if (filterCount === 0) {
    gridPromo.forEach((promoItem) => {
      const slotNumber = (promoItem.slot && promoItem.slot.split('slot_')[1]) || '';
      slots.push(parseInt(slotNumber, 10));
    });

    horizontalPromo.forEach((promoItem) => {
      const slotNumber = (promoItem.slot && promoItem.slot.split('slot_')[1]) || '';
      horizontalSlots.push(parseInt(slotNumber, 10));
    });

    if (inGridPlp.isSingleIngridEnabled === true) {
      inGridSlots = inGridPlp.mobileSlots;
    }

    if (inGridPlp.isMultiIngridEnabled === true) {
      inGridSlots = inGridPlp.mobileSlots;
    }
  }
  let productCount = 0;
  const productsAndPromos = [];

  if (products) {
    products.forEach((product, index) => {
      if (showInGridPromos) {
        let slotCounter = 1;
        let slotIndex = slots.indexOf(productCount + slotCounter);
        while (slotIndex !== -1) {
          productCount += 1;
          productsAndPromos.push({
            itemType: 'gridPromo',
            gridStyle: 'vertical',
            itemVal: gridPromo[slotIndex],
          });
          slotIndex = slots.indexOf(productCount + slotCounter);
          slotCounter += 1;
        }

        const horizontalSlotIndex = horizontalSlots.indexOf(productCount);
        if (horizontalSlotIndex !== -1) {
          productsAndPromos.push({
            itemType: 'gridPromo',
            gridStyle: 'horizontal',
            itemVal: horizontalPromo[horizontalSlotIndex],
          });

          // Since horizontal promo occupies two slots, add a dummy blank promo
          productsAndPromos.push({
            itemType: 'gridPromo',
            gridStyle: 'blank',
            itemVal: horizontalPromo[horizontalSlotIndex],
          });
        }
      }

      if (inGridPlp.isSingleIngridEnabled === true) {
        productCount = addInGridPlpMobile(productsAndPromos, inGridSlots, productCount);
      }

      if (inGridPlp.isMultiIngridEnabled === true) {
        const flatSlots = convertMultiGridSlotsRangeToList(inGridPlp.mobileSlots);
        productCount = addInMultiGridPlpMobile(
          productsAndPromos,
          inGridSlots,
          flatSlots,
          productCount
        );
      }

      productsAndPromos.push(products[index]);
      productCount += 1;
    });
  }

  if (shouldAddExtraPromo(showInGridPromos, loadedProductCount, totalProductsCount, slots)) {
    const extraSlotPos = slots.filter((slot, index) => totalProductsCount + index < slot);
    if (extraSlotPos[0]) {
      const slotIndex = slots.indexOf(extraSlotPos[0]);
      productsAndPromos.push({
        itemType: 'gridPromo',
        gridStyle: 'vertical',
        itemVal: gridPromo[slotIndex],
      });
    }
  }
  return productsAndPromos;
};

/**
 * This function will return the total count of displayable category.
 * @param {*} items
 */
export const visibleCategoryCount = (items) => {
  return items.reduce((acc, item) => {
    return item.displayToCustomer ||
      (item.categoryContent && item.categoryContent.displayToCustomer)
      ? acc + 1
      : acc;
  }, 0);
};

export const getCategoryFilters = (categoryListObj) => {
  let categoryFilterArray = [];
  if (categoryListObj.length >= 2) {
    categoryFilterArray = categoryListObj[1].categoryFilters;
  }
  return categoryFilterArray;
};

export const CarouselOptions = {
  CAROUSEL_OPTIONS: {
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    slide: true,
    touchMove: true,
    touchThreshold: 100,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: breakpoints.values.lg - 1,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: breakpoints.values.sm - 1,
        settings: {
          arrows: false,
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
    ],
  },
};
/* returns CLPPage object if the current page is  CLP */
export const getIsCLPPage = (navigationData, router, siteId) => {
  return (
    navigationData &&
    navigationData.find(
      (item) => item.categoryContent && `/${siteId}${item.categoryContent.asPath}` === router.asPath
    )
  );
};

export const checkPromoCondition = (
  filterCount,
  plpHorizontalPromos,
  plpGridPromos,
  inGridPlp,
  inMultiGridPlp,
  showInGridPromos
) => {
  let isPromoApplied = false;
  if (
    filterCount ||
    ((Object.keys(plpHorizontalPromos).length !== 0 || Object.keys(plpGridPromos).length !== 0) &&
      showInGridPromos) ||
    Object.keys(inGridPlp).length !== 0 ||
    Object.keys(inMultiGridPlp).length !== 0
  ) {
    isPromoApplied = true;
  }
  return isPromoApplied;
};

export const checkItemIsProduct = (item) => {
  let isProduct = true;
  if (
    typeof item === 'string' ||
    item.itemType === 'gridPromo' ||
    item.itemType === 'inMultiGridPlpRecommendation'
  ) {
    isProduct = false;
  }
  return isProduct;
};
