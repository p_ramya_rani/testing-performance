// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';

const getAdditionalStyle = (props) => {
  const { margins, paddings, displayAltFilterView } = props;
  const flexDisplay = displayAltFilterView ? { 'justify-content': 'flex-start' } : '';
  return {
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
    ...flexDisplay,
  };
};

const FilterViewContainer = styled.View`
  padding-top: 10px;
  background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
`;

const PageContainer = styled.View`
  flex: 1;
  ${getAdditionalStyle}
  position:relative;
  ${(props) =>
    props.isNewReDesignProductTile
      ? `background: ${props.theme.colors.PRIMARY.PALEGRAY} margin-bottom: ${props.theme.spacing.XL}`
      : ``}
`;

const ItemCountContainer = styled.View`
  flex-direction: row;
`;

const EmptyView = styled.View`
  min-width: 10px;
  min-height: 10px;
  ${getAdditionalStyle};
`;

const FilterContainer = styled.View`
  margin-top: ${(props) =>
    props.changeFilterViewABTest ? 5 : props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: ${(props) =>
    props.changeFilterViewABTest ? -12 : props.theme.spacing.ELEM_SPACING.MED};
`;

const BannerWrapper = styled.View`
  background-color: ${(props) =>
    props.theme.isGymboree
      ? props.theme.colorPalette.orange[900]
      : props.theme.colorPalette.blue[1000]};
  padding: ${(props) => props.theme.typography.fontSizes.fs10};
  color: ${(props) => props.theme.colorPalette.white};
  font-size: ${(props) => props.theme.typography.fontSizes.fs24};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
  align-items: center;
`;

const ListHeaderContainer = styled.View`
  padding-bottom: 20px;
`;

const RowContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  ${getAdditionalStyle};
`;

const ButtonContainer = styled.View`
  align-items: center;
  top: -50px;
  left: 0;
  right: 0;
  background: ${(props) => props.theme.colorPalette.white};
`;

const DisplaySkeleton = styled.View`
  position: absolute;
  background-color: ${(props) => props.theme.colorPalette.white};
`;

const DisplayDepartment = styled.View`
  margin: -10px auto;
`;

const RecommendationWrapper = styled.View`
  margin-left: -${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;
const ContentWrapper = styled.Text`
  text-align: center;
`;
const PromoContainerForRedesign = styled.View`
  ${(props) =>
    props.isNewReDesignProductTile
      ? `background-color: ${props.theme.colorPalette.white};flex:1`
      : ``}
`;

const PlpView = styled.View`
  ${(props) =>
    props.isNewReDesignProductTile
      ? `background-color: ${props.theme.colors.PRIMARY.PALEGRAY}; flex: 1;`
      : ``};
`;

const styles = css``;

export {
  styles,
  PageContainer,
  ItemCountContainer,
  ListHeaderContainer,
  FilterContainer,
  EmptyView,
  RowContainer,
  DisplaySkeleton,
  RecommendationWrapper,
  BannerWrapper,
  ButtonContainer,
  DisplayDepartment,
  ContentWrapper,
  PlpView,
  PromoContainerForRedesign,
  FilterViewContainer,
};
