/* eslint-disable max-lines */
/* eslint-disable react/prop-types */
// 9fbef606107a605d69c0edbcd8029e5d
// Disabling eslint for temporary file
import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf/RenderPerf';
import {
  getProductListingPageTrackData,
  getIcidValue,
  isClient,
  getQueryParamsFromUrl,
  scrollToBreadCrumb,
  isMobileWeb,
  isTCP,
  isServer,
  fireEnhancedEcomm,
  filterQuantityLabels,
  getViewportInfo,
} from '@tcp/core/src/utils';
import {
  CONTROLS_VISIBLE,
  RESULTS_VISIBLE,
  PROMOTION_VISIBLE,
} from '@tcp/core/src/constants/rum.constants';

import { setProp } from '@tcp/core/src/analytics/utils';
// Changes as per RWD-9852. Keeping this for future reference.
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import PromoFilter from '@tcp/core/src/components/common/molecules/PromoFilter/views/PromoFilter.view';
import L3NavigationCarousel from '@tcp/core/src/components/common/molecules/L3NavigationCarousel/views/L3NavigationCarousel.view';
import { Banner404 } from '@tcp/core/src/components/common/molecules';
import PromoModules from '../../../../common/organisms/PromoModules';
import { Row, Col, PLPSkeleton, RichText } from '../../../../common/atoms';
import ProductsGrid from '../molecules/ProductsGrid/views';
import EmptyCategoryBanner from '../molecules/EmptyCategoryBanner/views';
import GlobalNavigationMenuDesktopL2 from '../molecules/GlobalNavigationMenuDesktopL2/views';
import withStyles from '../../../../common/hoc/withStyles';

import ProductListingStyle, {
  recommendationStyles,
  bannerRecommendationStyles,
} from '../ProductListing.style';

import FixedBreadCrumbs from '../molecules/FixedBreadCrumbs/views';

import ProductListingFiltersForm from '../molecules/ProductListingFiltersForm';
import ReadMore from '../molecules/ReadMore/views';
import SpotlightContainer from '../molecules/Spotlight/index.async';
import LoadedProductsCount from '../molecules/LoadedProductsCount/views';
import searchConstants from '../../../../common/molecules/SearchBar/SearchBar.constants';
import { CarouselOptions } from '../container/ProductListing.util';
import NewLoadedProductsCount from '../molecules/NewLoadedProductsCount/views/NewLoadedProductsCount.view';
import BopisFilter from '../molecules/BopisFilter/views/BopisFilter';

// Minimum number of product results worth measuring with a UX timer
const MINIMUM_RESULTS_TO_MEASURE = 3;

const formatProductsData = (products, breadCrumbs) => {
  let type = '';
  const formattedCategory =
    breadCrumbs &&
    breadCrumbs.map((listing) => listing.displayName && listing.displayName.toLowerCase());

  if (formattedCategory.length && formattedCategory.includes('accessories')) {
    type = 'accessories';
  } else if (formattedCategory.includes('shoes')) {
    type = 'shoes';
  } else {
    type = 'apparel';
  }

  return products.map((tile, index) => {
    const {
      productInfo: { listPrice, offerPrice, name, generalProductId, priceRange },
    } = tile;
    const productId = generalProductId && generalProductId.split('_')[0];
    const productName = name;
    return {
      id: productId,
      colorId: generalProductId,
      name: productName,
      price: offerPrice,
      listPrice,
      extPrice: priceRange.lowOfferPrice,
      position: index + 1,
      type,
      pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
    };
  });
};
const formatProductsDataEE = (products, breadCrumbs, currency) => {
  let type = '';
  const formattedCategory =
    breadCrumbs &&
    breadCrumbs.map((listing) => listing.displayName && listing.displayName.toLowerCase());

  if (formattedCategory.length && formattedCategory.includes('accessories')) {
    type = 'accessories';
  } else if (formattedCategory.includes('shoes')) {
    type = 'shoes';
  } else {
    type = 'apparel';
  }

  return products.map((tile, index) => {
    const {
      productInfo: { listPrice, offerPrice, generalProductId, priceRange },
    } = tile;
    return {
      id: generalProductId,
      price: offerPrice,
      dimension35: isTCP() ? 'TCP' : 'GYM',
      listPrice,
      extPrice: priceRange.lowOfferPrice,
      metric92: '1',
      dimension63: `${offerPrice}${currency} ${listPrice}${currency}`,
      dimension67: offerPrice < listPrice ? 'on sale' : 'full price',
      dimension80: type,
      dimension83: formattedCategory && formattedCategory[0],
      dimension89: generalProductId,
      dimension100: index + 1,
    };
  });
};

const isViaPromoFilterModule = (router) => {
  const queryParams = getQueryParamsFromUrl(router && router.asPath);
  return queryParams && queryParams.viaModule && queryParams.viaModule[0] === 'promoFilter';
};

const initiatePageLoadTracker = (
  products,
  breadCrumbs,
  router,
  trackPageLoad,
  initialValues,
  miscObj
) => {
  const { filterCount, storeId, totalProductsCount, currency } = miscObj;
  const productsFormatted = formatProductsData(products, breadCrumbs);
  const productsFormattedEE = formatProductsDataEE(products, breadCrumbs, currency);
  const {
    query: { searchType = searchConstants.NAVIGATION_CATEGORY, shopall },
  } = router;
  const icid = getIcidValue(router && router.asPath);
  const viaPromoFilter = isViaPromoFilterModule(router);

  if (products.length > 0) {
    setProp('eVar30', products.length);
    if (searchType !== searchConstants.NAVIGATION_CATEGORY) {
      setProp('eVar94', '+1');
    }
    const productListingPageTrackerData = getProductListingPageTrackData(
      breadCrumbs,
      {
        products: productsFormatted,
        filterCount,
        shopall,
        totalProductsCount,
      },
      searchType,
      initialValues,
      '',
      '',
      viaPromoFilter
    );
    if (
      searchType &&
      searchType === searchConstants.ANALYTICS_TYPEAHEAD_CATEGORY &&
      productListingPageTrackerData &&
      productListingPageTrackerData.customEvents
    ) {
      productListingPageTrackerData.customEvents.push('event20');
    }
    if (icid) {
      productListingPageTrackerData.customEvents = ['event91', 'event92', 'event81', 'event80'];
      productListingPageTrackerData.internalCampaignId = icid;
      productListingPageTrackerData.pageNavigationText = '';
      productListingPageTrackerData.departmentList = '';
      productListingPageTrackerData.pageSearchType = '';
    }
    if (storeId) {
      productListingPageTrackerData.storeId = storeId;
    }
    trackPageLoad(productListingPageTrackerData);
    fireEnhancedEcomm({
      eventName: 'productimpression',
      productsObj: productsFormattedEE,
      eventType: 'impressions',
      pageName: 'PLP',
      currCode: currency,
    });
  }
};

const isProductCountZeroForBot = (setResStatusNotFound, deviceBot, totalProductsCount) =>
  setResStatusNotFound && deviceBot && totalProductsCount === '0';

const getPLPRecommendation = (
  entityCategoryName,
  abPlpPersonalizedCategories,
  isAbPlpPersonalized
) => {
  const isL2OrL3Category =
    entityCategoryName.split(':').length === 2 || entityCategoryName.split(':').length === 3;
  const isAbCategoryEnabled = abPlpPersonalizedCategories.indexOf(entityCategoryName) !== -1;
  return isL2OrL3Category && isAbPlpPersonalized && isAbCategoryEnabled;
};

const isPageNotFound = (isLoadingMore, totalProductsCount) =>
  !isLoadingMore && totalProductsCount === '0' && isClient();

class ProductListView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      resultsExist: false,
      isProductsLoadedZero: false,
      showBannerRecos: false,
      showRecosBannerClass: '',
    };
  }

  componentDidMount() {
    const {
      productsBlock,
      loadedProductCount,
      shouldPlpScrollState,
      resetPlpScrollState,
      isPlpPdpAnchoringEnabled,
    } = this.props;
    const { resultsExist } = this.state;
    if (loadedProductCount === 0) {
      this.setState({
        isProductsLoadedZero: true,
      });
    }
    const initialProductBlock = (productsBlock && productsBlock[0]) || [];
    if (initialProductBlock.length >= MINIMUM_RESULTS_TO_MEASURE && !resultsExist) {
      this.setState({
        resultsExist: true,
      });
    }

    /** Show content by default in mobile web */
    if (isPlpPdpAnchoringEnabled && isMobileWeb() && shouldPlpScrollState) {
      scrollToBreadCrumb();
      resetPlpScrollState();
    }

    this.setState({
      showBannerRecos: true,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate(prevProps) {
    const {
      products,
      breadCrumbs,
      router,
      trackPageLoad,
      filterCount,
      initialValues,
      storeId,
      totalProductsCount,
      currency,
      shouldPlpScrollState,
      resetPlpScrollState,
      isPlpPdpAnchoringEnabled,
    } = this.props;

    const { products: prevProducts } = prevProps;
    const miscObj = {
      filterCount,
      storeId,
      totalProductsCount,
      currency,
    };
    const {
      components: {
        '/ProductListing': {
          props: { isServerAvailable = false },
        },
      },
    } = router;
    if (prevProducts !== products && breadCrumbs.length > 0 && !isServerAvailable) {
      initiatePageLoadTracker(products, breadCrumbs, router, trackPageLoad, initialValues, miscObj);
    }
    if (isPlpPdpAnchoringEnabled && shouldPlpScrollState && isMobileWeb()) {
      scrollToBreadCrumb();
      resetPlpScrollState();
    }
    this.setResultsState(products);
  }

  componentWillUnmount() {
    this.props.updateFormValue('bopisStoreId', []);
  }

  setResultsState = (products) => {
    const { resultsExist } = this.state;
    if (!resultsExist && products.length >= MINIMUM_RESULTS_TO_MEASURE) {
      this.setState({
        resultsExist: true,
      });
    }
  };

  setResultsVisiblePerfTimer = () => {
    const { resultsExist } = this.state;
    return resultsExist && <RenderPerf.Measure name={RESULTS_VISIBLE} />;
  };

  showCategoryFilters = () => {
    const {
      plpTopPromos,
      isPlpCategoryFiltersEnabled,
      categoryFilters,
      categoryFilterEnabledList = [],
      entityCategoryName = '',
    } = this.props;
    const categoryFilterListLowerCased = categoryFilterEnabledList.map((value) =>
      value.toLowerCase()
    );

    const isEnabledForCategory =
      categoryFilterListLowerCased.indexOf(entityCategoryName.toLowerCase()) !== -1;
    return (
      (!plpTopPromos || (plpTopPromos && plpTopPromos.length === 0)) &&
      isPlpCategoryFiltersEnabled &&
      categoryFilters &&
      categoryFilters.length > 1 &&
      isEnabledForCategory
    );
  };

  showL3Carousel = () => {
    const { isABTestForStickyFilter } = this.props;
    return isClient() && isABTestForStickyFilter;
  };

  show404Banner = (is404AbTest, mon404, labels) => {
    let pageName = 'plpGYM';
    if (isTCP()) pageName = 'plpTCP';
    if (is404AbTest && mon404)
      return (
        <>
          <Banner404 labels={labels} pageName={pageName} isMobileWeb={isMobileWeb()} />
        </>
      );
    return null;
  };

  showNewCarousel = () => {
    const { isPLPCarouselEnabled, genericCarousel, plpTopPromos, showCarouselInspiteOfPromo } =
      this.props;
    const noPromoConfigured = !plpTopPromos || (plpTopPromos && plpTopPromos.length === 0);
    return (
      isClient() &&
      isPLPCarouselEnabled &&
      genericCarousel &&
      genericCarousel.carouselData &&
      genericCarousel.carouselData.length > 1 &&
      (noPromoConfigured || showCarouselInspiteOfPromo)
    );
  };

  getQueryVariable = (asPath, searchKey) => {
    const query = asPath && asPath.split('?') && asPath.split('?')[1];
    const vars = query && query.split('&');
    if (vars) {
      for (let i = 0; i < vars.length; i += 1) {
        const pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) === searchKey) {
          return decodeURIComponent(pair[1]);
        }
      }
    }
    return null;
  };

  checkIfUrlMatchWithPattern = (path) => {
    const { redirect404Patterns } = this.props;
    let result = '';
    if (redirect404Patterns) {
      Object.keys(redirect404Patterns).forEach((k) => {
        const index = path && path.indexOf(`${k}`);
        if (index > -1) result = redirect404Patterns[k];
      });
    }
    return result;
  };

  redirectToPage = (path) => {
    const urlToRedirect = `${window.location.protocol}//${window.location.host}${path}`;
    window.location = urlToRedirect;
  };

  getBannerRecosProducts = (products) => {
    const { showRecosBannerClass } = this.state;
    if (products.length < 1 && showRecosBannerClass === '') {
      this.setState({ showRecosBannerClass: 'hideBannerRecos' });
    } else if (showRecosBannerClass !== '' && products.length > 0) {
      this.setState({ showRecosBannerClass: '' });
    }
  };

  renderBreadCrumbs = (breadCrumbs) => {
    const { totalProductsCount, slpLabels, isABTestForStickyFilter } = this.props;

    return (
      <Row>
        <Col className="fixed-bread-crumb-height" colSize={{ small: 6, medium: 8, large: 12 }}>
          {breadCrumbs.length ? (
            <div className="bread-crumb">
              <FixedBreadCrumbs crumbs={breadCrumbs} separationChar=">" />
            </div>
          ) : null}
        </Col>
        {isABTestForStickyFilter && (
          <Col colSize={{ small: 6, medium: 8, large: 12 }} className="new-count-section">
            <NewLoadedProductsCount
              totalProductsCount={totalProductsCount}
              showingItemsLabel={slpLabels}
              breadCrumbs={breadCrumbs}
            />
          </Col>
        )}
      </Row>
    );
  };

  getCustomProductLength = () => {
    if (isClient() && getViewportInfo().isTablet) {
      return 3;
    }
    return 4;
  };

  renderProductsCount = () => {
    const { totalProductsCount, slpLabels, isABTestForStickyFilter } = this.props;
    return (
      !isABTestForStickyFilter && (
        <Col colSize={{ small: 6, medium: 8, large: 12 }} className="show-count-section">
          <LoadedProductsCount
            totalProductsCount={totalProductsCount}
            showingItemsLabel={slpLabels}
          />
        </Col>
      )
    );
  };

  // eslint-disable-next-line
  renderPLP = () => {
    const {
      className,
      productsBlock,
      currentNavIds,
      navTree,
      breadCrumbs,
      filters,
      totalProductsCount,
      initialValues,
      filtersLength,
      longDescription,
      seoHeaderCopyText,
      labels,
      accessibilityLabels,
      labelsFilter,
      categoryId,
      formValues,
      getProducts,
      onSubmit,
      sortLabels,
      slpLabels,
      banner404Labels,
      onPickUpOpenClick,
      isFilterBy,
      currencyAttributes,
      currency,
      isLoadingMore,
      plpTopPromos,
      plpGridPromos,
      plpHorizontalPromos,
      asPathVal,
      isSearchListing,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      isLoggedIn,
      isPlcc,
      pageItems,
      products,
      store,
      pageNameProp,
      pageSectionProp,
      pageSubSectionProp,
      trackPageLoad,
      isSearchPage,
      deviceBot,
      router,
      setResStatusNotFound,
      isFavoriteView,
      entityCategoryName,
      filterCount,
      wishList,
      storeId,
      categoryFilters,
      isPlpCategoryFiltersEnabled,
      asPath,
      pageUrl,
      is404AbTest,
      isAbPlpPersonalized,
      abPlpPersonalizedCategories,
      isSeoHeaderEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isPlpBannerRecoEnabled,
      isDynamicBadgeEnabled,
      isABTestForStickyFilter,
      isPlpGridEnabled,
      isPLPCarouselEnabled,
      genericCarousel,
      isPromoApplied,
      dynamicPromoModules,
      isSecureImageFlagEnabled,
      alternateBrand,
      isShowBrandNameEnabled,
      isBopisFilterEnabled,
      isBopisFilterOn,
      updateFormValue,
      setBopisFilterStateActn,
      resetSuggestedStores,
      defaultStore,
      ...otherProps
    } = this.props;
    const { showBannerRecos, showRecosBannerClass } = this.state;
    const filterMaps = filterQuantityLabels(filters, slpLabels);
    if (isProductCountZeroForBot(setResStatusNotFound, deviceBot, totalProductsCount)) {
      setResStatusNotFound();
    }

    if (isPageNotFound(isLoadingMore, totalProductsCount)) {
      const result = this.checkIfUrlMatchWithPattern(window.location.pathname);
      if (result !== '') return this.redirectToPage(result);
      return <>{labels.pageNotFound}</>;
    }

    const recommendationAttributes = {
      variations: 'moduleO',
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.PLP,
      showLoyaltyPromotionMessage: false,
      headerAlignment: 'left',
      categoryName: entityCategoryName,
      accordionHeader: false,
    };
    const mon404 = this.getQueryVariable(asPath, 'mon404');
    const showPLPRecommendation = getPLPRecommendation(
      entityCategoryName,
      abPlpPersonalizedCategories,
      isAbPlpPersonalized
    );

    let customProductsLength = 4;

    customProductsLength = this.getCustomProductLength();

    const bannerWidth = { width: '100%' };
    const showGenericCarousel = this.showNewCarousel();
    return (
      <div className={className}>
        {this.renderBreadCrumbs(breadCrumbs)}
        <Row>
          <Col colSize={{ small: 6, medium: 8, large: 2 }}>
            {isABTestForStickyFilter ? (
              <div className="sidebar desktop-filter-sidebar">
                <ProductListingFiltersForm
                  filtersMaps={filterMaps}
                  totalProductsCount={totalProductsCount}
                  initialValues={initialValues}
                  filtersLength={filtersLength}
                  labels={labelsFilter}
                  onSubmit={onSubmit}
                  formValues={formValues}
                  getProducts={getProducts}
                  sortLabels={sortLabels}
                  slpLabels={slpLabels}
                  isFilterBy={isFilterBy}
                  isLoadingMore={isLoadingMore}
                  isABTestForStickyFilter={isABTestForStickyFilter}
                  isBopisFilterOn={isBopisFilterOn}
                />
              </div>
            ) : (
              <div className="sidebar">
                {navTree && (
                  <>
                    <GlobalNavigationMenuDesktopL2
                      navigationTree={navTree}
                      activeCategoryIds={currentNavIds}
                    />
                  </>
                )}
              </div>
            )}
          </Col>
          <Col colSize={{ small: 6, medium: 8, large: 10 }}>
            <Row>
              <Col style={bannerWidth} colSize={{ small: 6, medium: 8, large: 10 }}>
                {this.show404Banner(is404AbTest, mon404, banner404Labels)}
              </Col>
            </Row>
            <Row fullBleed>
              <L3NavigationCarousel
                categoryFilters={categoryFilters}
                asPath={asPath}
                accessibilityLabels={accessibilityLabels}
                showCarousel={this.showL3Carousel()}
                variation="mobile"
              />
            </Row>
            <div>
              {this.showCategoryFilters() && (
                <PromoFilter
                  categoryFilters={categoryFilters}
                  asPath={asPath}
                  accessibilityLabels={accessibilityLabels}
                />
              )}
              {showGenericCarousel && (
                <L3NavigationCarousel
                  categoryFilters={genericCarousel && genericCarousel.carouselData}
                  carouselTitle={
                    (genericCarousel && genericCarousel.carouselTitle) ||
                    labels.plpGenericCarouselDefaultTitle
                  }
                  asPath={asPath}
                  accessibilityLabels={accessibilityLabels}
                  showCarousel={showGenericCarousel}
                  showPLPGenericCarousel
                />
              )}
            </div>
            {plpTopPromos.length > 0 && (
              <PromoModules
                plpTopPromos={plpTopPromos}
                asPath={asPathVal}
                isLoggedIn={isLoggedIn}
                isPlcc={isPlcc}
                accessibilityLabels={accessibilityLabels}
                viaModule={entityCategoryName}
                showEssentials
                dynamicPromoModules={dynamicPromoModules}
              />
            )}
            {plpTopPromos.length > 0 && <RenderPerf.Measure name={PROMOTION_VISIBLE} />}
            {showBannerRecos &&
              isPlpBannerRecoEnabled.length > 0 &&
              isPlpBannerRecoEnabled[0].isNewPlpExperience && (
                <Row>
                  <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                    <div className={`banner-plp-recommendation-wrapper ${showRecosBannerClass}`}>
                      <Recommendations
                        customProductsLength={customProductsLength}
                        inheritedStyles={bannerRecommendationStyles}
                        portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.PLP_PRODUCTS}
                        carouselConfigProps={CarouselOptions.CAROUSEL_OPTIONS}
                        isBannerRecommendation
                        getRecommendationProducts={this.getBannerRecosProducts}
                        recommendationBannerHTML={isPlpBannerRecoEnabled[1].plpRecommBanner}
                        {...recommendationAttributes}
                        starRatingSize={{ small: 82, large: 82 }}
                        sequence="1"
                        showPeek={{ small: true, medium: false }}
                      />
                    </div>
                  </Col>
                </Row>
              )}
            {showPLPRecommendation && (
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <div className="recommendation-wrapper">
                  <Recommendations
                    customProductsLength={customProductsLength}
                    headerLabel={labels.plpPersonalized}
                    inheritedStyles={recommendationStyles}
                    portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.PLP_PRODUCTS}
                    carouselConfigProps={CarouselOptions.CAROUSEL_OPTIONS}
                    {...recommendationAttributes}
                    sequence="1"
                    showPeek={{ small: true, medium: false }}
                  />
                </div>
              </Col>
            )}
            {isSeoHeaderEnabled && (
              <Col colSize={{ large: 12, medium: 8, small: 6 }}>
                <RichText className="seo-header-text-block" richTextHtml={seoHeaderCopyText} />
              </Col>
            )}
            {isBopisFilterEnabled && (
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <BopisFilter
                  isBopisFilterOn={isBopisFilterOn}
                  labels={labels}
                  formValues={formValues}
                  getProducts={getProducts}
                  onSubmit={onSubmit}
                  updateFormValue={updateFormValue}
                  setBopisFilterStateActn={setBopisFilterStateActn}
                  defaultStoreData={defaultStore}
                  resetSuggestedStores={resetSuggestedStores}
                />
              </Col>
            )}
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <div
                className={` filter-section ${
                  isABTestForStickyFilter ? 'sticky-filter-enabled ' : ''
                }`}
                id="filterWrapper"
              >
                <ProductListingFiltersForm
                  filtersMaps={filterMaps}
                  totalProductsCount={totalProductsCount}
                  initialValues={initialValues}
                  filtersLength={filtersLength}
                  labels={labelsFilter}
                  onSubmit={onSubmit}
                  formValues={formValues}
                  getProducts={getProducts}
                  sortLabels={sortLabels}
                  slpLabels={slpLabels}
                  isFilterBy={isFilterBy}
                  isLoadingMore={isLoadingMore}
                  isABTestForStickyFilter={isABTestForStickyFilter}
                  isBopisFilterOn={isBopisFilterOn}
                />
                <L3NavigationCarousel
                  categoryFilters={categoryFilters}
                  asPath={asPath}
                  accessibilityLabels={accessibilityLabels}
                  showCarousel={this.showL3Carousel()}
                  variation="desktop"
                />
                {/* UX timer */}
                <RenderPerf.Measure name={CONTROLS_VISIBLE} />
              </div>
            </Col>
            {this.renderProductsCount()}
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <ProductsGrid
                productsBlock={productsBlock}
                products={products}
                labels={labels}
                currency={currency}
                currencyAttributes={currencyAttributes}
                isLoadingMore={isLoadingMore}
                isSearchListing={isSearchListing}
                plpGridPromos={plpGridPromos}
                plpHorizontalPromos={plpHorizontalPromos}
                getProducts={getProducts}
                asPathVal={asPathVal}
                AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                isLoggedIn={isLoggedIn}
                isPlcc={isPlcc}
                pageNameProp={pageNameProp}
                pageSectionProp={pageNameProp}
                pageSubSectionProp={pageNameProp}
                isSearchPage={isSearchPage}
                isFavoriteView={isFavoriteView}
                totalProductsCount={totalProductsCount}
                wishList={wishList}
                onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                router={router}
                deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                isPlpGridEnabled={isPlpGridEnabled}
                isPromoApplied={isPromoApplied}
                breadCrumbs={breadCrumbs}
                isSecureImageFlagEnabled={isSecureImageFlagEnabled}
                alternateBrand={alternateBrand}
                isShowBrandNameEnabled={isShowBrandNameEnabled}
                isBopisFilterOn={isBopisFilterOn}
                {...otherProps}
              />
              {/* UX timer */}
              {this.setResultsVisiblePerfTimer()}
              {/* Skeleton placeholder */}
              {isLoadingMore ? <PLPSkeleton col={20} /> : null}
            </Col>

            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <ReadMore
                description={longDescription}
                labels={labels}
                className={`${className} seo-text`}
              />
            </Col>
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              {categoryId ? <SpotlightContainer categoryId={categoryId} /> : null}
            </Col>
          </Col>
        </Row>
      </div>
    );
  };

  renderEmptyCategoryBanner = () => {
    const {
      className,
      currentNavIds,
      navTree,
      breadCrumbs,
      labels,
      entityCategoryName,
      plpSeoInfo,
      longDescription,
      isBopisFilterOn,
      formValues,
      getProducts,
      onSubmit,
      updateFormValue,
      setBopisFilterStateActn,
      isBopisFilterEnabled,
      resetSuggestedStores,
    } = this.props;
    return (
      <>
        <EmptyCategoryBanner
          className={className}
          currentNavIds={currentNavIds}
          navTree={navTree}
          breadCrumbs={breadCrumbs}
          labels={labels}
          entityCategoryName={entityCategoryName}
          plpSeoInfo={plpSeoInfo}
          longDescription={longDescription}
          isBopisFilterOn={isBopisFilterOn}
          formValues={formValues}
          getProducts={getProducts}
          onSubmit={onSubmit}
          updateFormValue={updateFormValue}
          setBopisFilterStateActn={setBopisFilterStateActn}
          isBopisFilterEnabled={isBopisFilterEnabled}
          resetSuggestedStores={resetSuggestedStores}
        />
      </>
    );
  };

  render() {
    const { totalProductsCount } = this.props;
    const { isProductsLoadedZero } = this.state;
    if (totalProductsCount === 0 || (isServer() && isProductsLoadedZero)) {
      return this.renderEmptyCategoryBanner();
    }
    return this.renderPLP();
  }
}

ProductListView.propTypes = {
  className: PropTypes.string,
  productsBlock: PropTypes.arrayOf(PropTypes.shape({})),
  longDescription: PropTypes.string,
  seoHeaderCopyText: PropTypes.string,
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  accessibilityLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  /* eslint-disable */
  currentNavIds: PropTypes.arrayOf(PropTypes.shape({})),
  categoryId: PropTypes.string,
  navTree: PropTypes.shape({}),
  breadCrumbs: PropTypes.arrayOf(PropTypes.shape({})),
  filters: PropTypes.shape({}),
  totalProductsCount: PropTypes.string,
  initialValues: PropTypes.shape({}),
  filtersLength: PropTypes.shape({}),
  labelsFilter: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  getProducts: PropTypes.func,
  onSubmit: PropTypes.func,
  formValues: PropTypes.shape({}).isRequired,
  onPickUpOpenClick: PropTypes.func.isRequired,
  sortLabels: PropTypes.arrayOf(PropTypes.shape({})),
  slpLabels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  banner404Labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  isFilterBy: PropTypes.bool.isRequired,
  currencyAttributes: PropTypes.shape({}).isRequired,
  currency: PropTypes.string,
  isLoadingMore: PropTypes.bool,
  plpTopPromos: PropTypes.arrayOf(
    PropTypes.shape({
      // Only including the most important property
      moduleName: PropTypes.string,
    })
  ),
  asPathVal: PropTypes.string,
  isSearchListing: PropTypes.bool,
  plpGridPromos: PropTypes.shape({}),
  plpHorizontalPromos: PropTypes.shape({}),
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  isPlcc: PropTypes.bool,
  isFavoriteView: PropTypes.bool,
  isOnModelAbTestPlp: PropTypes.bool,
  entityCategoryName: PropTypes.string,
  isSeoHeaderEnabled: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  isPlpBannerRecoEnabled: PropTypes.arrayOf(PropTypes.any),
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isABTestForStickyFilter: PropTypes.bool,
  isPlpGridEnabled: PropTypes.bool,
  isPromoApplied: PropTypes.bool,
  breadCrumbs: PropTypes.shape({}),
};

ProductListView.defaultProps = {
  className: '',
  productsBlock: [],
  longDescription: [],
  seoHeaderCopyText: '',
  currentNavIds: [],
  navTree: {},
  breadCrumbs: [],
  filters: {},
  totalProductsCount: '',
  initialValues: {},
  filtersLength: {},
  categoryId: '',
  labels: {},
  accessibilityLabels: {},
  labelsFilter: {},
  sortLabels: [],
  slpLabels: {},
  banner404Labels: {},
  isFilterBy: true,
  isLoadingMore: true,
  currency: 'USD',
  plpTopPromos: [],
  asPathVal: '',
  isSearchListing: false,
  plpGridPromos: {},
  plpHorizontalPromos: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  isLoggedIn: false,
  isPlcc: false,
  isFavoriteView: false,
  isOnModelAbTestPlp: false,
  entityCategoryName: '',
  isSeoHeaderEnabled: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isPlpBannerRecoEnabled: [],
  isDynamicBadgeEnabled: false,
  isABTestForStickyFilter: false,
  isPlpGridEnabled: false,
  isPromoApplied: false,
  breadCrumbs: [],
};

export default withStyles(ProductListView, ProductListingStyle);
