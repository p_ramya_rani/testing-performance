/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import isEqual from 'lodash/isEqual';
import { ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { getLabelValue, filterQuantityLabels } from '@tcp/core/src/utils/utils';
import { navigateToNestedRoute, isIOS } from '@tcp/core/src/utils/utils.app';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import L3NavigationCarousel from '@tcp/core/src/components/common/molecules/L3NavigationCarousel/views/L3NavigationCarousel.view.native';
import NavigationCarousel from '../../../../../../../mobileapp/src/components/features/content/Navigation/molecules/NavigationCarousel';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import withStyles from '../../../../common/hoc/withStyles.native';
import ProductList from '../molecules/ProductList/views';
import {
  styles,
  PageContainer,
  ListHeaderContainer,
  FilterContainer,
  EmptyView,
  RowContainer,
  ItemCountContainer,
  BannerWrapper,
  ContentWrapper,
  PlpView,
  PromoContainerForRedesign,
  FilterViewContainer,
} from '../styles/ProductListing.style.native';
import FilterModal from '../molecules/FilterModal';
import { FACETS_FIELD_KEY } from '../../../../../services/abstractors/productListing/productListing.utils';
import PLPSkeleton from '../../../../common/atoms/PLPSkeleton';
import PLPQRScannerAnimation from '../molecules/PLPQRScannerAnimation';
import PromoModules from '../../../../common/organisms/PromoModules';
import SpotlightContainer from '../molecules/Spotlight/container/Spotlight.container';

const navigatePlpPage = (navigation) => {
  navigateToNestedRoute(navigation, 'PlpStack', 'Navigation');
};

const getSecondBreadCrumb = (breadCrumbs) => {
  let breadCrumbObj = {};
  if (breadCrumbs.length === 1) {
    breadCrumbObj = breadCrumbs[1] || {};
  } else if (breadCrumbs.length > 1) {
    breadCrumbObj = breadCrumbs[2] || {};
  }
  return breadCrumbObj;
};

export const getNoProductBanner = (text, entityCategoryName, breadCrumbs, navigation) => {
  const pluralMapping = {
    'TODDLER GIRL': 'Toddler Girls',
    'TODDLER BOY': 'Toddler Boys',
    'Family Shop': 'Family Shops',
    'Uniform Shop': 'Uniform Shops',
    GIRL: 'Girls',
    BOY: 'Boys',
  };

  const firstBreadCrumbObj = breadCrumbs[0] || {};
  const { displayName: display1 = '' } = firstBreadCrumbObj;

  const secondBreadCrumbObj = getSecondBreadCrumb(breadCrumbs);
  const { displayName: display2 = '' } = secondBreadCrumbObj;

  let str = text;
  let departmentStr = '<Department Name>';
  [str] = str.split(departmentStr);
  const mapObj = {
    '<Department Name>': display1,
    '<Category Name>': display2,
  };
  str = str.replace(/<Category Name>/gi, (matched) => {
    return mapObj[matched];
  });
  departmentStr = departmentStr.replace(/<Department Name>/gi, (matched) => {
    return mapObj[matched];
  });
  const dept = departmentStr;
  if (dept in pluralMapping) {
    departmentStr = pluralMapping[dept];
  }

  return (
    <>
      <ContentWrapper>
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs36"
          fontWeight="bold"
          color="white"
          textAlign="center"
          text={`${str.split('?')[0]}?`.toUpperCase()}
        />
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs24"
          fontWeight="regular"
          color="white"
          textAlign="center"
          text={str.split('#')[0].split('?')[1]}
        />
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs24"
          fontWeight="regular"
          color="white"
          textAlign="left"
          textDecoration="underline"
          text={str.split('#')[1]}
          onPress={() => {
            navigatePlpPage(navigation);
          }}
        />

        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs24"
          fontWeight="regular"
          color="white"
          textAlign="center"
          text={str.split('#')[2]}
        />
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs22"
          textAlign="center"
          fontWeight="bold"
          color="white"
          text={`${departmentStr}!`.toUpperCase()}
        />
      </ContentWrapper>
    </>
  );
};

const renderItemCountView = (
  itemCount,
  labelsFavorite,
  isBothTcpAndGymProductAreAvailable,
  displayAltFilterView
) => {
  if (itemCount === undefined && !displayAltFilterView) {
    return <EmptyView />;
  }

  return (
    <RowContainer
      displayAltFilterView={displayAltFilterView}
      margins={displayAltFilterView ? '0' : '12px 0 0 0'}
    >
      {isBothTcpAndGymProductAreAvailable ? (
        <BodyCopy
          dataLocator="fav_brand_title"
          mobileFontFamily="secondary"
          fontSize="fs14"
          fontWeight="regular"
          color="gray.1700"
          text={getLabelValue(labelsFavorite, 'lbl_fav_brand')}
        />
      ) : (
        <EmptyView />
      )}
      <ItemCountContainer>
        <BodyCopy
          dataLocator="pdp_product_badges"
          fontFamily="secondary"
          fontSize="fs14"
          fontWeight="extrabold"
          color="gray.900"
          text={itemCount}
        />
        <BodyCopy
          margin="0 0 0 4px"
          dataLocator="pdp_product_badges"
          mobileFontFamily="secondary"
          fontSize="fs14"
          fontWeight="regular"
          color="gray.900"
          text="Items"
        />
      </ItemCountContainer>
    </RowContainer>
  );
};

const renderAlternateFilterView = (changeFilterViewABTest, isFavorite) => {
  return changeFilterViewABTest && !isFavorite;
};

const getAppliedFiltersCount = (initialValues) => {
  let count = 0;

  // returns count for the applied filters.
  // eslint-disable-next-line
  for (let key in initialValues) {
    count += key.toLowerCase() !== FACETS_FIELD_KEY.sort ? initialValues[key].length : 0;
  }
  return count;
};

const spotlightWrapper = (categoryId) => {
  return categoryId && !isIOS() ? <SpotlightContainer categoryId={categoryId} /> : null;
};

const getTotalBanner = (
  totalProductsCount,
  navigation,
  labeltext,
  shopText,
  entityCategoryName,
  isLoadingMore,
  breadCrumbs
) => {
  return (
    <>
      {totalProductsCount === 0 && (
        <BannerWrapper>
          {getNoProductBanner(labeltext, entityCategoryName, breadCrumbs, navigation)}
        </BannerWrapper>
      )}
      {isLoadingMore ? <PLPSkeleton col={2} /> : null}
    </>
  );
};

const onRenderHeader = (data) => {
  const {
    filters,
    labelsFilter,
    onSubmit,
    getProducts,
    navigation,
    sortLabels,
    totalProductsCount,
    isFavorite,
    onFilterSelection,
    onSortSelection,
    filteredId,
    renderBrandFilter,
    setSelectedFilter,
    selectedFilterValue,
    isKeepModalOpen,
    isLoadingMore,
    labelsFavorite,
    isBothTcpAndGymProductAreAvailable,
    filtersLength,
    initialValues,
    resetForm,
    changeFilterViewABTest,
    trackClickFilter,
  } = data;
  let appliedfilters = false;
  let appliedFiltersCount = 0;
  appliedfilters =
    filtersLength &&
    Object.keys(filtersLength).some((key) => {
      return filtersLength[key] > 0;
    });

  appliedFiltersCount = getAppliedFiltersCount(initialValues);
  const displayAltFilterView = renderAlternateFilterView(changeFilterViewABTest, isFavorite);
  const productCountView = renderItemCountView(
    totalProductsCount,
    labelsFavorite,
    isBothTcpAndGymProductAreAvailable,
    displayAltFilterView
  );
  return (
    <ListHeaderContainer>
      {(totalProductsCount > 1 || appliedfilters || isFavorite) && (
        <FilterModal
          filters={filters}
          labelsFilter={labelsFilter}
          onSubmit={onSubmit}
          getProducts={getProducts}
          navigation={navigation}
          sortLabels={sortLabels}
          isFavorite={isFavorite}
          onFilterSelection={onFilterSelection}
          onSortSelection={onSortSelection}
          filteredId={filteredId}
          setSelectedFilter={setSelectedFilter}
          selectedFilterValue={selectedFilterValue}
          isKeepModalOpen={isKeepModalOpen}
          isLoadingMore={isLoadingMore}
          appliedFiltersCount={appliedFiltersCount}
          resetForm={resetForm}
          displayAltFilterView={displayAltFilterView}
          renderItemCountView={productCountView}
          totalProductsCount={totalProductsCount}
          trackClickFilter={trackClickFilter}
        />
      )}
      {}
      {displayAltFilterView ? null : (
        <>
          {productCountView}
          {renderBrandFilter && renderBrandFilter()}
        </>
      )}
    </ListHeaderContainer>
  );
};

const renderPromModules = (
  isSearchListing,
  plpTopPromos,
  navigation,
  isPlcc,
  isLoggedIn,
  breadCrumbs
) => {
  return (
    <PromoModules
      plpTopPromos={plpTopPromos}
      navigation={navigation}
      isPlcc={isPlcc}
      isLoggedIn={isLoggedIn}
      breadCrumbs={breadCrumbs}
    />
  );
};

class ProductListView extends React.Component {
  componentDidMount = () => {
    const { totalProductsCount, navigation } = this.props;
    if (navigation) {
      navigation.setParams({
        disableHeaderBottomWidth: true,
        totalProductCount: totalProductsCount,
      });
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate(prevProps) {
    const { totalProductsCount, navigation } = this.props;
    if (!isEqual(prevProps.totalProductsCount, totalProductsCount)) {
      navigation.setParams({
        totalProductCount: totalProductsCount,
      });
    }
  }

  showCategoryCarousel = () => {
    const { navigation, appCategoryCarouselL3, plpTopPromos } = this.props;
    let subCategories = navigation && navigation.getParam('navigationObj');
    const promos = plpTopPromos && plpTopPromos.filter((promo) => promo && promo.moduleName);
    subCategories =
      subCategories &&
      subCategories.filter(
        (item) => item.categoryContent && item.categoryContent.displayToCustomer
      );
    return (
      appCategoryCarouselL3 &&
      subCategories &&
      subCategories.length > 2 &&
      !(promos && promos.length > 0)
    );
  };

  renderNavigationCarousel = (title, navigation) => {
    return (
      this.showCategoryCarousel() && <NavigationCarousel title={title} navigation={navigation} />
    );
  };

  getPLPRecommendation = (abPlpPersonalizedCategories, isAbPlpPersonalized, breadCrumbs = []) => {
    const isL2OrL3Category = breadCrumbs.length >= 2;
    const entityCategoryName = breadCrumbs.map((breadCrumb) => breadCrumb.displayName).join(':');
    const isAbCategoryEnabled = abPlpPersonalizedCategories.indexOf(entityCategoryName) !== -1;
    return isL2OrL3Category && isAbPlpPersonalized && isAbCategoryEnabled;
  };

  showNewCarousel = (carouselDataArray) => {
    const { isPLPCarouselEnabled, plpTopPromos, showCarouselInspiteOfPromo } = this.props;
    const noPromoConfigured = !plpTopPromos || (plpTopPromos && plpTopPromos.length === 0);
    return (
      isPLPCarouselEnabled &&
      carouselDataArray &&
      carouselDataArray.length > 1 &&
      (noPromoConfigured || showCarouselInspiteOfPromo)
    );
  };

  getFilterMaps = (isFavoritePage, filters, slpLabels) =>
    !isFavoritePage ? filterQuantityLabels(filters, slpLabels) : filters;

  skeletonCheck = (isDataLoading, isKeepModalOpen, showCustomLoader) =>
    isDataLoading && !isKeepModalOpen && !showCustomLoader;

  getTitle = (navigation) => navigation && navigation.getParam('title');

  renderMainItems = () => {
    const {
      breadCrumbs,
      navigation,
      isDataLoading,
      isLoggedIn,
      isPlcc,
      plpTopPromos,
      isSearchListing,
      isKeepModalOpen,
      showCustomLoader,
      labels,
      entityCategoryName,
      isAbPlpPersonalized,
      abPlpPersonalizedCategories,
      genericCarousel,
      accessibilityLabels,
      isNewReDesignProductTile,
    } = this.props;
    const title = this.getTitle(navigation);

    if (this.skeletonCheck(isDataLoading, isKeepModalOpen, showCustomLoader))
      return <PLPSkeleton col={4} />;

    const recommendationAttributes = {
      variation: 'moduleO',
      navigation,
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.PLP,
      isHeaderAccordion: false,
      categoryName: entityCategoryName,
    };
    const showPLPRecommendation = this.getPLPRecommendation(
      abPlpPersonalizedCategories,
      isAbPlpPersonalized,
      breadCrumbs
    );

    const showGenericCarousel = this.showNewCarousel(genericCarousel.carouselData);

    return (
      <>
        {showGenericCarousel && (
          <L3NavigationCarousel
            categoryFilters={genericCarousel.carouselData}
            carouselTitle={
              (genericCarousel && genericCarousel.carouselTitle) ||
              labels.plpGenericCarouselDefaultTitle
            }
            accessibilityLabels={accessibilityLabels}
            showCarousel={showGenericCarousel}
            showPLPGenericCarousel
            navigation={navigation}
            title={title}
          />
        )}
        <PromoContainerForRedesign isNewReDesignProductTile={isNewReDesignProductTile}>
          {renderPromModules(
            isSearchListing,
            plpTopPromos,
            navigation,
            isPlcc,
            isLoggedIn,
            breadCrumbs
          )}
        </PromoContainerForRedesign>
        <>
          {showPLPRecommendation && (
            <Recommendations
              headerLabel={labels.plpPersonalized}
              portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.PLP_PRODUCTS}
              {...recommendationAttributes}
              sequence="2"
            />
          )}
        </>
        {!showGenericCarousel && this.renderNavigationCarousel(title, navigation)}
      </>
    );
  };

  render() {
    const {
      products,
      filters,
      labelsFilter,
      labelsLogin,
      breadCrumbs,
      onPressFilter,
      onPressSort,
      onSubmit,
      getProducts,
      navigation,
      sortLabels,
      scrollToTop,
      onPickUpOpenClick,
      isPickupModalOpen,
      totalProductsCount,
      isDataLoading,
      isFavorite,
      onFilterSelection,
      onSortSelection,
      filteredId,
      renderBrandFilter,
      margins,
      paddings,
      onAddItemToFavorites,
      isLoggedIn,
      isPlcc,
      isLoadingMore,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      setSelectedFilter,
      selectedFilterValue,
      plpTopPromos,
      isSearchListing,
      isKeepModalOpen,
      showCustomLoader,
      labelsFavorite,
      isBothTcpAndGymProductAreAvailable,
      renderMoveToList,
      filtersLength,
      updateAppTypeHandler,
      QRAnimationURL,
      resetCustomLoader,
      labels,
      loadedProductCount,
      initialValues,
      categoryId,
      onScrollHandler,
      wishList,
      resetForm,
      changeFilterViewABTest,
      entityCategoryName,
      slpLabels,
      isAbPlpPersonalized,
      abPlpPersonalizedCategories,
      isBrierleyPromoEnabled,
      isFavoritePage,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      genericCarousel,
      accessibilityLabels,
      isNewReDesignProductTile,
      alternateBrand,
      trackClickFilter,
      ...otherProps
    } = this.props;
    const filterMaps = this.getFilterMaps(isFavoritePage, filters, slpLabels);
    const title = this.getTitle(navigation);

    if (this.skeletonCheck(isDataLoading, isKeepModalOpen, showCustomLoader))
      return <PLPSkeleton col={4} />;

    const headerData = {
      filters: filterMaps,
      labelsFilter,
      onSubmit,
      getProducts,
      navigation,
      sortLabels,
      totalProductsCount,
      isFavorite,
      onFilterSelection,
      onSortSelection,
      filteredId,
      renderBrandFilter,
      setSelectedFilter,
      selectedFilterValue,
      plpTopPromos,
      isKeepModalOpen,
      isLoadingMore,
      labelsFavorite,
      isBothTcpAndGymProductAreAvailable,
      filtersLength,
      initialValues,
      resetForm,
      changeFilterViewABTest,
      labels,
      entityCategoryName,
      trackClickFilter,
    };

    return showCustomLoader ? (
      <PLPQRScannerAnimation
        url={QRAnimationURL}
        navigation={navigation}
        resetCustomLoader={resetCustomLoader}
        isOpen={showCustomLoader}
      />
    ) : (
      <PlpView isNewReDesignProductTile={isNewReDesignProductTile}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          onScroll={onScrollHandler}
          scrollEventThrottle={500}
          stickyHeaderIndices={[1]}
        >
          {this.renderMainItems()}

          {totalProductsCount > 0 && (
            <FilterViewContainer>
              <FilterContainer changeFilterViewABTest={changeFilterViewABTest}>
                {onRenderHeader(headerData)}
              </FilterContainer>
            </FilterViewContainer>
          )}

          <PageContainer
            margins={margins}
            paddings={paddings}
            isNewReDesignProductTile={isNewReDesignProductTile}
          >
            <>
              {totalProductsCount > 0 && (
                <ProductList
                  getProducts={getProducts}
                  navigation={navigation}
                  products={products}
                  title={title}
                  scrollToTop={scrollToTop}
                  totalProductsCount={totalProductsCount}
                  isFavorite={isFavorite}
                  onAddItemToFavorites={onAddItemToFavorites}
                  isLoggedIn={isLoggedIn}
                  labelsLogin={labelsLogin}
                  AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                  removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                  isSearchListing={isSearchListing}
                  renderMoveToList={renderMoveToList}
                  updateAppTypeHandler={updateAppTypeHandler}
                  labels={labels}
                  loadedProductCount={loadedProductCount}
                  isPlcc={isPlcc}
                  labelsFavorite={labelsFavorite}
                  onScrollEnd={onScrollHandler}
                  noCarousel
                  wishList={wishList}
                  breadCrumbs={breadCrumbs}
                  isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                  onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                  deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                  isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                  isNewReDesignProductTile={isNewReDesignProductTile}
                  alternateBrand={alternateBrand}
                  {...otherProps}
                />
              )}

              {getTotalBanner(
                totalProductsCount,
                navigation,
                labels.interimSoldout,
                labels.shopnow,
                entityCategoryName,
                isLoadingMore,
                breadCrumbs
              )}
            </>
            {spotlightWrapper(categoryId)}
          </PageContainer>
        </ScrollView>
      </PlpView>
    );
  }
}

ProductListView.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({})),
  filters: PropTypes.shape({}),
  breadCrumbs: PropTypes.arrayOf(PropTypes.shape({})),
  labelsFilter: PropTypes.shape({}),
  onPressFilter: PropTypes.func.isRequired,
  onPressSort: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isPickupModalOpen: PropTypes.bool.isRequired,
  getProducts: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  sortLabels: PropTypes.arrayOf(PropTypes.shape({})),
  scrollToTop: PropTypes.bool.isRequired,
  onPickUpOpenClick: PropTypes.func,
  totalProductsCount: PropTypes.number.isRequired,
  isDataLoading: PropTypes.bool.isRequired,
  isFavorite: PropTypes.bool,
  onFilterSelection: PropTypes.func,
  onSortSelection: PropTypes.func,
  filteredId: PropTypes.string,
  renderBrandFilter: PropTypes.func,
  margins: PropTypes.string,
  paddings: PropTypes.string,
  onAddItemToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  labelsLogin: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  isLoadingMore: PropTypes.bool.isRequired,
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  selectedFilterValue: PropTypes.shape({}).isRequired,
  setSelectedFilter: PropTypes.func.isRequired,
  plpTopPromos: PropTypes.arrayOf(PropTypes.shape({})),
  isSearchListing: PropTypes.bool,
  isKeepModalOpen: PropTypes.bool,
  labelsFavorite: PropTypes.shape({}),
  isBothTcpAndGymProductAreAvailable: PropTypes.bool,
  renderMoveToList: PropTypes.func,
  addToBagEcom: PropTypes.func,
  isPlcc: PropTypes.bool,
  filtersLength: PropTypes.number,
  showCustomLoader: PropTypes.bool,
  QRAnimationURL: PropTypes.string.isRequired,
  resetCustomLoader: PropTypes.func,
  updateAppTypeHandler: PropTypes.func.isRequired,
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  loadedProductCount: PropTypes.bool.isRequired,
  initialValues: PropTypes.shape({}).isRequired,
  categoryId: PropTypes.string,
  onScrollHandler: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  wishList: PropTypes.shape({}).isRequired,
  changeFilterViewABTest: PropTypes.bool,
  entityCategoryName: PropTypes.string.isRequired,
  slpLabels: PropTypes.shape({}).isRequired,
  appCategoryCarouselL3: PropTypes.bool,
  isAbPlpPersonalized: PropTypes.bool,
  abPlpPersonalizedCategories: PropTypes.shape([]),
  isBrierleyPromoEnabled: PropTypes.bool,
  isFavoritePage: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  genericCarousel: PropTypes.shape({}),
  accessibilityLabels: PropTypes.shape({}),
  isPLPCarouselEnabled: PropTypes.bool,
  showCarouselInspiteOfPromo: PropTypes.bool,
  isNewReDesignProductTile: PropTypes.bool,
};

ProductListView.defaultProps = {
  products: [],
  filters: {},
  breadCrumbs: [],
  labelsFilter: {},
  navigation: {},
  sortLabels: [],
  onPickUpOpenClick: () => {},
  isFavorite: false,
  onFilterSelection: () => {},
  onSortSelection: () => {},
  filteredId: 'ALL',
  renderBrandFilter: null,
  margins: null,
  paddings: null,
  onAddItemToFavorites: null,
  isLoggedIn: false,
  labelsLogin: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  plpTopPromos: [],
  isSearchListing: false,
  isKeepModalOpen: false,
  labelsFavorite: {},
  isBothTcpAndGymProductAreAvailable: false,
  renderMoveToList: () => {},
  addToBagEcom: () => {},
  isPlcc: false,
  filtersLength: 0,
  showCustomLoader: false,
  resetCustomLoader: () => {},
  labels: {},
  categoryId: '',
  changeFilterViewABTest: false,
  appCategoryCarouselL3: false,
  isAbPlpPersonalized: false,
  abPlpPersonalizedCategories: [],
  isBrierleyPromoEnabled: false,
  isFavoritePage: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  isDynamicBadgeEnabled: {},
  genericCarousel: {},
  accessibilityLabels: {},
  isPLPCarouselEnabled: false,
  showCarouselInspiteOfPromo: false,
  isNewReDesignProductTile: false,
};

export default withStyles(ProductListView, styles);
export { ProductListView as ProductListViewVanilla };
