// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { DamImage, BodyCopy } from '../../../../../../common/atoms';

const getImageStyle = (props) => {
  const { theme, width, height, resizeMode, borderRadius, selected } = props;
  const { colorPalette } = theme;
  const borderWidth = selected ? 2 : 0;
  return `
    width: ${width};
    height: ${height};
    resize-mode: ${resizeMode};
    border-color: ${selected ? colorPalette.white : 'transparent'};
    border-width: ${borderWidth};
    border-radius: ${borderRadius};
  `;
};
const getImageBorderStyle = (props) => {
  const {
    theme,
    selected,
    width,
    height,
    borderWidth,
    borderRadius,
    overflow,
    imageWithText,
    isFromFilterPills,
  } = props;
  const { colorPalette } = theme;
  const borderColor = selected ? colorPalette.gray[900] : colorPalette.gray[1300];
  const borderWidthCalc = selected ? borderWidth + 1 : borderWidth;
  const borderWidthValue = selected ? 2 : borderWidth;
  return `
    align-items: center;
    justify-content: center;
    width: ${imageWithText && selected ? width + 4 : width + 1};
    height: ${imageWithText && selected ? height + 4 : height + 1};
    border-color: ${borderColor};
    border-width: ${isFromFilterPills ? borderWidthValue : borderWidthCalc};
    border-radius: ${borderRadius + 1};
    overflow: ${overflow};
    ${props.imageWithText && !props.isFromFilterPills ? 'margin-right: 8px;' : '0px'};
  `;
};

const ImageTouchableOpacity = styled.TouchableOpacity`
  ${getImageBorderStyle}
`;

const IconContainerView = styled.View`
  ${getImageBorderStyle}
`;

const ImageComp = styled(DamImage)`
  ${getImageStyle}
`;

const ImageOpacityContainer = styled.TouchableOpacity`
  flex-direction: ${(props) => (props.isFromFilterPills ? 'column' : 'row')};
  margin-bottom: 20;
  margin-left: ${(props) => (props.isFromFilterPills ? 0 : 10)};
  align-items: center;
  justify-content: ${(props) => (props.isFromFilterPills ? 'center' : 'flex-start')};
  flex: ${(props) => (props.isFromFilterPills ? 0.2 : 'none')};
`;

const StyledBodyCopy = styled(BodyCopy)`
  ${(props) => (props.textStyles ? { ...props.textStyles } : {})};
`;

export {
  ImageComp,
  ImageTouchableOpacity,
  IconContainerView,
  ImageOpacityContainer,
  StyledBodyCopy,
};
