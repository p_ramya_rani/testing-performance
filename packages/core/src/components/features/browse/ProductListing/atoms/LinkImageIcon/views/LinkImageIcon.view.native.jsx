// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import {
  ImageComp,
  ImageTouchableOpacity,
  IconContainerView,
  ImageOpacityContainer,
  StyledBodyCopy,
} from '../styles/LinkImageIcon.style.native';

const LinkImageIcon = (props) => {
  const {
    onPress,
    uri,
    selected,
    width,
    height,
    resizeMode,
    borderWidth,
    borderRadius,
    imageWidth,
    imageHeight,
    name,
    isGiftCard,
    itemBrand,
    isProductImage,
    overflow,
    imageWithText,
    fontFamily,
    fontSize,
    fontWeight,
    textStyles,
    primaryBrand,
    alternateBrand,
    isFromFilterPills,
  } = props;

  const imageCompAccessibilityRole = `image ${name}`;
  const imgConfig = isGiftCard ? 'w_125' : 't_swatch_img,w_50,h_50,c_thumb,g_auto:0';
  return imageWithText ? (
    <ImageOpacityContainer
      onPress={onPress}
      accessibilityRole="button"
      accessibilityLabel={imageCompAccessibilityRole}
      accessibilityState={{ selected }}
      isFromFilterPills={isFromFilterPills}
    >
      <IconContainerView
        selected={selected}
        width={width}
        height={height}
        resizeMode={resizeMode}
        borderWidth={borderWidth}
        borderRadius={borderRadius}
        overflow={overflow}
        imageWithText
        isFromFilterPills={isFromFilterPills}
      >
        <ImageComp
          accessibilityRole="image"
          url={uri}
          swatchConfig={imgConfig}
          selected={selected}
          width={imageWidth || width}
          height={imageHeight || height}
          resizeMode={resizeMode}
          borderWidth={borderWidth}
          borderRadius={borderRadius}
          itemBrand={itemBrand}
          isProductImage={isProductImage}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
        />
      </IconContainerView>

      <StyledBodyCopy
        fontFamily={fontFamily}
        fontSize={fontSize}
        text={name}
        fontWeight={fontWeight}
        selected={selected}
        textStyles={textStyles}
      />
    </ImageOpacityContainer>
  ) : (
    <ImageTouchableOpacity
      onPress={onPress}
      accessibilityRole="button"
      accessibilityLabel={imageCompAccessibilityRole}
      accessibilityState={{ selected }}
      selected={selected}
      width={width}
      height={height}
      resizeMode={resizeMode}
      borderWidth={borderWidth}
      borderRadius={borderRadius}
      overflow={overflow}
    >
      <ImageComp
        accessibilityRole="image"
        url={uri}
        swatchConfig={imgConfig}
        selected={selected}
        width={imageWidth || width}
        height={imageHeight || height}
        resizeMode={resizeMode}
        borderWidth={borderWidth}
        borderRadius={borderRadius}
        itemBrand={itemBrand}
        isProductImage={isProductImage}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
      />
    </ImageTouchableOpacity>
  );
};

LinkImageIcon.propTypes = {
  uri: PropTypes.string.isRequired,
  selected: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
  resizeMode: PropTypes.string,
  borderWidth: PropTypes.number,
  borderRadius: PropTypes.number,
  onPress: PropTypes.func,
  imageWidth: PropTypes.number,
  imageHeight: PropTypes.number,
  name: PropTypes.string,
  isGiftCard: PropTypes.bool,
  itemBrand: PropTypes.string.isRequired,
  isProductImage: PropTypes.bool,
  overflow: PropTypes.string,
  imageWithText: PropTypes.bool,
  fontFamily: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.string,
  textStyles: PropTypes.shape({}),
};

LinkImageIcon.defaultProps = {
  selected: false,
  width: 23,
  height: 23,
  resizeMode: 'contain',
  borderWidth: 1,
  borderRadius: 12,
  onPress: null,
  imageWidth: null,
  imageHeight: null,
  name: '',
  isGiftCard: false,
  isProductImage: true,
  overflow: 'visible',
  imageWithText: false,
  fontFamily: 'secondary',
  fontSize: 'fs14',
  fontWeight: 'regular',
  textStyles: {},
};

export default withStyles(LinkImageIcon);
export { LinkImageIcon as LinkImageIconVanilla };
