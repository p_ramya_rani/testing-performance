// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { CHECKOUT_ROUTES } from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import ApplyRewardsCreditCardStyle from '../style/ApplyCardPage.style';
import PLCCForm from '../molecules/Form/PLCCForm/PLCCForm';
import ApplicationInProgress from '../molecules/Common/UnderProgressApplication';
import ApprovedPLCCApplicationView from '../molecules/Common/ApprovedPLCCApplication';
import ApprovedPLCCApplicationViewNew from '../molecules/Common/ApprovedPLCCApplication/ApprovedPLCCApplicationNew';
import ExistingPLCCUserView from '../molecules/Common/ExistingPLCCUser';
import constants from '../RewardsCard.constants';

let PAGE_CATEGORY = '';
const isCheckoutBag = (router) => {
  let isCheckoutAndBagPage = false;
  const { bagPage, pickupPage, shippingPage, billingPage, reviewPage, internationalCheckout } =
    CHECKOUT_ROUTES;
  const checkoutPageURL = [
    bagPage.asPath,
    pickupPage.asPath,
    shippingPage.asPath,
    billingPage.asPath,
    reviewPage.asPath,
    internationalCheckout.asPath,
  ];

  for (let i = 0; i < checkoutPageURL.length; i += 1) {
    if (router.asPath.indexOf(checkoutPageURL[i]) > -1) {
      isCheckoutAndBagPage = true;
    }
  }

  return isCheckoutAndBagPage;
};

const getApplyCardLayoutView = ({ props = {}, renderViewInfo = {} }) => {
  const {
    cartOrderItems,
    applicationStatus,
    plccData,
    labels,
    submitPLCCForm,
    isPLCCModalFlow,
    isRegisteredUser,
    newUserTempPass,
    changePassword,
    isCreateAccountAfterGuestUserApplyForPLCCCardEnabled,
    isCreateAdsAccountFailed,
    isPLCCModalOpen,
  } = props;

  if (
    applicationStatus === constants.APPLICATION_STATE_EXISTING ||
    (renderViewInfo.plccUser &&
      isPLCCModalFlow &&
      applicationStatus !== constants.APPLICATION_STATE_APPROVED)
  ) {
    return (
      <ExistingPLCCUserView
        labels={labels}
        existingCustomerDetails={plccData && plccData.plcc_existing_customer_details}
        isPLCCModalFlow={isPLCCModalFlow}
        resetPLCCResponse={renderViewInfo.resetPLCCApplicationStatus}
        isRtpsFlow={renderViewInfo.isRtpsFlow}
        togglePLCCModal={renderViewInfo.togglePLCCModal}
        closePLCCModal={renderViewInfo.closePLCCModal}
        isCheckoutBagFlag={renderViewInfo.isCheckoutBagFlag}
        approvedPLCCData={renderViewInfo.approvedPLCCData}
        isGuest={renderViewInfo.isGuest}
      />
    );
  }
  if (applicationStatus === constants.APPLICATION_STATE_PENDING) {
    return (
      <ApplicationInProgress
        labels={labels}
        isPLCCModalFlow={isPLCCModalFlow}
        resetPLCCResponse={renderViewInfo.resetPLCCApplicationStatus}
        isRtpsFlow={renderViewInfo.isRtpsFlow}
        togglePLCCModal={renderViewInfo.togglePLCCModal}
        closePLCCModal={renderViewInfo.closePLCCModal}
        setAnalyticsVars={renderViewInfo.setAnalyticsVars}
        isCheckoutBagFlag={renderViewInfo.isCheckoutBagFlag}
      />
    );
  }

  if (applicationStatus === constants.APPLICATION_STATE_APPROVED && isCreateAdsAccountFailed) {
    return (
      <ApprovedPLCCApplicationView
        isGuest={renderViewInfo.isGuest}
        labels={labels}
        plccData={plccData}
        isPLCCModalFlow={isPLCCModalFlow}
        approvedPLCCData={renderViewInfo.approvedPLCCData}
        resetPLCCResponse={renderViewInfo.resetPLCCApplicationStatus}
        isRtpsFlow={renderViewInfo.isRtpsFlow}
        togglePLCCModal={renderViewInfo.togglePLCCModal}
        closePLCCModal={renderViewInfo.closePLCCModal}
        isCheckoutBagFlag={renderViewInfo.isCheckoutBagFlag}
        fullPageAuthEnabled={renderViewInfo.fullPageAuthEnabled}
      />
    );
  }

  if (applicationStatus === constants.APPLICATION_STATE_APPROVED) {
    return isCreateAccountAfterGuestUserApplyForPLCCCardEnabled &&
      (renderViewInfo.isGuest || !isRegisteredUser) ? (
      // eslint-disable-next-line react/jsx-indent
      <ApprovedPLCCApplicationViewNew
        isGuest={renderViewInfo.isGuest}
        labels={labels}
        plccData={plccData}
        isPLCCModalFlow={isPLCCModalFlow}
        approvedPLCCData={renderViewInfo.approvedPLCCData}
        resetPLCCResponse={renderViewInfo.resetPLCCApplicationStatus}
        isRtpsFlow={renderViewInfo.isRtpsFlow}
        togglePLCCModal={renderViewInfo.togglePLCCModal}
        closePLCCModal={renderViewInfo.closePLCCModal}
        isCheckoutBagFlag={renderViewInfo.isCheckoutBagFlag}
        fullPageAuthEnabled={renderViewInfo.fullPageAuthEnabled}
        changePassword={changePassword}
        newUserTempPass={newUserTempPass}
        isRegisteredUser={isRegisteredUser}
        isPLCCModalOpen={isPLCCModalOpen}
      />
    ) : (
      <ApprovedPLCCApplicationView
        isGuest={renderViewInfo.isGuest}
        labels={labels}
        plccData={plccData}
        isPLCCModalFlow={isPLCCModalFlow}
        approvedPLCCData={renderViewInfo.approvedPLCCData}
        resetPLCCResponse={renderViewInfo.resetPLCCApplicationStatus}
        isRtpsFlow={renderViewInfo.isRtpsFlow}
        togglePLCCModal={renderViewInfo.togglePLCCModal}
        closePLCCModal={renderViewInfo.closePLCCModal}
        isCheckoutBagFlag={renderViewInfo.isCheckoutBagFlag}
        fullPageAuthEnabled={renderViewInfo.fullPageAuthEnabled}
      />
    );
  }
  return (
    <div className="plcc-form">
      <PLCCForm
        plccData={plccData}
        labels={labels}
        isPLCCModalFlow={isPLCCModalFlow}
        onSubmit={submitPLCCForm}
        initialValues={renderViewInfo.profileInfo}
        applicationStatus={applicationStatus}
        isRtpsFlow={renderViewInfo.isRtpsFlow}
        closePLCCModal={renderViewInfo.closePLCCModal}
        cartOrderItems={cartOrderItems}
        formErrorMessage={renderViewInfo.formErrorMessage}
        mapboxAutocompleteTypesParam={renderViewInfo.mapboxAutocompleteTypesParam}
        mapboxSwitch={renderViewInfo.mapboxSwitch}
        pageCategory={PAGE_CATEGORY}
      />
    </div>
  );
};

const ApplyCardLayoutView = ({
  applicationStatus,
  plccData,
  labels,
  submitPLCCForm,
  isPLCCModalFlow,
  plccUser,
  isGuest,
  profileInfo,
  approvedPLCCData,
  resetPLCCApplicationStatus,
  isRtpsFlow,
  closePLCCModal,
  togglePLCCModal,
  cartOrderItems,
  setAnalyticsVars,
  router,
  formErrorMessage,
  fullPageAuthEnabled,
  mapboxAutocompleteTypesParam,
  mapboxSwitch,
  pageCategory,
  isRegisteredUser,
  newUserTempPass,
  changePassword,
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled,
  isCreateAdsAccountFailed,
  isPLCCModalOpen,
}) => {
  const isCheckoutBagFlag = isCheckoutBag(router);
  PAGE_CATEGORY = pageCategory;

  useEffect(() => {
    if (applicationStatus === constants.APPLICATION_STATE_APPROVED) {
      const applyNowModal = document.querySelector('.ApplyNowPLCCModal .TCPModal__InnerContent');
      if (applyNowModal) {
        applyNowModal.scrollTop = 0;
      }
    }
  }, [applicationStatus]);
  return (
    <ApplyRewardsCreditCardStyle
      isPLCCModalFlow={isPLCCModalFlow}
      applicationStatus={applicationStatus}
    >
      {getApplyCardLayoutView({
        props: {
          cartOrderItems,
          applicationStatus,
          plccData,
          labels,
          submitPLCCForm,
          isPLCCModalFlow,
          isRegisteredUser,
          newUserTempPass,
          changePassword,
          isCreateAccountAfterGuestUserApplyForPLCCCardEnabled,
          isCreateAdsAccountFailed,
          isPLCCModalOpen,
        },
        renderViewInfo: {
          isGuest,
          plccUser,
          profileInfo,
          approvedPLCCData,
          resetPLCCApplicationStatus,
          isRtpsFlow,
          closePLCCModal,
          togglePLCCModal,
          setAnalyticsVars,
          isCheckoutBagFlag,
          formErrorMessage,
          fullPageAuthEnabled,
          mapboxAutocompleteTypesParam,
          mapboxSwitch,
        },
      })}
    </ApplyRewardsCreditCardStyle>
  );
};

ApplyCardLayoutView.propTypes = {
  plccData: PropTypes.shape({}).isRequired,
  submitPLCCForm: PropTypes.func.isRequired,
  applicationStatus: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  isPLCCModalFlow: PropTypes.bool.isRequired,
  plccUser: PropTypes.bool.isRequired,
  profileInfo: PropTypes.shape({}).isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  isGuest: PropTypes.bool.isRequired,
  resetPLCCApplicationStatus: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool.isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  cartOrderItems: PropTypes.shape([]),
  setAnalyticsVars: PropTypes.func.isRequired,
  router: PropTypes.func.isRequired,
  formErrorMessage: PropTypes.shape({}),
  fullPageAuthEnabled: PropTypes.bool,
  mapboxAutocompleteTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  pageCategory: PropTypes.string,
  isRegisteredUser: PropTypes.bool,
  newUserTempPass: PropTypes.string,
  changePassword: PropTypes.func,
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled: PropTypes.bool,
  isCreateAdsAccountFailed: PropTypes.bool,
  isPLCCModalOpen: PropTypes.bool,
};

ApplyCardLayoutView.defaultProps = {
  cartOrderItems: [],
  formErrorMessage: {},
  fullPageAuthEnabled: false,
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  pageCategory: '',
  isRegisteredUser: true,
  newUserTempPass: '',
  changePassword: () => {},
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled: false,
  isCreateAdsAccountFailed: false,
  isPLCCModalOpen: false,
};

export default ApplyCardLayoutView;
