/* eslint-disable react/jsx-indent */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import PLCCForm from '../molecules/Form/PLCCForm/PLCCForm';
import constants from '../RewardsCard.constants';
import ApplicationInProgress from '../molecules/Common/UnderProgressApplication/ApplicationInProgress.native';
import ExistingPLCCUserView from '../molecules/Common/ExistingPLCCUser/ExistingPLCCUser.view.native';
import ApprovedPLCCApplicationView from '../molecules/Common/ApprovedPLCCApplication/ApprovedPLCCApplication.native';
import ApprovedPLCCApplicationViewNew from '../molecules/Common/ApprovedPLCCApplication/ApprovedPLCCApplicationNew.native';
import AddressVerification from '../../../../common/organisms/AddressVerification/container/AddressVerification.container';

const styleHide = { height: 0 };
const styleVisible = { height: 'auto' };

class ApplyCardLayoutView extends React.PureComponent {
  state = {
    isScrollEnabled: true,
  };

  updateScrollViewState = (status) => {
    const { isScrollEnabled } = this.state;
    if (isScrollEnabled !== status) this.setState({ isScrollEnabled: status });
  };

  renderPLCCView = (
    labels,
    onSubmit,
    applicationStatus,
    bagItems,
    plccData,
    activeErrorField,
    renderViewArgs = {}
  ) => {
    const {
      isRegisteredUser,
      isPlccDone,
      newUserTempPass,
      changePassword,
      isCreateAdsAccountFailed,
      isGuest,
      isCreateAccountAfterGuestUserApplyForPLCCCardEnabled,
    } = renderViewArgs;
    if (applicationStatus === constants.APPLICATION_STATE_PENDING) {
      return (
        <ApplicationInProgress
          labels={labels}
          bagItems={bagItems}
          navigation={renderViewArgs.navigation}
          isRtpsFlow={renderViewArgs.isRtpsFlow}
          togglePLCCModal={renderViewArgs.togglePLCCModal}
          cartOrderItems={renderViewArgs.cartOrderItems}
        />
      );
    }
    if (
      applicationStatus === constants.APPLICATION_STATE_EXISTING ||
      (renderViewArgs.plccUser && applicationStatus !== constants.APPLICATION_STATE_APPROVED)
    ) {
      return (
        <ExistingPLCCUserView
          bagItems={bagItems}
          labels={labels}
          existingCustomerDetails={plccData && plccData.plcc_existing_customer_details}
          navigation={renderViewArgs.navigation}
          isRtpsFlow={renderViewArgs.isRtpsFlow}
          togglePLCCModal={renderViewArgs.togglePLCCModal}
        />
      );
    }
    if (applicationStatus === constants.APPLICATION_STATE_APPROVED && isCreateAdsAccountFailed) {
      return (
        <ApprovedPLCCApplicationView
          bagItems={bagItems}
          isGuest={renderViewArgs.isGuest}
          labels={labels}
          plccData={plccData}
          approvedPLCCData={renderViewArgs.approvedPLCCData}
          navigation={renderViewArgs.navigation}
          isRtpsFlow={renderViewArgs.isRtpsFlow}
          togglePLCCModal={renderViewArgs.togglePLCCModal}
          cartOrderItems={renderViewArgs.cartOrderItems}
        />
      );
    }
    if (applicationStatus === constants.APPLICATION_STATE_APPROVED) {
      return isCreateAccountAfterGuestUserApplyForPLCCCardEnabled &&
        (isGuest || !isRegisteredUser) ? (
        <ApprovedPLCCApplicationViewNew
          bagItems={bagItems}
          isGuest={renderViewArgs.isGuest}
          labels={labels}
          plccData={plccData}
          approvedPLCCData={renderViewArgs.approvedPLCCData}
          navigation={renderViewArgs.navigation}
          isRtpsFlow={renderViewArgs.isRtpsFlow}
          togglePLCCModal={renderViewArgs.togglePLCCModal}
          cartOrderItems={renderViewArgs.cartOrderItems}
          changePassword={changePassword}
          newUserTempPass={newUserTempPass}
          isRegisteredUser={isRegisteredUser}
          isPlccDone={isPlccDone}
        />
      ) : (
        <ApprovedPLCCApplicationView
          bagItems={bagItems}
          isGuest={renderViewArgs.isGuest}
          labels={labels}
          plccData={plccData}
          approvedPLCCData={renderViewArgs.approvedPLCCData}
          navigation={renderViewArgs.navigation}
          isRtpsFlow={renderViewArgs.isRtpsFlow}
          togglePLCCModal={renderViewArgs.togglePLCCModal}
          cartOrderItems={renderViewArgs.cartOrderItems}
        />
      );
    }
    return (
      <PLCCForm
        onSubmit={onSubmit}
        labels={labels}
        plccData={plccData}
        activeErrorField={activeErrorField}
        toggleModal={renderViewArgs.toggleModal}
        initialValues={renderViewArgs.profileInfo}
        isRtpsFlow={renderViewArgs.isRtpsFlow}
        navigation={renderViewArgs.navigation}
        formErrorMessage={renderViewArgs.formErrorMessage}
        isPLCCModalFlow={renderViewArgs.isPLCCModalFlow}
        isScrollEnabled={renderViewArgs.isScrollEnabled}
        updateScrollViewState={this.updateScrollViewState}
        mapboxAutocompleteTypesParam={renderViewArgs.mapboxAutocompleteTypesParam}
        mapboxSwitch={renderViewArgs.mapboxSwitch}
        loyaltyPageName={renderViewArgs.loyaltyPageName}
      />
    );
  };

  onCloseCallBack = (resetPLCCApplicationStatus, closeAddressVerificationModal) => {
    resetPLCCApplicationStatus({ status: null });
    closeAddressVerificationModal();
  };

  render() {
    const {
      plccData,
      activeErrorField,
      labels,
      onSubmit,
      applicationStatus,
      bagItems,
      approvedPLCCData,
      plccUser,
      navigation,
      showAddEditAddressForm,
      submitForm,
      resetPLCCApplicationStatus,
      closeAddressVerificationModal,
      profileInfo,
      closePLCCModal,
      isRtpsFlow,
      togglePLCCModal,
      cartOrderItems = 0,
      formErrorMessage,
      isPLCCModalFlow,
      isGuest,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      loyaltyPageName,
      isRegisteredUser,
      newUserTempPass,
      changePassword,
      isCreateAdsAccountFailed,
      isPlccDone,
      isCreateAccountAfterGuestUserApplyForPLCCCardEnabled,
    } = this.props;
    const { isScrollEnabled } = this.state;
    return (
      <ScrollView scrollEnabled={isScrollEnabled} keyboardShouldPersistTaps="handled">
        <View style={showAddEditAddressForm ? styleVisible : styleHide}>
          <AddressVerification
            onSuccess={submitForm}
            plccOnClose={() =>
              this.onCloseCallBack(resetPLCCApplicationStatus, closeAddressVerificationModal)
            }
          />
        </View>

        <View style={showAddEditAddressForm ? styleHide : styleVisible}>
          {this.renderPLCCView(
            labels,
            onSubmit,
            applicationStatus,
            bagItems,
            plccData,
            activeErrorField,
            {
              approvedPLCCData,
              plccUser,
              navigation,
              toggleModal: closePLCCModal,
              profileInfo,
              isRtpsFlow,
              togglePLCCModal,
              cartOrderItems,
              isGuest,
              isScrollEnabled,
              formErrorMessage,
              isPLCCModalFlow,
              mapboxAutocompleteTypesParam,
              mapboxSwitch,
              loyaltyPageName,
              isRegisteredUser,
              newUserTempPass,
              changePassword,
              isCreateAdsAccountFailed,
              isPlccDone,
              isCreateAccountAfterGuestUserApplyForPLCCCardEnabled,
            }
          )}
        </View>
      </ScrollView>
    );
  }
}

ApplyCardLayoutView.propTypes = {
  plccData: PropTypes.shape({}).isRequired,
  activeErrorField: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  applicationStatus: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
  bagItems: PropTypes.bool.isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  navigation: PropTypes.func.isRequired,
  plccUser: PropTypes.bool.isRequired,
  submitForm: PropTypes.func.isRequired,
  showAddEditAddressForm: PropTypes.bool.isRequired,
  profileInfo: PropTypes.shape({}).isRequired,
  resetPLCCApplicationStatus: PropTypes.func.isRequired,
  closeAddressVerificationModal: PropTypes.func.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool.isRequired,
  cartOrderItems: PropTypes.shape([]),
  isPLCCModalFlow: PropTypes.bool,
  formErrorMessage: PropTypes.shape({}),
  isGuest: PropTypes.bool.isRequired,
  mapboxAutocompleteTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  loyaltyPageName: PropTypes.string,
  isRegisteredUser: PropTypes.bool,
  isPlccDone: PropTypes.bool,
  newUserTempPass: PropTypes.string,
  changePassword: PropTypes.func,
  isCreateAdsAccountFailed: PropTypes.bool,
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled: PropTypes.bool,
};

ApplyCardLayoutView.defaultProps = {
  cartOrderItems: [],
  formErrorMessage: {},
  isPLCCModalFlow: false,
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  loyaltyPageName: 'account',
  isRegisteredUser: false,
  isPlccDone: false,
  newUserTempPass: '',
  changePassword: () => {},
  isCreateAdsAccountFailed: false,
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled: false,
};

export default withNavigation(ApplyCardLayoutView);
export { ApplyCardLayoutView as ApplyCardLayoutViewVanilla };
