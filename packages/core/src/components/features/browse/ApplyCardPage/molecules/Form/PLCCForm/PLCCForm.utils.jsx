// 9fbef606107a605d69c0edbcd8029e5d 
import { getAPIConfig } from '../../../../../../../utils';
/**
 * To load the snare js whenever the form is opened
 * this will not load the js if the js is already present in dom.
 */

const loadScript = () => {
  if (!window.IGLOO) {
    const timer = setTimeout(() => {
      const { RWD_WEB_SNARE_SCRIPT_URL } = getAPIConfig();
      // basic configuration
      window.io_bbout_element_id = 'ioBlackBox'; // populate ioBlackBox in form
      window.io_install_stm = false; // do not install ActiveX
      window.io_install_flash = false; // do not install ActiveX
      window.io_exclude_stm = 12; // exclude Active X on XP and higher

      window.io_submit_element_id = 'plccSubmitButton'; // tie to form's submit button
      window.io_max_wait = 3000; // wait 3 seconds
      window.io_submit_form_id = 'plccForm';

      const hn = document.createElement('script');
      const sc = document.getElementsByTagName('script')[0];

      hn.type = 'text/javascript';
      hn.async = true;
      hn.src = RWD_WEB_SNARE_SCRIPT_URL;
      sc.parentNode.insertBefore(hn, sc);
      clearTimeout(timer);
    }, 3000);
  }
};

export default loadScript;
