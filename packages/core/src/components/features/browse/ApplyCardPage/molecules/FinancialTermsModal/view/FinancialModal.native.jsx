// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, Modal, SafeAreaView } from 'react-native';
import { RichText } from '../../../../../../common/atoms';

import { StyledImage, Touchable, TextContainer } from '../style/FinancialModal.style.native';

const closeImage = require('../../../../../../../../src/assets/close.png');

const scrollViewStyle = { backgroundColor: 'white', marginTop: 35 };

const FinancialPrompt = props => {
  const { closeModal } = props;
  return (
    <SafeAreaView>
      <Modal isOpen onRequestClose={closeModal}>
        <Touchable accessibilityRole="button" onPress={() => closeModal()}>
          <StyledImage source={closeImage} width="15px" height="15px" />
        </Touchable>
        <TextContainer>
          <ScrollView style={scrollViewStyle}>
            <RichText
              source={{
                html:
                  '<div style="width: 100%; height:100%;"><iframe style="width:100%; border:0; min-height: 29000px; height:100%;" title="FINANCIAL TERMS OF YOUR ACCOUNT" class="financial-terms-disclosures" src="https://comenity.net/childrensplace/common/Legal/disclosures.xhtml"></iframe></div>',
              }}
            />
          </ScrollView>
        </TextContainer>
      </Modal>
    </SafeAreaView>
  );
};

FinancialPrompt.propTypes = {
  qrScannerLabels: PropTypes.shape({}).isRequired,
  closeModal: PropTypes.shape({}).isRequired,
};

export { FinancialPrompt as FinancialPromptPromptVanilla };
export default FinancialPrompt;
