// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

export default styled.div`
  .title {
    margin: 34px 0px 27px 0px;
  }

  .financial-terms-disclosures {
    width: 100%;
    border: 1px solid ${props => props.theme.colors.BUTTON.WHITE.BORDER};
    height: 450px;
    @media ${props => props.theme.mediaQuery.large} {
      height: 914px;
    }
  }
`;
