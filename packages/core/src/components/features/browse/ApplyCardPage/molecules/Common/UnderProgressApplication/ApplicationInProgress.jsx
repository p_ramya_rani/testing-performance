// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { BodyCopy, Button, Col, Row } from '../../../../../../common/atoms';
import ApplicationInProgressWrapper from './style/ApplicationInProgress.style';
import { getLabelValue } from '../../../../../../../utils';
import { redirectToBag, redirectToHome, getFooterButtonSize } from '../../../utils/utility';
import { getCartItemCount } from '../../../../../../../utils/cookie.util';

const handleCheckoutClick = (
  isRtpsFlow,
  togglePLCCModal,
  resetPLCCResponse,
  setAnalyticsVars,
  closePLCCModal,
  isCheckoutBagFlag
) => {
  setAnalyticsVars();
  if (isRtpsFlow) {
    togglePLCCModal({ isPLCCModalOpen: false, status: null });
  }
  if (isCheckoutBagFlag) {
    closePLCCModal();
  } else {
    redirectToBag({ resetPLCCResponse, closePLCCModal });
  }
};

const handleContinueClick = (
  isPLCCModalFlow,
  closePLCCModal,
  resetPLCCResponse,
  setAnalyticsVars
) => {
  setAnalyticsVars();
  redirectToHome(isPLCCModalFlow, closePLCCModal, resetPLCCResponse);
};

/**
 * @const ApplicationInProgress
 *
 * @param - labels
 * @description - showcases application in progress screen.
 */

const ApplicationInProgress = ({
  isPLCCModalFlow,
  labels,
  resetPLCCResponse,
  isRtpsFlow,
  togglePLCCModal,
  closePLCCModal,
  setAnalyticsVars,
  isCheckoutBagFlag,
}) => {
  const bagItems = getCartItemCount();
  return (
    <ApplicationInProgressWrapper isPLCCModalFlow={isPLCCModalFlow}>
      <div className="header-image" />
      <BodyCopy
        fontSize="fs22"
        className="card-InProgress-header"
        fontFamily="secondary"
        fontWeight="semibold"
      >
        {getLabelValue(labels, 'lbl_PLCCForm_underProgress')}
      </BodyCopy>
      <BodyCopy fontSize="fs16" fontFamily="secondary" className="in_progress_status_details">
        {getLabelValue(labels, 'lbl_PLCCForm_underProcessDetails')}
      </BodyCopy>
      {bagItems ? (
        <Row fullBleed className="submit_plcc_form">
          <Col
            ignoreGutter={{ small: true }}
            colSize={{ large: getFooterButtonSize(isPLCCModalFlow), medium: 4, small: 12 }}
            className="underprogress_checkout_button"
          >
            <ClickTracker
              clickData={{
                eventName: 'loyaltyclick',
                customEvents: ['event114'],
              }}
            >
              <Button
                buttonVariation="fixed-width"
                fill="BLUE"
                type="submit"
                className="underprogress_checkout_button"
                data-locator="submit-plcc-btn"
                onClick={() =>
                  handleCheckoutClick(
                    isRtpsFlow,
                    togglePLCCModal,
                    resetPLCCResponse,
                    setAnalyticsVars,
                    closePLCCModal,
                    isCheckoutBagFlag
                  )
                }
              >
                {getLabelValue(labels, 'lbl_PLCCForm_ctcButton')}
              </Button>
            </ClickTracker>
          </Col>
        </Row>
      ) : null}
      {!isRtpsFlow && (
        <Row fullBleed className="submit_plcc_form">
          <Col
            ignoreGutter={{ small: true }}
            colSize={{ large: getFooterButtonSize(isPLCCModalFlow), medium: 4, small: 12 }}
            className="underprogress_continue_button"
          >
            <ClickTracker
              clickData={{
                eventName: 'loyaltyclick',
                customEvents: ['event115'],
              }}
            >
              <Button
                onClick={() =>
                  handleContinueClick(
                    isPLCCModalFlow,
                    closePLCCModal,
                    resetPLCCResponse,
                    setAnalyticsVars
                  )
                }
                buttonVariation="fixed-width"
                type="submit"
                fill={!bagItems ? 'BLUE' : 'WHITE'}
                centered
                className="existing_continue_button"
              >
                {getLabelValue(labels, 'lbl_PLCCForm_continueShopping')}
              </Button>
            </ClickTracker>
          </Col>
        </Row>
      )}
    </ApplicationInProgressWrapper>
  );
};

ApplicationInProgress.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  isPLCCModalFlow: PropTypes.bool.isRequired,
  resetPLCCResponse: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  setAnalyticsVars: PropTypes.func.isRequired,
  isCheckoutBagFlag: PropTypes.bool.isRequired,
};

export default ApplicationInProgress;
