// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import ChangePasswordContainer from '@tcp/core/src/components/features/account/ChangePassword/container/ChangePassword.container';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import { BodyCopy, RichText, Col, Row, Image } from '../../../../../../common/atoms';
import ApprovedPLCCApplicationViewNewStyled from './style/ApprovedPLCCApplicationNew.style';
import { getLabelValue, getIconPath } from '../../../../../../../utils';
import { redirectToBag, redirectToHomeOrBack, getFooterButtonSize } from '../../../utils/utility';
import { getCartItemCount } from '../../../../../../../utils/cookie.util';
import Notification from '../../../../../../common/molecules/Notification';
import { COUPON_CODE_STATE } from '../../../RewardsCard.constants';

/**
 *
 * @description - return taotal saving and button footer set.
 *
 * @param {contains successfully registered plcc data} approvedPLCCData
 * @param {set of labels to be displayed} labels
 * @param {moduleX content} plccData
 * @param {} bagItems
 */

const redirectToBagOrCloseModal = (isRtpsFlow, resetPLCCResponse, residualProps = {}) => {
  return isRtpsFlow
    ? residualProps.togglePLCCModal({ isPLCCModalOpen: false, status: null })
    : redirectToBag({
        resetPLCCResponse,
        closePLCCModal: residualProps.closePLCCModal,
      });
};
const totalSavingsFooterContainer = (
  bagItems,
  resetPLCCResponse,
  isRtpsFlow,
  labels = {},
  residualProps = {}
) => {
  const continueShopLabel =
    typeof getLabelValue(labels, 'lbl_PLCCForm_continueShopping') === 'string'
      ? getLabelValue(labels, 'lbl_PLCCForm_continueShopping')
      : '';
  const checkoutLabel =
    typeof getLabelValue(labels, 'lbl_PLCCForm_checkout') === 'string'
      ? getLabelValue(labels, 'lbl_PLCCForm_checkout')
      : '';

  return (
    <React.Fragment>
      <Row fullBleed className="cta-container">
        {!isRtpsFlow && (
          <Col
            className={`${
              !bagItems
                ? 'no_bag_items_continue existing_checkout_button'
                : 'existing_checkout_button'
            }`}
            ignoreGutter={{ small: true }}
            colSize={{
              large: getFooterButtonSize(residualProps.isPLCCModalFlow),
              medium: 4,
              small: 12,
            }}
          >
            <Anchor
              handleLinkClick={(e) => {
                e.preventDefault();
                redirectToHomeOrBack(
                  residualProps.isPLCCModalFlow,
                  residualProps.closePLCCModal,
                  () => {}
                );
              }}
              fontSizeVariation="large"
              anchorVariation="secondary"
              underline
              className="elem-mb-LRG continue_btn_plcc"
              noLink
            >
              {continueShopLabel && continueShopLabel.toLowerCase()}
            </Anchor>
          </Col>
        )}
        {bagItems && isRtpsFlow ? (
          <Col
            ignoreGutter={{ small: true }}
            colSize={{
              large: getFooterButtonSize(residualProps.isPLCCModalFlow),
              medium: 4,
              small: 12,
            }}
            className="existing_checkout_button"
          >
            <Anchor
              handleLinkClick={(e) => {
                e.preventDefault();
                redirectToBagOrCloseModal(isRtpsFlow, resetPLCCResponse, residualProps);
              }}
              fontSizeVariation="large"
              anchorVariation="secondary"
              underline
              className="elem-mb-LRG continue_btn_plcc"
              noLink
            >
              {checkoutLabel && checkoutLabel.toLowerCase()}
            </Anchor>
          </Col>
        ) : null}
      </Row>
    </React.Fragment>
  );
};

const getUserNameGreet = ({ userName, labels, classes = '' }) => {
  return (
    <BodyCopy
      component="span"
      fontSize="fs18"
      fontWeight="bold"
      fontFamily="secondary"
      className={`credit_card_heading card-user-name${classes}`}
      aria-label={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
      tabIndex="0"
      textAlign="left"
    >
      {`${userName}!`}
    </BodyCopy>
  );
};

const ApprovedPLCCApplicationViewNew = ({
  labels,
  isPLCCModalFlow,
  approvedPLCCData,
  resetPLCCResponse,
  isRtpsFlow,
  togglePLCCModal,
  closePLCCModal,
  changePassword,
  newUserTempPass,
  isRegisteredUser,
  isPLCCModalOpen,
}) => {
  // For window scroll up to top once component gets loaded.
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, [isRegisteredUser]);
  const bagItems = getCartItemCount();
  const [couponCodeStatus] = useState('');
  const userName =
    approvedPLCCData &&
    approvedPLCCData.address.firstName &&
    approvedPLCCData.address.firstName.toLowerCase();
  let copyStausMessage = '';
  if (couponCodeStatus === COUPON_CODE_STATE.SUCCESS) {
    copyStausMessage = getLabelValue(labels, 'lbl_PLCC_CouponCopy_Success');
  } else if (couponCodeStatus === COUPON_CODE_STATE.ERROR) {
    copyStausMessage = getLabelValue(labels, 'lbl_PLCC_CouponCopy_Fail');
  }
  return (
    <ApprovedPLCCApplicationViewNewStyled
      isPLCCModalFlow={isPLCCModalFlow}
      isRegisteredUser={isRegisteredUser}
    >
      {couponCodeStatus && <Notification status={couponCodeStatus} message={copyStausMessage} />}
      <BodyCopy
        fontSize="fs18"
        fontFamily="secondary"
        className="credit_card_heading congratulation-style hide-on-desktop hide-on-tablet"
        aria-label={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
        tabIndex="0"
        textAlign="left"
      >
        {getLabelValue(labels, 'lbl_PLCCForm_congratulations')}
        {getUserNameGreet({ labels, userName })}
      </BodyCopy>
      <div className="crd-cont">
        <div>
          <div className="header-image-new" />
        </div>
        <div>
          <div className="plcc-card-container">
            <BodyCopy
              fontSize="fs18"
              fontFamily="secondary"
              className="credit_card_heading hide-on-mobile"
              aria-label={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
              tabIndex="0"
              textAlign="left"
            >
              {getLabelValue(labels, 'lbl_PLCCForm_congratulations')}
              {getUserNameGreet({ labels, userName, classes: ' hide-on-mobile' })}
            </BodyCopy>
            <BodyCopy
              fontSize="fs16"
              fontFamily="secondary"
              className="approved-for"
              aria-label={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
              tabIndex="0"
              textAlign="left"
            >
              {getLabelValue(labels, 'lbl_PLCC_you_approved_for')}
            </BodyCopy>
            <BodyCopy
              fontSize="fs16"
              fontFamily="secondary"
              className=""
              aria-label={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
              tabIndex="0"
              textAlign="left"
            >
              <RichText richTextHtml={getLabelValue(labels, 'lbl_PLCC_reward_multiclr')} />
            </BodyCopy>
            <BodyCopy
              fontWeight="bold"
              fontSize="fs18"
              fontFamily="secondary"
              className="approved-for credit-limit"
              tabIndex="0"
              textAlign="left"
            >
              {getLabelValue(labels, 'lbl_PLCCForm_creditLimit_lower')}
              {`$${approvedPLCCData && approvedPLCCData.creditLimit}`}
            </BodyCopy>
          </div>
        </div>
      </div>
      <Row fullBleed className="card-container">
        <Col colSize={{ large: 6, medium: 4.5, small: 12 }} className="no-need-col">
          <div className="no-need-to-wait-card">
            <BodyCopy
              fontSize="fs24"
              fontWeight="bold"
              fontFamily="secondary"
              className="no-need-wait-text"
              textAlign="center"
            >
              {getLabelValue(labels, 'lbl_PLCC_no_need_wait')}
            </BodyCopy>
            <div className="percent-off">
              <div className="promo">{getLabelValue(labels, 'lbl_plcc_approved_offer')}</div>
              <div className="reset-text">
                <RichText richTextHtml={getLabelValue(labels, 'lbl_PLCC_reset_pass_web')} />
              </div>
            </div>
          </div>
        </Col>
        <Col colSize={{ large: 6, medium: 3.5, small: 12 }}>
          <div className="update-pass">
            <BodyCopy
              fontSize="fs14"
              fontFamily="secondary"
              className="grey-color"
              textAlign="left"
            >
              {getLabelValue(labels, 'lbl_PLCC_your_email_address')}
            </BodyCopy>
            <BodyCopy fontSize="fs14" fontFamily="secondary" className="email-id" textAlign="left">
              {approvedPLCCData && approvedPLCCData.emailAddress}
            </BodyCopy>
            <ChangePasswordContainer
              changeTempPassword={changePassword}
              newUserTempPass={newUserTempPass}
              isRegisteredUser={isRegisteredUser}
              emailAddress={approvedPLCCData && approvedPLCCData.emailAddress}
              plccCardId={approvedPLCCData && approvedPLCCData.onFileCardId}
              isRtpsFlow={isRtpsFlow}
              isPLCCModalFlow={isPLCCModalFlow}
              isPLCCModalOpen={isPLCCModalOpen}
              isPLCCFlow
            />
          </div>
        </Col>
      </Row>
      <Row fullBleed className="centered">
        <Col ignoreGutter={{ small: true }} colSize={{ large: 12, medium: 8, small: 12 }}>
          <hr className="horizontal_divider" />
        </Col>
      </Row>
      <Row fullBleed className="centered">
        <Col colSize={{ large: 12, medium: 8, small: 12 }}>
          <BodyCopy
            fontSize="fs16"
            fontWeight="bold"
            className="shiny"
            fontFamily="secondary"
            textAlign="center"
          >
            <RichText richTextHtml={getLabelValue(labels, 'lbl_PLCC_your_shiny')} />
          </BodyCopy>
          <BodyCopy
            fontSize="fs16"
            fontFamily="secondary"
            className="grey-color"
            textAlign="center"
          >
            <Image src={`${getIconPath('plcc-transit')}`} className="arrive-icon" />
            {getLabelValue(labels, 'lbl_PLCC_arrive_in')}
          </BodyCopy>

          <BodyCopy
            fontSize="fs16"
            fontFamily="secondary"
            className="shoping-instore"
            textAlign="center"
          >
            {getLabelValue(labels, 'lbl_PLCC_shopping_in_store')}
          </BodyCopy>
          <BodyCopy fontSize="fs14" fontFamily="secondary" className="" textAlign="center">
            {getLabelValue(labels, 'lbl_PLCC_associate_look_cc')}
          </BodyCopy>
        </Col>
      </Row>
      {totalSavingsFooterContainer(bagItems, resetPLCCResponse, isRtpsFlow, labels, {
        togglePLCCModal,
        isPLCCModalFlow,
        closePLCCModal,
      })}
    </ApprovedPLCCApplicationViewNewStyled>
  );
};

ApprovedPLCCApplicationViewNew.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  isPLCCModalFlow: PropTypes.bool.isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  plccData: PropTypes.shape({}).isRequired,
  resetPLCCResponse: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  newUserTempPass: PropTypes.string,
  changePassword: PropTypes.func,
  isRegisteredUser: PropTypes.bool,
  isPLCCModalOpen: PropTypes.bool,
};

ApprovedPLCCApplicationViewNew.defaultProps = {
  newUserTempPass: '',
  changePassword: () => {},
  isRegisteredUser: false,
  isPLCCModalOpen: false,
};

export default ApprovedPLCCApplicationViewNew;
