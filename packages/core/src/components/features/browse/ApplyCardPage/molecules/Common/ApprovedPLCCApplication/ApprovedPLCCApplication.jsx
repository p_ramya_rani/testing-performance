// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Anchor, BodyCopy, RichText, Button, Col, Row } from '../../../../../../common/atoms';
import ApprovedPLCCApplicationViewStyled from './style/ApprovedPLCCApplication.style';
import { getLabelValue, scrollPage } from '../../../../../../../utils';
import {
  redirectToBag,
  redirectToHome,
  getModalSizeForApprovedPLCC,
  getFooterButtonSize,
} from '../../../utils/utility';
import { getCartItemCount } from '../../../../../../../utils/cookie.util';
import Espot from '../../../../../../common/molecules/Espot';
import Notification from '../../../../../../common/molecules/Notification';
import { COUPON_CODE_STATE } from '../../../RewardsCard.constants';

const EMPTY_DIV = '<div></div>';
/**
 * @constant - CopyToClipboard
 *
 * @param {*} e - Synthentic event being triggered.
 * @param {*} changeStatus - state change dispatch event from useState.
 */
const CopyToClipboard = (e, changeStatus) => {
  e.preventDefault();
  if (window.getSelection) {
    const copyText = document.querySelector('#couponCode');
    copyText.select();
    const timer = setTimeout(() => {
      const result = document.execCommand('copy');
      if (result) {
        changeStatus(COUPON_CODE_STATE.SUCCESS);
      } else {
        changeStatus(COUPON_CODE_STATE.ERROR);
      }
      clearTimeout(timer);
    }, 0);
  } else {
    changeStatus(COUPON_CODE_STATE.ERROR);
  }
};

/**
 *
 * @const getCouponNote
 * @param - rtps - flag to indicate if we are in RTPS flow or WIC flow
 * @param - guest - flag to indicate if guest user or logged in user
 * @param - plccData - text to form the approval modal
 */

const getCouponNote = (plccData, rtps = false, guest = true) => {
  if (!plccData) {
    return <div />;
  }
  const couponNoteText =
    (rtps && plccData.plcc_approved_ps_rtps) || plccData.plcc_approved_ps || EMPTY_DIV;
  const guestCouponNoteText =
    (rtps && plccData.guest_approved_ps_rtps) || plccData.guest_approved_ps || EMPTY_DIV;
  return guest ? (
    <RichText richTextHtml={guestCouponNoteText} />
  ) : (
    <RichText richTextHtml={couponNoteText} />
  );
};

/**
 * @description return coupon code container
 *
 * @param {contains successfully registered plcc data} approvedPLCCData
 * @param {set of labels to be displayed} labels
 * @param {moduleX content} plccData
 */
const getCouponCodeBody = (
  approvedPLCCData,
  isPLCCModalFlow,
  changeStatus,
  isRtpsFlow,
  isGuest,
  labels = {},
  plccData = {}
) => {
  scrollPage();
  return approvedPLCCData && approvedPLCCData.couponCode ? (
    <React.Fragment>
      <Row fullBleed className="centered">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: getModalSizeForApprovedPLCC(isPLCCModalFlow), medium: 8, small: 12 }}
        >
          <Row className="offer-container">
            <Col
              ignoreGutter={{ small: true }}
              colSize={{ large: 9, medium: 6, small: 9 }}
              className="promo_offer_section"
            >
              <BodyCopy
                fontWeight="black"
                fontSize="fs22"
                fontFamily="secondary"
                className="credit_limit_heading"
                aria-label={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
                textAlign="center"
              >
                {getLabelValue(labels, 'lbl_PLCCForm_welcomeOffer')}
              </BodyCopy>
              <input
                className="promo_code"
                value={approvedPLCCData && approvedPLCCData.couponCode}
                id="couponCode"
              />
              <Anchor onClick={e => CopyToClipboard(e, changeStatus)} underline>
                {getLabelValue(labels, 'lbl_PLCCForm_copyToClipboard')}
              </Anchor>
            </Col>
            <Col
              ignoreGutter={{ small: true }}
              colSize={{ large: 3, medium: 2, small: 3 }}
              className="promo-container "
            >
              <div className="promo">{getLabelValue(labels, 'lbl_plcc_approved_offer')}</div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row fullBleed className="centered">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: getModalSizeForApprovedPLCC(isPLCCModalFlow), medium: 8, small: 12 }}
        >
          {getCouponNote(plccData, isRtpsFlow, isGuest)}
        </Col>
      </Row>
      <Row fullBleed className="centered">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: getModalSizeForApprovedPLCC(isPLCCModalFlow), medium: 8, small: 12 }}
        >
          <hr className="horizontal_divider" />
        </Col>
      </Row>
    </React.Fragment>
  ) : null;
};

/**
 *
 * @description - return taotal saving and button footer set.
 *
 * @param {contains successfully registered plcc data} approvedPLCCData
 * @param {set of labels to be displayed} labels
 * @param {moduleX content} plccData
 * @param {} bagItems
 */
const totalSavingsFooterContainer = (
  bagItems,
  resetPLCCResponse,
  isRtpsFlow,
  approvedPLCCData = {},
  plccData = {},
  labels = {},
  residualProps = {}
) => {
  return (
    <React.Fragment>
      {approvedPLCCData && approvedPLCCData.savingAmount > 0 ? (
        <Row fullBleed className="submit_plcc_form">
          <Col ignoreGutter={{ small: true }} colSize={{ large: 6, medium: 8, small: 12 }}>
            <RichText
              richTextHtml={
                plccData &&
                plccData.total_savings_amount &&
                plccData.total_savings_amount.replace('amount', `$${approvedPLCCData.savingAmount}`)
              }
            />
          </Col>
        </Row>
      ) : null}
      {bagItems ? (
        <Row fullBleed className="submit_plcc_form">
          <Col
            ignoreGutter={{ small: true }}
            colSize={{
              large: getFooterButtonSize(residualProps.isPLCCModalFlow),
              medium: 4,
              small: 12,
            }}
            className="existing_checkout_button"
          >
            <Button
              buttonVariation="fixed-width"
              fill="BLUE"
              type="submit"
              className="existing_checkout_button"
              onClick={() =>
                isRtpsFlow
                  ? residualProps.togglePLCCModal({ isPLCCModalOpen: false, status: null })
                  : redirectToBag({
                      resetPLCCResponse,
                      closePLCCModal: residualProps.closePLCCModal,
                    })
              }
            >
              {getLabelValue(labels, 'lbl_PLCCForm_checkout')}
            </Button>
          </Col>
        </Row>
      ) : null}
      {!isRtpsFlow && (
        <Row fullBleed className="submit_buttons_set">
          <Col
            className={`${
              !bagItems
                ? 'no_bag_items_continue existing_checkout_button'
                : 'existing_checkout_button'
            }`}
            ignoreGutter={{ small: true }}
            colSize={{
              large: getFooterButtonSize(residualProps.isPLCCModalFlow),
              medium: 4,
              small: 12,
            }}
          >
            <Button
              onClick={() =>
                redirectToHome(
                  residualProps.isPLCCModalFlow,
                  residualProps.closePLCCModal,
                  () => {}
                )
              }
              buttonVariation="fixed-width"
              type="submit"
              fill={!bagItems ? 'BLUE' : 'WHITE'}
              centered
              className="existing_continue_button"
            >
              {getLabelValue(labels, 'lbl_PLCCForm_continueShopping')}
            </Button>
          </Col>
        </Row>
      )}
    </React.Fragment>
  );
};

/**
 *
 * @const getShippingText
 * @param - rtps - flag to indicate if we are in RTPS flow or WIC flow
 * @param - guest - flag to indicate if guest user or logged in user
 * @param - plccData - text to form the approval modal
 */

const getShippingText = (
  plccData,
  rtps = false,
  guest = true,
  fullPageAuthEnabled = false,
  isPLCCModalFlow = false
) => {
  if (!plccData) {
    return <div />;
  }
  const redirectToFullPageAuth = !isPLCCModalFlow && fullPageAuthEnabled;
  const shippingInfo =
    (rtps && plccData.plcc_shipping_info_rtps) || plccData.plcc_shipping_info || EMPTY_DIV;
  const guestShippingInfo =
    (rtps && plccData.guest_shipping_info_rtps) || plccData.guest_shipping_info || EMPTY_DIV;
  return guest ? (
    <Espot richTextHtml={guestShippingInfo} redirectToFullPageAuth={redirectToFullPageAuth} />
  ) : (
    <RichText richTextHtml={shippingInfo} />
  );
};
/**
 * @const ApprovedPLCCApplicationView
 *
 * @param - labels
 * @param - plccData - comprehensive plcc data for forming view of approved plcc customer.
 * @description - showcases user already holds a plcc card.
 */

const ApprovedPLCCApplicationView = ({
  plccData,
  labels,
  isPLCCModalFlow,
  approvedPLCCData,
  isGuest,
  resetPLCCResponse,
  isRtpsFlow,
  togglePLCCModal,
  closePLCCModal,
  fullPageAuthEnabled,
}) => {
  const bagItems = getCartItemCount();
  const [couponCodeStatus, changeStatus] = useState('');
  let copyStausMessage = '';
  if (couponCodeStatus === COUPON_CODE_STATE.SUCCESS) {
    copyStausMessage = getLabelValue(labels, 'lbl_PLCC_CouponCopy_Success');
  } else if (couponCodeStatus === COUPON_CODE_STATE.ERROR) {
    copyStausMessage = getLabelValue(labels, 'lbl_PLCC_CouponCopy_Fail');
  }
  return (
    <ApprovedPLCCApplicationViewStyled isPLCCModalFlow={isPLCCModalFlow}>
      {couponCodeStatus && <Notification status={couponCodeStatus} message={copyStausMessage} />}
      <div className="header-image" />
      <Row fullBleed className="submit_plcc_form">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: getModalSizeForApprovedPLCC(isPLCCModalFlow), medium: 8, small: 12 }}
          className="congratulations_header"
        >
          <BodyCopy
            fontWeight="bold"
            fontSize="fs22"
            fontFamily="secondary"
            className="credit_card_heading"
            aria-label={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
            tabIndex="0"
            textAlign="center"
          >
            {`${getLabelValue(labels, 'lbl_PLCCForm_congratulations')} ${approvedPLCCData &&
              approvedPLCCData.address.firstName}!`}
          </BodyCopy>
        </Col>
      </Row>
      <Row fullBleed className="centered">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: getModalSizeForApprovedPLCC(isPLCCModalFlow), medium: 8, small: 12 }}
        >
          <BodyCopy
            fontWeight="bold"
            fontSize="fs22"
            fontFamily="secondary"
            className="credit_card_heading"
            aria-label={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
            tabIndex="0"
            textAlign="center"
          >
            <RichText richTextHtml={plccData && plccData.rewards_card_welcome} />
          </BodyCopy>
        </Col>
      </Row>
      <Row fullBleed className="centered">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: getModalSizeForApprovedPLCC(isPLCCModalFlow), medium: 8, small: 12 }}
        >
          <BodyCopy
            fontWeight="extrabold"
            fontSize="fs22"
            fontFamily="secondary"
            className="credit_limit_heading"
            tabIndex="0"
            textAlign="center"
          >
            {getLabelValue(labels, 'lbl_PLCCForm_creditLimit')}
            {`$${approvedPLCCData && approvedPLCCData.creditLimit}`}
          </BodyCopy>
        </Col>
      </Row>
      <Row fullBleed className="centered">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: getModalSizeForApprovedPLCC(isPLCCModalFlow), medium: 8, small: 12 }}
        >
          {getShippingText(plccData, isRtpsFlow, isGuest, fullPageAuthEnabled, isPLCCModalFlow)}
        </Col>
      </Row>
      <Row fullBleed className="centered">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: getModalSizeForApprovedPLCC(isPLCCModalFlow), medium: 8, small: 12 }}
        >
          <hr className="horizontal_divider" />
        </Col>
      </Row>
      {getCouponCodeBody(
        approvedPLCCData,
        isPLCCModalFlow,
        changeStatus,
        isRtpsFlow,
        isGuest,
        labels,
        plccData
      )}
      {totalSavingsFooterContainer(
        bagItems,
        resetPLCCResponse,
        isRtpsFlow,
        approvedPLCCData,
        plccData,
        labels,
        {
          togglePLCCModal,
          isPLCCModalFlow,
          closePLCCModal,
        }
      )}
      <Row fullBleed className="centered">
        <Col
          ignoreGutter={{ small: true }}
          colSize={{ large: 3, medium: 3, small: 12 }}
          className="footer_links"
        >
          <BodyCopy component="span" fontSize="fs12" fontFamily="secondary">
            {getLabelValue(labels, 'lbl_PLCCModal_linksTextPrefix')}
          </BodyCopy>
          <Anchor
            url={getLabelValue(labels, 'lbl_PLCCForm_detailsLink')}
            target="_blank"
            fontSizeVariation="large"
            anchorVariation="primary"
            className="linkIconSeperator"
            underline
          >
            {getLabelValue(labels, 'lbl_PLCCForm_details')}
          </Anchor>
        </Col>
      </Row>
    </ApprovedPLCCApplicationViewStyled>
  );
};

ApprovedPLCCApplicationView.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  isPLCCModalFlow: PropTypes.bool.isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  isGuest: PropTypes.bool.isRequired,
  plccData: PropTypes.shape({}).isRequired,
  resetPLCCResponse: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  fullPageAuthEnabled: PropTypes.bool.isRequired,
};

export default ApprovedPLCCApplicationView;
