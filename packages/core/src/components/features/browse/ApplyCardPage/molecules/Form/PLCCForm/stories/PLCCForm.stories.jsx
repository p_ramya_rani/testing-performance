// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { storiesOf } from '@storybook/react';
import { reducer as reduxFormReducer } from 'redux-form';

import PLCCForm from '../PLCCForm';

const plccFormLabels = {
  lbl_PLCCForm_firstName: 'First Name',
  lbl_PLCCForm_rewardsCardHeading: 'MY PLACE REWARDS CREDIT CARD',
  lbl_PLCCForm_backButton: 'Back',
  lbl_PLCCForm_contactInfoHeader: 'CONTACT INFORMATION',
  lbl_PLCCForm_middleNameInitial: 'M.I.',
  lbl_PLCCForm_lastName: 'Last Name',
  lbl_PLCCForm_addressLine1: 'Address Line 1',
  lbl_PLCCForm_addressLine2: 'Address Line 2 (Optional)',
  lbl_PLCCForm_city: 'City',
  lbl_PLCCForm_state: 'State',
  lbl_PLCCForm_zipCode: 'Zip Code',
  lbl_PLCCForm_mobilePhoneNumber: 'Mobile Phone Number*',
  lbl_PLCCForm_email: 'Email',
  lbl_PLCCForm_alternatePhone: 'Alternate Phone Number*',
  lbl_PLCCForm_minPhone: 'At least one phone number is required',
  lbl_PLCCForm_personalInfo: 'PERSONAL INFORMATION',
  lbl_PLCCForm_dob: 'Date of Birth',
  plcc_form_electronic_consent: 'Electronic Consent',
  lbl_PLCCForm_financialTermsHeading: 'FINANCIAL TERMS OF YOUR ACCOUNT',
  lbl_PLCCForm_iAgreeCheckboxText:
    "By checking this box and clicking 'Submit to open an account' I agree to the Terms and Conditions, acknowledge receipt of the Privacy Notice, consent to receive documents electronically, and electronically sign this application/solicitation.",
  lbl_PLCCForm_submitButton: 'SUBMIT TO OPEN AN ACCOUNT',
  lbl_PLCCForm_noThanks: 'No thanks',
  plcc_form_list_heading:
    'Please review the important information and terms about opening a ${<b>MY PLACE REWARDS CREDIT CARD</b>} account prior to submitting your application or accepting a pre-approved offer.',
  plcc_form_list_item1: 'Be at the age of majority in your state or territory',
  plcc_form_list_item2: 'Have a valid government-issued photo ID',
  plcc_form_list_item3:
    'Have a valid government issued tax identification number (such as a SSN or SIN)',
  plcc_form_list_item4: 'Have a street, rural route or APO/FPO mailing address (no P.O. Boxes)',
  lbl_PLCCForm_preScreenCodeText: 'If you’ve received a pre-screen code, ',
  lbl_PLCCForm_clickHere: 'click here.',
  lbl_PLCCForm_preScreenCodeOpt: 'Enter Pre-Screen Code (optional)',
  lbl_PLCCForm_enterHere: 'enter it here.',
  lbl_PLCCForm_ssn: 'Last 4 Digits of SSN',
  lbl_PLCCForm_underProgress: 'Your information is under review and is being processed.',
  lbl_PLCCForm_underProcessDetails:
    "Your MY PLACE REWARDS CREDIT CARD application needs further review. You will be notified by mail within 10 business days with your account's status.",
  lbl_PLCCForm_ctcButton: 'CONTINUE TO CHECKOUT',
  lbl_PLCCForm_continueShopping: 'CONTINUE SHOPPING',
  lbl_PLCCForm_statePlaceholder: 'Select',
  lbl_PLCCModal_applyNowHeaderText: 'Save 30% Today!',
  lbl_PLCCModal_applyNowSubText: 'When you Open & Use a My Place Rewards Credit Card',
  lbl_PLCCModal_applyNowCTA: 'APPLY OR ACCEPT OFFER',
  lbl_PLCCModal_learnMoreText: 'Learn More',
  lbl_PLCCModal_benefitsText: 'Bigger, Better Benefits',
  lbl_PLCCForm_details: 'Details',
  lbl_PLCCModal_faqText: 'FAQ',
  lbl_PLCCModal_rewardsProgramText: 'Reward Terms',
  lbl_PLCCModal_linksTextPrefix: '*§**†',
  apply_now_double: 'DOUBLE POINTS',
  apply_now_double_subtext: ' on every credit card purchase*',
  apply_now_discount_30: '30% OFF',
  apply_now_discount_30_subtext: ' your first credit card purchase',
  apply_now_discount_25: '25% OFF',
  apply_now_discount_25_subtext: " for your kids' birthdays!**",
  apply_now_discount_20: '20% OFF',
  apply_now_discount_20_subtext: ' when you get card',
  apply_now_discount_standard: 'FREE STANDARD SHIPPING ',
  apply_now_discount_standard_subtext: ' every day',
  lbl_PLCCModal_learnMoreLink:
    'https://www.childrensplace.com/us/place-card?ecid=mprcc_txt_learn_glft_100916',
  lbl_PLCCModal_detailsLink:
    'https://www.childrensplace.com/us/place-card?ecid=mprcc_txt_learn_glft_100916',
  lbl_PLCCModal_rewardsProgramLink: 'https://www.childrensplace.com/us/help-center/#fullTermsli',
  lbl_PLCCModal_faqLink: 'https://www.childrensplace.com/us/help-center/#creditcard',
  lbl_PLCCModal_applyNowLink: 'Apply Now',
  lbl_PLCCModal_oneEqualsTwoPoints: 'true',
  plccc_approved_saving: 'You could could save $12.74 on your current order!',
  plcc_approved_ps:
    'P.S. You’ll also receive this coupon via email. Once this window is closed, you will not have access to this page. Please copy your coupon code.',
  lbl_PLCCForm_copyToClipboard: 'Copy to Clipboard',
  lbl_PLCCForm_welcomeOffer: 'YOUR WELCOME OFFER',
  lbl_PLCCForm_creditLimit: 'YOUR CREDIT LIMIT: ',
  lbl_PLCCForm_congratulations: 'Congratulations, ',
  lbl_PLCCForm_checkout: 'CHECKOUT',
  lbl_PLCCTimeoutModal_applicationClosure: 'We’ve closed your application.',
  lbl_PLCCTimeoutModal_restartApplication: 'Restart Application',
  lbl_PLCCTimeoutModal_closureSubHeader: 'To protect your privacy, we have closed this session.',
  lbl_PLCCTimeoutModal_stillThere: 'Still there?',
  lbl_PLCCTimeoutModal_interimText:
    'To protect your privacy, we will close this page in XX seconds if you don’t choose to continue.',
  lbl_PLCCTimeoutModal_continueApplication: 'Continue Application',
  lbl_PLCCTimeoutModal_preacceptance: 'We’ve closed your pre-screen acceptance.',
  lbl_PLCCTimeoutModal_restartAcceptance: 'Restart Accpetance',
  lbl_PLCCTimeoutModal_returnCheckout: 'RETURN TO CHECKOUT',
  lbl_PLCCForm_footerlinksPrefix: '*§**†',
  lbl_PLCCForm_manageCreditCardAccount: 'Manage Credit Card Account',
  lbl_PLCCForm_withMyPlaceRewardsCard: 'With the My Place Rewards Card',
  lbl_PLCCModal_applyNowHeaderTextSuffix: '§',
  lbl_PLCCModal_applyAcceptOfferLink:
    'https://www.childrensplace.com/us/place-card?ecid=mprcc_txt_learn_glft_100916',
  lbl_PLCCForm_month: 'Mm',
  lbl_PLCCForm_day: 'Dd',
  lbl_PLCCForm_year: 'Yyyy',
  lbl_PLCC_interested: "Yes, I'm Interested",
  lbl_PLCC_noThanks: 'No Thanks',
  lbl_PLCC_CouponCopy_Success: 'Coupon code has been copied successfully.',
  lbl_PLCC_CouponCopy_Fail: 'An error occured while copying the coupon code.',
  lbl_PLCC_month: 'Month',
  lbl_PLCC_day: 'Day',
  lbl_PLCC_year: 'Year',
  lbl_PLCCForm_detailsLink: '/content/myplace-rewards-page',
};

const rootReducer = combineReducers({
  Labels: () => {
    return { global: {} };
  },
  form: reduxFormReducer,
});

const store = createStore(rootReducer);

const propsMock = {
  dispatch: data => console.log('dispatched', data),
  disclaimersData: {
    credit_card_header: 'header',
    contact_information_disclaimer: 'contact info',
    account_classified_disclaimer: 'account',
    electronic_consent: 'consent',
  },
  labels: plccFormLabels,
};

storiesOf('PLCCForm', module).add('Basic', () => {
  return (
    <Provider store={store}>
      <PLCCForm {...propsMock} />
    </Provider>
  );
});
