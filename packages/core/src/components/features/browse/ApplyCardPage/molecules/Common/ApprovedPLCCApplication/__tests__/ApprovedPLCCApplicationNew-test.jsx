// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import ApprovedPLCCApplicationViewNew from '../ApprovedPLCCApplicationNew';

describe('ApplicationInProgress component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      isPLCCModalFlow: false,
      approvedPLCCData: {
        savingAmount: 900,
        couponCode: 'xyz',
        address: {
          firstName: 'first',
        },
        creditLimit: 566,
      },
      plccData: {
        guest_shipping_info: 'xyz',
        total_savings_amount: 'abc',
      },
    };
    const component = shallow(<ApprovedPLCCApplicationViewNew {...props} />);
    expect(component).toMatchSnapshot();
  });
});
