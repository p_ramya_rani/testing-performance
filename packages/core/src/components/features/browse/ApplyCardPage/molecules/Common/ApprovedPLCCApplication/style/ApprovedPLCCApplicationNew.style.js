// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import { getStaticFilePath } from '@tcp/core/src/utils';

export default styled.div`
  padding-top: 24px;

  @media ${(props) => props.theme.mediaQuery.large} {
    padding-right: ${(props) =>
      props.isPLCCModalFlow ? `0px` : props.theme.spacing.LAYOUT_SPACING.XXL};
  }

  .underprogress_application {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .congratulations_header {
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  }

  .centered {
    justify-content: center;
    text-align: center;
  }

  .credit_limit_heading {
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy7}px;
    }
  }

  .card-InProgress-header {
    text-align: center;
    height: 30px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0px;
  }

  .horizontal_divider {
    color: ${(props) => props.theme.colorPalette.gray['300']};
    margin-top: 15px;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    border: 2px solid ${(props) => props.theme.colorPalette.gray['300']};
  }

  .linkIconSeperator {
    margin-left: 10px;
  }

  .in_progress_status_details {
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 5px 248px 0px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 5px 140px 0px;
    }
  }

  .no_bag_items_continue {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  }

  .submit_buttons_set {
    justify-content: space-around;
  }

  .existing_checkout_button {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    font-weight: bold;
    width: ${(props) => (props.isPLCCModalFlow ? `300px` : ``)};
    text-align: center;
  }

  .existing_continue_button {
    font-weight: bold;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    width: ${(props) => (props.isPLCCModalFlow ? `300px` : ``)};
  }

  .footer_links {
    margin-top: 32px;
  }

  .header-image-new {
    background: transparent url(${getStaticFilePath('images/tcp-cc@2x.png')}) no-repeat 0 0;
    background-size: contain;
    border: none;
    width: 189px;
    height: 118px;
    object-fit: contain;
    display: inline-block;
    margin-right: 17px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 128px;
    }
  }

  .existing_user_info_text2 {
    text-align: center;
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};

    @media ${(props) => props.theme.mediaQuery.medium} and ${(props) =>
        props.theme.mediaQuery.largeMax} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG}
        ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL} 0px;
    }
  }

  .offer-container {
    align-items: center;
    justify-content: center;
  }

  .promo-section-container {
    margin-left: 0;
    margin-right: 0;
    width: 100%;
  }

  .promo_offer_section {
    display: flex;
    flex-direction: column;
    align-items: center;
    flex: 2;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      .credit_limit_heading {
        font-size: 14px;
      }
      .promo-code {
        width: 100px;
        font-size: 12px;
      }
    }
  }

  .promo-container {
    display: flex;
    flex: 1;
    width: auto;
    align-items: center;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      justify-content: center;
      margin-top: 32px;
    }
  }

  .promo_code {
    background-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    margin: 21px 0;
    width: 160px;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 227px;
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy7}px;
    }
  }

  .promo {
    border: none;
    width: 35px;
    height: 35px;
    padding: 30px;
    background-color: ${(props) => props.theme.colorPalette.userTheme.plcc};
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    font-family: ${(props) => props.theme.typography.fonts.PRIMARY};
    font-size: ${(props) => props.theme.typography.fontSizes.fs28};
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    line-height: 0.9;
    text-align: center;
    color: ${(props) => props.theme.colors.WHITE};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs22};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-left: 0;
    }

    .shipping_info_subtext {
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      font-size: ${(props) => props.theme.fonts.fontSize.heading.large.h6}px;
      text-align: center;
      padding: 20px 51px 0px;
      @media ${(props) => props.theme.mediaQuery.medium} {
        padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0px;
      }
    }
  }

  .congratulation-style {
    margin-bottom: 20px;
    text-align: center;
  }
  .crd-cont {
    display: flex;
    width: 86%;
    justify-content: center;
    box-sizing: border-box;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
      padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .email-id {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    font-size: ${(props) => props.theme.typography.fontSizes.fs18};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    }
  }
  .no-need-col {
    background: ${(props) => props.theme.colorPalette.gray['300']};
    margin-right: 5px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: 0px;
      margin-right: 0px;
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    }
  }

  .text-right {
    text-align: right;
  }

  .plcc-card-container {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  }
  .card-container {
    margin-top: 35px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: 0px;
    }
  }
  .no-need-to-wait-card {
    background: ${(props) => props.theme.colorPalette.gray['300']};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    box-sizing: border-box;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    }
  }
  .percent-off {
    display: flex;
    justify-content: space-between;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      justify-content: left;
    }
  }
  .no-need-wait-text {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs18};
    }
  }
  .reset-text {
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    margin-top: 15px;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    }
  }
  .approved-for {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: 0px;
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    }
  }
  .credit-limit {
    text-transform: capitalize;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    }
  }
  .grey-color {
    color: ${(props) => props.theme.colorPalette.gray['800']};
  }
  .shoping-instore {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
  }
  .rounded-cta {
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
  }
  .cta-container {
    justify-content: center;
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    box-sizing: border-box;
  }
  .update-pass {
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    }
  }
  .shiny {
    margin-bottom: 25px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-bottom: 20px;
      padding-right: 10px;
      padding-left: 10px;
    }
  }
  .arrive-icon {
    width: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .continue_btn_plcc {
    text-transform: capitalize;
    color: ${(props) => props.theme.colorPalette.gray['900']};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
  }

  .card-user-name {
    text-transform: capitalize;
  }
`;
