// 9fbef606107a605d69c0edbcd8029e5d
import React, { useRef, useEffect, useState } from 'react';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import PropTypes from 'prop-types';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import ChangePasswordContainer from '@tcp/core/src/components/features/account/ChangePassword/container/ChangePassword.container';
import LoginPageContainer from '@tcp/core/src/components/features/account/LoginPage';
import names from '../../../../../../../constants/eventsName.constants';
import { RichText } from '../../../../../../common/atoms';
import {
  ImageContainer,
  StyledImage,
  ScrollViewContainer,
  StyledBodyCopy,
  CheckoutButtonWrapper,
  CardContainer,
  RichTextContainerTop,
  PromoContainer,
  PromoCircle,
  AccountContainer,
  ShinyContainer,
  GrayBar,
  StyledCTA,
  BottomText,
} from './style/ApprovedPLCCApplicationNew.style.native';
import { getLabelValue, capitalizeWords } from '../../../../../../../utils/utils';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

const headerImage = require('../../../../../../../../../mobileapp/src/assets/images/tcp-plcc.png');
const truckImage = require('../../../../../../../../../mobileapp/src/assets/images/transit.png');

/**
 * @const footerContainer
 *
 * @param - labels
 * @param - plccData - comprehensive plcc data for forming view of approved plcc customer.
 * @param - approvedPLCCData - data of successful WIC
 * @param - bagItems - bag items contained
 * @param - navigation - helper to navigate between screens
 * @param - toggleModal - toggles modal
 *
 * @description - showcases user already holds a plcc card.
 */

const footerBottom = (
  plccData,
  labels,
  approvedPLCCData,
  bagItems,
  navigation,
  togglePLCCModal,
  rest = {}
) => {
  const eventsData = ['scCheckout', 'event86', 'event69', 'event114', 'event115'];
  const productsData = BagPageUtils.formatBagProductsData(rest.cartOrderItems);

  return (
    <React.Fragment>
      {!rest.isRtpsFlow && (
        <CheckoutButtonWrapper>
          <ClickTracker
            as={StyledCTA}
            name={names.screenNames.plccShopping}
            module="global"
            underline
            clickData={{
              customEvents: eventsData,
              products: productsData,
              pageName: rest.isRtpsFlow ? 'checkout:shipping' : 'checkout:pickup',
            }}
            text={capitalizeWords(getLabelValue(labels, 'lbl_PLCCForm_continueShopping'))}
            onPress={() => {
              setTimeout(() => navigation.navigate('Home'), 0);
            }}
          />
        </CheckoutButtonWrapper>
      )}
    </React.Fragment>
  );
};

/**
 * @const ApprovedPLCCApplicationView
 *
 * @param - labels
 * @param - plccData - comprehensive plcc data for forming view of approved plcc customer.
 * @description - showcases user already holds a plcc card.
 */

const ApprovedPLCCApplicationView = ({
  plccData,
  labels,
  approvedPLCCData,
  isGuest,
  navigation,
  togglePLCCModal,
  isRtpsFlow,
  cartOrderItems = 0,
  changePassword,
  newUserTempPass,
  isRegisteredUser,
  isPlccDone,
}) => {
  const isInitalMount = useRef(true);
  const viewRef = useRef(null);
  const [bagItems, setBagItems] = useState(0);
  const setCount = () => {
    const cartValuePromise = readCookie('cartItemsCount');
    cartValuePromise.then((res) => {
      setBagItems(parseInt(res || 0, 10));
    });
  };
  useEffect(() => {
    viewRef.current.scrollTo({ x: 0, y: 0 });
    setCount();
  }, []);

  useEffect(() => {
    if (isInitalMount.current) {
      isInitalMount.current = false;
    }
  }, [isGuest]);

  useEffect(() => {
    if (isPlccDone === true && !isGuest) {
      navigation.goBack();
    }
  }, [isPlccDone, isGuest]);

  return (
    <ScrollViewContainer ref={viewRef} keyboardShouldPersistTaps="handled">
      <StyledBodyCopy
        color="gray.900"
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs20"
        textAlign="center"
        text={`${getLabelValue(labels, 'lbl_PLCCForm_congratulations')}${
          approvedPLCCData && approvedPLCCData.address.firstName
        }!`}
        paddingTop="20px"
        paddingBottom="20px"
      />
      <CardContainer>
        <StyledImage source={headerImage} width="128px" height="80px" />
        <RichTextContainerTop>
          <StyledBodyCopy
            color="gray.900"
            fontFamily="secondary"
            fontWeight="regular"
            fontSize="fs14"
            textAlign="left"
            text={getLabelValue(labels, 'lbl_PLCC_you_approved_for')}
          />
          <RichText source={getLabelValue(labels, 'lbl_PLCC_reward_multiclr')} />
          <StyledBodyCopy
            color="gray.900"
            fontFamily="secondary"
            fontWeight="regular"
            fontSize="fs14"
            textAlign="left"
            text={`${getLabelValue(labels, 'lbl_PLCCForm_creditLimit_lower')} $${
              approvedPLCCData && approvedPLCCData.creditLimit
            }`}
          />
        </RichTextContainerTop>
      </CardContainer>

      <PromoContainer>
        <StyledBodyCopy
          color="gray.900"
          fontFamily="secondary"
          fontWeight="regular"
          fontSize="fs18"
          textAlign="center"
          paddingBottom="12px"
          text={getLabelValue(labels, 'lbl_PLCC_no_need_wait')}
        />
        <CardContainer>
          <PromoCircle>
            <StyledBodyCopy
              color="white"
              fontFamily="primary"
              fontWeight="black"
              fontSize="fs22"
              textAlign="center"
              paddingTop="12px"
              text={getLabelValue(labels, 'lbl_plcc_approved_offer')}
              lineHeight="22px"
            />
          </PromoCircle>
          <RichTextContainerTop>
            <StyledBodyCopy
              color="gray.900"
              fontFamily="secondary"
              fontWeight="regular"
              fontSize="fs16"
              textAlign="left"
              paddingBottom="8px"
              text={getLabelValue(labels, 'lbl_PLCC_reset_pass')}
            />
          </RichTextContainerTop>
        </CardContainer>
      </PromoContainer>
      <StyledBodyCopy
        color="gray.600"
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs13"
        textAlign="left"
        paddingBottom="8px"
        text={getLabelValue(labels, 'lbl_PLCC_your_email_address')}
      />
      <StyledBodyCopy
        color="gray.900"
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs16"
        textAlign="left"
        text={approvedPLCCData && approvedPLCCData.emailAddress}
      />
      <AccountContainer>
        {!isRegisteredUser && (
          <ChangePasswordContainer
            changeTempPassword={changePassword}
            newUserTempPass={newUserTempPass}
            isPLCCFlow
            isRtpsFlow={isRtpsFlow}
            plccCardId={approvedPLCCData && approvedPLCCData.onFileCardId}
          />
        )}
        {isRegisteredUser && (
          <LoginPageContainer
            variation=""
            currentForm=""
            closeModal={() => {}}
            setLoginModalMountState={() => {}}
            showOnPlccModal
            plccEmail={approvedPLCCData && approvedPLCCData.emailAddress}
            plccCardId={approvedPLCCData && approvedPLCCData.onFileCardId}
            isRtpsFlow={isRtpsFlow}
            updateHeader={() => {}}
          />
        )}
      </AccountContainer>

      <GrayBar />

      <ShinyContainer>
        <RichText source={getLabelValue(labels, 'lbl_PLCC_your_shiny')} />
      </ShinyContainer>

      <CardContainer>
        <ImageContainer marginTop="0px">
          <StyledImage source={truckImage} width="25px" height="23px" />
        </ImageContainer>
        <StyledBodyCopy
          color="gray.600"
          fontFamily="secondary"
          fontWeight="regular"
          fontSize="fs16"
          textAlign="center"
          paddingLeft="12px"
          text={getLabelValue(labels, 'lbl_PLCC_arrive_in')}
        />
      </CardContainer>
      <BottomText>
        <StyledBodyCopy
          color="gray.900"
          fontFamily="secondary"
          fontWeight="bold"
          fontSize="fs16"
          textAlign="center"
          paddingBottom="8px"
          paddingTop="12px"
          text={getLabelValue(labels, 'lbl_PLCC_shopping_in_store')}
        />
      </BottomText>
      <StyledBodyCopy
        color="gray.900"
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs14"
        textAlign="center"
        text={getLabelValue(labels, 'lbl_PLCC_associate_look_cc')}
      />
      {footerBottom(plccData, labels, approvedPLCCData, bagItems, navigation, togglePLCCModal, {
        isRtpsFlow,
        cartOrderItems,
      })}
    </ScrollViewContainer>
  );
};

ApprovedPLCCApplicationView.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  isGuest: PropTypes.bool.isRequired,
  plccData: PropTypes.shape({}).isRequired,
  navigation: PropTypes.func.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  cartOrderItems: PropTypes.shape([]),
  isRtpsFlow: PropTypes.bool,
  newUserTempPass: PropTypes.string,
  changePassword: PropTypes.func,
  isRegisteredUser: PropTypes.bool,
};

ApprovedPLCCApplicationView.defaultProps = {
  isRtpsFlow: false,
  cartOrderItems: [],
  newUserTempPass: '',
  changePassword: () => {},
  isRegisteredUser: false,
};

export default ApprovedPLCCApplicationView;
