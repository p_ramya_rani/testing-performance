// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import names from '../../../../../../../constants/eventsName.constants';
import { Button } from '../../../../../../common/atoms';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

import {
  ImageContainer,
  StyledBodyCopy,
  ScrollViewContainer,
  ButtonWrapper,
  StyledAnchor,
  CheckoutButtonWrapper,
  BottomContainer,
  StyledImage,
} from './style/ApplicationInProgress.style.native';
import { getLabelValue } from '../../../../../../../utils/utils';

const headerImage = require('../../../../../../../../../mobileapp/src/assets/images/tcp-cc.png');

const ApplicationInProgress = ({
  labels,
  navigation,
  togglePLCCModal,
  isRtpsFlow,
  cartOrderItems = 0,
}) => {
  const [bagItems, setBagItems] = useState(0);
  const productsData = BagPageUtils.formatBagProductsData(cartOrderItems);
  const setCount = () => {
    const cartValuePromise = readCookie('cartItemsCount');
    cartValuePromise.then(res => {
      setBagItems(parseInt(res || 0, 10));
    });
  };
  useEffect(() => {
    setCount();
  }, []);
  return (
    <ScrollViewContainer keyboardShouldPersistTaps="handled">
      <ImageContainer>
        <StyledImage source={headerImage} width="60%" height="166px" />
      </ImageContainer>
      <StyledBodyCopy
        color="gray.900"
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs22"
        textAlign="center"
        text={getLabelValue(labels, 'lbl_PLCCForm_underProgress')}
        paddingTop="32px"
        paddingLeft="14px"
        paddingRight="14px"
      />
      <StyledBodyCopy
        color="gray.900"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="regular"
        textAlign="center"
        text={getLabelValue(labels, 'lbl_PLCCForm_underProcessDetails')}
        paddingTop="24px"
        paddingLeft="14px"
        paddingRight="14px"
      />
      {bagItems ? (
        <ButtonWrapper>
          <ClickTracker
            as={Button}
            name={names.screenNames.plccCheckout}
            module="global"
            clickData={{
              customEvents: ['event114'],
              products: productsData,
              pageName: isRtpsFlow ? 'checkout:shipping' : 'checkout:pickup',
            }}
            fill="BLUE"
            type="submit"
            fontWeight="regular"
            color="white"
            onPress={() => {
              setTimeout(() => {
                togglePLCCModal({ isPLCCModalOpen: false, status: null });
                navigation.goBack();
                if (!isRtpsFlow) {
                  navigation.navigate('BagPage');
                }
              }, 0);
            }}
            buttonVariation="variable-width"
            text={getLabelValue(labels, 'lbl_PLCCForm_checkout')}
            width="90%"
          />
        </ButtonWrapper>
      ) : null}
      {!isRtpsFlow && (
        <CheckoutButtonWrapper>
          <ClickTracker
            as={Button}
            name={names.screenNames.plccCheckout}
            module="global"
            clickData={{
              customEvents: ['event115'],
              products: productsData,
              pageName: isRtpsFlow ? 'checkout:shipping' : 'checkout:pickup',
            }}
            fill={bagItems ? 'WHITE' : 'BLUE'}
            type="submit"
            color={bagItems ? 'black' : 'white'}
            buttonVariation="variable-width"
            onPress={() => {
              setTimeout(() => navigation.navigate('Home'), 0);
            }}
            text={getLabelValue(labels, 'lbl_PLCCForm_continueShopping')}
            width="90%"
          />
        </CheckoutButtonWrapper>
      )}
      <BottomContainer>
        <StyledBodyCopy
          fontSize="fs10"
          fontFamily="secondary"
          text={getLabelValue(labels, 'lbl_PLCCModal_linksTextPrefix')}
          paddingRight="4px"
        />

        <StyledAnchor
          url={getLabelValue(labels, 'lbl_PLCCModal_detailsLink')}
          fontSizeVariation="medium"
          anchorVariation="primary"
          underline
          text={getLabelValue(labels, 'lbl_PLCCForm_details')}
          paddingRight="28px"
          navigation={navigation}
        />
      </BottomContainer>
    </ScrollViewContainer>
  );
};

ApplicationInProgress.propTypes = {
  labels: PropTypes.shape({
    apply_now_link_modal: PropTypes.string,
  }).isRequired,
  navigation: PropTypes.func.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool,
  cartOrderItems: PropTypes.shape([]),
};

ApplicationInProgress.defaultProps = {
  isRtpsFlow: false,
  cartOrderItems: [],
};

export default ApplicationInProgress;
