// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import { getImageFilePath } from '@tcp/core/src/utils';

export default styled.div`
  padding-top: 24px;

  @media ${(props) => props.theme.mediaQuery.large} {
    padding-right: ${(props) =>
      props.isPLCCModalFlow ? `0px` : props.theme.spacing.LAYOUT_SPACING.XXL};
  }

  .underprogress_application {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .congratulations_header {
    padding-top: 32px;
  }

  .centered {
    justify-content: center;
    text-align: center;
  }

  .credit_limit_heading {
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy7}px;
    }
  }

  .card-InProgress-header {
    text-align: center;
    height: 30px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0px;
  }

  .horizontal_divider {
    color: ${(props) => props.theme.colorPalette.gray['500']};
    margin-top: 32px;
  }

  .linkIconSeperator {
    margin-left: 10px;
  }

  .in_progress_status_details {
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 5px 248px 0px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: 5px 140px 0px;
    }
  }

  .no_bag_items_continue {
    margin-top: 32px;
  }

  .submit_buttons_set {
    justify-content: space-around;
  }

  .existing_checkout_button {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    font-weight: normal;
    width: ${(props) => (props.isPLCCModalFlow ? `300px` : ``)};
  }

  .existing_continue_button {
    font-weight: normal;
    width: ${(props) => (props.isPLCCModalFlow ? `300px` : ``)};
  }

  .footer_links {
    margin-top: 32px;
  }

  .header-image {
    background: transparent url('${(props) => getImageFilePath(props)}/tcp-cc_2x.png') no-repeat 0 0;
    background-size: contain;
    border: none;
    width: 259px;
    height: 166px;
    object-fit: contain;
    margin: auto;
  }

  .existing_user_info_text2 {
    text-align: center;
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};

    @media ${(props) => props.theme.mediaQuery.medium} and ${(props) =>
        props.theme.mediaQuery.largeMax} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG}
        ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL} 0px;
    }
  }

  .offer-container {
    align-items: center;
    justify-content: center;
  }

  .promo-section-container {
    margin-left: 0;
    margin-right: 0;
    width: 100%;
  }

  .promo_offer_section {
    display: flex;
    flex-direction: column;
    align-items: center;
    flex: 2;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      .credit_limit_heading {
        font-size: 14px;
      }
      .promo-code {
        width: 100px;
        font-size: 12px;
      }
    }
  }

  .promo-container {
    display: flex;
    flex: 1;
    width: auto;
    align-items: center;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      justify-content: center;
      margin-top: 32px;
    }
  }

  .promo_code {
    background-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    margin: 21px 0;
    width: 160px;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 227px;
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy7}px;
    }
  }

  .promo {
    border: none;
    width: 90px;
    height: 90px;
    background-color: ${(props) => props.theme.colorPalette.userTheme.plcc};
    border-radius: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    font-family: ${(props) => props.theme.typography.fonts.PRIMARY};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy8}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    line-height: 0.9;
    text-align: center;
    color: ${(props) => props.theme.colors.WHITE};
    margin-left: 10px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: 16px;
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 125px;
      height: 125px;
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy11}px;
      margin-left: 0;
    }
  }

  /*
      Font related details are given since class has to be embedded with content coming from CMS
      **/
  .shipping_info_subtext {
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-size: ${(props) => props.theme.fonts.fontSize.heading.large.h6}px;
    text-align: center;
    padding: 20px 51px 0px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0px;
    }
  }
`;
