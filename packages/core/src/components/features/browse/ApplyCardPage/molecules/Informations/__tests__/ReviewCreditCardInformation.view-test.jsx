// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ReviewCreditCardInformation from '../ReviewCreditCardInformation.view';

describe('ReviewCreditCardInformation component', () => {
  it('should renders correctly', () => {
    const props = {
      creditCardHeader: '<h1>Contact Information</h1>',
    };
    const component = shallow(<ReviewCreditCardInformation {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly when no arguments are passed', () => {
    const component = shallow(<ReviewCreditCardInformation />);
    expect(component).toMatchSnapshot();
  });
});
