// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import names from '../../../../../../../constants/eventsName.constants';
import { Button, RichText } from '../../../../../../common/atoms';
import {
  ImageContainer,
  StyledBodyCopy,
  ScrollViewContainer,
  ButtonWrapper,
  StyledAnchor,
  Container,
  RichTextContainer,
  CheckoutButtonWrapper,
  BottomContainer,
  StyledImage,
} from './style/ExistingPLCCUser.view.style.native';
import { getLabelValue } from '../../../../../../../utils/utils';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

const headerImage = require('../../../../../../../../../mobileapp/src/assets/images/tcp-plcc.png');

const ExistingPLCCUserView = ({
  labels,
  existingCustomerDetails,
  navigation,
  togglePLCCModal,
  isRtpsFlow,
}) => {
  const [bagItems, setBagItems] = useState(0);
  const pageAnalyticsData = isRtpsFlow ? 'checkout' : 'account';
  const clickData = {
    pageType: pageAnalyticsData,
    pageSection: pageAnalyticsData,
    pageSubSection: pageAnalyticsData,
    customEvents: ['scCheckout,event86,event69,PLCC_CONTINUE_TO_CHECKOUT_e114, event114'],
    pageNavigationText: 'header-cart',
  };
  const pageData = {
    pageName: pageAnalyticsData,
  };
  const setCount = () => {
    const cartValuePromise = readCookie('cartItemsCount');
    cartValuePromise.then(res => {
      setBagItems(parseInt(res || 0, 10));
    });
  };
  useEffect(() => {
    setCount();
  }, []);
  return (
    <ScrollViewContainer keyboardShouldPersistTaps="handled">
      <ImageContainer>
        <StyledImage source={headerImage} width="60%" height="166px" />
      </ImageContainer>
      <Container>
        <RichTextContainer>
          <RichText source={{ html: existingCustomerDetails }} />
        </RichTextContainer>
      </Container>
      {bagItems ? (
        <ButtonWrapper>
          <ClickTracker
            as={Button}
            name={names.screenNames.plccCheckout}
            clickData={clickData}
            pageData={pageData}
            fill="BLUE"
            type="submit"
            fontWeight="regular"
            color="white"
            buttonVariation="variable-width"
            onPress={() => {
              togglePLCCModal({ isPLCCModalOpen: false, status: null });
              navigation.goBack();
              if (!isRtpsFlow) {
                navigation.navigate('BagPage');
              }
            }}
            text={getLabelValue(labels, 'lbl_PLCCForm_checkout')}
            width="90%"
          />
        </ButtonWrapper>
      ) : null}
      {!isRtpsFlow && (
        <CheckoutButtonWrapper>
          <ClickTracker
            as={Button}
            name={names.screenNames.plccCheckout}
            clickData={clickData}
            pageData={pageData}
            fill={bagItems ? 'WHITE' : 'BLUE'}
            type="submit"
            color={bagItems ? 'black' : 'white'}
            buttonVariation="variable-width"
            text={getLabelValue(labels, 'lbl_PLCCForm_continueShopping')}
            onPress={() => {
              navigation.navigate('Home');
            }}
            width="90%"
          />
        </CheckoutButtonWrapper>
      )}
      <BottomContainer>
        <StyledBodyCopy
          fontSize="fs10"
          fontFamily="secondary"
          text={getLabelValue(labels, 'lbl_PLCCModal_linksTextPrefix')}
          paddingRight="4px"
        />

        <StyledAnchor
          url={getLabelValue(labels, 'lbl_PLCCModal_detailsLink')}
          fontSizeVariation="medium"
          anchorVariation="primary"
          underline
          text={getLabelValue(labels, 'lbl_PLCCForm_details')}
          paddingRight="28px"
        />
      </BottomContainer>
    </ScrollViewContainer>
  );
};

ExistingPLCCUserView.propTypes = {
  labels: PropTypes.shape({
    apply_now_link_modal: PropTypes.string,
  }).isRequired,
  existingCustomerDetails: PropTypes.string.isRequired,
  navigation: PropTypes.func.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  isRtpsFlow: PropTypes.bool,
};

ExistingPLCCUserView.defaultProps = {
  isRtpsFlow: false,
};

export default ExistingPLCCUserView;
