// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import PLCCForm from '../PLCCForm';

jest.mock('@tcp/core/src/services/abstractors/common/EmailSmsSignup', () => ({
  emailSignupAbstractor: {
    verifyEmail: jest.fn(),
  },
}));

describe('ContactInformationFormWrapper component', () => {
  const props = {
    dispatch: jest.fn(),
    disclaimersData: {
      credit_card_header: 'header',
      contact_information_disclaimer: 'contact info',
      account_classified_disclaimer: 'account',
      electronic_consent: 'consent',
    },
    labels: {
      lbl_PLCCForm_submitButton: 'submit',
      lbl_PLCCForm_noThanks: 'no thanks',
      lbl_PLCCForm_iAgreeCheckboxText: 'checkbox text',
    },
  };

  it('should renders correctly', () => {
    const component = shallow(<PLCCForm {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call onSubmit', () => {
    const newProps = {
      ...props,
      onSubmit: jest.fn(),
      formErrorMessage: {},
    };
    const component = shallow(<PLCCForm {...newProps} />);
    component.simulate('submit');
    expect(newProps.onSubmit).toHaveBeenCalled();
  });
});
