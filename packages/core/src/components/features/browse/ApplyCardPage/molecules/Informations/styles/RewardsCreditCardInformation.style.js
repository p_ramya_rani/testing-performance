// 9fbef606107a605d69c0edbcd8029e5d
/* stylelint-disable */
import styled from 'styled-components';
import { getImageFilePath } from '@tcp/core/src/utils';

export default styled.div`
  display: flex;
  flex-direction: column;
  padding-top: ${(props) =>
    props.isPLCCModalFlow ? props.theme.spacing.LAYOUT_SPACING.MED : `0px`};

  @media ${(props) => props.theme.mediaQuery.medium} {
    flex-direction: row;
  }

  .rewards_card_logo {
    text-align: center;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 0px;
    @media ${(props) => props.theme.mediaQuery.large} {
      text-align: left;
    }
  }

  .header-image {
    background: transparent url('${(props) => getImageFilePath(props)}/tcp-cc_2x.png') no-repeat 0 0;
    background-size: contain;
    border: none;
    object-fit: contain;
    margin: auto;
  }
  .rewards_card_logo .header-image {
    width: 259px;
    height: 166px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin: 0;
      width: 175px;
      height: 112px;
    }
  }

  .rewards_card_instruction {
    text-align: left;
  }
`;
