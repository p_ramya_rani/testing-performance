// 9fbef606107a605d69c0edbcd8029e5d 
/**
 * Form fields name and id to be used in PLCC native form.
 */

export const PLCC_FORM_FIELDS = {
  PRE_SCREEN_CODE: 'preScreenCode',
  FIRST_NAME: 'firstName',
  MIDDLE_NAME: 'middleName',
  LAST_NAME: 'lastName',
  EMAIL_ADDRESS: 'emailAddress',
  ADDRESS_LINE1: 'addressLine1',
  ADDRESS_LINE2: 'addressLine2',
  CITY: 'city',
  STATE_TWO_COUNTRY: 'statewocountry',
  NO_COUNTRY_ZIP: 'noCountryZip',
  PHONE_NUMBER_WITH_ALT: 'phoneNumberWithAlt',
  ALT_PHONE_NUMBER: 'altPhoneNumber',
  DATE: 'date',
  MONTH: 'month',
  YEAR: 'year',
  SS_NUMBER: 'ssNumber',
  IAGREE: 'iAgree',
};

/**
 * Set of fields which will come under section one for scrolling
 */

export const FIELDS_SECTION_ONE = [
  PLCC_FORM_FIELDS.PRE_SCREEN_CODE,
  PLCC_FORM_FIELDS.FIRST_NAME,
  PLCC_FORM_FIELDS.LAST_NAME,
  PLCC_FORM_FIELDS.EMAIL_ADDRESS,
  PLCC_FORM_FIELDS.ADDRESS_LINE1,
  PLCC_FORM_FIELDS.CITY,
  PLCC_FORM_FIELDS.STATE_TWO_COUNTRY,
  PLCC_FORM_FIELDS.NO_COUNTRY_ZIP,
];

/**
 * Set of fields which will come under section tow for scrolling
 */

export const FIELDS_SECTION_TWO = [PLCC_FORM_FIELDS.PHONE_NUMBER_WITH_ALT];

/**
 * Set of fields which will come under section three for scrolling
 */
export const FIELDS_SECTION_THREE = [
  PLCC_FORM_FIELDS.DATE,
  PLCC_FORM_FIELDS.MONTH,
  PLCC_FORM_FIELDS.YEAR,
  PLCC_FORM_FIELDS.SS_NUMBER,
];

/**
 * Set of References to be used for scrolling to particular postion.
 */
export const SECTION_ONE_REF = 'fieldSectionRefOne';
export const SECTION_TWO_REF = 'fieldSectionRefTwo';
export const SECTION_THREE_REF = 'fieldSectionRefThree';

export default {
  PLCC_FORM_FIELDS,
  FIELDS_SECTION_ONE,
  FIELDS_SECTION_TWO,
  FIELDS_SECTION_THREE,
  SECTION_ONE_REF,
  SECTION_TWO_REF,
  SECTION_THREE_REF,
};
