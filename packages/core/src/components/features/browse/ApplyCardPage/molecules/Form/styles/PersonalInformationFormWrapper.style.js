// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

export default styled.div`
 .title {
    margin: 34px 0px 19px 0px;
    line-height: 1;
  }

  .columnWrapper {
    margin-top:  ${props => props.theme.spacing.ELEM_SPACING.XS};
  }

  .field_dob {
    height: auto;
  }

  .free_dropdown_label {
    padding-left: 0px;
  }

  .row-personal-information {
    fieldset.date_of_birth {
      padding: 0;
      margin: 0;
      border: 0;
    }

    @media ${props => props.theme.mediaQuery.medium} {
      .contact_information_form:nth-of-type(2) {
        margin-top: 9px;
      }
    }
  }

  .table_contact_month {
    @media ${props => props.theme.mediaQuery.medium} and ${props =>
  props.theme.mediaQuery.largeMax} {
      width: 29%;
      margin-right: 20px;
    }
  }

  .table_contact_day {
    @media ${props => props.theme.mediaQuery.medium} and ${props =>
  props.theme.mediaQuery.largeMax} {
      width: 29%;
      margin-right: 20px;
    }
  }

  .table_contact_year {
    @media ${props => props.theme.mediaQuery.medium} and ${props =>
  props.theme.mediaQuery.largeMax} {
      width: 29%;
    }
  }

  .contact_information_form {
    text-align: left;
      >label {
        >p {
          margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
        }
      }
    }
  `;
