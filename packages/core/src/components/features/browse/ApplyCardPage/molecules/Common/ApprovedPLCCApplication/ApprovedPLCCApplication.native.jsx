// 9fbef606107a605d69c0edbcd8029e5d
import React, { useRef, useEffect, useState } from 'react';
import { TouchableOpacity, Clipboard, Text } from 'react-native';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import PropTypes from 'prop-types';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import names from '../../../../../../../constants/eventsName.constants';
import { Button, RichText } from '../../../../../../common/atoms';
import {
  ImageContainer,
  StyledImage,
  ScrollViewContainer,
  StyledBodyCopy,
  CouponCodeWrapper,
  RichTextContainer,
  CheckoutButtonWrapper,
  ButtonWrapper,
  BottomContainer,
  StyledAnchor,
  SavingAmountWrapper,
  HorizontalLine,
  HorizontalLine2,
  PSContainer,
  CopyToClipBoardWrapper,
  CouponContainer,
  CouponSection,
  OfferSection,
} from './style/ApprovedPLCCApplication.style.native';
import { getLabelValue } from '../../../../../../../utils/utils';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

const headerImage = require('../../../../../../../../../mobileapp/src/assets/images/tcp-plcc.png');
const couponImage = require('../../../../../../../../../mobileapp/src/assets/images/promo-new.png');

const EMPTY_DIV = '<div></div>';

/**
 * @function - fetchTotalSavingOnOrder
 *
 * @param {*} plccData - richtext data.
 * @param {*} approvedPLCCData - successful WIC data.
 */
const fetchTotalSavingOnOrder = (plccData = {}, approvedPLCCData = {}) => {
  return (
    plccData &&
    plccData.total_savings_amount &&
    plccData.total_savings_amount.replace('amount', `$${approvedPLCCData.savingAmount}`)
  );
};

/**
 * @function - copyToClipboard
 *
 * @param {*} couponCode - text data.
 */
const copyToClipboard = async (couponCode) => {
  await Clipboard.setString(couponCode);
};

/**
 *
 * @const getCouponNote
 * @param - rtps - flag to indicate if we are in RTPS flow or WIC flow
 * @param - guest - flag to indicate if guest user or logged in user
 * @param - plccData - text to form the approval modal
 */

const getCouponNote = (plccData, rtps = false, guest = true) => {
  if (!plccData) {
    return <Text> </Text>;
  }
  const couponNoteText =
    (rtps && plccData.plcc_approved_ps_rtps) || plccData.plcc_approved_ps || EMPTY_DIV;
  const guestCouponNoteText =
    (rtps && plccData.guest_approved_ps_rtps) || plccData.guest_approved_ps || EMPTY_DIV;
  return guest ? (
    <RichText source={{ html: guestCouponNoteText }} />
  ) : (
    <RichText source={{ html: couponNoteText }} />
  );
};

/**
 * @const getCouponBody
 *
 * @param - labels
 * @param - plccData - comprehensive plcc data for forming view of approved plcc customer.
 * @param - approvedPLCCData - data of successful WIC
 *
 * @description - showcases user already holds a plcc card.
 */

const getCouponBody = (plccData, labels, approvedPLCCData, isRtpsFlow, isGuest) => {
  return approvedPLCCData && approvedPLCCData.couponCode ? (
    <React.Fragment>
      <HorizontalLine />
      <CouponContainer>
        <CouponSection>
          <StyledBodyCopy
            color="gray.900"
            fontFamily="secondary"
            fontWeight="extrabold"
            fontSize="fs18"
            textAlign="center"
            text={getLabelValue(labels, 'lbl_PLCCForm_welcomeOffer')}
            paddingBottom="16px"
          />
          <CouponCodeWrapper
            color="black"
            fontFamily="secondary"
            fontWeight="regular"
            fontSize="fs18"
            textAlign="center"
            paddingTop="16px"
            paddingBottom="16px"
            text={approvedPLCCData && approvedPLCCData.couponCode}
          />
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityLabel={getLabelValue(labels, 'lbl_PLCCForm_copyToClipboard')}
            onPress={() => copyToClipboard(approvedPLCCData.couponCode)}
          >
            <CopyToClipBoardWrapper
              color="gray.900"
              fontFamily="secondary"
              fontWeight="regular"
              fontSize="fs16"
              textAlign="center"
              text={getLabelValue(labels, 'lbl_PLCCForm_copyToClipboard')}
              paddingTop="16px"
            />
          </TouchableOpacity>
        </CouponSection>
        <OfferSection>
          <ImageContainer marginTop="0px">
            <StyledImage source={couponImage} width="158px" height="125px" />
          </ImageContainer>
        </OfferSection>
      </CouponContainer>
      <PSContainer>{getCouponNote(plccData, isRtpsFlow, isGuest)}</PSContainer>
      <HorizontalLine2 />
    </React.Fragment>
  ) : null;
};

/**
 * @const footerContainer
 *
 * @param - labels
 * @param - plccData - comprehensive plcc data for forming view of approved plcc customer.
 * @param - approvedPLCCData - data of successful WIC
 * @param - bagItems - bag items contained
 * @param - navigation - helper to navigate between screens
 * @param - toggleModal - toggles modal
 *
 * @description - showcases user already holds a plcc card.
 */

const footerBottom = (
  plccData,
  labels,
  approvedPLCCData,
  bagItems,
  navigation,
  togglePLCCModal,
  rest = {}
) => {
  const totalSavings = fetchTotalSavingOnOrder(plccData, approvedPLCCData);
  const eventsData = ['scCheckout', 'event86', 'event69', 'event114', 'event115'];
  const productsData = BagPageUtils.formatBagProductsData(rest.cartOrderItems);

  return (
    <React.Fragment>
      {approvedPLCCData && approvedPLCCData.savingAmount > 0 ? (
        <SavingAmountWrapper>
          <RichText
            source={{
              html: totalSavings,
            }}
          />
        </SavingAmountWrapper>
      ) : null}

      {bagItems ? (
        <ButtonWrapper>
          <ClickTracker
            as={Button}
            name={names.screenNames.plccCheckout}
            module="global"
            clickData={{
              customEvents: eventsData,
              products: productsData,
              pageName: rest.isRtpsFlow ? 'checkout:shipping' : 'checkout:pickup',
            }}
            fill="BLUE"
            type="submit"
            fontWeight="regular"
            color="white"
            text={getLabelValue(labels, 'lbl_PLCCForm_checkout')}
            onPress={() => {
              setTimeout(() => {
                togglePLCCModal({ isPLCCModalOpen: false, status: null });
                navigation.goBack();
                if (!rest.isRtpsFlow) {
                  navigation.navigate('BagPage');
                }
              }, 0);
            }}
            width="100%"
          />
        </ButtonWrapper>
      ) : null}
      {!rest.isRtpsFlow && (
        <CheckoutButtonWrapper>
          <ClickTracker
            as={Button}
            name={names.screenNames.plccShopping}
            module="global"
            clickData={{
              customEvents: eventsData,
              products: productsData,
              pageName: rest.isRtpsFlow ? 'checkout:shipping' : 'checkout:pickup',
            }}
            fill={bagItems ? 'WHITE' : 'BLUE'}
            type="submit"
            color={bagItems ? 'black' : 'white'}
            text={getLabelValue(labels, 'lbl_PLCCForm_continueShopping')}
            onPress={() => {
              setTimeout(() => navigation.navigate('Home'), 0);
            }}
            width="100%"
          />
        </CheckoutButtonWrapper>
      )}
      <BottomContainer>
        <StyledBodyCopy
          fontSize="fs10"
          fontFamily="secondary"
          text={getLabelValue(labels, 'lbl_PLCCModal_linksTextPrefix')}
          paddingRight="4px"
        />
        <StyledAnchor
          url={getLabelValue(labels, 'lbl_PLCCModal_detailsLink')}
          fontSizeVariation="medium"
          anchorVariation="primary"
          underline
          navigation={navigation}
          text={getLabelValue(labels, 'lbl_PLCCForm_details')}
          paddingRight="28px"
        />
      </BottomContainer>
    </React.Fragment>
  );
};

/**
 *
 * @const getShippingText
 * @param - rtps - flag to indicate if we are in RTPS flow or WIC flow
 * @param - guest - flag to indicate if guest user or logged in user
 * @param - plccData - text to form the approval modal
 */

const getShippingText = (plccData, rtps = false, guest = true) => {
  if (!plccData) {
    return <Text> </Text>;
  }

  const shippingInfo =
    (rtps && plccData.plcc_shipping_info_rtps) || plccData.plcc_shipping_info || EMPTY_DIV;
  const guestShippingInfo =
    (rtps && plccData.guest_shipping_info_rtps) || plccData.guest_shipping_info || EMPTY_DIV;
  return guest ? (
    <Espot richTextHtml={guestShippingInfo} isNativeView={false} />
  ) : (
    <Espot richTextHtml={shippingInfo} isNativeView={false} />
  );
};

/**
 * @const ApprovedPLCCApplicationView
 *
 * @param - labels
 * @param - plccData - comprehensive plcc data for forming view of approved plcc customer.
 * @description - showcases user already holds a plcc card.
 */

const ApprovedPLCCApplicationView = ({
  plccData,
  labels,
  approvedPLCCData,
  isGuest,
  navigation,
  togglePLCCModal,
  isRtpsFlow,
  cartOrderItems = 0,
}) => {
  const isInitalMount = useRef(true);
  const viewRef = useRef(null);
  const [bagItems, setBagItems] = useState(0);
  const setCount = () => {
    const cartValuePromise = readCookie('cartItemsCount');
    cartValuePromise.then((res) => {
      setBagItems(parseInt(res || 0, 10));
    });
  };
  useEffect(() => {
    viewRef.current.scrollTo({ x: 0, y: 0 });
    setCount();
  }, []);

  useEffect(() => {
    if (isInitalMount.current) {
      isInitalMount.current = false;
    } else if (isGuest === false) {
      if (togglePLCCModal) togglePLCCModal({ isPLCCModalOpen: false, status: null });
      if (navigation) navigation.navigate('Home');
    }
  }, [isGuest]);

  return (
    <ScrollViewContainer ref={viewRef} keyboardShouldPersistTaps="handled">
      <ImageContainer>
        <StyledImage source={headerImage} width="70%" height="166px" />
      </ImageContainer>
      <StyledBodyCopy
        color="gray.900"
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs22"
        textAlign="center"
        text={`${getLabelValue(labels, 'lbl_PLCCForm_congratulations')}${
          approvedPLCCData && approvedPLCCData.address.firstName
        }!`}
        paddingTop="32px"
      />
      <RichTextContainer>
        <RichText source={{ html: plccData && plccData.rewards_card_welcome }} />
      </RichTextContainer>
      <StyledBodyCopy
        color="gray.900"
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs22"
        textAlign="center"
        text={`${getLabelValue(labels, 'lbl_PLCCForm_creditLimit')} $${
          approvedPLCCData && approvedPLCCData.creditLimit
        }`}
      />
      <RichTextContainer>{getShippingText(plccData, isRtpsFlow, isGuest)}</RichTextContainer>
      {getCouponBody(plccData, labels, approvedPLCCData, isRtpsFlow, isGuest)}
      {footerBottom(plccData, labels, approvedPLCCData, bagItems, navigation, togglePLCCModal, {
        isRtpsFlow,
        cartOrderItems,
      })}
    </ScrollViewContainer>
  );
};

ApprovedPLCCApplicationView.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  isGuest: PropTypes.bool.isRequired,
  plccData: PropTypes.shape({}).isRequired,
  navigation: PropTypes.func.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  cartOrderItems: PropTypes.shape([]),
  isRtpsFlow: PropTypes.bool,
};

ApprovedPLCCApplicationView.defaultProps = {
  isRtpsFlow: false,
  cartOrderItems: [],
};

export default ApprovedPLCCApplicationView;
