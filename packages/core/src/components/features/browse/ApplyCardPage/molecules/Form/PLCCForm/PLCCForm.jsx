/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Field, reduxForm, change, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';
import {
  getPLCCNoThanksPayload,
  getCustomLoyaltyLocation,
} from '@tcp/core/src/constants/analytics';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import { isGymboree } from '@tcp/core/src/utils/utils';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { BodyCopy, Button, Col, Row } from '../../../../../../common/atoms';
import { Grid } from '../../../../../../common/molecules';
import { getLabelValue } from '../../../../../../../utils';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import InputCheckBox from '../../../../../../common/atoms/InputCheckbox';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import {
  AccountInformations,
  ContactInformation,
  ContactInformationFormWrapper,
  CreditCardPageHeader,
  ElectronicConsent,
  PrescreenCode,
  PLCCAgreements,
  PersonalInformationFormWrapper,
  ReviewCreditCardInformation,
} from '../../index';
import { backToHome } from '../../../utils/DateOfBirthHelper';
import StyledPLCCFormWrapper from '../styles/PLCCForm.style';
import PLCCTimeoutInterimModal from '../../Modals/PLCCTmeoutInterimModal';
import { getPageViewGridRowSize, fetchPLCCFormErrors } from '../../../utils/utility';
import Notification from '../../../../../../common/molecules/Notification';
import { getCartItemCount } from '../../../../../../../utils/cookie.util';
import { INVALID_PRESCREEN_CODE } from '../../../RewardsCard.constants';
import loadScript from './PLCCForm.utils';

export const handleSubmitFail = errors => {
  const formattedErrors = fetchPLCCFormErrors(errors);
  const errorEl = document.querySelector(errors && `[id="${formattedErrors[0]}"]`.trim());
  if (errorEl && errorEl.focus) {
    errorEl.focus();
  }
};

class PLCCForm extends React.PureComponent {
  static idleUserEvents = [
    'blur',
    'change',
    'click',
    'dblclick',
    'focus',
    'focusin',
    'focusout',
    'hover',
    'keydown',
    'keypress',
    'keyup',
    'mousedown',
    'mouseenter',
    'mouseleave',
    'mousemove',
    'mouseout',
    'mouseover',
    'mouseup',
    'resize',
    'scroll',
    'select',
    'submit',
  ];

  constructor(props) {
    super(props);
    this.state = {
      isIdleModalActive: false,
      isTimedOutModalActive: false,
      emailAddressEntered: null,
    };
  }

  componentDidMount() {
    this.bindIdleVerification();
    loadScript();
    const { isPLCCModalFlow } = this.props;
    if (!isPLCCModalFlow) {
      window.scrollTo(0, 0);
    }
  }

  componentDidUpdate() {
    const { dispatch, applicationStatus } = this.props;
    if (applicationStatus === INVALID_PRESCREEN_CODE)
      dispatch(change('PLCCForm', 'preScreenCode', ''));
    this.bindIdleVerification();
  }

  componentWillUnmount() {
    this.unbindIdleVerification();
  }

  /**
   * @fatarrow - bindIdleVerification
   *
   * @description - registers idleuserevents to check user activity on form page.
   */
  bindIdleVerification = () => {
    PLCCForm.idleUserEvents.forEach(event => {
      document.addEventListener(event, this.restartIdleUserTimeout, true);
    });
  };

  /**
   * @fatarrow - unbindIdleVerification
   *
   * @description - releases idleuserevents registered with document.
   */
  unbindIdleVerification = () => {
    PLCCForm.idleUserEvents.forEach(event => {
      document.removeEventListener(event, this.restartIdleUserTimeout, true);
    });
  };

  /**
   * @fatarrow - restartIdleUserTimeout
   *
   * @description - restart timer when user interacts with form.
   */
  restartIdleUserTimeout = () => {
    const timeout = setTimeout(() => {
      this.setState({
        isIdleModalActive: true,
      });
      clearTimeout(timeout);
    }, 13 * 60 * 1000);
  };

  /**
   * @fatarrow - onCloseInterimModal
   *
   * @description - resets idleModalActive state to close close interim moda.
   */
  onCloseInterimModal = () => {
    this.setState({ isIdleModalActive: false });
  };

  /**
   * @fatarrow - onCloseInterimModal
   *
   * @description - resets idleModalActive state to close interim modal.
   */
  handleContinueApplication = () => {
    this.setState({
      isIdleModalActive: false,
    });
    this.restartIdleUserTimeout();
  };

  /**
   * @fatarrow - unregisterIdleVerfication
   *
   * @description - unregisters idleUserEvents from document.
   */
  unregisterIdleVerfication = () => {
    this.unbindIdleVerification();
  };

  /**
   * @fatarrow - handleFormReset
   *
   * @description - resets plcc form data entered by user.
   */
  handleFormReset = () => {
    const { reset } = this.props;
    this.setState({ isTimedOutModalActive: true });
    reset();
  };

  subscribeEmail = (emailAddress, status, isPLCCModalFlow) => {
    const statusMessage = status.status || status.error;
    const payloadObject = {
      emailaddr: emailAddress,
      URL: isPLCCModalFlow ? 'plcc-modal' : 'plcc page',
      response: `${statusMessage}:::false:false`,
      brandTCP: !isGymboree(),
      brandGYM: !!isGymboree(),
      registrationType: '15',
    };
    emailSignupAbstractor.subscribeEmail(payloadObject);
  };

  submitForm = formObject => {
    const { onSubmit, formErrorMessage, isPLCCModalFlow } = this.props;
    const { emailAddressEntered } = this.state;
    const { emailAddress } = formObject;
    const error = { emailAddress: formErrorMessage.lbl_err_email_req };
    if (emailAddressEntered && emailAddress && emailAddress === emailAddressEntered) {
      throw new SubmissionError({ ...error, _error: error });
    }
    return emailSignupAbstractor
      .verifyEmail(emailAddress)
      .then(status => {
        if (status && status.error) {
          this.setState({ emailAddressEntered: emailAddress });
          return Promise.reject();
        }
        setTimeout(this.subscribeEmail(emailAddress, status, isPLCCModalFlow), 0);
        return onSubmit(formObject);
      })
      .catch(() => {
        throw new SubmissionError({ ...error, _error: error });
      });
  };

  render() {
    const {
      dispatch,
      plccData,
      handleSubmit,
      labels,
      isPLCCModalFlow,
      applicationStatus,
      isRtpsFlow,
      closePLCCModal,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      pageCategory,
    } = this.props;
    const { isIdleModalActive, isTimedOutModalActive } = this.state;
    const bagItems = getCartItemCount();

    return (
      <StyledPLCCFormWrapper isPLCCModalFlow={isPLCCModalFlow}>
        <form onSubmit={handleSubmit(this.submitForm)}>
          <input name="BF_ioBlackBox" id="ioBlackBox" type="hidden" />
          <Grid>
            <CreditCardPageHeader labels={labels} isPLCCModalFlow={isPLCCModalFlow} />
            <Row fullBleed>
              <ReviewCreditCardInformation
                isPLCCModalFlow={isPLCCModalFlow}
                creditCardHeader={plccData && plccData.credit_card_header}
              />
            </Row>
            {!isRtpsFlow && (
              <Row fullBleed>
                <Col
                  key="Prescreen_code_link"
                  data-locator="Prescreen_code_link"
                  colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
                >
                  <PrescreenCode labels={labels} />
                </Col>
              </Row>
            )}
            <ContactInformationFormWrapper
              labels={labels}
              dispatch={dispatch}
              isPLCCModalFlow={isPLCCModalFlow}
              mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
              mapboxSwitch={mapboxSwitch}
            />
            <Row fullBleed>
              <Col
                key="container_contact_info"
                colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
              >
                <ContactInformation
                  contactInfo={plccData && plccData.contact_information_disclaimer}
                />
              </Col>
            </Row>
            <PersonalInformationFormWrapper labels={labels} isPLCCModalFlow={isPLCCModalFlow} />
            <Row fullBleed className="classifiedInfo">
              <Col
                key="container_account_info"
                colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
              >
                <AccountInformations
                  classifiedDisclaimer={plccData && plccData.account_classified_disclaimer}
                />
              </Col>
            </Row>
            <Row fullBleed>
              <Col
                key="container_electronic_consent"
                colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
              >
                <ElectronicConsent electronicConsent={plccData && plccData.electronic_consent} />
              </Col>
            </Row>
            <Row fullBleed>
              <Col
                key="container_plcc_agreement"
                colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
              >
                <PLCCAgreements labels={labels} />
              </Col>
            </Row>

            <Row fullBleed>
              <Col
                className="plcc_iAgree_container"
                key="container_checkbox"
                colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
              >
                <Field
                  name="iAgree"
                  component={InputCheckBox}
                  fontSize="fs16"
                  id="iAgree"
                  dataLocator="plcc_T&C_checkbox"
                  className="iAgree_terms_conditions"
                  disabled={false}
                  genericCheckboxStyle
                >
                  <BodyCopy
                    className="iAgreeCheck"
                    fontSize="fs12"
                    fontFamily="secondary"
                    fontWeight="regular"
                  >
                    {getLabelValue(labels, 'lbl_PLCCForm_iAgreeCheckboxText')}
                  </BodyCopy>
                </Field>
              </Col>
            </Row>
            <Row fullBleed>
              <Col
                key="container_plcc_agreement"
                colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
              >
                {applicationStatus && <Notification status="error" message={applicationStatus} />}
              </Col>
            </Row>
            {/* {applicationStatus && <ErrorMessage error={applicationStatus} />} */}
            <BodyCopy
              className="underprogress_application"
              fontSize="fs16"
              fontFamily="secondary"
              fontWeight="regular"
            >
              <Row fullBleed>
                <Col
                  ignoreGutter={{ small: true, medium: true }}
                  colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
                  className="submit_button_plcc_form_container"
                >
                  <ClickTracker
                    as={Button}
                    buttonVariation="fixed-width"
                    fill="BLUE"
                    type="submit"
                    className="submit_button_plcc_form"
                    data-locator="plcc_submit_btn"
                  >
                    {getLabelValue(labels, 'lbl_PLCCForm_submitButton')}
                  </ClickTracker>
                </Col>
              </Row>
              <Row fullBleed>
                <Col
                  ignoreGutter={{ small: true }}
                  colSize={{ large: getPageViewGridRowSize(isPLCCModalFlow), medium: 8, small: 6 }}
                  className="no_thanks_link"
                >
                  <ClickTracker
                    isLoyaltyClick
                    clickData={getPLCCNoThanksPayload(
                      getCustomLoyaltyLocation(pageCategory, isRtpsFlow)
                    )}
                  >
                    <BodyCopy
                      fontFamily="secondary"
                      component="div"
                      onClick={() =>
                        isRtpsFlow || isPLCCModalFlow ? closePLCCModal() : backToHome()
                      }
                      textAlign="center"
                      tabIndex="0"
                    >
                      {getLabelValue(labels, 'lbl_PLCCForm_noThanks')}
                    </BodyCopy>
                  </ClickTracker>
                </Col>
              </Row>
            </BodyCopy>
          </Grid>
        </form>
        {isIdleModalActive ? (
          <PLCCTimeoutInterimModal
            isModalOpen={isIdleModalActive}
            handleContinueApplication={this.handleContinueApplication}
            labels={labels}
            closeModal={this.onCloseInterimModal}
            isPLCCModalFlow={isPLCCModalFlow}
            unregisterIdleVerfication={this.unregisterIdleVerfication}
            bagItems={bagItems}
            time={120}
            isTimedOutModalActive={isTimedOutModalActive}
            handleFormReset={this.handleFormReset}
            isRtpsFlow={isRtpsFlow}
            closePLCCModal={closePLCCModal}
          />
        ) : null}
      </StyledPLCCFormWrapper>
    );
  }
}

PLCCForm.propTypes = {
  plccData: PropTypes.shape({
    credit_card_header: PropTypes.string.isRequired,
    contact_information_disclaimer: PropTypes.string.isRequired,
    account_classified_disclaimer: PropTypes.string.isRequired,
    electronic_consent: PropTypes.string.isRequired,
    plcc_form_checkbox_text: PropTypes.string.isRequired,
    plcc_form_submit_button: PropTypes.string.isRequired,
    plcc_form_nothanks: PropTypes.string.isRequired,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
  isPLCCModalFlow: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  applicationStatus: PropTypes.string.isRequired,
  labels: PropTypes.shape({
    plcc_form_checkbox_text: PropTypes.string.isRequired,
    plcc_form_submit_button: PropTypes.string.isRequired,
    plcc_form_nothanks: PropTypes.string.isRequired,
  }).isRequired,
  isRtpsFlow: PropTypes.bool.isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  formErrorMessage: PropTypes.shape({}),
  onSubmit: PropTypes.func.isRequired,
  mapboxAutocompleteTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  pageCategory: PropTypes.string,
};

PLCCForm.defaultProps = {
  formErrorMessage: {},
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  pageCategory: '',
};

const validateMethod = createValidateMethod(
  getStandardConfig([
    'firstName',
    'lastName',
    'addressLine1',
    'addressLine2',
    'city',
    'statewocountry',
    'date',
    'month',
    'year',
    'ssNumber',
    'preScreenCode',
    'noCountryZip',
    'emailAddress',
    'password',
    'confirmPassword',
    'iAgree',
    'phoneNumberWithAlt',
    'altPhoneNumber',
  ])
);

export default reduxForm({
  form: 'PLCCForm',
  ...validateMethod,
  enableReinitialize: true,
  destroyOnUnmount: true,
  onSubmitFail: errors => handleSubmitFail(errors),
})(PLCCForm);
