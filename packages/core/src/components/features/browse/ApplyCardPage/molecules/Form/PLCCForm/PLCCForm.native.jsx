/* eslint-disable max-lines */
/* eslint-disable sonarjs/cognitive-complexity */
/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes, { func, bool, shape } from 'prop-types';
import { View } from 'react-native';
import { Field, reduxForm, change, SubmissionError } from 'redux-form';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import emailSignupAbstractor from '@tcp/core/src/services/abstractors/common/EmailSmsSignup';
import { getAddressFromPlace } from '@tcp/core/src/utils';
import { isGymboree } from '@tcp/core/src/utils/utils';
import { getScreenHeight } from '@tcp/core/src/utils/index.native';
import { isIOS, getBlackBoxData } from '@tcp/core/src/utils/utils.app';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import names from '../../../../../../../constants/eventsName.constants';
import { RichText, Button } from '../../../../../../common/atoms';
import TextBox from '../../../../../../common/atoms/TextBox';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox/views/InputCheckbox.native';
import { calendarDaysMap, calendarYearsMap } from '../../../utils/DateOfBirthHelper';
import { MONTH_OPTIONS_MAP_WITH_EMPTY as months } from '../../../RewardsCard.constants';
import { GooglePlacesInput } from '../../../../../../common/atoms/AutoSuggest/AutoCompleteComponent.native';
import { GooglePlacesInputGmaps } from '../../../../../../common/atoms/AutoSuggest/AutoCompleteComponentGmaps.native';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import Select from '../../../../../../common/atoms/Select';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import { formatPhoneNumber } from '../../../../../../../utils/formValidation/phoneNumber';
import FinancialPrompt from '../../FinancialTermsModal/view/FinancialModal.native';

import {
  ImageContainer,
  Container,
  TextBoxContainer,
  StyledBodyCopy,
  NameFieldContainer,
  ContainerView,
  StateContainerView,
  ZipContainerView,
  MessageViewContainer,
  PersonalInformationContainerView,
  DateContainerView,
  ScrollViewContainer,
  CheckBoxContainerView,
  CheckBoxImage,
  ButtonWrapper,
  StyledImage,
  RichTextContainer,
  StyledAnchor,
  PreScreenCodeContainer,
  ParentMessageContainer,
  FirstNameContainer,
  MiddleNameContainer,
  SSNContainer,
  AddressLine1Container,
} from './style/PLCCForm.style.native';
import {
  CAcountriesStatesTable,
  UScountriesStatesTable,
} from '../../../../../../common/organisms/AddressForm/CountriesAndStates.constants';
import { getLabelValue, getSiteId } from '../../../../../../../utils';
import { setActiveErrorField } from '../../../container/ApplyCard.actions';
import {
  PLCC_FORM_FIELDS,
  FIELDS_SECTION_ONE,
  FIELDS_SECTION_TWO,
  FIELDS_SECTION_THREE,
  SECTION_ONE_REF,
  SECTION_TWO_REF,
  SECTION_THREE_REF,
} from './PLCCForm.native.config';
import logger from '../../../../../../../utils/loggerInstance';

const headerImage = require('../../../../../../../../src/assets/tcp-plcc.png');

/**
 * This function will be called when there is any error on client side for plcc form.
 * @param {*} errors
 * @param {*} dispatch
 */

const scrollToFirstError = (errors, dispatch) => {
  const errorFieldName = Object.keys(errors)[0];
  if (FIELDS_SECTION_ONE.indexOf(errorFieldName) !== -1) {
    dispatch(setActiveErrorField(SECTION_ONE_REF));
  } else if (FIELDS_SECTION_TWO.indexOf(errorFieldName) !== -1) {
    dispatch(setActiveErrorField(SECTION_TWO_REF));
  } else if (FIELDS_SECTION_THREE.indexOf(errorFieldName) !== -1) {
    dispatch(setActiveErrorField(SECTION_THREE_REF));
  }
};

class PLCCForm extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    labels: PropTypes.shape({}).isRequired,
    countryState: PropTypes.string.isRequired,
    isPreScreen: PropTypes.bool.isRequired,
    updateScrollViewState: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
    initialValues: PropTypes.shape([]).isRequired,
    isRtpsFlow: PropTypes.bool.isRequired,
    isScrollEnabled: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.siteId = getSiteId();

    const selectArray = [
      {
        id: ``,
        fullName: '',
        displayName: 'Select',
      },
    ];

    this.CAcountriesStates = [...selectArray, ...CAcountriesStatesTable];
    this.UScountriesStates = [...selectArray, ...UScountriesStatesTable];

    this.date = calendarDaysMap();
    this.year = calendarYearsMap();

    this.state = {
      // eslint-disable-next-line react/no-unused-state
      country: 'US',
      // eslint-disable-next-line react/no-unused-state
      dropDownItem: props.countryState ? props.countryState : this.UScountriesStates[0].displayName,
      isPreScreen: false,
      isOpen: false,
      blackBox: '',
    };

    this.locationRef = null;
    this.scrolled = false;
  }

  componentDidMount() {
    try {
      getBlackBoxData()
        .then((data) => this.setState({ blackBox: data }))
        .catch((err) => logger.error('BlackBoxData', 'Error While Getting BlackBoxData', err));
    } catch (error) {
      logger.error('Iovation', 'Error While Fetching BlackBoxData from Native Code', error);
    }
  }

  componentDidUpdate(prevProps) {
    const { activeErrorField } = this.props;
    if (
      prevProps !== this.props &&
      activeErrorField &&
      this[activeErrorField] &&
      !this.scrolled &&
      this.scrollView
    ) {
      this.triggerScrollTo();
    }
  }

  /**
   * @toggleModal : To manage the modal state .
   */
  closeModal = () => {
    this.setState({
      isOpen: false,
    });
  };

  openFinancialModal = () => {
    this.setState({ isOpen: true });
  };

  /**
   * This function will be used to trigger the scroll position based on the form errors.
   */
  triggerScrollTo = () => {
    this.scrolled = true;
    const { activeErrorField } = this.props;
    if (this[activeErrorField] && this[activeErrorField].measure) {
      this[activeErrorField].measure((x, y, width, height, pageX, pageY) => {
        if (this.scrollView && this.scrollView.scrollTo) {
          const scrollPos = this.currentScrollValue - Math.abs(pageY) - height;
          this.scrollView.scrollTo({ y: scrollPos, animated: true });
          this.scrolled = false;
        }
      });
    }
  };

  togglePreScreen = () => {
    const { isPreScreen } = this.props;
    this.setState({ isPreScreen: !isPreScreen });
  };

  /**
   * @fatarrow - handlePlaceSelected
   * @params - @param - place - place picked up from google autocomplete.
   *           @param - inputValue - input value.
   *
   * @description - handles the place selected from address1 field of PLCC appliation form.
   *
   */
  handlePlaceSelectedGmaps = (place, inputValue) => {
    const { dispatch } = this.props;
    const address = getAddressFromPlace(place, inputValue);
    dispatch(change('PLCCForm', 'city', address.city));
    dispatch(change('PLCCForm', 'noCountryZip', address.zip));
    dispatch(change('PLCCForm', 'statewocountry', address.state));
    dispatch(change('PLCCForm', 'addressLine1', address.street));

    this.locationRef.setAddressText(address.street);
  };

  handlePlaceSelected = (address) => {
    const { dispatch } = this.props;
    dispatch(change('PLCCForm', 'city', address.city));
    dispatch(change('PLCCForm', 'noCountryZip', address.zip));
    dispatch(change('PLCCForm', 'statewocountry', address.state));
    dispatch(change('PLCCForm', 'addressLine1', address.addressline));
  };

  getInitialAddressLine1 = (initialValues) => {
    return (initialValues && initialValues.addressLine1) || '';
  };

  richtextAndroid = () => {
    return (
      <View
        onResponderGrant={() => this.startTouchOnRichText()}
        onResponderRelease={() => this.stopTouchOnRichText()}
        onStartShouldSetResponder={() => true}
      >
        {this.renderRichText()}
      </View>
    );
  };

  renderRichText = () => {
    return (
      <RichText
        source={{
          html: '<iframe style="width:100%; border:0; min-height: 450px;" title="FINANCIAL TERMS OF YOUR ACCOUNT" class="financial-terms-disclosures" src="https://comenity.net/childrensplace/common/Legal/disclosures.xhtml" scrolling="yes"></iframe>',
        }}
      />
    );
  };

  /**
   * To capture the scroll postion of scroll view.
   */

  handleScroll = (event) => {
    this.currentScrollValue = event.nativeEvent.contentOffset.y;
  };

  startTouchOnRichText = () => {
    const { updateScrollViewState } = this.props;
    if (updateScrollViewState) updateScrollViewState(false);
  };

  stopTouchOnRichText = () => {
    const { updateScrollViewState } = this.props;
    if (updateScrollViewState) updateScrollViewState(true);
  };

  subscribeEmail = (emailAddress, status, isPLCCModalFlow) => {
    const statusMessage = status.status || status.error;
    const payloadObject = {
      emailaddr: emailAddress,
      URL: isPLCCModalFlow ? 'plcc-modal' : 'plcc page',
      response: `${statusMessage}:::false:false`,
      brandTCP: !isGymboree(),
      brandGYM: !!isGymboree(),
      registrationType: '15',
    };
    emailSignupAbstractor.subscribeEmail(payloadObject);
  };

  submitForm = (formObject) => {
    const { onSubmit, formErrorMessage, isPLCCModalFlow } = this.props;
    const { blackBox } = this.state;
    const { emailAddress } = formObject;

    const updateFormObject = {
      ...formObject,
      BF_ioBlackBox: blackBox,
    };

    return emailSignupAbstractor
      .verifyEmail(emailAddress)
      .then((status) => {
        if (status && status.error) {
          return Promise.reject();
        }
        this.subscribeEmail(emailAddress, status, isPLCCModalFlow);
        return onSubmit(updateFormObject);
      })
      .catch(() => {
        const error = { emailAddress: formErrorMessage.lbl_err_email_req };
        throw new SubmissionError({ ...error, _error: error });
      });
  };

  /**
   * @function render  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */
  // eslint-disable-next-line complexity
  render() {
    const {
      toggleModal,
      plccData,
      labels,
      handleSubmit,
      isRtpsFlow,
      cartOrderItems,
      navigation,
      isScrollEnabled,
      initialValues,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      loyaltyPageName,
    } = this.props;

    const { isPreScreen, isOpen } = this.state;
    const productsData = BagPageUtils.formatBagProductsData(cartOrderItems);

    return (
      <View>
        {isOpen && <FinancialPrompt closeModal={this.closeModal} />}

        <ScrollViewContainer
          ref={(node) => {
            this.scrollView = node;
          }}
          height={isIOS() ? getScreenHeight() : null}
          onScroll={this.handleScroll}
          keyboardShouldPersistTaps="handled"
          scrollEnabled={isScrollEnabled}
        >
          <ImageContainer>
            <StyledImage source={headerImage} width="70%" height="166px" />
          </ImageContainer>
          <Container>
            <RichTextContainer>
              <RichText
                source={{
                  html: plccData && plccData.credit_card_header ? plccData.credit_card_header : '',
                }}
              />
            </RichTextContainer>
          </Container>
          {isPreScreen && (
            <TextBoxContainer>
              <Field
                name="preScreenCode"
                id="preScreenCode"
                label={getLabelValue(labels, 'lbl_PLCCForm_preScreenCodeOpt')}
                type="tel"
                component={TextBox}
                maxLength={12}
                keyboardType="numeric"
              />
            </TextBoxContainer>
          )}
          {!isRtpsFlow && (
            <PreScreenCodeContainer>
              <StyledBodyCopy
                text={getLabelValue(labels, 'lbl_PLCCForm_preScreenCodeText')}
                fontSize="fs15"
                color="gray.900"
                paddingLeft="16px"
                fontFamily="secondary"
                textAlign="center"
                paddingTop={!isPreScreen ? '22px' : '1px'}
              />

              {!isPreScreen ? (
                <StyledAnchor
                  onPress={() => this.togglePreScreen()}
                  fontSizeVariation="large"
                  underline
                  text={getLabelValue(labels, 'lbl_PLCCForm_clickHere')}
                  paddingRight="16px"
                  paddingTop={!isPreScreen ? '22px' : '1px'}
                />
              ) : (
                <StyledBodyCopy
                  text={getLabelValue(labels, 'lbl_PLCCForm_enterHere')}
                  fontSize="fs15"
                  color="gray.900"
                  textAlign="center"
                  paddingRight="16px"
                  paddingTop={!isPreScreen ? '12px' : '1px'}
                />
              )}
            </PreScreenCodeContainer>
          )}
          <StyledBodyCopy
            text={getLabelValue(labels, 'lbl_PLCCForm_contactInfoHeader')}
            fontSize="fs16"
            color="black"
            fontFamily="secondary"
            textAlign="left"
            fontWeight="semibold"
            paddingLeft="16px"
            paddingRight="16px"
            paddingTop="40px"
            paddingBottom="12px"
          />
          <NameFieldContainer
            ref={(node) => {
              this[SECTION_ONE_REF] = node;
            }}
          >
            <ParentMessageContainer>
              <FirstNameContainer>
                <Field
                  name={PLCC_FORM_FIELDS.FIRST_NAME}
                  id={PLCC_FORM_FIELDS.FIRST_NAME}
                  label={getLabelValue(labels, 'lbl_PLCCForm_firstName')}
                  type="text"
                  component={TextBox}
                  maxLength={20}
                />
              </FirstNameContainer>

              <MiddleNameContainer>
                <Field
                  name={PLCC_FORM_FIELDS.MIDDLE_NAME}
                  id={PLCC_FORM_FIELDS.MIDDLE_NAME}
                  label={getLabelValue(labels, 'lbl_PLCCForm_middleNameInitial')}
                  type="text"
                  component={TextBox}
                  maxLength={5}
                />
              </MiddleNameContainer>
            </ParentMessageContainer>
          </NameFieldContainer>
          <NameFieldContainer>
            <Field
              label={getLabelValue(labels, 'lbl_PLCCForm_lastName')}
              type="text"
              component={TextBox}
              maxLength={15}
              name={PLCC_FORM_FIELDS.LAST_NAME}
              id={PLCC_FORM_FIELDS.LAST_NAME}
            />
          </NameFieldContainer>
          <NameFieldContainer>
            <Field
              name={PLCC_FORM_FIELDS.EMAIL_ADDRESS}
              id={PLCC_FORM_FIELDS.EMAIL_ADDRESS}
              label={getLabelValue(labels, 'lbl_PLCCForm_email')}
              type="text"
              component={TextBox}
            />
          </NameFieldContainer>
          <NameFieldContainer>
            <Field
              name={PLCC_FORM_FIELDS.ADDRESS_LINE1}
              id={PLCC_FORM_FIELDS.ADDRESS_LINE1}
              headerTitle={getLabelValue(labels, 'lbl_PLCCForm_addressLine1')}
              component={mapboxSwitch ? GooglePlacesInput : GooglePlacesInputGmaps}
              componentRestrictions={Object.assign({}, { country: [this.siteId] })}
              onValueChange={(data, inputValue) => {
                if (mapboxSwitch) {
                  this.handlePlaceSelected(data);
                } else {
                  this.handlePlaceSelectedGmaps(data, inputValue);
                }
              }}
              maxLength={30}
              initialValue={this.getInitialAddressLine1(initialValues)}
              refs={(instance) => {
                this.locationRef = instance;
              }}
              isPLCC
              mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
            />
            <AddressLine1Container>
              <Field label="" component={TextBox} title="" type="hidden" id="addressLine1" />
            </AddressLine1Container>
          </NameFieldContainer>
          <NameFieldContainer>
            <Field
              name={PLCC_FORM_FIELDS.ADDRESS_LINE2}
              id={PLCC_FORM_FIELDS.ADDRESS_LINE2}
              label={getLabelValue(labels, 'lbl_PLCCForm_addressLine2')}
              type="text"
              component={TextBox}
              maxLength={30}
            />
          </NameFieldContainer>
          <NameFieldContainer>
            <Field
              name={PLCC_FORM_FIELDS.CITY}
              id={PLCC_FORM_FIELDS.CITY}
              label={getLabelValue(labels, 'lbl_PLCCForm_city')}
              type="text"
              component={TextBox}
              maxLength={20}
            />
          </NameFieldContainer>
          <ContainerView>
            <StateContainerView>
              <Field
                name={PLCC_FORM_FIELDS.STATE_TWO_COUNTRY}
                id={PLCC_FORM_FIELDS.STATE_TWO_COUNTRY}
                component={Select}
                heading="State"
                options={UScountriesStatesTable}
                placeholder="State"
                color="gray.900"
              />
            </StateContainerView>

            <ZipContainerView>
              <Field
                name={PLCC_FORM_FIELDS.NO_COUNTRY_ZIP}
                id={PLCC_FORM_FIELDS.NO_COUNTRY_ZIP}
                label={getLabelValue(labels, 'lbl_PLCCForm_zipCode')}
                type="text"
                component={TextBox}
                maxLength={5}
                keyboardType="numeric"
              />
            </ZipContainerView>
          </ContainerView>
          <NameFieldContainer
            ref={(node) => {
              this[SECTION_TWO_REF] = node;
            }}
          >
            <Field
              name={PLCC_FORM_FIELDS.PHONE_NUMBER_WITH_ALT}
              id={PLCC_FORM_FIELDS.PHONE_NUMBER_WITH_ALT}
              label={getLabelValue(labels, 'lbl_PLCCForm_mobilePhoneNumber')}
              component={TextBox}
              maxLength={14}
              keyboardType="phone-pad"
              normalize={formatPhoneNumber}
            />
          </NameFieldContainer>
          <NameFieldContainer>
            <Field
              name={PLCC_FORM_FIELDS.ALT_PHONE_NUMBER}
              id={PLCC_FORM_FIELDS.ALT_PHONE_NUMBER}
              label={getLabelValue(labels, 'lbl_PLCCForm_alternatePhone')}
              component={TextBox}
              maxLength={14}
              keyboardType="phone-pad"
              normalize={formatPhoneNumber}
            />
          </NameFieldContainer>
          <StyledBodyCopy
            text={getLabelValue(labels, 'lbl_PLCCForm_minPhone')}
            fontSize="fs10"
            color="gray.900"
            paddingLeft="16px"
            paddingRight="16px"
            fontFamily="secondary"
            textAlign="left"
          />
          <MessageViewContainer>
            <RichText
              source={{
                html:
                  plccData && plccData.contact_information_disclaimer
                    ? plccData.contact_information_disclaimer
                    : '',
              }}
            />
          </MessageViewContainer>
          <StyledBodyCopy
            text="PERSONAL INFORMATION"
            fontSize="fs16"
            color="black"
            paddingLeft="16px"
            paddingRight="16px"
            paddingTop="34px"
            fontFamily="secondary"
            textAlign="left"
          />
          <StyledBodyCopy
            text={getLabelValue(labels, 'lbl_PLCCForm_dob')}
            mobilefontSize="fs10"
            color="gray.900"
            paddingLeft="16px"
            paddingTop="10px"
            fontFamily="secondary"
            textAlign="left"
            fontWeight="extrabold"
          />
          <PersonalInformationContainerView
            ref={(node) => {
              this[SECTION_THREE_REF] = node;
            }}
          >
            <DateContainerView>
              <Field
                name={PLCC_FORM_FIELDS.MONTH}
                id={PLCC_FORM_FIELDS.MONTH}
                component={Select}
                heading="Mm"
                options={months}
                placeholder="Mm"
                color="gray.900"
              />
            </DateContainerView>

            <DateContainerView>
              <Field
                name={PLCC_FORM_FIELDS.DATE}
                id={PLCC_FORM_FIELDS.DATE}
                component={Select}
                heading="Dd"
                options={this.date}
                placeholder="Dd"
                color="gray.900"
              />
            </DateContainerView>

            <DateContainerView>
              <Field
                name={PLCC_FORM_FIELDS.YEAR}
                id={PLCC_FORM_FIELDS.YEAR}
                component={Select}
                heading="Yyyy"
                options={this.year}
                placeholder="Yyyy"
                color="gray.900"
              />
            </DateContainerView>
          </PersonalInformationContainerView>
          <SSNContainer>
            <Field
              name={PLCC_FORM_FIELDS.SS_NUMBER}
              id={PLCC_FORM_FIELDS.SS_NUMBER}
              label={getLabelValue(labels, 'lbl_PLCCForm_ssn')}
              component={TextBox}
              maxLength={4}
              keyboardType="number-pad"
            />
          </SSNContainer>
          <View
            onResponderGrant={() => this.stopTouchOnRichText()}
            onStartShouldSetResponder={() => true}
          >
            <MessageViewContainer>
              <RichText
                source={{
                  html:
                    plccData && plccData.account_classified_disclaimer
                      ? plccData.account_classified_disclaimer
                      : '',
                }}
              />
            </MessageViewContainer>
            <MessageViewContainer>
              <Espot
                richTextHtml={
                  plccData && plccData.electronic_consent ? plccData.electronic_consent : ''
                }
                navigation={navigation}
                isNativeView={false}
              />
            </MessageViewContainer>
            <StyledBodyCopy
              text={getLabelValue(labels, 'lbl_PLCCForm_financialTermsHeading')}
              fontSize="fs16"
              color="black"
              fontWeight="semibold"
              paddingLeft="16px"
              paddingRight="16px"
              paddingTop="24px"
              paddingBottom="10px"
              fontFamily="secondary"
              textAlign="left"
            />
          </View>
          <MessageViewContainer withBorder>
            {isIOS() ? this.renderRichText() : this.richtextAndroid()}
          </MessageViewContainer>
          <View
            onResponderGrant={() => this.stopTouchOnRichText()}
            onStartShouldSetResponder={() => true}
          >
            <StyledAnchor
              fontSizeVariation="large"
              underline
              text={getLabelValue(labels, 'lbl_PLCCForm_financialTerms')}
              paddingTop="10px"
              paddingBottom="10px"
              onPress={this.openFinancialModal}
              url=""
            />
            <CheckBoxContainerView>
              <CheckBoxImage>
                <Field
                  name={PLCC_FORM_FIELDS.IAGREE}
                  id={PLCC_FORM_FIELDS.IAGREE}
                  component={InputCheckbox}
                  enableSuccessCheck={false}
                  rightText={getLabelValue(labels, 'lbl_PLCCForm_iAgreeCheckboxText')}
                  checkboxAlignTop
                  errorStyle="margin-left: 32px"
                />
              </CheckBoxImage>
            </CheckBoxContainerView>
            <ButtonWrapper>
              <ClickTracker
                as={Button}
                name={names.screenNames.plcc_success}
                clickData={{
                  customEvents: ['PLCC_Application_Submitted_e49', 'event49'],
                  products: productsData,
                }}
                pageData={{
                  pageName: loyaltyPageName,
                }}
                fill="BLUE"
                type="submit"
                onPress={handleSubmit(this.submitForm)}
                color="white"
                text={getLabelValue(labels, 'lbl_PLCCForm_submitButton')}
                width="90%"
              />

              <ClickTracker
                as={StyledAnchor}
                name={names.screenNames.loyalty_plcc_no_thanks_click}
                clickData={{
                  customEvents: ['event133'],
                  products: productsData,
                  loyaltyLocation: loyaltyPageName,
                }}
                fontSizeVariation="large"
                underline
                text={getLabelValue(labels, 'lbl_PLCCForm_noThanks')}
                paddingTop="40px"
                paddingBottom="60px"
                onPress={() => toggleModal()}
                url=""
              />
            </ButtonWrapper>
          </View>
        </ScrollViewContainer>
      </View>
    );
  }
}

const validateMethod = createValidateMethod(
  getStandardConfig([
    PLCC_FORM_FIELDS.PRE_SCREEN_CODE,
    PLCC_FORM_FIELDS.FIRST_NAME,
    PLCC_FORM_FIELDS.LAST_NAME,
    PLCC_FORM_FIELDS.EMAIL_ADDRESS,
    PLCC_FORM_FIELDS.ADDRESS_LINE1,
    PLCC_FORM_FIELDS.CITY,
    PLCC_FORM_FIELDS.STATE_TWO_COUNTRY,
    PLCC_FORM_FIELDS.NO_COUNTRY_ZIP,
    PLCC_FORM_FIELDS.PHONE_NUMBER_WITH_ALT,
    PLCC_FORM_FIELDS.ALT_PHONE_NUMBER,
    PLCC_FORM_FIELDS.DATE,
    PLCC_FORM_FIELDS.MONTH,
    PLCC_FORM_FIELDS.YEAR,
    PLCC_FORM_FIELDS.SS_NUMBER,
    PLCC_FORM_FIELDS.IAGREE,
  ])
);

PLCCForm.propTypes = {
  plccData: PropTypes.shape({}).isRequired,
  activeErrorField: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  profileInfo: PropTypes.shape({}).isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape([]),
  navigation: PropTypes.shape({}),
  formErrorMessage: shape({}),
  onSubmit: func.isRequired,
  isPLCCModalFlow: bool,
  mapboxAutocompleteTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  loyaltyPageName: PropTypes.string,
};

PLCCForm.defaultProps = {
  cartOrderItems: [],
  navigation: {},
  isPLCCModalFlow: false,
  formErrorMessage: {},
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  loyaltyPageName: 'account',
};

export default reduxForm({
  form: 'PLCCForm', // a unique identifier for this form
  enableReinitialize: true,
  ...validateMethod,
  onSubmitFail: scrollToFirstError,
})(PLCCForm);

export { PLCCForm as PLCCFormVanilla };
