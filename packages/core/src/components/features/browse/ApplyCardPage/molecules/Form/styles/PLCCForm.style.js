// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

export default styled.div`
 .title {
    margin: 34px 0px 19px 0px;
    line-height: 1;
  }

  .select__label {
    margin-top: 10px;
  }

  .Checkbox__error {
    padding-left: 37px;
  }

  .stateField {
    margin-top: -2px;
  }

  .select__input {
    padding-bottom: 6px;
  }

  #checkbox__error__iAgree {
    padding-right: 32px;
    margin-top: 1px;
    @media ${props => props.theme.mediaQuery.medium} {
      padding-right: 0px;
      margin-top: 4px;
    }
    @media ${props => props.theme.mediaQuery.smallMax} {
      margin-top: 0px;
    }
  }

  .columnWrapper {
    margin-top:  ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }

  .field_dob {
    height: auto;
    margin-top: -7px;
  }

  .SSNumber {
    margin-top: -6px;
  }

  .classifiedInfo {
    margin-top: 25px;
    font-size: ${props => props.theme.fonts.fontSize.body.small.secondary}px;
  }

  .table_contact_month {
    @media ${props => props.theme.mediaQuery.medium} and ${props =>
  props.theme.mediaQuery.largeMax} {
      width: 29%;
      margin-right: 20px;
    }
  }

  .iAgreeCheck {
    line-height: 2;
  }

  .table_contact_day {
    @media ${props => props.theme.mediaQuery.medium} and ${props =>
  props.theme.mediaQuery.largeMax} {
      width: 29%;
      margin-right: 20px;
    }
  }

  .table_contact_year {
    @media ${props => props.theme.mediaQuery.medium} and ${props =>
  props.theme.mediaQuery.largeMax} {
      width: 29%;
    }
  }

  .plcc_iAgree_container {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXL};
    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }

  .TextBox__error div:nth-child(2) {
    margin-top: -1px;
    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: 0px;
    }
  }

  .SelectBox__error div:nth-child(2) {
    margin-top: -1px;
    @media ${props => props.theme.mediaQuery.medium} {
      margin-top: 0px;
    }
  }

  .contact_information_form {
    text-align: left;
      >label {
        >input {
          padding-bottom: 6px;
        }
        >p {
          margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
        }
      }
    }
  `;
