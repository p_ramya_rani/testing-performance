// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { BodyCopy, Anchor, Image } from '../../../../../../../common/atoms';

export const CardContainer = styled.View`
  flex-direction: row;
  justify-content: flex-start;
`;

export const PromoContainer = styled.View`
  background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  margin: 20px 0;
  padding: 20px;
  position: relative;
  left: -20px;
  width: 112%;
  right: -20px;
`;

export const ShinyContainer = styled.View`
  text-align: center;
  padding: 20px 0 12px;
  margin: 0 -${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
`;

export const GrayBar = styled.View`
  height: 5px;
  width: 100%;
  background: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
`;

export const AccountContainer = styled.View`
  padding: 20px 0;
`;

export const PromoCircle = styled.View`
  background-color: ${(props) => props.theme.colorPalette.userTheme.plcc};
  border-radius: 50px;
  width: 68px;
  height: 68px;
  line-height: 68px;
  padding: 3px;
`;

export const ImageContainer = styled.View`
  margin-top: ${(props) => props.marginTop || props.theme.spacing.ELEM_SPACING.XS};
  align-items: center;
`;

export const TextBoxContainer = styled.View`
  width: 100%;
  padding: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS}
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS} 0
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
`;

export const StyledBodyCopy = styled(BodyCopy)`
  padding: ${(props) => props.paddingTop || '0px'} ${(props) => props.paddingRight || '0px'}
    ${(props) => props.paddingBottom || '0px'} ${(props) => props.paddingLeft || '0px'};
  line-height: 22px;
  ${(props) => (props.lineHeight ? `line-height: ${props.lineHeight};` : '')}
`;

export const CouponCodeWrapper = styled(StyledBodyCopy)`
  background-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
  width: 100%;
  text-align: center;
  margin-left: ${(props) => props.marginLeft || '0px'};
`;

export const ScrollViewContainer = styled.ScrollView`
  width: 100%;
  padding: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS}
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XS}
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL}
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
`;

export const ButtonWrapper = styled.View`
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
`;

export const CopyToClipBoardWrapper = styled(StyledBodyCopy)`
  text-decoration: underline;
`;

export const StyledAnchor = styled(Anchor)`
  padding: ${(props) => props.paddingTop || '0px'} ${(props) => props.paddingRight || '0px'}
    ${(props) => props.paddingBottom || '0px'} ${(props) => props.paddingLeft || '0px'};
  align-items: center;
  justify-content: center;
`;

export const BottomContainer = styled.View`
  width: 100%;
  padding: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM}
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS}
    ${(props) => props.theme.spacing.LAYOUT_SPACING.SM}
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
  align-items: center;
  flex-direction: row;
  justify-content: center;
`;

export const PSContainer = styled.View`
  height: 125px;
`;

export const Container = styled.View`
  width: 85%;
  margin: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS}
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XS} 0
    ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
`;

export const CheckoutButtonWrapper = styled.View`
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
`;

export const RichTextContainerTop = styled.View`
  width: 62%;
  padding-left: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
`;

export const RichTextContainer = styled.View`
  width: 100%;
`;

export const StyledImage = styled(Image)`
  /* stylelint-disable-next-line */
  resize-mode: stretch;
`;

export const SavingAmountWrapper = styled.View`
  height: 70px;
`;

export const HorizontalLine = styled.View`
  border-bottom-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
  border-bottom-width: 1px;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
`;

export const HorizontalLine2 = styled.View`
  border-bottom-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
  border-bottom-width: 1px;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
`;

export const CouponContainer = styled.View`
  flex-direction: row;
  flex: 1;
`;
export const CouponSection = styled.View`
  width: 60%;
`;
export const OfferSection = styled.View`
  width: 40%;
`;

export const StyledCTA = styled(Anchor)`
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
  font-size: 16px;
  color: ${(props) => props.theme.colors.TEXT.DARKGRAY};
`;

export const BottomText = styled.View`
  margin: 0 -${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
`;

export default {
  CardContainer,
  ImageContainer,
  StyledBodyCopy,
  ScrollViewContainer,
  CheckoutButtonWrapper,
  ButtonWrapper,
  StyledAnchor,
  Container,
  RichTextContainer,
  BottomContainer,
  StyledImage,
  CouponCodeWrapper,
  SavingAmountWrapper,
  HorizontalLine,
  PSContainer,
  CopyToClipBoardWrapper,
  CouponContainer,
  CouponSection,
  OfferSection,
  RichTextContainerTop,
  PromoContainer,
  PromoCircle,
  AccountContainer,
  ShinyContainer,
  GrayBar,
  StyledCTA,
  BottomText,
};
