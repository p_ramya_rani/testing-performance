// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { Image, BodyCopy, Button, Anchor } from '../../../../../../common/atoms';

export const Wrapper = styled.View`
  align-items: center;
  background-color: ${props => props.theme.colorPalette.white};
  height: 800px;
  width: ${props => props.width};
  padding: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const Container = styled.View`
  align-items: center;
  justify-content: center;
`;

export const StyledImage = styled(Image)`
  /* stylelint-disable-next-line */
  tint-color: ${props => props.theme.colorPalette.black};
  ${props => (props.marginTop ? `margin-top: ${props.marginTop};` : ``)}
`;

export const StyledBodyCopy = styled(BodyCopy)`
  ${props => (props.marginTop ? `margin-top: ${props.marginTop};` : ``)}
`;

export const Touchable = styled.TouchableOpacity`
  right: 0;
  padding-top: 35px;
  padding-right: 20px;
  position: absolute;
  background: white;
  z-index: 99;
`;

export const StyledButton = styled(Button)`
  ${props => (props.marginTop ? `margin-top: ${props.marginTop};` : ``)}
`;

export const StyledAnchor = styled(Anchor)`
  ${props => (props.marginTop ? `margin-top: ${props.marginTop};` : ``)}
  ${props => (props.marginBottom ? `margin-bottom: ${props.marginBottom};` : ``)}
`;

export const MessageContainer = styled.View`
  align-items: center;
  justify-content: center;
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const ShadowContainer = styled.View`
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.6);
  height: ${props => props.height}px;
`;

export const TextContainer = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

export default {
  Wrapper,
  Container,
  StyledImage,
  Touchable,
  StyledBodyCopy,
  StyledButton,
  StyledAnchor,
  MessageContainer,
  ShadowContainer,
  TextContainer,
};
