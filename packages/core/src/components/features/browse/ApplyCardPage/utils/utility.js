// 9fbef606107a605d69c0edbcd8029e5d
import { routerPush } from '../../../../../utils';
import userConstants from '../../../CnC/LoyaltyBanner/molecules/LoyaltyFooterSection/loyaltyFooterSection.constants';

const refFields = [
  'preScreenCode',
  'firstName',
  'lastName',
  'addressLine1',
  'addressLine2',
  'city',
  'statewocountry',
  'noCountryZip',
  'phoneNumberWithAlt',
  'altPhoneNumber',
  'month',
  'date',
  'year',
  'ssNumber',
  'iAgree',
  'emailAddress',
];

const userAddressData = (addressTemp) => {
  return {
    addressLine1: addressTemp.get('addressLine1') || '',
    addressLine2: addressTemp.get('addressLine2') || '',
    city: addressTemp.get('city') || '',
    state: addressTemp.get('state') || '',
    zipCode: addressTemp.get('zipCode'),
  };
};

const fetchBillingOrShippingAddress = (address) => {
  let plccAddress = {};
  address.map((item) => {
    if (item.xcont_isBillingAddress === 'true' && item.xcont_isDefaultBilling === 'true') {
      plccAddress = item;
    }
    return true;
  });

  if (!Object.keys(plccAddress).length) {
    const primaryShippingAddress = address.filter((item) => item.primary === 'true');
    primaryShippingAddress.map((item) => {
      if (item.xcont_isShippingAddress === 'true') {
        plccAddress = item;
      }
      return true;
    });
  }
  return plccAddress;
};

const fetchPLCCFormErrors = (errors) => {
  const plccFormFields = [];
  refFields.forEach((fieldName) => {
    if (errors[fieldName]) {
      plccFormFields.push(fieldName);
    }
  });
  return plccFormFields;
};

/**
 * @const getModalSizeForApprovedPLCC - returning grid row size for approved plcc modal.
 *
 */
const getModalSizeForApprovedPLCC = (isPLCCModalFlow) => {
  return isPLCCModalFlow ? 12 : 8;
};

/**
 * @const redirectToBag - function to return to bag page.
 *
 */
const redirectToBag = ({ resetPLCCResponse, closePLCCModal }) => {
  resetPLCCResponse({ status: null });
  if (closePLCCModal) {
    closePLCCModal();
  }
  routerPush('/bag', '/bag');
};

/**
 * @const redirectToHome - function to return home.
 *
 * @param - isModalFlow - Check whether working on a modal based plcc flow.
 * @param - closeModal - Function to trigger the closure of modal.
 * @param - resetResponse - Reset response of form submission.
 *
 */
const redirectToHome = (isModalFlow, closeModal, resetResponse) => {
  if (isModalFlow && closeModal) {
    closeModal();
  }
  // reseting the plcc form submission response.
  resetResponse({ status: null });
  routerPush('/', '/home');
};

const redirectToHomeOrBack = (isModalFlow, closeModal, resetResponse) => {
  if (isModalFlow && closeModal) {
    closeModal();
  }
  // reseting the plcc form submission response.
  resetResponse({ status: null });
  if (!isModalFlow) {
    if (window.history && window.history.back) {
      window.history.back();
    } else {
      routerPush('/', '/home');
    }
  }
};

/**
 * @const getPageViewGridColumnSize - returning grid columns for plcc forms
 *
 */
const getPageViewGridColumnSize = (isPLCCModalFlow) => {
  return isPLCCModalFlow ? 6 : 5;
};

/**
 * @const getPageViewGridRowSize - returning grid rows for plcc forms
 *
 */
const getPageViewGridRowSize = (isPLCCModalFlow) => {
  return isPLCCModalFlow ? 12 : 10;
};

/**
 * @const getFooterButtonSize - returning grid rows for plcc forms.
 * @param isPLCCModalFlow - flag to check for a modal flow.
 *
 */
const getFooterButtonSize = (isPLCCModalFlow) => {
  return isPLCCModalFlow ? 3 : 4;
};

const routeToAnalyticsEventMap = {
  applycardpage: 'Landing Page',
  shipping: 'Shipping Page',
  shippingpage: 'Shipping Page',
  billing: 'Billing Page',
  review: 'Review Page',
  bagpage: 'Bag',
  account: 'Account Overview',
  minibag: 'Mini Bag',
  isaddedtobagpage: 'Added To Bag',
  productdetail: 'PDP',
  login: 'Login',
  createaccount: 'Create Account',
  pickup: 'Pickup',
  isproductdetailview: 'PDP',
  guestbilling: 'Billing Page',
};

/**
 * @description - returns user type.
 *
 */
const getUserType = (isGuestUser, plccUser) => {
  let userType = '';
  if (isGuestUser) {
    userType = userConstants.GUEST_USER;
  } else {
    userType = plccUser ? userConstants.PLCC_USER : userConstants.MPR_USER;
  }
  return userType;
};

export {
  refFields,
  userAddressData,
  fetchBillingOrShippingAddress,
  redirectToBag,
  redirectToHome,
  getPageViewGridColumnSize,
  getPageViewGridRowSize,
  getModalSizeForApprovedPLCC,
  fetchPLCCFormErrors,
  getFooterButtonSize,
  routeToAnalyticsEventMap,
  getUserType,
  redirectToHomeOrBack,
};
