// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  getAutocompleteTypesParam,
  getMapboxSwitch,
  getPageNameFromState,
  getCreateAccountAfterGuestUserApplyForPLCCCard,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import { trackPageView } from '@tcp/core/src/analytics/actions';
import { changePassword } from '@tcp/core/src/components/features/account/ChangePassword/container/ChangePassword.actions';
import constants from './ApplyCard.constants';
import BagPageSelector from '../../../CnC/BagPage/container/BagPage.selectors';
import ApplyCardLayoutView from '../views';
import { fetchModuleX, resetPLCCResponse, submitInstantCardApplication } from './ApplyCard.actions';
import { isPlccUser } from '../../../account/User/container/User.selectors';
import CheckoutSelectors from '../../../CnC/Checkout/container/Checkout.selector';
import {
  getUserProfileData,
  getUserId,
  isGuest,
  getIsRegisteredUser,
  getNewUserTempPass,
  getIsPlccDone,
} from './ApplyCard.selectors';
import { getFormValidationErrorMessages } from '../../../account/Account/container/Account.selectors';
import AddressVerification from '../../../../common/organisms/AddressVerification/container/AddressVerification.container';
import { verifyAddress } from '../../../../common/organisms/AddressVerification/container/AddressVerification.actions';
import { isMobileApp, routerPush } from '../../../../../utils';
import names from '../../../../../constants/eventsName.constants';

class ApplyCardLayoutContainer extends React.Component {
  /**
   *  @function - constructor
   *
   *  @state - showAddEditAddressForm - state member that decides whether to show or hide th do verify contact window.
   */

  constructor(props) {
    super(props);
    this.state = {
      showAddEditAddressForm: false,
      hideAddressEditAddressFormPending: false,
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (
      state.hideAddressEditAddressFormPending &&
      !props.loaderState &&
      state.showAddEditAddressForm
    ) {
      return {
        showAddEditAddressForm: false,
      };
    }
    return null;
  }

  componentDidMount() {
    const { plccData, fetchModuleXContent, labels, plccUser, navigation, isPLCCModalFlow } =
      this.props;
    const { resetPLCCApplicationStatus, trackPageViewPlcc } = this.props;
    resetPLCCApplicationStatus({ status: null });
    if (plccUser) {
      if (isMobileApp() && isPLCCModalFlow) {
        navigation.navigate('Home');
      }
      if (!isMobileApp() && !isPLCCModalFlow) {
        routerPush(window.location.href, '/home');
      }
    } else if (!plccData && labels && labels.referred) {
      fetchModuleXContent(labels.referred);
    }
    const { MYPLACE, PLACECARD_APPLICATION } = constants;
    trackPageViewPlcc({
      pageName: PLACECARD_APPLICATION,
      pageSection: MYPLACE,
      pageSubSection: MYPLACE,
      pageType: MYPLACE,
      pageShortName: PLACECARD_APPLICATION,
    });
  }

  /**
   *  @fatarrow - formatPayload
   *  @param - payload - contains payload of plcc form.
   *
   *  @description - deals with form final submission.
   */
  formatPayload = (payload) => {
    const { addressLine1, addressLine2, noCountryZip, primary, ...otherPayload } = payload;
    return {
      ...otherPayload,
      ...{
        address1: addressLine1,
        address2: addressLine2,
        zip: noCountryZip,
        primary: primary ? 'true' : 'false',
      },
    };
  };

  /**
   *  @fatarrow - submitPLCCForm
   *  @param - formData - contains the data of redux form.
   *
   *  @description - submits for an instant credit card
   */
  submitPLCCForm = (formData) => {
    const { verifyAddressAction, resetPLCCApplicationStatus } = this.props;
    resetPLCCApplicationStatus({ status: null });
    const payload = Object.assign({}, formData);
    const formattedPayload = this.formatPayload(payload);
    if (Object.keys(formattedPayload).length) {
      verifyAddressAction({ state: formattedPayload.statewocountry, ...formattedPayload });
      this.setState({
        showAddEditAddressForm: true,
        formData,
        hideAddressEditAddressFormPending: false,
      });
    }
  };

  /**
   *  @fatarrow - submitForm
   *
   *  @description - deals with form final submission.
   */
  submitForm = () => {
    const { submitApplication, userId } = this.props;
    const { formData } = this.state;

    const userData = Object.assign({}, formData);
    if (userData) {
      userData.userId = userId;
    }
    this.setState({ hideAddressEditAddressFormPending: true });
    submitApplication(userData);
  };

  closeAddressVerificationModal = () => {
    this.setState({ showAddEditAddressForm: false });
  };

  changePassword = ({ password, logonId, isPLCCFlow }) => {
    const { changePasswordAction, newUserTempPass } = this.props;
    changePasswordAction({
      currentPassword: newUserTempPass,
      newPassword: password,
      newPasswordVerify: password,
      logonId,
      operation: 'resetPassword',
      fromPLCCModal: isPLCCFlow,
    });
  };

  render() {
    const {
      applicationStatus,
      approvedPLCCData,
      activeErrorField,
      isPLCCModalFlow,
      plccData,
      isGuestUser,
      labels,
      plccUser,
      profileInfo,
      applyCard,
      toggleModal,
      resetPLCCApplicationStatus,
      closeModal,
      isRtpsFlow,
      closePLCCModal,
      togglePLCCModal,
      cartOrderItems,
      formErrorMessage,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      loyaltyPageName,
      isRegisteredUser,
      newUserTempPass,
      isPlccDone,
      isCreateAccountAfterGuestUserApplyForPLCCCardEnabled,
    } = this.props;
    const { showAddEditAddressForm } = this.state;
    return (
      <React.Fragment>
        <ApplyCardLayoutView
          applicationStatus={applicationStatus}
          activeErrorField={activeErrorField}
          labels={labels}
          plccData={plccData}
          isGuest={isGuestUser}
          submitPLCCForm={this.submitPLCCForm}
          approvedPLCCData={approvedPLCCData}
          plccUser={plccUser}
          profileInfo={profileInfo}
          isPLCCModalFlow={isPLCCModalFlow}
          toggleModal={toggleModal}
          applyCard={applyCard}
          onSubmit={this.submitPLCCForm}
          resetPLCCApplicationStatus={resetPLCCApplicationStatus}
          showAddEditAddressForm={showAddEditAddressForm}
          submitForm={this.submitForm}
          closeAddressVerificationModal={this.closeAddressVerificationModal}
          closeModal={closeModal}
          isRtpsFlow={isRtpsFlow}
          closePLCCModal={closePLCCModal}
          togglePLCCModal={togglePLCCModal}
          formErrorMessage={formErrorMessage}
          cartOrderItems={cartOrderItems}
          mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
          mapboxSwitch={mapboxSwitch}
          loyaltyPageName={loyaltyPageName}
          isRegisteredUser={isRegisteredUser}
          newUserTempPass={newUserTempPass}
          changePassword={this.changePassword}
          isPlccDone={isPlccDone}
          isCreateAccountAfterGuestUserApplyForPLCCCardEnabled={
            isCreateAccountAfterGuestUserApplyForPLCCCardEnabled
          }
        />
        {!isMobileApp() && showAddEditAddressForm ? (
          <AddressVerification onSuccess={this.submitForm} />
        ) : null}
      </React.Fragment>
    );
  }
}

ApplyCardLayoutContainer.propTypes = {
  plccData: PropTypes.shape({}).isRequired,
  activeErrorField: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  fetchModuleXContent: PropTypes.func.isRequired,
  isPLCCModalFlow: PropTypes.bool.isRequired,
  submitApplication: PropTypes.func.isRequired,
  applicationStatus: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  plccUser: PropTypes.bool.isRequired,
  profileInfo: PropTypes.shape({}).isRequired,
  verifyAddressAction: PropTypes.func.isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  isGuestUser: PropTypes.bool.isRequired,
  userId: PropTypes.string.isRequired,
  applyCard: PropTypes.bool.isRequired,
  toggleModal: PropTypes.shape({}).isRequired,
  resetPLCCApplicationStatus: PropTypes.func.isRequired,
  closeModal: PropTypes.func,
  isRtpsFlow: PropTypes.bool.isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  formErrorMessage: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape([]),
  trackPageViewPlcc: PropTypes.func.isRequired,
  mapboxAutocompleteTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  loyaltyPageName: PropTypes.string,
  isRegisteredUser: PropTypes.bool,
  newUserTempPass: PropTypes.string,
  changePasswordAction: PropTypes.func,
  isPlccDone: PropTypes.bool,
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled: PropTypes.bool,
};

ApplyCardLayoutContainer.defaultProps = {
  closeModal: () => {},
  cartOrderItems: [],
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  loyaltyPageName: 'account',
  isRegisteredUser: false,
  newUserTempPass: '',
  changePasswordAction: () => {},
  isPlccDone: false,
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled: false,
};

/* istanbul ignore next */
export const mapStateToProps = (state) => {
  const { ApplyCardPage, Labels, PageLoader } = state;

  return {
    applicationStatus: ApplyCardPage && ApplyCardPage.applicationStatus,
    approvedPLCCData: ApplyCardPage && ApplyCardPage.approvedPLCCData,
    activeErrorField: ApplyCardPage && ApplyCardPage.activeErrorField,
    loaderState: PageLoader && PageLoader.loaderState,
    plccData: ApplyCardPage && ApplyCardPage.plccData,
    plccUser: isPlccUser(state),
    isGuestUser: isGuest(state),
    profileInfo: getUserProfileData(state),
    labels: Labels && Labels.global && Labels.global.plccForm,
    userId: getUserId(state),
    isRtpsFlow: CheckoutSelectors.getIsRtpsFlow(state),
    cartOrderItems: BagPageSelector.getOrderItems(state),
    formErrorMessage: getFormValidationErrorMessages(state),
    mapboxAutocompleteTypesParam: getAutocompleteTypesParam(state),
    mapboxSwitch: getMapboxSwitch(state),
    loyaltyPageName: getPageNameFromState(state),
    isRegisteredUser: getIsRegisteredUser(state),
    newUserTempPass: getNewUserTempPass(state),
    isPlccDone: getIsPlccDone(state),
    isCreateAccountAfterGuestUserApplyForPLCCCardEnabled:
      getCreateAccountAfterGuestUserApplyForPLCCCard(state),
  };
};

/* istanbul ignore next */
export const mapDispatchToProps = (dispatch) => {
  return {
    submitApplication: (payload) => {
      dispatch(submitInstantCardApplication(payload));
    },
    fetchModuleXContent: (contentId) => {
      dispatch(fetchModuleX(contentId));
    },
    resetPLCCApplicationStatus: (payload) => {
      dispatch(resetPLCCResponse(payload));
    },
    verifyAddressAction: (payload) => {
      dispatch(verifyAddress(payload));
    },
    togglePLCCModal: (payload) => {
      dispatch(toggleApplyNowModal(payload));
    },
    changePasswordAction: (payload) => {
      dispatch(changePassword(payload));
    },
    trackPageViewPlcc: (payload) => {
      const { ...restPayload } = payload;
      dispatch(
        trackPageView({
          currentScreen: names.screenNames.plcc,
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...restPayload,
                },
              },
            },
          },
        })
      );
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplyCardLayoutContainer);
