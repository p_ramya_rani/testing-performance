// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../RewardsCard.constants';

const initialState = {
  contact_information_disclaimer: '',
  pre_screen_code: '',
  plccEligible: false,
  activeErrorField: null,
  isRegistered: true,
  tempPass: '',
  isPLCCDone: false,
  isCreateAdsAccountFailed: false,
};

const ApplyCardReducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.SET_MODULEX_CONTENT:
      return {
        ...state,
        plccData: action.payload,
      };
    case constants.RESET_PLCC_APPLICATION_RESPONSE:
      return {
        ...state,
        applicationStatus: action.payload.status,
      };
    case constants.RESPONSE_INSTANT_CARD_APPLICATION:
      return {
        ...state,
        applicationStatus: action.payload.status,
        approvedPLCCData: action.payload,
      };
    case constants.SET_PLCC_ELIGIBLE:
      return {
        ...state,
        plccEligible: action.payload,
      };
    case constants.SET_PLCC_PRESCREEN_CODE:
      return {
        ...state,
        pre_screen_code: action.payload,
      };
    case constants.SET_ACTIVE_ERROR_FIELD:
      return {
        ...state,
        activeErrorField: action.payload,
      };
    case constants.SET_IS_REGISTERED_USER:
      return {
        ...state,
        isRegistered: action.payload,
      };
    case constants.SET_CREATE_ACCOUNT_PASS:
      return {
        ...state,
        tempPass: action.payload,
      };
    case constants.SET_CREATE_ADS_ACCOUNT_FAILED:
      return {
        ...state,
        isCreateAdsAccountFailed: action.payload,
      };
    case constants.SET_IS_PLCC_DONE:
      return {
        ...state,
        isPLCCDone: action.payload,
      };
    default:
      return state;
  }
};

export default ApplyCardReducer;
