// 9fbef606107a605d69c0edbcd8029e5d 
const CONSTANTS = {
  PLACECARD_APPLICATION: 'myplace:placecard:application',
  MYPLACE: 'myplace',
  SEO_PATH: '/place-card/application',
};

export default CONSTANTS;
