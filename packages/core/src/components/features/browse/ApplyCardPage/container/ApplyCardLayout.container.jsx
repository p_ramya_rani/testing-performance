// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable extra-rules/no-commented-out-code */
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router'; //eslint-disable-line
import PropTypes from 'prop-types';
import hoistNonReactStatic from 'hoist-non-react-statics';
import {
  getAutocompleteTypesParam,
  getMapboxSwitch,
  getCreateAccountAfterGuestUserApplyForPLCCCard,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import { getPageCategory } from '@tcp/core/src/analytics/Analytics.selectors';
import { handleClickData } from '@tcp/core/src/components/features/CnC/LoyaltyBanner/molecules/LoyaltyFooterSection/views/LoyaltyFooterSection';
import { getUserType } from '@tcp/core/src/components/features/browse/ApplyCardPage/utils/utility';
import { loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import {
  getIcidValue,
  internalCampaignProductAnalyticsList,
  getAPIConfig,
} from '@tcp/core/src/utils';
import { trackPageView, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import { changePassword } from '@tcp/core/src/components/features/account/ChangePassword/container/ChangePassword.actions';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import { getIsPLCCModalOpen } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.selectors';
import BagPageSelector from '../../../CnC/BagPage/container/BagPage.selectors';
import ApplyCardLayoutView from '../views';
import { fetchModuleX, resetPLCCResponse, submitInstantCardApplication } from './ApplyCard.actions';
import { isPlccUser } from '../../../account/User/container/User.selectors';
import CheckoutSelectors from '../../../CnC/Checkout/container/Checkout.selector';
import {
  getUserProfileData,
  getUserId,
  isGuest,
  getSeo,
  getIsRegisteredUser,
  getNewUserTempPass,
} from './ApplyCard.selectors';
import { getFormValidationErrorMessages } from '../../../account/Account/container/Account.selectors';
import AddressVerification from '../../../../common/organisms/AddressVerification/container/AddressVerification.container';
import { verifyAddress } from '../../../../common/organisms/AddressVerification/container/AddressVerification.actions';
import { isMobileApp } from '../../../../../utils';
import APPLY_CARD_CONSTANTS from './ApplyCard.constants';

class ApplyCardLayoutContainer extends React.Component {
  /**
   *  @function - constructor
   *
   *  @state - showAddEditAddressForm - state member that decides whether to show or hide th do verify contact window.
   */

  constructor(props) {
    super(props);
    this.state = {
      showAddEditAddressForm: false,
    };
  }

  componentDidMount() {
    const { fetchModuleXContent, labels } = this.props;
    const { resetPLCCApplicationStatus } = this.props;
    resetPLCCApplicationStatus({ status: null });
    if (labels && labels.referred) {
      fetchModuleXContent(labels.referred);
    }
    this.trackPageLoadAnalytics();
  }

  trackPageLoadAnalytics = () => {
    const { trackPLCCPageLoad, router } = this.props;
    const icidValue = router && router.asPath && getIcidValue(router.asPath);
    const products = internalCampaignProductAnalyticsList();
    const PLCC = 'plcc';

    trackPLCCPageLoad({
      internalCampaignIdList: products,
      internalCampaignId: icidValue,
      pageName: PLCC,
      pageSection: PLCC,
      pageSubSection: PLCC,
      pageType: PLCC,
    });
  };

  /**
   *  @fatarrow - formatPayload
   *  @param - payload - contains payload of plcc form.
   *
   *  @description - deals with form final submission.
   */
  formatPayload = (payload) => {
    const { addressLine1, addressLine2, noCountryZip, primary, ...otherPayload } = payload;
    return {
      ...otherPayload,
      ...{
        address1: addressLine1,
        address2: addressLine2,
        zip: noCountryZip,
        primary: primary ? 'true' : 'false',
      },
    };
  };

  setAnalyticsVars = () => {
    const { isGuestUser, plccUser, pageCategory } = this.props;
    const userType = getUserType(isGuestUser, plccUser);
    const data = handleClickData(userType, pageCategory);
    setClickAnalyticsData({ ...data });
  };

  /**
   *  @fatarrow - submitPLCCForm
   *  @param - formData - contains the data of redux form.
   *
   *  @description - submits for an instant credit card
   */
  submitPLCCForm = (formData) => {
    const { verifyAddressAction, resetPLCCApplicationStatus } = this.props;
    resetPLCCApplicationStatus({ status: null });
    const ioBlackBoxElem = document.querySelector('#ioBlackBox');
    const payload = Object.assign({}, formData);
    const updatedFormData = {
      ...formData,
      BF_ioBlackBox: ioBlackBoxElem ? ioBlackBoxElem.value : '',
    };
    const formattedPayload = this.formatPayload(payload);
    if (Object.keys(formattedPayload).length) {
      verifyAddressAction({ state: formattedPayload.statewocountry, ...formattedPayload });
      this.setState({ showAddEditAddressForm: true, formData: updatedFormData });
    }
  };

  /**
   *  @fatarrow - submitForm
   *
   *  @description - deals with form final submission.
   */
  submitForm = () => {
    const { submitApplication, userId } = this.props;
    const { formData } = this.state;
    this.setState({ showAddEditAddressForm: false });
    const userData = Object.assign({}, formData);
    if (userData) {
      userData.userId = userId;
    }
    submitApplication(userData);
  };

  closeAddressVerificationModal = () => {
    this.setState({ showAddEditAddressForm: false });
  };

  changePassword = ({ password, logonId }) => {
    const { changePasswordAction, newUserTempPass, isPLCCModalOpen, togglePLCCModal } = this.props;
    changePasswordAction({
      currentPassword: newUserTempPass,
      newPassword: password,
      newPasswordVerify: password,
      logonId,
      operation: 'resetPassword',
      showLoader: true,
    });
    if (isPLCCModalOpen) {
      setTimeout(() => {
        togglePLCCModal({ isPLCCModalOpen: false });
      }, 1000);
    }
  };

  render() {
    const {
      applicationStatus,
      approvedPLCCData,
      activeErrorField,
      isPLCCModalFlow,
      plccData,
      isGuestUser,
      labels,
      plccUser,
      profileInfo,
      applyCard,
      toggleModal,
      resetPLCCApplicationStatus,
      closeModal,
      isRtpsFlow,
      closePLCCModal,
      togglePLCCModal,
      cartOrderItems,
      router,
      formErrorMessage,
      fullPageAuthEnabled,
      mapboxAutocompleteTypesParam,
      mapboxSwitch,
      pageCategory,
      isRegisteredUser,
      newUserTempPass,
      isCreateAccountAfterGuestUserApplyForPLCCCardEnabled,
      isCreateAdsAccountFailed,
      isPLCCModalOpen,
    } = this.props;

    const { showAddEditAddressForm } = this.state;
    return (
      <React.Fragment>
        <ApplyCardLayoutView
          applicationStatus={applicationStatus}
          activeErrorField={activeErrorField}
          labels={labels}
          plccData={plccData}
          isGuest={isGuestUser}
          submitPLCCForm={this.submitPLCCForm}
          approvedPLCCData={approvedPLCCData}
          plccUser={plccUser}
          profileInfo={profileInfo}
          isPLCCModalFlow={isPLCCModalFlow}
          toggleModal={toggleModal}
          applyCard={applyCard}
          onSubmit={this.submitPLCCForm}
          resetPLCCApplicationStatus={resetPLCCApplicationStatus}
          showAddEditAddressForm={showAddEditAddressForm}
          submitForm={this.submitForm}
          closeAddressVerificationModal={this.closeAddressVerificationModal}
          closeModal={closeModal}
          isRtpsFlow={isRtpsFlow}
          closePLCCModal={closePLCCModal}
          togglePLCCModal={togglePLCCModal}
          cartOrderItems={cartOrderItems}
          setAnalyticsVars={this.setAnalyticsVars}
          router={router}
          formErrorMessage={formErrorMessage}
          fullPageAuthEnabled={fullPageAuthEnabled}
          mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
          mapboxSwitch={mapboxSwitch}
          pageCategory={pageCategory}
          isRegisteredUser={isRegisteredUser}
          newUserTempPass={newUserTempPass}
          changePassword={this.changePassword}
          isCreateAccountAfterGuestUserApplyForPLCCCardEnabled={
            isCreateAccountAfterGuestUserApplyForPLCCCardEnabled
          }
          isCreateAdsAccountFailed={isCreateAdsAccountFailed}
          isPLCCModalOpen={isPLCCModalOpen}
        />
        {!isMobileApp() && showAddEditAddressForm ? (
          <AddressVerification onSuccess={this.submitForm} />
        ) : null}
      </React.Fragment>
    );
  }
}

ApplyCardLayoutContainer.propTypes = {
  plccData: PropTypes.shape({}).isRequired,
  activeErrorField: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  fetchModuleXContent: PropTypes.func.isRequired,
  isPLCCModalFlow: PropTypes.bool.isRequired,
  submitApplication: PropTypes.func.isRequired,
  applicationStatus: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  plccUser: PropTypes.bool.isRequired,
  profileInfo: PropTypes.shape({}).isRequired,
  verifyAddressAction: PropTypes.func.isRequired,
  approvedPLCCData: PropTypes.shape({}).isRequired,
  isGuestUser: PropTypes.bool.isRequired,
  userId: PropTypes.string.isRequired,
  applyCard: PropTypes.bool.isRequired,
  toggleModal: PropTypes.shape({}).isRequired,
  resetPLCCApplicationStatus: PropTypes.func.isRequired,
  closeModal: PropTypes.func,
  isRtpsFlow: PropTypes.bool.isRequired,
  closePLCCModal: PropTypes.func.isRequired,
  togglePLCCModal: PropTypes.func.isRequired,
  router: PropTypes.shape({}).isRequired,
  cartOrderItems: PropTypes.shape([]),
  pageCategory: PropTypes.string,
  trackPLCCPageLoad: PropTypes.func,
  formErrorMessage: PropTypes.shape({}).isRequired,
  fullPageAuthEnabled: PropTypes.bool,
  mapboxAutocompleteTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  isRegisteredUser: PropTypes.bool,
  newUserTempPass: PropTypes.string,
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled: PropTypes.string,
  isPLCCModalOpen: PropTypes.bool,
  isCreateAdsAccountFailed: PropTypes.bool,
};

ApplyCardLayoutContainer.defaultProps = {
  closeModal: () => {},
  cartOrderItems: [],
  pageCategory: 'account',
  trackPLCCPageLoad: () => {},
  fullPageAuthEnabled: false,
  mapboxAutocompleteTypesParam: '',
  mapboxSwitch: true,
  isRegisteredUser: false,
  newUserTempPass: '',
  isCreateAccountAfterGuestUserApplyForPLCCCardEnabled: false,
  isPLCCModalOpen: false,
  isCreateAdsAccountFailed: false,
};

/* istanbul ignore next */
export const mapStateToProps = (state) => {
  const { ApplyCardPage, Labels } = state;
  return {
    applicationStatus: ApplyCardPage && ApplyCardPage.applicationStatus,
    approvedPLCCData: ApplyCardPage && ApplyCardPage.approvedPLCCData,
    activeErrorField: ApplyCardPage && ApplyCardPage.activeErrorField,
    isCreateAdsAccountFailed: ApplyCardPage && ApplyCardPage.isCreateAdsAccountFailed,
    plccData: ApplyCardPage && ApplyCardPage.plccData,
    plccUser: isPlccUser(state),
    isGuestUser: isGuest(state),
    profileInfo: getUserProfileData(state),
    labels: Labels && Labels.global && Labels.global.plccForm,
    userId: getUserId(state),
    isRtpsFlow: CheckoutSelectors.getIsRtpsFlow(state),
    cartOrderItems: BagPageSelector.getOrderItems(state),
    pageCategory: getPageCategory(state),
    seoData: getSeo(state),
    formErrorMessage: getFormValidationErrorMessages(state),
    fullPageAuthEnabled: getEnableFullPageAuth(state),
    mapboxAutocompleteTypesParam: getAutocompleteTypesParam(state),
    mapboxSwitch: getMapboxSwitch(state),
    isRegisteredUser: getIsRegisteredUser(state),
    newUserTempPass: getNewUserTempPass(state),
    isCreateAccountAfterGuestUserApplyForPLCCCardEnabled:
      getCreateAccountAfterGuestUserApplyForPLCCCard(state),
    isPLCCModalOpen: getIsPLCCModalOpen(state),
  };
};

/* istanbul ignore next */
export const mapDispatchToProps = (dispatch) => {
  return {
    submitApplication: (payload) => {
      dispatch(submitInstantCardApplication(payload));
    },
    fetchModuleXContent: (contentId) => {
      dispatch(fetchModuleX(contentId));
    },
    resetPLCCApplicationStatus: (payload) => {
      dispatch(resetPLCCResponse(payload));
    },
    verifyAddressAction: (payload) => {
      dispatch(verifyAddress(payload));
    },
    togglePLCCModal: (payload) => {
      dispatch(toggleApplyNowModal(payload));
    },
    trackPLCCPageLoad: (payload) => {
      dispatch(setClickAnalyticsData(payload));
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...payload,
                },
              },
            },
          },
        })
      );
      const analyticsTimer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(analyticsTimer);
      }, 100);
    },
    changePasswordAction: (payload) => {
      dispatch(changePassword(payload));
    },
  };
};

ApplyCardLayoutContainer.getInitialProps = async ({ store, res, isServer }, pageProps) => {
  let apiConfig;
  if (isServer) {
    const {
      locals: { apiConfig: config },
    } = res;
    apiConfig = config;
  } else {
    apiConfig = getAPIConfig();
  }
  store.dispatch(loadPageSEOData({ page: APPLY_CARD_CONSTANTS.SEO_PATH, apiConfig }));
  return pageProps;
};

ApplyCardLayoutContainer.pageInfo = {
  pageId: 'place-card',
};

const ApplyCardConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplyCardLayoutContainer);

const ApplyCardWithRouter = withRouter(ApplyCardConnectComponent);

hoistNonReactStatic(ApplyCardWithRouter, ApplyCardConnectComponent, {
  getInitialProps: true,
});

export default ApplyCardWithRouter;
