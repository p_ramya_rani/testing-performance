// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select } from 'redux-saga/effects';
import CheckoutSelectors from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { createsaga } from '@tcp/core/src/components/features/account/CreateAccount/container/CreateAccount.saga';
import { getCouponList } from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import { setClickAnalyticsData, trackClick, trackFormError } from '@tcp/core/src/analytics/actions';
import { getAPIConfig, parseBoolean } from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import { setIsPlccDone } from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.actions';
import constants, { ERR_CONFIG } from '../RewardsCard.constants';
import { setModuleX, obtainInstantCardApplication, setIsRegisteredUser } from './ApplyCard.actions';
import {
  setPlccCardIdActn,
  setPlccCardNumberActn,
  getUserInfo,
} from '../../../account/User/container/User.actions';
import { isGuest } from '../../../CnC/Checkout/container/Checkout.selector';
import { getAddressList } from '../../../account/AddressBook/container/AddressBook.actions';
import { getCardList } from '../../../account/Payment/container/Payment.saga';
import { getModuleX } from '../../../../../services/abstractors/common/moduleXComposite';
import applyInstantCard from '../../../../../services/abstractors/common/PLCC';
import { validateReduxCache } from '../../../../../utils/cache.util';
import {
  getErrorMapping,
  getRtpsPreScreenData,
  getApplyCardModuleXComposite,
} from './ApplyCard.selectors';
import { toastMessageInfo } from '../../../../common/atoms/Toast/container/Toast.actions';
import errorMessages from '../../../../../services/handler/stateful/errorResponseMapping/index.json';
import { isMobileApp } from '../../../../../utils';
import BAG_PAGE_ACTIONS, {
  updateMiniBagFlag,
} from '../../../CnC/BagPage/container/BagPage.actions';
import names from '../../../../../constants/eventsName.constants';
import {
  getPageNameFromState,
  getCreateAccountAfterGuestUserApplyForPLCCCard,
} from '../../../../../reduxStore/selectors/session.selectors';

const { getCurrentCheckoutStage, getIsRtpsFlow } = CheckoutSelectors;

function getRequiredList3Vals(payload) {
  const {
    firstName,
    middleNameInitial,
    lastName,
    addressLine1,
    addressLine2,
    city,
    noCountryZip,
    statewocountry,
    phoneNumberWithAlt,
    altPhoneNumber,
    emailAddress,
    month,
    date,
    year,
    ssNumber,
  } = payload;
  return {
    firstName,
    middleNameInitial,
    lastName,
    addressLine1,
    addressLine2,
    city,
    noCountryZip,
    statewocountry,
    phoneNumberWithAlt,
    altPhoneNumber,
    emailAddress,
    month,
    date,
    year,
    ssNumber,
  };
}

/*
 * @Generator - fetchModuleX Saga -
 * @params - payload - array of contentIds
 *
 * @description - Helper Saga function to fetch the richtext data from module X.
 */

export function* fetchModuleX({ payload = '' }) {
  try {
    const applyCardModuleX = yield select(getApplyCardModuleXComposite);
    const applyCardModuleXObjLen =
      applyCardModuleX &&
      Object.getOwnPropertyNames(applyCardModuleX) &&
      Object.getOwnPropertyNames(applyCardModuleX).length;
    if (!applyCardModuleX || !applyCardModuleXObjLen) {
      const result = yield call(getModuleX, payload);
      yield put(setModuleX(result));
    }
  } catch (err) {
    yield null;
  }
}

function* callAnalytics(isRTPSFlow) {
  let pageName = '';
  let name = 'PLCC_Submit132';
  if (!isMobileApp()) {
    pageName = window && window.loyaltyLocation;
  } else {
    pageName = yield select(getPageNameFromState);
    if (isRTPSFlow) {
      pageName = `rtps-${pageName}`;
    }
    name = names.screenNames.loyalty_plcc_submit_success;
  }

  yield put(
    setClickAnalyticsData({
      eventName: 'PLCC_Submit132',
      customEvents: ['event132'],
      loyaltyLocation: pageName,
    })
  );
  yield put(trackClick({ name }));
}

export function shouldMakeCreateAccountCall({ isRegistered, isMprIdExist }) {
  let callFlag = false;
  if (
    (isRegistered === true && isMprIdExist === false) ||
    (isRegistered === false && isMprIdExist === true) ||
    (isRegistered === false && isMprIdExist === false)
  ) {
    callFlag = true;
  }
  return callFlag;
}
export function isApproved(status) {
  return status === 'APPROVED';
}

function* callCreateAccountPLCC({ res, payload }) {
  if (isApproved(res.status)) {
    const isRegistered = parseBoolean(res.isRegistered);
    const isMprIdExist = parseBoolean(res.isMprIdExist);
    yield put(setIsRegisteredUser(isRegistered));
    yield put(setIsPlccDone(false));
    if (shouldMakeCreateAccountCall({ isRegistered, isMprIdExist })) {
      const apiConfig = getAPIConfig();
      const createAccountPayload = {
        adsCreateAccount: true,
        password: '',
        xCreditCardId: res.xCardId,
        rememberCheck: false,
        rememberMe: false,
        firstName: payload.firstName,
        lastName: payload.lastName,
        logonId: payload.emailAddress,
        emailAddress: payload.emailAddress,
        zipCode: payload.noCountryZip || payload.zipCode,
        catalogId: apiConfig.catalogId,
        langId: apiConfig.langId,
        storeId: apiConfig.storeId,
        ordersession: true,
        coupon: res.couponCode,
        isRegistered,
        isMprIdExist,
        isApplyPlccCardFlow: true,
        response: payload.emailValidationStatus,
        phoneNumber: payload.phoneNumberWithAlt,
      };
      yield call(createsaga, { payload: createAccountPayload });
      yield put(getCouponList({ isSourceLogin: true, ignoreCache: true }));
    }
  }
}

/*
 * @Generator - submitCreditCardForm Saga -
 * @params - payload - form data for instant credit card form
 *
 * @description - Helper Saga function to submit the form.
 */

// eslint-disable-next-line complexity
export function* submitCreditCardForm({ payload = '' }) {
  try {
    yield put(setLoaderState(true));
    const labels = yield select(getErrorMapping);
    const { preScreenCode } = yield select(getRtpsPreScreenData);
    const isRTPSFlow = yield select(getIsRtpsFlow);
    const currentCheckoutStage = yield select(getCurrentCheckoutStage);
    const isCreateAccountAfterGuestUserApplyForPLCCCardEnabled = yield select(
      getCreateAccountAfterGuestUserApplyForPLCCCard
    );
    const res = yield call(
      applyInstantCard,
      payload,
      labels,
      preScreenCode,
      currentCheckoutStage === 'review',
      isRTPSFlow
    );
    if (isCreateAccountAfterGuestUserApplyForPLCCCardEnabled) {
      yield call(callCreateAccountPLCC, { res, payload });
    }

    yield put(setLoaderState(false));

    // GetRegisteredUserInfo post processWIC.
    const isGuestCheck = yield select(isGuest);
    if (!isGuestCheck) {
      yield put(getUserInfo({ ignoreCache: true }));
    }
    // Check for mobile App and to showcase the toast message of results.
    if (isMobileApp() && ERR_CONFIG.indexOf(res.status) === -1) {
      yield put(toastMessageInfo(res.status));
    } else if (typeof res.status === 'string') {
      yield put(trackFormError({ formName: 'PLCCForm', formData: getRequiredList3Vals(payload) }));
    }
    yield put(obtainInstantCardApplication(res));
    yield put(setPlccCardIdActn(res.onFileCardId || res.xCardId));
    yield put(setPlccCardNumberActn((res.cardNumber || '').substr(-4)));

    if (isRTPSFlow) {
      if (!isGuestCheck) {
        yield put(getAddressList({ ignoreCache: true }));
        yield call(getCardList, { ignoreCache: true });
      }
      yield put(
        BAG_PAGE_ACTIONS.getCartData({
          isRecalculateTaxes: false,
          excludeCartItems: false,
          recalcRewards: false,
          isCheckoutFlow: true,
          updateSmsInfo: false,
          translation: true,
        })
      );
    }
    if (!isMobileApp()) {
      yield put(updateMiniBagFlag(true));
    }
    yield call(callAnalytics, isRTPSFlow);
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'Apply Card Saga - submitCreditCardForm',
        payloadRecieved: payload,
      },
    });
    if (isMobileApp()) {
      yield put(toastMessageInfo(errorMessages.ERR_GENERIC_USER.errorMessage));
    }
    yield put(setLoaderState(false));
    yield put(trackFormError({ formName: 'PLCCForm', formData: getRequiredList3Vals(payload) }));
  } finally {
    yield put(setLoaderState(false));
  }
}

/*
 * @Generator - ApplyCreditCardSaga Saga -
 *
 * @description - Primary saga to yield moduleX data.
 */

export function* ApplyCreditCardSaga() {
  const cachedfetchModuleX = validateReduxCache(fetchModuleX);
  yield takeLatest(constants.FETCH_MODULEX_CONTENT, cachedfetchModuleX);
}

/*
 * @Generator - SubmitInstantCardApplication Saga -
 *
 * @description - Primary saga to submit the instant credit card form.
 */

export function* SubmitInstantCardApplication() {
  const cachedsubmitCreditCardForm = validateReduxCache(submitCreditCardForm);
  yield takeLatest(constants.SEND_INSTANT_CARD_APPLICATION, cachedsubmitCreditCardForm);
}

export default ApplyCreditCardSaga;
