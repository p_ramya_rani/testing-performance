/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const outfitDisabledArrowIcon = getIconPath('family-outfit-disabled-arrow');

export default css`
  .product-list {
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) => (props.isNewPDPEnabled ? `display: flex;` : '')}
    }
  }

  .hardcoded-plcc-wrapper {
    display: flex;
    flex: 1;
    height: 40px;
    padding-top: 7px;
  }
  .plcc-text-wrapper {
    font-size: 13px;
    line-height: 1;
    margin-top: 5px;
  }
  .hardcoded-plcc-text {
    white-space: pre;
  }
  .hardcoded-plcc {
    width: 427px;
    align-items: center;
    display: flex;
  }
  .hardcoded-plcc.image {
    width: 46px;
    height: 34px;
    padding-right: 7px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 39px;
      height: 29px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 33px;
      height: 24px;
      margin-top: 10px;
      padding-right: 6px;
    }
  }
  .hardcoded-plcc-text.text1 {
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
  }
  .hardcoded-plcc-text.text2 {
    color: #e86726;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
  }
  .hardcoded-plcc-text.text3 {
    color: ${(props) => props.theme.colors.REWARDS.PLCC};
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
  }
  .hardcoded-plcc-text.text4 {
    color: #e2499b;
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
  }
  .hardcoded-plcc-text.text5 {
    color: ${(props) => props.theme.colors.PRIMARY.GRAY};
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    margin-right: 3px;
  }
  .hardcoded-plcc-text.text6 {
    border: none;
    background: none;
    text-decoration: underline;
    color: ${(props) => props.theme.colors.TEXT.BLUE};
    cursor: pointer;
  }

  .product-summary-new-pdp {
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .new-pdp-product-detail-wrapper {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }
  .new-pdp-product-detail-below-wrapper {
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .placeholder div {
    background: #d8d8d8;
    padding: 10px 0;
    margin-bottom: 5px;
    text-align: center;
  }
  .also-available {
    font-family: Nunito;
    font-size: 16px;
    font-weight: 800;
    font-stretch: normal;
    font-style: normal;
    line-height: 0.88;
    letter-spacing: 0.57px;
    color: #1a1a1a;
    margin-top: ${(props) => (props.isNewPDPEnabled ? '26px' : '')};
  }
  .family-outfit-two-item {
    .slick-slide {
      width: 30% !important;
    }
  }

  .family-outfit-carousel-block {
    margin-bottom: 32px;
    margin-top: 16px;
    padding-bottom: 24px;
    border: solid 1px #d8d8d8;
    background-color: #ffffff;
    max-height: 183px;

    .slick-slide {
      width: 150px !important;
      position: relative;
    }
    .slick-track {
      display: flex;
      justify-content: center;
    }
    .slick-arrow.slick-prev,
    .slick-arrow.slick-next {
      display: block;
      height: 35px;
      width: 15px;
      top: 111.5px;
    }
    .slick-arrow.slick-prev {
      left: -50px;
      top: 111.5px;
    }
    .slick-arrow.slick-prev.slick-disabled {
      background: url(${outfitDisabledArrowIcon}) no-repeat;
      transform: translate(0, -50%);
      position: absolute;
      top: 111.5px;
      height: 35px;
      width: 15px;
      left: -50px;
    }

    .slick-arrow.slick-next.slick-disabled {
      background: url(${outfitDisabledArrowIcon}) no-repeat;
      transform: rotateY(180deg) translate(0, -50%);
      top: 111.5px;
      right: -50px;
    }

    .slick-arrow.slick-next {
      right: -50px;
      top: 111.5px;
    }
    .slick-dots {
      top: 180px;
      bottom: auto;
      position: absolute;
    }
    li.slick-active {
      margin: 0px;
      margin-top: 4px;
      width: 10px;
      height: 10px;
      padding-right: 35px;
    }
    .slick-dots li {
      position: relative;
      display: inline-block;
      margin: 0 2px;
      width: 6px;
      height: 6px;
      padding-right: 36px;
    }
    .slick-dots li button:before {
      content: '';
      box-sizing: border-box;
      position: absolute;
      top: 0;
      left: 0;
      width: 48px;
      height: 2px;
      border-radius: 4px;
      background: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
    }
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      .slick-arrow {
        display: none !important;
      }
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      .slick-dots {
        display: none;
      }
      .tcp_carousel_wrapper {
        margin-left: 64px;
        margin-right: 64px;
      }
    }

    .family-outfit-carousel-img {
      margin-top: 24px;
      height: 108px;
      max-width: 70px;
      margin-bottom: 5px;
      object-fit: contain;
      @media ${(props) => props.theme.mediaQuery.large} {
        max-width: calc(100% - 10px);
        height: 154px;
      }
    }
    .family-carousel-item-name {
      font-family: Nunito;
      font-size: 11px;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: 0.39px;
      color: #1a1a1a;
      margin-bottom: 30px;
      display: inline-block;
      @media ${(props) => props.theme.mediaQuery.large} {
        margin-bottom: 24px;
      }
    }
    .family-outfit-carousel-plus-icon {
      font-size: 16px;
      font-weight: bold;
      color: #1a1a1a;
      position: absolute;
      top: 67px;
      right: 0px;
      @media ${(props) => props.theme.mediaQuery.large} {
        top: 100px;
      }
    }
    .slick-arrow .slick-prev,
    .slick-arrow .slick-next {
      width: 19px;
      height: 39px;
    }
    .image-link {
      display: inline-block;
      @media ${(props) => props.theme.mediaQuery.large} {
        height: 0;
      }
    }
  }
  .carousel-container {
    position: relative;
    width: 100%;
  }

  .product-detail-section {
    flex: 1;
    @media ${(props) => props.theme.mediaQuery.medium} {
      .product-description-list {
        ${(props) => (props.isNewPDPEnabled ? `padding: 0px;` : ``)}
      }
    }
    position: relative;
    background-color: ${(props) => props.theme.colors.WHITE};
  }
  .key-feature-section {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled ? `background: ${props.theme.colors.PRIMARY.PALEGRAY};` : ``}
    }
  }
  .product-detail-section.complete-look-section {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled ? `background: ${props.theme.colors.PRIMARY.PALEGRAY};` : ``}
    }
  }

  .sticky-product-detail {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => (!props.isNewPDPEnabled ? `padding-top: 30px;` : '')}
    }
  }
  .product-description-list {
    position: relative;
    background-color: ${(props) => props.theme.colors.WHITE};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          border-radius: 18px;
     `
          : ``}
    }
  }
  .product-description-list.complete-the-look-section.add-border {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          padding:0 10px;
     `
          : ``}
    }
  }
  .also-available-wrapper-new-design {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `border: ${props.theme.spacing.ELEM_SPACING.SM} solid  ${props.theme.colors.PRIMARY.PALEGRAY};
      margin: 0px -${props.theme.spacing.ELEM_SPACING.REG};
      padding: ${props.theme.spacing.ELEM_SPACING.SM};
      background: ${props.theme.colors.WHITE};`
          : ``}
    }
  }
  .loyalty-banner-wrapper-new-design {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `border: ${props.theme.spacing.ELEM_SPACING.SM} solid  ${props.theme.colors.PRIMARY.PALEGRAY};
        margin: 0px -${props.theme.spacing.ELEM_SPACING.REG};
        padding: ${props.theme.spacing.ELEM_SPACING.SM};
        border-radius: ${props.theme.spacing.ELEM_SPACING.MED_1};background: ${props.theme.colors.WHITE};`
          : ``}
    }
  }
  .loyalty-banner-wrapper {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) =>
        props.isNewPDPEnabled &&
        ` padding: ${props.theme.spacing.ELEM_SPACING.MED_1} 0 ${props.theme.spacing.ELEM_SPACING.XL}`};
    }
  }
  .product-image-wrapper {
    margin-right: 0;
  }
  .sticky-mobile-image-wrapper {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-top: 13px;
      position: sticky;
      top: 25px;
    }
  }
  .product-detail-image-wrapper {
    margin-bottom: 8px;
  }
  .product-breadcrumb-wrapper {
    height: 20px;
    padding-top: ${(props) => (props.isDefaultBreadCrumb ? '35' : '25')}px;
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .button-go-back {
    border: none;
    display: flex;
    align-items: center;
    background: transparent;
    cursor: pointer;
  }
  .back-button {
    margin-left: 5px;
    color: ${(props) => props.theme.colors.TEXT.BLUE};
  }
  .go-back-container {
    padding-bottom: 18px;
    button {
      padding-left: 0;
    }
  }
  .product-summary-mobile-view {
    display: flex;
  }
  div.product-property-section {
    margin: 0;
    padding: 10px 0 20px;
    width: 100%;
  }
  .product-summary-desktop-view {
    display: none;
  }
  .promo-area-top {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
  }
  .pdp-qty {
    padding-top: 5px;
  }
  .promo-area-middle {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .promo-area-bottom {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .product-summary-wrapper {
    display: flex;
    flex-direction: column;
    flex: 1;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      border: ${(props) =>
        !props.isNewPDPEnabled && `1px solid ${props.theme.colorPalette.gray[500]}`};
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
      ${(props) =>
        props.productInfo.isGiftCard && props.isNewPDPEnabled ? 'padding-bottom: 0px;' : ''};
      border-left: 0;
      border-right: 0;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      border-bottom: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          border-bottom: none;
        `
          : ''}
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
    }
    .item-rating-meta {
      display: none;
    }
    .actual-price {
      font-size: ${(props) => props.theme.typography.fontSizes.fs22};
      color: ${(props) =>
        props.theme.isGymboree
          ? props.theme.colorPalette.gray[900]
          : props.theme.colorPalette.red[500]};
      display: inline-block;
      @media ${(props) => props.theme.mediaQuery.medium} {
        ${(props) =>
          props.isNewPDPEnabled ? `font-size: ${props.theme.typography.fontSizes.fs24};` : ''}
      }
    }

    .original-price {
      font-size: ${(props) => props.theme.typography.fontSizes.fs13};
    }
    .points-and-rewards {
      font-family: ${(props) => props.theme.typography.fonts.secondary};
      font-size: 14px;
    }
    .points-and-rewards-1 {
      color: ${(props) => props.theme.colors.PROMO.ORANGE};
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    }
    .points-and-rewards-2 {
      color: ${(props) => props.theme.colors.PROMO.BLUE};
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    }
    .points-and-rewards-or {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
      color: ${(props) => props.theme.colors.BLACK};
      font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      .product-details-header-container {
        margin-top: ${(props) => props.theme.typography.fontSizes.fs13};
      }
    }
  }
  .clear-button {
    border: none;
    background: transparent;
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    padding-bottom: 0;
    width: 35px;
  }
  .product-price-mobile-view {
    display: flex;
    ${(props) =>
      !props.isNewPDPEnabled &&
      `border-bottom: 1px solid ${props.theme.colors.PRIMARY.LIGHTGRAY};
      border-top: 1px solid ${props.theme.colors.PRIMARY.LIGHTGRAY};
      padding: 16px 0;`}
  }
  .product-price-desktop-view {
    display: none;
  }

  .pdp-price-container {
    min-height: 58px;
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .product-image-wrapper {
      margin-right: 30px;
    }
    .product-detail-image-wrapper {
      margin-bottom: 11px;
    }
    .product-summary-mobile-view {
      display: none;
    }
    .product-summary-desktop-view {
      display: flex;
    }
    .product-price-mobile-view {
      display: none;
    }
    .product-price-desktop-view {
      display: flex;
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      ${(props) =>
        !props.isNewPDPEnabled &&
        `border-bottom: 1px solid ${props.theme.colors.PRIMARY.LIGHTGRAY};`}
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .product-image-wrapper {
      margin-right: 0;
    }
    .product-detail-image-wrapper {
      margin-bottom: 27px;
    }
    .product-summary-mobile-view {
      display: none;
    }
    .product-summary-desktop-view {
      display: flex;
    }
    .product-price-mobile-view {
      display: none;
    }
    .product-price-desktop-view {
      display: flex;
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      ${(props) =>
        !props.isNewPDPEnabled &&
        `border-bottom: 1px solid ${props.theme.colors.PRIMARY.LIGHTGRAY};`}
    }
    .go-back-container {
      padding-bottom: 36px;
    }
  }
  .loyalty-banner {
    padding-right: 0;
    padding-left: 0;
  }
  .get-candid-section {
    flex: 1;
  }
  .complete-the-look-test {
    .slick-dots {
      .slick-active {
        height: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
        width: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      }
    }
    .slick-dots .slick-active button {
      height: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      width: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      &:before {
        height: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
        width: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      }
    }
  }
  .pdp-recommendations-wrapper {
    position: relative;
    background-color: ${(props) => props.theme.colors.WHITE};
    ${(props) => (props.isNewPDPEnabled ? `overflow-x: hidden;` : '')}
  }
`;

export const recommendationStylesPdp = css`
  .accordion-recommendation {
    text-transform: initial;
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
    text-align: center;
    background: none;
    font-size: 22px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: 18px;
      padding: 12px 0 0 0px;
      margin: 12px 0 0 0px;
    }
    font-weight: bold;
  }

  .recommendations-section-row {
    position: relative;
    top: 14px;
  }

  .accordion-recommendation:before {
    content: '';
    border-top: 1px solid;
    margin: 0 20px 0 0;
    flex: 0.41 0 20px;
  }

  .accordion-recommendation:after {
    content: '';
    border-top: 1px solid;
    margin: 0 0 0 20px;
    flex: 0.41 0 20px;
  }

  .smooth-scroll-list-item {
    margin-right: 5px;
  }

  .container-price p {
    font-size: 18px;
  }
  .container-price span {
    top: -4px;
    position: relative;
  }

  .recommendations-container {
    border-bottom: 7px solid ${(props) => props.theme.colors.ACCORDION.ACTIVE_HEADER};
    padding-bottom: 20px;
    @media ${(props) => props.theme.mediaQuery.large} {
      border-bottom: 1px solid ${(props) => props.theme.colors.ACCORDION.ACTIVE_HEADER};
    }
  }

  .product-image-container {
    height: 237px !important;
    width: 100%;
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) => (props.isNewPDPEnabled ? `height:286px;` : 'height:352px;')}
    }
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      height: 190px !important;
      min-width: 150px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 170px !important;
      min-width: 120px;
    }
  }

  .product-image-container img {
    top: 0px !important;
    left: 0px !important;
    transform: none !important;
  }

  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .smooth-scroll-list-item {
      width: 38%;
    }
  }
`;

export const customSubmitButtonStyle = css`
  .add-to-bag-button {
    max-width: none;
  }
  ${(props) =>
    props.isGiftCard
      ? `.button-wrapper {
    margin-top: ${props.theme.spacing.ELEM_SPACING.XXL};
    margin-bottom: ${props.theme.spacing.ELEM_SPACING.LRG};
  }
  .qty-selector {
    margin-top: 40px;
  }
  @media ${props.theme.mediaQuery.smallOnly} {
    .edit-form-css {
      margin-top: ${
        props.productInfo.isGiftCard && props.isNewPDPEnabled
          ? '0px'
          : props.theme.spacing.ELEM_SPACING.MED
      };
    }
  }
  `
      : ``}
  @media ${(props) => props.theme.mediaQuery.medium} {
    .add-to-bag-button {
      max-width: none;
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .add-to-bag-button {
      max-width: 450px;
    }
  }
`;

export const recommendationStylesPdp1AndPdp2 = css`
  .recommendations-header {
    font-family: Nunito;
    font-size: 14px;
    font-weight: 900;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #1a1a1a;
  }

  .container-price {
    @media ${(props) => props.theme.mediaQuery.medium} {
      min-height: 47px !important;
    }
  }

  .product-wishlist-container + .fulfillment-section {
    margin-top: 23px;
  }

  @media ${(props) => props.theme.mediaQuery.medium} {
    .product-list {
      margin: 0px;
    }
  }
`;
