// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes } from 'prop-types';

const getColorsChipsOptionsMap = (colorFitsSizesMapArg, isShowZeroInventoryEntries) => {
  let colorFitsSizesMap = colorFitsSizesMapArg;
  if (!isShowZeroInventoryEntries) {
    colorFitsSizesMap = colorFitsSizesMap.filter(colorEntry => colorEntry.get('maxAvailable') > 0);
  }
  return colorFitsSizesMap.map(colorEntry => {
    const name = colorEntry.getIn(['color', 'name']);
    const imagePath = colorEntry.getIn(['color', 'imagePath']);
    return (
      <span className="color-title-container" title={name}>
        <img className="color-image" src={imagePath} alt="color" />
      </span>
    );
  });
};

const ProductColorChipsSelector = props => {
  const { options: colorFitsSizesMap } = props;

  return getColorsChipsOptionsMap(colorFitsSizesMap);
};

ProductColorChipsSelector.propTypes = {
  colorFitsSizesMap: PropTypes.shape({}).isRequired,
  isShowZeroInventoryEntries: PropTypes.bool,
  isDisableZeroInventoryEntries: PropTypes.bool,
};

ProductColorChipsSelector.defaultProps = {
  isShowZeroInventoryEntries: false,
  isDisableZeroInventoryEntries: false,
};

export default ProductColorChipsSelector;
