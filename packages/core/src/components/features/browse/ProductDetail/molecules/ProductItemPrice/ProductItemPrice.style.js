// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const productItemPriceStyle = css`
  .per-item-price {
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
  .per-item-text {
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
`;

export default productItemPriceStyle;
