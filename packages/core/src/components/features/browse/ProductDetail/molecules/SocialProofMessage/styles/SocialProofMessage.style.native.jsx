// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

export const SocialProofBodyCopyContainer = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: center;
  background-color: ${(props) =>
    props.bgdColor ? props.bgdColor : props.theme.colors.PRIMARY.COLOR2};
  align-items: center;
`;

export default { SocialProofBodyCopyContainer };
