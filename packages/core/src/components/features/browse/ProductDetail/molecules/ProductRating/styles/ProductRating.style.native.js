// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const CustomRatingContainer = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  display: flex;
  flex-direction: row;
  ${props => (props.inGridPlpRecommendation ? `padding-left: 10px` : ``)}
`;

const RatingStarOverlay = styled.View`
  position: absolute;
  background-color: ${props => props.theme.colorPalette.yellow[600]};
`;

const RatingStarOverlayGray = styled.View`
  background-color: ${props => props.theme.colorPalette.gray[500]};
`;

const RatingImage = styled.Image`
  width: ${props => (props.isPlaceCashReward ? 60 : 76)}px;
  height: ${props => (props.isPlaceCashReward ? 12 : 15)}px;
`;

const ReviewCountText = styled.Text`
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy1}px;
  color: ${props =>
    props.isNewReDesignProductTile
      ? props.theme.colors.PRIMARY.DARK
      : props.theme.colorPalette.gray[600]};
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
  margin-top: ${props =>
    props.isPlaceCashReward ? `-1px` : props.theme.spacing.ELEM_SPACING.XXXS};
`;

export {
  CustomRatingContainer,
  RatingStarOverlay,
  RatingImage,
  RatingStarOverlayGray,
  ReviewCountText,
};
