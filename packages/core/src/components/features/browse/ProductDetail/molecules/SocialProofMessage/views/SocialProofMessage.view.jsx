// 9fbef606107a605d69c0edbcd8029e5d
/** @module SocialProofMessage
 * @summary renders social proof messages.
 *
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import styles from '../styles/SocialProofMessage.style';

export class SocialProofMessage extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string.isRequired,
    socialProofMessage: PropTypes.string.isRequired,
  };

  render() {
    const { className, socialProofMessage } = this.props;
    const fullMessage = socialProofMessage?.priorityMsg;
    let result = '';
    let count = 0;
    if (fullMessage?.includes('$')) {
      fullMessage.split('').forEach((letter) => {
        if (letter === '$' && count === 0) {
          result += '<span class="toBeBold">';
          count = 1;
        } else if (letter === '$' && count === 1) {
          result += '</span>';
          count = 0;
        } else {
          result += letter;
        }
      });
    } else {
      result = fullMessage;
    }
    return (
      <div className={className}>
        <div className="social-proof-wrapper">
          <BodyCopy
            className="social-proof-text"
            fontSize="fs14"
            color="black"
            fontFamily="secondary"
            dangerouslySetInnerHTML={{ __html: `${socialProofMessage ? result : ''}` }}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(SocialProofMessage, styles);

export { SocialProofMessage as SocialProofMessageVanilla };
