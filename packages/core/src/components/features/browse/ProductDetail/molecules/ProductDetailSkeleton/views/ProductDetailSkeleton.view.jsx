// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { fromJS } from 'immutable';
import FixedBreadCrumbs from '@tcp/core/src/components/features/browse/ProductListing/molecules/FixedBreadCrumbs/views';
import ProductImagesWrapper from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductImagesWrapper/views/ProductImagesWrapper.view';
import ProductColorChipsSelector from '@tcp/core/src/components/common/molecules/ProductColorChipSelector';
import { parseBoolean } from '@tcp/core/src/utils';
import {
  getMapSliceForColorProductId,
  getImagesToDisplay,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import ProductPrice from '../../ProductPrice/ProductPrice';
import withStyles from '../../../../../../common/hoc/withStyles';
import LoaderSkelton from '../../../../../../common/molecules/LoaderSkelton';
import { Row, BodyCopy, Col } from '../../../../../../common/atoms';
import styles from '../styles/ProductDetailSkeleton.style';

import { BadgeItem } from '../../../../ProductListing/molecules/ProductList/views/ProductItemComponents';

const colorFitSizeDisplayNames = {
  color: 'Color',
  fit: 'Fit',
  size: 'Size',
  size_alt: 'Size',
};

const ProductDetailsSkeletonStructure = (className) => {
  const thumbnailList = [];
  for (let itr = 0; itr < 5; itr += 1) {
    thumbnailList.push(
      <div className="thumbnail-list">
        <LoaderSkelton />
      </div>
    );
  }
  return (
    <div className={className}>
      <Row fullBleed className="product-detail-skeleton-wrapper">
        <div className="thumbnail-list-wrapper">{thumbnailList}</div>
        <div className="product-skeleton-img" />
        <div className="product-overview-wrapper">
          {[
            'product-title',
            'product-price',
            'product-color',
            'product-size',
            'product-add-to-bag',
          ].map((classValue) => (
            <div className={classValue}>
              <LoaderSkelton />
            </div>
          ))}
        </div>
      </Row>
    </div>
  );
};

const renderColorSkeleton = (
  currentColorEntry,
  isGiftCard,
  colorFitsSizesMapList,
  imagesByColor,
  primaryBrand,
  alternateBrand,
  isNewPDPEnabled
) => {
  let colorChip;
  if (currentColorEntry && currentColorEntry.color) {
    colorChip = (
      <ProductColorChipsSelector
        isGiftCard={isGiftCard}
        colorFitsSizesMap={colorFitsSizesMapList}
        title={`${colorFitSizeDisplayNames.color}:`}
        imagesByColor={imagesByColor}
        input={{ value: currentColorEntry.color }}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
        isNewPDPEnabled={isNewPDPEnabled}
        isPDP
        isABTestLoaded={isNewPDPEnabled}
      />
    );
  } else {
    colorChip = <LoaderSkelton />;
  }

  return colorChip;
};

const renderPriceSkeleton = ({
  priceRange,
  listPrice,
  offerPrice,
  currency,
  currencyAttributes,
  isGiftCard,
  badgeText,
  isShowPriceRangeKillSwitch,
  showPriceRangeForABTest,
  isNewPDPEnabled,
}) => {
  let priceSection;
  const showPriceRange =
    parseBoolean(isShowPriceRangeKillSwitch) && showPriceRangeForABTest && priceRange;
  if (listPrice) {
    priceSection = (
      <ProductPrice
        listPrice={listPrice}
        offerPrice={offerPrice}
        hideItemProp // prop is used to hide unwanted duplicate itemProp as we have duplicate ProductPrice component for web and mobile on PDP pages
        currencySymbol={currency}
        currencyAttributes={currencyAttributes}
        isGiftCard={isGiftCard}
        priceRange={priceRange}
        badge2={badgeText}
        highOfferPrice={showPriceRange && priceRange.highOfferPrice}
        highListPrice={showPriceRange && priceRange.highListPrice}
        isNewPDPEnabled={isNewPDPEnabled}
      />
    );
  } else {
    priceSection = <LoaderSkelton />;
  }
  return priceSection;
};

const getSingleImageObj = (uniqueId) => {
  const imagePath = `${uniqueId.split('_')[0]}/${uniqueId}`;
  return [
    {
      isShortImage: false,
      isOnModalImage: false,
      iconSizeImageUrl: imagePath,
      listingSizeImageUrl: imagePath,
      regularSizeImageUrl: imagePath,
      bigSizeImageUrl: imagePath,
      superSizeImageUrl: imagePath,
    },
  ];
};

const ProductDetailSkeleton = ({
  className,
  retrievedProductInfo,
  breadCrumbs,
  ratingsProductId,
  pdpLabels,
  outOfStockLabels,
  accessibilityLabels,
  isKeepAliveEnabled,
  currencyAttributes,
  currency,
  imgConfigVal,
  isShowPriceRangeKillSwitch,
  showPriceRangeForABTest,
  isDynamicBadgeEnabled,
  fromPLPPage,
  isPDPSmoothScrollEnabled,
  primaryBrand,
  alternateBrand,
  isNewPDPEnabled,
  // eslint-disable-next-line sonarjs/cognitive-complexity
}) => {
  if (!retrievedProductInfo) {
    return ProductDetailsSkeletonStructure(className);
  }
  const {
    name,
    colorFitsSizesMap,
    uniqueId,
    imagesByColor,
    listPrice,
    offerPrice,
    priceRange,
    isGiftCard,
    tcpStyleQty,
    tcpStyleType,
  } = retrievedProductInfo;

  const currentColorEntry = getMapSliceForColorProductId(colorFitsSizesMap, uniqueId) || {};
  const keepAlive =
    isKeepAliveEnabled && currentColorEntry.miscInfo && currentColorEntry.miscInfo.keepAlive;
  const currentProduct = { colorFitsSizesMap };
  let imagesToDisplay = getImagesToDisplay({
    imagesByColor,
    curentColorEntry: currentColorEntry,
    isAbTestActive: false,
    isFullSet: true,
    excludeShortImage: false,
  });
  if ((Array.isArray(imagesToDisplay) && imagesToDisplay.length === 0) || !imagesToDisplay) {
    imagesToDisplay = getSingleImageObj(uniqueId);
  }
  const colorFitsSizesMapList = fromJS(colorFitsSizesMap);

  const { miscInfo: { badge1 = {}, badge3 = '' } = {} } = currentColorEntry;

  const topBadge = badge1.defaultBadge;
  const badgeText = listPrice - offerPrice !== 0 ? badge3 : '';

  return (
    <div className={className}>
      {!isNewPDPEnabled && (
        <Row fullBleed>
          <Col className="product-breadcrumb-wrapper" colSize={{ small: 6, medium: 8, large: 12 }}>
            {breadCrumbs && breadCrumbs.length ? (
              <FixedBreadCrumbs crumbs={breadCrumbs} separationChar=">" isPDPPage />
            ) : (
              <LoaderSkelton height="20px" />
            )}
            {!isNewPDPEnabled && (
              <div className="hide-on-desktop  hide-on-tablet">
                <BadgeItem
                  customFontWeight="regular"
                  customFontSize={['fs10', 'fs10', 'fs10']}
                  className="inline-badge-item"
                  isShowBadges
                  text={topBadge}
                />
              </div>
            )}
          </Col>
        </Row>
      )}
      <Row fullBleed className="product-detail-skeleton-wrapper">
        <Col className="product-image-wrapper" colSize={{ small: 6, medium: 4, large: 7 }}>
          {!isNewPDPEnabled && (
            <BodyCopy
              className="product-title hide-on-desktop hide-on-tablet"
              fontSize="fs18"
              component="h1"
              fontFamily="secondary"
              fontWeight="extrabold"
              itemprop="name"
            >
              {name}
            </BodyCopy>
          )}
          <ProductImagesWrapper
            productName={name}
            isThumbnailListVisible
            images={imagesToDisplay}
            pdpLabels={pdpLabels}
            isZoomEnabled
            currentProduct={currentProduct}
            currentColorEntry={currentColorEntry}
            initialValues={null}
            keepAlive={keepAlive}
            outOfStockLabels={outOfStockLabels}
            accessibilityLabels={accessibilityLabels}
            ratingsProductId={ratingsProductId}
            fromSkeleton
            imgConfigVal={imgConfigVal}
            TCPStyleQTY={tcpStyleQty}
            TCPStyleType={tcpStyleType}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            fromPLPPage={fromPLPPage}
            isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
            isNewPDPEnabled={isNewPDPEnabled}
            pageName="PDP"
            isPDP
          />
        </Col>
        <div className="product-overview-wrapper">
          {!isNewPDPEnabled && (
            <div className="product-color">
              {renderColorSkeleton(
                currentColorEntry,
                isGiftCard,
                colorFitsSizesMapList,
                imagesByColor,
                primaryBrand,
                alternateBrand,
                isNewPDPEnabled
              )}
            </div>
          )}
          <div className={`product-title${isNewPDPEnabled ? '' : ' hide-on-mobile'}`}>
            <BadgeItem
              customFontWeight="regular"
              customFontSize={['fs10', 'fs10', 'fs10']}
              className="inline-badge-item"
              isShowBadges
              text={topBadge}
            />
            <BodyCopy
              className={`product-title${isNewPDPEnabled ? '' : ' hide-on-mobile'}`}
              fontSize={isNewPDPEnabled ? 'fs22' : 'fs18'}
              component="h1"
              fontFamily="secondary"
              fontWeight="extrabold"
              itemprop="name"
            >
              {name}
            </BodyCopy>
          </div>
          <div className="product-price">
            {renderPriceSkeleton({
              priceRange,
              listPrice,
              offerPrice,
              currency,
              currencyAttributes,
              isGiftCard,
              badgeText,
              isShowPriceRangeKillSwitch,
              showPriceRangeForABTest,
              isNewPDPEnabled,
            })}
          </div>
          {!isNewPDPEnabled && (
            <div className="product-color">
              {renderColorSkeleton(
                currentColorEntry,
                isGiftCard,
                colorFitsSizesMapList,
                imagesByColor,
                primaryBrand,
                alternateBrand,
                isNewPDPEnabled
              )}
            </div>
          )}
          <div className="product-size">
            <LoaderSkelton />
          </div>
          <div className="product-add-to-bag">
            <LoaderSkelton />
          </div>
        </div>
      </Row>
      <div className="product-discription">
        <LoaderSkelton height="120px" />
      </div>
    </div>
  );
};

ProductDetailSkeleton.propTypes = {
  className: PropTypes.string.isRequired,
  retrievedProductInfo: PropTypes.shape({}),
  breadCrumbs: PropTypes.shape({}),
  ratingsProductId: PropTypes.string.isRequired,
  pdpLabels: PropTypes.shape({}).isRequired,
  outOfStockLabels: PropTypes.shape({}).isRequired,
  accessibilityLabels: PropTypes.shape({}).isRequired,
  isKeepAliveEnabled: PropTypes.shape({}),
  currencyAttributes: PropTypes.shape({}).isRequired,
  currency: PropTypes.string.isRequired,
  imgConfigVal: PropTypes.shape([]),
  isShowPriceRangeKillSwitch: PropTypes.bool.isRequired,
  showPriceRangeForABTest: PropTypes.bool.isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  fromPLPPage: PropTypes.bool,
  isPDPSmoothScrollEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
};

ProductDetailSkeleton.defaultProps = {
  retrievedProductInfo: {},
  breadCrumbs: null,
  isKeepAliveEnabled: false,
  imgConfigVal: [],
  isDynamicBadgeEnabled: false,
  fromPLPPage: false,
  isPDPSmoothScrollEnabled: false,
  isNewPDPEnabled: false,
};

export default withStyles(ProductDetailSkeleton, styles);
export { ProductDetailSkeleton as ProductDetailSkeletonVanilla };
