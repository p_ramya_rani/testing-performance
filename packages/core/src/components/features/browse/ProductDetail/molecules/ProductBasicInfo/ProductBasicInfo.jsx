// 9fbef606107a605d69c0edbcd8029e5d
/** @module ProductBasicInfo
 * @summary Show the product's name, rating and wishlist CTA.
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { isClient, getViewportInfo, getBrand } from '@tcp/core/src/utils';
import ProductRating from '../ProductRating/ProductRating';
import { Anchor, BodyCopy } from '../../../../../common/atoms';
import withStyles from '../../../../../common/hoc/withStyles';
import ProductBasicInfoStyle from './ProductBasicInfo.style';

import {
  BadgeItem,
  WishListIcon,
} from '../../../ProductListing/molecules/ProductList/views/ProductItemComponents';

class ProductBasicInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clickedProdutId: '',
      isLoggedIn: props.isLoggedIn,
    };
  }

  static getDerivedStateFromProps(props, state) {
    const {
      isLoggedIn,
      onAddItemToFavorites,
      productInfo: { productId },
      productMiscInfo: { colorProductId, colorDisplayId },
      pageName,
      skuId,
      formName,
    } = props;
    const selectedColorId = colorDisplayId || productId;

    const { clickedProdutId, isLoggedIn: wasLoggedIn } = state;
    if (isLoggedIn && isLoggedIn !== wasLoggedIn && clickedProdutId === colorDisplayId) {
      onAddItemToFavorites({
        colorProductId: selectedColorId,
        productSkuId: (skuId && skuId.skuId) || null,
        pdpColorProductId: colorProductId,
        formName: formName || PRODUCT_ADD_TO_BAG,
        page: pageName || 'PDP',
      });
      return { clickedProdutId: '', isLoggedIn };
    }
    return null;
  }

  componentDidUpdate() {
    const { countUpdated, resetCountUpdate } = this.props;
    if (countUpdated === true) resetCountUpdate();
  }

  componentWillUnmount() {
    const { removeAddToFavoritesErrorMsg, countUpdated, resetCountUpdate } = this.props;
    if (typeof removeAddToFavoritesErrorMsg === 'function') {
      removeAddToFavoritesErrorMsg('');
    }

    if (countUpdated === true) resetCountUpdate();
  }

  title = () => {
    const {
      productInfo,
      className,
      isOutfitPage,
      reviewOnTop,
      bundleProductName,
      isStyleWith,
      colorProductName,
      isNewPDPEnabled,
      formValues,
      renderProductRatings,
      isNewQVEnabled,
      socialProofMessage,
      productMiscInfo,
      isGiftCard,
    } = this.props;
    const selectedColour = formValues?.color || colorProductName;
    const name = !bundleProductName ? productInfo.name : bundleProductName;
    const isMobile = isClient() ? getViewportInfo().isMobile : null;
    const headingType = reviewOnTop ? 'h1' : 'h2';
    const fontSize = isNewPDPEnabled ? 'fs22' : 'fs18';
    const colorImageNameSuffix = productMiscInfo.imageName?.toLowerCase();
    let colorStr = String(selectedColour).toLowerCase();
    colorStr = colorStr.replace(`-${colorImageNameSuffix}`, '');
    if (isStyleWith) {
      return null;
    }
    return (
      <BodyCopy
        className={`product-title ${className} ${isNewPDPEnabled ? 'new-pdp-colour' : ''}`}
        fontSize={isOutfitPage ? ['fs16', 'fs18'] : fontSize}
        component={isOutfitPage ? 'p' : headingType}
        fontFamily="secondary"
        fontWeight="extrabold"
        itemprop="name"
      >
        {isNewPDPEnabled && colorStr ? `${name} - ${colorStr}` : name}
        {isNewPDPEnabled && !isMobile ? (
          <div className="rating-section-bottom">
            {renderProductRatings(
              productInfo,
              isNewQVEnabled,
              isNewPDPEnabled,
              socialProofMessage,
              isGiftCard
            )}
          </div>
        ) : null}
      </BodyCopy>
    );
  };

  handleAddToWishlist = () => {
    const {
      onAddItemToFavorites,
      productInfo: { productId },
      productMiscInfo: { colorProductId, colorDisplayId },
      pageName,
      skuId,
      formName,
      checkForOOSForVariant,
      deleteFavItemInProgressFlag,
    } = this.props;
    if (checkForOOSForVariant || deleteFavItemInProgressFlag) {
      return;
    }
    const selectedColorId = colorDisplayId || productId;
    onAddItemToFavorites({
      colorProductId: selectedColorId,
      productSkuId: (skuId && skuId.skuId) || null,
      pdpColorProductId: colorProductId,
      formName: formName || PRODUCT_ADD_TO_BAG,
      page: pageName || 'PDP',
    });
    this.setState({
      clickedProdutId: selectedColorId,
    });
  };

  handleRemoveFromWishList = () => {
    const {
      onSetLastDeletedItemIdAction,
      productInfo: { productId },
      productMiscInfo: { colorDisplayId },
      checkForOOSForVariant,
      wishList,
      deleteFavItemInProgressFlag,
    } = this.props;
    if (checkForOOSForVariant || deleteFavItemInProgressFlag) {
      return;
    }
    const selectedWishList = wishList && wishList[colorDisplayId || productId];
    if (selectedWishList) {
      const { externalIdentifier, giftListItemID } = selectedWishList;
      onSetLastDeletedItemIdAction({
        externalId: externalIdentifier,
        itemId: giftListItemID,
        notFromFavoritePage: true,
        productId: colorDisplayId || productId,
      });
    }
  };

  // eslint-disable-next-line
  render() {
    const {
      asPath,
      pdpUrl,
      badge,
      isGiftCard,
      className,
      reviewOnTop,
      isLoggedIn,
      productInfo: { ratingsProductId, ratings, productId, reviewsCount },
      keepAlive,
      outOfStockLabels,
      productMiscInfo: { colorDisplayId, isFavorite, miscInfo, favoritedCount },
      isInWishList,
      hideRating,
      isStyleWith,
      deleteFavItemInProgressFlag,
      isOutfitPage,
      errorMessages,
      pageName,
      socialProofMessage,
      isNewPDPEnabled,
      pdpLabels,
      isShowBrandNameEnabled,
      primaryBrand,
      alternateBrand,
      isFbt,
    } = this.props;
    const title = this.title();
    const isFavoriteView = false;
    const isFavorited = isFavorite || (miscInfo && miscInfo.isInDefaultWishlist) || isInWishList;
    const outFitFavt = isOutfitPage ? isFavorited : isInWishList;
    const handleAddRemoveFromWishlistObj = {
      handleAddToWishlist: this.handleAddToWishlist,
      handleRemoveFromWishList: this.handleRemoveFromWishList,
      deleteFavItemInProgressFlag,
    };
    const currentColorId = colorDisplayId || productId;
    const prodBrand = getBrand() && getBrand().toUpperCase();
    const { isMobile } = isClient() && getViewportInfo();
    return (
      <div className={`product-details-header-container ${className}`}>
        <div className="sub-header-wrapper">
          <div>
            {!isStyleWith && !isFbt && (
              <BadgeItem
                customFontWeight="regular"
                customFontSize={['fs10', 'fs10', 'fs10']}
                className="inline-badge-item"
                text={badge}
              />
            )}
            {isShowBrandNameEnabled ? (
              <BodyCopy
                fontSize="fs18"
                fontWeight="bold"
                fontFamily="secondary"
                className="product-brand"
              >
                {(primaryBrand || alternateBrand || prodBrand) === 'GYM'
                  ? pdpLabels?.gymLabel
                  : pdpLabels?.tcpLabel}
              </BodyCopy>
            ) : null}
          </div>
          {isMobile && isNewPDPEnabled && !isGiftCard && (
            <div
              className="write-review-link"
              data-bv-show="rating_summary"
              data-bv-product-id={ratingsProductId}
              data-bv-seo="false"
            />
          )}
        </div>

        {keepAlive && (
          <BodyCopy color="red.500" fontSize="fs10" fontFamily="secondary">
            {outOfStockLabels.itemSoldOutMessage}
          </BodyCopy>
        )}

        {errorMessages &&
          errorMessages[currentColorId] &&
          errorMessages[currentColorId].itemId === currentColorId && (
            <Notification
              status="error"
              colSize={{ large: 12, medium: 8, small: 6 }}
              message={errorMessages[currentColorId].errorMessage}
            />
          )}
        <div className="information-container">
          <div className="title-wrapper">
            {typeof pdpUrl === 'string' ? (
              <Anchor to={pdpUrl} asPath={asPath} className="product-link-title">
                {title}
              </Anchor>
            ) : (
              title
            )}

            {!isGiftCard && !isStyleWith && !isFbt && !isNewPDPEnabled && (
              <div className="wishlist-container">
                {WishListIcon(
                  isFavoriteView,
                  outFitFavt,
                  handleAddRemoveFromWishlistObj,
                  false, // itemNotAvailable
                  favoritedCount
                )}
              </div>
            )}
          </div>
          {/* TODO - fix it with bundle product requirement */}
          {/* {!isBundleProduct && !isGiftCard && isRatingsVisible && <ProductRating ratingsProductId={ratingsProductId} /> } */}
          {!hideRating && !isGiftCard && !isStyleWith && !isFbt && !isNewPDPEnabled ? (
            <ProductRating
              ratingsProductId={ratingsProductId}
              ratings={ratings || 0}
              reviewOnTop={reviewOnTop}
              isLoggedIn={isLoggedIn}
              reviews={reviewsCount}
              pageName={pageName}
              socialProofMessage={socialProofMessage}
            />
          ) : null}
        </div>
        {/* TODO - the favourite functionality needs to be implemented here */}
        {/* {!isBundleProduct && !isGiftCard && (
                <FavoriteButtonContainer isActiveHoverMessage uniqueId={uniqueId} favoritedCount={colorSlice.favoritedCount}
                  generalProductId={generalProductId} colorProductId={colorSlice.colorProductId} skuId={skuId} quantity={quantity}
                  isInDefaultWishlist={isInDefaultWishlist} isShowFavoriteCount={isShowFavoriteCount} />
              )} */}
        {/* TODO - fix it with bundle product requirement */}
        {/* {!isBundleProduct && !isMobile && keepAlive && <div className="oos-message">OUT OF STOCK</div>} */}
      </div>
    );
  }
}

ProductBasicInfo.propTypes = {
  className: PropTypes.string,
  productInfo: PropTypes.shape({}).isRequired,
  asPath: PropTypes.string,
  pdpUrl: PropTypes.string,
  badge: PropTypes.string,
  isGiftCard: PropTypes.bool.isRequired,
  onAddItemToFavorites: PropTypes.func.isRequired,
  keepAlive: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({
    itemSoldOutMessage: PropTypes.string,
  }),
  productMiscInfo: PropTypes.shape({
    isInDefaultWishlist: PropTypes.bool,
  }),
  removeAddToFavoritesErrorMsg: PropTypes.func,
  pageName: PropTypes.string,
  skuId: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  formName: PropTypes.string,
  isOutfitPage: PropTypes.bool,
  reviewOnTop: PropTypes.bool,
  isInWishList: PropTypes.bool.isRequired,
  hideRating: PropTypes.bool,
  bundleProductName: PropTypes.string,
  countUpdated: PropTypes.bool,
  resetCountUpdate: PropTypes.func.isRequired,
  socialProofMessage: PropTypes.string,
  isStyleWith: PropTypes.bool,
  checkForOOSForVariant: PropTypes.bool.isRequired,
  onSetLastDeletedItemIdAction: PropTypes.func,
  wishList: PropTypes.shape({}),
  deleteFavItemInProgressFlag: PropTypes.bool,
  errorMessages: PropTypes.shape({}),
  isNewPDPEnabled: PropTypes.bool,
  colorProductName: PropTypes.string,
  pdpLabels: PropTypes.shape({}),
  isShowBrandNameEnabled: PropTypes.bool,
  renderProductRatings: PropTypes.func,
  isNewQVEnabled: PropTypes.bool,
  primaryBrand: PropTypes.string,
  formValues: PropTypes.shape({}),
};

ProductBasicInfo.defaultProps = {
  className: '',
  asPath: null,
  pdpUrl: null,
  badge: '',
  outOfStockLabels: {
    itemSoldOutMessage: '',
  },
  keepAlive: false,
  productMiscInfo: {
    isInDefaultWishlist: false,
  },
  pageName: '',
  skuId: '',
  removeAddToFavoritesErrorMsg: () => {},
  isLoggedIn: false,
  formName: PRODUCT_ADD_TO_BAG,
  isOutfitPage: false,
  reviewOnTop: false,
  hideRating: false,
  bundleProductName: '',
  countUpdated: false,
  isStyleWith: false,
  onSetLastDeletedItemIdAction: () => {},
  socialProofMessage: '',
  wishList: {},
  deleteFavItemInProgressFlag: false,
  errorMessages: {},
  isNewPDPEnabled: false,
  colorProductName: '',
  pdpLabels: {},
  isShowBrandNameEnabled: false,
  renderProductRatings: () => {},
  isNewQVEnabled: false,
  primaryBrand: '',
  formValues: {},
};

export default errorBoundary(withStyles(ProductBasicInfo, ProductBasicInfoStyle));
