// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { getDynamicBadgeQty } from '@tcp/core/src/utils/utils';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import ImageCarousel from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ImageCarousel';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import {
  getMapSliceForColorProductId,
  getImagesToDisplay,
} from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductList/utils/productsCommonUtils';
import ProductSummary from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductSummary';
import {
  ProductDetailWrapper,
  ProductDetailNewWrapper,
} from '../styles/ProductDetailSkeleton.style.native';
import config from '../../../../../../common/molecules/ProductDetailImage/config';

const ProductDetailsSkeletonStructure = () => {
  return (
    <ProductDetailWrapper>
      {[
        {
          width: '100%',
          height: '400px',
        },
        {
          width: '100%',
          height: '400px',
        },
        {
          width: '100%',
          height: '400px',
        },
        {
          width: '100%',
          height: '400px',
        },
        {
          width: '100%',
          height: '400px',
        },
        {
          width: '100%',
          height: '400px',
        },
        {
          width: '100%',
          height: '400px',
        },
      ].map(({ width, height }) => (
        <ViewWithSpacing spacingStyles="margin-bottom-MED">
          <LoaderSkelton width={width} height={height} />
        </ViewWithSpacing>
      ))}
    </ProductDetailWrapper>
  );
};

const getDynamicBadgeQuantity = (isDynamicBadgeEnabled, tcpStyleType, tcpStyleQty) => {
  return getDynamicBadgeQty(tcpStyleType, tcpStyleQty, isDynamicBadgeEnabled);
};

const getWrapperComponent = (isNewReDesignForNewPDPSkeleton) => {
  return isNewReDesignForNewPDPSkeleton ? ProductDetailNewWrapper : ProductDetailWrapper;
};

const ProductDetailSkeleton = ({
  retrievedProductInfo,
  outOfStockLabels,
  isKeepAliveEnabled,
  currency,
  currencyExchange,
  accessibilityLabels,
  pdpLabels,
  isPlcc,
  selectedColorProductId,
  showPriceRange,
  isDynamicBadgeEnabled,
  isNewReDesignForNewPDPSkeleton,
}) => {
  if (!retrievedProductInfo) {
    return ProductDetailsSkeletonStructure();
  }

  const { colorFitsSizesMap, uniqueId, imagesByColor, priceRange, tcpStyleQty, tcpStyleType } =
    retrievedProductInfo;
  const currentProductPrices = priceRange;
  if (showPriceRange) {
    currentProductPrices.offerPrice = currentProductPrices.lowOfferPrice;
    currentProductPrices.listPrice = currentProductPrices.lowListPrice;
  }
  const currentColorEntry = getMapSliceForColorProductId(colorFitsSizesMap, uniqueId) || {};
  const keepAlive =
    isKeepAliveEnabled && currentColorEntry.miscInfo && currentColorEntry.miscInfo.keepAlive;
  const currentProduct = {
    colorFitsSizesMap,
    productId: uniqueId,
    ...currentProductPrices,
    ...retrievedProductInfo,
  };
  let imageUrls = [];
  if (imagesByColor) {
    imageUrls = getImagesToDisplay({
      imagesByColor,
      curentColorEntry: currentColorEntry,
      isAbTestActive: false,
      isFullSet: true,
      excludeShortImage: true,
    });
  }
  const WrapperComponent = getWrapperComponent(isNewReDesignForNewPDPSkeleton);
  return (
    <WrapperComponent>
      <View marginBottom={10}>
        <ImageCarousel
          isSBP={retrievedProductInfo && retrievedProductInfo.isSBP}
          imageUrls={imageUrls}
          currentProduct={currentProduct}
          currentColorEntry={currentColorEntry}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          skuId={null}
          formName={PRODUCT_ADD_TO_BAG}
          imgConfig={config.IMG_DATA_CLIENT.imgConfig[0]}
          isGiftCard={currentProduct.isGiftCard}
          dynamicBadgeOverlayQty={getDynamicBadgeQuantity(
            isDynamicBadgeEnabled,
            tcpStyleType,
            tcpStyleQty
          )}
          tcpStyleType={tcpStyleType}
          isNewReDesignEnabled={isNewReDesignForNewPDPSkeleton}
        />
      </View>
      {!currentProduct.isGiftCard && <LoaderSkelton width="100%" height="42px" />}
      <ViewWithSpacing spacingStyles="margin-bottom-MED">
        <ProductSummary
          productData={currentProduct}
          selectedColorProductId={selectedColorProductId}
          {...currentProductPrices}
          offerPrice={
            currentProduct.isGiftCard
              ? parseInt(currentProduct.offerPrice, 10)
              : currentProductPrices.offerPrice
          }
          listPrice={
            currentProduct.isGiftCard
              ? parseInt(currentProduct.offerPrice, 10)
              : currentProductPrices.listPrice
          }
          currencySymbol={currency}
          currencyExchange={currencyExchange}
          isGiftCard={currentProduct.isGiftCard}
          accessibilityLabels={accessibilityLabels}
          pdpLabels={pdpLabels}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          renderRatingReview
          isPlcc={isPlcc}
          showPriceRange={showPriceRange}
          fromSkeleton
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          isNewReDesignProductSummary={isNewReDesignForNewPDPSkeleton}
        />
      </ViewWithSpacing>
      <ViewWithSpacing spacingStyles="margin-bottom-MED">
        <LoaderSkelton width="30%" height="20px" />
      </ViewWithSpacing>
      <ViewWithSpacing spacingStyles="margin-bottom-MED">
        <LoaderSkelton width="100%" height="40px" />
      </ViewWithSpacing>
      <ViewWithSpacing spacingStyles="margin-bottom-MED">
        <LoaderSkelton width="100%" height="40px" />
      </ViewWithSpacing>
      <ViewWithSpacing spacingStyles="margin-bottom-MED">
        <LoaderSkelton width="100%" height="20px" />
      </ViewWithSpacing>
    </WrapperComponent>
  );
};

ProductDetailSkeleton.propTypes = {
  retrievedProductInfo: PropTypes.shape({}),
  outOfStockLabels: PropTypes.shape({}).isRequired,
  accessibilityLabels: PropTypes.shape({}).isRequired,
  isKeepAliveEnabled: PropTypes.bool,
  currencyExchange: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  pdpLabels: PropTypes.shape({}).isRequired,
  isPlcc: PropTypes.bool,
  selectedColorProductId: PropTypes.number.isRequired,
  showPriceRange: PropTypes.bool,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  isNewReDesignForNewPDPSkeleton: PropTypes.bool,
};

ProductDetailSkeleton.defaultProps = {
  retrievedProductInfo: {},
  isKeepAliveEnabled: false,
  isPlcc: false,
  showPriceRange: false,
  isDynamicBadgeEnabled: {},
  isNewReDesignForNewPDPSkeleton: false,
};

export default ProductDetailSkeleton;
