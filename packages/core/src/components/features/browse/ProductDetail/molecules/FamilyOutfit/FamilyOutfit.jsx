import React from 'react';
import { PropTypes } from 'prop-types';
import { Carousel } from '@tcp/core/src/components/common/molecules';
import { getIconPath, getAPIConfig, isClient, getViewportInfo } from '@tcp/core/src/utils';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { breakpoints } from '@tcp/core/styles/themes/TCP/mediaQuery';
import { Anchor } from '../../../../../common/atoms';

const RenderImg = (imgUrl, finalImageSrc, imageId) => {
  return imgUrl && <img className="family-outfit-carousel-img" src={finalImageSrc} alt={imageId} />;
};

class FamilyOutfit extends React.PureComponent {
  render() {
    const isMobile = isClient() ? getViewportInfo().isMobile : false;
    const { alternateSizesImageNameAndPath, alternateBrand } = this.props;
    const alternateSizesImageNameAndPathLength =
      alternateSizesImageNameAndPath && alternateSizesImageNameAndPath.length;
    const slidesToShowPeek = isMobile ? 3.5 : 4;
    const slidesToShow =
      alternateSizesImageNameAndPathLength < 4
        ? alternateSizesImageNameAndPathLength
        : slidesToShowPeek;
    const familyOutfitClass =
      alternateSizesImageNameAndPathLength === 2 ? 'family-outfit-two-item' : '';
    const familyOutfitFourItemClass =
      alternateSizesImageNameAndPathLength >= 4 ? 'family-outfit-four-item' : '';
    const apiConfig = getAPIConfig();
    const { brandId } = apiConfig;
    const brand = alternateBrand || brandId;
    const assetPrefix = apiConfig[`productAssetPath${brand.toUpperCase()}`];
    const assetHost = apiConfig[`assetHost${brand.toUpperCase()}`];
    let imgConfig = 't_pdp_img_m,f_auto,q_auto';
    const extension = '.jpg';
    let imgUrl;
    return (
      <div
        className={`carousel-container family-outfit-carousel-block ${familyOutfitClass} ${familyOutfitFourItemClass}`}
      >
        <Carousel
          options={{
            autoplay: false,
            arrows: true,
            fade: false,
            swipeToSlide: true,
            dots: true,
            dotsClass: 'slick-dots',
            swipe: true,
            slide: true,
            slidesToShow,
            infinite: false,
            slidesToScroll: 1,
            responsive: [
              {
                breakpoint: breakpoints.large,
                settings: {
                  slidesToShow,
                  dots: false,
                },
              },
              {
                breakpoint: breakpoints.medium,
                settings: {
                  slidesToShow,
                  arrows: false,
                },
              },
              {
                breakpoint: breakpoints.small,
                settings: {
                  slidesToShow,
                  arrows: false,
                },
              },
            ],
          }}
          carouselConfig={{
            autoplay: false,
            customArrowLeft: getIconPath('family-outfit-arrow'),
            customArrowRight: getIconPath('family-outfit-arrow'),
          }}
        >
          {alternateSizesImageNameAndPath.map((item, index) => {
            const [imageName, imageLargePath] = item;
            const imageNamewithoutSizesText = imageName && imageName.replace('Sizes', '');

            const imageNameSplitArray = imageLargePath && imageLargePath.split('-');
            const imageNameSplitArrayLength = imageNameSplitArray && imageNameSplitArray.length;
            const imageId = imageNameSplitArray[imageNameSplitArrayLength - 2];
            const imageSuffix = imageNameSplitArray[imageNameSplitArrayLength - 1];

            imgUrl = `${imageId}/${imageId}_${imageSuffix}`;
            imgConfig = 't_pdp_imgswatch,f_auto,q_auto';
            const finalImageSrc = `${assetHost}/${imgConfig}/${assetPrefix}/${imgUrl}${extension}`;
            const newImageLargePath = alternateBrand
              ? `${imageLargePath}$brand=${alternateBrand}`
              : `${imageLargePath}`;

            return (
              <div>
                <Anchor
                  className="image-link"
                  to={`/p?pid=${newImageLargePath}`}
                  asPath={`/p/${newImageLargePath}`}
                >
                  {RenderImg(imgUrl, finalImageSrc, imageId)}
                  <span className="family-carousel-item-name">{imageNamewithoutSizesText}</span>
                </Anchor>

                {alternateSizesImageNameAndPathLength > 1 &&
                index < alternateSizesImageNameAndPathLength - 1 ? (
                  <div className="family-outfit-carousel-plus-icon">+</div>
                ) : null}
              </div>
            );
          })}
        </Carousel>
      </div>
    );
  }
}

FamilyOutfit.propTypes = {
  alternateSizesImageNameAndPath: PropTypes.shape([]),
};

FamilyOutfit.defaultProps = {
  alternateSizesImageNameAndPath: [],
};

export default errorBoundary(FamilyOutfit);
