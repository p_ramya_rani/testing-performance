// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Image, FlatList } from 'react-native';
import { PropTypes } from 'prop-types';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { BodyCopy, Anchor } from '../../../../../../common/atoms';
import {
  PageContainer,
  StyleProductDescription,
  StyleLongDescription,
  ImageStyleWrapper,
  ItemStyleWrapper,
  PageContainerNewRedesign,
  PageContainerContent,
  StyleProductDescriptionNew,
  HeaderWrapper,
  PageContainerBundleViewContent,
  BodyContentBundleViewWrapper,
} from '../ProductDescription.native.style';
import AccessibilityRoles from '../../../../../../../constants/AccessibilityRoles.constant';
import {
  getMapSliceForColor,
  getMapColorList,
} from '../../../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { isGymboree } from '../../../../../../../utils';

const downIcon = require('../../../../../../../../../mobileapp/src/assets/images/carrot-small-down.png');
const upIcon = require('../../../../../../../../../mobileapp/src/assets/images/carrot-small-up.png');

const downIconNew = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/icon_down.png');
const upIconNew = require('../../../../../../../../../mobileapp/src/assets/images/isNewRedesign/icon_up.png');
// Key extractor for flat list
const keyExtractor = (_, index) => index.toString();

const renderItem = (item) => {
  return item ? (
    <StyleLongDescription>
      <BodyCopyWithSpacing text={`\u2022`} spacingStyles="padding-right-XXS" />
      <BodyCopyWithSpacing
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs14"
        text={item}
        spacingStyles="padding-bottom-XXS padding-right-XS"
      />
    </StyleLongDescription>
  ) : null;
};

class ProductDetailDescription extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isAccordionOpen: false,
    };
  }

  componentDidMount() {
    const { isFromBundleProductView } = this.props;
    const { isAccordionOpen } = this.state;
    if (isFromBundleProductView) {
      this.setState({ isAccordionOpen: !isAccordionOpen });
    }
  }

  handleAccordionToggle = () => {
    const { isAccordionOpen } = this.state;
    this.setState({ isAccordionOpen: !isAccordionOpen });
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { scrollToAccordionBottom } = this.props;
    const { isAccordionOpen } = this.state;
    const { isAccordionOpen: prevValue } = prevState;
    if (isAccordionOpen && !prevValue) {
      setTimeout(() => {
        /* measure fun calculate the height, width and offset x y value of element  */
        this.accordionView.measure((x, y, width, height, pageX, pageY) => {
          if (scrollToAccordionBottom) {
            scrollToAccordionBottom(x, y, width, height, pageX, pageY);
          }
        });
      }, 0);
    }
  };

  renderLongDescription = (longDescription) => {
    const longDescriptionText = longDescription.replace(/<\/li>/g, '');
    return longDescriptionText.split('<li>');
  };

  getGymStyling = () => {
    return isGymboree()
      ? { fontFamily: 'primary', fontWeight: 'semibold' }
      : { fontFamily: 'secondary', fontWeight: 'bold' };
  };

  renderProductDescription = () => {
    const {
      longDescription,
      shortDescription,
      pdpLabels,
      itemPartNumber,
      margins,
      accessibilityLabels,
      backgroundColor,
      paddings,
      color,
    } = this.props;
    const {
      productInfo: { colorFitsSizesMap, TCPStyleColor },
    } = this.props;
    const { ProductDescription, ClaimMessage, PartNumber } = pdpLabels;
    const { isAccordionOpen } = this.state;
    const colorSlice = getMapSliceForColor(colorFitsSizesMap, color);
    const colorList = getMapColorList(TCPStyleColor, colorSlice);
    const colorDisplayId = colorSlice && colorSlice.colorDisplayId;

    return (
      <PageContainer
        ref={(ref) => {
          this.accordionView = ref;
        }}
        margins={margins}
        paddings={paddings}
        renderToHardwareTextureAndroid
        backgroundColor={backgroundColor}
      >
        <StyleProductDescription
          onPress={this.handleAccordionToggle}
          accessible
          accessibilityRole={AccessibilityRoles.Button}
          accessibilityLabel={ProductDescription}
          accessibilityState={{ expanded: isAccordionOpen }}
        >
          <BodyCopy
            fontFamily="secondary"
            fontWeight="black"
            fontSize="fs14"
            isAccordionOpen={isAccordionOpen}
            text={ProductDescription}
            textAlign="center"
          />
          <ImageStyleWrapper>
            <Anchor onPress={this.handleAccordionToggle}>
              <Image
                source={isAccordionOpen ? upIcon : downIcon}
                alt={
                  isAccordionOpen
                    ? accessibilityLabels.lbl_up_arrow_icon
                    : accessibilityLabels.lbl_down_arrow_icon
                }
              />
            </Anchor>
          </ImageStyleWrapper>
        </StyleProductDescription>

        {isAccordionOpen ? (
          <>
            {shortDescription ? (
              <BodyCopyWithSpacing
                text={shortDescription}
                fontFamily="secondary"
                fontWeight="regular"
                fontSize="fs14"
                spacingStyles="padding-bottom-XXS"
              />
            ) : null}
            {longDescription ? (
              <>
                <FlatList
                  numColumns={1}
                  keyExtractor={keyExtractor}
                  data={this.renderLongDescription(longDescription)}
                  renderItem={({ item }) => renderItem(item)}
                />
                {colorList && (
                  <>
                    <BodyCopyWithSpacing
                      fontFamily="secondary"
                      fontWeight="black"
                      fontSize="fs14"
                      spacingStyles="padding-top-SM padding-right-XS padding-bottom-XS"
                      text={pdpLabels && pdpLabels.lblPdpProductInclude}
                      textAlign="left"
                    />
                    <FlatList
                      numColumns={1}
                      keyExtractor={keyExtractor}
                      data={colorList}
                      renderItem={({ item }) => renderItem(item)}
                    />
                  </>
                )}
              </>
            ) : null}
            <BodyCopyWithSpacing
              text={ClaimMessage}
              spacingStyles="padding-top-LRG"
              fontFamily="secondary"
              fontSize="fs14"
              fontWeight="regular"
            />
            <ItemStyleWrapper>
              <BodyCopy
                text={`${PartNumber} ${colorDisplayId || itemPartNumber}`}
                fontFamily="secondary"
                fontWeight="regular"
                fontSize="fs10"
              />
            </ItemStyleWrapper>
          </>
        ) : null}
      </PageContainer>
    );
  };

  renderProductDescriptionNewRedesign = () => {
    const {
      longDescription,
      shortDescription,
      pdpLabels,
      itemPartNumber,
      margins,
      accessibilityLabels,
      backgroundColor,
      paddings,
      color,
    } = this.props;
    const {
      productInfo: { colorFitsSizesMap, TCPStyleColor },
    } = this.props;
    const { ProductDescriptionNewDesign, ClaimMessage, PartNumber } = pdpLabels;
    const { isAccordionOpen } = this.state;
    const colorSlice = getMapSliceForColor(colorFitsSizesMap, color);
    const colorList = getMapColorList(TCPStyleColor, colorSlice);
    const colorDisplayId = colorSlice && colorSlice.colorDisplayId;
    const imageDownStyle = { width: 18, height: 10 };

    return (
      <PageContainerNewRedesign
        ref={(ref) => {
          this.accordionView = ref;
        }}
        margins={margins}
        paddings={paddings}
        renderToHardwareTextureAndroid
        backgroundColor={backgroundColor}
        isAccordionOpen={isAccordionOpen}
      >
        <PageContainerContent isAccordionOpen={isAccordionOpen}>
          <HeaderWrapper>
            <BodyCopy
              dataLocator="pdp_product_description_label"
              fontFamily={this.getGymStyling().fontFamily}
              fontSize="fs16"
              fontWeight={this.getGymStyling().fontWeight}
              color="gray.900"
              text={ProductDescriptionNewDesign}
              letterSpacing="ls1"
            />
          </HeaderWrapper>
          {shortDescription ? (
            <BodyCopyWithSpacing
              text={shortDescription}
              fontFamily="secondary"
              fontWeight="light"
              color="gray.900"
              fontSize="fs14"
              spacingStyles="padding-bottom-XXS"
              letterSpacing="ls05"
            />
          ) : null}
          {longDescription ? (
            <>
              {this.renderLongDescription(longDescription).map((prop) => {
                return (
                  <BodyCopyWithSpacing
                    fontFamily="secondary"
                    fontWeight="light"
                    color="gray.900"
                    fontSize="fs14"
                    spacingStyles="padding-bottom-XXS"
                    text={prop}
                    textAlign="left"
                    letterSpacing="ls05"
                  />
                );
              })}
              {colorList && (
                <>
                  <BodyCopyWithSpacing
                    fontFamily="secondary"
                    fontWeight="black"
                    fontSize="fs14"
                    spacingStyles="padding-top-SM padding-right-XS padding-bottom-XS"
                    text={pdpLabels && pdpLabels.lblPdpProductInclude}
                    textAlign="left"
                  />
                  <FlatList
                    numColumns={1}
                    keyExtractor={keyExtractor}
                    data={colorList}
                    renderItem={({ item }) => renderItem(item)}
                  />
                </>
              )}
            </>
          ) : null}
          <BodyCopyWithSpacing
            text={ClaimMessage}
            spacingStyles="padding-top-SM"
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="regular"
          />
          <BodyCopyWithSpacing
            text={`${PartNumber} ${colorDisplayId || itemPartNumber}`}
            fontFamily="secondary"
            fontWeight="regular"
            fontSize="fs10"
          />
        </PageContainerContent>
        <StyleProductDescriptionNew
          onPress={this.handleAccordionToggle}
          accessible
          accessibilityRole={AccessibilityRoles.Button}
          accessibilityLabel={ProductDescriptionNewDesign}
          accessibilityState={{ expanded: isAccordionOpen }}
        >
          <ImageStyleWrapper>
            <Anchor onPress={this.handleAccordionToggle}>
              <Image
                source={isAccordionOpen ? upIconNew : downIconNew}
                alt={
                  isAccordionOpen
                    ? accessibilityLabels.lbl_up_arrow_icon
                    : accessibilityLabels.lbl_down_arrow_icon
                }
                style={imageDownStyle}
              />
            </Anchor>
          </ImageStyleWrapper>
        </StyleProductDescriptionNew>
      </PageContainerNewRedesign>
    );
  };

  renderProductDescriptionForBundleProductView = () => {
    const {
      longDescription,
      shortDescription,
      pdpLabels,
      itemPartNumber,
      margins,
      accessibilityLabels,
      backgroundColor,
      paddings,
      color,
    } = this.props;
    const {
      productInfo: { colorFitsSizesMap },
    } = this.props;
    const { ProductDescriptionNewDesign, PartNumber } = pdpLabels;
    const { isAccordionOpen } = this.state;
    const colorSlice = getMapSliceForColor(colorFitsSizesMap, color);
    const colorDisplayId = colorSlice && colorSlice.colorDisplayId;
    const imageDownStyle = { width: 18, height: 10 };

    return (
      <PageContainerNewRedesign
        ref={(ref) => {
          this.accordionView = ref;
        }}
        margins={margins}
        paddings={paddings}
        renderToHardwareTextureAndroid
        backgroundColor={backgroundColor}
        isAccordionOpen={isAccordionOpen}
      >
        <PageContainerBundleViewContent>
          <BodyCopy
            dataLocator="pdp_product_description_label"
            fontFamily={this.getGymStyling().fontFamily}
            fontSize="fs16"
            fontWeight={this.getGymStyling().fontWeight}
            color="gray.900"
            text="Product Details"
            letterSpacing="ls1"
          />
          {shortDescription ? (
            <BodyContentBundleViewWrapper>
              <BodyCopyWithSpacing
                text={shortDescription}
                fontFamily="secondary"
                fontWeight="light"
                color="gray.900"
                fontSize="fs14"
                letterSpacing="ls05"
              />
            </BodyContentBundleViewWrapper>
          ) : null}
          {!isAccordionOpen && longDescription ? (
            <>
              {this.renderLongDescription(longDescription).map((prop) => {
                return (
                  <BodyContentBundleViewWrapper>
                    <BodyCopy
                      fontFamily="secondary"
                      fontWeight="light"
                      color="gray.900"
                      fontSize="fs14"
                      spacingStyles="padding-bottom-XXS"
                      text={prop}
                      textAlign="left"
                      letterSpacing="ls05"
                    />
                  </BodyContentBundleViewWrapper>
                );
              })}
            </>
          ) : null}

          {!isAccordionOpen && (
            <BodyContentBundleViewWrapper>
              <BodyCopyWithSpacing
                text={`${PartNumber} ${colorDisplayId || itemPartNumber}`}
                fontFamily="secondary"
                fontWeight="light"
                fontSize="fs14"
                spacingStyles="padding-top-XXS"
              />
            </BodyContentBundleViewWrapper>
          )}
        </PageContainerBundleViewContent>
        <StyleProductDescriptionNew
          onPress={this.handleAccordionToggle}
          accessible
          accessibilityRole={AccessibilityRoles.Button}
          accessibilityLabel={ProductDescriptionNewDesign}
          accessibilityState={{ expanded: isAccordionOpen }}
        >
          <ImageStyleWrapper>
            <Anchor onPress={this.handleAccordionToggle}>
              <Image
                source={!isAccordionOpen ? upIconNew : downIconNew}
                alt={
                  isAccordionOpen
                    ? accessibilityLabels.lbl_up_arrow_icon
                    : accessibilityLabels.lbl_down_arrow_icon
                }
                style={imageDownStyle}
              />
            </Anchor>
          </ImageStyleWrapper>
        </StyleProductDescriptionNew>
      </PageContainerNewRedesign>
    );
  };

  render() {
    const { isNewReDesignDescription, isFromBundleProductView } = this.props;

    if (isFromBundleProductView && isNewReDesignDescription) {
      return this.renderProductDescriptionForBundleProductView();
    }
    if (isNewReDesignDescription && !isFromBundleProductView) {
      return this.renderProductDescriptionNewRedesign();
    }
    return this.renderProductDescription();
  }
}

ProductDetailDescription.propTypes = {
  pdpLabels: PropTypes.shape({}),
  longDescription: PropTypes.string,
  shortDescription: PropTypes.string,
  itemPartNumber: PropTypes.string,
  margins: PropTypes.string,
  paddings: PropTypes.string,
  scrollToAccordionBottom: PropTypes.func,
  accessibilityLabels: PropTypes.shape({}),
  backgroundColor: PropTypes.string,
  productInfo: PropTypes.shape({}),
  color: PropTypes.string,
  isNewReDesignDescription: PropTypes.bool,
  isFromBundleProductView: PropTypes.bool,
};

ProductDetailDescription.defaultProps = {
  longDescription: '',
  pdpLabels: {},
  shortDescription: '',
  itemPartNumber: '',
  margins: null,
  paddings: null,
  scrollToAccordionBottom: () => {},
  accessibilityLabels: {},
  backgroundColor: null,
  productInfo: {},
  color: '',
  isNewReDesignDescription: false,
  isFromBundleProductView: false,
};

export default ProductDetailDescription;
export { ProductDetailDescription as ProductDetailDescriptionVanilla };
