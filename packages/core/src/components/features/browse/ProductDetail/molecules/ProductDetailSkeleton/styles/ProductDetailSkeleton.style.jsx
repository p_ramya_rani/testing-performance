// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  padding: 10px 15px 20px 15px;
  background: ${(props) => props.theme.colors.WHITE};
  margin: 15px 0;
  .product-detail-skeleton-wrapper {
    display: flex;
    flex-direction: column;
  }
  .thumbnail-list-wrapper {
    display: none;
  }
  .thumbnail-list {
    margin-bottom: 10px;
    height: 110px;
  }
  .product-main-image {
    height: 436px;
    width: 100%;
    margin-bottom: 40px;
    margin-right: 0;
  }
  .product-overview-wrapper {
    width: 100%;
    display: flex;
    flex-direction: column;
    margin-left: 0;
  }
  .product-title {
    width: 100%;
    height: 50px;
    margin-bottom: 20px;
    margin-top: 0px;
  }
  .product-price {
    width: 70%;
    height: 50px;
    margin-bottom: 30px;
  }
  .actual-price {
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        props.isNewPDPEnabled ? `font-size: ${props.theme.typography.fontSizes.fs24};` : ''}
    }
  }

  .product-color {
    width: 100%;
    height: 70px;
    margin-bottom: 20px;
  }
  .product-size {
    width: 100%;
    height: 70px;
    margin-bottom: 40px;
  }
  .product-add-to-bag {
    width: 70%;
    height: 50px;
  }
  .product-breadcrumb-wrapper {
    padding-top: 0px;
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: 15px;
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .inline-badge-item {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-top: 13px;
    }
  }
  @media ${(props) => props.theme.mediaQuery.medium} {
    .product-detail-skeleton-wrapper {
      display: flex;
      flex-direction: row;
    }
    .thumbnail-list-wrapper {
      display: none;
    }
    .product-main-image {
      height: 436px;
      width: 45%;
      margin-right: 2%;
    }
    .product-overview-wrapper {
      width: 45%;
      display: flex;
      flex-direction: column;
      margin-left: 2%;
    }
  }
  .product-skeleton-img {
    min-width: 650px;
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .product-detail-skeleton-wrapper {
      display: flex;
      flex-direction: row;
    }
    .thumbnail-list-wrapper {
      width: 8%;
      display: flex;
      flex-direction: column;
      margin-right: 5%;
    }
    .product-main-image {
      height: 703px;
      width: 40%;
      margin-right: 5%;
    }
    .product-overview-wrapper {
      width: 36%;
      display: flex;
      flex-direction: column;
      margin-right: 2%;
    }
  }

  .color-chips-selector-title,
  .color-chips-selector-title-name {
    font-size: ${(props) => props.theme.fonts.fontSize.listmenu.large}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.black};
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: ${(props) => props.theme.colors.PRIMARY.DARK};
    text-transform: uppercase;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  }

  .color-chips-selector-title-name {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: normal;
    margin-left: 6px;
    font-stretch: normal;
  }

  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;
