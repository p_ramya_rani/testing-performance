// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';
import { StyleSheet } from 'react-native';
import { isAndroid } from '../../../../../../../utils/utils.app';
import { colorWhite } from '../../../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.styles';

const ImageTouchableOpacity = styled.TouchableOpacity`
  justify-content: center;
`;

const Container = styled.View`
  justify-content: center;
  width: 100%;
  bottom: ${(props) => (props.isSBP && isAndroid() ? '32px' : 0)};
  z-index: 0;
`;

const FavoriteAndShareContainer = styled.View`
  z-index: 1;
  position: absolute;
  left: 87%;
  top: 3%;
  align-items: flex-end;
  flex-direction: column;
  justify-content: space-between;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const FavoriteAndPaginationContainer = styled.View`
  justify-content: ${(props) => (props.isNewReDesignEnabled ? `center` : `space-between`)};
  flex-direction: row;
  align-items: center;
  width: 100%;
  margin-top: ${(props) => (props.isNewReDesignEnabled ? 0 : props.theme.spacing.ELEM_SPACING.SM)};
`;

const FavoriteContainer = styled.View`
  flex-direction: ${(props) => (props.isNewReDesignEnabled ? `column` : `row`)};
  align-items: center;
  ${(props) =>
    props.isNewReDesignEnabled
      ? `
    opacity: ${props.theme.opacity.opacity.high};
    background-color: ${props.theme.colors.TRANSPARENT};
    padding: 0 ${props.theme.spacing.ELEM_SPACING.XS_6};
    border-radius: 5px;
    position: absolute;
    z-index: 900;
    height: 40px;
    width: 40px;
    `
      : ``};
  justify-content: ${(props) => (props.isSBP && isAndroid() ? 'flex-end' : 'flex-start')};
  margin-top: ${(props) => (props.isSBP && isAndroid() ? 30 : 1)};
`;

const DownloadContainer = styled.View`
  ${(props) =>
    props.isNewReDesignEnabled
      ? `
    justify-content: center;
    align-items: center;
    width: ${props.theme.spacing.ELEM_SPACING.LRG};
    height: ${props.theme.spacing.ELEM_SPACING.LRG};
    margin-right: ${props.theme.spacing.ELEM_SPACING.XS_6};
  `
      : `
    width: 21px;
    height: 19px;
    margin-right: 0px;
  `}
`;

const ShareDialog = styled.View`
  display: none;
  width: 95%;
  max-width: 500px;
  box-shadow: 0 8px 16px rgba(0, 0, 0, 0.15);
  z-index: -1;
  border: 1px solid #ddd;
  padding: 20px;
  border-radius: 4px;
  background-color: #fff;
`;

const OosText = styled.Text`
  font-size: ${(props) => props.theme.typography.fontSizes.fs32};
  font-weight: ${(props) => props.theme.fonts.fontWeight.black};
  color: ${(props) => props.theme.colors.BLACK};
  font-family: Nunito-Black;
  text-align: ${(props) => props.theme.typography.textAligns.center};
`;

const EmptyView = styled.View``;

const styles = css``;

const ImageCarouselStyleNew = StyleSheet.create({
  customIcon: {
    backgroundColor: colorWhite,
    bottom: 7,
  },
});

export const CompleteTheLookContainer = styled.View`
  height: 44px;
  border-radius: 22px;
  position: absolute;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row-reverse;
  background-color: ${(props) => props.theme.colors.WHITE};
  shadow-color: ${(props) => props.theme.colors.BLACK};
  shadow-opacity: 0.2;
  shadow-radius: 22;
  elevation: 2;
  bottom: ${isAndroid() ? 55 : 23};
  right: 10;
`;

export const CTLTouchableOpacty = styled.TouchableOpacity`
  align-items: center;
  justify-content: flex-start;
  flex-direction: row-reverse;
`;

export const CTLIconContainer = styled.View`
  height: 44px;
  width: 44px;
  border-radius: 22px;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.colors.WHITE};
`;

export const CTLTextContainer = styled.View`
  height: 44px;
  width: 150px;
  padding-right: 5px;
  padding-left: 5px;
  border-radius: 22px;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: ${(props) => props.theme.colors.WHITE};
`;

const ImageCarouselStyle = StyleSheet.create({
  customIcon: {
    backgroundColor: colorWhite,
    bottom: 0,
  },
  favoriteAndPaginationContainer: {
    height: 20,
    justifyContent: 'center',
  },
  view1: {
    flexDirection: 'row',
    height: 45,
  },
});

export {
  styles,
  Container,
  FavoriteAndPaginationContainer,
  FavoriteAndShareContainer,
  FavoriteContainer,
  DownloadContainer,
  ImageTouchableOpacity,
  ShareDialog,
  EmptyView,
  OosText,
  ImageCarouselStyle,
  ImageCarouselStyleNew,
};
