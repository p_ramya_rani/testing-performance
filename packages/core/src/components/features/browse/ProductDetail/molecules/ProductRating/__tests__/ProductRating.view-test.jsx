// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ProductRatingVanilla } from '../ProductRating';

describe('ProductRating should render', () => {
  let shallowComponent;
  const props = {
    ratings: 4.6,
    reviews: 34,
  };

  beforeEach(() => {
    shallowComponent = shallow(<ProductRatingVanilla {...props} />);
  });

  it('Component should be defined', () => {
    expect(shallowComponent).toBeDefined();
  });

  it('should render correctly', () => {
    expect(shallowComponent).toMatchSnapshot();
  });
});
