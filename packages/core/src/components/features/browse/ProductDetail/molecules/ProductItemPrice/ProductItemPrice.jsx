// 9fbef606107a605d69c0edbcd8029e5d
/** @module PDP - Product Price
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import { getDecimalPrice } from '@tcp/core/src/utils/utils';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import withStyles from '../../../../../common/hoc/withStyles';
import productItemPriceStyle from './ProductItemPrice.style';

const ProductItemPrice = (props) => {
  const { packPrice, selectedMultipack, className, pdpLabels } = props;
  const perItemPrice = packPrice / parseInt(selectedMultipack, 10);
  const priceWithDecimal = getDecimalPrice(perItemPrice);
  const perUnitStr = pdpLabels && pdpLabels.lblPdpPerUnit;
  return (
    <BodyCopy className={className} component="span" fontWeight="semibold" fontFamily="secondary">
      <BodyCopy
        className="per-item-price"
        component="span"
        color="gray.800"
        fontSize="fs14"
        fontWeight="semibold"
        fontFamily="secondary"
      >
        <PriceCurrency price={priceWithDecimal} />
        <BodyCopy
          component="span"
          color="gray.800"
          fontSize="fs14"
          fontWeight="semibold"
          fontFamily="secondary"
        >
          {perUnitStr}
        </BodyCopy>
      </BodyCopy>
    </BodyCopy>
  );
};

ProductItemPrice.propTypes = {
  packPrice: PropTypes.string.isRequired,
  selectedMultipack: PropTypes.string,
  className: PropTypes.string,
  pdpLabels: PropTypes.shape({}),
};

ProductItemPrice.defaultProps = {
  selectedMultipack: '1',
  className: '',
  pdpLabels: {},
};

export default errorBoundary(withStyles(ProductItemPrice, productItemPriceStyle));
