// 9fbef606107a605d69c0edbcd8029e5d
/** @module SocialProofMessage
 * @summary renders social proof messages.
 *
 */

import React, { useRef } from 'react';
import { Animated } from 'react-native';
import { PropTypes } from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { SocialProofBodyCopyContainer } from '../styles/SocialProofMessage.style.native';

const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current; // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 22,
      duration: 500,
    }).start();
  }, [fadeAnim]);

  return (
    <Animated.View // Special animatable View
      style={{
        ...props.style,
        height: fadeAnim, // Bind opacity to animated value
      }}
    >
      {props.children}
    </Animated.View>
  );
};

const SocialProofMessage = (props) => {
  const { socialProofMessage } = props;
  const fullMessage = socialProofMessage?.priorityMsg;
  const aboutToSell = fullMessage?.includes('About to sell out');
  const textColor = aboutToSell ? 'red.500' : '';

  let boldTitle = '';
  let title = '';

  if (fullMessage?.includes('$')) {
    const messageArray = fullMessage.split('$');
    [boldTitle, title] = [messageArray[1], messageArray[2]];
  } else {
    title = fullMessage;
  }

  return (
    <FadeInView>
      <SocialProofBodyCopyContainer bgdColor={aboutToSell ? '#f4cfd5' : ''}>
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs12"
          text={boldTitle}
          fontWeight="extrabold"
          color={textColor}
        />
        <BodyCopy fontFamily="secondary" fontSize="fs12" text={title} color={textColor} />
      </SocialProofBodyCopyContainer>
    </FadeInView>
  );
};

SocialProofMessage.propTypes = {
  socialProofMessage: PropTypes.shape({
    priorityMsg: PropTypes.string,
  }),
};

SocialProofMessage.defaultProps = {
  socialProofMessage: {
    priorityMsg: '',
  },
};
export default SocialProofMessage;
