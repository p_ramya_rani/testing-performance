// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .social-proof-text {
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    padding: 5px ${(props) => props.theme.spacing.ELEM_SPACING.XS_6} 5px 7px;
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    white-space: pre-wrap;
    background-color: ${(props) => props.theme.colors.PRIMARY.COLOR2};
    font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
    ${(props) => (props.top ? `white-space: pre-wrap;` : '')}

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
        ${(props) => props.theme.spacing.ELEM_SPACING.XS_6}
        ${(props) => props.theme.spacing.ELEM_SPACING.XXS} 7px;
      ${(props) =>
        !props.top ? 'margin-left: 0px;display: flex;width: 372px;justify-content: center;' : ''};
      ${(props) =>
        props.top
          ? `margin-left: 5px;font-size: ${props.theme.typography.fontSizes.fs12};background-color: ${props.theme.colors.PRIMARY.COLOR2};align-items: center;justify-content: center;min-height: ${props.theme.spacing.ELEM_SPACING.MED};`
          : ''}
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-left: 5px;
      ${(props) =>
        props.top
          ? `margin-top: ${props.theme.spacing.ELEM_SPACING.XXS};font-size: ${props.theme.typography.fontSizes.fs12};background-color: ${props.theme.colors.PRIMARY.COLOR2};align-items: center;justify-content: center;min-height:${props.theme.spacing.ELEM_SPACING.MED};`
          : ''}
    }
  }
  .social-proof-wrapper {
    display: flex;
    flex: 1;
    position: relative;
    bottom: ${(props) =>
      props.longText >= 65 && props.top
        ? props.theme.spacing.ELEM_SPACING.MED
        : props.theme.spacing.ELEM_SPACING.XXS};
    margin-top: ${(props) => (!props.top ? '28px' : 'inherit')};
    margin-left: ${(props) => (!props.top ? '20px' : 'inherit')};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      bottom: ${(props) =>
        props.longText >= 35 && props.top
          ? props.theme.spacing.ELEM_SPACING.MED
          : props.theme.spacing.ELEM_SPACING.XXS};
      ${(props) =>
        !props.top
          ? `margin-top:${props.theme.spacing.ELEM_SPACING.SM};margin-left: inhertit;justify-content: center;bottom: 6px;margin-left:0px;`
          : ''};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      bottom: ${(props) =>
        props.longText >= 32 && props.top
          ? props.theme.spacing.ELEM_SPACING.MED
          : props.theme.spacing.ELEM_SPACING.XXS};
    }
  }
  .toBeBold {
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    display: contents;
  }
`;

export default styles;
