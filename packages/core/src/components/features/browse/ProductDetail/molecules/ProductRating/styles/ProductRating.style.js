// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const productRatingStyle = css`
  min-height: 23px;
  height: 22px;
  display: flex;
  /* Using !important to override bazaar voice plugin css, unable to achieve via increasing the specificity */
  [data-bv-show='rating_summary'] .bv_main_container {
    button.bv_main_container_row_flex {
      display: none !important;
    }
    button.bv_button_buttonMinimalist {
      white-space: nowrap !important;
    }
    .bv_main_container_row_flex {
      padding-right: 0 !important;
    }
  }

  &.ranking-container {
    .bv-core-container-311 .bv-summary-bar-minimalist-horizontal .bv-stars-container {
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} !important;
    }

    .bv-stars-container .bv-rating-ratio {
      padding-right: 0 !important;

      .bv-rating-stars-container .bv-rating-stars {
        letter-spacing: 8px !important;
      }

      .bv-rating-stars-on {
        color: ${(props) => props.theme.colorPalette.yellow[600]} !important;
      }
    }

    .bv-summary-bar-minimalist .bv-write-container .bv-submission-button.bv-write-review {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14} !important;
      font-family: 'Nunito' !important;
      color: ${(props) => props.theme.colorPalette.gray[900]} !important;
    }

    .bv-summary-bar .bv-rating-ratio-number {
      padding-right: 0 !important;
    }
  }
  &.guest {
    #WAR {
      display: none !important;
    }
  }
  &.rating_summary {
    height: 20px;
  }
  &.logged-in {
    #WAR-login1,
    #WAR-login2 {
      display: none !important;
    }
  }

  .ratings-container {
    height: 22px;
    margin-right: 8px;
  }
  &.rating-wrapper-new-pdp {
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-top: 15px;
    }
  }
  .write-review-link {
    margin-top: 1px;
  }
  .custom-ranking-container {
    text-align: left;
    height: 20px;
    display: flex;
    flex: 1;
    img {
      @media ${(props) => props.theme.mediaQuery.medium} {
        min-width: ${(props) => (props.isBagPage ? '70px' : '100px')};
      }
      width: ${(props) => (props.isBagPage ? '70px' : '100px')};
      height: 17px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        width: 72px;
        height: 15px;
      }
    }

    .rating-star-overlay {
      ${(props) => props.pageName === 'PDP' && `cursor: pointer`};
      position: absolute;
      overflow: hidden;
      width: calc(
        ${(props) =>
          `(${props.ratings} / 5) * ${
            (props.starRatingSize && props.starRatingSize.large) || 100
          }px`}
      );

      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        width: calc(
          ${(props) =>
            `(${props.ratings} / 5) * ${
              (props.starRatingSize && props.starRatingSize.small) || 72
            }px`}
        );
      }
    }

    .rating-star-yellow {
      background-color: ${(props) => props.theme.colorPalette.yellow[600]};
      max-width: initial;
      display: inline;
      @media ${(props) => props.theme.mediaQuery.medium} {
        ${(props) =>
          props.isNewPDPEnabled
            ? `
          margin-bottom: 5px;
        `
            : ''}
      }
    }

    .rating-star-grey {
      ${(props) => props.pageName === 'PDP' && `cursor: pointer`};
      background-color: ${(props) => props.theme.colorPalette.gray[500]};
      display: inline;
    }

    .reviews-count {
      ${(props) => props.pageName === 'PDP' && `cursor: pointer`};
      color: ${(props) => props.theme.colorPalette.gray[600]};
      vertical-align: top;
      display: inline-block;
      margin-top: 2px;
      margin-left: 2px;
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy1}px;
      }
    }
    .reviews-count-quick-view-drawer-redesign {
      ${(props) => props.pageName === 'PDP' && `cursor: pointer`};
      color: ${(props) => props.theme.colorPalette.gray[900]};
      vertical-align: top;
      display: inline-block;
      margin-top: 2px;
      margin-left: 2px;
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
      ${(props) =>
        props.isNewPDPEnabled
          ? `
        text-decoration: underline;
      `
          : ''}
      @media ${(props) => props.theme.mediaQuery.large} {
        ${(props) =>
          props.isNewPDPEnabled
            ? `
          margin-left: 6px;
        `
            : ''}
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        ${(props) =>
          props.isNewPDPEnabled
            ? `
          margin-top: 0;
        `
            : ''}
      }
    }
  }
`;

export default productRatingStyle;
