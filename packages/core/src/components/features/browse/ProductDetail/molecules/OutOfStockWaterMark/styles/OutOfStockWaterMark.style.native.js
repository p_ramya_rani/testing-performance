// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

const WaterMarkContainer = styled.View`
  ${(props) =>
    props.isNewReDesignEnabled
      ? `
  justify-content: flex-start;
  `
      : `
  background-color: rgba(255, 255, 255, 0.7);
  justify-content: center;
  `}
  width: 100%;
  height: 100%;
  position: absolute;
  top: 16;
  align-items: center;
  display: flex;
`;

export const LabelContainer = styled.View`
  background: ${(props) => props.theme.colors.WHITE};
  opacity: 0.7;
  border-radius: 17px;
  ${(props) =>
    props.isNewReDesignEnabled
      ? `
  padding: 8px 10px;
  `
      : `
  width: 75%;
  `}
  align-items: center;
`;

export default WaterMarkContainer;
