// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ImageCarouselVanilla } from '../views/ImageCarousel.view.native';

describe('ImageCarouselVanilla component', () => {
  const imageUrlOne = '3010436/3010436_01.mp4';
  const imageUrlTwo = '3010436/3010436_01-1.mp4';
  const imageUrlThree = '3010436/3010436_01-1.jpg';
  const props = {
    selectedColorProductId: 1281561,
    item: {
      colorFitsSizesMap: [
        {
          color: {
            name: 'TIDAL',
            imagePath:
              'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/swatches/3000789_IV.jpg',
            family: 'BLUE',
          },
          pdpUrl: '/p/3000789_IV',
          colorProductId: '1281561',
          colorDisplayId: '3000789_IV',
          categoryEntity: 'Girl:School Uniforms:Tops',
          fits: [
            {
              fitNameVal: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'XS (4)',
                  skuId: '1281977',
                  listPrice: 26.95,
                  offerPrice: 26.95,
                  maxAvailable: 262,
                  variantId: '00193511089371',
                  variantNo: '3000789019',
                  position: 0,
                },
              ],
            },
          ],
          listPrice: 26.95,
          offerPrice: 26.95,
        },
      ],
      imagesByColor: {
        BLACK: {
          basicImageUrl:
            'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/3000789_01.jpg',
          extraImages: [
            {
              isOnModalImage: false,
              iconSizeImageUrl:
                'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/125/3000789_01.jpg',
              listingSizeImageUrl:
                'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/380/3000789_01.jpg',
              regularSizeImageUrl:
                'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/3000789_01.jpg',
              bigSizeImageUrl:
                'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/3000789_01.jpg',
              superSizeImageUrl:
                'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/900/3000789_01.jpg',
            },
          ],
        },
      },
    },
  };

  const testPropsSBP = {
    isBundleProduct: false,
    onFavoriteRemove: jest.fn(),
    onAddItemToFavorites: jest.fn(),
    currentProduct: {
      generalProductId: 1220912,
    },
    currentColorEntry: {
      colorProductId: 123456,
      miscInfo: {
        isBopisEligible: false,
        isBossEligible: true,
        badge1: {
          defaultBadge: 'ONLINE EXCLUSIVE',
        },
        badge2: '40% OFF',
        keepAlive: false,
        isInDefaultWishlist: false,
      },
    },
    isLoggedIn: false,
    defaultWishListFromState: [
      {
        externalIdentifier: 122011,
        giftListItemID: 1111000,
      },
    ],
    ...props,
    isSBP: true,
    imageUrls: [
      {
        isShortImage: false,
        isOnModalImage: false,
        iconSizeImageUrl: imageUrlOne,
        listingSizeImageUrl: imageUrlOne,
        regularSizeImageUrl: imageUrlOne,
        bigSizeImageUrl: imageUrlOne,
        superSizeImageUrl: imageUrlOne,
      },
      {
        isShortImage: false,
        isOnModalImage: false,
        iconSizeImageUrl: imageUrlTwo,
        listingSizeImageUrl: imageUrlTwo,
        regularSizeImageUrl: imageUrlTwo,
        bigSizeImageUrl: imageUrlTwo,
        superSizeImageUrl: imageUrlTwo,
      },
      {
        isShortImage: false,
        isOnModalImage: false,
        iconSizeImageUrl: imageUrlThree,
        listingSizeImageUrl: imageUrlThree,
        regularSizeImageUrl: imageUrlThree,
        bigSizeImageUrl: imageUrlThree,
        superSizeImageUrl: imageUrlThree,
      },
    ],
  };

  it('should renders ImageCarouselVanilla correctly', () => {
    const component = shallow(<ImageCarouselVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders favorite icon and onFavoriteRemove', () => {
    const props1 = {
      onFavoriteRemove: jest.fn(),
      currentProduct: {
        generalProductId: 1220912,
      },
      currentColorEntry: {
        colorProductId: 123456,
      },
      isLoggedIn: false,
      defaultWishListFromState: [
        {
          externalIdentifier: 122011,
          giftListItemID: 1111000,
        },
      ],
      ...props,
    };
    const component = shallow(<ImageCarouselVanilla {...props1} />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly when sbp flag is true', () => {
    const component = shallow(<ImageCarouselVanilla {...testPropsSBP} />);
    expect(component).toMatchSnapshot();
  });

  it('should not add product to favorite', () => {
    const props2 = {
      ...testPropsSBP,
      deleteFavItemInProgressFlag: true,
    };
    const component = shallow(<ImageCarouselVanilla {...props2} />);
    expect(component.instance().onFavorite()).toBeUndefined();
  });

  it('should not add product to favorite if not logged in', () => {
    const setStateSpy = jest.spyOn(ImageCarouselVanilla.prototype, 'setState');
    const component = shallow(<ImageCarouselVanilla {...testPropsSBP} />);
    component.instance().onFavorite();
    expect(setStateSpy).toHaveBeenCalledTimes(2);
  });

  it('should add product to favorite', () => {
    const setStateSpy = jest.spyOn(ImageCarouselVanilla.prototype, 'setState');
    const props3 = {
      ...testPropsSBP,
      isLoggedIn: true,
    };
    const component = shallow(<ImageCarouselVanilla {...props3} />);
    component.instance().onFavorite();
    expect(props3.onAddItemToFavorites).toHaveBeenCalled();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should not remove product from favorite', () => {
    const props4 = {
      ...testPropsSBP,
      deleteFavItemInProgressFlag: true,
    };
    const component = shallow(<ImageCarouselVanilla {...props4} />);
    expect(component.instance().onFavoriteRemove()).toBeUndefined();
  });

  it('should render favorite icon correctly sbp', () => {
    const component = shallow(<ImageCarouselVanilla {...testPropsSBP} />);
    const isSBP = true;
    expect(component.instance().renderFavoriteIcon(isSBP)).toMatchSnapshot();
  });

  it('should set default wishlist', () => {
    const component = shallow(<ImageCarouselVanilla {...testPropsSBP} />);
    const isSBP = false;
    expect(component.instance().renderFavoriteIcon(isSBP)).toMatchSnapshot();
  });

  it('should remove product from wishlist', () => {
    const setStateSpy = jest.spyOn(ImageCarouselVanilla.prototype, 'setState');
    const props5 = {
      ...testPropsSBP,
      onSetLastDeletedItemIdAction: jest.fn(),
      defaultWishListFromState: {
        '3010701_01': {
          isInDefaultWishlist: false,
          externalIdentifier: '5640007',
          giftListItemID: '30725008',
        },
      },
    };
    const productId = '3010701_01';
    const component = shallow(<ImageCarouselVanilla {...props5} />);
    component.setState({ favoritedCount: 1 });
    component.instance().onFavoriteRemove(productId);
    expect(props5.onSetLastDeletedItemIdAction).toHaveBeenCalled();
    expect(setStateSpy).toHaveBeenCalled();
  });
});
