// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import get from 'lodash/get';

const getAdditionalStyle = (props) => {
  const { margins, backgroundColor, theme, paddings } = props;
  const { colorPalette } = theme;
  const bgColor = get(colorPalette, backgroundColor, '#f1f0f0');
  return {
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
    ...(backgroundColor && { background: bgColor }),
  };
};

export const StyleProductDescription = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const StyleProductDescriptionNew = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
  background-color: white;
  padding: 12px;
`;

export const ImageStyleWrapper = styled.View`
  justify-content: center;
  align-items: center;
`;

export const ItemStyleWrapper = styled.View`
  width: 100%;
  align-items: flex-end;
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

export const StyleLongDescription = styled.View`
  flex-direction: row;
`;

export const PageContainer = styled.View`
  ${getAdditionalStyle}
`;

export const PageContainerContent = styled.View`
  height: ${(props) => (props.isAccordionOpen ? 'auto' : '160px')};
  padding: 15px;
  overflow: hidden;
`;

export const PageContainerNewRedesign = styled.View`
  border-radius: 20px;
  margin-top: 10px;
  box-shadow: ${(props) => props.theme.shadow.NEW_DESIGN.BOX_SHADOW};
  elevation: 10;
`;

export const HeaderWrapper = styled.View`
  margin-bottom: -8px;
`;

export const PageContainerBundleViewContent = styled.View`
  height: auto;
  padding: 15px;
  overflow: hidden;
`;

export const BodyContentBundleViewWrapper = styled.View`
  padding-top: 20px;
`;

export default {
  PageContainer,
  StyleProductDescription,
  ImageStyleWrapper,
  StyleLongDescription,
  PageContainerNewRedesign,
  PageContainerContent,
  StyleProductDescriptionNew,
  HeaderWrapper,
  PageContainerBundleViewContent,
  BodyContentBundleViewWrapper,
};
