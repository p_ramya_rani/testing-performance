// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

const getAdditionalStyle = (props) => {
  const { isBoss, isNoRush, disabled } = props;
  const width = isNoRush ? '40%' : '30%';
  return {
    ...(width && { width: isBoss ? width : '50%' }),
    ...(disabled && { opacity: 0.5 }),
  };
};

const Container = styled.View`
  width: 90%;
  padding: 16px;
`;

const TabContainer = styled.View`
  flex-direction: row;
  background-color: ${(props) => props.theme.colors.WHITE};
  border-radius: 18px;
  height: 36px;
  align-items: center;
  justify-content: space-between;
`;

// eslint-disable-next-line no-nested-ternary
const TabButton = styled.TouchableOpacity`
  background-color: ${(props) =>
    props.isSelected ? props.theme.colorPalette.blue[800] : props.theme.colors.WHITE};
  border-radius: 18px;
  height: 36px;
  align-items: center;
  justify-content: center;
  padding-horizontal: 12px;
  ${getAdditionalStyle}
`;

const styles = css``;

export { styles, Container, TabContainer, TabButton };
