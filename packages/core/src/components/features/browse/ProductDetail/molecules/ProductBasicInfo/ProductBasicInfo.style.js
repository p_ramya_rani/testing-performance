// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const ProductBasicInfoStyle = css`
  /* Using !important to override bazaar voice plugin css, unable to achieve via increasing the specificity */
  [data-bv-show='rating_summary'] .bv_main_container {
    button.bv_main_container_row_flex {
      display: none !important;
    }
    button.bv_button_buttonMinimalist {
      white-space: nowrap !important;
      text-transform: lowercase !important;
      ::first-letter {
        text-transform: capitalize !important;
      }
    }
    .bv_main_container_row_flex {
      padding-right: 0 !important;
    }
  }
  .sub-header-wrapper {
    display: flex;
    justify-content: space-between;
    align-items: baseline;
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-top: 13px;
    }
  }
  .information-container {
    min-height: ${(props) => !props.isNewPDPEnabled && !props.isFbt && '70px'};
  }
  .product-brand {
    text-transform: capitalize;
    color: ${(props) => props.theme.colors.TEXT.BRANDGRAY};
    padding-bottom: 0;
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
  }
  .title-wrapper {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-bottom: ${(props) => (props.socialProofMessage?.variation === 'top' ? '15px' : '')};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: ${(props) => (props.socialProofMessage?.variation === 'top' ? '11px' : '')};
    }

    h2 {
      ${(props) =>
        props.isFbt
          ? `font-size: 13px; font-weight: 700; margin-bottom: ${props.theme.spacing.ELEM_SPACING.XS} `
          : ''};
    }
  }
  .fav-icon-wrapper {
    display: grid;
  }
  .favorite-count {
    text-align: center;
    line-height: 1;
  }
  .wishlist-container {
    margin-top: 10px;
    height: 35px;
    width: 35px;
  }
  .product-title {
    @media ${(props) => props.theme.mediaQuery.smallMax} {
      margin-block-end: 0;
      margin-block-start: 0;
    }
  }
  .new-pdp-colour {
    text-transform: capitalize;
  }
  .rating-section-bottom {
    padding-bottom: 10px;
    padding-top: 2px;
  }
`;

export default ProductBasicInfoStyle;
