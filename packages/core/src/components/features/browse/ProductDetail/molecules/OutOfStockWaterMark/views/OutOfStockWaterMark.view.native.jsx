// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import WaterMarkContainer, { LabelContainer } from '../styles/OutOfStockWaterMark.style.native';

const OutOfStockWaterMark = ({ label, fontSize, children, isNewReDesignEnabled }) => {
  if (children)
    return (
      <WaterMarkContainer isNewReDesignEnabled={isNewReDesignEnabled}>
        {children}
      </WaterMarkContainer>
    );
  return (
    <WaterMarkContainer isNewReDesignEnabled={isNewReDesignEnabled}>
      <LabelContainer isNewReDesignEnabled={isNewReDesignEnabled}>
        <BodyCopy
          fontFamily="secondary"
          fontSize={fontSize}
          fontWeight="extrabold"
          color="gray[900]"
          text={label}
        />
      </LabelContainer>
    </WaterMarkContainer>
  );
};

OutOfStockWaterMark.propTypes = {
  label: PropTypes.string.isRequired,
  fontSize: PropTypes.string,
  children: PropTypes.element,
  isNewReDesignEnabled: PropTypes.bool,
};

OutOfStockWaterMark.defaultProps = {
  fontSize: 'fs16',
  children: null,
  isNewReDesignEnabled: false,
};

export default withStyles(OutOfStockWaterMark);
