/* eslint-disable complexity , max-lines, sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import Stars from 'react-native-stars';
import { TouchableOpacity, View, Share } from 'react-native';
import logger from '@tcp/core/src/utils/loggerInstance';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { SINGLE_TEXT } from '@tcp/core/src/constants/rum.constants';
import PromotionalMessage from '@tcp/core/src/components/common/atoms/PromotionalMessage';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { getDecimalPrice } from '@tcp/core/src/utils/utils';
import ProductRating from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductRating/ProductRating.view.native';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import { getAPIConfig } from '../../../../../../../utils/index.native';
import {
  Container,
  RowContainer,
  BazaarVoiceContainer,
  ReviewAndRatingContainer,
  styles,
  EmptyView,
  ProductSummaryStyle,
  ProductSummaryRatingAndMessage,
  ProductSummaryRating,
  ProductSummaryPromotionalMessage,
  ProductSummaryPrice,
  ProductSummaryPriceDiscount,
  AfterPayWrapper,
} from '../styles/ProductSummary.style.native';

import LineComp from '../../../../../../common/atoms/Line';
import Anchor from '../../../../../../common/atoms/Anchor';
import { getMapSliceForColorProductId } from '../../../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { getPromotionalMessage } from '../../../../ProductListing/molecules/ProductList/utils/utility';
import CustomIcon from '../../../../../../common/atoms/Icon';
import { ICON_NAME, ICON_FONT_CLASS } from '../../../../../../common/atoms/Icon/Icon.constants';
import { streamlineProductName } from '../../../../../../../../../mobileapp/src/components/features/shopByProfile/PLP/helper';
import ModuleQConstant from '../../../../../../common/molecules/ModuleQ/ModuleQ.constant';
import starImg from '../../../../../../../../../mobileapp/src/assets/images/star-new.png';

class ProductSummary extends React.PureComponent {
  constructor(props) {
    super(props);
    this.loyaltyPromotionMessage = '';
  }

  renderWriteAReviewAnchor = (anchorLabel) => {
    const { isBundleProduct } = this.props;
    if (isBundleProduct) {
      return (
        <Anchor
          fontSizeVariation="medium"
          anchorVariation="custom"
          colorName="gray.900"
          underline
          href="#"
          locator="pdp_lbl_write_review"
          className="details-link"
          text={anchorLabel}
        />
      );
    }
    return <EmptyView width="20%" />;
  };

  onCompleteTheLook = () => {
    const { isSBP, navigation, productData, stylisticsProductTabList } = this.props;
    const { productId } = productData;
    const productItem = stylisticsProductTabList[productId];
    const { subItemsId, id } = productItem?.[0] || {};
    const { RECOMMENDATION, COMPLETETHELOOK } = ModuleQConstant;

    navigation.navigate('OutfitDetail', {
      title: COMPLETETHELOOK,
      outfitId: id,
      vendorColorProductIdsList: subItemsId,
      viaModule: RECOMMENDATION,
      isSBP,
      selectedProductId: productId,
    });
  };

  renderCompleteTheLookAnchor = (anchorLabel) => {
    const { isBundleProduct, showCompleteTheLook, productData, stylisticsProductTabList } =
      this.props;
    const { productId } = productData;
    const productItem = stylisticsProductTabList[productId];
    const { subItemsId, id } = productItem?.[0] || {};
    if (showCompleteTheLook && !isBundleProduct) {
      if (subItemsId && id) {
        return (
          <Anchor
            fontSizeVariation="medium"
            anchorVariation="custom"
            colorName="gray.900"
            underline
            href="#"
            locator="pdp_anchor_complete_the_look"
            className="details-link"
            onPress={this.onCompleteTheLook}
            text={anchorLabel}
          />
        );
      }
      return null;
    }
    return <EmptyView width="30%" />;
  };

  renderBazaarVoiceComponentIsSBP = (bazaarVoice, isSBP) => {
    // SBP-MERGE JIRA SBP-902 STYLING
    // SBP MERGE JIRA SBP-900 CMS LABELS
    return (
      <View style={ProductSummaryStyle.view1}>
        <View style={ProductSummaryStyle.view2}>
          <CustomIcon
            iconFontName={ICON_FONT_CLASS.Icomoon}
            name={ICON_NAME.iconShare}
            size="fs24"
            color="gray.1600"
            dataLocator="pdp_social_connect"
            onPress={this.onShare}
            title="Share"
            isButton
            accessibilityLabel={ICON_NAME.iconShare}
          />
        </View>
        <View style={isSBP && ProductSummaryStyle.view3}>
          {this.renderBazaarVoiceComponent(bazaarVoice, isSBP)}
        </View>
      </View>
    );
  };

  renderBazaarVoiceComponent = (bazaarVoice, isSBP) => {
    const { pdpLabels, accessibilityLabels, isNewReDesignProductSummary } = this.props;
    const { completeTheLook, writeAReview } = pdpLabels;
    const totalRating = isNewReDesignProductSummary
      ? bazaarVoice.totalReviewCount
      : `( ${bazaarVoice.totalReviewCount} )`;

    return (
      <BazaarVoiceContainer isNewReDesignProductSummary={isNewReDesignProductSummary}>
        <TouchableOpacity accessibilityRole="link" onPress={() => this.scrollToRatingSection()}>
          <ReviewAndRatingContainer>
            <Stars
              display={bazaarVoice.avgRating}
              spacing={isNewReDesignProductSummary ? 4 : 8}
              count={5}
              starSize={15}
              disabled
              fullStar={starImg}
            />
            <BodyCopy
              margin={isNewReDesignProductSummary ? '0 0 0 5px' : '0 0 0 18px'}
              dataLocator="pdp_write_review_icon"
              fontFamily="secondary"
              fontSize="fs12"
              fontWeight="regular"
              color="gray.900"
              text={totalRating}
              accessibilityText={`${accessibilityLabels.rating} ${bazaarVoice.avgRating.toFixed(
                1
              )} ${accessibilityLabels.outOf} ${totalRating}`}
            />
          </ReviewAndRatingContainer>
        </TouchableOpacity>
        {this.renderWriteAReviewAnchor(writeAReview)}
        {!isSBP && !isNewReDesignProductSummary
          ? this.renderCompleteTheLookAnchor(completeTheLook)
          : null}
      </BazaarVoiceContainer>
    );
  };

  renderBVSkeleton = (fromSkeleton, renderRatingReview, isGiftCard, isStyleWith) => {
    if (!fromSkeleton && renderRatingReview && !isGiftCard && !isStyleWith)
      return <LoaderSkelton width="100%" height="40px" />;
    return null;
  };

  renderPromotionalMessage = () => {
    const { productData, isPlcc, isBrierleyPromoEnabled, pdpLabels, isNewReDesignProductSummary } =
      this.props;
    const { promotionalMessage, promotionalPLCCMessage } = productData;

    this.loyaltyPromotionMessage = isBrierleyPromoEnabled
      ? pdpLabels.pointsAndRewardsText
      : getPromotionalMessage(isPlcc, {
          promotionalMessage,
          promotionalPLCCMessage,
        });
    if (this.loyaltyPromotionMessage !== '') {
      return (
        <PromotionalMessage
          fontSize="fs12"
          text={this.loyaltyPromotionMessage}
          isPlcc={isPlcc}
          marginTop={isNewReDesignProductSummary ? 0 : 8}
          dataLocator="pdp_loyalty_text"
          isBrierleyPromoEnabled={isBrierleyPromoEnabled}
          isNewReDesignProductSummary={isNewReDesignProductSummary}
        />
      );
    }
    return null;
  };

  renderTopBadge = (badge1Value) => {
    if (badge1Value !== '') {
      return (
        <BodyCopy
          dataLocator="pdp_product_badges"
          margin="16px 0 0 0"
          fontFamily="secondary"
          fontSize="fs10"
          fontWeight="regular"
          color="gray.900"
          text={badge1Value}
        />
      );
    }
    return null;
  };

  renderOfferPrice = () => {
    const {
      productData,
      offerPrice,
      showPriceRange,
      isBundleProduct,
      selectedSize,
      highOfferPrice,
      pdpLabels,
      isNewReDesignProductSummary,
      isFromBundleProductView,
    } = this.props;
    const apiConfigObj = getAPIConfig();
    const { brandId } = apiConfigObj;
    const { TCPStyleQTY } = productData;
    const offerPricePickValue = offerPrice || productData.offerPrice;
    const { highListPrice } = productData;
    const offerPriceValue = this.calculatePriceValue(offerPricePickValue);
    const highOfferPriceValue = this.calculatePriceValue(
      highOfferPrice || productData.highOfferPrice,
      null
    );
    const text =
      !selectedSize && highOfferPriceValue && (showPriceRange || isBundleProduct)
        ? `${offerPriceValue} - $${highOfferPriceValue}`
        : offerPriceValue;
    const priceText = isFromBundleProductView ? `${offerPriceValue} - $${highListPrice}` : text;

    const { selectedMultipack, TCPMultipackProductMapping } = this.props;
    const selectedMpackCount = TCPStyleQTY || selectedMultipack;
    const ifSingleText =
      selectedMultipack === pdpLabels.lblPdpMultipackSingle ? 1 : selectedMpackCount;
    const perUnitStr = pdpLabels && pdpLabels.lblPdpPerUnit;
    const multiPackSingle = (pdpLabels && pdpLabels.lblPdpMultipackSingle) || SINGLE_TEXT;

    let singleMultipackCheck;
    if (Number.isNaN(selectedMultipack)) {
      if (selectedMultipack.toLowerCase() === multiPackSingle.toLowerCase()) {
        singleMultipackCheck = true;
      }
    } else {
      singleMultipackCheck = false;
    }

    const showPerPrice =
      selectedMpackCount &&
      String(selectedMpackCount) !== '1' &&
      !singleMultipackCheck &&
      TCPMultipackProductMapping &&
      TCPMultipackProductMapping.length;

    const perPrice = offerPricePickValue / parseInt(ifSingleText, 10).toFixed(2);
    const priceWithDecimal = getDecimalPrice(perPrice);

    return (
      <RowContainer>
        <BodyCopy
          margin={isNewReDesignProductSummary ? '0 0 0 0' : '4px 0 0 0'}
          dataLocator="pdp_current_product_price"
          fontFamily="secondary"
          fontSize="fs22"
          fontWeight="black"
          color={brandId === 'tcp' ? 'red.600' : 'black'}
          text={priceText}
        />
        {this.renderPerItemPrice(priceWithDecimal, showPerPrice, perUnitStr)}
      </RowContainer>
    );
  };

  renderPerItemPrice = (perPrice, showPerPrice, perUnitStr) => {
    const { isPerUnitPriceEnabled } = this.props;
    if (showPerPrice && isPerUnitPriceEnabled) {
      return (
        <BodyCopy
          margin="10px 0 0 10px"
          fontFamily="secondary"
          fontSize="fs13"
          fontWeight="semibold"
          color="gray.800"
          text={`${this.calculatePriceValue(perPrice)}${perUnitStr}`}
        />
      );
    }
    return null;
  };

  renderPricePercentageDiscountLabel = (value) => {
    if (value) {
      return (
        <BodyCopy
          dataLocator="pdp_discounted_percentage"
          margin="0 0 0 10px"
          fontFamily="secondary"
          fontSize="fs13"
          fontWeight="regular"
          color="red.600"
          text={value}
        />
      );
    }
    return null;
  };

  renderListPriceDash = (value) => {
    if (value) {
      return (
        <BodyCopy
          dataLocator="pdp_discounted_product_price"
          margin="0 0 0 10px"
          fontFamily="secondary"
          fontSize="fs13"
          fontWeight="regular"
          color="gray.800"
          text=" - "
        />
      );
    }
    return null;
  };

  renderListPriceLabels = (value) => {
    const { accessibilityLabels } = this.props;
    if (value) {
      return (
        <BodyCopy
          dataLocator="pdp_discounted_product_price"
          margin="0 0 0 5px"
          textDecoration="line-through"
          fontFamily="secondary"
          fontSize="fs13"
          fontWeight="regular"
          color="gray.800"
          text={value}
          accessibilityText={`${accessibilityLabels && accessibilityLabels.previousPrice} ${value}`}
        />
      );
    }
    return null;
  };

  calculatePriceValue = (price, defaultReturn = 0) => {
    let priceValue = defaultReturn;
    const { currencySymbol, currencyExchange } = this.props;
    const currency = currencySymbol === 'USD' ? '$' : currencySymbol;
    if (price && price > 0) {
      priceValue = `${currency}${(price * currencyExchange).toFixed(2)}`;
    }
    return priceValue;
  };

  renderListPrice = (badge2) => {
    const {
      productData,
      showPriceRange,
      isBundleProduct,
      selectedSize,
      highListPrice,
      listPrice,
      offerPrice,
    } = this.props;
    const listPriceValue = this.calculatePriceValue(listPrice);
    const highListPriceValue = this.calculatePriceValue(
      highListPrice || productData.highListPrice,
      null
    );
    if (
      listPrice > offerPrice ||
      (showPriceRange && (highListPrice || productData.highListPrice > listPrice))
    ) {
      return (
        <RowContainer>
          {this.renderListPriceLabels(listPriceValue)}
          {!selectedSize &&
            (showPriceRange || isBundleProduct) &&
            this.renderListPriceDash(highListPriceValue)}
          {!selectedSize &&
            (showPriceRange || isBundleProduct) &&
            this.renderListPriceLabels(highListPriceValue)}
          {this.renderPricePercentageDiscountLabel(badge2)}
        </RowContainer>
      );
    }
    return null;
  };

  renderSoldOutError = () => {
    const { keepAlive, outOfStockLabels } = this.props;
    return keepAlive ? (
      <BodyCopy
        text={outOfStockLabels.itemSoldOutMessage}
        color="red.500"
        fontFamily="secondary"
        fontSize="fs10"
      />
    ) : null;
  };

  onShare = async () => {
    const { navigation } = this.props;
    const { webAppDomain } = getAPIConfig();
    const pdpUrl = (navigation && navigation.getParam('pdpUrl')) || '';
    const shareUrl = `${webAppDomain}/us/p/${pdpUrl}`;
    try {
      await Share.share({
        message: shareUrl,
      });
    } catch (error) {
      logger.log(error.message);
    }
  };

  scrollToRatingSection() {
    const { scrollToTarget, parentExpandRatingReview } = this.props;
    parentExpandRatingReview();
    scrollToTarget('rating');
  }

  renderProductRating = (bazaarVoice) => {
    const { renderRatingReview, isSBP, isGiftCard, isStyleWith, fromSkeleton } = this.props;
    if (bazaarVoice && renderRatingReview && !isGiftCard && !isStyleWith) {
      if (isSBP) {
        return this.renderBazaarVoiceComponentIsSBP(bazaarVoice, isSBP);
      }
      return this.renderBazaarVoiceComponent(bazaarVoice);
    }
    return this.renderBVSkeleton(fromSkeleton, renderRatingReview, isGiftCard, isStyleWith);
  };

  render() {
    const {
      productData,
      selectedColorProductId,
      isGiftCard,
      fromSkeleton,
      isStyleWith,
      isSBP,
      isNewReDesignProductSummary,
      getProductNameAndTopbadge,
      parentExpandRatingReview,
      keepAlive,
      offerPrice,
      isBundleProduct,
      isFromBundleProductView,
    } = this.props;

    if (JSON.stringify(productData) !== '{}') {
      const colorFitsSizesMap = get(productData, 'colorFitsSizesMap', null);
      const curentColorEntry =
        colorFitsSizesMap &&
        getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId);
      const currentColorMiscInfo = (curentColorEntry && curentColorEntry.miscInfo) || {};
      const { badge1, badge2 } = currentColorMiscInfo;
      let badge1Value;
      if (curentColorEntry) {
        badge1Value = badge1.matchBadge ? badge1.matchBadge : badge1.defaultBadge;
      }
      const {
        name,
        bazaarVoice,
        unbxdRatings,
        unbxdReviewsCount,
        ratings,
        reviewsCount,
        alternateSizes,
      } = productData;
      let productName = name;
      const isProductPartOfCollection = alternateSizes && Object.keys(alternateSizes).length > 0;
      if (isSBP) {
        productName = streamlineProductName(name, isProductPartOfCollection);
      }
      const offerPricePickValue = offerPrice || productData.offerPrice;
      const afterPayMessagingPrice = isFromBundleProductView
        ? productData.highListPrice
        : offerPricePickValue;

      const show = isSBP || !isNewReDesignProductSummary;
      return isNewReDesignProductSummary ? (
        <Container
          isNewReDesignProductSummary={isNewReDesignProductSummary}
          isFromBundleProductView={isFromBundleProductView}
        >
          {!isSBP && getProductNameAndTopbadge(productName, badge1Value)}
          <ProductSummaryPrice>
            {this.renderOfferPrice()}
            <ProductSummaryPriceDiscount>
              {!isGiftCard && this.renderListPrice(badge2)}
            </ProductSummaryPriceDiscount>
          </ProductSummaryPrice>
          {!isGiftCard && !isBundleProduct && (
            <AfterPayMessaging offerPrice={afterPayMessagingPrice} />
          )}
          <ProductSummaryRatingAndMessage>
            {!isFromBundleProductView && (
              <ProductSummaryRating
                accessibilityRole="link"
                onPress={() => parentExpandRatingReview()}
              >
                <ProductRating
                  isNewReDesignProductTile={isNewReDesignProductSummary}
                  ratings={unbxdRatings}
                  reviewsCount={unbxdReviewsCount}
                />
              </ProductSummaryRating>
            )}
            <ProductSummaryPromotionalMessage>
              {!isStyleWith && !keepAlive && this.renderPromotionalMessage()}
            </ProductSummaryPromotionalMessage>
          </ProductSummaryRatingAndMessage>
        </Container>
      ) : (
        <Container>
          {!fromSkeleton && !isStyleWith && !isSBP && (
            <LineComp marginTop={10} borderColor="gray.500" />
          )}
          {this.renderProductRating(bazaarVoice)}
          {!isGiftCard && !fromSkeleton && !isStyleWith ? (
            <LineComp marginTop={0} borderColor="gray.500" />
          ) : null}
          {!isStyleWith && show && !fromSkeleton && this.renderTopBadge(badge1Value)}
          {!isStyleWith && this.renderSoldOutError()}
          {!isStyleWith && show && !fromSkeleton && (
            <BodyCopy
              dataLocator="pdp_product_titles"
              margin={badge1Value !== '' ? '0px' : '16px 0 0 0'}
              fontFamily="secondary"
              fontSize="fs18"
              fontWeight="extrabold"
              color="gray.900"
              text={productName}
              numberOfLines={2}
            />
          )}
          {this.renderOfferPrice()}
          {!isGiftCard && this.renderListPrice(badge2)}
          {!isGiftCard && !isBundleProduct && (
            <AfterPayWrapper>
              <AfterPayMessaging offerPrice={offerPricePickValue} />
            </AfterPayWrapper>
          )}
          {!!ratings && isStyleWith && (
            <ProductRating ratings={ratings} reviewsCount={reviewsCount} />
          )}
          {!isStyleWith && this.renderPromotionalMessage()}
          {!fromSkeleton && !isStyleWith && <LineComp marginTop={12} borderColor="gray.500" />}
        </Container>
      );
    }
    return null;
  }
}

ProductSummary.propTypes = {
  productData: PropTypes.shape({}).isRequired,
  selectedColorProductId: PropTypes.number.isRequired,
  isPlcc: PropTypes.bool,
  isGiftCard: PropTypes.bool,
  currencyExchange: PropTypes.arrayOf(PropTypes.shape({})),
  currencySymbol: PropTypes.string,
  showCompleteTheLook: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  accessibilityLabels: PropTypes.shape({}),
  isBundleProduct: PropTypes.bool,
  keepAlive: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({
    itemSoldOutMessage: PropTypes.string,
  }),
  scrollToTarget: PropTypes.func,
  parentExpandRatingReview: PropTypes.func,
  renderRatingReview: PropTypes.bool,
  offerPrice: PropTypes.number.isRequired,
  showPriceRange: PropTypes.bool,
  selectedSize: PropTypes.string,
  fromSkeleton: PropTypes.bool,
  highOfferPrice: PropTypes.number,
  highListPrice: PropTypes.number,
  listPrice: PropTypes.number,
  selectedMultipack: PropTypes.string,
  TCPMultipackProductMapping: PropTypes.shape([]),
  isPerUnitPriceEnabled: PropTypes.bool,
  isBrierleyPromoEnabled: PropTypes.bool,
  isStyleWith: PropTypes.bool,
  isSBP: PropTypes.bool,
  navigation: PropTypes.shape({}).isRequired,
  isNewReDesignProductSummary: PropTypes.bool,
  stylisticsProductTabList: PropTypes.shape({}),
  getProductNameAndTopbadge: PropTypes.func,
  isFromBundleProductView: PropTypes.bool,
};

ProductSummary.defaultProps = {
  isPlcc: false,
  currencyExchange: 1,
  currencySymbol: '$',
  isGiftCard: false,
  showCompleteTheLook: false,
  pdpLabels: {},
  accessibilityLabels: {},
  isBundleProduct: false,
  keepAlive: false,
  outOfStockLabels: {
    itemSoldOutMessage: '',
  },
  scrollToTarget: () => {},
  renderRatingReview: false,
  parentExpandRatingReview: () => {},
  showPriceRange: false,
  selectedSize: '',
  fromSkeleton: false,
  highOfferPrice: null,
  highListPrice: null,
  listPrice: null,
  selectedMultipack: '1',
  TCPMultipackProductMapping: [],
  isPerUnitPriceEnabled: false,
  isBrierleyPromoEnabled: false,
  isStyleWith: false,
  isSBP: false,
  isNewReDesignProductSummary: false,
  stylisticsProductTabList: {},
  getProductNameAndTopbadge: () => {},
  isFromBundleProductView: false,
};

export default withStyles(ProductSummary, styles);
export { ProductSummary as ProductSummaryVanilla };
