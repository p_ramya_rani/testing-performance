// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import ExecutionEnvironment from 'exenv';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import { breakpoints } from '../../../../../../../../styles/themes/TCP/mediaQuery';
import ProductImages from '../../../../../../common/organisms/ProductImages';
import FullSizeImageWithQuickViewModal from '../../FullSizeImageWithQuickViewModal/views/FullSizeImageWithQuickViewModal.view';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../../../ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';

class ProductImageWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFullSizeModalOpen: false,
    };
    this.handleShowHideFullSizeModalClick = this.handleShowHideFullSizeModalClick.bind(this);
  }

  colorChange = (e) => {
    // const { selectedSize } = this.state;
    const { onChangeColor, initialValues } = this.props;
    const { Fit, Quantity, Size } = initialValues;
    if (onChangeColor) {
      onChangeColor(e, Size, Fit, Quantity);
    }
  };

  handleShowHideFullSizeModalClick(e) {
    e.preventDefault();
    const { isFullSizeModalOpen } = this.state;
    this.setState({ isFullSizeModalOpen: !isFullSizeModalOpen });
  }

  render() {
    const {
      productName,
      images,
      isZoomEnabled,
      isThumbnailListVisible,
      pdpLabels,
      currentProduct,
      currentColorEntry,
      isGiftCard,
      keepAlive,
      outOfStockLabels,
      accessibilityLabels,
      ratingsProductId,
      completeLookSlotTile,
      itemPartNumber,
      fromSkeleton,
      imgConfigVal,
      isCompleteTheLookTestEnabled,
      isStyleWith,
      checkForOOSForVariant,
      largeImageNameOnHover,
      imagesByColor,
      hasABTestForPDPColorOrImageSwatchHover,
      TCPStyleQTY,
      TCPStyleType,
      isDynamicBadgeEnabled,
      fromPLPPage,
      isPDPSmoothScrollEnabled,
      isNewQVEnabled,
      isNewPDPEnabled,
      onAddItemToFavorites,
      productInfo,
      selectedColorProductId,
      productMiscInfo,
      pageName,
      skuId,
      formName,
      deleteFavItemInProgressFlag,
      onSetLastDeletedItemIdAction,
      isLoggedIn,
      wishList,
      primaryBrand,
      isInWishList,
      alternateBrand,
      socialProofMessage,
      isPDP,
    } = this.props;
    const { isFullSizeModalOpen } = this.state;
    const { colorFitsSizesMap } = currentProduct;
    const colorChipSelector = {
      colorList: colorFitsSizesMap,
      selectColor: this.colorChange,
    };
    const initialValuesForm = {
      colorSwatchModal: {
        name: currentColorEntry && currentColorEntry.color && currentColorEntry.color.name,
      },
    };

    const isMobile =
      ExecutionEnvironment.canUseDOM && document.body.offsetWidth < breakpoints.values.sm;
    return (
      <React.Fragment>
        {images && images.length > 0 ? (
          <ProductImages
            productName={productName}
            isThumbnailListVisible={isThumbnailListVisible && !isGiftCard}
            isGiftCard={isGiftCard}
            images={images}
            isMobile={isMobile}
            isZoomEnabled={!keepAlive && isZoomEnabled}
            onCloseClick={this.handleShowHideFullSizeModalClick}
            isFullSizeModalOpen={isFullSizeModalOpen}
            pdpLabels={pdpLabels}
            keepAlive={keepAlive}
            outOfStockLabels={outOfStockLabels}
            accessibilityLabels={accessibilityLabels}
            ratingsProductId={ratingsProductId}
            completeLookSlotTile={completeLookSlotTile}
            itemPartNumber={itemPartNumber}
            fromSkeleton={fromSkeleton}
            imgConfigVal={imgConfigVal}
            isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
            isStyleWith={isStyleWith}
            checkForOOSForVariant={checkForOOSForVariant}
            largeImageNameOnHover={largeImageNameOnHover}
            imagesByColor={imagesByColor}
            hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
            TCPStyleQTY={TCPStyleQTY}
            TCPStyleType={TCPStyleType}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            fromPLPPage={fromPLPPage}
            isNewQVEnabled={isNewQVEnabled}
            isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
            isNewPDPEnabled={isNewPDPEnabled}
            onAddItemToFavorites={onAddItemToFavorites}
            productInfo={productInfo}
            selectedColorProductId={selectedColorProductId}
            productMiscInfo={productMiscInfo}
            pageName={pageName}
            skuId={skuId}
            formName={formName}
            deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            isLoggedIn={isLoggedIn}
            isInWishList={isInWishList}
            wishList={wishList}
            primaryBrand={primaryBrand}
            alternateBrand={alternateBrand}
            socialProofMessage={socialProofMessage}
            isPDP={isPDP}
          />
        ) : null}
        {isFullSizeModalOpen &&
          (isMobile ? null : (
            <FullSizeImageWithQuickViewModal
              onCloseClick={this.handleShowHideFullSizeModalClick}
              isGiftCard={isGiftCard}
              images={images}
              isMobile={isMobile}
              name={productName}
              isThumbnailListVisible
              isFullSizeModalOpen={isFullSizeModalOpen}
              colorChipSelector={colorChipSelector}
              initialValues={initialValuesForm}
              accessibilityLabels={accessibilityLabels}
              ratingsProductId={ratingsProductId}
              currentProduct={currentProduct}
              largeImageNameOnHover={largeImageNameOnHover}
              imagesByColor={imagesByColor}
              hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
              TCPStyleQTY={TCPStyleQTY}
              TCPStyleType={TCPStyleType}
              isDynamicBadgeEnabled={isDynamicBadgeEnabled}
              isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
              primaryBrand={primaryBrand}
              alternateBrand={alternateBrand}
              isPDP={isPDP}
            />
          ))}
      </React.Fragment>
    );
  }
}

ProductImageWrapper.defaultProps = {
  onChangeColor: () => {},
  currentProduct: {},
  currentColorEntry: {},
  initialValues: {},
  keepAlive: false,
  outOfStockLabels: {
    outOfStockCaps: '',
  },
  accessibilityLabels: {},
  completeLookSlotTile: {},
  itemPartNumber: '',
  imgConfigVal: [],
  isCompleteTheLookTestEnabled: false,
  checkForOOSForVariant: false,
  isStyleWith: false,
  largeImageNameOnHover: '',
  imagesByColor: {},
  hasABTestForPDPColorOrImageSwatchHover: false,
  TCPStyleQTY: 0,
  TCPStyleType: '',
  isDynamicBadgeEnabled: false,
  fromPLPPage: false,
  isNewQVEnabled: false,
  isPDPSmoothScrollEnabled: false,
  isNewPDPEnabled: false,
  onAddItemToFavorites: () => {},
  productInfo: {},
  selectedColorProductId: '',
  productMiscInfo: {
    isInDefaultWishlist: false,
  },
  pageName: '',
  skuId: '',
  formName: PRODUCT_ADD_TO_BAG,
  deleteFavItemInProgressFlag: false,
  onSetLastDeletedItemIdAction: () => {},
  isLoggedIn: false,
  wishList: {},
  isInWishList: false,
};

ProductImageWrapper.propTypes = {
  /** Product's Name (global product, not by color, size, fit or some clasification) */
  productName: PropTypes.string.isRequired,

  /** list of images of the product */
  images: PropTypes.arrayOf(
    PropTypes.shape({
      iconSizeImageUrl: PropTypes.string.isRequired,
      regularSizeImageUrl: PropTypes.string.isRequired,
      bigSizeImageUrl: PropTypes.string.isRequired,
      superSizeImageUrl: PropTypes.string.isRequired,
      fromSkeleton: PropTypes.bool.isRequired,
    })
  ).isRequired,
  pdpLabels: PropTypes.shape({
    fullSize: PropTypes.string.isRequired,
  }).isRequired,

  /**
   * Flags if we should show big size images, instead of regular size
   * images (default behavior)
   */

  /** Flags if the zoom should be enabled */
  isZoomEnabled: PropTypes.bool.isRequired,
  isThumbnailListVisible: PropTypes.bool.isRequired,
  onChangeColor: PropTypes.func,
  currentProduct: PRODUCT_INFO_PROP_TYPE_SHAPE,
  currentColorEntry: PropTypes.shape({}),
  isGiftCard: PropTypes.bool.isRequired,
  initialValues: PropTypes.shape({}),
  keepAlive: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({
    outOfStockCaps: PropTypes.string,
  }),
  accessibilityLabels: PropTypes.shape({}),
  ratingsProductId: PropTypes.string.isRequired,
  completeLookSlotTile: PropTypes.shape({}),
  itemPartNumber: PropTypes.string,
  fromSkeleton: PropTypes.bool.isRequired,
  imgConfigVal: PropTypes.shape([]),
  isCompleteTheLookTestEnabled: PropTypes.bool,
  checkForOOSForVariant: PropTypes.bool,
  isStyleWith: PropTypes.bool,
  largeImageNameOnHover: PropTypes.string,
  imagesByColor: PropTypes.shape({}),
  hasABTestForPDPColorOrImageSwatchHover: PropTypes.bool,
  TCPStyleQTY: PropTypes.number,
  TCPStyleType: PropTypes.string,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  fromPLPPage: PropTypes.bool,
  isNewQVEnabled: PropTypes.bool,
  isPDPSmoothScrollEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  onAddItemToFavorites: PropTypes.func,
  productInfo: PropTypes.shape({}),
  selectedColorProductId: PropTypes.string,
  productMiscInfo: PropTypes.shape({
    isInDefaultWishlist: PropTypes.bool,
  }),
  pageName: PropTypes.string,
  skuId: PropTypes.string,
  formName: PropTypes.string,
  deleteFavItemInProgressFlag: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  wishList: PropTypes.shape({}),
  isLoggedIn: PropTypes.bool,
  isInWishList: PropTypes.bool,
};

export default errorBoundary(ProductImageWrapper);
