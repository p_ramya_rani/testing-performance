// 9fbef606107a605d69c0edbcd8029e5d
/** @module ProductRating
 */

import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import {
  CustomRatingContainer,
  RatingStarOverlay,
  RatingImage,
  RatingStarOverlayGray,
  ReviewCountText,
} from './styles/ProductRating.style.native';

const starsRatingWhite = require('../../../../../../../../mobileapp/src/assets/images/stars-rating.png');

const ProductRating = ({
  ratings,
  reviewsCount,
  isPlaceCashReward,
  inGridPlpRecommendation,
  isNewReDesignProductTile,
}) => {
  const ratingBar = (ratings * 100) / 5;
  const ratingCount = isNewReDesignProductTile ? reviewsCount : `(${reviewsCount})`;
  return (
    <CustomRatingContainer
      inGridPlpRecommendation={inGridPlpRecommendation}
      isNewReDesignProductTile={isNewReDesignProductTile}
    >
      <View>
        <RatingStarOverlayGray>
          <RatingImage source={starsRatingWhite} isPlaceCashReward={isPlaceCashReward} />
        </RatingStarOverlayGray>
        <RatingStarOverlay width={`${ratingBar}%`}>
          <RatingImage source={starsRatingWhite} isPlaceCashReward={isPlaceCashReward} />
        </RatingStarOverlay>
      </View>
      {reviewsCount > 0 && (
        <ReviewCountText
          isPlaceCashReward={isPlaceCashReward}
          isNewReDesignProductTile={isNewReDesignProductTile}
        >
          {ratingCount}
        </ReviewCountText>
      )}
    </CustomRatingContainer>
  );
};

ProductRating.propTypes = {
  ratings: PropTypes.string.isRequired,
  reviewsCount: PropTypes.string.isRequired,
  isPlaceCashReward: PropTypes.bool,
  inGridPlpRecommendation: PropTypes.bool,
  isNewReDesignProductTile: PropTypes.bool,
};

ProductRating.defaultProps = {
  isPlaceCashReward: false,
  inGridPlpRecommendation: false,
  isNewReDesignProductTile: false,
};

export default ProductRating;
