// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import SocialProofMessage from '../views/SocialProofMessage.view.native';

describe('SocialProofMessageVanilla', () => {
  let component;
  const props = {
    socialProofMessage: {
      priorityMsg: '$Urgent!$ - This is a test message',
    },
  };

  beforeEach(() => {
    component = shallow(<SocialProofMessage {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should return styled BodyCopy component value six', () => {
    expect(component.find('Styled(BodyCopy)')).toHaveLength(2);
  });
});
