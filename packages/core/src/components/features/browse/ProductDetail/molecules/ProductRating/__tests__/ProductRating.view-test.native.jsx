// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ProductRating from '../ProductRating.view.native';

describe('ProductRating should render', () => {
  let shallowComponent;
  const props = {
    ratings: 4.6,
    reviews: 34,
  };

  beforeEach(() => {
    shallowComponent = shallow(<ProductRating {...props} />);
  });

  it('Component should be defined', () => {
    expect(shallowComponent).toBeDefined();
  });

  it('should render correctly', () => {
    expect(shallowComponent).toMatchSnapshot();
  });
});
