// 9fbef606107a605d69c0edbcd8029e5d
import getPrefixId from '../views/ProductDescription.view.util';

jest.mock('../../../../../../../utils', () => ({
  isCanada: () => false,
}));

const getMultiPackAllColor = [
  {
    TCPMultiPackUSStore: false,
    name: 'TIDAL',
    variants: [],
    styleTypeUS: '0002',
    styleTypeCA: '0002',
    prodpartno: '3011187_IV',
    TCPMultiPackReferenceUSStore: false,
  },
  {
    TCPMultiPackUSStore: false,
    name: 'MULTI CLR',
    variants: [],
    styleTypeUS: '0005',
    styleTypeCA: '0005',
    prodpartno: '3011187_BQ',
    TCPMultiPackReferenceUSStore: false,
  },
  {
    TCPMultiPackUSStore: '1',
    name: 'RUBY',
    variants: [],
    styleTypeUS: '0003',
    styleTypeCA: '0003',
    prodpartno: '3011187_6B',
    TCPMultiPackReferenceUSStore: '2044391_6B#2',
  },
  {
    TCPMultiPackUSStore: '1',
    name: 'RUBY',
    variants: [],
    styleTypeUS: '0006',
    styleTypeCA: '0006',
    prodpartno: '3011187_NM',
    TCPMultiPackReferenceUSStore: false,
  },
];

describe('Product Description Util', () => {
  it('should render item for single product', () => {
    expect(getPrefixId(getMultiPackAllColor, '3011187_IV', 'Item #: ')).toEqual('Item #: ');
  });

  it('should render item for multi style 0005 product', () => {
    expect(getPrefixId(getMultiPackAllColor, '3011187_BQ', 'Item #: ')).toEqual('V-Item #: ');
  });

  it('should render item for multi style 0002 product', () => {
    expect(getPrefixId(getMultiPackAllColor, '3011187_6B', 'Item #: ')).toEqual('PV Set-Item #: ');
  });
  it('should render item for multi style 0006 product', () => {
    expect(getPrefixId(getMultiPackAllColor, '3011187_NM', 'Item #: ')).toEqual('V Set-Item #: ');
  });
});
