/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d

import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { FlatList, Text, Dimensions, Share, Animated } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FastImage from '@stevenmasini/react-native-fast-image';
import { withTheme } from 'styled-components/native';
import PaginationDots from '@tcp/core/src/components/common/molecules/PaginationDots';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { getVideoUrl, getAPIConfig } from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import { trackForterAction, ForterActionType } from '@tcp/core/src/utils/forter.util';
import imgConfigObj from '@tcp/core/src/components/common/molecules/ProductDetailImage/config';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import {
  Container,
  FavoriteAndPaginationContainer,
  FavoriteAndShareContainer,
  FavoriteContainer,
  DownloadContainer,
  ImageTouchableOpacity,
  styles,
  EmptyView,
  OosText,
  ImageCarouselStyle,
  ImageCarouselStyleNew,
  CompleteTheLookContainer,
  CTLIconContainer,
  CTLTextContainer,
  CTLTouchableOpacty,
} from '../styles/ImageCarousel.style.native';
import CustomIcon from '../../../../../../common/atoms/Icon';
import { ICON_NAME, ICON_FONT_CLASS } from '../../../../../../common/atoms/Icon/Icon.constants';
import { DamImage } from '../../../../../../common/atoms';
import OpenLoginModal from '../../../../../account/LoginPage/container/LoginModal.container';
import OutOfStockWaterMark from '../../OutOfStockWaterMark';
import Image from '../../../../../../common/atoms/Image';
import ModuleQConstant from '../../../../../../common/molecules/ModuleQ/ModuleQ.constant';

const favorites = require('../../../../../../../../../mobileapp/src/assets/images/favorites.png');
const favoriteSolid = require('../../../../../../../../../mobileapp/src/assets/images/favorites_solid.png');
const ctlIcon = require('../../../../../../../../../mobileapp/src/assets/images/icons_standard_clothes_3x.png');

const win = Dimensions.get('window');
const paddingAroundImage = 24;
const imageWidth = win.width - paddingAroundImage;
/*
  I calculate percentage height of the image based on the provide zeplin - https://zpl.io/bAWKrgq, https://zpl.io/bPg0eqA
  formula - imageHeight * 100 / screenHeight i.e 427 * 100 / (805) = 53
*/
const imagePercentage = 53;
let imageHeight = (imagePercentage * win.height) / 100;
imageHeight = imageHeight >= 427 ? imageHeight : 427;
class ImageCarousel extends React.PureComponent {
  favoriteIconColor;

  favoriteIconSize;

  constructor(props) {
    super(props);
    this.state = {
      activeSlideIndex: 0,
      showModal: false,
      generalProductId: '',
      favoritedCount:
        props.currentColorEntry && props.currentColorEntry.favoritedCount
          ? props.currentColorEntry.favoritedCount
          : 0,
    };
    const { theme } = props;
    this.favoriteIconColor = get(theme, 'colorPalette.gray[600]', '#9b9b9b');
    this.favoriteIconSize = get(theme, 'typography.fontSizes.fs25', 25);

    this.animateValue = new Animated.Value(0);
    this.ctlWidth = this.animateValue.interpolate({
      inputRange: [0, 1],
      outputRange: [204, 44],
    });
    this.ctlTextWidth = this.animateValue.interpolate({
      inputRange: [0, 1],
      outputRange: [150, 0],
    });
    this.ctlTextOpacity = this.animateValue.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0],
    });
  }

  componentDidMount() {
    const { isNewReDesignEnabled } = this.props;
    if (isNewReDesignEnabled) this.animateCTL();
  }

  componentWillUnmount() {
    const { removeAddToFavoritesErrorMsg } = this.props;
    if (typeof removeAddToFavoritesErrorMsg === 'function') {
      removeAddToFavoritesErrorMsg('');
    }
  }

  // this method set current visible image
  setActiveSlideIndex = (index) => {
    const { activeSlideIndex } = this.state;
    if (index !== activeSlideIndex) {
      this.setState({
        activeSlideIndex: index,
      });
    }
  };

  // this method when swipe image and return changed view
  onViewableItemsChanged = ({ changed }) => {
    const len = (changed && changed.length) || 0;
    for (let i = 0; i < len; i += 1) {
      const item = changed[i];
      const { isViewable, index } = item;
      if (isViewable) {
        this.setActiveSlideIndex(index);
        break;
      }
    }
  };

  static getDerivedStateFromProps(props, state) {
    const { onAddItemToFavorites, skuId, currentColorEntry, isLoggedIn, formName, currentProduct } =
      props;
    const { colorProductId } = currentColorEntry;
    const { generalProductId, showModal } = state;
    if (isLoggedIn && showModal) {
      if (generalProductId !== '') {
        onAddItemToFavorites({
          colorProductId: generalProductId,
          productSkuId: (skuId && skuId.skuId) || null,
          pdpColorProductId: colorProductId,
          formName,
          page: 'PDP',
          products: currentProduct,
        });
      }
      return { showModal: false };
    }
    return null;
  }

  animateCTL = () => {
    this.animateValue.setValue(0);
    Animated.timing(this.animateValue, {
      toValue: 1,
      duration: 750,
      delay: 5000,
    }).start();
  };

  // this method call when tap on the pagination dots and navigate to clicked image
  onPageChange = (dotClickedIndex) => {
    const { imageUrls } = this.props;
    if (this.flatListRef && dotClickedIndex < imageUrls.length) {
      this.flatListRef.scrollToIndex({ animated: true, index: dotClickedIndex });
    }
  };

  onFavorite = (generalProductId, activeWishListId) => {
    const { deleteFavItemInProgressFlag } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const {
      isLoggedIn,
      onAddItemToFavorites,
      skuId,
      currentColorEntry,
      formName,
      currentProduct,
      isSBP,
    } = this.props;
    const { favoritedCount } = this.state;
    const { colorProductId } = currentColorEntry;

    if (!isLoggedIn) {
      this.setState({ generalProductId });
      this.setState({ showModal: true });
    } else {
      onAddItemToFavorites({
        colorProductId: generalProductId,
        productSkuId: (skuId && skuId.skuId) || null,
        pdpColorProductId: colorProductId,
        formName,
        page: 'PDP',
        products: currentProduct,
        activeWishListId: isSBP ? activeWishListId : undefined,
        loading: isSBP,
      });
      this.setState({ favoritedCount: favoritedCount + 1 });
    }
  };

  onFavoriteRemove = (generalProductId) => {
    const { deleteFavItemInProgressFlag } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const { defaultWishListFromState, onSetLastDeletedItemIdAction } = this.props;
    const { favoritedCount } = this.state;
    if (defaultWishListFromState && defaultWishListFromState[generalProductId]) {
      const { externalIdentifier, giftListItemID } =
        defaultWishListFromState && defaultWishListFromState[generalProductId];
      onSetLastDeletedItemIdAction({
        externalId: externalIdentifier,
        itemId: giftListItemID,
        notFromFavoritePage: true,
      });
      if (favoritedCount > 0) this.setState({ favoritedCount: favoritedCount - 1 });
    }
  };

  sendToggleActionBackToPdpForNewDesign = () => {
    const { hideLoginModal, isNewReDesignEnabled } = this.props;
    if (isNewReDesignEnabled) return hideLoginModal();
    return null;
  };

  toggleModal = () => {
    this.setState(
      (state) => ({
        showModal: !state.showModal,
      }),
      this.sendToggleActionBackToPdpForNewDesign
    );
  };

  onShare = async () => {
    const { navigation } = this.props;
    const { webAppDomain } = getAPIConfig();
    const pdpUrl = (navigation && navigation.getParam('pdpUrl')) || '';
    const shareUrl = `${webAppDomain}/us/p/${pdpUrl}`;
    try {
      trackForterAction(ForterActionType.SHARE, 'PDP_ITEM_SHARE');
      await Share.share({
        message: shareUrl,
      });
    } catch (error) {
      logger.error(error);
    }
  };

  renderComponent = ({ showModal, isLoggedIn }) => {
    let componentContainer = null;
    if (!isLoggedIn) {
      componentContainer = (
        <OpenLoginModal
          component="login"
          openState={showModal}
          setLoginModalMountState={this.toggleModal}
          isUserLoggedIn={isLoggedIn}
          variation="favorites"
        />
      );
    }
    return <React.Fragment>{componentContainer}</React.Fragment>;
  };

  renderOutOfStockOverlay = () => {
    const { keepAlive, outOfStockLabels, checkForOOSForVariant, isNewReDesignEnabled } = this.props;
    if (checkForOOSForVariant && !isNewReDesignEnabled)
      return (
        <OutOfStockWaterMark
          label={outOfStockLabels.outOfStockCaps}
          isNewReDesignEnabled={isNewReDesignEnabled}
          fontSize="fs32"
        >
          <OosText>{outOfStockLabels.outOfStockCaps}</OosText>
        </OutOfStockWaterMark>
      );
    return keepAlive ? (
      <OutOfStockWaterMark
        label={outOfStockLabels.outOfStockCaps}
        isNewReDesignEnabled={isNewReDesignEnabled}
        fontSize={isNewReDesignEnabled ? 'fs18' : 'fs24'}
      />
    ) : null;
  };

  renderNormalImage = (imgSource) => {
    const {
      onImageClick,
      imgConfig,
      parentActiveIndex,
      currentProduct,
      videoConfig,
      isPDPImage,
      dynamicBadgeOverlayQty,
      tcpStyleType,
      bottomSheetHeight,
      isNewReDesignEnabled,
      primaryBrand,
      alternateBrand,
    } = this.props;
    const { activeSlideIndex } = this.state;
    const { index } = imgSource;
    const isVideoUrl = getVideoUrl(imgSource.item.regularSizeImageUrl);
    const imageConfig = isPDPImage && isVideoUrl ? videoConfig : imgConfig;
    parentActiveIndex(activeSlideIndex);

    const Touchable = isNewReDesignEnabled ? TouchableOpacity : ImageTouchableOpacity;

    return (
      <Touchable
        onPress={!isVideoUrl ? onImageClick : null}
        accessible={index === activeSlideIndex}
        accessibilityRole="image"
        accessibilityLabel={`${currentProduct && currentProduct.name} product image ${index + 1}`}
      >
        <DamImage
          key={index.toString()}
          url={imgSource.item.regularSizeImageUrl}
          isProductImage
          width={imageWidth}
          height={
            isNewReDesignEnabled && bottomSheetHeight
              ? win.height - bottomSheetHeight - 100
              : imageHeight
          }
          imgConfig={imageConfig}
          resizeMode="contain"
          dynamicBadgeOverlayQty={index === 0 ? dynamicBadgeOverlayQty : 0}
          tcpStyleType={tcpStyleType}
          badgeConfig={imgConfigObj.IMG_DATA.badgeConfig}
          isNewReDesignEnabled={isNewReDesignEnabled}
          primaryBrand={primaryBrand || alternateBrand}
        />
        {this.renderOutOfStockOverlay()}
      </Touchable>
    );
  };

  renderSelectedHeartIcon = (generalProductId) => {
    return (
      <ImageTouchableOpacity
        accessibilityRole="button"
        accessibilityLabel="favorite"
        accessibilityState={{ selected: true }}
        onPress={() => {
          this.onFavoriteRemove(generalProductId);
        }}
      >
        <Image source={favoriteSolid} height={16} width={16} dataLocator="pdp_favorite_icon" />
      </ImageTouchableOpacity>
    );
  };

  renderUnselectedHeartIcon = (generalProductId, activeWishListId, isSBP) => {
    let imageProp = {
      height: 16,
      width: 16,
    };
    if (isSBP) {
      imageProp = {
        height: 24,
        width: 24,
      };
    }
    return (
      <ImageTouchableOpacity
        accessibilityRole="button"
        accessibilityLabel="set as favorite"
        accessibilityState={{ selected: false }}
        onPress={() => {
          this.onFavorite(generalProductId, activeWishListId);
        }}
      >
        {/* SBP MERGE JIRA SBP-902 STYLING */}
        <Image source={favorites} {...imageProp} dataLocator="pdp_favorite_icon" />
      </ImageTouchableOpacity>
    );
  };

  renderFavoriteCount = (isSBP) => {
    const { favoritedCount } = this.state;
    const margin = '0 0 0 8px';
    const color = 'gray.600';
    /* SBP MERGE JIRA SBP-902 STYLING */
    return (
      <BodyCopy
        dataLocator="pdp_favorite_icon_count"
        margin={isSBP ? '0 0 0 2px' : margin}
        mobileFontFamily={isSBP ? 'Nunito-Regular' : 'secondary'}
        fontSize="fs10"
        fontWeight="regular"
        color={isSBP ? 'gray.900' : color}
        text={favoritedCount}
      />
    );
  };

  renderFavoriteIcon = (isSBP) => {
    const {
      currentColorEntry,
      isBundleProduct,
      currentProduct,
      defaultWishListFromState,
      activeWishListProducts = [],
      activeWishListId,
      isFavorite: favorite,
    } = this.props;
    const { miscInfo } = currentColorEntry;
    let { isFavorite = false } = currentColorEntry;
    if (isSBP) {
      isFavorite = favorite;
      miscInfo.isInDefaultWishlist = false;
    }
    const { generalProductId } = currentProduct;
    let wishList = {};
    if (isSBP) {
      activeWishListProducts.forEach((product) => {
        wishList[product.skuInfo.colorProductId] = { isInDefaultWishlist: false };
      });
    } else wishList = defaultWishListFromState;
    if (!isBundleProduct) {
      return (
        <FavoriteContainer isSBP={isSBP}>
          {(isFavorite === true && (defaultWishListFromState.isInDefaultWishlist || isSBP)) ||
          (wishList && wishList[generalProductId])
            ? this.renderSelectedHeartIcon(generalProductId)
            : this.renderUnselectedHeartIcon(generalProductId, activeWishListId, isSBP)}
          {this.renderFavoriteCount(isSBP)}
        </FavoriteContainer>
      );
    }
    return <EmptyView />;
  };

  renderProductImages = (imageUrls) => {
    const { primaryBrand, alternateBrand } = this.props;
    return (
      <FlatList
        ref={(ref) => {
          this.flatListRef = ref;
        }}
        onViewableItemsChanged={this.onViewableItemsChanged}
        viewabilityConfig={{
          itemVisiblePercentThreshold: 50,
        }}
        initialNumToRender={1}
        initialScrollIndex={0}
        refreshing={false}
        data={imageUrls}
        pagingEnabled
        horizontal
        showsHorizontalScrollIndicator={false}
        listKey={(_, index) => index.toString()}
        renderItem={this.renderNormalImage}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
      />
    );
  };

  renderPaginationDots = () => {
    const { imageUrls, isSBP, accessibilityLabels, isNewReDesignEnabled } = this.props;
    const { activeSlideIndex } = this.state;
    return (
      <PaginationDots
        numberOfDots={imageUrls.length}
        selectedIndex={activeSlideIndex}
        onPress={this.onPageChange}
        accessibilityLabels={accessibilityLabels}
        isSBP={isSBP}
        isNewReDesignEnabled={isNewReDesignEnabled}
      />
    );
  };

  renderFavoriteAndShare = () => {
    const { isSBP, isNewReDesignEnabled } = this.props;
    return (
      <FavoriteAndShareContainer>
        <DownloadContainer isNewReDesignEnabled={isNewReDesignEnabled}>
          {/* SBP MERGE JIRA SBP-902 STYLING */}
          {!isSBP && (
            <CustomIcon
              iconFontName={ICON_FONT_CLASS.Icomoon}
              name={ICON_NAME.iconShare}
              size="fs22"
              color="gray.1900"
              dataLocator="pdp_social_connect"
              onPress={this.onShare}
              title="Share"
              isButton
              style={
                isNewReDesignEnabled
                  ? ImageCarouselStyleNew.customIcon
                  : ImageCarouselStyle.customIcon
              }
              accessibilityLabel={ICON_NAME.iconShare}
            />
          )}
        </DownloadContainer>
        {!isSBP && this.renderFavoriteIcon()}
      </FavoriteAndShareContainer>
    );
  };

  renderFavoritePaginationContainer = () => {
    const { isSBP, imageUrls, isNewReDesignEnabled } = this.props;
    return (
      <FavoriteAndPaginationContainer
        style={isSBP ? ImageCarouselStyle.favoriteAndPaginationContainer : null}
        isNewReDesignEnabled={isNewReDesignEnabled}
      >
        {!isSBP && !isNewReDesignEnabled ? this.renderFavoriteIcon() : null}
        {imageUrls.length > 1 && this.renderPaginationDots()}
        <DownloadContainer>
          {/* SBP MERGE JIRA SBP-902 STYLING */}
          {!isSBP && !isNewReDesignEnabled ? (
            <CustomIcon
              iconFontName={ICON_FONT_CLASS.Icomoon}
              name={ICON_NAME.iconShare}
              size="fs18"
              color="gray.1600"
              dataLocator="pdp_social_connect"
              onPress={this.onShare}
              title="Share"
              isButton
              style={
                isNewReDesignEnabled
                  ? ImageCarouselStyleNew.customIcon
                  : ImageCarouselStyle.customIcon
              }
              accessibilityLabel={ICON_NAME.iconShare}
            />
          ) : null}
        </DownloadContainer>
      </FavoriteAndPaginationContainer>
    );
  };

  renderFavoriteShareProductImageAndPagination = () => {
    const { imageUrls, isGiftCard } = this.props;
    return (
      <>
        {this.renderProductImages(imageUrls)}
        {!isGiftCard ? this.renderFavoritePaginationContainer() : null}
      </>
    );
  };

  onCompleteTheLook = () => {
    const { isSBP, navigation, currentProduct, stylisticsProductTabList, fromPage } = this.props;
    const { productId, generalProductId } = currentProduct;
    const selectedProductId = stylisticsProductTabList[productId] ? productId : generalProductId;
    const productItem = stylisticsProductTabList[selectedProductId];
    const { subItemsId, id } = productItem?.[0] || {};
    const { RECOMMENDATION, COMPLETETHELOOK } = ModuleQConstant;

    navigation.navigate('OutfitDetail', {
      title: COMPLETETHELOOK,
      outfitId: id,
      vendorColorProductIdsList: subItemsId,
      viaModule: RECOMMENDATION,
      isSBP,
      isCTL: true,
      selectedProductId,
      fromPage,
    });
  };

  renderCTLIcon = () => {
    const imageStyle = {
      height: 24,
      width: 24,
    };
    return (
      <CTLIconContainer>
        <FastImage source={ctlIcon} style={imageStyle} dataLocator="pdp_ctl_icon" />
      </CTLIconContainer>
    );
  };

  renderCTLText = () => {
    const { pdpLabels } = this.props;
    const { completeTheLook } = pdpLabels;
    const AnimatedCTLTextContainer = Animated.createAnimatedComponent(CTLTextContainer);
    return (
      <AnimatedCTLTextContainer opacity={this.ctlTextOpacity} width={this.ctlTextWidth}>
        <BodyCopy
          dataLocator="pdp_product_titles"
          fontFamily="secondary"
          fontSize="fs15"
          fontWeight="regular"
          color="gray.800"
          text={completeTheLook}
          numberOfLines={1}
          textAlign="center"
        />
      </AnimatedCTLTextContainer>
    );
  };

  renderCTL = () => {
    const { showCompleteTheLook, isBundleProduct, isNewReDesignEnabled } = this.props;
    const AnimatedCTL = Animated.createAnimatedComponent(CompleteTheLookContainer);
    return (
      showCompleteTheLook &&
      !isBundleProduct &&
      isNewReDesignEnabled && (
        <AnimatedCTL width={this.ctlWidth}>
          <CTLTouchableOpacty
            accessibilityRole="button"
            accessibilityLabel="favorite"
            accessibilityState={{ selected: true }}
            onPress={this.onCompleteTheLook}
          >
            {this.renderCTLIcon()}
            {this.renderCTLText()}
          </CTLTouchableOpacty>
        </AnimatedCTL>
      )
    );
  };

  checkActiveSlideIndexAvailable = () => {
    const { activeSlideIndex } = this.state;
    const { imageUrls, isNewReDesignEnabled } = this.props;

    if (activeSlideIndex + 1 > imageUrls.length && isNewReDesignEnabled) {
      return true;
    }
    return false;
  };

  render() {
    const { imageUrls, isLoggedIn, isSBP, showLoginModal, isNewReDesignEnabled } = this.props;
    const { showModal } = this.state;
    let modal = showModal;

    if (this.checkActiveSlideIndexAvailable()) {
      this.setState({
        activeSlideIndex: 0,
      });
      return null;
    }

    if (isNewReDesignEnabled) modal = showLoginModal;
    if (imageUrls && imageUrls.length > 0) {
      return (
        <Container isSBP={isSBP}>
          {this.renderFavoriteShareProductImageAndPagination()}
          {this.renderCTL()}
          {!isSBP && (
            <FavoriteAndPaginationContainer>
              {modal &&
                this.renderComponent({
                  showModal: modal,
                  isLoggedIn,
                })}
            </FavoriteAndPaginationContainer>
          )}
        </Container>
      );
    }
    return <Text>Loading...</Text>;
  }
}

ImageCarousel.propTypes = {
  theme: PropTypes.shape({}),
  imageUrls: PropTypes.arrayOf(
    PropTypes.shape({
      item: PropTypes.shape({
        regularSizeImageUrl: PropTypes.string.isRequired,
      }),
    })
  ),
  onImageClick: PropTypes.func.isRequired,
  isGiftCard: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  currentProduct: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  onAddItemToFavorites: PropTypes.func,
  imgConfig: PropTypes.string,
  videoConfig: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  parentActiveIndex: PropTypes.func,
  currentColorEntry: PropTypes.string,
  isBundleProduct: PropTypes.bool,
  keepAlive: PropTypes.bool,
  skuId: PropTypes.string,
  outOfStockLabels: PropTypes.shape({
    outOfStockCaps: PropTypes.string,
  }),
  accessibilityLabels: PropTypes.shape({}),
  formName: PropTypes.string,
  defaultWishListFromState: PropTypes.shape({}),
  isPDPImage: PropTypes.bool,
  isSBP: PropTypes.bool,
  activeWishListProducts: PropTypes.shape([]),
  activeWishListId: PropTypes.string,
  isFavorite: PropTypes.bool,
  checkForOOSForVariant: PropTypes.bool,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  dynamicBadgeOverlayQty: PropTypes.number,
  tcpStyleType: PropTypes.string,
  isNewReDesignEnabled: PropTypes.bool,
  showLoginModal: PropTypes.bool,
  showCompleteTheLook: PropTypes.bool,
  pdpLabels: PropTypes.shape({}),
  stylisticsProductTabList: PropTypes.shape({}),
  hideLoginModal: PropTypes.func,
  bottomSheetHeight: PropTypes.number,
  isFastImage: PropTypes.bool,
  fromPage: PropTypes.string,
};

ImageCarousel.defaultProps = {
  theme: {},
  imageUrls: [],
  isGiftCard: false,
  isLoggedIn: false,
  currentProduct: {},
  navigation: {},
  onAddItemToFavorites: () => {},
  removeAddToFavoritesErrorMsg: () => {},
  parentActiveIndex: () => {},
  currentColorEntry: '',
  imgConfig: '',
  videoConfig: '',
  isBundleProduct: false,
  keepAlive: false,
  outOfStockLabels: {
    outOfStockCaps: '',
  },
  skuId: '',
  accessibilityLabels: {},
  formName: '',
  defaultWishListFromState: {},
  isPDPImage: false,
  isSBP: false,
  activeWishListProducts: [],
  activeWishListId: '',
  isFavorite: false,
  checkForOOSForVariant: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  dynamicBadgeOverlayQty: 0,
  tcpStyleType: '',
  isNewReDesignEnabled: false,
  showLoginModal: false,
  showCompleteTheLook: false,
  pdpLabels: {},
  stylisticsProductTabList: {},
  hideLoginModal: () => {},
  bottomSheetHeight: 295,
  isFastImage: true,
  fromPage: '',
};

export default withStyles(withTheme(ImageCarousel), styles);
export { ImageCarousel as ImageCarouselVanilla };
