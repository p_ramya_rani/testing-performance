// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { PropTypes } from 'prop-types';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import style from '../RelatedOutfits.style';
import { getLocator } from '../../../../../../../utils';
import ModuleQ from '../../../../../../common/molecules/ModuleQ';

class RelatedOutfits extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isAccordionOpen: true,
      showHeader: false,
    };
    this.handleAccordionToggle = this.handleAccordionToggle.bind(this);
  }

  getAccordionClass = (isAccordionOpen) => {
    return isAccordionOpen ? 'show-accordion-toggle' : '';
  };

  getRelatedOutfitSlots = () => {
    const {
      selectedColorProductId,
      getCompleteTheLookSlot,
      page,
      triggerStylyticsAnalytics,
      pdpLabels,
      partNumberAfterColorChange,
      isNewPDPEnabled,
    } = this.props;

    return (
      <ModuleQ
        selectedColorProductId={selectedColorProductId}
        hideTabs
        divTabs={[]}
        showRelatedOutfitHeader={this.setShowHeader}
        isRelatedOutfit
        getCompleteTheLookSlot={getCompleteTheLookSlot}
        page={page}
        triggerStylyticsAnalytics={triggerStylyticsAnalytics}
        pdpLabels={pdpLabels}
        partNumberAfterColorChange={partNumberAfterColorChange}
        isNewPDPEnabled={isNewPDPEnabled}
      />
    );
  };

  setShowHeader = (value) => {
    const { showHeader } = this.state;
    if (!showHeader) {
      this.setState({ showHeader: value });
    }
  };

  handleAccordionToggle() {
    const { isAccordionOpen } = this.state;
    this.setState({ isAccordionOpen: !isAccordionOpen });
  }

  render() {
    const { pdpLabels, className, selectedColorProductId, completeLookSlotTile, isNewPDPEnabled } =
      this.props;
    const { completeTheLook } = pdpLabels;
    const { isAccordionOpen, showHeader } = this.state;
    const accordionToggleClass = this.getAccordionClass(isAccordionOpen);
    const completeLookSlotTileKeysLength = Object.keys(completeLookSlotTile).length;
    const iscompleteLookSlotTileAvailable =
      completeLookSlotTileKeysLength &&
      completeLookSlotTile[selectedColorProductId] &&
      completeLookSlotTile[selectedColorProductId].length !== 0;

    return (
      <div
        className={`${className} product-description-list  ${
          showHeader ? 'complete-the-look-section' : ''
        } ${iscompleteLookSlotTileAvailable ? '' : 'hide-complete-look-header'} ${
          isNewPDPEnabled ? 'add-border' : ''
        }`}
      >
        {showHeader && (
          <BodyCopy
            className={`product-desc-heading ${accordionToggleClass}`}
            fontSize="fs14"
            component="div"
            fontFamily="secondary"
            fontWeight="black"
            onClick={this.handleAccordionToggle}
            data-locator={getLocator('pdp_anchor_complete_the_look')}
          >
            {completeTheLook.toUpperCase()}
          </BodyCopy>
        )}
        {isAccordionOpen && this.getRelatedOutfitSlots()}
      </div>
    );
  }
}

RelatedOutfits.propTypes = {
  className: PropTypes.string,
  pdpLabels: PropTypes.shape({}),
  selectedColorProductId: PropTypes.string.isRequired,
  getCompleteTheLookSlot: PropTypes.func.isRequired,
  page: PropTypes.string,
  triggerStylyticsAnalytics: PropTypes.func.isRequired,
  partNumberAfterColorChange: PropTypes.string,
  completeLookSlotTile: PropTypes.shape({}),
  isNewPDPEnabled: PropTypes.bool,
};

RelatedOutfits.defaultProps = {
  className: '',
  pdpLabels: {},
  page: '',
  partNumberAfterColorChange: '',
  completeLookSlotTile: {},
  isNewPDPEnabled: false,
};

export default errorBoundary(withStyles(RelatedOutfits, style));
export { RelatedOutfits as RelatedOutfitsVanilla };
