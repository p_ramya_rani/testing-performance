// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '../../../../../../utils/index';

const downArrowIcon = getIconPath('down_arrow_icon');
const upArrowIcon = getIconPath('up_arrow_icon');

export default css`
  &.complete-the-look-section {
    ${(props) =>
      props.isPDP
        ? `
        border-top: ${props.theme.spacing.ELEM_SPACING.XS} solid
        ${props.theme.colorPalette.gray[300]};
        @media ${props.theme.mediaQuery.large} {
          border-top: 1px solid
        ${props.theme.colorPalette.gray[300]};
        }
   `
        : ``}

    ${(props) =>
      !props.isPDP
        ? `
        border-bottom: ${props.theme.spacing.ELEM_SPACING.XXXS} solid
        ${props.theme.colorPalette.gray[300]};

   `
        : ``}
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0;

    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) =>
        !props.isPDP
          ? `
            border-bottom: ${props.theme.spacing.ELEM_SPACING.XXXS} solid
            ${props.theme.colorPalette.gray[300]};

       `
          : ``}

      padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0;
    }
  }
  &.complete-the-look-section.hide-complete-look-header {
    display: none;
  }
  &.complete-the-look-section.add-border {
    ${(props) =>
      props.isPDP
        ? `
        border: ${props.theme.spacing.ELEM_SPACING.XS} solid
        ${props.theme.colorPalette.gray[300]};
        @media ${props.theme.mediaQuery.large} {
          border:unset;
          border-top: 1px solid
        ${props.theme.colorPalette.gray[300]};
        }
   `
        : ``}

    ${(props) =>
      !props.isPDP
        ? `
        border: ${props.theme.spacing.ELEM_SPACING.XXXS} solid
        ${props.theme.colorPalette.gray[300]};

   `
        : ``}
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0;

    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) =>
        props.isPDP
          ? `
          border:unset;
     `
          : ``}
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      ${(props) =>
        !props.isPDP
          ? `
          border:unset;
          border-bottom: ${props.theme.spacing.ELEM_SPACING.XXXS} solid
          ${props.theme.colorPalette.gray[300]};

     `
          : ``}

      padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) =>
        props.isNewPDPEnabled
          ? `
          border-radius: 16px;background-color: ${props.theme.colors.WHITE};
     `
          : ``}
    }
  }

  .product-detail-footer {
    display: flex;
    justify-content: space-between;
    align-items: baseline;
  }

  max-height: initial;

  .product-desc-heading {
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
    ${(props) =>
      !props.isNewPDPEnabled && `background: url(${downArrowIcon}) no-repeat right 0 bottom 7px;`}
    @media ${(props) => props.theme.mediaQuery.large} {
      background: none;
    }
  }
  .show-accordion-toggle {
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
    ${(props) =>
      !props.isNewPDPEnabled && `background: url(${upArrowIcon}) no-repeat right 0 bottom 7px;`}
    @media ${(props) => props.theme.mediaQuery.large} {
      background: none;
    }
    @media ${(props) => props.theme.mediaQuery.largeOnly} {
      ${(props) =>
        props.isNewPDPEnabled &&
        ` margin: ${props.theme.spacing.ELEM_SPACING.SM} ${props.theme.spacing.ELEM_SPACING.SM};`}
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      ${(props) => props.isNewPDPEnabled && ` margin: ${props.theme.spacing.ELEM_SPACING.SM} 20px;`}
    }
  }
`;
