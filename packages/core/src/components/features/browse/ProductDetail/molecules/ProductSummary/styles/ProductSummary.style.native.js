// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';
import { StyleSheet } from 'react-native';

const getAdditionalStyle = (props) => {
  const { margin, width } = props;
  return {
    ...(margin && { margin }),
    ...(width && { width }),
  };
};

const ImageTouchableOpacity = styled.TouchableOpacity`
  justify-content: center;
`;

const Container = styled.View`
  ${(props) =>
    props.isNewReDesignProductSummary
      ? `
  flex-direction: column;
  justify-content: space-around;
  height:${() => (props.isFromBundleProductView ? 80 : 125)};
  `
      : `
  justify-content: center;
  `}
`;

const BazaarVoiceContainer = styled.View`
  flex-direction: row;
  ${(props) => !props.isNewReDesignProductSummary && `height: 40px;`}
  justify-content: space-between;
  align-items: center;
`;

const ReviewAndRatingContainer = styled.View`
  flex-direction: row;
`;

const ProductSummaryRatingAndMessage = styled.View`
  flex-direction: row;
`;

const ProductSummaryRating = styled.TouchableOpacity`
  flex: 0.4;
  justify-content: center;
`;

const ProductSummaryPromotionalMessage = styled.View`
  flex: 0.6;
  align-items: flex-end;
  justify-content: center;
`;

const ProductSummaryPrice = styled.View`
  flex-direction: row;
  align-items: flex-end;
`;

const ProductSummaryPriceDiscount = styled.View`
  margin-left: 6px;
  margin-bottom: 2px;
`;

const RowContainer = styled.View`
  flex-direction: row;
  ${getAdditionalStyle}
`;

const EmptyView = styled.View`
  ${getAdditionalStyle};
`;

// SBP MERGE JIRA SBP-902 STYLING
const ProductSummaryStyle = StyleSheet.create({
  view1: {
    flexDirection: 'row',
    height: 60,
    justifyContent: 'space-between',
  },
  view2: {
    justifyContent: 'center',
  },
  view3: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginRight: '-13%',
  },
});

const styles = css``;

const AfterPayWrapper = styled.View`
  margin-top: 6px;
`;

export {
  styles,
  Container,
  RowContainer,
  ReviewAndRatingContainer,
  BazaarVoiceContainer,
  ImageTouchableOpacity,
  EmptyView,
  ProductSummaryStyle,
  ProductSummaryRatingAndMessage,
  ProductSummaryRating,
  ProductSummaryPromotionalMessage,
  ProductSummaryPrice,
  ProductSummaryPriceDiscount,
  AfterPayWrapper,
};
