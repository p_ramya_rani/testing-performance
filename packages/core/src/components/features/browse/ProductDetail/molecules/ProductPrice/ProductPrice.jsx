// 9fbef606107a605d69c0edbcd8029e5d
/** @module PDP - Product Price
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import { currencyConversion } from '@tcp/core/src/components/features/CnC/CartItemTile/utils/utils';
import { isClient, getViewportInfo } from '@tcp/core/src/utils';
import { PriceCurrency } from '@tcp/core/src/components/common/molecules';
import { PRICING_VISIBLE, SINGLE_TEXT } from '@tcp/core/src/constants/rum.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import AfterPayMessaging from '@tcp/core/src/components/common/organisms/AfterPayMessaging/container/AfterPayMessaging.container';
import { PromotionalMessage } from '../../../ProductListing/molecules/ProductList/views/ProductItemComponents';
import { getPromotionalMessage, isGymboree } from '../../../../../../utils';
import { BodyCopy } from '../../../../../common/atoms';
import withStyles from '../../../../../common/hoc/withStyles';
import productPriceStyle from './ProductPrice.style';
import ProductItemPrice from '../ProductItemPrice/ProductItemPrice';
import ProductRating from '../ProductRating/ProductRating';

const getListPricePostFix = (highListPrice, nonUSCA, currencySign) => {
  return highListPrice ? (
    <>
      <span> - </span>
      <span className="post">
        {nonUSCA ? (
          `${currencySign}${highListPrice.toFixed(2)}`
        ) : (
          <PriceCurrency price={highListPrice} />
        )}
      </span>
    </>
  ) : (
    ''
  );
};

const getHighOfferPrice = (highOfferPrice, nonUSCA, currencySign) => {
  return highOfferPrice ? (
    <>
      <span> - </span>
      <span className="post">
        {nonUSCA ? (
          `${currencySign}${highOfferPrice.toFixed(2)}`
        ) : (
          <PriceCurrency price={highOfferPrice} />
        )}
      </span>
    </>
  ) : (
    ''
  );
};
const getOnlyListPricePostFix = (onlyListPrice, listPrice) => {
  return listPrice || onlyListPrice;
};
const renderPointsAndReward = (labels) => {
  const pointsAndRewardsLabel =
    (labels.pointsAndRewardsText && labels.pointsAndRewardsText.split('##')) || '';
  return pointsAndRewardsLabel ? (
    <BodyCopy component="p" className="points-and-rewards">
      {pointsAndRewardsLabel[0] && (
        <BodyCopy component="span" className="points-and-rewards-1">
          {pointsAndRewardsLabel[0]}
        </BodyCopy>
      )}
      {pointsAndRewardsLabel[1] && (
        <BodyCopy component="span" className="points-and-rewards-or">
          {pointsAndRewardsLabel[1]}
        </BodyCopy>
      )}
      {pointsAndRewardsLabel[2] && (
        <BodyCopy component="span" className="points-and-rewards-2">
          {pointsAndRewardsLabel[2]}
        </BodyCopy>
      )}
    </BodyCopy>
  ) : null;
};
class ProductPrice extends React.Component {
  static propTypes = {
    isItemPartNumberVisible: PropTypes.bool.isRequired,
    currencySymbol: PropTypes.string.isRequired,
    listPrice: PropTypes.number.isRequired,
    hideItemProp: PropTypes.bool, // prop is used to hide unwanted duplicate itemProp as we have duplicate ProductPrice component for web and mobile on PDP pages
    offerPrice: PropTypes.number,
    highOfferPrice: PropTypes.number,
    highListPrice: PropTypes.number,
    itemPartNumber: PropTypes.string,
    currencyExchange: PropTypes.shape({}),
    promotionalMessage: PropTypes.string,
    isPlcc: PropTypes.bool.isRequired,
    promotionalPLCCMessage: PropTypes.string,
    badge2: PropTypes.string,
    favIconContainer: PropTypes.shape({}),
    currencyAttributes: PropTypes.shape({}).isRequired,
    customFonts: PropTypes.shape({ listPriceFont: PropTypes.string }),
    className: PropTypes.string,
    isInternationalShipping: PropTypes.bool.isRequired,
    onlyListPrice: PropTypes.number,
    currencySign: PropTypes.string,
    fromBagPage: PropTypes.bool.isRequired,
    isGiftCard: PropTypes.bool.isRequired,
    productInfoFromBag: PropTypes.shape({}).isRequired,
    isOutfitPage: PropTypes.bool,
    TCPMultipackProductMapping: PropTypes.shape([]),
    selectedMultipack: PropTypes.string,
    pdpLabels: PropTypes.shape({}),
    isPerUnitPriceEnabled: PropTypes.bool,
    isStyleWith: PropTypes.bool,
    productInfo: PropTypes.shape({}),
    isBrierleyPromoEnabled: PropTypes.bool,
    TCPStyleQTY: PropTypes.string,
    isAfterPayDisplay: PropTypes.bool,
    isNewQVEnabled: PropTypes.bool,
    isNewPDPEnabled: PropTypes.bool,
    fromPickUp: PropTypes.bool,
    renderProductRatings: PropTypes.func,
  };

  static defaultProps = {
    offerPrice: null,
    itemPartNumber: '',
    currencyExchange: {},
    promotionalMessage: '',
    promotionalPLCCMessage: '',
    highOfferPrice: '',
    onlyListPrice: '',
    hideItemProp: false,
    highListPrice: '',
    badge2: '',
    favIconContainer: null,
    className: '',
    customFonts: {},
    currencySign: '',
    isOutfitPage: false,
    TCPMultipackProductMapping: [],
    selectedMultipack: '1',
    pdpLabels: {},
    isPerUnitPriceEnabled: false,
    isStyleWith: false,
    productInfo: {},
    isBrierleyPromoEnabled: false,
    TCPStyleQTY: '',
    isAfterPayDisplay: false,
    isNewQVEnabled: false,
    fromPickUp: false,
    renderProductRatings: () => {},
    isNewPDPEnabled: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      isPriceVisible: false,
    };
    this.afterPayPrice = 0;
  }

  componentDidMount() {
    const { listPrice, offerPrice, highOfferPrice, highListPrice, onlyListPrice } = this.props;
    if (listPrice || offerPrice || highOfferPrice || highListPrice || onlyListPrice) {
      this.setState({ isPriceVisible: true });
    }
  }

  getBadge = (badge2) => {
    return badge2 ? (
      <BodyCopy
        className="badge"
        color="red.600"
        fontSize="fs14"
        fontWeight="semibold"
        fontFamily="secondary"
      >
        {badge2}
      </BodyCopy>
    ) : null;
  };

  getPerItemPrice = (offerPrice, giftCardPrice, pdpLabels) => {
    const { selectedMultipack, isPerUnitPriceEnabled, TCPStyleQTY } = this.props;
    const multiPackSingle = (pdpLabels && pdpLabels.lblPdpMultipackSingle) || SINGLE_TEXT;
    const selectedMpackCount = TCPStyleQTY || selectedMultipack;
    const showPerPrice =
      selectedMpackCount &&
      String(selectedMpackCount).toLowerCase() !== multiPackSingle.toLowerCase() &&
      String(selectedMpackCount) !== '1';

    return showPerPrice && isPerUnitPriceEnabled ? (
      <ProductItemPrice
        packPrice={this.getGiftCardOrOfferPrice(offerPrice, giftCardPrice)}
        selectedMultipack={selectedMpackCount}
        pdpLabels={pdpLabels}
      />
    ) : null;
  };

  isCurrencyExchangeValid = (currencyAttributes) =>
    currencyAttributes && currencyAttributes.exchangevalue;

  getBundlePriceList = (highOfferPrice, highListPrice, listPrice) => {
    return (!!highOfferPrice || !!highListPrice) && highListPrice !== listPrice;
  };

  getBothPrice = (offerPrice, listPrice, highOfferPrice, highListPrice) => {
    return (
      (offerPrice && offerPrice !== listPrice) ||
      (highOfferPrice && highOfferPrice !== highListPrice)
    );
  };

  getPriceColor = () => {
    return isGymboree() ? 'gray.900' : 'red.500';
  };

  getGiftCardPrice = () => {
    const { fromBagPage, isGiftCard, productInfoFromBag } = this.props;
    if (fromBagPage && isGiftCard) {
      return parseInt(productInfoFromBag.selectedSize, 10);
    }
    return null;
  };

  getGiftCardOrOfferPrice = (offerPrice, giftCardPrice) => {
    return giftCardPrice || offerPrice;
  };

  productRatingCheck = (isGiftCard, isStyleWith) => {
    return !isGiftCard && isStyleWith;
  };

  getPriceMarkUp = () => {
    let { listPrice, offerPrice, highOfferPrice, highListPrice, onlyListPrice } = this.props;
    const { badge2 } = this.props;
    const {
      className,
      currencySymbol,
      currencyAttributes,
      customFonts: { listPriceFont },
      currencySign,
      isOutfitPage,
      pdpLabels,
      isNewQVEnabled,
      isNewPDPEnabled,
      isFbt,
    } = this.props;

    if (this.isCurrencyExchangeValid(currencyAttributes)) {
      offerPrice = currencyConversion(offerPrice, currencyAttributes);
      listPrice = currencyConversion(listPrice, currencyAttributes);
      highOfferPrice = currencyConversion(highOfferPrice, currencyAttributes);
      highListPrice = currencyConversion(highListPrice, currencyAttributes);
      onlyListPrice = currencyConversion(onlyListPrice, currencyAttributes);
    }
    const nonUSCA = currencySymbol === 'CAD' || currencySymbol === 'USD';
    const listPricePostFix = getListPricePostFix(highListPrice, nonUSCA, currencySign);
    const offerPricePostFix = getHighOfferPrice(highOfferPrice, nonUSCA, currencySign);
    const onlyListPriceInfo = getOnlyListPricePostFix(onlyListPrice, listPrice);
    const showBothPrice = this.getBothPrice(offerPrice, listPrice, highOfferPrice, highListPrice);
    const offerPriceFontSize = isFbt ? 'fs14' : 'fs22';

    const bundleListPrice = this.getBundlePriceList(highOfferPrice, highListPrice, listPrice);
    const priceColor = this.getPriceColor();
    const giftCardPrice = this.getGiftCardPrice();
    const perItemPrice = this.getPerItemPrice(offerPrice, giftCardPrice, pdpLabels);
    if (showBothPrice) {
      this.afterPayPrice = offerPrice;
      return (
        <div
          className={`${className} price-container ${
            isNewQVEnabled || isNewPDPEnabled ? 'quick-view-drawer-redesign' : ''
          }`}
        >
          <meta itemProp="price" content={offerPrice} />
          <BodyCopy
            className={`${isFbt ? 'fbt-price' : 'price-item actual-price'}`}
            fontSize={offerPriceFontSize}
            fontFamily="secondary"
            fontWeight="black"
            color={priceColor}
          >
            <PriceCurrency price={offerPrice} />
            {offerPricePostFix}
          </BodyCopy>
          {perItemPrice}
          <div className={`list-badge-container ${isFbt ? 'fbt-price-original' : ''}`}>
            <BodyCopy
              className="price-item original-price"
              fontSize={listPriceFont || 'fs13'}
              fontFamily="secondary"
              color="gray.800"
            >
              {/* TODO - fix it with bundle {!(isBundleProduct || isBundleList) ? 'Was' : ''}  */}

              <span className="pre">
                <PriceCurrency price={listPrice} />
              </span>
              {listPricePostFix}
            </BodyCopy>
            {this.getBadge(badge2)}
          </div>
        </div>
      );
    }
    this.afterPayPrice = this.getGiftCardOrOfferPrice(offerPrice, giftCardPrice);
    if (this.afterPayPrice) {
      return (
        <>
          <meta itemProp="price" content={this.afterPayPrice} />
          <BodyCopy
            className="price-item actual-price"
            fontSize={offerPriceFontSize}
            fontFamily="secondary"
            fontWeight="black"
            color={priceColor}
          >
            <PriceCurrency price={this.afterPayPrice} />
            {offerPricePostFix}
          </BodyCopy>
          {perItemPrice}
        </>
      );
    }

    if (bundleListPrice) {
      this.afterPayPrice = listPrice;
      return (
        <>
          <meta itemProp="price" content={listPrice} />
          <BodyCopy
            className="price-item actual-price"
            fontSize={offerPriceFontSize}
            fontFamily="secondary"
            fontWeight="black"
            color={priceColor}
          >
            <PriceCurrency price={listPrice} />
            {listPricePostFix}
          </BodyCopy>
          {perItemPrice}
        </>
      );
    }

    this.afterPayPrice = onlyListPriceInfo;
    return (
      <>
        <meta itemProp="price" content={onlyListPriceInfo} />
        <BodyCopy
          className="price-item actual-price"
          fontSize={isOutfitPage ? ['fs22'] : 'fs16'}
          fontFamily="secondary"
          fontWeight="black"
          color={priceColor}
        >
          <PriceCurrency price={onlyListPriceInfo} />
        </BodyCopy>
        {perItemPrice}
      </>
    );
  };

  checkisMobile = () => {
    return isClient() ? getViewportInfo().isMobile : null;
  };

  redesignCheck = () => {
    const { isNewQVEnabled, isNewPDPEnabled } = this.props;
    return isNewQVEnabled || (isNewPDPEnabled && this.checkisMobile());
  };

  afterPayCheck = () => {
    const { isAfterPayDisplay, isGiftCard, fromBagPage } = this.props;
    return isAfterPayDisplay && !isGiftCard && !(this.redesignCheck() && fromBagPage);
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      isItemPartNumberVisible,
      itemPartNumber,
      promotionalMessage,
      // isCanada,
      promotionalPLCCMessage,
      isPlcc,
      currencySymbol,
      isInternationalShipping,
      // isBundleProduct,
      className,
      // isBundleList,
      hideItemProp,
      isGiftCard,
      isStyleWith,
      productInfo: { ratings, reviewsCount },
      pdpLabels,
      isBrierleyPromoEnabled,
      fromPickUp,
      renderProductRatings,
      productInfo,
      isNewQVEnabled,
      fromBagPage,
      isNewPDPEnabled,
      isFbt,
    } = this.props;
    let productItemProps = {};
    let productInStockItemProp;

    if (!hideItemProp) {
      productItemProps = {
        itemProp: 'offers',
        itemScope: true,
        itemType: 'https://schema.org/Offer',
      };
      productInStockItemProp = 'availability';
    }
    const { isPriceVisible } = this.state;

    const offerPrice = this.afterPayPrice;
    const isFromBagPageClass = fromBagPage ? ' price-container-bagPage' : '';
    return (
      <section
        className={`${className} product-price-container${isFromBagPageClass}`}
        {...productItemProps}
      >
        <div>
          {this.redesignCheck() && !fromBagPage ? (
            <div className="price-rating-section">
              <div className="price-section-left">{this.getPriceMarkUp()}</div>
              <div className="rating-section-right">
                {renderProductRatings(productInfo, isNewQVEnabled, isNewPDPEnabled)}
              </div>
            </div>
          ) : (
            this.getPriceMarkUp()
          )}
          {!isNewPDPEnabled && this.afterPayCheck() && (
            <div className={`${fromPickUp ? 'afterPay-msg-wrapper' : ''}`}>
              <AfterPayMessaging offerPrice={offerPrice} />
            </div>
          )}

          {isPriceVisible && <RenderPerf.Measure name={PRICING_VISIBLE} />}
          {isItemPartNumberVisible && (
            <strong className="number-item">
              Item #:
              {itemPartNumber}
            </strong>
          )}
        </div>
        {!hideItemProp && <meta itemProp="priceCurrency" content={currencySymbol} />}
        <link itemProp={productInStockItemProp} href="http://schema.org/InStock" />
        {isBrierleyPromoEnabled
          ? renderPointsAndReward(pdpLabels)
          : promotionalMessage &&
            !isStyleWith &&
            !isFbt && (
              // Adding this exception as prettier is formating it against the indentation rules
              // eslint-disable-next-line react/jsx-indent
              <PromotionalMessage
                pdpLabels={pdpLabels}
                isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                isInternationalShipping={isInternationalShipping}
                text={getPromotionalMessage(isPlcc, {
                  promotionalMessage,
                  promotionalPLCCMessage,
                })}
              />
            )}
        {isNewPDPEnabled && this.afterPayCheck() && (
          <div className={`newafterPay ${fromPickUp ? 'afterPay-msg-wrapper' : ''}`}>
            <AfterPayMessaging offerPrice={offerPrice} />
          </div>
        )}
        {this.productRatingCheck(isGiftCard, isStyleWith) ? (
          <ProductRating ratings={ratings || 0} reviews={reviewsCount} />
        ) : null}
      </section>
    );
  }
}

export default errorBoundary(withStyles(ProductPrice, productPriceStyle));
