// 9fbef606107a605d69c0edbcd8029e5d
/** @module ProductRating
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import { getIconPath, getViewportInfo, isClient } from '@tcp/core/src/utils';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import SocialProofMessage from '@tcp/core/src/components/features/browse/ProductDetail/molecules/SocialProofMessage/container/SocialProofMessage.container';
import withStyles from '../../../../../common/hoc/withStyles';
import ProductRatingStyle from './styles/ProductRating.style';

const executeScroll = (pageName, handleRatingClick) => {
  const isMobile = isClient() ? getViewportInfo()?.isMobile : null;
  let ratingReviewSection = document.querySelector('.bv-content-list-container')
    ? document.querySelector('.ratings-and-reviews-accordion')
    : null;
  if (!ratingReviewSection) {
    ratingReviewSection = document.querySelector('.reviews-anchoring-wrapper')
      ? document.querySelector('.reviews-anchoring-wrapper')
      : null;
  }
  let yPos = ratingReviewSection && ratingReviewSection.getBoundingClientRect().top;
  if (ratingReviewSection) {
    yPos += isMobile ? window.scrollY - 5 : window.scrollY - 70;
  }
  if (yPos && pageName === 'PDP') {
    handleRatingClick(true);
    window.scrollTo({
      top: yPos,
      behavior: 'smooth',
    });
  }
};
/* eslint-disable max-params */
const renderCustomRatings = (
  ratings,
  reviews,
  className,
  pageName,
  socialProofMessage,
  isNewQVEnabled,
  isNewPDPEnabled,
  handleRatingClick,
  ratingsProductId,
  isGiftCard
) => {
  const starsRatingWhite = getIconPath('stars-rating');
  const tab = isNewPDPEnabled ? 1 : -1;
  const { isMobile } = isClient() && getViewportInfo();

  return (
    <div className={`ranking-wrapper ${className}`}>
      <div className="ratings-container">
        {Number(ratings) >= 0 && (
          <div className="custom-ranking-container">
            <span className="rating-star-overlay" ratings={ratings}>
              <Image
                src={starsRatingWhite}
                title={`Product ratings - ${ratings}`}
                alt="Star Rating Icon"
                className="rating-star-yellow"
                onClick={() => executeScroll(pageName, handleRatingClick)}
                onKeyPress={() => executeScroll(pageName, handleRatingClick)}
                role="button"
                tabIndex={tab}
              />
            </span>
            <Image
              src={starsRatingWhite}
              title="Product ratings"
              alt="Star Rating Icon"
              className="rating-star-grey"
              ratings={ratings}
              onClick={() => executeScroll(pageName, handleRatingClick)}
              onKeyPress={() => executeScroll(pageName, handleRatingClick)}
              role="button"
              tabIndex={tab}
            />
            {Number(reviews) >= 0 && Number(ratings) >= 0 && !isNewQVEnabled && !isNewPDPEnabled && (
              <BodyCopy
                component="span"
                fontFamily="secondary"
                className="reviews-count"
                onClick={() => executeScroll(pageName, handleRatingClick)}
                onKeyPress={() => executeScroll(pageName, handleRatingClick)}
                role="button"
                tabIndex={tab}
              >
                {`(${reviews})`}
              </BodyCopy>
            )}
            {!!reviews && ratings && (isNewQVEnabled || isNewPDPEnabled) && (
              <BodyCopy
                component="span"
                fontFamily="secondary"
                className="reviews-count-quick-view-drawer-redesign"
                onClick={() => executeScroll(pageName, handleRatingClick)}
                onKeyPress={() => executeScroll(pageName, handleRatingClick)}
                role="button"
                tabIndex={tab}
              >
                {`${reviews}`}
              </BodyCopy>
            )}
          </div>
        )}
      </div>
      {/* Showing custom rating using above function and hiding rating from below div via css */}
      {pageName === 'PDP' && isNewPDPEnabled && !isMobile && !isGiftCard && (
        <div
          className="write-review-link"
          data-bv-show="rating_summary"
          data-bv-product-id={ratingsProductId}
          data-bv-seo="false"
        />
      )}
      {pageName === 'PDP' && socialProofMessage?.variation === 'top' && (
        <SocialProofMessage socialProofMessage={socialProofMessage} />
      )}
    </div>
  );
};
const ProductRating = (props) => {
  const {
    ratings,
    reviews,
    className,
    pageName,
    socialProofMessage,
    isNewQVEnabled,
    isNewPDPEnabled,
    handleRatingClick,
    ratingsProductId,
    isGiftCard,
  } = props;
  return (
    <div className={`${className} ${isNewPDPEnabled ? 'rating-wrapper-new-pdp' : ''}`}>
      {renderCustomRatings(
        ratings,
        reviews,
        className,
        pageName,
        socialProofMessage,
        isNewQVEnabled,
        isNewPDPEnabled,
        handleRatingClick,
        ratingsProductId,
        isGiftCard
      )}
    </div>
  );
};

ProductRating.propTypes = {
  ratings: PropTypes.string.isRequired,
  reviews: PropTypes.string.isRequired,
  starRatingSize: PropTypes.shape({}),
  ratingsProductId: PropTypes.string,
  isNewQVEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  pageName: PropTypes.string,
  socialProofMessage: PropTypes.string,
  handleRatingClick: PropTypes.func,
  className: PropTypes.string,
  isGiftCard: PropTypes.bool.isRequired,
  /* Rating of the product (float number between 0 and 5). */
};

ProductRating.defaultProps = {
  ratingsProductId: '',
  socialProofMessage: '',
  starRatingSize: { small: 72, large: 100 },
  isNewQVEnabled: false,
  isNewPDPEnabled: false,
  pageName: '',
  handleRatingClick: () => {},
  className: '',
};

export default errorBoundary(withStyles(ProductRating, ProductRatingStyle));
export { ProductRating as ProductRatingVanilla };
