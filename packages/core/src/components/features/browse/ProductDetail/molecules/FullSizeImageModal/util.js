// 9fbef606107a605d69c0edbcd8029e5d
const onDoubleTap = callback => {
  let touchCount = 0;
  return () => {
    touchCount += 1;

    const countTimer = setTimeout(() => {
      touchCount = 0;
      clearTimeout(countTimer);
    }, 500);

    if (touchCount >= 2) {
      touchCount = 0;

      callback();
    }
  };
};

const imgCrop = 'w_auto,f_auto,q_auto';
export const imgConfigs = [imgCrop, imgCrop, imgCrop];

export const videoConfigs = ['t_pdp_vid_m', 't_pdp_vid_t', 't_pdp_vid_d'];

export default onDoubleTap;
