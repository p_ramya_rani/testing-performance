// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const productPriceStyle = css`
  .original-price {
    .pre,
    .post {
      text-decoration: line-through;
      line-height: 2.2;
    }
  }
  .badge {
    line-height: 2;
    margin-left: 8px;
  }
  .list-badge-container {
    display: flex;
    ${(props) => (props.isNewPDPEnabled ? 'padding-left:10px;' : '')}
  }
  .price-rating-section {
    display: flex;
    flex: 1;
    justify-content: space-between;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
      ${(props) => (props.isNewPDPEnabled ? 'margin-top:0px;' : '')}
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      ${(props) => (props.isNewPDPEnabled ? 'margin-top:0px;' : '')}
    }
  }
  .rating-section-right {
    flex-shrink: 0;
    margin-right: 12px;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-right: 14px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-right: 0px;
    }
  }
  .price-container.quick-view-drawer-redesign {
    display: block;
    ${(props) => (props.isNewPDPEnabled ? 'display: flex;' : '')}
  }
  .loyalty-text-container {
    color: ${(props) => props.theme.colors.BLACK};
    span {
      color: ${(props) =>
        props.isPlcc
          ? props.theme.colorPalette.userTheme.plcc
          : props.theme.colorPalette.userTheme.mpr};
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      ${(props) =>
        props.isNewPDPEnabled ? `font-size: ${props.theme.typography.fontSizes.fs12};` : ''}
    }
  }
  .afterPay-msg-wrapper {
    position: absolute;
    left: 0;
    ${(props) => (props.isNewQVEnabled && props.fromPickUp ? 'margin-top: 5px;' : '')}
  }
  .newafterPay {
    padding: 5px 0;
  }
  .fbt-price {
    display: inline-block;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
  .fbt-price-original {
    display: inline-block;
  }
  ${(props) => (props.inheritedStyles ? props.inheritedStyles : '')};
`;

export default productPriceStyle;
