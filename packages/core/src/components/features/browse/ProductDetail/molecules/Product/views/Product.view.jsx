// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FULLY_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import ProductRating from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductRating/ProductRating';
import { isCanada } from '@tcp/core/src/utils/utils';
import ProductPrice from '../../ProductPrice/ProductPrice';
import PlccWrapperStatic from '../../PlccWrapperStatic/views/PlccWrapperStatic.view';
import ProductBasicInfo from '../../ProductBasicInfo/ProductBasicInfo';

import {
  getPrices,
  getPricesWithRange,
  getMapSliceForColorProductId,
  checkIsSelectedSizeDisabled,
  getMapSliceForSizeSkuID,
} from '../../../../ProductListing/molecules/ProductList/utils/productsCommonUtils';

const getColorname = (colorProduct) => colorProduct.color && colorProduct.color.name;

const getBadgePercentage = (colorProduct) =>
  colorProduct.miscInfo && colorProduct.miscInfo.badge2 ? colorProduct.miscInfo.badge2 : '';

/* eslint-disable-next-line complexity, sonarjs/cognitive-complexity */
const Product = (props) => {
  /**
   * This just holds the logic for rendering a UX timer
   * to measure when the cart items appear. It reads the
   * number of cart items and renders the timer if that
   * number is greater than zero.
   */
  function ProductInfoUXTimer({ productObj }) {
    const [state, setState] = useState(false);
    useEffect(() => {
      if (productObj.name) setState(true);
    }, [productObj.name]);
    return state ? <RenderPerf.Measure name={FULLY_VISIBLE} /> : null;
  }
  const renderProductRatings = (
    productInfo,
    isNewQVEnabled,
    isNewPDPEnabled,
    socialProofMessage
  ) => {
    const { handleRatingClick } = props;
    const { ratingsProductId } = productInfo;
    const ratings = (productInfo && productInfo.ratings) || 0;
    const reviewsCount = (productInfo && productInfo.reviewsCount) || 0;
    return (
      Number(ratings) >= 0 && (
        <ProductRating
          ratings={ratings || 0}
          reviews={reviewsCount}
          isNewQVEnabled={isNewQVEnabled}
          isNewPDPEnabled={isNewPDPEnabled}
          socialProofMessage={socialProofMessage}
          pageName="PDP"
          handleRatingClick={handleRatingClick}
          ratingsProductId={ratingsProductId}
        />
      )
    );
  };
  const {
    productDetails,
    currencySymbol,
    priceCurrency,
    currencyAttributes,
    isHasPlcc,
    isInternationalShipping,
    isMatchingFamily,
    selectedColorProductId,
    isGiftCard,
    onAddItemToFavorites,
    isLoggedIn,
    isShowPriceRangeKillSwitch,
    formValues = {},
    isBundleProduct,
    keepAlive,
    outOfStockLabels,
    reviewOnTop,
    AddToFavoriteErrorMsg,
    removeAddToFavoritesErrorMsg,
    hideItemProp,
    isInWishList,
    currencySign,
    showPriceRangeForABTest,
    resetCountUpdate,
    socialProofMessage,
    countUpdated,
    hidePriceDisplay,
    selectedMultipack,
    pdpLabels,
    isPerUnitPriceEnabled,
    isStyleWith,
    checkForOOSForVariant,
    isBrierleyPromoEnabled,
    TCPStyleQTY,
    onSetLastDeletedItemIdAction,
    wishList,
    deleteFavItemInProgressFlag,
    isAfterPayDisplay,
    isOutfitPage,
    errorMessages,
    isNewPDPEnabled,
    primaryBrand,
    isShowBrandNameEnabled,
    openModal,
    alternateBrand,
  } = props;
  const { fit, size } = formValues;
  const { currentProduct: productInfo } = productDetails;
  const { TCPMultipackProductMapping, TCPStyleType } = productInfo || {};

  if (!productInfo) {
    return <div />; // TODO - maybe add loader later
  }
  const { promotionalMessage, promotionalPLCCMessage } = productInfo;
  const colorProduct =
    getMapSliceForColorProductId(productInfo.colorFitsSizesMap, selectedColorProductId) || {};
  let prices = getPrices(productInfo, colorProduct.color && colorProduct.color.name, fit, size);
  const badges = colorProduct.miscInfo ? colorProduct.miscInfo.badge1 : {};
  const badge1 = isMatchingFamily && badges.matchBadge ? badges.matchBadge : badges.defaultBadge;
  const badge2 = getBadgePercentage(colorProduct);

  let skuId = null;
  if (typeof size === 'string' && size) {
    skuId = getMapSliceForSizeSkuID(colorProduct, size);
  }

  const colorProductName = getColorname(colorProduct);
  if (isShowPriceRangeKillSwitch) {
    const isSelectedSizeDisabled = checkIsSelectedSizeDisabled(productInfo, formValues);
    prices = getPricesWithRange(productInfo, colorProductName, fit, size, isSelectedSizeDisabled);
  }

  if (!showPriceRangeForABTest) {
    prices = {
      listPrice: prices.listPrice,
      offerPrice: prices.offerPrice,
      badge2: prices.badge2,
    };
  }

  if (isBundleProduct) {
    prices = getPricesWithRange(productInfo, colorProductName);
  }
  return (
    <>
      <div className={!reviewOnTop ? 'hide-on-mobile' : 'hide-on-desktop hide-on-tablet'}>
        <ProductBasicInfo
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          badge={badge1}
          isGiftCard={isGiftCard}
          productInfo={productInfo}
          isShowFavoriteCount
          currencySymbol={currencySymbol}
          priceCurrency={priceCurrency}
          currencyAttributes={currencyAttributes}
          isRatingsVisible
          isCanada={isCanada()}
          isPlcc={isHasPlcc}
          isBundleProduct={isBundleProduct}
          isInternationalShipping={isInternationalShipping}
          onAddItemToFavorites={onAddItemToFavorites}
          isLoggedIn={isLoggedIn}
          productMiscInfo={colorProduct}
          AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
          removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
          pageName="PDP"
          skuId={skuId}
          formName={PRODUCT_ADD_TO_BAG}
          reviewOnTop={reviewOnTop}
          isInWishList={isInWishList}
          hideRating={isBundleProduct}
          resetCountUpdate={resetCountUpdate}
          socialProofMessage={socialProofMessage}
          countUpdated={countUpdated}
          isStyleWith={isStyleWith}
          checkForOOSForVariant={checkForOOSForVariant}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          wishList={wishList}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          isOutfitPage={isOutfitPage}
          errorMessages={errorMessages}
          isNewPDPEnabled={isNewPDPEnabled}
          primaryBrand={primaryBrand}
          colorProductName={colorProductName}
          pdpLabels={pdpLabels}
          isShowBrandNameEnabled={isShowBrandNameEnabled}
          renderProductRatings={renderProductRatings}
          formValues={formValues}
          alternateBrand={alternateBrand}
        />
      </div>
      <div className={reviewOnTop ? 'hide-on-mobile hide-on-desktop hide-on-tablet' : ''}>
        {!isGiftCard && !hidePriceDisplay && (
          <>
            <ProductPrice
              currencySign={currencySign}
              hideItemProp={hideItemProp}
              currencySymbol={currencySymbol}
              priceCurrency={priceCurrency}
              currencyAttributes={currencyAttributes}
              isItemPartNumberVisible={false}
              itemPartNumber={colorProduct.colorDisplayId}
              {...prices}
              badge2={badge2}
              promotionalMessage={promotionalMessage}
              isCanada={isCanada()}
              promotionalPLCCMessage={promotionalPLCCMessage}
              isPlcc={isHasPlcc}
              isInternationalShipping={isInternationalShipping}
              selectedMultipack={selectedMultipack}
              TCPMultipackProductMapping={TCPMultipackProductMapping}
              TCPStyleType={TCPStyleType}
              pdpLabels={pdpLabels}
              isPerUnitPriceEnabled={isPerUnitPriceEnabled}
              isStyleWith={isStyleWith}
              isGiftCard={isGiftCard}
              productInfo={productInfo}
              isBrierleyPromoEnabled={isBrierleyPromoEnabled}
              TCPStyleQTY={TCPStyleQTY}
              deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
              wishList={wishList}
              isAfterPayDisplay={isAfterPayDisplay}
              isInWishList={isInWishList}
              isOutfitPage={isOutfitPage}
              errorMessages={errorMessages}
              isNewPDPEnabled={isNewPDPEnabled}
              renderProductRatings={renderProductRatings}
              primaryBrand={primaryBrand}
              alternateBrand={alternateBrand}
            />
            <ProductInfoUXTimer productObj={productInfo} />
            {!isCanada() && !isHasPlcc && !isBundleProduct && (
              <PlccWrapperStatic openModal={openModal} pdpLabels={pdpLabels} />
            )}
          </>
        )}
      </div>
    </>
  );
};

Product.propTypes = {
  currencySymbol: PropTypes.string.isRequired,
  priceCurrency: PropTypes.string.isRequired,
  selectedColorProductId: PropTypes.string.isRequired,

  isCanada: PropTypes.bool.isRequired,
  isHasPlcc: PropTypes.bool.isRequired,
  isGiftCard: PropTypes.bool.isRequired,
  currencyAttributes: PropTypes.shape({}).isRequired,

  /* We are available to know if is an international shipping */
  isInternationalShipping: PropTypes.bool.isRequired,
  productDetails: PropTypes.shape({}).isRequired,
  formValues: PropTypes.shape({}).isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  onAddItemToFavorites: PropTypes.func.isRequired,
  isShowPriceRangeKillSwitch: PropTypes.bool.isRequired,
  isMatchingFamily: PropTypes.bool.isRequired,
  isBundleProduct: PropTypes.bool,
  keepAlive: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({
    itemSoldOutMessage: PropTypes.string,
  }),
  reviewOnTop: PropTypes.bool.isRequired,
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  isLoading: PropTypes.bool.isRequired,
  hideItemProp: PropTypes.bool,
  isInWishList: PropTypes.bool.isRequired,
  currencySign: PropTypes.string,
  showPriceRangeForABTest: PropTypes.bool.isRequired,
  resetCountUpdate: PropTypes.func.isRequired,
  socialProofMessage: PropTypes.string,
  countUpdated: false,
  hidePriceDisplay: PropTypes.bool,
  productObj: PropTypes.shape({}),
  selectedMultipack: PropTypes.string,
  pdpLabels: PropTypes.shape({}),
  isPerUnitPriceEnabled: PropTypes.bool,
  isStyleWith: PropTypes.bool,
  checkForOOSForVariant: PropTypes.bool.isRequired,
  isBrierleyPromoEnabled: PropTypes.bool,
  TCPStyleQTY: PropTypes.string,
  onSetLastDeletedItemIdAction: PropTypes.func,
  wishList: PropTypes.shape({}),
  deleteFavItemInProgressFlag: PropTypes.bool,
  isAfterPayDisplay: PropTypes.bool,
  isOutfitPage: PropTypes.bool,
  errorMessages: PropTypes.shape({}),
  isShowBrandNameEnabled: PropTypes.bool,
  openModal: PropTypes.func,
  handleRatingClick: PropTypes.func,
};

Product.defaultProps = {
  productObj: {},
  isBundleProduct: false,
  hideItemProp: false,
  keepAlive: false,
  outOfStockLabels: {
    itemSoldOutMessage: '',
  },
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  socialProofMessage: '',
  currencySign: '',
  countUpdated: false,
  hidePriceDisplay: false,
  selectedMultipack: '1',
  isPerUnitPriceEnabled: false,
  isStyleWith: false,
  pdpLabels: {},
  isBrierleyPromoEnabled: false,
  TCPStyleQTY: '',
  onSetLastDeletedItemIdAction: () => {},
  wishList: {},
  deleteFavItemInProgressFlag: false,
  isAfterPayDisplay: false,
  isOutfitPage: false,
  errorMessages: {},
  isShowBrandNameEnabled: false,
  openModal: () => {},
  handleRatingClick: () => {},
};

export default errorBoundary(Product);
