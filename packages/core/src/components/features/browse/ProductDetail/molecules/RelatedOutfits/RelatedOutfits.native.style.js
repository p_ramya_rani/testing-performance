// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

const getAdditionalStyle = props => {
  const { margins, paddings, background, showHeader } = props;
  return {
    ...(margins && showHeader && { margin: margins }),
    ...(paddings && showHeader && { padding: paddings }),
    ...(background && showHeader && { background }),
  };
};

const getOutfitWrapperAdditionalStyle = props => {
  const { showHeader } = props;
  return {
    ...(!showHeader && { height: 0 }),
  };
};

export const StyleRelatedOutfits = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export const ImageStyleWrapper = styled.View`
  justify-content: center;
  align-items: center;
`;

export const OutfitWrapper = styled.View`
  ${props =>
    props.isNewReDesignCTL
      ? `
  margin-left: -${props.theme.spacing.ELEM_SPACING.REG};
  margin-right: -${props.theme.spacing.ELEM_SPACING.REG};
  `
      : `
  margin-left: -${props.theme.spacing.ELEM_SPACING.XS};
  `}
  ${getOutfitWrapperAdditionalStyle}
`;

export const Container = styled.View`
  ${getAdditionalStyle};
`;

export default { StyleRelatedOutfits, ImageStyleWrapper };
