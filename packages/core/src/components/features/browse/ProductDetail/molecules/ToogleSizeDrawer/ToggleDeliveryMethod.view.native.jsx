/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '../../../../../common/hoc/withStyles';
import { Container, styles, TabContainer, TabButton } from './styles/ToggleSizeDrawer.style.native';

class ToggleDeliveryMethod extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tabData: this.getTabs(props),
    };
  }

  componentDidMount() {
    this.setupTabData();
  }

  componentDidUpdate(prevProps) {
    const { isBossEligible, selectedTab } = this.props;
    if (prevProps.selectedTab !== selectedTab) {
      this.updateTabs();
    }
    if (prevProps.isBossEligible !== isBossEligible) {
      this.setupTabData(this.defaultTabData);
    }
  }

  getTabs = (props) => {
    return [
      {
        index: 0,
        isSelected: props.selectedTab === 0 || false,
        title: props.pickupLabels.lbl_Product_pickup_SHIPPING,
      },
      {
        index: 1,
        isSelected: props.selectedTab === 1 || false,
        title: props.pickupLabels.lbl_Product_pickup_PICKUP,
      },
      {
        index: 2,
        isSelected: props.selectedTab === 2 || false,
        title: props.pickupLabels.lbl_Product_pickup_NO_RUSH_PICKUP,
      },
    ];
  };

  updateTabs = () => {
    const data = this.getTabs(this.props);
    this.setState({ tabData: data }, () => {
      this.setupTabData();
    });
  };

  setupTabData = () => {
    const { isBossEligible } = this.props;
    const { tabData } = this.state;
    if (!isBossEligible && tabData.length > 2) {
      const updatedData = tabData.splice(0, 2);
      this.setState({ tabData: updatedData });
    } else if (isBossEligible) {
      this.setState({ tabData: this.getTabs(this.props) });
    }
  };

  onTabPress = (idx) => {
    const { onTabChange } = this.props;
    const { tabData } = this.state;
    let eventTitle = '';
    const data = tabData.map((item) => {
      if (idx === item.index) {
        item.isSelected = true;
        eventTitle = item.title;
      } else {
        item.isSelected = false;
      }
      return item;
    });
    onTabChange(idx, eventTitle);
    this.setState({ tabData: data });
  };

  renderTabText = (value, isSelected) => {
    if (value) {
      return (
        <BodyCopy
          dataLocator="size_drawer_toggle_title"
          margin="0 0 0 0"
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="bold"
          letterSpacing={0.5}
          color={isSelected ? 'white' : 'gray.900'}
          text={value}
        />
      );
    }
    return null;
  };

  render() {
    const { keepAlive, noBossBopisInfo, isBossEligible, isDefaultBadge } = this.props;
    const { tabData } = this.state;
    return (
      <Container>
        <TabContainer>
          {tabData.map((tab) => {
            return (
              <TabButton
                key={tab.index}
                isBoss={isBossEligible}
                isSelected={tab.isSelected}
                isNoRush={tab.index === 2}
                onPress={() => this.onTabPress(tab.index)}
                disabled={(tab.index === 1 && (noBossBopisInfo || isDefaultBadge)) || keepAlive}
              >
                {this.renderTabText(tab.title, tab.isSelected)}
              </TabButton>
            );
          })}
        </TabContainer>
      </Container>
    );
  }
}

ToggleDeliveryMethod.propTypes = {
  isBossEligible: PropTypes.bool,
  keepAlive: PropTypes.bool,
  noBossBopisInfo: PropTypes.bool,
  onTabChange: PropTypes.func,
  selectedTab: PropTypes.number,
  pickupLabels: PropTypes.shape({}).isRequired,
  isDefaultBadge: PropTypes.bool,
};

ToggleDeliveryMethod.defaultProps = {
  isBossEligible: false,
  keepAlive: false,
  noBossBopisInfo: false,
  onTabChange: () => {},
  selectedTab: 0,
  isDefaultBadge: false,
};

export default withStyles(ToggleDeliveryMethod, styles);
export { ToggleDeliveryMethod as ToggleDeliveryMethodVanilla };
