// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import SocialProofMessage from '../views';

class SocialProofMessageContainer extends React.PureComponent {
  render() {
    const { socialProofMessage } = this.props;
    return (
      <SocialProofMessage
        socialProofMessage={socialProofMessage}
        top={socialProofMessage.variation === 'top'}
        longText={socialProofMessage?.priorityMsg?.length}
      />
    );
  }
}

SocialProofMessageContainer.propTypes = {
  socialProofMessage: PropTypes.string.isRequired,
};

export default connect(
  null,
  null
)(SocialProofMessageContainer);
