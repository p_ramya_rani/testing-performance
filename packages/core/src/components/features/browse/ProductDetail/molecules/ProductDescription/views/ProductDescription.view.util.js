// 9fbef606107a605d69c0edbcd8029e5d
// Function to construct Item label to show in  product description for multipacks
const getPrefixId = (getMultiPackAllColor, colorDisplayId, PartNumber) => {
  if (!Array.isArray(getMultiPackAllColor) || !colorDisplayId) {
    return PartNumber;
  }

  const { styleTypeUS, TCPMultiPackUSStore } =
    getMultiPackAllColor.find((multipack) => multipack.prodpartno === colorDisplayId) || {};

  switch (styleTypeUS) {
    case '0006':
      return `V Set-${PartNumber}`;
    case '0005':
      return `V-${PartNumber}`;
    case '0003':
      return TCPMultiPackUSStore ? `PV Set-${PartNumber}` : PartNumber;
    case '0002':
      return TCPMultiPackUSStore ? `PV-${PartNumber}` : PartNumber;
    default:
      return PartNumber;
  }
};

export default getPrefixId;
