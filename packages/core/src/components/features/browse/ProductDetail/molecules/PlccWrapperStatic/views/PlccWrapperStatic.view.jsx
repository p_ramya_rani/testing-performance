// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import PropTypes from 'prop-types';
import { Image, Anchor, BodyCopy } from '@tcp/core/src/components/common/atoms';
import { getAPIConfig } from '@tcp/core/src/utils/utils';

const PlccWrapperStatic = (props) => {
  const { pdpLabels, openModal } = props;
  const {
    myLabel = '',
    placeLabel = '',
    save30 = '',
    learnMoreLabel = '',
    creditCardLabel = '',
    plccDiscription = '',
    rewardsLabel = '',
    plccMoreDiscription = '',
    elementsPlccCardImageUrl = '/ecom/assets/static/PLCC_Card.svg',
  } = pdpLabels;
  const { assetHostTCP } = getAPIConfig();
  const plccCardPath = `${assetHostTCP}${elementsPlccCardImageUrl}`;
  return (
    <div className="hardcoded-plcc-wrapper">
      <Image className="hardcoded-plcc image" src={plccCardPath} />
      <Anchor
        onClick={openModal}
        noLink
        href="#"
        anchorVariation="primary"
        dataLocator="Learn More"
        target="_self"
      >
        <BodyCopy fontFamily="secondary" className="plcc-text-wrapper">
          <p>
            <span className="hardcoded-plcc-text text1">{save30}</span> {plccDiscription}{' '}
          </p>

          <p>
            {plccMoreDiscription} <span className="hardcoded-plcc-text text2">{myLabel}</span>{' '}
            <span className="hardcoded-plcc-text text3">{placeLabel}</span>{' '}
            <span className="hardcoded-plcc-text text4">{rewardsLabel}</span>{' '}
            <span className="hardcoded-plcc-text text5">{creditCardLabel}</span>{' '}
            <span className="hardcoded-plcc-text text6">{learnMoreLabel}</span>
          </p>
        </BodyCopy>
      </Anchor>
    </div>
  );
};

PlccWrapperStatic.propTypes = {
  pdpLabels: PropTypes.shape({}),
  openModal: PropTypes.func,
};

PlccWrapperStatic.defaultProps = {
  pdpLabels: {},

  openModal: () => {},
};

export default PlccWrapperStatic;
