// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const ProductDetailWrapper = styled.ScrollView`
  flex-direction: column;
  margin: 16px 12px;
  flex: 1;
`;

export const ProductDetailNewWrapper = styled.View`
  flex-grow: 1;
  margin: 36px 12px 40px 12px;
`;

export const BadgeTitleContainer = styled.View`
  margin-top: 12px;
  margin-bottom: 12px;
  justify-content: center;
  align-items: center;
  margin-left: 40px;
  margin-right: 40px;
`;

export const BadgeContainer = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  height: 15px;
`;

export const TitleContainer = styled.View`
  height: 44px;
  width: 100%;
  justify-content: center;
  align-items: center;
`;
