// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ToggleDeliveryMethodVanilla } from '../ToggleDeliveryMethod.view.native';

const tabData = [
  {
    index: 0,
    isSelected: true,
    title: 'Shipping',
  },
  {
    index: 1,
    isSelected: false,
    title: 'Pickup',
  },
  {
    index: 2,
    isSelected: false,
    title: 'No Rush Pickup',
  },
];

describe('RelatedOutfitsVanilla component', () => {
  let wrapper;

  beforeEach(() => {
    const props = {
      isBossEligible: false,
      keepAlive: false,
      noBossBopisInfo: false,
      onTabChange: jest.fn(),
      selectedTab: 0,
      pickupLabels: {
        lbl_Product_pickup_SHIPPING: 'Shipping',
        lbl_Product_pickup_PICKUP: 'Pickup',
        lbl_Product_pickup_NO_RUSH_PICKUP: 'No Rush Pickup',
      },
      isDefaultBadge: false,
    };
    wrapper = shallow(<ToggleDeliveryMethodVanilla {...props} />);
  });

  it('should renders correctly', () => {
    wrapper.setState({ tabData });
    expect(wrapper).toMatchSnapshot();
  });

  it('should render selected renderTabText correctly', () => {
    expect(wrapper.instance().renderTabText('12112_11', true)).toMatchSnapshot();
  });

  it('should render selected renderTabText false', () => {
    expect(wrapper.instance().renderTabText('', true)).toMatchSnapshot();
  });

  it('should return onTabPress', () => {
    wrapper.setState({ tabData });
    wrapper.setProps({ onTabChange: jest.fn() });
    expect(wrapper.instance().onTabPress(0)).toBeUndefined();
  });
});
