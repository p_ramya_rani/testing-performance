// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Image } from 'react-native';
import { PropTypes } from 'prop-types';
import { isGymboree, LAZYLOAD_HOST_NAME, getBrand } from '@tcp/core/src/utils';
import { BodyCopy, Anchor } from '../../../../../../common/atoms';
import {
  StyleRelatedOutfits,
  ImageStyleWrapper,
  OutfitWrapper,
  Container,
} from '../RelatedOutfits.native.style';
import ModuleQ from '../../../../../../common/molecules/ModuleQ';
import AccessibilityRoles from '../../../../../../../constants/AccessibilityRoles.constant';

const downIcon = require('../../../../../../../../../mobileapp/src/assets/images/carrot-small-down.png');
const upIcon = require('../../../../../../../../../mobileapp/src/assets/images/carrot-small-up.png');

class RelatedOutfits extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isAccordionOpen: true,
      showHeader: false,
    };
  }

  handleAccordionToggle = () => {
    const { isAccordionOpen } = this.state;
    this.setState({ isAccordionOpen: !isAccordionOpen });
  };

  getRelatedOutfitSlots = () => {
    const {
      navigation,
      selectedColorProductId,
      triggerStylyticsAnalytics,
      isNewReDesignCTL,
      fromPage,
      onRequestClose,
    } = this.props;

    const brand = getBrand() || '';
    const { showHeader } = this.state;

    return (
      <OutfitWrapper showHeader={showHeader} isNewReDesignCTL={isNewReDesignCTL}>
        <ModuleQ
          navigation={navigation}
          hostLazyLoad={LAZYLOAD_HOST_NAME.PDP}
          selectedColorProductId={selectedColorProductId}
          hideTabs
          divTabs={[]}
          showRelatedOutfitHeader={this.setShowHeader}
          isRelatedOutfit
          triggerStylyticsAnalytics={triggerStylyticsAnalytics}
          isNewReDesignCTL={isNewReDesignCTL}
          fromPage={fromPage}
          onRequestClose={onRequestClose}
          brand={brand}
        />
      </OutfitWrapper>
    );
  };

  setShowHeader = (value) => {
    const { setShowCompleteTheLook } = this.props;
    if (value) {
      this.setState({ showHeader: true }, () => setShowCompleteTheLook(value));
    } else {
      this.setState({ showHeader: false }, () => setShowCompleteTheLook(value));
    }
  };

  getGymStyling = () => {
    return isGymboree()
      ? { fontFamily: 'primary', fontWeight: 'semibold' }
      : { fontFamily: 'secondary', fontWeight: 'bold' };
  };

  render() {
    const { pdpLabels, accessibilityLabels, paddings, margins, background, isNewReDesignCTL } =
      this.props;
    const { completeTheLook } = pdpLabels;
    const { isAccordionOpen, showHeader } = this.state;
    return isNewReDesignCTL ? (
      <>
        {showHeader ? (
          <BodyCopy
            fontFamily={this.getGymStyling().fontFamily}
            fontWeight={this.getGymStyling().fontWeight}
            fontSize="fs20"
            color="gray.900"
            margin="20px 0 0"
            textAlign="left"
            letterSpacing="ls1"
            isAccordionOpen={isAccordionOpen}
            text={completeTheLook}
          />
        ) : null}
        {this.getRelatedOutfitSlots()}
      </>
    ) : (
      <Container
        paddings={paddings}
        margins={margins}
        background={background}
        showHeader={showHeader}
      >
        {showHeader && (
          <StyleRelatedOutfits
            onPress={this.handleAccordionToggle}
            accessible
            accessibilityRole={AccessibilityRoles.Button}
            accessibilityLabel={completeTheLook}
            accessibilityState={{ expanded: isAccordionOpen }}
          >
            <BodyCopy
              fontFamily="secondary"
              fontWeight="black"
              fontSize="fs14"
              isAccordionOpen={isAccordionOpen}
              text={completeTheLook && completeTheLook.toUpperCase()}
              textAlign="center"
            />
            <ImageStyleWrapper>
              <Anchor onPress={this.handleAccordionToggle}>
                <Image
                  source={isAccordionOpen ? upIcon : downIcon}
                  alt={
                    isAccordionOpen
                      ? accessibilityLabels.lbl_up_arrow_icon
                      : accessibilityLabels.lbl_down_arrow_icon
                  }
                />
              </Anchor>
            </ImageStyleWrapper>
          </StyleRelatedOutfits>
        )}

        {isAccordionOpen ? this.getRelatedOutfitSlots() : null}
      </Container>
    );
  }
}

RelatedOutfits.propTypes = {
  pdpLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  selectedColorProductId: PropTypes.number.isRequired,
  setShowCompleteTheLook: PropTypes.func,
  accessibilityLabels: PropTypes.shape({}),
  margins: PropTypes.string,
  paddings: PropTypes.string,
  background: PropTypes.string,
  triggerStylyticsAnalytics: PropTypes.func,
  isNewReDesignCTL: PropTypes.bool,
  onRequestClose: PropTypes.func,
  fromPage: PropTypes.string,
};

RelatedOutfits.defaultProps = {
  pdpLabels: {},
  navigation: {},
  setShowCompleteTheLook: null,
  accessibilityLabels: {},
  margins: null,
  paddings: null,
  background: null,
  isNewReDesignCTL: false,
  triggerStylyticsAnalytics: () => {},
  onRequestClose: () => {},
  fromPage: '',
};

export default RelatedOutfits;
export { RelatedOutfits as RelatedOutfitsVanilla };
