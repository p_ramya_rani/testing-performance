// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '../../../../../../utils/index';

const downArrowIcon = getIconPath('down_arrow_icon');
const upArrowIcon = getIconPath('up_arrow_icon');

export default css`
  ${(props) =>
    props.isNewPDPEnabled
      ? `
    border: ${props.theme.spacing.ELEM_SPACING.SM} solid
    ${props.theme.colorPalette.gray[300]};
      `
      : ` border-top: ${props.theme.spacing.ELEM_SPACING.XS} solid
    ${props.theme.colorPalette.gray[300]};
    `}

  ${(props) =>
    !props.isPDP
      ? `
          border-bottom: ${props.theme.spacing.ELEM_SPACING.XS} solid
          ${props.theme.colorPalette.gray[300]};
          `
      : ``}

  ${(props) =>
    props.isNewPDPEnabled
      ? `
          border-bottom: ${props.theme.spacing.ELEM_SPACING.XXXS} solid
          ${props.theme.colorPalette.gray[300]};
          `
      : `border-left: 0; border-right : 0;`}

  ${(props) =>
    !props.isBundleProduct
      ? `
            padding: 0 14px;
        `
      : `padding: 0`}

  @media ${(props) => props.theme.mediaQuery.medium} {
    ${(props) => (props.isBundleProduct ? `border: none` : '')};
    ${(props) => props.isNewPDPEnabled && `border:unset;`}
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    ${(props) =>
      props.isPDP
        ? `
      border-bottom:0;
      `
        : ``}
  }
  .button-wrapper {
    cursor: pointer;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0;
    padding: 0;
    text-decoration: underline;
    font-size: 10px;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    border: none;
    background: none;
    outline: none;
  }

  .part-number-section {
    margin: 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    text-align: right;
  }

  .show-description-list {
    display: none;
  }

  .product-detail-footer {
    display: flex;
    justify-content: space-between;
    align-items: baseline;
  }
  .claim-message {
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0 7px;
  }

  .short-description {
    line-height: 22px;
  }

  max-height: initial;

  .product-desc-heading {
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
    ${(props) =>
      !props.isNewPDPEnabled && `background: url(${upArrowIcon}) no-repeat right 0 bottom 7px;`}
  }

  .show-accordion-toggle {
    ${(props) =>
      !props.isNewPDPEnabled && `background: url(${downArrowIcon}) no-repeat right 0 bottom 7px;`}
  }
  .common-claim-message {
    padding-top: 0;
  }
  .introduction-text {
    max-height: 90px;
    overflow: hidden;
    .list-content {
      padding-left: 18px;
      li {
        list-style-type: disc;
        line-height: 22px;
      }
    }
  }

  .show-more-expanded {
    max-height: initial;
    overflow: visible;
  }

  @media ${(props) => props.theme.mediaQuery.large} {
    .product-desc-heading {
      background: none;
    }
    .show-description-list {
      display: block;
    }
    .introduction-text {
      max-height: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    }
    .show-more-expanded {
      max-height: 100%;
      overflow: visible;
    }
    .button-wrapper {
      border: none;
      outline: none;
    }
    ${(props) =>
      !props.isBundleProduct
        ? `
      border: 1px solid ${props.theme.colorPalette.gray[500]}
      `
        : `border: none`}
    border-left: 0;
    border-right: 0;
  }
`;
