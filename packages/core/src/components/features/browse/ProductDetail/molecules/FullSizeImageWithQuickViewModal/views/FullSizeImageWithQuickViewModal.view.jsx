// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { fromJS } from 'immutable';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles, { customHeaderStyle } from '../styles/FullSizeImageWithQuickViewModal.style';
import Modal from '../../../../../../common/molecules/Modal';
import ProductImages from '../../../../../../common/organisms/ProductImages';
import { getLocator } from '../../../../../../../utils';
import ProductColorChipsSelector from '../../../../../../common/molecules/ProductColorChipSelector';
import FullSizeImageWithQuickViewConstant from './FullSizeImageWithQuickViewModal.constants';

const FullSizeImageWithQuickViewModal = (props) => {
  const {
    isMobile,
    onCloseClick,
    name,
    isThumbnailListVisible,
    images,
    colorChipSelector,
    accessibilityLabels,
    ratingsProductId,
    currentProduct: { imagesByColor },
    isGiftCard,
    isPDPSmoothScrollEnabled,
    primaryBrand,
    alternateBrand,
  } = props;
  const { selectColor } = colorChipSelector;
  let { colorList } = colorChipSelector;
  colorList = fromJS(colorList);
  return (
    <Modal
      isOpen
      onRequestClose={onCloseClick}
      overlayClassName="TCPModal__Overlay"
      className="TCPModal__Content"
      dataLocatorHeader={getLocator('pdp_product_titles')}
      dataLocator={getLocator('pdp_full_size_image_modal')}
      closeIconDataLocator={getLocator('pdp_zoomed_image_closed_btn')}
      heading={name}
      fixedWidth
      widthConfig={{ small: '375px', medium: '760px', large: '1200px' }}
      heightConfig={{ height: '95%' }}
      inheritedStyles={customHeaderStyle}
      imagesByColor={imagesByColor}
      primaryBrand={primaryBrand}
      alternateBrand={alternateBrand}
    >
      <div className="overlay-content">
        {!isMobile && (
          <form noValidate>
            <Row>
              <Col colSize={{ small: 10, medium: 10, large: 10 }}>
                <div className="select-value-wrapper">
                  {colorList && colorList.size > 0 && (
                    <div className="color-selector">
                      <Field
                        id="colorSwatchModal"
                        name="colorSwatchModal"
                        component={ProductColorChipsSelector}
                        colorFitsSizesMap={colorList}
                        onChange={selectColor}
                        imagesByColor={imagesByColor}
                        isGiftCard={isGiftCard}
                        primaryBrand={primaryBrand}
                        alternateBrand={alternateBrand}
                      />
                    </div>
                  )}
                </div>
              </Col>
            </Row>
          </form>
        )}
        <ProductImages
          productName={name}
          isMobile={isMobile}
          images={images}
          isShowBigSizeImages
          isFullSizeVisible={false}
          isFullSizeForTab
          isZoomEnabled={false}
          isThumbnailListVisible={isThumbnailListVisible}
          accessibilityLabels={accessibilityLabels}
          ratingsProductId={ratingsProductId}
          isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
          primaryBrand={primaryBrand}
          alternateBrand={alternateBrand}
        />
      </div>
    </Modal>
  );
};

FullSizeImageWithQuickViewModal.propTypes = {
  images: PropTypes.arrayOf(
    PropTypes.shape({
      iconSizeImageUrl: PropTypes.string.isRequired,
      regularSizeImageUrl: PropTypes.string.isRequired,
      bigSizeImageUrl: PropTypes.string.isRequired,
      superSizeImageUrl: PropTypes.string.isRequired,
    })
  ).isRequired,

  /** function to call to close this modal */
  onCloseClick: PropTypes.func.isRequired,

  /** Flags if the thumnails should be visible. */
  isThumbnailListVisible: PropTypes.bool,
  isMobile: PropTypes.bool,
  name: PropTypes.string.isRequired,
  ratingsProductId: PropTypes.string.isRequired,
  colorChipSelector: PropTypes.objectOf(
    PropTypes.shape({
      colorList: PropTypes.array,
      selectColor: PropTypes.func,
    })
  ),
  accessibilityLabels: PropTypes.shape({}),
  currentProduct: PropTypes.shape({
    imagesByColor: PropTypes.shape({}),
  }),
  isGiftCard: PropTypes.bool,
  isPDPSmoothScrollEnabled: PropTypes.bool,
};

FullSizeImageWithQuickViewModal.defaultProps = {
  isMobile: true,
  isThumbnailListVisible: true,
  colorChipSelector: {},
  accessibilityLabels: {},
  currentProduct: {
    imagesByColor: {},
  },
  isGiftCard: false,
  isPDPSmoothScrollEnabled: false,
};

export default compose(
  connect((state, props) => {
    const formName =
      props.customFormName || FullSizeImageWithQuickViewConstant.FULL_SIZE_QUICK_VIEW_FORM;
    return {
      form: formName,
      enableReinitialize: true,
    };
  }),
  reduxForm()
)(withStyles(errorBoundary(FullSizeImageWithQuickViewModal), styles));

export { FullSizeImageWithQuickViewModal as FullSizeImageWithQuickViewModalVanilla };
