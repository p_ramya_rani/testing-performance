import { css } from 'styled-components';

export default css`
  width: 100%;
  height: auto;
  padding: 8px 16px;
  box-sizing: border-box;
  background-color: #eee;
  border: 1px solid #ccc;
  .prod-img {
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
  }
  .product-thumbnail {
    height: 50px;
  }
  .vmp-pack-title {
    font-size: 16px;
    padding: 20px 0px;
    display: flex;
    align-items: center;
    .vmp-pack-name {
      color: #000;
      font-weight: bold;
    }
    .vmp-pack-sub-title {
      margin-left: 10px;
      font-size: 12px;
      font-weight: 400;
    }
  }
  .threshold-explained {
    border: 1px soild #000;
    padding: 10px 10px;
    background-color: #fffff0;
    margin-top: 16px;
    ul {
      padding-left: 10px;
    }
    li {
      padding-left: 24px;
      font-size: 12px;
    }
  }
  .vmp-matrix-tbl {
    display: block;
    overflow-x: auto;
    white-space: nowrap;
    box-sizing: border-box;
    width: 100%;
    padding: 5px 0px;
    border: none;
    border-collapse: collapse;
    td,
    th {
      padding: 3px 20px;
      text-align: center;
    }
    td {
      padding-top: 5px;
    }
    tr:nth-child(even) {
      background-color: #fffff0;
    }
  }
`;
