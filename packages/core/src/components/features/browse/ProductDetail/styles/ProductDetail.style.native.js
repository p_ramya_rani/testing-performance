// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import get from 'lodash/get';
import { isAndroid } from '@tcp/core/src/utils/utils.app';
import StyleGuide from '@tcp/core/styles/themes/TCP';
import { StyleSheet } from 'react-native';
import {
  colorWhite,
  colorMortar,
  colorGainsboro,
  colorPrussian,
  colorWhiteSmoke,
  colorNobel3,
  fontFamilyNunitoExtraBold,
  fontFamilyNunitoSemiBold,
  fontSizeFourteen,
  colorBlack,
} from '../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.styles';

const getAdditionalStyle = (props) => {
  const { backgroundColor, theme, margins, paddings } = props;
  const { colorPalette } = theme;
  const bgColor = get(colorPalette, backgroundColor, '#f3f3f3');
  return {
    ...(backgroundColor && { background: bgColor }),
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
  };
};

const transparentHeaderStyle = (props) => {
  return isAndroid() && props.isTransparent ? `margin-top: 0` : '';
};

export const PageContainer = styled.View`
  justify-content: center;
  ${transparentHeaderStyle}
`;

export const SizeDrawerUnderlay = styled.View`
  height: 200;
  background-color: white;
  width: 100%;
  bottom: 0;
  position: absolute;
`;

export const ReviewsAndRattingContainer = styled.View`
  background: ${(props) => props.theme.colorPalette.white};
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const Margin = styled.View`
  justify-content: center;
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  ${getAdditionalStyle};
`;

export const PromoMiddleContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const PromoBottomContainer = styled.View`
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const LoyaltyBannerView = styled.View`
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0;
`;

export const RecommendationWrapper = styled.View`
  background: ${(props) => props.theme.colorPalette.white};
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const RecommendationMargin = styled.View`
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  ${getAdditionalStyle};
`;

export const RecommendationOosWrapper = styled.View`
  background: ${(props) => props.theme.colorPalette.gray[300]};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const AddToBagSBPWrapper = styled.TouchableOpacity`
  background-color: ${colorPrussian};
  border-width: 1;
  border-color: ${colorNobel3};
  height: 42;
  flex: 1;
  margin-left: 8;
  justify-content: center;
  align-self: center;
  width: 100%;
`;

export const BadgeTitleContainer = styled.View`
  justify-content: center;
`;

export const AddToBagText = styled.Text`
  font-size: ${fontSizeFourteen};
  font-family: ${fontFamilyNunitoExtraBold};
  color: ${colorWhite};
  letter-spacing: 0.4;
  text-align: center;
`;

export const ModalBoxCloseImage = styled.Image`
  height: 20px;
  width: 20px;
`;

export const ModalBoxBtn = styled.TouchableOpacity`
  ${(props) =>
    props.isPdpNewReDesign
      ? `
  height: 44px;
  top: 1px;
  right: 1px;
  width: 44px;
  position: absolute;
  justify-content: center;
  align-items: center;
  `
      : `
  height: 20px;
  top: 18px;
  width: 20px;
  left: 15px
  `}
  z-index: 1;
`;

export const AtbButton = styled.View`
  align-self: center;
  flex: 1;
  justify-content: center;
  margin-left: 8px;
  ${(props) => (props.isSBP ? `margin-top: 10px;` : ``)}
`;

export const FooterWrapper = styled.View`
  justify-content: space-around;
  align-content: center;
  flex-direction: row;
  bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  ${(props) =>
    props.isPdpNewReDesign &&
    `margin-top: -20px;
    margin-bottom: 10px;
  `}
  margin-left: 7%;
  ${(props) =>
    props.isNewDesignBoss &&
    `
    justify-content: center;
    align-items: center;
    margin-left: 0;
  `}
`;

export const PLPModalBoxBtn = styled.TouchableOpacity`
  height: 20px;
  top: 16px;
  right: 16px;
  width: 20px;
  position: absolute;
  flex: 0.2;
`;

export const TitleWrapper = styled.View`
  border-radius: 16px;
  align-self: center;
  text-align: center;
  justify-content: center;
  align-items: center;
  flex: 0.6;
`;

export const HeaderView = styled.View`
  flex-direction: row;
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
  width: 100%;
  height: 54px;
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
`;

export const ProductDetailStyle = StyleSheet.create({
  addToBagText: {
    color: colorWhite,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeFourteen,
    letterSpacing: 0.4,
    textAlign: 'center',
  },
  customIcon: {
    color: colorMortar,
    marginBottom: 4,
    marginLeft: 16,
  },
  lazyLoadScrollView: {
    height: 200,
  },
  lazyLoadScrollViewSBP: {
    height: 100,
  },
  modalBox: {
    borderRadius: 16,
    height: '39%',
    padding: 16,
    paddingTop: 0,
  },
  modalBoxImage: {
    height: 20,
    width: 20,
  },
  modalBoxNew: (isBossBopis) => {
    return {
      borderRadius: 16,
      height: 'auto',
      padding: 16,
      paddingBottom: isBossBopis ? 80 : 16,
      paddingTop: 0,
    };
  },
  modalBoxText: {
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    marginBottom: 16,
    marginTop: 4,
    textAlign: 'center',
  },
  modalBoxTouchable: {
    height: 20,
    position: 'absolute',
    right: 18,
    top: 18,
    width: 20,
  },
  modalBoxTouchable2: {
    alignSelf: 'center',
    backgroundColor: colorPrussian,
    height: 42,
    justifyContent: 'center',
    marginTop: '-10%',
    width: '100%',
  },
  modalBoxView: {
    alignSelf: 'center',
    backgroundColor: StyleGuide.colors.WHITE,
    borderRadius: 2,
    height: 4,
    marginVertical: 16,
    width: 95,
  },
  pageContainer: {
    backgroundColor: colorWhite,
  },
  text2: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeFourteen,
    letterSpacing: 0.4,
    textAlign: 'center',
  },
  touchableOpacity2: {
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: colorWhiteSmoke,
    borderColor: colorNobel3,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    height: 42,
    justifyContent: 'center',
    marginRight: 8,
  },
  touchableOpacity3: {
    alignSelf: 'center',
    backgroundColor: colorPrussian,
    borderColor: colorNobel3,
    borderWidth: 1,
    flex: 1,
    height: 42,
    justifyContent: 'center',
    marginLeft: 8,
    width: '100%',
  },
  touchableOpacity5: {
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: colorWhite,
    borderRadius: 16,
    elevation: 22,
    flex: 1,
    flexDirection: 'row',
    height: 46,
    justifyContent: 'center',
    marginRight: 8,
    shadowColor: colorBlack,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 15,
  },
  view2: {
    backgroundColor: isAndroid() ? colorWhiteSmoke : colorWhite,
    borderColor: colorGainsboro,
    bottom: 0,
    justifyContent: 'center',
    padding: 16,
    position: 'absolute',
    width: '100%',
    zIndex: 10,
  },
});

export const OosText = styled.Text`
  font-family: Nunito-Black;
  font-size: ${(props) => props.theme.fonts.fontSize.heading.small.h6};
  font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  text-align: center;
  color: ${(props) => props.theme.colors.TEXT.DARK};
  margin-top: 45px;
`;
