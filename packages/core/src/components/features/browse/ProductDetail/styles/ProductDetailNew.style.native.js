// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import get from 'lodash/get';
import { isAndroid } from '@tcp/core/src/utils/utils.app';
import { StyleSheet, Dimensions } from 'react-native';
import {
  colorWhite,
  colorMortar,
  colorGainsboro,
  colorPrussian,
  colorWhiteSmoke,
  colorNobel3,
  fontFamilyNunitoExtraBold,
  fontFamilyNunitoSemiBold,
  fontSizeFourteen,
} from '../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.styles';

const getAdditionalStyle = (props) => {
  const { backgroundColor, theme, margins, paddings } = props;
  const { colorPalette } = theme;
  const bgColor = get(colorPalette, backgroundColor, '#f3f3f3');
  return {
    ...(backgroundColor && { background: bgColor }),
    ...(margins && { margin: margins }),
    ...(paddings && { padding: paddings }),
  };
};

const transparentHeaderStyle = (props) => {
  return isAndroid() && props.isTransparent ? `margin-top: 0` : '';
};

export const ImageStyleContainer = styled.View`
  margin-left: 16px;
`;

export const PageContainer = styled.View`
  justify-content: center;
  ${transparentHeaderStyle}
`;

export const Container = styled.View`
  flex-grow: 1;
  margin-bottom: 30;
`;

export const StickyContainer = styled.View`
  position: absolute;
  bottom: 0;
  width: 100%;
`;

export const ProductRatingDetailButton = styled.TouchableOpacity`
  shadow-color: ${(props) => props.theme.colors.BLACK};
  box-shadow: ${(props) => props.theme.shadow.NEW_DESIGN.BOX_SHADOW};
  elevation: 5;
  background-color: ${(props) => props.theme.colors.WHITE};
  border-radius: 16px;
  flex-direction: row;
  align-items: center;
  padding: 16px;
  ${(props) => (props.isAndroid && props.reviewsCount === 0 ? `margin-bottom: 35px` : ``)}
`;

export const ProductRatingDetailArrow = styled.TouchableOpacity`
  justify-content: center;
  position: absolute;
  right: 0;
  padding-right: 16px;
`;

export const Margin = styled.View`
  justify-content: center;
  ${getAdditionalStyle};
`;

export const PromoMiddleContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const PromoBottomContainer = styled.View`
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const LoyaltyBannerView = styled.View`
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.LRG} 0;
`;

export const RecommendationWrapper = styled.View``;

export const RecommendationMargin = styled.View`
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  ${getAdditionalStyle};
`;

export const RecommendationOosWrapper = styled.View`
  background: ${(props) => props.theme.colorPalette.gray[300]};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const AddToBagSBPWrapper = styled.TouchableOpacity`
  background-color: ${colorPrussian};
  border-width: 1;
  border-color: ${colorNobel3};
  height: 42;
  flex: 1;
  margin-left: 8;
  justify-content: center;
  align-self: center;
  width: 100%;
`;

export const AddToBagText = styled.Text`
  font-size: ${fontSizeFourteen};
  font-family: ${fontFamilyNunitoExtraBold};
  color: ${colorWhite};
  letter-spacing: 0.4;
  text-align: center;
`;

export const ProductDetailStyleRedesign = StyleSheet.create({
  addToBagText: {
    color: colorWhite,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeFourteen,
    letterSpacing: 0.4,
    textAlign: 'center',
  },
  customIcon: {
    color: colorMortar,
    marginBottom: 4,
    marginLeft: 16,
  },
  lazyLoadScrollView: {
    height: 200,
  },
  lazyLoadScrollViewSBP: {
    height: 100,
  },
  modalBox: {
    borderRadius: 16,
    height: '39%',
    padding: 16,
    paddingTop: 0,
  },
  modalBoxImage: {
    height: 20,
    width: 20,
  },
  modalBoxReview: {},
  modalBoxText: {
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    marginTop: 4,
    textAlign: 'center',
  },
  modalBoxTouchable: {
    height: 20,
    position: 'absolute',
    right: 18,
    top: 18,
    width: 20,
  },
  modalBoxTouchable2: {
    alignSelf: 'center',
    backgroundColor: colorPrussian,
    height: 42,
    justifyContent: 'center',
    marginTop: '-10%',
    width: '100%',
  },
  modalBoxView: {
    alignSelf: 'center',
    backgroundColor: colorMortar,
    borderRadius: 2,
    height: 4,
    marginVertical: 16,
    width: 95,
  },
  otherDepartmentList: {
    marginVertical: 16,
  },
  pageContainer: {
    backgroundColor: colorWhite,
  },
  text2: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeFourteen,
    letterSpacing: 0.4,
    textAlign: 'center',
  },
  touchableOpacity2: {
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: colorWhiteSmoke,
    borderColor: colorNobel3,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    height: 42,
    justifyContent: 'center',
    marginRight: 8,
  },
  touchableOpacity3: {
    alignSelf: 'center',
    backgroundColor: colorPrussian,
    borderColor: colorNobel3,
    borderWidth: 1,
    flex: 1,
    height: 42,
    justifyContent: 'center',
    marginLeft: 8,
    width: '100%',
  },
  view2: {
    backgroundColor: colorWhite,
    borderColor: colorGainsboro,
    bottom: 0,
    justifyContent: 'center',
    padding: 16,
    position: 'absolute',
    width: '100%',
    zIndex: 10,
  },
});

export const OosText = styled.Text`
  font-family: Nunito-Black;
  font-size: ${(props) => props.theme.fonts.fontSize.heading.small.h6};
  font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  text-align: center;
  color: ${(props) => props.theme.colors.TEXT.DARK};
  margin-top: 45px;
`;

export const PullDrawerWrapper = styled.View`
  border-radius: 3px;
  background-color: ${(props) => props.theme.colors.WHITE};
  width: 100;
  opacity: 1;
`;

export const PullDrawerContent = styled.View`
  background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  min-height: 103%;
  margin-bottom: 50px;
`;

export const PullDrawerContentScrollView = styled.View`
  background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  min-height: ${Dimensions.get('window').height};
`;

export const PullDrawerHeader = styled.View`
  height: 64px;
  position: absolute;
  bottom: ${(props) => (!props.isDrawerOpen ? -18 : -2)};
  width: 100%;
  background-color: ${(props) => props.theme.colors.WHITE};
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  shadow-color: ${(props) => props.theme.colors.BLACK};
  shadow-opacity: 0.09;
  shadow-offset: 0 -3px;
  align-items: center;
  justify-content: center;
  transform: scaleY(${(props) => (!props.isDrawerOpen ? 0.5 : 1)});
  ${(props) =>
    props.isAndroid &&
    `
  border-top-width: 2;
  border-left-width: 2;
  border-right-width: 2;
  border-color: ${props.theme.colors.PRIMARY.LIGHTGRAY};
  `}
`;

export const BadgeTitleContainer = styled.View`
  justify-content: space-around;
  align-items: flex-start;
  width: 100%;
`;

export const ShareContainer = styled.View`
  height: 36px;
  width: 36px;
  border-radius: 10px;
  position: absolute;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  background-color: ${(props) => props.theme.colors.TRANSLUCENT};
  top: ${(props) => props.defaultPositions.top};
  left: ${(props) => props.defaultPositions.left};
`;

export const FavoriteContainer = styled.View`
  flex-direction: column;
  align-items: center;
  justify-content: ${(props) => (props.isSBP && isAndroid() ? 'flex-end' : 'center')};
  position: absolute;
  z-index: 900;
  width: 36px;
  height: 36px;
  border-radius: 10px;
  background-color: ${(props) => props.theme.colors.TRANSLUCENT};
  top: ${(props) => props.defaultPositions.top};
  left: ${(props) => props.defaultPositions.left};
`;

export const FavoriteIconContainer = styled.View`
  flex: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const FavoriteCountContainer = styled.View`
  top: -2;
  height: 12px;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const AnimationContainer = styled.View`
  z-index: 101;
`;
export const ReviewsRatingsHeader = styled.View`
  flex-direction: row;
  margin-top: ${(props) => (props.isAndroid ? '0px' : '40px')};
  padding: 10px 0;
  justify-content: center;
`;

export const ReviewsRatingsHeaderTitle = styled.View`
  justify-content: center;
`;

export const ModalBoxReviewBtn = styled.TouchableOpacity`
  left: 0;
  padding-left: 10px;
  margin-top: 8px;
  position: absolute;
`;

export const PullDrawerProductSummary = styled.View`
  ${(props) => `
    background-color: ${props.theme.colors.WHITE};
    box-shadow: ${props.theme.shadow.NEW_DESIGN.BOX_SHADOW};
    elevation: 10;
  `}
`;

export const ProductSummaryContainer = styled.View`
  padding: 0;
`;

export const SkeletonContainer = styled.View`
  height: 320px;
  border-width: 1;
  width: 100%;
  border-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  margin-top: -10px;
  shadow-color: ${(props) => props.theme.colors.BLACK};
  shadow-opacity: 0.09;
  shadow-offset: ${(props) => (props.isAndroid ? '0px 2px' : '0px -6px')};
  z-index: 101;
`;

export const SkeletonHeaderContainer = styled.View`
  height: 30px;
  width: 100%;
  elevation: 5;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => props.theme.colors.WHITE};
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
`;

export const SkeletonHeaderContent = styled.View`
  border-radius: 3px;
  height: 5;
  background-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
  width: 100;
`;

export const SkeletonBodyContainer = styled.View`
  padding-left: 14px;
  background-color: ${(props) => props.theme.colors.WHITE};
`;

export const SkeletonProdSummaryContainer = styled.View`
  height: 125px;
  flex-direction: column;
  justify-content: space-around;
`;

export const SkeletonSwatchesContainer = styled.View`
  height: 85px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  width: 110%;
  margin-left: -12px;
`;

export const SkeletonSwatch = styled.View`
  margin-right: 12px;
  height: 65px;
  width: 60px;
`;

export const SkeletonMulPackContainer = styled.View`
  height: 85px;
`;

export const IconWrapper = styled.View`
  top: 1px;
  ${(props) =>
    !props.isNewDesignBoss &&
    `
    top: 4px;
    left: -29px;
  `}
`;

export const PromoBelowRecsContainer = styled.View`
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const SizeHeaderContainer = styled.View`
  background-color: ${(props) => props.theme.colors.PRIMARY.LIGHTGRAY};
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
  margin: -16px;
  align-items: center;
`;

export const SizeToggleContainer = styled.View`
  flex-direction: row;
  width: 100%;
`;

export const RowContainer = styled.View`
  ${getAdditionalStyle};
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const ContainerOfferNoRush = styled.View`
  background-color: ${(props) => props.theme.colorPalette.yellow[500]};
  padding: 3px 7.4px 3px 6.6px;
  border-radius: 11px;
  margin-bottom: 10px;
`;

export const SocialProofWrapper = styled.View`
  bottom: ${(props) => props.height};
  position: absolute;
  z-index: ${(props) => props.zIndex};
  width: 100%;
  height: auto;
`;
