import styled from 'styled-components';

export const RowView = styled.View`
  flex-direction: row;
  margin-top: ${(props) => props.marginTop ?? 0};
`;

export const MainView = styled.View`
  flex-direction: row;
  width: 100%;
  background-color: ${(props) => props.backGroundColor};
  border-radius: 12px;
  padding-top: 6px;
  padding-right: 10px;
  padding-bottom: 6px;
  padding-left: ${(props) => (props.isFromCollection ? 0 : 10)};
  margin-top: ${(props) => (props.isFromCollection ? 0 : 12)};
  margin-bottom: ${(props) => (props.isFromCollection ? 0 : 20)};
`;

export const UnderLine = styled.View`
  margin-left: 10px;
  height: 1px;
  background-color: ${(props) => props.theme.colorPalette.blue[800]};
`;

export const ViewWrap = styled.View`
  flex-wrap: wrap;
  margin-top: ${(props) => props.marginTop ?? 0};
`;
