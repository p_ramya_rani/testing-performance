// 9fbef606107a605d69c0edbcd8029e5d
import PRODUCTDETAIL_CONSTANTS from './ProductDetail.constants';

export const setProductDetails = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_PRODUCT_DETAILS,
    payload,
  };
};

export const setMultiPackDetails = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_MULTIPRODUCT_DETAILS,
    payload,
  };
};

export const setDisableSelectedTab = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_DISABLE_SELECTED_TAB,
    payload,
  };
};

export const singlePageLoadAction = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SINGLE_TAB_STORE,
    payload,
  };
};

export const updateMultipackSelection = (payload) => ({
  type: PRODUCTDETAIL_CONSTANTS.MULTIPACK_SELECTION,
  payload,
});

export const setProductDetailsDynamicData = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_PRODUCT_DETAILS_DYNAMIC_DATA,
    payload,
  };
};

export const getProductDetails = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.FETCH_PRODUCT_DETAILS,
    payload,
  };
};

export const setAddToFavoritePDP = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_ADD_TO_FAVORITE,
    payload,
  };
};

export const setPageUrl = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_PAGE_URL,
    payload,
  };
};

export const setPDPLoadingState = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_PDP_LOADING_STATE,
    payload,
  };
};

export const loadSocialProofData = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.LOAD_SOCIAL_PROOF_DATA,
    payload,
  };
};

export const clearSocialProofData = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.CLEAR_SOCIAL_PROOF_DATA,
    payload,
  };
};

export const setPDPDisplayPriceState = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_PDP_DISPLAY_PRICE,
    payload,
  };
};

export const resetCountUpdated = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.RESET_COUNT_UPDATED,
    payload,
  };
};

export const setIsPDPCalledFromRecommendation = () => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SET_PDP_CALLED_VIA_RECOMMENDATION,
  };
};
export const updateSelectedSizeStatus = (payload) => {
  return {
    type: PRODUCTDETAIL_CONSTANTS.SELECTED_SIZE_STATUS,
    payload,
  };
};
