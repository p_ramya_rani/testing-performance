/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import findIndex from 'lodash/findIndex';
import { createSelector } from 'reselect';
import PRODUCTDETAIL_CONSTANTS from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.constants';
import {
  getIsGuest,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getLabelValue, getABtestFromState, isServer } from '../../../../../utils';
import getAddedToBagFormValues from '../../../../../reduxStore/selectors/form.selectors';
import { processBreadCrumbs } from '../../ProductListing/container/ProductListing.util';
import { getProductDetails } from '../../../CnC/CartItemTile/container/CartItemTile.selectors';

export const getNavTree = (state) => {
  return state && state.Navigation.navigationData;
};
/*
const selectSelf = (state) =>  state;
export const prodDetails = createSelector( selectSelf, (state) => state.ProductDetail);
*

/*
export const prodDetails = (state) => {
  return state.ProductDetail;
};
*/

const getProdDetailMemo = (state) => {
  return state.ProductDetail;
};
export const prodDetails = createSelector(getProdDetailMemo, (prodDetailsDta) => prodDetailsDta);

export const getMultiPackProductInfo = (state) => {
  return state && state.ProductDetail.newPartNumber;
};

export const getMultiPackCount = (state) => {
  return state && state.ProductDetail.multiPackCount;
};

export const getSocialProofMessage = (state) => {
  return state && state?.ProductDetail?.socialProofData?.socialProofMsg;
};

export const getSinglePageLoad = (state) => {
  return state && state.ProductDetail.singlepageload;
};

export const getInitialMultipackMapping = (state) => {
  return state && state.ProductDetail && state.ProductDetail.availableTCPMultipackProductMapping;
};

export const getInitialAvailableTCPmapNewStyleId = (state) => {
  return state && state.ProductDetail && state.ProductDetail.availableTCPmapNewStyleId;
};

export const getInitialTCPStyleQTY = (state) => {
  return state && state.ProductDetail && state.ProductDetail.initialSelectedQty;
};

export const getAllMultiPackData = (state) => {
  return state && state.ProductDetail.getMultiPackAllColor;
};

export const getAllNewMultiPackData = (state) => {
  return state && state.ProductDetail && state.ProductDetail.getNewMultiPackAllColor;
};

export const partIdInformation = (state) => {
  return state && state.ProductDetail && state.ProductDetail.partIdDetail;
};

const getProductListing = (state) => state.ProductListing;

export const getBreadCrumbs = createSelector(
  getProductListing,
  getProdDetailMemo,
  (ProductListing, ProductDetail) => {
    const productListingBreadCrumbTrail = ProductListing && ProductListing.breadCrumbTrail;
    let breadCrumbsData;
    if (productListingBreadCrumbTrail) {
      breadCrumbsData = processBreadCrumbs(productListingBreadCrumbTrail);
    }
    breadCrumbsData = processBreadCrumbs(ProductDetail && ProductDetail.breadCrumbs);
    return breadCrumbsData;
  }
);

export const getSelectedMultipack = (state) => {
  return state && state.ProductDetail && state.ProductDetail.selectedMultipack;
};

export const getDisableMultiPackTab = (state) => {
  return state && state.ProductDetail && state.ProductDetail.disabledTab;
};

export const getProductInfoFromRecommendationData = ({ pid, details }) => {
  try {
    const [...recKeys] = details.keys();
    let productData = {};
    let finalProductData = {};
    recKeys.forEach((recKey) => {
      const prodObj = details.get(recKey, null);
      productData =
        prodObj &&
        prodObj.products.find(
          (product) => product && product.productInfo && product.productInfo.uniqueId === pid
        );
      if (productData) {
        finalProductData = productData;
      }
    });

    const {
      productInfo: {
        uniqueId,
        name,
        listPrice,
        offerPrice,
        priceRange,
        isGiftCard,
        tcpStyleQty,
        tcpStyleType,
      } = {},
      colorsMap,
      imagesByColor,
    } = finalProductData;
    return {
      uniqueId,
      name,
      colorFitsSizesMap: colorsMap,
      imagesByColor,
      listPrice,
      offerPrice,
      priceRange,
      isGiftCard,
      tcpStyleQty,
      tcpStyleType,
    };
  } catch (err) {
    return null;
  }
};

export const getProductInfoFromModule = ({ pid, details }) => {
  try {
    const productData = details.find((product) => product && product.uniqueId === pid);
    const { uniqueId, product_name: productName, imageUrl } = productData;
    return {
      uniqueId,
      name: productName,
      colorFitsSizesMap: [],
      imagesByColor: imageUrl,
      productInfo: [],
    };
  } catch (err) {
    return null;
  }
};

export const getProductInfoFromOutfit = ({ pid, details }) => {
  try {
    const {
      productId,
      name,
      listPrice,
      offerPrice,
      colorFitsSizesMap,
      imagesByColor,
      highListPrice,
      highOfferPrice,
      lowListPrice,
      lowOfferPrice,
      onlyListPrice,
      TCPStyleQTY,
      TCPStyleType,
    } = details.find((product) => product && product.productId === pid);
    return {
      uniqueId: productId,
      name,
      colorFitsSizesMap,
      imagesByColor,
      listPrice,
      offerPrice,
      priceRange: {
        highListPrice,
        highOfferPrice,
        lowListPrice,
        lowOfferPrice,
        onlyListPrice,
      },
      TCPStyleQTY,
      TCPStyleType,
    };
  } catch (err) {
    return null;
  }
};

export const getProductInfoFromBundle = ({ pid, details }) => {
  try {
    let selectedProductIndex = 0;
    details.forEach((item, index) => {
      const productIndex = findIndex(item.products && item.products.colorFitsSizesMap, {
        colorDisplayId: pid,
      });
      if (productIndex > -1) {
        selectedProductIndex = index;
      }
    });
    const productList = details && details[selectedProductIndex].products.colorFitsSizesMap;
    const imagesByColor = details && details[selectedProductIndex].products.imagesByColor;
    const {
      colorDisplayId,
      name,
      listPrice,
      offerPrice,
      priceRange,
      miscInfo: { badge2 },
      color,
    } = productList.find((product) => product && product.colorDisplayId === pid);
    return {
      uniqueId: colorDisplayId,
      name,
      colorFitsSizesMap: [{ color: { ...color, colorDisplayId } }],
      imagesByColor,
      listPrice,
      offerPrice,
      priceRange,
      badge2,
    };
  } catch (err) {
    return null;
  }
};

// eslint-disable-next-line sonarjs/cognitive-complexity
export const getProductInfoFromProductListingData = ({ pid, details, type }) => {
  try {
    let selectedProductIndex = 0;
    let selectedProduct = {};
    if (details) {
      if (type === 'mbox_Recommendation') {
        details.forEach((item, index) => {
          if (item.uniqueId === pid) {
            selectedProductIndex = index;
            selectedProduct = item;
          }
        });
      } else {
        details.forEach((item, index) => {
          const productIndex = findIndex(item, { uniqueId: pid });
          if (productIndex > -1) {
            selectedProductIndex = index;
          }
        });
        const productList = details && details[selectedProductIndex];
        selectedProduct =
          productList &&
          productList.find(
            (product) => product && product.productInfo && product.productInfo.uniqueId === pid
          );
      }

      if (selectedProduct) {
        const {
          productInfo: {
            uniqueId,
            name,
            listPrice,
            offerPrice,
            priceRange,
            isGiftCard,
            tcpStyleQty,
            tcpStyleType,
            ratings,
            reviewsCount,
            unbxdRatings,
            unbxdReviewsCount,
            primaryBrand,
          },
          colorsMap,
          imagesByColor,
        } = selectedProduct;

        const selectedProductMiscInfo = colorsMap?.[0]?.miscInfo;

        return {
          uniqueId,
          name,
          colorFitsSizesMap: colorsMap,
          imagesByColor,
          listPrice,
          offerPrice,
          priceRange,
          isGiftCard,
          tcpStyleQty,
          tcpStyleType,
          reviewsCount,
          ratings,
          unbxdReviewsCount,
          unbxdRatings,
          badge1: selectedProductMiscInfo?.badge1?.defaultBadge,
          primaryBrand,
        };
      }
    }
  } catch (err) {
    return null;
  }
  return null;
};

export const getProductInfoFromCart = ({ pid, details }) => {
  try {
    const selectedProductFromCart = details.find((product) => {
      const modifiedProductKeys = getProductDetails(product);
      const { productInfo } = modifiedProductKeys;
      if (productInfo.productPartNumber === pid) return true;
      return false;
    });
    const {
      productInfo: { productPartNumber, tcpStyleQty, tcpStyleType },
      itemInfo: { listPrice, offerPrice, name, imagePath, color },
      miscInfo,
    } = getProductDetails(selectedProductFromCart);

    return {
      uniqueId: productPartNumber,
      name,
      listPrice,
      offerPrice,
      tcpStyleQty,
      tcpStyleType,
      colorFitsSizesMap: [
        {
          colorProductId: productPartNumber,
          imageName: productPartNumber,
          productImage: imagePath.split('/')[1],
          miscInfo,
          color: { name: color },
        },
      ],
      imagesByColor: {
        [color]: {
          basicImageUrl: imagePath,
          extraImages: [
            {
              isShortImage: false,
              isOnModalImage: false,
              iconSizeImageUrl: imagePath,
              listingSizeImageUrl: imagePath,
              regularSizeImageUrl: imagePath,
              bigSizeImageUrl: imagePath,
              superSizeImageUrl: imagePath,
            },
          ],
        },
        listPrice,
        offerPrice,
      },
    };
  } catch (error) {
    return null;
  }
};

export const getAlternateSizes = (state) => {
  return state.ProductDetail.currentProduct && state.ProductDetail.currentProduct.alternateSizes;
};

export const getDescription = (state) => {
  return state.ProductDetail.currentProduct && state.ProductDetail.currentProduct.longDescription;
};

export const getRatingsProductId = (state) => {
  return state.ProductDetail.currentProduct && state.ProductDetail.currentProduct.ratingsProductId;
};

export const getGeneralProductId = (state) => {
  return state.ProductDetail.currentProduct && state.ProductDetail.currentProduct.generalProductId;
};

export const getShortDescription = (state) => {
  return state.ProductDetail.currentProduct && state.ProductDetail.currentProduct.shortDescription;
};

export const getPrimaryBrand = (state) => {
  return state.ProductDetail.currentProduct && state.ProductDetail.currentProduct.primaryBrand;
};

export const getPDPLoadingState = (state) => {
  return state.ProductDetail && state.ProductDetail.isLoading;
};

export const getIsPDPCalledViaRecommendation = (state) => {
  return state.ProductDetail && state.ProductDetail.isPDPCalledFromRecommendationFlag;
};
export const getProductListingDetail = (state) => {
  return state && state.ProductListing && state.ProductListing.loadedProductsPages;
};

export const getSearchListingPage = (state) => {
  return state && state.SearchListingPage && state.SearchListingPage.loadedProductsPages;
};

const selectStateForm = (state) => (state && state.form ? state.form : {});
export const getProductDetailFormValues = createSelector(
  selectStateForm,
  getGeneralProductId,
  (stateForm, generalProductId) => {
    return getAddedToBagFormValues(stateForm, `ProductAddToBag-${generalProductId}`);
  }
);

const allLabels = (state) => state.Labels;

export const getPlpLabels = createSelector(allLabels, (labels) => {
  if (!labels || !labels.PLP)
    return {
      addToBag: '',
      update: '',
      errorMessage: '',
      size: '',
      fit: '',
      color: '',
      quantity: '',
      sizeUnavalaible: '',
      sizeAvailable: '',
      saveProduct: '',
      sizeChart: '',
      soldOutError: '',
      addToFavorites: '',
      plpRecosProductTilesTitle: '',
      plpPersonalized: '',
      lbl_plp_view_this_look: '',
      lbl_plp_also_available_in: '',
    };
  const {
    PLP: {
      plpTiles: {
        lbl_add_to_bag: addToBag,
        lbl_pdp_update: update,
        lbl_pdp_size_error: errorMessage,
        lbl_pdp_size: size,
        lbl_pdp_fit: fit,
        lbl_pdp_color: color,
        lbl_pdp_quantity: quantity,
        lbl_size_unavailable_online: sizeUnavalaible,
        lbl_other_sizes_available: sizeAvailable,
        lbl_fav_save_product: saveProduct,
        lbl_size_chart: sizeChart,
        lbl_soldout_error: soldOutError,
        lbl_add_to_favorites: addToFavorites,
        lbl_view_details: viewProductDetails,
        lbl_see_similar_items: seeSimilarItems,
        lbl_sold_out: soldOut,
        lbl_plp_recos_producttiles_title: plpRecosProductTilesTitle,
        lbl_plp_personalized: plpPersonalized,
        lbl_plp_also_available_in: alsoAvailableIn,
      },
    },
  } = labels;

  return {
    addToBag,
    errorMessage,
    size,
    fit,
    color,
    quantity,
    update,
    sizeUnavalaible,
    sizeAvailable,
    saveProduct,
    sizeChart,
    soldOutError,
    addToFavorites,
    viewProductDetails,
    seeSimilarItems,
    soldOut,
    plpRecosProductTilesTitle,
    plpPersonalized,
    alsoAvailableIn,
  };
});

// TODO - This is temporary - fix it by introducing the image carousel and zoom
export const getDefaultImage = (state) => {
  const firstColor =
    state.ProductDetail.currentProduct &&
    state.ProductDetail.currentProduct.colorFitsSizesMap &&
    state.ProductDetail.currentProduct.colorFitsSizesMap[0] &&
    state.ProductDetail.currentProduct.colorFitsSizesMap[0].color &&
    state.ProductDetail.currentProduct.colorFitsSizesMap[0].color.name;
  return (
    firstColor &&
    state.ProductDetail.currentProduct &&
    state.ProductDetail.currentProduct.imagesByColor[firstColor].basicImageUrl
  );
};

export const getCurrentProduct = (state) => {
  return state.ProductDetail.currentProduct;
};

export const getPDPLabels = createSelector(allLabels, (labels) => {
  return {
    pageNotFound: getLabelValue(labels, 'lbl_page_not_found', 'PDP_Sub', 'ProductDetailPage'),
    fullSize: getLabelValue(labels, 'lbl_full_size', 'PDP_Sub', 'ProductDetailPage'),
    viewThisLook: getLabelValue(labels, 'lbl_View_this_look', 'PDP_Sub', 'ProductDetailPage'),
    promoArea1: getLabelValue(labels, 'lbl_promo_area_1', 'PDP_Sub', 'ProductDetailPage'),
    promoArea: getLabelValue(labels, 'lbl_promo_area', 'PDP_Sub', 'ProductDetailPage'),
    promoArea3: getLabelValue(labels, 'lbl_promo_area_3', 'PDP_Sub', 'ProductDetailPage'),
    completeTheLook: getLabelValue(labels, 'lbl_complete_the_look', 'PDP_Sub', 'ProductDetailPage'),
    youMayAlsoLike: getLabelValue(labels, 'lbl_you_may_also_like', 'PDP_Sub', 'ProductDetailPage'),
    weFoundTheseSimilarItems: getLabelValue(
      labels,
      'lbl_we_found_these_similar_items',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    addToBagPlpDrawer: getLabelValue(
      labels,
      'lbl_add_to_bag_plp_drawer_redesign',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    checkoutSimilarStyle: getLabelValue(
      labels,
      'lbl_pdp_pla_abtest',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    recentlyViewed: getLabelValue(labels, 'lbl_recently_viewed', 'PDP_Sub', 'ProductDetailPage'),
    myStylePlace: getLabelValue(labels, 'lbl_my_style_place', 'PDP_Sub', 'ProductDetailPage'),
    ratingReview: getLabelValue(labels, 'lbl_rating_review', 'PDP_Sub', 'ProductDetailPage'),
    ratingReviewReDesign: getLabelValue(
      labels,
      'lbl_rating_review_redesign',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    ShowMore: getLabelValue(
      labels,
      'lbl_product_description_show_more',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    ShowLess: getLabelValue(
      labels,
      'lbl_product_description_show_less',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    ProductDescription: getLabelValue(
      labels,
      'lbl_product_description_label',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    ViewMore: getLabelValue(labels, 'lbl_view_more', 'PDP_Sub', 'ProductDetailPage'),
    ViewLess: getLabelValue(labels, 'lbl_view_less', 'PDP_Sub', 'ProductDetailPage'),
    KeyFeatures: getLabelValue(labels, 'lbl_key_features', 'PDP_Sub', 'ProductDetailPage'),
    ProductDescriptionNewDesign: getLabelValue(
      labels,
      'lbl_product_description_label_redesign',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    ClaimMessage: getLabelValue(
      labels,
      'lbl_product_description_claim_message',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    PartNumber: getLabelValue(
      labels,
      'lbl_product_description_item_part_number',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    preferSendingViaEmail: getLabelValue(
      labels,
      'lbl_prefer_sending_via_email',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    sendAnEmailCard: getLabelValue(
      labels,
      'lbl_send_an_email_card',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    freeShippingEveryDay: getLabelValue(
      labels,
      'lbl_free_shipping_every_day',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    back: getLabelValue(labels, 'lbl_back', 'PDP_Sub', 'ProductDetailPage'),
    eGiftCardLink: getLabelValue(labels, 'eGiftCardLink', 'PDP_Sub', 'ProductDetailPage'),
    backToResults: getLabelValue(labels, 'lbl_back_to_results', 'PDP_Sub', 'ProductDetailPage'),
    lblPdpSEODesc: getLabelValue(labels, 'lbl_pdp_SEO_desc', 'PDP_Sub', 'ProductDetailPage'),
    lblPdpPerUnit: getLabelValue(labels, 'lbl_pdp_per_unit', 'PDP_Sub', 'ProductDetailPage'),
    lblPdpProductInclude: getLabelValue(
      labels,
      'lbl_product_include_text',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    lblPdpMultipackPack: getLabelValue(
      labels,
      'lbl_multipack_pack_text',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    lblPdpMultipackTitle: getLabelValue(
      labels,
      'lbl_multipack_pack_title',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    lblPdpMultipackSingle: getLabelValue(
      labels,
      'lbl_multipack_single_text',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    lblUnavailableColour: getLabelValue(
      labels,
      'lbl_unavailable_color',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    pointsAndRewardsText: getLabelValue(
      labels,
      'lbl_brierley_promo_text_generic',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    headingStyleWith: getLabelValue(labels, 'lbl_heading_styleWith', 'addedToBagModal', 'global'),
    sizeSelectionLabel: getLabelValue(
      labels,
      'lbl_size_heading_redesign',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    fitSelectionLabel: getLabelValue(
      labels,
      'lbl_fit_heading_redesign',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    availableInOtherDepartments: getLabelValue(
      labels,
      'lbl_available_in_other_departments',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    selectSizeErrorText: getLabelValue(
      labels,
      'lbl_select_size_error_text',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    addToBag: getLabelValue(labels, 'lbl_add_to_bag', 'PDP_Sub', 'ProductDetailPage'),
    selectSizeLabel: getLabelValue(labels, 'lbl_select_size_label', 'PDP_Sub', 'ProductDetailPage'),
    sizeGuide: getLabelValue(labels, 'lbl_size_guide_redesign', 'PDP_Sub', 'ProductDetailPage'),
    reviews: getLabelValue(labels, 'lbl_reviews_redesign', 'PDP_Sub', 'ProductDetailPage'),
    selectValueLabel: getLabelValue(
      labels,
      'lbl_gift_card_select_value_redesign',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    sizeDrawerValue: getLabelValue(
      labels,
      'lbl_gift_card_value_redesign',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    save30: getLabelValue(labels, 'lbl_plcc_banner_save30', 'PDP_Sub', 'ProductDetailPage'),
    plccDiscription: getLabelValue(
      labels,
      'lbl_plcc_banner_discription',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    myLabel: getLabelValue(labels, 'lbl_plcc_banner_my', 'PDP_Sub', 'ProductDetailPage'),
    placeLabel: getLabelValue(labels, 'lbl_plcc_banner_place', 'PDP_Sub', 'ProductDetailPage'),
    rewardsLabel: getLabelValue(labels, 'lbl_plcc_banner_rewards', 'PDP_Sub', 'ProductDetailPage'),
    creditCardLabel: getLabelValue(
      labels,
      'lbl_plcc_banner_credit_card',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    learnMoreLabel: getLabelValue(
      labels,
      'lbl_plcc_banner_learn_more',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    plccMoreDiscription: getLabelValue(
      labels,
      'lbl_plcc_banner_discription_open',
      'PDP_Sub',
      'ProductDetailPage'
    ),
    tcpLabel: getLabelValue(labels, 'lbl_tcp', 'PDP_Sub', 'ProductDetailPage'),
    gymLabel: getLabelValue(labels, 'lbl_gym', 'PDP_Sub', 'ProductDetailPage'),
    elementsPlccCardImageUrl: getLabelValue(
      labels,
      'lbl_plcc_brand_image_path',
      'PDP_Sub',
      'ProductDetailPage'
    ),
  };
});

export const getAvailableSizeLabel = (state) =>
  getLabelValue(state.Labels, 'lbl_other_sizes_available', 'PDP_Sub', 'ProductDetailPage');

const getLayouts = (state) => {
  return state.Layouts;
};

const getModulesState = (state) => {
  return state.Modules;
};

const getIsTopPromoEnabled = (state) => {
  return getABtestFromState(state.AbTest).pdpTopPromo;
};

const getIsMiddlePromoEnabled = (state) => {
  return getABtestFromState(state.AbTest).pdpMiddlePromo;
};

const getIsBottomPromoEnabled = (state) => {
  return getABtestFromState(state.AbTest).pdpBottomPromo;
};

const getIsBelowrecsPromoEnabled = (state) => {
  return getABtestFromState(state.AbTest).pdpBelowrecsPromo;
};

export const getCategory = (state) =>
  state &&
  state.ProductDetail &&
  state.ProductDetail.currentProduct &&
  state.ProductDetail.currentProduct.category;

const getUserType = (isUserGuest, isUserPlcc) => {
  let userType = 'mpr';
  if (isUserGuest || isServer()) {
    userType = 'guest';
  } else if (isUserPlcc) {
    userType = 'plcc';
  }
  return userType;
};

const getPromo = (promos, userType) => {
  let isNewPromo = false;
  const filteredPromos = Array.isArray(promos)
    ? promos.filter((slot) => {
        const userTier = slot && slot.userType;
        if (userTier) isNewPromo = userTier;
        return userTier === userType;
      })
    : null;
  return isNewPromo ? filteredPromos : promos;
};

export const getPDPTopPromos = createSelector(
  getLayouts,
  getModulesState,
  getCategory,
  getIsGuest,
  isPlccUser,
  getIsTopPromoEnabled,
  (Layouts, Modules, primaryCategory, isUserGuest, isUserPlcc, isTopPromoEnabled) => {
    if (!isTopPromoEnabled) return null;
    const type = PRODUCTDETAIL_CONSTANTS.PROMO_TOP;
    let result = null;
    if (Layouts && Layouts.pdp && primaryCategory) {
      const { pdp } = Layouts;
      const promoCatId = pdp[primaryCategory] ? primaryCategory : 'global';
      if (pdp[promoCatId]) {
        const promos = pdp[promoCatId][type] && pdp[promoCatId][type].slots;
        const userType = getUserType(isUserGuest, isUserPlcc);
        const promo = getPromo(promos, userType);
        result =
          (promo &&
            promo.map((promoItem) => {
              return (promoItem.contentId && Modules[promoItem.contentId]) || {};
            })) ||
          [];
      }
    }
    return result;
  }
);

export const getPDPMiddlePromos = createSelector(
  getLayouts,
  getModulesState,
  getCategory,
  getIsGuest,
  isPlccUser,
  getIsMiddlePromoEnabled,
  (Layouts, Modules, primaryCategory, isUserGuest, isUserPlcc, isMiddlePromoEnabled) => {
    if (!isMiddlePromoEnabled) return null;
    const type = PRODUCTDETAIL_CONSTANTS.PROMO_MIDDLE;
    let result = null;
    if (Layouts && Layouts.pdp && primaryCategory) {
      const { pdp } = Layouts;
      const promoCatId = pdp[primaryCategory] ? primaryCategory : 'global';
      if (pdp[promoCatId]) {
        const promos = pdp[promoCatId][type] && pdp[promoCatId][type].slots;
        const userType = getUserType(isUserGuest, isUserPlcc);
        const promo = getPromo(promos, userType);
        result =
          (promo &&
            promo.map((promoItem) => {
              return (promoItem.contentId && Modules[promoItem.contentId]) || {};
            })) ||
          [];
      }
    }
    return result;
  }
);

export const getPDPBottomPromos = createSelector(
  getLayouts,
  getModulesState,
  getCategory,
  getIsGuest,
  isPlccUser,
  getIsBottomPromoEnabled,
  (Layouts, Modules, primaryCategory, isUserGuest, isUserPlcc, isBottomPromoEnabled) => {
    if (!isBottomPromoEnabled) return null;

    const type = PRODUCTDETAIL_CONSTANTS.PROMO_BOTTOM;
    let result = null;
    if (Layouts && Layouts.pdp && primaryCategory) {
      const { pdp } = Layouts;
      const promoCatId = pdp[primaryCategory] ? primaryCategory : 'global';
      if (pdp[promoCatId]) {
        const promos = pdp[promoCatId][type] && pdp[promoCatId][type].slots;
        const userType = getUserType(isUserGuest, isUserPlcc);
        const promo = getPromo(promos, userType);
        result =
          (promo &&
            promo.map((promoItem) => {
              return (promoItem.contentId && Modules[promoItem.contentId]) || {};
            })) ||
          [];
      }
    }
    return result;
  }
);

export const getPDPBelowRecsPromos = createSelector(
  getLayouts,
  getModulesState,
  getCategory,
  getIsGuest,
  isPlccUser,
  getIsBelowrecsPromoEnabled,
  (Layouts, Modules, primaryCategory, isUserGuest, isUserPlcc, isBelowrecsPromoEnabled) => {
    if (!isBelowrecsPromoEnabled) return null;

    const type = PRODUCTDETAIL_CONSTANTS.PROMO_BELOWRECS;
    let result = null;
    if (Layouts && Layouts.pdp && primaryCategory) {
      const { pdp } = Layouts;
      const promoCatId = pdp[primaryCategory] ? primaryCategory : 'global';
      if (pdp[promoCatId]) {
        const promos = pdp[promoCatId][type] && pdp[promoCatId][type].slots;
        const userType = getUserType(isUserGuest, isUserPlcc);
        const promo = getPromo(promos, userType);
        result =
          (promo &&
            promo.map((promoItem) => {
              return (promoItem.contentId && Modules[promoItem.contentId]) || {};
            })) ||
          [];
      }
    }
    return result;
  }
);

const getRefinedNavTree = (catId, navigationTree) => {
  return navigationTree.find((L1) => L1.categoryId === catId);
};

const getRefinedNavTreeL2orL3 = (catId, navigationTree) => {
  return (navigationTree && navigationTree.find((L2) => L2.categoryContent.id === catId)) || [];
};

const getCatMapL1Params = (catMapL1) => {
  const menuItems = [];
  const mainObj = catMapL1 && catMapL1.subCategories;
  let returnValue = [];
  if (mainObj) {
    Object.keys(mainObj).forEach((key) => {
      menuItems.push(mainObj[key].items);
    });
    try {
      returnValue = menuItems.flat(2);
    } catch (err) {
      returnValue = [];
    }
  }
  return returnValue;
};

const getCatMapL2 = (catMapL2Id, catMapL1Params) => {
  return (catMapL2Id && getRefinedNavTreeL2orL3(catMapL2Id, catMapL1Params)) || [];
};

const getNavTreeFromCatMap = (navTree, categoryPath) => {
  if (!categoryPath) return '';
  const catMapL1Id = categoryPath[0] && categoryPath[0].split('|')[0].split('>')[0];
  const catMapL2Id = categoryPath[0] && categoryPath[0].split('|')[0].split('>')[1];
  const catMapL3Id = (categoryPath[0] && categoryPath[0].split('|')[0].split('>')[2]) || null;
  const catMapL1 = catMapL1Id && getRefinedNavTree(catMapL1Id, navTree);
  const catMapL1Params = getCatMapL1Params(catMapL1);
  const catMapL2 = getCatMapL2(catMapL2Id, catMapL1Params);
  return (
    (catMapL3Id && getRefinedNavTreeL2orL3(catMapL3Id, catMapL2 && catMapL2.subCategories)) ||
    catMapL2
  );
};

const getCatMapFromBreadCrump = (categoryPath, breadCrumbs) => {
  return (
    categoryPath &&
    breadCrumbs[1] &&
    categoryPath.find((_catMap) => _catMap.split('|')[0].includes(breadCrumbs[1].categoryId))
  );
};

const getSizeChartSel = (l3CatFromString) => {
  return (
    l3CatFromString &&
    l3CatFromString.categoryContent &&
    l3CatFromString.categoryContent.sizeChartSelection
  );
};

const getNavTreeFromBreadCrumb = (breadCrumbs, categoryPath, navTree) => {
  const catMap = getCatMapFromBreadCrump(categoryPath, breadCrumbs);
  const l3String = catMap ? catMap.split('|')[0].split('>')[2] || '' : '';
  const l3CatFromString =
    navTree &&
    navTree.subCategories &&
    navTree.subCategories.map((navMap) => {
      return navMap.items.find((cat) => cat.id === l3String);
    });

  return (
    getSizeChartSel(l3CatFromString) ||
    (navTree && navTree.categoryContent && navTree.categoryContent.sizeChartSelection) ||
    ''
  );
};

const fetchL2andL3Category = (navTree, breadCrumbs, isBundleProduct, categoryPath) => {
  let l3Cat = {};
  let l2Cat = {};
  if (breadCrumbs && breadCrumbs[0] && breadCrumbs[0].categoryId && !isBundleProduct) {
    const tree = getRefinedNavTree(breadCrumbs[0].categoryId, navTree);
    l2Cat =
      tree &&
      tree.subCategories &&
      tree.subCategories.map((navMap) => {
        return navMap.items.find((cat) => cat.id === breadCrumbs[1].categoryId);
      });

    l3Cat =
      breadCrumbs[2] &&
      l2Cat &&
      l2Cat.subCategories &&
      l2Cat.subCategories.find((cat) => cat.categoryId === breadCrumbs[2].categoryId);
  } else {
    l3Cat = getNavTreeFromCatMap(navTree, categoryPath);
  }
  return { l2Cat, l3Cat };
};

// Disabling eslint for temporary fix
// eslint-disable-next-line no-unused-vars
const fetchSizeChartDetails = (navTree, breadCrumbs, categoryPath, isBundleProduct) => {
  // Return empty if Navigation Tree not available/passed
  if (!navTree) {
    return '';
  }
  const payload = fetchL2andL3Category(navTree, breadCrumbs, isBundleProduct, categoryPath);
  if (payload.l3Cat) {
    return (
      (payload.l3Cat.categoryContent && payload.l3Cat.categoryContent.sizeChartSelection) ||
      (payload.l2Cat &&
        payload.l2Cat.categoryContent &&
        payload.l2Cat.categoryContent.sizeChartSelection) ||
      ''
    );
  }
  return getNavTreeFromBreadCrumb(breadCrumbs, categoryPath, payload.l2Cat);
};

// Disabling eslint for temporary fix
// eslint-disable-next-line no-unused-vars
export const getSizeChartDetails = (state) => {
  const breadCrumbs = processBreadCrumbs(state.ProductDetail && state.ProductDetail.breadCrumbs);
  const navigationTree = state.Navigation && state.Navigation.navigationData;
  const isBundleProduct =
    state.ProductDetail &&
    state.ProductDetail.currentProduct &&
    state.ProductDetail.currentProduct.bundleProducts &&
    state.ProductDetail.currentProduct.bundleProducts.length > 0;
  const categoryPathMap =
    state.ProductDetail &&
    state.ProductDetail.currentProduct &&
    state.ProductDetail.currentProduct.categoryPathMap;
  return fetchSizeChartDetails(navigationTree, breadCrumbs, categoryPathMap, isBundleProduct);
};

const getStateLabels = (state) => state.Labels;

export const getAccessibilityLabels = createSelector(getStateLabels, (stateLabels) => {
  return {
    lbl_social_twitter: getLabelValue(stateLabels, 'lbl_social_twitter', 'accessibility', 'global'),
    lbl_social_facebook: getLabelValue(
      stateLabels,
      'lbl_social_facebook',
      'accessibility',
      'global'
    ),
    lbl_social_pinterest: getLabelValue(
      stateLabels,
      'lbl_social_pinterest',
      'accessibility',
      'global'
    ),
    lbl_up_arrow_icon: getLabelValue(stateLabels, 'lbl_up_arrow_icon', 'accessibility', 'global'),
    lbl_down_arrow_icon: getLabelValue(
      stateLabels,
      'lbl_down_arrow_icon',
      'accessibility',
      'global'
    ),
    lbl_left_arrow_icon: getLabelValue(
      stateLabels,
      'lbl_left_arrow_icon',
      'accessibility',
      'global'
    ),
    lbl_show_page: getLabelValue(stateLabels, 'lbl_show_page', 'accessibility', 'global'),
    lbl_product_image: getLabelValue(stateLabels, 'lbl_product_image', 'accessibility', 'global'),
    lbl_product_hint: getLabelValue(stateLabels, 'lbl_product_hint', 'accessibility', 'global'),
    previousPrice: getLabelValue(stateLabels, 'lbl_previous_price', 'accessibility', 'global'),
    rating: getLabelValue(stateLabels, 'lbl_rating', 'accessibility', 'global'),
    outOf: getLabelValue(stateLabels, 'lbl_out_of', 'accessibility', 'global'),
  };
});

export const getOnModelImageAbTestPdp = (state) => {
  return getABtestFromState(state.AbTest).enableOnModelImageOnPDP;
};

export const isValidProduct = (state) => {
  const currentProduct = getCurrentProduct(state);
  return currentProduct && !!currentProduct.unbxdProdId;
};

export const getPageUrl = (state) => {
  return state.ProductDetail.pageUrl;
};

export const getUpdatedCount = (state) => {
  return state && state.ProductDetail && state.ProductDetail.countUpdated;
};

export const getStyliticsProductTabListSelector = (state) => {
  const { StyliticsProductTabList } = state;
  return StyliticsProductTabList;
};

export const getPriceDisplayState = (state) => {
  return state && state.ProductDetail && state.ProductDetail.hidePriceDisplay;
};

export const getIsAbPdpPlaEnabled = (state) => {
  return getABtestFromState(state.AbTest).isAbPdpPlaEnabled;
};

export const getIsOosPdpEnabled = (state) => {
  return getABtestFromState(state.AbTest).isOosPdpEnabled;
};

export const getIsPdp1andPdp2Enabled = (state) => {
  return getABtestFromState(state.AbTest).isPdp1andPdp2Enabled;
};

export const getPdpNewRecommendation = (state) => {
  return getABtestFromState(state.AbTest).PDP_NEW_RECOMMENDATION;
};

export const getSelectedSizeDisabled = (state) => {
  return state && state.ProductDetail && state.ProductDetail.selectedSizeStatus;
};
