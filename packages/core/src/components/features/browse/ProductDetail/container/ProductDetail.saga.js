// 9fbef606107a605d69c0edbcd8029e5d
import { loadLayoutData, loadModulesData } from '@tcp/core/src/reduxStore/actions';
import { call, put, takeLatest, select, race, delay } from 'redux-saga/effects';
import { isMobileApp, isClient } from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getUnbxdXappConfigs } from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import { getAlternateBrandName } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import { getIsPDPCalledViaRecommendation } from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.selectors';
import {
  getStringAfterSplit,
  getMultipackCount,
  filterMultiPack,
  isServer,
  getAPIConfig,
} from '../../../../../utils/utils';
import PRODUCTLISTING_CONSTANTS from './ProductDetail.constants';
import {
  getIsPdpServiceEnabled,
  getABTestPdpService,
  getIsNewReDesignEnabled,
} from '../../../../../reduxStore/selectors/session.selectors';

import {
  setProductDetails,
  setPDPLoadingState,
  setProductDetailsDynamicData,
  setPageUrl,
  setPDPDisplayPriceState,
  setMultiPackDetails,
  singlePageLoadAction,
  setDisableSelectedTab,
} from './ProductDetail.actions';
import getProductInfoById, {
  layoutResolver,
  getProductBVReviewStats,
} from '../../../../../services/abstractors/productListing/productDetail';
import { getDefaultWishList } from '../../Favorites/container/Favorites.saga';

const checkSSR = () => typeof window !== 'undefined' || isMobileApp();
const windowCheck = () => typeof window !== 'undefined' && !isMobileApp();

const shouldSupressPriceDisplay = (isBotDevice) => {
  return !isMobileApp() && !isBotDevice && !isClient();
};

// eslint-disable-next-line max-params
function* backgroundTask(
  productColorId,
  state,
  isBotDevice,
  originalURL,
  xappURLConfig,
  isPdpServiceEnabled,
  enablePDPServiceABTest,
  config,
  otherBrand
) {
  try {
    const alternateBrand = yield select(getAlternateBrandName);
    const isPDPCalledViaRecommendationModule = yield select(getIsPDPCalledViaRecommendation);
    return yield call(
      getProductInfoById,
      xappURLConfig,
      productColorId,
      state,
      undefined,
      undefined,
      isBotDevice,
      originalURL,
      undefined,
      undefined,
      undefined,
      undefined,
      isPdpServiceEnabled,
      enablePDPServiceABTest,
      config,
      !isPDPCalledViaRecommendationModule && (otherBrand || alternateBrand)
    );
  } catch (e) {
    throw new Error();
  }
}

// eslint-disable-next-line
function* fetchProductDetail({ payload }) {
  const {
    productColorId,
    isMultiPack,
    generalProductId,
    escapeEmptyProduct,
    resolve,
    isBotDevice,
    url,
    originalURL = '',
    getDetailonClick,
    apiConfig: config,
    otherBrand,
  } = payload;
  const xappURLConfig = yield call(getUnbxdXappConfigs);
  const alternateBrand = yield select(getAlternateBrandName);
  const isPDPCalledViaRecommendationModule = yield select(getIsPDPCalledViaRecommendation);
  try {
    const pageName = 'pdp';
    let oldProductDetail;
    let newProductDetail;
    let multipackCall;
    let newPartId;
    let finalPartId;
    let partId;
    yield put(loadLayoutData({}, pageName));
    if (!escapeEmptyProduct && !getDetailonClick) {
      yield put(setProductDetails({ product: {} }));
    }
    const state = yield select();
    yield call(getDefaultWishList);
    yield put(setPDPLoadingState({ isLoading: true }));

    const hidePriceDisplay = shouldSupressPriceDisplay(isBotDevice);
    yield put(setPDPDisplayPriceState({ hidePriceDisplay }));

    const isPdpServiceEnabled = yield select(getIsPdpServiceEnabled);
    const enablePDPServiceABTest = yield select(getABTestPdpService);
    if (!hidePriceDisplay && !isMultiPack) {
      const apiOutput = yield race({
        productDetailInfo: backgroundTask(
          productColorId,
          state,
          isBotDevice,
          originalURL,
          xappURLConfig,
          isPdpServiceEnabled,
          enablePDPServiceABTest,
          config,
          !isPDPCalledViaRecommendationModule && (otherBrand || alternateBrand)
        ),
        timeout: delay(5000),
      });
      if (apiOutput && apiOutput.productDetailInfo) {
        oldProductDetail = apiOutput.productDetailInfo;
      } else {
        yield put(setPDPDisplayPriceState({ hidePriceDisplay: false }));
      }
    } else if (isMultiPack) {
      multipackCall = yield call(
        getProductInfoById,
        xappURLConfig,
        productColorId,
        state,
        undefined,
        undefined,
        isBotDevice,
        originalURL,
        false,
        undefined,
        generalProductId,
        true,
        isPdpServiceEnabled,
        enablePDPServiceABTest,
        config,
        !isPDPCalledViaRecommendationModule && (otherBrand || alternateBrand)
      );
      oldProductDetail = multipackCall;
    } else {
      oldProductDetail = yield call(
        getProductInfoById,
        xappURLConfig,
        productColorId,
        state,
        undefined,
        undefined,
        isBotDevice,
        originalURL,
        false,
        undefined,
        undefined,
        false,
        isPdpServiceEnabled,
        enablePDPServiceABTest,
        config,
        !isPDPCalledViaRecommendationModule && (otherBrand || alternateBrand)
      );
    }
    if (
      !!oldProductDetail &&
      !!oldProductDetail.product &&
      oldProductDetail.product.multiPackUSStore
    ) {
      let multiplePartId = '';
      partId = oldProductDetail.product.tcpMultiPackReferenceUSStore.join(',').split(',');
      const updatedPartId = new Set(partId);
      const findUpdatedPartId = [...updatedPartId];
      for (let id = 0; id < findUpdatedPartId.length; id += 1) {
        if (findUpdatedPartId[id].includes(',')) {
          const arrayOfPartNum = getStringAfterSplit(findUpdatedPartId[id], ',');
          for (let num = 0; num < arrayOfPartNum.length; num += 1) {
            multiplePartId = multiplePartId.concat(
              getStringAfterSplit(arrayOfPartNum[num], '#')[0]
            );
            if (num !== arrayOfPartNum.length - 1) {
              multiplePartId = multiplePartId.concat(',');
            }
          }
        } else {
          newPartId = findUpdatedPartId[id]
            ? [getStringAfterSplit(findUpdatedPartId[id], '#')[0]]
            : '';
          multiplePartId += newPartId ? `${newPartId},` : '';
        }
      }

      finalPartId = multiplePartId;
      newProductDetail = yield call(
        getProductInfoById,
        xappURLConfig,
        finalPartId,
        state,
        undefined,
        undefined,
        isBotDevice,
        originalURL,
        true,
        partId,
        undefined,
        undefined,
        isPdpServiceEnabled,
        enablePDPServiceABTest,
        config,
        !isPDPCalledViaRecommendationModule && (otherBrand || alternateBrand)
      );
    }
    const productDetail = oldProductDetail;
    const newMultiPackCount =
      productDetail &&
      productDetail.product &&
      productDetail.product.multiPackUSStore &&
      productDetail.product.tcpMultiPackReferenceUSStore &&
      productDetail.product.tcpMultiPackReferenceUSStore[0]?.split(',');

    const multiPackCount = newMultiPackCount ? getMultipackCount(newMultiPackCount) : 0;
    const {
      product: { category },
    } = productDetail;

    productDetail.product.isSSRData = !isClient();
    if (!getDetailonClick) {
      yield put(
        singlePageLoadAction({
          singlepageload: true,
          availableTCPMultipackProductMapping:
            productDetail.product && productDetail.product.TCPMultipackProductMapping,
          availableTCPmapNewStyleId:
            productDetail.product && productDetail.product.TCPmapNewStyleId,
          initialSelectedQty: Number(
            productDetail.product && productDetail.product.TCPStyleQTY
          ).toString(),
        })
      );
    }
    const CheckifProductAvailable =
      productDetail &&
      productDetail.product &&
      productDetail.product.colorFitsSizesMap.length &&
      productDetail.product.unbxdProdId;
    if (CheckifProductAvailable) {
      yield put(setProductDetails({ ...productDetail }));
    }
    if (newProductDetail) {
      const getFilterData = newProductDetail.product.categoryIdList;
      const getmultipackProduct = getFilterData && filterMultiPack(getFilterData);
      yield put(
        setMultiPackDetails({
          newPartNumber: getmultipackProduct,
          multiPackCount,
          getMultiPackAllColor: productDetail.product.getMultiPackAllColor,
          getNewMultiPackAllColor: newProductDetail.product.getMultiPackAllColor,
          partIdDetail: newProductDetail.product.partIdDetails,
        })
      );
    } else {
      yield put(
        setMultiPackDetails({
          newPartNumber: null,
          multiPackCount: null,
          getMultiPackAllColor: null,
        })
      );
    }
    yield put(setPDPLoadingState({ isLoading: false }));

    if (category) {
      let apiConfig = config;
      if (
        !isServer() &&
        (typeof apiConfig === 'undefined' || Object.keys(apiConfig).indexOf('brandIdCMS') < 0)
      ) {
        apiConfig = getAPIConfig();
      }
      const { layout, modules } = yield call(layoutResolver, { category, pageName, apiConfig });
      if (checkSSR()) {
        yield put(loadLayoutData(layout, pageName));
        yield put(loadModulesData(modules));
      }
    }
    // fetch review/rating summary from Bazar Voice only for App
    const isNewReDesignEnabled = yield select(getIsNewReDesignEnabled);
    if ((isClient() || isMobileApp()) && (!isNewReDesignEnabled || payload.isSBP)) {
      const productId = productDetail.product.ratingsProductId || 0;
      productDetail.product.bazaarVoice = yield call(getProductBVReviewStats, productId);
    }
    if (!CheckifProductAvailable) {
      yield put(setDisableSelectedTab({ disabledTab: true }));
    } else {
      yield put(setProductDetails({ ...productDetail }));
    }
    if (windowCheck() && CheckifProductAvailable) {
      yield put(setProductDetailsDynamicData({ ...productDetail }));
    }
    if (url) {
      yield put(setPageUrl(url));
    }
    if (resolve) {
      resolve();
    }
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'ProductDetail Saga - fetchProductDetail',
        payload: {
          colorId: productColorId,
          botDevice: isBotDevice,
        },
      },
    });
    yield put(setPDPLoadingState({ isLoading: false }));

    yield put(setPDPDisplayPriceState({ hidePriceDisplay: false }));
    if (resolve) {
      resolve();
    }
  }
}

function* ProductDetailSaga() {
  yield takeLatest(PRODUCTLISTING_CONSTANTS.FETCH_PRODUCT_DETAILS, fetchProductDetail);
}

export default ProductDetailSaga;
