// 9fbef606107a605d69c0edbcd8029e5d
import PRODUCTDETAIL_CONSTANTS from './ProductDetail.constants';
import { DEFAULT_REDUCER_KEY } from '../../../../../utils/cache.util';

const initialState = {
  [DEFAULT_REDUCER_KEY]: null,
};

function productDetailReducer(state, action, payload, type) {
  switch (type) {
    case PRODUCTDETAIL_CONSTANTS.SET_MULTIPRODUCT_DETAILS: {
      return { ...state, ...payload };
    }

    case PRODUCTDETAIL_CONSTANTS.SINGLE_TAB_STORE: {
      return { ...state, ...payload };
    }

    case PRODUCTDETAIL_CONSTANTS.MULTIPACK_SELECTION:
      return { ...state, selectedMultipack: action.payload };

    case PRODUCTDETAIL_CONSTANTS.SET_DISABLE_SELECTED_TAB:
      return { ...state, ...payload };

    default:
      return { ...state };
  }
}

const getPageUrl = (payload) => {
  return typeof payload === 'object' ? '' : payload;
};

const ProductDetailReducer = (state = initialState, action) => {
  const { payload = {}, type } = action;
  switch (type) {
    case PRODUCTDETAIL_CONSTANTS.SET_PRODUCT_DETAILS: {
      return { ...state, currentProduct: { ...payload.product }, breadCrumbs: payload.breadCrumbs };
    }
    case PRODUCTDETAIL_CONSTANTS.SET_PRODUCT_DETAILS_DYNAMIC_DATA:
      return { ...state, currentProductDynamicData: { ...payload.product } };
    case PRODUCTDETAIL_CONSTANTS.SET_PDP_LOADING_STATE:
      return { ...state, ...payload };
    case PRODUCTDETAIL_CONSTANTS.SET_PDP_CALLED_VIA_RECOMMENDATION:
      return { ...state, ...payload };
    case PRODUCTDETAIL_CONSTANTS.LOAD_SOCIAL_PROOF_DATA:
      return { ...state, ...payload };
    case PRODUCTDETAIL_CONSTANTS.CLEAR_SOCIAL_PROOF_DATA:
      return { ...state, socialProofData: null };
    case PRODUCTDETAIL_CONSTANTS.RESET_COUNT_UPDATED:
      return { ...state, countUpdated: false };
    case PRODUCTDETAIL_CONSTANTS.SET_PAGE_URL:
      return { ...state, pageUrl: getPageUrl(payload) };
    case PRODUCTDETAIL_CONSTANTS.SET_ADD_TO_FAVORITE: {
      const productDetailsMap = state.currentProduct;
      const currentProductDynamicDataMap = state.currentProductDynamicData;
      const productMap = currentProductDynamicDataMap
        ? [productDetailsMap, currentProductDynamicDataMap]
        : [productDetailsMap];
      productMap.map((productInfo) => {
        // eslint-disable-next-line no-param-reassign
        productInfo.colorFitsSizesMap = productInfo.colorFitsSizesMap.map((item) => {
          if (item.colorProductId === action.payload.pdpColorProductId) {
            // eslint-disable-next-line no-param-reassign
            item = {
              ...item,
              isFavorite: true,
              favoritedCount: action.payload.res && action.payload.res.favoritedCount,
            };
          }
          return item;
        });
        return productInfo;
      });
      return { ...state, countUpdated: true, currentProduct: { ...productDetailsMap } };
    }
    case PRODUCTDETAIL_CONSTANTS.SET_PDP_DISPLAY_PRICE:
      return { ...state, ...payload };
    case PRODUCTDETAIL_CONSTANTS.SELECTED_SIZE_STATUS:
      return { ...state, selectedSizeStatus: payload };
    default:
      return productDetailReducer(state, action, payload, type);
  }
};

export default ProductDetailReducer;
