/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import { change } from 'redux-form';
import { PropTypes } from 'prop-types';
import isEqual from 'lodash/isEqual';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import {
  getModalState,
  getUniqueId,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import {
  getIsKeepAliveProductApp,
  getIsShowPriceRangeForApp,
  getABTestIsShowPriceRange,
  getMultiPackThreshold,
  getIsPerUnitPriceEnabled,
  getIsBrierleyPromoEnabled,
  getMultiColorNameEnabled,
  getIsDynamicBadgeEnabled,
  getCartsSessionStatus,
  getIsNewReDesignEnabled,
  getIsBossEnabled,
  getIsBopisEnabled,
  getIsATBModalBackAbTestNewDesign,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getLedgerSummaryData } from '@tcp/core/src/components/features/CnC/common/organism/OrderLedger/container/orderLedger.selector';
import {
  selectWishlistsSummaries,
  selectActiveWishlistProducts,
  selectActiveWishList,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.selectors';
import * as PickupSelectors from '@tcp/core/src/components/common/organisms/ProductPickup/container/ProductPickup.selectors';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  getProducts,
  getCompleteRecommendations,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import { getStyliticsProductTabListSelector } from '@tcp/core/src/components/common/molecules/ModuleQ/container/ModuleQ.selector';
import { trackPageView, trackClick, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import { parseBoolean, getRecommendation, isMobileApp } from '@tcp/core/src/utils';
import { playHapticFeedback } from '@tcp/core/src/components/common/atoms/hapticFeedback/container/HapticFeedback.actions.native';
import { getAlternateBrandName } from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.selectors';
import { getOutfitProducts } from '../../OutfitDetails/container/OutfitDetails.selectors';
import { getLabelsOutOfStock } from '../../ProductListing/container/ProductListing.selectors';

import ProductDetail from '../views';
import {
  getProductDetails,
  setProductDetails,
  setDisableSelectedTab,
  setIsPDPCalledFromRecommendation,
  clearSocialProofData,
  updateSelectedSizeStatus,
} from './ProductDetail.actions';

import {
  getNavTree,
  getBreadCrumbs,
  getCurrentProduct,
  getPlpLabels,
  getProductDetailFormValues,
  getPDPLabels,
  getShortDescription,
  getGeneralProductId,
  getDescription,
  getAlternateSizes,
  getPDPMiddlePromos,
  getPDPBottomPromos,
  getPDPBelowRecsPromos,
  getPDPLoadingState,
  getProductInfoFromProductListingData,
  getSizeChartDetails,
  getAccessibilityLabels,
  getOnModelImageAbTestPdp,
  getMultiPackProductInfo,
  getMultiPackCount,
  getAllMultiPackData,
  partIdInformation,
  getAllNewMultiPackData,
  getProductListingDetail,
  getSearchListingPage,
  getSelectedMultipack,
  prodDetails,
  getPdpNewRecommendation,
  getIsOosPdpEnabled,
  getIsPdp1andPdp2Enabled,
  getSinglePageLoad,
  getInitialMultipackMapping,
  getInitialTCPStyleQTY,
  getDisableMultiPackTab,
  getInitialAvailableTCPmapNewStyleId,
  getProductInfoFromOutfit,
  getProductInfoFromCart,
  getSocialProofMessage,
  getSelectedSizeDisabled,
} from './ProductDetail.selectors';
import {
  getIsPickupModalOpen,
  getIsPickupModalOpenFromBagPage,
} from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import {
  addToCartEcom,
  addToCartEcomNewDesign,
  clearAddToBagErrorState,
  AddToCartError,
} from '../../../CnC/AddedToBag/container/AddedToBag.actions';
import {
  getAddedToBagError,
  addedToBagMsgSelector,
} from '../../../CnC/AddedToBag/container/AddedToBag.selectors';
import { getCartItemInfo } from '../../../CnC/AddedToBag/util/utility';
import {
  getUserLoggedInState,
  isRememberedUser,
  isPlccUser,
  getUserNearStore,
} from '../../../account/User/container/User.selectors';
import {
  addItemsToWishlist,
  removeAddToFavoriteErrorState,
  setLastDeletedItemIdAction,
} from '../../Favorites/container/Favorites.actions';

import {
  fetchAddToFavoriteErrorMsg,
  wishListFromState,
  getDefaultFavItemInProgressFlag,
} from '../../Favorites/container/Favorites.selectors';
import ProductDetailSkeleton from '../molecules/ProductDetailSkeleton';
import { getSBPLabels } from '../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.selector';
import { openPickupModalWithValues } from '../../../../common/organisms/PickupStoreModal/container/PickUpStoreModal.actions';
import {
  validateBopisEligibility,
  validateBossEligibility,
} from '../../../../common/organisms/ProductPickup/util';
import {
  getIsDefaultProductBadge,
  getMapSliceForColorProductId,
} from '../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import { getQuickViewLoader } from '../../../../common/molecules/Loader/container/Loader.selector';
import BagPageSelectors from '../../../CnC/BagPage/container/BagPage.selectors';
import { getCurrentBundle } from '../../BundleProduct/container/BundleProduct.selectors';
import {
  resetSelectedChangedStoreActiom,
  setBopisInventoryDetailsAction,
} from '../../../../common/organisms/ProductPickup/container/ProductPickup.actions';

class ProductDetailContainer extends React.Component {
  // this variable will contain the id of the wish list of the current profile
  // eslint-disable-next-line react/sort-comp
  static activeWishListId = '';

  static extractPID = (navigation) => {
    const pid = (navigation && navigation.getParam('pdpUrl')) || '';
    // TODO - fix this to extract the product ID from the page.
    const id = pid && pid.split('-');
    let productId = id && id.length > 1 ? `${id[id.length - 2]}_${id[id.length - 1]}` : pid;
    if (
      (id.indexOf('Gift') > -1 || id.indexOf('gift') > -1) &&
      (id.indexOf('Card') > -1 || id.indexOf('card') > -1)
    ) {
      productId = 'gift';
    }
    return productId;
  };

  selectedColorProductId;

  didBlurSubscription;

  constructor(props) {
    super(props);
    this.state = {
      changedCurrentColorEntry: null,
    };
    const { navigation } = props;
    // eslint-disable-next-line react/prop-types
    this.selectedColorProductId = navigation && navigation.getParam('selectedColorProductId');
    this.clickOrigin = navigation && navigation.getParam('clickOrigin');
    this.didBlurSubscription = navigation && navigation.addListener('didBlur', () => {});

    const isSBP = navigation && navigation.getParam('isSBP');
    if (isSBP)
      BackHandler.addEventListener('hardwareBackPress', () => {
        navigation.goBack(null);
        return true;
      });
  }

  static getDerivedStateFromProps = (props, state) => {
    const { navigation } = props;
    const pid = ProductDetailContainer.extractPID(navigation);
    const { renderedPid } = state;
    if (pid !== renderedPid) {
      return {
        renderedPid: pid,
      };
    }
    return null;
  };

  state = {
    renderedPid: '',
  };

  componentDidMount() {
    const { getDetails, navigation, resetChangeSeletedStore } = this.props;
    const productId = ProductDetailContainer.extractPID(navigation);
    const isSBP = navigation && navigation.getParam('isSBP');
    getDetails({ productColorId: productId, ignoreCache: true, isSBP });

    let wishId = '';
    const { wishlistsSummaries } = this.props;
    /**
     * There is a bug in the application. The Selector file is always returning the wish list products
     * of the last profile created, instead of the current profile. So in this block of code, I am first getting
     *  the active wish list ID from wishlistsSummaries and then fetching the wishlist products against that ID.
     * This will provide us the products of the current profile.
     * TODO: This code need a clean up as the fix should ideally go in selector file.
     */
    if (isSBP && navigation.state.params.profile) {
      try {
        const activeWishListProfile = wishlistsSummaries.find(
          (itm) => itm.displayName === navigation.state.params.profile.name
        );
        if (activeWishListProfile) {
          wishId = activeWishListProfile.id;
          this.activeWishListId = wishId;
        }
      } catch (err) {
        logger.error(err);
      }
    }
    if (isMobileApp()) {
      resetChangeSeletedStore();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate() {
    const {
      navigation,
      currentProduct: { generalProductId } = {},
      getDetails,
      isNewReDesignEnabled,
      resetChangeSeletedStore,
    } = this.props;
    const productId = ProductDetailContainer.extractPID(navigation);
    if (generalProductId && productId && productId !== generalProductId) {
      this.selectedColorProductId = navigation && navigation.getParam('selectedColorProductId');
      const isSBP = navigation && navigation.getParam('isSBP');
      getDetails({ productColorId: productId, ignoreCache: true, isSBP });
      if (
        isNewReDesignEnabled &&
        navigation?.getParam('selectedColor') &&
        (navigation?.getParam('viaModule') === 'mbox_Recommendation' ||
          navigation?.getParam('fromCartPage'))
      ) {
        resetChangeSeletedStore();
        this.onUpdateColor(navigation?.getParam('selectedColor'), this.selectedColorProductId);
      }
      if (isNewReDesignEnabled) {
        setTimeout(() => {
          if (navigation?.state?.params?.departmentChange) {
            this.onUpdateSize('', productId);
          } else {
            this.onUpdateSize('', this.selectedColorProductId);
          }
        }, 1000);
      }
    }
  }

  componentWillUnmount() {
    const {
      resetProductDetails,
      resetBopisInventoryData,
      resetChangeSeletedStore,
      resetSocialProofData,
    } = this.props;
    this.resetFormValues();
    resetSocialProofData();
    if (this.didBlurSubscription) {
      resetBopisInventoryData();
      resetProductDetails();
      resetChangeSeletedStore();
      this.didBlurSubscription.remove();
    }
  }

  resetFormValues = (color, productId) => {
    const { updateColor, updateSize } = this.props;
    const form = this.getFormNameProductAddToBag(productId);
    if (form && form !== '') {
      updateColor({ form, color: {} });
      updateSize({ form, size: '' });
    }
  };

  getFormValuesFromKey = (formValues, key) => formValues && formValues[key];

  handleAddToBag = (formValuesSbp, currentProductSBP) => {
    const {
      addToBagEcom,
      formValues,
      currentProduct,
      multipackProduct,
      multiPackCount,
      getAllMultiPack,
      partIdInfo,
      getAllNewMultiPack,
      navigation,
    } = this.props;
    let cartItemInfo;
    const isSBP = navigation && navigation.getParam('isSBP');
    if (
      isSBP &&
      (formValues === undefined || formValues.color === undefined || formValues.size === undefined)
    ) {
      const quantity =
        formValues && formValues.quantity ? formValues.quantity : formValuesSbp.quantity;
      const formValuesSBPNew = {
        color: this.getFormValuesFromKey(formValuesSbp, 'color'),
        size: this.getFormValuesFromKey(formValuesSbp, 'size'),
        quantity,
        fit: this.getFormValuesFromKey(formValuesSbp, 'fit'),
      };
      cartItemInfo = getCartItemInfo(currentProductSBP, formValuesSBPNew);
    } else {
      cartItemInfo = getCartItemInfo(currentProduct, formValues);
    }
    cartItemInfo = {
      ...cartItemInfo,
      isPdpPage: true,
      multipackProduct,
      multiPackCount,
      getAllMultiPack,
      partIdInfo,
      getAllNewMultiPack,
      originPage: this.clickOrigin === 'search' ? 'spdp' : 'pdp',
    };
    cartItemInfo.addedFromSBP = isSBP;
    addToBagEcom(cartItemInfo);
  };

  handleAddToBagNew = (formValues, deliveryMethodType = 'shipIt') => {
    const {
      addToBagEcom,
      addToBagEcomNew,
      currentProduct,
      multipackProduct,
      multiPackCount,
      getAllMultiPack,
      partIdInfo,
      getAllNewMultiPack,
      isNewReDesignEnabled,
      isATBModalBackAbTestNewDesign,
      alternateBrand,
      userDefaultStore,
      isPDPBossBopisEnabled,
    } = this.props;
    const productFormData = {
      ...formValues,
      wishlistItemId: this.activeWishListId,
      isBoss: deliveryMethodType === 'noRushPickup',
      storeLocId: userDefaultStore?.basicInfo?.id || '',
    };
    let cartItemInfo = getCartItemInfo(
      currentProduct,
      isPDPBossBopisEnabled ? productFormData : formValues
    );
    cartItemInfo = {
      ...cartItemInfo,
      isPdpPage: true,
      multipackProduct,
      multiPackCount,
      getAllMultiPack,
      partIdInfo,
      getAllNewMultiPack,
      originPage: this.clickOrigin === 'search' ? 'spdp' : 'pdp',
    };
    if (isNewReDesignEnabled && isATBModalBackAbTestNewDesign) {
      const cartItemInfo1 = {
        ...cartItemInfo,
        isOpenNewATBModal: true,
        brand: (currentProduct && currentProduct.primaryBrand) || alternateBrand,
        isBossBopisOrder: isPDPBossBopisEnabled && deliveryMethodType !== 'shipIt',
      };
      addToBagEcomNew(cartItemInfo1);
    } else addToBagEcom(cartItemInfo);
  };

  getOOSFlag = (productInfo) => {
    const { colorFitsSizesMap, tcpMultiPackReferenceUSStore, multiPackUSStore } = productInfo;
    const filteredColorFitsList =
      colorFitsSizesMap && colorFitsSizesMap.filter((item) => item.maxAvailable === 0);

    if (
      (colorFitsSizesMap && colorFitsSizesMap.length) ===
        (filteredColorFitsList && filteredColorFitsList.length) &&
      !(tcpMultiPackReferenceUSStore && multiPackUSStore)
    ) {
      return true;
    }
    return false;
  };

  /**
   * @function checkIfSBP This function checks if the current flow is SBP or not.
   * @returns {Boolean} true if the flow is SBP, else false.
   */
  checkIfSBP = (selectActiveWishListFromState) => {
    const { navigation } = this.props;
    return navigation && selectActiveWishListFromState && navigation.getParam('isSBP');
  };

  getFormNameProductAddToBag = (newProductId) => {
    const { currentProduct } = this.props;
    if (currentProduct) return `ProductAddToBag-${newProductId || currentProduct.generalProductId}`;
    return '';
  };

  onUpdateSize = (size, productId) => {
    const { updateSize, isNewReDesignEnabled } = this.props;
    const form = this.getFormNameProductAddToBag(productId);
    if (isNewReDesignEnabled) updateSize({ form, size });
  };

  onUpdateColor = (color, newProductId) => {
    const { updateColor, isNewReDesignEnabled } = this.props;
    const form = this.getFormNameProductAddToBag(newProductId);
    if (isNewReDesignEnabled) updateColor({ form, color });
  };

  onUpdateFit = (fit) => {
    const { updateFit, isNewReDesignEnabled } = this.props;
    const form = this.getFormNameProductAddToBag();
    if (isNewReDesignEnabled) updateFit({ form, fit });
  };

  getIsStoreBopisEligible = (bopisItemInventory) => {
    return (
      this.isBopisEligible &&
      bopisItemInventory &&
      bopisItemInventory.inventoryResponse &&
      bopisItemInventory.inventoryResponse.length > 0 &&
      bopisItemInventory.inventoryResponse[0].quantity > 0
    );
  };

  getProductsFromBundle = (currentBundle) => {
    return currentBundle.map((product) => ({ ...product.products }));
  };

  getCurrentColorEntry = (colorEntry) => {
    this.setState({ changedCurrentColorEntry: colorEntry });
  };

  renderSkeleton = (
    isProductDataAvailable,
    retrievedProductInfo,
    showPriceRange,
    sbpRetrievedProductInfo
  ) => {
    const {
      breadCrumbs,
      pdpLabels,
      currency,
      currencyAttributes,
      isKeepAliveEnabled,
      outOfStockLabels,
      isLoading,
      isPlcc,
      accessibilityLabels,
      isBrierleyPromoEnabled,
      isDynamicBadgeEnabled,
      isNewReDesignEnabled,
      primaryBrand,
      alternateBrand,
      navigation,
    } = this.props;
    const isSBP = navigation && navigation.getParam('isSBP');
    const showSkeleton =
      isNewReDesignEnabled && !isSBP
        ? isLoading && !isProductDataAvailable && !retrievedProductInfo
        : isLoading;
    return showSkeleton ? (
      <ProductDetailSkeleton
        retrievedProductInfo={sbpRetrievedProductInfo || retrievedProductInfo}
        breadCrumbs={breadCrumbs}
        pdpLabels={pdpLabels}
        outOfStockLabels={outOfStockLabels}
        isKeepAliveEnabled={isKeepAliveEnabled}
        accessibilityLabels={accessibilityLabels}
        currencyExchange={currencyAttributes.exchangevalue}
        currency={currency}
        isPlcc={isPlcc}
        selectedColorProductId={this.selectedColorProductId}
        showPriceRange={showPriceRange}
        isBrierleyPromoEnabled={isBrierleyPromoEnabled}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        isNewReDesignForNewPDPSkeleton={isNewReDesignEnabled}
        primaryBrand={primaryBrand}
        alternateBrand={alternateBrand}
      />
    ) : null;
  };

  // eslint-disable-next-line complexity, sonarjs/cognitive-complexity
  render() {
    const {
      currentProduct,
      breadCrumbs,
      navTree,
      plpLabels,
      pdpLabels,
      navigation,
      addToBagError,
      clearAddToBagError,
      isPickupModalOpen,
      longDescription,
      shortDescription,
      itemPartNumber,
      onAddItemToFavorites,
      isLoggedIn,
      currency,
      currencyAttributes,
      alternateSizes,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      toastMessage,
      isKeepAliveEnabled,
      outOfStockLabels,
      middlePromos,
      bottomPromos,
      belowrecsPromos,
      isLoading,
      trackPageLoad,
      sizeChartDetails,
      isPlcc,
      isOnModelAbTestPdp,
      stylisticsProductTabList,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      trackClickAction,
      accessibilityLabels,
      setClickAnalyticsDataAction,
      productListingDetail,
      searchListingDetail,
      defaultWishListFromState,
      addBtnMsg,
      multiPackCount,
      multiPackThreshold,
      multipackProduct,
      partIdInfo,
      getAllNewMultiPack,
      selectedMultipack,
      isPerUnitPriceEnabled,
      getDetails,
      getRecommendationProduct,
      recommendationListingDetail,
      outfitProducts,
      isPdpNewRecommendationEnabled,
      isBrierleyPromoEnabled,
      isOosPdpEnabled,
      isPdp1andPdp2Enabled,
      singlePageLoad,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      formValues,
      disableMultiPackTab,
      getDisableSelectedTab,
      istMultiColorNameEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      sbpSizeClick,
      addToCartError,
      activeWishListProducts,
      selectActiveWishListFromState,
      isDynamicBadgeEnabled,
      ledgerSummaryData,
      sbpLabels,
      availableTCPmapNewStyleId,
      cartsSessionStatus,
      isNewReDesignEnabled,
      pickupLabels,
      onPickUpOpenClick,
      isBopisEnabled,
      isBopisClearanceProductEnabled,
      isBossEnabled,
      isBossClearanceProductEnabled,
      isQVModalOpen,
      openApplyNowModal,
      quickViewLoader,
      quickViewProductId,
      fireHapticFeedback,
      userDefaultStore,
      bopisItemInventory,
      userNearByStore,
      alternateBrand,
      cartProducts,
      isBossBopisPDPSizeDrawer,
      resetBopisInventoryData,
      socialProofMessage,
      getIsPDPCalledViaRecommendation,
      currentBundle,
      fromBagPage,
      isSelectedSizeDisabled,
      updateSizeStatus,
    } = this.props;
    const checkForOOSForAllVariantsFlag = this.getOOSFlag(currentProduct);
    const isProductDataAvailable = Object.keys(currentProduct).length > 0;
    const { renderedPid, changedCurrentColorEntry } = this.state;
    const getPriceRange = isShowPriceRangeKillSwitch
      ? parseBoolean(isShowPriceRangeKillSwitch)
      : false;
    let dataToCarryForward =
      this.clickOrigin === 'search' ? searchListingDetail : productListingDetail;
    let productType = '';
    if (navigation && navigation.getParam('viaModule') === 'mbox_Recommendation') {
      const viaModule =
        navigation.getParam('clickOrigin') === 'pdp'
          ? 'PreviousPDP'
          : navigation.getParam('clickOrigin');
      productType = navigation.getParam('viaModule');
      dataToCarryForward =
        recommendationListingDetail &&
        recommendationListingDetail.get(viaModule) &&
        recommendationListingDetail.get(viaModule).products;
    }

    let retrievedProductInfo = '';

    if (navigation && navigation.getParam('viaModule') === 'outfit') {
      dataToCarryForward = outfitProducts;
      if (navigation.getParam('isFromBundlePage'))
        dataToCarryForward = this.getProductsFromBundle(currentBundle);
      retrievedProductInfo = getProductInfoFromOutfit({
        pid: this.selectedColorProductId,
        details: dataToCarryForward,
      });
    } else {
      retrievedProductInfo = getProductInfoFromProductListingData({
        pid: this.selectedColorProductId,
        details: dataToCarryForward,
        type: productType,
      });
    }

    if (navigation && navigation.getParam('fromCartPage')) {
      dataToCarryForward = cartProducts;
      retrievedProductInfo = getProductInfoFromCart({
        pid: this.selectedColorProductId,
        details: dataToCarryForward,
      });
    }

    const getABTestPriceRange = showPriceRangeForABTest;
    const showPriceRange = isShowPriceRangeKillSwitch && showPriceRangeForABTest;
    const sbpRetrievedProductInfo = navigation && navigation.getParam('retrievedProductInfo');

    const isSBP = this.checkIfSBP(selectActiveWishListFromState);
    let wishList = defaultWishListFromState;
    // Checking if the current flow is SBP.
    if (isSBP) {
      const wishListSBP = {};
      /**
       * The defaultWishListFromState has a certain structure. When its the SBP flow then we need to
       * transform active wish list to the same structure so that the Image carousel component's
       *  actions perform well.
       */
      selectActiveWishListFromState.items.map((itm) => {
        wishListSBP[itm.skuInfo.color.imagePath] = {
          isInDefaultWishlist: selectActiveWishListFromState.isDefault,
          externalIdentifier: selectActiveWishListFromState.id,
          giftListItemID: itm.itemInfo.itemId,
        };
        return null;
      });
      wishList = wishListSBP;
    }

    const bossValidatingParams = {
      isBossClearanceProductEnabled,
      isBossEnabled,
    };
    const bopisValidatingParams = {
      isBopisClearanceProductEnabled,
      isBopisEnabled,
    };

    const currentColorEntry = getMapSliceForColorProductId(
      currentProduct?.colorFitsSizesMap || retrievedProductInfo?.colorFitsSizesMap,
      this.selectedColorProductId
    );
    const colorObj = changedCurrentColorEntry
      ? changedCurrentColorEntry || {}
      : JSON.parse(JSON.stringify(currentColorEntry)) || {};
    const { miscInfo } = JSON.parse(JSON.stringify(colorObj)) || {};
    const isDefaultBadge = getIsDefaultProductBadge(miscInfo);
    this.isBopisEligible =
      validateBopisEligibility({ ...bopisValidatingParams, miscInfo }) && !isDefaultBadge;
    this.isBossEligible =
      validateBossEligibility({ ...bossValidatingParams, miscInfo }) && !isDefaultBadge;
    const isStoreBopisEligible = this.getIsStoreBopisEligible(bopisItemInventory);

    const showPDP =
      isNewReDesignEnabled && !isSBP
        ? (isNewReDesignEnabled && retrievedProductInfo) ||
          (isProductDataAvailable && currentProduct.productId)
        : isProductDataAvailable && currentProduct.productId;

    return (
      <React.Fragment>
        {showPDP && (
          <ProductDetail
            retrievedProductInfo={retrievedProductInfo}
            ledgerSummaryData={ledgerSummaryData}
            currentProduct={currentProduct}
            breadCrumbs={breadCrumbs}
            navTree={navTree}
            selectedColorProductId={this.selectedColorProductId}
            plpLabels={plpLabels}
            pdpLabels={pdpLabels}
            handleFormSubmit={this.handleAddToBag}
            handleAddToBag={this.handleAddToBagNew}
            navigation={navigation}
            addToBagError={addToBagError}
            clearAddToBagError={clearAddToBagError}
            isPickupModalOpen={isPickupModalOpen}
            shortDescription={shortDescription}
            itemPartNumber={itemPartNumber}
            longDescription={longDescription}
            onAddItemToFavorites={onAddItemToFavorites}
            isLoggedIn={isLoggedIn}
            currency={currency}
            currencyExchange={currencyAttributes.exchangevalue}
            alternateSizes={alternateSizes}
            AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
            removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
            toastMessage={toastMessage}
            fireHapticFeedback={fireHapticFeedback}
            isKeepAliveEnabled={isKeepAliveEnabled}
            outOfStockLabels={outOfStockLabels}
            middlePromos={middlePromos}
            bottomPromos={bottomPromos}
            belowrecsPromos={belowrecsPromos}
            renderedPid={renderedPid}
            trackPageLoad={trackPageLoad}
            sizeChartDetails={sizeChartDetails}
            isPlcc={isPlcc}
            isOnModelAbTestPdp={isOnModelAbTestPdp}
            stylisticsProductTabList={stylisticsProductTabList}
            isShowPriceRangeKillSwitch={getPriceRange}
            showPriceRangeForABTest={getABTestPriceRange}
            trackClickAction={trackClickAction}
            accessibilityLabels={accessibilityLabels}
            setClickAnalyticsDataAction={setClickAnalyticsDataAction}
            defaultWishListFromState={wishList}
            addBtnMsg={addBtnMsg}
            multiPackThreshold={multiPackThreshold}
            multiPackCount={multiPackCount}
            multipackProduct={multipackProduct}
            partIdInfo={partIdInfo}
            getAllNewMultiPack={getAllNewMultiPack}
            isRecommendationAvailable={getRecommendation(getRecommendationProduct)}
            checkForOOSForVariant={checkForOOSForAllVariantsFlag}
            isPdpNewRecommendationEnabled={isPdpNewRecommendationEnabled}
            isBrierleyPromoEnabled={isBrierleyPromoEnabled}
            isOosPdpEnabled={isOosPdpEnabled}
            isPdp1andPdp2Enabled={isPdp1andPdp2Enabled}
            selectedMultipack={selectedMultipack}
            isPerUnitPriceEnabled={isPerUnitPriceEnabled}
            getDetails={getDetails}
            singlePageLoad={singlePageLoad}
            initialMultipackMapping={initialMultipackMapping}
            setInitialTCPStyleQty={setInitialTCPStyleQty}
            formValues={formValues}
            disableMultiPackTab={disableMultiPackTab}
            getDisableSelectedTab={getDisableSelectedTab}
            istMultiColorNameEnabled={istMultiColorNameEnabled}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
            sbpSizeClick={sbpSizeClick}
            AddToCartError={addToCartError}
            activeWishListProducts={activeWishListProducts}
            activeWishListId={this.activeWishListId}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            sbpLabels={sbpLabels}
            availableTCPmapNewStyleId={availableTCPmapNewStyleId}
            cartsSessionStatus={cartsSessionStatus}
            isNewReDesignEnabled={isNewReDesignEnabled}
            pickupLabels={pickupLabels}
            onPickUpOpenClick={onPickUpOpenClick}
            updateSizeValue={this.onUpdateSize}
            updateColor={this.onUpdateColor}
            updateFit={this.onUpdateFit}
            isLoading={isLoading}
            isBopisEligible={this.isBopisEligible}
            isBossEligible={this.isBossEligible}
            isQVModalOpen={isQVModalOpen}
            primaryBrand={currentProduct && currentProduct.primaryBrand}
            openApplyNowModal={openApplyNowModal}
            quickViewLoader={quickViewLoader}
            quickViewProductId={quickViewProductId}
            userDefaultStore={userDefaultStore}
            bopisItemInventory={bopisItemInventory}
            isStoreBopisEligible={isStoreBopisEligible}
            userNearByStore={userNearByStore}
            alternateBrand={alternateBrand}
            isBossBopisPDPSizeDrawer={isBossBopisPDPSizeDrawer}
            resetBopisInventoryData={resetBopisInventoryData}
            fromBagPage={fromBagPage}
            getCurrentColorEntry={this.getCurrentColorEntry}
            socialProofMessage={socialProofMessage}
            getIsPDPCalledViaRecommendation={getIsPDPCalledViaRecommendation}
            isSelectedSizeDisabled={isSelectedSizeDisabled}
            updateSizeStatus={updateSizeStatus}
          />
        )}
        {this.renderSkeleton(
          isProductDataAvailable,
          retrievedProductInfo,
          showPriceRange,
          sbpRetrievedProductInfo
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const userDefaultStore = PickupSelectors.getDefaultStore(state);
  const geoDefaultStore = PickupSelectors.getGeoDefaultStore(state);
  const nearByStore = getUserNearStore(state) || null;
  const changeSelectedStores = PickupSelectors.getChangedSelectedStore(state);
  let addressStore = userDefaultStore || (nearByStore && nearByStore[0]) || geoDefaultStore || null;
  if (changeSelectedStores && changeSelectedStores?.basicInfo?.storeName) {
    addressStore = changeSelectedStores;
  }
  return {
    navTree: getNavTree(state),
    productDetails: prodDetails(state),
    isLoading: getPDPLoadingState(state),
    currentProduct: getCurrentProduct(state),
    breadCrumbs: getBreadCrumbs(state),
    plpLabels: getPlpLabels(state),
    pdpLabels: getPDPLabels(state),
    isPickupModalOpen: getIsPickupModalOpen(state),
    addToBagError: getAddedToBagError(state),
    formValues: getProductDetailFormValues(state),
    shortDescription: getShortDescription(state),
    itemPartNumber: getGeneralProductId(state),
    longDescription: getDescription(state),
    isLoggedIn: getUserLoggedInState(state) && !isRememberedUser(state),
    isPlcc: isPlccUser(state),
    currency: getCurrentCurrency(state),
    currencyAttributes: getCurrencyAttributes(state),
    alternateSizes: getAlternateSizes(state),
    AddToFavoriteErrorMsg: fetchAddToFavoriteErrorMsg(state),
    isKeepAliveEnabled: getIsKeepAliveProductApp(state),
    outOfStockLabels: getLabelsOutOfStock(state),
    middlePromos: getPDPMiddlePromos(state),
    bottomPromos: getPDPBottomPromos(state),
    belowrecsPromos: getPDPBelowRecsPromos(state),
    productListingDetail: getProductListingDetail(state),
    searchListingDetail: getSearchListingPage(state),
    sizeChartDetails: getSizeChartDetails(state),
    isOnModelAbTestPdp: getOnModelImageAbTestPdp(state),
    accessibilityLabels: getAccessibilityLabels(state),
    stylisticsProductTabList: getStyliticsProductTabListSelector(state),
    isShowPriceRangeKillSwitch: getIsShowPriceRangeForApp(state),
    showPriceRangeForABTest: getABTestIsShowPriceRange(state),
    defaultWishListFromState: wishListFromState(state),
    selectActiveWishListFromState: selectActiveWishList(state),
    addBtnMsg: addedToBagMsgSelector(state),
    multipackProduct: getMultiPackProductInfo(state),
    multiPackCount: getMultiPackCount(state),
    multiPackThreshold: getMultiPackThreshold(state),
    getAllMultiPack: getAllMultiPackData(state),
    partIdInfo: partIdInformation(state),
    getAllNewMultiPack: getAllNewMultiPackData(state),
    selectedMultipack: getSelectedMultipack(state),
    isPerUnitPriceEnabled: getIsPerUnitPriceEnabled(state),
    getRecommendationProduct: getProducts(state, 'pdp'),
    recommendationListingDetail: getCompleteRecommendations(state),
    isPdpNewRecommendationEnabled: getPdpNewRecommendation(state),
    isBrierleyPromoEnabled: getIsBrierleyPromoEnabled(state),
    isOosPdpEnabled: getIsOosPdpEnabled(state),
    isPdp1andPdp2Enabled: getIsPdp1andPdp2Enabled(state),
    singlePageLoad: getSinglePageLoad(state),
    initialMultipackMapping: getInitialMultipackMapping(state),
    setInitialTCPStyleQty: getInitialTCPStyleQTY(state),
    deleteFavItemInProgressFlag: getDefaultFavItemInProgressFlag(state),
    disableMultiPackTab: getDisableMultiPackTab(state),
    istMultiColorNameEnabled: getMultiColorNameEnabled(state),
    wishlistsSummaries: selectWishlistsSummaries(state),
    activeWishListProducts: selectActiveWishlistProducts(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    ledgerSummaryData: getLedgerSummaryData(state),
    sbpLabels: getSBPLabels(state),
    availableTCPmapNewStyleId: getInitialAvailableTCPmapNewStyleId(state),
    cartsSessionStatus: getCartsSessionStatus(state),
    isNewReDesignEnabled: getIsNewReDesignEnabled(state),
    isBossEnabled: getIsBossEnabled(state),
    isBopisEnabled: getIsBopisEnabled(state),
    isBossClearanceProductEnabled: PickupSelectors.getIsBossClearanceProductEnabled(state),
    isBopisClearanceProductEnabled: PickupSelectors.getIsBopisClearanceProductEnabled(state),
    pickupLabels: {
      ...PickupSelectors.getLabels(state),
      ...PickupSelectors.getAccessibilityLabels(state),
    },
    isATBModalBackAbTestNewDesign: getIsATBModalBackAbTestNewDesign(state),
    outfitProducts: getOutfitProducts(state),
    isQVModalOpen: getModalState(state),
    quickViewLoader: getQuickViewLoader(state),
    quickViewProductId: getUniqueId(state),
    bopisItemInventory: PickupSelectors.getBopisItemInventory(state),
    userDefaultStore: addressStore,
    userNearByStore: nearByStore,
    alternateBrand: getAlternateBrandName(state),
    cartProducts: BagPageSelectors.getOrderItemsLocal(state),
    isBossBopisPDPSizeDrawer: PickupSelectors.getEnableBossBopisPDPSizeDrawer(state),
    isPDPBossBopisEnabled: PickupSelectors.getIsPDPBossBopisEnabled(state),
    socialProofMessage: getSocialProofMessage(state),
    currentBundle: getCurrentBundle(state),
    fromBagPage: getIsPickupModalOpenFromBagPage(state),
    isSelectedSizeDisabled: getSelectedSizeDisabled(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    resetProductDetails: () => {
      dispatch(setProductDetails({ product: {} }));
    },
    getDetails: (payload) => {
      dispatch(getProductDetails(payload));
    },
    addToBagEcom: (payload) => {
      dispatch(addToCartEcom(payload));
    },
    addToBagEcomNew: (payload) => {
      dispatch(addToCartEcomNewDesign(payload));
    },
    clearAddToBagError: () => {
      dispatch(clearAddToBagErrorState());
    },
    onAddItemToFavorites: (payload) => {
      dispatch(addItemsToWishlist(payload));
    },
    removeAddToFavoritesErrorMsg: (payload) => {
      dispatch(removeAddToFavoriteErrorState(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    trackPageLoad: (payload, customData) => {
      dispatch(setClickAnalyticsData(customData));
      dispatch(trackPageView(payload));
      setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
      }, 300);
    },
    trackClickAction: (data) => {
      dispatch(trackClick(data));
    },
    setClickAnalyticsDataAction: (data) => {
      dispatch(setClickAnalyticsData(data));
    },
    getDisableSelectedTab: (payload) => {
      dispatch(setDisableSelectedTab(payload));
    },
    getIsPDPCalledViaRecommendation: (payload) => {
      dispatch(setIsPDPCalledFromRecommendation(payload));
    },
    onSetLastDeletedItemIdAction: (payload) => {
      dispatch(setLastDeletedItemIdAction(payload));
    },
    sbpSizeClick: ({ clickData, name, module }) => {
      dispatch(setClickAnalyticsData(clickData));
      dispatch(trackClick({ name, module }));
    },
    addToCartError: (data) => {
      dispatch(AddToCartError(data));
    },
    onPickUpOpenClick: (payload) => {
      dispatch(openPickupModalWithValues(payload));
    },
    updateSize: (payload) => {
      dispatch(change(payload.form, 'Size', payload.size));
    },
    updateColor: (payload) => {
      dispatch(change(payload.form, 'color', payload.color));
    },
    updateFit: (payload) => {
      dispatch(change(payload.form, 'Fit', payload.fit));
    },
    openApplyNowModal: (payload) => {
      dispatch(toggleApplyNowModal(payload));
    },
    fireHapticFeedback: (payload) => {
      dispatch(playHapticFeedback(payload));
    },
    resetChangeSeletedStore: (payload) => {
      dispatch(resetSelectedChangedStoreActiom(payload));
    },
    resetBopisInventoryData: () => {
      dispatch(setBopisInventoryDetailsAction({}));
    },
    resetSocialProofData: () => {
      dispatch(clearSocialProofData());
    },
    updateSizeStatus: (payload) => {
      dispatch(updateSelectedSizeStatus(payload));
    },
  };
}

ProductDetailContainer.propTypes = {
  primaryBrand: PropTypes.string.isRequired,
  currentProduct: PropTypes.shape({
    generalProductId: PropTypes.string,
    primaryBrand: PropTypes.string,
    productId: PropTypes.string,
    colorFitsSizesMap: PropTypes.shape([]),
  }),
  getDetails: PropTypes.func.isRequired,
  clearAddToBagError: PropTypes.func.isRequired,
  breadCrumbs: PropTypes.shape({}),
  navigation: PropTypes.shape({
    getParam: PropTypes.func.isRequired,
    setParams: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
    addListener: PropTypes.func.isRequired,
    state: PropTypes.shape({
      params: PropTypes.shape({
        profile: PropTypes.shape({
          name: PropTypes.string,
        }),
        departmentChange: PropTypes.bool,
      }),
    }),
  }).isRequired,
  addToBagEcom: PropTypes.func.isRequired,
  navTree: PropTypes.shape({}),
  plpLabels: PropTypes.shape({}),
  pdpLabels: PropTypes.shape({}),
  isPickupModalOpen: PropTypes.bool,
  addToBagError: PropTypes.string,
  formValues: PropTypes.shape({
    color: PropTypes.shape({}),
    size: PropTypes.shape({}),
    quantity: PropTypes.shape({}),
  }).isRequired,
  shortDescription: PropTypes.string,
  itemPartNumber: PropTypes.string,
  longDescription: PropTypes.string,
  onAddItemToFavorites: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  currency: PropTypes.string,
  currencyAttributes: PropTypes.shape({
    exchangevalue: PropTypes.string,
  }),
  alternateSizes: PropTypes.shape({
    key: PropTypes.string,
  }),
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  toastMessage: PropTypes.func,
  isKeepAliveEnabled: PropTypes.bool,
  outOfStockLabels: PropTypes.shape({}),
  middlePromos: PropTypes.string,
  bottomPromos: PropTypes.string,
  belowrecsPromos: PropTypes.string,
  isLoading: PropTypes.string,
  isPlcc: PropTypes.bool,
  trackPageLoad: PropTypes.func,
  sizeChartDetails: PropTypes.shape([]).isRequired,
  isOnModelAbTestPdp: PropTypes.bool,
  stylisticsProductTabList: PropTypes.shape({}).isRequired,
  resetProductDetails: PropTypes.func.isRequired,
  isShowPriceRangeKillSwitch: PropTypes.bool.isRequired,
  showPriceRangeForABTest: PropTypes.bool.isRequired,
  trackClickAction: PropTypes.func.isRequired,
  accessibilityLabels: PropTypes.shape({}),
  setClickAnalyticsDataAction: PropTypes.func.isRequired,
  productListingDetail: PropTypes.shape({}),
  searchListingDetail: PropTypes.shape({}),
  defaultWishListFromState: PropTypes.shape({}),
  addBtnMsg: PropTypes.string.isRequired,
  multipackProduct: PropTypes.string.isRequired,
  multiPackCount: PropTypes.string.isRequired,
  multiPackThreshold: PropTypes.string.isRequired,
  getAllMultiPack: PropTypes.shape({}).isRequired,
  partIdInfo: PropTypes.string.isRequired,
  getAllNewMultiPack: PropTypes.shape({}).isRequired,
  selectedMultipack: PropTypes.string,
  isPerUnitPriceEnabled: PropTypes.bool,
  getRecommendationProduct: PropTypes.bool.isRequired,
  recommendationListingDetail: PropTypes.bool.isRequired,
  outfitProducts: PropTypes.bool.isRequired,
  productDetails: PropTypes.shape({}).isRequired,
  isPdpNewRecommendationEnabled: PropTypes.bool,
  isBrierleyPromoEnabled: PropTypes.bool,
  isOosPdpEnabled: PropTypes.bool,
  isPdp1andPdp2Enabled: PropTypes.bool,
  singlePageLoad: PropTypes.bool.isRequired,
  initialMultipackMapping: PropTypes.shape({}).isRequired,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  disableMultiPackTab: PropTypes.bool.isRequired,
  getDisableSelectedTab: PropTypes.func.isRequired,
  istMultiColorNameEnabled: PropTypes.bool.isRequired,
  onSetLastDeletedItemIdAction: PropTypes.func,
  deleteFavItemInProgressFlag: PropTypes.bool,
  sbpSizeClick: PropTypes.func.isRequired,
  addToCartError: PropTypes.string,
  wishlistsSummaries: PropTypes.shape([]).isRequired,
  selectActiveWishListFromState: PropTypes.shape({
    isDefault: PropTypes.bool,
    id: PropTypes.string,
    items: PropTypes.shape({
      map: PropTypes.shape([]),
    }),
  }).isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  activeWishListProducts: PropTypes.shape([]).isRequired,
  ledgerSummaryData: PropTypes.bool.isRequired,
  sbpLabels: PropTypes.shape({}).isRequired,
  availableTCPmapNewStyleId: PropTypes.shape([]),
  cartsSessionStatus: PropTypes.number.isRequired,
  isNewReDesignEnabled: PropTypes.bool,
  isATBModalBackAbTestNewDesign: PropTypes.bool,
  pickupLabels: PropTypes.shape({}).isRequired,
  onPickUpOpenClick: PropTypes.func.isRequired,
  openApplyNowModal: PropTypes.func.isRequired,
  updateSize: PropTypes.func,
  updateColor: PropTypes.func,
  updateFit: PropTypes.func,
  addToBagEcomNew: PropTypes.func,
  isBossEnabled: PropTypes.bool,
  isBopisEnabled: PropTypes.bool,
  isBopisClearanceProductEnabled: PropTypes.bool,
  isBossClearanceProductEnabled: PropTypes.bool,
  isQVModalOpen: PropTypes.bool,
  quickViewLoader: PropTypes.bool,
  quickViewProductId: PropTypes.string,
  fireHapticFeedback: PropTypes.func,
  bopisItemInventory: PropTypes.arrayOf(PropTypes.object),
  userNearByStore: PropTypes.arrayOf(PropTypes.object),
  userDefaultStore: PropTypes.shape({
    basicInfo: PropTypes.shape({
      id: PropTypes.string.isRequired,
      storeName: PropTypes.string.isRequired,
    }).isRequired,
  }),
  cartProducts: PropTypes.shape([]),
  isBossBopisPDPSizeDrawer: PropTypes.bool,
  resetChangeSeletedStore: PropTypes.shape({}),
  resetBopisInventoryData: PropTypes.func,
  alternateBrand: PropTypes.string,
  isPDPBossBopisEnabled: PropTypes.bool,
  socialProofMessage: PropTypes.string,
  currentBundle: PropTypes.shape([]),
  fromBagPage: PropTypes.bool,
  resetSocialProofData: PropTypes.func,
  isSelectedSizeDisabled: PropTypes.bool,
  updateSizeStatus: PropTypes.func,
};

ProductDetailContainer.defaultProps = {
  currentProduct: {},
  breadCrumbs: {},
  navTree: {},
  plpLabels: {},
  pdpLabels: {},
  isPickupModalOpen: false,
  addToBagError: '',
  shortDescription: '',
  itemPartNumber: '',
  longDescription: '',
  onAddItemToFavorites: null,
  isLoggedIn: false,
  currency: 'USD',
  currencyAttributes: {
    exchangevalue: 1,
  },
  alternateSizes: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  toastMessage: () => {},
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  middlePromos: '',
  bottomPromos: '',
  belowrecsPromos: '',
  isLoading: '',
  isPlcc: false,
  trackPageLoad: () => {},
  accessibilityLabels: {},
  isOnModelAbTestPdp: false,
  productListingDetail: null,
  searchListingDetail: null,
  defaultWishListFromState: {},
  selectedMultipack: '1',
  isPerUnitPriceEnabled: false,
  isPdpNewRecommendationEnabled: false,
  isBrierleyPromoEnabled: false,
  isOosPdpEnabled: false,
  isPdp1andPdp2Enabled: true,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  addToCartError: '',
  isDynamicBadgeEnabled: {},
  availableTCPmapNewStyleId: [],
  isNewReDesignEnabled: false,
  isATBModalBackAbTestNewDesign: false,
  updateSize: () => {},
  updateColor: () => {},
  updateFit: () => {},
  addToBagEcomNew: () => {},
  isBossEnabled: false,
  isBopisEnabled: false,
  isBopisClearanceProductEnabled: false,
  isBossClearanceProductEnabled: false,
  isQVModalOpen: false,
  quickViewLoader: false,
  quickViewProductId: '',
  fireHapticFeedback: () => {},
  bopisItemInventory: [],
  userNearByStore: [],
  userDefaultStore: {
    basicInfo: {
      id: '',
      storeName: '',
    },
  },
  cartProducts: [],
  isBossBopisPDPSizeDrawer: false,
  resetChangeSeletedStore: {},
  resetBopisInventoryData: () => {},
  alternateBrand: '',
  isPDPBossBopisEnabled: false,
  socialProofMessage: '',
  currentBundle: [],
  fromBagPage: false,
  resetSocialProofData: () => {},
  isSelectedSizeDisabled: false,
  updateSizeStatus: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailContainer);
export { ProductDetailContainer as ProductDetailContainerVanilla };
