/* eslint-disable max-lines */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { LAZYLOAD_HOST_NAME, scrollToViewBottom } from '@tcp/core/src/utils';
import {
  ScrollView as LazyloadScrollView,
  View,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import isEqual from 'lodash/isEqual';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import { TIER1 } from '@tcp/core/src/constants/tiering.constants';
import {
  constructPageName,
  isIOS,
  getValueFromAsyncStorage,
  setValueInAsyncStorage,
  setRecommendationsObj,
} from '@tcp/core/src/utils/utils.app';
import { formatProductsData, getDynamicBadgeQty, getBrand } from '@tcp/core/src/utils/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import CustomIcon from '@tcp/core/src/components/common/atoms/Icon';
import sbpConstant from '@tcp/core/src/components/features/browse/ShopByProfile/container/ShopByProfile.constants';
import ModalBox from 'react-native-modalbox';
import GlobalModals from '../../../../../../../mobileapp/src/components/common/organisms/GlobalModals';
import {
  SBP_SIZE_MAPPING,
  SBP_CATEGORY_ID_MAPPING,
  SBP_ONE_SIZE,
  BRAND_ID,
} from '../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.constants';
import names from '../../../../../constants/eventsName.constants';
import withStyles from '../../../../common/hoc/withStyles.native';
import ImageCarousel from '../molecules/ImageCarousel';
import {
  PageContainer,
  LoyaltyBannerView,
  RecommendationWrapper,
  PromoMiddleContainer,
  PromoBottomContainer,
  Margin,
  ReviewsAndRattingContainer,
  OosText,
  RecommendationOosWrapper,
  RecommendationMargin,
  ProductDetailStyle,
} from '../styles/ProductDetail.style.native';
import ProductAddToBagContainer from '../../../../common/molecules/ProductAddToBag';
import ProductSummary from '../molecules/ProductSummary';
import ProductPickupContainer from '../../../../common/organisms/ProductPickup';
import {
  getImagesToDisplay,
  getMapSliceForColorProductId,
  getSkuId,
  getMapSliceForColor,
  getMapSliceForSizeSkuID,
  getPricesWithRange,
  getPrices,
  getMapColorList,
} from '../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import SIZE_CHART_LINK_POSITIONS from '../../../../common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import { FullScreenImageCarousel } from '../../../../common/molecules/index.native';
import ProductDetailDescription from '../molecules/ProductDescription/views/ProductDescription.view.native';
import RelatedOutfits from '../molecules/RelatedOutfits/views';
import SendAnEmailGiftCard from '../molecules/SendAnEmailGiftCard';
import LoyaltyBanner from '../../../CnC/LoyaltyBanner';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import ProductReviewsContainer from '../../ProductListing/molecules/ProductReviews/container/ProductReviews.container';
import PromoPDPBanners from '../../../../common/organisms/PromoPDPBanners';
import config from '../../../../common/molecules/ProductDetailImage/config';
import ProductDetailOldPropTypes from '../ProductDetailPropTypes';
import RecommendationsAbstractor from '../../../../../services/abstractors/common/recommendations';
import { showSBPLabels } from '../../../../../../../mobileapp/src/components/features/shopByProfile/PLP/helper';

const closeImage = require('../../../../../assets/close.png');

class ProductDetailOld extends React.Component {
  // on app candidate module will not be display [RWD-19103]
  isShowCandidateModule = false;

  constructor(props) {
    super(props);
    const {
      currentProduct: { colorFitsSizesMap },
      currentProduct,
      selectedColorProductId,
    } = this.props;
    this.state = {
      showCarousel: false,
      currentColorEntry: getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId),
      currentGiftCardValue: currentProduct.offerPrice,
      selectedColorProductId,
      showCompleteTheLook: false,
      size: '',
      fit: '',
      expanded: false,
      activeIndex: 0,
      expandReviewFromAnchor: false,
      isAddToBagShow: true,
      isSizeDrawerOpen: false,
      showSize: false,
      sbpFormValues: null,
      accessorySize: '',
    };
    this.currentScrollValue = 0;
    this.scrollPageToTarget = this.scrollPageToTarget.bind(this);
    this.isTransparentHeader = props.navigation && props.navigation.getParam('isTransparentHeader');
    this.isIOSApp = isIOS();
    const { currentColorEntry } = this.state;
    const colorName = currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;
    this.formValues = {
      Fit: '',
      Size: '',
      color: colorName,
      Quantity: 1,
    };
  }

  static getDerivedStateFromProps(props, state) {
    const { currentProduct } = props;

    const { currentColorEntry, selectedColorProductId } = state;

    const colorDetails = getMapSliceForColorProductId(
      currentProduct.colorFitsSizesMap,
      selectedColorProductId
    );

    if (
      colorDetails.favoritedCount !== currentColorEntry.favoritedCount &&
      colorDetails.color.name === currentColorEntry.color.name
    ) {
      return {
        currentColorEntry: colorDetails || {},
      };
    }
    return null;
  }

  // eslint-disable-next-line complexity
  /* eslint-disable */
  async componentDidMount() {
    const {
      navigation,
      currentProduct,
      trackPageLoad,
      breadCrumbs,
      isExternalCampaignFired,
      setExternalCampaignFired,
    } = this.props;
    const { selectedColorProductId } = this.state;

    // Set the initial scroll value for header transparency to false.
    navigation.setParams({
      isScrolled: false,
    });

    let productsFormatted = formatProductsData(
      currentProduct,
      null,
      navigation,
      selectedColorProductId
    );

    const clickOrigin = (navigation && navigation.getParam('clickOrigin')) || '';
    const viaModule = (navigation && navigation.getParam('viaModule')) || '';
    const navigateType = (navigation && navigation.getParam('navigateType')) || '';
    const icidParam = (navigation && navigation.getParam('icidParam')) || '';
    const externalCampaignId = navigation.getParam('externalCampaignId');
    const omMID = navigation.getParam('omMID');
    const omRID = navigation.getParam('omRID');
    const viaModuleName = (
      (navigation && navigation.getParam('viaModuleName')) ||
      ''
    ).toLowerCase();
    const portalValue = navigation && navigation.getParam('portalValue');
    const NON_INTERNAL_SEARCH = 'non-internal search';
    const NON_BROWSE = 'non-browse';
    const NON_INTERNAL_CAMPAIGN = 'non-internal campaign';

    const recIdentifier = RecommendationsAbstractor.handleWhichRecommendationClicked(
      portalValue,
      clickOrigin
    );
    productsFormatted = productsFormatted.map((product) => {
      const productWithGender = { ...product };
      productWithGender.gender = this.getGender(breadCrumbs);

      // delete these two to suppress eVar20, event50, eVar21 in the product string
      delete productWithGender.recsProductId;
      delete productWithGender.recsPageType;

      return {
        ...productWithGender,
        ...this.getRecsObject(product, viaModule, recIdentifier, clickOrigin),
      };
    });

    const customEvents =
      navigateType === 'direct'
        ? ['prodView', 'event1', 'internalCampaignImpressions_e81', 'event100', 'event20']
        : ['prodView', 'event1', 'internalCampaignImpressions_e81'];
    if (externalCampaignId) {
      customEvents.push('event18');
      setExternalCampaignFired(true);
    }
    if (isExternalCampaignFired && !externalCampaignId) {
      customEvents.push('event19');
      setExternalCampaignFired(false);
    }
    let customData = {
      products: productsFormatted,
      customEvents,
    };
    const analyticsData = this.getAnalyticData(navigation);

    if (icidParam) {
      customData = {
        products: productsFormatted,
        internalCampaignId: icidParam,
        searchText: NON_INTERNAL_SEARCH,
        pageNavigationText: NON_BROWSE,
        originType: 'internal campaign',
        listingCount: NON_INTERNAL_SEARCH,
        searchType: NON_INTERNAL_SEARCH,
      };
      if (viaModule === 'bag_carousel_module') {
        customData.customEvents = ['internalCampaignImpressions_e81'];
      }
    } else if (viaModule === 'mbox_Recommendation') {
      productsFormatted[0].recsType = recIdentifier ? `monetate-${recIdentifier}` : '';
      customData = {
        products: productsFormatted,
        internalCampaignId: NON_INTERNAL_CAMPAIGN,
        searchText: NON_INTERNAL_SEARCH,
        searchType: NON_INTERNAL_SEARCH,

        customEvents: [
          'prodView',
          'Product_Views_Custom_e1',
          'internalCampaignImpressions_e81',
          names.eVarPropEvent.event50,
        ],
        originType: `cross-sell:${viaModuleName}`,
        pageNavigationText: NON_BROWSE,
        listingCount: NON_INTERNAL_SEARCH,
      };
      analyticsData.supressProps = ['eVar91'];
      analyticsData.currentScreen = 'outfitDetail';
    } else if (navigateType === 'direct') {
      // PDP item search
      customData = {
        internalCampaignId: NON_INTERNAL_CAMPAIGN,
        pageNavigationText: NON_BROWSE,
      };
    } else {
      analyticsData.supressProps = [names.eVarPropEvent.eVar19, 'eVar97'];
    }

    trackPageLoad(analyticsData, {
      ...customData,
      ...(externalCampaignId && {
        externalCampaignId,
      }),
      ...(omMID && { omMID }),
      ...(omRID && { omRID }),
    });
    const isSBP = navigation && navigation.getParam('isSBP');
    if (isSBP) {
      let accessorySize = '';
      let productSizeForPreselect = '';
      let productFitForPreselect = '';
      const profile = navigation.getParam('profile');
      accessorySize = this.isProductAnAccessory(currentProduct);
      if (!accessorySize) {
        const { sizeForPreselect, productFit } = await this.categoryIDMapping(
          currentProduct,
          profile
        );
        productSizeForPreselect = sizeForPreselect;
        productFitForPreselect = productFit;
      } else {
        productSizeForPreselect = accessorySize;
      }

      this.setState((prevState) => ({
        ...prevState,
        size: productSizeForPreselect,
        fit: productFitForPreselect,
        accessorySize,
      }));
    }
  }
  /* eslint-enable */

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate(prevProps, prevState) {
    const { navigation, currentProduct, trackPageLoad } = this.props;
    const { showCompleteTheLook } = this.state;

    if (
      prevState.showCompleteTheLook !== showCompleteTheLook &&
      prevState.showCompleteTheLook === true
    ) {
      const productsFormatted = formatProductsData(currentProduct, null, navigation);

      const analyticsData = this.getAnalyticData(navigation);
      trackPageLoad(analyticsData, {
        products: productsFormatted,
        customEvents: ['event74', 'event76', 'event95'],
      });
    }
  }

  checkaddtobag = ({ currentProduct, productData, pageShortName, pageName }) => {
    const { showSize } = this.state;
    const { navigation } = this.props;
    const brandId = navigation.getParam('brandId');
    const addToBagGymStyling = brandId === 'gym' ? { borderRadius: 25 } : {};
    return (
      <TouchableOpacity
        accessibilityRole="button"
        onPress={() => this.handleSizeClick(currentProduct, productData, pageName, pageShortName)}
        style={[
          !showSize ? ProductDetailStyle.touchableOpacity3 : ProductDetailStyle.modalBoxTouchable2,
          addToBagGymStyling,
        ]}
      >
        {this.addToBagTextSBP()}
      </TouchableOpacity>
    );
  };

  renderAddToBagButton = () => {
    const {
      handleFormSubmit,
      fitChanged,
      displayErrorMessage,
      keepAlive,
      currentProduct,
      selectedQuantity,
      selectedSize,
      selectedColor,
      selectedFit,
      itemBrand,
      ledgerSummaryData,
      pageAddType,
      showAddedToBagCta,
      revertAtbButton,
      addedToBagLabel,
      addBtnMsg,
      isStyleWith,
    } = this.props;

    const brandName = itemBrand || getBrand();

    const { subTotal } = ledgerSummaryData;
    const { name: colorName } = selectedColor || {};
    const { name: fitName = '' } = selectedFit || {};
    const { name: sizeName = '' } = selectedSize || {};

    const productData = formatProductsData(currentProduct, {
      selectedQuantity,
      selectedSize,
      selectedColor,
      itemBrand: brandName,
      subTotal: subTotal > 0 ? subTotal : currentProduct.listPrice,
      cartAddType: pageAddType,
      sku: getSkuId(currentProduct.colorFitsSizesMap, colorName, fitName, sizeName),
    });
    const { generalProductId, name } = currentProduct;

    const productId = generalProductId && generalProductId.split('_')[0];
    const productName = name && name.toLowerCase();
    const pageShortName = productId ? `product:${productId}:${productName}` : '';
    const pageName = pageShortName;
    return this.checkaddtobag({
      showAddedToBagCta,
      addBtnMsg,
      addedToBagLabel,
      keepAlive,
      fitChanged,
      displayErrorMessage,
      handleFormSubmit,
      currentProduct,
      productData,
      pageShortName,
      pageName,
      revertAtbButton,
      isStyleWith,
    });
  };

  componentWillUnmount = () => {
    const { clearAddToBagError } = this.props;
    clearAddToBagError();
  };

  categoryIDMapping = async (currentProduct, profile) => {
    const categoryIDMap = await getValueFromAsyncStorage(SBP_CATEGORY_ID_MAPPING);
    const parsedCategoryIDMap = JSON.parse(categoryIDMap);
    const {
      json: { SBPCategoryMapping },
    } = parsedCategoryIDMap;
    try {
      const { gender } = profile;
      const productCatID = this.getProductCategoryID(currentProduct);
      const catIdMapping = SBPCategoryMapping.find(
        (mapObj) =>
          mapObj.department.toLowerCase() === gender.toLowerCase() &&
          mapObj.categoryIds.split(',').some((element) => productCatID.indexOf(element) >= 0)
      );

      if (typeof catIdMapping !== 'undefined') {
        const { category } = catIdMapping;
        const sizesForPreselect = this.extractSizesFromSBPProfile(category, profile);
        const { profileSize, profileFit } = sizesForPreselect;
        return await this.getSizeMapping(
          currentProduct,
          profileSize,
          profileFit,
          category,
          profile
        );
      }
    } catch (error) {
      logger.error(error);
    }
    return null;
  };

  getProductCategoryID = (currentProduct) => {
    const listCategoryIds = [];
    if (currentProduct && currentProduct.categoryPathMap) {
      currentProduct.categoryPathMap.forEach((catMap) => {
        const value = catMap.split('>')[1].split('|')[0];
        listCategoryIds.push(value);
      });
    }
    return listCategoryIds;
  };

  extractSizesFromSBPProfile = (categoryName, profile) => {
    const { size } = profile;
    const parsedSize = JSON.parse(size);
    const sizesForPreselect = {};
    Object.entries(parsedSize).forEach(([key, value]) => {
      if (key.toLowerCase() === categoryName.toLowerCase()) {
        sizesForPreselect.profileSize = value.size;
        sizesForPreselect.profileFit = value.fit ? value.fit : '';
      }
    });
    return sizesForPreselect;
  };

  getSizeMapping = async (currentProduct, sizesForPreselect, profileFit, category, profile) => {
    try {
      const { colorFitsSizesMap, generalProductId } = currentProduct;
      const { gender } = profile;
      let productObj;
      let productFit;
      let sizeForPreselect = '';
      const currentProductColorFitsSizesMap = colorFitsSizesMap.find(
        (element) => element.colorDisplayId === generalProductId
      );
      const sizeMappingSBP = await getValueFromAsyncStorage(SBP_SIZE_MAPPING);
      const parsedSizeMappingSBP = JSON.parse(sizeMappingSBP);
      const profileSize = sizesForPreselect.find((size) => {
        const [fitProduct] = currentProductColorFitsSizesMap.fits;
        const sizeObj = parsedSizeMappingSBP[gender].find(
          (sizeMapObj) =>
            sizeMapObj.size === size && sizeMapObj.category.toLowerCase() === category.toLowerCase()
        );
        const { all_sizes: everySize } = sizeObj;
        return everySize.find((singleSize) =>
          fitProduct.sizes.some((el) => el.sizeName === singleSize && el.maxAvailable > 0)
        );
      });

      const profileSizeObj = parsedSizeMappingSBP[gender].find(
        (element) =>
          element.size === profileSize && element.category.toLowerCase() === category.toLowerCase()
      );

      if (currentProductColorFitsSizesMap.fits.length === 1) {
        const [prodFit] = currentProductColorFitsSizesMap.fits;
        productObj = prodFit;
        productFit = '';
      } else {
        productObj = currentProductColorFitsSizesMap.fits.find(
          (element) => element.fitName.toLowerCase() === profileFit.toLowerCase()
        );
        productFit = profileFit;
      }

      const { all_sizes: allSizes } = profileSizeObj;
      allSizes.forEach((size) => {
        productObj.sizes.forEach(({ sizeName, maxAvailable }) => {
          if (sizeName === size && maxAvailable > 0) sizeForPreselect = sizeName;
        });
      });

      return { sizeForPreselect, productFit };
    } catch (error) {
      logger.error(error);
    }
    return null;
  };

  isProductAnAccessory = (currentProduct) => {
    const { colorFitsSizesMap } = currentProduct;
    const findAccessorySize =
      colorFitsSizesMap &&
      colorFitsSizesMap[0].fits &&
      colorFitsSizesMap[0].fits[0].sizes.find(
        (element) => element.sizeName.toLowerCase() === SBP_ONE_SIZE
      );
    if (findAccessorySize) return `${findAccessorySize.sizeName}`;
    return '';
  };

  getRecsObject = (product, viaModule, recIdentifier, clickOrigin) => {
    if (viaModule === 'mbox_Recommendation') {
      const recObj = {
        recsType: recIdentifier ? `monetate-${recIdentifier}` : '',
        recsPageType: clickOrigin,
        recsProductId: product.colorId,
      };
      setRecommendationsObj(recObj);
      return recObj;
    }
    return {};
  };

  // Return the gender based on breadcrumb
  getGender = (breadcrumbs) => {
    const gender =
      breadcrumbs && breadcrumbs[0] && breadcrumbs[0].displayName
        ? breadcrumbs[0].displayName.toLowerCase()
        : '';
    return gender === 'home' ? '' : gender;
  };

  getAnalyticData = (navigation) => {
    return {
      currentScreen: 'ProductDetail',
      pageData: {
        pageName: constructPageName(navigation),
        pageType: 'product',
        pageSection: 'product',
        pageSubSection: 'product',
      },
    };
  };

  // This is required for reommendations.
  getCatIdForRecommendation = () => {
    const { breadCrumbs } = this.props;
    if (breadCrumbs) {
      const category = breadCrumbs.map((crumb, index) => {
        const { displayName } = crumb;
        const separationChar = index !== breadCrumbs.length - 1 ? ':' : '';
        return displayName + separationChar;
      });
      return category.join('');
    }
    return '';
  };

  // Outfit Analytics click
  formatOutfitProductsData = (products) => {
    return products.map((tile) => {
      return {
        id: tile.id,
        stylitics: 'true',
        uniqueOutfitId: tile.id,
        outfitId: tile.id,
      };
    });
  };

  triggerStylyticsAnalytics = (items) => {
    const { currentProduct, trackClickAction, setClickAnalyticsDataAction } = this.props;

    const { name, ratingsProductId } = currentProduct;
    const nameParam = name && name.toLowerCase();
    const pageName = `product:${ratingsProductId}:${nameParam}`;
    const productsFormatted = this.formatOutfitProductsData(items);
    const mappingName = 'outfitStylytics';
    const module = 'browse';
    const trackPayload = {
      pageType: 'product',
      pageName,
      pageSection: 'product',
      pageSubSection: 'product',
      products: productsFormatted,
      name: 'outfitStylytics',
    };
    if (productsFormatted) {
      setClickAnalyticsDataAction(trackPayload);
      trackClickAction({ name: mappingName, module });
    }
  };

  onChangeColor = (e) => {
    const {
      currentProduct: { colorFitsSizesMap },
    } = this.props;
    const currentColorEntry = getMapSliceForColor(colorFitsSizesMap, e);
    const { colorDisplayId } = currentColorEntry;
    this.setState({
      currentColorEntry,
      partNumberAfterColorChange: colorDisplayId,
    });
  };

  onChangeSize = (color, e, fit) => {
    const {
      currentProduct: { isGiftCard },
    } = this.props;
    if (typeof e !== 'object' && !isGiftCard) {
      this.setState({ currentGiftCardValue: e, size: e, fit });
    }
    if (isGiftCard) {
      this.setState({ currentGiftCardValue: e, size: e, fit });
    }
  };

  productContainer = (props) => {
    const {
      currentProduct,
      plpLabels,
      handleFormSubmit,
      navigation,
      addToBagError,
      handleSubmit,
      alternateSizes,
      toastMessage,
      isKeepAliveEnabled,
      outOfStockLabels,
      sizeChartDetails,
      addBtnMsg,
      multipackProduct,
      multiPackCount,
      multiPackThreshold,
      partIdInfo,
      getAllNewMultiPack,
      isRecommendationAvailable,
      checkForOOSForVariant,
      isBrierleyPromoEnabled,
      pdpLabels,
      getDetails,
      singlePageLoad,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      disableMultiPackTab,
      getDisableSelectedTab,
      formValues,
      istMultiColorNameEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      sbpLabels,
      availableTCPmapNewStyleId,
    } = this.props;
    const colorNameAvailable = formValues && formValues.color;
    const { colorFitsSizesMap, TCPStyleColor } = currentProduct;
    const pdpDataInformation = {
      color: { name: colorNameAvailable },
    };
    const { selectedColorProductId, size, fit } = this.state;
    const colorSlice = getMapSliceForColor(colorFitsSizesMap, pdpDataInformation.color.name);
    const newColorList = istMultiColorNameEnabled ? getMapColorList(TCPStyleColor, colorSlice) : [];
    const isSBP = navigation.getParam('isSBP');
    const profile = navigation.getParam('profile');
    const { TCPStyleType, TCPStyleQTY } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );
    return (
      <ProductAddToBagContainer
        currentProduct={currentProduct}
        plpLabels={plpLabels}
        handleFormSubmit={handleFormSubmit}
        selectedColorProductId={selectedColorProductId}
        errorOnHandleSubmit={addToBagError}
        onChangeColor={this.onChangeColor}
        handleSubmit={handleSubmit}
        onChangeSize={this.onChangeSize}
        scrollToTarget={this.scrollPageToTarget}
        sizeChartLinkVisibility={props.sizeChartLinkVisibility}
        alternateSizes={alternateSizes}
        navigation={navigation}
        toastMessage={toastMessage}
        isKeepAliveEnabled={isKeepAliveEnabled}
        outOfStockLabels={outOfStockLabels}
        sizeChartDetails={sizeChartDetails}
        addBtnMsg={addBtnMsg}
        multipackProduct={multipackProduct}
        multiPackCount={multiPackCount}
        multiPackThreshold={multiPackThreshold}
        partIdInfo={partIdInfo}
        getAllNewMultiPack={getAllNewMultiPack}
        isRecommendationAvailable={isRecommendationAvailable}
        pdpLabels={pdpLabels}
        getDetails={getDetails}
        checkForOOSForVariant={checkForOOSForVariant}
        isBrierleyPromoEnabled={isBrierleyPromoEnabled}
        isPDP
        singlePageLoad={singlePageLoad}
        initialMultipackMapping={initialMultipackMapping}
        setInitialTCPStyleQty={setInitialTCPStyleQty}
        disableMultiPackTab={disableMultiPackTab}
        getDisableSelectedTab={getDisableSelectedTab}
        newColorList={newColorList}
        onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
        isSBP={isSBP}
        childProfile={profile}
        sbpSize={size}
        size={size}
        formValues={formValues}
        dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        sbpFit={fit}
        sbpLabels={sbpLabels}
        availableTCPmapNewStyleId={availableTCPmapNewStyleId}
      />
    );
  };

  onImageClick = () => {
    const { showCarousel } = this.state;
    this.setState({ showCarousel: !showCarousel });
  };

  callbackExpandReviewSection = () => {
    this.setState({ expandReviewFromAnchor: true });
  };

  renderCarousel = (imageUrls, isSBP) => {
    const { showCarousel, activeIndex } = this.state;
    const {
      currentProduct,
      isDynamicBadgeEnabled,
      primaryBrand,
      alternateBrand,
      isPDPCalledFromRecommendation,
    } = this.props;
    const { TCPStyleType, TCPStyleQTY } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );
    if (!showCarousel) return null;
    return (
      <FullScreenImageCarousel
        imageUrls={imageUrls}
        parentActiveIndex={activeIndex}
        onCloseModal={this.onImageClick}
        isSBP={isSBP}
        dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
        primaryBrand={primaryBrand}
        alternateBrand={!isPDPCalledFromRecommendation && alternateBrand}
      />
    );
  };

  callbackIndex = (fullScreenImageIndex) => {
    this.setState({ activeIndex: fullScreenImageIndex });
  };

  renderFulfilmentSection = (keepAlive) => {
    const { currentProduct, outOfStockLabels, availableTCPmapNewStyleId, initialMultipackMapping } =
      this.props;
    const { currentColorEntry } = this.state;
    return (
      currentProduct &&
      currentColorEntry && (
        <ProductPickupContainer
          productInfo={currentProduct}
          formName={`ProductAddToBag-${currentProduct.generalProductId}`}
          miscInfo={currentColorEntry.miscInfo}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          availableTCPmapNewStyleId={availableTCPmapNewStyleId}
          initialMultipackMapping={initialMultipackMapping}
        />
      )
    );
  };

  renderMiddlePromoBanner = (promoBanners) => {
    const { navigation } = this.props;
    if (promoBanners && promoBanners.length) {
      return (
        <PromoMiddleContainer>
          <PromoPDPBanners promos={promoBanners} navigation={navigation} />
        </PromoMiddleContainer>
      );
    }
    return null;
  };

  renderBottomPromoBanner = (promoBanners) => {
    const { navigation } = this.props;
    if (promoBanners && promoBanners.length) {
      return (
        <PromoBottomContainer>
          <PromoPDPBanners promos={promoBanners} navigation={navigation} />
        </PromoBottomContainer>
      );
    }
    return null;
  };

  setShowCompleteTheLook = (value) => {
    if (value) {
      this.setState({ showCompleteTheLook: value });
    }
  };

  scrollToAccordionBottom = (x, y, width, height, pageX, pageY) => {
    scrollToViewBottom({
      width,
      height,
      pageX,
      pageY,
      callBack: this.scrollRef,
      currentScrollValue: this.currentScrollValue,
    });
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height;
  };

  handleScroll = (event) => {
    this.currentScrollValue = event.nativeEvent.contentOffset.y;
  };

  onScrollEndDrag = (event) => {
    if (this.isIOSApp) {
      const { navigation } = this.props;
      const isScrolled =
        event &&
        event.nativeEvent &&
        event.nativeEvent.contentOffset &&
        event.nativeEvent.contentOffset.y > 400;

      navigation.setParams({
        isScrolled,
      });
    }
  };

  scrollPageToTarget = (param) => {
    if (param === 'completeTheLook') {
      this.completeTheLookViewRef.measure((fx, fy, width, height, px, py) => {
        this.scrollRef.scrollTo({ y: py - 100, animated: true });
      });
    } else if (param === 'onRecommendation') {
      this.recommendationSection.measure((fx, fy, width, height, px, py) => {
        this.scrollRef.scrollTo({ y: py, animated: true });
      });
    } else {
      this.ratingViewRef.measure((fx, fy, width, height, px, py) => {
        this.scrollRef.scrollTo({ y: py, animated: true });
      });
    }
  };

  shouldRenderRatingReview = (isGiftCard, isBundleProduct, bazaarVoice) => {
    return !isGiftCard && !isBundleProduct && bazaarVoice;
  };

  renderProductReview = ({ renderRatingReview, productId, bazaarVoice, expanded, giftCard }) => {
    const { expandReviewFromAnchor } = this.state;

    return renderRatingReview ? (
      <ReviewsAndRattingContainer
        ref={(view) => {
          this.ratingViewRef = view;
        }}
        collapsable={false}
      >
        {!giftCard && (
          <ProductReviewsContainer
            reviewsCount={bazaarVoice.totalReviewCount}
            ratingsProductId={productId}
            expanded={expanded || expandReviewFromAnchor}
          />
        )}
      </ReviewsAndRattingContainer>
    ) : null;
  };

  renderRelatedOutfits = (isSBP) => {
    const { navigation, pdpLabels, accessibilityLabels, currentProduct } = this.props;
    const { productId } = currentProduct;
    const { partNumberAfterColorChange } = this.state;
    let productIdForSwatch;
    if (partNumberAfterColorChange && partNumberAfterColorChange !== productId) {
      productIdForSwatch = partNumberAfterColorChange;
    } else {
      productIdForSwatch = productId;
    }
    return (
      <View
        ref={(view) => {
          this.completeTheLookViewRef = view;
        }}
        collapsable={false}
      >
        <RelatedOutfits
          paddings="0 12px 0 12px"
          margins="8px 0 0 0"
          background="#ffffff"
          pdpLabels={pdpLabels}
          navigation={navigation}
          selectedColorProductId={productIdForSwatch}
          setShowCompleteTheLook={this.setShowCompleteTheLook}
          accessibilityLabels={accessibilityLabels}
          triggerStylyticsAnalytics={this.triggerStylyticsAnalytics}
          isSBP={isSBP}
        />
      </View>
    );
  };

  addToBagTextSBP = () => {
    const { plpLabels } = this.props;
    return <Text style={ProductDetailStyle.addToBagText}>{plpLabels.addToBag}</Text>;
  };

  getProductPrices = (showPriceRange) => {
    const { currentProduct } = this.props;
    const { currentColorEntry, size, fit } = this.state;
    return !showPriceRange
      ? getPrices(currentProduct, currentColorEntry.color.name, fit, size)
      : getPricesWithRange(currentProduct, currentColorEntry.color.name, fit, size);
  };

  persistSbpProductIds = async () => {
    const { currentProduct } = this.props;
    try {
      const id = currentProduct.productId;
      let products = await getValueFromAsyncStorage(sbpConstant.PRODUCT_CACHE);
      if (typeof products === 'string') products = JSON.parse(products);
      else products = [];
      const index = products.indexOf(id);
      if (index === -1) {
        products.push(id);
        await setValueInAsyncStorage(sbpConstant.PRODUCT_CACHE, JSON.stringify(products));
      }
    } catch (error) {
      logger.error(error);
    }
  };

  closeSizeDrawerSBP = () => {
    const { formValues } = this.props;
    this.setState({ showSize: false, sbpFormValues: formValues });
  };

  showPreSelectedSize = () => {
    const { size, accessorySize } = this.state;
    const { sbpLabels } = this.props;
    const sizeLabel = showSBPLabels(sbpLabels, 'lbl_sbp_sticky_size_btn_pdp', 'SIZE');
    if (size && !accessorySize) return `${sizeLabel} ${size}`;
    if (accessorySize) return `${sizeLabel} - ${size}`;
    return sizeLabel;
  };

  getEventData = () => {
    const { isOutfitPage, navigation, cartsSessionStatus } = this.props;
    const source = (navigation && navigation.getParam('source')) || '';

    const eventsData = ['scAdd', 'event61'];
    if (cartsSessionStatus) {
      eventsData.push('scOpen');
    }
    if (isOutfitPage) {
      eventsData.push('event77', 'event85');
    }
    if (source === 'Candid') {
      eventsData.push(names.eVarPropEvent.event154);
    }
    return eventsData;
  };

  handleSizeClick = (currentProduct, productData, pageName, pageShortName) => {
    const { size, sbpFormValues, accessorySize } = this.state;
    let isSizeSelected = false;
    const {
      navigation,
      AddToCartError,
      currentProduct: currentProductSBP,
      handleFormSubmit,
      setClickAnalyticsDataAction,
      sbpLabels,
    } = this.props;
    const isSBP = navigation.getParam('isSBP');
    if (size || accessorySize) isSizeSelected = true;
    if (!isSizeSelected) {
      const selectSizeLabel = showSBPLabels(
        sbpLabels,
        'lbl_sbp_select_size_fit_pdp',
        'Please select a size/fit.'
      );
      AddToCartError(selectSizeLabel, currentProductSBP ? currentProductSBP.id : 0);
    } else {
      let formValuesSbp;
      if (sbpFormValues) {
        formValuesSbp = {
          color: sbpFormValues.color,
          size,
          quantity: sbpFormValues.quantity,
          fit: sbpFormValues.fit,
        };
      }
      handleFormSubmit(formValuesSbp, currentProductSBP);
      this.setState((prevState) => ({
        ...prevState,
        showSize: false,
      }));
      if (isSBP) this.persistSbpProductIds();
    }
    setClickAnalyticsDataAction({
      customEvents: this.getEventData(),
      CartAddProductId: currentProduct ? currentProduct.generalProductId : '',
      products: productData,
      linkName: 'cart add',
      pageShortName,
      pageName,
      pageType: 'product',
      pageSection: 'product',
      pageSubSection: 'product',
    });
  };

  getItemColor = () => {
    const { currentProduct, formValues } = this.props;
    const { colorFitsSizesMap } = currentProduct;
    const getFormName = formValues && formValues.color;
    const updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, getFormName);
    return updatedColorEntry && updatedColorEntry.color && updatedColorEntry.color.name;
  };

  stickyButtonContainerStyling = () => {
    const { showSize } = this.state;
    if (showSize)
      return { flexDirection: 'column', height: 'auto', borderWidth: 0, borderRadius: 30 };
    return { flexDirection: 'row', height: 100, borderWidth: 1, borderRadius: 0 };
  };

  renderProductSummary = () => {
    const {
      currentProduct,
      pdpLabels,
      currency,
      currencyExchange,
      outOfStockLabels,
      accessibilityLabels,
      isKeepAliveEnabled,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      isPlcc,
      selectedMultipack,
      isPerUnitPriceEnabled,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      navigation,
      isDynamicBadgeEnabled,
      isNewReDesignEnabled,
      stylisticsProductTabList,
    } = this.props;
    const { productId } = currentProduct;
    const isSBP = navigation.getParam('isSBP');
    const { currentColorEntry, currentGiftCardValue, showCompleteTheLook, size } = this.state;

    const keepAlive = isKeepAliveEnabled && currentColorEntry.miscInfo.keepAlive;
    const showPriceRange = isShowPriceRangeKillSwitch && showPriceRangeForABTest;

    const currentProductPrices = this.getProductPrices(showPriceRange);
    const { TCPMultipackProductMapping, TCPStyleType } = currentProduct;
    const { TCPStyleQTY } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );
    return (
      <ProductSummary
        productData={currentProduct}
        selectedColorProductId={currentColorEntry.colorDisplayId || productId}
        {...currentProductPrices}
        offerPrice={
          currentProduct.isGiftCard
            ? parseInt(currentGiftCardValue, 10)
            : currentProductPrices.offerPrice
        }
        listPrice={
          currentProduct.isGiftCard
            ? parseInt(currentGiftCardValue, 10)
            : currentProductPrices.listPrice
        }
        currencySymbol={currency}
        currencyExchange={currencyExchange}
        isGiftCard={currentProduct.isGiftCard}
        showCompleteTheLook={showCompleteTheLook}
        accessibilityLabels={accessibilityLabels}
        pdpLabels={pdpLabels}
        keepAlive={keepAlive}
        outOfStockLabels={outOfStockLabels}
        scrollToTarget={this.scrollPageToTarget}
        renderRatingReview
        isPlcc={isPlcc}
        parentExpandRatingReview={this.callbackExpandReviewSection}
        showPriceRange={showPriceRange}
        selectedSize={size}
        selectedMultipack={selectedMultipack}
        TCPMultipackProductMapping={TCPMultipackProductMapping}
        TCPStyleType={TCPStyleType}
        TCPStyleQTY={TCPStyleQTY}
        isPerUnitPriceEnabled={isPerUnitPriceEnabled}
        isBrierleyPromoEnabled={isBrierleyPromoEnabled}
        onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
        isSBP={isSBP}
        navigation={navigation}
        isNewReDesignEnabled={isNewReDesignEnabled}
        dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
        stylisticsProductTabList={stylisticsProductTabList}
      />
    );
  };

  renderSBPStickyButtons = (formValues) => {
    const { showSize } = this.state;
    const { navigation } = this.props;
    const stickyButtonContainer = this.stickyButtonContainerStyling();
    const brandId = navigation.getParam('brandId');
    const addToBagGymStyling = brandId === BRAND_ID.GYM ? { borderRadius: 25 } : {};
    return (
      <View style={[ProductDetailStyle.view2, stickyButtonContainer]}>
        {!showSize && (
          <TouchableOpacity
            accessibilityRole="button"
            onPress={() =>
              this.setState({
                showSize: true,
                sbpFormValues: formValues,
              })
            }
            style={[ProductDetailStyle.touchableOpacity2, addToBagGymStyling]}
          >
            <Text style={ProductDetailStyle.text2}>{this.showPreSelectedSize()}</Text>
            {showSize ? (
              <CustomIcon name="chevron-up" size="fs14" style={ProductDetailStyle.customIcon} />
            ) : (
              <CustomIcon name="chevron-down" size="fs14" style={ProductDetailStyle.customIcon} />
            )}
          </TouchableOpacity>
        )}
        {!showSize && this.renderAddToBagButton()}
      </View>
    );
  };

  renderSBPSizeDrawerHeader = () => {
    const { sbpLabels } = this.props;
    const selectSizeLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_select_size_header_drawer',
      'SELECT SIZE'
    );
    return (
      <>
        <TouchableOpacity
          accessibilityRole="button"
          style={ProductDetailStyle.modalBoxTouchable}
          onPress={this.closeSizeDrawerSBP}
        >
          <Image source={closeImage} style={ProductDetailStyle.modalBoxImage} />
        </TouchableOpacity>
        <View style={ProductDetailStyle.modalBoxView} />
        <Text style={ProductDetailStyle.modalBoxText}>{selectSizeLabel}</Text>
      </>
    );
  };

  sbpStylingForBottom = () => {
    const { navigation } = this.props;
    const isSBP = navigation.getParam('isSBP');
    if (isSBP) return <View style={ProductDetailStyle.lazyLoadScrollViewSBP} />;
    return <View style={ProductDetailStyle.lazyLoadScrollView} />;
  };

  renderImageCarousel = () => {
    const {
      currentProduct,
      currentProduct: { colorFitsSizesMap },
      navigation,
      onAddItemToFavorites,
      isLoggedIn,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      isKeepAliveEnabled,
      outOfStockLabels,
      accessibilityLabels,
      isOnModelAbTestPdp,
      defaultWishListFromState,
      checkForOOSForVariant,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      formValues,
      activeWishListProducts,
      activeWishListId,
      isDynamicBadgeEnabled,
      isNewReDesignEnabled,
      primaryBrand,
      alternateBrand,
      isPDPCalledFromRecommendation,
    } = this.props;
    const { TCPStyleType, TCPStyleQTY } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );
    const { currentColorEntry, size } = this.state;
    let imageUrls = [];
    let skuId = null;

    const keepAlive = isKeepAliveEnabled && currentColorEntry.miscInfo.keepAlive;
    const getFormName = formValues && formValues.color;
    const updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, getFormName);
    if (colorFitsSizesMap && currentProduct?.imagesByColor) {
      imageUrls = getImagesToDisplay({
        imagesByColor: currentProduct?.imagesByColor,
        curentColorEntry: !updatedColorEntry ? currentColorEntry : updatedColorEntry,
        isAbTestActive: isOnModelAbTestPdp,
        isFullSet: true,
      });
    }

    if (size) {
      skuId = getMapSliceForSizeSkuID(currentColorEntry, size);
    }
    const isSBP = navigation.getParam('isSBP');

    return (
      <ImageCarousel
        isGiftCard={currentProduct.isGiftCard}
        imageUrls={imageUrls}
        onAddItemToFavorites={onAddItemToFavorites}
        isLoggedIn={isLoggedIn}
        currentProduct={currentProduct}
        onImageClick={this.onImageClick}
        AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
        removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
        currentColorEntry={currentColorEntry}
        keepAlive={keepAlive}
        outOfStockLabels={outOfStockLabels}
        skuId={skuId}
        accessibilityLabels={accessibilityLabels}
        imgConfig={config.IMG_DATA_CLIENT.imgConfig[0]}
        videoConfig={config.VID_DATA.imgConfig[0]}
        formName={PRODUCT_ADD_TO_BAG}
        parentActiveIndex={this.callbackIndex}
        navigation={navigation}
        defaultWishListFromState={defaultWishListFromState}
        isPDPImage
        isBrierleyPromoEnabled={isBrierleyPromoEnabled}
        checkForOOSForVariant={checkForOOSForVariant}
        onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
        deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
        isSBP={isSBP}
        activeWishListProducts={activeWishListProducts}
        activeWishListId={activeWishListId}
        dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
        tcpStyleType={TCPStyleType}
        isNewReDesignEnabled={isNewReDesignEnabled}
        primaryBrand={primaryBrand}
        alternateBrand={!isPDPCalledFromRecommendation && alternateBrand}
      />
    );
  };

  renderRecommendationVariation = ({
    isPdp1andPdp2Enabled,
    colorNameAvailable,
    pdpLabels,
    showPriceRange,
    recommendationAttributes,
    pdpDataInformation,
    isPdpNewRecommendationEnabled,
    deviceTier,
  }) => {
    const { navigation } = this.props;
    const isSBP = navigation.getParam('isSBP');
    return (
      <>
        {isPdp1andPdp2Enabled && colorNameAvailable && (
          <React.Fragment>
            <RecommendationMargin backgroundColor="gray.300" />
            <RecommendationWrapper
              ref={(view) => {
                this.recommendationSection = view;
              }}
            >
              <Recommendations
                isSBP={isSBP}
                headerLabel={pdpLabels.youMayAlsoLike}
                showPriceRange={showPriceRange}
                {...recommendationAttributes}
                sequence="1"
                pdpDataInformation={pdpDataInformation}
                newRecommendationEnabled={isPdpNewRecommendationEnabled}
              />
            </RecommendationWrapper>
            {isPdpNewRecommendationEnabled && <RecommendationMargin backgroundColor="gray.300" />}
            {deviceTier === TIER1 && (
              <RecommendationWrapper
                ref={(view) => {
                  this.recommendationSection = view;
                }}
              >
                <Recommendations
                  isSBP={isSBP}
                  isRecentlyViewed
                  showPriceRange={showPriceRange}
                  {...recommendationAttributes}
                  headerLabel={pdpLabels.recentlyViewed}
                  portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
                  sequence="2"
                  pdpDataInformation={pdpDataInformation}
                  newRecommendationEnabled={isPdpNewRecommendationEnabled}
                />
              </RecommendationWrapper>
            )}
            <RecommendationMargin backgroundColor="gray.300" />
          </React.Fragment>
        )}
      </>
    );
  };

  getFormName = (formValues) => {
    return formValues && formValues.color;
  };

  renderSizeDrawerSBP = () => {
    const {
      currentProduct,
      currentProduct: { colorFitsSizesMap },
      plpLabels,
      handleFormSubmit,
      navigation,
      addToBagError,
      handleSubmit,
      alternateSizes,
      toastMessage,
      isKeepAliveEnabled,
      outOfStockLabels,
      sizeChartDetails,
      sbpSizeClick,
      isDynamicBadgeEnabled,
      formValues,
      availableTCPmapNewStyleId,
    } = this.props;
    const profile = navigation.getParam('profile');
    const isSBP = navigation.getParam('isSBP');
    const { TCPStyleType, TCPStyleQTY } = currentProduct;
    const { selectedColorProductId, size, showSize, sbpFormValues, fit } = this.state;
    const drawerHeight = colorFitsSizesMap[0].hasFits ? { height: '55%' } : { height: '39%' };
    const dynamicBadgeOverlayQty =
      isDynamicBadgeEnabled && TCPStyleType === '0002' ? TCPStyleQTY : 0;
    const sizeChartLinkVisibility = !currentProduct.isGiftCard
      ? SIZE_CHART_LINK_POSITIONS.AFTER_SIZE
      : null;
    return (
      <ModalBox
        animationDuration={400}
        swipeThreshold={10}
        style={[ProductDetailStyle.modalBox, drawerHeight]}
        position="bottom"
        swipeArea={400}
        isOpen={showSize}
        entry="bottom"
        swipeToClose={false}
        onClosed={() => this.setState({ showSize: false })}
        coverScreen
        useNativeDriver
      >
        {this.renderSBPSizeDrawerHeader()}
        <ProductAddToBagContainer
          currentProduct={currentProduct}
          plpLabels={plpLabels}
          handleFormSubmit={handleFormSubmit}
          selectedColorProductId={selectedColorProductId}
          errorOnHandleSubmit={addToBagError}
          onChangeColor={this.onChangeColor}
          handleSubmit={handleSubmit}
          onChangeSize={this.onChangeSize}
          sizeChartLinkVisibility={sizeChartLinkVisibility}
          alternateSizes={alternateSizes}
          navigation={navigation}
          toastMessage={toastMessage}
          isKeepAliveEnabled={isKeepAliveEnabled}
          outOfStockLabels={outOfStockLabels}
          sizeChartDetails={sizeChartDetails}
          isPDP
          showColorChips={false}
          showAddToBagCTA={false}
          hideQuatity
          childProfile={profile}
          sbpSize={size}
          isSBPEnabled={isSBP}
          sbpSizeClick={sbpSizeClick}
          sbpFormValues={sbpFormValues}
          dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
          sbpFit={fit}
          formValues={formValues}
          availableTCPmapNewStyleId={availableTCPmapNewStyleId}
        />
        {this.renderAddToBagButton()}
      </ModalBox>
    );
  };

  // eslint-disable-next-line complexity, sonarjs/cognitive-complexity
  render() {
    const {
      currentProduct,
      currentProduct: { colorFitsSizesMap },
      currentProduct: { bazaarVoice },
      navigation,
      shortDescription,
      itemPartNumber,
      longDescription,
      pdpLabels,
      isKeepAliveEnabled,
      middlePromos,
      bottomPromos,
      belowrecsPromos,
      accessibilityLabels,
      isOnModelAbTestPdp,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      checkForOOSForVariant,
      isPdpNewRecommendationEnabled,
      isBrierleyPromoEnabled,
      isOosPdpEnabled,
      isPdp1andPdp2Enabled,
      onSetLastDeletedItemIdAction,
      defaultWishListFromState,
      deleteFavItemInProgressFlag,
      formValues,
      isDynamicBadgeEnabled,
    } = this.props;
    const isSBP = navigation.getParam('isSBP');
    const actionId = navigation.getParam('actionId');
    const viaModule = navigation.getParam('viaModule');
    const { deviceTier } = navigation.getScreenProps();

    const { currentColorEntry, expanded } = this.state;
    let imageUrls = [];
    const { productId, TCPStyleType, TCPStyleQTY, productFamily } = currentProduct;
    const colorNameAvailable = this.getFormName(formValues);
    const updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, colorNameAvailable);
    const itemColor = this.getItemColor();
    const pdpDataInformation = {
      TCPStyleQTY,
      TCPStyleType,
      color: { name: colorNameAvailable },
      productFamily,
    };
    if (colorFitsSizesMap && currentProduct?.imagesByColor) {
      imageUrls = getImagesToDisplay({
        imagesByColor: currentProduct?.imagesByColor,
        curentColorEntry: !updatedColorEntry ? currentColorEntry : updatedColorEntry,
        isAbTestActive: isOnModelAbTestPdp,
        isFullSet: true,
      });
    }
    const sizeChartLinkVisibility = !currentProduct.isGiftCard
      ? SIZE_CHART_LINK_POSITIONS.AFTER_SIZE
      : null;
    const categoryId = this.getCatIdForRecommendation();
    const recommendationAttributes = {
      variation: 'moduleO',
      navigation,
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP,
      partNumber: productId,
      isHeaderAccordion: true,
      categoryName: categoryId,
    };
    const recommendationOosAttributes = {
      variation: 'moduleO',
      navigation,
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP,
      partNumber: productId,
      isHeaderAccordion: false,
      categoryName: categoryId,
      outOfStock: checkForOOSForVariant,
    };
    if (viaModule === 'mbox_Recommendation') {
      recommendationAttributes.actionId = actionId;
      recommendationAttributes.isRecProduct = true;
      recommendationOosAttributes.actionId = actionId;
      recommendationOosAttributes.isRecProduct = true;
    }
    const keepAlive = isKeepAliveEnabled && currentColorEntry.miscInfo.keepAlive;
    const showPriceRange = isShowPriceRangeKillSwitch && showPriceRangeForABTest;
    const renderRatingReview = this.shouldRenderRatingReview(
      currentProduct.isGiftCard,
      false,
      bazaarVoice
    );
    // const candidConfig = getAPIConfig();
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );
    return (
      <View>
        {!!this.isIOSApp && (
          <GlobalModals navigation={navigation} onInitialModalsActionComplete={() => {}} />
        )}
        <LazyloadScrollView
          onScroll={this.handleScroll}
          onMomentumScrollEnd={this.onScrollEndDrag}
          onScrollEndDrag={this.onScrollEndDrag}
          name={LAZYLOAD_HOST_NAME.PDP}
          ref={(ref) => {
            this.scrollRef = ref;
          }}
          keyboardShouldPersistTaps="handled"
          alwaysBounceVertical={false}
        >
          <PageContainer isTransparent={this.isTransparentHeader}>
            {checkForOOSForVariant && isOosPdpEnabled && (
              <Margin backgroundColor="gray.300" margins="0 0 0 0" paddings="24px 0 0px 0">
                <OosText>This item is out of stock</OosText>
                <RecommendationOosWrapper
                  ref={(view) => {
                    this.recommendationSection = view;
                  }}
                >
                  <Recommendations
                    isSBP={isSBP}
                    headerLabel={pdpLabels.checkoutSimilarStyle}
                    portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.OUT_OF_STOCK_PDP}
                    {...recommendationOosAttributes}
                    sequence="1"
                    pdpDataInformation={pdpDataInformation}
                    isNavL2
                    priceOnly
                    paddings="0px 6px 0px 6px"
                    outOfStock
                  />
                </RecommendationOosWrapper>
              </Margin>
            )}
            <Margin margins="16px 12px 0 12px" paddings="24px 0">
              {this.renderImageCarousel()}
              {this.renderProductSummary()}
            </Margin>
            {this.renderMiddlePromoBanner(middlePromos)}
            <Margin
              onLayout={(event) => {
                const { layout } = event.nativeEvent;
                this.addToBagButtonPosition = layout.y;
              }}
            >
              {this.productContainer({ sizeChartLinkVisibility })}
              {currentProduct.isGiftCard ? <SendAnEmailGiftCard pdpLabels={pdpLabels} /> : null}
              {!isSBP && this.renderFulfilmentSection(keepAlive)}
              {this.renderCarousel(imageUrls, isSBP)}
              {!isSBP && (
                <LoyaltyBannerView>
                  <LoyaltyBanner pageCategory="isProductDetailView" navigation={navigation} />
                </LoyaltyBannerView>
              )}
            </Margin>

            {this.renderBottomPromoBanner(bottomPromos)}
            <Margin backgroundColor="gray.300" margins="0 0 0 0" paddings="8px  0 0 0">
              {!currentProduct.isGiftCard ? (
                <>
                  <ProductDetailDescription
                    isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                    shortDescription={shortDescription}
                    itemPartNumber={itemPartNumber}
                    longDescription={longDescription}
                    isShowMore={false}
                    pdpLabels={pdpLabels}
                    scrollToAccordionBottom={this.scrollToAccordionBottom}
                    accessibilityLabels={accessibilityLabels}
                    backgroundColor="white"
                    paddings="18px 12px 2px 12px"
                    onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                    defaultWishListFromState={defaultWishListFromState}
                    color={itemColor}
                    productInfo={currentProduct}
                    deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                    dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
                  />

                  {this.renderRelatedOutfits(isSBP)}
                </>
              ) : null}
              {this.renderRecommendationVariation({
                isPdp1andPdp2Enabled,
                colorNameAvailable,
                pdpLabels,
                showPriceRange,
                recommendationAttributes,
                pdpDataInformation,
                isPdpNewRecommendationEnabled,
                deviceTier,
              })}
              {this.renderBottomPromoBanner(belowrecsPromos)}
              {/* {this.canDid({
                candidConfig,
                navigation,
                giftCard: currentProduct.isGiftCard,
              })} */}

              {this.renderProductReview({
                renderRatingReview,
                productId: currentProduct.ratingsProductId,
                bazaarVoice,
                expanded,
                giftCard: currentProduct.isGiftCard,
              })}
            </Margin>
          </PageContainer>
          {isSBP && this.sbpStylingForBottom()}
        </LazyloadScrollView>
        {isSBP && this.renderSizeDrawerSBP()}
        {isSBP && this.renderSBPStickyButtons(formValues)}
      </View>
    );
  }
}

ProductDetailOld.propTypes = ProductDetailOldPropTypes;

ProductDetailOld.defaultProps = {
  currentProduct: {
    colorFitsSizesMap: {},
    offerPrice: '',
    listPrice: '',
    generalProductId: '',
    imagesByColor: {},
    isGiftCard: false,
  },
  navigation: {},
  plpLabels: null,
  handleSubmit: null,
  isPickupModalOpen: false,
  handleFormSubmit: null,
  addToBagError: '',
  shortDescription: '',
  itemPartNumber: '',
  longDescription: '',
  pdpLabels: {},
  currency: 'USD',
  currencyExchange: 1,
  onAddItemToFavorites: null,
  isLoggedIn: false,
  alternateSizes: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  toastMessage: () => {},
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  bottomPromos: '',
  middlePromos: '',
  accessibilityLabels: {},
  trackViewAction: () => {},
  setClickAnalyticsDataAction: () => {},
  isOnModelAbTestPdp: false,
  setExternalCampaignFired: () => {},
  isPdpNewRecommendationEnabled: false,
  isBrierleyPromoEnabled: false,
  isOosPdpEnabled: false,
  isPdp1andPdp2Enabled: true,
  disableMultiPackTab: false,
  defaultWishListFromState: {},
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  AddToCartError: () => {},
  availableTCPmapNewStyleId: [],
  belowrecsPromos: '',
};

export default withStyles(ProductDetailOld);

export { ProductDetailOld as ProductDetailOldVanilla };
