// 9fbef606107a605d69c0edbcd8029e5d
import { breakpoints } from '@tcp/core/styles/themes/TCP/mediaQuery';

const PDPCarouselOptions = {
  CAROUSEL_OPTIONS: {
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 6,
    slide: true,
    touchMove: true,
    touchThreshold: 100,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: breakpoints.values.lg - 1,
        settings: {
          arrows: false,
          slidesToShow: 5,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: breakpoints.values.sm - 1,
        settings: {
          arrows: false,
          slidesToShow: 2.8,
          slidesToScroll: 1,
        },
      },
    ],
  },
};

export default {
  PDPCarouselOptions,
};
