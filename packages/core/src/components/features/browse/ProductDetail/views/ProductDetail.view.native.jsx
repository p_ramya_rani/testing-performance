// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../common/hoc/withStyles.native';
import ProductDetailNew from './ProductDetailNew.view.native';

class ProductDetailView extends React.Component {
  // on app candidate module will not be display [RWD-19103]
  isShowCandidateModule = false;

  render() {
    const { isNewReDesignEnabled, navigation } = this.props;
    const viaModule = navigation.getParam('viaModule');
    const isPDPCalledFromRecommendation = viaModule === 'mbox_Recommendation';
    return (
      <ProductDetailNew
        {...this.props}
        isNewReDesignEnabled={isNewReDesignEnabled}
        isPDPCalledFromRecommendation={isPDPCalledFromRecommendation}
      />
    );
  }
}

ProductDetailView.propTypes = {
  isNewReDesignEnabled: PropTypes.bool,
  navigation: PropTypes.shape({}),
};

ProductDetailView.defaultProps = {
  isNewReDesignEnabled: false,
  navigation: {},
};

export default withStyles(ProductDetailView);

export { ProductDetailView as ProductDetailViewVanilla };
