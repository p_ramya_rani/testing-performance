import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { PropTypes } from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import FastImage from '@stevenmasini/react-native-fast-image';
import {
  colorWhiteSmoke,
  colorWhite,
} from '../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.styles';
import { MainView, RowView, UnderLine, ViewWrap } from '../styles/PLCCBanner.style.native';
import ClickTracker from '../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import names from '../../../../../constants/eventsName.constants';

const plccCard = require('../../../../../../../mobileapp/src/assets/images/elements-plcc-card.png');

const PLCCBanner = ({
  pdpLabels,
  navigation,
  openApplyNowModal,
  backGroundColor,
  isFromCollection,
}) => {
  const {
    myLabel,
    placeLabel,
    save30,
    learnMoreLabel,
    creditCardLabel,
    plccDiscription,
    rewardsLabel,
    plccMoreDiscription,
  } = pdpLabels;
  const imageStyle = {
    height: 26,
    width: 36,
    marginTop: 3,
  };

  const toggleApplyNowModal = () => {
    navigation.navigate('ApplyNow');
    openApplyNowModal({ isModalOpen: true });
  };

  return (
    <View>
      <MainView
        backGroundColor={backGroundColor ? colorWhiteSmoke : colorWhite}
        isFromCollection={isFromCollection}
      >
        <FastImage
          source={plccCard}
          dataLocator="plcc_card_icon"
          style={imageStyle}
          resizeMode={FastImage?.resizeMode?.contain}
        />
        <View>
          <RowView>
            <BodyCopy
              fontFamily="secondary"
              fontWeight="bold"
              fontSize="fs12"
              text={save30}
              textAlign="left"
              margin="0 0 0 10px"
            />

            <BodyCopy
              dataLocator="plcc_banner_discription"
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="regular"
              color="black"
              text={plccDiscription}
              margin="0 0 0 2px"
            />
          </RowView>
          <RowView marginTop={3}>
            <BodyCopy
              dataLocator="plcc_banner_discription"
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="regular"
              color="black"
              text={plccMoreDiscription}
              margin="0 0 0 10px"
            />

            <BodyCopy
              dataLocator="plcc_banner_myLabel"
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="extrabold"
              color="orange.1000"
              text={myLabel}
              textAlign="left"
              margin="0 0 0 3px"
              lineHeight={16}
              letterSpacing={0.2}
            />

            <BodyCopy
              dataLocator="plcc_banner_placeLabel"
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="extrabold"
              color="blue.1100"
              text={placeLabel}
              textAlign="left"
              margin="0 0 0 3px"
              lineHeight={16}
              letterSpacing={0.2}
            />

            <BodyCopy
              dataLocator="plcc_banner_rewardsLabel"
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="extrabold"
              color="pink.600"
              text={rewardsLabel}
              textAlign="left"
              margin="0 0 0 4px"
              lineHeight={16}
              letterSpacing={0.2}
            />
            <BodyCopy
              dataLocator="plcc_banner_creditCardLabel"
              fontSize="fs12"
              fontFamily="secondary"
              fontWeight="regular"
              color="black"
              margin="0 0 0 4px"
              text={creditCardLabel}
              lineHeight={16}
              letterSpacing={0.2}
            />
          </RowView>
          <ViewWrap marginTop={3}>
            <ClickTracker
              as={TouchableOpacity}
              name={names.screenNames.learn_more_plcc_banner_click_pdp}
              clickData={{
                customEvents: ['event134'],
              }}
              module="PDP"
              onPress={toggleApplyNowModal}
              accessibilityLabel={learnMoreLabel}
              accessibilityRole="button"
            >
              <BodyCopy
                dataLocator="plcc_banner_learnMoreLabel"
                fontSize="fs12"
                fontFamily="secondary"
                fontWeight="regular"
                color="blue.800"
                margin="0 0 0 10px"
                text={learnMoreLabel}
                lineHeight={16}
                letterSpacing={0.2}
              />
              <UnderLine />
            </ClickTracker>
          </ViewWrap>
        </View>
      </MainView>
    </View>
  );
};

PLCCBanner.propTypes = {
  pdpLabels: PropTypes.shape({
    myLabel: PropTypes.string,
    placeLabel: PropTypes.string,
    save30: PropTypes.string,
    learnMoreLabel: PropTypes.string,
    creditCardLabel: PropTypes.string,
    plccDiscription: PropTypes.string,
    rewardsLabel: PropTypes.string,
    plccMoreDiscription: PropTypes.string,
  }),
  navigation: PropTypes.func.isRequired,
  openApplyNowModal: PropTypes.func.isRequired,
  isFromCollection: PropTypes.bool,
};

PLCCBanner.defaultProps = {
  pdpLabels: {},
  isFromCollection: false,
};
export default PLCCBanner;
