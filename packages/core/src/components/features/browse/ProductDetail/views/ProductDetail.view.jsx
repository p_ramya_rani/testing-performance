/* eslint-disable max-lines */
/* eslint-disable extra-rules/no-commented-out-code */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import dynamic from 'next/dynamic';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import {
  isClient,
  getIcidValue,
  createBVUrl,
  isUsOnly,
  getBrand,
  fireEnhancedEcomm,
  getViewportInfo,
} from '@tcp/core/src/utils';
import { getMonetateEnabledFlag } from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import {
  getSessionStorage,
  setSessionStorage,
  DIMENSION92_TEXT,
  getDimension2Value,
  getGAPageName,
} from '@tcp/core/src/utils/utils.web';
import { setLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { getParam } from '@tcp/core/src/analytics/utils';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import Recommendations from '@tcp/web/src/components/common/molecules/Recommendations';
import { DynamicModule } from '@tcp/core/src/components/common/atoms';
import smoothscroll from 'smoothscroll-polyfill';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary/errorBoundary';
import { isTier1Device, isTier3Device } from '@tcp/web/src/utils/device-tiering';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import { Row, Col, BodyCopy, Image } from '../../../../common/atoms';
import withStyles from '../../../../common/hoc/withStyles';
import ProductDetailStyle, {
  customSubmitButtonStyle,
  recommendationStylesPdp,
  recommendationStylesPdp1AndPdp2,
} from '../ProductDetail.style';
import { PRODUCT_INFO_PROP_TYPE_SHAPE } from '../../ProductListing/molecules/ProductList/propTypes/productsAndItemsPropTypes';
import Product from '../molecules/Product/views/Product.view';
import FixedBreadCrumbs from '../../ProductListing/molecules/FixedBreadCrumbs/views';
import ProductAddToBagContainer from '../../../../common/molecules/ProductAddToBag';
import SIZE_CHART_LINK_POSITIONS from '../../../../common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import ProductPickupContainer from '../../../../common/organisms/ProductPickup';
import names from '../../../../../constants/eventsName.constants';

import {
  routerPush,
  getIconPath,
  isValidObj,
  parseBoolean,
  getAPIConfig,
} from '../../../../../utils';
import { requireUrlScript } from '../../../../../utils/resourceLoader';
import ProductDescription from '../molecules/ProductDescription/views';
import LoyaltyBanner from '../../../CnC/LoyaltyBanner';
import ProductPrice from '../molecules/ProductPrice/ProductPrice';
import ProductImagesWrapper from '../molecules/ProductImagesWrapper/views/ProductImagesWrapper.view';
import {
  getImagesToDisplay,
  getPrimaryImages,
  getMapSliceForColorProductId,
  getMapSliceForColor,
  getFeatures,
  getMapColorList,
  getMapSliceForSizeSkuID,
} from '../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import ProductReviewsContainer from '../../ProductListing/molecules/ProductReviews/container/index.async';
import SendAnEmailGiftCard from '../molecules/SendAnEmailGiftCard';
import ProductDetailsUtils from './ProductDetail.utils';
import FamilyOutfit from '../molecules/FamilyOutfit/FamilyOutfit';
import PdpVmpDataLogger from './PdpVmpDataLogger.view';

const RelatedOutfits = dynamic(() =>
  import(/* webpackChunkName: "relatedOutfits" */ '../molecules/RelatedOutfits/views')
);
const PromoPDPBanners = dynamic(() =>
  import(/* webpackChunkName: "promoPDPBanners" */ '../../../../common/organisms/PromoPDPBanners')
);

const { RECOMMENDATION } = Constants;

class ProductDetailView extends React.PureComponent {
  constructor(props) {
    super(props);
    const {
      productInfo: { colorFitsSizesMap, generalProductId, offerPrice },
      alternateBrand,
    } = this.props;
    this.state = {
      currentColorEntry:
        getMapSliceForColorProductId(
          colorFitsSizesMap,
          generalProductId?.replace(`$brand=${alternateBrand}`, '')
        ) || {},
      currentGiftCardValue: offerPrice,
      renderReceiveProps: false,
      showBazaarVoice: false,
      starRatingClicked: false,
    };
    const { currentColorEntry } = this.state;
    const colorName = currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;

    this.formValues = {
      Fit: '',
      Size: '',
      color: colorName,
      Quantity: 1,
    };
    this.analyticsTriggered = false;
    this.imageSwatchesToDisplayHolder = [];
    this.newColorListHolder = [];
    this.apiConfig = getAPIConfig();
    this.isLowTier = isTier3Device();
  }

  static getDerivedStateFromProps(props, state) {
    const {
      productInfo: { colorFitsSizesMap },
    } = props;

    return {
      ...state,
      currentColorEntry:
        getMapSliceForColorProductId(colorFitsSizesMap, state.currentColorEntry.colorProductId) ||
        {},
    };
  }

  componentDidMount() {
    if (window.BV) {
      this.setState({ showBazaarVoice: true });
    } else if (!this.isLowTier) {
      requireUrlScript(createBVUrl()).then(() => {
        this.setState({ showBazaarVoice: true });
      });
    }
    // kick off the polyfill for smooth scroll safari
    smoothscroll.polyfill();
    // Removing the previous value to handle the view more on backbutton
    setLocalStorage({ key: 'pdp1', value: 0 });
    setLocalStorage({ key: 'pdp2', value: 0 });

    window.onpopstate = this.onBackButton;

    const {
      productDetails,
      checkForOOSForVariant,
      isPlcc,
      getSocialProofData,
      productInfo,
      formValues,
    } = this.props;

    const { TCPStyleQTY, TCPStyleType, productFamily } = productInfo;
    const colorNameAvailable = formValues && formValues.color;
    const { currentProduct } = productDetails;
    const { productId } = currentProduct;
    const pdpDataInformation = {
      TCPStyleQTY,
      TCPStyleType,
      color: { name: colorNameAvailable },
      productFamily,
    };
    const skuData = pdpDataInformation;
    const categoryId = this.getCatIdForRecommendation();
    const MONETATE_ENABLED = getMonetateEnabledFlag();
    const action = {
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP,
      ...(productId && { itemPartNumber: productId }),
      categoryName: categoryId || '',
      excludedIds: '',
      provider: 'Monetate',
      reduxKey: Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP,
      MONETATE_ENABLED,
      skuData,
      styleWithEligibility: false,
      isPlcc,
      outOfStock: checkForOOSForVariant,
      isRecProduct: false,
    };

    action.reduxKey = action.page;
    const { isMobile } = getViewportInfo();

    if (isMobile) {
      window.addEventListener('scroll', this.handleStickyImage);
    }

    return window.addEventListener('load', getSocialProofData(action));
  }
  /*
  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(this.props, nextProps) && isEqual(this.state, nextState)) return false;
    return true;
  } */

  componentDidUpdate(prevProps) {
    const { isLoading } = this.props;

    const { isLoading: prevIsLoading } = prevProps;
    /*
       Triggering Page load tracking on componentDidUpdate because we need to wait for the listPrice and
       it is triggered only in client side.
     */
    if (prevIsLoading !== isLoading && !isLoading) {
      this.triggerAnalytics();
      this.willMonetateCarouselDisplay = isTier1Device();
    }
  }

  onBackButton = () => {
    const { router } = this.props;
    const { route } = router;
    if (
      route &&
      route.includes('ProductDetail') &&
      getSessionStorage('LAST_PAGE_PATH') === window.location.pathname
    ) {
      setSessionStorage({ key: 'pdp_back', value: true });
    }
  };

  componentWillUnmount() {
    const { isMobile } = getViewportInfo();

    if (isMobile) {
      window.removeEventListener('scroll', this.handleStickyImage);
    }
  }

  handleStickyImage = () => {
    const stickyElem = document.getElementsByClassName('sticky-mobile-image-wrapper')[0];
    if (stickyElem) {
      const { top } = stickyElem.getBoundingClientRect();
      if (top && top <= 26) {
        stickyElem.style.zIndex = -1;
      } else {
        stickyElem.style.zIndex = '';
      }
    }
  };

  // eslint-disable-next-line complexity
  triggerAnalytics = () => {
    const {
      productInfo,
      trackPageLoad,
      router,
      router: {
        query: { isSearchPage, viaModule, viaPage, imageSearch, recommendationCarousel },
      },
      navigateType,
      storeId,
      currency,
    } = this.props;

    const icid = getIcidValue(router.asPath);
    const isSearchPageFlag = isSearchPage === 'true';
    const { currentColorEntry } = this.state;
    const selectedColorProductId = currentColorEntry && currentColorEntry.colorProductId;
    const productsFormatted = this.formatProductsData(
      productInfo,
      isSearchPageFlag,
      selectedColorProductId,
      viaPage,
      viaModule
    );

    const formatProductDataForEE = this.formatProductsDataForEE(
      productInfo,
      isSearchPageFlag,
      selectedColorProductId,
      viaPage,
      viaModule,
      currency,
      recommendationCarousel
    );

    const { name, ratingsProductId } = productInfo;
    const nameParam = name && name.toLowerCase();
    const pageName = `product:${ratingsProductId}:${nameParam}`;
    let customEvents = ['prodView', 'event1', 'event80', 'event93'];
    const { generalProductId } = productInfo;
    const productId = generalProductId ? generalProductId.split('_')[0] : '';

    let additionalData = {};

    if (isSearchPageFlag) {
      customEvents.push('event83');
    } else if (imageSearch) {
      customEvents = ['prodView', 'event1', 'event80'];
    } else if (viaModule === RECOMMENDATION) {
      customEvents = ['prodView', 'event1', 'event80', names.eVarPropEvent.event50];
      additionalData = {
        internalCampaignId: '',
        pageSection: '',
        searchTerm: '',
        searchType: '',
        storeId,
      };
    } else if (icid) {
      additionalData.internalCampaignId = icid;
      additionalData.pageNavigationText = '';
    }

    if (navigateType === 'direct') {
      customEvents = ['prodView', 'event1', 'event80', 'event93', 'event100', 'event20'];
    }

    const trackPayload = {
      pageType: 'product',
      pageName,
      pageSection: 'product',
      pageSubSection: 'product',
      products: productsFormatted,
      customEvents,
      productId,
      ...additionalData,
    };

    if (isSearchPageFlag) {
      trackPayload.pageNavigationText = '';
    }

    if (productsFormatted) {
      trackPageLoad(trackPayload);
    }

    if (formatProductDataForEE) {
      fireEnhancedEcomm({
        eventName: 'productDetail',
        productsObj: formatProductDataForEE,
        eventType: 'detail',
        pageName: getGAPageName(),
        currCode: currency,
      });
    }
  };

  getCD92 = (recommendationCarousel, recsObj) => {
    let cd92 = recommendationCarousel ? `${DIMENSION92_TEXT}${recommendationCarousel}` : null;

    if (!cd92 && recsObj && recsObj.dimension92) {
      cd92 = recsObj.dimension92;
    }
    return cd92;
  };

  // eslint-disable-next-line complexity
  formatProductsDataForEE = (
    product,
    isSearchPage,
    selectedColorProductId,
    viaPage,
    viaModule,
    currency,
    recommendationCarousel
  ) => {
    const productData = [];
    const { isGiftCard, colorFitsSizesMap } = product;
    const { breadCrumbs } = this.props;
    const features = getFeatures(colorFitsSizesMap, selectedColorProductId, isGiftCard);
    const recsObj = this.getRecsObject(product, viaModule, viaPage, false, recommendationCarousel);
    const color =
      (product &&
        product.colorFitsSizesMap &&
        product.colorFitsSizesMap
          .filter((productTile) => {
            return productTile.colorProductId === selectedColorProductId;
          })
          .map((productTile) => productTile.color.name)
          .join('')) ||
      '';
    const prodBrand = getBrand().toUpperCase();
    let type = '';
    const formattedCategory =
      breadCrumbs &&
      breadCrumbs.map((listing) => listing.displayName && listing.displayName.toLowerCase());
    if (formattedCategory.length && formattedCategory.includes('accessories')) {
      type = 'accessories';
    } else if (formattedCategory.includes('shoes')) {
      type = 'shoes';
    } else {
      type = 'apparel';
    }

    const cd92 = this.getCD92(recommendationCarousel, recsObj);

    productData.push({
      id: product.generalProductId,
      variant: color,
      category: type,
      brand: prodBrand,
      price: product.listPrice,
      metric1: '1',
      metric93: '1',
      dimension35: prodBrand,
      dimension89: product.generalProductId,
      dimension77: color,
      dimension72: features,
      dimension67: product.offerPrice < product.listPrice ? 'on sale' : 'full price',
      dimension63: `${product.offerPrice}${currency} ${product.listPrice}${currency}`,
      dimension2: getDimension2Value(cd92) || 'browse',
      ...recsObj,
    });
    return productData;
  };

  handleRatingClick = (value) => {
    this.setState({ starRatingClicked: value });
  };

  formatProductsData = (product, isSearchPage, selectedColorProductId, viaPage, viaModule) => {
    const productData = [];
    const color =
      (product &&
        product.colorFitsSizesMap &&
        product.colorFitsSizesMap
          .filter((productTile) => {
            return productTile.colorProductId === selectedColorProductId;
          })
          .map((productTile) => productTile.color.name)
          .join('')) ||
      '';
    const { isGiftCard, colorFitsSizesMap } = product;
    const selectedColorElement = this.getSelectedColorData(
      colorFitsSizesMap,
      selectedColorProductId
    );
    const hasFits =
      selectedColorElement && selectedColorElement[0] && selectedColorElement[0].hasFits;

    let features = '';

    if (!isGiftCard) {
      features += 'alt images full size image';
    }
    if (hasFits) {
      features += `${features} size chart`;
    }

    const productId = product.generalProductId.split('_')[0];
    productData.push({
      colorId: product.generalProductId,
      color,
      id: productId,
      price: product.listPrice,
      extPrice: product.listPrice,
      listPrice: product.listPrice,
      pricingState: product.offerPrice < product.listPrice ? 'on sale' : 'full price',
      rating: product.ratings,
      reviews: product.reviewsCount,
      plpClick: (viaModule !== RECOMMENDATION).toString(),
      searchClick: isSearchPage,
      features,
      ...this.getRecsObject(product, viaModule, viaPage, true, null),
    });
    return productData;
  };

  formatOutfitProductsData = (products) => {
    return products.map((tile) => {
      return {
        id: tile.id,
        stylitics: 'true',
        uniqueOutfitId: tile.id,
        outfitId: tile.id,
      };
    });
  };

  triggerStylyticsAnalytics = (items) => {
    const { productInfo, trackClick } = this.props;
    const { name, ratingsProductId } = productInfo;
    const nameParam = name && name.toLowerCase();
    const pageName = `product:${ratingsProductId}:${nameParam}`;
    const productsFormatted = this.formatOutfitProductsData(items);
    const trackPayload = {
      pageType: 'product',
      pageName,
      pageSection: 'product',
      pageSubSection: 'product',
      products: productsFormatted,
      customEvents: ['event74', 'event76', 'event95'],
      pageNavigationText: '',
    };
    if (!this.analyticsTriggered && productsFormatted) {
      trackClick(trackPayload);
      this.analyticsTriggered = true;
    }
  };

  get404PageType = (recIdenFor404Page) => {
    const brand = getBrand();
    if (recIdenFor404Page) return `${brand}_404_page`;
    return '';
  };

  getRecsObject = (product, viaModule, viaPage, isAdobe, recommendationCarousel) => {
    const { pdpLabels, router } = this.props;
    let recIdenFrom404Page = '';
    if (router.asPath.split('?').length > 0)
      recIdenFrom404Page = getParam('recommendationSource', router.asPath.split('?')[1]);
    const page404Type = this.get404PageType(recIdenFrom404Page);
    if (viaModule === RECOMMENDATION || recIdenFrom404Page) {
      if (isAdobe) {
        return {
          recsType: pdpLabels.youMayAlsoLike,
          recsPageType: recIdenFrom404Page ? page404Type : viaPage,
          recsProductId: product.unbxdProdId,
        };
      }

      const cd92 = `${DIMENSION92_TEXT}${recIdenFrom404Page || recommendationCarousel || viaPage}`;
      return {
        dimension20: product.unbxdProdId,
        dimension21: recIdenFrom404Page ? page404Type : viaPage,
        dimension92: cd92,
        metric50: '1',
      };
    }
    return null;
  };

  getSelectedColorData = (colorFitsSizesMap, colorId) => {
    return (
      colorFitsSizesMap &&
      colorFitsSizesMap.filter((colorItem) => {
        return colorItem.colorProductId === colorId;
      })
    );
  };

  onChangeColor = (e, selectedSize, selectedFit, selectedQuantity) => {
    const {
      productInfo: { colorFitsSizesMap },
    } = this.props;
    const { currentGiftCardValue } = this.state;
    this.setState({
      currentColorEntry: getMapSliceForColor(colorFitsSizesMap, e),
      renderReceiveProps: true,
      currentGiftCardValue:
        (getMapSliceForColor(colorFitsSizesMap, e) &&
          getMapSliceForColor(colorFitsSizesMap, e).offerPrice) ||
        currentGiftCardValue,
    });
    this.formValues = {
      Fit: selectedFit,
      Size: selectedSize,
      color: e,
      Quantity: selectedQuantity,
    };
  };

  onChangeSize = (selectedColor, e, selectedFit, selectedQuantity) => {
    this.setState({ currentGiftCardValue: e });
    this.formValues = {
      Fit: selectedFit,
      Size: e,
      color: selectedColor,
      Quantity: selectedQuantity,
    };
  };

  updateQuantityChange = (selectedQuantity) => {
    this.formValues = {
      ...this.formValues,
      Quantity: selectedQuantity,
    };
  };

  onGoBack = (e) => {
    const { isDefaultBreadCrumb } = this.props;
    if (isDefaultBreadCrumb) {
      e.preventDefault();
      routerPush('/', '/home');
    }
    this.triggerBack(e);
  };

  triggerBack = (e) => {
    e.preventDefault();
    if (window.history.length > 2) window.history.back();
  };

  checkInWishlist = (wishList, productId) =>
    wishList && wishList[productId] && wishList[productId].isInDefaultWishlist;

  getProductSummary = (keepAlive, handleMetaProps, isInWishList) => {
    const {
      productDetails,
      productInfo,
      currency,
      pdpLabels,
      currencyAttributes,
      onAddItemToFavorites,
      isLoggedIn,
      outOfStockLabels,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      isLoading,
      wishList,
      hidePriceDisplay,
      isPerUnitPriceEnabled,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      productInfo: { TCPStyleQTY },
      deleteFavItemInProgressFlag,
      checkForOOSForVariant,
      isShowBrandNameEnabled,
      alternateBrand,
      openModal,
      formValues,
      ...otherProps
    } = this.props;
    const { isPlcc } = otherProps;
    const { currentGiftCardValue, currentColorEntry, showBazaarVoice } = this.state;
    const { colorDisplayId, colorProductId } = currentColorEntry || {};
    const getGiftCardValue = currentGiftCardValue || productInfo.offerPrice;
    const { isGiftCard, primaryBrand } = productInfo;
    const {
      currentProduct: { ratings, reviewsCount },
    } = productDetails;
    return (
      <div className="product-summary-wrapper">
        <Product
          {...otherProps}
          isGiftCard={isGiftCard}
          productDetails={productDetails}
          currencySymbol={currency}
          selectedColorProductId={colorProductId}
          currencyAttributes={currencyAttributes}
          onAddItemToFavorites={onAddItemToFavorites}
          isLoggedIn={isLoggedIn}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          className="hide-on-mobile"
          AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
          removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
          isLoading={isLoading}
          isInWishList={isInWishList}
          isHasPlcc={isPlcc}
          hidePriceDisplay={hidePriceDisplay}
          pdpLabels={pdpLabels}
          isPerUnitPriceEnabled={isPerUnitPriceEnabled}
          isBrierleyPromoEnabled={isBrierleyPromoEnabled}
          TCPStyleQTY={TCPStyleQTY}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          wishList={wishList}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          isAfterPayDisplay={!checkForOOSForVariant}
          isOutfitPage={false}
          colorDisplayId={colorDisplayId}
          primaryBrand={primaryBrand}
          isShowBrandNameEnabled={isShowBrandNameEnabled}
          openModal={openModal}
          alternateBrand={alternateBrand}
          formValues={formValues}
          handleRatingClick={this.handleRatingClick}
        />
        {/* As BazaarVoice renders in CSR only so added below meta tags to add aggregateRating(BazaarVoice) SEO data in page */}
        {showBazaarVoice && ratings > 0 && reviewsCount > 0 && handleMetaProps && (
          <script type="application/ld+json">
            {JSON.stringify(this.getAggregateRatings(ratings, reviewsCount))}
          </script>
        )}
        {isGiftCard ? (
          <div className="product-price-desktop-view">
            {isLoading ? (
              <LoaderSkelton />
            ) : (
              <ProductPrice
                offerPrice={parseInt(getGiftCardValue, 10)}
                listPrice={parseInt(getGiftCardValue, 10)}
                hideItemProp // prop is used to hide unwanted duplicate itemProp as we have duplicate ProductPrice component for web and mobile on PDP pages
                currencySymbol={currency}
                currencyAttributes={currencyAttributes}
                isGiftCard={isGiftCard}
                isPlcc={isPlcc}
                pdpLabels={pdpLabels}
                isPerUnitPriceEnabled={isPerUnitPriceEnabled}
                TCPStyleQTY={TCPStyleQTY}
                wishList={wishList}
                isAfterPayDisplay
              />
            )}
          </div>
        ) : null}
      </div>
    );
  };

  getAggregateRatings = (ratings, reviewsCount) => {
    return {
      '@context': 'http://schema.org',
      '@type': 'Product',
      name: 'Product',
      aggregateRating: {
        '@type': 'AggregateRating',
        ratingValue: ratings,
        bestRating: '5',
        reviewCount: reviewsCount,
      },
    };
  };

  getBackButton = (buttonText, accessibilityLabels, backAction) => {
    return (
      <div className="go-back-container">
        <button type="button" onClick={backAction} className="button-go-back">
          <Image
            src={getIconPath('medium-left-arrow')}
            alt={accessibilityLabels.lbl_left_arrow_icon}
          />
          <BodyCopy className="back-button" fontFamily="secondary" fontSize="fs16">
            {buttonText}
          </BodyCopy>
        </button>
      </div>
    );
  };

  getBreadCrumb = () => {
    const {
      pdpLabels,
      breadCrumbs,
      accessibilityLabels,
      router: {
        query: { isSearchPage },
      },
    } = this.props;
    if (parseBoolean(isSearchPage)) {
      return this.getBackButton(pdpLabels.backToResults, accessibilityLabels, this.triggerBack);
    }
    if (breadCrumbs) {
      return <FixedBreadCrumbs crumbs={breadCrumbs} separationChar=">" isPDPPage />;
    }
    return null;
  };

  getSendAnEmailComponent = () => {
    const { pdpLabels, productInfo, className } = this.props;
    return productInfo.isGiftCard ? (
      <SendAnEmailGiftCard className={className} pdpLabels={pdpLabels} />
    ) : null;
  };

  getProductPriceForGiftCard = () => {
    const {
      productInfo,
      currency,
      currencyAttributes,
      isLoading,
      isPlcc,
      isPerUnitPriceEnabled,
      pdpLabels,
    } = this.props;
    const { currentGiftCardValue } = this.state;
    const { TCPStyleQTY } = productInfo;
    const getGiftCardValue = currentGiftCardValue || productInfo.offerPrice;
    return productInfo.isGiftCard ? (
      <div className="product-price-mobile-view">
        {isLoading ? (
          <LoaderSkelton />
        ) : (
          <ProductPrice
            listPrice={parseInt(getGiftCardValue, 10)}
            offerPrice={parseInt(getGiftCardValue, 10)}
            currencyAttributes={currencyAttributes}
            currencySymbol={currency}
            isPlcc={isPlcc}
            isPerUnitPriceEnabled={isPerUnitPriceEnabled}
            pdpLabels={pdpLabels}
            TCPStyleQTY={TCPStyleQTY}
            isAfterPayDisplay
          />
        )}
      </div>
    ) : null;
  };

  // This is required for reommendations.
  getCatIdForRecommendation = () => {
    const { breadCrumbs } = this.props;
    if (breadCrumbs) {
      const category = breadCrumbs.map((crumb, index) => {
        const { displayName } = crumb;
        const separationChar = index !== breadCrumbs.length - 1 ? ':' : '';
        return displayName + separationChar;
      });
      return category.join('');
    }
    return '';
  };

  getCustomProductLength = () => {
    if (isClient() && getViewportInfo().isTablet) {
      return 4;
    }
    return 6;
  };

  renderPromoBanner = (promoBanners) => {
    const { asPathVal } = this.props;
    return <PromoPDPBanners promos={promoBanners} asPath={asPathVal} />;
  };

  renderPromo = (area, props) => {
    const { [`${area}Promos`]: promos } = props;
    return (
      promos &&
      promos.length > 0 && (
        <Row>
          <Col className={`promo-area-${area}`} colSize={{ small: 6, medium: 8, large: 12 }}>
            {this.renderPromoBanner(promos)}
          </Col>
        </Row>
      )
    );
  };

  getItemColor = () => {
    const { productInfo, formValues } = this.props;
    const { colorFitsSizesMap } = productInfo;
    const getFormName = formValues && formValues.color;
    return getMapSliceForColor(colorFitsSizesMap, getFormName);
  };

  shouldDisplayProductInfo = () => {
    const { isValidProduct, isLoading } = this.props;
    return isValidProduct || isLoading;
  };

  getKeepAlive = () => {
    const { isKeepAliveEnabled } = this.props;
    const { currentColorEntry } = this.state;
    return (
      isKeepAliveEnabled &&
      currentColorEntry &&
      currentColorEntry.miscInfo &&
      currentColorEntry.miscInfo.keepAlive
    );
  };

  getSkuId = (colorProduct) => {
    const { formValues } = this.props;
    const size = formValues && formValues.Size;
    let skuId = null;
    if (typeof size === 'string' && size) {
      skuId = getMapSliceForSizeSkuID(colorProduct, size);
    }
    return skuId;
  };

  familyOutfitCheck = (isFamilyOutfitEnabled, isABTestLoaded, alternateSizesImageNameAndPath) => {
    const { isNewPDPEnabled } = this.props;
    return (
      isNewPDPEnabled &&
      isFamilyOutfitEnabled &&
      isABTestLoaded &&
      alternateSizesImageNameAndPath.length > 0
    );
  };

  getImgWrapperClass = (isCompleteTheLookTestEnabled) => {
    const { isNewPDPEnabled } = this.props;
    const wrapperClassName = isCompleteTheLookTestEnabled ? ` complete-the-look-test` : '';
    return `product-image-wrapper${wrapperClassName}${
      isNewPDPEnabled ? ' sticky-mobile-image-wrapper' : ''
    }`;
  };

  /* eslint-disable-next-line complexity, sonarjs/cognitive-complexity */
  render() {
    const {
      className,
      productDetails,
      longDescription,
      shortDescription,
      itemPartNumber,
      productInfo,
      plpLabels,
      pdpLabels,
      outOfStockLabels,
      handleAddToBag,
      addToBagError,
      alternateSizes,
      isKeepAliveEnabled,
      currency,
      currencyAttributes,
      onAddItemToFavorites,
      isLoggedIn,
      topPromos,
      middlePromos,
      bottomPromos,
      belowrecsPromos,
      sizeChartDetails,
      accessibilityLabels,
      isLoading,
      isOnModelAbTestPdp,
      isAbPdpPlaEnabled,
      completeLookSlotTile,
      isValidProduct,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      wishList,
      imgConfigVal,
      resetCountUpdate,
      socialProofMessage,
      countUpdated,
      addBtnMsg,
      multipackProduct,
      multiPackCount,
      multiPackThreshold,
      partIdInfo,
      getAllNewMultiPack,
      isRecommendationAvailable,
      isPerUnitPriceEnabled,
      getDetails,
      selectedMultipack,
      checkForOOSForVariant,
      isBrierleyPromoEnabled,
      isOosPdpEnabled,
      isPdpNewRecommendationEnabled,
      isPdp1andPdp2Enabled,
      largeImageNameOnHover,
      hasABTestForPDPColorOrImageSwatchHover,
      initialMultipackMapping,
      singlePageLoad,
      setInitialTCPStyleQty,
      resetProductDetailsDynamicData,
      onSetLastDeletedItemIdAction,
      formValues,
      deleteFavItemInProgressFlag,
      addToBagErrorId,
      disableMultiPackTab,
      getDisableSelectedTab,
      istMultiColorNameEnabled,
      isDynamicBadgeEnabled,
      fromPLPPage,
      availableTCPmapNewStyleId,
      isPDPSmoothScrollEnabled,
      isFamilyOutfitEnabled,
      isABTestLoaded,
      isNewPDPEnabled,
      quickViewLabels,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      hidePriceDisplay,
      asPathVal,
      isShowBrandNameEnabled,
      openModal,
      alternateBrand,
      productInfo: { generalProductId },
      ...otherProps
    } = this.props;
    let { isCompleteTheLookTestEnabled } = this.props;
    const {
      isGiftCard,
      TCPStyleQTY,
      TCPStyleType,
      productFamily,
      TCPStyleColor,
      colorFitsSizesMap,
      imagesByColor,
      primaryBrand,
    } = productInfo;
    const colorNameAvailable = formValues && formValues.color;
    const pdpDataInformation = {
      TCPStyleQTY,
      TCPStyleType,
      color: { name: colorNameAvailable },
      productFamily,
    };

    const { showBazaarVoice, starRatingClicked } = this.state;
    const {
      ratingsProductId,
      isPlcc,
      router: {
        query: { viaModule, actionId },
      },
    } = otherProps;
    const { currentProduct, currentProductDynamicData } = productDetails;

    const { productId } = currentProduct;
    let imagesToDisplay = [];
    let imageSwatchesToDisplay = [];
    const isProductDataAvailable = Object.keys(productInfo).length > 0;
    const { currentColorEntry, renderReceiveProps } = this.state;
    const { colorDisplayId, colorProductId } = currentColorEntry || {};
    const getFormName = formValues && formValues.color;
    const updatedColorEntry = getMapSliceForColor(colorFitsSizesMap, getFormName);
    let newColorList =
      istMultiColorNameEnabled && TCPStyleColor && TCPStyleColor.length
        ? getMapColorList(TCPStyleColor, updatedColorEntry)
        : getFormName && [getFormName];
    // colorList should never be empty, even if color is not coming
    // from TCPStyleColor then we default to the formValue.
    if (!newColorList && TCPStyleColor && TCPStyleColor.length) {
      newColorList = getFormName && [getFormName];
    }
    if (!isEqual(newColorList, this.newColorListHolder)) {
      this.newColorListHolder = newColorList;
    }
    const keepAlive = this.getKeepAlive();
    if (isProductDataAvailable) {
      imagesToDisplay = getImagesToDisplay({
        imagesByColor,
        curentColorEntry:
          !updatedColorEntry || generalProductId?.includes(`$brand=${alternateBrand}`)
            ? currentColorEntry
            : updatedColorEntry,
        isAbTestActive: isOnModelAbTestPdp,
        isFullSet: true,
      });
      imageSwatchesToDisplay = getPrimaryImages({
        imagesByColor,
        curentColorEntry: !updatedColorEntry ? currentColorEntry : updatedColorEntry,
        isAbTestActive: isOnModelAbTestPdp,
        isFullSet: true,
      });
      if (!isEqual(imageSwatchesToDisplay, this.imageSwatchesToDisplayHolder)) {
        this.imageSwatchesToDisplayHolder = imageSwatchesToDisplay;
      }
    }

    if (!isUsOnly()) {
      isCompleteTheLookTestEnabled = false;
    }
    const sizeChartLinkVisibility = !isGiftCard ? SIZE_CHART_LINK_POSITIONS.AFTER_SIZE : null;
    const carouselOptions = ProductDetailsUtils.PDPCarouselOptions.CAROUSEL_OPTIONS;

    const categoryId = this.getCatIdForRecommendation();
    const customProductsLength = this.getCustomProductLength();
    const recommendationAttributes = {
      variations: 'moduleO',
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP,
      categoryName: categoryId,
      partNumber: productId,
      showLoyaltyPromotionMessage: false,
      headerAlignment: 'left',
      outOfStock: checkForOOSForVariant,
      isRecProduct: viaModule === RECOMMENDATION,
      actionId,
    };
    let currentPdpProduct = null;
    if (isValidObj(currentProductDynamicData)) {
      currentPdpProduct = currentProductDynamicData;
    } else if (isValidObj(currentProduct)) {
      currentPdpProduct = currentProduct;
    }
    const pdpProductDetails = {
      currentProduct: currentPdpProduct,
    };
    const isInWishList = this.checkInWishlist(
      wishList,
      colorDisplayId || pdpProductDetails.currentProduct.generalProductId
    );

    const itemColorObj = this.getItemColor();
    const showPriceRange = isShowPriceRangeKillSwitch && showPriceRangeForABTest;
    const alternateSizesImageNameAndPath = Object.entries(alternateSizes);
    const productIdForSwatch = itemColorObj && itemColorObj.colorDisplayId;
    const colorProduct =
      getMapSliceForColorProductId(productInfo.colorFitsSizesMap, colorProductId) || {};
    const prodSku = this.getSkuId(colorProduct);
    const isMobile = isClient() ? getViewportInfo().isMobile : null;
    const { currentGiftCardValue } = this.state;
    const getGiftCardValue = currentGiftCardValue || productInfo.offerPrice;
    return (
      <div className={className} itemScope itemType="https://schema.org/Product">
        {this.shouldDisplayProductInfo() ? (
          <>
            <Row fullBleed={{ small: true, medium: true, large: false }}>
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <div className={`${className} product-recommendation-list`}>
                  {checkForOOSForVariant && isOosPdpEnabled && colorNameAvailable && !isLoading && (
                    <Recommendations
                      headerLabel={pdpLabels.checkoutSimilarStyle}
                      portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.OUT_OF_STOCK_PDP}
                      {...recommendationAttributes}
                      priceOnly
                      customProductsLength={customProductsLength}
                      inheritedStyles={recommendationStylesPdp}
                      carouselConfigProps={carouselOptions}
                      sequence="1"
                      accordionHeader
                      isPdpPlaProducts
                      outOfStock
                      pdpDataInformation={pdpDataInformation}
                      topRecommendationOOS
                    />
                  )}
                  {isAbPdpPlaEnabled && colorNameAvailable && !isLoading && (
                    <Recommendations
                      headerLabel={pdpLabels.checkoutSimilarStyle}
                      portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.CHECKOUT_SIMILAR_STYLES}
                      {...recommendationAttributes}
                      priceOnly
                      customProductsLength={customProductsLength}
                      inheritedStyles={recommendationStylesPdp}
                      carouselConfigProps={carouselOptions}
                      sequence="1"
                      accordionHeader
                      isPdpPlaProducts
                      pdpDataInformation={pdpDataInformation}
                      isNewPDPEnabled={isNewPDPEnabled}
                      topRecommendationOOS
                    />
                  )}
                </div>
              </Col>
            </Row>
            <Row>
              <Col
                className="product-breadcrumb-wrapper"
                colSize={{ small: 6, medium: 8, large: 12 }}
              >
                {this.getBreadCrumb()}
              </Col>
            </Row>
            {this.renderPromo('top', this.props)}
            <Row>
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                {isGiftCard && !(isNewPDPEnabled && isMobile) ? (
                  <div className="product-summary-mobile-view">
                    {this.getProductSummary(keepAlive, false, isInWishList)}
                  </div>
                ) : null}
              </Col>
              <Col
                className={this.getImgWrapperClass(isCompleteTheLookTestEnabled)}
                colSize={{ small: 6, medium: 4, large: 7 }}
              >
                <div className={`${isNewPDPEnabled ? 'new-pdp-product-detail-wrapper' : ''}`}>
                  <Product
                    {...otherProps}
                    keepAlive={keepAlive}
                    outOfStockLabels={outOfStockLabels}
                    isGiftCard={isGiftCard}
                    productDetails={pdpProductDetails}
                    currencySymbol={currency}
                    selectedColorProductId={colorProductId}
                    currencyAttributes={currencyAttributes}
                    onAddItemToFavorites={onAddItemToFavorites}
                    isLoggedIn={isLoggedIn}
                    reviewOnTop
                    hideItemProp // prop is used to hide unwanted duplicate itemProp as we have duplicate ProductPrice component for web and mobile on PDP pages
                    isHasPlcc={isPlcc}
                    isInWishList={isInWishList}
                    resetCountUpdate={resetCountUpdate}
                    socialProofMessage={socialProofMessage}
                    countUpdated={countUpdated}
                    pdpLabels={pdpLabels}
                    isPerUnitPriceEnabled={isPerUnitPriceEnabled}
                    checkForOOSForVariant={checkForOOSForVariant}
                    isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                    TCPStyleQTY={TCPStyleQTY}
                    onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                    deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                    wishList={wishList}
                    isLowTier={this.isLowTier}
                    isOutfitPage={false}
                    colorDisplayId={colorDisplayId}
                    isNewPDPEnabled={isNewPDPEnabled}
                    primaryBrand={primaryBrand}
                    isShowBrandNameEnabled={isShowBrandNameEnabled}
                    openModal={openModal}
                    alternateBrand={alternateBrand}
                    formValues={formValues}
                    handleRatingClick={this.handleRatingClick}
                  />
                </div>
                <ProductImagesWrapper
                  productName={productInfo.name}
                  isGiftCard={isGiftCard}
                  isThumbnailListVisible
                  images={imagesToDisplay}
                  pdpLabels={pdpLabels}
                  isZoomEnabled
                  currentProduct={currentProduct}
                  onChangeColor={this.onChangeColor}
                  currentColorEntry={currentColorEntry}
                  initialValues={this.formValues}
                  keepAlive={keepAlive}
                  outOfStockLabels={outOfStockLabels}
                  accessibilityLabels={accessibilityLabels}
                  ratingsProductId={ratingsProductId}
                  completeLookSlotTile={completeLookSlotTile}
                  itemPartNumber={itemPartNumber}
                  imgConfigVal={imgConfigVal}
                  checkForOOSForVariant={checkForOOSForVariant}
                  isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
                  imagesByColor={productInfo.imagesByColor}
                  largeImageNameOnHover={largeImageNameOnHover}
                  hasABTestForPDPColorOrImageSwatchHover={hasABTestForPDPColorOrImageSwatchHover}
                  pdpDataInformation={pdpDataInformation}
                  deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                  TCPStyleQTY={TCPStyleQTY}
                  TCPStyleType={TCPStyleType}
                  isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                  fromPLPPage={fromPLPPage}
                  wishList={wishList}
                  isPDPSmoothScrollEnabled={isPDPSmoothScrollEnabled}
                  isNewPDPEnabled={isNewPDPEnabled}
                  onAddItemToFavorites={onAddItemToFavorites}
                  productInfo={productInfo}
                  selectedColorProductId={colorProductId}
                  productMiscInfo={colorProduct}
                  pageName="PDP"
                  skuId={prodSku}
                  onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                  formName={PRODUCT_ADD_TO_BAG}
                  primaryBrand={primaryBrand}
                  isLoggedIn={isLoggedIn}
                  isInWishList={isInWishList}
                  alternateBrand={alternateBrand}
                  socialProofMessage={socialProofMessage}
                  isPDP
                />
              </Col>
              <Col
                id="productDetailsSection"
                className="product-detail-section sticky-product-detail"
                colSize={{ small: 6, medium: 4, large: 5 }}
              >
                {!(isNewPDPEnabled && isMobile) && (
                  <div className={isGiftCard ? 'product-summary-desktop-view' : ''}>
                    {this.getProductSummary(keepAlive, true, isInWishList)}
                  </div>
                )}
                {!(isNewPDPEnabled && isMobile) && this.getProductPriceForGiftCard()}
                {middlePromos && middlePromos.length > 0 && (
                  <div className="promo-area-middle">{this.renderPromoBanner(middlePromos)}</div>
                )}

                {currentProduct && (
                  <ProductAddToBagContainer
                    handleFormSubmit={handleAddToBag}
                    errorOnHandleSubmit={addToBagError}
                    currentProduct={currentPdpProduct}
                    plpLabels={plpLabels}
                    onChangeColor={this.onChangeColor}
                    customSubmitButtonStyle={customSubmitButtonStyle}
                    onChangeSize={this.onChangeSize}
                    updateQuantityChange={this.updateQuantityChange}
                    selectedColorProductId={colorProductId}
                    renderReceiveProps={renderReceiveProps}
                    initialFormValues={this.formValues}
                    isPDP
                    alternateSizes={alternateSizes}
                    sizeChartLinkVisibility={sizeChartLinkVisibility}
                    isKeepAliveEnabled={isKeepAliveEnabled}
                    outOfStockLabels={outOfStockLabels}
                    sizeChartDetails={sizeChartDetails}
                    isLoading={isLoading}
                    addBtnMsg={addBtnMsg}
                    multipackProduct={multipackProduct}
                    multiPackCount={multiPackCount}
                    multiPackThreshold={multiPackThreshold}
                    partIdInfo={partIdInfo}
                    getAllNewMultiPack={getAllNewMultiPack}
                    isRecommendationAvailable={isRecommendationAvailable}
                    pdpLabels={pdpLabels}
                    getDetails={getDetails}
                    fromPDP
                    selectedMultipack={selectedMultipack}
                    TCPStyleQTY={TCPStyleQTY}
                    checkForOOSForVariant={checkForOOSForVariant}
                    imageSwatchesToDisplay={this.imageSwatchesToDisplayHolder}
                    initialMultipackMapping={initialMultipackMapping}
                    singlePageLoad={singlePageLoad}
                    setInitialTCPStyleQty={setInitialTCPStyleQty}
                    resetProductDetailsDynamicData={resetProductDetailsDynamicData}
                    addToBagErrorId={addToBagErrorId}
                    disableMultiPackTab={disableMultiPackTab}
                    getDisableSelectedTab={getDisableSelectedTab}
                    newColorList={this.newColorListHolder}
                    formValues={formValues}
                    isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                    availableTCPmapNewStyleId={availableTCPmapNewStyleId}
                    wishList={wishList}
                    socialProofMessage={socialProofMessage}
                    isFamilyOutfitEnabled={isFamilyOutfitEnabled}
                    isNewPDPEnabled={isNewPDPEnabled}
                    isNewQVEnabled={isNewPDPEnabled}
                    quickViewLabels={quickViewLabels}
                    primaryBrand={primaryBrand}
                    keepAlive={keepAlive}
                    isGiftCard={isGiftCard}
                    pdpProductDetails={pdpProductDetails}
                    currencySymbol={currency}
                    currencyAttributes={currencyAttributes}
                    onAddItemToFavorites={onAddItemToFavorites}
                    isLoggedIn={isLoggedIn}
                    isHasPlcc={isPlcc}
                    isInWishList={isInWishList}
                    resetCountUpdate={resetCountUpdate}
                    countUpdated={countUpdated}
                    isPerUnitPriceEnabled={isPerUnitPriceEnabled}
                    isBrierleyPromoEnabled={isBrierleyPromoEnabled}
                    onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
                    deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
                    isLowTier={this.isLowTier}
                    isOutfitPage={false}
                    colorDisplayId={colorDisplayId}
                    productDetailsPDP={productDetails}
                    productInfo={productInfo}
                    AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
                    removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
                    hidePriceDisplay={hidePriceDisplay}
                    getGiftCardValue={getGiftCardValue}
                    isShowBrandNameEnabled={isShowBrandNameEnabled}
                    openModal={openModal}
                    alternateBrandFromUrl={alternateBrand}
                    alternateBrand={alternateBrand}
                    handleRatingClick={this.handleRatingClick}
                  />
                )}

                {!isNewPDPEnabled && productInfo && currentColorEntry && (
                  <ProductPickupContainer
                    productInfo={productInfo}
                    formName={`ProductAddToBag-${productInfo.generalProductId}`}
                    miscInfo={currentColorEntry.miscInfo}
                    keepAlive={keepAlive}
                    outOfStockLabels={outOfStockLabels}
                    availableTCPmapNewStyleId={availableTCPmapNewStyleId}
                    initialMultipackMapping={initialMultipackMapping}
                    isNewPDPEnabled={isNewPDPEnabled}
                    setInitialTCPStyleQty={setInitialTCPStyleQty}
                    primaryBrand={primaryBrand}
                    alternateBrand={alternateBrand}
                    // onPickUpOpenClick={onPickUpOpenClick}
                  />
                )}
                <div className="product-detail-section key-feature-section">
                  {!isGiftCard && isNewPDPEnabled && !isMobile && (
                    <ProductDescription
                      productId={itemPartNumber}
                      productInfo={productInfo}
                      isShowMore={false}
                      pdpLabels={pdpLabels}
                      shortDescription={shortDescription}
                      longDescription={longDescription}
                      color={itemColorObj && itemColorObj.color && itemColorObj.color.name}
                      isPDP
                      TCPStyleColor={TCPStyleColor}
                      wishList={wishList}
                      isNewPDPEnabled={isNewPDPEnabled}
                      primaryBrand={primaryBrand}
                    />
                  )}
                </div>
                <div
                  className={`${
                    this.familyOutfitCheck(
                      isFamilyOutfitEnabled,
                      isABTestLoaded,
                      alternateSizesImageNameAndPath
                    )
                      ? 'border-background-new-design'
                      : ''
                  }`}
                >
                  <div
                    className={`${
                      this.familyOutfitCheck(
                        isFamilyOutfitEnabled,
                        isABTestLoaded,
                        alternateSizesImageNameAndPath
                      )
                        ? 'also-available-wrapper-new-design'
                        : ''
                    }`}
                  >
                    {isFamilyOutfitEnabled &&
                    isABTestLoaded &&
                    alternateSizesImageNameAndPath.length > 0 ? (
                      <div className="also-available">{plpLabels.alsoAvailableIn}</div>
                    ) : null}
                    {isFamilyOutfitEnabled &&
                    isABTestLoaded &&
                    alternateSizesImageNameAndPath.length > 0 ? (
                      <FamilyOutfit
                        alternateSizesImageNameAndPath={alternateSizesImageNameAndPath}
                        alternateBrand={alternateBrand}
                      />
                    ) : null}
                  </div>
                </div>
                {!isLoading ? (
                  <div
                    className={`${
                      isNewPDPEnabled && isUsOnly() ? 'border-background-new-design' : ''
                    }`}
                  >
                    <div
                      className={`${
                        isNewPDPEnabled && isUsOnly() ? 'loyalty-banner-wrapper-new-design' : ''
                      }`}
                    >
                      <LoyaltyBanner
                        pageCategory="isProductDetailView"
                        accessibilityLabels={accessibilityLabels}
                        ratingsProductId={ratingsProductId}
                        isCompleteTheLookTestEnabled={isCompleteTheLookTestEnabled}
                      />
                    </div>
                  </div>
                ) : (
                  <div className="product-property-section">
                    <LoaderSkelton height="60px" />
                  </div>
                )}
                {this.getSendAnEmailComponent()}
              </Col>
            </Row>
            {this.renderPromo('bottom', this.props)}
            <Row fullBleed={{ small: true, medium: true, large: false }}>
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <div className="product-detail-section key-feature-section">
                  {!isGiftCard && !(isNewPDPEnabled && !isMobile) && (
                    <ProductDescription
                      productId={itemPartNumber}
                      productInfo={productInfo}
                      isShowMore={false}
                      pdpLabels={pdpLabels}
                      shortDescription={shortDescription}
                      longDescription={longDescription}
                      color={itemColorObj && itemColorObj.color && itemColorObj.color.name}
                      isPDP
                      TCPStyleColor={TCPStyleColor}
                      wishList={wishList}
                      isNewPDPEnabled={isNewPDPEnabled}
                      primaryBrand={primaryBrand}
                    />
                  )}
                </div>
              </Col>
            </Row>
            <Row
              className="pdp-recommendations-wrapper"
              fullBleed={{ small: true, medium: true, large: false }}
            >
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <div className="product-detail-section complete-look-section">
                  {!isLoading && (
                    <RelatedOutfits
                      pdpLabels={pdpLabels}
                      selectedColorProductId={productIdForSwatch}
                      isPDP
                      page={Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP}
                      triggerStylyticsAnalytics={this.triggerStylyticsAnalytics}
                      completeLookSlotTile={completeLookSlotTile}
                      isNewPDPEnabled={isNewPDPEnabled}
                      primaryBrand={primaryBrand}
                    />
                  )}
                </div>
              </Col>
            </Row>
            {isPdp1andPdp2Enabled && colorNameAvailable && !isLoading && (
              <React.Fragment>
                <Row
                  className="pdp-recommendations-wrapper"
                  fullBleed={{ small: true, medium: true, large: false }}
                >
                  <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                    <div className={`${className} product-description-list`}>
                      <Recommendations
                        inheritedStyles={recommendationStylesPdp1AndPdp2}
                        headerLabel={pdpLabels.youMayAlsoLike}
                        showPriceRange={showPriceRange}
                        {...recommendationAttributes}
                        sequence="1"
                        accordionHeader={!isPdpNewRecommendationEnabled}
                        newRecommendationEnabled={isPdpNewRecommendationEnabled}
                        pdpDataInformation={pdpDataInformation}
                        isNewPDPEnabled={isNewPDPEnabled}
                        primaryBrand={primaryBrand}
                      />
                    </div>
                  </Col>
                </Row>
                {this.willMonetateCarouselDisplay && (
                  <Row
                    className="pdp-recommendations-wrapper"
                    fullBleed={{ small: true, medium: true, large: false }}
                  >
                    <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                      <div className="product-detail-section">
                        {!isLoading && (
                          <Recommendations
                            inheritedStyles={recommendationStylesPdp1AndPdp2}
                            headerLabel={pdpLabels.recentlyViewed}
                            portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
                            {...recommendationAttributes}
                            sequence="2"
                            accordionHeader={!isPdpNewRecommendationEnabled}
                            newRecommendationEnabled={isPdpNewRecommendationEnabled}
                            pdpDataInformation={pdpDataInformation}
                            isNewPDPEnabled={isNewPDPEnabled}
                            primaryBrand={primaryBrand}
                          />
                        )}
                      </div>
                    </Col>
                  </Row>
                )}
              </React.Fragment>
            )}
            <PdpVmpDataLogger {...this.props} />
            {this.renderPromo('belowrecs', this.props)}
            <Row className="pdp-recommendations-wrapper">
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <div className="get-candid-section">
                  {this.apiConfig.isCandidEnabled && !isGiftCard && (
                    <DynamicModule
                      key="get-candid"
                      importCallback={() =>
                        import(
                          /* webpackChunkName:"get-candid" */ '@tcp/core/src/components/common/molecules/GetCandid'
                        )
                      }
                      productId={productId}
                      isPDP
                    />
                  )}
                </div>
              </Col>
            </Row>
            <Row
              className="pdp-recommendations-wrapper"
              fullBleed={{ small: true, medium: true, large: false }}
            >
              <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                <div className="reviews-anchoring-wrapper">
                  {!this.isLowTier && showBazaarVoice && !isGiftCard && isLoggedIn !== null && (
                    <ProductReviewsContainer
                      expanded={false}
                      reviewsCount={productInfo.reviewsCount}
                      ratingsProductId={productInfo.ratingsProductId}
                      isClient={isClient()}
                      bazaarVoice={productDetails.currentProduct.bazaarVoice}
                      starRatingClicked={starRatingClicked}
                      handleRatingClick={this.handleRatingClick}
                    />
                  )}
                </div>
              </Col>
            </Row>
          </>
        ) : (
          <>{pdpLabels.pageNotFound}</>
        )}
      </div>
    );
  }
}

ProductDetailView.propTypes = {
  className: PropTypes.string,
  addToBagError: PropTypes.string,
  handleAddToBag: PropTypes.func.isRequired,
  productDetails: PropTypes.shape({}),
  productInfo: PRODUCT_INFO_PROP_TYPE_SHAPE,
  shortDescription: PropTypes.string,
  itemPartNumber: PropTypes.string,
  longDescription: PropTypes.string,
  breadCrumbs: PropTypes.shape([]),
  pdpLabels: PropTypes.shape({}),
  currency: PropTypes.string,
  currencyAttributes: PropTypes.shape({}).isRequired,
  plpLabels: PropTypes.shape({
    lbl_sort: PropTypes.string,
  }),
  onAddItemToFavorites: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  alternateSizes: PropTypes.shape({
    key: PropTypes.string,
  }),
  outOfStockLabels: PropTypes.shape({
    outOfStockCaps: PropTypes.string,
  }).isRequired,
  isKeepAliveEnabled: PropTypes.bool,
  AddToFavoriteErrorMsg: PropTypes.string,
  removeAddToFavoritesErrorMsg: PropTypes.func,
  sizeChartDetails: PropTypes.shape([]),
  asPathVal: PropTypes.string,
  isValidProduct: PropTypes.bool.isRequired,
  topPromos: PropTypes.string,
  middlePromos: PropTypes.string,
  bottomPromos: PropTypes.string,
  belowrecsPromos: PropTypes.string,
  trackPageLoad: PropTypes.func,
  isSearchPage: PropTypes.bool,
  router: PropTypes.shape({}).isRequired,
  accessibilityLabels: PropTypes.shape({}),
  isLoading: PropTypes.bool.isRequired,
  isOnModelAbTestPdp: PropTypes.bool,
  isAbPdpPlaEnabled: PropTypes.bool,
  completeLookSlotTile: PropTypes.shape({}),
  isInternationalShipping: PropTypes.bool.isRequired,
  navigateType: PropTypes.string,
  wishList: PropTypes.func.isRequired,
  searchedText: PropTypes.string,
  isShowPriceRangeKillSwitch: PropTypes.bool.isRequired,
  showPriceRangeForABTest: PropTypes.bool.isRequired,
  isPlcc: PropTypes.bool.isRequired,
  storeId: PropTypes.string,
  trackClick: PropTypes.func.isRequired,
  imgConfigVal: PropTypes.shape([]),
  resetCountUpdate: PropTypes.func.isRequired,
  getSocialProofData: PropTypes.func,
  countUpdated: PropTypes.bool,
  isCompleteTheLookTestEnabled: PropTypes.bool,
  hidePriceDisplay: PropTypes.bool,
  isDefaultBreadCrumb: PropTypes.bool.isRequired,
  addBtnMsg: PropTypes.string.isRequired,
  multipackProduct: PropTypes.string.isRequired,
  multiPackCount: PropTypes.string.isRequired,
  multiPackThreshold: PropTypes.string.isRequired,
  partIdInfo: PropTypes.string.isRequired,
  getAllNewMultiPack: PropTypes.shape([]).isRequired,
  isRecommendationAvailable: PropTypes.bool.isRequired,
  isPerUnitPriceEnabled: PropTypes.bool,
  getDetails: PropTypes.shape([]).isRequired,
  selectedMultipack: PropTypes.string.isRequired,
  TCPStyleQTY: PropTypes.number.isRequired,
  checkForOOSForVariant: PropTypes.bool.isRequired,
  isBrierleyPromoEnabled: PropTypes.bool,
  isOosPdpEnabled: PropTypes.bool,
  isPdpNewRecommendationEnabled: PropTypes.bool,
  isPdp1andPdp2Enabled: PropTypes.bool,
  selectColorToHover: PropTypes.func,
  largeImageNameOnHover: PropTypes.string,
  hasABTestForPDPColorOrImageSwatchHover: PropTypes.bool,
  initialMultipackMapping: PropTypes.shape([]).isRequired,
  singlePageLoad: PropTypes.bool.isRequired,
  setInitialTCPStyleQty: PropTypes.bool.isRequired,
  resetProductDetailsDynamicData: PropTypes.shape([]).isRequired,
  onSetLastDeletedItemIdAction: PropTypes.func,
  formValues: PropTypes.shape([]).isRequired,
  deleteFavItemInProgressFlag: PropTypes.bool,
  addToBagErrorId: PropTypes.string,
  disableMultiPackTab: PropTypes.bool,
  getDisableSelectedTab: PropTypes.func.isRequired,
  istMultiColorNameEnabled: PropTypes.bool.isRequired,
  isDynamicBadgeEnabled: PropTypes.shape({}),
  fromPLPPage: PropTypes.bool,
  availableTCPmapNewStyleId: PropTypes.shape([]),
  isPDPSmoothScrollEnabled: PropTypes.bool,
  socialProofMessage: PropTypes.string,
  isFamilyOutfitEnabled: PropTypes.bool,
  isNewPDPEnabled: PropTypes.bool,
  isABTestLoaded: PropTypes.bool,
  quickViewLabels: PropTypes.shape({
    addToBag: PropTypes.string,
    viewProductDetails: PropTypes.string,
  }).isRequired,
  isShowBrandNameEnabled: PropTypes.bool,
  openModal: PropTypes.func,
};

ProductDetailView.defaultProps = {
  className: '',
  productDetails: {},
  longDescription: '',
  shortDescription: '',
  breadCrumbs: [],
  currency: '',
  plpLabels: {
    lbl_sort: '',
  },
  itemPartNumber: '',
  productInfo: {},
  pdpLabels: {},
  addToBagError: '',
  isLoggedIn: false,
  alternateSizes: {},
  isKeepAliveEnabled: false,
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  trackPageLoad: () => {},
  getSocialProofData: () => {},
  sizeChartDetails: [],
  asPathVal: '',
  topPromos: '',
  middlePromos: '',
  bottomPromos: '',
  belowrecsPromos: '',
  isSearchPage: false,
  accessibilityLabels: {},
  isOnModelAbTestPdp: false,
  isAbPdpPlaEnabled: false,
  completeLookSlotTile: {},
  navigateType: '',
  searchedText: '',
  storeId: null,
  imgConfigVal: [],
  countUpdated: false,
  isCompleteTheLookTestEnabled: false,
  hidePriceDisplay: false,
  isPerUnitPriceEnabled: false,
  isBrierleyPromoEnabled: false,
  isOosPdpEnabled: false,
  isPdpNewRecommendationEnabled: false,
  isPdp1andPdp2Enabled: true,
  selectColorToHover: () => {},
  largeImageNameOnHover: '',
  hasABTestForPDPColorOrImageSwatchHover: false,
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  addToBagErrorId: '',
  disableMultiPackTab: false,
  isDynamicBadgeEnabled: false,
  fromPLPPage: false,
  availableTCPmapNewStyleId: [],
  isPDPSmoothScrollEnabled: false,
  socialProofMessage: '',
  isFamilyOutfitEnabled: false,
  isNewPDPEnabled: false,
  isABTestLoaded: false,
  isShowBrandNameEnabled: false,
  openModal: () => {},
};

export default errorBoundary(withStyles(ProductDetailView, ProductDetailStyle));
