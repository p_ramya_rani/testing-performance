/* eslint-disable sonarjs/cognitive-complexity */
import PropTypes from 'prop-types';
import React from 'react';
import { isCanada } from '../../../../../utils';
import { DamImage } from '../../../../common/atoms';
import withStyles from '../../../../common/hoc/withStyles';
import styles from '../PdpVmpDataLogger.style';

const parseProductIds = (pString = '') => {
  const parr = pString.split(',');
  if (parr.length < 1) {
    return [];
  }
  return parr.map((pv) => pv.split('#')[0]);
};
const mergeDataArrays = (arrays) => {
  let finalArr = [];
  finalArr = [...arrays[0]];
  arrays.slice(1).forEach((arr) => {
    arr.forEach((arrValue, i) => {
      finalArr[i].name = `${finalArr[i].name} | ${arrValue.name}`;
      finalArr[i].size = `${finalArr[i].size} | ${arrValue.size}`;
      finalArr[i].styleType = `${finalArr[i].styleType} | ${arrValue.styleType}`;
      finalArr[i].singleQty = `${finalArr[i].singleQty} | ${arrValue.singleQty}`;
      finalArr[i].fit = `${finalArr[i].fit} | ${arrValue.fit}`;
      finalArr[i].variantId = `${finalArr[i].variantId} | ${arrValue.variantId}`;
      finalArr[i].thresholdValue = `${finalArr[i].thresholdValue} | ${arrValue.thresholdValue}`;
    });
  });
  return finalArr;
};
const formatSize = (size = '') => {
  const sizeValue = size.split('_');
  if (sizeValue) {
    return sizeValue[1];
  }
  return '';
};

const getThresholdValue = (availableQty, quantity, selectedQuantity, thresholdCount) => {
  return availableQty / parseInt(quantity, 10) - parseInt(selectedQuantity, 10) < thresholdCount;
};

const PdpVmpDataLogger = ({ router, productDetails, multiPackThreshold, className }) => {
  const asPath = router?.asPath || '';
  const params = new URLSearchParams(asPath.split('?')[1] || '');
  const shouldVmpDebug = params.get('debugvmp') === 'true';
  const isCanadaStore = isCanada();

  if (!shouldVmpDebug) {
    return null;
  }
  const vmpMatrix = {};

  try {
    const multipkArr = productDetails.getMultiPackAllColor;
    const singlepkArr = productDetails.getNewMultiPackAllColor;

    if (multipkArr && multipkArr.length > 0) {
      multipkArr.forEach((mulpk) => {
        if (mulpk?.TCPMultiPackReferenceUSStore) {
          const packId = mulpk?.prodpartno;
          const productIds = parseProductIds(mulpk.TCPMultiPackReferenceUSStore);
          vmpMatrix[packId] = {
            name: mulpk.name,
            packCount: productDetails.multiPackCount,
            TCPMultiPackReferenceUSStore: mulpk.TCPMultiPackReferenceUSStore,
            TCPMultiPackUSStore: mulpk?.TCPMultiPackUSStore,
            prodpartno: mulpk?.prodpartno,
            productIds,
          };
          const mulpkVariants = mulpk.variants;
          vmpMatrix[packId].variants = [];
          vmpMatrix[packId].images = [];

          if (productIds.length > 0) {
            productIds.forEach((pid) => {
              const singleProduct = singlepkArr.find((spk) => spk.prodpartno === pid);

              if (singleProduct) {
                vmpMatrix[packId][pid] = [];
                if (singleProduct.variants.length > 0) {
                  vmpMatrix[packId].images = [
                    ...vmpMatrix[packId].images,
                    {
                      title: singleProduct.variants[0].product_name,
                      color: singleProduct.variants[0].auxdescription,
                      imgData: {
                        alt: singleProduct.variants[0].productimage,
                        url: `${singleProduct.variants[0].productfamily}/${singleProduct.variants[0].productimage}`,
                      },
                    },
                  ];
                  mulpkVariants.forEach((spv, i) => {
                    const singlePackVariant = singleProduct.variants.find(
                      (sp) => sp.v_tcpsize === spv.v_tcpsize
                    );

                    vmpMatrix[packId][pid] = [
                      ...vmpMatrix[packId][pid],
                      {
                        name: singleProduct?.name || '-',
                        packQty:
                          mulpkVariants[i]?.v_qty !== undefined ? mulpkVariants[i]?.v_qty : '-',
                        singleQty:
                          singlePackVariant?.v_qty !== undefined ? singlePackVariant?.v_qty : '-',
                        size: formatSize(singlePackVariant?.v_tcpsize || '') || '-',
                        mul_size: mulpkVariants[i]?.v_tcpsize || '-',
                        fit: singlePackVariant?.v_tcpfit || '-',
                        styleType: isCanadaStore
                          ? singlePackVariant?.TCPStyleTypeUS
                          : singlePackVariant?.TCPStyleTypeCA || '-',
                        mul_fit: mulpkVariants[i]?.v_tcpfit || '-',
                        mul_price: mulpkVariants[i]?.v_listprice || '-',
                        productfamily: singlePackVariant?.productfamily,
                        productimage: singlePackVariant?.productimage,
                        thresholdValue: getThresholdValue(
                          singlePackVariant?.v_qty,
                          productDetails.multiPackCount,
                          1,
                          multiPackThreshold
                        )
                          ? 'FALSE'
                          : 'TRUE',
                        variantId:
                          singlePackVariant?.variantId !== undefined
                            ? singlePackVariant.variantId
                            : '-',
                      },
                    ];
                  });
                  vmpMatrix[packId].variants = [
                    ...vmpMatrix[packId].variants,
                    vmpMatrix[packId][pid],
                  ];
                }
              }
            });
            vmpMatrix[packId].mergedVariants = mergeDataArrays(vmpMatrix[packId].variants);
          }
        }
      });
    }
  } catch (error) {
    console.log('-----vmp-logger--:error--', error);
  }

  const vpmObjectKeys = Object.keys(vmpMatrix);
  if (vpmObjectKeys.length < 1) {
    return null;
  }

  return (
    <div className={className}>
      <h1>
        VMP pack of
        {` (${productDetails.multiPackCount})`}
      </h1>
      <span> Threshold Value: </span>
      <span>{multiPackThreshold || ''}</span>
      <div className="threshold-explained">
        <span>Formula used to calculate Threshold</span>
        <ul>
          {`threshold = availableQty / parseInt(quantity, 10) - parseInt(selectedQuantity, 10) < thresholdCount`}
          <li>availableQty is v_qty</li>
          <li>quantity is multipack count (2pack,3pack)</li>
          <li>selectedQuantity is 1</li>
          <li>{`thresholdCount is ${multiPackThreshold}`}</li>
        </ul>
      </div>
      {vpmObjectKeys.length > 0 &&
        vpmObjectKeys.map((key) => {
          const packInfo = vmpMatrix[key];
          return (
            <div key={key}>
              <div className="vmp-pack-title">
                <span className="vmp-pack-name ">{packInfo.name}</span>
                <span className="vmp-pack-sub-title">{` TCPMultiPackReferenceUSStore: ${packInfo.TCPMultiPackReferenceUSStore} `}</span>
                <span className="vmp-pack-sub-title">{` prodpartno:  ${packInfo.prodpartno} `}</span>
              </div>
              {packInfo.images &&
                packInfo.images.map((product) => {
                  return (
                    <div key={product.imgData.alt}>
                      <DamImage
                        className="product-thumbnail"
                        imgData={product.imgData}
                        isProductImage
                        lazyLoad={false}
                        height={50}
                      />
                      <span className="vmp-pack-name">{` Title: ${product.title} `}</span>
                      <span className="vmp-pack-name">{` Color: ${product.color}`}</span>
                    </div>
                  );
                })}
              <table className="vmp-matrix-tbl">
                <thead>
                  <th>UPC</th>
                  <th>THRESHOLD</th>
                  <th>COLOR</th>
                  <th>PACK QTY</th>
                  <th>SINGLE QTY</th>
                  <th>SINGLE SIZE</th>
                  <th>SINGLE FIT</th>
                </thead>
                <tbody>
                  {packInfo.mergedVariants.map((variant) => {
                    return (
                      <tr key={variant.variantId}>
                        <td>{variant.variantId}</td>
                        <td>{variant.thresholdValue}</td>
                        <td>{variant.name}</td>
                        <td>{variant.packQty}</td>
                        <td>{variant.singleQty}</td>
                        <td>{variant.size}</td>
                        <td>{variant.fit}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          );
        })}
    </div>
  );
};
PdpVmpDataLogger.propTypes = {
  router: PropTypes.object.isRequired,
  productDetails: PropTypes.object.isRequired,
  multiPackThreshold: PropTypes.number.isRequired,
  className: PropTypes.string.isRequired,
};

export default withStyles(PdpVmpDataLogger, styles);
