/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Dimensions, Share, View, TouchableOpacity } from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import moment from 'moment';
import {
  gestureHandlerRootHOC,
  TouchableOpacity as TouchableOpacityGH,
} from 'react-native-gesture-handler';
import Stars from 'react-native-stars';
import FastImage from '@stevenmasini/react-native-fast-image';
import isEqual from 'lodash/isEqual';
import get from 'lodash/get';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { PRODUCT_ADD_TO_BAG } from '@tcp/core/src/constants/reducer.constants';
import CustomIcon from '@tcp/core/src/components/common/atoms/Icon';
import { TIER1 } from '@tcp/core/src/constants/tiering.constants';
import ModalBox from 'react-native-modalbox';
import {
  constructPageName,
  isIOS,
  getValueFromAsyncStorage,
  setRecommendationsObj,
  isAndroid,
} from '@tcp/core/src/utils/utils.app';
import {
  formatProductsData,
  getAPIConfig,
  getDynamicBadgeQty,
  getBrand,
  isGymboree,
} from '@tcp/core/src/utils/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import PickupStoreModal from '@tcp/core/src/components/common/organisms/PickupStoreModal';
import AddedToBagContainer from '@tcp/core/src/components/features/CnC/AddedToBag';
import QuickViewModal from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.container';
import SocialProofMessage from '@tcp/core/src/components/features/browse/ProductDetail/molecules/SocialProofMessage/container/SocialProofMessage.container';
import capitalize from 'lodash/capitalize';
import RecommendationsAbstractor from '../../../../../services/abstractors/common/recommendations';
import ProductSummary from '../molecules/ProductSummary';
import ProductPickupContainer from '../../../../common/organisms/ProductPickup';
import {
  getImagesToDisplay,
  getMapSliceForColorProductId,
  getMapSliceForColor,
  getMapSliceForSizeSkuID,
  getPricesWithRange,
  getPrices,
  getMapColorList,
  getSkuId,
  getIsDefaultProductBadge,
} from '../../ProductListing/molecules/ProductList/utils/productsCommonUtils';
import {
  SBP_SIZE_MAPPING,
  SBP_CATEGORY_ID_MAPPING,
  SBP_ONE_SIZE,
} from '../../../../../../../mobileapp/src/components/features/shopByProfile/ShopByProfile.constants';
import names from '../../../../../constants/eventsName.constants';
import withStyles from '../../../../common/hoc/withStyles.native';
import {
  ProductDetailStyle,
  SizeDrawerUnderlay,
  ModalBoxCloseImage,
  ModalBoxBtn,
  AtbButton,
  FooterWrapper,
} from '../styles/ProductDetail.style.native';
import ImageCarousel from '../molecules/ImageCarousel';
import {
  Container,
  Margin,
  BadgeTitleContainer,
  PullDrawerWrapper,
  PullDrawerHeader,
  PullDrawerContent,
  ShareContainer,
  AnimationContainer,
  FavoriteContainer,
  PullDrawerProductSummary,
  StickyContainer,
  FavoriteCountContainer,
  FavoriteIconContainer,
  ProductSummaryContainer,
  ProductRatingDetailButton,
  ProductRatingDetailArrow,
  ModalBoxReviewBtn,
  ReviewsRatingsHeader,
  ReviewsRatingsHeaderTitle,
  ProductDetailStyleRedesign,
  ImageStyleContainer,
  SkeletonBodyContainer,
  SkeletonProdSummaryContainer,
  SkeletonSwatchesContainer,
  SkeletonSwatch,
  SkeletonMulPackContainer,
  IconWrapper,
  PromoBelowRecsContainer,
  SizeHeaderContainer,
  SizeToggleContainer,
  RowContainer,
  ContainerOfferNoRush,
  SocialProofWrapper,
} from '../styles/ProductDetailNew.style.native';
import { FullScreenImageCarousel } from '../../../../common/molecules/index.native';
import config from '../../../../common/molecules/ProductDetailImage/config';
import ProductDetailNewPropTypes from '../ProductDetailPropTypes';
import ProductAddToBagContainer from '../../../../common/molecules/ProductAddToBag';
import ProductDetailDescription from '../molecules/ProductDescription/views/ProductDescription.view.native';
import Recommendations from '../../../../../../../mobileapp/src/components/common/molecules/Recommendations';
import { ForterActionType, trackForterAction } from '../../../../../utils/forter.util';
import { EmptyView } from '../molecules/ImageCarousel/styles/ImageCarousel.style.native';
import Image from '../../../../common/atoms/Image';
import RelatedOutfits from '../molecules/RelatedOutfits/views/RelatedOutfits.view.native';
import SIZE_CHART_LINK_POSITIONS from '../../../../common/molecules/ProductAddToBag/container/ProductAddToBag.config';
import CustomButton from '../../../../common/atoms/ButtonRedesign';
import AlternateSizes from '../../../../common/molecules/ProductAddToBag/molecules/AlternateSizes';
import { BodyCopyWithSpacing, ViewWithSpacing } from '../../../../common/atoms/styledWrapper';
import { Anchor } from '../../../../common/atoms';
import ProductReviewsContainer from '../../ProductListing/molecules/ProductReviews/container/ProductReviews.container';
import LoaderSkelton from '../../../../common/molecules/LoaderSkelton';
import ProductRating from '../molecules/ProductRating/ProductRating.view.native';
import PLCCBanner from './PLCCBanner.view.native';
import PromoPDPBanners from '../../../../common/organisms/PromoPDPBanners';
import ToggleDeliveryMethod from '../molecules/ToogleSizeDrawer/ToggleDeliveryMethod.view.native';
import { BodyCopyWithTextTransform } from '../../../../common/atoms/styledWrapper/styledWrapper.native';
import { PillContainerForInventoryStatus } from '../../../../common/organisms/ProductPickup/styles/ProductPickup.style.native';
import {
  getOOBErrorBOPIS,
  getStatusFromBOPIS,
  getStoreInformation,
} from '../../../../common/organisms/ProductPickup/util';
import PickupPromotionBanner from '../../../../common/molecules/PickupPromotionBanner';

const drawerPullIndicator = require('../../../../../../../mobileapp/src/assets/images/drawer-pull-indicator.png');
const favoriteSolidNew = require('../../../../../../../mobileapp/src/assets/images/favorites_solid_3x.png');
const favoritesNew = require('../../../../../../../mobileapp/src/assets/images/favorites_3x.png');
const shareImage = require('../../../../../../../mobileapp/src/assets/images/icons_standard_share_3x.png');
const shareImageAndroid = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icons-standard-shareandroid.png');
const closeImage = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/close_icon_drawer.png');
const rightArrowImage = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icons-medium-left.png');
const starImg = require('../../../../../../../mobileapp/src/assets/images/star-new.png');
const leftArrowImage = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icons-standard-caret-large-left.png');
const downArrowImage = require('../../../../../../../mobileapp/src/assets/images/isNewRedesign/icon-carrot-down.png');
const warning = require('../../../../../../../mobileapp/src/assets/images/circle-warning-fill.png');
const markerIcon = require('../../../../../../../mobileapp/src/assets/images/store-location.png');

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

class ProductDetailNew extends React.Component {
  // on app candidate module will not be display [RWD-19103]
  isShowCandidateModule = false;

  constructor(props) {
    super(props);
    const { retrievedProductInfo, currentProduct, selectedColorProductId, formValues } = this.props;
    this.state = {
      showCarousel: false,
      currentColorEntry: getMapSliceForColorProductId(
        currentProduct?.colorFitsSizesMap || retrievedProductInfo?.colorFitsSizesMap,
        selectedColorProductId
      ),
      currentGiftCardValue: currentProduct?.offerPrice || retrievedProductInfo?.offerPrice,
      selectedColorProductId,
      showCompleteTheLook: false,
      size: '',
      fit: '',
      quantity: 1,
      expanded: false,
      activeIndex: 0,
      expandReviewFromAnchor: false,
      isAddToBagShow: true,
      isSizeDrawerOpen: false,
      showSize: false,
      sbpFormValues: null,
      accessorySize: '',
      favoritedCount: 0,
      generalProductId: '',
      showLoginModal: false,
      showdrawerHandle: true,
      formValues,
      isComingFromFullScreen: false,
      isComingFromPDPSwatch: false,
      isDrawerOpen: false,
      isDrawerClose: true,
      startAnimate: false,
      isFastImage: true,
      selectedSizeDrawerTab: 0,
      selectedDeliveryType: 'shipIt',
      noBossBopisInfo: false,
      drawerOpenStart: false,
    };
    this.currentScrollValue = 0;
    this.scrollPageToTarget = this.scrollPageToTarget.bind(this);
    this.isTransparentHeader = props.navigation && props.navigation.getParam('isTransparentHeader');
    this.isIOSApp = isIOS();
    const { currentColorEntry } = this.state;
    const colorName = currentColorEntry && currentColorEntry.color && currentColorEntry.color.name;
    this.formValues = {
      Fit: '',
      Size: '',
      color: colorName,
      Quantity: 1,
    };
    this.analyticsTriggered = false;
    this.noBossBopisInfoFromPickUpComponent = false;
    this.stickyContainerZindex = 100;

    this.barCodeRef = React.createRef();
    this.bottomSheet = React.createRef();

    this.isAndroidDevice = isAndroid();
    this.topIconPos = this.isAndroidDevice ? 0 : 36;
    const stickyButtonHeight = this.isAndroidDevice ? 20 : 30;
    const drawerHeaderMaxHeight = 64;
    const maxHeight = this.isAndroidDevice
      ? screenHeight - this.topIconPos - stickyButtonHeight - drawerHeaderMaxHeight - 10 + 2
      : screenHeight - this.topIconPos - stickyButtonHeight - drawerHeaderMaxHeight + 2;
    const drawerHeader = 30;
    const priceTitleRating = 125;
    const swatches = 85;
    this.minHeight = drawerHeader + priceTitleRating + swatches + stickyButtonHeight + 15;
    const midHeight = this.minHeight + 90;
    this.snapPoints = [this.minHeight - 32, midHeight - 32, maxHeight];
    const midPoint =
      (this.snapPoints[2] - this.snapPoints[1]) / (this.snapPoints[2] - this.snapPoints[0]);

    const iconHeight = 36;
    this.shareIconStartTop = this.topIconPos + iconHeight + 14;
    this.favIconStartTop = this.shareIconStartTop + iconHeight + 10;
    this.iconStartLeft = screenWidth - iconHeight - 22;
    this.shareIconEndLeft = this.iconStartLeft - iconHeight - 5;
    this.favIconEndLeft = this.shareIconEndLeft - iconHeight - 5;

    this.animateValue = new Animated.Value(1);
    this.headerScale = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [1, 0.5, 0.5],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
    this.drawerHandleOpacity = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [0, 1, 1],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
    this.headerBottom = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [-2, -18, -18],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
    this.shareIconPositionTop = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [this.topIconPos + 4, this.shareIconStartTop, this.shareIconStartTop],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
    this.shareIconPositionLeft = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [this.shareIconEndLeft, this.iconStartLeft, this.iconStartLeft],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
    this.favIconPositionTop = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [this.topIconPos + 7, this.favIconStartTop, this.favIconStartTop],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
    this.favIconPositionLeft = Animated.interpolate(this.animateValue, {
      inputRange: [0, midPoint, 1],
      outputRange: [this.favIconEndLeft, this.iconStartLeft, this.iconStartLeft],
      extrapolate: Animated.Extrapolate.CLAMP,
    });
  }

  static getDerivedStateFromProps(props, state) {
    const { currentProduct, retrievedProductInfo } = props;

    const { currentColorEntry, selectedColorProductId, favoritedCount } = state;

    const colorDetails = getMapSliceForColorProductId(
      currentProduct?.colorFitsSizesMap || retrievedProductInfo?.colorFitsSizesMap,
      selectedColorProductId
    );
    if (
      colorDetails?.favoritedCount !== currentColorEntry?.favoritedCount &&
      colorDetails?.color.name === currentColorEntry?.color.name
    ) {
      return {
        favoritedCount: colorDetails.favoritedCount,
        currentColorEntry: colorDetails || {},
      };
    }
    if (
      favoritedCount === 0 &&
      colorDetails.favoritedCount > 0 &&
      colorDetails?.color.name === currentColorEntry?.color.name
    ) {
      return {
        favoritedCount: colorDetails.favoritedCount,
        currentColorEntry: colorDetails || {},
      };
    }
    return null;
  }

  async componentDidMount() {
    const { navigation, currentProduct } = this.props;
    // Set the initial scroll value for header transparency to false.
    navigation.setParams({
      isScrolled: false,
    });
    const isSBP = navigation && navigation.getParam('isSBP');
    if (isSBP) {
      let accessorySize = '';
      let productSizeForPreselect = '';
      let productFitForPreselect = '';
      const profile = navigation.getParam('profile');
      accessorySize = this.isProductAnAccessory(currentProduct);
      if (!accessorySize) {
        const { sizeForPreselect, productFit } = await this.categoryIDMapping(
          currentProduct,
          profile
        );
        productSizeForPreselect = sizeForPreselect;
        productFitForPreselect = productFit;
      } else {
        productSizeForPreselect = accessorySize;
      }

      this.setState((prevState) => ({
        ...prevState,
        size: productSizeForPreselect,
        fit: productFitForPreselect,
        accessorySize,
      }));
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    }
    return true;
  }

  componentDidUpdate(prevProps, prevState) {
    const { navigation, currentProduct, trackPageLoad } = this.props;
    const { showCompleteTheLook } = this.state;
    const viaModule = (navigation && navigation.getParam('viaModule')) || '';
    if (
      prevState.showCompleteTheLook !== showCompleteTheLook &&
      prevState.showCompleteTheLook === true &&
      viaModule !== 'mbox_Recommendation'
    ) {
      const productsFormatted = formatProductsData(currentProduct, null, navigation);
      const analyticsData = this.getAnalyticData(navigation);
      trackPageLoad(analyticsData, {
        products: productsFormatted,
        customEvents: ['event74', 'event76', 'event95'],
      });
    }

    if (prevProps.currentProduct?.productId !== currentProduct?.productId) {
      this.setShowCompleteTheLook(false);
      if (currentProduct?.productId) {
        const { productId } = currentProduct;
        this.triggerAnalytics(currentProduct, productId);
        this.analyticsTriggered = true;
      }
      this.bottomSheet.current.snapTo(0);
      this.updateFormValuesOnProductChange(
        prevProps.currentProduct?.productId,
        currentProduct?.productId
      );
    }

    if (!this.analyticsTriggered && currentProduct?.productId) {
      this.triggerAnalytics(currentProduct, currentProduct?.productId);
      this.analyticsTriggered = true;
    }
  }

  noBossBopisInfoFunc = (noBossBopisInfo) => {
    this.setState({ noBossBopisInfo });
    this.noBossBopisInfoFromPickUpComponent = noBossBopisInfo;
  };

  // eslint-disable-next-line complexity
  /* eslint-disable */
  triggerAnalytics = (currentProduct, selectedColorProductId) => {
    const {
      navigation,
      breadCrumbs,
      trackPageLoad,
      isExternalCampaignFired,
      setExternalCampaignFired,
      formValues,
    } = this.props;
    let productsFormatted =
      currentProduct &&
      formatProductsData(currentProduct, null, navigation, selectedColorProductId);
    const clickOrigin = (navigation && navigation.getParam('clickOrigin')) || '';
    const viaModule = (navigation && navigation.getParam('viaModule')) || '';
    const navigateType = (navigation && navigation.getParam('navigateType')) || '';
    const icidParam = (navigation && navigation.getParam('icidParam')) || '';
    const externalCampaignId = navigation.getParam('externalCampaignId');
    const omMID = navigation.getParam('omMID');
    const omRID = navigation.getParam('omRID');
    const viaModuleName = (
      (navigation && navigation.getParam('viaModuleName')) ||
      ''
    ).toLowerCase();
    const portalValue = navigation && navigation.getParam('portalValue');
    const NON_INTERNAL_SEARCH = 'non-internal search';
    const NON_BROWSE = 'non-browse';
    const NON_INTERNAL_CAMPAIGN = 'non-internal campaign';

    const recIdentifier = RecommendationsAbstractor.handleWhichRecommendationClicked(
      portalValue,
      clickOrigin
    );
    productsFormatted = productsFormatted.map((product) => {
      const productWithGender = { ...product };
      productWithGender.gender = this.getGender(breadCrumbs);

      // delete these two to suppress eVar20, event50, eVar21 in the product string
      delete productWithGender.recsProductId;
      delete productWithGender.recsPageType;

      return {
        ...productWithGender,
        ...this.getRecsObject(product, viaModule, recIdentifier, clickOrigin),
      };
    });
    if (productsFormatted && productsFormatted.length) {
      const { color } = formValues || {};
      productsFormatted[0].color = color;
    }
    const customEvents =
      navigateType === 'direct'
        ? ['prodView', 'event1', 'internalCampaignImpressions_e81', 'event100', 'event20']
        : ['prodView', 'event1', 'internalCampaignImpressions_e81'];
    if (externalCampaignId) {
      customEvents.push('event18');
      setExternalCampaignFired(true);
    }
    if (isExternalCampaignFired && !externalCampaignId) {
      customEvents.push('event19');
      setExternalCampaignFired(false);
    }
    let customData = {
      products: productsFormatted,
      customEvents,
    };
    const analyticsData = this.getAnalyticData(navigation);

    if (icidParam) {
      customData = {
        products: productsFormatted,
        internalCampaignId: icidParam,
        searchText: NON_INTERNAL_SEARCH,
        pageNavigationText: NON_BROWSE,
        originType: 'internal campaign',
        listingCount: NON_INTERNAL_SEARCH,
        searchType: NON_INTERNAL_SEARCH,
      };
      if (viaModule === 'bag_carousel_module') {
        customData.customEvents = ['internalCampaignImpressions_e81'];
      }
    } else if (viaModule === 'mbox_Recommendation') {
      productsFormatted[0].recsType = recIdentifier ? `monetate-${recIdentifier}` : '';
      customData = {
        products: productsFormatted,
        internalCampaignId: NON_INTERNAL_CAMPAIGN,
        searchText: NON_INTERNAL_SEARCH,
        searchType: NON_INTERNAL_SEARCH,

        customEvents: [
          'prodView',
          'Product_Views_Custom_e1',
          'internalCampaignImpressions_e81',
          names.eVarPropEvent.event50,
        ],
        originType: `cross-sell:${viaModuleName}`,
        pageNavigationText: NON_BROWSE,
        listingCount: NON_INTERNAL_SEARCH,
      };
      analyticsData.supressProps = ['eVar91'];
    } else if (navigateType === 'direct') {
      // PDP item search
      customData = {
        internalCampaignId: NON_INTERNAL_CAMPAIGN,
        pageNavigationText: NON_BROWSE,
      };
    } else {
      analyticsData.supressProps = [names.eVarPropEvent.eVar19, 'eVar97'];
    }

    trackPageLoad(analyticsData, {
      ...customData,
      ...(externalCampaignId && {
        externalCampaignId,
      }),
      ...(omMID && { omMID }),
      ...(omRID && { omRID }),
    });
  };
  /* eslint-enable */

  componentWillUnmount = () => {
    const { clearAddToBagError } = this.props;
    clearAddToBagError();
  };

  updateFormValuesOnProductChange = (previousProductId, currentProductId) => {
    const { updateSizeValue } = this.props;
    updateSizeValue('', currentProductId);
    updateSizeValue('', previousProductId);
  };

  categoryIDMapping = async (currentProduct, profile) => {
    const categoryIDMap = await getValueFromAsyncStorage(SBP_CATEGORY_ID_MAPPING);
    const parsedCategoryIDMap = JSON.parse(categoryIDMap);
    const {
      json: { SBPCategoryMapping },
    } = parsedCategoryIDMap;
    try {
      const { gender } = profile;
      const productCatID = this.getProductCategoryID(currentProduct);
      const catIdMapping = SBPCategoryMapping.find(
        (mapObj) =>
          mapObj.department.toLowerCase() === gender.toLowerCase() &&
          mapObj.categoryIds.split(',').some((element) => productCatID.indexOf(element) >= 0)
      );

      if (typeof catIdMapping !== 'undefined') {
        const { category } = catIdMapping;
        const sizesForPreselect = this.extractSizesFromSBPProfile(category, profile);
        const { profileSize, profileFit } = sizesForPreselect;
        return await this.getSizeMapping(
          currentProduct,
          profileSize,
          profileFit,
          category,
          profile
        );
      }
    } catch (error) {
      logger.error(error);
    }
    return null;
  };

  getProductCategoryID = (currentProduct) => {
    const listCategoryIds = [];
    if (currentProduct && currentProduct.categoryPathMap) {
      currentProduct.categoryPathMap.forEach((catMap) => {
        const value = catMap.split('>')[1].split('|')[0];
        listCategoryIds.push(value);
      });
    }
    return listCategoryIds;
  };

  extractSizesFromSBPProfile = (categoryName, profile) => {
    const { size } = profile;
    const parsedSize = JSON.parse(size);
    const sizesForPreselect = {};
    Object.entries(parsedSize).forEach(([key, value]) => {
      if (key.toLowerCase() === categoryName.toLowerCase()) {
        sizesForPreselect.profileSize = value.size;
        sizesForPreselect.profileFit = value.fit ? value.fit : '';
      }
    });
    return sizesForPreselect;
  };

  getSizeMapping = async (currentProduct, sizesForPreselect, profileFit, category, profile) => {
    try {
      const { colorFitsSizesMap, generalProductId } = currentProduct;
      const { gender } = profile;
      let productObj;
      let productFit;
      let sizeForPreselect = '';
      const currentProductColorFitsSizesMap = colorFitsSizesMap.find(
        (element) => element.colorDisplayId === generalProductId
      );
      const sizeMappingSBP = await getValueFromAsyncStorage(SBP_SIZE_MAPPING);
      const parsedSizeMappingSBP = JSON.parse(sizeMappingSBP);
      const profileSize = sizesForPreselect.find((size) => {
        const [fitProduct] = currentProductColorFitsSizesMap.fits;
        const sizeObj = parsedSizeMappingSBP[gender].find(
          (sizeMapObj) =>
            sizeMapObj.size === size && sizeMapObj.category.toLowerCase() === category.toLowerCase()
        );
        const { all_sizes: everySize } = sizeObj;
        return everySize.find((singleSize) =>
          fitProduct.sizes.some((el) => el.sizeName === singleSize && el.maxAvailable > 0)
        );
      });

      const profileSizeObj = parsedSizeMappingSBP[gender].find(
        (element) =>
          element.size === profileSize && element.category.toLowerCase() === category.toLowerCase()
      );

      if (currentProductColorFitsSizesMap.fits.length === 1) {
        const [prodFit] = currentProductColorFitsSizesMap.fits;
        productObj = prodFit;
        productFit = '';
      } else {
        productObj = currentProductColorFitsSizesMap.fits.find(
          (element) => element.fitName.toLowerCase() === profileFit.toLowerCase()
        );
        productFit = profileFit;
      }

      const { all_sizes: allSizes } = profileSizeObj;
      allSizes.forEach((size) => {
        productObj.sizes.forEach(({ sizeName, maxAvailable }) => {
          if (sizeName === size && maxAvailable > 0) sizeForPreselect = sizeName;
        });
      });

      return { sizeForPreselect, productFit };
    } catch (error) {
      logger.error(error);
    }
    return null;
  };

  isProductAnAccessory = (currentProduct) => {
    const { colorFitsSizesMap } = currentProduct;
    const findAccessorySize =
      colorFitsSizesMap &&
      colorFitsSizesMap[0].fits &&
      colorFitsSizesMap[0].fits[0].sizes.find(
        (element) => element.sizeName.toLowerCase() === SBP_ONE_SIZE
      );
    if (findAccessorySize) return `${findAccessorySize.sizeName}`;
    return '';
  };

  getRecsObject = (product, viaModule, recIdentifier, clickOrigin) => {
    if (viaModule === 'mbox_Recommendation') {
      const recObj = {
        recsType: recIdentifier ? `monetate-${recIdentifier}` : '',
        recsPageType: clickOrigin,
        recsProductId: product.colorId,
      };
      setRecommendationsObj(recObj);
      return recObj;
    }
    return {};
  };

  // Return the gender based on breadcrumb
  getGender = (breadcrumbs) => {
    const gender =
      breadcrumbs && breadcrumbs[0] && breadcrumbs[0].displayName
        ? breadcrumbs[0].displayName.toLowerCase()
        : '';
    return gender === 'home' ? '' : gender;
  };

  getAnalyticData = (navigation) => {
    return {
      currentScreen: 'ProductDetail',
      pageData: {
        pageName: constructPageName(navigation),
        pageType: 'product',
        pageSection: 'product',
        pageSubSection: 'product',
      },
    };
  };

  // Outfit Analytics click
  formatOutfitProductsData = (products) => {
    return products.map((tile) => {
      return {
        id: tile.id,
        stylitics: 'true',
        uniqueOutfitId: tile.id,
        outfitId: tile.id,
      };
    });
  };

  triggerStylyticsAnalytics = (items) => {
    const { currentProduct, trackClickAction, setClickAnalyticsDataAction } = this.props;

    const { name, ratingsProductId } = currentProduct;
    const nameParam = name && name.toLowerCase();
    const pageName = `product:${ratingsProductId}:${nameParam}`;
    const productsFormatted = this.formatOutfitProductsData(items);
    const mappingName = 'outfitStylytics';
    const module = 'browse';
    const trackPayload = {
      pageType: 'product',
      pageName,
      pageSection: 'product',
      pageSubSection: 'product',
      products: productsFormatted,
      name: 'outfitStylytics',
    };
    if (productsFormatted) {
      setClickAnalyticsDataAction(trackPayload);
      trackClickAction({ name: mappingName, module });
    }
  };

  onImageClick = () => {
    const { showCarousel } = this.state;
    this.setState({ showCarousel: !showCarousel });
  };

  callbackExpandReviewSection = () => {
    this.setState({ expandReviewFromAnchor: true });
  };

  getCurrentSelectImageUrls = (color) => {
    const {
      currentProduct: { colorFitsSizesMap },
      getCurrentColorEntry,
    } = this.props;
    const currentColorEntry = getMapSliceForColor(colorFitsSizesMap, color);
    this.setState(
      {
        currentColorEntry,
      },
      () => {
        getCurrentColorEntry(currentColorEntry);
      }
    );
  };

  comingFromFullScreen = () => {
    this.setState({ isComingFromFullScreen: true, isComingFromPDPSwatch: false });
  };

  renderCarousel = (imageUrls, isNewReDesignEnabled, colorFitsSizesMap) => {
    const { showCarousel, activeIndex, currentColorEntry, isComingFromPDPSwatch } = this.state;
    const {
      currentProduct,
      isDynamicBadgeEnabled,
      retrievedProductInfo,
      formValues,
      fireHapticFeedback,
      primaryBrand,
      alternateBrand,
      isPDPCalledFromRecommendation,
    } = this.props;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      currentProduct?.TCPStyleType || retrievedProductInfo?.tcpStyleType,
      currentProduct?.TCPStyleQTY || retrievedProductInfo?.tcpStyleQty,
      isDynamicBadgeEnabled
    );
    if (!showCarousel) return null;
    return (
      <FullScreenImageCarousel
        zoomImages={true}
        imageUrls={imageUrls}
        parentActiveIndex={activeIndex}
        onCloseModal={this.onImageClick}
        dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
        isNewReDesignFullScreenImageCarousel={isNewReDesignEnabled}
        colorFitsSizesMap={colorFitsSizesMap}
        currentColor={currentColorEntry}
        getCurrentSelectedUrls={this.onChangeColor}
        comingFromFullScreen={this.comingFromFullScreen}
        isComingFromPDPSwatch={isComingFromPDPSwatch}
        formValues={formValues}
        fireHapticFeedback={fireHapticFeedback}
        primaryBrand={primaryBrand}
        alternateBrand={!isPDPCalledFromRecommendation && alternateBrand}
      />
    );
  };

  callbackIndex = (fullScreenImageIndex) => {
    this.setState({ activeIndex: fullScreenImageIndex });
  };

  setShowCompleteTheLook = (value) => {
    if (typeof value === 'boolean') {
      this.setState({ showCompleteTheLook: value });
    }
  };

  scrollPageToTarget = (param) => {
    return param;
  };

  renderTopBadge = (badge1Value) => {
    if (badge1Value !== '') {
      return (
        <BodyCopy
          dataLocator="pdp_product_badges"
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="regular"
          color="gray.900"
          text={badge1Value}
          textAlign="left"
        />
      );
    }
    return null;
  };

  renderTopTitle = (titleValue) => {
    if (titleValue !== '') {
      const { formValues } = this.props;
      const selectedColor = formValues?.color;
      return (
        <BodyCopy
          dataLocator="pdp_product_titles"
          fontFamily="secondary"
          fontSize="fs14"
          fontWeight="extrabold"
          color="gray.900"
          text={selectedColor && `${titleValue} - ${capitalize(selectedColor)}`}
          numberOfLines={2}
          textAlign="left"
        />
      );
    }
    return null;
  };

  renderBadgeAndTitleContainer = (name, badge1Value) => {
    const { isStyleWith } = this.props;
    return (
      <BadgeTitleContainer>
        {!isStyleWith && !!badge1Value && this.renderTopBadge(badge1Value)}
        {!isStyleWith && this.renderTopTitle(name)}
      </BadgeTitleContainer>
    );
  };

  hideLoginModal = () => {
    this.setState((state) => ({
      showLoginModal: !state.showLoginModal,
    }));
  };

  getCurrentProduct = () => {
    const { currentProduct, retrievedProductInfo } = this.props;
    if (currentProduct.productId) {
      return { ...currentProduct };
    }
    if (retrievedProductInfo) {
      return {
        ...retrievedProductInfo,
        TCPStyleType: retrievedProductInfo.tcpStyleType,
        TCPStyleQTY: retrievedProductInfo.tcpStyleQty,
      };
    }
    return null;
  };

  isFastImageDisableForRecommendations = (navigation) => {
    const viaModule = (navigation && navigation.getParam('viaModule')) || '';
    const clickOrigin = (navigation && navigation.getParam('clickOrigin')) || '';
    if (
      viaModule === 'mbox_Recommendation' &&
      (clickOrigin === 'pdp' || clickOrigin === 'added_to_bag')
    ) {
      return true;
    }
    return false;
  };

  renderImageCarousel = () => {
    const {
      currentProduct,
      navigation,
      onAddItemToFavorites,
      isLoggedIn,
      AddToFavoriteErrorMsg,
      removeAddToFavoritesErrorMsg,
      isKeepAliveEnabled,
      outOfStockLabels,
      accessibilityLabels,
      isOnModelAbTestPdp,
      defaultWishListFromState,
      checkForOOSForVariant,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      activeWishListProducts,
      activeWishListId,
      isDynamicBadgeEnabled,
      isNewReDesignEnabled,
      pdpLabels,
      stylisticsProductTabList,
      formValues,
      primaryBrand,
      alternateBrand,
      isPDPCalledFromRecommendation,
    } = this.props;

    const product = this.getCurrentProduct();
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      product?.TCPStyleType,
      product?.TCPStyleQTY,
      isDynamicBadgeEnabled
    );

    const { currentColorEntry, size, showLoginModal, showCompleteTheLook, isFastImage } =
      this.state;
    let isFastImg = isFastImage;
    if (this.isFastImageDisableForRecommendations(navigation)) {
      isFastImg = false;
    }
    let imageUrls = [];
    let skuId = null;

    const keepAlive = isKeepAliveEnabled && currentColorEntry.miscInfo.keepAlive;
    const getFormName = formValues && formValues.color;
    let updatedColorEntry = getMapSliceForColor(product?.colorFitsSizesMap, getFormName);
    if (!updatedColorEntry) {
      updatedColorEntry = getMapSliceForColorProductId(
        product?.colorFitsSizesMap,
        product?.productId
      );
    }
    if (product?.colorFitsSizesMap && product?.imagesByColor) {
      imageUrls = getImagesToDisplay({
        imagesByColor: product?.imagesByColor,
        curentColorEntry: !updatedColorEntry ? currentColorEntry : updatedColorEntry,
        isAbTestActive: isOnModelAbTestPdp,
        isFullSet: true,
      });
    }

    if (size) {
      skuId = getMapSliceForSizeSkuID(currentColorEntry, size);
    }
    if (!imageUrls.length) return null;

    return (
      product && (
        <ImageCarousel
          isGiftCard={product?.isGiftCard}
          imageUrls={imageUrls}
          onAddItemToFavorites={onAddItemToFavorites}
          isLoggedIn={isLoggedIn}
          currentProduct={currentProduct}
          onImageClick={this.onImageClick}
          AddToFavoriteErrorMsg={AddToFavoriteErrorMsg}
          removeAddToFavoritesErrorMsg={removeAddToFavoritesErrorMsg}
          currentColorEntry={currentColorEntry}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          skuId={skuId}
          accessibilityLabels={accessibilityLabels}
          imgConfig={config.IMG_DATA_CLIENT.imgConfig[0]}
          videoConfig={config.VID_DATA.imgConfig[0]}
          formName={PRODUCT_ADD_TO_BAG}
          parentActiveIndex={this.callbackIndex}
          navigation={navigation}
          defaultWishListFromState={defaultWishListFromState}
          isPDPImage
          isBrierleyPromoEnabled={isBrierleyPromoEnabled}
          checkForOOSForVariant={checkForOOSForVariant}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          activeWishListProducts={activeWishListProducts}
          activeWishListId={activeWishListId}
          dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
          tcpStyleType={product?.TCPStyleType}
          isNewReDesignEnabled={isNewReDesignEnabled}
          showLoginModal={showLoginModal}
          hideLoginModal={this.hideLoginModal}
          showCompleteTheLook={showCompleteTheLook}
          pdpLabels={pdpLabels}
          stylisticsProductTabList={stylisticsProductTabList}
          bottomSheetHeight={this.minHeight}
          isFastImage={isFastImg}
          fromPage={Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP}
          primaryBrand={primaryBrand}
          alternateBrand={!isPDPCalledFromRecommendation && alternateBrand}
        />
      )
    );
  };

  renderFavoriteIcon = () => {
    const {
      isBundleProduct,
      currentProduct,
      defaultWishListFromState,
      activeWishListProducts = [],
      isFavorite: favorite,
      navigation,
    } = this.props;
    const { currentColorEntry } = this.state;
    const { miscInfo } = currentColorEntry;
    const isSBP = navigation.getParam('isSBP');
    let { isFavorite = false } = currentColorEntry;
    if (isSBP) {
      isFavorite = favorite;
      miscInfo.isInDefaultWishlist = false;
    }
    const { generalProductId } = currentProduct;
    let wishList = {};
    if (isSBP && activeWishListProducts) {
      activeWishListProducts.forEach((product) => {
        wishList[product.skuInfo.colorProductId] = { isInDefaultWishlist: false };
      });
    } else wishList = defaultWishListFromState;
    if (!isBundleProduct) {
      return this.renderFavIcon(isFavorite, generalProductId, defaultWishListFromState, wishList);
    }
    return <EmptyView />;
  };

  getDefaultPositionsForFavIcon = () => {
    const { isDrawerOpen, isDrawerClose } = this.state;
    return isDrawerOpen || !isDrawerClose
      ? { left: this.favIconEndLeft, top: this.topIconPos + 7 }
      : { left: this.iconStartLeft, top: this.favIconStartTop };
  };

  renderFavIcon = (isFavorite, generalProductId, defaultWishListFromState, wishList) => {
    const { activeWishListId, isNewReDesignEnabled, navigation } = this.props;
    const { startAnimate, favoritedCount } = this.state;
    const isSBP = navigation.getParam('isSBP');
    const AnimatedFavIconContainer = Animated.createAnimatedComponent(FavoriteContainer);
    return (
      <AnimatedFavIconContainer
        isSBP={isSBP}
        top={startAnimate && this.favIconPositionTop}
        left={startAnimate && this.favIconPositionLeft}
        defaultPositions={this.getDefaultPositionsForFavIcon()}
      >
        <FavoriteIconContainer>
          {(isFavorite === true && (defaultWishListFromState.isInDefaultWishlist || isSBP)) ||
          (wishList && wishList[generalProductId])
            ? this.renderSelectedHeartIcon(generalProductId)
            : this.renderUnselectedHeartIcon(generalProductId, activeWishListId)}
        </FavoriteIconContainer>
        {!!favoritedCount && (
          <FavoriteCountContainer>
            {this.renderFavoriteCount(isSBP, isNewReDesignEnabled)}
          </FavoriteCountContainer>
        )}
      </AnimatedFavIconContainer>
    );
  };

  renderFavoriteCount = (isSBP, isNewReDesignEnabled) => {
    const { favoritedCount } = this.state;
    let count = null;
    let margin = '0 0 0 8px';
    let color = 'gray.600';
    count = favoritedCount;
    if (isNewReDesignEnabled) {
      color = 'gray.900';
      margin = '0px';
      count = favoritedCount === 0 ? null : favoritedCount;
    }
    /* SBP MERGE JIRA SBP-902 STYLING */
    return (
      <BodyCopy
        dataLocator="pdp_favorite_icon_count"
        margin={isSBP ? '0 0 0 2px' : margin}
        mobileFontFamily={isSBP ? 'Nunito-Regular' : 'secondary'}
        fontSize="fs10"
        fontWeight="regular"
        color={isSBP ? '#1a1a1a' : color}
        text={count}
      />
    );
  };

  renderSelectedHeartIcon = (generalProductId) => {
    const imageStyle = {
      height: 24,
      width: 24,
    };
    return (
      <TouchableOpacityGH
        accessibilityRole="button"
        accessibilityLabel="favorite"
        accessibilityState={{ selected: true }}
        onPress={() => {
          this.onFavoriteRemove(generalProductId);
        }}
      >
        <FastImage
          source={favoriteSolidNew}
          dataLocator="pdp_favorite_icon"
          style={imageStyle}
          resizeMode={FastImage?.resizeMode?.contain}
        />
      </TouchableOpacityGH>
    );
  };

  onFavoriteRemove = (generalProductId) => {
    const { deleteFavItemInProgressFlag } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const { defaultWishListFromState, onSetLastDeletedItemIdAction } = this.props;
    const { favoritedCount } = this.state;
    if (defaultWishListFromState && defaultWishListFromState[generalProductId]) {
      const { externalIdentifier, giftListItemID } =
        defaultWishListFromState && defaultWishListFromState[generalProductId];
      onSetLastDeletedItemIdAction({
        externalId: externalIdentifier,
        itemId: giftListItemID,
        notFromFavoritePage: true,
      });
      if (favoritedCount > 0) this.setState({ favoritedCount: favoritedCount - 1 });
    }
  };

  renderUnselectedHeartIcon = (generalProductId, activeWishListId) => {
    const imageStyle = {
      height: 24,
      width: 24,
    };

    return (
      <TouchableOpacityGH
        accessibilityRole="button"
        accessibilityLabel="set as favorite"
        accessibilityState={{ selected: false }}
        onPress={() => {
          this.onFavorite(generalProductId, activeWishListId);
        }}
      >
        <FastImage
          source={favoritesNew}
          dataLocator="pdp_favorite_icon"
          style={imageStyle}
          resizeMode={FastImage?.resizeMode?.contain}
        />
      </TouchableOpacityGH>
    );
  };

  onFavorite = (generalProductId, activeWishListId) => {
    const { deleteFavItemInProgressFlag } = this.props;
    if (deleteFavItemInProgressFlag) {
      return;
    }
    const { isLoggedIn, onAddItemToFavorites, currentProduct, navigation } = this.props;

    let skuId = null;
    const { currentColorEntry, size, favoritedCount } = this.state;
    if (size) {
      skuId = getMapSliceForSizeSkuID(currentColorEntry, size);
    }
    const { colorProductId } = currentColorEntry;
    const isSBP = navigation.getParam('isSBP');

    if (!isLoggedIn) {
      this.setState({ generalProductId });
      this.setState({ showLoginModal: true });
    } else {
      onAddItemToFavorites({
        colorProductId: generalProductId,
        productSkuId: (skuId && skuId.skuId) || null,
        pdpColorProductId: colorProductId,
        formName: PRODUCT_ADD_TO_BAG,
        page: 'PDP',
        products: currentProduct,
        activeWishListId: isSBP ? activeWishListId : undefined,
        loading: isSBP,
      });
      this.setState({ favoritedCount: favoritedCount + 1 });
    }
  };

  getItemColor = () => {
    const { currentProduct, formValues } = this.props;
    const { colorFitsSizesMap } = currentProduct;
    const getFormName = formValues && formValues.color;
    return getMapSliceForColor(colorFitsSizesMap, getFormName);
  };

  renderProductAlternateSizes = () => {
    const {
      className,
      navigation,
      plpLabels,
      hideAlternateSizes,
      sbpLabels,
      isPickup,
      isStyleWith,
      alternateSizes,
      isOutfitPage,
      hideQuatity,
      isNewReDesignEnabled,
    } = this.props;
    const sizeAvailable = plpLabels && plpLabels.sizeAvailable ? plpLabels.sizeAvailable : '';
    const visibleAlternateSizes =
      !hideAlternateSizes && alternateSizes && Object.keys(alternateSizes).length > 0;
    const quantityCheck =
      !(isOutfitPage || hideQuatity) && !isPickup && !isStyleWith && visibleAlternateSizes;

    const isSBP = navigation && navigation.getParam('isSBP');
    return (
      quantityCheck && (
        <AlternateSizes
          title={sizeAvailable}
          buttonsList={alternateSizes}
          className={className}
          navigation={navigation}
          isNewReDesignEnabled={isNewReDesignEnabled}
          sbpLabels={sbpLabels}
          isSBP={isSBP}
        />
      )
    );
  };

  renderProductDescription = (itemColorObj) => {
    const {
      currentProduct,
      shortDescription,
      itemPartNumber,
      longDescription,
      pdpLabels,
      accessibilityLabels,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      defaultWishListFromState,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      isNewReDesignEnabled,
      primaryBrand,
      alternateBrand,
      isPDPCalledFromRecommendation,
      getIsPDPCalledViaRecommendation,
    } = this.props;
    if (isPDPCalledFromRecommendation)
      getIsPDPCalledViaRecommendation({
        isPDPCalledFromRecommendationFlag: isPDPCalledFromRecommendation,
      });
    const { TCPStyleType, TCPStyleQTY } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );

    return (
      !currentProduct?.isGiftCard && (
        <Margin margins="10px 0px 50px 0px">
          <ProductDetailDescription
            isBrierleyPromoEnabled={isBrierleyPromoEnabled}
            shortDescription={shortDescription}
            itemPartNumber={itemPartNumber}
            longDescription={longDescription}
            isShowMore={false}
            pdpLabels={pdpLabels}
            scrollToAccordionBottom={this.scrollToAccordionBottom}
            accessibilityLabels={accessibilityLabels}
            backgroundColor="white"
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            defaultWishListFromState={defaultWishListFromState}
            color={itemColorObj && itemColorObj.color && itemColorObj.color.name}
            isNewReDesignDescription={isNewReDesignEnabled}
            productInfo={currentProduct}
            deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
            dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
            primaryBrand={primaryBrand}
            alternateBrand={!isPDPCalledFromRecommendation && alternateBrand}
          />
        </Margin>
      )
    );
  };

  renderRecommendationVariation = ({
    isPdp1andPdp2Enabled,
    colorNameAvailable,
    pdpLabels,
    showPriceRange,
    recommendationAttributes,
    pdpDataInformation,
    deviceTier,
    quickViewLoader,
    quickViewProductId,
  }) => {
    const { navigation } = this.props;
    const isSBP = navigation.getParam('isSBP');
    return isPdp1andPdp2Enabled && colorNameAvailable ? (
      <>
        {deviceTier === TIER1 && (
          <Margin margins="15px 0 -5px 0px">
            <Recommendations
              isSBP={isSBP}
              isRecentlyViewed
              showPriceRange={showPriceRange}
              {...recommendationAttributes}
              headerLabel={pdpLabels.recentlyViewed}
              portalValue={Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
              sequence="2"
              pdpDataInformation={pdpDataInformation}
              isCardTypeTiles
              productImageWidth={130}
              productImageHeight={161}
              paddings="0px"
              margins="5px 6px 18px"
              quickViewLoader={quickViewLoader}
              quickViewProductId={quickViewProductId}
            />
          </Margin>
        )}

        <Margin margins="14px 0 0px 0px">
          <Recommendations
            isSBP={isSBP}
            headerLabel={pdpLabels.youMayAlsoLike}
            showPriceRange={showPriceRange}
            {...recommendationAttributes}
            sequence="1"
            paddings="0px"
            margins="5px 6px 18px"
            pdpDataInformation={pdpDataInformation}
            isCardTypeTiles
            productImageWidth={130}
            productImageHeight={161}
            quickViewLoader={quickViewLoader}
            quickViewProductId={quickViewProductId}
          />
        </Margin>
      </>
    ) : null;
  };

  getFormName = (formValues) => {
    return formValues && formValues.color;
  };

  closeSizeDrawer = () => {
    this.setState((prevState) => ({
      ...prevState,
      showSize: false,
    }));
  };

  closeRatingDrawer = () => {
    this.setState((prevState) => ({
      ...prevState,
      expandReviewFromAnchor: false,
    }));
  };

  goToPDP = (item) => {
    const { navigation, alternateSizes } = this.props;
    navigation.navigate('ProductDetail', {
      pdpUrl: alternateSizes[item],
      reset: true,
    });
  };

  analyticsOnSizeDrawerToggle = (customEvents, name) => {
    const { setClickAnalyticsDataAction, trackClickAction } = this.props;
    setClickAnalyticsDataAction({
      customEvents: [customEvents],
      name,
      clickEvent: true,
      Shipping_Preferences_Location_v137: 'pdp_size_drawer',
    });
    trackClickAction({ name: 'PDP_Boss_Bopis_Size_drawer' });
  };

  clearSizeOnDisableState = (index) => {
    const { updateSizeValue, isSelectedSizeDisabled, updateSizeStatus } = this.props;
    if (index === 0 && isSelectedSizeDisabled) {
      updateSizeValue('');
      updateSizeStatus(null);
    }
  };

  onTabChange = (index) => {
    const { resetBopisInventoryData } = this.props;
    let eventName;
    let customEvents;
    let methodType;
    if (index === 0) {
      eventName = 'App_Ship_To_Home_Click_e165';
      customEvents = 'event165';
      methodType = 'shipIt';
    } else if (index === 1) {
      eventName = 'App_Pickup_Click_e166';
      customEvents = 'event166';
      methodType = 'pickItUp';
    } else if (index === 2) {
      eventName = 'App_No_Rush_Pickup_Click_e167';
      customEvents = 'event167';
      methodType = 'noRushPickup';
    }
    resetBopisInventoryData();
    this.clearSizeOnDisableState(index);
    this.analyticsOnSizeDrawerToggle(customEvents, eventName);
    this.setState({ selectedSizeDrawerTab: index, selectedDeliveryType: methodType });
  };

  getNoRushPickupDate = () => {
    const { userDefaultStore } = this.props;
    if (!userDefaultStore) {
      return '';
    }
    const { storeBossInfo } = userDefaultStore;
    const startMonth = moment(storeBossInfo?.startDate).format('MMMM');
    const endMonth = moment(storeBossInfo?.endDate).format('MMMM');
    const startDate = moment(storeBossInfo?.startDate).format('D');
    const endDate = moment(storeBossInfo?.endDate).format('D');
    if (startMonth === endMonth) {
      return `${startMonth} ${startDate}-${endDate}`;
    }
    return `${startMonth} ${startDate}-${endMonth} ${endDate}`;
  };

  renderShowChangeStore = () => {
    const { userDefaultStore, pickupLabels, userNearByStore } = this.props;
    const storeInformation = getStoreInformation(userDefaultStore, userNearByStore);
    if (!storeInformation) {
      return null;
    }
    const { storeName, city, state } = storeInformation;
    return (
      <RowContainer margins="0 0 10px 0">
        <Image source={markerIcon} height={22} width={20} dataLocator="pdp_store_map_icon" />
        <BodyCopyWithTextTransform
          margin="0 4px 0 4px"
          dataLocator="pdp_store_name_value"
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="black"
          color="gray.900"
          text={storeName || ''}
        />
        <BodyCopy
          margin="0 4px 0 4px"
          dataLocator="pdp_store_city_label"
          fontFamily="secondary"
          fontSize="fs12"
          fontWeight="regular"
          color="gray.900"
          text={`${capitalize(city)}, ${state}`}
        />
        <Anchor
          fontSizeVariation="medium"
          anchorVariation="custom"
          colorName="gray.900"
          underline
          href="#"
          locator="pdp_change_store_label"
          className="details-link"
          onPress={this.handlePickupModalClick}
          text={pickupLabels.lbl_Product_pickup_CHANGE_STORE_DM}
        />
      </RowContainer>
    );
  };

  pickDateContainer = () => {
    const { pickupLabels } = this.props;
    const { selectedSizeDrawerTab } = this.state;
    return selectedSizeDrawerTab === 2 ? (
      <RowContainer margins="0 0 10px 0px">
        <BodyCopy
          dataLocator="pdp_pick_label"
          fontFamily="secondary"
          fontSize="fs14"
          fontWeight="regular"
          color="gray.900"
          text={pickupLabels.lbl_Product_pickup_PICKUP}
        />
        <BodyCopy
          margin="0 0 0 4px"
          dataLocator="pdp_pick_date_label"
          fontFamily="secondary"
          fontSize="fs14"
          fontWeight="regular"
          color="green.500"
          text={this.getNoRushPickupDate() || ''}
        />
      </RowContainer>
    ) : null;
  };

  renderBopisStatusAvailability = () => {
    const { pickupLabels, bopisItemInventory } = this.props;
    const { selectedSizeDrawerTab } = this.state;
    const bopisInventoryError = getOOBErrorBOPIS(bopisItemInventory);
    const bopisInventoryStatus = bopisInventoryError
      ? pickupLabels.lbl_Product_pickup_OUT_OF_STOCK_TEXT_redesign
      : getStatusFromBOPIS(bopisItemInventory);
    if (bopisInventoryStatus || selectedSizeDrawerTab === 2) {
      return selectedSizeDrawerTab === 2 ? (
        this.pickDateContainer()
      ) : (
        <RowContainer margins="0 0 10px 0px">
          <PillContainerForInventoryStatus soldOut={this.getQuantityFromBOPIS() === 0}>
            <BodyCopy
              dataLocator="pdp_store_availability_label"
              fontFamily="secondary"
              fontSize="fs14"
              fontWeight="black"
              color="gray.900"
              text={`${bopisInventoryStatus}`}
            />
          </PillContainerForInventoryStatus>
          <BodyCopy
            margin="0 0 0 4px"
            dataLocator="pdp_store_availability_value"
            fontFamily="primary"
            fontSize="fs14"
            fontWeight="regular"
            color="gray.900"
            text={pickupLabels.lbl_Product_pickup_BOPIS_IN_THE_STORE_redesign}
          />
        </RowContainer>
      );
    }
    return null;
  };

  renderSizeDrawerHeader = () => {
    const { pdpLabels, currentProduct } = this.props;
    const { isGiftCard } = currentProduct;
    const { selectSizeLabel, selectValueLabel } = pdpLabels || {};
    const sizeDrawerSelectText = isGiftCard ? selectValueLabel : selectSizeLabel;
    return (
      <>
        <ModalBoxBtn
          accessibilityRole="button"
          onPress={() => this.closeSizeDrawer()}
          isPdpNewReDesign
        >
          <ModalBoxCloseImage source={closeImage} />
        </ModalBoxBtn>
        <View style={ProductDetailStyle.modalBoxView} />
        <BodyCopy
          fontFamily="secondary"
          margin="15px 0 0"
          fontSize="fs16"
          fontWeight="bold"
          color="gray.900"
          text={sizeDrawerSelectText}
          letterSpacing="ls1"
          textAlign="center"
        />
      </>
    );
  };

  renderSizeDrawerHeaderBossBopis = () => {
    const { isKeepAliveEnabled, isBossEligible, pickupLabels, formValues, checkForOOSForVariant } =
      this.props;
    const { selectedSizeDrawerTab, noBossBopisInfo } = this.state;
    const { size } = formValues || {};
    const isSizeSelected = this.checkSizeTruthValue(size);
    const { currentColorEntry } = this.state;
    const isDefaultBadge = getIsDefaultProductBadge(currentColorEntry?.miscInfo);
    const keepAlive =
      isKeepAliveEnabled && currentColorEntry.miscInfo && currentColorEntry.miscInfo.keepAlive;
    return (
      <SizeHeaderContainer>
        <SizeToggleContainer>
          <ToggleDeliveryMethod
            keepAlive={keepAlive}
            noBossBopisInfo={noBossBopisInfo}
            isBossEligible={isBossEligible}
            onTabChange={this.onTabChange}
            selectedTab={selectedSizeDrawerTab}
            pickupLabels={pickupLabels}
            isDefaultBadge={isDefaultBadge}
          />
          <ModalBoxBtn
            accessibilityRole="button"
            onPress={() => this.closeSizeDrawer()}
            isPdpNewReDesign
          >
            <ModalBoxCloseImage source={closeImage} />
          </ModalBoxBtn>
        </SizeToggleContainer>
        {selectedSizeDrawerTab !== 0 && (
          <>
            {selectedSizeDrawerTab === 2 && (
              <ContainerOfferNoRush>
                <PickupPromotionBanner bossBanner isfromNewdesign={true} />
              </ContainerOfferNoRush>
            )}
            {this.renderShowChangeStore()}
            {!isSizeSelected ? (
              <RowContainer margins="0 0 16px 0px">
                <Image
                  source={warning}
                  height={14}
                  width={16}
                  dataLocator="pdp_fast_shipping_icon"
                />
                <BodyCopy
                  margin="0 0 0 6px"
                  dataLocator="pdp_store_availability_value"
                  fontFamily="secondary"
                  fontSize="fs14"
                  fontWeight="light"
                  color="gray.900"
                  text={pickupLabels.lbl_Product_pickup_SELECT_SIZE_WARNING_TEXT_redesign}
                />
              </RowContainer>
            ) : (
              this.renderBopisStatusAvailability()
            )}
          </>
        )}
        {selectedSizeDrawerTab === 0 && checkForOOSForVariant && (
          <RowContainer margins="32px 0 0 0" />
        )}
      </SizeHeaderContainer>
    );
  };

  getEventData = () => {
    const { isOutfitPage, navigation, cartsSessionStatus } = this.props;
    const source = (navigation && navigation.getParam('source')) || '';

    const eventsData = ['scAdd', 'event61'];
    if (cartsSessionStatus) {
      eventsData.push('scOpen');
    }
    if (isOutfitPage) {
      eventsData.push('event77', 'event85');
    }
    if (source === 'Candid') {
      eventsData.push(names.eVarPropEvent.event154);
    }
    return eventsData;
  };

  checkSizeTruthValue = (size) => {
    if (size && typeof size !== 'object') return true;
    return false;
  };

  getButtonBorderRadius = () => {
    return isGymboree() ? '23px' : '16px';
  };

  handleSizeClick = () => {
    const {
      handleAddToBag,
      formValues,
      currentProduct,
      setClickAnalyticsDataAction,
      itemBrand,
      ledgerSummaryData,
      pageAddType,
    } = this.props;
    const { selectedDeliveryType } = this.state;
    const { size, fit, color, quantity } = formValues || {};
    const isSizeSelected = this.checkSizeTruthValue(size);
    if (!isSizeSelected) {
      this.setState((prevState) => ({
        ...prevState,
        showSize: true,
      }));
    } else {
      handleAddToBag(formValues, selectedDeliveryType);
      this.setState((prevState) => ({
        ...prevState,
        showSize: false,
      }));
    }
    const { subTotal } = ledgerSummaryData;
    const brandName = itemBrand || getBrand();

    const productData = formatProductsData(currentProduct, {
      selectedQuantity: quantity,
      selectedSize: size,
      selectedColor: color,
      itemBrand: brandName,
      subTotal: subTotal > 0 ? subTotal : currentProduct.listPrice,
      addType: pageAddType,
      sku: getSkuId(currentProduct.colorFitsSizesMap, color, fit, size),
    });
    if (productData && productData.length) {
      productData[0].color = color;
    }

    const { generalProductId, name } = currentProduct;
    const productId = generalProductId && generalProductId.split('_')[0];
    const productName = name && name.toLowerCase();
    const pageShortName = productId ? `product:${productId}:${productName}` : '';
    const pageName = pageShortName;
    setClickAnalyticsDataAction({
      customEvents: this.getEventData(),
      CartAddProductId: currentProduct ? currentProduct.generalProductId : '',
      products: productData,
      linkName: 'cart add',
      pageShortName,
      pageName,
      pageType: 'product',
      pageSection: 'product',
      pageSubSection: 'product',
    });
  };

  checkaddtobag = () => {
    const {
      pdpLabels,
      isKeepAliveEnabled,
      isLoading,
      pickupLabels,
      isBossBopisPDPSizeDrawer,
      navigation,
      bopisItemInventory,
      formValues,
      isSelectedSizeDisabled,
    } = this.props;
    const { currentColorEntry, selectedDeliveryType, selectedSizeDrawerTab } = this.state;
    const keepAlive = isKeepAliveEnabled && currentColorEntry.miscInfo.keepAlive;
    const { size } = formValues || {};
    const isSizeSelected = this.checkSizeTruthValue(size);
    const bopisInventoryError = getOOBErrorBOPIS(bopisItemInventory);
    const isOutOfStock = this.getQuantityFromBOPIS() === 0;
    const isPickupButtonDisable =
      selectedSizeDrawerTab === 1 && (bopisInventoryError || isOutOfStock) && isSizeSelected;
    const isSizeDisable = selectedSizeDrawerTab === 0 && isSizeSelected && isSelectedSizeDisabled;
    const buttonTitle =
      selectedDeliveryType !== 'shipIt' && isBossBopisPDPSizeDrawer
        ? pickupLabels.lbl_Product_pickup_PICKUP_IN_STORE_DM
        : pdpLabels.addToBag;
    const isSBP = navigation && navigation.getParam('isSBP');
    return (
      <AtbButton isSBP={isSBP}>
        <CustomButton
          fontSize="fs16"
          fontWeight="regular"
          fontFamily="secondary"
          paddings="10px"
          textPadding="0px 5px"
          margin="10px 0px 0px"
          borderRadius={this.getButtonBorderRadius}
          showShadow
          fill="BLUE"
          disableButton={keepAlive}
          buttonVariation="variable-width"
          height={isSBP ? '50px' : '46px'}
          wrapContent={false}
          text={buttonTitle}
          onPress={() => this.handleSizeClick()}
          disabled={keepAlive || isLoading || isPickupButtonDisable || isSizeDisable}
        />
      </AtbButton>
    );
  };

  renderAddToBagButton = () => {
    return this.checkaddtobag();
  };

  handlePickupModalClick = () => {
    const {
      currentProduct,
      onPickUpOpenClick,
      isBopisEligible,
      isBossEligible,
      availableTCPmapNewStyleId,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      isProductPickup,
      navigation,
    } = this.props;
    const { generalProductId } = currentProduct;
    this.closeSizeDrawer();
    onPickUpOpenClick({
      generalProductId,
      colorProductId: generalProductId,
      isBopisCtaEnabled: isBopisEligible,
      isBossCtaEnabled: isBossEligible,
      currentProduct: { ...currentProduct, itemBrand: navigation.getParam('brandId') },
      availableTCPmapNewStyleId,
      availableTCPMultipackProductMapping: initialMultipackMapping,
      initialSelectedQty: setInitialTCPStyleQty,
      isProductPickup,
    });
  };

  renderSizeDrawer = () => {
    const {
      currentProduct,
      plpLabels,
      pdpLabels,
      handleFormSubmit,
      navigation,
      addToBagError,
      handleSubmit,
      alternateSizes,
      toastMessage,
      fireHapticFeedback,
      clearAddToBagError,
      isKeepAliveEnabled,
      outOfStockLabels,
      sizeChartDetails,
      isDynamicBadgeEnabled,
      availableTCPmapNewStyleId,
      formValues,
      isLoading,
      multipackProduct,
      multiPackCount,
      multiPackThreshold,
      partIdInfo,
      getAllNewMultiPack,
      addBtnMsg,
      isRecommendationAvailable,
      checkForOOSForVariant,
      isBrierleyPromoEnabled,
      getDetails,
      singlePageLoad,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      disableMultiPackTab,
      getDisableSelectedTab,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      sbpLabels,
      primaryBrand,
      alternateBrand,
      isBossBopisPDPSizeDrawer,
      bopisItemInventory,
      isPDPCalledFromRecommendation,
    } = this.props;

    const { selectedColorProductId, showSize, currentColorEntry, selectedSizeDrawerTab } =
      this.state;
    const { size, fit, quantity } = formValues || {};
    const isSizeSelected = this.checkSizeTruthValue(size);
    const bopisInventoryError = getOOBErrorBOPIS(bopisItemInventory);
    const isOutOfStock = this.getQuantityFromBOPIS() === 0;
    const isPickupButtonDisable =
      selectedSizeDrawerTab === 1 && (bopisInventoryError || isOutOfStock) && isSizeSelected;
    const atbDisabled =
      (size && typeof size === 'object' && size.name === '') ||
      size === '' ||
      (isBossBopisPDPSizeDrawer && isPickupButtonDisable);
    const profile = navigation.getParam('profile');
    const { TCPStyleType, TCPStyleQTY, isGiftCard } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );
    return (
      <ModalBox
        animationDuration={50}
        swipeThreshold={10}
        style={ProductDetailStyle.modalBoxNew(isBossBopisPDPSizeDrawer)}
        position="bottom"
        swipeArea={400}
        isOpen={showSize}
        entry="bottom"
        swipeToClose={false}
        onClosed={this.closeSizeDrawer}
        coverScreen
        useNativeDriver
        backdropContent={<SizeDrawerUnderlay />}
        backdropOpacity={0.7}
      >
        {isBossBopisPDPSizeDrawer
          ? this.renderSizeDrawerHeaderBossBopis()
          : this.renderSizeDrawerHeader()}
        {!isLoading && (
          <ProductAddToBagContainer
            currentProduct={currentProduct}
            form={`ProductAddToBag-${currentProduct.generalProductId}`}
            plpLabels={plpLabels}
            handleFormSubmit={handleFormSubmit}
            selectedColorProductId={selectedColorProductId}
            errorOnHandleSubmit={addToBagError}
            handleSubmit={handleSubmit}
            onChangeSize={this.onChangeSize}
            onFitChange={this.onChangeFit}
            onChangeColor={this.onChangeColor}
            onQtyChange={this.onChangeQuantity}
            onMultiPackChange={this.onMultiPackChange}
            scrollToTarget={this.scrollPageToTarget}
            sizeChartLinkVisibility={false}
            alternateSizes={alternateSizes}
            navigation={navigation}
            toastMessage={toastMessage}
            fireHapticFeedback={fireHapticFeedback}
            clearAddToBagError={clearAddToBagError}
            isKeepAliveEnabled={isKeepAliveEnabled}
            outOfStockLabels={outOfStockLabels}
            sizeChartDetails={sizeChartDetails}
            addBtnMsg={addBtnMsg}
            multipackProduct={multipackProduct}
            multiPackCount={multiPackCount}
            multiPackThreshold={multiPackThreshold}
            partIdInfo={partIdInfo}
            getAllNewMultiPack={getAllNewMultiPack}
            isRecommendationAvailable={isRecommendationAvailable}
            isPDP
            pdpLabels={pdpLabels}
            getDetails={getDetails}
            checkForOOSForVariant={checkForOOSForVariant}
            isBrierleyPromoEnabled={isBrierleyPromoEnabled}
            singlePageLoad={singlePageLoad}
            initialMultipackMapping={initialMultipackMapping}
            setInitialTCPStyleQty={setInitialTCPStyleQty}
            disableMultiPackTab={disableMultiPackTab}
            getDisableSelectedTab={getDisableSelectedTab}
            onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
            deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
            childProfile={profile}
            sbpSize={size}
            size={size}
            formValues={formValues}
            dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
            sbpFit={fit}
            sbpLabels={sbpLabels}
            showColorChips={false}
            showAddToBagCTA={false}
            hideQuatity
            availableTCPmapNewStyleId={availableTCPmapNewStyleId}
            initialFormValues={{ Size: size, Fit: fit, Quantity: quantity }}
            pageName="PDP"
            currentColorEntry={currentColorEntry}
            selectedQuantity={formValues && formValues.quantity}
            fromPdpQuickDraw={true}
            primaryBrand={primaryBrand}
            atbDisabled={atbDisabled}
            isGiftCard={isGiftCard}
            borderRadius={this.getButtonBorderRadius}
            handleSizeClick={this.handleSizeClick}
            selectedSizeDrawerTab={selectedSizeDrawerTab}
            alternateBrand={!isPDPCalledFromRecommendation && alternateBrand}
            isShowDisabledSize={selectedSizeDrawerTab}
          />
        )}
        {this.renderPickUpInStoreAnchorButton(!atbDisabled)}
      </ModalBox>
    );
  };

  getStickyContainerHeight = () => {
    return this.isAndroidDevice ? 70 : 80;
  };

  stickyButtonContainerStyling = () => {
    const { showSize } = this.state;
    if (showSize)
      return { flexDirection: 'column', height: 'auto', borderWidth: 0, borderRadius: 30 };
    return {
      flexDirection: 'row',
      height: this.getStickyContainerHeight(),
      borderWidth: 1,
      borderRadius: 0,
      zIndex: this.stickyContainerZindex,
      paddingBottom: this.isAndroidDevice ? 15 : 25,
    };
  };

  onChangeSize = (color, e, fit) => {
    const {
      currentProduct: { isGiftCard },
      updateSizeValue,
      updateFit,
    } = this.props;
    if (e) updateSizeValue(e);
    if (fit) updateFit(fit);
    if (typeof e !== 'object' && !isGiftCard) {
      this.setState({ currentGiftCardValue: e, size: e, fit, color });
    }
    if (isGiftCard) {
      this.setState({ currentGiftCardValue: e, size: e, fit, color });
    }
  };

  onChangeFit = () => {
    const { formValues } = this.state;
    const { updateSizeValue } = this.props;
    updateSizeValue('');
    this.setState({ size: '', formValues: { ...formValues, Size: '' } });
  };

  showPreSelectedSize = () => {
    const { accessorySize } = this.state;
    const { pdpLabels, formValues, currentProduct } = this.props;
    const { selectSizeLabel, selectValueLabel } = pdpLabels;
    const { size } = formValues || {};
    const { isGiftCard } = currentProduct;
    if (size && !accessorySize && typeof size !== 'object') {
      if (isGiftCard) return `$${size}`;
      return `${size}`;
    }
    if (accessorySize) return `${size}`;
    if (isGiftCard) {
      return selectValueLabel;
    }
    return selectSizeLabel;
  };

  showSize = () => {
    this.setState({
      showSize: true,
    });
  };

  renderStickyButtons = () => {
    const { showSize, currentColorEntry } = this.state;
    const { isKeepAliveEnabled, isLoading, navigation } = this.props;
    const isSBP = navigation && navigation.getParam('isSBP');
    const sbpThemeColor = navigation && navigation.getParam('backgroundColor');
    const stickyButtonContainer = this.stickyButtonContainerStyling();
    const addToBagGymStyling = isGymboree() ? { borderRadius: 23 } : {};
    const keepAlive = isKeepAliveEnabled && currentColorEntry.miscInfo.keepAlive;
    const disableButton = keepAlive ? { opacity: 0.5 } : {};
    const sbpThemeBackgroundColor = isSBP ? { backgroundColor: sbpThemeColor } : {};
    return (
      <View style={[ProductDetailStyle.view2, stickyButtonContainer, sbpThemeBackgroundColor]}>
        {!showSize && (
          <TouchableOpacity
            accessibilityRole="button"
            onPress={() => this.showSize()}
            disabled={keepAlive || isLoading}
            style={[ProductDetailStyle.touchableOpacity5, disableButton, addToBagGymStyling]}
          >
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs16"
              fontWeight="bold"
              color="gray.900"
              letterSpacing="ls02"
              text={this.showPreSelectedSize()}
              textAlign="center"
            />
            {showSize ? (
              <CustomIcon name="chevron-up" size="fs14" style={ProductDetailStyle.customIcon} />
            ) : (
              <ImageStyleContainer>
                <Image source={downArrowImage} width={12} height={12} />
              </ImageStyleContainer>
            )}
          </TouchableOpacity>
        )}
        {!showSize && this.renderAddToBagButton()}
      </View>
    );
  };

  // This is required for reommendations.
  getCatIdForRecommendation = () => {
    const { breadCrumbs } = this.props;
    if (breadCrumbs) {
      const category = breadCrumbs.map((crumb, index) => {
        const { displayName } = crumb;
        const separationChar = index !== breadCrumbs.length - 1 ? ':' : '';
        return displayName + separationChar;
      });
      return category.join('');
    }
    return '';
  };

  onChangeColor = (e) => {
    const {
      currentProduct: { colorFitsSizesMap },
      updateColor,
      isBossBopisPDPSizeDrawer,
      getCurrentColorEntry,
    } = this.props;
    const currentColorEntry = getMapSliceForColor(colorFitsSizesMap, e);
    const isDefaultBadge = getIsDefaultProductBadge(currentColorEntry?.miscInfo);
    if (isBossBopisPDPSizeDrawer && isDefaultBadge) {
      this.onTabChange(0);
    }
    if (updateColor) updateColor(e);
    this.setState(
      {
        currentColorEntry,
        quantity: 1,
      },
      () => {
        getCurrentColorEntry(currentColorEntry);
      }
    );
  };

  onChangeQuantity = (selectedQuantity) => {
    this.setState({
      quantity: selectedQuantity,
    });
  };

  onMultiPackChange = () => {
    const { formValues } = this.state;
    this.setState({
      size: '',
      quantity: 1,
      isFastImage: false,
      formValues: { ...formValues, Size: '', quantity: 1 },
    });
  };

  onShare = async () => {
    const { navigation } = this.props;
    const { webAppDomain } = getAPIConfig();
    const pdpUrl = (navigation && navigation.getParam('pdpUrl')) || '';
    const shareUrl = `${webAppDomain}/us/p/${pdpUrl}`;
    try {
      trackForterAction(ForterActionType.SHARE, 'PDP_ITEM_SHARE');
      await Share.share({
        message: shareUrl,
      });
    } catch (error) {
      logger.error(error);
    }
  };

  renderShare = () => {
    const AnimatedShareContainer = Animated.createAnimatedComponent(ShareContainer);
    const imageStyle = {
      height: 24,
      width: 24,
    };
    const { isDrawerOpen, startAnimate, isDrawerClose } = this.state;
    return (
      <AnimatedShareContainer
        left={startAnimate && this.shareIconPositionLeft}
        top={startAnimate && this.shareIconPositionTop}
        defaultPositions={
          isDrawerOpen || !isDrawerClose
            ? { left: this.shareIconEndLeft, top: this.topIconPos + 4 }
            : { left: this.iconStartLeft, top: this.shareIconStartTop }
        }
      >
        <TouchableOpacityGH
          accessibilityRole="button"
          accessibilityLabel="favorite"
          accessibilityState={{ selected: true }}
          onPress={this.onShare}
        >
          <FastImage
            source={this.isAndroidDevice ? shareImageAndroid : shareImage}
            dataLocator="pdp_social_connect"
            style={imageStyle}
            resizeMode={FastImage?.resizeMode?.contain}
          />
        </TouchableOpacityGH>
      </AnimatedShareContainer>
    );
  };

  comingFromPDPSwatch = () => {
    this.setState({ isComingFromPDPSwatch: true, isComingFromFullScreen: false });
  };

  getProductPrices = (showPriceRange) => {
    const { currentProduct } = this.props;
    const { currentColorEntry, size, fit } = this.state;
    return !showPriceRange
      ? getPrices(currentProduct, currentColorEntry.color.name, fit, size)
      : getPricesWithRange(currentProduct, currentColorEntry.color.name, fit, size);
  };

  productContainer = (props) => {
    const {
      currentProduct,
      plpLabels,
      handleFormSubmit,
      navigation,
      addToBagError,
      handleSubmit,
      alternateSizes,
      toastMessage,
      fireHapticFeedback,
      isKeepAliveEnabled,
      outOfStockLabels,
      sizeChartDetails,
      addBtnMsg,
      multipackProduct,
      multiPackCount,
      multiPackThreshold,
      partIdInfo,
      getAllNewMultiPack,
      isRecommendationAvailable,
      checkForOOSForVariant,
      isBrierleyPromoEnabled,
      pdpLabels,
      getDetails,
      singlePageLoad,
      initialMultipackMapping,
      setInitialTCPStyleQty,
      disableMultiPackTab,
      getDisableSelectedTab,
      istMultiColorNameEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      isDynamicBadgeEnabled,
      sbpLabels,
      availableTCPmapNewStyleId,
      isNewReDesignEnabled,
      isLoading,
      formValues,
      clearAddToBagError,
      primaryBrand,
      alternateBrand,
      isPDPCalledFromRecommendation,
    } = this.props;
    const {
      isComingFromFullScreen,
      isComingFromPDPSwatch,
      currentColorEntry,
      selectedSizeDrawerTab,
    } = this.state;
    const colorNameAvailable = formValues && formValues.color;
    const { colorFitsSizesMap, TCPStyleColor } = currentProduct;
    const pdpDataInformation = {
      color: { name: colorNameAvailable },
    };
    const { selectedColorProductId, size, fit } = this.state;
    const colorSlice = getMapSliceForColor(colorFitsSizesMap, pdpDataInformation.color.name);
    const newColorList = istMultiColorNameEnabled ? getMapColorList(TCPStyleColor, colorSlice) : [];
    const profile = navigation.getParam('profile');
    const { TCPStyleType, TCPStyleQTY } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );

    return (
      !isLoading && (
        <ProductAddToBagContainer
          currentProduct={currentProduct}
          plpLabels={plpLabels}
          handleFormSubmit={handleFormSubmit}
          selectedColorProductId={selectedColorProductId}
          errorOnHandleSubmit={addToBagError}
          handleSubmit={handleSubmit}
          onChangeSize={this.onChangeSize}
          onChangeColor={this.onChangeColor}
          onQtyChange={this.onChangeQuantity}
          onMultiPackChange={this.onMultiPackChange}
          scrollToTarget={this.scrollPageToTarget}
          sizeChartLinkVisibility={props.sizeChartLinkVisibility}
          alternateSizes={alternateSizes}
          navigation={navigation}
          toastMessage={toastMessage}
          fireHapticFeedback={fireHapticFeedback}
          clearAddToBagError={clearAddToBagError}
          isKeepAliveEnabled={isKeepAliveEnabled}
          outOfStockLabels={outOfStockLabels}
          sizeChartDetails={sizeChartDetails}
          addBtnMsg={addBtnMsg}
          multipackProduct={multipackProduct}
          multiPackCount={multiPackCount}
          multiPackThreshold={multiPackThreshold}
          partIdInfo={partIdInfo}
          getAllNewMultiPack={getAllNewMultiPack}
          isRecommendationAvailable={isRecommendationAvailable}
          pdpLabels={pdpLabels}
          getDetails={getDetails}
          checkForOOSForVariant={checkForOOSForVariant}
          isBrierleyPromoEnabled={isBrierleyPromoEnabled}
          singlePageLoad={singlePageLoad}
          initialMultipackMapping={initialMultipackMapping}
          setInitialTCPStyleQty={setInitialTCPStyleQty}
          disableMultiPackTab={disableMultiPackTab}
          getDisableSelectedTab={getDisableSelectedTab}
          newColorList={newColorList}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          isPDP
          childProfile={profile}
          sbpSize={size}
          size={size}
          formValues={formValues}
          dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          sbpFit={fit}
          sbpLabels={sbpLabels}
          availableTCPmapNewStyleId={availableTCPmapNewStyleId}
          isNewReDesignProductSummary={isNewReDesignEnabled}
          isComingFromFullScreen={isComingFromFullScreen}
          isComingFromPDPSwatch={isComingFromPDPSwatch}
          comingFromPDPSwatch={this.comingFromPDPSwatch}
          currentColorEntry={currentColorEntry}
          selectedQuantity={formValues && formValues.quantity}
          primaryBrand={primaryBrand}
          alternateBrand={!isPDPCalledFromRecommendation && alternateBrand}
          isQuantityRow
          isShowDisabledSize={selectedSizeDrawerTab}
        />
      )
    );
  };

  shouldRenderRatingReview = (isGiftCard, isBundleProduct, ratings) => {
    return !isGiftCard && !isBundleProduct && ratings;
  };

  renderBelowRecsPromoBanner = (promoBanners) => {
    const { navigation } = this.props;
    if (promoBanners && promoBanners.length) {
      return (
        <PromoBelowRecsContainer>
          <PromoPDPBanners promos={promoBanners} navigation={navigation} />
        </PromoBelowRecsContainer>
      );
    }
    return null;
  };

  renderProductReview = ({ renderRatingReview, productId, reviewsCount, pdpLabels, giftCard }) => {
    const { expandReviewFromAnchor } = this.state;
    const label = `${typeof reviewsCount !== 'undefined' ? reviewsCount : 0} ${
      pdpLabels && pdpLabels.reviews
    }`;
    return renderRatingReview >= 0 && !giftCard ? (
      <Margin margins="6px 0px 32px 0px">
        <ProductRatingDetailButton
          onPress={() => this.callbackExpandReviewSection()}
          isAndroid={this.isAndroidDevice}
          reviewsCount={reviewsCount}
        >
          <Stars
            display={renderRatingReview}
            spacing={6}
            count={5}
            starSize={18}
            disabled
            fullStar={starImg}
          />
          <BodyCopyWithSpacing
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="extrabold"
            color="gray.900"
            text={label}
            textAlign="left"
            margin="0 6px"
          />
          <ProductRatingDetailArrow>
            <Image source={rightArrowImage} width={16} height={16} />
          </ProductRatingDetailArrow>
        </ProductRatingDetailButton>

        {!giftCard && (
          <ModalBox
            animationDuration={400}
            swipeThreshold={50}
            position="bottom"
            isOpen={expandReviewFromAnchor}
            style={ProductDetailStyleRedesign.modalBoxReview}
            entry="bottom"
            swipeToClose={false}
            onClosed={this.closeRatingDrawer}
            coverScreen
            useNativeDriver
            swipeArea={40}
            backdropContent={<SizeDrawerUnderlay />}
          >
            <ReviewsRatingsHeader isAndroid={this.isAndroidDevice}>
              <ModalBoxReviewBtn
                accessibilityRole="button"
                onPress={() => this.closeRatingDrawer()}
              >
                <Image source={leftArrowImage} width={24} height={24} />
              </ModalBoxReviewBtn>
              <ReviewsRatingsHeaderTitle>
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs16"
                  fontWeight="extrabold"
                  color="gray.900"
                  text={pdpLabels && pdpLabels.ratingReviewReDesign}
                  textAlign="center"
                />
              </ReviewsRatingsHeaderTitle>
            </ReviewsRatingsHeader>

            {expandReviewFromAnchor && (
              <ProductReviewsContainer
                reviewsCount={reviewsCount}
                ratingsProductId={productId}
                isNewReDesignEnabled
                closeRatingDrawer={this.closeRatingDrawer}
                expanded={expandReviewFromAnchor}
              />
            )}
          </ModalBox>
        )}
      </Margin>
    ) : null;
  };

  getQuantityFromBOPIS = () => {
    const { bopisItemInventory } = this.props;

    const bopisItemInventoryRes =
      bopisItemInventory &&
      bopisItemInventory.inventoryResponse &&
      bopisItemInventory.inventoryResponse[0];

    return bopisItemInventoryRes?.quantity || 0;
  };

  getButtonTextNewDesign = (isPickupInstead) => {
    const { pickupLabels } = this.props;
    const { selectedSizeDrawerTab } = this.state;
    if (selectedSizeDrawerTab === 0) {
      if (isPickupInstead) return pickupLabels.lbl_Product_pickup_PICKUP_INSTEAD;
      return pickupLabels.lbl_Product_pickup_FIND_STORE;
    }
    return pickupLabels.lbl_Product_pickup_SEARCH_OTHER_STORES;
  };

  selectDeliveryType = (type, index) => {
    const { resetBopisInventoryData } = this.props;
    resetBopisInventoryData();
    this.setState({ selectedDeliveryType: type, selectedSizeDrawerTab: index });
    this.clearSizeOnDisableState(index);
  };

  isPickupAvailable = () => {
    const { isKeepAliveEnabled } = this.props;
    const { currentColorEntry, noBossBopisInfo } = this.state;
    const isDefaultBadge = getIsDefaultProductBadge(currentColorEntry?.miscInfo);
    const keepAlive =
      isKeepAliveEnabled && currentColorEntry.miscInfo && currentColorEntry.miscInfo.keepAlive;

    return !noBossBopisInfo && !isDefaultBadge && !keepAlive;
  };

  renderPickUpInStoreAnchorButton(isSizeSelected) {
    const {
      plpLabels,
      currentProduct,
      pickupLabels,
      isBossBopisPDPSizeDrawer,
      bopisItemInventory,
    } = this.props;
    const { selectedSizeDrawerTab } = this.state;
    const { isGiftCard } = currentProduct;
    const isOutOfStock = this.getQuantityFromBOPIS() === 0;
    const sizeLabel = isOutOfStock
      ? pickupLabels.lbl_Product_pickup_NEED_ASAP
      : pickupLabels.lbl_Product_pickup_STORE_NEAR_YOU;
    const labelText = isSizeSelected ? sizeLabel : plpLabels.sizeUnavalaible;
    const isPickupInstead = isSizeSelected && !isOutOfStock && selectedSizeDrawerTab === 0;
    const anchorLabel = this.getButtonTextNewDesign(isPickupInstead);
    const bopisInventoryStatus = getStatusFromBOPIS(bopisItemInventory);
    if (
      isGiftCard ||
      selectedSizeDrawerTab === 2 ||
      (selectedSizeDrawerTab === 1 && (!isSizeSelected || !isOutOfStock))
    ) {
      return null;
    }
    return isBossBopisPDPSizeDrawer ? (
      <FooterWrapper isPdpNewReDesign isNewDesignBoss>
        {isPickupInstead && bopisInventoryStatus && (
          <PillContainerForInventoryStatus>
            <BodyCopy
              dataLocator="pdp_store_availability_label"
              fontFamily="secondary"
              fontSize="fs14"
              fontWeight="black"
              color="gray.900"
              text={`${bopisInventoryStatus}`}
            />
          </PillContainerForInventoryStatus>
        )}
        {selectedSizeDrawerTab === 0 && this.isPickupAvailable() && (
          <BodyCopyWithSpacing
            margin="0 6px"
            fontFamily="secondary"
            fontWeight="bold"
            fontSize="fs14"
            color="black"
            text={labelText}
          />
        )}
        {this.isPickupAvailable() && (
          <RowContainer margins="0 0 0 10px">
            <Anchor
              noLink
              onPress={() => {
                if (isPickupInstead) {
                  this.analyticsOnSizeDrawerToggle('event168', 'App_Pickup_Instead_Click_e168');
                }
                this.handlePickupModalClick();
              }}
              accessibilityRole="link"
              accessibilityLabel={anchorLabel}
              text={anchorLabel}
              anchorVariation="custom"
              colorName="gray.900"
              fontSize="fs14"
              fontWeight="regular"
              fontSizeVariation="large"
              underline
            />
            {!isPickupInstead && (
              <IconWrapper isNewDesignBoss>
                <Image source={rightArrowImage} width={12} height={12} />
              </IconWrapper>
            )}
          </RowContainer>
        )}
      </FooterWrapper>
    ) : (
      <FooterWrapper isPdpNewReDesign>
        <BodyCopyWithSpacing
          fontFamily="secondary"
          fontWeight="semibold"
          fontSize="fs14"
          color="black"
          text={plpLabels.sizeUnavalaible}
        />
        <Anchor
          noLink
          onPress={this.handlePickupModalClick}
          accessibilityRole="link"
          accessibilityLabel={pickupLabels.lbl_Product_pickup_FIND_STORE}
          text={pickupLabels.lbl_Product_pickup_FIND_STORE}
          anchorVariation="custom"
          colorName="gray.900"
          fontSizeVariation="large"
          underline
        />
        <IconWrapper>
          <Image source={rightArrowImage} width={12} height={12} />
        </IconWrapper>
      </FooterWrapper>
    );
  }

  renderRelatedOutfits = (itemColorObj) => {
    const { navigation, pdpLabels, accessibilityLabels, isNewReDesignEnabled } = this.props;
    const productIdForSwatch = itemColorObj && itemColorObj.colorDisplayId;
    return (
      <View collapsable={false}>
        <RelatedOutfits
          paddings="0 12px 0 12px"
          margins="8px 0 0 0"
          background="#ffffff"
          pdpLabels={pdpLabels}
          navigation={navigation}
          selectedColorProductId={productIdForSwatch}
          showSmallImage
          setShowCompleteTheLook={this.setShowCompleteTheLook}
          accessibilityLabels={accessibilityLabels}
          triggerStylyticsAnalytics={this.triggerStylyticsAnalytics}
          fromPage={Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP}
          isNewReDesignCTL={isNewReDesignEnabled}
        />
      </View>
    );
  };

  renderProductSummary = () => {
    const {
      currentProduct,
      pdpLabels,
      currency,
      currencyExchange,
      outOfStockLabels,
      accessibilityLabels,
      isKeepAliveEnabled,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      isPlcc,
      selectedMultipack,
      isPerUnitPriceEnabled,
      isBrierleyPromoEnabled,
      onSetLastDeletedItemIdAction,
      deleteFavItemInProgressFlag,
      navigation,
      isDynamicBadgeEnabled,
      isNewReDesignEnabled,
      stylisticsProductTabList,
      primaryBrand,
      alternateBrand,
      isPDPCalledFromRecommendation,
    } = this.props;
    const { productId } = currentProduct;
    const { currentColorEntry, currentGiftCardValue, showCompleteTheLook, size } = this.state;

    const keepAlive = isKeepAliveEnabled && currentColorEntry.miscInfo.keepAlive;
    const showPriceRange = isShowPriceRangeKillSwitch && showPriceRangeForABTest;

    const currentProductPrices = this.getProductPrices(showPriceRange);
    const { TCPMultipackProductMapping, TCPStyleType } = currentProduct;
    const { TCPStyleQTY } = currentProduct;
    const dynamicBadgeOverlayQty = getDynamicBadgeQty(
      TCPStyleType,
      TCPStyleQTY,
      isDynamicBadgeEnabled
    );
    return (
      <ProductSummaryContainer>
        <ProductSummary
          productData={currentProduct}
          selectedColorProductId={currentColorEntry.colorDisplayId || productId}
          {...currentProductPrices}
          offerPrice={
            currentProduct?.isGiftCard
              ? parseInt(currentGiftCardValue, 10)
              : currentProductPrices.offerPrice
          }
          listPrice={
            currentProduct?.isGiftCard
              ? parseInt(currentGiftCardValue, 10)
              : currentProductPrices.listPrice
          }
          currencySymbol={currency}
          currencyExchange={currencyExchange}
          isGiftCard={currentProduct?.isGiftCard}
          showCompleteTheLook={showCompleteTheLook}
          accessibilityLabels={accessibilityLabels}
          pdpLabels={pdpLabels}
          keepAlive={keepAlive}
          outOfStockLabels={outOfStockLabels}
          scrollToTarget={this.scrollPageToTarget}
          renderRatingReview
          isPlcc={isPlcc}
          parentExpandRatingReview={this.callbackExpandReviewSection}
          showPriceRange={showPriceRange}
          selectedSize={size}
          selectedMultipack={selectedMultipack}
          TCPMultipackProductMapping={TCPMultipackProductMapping}
          TCPStyleType={TCPStyleType}
          TCPStyleQTY={TCPStyleQTY}
          isPerUnitPriceEnabled={isPerUnitPriceEnabled}
          isBrierleyPromoEnabled={isBrierleyPromoEnabled}
          onSetLastDeletedItemIdAction={onSetLastDeletedItemIdAction}
          deleteFavItemInProgressFlag={deleteFavItemInProgressFlag}
          navigation={navigation}
          isNewReDesignProductSummary={isNewReDesignEnabled}
          dynamicBadgeOverlayQty={dynamicBadgeOverlayQty}
          stylisticsProductTabList={stylisticsProductTabList}
          getProductNameAndTopbadge={this.renderBadgeAndTitleContainer}
          primaryBrand={primaryBrand}
          alternateBrand={!isPDPCalledFromRecommendation && alternateBrand}
        />
      </ProductSummaryContainer>
    );
  };

  renderFulfillmentSection = () => {
    const {
      currentProduct,
      outOfStockLabels,
      availableTCPmapNewStyleId,
      isKeepAliveEnabled,
      isNewReDesignEnabled,
      isPickupModalOpen,
      navigation,
      isBossBopisPDPSizeDrawer,
      formValues,
      fromBagPage,
      retrievedProductInfo,
    } = this.props;
    const { currentColorEntry, size, fit, selectedDeliveryType } = this.state;
    const { color: colorObj } = JSON.parse(JSON.stringify(currentColorEntry)) || {};
    const { name: colorNameFromCurrentColorEntry } = colorObj || {};
    let values = { size, fit, color: colorNameFromCurrentColorEntry };
    if (!size && formValues) {
      const { color } = formValues;
      values = { size: formValues?.size, fit: formValues?.fit, color };
    }
    const keepAlive =
      isKeepAliveEnabled && currentColorEntry.miscInfo && currentColorEntry.miscInfo.keepAlive;

    const { productId } = currentProduct;
    const selectedColorProductId = currentColorEntry.colorDisplayId || productId;
    const colorFitsSizesMap = get(
      currentProduct || retrievedProductInfo,
      'colorFitsSizesMap',
      null
    );
    const curentColorEntryValue =
      colorFitsSizesMap && getMapSliceForColorProductId(colorFitsSizesMap, selectedColorProductId);

    return (
      currentProduct &&
      currentColorEntry && (
        <>
          {isPickupModalOpen && !fromBagPage && <PickupStoreModal navigation={navigation} />}
          <ProductPickupContainer
            productInfo={currentProduct}
            formName={`ProductAddToBag-${currentProduct.generalProductId}`}
            miscInfo={JSON.parse(JSON.stringify(curentColorEntryValue.miscInfo))}
            keepAlive={keepAlive}
            outOfStockLabels={outOfStockLabels}
            availableTCPmapNewStyleId={availableTCPmapNewStyleId}
            isNewReDesignFullfilmentSection={isNewReDesignEnabled}
            initialValues={values}
            noBossBopisInfoSendToPDP={(flag) => this.noBossBopisInfoFunc(flag)}
            selectDeliveryType={this.selectDeliveryType}
            currentSelectedPickUpType={selectedDeliveryType}
            isBossBopisPDPSizeDrawer={isBossBopisPDPSizeDrawer}
          />
        </>
      )
    );
  };

  renderContentValue = () => {
    const {
      currentProduct,
      pdpLabels,
      pdpDataInformation,
      isPdp1andPdp2Enabled,
      retrievedProductInfo,
      isLoading,
      isShowPriceRangeKillSwitch,
      showPriceRangeForABTest,
      navigation,
      formValues,
      openApplyNowModal,
      quickViewLoader,
      quickViewProductId,
      belowrecsPromos,
    } = this.props;
    const product = this.getCurrentProduct();
    const { startAnimate } = this.state;
    const actionId = navigation.getParam('actionId');
    const viaModule = navigation.getParam('viaModule');
    const { deviceTier } = navigation.getScreenProps();
    const sizeChartLinkVisibility = !product?.isGiftCard
      ? SIZE_CHART_LINK_POSITIONS.AFTER_SIZE
      : null;
    const showPriceRange = isShowPriceRangeKillSwitch && showPriceRangeForABTest;
    const categoryId = this.getCatIdForRecommendation();
    const recommendationAttributes = {
      variation: 'moduleO',
      navigation,
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP,
      partNumber: product?.productId,
      isHeaderAccordion: true,
      categoryName: categoryId,
    };
    const colorNameAvailable = this.getFormName(formValues);

    if (viaModule === 'mbox_Recommendation') {
      recommendationAttributes.actionId = actionId;
      recommendationAttributes.isRecProduct = true;
    }
    const renderRatingReview = this.shouldRenderRatingReview(
      product?.isGiftCard,
      false,
      product?.unbxdRatings
    );

    const itemColorObj = this.getItemColor();
    return (
      <PullDrawerContent>
        {!isLoading && !!startAnimate ? (
          <>
            <PullDrawerProductSummary>
              <Margin margins="0 14px">
                {this.renderProductSummary()}
                {this.productContainer({ sizeChartLinkVisibility })}
                <PLCCBanner
                  pdpLabels={pdpLabels}
                  openApplyNowModal={openApplyNowModal}
                  navigation={navigation}
                  backGroundColor
                />
              </Margin>
            </PullDrawerProductSummary>
            <Margin margins="0 14px">
              {this.renderFulfillmentSection()}
              {this.renderProductAlternateSizes()}
              {this.renderProductDescription(itemColorObj)}
              <Margin margins="-30px 0px 0px 0px">{this.renderRelatedOutfits(itemColorObj)}</Margin>
              {this.renderRecommendationVariation({
                isPdp1andPdp2Enabled,
                colorNameAvailable,
                pdpLabels,
                showPriceRange,
                recommendationAttributes,
                pdpDataInformation,
                deviceTier,
                quickViewLoader,
                quickViewProductId,
              })}
              {this.renderBelowRecsPromoBanner(belowrecsPromos)}
              {this.renderProductReview({
                renderRatingReview,
                productId: currentProduct?.ratingsProductId,
                reviewsCount:
                  currentProduct?.unbxdReviewsCount || retrievedProductInfo?.unbxdReviewsCount,
                pdpLabels,
                giftCard: currentProduct?.isGiftCard,
              })}
            </Margin>
          </>
        ) : (
          this.renderSkeleton()
        )}
      </PullDrawerContent>
    );
  };

  renderContent = () => {
    return <View>{this.renderContentValue()}</View>;
  };

  renderSkeleton = () => {
    const { retrievedProductInfo } = this.props;
    const skeletonSwatchesCount =
      retrievedProductInfo && Object.keys(retrievedProductInfo.imagesByColor).length < 8
        ? Object.keys(retrievedProductInfo?.imagesByColor).length
        : 8;
    return (
      <SkeletonBodyContainer>
        <SkeletonProdSummaryContainer>
          {retrievedProductInfo ? (
            this.renderBadgeAndTitleContainer(
              retrievedProductInfo?.name,
              retrievedProductInfo?.badge1
            )
          ) : (
            <ViewWithSpacing spacingStyles="margin-bottom-XXS">
              <LoaderSkelton width="80px" height="10px" borderRadius={10} />
              <LoaderSkelton width="250px" height="15px" borderRadius={10} />
              <LoaderSkelton width="130px" height="15px" borderRadius={10} />
            </ViewWithSpacing>
          )}
          {retrievedProductInfo ? (
            <BodyCopy
              margin="0 0 0 0"
              dataLocator="pdp_current_product_price"
              fontFamily="secondary"
              fontSize="fs22"
              fontWeight="black"
              color="red.600"
              text={`$${retrievedProductInfo?.offerPrice}`}
            />
          ) : (
            <LoaderSkelton width="100px" height="40px" borderRadius={10} />
          )}
          {retrievedProductInfo ? (
            <ProductRating
              isNewReDesignProductTile
              ratings={retrievedProductInfo?.ratings}
              reviewsCount={retrievedProductInfo?.reviewsCount}
            />
          ) : (
            <LoaderSkelton width="119px" height="20px" borderRadius={10} />
          )}
        </SkeletonProdSummaryContainer>
        {skeletonSwatchesCount && skeletonSwatchesCount > 1 && (
          <SkeletonSwatchesContainer>
            {[...Array(skeletonSwatchesCount).keys()].map(() => {
              return (
                <SkeletonSwatch>
                  <LoaderSkelton width="60px" height="65px" borderRadius={10} />
                </SkeletonSwatch>
              );
            })}
          </SkeletonSwatchesContainer>
        )}
        <SkeletonMulPackContainer />
      </SkeletonBodyContainer>
    );
  };

  renderHeader = () => {
    const AnimatedPullDrawerHeader = Animated.createAnimatedComponent(PullDrawerHeader);
    const AnimatedPullDrawerWrap = Animated.createAnimatedComponent(PullDrawerWrapper);
    const { isDrawerOpen } = this.state;
    const imageStyle = {
      height: 3.5,
      width: 80,
    };

    // Animated component with transform animations require styles to be passed inline
    return (
      <AnimatedPullDrawerHeader
        isAndroid={this.isAndroidDevice}
        style={{ transform: [{ scaleY: this.headerScale }] }}
        bottom={this.headerBottom}
        isDrawerOpen={isDrawerOpen}
      >
        <AnimatedPullDrawerWrap opacity={this.isAndroidDevice && this.drawerHandleOpacity}>
          <FastImage source={drawerPullIndicator} style={imageStyle} />
        </AnimatedPullDrawerWrap>
      </AnimatedPullDrawerHeader>
    );
  };

  render() {
    const {
      currentProduct,
      navigation,
      isOnModelAbTestPdp,
      checkForOOSForVariant,
      isNewReDesignEnabled,
      retrievedProductInfo,
      formValues,
      isQVModalOpen,
      socialProofMessage,
    } = this.props;
    const actionId = navigation.getParam('actionId');
    const viaModule = navigation.getParam('viaModule');
    const { currentColorEntry, showSize, drawerOpenStart } = this.state;
    let imageUrls = [];
    const product = this.getCurrentProduct();
    const colorNameAvailable = this.getFormName(formValues);
    const categoryId = this.getCatIdForRecommendation();
    const recommendationOosAttributes = {
      variation: 'moduleO',
      navigation,
      page: Constants.RECOMMENDATIONS_PAGES_MAPPING.PDP,
      partNumber: product?.productId,
      isHeaderAccordion: false,
      categoryName: categoryId,
      outOfStock: checkForOOSForVariant,
    };
    if (viaModule === 'mbox_Recommendation') {
      recommendationOosAttributes.actionId = actionId;
      recommendationOosAttributes.isRecProduct = true;
    }

    const updatedColorEntry = getMapSliceForColor(
      currentProduct?.colorFitsSizesMap || retrievedProductInfo?.colorFitsSizesMap,
      colorNameAvailable
    );
    if (currentProduct?.colorFitsSizesMap || retrievedProductInfo?.colorFitsSizesMap) {
      imageUrls = getImagesToDisplay({
        imagesByColor: currentProduct?.imagesByColor || retrievedProductInfo?.imagesByColor,
        curentColorEntry: !updatedColorEntry ? currentColorEntry : updatedColorEntry,
        isAbTestActive: isOnModelAbTestPdp,
        isFullSet: true,
      });
    }

    return (
      <>
        <Container>
          <BottomSheet
            snapPoints={this.snapPoints}
            enabledInnerScrolling
            enabledBottomClamp
            overdragResistanceFactor={1}
            enabledHeaderGestureInteraction
            enabledContentGestureInteraction
            enabledContentTapInteraction={!this.isAndroidDevice}
            ref={this.bottomSheet}
            renderContent={this.renderContent}
            renderHeader={this.renderHeader}
            callbackNode={this.animateValue}
            onOpenStart={() => this.setState({ drawerOpenStart: true })}
            onOpenEnd={() => this.setState({ isDrawerOpen: true, isDrawerClose: false })}
            onCloseStart={() => this.setState({ isDrawerOpen: false })}
            onCloseEnd={() => {
              this.setState({
                startAnimate: true,
                isDrawerClose: true,
                isDrawerOpen: false,
                drawerOpenStart: false,
              });
            }}
          />
          <AnimationContainer>
            <Margin margins="12px 12px 0 12px" paddings="0 0 12px 0">
              {this.renderShare()}
              {this.renderFavoriteIcon()}
            </Margin>
          </AnimationContainer>
          <Margin margins="12px 12px 0 12px" paddings="0 0">
            {this.renderImageCarousel()}
          </Margin>
          {this.renderCarousel(
            imageUrls,
            isNewReDesignEnabled,
            currentProduct?.colorFitsSizesMap || retrievedProductInfo?.colorFitsSizesMap
          )}
        </Container>
        <StickyContainer>
          {showSize && this.renderSizeDrawer()}
          {socialProofMessage?.priorityMsg && !drawerOpenStart && (
            <SocialProofWrapper
              height={this.getStickyContainerHeight()}
              zIndex={this.stickyContainerZindex + 1}
            >
              <SocialProofMessage socialProofMessage={socialProofMessage} />
            </SocialProofWrapper>
          )}
          {this.renderStickyButtons()}
        </StickyContainer>
        {!!this.isIOSApp && <AddedToBagContainer navigation={navigation} />}
        {isQVModalOpen && <QuickViewModal navigation={navigation} />}
      </>
    );
  }
}

ProductDetailNew.propTypes = ProductDetailNewPropTypes;

ProductDetailNew.defaultProps = {
  currentProduct: {
    colorFitsSizesMap: {},
    offerPrice: '',
    listPrice: '',
    generalProductId: '',
    imagesByColor: {},
    isGiftCard: false,
  },
  navigation: {},
  plpLabels: null,
  handleSubmit: null,
  isPickupModalOpen: false,
  handleFormSubmit: null,
  addToBagError: '',
  shortDescription: '',
  itemPartNumber: '',
  longDescription: '',
  pdpLabels: {},
  currency: 'USD',
  currencyExchange: 1,
  onAddItemToFavorites: null,
  isLoggedIn: false,
  alternateSizes: {},
  AddToFavoriteErrorMsg: '',
  removeAddToFavoritesErrorMsg: () => {},
  toastMessage: () => {},
  fireHapticFeedback: () => {},
  isKeepAliveEnabled: false,
  outOfStockLabels: {},
  bottomPromos: '',
  middlePromos: '',
  belowrecsPromos: '',
  accessibilityLabels: {},
  trackViewAction: () => {},
  setClickAnalyticsDataAction: () => {},
  isOnModelAbTestPdp: false,
  setExternalCampaignFired: () => {},
  isBrierleyPromoEnabled: false,
  isOosPdpEnabled: false,
  isPdp1andPdp2Enabled: true,
  disableMultiPackTab: false,
  defaultWishListFromState: {},
  onSetLastDeletedItemIdAction: () => {},
  deleteFavItemInProgressFlag: false,
  AddToCartError: () => {},
  availableTCPmapNewStyleId: [],
  isBundleProduct: false,
  isFavorite: false,
  stylisticsProductTabList: {},
  retrievedProductInfo: {},
  isLoading: false,
  isProductPickup: true,
  userNearByStore: [],
  userDefaultStore: {
    storeBossInfo: {
      startDate: '',
      endDate: '',
    },
    basicInfo: {
      address: '',
      storeName: '',
    },
  },
  isBossBopisPDPSizeDrawer: false,
  resetBopisInventoryData: () => {},
  socialProofMessage: '',
  isSelectedSizeDisabled: false,
  updateSizeStatus: () => {},
};

export default gestureHandlerRootHOC(withStyles(ProductDetailNew));

export { ProductDetailNew as ProductDetailNewVanilla };
