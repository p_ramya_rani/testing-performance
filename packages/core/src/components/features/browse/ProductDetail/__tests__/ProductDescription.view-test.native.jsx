import React from 'react';
import { shallow } from 'enzyme';
import { ProductDetailDescriptionVanilla } from '../molecules/ProductDescription/views/ProductDescription.view.native';

describe('ProductDetailDescriptionView native should render correctly', () => {
  let wrapper;

  beforeEach(() => {
    const props = {
      longDescription: '',
      shortDescription: '',
      pdpLabels: {},
      itemPartNumber: '',
      margins: null,
      isShowMore: true,
      productInfo: {
        colorFitsSizesMap: [
          {
            color: {
              name: 'BLACK',
              colorDisplayId: '2103349_JP',
            },
          },
        ],
        TCPStyleColor: ['BLACK|TRUE INDIGO#2103349_JP'],
      },
      color: 'BLACK',
    };
    wrapper = shallow(<ProductDetailDescriptionVanilla {...props} />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe('ProductDetailDescriptionView component', () => {
  let wrapper;

  beforeEach(() => {
    const props = {
      longDescription: '',
      shortDescription: '',
      pdpLabels: {},
      itemPartNumber: '',
      margins: null,
      isShowMore: false,
      productInfo: {
        colorFitsSizesMap: [
          {
            color: {
              name: 'BLACK',
              colorDisplayId: '2103349_JP',
            },
          },
        ],
        TCPStyleColor: ['BLACK|TRUE INDIGO#2103349_JP'],
      },
      color: 'BLACK',
    };
    wrapper = shallow(<ProductDetailDescriptionVanilla {...props} />);
  });

  it('should renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe('ProductDetailDescriptionView native should render correctly with isFromBundleProductView prop', () => {
  let component;
  const props = {
    longDescription: '',
    shortDescription: '',
    pdpLabels: {},
    itemPartNumber: '',
    margins: null,
    isShowMore: false,
    productInfo: {
      colorFitsSizesMap: [
        {
          color: {
            name: 'BLACK',
            colorDisplayId: '2103349_JP',
          },
        },
      ],
      TCPStyleColor: ['BLACK|TRUE INDIGO#2103349_P'],
    },
    color: 'BLACK',
    isFromBundleProductView: true,
  };
  beforeEach(() => {
    component = shallow(<ProductDetailDescriptionVanilla {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should renders correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
