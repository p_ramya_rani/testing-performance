// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ProductDetailContainerVanilla } from '../container/ProductDetail.container.native';

const basicImageUrlOne = '3010436/3010436_01.mp4';
const basicImageUrlTwo = '3010436/3010436_01-1.mp4';
const basicImageUrlThree = '3010436/3010436_01-1.jpg';
const testProd = {
  ratingsProductId: '3010436',
  generalProductId: '3010436_01',
  categoryId: '47503>47539',
  category: '',
  name: 'Boys Ninja Video Game Backpack',
  pdpUrl: '/p/3010436_01',
  longDescription:
    "<li>Made of 100% polyester</li><li>Zip closure</li><li>'High Score' with ninja video game graphic design at front</li><li>Front zip pocket with video game controller graphic design</li><li>Side mesh pockets for storing water bottles, gadgets and more!</li><li>Locker loop</li><li>Adjustable padded shoulder straps</li><li>Measures approx.11 x 5 x 16 inches</li><li>Recommended for ages 4 and up</li><li>Imported</li>",
  imagesByColor: {
    BLACK: {
      basicImageUrl: basicImageUrlOne,
      extraImages: [
        {
          isShortImage: false,
          isOnModalImage: false,
          iconSizeImageUrl: basicImageUrlOne,
          listingSizeImageUrl: basicImageUrlOne,
          regularSizeImageUrl: basicImageUrlOne,
          bigSizeImageUrl: basicImageUrlOne,
          superSizeImageUrl: basicImageUrlOne,
        },
        {
          isShortImage: false,
          isOnModalImage: false,
          iconSizeImageUrl: basicImageUrlTwo,
          listingSizeImageUrl: basicImageUrlTwo,
          regularSizeImageUrl: basicImageUrlTwo,
          bigSizeImageUrl: basicImageUrlTwo,
          superSizeImageUrl: basicImageUrlTwo,
        },
        {
          isShortImage: false,
          isOnModalImage: false,
          iconSizeImageUrl: basicImageUrlThree,
          listingSizeImageUrl: basicImageUrlThree,
          regularSizeImageUrl: basicImageUrlThree,
          bigSizeImageUrl: basicImageUrlThree,
          superSizeImageUrl: basicImageUrlThree,
        },
      ],
    },
  },
  colorFitsSizesMap: [
    {
      color: {
        name: 'BLACK',
        imagePath: null,
        family: 'BLACK',
        swatchImage: '3010436_01_swatch.jpg',
      },
      pdpUrl: '/p/3010436_01',
      pdpSeoUrl: 'Boys--High-Score--Ninja-Video-Game-Backpack-3010436-01',
      name: null,
      colorProductId: '1448001',
      colorDisplayId: '3010436_01',
      categoryEntity: '',
      imageName: '3010436_01',
      favoritedCount: 262,
      maxAvailable: 4396,
      maxAvailableBoss: 3666,
      hasFits: false,
      miscInfo: {
        isBopisEligible: false,
        isBossEligible: true,
        badge1: {
          defaultBadge: 'ONLINE EXCLUSIVE',
        },
        badge2: '40% OFF',
        keepAlive: false,
        isInDefaultWishlist: false,
      },
      fits: [
        {
          fitName: '',
          isDefault: true,
          maxAvailable: 1.7976931348623157e308,
          sizes: [
            {
              sizeName: 'ONE SIZE',
              skuId: '1448148',
              listPrice: 36.95,
              offerPrice: 22.17,
              maxAvailable: 4396,
              maxAvailableBoss: 3666,
              variantId: '00193511822015',
              variantNo: '3010436001',
              position: 0,
            },
          ],
        },
      ],
      listPrice: 36.95,
      offerPrice: 22.17,
      highListPrice: 0,
      highOfferPrice: 0,
      lowListPrice: 36.95,
      lowOfferPrice: 22.17,
    },
  ],
  isGiftCard: false,
  colorFitSizeDisplayNames: null,
  listPrice: 36.95,
  offerPrice: 22.17,
  highListPrice: 0,
  highOfferPrice: 0,
  lowListPrice: 36.95,
  lowOfferPrice: 22.17,
  onlyListPrice: 36.95,
  ratings: 4.8,
  reviewsCount: 32,
  unbxdProdId: '3010436_01',
  alternateSizes: {},
  bazaarVoice: {
    avgRating: 4.764705882352941,
    totalReviewCount: 34,
  },
};

describe.skip('ProductDetailContainerVanilla component', () => {
  it('should renders  correctly', () => {
    const props = {
      currentProduct: {},
      breadCrumbs: {},
      navTree: {},
      getDetails: jest.fn(),
      navigation: '',
      wishlistsSummaries: '',
      getActiveWishlist: jest.fn(),
    };
    const component = shallow(<ProductDetailContainerVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should are product to the bag', () => {
    const props = {
      addToBagEcom: jest.fn(),
      formValues: {
        color: 'BLACK',
        size: 10,
        quantity: 1,
        fit: null,
      },
      breadCrumbs: {},
      navTree: {},
      getDetails: jest.fn(),
      wishlistsSummaries: '',
      getActiveWishlist: jest.fn(),
      navigation: {
        addListener: () => {},
        getParam: () => {},
        state: {
          params: {
            isSBP: true,
          },
        },
      },
      currentProduct: testProd,
    };
    const formValuesSbp = undefined;
    const component = shallow(<ProductDetailContainerVanilla {...props} />);
    component.instance().handleAddToBag(formValuesSbp, testProd);
    expect(props.addToBagEcom).toHaveBeenCalled();
  });
});
