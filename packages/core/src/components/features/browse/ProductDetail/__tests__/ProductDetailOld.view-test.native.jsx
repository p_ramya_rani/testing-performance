/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import * as dependency from '@tcp/core/src/utils';
import { ProductDetailOldVanilla } from '../views/ProductDetailOld.view.native';

const sbpTestProps = {
  plpLabels: { addToBag: 'ADD TO BAG' },
  sbpLabels: {
    lbl_sbp_sticky_size_btn_pdp: 'SIZE',
    lbl_sbp_add_to_bag_btn_pdp: 'ADD TO BAG',
    lbl_sbp_select_size_header_drawer: 'SELECT SIZE',
    lbl_sbp_select_size_fit_pdp: 'Please select a size/fit.',
  },
  selectedColorProductId: '',
  currentProduct: {
    name: '',
    shortDescription: '',
    generalProductId: '12112_11',
    colorFitsSizesMap: [
      {
        favoritedCount: 0,
        color: {
          name: 'black',
        },
        fits: [
          {
            sizes: [
              {
                sizeName: '4',
              },
            ],
          },
        ],
      },
    ],
    bazaarVoice: {
      totalReviewCount: 3,
    },
  },
  navigation: {
    getParam: () => {},
    getScreenProps: () => {
      return { deviceTier: 'tier1' };
    },
    setParams: jest.fn(),
    state: {
      params: {
        isSBP: true,
        brandId: 'gym',
      },
    },
  },
  trackPageLoad: () => {},
  isDynamicBadgeEnabled: true,
  TCPStyleType: '0002',
  TCPStyleQTY: 10,
  formValues: {},
  handleFormSubmit: jest.fn(),
  ledgerSummaryData: {
    subTotal: 0,
  },
  addBtnMsg: false,
  AddToCartError: jest.fn(),
  setClickAnalyticsDataAction: jest.fn(),
};

const testProduct = {
  ratingsProductId: '3015764',
  categoryId: '47503>454005',
  name: 'Boys Football Graphic Tee 2-Pack',
  pdpUrl: '/p/3015764_BQ',
  colorFitsSizesMap: [
    {
      color: {
        name: 'MULTI CLR',
        imagePath: null,
        family: 'MULTI',
        swatchImage: '3015764_BQ_swatch.jpg',
      },
      fits: [
        {
          fitName: '',
          isDefault: true,
          maxAvailable: 1.7976931348623157e308,
          sizes: [
            {
              sizeName: 'XS (4)',
              skuId: '1629451',
              listPrice: 19.95,
              offerPrice: 7.98,
              maxAvailable: 507,
              maxAvailableBoss: 185,
              variantId: '00194936199706',
              variantNo: '3015764001',
              position: 0,
            },
            {
              sizeName: 'S (5/6)',
              skuId: '1630010',
              listPrice: 19.95,
              offerPrice: 7.98,
              maxAvailable: 2003,
              maxAvailableBoss: 751,
              variantId: '00194936199713',
              variantNo: '3015764002',
              position: 1,
            },
            {
              sizeName: 'M (7/8)',
              skuId: '1628851',
              listPrice: 19.95,
              offerPrice: 7.98,
              maxAvailable: 2711,
              maxAvailableBoss: 862,
              variantId: '00194936199720',
              variantNo: '3015764003',
              position: 2,
            },
          ],
        },
      ],
      listPrice: 19.95,
      offerPrice: 7.98,
      highListPrice: 0,
      highOfferPrice: 0,
      lowListPrice: 19.95,
      lowOfferPrice: 7.98,
    },
  ],
};

const productData = [
  {
    colorId: '3010436_01',
    color: ['BLACK'],
    id: '3010436',
    outfitId: '',
    price: 36.95,
    listPrice: 36.95,
    extPrice: 22.17,
    rating: 4.8,
    reviews: 32,
    plpClick: false,
    searchClick: false,
    recsProductId: '3010436_01',
    recsPageType: '',
    recsType: '',
    name: 'Boys Ninja Video Game Backpack',
    paidPrice: 22.17,
    position: null,
    pricingState: 'on sale',
    features: 'alt images full size image',
    prodBrand: 'tcp',
    cartPrice: 36.95,
  },
];

describe('ProductDetailOld component', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      trackPageLoad: () => {},
    };
    wrapper = shallow(<ProductDetailOldVanilla {...props} />);
  });

  it('should renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('should render image badge correctly', () => {
    const props1 = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      trackPageLoad: () => {},
      isDynamicBadgeEnabled: true,
      TCPStyleType: '0002',
      TCPStyleQTY: 10,
    };
    wrapper = shallow(<ProductDetailOldVanilla {...props1} />);
  });

  it('should render correctly on SBP on open drawer', () => {
    wrapper.setState({ showSize: true });
    expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly with SBP flag', () => {
    const props2 = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        state: {
          params: {
            isSBP: true,
          },
        },
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      trackPageLoad: () => {},
      isDynamicBadgeEnabled: true,
      TCPStyleType: '0002',
      TCPStyleQTY: 10,
    };
    wrapper = shallow(<ProductDetailOldVanilla {...props2} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should return the product category ids', () => {
    const testProduct3 = {
      ratingsProductId: '3019350',
      generalProductId: '3019350_BQ',
      categoryId: '47504>714003',
      category: '',
      name: 'B SAFARI 5PK MULTI CLR UPTO7LBS._BQ',
      pdpUrl: '/p/3019350_BQ',
      isGiftCard: false,
      colorFitSizeDisplayNames: null,
      listPrice: 32.95,
      offerPrice: 23.07,
      highListPrice: 0,
      highOfferPrice: 0,
      lowListPrice: 32.95,
      lowOfferPrice: 23.07,
      onlyListPrice: 32.95,
      ratings: 4.8,
      reviewsCount: 12,
      unbxdProdId: '3019350_BQ',
      alternateSizes: {},
      productId: '3019350_BQ',
      product_long_description: '',
      bundleProducts: [],
      categoryPathMap: ['47504>714003>714005|Baby>Tops>Short Sleeve'],
      multiPackUSStore: false,
    };

    const expectedResult = ['714003'];
    expect(wrapper.instance().getProductCategoryID(testProduct3)).toEqual(expectedResult);
  });

  it('should extract sizes from SBP profile', () => {
    const categoryName = 'Tops & Bodysuits';
    const testSBPProfile = {
      id: '39f0e736-602d-4611-ac4f-83be41c3e17d',
      theme: 6,
      size:
        '{"Tops & Bodysuits":{"size":["9-12 M"]},"Bottoms":{"size":["9-12 M"]},"Rompers":{"size":["12-18 M"]},"Pajamas":{"size":["12-18 M"]},"Shoes":{"size":["3-6 M"]}}',
      relation: 'Parent',
      gender: 'BABY BOY',
      name: 'Test90',
      fit: 'REGULAR',
      month: 'August',
      shoe: 'TODDLER 1',
      year: '2019',
      interests:
        'Hearts,Humor & Emojis,Gaming,Food,Dinos,Butterflies,Animals,Books & Education,Jackets & Outerwear,Shoes,Tops,Sets,Bodysuits',
      styles: '0',
      favorite_colors: 'pink,blue',
      refresh: true,
      isSBP: true,
      index: 0,
    };
    const expectedResult = {
      profileSize: ['9-12 M'],
      profileFit: '',
    };

    expect(wrapper.instance().extractSizesFromSBPProfile(categoryName, testSBPProfile)).toEqual(
      expectedResult
    );
  });

  it('should return ONE SIZE if product is ONE SIZE accessory', () => {
    const testProduct1 = {
      ratingsProductId: '3010436',
      categoryId: '47503>47539',
      name: 'Boys Ninja Video Game Backpack',
      colorFitsSizesMap: [
        {
          color: {
            name: 'BLACK',
            imagePath: null,
            family: 'BLACK',
            swatchImage: '3010436_01_swatch.jpg',
          },
          pdpUrl: '/p/3010436_01',
          pdpSeoUrl: 'Boys--High-Score--Ninja-Video-Game-Backpack-3010436-01',
          name: null,
          colorProductId: '1448001',
          colorDisplayId: '3010436_01',
          categoryEntity: '',
          imageName: '3010436_01',
          favoritedCount: 262,
          maxAvailable: 4396,
          maxAvailableBoss: 3666,
          hasFits: false,
          fits: [
            {
              fitName: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'ONE SIZE',
                  skuId: '1448148',
                  listPrice: 36.95,
                  offerPrice: 22.17,
                  maxAvailable: 4396,
                  maxAvailableBoss: 3666,
                  variantId: '00193511822015',
                  variantNo: '3010436001',
                  position: 0,
                },
              ],
            },
          ],
          listPrice: 36.95,
          offerPrice: 22.17,
          highListPrice: 0,
          highOfferPrice: 0,
          lowListPrice: 36.95,
          lowOfferPrice: 22.17,
        },
      ],
      isGiftCard: false,
      colorFitSizeDisplayNames: null,
      listPrice: 36.95,
      offerPrice: 22.17,
      highListPrice: 0,
      highOfferPrice: 0,
      lowListPrice: 36.95,
      lowOfferPrice: 22.17,
      onlyListPrice: 36.95,
    };
    const expectedResult = 'ONE SIZE';
    expect(wrapper.instance().isProductAnAccessory(testProduct1)).toEqual(expectedResult);
  });

  it('should return an empty string if product not ONE SIZE accessory', () => {
    const testProduct2 = {
      ratingsProductId: '3015764',
      categoryId: '47503>454005',
      name: 'Boys Football Graphic Tee 2-Pack',
      pdpUrl: '/p/3015764_BQ',
      colorFitsSizesMap: [
        {
          color: {
            name: 'MULTI CLR',
            imagePath: null,
            family: 'MULTI',
            swatchImage: '3015764_BQ_swatch.jpg',
          },
          fits: [
            {
              fitName: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'XS (4)',
                  skuId: '1629451',
                  listPrice: 19.95,
                  offerPrice: 7.98,
                  maxAvailable: 507,
                  maxAvailableBoss: 185,
                  variantId: '00194936199706',
                  variantNo: '3015764001',
                  position: 0,
                },
                {
                  sizeName: 'S (5/6)',
                  skuId: '1630010',
                  listPrice: 19.95,
                  offerPrice: 7.98,
                  maxAvailable: 2003,
                  maxAvailableBoss: 751,
                  variantId: '00194936199713',
                  variantNo: '3015764002',
                  position: 1,
                },
                {
                  sizeName: 'M (7/8)',
                  skuId: '1628851',
                  listPrice: 19.95,
                  offerPrice: 7.98,
                  maxAvailable: 2711,
                  maxAvailableBoss: 862,
                  variantId: '00194936199720',
                  variantNo: '3015764003',
                  position: 2,
                },
              ],
            },
          ],
          listPrice: 19.95,
          offerPrice: 7.98,
          highListPrice: 0,
          highOfferPrice: 0,
          lowListPrice: 19.95,
          lowOfferPrice: 7.98,
        },
      ],
    };
    const expectedResult = '';
    expect(wrapper.instance().isProductAnAccessory(testProduct2)).toEqual(expectedResult);
  });

  it('shows the correct text in size button if not an accessory', () => {
    const testSize = 10;
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    wrapper.setState({ size: testSize });
    const expectedResult = `${sbpTestProps.sbpLabels.lbl_sbp_sticky_size_btn_pdp} ${testSize}`;
    expect(wrapper.instance().showPreSelectedSize()).toEqual(expectedResult);
  });

  it('shows correct text in size text when product is an accessory', () => {
    const testSize = 'ONE SIZE';
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    wrapper.setState({ accessorySize: testSize, size: testSize });
    const expectedResult = `${sbpTestProps.sbpLabels.lbl_sbp_sticky_size_btn_pdp} - ${testSize}`;
    expect(wrapper.instance().showPreSelectedSize()).toEqual(expectedResult);
  });

  it('should not show size if not available for preselect', () => {
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    const expectedResult = sbpTestProps.sbpLabels.lbl_sbp_sticky_size_btn_pdp;
    expect(wrapper.instance().showPreSelectedSize()).toEqual(expectedResult);
  });

  it('should render correct styling when sbp size drawer is open', () => {
    wrapper.setState({ showSize: true });
    const expectedResult = {
      flexDirection: 'column',
      height: 'auto',
      borderWidth: 0,
      borderRadius: 30,
    };
    expect(wrapper.instance().stickyButtonContainerStyling()).toEqual(expectedResult);
  });

  it('should render correct styling when sbp size drawer is closed', () => {
    const expectedResult = { flexDirection: 'row', height: 100, borderWidth: 1, borderRadius: 0 };
    expect(wrapper.instance().stickyButtonContainerStyling()).toEqual(expectedResult);
  });

  it('should return correct styled view for bottom of screen when is sbp', () => {
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    expect(wrapper.instance().sbpStylingForBottom()).toMatchSnapshot();
  });

  it('should return correct styled view for bottom of screen when sbp is not enabled', () => {
    expect(wrapper.instance().sbpStylingForBottom()).toMatchSnapshot();
  });

  it('should return correct add to bag text for sbp', () => {
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    expect(wrapper.instance().addToBagTextSBP()).toMatchSnapshot();
  });

  it('should render sticky add to bag button on sbp', () => {
    const testObj = {
      currentProduct: testProduct,
      productData,
      pageShortName: 'product:3010436:ninja video game backpack',
      pageName: 'product:3010436:boys ninja video game backpack',
    };
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    expect(wrapper.instance().checkaddtobag(testObj)).toMatchSnapshot();
  });

  it('should call setState when closing drawer', () => {
    const setStateSpy = jest.spyOn(ProductDetailOldVanilla.prototype, 'setState');
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    wrapper.instance().closeSizeDrawerSBP();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should render sticky buttons on sbp', () => {
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    expect(wrapper.instance().renderSBPStickyButtons()).toMatchSnapshot();
  });

  it('should render header in sbp size drawer', () => {
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    expect(wrapper.instance().renderSBPSizeDrawerHeader()).toMatchSnapshot();
  });

  it('should render size drawer on sbp', () => {
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    expect(wrapper.instance().renderSizeDrawerSBP()).toMatchSnapshot();
  });

  it('should give an error if no size is selected', () => {
    const pageShortName = 'product:3010436:boys ninja game backpack';
    const pageName = 'product:3010436:boys ninja game backpack';
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    wrapper.instance().handleSizeClick(testProduct, productData, pageName, pageShortName);
    expect(sbpTestProps.AddToCartError).toHaveBeenCalled();
  });

  it('should call handleFormSubmit when addToBag is pressed and size is present', () => {
    const pageShortName = 'product:3010436:boys video game backpack';
    const pageName = 'product:3010436:boys video game backpack';
    const setStateSpy = jest.spyOn(ProductDetailOldVanilla.prototype, 'setState');
    wrapper = shallow(<ProductDetailOldVanilla {...sbpTestProps} />);
    wrapper.setState({ size: '10' });
    wrapper.instance().handleSizeClick(testProduct, productData, pageName, pageShortName);
    expect(sbpTestProps.handleFormSubmit).toHaveBeenCalled();
    expect(setStateSpy).toHaveBeenCalled();
  });
  it('should map categories by ID', async () => {
    const instance = wrapper.instance();
    Object.defineProperty(dependency, 'utils', {
      getValueFromAsyncStorage: () => {
        return JSON.stringify({ json: {} });
      },
    });
    const parse = jest
      .spyOn(JSON, 'parse')
      .mockImplementation(() => ({ json: { SBPCategoryMapping: [] } }));
    const getProductCategoryIdSpy = jest.spyOn(instance, 'getProductCategoryID');
    const res = await instance.categoryIDMapping(
      {},
      {
        gender: 'BOY',
      }
    );
    expect(parse).toHaveBeenCalled();
    expect(getProductCategoryIdSpy).toHaveBeenCalled();
    expect(res).toBeNull();
  });
  it('should get size mapping', async () => {
    const instance = wrapper.instance();
    Object.defineProperty(dependency, 'utils', {
      getValueFromAsyncStorage: () => {
        return JSON.stringify({ json: {} });
      },
    });
    const parse = jest.spyOn(JSON, 'parse').mockImplementation(() => []);
    const product = {
      colorFitsSizesMap: [
        {
          favoritedCount: 0,
          color: {
            name: 'black',
          },
          fits: [
            {
              sizes: [
                {
                  sizeName: '4',
                },
              ],
            },
          ],
        },
      ],
    };
    const res = await instance.getSizeMapping(product, ['4'], 'Regular', 'Tops');
    expect(parse).toHaveBeenCalled();
    expect(res).toBeNull();
  });
});
