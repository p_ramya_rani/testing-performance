/* eslint-disable max-lines */
/* eslint-disable max-statements */
/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import * as dependency from '@tcp/core/src/utils';
import { ProductDetailNewVanilla } from '../views/ProductDetailNew.view.native';

const selectSize = 'Select a Size';
const findInStore = 'Find In Store';
const onlineExclusive = 'ONLINE EXCLUSIVE';
describe.skip('ProductDetailNew component', () => {
  const newProps = {
    plpLabels: { addToBag: '' },
    pdpLabels: { selectValueLabel: 'Select a Value', selectSizeLabel: selectSize },
    selectedColorProductId: '',
    currentProduct: {
      name: '',
      shortDescription: '',
      isGiftCard: true,
      generalProductId: '12112_11',
      colorFitsSizesMap: [
        {
          favoritedCount: 0,
          color: {
            name: 'black',
          },
          fits: [
            {
              sizes: [
                {
                  sizeName: '4',
                },
              ],
            },
          ],
        },
      ],
      bazaarVoice: {
        totalReviewCount: 3,
      },
    },
    navigation: {
      getParam: () => {},
      setParams: jest.fn(),
      getScreenProps: () => {
        return { deviceTier: 'tier1' };
      },
    },
    trackPageLoad: () => {},
  };

  let wrapper;
  beforeEach(() => {
    const props = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      trackPageLoad: () => {},
    };
    wrapper = shallow(<ProductDetailNewVanilla {...props} />);
  });

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should render image badge correctly', () => {
    const props1 = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      trackPageLoad: () => {},
      isDynamicBadgeEnabled: true,
      TCPStyleType: '0002',
      TCPStyleQTY: 10,
    };
    wrapper = shallow(<ProductDetailNewVanilla {...props1} />);
  });

  it('should render correctly with SBP flag', () => {
    const props2 = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        state: {
          params: {
            isSBP: true,
          },
        },
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      trackPageLoad: () => {},
      isDynamicBadgeEnabled: true,
      TCPStyleType: '0002',
      TCPStyleQTY: 10,
    };
    wrapper = shallow(<ProductDetailNewVanilla {...props2} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should return the product category ids', () => {
    const testProduct3 = {
      ratingsProductId: '3019350',
      generalProductId: '3019350_BQ',
      categoryId: '47504>714003',
      category: '',
      name: 'B SAFARI 5PK MULTI CLR UPTO7LBS._BQ',
      pdpUrl: '/p/3019350_BQ',
      isGiftCard: false,
      colorFitSizeDisplayNames: null,
      listPrice: 32.95,
      offerPrice: 23.07,
      highListPrice: 0,
      highOfferPrice: 0,
      lowListPrice: 32.95,
      lowOfferPrice: 23.07,
      onlyListPrice: 32.95,
      ratings: 4.8,
      reviewsCount: 12,
      unbxdProdId: '3019350_BQ',
      alternateSizes: {},
      productId: '3019350_BQ',
      product_long_description: '',
      bundleProducts: [],
      categoryPathMap: ['47504>714003>714005|Baby>Tops>Short Sleeve'],
      multiPackUSStore: false,
    };

    const expectedResult = ['714003'];
    expect(wrapper.instance().getProductCategoryID(testProduct3)).toEqual(expectedResult);
  });

  it('should extract sizes from SBP profile', () => {
    const categoryName = 'Tops & Bodysuits';
    const testSBPProfile = {
      id: '39f0e736-602d-4611-ac4f-83be41c3e17d',
      theme: 6,
      size: '{"Tops & Bodysuits":{"size":["9-12 M"]},"Bottoms":{"size":["9-12 M"]},"Rompers":{"size":["12-18 M"]},"Pajamas":{"size":["12-18 M"]},"Shoes":{"size":["3-6 M"]}}',
      relation: 'Parent',
      gender: 'BABY BOY',
      name: 'Test90',
      fit: 'REGULAR',
      month: 'August',
      shoe: 'TODDLER 1',
      year: '2019',
      interests:
        'Hearts,Humor & Emojis,Gaming,Food,Dinos,Butterflies,Animals,Books & Education,Jackets & Outerwear,Shoes,Tops,Sets,Bodysuits',
      styles: '0',
      favorite_colors: 'pink,blue',
      refresh: true,
      isSBP: true,
      index: 0,
    };
    const expectedResult = {
      profileSize: ['9-12 M'],
      profileFit: '',
    };

    expect(wrapper.instance().extractSizesFromSBPProfile(categoryName, testSBPProfile)).toEqual(
      expectedResult
    );
  });

  it('should return ONE SIZE if product is ONE SIZE accessory', () => {
    const testProduct1 = {
      ratingsProductId: '3010436',
      categoryId: '47503>47539',
      name: 'Boys Ninja Video Game Backpack',
      colorFitsSizesMap: [
        {
          color: {
            name: 'BLACK',
            imagePath: null,
            family: 'BLACK',
            swatchImage: '3010436_01_swatch.jpg',
          },
          pdpUrl: '/p/3010436_01',
          pdpSeoUrl: 'Boys--High-Score--Ninja-Video-Game-Backpack-3010436-01',
          name: null,
          colorProductId: '1448001',
          colorDisplayId: '3010436_01',
          categoryEntity: '',
          imageName: '3010436_01',
          favoritedCount: 262,
          maxAvailable: 4396,
          maxAvailableBoss: 3666,
          hasFits: false,
          fits: [
            {
              fitName: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'ONE SIZE',
                  skuId: '1448148',
                  listPrice: 36.95,
                  offerPrice: 22.17,
                  maxAvailable: 4396,
                  maxAvailableBoss: 3666,
                  variantId: '00193511822015',
                  variantNo: '3010436001',
                  position: 0,
                },
              ],
            },
          ],
          listPrice: 36.95,
          offerPrice: 22.17,
          highListPrice: 0,
          highOfferPrice: 0,
          lowListPrice: 36.95,
          lowOfferPrice: 22.17,
        },
      ],
      isGiftCard: false,
      colorFitSizeDisplayNames: null,
      listPrice: 36.95,
      offerPrice: 22.17,
      highListPrice: 0,
      highOfferPrice: 0,
      lowListPrice: 36.95,
      lowOfferPrice: 22.17,
      onlyListPrice: 36.95,
    };
    const expectedResult = 'ONE SIZE';
    expect(wrapper.instance().isProductAnAccessory(testProduct1)).toEqual(expectedResult);
  });

  it('should return an empty string if product not ONE SIZE accessory', () => {
    const testProduct2 = {
      ratingsProductId: '3015764',
      categoryId: '47503>454005',
      name: 'Boys Football Graphic Tee 2-Pack',
      pdpUrl: '/p/3015764_BQ',
      colorFitsSizesMap: [
        {
          color: {
            name: 'MULTI CLR',
            imagePath: null,
            family: 'MULTI',
            swatchImage: '3015764_BQ_swatch.jpg',
          },
          fits: [
            {
              fitName: '',
              isDefault: true,
              maxAvailable: 1.7976931348623157e308,
              sizes: [
                {
                  sizeName: 'XS (4)',
                  skuId: '1629451',
                  listPrice: 19.95,
                  offerPrice: 7.98,
                  maxAvailable: 507,
                  maxAvailableBoss: 185,
                  variantId: '00194936199706',
                  variantNo: '3015764001',
                  position: 0,
                },
                {
                  sizeName: 'S (5/6)',
                  skuId: '1630010',
                  listPrice: 19.95,
                  offerPrice: 7.98,
                  maxAvailable: 2003,
                  maxAvailableBoss: 751,
                  variantId: '00194936199713',
                  variantNo: '3015764002',
                  position: 1,
                },
                {
                  sizeName: 'M (7/8)',
                  skuId: '1628851',
                  listPrice: 19.95,
                  offerPrice: 7.98,
                  maxAvailable: 2711,
                  maxAvailableBoss: 862,
                  variantId: '00194936199720',
                  variantNo: '3015764003',
                  position: 2,
                },
              ],
            },
          ],
          listPrice: 19.95,
          offerPrice: 7.98,
          highListPrice: 0,
          highOfferPrice: 0,
          lowListPrice: 19.95,
          lowOfferPrice: 7.98,
        },
      ],
    };
    const expectedResult = '';
    expect(wrapper.instance().isProductAnAccessory(testProduct2)).toEqual(expectedResult);
  });

  it('should map categories by ID', async () => {
    const instance = wrapper.instance();
    Object.defineProperty(dependency, 'utils', {
      getValueFromAsyncStorage: () => {
        return JSON.stringify({ json: {} });
      },
    });
    const parse = jest
      .spyOn(JSON, 'parse')
      .mockImplementation(() => ({ json: { SBPCategoryMapping: [] } }));
    const getProductCategoryIdSpy = jest.spyOn(instance, 'getProductCategoryID');
    const res = await instance.categoryIDMapping(
      {},
      {
        gender: 'BOY',
      }
    );
    expect(parse).toHaveBeenCalled();
    expect(getProductCategoryIdSpy).toHaveBeenCalled();
    expect(res).toBeNull();
  });
  it('should get size mapping', async () => {
    const instance = wrapper.instance();
    Object.defineProperty(dependency, 'utils', {
      getValueFromAsyncStorage: () => {
        return JSON.stringify({ json: {} });
      },
    });
    const parse = jest.spyOn(JSON, 'parse').mockImplementation(() => []);
    const product = {
      colorFitsSizesMap: [
        {
          favoritedCount: 0,
          color: {
            name: 'black',
          },
          fits: [
            {
              sizes: [
                {
                  sizeName: '4',
                },
              ],
            },
          ],
        },
      ],
    };
    const res = await instance.getSizeMapping(product, ['4'], 'Regular', 'Tops');
    expect(parse).toHaveBeenCalled();
    expect(res).toBeNull();
  });

  it('should call setState when setShowCompleteTheLook', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().setShowCompleteTheLook(true);
    expect(setStateSpy).not.toHaveBeenCalled();
  });

  it('should call setState when onImageClick', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().onImageClick();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should call setState when callbackExpandReviewSection', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().callbackExpandReviewSection();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should call setState when comingFromFullScreen', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().comingFromFullScreen();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should render top badge if available correctly', () => {
    expect(wrapper.instance().renderTopBadge('Online')).toMatchSnapshot();
  });

  it('should not render top badge', () => {
    expect(wrapper.instance().renderTopBadge('')).toBeNull();
  });

  it('should render top title', () => {
    expect(wrapper.instance().renderTopTitle('Shirt')).toBeTruthy();
  });

  it('should not render top title', () => {
    expect(wrapper.instance().renderTopTitle('')).toBeNull();
  });

  it('should call setState when hideLoginModal', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().hideLoginModal();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should return null if productId not present', () => {
    const expected = { TCPStyleQTY: undefined, TCPStyleType: undefined };
    expect(wrapper.instance().getCurrentProduct()).toEqual(expected);
  });

  it('should return false if pdp or mbox_Reccomendation is not present', () => {
    const props4 = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        state: {
          params: {
            isSBP: true,
          },
        },
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      trackPageLoad: () => {},
      isDynamicBadgeEnabled: true,
      TCPStyleType: '0002',
      TCPStyleQTY: 10,
    };
    wrapper = shallow(<ProductDetailNewVanilla {...props4} />);
    expect(wrapper.instance().isFastImageDisableForRecommendations()).toBeFalsy();
  });

  it('should render favorite count correctly', () => {
    expect(wrapper.instance().renderFavoriteCount(false, true)).toMatchSnapshot();
  });

  it('should render selected heart icon correctly', () => {
    expect(wrapper.instance().renderSelectedHeartIcon('12112_11')).toMatchSnapshot();
  });

  it('should return falsy if deleteFavItemInProgressFlag is true', () => {
    wrapper.setProps({ deleteFavItemInProgressFlag: true });
    expect(wrapper.instance().onFavoriteRemove()).toBeUndefined();
  });

  it('should render unselected heart icon correctly', () => {
    expect(wrapper.instance().renderUnselectedHeartIcon('12112_11', '123456')).toMatchSnapshot();
  });

  it('should return falsy if onFavorite is true', () => {
    wrapper.setProps({ deleteFavItemInProgressFlag: true });
    expect(wrapper.instance().onFavorite()).toBeUndefined();
  });

  it('should return color from formValues', () => {
    const formValues = {
      color: 'BLUE',
    };
    const expected = 'BLUE';
    expect(wrapper.instance().getFormName(formValues)).toEqual(expected);
  });

  it('should call setState when closeSizeDrawer', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().closeSizeDrawer();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should call setState when closeRatingDrawer', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().closeRatingDrawer();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should call updateSizeValue within updateFormValuesOnProductChange method', () => {
    const updateSizeValue = jest.fn();
    wrapper.setProps({ updateSizeValue });
    wrapper.instance().updateFormValuesOnProductChange();
    expect(updateSizeValue).toHaveBeenCalledTimes(2);
  });

  it('should return correct gender', () => {
    const breadcrumbs = [{ displayName: 'Girl' }];
    const expectedResult = 'girl';
    expect(wrapper.instance().getGender(breadcrumbs)).toEqual(expectedResult);
  });

  it('should return empty string if displayName is not present', () => {
    const breadcrumbs = [{}];
    const expectedResult = '';
    expect(wrapper.instance().getGender(breadcrumbs)).toEqual(expectedResult);
  });

  it('should render badge and title correctly', () => {
    const name = 'Girl Polo';
    const badge = 'Online Exclusive';
    wrapper.setProps({ isStyleWith: {} });
    expect(wrapper.instance().renderBadgeAndTitleContainer(name, badge)).toMatchSnapshot();
  });

  it('should return color for item when getItemColor method is called', () => {
    const formValues = {
      color: 'black',
    };
    wrapper.setProps({ formValues });
    const expected = 'black';
    expect(wrapper.instance().getItemColor().color.name).toEqual(expected);
  });

  it('should render the size drawer header correctly', () => {
    wrapper.setProps({
      isKeepAliveEnabled: true,
      isBossEligible: true,
      pickupLabels: { lbl_Product_pickup_SELECT_SIZE_WARNING_TEXT_redesign: '' },
      formValues: { size: '12' },
      isNewReDesignEnabled: false,
    });
    wrapper.setState({ selectedSizeDrawerTab: 0 });
    expect(wrapper.instance().renderSizeDrawerHeader()).toMatchSnapshot();
  });

  it('should render the size with tab shippig', () => {
    wrapper.setProps({
      isKeepAliveEnabled: false,
      isBossEligible: false,
      pickupLabels: { lbl_Product_pickup_SELECT_SIZE_WARNING_TEXT_redesign: '' },
      formValues: { size: '12' },
      isNewReDesignEnabled: false,
    });
    wrapper.setState({ selectedSizeDrawerTab: 1 });
    expect(wrapper.instance().renderSizeDrawerHeader()).toMatchSnapshot();
  });

  it('should render the size with tab shippig', () => {
    wrapper.setProps({
      isKeepAliveEnabled: false,
      isBossEligible: false,
      pickupLabels: { lbl_Product_pickup_SELECT_SIZE_WARNING_TEXT_redesign: '' },
      formValues: { size: '12' },
      isNewReDesignEnabled: true,
    });
    wrapper.setState({ selectedSizeDrawerTab: 1 });
    expect(wrapper.instance().renderSizeDrawerHeaderBossBopis()).toMatchSnapshot();
  });

  it('should return true if size exist and is not an object', () => {
    expect(wrapper.instance().checkSizeTruthValue('XL')).toBeTruthy();
  });

  it('should return false if size doest exist or is an object', () => {
    expect(wrapper.instance().checkSizeTruthValue('')).toBeFalsy();
  });

  it('should return correct border radius for TCP', () => {
    const expectedResult = '16px';
    expect(wrapper.instance().getButtonBorderRadius()).toEqual(expectedResult);
  });

  it('should render AddToBag button correctly', () => {
    wrapper.setProps({
      pdpLabels: { selectSizeLabel: selectSize },
      isLoading: false,
      isKeepAliveEnabled: false,
    });
    expect(wrapper.instance().checkaddtobag()).toMatchSnapshot();
  });

  it('should close size drawer and open pick up modal when handlePickupModalClick is called', () => {
    const mockFunc = jest.fn();
    wrapper.setProps({ onPickUpOpenClick: mockFunc });
    wrapper.instance().handlePickupModalClick();
    expect(mockFunc).toHaveBeenCalled();
  });

  it('should return correct styling for sticky buttons if showSize in state is false', () => {
    const expectedResult = {
      flexDirection: 'row',
      height: 80,
      borderWidth: 1,
      borderRadius: 0,
      zIndex: 100,
      paddingBottom: 25,
    };

    wrapper.setState({ showSize: false });
    expect(wrapper.instance().stickyButtonContainerStyling()).toEqual(expectedResult);
  });

  it('should call updateSizeValue when e is present', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    const color = 'black';
    const element = 'XL';
    const updateSizeValue = jest.fn();
    wrapper.setProps({ updateSizeValue });
    wrapper.instance().onChangeSize(color, element);
    expect(updateSizeValue).toHaveBeenCalledWith(element);
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should call updateFit when fit is changed', () => {
    const color = 'black';
    const element = '';
    const fit = 'Husky';
    const updateFit = jest.fn();
    wrapper.setProps({ updateFit });
    wrapper.instance().onChangeSize(color, element, fit);
    expect(updateFit).toHaveBeenCalledWith(fit);
  });

  it('should reset size when fit is changed', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    const updateSizeValue = jest.fn();
    wrapper.setProps({ updateSizeValue });
    wrapper.instance().onChangeFit();
    expect(updateSizeValue).toHaveBeenCalledWith('');
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should return preselected size if not an accessory', () => {
    const formValues = {
      size: 'XL',
    };
    const expectedResult = 'XL';
    wrapper.setProps({ pdpLabels: { selectSizeLabel: 'Select Size' }, formValues });
    expect(wrapper.instance().showPreSelectedSize()).toEqual(expectedResult);
  });

  it('should return preselected size if an accessory', () => {
    const formValues = {
      size: 'ONE SIZE',
    };
    const expectedResult = 'ONE SIZE';
    wrapper.setProps({ pdpLabels: { selectSizeLabel: 'Select Size' }, formValues });
    wrapper.setState({ accessorySize: true });
    expect(wrapper.instance().showPreSelectedSize()).toEqual(expectedResult);
  });

  it('should return please select a value if giftcard not selected', () => {
    const component = shallow(<ProductDetailNewVanilla {...newProps} />);
    expect(component.instance().showPreSelectedSize()).toEqual('Select a Value');
  });

  it('should return correct text if product is a gift card and one is selected', () => {
    const formValues = {
      size: '25',
    };
    const component = shallow(<ProductDetailNewVanilla {...newProps} />);
    component.setProps({ formValues });
    expect(component.instance().showPreSelectedSize()).toEqual('$25');
  });
});

const props4 = {
  plpLabels: { addToBag: '' },
  selectedColorProductId: '',
  pickupLabels: { lbl_Product_pickup_FIND_STORE: findInStore },
  currentProduct: {
    name: '',
    shortDescription: '',
    generalProductId: '12112_11',
    colorFitsSizesMap: [
      {
        favoritedCount: 0,
        color: {
          name: 'black',
        },
        miscInfo: {
          isBopisEligible: false,
          isBossEligible: true,
          badge2: undefined,
          isClearance: undefined,
          badge1: {
            defaultBadge: onlineExclusive,
          },
          hasOnModelAltImages: undefined,
          videoUrl: undefined,
          keepAlive: false,
          isInDefaultWishlist: false,
          groupBadgeLabel: undefined,
        },
        fits: [
          {
            sizes: [
              {
                sizeName: '4',
              },
            ],
          },
        ],
        listPrice: 9.95,
        offerPrice: 9.95,
        highListPrice: 0,
        highOfferPrice: 0,
        lowListPrice: 9.95,
        lowOfferPrice: 0,
      },
    ],
    bazaarVoice: {
      totalReviewCount: 3,
    },
  },
  navigation: {
    getParam: () => {},
    setParams: jest.fn(),
    getScreenProps: () => {
      return { deviceTier: 'tier1' };
    },
  },
};

describe.skip('ProductDetailNew component describe two', () => {
  let wrapper;
  beforeEach(() => {
    const propsNew = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      pickupLabels: { lbl_Product_pickup_FIND_STORE: findInStore },
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            miscInfo: {
              isBopisEligible: false,
              isBossEligible: true,
              badge2: undefined,
              isClearance: undefined,
              badge1: {
                defaultBadge: onlineExclusive,
              },
              hasOnModelAltImages: undefined,
              videoUrl: undefined,
              keepAlive: false,
              isInDefaultWishlist: false,
              groupBadgeLabel: undefined,
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
            listPrice: 9.95,
            offerPrice: 9.95,
            highListPrice: 0,
            highOfferPrice: 0,
            lowListPrice: 9.95,
            lowOfferPrice: 0,
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    wrapper = shallow(<ProductDetailNewVanilla {...propsNew} />);
  });

  it('should call set state when onChangeQuantity is called', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().onChangeQuantity(4);
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should call set state when onMultiPackChange is called', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().onMultiPackChange();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should render share icon correctly', () => {
    expect(wrapper.instance().renderShare()).toMatchSnapshot();
  });

  it('should call set state when comingFromPDPSwatch is called', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().comingFromPDPSwatch();
    expect(setStateSpy).toHaveBeenCalled();
  });
  it('should return correct price when getProductPrices method is called', () => {
    wrapper.setState({
      size: '',
      fit: '',
      currentColorEntry: {
        pdpUrl: '/p/3011599_OM',
        pdpSeoUrl: 'Toddler-Girls-Uniform-Short-Sleeve-Pique-Polo-3011599-OM',
        name: null,
        colorProductId: 1575242,
        colorDisplayId: '3011599_OM',
        categoryEntity: '',
        imageName: '3011599_OM',
        favoritedCount: 151,
        maxAvailable: 2702,
        maxAvailableBoss: 7478,
        hasFits: false,
        listPrice: 9.95,
        offerPrice: 9.95,
        highListPrice: 0,
        highOfferPrice: 0,
        lowListPrice: 9.95,
        lowOfferPrice: 0,
        color: {
          name: 'SEAFROST',
          imagePath: null,
          family: 'BLUE',
          swatchImage: '3011599_OM_swatch.jpg',
        },
      },
    });
    expect(wrapper.instance().getProductPrices(false)).toBeTruthy();
  });

  it('should return ratings if product is not a gift card or bundle product', () => {
    const ratings = 4.4;
    expect(wrapper.instance().shouldRenderRatingReview(false, false, ratings)).toEqual(ratings);
  });

  it('should render pick up in store button correctly', () => {
    expect(wrapper.instance().renderPickUpInStoreAnchorButton()).toMatchSnapshot();
  });

  it('should not render pick up in store button if product is a gift card', () => {
    const newProps = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      pickupLabels: { lbl_Product_pickup_FIND_STORE: findInStore },
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        isGiftCard: true,
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            miscInfo: {
              isBopisEligible: false,
              isBossEligible: true,
              badge2: undefined,
              isClearance: undefined,
              badge1: {
                defaultBadge: onlineExclusive,
              },
              hasOnModelAltImages: undefined,
              videoUrl: undefined,
              keepAlive: false,
              isInDefaultWishlist: false,
              groupBadgeLabel: undefined,
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
            listPrice: 9.95,
            offerPrice: 9.95,
            highListPrice: 0,
            highOfferPrice: 0,
            lowListPrice: 9.95,
            lowOfferPrice: 0,
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const component = shallow(<ProductDetailNewVanilla {...newProps} />);
    expect(component.instance().renderPickUpInStoreAnchorButton()).toBeNull();
  });

  it('should render related outfits correctly', () => {
    expect(wrapper.instance().renderRelatedOutfits(false)).toMatchSnapshot();
  });

  it('should call set state when callbackIndex method is called', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().callbackIndex(2);
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should return correct styling when stickyButtonContainerStyling is called and showSize is true', () => {
    wrapper.setState({ showSize: true });
    const expectedResult = {
      flexDirection: 'column',
      height: 'auto',
      borderWidth: 0,
      borderRadius: 30,
    };

    expect(wrapper.instance().stickyButtonContainerStyling()).toEqual(expectedResult);
  });

  it('should call set state when onChangeSize is called and product is a gift card', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    const color = 'black';
    const element = 'XL';
    const updateSizeValue = jest.fn();
    const newProps = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '',
      pickupLabels: { lbl_Product_pickup_FIND_STORE: findInStore },
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        isGiftCard: true,
        colorFitsSizesMap: [
          {
            favoritedCount: 0,
            color: {
              name: 'black',
            },
            miscInfo: {
              isBopisEligible: false,
              isBossEligible: true,
              badge2: undefined,
              isClearance: undefined,
              badge1: {
                defaultBadge: onlineExclusive,
              },
              hasOnModelAltImages: undefined,
              videoUrl: undefined,
              keepAlive: false,
              isInDefaultWishlist: false,
              groupBadgeLabel: undefined,
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '4',
                  },
                ],
              },
            ],
            listPrice: 9.95,
            offerPrice: 9.95,
            highListPrice: 0,
            highOfferPrice: 0,
            lowListPrice: 9.95,
            lowOfferPrice: 0,
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
    };
    const component = shallow(<ProductDetailNewVanilla {...newProps} />);
    component.setProps({ updateSizeValue });
    component.instance().onChangeSize(color, element);
    expect(updateSizeValue).toHaveBeenCalledWith(element);
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should call set state when showSize method is called', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().showSize();
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should call set state when getCurrentSelectImageUrls method is called', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    const color = 'black';
    wrapper.instance().getCurrentSelectImageUrls(color);
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should render sticky buttons correctly', () => {
    expect(wrapper.instance().renderStickyButtons()).toMatchSnapshot();
  });

  it('should call setState when onTabChange', () => {
    const setStateSpy = jest.spyOn(ProductDetailNewVanilla.prototype, 'setState');
    wrapper.instance().onTabChange(1);
    expect(setStateSpy).toHaveBeenCalled();
  });

  it('should return getStoreAvailabilityStatus', () => {
    wrapper.setProps({
      bopisItemInventory: {
        inventoryResponse: [
          {
            quantity: 'In Stock',
          },
        ],
      },
    });
    expect(wrapper.instance().getStoreAvailabilityStatus()).toEqual('In Stock');
  });

  it('should return getQuantityFromBOPIS', () => {
    wrapper.setProps({
      bopisItemInventory: {
        inventoryResponse: [
          {
            status: 12,
          },
        ],
      },
    });
    expect(wrapper.instance().getQuantityFromBOPIS()).toEqual(12);
  });

  it('should return getButtonTextNewDesign tab 0', () => {
    wrapper.setProps({ pickupLabels: { lbl_Product_pickup_FIND_STORE: 'Find Store' }, size: 1 });
    wrapper.setState({ selectedSizeDrawerTab: 0 });
    expect(wrapper.instance().getButtonTextNewDesign()).toEqual(12);
  });

  it('should return getButtonTextNewDesign tab 1', () => {
    wrapper.setProps({ pickupLabels: { lbl_Product_pickup_FIND_STORE: 'Find Store' }, size: 1 });
    wrapper.setState({ selectedSizeDrawerTab: 1 });
    expect(wrapper.instance().getButtonTextNewDesign()).toEqual(12);
  });

  it('should return getNoRushPickupDate', () => {
    wrapper.setProps({
      userDefaultStore: {
        storeBossInfo: {
          startDate: '11/06/2021',
          endDate: '11/12/2021',
        },
      },
    });
    expect(wrapper.instance().getNoRushPickupDate()).toEqual('November 6-12');
  });

  it('should render renderBopisStatusAvialability isStoreEligible', () => {
    wrapper.setProps({
      pickupLabels: { lbl_Product_pickup_BOPIS_IN_THE_STORE_redesign: 'In Store' },
      isStoreBopisEligible: true,
    });
    expect(wrapper.instance().renderBopisStatusAvialability()).toMatchSnapshot();
  });

  it('should render renderBopisStatusAvialability isStore not Eligible', () => {
    wrapper.setProps({
      pickupLabels: { lbl_Product_pickup_BOPIS_IN_THE_STORE_redesign: 'Select' },
      isStoreBopisEligible: false,
    });
    expect(wrapper.instance().renderBopisStatusAvialability()).toMatchSnapshot();
  });

  it('should render pickDateContainer', () => {
    wrapper.setProps({ isStoreBopisEligible: false });
    wrapper.setState({ selectedSizeDrawerTab: 2 });
    expect(wrapper.instance().pickDateContainer()).toMatchSnapshot();
  });

  it('should render renderShowChangeStore', () => {
    wrapper.setProps({
      userDefaultStore: {
        basicInfo: {
          address: 'address',
          storeName: 'storeName',
        },
      },
    });
    wrapper.setState({ selectedSizeDrawerTab: 2 });
    expect(wrapper.instance().renderShowChangeStore()).toMatchSnapshot();
  });

  it('should call getDerivedStateFromProps', () => {
    const props5 = {
      plpLabels: { addToBag: '' },
      selectedColorProductId: '1',
      currentProduct: {
        name: '',
        shortDescription: '',
        generalProductId: '12112_11',
        colorFitsSizesMap: [
          {
            favoritedCount: 2,
            color: {
              name: 'green',
            },
            fits: [
              {
                sizes: [
                  {
                    sizeName: '5',
                  },
                ],
              },
            ],
          },
        ],
        bazaarVoice: {
          totalReviewCount: 3,
        },
      },
      navigation: {
        getParam: () => {},
        setParams: jest.fn(),
        state: {
          params: {
            isSBP: true,
          },
        },
        getScreenProps: () => {
          return { deviceTier: 'tier1' };
        },
      },
      trackPageLoad: () => {},
      isDynamicBadgeEnabled: true,
      TCPStyleType: '0002',
      TCPStyleQTY: 10,
    };

    const state = {
      currentColorEntry: {
        favoritedCount: 3,
        color: { name: 'green' },
        fits: [
          {
            sizes: [
              {
                sizeName: '4',
              },
            ],
          },
        ],
      },
    };

    const result = ProductDetailNewVanilla.getDerivedStateFromProps(props5, state);
    expect(result).toEqual(result);
  });

  it('should call shouldComponentUpdate and return false when updated state and props are same', () => {
    const comp = shallow(<ProductDetailNewVanilla {...props4} />);
    const updatedProps = comp.instance().props;
    const newState = comp.instance().state;
    const shouldUpdate = comp.instance().shouldComponentUpdate(updatedProps, newState);
    expect(shouldUpdate).toBe(false);
  });
});
