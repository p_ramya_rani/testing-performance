import React from 'react';
import { shallow } from 'enzyme';
import PLCCBanner from '../views/PLCCBanner.view.native';

describe('PLCCBanner component', () => {
  const newProps = {
    pdpLabels: {
      myLabel: 'MY',
      placeLabel: 'Place',
      save30: 'save30',
      learnMoreLabel: 'learn more',
      creditCardLabel: 'Credit Card',
      plccDiscription: 'on your first purchase when you',
      rewardsLabel: 'REWARDS',
      plccMoreDiscription: 'open & use the',
    },
    navigation: {
      getParam: () => {},
      setParams: jest.fn(),
      getScreenProps: () => {
        return { deviceTier: 'tier1' };
      },
    },
    backGroundColor: true,
    openApplyNowModal: () => {},
  };

  it('should render correctly', () => {
    const wrapper = shallow(<PLCCBanner {...newProps} />);
    expect(wrapper).toMatchSnapshot();
  });
  it('should be defined', () => {
    const wrapper = shallow(<PLCCBanner {...newProps} />);
    expect(wrapper).toBeDefined();
  });
});
