// 9fbef606107a605d69c0edbcd8029e5d
import CUSTOMNAVIGATION_CONSTANTS from '../CustomNavigation.constant';

export const navigateTo = payload => {
  return {
    payload,
    type: CUSTOMNAVIGATION_CONSTANTS.CUSTOM_NAVIGATE_TO,
  };
};

export const isBoostrapDone = payload => {
  return {
    payload,
    type: CUSTOMNAVIGATION_CONSTANTS.IS_BOOTSTRAP_DONE,
  };
};

export const isNavigationDataDone = payload => {
  return {
    payload,
    type: CUSTOMNAVIGATION_CONSTANTS.IS_NAVIGATIONDATA_DONE,
  };
};

export const updateCustomNavigationState = payload => {
  return {
    payload,
    type: CUSTOMNAVIGATION_CONSTANTS.IS_NAVIGATE,
  };
};

export const setAppLaunch = payload => {
  return {
    payload,
    type: CUSTOMNAVIGATION_CONSTANTS.IS_APP_LAUNCH,
  };
};

export default { navigateTo };
