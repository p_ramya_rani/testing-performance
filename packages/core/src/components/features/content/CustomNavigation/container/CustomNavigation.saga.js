// 9fbef606107a605d69c0edbcd8029e5d
import { takeLatest, put, select } from 'redux-saga/effects';
import { updateCustomNavigationState } from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.actions';
import {
  getNavigationData,
  getNavigationLOneData,
  getAccessibilityData,
} from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.selectors';
import CUSTOMNAVIGATION_CONSTANTS from '../CustomNavigation.constant';

export const getParamSplitValue = (pageParams, index) => {
  return (pageParams && pageParams[index] && pageParams[index].split('=')) || [];
};

export const getRequiredParams = pageParamsWithKey => {
  let paramOne = [];
  let paramTwo = [];
  let paramThree = [];
  if (pageParamsWithKey.length === 1) {
    paramOne = getParamSplitValue(pageParamsWithKey, 0);
  } else if (pageParamsWithKey.length === 2) {
    paramOne = getParamSplitValue(pageParamsWithKey, 0);
    paramTwo = getParamSplitValue(pageParamsWithKey, 1);
  } else if (pageParamsWithKey.length === 3) {
    paramOne = getParamSplitValue(pageParamsWithKey, 0);
    paramTwo = getParamSplitValue(pageParamsWithKey, 1);
    paramThree = getParamSplitValue(pageParamsWithKey, 2);
  }
  return { paramOne, paramTwo, paramThree };
};

export const searchRedirect = (navigation, pageParamsWithKey) => {
  const { paramOne: searchTerm } = getRequiredParams(pageParamsWithKey);
  navigation.navigate({
    routeName: 'SearchDetail',
    params: {
      title: searchTerm.length > 0 && searchTerm[1],
      isForceUpdate: false,
    },
  });
};

export const plpRedirect = (navigation, pageParamsWithKey) => {
  const { paramOne: categoryId, paramTwo: title } = getRequiredParams(pageParamsWithKey);
  navigation.navigate({
    routeName: 'ProductListing',
    params: {
      reset: true,
      title: title.length > 1 && title[1],
      url: `${CUSTOMNAVIGATION_CONSTANTS.URL_PREFIX}${categoryId.length > 1 && categoryId[1]}`,
    },
  });
};

export const productDetailRedirect = (navigation, pageParamsWithKey) => {
  const { paramTwo: category, paramThree: productId } = getRequiredParams(pageParamsWithKey);
  navigation.navigate({
    routeName: 'ProductDetail',
    params: {
      reset: true,
      title: category.length > 1 && category[1],
      pdpUrl: productId.length > 1 && productId[1],
      selectedColorProductId: productId.length > 1 && productId[1], // replace deash with underscore
    },
  });
};

// function will navigate to the outfit listing tab
export const outfitListingRedirect = (navigation, pageParamsWithKey) => {
  const { paramTwo: seotoken, paramThree: title } = getRequiredParams(pageParamsWithKey);
  navigation.navigate({
    routeName: 'OutfitListing',
    params: {
      outfitPath: seotoken.length > 1 && seotoken[1],
      title: title.length > 1 && title[1],
      url: seotoken.length > 1 && `${CUSTOMNAVIGATION_CONSTANTS.URL_PREFIX}${seotoken[1]}`,
    },
  });
};

// function will navigate to the account tab
export const accountRedirect = (navigation, selectedTab, NavigationActions, StackActions) => {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({
        routeName: 'Home',
        action: NavigationActions.navigate({
          routeName: 'Account',
          params: { customNavigateToPage: selectedTab },
        }),
      }),
    ],
  });
  navigation.dispatch(resetAction);
};

// this function will check sub cat is exist then return the subcat of l2 nav
function* findCatL3Exist(subCategory, subCatId) {
  const result = [];
  Object.keys(subCategory).forEach(obj => {
    const data = subCategory[obj].items.filter(i => i.categoryContent.catgroupId === subCatId);
    if (data && data.length > 0) {
      result.push({ type: obj, item: data[0] });
    }
  });
  yield result;
}

//  this function will open L3 navigation of shop tab
const navigateToL3 = (navigation, accessibilityLabels, result) => {
  const { item } = result;
  let hasL3 = false;
  if (item.subCategories && item.subCategories.length) {
    hasL3 = true;
  }
  if (hasL3) {
    navigation.navigate('NavMenuLevel3', {
      navigationObj: item.subCategories,
      l2Title: item.categoryContent.name,
      accessibilityLabels,
    });
  }
};

// this function will open L2 navigation of shop tab
const navigateToL2 = (navigation, accessibilityLabels, result) => {
  const item = { item: result };
  const {
    item: {
      categoryContent: { name },
    },
  } = item;
  navigation.navigate({
    routeName: 'NavMenuLevel2',
    params: {
      navigationObj: item,
      l1Title: name,
      accessibilityLabels,
    },
  });
};

// function will handle L1 & L2 navigation of shop tab
const handleNavigationCategory = (
  selectedCategory,
  childCategoryId,
  navigation,
  accessibilityLabels
) => {
  if (childCategoryId) {
    const subCategory = selectedCategory.subCategories;
    const catLThree = findCatL3Exist(subCategory, childCategoryId);
    const result = catLThree.next().value;
    if (result && result[0] && result[0].item) {
      navigateToL3(navigation, accessibilityLabels, result[0]);
    } else {
      navigateToL2(navigation, accessibilityLabels, selectedCategory);
    }
  } else {
    navigateToL2(navigation, accessibilityLabels, selectedCategory);
  }
};

export const handleNavigationCategoryRoot = (
  navigation,
  pageParamsWithKey,
  navigationLOneData,
  accessibilityLabels
) => {
  const { paramOne: categoryId, paramThree: childCategoryId } = getRequiredParams(
    pageParamsWithKey
  );
  const categoryIdValue = (categoryId.length && categoryId[1]) || undefined;
  const childCategoryIdValue = (childCategoryId.length && childCategoryId[1]) || undefined;

  if (categoryIdValue) {
    const selectedCategory = navigationLOneData.filter(i => i.categoryId === categoryIdValue);
    if (selectedCategory && selectedCategory[0] && selectedCategory[0].subCategories) {
      handleNavigationCategory(
        selectedCategory[0],
        childCategoryIdValue,
        navigation,
        accessibilityLabels
      );
    }
  }
};

function navigateToPageUtil(page, navigation, pageParamsWithKey, NavigationActions, StackActions) {
  switch (page) {
    case CUSTOMNAVIGATION_CONSTANTS.PAGE_SEARCH:
      searchRedirect(navigation, pageParamsWithKey);
      break;
    case CUSTOMNAVIGATION_CONSTANTS.PAGE_BAG:
      navigation.navigate('Bag');
      break;
    case CUSTOMNAVIGATION_CONSTANTS.PAGE_PLP:
      plpRedirect(navigation, pageParamsWithKey);
      break;
    case CUSTOMNAVIGATION_CONSTANTS.PAGE_PDP:
      productDetailRedirect(navigation, pageParamsWithKey);
      break;
    case CUSTOMNAVIGATION_CONSTANTS.PAGE_OUTFIT:
      outfitListingRedirect(navigation, pageParamsWithKey);
      break;
    case CUSTOMNAVIGATION_CONSTANTS.PLCC_CARD:
      accountRedirect(
        navigation,
        CUSTOMNAVIGATION_CONSTANTS.MYPLACEREWARDSCCPAGEMOBILE,
        NavigationActions,
        StackActions
      );
      break;
    case CUSTOMNAVIGATION_CONSTANTS.PAGE_ACCOUNT:
      navigation.navigate('Account');
      break;
    default:
      break;
  }
}

export function* navigateToPage({
  payload: { navigation, NavigationActions, StackActions } = {},
} = {}) {
  const payload = {
    isNavigate: false,
    notification: {},
  };
  const notificationData = yield select(getNavigationData);
  let navigationLOneData = [];
  let accessibilityLabels = {};
  yield put(updateCustomNavigationState(payload));
  const { deepLink } = notificationData;
  const indexOfQ = deepLink.indexOf('?');
  if (indexOfQ) {
    const page = deepLink.substring(6, indexOfQ).toLowerCase();
    const pageParamsWithKey = deepLink.substring(indexOfQ + 1).split('&');
    if (page === CUSTOMNAVIGATION_CONSTANTS.CATEGORY) {
      navigationLOneData = yield select(getNavigationLOneData);
      accessibilityLabels = yield select(getAccessibilityData);
    }
    switch (page) {
      case CUSTOMNAVIGATION_CONSTANTS.FAVORITE:
        accountRedirect(
          navigation,
          CUSTOMNAVIGATION_CONSTANTS.MYFAVORITEPAGEMOBILE,
          NavigationActions,
          StackActions
        );
        break;
      case CUSTOMNAVIGATION_CONSTANTS.CATEGORY:
        handleNavigationCategoryRoot(
          navigation,
          pageParamsWithKey,
          navigationLOneData,
          accessibilityLabels
        );
        break;
      default:
        navigateToPageUtil(page, navigation, pageParamsWithKey, NavigationActions, StackActions);
    }
  }
}

export function* CustomNavigationSaga() {
  yield takeLatest(CUSTOMNAVIGATION_CONSTANTS.CUSTOM_NAVIGATE_TO, navigateToPage);
}

export default CustomNavigationSaga;
