// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import CUSTOMNAVIGATION_CONSTANTS from '../CustomNavigation.constant';

const INITIAL_STATE = fromJS({
  isNavigate: false,
  notificationId: null,
  notification: {},
  isBootstrapDone: false,
  isNavigationDataDone: false,
});

const CustomNavigatioReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CUSTOMNAVIGATION_CONSTANTS.IS_NAVIGATE:
      return state
        .set('isNavigate', action.payload.isNavigate)
        .set(
          'notificationId',
          action.payload.notificationId
            ? action.payload.notificationId
            : state.get('notificationId')
        )
        .set('notification', action.payload.notification);
    case CUSTOMNAVIGATION_CONSTANTS.IS_BOOTSTRAP_DONE:
      return state.set('isBootstrapDone', action.payload);
    case CUSTOMNAVIGATION_CONSTANTS.IS_NAVIGATIONDATA_DONE:
      return state.set('isNavigationDataDone', action.payload);
    case CUSTOMNAVIGATION_CONSTANTS.IS_APP_LAUNCH:
      return state.set('isAppLaunch', action.payload);
    default:
      return state;
  }
};

export default CustomNavigatioReducer;
