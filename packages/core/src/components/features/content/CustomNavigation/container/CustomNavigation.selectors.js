// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';

import {
  CUSTOM_NAVIGATION_REDUCER_KEY,
  NAVIGATION_REDUCER_KEY,
  LABEL_REDUCER_KEY,
} from '@tcp/core/src/constants/reducer.constants';

export const getCustomNavigationState = state => {
  return state[CUSTOM_NAVIGATION_REDUCER_KEY];
};

export const getNavigationState = createSelector(
  getCustomNavigationState,
  state => state && state.get('isNavigate')
);

export const getNavigationData = createSelector(
  getCustomNavigationState,
  state => state && state.get('notification')
);

export const getNotificationId = createSelector(
  getCustomNavigationState,
  state => state && state.get('notificationId')
);

export const getIsBootstrapDone = createSelector(
  getCustomNavigationState,
  state => state && state.get('isBootstrapDone')
);

export const getIsNavigationDataDone = createSelector(
  getCustomNavigationState,
  state => state && state.get('isNavigationDataDone')
);

export const getNavigationReducerState = state => {
  return state[NAVIGATION_REDUCER_KEY];
};

export const getNavigationLOneData = createSelector(
  getNavigationReducerState,
  state => (state && state.navigationData) || []
);

export const getLabelsReducerState = state => {
  return state[LABEL_REDUCER_KEY];
};

export const getAccessibilityData = createSelector(
  getLabelsReducerState,
  state => (state.Labels && state.Labels.global && state.Labels.global.accessibility) || {}
);

export const getIsAppLaunch = createSelector(
  getCustomNavigationState,
  state => state && state.get('isAppLaunch')
);

export default {
  getNavigationState,
  getNavigationData,
  getNotificationId,
  getIsBootstrapDone,
  getIsNavigationDataDone,
  getNavigationLOneData,
  getAccessibilityData,
  getIsAppLaunch,
};
