// 9fbef606107a605d69c0edbcd8029e5d 
import CUSTOMNAVIGATION_CONSTANTS from '../CustomNavigation.constant';
import { navigateTo, updateCustomNavigationState } from '../container/CustomNavigation.actions';

describe('#Custom Navigation actions', () => {
  it('navigateTo', () => {
    const data = {
      navigation: {},
      NavigationActions: {},
      StackActions: {},
    };
    expect(navigateTo(data)).toEqual({
      payload: { navigation: {}, NavigationActions: {}, StackActions: {} },
      type: CUSTOMNAVIGATION_CONSTANTS.CUSTOM_NAVIGATE_TO,
    });
  });

  it('updateCustomNavigationState', () => {
    const data = {
      isNavigate: false,
      notification: {},
    };
    expect(updateCustomNavigationState(data)).toEqual({
      payload: { isNavigate: false, notification: {} },
      type: CUSTOMNAVIGATION_CONSTANTS.IS_NAVIGATE,
    });
  });
});
