// 9fbef606107a605d69c0edbcd8029e5d 
import { takeLatest } from 'redux-saga/effects';

import CUSTOMNAVIGATION_CONSTANTS from '../CustomNavigation.constant';

import { navigateToPage, CustomNavigationSaga } from '../container/CustomNavigation.saga';

describe('#CustomNavigationSaga', () => {
  it('should return correct navigateToPage', () => {
    const generator = CustomNavigationSaga();
    let takeLatestDescriptor;
    function expectValue(action, value) {
      takeLatestDescriptor = generator.next().value;
      expect(takeLatestDescriptor).toEqual(takeLatest(action, value));
    }
    expectValue(CUSTOMNAVIGATION_CONSTANTS.CUSTOM_NAVIGATE_TO, navigateToPage);
  });
});

describe('navigateToPage Saga', () => {
  const mock = jest.fn();
  let generator;
  beforeEach(() => {
    generator = navigateToPage({
      payload: {
        navigation: {
          dispatch: mock,
          navigate: mock,
        },
        NavigationActions: {},
        StackActions: [],
      },
    });
  });

  it('navigateToPage for search page', () => {
    const res = {
      deepLink: 'tcp://search?search_term=jeans',
    };
    generator.next();
    generator.next(res);
    generator.next();
  });

  it('navigateToPage for bag page', () => {
    const res = {
      deepLink: 'tcp://bag?term=pag',
    };
    generator.next();
    generator.next(res);
    generator.next();
  });

  it('navigateToPage for plp page', () => {
    const res = {
      deepLink: 'tcp://plp?category_id=toddler-girl-skinny-jeans&title=Skinny',
    };
    generator.next();
    generator.next(res);
    generator.next();
  });

  it('navigateToPage for outfit page', () => {
    const res = {
      deepLink: 'tcp://outfit?department=Girls&seotoken=dressy-outfits&title=Title',
    };
    generator.next();
    generator.next(res);
    generator.next();
  });

  it('navigateToPage for pdp page', () => {
    const res = {
      deepLink: 'tcp://pdp?department=Girls&category=title&product_id=2043572-10',
    };
    generator.next();
    generator.next(res);
    generator.next();
  });

  it('navigateToPage for category page', () => {
    const res = {
      deepLink: 'tcp://category?category_id=47511&title=girls&child_category_id=49007',
    };
    generator.next();
    generator.next(res);
    generator.next();
    generator.next();
  });

  it('navigateToPage for default page', () => {
    const res = {
      deepLink: 'tcp://abc?category_id=877887&title=title&name=title',
    };
    generator.next();
    generator.next(res);
    generator.next();
  });
});
