// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import CustomNavigatioReducer from '../container/CustomNavigation.reducer';
import CUSTOMNAVIGATION_CONSTANTS from '../CustomNavigation.constant';

describe('CustomNavigatioReducer reducer', () => {
  const initialState = {
    isNavigate: false,
    notificationId: null,
    notification: {},
    isBootstrapDone: false,
    isNavigationDataDone: false,
  };
  const initialStateMutated = fromJS(initialState);

  const getNavigateAction = {
    type: CUSTOMNAVIGATION_CONSTANTS.IS_NAVIGATE,
    payload: {
      isNavigate: true,
      notificationId: 'abc-abc',
      notification: {},
    },
  };

  const getbootstrapAction = {
    type: CUSTOMNAVIGATION_CONSTANTS.IS_BOOTSTRAP_DONE,
    payload: true,
  };

  const getNavigationDoneAction = {
    type: CUSTOMNAVIGATION_CONSTANTS.IS_NAVIGATIONDATA_DONE,
    payload: true,
  };

  it('should return empty Navigation as default state', () => {
    expect(CustomNavigatioReducer(undefined, {})).toEqual(initialStateMutated);
  });

  it('IS_NAVIGATE', () => {
    const newState = CustomNavigatioReducer(initialStateMutated, {
      ...getNavigateAction,
    });
    expect(newState.get('isNavigate')).toEqual(getNavigateAction.payload.isNavigate);
    expect(newState.get('notificationId')).toEqual(getNavigateAction.payload.notificationId);
  });

  it('IS_BOOTSTRAP_DONE', () => {
    const newState = CustomNavigatioReducer(initialStateMutated, {
      ...getbootstrapAction,
    });
    expect(newState.get('isBootstrapDone')).toEqual(getbootstrapAction.payload);
  });

  it('IS_NAVIGATIONDATA_DONE', () => {
    const newState = CustomNavigatioReducer(initialStateMutated, {
      ...getNavigationDoneAction,
    });
    expect(newState.get('isNavigationDataDone')).toEqual(getNavigationDoneAction.payload);
  });
});
