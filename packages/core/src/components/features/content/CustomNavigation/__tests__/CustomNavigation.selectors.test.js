// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import { CUSTOM_NAVIGATION_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { getNavigationState, getNavigationData } from '../container/CustomNavigation.selectors';

describe('CustomNavigation Selectors', () => {
  it('#getNavigationState should return boolean', () => {
    const UserState = fromJS({
      isNavigate: false,
    });
    const State = {
      [CUSTOM_NAVIGATION_REDUCER_KEY]: fromJS({
        isNavigate: false,
      }),
    };
    expect(getNavigationState(State)).toEqual(UserState.get('isNavigate'));
  });

  it('#getNavigationData should return object', () => {
    const UserState = fromJS({
      notification: {
        push: '',
        deeplink: '',
      },
    });
    const State = {
      [CUSTOM_NAVIGATION_REDUCER_KEY]: fromJS({
        notification: {
          push: '',
          deeplink: '',
        },
      }),
    };
    expect(getNavigationData(State)).toEqual(UserState.get('notification'));
  });
});
