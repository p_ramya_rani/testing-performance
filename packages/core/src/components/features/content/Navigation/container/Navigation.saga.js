// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLatest } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  isMobileApp,
  getBootstrapCachedData,
  setBootstrapCachedData,
  getAPIConfig,
} from '@tcp/core/src/utils';
import { generateSessionId } from '@tcp/core/src/utils/cookie.util';
import { API_CONFIG } from '@tcp/core/src/services/config';
import { errorMessages } from '@tcp/core/src/services/api.constants';
import { ENV_PREVIEW } from '@tcp/core/src/constants/env.config';
import NavigationAbstractor from '../../../../../services/abstractors/bootstrap/navigation';
import { loadNavigationData, mergeAndLoadNavigationData } from './Navigation.actions';
import { isNavigationDataDone } from '../../CustomNavigation/container/CustomNavigation.actions';
// import { setBootstrapError } from '../../../../../reduxStore/actions';
import { FETCH_NAVIGATION_DATA } from './Navigation.constants';
import { validateReduxCache } from '../../../../../utils/cache.util';
import {
  LAST_SAVED_NAVIGATION_DATA_KEY,
  NAVIGATION_DATA_KEY,
} from '../../../../../constants/fallback-data/static.config';

const { sessionCookieKey } = API_CONFIG;
const errorHandler = (errorArgs) => {
  const { errorMsg, payload, component, errorObject = '' } = errorArgs;
  logger.error({
    error: errorMsg,
    errorTags: [`NAVIGATION Saga`, component],
    extraData: {
      ...payload,
      errorObject,
      [sessionCookieKey]: !isMobileApp() && generateSessionId(true),
    },
  });
};

const isErrorInNavigationData = (result) => !result || !result.length;

// eslint-disable-next-line complexity, sonarjs/cognitive-complexity
export function* fetchNavigationData(param) {
  const { payload } = param;
  let result = {};
  const { previewEnvId } = getAPIConfig();
  try {
    if (previewEnvId !== ENV_PREVIEW && isMobileApp()) {
      const { data } = yield call(
        getBootstrapCachedData,
        LAST_SAVED_NAVIGATION_DATA_KEY,
        NAVIGATION_DATA_KEY
      );
      if (data) {
        result = [...data];
      } else {
        try {
          result = yield call(NavigationAbstractor.getData, 'navigation');
          if (isErrorInNavigationData(result)) {
            const { navigationDataError } = errorMessages;
            // eslint-disable-next-line no-throw-literal
            throw {
              navigationDataError,
            };
          } else {
            setBootstrapCachedData(LAST_SAVED_NAVIGATION_DATA_KEY, NAVIGATION_DATA_KEY, result);
          }
        } catch (error) {
          const { data: previousSavedData, staticData } = yield call(
            getBootstrapCachedData,
            LAST_SAVED_NAVIGATION_DATA_KEY,
            NAVIGATION_DATA_KEY,
            true
          );
          const { navigationFirstLoadError } = errorMessages;
          if (previousSavedData) {
            result = [...previousSavedData];
          } else {
            errorHandler({
              errorMsg: navigationFirstLoadError,
              component: 'Navigation API',
              payload: { ...staticData },
              errorObject: error,
            });
            result = [...staticData];
          }
        }
      }
    } else {
      result = yield call(NavigationAbstractor.getData, {
        module: 'navigation',
        data: {
          depth: payload.depth,
          filter: 'processNestedData',
        },
      });
    }

    // If depth provided, we need to merge navigation result to existing redux state
    if (payload && payload.depth) {
      yield put(mergeAndLoadNavigationData(result));
    } else {
      yield put(loadNavigationData(result));
    }
    if (payload) yield put(isNavigationDataDone(payload));
  } catch (err) {
    const { navigationDataError = null } = err;
    const { navigationSagaError } = errorMessages;
    const errorMsg = navigationDataError || navigationSagaError;
    const customError = {
      isErrorInBootstrap: true,
      navigation_error: 1,
      bootstrapErrorMssg: errorMsg,
      result,
      navigationErrorResponse: JSON.stringify(result),
    };
    // yield put(setBootstrapError(customError));
    errorHandler({
      errorMsg,
      component: `NAVIGATION API`,
      payload: { ...customError },
      errorObject: err,
    });
  }
}
function* NavigationSaga() {
  const cachedFetchNavigation = validateReduxCache(fetchNavigationData);
  yield takeLatest(FETCH_NAVIGATION_DATA, cachedFetchNavigation);
}

export default NavigationSaga;
