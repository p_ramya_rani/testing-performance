// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const ImageSize = styled.Image`
  height: 60px;
  width: 60px;
`;

const RichTextWrapper = styled.View`
  min-height: 230px;
  margin: ${props => props.theme.spacing.ELEM_SPACING.MED};
  overflow: hidden;
`;

export { RichTextWrapper, ImageSize };
