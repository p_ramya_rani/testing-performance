// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import { PropTypes } from 'prop-types';
import { routerPush } from '@tcp/core/src/utils';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import { Row } from '@tcp/core/src/components/common/atoms';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import style from '../RedirectContentPage.style';

class RedirectContentView extends React.PureComponent {
  componentDidMount() {
    const {
      ContentId,
      getRichText,
      router: { query },
    } = this.props;

    if (query && query.errorType) {
      getRichText(ContentId, query.errorType);
    } else {
      routerPush('/', '/home');
    }
  }

  render() {
    const { className, ContentRichText } = this.props;
    return (
      <Row className={className}>
        <Espot richTextHtml={ContentRichText} className={className} />
      </Row>
    );
  }
}

RedirectContentView.propTypes = {
  className: PropTypes.string,
  ContentRichText: PropTypes.string,
  ContentId: PropTypes.string,
  getRichText: PropTypes.func,
  router: PropTypes.shape({
    query: PropTypes.shape({}),
  }),
};

RedirectContentView.defaultProps = {
  className: '',
  ContentRichText: '',
  ContentId: '',
  getRichText: () => {},
  router: {
    query: {},
  },
};

export default withStyles(errorBoundary(RedirectContentView), style);
export { RedirectContentView as RedirectContentViewVanilla };
