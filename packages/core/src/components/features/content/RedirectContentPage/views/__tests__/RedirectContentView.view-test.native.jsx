// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { RedirectContentViewVanilla } from '../RedirectContentView.view.native';

describe('RedirectContentView component', () => {
  it('should renders correctly', () => {
    const props = {
      ContentRichText: 'test',
    };
    const component = shallow(<RedirectContentViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
