// 9fbef606107a605d69c0edbcd8029e5d 
/* istanbul ignore file */
import React from 'react';
import { PropTypes } from 'prop-types';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import errorBoundary from '@tcp/core/src/components/common/hoc/withErrorBoundary';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { RichTextWrapper } from '../RedirectContentPage.style.native';
import style from '../RedirectContentPage.style';

class RedirectContentView extends React.PureComponent {
  componentDidMount() {
    const { ContentId, getRichText, navigation } = this.props;
    const errorType = navigation && navigation.getParam('errorType');
    getRichText(ContentId, errorType);
  }

  componentDidUpdate(prevProps) {
    const { ContentId, getRichText, navigation } = this.props;
    const errorType = navigation && navigation.getParam('errorType');
    if (ContentId !== prevProps.ContentId) {
      getRichText(ContentId, errorType);
    }
  }

  render() {
    const { ContentRichText } = this.props;
    return (
      <RichTextWrapper>
        <Espot isNativeView={false} richTextHtml={ContentRichText} />
      </RichTextWrapper>
    );
  }
}

RedirectContentView.propTypes = {
  ContentRichText: PropTypes.string,
  ContentId: PropTypes.string,
  getRichText: PropTypes.func,
  navigation: PropTypes.shape({}),
};

RedirectContentView.defaultProps = {
  ContentRichText: '',
  ContentId: '',
  getRichText: () => {},
  navigation: {},
};

export default withStyles(errorBoundary(RedirectContentView), style);
export { RedirectContentView as RedirectContentViewVanilla };
