// 9fbef606107a605d69c0edbcd8029e5d 
import REDIRECT_PAGE_CONSTANTS from './RedirectContentPage.constants';

export const fetchModuleX = payload => {
  return {
    payload,
    type: REDIRECT_PAGE_CONSTANTS.FETCH_MODULEX_CONTENT,
  };
};

export const setModuleX = payload => {
  return {
    payload,
    type: REDIRECT_PAGE_CONSTANTS.SET_CONTENT_DATA,
  };
};

export default {
  fetchModuleX,
  setModuleX,
};
