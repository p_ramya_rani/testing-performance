// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import { withRouter } from 'next/router'; //eslint-disable-line
import RedirectContentView from '../views';
import { getRedirectPageContent, getRedirectPageContentId } from './RedirectContentPage.selectors';
import { fetchModuleX } from './RedirectContentPage.actions';

const mapStateToProps = (state, ownProps) => {
  const {
    router: { query },
  } = ownProps;
  return {
    ContentId: getRedirectPageContentId(state, query.errorType),
    ContentRichText: getRedirectPageContent(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRichText: (cid, errType) => {
      dispatch(fetchModuleX({ cid: cid.contentId, errType }));
    },
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RedirectContentView)
);

export { RedirectContentView as RedirectContentViewVanilla };
