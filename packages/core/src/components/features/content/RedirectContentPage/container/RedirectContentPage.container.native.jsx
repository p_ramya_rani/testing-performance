// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import RedirectContentView from '../views';
import { getRedirectPageContent, getRedirectPageContentId } from './RedirectContentPage.selectors';
import { fetchModuleX } from './RedirectContentPage.actions';

const mapStateToProps = (state, ownProps) => {
  const errorType = ownProps.navigation && ownProps.navigation.getParam('errorType');
  return {
    ContentId: getRedirectPageContentId(state, errorType),
    ContentRichText: getRedirectPageContent(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRichText: (cid, errType) => {
      dispatch(fetchModuleX({ cid: cid.contentId, errType }));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RedirectContentView);

export { RedirectContentView as RedirectContentViewVanilla };
