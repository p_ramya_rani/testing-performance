// 9fbef606107a605d69c0edbcd8029e5d 
import REDIRECT_PAGE_CONSTANTS from './RedirectContentPage.constants';

const initialState = {};

const redirectPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case REDIRECT_PAGE_CONSTANTS.SET_CONTENT_DATA:
      return { ...state, pageRichText: action.payload.richText };
    default:
      return state;
  }
};

export default redirectPageReducer;
