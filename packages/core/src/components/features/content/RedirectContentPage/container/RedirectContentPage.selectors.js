// 9fbef606107a605d69c0edbcd8029e5d 
import { REDIRECT_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';

export const getRedirectPageContent = state => {
  return state[REDIRECT_REDUCER_KEY].pageRichText;
};

export const getRedirectPageContentId = (state, errType) => {
  let contentID;
  if (
    state.Labels &&
    state.Labels.checkout &&
    state.Labels.checkout.billing &&
    Array.isArray(state.Labels.checkout.billing.referred)
  ) {
    contentID =
      state.Labels.checkout.billing.referred.length &&
      state.Labels.checkout.billing.referred.find(label => label.name === `payment_msg_${errType}`);
  }
  return contentID;
};
