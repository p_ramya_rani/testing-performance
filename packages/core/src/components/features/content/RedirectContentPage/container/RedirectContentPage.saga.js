// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest } from 'redux-saga/effects';
import { getModuleX } from '@tcp/core/src/services/abstractors/common/moduleX';
import { isMobileApp, clearLoginCookiesFromAsyncStore } from '@tcp/core/src/utils';
import BAG_PAGE_ACTIONS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import CHECKOUT_ACTIONS from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.action';
import { resetFavoritesReducer } from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import {
  resetUserInfo,
  getUserInfo,
} from '@tcp/core/src/components/features/account/User/container/User.actions';
import {
  resetWalletAppState,
  resetCouponReducer,
} from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import { resetAirmilesReducer } from '@tcp/core/src/components/features/CnC/common/organism/AirmilesBanner/container/AirmilesBanner.actions';

import REDIRECT_PAGE_CONSTANTS from './RedirectContentPage.constants';
import { setModuleX } from './RedirectContentPage.actions';

export function* fetchModuleX({ payload }) {
  const { cid, errType } = payload;

  try {
    const result = yield call(getModuleX, cid);
    yield put(setModuleX(result));
    if (errType === 'disable' || errType === 'invalidate') {
      if (isMobileApp()) {
        yield put(resetWalletAppState());
      }
      yield put(resetUserInfo());
      yield put(CHECKOUT_ACTIONS.resetCheckoutReducer());
      yield put(resetAirmilesReducer());
      yield put(resetCouponReducer());
      yield put(BAG_PAGE_ACTIONS.resetCartReducer());
      yield put(resetFavoritesReducer());
      yield put(getUserInfo({ ignoreCache: true, loadFavStore: true }));
      if (isMobileApp()) {
        yield call(clearLoginCookiesFromAsyncStore);
      }
    }
  } catch (err) {
    yield null;
  }
}

function* RedirectSaga() {
  yield takeLatest(REDIRECT_PAGE_CONSTANTS.FETCH_MODULEX_CONTENT, fetchModuleX);
}

export default RedirectSaga;
