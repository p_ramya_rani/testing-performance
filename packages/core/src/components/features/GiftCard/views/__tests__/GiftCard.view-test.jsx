import React from 'react';
import { shallow } from 'enzyme';
import GiftCard from '../GiftCard.view';

describe('GiftCard Page', () => {
  it('should render correctly', () => {
    const props = {
      onSubmit: jest.fn(),
      labels: {},
    };
    const tree = shallow(<GiftCard {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
