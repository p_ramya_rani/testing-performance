// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import GiftCardSection from '../organism/GiftSection';

const GiftView = React.memo(
  ({
    onSubmit,
    giftCardBalance,
    giftcardBalanceError,
    labels,
    isRecapchaEnabled,
    resetGiftCardBalance,
    formErrorMessage,
    trackWebGiftCardBalancePageView,
    trackMobilePageViewGcb,
  }) => (
    <GiftCardSection
      onSubmit={onSubmit}
      resetGiftCardBalance={resetGiftCardBalance}
      giftCardBalance={giftCardBalance}
      giftcardBalanceError={giftcardBalanceError}
      labels={labels}
      isRecapchaEnabled={isRecapchaEnabled}
      formErrorMessage={formErrorMessage}
      trackWebGiftCardBalancePageView={trackWebGiftCardBalancePageView}
      trackMobilePageViewGcb={trackMobilePageViewGcb}
    />
  )
);

GiftView.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  giftCardBalance: PropTypes.shape({}),
  giftcardBalanceError: PropTypes.string,
  resetGiftCardBalance: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  isRecapchaEnabled: PropTypes.boolean,
  formErrorMessage: PropTypes.shape({}).isRequired,
  trackWebGiftCardBalancePageView: PropTypes.func.isRequired,
  trackMobilePageViewGcb: PropTypes.func.isRequired,
};

GiftView.defaultProps = {
  giftCardBalance: {},
  giftcardBalanceError: '',
  resetGiftCardBalance: '',
  isRecapchaEnabled: false,
};

export default GiftView;
