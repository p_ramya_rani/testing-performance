import styled from 'styled-components';
import { ScrollView } from 'react-native';

const getPageStyle = (props) => {
  const { theme } = props;
  return `
  margin:20px
  margin-bottom: ${theme.spacing.APP_LAYOUT_SPACING.XS}
  `;
};
const ScrollViewStyle = styled(ScrollView)`
  ${getPageStyle}
`;
export default ScrollViewStyle;
