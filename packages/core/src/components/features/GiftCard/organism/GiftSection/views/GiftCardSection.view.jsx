// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import GiftCardForm from '../../../molecules/GiftCardForm/views/GiftCardForm.view';
import GiftCardBalance from '../../../molecules/GiftCardBalance/views/GiftCardBalance.view';

const GiftCardSection = ({
  className,
  onSubmit,
  giftCardBalance,
  giftcardBalanceError,
  labels,
  formErrorMessage,
  trackWebGiftCardBalancePageView,
}) => {
  React.useEffect(() => {
    const giftCardBalanceValue = 'gift card balance';
    trackWebGiftCardBalancePageView({
      pageName: giftCardBalanceValue,
      pageSection: giftCardBalanceValue,
      pageSubSection: giftCardBalanceValue,
      pageType: giftCardBalanceValue,
    });
  }, []);
  return (
    <div className={`${className}`}>
      <GiftCardForm
        onSubmit={onSubmit}
        giftCardBalance={giftCardBalance}
        giftcardBalanceError={giftcardBalanceError}
        labels={labels}
        formErrorMessage={formErrorMessage}
      />
      {giftCardBalance && Object.keys(giftCardBalance).length > 0 && (
        <GiftCardBalance giftCardBalance={giftCardBalance} />
      )}
    </div>
  );
};

GiftCardSection.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  className: PropTypes.string,
  giftcardBalanceError: PropTypes.string,
  giftCardBalance: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  formErrorMessage: PropTypes.shape({}).isRequired,
  trackWebGiftCardBalancePageView: PropTypes.func.isRequired,
};

GiftCardSection.defaultProps = {
  giftCardBalance: {},
  className: '',
  giftcardBalanceError: '',
};

export default React.memo(GiftCardSection);
export { GiftCardSection as GiftCardSectionVanilla };
