import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import GiftCardForm from '../../../molecules/GiftCardForm/views/GiftCardForm.view.native';
import ScrollViewStyle from '../styles/GiftCardSection.style.native';

const GiftCardSection = ({
  className,
  onSubmit,
  giftCardBalance,
  giftcardBalanceError,
  labels,
  isRecapchaEnabled,
  resetGiftCardBalance,
  trackMobilePageViewGcb,
}) => {
  React.useEffect(() => {
    const giftCardBalanceValue = 'gift card balance';
    trackMobilePageViewGcb({
      pageName: giftCardBalanceValue,
      pageSection: giftCardBalanceValue,
      pageSubSection: giftCardBalanceValue,
      pageType: giftCardBalanceValue,
      pageShortName: 'GCB',
    });
  }, []);

  return (
    <ScrollViewStyle testId={`${className}`} keyboardShouldPersistTaps="handled">
      <View testId="gift-card-section-wrap__container">
        <GiftCardForm
          onSubmit={onSubmit}
          resetGiftCardBalance={resetGiftCardBalance}
          giftCardBalance={giftCardBalance}
          giftcardBalanceError={giftcardBalanceError}
          labels={labels}
          isRecapchaEnabled={isRecapchaEnabled}
        />
      </View>
    </ScrollViewStyle>
  );
};

GiftCardSection.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  className: PropTypes.string,
  giftcardBalanceError: PropTypes.string,
  resetGiftCardBalance: PropTypes.string,
  giftCardBalance: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  isRecapchaEnabled: PropTypes.bool,
  trackMobilePageViewGcb: PropTypes.func.isRequired,
};

GiftCardSection.defaultProps = {
  giftCardBalance: {},
  className: '',
  giftcardBalanceError: '',
  isRecapchaEnabled: false,
  resetGiftCardBalance: '',
};

export default withStyles(React.memo(GiftCardSection));
export { GiftCardSection as GiftCardSectionVanilla };
