// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { GiftCardSectionVanilla } from '../GiftCardSection.view';

describe('GiftCardSection component', () => {
  const props = {
    onSubmit: () => {},
    labels: {},
  };
  it('should renders correctly', () => {
    const component = shallow(<GiftCardSectionVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
