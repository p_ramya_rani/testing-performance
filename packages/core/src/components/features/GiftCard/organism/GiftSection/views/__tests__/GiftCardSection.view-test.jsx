// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { GiftCardSectionVanilla } from '../GiftCardSection.view';
import GiftCardBalance from '../../../../molecules/GiftCardBalance/views/GiftCardBalance.view';

describe('GiftCardSection component', () => {
  const props = {
    onSubmit: () => {},
    labels: {},
  };
  it('should renders correctly', () => {
    const component = shallow(<GiftCardSectionVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders GiftCardBalance component', () => {
    props.giftCardBalance = {
      giftCardAuthorizedAmt: '0',
    };
    const component = shallow(<GiftCardSectionVanilla {...props} />);
    expect(component.find(GiftCardBalance)).toHaveLength(1);
  });
});
