// 9fbef606107a605d69c0edbcd8029e5d
import { GIFT_CARD_BALANCE_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { createSelector } from 'reselect';

export const getLabels = (state) => state?.Labels?.global;

export const getGiftCardResponse = (state) => {
  return state[GIFT_CARD_BALANCE_REDUCER_KEY].get('giftCardBalanceData');
};

export const getGiftCardError = (state) => {
  return state[GIFT_CARD_BALANCE_REDUCER_KEY].get('error');
};

export const getGiftCardBalance = createSelector(
  getGiftCardResponse,
  (giftCardData) => giftCardData
);

export const getGiftCardBalanceError = createSelector(
  [getGiftCardError, getLabels],
  (error, labels) => {
    return error ? labels.errorMessages.lbl_errorMessages_DEFAULT : null;
  }
);
