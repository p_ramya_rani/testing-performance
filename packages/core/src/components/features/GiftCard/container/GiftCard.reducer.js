// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import GIFT_CARD_CONSTANTS from '../GiftCard.constants';

const initialState = fromJS({
  error: null,
  giftCardBalanceData: null,
});

const GiftCardBalanceReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_SUCCESS: {
      const { giftCardAuthorizedAmt, giftCardNbr, giftCardPin } = payload;
      return state.set('error', null).set('giftCardBalanceData', {
        giftCardAuthorizedAmt,
        giftCardNbr,
        giftCardPin,
      });
    }
    case GIFT_CARD_CONSTANTS.RESET_GIFT_CARD_BALANCE: {
      return state.set('giftCardBalanceData', null).set('error', null);
    }
    case GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_FAILED:
      return state.set('giftCardBalanceData', null).set('error', fromJS(payload));
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default GiftCardBalanceReducer;
