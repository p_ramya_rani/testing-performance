// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { trackPageView } from '@tcp/core/src/analytics/actions';
import names from '@tcp/core/src/constants/eventsName.constants';
import GiftCardView from '../views';
import { giftCardBalanceRequest, resetGiftCardBalanceRequest } from './GiftCard.action';
import { getGiftCardBalance, getGiftCardBalanceError, getLabels } from './GiftCard.selectors';
import { getIsRecapchaEnabled } from '../../../../reduxStore/selectors/session.selectors';
import { getFormValidationErrorMessages } from '../../account/Account/container/Account.selectors';

export class GiftCardPageContainer extends React.PureComponent {
  componentWillUnmount() {
    const { resetGiftCardBalance } = this.props;
    resetGiftCardBalance();
  }

  render() {
    const {
      onSubmit,
      giftCardBalance,
      giftcardBalanceError,
      labels,
      isRecapchaEnabled,
      resetGiftCardBalance,
      formErrorMessage,
      trackWebGiftCardBalancePageView,
      trackMobilePageViewGcb,
    } = this.props;
    return (
      <GiftCardView
        resetGiftCardBalance={resetGiftCardBalance}
        onSubmit={onSubmit}
        giftCardBalance={giftCardBalance}
        giftcardBalanceError={giftcardBalanceError}
        labels={labels}
        isRecapchaEnabled={isRecapchaEnabled}
        formErrorMessage={formErrorMessage}
        trackWebGiftCardBalancePageView={trackWebGiftCardBalancePageView}
        trackMobilePageViewGcb={trackMobilePageViewGcb}
      />
    );
  }
}

GiftCardPageContainer.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  onSubmit: PropTypes.func.isRequired,
  giftCardBalance: PropTypes.shape({}),
  giftcardBalanceError: PropTypes.string,
  resetGiftCardBalance: PropTypes.string,
  isRecapchaEnabled: PropTypes.bool,
  formErrorMessage: PropTypes.shape({}).isRequired,
};

GiftCardPageContainer.defaultProps = {
  giftCardBalance: {},
  giftcardBalanceError: '',
  resetGiftCardBalance: null,
  isRecapchaEnabled: false,
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (payload) => {
      dispatch(giftCardBalanceRequest(payload));
    },
    resetGiftCardBalance: () => {
      dispatch(resetGiftCardBalanceRequest(null));
    },
    trackWebGiftCardBalancePageView: (payload) => {
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...payload,
                },
              },
            },
          },
        })
      );
    },
    trackMobilePageViewGcb: (payload) => {
      dispatch(
        trackPageView({
          currentScreen: names.screenNames.gcb,
          pageData: {
            ...payload,
          },
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...payload,
                },
              },
            },
          },
        })
      );
    },
  };
};

const mapStateToProps = (state) => {
  return {
    giftCardBalance: getGiftCardBalance(state),
    giftcardBalanceError: getGiftCardBalanceError(state),
    labels: getLabels(state),
    isRecapchaEnabled: getIsRecapchaEnabled(state),
    formErrorMessage: getFormValidationErrorMessages(state),
  };
};

GiftCardPageContainer.propTypes = {
  trackWebGiftCardBalancePageView: PropTypes.func.isRequired,
  trackMobilePageViewGcb: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(GiftCardPageContainer);
