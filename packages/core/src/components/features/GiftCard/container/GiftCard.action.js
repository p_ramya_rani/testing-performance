// 9fbef606107a605d69c0edbcd8029e5d
import GIFT_CARD_CONSTANTS from '../GiftCard.constants';

export const giftCardBalanceRequest = (payload) => ({
  type: GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_REQUEST,
  payload,
});

export const resetGiftCardBalanceRequest = () => ({
  type: GIFT_CARD_CONSTANTS.RESET_GIFT_CARD_BALANCE,
});

export const giftCardBalanceFailure = (payload) => ({
  type: GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_FAILED,
  payload,
});

export const giftCardBalanceSuccess = (payload) => ({
  type: GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_SUCCESS,
  payload,
});
