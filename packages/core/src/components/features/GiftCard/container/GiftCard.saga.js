// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put } from 'redux-saga/effects';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { isMobileApp } from '@tcp/core/src/utils';
import getGiftCardBalance from '../../../../services/abstractors/GiftCard/CheckBalance';
import GIFT_CARD_CONSTANTS from '../GiftCard.constants';
import { giftCardBalanceSuccess, giftCardBalanceFailure } from './GiftCard.action';

function resetRecaptch() {
  if (!isMobileApp()) {
    window.grecaptcha.reset();
  }
}

export function* giftCardBalance({ payload }) {
  try {
    yield put(setLoaderState(true));
    const response = yield call(getGiftCardBalance, payload);
    yield put(setLoaderState(false));
    resetRecaptch();
    if (response && response.body) {
      return yield put(giftCardBalanceSuccess(response.body));
    }
    return yield put(giftCardBalanceFailure(response));
  } catch (err) {
    yield put(setLoaderState(false));
    resetRecaptch();
    return yield put(giftCardBalanceFailure(err));
  }
}

export function* GiftCardSaga() {
  yield takeLatest(GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_REQUEST, giftCardBalance);
}

export default GiftCardSaga;
