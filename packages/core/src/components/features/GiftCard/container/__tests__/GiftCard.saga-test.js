// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLatest } from 'redux-saga/effects';
import { GiftCardSaga, giftCardBalance } from '../GiftCard.saga';
import { giftCardBalanceSuccess, giftCardBalanceFailure } from '../GiftCard.action';
import GIFT_CARD_CONSTANTS from '../../GiftCard.constants';
import getGiftCardBalance from '../../../../../services/abstractors/GiftCard/CheckBalance';

window.grecaptcha = {};
window.grecaptcha.reset = jest.fn();

describe('Gift Card Balance saga', () => {
  let gen;
  const payload = {
    giftCardNumber: null,
    giftCardPin: null,
    recaptchaToken: null,
  };

  beforeEach(() => {
    gen = giftCardBalance({ payload });
    gen.next();
  });

  it('should test GiftCardBalance', () => {
    gen = GiftCardSaga();
    expect(gen.next().value).toEqual(
      takeLatest(GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_REQUEST, giftCardBalance)
    );
    expect(gen.next().done).toBeTruthy();
  });

  it('should check gift card balance', () => {
    const res = {
      body: {
        giftCardAuthorizedAmt: '0',
        giftCardNumber: '12345678',
        giftCardPin: '123',
      },
    };
    expect(gen.next().value).toEqual(call(getGiftCardBalance, payload));
    gen.next(res);
    expect(gen.next(res).value).toEqual(put(giftCardBalanceSuccess(res.body)));
    expect(gen.next().done).toBeTruthy();
  });

  it('should fail gift card balance', () => {
    const err = {
      statusCode: 404,
      errorMessage: { error: 'Object not found' },
    };
    expect(gen.next().value).toEqual(call(getGiftCardBalance, payload));
    gen.throw(err);
    expect(gen.next(err).value).toEqual(put(giftCardBalanceFailure(err)));
    expect(gen.next().done).toBeTruthy();
  });
});
