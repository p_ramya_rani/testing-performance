// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import { getGiftCardError, getGiftCardResponse } from '../GiftCard.selectors';

describe('Gift Card selector', () => {
  const GiftCardBalanceState = fromJS({
    error: null,
    giftCardBalanceData: null,
  });

  const state = {
    giftCardBalance: GiftCardBalanceState,
  };

  it('GiftCardBalanceState should return error state', () => {
    expect(getGiftCardError(state)).toEqual(GiftCardBalanceState.get('error'));
  });

  it('GiftCardBalanceState should return response state', () => {
    expect(getGiftCardResponse(state)).toEqual(GiftCardBalanceState.get('giftCardBalanceData'));
  });
});
