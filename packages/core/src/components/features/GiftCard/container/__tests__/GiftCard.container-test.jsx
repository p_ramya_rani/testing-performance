// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import GiftCardContainer from '../GiftCard.container';

describe('GiftCardContainer', () => {
  const props = {
    onSubmit: jest.fn(),
    giftCardBalance: {},
    giftcardBalanceError: '',
    labels: {},
  };
  it('should render correctly', () => {
    const component = shallow(<GiftCardContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
