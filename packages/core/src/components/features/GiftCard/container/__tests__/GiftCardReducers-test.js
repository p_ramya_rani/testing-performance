// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS, Map } from 'immutable';
import GiftCardReducer from '../GiftCard.reducer';
import GIFT_CARD_CONSTANTS from '../../GiftCard.constants';

describe('GiftCardBalance Reducer', () => {
  const initialState = fromJS({
    error: null,
    giftCardBalanceData: null,
  });

  it('should return empty gift card as default state', () => {
    expect(GiftCardReducer(undefined, {}).get('giftCardBalanceData')).toBeNull();
    expect(GiftCardReducer(undefined, {}).get('error')).toBeNull();
  });

  it('should be called on gift card success', () => {
    const payload = {
      giftCardAuthorizedAmt: '0',
      giftCardNbr: '123456789',
      giftCardPin: '123',
    };
    expect(
      GiftCardReducer(initialState, {
        type: GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_SUCCESS,
        payload,
      })
    ).toEqual(
      Map({
        error: null,
        giftCardBalanceData: payload,
      })
    );
  });

  it('should be called on gift card failed', () => {
    const err = fromJS({
      statusCode: 400,
      message: 'something went wrong',
    });
    expect(
      GiftCardReducer(initialState, {
        type: GIFT_CARD_CONSTANTS.GIFT_CARD_BALANCE_FAILED,
        payload: err,
      })
    ).toEqual(
      fromJS({
        error: err,
        giftCardBalanceData: null,
      })
    );
  });
});
